import os
import sys
import shutil
import logging
from collections import OrderedDict

# pipeline
import rf_config as config
from rf_utils import file_utils
from rf_utils.context import context_info
from rf_utils.pipeline import version
from rf_utils import admin
cacheInfo = dict()

class Config:
    workspace = 'workspace'
    media = 'media'
    mov = 'mov'
    preview = 'preview'
    versionId = 'versionId'
    version = 'version'
    cache = 'cache'
    cacheTech = 'cacheTech'
    fbx = 'fbx'
    rsproxy = 'rsproxy'
    maRsproxy = 'maRsproxy'
    imageplane = 'imageplane'
    animCurve = 'animCurve'
    rig = 'rig'
    maya = 'maya'
    imagePath = 'imagePath'
    look = 'look'
    process = 'process'
    step = 'step'
    task = 'task'
    user = 'user'
    setOverride = 'setOverride'
    setShot = 'setShot'
    shotDress = 'shotDress'
    instDress = 'instDress'
    project = 'project'
    shotName = 'shotName'
    shotId = 'shotId'
    sound = 'sound'
    assets = 'assets'
    render = 'render'
    description = 'description'
    setDescription = 'setDescription'
    heroFile = 'heroFile'
    publishedFile = 'publishedFile'
    # process
    main = 'main'
    camera = 'camera'
    shotdress = 'shotdress'
    # shot header
    type = 'type'
    namespace = 'namespace'
    exportNamespace = 'exportNamespace'
    node = 'node'
    # steps
    animatic = 'animatic'
    layout = 'layout'
    anim = 'anim'
    sim = 'sim'
    fx = 'fx'
    light = 'light'
    comp = 'comp'
    outputTypes = ['cache', 'animCurve', 'shotDress']
    masterOrder = []
    preferSortingTitle = [project, shotName, shotId, user, sound, media, assets, render]
    preferSortingProcess = [main, camera, shotdress]
    preferSortingStep = [type, namespace, exportNamespace, animatic, layout, anim, sim, fx, light, comp]
    preferSortingOutput = [step, version, versionId, node, description, task, setDescription, workspace, mov, cache, rig, rsproxy, imageplane, animCurve, imagePath]

class RegType:
    asset = 'assets'
    media = 'media'
    render = 'render'
    dataTypes = ['abc', 'yeti', 'camera']

class Cache: 
    asmVersion = OrderedDict()


class Register(object):
    """docstring for Register"""
    def __init__(self, ContextPathInfo):
        super(Register, self).__init__()
        self.entity = ContextPathInfo
        self.configKey = '{0}_desc'.format(self.entity.name)
        self.prodName = 'temp_desc.yml'
        self.configHero = '{0}.hero.yml'.format(self.configKey)
        self.configVersion = '{0}.$version.yml'.format(self.configKey)
        self.regFile = self.tmp_config_path()
        self.datas = list()
        self.read()

    def load_version_datas(self): 
        self.datas = [file_utils.ymlLoader(a) for a in self.config_version()]

    def tmp_config_path(self):
        return '%s/%s' % (self.entity.path.asm_data().abs_path(), self.prodName)

    def hero_config_path(self):
        return '%s/%s' % (self.entity.path.asm_data().abs_path(), self.configHero)

    def config_version(self):
        """ return list of versions exclude hero and tmp """ 
        configPath = self.entity.path.asm_data().abs_path()
        versions = list()
        if os.path.exists(configPath):
            for file in file_utils.list_file(configPath): 
                filePath = '{}/{}'.format(configPath, file)
                if not filePath in [self.tmp_config_path(), self.hero_config_path()]: 
                    versions.append(filePath)
        return sorted(versions)

    def read(self):
        # dataFile = self.tmp_config_path()
        dataFile = self.config_path()
        data = file_utils.ymlLoader(dataFile) if os.path.exists(dataFile) else OrderedDict()
        self.data = data if not data == None else OrderedDict()

    def config_path(self):
        return self.tmp_config_path() if os.path.exists(self.tmp_config_path()) else self.hero_config_path()

    def set_hero_config(self, reset=False):
        if not reset:
            heroConfigFile = self.hero_config_path()
            data = file_utils.ymlLoader(heroConfigFile) if os.path.exists(heroConfigFile) else OrderedDict()
            self.data = data if not data == None else OrderedDict()
        if reset:
            self.read()

    def is_hero_config(self):
        return False if os.path.exists(self.tmp_config_path()) else True if self.hero_config_path() else None

    def read_version(self):
        if not Cache.asmVersion: 
            configVersions = sorted(self._get_register_version())
            if configVersions:
                for configVersion in configVersions:
                    name = os.path.basename(configVersion)
                    if not name in Cache.asmVersion.keys():
                        data = file_utils.ymlLoader(configVersion)
                        Cache.asmVersion[name] = data
        return Cache.asmVersion

    def write(self):
        dataFile = self.tmp_config_path()
        os.makedirs(os.path.dirname(dataFile)) if not os.path.exists(os.path.dirname(dataFile)) else None
        file_utils.ymlDumper(dataFile, self.data)
        return dataFile

    def register(self):
        """ write hero file and version """
        dataFile = self.write()
        publishData = self.publish_version(dataFile)
        self.remove_temp(dataFile)
        return publishData or dataFile

    def publish_version(self, dataFile):
        versionFile = self.get_publish_name(dataFile)
        heroFile = self.get_hero_name(dataFile)
        fileNew = self._is_file_new(dataFile, heroFile)
        if fileNew:
            shutil.copy2(dataFile, versionFile)
            shutil.copy2(dataFile, heroFile)
            return versionFile if os.path.exists(versionFile) else False

    def remove_temp(self, dataFile):
        admin.remove(dataFile)

    def set(self, dataObject=None):
        """ set data """
        inputData = dataObject.data
        self.data = combine_dict(inputData, self.data)
        self.data = self.sorted_dict(self.data)

    def set_data(self, entity, dataType, regType, step, key, namespace, 
            node='', adPath='', workspace=[], cache=[], cacheTech=[], fbx=[], maya=[], animCurve=[], setOverride=[], 
            setShot=[], shotDress=[], instDress=[], media=[], rsproxy=[], maRsproxy=[], 
            asmPath='', exportNamespace='', others={}):
        title = Title()
        title.set_title(entity)

        if dataType == 'asset':
            output = Output()
            output.add_task(entity.task)
            output.add_namespace(namespace)
            output.add_export_namespace(exportNamespace)
            output.add_type(regType)
            output.add_version(entity.publishVersion)
            output.add_description(adPath)
            output.add_setDrescription(asmPath) if asmPath else None
            output.add_node(node) if node else ''
            output.add_cache(publishedFile=cache[0], heroFile=cache[1]) if cache else None
            output.add_cache_tech(publishedFile=cacheTech[0], heroFile=cacheTech[1]) if cacheTech else None
            output.add_fbx(publishedFile=fbx[0], heroFile=fbx[1]) if fbx else None
            output.add_workspace(publishedFile=workspace[0], heroFile=workspace[1]) if workspace else None
            output.add_animcurve(publishedFile=animCurve[0], heroFile=animCurve[1]) if animCurve else None
            output.add_setoverride(publishedFile=setOverride[0], heroFile=setOverride[1]) if setOverride else None
            output.add_setshot(publishedFile=setShot[0], heroFile=setShot[1]) if setShot else None
            output.add_shotdress(publishedFile=shotDress[0], heroFile=shotDress[1]) if shotDress else None
            output.add_instdress(publishedFile=instDress[0], heroFile=instDress[1]) if instDress else None
            output.add_maya(publishedFile=maya[0], heroFile=maya[1]) if maya else None
            output.add_rsproxy(publishedFile=rsproxy[0], heroFile=rsproxy[1]) if rsproxy else None
            output.add_maya_rsproxy(publishedFile=maRsproxy[0], heroFile=maRsproxy[1]) if maRsproxy else None
            title.set_output(RegType.asset, key, step, output)
            self.set(title)
            self.write()
            return output

        if dataType == 'media':
            step = Step()
            output = Output()
            output.add_task(entity.task)
            output.add_version(entity.publishVersion)
            output.add_workspace(publishedFile=workspace[0], heroFile=workspace[1]) if workspace else None
            output.add_mov(publishedFile=media[0], heroFile=media[1]) if media else None
            step.set_output(step, output)
            title.set_media(step)
            self.set(title)
            self.write()
            return output

    def set_media(self, entity, workspace=[], media=[]):
        # media
        title = Title()
        step = Step()
        output = Output()

        output.add_task(entity.task)
        output.add_version(entity.publishVersion)
        output.add_workspace(publishedFile=workspace[0], heroFile=workspace[1]) if workspace else None
        output.add_mov(publishedFile=media[0], heroFile=media[1]) if media else None
        step.set_output(entity.step, output)
        title.set_media(step)
        self.set(title)
        self.write()
        return output

    def set_asset_output(self, key, step, output): 
        title = Title()
        title.set_title(self.entity)
        title.set_output(RegType.asset, key, step, output)
        self.set(title)
        self.write()
        return title 

    def set_media_output(self, step, output): 
        # media
        title = Title()
        stepObject = Step()
        stepObject.set_output(step, output)
        title.set_media(stepObject)
        self.set(title)
        self.write()
        return output

    @property
    def get(self):
        return GetInfo(self.data)

    @property
    def version(self):
        return GetVersion(self.read_version())

    def get_publish_name(self, dataFile):
        dirname = os.path.dirname(dataFile)
        replaceVersion = version.increment_version(os.path.dirname(dataFile))
        configFileVersion = self.configVersion.replace('$version', replaceVersion)
        configFile = '%s/%s' % (os.path.dirname(dataFile), configFileVersion)
        return configFile

    def get_hero_name(self, dataFile):
        dirname = os.path.dirname(dataFile)
        heroFile = '%s/%s' % (dirname, self.configHero)
        return heroFile


    def _combine_dict(self, dictData1, dictData2):
        for k, v in dictData2.iteritems():
            if not k in dictData1.keys():
                dictData1[k] = v
        return dictData1

    def _is_file_new(self, dataFile, heroFile):
        if not os.path.exists(heroFile):
            return True
        data1 = file_utils.ymlLoader(dataFile)
        data2 = file_utils.ymlLoader(heroFile)
        return True if not data1 == data2 else False

    def _get_register_version(self):
        dataDir = self.entity.path.asm_data().abs_path()
        files = file_utils.list_file(dataDir) if os.path.exists(dataDir) else []
        if files:
            files.remove(self.configHero) if self.configHero in files else None
            configFiles = [('/').join([dataDir, a]) for a in files]
            return configFiles

    def sorted_dict(self, data):
        sortedDict = OrderedDict()
        title = Title(data)
        for k, v in title.data.iteritems():
            elem = Element(v)
            elemDict = v
            if hasattr(elem.data, 'iteritems'):
                elemDict = OrderedDict()
                for elemKey, elemValue in elem.data.iteritems():
                    step = Step(elemValue)
                    stepDict = OrderedDict()
                    for stepKey, stepValue in step.data.iteritems():
                        output = Output(stepValue)
                        stepDict[stepKey] = output.data
                    elemDict[elemKey] = stepDict
            sortedDict[k] = elemDict

        return sortedDict

    def sorting_dict(self, data):
        newDict = self.sorting_data(data, Config.masterOrder)

        for key1, value1 in newDict.iteritems():
            sortDict1 = self.sorting_data(value1, Config.preferSortingRes) if type(value1) == type(OrderedDict()) else value1
            newDict[key1] = sortDict1
            if type(sortDict1) == type(OrderedDict()):
                for key2 in sortDict1.keys():
                    outputList = sortDict1[key2]
                    orderList = []
                    for value2 in outputList:
                        sortDict2 = self.sorting_data(value2, Config.preferSortingOutput) if type(value2) == type(OrderedDict()) else value2
                        orderList.append(sortDict2)
                    sortDict1[key2] = orderList
                newDict[key1] = sortDict1
        return newDict

    def sorting_data(self, dictData, orders):
        sortDict = OrderedDict()
        for order in orders:
            if order in dictData.keys():
                sortDict[order] = dictData[order]

        for key in dictData.keys():
            if not key in sortDict.keys():
                sortDict[key] = dictData[key]
        return sortDict

    def rearrange_version_data(self): 
        """ rearrange into more logical relaton 
        {key: 
            {step: 
                {version: 
                    {cache: {pub: file, hero: file}}}}

        kai_001: 
            anim: 
                v001: 
                    cache: 
                        pub: file
                        hero: file
                v002: 
                    cache: 
                        pub: file 
                        hero: file 
            sim: 
                v001: 
                    cache: 
                        pub: file 
                        hero: file 
                        
        """ 
        version_info = OrderedDict()
        self.load_version_datas()
        hero_info = OrderedDict()

        hero_keys = self.get.asset_list()

        for key in hero_keys: 
            step = self.get.step(key)
            version = self.get.version(key)
            hero_info[key] = {'step': step, 'version': version}

        for data in self.datas: 
            reg = GetInfo(data)
            keys = reg.asset_list()

            for key in keys: 
                item_dict = reg.asset(key)
                step = item_dict['step']
                version = item_dict['version']
                data['assets'][key]['hero'] = True if step == hero_info[key]['step'] and version == hero_info[key]['version'] else False

                if not key in version_info.keys(): 
                    step_dict = OrderedDict()
                    version_dict = OrderedDict()
                    version_dict[version] = data['assets'][key]
                    step_dict[step] = version_dict
                    version_info[key] = step_dict

                else: 
                    step_dict = version_info[key]
                    if not step in step_dict.keys(): 
                        version_dict = OrderedDict()
                        version_dict[version] = data['assets'][key]
                        step_dict[step] = version_dict
                    else: 
                        version_dict = step_dict[step]
                        if not version in version_dict.keys(): 
                            version_dict[version] = data['assets'][key]
                    
        return version_info

    def print_test(self): 
        version_info = self.rearrange_version_data()
        for k, step_data in version_info.items(): 
            print k
            for step, version_data in step_data.items(): 
                print '\t', step
                for version, file_data in version_data.items(): 
                    if file_data['hero']: 
                        print '\t\t', version, '(hero)'
                    else: 
                        print '\t\t', version
                    # print '\t\t {}'.format(file_data['cache']['heroFile'])


class GetVersion(object):
    """docstring for GetVersion"""
    def __init__(self, dataDict):
        super(GetVersion, self).__init__()
        self.dataDict = dataDict

    def assets(self, key, step=''):
        info = []
        # hero
        for k, data in self.dataDict.iteritems(): 
            info.append(GetInfo(data).asset(key, step=step))

        return info

    def caches(self, key, step='', hero=False): 
        files = []
        keyType = Config.heroFile if hero else Config.publishedFile
        for k, data in self.dataDict.iteritems(): 
            asset_list_version = GetInfo(data).asset_list()
            if key in asset_list_version:
                file = GetInfo(data).asset(key, step=step).get(Config.cache, dict()).get(keyType)
                if file and not file in files: 
                    files.append(file)

        return files

    def steps(self, key): 
        step_list = []
        keyType = Config.publishedFile
        for k, data in self.dataDict.iteritems(): 
            asset_list_version = GetInfo(data).asset_list()
            if key in asset_list_version:
                step = GetInfo(data).step(key)
                if step and not step in step_list: 
                    step_list.append(step)

        return step_list

    def hero_steps(self, key): 
        """ collect only step that has its hero path """
        step_list = []
        step_dict = OrderedDict()
        keyType = Config.publishedFile
        for k, data in self.dataDict.iteritems(): 
            asset_list_version = GetInfo(data).asset_list()
            if key in asset_list_version:
                step = GetInfo(data).step(key)
                cache = GetInfo(data).cache(key, hero=True)
                version = GetInfo(data).version(key)
                step_dict[cache] = step
        step_list = [v for k, v in step_dict.items()]
        if not step_list:
            return self.steps(key)
        return step_list


    def custom_key(self, key, step='', dataKey=''): 
        info = []
        for k, data in self.dataDict.iteritems(): 
            fileInfo = GetInfo(data).asset(key, step=step).get(dataKey, dict())
            if fileInfo and not fileInfo in info: 
                info.append(fileInfo)

        return info


class GetInfo(object):
    """docstring for GetInfo"""
    def __init__(self, data):
        super(GetInfo, self).__init__()
        # self.datas = datas
        self.data = data
        self.assetData = self.data.get(Config.assets, OrderedDict()) or OrderedDict()


    def asset_list(self, type=''):
        keys = self.assetData.keys()
        if not type: 
            return keys 
        return [a for a in keys if self.check_type(a) == type]


    def asset(self, key, step=''):
        # hero
        data = self.assetData.get(key, OrderedDict())
        if step:
            if data.get(Config.step) == step:
                return data
            else:
                # find in dir
                return OrderedDict()
        return data

    def asset_version(self, key, step=''):
        assetData = self.asset(key)
        regStep = assetData.get(Config.step)
        version = assetData.get(Config.version)
        if step:
            if step == regStep:
                return version
            else:
                versions = self._version(assetData, step)
                return versions[-1] if versions else []
        else:
            return version

    def asset_versions(self, key, step=''):
        return self.asset(key).get(Config.version, [])

    def step(self, key): 
        return self.asset(key).get(Config.step)

    # def all_step(self, key): 


    def version(self, key): 
        return self.asset(key).get(Config.version)

    def output_types(self, key): 
        data = self.asset(key)
        types = [a for a in Config.outputTypes if a in data.keys()]
        return types

    def camera(self):
        return [k for k, v in self.assetData.iteritems() if v.get(Config.type) == Config.camera]

    def cache(self, key, step='', hero=False, version=False):
        cachePath = self.asset(key, step).get(Config.cache, [])
        if cachePath:
            publishedFile = cachePath.get(Config.publishedFile)
            heroFile = cachePath.get(Config.heroFile)

            if hero:
                return heroFile
            if version:
                return publishedFile

        return 

        # read from folder
        print 'begin read from folder ...'
        cachePath = self.asset(key).get(Config.cache)
        if cachePath:
            publishedFile = cachePath.get(Config.publishedFile)
            info = cache_data(publishedFile)
            versionDict = info.get(step, dict()).get(key, dict())

            if version:
                return versionDict.get(sorted(versionDict.keys())[-1]) if versionDict else ''
            if hero:
                return versionDict.get(sorted(versionDict.keys())[0]) if versionDict else ''

    def cache_local(self, key, step='', hero=False, version=False): 
        # read from folder
        cachePath = self.asset(key).get(Config.cache)
        if cachePath:
            publishedFile = cachePath.get(Config.publishedFile)
            info = cache_data(publishedFile)
            versionDict = info.get(step, dict()).get(key, dict())

            if version:
                return versionDict.get(sorted(versionDict.keys())[-1]) if versionDict else ''
            if hero:
                return versionDict.get(sorted(versionDict.keys())[0]) if versionDict else ''


    def maya(self, key, step='', hero=False, version=False):
        cachePath = self.asset(key, step).get(Config.maya, [])
        if cachePath:
            publishedFile = cachePath.get(Config.publishedFile)
            heroFile = cachePath.get(Config.heroFile)

            if hero:
                return heroFile
            if version:
                return publishedFile

    def custom_data(self, key, step='', dataType='', hero=False, version=False):
        cachePath = self.asset(key, step).get(dataType, [])
        if cachePath:
            publishedFile = cachePath.get(Config.publishedFile)
            heroFile = cachePath.get(Config.heroFile)

            if hero:
                return heroFile
            if version:
                return publishedFile

    def custom_key(self, key, step='', dataType=''):
        return self.asset(key, step).get(dataType, [])

    def caches(self, key, step=''):
        # read from folder
        cachePath = self.asset(key).get(Config.cache)
        publishedFile = cachePath.get(Config.publishedFile)
        info = cache_data(publishedFile)
        versionDict = info.get(step, dict()).get(key, dict())

        return [versionDict.get(a) for a in sorted(versionDict.keys())]


    def list_types(self, preferredOrder=[]):
        orderedType = []
        dataType = list(set([v.get(Config.type) for k, v in self.assetData.iteritems()]))
        if not preferredOrder: 
            return dataType
        
        # sorting section 
        for typ in preferredOrder: 
            if typ in dataType: 
                orderedType.append(typ)
        for typ in dataType: 
            if not typ in orderedType: 
                orderedType.append(typ)
        return orderedType


    def type(self, data_type):
        """ find key by type, return list keys """ 
        return [k for k, v in self.assetData.iteritems() if v.get(Config.type) == data_type],

    def check_type(self, key):
        return self.asset(key).get(Config.type)


def cache_data(cachePath):
    # {'anim': {'char': {v001': cache path}}}
    cacheDict = dict()

    # hard coded, loop through folder structure
    skipSteps = ['output', '_data']
    cacheDir = os.path.dirname(cachePath)
    versionDir = os.path.split(cacheDir)[0]
    namespaceDir = os.path.split(versionDir)[0]
    stepDir = os.path.split(namespaceDir)[0]
    entityDir = os.path.split(stepDir)[0]

    if not entityDir in cacheInfo.keys(): 
        steps = file_utils.list_folder(entityDir)
        steps = [a for a in steps if not a in skipSteps]

        for step in steps:
            stepPath = '%s/%s/output' % (entityDir, step)

            nsDict = dict()
            if os.path.exists(stepPath):
                assetKeys = file_utils.list_folder(stepPath)

                for keyNs in assetKeys:
                    keyNsPath = '%s/%s' % (stepPath, keyNs)

                    if os.path.exists(keyNsPath):
                        versions = file_utils.list_folder(keyNsPath)
                        versionDict = dict()

                        for version in versions:
                            versionPath = '%s/%s' % (keyNsPath, version)
                            cacheFile = list_cache(versionPath)
                            versionDict[version] = cacheFile

                    nsDict[keyNs] = versionDict
            cacheDict[step] = nsDict
        cacheInfo[entityDir] = cacheDict

    return cacheInfo[entityDir]


def list_cache(path):
    ext = ['.abc', '.fur']
    cacheFile = file_utils.list_file(path)
    files = ['%s/%s' % (path, a) for a in cacheFile if os.path.splitext(a)[-1] in ext]

    if len(files) == 1:
        return files[0]

    if len(files) > 1:
        formatStr = files[0].split('.')[-2]
        cache = files[0].replace(formatStr, '%04d')
        return cache




class RegisterInfo(object):
    """docstring for RegisterInfo"""
    def __init__(self, regFile):
        super(RegisterInfo, self).__init__()
        self.regFile = regFile
        self.data = self.read()

    def read(self):
        data = file_utils.ymlLoader(self.regFile)
        return data

    @property
    def get(self):
        return GetInfo(self.data)


class Title(object):
    """docstring for Title"""
    def __init__(self, inputData=None):
        super(Title, self).__init__()
        self.inputData = OrderedDict() if inputData == None else inputData
        self.sorting = Config.preferSortingTitle

    @property
    def data(self):
        self.inputData = sorting_data(self.inputData, self.sorting)
        return self.inputData

    def set(self, name):
        if not name in self.inputData:
            outputDict = OrderedDict()
            self.inputData[name] = outputDict
        outputDict = self.inputData[name]
        return Element(outputDict)

    def set_title(self, entity=None, project=None, name=None, id=None, user=None):
        project = entity.project if entity else project
        name = entity.name if entity else name
        id = entity.id if entity else id
        user = ''
        # set values
        self.inputData[Config.project] = project
        self.inputData[Config.shotName] = name
        self.inputData[Config.shotId] = id
        self.inputData[Config.user] = user
        return self.inputData

    def set_media(self, element):
        self.inputData[Config.media] = element.data
        return Element(element.data)

    def set_asset(self, element):
        self.inputData[Config.assets] = element.data
        return Element(element.data)

    def set_render(self, element):
        self.inputData[Config.render] = element.data
        return Element(element.data)

    def set_output(self, title, namespace, stepName, output):
        element = self.set(title)
        element.set_output(namespace, stepName, output)
        return self.data

    def get_element(self, name):
        return Element(self.data[name]) if name in self.data.keys() else None



class Element(Title):
    """docstring for Element"""
    def __init__(self, inputData=None):
        super(Element, self).__init__()
        self.inputData = OrderedDict() if inputData == None else inputData
        self.sorting = Config.preferSortingProcess

    def set(self, name):
        if not name in self.inputData:
            outputDict = OrderedDict()
            self.inputData[name] = outputDict
        outputDict = self.inputData[name]
        return Step(outputDict)

    # def set_output(self, namespace, stepName, output):
    #     step = self.set(namespace)
    #     step.set_output(stepName, output)
    #     return self.data

    def set_output(self, namespace, stepName, output):
        output.add_step(stepName)
        if namespace in self.inputData.keys():
            currentOutput = self.inputData[namespace]
            combineOutput = combine_dict(output.data, currentOutput)
            self.inputData[namespace] = Output(combineOutput).data # sorting purpose
        else:
            self.inputData[namespace] = output.data
        return self.data

    def key(self):
        return self.data.keys()


class Step(Element):
    """docstring for Step"""
    def __init__(self, inputData=None):
        super(Step, self).__init__(inputData=inputData)
        self.inputData = OrderedDict() if inputData == None else inputData
        self.sorting = Config.preferSortingStep

    def set_step(self, name):
        if not name in self.inputData:
            outputDict = OrderedDict()
            self.inputData[name] = outputDict
        outputDict = self.inputData[name]
        return Output(outputDict)

    def set_output(self, name, output):
        if name in self.inputData.keys():
            currentOutput = self.inputData[name]
            combineOutput = combine_dict(output.data, currentOutput)
            self.inputData[name] = Output(combineOutput).data # sorting purpose
        else:
            self.inputData[name] = output.data
        return self.data


class Output(Step):
    """docstring for Output"""
    def __init__(self, inputData=None):
        super(Output, self).__init__(inputData=inputData)
        self.inputData = OrderedDict() if inputData == None else inputData
        self.sorting = Config.preferSortingOutput

    def add_task(self, value):
        self.add_key(Config.task, value)

    def add_namespace(self, value):
        self.add_key(Config.namespace, value)

    def add_export_namespace(self, value): 
        self.add_key(Config.exportNamespace, value)

    def add_type(self, value):
        self.add_key(Config.type, value)

    def add_step(self, value):
        self.add_key(Config.step, value)

    def add_preview(self, value):
        self.inputData[Config.preview] = value

    def add_version(self, value):
        self.inputData[Config.version] = value

    def add_version_id(self, value):
        self.inputData[Config.versionId] = value

    def add_description(self, value):
        self.inputData[Config.description] = value

    def add_setDrescription(self, value):
        self.inputData[Config.setDescription] = value

    def add_node(self, value):
        self.inputData[Config.node] = value

    def add_workspace(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.workspace, value)

    def add_cache(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.cache, value)

    def add_cache_tech(self, publishedFile='', heroFile=''): 
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.cacheTech, value)

    def add_fbx(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.fbx, value)

    def add_rsproxy(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.rsproxy, value)

    def add_maya_rsproxy(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.maRsproxy, value)

    def add_imageplane(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.imageplane, value)

    def add_rig(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.rig, value)

    def add_maya(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.maya, value)

    def add_setoverride(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.setOverride, value)

    def add_setshot(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.setShot, value)

    def add_shotdress(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.shotDress, value)

    def add_instdress(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.instDress, value)

    def add_animcurve(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.animCurve, value)

    def add_output_sequences(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.imagePath, value)

    def add_mov(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.mov, value)

    def add_key(self, key, value):
        self.inputData[key] = value

    def add_files(self, publishedFile, heroFile):
        data = OrderedDict()
        data['publishedFile'] = publishedFile
        data['heroFile'] = heroFile
        return data

    def get_task(self):
        return self.get(Config.task)

    def get_step(self):
        return self.get(Config.step)

    def get_type(self): 
        return self.get(Config.type)

    def get_preview(self):
        return self.get(Config.preview)

    def get_version(self):
        return self.get(Config.version)

    def get_version_id(self):
        return self.get(Config.id)

    def get_description(self):
        return self.get(Config.description) or ''

    def get_setDescription(self):
        return self.get(Config.setDescription) or ''

    def get_node(self):
        return self.get(Config.node) or ''

    def get_process(self):
        return self.get(Config.process)

    def get_workspace(self):
        return self.get(Config.workspace)

    def get_rig(self):
        return self.get(Config.rig)

    def get_cache(self):
        return self.get(Config.cache)

    def get_rsproxy(self):
        return self.get(Config.rsproxy)

    def get_imageplane(self):
        return self.get(Config.imageplane)

    def get_setoverride(self):
        return self.get(Config.setOverride)

    def get_setshot(self):
        return self.get(Config.setShot)

    def get_shotdress(self):
        return self.get(Config.shotDress)

    def get_animcurve(self):
        return self.get(Config.animcurve)

    def get_output_sequences(self):
        return self.get(Config.imagePath)

    def get_mov(self):
        return self.get(Config.mov)

    def keys(self):
        return self.inputData.keys()

    def get(self, key):
        return self.inputData[key] if key in self.inputData.keys() else OrderedDict()


class Member(object):
    """docstring for Member"""
    def __init__(self, name='', data=None):
        super(Member, self).__init__()
        if not data:
            self.data = OrderedDict()
            self.name = name
            self.data[self.name] = OrderedDict()
        else:
            self.data = self.init_data(data)

    def init_data(self, inputData):
        data = OrderedDict()
        self.name = inputData.keys()[0]
        data[self.name] = inputData[self.name]
        return

    def set_member(self, member):
        """ set dict member """
        self.set_object(member)
        data = member.data if type(member) == type(Member()) else member
        self.data[self.name][member.name] = member.data[member.name]

    def set_add(self):
        """ set to list mode """
        self.data[self.name] = []

    def add_member(self, member):
        """ append for list mode """
        self.set_object(member)
        appendData = member.data if type(member) == type(Member()) or type(member) == type(Output()) else member
        self.data[self.name].append(appendData)

    def set_object(self, member):
        self.child = member

    def get_data(self):
        return self.data[self.name]

    def get(self, key):
        return self.get_data().get(key)

    def member_keys(self):
        return self.get_data().keys()

def sorting_data(dictData, orders):
    sortDict = OrderedDict()
    if type(dictData) == type(OrderedDict()):
        for order in orders:
            if order in dictData.keys():
                sortDict[order] = dictData[order]

        for key in dictData.keys():
            if not key in sortDict.keys():
                sortDict[key] = dictData[key]
    else:
        return dictData
    return sortDict


def combine_dict(newDict, currentDict):
    if type(currentDict) == type(OrderedDict()) and type(newDict) == type(OrderedDict()):
        for k, v in newDict.iteritems():
            if not k in currentDict.keys():
                currentDict[k] = v

            else:
                currentValue = currentDict[k]
                newValue = v
                currentDict[k] = combine_dict(newValue, currentValue)
    else:
        return newDict
    return currentDict


def register_asset(scene, namespace, step, taskName, regType, regVersion, cachePaths=[], animCurvePaths=[], workspacePaths=[]):
    reg = Register(scene)

    title = Title()
    title.set_title(scene)
    element = Element()
    output = Output()
    output.add_task(taskName)
    output.add_type(regType)
    output.add_version(regVersion)
    output.add_cache(cachePaths[0], cachePaths[1])
    output.add_animcurve(animCurvePaths[0], animCurvePaths[1])
    output.add_workspace(workspacePaths[0], workspacePaths[1])
    title.set_output('assets', namespace, step, output)
    reg.set(title)
    reg.write()

def example():
    from rf_utils.context import context_info
    scene = context_info.ContextPathInfo()

    reg = Register(scene)

    title = Title()
    title.set_title(scene)
    element = Element()
    output = Output()

    # output.add_step('layout')
    output.add_task('layout')
    output.add_version('v001')
    title.set_output('assets', 'charA', 'layout', output)

    reg.set(title)
    reg.write()

    # media
    title = Title()
    step = Step()
    output = Output()

    output.add_task('animatic')
    output.add_version('v002')
    step.set_output('animatic', output)
    title.set_media(step)

    reg.set(title)

def ex2():
    from rf_utils.context import context_info
    scene = context_info.ContextPathInfo()
    reg = Register(scene)
    

# from rf_utils.context import context_info
# scene = context_info.ContextPathInfo()
# from rf_utils import register_shot
# reload(register_shot)

# example 1
# reg = register_shot.Register(scene)
# title = register_shot.Title()
# element = register_shot.Element()
# output = register_shot.Output()

# output.add_step('layout')
# output.add_task('layout')
# output.add_version('v001')

# element.set_output('charB', 'layout', output)
# title.set_asset(element)
# reg.set(title)
# reg.write()

# example 2
# reg = register_shot.Register(scene)
# title = register_shot.Title()
# output = register_shot.Output()

# output.add_step('layout')
# output.add_task('layout')
# output.add_version('v001')

# title.set_title(scene)
# title.set_output('assets', 'charA', 'layout', output)
# reg.set(title)
# reg.write()


# # media example
# from rf_utils import register_shot
# from rf_utils.context import context_info
# scene = context_info.ContextPathInfo()
# reload(register_shot)
# reg = register_shot.Register(scene)
# scene.context.update(publishVersion='v002')
# reg.set_media(scene, workspace=['path', 'path'], media=[])

# reg.register()

# example  add output to existing data 

# reg = register_shot.Register(scene)

# # add to asset output
# output = register_shot.Output()
# output.add_rsproxy('A', 'B')
# reg.set_asset_output('arthur_001', 'anim', output)

# # add to media output
# output.add_imageplane('A', 'B')
# reg.set_media_output('fx', output)
# reg.register()

# get backward versions
# reg = register_shot.Register(entity)
# files = reg.version.cache('badger_001', step='anim')

# reg.version.custom_key('badger_001', step='anim', dataKey='cache')
