import sys
import os

if os.environ.get('RFSCRIPT', False):
    path = '%s/core' % os.environ.get('RFSCRIPT')
    if not path in sys.path:
        sys.path.append(path)

import rf_config as config
from shotgun_api3 import Shotgun

# connection to server
server = config.Shotgun.server
script = config.Shotgun.script
id = config.Shotgun.id
sg = Shotgun(server, script, id)
# sg = None


def get_tasks(project):
    project_filtered = {
        "type": "Project",
        "id": project['id'],        
        "name": project['name']
    }
    tasks_filter = [
        ['project', 'is', project_filtered],
        ['sg_type', 'is', ""]
    ]
    fields = ['entity', 'sg_type']
    return sg.find('Task', tasks_filter, fields)


def update_type_tasks(task):
    print "update task %s", task
    payload = {
        'sg_type': task['entity']['type']
    }

    if payload['sg_type'] in ['Asset', 'Shot']:
        sg.update('Task', task['id'], payload)


def update_type_tasks_project_name():
    project = {
        'id': 134,
        'name': 'projectName'
    }
    tasks = get_tasks(project)
    print "tasks projectName lengt %s", len(tasks)
    for task in tasks:
        update_type_tasks(task)
    print "tasks projectName lengt %s", len(tasks)


def update_type_tasks_cloud_maker():
    project = {
        'id': 203,
        'name': 'CloudMaker'
    }
    tasks = get_tasks(project)
    print "tasks CloudMaker lengt %s", len(tasks)
    for task in tasks:
        update_type_tasks(task)
    print "tasks CloudMaker lengt %s", len(tasks)


def update_type_tasks_seven_chicks():
    project = {
        'id': 204,
        'name': 'SevenChickMovie'
    }
    tasks = get_tasks(project)
    print "tasks SevenChickMovie lengt %s", len(tasks)
    for task in tasks:
        update_type_tasks(task)
    print "tasks SevenChickMovie lengt %s", len(tasks)


def process_update():
    update_type_tasks_project_name()
    update_type_tasks_cloud_maker()
    update_type_tasks_seven_chicks()


process_update()
