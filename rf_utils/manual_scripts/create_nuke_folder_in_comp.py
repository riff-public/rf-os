import os


def process_projectName():
    episodes = ["ep01", "ep02", "ep03", "ep04"]
    base_path = "P:/projectName/scene/work"
    for episode in episodes:
        shot_path = "{base_path}/{ep}".format(base_path=base_path, ep=episode)
        if os.path.exists(shot_path):
            list_shots = list(filter(lambda shot_dir: len(shot_dir.split("_")) == 4, os.listdir(shot_path)))
            for shot in list_shots:
                nuke_app_path = "{shot_path}/{seq_shot}/comp/main/nuke".format(shot_path=shot_path, seq_shot=shot)
                print "{shot_path}:{nuke_app_path} ".format(shot_path=shot, nuke_app_path=os.path.exists(nuke_app_path))
                if not os.path.exists(nuke_app_path):
                    os.makedirs(nuke_app_path)


def process_sevenchicks():
    episodes = ["act1", "act2", "act3", "dev"]
    base_path = "P:/SevenChickMovie/scene/work"
    for episode in episodes:
        shot_path = "{base_path}/{ep}".format(base_path=base_path, ep=episode)
        if os.path.exists(shot_path):
            list_shots = list(filter(lambda shot_dir: len(shot_dir.split("_")) == 4, os.listdir(shot_path)))
            for shot in list_shots:
                nuke_app_path = "{shot_path}/{seq_shot}/comp/main/nuke".format(shot_path=shot_path, seq_shot=shot)
                print "{shot_path}:{nuke_app_path} ".format(shot_path=shot, nuke_app_path=os.path.exists(nuke_app_path))
                if not os.path.exists(nuke_app_path):
                    os.makedirs(nuke_app_path)


def run():
    # process_projectName()
    process_sevenchicks()


run()
