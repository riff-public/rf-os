import os
import sys
import csv
from rf_utils.sg import sg_process
sg = sg_process.sg
import urllib

def read_csv(path):
	with open(path, 'rb') as f:
	    reader = csv.reader(f)
	    listData = list(reader)
	return listData

def run():
	path = 'C:/Users/chanon.v/Downloads/Pucca2.csv'
	assetList = get_asset_dict(path)
	assetName = list(set([a['assetName'] for a in assetList]))

	types = list(set([a['assetType'] for a in assetList]))
	# eps = list(set([a['episode'] for a in assetList]))
	upload(assetList)
	# download_image(assetList)
	# update(assetList, 'Matte Painting', 'mattepaint')

def download_image(assetList):
	dst = 'D:/test/pucca'
	if not os.path.exists(dst):
		os.makedirs(dst)

	for i, asset in enumerate(assetList):
		assetName = asset['assetName']
		dstfile = '%s/%s.png' % (dst, assetName)
		url = asset['thumbnail']
		try:
			if not os.path.exists(dstfile):
				urllib.urlretrieve(url, dstfile)
				print 'download %s/%s' % (i, len(assetList))
		except IOError:
			print 'skip %s' % assetName


def upload(assetList):
	dst = 'D:/test/pucca'

	for i, asset in enumerate(assetList):
		assetName = asset['assetName']
		dstfile = '%s/%s.png' % (dst, assetName)
		if os.path.exists(dstfile):
			entity = sg_process.get_one_asset('pucca', assetName)
			sg_process.update_entity_thumbnail('Asset', entity['id'], dstfile)
			print 'upload %s' % dstfile



def update(assetList, type='', mapType=''):
	project = 'pucca'
	cacheEp = dict()
	projectEntity = {'type': 'Project', 'id': 205, 'name': 'pucca'}
	done = []
	for i, asset in enumerate(assetList):
		if asset['assetType'] == type:
			assetName = asset['assetName']
			assetType = mapType
			description = asset['description']
			episode = asset['episode']

			if not assetName in done:
				# find episode
				if episode in cacheEp.keys():
					episodeEntity = cacheEp[episode]
				else:
					episodeEntity = sg_process.get_one_episode(project, episode)
					if not episodeEntity:
						episodeEntity = sg_process.create_episode(projectEntity, episode)
					cacheEp[episode] = episodeEntity

				create_asset(projectEntity, assetType, assetName, description, episodeEntity)
				done.append(assetName)
				print '%s/%s' % (i, len(assetList))


def create_asset(project, assetType, assetName, description, episode):
	data = {'project': project,
			'sg_asset_type': assetType,
			'code': assetName,
			'description': description,
			'sg_scenes': [episode]
			}
	return sg_process.sg.create('Asset', data)


def get_asset_dict(path):
	listData = read_csv(path)
	newList = []

	for row in listData[2:]:
		dictData = asset_dict(row)
		newList.append(dictData)

	return newList

def asset_dict(data):
	columnList = ['id', 'thumbnail', 'assetType', 'assetName', 'description', 'episode']
	dictData = dict()
	for i, column in enumerate(data[0:6]):
		dictData.update({columnList[i]: column})
	return dictData
