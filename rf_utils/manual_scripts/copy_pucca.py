import os
import glob
import time
import re
import shutil
import logging

from rf_utils import file_utils

# from rf_utils import log_utils

# ------------------------------------------
# GLOBAL VARS
HOME_DIR = '%s/%s' %(os.environ['HOME'], 'copy_pucca')
LOG_LEVEL = logging.INFO

srcPath = 'Z:\\NPC\\post'
desPath = 'Z:\\cj\\out\\Z\\NPC\\post'

# ------------------------------------------
# copy lighting files
def copy_light():
    # ------------------------------------------
    # setup logging
    timestr = time.strftime("%Y%m%d-%H%M%S")
    log_file = 'copy_light.%s.log' %(timestr)
    log_path = '%s/%s' %(HOME_DIR, log_file)
    if not os.path.exists(HOME_DIR):
        os.makedirs(HOME_DIR)
    fh1 = logging.FileHandler(log_path)
    fh1.setLevel(LOG_LEVEL)
    logger = logging.getLogger('copy_pucca')
    logger.addHandler(fh1)

    lightSrcPath = '%s\\06_lighting' %(srcPath)

    epFolders = glob.glob('%s\\ep[0-9][0-9]'%lightSrcPath)
    latest_files = []
    nums = re.compile(r"[+-]?v\d+(?:\.\d+)?")
    for epPath in epFolders:
        ep = epPath.split('\\')[-1]
        shotPaths = glob.glob('%s\\%s_[0-9][0-9][0-9][0-9][a-z]' %(epPath, ep))
        for shotPath in shotPaths:
            shot = shotPath.split('\\')[-1]
            verPath = os.path.join(shotPath, 'ver')
            if not os.path.exists(verPath):
                logger.warning('Ver path does not exists: %s' %verPath)
                continue
            print 1
            # find latest file
            files = glob.glob('%s\\%s_light_v[0-9][0-9].ma' %(verPath, shot))
            if files:
                latest_path = files[-1]
                latest_file = os.path.basename(latest_path)
            else:
                founds = [os.path.join(verPath, f) for f in os.listdir(verPath) if f.endswith('.ma')]
                newest_file = max(founds, key=os.path.getctime)

                match = re.match('.*_v[0-9]+.ma$', newest_file)
                if match:
                    latest_file = match.group()
                    latest_path = os.path.join(verPath, latest_file)
                else:
                    logger.warning('No file matching the pattern: %s' %(verPath))
                    continue

            # find version, renew file name
            version = int(re.search(r"[+-]?_v\d+(?:\.\d+)?", latest_file).group(0)[2:])
            new_file_name = '%s_light_v%s.ma' %(shot, str(version).zfill(2))
            desVerPath = verPath.replace(srcPath, desPath)
            des_file_path =os.path.join(desVerPath, new_file_name)

            # create folder in destination directory
            des_fol = os.path.dirname(des_file_path)
            if not os.path.exists(des_fol):
                # pass
                os.makedirs(des_fol)

            # print latest_file, des_file_path
            print latest_path, des_file_path
            shutil.copy(latest_path, des_file_path)


# copy lighting files
def copy_nuke_comp():
    # ------------------------------------------
    # setup logging
    timestr = time.strftime("%Y%m%d-%H%M%S")
    log_file = 'copy_nuke_comp.%s.log' %(timestr)
    log_path = '%s/%s' %(HOME_DIR, log_file)
    if not os.path.exists(HOME_DIR):
        os.makedirs(HOME_DIR)
    fh1 = logging.FileHandler(log_path)
    fh1.setLevel(LOG_LEVEL)
    logger = logging.getLogger('copy_pucca')
    logger.addHandler(fh1)

    lightSrcPath = '%s\\02_compositing' %(srcPath)

    epFolders = glob.glob('%s\\ep[0-9][0-9]'%lightSrcPath)
    latest_files = []
    nums = re.compile(r"[+-]?v\d+(?:\.\d+)?")
    for epPath in epFolders:
        ep = epPath.split('\\')[-1]
        shotPaths = glob.glob('%s\\%s_[0-9][0-9][0-9][0-9][a-z]' %(epPath, ep))
        for shotPath in shotPaths:
            shot = shotPath.split('\\')[-1]
            verPath = os.path.join(shotPath, 'ver')
            if not os.path.exists(verPath):
                logger.warning('Ver path does not exists: %s' %verPath)
                continue

            # find latest file
            files = glob.glob('%s\\%s_comp_v[0-9][0-9].nk' %(verPath, shot))
            print files
            if files:
                latest_path = files[-1]
                latest_file = os.path.basename(latest_path)
            else:
                founds = [os.path.join(verPath, f) for f in os.listdir(verPath) if f.endswith('.nk')]
                print founds,'sss'
                if founds:
                    newest_file = max(founds, key=os.path.getctime)
                else:
                    print 123
                    newest_file = ''

                match = re.match('.*_v[0-9]+.nk$', newest_file)
                print match,'match'
                if match:
                    latest_file = match.group()
                    latest_path = os.path.join(verPath, latest_file)
                else:
                    logger.warning('No file matching the pattern: %s' %(verPath))
                    continue

            # find version, renew file name
            version = int(re.search(r"[+-]?_v\d+(?:\.\d+)?", latest_file).group(0)[2:])
            new_file_name = '%s_comp_v%s.nk' %(shot, str(version).zfill(2))
            desVerPath = verPath.replace(srcPath, desPath)
            des_file_path =os.path.join(desVerPath, new_file_name)

            # create folder in destination directory
            des_fol = os.path.dirname(des_file_path)
            print des_fol,'test'
            if not os.path.exists(des_fol):
                # pass
                os.makedirs(des_fol)

            # print latest_file, des_file_path
            print latest_path, des_file_path
            # shutil.copy(latest_path, des_file_path)

def copy_ae_comp():
    # ------------------------------------------
    # setup logging
    timestr = time.strftime("%Y%m%d-%H%M%S")
    log_file = 'copy_ae_comp.%s.log' %(timestr)
    log_path = '%s/%s' %(HOME_DIR, log_file)
    if not os.path.exists(HOME_DIR):
        os.makedirs(HOME_DIR)
    fh1 = logging.FileHandler(log_path)
    fh1.setLevel(LOG_LEVEL)
    logger = logging.getLogger('copy_pucca')
    logger.addHandler(fh1)

    lightSrcPath = '%s\\04_fx' %(srcPath)

    epFolders = glob.glob('%s\\ep[0-9][0-9]'%lightSrcPath)
    latest_files = []
    nums = re.compile(r"[+-]?v\d+(?:\.\d+)?")
    for epPath in epFolders:
        ep = epPath.split('\\')[-1]
        shotPaths = glob.glob('%s\\%s_[0-9][0-9][0-9][0-9][a-z]' %(epPath, ep))
        for shotPath in shotPaths:
            shot = shotPath.split('\\')[-1]
            verPath = os.path.join(shotPath, 'ver')
            if not os.path.exists(verPath):
                logger.warning('Ver path does not exists: %s' %verPath)
                continue

            # find latest file
            ae_file = file_utils.get_lastest_file(verPath, ext="aep")
            if ae_file:
                new_file_name = os.path.basename(ae_file)
                desVerPath = verPath.replace(srcPath, desPath)
                des_file_path =os.path.join(desVerPath, new_file_name)

                # create folder in destination directory
                des_fol = os.path.dirname(des_file_path)
                if not os.path.exists(des_fol):
                    # pass
                    os.makedirs(des_fol)

                # print ae_file, des_file_path
                if os.path.exists(ae_file):
                    print ae_file, des_file_path
                    shutil.copy(ae_file, des_file_path)
                else:
                    logger.warning('Ver path does not exists: %s' %ae_file)

def copy_anim():
    # ------------------------------------------
    # setup logging
    anim_desPath = 'Z:\\cj\\out\\Y\\NPC\\prod'
    timestr = time.strftime("%Y%m%d-%H%M%S")
    log_file = 'copy_anim.%s.log' %(timestr)
    log_path = '%s/%s' %(HOME_DIR, log_file)
    if not os.path.exists(HOME_DIR):
        os.makedirs(HOME_DIR)
    fh1 = logging.FileHandler(log_path)
    fh1.setLevel(LOG_LEVEL)
    logger = logging.getLogger('copy_pucca')
    logger.addHandler(fh1)

    anim_srcPath = 'P:/pucca'
    animOutputPath = '%s\\02_animation' %(anim_desPath)

    epFolders = glob.glob('%s\\EP[0-9][0-9]'%anim_srcPath)
    latest_files = []
    nums = re.compile(r"[+-]?v\d+(?:\.\d+)?")
    for epPath in epFolders:
        ep = epPath.split('\\')[-1]
        anim_path = os.path.join(epPath, 'scene/anim')
        shotPaths = glob.glob('%s\\S[0-9][0-9][0-9][0-9][a-z]' %(anim_path))
        for shotPath in shotPaths:
            shot = shotPath.split('\\')[-1]
            verPath = os.path.join(shotPath, 'version')
            if not os.path.exists(verPath):
                logger.warning('Ver path does not exists: %s' %verPath)
                continue

            # find latest file
            print verPath
            ma_file = file_utils.get_lastest_file(verPath, ext="ma")
            if ma_file:
                new_file_name = os.path.basename(ma_file)
                # desVerPath = verPath.replace(shotPath, anim_desPath)
                ep_shot = '%s_%s'%(ep.lower(),shot.lower())
                desVerPath = '{animOutputPath}\\{ep}\\{ep_shot}\\ver'.format(animOutputPath=animOutputPath, ep= ep.lower(), ep_shot= ep_shot)
                des_file_path =os.path.join(desVerPath, new_file_name)

                # create folder in destination directory
                des_fol = os.path.dirname(des_file_path)
                if not os.path.exists(des_fol):
                    # pass
                    os.makedirs(des_fol)

                # print ma_file, des_file_path
                if os.path.exists(ma_file):
                    print ma_file, des_file_path
                    shutil.copy(ma_file, des_file_path)
                
                else:
                    logger.warning('Ver path does not exists: %s' %ma_file)
            else:
                logger.warning('Ver path does not exists: %s' %ma_file)
