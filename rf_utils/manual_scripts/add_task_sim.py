import sys
import os

if os.environ.get('RFSCRIPT', False):
    path = '%s/core' % os.environ.get('RFSCRIPT')
    if not path in sys.path:
        sys.path.append(path)

import rf_config as config
from shotgun_api3 import Shotgun

# connection to server
server = config.Shotgun.server
script = config.Shotgun.script
id = config.Shotgun.id
sg = Shotgun(server, script, id)
# sg = None


PROJECT_ENTITY = {
    "type": "Project",
    "id": 204,
    "name": "SevenChickMovie"
}
TASKS_SIM = ["clothDyn", "clothWrap", "hairDyn"]


def get_asset_char():
    assets_filter = [
        ['project', 'is', PROJECT_ENTITY],
        ['sg_asset_type', 'is', "char"]
    ]
    fields = ['code', 'sg_asset_type']
    return sg.find('Asset', assets_filter, fields)


def add_sim_task_asset_char(project, assets_char):
    for asset in assets_char:
        create_task_sim(asset)
    # return sg.update('Asset', assets_char['id'], fields)


def create_task_sim(assets_char):
    for entity in assets_char:
        for content in TASKS_SIM:
            stepEntity = sg.find_one('Step', [['code', 'is', "Sim"], ["entity_type", "is", "Asset"]], ["entity_type"])
            data = {
                'project': PROJECT_ENTITY,
                'entity': entity,
                'step': stepEntity,
                'content': content,
                'sg_type': "Asset"
            }
            taskEntity = sg.create('Task', data)
            print taskEntity


def remove_all_task_sim():
    stepEntity = sg.find_one('Step', [['code', 'is', "Sim"], ["entity_type", "is", "Asset"]], ["entity_type"])
    tasks = sg.find('Task', [['step', 'is', stepEntity], ["project", "is", PROJECT_ENTITY]], ["content"])
    print "tasks length : ", len(tasks)
    for task in tasks:
        if task["content"] == "clothDyn" or task["content"] == "clothWrap" or task["content"] == "hairDyn":
            result = sg.delete("Task", task['id'])
            print "result : ", result
    # data = {
    #     'project': PROJECT_ENTITY,
    #     'entity': entity,
    #     'step': stepEntity,
    #     'content': "aaaa",
    #     'sg_type': "Asset"
    # }
    # taskEntity = sg.create('Task', data)
    # return taskEntity


def processor():
    assets_char = get_asset_char()
    # print "assets_char: ", assets_char
    # print "assets_char length: ", len(assets_char)
    # add_sim_task_asset_char(project, assets_char)
    # a = {'code': 'ArthurBase', 'type': 'Asset', 'id': 3209, 'sg_asset_type': 'char'}
    remove_all_task_sim()
    create_task_sim(assets_char)


processor()


def update_type_tasks(task):
    print "update task %s", task
    payload = {
        'sg_type': task['entity']['type']
    }

    if payload['sg_type'] in ['Asset', 'Shot']:
        sg.update('Task', task['id'], payload)
