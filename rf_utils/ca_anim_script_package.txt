core/icons

*core/rf_template

*core/rf_env/default/maya/2020
*core/rf_env/CA/maya/2020

core/rf_config/yaml
core/rf_config/env_config.yml
core/rf_config/maya_config.yml
core/rf_config/sg_config.yml
core/rf_config/software_config.yml
core/rf_config/outsource_config.yml
core/rf_config/project/default
core/rf_config/project/CA

core/rf_app/publish/asset/sg_hook.py
core/rf_app/publish/scene
core/rf_app/asm
core/rf_app/export
core/rf_app/save_plus
core/rf_app/anim
core/rf_app/builder
*core/rf_app/timer

*core/rf_maya/contextMenu
core/rf_maya/environment
core/rf_maya/shelfs
core/rf_maya/rfmenu
core/rf_maya/startup
core/rf_maya/rftool/scene/playblast
core/rf_maya/rftool/movie_image
core/rf_maya/rftool/misc
core/rf_maya/rftool/utils
core/rf_maya/rftool/shade
core/rf_maya/rftool/anim
core/rf_maya/rftool/control
core/rf_maya/rftool/rig/ncmel
core/rf_maya/lib
core/rf_maya/nuTools
core/rf_maya/mel/2018/dagMenuProc.mel
core/rf_maya/studiolibrary-2.4.15

core/rf_utils

core/rf_qc/config
core/rf_qc/app.py
core/rf_qc/qc.py
core/rf_qc/qc_widget.py
core/rf_qc/ui.ui
core/rf_qc/icons
core/rf_qc/maya_lib/scene

core/rf_launcher
*core/rf_launcher/launcher_cmd.py
*core/rf_launcher/preload.py

*core/rf_launcher_apps/Software/Maya CA

*core/rf_lib/ffmpeg-N-99484
*core/rf_lib/mayapy_packages
*core/rf_lib/python/2.7.11
*core/rf_lib/Qt
*core/rf_lib/Qt.py
*core/rf_lib/PIL_Maya