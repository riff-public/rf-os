import os
import sys
import logging
logger = logging.getLogger(__name__)

from rf_utils.context import context_info
from rf_utils import file_utils

class Config:
	geo = {'step': 'model', 'outputKey': 'cache'}
	geoMaya = {'step': 'model', 'outputKey': 'cache'}
	gpu = {'step': 'model', 'outputKey': 'gpu'}
	mtrPreview = {'step': 'model', 'outputKey': 'materialRig'}
	mtrRig = {'step': 'texture', 'outputKey': 'materialRig'}
	mtrTexture = {'step': 'texture', 'outputKey': 'material'}
	mtrLookdev = {'step': 'lookdev', 'outputKey': 'material'}
	mtrGroom = {'step': 'groom', 'outputKey': 'material'}
	groom = {'step': 'groom', 'outputKey': 'yeti'}
	rig = {'step': 'rig', 'outputKey': 'rig'}



class Output(object):
	"""docstring for Output"""
	def __init__(self, entity):
		super(Output, self).__init__()
		self.entity = entity

	@property
	def publish(self):
		return Publish(self)

	@property
	def hero(self):
		return Hero(self)


class Publish(object):
	"""docstring for Hero"""
	def __init__(self, main):
		super(Publish, self).__init__()
		self.main = main
		self.entity = self.main.entity
		self.libHero = False

	def geo(self, res='md', hero=True):
		config = Config.geo
		return self._path(config, process='main', res=res, hero=hero)

	def gpu(self, res='md', hero=True):
		config = Config.gpu
		return self._path(config, process='main', res=res, hero=hero)

	def rig(self, res='md', hero=True):
		config = Config.rig
		return self._path(config, process='main', res=res, hero=hero)

	def groom(self, res='md', look='main', hero=True):
		config = Config.groom
		return self._path(config, res=res, look=look, hero=hero)

	def mtr_preview(self, res='md', look='main', hero=True):
		config = Config.mtrPreview
		return self._path(config, res=res, look=look, hero=hero)

	def mtr_rig(self, res='md', look='main', hero=True):
		config = Config.mtrRig
		return self._path(config, res=res, look=look, hero=hero)

	def mtr_texture(self, res='md', look='main', hero=True):
		config = Config.mtrTexture
		return self._path(config, res=res, look=look, hero=hero)

	def mtr_groom(self, res='md', look='main', hero=True):
		config = Config.mtrGroom
		return self._path(config, res=res, look=look, hero=hero)

	def mtr_lookdev(self, res='md', look='main', hero=True):
		config = Config.mtrLookdev
		return self._path(config, res=res, look=look, hero=hero)

	def _path(self, config, process='main', res='md', look='main', hero=True):
		return _output_path(self.entity, step=config['step'], process=process, res=res, look=look, outputKey=config['outputKey'], hero=hero, libHero=self.libHero)


class Hero(Publish):
	"""docstring for Hero"""
	def __init__(self, main):
		super(Hero, self).__init__(main)
		self.libHero = True


def latest_version(entity):
	versionPath = os.path.split(entity.path.output().abs_path())[0]
	if os.path.exists(versionPath):
		dirs = file_utils.list_folder(versionPath)
		versions = [a for a in dirs if a[0] == 'v' and a[1:].isdigit()]
		return sorted(versions)[-1] if versions else None


def _output_path(entity, step, process, res, look, outputKey, hero, libHero=False):
	entity.context.update(step=step, process=process, res=res, look=look)
	if not hero:
		outputDir = ''
		version = latest_version(entity)
		if version:
			entity.context.update(publishVersion=version)
			outputDir = entity.path.publish_output().abs_path()
	else:
		outputDir = entity.path.hero().abs_path() if libHero else entity.path.output_hero().abs_path()

	fileName = entity.output_name(outputKey=outputKey, hero=hero)
	outputFile = '%s/%s' % (outputDir, fileName)

	return outputFile if os.path.exists(outputFile) else ''




# usages
# output.publish.gpu(hero=True)
# Result: u'P:/projectName/asset/publ/char/canisTest/model/main/hero/output/canisTest_mdl_gpu_md.hero.abc' #
# output.publish.gpu(hero=True)
# Result: u'P:/projectName/asset/publ/char/canisTest/model/main/hero/output/canisTest_mdl_gpu_md.hero.abc' #
# output.hero.gpu()
# Result: u'P:/projectName/asset/publ/char/canisTest/hero/canisTest_mdl_gpu_md.hero.abc' #


# output.publish.geo()
# P:/SevenChickMovie/asset/publ/char/canis/model/main/hero/output/canis_mdl_main_md.hero.abc

# output.publish.gpu()
# P:/SevenChickMovie/asset/publ/char/canis/model/main/hero/output/canis_mdl_gpu_md.hero.abc

# output.publish.rig()
# P:/SevenChickMovie/asset/publ/char/canis/rig/main/hero/output/canis_rig_main_md.hero.ma

# output.publish.groom()
# P:/SevenChickMovie/asset/publ/char/canis/groom/main/hero/output/canis_yeti_main_md.hero.ma

# output.publish.mtr_preview()
# P:/SevenChickMovie/asset/publ/char/canis/texture/main/hero/output/canis_mtr_rig_main_md.hero.ma

# output.publish.mtr_texture()
# P:/SevenChickMovie/asset/publ/char/canis/texture/main/hero/output/canis_mtr_mdl_main_md.hero.ma
# output.publish.mtr_lookdev()
# P:/SevenChickMovie/asset/publ/char/canis/lookdev/main/hero/output/canis_mtr_ldv_main_md.hero.ma



# output.hero.gpu()
# Result: u'P:/SevenChickMovie/asset/publ/char/canis/hero/canis_mdl_gpu_md.hero.abc' #
# output.hero.rig()
# Result: u'P:/SevenChickMovie/asset/publ/char/canis/hero/canis_rig_main_md.hero.ma' #
# output.hero.groom()
# Result: '' #
# output.hero.mtr_preview()
# Result: u'P:/SevenChickMovie/asset/publ/char/canis/hero/canis_mtr_rig_main_md.hero.ma' #
# output.hero.mtr_model()
# Result: '' #
# output.hero.mtr_texture()
# Result: u'P:/SevenChickMovie/asset/publ/char/canis/hero/canis_mtr_mdl_main_md.hero.ma' #
# output.hero.mtr_lookdev()
# Result: u'P:/SevenChickMovie/asset/publ/char/canis/hero/canis_mtr_ldv_main_md.hero.ma' #
