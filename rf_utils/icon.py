import sys
import os

moduleDir = sys.modules[__name__].__file__
iconPath = '%s/core' % os.environ['RFSCRIPT']

ok = '%s/icons/%s' % (iconPath, 'OK_icon.png')
no = '%s/icons/%s' % (iconPath, 'X_icon.png')
dir = '%s/icons/%s' % (iconPath, 'dir_icon.png')
nodir = '%s/icons/%s' % (iconPath, 'nodir_icon.png')
maya = '%s/icons/%s' % (iconPath, 'maya_icon.png')
etc = '%s/icons/%s' % (iconPath, 'etc_icon.png')
camera = '%s/icons/%s' % (iconPath, 'camera_icon.png')
cameraNa = '%s/icons/%s' % (iconPath, 'cameraNa_icon.png')
noimg = '%s/icons/%s' % (iconPath, 'tmp_thumbnail.jpg')
nopreview = '%s/icons/%s' % (iconPath, 'nopreview_icon.png')
gear = '%s/icons/%s' % (iconPath, 'gear_icon.gif')
success = '%s/icons/%s' % (iconPath, 'success_icon.gif')
failed = '%s/icons/%s' % (iconPath, 'failed_icon.gif')
skip = '%s/icons/%s' % (iconPath, 'skip_icon.png')
task_manager = '%s/icons/%s' % (iconPath, 'taskManager_sm.png')
checkup = '%s/icons/%s' % (iconPath, 'checkup.png')
play_white = '%s/icons/%s' % (iconPath, 'play_icon_white.png')
publish_sm = '%s/icons/%s' % (iconPath, 'publish_sm.png')
user_eval = '%s/icons/%s' % (iconPath, 'user_eval.jpg')
evaluation_icon = '%s/icons/%s' % (iconPath, 'evaluation_icon.jpg')
document_icon = '%s/icons/%s' % (iconPath, 'document_icon.png')
folder_empty_icon = '%s/icons/%s' % (iconPath, 'folder_empty_icon.png')
folder_full_icon = '%s/icons/%s' % (iconPath, 'folder_full_icon.png')
folder_grey_icon = '%s/icons/%s' % (iconPath, 'folder_grey_icon.png')
file_icon = '%s/icons/%s' % (iconPath, 'file_icon.png')

# sg
sgWtg = '%s/icons/%s' % (iconPath, 'wtg_icon.png')
sgAprv = '%s/icons/%s' % (iconPath, 'aprv_icon.png')
sgPndgAprv = '%s/icons/%s' % (iconPath, 'p_aprv_icon.png')
sgPndgPub = '%s/icons/%s' % (iconPath, 'pub_icon.png')
sgFix = '%s/icons/%s' % (iconPath, 'fix_icon.png')
needFix = '%s/icons/%s' % (iconPath, 'needFix_icon.png')
needFix2 = '%s/icons/%s' % (iconPath, 'needFix2_icon.png')
sgHold = '%s/icons/%s' % (iconPath, 'hld_icon.png')
sgIp = '%s/icons/%s' % (iconPath, 'ip_icon.png')
sgLate = '%s/icons/%s' % (iconPath, 'late_icon.png')
sgRevise = '%s/icons/%s' % (iconPath, 'rev_icon.png')
sgRdy = '%s/icons/%s' % (iconPath, 'rdy_icon.png')
sgNa = '%s/icons/%s' % (iconPath, 'sgna_icon.png')
sgTag = '%s/icons/%s' % (iconPath, 'sg_tag_icon.png')
checkin = '%s/icons/%s' % (iconPath, 'checkin.png')
omt_icon = '%s/icons/%s' % (iconPath, 'omt_icon.png')
pendpubl_icon = '%s/icons/%s' % (iconPath, 'pendpubl_icon.png')
review_icon = '%s/icons/%s' % (iconPath, 'review_icon.png')

# general
logo = '%s/icons/%s' % (iconPath, 'riff_logo.png')
buttonCheck = '%s/icons/%s' % (iconPath, 'check_icon.png')
buttonFail = '%s/icons/%s' % (iconPath, 'fail_icon.png')

class Tool: 
    build16 = '%s/icons/16/%s' % (iconPath, 'build_icon.png')
    remove16 = '%s/icons/16/%s' % (iconPath, 'remove_icon.png')
    refresh16 = '%s/icons/16/%s' % (iconPath, 'refresh_icon.png')
    import16 = '%s/icons/16/%s' % (iconPath, 'import_icon.png')
    camera16 = '%s/icons/16/%s' % (iconPath, 'camera_icon.png')
    abc16 = '%s/icons/16/%s' % (iconPath, 'abc_icon.png')
    yeti16 = '%s/icons/16/%s' % (iconPath, 'yeti_icon.png')
    set16 = '%s/icons/16/%s' % (iconPath, 'set_icon.png')
    instdress16 = '%s/icons/16/%s' % (iconPath, 'instdress_icon.png')
    shotdress16 = '%s/icons/16/%s' % (iconPath, 'shotdress_icon.png')
    fx16 = '%s/icons/16/%s' % (iconPath, 'fx_icon.png')




ICONS_LIST = [
    {
        'display': 'Approved',
        'code': 'apr',
        'picture_name': 'aprv_icon.png',
        'icon_path': sgAprv
    },
    {
        'display': 'Fix',
        'code': 'fix',
        'picture_name': 'fix_icon.png',
        'icon_path': sgFix
    },
    {
        'display': 'On Hold',
        'code': 'hld',
        'picture_name': 'hld_icon.png',
        'icon_path': sgHold
    },
    {
        'display': 'In Progress',
        'code': 'ip',
        'picture_name': 'ip_icon.png',
        'icon_path': sgIp
    },
    {
        'display': 'Omit',
        'code': 'omt',
        'picture_name': 'omt_icon.png',
        'icon_path': omt_icon
    },
    {
        'display': 'Pending for Publish',
        'code': 'pub',
        'picture_name': 'pendpubl_icon.png',
        'icon_path': pendpubl_icon
    },
    {
        'display': 'Ready to Start',
        'code': 'rdy',
        'picture_name': 'rdy_icon.png',
        'icon_path': sgRdy
    },
    {
        'display': 'Pending Review',
        'code': 'rev',
        'picture_name': 'review_icon.png',
        'icon_path': review_icon
    },
    {
        'display': 'Waiting to Start',
        'code': 'wtg',
        'picture_name': 'wtg_icon.png',
        'icon_path': sgWtg
    }
]
