def truncate_str(text, limit_str):
    if len(text) > limit_str:
        text = text[:limit_str] + '...'
    return text


def forward_slash_to_backslash(text):
    return text.replace('/', '\\')


def back_slash_to_forwardslash(text):
    return text.replace('\\', '/')
