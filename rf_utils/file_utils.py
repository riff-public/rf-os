# read write file
import os
import sys
import traceback
import logging
import shutil
import time
from datetime import datetime
import glob
import json
import yaml
import re
from collections import OrderedDict
import subprocess
import hashlib
import filecmp
import tempfile

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

is_python2 = True if sys.version_info[0] < 3 else False
# from yaml import load, dump
# from yaml import Loader, Dumper
# import yaml


class ExportCheck():
    """check if file is exported"""
    def __init__(self, filepath):
        self.filepath = filepath 
        self.startTime = None 
        self.result = False 

    def __enter__(self): 
        if os.path.exists(self.filepath): 
            self.startTime = os.path.getmtime(self.filepath)

    def __exit__(self, *args): 
        if os.path.exists(self.filepath): 
            if self.startTime: 
                if not self.startTime == os.path.getmtime(self.filepath): 
                    self.result = True 
            else: 
                self.result = True 

def createFile(file) :
    if '\\' in file :
        file = file.replace('\\', '/')

    dir = file.replace(file.split('/')[-1], '')
    if not os.path.exists(dir) :
        os.makedirs(dir)

    f = open(file, 'w')
    f.close()
    return True

def writeFile(file, data) :
    f = open(file, 'w')
    f.write(data)
    f.close()
    return True

def appendData(file, data) :
    f = open(file, 'a')
    f.write(data)
    f.close()
    return True

def readFile(file) :
    file_open_kwargs = {} if is_python2 else {'encoding': 'utf-8'}
    f = open(file, 'r', **file_open_kwargs)
    data = f.read()
    return data

def eraseData(file) :
    f = open(file, 'w')
    f.close()
    return True

def copy(src, dst) :
    if os.path.exists(src) :
        dstDir = dst.replace(dst.split('/')[-1], '')
        if not os.path.exists(dstDir) :
            os.makedirs(dstDir)

        shutil.copy2(src, dst)

    if os.path.exists(dst):
        return dst


def xcopy_file(src, dst):
    # print "src: ", src
    # print "dst: ", dst
    src = forward_slash_to_backslash(src)
    dst = forward_slash_to_backslash(dst)
    cmd = "echo f | xcopy /y {src} {dst}".format(src=src, dst=dst)
    ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = ps.communicate()[0]
    print (output)
    # print "output", output
    if os.path.exists(dst):
        return dst
    else: 
        logger.debug('Failed to copy to "%s"' % dst)

def xcopy_directory(src, dst):
    src = forward_slash_to_backslash(src)
    dst = forward_slash_to_backslash(dst)
    cmd = "echo d | xcopy /y {src} {dst}".format(src=src, dst=dst)
    ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = ps.communicate()[0]
    # print output
    # return dst 
    if os.path.exists(dst):
        return dst

def remove(file):
    os.remove(file)

def removeTree(directory):
    shutil.rmtree(directory)

def emptyDir(directory, filters=[]):
    directory = directory.replace('\\', '/')
    files_and_folders = os.listdir(directory)
    for f in files_and_folders:
        delete = True
        if filters:
            for fil in filters:
                if re.search(fil, f):
                    delete = True
                    break
            else:
                delete = False

        if delete:
            fp = '{}/{}'.format(directory, f)
            if os.path.isfile(fp):
                os.remove(fp)
            else:
                shutil.rmtree(fp)

def remove_file(file, emptyDir=True): 
    # remove(file)
    file = file.replace('\\', '/')
    if os.path.exists(file): 
        remove(file)
    else: 
        logger.info('%s not exists, skip' % file)

    # emptyDir 
    dirElems = file.split('/')
    if emptyDir: 
        for i in range(len(dirElems)): 
            path = '/'.join(dirElems[0:-i-1])
            if os.path.exists(path): 
                if os.listdir(path): 
                    continue 
                else: 
                    os.rmdir(path)
                    print ('remove %s' % path)

def remove_empty_dir(path): 
    # emptyDir 
    dirElems = path.split('/')
    for i in range(len(dirElems)): 
        path = '/'.join(dirElems[0:-i]) if not i == 0 else '/'.join(dirElems)
        if os.path.exists(path): 
            if os.listdir(path): 
                continue 
            else: 
                os.rmdir(path)
                print ('remove %s' % path)

def copyTree(src, dst) :
    if os.path.exists(src) :
        shutil.copytree(src, dst, symlinks=False, ignore=None)
    return dst

def listFolder(path='',prefix='',pad=''):
    dirs = []
    try:
        if os.path.exists(path):
            # print path
            # path = path.replace('\\','/')
            # print os.path.join(path, d).replace('\\','/')
            for i in [d for d in os.listdir(path) if os.path.isdir(os.path.join(path,d))]:
                if len(prefix) and pad:
                    if i[:len(prefix)] == prefix:
                        if type(pad) == type(int()):
                            number = i[len(prefix):(len(prefix)+int(pad))]
                            if int(len(number)) == int(pad):
                                dirs.append(i)

                elif len(prefix):
                    if i[:len(prefix)] == prefix:
                        dirs.append(i)

                elif type(pad) == type(int()):
                    number = i[len(prefix):(len(prefix)+int(pad))]
                    if int(len(number)) == int(pad):
                        dirs.append(i)

                elif not len(prefix):
                    dirs.append(i)


        return sorted(dirs)
    except:
        traceback.print_exc()
        return dirs

def listFile(path,ex='',prefix='',pad=''):
    dirs = []
    # print path,ex,prefix,pad
    try:
        if os.path.exists(path):
            for i in [d for d in os.listdir(path) if os.path.isfile(os.path.join(path, d))]:
                # if len(ex):
                #     if not os.path.splitext(i)[-1][1:] == ex:
                #         continue

                if len(prefix) and pad:
                    if i[:len(prefix)] == prefix:
                        print (i[:len(prefix)], prefix)
                        if type(pad) == type(int()):
                            number = i[len(prefix):(len(prefix)+int(pad))]
                            if int(len(number)) == int(pad):
                                if len(ex):
                                    if i.split('.')[-1] == ex:
                                        dirs.append(i)
                                else:
                                    dirs.append(i)

                elif len(prefix):
                    if i[:len(prefix)] == prefix:
                        if len(ex):
                           if i.split('.')[-1] == ex:
                                dirs.append(i)
                        else:
                            dirs.append(i)

                elif type(pad) == type(int()):
                    number = i[len(prefix):(len(prefix)+int(pad))]
                    if int(len(number)) == int(pad):
                        if len(ex):
                           if i.split('.')[-1] == ex:
                                dirs.append(i)
                        else:
                            dirs.append(i)

                elif not len(prefix):
                    if len(ex):
                       if i.split('.')[-1] == ex:
                            dirs.append(i)
                    else:
                        dirs.append(i)

        return sorted(dirs)
    except:
        return dirs


def md5(fname):
    """ check md5 file """ 
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def is_file_same(file1, file2): 
    """ compare if files are the same """ 
    if os.path.exists(file1) and os.path.exists(file2): 
        if md5(file1) == md5(file2): 
            return True 
    else: 
        logger.warning('File does not exists')
    return False 


def rename(src, dst) :
    if os.path.exists(src) :
        return os.rename(src, dst)

    else :
        print ('File not Exists')

def increment_version(filename):
    if not os.path.exists(filename):
        return filename
    dirname = os.path.dirname(filename)
    basename = os.path.basename(filename)
    ver = find_version(basename)
    firstElem = basename.split('_%s_' % ver)[0]
    lastElem = basename.split('_%s_' % ver)[-1]
    print ('num', ver)
    num = int(ver[1:])+1
    verPad = 'v%03d' % num
    versionName = basename.replace(ver, verPad)
    return increment_version('%s/%s' % (dirname, versionName))

def find_next_version(files):
    vers = sorted([int(find_version(a).replace('v', '') if find_version(a) else 0) for a in files])

    if vers:
        newVer = vers[-1] + 1
        return 'v%03d' % newVer

    return 'v001'


def find_version(filename, prefix='v', padding=3):
    elems = filename.split('.')[0].split('_')
    for elem in elems:
        if elem[0] == prefix and elem[1:].isdigit():
            return elem

def find_next_image_path(img_path,chkname=''):
    # get image path with next 4 padding number
    img_dir = os.path.dirname(img_path)
    img_type = img_path.split('.')[-1]

    if not os.path.exists(img_dir):
        os.makedirs(img_dir)

    if os.path.exists(img_dir):
        imgs = listFile(img_dir)

    if imgs:
        index = len(imgs) + 1
    else:
        index = 1

    image_path = '%s.%04d.%s' %(img_path,index,img_type)

    return image_path

def get_modified_time(filePath):
    return time.mtime(os.path.getmtime(filePath))

def get_now_time():
    now = str(datetime.now())
    return now.split('.')[0]

def ymlDumper(file, dictData, flowStyle=False) :
    # input dictionary data
    data = ordered_dump(dictData, Dumper=yaml.SafeDumper, default_flow_style=flowStyle)
    result = writeFile(file, data)
    return result


def ymlLoader(file) :
    file_open_kwargs = {} if is_python2 else {'encoding': 'utf-8'}
    class Loader(yaml.Loader):
        """ add !include yaml constructor """
        def __init__(self, stream):
            self._root = os.path.split(stream.name)[0]
            super(Loader, self).__init__(stream)

        def include(self, node):
            filename = os.path.join(self._root, self.construct_scalar(node))

            with open(filename, 'r', **file_open_kwargs) as f:
                return ordered_load(f, Loader)

    Loader.add_constructor('!include', Loader.include)

    with open(file, 'r', **file_open_kwargs) as stream:
        dictData = ordered_load(stream, Loader)

        return dictData


def ordered_load(stream, Loader=yaml.Loader, object_pairs_hook=OrderedDict):
    class OrderedLoader(Loader):
        pass
    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))
    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)
    return yaml.load(stream, OrderedLoader)


def ordered_dump(data, stream=None, Dumper=yaml.Dumper, **kwds):
    class OrderedDumper(Dumper):
        pass
    def _dict_representer(dumper, data):
        return dumper.represent_mapping(
            yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
            data.items())
    OrderedDumper.add_representer(OrderedDict, _dict_representer)
    return yaml.dump(data, stream, OrderedDumper, **kwds)


def search_replace_file(srcFile, search, replace, removeLine = True, backupFile = True) :
    if backupFile :
        backupSuccess = backup(srcFile)

    else :
        backupSuccess = True

    if backupSuccess :
        # open file
        f = open(srcFile, 'r')
        data = f.read()
        f.close()

        if removeLine :
            replace = ''

        replaceData = data.replace(search, replace)

        # write back
        f = open(srcFile, 'w')
        f.write(replaceData)
        f.close()

        return True

def search_replace_keys(srcFile, searchReplaceDict, backupFile = True) :
    if backupFile :
        backupSuccess = backup(srcFile)

    else :
        backupSuccess = True

    if backupSuccess :
        # open file
        f = open(srcFile, 'r')
        data = f.read()
        f.close()

        for search, replace in searchReplaceDict.items():
            data = data.replace(search, replace)

        replaceData = data

        # write back
        f = open(srcFile, 'w')
        f.write(replaceData)
        f.close()

        return True

def backup(srcFile) :
    backupName = '.bk'
    srcDir = os.path.dirname(srcFile)
    backupDir = '%s/%s' % (srcDir, backupName)
    timeSuffix = str(datetime.now()).replace(' ', '_').split('.')[0].replace(':', '-')
    basename = os.path.basename(srcFile)
    ext = basename.split('.')[-1]
    backupFile = '%s/%s_%s.%s' % (backupDir, basename, timeSuffix, ext)

    # create backup dir
    if not os.path.exists(backupDir) :
        os.makedirs(backupDir)

    # backup
    result = backupSuccess = copy(srcFile, backupFile)

    return result

def get_latest_file(srcDir, ext=''):
    # print srcDir
    # print listFile(srcDir)
    list_of_files = glob.glob('{}/*{}'.format(srcDir, ext)) # * means all if need specific format then *.csv
    latest_file = max(list_of_files, key=os.path.getctime)
    return latest_file

def copy_to_clipboard(text):
    result = os.system("echo '%s'|clip" % text)
    return result


def json_dumper(path, data):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    with open(path, 'w') as outfile:
        json.dump(data, outfile, indent=4)


def json_loader(path):
    with open(path) as configData:
        config = json.load(configData, object_pairs_hook=OrderedDict)
        return config


def forward_slash_to_backslash(text):
    return text.replace('/', '\\')


def back_slash_to_forwardslash(text):
    return text.replace('\\', '/')



def increment_file(path):
    dirname = os.path.dirname(path)
    newVersion = calculate_version(list_file(dirname) if os.path.exists(dirname) else [])
    currentVersion = search_for_version(path)
    basename = os.path.basename(path)
    newName = basename.replace(currentVersion, newVersion)
    incrementFileName = '%s/%s' % (dirname, newName)
    return incrementFileName


def list_file(path):
    files = []
    if os.path.exists(path): 
        files = [d for d in os.listdir(path) if os.path.isfile(os.path.join(path, d))]
    return files

def list_folder(path):
    dirs = []
    if os.path.exists(path): 
        dirs = [d for d in os.listdir(path) if os.path.isdir(os.path.join(path,d))]
    return dirs 

def search_for_version(filename, prefix='v', padding=3):
    versionNumber = [a[0: padding] for a in filename.split(prefix) if a[0: padding].isdigit()]
    return '%s%s' % (prefix, versionNumber[0]) if versionNumber else ''


def get_increment_version(filename, prefix='v', padding=3):
    version = search_for_version(filename, prefix=prefix, padding=padding)
    if version:
        num = version.split(prefix)[-1]
        if num.isdigit():
            nextVersion = int(num) + 1
            newVersion = get_version(prefix, padding, nextVersion)
            return newVersion
    else:
        return get_version(prefix, padding, 1)


def get_version(prefix, padding, version):
    cmd = '"%0' + str(padding) + 'd" % version'
    newVersion = '%s%s' % (prefix, eval(cmd))
    return newVersion


def calculate_version(files, prefix='v', padding=3):
    if not files:
        return get_version(prefix, padding, 1)
    allVersions = sorted([search_for_version(a).split(prefix)[-1] for a in files if search_for_version(a)])
    intVersions = [int(a) for a in allVersions if a.isdigit()]
    if intVersions:
        nextVersion = max(intVersions) + 1
        return get_version(prefix=prefix, padding=padding, version=nextVersion)
    else:
        return get_version(prefix=prefix, padding=padding, version=1)


def copy_json(data):
    jsonData = json_loader(data)
    failedResult = []
    for src, dst in jsonData.iteritems:
        result = copy(src, dst)
        if not os.path.exists(dst):
            logger.warning('failed to copy %s' % dst)
            failedResult.append(dst)

    return True if not failedResult else False


def sort_by_date(files):
    infoDict = dict()
    sortedList = []
    for file in files:
        mtime = os.path.getmtime(file)
        infoDict[mtime] = file

    for time in sorted(infoDict.keys()):
        sortedList.append(infoDict[time])

    return sortedList

def get_frame_digits(fileName, padding=4):
    name, ext = os.path.splitext(fileName)
    last_four = name[padding*-1:]
    if all([i.isdigit() for i in last_four]):
        return last_four

def get_lastest_file(folder_path, ext):
    list_of_files = glob.glob('{}/*.{}'.format(folder_path, ext)) # * means all if need specific format then *.csv
    if list_of_files:
        return max(list_of_files, key=os.path.getctime)


def find_number_from_pattern(path, pathPattern, pattern='####'): 
    # path = abc.1001.abc 
    # pathPattern = abc.###.abc 
    # result 1001 

    patternLen = len(pattern)
    basenameTmp = os.path.basename(pathPattern)
    basename = os.path.basename(path) 
    filenameMatch = basenameTmp.split(pattern)[0]

    if filenameMatch in basename: 
        digit = basename.replace(filenameMatch, '')[0: patternLen]
        if digit.isdigit(): 
            return digit

def replace_pattern_number(pathPattern, number, pattern='####'): 
    if pattern in pathPattern: 
        return pathPattern.replace(pattern, number) 
    else: 
        return pathPattern


def get_sequence_info(path, pattern='####'): 
    """ return frame range information of a given path pattern """
    info = dict()
    patternLen = len(pattern)
    
    if pattern in path: 
        dirname = os.path.dirname(path)
        filenameMatch = os.path.basename(path).split(pattern)[0]

        files = list_file(dirname)
        sequences = []
        numbers = []

        for file in files: 
            digit = find_number_from_pattern(file, path, pattern)
            if digit: 
                sequences.append(file)
                numbers.append(file.replace(filenameMatch, '')[0: patternLen])

        if sequences: 
            info['sequences'] = ['%s/%s' % (dirname, a) for a in sequences]
        
        if numbers: 
            info['missing'] = missing_range(numbers)
            info['range'] = [numbers[0], numbers[-1]]
    
    return info

def missing_range(inputRange): 
    """ find a missing range from given list """ 
    # num = ['001', '002', '003', '004', '006', '008']
    # Result: ['005', '007'] # 

    def pad_string(num, padding): 
        """ generate padding string """ 
        command = ('"%0' + str(padding) + 'd" % num')
        return eval(command)

    missingFrame = []
    for i, n in enumerate(inputRange): 
        isStr = False 
        if isinstance(n, (str, unicode)): 
            paddingNum = len(n)
            n = int(n)
            isStr = True


        if not i == len(inputRange) - 1: 
            nextNum = inputRange[i+1]
            if isinstance(nextNum, (str, unicode)): 
                nextNum = int(nextNum)
            dif = nextNum - n
            # print nextNum, n, dif
            if dif == 1: 
                continue 
            else: 
                print ('missing range', nextNum, n, dif)
                missingRange = range(n, nextNum)[1:]
                
                if isStr: 
                    missingRange = [pad_string(a, paddingNum) for a in missingRange]
                missingFrame += missingRange
                
    return missingFrame


def copy2(src, dst): 
    """ copy pattern sequences """ 
    sequenceInfo = get_sequence_info(src)
    if not sequenceInfo: 
        return copy(src, dst)

    else: 
        pathPattern = src
        sequences = sequenceInfo['sequences']

        for file in sequences: 
            num = find_number_from_pattern(file, pathPattern)
            if num: 
                srcFile = file 
                dstFile = replace_pattern_number(dst, num)
                xcopy_file(srcFile, dstFile)


def create_shortcut(shortcutPath, linkPath, iconPath=''): 
    if not os.path.exists(os.path.dirname(shortcutPath)): 
        os.makedirs(os.path.dirname(shortcutPath))

    shortcutPath = shortcutPath.replace('/', '\\')
    linkPath = linkPath.replace('/', '\\')
    iconPath = iconPath.replace('/', '\\')

    commands = []
    commands.append('@echo off')
    commands.append('echo Set oWS = WScript.CreateObject("WScript.Shell") > CreateShortcut.vbs')
    commands.append('echo sLinkFile = "{}" >> CreateShortcut.vbs'.format(shortcutPath))
    commands.append('echo Set oLink = oWS.CreateShortcut(sLinkFile) >> CreateShortcut.vbs')
    commands.append('echo oLink.TargetPath = "{}" >> CreateShortcut.vbs'.format(linkPath))
    commands.append('echo oLink.Arguments = "{}" >> CreateShortcut.vbs'.format(linkPath))
    # commands.append('echo oLink.IconLocation = "%RFSCRIPT%\icons\launcher_logo.ico" >> CreateShortcut.vbs')
    commands.append('echo oLink.Save >> CreateShortcut.vbs')
    commands.append('cscript CreateShortcut.vbs')
    commands.append('del CreateShortcut.vbs')
    subprocess.call('&&'.join(commands), shell=True)

def are_dir_trees_equal(dir1, dir2):
    """
    Compare two directories recursively. Files in each directory are
    assumed to be equal if their names and contents are equal.

    @param dir1: First directory path
    @param dir2: Second directory path

    @return: True if the directory trees are the same and 
        there were no errors while accessing the directories or files, 
        False otherwise.
   """

    dirs_cmp = filecmp.dircmp(dir1, dir2)
    if len(dirs_cmp.left_only)>0 or len(dirs_cmp.right_only)>0 or \
        len(dirs_cmp.funny_files)>0:
        return False
    (_, mismatch, errors) =  filecmp.cmpfiles(
        dir1, dir2, dirs_cmp.common_files, shallow=False)
    if len(mismatch)>0 or len(errors)>0:
        return False
    for common_dir in dirs_cmp.common_dirs:
        new_dir1 = os.path.join(dir1, common_dir)
        new_dir2 = os.path.join(dir2, common_dir)
        if not are_dir_trees_equal(new_dir1, new_dir2):
            return False
    return True

def zip_dir(sources, destination, password=None):
    import rf_config as config
    cmd = [config.Software.sevenZip, 'a']
    if password != None:
        cmd += ['-p{}'.format(password)]
    cmd += ['-y', destination]
    cmd += sources
    rc = subprocess.call(cmd)

def get_readable_filesize(file_name):
    """ Return file size in human readable units like KB, MB or GB"""
    size = os.path.getsize(file_name)
    if size < 1024*1024:
        return '{} KB'.format(round(size/1024.0), 2)
    elif size < 1024*1024*1024:
        return '{} MB'.format(round(size/(1024*1024.0)), 2)
    else:
        return '{} GB'.format(round(size/(1024*1024*1024.0)), 2)

def is_ascii(text):
    if isinstance(text, unicode):
        try:
            text.encode('ascii')
        except UnicodeEncodeError:
            return False
    else:
        try:
            text.decode('ascii')
        except UnicodeDecodeError:
            return False
    return True

def is_writable(dir_path):
    try:
        testfile = tempfile.TemporaryFile(dir=dir_path)
        testfile.close()
    except (OSError, IOError, WindowsError) as e:
        return False
    return True

def is_readable(dir_path):
    try:
        dirs = os.listdir(dir_path)
    except (OSError, IOError, WindowsError) as e:
        return False
    return True