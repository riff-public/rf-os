# -*- coding: utf-8 -*-

import os
import sys
import json
import urllib2

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)
import rf_config as config

from rf_utils import project_info
from rf_utils.fb_workplace.settings import ACCESS_TOKEN
from rf_utils.sg import sg_utils, sg_process


GRAPH_URL_PREFIX = 'https://graph.facebook.com'


def message_to_workplace_user(users=[], msg="", group_chat=False, version="", img=""):
    # https://graph.facebook.com/me/messages
    endpoint = GRAPH_URL_PREFIX + "/me/messages"
    headers = buildHeader()
    if not group_chat:
        for user in users:
            payload = build_payload(user, msg, version, img)
            if payload:
                request = urllib2.Request(endpoint, json.dumps(payload), headers)
                response = urllib2.urlopen(request)
                print("response", response.read())
    elif group_chat:
        payload = build_payload_group(users, msg, version, img)
        if payload:
            request = urllib2.Request(endpoint, json.dumps(payload), headers)
            response = urllib2.urlopen(request)
            print("response", response.read())


def buildHeader():
    return {'Authorization': 'Bearer ' + ACCESS_TOKEN, 'Content-Type': 'application/json'}


def make_message(user, msg, version, img):
    return "noti mada mada"


def group_trigger_by_task_step(triggers):
    grouped_trigger_task = []
    for trigger in triggers:
        data = {
            "step": trigger['step'],
            "task": trigger['task']
        }
        if data not in grouped_trigger_task:
            grouped_trigger_task.append(data)
    return grouped_trigger_task


def build_payload(user, msg="noti ", version="", img=""):
    human_user = get_human_user(user)
    if human_user['sg_meta_data']:
        human_user['sg_meta_data'] = json.loads(human_user['sg_meta_data'])
        ids_assigned = [human_user['sg_meta_data']['workplace']]
        msg_data = make_message(user, msg, version, img)
        data = {
            "recipient": {
                "ids": ids_assigned
            },
            "message": {
                "text": msg_data
            }
        }
        return data
    else:
        return False


def build_payload_group(users, msg="noti ", version="", img=""):
    ids_assigned = []
    msg_data = make_message(users, msg, version, img)
    for user in users:
        human_user = get_human_user(user)
        if human_user['sg_meta_data']:
            human_user['sg_meta_data'] = json.loads(human_user['sg_meta_data'])
            ids_assigned.append([human_user['sg_meta_data']['workplace']])

    if ids_assigned:
        data = {
            "recipient": {
                "ids": ids_assigned
            },
            "message": {
                "text": msg_data
            }
        }
        return data
    else:
        return False


def get_target_tasks(project_entity, src_task):
    proj = project_info.ProjectInfo(project=project_entity['name'])
    if src_task['entity']['type'] == "Asset":
        tasks_trigger_table = proj.task.dependency('asset')
    elif src_task['entity']['type'] == 'Shot':
        tasks_trigger_table = proj.task.dependency('scene')

    taskEntity = sg_process.get_one_task_by_step(
        src_task.get('entity'),
        src_task.get('step', {}).get('name'),
        src_task.get('content'),
        create=False
    )
    if taskEntity:
        triggers = []
        target_tasks = []
        for task_config in tasks_trigger_table:
            if len(task_config['events']) == 1:
                if (
                    task_config['events'][0]['step'] == src_task.get('step', {}).get('name') and
                    task_config['events'][0]['task'] == src_task.get('content')
                ):
                    triggers.append(task_config['trigger'])

        triggers_grouped = group_trigger_by_task_step(triggers[0])
        for trigger in triggers_grouped:
            target_tasks.append(sg_process.get_one_task_by_step(taskEntity['entity'], trigger['step'], trigger['task']))

    return target_tasks


def get_users(target_tasks):
    users = []
    for target_task in target_tasks:
        for user_assign in target_task['task_assignees']:
            users.append(user_assign)
    return users


def get_human_user(user):
    human_user_filtered = [
        ['id', 'is', user['id']]
    ]
    fields = ['id', 'name', 'sg_meta_data']
    human_user = sg_utils.sg.find_one('HumanUser', human_user_filtered, fields)
    return human_user


def send_message(
    project_entity,
    src_task,
    users=[],
    msg="ASDFG",
    group_chat=False,
    version="",
    img=""
):
    project_entity = {
        "type": "Project",
        "id": 134,
        "name": "projectName"
    }

    src_task = sg_utils.sg.find_one("Task", [
        ['id', 'is', 39273],
        ['project', 'is', project_entity]],
        ['entity', 'step', 'content']
    )

    target_tasks = get_target_tasks(project_entity, src_task)
    users = get_users(target_tasks)
    message_to_workplace_user(users=users, msg="", group_chat=False, version="", img="")
