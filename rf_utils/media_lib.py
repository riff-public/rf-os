import sys
import os
import rf_config as config
import subprocess

def img_to_h264(imgs, dst, fps=24):
    ffmpeg = config.Software.data['ffmpeg']
    cmd = '"O:\\systemTool\\convertor\\ffmpeg_codec\\bin\\ffmpeg.exe" -y -i %s -r %s -vcodec libx264 -vprofile baseline -crf 22 -bf 0 -pix_fmt yuv420p -f mov %s' % (c, frameRate, s)


def mov_thumbnail(src, ext='.jpg', dst=''): 
    basename, srcExt = os.path.splitext(src)
    if dst: 
        dstBase, dstExt = os.path.splitext(dst)
        dst = '%s%s' % (dstBase, ext)
    else: 
        dst = '%s%s' % (basename, ext)

    src = src.replace('/', '\\')
    dst = dst.replace('/', '\\')

    if os.path.exists(dst): 
        os.remove(dst)

    console = False 
    startupinfo = None
    if not console:
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    subprocess.call([config.Software.ffmpeg, '-i', src, '-ss', '00:00:00.000', '-vframes', '1', dst], startupinfo=startupinfo)

    return dst if os.path.exists(dst) else False


def play_media(player='rv', files=[]): 
    import subprocess

    if player == 'rv': 
        rv = config.Software.rv
        commands = [rv]
        commands += files
        subprocess.Popen(commands)

    if player == 'keyframePro': 
        python = config.Software.pythonPath
        keyframeProPy = '{}/core/rf_utils/keyframepro_utils.py'.format(os.environ.get('RFSCRIPT', 'O:/Pipeline'))
        commands = [python, keyframeProPy, '-i']
        commands += files
        subprocess.Popen(commands)

        print commands

        # import subprocess
        # from keyframe_pro.keyframe_pro_client import KeyframeProClient

        # kpro_client = KeyframeProClient()
        # player = config.Software.keyframePro
        # subprocess.Popen(player)

        # if kpro_client.connect(port=18181) and kpro_client.initialize():
         
        #     kpro_client.new_project(empty=True)

        #     # Import sources
        #     sources = []
        #     for path in files:
        #         sources.append(kpro_client.import_file(path))

        #     # Create a new timeline
        #     timeline = kpro_client.add_timeline('My Timeline')
        #     for source in sources:
        #         kpro_client.insert_element_in_timeline(source['id'], timeline['id'])

        #     # Make the timeline active in viewer A
        #     kpro_client.set_active_in_viewer(timeline['id'], 0)
        #     kpro_client.disconnect()
