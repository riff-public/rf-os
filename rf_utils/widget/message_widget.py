import os
import sys
import traceback
from collections import OrderedDict
from functools import partial 

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class MessageLabel(QtWidgets.QWidget) :
    currentIndexChanged = QtCore.Signal(str)
    def __init__(self, parent=None) :
        super(MessageLabel, self).__init__()
        # layout
        self.allLayout = QtWidgets.QVBoxLayout()
        # label 
        self.label = QtWidgets.QLabel()

        # add layout
        self.allLayout.addWidget(self.label)
        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)

    def set_message(self, text): 
        self.label.setText(text)

    def set_color(self, color): 
        self.label.setStyleSheet("color: rgb(%s, %s, %s);" % (color[0], color[1], color[2]))

    def reset_color(self): 
        self.label.setStyleSheet('')
