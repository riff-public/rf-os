# entity browser

import os
import sys
import traceback
from datetime import datetime
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils import icon
from rf_utils.widget import project_widget
from rf_utils.widget import sg_entity_widget
# from rf_utils.widget import media_widget
from rf_utils.widget import file_widget
from rf_utils.sg import sg_process
from rf_utils.sg import sg_utils
from rf_utils import thread_pool
from rf_utils import file_utils
from rf_utils.context import context_info
sg = sg_process.sg

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

ENTITY_CACHE = dict()


class Config: 
    asset = 'Asset'
    scene = 'Shot'
    entity_types = [asset, scene]
    context_type_all = 'All'
    all_tag = 'All Tags'
    context_type = {asset: 'asset', scene: 'scene'}
    attr_type = {asset: 'sg_asset_type', scene: 'sg_episode.Scene.code'}
    filetypes = ['rig']
    preferred_res = 'md'


class EntityBrowser(QtWidgets.QWidget):
    entitySelected = QtCore.Signal(object)
    displayChanged = QtCore.Signal(bool)
    def __init__(self, parent=None) :
        super(EntityBrowser, self).__init__(parent=parent)
        self.entities = list()
        self.entity_dict = OrderedDict()
        self.id_dict = OrderedDict()

        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)
        self.setup_ui()
        self.set_default()
        self.init_signals()
        # self.init_functions()

    def setup_ui(self): 
        self.layout = QtWidgets.QVBoxLayout()

        # filter layout
        self.filter_layout = QtWidgets.QHBoxLayout()
        self.layout.addLayout(self.filter_layout)

        # project 
        self.project_widget = project_widget.SGProjectComboBox(sg=sg_process.sg, layout=QtWidgets.QHBoxLayout())
        # entity_type
        self.entitytype_label = QtWidgets.QLabel('Entity')
        self.entitytype_comboBox = QtWidgets.QComboBox()
        # filter type
        self.filter_label = QtWidgets.QLabel('Type')
        self.filtertype_comboBox = QtWidgets.QComboBox()
        # spacer
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.filter_layout.addWidget(self.project_widget)
        self.filter_layout.addWidget(self.entitytype_label)
        self.filter_layout.addWidget(self.entitytype_comboBox)
        self.filter_layout.addWidget(self.filter_label)
        self.filter_layout.addWidget(self.filtertype_comboBox)
        self.filter_layout.addItem(spacerItem)

        # list layout 
        self.entity_layout = QtWidgets.QGridLayout()
        self.layout.addLayout(self.entity_layout)
        # search bar 
        self.search_lineEdit = QtWidgets.QLineEdit()
        self.entity_layout.addWidget(self.search_lineEdit, 0, 0)
        # asset list
        self.entity_widget = sg_entity_widget.EntityListWidget()
        self.entity_layout.addWidget(self.entity_widget, 1, 0)

        # # set stretches
        # self.entity_layout.setRowStretch(0, 0)
        # self.filter_layout.setStretch(0, 0)
        # self.filter_layout.setStretch(1, 0)
        # self.filter_layout.setStretch(2, 1)
        # self.filter_layout.setStretch(3, 0)
        # self.filter_layout.setStretch(4, 1)
        # self.filter_layout.setStretch(5, 2)

        self.setLayout(self.layout)

    def set_default(self): 
        self.setup_type()

    def init_signals(self): 
        self.project_widget.projectChanged.connect(self.project_changed)
        self.entitytype_comboBox.currentIndexChanged.connect(self.type_changed)
        self.filtertype_comboBox.currentIndexChanged.connect(self.display_entities)
        self.search_lineEdit.textChanged.connect(self.display_entities)
        self.entity_widget.selected.connect(self.entity_selected)

    def setup_type(self): 
        self.entitytype_comboBox.clear()
        i = 0 
        for k in sorted(Config.context_type.keys()): 
            v = Config.context_type[k]
            self.entitytype_comboBox.addItem(v)
            self.entitytype_comboBox.setItemData(i, k, QtCore.Qt.UserRole)
            i+=1

    def start(self, project=None, entity_type=None, filter_type=None, enabled=False): 
        """ run this to start fetch data and use comboBox as input """
        if project: 
            self.project_widget.blockSignals(True)
            self.project_widget.set_current_item(project)
            self.project_widget.blockSignals(False)
            self.project_widget.setEnabled(enabled)

        if entity_type: 
            self.entitytype_comboBox.blockSignals(True)
            index = self.entitytype_comboBox.findText(entity_type, QtCore.Qt.MatchFixedString)
            self.entitytype_comboBox.setCurrentIndex(index)
            self.entitytype_comboBox.blockSignals(False)
            self.entitytype_comboBox.setEnabled(enabled)

        self.refresh()

        if filter_type: 
            index = self.filtertype_comboBox.findText(filter_type, QtCore.Qt.MatchFixedString)
            self.filtertype_comboBox.setCurrentIndex(index)
            self.filtertype_comboBox.setEnabled(enabled)

    def fetch(self, project, entity_type): 
        self.entities = find_entities(project, entity_type)
        return self.entities

    def init_project(self, project, entity_type): 
        self.entities = self.fetch(project, entity_type)
        self.build_keyword(self.entities)
        self.setup_filters(self.entities)
        # self.display_entities()

    def setup_filters(self, entities): 
        types = list()
        self.filtertype_comboBox.clear()

        for entity in entities: 
            attr = Config.attr_type[entity.get('type')]
            key = entity.get(attr)
            if not key in types and key: 
                types.append(key)

        self.filtertype_comboBox.addItems(sorted(types) + [Config.context_type_all])

    def filter_data(self, entities): 
        # filter
        entity_filter = self.filtertype_comboBox.currentText()
        try:
            keywords = [str(kw.lower()) for kw in self.search_lineEdit.text().replace(' ', '').split(',')]
        except UnicodeEncodeError:
            keywords = []
        keyword_matches = list()
        tag_matches = list()
        seq_matches = list()
        for name, entity in entities.items(): 
            input_type = '{}'.format(entity_filter)
            if input_type in name or input_type == Config.context_type_all: 
                asset_tags = [str(tag['name'].lower()) for tag in entity.get('tags', [])]
                sequences = [s['name'] for s in entity.get('sequences', [])]
                for keyword in keywords:
                    # search by word or tags
                    if keyword in name: 
                        keyword_matches.append(entity)
                        break
                    elif [t for t in asset_tags if keyword in t]:
                        tag_matches.append(entity)
                        break
                    elif [s for s in sequences if keyword in s]:
                        seq_matches.append(entity)
                        break

        matches = keyword_matches + tag_matches + seq_matches
        result = sorted(matches, key=lambda i: i['code'])

        return result  # sort by entity code

    def update_one_entity(self, new_entity):
        new_entity_id = new_entity.get('id')
        match_entities = [e for e in self.entities if e.get('id') == new_entity_id]
        if match_entities:
            old_entity = match_entities[0]
            index = self.entities.index(old_entity)
            self.entities[index] = new_entity
            self.build_keyword(self.entities)
            self.display_entities()
            # update tooltips
            item = self.get_item_from_entity(entity_id=new_entity_id)
            if item:
                item.setData(QtCore.Qt.UserRole, new_entity)
                self.set_item_tooltips(item=item, entity=new_entity)
                self.set_item_icon(item=item, entity=new_entity)

    def get_item_from_entity(self, entity_id):
        for item in self.entity_widget.all_items():
            item_data = item.data(QtCore.Qt.UserRole) 
            if isinstance(item_data, dict) and item_data.get('id') == entity_id:
                return item

    def build_keyword(self, entities): 
        self.entity_dict = OrderedDict()
        self.id_dict = OrderedDict()

        for entity in entities: 
            # kw dict
            attr = Config.attr_type[entity.get('type')]
            entity_filter = entity.get(attr)
            kw = '{}:{}'.format(entity_filter, entity['code'].lower())
            self.entity_dict[kw] = entity

            # id dict
            self.id_dict[entity.get('id')] = entity

    def display_entities(self): 
        self.entity_widget.clear()
        entities = self.filter_data(entities=self.entity_dict)
        self.entity_widget.add_items(entities, widget=False)
        self.entity_widget.set_text_mode()
        self.displayChanged.emit(True)
        
    def refresh(self): 
        project_entity = self.project_widget.current_item()
        entity_type = self.current_type()
        if entity_type: 
            self.init_project(project_entity['name'], entity_type)

    def project_changed(self, project_entity): 
        self.refresh()

    def refresh_current_project(self):
        project_entity = self.project_widget.current_item()
        entity_type = self.current_type()
        if entity_type: 
            self.entities = self.fetch(project_entity['name'], entity_type)
            self.build_keyword(self.entities)
            self.display_entities()

    def type_changed(self): 
        self.refresh()
        entity_type = self.current_type()
        if entity_type == 'Asset':
            self.filter_label.setText('Type')
        elif entity_type == 'Shot':
            self.filter_label.setText('Episode')

    def entity_selected(self, sg_entity): 
        self.entitySelected.emit(sg_entity)

    def sort_items(self):
        self.entity_widget.listWidget.sortItems()

    def current_entity(self):
        return self.entity_widget.current_item()

    def current_type(self):
        return self.entitytype_comboBox.itemData(self.entitytype_comboBox.currentIndex(), QtCore.Qt.UserRole) if self.entitytype_comboBox.currentIndex()!=None else None

class EntityTreeBrowser(EntityBrowser):
    ''' Entity browser with tree widget and tag listwidget '''

    entitySelected = QtCore.Signal(object)
    displayChanged = QtCore.Signal(bool)
    def __init__(self, parent=None) :
        super(EntityTreeBrowser, self).__init__(parent=parent)
        self._previous_tags = []
        self.sort_func = None
        self.sort_reverse = False

        # parent icon
        self.parent_icon = QtGui.QIcon()
        self.parent_icon.addPixmap(QtGui.QPixmap(icon.folder_full_icon), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        # child icon
        self.child_icon = QtGui.QIcon()
        self.child_icon.addPixmap(QtGui.QPixmap(icon.document_icon), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        # single icon
        self.single_icon = QtGui.QIcon()
        self.single_icon.addPixmap(QtGui.QPixmap(icon.folder_empty_icon), QtGui.QIcon.Normal, QtGui.QIcon.Off)

    def init_signals(self): 
        self.project_widget.projectChanged.connect(self.project_changed)
        self.entitytype_comboBox.currentIndexChanged.connect(self.type_changed)
        self.filtertype_comboBox.currentIndexChanged.connect(self.filtertype_changed)
        self.search_lineEdit.textChanged.connect(self.display_entities)
        self.tag_widget.selectionChanged.connect(self.display_entities)
        self.entity_widget.selectionChanged.connect(self.entity_selected)

    def setup_ui(self): 
        # main layout
        self.layout = QtWidgets.QVBoxLayout()

        # filter layout
        self.filter_layout = QtWidgets.QHBoxLayout()
        self.layout.addLayout(self.filter_layout)

        # project 
        self.project_widget = project_widget.SGProjectComboBox(sg=sg_process.sg, layout=QtWidgets.QHBoxLayout())
        # entity type
        self.entitytype_label = QtWidgets.QLabel('Entity')
        self.entitytype_comboBox = QtWidgets.QComboBox()
        # asset type
        self.filter_label = QtWidgets.QLabel('Type')
        self.filtertype_comboBox = QtWidgets.QComboBox()
        spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.filter_layout.addWidget(self.project_widget)
        self.filter_layout.addWidget(self.entitytype_label)
        self.filter_layout.addWidget(self.entitytype_comboBox)
        self.filter_layout.addWidget(self.filter_label)
        self.filter_layout.addWidget(self.filtertype_comboBox)
        self.filter_layout.addItem(spacerItem)

        # search bar 
        self.search_lineEdit = QtWidgets.QLineEdit()
        self.layout.addWidget(self.search_lineEdit)

        # splitter
        self.tagSplitter = QtWidgets.QSplitter()
        self.tagSplitter.setOrientation(QtCore.Qt.Horizontal)
        self.tagSplitWidget = QtWidgets.QWidget(self.tagSplitter)
        self.entitySplitWidget = QtWidgets.QWidget(self.tagSplitter)
        self.layout.addWidget(self.tagSplitter)

        # tag widget
        self.tag_layout = QtWidgets.QVBoxLayout(self.tagSplitWidget)
        self.tag_layout.setContentsMargins(0, 0, 0, 0)
        self.tag_widget = sg_entity_widget.TagListWidget()
        self.tag_widget.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)  # allow selection of multiple tags at a time
        self.tag_layout.addWidget(self.tag_widget)
        
        # tree widget
        self.entity_layout = QtWidgets.QVBoxLayout(self.entitySplitWidget)
        self.entity_layout.setContentsMargins(0, 0, 0, 0)
        self.entity_widget = sg_entity_widget.EntityTreeWidget()
        self.entity_widget.treeWidget.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)  # allow selection of 1 asset/shot at a time
        self.entity_layout.addWidget(self.entity_widget)

        # set stretches
        self.tagSplitter.setSizes([0, 300])
        self.filter_layout.setStretch(0, 0)
        self.filter_layout.setStretch(1, 0)
        self.filter_layout.setStretch(2, 1)
        self.filter_layout.setStretch(3, 0)
        self.filter_layout.setStretch(4, 1)
        self.filter_layout.setStretch(5, 2)

        self.setLayout(self.layout)

        # toolTip
        self.search_lineEdit.setToolTip('Search for asset by name and hashtags.\nUse comma(,) to split between keywards.')

    def get_filter_entity(self, entities):
        # filters
        index = self.entitytype_comboBox.currentIndex()
        mode = self.entitytype_comboBox.itemData(index, QtCore.Qt.UserRole)
        filter_type = self.filtertype_comboBox.currentText()
        filtered_entities = sg_utils.filter_entities(entities, (filter_type, Config.attr_type[mode]), (None, None))
        return filtered_entities

    def update_one_entity(self, new_entity):
        new_entity_id = new_entity.get('id')
        match_entities = [e for e in self.entities if e.get('id') == new_entity_id]
        if match_entities:
            old_entity = match_entities[0]
            index = self.entities.index(old_entity)
            self.entities[index] = new_entity
            self.build_keyword(self.entities)
            # update tooltips
            item = self.get_item_from_entity(entity_id=new_entity_id)
            if item:
                item.setData(QtCore.Qt.UserRole, 0, new_entity)
                self.set_item_tooltips(item=item, entity=new_entity)
                self.set_item_icon(item=item, entity=new_entity)

    def get_item_from_entity(self, entity_id):
        for item in self.entity_widget.all_items():
            item_data = item.data(QtCore.Qt.UserRole, 0) 
            if isinstance(item_data, dict) and item_data.get('id') == entity_id:
                return item

    def populate_tags(self, entities):
        self.tag_widget.blockSignals(True)
        if self.filtertype_comboBox.currentText() == Config.context_type_all:
            filtered_entities = entities
        else:
            filtered_entities = self.get_filter_entity(entities)
        tagDict = sg_utils.get_tags(filtered_entities) or dict()
        self.tag_widget.clear()
        allTagDict = {'name': Config.all_tag}
        all_item = self.tag_widget.add_item(allTagDict, iconPath=icon.sgTag, showIcon=True)
        if Config.all_tag in self._previous_tags:
            all_item.setSelected(True)

        found_previous = False
        for tag in sorted(tagDict.keys(), key=lambda s: s.lower()):
            tagEntity = tagDict[tag]
            item = self.tag_widget.add_item(tagEntity, iconPath=icon.sgTag, showIcon=True)
            if tag in self._previous_tags:
                item.setSelected(True)
                found_previous = True

        if not found_previous:
            self.tag_widget.set_current(Config.all_tag)
        self.tag_widget.blockSignals(False)

    def filter_data(self, entities, sort_func=None, sort_reverse=False): 
        # filter by tags
        selectedTags = self.tag_widget.selected_items()
        tagged_entities = OrderedDict()
        if selectedTags:
            filterTags = [str(a['name']) for a in selectedTags]
            allTags = True if Config.all_tag in filterTags else False

            if not allTags:
                for keyword, sgEntity in entities.iteritems():
                    tags = sg_utils.get_entity_tags(sgEntity)
                    names = [str(a) for a in tags.keys()] if tags else []
                    if any(tagKey in names for tagKey in filterTags):
                        tagged_entities[keyword] = sgEntity
        # no tag selection or All Tag is selected
        if not tagged_entities:
            tagged_entities = entities

        # filter by search bar
        entity_filter = self.filtertype_comboBox.currentText()
        try:
            keywords = [str(kw.lower()) for kw in self.search_lineEdit.text().replace(' ', '').split(',')]
        except UnicodeEncodeError:
            keywords = []
        keyword_matches = list()
        tag_matches = list()
        seq_matches = list()
        for name, entity in tagged_entities.items(): 
            input_type = '{}'.format(entity_filter)
            if input_type in name or input_type == Config.context_type_all: 
                asset_tags = [str(tag['name'].lower()) for tag in entity.get('tags', [])]
                sequences = [s['name'] for s in entity.get('sequences', [])]
                for keyword in keywords:
                    # search by word or tags
                    if keyword in name: 
                        keyword_matches.append(entity)
                        break
                    elif [t for t in asset_tags if keyword in t]:
                        tag_matches.append(entity)
                        break
                    elif [s for s in sequences if keyword in s]:
                        seq_matches.append(entity)
                        break

        # sortby = self.sort_combobox.currentIndex()
        match_result = keyword_matches + tag_matches + seq_matches
        # if sortby == 0:  # alphabetically
        #     result = sorted(matches, key=lambda i: i.get('code', None))
        # elif sortby == 1:  # sort by modified date
        #     result = sorted(matches, key=lambda i: i.get('updated_at', None), reverse=True)
        sorted_result = sorted(match_result, key=self.sort_func, reverse=self.sort_reverse)

        return sorted_result  

    def filtertype_changed(self):
        # store previous tag selection
        self._previous_tags = [str(a['name']) for a in self.tag_widget.selected_items()]
        # update tags
        self.populate_tags(self.entities)
        self.display_entities()

    def display_entities(self): 
        # update entities
        self.entity_widget.clear()
        entities = self.filter_data(entities=self.entity_dict)

        # add top level assets
        for entity in entities:
            # add parent item
            parent_item = self.entity_widget.add_item(entity=entity)

            has_children = 'assets' in entity and entity['assets']
            has_parent = 'parents' in entity and entity['parents']
            # set parent tool tip
            self.set_item_tooltips(item=parent_item, entity=entity)

            # set icon
            self.set_item_icon(item=parent_item, entity=entity)
            
            # asset has children, add tree item children
            if has_children:
                for sub_asset in entity['assets']:
                    sub_asset_id = sub_asset.get('id', None)
                    if sub_asset_id and sub_asset_id in self.id_dict:
                        asset_entity = self.id_dict[sub_asset_id]

                        # add child item
                        child_item = self.entity_widget.add_item(entity=asset_entity, parent=parent_item, icon=self.child_icon)
                        # set tool tips for child item
                        self.set_item_tooltips(item=child_item, entity=asset_entity)

        self.displayChanged.emit(True)

    def set_item_tooltips(self, item, entity):
        toolTipTexts = []
        # create date
        create_date = entity.get('created_at')
        if create_date: 
            toolTipTexts.append('Created: {}'.format(datetime.strftime(create_date, '%y/%m/%d-%H:%M')))
        # modified date
        mod_date = entity.get('updated_at')
        if mod_date: 
            toolTipTexts.append('Modified: {}'.format(datetime.strftime(mod_date, '%y/%m/%d-%H:%M')))
        # tags
        asset_tags = [tag['name'] for tag in entity.get('tags', []) if tag['name']]
        if asset_tags:
            toolTipTexts.append('Tags: {}'.format(', '.join(asset_tags)))
        # parent sets
        parent_names = [asset.get('name') for asset in entity.get('parents', []) if 'name' in asset and asset['name']]
        if parent_names:
            toolTipTexts.append('Sets: {}'.format(', '.join(parent_names)))
        # used in
        usedin = [s['name'] for s in entity.get('sequences', []) if s['name']]
        if usedin:
            toolTipTexts.append('Used in: {}'.format('\n               '.join(usedin)))

        if toolTipTexts:
            item.setToolTip(0, '\n'.join(toolTipTexts))

    def set_item_icon(self, item, entity):
        has_children = 'assets' in entity and entity['assets']
        has_parent = 'parents' in entity and entity['parents']
        if has_parent:
            item.setIcon(0, self.child_icon)
        else:
            if has_children:
                item.setIcon(0, self.parent_icon)
            else:
                item.setIcon(0, self.single_icon)

    def sort_items(self, sort_func, sort_reverse):
        self.sort_func = sort_func
        self.sort_reverse = sort_reverse
        self.display_entities()

    def entity_selected(self, sg_entity): 
        self.entitySelected.emit(sg_entity)

class EntityHeroBrowser(EntityBrowser):
    """docstring for EntityHeroBrowser"""
    fileSelected = QtCore.Signal(object)

    def __init__(self, parent=None):
        self.hero_files = list()
        super(EntityHeroBrowser, self).__init__(parent=parent)
        self.setup_filetype()
        self.setup_res()

    def setup_ui(self): 
        super(EntityHeroBrowser, self).setup_ui()
        self.res_layout = QtWidgets.QHBoxLayout()
        self.res_label = QtWidgets.QLabel('res')
        self.res_comboBox = QtWidgets.QComboBox()
        
        self.filetype_label = QtWidgets.QLabel('type')
        self.type_comboBox = QtWidgets.QComboBox()

        spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.res_layout.addWidget(self.res_label)
        self.res_layout.addWidget(self.res_comboBox)
        self.res_layout.addWidget(self.filetype_label)
        self.res_layout.addWidget(self.type_comboBox)
        self.res_layout.addItem(spacer)
        
        self.file_widget = file_widget.FileListWidgetExtended()
        self.entity_layout.addLayout(self.res_layout, 0, 1)
        self.entity_layout.addWidget(self.file_widget, 1, 1)

    def setup_res(self): 
        projectInfo = context_info.project_info.ProjectInfo('default')
        res_dict = projectInfo.asset.list_res()
        res = [v for k, v in res_dict.items()]

        self.res_comboBox.clear()
        self.res_comboBox.addItems(res)

        index = self.res_comboBox.findText(Config.preferred_res, QtCore.Qt.MatchFixedString)
        self.res_comboBox.setCurrentIndex(index)

    def setup_filetype(self): 
        self.type_comboBox.addItems(Config.filetypes)

    def init_signals(self): 
        super(EntityHeroBrowser, self).init_signals()
        self.res_comboBox.currentIndexChanged.connect(self.display_files)
        self.type_comboBox.currentIndexChanged.connect(self.display_files)
        self.file_widget.itemSelected.connect(self.file_selected)

    def setup_context(self, project, entity_type, filter_type, entity_name): 
        context = context_info.Context()
        context.update(project=project, entityType=entity_type, entityGrp=filter_type, entity=entity_name)
        entity = context_info.ContextPathInfo(context=context)
        return entity

    def entity_selected(self, sg_entity): 
        super(EntityHeroBrowser, self).entity_selected(sg_entity)
        # list hero 
        project = sg_entity['project']['name']
        entity_name = sg_entity['code']
        entity_type = Config.context_type[sg_entity['type']]
        attr = Config.attr_type[sg_entity.get('type')]
        filter_type = sg_entity.get(attr)

        self.entity = self.setup_context(project, entity_type, filter_type, entity_name)
        self.hero_files = self.list_files(self.entity)
        self.display_files()


    def list_files(self, entity): 
        path = entity.path.hero().abs_path()
        files = file_utils.list_file(path)
        hero_files = list()

        for file in files: 
            filepath = '{}/{}'.format(path, file)
            hero_files.append(filepath)
        
        return hero_files

    def display_files(self): 
        # hardcode 
        exclude = '_mtr_' 
        self.file_widget.clear()
        res = '_{}'.format(self.res_comboBox.currentText())
        filetype = '_{}_'.format(self.type_comboBox.currentText())

        for file in self.hero_files: 
            fn = os.path.basename(file) 
            if res in fn and filetype in fn: 
                if not exclude in fn: 
                    self.file_widget.add_file(file)

    def file_selected(self, data): 
        self.fileSelected.emit(data)

class EntityTaskBrowser(EntityBrowser):
    taskSelected = QtCore.Signal(object)

    def __init__(self, parent=None):
        self.hero_files = list()
        super(EntityTaskBrowser, self).__init__(parent=parent)
        
    def setup_ui(self): 
        super(EntityTaskBrowser, self).setup_ui()
        self.task_layout = QtWidgets.QHBoxLayout()
        self.step_label = QtWidgets.QLabel('Step')
        self.step_comboBox = QtWidgets.QComboBox()
        self.task_label = QtWidgets.QLabel('Task')
        self.task_comboBox = QtWidgets.QComboBox()
        spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.task_layout.addItem(spacerItem)
        self.task_layout.addWidget(self.step_label)
        self.task_layout.addWidget(self.step_comboBox)
        self.task_layout.addWidget(self.task_label)
        self.task_layout.addWidget(self.task_comboBox)
    
        self.entity_layout.addLayout(self.task_layout, 2, 0)

    def entity_selected(self, sg_entity): 
        super(EntityTaskBrowser, self).entity_selected(sg_entity)
        project = sg_entity['project']['name']
        self.tasks = find_tasks(project, sg_entity)
        self.display_steps()
        self.display_tasks()

    def init_signals(self): 
        super(EntityTaskBrowser, self).init_signals()
        self.step_comboBox.currentIndexChanged.connect(self.display_tasks)
        self.task_comboBox.currentIndexChanged.connect(self.task_selected)

    def display_steps(self): 
        steps = list()
        self.step_comboBox.clear()

        for task in self.tasks: 
            step = task['step.Step.code']
            if step: 
                if not step in steps: 
                    steps.append(step)

        self.step_comboBox.addItems(steps)

    def display_tasks(self): 
        step = self.step_comboBox.currentText()
        self.task_comboBox.blockSignals(True)
        self.task_comboBox.clear()
        self.task_comboBox.addItem('-- Select Task --')
        self.task_comboBox.setItemData(0, None, QtCore.Qt.UserRole)
        i = 1 
        for task in self.tasks: 
            task_step = task['step.Step.code']
            if step == task_step: 
                self.task_comboBox.addItem(task['content'])
                self.task_comboBox.setItemData(i, task, QtCore.Qt.UserRole)
                i+=1 
        self.task_comboBox.blockSignals(False)
        # self.task_comboBox.currentIndexChanged.emit(True)

    def task_selected(self): 
        task = self.current_task()
        if task:
            self.taskSelected.emit(task)

    def current_task(self):
        return self.task_comboBox.itemData(self.task_comboBox.currentIndex(), QtCore.Qt.UserRole) if self.task_comboBox.currentIndex()!=None else None

def find_entities(project, entity_type): 
    filters = [['project.Project.name', 'is', project]]
    entities = sg.find(entity_type, filters, get_fields(entity_type))
    return entities


def find_tasks(project, entity): 
    filters = [['project.Project.name', 'is', project], ['entity', 'is', entity]]
    fields = ['content', 'id', 'step.Step.code', 'sg_involve_with']
    return sg.find('Task', filters, fields)


def get_fields(entity_type): 
    if entity_type == 'Asset':
        fields = ['project', 'type', 'code', 'id', 'sg_asset_type', 'sg_subtype', 'parents',
                'tags', 'image', 'assets', 'sg_scenes', 'sequences', 'shots', 
                'updated_at', 'created_at', 'sg_link_designs', 'start_date', 'due_date']
    if entity_type == 'Shot':
        fields = ['project', 'type', 'code', 'id', 'sg_episode.Scene.code', 'sg_sequence_code', 
                'sg_shortcode', 'tags', 'image', 'updated_at', 'created_at', 'start_date', 'due_date']
    return fields








""" 
How to use 

from rf_utils.widget import entity_browser as win
reload(win)
a = win.EntityBrowser()
a.show()

1. you can leave blank and all navigation will be done by ui 
a.start()

2. defind project or entity_type or filter_type to auto navigate through these options
a.start(project='Hanuman', entity_type='asset', filter_type='char')

EntityHeroBrowser()
entity with hero files 

EntityTaskBrowser()
entity with task selection

"""
