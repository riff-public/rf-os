import os
import sys
import time

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

from rf_utils import file_utils
from rf_utils import publish_info
from rf_utils import register_entity
from rf_utils.context import context_info
from rf_utils.widget import file_widget
reload(register_entity)

cacheWorkspaceDict = dict()


class ViewPublishSpaceUI(QtWidgets.QWidget):
    """ UI ViewPublishSpaceUI """
    currentIndexChanged = QtCore.Signal(str)
    def __init__(self, parent=None):
        super(ViewPublishSpaceUI, self).__init__(parent=parent)
        self.allLayout = QtWidgets.QVBoxLayout()

        self.layout = QtWidgets.QVBoxLayout()
        self.checkBox = QtWidgets.QCheckBox('Hero')
        self.listWidget = file_widget.FileListWidget()
        self.layout.addWidget(self.checkBox)
        self.layout.addWidget(self.listWidget)

        self.allLayout.addLayout(self.layout)
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)
        self.checkBox.setVisible(False)



class ViewPublishSpace(ViewPublishSpaceUI):
    """ Logic Part for viewing publish """
    def __init__(self, parent=None):
        super(ViewPublishSpace, self).__init__(parent=parent)

    def update_input(self, ContextPathInfo):
        """ run here to activate ui """
        self.entity = ContextPathInfo
        self.populate_ui()

    def populate_ui(self):
        self.set_workspace()

    def set_workspace(self):
        files = self.list_workspace()
        self.listWidget.clear()
        self.listWidget.add_files(files)

    def list_workspace(self):
        """ list all workspace from publish _data file """
        publish = publish_info.RegisterAsset(self.entity)
        allData = publish.read_all_data()
        files = []

        for data in allData:
            if self.entity.step in data.keys():
                stepData = data[self.entity.step]

                for process, processData in stepData.iteritems():
                    if publish_info.Data.workspace in processData.keys():
                        workspaceFiles = processData[publish_info.Data.workspace]

                        for workspaceFile in workspaceFiles:
                            if not workspaceFile in files:
                                files.append(workspaceFile)

        return sorted(files)


class ViewPublishSpace2(ViewPublishSpace):
    """docstring for ViewPublishSpace2"""
    def __init__(self, parent=None):
        super(ViewPublishSpace2, self).__init__(parent=parent)
        self.checkBox.setVisible(True)
        self.checkBox.setChecked(True)
        self.init_signals()

    def init_signals(self):
        self.checkBox.stateChanged.connect(self.set_workspace)

    def list_workspace(self):
        """ list all workspace from publish _data file """
        data = cacheWorkspaceDict.get(str(self.entity.context))
        publishedVersions = data.get('published') if data else None
        heroFile = data.get('hero') if data else None

        if not publishedVersions: 
            regInfo = register_entity.Register(self.entity)
            self.entity.context.update(look=self.entity.process)
            workspaceDict = regInfo.version.workspaces(step=self.entity.step, res='md',process=self.entity.process, look=self.entity.look)

            if workspaceDict:
                publishedVersions = workspaceDict['publishedFile']
                heroFile = workspaceDict['heroFile']
                cacheWorkspaceDict[str(self.entity.context)] = {'published': publishedVersions, 'hero': heroFile}

        if self.checkBox.isChecked():
            return heroFile
        return publishedVersions

    def clear(self): 
        self.listWidget.clear()



