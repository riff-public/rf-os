from Qt import QtWidgets 
from Qt import QtGui 
from Qt import QtCore

class DroppedButton(QtWidgets.QPushButton):
    """docstring for DroppedButton"""
    dropped = QtCore.Signal(object)
    def __init__(self, *args, **kwargs):
        super(DroppedButton, self).__init__(*args, **kwargs)
        self.setAcceptDrops(True)
        
    def dragEnterEvent(self, event):
            if event.mimeData().hasUrls:
                event.accept()
            else:
                event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.dropped.emit(links[-1])
        else:
            event.ignore()


class DroppedFile(QtWidgets.QWidget):
    """docstring for DroppedFile"""
    def __init__(self, label='', parent=None):
        super(DroppedFile, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.button = DroppedButton(label, parent=parent)
        self.layout.addWidget(self.button)
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.button.dropped.connect(self.preview)

    def preview(self, url): 
        print url


# display a frame
# from PySide2 import QtWidgets, QtGui, QtCore
# import cv2
# import numpy
# label = QtWidgets.QLabel()
# video_file = 'P:/Hanuman/scene/work/mv/hnm_mv_q0010_s0010/anim/main/output/hnm_mv_q0010_s0010_anim_main.v002.mov'
# vid = cv2.VideoCapture(video_file)
# read_flag, frame = vid.read()
# rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
# h, w, ch = rgbImage.shape
# bytesPerLine = ch * w
# convertToQtFormat = QtGui.QImage(rgbImage.data, w, h, bytesPerLine, QtGui.QImage.Format_RGB888)
# p = convertToQtFormat.scaled(640, 480, QtCore.Qt.KeepAspectRatio)

# label.setPixmap(QtGui.QPixmap.fromImage(p))           
# label.show()
