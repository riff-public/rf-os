import os
import sys

is_python2 = True if sys.version_info[0] < 3 else False

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# import framework modules 
from rf_utils import user_info
from rf_utils.widget import completer

class Color: 
    bgred = 'background-color: rgb(200, 0, 0);'

class Message: 
    missingUser = 'No Shotgun User'

class UserComboBox(QtWidgets.QWidget) :
    """ user comboBox """
    def __init__(self, user=None, parent = None) :
        super(UserComboBox, self).__init__(parent)
        self.allLayout = QtWidgets.QHBoxLayout()
        self.user = user
        
        self._searchable_by_id = False

        # text label
        self.label = QtWidgets.QLabel('User')
        self.label.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)

        # combobox
        self.comboBox = QtWidgets.QComboBox()

        # check box
        self.checkBox = QtWidgets.QCheckBox('Debug')

        # add to layout
        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.comboBox)
        self.allLayout.addWidget(self.checkBox)

        self.allLayout.setStretch(0, 1)
        self.allLayout.setStretch(1, 5)
        self.allLayout.setStretch(2, 0)
        
        self.allLayout.setSpacing(5)
        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allLayout)
        self.checkBox.setVisible(False)

        self.init_signals()
        self.set_user()

    @property
    def searchable_by_id(self):
        return self._searchable_by_id

    @searchable_by_id.setter
    def searchable_by_id(self, value):
        self._searchable_by_id = value

    def set_user(self): 
        self.user = user_info.User() if not self.user else self.user
        result = self.check_user()
        self.comboBox.clear()

        if result: 
            self.comboBox.addItem(self.user.name())
            self.comboBox.setItemData(0, self.user.sg_user(), QtCore.Qt.UserRole)
            self.checkBox.setVisible(self.allow_debug())
        else: 
            self.comboBox.addItem(Message.missingUser)

    def init_signals(self): 
        self.checkBox.stateChanged.connect(self.debug_mode)
        
    def allow_debug(self): 
        return self.user.is_admin()

    def set_user_by_condition(self, filter_type='sg_employee_id', filters=[]): 
        self.all_user = user_info.SGUser()
        match_list = [a for a in self.all_user.userEntities if a[filter_type] in filters]
        if len(match_list) == 1: 
            entity = match_list[0]
            name = entity['sg_name']
            index = self.comboBox.findText(name, QtCore.Qt.MatchFixedString)
            if index >= 0:
                 self.comboBox.setCurrentIndex(index)
            self.comboBox.lineEdit().clearFocus()
        else: 
            logger.warning('Condition not found or duplicate')

    def check_user(self): 
        sgUser = None
        try:
            sgUser = self.user.name()
        except:
            pass

        userStatus = True if sgUser else False
        color = Color.bgred if not userStatus else ''
        self.comboBox.setStyleSheet(color)
        return userStatus

    def data(self): 
        index = self.comboBox.currentIndex()
        return self.comboBox.itemData(index, QtCore.Qt.UserRole)

    def debug_mode(self): 
        debug = self.checkBox.isChecked()
        sgUser = self.user.name()
        if debug: 
            # populate users
            self.all_user = user_info.SGUser()
            # get all users except the current user will be at index 0
            users = sorted([(a['sg_name'], a) for a in self.all_user.userEntities if a['sg_name'] != sgUser])
            
            for i, (name, user) in enumerate(users): 
                self.comboBox.addItem(name)
                self.comboBox.setItemData(i+1, user, QtCore.Qt.UserRole)
        else: 
            self.set_user()

    def set_searchable(self, state):
        if state:
            self.comboBox.setEditable(True)
            # monkey patch for different Python(Qt) version
            if is_python2:  # use custom completer in Python2 to match contains in string
                self.searchCompleter = completer.CustomQCompleter()
            else:  # in Python3, completer has "setFilterMode" method for matching contains
                self.searchCompleter = QtWidgets.QCompleter()
                self.searchCompleter.setFilterMode(QtCore.Qt.MatchContains)

            self.searchCompleter.setCompletionMode(QtWidgets.QCompleter.UnfilteredPopupCompletion)
            self.searchCompleter.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
            self.searchCompleter.setModel(self.comboBox.model())
            self.comboBox.setCompleter(self.searchCompleter)
            self.comboBox.setInsertPolicy(QtWidgets.QComboBox.NoInsert) # add this so recent search doesn't appear in combobox

            # focus setup
            # have to set focus to ClickFocus or it will ALWAYS recieve focus
            lineedit = self.comboBox.lineEdit()
            lineedit.setFocusPolicy(QtCore.Qt.ClickFocus)  
            lineedit.returnPressed.connect(self.search_employee_id)
        else:
            self.comboBox.setEditable(False)
            qcompleter = QtWidgets.QCompleter()
            qcompleter.setModel(self.comboBox.model())
            self.comboBox.setCompleter(qcompleter)

    def search_employee_id(self):
        if not self.searchable_by_id:
            return
        lineedit = self.comboBox.lineEdit()
        curr_text = lineedit.text()
        if all([i.isdigit() for i in curr_text]):
            self.set_user_by_condition('sg_employee_id', [curr_text])
            

class SGUserComboBox(QtWidgets.QWidget) :
    """ user comboBox """
    def __init__(self, parent = None) :
        super(SGUserComboBox, self).__init__(parent)
        self.allLayout = QtWidgets.QHBoxLayout()
        
        # display listWidget 
        self.label = QtWidgets.QLabel('User')
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        # export pushButton
        self.comboBox = QtWidgets.QComboBox()

        # add to layout
        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.comboBox)

        self.allLayout.setStretch(0, 1)
        self.allLayout.setStretch(1, 2)
        
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allLayout)

    def clear(self): 
        self.comboBox.clear()

    def add_items(self, userEntities): 
        for index, user in enumerate(userEntities): 
            self.comboBox.addItem(user.get('sg_name'))
            self.comboBox.setItemData(index, user, QtCore.Qt.UserRole)

    def selected_user(self): 
        return self.comboBox.itemData(self.comboBox.currentIndex(), QtCore.Qt.UserRole)


class SGReviewerComboBox(QtWidgets.QWidget) :
    """ user comboBox """
    def __init__(self, parent = None) :
        super(SGReviewerComboBox, self).__init__(parent)
        self.allLayout = QtWidgets.QHBoxLayout()
        
        # display listWidget 
        self.label = QtWidgets.QLabel('Reviewer')
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        # export pushButton
        self.comboBox = QtWidgets.QComboBox()

        # add to layout
        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.comboBox)

        self.allLayout.setStretch(0, 1)
        self.allLayout.setStretch(1, 2)
        
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allLayout)

    def clear(self): 
        self.comboBox.clear()

    def add_items(self, userEntities): 
        for index, user in enumerate(userEntities): 
            self.comboBox.addItem(user.get('sg_name'))
            self.comboBox.setItemData(index, user, QtCore.Qt.UserRole)

    def selected_user(self): 
        return self.comboBox.itemData(self.comboBox.currentIndex(), QtCore.Qt.UserRole)
