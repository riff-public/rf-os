import sys
import os
import re
from Qt import QtWidgets
from Qt import QtCore
from Qt import QtGui

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler)

moduleDir = os.path.dirname(sys.modules[__name__].__file__)

from rf_utils.sg import sg_thread
from rf_utils.pipeline import create_asset
from rf_utils.pipeline import create_structure
reload(create_structure)
from rf_utils.widget import dialog
from rf_utils import icon
reload(create_asset)

class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'

class CreateShotSequence(QtWidgets.QDialog):

    def __init__(self, context, parent=None):
        super(CreateShotSequence, self).__init__(parent=parent)

        # data
        self.scene = context

        self.layout = QtWidgets.QVBoxLayout(self)
        self.messageLayout = QtWidgets.QHBoxLayout()

        # title
        self.setWindowTitle('Create Sequence')

        # inputs
        self.shotLabel = QtWidgets.QLabel('Enter Sequence Number\n(e.g. 100)')
        self.shotNumber = QtWidgets.QLineEdit()
        self.layout.addWidget(self.shotLabel)
        self.layout.addWidget(self.shotNumber)

        self.previewLabel = QtWidgets.QLabel()
        self.layout.addWidget(self.previewLabel)

        # buttons
        # buttons = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Yes | QtWidgets.QDialogButtonBox.Cancel, QtCore.Qt.Horizontal, self)
        self.buttonBox = QtWidgets.QDialogButtonBox()
        self.buttonBox.addButton('Create', QtWidgets.QDialogButtonBox.AcceptRole)
        self.buttonBox.addButton('Cancel', QtWidgets.QDialogButtonBox.RejectRole)

        # signals
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        # set layout
        self.layout.setSpacing(4)
        self.layout.setContentsMargins(9, 9, 9, 9)


        self.layout.addWidget(self.buttonBox)
        self.setMinimumSize(260, 100)

        self.init_signals()

    def init_signals(self):
        self.shotNumber.textChanged.connect(self.set_preview)

    def set_preview(self):
        text = str(self.shotNumber.text())
        num = re.findall(r'\d+', text)
        num = num[0] if num else 0
        padding = '%04d' % int(num) # hard coded

        # modify context
        self.scene.checkFormat = True
        self.scene.context.update(entityParent=padding, entityCode='all')
        self.scene.context.update(entityParent=self.scene.sequence)

        # set preview
        self.shotName = self.scene.sg_name
        self.previewLabel.setText(self.shotName)

    def get_shot(self):
        return str(self.previewLabel.text())


class CreateShot(QtWidgets.QDialog):

    def __init__(self, context, parent=None):
        super(CreateShot, self).__init__(parent=parent)

        # data
        self.scene = context

        self.layout = QtWidgets.QVBoxLayout(self)
        self.messageLayout = QtWidgets.QHBoxLayout()

        # title
        self.setWindowTitle('Create Sequence')

        # inputs
        self.episodeLabel = QtWidgets.QLabel('Episode')
        self.episodeComboBox = QtWidgets.QComboBox()
        self.episodeCheckBox = QtWidgets.QCheckBox('Add new episode')
        self.seqLabel = QtWidgets.QLabel('Enter Sequence Number\n(e.g. 100)')
        self.seqNumber = QtWidgets.QLineEdit()
        self.shotLabel = QtWidgets.QLabel('Enter Shot Number\n(e.g. 100)')
        self.shotNumber = QtWidgets.QLineEdit()
        self.layout.addWidget(self.episodeLabel)
        self.layout.addWidget(self.episodeComboBox)
        self.layout.addWidget(self.episodeCheckBox)
        self.layout.addWidget(self.seqLabel)
        self.layout.addWidget(self.seqNumber)
        self.layout.addWidget(self.shotLabel)
        self.layout.addWidget(self.shotNumber)

        self.sequenceCheckBox = QtWidgets.QCheckBox('Sequence')
        self.layout.addWidget(self.sequenceCheckBox)

        self.previewLabel = QtWidgets.QLabel()
        self.layout.addWidget(self.previewLabel)

        self.statusLabel = QtWidgets.QLabel('')
        self.layout.addWidget(self.statusLabel)

        # buttons
        # buttons = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Yes | QtWidgets.QDialogButtonBox.Cancel, QtCore.Qt.Horizontal, self)
        self.buttonBox = QtWidgets.QDialogButtonBox()
        self.buttonBox.addButton('Create', QtWidgets.QDialogButtonBox.AcceptRole)
        self.buttonBox.addButton('Cancel', QtWidgets.QDialogButtonBox.RejectRole)

        # signals
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        # set layout
        self.layout.setSpacing(4)
        self.layout.setContentsMargins(9, 9, 9, 9)


        self.layout.addWidget(self.buttonBox)
        self.setMinimumSize(260, 100)

        self.sgEntities = None

        self.set_guide_value()
        self.init_signals()
        self.init_functions()

    def init_signals(self):
        self.sequenceCheckBox.stateChanged.connect(self.set_sequence_name)
        self.seqNumber.textChanged.connect(self.set_preview)
        self.shotNumber.textChanged.connect(self.set_preview)
        self.episodeComboBox.currentIndexChanged.connect(self.set_preview)
        self.episodeComboBox.editTextChanged.connect(self.set_preview)
        self.episodeCheckBox.stateChanged.connect(self.episodeComboBox.setEditable)

    def init_functions(self):
        self.sgThread = sg_thread.GetEpisodes(project=self.scene.project)
        self.sgThread.value.connect(self.populate_episode)
        # self.sgThread.finished.connect(self.fetch_finished)
        self.sgThread.start()

    def set_guide_value(self):
        self.seqNumber.setText('0010')
        self.shotNumber.setText('0010')
        self.set_preview()

    def populate_episode(self, episodes): 
        self.list_episode(episodes)
        self.sgThread = sg_thread.GetEntity(self.scene.project, self.scene.sgScene)
        self.sgThread.value.connect(self.populate_entity)

    def populate_entity(self, sgEntities):
        # set caches
        self.sgEntities = sgEntities
        self.sgEntities = sgEntities
        self.shotNames = sorted([a['code'] for a in sgEntities])
        # self.set_episode(sgEntities)

    def fetch_finished(self):
        pass

    def set_episode(self, sgEntities):
        if sgEntities: 
            self.episodeComboBox.clear()
            eps = list(set([a['sg_episode.Scene.code'] for a in sgEntities]))
            index = 0
            for i, ep in enumerate(sorted(eps)):
                index = i if self.scene.episode == ep else 0
                self.episodeComboBox.addItem(ep)
            self.episodeComboBox.setCurrentIndex(index)

    def list_episode(self, episodes): 
        eps = [a['code'] for a in episodes]
        index = 0
        for i, ep in enumerate(sorted(eps)):
            index = i if self.scene.episode == ep else 0
            self.episodeComboBox.addItem(ep)
        self.episodeComboBox.setCurrentIndex(index)

    def set_preview(self):
        episode = str(self.episodeComboBox.currentText())
        seqText = str(self.seqNumber.text())
        numSeq = re.findall(r'\d+', seqText)
        seqIsNum = True if seqText.isdigit() else False
        if seqIsNum:
            # numSeq = numSeq[0] if numSeq else 0
            numSeq = seqText
            seqPadding = '%04d' % int(numSeq) # hard coded
            self.scene.checkFormat = True
        else:
            seqPadding = seqText
            self.scene.checkFormat = False

        self.scene.context.update(entityGrp=episode, entityParent=seqPadding)
        self.scene.context.update(entityParent=self.scene.sequence)

        shotText = str(self.shotNumber.text())
        numShot = re.findall(r'\d+', shotText)
        
        shotIsNum = True if shotText.isdigit() else False
        if shotIsNum:
            # numShot = numShot[0] if numShot else 0
            numShot = shotText
            shotPadding = '%04d' % int(numShot) # hard coded
            self.scene.checkFormat = True
        else:
            shotPadding = shotText
            self.scene.checkFormat = False

        self.scene.context.update(entityCode=shotPadding)
        self.scene.context.update(entityCode=self.scene.name_code)

        # set preview
        self.shotName = self.scene.sg_name
        self.previewLabel.setText(self.shotName)
        self.check_name()


    def check_name(self):
        if self.sgEntities:
            shotNames = [a['code'] for a in self.sgEntities]
            shotName = str(self.previewLabel.text())
            if shotName in self.shotNames:
                self.statusLabel.setStyleSheet(Color.bgRed)
                self.statusLabel.setText('Shot already exists!')
                self.buttonBox.setEnabled(False)
            else:
                self.statusLabel.setText('')
                self.statusLabel.setStyleSheet('')
                self.buttonBox.setEnabled(True)


    def set_sequence_name(self):
        prevValue = str(self.seqNumber.text())
        isSequence = self.sequenceCheckBox.isChecked()
        shotName = 'all' if isSequence else prevValue
        self.shotNumber.setText(shotName)


    def get_shot(self):
        return str(self.previewLabel.text())


    # static method
    @staticmethod
    def show(context, parent=None):
        createShotDialog = CreateShot(context, parent=parent)
        result = createShotDialog.exec_()

        if result:
            from rf_utils.pipeline import create_scene
            shotName = createShotDialog.get_shot()
            shotType = 'Sequence' if createShotDialog.sequenceCheckBox.isChecked() else 'Shot'
            createResult = create_scene.create(str(createShotDialog.scene.project), 
                                shotName, 
                                str(createShotDialog.scene.episode), 
                                sequence=str(createShotDialog.scene.sequence), 
                                shortcode=createShotDialog.scene.name_code, 
                                shotType=shotType, 
                                link=True)
            return createResult

    @staticmethod
    def create_part(context, parent=None): 
        createShotDialog = CreateShot(context, parent=parent)
        
        # customized to part UI
        createShotDialog.sequenceCheckBox.setChecked(True)
        createShotDialog.shotLabel.setVisible(False)
        # createShotDialog.seqLabel.setVisible(False)
        createShotDialog.shotNumber.setVisible(False)
        createShotDialog.episodeCheckBox.setVisible(False)
        createShotDialog.sequenceCheckBox.setVisible(False)

        createShotDialog.seqLabel.setText('Create Part')
        createShotDialog.seqNumber.setText('')
        createShotDialog.seqNumber.setPlaceholderText('Part1')

        result = createShotDialog.exec_()

        if result:
            from rf_utils.pipeline import create_scene
            shotName = createShotDialog.get_shot()
            shotType = 'Preproduction'
            createResult = create_scene.create(str(createShotDialog.scene.project), shotName, str(createShotDialog.scene.episode), sequence=str(createShotDialog.scene.sequence), shortcode=createShotDialog.scene.name_code, shotType=shotType, link=True)
            return createResult



class CreateAsset(QtWidgets.QDialog):
    """docstring for CreateAsset"""
    def __init__(self, context, parent=None):
        super(CreateAsset, self).__init__(parent=parent)

        # data
        self.asset = context

        self.layout = QtWidgets.QVBoxLayout(self)
        self.messageLayout = QtWidgets.QHBoxLayout()

        # title
        self.setWindowTitle('Create Asset')

        # inputs
        self.assetLabel = QtWidgets.QLabel('Asset Name')
        self.assetName = QtWidgets.QLineEdit()
        self.layout.addWidget(self.assetLabel)
        self.layout.addWidget(self.assetName)


        self.assetTypeLabel = QtWidgets.QLabel('Type')
        self.assetTypeLayout = QtWidgets.QHBoxLayout()
        self.assetType = QtWidgets.QComboBox()
        self.assetTypeCheckBox = QtWidgets.QCheckBox('Show All')
        self.assetTypeLayout.addWidget(self.assetType)
        self.assetTypeLayout.addWidget(self.assetTypeCheckBox)
        self.layout.addWidget(self.assetTypeLabel)
        self.layout.addLayout(self.assetTypeLayout)

        self.assetTypeLayout.setSpacing(4)
        self.assetTypeLayout.setStretch(0, 1)
        self.assetTypeLayout.setStretch(1, 0)

        self.assetSubtypeLabel = QtWidgets.QLabel('Subtype')
        self.assetSubtypeLayout = QtWidgets.QHBoxLayout()
        self.assetSubtype = QtWidgets.QComboBox()
        self.assetSubtypeCheckBox = QtWidgets.QCheckBox('Show All')
        self.assetSubtypeLayout.addWidget(self.assetSubtype)
        self.assetSubtypeLayout.addWidget(self.assetSubtypeCheckBox)
        self.layout.addWidget(self.assetSubtypeLabel)
        self.layout.addLayout(self.assetSubtypeLayout)

        self.assetSubtypeLayout.setSpacing(4)
        self.assetSubtypeLayout.setStretch(0, 1)
        self.assetSubtypeLayout.setStretch(1, 0)

        self.setLabel = QtWidgets.QLabel('Set')
        self.assetParent = QtWidgets.QComboBox()
        self.layout.addWidget(self.setLabel)
        self.layout.addWidget(self.assetParent)

        self.layout2 = QtWidgets.QHBoxLayout()
        self.assetTagLabel = QtWidgets.QLabel('Search Tag')
        self.assetTag = QtWidgets.QLineEdit()
        self.tagButton = QtWidgets.QPushButton('+')
        self.tagButton.setMaximumSize(QtCore.QSize(20, 20))
        self.tabLabel = QtWidgets.QLabel('')

        self.layout.addWidget(self.assetTagLabel)
        self.layout2.addWidget(self.assetTag)
        self.layout2.addWidget(self.tagButton)
        self.layout2.addWidget(self.tabLabel)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.layout2.addItem(spacerItem)

        self.layout.addLayout(self.layout2)

        self.tagSubLayout = QtWidgets.QHBoxLayout()
        self.tagListWidget = QtWidgets.QListWidget()
        self.tagListWidget.setMaximumSize(QtCore.QSize(160, 100))
        self.tagSelectionListWidget = QtWidgets.QListWidget()
        self.tagSelectionListWidget.setMaximumSize(QtCore.QSize(160, 100))

        self.tagSubLayout.addWidget(self.tagListWidget)
        self.tagSubLayout.addWidget(self.tagSelectionListWidget)

        self.tagSubLayout.setContentsMargins(0, 0, 0, 0)
        self.tagSubLayout.setSpacing(0)
        self.layout.addLayout(self.tagSubLayout)

        self.statusLabel = QtWidgets.QLabel()
        self.layout.addWidget(self.statusLabel)

        # buttons
        # buttons = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Yes | QtWidgets.QDialogButtonBox.Cancel, QtCore.Qt.Horizontal, self)
        self.buttonBox = QtWidgets.QDialogButtonBox()
        self.buttonBox.addButton('Create', QtWidgets.QDialogButtonBox.AcceptRole)
        self.buttonBox.addButton('Cancel', QtWidgets.QDialogButtonBox.RejectRole)

        # signals
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        # set layout
        self.layout.setSpacing(4)
        self.layout.setContentsMargins(9, 9, 9, 9)


        self.layout.addWidget(self.buttonBox)
        self.setMinimumSize(260, 100)

        self.init_signals()
        self.init_functions()

        self.sgCaches = dict()

    def init_signals(self):
        self.assetName.textChanged.connect(self.check_name)
        self.assetTag.textChanged.connect(self.search_tag)
        self.tagButton.clicked.connect(self.create_tag)
        self.tagListWidget.itemSelectionChanged.connect(self.tag_selected)
        self.tagListWidget.itemDoubleClicked.connect(self.add_tags)
        self.assetTypeCheckBox.stateChanged.connect(self.asset_type_list)
        self.assetSubtypeCheckBox.stateChanged.connect(self.asset_subtype_list)

    def init_functions(self):
        """ populate form with sg data """
        self.sgThread = sg_thread.GetEntity(self.asset.project, self.asset.sgAsset)
        self.sgThread.value.connect(self.populate_entity)
        self.sgThread.finished.connect(self.fetch_finished)
        self.sgThread.start()

    def populate_entity(self, sgEntities):
        # set caches
        self.sgEntities = sgEntities
        self.assetNames = sorted([a['code'] for a in sgEntities])
        self.types = sorted(list(set([a.get('sg_asset_type') for a in sgEntities if a.get('sg_asset_type')])))
        self.subtypes = sorted(list(set([a.get('sg_subtype') for a in sgEntities if a.get('sg_subtype')])))
        
        self.set_type(self.types)
        self.set_subtype(self.subtypes)
        self.set_set(sgEntities)
        self.fetch_tags()

    def fetch_tags(self):
        self.sgThread2 = sg_thread.GetTags()
        self.sgThread2.value.connect(self.set_tags)
        self.sgThread2.start()

    def set_tags(self, tags):
        self.tags = tags
        key = str(self.assetTag.text())
        self.tagListWidget.clear()

        for tag in tags:
            name = tag['name']
            if key in name:
                self.add_tag_widget(self.tagListWidget, tag)

    def add_tag_widget(self, widget, tagEntity):
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(icon.sgTag), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        item = QtWidgets.QListWidgetItem(widget)
        item.setText(tagEntity['name'])
        item.setData(QtCore.Qt.UserRole, tagEntity)
        item.setIcon(iconWidget)

    def fetch_finished(self):
        pass

    def asset_type_list(self): 
        if self.assetTypeCheckBox.isChecked(): 
            data = sg_thread.sg.schema_field_read('Asset', field_name='sg_asset_type')
            allTypes = data['sg_asset_type'].get('properties').get('valid_values').get('value')
            self.set_type(allTypes)
        else: 
            self.set_type(self.subtypes)

    def asset_subtype_list(self): 
        if self.assetSubtypeCheckBox.isChecked(): 
            data = sg_thread.sg.schema_field_read('Asset', field_name='sg_subtype')
            allTypes = data['sg_subtype'].get('properties').get('valid_values').get('value')
            self.set_subtype(allTypes)
        else: 
            self.set_subtype(self.subtypes)

    def set_type(self, types):
        self.assetType.clear()
        self.assetType.addItems(types)

    def set_subtype(self, subtypes):
        self.assetSubtype.clear()
        self.assetSubtype.addItems(sorted(subtypes))

    def set_set(self, sgEntities):
        sets = [a for a in sgEntities if a.get('sg_asset_type') == 'set']
        self.assetParent.clear()
        self.assetParent.addItem('-- None --')
        for i, setEntity in enumerate(sorted(sets)):
            setName = setEntity.get('code')
            self.assetParent.addItem(setName)
            self.assetParent.setItemData(i+1, setEntity, QtCore.Qt.UserRole)

    def create_tag(self):
        tag = str(self.assetTag.text())
        if not tag in [a['name'] for a in self.tags]:
            data = {'name': tag}
            tagEntity = sg_thread.sg.create('Tag', data)

        else:
            tagEntity = [a for a in self.tags if a['name'] == tag]
            tagEntity = tagEntity[0] if tagEntity else None

        self.tags.append(tagEntity)
        self.set_tags(self.tags)

    def tag_selected(self):
        items = self.tagListWidget.selectedItems()

    def add_tags(self, item):
        tagEntity = item.data(QtCore.Qt.UserRole) or dict()
        tagName = tagEntity.get('name')

        allItems = [self.tagSelectionListWidget.item(a).data(QtCore.Qt.UserRole) for a in range(self.tagSelectionListWidget.count())]
        names = [a['name'] for a in allItems]
        if not tagName in names:
            self.add_tag_widget(self.tagSelectionListWidget, tagEntity)


    def check_name(self):
        text = str(self.assetName.text())
        if text in self.assetNames:
            self.statusLabel.setStyleSheet(Color.bgRed)
            self.statusLabel.setText('Asset already exists!')
            self.buttonBox.setEnabled(False)
        else:
            self.statusLabel.setText('')
            self.statusLabel.setStyleSheet('')
            self.buttonBox.setEnabled(True)

    def search_tag(self):
        self.set_tags(self.tags)


    def get_info(self):
        project = self.asset.project
        assetName = str(self.assetName.text())
        assetType = str(self.assetType.currentText())
        assetSubtype = str(self.assetSubtype.currentText())
        assetParent = self.assetParent.itemData(self.assetParent.currentIndex(), QtCore.Qt.UserRole)
        assetSet = [] if not assetParent else [assetParent]
        items = [self.tagSelectionListWidget.item(a) for a in range(self.tagSelectionListWidget.count())]
        tags = [a.data(QtCore.Qt.UserRole) for a in items]
        return project, assetName, assetType, assetSubtype, assetSet, tags

    def create_asset(self):
        project, assetName, assetType, assetSubtype, assetSet, tags = self.get_info()
        template = 'default'
        if assetType == 'char':
            template = 'char'
        if assetType == 'prop':
            template = 'prop'
        if assetType == 'set':
            template = 'set'
        if assetType == 'mattepaint': 
            template = 'mattepaint'
        if assetType == 'animated': 
            template = 'animated'
        createResult = create_asset.create(project, assetName, assetType, assetSubtype=assetSubtype, mainAsset='', tags=tags, template=template, setEntity=assetSet, link=True)
        return createResult

    # static method
    @staticmethod
    def show(context, parent=None):
        createAssetDialog = CreateAsset(context, parent=parent)
        result = createAssetDialog.exec_()

        if result:
            createResult = createAssetDialog.create_asset()
            return createResult


class CreateAssetThread(QtCore.QThread):
    value = QtCore.Signal(list)

    def __init__(self, project, assetName, assetType, assetSubtype):
        QtCore.QThread.__init__(self)

        self.project = project
        self.assetName = assetName
        self.assetType = assetType
        self.assetSubtype = assetSubtype

    def run(self):
        createResult = create_asset.create(project=self.project, 
                                        name=self.assetName, 
                                        assetType=self.assetType, 
                                        assetSubtype=self.assetSubtype, 
                                        mainAsset='', 
                                        template='', 
                                        link=True)
        self.value.emit(createResult)


class CreateLook(QtWidgets.QDialog):
    """docstring for CreateLook"""
    def __init__(self, context, sgHook=None, parent=None):
        super(CreateLook, self).__init__(parent=parent)
        self.asset = context
        self.sgHook = sgHook

        self.layout = QtWidgets.QVBoxLayout(self)
        self.messageLayout = QtWidgets.QHBoxLayout()

        # title
        self.setWindowTitle('Create Look')

        # inputs
        self.lookLabel = QtWidgets.QLabel('Look Name')
        self.lookComboBox = QtWidgets.QComboBox()
        self.lookName = QtWidgets.QLineEdit()
        self.layout.addWidget(self.lookLabel)
        self.layout.addWidget(self.lookComboBox)
        self.layout.addWidget(self.lookName)


        # buttons
        # buttons = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Yes | QtWidgets.QDialogButtonBox.Cancel, QtCore.Qt.Horizontal, self)
        self.buttonBox = QtWidgets.QDialogButtonBox()
        self.buttonBox.addButton('Create', QtWidgets.QDialogButtonBox.AcceptRole)
        self.buttonBox.addButton('Cancel', QtWidgets.QDialogButtonBox.RejectRole)

        # signals
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        # set layout
        self.layout.setSpacing(4)
        self.layout.setContentsMargins(9, 9, 9, 9)


        self.layout.addWidget(self.buttonBox)
        self.setMinimumSize(260, 100)

        self.init_functions()

    def init_functions(self):
        self.add_look()

    def list_look(self):
        lookPath = self.asset.path.look().abs_path()
        browseLook, look = os.path.split(lookPath)
        looks = [a for a in os.listdir(browseLook) if os.path.isdir('/'.join([browseLook, a]))]
        return looks

    def add_look(self):
        looks = self.list_look()
        self.lookComboBox.addItems(looks)

    def look_name(self):
        return str(self.lookName.text())

    def create_look(self, entity, lookName):
        # create task
        result = self.sgHook.create_look(entity, lookName)
        if result:
            create_structure.create_look(self.sgHook.sg, self.asset.project, entity['id'])
        # create folder



    # static method
    @staticmethod
    def show(context, sgHook=None, entity='', parent=None):
        createLookDialog = CreateLook(context, sgHook=sgHook, parent=parent)
        result = createLookDialog.exec_()

        if result:
            lookName = createLookDialog.look_name()

            if not lookName in createLookDialog.list_look():
                result = createLookDialog.create_look(entity, lookName)
            else:
                dialog.MessageBox.warning('Error', 'Look %s already exists' % lookName)



class CreateEpisode(QtWidgets.QDialog):
    """docstring for CreateEpisode"""
    def __init__(self, projectEntity, parent=None):
        super(CreateEpisode, self).__init__(parent=parent)
        self.projectEntity = projectEntity 

        self.layout = QtWidgets.QVBoxLayout(self)

        # title
        self.setWindowTitle('Create Episode')

        # inputs
        self.episodeLabel = QtWidgets.QLabel('Episode Name')
        self.episodeLineEdit = QtWidgets.QLineEdit()
        self.episodeLineEdit.setEnabled(False)
        self.statusLabel = QtWidgets.QLabel()
        self.layout.addWidget(self.episodeLabel)
        self.layout.addWidget(self.episodeLineEdit)
        self.layout.addWidget(self.statusLabel)


        # buttons
        # buttons = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Yes | QtWidgets.QDialogButtonBox.Cancel, QtCore.Qt.Horizontal, self)
        self.buttonBox = QtWidgets.QDialogButtonBox()
        self.buttonBox.addButton('Create', QtWidgets.QDialogButtonBox.AcceptRole)
        self.buttonBox.addButton('Cancel', QtWidgets.QDialogButtonBox.RejectRole)

        # signals
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        # set layout
        self.layout.setSpacing(4)
        self.layout.setContentsMargins(9, 9, 9, 9)

        self.layout.addWidget(self.buttonBox)
        self.setMinimumSize(260, 100)

        self.init_signals()
        self.init_functions()

    def init_signals(self): 
        self.episodeLineEdit.textChanged.connect(self.name_checked)

    def init_functions(self):
        """ populate form with sg data """
        self.sgThread = sg_thread.GetEpisodes(self.projectEntity)
        self.sgThread.value.connect(self.populate_entity)
        self.sgThread.finished.connect(self.fetch_finished)
        self.sgThread.start()

    def populate_entity(self, epEntities):
        # set caches
        self.epEntities = epEntities
        self.episodes = sorted([a['code'] for a in epEntities])
        self.episodeLineEdit.setEnabled(True)

    def fetch_finished(self): 
        pass 

    def name_checked(self): 
        self.episodeName = self.episodeLineEdit.text()

        if self.episodeName in self.episodes: 
            self.statusLabel.setText('"%s" already exists' % self.episodeName)
            self.statusLabel.setStyleSheet(Color.bgRed)
            self.buttonBox.setEnabled(False)

        else: 
            self.statusLabel.setText('')
            self.statusLabel.setStyleSheet('')
            self.buttonBox.setEnabled(True)

    # static method
    @staticmethod
    def show(projectEntity, parent=None):
        from rf_utils.sg import sg_process
        createEpisodeDialog = CreateEpisode(projectEntity, parent=parent)
        result = createEpisodeDialog.exec_()

        if result:
            projectEntity = result.projectEntity
            episodeName = result.episodeName
            return sg_process.create_episode(projectEntity, episodeName)



# usage
# custom_dialog.Dialog.complete('Complete', 'Complete')
