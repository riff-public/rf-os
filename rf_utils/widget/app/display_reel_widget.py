import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui


module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from .. import display_widget
from .. import filmstrip_widget

class DisplayImages(QtWidgets.QWidget) :
    deleted = QtCore.Signal(list)
    dropped = QtCore.Signal(list)
    clicked = QtCore.Signal(str)
    def __init__(self) :
        super(DisplayImages, self).__init__()
        
        self.allLayout = QtWidgets.QHBoxLayout()
        
        # display 
        self.displayWidget = display_widget.Display()
        self.allLayout.addWidget(self.displayWidget)
        self.set_drop_area()

        # thumb
        self.reelWidget = filmstrip_widget.DisplayReel()
        self.allLayout.addWidget(self.reelWidget)
        self.setLayout(self.allLayout)

        # layout
        self.allLayout.setStretch(0, 8)
        self.allLayout.setStretch(1, 1)

        # imgs extension
        self.imgExts = ['.jpg', '.png']
        self.multipleFile = False

        self.init_signals()

    def init_signals(self): 
        self.displayWidget.multipleDropped.connect(self.dropped_event)
        self.reelWidget.clicked.connect(self.preview)
        self.reelWidget.deleted.connect(self.delete_reel)

    def item_deleted(self, items):
        if items:
            self.deleted.emit(items)

    def multiple_file(self, state): 
        self.multipleFile = state
        self.reelWidget.displayListWidget.setVisible(state)
        self.allLayout.setStretch(1, 0)
        if state: 
             self.allLayout.setStretch(1, 1)

    def set_drop_area(self): 
        self.displayWidget.set_label('< Drop Images Here >')

    def delete_reel(self, del_data):
        self.set_drop_area()
        self.deleted.emit(del_data)

    def dropped_event(self, paths): 
        if not self.multipleFile: 
            self.reelWidget.clear()
            paths = [paths[-1]]

        valid_paths = []
        for path in paths:
            # folder dropped 
            if os.path.isdir(path): 
                imgs = self.find_imgs(path)
                if imgs: 
                    for img in imgs: 
                        self.reelWidget.add_item(img)
                        valid_paths.append(img)
            # single file 
            else: 
                self.reelWidget.add_item(path)
                valid_paths.append(path)

        # display and select the last dropped item
        self.displayWidget.set_display(valid_paths[-1])
        self.reelWidget.displayListWidget.setCurrentRow(self.reelWidget.displayListWidget.count()-1)

        # emit signal 
        self.dropped.emit(self.get_all_items())

    def preview(self, path): 
        self.displayWidget.clear()
        if path:
            self.displayWidget.set_display(path)

    def find_imgs(self, dirPath): 
        files = list_file(dirPath)
        imgFiles = []

        for imgFile in sorted(files): 
            name, ext = os.path.splitext(imgFile)
            if ext in self.imgExts: 
                imgFiles.append(imgFile)

        return imgFiles

    def get_all_items(self): 
        return self.reelWidget.get_all_items()

    def clear(self): 
        self.reelWidget.clear()
        self.displayWidget.clear()
        self.set_drop_area()
        
        
def list_file(path): 
    return [os.path.join(path, d) for d in os.listdir(path) if os.path.isfile(os.path.join(path, d))]