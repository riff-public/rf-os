#!/usr/bin/env python
# -- coding: utf-8 --
import os
import sys
from datetime import datetime
from collections import OrderedDict

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

from rf_app.publish.asset import task_dependency
from rf_utils import icon
from rf_utils import mesg_utils
import status_widget
reload(task_dependency)
reload(mesg_utils)


class TaskDependencyUI(QtWidgets.QWidget):
    """docstring for TaskDependencyUI"""
    currentIndexChanged = QtCore.Signal(str)
    def __init__(self, parent=None):
        super(TaskDependencyUI, self).__init__(parent=parent)
        self.allLayout = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel('Next Dept')
        self.comboBox = QtWidgets.QComboBox()

        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.comboBox)

        self.allLayout.setStretch(0, 0)
        self.allLayout.setStretch(1, 2)

        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)


class TaskDependencyWidget(TaskDependencyUI):
    """docstring for TaskDependencyWidget"""
    message = QtCore.Signal(str)
    def __init__(self, parent=None):
        super(TaskDependencyWidget, self).__init__(parent=parent)
        self.warningColor = 'background-color: rgb(140, 0, 0);'
        self.okColor = 'background-color: rgb(0, 140, 0);'
        
    def update(self, contextPathInfo, step, task): 
        taskEntities = task_dependency.get_dependency(contextPathInfo, step, task)
        affectedTasks = self.check_status(taskEntities)
        warningMessages = mesg_utils.downstream_task_warning(affectedTasks)
        self.add_status(taskEntities, affectedTasks)

        return warningMessages

    def check_status(self, taskEntities): 
        # any status that's not in notStartTasks will be affected 
        affectedTasks = [a for a in taskEntities if a['sg_status_list'] not in task_dependency.notStartTasks]
        return affectedTasks

    def add_status(self, taskEntities, affectedTasks): 
        self.comboBox.clear()
        self.comboBox.setStyleSheet('')
        warningMessages = ''

        if affectedTasks: 
            self.comboBox.addItem('Already started')
            warningMessages = mesg_utils.downstream_task_warning(affectedTasks)
            # icon 
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(icon.needFix), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            self.comboBox.setItemIcon(0, iconWidget)  
            self.comboBox.setStyleSheet(self.warningColor)
            self.message.emit(warningMessages['message'])

        else: 
            self.comboBox.addItem('Not start yet')
            self.comboBox.setStyleSheet(self.okColor)

        for row, taskEntity in enumerate(taskEntities): 
            row = row + 1
            content = taskEntity['content']
            status = taskEntity['sg_status_list']
            assignees = taskEntity['task_assignees']
            users = [a['name'] for a in assignees] if assignees else []
            iconName = status_widget.Icon.statusMap[status]['icon']
            iconPath = '%s/%s' % (status_widget.Icon.path, iconName)

            display = '%s - %s' % (content, (',').join(users))
            self.comboBox.addItem(display)
            self.comboBox.setItemData(row, taskEntity, QtCore.Qt.UserRole)

            # icon 
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(iconPath), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            self.comboBox.setItemIcon(row, iconWidget)  
