from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

class PauseSignals(object): 
    def __init__(self, widgets): 
        self.widgets = widgets

    def __enter__(self, *args): 
        for widget in self.widgets: 
            widget.blockSignals(True)

    def __exit__(self, *args): 
        for widget in self.widgets: 
            widget.blockSignals(False)