uiName = 'LookWidget'
import os
import sys
import traceback
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import file_comboBox

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

notStartwith = ['.']

from rf_maya.rftool.utils import maya_utils
from rf_maya.rftool.shade import shade_utils
reload(shade_utils)
from rf_utils import file_utils
from rf_utils.context import context_info
from rf_utils import register_entity
from rf_utils.widget import dialog
reload(register_entity)
from rf_utils import icon

import maya.cmds as mc


class QCustomQWidget (QtWidgets.QWidget):
    def __init__ (self, parent = None):
        super(QCustomQWidget, self).__init__(parent)
        self.textQVBoxLayout = QtWidgets.QVBoxLayout()
        self.textUpQLabel    = QtWidgets.QLabel()
        self.textDownQLabel  = QtWidgets.QLabel()
        self.textQVBoxLayout.addWidget(self.textUpQLabel)
        self.textQVBoxLayout.addWidget(self.textDownQLabel)
        self.allQHBoxLayout  = QtWidgets.QHBoxLayout()
        self.iconQLabel      = QtWidgets.QLabel()
        self.allQHBoxLayout.addWidget(self.iconQLabel, 0)
        self.allQHBoxLayout.addLayout(self.textQVBoxLayout, 1)
        self.setLayout(self.allQHBoxLayout)
        #setStyleSheet
        self.textUpQLabel.setStyleSheet('''   color: rgb(0, 0, 255); ''')
        self.textDownQLabel.setStyleSheet('''    color: rgb(255, 0, 0);   ''')

    def setTextUp (self, text):
        self.textUpQLabel.setText(text)

    def setTextDown (self, text):
        self.textDownQLabel.setText(text)

    def setIcon (self, imagePath):
        pixmap = QtGui.QPixmap(imagePath)
        pixmap2 = pixmap.scaled(160, 160, QtCore.Qt.KeepAspectRatio)
        self.iconQLabel.setPixmap(pixmap2)



class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.listLayout = QtWidgets.QVBoxLayout()
        self.material_listWidget = QtWidgets.QListWidget()
        # self.material_listWidget.setViewMode(QtWidgets.QListView.IconMode)

        self.apply_pushButton = QtWidgets.QPushButton()
        self.apply_pushButton.setText('Apply')
        self.fix_pushButton = QtWidgets.QPushButton()
        self.fix_pushButton.setText('Fix')
        self.refresh_pushButton = QtWidgets.QPushButton()
        self.refresh_pushButton.setText('refresh')

        self.listLayout.addWidget(self.material_listWidget)
        self.listLayout.addWidget(self.apply_pushButton)
        self.listLayout.addWidget(self.fix_pushButton)
        self.listLayout.addWidget(self.refresh_pushButton)
        self.allLayout.addLayout(self.listLayout)

        self.setLayout(self.allLayout)
        self.set_default()

    def set_default(self):
        pass

class LookWidget(Ui):
    """docstring for LookWidget"""
    def __init__(self, dccHook=None, parent=None):
        super(LookWidget, self).__init__(parent=parent)
        self.regInfo = None
        self.dccHook = dccHook
        self.init_signals()


    def init_signals(self):
        self.apply_pushButton.clicked.connect(self.reconnect_shader)
        self.fix_pushButton.clicked.connect(self.fix_ref_shade)
        self.refresh_pushButton.clicked.connect(self.update_input_selected)

    def update_input_selected(self):
        refPath = mc.referenceQuery(mc.ls(sl=True)[0], f=True)
        entity = context_info.ContextPathInfo(path=refPath)
        self.update_input(entity)

    def update_input(self, entity):
        self.entity = entity
        self.regInfo = register_entity.Register(entity)
        self.check_mat_reference(self.regInfo)

    def check_mat_reference(self, regInfo):
        self.material_listWidget.clear()
        obj = mc.ls(sl=True)[0]
        refPath = mc.referenceQuery(obj, f=True)
        self.asset = context_info.ContextPathInfo(path=refPath)
        self.reg = regInfo
        all_look = self.reg.get_looks(step='texture', res='md')

        for index,look in enumerate(all_look):
            preview = self.reg.get.preview(step='texture', look=look)
            mtrPath = self.reg.get.mtr(look=look)['heroFile']
            self.set_image (obj, refPath, preview, look, mtrPath)

    def set_image(self, obj, refPath, preview_path, look, mtrPath):
        self.supportFormat =['jpg', 'png', 'JPG', 'PNG']
        name, ext = os.path.splitext(preview_path)
        iconPath = preview_path
        # item.setText(look)
        self.myQCustomQWidget = QCustomQWidget()
        self.myQCustomQWidget.setTextDown(look)
        self.myQCustomQWidget.setIcon(iconPath)
        self.myQListWidgetItem = QtWidgets.QListWidgetItem(self.material_listWidget)
        self.myQListWidgetItem.setSizeHint(self.myQCustomQWidget.sizeHint())
        self.material_listWidget.addItem(self.myQListWidgetItem)
        self.material_listWidget.setItemWidget(self.myQListWidgetItem,self.myQCustomQWidget)

        # icons = QtGui.QIcon(iconPath)
        # item.setIcon(icons)
        data = []
        data.append(mtrPath)
        data.append(look)
        self.myQListWidgetItem.setData(QtCore.Qt.UserRole, data)

    def reconnect_shader(self):
        index = self.material_listWidget.currentIndex()
        data = index.data( QtCore.Qt.UserRole)
        mtrPath, look = data
        currentObj = mc.ls(sl=True)

        nameSpaceObj = mc.referenceQuery(currentObj[0], namespace=True)
        ####test_mat = referencename mat
        self.asset.context.update(look=look)
        shadeNamespace = self.asset.mtr_namespace
        all_namespace = maya_utils.list_all_namespaces()
        if shadeNamespace in all_namespace:
            mc.file(loadReference= "%sRN"%(shadeNamespace))

        for ix in currentObj:
            shade_utils.apply_ref_shade(mtrPath, shadeNamespace, nameSpaceObj)
            shadeFilepath, shadeName, geoNameSpace = shade_utils.fix_reference_shader(ix)
            shade_utils.apply_ref_shade(mtrPath, shadeNamespace, geoNameSpace)
        mc.select(currentObj)

    def fix_ref_shade(self):
        index = self.material_listWidget.currentIndex()
        data = index.data( QtCore.Qt.UserRole)
        mtrPath, look = data
        currentObj = mc.ls(sl=True)
        nameSpaceObj = mc.referenceQuery(currentObj[0], namespace=True)

        self.asset.context.update(look=look)
        shadeNamespace = self.asset.mtr_namespace
        all_namespace = maya_utils.list_all_namespaces()
        if shadeNamespace in all_namespace:
            mc.file(loadReference= "%sRN"%(shadeNamespace))

        for ix in currentObj:
            shade_utils.reconnect_selected_shader(ix)
        mc.select(currentObj)

    def set_ui(self):
        pass

class Main(QtWidgets.QMainWindow):
    """docstring for Main"""
    def __init__(self, parent=None):
        super(Main, self).__init__(parent=parent)
        self.ui = LookWidget(self)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

def show():
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = Main(maya_win.getMayaWindow())
    myApp.show()
    return myApp
