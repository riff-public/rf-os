uiName = 'sgInputWidgetUI'
import os
import sys
import traceback
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import file_comboBox

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

notStartwith = ['.']


from rf_utils.widget import file_widget
from rf_utils.sg import sg_thread
from rf_utils.widget import sg_version_widget
from rf_utils import file_utils
from rf_utils.context import context_info
reload(sg_version_widget)
from rf_utils import icon
reload(file_widget)

class Color:
    green = [0, 100, 0]

class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.title_layout = QtWidgets.QHBoxLayout()

        self.label = QtWidgets.QLabel('Step')
        self.step_comboBox = QtWidgets.QComboBox()
        self.label2 = QtWidgets.QLabel('Res')
        self.res_comboBox = QtWidgets.QComboBox()
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.title_layout.addWidget(self.label)
        self.title_layout.addWidget(self.step_comboBox)
        self.title_layout.addWidget(self.label2)
        self.title_layout.addWidget(self.res_comboBox)
        self.title_layout.addItem(spacerItem)
        self.title_layout.setStretch(1, 1)
        self.title_layout.setStretch(2, 1)

        self.allLayout.addLayout(self.title_layout)

        # version
        self.versionWidget = sg_version_widget.SGVersionListWidget()
        self.allLayout.addWidget(self.versionWidget)

        self.mode_layout = QtWidgets.QHBoxLayout()
        self.file_radioButton = QtWidgets.QRadioButton('Files')
        self.image_radioButton = QtWidgets.QRadioButton('Images')
        self.heroCheckBox = QtWidgets.QCheckBox('Hero')
        self.recommendedCheckBox = QtWidgets.QCheckBox('Recommend')
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        # mode layout
        self.mode_layout.addWidget(self.file_radioButton)
        self.mode_layout.addWidget(self.image_radioButton)
        self.mode_layout.addWidget(self.heroCheckBox)
        self.mode_layout.addWidget(self.recommendedCheckBox)
        self.mode_layout.addItem(spacerItem1)
        self.allLayout.addLayout(self.mode_layout)

        # file listWidget
        self.fileWidget = file_widget.FileListWidget()
        self.allLayout.addWidget(self.fileWidget)

        self.choice_frame = QtWidgets.QFrame()
        self.frameLayout = QtWidgets.QHBoxLayout(self.choice_frame)

        self.import_radioButton = QtWidgets.QRadioButton('Import', self.choice_frame)
        self.reference_radioButton = QtWidgets.QRadioButton('Reference', self.choice_frame)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.frameLayout.addWidget(self.import_radioButton)
        self.frameLayout.addWidget(self.reference_radioButton)
        self.frameLayout.addItem(spacerItem2)
        self.allLayout.addWidget(self.choice_frame)

        self.line = QtWidgets.QFrame()
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.allLayout.addWidget(self.line)

        # button
        self.action_pushButton = QtWidgets.QPushButton('Import')
        self.allLayout.addWidget(self.action_pushButton)

        self.setLayout(self.allLayout)
        self.set_default()

    def set_default(self):
        self.heroCheckBox.setChecked(True)
        self.recommendedCheckBox.setChecked(True)
        self.file_radioButton.setChecked(True)
        self.import_radioButton.setChecked(True)
        self.allLayout.setSpacing(4)
        self.allLayout.setContentsMargins(0, 0, 0, 0)

class SGInputWidget(Ui):
    """docstring for SGInputWidget"""
    def __init__(self, sg, dccHook=None, parent=None):
        super(SGInputWidget, self).__init__(parent=parent)
        self.sg = sg
        self.dccHook = dccHook
        self.init_signals()

    def init_signals(self):
        self.step_comboBox.currentIndexChanged.connect(self.option_change)
        self.res_comboBox.currentIndexChanged.connect(self.option_change)
        self.versionWidget.itemSelected.connect(self.list_data)
        self.file_radioButton.clicked.connect(self.list_data)
        self.image_radioButton.clicked.connect(self.list_data)
        self.heroCheckBox.stateChanged.connect(self.list_data)
        self.recommendedCheckBox.stateChanged.connect(self.list_data)

        # button
        self.import_radioButton.clicked.connect(self.set_action)
        self.reference_radioButton.clicked.connect(self.set_action)

        # file signal
        self.action_pushButton.clicked.connect(self.action)

    def block_step_res_signal(self, state):
        self.step_comboBox.blockSignals(state)
        self.res_comboBox.blockSignals(state)

    def update_input(self, sgEntity):
        self.fetchVersionThread = sg_thread.FetchVersions(sgEntity)
        self.fetchVersionThread.value.connect(self.update_version)
        self.fetchVersionThread.start()
        self.set_loading()

    def set_loading(self):
        self.versionWidget.listWidget.addItem('Loading ...')

    def option_change(self):
        self.set_version_list()

    def update_version(self, sgEntities):
        """ list of versions """
        self.sgEntities = self.modify_sgvalue(sgEntities)
        self.set_step()
        self.set_res()
        self.set_version_list()

    def set_step(self):
        self.block_step_res_signal(True)
        steps = list(set([a.get('sg_task.Task.step.Step.code') for a in self.sgEntities]))
        sortedSteps = self.get_sorted_step(steps)
        self.step_comboBox.clear()
        self.step_comboBox.addItems(sortedSteps)

        self.block_step_res_signal(False)

    def set_res(self):
        self.block_step_res_signal(True)

        dataList = list(set([a.get('sg_data') for a in self.sgEntities]))
        allRes = list(set([eval(a).get('res') for a in dataList if a]))
        self.res_comboBox.clear()
        for i, res in enumerate(allRes):
            display = res if res else '- No Data -'
            self.res_comboBox.addItem(display)
            self.res_comboBox.setItemData(i, res)

        self.block_step_res_signal(False)


    def set_version_list(self):
        step = str(self.step_comboBox.currentText()) or ''
        res = str(self.res_comboBox.itemData(self.res_comboBox.currentIndex())) or ''
        displayEntities = [a for a in self.sgEntities if a['sg_task.Task.step.Step.code'] == step] if step else self.sgEntities
        displayEntities = [a for a in displayEntities if res in eval(a['sg_data']).get('res')]
        self.versionWidget.clear()
        self.fileWidget.clear()
        self.versionWidget.add_items(displayEntities)

    def modify_sgvalue(self, sgEntities):
        newList = []
        for i, sgEntity in enumerate(sgEntities):
            sgEntity['icon'] = icon.sgNa
            if not sgEntity['sg_data']:
                sgEntity['sg_data'] = '{"res": ""}'
            newList.append(sgEntity)
        return newList

    def list_data(self, sgEntity=None):
        sgEntity = self.versionWidget.get_data()
        browsePath = self.get_output_path(sgEntity)

        if os.path.exists(browsePath): 
            files = ['/'.join([browsePath, a]) for a in file_utils.list_file(browsePath)]
            recFile = self.get_recommended_file(sgEntity)
            showRec = self.recommendedCheckBox.isChecked()

            self.list_files(files)
            self.fileWidget.set_bg_color(recFile, Color.green) if showRec else None


    def get_sorted_step(self, steps):
        newOrder = []
        preferedOrder = ['Model', 'Rig', 'Texture', 'Lookdev']

        for step in preferedOrder:
            if step in steps:
                newOrder.append(step)

        for step in steps:
            if not step in newOrder:
                newOrder.append(step)

        return newOrder

    def get_recommended_file(self, sgEntity):
        recommendedFile = sgEntity.get('sg_recommended_file') or ''
        recFile = context_info.RootPath(recommendedFile).abs_path()
        return recFile


    def get_output_path(self, sgEntity):
        # get ui
        hero = self.heroCheckBox.isChecked()
        fileOption = self.file_radioButton.isChecked()
        imageOption = self.image_radioButton.isChecked()

        # sg data
        publishPath = sgEntity.get('sg_path_to_publish')
        heroPath = sgEntity.get('sg_path_to_hero')
        browsePath = heroPath if hero else publishPath
        browsePath = context_info.RootPath(browsePath).abs_path()

        # hard coded to check for output / outputImg folder
        groupDir = 'output' if fileOption else 'outputImg' if imageOption else ''
        path = ('/').join([browsePath, groupDir])
        return path

    def list_files(self, files):
        self.fileWidget.clear()
        self.fileWidget.add_files(files) if files else None

    def set_action(self):
        if self.import_radioButton.isChecked():
            self.action_pushButton.setText('Import')
        if self.reference_radioButton.isChecked():
            self.action_pushButton.setText('Reference')

    def action(self):
        path = self.fileWidget.selected_file()
        sgEntity = self.versionWidget.get_data()
        fileType = self.guess_file_type(path)

        print path
        print fileType
        print sgEntity.get('entity.Asset.code')
        return

        if self.import_radioButton.isChecked():
            self.dccHook.import_file(path)

        if self.reference_radioButton.isChceked():
            namespace = sgEntity.get('entity.Asset.code')
            self.dccHook.reference_file(namespace, path)

        if self.action == self.importMtr:
            self.dccHook.import_mtr(path)

    def guess_file_type(self, filepath):
        """ hardcoded """
        keys = {'_mtr_': 'mayaMtr', '_gpu_': 'gpu'}
        exts = {'.ma': 'maya', '.abc': 'cache'}
        matchKey = [a for a in keys if a in os.path.basename(filepath)]
        matchExt = [a for a in exts if a == os.path.splitext(filepath)[-1]]
        fileType = keys[matchKey[0]] if matchKey else exts[matchExt[0]] if matchExt else None
        return fileType


class Main(QtWidgets.QMainWindow):
    """docstring for Main"""
    def __init__(self, parent=None):
        super(Main, self).__init__(parent=parent)
        self.ui = SGInputWidget(self)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

def show():
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = Main(maya_win.getMayaWindow())
    myApp.show()
    return myApp

