import os
import sys
import traceback
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from . import file_comboBox
from . import completer
sg = None
module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

notStartwith = ['.']

class TaskComboBox(QtWidgets.QWidget):
    currentIndexChanged = QtCore.Signal(dict)
    def __init__(self, sg, layout=None, parent=None) :
        super(TaskComboBox, self).__init__()
        # data
        self.sg = sg

        # layout
        layout = QtWidgets.QVBoxLayout() if not layout else layout
        self.allLayout = layout
        self.comboBox = QtWidgets.QComboBox()
        self.label = QtWidgets.QLabel('Task')

        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.comboBox)
        self.setLayout(self.allLayout)

        self.allLayout.setStretch(0, 1)
        self.allLayout.setStretch(1, 3)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.allLayout.setSpacing(0)

        self.comboBox.currentIndexChanged.connect(self.emit_signal)


    def activate(self, project, entityType, entityName, step, entityAssetType= ''):
        """ list task (connect signal here) """
        entity = self.entity(project, entityType, entityName, entityAssetType)
        if entity:
            tasks = self.get_tasks(entity, step)
            self.comboBox.clear()
            self.comboBox.addItem('-- Select Task --')
            self.comboBox.setItemData(0, None, QtCore.Qt.UserRole)

            for row, task in enumerate(tasks):
                self.comboBox.addItem(task.get('content'))
                self.comboBox.setItemData(row+1, task, QtCore.Qt.UserRole)

            return True
        else:
            return False


    def entity(self, project, entityType, entityName, entityAssetType= ''):
        """ find entity """
        filters = [['project.Project.name', 'is', project], ['code', 'is', entityName]]
        if entityAssetType:
            filters.append(['sg_asset_type', 'is', entityAssetType])
        fields = ['code', 'id', 'project']
        sgEntity = self.sg.find_one(entityType, filters, fields)
        return sgEntity

    def get_tasks(self, entity, step):
        """ list task """
        filters = [['entity', 'is', entity], ['project', 'is', entity.get('project')], ['step.Step.code', 'is', step]]
        fields = ['content', 'id', 'entity', 'project', 'step', 'sg_name', 'sg_resolution', 'sg_status_list', 'start_date', 'due_date']
        return self.sg.find('Task', filters, fields)

    def current_item(self):
        return self.comboBox.itemData(self.comboBox.currentIndex(), QtCore.Qt.UserRole)

    def emit_signal(self):
        data = self.comboBox.itemData(self.comboBox.currentIndex(), QtCore.Qt.UserRole)
        self.currentIndexChanged.emit(data)

    def clear(self):
        self.comboBox.clear()

    def set_label_color(self, colors):
        self.label.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))



class SGComboBox(QtWidgets.QWidget):
    """docstring for SGComboBox"""
    currentIndexChanged = QtCore.Signal(dict)
    noItem = QtCore.Signal(bool)

    def __init__(self, displayAttr, parent=None):
        super(SGComboBox, self).__init__(parent=parent)
        self.displayAttr = displayAttr
        self.currentItem = ''

        self.allLayout = QtWidgets.QVBoxLayout()
        self.comboBox = QtWidgets.QComboBox()
        self.allLayout.addWidget(self.comboBox)
        self.setLayout(self.allLayout)

        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.allLayout.setSpacing(0)

        self.init_signals()

    def init_signals(self):
        self.comboBox.currentIndexChanged.connect(self.item_changed)

    def item_changed(self):
        data = self.current_item()
        if data:
            self.currentIndexChanged.emit(data)

    def set_searchable(self, state):
        if state:
            self.comboBox.setEditable(True)
            # searchCompleter = completer.CustomQCompleter()
            # searchCompleter.setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
            # searchCompleter.setCaseSensitivity(QtCore.Qt.CaseSensitive)
            # searchCompleter.setModel(self.comboBox.model())
            # self.comboBox.setCompleter(searchCompleter)
            self.comboBox.completer().setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
        else:
            self.comboBox.setEditable(False)

    def add_item(self, sgEntitity):
        self.comboBox.addItem(sgEntitity.get(self.displayAttr))
        index = self.comboBox.count() - 1
        self.comboBox.setItemData(index, sgEntitity, QtCore.Qt.UserRole)

    def add_items(self, sgEntities):
        self.comboBox.clear()
        sortData = self.sort_data(sgEntities)
        currentIndex = 0

        for index, sgEntity in enumerate(sortData):
            self.add_item(sgEntity)
            if sgEntity.get(self.displayAttr) == self.currentItem:
                currentIndex = index

        # set default
        self.comboBox.setCurrentIndex(currentIndex)

        # if data, emit signal
        if sortData:
            self.currentIndexChanged.emit(self.current_item())
        # show no item text
        else:
            self.comboBox.addItem('--- No Item ---')
            self.noItem.emit(True)

    def clear(self):
        self.comboBox.clear()

    def current_item(self):
        return self.comboBox.itemData(self.comboBox.currentIndex(), QtCore.Qt.UserRole)

    def current_text(self):
        return str(self.comboBox.currentText())


    def sort_data(self, sgEntities):
        data = dict()
        orderData = []

        sortList = sorted([a.get(self.displayAttr) for a in sgEntities])
        for entity in sgEntities:
            data[entity.get(self.displayAttr)] = entity

        for display in sortList:
            orderData.append(data[display])

        return orderData

    def set_current_item(self, text):
        items = self.get_items()
        index = items.index(text) if text in items else 0
        self.comboBox.setCurrentIndex(index)

    def get_items(self):
        items = [self.comboBox.itemText(a) for a in range(self.comboBox.count())]
        return items



class TagComboBox(QtWidgets.QDialog):
    """docstring for TagComboBox"""
    def __init__(self, sg=None, parent=None):
        super(TagComboBox, self).__init__(parent)
        self.sg = sg
        self.allLayout = QtWidgets.QVBoxLayout()
        self.comboBox = ComboBoxExpanded()
        self.comboBox.setEditable(True)
        self.allLayout.addWidget(self.comboBox)
        self.setLayout(self.allLayout)
        self.list_tag()

    def list_tag(self):
        tags = self.get_tags()
        self.comboBox.clear()

        for i, tag in enumerate(tags):
            self.comboBox.addItem(tag['name'])
            self.comboBox.setItemData(i, tag, QtCore.Qt.UserRole)

    def get_tags(self):
        filters = []
        fields = ['name', 'id']
        tags = self.sg.find('Tag', filters, fields)
        return tags

    @staticmethod
    def show(sg):
        tag = TagComboBox(sg)
        tag.exec_()



class ComboBoxExpanded(QtWidgets.QComboBox):
    def __init__(self, parent=None):
        super(ComboBoxExpanded, self).__init__(parent)
        self.editTextChanged.connect(self.handleItemPressed)
        self._changed = False

    def handleItemPressed(self, index):
        if not self._changed:
            super(ComboBoxExpanded, self).showPopup()
        self._changed = True

    def hidePopup(self):
        if not self._changed:
            super(ComboBoxExpanded, self).hidePopup()
        self._changed = False

