import os 
import sys 
from datetime import datetime
from collections import OrderedDict

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

try:
    moduleDir = sys._MEIPASS
except Exception:
    moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')

class Icon: 
    
    path = '{}/icons/task_status'.format(moduleDir)
    statusMap = OrderedDict()
    statusMap['All'] = {'display': 'All', 'icon': ''} 
    statusMap['wtg'] = {'display': 'Wating to Start', 'icon': 'wtg_icon.png'} 
    statusMap['rdy'] = {'display': 'Ready to Start', 'icon': 'rdy_icon.png'} 
    statusMap['ip'] = {'display': 'In Progress', 'icon': 'ip_icon.png'} 
    statusMap['rev'] = {'display': 'Daily', 'icon': 'review_icon.png'} 
    statusMap['pub'] = {'display': 'Pending Publish', 'icon': 'pub_icon.png'} 
    statusMap['fix'] = {'display': 'Need Fix', 'icon': 'fix_icon.png'} 
    statusMap['hld'] = {'display': 'On Hold', 'icon': 'hld_icon.png'} 
    statusMap['apr'] = {'display': 'Approved', 'icon': 'aprv_icon.png'} 
    statusMap['p_aprv'] = {'display': 'Pending Approve', 'icon': 'p_aprv_icon.png'} 
    statusMap['omt'] = {'display': 'Omit', 'icon': 'omt_icon.png'}
    statusMap['ardy'] = {'display': 'Anim Ready', 'icon': 'ardy_icon.png'}
    statusMap['clsd'] = {'display': 'Closed', 'icon': 'clsd_icon.png'}
    statusMap['cmpt'] = {'display': 'Complete', 'icon': 'cmpt_icon.png'}
    statusMap['opn'] = {'display': 'Open', 'icon': 'rdy_icon.png'}
    statusMap['wip'] = {'display': 'Work In Progress', 'icon': 'wip_icon.png'}
    statusMap['dlvr'] = {'display': 'Delivered', 'icon': 'dlvr_icon.png'}
    statusMap['out'] = {'display': 'With Outsourcing', 'icon': 'out_icon.png'}
    statusMap['pxy'] = {'display': 'Proxy', 'icon': 'pxy_icon.png'}
    statusMap['pblm'] = {'display': 'Problem', 'icon': 'pblm_icon.png'}
    statusMap['queue'] = {'display': 'Queue', 'icon': 'queue_icon.png'}
    statusMap['vwd'] = {'display': 'Viewed', 'icon': 'vwd_icon.png'}
    statusMap['recd'] = {'display': 'Received', 'icon': 'recd_icon.png'}

    limit = {'Task': ['rev', 'apr'], 
            'Fx': ['rev', 'ardy', 'apr']}

    availableMaps = {'Task': ['wtg', 'rdy', 'ip', 'rev', 'pub', 'fix', 'hld', 'p_aprv', 'apr', 'dlvr', 'out', 'pxy', 'omt', 'pblm', 'queue'],
            'Version': ['wip', 'rev', 'vwd', 'fix', 'apr', 'ardy', 'p_aprv']}

class NoteIcon: 
    path = '{}/icons/note_status'.format(moduleDir)
    statusMap = OrderedDict()
    statusMap['clsd'] = {'display': 'Closed', 'icon': 'clsd_icon.png'} 
    statusMap['cmpt'] = {'display': 'Complete', 'icon': 'cmpt_icon.png'} 
    statusMap['ip'] = {'display': 'In Progress', 'icon': 'ip_icon.png'} 
    statusMap['opn'] = {'display': 'Open', 'icon': 'opn_icon.png'} 

    availableMaps = {'Brief': ['opn', 'clsd']}

class StatusComboBox(QtWidgets.QWidget):
    """docstring for StatusComboBox"""
    currentIndexChanged = QtCore.Signal(str)
    def __init__(self, layout=None, parent=None):
        super(StatusComboBox, self).__init__(parent=parent)
        self.allLayout = QtWidgets.QHBoxLayout() if not layout else layout
        self.label = QtWidgets.QLabel()
        self.comboBox = QtWidgets.QComboBox()
                
        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.comboBox)

        self.allLayout.setStretch(0, 1)
        self.allLayout.setStretch(1, 3)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)

        self.comboBox.currentIndexChanged.connect(self.emit_signal)
        self.limit = None


    def set_icon(self): 
        row = 0 
        allStatuses = [a for a in Icon.statusMap]
        limitStatus = Icon.limit.get(self.limit, allStatuses)
        self.comboBox.clear()

        for code, values in Icon.statusMap.items(): 
            if code in limitStatus: 
                display = values['display']
                iconPath = '{}/{}'.format(Icon.path, values['icon'])
                self.comboBox.addItem(display)
                self.comboBox.setItemData(row, code, QtCore.Qt.UserRole)
                
                # icon 
                iconWidget = QtGui.QIcon()
                iconWidget.addPixmap(QtGui.QPixmap(iconPath), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                self.comboBox.setItemIcon(row, iconWidget)

                row+=1

    def emit_signal(self, index): 
        self.currentIndexChanged.emit(self.comboBox.itemData(index, QtCore.Qt.UserRole))

    def current_item(self): 
        return self.comboBox.itemData(self.comboBox.currentIndex(), QtCore.Qt.UserRole)

class TaskStatusWidget(StatusComboBox):
    """docstring for TaskStatusWidget"""
    def __init__(self, layout=None, parent=None):
        super(TaskStatusWidget, self).__init__(layout=layout, parent=parent)
        self.limit = 'Task'
        self.set_icon()


class StatusListWidget(QtWidgets.QWidget):
    """docstring for StatusListWidget"""
    item_selected = QtCore.Signal(object)

    def __init__(self, layout=None, parent=None):
        super(StatusListWidget, self).__init__(parent=parent)
        self.allLayout = QtWidgets.QHBoxLayout() if not layout else layout
        self.label = QtWidgets.QLabel()
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
                
        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.listWidget)

        self.allLayout.setStretch(0, 1)
        self.allLayout.setStretch(1, 3)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)

        self.all_filter = 'All'

        self.listWidget.itemSelectionChanged.connect(self.status_selected)
        self.limit = None
        self.init_all_item()

    def init_all_item(self): 
        self.all_item = QtWidgets.QListWidgetItem()
        self.all_item.setText(self.all_filter)
        self.all_item.setData(QtCore.Qt.UserRole, self.all_filter)

    def status_selected(self): 
        self.item_selected.emit(self.selected_filters)

    def add_filter(self, code): 
        iconPath = '%s/%s' % (Icon.path, Icon.statusMap[code]['icon'])
        item = QtWidgets.QListWidgetItem()
        item.setText(Icon.statusMap[code]['display'])
        item.setData(QtCore.Qt.UserRole, code)
        self.listWidget.addItem(item)
        
        if os.path.exists(iconPath): 
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(iconPath), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            item.setIcon(iconWidget)
        return item

    def clear(self): 
        self.listWidget.clear()

    def selected_filters(self): 
        return [a.data(QtCore.Qt.UserRole) for a in self.listWidget.selectedItems()]

    def all_items(self, *kwargs): 
        return [self.listWidget.item(a) for a in range(self.listWidget.count())]

    def set_all_item(self): 
        item = self.add_filter(self.all_filter)
        row = self.listWidget.row(item)
        self.listWidget.takeItem(row)
        self.listWidget.insertItem(0, item)

    def select_item(self, keyword): 
        items = self.all_items()
        labels = [str(a.text()) for a in items]
        datas = [a.data(QtCore.Qt.UserRole) for a in items]
        if keyword in labels: 
            index = labels.index(keyword)
            self.listWidget.setCurrentRow(index)
        elif keyword in datas: 
            index = datas.index(keyword)
            self.listWidget.setCurrentRow(index)



class StatusListWidgetCount(StatusListWidget):
    """docstring for StatusListWidgetCount"""
    def __init__(self, layout=None, parent=None):
        super(StatusListWidgetCount, self).__init__(layout=layout, parent=parent)
        
    def add_filter(self, code, count=None): 
        iconPath = '{}/{}'.format(Icon.path, Icon.statusMap[code]['icon'])
        item = QtWidgets.QListWidgetItem()
        display = '{} [{}]'.format(Icon.statusMap[code]['display'], count)
        item.setText(display)
        item.setData(QtCore.Qt.UserRole, code)
        self.listWidget.addItem(item)
        
        if os.path.exists(iconPath): 
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(iconPath), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            item.setIcon(iconWidget)
        return item

    def set_all_item(self, count): 
        item = self.add_filter(self.all_filter, count)
        row = self.listWidget.row(item)
        self.listWidget.takeItem(row)
        self.listWidget.insertItem(0, item)


class StatusLabel(QtWidgets.QWidget):
    def __init__(self, status=None, full_name=True, icon_data=Icon, parent=None):
        super(StatusLabel, self).__init__(parent=parent)
        self.status = status
        self.icon_data = icon_data
        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.icon_label = QtWidgets.QLabel()
        self.icon_label.setMaximumWidth(23)
        self.text_label = QtWidgets.QLabel()

        self.layout.addWidget(self.icon_label)
        self.layout.addWidget(self.text_label)

        if status:
            self.set_status(status, full_name)

    def set_status(self, status, full_name=True):
        pixmap = QtGui.QPixmap()
        status_name = status
        self.icon_label.setVisible(False)
        if status in self.icon_data.statusMap:
            pixmap = QtGui.QPixmap('{}/{}'.format(self.icon_data.path, self.icon_data.statusMap[status]['icon']))
            pixmap = pixmap.scaled(16, 16, QtCore.Qt.KeepAspectRatio)
            # use fullname for status label
            if full_name:
                status_name = self.icon_data.statusMap[status]['display']
            self.icon_label.setVisible(True)
            
        self.icon_label.setPixmap(pixmap)
        self.text_label.setText(status_name)
        self.status = status


