import sys
import os
from functools import partial
from Qt import QtWidgets
from Qt import QtCore
from Qt import QtGui

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler)

moduleDir = os.path.dirname(sys.modules[__name__].__file__)

class Icon: 
    complete = '%s/icons/dialog/complete.png' % moduleDir
    error = '%s/icons/dialog/error.png' % moduleDir
    warning = '%s/icons/dialog/warning.png' % moduleDir

class MessageBox(QtWidgets.QDialog):

    def __init__(self, title='', message='', iconPath='', parent=None):
        super(MessageBox, self).__init__(parent=parent)

        self.layout = QtWidgets.QVBoxLayout(self)
        self.messageLayout = QtWidgets.QHBoxLayout()
        
        # title 
        self.setWindowTitle(title)
        
        # message 
        label = QtWidgets.QLabel(message)

        # Icon 
        iconLabel = QtWidgets.QLabel()
        iconLabel.setPixmap(QtGui.QPixmap(iconPath).scaled(64, 64, QtCore.Qt.KeepAspectRatio))
        
        # add message layout
        self.messageLayout.addWidget(iconLabel)
        self.messageLayout.addWidget(label)

        self.layout.addLayout(self.messageLayout)
        self.add_button()

        # set layout
        self.messageLayout.setSpacing(0)
        self.messageLayout.setContentsMargins(2, 2, 2, 2)

    def add_button(self): 
        # buttons
        buttons = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok, QtCore.Qt.Horizontal, self)
        
        # signals 
        buttons.accepted.connect(self.accept)
        buttons.rejected.connect(self.reject)

        self.layout.addWidget(buttons)

    # static method to create the dialog and return (date, time, accepted)
    @staticmethod
    def success(title, message, parent=None):
        dialog = MessageBox(title=title, message=message, iconPath=Icon.complete, parent=parent)
        if parent and hasattr(parent, 'windowIcon') and parent.windowIcon():
            dialog.setWindowIcon(parent.windowIcon())
        result = dialog.exec_()
        return result

    @staticmethod
    def error(title, message, parent=None):
        dialog = MessageBox(title=title, message=message, iconPath=Icon.error, parent=parent)
        if parent and hasattr(parent, 'windowIcon') and parent.windowIcon():
            dialog.setWindowIcon(parent.windowIcon())
        result = dialog.exec_()
        return result

    @staticmethod
    def warning(title, message, parent=None):
        dialog = MessageBox(title=title, message=message, iconPath=Icon.warning, parent=parent)
        if parent and hasattr(parent, 'windowIcon') and parent.windowIcon():
            dialog.setWindowIcon(parent.windowIcon())
        result = dialog.exec_()
        return result


class CustomMessageBox(QtWidgets.QDialog):
    clicked = QtCore.Signal(object)

    def __init__(self, title='', message='', buttons=[], parent=None):
        super(CustomMessageBox, self).__init__(parent=parent)

        self.layout = QtWidgets.QVBoxLayout(self)
        self.messageLayout = QtWidgets.QHBoxLayout()
        
        # title 
        self.setWindowTitle(title)
        
        # message 
        label = QtWidgets.QLabel(message)

        # add message layout
        self.messageLayout.addWidget(label)

        self.layout.addLayout(self.messageLayout)
        self.add_button(buttons)

        # set layout
        self.messageLayout.setSpacing(0)
        self.messageLayout.setContentsMargins(2, 2, 2, 2)

        self.value = None


    def add_button(self, buttons): 
        # buttons
        layout = QtWidgets.QHBoxLayout()
        for button in buttons: 
            buttonWidget = QtWidgets.QPushButton(button)
            layout.addWidget(buttonWidget)
            buttonWidget.clicked.connect(partial(self.button_value, button))

        self.layout.addLayout(layout)

    def button_value(self, value): 
        self.close()
        self.value = value
        self.clicked.emit(value)
        return value 

    @staticmethod
    def show(title, message, buttons, parent=None):
        dialog = CustomMessageBox(title=title, message=message, buttons=buttons, parent=parent)
        result = dialog.exec_()
        return dialog

class ScrollMessageBox(QtWidgets.QMessageBox):
    def __init__(self, title, message, ls=[], *args, **kwargs):
        QtWidgets.QMessageBox.__init__(self, *args, **kwargs)
        self.setWindowTitle(title)

        self.content = QtWidgets.QWidget(self)
        self.vLayout = QtWidgets.QVBoxLayout(self.content)


        self.label = QtWidgets.QLabel(message)
        self.vLayout.addWidget(self.label)

        self.list_widget = QtWidgets.QListWidget()
        self.vLayout.addWidget(self.list_widget)
        h = 200
        if ls:
            for l in ls:
                self.list_widget.addItem(QtWidgets.QListWidgetItem(l))
                h += 5
            self.list_widget.setCurrentRow(0)
            if h > 500:
                h = 500

        self.content.setMinimumSize(250, h)

        self.layout().addWidget(self.content, 0, 0, 1, self.layout().columnCount())
        self.yes_button = self.addButton('OK', QtWidgets.QMessageBox.ButtonRole.YesRole)
        self.no_button = self.addButton('Cancel', QtWidgets.QMessageBox.ButtonRole.RejectRole)

    @staticmethod
    def show(title, message, ls, parent=None):
        dialog = ScrollMessageBox(title=title, message=message, ls=ls, parent=parent)
        result = dialog.exec_()
        return dialog

class PasswordDialog(QtWidgets.QDialog):

    def __init__(self, title='', message='', parent=None):
        super(PasswordDialog, self).__init__(parent=parent)

        self.layout = QtWidgets.QVBoxLayout(self)
        self.messageLayout = QtWidgets.QHBoxLayout()
        
        # title 
        self.setWindowTitle(title)
        
        # message 
        label = QtWidgets.QLabel(message)

        # add message layout
        self.messageLayout.addWidget(label)

        self.pwdLineEdit = QtWidgets.QLineEdit()
        self.pwdLineEdit.setEchoMode(QtWidgets.QLineEdit.Password)

        self.layout.addLayout(self.messageLayout)
        self.layout.addWidget(self.pwdLineEdit)
        self.add_button()

        # set layout
        self.messageLayout.setSpacing(0)
        self.messageLayout.setContentsMargins(2, 2, 2, 2)

        self.value = None


    def add_button(self): 
        buttons = [('OK', True), ('Cancel', False)]
        # buttons
        layout = QtWidgets.QHBoxLayout()
        for button in buttons: 
            name = button[0]
            value = button[1]
            buttonWidget = QtWidgets.QPushButton(name)
            layout.addWidget(buttonWidget)
            buttonWidget.clicked.connect(partial(self.button_value, value))

        self.layout.addLayout(layout)

    def button_value(self, value): 
        self.close()
        self.value = value
        return value  

    def field_value(self): 
        return str(self.pwdLineEdit.text())

    @staticmethod
    def show(title, message, parent=None):
        dialog = PasswordDialog(title=title, message=message, parent=parent)
        result = dialog.exec_()
        return dialog


class InputDialog(QtWidgets.QDialog):

    def __init__(self, title='', message='', parent=None):
        super(InputDialog, self).__init__(parent=parent)

        self.layout = QtWidgets.QVBoxLayout(self)
        self.messageLayout = QtWidgets.QHBoxLayout()
        
        # title 
        self.setWindowTitle(title)
        
        # message 
        label = QtWidgets.QLabel(message)

        # add message layout
        self.messageLayout.addWidget(label)

        self.inputLineEdit = QtWidgets.QLineEdit()

        self.layout.addLayout(self.messageLayout)
        self.layout.addWidget(self.inputLineEdit)
        self.add_button()

        # set layout
        self.messageLayout.setSpacing(0)
        self.messageLayout.setContentsMargins(2, 2, 2, 2)

        self.value = None


    def add_button(self): 
        buttons = [('OK', True), ('Cancel', False)]
        # buttons
        layout = QtWidgets.QHBoxLayout()
        for button in buttons: 
            name = button[0]
            value = button[1]
            buttonWidget = QtWidgets.QPushButton(name)
            layout.addWidget(buttonWidget)
            buttonWidget.clicked.connect(partial(self.button_value, value))

        self.layout.addLayout(layout)

    def button_value(self, value): 
        self.close()
        self.value = value
        return value  

    def field_value(self): 
        return str(self.inputLineEdit.text())

    @staticmethod
    def show(title, message, parent=None):
        dialog = InputDialog(title=title, message=message, parent=parent)
        result = dialog.exec_()
        return dialog

# usage
# custom_dialog.Dialog.complete('Complete', 'Complete')

# usage
# result = dialog.PasswordDialog.show('test', 'test')
# result.value # button value 
# result.field_value() # field result 
