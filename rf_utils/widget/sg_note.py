import os
import sys
from Qt import QtWidgets
from rf_utils.sg import sg_process
sg = sg_process.sg


class NoteWidget(QtWidgets.QWidget):
    def __init__(self, note_entity, parent=None):
        pass


def test():
    from rf_app.info_panel import sg_request
    project = 'Hanuman'
    name = 'testProp'
    notes = sg_request.fetch_notes_information(project, name)

    for id, note in notes.items():
        fields = {
                "Note": [
                    "created_by.HumanUser.image",
                    "addressings_to",
                    "playlist",
                    "user"],
                "Reply": ["content"],
                "Attachment": [
                    "filmstrip_image",
                    "local_storage",
                    "this_file",
                    "image"]
                    }
        print note['note']['content']
        replies = sg.note_thread_read(id, fields)
        for i, reply in enumerate(replies):
            print i
            print reply.get('content')
