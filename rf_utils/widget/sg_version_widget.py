uiName = 'sgInputWidgetUI'
import os
import sys
import traceback
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

notStartwith = ['.']


class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Versions')
        self.listWidget = QtWidgets.QListWidget()
        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.listWidget)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(0, 0, 0, 0)

        self.setLayout(self.allLayout)

class SGVersionListWidget(Ui):
    """docstring for SGVersionListWidget"""
    itemSelected = QtCore.Signal(dict)
    def __init__(self, parent=None):
        super(SGVersionListWidget, self).__init__(parent=parent)
        self.init_signals()
    def init_signals(self):
        self.listWidget.itemSelectionChanged.connect(self.emit_signal)

    def emit_signal(self):
        data = self.get_data()
        self.itemSelected.emit(data)

    def add_items(self, sgEntities):
        for sgEntity in sgEntities:
            self.add_item(sgEntity)

    def add_item(self, sgEntity):
        item = QtWidgets.QListWidgetItem(self.listWidget)
        itemWidget = VersionItemWidget()
        item.setSizeHint(itemWidget.sizeHint())
        self.listWidget.setItemWidget(item, itemWidget)
        item.setData(QtCore.Qt.UserRole, sgEntity)

        self.add_data(itemWidget, sgEntity)


    def add_data(self, itemWidget, sgEntity):
        code = sgEntity.get('code')
        description = sgEntity.get('description')
        iconPath = sgEntity.get('icon')
        itemWidget.set_text1(code)
        itemWidget.set_text2(description)

        if os.path.exists(iconPath):
            itemWidget.set_icon(iconPath)

    def get_data(self):
        item = self.listWidget.currentItem()
        return item.data(QtCore.Qt.UserRole) if item else None

    def clear(self):
        self.listWidget.clear()


class VersionItemWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(VersionItemWidget, self).__init__(parent)

        self.allLayout = QtWidgets.QHBoxLayout()
        self.iconLabel = QtWidgets.QLabel()

        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.label1 = QtWidgets.QLabel()
        self.label2 = QtWidgets.QLabel()

        self.verticalLayout.addWidget(self.label1)
        self.verticalLayout.addWidget(self.label2)

        self.verticalLayout.setContentsMargins(1, 1, 1, 1)
        self.verticalLayout.setSpacing(1)

        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.allLayout.setSpacing(0)

        self.allLayout.addWidget(self.iconLabel)
        self.allLayout.addLayout(self.verticalLayout)

        self.allLayout.setStretch(0, 1)
        self.allLayout.setStretch(1, 6)
        self.setLayout(self.allLayout)

    def set_icon(self, path):
        self.iconLabel.setPixmap(QtGui.QPixmap(path).scaled(16, 16, QtCore.Qt.KeepAspectRatio))

    def set_text1(self, text):
        self.label1.setText(text)

    def set_text2(self, text):
        self.label2.setText(text)

