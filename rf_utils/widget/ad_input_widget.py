uiName = 'InputWidgetUI'
_title = 'Input Widget'
_version = 'v.0.0.1'
_des = 'beta'

import os
import sys
import traceback
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import file_comboBox

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

notStartwith = ['.']


from rf_utils.widget import file_input_widget
from rf_utils import file_utils
from rf_utils.context import context_info
from rf_utils import register_entity
from rf_utils.widget import dialog
reload(register_entity)
from rf_utils import icon
reload(file_input_widget)

class Color:
    green = [0, 100, 0]

class Config:
    alembic = 'Alembic'
    maya = 'maya'
    mtr = 'mtr'
    asm = 'asm'
    file = 'file'
    media = 'media'
    fileType = 'fileType'
    modelSteps = [context_info.Step.design, context_info.Step.model, context_info.Step.rig, context_info.Step.set, context_info.Step.sim, context_info.Step.crowd]
    mtrSteps = [context_info.Step.texture, context_info.Step.lookdev, context_info.Step.groom]
    fileTypeMap = {'design': {'.jpg': media, '.png': media, '.jpeg': media},
                    'model': {'.ma': maya, '.abc': alembic},
                    'rig': {'.ma': maya},
                    'texture': {'.ma': mtr},
                    'lookdev': {'.ma': mtr},
                    'groom': {'.ma': maya, '.abc': alembic},
                    'sim': {'.ma': maya},
                    'set': {'.ma': maya, '.asm': asm}, 
                    'crowd': {'.ma': maya}
                    }
    keywordMap = {'_mtr_': mtr}

class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()

        # asset title
        self.frame = QtWidgets.QFrame()
        self.asset_layout = QtWidgets.QGridLayout(self.frame)

        self.assetRadioButton = QtWidgets.QRadioButton('Asset')
        self.assetComboBox = QtWidgets.QComboBox()
        self.setRadioButton = QtWidgets.QRadioButton('Set')
        self.setComboBox = QtWidgets.QComboBox()

        self.asset_layout.addWidget(self.assetRadioButton, 0, 1, 1, 1)
        self.asset_layout.addWidget(self.assetComboBox, 0, 2, 1, 1)
        self.asset_layout.addWidget(self.setRadioButton, 0, 3, 1, 1)
        self.asset_layout.addWidget(self.setComboBox, 0, 4, 1, 1)
        self.asset_layout.setSpacing(8)
        self.asset_layout.setContentsMargins(0, 0, 0, 0)
        self.allLayout.addWidget(self.frame)

        self.assetRadioButton.setChecked(True)
        self.setComboBox.setEnabled(False)


        # line
        self.line = QtWidgets.QFrame()
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.allLayout.addWidget(self.line)

        self.header_layout = QtWidgets.QGridLayout()
        self.stepLabel = QtWidgets.QLabel('Type')
        self.processLabel = QtWidgets.QLabel('Process')
        self.resLabel = QtWidgets.QLabel('Res')
        self.step_comboBox = QtWidgets.QComboBox()
        self.process_comboBox = QtWidgets.QComboBox()
        self.res_comboBox = QtWidgets.QComboBox()

        self.header_layout.addWidget(self.step_comboBox, 0, 1, 1, 1)
        self.header_layout.addWidget(self.resLabel, 1, 0, 1, 1)
        self.header_layout.addWidget(self.processLabel, 1, 2, 1, 1)
        self.header_layout.addWidget(self.process_comboBox, 1, 3, 1, 1)
        self.header_layout.addWidget(self.stepLabel, 0, 0, 1, 1)
        self.header_layout.addWidget(self.res_comboBox, 1, 1, 1, 1)

        self.header_layout.setColumnStretch(0, 1)
        self.header_layout.setColumnStretch(1, 3)
        self.header_layout.setColumnStretch(2, 1)
        self.header_layout.setColumnStretch(3, 3)

        self.allLayout.addLayout(self.header_layout)

        # line
        self.line1 = QtWidgets.QFrame()
        self.line1.setFrameShape(QtWidgets.QFrame.HLine)
        self.line1.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.allLayout.addWidget(self.line1)

        self.frame = QtWidgets.QFrame()
        self.option_layout = QtWidgets.QHBoxLayout(self.frame)

        self.file_radioButton = QtWidgets.QRadioButton('File')
        self.media_radioButton = QtWidgets.QRadioButton('Media')
        self.hero_checkBox = QtWidgets.QCheckBox('Hero')
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.option_layout.addWidget(self.file_radioButton)
        self.option_layout.addWidget(self.media_radioButton)
        self.option_layout.addWidget(self.hero_checkBox)
        self.option_layout.addItem(spacerItem)
        self.option_layout.setContentsMargins(0, 0, 0, 0)
        self.allLayout.addWidget(self.frame)

        # line
        self.line2 = QtWidgets.QFrame()
        self.line2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.allLayout.addWidget(self.line2)

        self.display_layout = QtWidgets.QVBoxLayout()

        self.fileWidget = file_input_widget.FileInputWidget()
        self.display_layout.addWidget(self.fileWidget)
        self.allLayout.addLayout(self.display_layout)

        self.info_frame = QtWidgets.QFrame()
        self.info_frame.setFrameShape(QtWidgets.QFrame.Box)
        self.info_frame.setFrameShadow(QtWidgets.QFrame.Sunken)

        self.frame_layout = QtWidgets.QVBoxLayout(self.info_frame)
        self.description_label = QtWidgets.QLabel(self.info_frame)

        self.frame_layout.addWidget(self.description_label)
        self.allLayout.addWidget(self.info_frame)

        self.processNamespaceCheckBox = QtWidgets.QCheckBox('Use process as namespace')

        self.import_pushButton = QtWidgets.QPushButton('Import')
        self.reference_pushButton = QtWidgets.QPushButton('Reference')
        self.import_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.reference_pushButton.setMinimumSize(QtCore.QSize(0, 30))

        self.allLayout.addWidget(self.processNamespaceCheckBox)
        self.allLayout.addWidget(self.import_pushButton)
        self.allLayout.addWidget(self.reference_pushButton)

        self.setLayout(self.allLayout)
        self.set_default()

    def set_default(self):
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.file_radioButton.setChecked(True)
        self.hero_checkBox.setChecked(True)


class InputWidget(Ui):
    """docstring for InputWidget"""
    def __init__(self, dccHook=None, parent=None):
        super(InputWidget, self).__init__(parent=parent)
        self.regInfo = None
        self.dccHook = dccHook
        self.init_signals()

    def init_signals(self):
        self.step_comboBox.currentIndexChanged.connect(self.step_changed)
        self.res_comboBox.currentIndexChanged.connect(self.ui_changed)
        self.process_comboBox.currentIndexChanged.connect(self.list_contents)
        self.hero_checkBox.stateChanged.connect(self.list_contents)
        self.fileWidget.itemSelected.connect(self.input_selected)

        self.file_radioButton.clicked.connect(self.list_contents)
        self.media_radioButton.clicked.connect(self.list_contents)

        self.assetRadioButton.clicked.connect(self.asset_selected)
        self.setRadioButton.clicked.connect(self.asset_selected)
        self.setComboBox.currentIndexChanged.connect(self.asset_selected)

        # button
        self.import_pushButton.clicked.connect(self.import_file)
        self.reference_pushButton.clicked.connect(self.reference_file)

    def ui_block_signal(self, state):
        self.step_comboBox.blockSignals(state)
        self.res_comboBox.blockSignals(state)

    def update_input(self, entity, setNames=None):
        self.set_asset_input_ui(entity, setNames)
        self.init_input(entity)

    def asset_selected(self):
        """ when asset selected, pass entity to init_input """
        if self.assetRadioButton.isChecked():
            entity = self.assetComboBox.itemData(0, QtCore.Qt.UserRole)

            # run input. Connected to previous logic
            self.init_input(entity)

            # set ui
            self.assetComboBox.setEnabled(True)
            self.setComboBox.setEnabled(False)

        if self.setRadioButton.isChecked():
            entity = self.setComboBox.itemData(self.setComboBox.currentIndex(), QtCore.Qt.UserRole)
            name = str(self.setComboBox.currentText())
            setEntity = entity.copy()
            setEntity.context.update(entityGrp='set', entity=name)

            # run input. Connected to previous logic
            self.init_input(setEntity)

            # set ui
            self.assetComboBox.setEnabled(False)
            self.setComboBox.setEnabled(True)

    def init_input(self, entity):
        """ replacement of update input """
        self.entity = entity
        self.regInfo = register_entity.Register(self.entity)
        self.set_ui()

    def set_ui(self):
        self.clear_widget()
        self.ui_block_signal(True)
        self.set_type()
        self.set_res()
        self.set_process()
        self.ui_block_signal(False)

    def set_asset_input_ui(self, entity, setNames):
        self.assetRadioButton.blockSignals(True)
        self.setRadioButton.blockSignals(True)
        self.setComboBox.blockSignals(True)

        self.assetComboBox.clear()
        self.setComboBox.clear()
        self.assetComboBox.addItem(entity.name)
        self.assetComboBox.setItemData(0, entity)

        if setNames:
            for i, name in enumerate(setNames):
                self.setComboBox.addItem(name)
                self.setComboBox.setItemData(i, entity)
        else:
            self.setComboBox.addItem('-- No Set --')
            self.setComboBox.setItemData(0, entity)

        self.assetRadioButton.blockSignals(False)
        self.setRadioButton.blockSignals(False)
        self.setComboBox.blockSignals(False)


    def set_type(self):
        types = self.regInfo.get_types()
        self.step_comboBox.clear()
        self.step_comboBox.addItems(types)

    def set_res(self):
        res = self.regInfo.get_res()
        self.res_comboBox.clear()
        self.res_comboBox.addItems(res)

    def set_process(self):
        step = str(self.step_comboBox.currentText())
        res = str(self.res_comboBox.currentText())
        items = []
        if step in Config.modelSteps:
            items = self.regInfo.get_processes(step=step, res=res)

        if step in Config.mtrSteps:
            items = self.regInfo.get_looks(step=step, res=res)

        self.process_comboBox.clear()
        self.process_comboBox.addItems(sorted(items))

    # signals

    def step_changed(self):
        self.import_pushButton.setEnabled(True)
        self.reference_pushButton.setEnabled(True)
        step = str(self.step_comboBox.currentText())

        if self.entity.step in Config.mtrSteps:
            if step == context_info.Step.model:
                self.import_pushButton.setEnabled(True)
            if step == context_info.Step.texture:
                self.reference_pushButton.setEnabled(True)

        self.ui_changed()

    def ui_changed(self):
        if self.regInfo:
            self.clear_widget()
            self.set_process()

    def clear_widget(self):
        self.fileWidget.clear()

    def input_selected(self, itemDict):
        text = itemDict.get('description')
        self.set_description(text)

    def import_file(self):
        """ do import files """
        itemData = self.fileWidget.get_data()
        filename = itemData.get(file_input_widget.Config.filename)
        fileType = itemData.get(file_input_widget.Config.fileType)

        if fileType == Config.alembic:
            result = self.dccHook.import_abc(filename)
            dialog.MessageBox.success('Import complete', 'Import Alembic complete') if result else logger.error('Import alembic failed')

        if fileType == Config.maya:
            self.dccHook.import_file(filename)

        if fileType == Config.mtr:
            targetGrp = self.entity.projectInfo.asset.geo_grp()
            self.dccHook.import_mtr(targetGrp, filename)


    def reference_file(self):
        """ do reference files """
        itemData = self.fileWidget.get_data()
        filename = itemData.get(file_input_widget.Config.filename)
        fileType = itemData.get(file_input_widget.Config.fileType)
        processNamespace = self.processNamespaceCheckBox.isChecked()

        if fileType in [Config.alembic, Config.maya]:
            namespace = self.entity.name if not processNamespace else str(self.process_comboBox.currentText())
            namespace = '%s_%s' % (namespace, self.entity.res)
            result = self.dccHook.reference_file(namespace, filename)
            dialog.MessageBox.success('Complete', 'Reference Alembic/Maya complete') if result else logger.error('Import alembic failed')


    def set_description(self, text):
        self.description_label.setText(text)

    def list_contents(self):
        # uis
        step = str(self.step_comboBox.currentText())
        res = str(self.res_comboBox.currentText())
        process = str(self.process_comboBox.currentText())
        hero = self.hero_checkBox.isChecked()
        isFile = self.file_radioButton.isChecked()
        isMedia = self.media_radioButton.isChecked()
        displayList = None
        datas = []
        heroData = None
        displayFile = 'heroFile' if hero else 'publishedFile'

        # update entity
        self.entity.context.update(res=res, step=step, process=process)

        # data
        if step in Config.modelSteps:
            datas = self.regInfo.version.outputs(step=step, process=process, res=res)
            heroData = self.regInfo.get.output(step=step, process=process, res=res)

        if step in Config.mtrSteps:
            datas = self.regInfo.version.outputs(step=step, look=process, res=res)
            heroData = self.regInfo.get.output(step=step, look=process, res=res)

        if isFile:
            displayList = self.get_display_list(step, datas, heroData, displayFile, hero, Config.file)
            print 'displayList', displayList

        if isMedia:
            displayList = self.get_media_list(step, datas, heroData, displayFile, hero, Config.media)

        self.set_display(displayList)



    def get_display_list(self, step, datas, heroData, displayFile, hero, displayType):
        displayList = []
        displayData = [heroData] if hero else datas
        # get hero file
        heroOutput = register_entity.Output(heroData)
        heroFiles = self.get_step_display_files(step=step, output=heroOutput, displayType=displayType)
        heroFiles = [a.get(displayFile, '') for a in heroFiles]

        for data in displayData:
            # model display
            output = register_entity.Output(data)
            stepFiles = self.get_step_display_files(step=step, output=output, displayType=displayType)


            for stepFile in stepFiles:
                filename = stepFile.get(displayFile, '')

                if filename:
                    recommendVersion = True if filename in heroFiles else False
                    recommendVersion = False
                    name, ext = os.path.splitext(filename)
                    fileType = Config.fileTypeMap.get(step).get(ext)

                    # filter by filename keyword
                    key = [v for k, v in Config.keywordMap.iteritems() if k in filename]
                    fileType = fileType if not key else key[0]

                    # pack dict
                    displayDict = self.display_dict(filename,
                                description=output.get_description(),
                                task=output.get_task(),
                                step=step,
                                fileType=fileType,
                                recommend=recommendVersion,
                                filePreview=False)

                    displayList.append(displayDict)

        return displayList

    def get_media_list(self, step, datas, heroData, displayFile, hero, displayType):
        displayList = []
        displayData = [heroData] if hero else datas
        # get hero file
        heroOutput = register_entity.Output(heroData)
        heroDicts = self.get_step_display_files(step=step, output=heroOutput, displayType=displayType)
        heroFiles = []

        # collect hero files
        for item in heroFiles:
            path = item.get(displayFile, '')
            files = file_utils.list_file(path)
            heroFiles = heroFiles + files

        for data in displayData:
            # model display
            output = register_entity.Output(data)
            stepFiles = self.get_step_display_files(step=step, output=output, displayType=displayType)

            for stepFile in stepFiles:
                imagePath = stepFile.get(displayFile, '')

                if imagePath:
                    files = file_utils.list_file(imagePath)
                    files = [('/').join([imagePath, a]) for a in files]

                    for filename in files:
                        recommendVersion = True if filename in heroFiles else False
                        name, ext = os.path.splitext(filename)
                        fileType = Config.media

                        # pack dict
                        displayDict = self.display_dict(filename,
                                    description=output.get_description(),
                                    task=output.get_task(),
                                    step=step,
                                    fileType=fileType,
                                    recommend=recommendVersion,
                                    filePreview=True)

                        displayList.append(displayDict)

        return displayList

    def get_step_display_files(self, step, output, displayType):
        """ this method return published files that will be used in next department """
        displayFiles = []
        if displayType == Config.file:
            if step == context_info.Step.design:
                cache = output.get_design()
                displayFiles.append(cache)

            if step == context_info.Step.model:
                cache = output.get_cache()
                workspace = output.get_maya()
                mtrPreview = output.get_mtr_preview()
                displayFiles.append(cache)
                displayFiles.append(workspace)
                displayFiles.append(mtrPreview)

            if step == context_info.Step.rig:
                rig = output.get_rig()
                displayFiles.append(rig)

            if step == context_info.Step.texture:
                mtr = output.get_mtr()
                # mtrPreview = output.get_mtr_preview()
                displayFiles.append(mtr) if not mtr in displayFiles else None
                # displayFiles.append(mtrPreview) if not mtrPreview in displayFiles else None

            if step == context_info.Step.lookdev:
                mtrRender = output.get_mtr_render()
                displayFiles.append(mtrRender)

            if step == context_info.Step.set:
                camera = output.get_camera()
                displayFiles.append(camera)

            if step == context_info.Step.groom:
                groom = output.get_groom_yeti()
                mtr = output.get_mtr_render()
                tech = output.get_tech_geo()
                displayFiles.append(groom)
                displayFiles.append(mtr)
                displayFiles.append(tech)

            if step == context_info.Step.sim:
                sim = output.get_maya()
                displayFiles.append(sim)

            if step == context_info.Step.crowd: 
                crowd = output.get_maya()
                displayFiles.append(crowd)

        if displayType == Config.media:
            media = output.get_media()
            media[Config.fileType] = Config.media
            displayFiles.append(media)

        return displayFiles



    def display_dict(self, filename, description, task, step, fileType, recommend, filePreview):
        # pack dict
        displayDict = OrderedDict()
        displayDict['filename'] = filename
        displayDict['description'] = description
        displayDict['task'] = task
        displayDict['step'] = step
        displayDict['fileType'] = fileType
        displayDict['recommended'] = recommend
        displayDict['filePreview'] = filePreview
        return displayDict

    def set_display(self, displayList):
        self.fileWidget.clear()
        self.fileWidget.add_items(displayList) if displayList else None


class Main(QtWidgets.QMainWindow):
    """docstring for Main"""
    def __init__(self, parent=None):
        super(Main, self).__init__(parent)
        from rf_app.sg_manager import dcc_hook
        from rf_utils.sg import sg_utils
        self.sg = sg_utils.sg

        self.inputWidget = InputWidget(dccHook=dcc_hook)
        self.inputWidget.allLayout.setContentsMargins(8, 8, 8, 8)
        self.setCentralWidget(self.inputWidget)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))
        self.resize(360, 500)

        self.init_functions()

    def init_functions(self):
        self.asset = context_info.ContextPathInfo()
        if self.asset.project:
            self.inputWidget.update_input(self.asset)




def show():
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = Main(maya_win.getMayaWindow())
    myApp.show()
    return myApp

    # parentSgEntities = sgEntity.get('parents') if hasattr(sgEntity, 'get') else None
    # setNames = [a['name'] for a in parentSgEntities] if parentSgEntities else []
    # self.ui.inputWidget.update_input(self.entity, setNames)

