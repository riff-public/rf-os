from Qt import QtWidgets

class Console(QtWidgets.QPlainTextEdit):
	"""docstring for Console"""
	def __init__(self, parent=None):
		super(Console, self).__init__(parent=parent)
		
	def append(self, text): 
		self.appendPlainText(text)

	def write(self, text): 
		self.setPlainText(text)

	def error(self, text):
		redText = '<span style="color:#ff0000;">'
		redText += text
		redText += '</span>'
		text.replace('\n', '<br>')
		self.appendHtml(redText)
		# redText = '<span style=" font-size:14pt; color:#ff0000;">ERROR</span></p></body></html>'

	def warning(self, text): 
		yellowText = '<span style="color:#ffff00;">'
		yellowText += text
		yellowText += '</span>'
		text.replace('\n', '<br>')
		self.appendHtml(yellowText)

	def debug(self, text): 
		orangeText = '<span style="color:#fc8403;">'
		orangeText += text
		orangeText += '</span>'
		text.replace('\n', '<br>')
		self.appendHtml(orangeText)		

	def complete(self, text): 
		completeText = '<span style="color:#3dfc03;">'
		completeText += text
		completeText += '</span>'
		text.replace('\n', '<br>')
		self.appendHtml(completeText)	

	def title(self, text): 
		completeText = '<span style="color:#fc8403;"><b>'
		completeText += text
		completeText += '</b></span>'
		text.replace('\n', '<br>')
		self.appendHtml(completeText)	
