uiName = 'sgInputWidgetUI'
import os
import sys
import traceback
import subprocess
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

notStartwith = ['.']

class Config:
    filename = 'filename'
    description = 'description'
    task = 'task'
    step = 'step'
    fileType = 'fileType'
    recommended = 'recommended'
    filePreview = 'filePreview'

class InputIcon(object):
    """docstring for InputIcon"""
    def __init__(self):
        super(InputIcon, self).__init__()
        self.iconPath = '%s/icons/input' % module_dir

    def get(self, step, fileType):
        filename = None
        if fileType == 'media':
            filename = 'media.png'
        else:
            if step == 'model' and fileType == 'Alembic':
                filename = 'model_abc.png'
            if step == 'model' and fileType == 'maya':
                filename = 'model_ma.png'
            if step == 'model' and fileType == 'mtr':
                filename = 'mtr_ma.png'
            if step == 'rig' and fileType == 'maya':
                filename = 'rig_ma.png'
            if step == 'groom' and fileType == 'maya':
                filename = 'groom_ma.png'
            if step == 'groom' and fileType == 'mtr':
                filename = 'mtr_ma.png'
            if step == 'groom' and fileType == 'Alembic':
                filename = 'model_abc.png'
            if step == 'set' and fileType == 'maya':
                filename = 'set_cam.png'
            if step in ['texture', 'lookdev'] and fileType == 'mtr':
                filename = 'mtr_ma.png'
            if step == 'sim' and fileType == 'maya': 
                filename = 'rig_ma.png'
            if step == 'crowd' and fileType == 'maya': 
                filename = 'rig_ma.png'

        return '%s/%s' % (self.iconPath, filename) if filename else ''

    def recommended(self):
        return '%s/%s' % (self.iconPath, 'recommended.png')


class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Versions')
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.listWidget)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(0, 0, 0, 0)

        self.setLayout(self.allLayout)

class FileInputWidget(Ui):
    """docstring for FileInputWidget"""
    itemSelected = QtCore.Signal(dict)
    def __init__(self, parent=None):
        super(FileInputWidget, self).__init__(parent=parent)
        self.inputIcon = InputIcon()
        self.init_signals()

    def init_signals(self):
        self.listWidget.itemSelectionChanged.connect(self.emit_signal)
        self.listWidget.itemDoubleClicked.connect(self.view_image)
        self.listWidget.customContextMenuRequested.connect(self.show_menu)

    def emit_signal(self):
        data = self.get_data()
        self.itemSelected.emit(data)

    def show_menu(self, pos):
        selItem = self.listWidget.currentItem()
        menu = QtWidgets.QMenu(self)
        explorerMenu = menu.addAction('Open in Explorer')
        explorerMenu.triggered.connect(partial(self.open_explorer, selItem))
        menu.popup(self.listWidget.mapToGlobal(pos))

    def view_image(self, item):
        """ show image on default os viewer """
        itemDict = item.data(QtCore.Qt.UserRole)
        filename = itemDict.get(Config.filename)
        filePreview = itemDict.get(Config.filePreview)
        if filePreview:
            os.startfile(filename)

    def open_explorer(self, item):
        data = item.data(QtCore.Qt.UserRole)
        path = data.get(Config.filename)
        subprocess.Popen(r'explorer /select,"%s"' % path.replace('/', '\\'))

    def add_items(self, displayDicts):
        for displayDict in displayDicts:
            self.add_item(displayDict)

    def add_item(self, displayDict):
        # extract data
        filename = displayDict.get(Config.filename)
        description = displayDict.get(Config.description)
        task = displayDict.get(Config.task) or ''
        step = displayDict.get(Config.step)
        fileType = displayDict.get(Config.fileType)
        recommended = displayDict.get(Config.recommended)
        filePreview = displayDict.get(Config.filePreview)
        iconPath = self.inputIcon.get(step, fileType)
        recommendedIcon = self.inputIcon.recommended() if recommended else ''

        item, itemWidget = self.set_item(filePreview)
        item.setData(QtCore.Qt.UserRole, displayDict)
        item.setBackground(QtGui.QColor(100, 70, 20)) if recommended else None

        self.add_data(itemWidget, filename, description, task, iconPath, recommendedIcon, filePreview)

    def set_item(self, filePreview=False):
        item = QtWidgets.QListWidgetItem(self.listWidget)
        itemWidget = CustomItemWidget()
        item.setSizeHint(itemWidget.sizeHint())
        self.listWidget.setItemWidget(item, itemWidget)

        return item, itemWidget

    def add_data(self, itemWidget, filename, description, task, iconPath1, iconPath2, filePreview):
        itemWidget.set_text1(os.path.basename(filename))
        itemWidget.set_text2(task)
        itemWidget.set_text2_color([100, 140, 200])
        itemWidget.set_text3(description)
        itemWidget.set_text3_color([100, 200, 100])
        iconSize = [64, 64] if filePreview else [32, 32]

        if os.path.exists(iconPath1):
            itemWidget.set_icon1(iconPath1, iconSize)
        if os.path.exists(iconPath2):
            itemWidget.set_icon2(iconPath2)

    def get_data(self):
        item = self.listWidget.currentItem()
        return item.data(QtCore.Qt.UserRole) if item else None

    def clear(self):
        self.listWidget.clear()


class CustomItemWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(CustomItemWidget, self).__init__(parent)
        self.set_ui()

    def set_ui(self):
        self.allLayout = QtWidgets.QHBoxLayout()
        self.iconLabel1 = QtWidgets.QLabel()
        self.iconLabel2 = QtWidgets.QLabel()

        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.label1 = QtWidgets.QLabel()
        self.label2 = QtWidgets.QLabel()
        self.label3 = QtWidgets.QLabel()

        self.verticalLayout.addWidget(self.label1)
        self.verticalLayout.addWidget(self.label2)
        self.verticalLayout.addWidget(self.label3)

        self.verticalLayout.setContentsMargins(1, 1, 1, 1)
        self.verticalLayout.setSpacing(1)

        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.allLayout.setSpacing(2)

        self.allLayout.addWidget(self.iconLabel2)
        self.allLayout.addWidget(self.iconLabel1)
        self.allLayout.addLayout(self.verticalLayout)

        self.allLayout.setStretch(0, 1)
        self.allLayout.setStretch(1, 1)
        self.allLayout.setStretch(2, 6)
        self.setLayout(self.allLayout)

    def set_icon1(self, path, size=[32, 32]):
        self.iconLabel1.setPixmap(QtGui.QPixmap(path).scaled(size[0], size[1], QtCore.Qt.KeepAspectRatio))

    def set_icon2(self, path, size=[32, 32]):
        self.iconLabel2.setPixmap(QtGui.QPixmap(path).scaled(size[0], size[1], QtCore.Qt.KeepAspectRatio))

    def set_text1(self, text):
        self.label1.setText(text)

    def set_text2(self, text):
        self.label2.setText(text)

    def set_text3(self, text):
        self.label3.setText(text)

    def set_text1_color(self, color):
        self.label1.setStyleSheet('color: rgb(%s, %s, %s);' % (color[0], color[1], color[2]))

    def set_text2_color(self, color):
        self.label2.setStyleSheet('color: rgb(%s, %s, %s);' % (color[0], color[1], color[2]))

    def set_text3_color(self, color):
        self.label3.setStyleSheet('color: rgb(%s, %s, %s);' % (color[0], color[1], color[2]))

