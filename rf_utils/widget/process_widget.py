import os
import sys
import yaml
from collections import OrderedDict

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

from rf_utils.widget import file_comboBox


class ProcessWidget(QtWidgets.QWidget):
    """ Diplay file and put associated icon """
    currentIndexChanged = QtCore.Signal(str)

    def __init__(self, parent=None):
        super(ProcessWidget, self).__init__(parent=parent)

        self.extraDirs = ['maya']

        self.ui()
        self.init_signals()

    def ui(self): 
        self.allLayout = QtWidgets.QHBoxLayout()

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)

        self.processComboBox = file_comboBox.FileComboBox(layout=QtWidgets.QHBoxLayout())
        self.processComboBox.set_label('Process : ')
        self.addProcessButton = QtWidgets.QPushButton('+')
        self.addProcessButton.setMaximumSize(QtCore.QSize(20, 18))

        self.allLayout.addWidget(self.processComboBox)
        self.allLayout.addWidget(self.addProcessButton)

        self.setLayout(self.allLayout)

    def init_signals(self): 
        self.addProcessButton.clicked.connect(self.add_process)
        self.processComboBox.currentIndexChanged.connect(self.emit_signal)

    def emit_signal(self, path): 
        self.currentIndexChanged.emit(path)

    def add_file(self, filePath): 
        self.processComboBox.add_file(filePath)

    def clear(self): 
        self.processComboBox.clear()

    def list_items(self, path): 
        self.processComboBox.list_items(path)

    def add_process(self): 
        text, okPressed = QtWidgets.QInputDialog.getText(self, 'Add Process', 'Process Name:', QtWidgets.QLineEdit.Normal, '')
        
        if okPressed: 
            currentPath = self.processComboBox.current_item()
            root = os.path.split(currentPath)[0]
            newDir = '%s/%s' % (root, text)
            dirs = self.dir_list(newDir)
            self.create_dir(dirs)
            self.list_items(root)

    def dir_list(self, path): 
        return ['%s/%s' % (path, a) for a in self.extraDirs]

    def create_dir(self, pathList): 
        for path in pathList: 
            if not os.path.exists(path): 
                os.makedirs(path)

    def blockSignals(self, state): 
        self.processComboBox.blockSignals(state)

    def current_text(self): 
        return self.processComboBox.current_text()

    def set_current(self, text): 
        self.processComboBox.set_current(text)

    def set_label_color(self, colors): 
        self.processComboBox.label.set_label_color(colors)
