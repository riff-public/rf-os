import os
import sys
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from collections import OrderedDict


module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import icon

class ShotListWidget(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    selected = QtCore.Signal(str)
    itemSelectionChanged = QtCore.Signal(dict)
    checkBoxChecked = QtCore.Signal(list)

    def __init__(self, parent=None) :
        super(ShotListWidget, self).__init__(parent)
        self.ui()
        self.init_signals()

    def ui(self):
        self.layout = QtWidgets.QVBoxLayout()
        self.listWidget = QtWidgets.QListWidget()
        self.layout.addWidget(self.listWidget)

        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

    def init_signals(self):
        self.listWidget.itemSelectionChanged.connect(self.item_selected)

    def item_selected(self):
        if len(self.listWidget.selectionModel().selectedIndexes()) > 1:
            list_data = []
            for item in self.listWidget.selectionModel().selectedIndexes():
                data = item.data(QtCore.Qt.UserRole)
                list_data.append(data)
            self.selected.emit(list_data)
            self.itemSelectionChanged.emit(list_data)

        else:
            item = self.listWidget.currentItem()
            if item:
                data = item.data(QtCore.Qt.UserRole)
                self.selected.emit(data)
                self.itemSelectionChanged.emit(data)

    def select_items(self, items, state):
        self.listWidget.blockSignals(True)
        for item in items:
            item.setSelected(state)
            data = item.data(QtCore.Qt.UserRole)
            self.itemSelectionChanged.emit(data)

        self.listWidget.blockSignals(False)


    def add_widget_item(self, widgetItem, data):
        item = QtWidgets.QListWidgetItem(self.listWidget)
        item.setSizeHint(widgetItem.sizeHint())
        item.setData(QtCore.Qt.UserRole, data)

        self.listWidget.addItem(item)
        self.listWidget.setItemWidget(item, widgetItem)
        return item

    def add_item(self, display, data, setIcon=None):
        data = data if data else display
        item = QtWidgets.QListWidgetItem(self.listWidget)
        item.setText(display)
        item.setData(QtCore.Qt.UserRole, data)

        if setIcon: 
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(icon.camera),QtGui.QIcon.Normal,QtGui.QIcon.Off)
            item.setIcon(iconWidget)

    def add_item_info(self, shotName, startFrame, endFrame, data, message, status, customWidget=None):
        data = data if data else shotName
        frameRange = '%s - %s'%(startFrame, endFrame)

        customWidget = ShotItemWidget() if not customWidget else customWidget
        customWidget.set_text_shot(shotName)
        customWidget.set_text_frame(frameRange)
        customWidget.set_text_status(message)
        customWidget.set_icon(icon.camera)

        item = QtWidgets.QListWidgetItem(self.listWidget)
        item.setSizeHint(customWidget.sizeHint())
        item.setData(QtCore.Qt.UserRole, data)

        self.listWidget.addItem(item)
        self.listWidget.setItemWidget(item, customWidget)
        customWidget.checkBoxChecked.connect(partial(self.checkBox_checked, item, customWidget, data))

        return customWidget

    def checkBox_checked(self, item, widgetItem, data, state):
        self.checkBoxChecked.emit([item, state])

    def widget_items(self):
        return [self.listWidget.itemWidget(a) for a in self.items()]

    def items(self):
        return [self.listWidget.item(a) for a in range(self.listWidget.count())]

    def datas(self):
        return [a.data(QtCore.Qt.UserRole) for a in self.items()]

    def selected_items(self):
        return self.listWidget.selectedItems()

    def selected_datas(self):
        return [a.data(QtCore.Qt.UserRole) for a in self.selected_items()]

    def checked_items(self):
        return [a for a in self.items() if self.listWidget.itemWidget(a).is_checked()]

    def clear(self):
        self.listWidget.clear()

    def widget_item(self, item):
        return self.listWidget.itemWidget(item)

    def data(self, select=False, all=False):
        items = []
        if select:
            items = self.listWidget.selectedItems()
        if all:
            items = [self.listWidget.item(a) for a in range(self.listWidget.count())]
        return [a.data(QtCore.Qt.UserRole) for a in items] if items else []


class ShotItemWidget(QtWidgets.QWidget):
    """docstring for ShotItemWidget"""
    checkBoxChecked = QtCore.Signal(bool)
    def __init__(self, parent=None):
        super(ShotItemWidget, self).__init__(parent)
        self.set_ui()
        self.init_signals()

    def set_ui(self):
        self.layout = QtWidgets.QVBoxLayout()

        self.textShot    = QtWidgets.QLabel()
        self.layout.addWidget(self.textShot)

        self.textFrameRange  = QtWidgets.QLabel()
        self.layout.addWidget(self.textFrameRange)

        self.textstatus  = QtWidgets.QLabel()
        self.layout.addWidget(self.textstatus)

        self.allQHBoxLayout  = QtWidgets.QHBoxLayout()

        self.checkBox = QtWidgets.QCheckBox()
        self.allQHBoxLayout.addWidget(self.checkBox)

        self.iconQLabel      = QtWidgets.QLabel()
        self.allQHBoxLayout.addWidget(self.iconQLabel, 0)

        self.allQHBoxLayout.addLayout(self.layout, 1)
        self.setLayout(self.allQHBoxLayout)

        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)

        # setStyleSheet
        self.textShot.setFont(QtGui.QFont("Arial", 10, QtGui.QFont.Bold))
        self.textFrameRange.setFont(QtGui.QFont("Arial", 8, QtGui.QFont.Bold))
        self.textstatus.setFont(QtGui.QFont("Arial", 8, QtGui.QFont.Bold))

    def init_signals(self):
        self.checkBox.stateChanged.connect(self.checkBox_checked)

    def checkBox_checked(self, value):
        state = True if value else False
        self.checkBoxChecked.emit(state)


    def set_shot_color(self, colors):
        self.textShot.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_range_color(self, colors):
        self.textFrameRange.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_status_color(self, colors):
        self.textstatus.setStyleSheet('background-color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text_shot(self, text):
        self.textShot.setText(text)

    def set_text_frame(self, text):
        self.textFrameRange.setText(text)

    def set_text_status(self, message):
        self.textstatus.setText(message)

    def set_icon(self, imagePath):
        self.iconQLabel.setPixmap(QtGui.QPixmap(imagePath))

    def is_checked(self):
        return self.checkBox.isChecked()

    def set_checked(self, state):
        self.checkBox.setChecked(state)

    def shot(self):
        return self.textShot.text()



class ShotItemBasic(QtWidgets.QWidget):
    """docstring for ShotItemBasic"""
    def __init__(self, parent=None):
        super(ShotItemBasic, self).__init__(parent=parent)
        self.set_ui()


    def set_ui(self):
        self.layout = QtWidgets.QHBoxLayout()
        self.vlayout = QtWidgets.QVBoxLayout()
        self.hlayout = QtWidgets.QHBoxLayout()

        self.checkBox = QtWidgets.QCheckBox()
        self.iconQLabel = QtWidgets.QLabel()
        self.textShot = QtWidgets.QLabel()
        self.textFrameRange  = QtWidgets.QLabel()
        self.textstatus = QtWidgets.QLabel()

        self.layout.addWidget(self.checkBox)
        self.layout.addWidget(self.iconQLabel)
        self.hlayout.addWidget(self.textFrameRange)
        self.hlayout.addWidget(self.textstatus)

        self.vlayout.addWidget(self.textShot)
        self.vlayout.addLayout(self.hlayout)
        self.layout.addLayout(self.vlayout)

        self.setLayout(self.layout)

        self.layout.setContentsMargins(2, 2, 2, 2)
        self.layout.setSpacing(0)
        self.layout.setStretch(0, 1)
        self.layout.setStretch(1, 1)
        self.layout.setStretch(2, 6)

        # setStyleSheet
        self.textShot.setFont(QtGui.QFont("Arial", 9, QtGui.QFont.Bold))
        self.textFrameRange.setFont(QtGui.QFont("Arial", 8))

    def set_checked(self, state):
        self.checkBox.setChecked(state)

    def is_checked(self):
        return self.checkBox.isChekced()

    def set_icon(self, imagePath):
        self.iconQLabel.setPixmap(QtGui.QPixmap(imagePath))

    def set_text_shot(self, text):
        self.textShot.setText(text)

    def set_text_frame(self, text):
        self.textFrameRange.setText(text)

    def set_text_status(self, message):
        self.textstatus.setText(message)

    def set_shot_color(self, colors):
        self.textShot.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_range_color(self, colors):
        self.textFrameRange.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_status_color(self, colors):
        self.textstatus.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def get_text_shot(self):
        return self.textShot.text()
