import os
import sys

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

class ListWidget(QtWidgets.QListWidget):
    """ subclass QLineEdit for dragdrop events """
    dropped = QtCore.Signal(list)
    multipleDropped = QtCore.Signal(list)
    def __init__(self, parent=None):
        super(ListWidget, self).__init__(parent)
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))

            self.multipleDropped.emit(links)
            # self.addWidgetItems(links)

        else:
            event.ignore()

    def addWidgetItems(self, items):
        self.addItems(items)