import os 
import sys 
import tempfile

# python3 compat
try:
    unicode('')
except NameError:
    unicode = str

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
try:
    moduleDir = sys._MEIPASS.replace('\\', '/')
except Exception:
    moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')

from rf_utils.pipeline import convert_lib

class Config:
    noPreview = '{}/icons/etc/nopreview_icon.png'.format(moduleDir)
    vidPreview = '{}/icons/etc/mov_preview_icon.png'.format(moduleDir)
    play_icon = '{}/icons/etc/play_button.png'.format(moduleDir)
    loading_movie = "{}/icons/gif/loading100.gif".format(moduleDir)
    img_ext = ('.jpg', '.png', '.tif', '.exr')
    vid_ext = ('.mov', '.mp4', '.mpg')

class CustomListWidget(QtWidgets.QListWidget):
    deleted = QtCore.Signal(list)
    fileDropped = QtCore.Signal(list)
    itemDoubleClicked = QtCore.Signal(QtWidgets.QListWidgetItem)
    def __init__(self, parent=None):
        super(CustomListWidget, self).__init__(parent)
        self.setAcceptDrops(True)
        self.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.setDefaultDropAction(QtCore.Qt.MoveAction)

    def dragLeaveEvent(self, event):
        items = self.selectedItems()
        mimedata = QtCore.QMimeData()
        urls = []
        for item in items:
            data = item.data(QtCore.Qt.UserRole)
            if 'filepath' in data:
                url = QtCore.QUrl.fromLocalFile(data['filepath'])
                urls.append(url)
        if urls:
            mimedata.setUrls(urls)
            drag = QtGui.QDrag(self)
            # set mime data
            drag.setMimeData(mimedata) 
            drag.start(QtCore.Qt.MoveAction)
            event.accept()
        else:
            event.ignore()

    def mouseDoubleClickEvent(self, event):
        currentItem = self.currentItem()
        if currentItem:
            self.itemDoubleClicked.emit(currentItem)
        event.accept()

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.fileDropped.emit(links)
        else:
            event.ignore()

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Delete:
            # emit current item if delete key is pressed
            sels = self.selectedItems()
            if sels:
                self.deleted.emit(sels)

class DisplayReel(QtWidgets.QWidget):
    clicked = QtCore.Signal(str)
    deleted = QtCore.Signal(list)
    fileDropped = QtCore.Signal(list)
    imagePasted = QtCore.Signal(QtGui.QPixmap)
    itemDoubleClicked = QtCore.Signal(object)
    def __init__(self, parent=None, always_reload=False) :
        super(DisplayReel, self).__init__(parent)
        self._proxy_items = []
        self.always_reload = always_reload
        self.allow_delete = True
        self.allow_paste = False

        self.allLayout = QtWidgets.QVBoxLayout()
        self.displayListWidget = CustomListWidget()
        self.displayListWidget.installEventFilter(self)
        self.allLayout.addWidget(self.displayListWidget)

        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)

        self.init_signals()

    def eventFilter(self, source, event):
        if source is self.displayListWidget and event.type() == QtCore.QEvent.KeyPress and event == QtGui.QKeySequence.Paste:
            pixmap = QtWidgets.QApplication.clipboard().pixmap()
            if pixmap:
                self.imagePasted.emit(pixmap)
                if self.allow_paste:
                    fh, temp = tempfile.mkstemp(suffix='.png')
                    os.close(fh)
                    pixmap.save(temp, "PNG")
                    self.add_item(temp)
                return True
        return super(DisplayReel, self).eventFilter(source, event)

    def init_signals(self): 
        self.displayListWidget.deleted.connect(self.item_deleted)
        self.displayListWidget.fileDropped.connect(self.file_dropped)
        self.displayListWidget.itemDoubleClicked.connect(self.item_double_clicked)

        # actions
        self.arrow_up_action = QtWidgets.QAction(self)
        self.arrow_up_action.setShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Up))
        self.addAction(self.arrow_up_action)
        self.arrow_up_action.triggered.connect(self.up_arrow_pressed)

        self.arrow_down_action = QtWidgets.QAction(self)
        self.arrow_down_action.setShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Down))
        self.addAction(self.arrow_down_action)
        self.arrow_down_action.triggered.connect(self.down_arrow_pressed)

        if self.always_reload:
            self.displayListWidget.itemClicked.connect(self.emit_signal)
        else:
            self.displayListWidget.itemSelectionChanged.connect(self.emit_signal)

    def up_arrow_pressed(self):
        current_row = self.displayListWidget.currentRow()
        if current_row > 0:
            self.displayListWidget.setCurrentRow(current_row - 1)

    def down_arrow_pressed(self):
        current_row = self.displayListWidget.currentRow()
        if current_row < self.displayListWidget.count() - 1:
            self.displayListWidget.setCurrentRow(current_row + 1)

    def item_double_clicked(self, item):
        data = item.data(QtCore.Qt.UserRole)
        if data:
            self.itemDoubleClicked.emit(data)

    def file_dropped(self, links):
        self.fileDropped.emit(links)

    def item_deleted(self, items):
        del_data = []
        for item in items:
            item_data = item.data(QtCore.Qt.UserRole)
            del_data.append(item_data)
        if del_data:
            if self.allow_delete:
                self.delete_selected_images()
            self.deleted.emit(del_data)

    def delete_selected_images(self):
        for item in self.displayListWidget.selectedItems(): 
            self.displayListWidget.takeItem(self.displayListWidget.row(item))

    def emit_signal(self): 
        sels = self.displayListWidget.selectedItems()
        currentItem = None
        if sels:
            currentItem = sels[-1]
        path = ''
        if currentItem: 
            data = currentItem.data(QtCore.Qt.UserRole)
            if isinstance(data, (str, unicode)):
                path = data
            elif isinstance(data, dict):
                path = data.get('filepath')
        self.clicked.emit(path)

    def overlay_still_pixmap(self, pixmapA, pixmapB):
        iconSize = pixmapA.size()
        # resize fg pixmap
        pixmapB = pixmapB.scaled(iconSize, QtCore.Qt.KeepAspectRatio)
        painter = QtGui.QPainter()
        painter.begin(pixmapA)
        painter.drawPixmap((pixmapA.width() - pixmapB.width())*0.5, (pixmapA.height() - pixmapB.height())*0.5, pixmapB)
        painter.end()
        return pixmapA

    def add_proxy_item(self, path):
        item = QtWidgets.QListWidgetItem(self.displayListWidget)
        item.setData(QtCore.Qt.UserRole, path)
        
        movie = QtGui.QMovie(parent=self)
        movie.setFileName(Config.loading_movie)
        movie.frameChanged.connect(lambda: item.setIcon(movie.currentPixmap()))
        self._proxy_items.append((item, movie))

        movie.start()
        self.set_icon_size_hint()
        QtWidgets.QApplication.processEvents()

    def remove_proxy_item(self):
        item, movie = self._proxy_items.pop(0)
        self.displayListWidget.takeItem(self.displayListWidget.row(item))

    def add_item(self, path, data=None): 
        if self._proxy_items:
            item, movie = self._proxy_items.pop(0)
            movie.stop()
            del(movie)
        else:
            item = QtWidgets.QListWidgetItem(self.displayListWidget)
        iconWidget = QtGui.QIcon()
        fn, ext = os.path.splitext(path)
        if ext in Config.vid_ext:
            cv_img = convert_lib.read_one_frame(path, convert_to_rgb=True)
            h, w, ch = cv_img.shape
            bytes_per_line = ch * w
            img = QtGui.QImage(cv_img.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
            pixmap = QtGui.QPixmap.fromImage(img)
            pixmap = self.overlay_still_pixmap(pixmap, QtGui.QPixmap(Config.play_icon))
        else:
            pixmap = QtGui.QPixmap(path)

        if pixmap.isNull():
            pixmap = QtGui.QPixmap(Config.noPreview)
        iconWidget.addPixmap(pixmap, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        item.setIcon(iconWidget)
        if data:
            item.setData(QtCore.Qt.UserRole, data)
        else:
            item.setData(QtCore.Qt.UserRole, path)

        self.set_icon_size()

    def set_icon_size(self):
        direction = self.displayListWidget.flow()
        if direction == QtWidgets.QListView.TopToBottom:
            size = self.displayListWidget.frameGeometry().width()
        else:
            size = self.displayListWidget.frameGeometry().height()
        self.displayListWidget.setIconSize(QtCore.QSize(size-9, size-9))

    def set_icon_size_hint(self):
        direction = self.displayListWidget.flow()
        if direction == QtWidgets.QListView.TopToBottom:
            size = self.displayListWidget.sizeHint().width()
        else:
            size = self.displayListWidget.sizeHint().height()
        self.displayListWidget.setIconSize(QtCore.QSize(size-9, size-9))

    def add_items(self, paths, data=[]):
        if not data:
            data = [None] * len(paths)
        for p, d in zip(paths, data):
            self.add_item(path=p, data=d)

    def get_all_items(self): 
        results = []
        for a in range(self.displayListWidget.count()):
            data = self.displayListWidget.item(a).data(QtCore.Qt.UserRole)
            path = None
            if isinstance(data, (str, unicode)):
                path = data
            elif isinstance(data, dict):
                path = data.get('filepath')
            if path:
                results.append(path)
        return results

    def get_all_data(self): 
        results = []
        for a in range(self.displayListWidget.count()):
            data = self.displayListWidget.item(a).data(QtCore.Qt.UserRole)
            results.append(data)
        return results

    def get_current_item(self):
        currentItem = self.displayListWidget.currentItem()
        if currentItem:
            data = currentItem.data(QtCore.Qt.UserRole)
            if isinstance(data, (str, unicode)):
                return data
            elif isinstance(data, dict):
                return data.get('filepath')

    def clear(self):
        self.displayListWidget.clear()
