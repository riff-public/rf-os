import os
import sys 
from functools import partial
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# import framework modules 
from rf_utils import user_info
from rf_utils.sg import sg_utils
from rf_utils.widget import project_widget
sg = sg_utils.sg 


class FieldSetting: 
    user = ['sg_localuser', 'sg_ad_account', 'name', 'department', 'groups', 'image']
    group = ['sg_department', 'users', 'code', 'sg_department.Department.name', 
            'sg_department.Department.sg_type']
    project_user = ['sg_role', 'user', 'project']


class Config: 
    entity_type = ['Asset', 'Shot', 'Management']


class Crew(QtWidgets.QWidget):
    """docstring for Crew"""
    checked_item = QtCore.Signal(object)

    def __init__(self, mode='default', project=None, parent=None):
        super(Crew, self).__init__(parent=parent)
        self.mode = mode
        self.project_input = project
        self.setup_ui()
        self.init_functions()
        self.init_signals()
        self.project.emit_project_changed()

    def setup_ui(self): 
        self.layout = QtWidgets.QVBoxLayout()
        # project 
        self.project = project_widget.SGProjectComboBox(sg, layout=QtWidgets.QHBoxLayout())
        self.project.allLayout.setStretch(0, 0)
        self.project.allLayout.setStretch(1, 1)

        # tree
        self.radio_layout = QtWidgets.QHBoxLayout()
        self.sup_radio = QtWidgets.QRadioButton('Supervisor')
        self.crew_radio = QtWidgets.QRadioButton('All')

        self.radio_layout.addWidget(self.sup_radio)
        self.radio_layout.addWidget(self.crew_radio)

        self.tree_widget = QtWidgets.QTreeWidget()
        self.button = QtWidgets.QPushButton('OK')
        self.layout.addWidget(self.project)
        self.layout.addWidget(self.tree_widget)
        self.layout.addLayout(self.radio_layout)
        self.layout.addWidget(self.button)
        self.setLayout(self.layout)

    def init_functions(self): 
        if self.mode == 'default': 
            self.crew_radio.setChecked(True)

        if self.mode == 'supervisor': 
            self.sup_radio.setChecked(True)

        if self.project_input: 
            self.project.set_current_item(self.project_input)
        self.users = sg.find('HumanUser', [], FieldSetting.user)
        self.groups = sg.find('Group', [], FieldSetting.group)
        self.roles = sg.find('ProjectUserConnection', [['sg_role', 'is', 'Supervisor']], FieldSetting.project_user)

    def init_signals(self): 
        self.project.projectChanged.connect(self.project_changed)
        self.tree_widget.currentItemChanged.connect(self.item_selected)
        self.button.clicked.connect(self.button_clicked)
        self.sup_radio.clicked.connect(partial(self.mode_selected, 'supervisor'))
        self.crew_radio.clicked.connect(partial(self.mode_selected, 'default'))

    def project_changed(self, project): 
        self.fetch_data(project, self.mode)

    def item_selected(self, item): 
        checked_list = item.data(0, QtCore.Qt.UserRole)

    def button_clicked(self): 
        items = self.find_child_checked_items()
        self.checked_item.emit(items)

    def mode_selected(self, mode): 
        self.mode = mode
        self.project.emit_project_changed()

    def find_child_checked_items(self): 
        def find_children_checked(root, checked_list=[]): 
            children_count = root.childCount()

            if children_count: 
                for i in range(children_count): 
                    children_item = root.child(i)
                    count = children_item.childCount()
                    if not count: 
                        if children_item.checkState(0) == QtCore.Qt.Checked:
                            checked_list.append(children_item)
                    else: 
                        checked_list = find_children_checked(children_item, checked_list)
            return checked_list

        root = self.tree_widget.invisibleRootItem()
        checked_list = find_children_checked(root)

    def fetch_data(self, project, mode): 
        self.tree_widget.blockSignals(True)
        if mode == 'default': 
            self.build_user(project)

        if mode == 'supervisor': 
            self.build_supervisor(project)
        self.tree_widget.blockSignals(False)

    def build_user(self, project): 
        users = self.users 
        groups = self.groups
        root = self.tree_widget.invisibleRootItem()
        root_dict = dict()
        step_dict = dict()
        group_dict = dict()

        for group in groups: 
            group_dict[group['id']] = group 

        self.tree_widget.clear()

        # build type 
        for entity_type in Config.entity_type: 
            if not entity_type in root_dict.keys(): 
                entity_root = self.add_item(root, index=0, display=entity_type, data=entity_type)
                root_dict[entity_type] = entity_root

        for user in users: 
            # 
            user_name = user['name']
            groups = user['groups']

            for group in groups: 
                group = group_dict[group['id']]
                entity_type = group['sg_department.Department.sg_type']

                if entity_type: 
                    if entity_type in root_dict.keys(): 
                        entity_root = root_dict[entity_type]
                        step_name = group['sg_department.Department.name']
                        group_key = '{}-{}'.format(entity_type, step_name)

                        if group_key in step_dict.keys(): 
                            step_root = step_dict[group_key]
                        else: 
                            step_root = self.add_item(entity_root, 0, step_name, group)
                            step_dict[group_key] = step_root
                            step_root.setFlags(step_root.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable)

                        user_item = self.add_item(step_root, 0, user_name, user)
                        user_item.setFlags(user_item.flags() | QtCore.Qt.ItemIsUserCheckable)
                        user_item.setCheckState(0, QtCore.Qt.Unchecked)

    def build_supervisor(self, project): 
        users = self.users 
        groups = self.groups 
        roles = self.roles
        self.tree_widget.clear()
        user_dict = dict()
        step_dict = dict()
        group_dict = dict()
        root = self.tree_widget.invisibleRootItem()

        for user in users: 
            user_dict[user['id']] = user

        for group in groups: 
            group_dict[group['id']] = group 

        for role in roles: 
            if role['project']['id'] == project['id']: 
                user = user_dict[role['user']['id']]
                user_name = user['name']
                groups = user['groups']

                for group in groups: 
                    group = group_dict[group['id']]
                    step_name = group['sg_department.Department.name']
                    
                    if not step_name in step_dict.keys(): 
                        step_root = self.add_item(root, 0, step_name, [user])
                        step_root.setFlags(step_root.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable)
                        step_dict[step_name] = step_root 
                    else: 
                        step_root = step_dict[step_name]
                        usrs = step_root.data(0, QtCore.Qt.UserRole)
                        usrs.append(user)
                        step_root.setData(0, QtCore.Qt.UserRole, usrs)

                    user_item = self.add_item(step_root, 0, user_name, user)
                    user_item.setFlags(user_item.flags() | QtCore.Qt.ItemIsUserCheckable)
                    user_item.setCheckState(0, QtCore.Qt.Unchecked)

    def add_item(self, root, index, display, data): 
        item = QtWidgets.QTreeWidgetItem(root)
        item.setText(index, display)
        item.setData(index, QtCore.Qt.UserRole, data)
        return item


class Dialog(QtWidgets.QDialog): 
    def __init__(self, mode, project=None, parent=None): 
        super(Dialog, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.crew = Crew(mode=mode, project=project)
        self.layout.addWidget(self.crew)
        self.setLayout(self.layout)


def show(mode, project=None): 
    dialog = Dialog(mode=mode, project=project)
    result = dialog.exec_()

