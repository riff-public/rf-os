import os
from collections import OrderedDict 
from Qt import QtWidgets
from Qt import QtCore 
from Qt import QtGui


class Icon: 
    icon_dir = '{0}/core/icons'.format(os.environ['RFSCRIPT'])
    lock = '{}/16/lock_icon.png'.format(icon_dir)
    unlock = '{}/16/unlock_icon.png'.format(icon_dir)


class History(QtWidgets.QWidget):
    """docstring for History"""
    def __init__(self, parent):
        super(History, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.widget = QtWidgets.QListWidget()
        self.layout.addWidget(self.widget)
        self.setLayout(self.layout)

    def set_data(self, data): 
        for entry in data: 
            widget = HistoryItem()
            widget.setToolTip(entry['workspace'])
            widget.set_date(entry['date'])
            widget.set_user(entry['user'])
            widget.set_status(entry['status'])

            item = QtWidgets.QListWidgetItem(self.widget)
            item.setSizeHint(widget.sizeHint())
            item.setData(QtCore.Qt.UserRole, entry)
            self.widget.setItemWidget(item, widget)


class HistoryItem(QtWidgets.QWidget):
    """docstring for HistoryItem"""
    def __init__(self, parent=None):
        super(HistoryItem, self).__init__(parent=parent)
        self.layout = QtWidgets.QHBoxLayout()
        self.time = QtWidgets.QLabel()
        self.user = QtWidgets.QLabel()
        self.user.setAlignment(QtCore.Qt.AlignCenter)
        self.status = QtWidgets.QLabel()
        self.layout.addWidget(self.time)
        self.layout.addWidget(self.user)
        self.layout.addWidget(self.status)
        self.setLayout(self.layout)
        self.layout.setStretch(0, 1)
        self.layout.setStretch(1, 1)
        self.layout.setStretch(2, 1)
        self.layout.setContentsMargins(6, 6, 6, 6)
        self.layout.setSpacing(0)

    def set_date(self, text): 
        self.time.setText(text)

    def set_user(self, text): 
        self.user.setText(text)

    def set_status(self, status): 
        path = Icon.lock if status == 'lock' else Icon.unlock
        self.status.setPixmap(QtGui.QPixmap(path).scaled(16, 16, QtCore.Qt.KeepAspectRatio))





def convert_history_dict(history_data): 
    # data = {'col1':['1','2','3','4'],
     #        'col2':['1','2','1','3'],
     #        'col3':['1','1','2','1']}
     # [{'date': '21-06-07 12:05:58', 'force': False, 'workspace': '', 'TA': 'lock'}]
    headers = list()
    data = OrderedDict()

    for dict_data in history_data: 
        for k, v in dict_data.items(): 
            headers.append(k)

    for k in headers: 
        data[k] = list()
        for dict_data in history_data: 
            v = dict_data[k]
            data[k].append(v)

    return data


def get_row_count(data): 
    """ get row and column count """ 
    row_count = 0 
    for n, key in enumerate(sorted(data.keys())):
        row = 0 
        for m, item in enumerate(data[key]):
            row += 1
            row_count = row if row > row_count else row_count

    return row_count
