import os
import sys
import tempfile

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
try:
    moduleDir = sys._MEIPASS.replace('\\', '/')
except Exception:
    moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')

from rf_utils.sg import sg_thread
from rf_utils.pipeline.convert_lib import read_one_frame

class Config:
    default_unknown_photo = '{}/icons/etc/unknown_person.png'.format(moduleDir)
    movPreview = '{}/icons/etc/mov_preview_icon.png'.format(moduleDir)
    workspacePreview = '{}/icons/etc/workspace_preview_icon.png'.format(moduleDir)
    etcPreview = '{}/icons/etc/etc_preview_icon.png'.format(moduleDir)
    browse_icon = '{}/icons/etc/browse_icon.png'.format(moduleDir)
    paste_icon = '{}/icons/etc/paste_icon.png'.format(moduleDir)
    play_icon = '{}/icons/etc/play_button.png'.format(moduleDir)
    imgExt = ['.jpg', '.png', '.tif', '.gif']
    movExt = ['.mov', '.mp4', '.mpg']
    workspaceExt = ['.psd']

class DropUrlList(QtWidgets.QListWidget):
    """ subclass QListWidget for dragdrop events """
    dropped = QtCore.Signal(list)
    multipleDropped = QtCore.Signal(list)

    def __init__(self, parent=None):
        super(DropUrlList, self).__init__(parent)
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.dropped.emit(links[-1])
            self.multipleDropped.emit(links)
        else:
            event.ignore()

class DropUrlTree(QtWidgets.QTreeWidget):
    """ subclass QTreeWidget for dragdrop events """
    dropped = QtCore.Signal(list)
    multipleDropped = QtCore.Signal(list)

    def __init__(self, parent=None):
        super(DropUrlTree, self).__init__(parent)
        self.setAcceptDrops(True)
    
    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.dropped.emit(links[-1])
            self.multipleDropped.emit(links)
        else:
            event.ignore()

class DropUrlLabel(QtWidgets.QLabel):
    """ subclass QLineEdit for dragdrop events """
    dropped = QtCore.Signal(list)
    multipleDropped = QtCore.Signal(list)
    def __init__(self, parent=None):
        super(DropUrlLabel, self).__init__(parent)
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.dropped.emit(links[-1])
            self.multipleDropped.emit(links)
        else:
            event.ignore()

class Display(QtWidgets.QWidget) :
    dropped = QtCore.Signal(str)
    multipleDropped = QtCore.Signal(list)
    def __init__(self) :
        super(Display, self).__init__()

        self.allLayout = QtWidgets.QVBoxLayout()
        self.display = DropUrlLabel()
        self.allLayout.addWidget(self.display)

        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)
        self.display.setAlignment(QtCore.Qt.AlignCenter)
        self.currentPath = ''

        self.display.multipleDropped.connect(self.call_back)
        self.customExt = dict()

    def set_label(self, text):
        self.display.setText(text)

    def call_back(self, inputs):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        if isinstance(inputs, (list, tuple)):
            self.set_display(inputs[-1])
            self.dropped.emit(inputs[-1])
        else:
            self.set_display(inputs)
            self.dropped.emit(inputs)
        self.multipleDropped.emit(inputs)

        QtWidgets.QApplication.restoreOverrideCursor()

    def set_display(self, path):
        self.currentPath = path
        pixmap = self.get_preview_image(path)
        width = self.display.frameGeometry().width()
        height = self.display.frameGeometry().height()
        return self.display.setPixmap(pixmap.scaled(width, height, QtCore.Qt.KeepAspectRatio))

    def set_style(self, style):
        self.setStyleSheet(style)

    def clear(self):
        self.display.clear()

    def overlay_still_pixmap(self, pixmapA, pixmapB):
        iconSize = pixmapA.size()
        # resize fg pixmap
        pixmapB = pixmapB.scaled(iconSize, QtCore.Qt.KeepAspectRatio)
        painter = QtGui.QPainter()
        painter.begin(pixmapA)
        painter.drawPixmap((pixmapA.width() - pixmapB.width())*0.5, (pixmapA.height() - pixmapB.height())*0.5, pixmapB)
        painter.end()
        return pixmapA

    def get_preview_image(self, path):
        name, ext = os.path.splitext(path)
        ext = ext.lower()  # need to support weird extensions like, .JPG, .PNG with upper case
        if ext in Config.imgExt:
            return QtGui.QPixmap(path)
        elif ext in Config.movExt:
            cv_img = read_one_frame(path, convert_to_rgb=True)
            h, w, ch = cv_img.shape
            bytes_per_line = ch * w
            img = QtGui.QImage(cv_img.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
            pixmap = QtGui.QPixmap.fromImage(img)
            pixmap = self.overlay_still_pixmap(pixmap, QtGui.QPixmap(Config.play_icon))
            return pixmap
        elif ext in Config.workspaceExt:
            return QtGui.QPixmap(Config.workspacePreview)
        elif ext in self.customExt:
            return QtGui.QPixmap(self.customExt[ext])
        else:
            return QtGui.QPixmap(Config.etcPreview)

class PhotoFrame(QtWidgets.QLabel):
    def __init__(self, parent=None, unknown_photo=Config.default_unknown_photo):
        super(PhotoFrame, self).__init__(parent)
        self.unknown_photo = unknown_photo
        self.imagePath = None
        self.imap = None
        self.ipixmap = QtGui.QPixmap()
        self._circular = False

    @property
    def circular(self):
        return self._circular

    @circular.setter
    def circular(self, value):
        self._circular = value

    def reset(self):
        self.update_photo(self.unknown_photo)

    def resizeEvent(self, event):
        if not self.ipixmap or self.ipixmap.isNull():
            return 0

        self.set_image()
        
    def update_photo(self, imagePath):
        self.imagePath = imagePath
        self.imap = QtGui.QImage(imagePath)
        self.set_image()

    def set_image(self):
        self.ipixmap = QtGui.QPixmap()
        if not self.imap.isNull():
            # make circular
            if self.circular:
                # convert image to 32-bit ARGB (adds an alpha channel ie transparency factor):
                crop_image = self.imap.copy()
                crop_image.convertToFormat(QtGui.QImage.Format_ARGB32)
                # Crop image to a square:
                crop_imgsize = min(crop_image.width(), crop_image.height())
                rect = QtCore.QRect((crop_image.width() - crop_imgsize) / 2,
                                    (crop_image.height() - crop_imgsize) / 2,
                                    crop_imgsize,
                                    crop_imgsize)
                crop_image = crop_image.copy(rect)
                # Create the output image with the same dimensions 
                # and an alpha channel and make it completely transparent:
                out_img = QtGui.QImage(crop_imgsize, crop_imgsize, QtGui.QImage.Format_ARGB32)
                out_img.fill(QtCore.Qt.transparent)
              
                # Create a texture brush and paint a circle with the original image onto the output image:
                brush = QtGui.QBrush(crop_image)
                
                # initiate QPainter
                painter = QtGui.QPainter(out_img)
                painter.setBrush(brush)  # Paint the output image
                painter.setPen(QtCore.Qt.NoPen)  # Don't draw an outline
              
                # drawing circle
                painter.drawEllipse(0, 0, crop_imgsize, crop_imgsize)
                self.imap = out_img
            try:
                self.ipixmap = QtGui.QPixmap(self.imap).scaled(self.size(), QtCore.Qt.KeepAspectRatio)
                self.setPixmap(self.ipixmap)
            except Exception as e:
                pass

class SGPhotoFrame(PhotoFrame):
    def __init__(self, parent=None, unknown_photo=Config.default_unknown_photo):
        super(SGPhotoFrame, self).__init__(parent, unknown_photo=unknown_photo)
        self.downloadThread =None
        self.threads = []

        self.reset()

    def show(self, image_link):
        fh, des = tempfile.mkstemp(suffix='.png')
        os.close(fh)
        downloadList = [(self, image_link, des)]
        self.download_image(downloadList=downloadList)

    def download_image(self, downloadList):
        self.downloadThread = sg_thread.DownloadImage(downloadList)
        self.downloadThread.update.connect(self.download_image_finished)
        self.downloadThread.start()
        self.threads.append(self.downloadThread)

    def download_image_finished(self, data):
        item, imagePath = data
        self.update_photo(imagePath)
        os.remove(imagePath)
    
class EditablePhotoFrame(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(EditablePhotoFrame, self).__init__(parent)
        self.setup_ui()
        self.init_signals()

    def setup_ui(self):
        self.allLayout = QtWidgets.QVBoxLayout()
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)
        self.setAcceptDrops(True)  # make it dropable

        self.photo_label = PhotoFrame(self)
        self.photo_label.setMinimumSize(QtCore.QSize(256, 256))
        self.allLayout.addWidget(self.photo_label)

        # photo buttons
        self.displayButton_layout = QtWidgets.QHBoxLayout()
        self.displayButton_layout.setSpacing(5)
        self.allLayout.addLayout(self.displayButton_layout)
        spacerItem3 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.displayButton_layout.addItem(spacerItem3)

        # paste button
        self.paste_button = QtWidgets.QPushButton()
        self.paste_button.setIcon(QtGui.QIcon(Config.paste_icon))
        self.paste_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.displayButton_layout.addWidget(self.paste_button)

        # browse button
        self.browse_button = QtWidgets.QPushButton()
        self.browse_button.setIcon(QtGui.QIcon(Config.browse_icon))
        self.browse_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.displayButton_layout.addWidget(self.browse_button)

        self.paste_button.setToolTip('Paste the image in clipboard')
        self.browse_button.setToolTip('Browse for image in local drive')
        self.setToolTip('Add image by doing one of the following:\n  - Drag and drop files\n  - Copy image to clipboard and press "Paste" button\n  - Browse using "Browse" button')

    def init_signals(self):
        self.paste_button.clicked.connect(self.update_from_clipboard)
        self.browse_button.clicked.connect(self.browse_update)

    def update_photo(self, imagePath):
        self.photo_label.update_photo(imagePath=imagePath)

    def dragEnterEvent(self, event):
        mimeData = event.mimeData()
        if mimeData.hasUrls() or mimeData.hasImage():
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        mimeData = event.mimeData()
        if mimeData.hasUrls() or mimeData.hasImage():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        mimeData = event.mimeData()
        # image data
        if mimeData.hasImage():
            self.photo_label.imagePath = None
            self.photo_label.imap = QtGui.QImage(event.mimeData().imageData())
            self.photo_label.set_image()
            event.accept()
            return
        # url data
        elif mimeData.hasUrls():  
            url = mimeData.urls()[0]
            scheme = url.scheme()
            if scheme == 'file':
                path = str(url.toLocalFile())
                self.photo_label.update_photo(imagePath=path)
                fp = self.to_file()
                print(fp)
                event.accept()
                return
        event.ignore()

    def browse_update(self, iconPath=None):
        dialog = QtWidgets.QFileDialog()
        if iconPath:
            dialog.setWindowIcon(QtGui.QIcon(iconPath))
        dialog.setWindowTitle('Select user photo')
        dialog.setNameFilters(["PNG (*.png *.PNG)", "JPEG (*.jpg *.JPG)", "TIFF (*.tiff *.TIFF)"])
        dialog.setFileMode(QtWidgets.QFileDialog.ExistingFile)
        dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptOpen) 
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            chosen_path = dialog.selectedFiles()[0]
            self.photo_label.update_photo(imagePath=chosen_path)

    def update_from_clipboard(self):
        image = QtWidgets.QApplication.clipboard().image()
        if not image.isNull():
            self.photo_label.imap = image
            self.photo_label.set_image()
            return True

    def to_file(self, path=None):
        if not self.photo_label.ipixmap:
            return 
        if not path:
            fh, path = tempfile.mkstemp(suffix='.png')
            os.close(fh)
        path = path.replace('\\', '/')
        self.photo_label.ipixmap.save(path, 'PNG', 100)
        return path

    def has_photo(self):
        return True if not self.photo_label.ipixmap.isNull() else False
