import os
import sys
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class Color:
    lightGrey = [200, 200, 200]
    yellow = [220, 220, 100]
    grey = [160, 160, 160]
    green = [100, 220, 100]
    red = [220, 100, 100]
    orange = [220, 160, 100]


class ExportListWidget(QtWidgets.QWidget):
    status = QtCore.Signal(str)
    checkBoxChecked = QtCore.Signal(list)
    selected = QtCore.Signal(str)
    itemSelectionChanged = QtCore.Signal(dict)

    def __init__(self, parent=None) :
        super(ExportListWidget, self).__init__(parent)
        self.ui()
        self.init_signals()

    def ui(self):
        self.layout = QtWidgets.QVBoxLayout()
        self.listWidget = QtWidgets.QListWidget()
        self.layout.addWidget(self.listWidget)

        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

    def init_signals(self):
        self.listWidget.itemSelectionChanged.connect(self.item_selected)

    def item_selected(self):
        item = self.listWidget.currentItem()
        if item:
            data = item.data(QtCore.Qt.UserRole)
            self.selected.emit(data)
            self.itemSelectionChanged.emit(data)
        return item

    def add_item(self, display, data):
        item = QtWidgets.QListWidgetItem(self.listWidget)
        item.setText(display)
        item.setData(QtCore.Qt.UserRole, data)
        item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
        item.setCheckState(QtCore.Qt.Checked)
        return item

    def add_export_Item(self, shotName, exportName, exportType, data):
        widgetItem = ExportItem()
        item = QtWidgets.QListWidgetItem(self.listWidget)
        item.setSizeHint(widgetItem.sizeHint())

        widgetItem.set_text1(shotName)
        widgetItem.set_text2(exportName)
        widgetItem.set_text3(exportType)
        item.setData(QtCore.Qt.UserRole, data)
        self.listWidget.setItemWidget(item, widgetItem)

        widgetItem.checkBoxChecked.connect(partial(self.checkBox_checked, item, widgetItem, data))

        return item, widgetItem

    def checkBox_checked(self, item, widgetItem, data, state):
        self.checkBoxChecked.emit([item, widgetItem, data, state])

    def widget_items(self):
        return [self.listWidget.itemWidget(a) for a in self.items(all=True)]

    def clear(self):
        self.listWidget.clear()

    def data(self, select=False, all=False):
        items = []
        items = self.items(select=select, all=all)
        return [a.data(QtCore.Qt.UserRole) for a in items] if items else []

    def items(self, select=False, all=False):
        items = []
        if select:
            items = self.listWidget.selectedItems()
        if all:
            items = [self.listWidget.item(a) for a in range(self.listWidget.count())]
        return [a for a in items] if items else []

    def checked_items(self):
        items = [a for a in self.items(all=True) if a.checkState() == QtCore.Qt.Checked]
        return items

    def set_checked_items(self, items, state): 
        value = QtCore.Qt.Checked if state else QtCore.Qt.Unchecked
        for item in items: 
            item.setCheckState(value)


class ExportItem(QtWidgets.QWidget):
    """docstring for ExportItem"""
    checkBoxChecked = QtCore.Signal(bool)

    def __init__(self, parent=None) :
        super(ExportItem, self).__init__(parent)

        self.layout = QtWidgets.QHBoxLayout()

        self.checkBox = QtWidgets.QCheckBox()
        self.layout.addWidget(self.checkBox)

        self.vLayout = QtWidgets.QVBoxLayout()

        self.hLayout = QtWidgets.QHBoxLayout()
        self.statusLayout = QtWidgets.QHBoxLayout()

        self.icon = QtWidgets.QLabel()
        self.text1 = QtWidgets.QLabel()
        self.text2 = QtWidgets.QLabel()
        self.text3 = QtWidgets.QLabel()

        self.status = QtWidgets.QLabel()
        self.text4 = QtWidgets.QLabel()

        self.vLayout.addLayout(self.hLayout)

        self.hLayout.addWidget(self.text1)
        self.hLayout.addWidget(self.text2)
        self.vLayout.addWidget(self.text3)
        self.vLayout.addLayout(self.statusLayout)

        self.statusLayout.addWidget(self.status)
        self.statusLayout.addWidget(self.text4)

        self.hLayout.setContentsMargins(0, 0, 0, 0)
        self.vLayout.setContentsMargins(0, 0, 0, 0)
        self.hLayout.setSpacing(8)
        self.vLayout.setSpacing(0)

        self.hLayout.setStretch(0, 1)
        self.hLayout.setStretch(1, 4)

        self.layout.addWidget(self.icon)
        self.layout.addLayout(self.vLayout)

        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 0)
        self.layout.setStretch(2, 1)

        self.layout.setContentsMargins(4, 4, 4, 4)
        self.setLayout(self.layout)

        self.text1.setFont(QtGui.QFont("Arial", 9, QtGui.QFont.Bold))
        self.text2.setFont(QtGui.QFont("Arial", 9, QtGui.QFont.Bold))

        self.set_text2_color(Color.lightGrey)
        self.set_text3_color(Color.grey)
        self.set_status_color(Color.green)
        # self.text1.setFont(QtGui.QFont("Arial", 8))

        self.init_signals()

    def init_signals(self):
        self.checkBox.stateChanged.connect(self.checkBox_checked)

    def checkBox_checked(self):
        self.checkBoxChecked.emit(self.is_checked())

    def lock(self, state):
        self.checkBox.setEnabled(state)

    def set_icon(self, path, size=[16, 16]):
        self.icon.setPixmap(QtGui.QPixmap(path).scaled(size[0], size[1], QtCore.Qt.KeepAspectRatio))

    def set_text1(self, text):
        self.text1.setText(text)

    def set_text2(self, text):
        self.text2.setText(text)

    def set_text3(self, text):
        self.text3.setText(text)

    def set_text4(self, text):
        self.text4.setText(text)

    def set_status(self, text):
        self.status.setText(text)

    def get_text1(self):
        return self.text1.text()

    def get_text2(self):
        return self.text2.text()

    def get_text3(self):
        return self.text3.text()

    def get_text4(self):
        return self.text4.text()

    def is_checked(self):
        return self.checkBox.isChecked()

    def set_checked(self, state):
        self.checkBox.setChecked(state)

    def set_text1_color(self, colors):
        self.text1.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text2_color(self, colors):
        self.text2.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text3_color(self, colors):
        self.text3.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text4_color(self, colors):
        self.text4.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_status_color(self, colors):
        self.status.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_status_bgcolor(self, colors):
        self.status.setStyleSheet('background-color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_enabled(self, state):
        self.checkBox.setEnabled(state)




