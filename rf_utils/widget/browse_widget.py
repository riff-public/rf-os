import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class DropUrlLineEdit(QtWidgets.QLineEdit):
    """ subclass QLineEdit for dragdrop events """
    dropped = QtCore.Signal(list)
    def __init__(self, parent=None):
        super(DropUrlLineEdit, self).__init__(parent)
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.dropped.emit(links[-1])
        else:
            event.ignore()


class BrowseFile(QtWidgets.QWidget) :
    textChanged = QtCore.Signal(str)
    def __init__(self, title='File Browse', path=str(), filter=str(), parent=None) :
        super(BrowseFile, self).__init__()
        self.title = title
        self.path = path
        self.filter = filter
        if not self.filter:
            self.filter = 'Media(*.png *.tif *.exr *.jpg *.jpeg)'

        # layout
        self.allLayout = QtWidgets.QHBoxLayout()
        # label
        self.label = QtWidgets.QLabel()
        # display listWidget
        self.lineEdit = DropUrlLineEdit()
        self.button = QtWidgets.QPushButton()


        # add layout
        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.lineEdit)
        self.allLayout.addWidget(self.button)
        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allLayout)

        # set name
        self.button.setText('Browse ...')

        self.init_signals()

    def set_label(self, text):
        self.label.setText(text)

    def set_button_name(self, text):
        self.button.setText(test)

    def init_signals(self):
        self.lineEdit.dropped.connect(self.show_file)
        self.button.clicked.connect(self.browse)
        self.lineEdit.returnPressed.connect(self.return_pressed)

    def browse(self):
        current_path = self.lineEdit.text()
        if current_path: 
            self.path = os.path.dirname(current_path)

        result = QtWidgets.QFileDialog.getOpenFileName(self, self.title, self.path, self.filter)
        path = result[0]
        if path:
            self.set_path(path)

    def show_file(self, path):
        self.set_path(path)

    def set_path(self, path):
        self.lineEdit.setText(path)
        self.textChanged.emit(path)

    def return_pressed(self):
        path = str(self.lineEdit.text())
        self.textChanged.emit(path)

    def current_text(self):
        return str(self.lineEdit.text())

    def set_visible(self, state):
        self.label.setVisible(state)
        self.lineEdit.setVisible(state)
        self.button.setVisible(state)


class BrowseFolder(BrowseFile) :
    textChanged = QtCore.Signal(str)
    def __init__(self, title='Directory Browse', path=str(), filter=str(), parent=None) :
        super(BrowseFolder, self).__init__(title='Directory Browse', path=str(), filter=str(), parent=None)

    def browse(self):
        result = QtWidgets.QFileDialog.getExistingDirectory(self, self.title, self.path, QtWidgets.QFileDialog.ShowDirsOnly)
        path = result
        if path:
            self.set_path(path)
