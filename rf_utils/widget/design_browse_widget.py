import os
import sys
import traceback
from collections import OrderedDict
from functools import partial 

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import file_comboBox

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

notStartwith = ['.']

class DesignNavigation(QtWidgets.QWidget):
    completed = QtCore.Signal(str)
    def __init__(self, projectInfo=None, parent=None) :
        super(DesignNavigation, self).__init__()
        self.projectInfo = projectInfo
        
        self.layout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('To Asset : ')

        self.typeWidget = file_comboBox.FileComboBox()
        self.typeWidget.set_label('Asset Type')
        self.assetWidget = file_comboBox.FileComboBox()
        self.assetWidget.set_label('Design Name')
        # spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        self.layout.addWidget(self.typeWidget)
        self.layout.addWidget(self.assetWidget)
        # self.layout.addItem(spacerItem)

        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)

                ## set filters 
        self.typeWidget.notStartwith = notStartwith
        self.assetWidget.notStartwith = notStartwith

        self.init_signals()


    def init_signals(self): 
        self.typeWidget.currentIndexChanged.connect(self.trigger_asset)
        self.assetWidget.currentIndexChanged.connect(self.emit_signal)

    def list_items(self, path): 
        self.typeWidget.list_items(path) 

    def trigger_asset(self, path): 
        self.assetWidget.set_path(path)

    def emit_signal(self, path): 
        self.completed.emit(path)



def list_file(path, ext=[]): 
    result = sorted([d for d in os.listdir(path) if os.path.isfile(os.path.join(path, d))]) if os.path.exists(path) else []
    return [a for a in result if os.path.splitext(a) in ext] if ext else result


def list_folder(path, notStartwith=[]): 
    result = sorted([d for d in os.listdir(path) if os.path.isdir(os.path.join(path,d))]) if os.path.exists(path) else []
    return [a for a in result if all(not b in a for b in notStartwith)] if notStartwith else result
                