import os
import sys
import traceback
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
try:
    module_dir = sys._MEIPASS
except Exception:
    module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class SGEpisodeComboBox(QtWidgets.QWidget):
    """docstring for SGEpisodeComboBox"""
    episodeChanged = QtCore.Signal(dict)
    def __init__(self, sg, layout=None, parent=None):
        super(SGEpisodeComboBox, self).__init__(parent)
        self.sg = sg
        self.project = None
        self.episodes = list()
        
        # layout
        self.layout = layout if layout else QtWidgets.QVBoxLayout()
        self.sublayout = QtWidgets.QHBoxLayout()

        # display listWidget
        self.episode = QtWidgets.QComboBox()
        self.label = QtWidgets.QLabel('Episode')
        self.create = QtWidgets.QPushButton('+')
        self.create.setMaximumSize(QtCore.QSize(25, 20))

        # add layout
        self.sublayout.addWidget(self.episode)
        self.sublayout.addWidget(self.create)

        self.layout.addWidget(self.label)
        self.layout.addLayout(self.sublayout)
        self.layout.setSpacing(5)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)
        self.create.clicked.connect(self.create_episode)
        self.episode.currentIndexChanged.connect(self.episode_changed)


    def fetch_episodes(self, project): 
        filters = [['project', 'is', project]]
        fields = ['code', 'id', 'shortCode', 'project']
        self.episodes = self.sg.find('Scene', filters, fields)
        return self.episodes 

    def set_episodes(self, project): 
        self.project = project
        if project: 
            episodes = self.fetch_episodes(project)
            self.episode.clear()

            for i, ep in enumerate(episodes): 
                self.episode.addItem(ep['code'])
                self.episode.setItemData(i, ep, QtCore.Qt.UserRole)
        self.episode_changed()

    def episode_changed(self): 
        self.episodeChanged.emit(self.episode_entity())

    def create_episode(self): 
        ep, status = QtWidgets.QInputDialog.getText(self, 'Episode Creation', 'Enter episode name:')
        if status: 
            if self.episodes: 
                if ep in [a['code'] for a in self.episodes]: 
                    dialog = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, 'Warning', 'Name exists', QtWidgets.QMessageBox.Ok)
                    result = dialog.exec_()
                    self.create_episode()
                    return 

            create_episode(self.project, ep)
            self.set_episodes(self.project)

    def current_episode(self): 
        return self.episode.currentText()

    def episode_entity(self): 
        return self.episode.itemData(self.episode.currentIndex(), QtCore.Qt.UserRole)



def create_episode(project, name): 
    print('create project', project, name)
    from rf_utils.pipeline import create_scene 
    return create_scene.create_episode(project, name)
