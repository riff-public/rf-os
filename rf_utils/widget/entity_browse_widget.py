import os
import sys
import traceback
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from . import file_comboBox
from . import sg_widget

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

notStartwith = ['.', '_']


class Cache:
    cacheDir = '%s/widget_cache' % os.path.expanduser('~')
    cacheFile = '%s/%s.yml' % (cacheDir, __name__)


class DesignNavigation(QtWidgets.QWidget):
    completed = QtCore.Signal(str)
    def __init__(self, projectInfo=None, parent=None) :
        super(DesignNavigation, self).__init__()
        self.projectInfo = projectInfo

        self.layout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('To Asset : ')

        self.typeWidget = file_comboBox.FileComboBox()
        self.typeWidget.set_label('Type')
        self.entityWidget = file_comboBox.FileComboBox()
        self.entityWidget.set_label('Design Name')
        # spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        self.layout.addWidget(self.typeWidget)
        self.layout.addWidget(self.entityWidget)
        # self.layout.addItem(spacerItem)

        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)

                ## set filters
        self.typeWidget.notStartwith = notStartwith
        self.entityWidget.notStartwith = notStartwith

        self.init_signals()


    def init_signals(self):
        self.typeWidget.currentIndexChanged.connect(self.trigger_asset)
        self.entityWidget.currentIndexChanged.connect(self.emit_signal)

    def list_items(self, path):
        self.typeWidget.list_items(path)

    def trigger_asset(self, path):
        self.entityWidget.set_path(path)

    def emit_signal(self, path):
        self.completed.emit(path)


class AssetNavigation(QtWidgets.QWidget):
    completed = QtCore.Signal(str)
    def __init__(self, projectInfo=None, parent=None) :
        super(AssetNavigation, self).__init__()
        self.projectInfo = projectInfo

        self.layout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('To Asset : ')

        self.typeWidget = file_comboBox.FileComboBox()
        self.typeWidget.set_label('Type')

        self.subtypeWidget = file_comboBox.FileComboBox()
        self.subtypeWidget.set_label('SubType')

        self.entityWidget = file_comboBox.FileComboBox()
        self.entityWidget.set_label('Asset Name')
        # spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        self.layout.addWidget(self.typeWidget)
        self.layout.addWidget(self.subtypeWidget)
        self.layout.addWidget(self.entityWidget)
        # self.layout.addItem(spacerItem)

        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)

                ## set filters
        self.typeWidget.notStartwith = notStartwith
        self.subtypeWidget.notStartwith = notStartwith
        self.entityWidget.notStartwith = notStartwith

        self.completePath = None

        self.init_signals()


    def init_signals(self):
        self.typeWidget.currentIndexChanged.connect(self.trigger_subtype)
        self.subtypeWidget.currentIndexChanged.connect(self.trigger_asset)
        self.entityWidget.currentIndexChanged.connect(self.emit_signal)

    def list_items(self, path):
        self.typeWidget.list_items(path)

    def trigger_subtype(self, path):
        self.subtypeWidget.set_path(path)

    def trigger_asset(self, path):
        self.entityWidget.set_path(path)

    def set_visible(self, state):
        self.typeWidget.set_visible(state)
        self.subtypeWidget.set_visible(state)
        self.entityWidget.set_visible(state)

    def set_enable(self, state):
        self.typeWidget.set_enable(state)
        self.subtypeWidget.set_enable(state)
        self.entityWidget.set_enable(state)

    def emit_signal(self, path):
        self.completed.emit(path)
        self.completePath = path


class SGSceneNavigation(QtWidgets.QWidget):
    """docstring for SGSceneNavigation"""
    completed = QtCore.Signal(dict)
    episodeTriggered = QtCore.Signal(dict)
    shotSignal = QtCore.Signal(object)

    def __init__(self, sg, shotFilter=True, thread=False, layout=None, parent=None):
        super(SGSceneNavigation, self).__init__(parent=parent)

        self.sg = sg
        self.layout = QtWidgets.QVBoxLayout() if not layout else layout

        # shot filtering
        self.shotExtraFilter = []
        self.shotExtraField = []
        self.shotFilter = shotFilter

        # widgets
        self.episodeLabel = QtWidgets.QLabel('Episode')
        self.episodeWidget = sg_widget.SGComboBox('code')

        self.sequenceLabel = QtWidgets.QLabel('Sequence')
        self.sequenceFilter = QtWidgets.QComboBox()

        self.shotLabel = QtWidgets.QLabel('Shot')
        self.shotWidget = sg_widget.SGComboBox('code')

        # add layout
        self.layout.addWidget(self.episodeLabel)
        self.layout.addWidget(self.episodeWidget)

        self.layout.addWidget(self.sequenceLabel)
        self.layout.addWidget(self.sequenceFilter)

        self.layout.addWidget(self.shotLabel)
        self.layout.addWidget(self.shotWidget)

        # set layout
        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)

        self.threadPool = QtCore.QThreadPool()
        self.init_signals()
        self.init_functions()

    def init_signals(self):
        self.episodeWidget.currentIndexChanged.connect(self.episode_trigger)

        if self.shotFilter:
            self.sequenceFilter.currentIndexChanged.connect(self.list_shot)
        self.shotWidget.currentIndexChanged.connect(self.shot_trigger)
        self.episodeWidget.noItem.connect(self.no_episode)

    def init_functions(self):
        if not self.shotFilter:
            self.sequenceLabel.setVisible(False)
            self.sequenceFilter.setVisible(False)

    def list_episodes(self, projectEntity):
        """ list episode """
        episodes = find_episode(self.sg, projectEntity)
        self.episodeWidget.add_items(episodes)


    def episode_trigger(self, epDict):
        """ find shots """
        shots = self.find_shot(epDict)

        # filter mode
        if self.shotFilter:
            self.shot_filtering(shots)

        # direct mode, show shot without filtering
        else:
            self.shotWidget.add_items(shots)

        # emit signal
        self.episodeTriggered.emit(epDict)

    def shot_trigger(self, shotDict):
        self.completed.emit(shotDict)

    def emit_entity(self):
        self.completed.emit(self.shotWidget.current_item())

    def no_episode(self):
        self.sequenceFilter.clear()
        self.shotWidget.comboBox.clear()

    def find_shot(self, epDict):
        filters = [['project', 'is', epDict['project']], ['sg_episode', 'is', epDict]]
        if self.shotExtraFilter:
            filters.append(self.shotExtraFilter)
        fields = ['code', 'id', 'project', 'sg_episode', 'sg_sequence_code', 'sg_shortcode', 'sg_shot_type']
        if self.shotExtraField:
            fields += self.shotExtraField

        # cache
        self.shotEntities = self.sg.find('Shot', filters, fields)
        return self.shotEntities

    def shot_filtering(self, shots):
        seqs = sorted(list(set([a['sg_sequence_code'] for a in shots])))
        self.sequenceFilter.clear()
        self.sequenceFilter.addItems(seqs)

    def list_shot(self):
        """ list shot according from filter and use cache data """
        seqFilter = str(self.sequenceFilter.currentText())
        shotEntities = [a for a in self.shotEntities if a['sg_sequence_code'] == seqFilter]
        self.shotWidget.add_items(shotEntities)
        self.shotSignal.emit(shotEntities)

    def set_enabled(self, state):
        self.episodeWidget.comboBox.setEnabled(state)
        self.sequenceFilter.setEnabled(state)
        self.shotWidget.comboBox.setEnabled(state)

    def current_episode(self):
        return self.episodeWidget.comboBox.currentText()

    def current_sequence(self):
        return self.sequenceFilter.currentText()


class SGSceneNavigation2(SGSceneNavigation):
    """ add save selection feature """
    def __init__(self, sg, shotFilter=True, thread=False, parent=None):
        super(SGSceneNavigation2, self).__init__(sg, shotFilter=shotFilter, parent=parent)


    def list_episodes(self, projectEntity):
        super(SGSceneNavigation2, self).list_episodes(projectEntity)
        episode = read_last_save('episode')

        self.episodeWidget.set_current_item(episode)

    def shot_filtering(self, shots):
        # save episode if not index == 0
        index = self.episodeWidget.comboBox.currentIndex()
        if not index == 0:
            episode = self.episodeWidget.current_text()
            save_selection(episode=episode)

        super(SGSceneNavigation2, self).shot_filtering(shots)

        # set last save
        sequence = read_last_save('sequence')
        items = [self.sequenceFilter.itemText(a) for a in range(self.sequenceFilter.count())]

        if sequence in items:
            index = items.index(sequence)
            self.sequenceFilter.setCurrentIndex(index)

    def list_shot(self):
        # save episode if not index == 0
        episode = self.episodeWidget.current_text()
        index = self.sequenceFilter.currentIndex()
        if  index > 0 and episode:
            sequence = self.sequenceFilter.itemText(index)
            save_selection(episode=episode, sequence=sequence)

        super(SGSceneNavigation2, self).list_shot()
        shot = read_last_save('shot')
        self.shotWidget.set_current_item(shot)

    def shot_trigger(self, shotDict):
        """ override and add save selection """
        # save episode if not index == 0
        index = self.shotWidget.comboBox.currentIndex()
        if not index == 0:
            episode = self.episodeWidget.current_text()
            seqIndex = self.sequenceFilter.currentIndex()
            sequence = self.sequenceFilter.itemText(seqIndex)
            shot = self.shotWidget.comboBox.itemText(index)
            save_selection(episode=episode, sequence=sequence, shot=shot)

        super(SGSceneNavigation2, self).shot_trigger(shotDict)


class SGAssetNavigationUI(QtWidgets.QWidget):
    """docstring for SGAssetNavigation"""
    def __init__(self, parent=None):
        super(SGAssetNavigationUI, self).__init__(parent=parent)

        self.layout = QtWidgets.QVBoxLayout()

        # widgets
        self.typeLabel = QtWidgets.QLabel('Type')
        self.typeWidget = QtWidgets.QComboBox()

        self.subtypeLabel = QtWidgets.QLabel('SubType')
        self.subtypeWidget = QtWidgets.QComboBox()

        self.assetLabel = QtWidgets.QLabel('Asset')
        self.assetWidget = sg_widget.SGComboBox('code')

        # add layout
        self.layout.addWidget(self.typeLabel)
        self.layout.addWidget(self.typeWidget)

        self.layout.addWidget(self.subtypeLabel)
        self.layout.addWidget(self.subtypeWidget)

        self.layout.addWidget(self.assetLabel)
        self.layout.addWidget(self.assetWidget)

        # set layout
        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)

    def set_visible(self, state):
        self.typeLabel.setVisible(state)
        self.typeWidget.setVisible(state)
        self.subtypeLabel.setVisible(state)
        self.subtypeWidget.setVisible(state)
        self.assetLabel.setVisible(state)
        self.assetWidget.setVisible(state)


class SGSceneNavigationList(SGSceneNavigation):
    """ add save selection feature """
    def __init__(self, sg, shotFilter=True, thread=False, parent=None):
        super(SGSceneNavigationList, self).__init__(sg, shotFilter=shotFilter, parent=parent)
        self.shotListWidget = QtWidgets.QListWidget()

    def list_shot(self):
        """ list shot according from filter and use cache data """
        super(SGSceneNavigationList, self).list_shot()
        seqFilter = str(self.sequenceFilter.currentText())
        shotEntities = [a for a in self.shotEntities if a['sg_sequence_code'] == seqFilter]
        self.shotWidget.add_items(shotEntities)


class SGAssetNavigation(SGAssetNavigationUI):
    """docstring for SGAssetNavigation"""
    completed = QtCore.Signal(dict)

    def __init__(self, parent=None):
        super(SGAssetNavigation, self).__init__(parent=parent)
        from rf_utils.sg import sg_utils
        self.sg = sg_utils
        # caches
        self.entities = OrderedDict()

        self.init_signals()

    def init_signals(self):
        self.typeWidget.currentIndexChanged.connect(self.type_changed)
        self.subtypeWidget.currentIndexChanged.connect(self.subtype_changed)
        self.assetWidget.currentIndexChanged.connect(self.asset_changed)

    def type_changed(self):
        """ type changed, list subtype """
        curType = str(self.typeWidget.currentText())
        subtypes = self.relationFilter.get(curType) or []
        self.list_subtype(subtypes)

    def subtype_changed(self):
        """ if subtype changed, filter assets """
        typeFilter = str(self.typeWidget.currentText())
        subtypeFilter = str(self.subtypeWidget.currentText())
        filterEntities = self.sg.filter_entities(self.entities[self.projectEntity['name']], (typeFilter, 'sg_asset_type'), (subtypeFilter, 'sg_subtype'))
        self.list_asset(filterEntities)

    def asset_changed(self, entity):
        self.completed.emit(entity)

    def list_type(self, entities):
        # type filters
        self.typeWidget.clear()
        self.typeWidget.addItems(self.relationFilter.keys())

    def list_subtype(self, subtypes):
        self.subtypeWidget.clear()
        self.subtypeWidget.addItems(subtypes)
        # print subtypes

    def list_asset(self, entities):
        """ list asset from input entities """
        self.assetWidget.add_items(entities)
        # print [a['code'] for a in entities]

    def populate(self, projectEntity):
        """ list only asset in type """
        self.projectEntity = projectEntity
        projectName = self.projectEntity['name']
        self.entities = self.find_assets(projectEntity)
        entities = self.entities[projectName]
        self.relationFilter = self.sg.find_attr_relation(entities, 'sg_asset_type', 'sg_subtype')
        self.list_type(entities)

    def find_assets(self, projectEntity):
        """ find assets and store in caches """
        if not self.entities.get(projectEntity['name']):
            entities = self.sg.find_all_assets(projectEntity)
            self.entities[projectEntity['name']] = entities
        return self.entities

    def set_current_item(self, entityDict):
        assetType = entityDict['sg_asset_type']
        subtype = entityDict['sg_subtype']
        assetName = entityDict['code']
        self.set_type(assetType)
        self.set_subtype(subtype)
        self.assetWidget.set_current_item(assetName)

    def set_type(self, text):
        items = [self.typeWidget.itemText(a) for a in range(self.typeWidget.count())]
        index = items.index(text)
        self.typeWidget.setCurrentIndex(index)

    def set_subtype(self, text):
        items = [self.subtypeWidget.itemText(a) for a in range(self.subtypeWidget.count())]
        index = items.index(text) if text in items else 0
        self.subtypeWidget.setCurrentIndex(index)

    def clear(self):
        self.typeWidget.clear()
        self.subtypeWidget.clear()
        self.assetWidget.clear()

    def set_label_color(self, colors):
        self.typeLabel.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))
        self.subtypeLabel.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))
        self.assetLabel.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

class AssetDisplayInfoUI(QtWidgets.QWidget):
    """docstring for AssetDisplayInfoUI"""
    def __init__(self, parent=None):
        super(AssetDisplayInfoUI, self).__init__(parent=parent)

        self.layout = QtWidgets.QVBoxLayout()

        # widgets
        self.projectLabel = QtWidgets.QLabel('Project')
        self.projectWidget = QtWidgets.QLabel()

        self.typeLabel = QtWidgets.QLabel('Type')
        self.typeWidget = QtWidgets.QLabel()

        self.subtypeLabel = QtWidgets.QLabel('SubType')
        self.subtypeWidget = QtWidgets.QLabel()

        self.assetLabel = QtWidgets.QLabel('Asset')
        self.assetWidget = QtWidgets.QLabel()

        self.stepLabel = QtWidgets.QLabel('Department')
        self.stepWidget = QtWidgets.QLabel()

        self.processLabel = QtWidgets.QLabel('Process')
        self.processWidget = QtWidgets.QLabel()

        self.versionWidget = QtWidgets.QLabel()

        # add layout
        self.layout.addWidget(self.projectLabel)
        self.layout.addWidget(self.projectWidget)

        self.layout.addWidget(self.typeLabel)
        self.layout.addWidget(self.typeWidget)

        self.layout.addWidget(self.subtypeLabel)
        self.layout.addWidget(self.subtypeWidget)

        self.layout.addWidget(self.assetLabel)
        self.layout.addWidget(self.assetWidget)

        self.layout.addWidget(self.stepLabel)
        self.layout.addWidget(self.stepWidget)

        self.layout.addWidget(self.processLabel)
        self.layout.addWidget(self.processWidget)

        self.layout.addWidget(self.versionWidget)

        # set layout
        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.set_bold()

    def set_visible(self, state):
        self.projectWidget.setVisible(state)
        self.typeWidget.setVisible(state)
        self.subtypeWidget.setVisible(state)
        self.assetWidget.setVisible(state)
        self.stepWidget.setVisible(state)
        self.processWidget.setVisible(state)
        self.versionWidget.setVisible(state)

        self.projectLabel.setVisible(state)
        self.typeLabel.setVisible(state)
        self.subtypeLabel.setVisible(state)
        self.assetLabel.setVisible(state)
        self.stepLabel.setVisible(state)
        self.processWidget.setVisible(state)
        self.versionWidget.setVisible(state)

    def set_bold(self):
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)

        self.projectLabel.setFont(font)
        self.typeLabel.setFont(font)
        self.subtypeLabel.setFont(font)
        self.assetLabel.setFont(font)
        self.stepLabel.setFont(font)
        self.processLabel.setFont(font)


class AssetDisplayInfoWidget(AssetDisplayInfoUI):
    """docstring for AssetDisplayInfoWidget"""
    def __init__(self, parent=None):
        super(AssetDisplayInfoWidget, self).__init__(parent=parent)
        self.set_enable(False)

    def set_display(self, ContextPathInfo):
        noData = '-- No Data --'
        asset = ContextPathInfo
        self.set_project(asset.project or noData)
        self.set_type(asset.type or noData)
        self.set_subtype(asset.parent or noData)
        self.set_name(asset.name or noData)
        self.set_step(asset.step or noData)
        self.set_process(asset.process or noData)
        self.set_version(asset.version or noData)


    def set_project(self, project):
        self.projectWidget.setText(project)

    def set_type(self, type):
        self.typeWidget.setText(type)

    def set_subtype(self, subtype):
        self.subtypeWidget.setText(subtype)

    def set_name(self, assetName):
        self.assetWidget.setText(assetName)

    def set_step(self, step):
        self.stepWidget.setText(step)

    def set_process(self, process):
        self.processWidget.setText(process)

    def set_version(self, version):
        self.versionWidget.setText(version)

    def clear(self):
        self.projectWidget.clear()
        self.typeWidget.clear()
        self.subtypeWidget.clear()
        self.assetWidget.clear()
        self.stepWidget.clear()
        self.processWidget.clear()
        self.versionWidget.clear()

    def set_enable(self, state):
        self.projectWidget.setEnabled(state)
        self.typeWidget.setEnabled(state)
        self.subtypeWidget.setEnabled(state)
        self.assetWidget.setEnabled(state)
        self.stepWidget.setEnabled(state)
        self.processWidget.setEnabled(state)
        self.versionWidget.setEnabled(state)



class GetEpisode(QtCore.QThread):
    finished = QtCore.Signal(list)

    def __init__(self, projectEntity):
        QtCore.QThread.__init__(self)
        self.projectEntity = projectEntity

    def run(self):
        from rf_utils.sg import sg_process
        logger.debug('Fetching episode ...')
        episodes = find_episode(sg_process.sg, self.projectEntity)
        self.finished.emit(episodes)


def find_episode(sg, projectEntity):
    return sg.find('Scene', [['project', 'is', projectEntity]], ['code', 'id', 'project'])


def list_file(path, ext=[]):
    result = sorted([d for d in os.listdir(path) if os.path.isfile(os.path.join(path, d))]) if os.path.exists(path) else []
    return [a for a in result if os.path.splitext(a) in ext] if ext else result


def list_folder(path, notStartwith=[]):
    result = sorted([d for d in os.listdir(path) if os.path.isdir(os.path.join(path,d))]) if os.path.exists(path) else []
    return [a for a in result if all(not b in a for b in notStartwith)] if notStartwith else result


def save_selection(episode='', sequence='', shot=''):
    from rf_utils import file_utils
    data = dict()
    if os.path.exists(Cache.cacheFile):
        data = file_utils.ymlLoader(Cache.cacheFile)
    data.update({'episode': episode}) if episode else None
    data.update({'sequence': sequence}) if sequence else None
    data.update({'shot': shot}) if shot else None

    if not os.path.exists(Cache.cacheDir):
        os.makedirs(Cache.cacheDir)

    file_utils.ymlDumper(Cache.cacheFile, data)

def read_last_save(key):
    from rf_utils import file_utils
    if os.path.exists(Cache.cacheFile):
        data = file_utils.ymlLoader(Cache.cacheFile) or dict()
        return data.get(key)
    return ''
