from collections import OrderedDict 
from Qt import QtWidgets
from Qt import QtCore 


class Config: 
    all_filter = 'All' 


class FilterWidget(QtWidgets.QWidget):
    """docstring for FilterWidget"""
    item_selected = QtCore.Signal(object)

    def __init__(self, label='', parent=None):
        super(FilterWidget, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel(label)
        self.filter = QtWidgets.QListWidget()
        self.filter.setSortingEnabled(True)
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.filter)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.filter.itemSelectionChanged.connect(self.filter_selected)
        # self.init_all_item()
        self.all_filter = Config.all_filter

    def init_all_item(self): 
        self.all_item = QtWidgets.QListWidgetItem()
        self.all_item.setText(Config.all_filter)
        self.all_item.setData(QtCore.Qt.UserRole, Config.all_filter)
        return self.all_item

    def set_multiple_selection(self): 
        self.filter.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

    def set_title(self, text): 
        self.label.setText(text)

    def add_filter(self, display, data=None, iconpath=''): 
        data = display if not data else data
        item = QtWidgets.QListWidgetItem()
        item.setText(display)
        item.setData(QtCore.Qt.UserRole, data)
        if iconpath and os.path.exists(iconpath): 
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(iconpath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
            # font = QtGui.QFont("Arial", 8)
            item.setIcon(iconWidget)
        self.filter.addItem(item)

    def filter_selected(self): 
        self.item_selected.emit(self.selected_filters)

    def selected_filters(self): 
        return [a.data(QtCore.Qt.UserRole) for a in self.filter.selectedItems()]
        
    def clear(self): 
        self.filter.clear()

    def all_items(self, *kwargs): 
        return [self.filter.item(a) for a in range(self.filter.count())]

    def set_all_item(self): 
        if self.all_items(): 
            all_item = self.init_all_item()
            self.filter.insertItem(0, all_item)

    def set_all_on_top(self): 
        all_items = self.all_items()
        if all_items: 
            if Config.all_filter in [str(a.text()) for a in all_items]: 
                row = self.filter.row(self.all_item)
                self.filter.takeItem(row)
                self.filter.insertItem(0, self.all_item)

        # self.filter.insertItem(0, Config.all_filter)

    def sort(self): 
        self.filter.sortItems() 
        self.set_all_on_top()
        # ascending by default
        # self.listWidget.sortItems(QtCore.Qt.DescendingOrder)
        # items = [self.filter.item(a) for a in range(self.filter.count())]
        # key = dict()
        # for item in items: 
        #     key[item.text()] = item 

    def select_item(self, keyword): 
        items = self.all_items()
        datas = [a.data(QtCore.Qt.UserRole) for a in items]
        labels = [str(a.text()) for a in items]
        if keyword in labels: 
            index = labels.index(keyword)
            self.filter.setCurrentRow(index)
        elif keyword in datas: 
            index = datas.index(keyword)
            self.filter.setCurrentRow(index)


class FilterWidgetCount(FilterWidget):
    """docstring for FilterWidgetCount"""
    def __init__(self, label='', parent=None):
        super(FilterWidgetCount, self).__init__(label=label, parent=parent)

    def add_filter(self, display, data=None, count=None, iconpath=''): 
        data = display if not data else data
        # attach_data = {'data': data, 'count': count}
        item = QtWidgets.QListWidgetItem()
        display = '{} [{}]'.format(display, count) if count else display
        item.setText(display)
        item.setData(QtCore.Qt.UserRole, data)
        if iconpath and os.path.exists(iconpath): 
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(iconpath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
            # font = QtGui.QFont("Arial", 8)
            item.setIcon(iconWidget)
        self.filter.addItem(item)

    def set_all_item(self, count): 
        if self.all_items(): 
            all_item = self.init_all_item()
            all_item.setText('{} [{}]'.format(all_item.text(), count))
            self.filter.insertItem(0, all_item)

    # def set_all_item(self): 
    #     count = 0
    #     all_items = [a.data(QtCore.Qt.UserRole) for a in self.all_items()]
    #     all_datas = [a['data'] for a in all_items]

    #     for item in all_items: 
    #         count += item.get('count')
            
    #     if not Config.all_filter in all_datas: 
    #         all_item = QtWidgets.QListWidgetItem()
    #         all_item.setText('{} [{}]'.format(Config.all_filter, count))
    #         all_item.setData(QtCore.Qt.UserRole, {'data': Config.all_filter})
    #         self.filter.insertItem(0, all_item)
    #         return all_item
    # def selected_filters(self): 

