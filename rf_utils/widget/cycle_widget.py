import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import icon

class Config: 
    statusMap = {'wtg': 'Waiting to Start', 
                'rdy': 'Ready to Start', 
                'rev': 'Pending Review', 
                'ip': 'In Progress', 'apr': 'Approved', 
                'hld': 'Hold', 
                'p_aprv': 'Pending Approve', 
                'apr': 'Approved', 
                'omt': 'Omit'}

class Color: 
    green = [100, 200, 100]
    red = [200, 100, 100]
    grey = [100, 100, 100]
    grey2 = [140, 140, 140]

class WidgetIcon: 
    animData = '%s/icons/output/animData_icon.png' % module_dir
    mcdAction = '%s/icons/output/mcdAction_icon.png' % module_dir
    alembicCache = '%s/icons/output/alembic_icon.png' % module_dir


class CycleListWidget(QtWidgets.QWidget) :
    selected = QtCore.Signal(dict)
    multiSelected = QtCore.Signal(list)
    def __init__(self, parent=None) :
        super(CycleListWidget, self).__init__(parent)
        self.iconNoPreview = icon.nopreview
        self.ui()
        self.init_signals()

    def ui(self): 
        self.allLayout = QtWidgets.QHBoxLayout()
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.allLayout.addWidget(self.listWidget)
        self.setLayout(self.allLayout)

    def init_signals(self): 
        self.listWidget.clicked.connect(self.item_selected)

    def item_selected(self):
        items = self.listWidget.selectedItems()
        if len(items) == 1:
            item = items[0]
            data = item.data(QtCore.Qt.UserRole)
            self.selected.emit(data)
        elif len(items) > 1: 
            self.multiSelected.emit([a.data(QtCore.Qt.UserRole) for a in items])

    def set_text(self, text): 
        self.listWidget.addItem(text)

    def current_item(self): 
        if self.listWidget.currentItem(): 
            return self.listWidget.currentItem().data(QtCore.Qt.UserRole)

    def selected_items(self): 
        return [a.data(QtCore.Qt.UserRole) for a in self.listWidget.selectedItems()]

    def clear(self): 
        self.listWidget.clear()

    def all_items(self): 
        count = self.listWidget.count()
        return [self.listWidget.item(a).data(QtCore.Qt.UserRole) for a in range(count)]

    def add_items(self, sgEntities, widget=False):
        for i, entity in enumerate(sgEntities):
            self.add_item(entity, widget=widget)

    def add_texts(self, sgEntities, widget=False):
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(icon.dir),QtGui.QIcon.Normal,QtGui.QIcon.Off)
        # font = QtGui.QFont("Arial", 8)

        for i, sgEntity in enumerate(sgEntities):
            item = QtWidgets.QListWidgetItem(self.listWidget)
            item.setData(QtCore.Qt.UserRole, sgEntity)
            item.setText(sgEntity['code'])
            item.setIcon(iconWidget)
            # item.setFont(font)

    def set_current_item(self, taskName): 
        taskNames = [a.get('content') for a in self.all_items()]
        if taskName in taskNames: 
            index = taskNames.index(taskName)
            self.listWidget.setCurrentRow(index)
        else:  # EDITED BY NU: if taskName not in taskNames, just select the first row
            self.listWidget.setCurrentRow(0)

    def add_item(self, taskEntity, iconPath='', showIcon=True, widget=False):
        # normal widget mode
        item = CustomListWidgetItem(self.listWidget)
        item.setData(QtCore.Qt.UserRole, taskEntity)
        iconPath = self.iconNoPreview if not iconPath else iconPath

        # extract data 
        assignees = taskEntity.get('task_assignees')
        rigStatus = taskEntity.get('rig').get('status') # custom data 
        animDataStatus = taskEntity.get('animData').get('status')
        actionStatus = taskEntity.get('mcdAction').get('status')
        cacheStatus = taskEntity.get('cache').get('status')
        # ---- TEMPORARY COMMENTED OUT BY NU
        # previewPath = taskEntity.get('preview').get('file')

        displayAssignes = (',').join([a.get('name') for a in assignees]) if assignees else '-'

        status = taskEntity.get('sg_status_list')
        statusDisplay = Config.statusMap.get(status) if status in Config.statusMap.keys() else status

        itemWidget = EntityItem()
        itemWidget.set_name(taskEntity.get('content'))
        itemWidget.set_text1('Range 1001 - 1100')
        itemWidget.set_text2('Assigned to : %s' % displayAssignes)
        itemWidget.set_text3('Status : %s' % statusDisplay)
        itemWidget.set_text4('Rig : %s' % rigStatus)

        itemWidget.set_text5('%s' % taskEntity.get('animData').get('display'))
        itemWidget.set_text6('%s' % taskEntity.get('mcdAction').get('display'))
        itemWidget.set_text7('%s' % taskEntity.get('cache').get('display'))
        # itemWidget.set_text8('Rig status: %s' % rigStatus)

        itemWidget.set_icon5(WidgetIcon.animData)
        itemWidget.set_icon6(WidgetIcon.mcdAction)
        itemWidget.set_icon7(WidgetIcon.alembicCache)


        itemWidget.set_icon(iconPath, size=[128, 128])

        # color 
        itemWidget.set_text1_color(Color.grey)
        itemWidget.set_text3_color(Color.grey2)

        rigColor = Color.green if rigStatus else Color.red
        itemWidget.set_text4_color(rigColor)

        animDataColor = Color.green if animDataStatus else Color.red
        itemWidget.set_text5_color(animDataColor)

        actionColor = Color.green if actionStatus else Color.red
        itemWidget.set_text6_color(actionColor)

        cacheColor = Color.green if cacheStatus else Color.red
        itemWidget.set_text7_color(cacheColor)

        item.setSizeHint(itemWidget.sizeHint())
        self.listWidget.setItemWidget(item, itemWidget)

    def set_list_mode(self):
        self.listWidget.setViewMode(QtWidgets.QListView.ListMode)
        self.listWidget.setMovement(QtWidgets.QListView.Static)
        self.refresh_list(True)

    def set_icon_mode(self):
        self.listWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.listWidget.setMovement(QtWidgets.QListView.Static)
        self.refresh_list(False)

    def set_text_mode(self): 
        self.listWidget.setViewMode(QtWidgets.QListView.ListMode)
        self.listWidget.setMovement(QtWidgets.QListView.Static)
        self.refresh_list(True, 'text')

    def refresh_list(self, widget, mode=''):
        sgEntities = [self.listWidget.item(a).data(QtCore.Qt.UserRole) for a in range(self.listWidget.count())]
        self.listWidget.clear()
        if mode == 'text': 
            self.add_texts(sgEntities)
        else: 
            self.add_items(sgEntities, widget=widget)


class CustomListWidget(QtWidgets.QListWidget):
    """docstring for CustomListWidget"""
    def __init__(self, *args, **kwargs):
        super(CustomListWidget, self).__init__(*args, **kwargs)
        
    # def mouseEnterEvent(self, event): 
    #     super(CustomListWidget, self).enterEvent(event)
    #     print 'Enter'

class CustomListWidgetItem(QtWidgets.QListWidgetItem):
    """docstring for CustomListWidget"""
    def __init__(self, *args, **kwargs):
        super(CustomListWidgetItem, self).__init__(*args, **kwargs)

    def mouseEnterEvent(self, event): 
        print 'enter'

class EntityItem(QtWidgets.QWidget):
    """docstring for EntityItem"""
    def __init__(self, view='icon', parent=None):
        super(EntityItem, self).__init__(parent=parent)

        # widgets
        self.icon = QtWidgets.QLabel()
        self.dir = QtWidgets.QLabel()
        self.name = QtWidgets.QLabel()
        self.text1 = QtWidgets.QLabel()
        self.text2 = QtWidgets.QLabel()
        self.text3 = QtWidgets.QLabel()
        self.text4 = QtWidgets.QLabel()

        # info
        self.text5 = QtWidgets.QLabel()
        self.text6 = QtWidgets.QLabel()
        self.text7 = QtWidgets.QLabel()
        self.text8 = QtWidgets.QLabel()

        # info
        self.icon5 = QtWidgets.QLabel()
        self.icon6 = QtWidgets.QLabel()
        self.icon7 = QtWidgets.QLabel()
        self.icon8 = QtWidgets.QLabel()

        # icon view
        self.icon_view_layout() if view == 'icon' else self.list_view_layout() if view == 'list' else 'icon'

    def icon_view_layout(self):
        self.layout = QtWidgets.QHBoxLayout()
        self.subLayout = QtWidgets.QGridLayout()
        self.layout.addWidget(self.icon)
        self.infoLayout = QtWidgets.QHBoxLayout()
        self.infoLayout.addWidget(self.dir)
        self.infoLayout.addWidget(self.name)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.infoLayout.addItem(spacerItem)

        self.infoLayout.setStretch(0, 0)
        self.infoLayout.setStretch(1, 0)
        self.infoLayout.setStretch(2, 1)
        self.infoLayout.setSpacing(0)
        self.layout.setContentsMargins(2, 2, 2, 2)

        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.subLayout.addLayout(self.infoLayout, 0, 1)
        self.subLayout.addWidget(self.text1, 1, 1)
        self.subLayout.addWidget(self.text2, 2, 1)
        self.subLayout.addWidget(self.text3, 3, 1)
        self.subLayout.addWidget(self.text4, 4, 1)

        self.subLayout.addWidget(self.icon5, 1, 2)
        self.subLayout.addWidget(self.icon6, 2, 2)
        self.subLayout.addWidget(self.icon7, 3, 2)
        self.subLayout.addWidget(self.icon8, 4, 2)
        
        self.subLayout.addWidget(self.text5, 1, 3)
        self.subLayout.addWidget(self.text6, 2, 3)
        self.subLayout.addWidget(self.text7, 3, 3)
        self.subLayout.addWidget(self.text8, 4, 3)

        # self.subLayout.addItem(spacerItem2)
        self.subLayout.setSpacing(2)
        self.subLayout.setContentsMargins(2, 2, 2, 2)

        self.layout.addLayout(self.subLayout)
        self.setLayout(self.layout)

        self.name.setFont(QtGui.QFont("Arial", 10, QtGui.QFont.Bold))
        self.text1.setFont(QtGui.QFont("Arial", 9))
        self.text2.setFont(QtGui.QFont("Arial", 9))
        # self.textFrameRange.setFont(QtGui.QFont("Arial", 8, QtGui.QFont.Bold))
        # self.textstatus.setFont(QtGui.QFont("Arial", 8, QtGui.QFont.Bold))

    def set_icon(self, path, size=[100, 100]):
        self.icon.setPixmap(QtGui.QPixmap(path).scaled(size[0], size[1], QtCore.Qt.KeepAspectRatio))

    def set_dir(self, state):
        iconPath = icon.dir if state else icon.nodir
        # self.dir.setPixmap(QtGui.QPixmap(iconPath).scaled(16, 16, QtCore.Qt.KeepAspectRatio))

    def set_name(self, text):
        self.name.setText(text)

    def set_text1(self, text):
        self.text1.setText(text)

    def set_text2(self, text):
        self.text2.setText(text)

    def set_text3(self, text):
        self.text3.setText(text)

    def set_text4(self, text):
        self.text4.setText(text)

    def set_text5(self, text):
        self.text5.setText(text)

    def set_text6(self, text):
        self.text6.setText(text)

    def set_text7(self, text):
        self.text7.setText(text)

    def set_text8(self, text):
        self.text8.setText(text)

    def set_text1_color(self, colors): 
        self.text1.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text2_color(self, colors): 
        self.text2.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text3_color(self, colors): 
        self.text3.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text4_color(self, colors): 
        self.text4.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text5_color(self, colors): 
        self.text5.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text6_color(self, colors): 
        self.text6.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text7_color(self, colors): 
        self.text7.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text8_color(self, colors): 
        self.text8.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_icon5(self, path, size=[16, 16]): 
        self.icon5.setPixmap(QtGui.QPixmap(path).scaled(size[0], size[1], QtCore.Qt.KeepAspectRatio))

    def set_icon6(self, path, size=[16, 16]): 
        self.icon6.setPixmap(QtGui.QPixmap(path).scaled(size[0], size[1], QtCore.Qt.KeepAspectRatio))

    def set_icon7(self, path, size=[16, 16]): 
        self.icon7.setPixmap(QtGui.QPixmap(path).scaled(size[0], size[1], QtCore.Qt.KeepAspectRatio))

    def set_icon8(self, path, size=[16, 16]): 
        self.icon8.setPixmap(QtGui.QPixmap(path).scaled(size[0], size[1], QtCore.Qt.KeepAspectRatio))
