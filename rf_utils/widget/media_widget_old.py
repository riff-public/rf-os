# coding=utf-8
import os
import re
import subprocess
import sys
import time
from collections import OrderedDict

#from tqdm import tqdm
import gc

# Fix ImageMagick ERROR --> RegistryKeyLookupFailed `CoderModulesPath' @ error/module.c/GetMagickModulePath/663
# NOTE: This override must set before calling PySide, PyQt, Because PySide will set its own environment and disturb ImageMagick environment.
# magick_home = 'M:/SCRIPTS/PACKAGES/ImageMagick-6.9.10-Q16'
magick_home = '%s/core/rf_lib/ImageMagick-7.0.10-Q16-HDRI' % os.environ['RFSCRIPT']
os.environ['PATH'] = os.environ['PATH'] + ';O:/studioTools/lib/gs/gs9.25/bin;' + magick_home
os.environ['MAGICK_HOME'] = magick_home
os.environ["MAGICK_CODER_MODULE_PATH"] = magick_home + "/modules/coders"

# from wand.image import Image as WandImage
# from wand.image import Color
import numpy as np
import cv2
# from psd_tools import PSDImage
# import OpenEXR
# import Imath
from PIL import Image
from Qt import QtWidgets
from Qt import QtCore
from Qt import QtGui

VIDEO_EXT = ['.mov', '.mp4']
IMAGE_EXT = ['.png', '.jpeg', '.jpg', '.tiff', '.TIF', '.tif']
PSD_EXT = '.psd'
PDF_EXT = '.pdf'
EXR_EXT = '.exr'
IMAGELAYER_EXT = ['.exr', '.psd']
DOCUMENT_EXT = ['.pdf']

PLAYABLE_MEDIA = VIDEO_EXT + [EXR_EXT]

_VERSION_ = '2.5'

MODULEDIR = os.path.dirname(sys.modules[__name__].__file__)
LOADINGICON = "%s/icons/gif/loading60.gif" % MODULEDIR

# V2.2 : add playbutton for mov
# V2.3 : Support PDF
# V2.4 : Support EXR with frame range.
# V2.5 : Fix Threading error

def convert_to_list(input, path=True):
    """

    Args:
        input (str,list):
        path (bool):

    Returns:

    """

    # Check sequence sign
    # detect and expand "filename.[100-200].ext" to list of filepath
    pattern = ("(\[[0-9]{1,}-[0-9]{1,}\])")

    if type(input) == type(str()):
        input = [input]

    if not path:
        return input
    else:
        result = []
        for input_i in input:
            if not input_i:
                continue
            r = re.findall(pattern, input_i)
            if r:
                framenum = r[0][1:-1].split('-')
                start = int(framenum[0])
                end = int(framenum[1])
                result += [input_i.replace(r[0], str("%04d" % i)) for i in range(start, end + 1)]
            else:
                result += [input_i]

    return result


class ClickableLabel(QtWidgets.QLabel):
    playStopMedia = QtCore.Signal()

    def __init__(self, parent=None):
        super(ClickableLabel, self).__init__(parent)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

    def mouseDoubleClickEvent(self, *args, **kwargs):
        if os.path.splitext(self.currentMedia.path)[-1] in VIDEO_EXT + [EXR_EXT]:
            subprocess.Popen(["M:/SCRIPTS/PACKAGES/RV-7.2.2/bin/rv.exe", self.currentMedia.path], shell=True)
        elif os.path.splitext(self.currentMedia.path)[-1] in PDF_EXT:
            print self.currentMedia.path
            subprocess.Popen('start "" /max %s' % self.currentMedia.path, shell=True)

    def mousePressEvent(self, event):
        if event.buttons() == QtCore.Qt.RightButton:
            pass
        elif event.buttons() == QtCore.Qt.LeftButton:
            self.playStopMedia.emit()

    @property
    def currentMedia(self):
        return self.parentWidget().currentMedia

    @property
    def videoFile(self):
        return self.parentWidget().videoFile


class MediaModel(object):

    def __init__(self,
                 name='Null',
                 path='',
                 type='Null',
                 images=[],
                 layers={},
                 width=0,
                 height=0):
        super(MediaModel, self).__init__()

        self.name = name
        self.path = path
        self.type = type
        self.width = width
        self.height = height

        self.ext = os.path.splitext(path)[-1] if path else ''

        images = convert_to_list(images, path=False)
        self.images = images

        self.layers = layers

        self.lenght = len(images)

    def loadlayers(self, layername):
        if not len(self.layers[layername]):
            if self.ext == PSD_EXT:
                self.layers[layername] = get_PSDLayerData(image_file=self.path, layername=layername)

        return self.layers[layername] if len(self.layers[layername]) else self.images[0]

    def __getitem__(self, item):
        return self.images[item]

    def __len__(self):
        return self.lenght

    def __eq__(self, other):
        if hasattr(other, 'path'):
            if other.path == self.path:
                return True
        return False


class MediaPlayThread(QtCore.QThread):
    CONTINUE = QtCore.Signal()
    LOOP = QtCore.Signal()

    def __init__(self, lenght, startframe=0, fps=24):
        super(MediaPlayThread, self).__init__()
        self.fps = float(fps)
        self.startframe = int(startframe)
        self.lenght = int(lenght)

    def __del__(self):
        self.wait()

    def restart(self):
        self.__restart = True

    def run(self):

        while self.startframe < self.lenght:
            time.sleep(1 / self.fps)
            self.CONTINUE.emit()

            self.startframe += 1
            if self.startframe >= self.lenght:
                self.startframe = 0
                self.LOOP.emit()

        self.exit()


class MediaThread(QtCore.QThread):
    loadFinished = QtCore.Signal(list)
    frameLoaded = QtCore.Signal(list)

    __stop = False

    def __init__(self, filenames=[]):
        super(MediaThread, self).__init__()
        self.filenames = filenames
        self.images = []

    def __del__(self):
        self.wait()

    def stop(self):
        self.__stop = True

    def run(self):

        if not self.filenames:
            return 0

        for each in self.filenames:

            each_name, each_ext = os.path.splitext(each)
            # each_name = os.path.basename(each_name)
            basename = os.path.basename(each)
            # print("filename : %s"%each_name)

            if not os.path.exists(each):
                print ("Missing file : %s" % each)
                continue

            if each_ext in VIDEO_EXT:
                # print("Load Video")
                data = load_videos(each)
                image = MediaModel(name=basename,
                                   path=each,
                                   type='video',
                                   images=data,
                                   width=data[0].shape[1],
                                   height=data[0].shape[0])
                self.images.append(image)

            elif each_ext in IMAGE_EXT:
                # print("Load Image")
                data = load_images(each)
                image = MediaModel(name=basename,
                                   path=each,
                                   type='image',
                                   images=[data],
                                   width=data.shape[1],
                                   height=data.shape[0])

                self.images.append(image)

            elif each_ext == PSD_EXT:
                # print("Load PSD")
                data, layers = load_PSD(each)
                image = MediaModel(name=basename,
                                   path=each,
                                   type='psd',
                                   images=[data],
                                   layers=layers,
                                   width=data.shape[1],
                                   height=data.shape[0])

                self.images.append(image)

            elif each_ext == EXR_EXT:
                # print("Load EXR")
                data, channels = load_EXR(each)
                image = MediaModel(name=basename,
                                   path=each,
                                   type='exr',
                                   images=[data],
                                   layers=channels,
                                   width=data.shape[1],
                                   height=data.shape[0])
                self.images.append(image)

            elif each_ext == PDF_EXT:
                # print("Load PDF")
                data = load_PDF(each)
                image = MediaModel(name=basename,
                                   path=each,
                                   type='pdf',
                                   images=data,
                                   width=data[0].shape[0],
                                   height=data[0].shape[0])

                self.images.append(image)

            else:
                print("Load Error : %s" % each_ext)
                return 0

            self.frameLoaded.emit([image])

            if self.__stop:
                self.__stop = False
                print ("Threading interrupted")
                self.loadFinished.emit(self.images)
            # self.exit()

        else:
            self.loadFinished.emit(self.images)


class PreviewMedia(QtWidgets.QWidget):
    _currentMedia = []
    currentMediaStart = 0
    currentMediaFrame = -1
    currentMediaLenght = 0
    currentFrameData = None
    globalFrameRange = 0
    layer = 'overall'
    imap = QtGui.QImage()
    images = []
    displayImages = []
    loadMediaFinished = True
    currentGlobalFrame = -1

    _playThread = None
    playing = False

    def __init__(self, parent=None):
        super(PreviewMedia, self).__init__(parent)

        self.pos = self.pos()
        self.previousLayer = self.layer
        self.ipixmap = QtGui.QPixmap()
        self.currentMedia = MediaModel()
        self.mediaThread = None
        self.movie = None

        mainLayout = QtWidgets.QVBoxLayout()
        mediaLayout = QtWidgets.QHBoxLayout()
        self.layerListWidget = QtWidgets.QListWidget(self)
        self.mediaplayer = ClickableLabel(self)
        self.informationText = QtWidgets.QLabel(self)
        controller_layout = QtWidgets.QHBoxLayout()
        self.controller_play = QtWidgets.QLabel(self)
        self.information2Text = QtWidgets.QLabel(self)

        mediaLayout.addWidget(self.mediaplayer)
        mediaLayout.addWidget(self.layerListWidget)
        mainLayout.addLayout(mediaLayout)
        mainLayout.addWidget(self.informationText)
        mainLayout.addLayout(controller_layout)
        controller_layout.addWidget(self.controller_play)
        mainLayout.addWidget(self.information2Text)

        self.setLayout(mainLayout)

        self.mediaplayer.setMouseTracking(True)
        self.setMouseTracking(True)
        self.layerListWidget.setMouseTracking(True)

        # Decoration
        mediaLayout.setStretch(0, 1)
        self.mediaplayer.setAlignment(QtCore.Qt.AlignCenter)
        self.mediaplayer.setAlignment(QtCore.Qt.AlignHCenter)
        self.layerListWidget.setVisible(False)
        self.informationText.setVisible(False)
        mainLayout.setStretch(0, 1)

        self.informationText.setText("Frame %d/%d" % (self.currentMediaFrame, len(self.images)))
        self.informationText.setAlignment(QtCore.Qt.AlignCenter)
        self.informationText.setAlignment(QtCore.Qt.AlignHCenter)

        controller_layout.setAlignment(QtCore.Qt.AlignCenter)
        controller_layout.setAlignment(QtCore.Qt.AlignHCenter)
        # self.controller_play.setFlat(True)

        self.information2Text.setText("Channel : RGBA | Resolution : %dX%d" % (0, 0))
        self.information2Text.setAlignment(QtCore.Qt.AlignCenter)
        self.information2Text.setAlignment(QtCore.Qt.AlignHCenter)

        self.controller_play.setVisible(True)
        # self.controller_play.setEnabled(False)
        # self.controller_play.setText("Loading media, please wait...")
        self.controller_play.setText("Click the image to play video")
        self.controller_play.setToolTip("Click the image to play video")

        self.layerListWidget.clicked.connect(self.layerDropdown_onChanged)
        self.mediaplayer.playStopMedia.connect(self.play_onCLicked)
        # self.connect(self.mediaplayer, SIGNAL('customContextMenuRequested(const QPoint &)'), self.context_menu)
        self.mediaplayer.customContextMenuRequested.connect(self.context_menu)

    def on_destroyed(self, *args, **kwargs):
        super(PreviewMedia, self).on_destroyed(*args, **kwargs)

    def loadMedia(self, mediapaths):

        # Not load any media when this widget is closed.
        if not self.isVisible():
            return False

        # If path not List
        mediapaths = convert_to_list(mediapaths)

        # Check if media already loaded.
        if sorted(self._currentMedia) != sorted(mediapaths):
            self.layerListWidget.setVisible(False)
            self._loadMedia(mediapaths)
        else:
            return 0

    def _loadMedia(self, filenames):

        if self.mediaThread != None:

            if self.mediaThread.isRunning():
                if os.path.splitext(self.mediaThread.filenames[0])[-1] in VIDEO_EXT:
                    return 0

                self.mediaThread.stop()
                self.mediaThread.terminate()
                self.mediaThread.exit()

        self.loadMediaFinished = False

        self._currentMedia = filenames

        if self.playing == True:
            self.stop_playMedia()

        self.controller_play.setText("Loading media, please wait...")
        self.setLoading(True)

        filenames = convert_to_list(filenames)

        # Reset list every time when load new media.
        self.images = []
        self.layer = 'overall'
        self.startAnim = False
        self.globalFrameRange = 0

        self.mediaThread = MediaThread(filenames)
        self.mediaThread.start()

        self.mediaThread.frameLoaded.connect(self.mediaThread_onLoadFrameFinished)
        self.mediaThread.loadFinished.connect(self.mediaThread_onLoadMediaFinished)

        self.controller_play.setVisible(True)

    def mediaThread_onLoadFrameFinished(self, medialist):
        self.images += medialist
        for each in self.images:
            # print ("Add : " + str(len(each.images)))
            self.globalFrameRange += len(each.images)

        if len(self.images) == 1:
            self.loadFrame(0)
            self.updateInfo()

    # print ("Frame %d loaded."%len(self.images))

    def mediaThread_onLoadMediaFinished(self, medialist):
        self.mediaThread = None
        self.images = medialist
        self.globalFrameRange = 0
        for each in self.images:
            self.globalFrameRange += len(each.images)

        if self.currentMedia.ext in PLAYABLE_MEDIA:
            self.controller_play.setVisible(True)
        else:
            self.controller_play.setVisible(False)

        self.controller_play.setText("Click the image to play video")
        self.loadMediaFinished = True

    def setLoading(self, start): 
        self.mediaplayer.setText('Loading ...')
        if not self.movie: 
            self.movie = QtGui.QMovie(LOADINGICON)
        self.mediaplayer.setMovie(self.movie)
        self.movie.start()


    def loadFrame(self, percent=None, nextframe=False, firstframe=False):
        """

        Args:
            percent (float): value between 0.0 - 1.0

        Returns (None): 0 if Fail, Otherwise return None

        """

        if percent == None and not nextframe and not firstframe:
            return 0

        # Calculate frame
        perframe = 1.0 / self.globalFrameRange if self.globalFrameRange else 0
        current_percent = self.currentGlobalFrame * perframe
        if nextframe:
            # Generate next frame's percent
            percent = current_percent + perframe

        elif firstframe:
            # Generate next frame's percent
            percent = 0

        # == Which media will play ==
        selectedMedia = int((percent * len(self.images)))
        self.currentGlobalFrame = round(self.globalFrameRange * percent)  # From whole frame
        # == Which frame of media will play ==
        try:
            self.currentMediaLenght = float(self.images[selectedMedia].lenght)
        except IndexError:
            return 0

        # Media area from the whole lenght, represent in float (0.0-1.0)
        currentMediaArea = float(1.0 / len(self.images))
        self.currentMediaStart = (currentMediaArea * selectedMedia) * self.globalFrameRange
        currentMediaLenght2 = currentMediaArea * self.globalFrameRange
        mediaCurrentFrame = int(
            (int(self.currentGlobalFrame - self.currentMediaStart) / currentMediaLenght2) * self.currentMediaLenght)

        currentMedia = self.images[selectedMedia]
        if currentMedia == self.currentMedia and mediaCurrentFrame == self.currentMediaFrame and self.previousLayer == self.layer:
            return 0
        else:
            self.currentMediaFrame = mediaCurrentFrame
            self.currentMedia = currentMedia

        if self.currentMediaFrame >= self.currentMediaLenght:
            self.currentMediaFrame = 0

        try:
            # Set currentMedia
            if self.layer != 'overall':
                if self.previousLayer == self.layer:
                    return 0
                self.currentFrameData = self.currentMedia.loadlayers(layername=self.layer)
                self.previousLayer = self.layer
            else:

                self.currentFrameData = self.currentMedia[self.currentMediaFrame]
                self.previousLayer = self.layer

            height, width, channel = self.currentFrameData.shape

            # Load media from path instead, if media is normal image
            if self.currentMedia.ext in IMAGE_EXT:
                self.imap = self.currentMedia.path
            else:
                self.imap = QtGui.QImage(self.currentFrameData.data, width, height, width * 3, QtGui.QImage.Format_RGB888)

            self.ipixmap = QtGui.QPixmap(self.imap).scaled(self.mediaplayer.size(), QtCore.Qt.KeepAspectRatio)
            self.mediaplayer.setPixmap(self.ipixmap)
            self.updateInfo()
        except IndexError as e:
            print("Could not load this frame : %d" % self.currentMediaFrame)

    def updateInfo(self):
        if self.currentMedia.lenght >= 2:
            self.informationText.setText("Frame %d/%d" % (self.currentMediaFrame + 1, self.currentMedia.lenght))
            self.informationText.setVisible(True)
        else:
            self.informationText.setVisible(False)
        self.information2Text.setText(
            "Channel : RGBA | Resolution : %dX%d" % (self.currentMedia.width, self.currentMedia.height))

    def layerDropdown_onChanged(self):
        layername = self.layerListWidget.currentItem().text()
        self.showLayer(layername)
        self.loadFrame(0)

    def play_onCLicked(self):
        # self.loadFrame(nextframe=True)

        if self.playing == False and self.currentMedia.ext in PLAYABLE_MEDIA:
            self.start_playMedia()

        else:
            self.stop_playMedia()

    def start_playMedia(self):
        self.playing = True
        self._playThread = MediaPlayThread(startframe=0, fps=24, lenght=self.globalFrameRange)
        self._playThread.CONTINUE.connect(lambda: self.loadFrame(nextframe=True))
        self._playThread.LOOP.connect(lambda: self.loadFrame(firstframe=True))
        self._playThread.start()

    def stop_playMedia(self):
        if hasattr(self._playThread, 'finished'):
            self._playThread.terminate()
            self._playThread = None
            self.playing = False

    def resizeEvent(self, event):
        self.layerListWidget.setMaximumWidth(0.2 * self.width())
        if self.ipixmap.isNull():
            return 0

        try:
            self.ipixmap = QPixmap(self.imap).scaled(self.mediaplayer.size(), Qt.KeepAspectRatio)
        except Exception as e:
            pass

        self.mediaplayer.setPixmap(self.ipixmap)

        self.setMinimumSize(QtCore.QSize(100, 100))
        super(PreviewMedia, self).resizeEvent(event)

    def context_menu(self):
        if self.currentMedia.ext == PSD_EXT:
            menu = QMenu(self)
            showLayer_action = menu.addAction("Show layers")

            showLayer_action.triggered.connect(self.showLayerListWidget)

            menu.exec_(QCursor.pos())

    def mouseMoveEvent(self, *args, **kwargs):
        if self.startAnim:
            # Mouse pos[x]
            mouse_x = self.cursor().pos().x()

            start_x = self.mapToGlobal(self.rect().topLeft()).x()
            end_x = self.width()

            playPercent = ((mouse_x - start_x) / float(end_x))

            self.loadFrame(playPercent)
        # self.updateInfo()

    def showLayerListWidget(self):
        # Show LayerListWidget
        self.layerListWidget.clear()
        # if self.currentMedia.ext == PSD_EXT :
        # 	self.currentMedia.loadLayers()

        if self.currentMedia.layers:
            self.layerListWidget.setVisible(True)
            self.layerListWidget.addItems(self.currentMedia.layers.keys())
        else:
            self.layerListWidget.setVisible(False)

    def enterEvent(self, *args, **kwargs):
        self.startAnim = True

    # self.loadFrame(0)

    def leaveEvent(self, *args, **kwargs):
        self.startAnim = False

    # self.loadFrame(0)

    def __newimagesize(self, w, h):
        new_w = self.width() - (self.width() * 0.2)
        new_h = (h * new_w) / w

        if h > self.mediaplayer.height() * 0.3:
            new_h = self.mediaplayer.height() - (self.mediaplayer.height() * 0.3)
            new_w = (w * new_h) / h

        return ((new_w, new_h))

    def showLayer(self, layername):
        self.layer = layername


def load_videos(video_file):
    capture = cv2.VideoCapture(video_file)

    read_flag, frame = capture.read()
    vid_frames = []
    i = 1

    while (read_flag):
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        vid_frames.append(frame)
        read_flag, frame = capture.read()
        i += 1
    vid_frames = np.asarray(vid_frames, dtype='uint8')[:-1]

    capture.release()
    gc.collect()
    return vid_frames


def load_images(image_file):
    capture = cv2.imread(image_file, cv2.IMREAD_UNCHANGED)
    height, width, channels = capture.shape

    # if channels > 3 :
    # 	# forground  = cv2.cvtColor(capture.copy(), cv2.COLOR_BGR2RGB)
    # 	# background = generateGrid( height, width, 30)
    # 	frame = cv2.cvtColor(capture, cv2.COLOR_BGRA2RGBA)
    # 	# frame = np.roll(capture, 1, axis=-1)
    # 	# frame= frame.astype(np.uint16)
    # else :
    frame = cv2.cvtColor(capture, cv2.COLOR_BGR2RGB)

    return frame


def get_PSDLayerData(image_file, layername):
    #return
    psd = PSDImage.load(image_file)

    result = []

    for layer in psd.layers[:]:
        if layer.name == layername:
            try:
                alpha = None
                layer_img = layer.as_PIL().convert('RGB')
                if len(layer.as_PIL().split()) > 3:
                    alpha = layer.as_PIL().split()[3]
                # canvas = Image.new('RGB', psd.as_PIL().size, (255,255,255,255))
                canvas = Image.fromarray(
                    generateGrid(w=psd.as_PIL().size[1], h=psd.as_PIL().size[0], sq=psd.as_PIL().size[1] / 50))
                canvas.paste(layer_img, box=layer.bbox, mask=alpha)
                result = np.asarray(canvas, dtype='uint8')
            except ValueError as e:
                return []
            gc.collect()
            return result


def load_PSD(image_file):
    #return
    psd = PSDImage.load(image_file)
    image = np.asarray(psd.as_PIL().convert('RGB'), dtype='uint8')

    layers = OrderedDict({})
    layers['overall'] = image
    for layer in psd.layers[:]:
        if layer.bbox.y2 == 0 and layer.bbox.x2 == 0:
            continue
        layers[layer.name] = []
    gc.collect()
    return (image, layers)


def load_EXR(image_file):
    def adjust_gamma(image, gamma=1.0):
        # build a lookup table mapping the pixel values [0, 255] to
        # their adjusted gamma values
        invGamma = 1.0 / gamma
        table = np.array([((i / 255.0) ** invGamma) * 255
                          for i in np.arange(0, 256)]).astype(np.uint8)

        # apply gamma correction using the lookup table
        return cv2.LUT(image, table)

    channels = {}

    pt = Imath.PixelType(Imath.PixelType.FLOAT)
    exr = OpenEXR.InputFile(image_file)
    dw = exr.header()['dataWindow']
    size = ((dw.max.x - dw.min.x + 1), (dw.max.y - dw.min.y + 1))

    # RED
    redstr = exr.channel('R', pt)
    red = np.fromstring(redstr, dtype=np.float32)
    red.shape = (size[1], size[0])  # Numpy arrays are (row, col)

    # Green
    greenstr = exr.channel('G', pt)
    green = np.fromstring(greenstr, dtype=np.float32)
    green.shape = (size[1], size[0])  # Numpy arrays are (row, col)

    # RED
    bluestr = exr.channel('B', pt)
    blue = np.fromstring(bluestr, dtype=np.float32)
    blue.shape = (size[1], size[0])  # Numpy arrays are (row, col)

    # Split channel
    '''
    exr_channels = {}
    for channel in exr.header()['channels'].keys():
        if channel in ('R', 'G', 'B', 'A'):
            continue

        channel_name = channel.split('.')[0]

        if exr_channels.has_key(channel_name):
            exr_channels[channel_name].append(channel)
        else:
            exr_channels[channel_name] = [channel]

    for index_i,channel in enumerate( exr_channels.keys()[:3] ):
        print ("Loading AOV %d/%d : %s"%(index_i,len(exr_channels.keys()),channel))
        layer_data = []
        for layer in exr_channels[channel]:
            layerstr = exr.channel(layer, pt)
            layer_np = np.fromstring(layerstr, dtype=np.float32)
            layer_np.shape = (size[1], size[0])  # Numpy arrays are (row, col)
            layer_data.append(layer_np.copy())

        channel_data = adjust_gamma((np.dstack(layer_data) * 255.999).astype(np.uint8).copy(), 2.2)
        channels[channel] = channel_data
    '''

    data = np.dstack((red, green, blue)) * 255.0  # Use 255.9 instead of 256 because of hot pixel in image
    data = data.astype(np.uint8)
    data = adjust_gamma(data, 2.2)

    return (data, channels)


# return paths

def load_PDF(image_file):
    import tempfile, glob, traceback

    # used to generate temp file name. so we will not duplicate or replace anything
    tempdir = tempfile.mkdtemp()
    temp_imagePath = tempdir + '/tmp_pdf.png'
    pages = []

    try:
        # now lets convert the PDF to Image
        # this is good resolution As far as I know
        with WandImage(filename=image_file, resolution=120) as img:
            img.background_color = Color("white")
            # keep good quality
            img.format = 'png'
            img.alpha_channel = False
            # save it to tmp name
            img.save(filename=temp_imagePath)
    except Exception, err:
        # always keep track the error until the code has been clean
        # print err
        print traceback.format_exc(err)
        return False
    else:
        """
        We finally success to convert pdf to image. 
        but image is not join by it self when we convert pdf files to image. 
        now we need to merge all file
        """
        try:
            # search all image in temp path. file name ends with uuid_set value
            list_im = glob.glob(tempdir + "/*")
            list_im.sort()  # sort the file before joining it
            for image in list_im:
                page = cv2.imread(image, cv2.IMREAD_UNCHANGED)

                if len(page.shape) > 2:
                    height, width, channels = page.shape
                else:
                    mn_channel = cv2.split(page)[0]
                    page = cv2.merge((mn_channel, mn_channel, mn_channel, mn_channel))
                    height, width, channels = page.shape

                if channels > 3:
                    forground = cv2.cvtColor(page.copy(), cv2.COLOR_BGR2RGB)
                    # background = generateGrid( height, width, 30)
                    page = cv2.cvtColor(forground, cv2.COLOR_BGR2RGB)

                frame = cv2.cvtColor(page, cv2.COLOR_BGR2RGB)

                pages.append(frame)

        except Exception, err:
            print traceback.format_exc(err)
            return False
        finally:
            # Remove all tempfile in temp dir
            for tmpfile in glob.glob(tempdir + "/*"):
                os.remove(tmpfile)
            # Remove temp dir
            os.rmdir(tempdir)
        gc.collect()
        return pages


def generateGrid(w, h, sq):
    color1 = (0xFF, 0xFF, 0xFF)
    color2 = (0x95, 0x95, 0x95)
    img = np.zeros((w, h, 3), dtype=np.uint8)
    c = np.fromfunction(lambda x, y: ((x // sq) + (y // sq)) % 2, (w, h))
    img[c == 0] = color1
    img[c == 1] = color2
    return img


def findCenter(img):
    # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # th, threshed = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)
    #
    # _, cnts, hierarchy = cv2.findContours(threshed, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # M = cv2.moments(cnts[0])
    #
    # cX = int(M["m10"] / M["m00"])
    # cY = int(M["m01"] / M["m00"])
    cX = img.shape[0] / 2
    cY = img.shape[1] / 2

    #  =========================

    return (cX, cY)


def generateThumbnailImage(image):
    """

    Args:
        image (object): numpy image object

    Returns: numpy image object

    """

    class point_2D(object):

        def __init__(self, x=0.0, y=0.0):
            self.x = x
            self.y = y

        @property
        def coor(self):
            return (self.x, self.y)

        @property
        def rcoor(self):
            return (int(round(self.x)), int(round(self.y)))

        def __str__(self):
            return str([self.x, self.y])

    def rotatepoint(cx, cy, angle, point):
        # cos(r)(px-cx) + (py-cy) sin(r)+cx
        # -sin(r)(a-m) + cos(r)(b-n)+n

        result = point_2D()
        # print result
        r = ((0 - angle) * (22.0 / 7.0)) / 180.0
        s = np.sin(r)
        c = np.cos(r)

        # Translate to origin
        result.x = point.x - cx
        result.y = point.y - cy

        # rotate point
        xnew = (c * result.x) - (result.y * s)
        ynew = (-s * result.x) + (result.y * c)

        # Translate back
        result.x = xnew + cx
        result.y = ynew + cy

        return result

    image = image.copy()

    origin = findCenter(image)

    height, width = image.shape[:2]
    circleRadius = int((np.minimum(height, width) / 2) * 0.7)
    triangleRadius = circleRadius * 0.8

    # Create circle
    # a = point_2D()
    # b = point_2D()

    # cv2.circle(img=image, center=o.rcoor, radius=10, color=(0, 0, 0), thickness=-1)
    # cv2.circle(img=image, center=a.rcoor, radius=10, color=(255, 0, 0), thickness=-1)
    # cv2.circle(img=image, center=b.rcoor, radius=10, color=(255, 255, 0), thickness=-1)
    # cv2.circle(img=image, center=c.rcoor, radius=10, color=(0, 150, 255), thickness=-1)

    button_image = np.zeros((circleRadius * 2, circleRadius * 2, 4), dtype=np.uint8)
    triangle_image = button_image.copy()

    _org = findCenter(button_image)
    o = point_2D()
    o.x = _org[0]
    o.y = _org[1]

    c = point_2D()
    c.x = round(o.x + triangleRadius)
    c.y = round(o.y)

    a = rotatepoint(o.x, o.y, 120, c)
    b = rotatepoint(o.x, o.y, 240, c)

    triangle_cnt = np.array([a.rcoor, b.rcoor, c.rcoor])
    cv2.circle(img=button_image, center=findCenter(button_image), radius=circleRadius, color=(255, 255, 255),
               thickness=-1)
    cv2.drawContours(triangle_image, [triangle_cnt], 0, (255, 255, 255), -1)

    button_image = button_image - triangle_image

    offset_x = origin[1] - (button_image.shape[1] / 2)
    offset_y = origin[0] - (button_image.shape[0] / 2)
    print button_image[:, :, 0].shape
    print button_image[:, :, 1].shape
    print button_image[:, :, 2].shape
    print button_image[:, :, 3].shape

    print image[:, :, 0].shape
    print image[:, :, 1].shape
    print image[:, :, 2].shape
    # print image[:,:,3].shape

    print (offset_x + button_image.shape[0]) - offset_x
    print (offset_y + button_image.shape[1]) - offset_y
    image[offset_x:offset_x + button_image.shape[0], offset_y:offset_y + button_image.shape[1], :3] -= button_image[:,
                                                                                                       :, :3]

    cv2.imshow(';', image)
    cv2.waitKey()

    return image


# cv2.imshow('',image)
# cv2.waitKey()

# ======= For testing ========================
# self.thumbnail = PreviewMedia(self)
# self.thumbnail.loadMedia("P:/Hanuman/scene/publ/act1/hnm_act1_q0030a_s0010/edit/animatic/hero/outputMedia/hnm_act1_q0030a_s0010_edit_animatic.hero.mov")
# self.thumbnail.loadMedia(
#             ["C:/Users/nook/Pictures/lego friends.jpeg", "C:/Users/nook/Pictures/wp-1480230666843.jpg"])
# self.thumbnail.loadMedia(['C:/Users/nook/META/SCRIPTS/MEME/memeXplorer/icons/F18.png'])
# self.thumbnail.loadMedia("C:/Users/nook/Pictures/Baby_concept_v032.psd")
# self.thumbnail.loadMedia("C:/Users/nook/Pictures/sunflower_lzw.tif")
# self.thumbnail.loadMedia(
#             "L:/F18/TVS1901/COMMON/RENDER/F18_TVS1901_SH0020/COMP/CMP_F18_TVS1901_SH0020.[101-200].exr")

class testWindow(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super(testWindow, self).__init__(parent)

        self.setGeometry(300, 300, 250, 300)

        self.initUI()
        self.initConnect()

    def initUI(self):
        self.mainLayout = QtWidgets.QVBoxLayout()

        self.thumbnail = PreviewMedia(self)

        self.textLabel = QtWidgets.QLabel(self)

        self.buttonLayout = QtWidgets.QHBoxLayout()
        self.loadMOVButton = QtWidgets.QPushButton(self)
        self.loadMOVButton.setText("Load MOV")
        self.loadJpegButton = QtWidgets.QPushButton(self)
        self.loadJpegButton.setText("Load JPEG")
        self.loadPngButton = QtWidgets.QPushButton(self)
        self.loadPngButton.setText("Load PNG")
        self.loadTiffButton = QtWidgets.QPushButton(self)
        self.loadTiffButton.setText("Load TIFF")
        self.loadPsdButton = QtWidgets.QPushButton(self)
        self.loadPsdButton.setText("Load PSD")
        self.loadExrButton = QtWidgets.QPushButton(self)
        self.loadExrButton.setText("Load EXR")
        self.loadPdfButton = QtWidgets.QPushButton(self)
        self.loadPdfButton.setText("Load PDF")
        self.buttonLayout.addWidget(self.loadMOVButton)
        self.buttonLayout.addWidget(self.loadJpegButton)
        self.buttonLayout.addWidget(self.loadPngButton)
        self.buttonLayout.addWidget(self.loadPsdButton)
        self.buttonLayout.addWidget(self.loadTiffButton)
        self.buttonLayout.addWidget(self.loadExrButton)
        self.buttonLayout.addWidget(self.loadPdfButton)
        self.buttonLayout.addWidget(self.textLabel)

        # self.loadPngButton.setEnabled(False)
        # self.loadPsdButton.setEnabled(False)
        # self.loadExrButton.setEnabled(False)
        # self.loadPdfButton.setEnabled(False)

        self.mainLayout.addWidget(self.thumbnail)
        self.mainLayout.addLayout(self.buttonLayout)

        self.setLayout(self.mainLayout)

    def initConnect(self):
        self.loadMOVButton.clicked.connect(self.loadMOVButton_onClicked)
        self.loadJpegButton.clicked.connect(self.loadJpegButton_onClicked)
        self.loadPngButton.clicked.connect(self.loadPngButton_onClicked)
        self.loadPsdButton.clicked.connect(self.loadPsdButton_onClicked)
        self.loadTiffButton.clicked.connect(self.loadTIFFButton_onClicked)
        self.loadExrButton.clicked.connect(self.loadExrButton_onClicked)
        self.loadPdfButton.clicked.connect(self.loadPdfButton_onClicked)

    def loadMOVButton_onClicked(self):
        # self.thumbnail.loadMedia("C:/Users/nook/Videos/ANM_F18_TVS1901_SH2240.mov")
        self.thumbnail.loadMedia("P:/Hanuman/scene/publ/act1/hnm_act1_q0030a_s0010/edit/animatic/hero/outputMedia/hnm_act1_q0030a_s0010_edit_animatic.hero.mov")

    def loadJpegButton_onClicked(self):
        self.thumbnail.loadMedia(
            ["C:/Users/nook/Pictures/lego friends.jpeg", "C:/Users/nook/Pictures/wp-1480230666843.jpg"])

    def loadPngButton_onClicked(self):
        self.thumbnail.loadMedia(['D:/seq/0.jpg'])

    def loadPsdButton_onClicked(self):
        self.thumbnail.loadMedia("D:/seq/psd.psd")

    def loadTIFFButton_onClicked(self):
        self.thumbnail.loadMedia("C:/Users/nook/Pictures/sunflower_lzw.tif")

    def loadExrButton_onClicked(self):
        self.thumbnail.loadMedia(
            'P:/DarkHorse/scene/publ/t1/dkh_t1_q0010_s0010/render/output/AlbMid_FG_v2/dkh_t1_q0010_s0010_render_AlbMid_FG_v2.[1001-1200].exr')


    # self.thumbnail.loadMedia("L:/F18/TVS1903/COMMON/RENDER/F18_TVS1903_SH0070/CHAR\REN_F18_TVS1903_SH0070__CHAR.[101-142].exr")

    def loadPdfButton_onClicked(self):
        # self.thumbnail.loadMedia(["C:/Users/nook/Downloads/examplefile/MultiViewOpenEXR.pdf"])
        self.thumbnail.loadMedia(["C:/Users/nook/Downloads/examplefile/DAT_FAC_REF_BRIEF.pdf"])


if __name__ == '__main__':
    app = QApplication(sys.argv[0])
    form = testWindow()
    form.show()
    app.exec_()

# image = generateGrid(1080,1920,10)
# image = cv2.imread("C:/Users/nook/Pictures/lego friends.jpeg")
# generateThumbnailImage(image)
