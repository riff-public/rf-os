import os
import sys
import traceback
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class LocalVar:
    dir = 'dir'
    file = 'file'


class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    grey = 'color: rgb(120, 120, 120);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgGrey = 'background-color: rgb(40, 40, 40);'


class FileComboBox(QtWidgets.QWidget) :
    currentIndexChanged = QtCore.Signal(str)
    def __init__(self, target=LocalVar.dir, addSuffix=str(), ext=[], notStartwith=[], layout=None, parent=None) :
        super(FileComboBox, self).__init__()
        self.target = target
        self.addSuffix = addSuffix
        self.ext = ext
        self.notStartwith = notStartwith
        self.skipList = []
        self.colorCheck = False

        # layout
        self.allLayout = layout if layout else QtWidgets.QVBoxLayout()
        # label
        self.label = QtWidgets.QLabel()
        # display listWidget
        self.comboBox = QtWidgets.QComboBox()

        # add layout
        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.comboBox)
        self.allLayout.setSpacing(2)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)

        # sort guide
        self.sortedGuide = []

        self.init_signals()


    def init_signals(self):
        self.comboBox.currentIndexChanged.connect(self.emit_signal)

    def set_label(self, text):
        return self.label.setText(text)

    def set_path(self, path):
        self.list_items(path)


    def list_items(self, path):
        files = list_file(path, self.ext) if self.target == LocalVar.file else list_folder(path, self.notStartwith) if self.target == LocalVar.dir else []
        self.comboBox.clear()
        files = self.sorted_file_filter(files)

        if files:
            for i, filename in enumerate(files):
                filePath = '%s/%s' % (path, filename)
                self.add_item(filename, filePath)

            self.emit_signal()

        if self.colorCheck:
            self.comboBox.setStyleSheet('')
            if not files:
                self.comboBox.setStyleSheet(Color.bgRed)

    def sorted_file_filter(self, files):
        if not self.sortedGuide:
            return files

        sortedFiles = []
        for item in self.sortedGuide:
            if item in files:
                sortedFiles.append(item)

        for file in files:
            if not file in sortedFiles:
                sortedFiles.append(file)

        return sortedFiles

    def add_item(self, filename, filePath=None):
        if not filename.lower() in [a.lower() for a in self.skipList]:
            self.comboBox.blockSignals(True)
            self.comboBox.addItem(filename)
            self.comboBox.setItemData(self.comboBox.count()-1, filePath, QtCore.Qt.UserRole)
            self.comboBox.blockSignals(False)

    def get_all_items(self):
        return [self.comboBox.itemText(a) for a in range(self.comboBox.count())]

    def get_all_items_data(self):
        return [self.comboBox.itemData(a, QtCore.Qt.UserRole) for a in range(self.comboBox.count())]

    def set_current(self, text):
        texts = self.get_all_items()
        index = texts.index(text) if text in texts else 0
        self.comboBox.setCurrentIndex(index)
        return True if text in texts else False

    def current_item(self):
        return self.comboBox.itemData(self.comboBox.currentIndex(), QtCore.Qt.UserRole)

    def current_text(self):
        return str(self.comboBox.currentText())

    def set_visible(self, state):
        self.label.setVisible(state)
        self.comboBox.setVisible(state)

    def set_enable(self, state):
        self.comboBox.setEnabled(state)

    def emit_signal(self):
        rawPath = self.comboBox.itemData(self.comboBox.currentIndex(), QtCore.Qt.UserRole)
        path = ('/').join([path, self.addSuffix]) if self.addSuffix else rawPath
        self.currentIndexChanged.emit(path)

    def comboBox_activate(self):
        self.comboBox.setStyleSheet('')

    def clear(self):
        self.comboBox.clear()

    def set_label_color(self, colors): 
        self.label.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))


def list_file(path, ext=[]):
    result = sorted([d for d in os.listdir(path) if os.path.isfile(os.path.join(path, d))]) if os.path.exists(path) else []
    return [a for a in result if os.path.splitext(a) in ext] if ext else result


def list_folder(path, notStartwith=[]):
    result = sorted([d for d in os.listdir(path) if os.path.isdir(os.path.join(path,d))]) if os.path.exists(path) else []
    return [a for a in result if all(not b in a for b in notStartwith)] if notStartwith else result
