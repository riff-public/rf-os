from collections import OrderedDict 
from Qt import QtWidgets
from Qt import QtCore 

from rf_utils import project_info


class Config: 
    asset = 'asset'
    scene = 'scene'
    entity_sg_map = {asset: 'Asset', scene: 'Shot'}
    sg_entity_map = {'Asset': asset, 'Shot': scene}
    asset_limit = ['design', 'model', 'texture', 'rig', 'groom', 'sim', 'lookdev']
    scene_limit = ['layout', 'anim', 'finalcam', 'fx', 'sim', 'light', 'comp', 'render']
    all_filter = 'All'


class EntityStepWidget(QtWidgets.QWidget):
    """docstring for EntityChoice"""
    step_selected = QtCore.Signal(object)

    def __init__(self, sg, project_info=None, parent=None):
        super(EntityStepWidget, self).__init__(parent=parent)
        self.sg = sg
        self.project_info = project_info
        self.setup_ui()
        self.load()
        self.init_signals()

    def setup_ui(self): 
        self.layout = QtWidgets.QHBoxLayout()
        self.asset_radioButton = QtWidgets.QRadioButton('Asset')
        self.scene_radioButton = QtWidgets.QRadioButton('Scene')
        self.step_label = QtWidgets.QLabel('Step')
        self.step_comboBox = QtWidgets.QComboBox()
        self.step_comboBox.setMinimumSize(QtCore.QSize(120, 21))
        spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.layout.addWidget(self.asset_radioButton)
        self.layout.addWidget(self.scene_radioButton)
        self.layout.addWidget(self.step_label)
        self.layout.addWidget(self.step_comboBox)
        # self.layout.addItem(spacer)

        self.layout.setStretch(0, 1)
        self.layout.setStretch(1, 2)
        self.layout.setStretch(2, 1)
        self.layout.setStretch(3, 2)
        self.setLayout(self.layout)


    def set_project(self, project): 
        self.project_info = project_info.ProjectInfo(project)

    def load(self): 
        self.step_dict = dict()
        filters = [{
            "filter_operator": "any",
            "filters": [
                ["entity_type", "is", 'Asset'],
                ["entity_type", "is", 'Shot']
                ]
            }]
        self.steps = self.sg.find('Step', [], ['code', 'entity_type', 'short_name'])

        for step in self.steps: 
            if not step['entity_type'] in self.step_dict.keys(): 
                self.step_dict[step['entity_type']] = {step['short_name']: step}
            else: 
                if not step['short_name'] in self.step_dict[step['entity_type']].keys(): 
                    self.step_dict[step['entity_type']][step['short_name']] = step 

        return self.step_dict

    def init_signals(self): 
        self.asset_radioButton.clicked.connect(self.entity_selected)
        self.scene_radioButton.clicked.connect(self.entity_selected)

    def entity_selected(self): 
        self.step_comboBox.clear()
        if self.asset_radioButton.isChecked(): 
            entity = Config.entity_sg_map[Config.asset]
            steps = self.project_info.step.list_asset()
            limit = Config.asset_limit
        elif self.scene_radioButton.isChecked(): 
            entity = Config.entity_sg_map[Config.scene]
            steps = self.project_info.step.list_scene()
            limit = Config.scene_limit

        index = 0 
        for i, step in enumerate(limit):     
            if step in steps: 
                self.step_comboBox.addItem(step)
                data = self.step_dict[entity].get(step)
                self.step_comboBox.setItemData(index, data, QtCore.Qt.UserRole)
                index += 1

    def get_step(self): 
        index = self.step_comboBox.currentIndex()
        data = self.step_comboBox.itemData(index, QtCore.Qt.UserRole)
        return data

    def get_entity_type(self, sgtype=True): 
        if self.asset_radioButton.isChecked(): 
            key = Config.asset 
        elif self.scene_radioButton.isChecked(): 
            key = Config.scene 
        if not sgtype: 
            return key 
        else: 
            return Config.entity_sg_map.get(key)


class EntityStepWidget2(EntityStepWidget):
    """docstring for EntityChoice"""
    step_selected = QtCore.Signal(object)

    def __init__(self, sg, project_info=None, parent=None):
        super(EntityStepWidget2, self).__init__(sg, project_info=project_info, parent=parent)

    def setup_ui(self): 
        self.layout = QtWidgets.QVBoxLayout()
        self.layout_h = QtWidgets.QHBoxLayout()
        self.asset_radioButton = QtWidgets.QRadioButton('Asset')
        self.scene_radioButton = QtWidgets.QRadioButton('Scene')
        self.step_label = QtWidgets.QLabel('Step')
        self.step_comboBox = QtWidgets.QComboBox()
        self.step_comboBox.setMinimumSize(QtCore.QSize(120, 21))
        spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.layout_h.addWidget(self.asset_radioButton)
        self.layout_h.addWidget(self.scene_radioButton)
        self.layout.addLayout(self.layout_h)
        self.layout.addWidget(self.step_label)
        self.layout.addWidget(self.step_comboBox)
        self.layout.addItem(spacer)

        self.layout.setStretch(0, 1)
        self.layout.setStretch(1, 2)
        self.layout.setStretch(2, 1)
        self.layout.setStretch(3, 2)
        self.setLayout(self.layout)

    