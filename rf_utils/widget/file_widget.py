import os
import sys
import yaml
from collections import OrderedDict
from functools import partial
import subprocess

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

from rf_utils.widget import textEdit

class Icon:
    """ Icon Extension Mapping """
    extMap = {
        '.ma': '{}/icons/ext/maya_icon.png'.format(moduleDir),
        '.mb': '{}/icons/ext/maya_icon.png'.format(moduleDir),
        '.jpg': '{}/icons/ext/jpg_icon.png'.format(moduleDir),
        '.png': '{}/icons/ext/png_icon.png'.format(moduleDir),
        '.exr': '{}/icons/ext/exr_icon.png'.format(moduleDir),
        '.pdf': '{}/icons/ext/pdf_icon.png'.format(moduleDir),
        '.tif': '{}/icons/ext/tiff_icon.png'.format(moduleDir),
        '.tiff': '{}/icons/ext/tiff_icon.png'.format(moduleDir),
        '.abc': '{}/icons/ext/abc_icon.png'.format(moduleDir),
        '.mov': '{}/icons/ext/mov_icon.png'.format(moduleDir),
        '.aep': '{}/icons/ext/afterfx_icon.png'.format(moduleDir),
        '.nk': '{}/icons/ext/nuke_icon.png'.format(moduleDir),
        'unknown': '{}/icons/ext/unknown_icon.png'.format(moduleDir), 
        'seq': '{}/icons/ext/seq_icon.png'.format(moduleDir)
    }
    dir = '{}/icons/ext/dir_icon.png'.format(moduleDir)
    pencil = '{}/icons/etc/pencil_icon.png'.format(moduleDir)
    noprev = '{}/icons/etc/nopreview_icon.png'.format(moduleDir)

class FileListWidget(QtWidgets.QWidget):
    """ Diplay file and put associated icon """
    itemSelected = QtCore.Signal(str)
    itemDataSelected = QtCore.Signal(dict)
    itemDoubleClicked = QtCore.Signal(dict)

    def __init__(self, parent=None):
        super(FileListWidget, self).__init__(parent=parent)
        self.displayNameOnly = True

        self.setup_ui()
        self.init_signals()

    def setup_ui(self):
        self.allLayout = QtWidgets.QHBoxLayout()
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

        self.allLayout.addWidget(self.listWidget)

        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)
        

    def init_signals(self):
        self.listWidget.itemSelectionChanged.connect(self.item_selected)
        # self.listWidget.currentItemChanged.connect(self.item_selected)
        self.listWidget.itemDoubleClicked.connect(self.item_double_clicked)
        self.listWidget.customContextMenuRequested.connect(self.show_menu)

    def item_double_clicked(self):
        self.itemDoubleClicked.emit(self.current_data())

    def show_menu(self, pos):
        itemAt = self.listWidget.itemAt(pos)
        if itemAt:
            menu = QtWidgets.QMenu(self)
            self.show_explorer(menu)
            menu.popup(self.listWidget.mapToGlobal(pos))

    def show_explorer(self, menu):
        selItem = self.listWidget.currentItem()
        if selItem:
            explorerMenu = menu.addAction('Open in Explorer')
            explorerMenu.triggered.connect(partial(self.open_explorer, selItem))

    def open_explorer(self, item):
        path = item.data(QtCore.Qt.UserRole)
        subprocess.Popen(r'explorer /select,"{}"'.format(path.replace('/', '\\')))

    def item_selected(self):
        currentItem = self.current_item()
        if currentItem:
            data = currentItem.data(QtCore.Qt.UserRole)
        else:
            data = None
        self.itemSelected.emit(data)

    def remove_all(self):
        for i in xrange(self.listWidget.count()):
            self.listWidget.takeItem(i)

    def clear(self):
        """ clear listWidget """
        self.listWidget.clear()

    def add_files(self, files):
        """ add multiple files """
        if files:
            for file in files:
                self.add_file(file)

    def add_file(self, filepath):
        """ add file item """
        # isfile = os.path.isfile(filepath)

        iconWidget = self.icon_widget(filepath)
        display = os.path.basename(filepath) if self.displayNameOnly else filepath
        self.add_item(display, filepath, iconWidget)

    def add_sequences(self, seqDir): 
        """ add sequences type file """ 
        iconWidget = self.icon_widget(iconPath=Icon.extMap['seq']) 
        display = os.path.basename(seqDir) if self.displayNameOnly else seqDir
        self.add_item(display, seqDir, iconWidget)


    def add_data(self, filepath, data):
        """ add file item """
        # isfile = os.path.isfile(filepath)

        iconWidget = self.icon_widget(filepath)
        return self.add_item(os.path.basename(filepath), data, iconWidget)

    def add_item(self, text, data, iconWidget):
        """ add item """
        item = QtWidgets.QListWidgetItem(self.listWidget)
        item.setData(QtCore.Qt.UserRole, data)
        item.setText(text)

        if iconWidget:
            item.setIcon(iconWidget)

        return item

    def icon_widget(self, filepath='', iconPath=''):
        """ icon widget """
        isdir = os.path.isdir(filepath)
        name, ext = os.path.splitext(filepath)
        
        if not iconPath: 
            iconPath = Icon.dir if isdir else Icon.extMap.get(ext, Icon.extMap['unknown'])

        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(iconPath), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        return iconWidget

    def current_item(self):
        return self.listWidget.currentItem()

    def items(self):
        return [self.listWidget.item(a) for a in range(self.listWidget.count())]

    def current_data(self):
        return self.listWidget.currentItem().data(QtCore.Qt.UserRole) if self.listWidget.currentItem() else None

    def selected_file(self):
        return self.current_data()

    def selected_files(self):
        return [a.data(QtCore.Qt.UserRole) for a in self.listWidget.selectedItems()]

    def get_all_files(self):
        return [self.listWidget.item(a).data(QtCore.Qt.UserRole) for a in range(self.listWidget.count())]

    def list_files(self, path, sortedVersion='bottom'):
        if os.path.exists(path):
            files = [('/').join([path, a])
                     for a in os.listdir(path) if os.path.isfile(('/').join([path, a]))]
            if files:
                if sortedVersion == 'bottom':
                    self.add_files(sorted(files))

                if sortedVersion == 'top':
                    self.add_files(sorted(files)[::-1])
            else:
                self.set_empty()
        else:
            logger.warning('path not exists {}'.format(path))

    def set_empty(self, text=''):
        text = 'No file' if not text else text
        self.listWidget.addItem(text)

    def set_bg_color(self, path, color):
        allFiles = self.get_all_files()
        index = allFiles.index(path) if path in allFiles else None
        item = self.listWidget.item(index) if index else None
        item.setBackground(QtGui.QColor(color[0], color[1], color[2])) if item else None

class ImageListWidget(FileListWidget):
    """ Diplay as thumbnail """

    def __init__(self):
        super(ImageListWidget, self).__init__()
        self.listWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.supportFormat = ['.jpg', '.png', '.JPG', '.PNG', '.JPEG']
        value = 128
        self.listWidget.setIconSize(QtCore.QSize(value, value))

    def init_signals(self):
        self.listWidget.itemSelectionChanged.connect(self.item_selected)
        self.listWidget.itemDoubleClicked.connect(self.view_image)

    def view_image(self, item):
        """ show image on default os viewer """
        imageFilePath = item.data(QtCore.Qt.UserRole)
        os.startfile(imageFilePath)

    def icon_widget(self, filepath):
        """ override iconwidget by return filepath """
        name, ext = os.path.splitext(filepath)
        iconPath = filepath if ext in self.supportFormat else Icon.noprev

        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(iconPath), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        return iconWidget

class FileListWidgetExtended(FileListWidget):
    """ File Listwidget with .data to show/hide curtain file """
    unlocked = QtCore.Signal(object)

    def __init__(self):
        # internals
        self._show_screenshot = True
        self._screenshot_size = 256

        # file format
        self.no_description_str = '< No description >'
        self.bg_col = (43, 43, 43)
        self.edit_col = (100, 50, 50)

        self.active_font = QtGui.QFont()
        self.active_font.setItalic(False)
        self.active_font.setPointSize(8)

        self.display_font = QtGui.QFont()
        self.display_font.setItalic(True)
        self.display_font.setPointSize(8)

        self.grey_font_palette = QtGui.QPalette()
        self.grey_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(192, 192, 192)))

        self.white_font_palette = QtGui.QPalette()
        self.white_font_palette.setBrush(QtGui.QPalette.Text, QtGui.QBrush(QtGui.QColor(255, 255, 255)))

        super(FileListWidgetExtended, self).__init__()

    def setup_ui(self):
        self.allLayout = QtWidgets.QHBoxLayout()

        # splitter
        self.mainSplitter = QtWidgets.QSplitter()
        self.mainSplitter.setOrientation(QtCore.Qt.Vertical)

        self.fileSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.descriptionSplitWidget = QtWidgets.QWidget(self.mainSplitter)
        self.descriptionSplitWidget.setMinimumHeight(50)
        self.mainSplitter.setSizes([750, 0])
        self.allLayout.addWidget(self.mainSplitter)

        self.listWidgetLayout = QtWidgets.QVBoxLayout(self.fileSplitWidget)
        self.listWidgetLayout.setContentsMargins(0, 0, 0, 0)
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.listWidgetLayout.addWidget(self.listWidget)

        # description
        self.description_layout = QtWidgets.QVBoxLayout(self.descriptionSplitWidget)
        self.description_layout.setContentsMargins(0, 0, 0, 0)
        self.description_layout.setSpacing(0)

        self.description_title_layout = QtWidgets.QHBoxLayout()
        self.description_title_layout.setContentsMargins(0, 0, 0, 0)
        self.description_layout.addLayout(self.description_title_layout)

        self.description_label = QtWidgets.QLabel('Description:')
        self.description_title_layout.addWidget(self.description_label)

        spacerItem33 = QtWidgets.QSpacerItem(25, 25, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.description_title_layout.addItem(spacerItem33)

        self.edit_description_button = QtWidgets.QPushButton('')
        self.edit_description_button.setCheckable(True)
        self.edit_description_button.setFixedSize(QtCore.QSize(25, 18))
        self.edit_description_button.setIcon(QtGui.QIcon(Icon.pencil))
        self.description_title_layout.addWidget(self.edit_description_button)

        self.description_textEdit = textEdit.TextEditWithPlaceHolder()
        # self.description_textEdit.setMinimumHeight(50)
        self.description_textEdit.setPlaceholderText(self.no_description_str)
        self.description_textEdit.setWordWrapMode(QtGui.QTextOption.WrapAtWordBoundaryOrAnywhere)
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.description_textEdit.setFont(self.display_font)
        self.description_textEdit.setPalette(self.grey_font_palette)
        self.description_layout.addWidget(self.description_textEdit)

        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)
        
    def init_signals(self):
        self.listWidget.itemSelectionChanged.connect(self.item_selected)
        # self.listWidget.currentItemChanged.connect(self.item_selected)
        self.listWidget.customContextMenuRequested.connect(self.show_menu)
        self.edit_description_button.clicked.connect(self.edit_description_toggled)
        self.description_textEdit.editorLostFocus.connect(lambda: self.disable_description_edit(force=False))

    def disable_description_edit(self, force=True):
        # set toggle off
        if force:
            self.edit_description_button.blockSignals(True)
            self.edit_description_button.setChecked(False)
            self.edit_description_button.blockSignals(False)
        else:
            self.edit_description_button.setChecked(False)
            self.edit_description_toggled()

        # set non editable
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        # set look
        self.description_textEdit.setFont(self.display_font)
        self.description_textEdit.setPalette(self.grey_font_palette)
        self.description_textEdit.setStyleSheet('background-color: rgb{};'.format(self.bg_col))

    def enable_description_edit(self):
        # set editable
        self.description_textEdit.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
        # set look
        self.description_textEdit.setPalette(self.white_font_palette)
        self.description_textEdit.setFont(self.active_font)
        self.description_textEdit.setFocus()
        self.description_textEdit.moveCursor(QtGui.QTextCursor.End)
        self.description_textEdit.setStyleSheet('background-color: rgb{};'.format(self.edit_col))

    def edit_description_toggled(self):
        items = self.listWidget.selectedItems()
        if not items:
            self.disable_description_edit()
            return
        item = items[0]
        data = item.data(QtCore.Qt.UserRole)
        tag = FileTag(data)
        extra_tags = tag.get_extra_tag()
        original_des = u''
        if extra_tags:
            original_desc = extra_tags.get('description', u'')

        # start editing
        if self.edit_description_button.isChecked():
            self.enable_description_edit()
        else: # finish editing
            current_text = self.description_textEdit.toPlainText()
            if current_text != original_desc:
                # popup question to user
                qmsgBox = QtWidgets.QMessageBox(self)
                qmsgBox.setText('Edit file description?')
                qmsgBox.setWindowTitle('Confirm')
                qmsgBox.setIcon(QtWidgets.QMessageBox.Question)
                qmsgBox.addButton('  Yes  ', QtWidgets.QMessageBox.AcceptRole)
                qmsgBox.addButton('  No  ', QtWidgets.QMessageBox.RejectRole)
                answer = qmsgBox.exec_()
                if answer == 0:
                    try:
                        extra_tags.update({'description': current_text})
                        tag.set_extra_tag(extra_tags)
                    except Exception as e:
                        print(e)
                        self.description_textEdit.setPlainText(original_desc)
                else:
                    # set text back to previous text
                    self.description_textEdit.setPlainText(original_desc)
            # disable edit UI
            self.disable_description_edit()

    def add_file(self, filepath):
        """ add file item """
        tag = FileTag(filepath)
        mode = tag.get_mode()
        if not TagMode.tagname in filepath:
            if mode == TagMode.hide or os.path.basename(filepath).startswith('.'):
                return

            # add item
            iconWidget = self.icon_widget(filepath)
            item = self.add_item(os.path.basename(filepath), filepath, iconWidget)

            # set disabled
            if mode == TagMode.disable:
                item.setFlags(QtCore.Qt.ItemIsSelectable)

            # set screenshot
            basename = os.path.basename(filepath)
            fn, ext = os.path.splitext(basename)
            dirname = os.path.dirname(filepath)
            ss_path = dirname + '/.preview/.' + fn + '.jpg'
            if self._show_screenshot and os.path.exists(ss_path):
                ss_img = QtGui.QImage(ss_path).scaled(QtCore.QSize(self._screenshot_size, self._screenshot_size), QtCore.Qt.KeepAspectRatio)
                data = QtCore.QByteArray()
                buffer = QtCore.QBuffer(data)
                ss_img.save(buffer, 'JPG', 100)
                html = "<img src='data:image/jpg;base64, {}'>".format(data.toBase64())
                item.setToolTip(html)
            else:
                item.setToolTip('')

    def unlock_file(self):
        items = self.items()
        for item in items:
            item.setFlags(QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
        
        self.unlocked.emit(True)

    def show_menu(self, pos):
        menu = QtWidgets.QMenu(self)
        self.show_explorer(menu)
        self.unlock_menu(menu)
        self.extra_menu(menu)
        self.copy_clipboard_menu(menu)
        menu.popup(self.listWidget.mapToGlobal(pos))

    def unlock_menu(self, menu):
        selItem = self.listWidget.currentItem()
        explorerMenu = menu.addAction('Unlock')
        explorerMenu.triggered.connect(partial(self.unlock_file))

    def extra_menu(self, menu):
        selItem = self.listWidget.currentItem()
        explorerMenu = menu.addAction('Reference')
        explorerMenu.triggered.connect(partial(self.reference, selItem))

    def copy_clipboard_menu(self, menu): 
        selItem = self.listWidget.currentItem()
        explorerMenu = menu.addAction('Copy to Clipboard')
        explorerMenu.triggered.connect(partial(self.copy_clipboard, selItem))

    def reference(self, item):
        path = item.data(QtCore.Qt.UserRole)
        namespace, ext = os.path.splitext(os.path.basename(path))
        self.hook(namespace, path)

    def copy_clipboard(self, item): 
        path = item.data(QtCore.Qt.UserRole)
        namespace, ext = os.path.splitext(os.path.basename(path))
        command = 'echo | set /p={}|clip'.format(path.strip())
        subprocess.call(command, shell=True)

    def hook(self, namespace, path):
        pass

    def item_selected(self):
        currentItem = self.current_item()
        self.description_textEdit.clear()
        data = None
        if currentItem:
            data = currentItem.data(QtCore.Qt.UserRole)
            # get description from tags
            tag = FileTag(data)
            curr_extra_tag = tag.get_extra_tag()
            if curr_extra_tag:
                desc = curr_extra_tag.get('description', u'')
                self.description_textEdit.setPlainText(desc)

        self.itemSelected.emit(data)

    # def mouseEnterEvent(self, event):
    #     super(FileListWidgetExtended, self).mouseMoveEvent(event)
    #     event.accept()

    # def mouseMoveEvent(self, event):
    #     super(FileListWidgetExtended, self).mouseMoveEvent(event)
    #     print event
    #     pos = event.pos()
    #     item = self.listWidget.itemAt(pos)
    #     print item
    #     event.accept()

class TagMode:
    hide = 'hide'
    disable = 'disable'
    tagname = '.tag'

class FileTag(object):
    """ Tag a file than can display or selectable """

    def __init__(self, filename):
        super(FileTag, self).__init__()
        self.filename = filename

        self.tagfile = '{}/{}'.format(os.path.dirname(self.filename), TagMode.tagname)

    def set_tag(self, mode, tagData=OrderedDict()):
        dirname = os.path.dirname(self.filename)
        if not os.path.exists(dirname):
            logger.warning('Input file not exists')
            return

        data = self.append_data(mode, tagData)
        return self.write_tag_file(data)

    def tag_data(self):
        """ read file tag data """
        data = self.read_tag_file()
        if self.filename in data.keys():
            return data[self.filename]
        else:
            return OrderedDict()

    def get_mode(self):
        tagData = self.tag_data()
        if 'mode' in tagData.keys():
            return tagData['mode']

    def get_extra_tag(self):
        tagData = self.tag_data()
        if 'tag' in tagData.keys():
            return tagData['tag']

    def get_tag_files(self):
        data = self.read_tag_file()
        return data.keys()

    def set_mode(self, mode):
        extraTag = self.get_extra_tag()
        data = self.append_data(mode, extraTag)
        self.write_tag_file(data)

    def set_extra_tag(self, extraTag):
        mode = self.get_mode()
        data = self.append_data(mode, extraTag)
        self.write_tag_file(data)

    def read_tag_file(self):
        """ read tag """
        if not os.path.exists(self.tagfile):
            data = OrderedDict()
            # yml_dumper(self.tagfile, data)
            return data
        else:
            return yml_loader(self.tagfile)

    def write_tag_file(self, data):
        return yml_dumper(self.tagfile, data)

    def append_data(self, mode, extraTagData):
        extraTagData = extraTagData if extraTagData else OrderedDict()
        data = self.read_tag_file()
        tagDict = OrderedDict()
        tagDict['mode'] = mode
        tagDict['tag'] = extraTagData
        data[self.filename] = tagDict

        return data


def writeFile(file, data):
    f = open(file, 'w')
    f.write(data)
    f.close()
    return True


def yml_dumper(file, dictData):
    # input dictionary data
    data = ordered_dump(dictData, Dumper=yaml.SafeDumper, default_flow_style=False)
    result = writeFile(file, data)
    return result


def yml_loader(file):
    class Loader(yaml.Loader):
        """ add !include yaml constructor """

        def __init__(self, stream):
            self._root = os.path.split(stream.name)[0]
            super(Loader, self).__init__(stream)

        def include(self, node):
            filename = os.path.join(self._root, self.construct_scalar(node))

            with open(filename, 'r') as f:
                return ordered_load(f, Loader)

    Loader.add_constructor('!include', Loader.include)

    with open(file, 'r') as stream:
        dictData = ordered_load(stream, Loader)

        return dictData


def ordered_load(stream, Loader=yaml.Loader, object_pairs_hook=OrderedDict):
    class OrderedLoader(Loader):
        pass

    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))
    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)
    return yaml.load(stream, OrderedLoader)


def ordered_dump(data, stream=None, Dumper=yaml.Dumper, **kwds):
    class OrderedDumper(Dumper):
        pass

    def _dict_representer(dumper, data):
        return dumper.represent_mapping(
            yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
            data.items())
    OrderedDumper.add_representer(OrderedDict, _dict_representer)
    return yaml.dump(data, stream, OrderedDumper, **kwds)
