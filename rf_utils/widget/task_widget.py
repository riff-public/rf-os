import os
import sys
from datetime import datetime
from collections import OrderedDict

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)


class TaskComboBoxUI(QtWidgets.QWidget):
    """docstring for TaskComboBoxUI"""
    currentIndexChanged = QtCore.Signal(str)
    def __init__(self, parent=None):
        super(TaskComboBoxUI, self).__init__(parent=parent)
        self.allLayout = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel()
        self.comboBox = QtWidgets.QComboBox()

        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.comboBox)

        # self.allLayout.setStretch(0, 1)
        # self.allLayout.setStretch(1, 2)

        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)


class TaskComboBox(TaskComboBoxUI):
    """docstring for TaskComboBox"""
    def __init__(self, sg, parent=None):
        super(TaskComboBox, self).__init__(parent=parent)
        self.sg = sg
        self.entityType = None
        self.projectName = None
        self.entityName = None


    def task_entities(self, projectName, entityType, entityName):
        entityKey = 'entity.%s.code' % entityType
        filter = [['project.Project.name', 'is', projectName], [entityKey, 'is', entityName]]
        fields = ['sg_status_list', 'id', 'content']
        taskEntities = sg.find('Task', filter, fields) or []

    def set_task(self, projectName, entityType, entityName):
        self.comboBox.clear()
        taskEntities = self.task_entities(projectName, entityType, entityName)
        for row, taskEntity in enumerate(taskEntities):
            self.comboBox.addItem(taskEntity['content'])
            self.comboBox.setItemData(row, taskEntity)


class TaskThreadComboBox(TaskComboBoxUI):
    """docstring for TaskComboBox"""
    fetchFinished = QtCore.Signal(bool)

    def __init__(self, parent=None):
        super(TaskThreadComboBox, self).__init__(parent=parent)
        self.entityType = None
        self.projectName = None
        self.entityName = None

    def fetch_task(self, projectName, entityType, entityName, step):
        self.thread = GetTaskThread(projectName, entityType, entityName, step)
        self.thread.value.connect(self.set_task)
        self.thread.start()

    def set_task(self, taskEntities):
        self.comboBox.clear()
        for row, taskEntity in enumerate(taskEntities):
            self.comboBox.addItem(taskEntity['content'])
            self.comboBox.setItemData(row, taskEntity)
        self.fetchFinished.emit(True)

    def list_tasks(self): 
        items = [self.comboBox.itemText(a) for a in range(self.comboBox.count())]
        return items

    def current_item(self): 
        return self.comboBox.itemData(self.comboBox.currentIndex(), QtCore.Qt.UserRole)


class GetTaskThread(QtCore.QThread):
    """docstring for SubmitDialy"""
    value = QtCore.Signal(list)

    def __init__(self, projectName, entityType, entityName, step):
        super(GetTaskThread, self).__init__()
        self.projectName = projectName
        self.entityType = entityType
        self.entityName = entityName
        self.step = step

    def run(self):
        from rf_utils.sg import sg_utils
        sg = sg_utils.sg
        entityKey = 'entity.%s.code' % self.entityType
        filter = [['project.Project.name', 'is', self.projectName],
                    [entityKey, 'is', self.entityName],
                    ['step.Step.code', 'is', self.step]]
        fields = ['sg_status_list', 'id', 'content', 'step', 'project', 'entity']
        taskEntities = sg.find('Task', filter, fields) or []
        self.value.emit(taskEntities)


