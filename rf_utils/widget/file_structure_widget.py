uiName = 'FileStructureWidget'
_title = 'File Structure Widget'
_version = 'v.0.0.2'
_des = 'add asset lister'

import os
import sys
import traceback
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
import rf_config as config
RFPROJECT = config.Env.projectVar


from rf_utils.sg import sg_process
from rf_utils.context import context_info
from rf_utils import project_info
from rf_utils import file_utils
from rf_utils.widget import file_widget



class Config():
    project_path = "{}:/".format(RFPROJECT)


class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)

        self.all_layout = QtWidgets.QVBoxLayout()
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.horizontalLayout_project = QtWidgets.QHBoxLayout()
        self.label_2 = QtWidgets.QLabel("Project :")
        self.project_comboBox = QtWidgets.QComboBox()
        self.project_comboBox.setMinimumSize(QtCore.QSize(200, 0))
        self.spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)


        self.horizontalLayout_project.addWidget(self.label_2)
        self.horizontalLayout_project.addWidget(self.project_comboBox)
        self.horizontalLayout_project.addItem(self.spacerItem)

        self.mode_horizontal = QtWidgets.QHBoxLayout()
        self.ep_label = QtWidgets.QLabel('Episode')
        self.ep_comboBox = QtWidgets.QComboBox()

        self.mode_horizontal.addWidget(self.ep_label)
        self.mode_horizontal.addWidget(self.ep_comboBox)

        self.sequence_label =QtWidgets.QLabel('Sequence')
        self.sequence_comboBox = QtWidgets.QComboBox()

        self.mode_horizontal.addWidget(self.sequence_label)
        self.mode_horizontal.addWidget(self.sequence_comboBox)
        self.line = QtWidgets.QFrame()
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)

        self.mode_horizontal.addWidget(self.line)
        self.label_3 = QtWidgets.QLabel("Department")

        self.mode_horizontal.addWidget(self.label_3)
        self.department_comboBox = QtWidgets.QComboBox()

        self.displayAllCheckBox = QtWidgets.QCheckBox('Show All')

        self.mode_horizontal.addWidget(self.department_comboBox)
        self.mode_horizontal.addWidget(self.displayAllCheckBox)

        self.verticalLayout.addLayout(self.horizontalLayout_project)
        self.verticalLayout.addLayout(self.mode_horizontal)
        self.folder_listWidget = QtWidgets.QListWidget()
        self.folder_listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.file_listWidget = file_widget.FileListWidgetExtended()

        self.horizontalLayout_list_widget = QtWidgets.QHBoxLayout()
        self.horizontalLayout_list_widget.addWidget(self.folder_listWidget)
        self.horizontalLayout_list_widget.addWidget(self.file_listWidget)

        self.verticalLayout.addLayout(self.horizontalLayout_list_widget)
        self.signal_pushButton = QtWidgets.QPushButton("Signals")

        self.verticalLayout.addWidget(self.signal_pushButton)
        self.all_layout.addLayout(self.verticalLayout)
        self.setLayout(self.all_layout)

class FileStructureWidget(Ui):
    sendData = QtCore.Signal(str)
    def __init__ (self, parent=None):
        super(FileStructureWidget, self).__init__(parent)
        self.shots_data = {}
        self.init_process()
        self.init_display()
        self.display_sequence()
        # self.display_shot()
        self.init_signals()
        
    def init_signals(self):
        self.project_comboBox.currentIndexChanged.connect(self.init_display)
        self.ep_comboBox.currentIndexChanged.connect(self.display_sequence)
        self.sequence_comboBox.currentIndexChanged.connect(self.display_shot)
        self.folder_listWidget.itemSelectionChanged.connect(self.display_lastest_file)
        self.department_comboBox.currentIndexChanged.connect(self.department_changed)
        self.displayAllCheckBox.stateChanged.connect(self.showAll_checked)

    def init_process(self):
        self.signal_pushButton.setVisible(False)
        self.list_projects = project_info.ProjectInfo().list_all(asObject=False)
        self.project_comboBox.addItems(self.list_projects)
        self.project_comboBox.setCurrentIndex(self.list_projects.index('SevenChickMovie'))
        self.department_comboBox.addItems(['anim', 'techanim'])

    def init_display(self): 
        self.get_shot_data() ########## list episode and list shot_data (use when change project : list all _shot in seq)
        self.display_episode()
        

    def get_shot_data(self):
        
        project = str(self.project_comboBox.currentText())
        sg = sg_process.sg 
        shots = sg.find('Shot', [['project.Project.name', 'is', project]], ['sg_sequence', 'code', 'sg_episode.Scene.code', 'sg_sequence_code', 'sg_shot_type'])
        
        context = context_info.Context()
        context.use_sg(sg, project, 'scene', entityName=shots[0]['code'])
        entity = context_info.ContextPathInfo(context=context)

        # shots_data['act1'] = {'seq':
        #                     {'shot':'path'}
        #                       'seq':
        #                      {'shot':'path'} }
        self.episode = []
        self.shots_data = OrderedDict()

        for i, shot in enumerate(shots): 

            entityGrp = shot['sg_episode.Scene.code']
            if not entityGrp in self.episode:
                self.episode.append(entityGrp)
            
            entityParent = shot['sg_sequence_code']
            entity.context.update(entity=shot['code'], entityGrp=entityGrp, entityParent=entityParent)
            entity.update_scene_context()
            shot_name = entity.name

            shot_path = entity.path.name().abs_path()
            shot_dict = {shot_name: shot_path}
            seq_dict = {entityParent: shot_dict}
            
            if shot['sg_shot_type'] == 'Shot':
                if not entityGrp in self.shots_data:
                    self.shots_data[entityGrp] = seq_dict
                else:
                    if not entityParent in self.shots_data[entityGrp]:
                        self.shots_data[entityGrp].update(seq_dict)
                    else:
                        self.shots_data[entityGrp][entityParent].update(shot_dict)
    
    def display_episode(self):
        self.ep_comboBox.addItems(self.episode)

    def display_sequence(self):
        act_text = self.ep_comboBox.currentText()
        act_list = [a for a in self.shots_data[act_text]]
        self.sequence_comboBox.clear()
        self.sequence_comboBox.addItems(sorted(act_list))
        self.display_shot()

    def department_changed(self): 
        self.file_listWidget.clear()
        self.display_lastest_file()

    def showAll_checked(self): 
        self.file_listWidget.clear()
        self.display_lastest_file()
        

    def display_shot(self):
        act_text = self.ep_comboBox.currentText()
        seq_text = self.sequence_comboBox.currentText()
        dep_text = self.department_comboBox.currentText()

        self.folder_listWidget.clear()
        
        for act in self.shots_data:
            if act == act_text:
                for seq in self.shots_data[act]:
                    if seq == seq_text:
                        for shot_name, path in sorted(self.shots_data[act][seq].iteritems()):
                            item = QtWidgets.QListWidgetItem()
                            item.setText(shot_name)
                            item.setData(QtCore.Qt.UserRole, path)
                            self.folder_listWidget.addItem(item)

    def display_lastest_file(self):
        list_item = self.folder_listWidget.selectedItems()
        self.file_listWidget.clear()

        for item in list_item:
            shot_path = item.data( QtCore.Qt.UserRole)
            files = self.check_lastest_file(shot_path)
            
            for file in files: 
                self.file_listWidget.add_file(file)



    def check_lastest_file(self, path):
        showAll = self.displayAllCheckBox.isChecked()
        step = str(self.department_comboBox.currentText())
        entity = context_info.ContextPathInfo(path=path)
        entity.context.update(step=step, process='main', app='maya')
        path = entity.path.workspace().abs_path()
        if not showAll: 
            latestFile = file_utils.get_lastest_file(path, 'ma')
            if not latestFile: 
                return ['No files']
            return [latestFile]
        else: 
            return file_utils.list_file(path)


class Main(QtWidgets.QMainWindow):
    def __init__ (self, parent=None):
        super(Main, self).__init__(parent)
        self.fileStructureWidget = FileStructureWidget()
        self.setCentralWidget(self.fileStructureWidget)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))

def show():
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = Main(maya_win.getMayaWindow())
    myApp.show()
    return myApp
