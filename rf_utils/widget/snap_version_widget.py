import os
import sys
import yaml
from collections import OrderedDict
from functools import partial
import subprocess

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)


class Icon:
    """ Icon Extension Mapping """
    extMap = {
        '.ma': '%s/icons/ext/maya_icon.png' % moduleDir,
        '.mb': '%s/icons/ext/maya_icon.png' % moduleDir,
        '.jpg': '%s/icons/ext/jpg_icon.png' % moduleDir,
        '.png': '%s/icons/ext/png_icon.png' % moduleDir,
        '.abc': '%s/icons/ext/abc_icon.png' % moduleDir,
        '.mov': '%s/icons/ext/mov_icon.png' % moduleDir,
        'unknown': '%s/icons/ext/unknown_icon.png' % moduleDir
    }
    dir = '%s/icons/ext/dir_icon.png' % moduleDir
    noprev = '%s/icons/etc/nopreview_icon.png' % moduleDir


class SnapVersionWidget(QtWidgets.QWidget):
    """ Diplay file and put associated icon """
    itemSelected = QtCore.Signal(str)
    itemDataSelected = QtCore.Signal(dict)

    def __init__(self, parent=None):
        super(SnapVersionWidget, self).__init__(parent=parent)
        self.allLayout = QtWidgets.QHBoxLayout()
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

        self.allLayout.addWidget(self.listWidget)

        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)
        self.init_signals()

    def init_signals(self):
        self.listWidget.itemSelectionChanged.connect(self.item_selected)

    def item_selected(self):
        currentItem = self.current_item()
        self.itemSelected.emit(currentItem)

    def clear(self):
        """ clear listWidget """
        self.listWidget.clear()



def writeFile(file, data):
    f = open(file, 'w')
    f.write(data)
    f.close()
    return True


def yml_dumper(file, dictData):
    # input dictionary data
    data = ordered_dump(dictData, Dumper=yaml.SafeDumper, default_flow_style=False)
    result = writeFile(file, data)
    return result


def yml_loader(file):
    class Loader(yaml.Loader):
        """ add !include yaml constructor """

        def __init__(self, stream):
            self._root = os.path.split(stream.name)[0]
            super(Loader, self).__init__(stream)

        def include(self, node):
            filename = os.path.join(self._root, self.construct_scalar(node))

            with open(filename, 'r') as f:
                return ordered_load(f, Loader)

    Loader.add_constructor('!include', Loader.include)

    with open(file, 'r') as stream:
        dictData = ordered_load(stream, Loader)

        return dictData


def ordered_load(stream, Loader=yaml.Loader, object_pairs_hook=OrderedDict):
    class OrderedLoader(Loader):
        pass

    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))
    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)
    return yaml.load(stream, OrderedLoader)


def ordered_dump(data, stream=None, Dumper=yaml.Dumper, **kwds):
    class OrderedDumper(Dumper):
        pass

    def _dict_representer(dumper, data):
        return dumper.represent_mapping(
            yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
            data.items())
    OrderedDumper.add_representer(OrderedDict, _dict_representer)
    return yaml.dump(data, stream, OrderedDumper, **kwds)
