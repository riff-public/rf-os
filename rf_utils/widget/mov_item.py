import os 
from Qt import QtWidgets, QtCore, QtGui
import time
from rf_utils import thread_pool


class Widget(QtWidgets.QWidget): 
    def __init__(self, parent=None): 
        super(Widget, self).__init__(parent=parent)
        self.threadpool = QtCore.QThreadPool()
        self.threadpool.setMaxThreadCount(1)
        self.imgPath = ''
        self.fps = 24
        self.dragPlay = False
        self.ui()

    def ui(self): 
        self.layout = QtWidgets.QVBoxLayout()
        self.previewWidget = QtWidgets.QPushButton()
        # self.previewWidget.setFlat(True)
        
        self.layout.addWidget(self.previewWidget)
        self.setLayout(self.layout)


    def setPath(self, path): 
    	self.imgPath = path
    	self.imgSequences = ['%s/%s' % (self.imgPath, a) for a in os.listdir(self.imgPath)]
    	self.setIcon()


    def setIcon(self): 
    	if self.imgSequences: 
    		icon = QtGui.QPixmap(self.imgSequences[0])
	    	self.previewWidget.setIcon(icon)
	    	self.previewWidget.setIconSize(QtCore.QSize(icon.width(), icon.height()))
	        self.previewWidget.setMaximumSize(icon.width(), icon.height())
	        self.previewWidget.setMinimumSize(icon.width(), icon.height())
	        self.setMaximumSize(icon.width(), icon.height())
	        self.setMinimumSize(icon.width(), icon.height())


    def enterEvent(self, event): 
    	worker = thread_pool.Worker(self.play)
    	self.doplay = True
    	self.threadpool.start(worker)

    def play(self): 
    	refreshRate = 1.0 / self.fps
    	i = 0 
    	enterPos = QtGui.QCursor.pos().x()
    	
    	while self.doplay: 
    		currentPos = float(QtGui.QCursor.pos().x())
    		if self.dragPlay: 
	    		index = int(((currentPos - enterPos)/100)*10)
	    		index = len(self.imgSequences) if index > len(self.imgSequences) else index 
	    	else: 
	    		index = i
    		path = self.imgSequences[int(index)]
    		# path = self.imgs[index % len(self.imgs)]
        	time.sleep(refreshRate)
        	self.previewWidget.setIcon(QtGui.QIcon(path))
        	i+=1

    def leaveEvent(self, event): 
    	self.doplay = False
    	self.setIcon()
    	print 'leave'
        	

    
class MainWindow(QtWidgets.QMainWindow): 
	def __init__(self, parent=None): 
		super(MainWindow, self).__init__(parent=parent)
		self.container = QtWidgets.QWidget()
		self.layout = QtWidgets.QVBoxLayout(self.container)
		self.label = QtWidgets.QLabel('Text Label')
		self.widget = Widget()
		self.widget.dragPlay = True
		self.widget.setPath('D:/seq')
		
		self.layout.addWidget(self.label)
		self.layout.addWidget(self.widget)
		self.setCentralWidget(self.container)
		

def show(): 
	win = MainWindow()
	win.show()