import os
import sys
import time
import re

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

from rf_utils import file_utils
from rf_utils import publish_info
from rf_utils import register_entity
from rf_utils.context import context_info
from rf_utils.widget import file_widget
reload(register_entity)

# from rf_app.asset_loader import manager
from rf_app.sg_manager import utils
from rf_app.sg_manager import dcc_hook

from rf_utils.sg import sg_process
from rftool.utils import pipeline_utils

from collections import OrderedDict

# import config
import rf_config as config

class ReferenceBucketUi(QtWidgets.QWidget):
    """ UI ReferenceBucketUi """
    currentIndexChanged = QtCore.Signal(str)

    def __init__(self, layout=None, parent=None):
        super(ReferenceBucketUi, self).__init__(parent=parent)
        self.allLayout = QtWidgets.QVBoxLayout() if not layout else layout

        self.refWidget = QtWidgets.QWidget()
        self.refLayout = QtWidgets.QVBoxLayout(self.refWidget)
        self.label = QtWidgets.QLabel('Reference List')
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        self.buttonLayout = QtWidgets.QHBoxLayout()
        self.addButton = QtWidgets.QPushButton('Add')
        self.removeButton = QtWidgets.QPushButton('Remove')
        self.clearButton = QtWidgets.QPushButton('Clear')

        self.buildButton = QtWidgets.QPushButton('Build All')

        self.layoutSplitter = QtWidgets.QSplitter()
        self.layoutSplitter.setOrientation(QtCore.Qt.Vertical)
        self.layoutSplitter.addWidget(self.refWidget)


        self.shotWidget = QtWidgets.QWidget(self.layoutSplitter)
        self.shotLayout = QtWidgets.QVBoxLayout(self.shotWidget)
        self.shotLabel = QtWidgets.QLabel('Shot Ref List')
        self.shotListWidget = QtWidgets.QListWidget()
        self.refreshButton = QtWidgets.QPushButton('Refresh')
        # self.shotListWidget.setEnabled(False)

        self.shotLayout.addWidget(self.shotLabel)
        self.shotLayout.addWidget(self.shotListWidget)
        self.shotLayout.addWidget(self.refreshButton)
                
        self.refLayout.addWidget(self.label)
        self.refLayout.addWidget(self.listWidget)
        self.refLayout.addLayout(self.buttonLayout)
        self.buttonLayout.addWidget(self.addButton)
        self.buttonLayout.addWidget(self.removeButton)
        self.buttonLayout.addWidget(self.clearButton)
        self.refLayout.addWidget(self.buildButton)
        # self.allLayout.addWidget(self.refWidget)
        self.allLayout.addWidget(self.layoutSplitter)

        self.layoutSplitter.setSizes([325, 300])
        self.layoutSplitter.setStretchFactor(0, 0)
        self.layoutSplitter.setStretchFactor(1, 1)

        # self.allLayout.addWidget(self.shotLabel)
        # self.allLayout.addWidget(self.shotListWidget)


        # self.allLayout.setStretch(0, 1)
        # self.allLayout.setStretch(1, 3)

        # self.allLayout.setSpacing(8)
        # self.listWidget.setSpacing(8)
        # self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)

        self.entity = None

        self.init_signals()
        self.shot_init()

        # self.listWidget.itemSelectionChanged.connect(self.status_selected)
        # self.limit = None
        # self.init_all_item()

    def init_signals(self):
        self.addButton.clicked.connect(self.add_item)
        self.removeButton.clicked.connect(self.remove_items)
        self.clearButton.clicked.connect(self.clear_items)
        self.refreshButton.clicked.connect(self.shot_init)
        self.buildButton.clicked.connect(self.build_ref)


    def add_item(self):
        # print self.entity
        listItems = self.listWidget.selectedItems()
        if listItems: 
            for item in listItems:
                label = self.listWidget.itemWidget(item)
                text = label.text()
                count = re.findall(r'\d+', text)[0]
                newText = text.replace(count , str(int(count)+1))
                label.setText(newText)
        else:
            entity = self.entity
            res = entity.projectInfo.asset.medium
            rigFile = utils.get_rig(entity, res=res, absPath=True)
            if not os.path.exists(rigFile):
                res = entity.projectInfo.asset.proxy
                rigFilePr = utils.get_rig(entity, res=res, absPath=True)
                if not os.path.exists(rigFilePr):
                    msg = QtWidgets.QMessageBox()
                    msg.setIcon(QtWidgets.QMessageBox.Warning)
                    msg.setText("This asset does not have Rig. Cannot reference.")
                    msg.exec_()
                    return

            for x in range(self.listWidget.count()):
                itemName = self.listWidget.item(x).data(QtCore.Qt.UserRole).name
                if itemName == entity.name:
                    label = self.listWidget.itemWidget(self.listWidget.item(x))
                    text = label.text()
                    count = re.findall(r'\d+', text)[0]
                    newText = text.replace(count , str(int(count)+1))
                    label.setText(newText)
                    return
            assetName = QtWidgets.QLabel('x1 -%s- %s' % (res,self.entity.name))
            newfont = QtGui.QFont("Times", 10, QtGui.QFont.Bold) 
            assetName.setFont(newfont)
            assetName.setFixedSize(500, 32)

            item = QtWidgets.QListWidgetItem(self.listWidget)
            self.listWidget.addItem(item)
            self.listWidget.setItemWidget(item,assetName)
            item.setData(QtCore.Qt.UserRole,entity)
            item.setSizeHint(assetName.sizeHint())

    def remove_items(self):
        listItems = self.listWidget.selectedItems()
        if listItems: 
            for item in listItems:
                label = self.listWidget.itemWidget(item)
                text = label.text()
                count = re.findall(r'\d+', text)[0]
                if int(count) == 1:
                    self.listWidget.takeItem(self.listWidget.row(item))
                else:
                    newText = text.replace(count , str(int(count)-1))
                    label.setText(newText)
        else:
            return

    def clear_items(self):
        self.listWidget.clear()

    def build_ref(self):
        for x in range(self.listWidget.count()):
            item = self.listWidget.item(x)
            entity = self.listWidget.item(x).data(QtCore.Qt.UserRole)

            label = self.listWidget.itemWidget(item)
            text = label.text()
            count = int(re.findall(r'\d+', text)[0])

            res = text.split('-')[1]
            rigFile = utils.get_rig(entity, res=res, absPath=True)
            rigFileRel = utils.get_rig(entity, res=res, absPath=False)
            if os.path.exists(rigFile):
                for x in range(count):
                    self.create_reference(rigFileRel, res, entity)
            else:
                print 'NOT FOUND'

        self.set_asset_list()
        self.clear_items()
        self.shot_init()

    def create_reference(self, path, res, entity=None):
        look = 'main'
        entity.context.update(res=res, look=look)
        # check for duplicated reference 
        allowReference = dcc_hook.duplicate_reference_warning(entity, path)
        defaultPose = dcc_hook.default_pose_choice(entity)
        
        if allowReference: 
            refs = dcc_hook.create_reference(entity, path, material=True, defaultPose=defaultPose)
            self.group_refs(entity, refs)
        else: 
            logger.info('Create reference %s cancelled' % path)

    def group_refs(self, entity, objs):
        """ grouping reference into group """
        assetType = entity.type
        group = ''
        if assetType == 'char':
            group = entity.projectInfo.asset.char_grp()
        if assetType == 'prop':
            group = entity.projectInfo.asset.prop_grp()
        if assetType == 'set':
            group = entity.projectInfo.asset.set_grp()
        if group:
            dcc_hook.group(group, objs)

    def set_asset_list(self):
        if config.isMaya:
            scene = context_info.ContextPathInfo()
            scene = scene.copy()
            if scene.name_code:
                from rf_app.asm import asm_lib
                shotName = scene.name_code
                scene.context.update(entityCode=shotName, entity='none')
                scene.update_scene_context()
                shotEntity = sg_process.get_shot_entity(scene.project, scene.name)
                assetDict = pipeline_utils.collect_assets(scene)
                setDict = asm_lib.collect_set_description(scene)
                setdressDict = asm_lib.collect_asset_description(scene)
                shotdressDict = asm_lib.collect_shotdress_description(scene)
                entities = combine_dict(assetDict, setDict)
                setDressEntities = combine_dict(shotdressDict, setdressDict)
                sg_process.update_asset_list(shotEntity.get('id'), entities, sg_field='assets')
                sg_process.update_asset_list(shotEntity.get('id'), setDressEntities, sg_field='sg_setdresses')

    def shot_init(self):
        if config.isMaya:
            from rf_app.asset_loader import manager
            self.shotListWidget.clear()
            assetManager = manager.Manager()
            assetData = assetManager.list_all_ref()
            for asset,data in assetData.items():
                if data.copies:
                    count = 0
                    for name,path in data.copies.items():
                        count += 1
                    item = QtWidgets.QListWidgetItem(self.shotListWidget)

                    assetName = QtWidgets.QLabel('x%s - %s' % (count, asset))

                    newfont = QtGui.QFont("Times", 10) 
                    assetName.setFont(newfont)
                    assetName.setFixedSize(500, 12)
                    self.shotListWidget.addItem(item)
                    self.shotListWidget.setItemWidget(item,assetName)
                    item.setSizeHint(assetName.sizeHint())
                    # item.setEnabled(False)

def combine_dict(dict1, dict2):
    sgEntities = []
    combDict = OrderedDict()
    for k, v in dict1.iteritems():
        if not k in combDict.keys():
            combDict[k] = v

    for k, v in dict2.iteritems():
        if not k in combDict.keys():
            combDict[k] = v

    for k, v in combDict.iteritems():
        if v.get('id'):
            entity = {'type': 'Asset', 'id': v.get('id'), 'code': v.get('name'), 'project': v.get('project')}
            if not entity in sgEntities:
                sgEntities.append(entity)
    return sgEntities

#     def __init__(self, parent=None):
#         super(ReferenceBucketUi, self).__init__(parent=parent)
#         self.allLayout = QtWidgets.QVBoxLayout()

#         self.layout = QtWidgets.QVBoxLayout()
#         self.checkBox = QtWidgets.QCheckBox('Hero')
#         self.listWidget = file_widget.FileListWidget()
#         self.layout.addWidget(self.checkBox)
#         self.layout.addWidget(self.listWidget)

#         self.allLayout.addLayout(self.layout)
#         self.allLayout.setSpacing(8)
#         self.allLayout.setContentsMargins(0, 0, 0, 0)
#         self.setLayout(self.allLayout)
#         self.checkBox.setVisible(False)



# class ViewPublishSpace(ReferenceBucketUi):
#     """ Logic Part for viewing publish """
#     def __init__(self, parent=None):
#         super(ViewPublishSpace, self).__init__(parent=parent)

#     def update_input(self, ContextPathInfo):
#         """ run here to activate ui """
#         self.entity = ContextPathInfo
#         self.populate_ui()

#     def populate_ui(self):
#         self.set_workspace()

#     def set_workspace(self):
#         files = self.list_workspace()
#         self.listWidget.clear()
#         self.listWidget.add_files(files)

#     def list_workspace(self):
#         """ list all workspace from publish _data file """
#         publish = publish_info.RegisterAsset(self.entity)
#         allData = publish.read_all_data()
#         files = []

#         for data in allData:
#             if self.entity.step in data.keys():
#                 stepData = data[self.entity.step]

#                 for process, processData in stepData.iteritems():
#                     if publish_info.Data.workspace in processData.keys():
#                         workspaceFiles = processData[publish_info.Data.workspace]

#                         for workspaceFile in workspaceFiles:
#                             if not workspaceFile in files:
#                                 files.append(workspaceFile)

#         return sorted(files)


# class ViewPublishSpace2(ViewPublishSpace):
#     """docstring for ViewPublishSpace2"""
#     def __init__(self, parent=None):
#         super(ViewPublishSpace2, self).__init__(parent=parent)
#         self.checkBox.setVisible(True)
#         self.checkBox.setChecked(True)
#         self.init_signals()

#     def init_signals(self):
#         self.checkBox.stateChanged.connect(self.set_workspace)

#     def list_workspace(self):
#         """ list all workspace from publish _data file """
#         data = cacheWorkspaceDict.get(str(self.entity.context))
#         publishedVersions = data.get('published') if data else None
#         heroFile = data.get('hero') if data else None

#         if not publishedVersions: 
#             regInfo = register_entity.Register(self.entity)
#             self.entity.context.update(look=self.entity.process)
#             workspaceDict = regInfo.version.workspaces(step=self.entity.step, res='md',process=self.entity.process, look=self.entity.look)

#             if workspaceDict:
#                 publishedVersions = workspaceDict['publishedFile']
#                 heroFile = workspaceDict['heroFile']
#                 cacheWorkspaceDict[str(self.entity.context)] = {'published': publishedVersions, 'hero': heroFile}

#         if self.checkBox.isChecked():
#             return heroFile
#         return publishedVersions

    # def clear(self): 
    #     self.listWidget.clear()



