# -*- coding: utf-8 -*-
import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class Color: 
    bgred = 'background-color: rgb(200, 0, 0);'

class NoteWidget(QtWidgets.QWidget):
    """docstring for NoteWidget"""
    def __init__(self, sg=None, taskEntity=None, prevSelection=None, parent=None):
        super(NoteWidget, self).__init__(parent=None)
        self.sg = sg 
        self.taskEntity = taskEntity
        self.prevSelection =prevSelection

        self.allLayout = QtWidgets.QVBoxLayout()
        
        # widgets 
        self.label = QtWidgets.QLabel('Check for completed notes')
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.listWidget)

        self.setLayout(self.allLayout)


    def show_notes(self): 
        notes = self.find_notes(self.taskEntity.get('project'), self.taskEntity, 'asset')
        self.listWidget.clear()
        prevSelectionIds = [a['id'] for a in self.prevSelection] if self.prevSelection else []

        for note in notes: 
            item = QtWidgets.QListWidgetItem(self.listWidget)
            item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
            item.setText('%s - %s' % (note['sg_status_list'], note['content'].decode('utf8')))
            item.setData(QtCore.Qt.UserRole, note)
            item.setCheckState(QtCore.Qt.Checked) if note['id'] in prevSelectionIds else item.setCheckState(QtCore.Qt.Unchecked)

            if note['sg_status_list'] == 'cmpt': 
                item.setFlags(not QtCore.Qt.ItemIsSelectable)


    def selected_notes(self): 
        items = self.listWidget.selectedItems()
        if items: 
            datas = [a.data(QtCore.Qt.UserRole) for a in items]
            return datas 

    def checked_notes(self): 
        allItems = [self.listWidget.item(a) for a in range(self.listWidget.count())]
        checkedItems = [a.data(QtCore.Qt.UserRole) for a in allItems if a.checkState() == QtCore.Qt.Checked]
        return checkedItems


    def find_notes(self, projectEntity, taskEntity, noteType='asset'):
            project_filtered = {
                "type": projectEntity['type'],
                "id": projectEntity['id'],
                "name": projectEntity['name']
            }
            note_links_filtered = [
                {
                    "type": taskEntity['entity']['type'],
                    "id": taskEntity['entity']['id'],
                    "name": taskEntity['entity']['name']
                }
            ]
            if noteType == 'task':
                tasks_filtered = [
                    {
                        "type": taskEntity['type'],
                        "id": taskEntity['id'],
                        "content": taskEntity['content']
                    }
                ]
                notes_filtered = [
                    ["project", "is", project_filtered],
                    ["note_links", "is", note_links_filtered],
                    ["tasks", "is", tasks_filtered]
                ]
                notes = self.sg.find('Note', notes_filtered, ['subject', 'content', 'attachments', 'sg_status_list'])
            elif noteType == 'asset':
                notes_filtered = [
                    ["project", "is", project_filtered],
                    ["note_links", "is", note_links_filtered]
                ]
                notes = self.sg.find('Note', notes_filtered, ['subject', 'content', 'attachments', 'sg_status_list'])
            else:
                notes = []
            return notes