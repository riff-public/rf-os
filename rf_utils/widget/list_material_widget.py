from Qt import QtGui, QtWidgets, QtCore


class ListMaterialWidget(QtWidgets.QWidget):
    """docstring for ListMaterialWidget"""
    def __init__(self, parent=None):
        super(ListMaterialWidget, self).__init__(parent)
        self.layout = QtWidgets.QVBoxLayout()

        self.material_name = QtWidgets.QLabel()
        self.layout.addWidget(self.material_name)

        self.obj_assign = QtWidgets.QLabel()
        self.layout.addWidget(self.obj_assign)

        self.allQHBoxLayout = QtWidgets.QHBoxLayout()
        self.iconQLabel = QtWidgets.QLabel()
        self.allQHBoxLayout.addWidget(self.iconQLabel, 0)

        self.allQHBoxLayout.addLayout(self.layout, 1)
        self.setLayout(self.allQHBoxLayout)

        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)

        # setStyleSheet
        self.material_name.setFont(QtGui.QFont("Arial", 12, QtGui.QFont.Bold))
        self.obj_assign.setFont(QtGui.QFont("Arial", 8, QtGui.QFont.Bold))

    def set_material_name_color(self, colors):
        self.material_name.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_obj_assign_color(self, colors):
        self.obj_assign.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text_material_name(self, text):
        self.material_name.setText(text)

    def set_text_obj_assign(self, text):
        self.obj_assign.setText(text)

    def set_icon(self, imagePath):
        self.iconQLabel.setPixmap(QtGui.QPixmap(imagePath))


class MaterialListWidget(QtWidgets.QListWidget):
    def __init__(self, parent=None):
        super(MaterialListWidget, self).__init__(parent)
        self.setDragEnabled(True)
        self.setDragDropMode(QtWidgets.QAbstractItemView.DragOnly)
        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.setIconSize(QtCore.QSize(72, 72))

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            super(MaterialListWidget, self).dragEnterEvent(event)

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            super(MaterialListWidget, self).dragMoveEvent(event)

    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            links = []
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.emit(QtCore.SIGNAL("dropped"), links)
            print "MaterialListWidget", links
        else:
            event.setDropAction(QtCore.Qt.MoveAction)
            super(MaterialListWidget, self).dropEvent(event)
