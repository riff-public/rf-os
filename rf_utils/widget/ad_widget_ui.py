# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ad_widget_ui.ui'
#
# Created: Sun Jun 17 17:18:23 2018
#      by: PyQt4 UI code generator 4.9.5
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(276, 482)
        self.verticalLayout_3 = QtGui.QVBoxLayout(Form)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.header_layout = QtGui.QGridLayout()
        self.header_layout.setObjectName(_fromUtf8("header_layout"))
        self.step_comboBox = QtGui.QComboBox(Form)
        self.step_comboBox.setObjectName(_fromUtf8("step_comboBox"))
        self.header_layout.addWidget(self.step_comboBox, 0, 1, 1, 1)
        self.label_2 = QtGui.QLabel(Form)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.header_layout.addWidget(self.label_2, 1, 0, 1, 1)
        self.label_3 = QtGui.QLabel(Form)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.header_layout.addWidget(self.label_3, 1, 2, 1, 1)
        self.res_comboBox = QtGui.QComboBox(Form)
        self.res_comboBox.setObjectName(_fromUtf8("res_comboBox"))
        self.header_layout.addWidget(self.res_comboBox, 1, 3, 1, 1)
        self.label = QtGui.QLabel(Form)
        self.label.setObjectName(_fromUtf8("label"))
        self.header_layout.addWidget(self.label, 0, 0, 1, 1)
        self.process_comboBox = QtGui.QComboBox(Form)
        self.process_comboBox.setObjectName(_fromUtf8("process_comboBox"))
        self.header_layout.addWidget(self.process_comboBox, 1, 1, 1, 1)
        self.header_layout.setColumnStretch(0, 1)
        self.header_layout.setColumnStretch(1, 3)
        self.header_layout.setColumnStretch(2, 1)
        self.header_layout.setColumnStretch(3, 3)
        self.verticalLayout_3.addLayout(self.header_layout)
        self.line_2 = QtGui.QFrame(Form)
        self.line_2.setFrameShape(QtGui.QFrame.HLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.verticalLayout_3.addWidget(self.line_2)
        self.option_layout = QtGui.QHBoxLayout()
        self.option_layout.setObjectName(_fromUtf8("option_layout"))
        self.file_radioButton = QtGui.QRadioButton(Form)
        self.file_radioButton.setObjectName(_fromUtf8("file_radioButton"))
        self.option_layout.addWidget(self.file_radioButton)
        self.media_adioButton = QtGui.QRadioButton(Form)
        self.media_adioButton.setObjectName(_fromUtf8("media_adioButton"))
        self.option_layout.addWidget(self.media_adioButton)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.option_layout.addItem(spacerItem)
        self.verticalLayout_3.addLayout(self.option_layout)
        self.line = QtGui.QFrame(Form)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout_3.addWidget(self.line)
        self.display_layout = QtGui.QVBoxLayout()
        self.display_layout.setObjectName(_fromUtf8("display_layout"))
        self.file_listWidget = QtGui.QListWidget(Form)
        self.file_listWidget.setObjectName(_fromUtf8("file_listWidget"))
        self.display_layout.addWidget(self.file_listWidget)
        self.verticalLayout_3.addLayout(self.display_layout)
        self.info_frame = QtGui.QFrame(Form)
        self.info_frame.setFrameShape(QtGui.QFrame.Box)
        self.info_frame.setFrameShadow(QtGui.QFrame.Sunken)
        self.info_frame.setObjectName(_fromUtf8("info_frame"))
        self.frame_layout = QtGui.QVBoxLayout(self.info_frame)
        self.frame_layout.setObjectName(_fromUtf8("frame_layout"))
        self.description_label = QtGui.QLabel(self.info_frame)
        self.description_label.setObjectName(_fromUtf8("description_label"))
        self.frame_layout.addWidget(self.description_label)
        self.verticalLayout_3.addWidget(self.info_frame)
        self.import_pushButton = QtGui.QPushButton(Form)
        self.import_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.import_pushButton.setObjectName(_fromUtf8("import_pushButton"))
        self.verticalLayout_3.addWidget(self.import_pushButton)
        self.reference_pushButton = QtGui.QPushButton(Form)
        self.reference_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.reference_pushButton.setObjectName(_fromUtf8("reference_pushButton"))
        self.verticalLayout_3.addWidget(self.reference_pushButton)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Form", "Process:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("Form", "res: ", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Form", "Type: ", None, QtGui.QApplication.UnicodeUTF8))
        self.file_radioButton.setText(QtGui.QApplication.translate("Form", "File", None, QtGui.QApplication.UnicodeUTF8))
        self.media_adioButton.setText(QtGui.QApplication.translate("Form", "Media", None, QtGui.QApplication.UnicodeUTF8))
        self.description_label.setText(QtGui.QApplication.translate("Form", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
        self.import_pushButton.setText(QtGui.QApplication.translate("Form", "Import", None, QtGui.QApplication.UnicodeUTF8))
        self.reference_pushButton.setText(QtGui.QApplication.translate("Form", "Reference", None, QtGui.QApplication.UnicodeUTF8))

