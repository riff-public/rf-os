import os
import sys
import traceback
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import icon


class EntityListWidget(QtWidgets.QWidget):
    selected = QtCore.Signal(dict)
    selectionChanged = QtCore.Signal(list)
    def __init__(self, parent=None) :
        super(EntityListWidget, self).__init__(parent)

        # data
        self.asset = 'Asset'
        self.scene = 'Shot'

        self.readAttr = 'code'
        self.noItem = 'No Item'
        self.iconNoPreview = icon.nopreview
        self.allLayout = QtWidgets.QVBoxLayout()
        self.listWidget = QtWidgets.QListWidget()
        self.allLayout.addWidget(self.listWidget)
        self.setLayout(self.allLayout)

        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.set_icon_size([128, 128])

        self.init_signals()

    def init_signals(self):
        self.listWidget.clicked.connect(self.item_selected)
        self.listWidget.itemSelectionChanged.connect(self.item_selection_changed)

    def set_list_mode(self):
        self.listWidget.setViewMode(QtWidgets.QListView.ListMode)
        self.listWidget.setMovement(QtWidgets.QListView.Static)
        self.refresh_list(True)

    def set_icon_mode(self):
        self.listWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.listWidget.setMovement(QtWidgets.QListView.Static)
        self.refresh_list(False)

    def set_text_mode(self): 
        self.listWidget.setViewMode(QtWidgets.QListView.ListMode)
        self.listWidget.setMovement(QtWidgets.QListView.Static)
        self.refresh_list(True, 'text')

    def refresh_list(self, widget, mode=''):
        sgEntities = [self.listWidget.item(a).data(QtCore.Qt.UserRole) for a in range(self.listWidget.count())]
        self.listWidget.clear()
        if mode == 'text': 
            # self.add_texts(sgEntities)
            self.add_texts_extra(sgEntities)
        else: 
            self.add_items(sgEntities, widget=widget)

    def add_items(self, sgEntities, widget=False):
        for i, entity in enumerate(sgEntities):
            self.add_item(entity, widget=widget)

    def add_texts(self, sgEntities, widget=False):
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(icon.dir),QtGui.QIcon.Normal,QtGui.QIcon.Off)
        # font = QtGui.QFont("Arial", 8)

        for i, sgEntity in enumerate(sgEntities):
            item = QtWidgets.QListWidgetItem(self.listWidget)
            item.setData(QtCore.Qt.UserRole, sgEntity)
            item.setText(sgEntity['code'])
            item.setIcon(iconWidget)
            # item.setFont(font)

    def add_texts_extra(self, sgEntities): 
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(icon.dir),QtGui.QIcon.Normal,QtGui.QIcon.Off)
        
        for sgEntity in sgEntities: 
            # normal widget mode
            item = QtWidgets.QListWidgetItem(self.listWidget)
            item.setData(QtCore.Qt.UserRole, sgEntity)
            iconPath = icon.dir
            sgEntity.update({'iconPath': iconPath})
            entityType = sgEntity.get('type')

            if entityType == self.asset: 
                itemWidget = ListInfoItem()
                itemWidget.set_name(sgEntity[self.readAttr])
                itemWidget.set_icon(iconPath)

                eps = [a['code'] for a in sgEntity.get('sg_scenes')]
                if eps: 
                    itemWidget.set_extra1(', '.join(eps))

                item.setSizeHint(itemWidget.sizeHint())
                self.listWidget.setItemWidget(item, itemWidget)
            else: 
                item.setText(sgEntity['code'])
                item.setIcon(iconWidget)


    def add_item(self, sgEntity, iconPath='', showIcon=True, widget=False):
        # normal widget mode
        item = QtWidgets.QListWidgetItem(self.listWidget)
        item.setData(QtCore.Qt.UserRole, sgEntity)
        iconPath = self.iconNoPreview if not iconPath else iconPath
        sgEntity.update({'iconPath': iconPath})
        entityType = sgEntity.get('type')

        if not widget:
            item.setText(sgEntity[self.readAttr])

            if showIcon:
                iconWidget = QtGui.QIcon()
                iconWidget.addPixmap(QtGui.QPixmap(iconPath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
                item.setIcon(iconWidget)

        else:
            itemWidget = EntityItem()
            if entityType == self.asset: 
                itemWidget.set_text1(sgEntity.get('sg_asset_type'))
                itemWidget.set_text2(sgEntity.get('sg_subtype'))
            
            if entityType == self.scene: 
                itemWidget.set_text1(sgEntity.get('sg_episode.Scene.code'))
                itemWidget.set_text2(sgEntity.get('sg_sequence_code'))
                itemWidget.set_text3(sgEntity.get('sg_shortcode'))
            
            itemWidget.set_name(sgEntity[self.readAttr])
            itemWidget.set_icon(iconPath, size=[128, 128])
            itemWidget.set_text1_color([120, 120, 120])
            itemWidget.set_text2_color([120, 120, 120])
            itemWidget.set_text3_color([120, 120, 120])
            itemWidget.set_dir(True)

            item.setSizeHint(itemWidget.sizeHint())
            self.listWidget.setItemWidget(item, itemWidget)

        return item

    def display_empty_item(self): 
        self.listWidget.clear()
        item = QtWidgets.QListWidgetItem(self.listWidget)
        item.setData(QtCore.Qt.UserRole, self.noItem)
        item.setText(self.noItem)

    def update_icon(self, item, path, widget=False):
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(path),QtGui.QIcon.Normal,QtGui.QIcon.Off)
        data = item.data(QtCore.Qt.UserRole)
        data.update({'iconPath': path})
        item.setData(QtCore.Qt.UserRole, data)
        if not widget:
            item.setIcon(iconWidget)
        else:
            itemWidget = self.listWidget.itemWidget(item)
            itemWidget.set_icon(path, size=[128, 128])

    def clear(self):
        self.listWidget.clear()

    def set_icon_size(self, values):
        self.listWidget.setIconSize(QtCore.QSize(values[0], values[1]))

    def item_selected(self):
        item = self.listWidget.currentItem()
        if item:
            data = item.data(QtCore.Qt.UserRole)
            self.selected.emit(data)

    def item_selection_changed(self):
        items = self.listWidget.selectedItems()
        data = []
        if items:
            for item in items:
                item_data = item.data(QtCore.Qt.UserRole)
                data.append(item_data)
        self.selectionChanged.emit(data)

    def set_current(self, displayText, signal=True):
        # find matched item
        selItem = [self.listWidget.item(a) for a in range(self.listWidget.count()) if self.listWidget.item(a).data(QtCore.Qt.UserRole).get(self.readAttr) == displayText]
        self.listWidget.blockSignals(True) if signal == False else None
        self.listWidget.setCurrentItem(selItem[0]) if selItem else None
        self.listWidget.blockSignals(False) if signal == False else None
        self.selected.emit(selItem[0].data(QtCore.Qt.UserRole)) if selItem else None

    def set_current_index(self, index, signal=True):
        self.listWidget.blockSignals(True) if signal == False else None
        item = self.listWidget.item(index)
        if item:
            self.listWidget.setCurrentItem(item)
        self.listWidget.blockSignals(False) if signal == False else None
        self.selected.emit(item.data(QtCore.Qt.UserRole)) if item else None

    def current_item(self):
        return self.listWidget.currentItem().data(QtCore.Qt.UserRole) if self.listWidget.currentItem() else None

    def selected_items(self):
        return [a.data(QtCore.Qt.UserRole) for a in self.listWidget.selectedItems()] if self.listWidget.selectedItems() else []

    def selected_texts(self):
        return [str(a.text()) for a in self.listWidget.selectedItems()] if self.listWidget.selectedItems() else []

    def current_text(self):
        return str(self.listWidget.currentItem().text()) if self.listWidget.currentItem() else None

    def all_items(self):
        return [self.listWidget.item(a) for a in range(self.listWidget.count())]

    def all_data(self):
        return [a.data(QtCore.Qt.UserRole) for a in self.all_items()]

    def all_texts(self):
        return [str(a.text()) for a in self.all_items()]


class TagListWidget(EntityListWidget):
    """docstring for TagListWidget"""
    def __init__(self, parent=None):
        super(TagListWidget, self).__init__(parent=parent)
        self.readAttr = 'name'
        self.iconNoPreview = icon.nopreview


class EntityItem(QtWidgets.QWidget):
    """docstring for EntityItem"""
    def __init__(self, view='icon', parent=None):
        super(EntityItem, self).__init__(parent=parent)

        # widgets
        self.icon = QtWidgets.QLabel()
        self.dir = QtWidgets.QLabel()
        self.name = QtWidgets.QLabel()
        self.entityGrp = QtWidgets.QLabel()
        self.entityParent = QtWidgets.QLabel()
        self.entityCode = QtWidgets.QLabel()

        # icon view
        self.icon_view_layout() if view == 'icon' else self.list_view_layout() if view == 'list' else 'icon'

    def icon_view_layout(self):
        self.layout = QtWidgets.QHBoxLayout()
        self.subLayout = QtWidgets.QVBoxLayout()
        self.layout.addWidget(self.icon)
        self.infoLayout = QtWidgets.QHBoxLayout()
        self.infoLayout.addWidget(self.dir)
        self.infoLayout.addWidget(self.name)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.infoLayout.addItem(spacerItem)

        self.infoLayout.setStretch(0, 0)
        self.infoLayout.setStretch(1, 0)
        self.infoLayout.setStretch(2, 1)
        self.infoLayout.setSpacing(0)
        self.layout.setContentsMargins(2, 2, 2, 2)

        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.subLayout.addLayout(self.infoLayout)
        self.subLayout.addWidget(self.entityGrp)
        self.subLayout.addWidget(self.entityParent)
        self.subLayout.addWidget(self.entityCode)
        self.subLayout.addItem(spacerItem2)
        self.subLayout.setSpacing(2)
        self.subLayout.setContentsMargins(2, 2, 2, 2)

        self.layout.addLayout(self.subLayout)
        self.setLayout(self.layout)

        self.name.setFont(QtGui.QFont("Arial", 10, QtGui.QFont.Bold))
        self.entityGrp.setFont(QtGui.QFont("Arial", 9))
        self.entityParent.setFont(QtGui.QFont("Arial", 9))
        # self.textFrameRange.setFont(QtGui.QFont("Arial", 8, QtGui.QFont.Bold))
        # self.textstatus.setFont(QtGui.QFont("Arial", 8, QtGui.QFont.Bold))

    def set_icon(self, path, size=[100, 100]):
        self.icon.setPixmap(QtGui.QPixmap(path).scaled(size[0], size[1], QtCore.Qt.KeepAspectRatio))

    def set_dir(self, state):
        iconPath = icon.dir if state else icon.nodir
        # self.dir.setPixmap(QtGui.QPixmap(iconPath).scaled(16, 16, QtCore.Qt.KeepAspectRatio))

    def set_name(self, text):
        self.name.setText(text)

    def set_text1(self, text):
        self.entityGrp.setText(text)

    def set_text2(self, text):
        self.entityParent.setText(text)

    def set_text3(self, text):
        self.entityCode.setText(text)

    def set_text1_color(self, colors): 
        self.entityGrp.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text2_color(self, colors): 
        self.entityParent.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text3_color(self, colors): 
        self.entityCode.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))


class EntityTreeWidget(QtWidgets.QWidget):
    selectionChanged = QtCore.Signal(object)
    def __init__(self, parent=None) :
        super(EntityTreeWidget, self).__init__(parent)

        # constants
        self.asset = 'Asset'
        self.scene = 'Shot'
        self.readAttr = 'code'

        # icons
        self.iconNoPreview = icon.nopreview

        # layout
        self.allLayout = QtWidgets.QVBoxLayout()
        self.setLayout(self.allLayout)
        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(0, 0, 0, 0)

        # tree widget
        self.treeWidget = QtWidgets.QTreeWidget()
        self.treeWidget.setSortingEnabled(False)
        self.treeWidget.setHeaderHidden(True)
        self.allLayout.addWidget(self.treeWidget)

        self.init_signals()

    def init_signals(self):
        self.treeWidget.currentItemChanged.connect(self.current_item_changed)

    def add_items(self, sgEntities):
        for i, entity in enumerate(sgEntities):
            item = self.add_item(entity)

    def add_item(self, entity, parent=None, icon=None):
        if not parent:
            parent = self.treeWidget
            
        item = QtWidgets.QTreeWidgetItem(parent)
        if icon:
            # icon
            item.setIcon(0, icon)
        # text
        item.setText(0, entity[self.readAttr])
         # data
        item.setData(QtCore.Qt.UserRole, 0, entity)
        return item

    def clear(self):
        self.treeWidget.blockSignals(True)
        self.treeWidget.clear()
        self.treeWidget.blockSignals(False)
        self.selectionChanged.emit(None)

    def current_item_changed(self, current, previous):
        item_data = None
        if current:
            item_data = current.data(QtCore.Qt.UserRole, 0)
        self.selectionChanged.emit(item_data)

    def set_current(self, displayText, signal=True):
        # find matched item
        rootItem = self.treeWidget.invisibleRootItem()
        selItem = [rootItem.child(a) for a in range(self.treeWidget.topLevelItemCount()) if rootItem.child(a).data(QtCore.Qt.UserRole, 0).get(self.readAttr)==displayText]
        self.treeWidget.blockSignals(True) if signal == False else None
        self.treeWidget.setCurrentItem(selItem[0]) if selItem else None
        self.treeWidget.blockSignals(False) if signal == False else None
        self.selectionChanged.emit(selItem[0].data(QtCore.Qt.UserRole, 0)) if selItem else None

    def current_item(self):
        return self.treeWidget.currentItem().data(QtCore.Qt.UserRole, 0) if self.treeWidget.currentItem() else None

    def selected_items(self):
        return [a.data(QtCore.Qt.UserRole, 0) for a in self.treeWidget.selectedItems()] if self.treeWidget.selectedItems() else None

    def selected_texts(self):
        return [str(a.text(0)) for a in self.treeWidget.selectedItems()] if self.treeWidget.selectedItems() else None

    def current_text(self):
        return str(self.treeWidget.currentItem().text(0)) if self.treeWidget.currentItem() else None

    def all_items(self):
        rootItem = self.treeWidget.invisibleRootItem()
        return [rootItem.child(a) for a in range(self.treeWidget.topLevelItemCount())]

    def all_data(self):
        return [a.data(QtCore.Qt.UserRole, 0) for a in self.all_items()]

    def all_texts(self):
        return [str(a.text(0)) for a in self.all_items()]


class ListInfoItem(QtWidgets.QWidget):
    """docstring for ListInfoItem"""
    def __init__(self, parent=None):
        super(ListInfoItem, self).__init__(parent=parent)

        # widgets
        self.layout = QtWidgets.QHBoxLayout()
        self.icon = QtWidgets.QLabel()
        self.name = QtWidgets.QLabel()
        self.extra1 = QtWidgets.QLabel()
        self.extra2 = QtWidgets.QLabel()
        self.layout.addWidget(self.icon)
        self.layout.addWidget(self.name)
        self.layout.addWidget(self.extra1)
        self.layout.addWidget(self.extra2)
        # self.spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        # self.layout.addItem(self.spacerItem)
        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 2)
        self.layout.setStretch(2, 1)
        self.layout.setStretch(3, 1)
        self.extra1.setStyleSheet('color: rgb(%s, %s, %s)' % (100, 100, 100))

    def set_icon(self, path, size=[16, 16]):
        self.icon.setPixmap(QtGui.QPixmap(path).scaled(size[0], size[1], QtCore.Qt.KeepAspectRatio))

    def set_name(self, text): 
        self.name.setText(text)

    def set_extra1(self, text): 
        self.extra1.setText(text)

    def set_extra2(self, text): 
        self.extra2.setText(text)
