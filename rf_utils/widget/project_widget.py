import os
import sys
import traceback
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
try:
    module_dir = sys._MEIPASS
except Exception:
    module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget import file_comboBox

class Cache: 
    cacheDir = '{}/widget_cache'.format(os.path.expanduser('~'))
    cacheFile = '{}/{}.yml'.format(cacheDir, __name__)

class ProjectComboBox(QtWidgets.QWidget):
    currentIndexChanged = QtCore.Signal(str)
    def __init__(self, projectInfo, firstItem=None, parent=None) :
        super(ProjectComboBox, self).__init__()

        self.projectInfo = projectInfo
        self.firstItem = firstItem

        # layout
        self.allLayout = QtWidgets.QVBoxLayout()

        # display listWidget
        self.projectComboBox = file_comboBox.FileComboBox()
        self.projectComboBox.set_label('Project')
        self.comboBox = self.projectComboBox.comboBox

        # add layout
        self.allLayout.addWidget(self.projectComboBox)
        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.setLayout(self.allLayout)

        self.init_signals()


    def init_signals(self):
        self.comboBox.currentIndexChanged.connect(self.emit_signal)

    def list_project(self):
        self.comboBox.clear()
        projects = self.projectInfo.list_all()
        self.comboBox.blockSignals(True)
        i = 0
        currentRow = 0
        # if self.firstItem:
        #     self.comboBox.addItem(self.firstItem)
        #     i = 1

        for row, project in enumerate(projects):
            path = ('/').join([project.rootProject, project.name()])
            self.comboBox.addItem(project.name())
            self.comboBox.setItemData(row+i, path, QtCore.Qt.UserRole)
            if project.name() == self.firstItem:
                currentRow = row

        lastSave = read_last_save()
        currentRow = projects.index(lastSave) if lastSave in projects else 0 
        self.comboBox.setCurrentIndex(currentRow)

        self.comboBox.blockSignals(False)
        self.emit_signal()

    def reset(self):
        """ reset list to first choice """
        self.comboBox.setCurrentIndex(0)


    def emit_signal(self):
        path = self.comboBox.itemData(self.comboBox.currentIndex(), QtCore.Qt.UserRole)
        self.currentIndexChanged.emit(path)



class SGProjectComboBox(QtWidgets.QWidget):
    """docstring for SGProjectComboBox"""
    projectChanged = QtCore.Signal(dict)
    cleared = QtCore.Signal(bool)

    def __init__(self, sg, defaultProject=None, projectActive=True, layout=None, fetchNow=True, thread=False, timelogProject=True, parent=None):
        super(SGProjectComboBox, self).__init__(parent=parent)
        self.sg = sg
        self.thread = thread
        self.defaultProject = defaultProject
        self.projectActive = projectActive
        self.showTimelogProject = timelogProject

        # layout
        self.allLayout = layout if layout else QtWidgets.QVBoxLayout()

        # display listWidget
        self.projectComboBox = QtWidgets.QComboBox()
        self.label = QtWidgets.QLabel('Project')

        # add layout
        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.projectComboBox)
        self.allLayout.setSpacing(5)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)

        self.init_signals()
        if fetchNow: 
            self.list_project()


    def init_signals(self):
        self.projectComboBox.currentIndexChanged.connect(self.project_changed)

    def fetch_project(self): 
        """ get project from shotgun """ 
        if self.thread: 
            self.projectThread = FetchProject(self.sg)
            self.projectThread.finished.connect(self.list_project)
            self.projectThread.start()
        else: 
            projects = sorted_project(find_projects(self.sg))
            self.list_project(projects)

    def list_project(self, projects=None):
        if not projects: 
            projects = sorted_project(find_projects(self.sg))

        if not self.showTimelogProject: 
            filterProjects = list()
            for project in projects: 
                if not project.get('sg_timelog_only'): 
                    filterProjects.append(project)

            projects = filterProjects

        self.projectComboBox.clear()
        currentIndex = 0
        indexOffset = 0
        # if not self.projectActive:
        #     self.projectComboBox.addItem('-- Select Project --')
        #     indexOffset = 1 # offset item index

        for index, project in enumerate(projects):
            self.projectComboBox.addItem(project['name'])
            self.projectComboBox.setItemData(index+indexOffset, project, QtCore.Qt.UserRole)

            if self.defaultProject == project['name']:
                currentIndex = index

        # last save 
        lastSave = read_last_save()
        projectStrs = [project['name'] for index, project in enumerate(projects)]
        currentRow = projectStrs.index(lastSave) if lastSave in projectStrs else 0 

        # use default project first. 
        self.projectComboBox.setCurrentIndex(currentRow)
        if self.defaultProject: 
            self.projectComboBox.setCurrentIndex(currentIndex)
        
        self.emit_project_changed()

    def current_item(self):
        return self.projectComboBox.itemData(self.projectComboBox.currentIndex(), QtCore.Qt.UserRole)

    def emit_project_changed(self): 
        self.projectChanged.emit(self.current_item())

    def current_project(self):
        return str(self.projectComboBox.currentText())

    def project_name(self): 
        return str(self.projectComboBox.currentText())

    def project_entity(self): 
        return self.projectComboBox.itemData(self.projectComboBox.currentIndex(), QtCore.Qt.UserRole)

    def project_changed(self):
        """ when selection changed, emit project entity """
        data = self.current_item()
        if data:
            save_selection(str(self.projectComboBox.currentText()))
            self.projectChanged.emit(data)
        else:
            self.cleared.emit(True)

    def set_current_item(self, project, signal=True):
        projects = [self.projectComboBox.itemText(a) for a in range(self.projectComboBox.count())]
        index = projects.index(project) if project in projects else 0
        self.projectComboBox.blockSignals(True) if signal == False else None
        self.projectComboBox.setCurrentIndex(index)
        self.projectComboBox.blockSignals(False) if signal == False else None

    def set_visible(self, state):
        self.projectComboBox.setVisible(state)
        self.label.setVisible(state)

    def set_label_color(self, colors): 
        self.label.setStyleSheet('color: rgb({}, {}, {})'.format(colors[0], colors[1], colors[2]))


class SGProjectComboBox2(SGProjectComboBox):
    """docstring for SGProjectComboBox2"""
    def __init__(self, sg, defaultProject=None, projectActive=True, layout=None, fetchNow=True, thread=False, parent=None): 
        super(SGProjectComboBox2, self).__init__(sg, defaultProject=defaultProject, projectActive=projectActive, layout=layout, fetchNow=fetchNow, thread=thread, parent=parent)
        self.projectComboBox.insertItem(0, 'All')
        self.projectComboBox.setItemData(0, {'name': 'All'}, QtCore.Qt.UserRole)
        

class SGProjectComboBoxPlaceHolder(SGProjectComboBox):
    """docstring for SGProjectComboBoxPlaceHolder"""
    def __init__(self, parent=None):
        layout = QtWidgets.QHBoxLayout()
        super(SGProjectComboBoxPlaceHolder, self).__init__(sg=None, fetchNow=False, layout=layout, timelogProject=False, parent=parent)
        self.allLayout.setStretch(0, 0)
        self.allLayout.setStretch(1, 1)
        

class FetchProject(QtCore.QThread):
    finished = QtCore.Signal(list)

    def __init__(self, sg):
        QtCore.QThread.__init__(self)
        self.sg = sg 
     
    def run(self): 
        logger.debug('Fetching project ...')
        projects = find_projects(self.sg)
        self.finished.emit(projects)


def find_projects(sg):
    filters = [['sg_status', 'is', 'Active']]
    fields = ['name', 'id', 'sg_project_code', 'sg_timelog_only']
    return sg.find('Project', filters, fields)

def list_file(path, ext=[]):
    result = sorted([d for d in os.listdir(path) if os.path.isfile(os.path.join(path, d))]) if os.path.exists(path) else []
    return [a for a in result if os.path.splitext(a) in ext] if ext else result


def list_folder(path, notStartwith=[]):
    result = sorted([d for d in os.listdir(path) if os.path.isdir(os.path.join(path,d))]) if os.path.exists(path) else []
    return [a for a in result if all(not b in a for b in notStartwith)] if notStartwith else result


def save_selection(project): 
    from rf_utils import file_utils
    data = {'project': project}
    if not os.path.exists(Cache.cacheDir): 
        os.makedirs(Cache.cacheDir)
        
    file_utils.ymlDumper(Cache.cacheFile, data)

def read_last_save(): 
    from rf_utils import file_utils
    if os.path.exists(Cache.cacheFile): 
        try:
            data = file_utils.ymlLoader(Cache.cacheFile) or dict()
            return data.get('project')
        except Exception:
            pass
    return ''

def sorted_project(projectEntities): 
    """ sorted list """ 
    projects = []
    nameSorted = sorted([a.get('name') for a in projectEntities])

    for project in nameSorted: 
        for projectEntity in projectEntities: 
            if project == projectEntity.get('name'): 
                projects.append(projectEntity)

    return projects 
