#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils.widget import hashtag_widget
from rf_utils.widget import project_widget
from rf_utils.widget import media_widget
from rf_utils.sg import sg_process
from rf_utils import project_info
from rf_utils import user_info
from rf_utils.sg import sg_note
from rf_utils.widget.attachment import attachment_widget
from rf_utils import thread_pool


module_path = sys.modules[__name__].__file__
module_dir = os.path.dirname(module_path)
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

sg = sg_process.sg

class Config: 
    step_task = {
                    'anim': ['anim'], 
                    'model': ['model'], 
                    'rig': ['rig']
                }


class NoteFormUi(QtWidgets.QWidget):

    def __init__(self, user=None, threadpool=None, parent=None):
        super(NoteFormUi, self).__init__(parent)
        self.threadpool = threadpool
        if not self.threadpool: 
            self.threadpool = QtCore.QThreadPool()
            self.threadpool.setMaxThreadCount(1)

        self.project_info = project_info.ProjectInfo('default')
        self.user = user
        if not user: 
            self.user = user_info.User().sg_user()
        self.setup_ui()
        self.init_signals()
        self.set_default()

    def setup_ui(self): 
        self.checkbox_widget_list = list()
        self.layout = QtWidgets.QGridLayout()

        # top layout 
        project_label = QtWidgets.QLabel('Project : ')
        entity_type_label = QtWidgets.QLabel('Type : ')
        to_label = QtWidgets.QLabel('To : ')
        note_label = QtWidgets.QLabel('Note : ')
        attachment_label = QtWidgets.QLabel('Attachments : ')

        project_label.setAlignment(QtCore.Qt.AlignTop)
        entity_type_label.setAlignment(QtCore.Qt.AlignTop)
        to_label.setAlignment(QtCore.Qt.AlignTop)
        to_label.setAlignment(QtCore.Qt.AlignRight)
        note_label.setAlignment(QtCore.Qt.AlignTop) 
        attachment_label.setAlignment(QtCore.Qt.AlignTop) 


        # widgets 
        self.project = project_widget.SGProjectComboBox(sg=sg, layout=QtWidgets.QHBoxLayout())
        self.project.label.setVisible(False)
        self.choice_layout = QtWidgets.QHBoxLayout()
        self.asset_rb = QtWidgets.QRadioButton('Asset')
        self.scene_rb = QtWidgets.QRadioButton('Shot')
        # fine tune 
        self.entity = hashtag_widget.TagWidget()
        self.entity.line_edit.setPlaceholderText('Problem Asset or Shot')
        self.entity.use_hash = False
        self.entity.allow_creation = False
        self.entity.allow_rename = False
        self.entity.label.hide()
        self.entity.display.setMaximumHeight(22)


        self.step_layout = QtWidgets.QGridLayout()
        self.checkbox_dict = self.add_step_checkboxs()
        self.content = QtWidgets.QPlainTextEdit()
        self.attachment = attachment_widget.Attachment()

        self.choice_layout.addWidget(self.asset_rb)
        self.choice_layout.addWidget(self.scene_rb)

        # add to layouts 
        self.layout.addWidget(project_label, 0, 0, 1, 1)
        self.layout.addWidget(self.project, 0, 1, 1, 1)
        self.layout.addItem(QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum), 0, 3, 1, 1)
        self.layout.addWidget(to_label, 2, 0, 1, 1)
        self.layout.addLayout(self.choice_layout, 2, 1, 1, 1)
        self.layout.addWidget(self.entity, 4, 1, 1, 3)
        self.layout.addLayout(self.step_layout, 5, 1, 1, 3)

        self.layout.addWidget(note_label, 9, 1, 1, 3)
        self.layout.addWidget(self.content, 10, 1, 1, 3)
        self.layout.addWidget(attachment_label, 11, 1, 1, 3)
        self.layout.addWidget(self.attachment, 12, 1, 1, 3)

        self.button = QtWidgets.QPushButton('Create Note')
        self.button.setMinimumSize(QtCore.QSize(200, 30))
        self.layout.addWidget(self.button, 13, 2, 1, 2)
        self.setLayout(self.layout)

        self.layout.setColumnStretch(0, 0)
        self.layout.setColumnStretch(1, 1)
        self.layout.setColumnStretch(2, 1)
        self.layout.setColumnStretch(3, 2)

    def set_default(self): 
        self.project.projectChanged.emit(self.project.current_item())
        self.scene_rb.setChecked(True)

    def init_signals(self): 
        self.project.projectChanged.connect(self.project_changed)
        self.asset_rb.clicked.connect(self.entity_selected)
        self.scene_rb.clicked.connect(self.entity_selected)
        self.button.clicked.connect(self.create_note)

    def add_step_checkboxs(self): 
        # project = self.project.current_item()
        steps = None 
        self.checkbox_dict = dict()
        clear_layout(self.step_layout)

        if self.asset_rb.isChecked(): 
            title = 'asset'
            steps = self.project_info.team_data()['asset'].keys()

        if self.scene_rb.isChecked(): 
            title = 'shot'
            steps = self.project_info.team_data()['shot'].keys()

        if steps: 
            display_limit = 8
            row = 0 
            col = 0 

            for i, step in enumerate(steps): 
                key = '{}-{}'.format(title, step)
                checkbox = QtWidgets.QCheckBox(step)
                self.checkbox_dict[key] = {'name': step, 'entity_type': title, 'widget': checkbox}
                col = (i % display_limit) + 1
                self.step_layout.addWidget(checkbox, row, col, 1, 1)
                
                if col == (display_limit): 
                    row += 1

        return self.checkbox_dict

    def project_changed(self, project): 
        self.project_info = project_info.ProjectInfo(project['name'])
        entity_type = self.get_entity_type()

        worker = thread_pool.Worker(self.fetch_tag_data, entity_type)
        worker.signals.result.connect(self.fetch_tag_finished)
        self.threadpool.start(worker)

    def fetch_tag_data(self, entity): 
        if entity == 'asset': 
            entities = sg_process.get_assets(project=self.project_info.name())

        if entity == 'scene': 
            entities = sg_process.get_all_shots(project=self.project_info.name())

        return entities

    def get_entity_type(self): 
        return 'asset' if self.asset_rb.isChecked() else 'scene'

    def get_checked_steps(self): 
        checked_list = list()
        for k, v in self.checkbox_dict.items(): 
            name = v['name']
            widget = v['widget'] 
            if widget.isChecked(): 
                checked_list.append([v['name'], v['entity_type']])

        return checked_list

    def get_receivers(self, checked_steps): 
        receivers = list()
        for step, entity_type in checked_steps: 
            data = self.project_info.team_data()[entity_type][step]
            receivers.extend(data['producer'])

        return receivers

    def fetch_tag_finished(self, data_list): 
        self.entity.clear()
        self.entity.set_data(data_list, 'code')
        self.add_step_checkboxs()

    def entity_selected(self): 
        self.project_changed(self.project.current_item())

    def create_note(self): 
        project = self.project.current_item()
        checked_steps = self.get_checked_steps()
        receivers = self.get_receivers(checked_steps)
        addressings_to = sg_note.get_user_entities(receivers)
        content = self.content.toPlainText()
        entities = self.entity.get_all_item_data()
        attachments = self.attachment.get_images()

        if entities: 
            name, entity = entities[0]
            print entities 
            print addressings_to
            print content

        sg_note.create_note2(
                        project_entity, 
                        entities=[], 
                        versions=[], 
                        content='', 
                        created_by=None, 
                        task_user=None, 
                        cc_user=None, 
                        status='opn', 
                        note_type='Fix', 
                        ticket_entity=None)
    

        # result = sg_note.create_ticket(
        #             project, 
        #             title, 
        #             description, 
        #             addressings_to, 
        #             created_by=self.user, 
        #             entity=entity, 
        #             addressings_cc=None, 
        #             priority=prioiry or None, 
        #             sg_ticket_type='Request', 
        #             attachments=attachments)


def clear_layout(layout):
    if layout is not None:
        while layout.count():
            child = layout.takeAt(0)
            if child.widget() is not None:
                child.widget().deleteLater()
            elif child.layout() is not None:
                clear_layout(child.layout())

    
def show(): 
    import maya.cmds
    from rftool.utils.ui import maya_win
    from rf_utils.sg import sg_process
    maya_win.deleteUI('test')
    logger.info('Run in Maya\n')
    win = QtWidgets.QMainWindow(maya_win.getMayaWindow())
    widget = NoteFormUi()
    win.setObjectName('test')
    win.setCentralWidget(widget)
    win.setWindowTitle('Riff Note Form')
    win.show()
