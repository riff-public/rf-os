import os
import sys
from datetime import datetime

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class DimScreen(QtWidgets.QSplashScreen):
    snapped = QtCore.Signal(str)
    """ snap screen shot with rubber band """
    """ darken the screen by making splashScreen """
    # app = QtWidgets.QApplication.instance() -> for maya
    # app = QtWidgets.QApplication(sys.argv) -> for standalone

    def __init__(self, isMaya):
        """"""
        if isMaya:
            app = QtWidgets.QApplication.instance()
        else:
            app = QtWidgets.QApplication.instance()
            if not app:
                app = QtWidgets.QApplication(sys.argv)

        screenGeo = QtWidgets.QDesktopWidget().screenGeometry(0)
        width = screenGeo.width()
        height = screenGeo.height()
        fillPix = QtGui.QPixmap(width, height)
        fillPix.fill(QtGui.QColor(1,1,1))

        super(DimScreen, self).__init__(fillPix)
        self.havePressed = False
        self.origin = QtCore.QPoint(0,0)
        self.end = QtCore.QPoint(0,0)
        self.rubberBand = None


        self.setWindowState(QtCore.Qt.WindowFullScreen)
        #self.setBackgroundRole(QtWidgets.QPalette.Dark)
        self.setWindowOpacity(0.4)

    def mousePressEvent(self, event):
        self.havePressed = True
        self.origin = event.pos()

        if not self.rubberBand:
            self.rubberBand = QtWidgets.QRubberBand(QtWidgets.QRubberBand.Rectangle, self)
        self.rubberBand.setGeometry(QtCore.QRect(self.origin, QtCore.QSize()))
        self.rubberBand.show()

    def mouseMoveEvent(self, event):
        self.rubberBand.setGeometry(QtCore.QRect(self.origin, event.pos()).normalized())

    def mouseReleaseEvent(self, event):
        self.rubberBand.hide()
        if self.havePressed == True:
            self.end = event.pos()
            self.hide()

            output = self.capture()
        QtWidgets.QApplication.restoreOverrideCursor()

    def capture(self):
        outputFile = self.get_output()
        if not os.path.exists(os.path.dirname(outputFile)):
            os.makedirs(os.path.dirname(outputFile))

        rbb_rect = self.rubberBand.geometry()
        QtGui.QPixmap.grabWindow(QtWidgets.QApplication.desktop().winId(), rbb_rect.x(), rbb_rect.y(), rbb_rect.width(), rbb_rect.height()).save(outputFile, 'png')
        self.snapped.emit(outputFile)
        logger.info(outputFile)
        return outputFile

    def get_output(self):
        filename = datetime.now().strftime("%y-%m-%d_%H-%M-%S")
        outputPath = '%s/tmpCapture/capture_%s.png' % (os.environ.get('TEMP'), filename)
        return outputPath


class SnapButton(QtWidgets.QWidget):
    """docstring for SnapButton"""
    captured = QtCore.Signal(str)
    precaptured = QtCore.Signal(bool)
    def __init__(self, captureCommand=None, parent=None):
        super(SnapButton, self).__init__(parent=parent)
        self.captureCommand = captureCommand
        self.w = 1280
        self.h = 1024
        self.allLayout = QtWidgets.QVBoxLayout()

        self.button = QtWidgets.QPushButton()

        self.allLayout.addWidget(self.button)
        self.allLayout.setSpacing(0)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)

        self.preCapScreen = DimScreen(isMaya=True)
        self.preCapScreen.snapped.connect(self.emit_path)

        self.init_signals()

    def init_signals(self):
        self.button.clicked.connect(self.capture)

    def capture(self):
        self.precaptured.emit(True)
        if not self.captureCommand:
            self.preCapScreen.show()
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.CrossCursor)

        else:
            result = self.captureCommand(self.w, self.h)
            self.captured.emit(result)

    def emit_path(self, path):
        self.captured.emit(path)

    def set_icon(self, iconPath):
        if os.path.exists(iconPath):
            iconWidget = QtGui.QIcon()
            iconWidget.addPixmap(QtGui.QPixmap(iconPath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
            self.button.setIcon(iconWidget)
            self.button.setIconSize(QtCore.QSize(30, 30))

