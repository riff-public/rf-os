uiName = 'AutoBuildWidgetUI'
_title = 'Auto Build'
_version = 'v.0.0.1'
_des = 'beta'

import os
import sys
import traceback
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import file_comboBox

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

notStartwith = ['.']


from rf_utils.widget import file_input_widget
from rf_utils import file_utils
from rf_utils.context import context_info
from rf_utils import register_entity
from rf_utils.widget import dialog
reload(register_entity)
from rf_utils import icon
reload(file_input_widget)
from rf_utils import ascii_utils
reload(ascii_utils)


class Color:
    green = [0, 100, 0]

class Config:
    alembic = 'Alembic'
    maya = 'maya'
    mtr = 'mtr'
    asm = 'asm'
    file = 'file'
    media = 'media'
    fileType = 'fileType'
    modelSteps = [context_info.Step.design, context_info.Step.model, context_info.Step.rig, context_info.Step.set]
    mtrSteps = [context_info.Step.texture, context_info.Step.lookdev, context_info.Step.groom, context_info.Step.sim]
    fileTypeMap = {'design': {'.jpg': media, '.png': media, '.jpeg': media},
                    'model': {'.ma': maya, '.abc': alembic},
                    'rig': {'.ma': maya},
                    'texture': {'.ma': mtr},
                    'lookdev': {'.ma': mtr},
                    'groom': {'.ma': maya},
                    'sim': {'.ma': maya},
                    'set': {'.ma': maya, '.asm': asm}
                    }
    keywordMap = {'_mtr_': mtr}
    importButton = 'Import new material'
    noImportButton = 'Use existing material'
    cancelButton = 'Cancel'

class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()

        # asset title
        self.titleLayout = QtWidgets.QGridLayout()

        # current
        self.currentStep = QtWidgets.QLabel('Department :')
        self.stepLabel = QtWidgets.QLabel('-')
        self.taskLabel = QtWidgets.QLabel('-')

        self.titleLayout.addWidget(self.currentStep, 0, 0, 1, 1)
        self.titleLayout.addWidget(self.stepLabel, 0, 1, 1, 1)
        self.titleLayout.addWidget(self.taskLabel, 0, 3, 1, 1)

        self.outputStep = QtWidgets.QLabel('Input from :')
        self.outputStepLabel = QtWidgets.QLabel('-')

        self.titleLayout.addWidget(self.outputStep, 1, 0, 1, 1)
        self.titleLayout.addWidget(self.outputStepLabel, 1, 1, 1, 1)

        self.lookLayout = QtWidgets.QHBoxLayout()
        self.lookLabel = QtWidgets.QLabel('Look')
        self.lookComboBox = QtWidgets.QComboBox()
        self.lookSpacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.lookLayout.addItem(self.lookSpacer)
        self.lookLayout.addWidget(self.lookLabel)
        self.lookLayout.addWidget(self.lookComboBox)

        self.listWidget = IOListWidget()
        self.buildAllCheckBox = QtWidgets.QCheckBox('Build All')
        self.buildButton = QtWidgets.QPushButton('Build All Items')

        self.allLayout.addLayout(self.titleLayout)
        self.allLayout.addLayout(self.lookLayout)
        self.allLayout.addWidget(self.listWidget)
        self.allLayout.addWidget(self.buildAllCheckBox)
        self.allLayout.addWidget(self.buildButton)

        self.setLayout(self.allLayout)
        self.set_default()

    def set_default(self):
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(0, 0, 0, 0)

        # button
        self.buildButton.setMinimumSize(QtCore.QSize(0, 30))
        self.buildAllCheckBox.setChecked(True)


class AutoBuildWidget(Ui):
    """docstring for InputWidget"""
    def __init__(self, dccHook=None, parent=None):
        super(AutoBuildWidget, self).__init__(parent=parent)
        self.regInfo = None
        self.dccHook = dccHook
        self.entity = None

        self.init_functions()

    def update_input(self, entity):
        # if asset
        if entity.entity_type == context_info.ContextKey.asset:
            self.entity = entity
            self.list_look(entity.copy())
            # data = list_input(entity.copy())

            # self.show_input(data)

    def list_look(self, entity): 
        res = 'md'
        regInfo = register_entity.Register(entity)
        looks = regInfo.get_looks(step='texture', res=res)
        if not looks:
            looks = ['main']

        for look in looks: 
            self.lookComboBox.addItem(look)

    def show_input(self, datas):
        self.listWidget.clear()

        for path, data in datas.iteritems():
            self.listWidget.add_data(os.path.basename(path), data)

    def init_functions(self):
        self.buildButton.clicked.connect(self.build)
        self.listWidget.listWidget.itemSelectionChanged.connect(self.item_selected)
        self.buildAllCheckBox.stateChanged.connect(self.checkbox_checked)
        self.lookComboBox.currentIndexChanged.connect(self.look_changed)


    def look_changed(self): 
        look = str(self.lookComboBox.currentText())
        entity = self.entity.copy()
        entity.context.update(look=look, process=look)
        data = list_input(entity)
        self.show_input(data)


    def item_selected(self):
        if not self.buildAllCheckBox.isChecked():
            items = self.listWidget.selected_items()
            if items:
                item = items[0]
                data = item.data(QtCore.Qt.UserRole)
                action = data['action']
                self.buildButton.setText(action)

    def checkbox_checked(self, value):
        state = True if value else False
        if state:
            self.buildButton.setText('Build All Items')
        else:
            self.item_selected()

    def build(self):
        complete = True
        missingCount = 0 
        missingPath = []

        if self.buildAllCheckBox.isChecked():
            datas = [a.data(QtCore.Qt.UserRole) for a in self.listWidget.items()]
        else:
            datas = [a.data(QtCore.Qt.UserRole) for a in self.listWidget.selected_items()]

        if datas:
            for data in datas:
                print 'data', data
                path = data['path']
                fileType = data['fileType']

                if os.path.exists(path): 
                    build(data, promptDialog=True)
                else: 
                    missingCount+=1 
                    complete = False 
                    missingPath.append('%s %s' % (fileType, path))
                    logger.warning('path not exists %s' % path)

            if not complete: 
                message = 'Missing %s files \n%s' % (missingCount, ('\n').join(missingPath))
                dialog.MessageBox.warning('Build not complete', message)


def list_input(entity, lookStep='texture'):
    # list inputs
    inputs = OrderedDict()
    task = 'any' if not entity.task else entity.task
    # register 
    res = 'md'
    regInfo = register_entity.Register(entity)
    looks = regInfo.get_looks(step=lookStep, res=res)

    # if qc, just use process as a task
    if entity.step == 'qc':
        task = entity.process
    inputConfig = entity.projectInfo.asset.io.step(step=entity.step, task=task)

    if inputConfig:
        for step in inputConfig:
            outputKeyData = inputConfig[step]
            entity.context.update(step=step, res=res)

            for outputKey in outputKeyData:
                heroDir = None
                for data in outputKeyData[outputKey]: 
                    if data == 'pathKey': 
                        pathKey = outputKeyData[outputKey][data]
                        heroDir = entity.path.scheme(key=pathKey)

                if outputKey in ['materialRig', 'material']: 
                    heroDir = entity.path.output_hero()
                    file = entity.output_name(outputKey=outputKey, hero=True)
                    inputFile = '%s/%s' % (heroDir.abs_path(), file)
                    outputKeyData[outputKey]['path'] = inputFile
                    outputKeyData[outputKey]['step'] = step
                    outputKeyData[outputKey]['context'] = entity.context
                    inputs[inputFile] = outputKeyData[outputKey]
                else: 
                    entity2 = entity.copy()
                    entity2.context.update(process='main', look='main')
                    heroDir = entity2.path.output_hero() if not heroDir else heroDir
                    file = entity2.output_name(outputKey=outputKey, hero=True)
                    inputFile = '%s/%s' % (heroDir.abs_path(), file)
                    outputKeyData[outputKey]['path'] = inputFile
                    outputKeyData[outputKey]['step'] = step
                    outputKeyData[outputKey]['context'] = entity2.context
                    inputs[inputFile] = outputKeyData[outputKey]


    return inputs


def build(data, promptDialog=False):
    from rf_app.sg_manager import dcc_hook
    path = data['path']
    action = data['action']
    fileType = data['fileType']
    contextStr = data['context']
    context = context_info.Context()
    context.set_context(str(contextStr))
    entity = context_info.ContextPathInfo(context=context)
    ns = data['ns']
    namespace = '%s_%s' % (entity.name, ns)


    if fileType in ['maya', 'alembic']:
        if action == 'reference':
            result = dcc_hook.reference_file(namespace, path)
            dcc_hook.add_obj_to_layer(namespace)
            logger.debug('Reference %s' % path)
        if action == 'import':
            result = dcc_hook.import_abc(path)
            logger.debug('Reference %s' % path)

    if fileType == 'material':
        if action == 'import':
            targetGrp = entity.projectInfo.asset.geo_grp()
            # dcc_hook.import_mtr(targetGrp, path)
            existingSe = dcc_hook.check_existing_se(path)

            if not existingSe: 
                # this is import fresh material 
                dcc_hook.import_mtr_se(targetGrp, path)
                logger.debug('Import material %s' % path)
            else: 
                if promptDialog: 
                    choice = material_choice()
                    
                    if choice == Config.importButton: 
                        dcc_hook.import_mtr_se(targetGrp, path)
                        logger.debug('User choose Import material %s' % path)
                    
                    elif choice == Config.noImportButton: 
                        dcc_hook.apply_mtr_se(targetGrp, path)
                        logger.debug('User choose Apply Existing material %s' % path)

                else: 
                    # use what existing in the scene 
                    dcc_hook.apply_mtr_se(targetGrp, path)
                    logger.debug('Apply Existing material %s' % path)


    if fileType == 'yetiMaterial':
        if action == 'import':
            existingSe = dcc_hook.check_existing_se(path)
            targetGrp = entity.projectInfo.asset.groom_grp()
            
            if not existingSe: 
                dcc_hook.mu.executeDeferred(dcc_hook.import_mtr_se, targetGrp, path)
                logger.debug('Import yetiMaterial %s' % path)
            
            else: 
                if promptDialog: 
                    choice = material_choice()

                    if choice == Config.importButton: 
                        dcc_hook.mu.executeDeferred(dcc_hook.import_mtr_se, targetGrp, path)
                        logger.debug('User choose Import material %s' % path)
                    
                    elif choice == Config.noImportButton: 
                        dcc_hook.mu.executeDeferred(dcc_hook.apply_mtr_se, targetGrp, path)
                        logger.debug('User choose Apply Existing material %s' % path)

                else: 
                    dcc_hook.mu.executeDeferred(dcc_hook.apply_mtr_se, targetGrp, path)
                    logger.debug('Apply Existing Yeti material %s' % path)



def material_choice(): 
    title = 'Material Existing'
    message = 'Material already exist.'
    result = dialog.CustomMessageBox.show(title, message, buttons=[Config.importButton, Config.noImportButton, Config.cancelButton])
    return result.value

def import_file(self):
    """ do import files """
    itemData = self.fileWidget.get_data()
    filename = itemData.get(file_input_widget.Config.filename)
    fileType = itemData.get(file_input_widget.Config.fileType)

    if fileType == Config.alembic:
        result = self.dccHook.import_abc(filename)
        dialog.MessageBox.success('Import complete', 'Import Alembic complete') if result else logger.error('Import alembic failed')

    if fileType == Config.maya:
        self.dccHook.import_file(filename)

    if fileType == Config.mtr:
        targetGrp = self.entity.projectInfo.asset.geo_grp()
        self.dccHook.import_mtr(targetGrp, filename)


def reference_file(self):
    """ do reference files """
    itemData = self.fileWidget.get_data()
    filename = itemData.get(file_input_widget.Config.filename)
    fileType = itemData.get(file_input_widget.Config.fileType)
    processNamespace = self.processNamespaceCheckBox.isChecked()

    if fileType in [Config.alembic, Config.maya]:
        namespace = self.entity.name if not processNamespace else str(self.process_comboBox.currentText())
        result = self.dccHook.reference_file(namespace, filename)
        dialog.MessageBox.success('Complete', 'Reference Alembic/Maya complete') if result else logger.error('Import alembic failed')


class IOListWidget(QtWidgets.QWidget):
    """docstring for IOWidget"""
    def __init__(self, parent=None):
        super(IOListWidget, self).__init__(parent)
        self.allLayout = QtWidgets.QHBoxLayout()
        self.listWidget = QtWidgets.QListWidget()
        self.allLayout.addWidget(self.listWidget)
        self.setLayout(self.allLayout)

        self.init_color()
        self.init_icon_dir()

    def init_icon_dir(self):
        self.iconDir = '%s/icons/input' % module_dir
        self.groomIcon = 'groom_ma.png'
        self.mayaIcon = 'model_ma.png'
        self.cacheIcon = 'model_abc.png'
        self.materialIcon = 'mtr_ma.png'
        self.rigIcon = 'rig_ma.png'

    def init_color(self):
        # color preset
        self.gray1 = [160, 160, 160]
        self.gray2 = [100, 100, 100]
        self.green1 = [0, 200, 60]
        self.red1 = [160, 0, 0]

    def clear(self):
        self.listWidget.clear()

    def add_data(self, display, data):
        itemWidget = IOItemWidget()
        item = QtWidgets.QListWidgetItem(self.listWidget)
        item.setData(QtCore.Qt.UserRole, data)
        item.setSizeHint(itemWidget.sizeHint())
        self.listWidget.setItemWidget(item, itemWidget)

        # display data
        itemWidget.set_text1(display)
        itemWidget.set_text2('%s department' % data['step'])
        # itemWidget.set_text3(data['fileType'])
        itemWidget.set_text3(data['action'])

        fileColor = self.file_color(data['path'])
        itemWidget.set_text1_color(fileColor)

        iconPath  = self.get_icon(data['icon'])
        itemWidget.set_icon1(iconPath)

    def file_color(self, file):
        if os.path.exists(file):
            return self.green1
        return self.red1

    def get_icon(self, fileType):
        if fileType == 'cache':
            return '%s/%s' % (self.iconDir, self.cacheIcon)
        if fileType == 'maya':
            return '%s/%s' % (self.iconDir, self.mayaIcon)
        if fileType == 'material':
            return '%s/%s' % (self.iconDir, self.materialIcon)
        if fileType == 'yeti':
            return '%s/%s' % (self.iconDir, self.groomIcon)

    def items(self):
        return [self.listWidget.item(a) for a in range(self.listWidget.count())]

    def selected_items(self):
        return self.listWidget.selectedItems()


class IOItemWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(IOItemWidget, self).__init__(parent)
        self.set_ui()

    def set_ui(self):
        self.allLayout = QtWidgets.QHBoxLayout()
        self.iconLabel1 = QtWidgets.QLabel()

        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.label1 = QtWidgets.QLabel()
        self.label2 = QtWidgets.QLabel()
        self.label3 = QtWidgets.QLabel()
        self.label4 = QtWidgets.QLabel()

        self.verticalLayout.addWidget(self.label1)
        self.verticalLayout.addWidget(self.label2)
        self.verticalLayout.addWidget(self.label3)
        self.verticalLayout.addWidget(self.label4)

        self.verticalLayout.setContentsMargins(1, 1, 1, 1)
        self.verticalLayout.setSpacing(1)

        self.allLayout.setContentsMargins(2, 2, 2, 2)
        self.allLayout.setSpacing(2)

        self.allLayout.addWidget(self.iconLabel1)
        self.allLayout.addLayout(self.verticalLayout)

        self.allLayout.setStretch(0, 1)
        self.allLayout.setStretch(1, 6)
        self.setLayout(self.allLayout)

        self.default_font()

    def default_font(self):
        font = QtGui.QFont('', 9)
        font.setBold(True)
        font.setWeight(75)
        self.label1.setFont(font)

        self.set_text2_color([160, 160, 160])
        self.set_text3_color([100, 100, 100])

    def set_icon1(self, path, size=[32, 32]):
        self.iconLabel1.setPixmap(QtGui.QPixmap(path).scaled(size[0], size[1], QtCore.Qt.KeepAspectRatio))

    def set_text1(self, text):
        self.label1.setText(text)

    def set_text2(self, text):
        self.label2.setText(text)

    def set_text3(self, text):
        self.label3.setText(text)

    def set_text4(self, text):
        self.label4.setText(text)

    def set_text1_color(self, color):
        self.label1.setStyleSheet('color: rgb(%s, %s, %s);' % (color[0], color[1], color[2]))

    def set_text2_color(self, color):
        self.label2.setStyleSheet('color: rgb(%s, %s, %s);' % (color[0], color[1], color[2]))

    def set_text3_color(self, color):
        self.label3.setStyleSheet('color: rgb(%s, %s, %s);' % (color[0], color[1], color[2]))

    def set_text4_color(self, color):
        self.label4.setStyleSheet('color: rgb(%s, %s, %s);' % (color[0], color[1], color[2]))



class Main(QtWidgets.QMainWindow):
    """docstring for Main"""
    def __init__(self, parent=None):
        super(Main, self).__init__(parent)
        from rf_app.sg_manager import dcc_hook
        reload(dcc_hook)

        self.inputWidget = AutoBuildWidget(dccHook=dcc_hook)
        self.inputWidget.allLayout.setContentsMargins(8, 8, 8, 8)
        self.setCentralWidget(self.inputWidget)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))
        self.resize(360, 500)

        self.init_functions()

    def init_functions(self):
        self.asset = context_info.ContextPathInfo()
        if self.asset.project:
            self.inputWidget.update_input(self.asset)

def show():
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = Main(maya_win.getMayaWindow())
    myApp.show()
    return myApp

    # parentSgEntities = sgEntity.get('parents') if hasattr(sgEntity, 'get') else None
    # setNames = [a['name'] for a in parentSgEntities] if parentSgEntities else []
    # self.ui.inputWidget.update_input(self.entity, setNames)

