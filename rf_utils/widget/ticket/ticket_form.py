#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import traceback
from collections import OrderedDict

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
from rf_utils.widget import hashtag_widget
from rf_utils.sg import sg_process
from rf_utils.widget import project_widget
from rf_utils import project_info
from rf_utils import user_info
from rf_utils.sg import sg_note
from rf_utils.widget.attachment import attachment_widget
from rf_utils import thread_pool


module_path = sys.modules[__name__].__file__
module_dir = os.path.dirname(module_path)
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

sg = sg_process.sg


class TicketUi(QtWidgets.QWidget):

    def __init__(self, user=None, threadpool=None, parent=None):
        super(TicketUi, self).__init__(parent)
        self.threadpool = threadpool
        self.project_info = project_info.ProjectInfo('default')
        self.user = user
        if not user: 
            self.user = user_info.User().sg_user()
        self.setup_ui()
        self.init_signals()
        self.set_default()

    def setup_ui(self): 
        self.checkbox_widget_list = list()
        self.layout = QtWidgets.QGridLayout()

        # top layout 
        project_label = QtWidgets.QLabel('Project : ')
        title_label = QtWidgets.QLabel('Title : ')
        priority_label = QtWidgets.QLabel('Priority : ')
        related_entity = QtWidgets.QLabel('Related Asset or Shot : ')
        to_label = QtWidgets.QLabel('To : ')
        asset_label = QtWidgets.QLabel('Assets : ')
        scene_label = QtWidgets.QLabel('Shots : ')
        description_label = QtWidgets.QLabel('Description : ')
        attachment_label = QtWidgets.QLabel('Attachments : ')

        project_label.setAlignment(QtCore.Qt.AlignTop)
        title_label.setAlignment(QtCore.Qt.AlignTop)
        priority_label.setAlignment(QtCore.Qt.AlignTop)
        related_entity.setAlignment(QtCore.Qt.AlignTop)
        to_label.setAlignment(QtCore.Qt.AlignTop)
        asset_label.setAlignment(QtCore.Qt.AlignTop)
        scene_label.setAlignment(QtCore.Qt.AlignTop)
        description_label.setAlignment(QtCore.Qt.AlignTop) 
        attachment_label.setAlignment(QtCore.Qt.AlignTop) 

        self.checkbox_widget_list.append(asset_label)
        self.checkbox_widget_list.append(scene_label)

        # widgets 
        self.project = project_widget.SGProjectComboBox(sg=sg, layout=QtWidgets.QHBoxLayout())
        self.project.label.setVisible(False)
        self.title = QtWidgets.QLineEdit()
        self.priority_combobox = QtWidgets.QComboBox()
        self.related_entity = self.add_entity_widget()
        self.choice_layout = QtWidgets.QHBoxLayout()
        self.specify_rb = QtWidgets.QRadioButton('Specify Departments')
        self.pipeline_rb = QtWidgets.QRadioButton('Not sure')
        self.asset_frame, self.scene_frame, self.checkbox_dict = self.add_step_checkboxs()
        self.description = QtWidgets.QPlainTextEdit()
        self.attachment = attachment_widget.Attachment()

        self.choice_layout.addWidget(self.specify_rb)
        self.choice_layout.addWidget(self.pipeline_rb)

        self.checkbox_widget_list.append(self.asset_frame)
        self.checkbox_widget_list.append(self.scene_frame)

        line1 = self.line()
        line2 = self.line()
        self.checkbox_widget_list.append(line1)
        self.checkbox_widget_list.append(line2)

        # add to layouts 
        self.layout.addWidget(project_label, 0, 0, 1, 1)
        self.layout.addWidget(self.project, 0, 1, 1, 1)
        self.layout.addItem(QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum), 0, 3, 1, 1)
        self.layout.addWidget(related_entity, 1, 0, 1, 1)
        self.layout.addWidget(self.related_entity, 1, 1, 1, 1)
        self.layout.addWidget(to_label, 2, 0, 1, 1)
        self.layout.addLayout(self.choice_layout, 2, 1, 1, 1)
        self.layout.addWidget(line1, 3, 1, 1, 3)
        self.layout.addWidget(asset_label, 4, 0, 1, 1)
        self.layout.addWidget(self.asset_frame, 4, 1, 1, 3)
        self.layout.addWidget(line2, 5, 1, 1, 3)
        self.layout.addWidget(scene_label, 6, 0, 1, 3)
        self.layout.addWidget(self.scene_frame, 6, 1, 1, 3)

        self.layout.addWidget(title_label, 7, 0, 1, 1)
        self.layout.addWidget(self.title, 7, 1, 1, 3)

        self.layout.addWidget(priority_label, 8, 0, 1, 1)
        self.layout.addWidget(self.priority_combobox, 8, 1, 1, 1)

        self.layout.addWidget(description_label, 9, 0, 1, 3)
        self.layout.addWidget(self.description, 9, 1, 1, 3)
        self.layout.addWidget(self.attachment, 10, 1, 1, 3)

        self.button = QtWidgets.QPushButton('Submit')
        self.button.setMinimumSize(QtCore.QSize(200, 30))
        self.layout.addWidget(self.button, 11, 2, 1, 2)
        self.setLayout(self.layout)

        self.layout.setColumnStretch(0, 0)
        self.layout.setColumnStretch(1, 1)
        self.layout.setColumnStretch(2, 1)
        self.layout.setColumnStretch(3, 2)

    def set_default(self): 
        self.project.projectChanged.emit(self.project.current_item())
        self.specify_rb.setChecked(True)

    def init_signals(self): 
        self.project.projectChanged.connect(self.project_changed)
        self.specify_rb.clicked.connect(self.choice_changed)
        self.pipeline_rb.clicked.connect(self.choice_changed)
        self.button.clicked.connect(self.create_ticket)

    def add_entity_widget(self): 
        # get project entity from project widget
        project = self.project.current_item()

        # --------------- ASSET TAG WIDGET
        # cache all the assets of the project so we don't do long fetch every time tag widget is initialized
        # assets = sg_process.get_assets(project=project['name'])
        # shots = sg_process.get_all_shots(project=project['name'])
        # self.related_entity = hashtag_widget.SGAssetTagWidget(project, assets=assets + shots, inLine=False)  # <--- this arg is the list of assets stored within the widget
        self.related_entity = hashtag_widget.TagWidget(inLine=False)  # <--- this arg is the list of assets stored within the widget
        # self.related_entity.set_data(data=[assets + shots], displayKey='code')

        # fine tune 
        self.related_entity.line_edit.setPlaceholderText('Problem Asset or Shot')
        self.related_entity.use_hash = False
        self.related_entity.allow_creation = False
        self.related_entity.allow_rename = False
        self.related_entity.label.hide()
        self.related_entity.display.setMaximumHeight(23)

        return self.related_entity

    def add_shot_widget(self): 
        # return QtWidgets.QLineEdit()

        # ------------------- SHOT TAG WIDGET
        project = self.project.current_item()
        # get ep cache
        episodes = sg_process.get_episodes(project=project['name'])

        # EP widget
        self.related_shot = hashtag_widget.SGEpTagWidget(project=project, episodes=episodes, inLine=False)
        self.related_shot.line_edit.setPlaceholderText('Problem shots')
        self.related_shot.use_hash = False
        self.related_shot.allow_creation = False
        self.related_shot.allow_rename = False
        self.related_shot.label.hide()
        self.related_shot.display.setMaximumHeight(52)

        return self.related_shot

    def add_step_checkboxs(self): 
        # project = self.project.current_item()
        asset_steps = self.project_info.team_data()['asset'].keys()
        scene_steps = self.project_info.team_data()['shot'].keys()

        asset_frame = QtWidgets.QFrame()
        scene_frame = QtWidgets.QFrame()
        grid_asset = QtWidgets.QGridLayout()
        grid_scene = QtWidgets.QGridLayout()
        asset_frame.setLayout(grid_asset)
        scene_frame.setLayout(grid_scene)
        self.checkbox_dict = dict()

        display_limit = 8

        for data in [('asset', grid_asset, asset_steps), ('shot', grid_scene, scene_steps)]: 
            row = 0 
            title, grid_layout, steps = data
            # label = QtWidgets.QLabel(title)
            # grid_layout.addWidget(label, row, 0, 1, 1)

            for i, step in enumerate(steps): 
                key = '{}-{}'.format(title, step)
                checkbox = QtWidgets.QCheckBox(step)
                self.checkbox_dict[key] = {'name': step, 'entity_type': title, 'widget': checkbox}
                col = (i % display_limit) + 1
                grid_layout.addWidget(checkbox, row, col, 1, 1)
                
                if col == (display_limit): 
                    row += 1

        return asset_frame, scene_frame, self.checkbox_dict

    def line(self, direction='h', parent=None):
        """ line """
        line = QtWidgets.QFrame(parent)
        line.setFrameShape(QtWidgets.QFrame.HLine) if direction == 'h' else line.setFrameShape(QtWidgets.QFrame.VLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        return line

    def project_changed(self, project=None): 
        if not project: 
            project = self.project.current_item()
        
        self.project_info = project_info.ProjectInfo(project['name'])
        worker = thread_pool.Worker(self.fetch_tag_data)
        worker.signals.result.connect(self.fetch_tag_finished)
        self.threadpool.start(worker)

    def fetch_tag_data(self): 
        assets = sg_process.get_assets(project=self.project_info.name())
        shots = sg_process.get_all_shots(project=self.project_info.name())

        # set priority 
        priorities = [''] + sg_note.get_field_list_value('Ticket', 'sg_priority')
        self.priority_combobox.clear()
        self.priority_combobox.addItems(priorities)
        return assets + shots

    def fetch_tag_finished(self, data_list): 
        self.related_entity.clear()
        self.related_entity.set_data(data_list, 'code')

    def choice_changed(self): 
        is_pipeline = self.pipeline_rb.isChecked()  
        
        for widget in self.checkbox_widget_list: 
            widget.setEnabled(not is_pipeline)

    def create_ticket(self): 
        project = self.project.current_item()
        checked_steps = self.get_checked_steps()
        receivers = self.get_receivers(checked_steps)
        addressings_to = sg_note.get_user_entities(receivers)
        description = self.description.toPlainText()
        prioiry = self.priority_combobox.currentText()
        entities = self.related_entity.get_all_item_data()
        attachments = self.attachment.get_images()
        title = self.title.text()

        if entities: 
            name, entity = entities[0]

        result = sg_note.create_ticket(
                    project, 
                    title, 
                    description, 
                    addressings_to, 
                    created_by=self.user, 
                    entity=entity, 
                    addressings_cc=None, 
                    priority=prioiry or None, 
                    sg_ticket_type='Request', 
                    attachments=attachments)

        

    def get_checked_steps(self): 
        checked_list = list()
        for k, v in self.checkbox_dict.items(): 
            name = v['name']
            widget = v['widget'] 
            if widget.isChecked(): 
                checked_list.append([v['name'], v['entity_type']])

        return checked_list

    def get_receivers(self, checked_steps): 
        receivers = list()
        for step, entity_type in checked_steps: 
            data = self.project_info.team_data()[entity_type][step]
            
            for k, v in data.items(): 
                receivers.extend(v)

        return receivers

    
def show(): 
    import maya.cmds
    from rftool.utils.ui import maya_win
    from rf_utils.sg import sg_process
    maya_win.deleteUI('test')
    logger.info('Run in Maya\n')
    win = QtWidgets.QMainWindow(maya_win.getMayaWindow())
    widget = TicketUi()
    win.setObjectName('test')
    win.setCentralWidget(widget)
    win.setWindowTitle('Riff Ticket Form')
    win.show()
