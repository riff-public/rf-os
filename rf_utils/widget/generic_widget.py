from Qt import QtWidgets 
from Qt import QtGui 
from Qt import QtCore 
from rf_utils import icon 


class Logo(QtWidgets.QWidget):
    """docstring for Logo"""
    def __init__(self, parent=None):
        super(Logo, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.logo = QtWidgets.QLabel()
        self.logo.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

        self.layout.addWidget(self.logo)
        self.setLayout(self.layout)

        
def spacer(direction='h'): 
    if direction == 'h': 
        w = 20
        h = 40
    if direction == 'w': 
        w = 40
        h = 20
    return QtWidgets.QSpacerItem(w, h, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        

def line(direction='h'):
    """ line """
    line = QtWidgets.QFrame(None)
    line.setFrameShape(QtWidgets.QFrame.HLine) if direction == 'h' else line.setFrameShape(QtWidgets.QFrame.VLine)
    line.setFrameShadow(QtWidgets.QFrame.Sunken)
    return line
