#!/usr/bin/env python
# -- coding: utf-8 --
import sys 
import os 
from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

from rf_utils.widget import display_snap
from rf_utils.widget import snap_widget

MODULE_PATH = sys.modules[__name__].__file__
MODULE_DIR  = os.path.dirname(MODULE_PATH)

class Icon:
    snap = '{}/snap.png'.format(MODULE_DIR)


class Color:
    date = 'color: rgb(100, 140, 200);'
    link = 'color: rgb(100, 200, 100);'
    green = 'color: rgb(40, 200, 40);'
    light_green_bg = 'background-color: rgb(60, 160, 60);'
    reply = 'background-color: rgb(80, 160, 240);'
    red = 'color: rgb(200, 70, 70);'
    progress = 'color: rgb(100, 200, 100);'
    version = 'color: rgb(220, 160, 100)'
    button = 'background-color: rgb(40, 40, 40)'
    status = 'color: rgb(40, 40, 40);'
    snap = 'background-color: rgb(200, 200, 200);'


class Attachment(QtWidgets.QWidget):
    """docstring for NoteContent"""
    item_selected = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(Attachment, self).__init__(parent=parent)
        self.setup_ui()
        self.init_signals()

    def setup_ui(self):
        self.attachment_layout = QtWidgets.QHBoxLayout()
        self.reply_layout = QtWidgets.QHBoxLayout()

        # attachment section 
        self.snap_widget = display_snap.DisplaySnap()
        self.snap_widget.setMaximumSize(QtCore.QSize(2000, 40))
        # self.snap_widget.hide()

        self.snap_button = snap_widget.SnapButton()
        # self.snap_button.hide()
        self.snap_button.button.setIcon(QtGui.QPixmap(Icon.snap))
        self.snap_button.button.setIconSize(QtCore.QSize(32, 32))
        self.snap_button.setMaximumSize(QtCore.QSize(64, 64))
        self.snap_button.button.setStyleSheet(Color.reply)

        self.attachment_layout.addWidget(self.snap_widget)
        self.attachment_layout.addWidget(self.snap_button)

        self.attachment_layout.setStretch(0, 1)
        self.attachment_layout.setStretch(1, 0)
        self.setLayout(self.attachment_layout)

    def init_signals(self): 
        self.snap_button.captured.connect(self.snap_widget.add_item)

    def get_images(self): 
        return self.snap_widget.get_images()
