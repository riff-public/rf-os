uiName = 'InfoWidget'
import os
import sys
import traceback
from collections import OrderedDict
from functools import partial
from datetime import datetime
import re

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import file_comboBox

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

notStartwith = ['.']

from rf_utils import file_utils
from rf_utils.context import context_info
from rf_utils import register_entity
from rf_utils.widget import dialog
from rf_app.sg_manager import utils
from rf_utils.widget import file_widget
from rf_utils.widget import media_widget
reload(media_widget)
reload(register_entity)
from rf_utils import icon

noPreviewImg = '%s/icons/etc/nopreview_icon.png' % module_dir
loadingImg = '%s/icons/etc/loading_icon.png' % module_dir


class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.gridLayout = QtWidgets.QGridLayout()

        # look
        self.look_label = QtWidgets.QLabel('Look')
        self.look_comboBox = QtWidgets.QComboBox()

        self.gridLayout.addWidget(self.look_label, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.look_comboBox, 0, 1, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.gridLayout.addItem(spacerItem, 0, 2, 1, 1)
        self.allLayout.addLayout(self.gridLayout)

        # info
        self.infoLayout = QtWidgets.QVBoxLayout()
        self.frame = QtWidgets.QFrame()
        self.frame.setFrameShape(QtWidgets.QFrame.Box)
        self.frame.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.frame_layout = QtWidgets.QVBoxLayout(self.frame)
        # self.preview_label = QtWidgets.QLabel(self.frame)
        self.preview_label = media_widget.PreviewMedia()
        self.frame_layout.addWidget(self.preview_label)
        self.infoLayout.addWidget(self.frame)
        self.allLayout.addLayout(self.infoLayout)

        self.assetFrame = QtWidgets.QFrame()
        self.assetLayout = QtWidgets.QVBoxLayout(self.assetFrame)

        self.sceneFrame = QtWidgets.QFrame()
        self.sceneLayout = QtWidgets.QVBoxLayout(self.sceneFrame)

        # res
        self.resLayout = QtWidgets.QGridLayout()
        self.showHero_checkBox = QtWidgets.QCheckBox('Custom Reference')
        self.res_label = QtWidgets.QLabel('Res')
        self.res_comboBox = QtWidgets.QComboBox()
        self.res_comboBox.setMaximumSize(QtCore.QSize(60, 30))

        # show 
        self.filterLabel = QtWidgets.QLabel('Type')
        self.filter_comboBox = QtWidgets.QComboBox()
        self.filter_comboBox.setMaximumSize(QtCore.QSize(60, 30))

        self.resLayout.addWidget(self.showHero_checkBox, 0, 4, 1, 1)
        self.resLayout.addWidget(self.res_label, 0, 0, 1, 1)
        self.resLayout.addWidget(self.res_comboBox, 0, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.resLayout.addWidget(self.filterLabel, 0, 2, 1, 1)
        self.resLayout.addWidget(self.filter_comboBox, 0, 3, 1, 1)
        # self.resLayout.addItem(spacerItem1, 0, 2, 1, 1)
        self.assetLayout.addLayout(self.resLayout)
        self.allLayout.addWidget(self.assetFrame)
        self.allLayout.addWidget(self.sceneFrame)

        self.hero_listWidget = file_widget.FileListWidget()
        self.assetLayout.addWidget(self.hero_listWidget)

        # hero area
        self.heroLayout = QtWidgets.QHBoxLayout()
        # ref
        self.reference_radioButton = QtWidgets.QRadioButton('Reference')
        self.import_radioButton = QtWidgets.QRadioButton('Import')

        self.heroLayout.addWidget(self.reference_radioButton)
        self.heroLayout.addWidget(self.import_radioButton)
        self.assetLayout.addLayout(self.heroLayout)

        self.line = QtWidgets.QFrame()
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.assetLayout.addWidget(self.line)


        # button
        self.custom_pushButton = QtWidgets.QPushButton('Custom')
        self.custom_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.rig_pushButton = QtWidgets.QPushButton('Rig')
        self.gpu_pushButton = QtWidgets.QPushButton('Gpu')

        self.assetLayout.addWidget(self.custom_pushButton)
        self.rig_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.assetLayout.addWidget(self.rig_pushButton)
        self.gpu_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.assetLayout.addWidget(self.gpu_pushButton)

        self.showAll_checkBox = QtWidgets.QCheckBox('Show All Mov files')
        self.playblast_listWidget = file_widget.FileListWidget()
        self.sceneLayout.addWidget(self.showAll_checkBox)
        self.sceneLayout.addWidget(self.playblast_listWidget)


        self.allLayout.setStretch(1, 1)
        self.allLayout.setStretch(2, 1)
        self.allLayout.setStretch(3, 1)
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.assetLayout.setContentsMargins(0, 0, 0, 0)

        self.setLayout(self.allLayout)
        self.set_default()

    def set_default(self):
        self.custom_pushButton.setVisible(False)
        self.hero_listWidget.setEnabled(False)
        self.reference_radioButton.setChecked(True)
        self.assetFrame.setVisible(False)
        self.sceneFrame.setVisible(False)

class Config: 
    filters = OrderedDict([('All', {'include': ''}), 
                        ('Rig', {'include': '_rig_', 'exclude': '_mtr_'}), 
                        ('Loop', {'include': '_anim_'}), 
                        ('Action', {'include': '_mcdAction_'})])


class AssetInfoWidget(Ui):
    """docstring for AssetInfoWidget"""
    def __init__(self, dccHook=None, parent=None):
        super(AssetInfoWidget, self).__init__(parent=parent)
        self.regInfo = None
        self.dccHook = dccHook
        self.cacheLookSelection = dict()
        self.init_signals()

    def update_input(self, entity, sgEntity=None):
        self.entity = entity
        self.sgEntity = sgEntity
        self.regInfo = register_entity.Register(entity)

        if self.entity.entity_type == context_info.ContextKey.asset:
            self.assetFrame.setVisible(True)
            self.sceneFrame.setVisible(False)
            self.check_ref()
            if self.check_publ():
                self.set_res()
                self.list_look()
                self.list_filters()
                self.list_hero()
            else:
                self.clear_ui(self.entity)
            self.set_preview()

        if self.entity.entity_type == context_info.ContextKey.scene: 
            self.assetFrame.setVisible(False)
            self.sceneFrame.setVisible(True)
            self.set_playblast_preview()

        # set media widget fps
        fps = entity.projectInfo.render.fps()
        self.preview_label.fps = fps

    def check_publ(self):
        heroPath = self.entity.path.hero().abs_path()
        return True if os.path.exists(heroPath) else False

    def clear_ui(self, entity):
        self.look_comboBox.clear()
        self.res_comboBox.clear()
        self.filter_comboBox.clear()
        self.hero_listWidget.clear()
        self.no_preview(entity)


    def init_signals(self):
        # ui
        self.showHero_checkBox.stateChanged.connect(self.custom_hero_mode)
        self.reference_radioButton.toggled.connect(self.custom_hero_mode)
        self.import_radioButton.toggled.connect(self.custom_hero_mode)

        self.res_comboBox.currentIndexChanged.connect(self.list_look)
        self.look_comboBox.currentIndexChanged.connect(self.refresh)
        self.filter_comboBox.currentIndexChanged.connect(self.list_hero)

        self.playblast_listWidget.itemSelected.connect(self.playblast_selected)

        # buttons
        self.rig_pushButton.clicked.connect(self.create_reference)
        self.gpu_pushButton.clicked.connect(self.create_gpu)

        # checkBox 
        self.showAll_checkBox.stateChanged.connect(self.set_playblast_preview)

    def custom_hero_mode(self, value):
        state = self.showHero_checkBox.isChecked()
        # button label
        ref = self.reference_radioButton.isChecked()
        imp = self.import_radioButton.isChecked()
        customButtonLabel = 'Reference' if ref else 'Import' if imp else ''
        # show button state
        self.custom_pushButton.setVisible(state)
        self.rig_pushButton.setVisible(not state)
        self.gpu_pushButton.setVisible(not state)
        self.hero_listWidget.setEnabled(state)

        self.custom_pushButton.setText(customButtonLabel) if customButtonLabel else None

    def refresh(self):
        if self.check_publ():
            self.set_preview()
            self.list_filters()
            self.list_hero()
            self.check_ref()
            self.record_selection()
        else:
            self.clear_ui(self.entity)

    def set_res(self):
        self.res_comboBox.blockSignals(True)
        res = self.regInfo.get_res()
        self.res_comboBox.clear()
        self.res_comboBox.addItems(res)
        self.res_comboBox.blockSignals(False)

    def list_look(self):
        self.look_comboBox.blockSignals(True)
        res = self.res()
        looks = self.regInfo.get_looks(step='texture', res=res)
        self.look_comboBox.clear()

        looks = looks if looks else self.regInfo.get_processes(step='model', res=res)
        # resorted 
        looks = self.resorted_looks(looks)

        for i, look in enumerate(looks):
            self.look_comboBox.addItem(look)
            previewPath = self.regInfo.get.preview(step='texture', look=look)
            previewPath = previewPath if previewPath else self.sgEntity.get('iconPath')
            self.look_comboBox.setItemData(i, previewPath, QtCore.Qt.UserRole)

        self.set_last_look()
        self.look_comboBox.blockSignals(False)

    def resorted_looks(self, looks): 
        """ main first """
        sortedLooks = []
        mainLook = 'main'
        if mainLook in looks: 
            sortedLooks.append(mainLook)
            for look in looks: 
                sortedLooks.append(look) if not look in sortedLooks else None

            return sortedLooks
        return looks 

    def set_last_look(self): 
        # set default selection 
        if self.entity.name in self.cacheLookSelection.keys(): 
            look = self.cacheLookSelection[self.entity.name]
            allTexts = [self.look_comboBox.itemText(a) for a in range(self.look_comboBox.count())]
            index = allTexts.index(look) if look in allTexts else 0
            self.look_comboBox.setCurrentIndex(index)


    def check_ref(self):
        rigFile, rigRelPath = self.get_rig()
        gpuFile, gpuRelPath = self.get_gpu()

        self.rig_pushButton.setEnabled(os.path.exists(rigFile))
        self.gpu_pushButton.setEnabled(os.path.exists(gpuFile))

    def record_selection(self): 
        look = str(self.look_comboBox.currentText())
        self.cacheLookSelection[self.entity.name] = look

    def list_filters(self): 
        self.filter_comboBox.blockSignals(True)
        self.filter_comboBox.clear()
        i = 0 
        for k, v in Config.filters.iteritems(): 
            self.filter_comboBox.addItem(k)
            self.filter_comboBox.setItemData(i, v, QtCore.Qt.UserRole)
            i+=1

        self.filter_comboBox.setCurrentIndex(Config.filters.keys().index('Rig'))
        self.filter_comboBox.blockSignals(False)

    def list_hero(self):
        heroPath = self.entity.path.hero().abs_path()
        files = self.list_files(heroPath)
        self.hero_listWidget.clear()
        # self.hero_listWidget.remove_all()
        self.hero_listWidget.add_files(files)

    def list_files(self, path): 
        # filter 
        filterData = self.filter_comboBox.itemData(self.filter_comboBox.currentIndex(), QtCore.Qt.UserRole)
        files = file_utils.list_file(path)
        resultPaths = []

        for file in files: 
            filePath = '%s/%s' % (path, file)
            include = filterData.get('include')
            exclude = filterData.get('exclude')

            if include in file: 
                if exclude: 
                    if not exclude in file: 
                        resultPaths.append(filePath)
                else: 
                    resultPaths.append(filePath)

        return resultPaths

    def set_preview(self):
        path = self.look_comboBox.itemData(self.look_comboBox.currentIndex())
        iconDir = self.entity.path.icon().abs_path()
        iconName = self.entity.icon_name(ext='.png')
        iconPath = '%s/%s' % (iconDir, iconName)
        previewPath = path if path else iconPath if os.path.exists(iconPath) else ''
        self.preview(previewPath) if previewPath else self.no_preview()

    def no_preview(self, entity=None):
        entity = self.entity if not entity else entity
        # self.preview_label.setText('%s\n No Hero' % entity.name)
        self.preview_label.mediaplayer.clear()
        self.preview_label.clear_view()
        self.preview_label.loadMedia(noPreviewImg)

    def preview(self, path, size=[300, 200]):
        self.preview_label.loadMedia([path])
        # self.preview_label.setPixmap(QtGui.QPixmap(path).scaled(size[0], size[1], QtCore.Qt.KeepAspectRatio))

    def set_playblast_preview(self): 
        showAll = self.showAll_checkBox.isChecked()
        path = self.entity.path.name().abs_path()
        movs = self.find_latest_playblast(path, not showAll)

        self.playblast_listWidget.clear()
        self.playblast_listWidget.remove_all()
        self.preview_label.mediaplayer.clear()
        self.preview_label.clear_view()
        self.playblast_listWidget.add_files(movs)
        self.playblast_listWidget.listWidget.setCurrentRow(0)

    def find_latest_playblast(self, path, latest=True): 
        movExts = ['.mov']
        latests = []
        steps = file_utils.list_folder(path)

        for step in steps: 
            stepPath = '%s/%s' % (path, step)
            processes = file_utils.list_folder(stepPath)

            for process in processes: 
                mediaPath = '%s/%s/output' % (stepPath, process)
                if os.path.exists(mediaPath): 
                    files = file_utils.list_file(mediaPath)

                    if files: 
                        if any(os.path.splitext(a)[-1] in movExts for a in files): 
                            if latest: 
                                versions = [re.findall(r'v\d{3}', a)[0] for a in files if re.findall(r'v\d{3}', a)]
                                if versions:
                                    highest = sorted(versions)[-1]
                                    pickFiles = [a for a in files if highest in a]
                                    pickFile = pickFiles[0] if pickFiles else files[-1]
                                    mov = '%s/%s' % (mediaPath, pickFile)
                                    latests.append(mov)
                            else: 
                                latests += ['%s/%s' % (mediaPath, a) for a in files if os.path.splitext(a)[-1] in movExts]

        return latests


    def playblast_selected(self, path): 
        if path:
            self.preview_label.loadMedia([path])
        
    def create_reference(self):
        path, relPath = self.get_rig()
        res = self.res()
        entity = self.entity.copy()
        entity.context.update(res=res, look=self.look())
        self.dccHook.create_reference(entity, relPath, material=True)

    def create_gpu(self):
        from rf_app.asm import asm_lib

        res = self.res()
        gpuFile, gpuRelFile = self.get_gpu()
        # create locator
        name = asm_lib.asm_name(self.entity.name)
        loc = asm_lib.MayaRepresentation(name, True)
        loc.set(gpuFile, self.entity, 'setdress')
        # build gpu
        loc.build('gpu', res)
        loc.select()

    def get_rig(self):
        res = self.res()
        path = utils.get_rig(self.entity, res=res, absPath=True)
        relPath = utils.get_rig(self.entity, res=res, absPath=False)
        return path, relPath

    def get_gpu(self):
        res = self.res()
        entity = self.entity.copy()

        entity.context.update(look=self.look())
        gpuFile = utils.get_gpu(entity, res=res, absPath=True)
        gpuRelFile = utils.get_gpu(entity, res=res, absPath=False)
        return gpuFile, gpuRelFile


    def look(self):
        return str(self.look_comboBox.currentText())

    def res(self):
        res = str(self.res_comboBox.currentText()) or 'md'
        return res


class Main(QtWidgets.QMainWindow):
    """docstring for Main"""
    def __init__(self, parent=None):
        super(Main, self).__init__(parent=parent)
        self.ui = AssetInfoWidget(self)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)

def show():
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = Main(maya_win.getMayaWindow())
    myApp.show()
    return myApp
