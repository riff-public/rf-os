import os
import sys

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

import file_widget
from rf_utils import file_utils
from rf_utils import publish_info
from rf_utils.context import context_info


class InputWidgetUI(QtWidgets.QWidget):
    """docstring for InputWidgetUI"""
    currentIndexChanged = QtCore.Signal(str)
    def __init__(self, parent=None):
        super(InputWidgetUI, self).__init__(parent=parent)
        self.allLayout = QtWidgets.QVBoxLayout()

        # header
        self.headerLayout = QtWidgets.QHBoxLayout()

        self.header_layout()
        self.filter_layout()
        self.center_layout()
        self.button_layout()
        self.set_all_layout()

    def header_layout(self):
        # root
        self.rootLabel = QtWidgets.QLabel('Variant')
        self.variantComboBox = QtWidgets.QComboBox()

        self.label = QtWidgets.QLabel('Department')
        self.stepComboBox = QtWidgets.QComboBox()
        self.line = self.line_widget()

        self.headerLayout.addWidget(self.rootLabel)
        self.headerLayout.addWidget(self.variantComboBox)

        self.headerLayout.addWidget(self.label)
        self.headerLayout.addWidget(self.stepComboBox)
        self.headerLayout.addWidget(self.line)

        # radioButton
        self.versionRadioButton = QtWidgets.QRadioButton('Version')
        self.versionRadioButton.setChecked(False)
        self.heroRadioButton = QtWidgets.QRadioButton('Hero')
        self.heroRadioButton.setChecked(True)
        self.latestVersionCheckBox = QtWidgets.QCheckBox('Latest version')
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.headerLayout.addWidget(self.versionRadioButton)
        self.headerLayout.addWidget(self.heroRadioButton)
        self.headerLayout.addWidget(self.latestVersionCheckBox)
        self.headerLayout.addItem(spacerItem)

        # line
        self.hline = self.hline_widget()

        self.allLayout.addLayout(self.headerLayout)
        self.allLayout.addWidget(self.hline)

    def filter_layout(self):
        self.filterLayout = QtWidgets.QHBoxLayout()
        self.resLabel = QtWidgets.QLabel('Res: ')
        self.resLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.resComboBox = QtWidgets.QComboBox()
        self.processLabel = QtWidgets.QLabel('Process: ')
        self.processLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.processComboBox = QtWidgets.QComboBox()
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.filterLayout.addItem(spacerItem)
        self.filterLayout.addWidget(self.resLabel)
        self.filterLayout.addWidget(self.resComboBox)
        self.filterLayout.addWidget(self.processLabel)
        self.filterLayout.addWidget(self.processComboBox)

        self.filterLayout.setStretch(0, 8)
        self.filterLayout.setStretch(1, 1)
        self.filterLayout.setStretch(2, 3)
        self.filterLayout.setStretch(3, 1)
        self.filterLayout.setStretch(4, 3)

        self.allLayout.addLayout(self.filterLayout)

    def center_layout(self):
        # list widget
        self.centerLayout = QtWidgets.QHBoxLayout()
        self.fileWidget = file_widget.FileListWidget()
        self.imgWidget = file_widget.ImageListWidget()

        self.centerLayout.addWidget(self.imgWidget)
        self.centerLayout.addWidget(self.fileWidget)
        self.centerLayout.setStretch(0, 3)
        self.centerLayout.setStretch(1, 4)

        self.allLayout.addLayout(self.centerLayout)

    def button_layout(self):
        # button layout
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.buttonLayout = QtWidgets.QHBoxLayout()

        # radioButtons
        self.frame = QtWidgets.QFrame()
        self.importRadioButton = QtWidgets.QRadioButton('Import')
        self.referenceRadioButton = QtWidgets.QRadioButton('Reference')
        self.importMtrRadioButton = QtWidgets.QRadioButton('Import Material')
        self.importRadioButton.setChecked(True)

        self.frameLayout = QtWidgets.QHBoxLayout(self.frame)
        self.frameLayout.addWidget(self.importRadioButton)
        self.frameLayout.addWidget(self.referenceRadioButton)
        self.frameLayout.addWidget(self.importMtrRadioButton)

        # button
        self.actionButton = QtWidgets.QPushButton('Import')
        self.actionButton.setMinimumSize(QtCore.QSize(0, 30))

        # button layout
        self.buttonLayout.addWidget(self.frame)
        self.buttonLayout.addWidget(self.actionButton)

        self.buttonLayout.setStretch(0, 1)
        self.buttonLayout.setStretch(1, 1)
        self.buttonLayout.setStretch(2, 1)
        self.buttonLayout.setStretch(3, 2)

        self.allLayout.addLayout(self.buttonLayout)

    def set_all_layout(self):
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.allLayout)

    def line_widget(self, parent=None):
        line = QtWidgets.QFrame(parent)
        line.setFrameShape(QtWidgets.QFrame.VLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        return line

    def hline_widget(self, parent=None):
        line = QtWidgets.QFrame(parent)
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        return line

class InputWidget(InputWidgetUI):
    """docstring for InputWidget"""
    def __init__(self, dccHook=None, parent=None):
        super(InputWidget, self).__init__(parent=parent)
        self.importFile = 'Import'
        self.referenceFile = 'Reference'
        self.importMtr = 'ImportMtr'
        self.action = self.importFile
        self.dccHook = dccHook
        self.addKey = '-- show all --'

        self.init_signals()

    def init_signals(self):
        self.variantComboBox.currentIndexChanged.connect(self.variant_signal)
        self.stepComboBox.currentIndexChanged.connect(self.step_signal)

        self.latestVersionCheckBox.stateChanged.connect(self.list_output)
        self.versionRadioButton.clicked.connect(self.list_output)
        self.heroRadioButton.clicked.connect(self.list_output)
        self.importRadioButton.clicked.connect(self.set_action)
        self.referenceRadioButton.clicked.connect(self.set_action)
        self.importMtrRadioButton.clicked.connect(self.set_action)
        self.actionButton.clicked.connect(self.button_action)

        # filter signals
        self.resComboBox.currentIndexChanged.connect(self.filter_output)
        self.processComboBox.currentIndexChanged.connect(self.filter_output)

    def update_input(self, ContextPathInfo):
        """ run here to activate ui """
        self.entity = ContextPathInfo
        self.populate_ui()

    def populate_ui(self):
        self.list_variant()
        self.set_ui_signal()

    def set_ui_signal(self):
        self.versionRadioButton.clicked.connect(self.version_signal)
        self.heroRadioButton.clicked.connect(self.version_signal)

    def version_signal(self):
        versionStatus = self.versionRadioButton.isChecked()
        heroStatus = not versionStatus

        self.latestVersionCheckBox.setVisible(versionStatus)

    def variant_signal(self):
        variant = str(self.variantComboBox.currentText())
        self.entity.context.update(entity=variant)
        self.list_step()

    def step_signal(self):
        """ trigger event for step changed """
        step = str(self.stepComboBox.currentText())
        self.entity.context.update(step=step)
        self.list_output()

    def set_action(self):
        # query state
        importAction = self.importRadioButton.isChecked()
        referenceAction = self.referenceRadioButton.isChecked()
        importMtrAction = self.importMtrRadioButton.isChecked()

        # set label
        if importAction:
            action = self.importFile
        if referenceAction:
            action = self.referenceFile
        if importMtrAction:
            action = self.importMtr

        self.actionButton.setText(action)
        self.action = action

    def button_action(self):
        path = self.fileWidget.selected_file()

        if self.action == self.importFile:
            self.dccHook.import_file(path)

        if self.action == self.referenceFile:
            namespace = self.entity.name
            self.dccHook.reference_file(namespace, path)

        if self.action == self.importMtr:
            self.dccHook.import_mtr(path)

    def list_variant(self):
        self.variantComboBox.blockSignals(True)
        self.variantComboBox.clear()
        # variant = self.entity.list_variant()
        variant = [self.entity.name]
        index = variant.index(self.entity.name)
        self.variantComboBox.addItems(variant)
        self.variantComboBox.setCurrentIndex(index)
        self.list_step()
        self.variantComboBox.blockSignals(False)


    def list_step(self):
        default = '-- No Data --'

        # use keys from _data
        publishInfo = publish_info.RegisterAsset(self.entity)
        latestData = publishInfo.read_data()
        steps = latestData.keys()

        self.stepComboBox.clear()
        if steps:
            self.stepComboBox.addItems(steps)
        else:
            self.stepComboBox.addItem(default)

    def list_output(self):
        """ list all output process """
        allOutputs, allImgOutputs = self.all_output()
        self.set_res(allOutputs)
        self.set_process(allOutputs)


    def filter_output(self):
        res = str(self.resComboBox.currentText())
        process = str(self.processComboBox.currentText())
        allOutputs, allImgOutputs = self.all_output()

        res = res if not res == self.addKey else ''
        process = process if not process == self.addKey else ''
        filterOutputs = [a for a in allOutputs if res in os.path.basename(a) and process in os.path.basename(a)]

        self.fileWidget.clear()
        self.imgWidget.clear()
        self.fileWidget.add_files(filterOutputs)
        self.imgWidget.add_files(allImgOutputs)

    def all_output(self):
        # input UI
        step = str(self.stepComboBox.currentText())
        showLatest = self.latestVersionCheckBox.isChecked()
        showVersion = self.versionRadioButton.isChecked()

        allOutputs, allImgOutputs = self.get_outputs(step, showLatest, showVersion)
        return allOutputs, allImgOutputs

    def set_res(self, allOutputs):
        res = list(set([self.find_res(a) for a in allOutputs]))
        self.resComboBox.clear()
        self.resComboBox.addItem(self.addKey)
        self.resComboBox.addItems(res)

    def set_process(self, allOutputs):
        processes = list(set([self.find_process(a) for a in allOutputs]))
        self.processComboBox.clear()
        self.processComboBox.addItem(self.addKey)
        self.processComboBox.addItems(processes)

        # set for main
        if 'main' in processes:
            self.processComboBox.setCurrentIndex(processes.index('main')+1)

    def find_res(self, filename):
        # hard coded to 4th elements
        return filename.split('.')[0].split('_')[-1]

    def find_process(self, filename):
        # hard coded to 4th elements
        return filename.split('.')[0].split('_')[-2]

    def get_outputs(self, step, showLatest, showVersion):
        # update context
        self.entity.context.update(step=step)

        # get publish data
        publishInfo = publish_info.RegisterAsset(self.entity)
        latestData = publishInfo.read_data()
        allData = publishInfo.read_all_data()

        datas = [latestData] if showLatest else allData


        allOutputs = []
        allImgOutputs = []
        # get output and publish path
        for data in datas:
            if step in data.keys():
                processes = data[step].keys()
                for process in processes:
                    versionPath = data[step][process][publish_info.Data.publishVersion]
                    outputPath = data[step][process][publish_info.Data.publishOutput]
                    heroOutputPath = data[step][process][publish_info.Data.heroOutput]
                    viewOutput = outputPath if showVersion else heroOutputPath

                    # file output
                    outputPathAbs = context_info.RootPath(viewOutput).abs_path()
                    outputFiles = file_utils.listFile(outputPathAbs)
                    outputFiles = [('/').join([outputPathAbs, a]) for a in outputFiles]

                    for output in outputFiles:
                        if not output in allOutputs:
                            allOutputs.append(output)

                    # img output
                    outputImgPath = data[step][process][publish_info.Data.outputImg]
                    outputImgPathAbs = context_info.RootPath(outputImgPath).abs_path()
                    imgOutputs = file_utils.listFile(outputImgPathAbs)
                    imgOutputs = [('/').join([outputImgPathAbs, a]) for a in imgOutputs]

                    for output in imgOutputs:
                        if not output in allImgOutputs:
                            allImgOutputs.append(output)

        return allOutputs, allImgOutputs

