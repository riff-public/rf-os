import sys
import os
import re
import platform
import logging
from collections import OrderedDict

logger = logging.getLogger(__name__)

import rf_config as config
from shotgun_api3 import Shotgun
from datetime import datetime
from rf_utils import custom_exception

# connection to server
server = config.Shotgun.server
script = config.Shotgun.script
id = config.Shotgun.id
sg = Shotgun(server, script, id)
# sg = None

def init_key(inputKey):
    script, id = config.get_key(inputKey)
    global sg
    sg = Shotgun(server, script, id)



class Config: 
	""" config for shotgun value """ 
	shotValue = ['project', 'code', 'id', 'sg_elements', 'sg_shot', 'assets', 'sg_shortcode']
	mapKey = {
				'hero': 'sg_file_path', 
				'publishedFile': 'sg_published_path', 
				'filetype': 'sg_type', 
				'version': 'sg_version', 
				'asset': 'sg_asset', 
				'step': 'sg_step', 
				'namespace': 'sg_namespace', 
				'exportNamespace': 'sg_export_namespace', 
				'status': 'sg_status_list', 
				'project': 'project', 
				'nsKey': 'code', 
				'lock': 'sg_lockdown', 
				'owner': 'sg_owner.HumanUser.sg_name', 
				'workspace': 'sg_workspace', 
				'meta': 'sg_meta'
			}

	taskMap = {
				'anim': 'anim_cache', 
				'sim': 'sim_cache', 
				'setDress': 'setDress_cache'
			}

	assetValue = ['code', 'id']
	lock = 'lock'
	release = 'release'
	lockStatus = {'lock': lock, 'cmpt': release}
	sgStatus = {lock: 'lock', release: 'cmpt'}
	historyHeaders = ['date', 'user', 'status', 'workspace', 'type']


class Cache: 
	""" caching variable """ 
	shotEntity = dict() 
	projectEntity = None
	elemDict = dict()
	assetEntity = dict()
	stepEntity = dict()
	taskEntity = dict()


class ShotElement(object):
	"""Shot elements API"""
	def __init__(self, entity):
		super(ShotElement, self).__init__()
		self.entity = entity
		self.taskEntities = None
		self.shotEntity = get_shot_entity(self.entity.project, self.entity.name)


	def add(self, nsKey, assetName='', step='', **kwargs): 
		elem = Element(self, self.entity, nsKey)
		elem.add(assetName, step, **kwargs)
		return elem

	def read(self, nsKey): 
		elem = Element(self, self.entity, nsKey)
		return elem 

	def list_keys(self): 
		if not self.taskEntities: 
			self.taskEntities = find_element(self.entity.project, self.entity.name)
		return [a['entity']['name'] for a in self.taskEntities]

	def list_elements(self): 
		if not self.taskEntities: 
			self.taskEntities = find_element(self.entity.project, self.entity.name)
		return self.taskEntities

	def list_lock(self, step=None): 
		step = self.entity.step if not step else step 
		nsKeys = []
		if not self.taskEntities: 
			 self.taskEntities = find_element(self.entity.project, self.entity.name)

		for task in self.taskEntities: 
			lock = task['sg_lockdown']
			key = task['entity']['name']
			taskStep = task['step']['name']

			if step.lower() == taskStep.lower(): 
				if lock: 
					nsKeys.append(key)
		return nsKeys

class Element(object):
	"""docstring for Element"""
	def __init__(self, main, entity, nsKey):
		super(Element, self).__init__()
		self.main = main
		self.entity = entity 
		self.nsKey = nsKey
		self.elemEntity = get_element_entity(self.entity.project, nsKey, self.entity.name) 
		self.message = str()

	def add(self, assetName='', step='', **kwargs): 
		nsKey = self.nsKey
		data = {'code': nsKey, 'sg_shot': self.main.shotEntity}
		if assetName: 
			assetEntity = get_asset_entity(self.entity.project, assetName)
			data.update({'sg_asset': assetEntity})
		if step: 
			data.update({'sg_step': step})
		
		taskData = dict()
		taskFields = ['status']

		for k, v in kwargs.iteritems(): 
			if k in Config.mapKey.keys(): 
				sgKey = Config.mapKey[k]
				if not k in taskFields: 
					data.update({sgKey: v})
				else: 
					taskData.update({sgKey: v})

			else: 
				logger.warning('Key "{}"" not exists, add value skipped'.format(k))

		# remove status 
		if not self.elemEntity: 
			self.elemEntity = create_element(self.entity.project, nsKey, self.entity.name, data=data)
			logger.info('Element "{}" created'.format(self.nsKey))
			logger.debug(kwargs)

		if self.elemEntity: 
			self.elemEntity = update_element(self.elemEntity, data=data)
			logger.info('Element "{}" updated'.format(self.elemEntity['code']))
			logger.info(self.elemEntity)
		
		if taskData: 
			if step: 
				status = taskData.get('sg_status_list')
				taskName = Config.taskMap.get(step)
				if taskName: 
					# taskEntity = get_task_entity(self.entity.project, nsKey, self.entity.name, step, Config.taskMap[step])
					update_task_status(self.entity.project, nsKey, self.entity.name, step, taskName, status)

				else: 
					logger.warning('Step "{}" not allowed'.format(step))
			else: 
				logger.warning('step require to set a task status')


	def read(self): 
		self.elemEntity = get_element_entity(self.entity.project, self.nsKey, self.entity.name)

	def update(self, elemEntity): 
		for k, v in elemEntity.items(): 
			self.elemEntity[k] = v 

	@property 
	def name(self): 
		return self.elemEntity.get('code')

	@property 
	def namespace(self): 
		return self.elemEntity.get(Config.mapKey['namespace'])

	@property
	def hero(self): 
		return self.elemEntity.get(Config.mapKey['hero'])

	@property 
	def filetype(self): 
		return self.elemEntity.get(Config.mapKey['filetype'])

	@property 
	def version(self): 
		return self.elemEntity.get(Config.mapKey['version'])

	@property 
	def step(self): 
		return self.elemEntity.get(Config.mapKey['step'])

	@property 
	def exportNamespace(self): 
		return self.elemEntity.get(Config.mapKey['exportNamespace'])

	@property 
	def status(self): 
		return self.elemEntity.get(Config.mapKey['status'])

	@property 
	def asset(self): 
		return self.elemEntity.get(Config.mapKey['asset'])

	@property 
	def owner(self): 
		user = self.elemEntity.get(Config.mapKey['owner'])
		return user if user else ''

	@property 
	def workspace(self): 
		return self.elemEntity.get(Config.mapKey['workspace'])

	@property 
	def meta(self): 
		meta = self.elemEntity.get(Config.mapKey['meta']) 
		return eval(meta) if meta else list()

	def task(self, step): 
		return Task(self.main, self.entity, self.nsKey, step)

	def set_task(self, step, **kwargs): 
		task = Task(self.main, self.entity, self.nsKey, step)
		task.set_attr(**kwargs)
		return task

	def is_lock(self): 
		status = self.status
		if status in Config.lockStatus.keys(): 
			if Config.lockStatus[status] == Config.lock: 
				return True 
			if Config.lockStatus[status] == Config.release: 
				return False 
		else: 
			return False

	def is_owner(self): 
		user_entity = self.get_user(user='')
		return True if user_entity.name() == self.owner else False

	def lock(self, user='', workspace='', force=False): 
		valid = False if self.is_lock() else True  
		user_entity = self.get_user(user)

		if user_entity: 
			if valid: 
				meta = self.generate_meta(user_entity.name(), Config.lock, force, workspace)
				data = {
					'sg_owner': user_entity.sg_user(), 
					'sg_workspace': workspace, 
					'sg_status_list': Config.lock, 
					'sg_meta': meta}
				self.elemEntity = update_element(self.elemEntity, data=data)
				return self.elemEntity
				
			else: 
				if force: 
					result = self.release(user_entity, force=force)
					if result: 
						self.update(result)
						return self.lock(user_entity, workspace, force=False)
					else: 
						self.message = 'Faled to force unlock "{}"'.format(self.name)
						logger.error(self.message)
					return False
				self.message = '"{}" is locked. PLease ask owner "{}" to release this task'.format(self.name, self.owner)
				logger.warning(self.message)
				return False
		else: 
			self.message = 'No user "{}" found. Cannot lock "{}"'.format(user, self.name)
			logger.error(self.message)

	def release(self, user='', force=False): 
		user_entity = self.get_user(user)
		valid = True if self.owner == user_entity.name() else False
		valid = True if force else valid
		if valid: 
			meta = self.generate_meta(user_entity.name(), Config.release, force)
			data = {
					'sg_owner': user_entity.sg_user(), 
					'sg_status_list': Config.sgStatus[Config.release], 
					'sg_meta': meta}
			self.elemEntity = update_element(self.elemEntity, data=data)
			return self.elemEntity

		else: 
			self.message = 'Owner is "{}". You cannot release "{}"'.format(self.owner, self.name)
			logger.warning(self.message)

	def generate_meta(self, user, status, force, workspace=''): 
		from datetime import datetime
		key = datetime.now().strftime('%y-%m-%d %H:%M:%S')
		meta = self.meta
		meta.append({'date': key, 'user': user, 'status': status, 'workspace': workspace, 'force': force})
		return str(meta) 

	def get_user(self, user): 
		from rf_utils import user_info
		if user: 
			if not isinstance(user, user_info.User): 
				user_entity = user_info.User(sg=sg, nickname=user)
			else: 
				user_entity = user
		else: 
			user_entity = user_info.User(sg=sg)
		return user_entity




class Task(object):
	"""docstring for Task"""
	def __init__(self, main, entity, nsKey, step):
		super(Task, self).__init__()
		self.main = main
		self.entity = entity 
		self.nsKey = nsKey
		self.step = step
		self.taskEntity = self.get_task_entity()

	def get_task_entity(self): 
		taskName = Config.taskMap.get(self.step)
		if taskName: 
			return get_task_entity(self.entity.project, self.nsKey, self.entity.name, self.step, taskName)
		else: 
			logger.error('No task related to this step')
		return None

	def set_attr(self, **kwargs): 
		if self.taskEntity: 
			data = dict()
			for k, v in kwargs.iteritems(): 
				data.update({k: v})
			result = sg.update('Task', self.taskEntity['id'], data)
			if result: 
				logger.info('Update task %s %s' % (self.taskEntity['content'], kwargs))
			else: 
				logger.error('Failed to update task %s %s' % (self.taskEntity['content'], kwargs))

		return None


def update_sg_reg(scene, nsKey, assetName, step, namespace='', exportNamespace='', filetype='', heroFile='', publishedFile='', version='', status=''): 
	shotElm = ShotElement(scene)
	elem = shotElm.add(nsKey, 
						assetName=assetName, step=step, 
						hero=heroFile, 
						filetype=filetype, 
						version=version, 
						namespace=namespace, 
						exportNamespace=exportNamespace, 
						status=status
						)

	return elem


def get_shot_entity(project, shotName): 
	""" find shot entity """ 
	# using cache if available
	if not Cache.shotEntity: 
		logger.debug('Fetching Shot from shotgun ...')
		filters = [['project.Project.name', 'is', project], ['code', 'is', shotName], ['sg_shot_type', 'is', 'Shot']]
		fields = Config.shotValue
		result = sg.find_one('Shot', filters, fields)
		if result: 
			Cache.shotEntity = result
		else: 
			logger.error('"%s" not found in Shotgun' % shotName)

	else: 
		logger.debug('Using Shot cache')

	Cache.projectEntity = Cache.shotEntity.get('project')
	return Cache.shotEntity


def get_asset_entity(project, assetName): 
	""" get asset entity """ 
	if not Cache.assetEntity.get(assetName): 
		logger.debug('Fetching Asset from shotgun ...')
		filters = [['project.Project.name', 'is', project], ['code', 'is', assetName]]
		assetEntity = sg.find_one('Asset', filters, Config.assetValue)
		Cache.assetEntity[assetName] = assetEntity

	else: 
		logger.debug('Using asset cache')

	return Cache.assetEntity[assetName]


def get_project_entity(project): 
	if not Cache.projectEntity: 
		filters = [['name', 'is', project]]
		fields = ['name', 'id']
		Cache.projectEntity = sg.find_one('Project', filters, fields)

	return Cache.projectEntity


def get_element_entity(project, nsKey, shotName): 
	# try to find cache 
	projectEntity = get_project_entity(project)
	projectDict = Cache.elemDict.get(project, dict())
	shotDict = projectDict.get(shotName, dict())
	elemEntity = shotDict.get(nsKey, dict())

	if not elemEntity: 
		logger.debug('Fetching element from sg ...')
		# find shot
		shotEntity = get_shot_entity(project, shotName)

		# find if exists 
		filters = [['project', 'is', projectEntity], ['code', 'is', nsKey], ['sg_shot', 'is', shotEntity]]
		fields = [v for k, v in Config.mapKey.iteritems()]
		elemEntity = sg.find_one('CustomEntity09', filters, fields) or dict()
		add_to_cache(project, shotName, nsKey, elemEntity)

	else: 
		logger.debug('Using element cache ...')

	return elemEntity


def get_task_entity(project, nsKey, shotName, step, taskName): 
	elemEntity = get_element_entity(project, nsKey, shotName)
	projectEntity = get_project_entity(project)

	filters = [['project', 'is', projectEntity], ['step.Step.code', 'is', step], ['content', 'is', taskName], ['entity', 'is', elemEntity]]
	fields = ['sg_status_list', 'content', 'id']
	result = sg.find_one('Task', filters, fields)
	return result


def get_step_entity(step): 
	if not Cache.stepEntity.get(step): 
		stepEntity = sg.find_one('Step', [['code', 'is', step], ['entity_type', 'is', 'CustomEntity09']], ['code', 'id'])
		if stepEntity: 
			Cache.stepEntity[step] = stepEntity

	return Cache.stepEntity[step]

def add_to_cache(project, shotName, nsKey, elemEntity): 
	""" add element entity to cache """ 
	if project in Cache.elemDict.keys(): 
		if shotName in Cache.elemDict[project].keys(): 
			Cache.elemDict[project][shotName].update({nsKey: elemEntity})
		else: 
			Cache.elemDict[project].update({shotName: elemEntity})
	else: 
		Cache.elemDict[project] = {shotName: {nsKey: elemEntity}}
	return elemEntity


def create_element(project, nsKey, shotName, data=None): 
	# find if exists 
	elemEntity = get_element_entity(project, nsKey, shotName)
	projectEntity = get_project_entity(project)
	
	# element data 
	data = data if data else dict()
	data.update({'project': projectEntity, 'code': nsKey})

	# create 
	if not elemEntity: 
		logger.debug('Create element')
		elemEntity = sg.create('CustomEntity09', data)
		return elemEntity

	# update 
	result = sg.update('CustomEntity09', elemEntity['id'], data)
	return result


def update_element(elemEntity, data=None): 
	""" update element """ 
	data = data if data else dict()
	data.update({'project': elemEntity['project'], 'code': elemEntity['code']})
	result = sg.update('CustomEntity09', elemEntity['id'], data)
	return result


def update_task_status(project, nsKey, shotName, step, taskName, status): 
	taskEntity = get_task_entity(project, nsKey, shotName, step, taskName)

	if not taskEntity: 
		projectEntity = get_project_entity(project)
		elemEntity = get_element_entity(project, nsKey, shotName)
		stepEntity = get_step_entity(step)
		if elemEntity and stepEntity: 
			data = {'project': projectEntity, 'entity': elemEntity, 'content': taskName, 'step': stepEntity, 'sg_status_list': status}
			taskEntity = sg.create('Task', data)
			return taskEntity
		else: 
			if not elemEntity: 
				logger.warning('element not found')
			if not stepEntity: 
				logger.warning('department not found')
			logger.warning('Skip updating status')

	data = {'sg_status_list': status}
	sg.update('Task', taskEntity['id'], data)

	return taskEntity


def find_element(project, shotName): 
	filters = [['project.Project.name', 'is', project], ['entity.CustomEntity09.sg_shot.Shot.code', 'is', shotName]]
	fields = ['sg_lockdown', 'content', 'entity', 'step', 'entity.CustomEntity09.sg_type']
	result = sg.find('Task', filters, fields)
	return result


def clear_cache(): 
	Cache.shotEntity = dict() 
	Cache.projectEntity = None
	Cache.elemDict = dict()
	Cache.assetEntity = dict()
	Cache.stepEntity = dict()
	Cache.taskEntity = dict()


def get_task_versions(project, entity_name, entity_type, taskname): 
	filters = [
		['project.Project.name', 'is', project], 
		['entity.{}.code'.format(entity_type), 'is', entity_name], 
		['sg_task.Task.content', 'is', taskname]]

	fields = ['sg_version_type', 'code']
	versions = sg.find('Version', filters, fields)
	vers = list()

	if versions: 
		for version in versions: 
			match_vers = re.findall(r'v\d{3}', version['code'])
			v = match_vers[0] if match_vers else None 
			vers.append(v) if v else None 

		max_version = sorted(vers)[-1].replace('v', '')
		next_version = int(max_version) + 1 
	else: 
		next_version = 1
	str_version = 'v%03d' % next_version
	return str_version
	

# examples 
""" 
shotElm = ShotElem(scene)
shotElm = ShotElem('SevenChickMovie', 'scm_act1_q0050_s0010')

shotElm.add('arthur_001', step='anim', version='v001', path=path)
shotElem.update('arthuor_001', status='apv')

namespaces = shotElm.list_namespace()

for ns in namespaces: 
	shotElm(ns, step='anim')
	----------------------------
	ns.update(step='anim')

================================================

from rf_utils import register_sg
reload(register_sg)
from rf_utils.context import context_info
path = 'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0050_s0010/layout/output/arthur_001/hero/scm_act1_q0050_s0010_layout_arthur_001.hero.ma'
entity = context_info.ContextPathInfo(path=path)

shotElm = register_sg.ShotElement(entity)
shotElm.add('arthur_004', step='anim')


=================================================

from rf_utils import register_sg
reload(register_sg)
from rf_utils.context import context_info
path = 'P:/SevenChickMovie/scene/publ/act1/scm_act1_q0050_s0010/layout/output/arthur_001/hero/scm_act1_q0050_s0010_layout_arthur_001.hero.ma'
entity = context_info.ContextPathInfo(path=path)

shotElm = register_sg.ShotElement(entity)
elem = shotElm.add('arthur_002')
elem.add(step='anim', status='apr', hero='P:/')
elem.add(step='anim', exportNamespace='aa', namespace='aa')
elem.step

=================================================



ns = shotElm.read('arthur_001')
ns.type
ns.version
ns.path 
ns.hero
ns.node 
ns.namespace 

---------------------------------------------------------------
shotElm = ShotElem('SevenChickMovie', 'scm_act1_q0050_s0010')
ns = shotElm.add('arthur_001')
ns.update(step='anim', version='v001', path=path)

ns.read(step=True)
---------------------------------------------------------------

"""

# arthur_001:
#     type: abc_cache
#     namespace: arthur_001
#     exportNamespace: arthur_001
#     step: anim
#     version: v001
#     node: Geo_Grp
#     description: P:/SevenChickMovie/asset/publ/char/arthur/_data/asmData/arthur_ad.hero.yml
#     task: anim
#     workspace:
#       publishedFile: P:/SevenChickMovie/scene/publ/act1/scm_act1_q0050_s0030/anim/main/v001/maya/scm_act1_q0050_s0030_anim_main.v001.ma
#       heroFile: P:/SevenChickMovie/scene/publ/act1/scm_act1_q0050_s0030/anim/main/hero/maya/scm_act1_q0050_s0030_anim_main.hero.ma
#     maya:
#       publishedFile: R:/SevenChickMovie/scene/publ/act1/scm_act1_q0050_s0030/layout/output/arthur_001/v001/scm_act1_q0050_s0030_layout_arthur_001.v001.ma
#       heroFile: P:/SevenChickMovie/scene/publ/act1/scm_act1_q0050_s0030/layout/output/arthur_001/hero/scm_act1_q0050_s0030_layout_arthur_001.hero.ma
#     cache:
#       publishedFile: R:/SevenChickMovie/scene/publ/act1/scm_act1_q0050_s0030/anim/output/arthur_001/v001/scm_act1_q0050_s0030_anim_arthur_001.v001.abc
#       heroFile: P:/SevenChickMovie/scene/publ/act1/scm_act1_q0050_s0030/output/arthur_001/scm_act1_q0050_s0030_anim_arthur_001.hero.abc