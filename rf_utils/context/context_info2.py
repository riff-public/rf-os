# use path to for contex info
import sys 
import os 
from collections import OrderedDict
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import rf_config as config 
from rf_utils import project_info
from rf_utils import file_utils

RFPROJECT = config.Env.projectVar
RFPUBL = config.Env.publVar
RFUSER = config.Env.userVar

RFPROJECT_VAR = '$%s' % RFPROJECT
RFPUBL_VAR = '$%s' % RFPUBL

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
configFile = 'context_config.yml'

if config.isMaya: 
    import maya.cmds as mc 


class ContextKey: 
    # context key
    project = 'project'
    entityType = 'entityType'
    entityGrp = 'entityGrp'
    entityParent = 'entityParent'
    entity = 'entity'
    step = 'step'
    process = 'process'
    version = 'version'
    app = 'app'
    workspace = 'workspace'
    work = 'work'
    publish = 'publish'
    hero = 'hero'
    task = 'task'
    workfile = 'workfile'
    res = 'res'
    asset = 'asset'
    scene = 'scene'

    # context attr 
    static = 'static'
    dynamic = 'dynamic'
    template = 'template'
    dynamicGrp = 'dynamic-group'
    group = 'group'


class Context(object):
    """docstring for Context"""
    def __init__(self, path='', **kwargs):
        super(Context, self).__init__()
        self.contextArgs = kwargs
        self.path = path
        self.sepkey = read_config().get('separator')
        self.format = read_config().get('format')
        self.noContext = read_config().get('noContext')
        self.context = self.construct_context(self.contextArgs)
        self.contextBk = self.construct_context(self.contextArgs)

    def __str__(self):
        return self.context

    def __repr__(self):
        return self.context

    @property 
    def str(self): 
        return self.context

    def construct_context(self, kwargs): 
        # construct context 
        # context = context_info.Context(project='Two_Heroes', entityType='asset', entityGrp='char', entityParent='sunny', entity='sunnyBase', step='model', process='main', version='v001', workspace='work', task='model_md')

        contextElems = []
        contextFormats = get_keys(self.format)
        
        # read format context 
        for key in contextFormats: 
            contextElems.append(kwargs.get(key, self.noContext))

        return (self.sepkey).join(contextElems)

    def update(self, **contextArgs): 
        """ update context """ 
        for k, v in contextArgs.iteritems(): 
            self.contextArgs.update({k: v})

        self.context = self.construct_context(self.contextArgs)

    def restore(self): 
        """ restore original context value """ 
        self.context = self.contextBk


    @property
    def elems(self): 
        """ split context elements """ 
        return self.context.split(self.sepkey)

        

class PathToContext(object):
    """Convert path input args to context"""
    def __init__(self, project=None, path=None, task=str(), res=str(), **kwargs):
        super(PathToContext, self).__init__()
        self.inputPath = path
        
        # get path 
        if config.isMaya and not self.inputPath: 
            self.inputPath = mc.file(q=True, loc=True) if not mc.file(q=True, loc=True) == 'unknown' else (mc.file(q=True, sn=True) or str())
        
        # project information 
        if not project: 
            project = self.guess_project()
            logger.info('Guessing project -> %s' % project)

        self.projectInfo = project_info.ProjectInfo(project)
        self.configData = self.projectInfo.config_data()

        # env 
        projectEnv = self.projectInfo.env()
        self.rootwork = projectEnv.get(RFPROJECT)
        self.rootpubl = projectEnv.get(RFPUBL)

        self.task = task
        self.res = res
        self.path = self.strip_root()

        # path elements 
        self.schema = self.configData.get('schema')
        self.contextKey = self.schema.get('contextKey')
        self.asset = self.contextKey.get('asset').get('key')
        self.scene = self.contextKey.get('scene').get('key')
        self.pathElems = self.path.split('/')
        self.elemCount = len(self.pathElems) - 1

        # schema config 
        self.schemaConfig = self.schema.get('pathLevel').get('assetWorkPath') # assume this is a work file / asset or scene share the same schema
        
        # version 
        self.versionKey = self.configData.get('workfile').get('versionFormat').get('prefix', 'v')
        self.versionPadding = self.configData.get('workfile').get('versionFormat').get('padding', 3)
        self.versionSep = self.configData.get('workfile').get('versionFormat').get('separator', '.')

        # context 
        self.noContext = read_config().get('noContext')


    def __str__(self):
        return ''

    def __repr__(self):
        return ''

    def guess_project(self): 
        if self.inputPath: 
            allProjects = project_info.ProjectInfo().list_all(asObject=False)
            result = [a for a in self.inputPath.split('/') if a in allProjects]

            if result: 
                project = result[0]
                return project

    @property
    def valid_root(self): 
        """ check if root on the path """ 
        return True if self.rootwork in self.inputPath or self.rootpubl in self.inputPath else False 

    @property
    def valid_env(self): 
        """ check if env match config """ 
        return True if self.rootwork == os.environ.get(RFPROJECT) and self.rootpubl == os.environ.get(RFPUBL) else False

    def strip_root(self): 
        # strip rootwork first if not found strip rootpubl 
        if self.valid_root: 
            replaceRoot = self.rootwork if self.rootwork in self.inputPath else self.rootpubl if self.rootpubl in self.inputPath else ''
            replaceRoot = '%s/' % replaceRoot if not replaceRoot[-1] == '/' else replaceRoot
            return self.inputPath.replace(replaceRoot, '')
        else: 
            logger.error('Root does not match current path : %s' % self.rootwork)
        return str()

    @property
    def entity_type(self): 
        """ return entity type if result matched config """ 
        entityType = self.get_value('entityType')
        return ContextKey.asset if entityType == self.asset else ContextKey.scene if entityType == self.scene else self.noContext

    @property
    def project(self): 
        """ return project """ 
        return self.get_value(ContextKey.project)

    @property
    def workspace(self): 
        """ return work or publ """ 
        return self.get_value(ContextKey.workspace)

    @property
    def entity_group(self): 
        """ return assetType or episode """ 
        return self.get_value(ContextKey.entityGrp)

    @property
    def entity_parent(self): 
        """ return asset parent or sequence """
        return self.get_value(ContextKey.entityParent)

    @property
    def entity_name(self): 
        """ return assetName or shotName """ 
        return self.get_value(ContextKey.entity)

    @property
    def step(self): 
        """ return department """ 
        return self.get_value(ContextKey.step)

    @property
    def process(self): 
        """ return process name """ 
        return self.get_value(ContextKey.process)

    @property
    def app(self): 
        """ return app name """ 
        return self.get_value(ContextKey.app)

    @property
    def workfile(self): 
        """ return workfile name """ 
        return self.get_value(ContextKey.workfile)

    @property
    def version(self): 
        """ get file version """ 
        workfile = self.workfile
        if workfile: 
            # find "v" by split and looking for 3 padding digits
            versionNumber = [a for a in workfile.split(self.versionKey) if a[0: self.versionPadding].isdigit()]
            return '%s%s' % (self.versionKey, versionNumber[0]) if versionNumber else self.noContext
        return self.noContext


    def context(self): 
        """ return context """
        context = Context(project=self.project, entityType=self.entity_type, entityGrp=self.entity_group, entityParent=self.entity_parent, entity=self.entity_name, step=self.step, process=self.process, version=self.version, workspace=self.workspace, task=self.task, res=self.res, app=self.app, workfile=self.workfile)
        return Info(context=context) if context else str()
    
    def get_value(self, key): 
        # get key value 
        schemaElements = get_keys(self.schemaConfig)
        # extract dictionary value to list 
        schemaElementData = [self.contextKey.get(a) for a in schemaElements]
        # get parent context or use normal contextKey for mapping
        nameMap = [a.get('parent') or a.get('key') for a in schemaElementData]
        level = nameMap.index(key) - 1
        return self.pathElems[level] if self.elemCount >= level else self.noContext

    def set_task(self, task): 
        """ set task information to context if not supply """ 
        self.task = task 
        return task 


class Info(object):
    """Receive context to process path"""
    def __init__(self, context=None, path=None):
        super(Info, self).__init__()
        self.context = context

        # if not context: 
        #     self.context = PathToContext(inputPath=path).context()

        self.pathSep = '/'
        self.contextElems = get_context_format()
        self.sepkey = read_config().get('separator')

        # project config 
        self.projectInfo = project_info.ProjectInfo(self.project)
        self.configData = self.projectInfo.config_data()
        self.schema = self.configData.get('schema').get('pathLevel')
        self.asset = self.configData.get('schema').get('contextKey').get('asset').get('key')
        self.scene = self.configData.get('schema').get('contextKey').get('scene').get('key')

        # workfile config 
        self.workfileConfig = self.configData.get('workfile')

        # root config 
        self.rootConfig = self.configData.get('env').get(RFPROJECT)
        self.publConfig = self.configData.get('env').get(RFPUBL)


    def __str__(self): 
        return str(self.context)

    def __repr__(self): 
        return str(self.context)

    def root(self, rel=True): 
        return '$%s' % RFPROJECT if rel else os.environ.get(RFPROJECT)

    def publ(self, rel=True): 
        return '$%s' % RFPUBL if rel else os.environ.get(RFPUBL)
    
    @property
    def project(self): 
        """ get project from context """ 
        return self.get_value(ContextKey.project)

    @property
    def entity_type(self): 
        """ get entity type - asset or scene """ 
        return self.get_value(ContextKey.entityType)

    @property
    def entity_group(self): 
        """ get entity_group from context """ 
        return self.get_value(ContextKey.entityGrp)

    @property
    def entity_parent(self): 
        """ get entity_parent from context """ 
        return self.get_value(ContextKey.entityParent)

    @property
    def entity_name(self): 
        """ get entity name from context """ 
        return self.get_value(ContextKey.entity)

    @property
    def step(self): 
        """ get step from context """ 
        return self.get_value(ContextKey.step)

    @property
    def process(self): 
        """ get process from context """ 
        return self.get_value(ContextKey.process)

    @property
    def app(self): 
        """ get app from context """ 
        return self.get_value(ContextKey.app)

    @property
    def version(self): 
        """ get file version from context """ 
        return self.get_value(ContextKey.version)

    @property
    def workspace(self): 
        """ get workspace from context """ 
        return self.get_value(ContextKey.workspace)

    @property
    def task(self): 
        """ get task from context """ 
        return self.get_value(ContextKey.task)

    @property
    def file(self, ext=True): 
        """ get filename """ 
        basename = self.get_value(ContextKey.workfile)
        if not ext: 
            return os.path.splitext(basename)[0]
        return basename

    @property
    def res(self): 
        """ get res from context """ 
        return self.get_value(ContextKey.res)


    @property
    def local_user(self): 
        return os.environ.get(RFUSER)

    def get_value(self, key): 
        index = self.contextElems.index(key)
        return self.context.elems[index]

    @property
    def contextMapValues(self): 
        """ map context value to dictionary """
        contextMap = OrderedDict()
        for i, key in enumerate(self.contextElems): 
            contextMap[key] = self.context.elems[i]
        return contextMap

    def type_path(self, workspace=ContextKey.work): 
        """ return type """ 
        pathSchema = self._get_schema(workspace=workspace)
        return self._combine_path(pathSchema, ContextKey.entityGrp)

    def episode_path(self, workspace=ContextKey.work): 
        """ return episode """ 
        pathSchema = self._get_schema(workspace=workspace)
        return self._combine_path(pathSchema, ContextKey.entityGrp)

    def parent_path(self, workspace=ContextKey.work): 
        """ return assetParent or sequence """ 
        pathSchema = self._get_schema(workspace=workspace)
        return self._combine_path(pathSchema, ContextKey.entityParent)

    def sequence_path(self, workspace=ContextKey.work): 
        """ return episode """ 
        pathSchema = self._get_schema(workspace=workspace)
        return self._combine_path(pathSchema, ContextKey.entityParent)

    def path(self, workspace=ContextKey.work): 
        """ get entity asset name or shot name """ 
        pathSchema = self._get_schema(workspace=workspace)
        return self._combine_path(pathSchema, ContextKey.entity)

    def step_path(self, step='', workspace=ContextKey.work): 
        """ get step path or replace with input step """ 
        pathSchema = self._get_schema(workspace=workspace)
        path = self._combine_path(pathSchema, ContextKey.step)
        return path.replace(self.step, step) if step else path

    def process_path(self, process='', workspace=ContextKey.work): 
        """ get process path or replace with input process """ 
        pathSchema = self._get_schema(workspace=workspace)
        path = self._combine_path(pathSchema, ContextKey.process)
        return path.replace(self.process, process) if process else path

    def app_path(self, app=''): 
        """ get app path or replace with input app """ 
        pathSchema = self._get_schema()
        path = self._combine_path(pathSchema, ContextKey.app)
        return path.replace(self.app, app) if app else path

    def work_path(self): 
        """ get full workspace path """ 
        pathSchema = self._get_schema()     
        return self._combine_path(pathSchema)

    # publish section 
    def output_path(self): 
        """ get output publish path """ 
        pathSchema = self._get_schema(ContextKey.publish)
        return self._combine_path(pathSchema)

    def publish_version_path(self): 
        """ get version publish path """ 
        pathSchema = self._get_schema(ContextKey.publish)
        return self._combine_path(pathSchema, ContextKey.version)

    # asset hero path 
    def hero_path(self): 
        """ return hero path """ 
        pathSchema = self._get_schema(ContextKey.hero)
        return self._combine_path(pathSchema)

    # file name
    def filename(self, user=False): 
        name = self._get_name_value(user=user)
        return name


    def _get_name_value(self, user=False): 
        """ get name value from workfile config """ 
        nameFormat = self.workfileConfig['format']['user'] if user else self.workfileConfig['format']['default']
        contextKey = self.workfileConfig.get('contextKey')
        keys = get_keys(nameFormat)
        name = nameFormat

        for key in keys: 
            element = contextKey.get(key)
            value = self._get_element_value(element)
            name = name.replace('<%s>' % key, value)

        if user: 
            replaceKey = self.workfileConfig.get('user').get('replace')
            name = name.replace(replaceKey, self.local_user)

        return self._get_ext(name)

    def _get_ext(self, name): 
        contextKey = self.workfileConfig.get('extFormat')
        replaceKey = contextKey.get('replace')
        ext = contextKey.get(self.workspace).get(self.app)
        return name.replace(replaceKey, ext)

    def _get_schema(self, workspace=ContextKey.work): 
        """ return full path schema for asset or scene """ 
        if workspace == ContextKey.work: 
            if self.entity_type == self.asset: 
                pathSchema = self.schema.get('assetWorkPath')
            if self.entity_type == self.scene: 
                pathSchema = self.schema.get('sceneWorkPath')
        if workspace == ContextKey.publish: 
            if self.entity_type == self.asset: 
                pathSchema = self.schema.get('assetPublPath')
            if self.entity_type == self.scene: 
                pathSchema = self.schema.get('scenePublPath')
        if workspace == ContextKey.hero: 
            if self.entity_type == self.asset: 
                pathSchema = self.schema.get('assetHeroPath')
        return self._translate_path_config(pathSchema)

    def _combine_path(self, pathSchema, stopKey=''): 
        """ combine path from schema config """ 
        targetIndex = [i for i, a in enumerate(pathSchema) if a.get('key') == stopKey]
        targetPath = pathSchema
        elements = []
        
        if targetIndex: 
            targetPath = pathSchema[0: targetIndex[0] + 1]

        for element in targetPath: 
            value = self._get_element_value(element)
            elements.append(value)

        joinPath = self.pathSep.join(elements)
        return RootPath(joinPath)

    def _get_element_value(self, element): 
        """ filter for static, dynamic, group and return value 
        static will use "name" key from schema config 
        dynamic will use context value 
        group will looking for child config """ 

        key = element.get('key')
        name = element.get('name')
        attr = element.get('attr')

        if attr == ContextKey.static: 
            value = name
        if attr == ContextKey.dynamic or attr == ContextKey.dynamicGrp: 
            value = self.contextMapValues.get(key)
        if attr == ContextKey.template: 
            value = self.contextMapValues.get(key)
        if attr == ContextKey.group: 
            element = name.get(self.contextMapValues.get(key))
            value = self._get_element_value(element)

        return value

    def _translate_path_config(self, pathConfig): 
        """ convert <item>/<item> format to data """ 
        separator = '/'
        elements = get_keys(pathConfig)
        contextKey = self.configData.get('schema').get('contextKey')
        translateData = []

        for element in elements: 
            translateData.append(contextKey.get(element))

        return translateData


class RootPath(object):
    """ Convert relative path or absolute path """
    def __init__(self, path):
        super(RootPath, self).__init__()
        self.path = path

    def __repr__(self): 
        return self.path 

    def __str__(self): 
        return self.path

    def rel_path(self): 
        """ convert to relative path """ 
        path = self.path
        if os.environ.get(RFPROJECT) in path:
            return path.replace(os.environ.get(RFPROJECT), RFPROJECT_VAR)
        if os.environ.get(RFPUBL) in path:
            return path.replace(os.environ.get(RFPUBL), RFPUBL_VAR)
        return path

    def abs_path(self): 
        """ convert to absolute path """ 
        path = self.path
        if RFPROJECT_VAR in path:
            return path.replace(RFPROJECT_VAR, os.environ[RFPROJECT])
        if RFPUBL_VAR in path:
            return path.replace(RFPUBL_VAR, os.environ[RFPUBL])
        return path
        

def read_config(): 
    """ read context config """ 
    config = '%s/%s' % (moduleDir, configFile)
    data = file_utils.ymlLoader(config)
    return data 


def get_context_format(): 
    """ get context format """ 
    data = read_config()
    contextFormat = data.get('format')
    sepkey = data.get('separator')
    elems = get_keys(contextFormat)
    return elems 


def get_keys(text): 
    """ get key between <> """
    startKey = '<'
    endKey = '>'
    values = [a.split('>')[0] for a in text.split(startKey) if a]
    return values 

""" example usage 
path = 'P:/ProjectRnd/projectName/asset/work/char/sunny/sunnyBase/model/main/maya/sunnyBase_model_main.v004.TA.ma'
asset = context_info.Info(path=path)
asset.work_path()
# Result: $RFPROJECT/projectName/asset/work/char/sunny/sunnyBase/model/main/maya/sunnyBase_model_main.v004.TA.ma
"""
