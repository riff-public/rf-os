# use path to for contex info
import sys
import os
import re
from collections import OrderedDict
import logging
from datetime import datetime
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import rf_config as config
from rf_utils import project_info
from rf_utils import file_utils
from rf_utils.context import context_info

if config.isMaya:
    import maya.cmds as mc


class SceneDescription(object):
    """docstring for ClassName"""
    def __init__(self, maya_path= '', path = ''): ### maya path for collect data in maya
        super(SceneDescription, self).__init__()
        if not maya_path:
            import maya.cmds as mc
            maya_path = mc.file(q=True, sn=True)
        self._path = path
        self._maya_path = maya_path
        if self._maya_path:
            self._scene = context_info.ContextPathInfo(path =maya_path)
        if self._path:
            self._scene =context_info.ContextPathInfo(path =path)
        self.yml_data = self.get_data()
    def path_shot_data(self):   #### get
        pass

    def get_asset_list(self):
        pass
    
    def set_asset_list (self):
        self.collect_data()


    def collect_data(self, asset = False, step=False):
        ref_nodes = mc.ls(type='reference')
        if 'sharedReferenceNode' in ref_nodes:
            ref_nodes.remove('sharedReferenceNode')

        self.shot_dict = dict()
        step_dict = dict()
        asset_list=[]
        for ref_node in ref_nodes:
            print ref_node
            name_space = mc.referenceQuery(ref_node, namespace=True)
            file_path = mc.referenceQuery(ref_node, filename=True)
            rev_version = find_rev_pattern(file_path)

            asset_dict = dict()
            asset_dict['path'] = file_path
            asset_dict['namespace'] = name_space
            asset_dict['rev'] = rev_version

            asset_list.append(asset_dict)

        if asset:
            return asset_list

        step_dict[self._scene.step] =dict()
        step_dict[self._scene.step]['workspace'] = self._maya_path
        step_dict[self._scene.step]['assets'] = asset_list
        # file_dict['workspace'] = self._maya_path
        if step:
            return step_dict
        self.shot_dict[self._scene.name] = step_dict
        print self.shot_dict

        return self.shot_dict

        # ev_ev04_q2010_s1210:
        #       anim: 
        #       workspace: P:/Eva/scene/work/ev04/ev_ev04_q2010_s1210/layout/main/maya/ev_ev04_q2010_s1210_layout_main.v001.TA.ma:
        #       assets: 
        #         - namespace: :eva_mk07_0_001
        #           path: Z:/asset/ev04/assets/eva/eva/mk07/0/publish/rig/provMid_Maya_all/rev003/mk07_0_pubRIG_provMid.ma
        #           rev: 003
    @property
    def shot_data(self):
        self.collect_data()
        return self.shot_dict
    @property
    def duration(self):
        if 'duration' in self.data:
            self._duration =self._data['duration']
            return self._duration

    def k_version(self, step= ''):
        if self.data['k_version']:
            return self.data['k_version']
        else:
            return 0
        # if step:
        #     shot = self._scene.name
        #     if 'k_version' in self.data[shot][step]:
        #         self._k_version =self._data[shot][step]['k_version']
        #         return self._k_version

    @property
    def camera(self):
        return self._camera
    
    # def check_exist_data(self):
    #     if os.path.exists(self.file_data_path):
    #         return True
    #     else:
    #         os.mkdir(self.file_data_path)
    #         return self.check_exist_data()

    def asset_version_list(self):
        pass

    #   ['mk07': rev001,
    #    'mk01': rev005 ]

    def get_version_asset(self):
        pass

    def write_data(self, dictData=dict(), new_version=False):
        # dictData = self.modify_data()
        print self.file_data_path()
        if dictData:
            if new_version:
                print dictData
                print self.file_data_path(new_version=True)
                file_utils.ymlDumper(self.file_data_path(new_version=True), dictData)
            else:
                file_utils.ymlDumper(self.file_data_path(), dictData)


    def get_data(self):
        data_path = self.file_data_path()
        if data_path:
            try:
                data = file_utils.ymlLoader(data_path)
                return data
            except:
                pass

    def file_data_path(self, new_version=False):
        data_path = self._scene.path.shot_data().abs_path()
        data_version = self.get_version_path(data_path)
        if new_version:
            data_version = self.get_version_path(data_path, new_version=True)
        shot_data_file = os.path.join(data_version, 'shot_data.yml').replace('\\', '/')
        return shot_data_file   
        # if os.path.exists(shot_data_file):
        

    @property
    def data(self):
        self._data = self.get_data()
        return self._data
    

    def add_shot_asset_data(self):
        scene_data = self.shot_data
        print self.yml_data, 'test'
        if self.yml_data:
            asset_data = self.collect_data(asset=True)
            # self.yml_data[self._scene.name][self._scene.step] = dict()
            self.yml_data[self._scene.name][self._scene.step]['assets'] = asset_data
            # if not 'k_version' in self.yml_data[self._scene.name][self._scene.step]:
            #     self.yml_data[self._scene.name][self._scene.step]['k_version'] = 0
            return self.yml_data
        else:
            asset_data = self.collect_data()
            # asset_data[self._scene.name][self._scene.step]['k_version'] = 0
            return asset_data

    def add_duration(self, duration):
        self.yml_data['duration'] = duration
        return self.yml_data

    def add_camera(self, camera):
        self.yml_data['camera'] = camera
        return self.yml_data

    def update_k_version(self):
        if self.yml_data:
            if 'k_version' in self.yml_data:
                self.yml_data['k_version'] = self.yml_data['k_version'] + 1
                self.write_data(dictData = self.yml_data)
            else:
                dict_data = dict()
                dict_data['k_version'] = 0
                self.write_data(dictData = self.yml_data)
        else:
            dict_data = dict()
            dict_data['k_version'] = 0
            self.write_data(dictData = self.yml_data)

        # if self.yml_data and self._scene.step:
        #     if not 'k_version' in self.yml_data[self._scene.name][self._scene.step]:
        #         self.yml_data[self._scene.name][self._scene.step]['k_version'] = 0
        #     else:
        #         k_version = self.yml_data[self._scene.name][self._scene.step]['k_version']
        #         self.yml_data[self._scene.name][self._scene.step]['k_version'] = int(k_version+1)
    
        #     return self.yml_data

        # elif self._scene.step:
        #     asset_data[self._scene.name][self._scene.step]['k_version'] = 0
        #     return asset_data

    def get_version_path(self, path, new_version=False):
        version = get_lastest_version(path)
        version_count = int(version.split('v')[1])
        new_count_version = 'v%03d'%(version_count+1)
        version_path = os.path.join(path, version)
        if new_version:
            print 'tests', new_version
            version_path = os.path.join(path, new_count_version)
            os.makedirs(version_path)
            print version_path
        return version_path

    def est_file_name(self, process='30_preViz', ext='.mp4', k_version=1, ):
        Compare_out = {'maya':'3dFile',
                    'layout':'3dLo', 
                    'anim':'3dAnim', 
                    'genz':'genz' }
        step_old = self._scene.step
        app_old = self._scene.app
        version = self._scene.version
        #step = Compare_out[str(step_old)]
        step = '3dAnim'
        # app = Compare_out[str(app_old)]
        seq =   self._scene.sequence
        shot = self._scene.name_code
        ext = ext if not '.' in ext else ext.replace('.', '')

        if process == '30_preViz':
            print 'test'
            seq = seq[:-1]
            seq =seq.replace('q', 'sn')
            shot = shot[:-1]
            shot = shot.replace('s', 'c')
        elif process == '10_main':
            seq = seq[:-1]
            seq =seq.replace('q', 'sn')
            shot = shot[:-1]
            shot = shot.replace('s', 'n')

        name_est = 'ev04_{seq}_{shot}_{process_est}_{k_version}_{version}.{ext}'.format(seq=seq, shot=shot, process_est=step, k_version= 'k%02d'%k_version, version=version.replace('v0',''), ext=ext)

        return name_est


    def est_shot_folder(self, process='30_preViz',publish=False, file_name=False):
        #### file_path = P:/Eva/scene/work/ev04/ev_ev04_q2020_s0160/layout/main/maya/ev_ev04_q2020_s0160_layout_main.v010.tot.ma
        ###{30_preViz}
        ####mapping_path> = Z:\pj\ev04\shots
        ### <mapping_path>\30_preViz\seqeunce\shot\
                                        ##  maya : work
                                        ##  pb   : review
                                                        #\step
                                                        ##layout =3dlo,
                                                        ##anim = 3dAnim

        ####Z:\pj\ev04\shots\30_preViz\ps202\p001\review\3dLO\3DFile  # file playblast
        ####Z:\pj\ev04\shots\30_preViz\ps202\p001\work\riff\3dlo\3dFile # file maya
        Compare_out = {'maya':'3dFile','layout':'3dLo', 'anim':'3dAnim', 'genz':'genz' }
        mapping_path = 'Z:/pj/ev04/shots'
        # os.path.join(project_path)
        step_old = self._scene.step
        app_old = self._scene.app
        step = Compare_out[step_old]
        app = Compare_out[app_old]
        seq =   self._scene.sequence
        shot = self._scene.name_code

        if process == '30_preViz':
            print 'test'
            seq = seq[:-1]
            seq =seq.replace('q', 'ps')
            shot = shot[:-1]
            shot = shot.replace('s', 'p')
        elif process == '10_main':
            seq = seq[:-1]
            seq =seq.replace('q', 'sn')
            shot = shot[:-1]
            shot = shot.replace('s', 'n')
        abs_path = '{mapping_path}/{process}/{seq}/{shot}'.format(mapping_path=mapping_path, process=process, seq=seq, shot=shot)
        print abs_path
        return abs_path



def get_lastest_version(file_path):
    # file_path = check_exists(file_path)
    if os.path.exists(file_path):
        all_file = os.listdir(file_path)
        increment_count=[]
        for file in all_file:
            increment = re.findall ( 'v' + '[0-9]{3}', file ) ;
            if increment:
                increment_count.append(increment[0])
        if increment_count:
            version_lastest = int(max(increment_count).split('v')[1])
            new_version = 'v%03d'%(version_lastest)
        else:
            new_version = 'v001'
            os.makedirs(os.path.join(file_path, new_version))
        return new_version
    else:
        os.makedirs(os.path.join(file_path, 'v001'))
        return 'v001'

def check_exists(path):
    exists = os.path.exists(path)
    if exists:
        return path.replace('\\','/')
    else:
        os.makedirs(path.replace('\\','/'))
        return path.replace('\\','/')

def find_rev_pattern(file_path):
    rev_version = None
    rev_pattern = re.findall('rev' + '[0-9]{3}', file_path)
    if rev_pattern:
        rev_version = rev_pattern[0].replace('rev', '')
    return rev_version 