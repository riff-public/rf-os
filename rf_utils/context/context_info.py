# use path to for contex info
import sys
import os
import re
from collections import OrderedDict
import logging
from datetime import datetime
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import rf_config as config
from rf_utils import project_info
from rf_utils import file_utils
from rf_utils import custom_exception
sg = None
serverMap = config.Env.serverMap

RFPROJECT = config.Env.projectVar
RFPUBL = config.Env.publVar
RFVERSION = config.Env.versionVar
RFUSER = config.Env.userVar

RFPROJECT_VAR = '$%s' % RFPROJECT
RFPUBL_VAR = '$%s' % RFPUBL
RFVERSION_VAR = '$%s' % RFVERSION

try:
    moduleDir = sys._MEIPASS.replace('\\', '/')
except Exception:
    moduleDir = os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')

# requires
configFile = 'context_config.yml'

if config.isMaya:
    import maya.cmds as mc

if config.isNuke: 
    import nuke  

if config.isHoudini: 
    import hou

class Env:
    context = 'RFCONTEXT'

class Cache: 
    configFile = OrderedDict()


class ContextKey:
    # context key
    project = 'project'
    projectCode = 'projectCode'
    episode = 'episode'
    entityType = 'entityType'
    entityGrp = 'entityGrp'
    entityParent = 'entityParent'
    entity = 'entity'
    entityCode = 'entityCode'
    assetType = 'assetType'
    designName = 'designName'
    assetName = 'assetName'
    shotName = 'shotName'
    step = 'step'
    assetStep = 'assetStep'
    sceneStep = 'sceneStep'
    process = 'process'
    version = 'version'
    publishVersion = 'publishVersion'
    app = 'app'
    workspace = 'workspace'
    task = 'task'
    workfile = 'workfile'
    res = 'res'
    id = 'id'
    look = 'look'
    asset = 'asset'
    scene = 'scene'
    design = 'design'
    vfx = 'vfx'
    user = 'user'

    # workspace
    work = 'work'
    publish = 'publish'
    hero = 'hero'
    recommended = 'recommended'
    cache = 'cache'
    output = 'output'
    outputImg = 'outputImg'
    texture = 'texture'
    texture = 'texture'
    textureMiarmyPath = 'textureMiarmyPath'
    publishTextureMiarmyPath = 'publishTextureMiarmyPath'
    snap = 'snap'
    data = 'data'
    shotData = 'shotData'
    processData = 'processData'

    # context attr
    static = 'static'
    dynamic = 'dynamic'
    template = 'template'
    dynamicGrp = 'dynamic-group'
    group = 'group'
    env = 'env'
    staticList = 'staticList'

    #  separator 
    nameSeparator = '_'


class SchemaKey:
    """ schema template name using in schema_config.yml "pathLevel" """
    workPath = 'workPath'
    workOutputPath = 'workOutputPath'
    publishPath = 'publishPath'
    publishWorkspacePath = 'publishWorkspacePath'
    publishHeroWorkspacePath = 'publishHeroWorkspacePath'
    publishOutputPath = 'publishOutputPath'
    heroPath = 'heroPath'
    cachePath = 'cachePath'
    outputImgPath = 'outputImgPath'
    outputHeroImgPath = 'outputHeroImgPath'
    outputHeroImgSeqPath = 'outputHeroImgSeqPath'
    publishHeroPath = 'publishHeroPath'
    publishHeroOutputPath = 'publishHeroOutputPath'
    texturePath = 'texturePath'
    publishTexturePath = 'publishTexturePath'
    publishGroomTexturePath = 'publishGroomTexturePath'
    previewTexturePath = 'previewTexturePath'
    snapPath = 'snapPath'
    shotDataPath = 'shotDataPath'
    dataPath = 'dataPath'
    asmDataPath = 'asmDataPath'
    renderDataPath = 'renderDataPath'
    processDataPath = 'processDataPath'
    dailyPath = 'dailyPath'
    iconPath = 'iconPath'
    lookPath = 'lookPath'

class WorkfileKey:
    # workfile key template using in workfile_config.yml
    workfile = 'workfile'
    workfileUser = 'workfileUser'
    daily = 'daily'
    sgName = 'sgName'
    sgSeqName = 'sgSeqName'
    iconName = 'iconName'

    # publish
    publish = 'publish'
    hero = 'hero'

    # res
    pr = 'proxy'
    md = 'medium'
    hi = 'hi'

class Lib:
    rig = 'rig'
    geo = 'geo'
    gpu = 'gpu'
    mtr = 'mtr'
    modelMtr = 'modelMtr'
    modelTmp = 'modelTmp'
    rigMtr = 'rigMtr'
    rigGrm = 'rigGrm'
    techMtr = 'techMtr'

class Step:
    design = 'design'
    model = 'model'
    rig = 'rig'
    lookdev = 'lookdev'
    texture = 'texture'
    groom = 'groom'
    sim = 'sim'
    set = 'set'
    crowd = 'crowd'


class Data:
    digit = 'digit'

class Status:
    skip = 'skip'


class SG: 
    entity = {ContextKey.asset: 'Asset', ContextKey.scene: 'Shot'}


class ProjectConfig(object):
    """docstring for ProjectConfig"""
    def __init__(self, project=None):
        super(ProjectConfig, self).__init__()
        self.projectInfo = project_info.ProjectInfo(project)
        self.configData = self.projectInfo.config_data()

        self.data = self.configData.get('schema')
        self.contextKey = self.data.get('contextKey')
        self.pathLevel = self.data.get('pathLevel')


class Context(object):
    """Context information for entity"""
    def __init__(self, **kwargs):
        super(Context, self).__init__()
        self.contextArgs = kwargs
        self.config = read_config()
        self.sepkey = self.config.get('separator')
        self.format = self.config.get('format')
        self.noContext = self.config.get('noContext')
        self.sgEntity = dict()
        self.userEntity = dict()
        self.taskEntity = dict()

        self.contextArgs = self.setup_root(self.contextArgs)
        self.context = self.construct_context(self.contextArgs)
        self.contextBk = self.construct_context(self.contextArgs)
        self.contextBk2 = self.construct_context(self.contextArgs)

    def __str__(self):
        return self.context

    def __repr__(self):
        return self.context

    @property
    def str(self):
        return self.context

    def construct_context(self, kwargs):
        # construct context
        # context = context_info.Context(project='Two_Heroes', entityType='asset', entityGrp='char', entityParent='sunny', entity='sunnyBase', step='model', process='main', version='v001', workspace='work', task='model_md')

        contextElems = []
        format = self.format
        contextFormats = get_context_format()

        # read format context
        for key in contextFormats:
            # check for static name
            format = format.replace('<%s>' % key, kwargs.get(key, self.noContext))

        return format

    def update(self, permanent=False, updateDict=None, **contextArgs):
        """ update context """
        if updateDict:
            contextArgs = updateDict

        for k, v in contextArgs.items():
            self.contextArgs.update({k: v})

        self.context = self.construct_context(self.contextArgs)
        self.contextBk = self.context if permanent else self.contextBk

    def set_context(self, contextStr):
        contextElems = contextStr.split(self.sepkey)
        formatElems = get_context_format()

        for i, formatKey in enumerate(formatElems):
            value = contextElems[i]
            self.contextArgs.update({formatKey: value})
        self.context = self.construct_context(self.contextArgs)

    def restore(self):
        """ restore original context value """
        self.context = self.contextBk

    def reset(self):
        """ reset to original value when instance """
        self.context = self.contextBk2

    def setup_root(self, contextArgs): 
        """ add root var to context args """
        roots = [RFPROJECT, RFPUBL, RFVERSION, RFUSER]
        for root in roots: 
            contextArgs[root] = os.environ.get(root)
        return contextArgs

    @property
    def elems(self):
        """ split context elements """
        return self.context.split(self.sepkey)

    @property
    def project(self):
        """ return project """
        return self.contextArgs.get('project', '')

    @property
    def projectCode(self):
        """ return projectCode """
        return self.contextArgs.get('projectCode', '')

    @property
    def entityType(self):
        """ return entityType """
        return self.contextArgs.get('entityType', '')

    @property
    def episode(self):
        """ return episode """
        return self.contextArgs.get('episode', '')

    @property
    def entityGrp(self):
        """ return entityGrp """
        return self.contextArgs.get('entityGrp', '')

    @property
    def entityParent(self):
        """ return entityParent """
        return self.contextArgs.get('entityParent', '')

    @property
    def entity(self):
        """ return entity """
        return self.contextArgs.get('entity', '')

    @property
    def entityCode(self):
        """ return entityCode """
        return self.contextArgs.get('entityCode', '')

    @property
    def step(self):
        """ return step """
        return self.contextArgs.get('step', '')

    @property
    def process(self):
        """ return process """
        return self.contextArgs.get('process', '')

    @property
    def app(self):
        """ return app """
        return self.contextArgs.get('app', '')

    @property
    def workfile(self):
        """ return workfile """
        return self.contextArgs.get('workfile', '')

    @property
    def workspace(self):
        """ return workspace """
        return self.contextArgs.get('workspace', '')

    @property
    def version(self):
        """ return version """
        return self.contextArgs.get('version', '')

    @property
    def publishVersion(self):
        """ return publishVersion """
        return self.contextArgs.get('publishVersion', '')

    @property
    def task(self):
        """ return task """
        if not self.contextArgs.get('task') and self.has_env(): 
            context = Context()
            context.use_env()
            self.update(task=context.task)
        return self.contextArgs.get('task', '')

    @property
    def res(self):
        """ return res """
        return self.contextArgs.get('res', '')

    @property
    def id(self):
        """ return id """
        return self.contextArgs.get('id', '')

    @property
    def look(self):
        """ return look """
        return self.contextArgs.get('look', '')

    @property
    def user(self):
        """ return look """
        return self.contextArgs.get('user', '')

    def keys(self): 
        return self.contextArgs.keys()

    def use_path(self, path='', schemaKey=SchemaKey.workPath):
        """ use input path or scene path as a context """
        pathInfo = PathToContext(project=self.project, path=path, task=self.task, res=self.res, schemaKey=schemaKey)
        pathContext = pathInfo.get_context()

        for k, v in pathContext.contextArgs.items():
            self.contextArgs.update({k: v})

        self.context = self.construct_context(self.contextArgs)
        self.contextBk = self.context
        # logger.debug('update context from path -> %s' % self.context)
        return self.context

    def use_sg(self, sg, project, entityType, entityId=int(), entityName=None, entity=None):
        """ use shotgun project and id """
        sgContext = SGToContext(sg, project=project, entityType=entityType, entityId=entityId, entityName=entityName, entity=entity)
        sgContextDict = sgContext.context_args()
        self.sgEntity = sgContext.sg_entity()

        for k, v in sgContextDict.items():
            self.contextArgs.update({k: v})

        self.context = self.construct_context(self.contextArgs)
        self.contextBk = self.context
        # logger.debug('update context from sg')
        return self.context

    def use_env(self):
        contextStr = os.environ.get(Env.context, '')
        self.set_context(contextStr)

    def has_env(self): 
        contextStr = os.environ.get(Env.context, '')
        return True if contextStr else False



class PathToContext(object):
    """Convert path input args to context"""
    def __init__(self, project=None, path='', task=str(), res=str(), schemaKey=SchemaKey.workPath, **kwargs):
        super(PathToContext, self).__init__()
        self.start = datetime.now()

        self.inputPath = RootPath(path).abs_path() if path else path
        self.schemaKey = schemaKey

        # get path
        if config.isMaya and not self.inputPath:
            self.inputPath = mc.file(q=True, loc=True) if not mc.file(q=True, loc=True) == 'unknown' else (mc.file(q=True, sn=True) or str())
        if config.isNuke and not self.inputPath: 
            self.inputPath = nuke.root().name()
        if config.isHoudini: 
            self.inputPath = hou.hipFile.path()
        # project information
        if not project:
            project = self.guess_project()

        self.projectInfo = project_info.ProjectInfo(project)
        # logger.debug('Read from config file -> %s' % self.projectInfo.config_file())

        # env
        projectEnv = self.projectInfo.env()
        self.rootwork = projectEnv.get(RFPROJECT)
        self.rootpubl = projectEnv.get(RFPUBL)
        self.rootpublversion = projectEnv.get(RFVERSION)

        self.task = task
        self.res = self.set_res(res)
        self.path = self.strip_root()

        # path elements
        self.schema = self.projectInfo.schema_data()
        self.contextKey = self.projectInfo.context_key()

        # work file config
        self.filenameSchema = self.projectInfo.workfile()

        # use "asset" and "scene"
        self.asset = self.contextKey.get('asset').get('key')
        self.scene = self.contextKey.get('scene').get('key')
        self.design = self.contextKey.get('design').get('key')
        self.vfx = self.contextKey.get('vfx').get('key')

        self.pathElems = self.path.split('/')
        self.pathCount = len(self.pathElems) - 1

        # schema config
        self.schemaConfig = self.get_schema() # assume this is a work file / asset or scene share the same schema
        # version
        self.versionKey = self.filenameSchema.get('versionFormat').get('prefix', 'v')
        self.versionPadding = self.filenameSchema.get('versionFormat').get('padding', 3)
        self.versionSep = self.filenameSchema.get('versionFormat').get('separator', '.')

        # context
        self.noContext = read_config().get('noContext')


    def __str__(self):
        return self.path

    def __repr__(self):
        return self.path

    def get_schema(self):
        """ get default schema """
        if self.guess_entity_type():
            result = self.projectInfo.schema_config(self.guess_entity_type()).get(self.schemaKey)
            return result

    def guess_entity_type(self):
        """ guessing entity_type of the path before able to read the config
            if "asset" or "scene" found in path using "asset" or "scene"
            if neither of them found, use "asset" as default """
        if self.asset in self.path.lower().split('/'):
            return self.asset

        if self.scene in self.path.lower().split('/'):
            return self.scene

        if self.design in self.path.lower().split('/'):
            return self.design

        if self.vfx in self.path.lower().split('/'):
            return self.vfx
        # return self.asset if self.asset in self.path.split('/') else self.scene if self.scene in self.path.split('/') else self.asset

    def guess_project(self):
        """ guessing project before get to config """
        if self.inputPath:
            allProjects = project_info.ProjectInfo().list_all(asObject=False)
            result = [a for a in self.inputPath.split('/') if a in allProjects]

            if result:
                project = result[0]
                # logger.debug('Guessing project -> %s' % project)
                return project

            else:
                logger.warning('No project found.')

    @property
    def valid_root(self):
        """ check if root on the path """
        return True if self.rootwork in self.inputPath or self.rootpubl in self.inputPath or self.rootpublversion in self.inputPath else False

    @property
    def valid_env(self):
        """ check if env match config """
        return True if self.rootwork == os.environ.get(RFPROJECT) and self.rootpubl == os.environ.get(RFPUBL) else False

    def set_res(self, res):
        """ set valid resolution """
        return res

    def strip_root(self):
        # strip rootwork first if not found strip rootpubl
        if self.valid_root:
            replaceRoot = self.rootwork if self.rootwork in self.inputPath else self.rootpubl if self.rootpubl in self.inputPath else self.rootpublversion if self.rootpublversion in self.inputPath else ''
            replaceRoot = '%s/' % replaceRoot if not replaceRoot[-1] == '/' else replaceRoot
            return self.inputPath.replace(replaceRoot, '')
        else:
            logger.warning('Root does not match current path : %s' % self.rootwork)
        return str()

    @property
    def entity_type(self):
        """ return entity type if result matched config """
        entityType = self.get_value('entityType')
        mapDict = {self.asset: ContextKey.asset, self.scene: ContextKey.scene, self.design: ContextKey.design, self.vfx: ContextKey.vfx}
        # temporty fix for lower case
        return mapDict.get(entityType.lower(), '')

    @property
    def project(self):
        """ return project """
        return self.get_value(ContextKey.project)

    @property
    def workspace(self):
        """ return work or publ """
        return self.get_value(ContextKey.workspace)

    @property
    def episode(self):
        """ return episode """
        return self.get_value(ContextKey.episode)

    @property
    def entity_group(self):
        """ return assetType or episode """
        return self.get_value(ContextKey.entityGrp)

    @property
    def entity_parent(self):
        """ return asset parent or sequence """
        return self.get_value(ContextKey.entityParent)

    @property
    def entity_name(self):
        """ return assetName or shotName """
        return self.get_value(ContextKey.entity)

    @property
    def entity_code(self):
        """ return assetCode or shotCode """
        return self.get_value(ContextKey.entityCode)

    @property
    def step(self):
        """ return department """
        return self.get_value(ContextKey.step)

    @property
    def process(self):
        """ return process name """
        return self.get_value(ContextKey.process)

    @property
    def app(self):
        """ return app name """
        return self.get_value(ContextKey.app)

    @property
    def workfile(self):
        """ return workfile name """
        return self.get_value(ContextKey.workfile)

    @property
    def publishVersion(self):
        """ return workfile name """
        return self.get_value(ContextKey.publishVersion)

    @property
    def version(self):
        """ get file version """
        workfile = self.workfile
        if workfile:
            # find "v" by split and looking for 3 padding digits
            versionNumber = [a[0: self.versionPadding] for a in workfile.split(self.versionKey) if a[0: self.versionPadding].isdigit()]
            return '%s%s' % (self.versionKey, versionNumber[0]) if versionNumber else self.noContext
        return self.noContext

    def get_context(self):
        """ return context """
        context = Context(project=self.project, entityType=self.entity_type, entityGrp=self.entity_group, entityParent=self.entity_parent, entity=self.entity_name, step=self.step, process=self.process, version=self.version, workspace=self.workspace, task=self.task, res=self.res, app=self.app, workfile=self.workfile, episode=self.episode, entityCode=self.entity_code, publishVersion=self.publishVersion)
        return context if context else str()


    def get_value(self, key):
        # get key value
        # <rootWork>/<project>/<entityType>/<work>/<entityGrp>/<entityParent>/<entity>/<assetStep>/<process>/<app>/<workfile>
        value = ''
        valid = None
        if self.schemaConfig:
            schemaElements = extract_keys(self.schemaConfig)

            # extract dictionary value to list
            schemaElementData = [self.contextKey.get(a) for a in schemaElements]
            # get parent context or use normal contextKey for mapping
            nameMap = [a.get('parent') or a.get('key') for a in schemaElementData]

            level = (nameMap.index(key) - 1) if key in nameMap else None
            if not level == None:
                value = self.pathElems[level] if self.pathCount >= level else self.noContext

                inputKey = schemaElementData[level+1].get('key')

                # check if data valid
                valid = self.filter_valid_data(inputKey, value)
        return value if valid else value


    def filter_valid_data(self, key, value):
        """ check for static attr if match the value """
        attr = self.contextKey.get(key).get('attr')
        if attr == ContextKey.static:
            staticValue = self.contextKey.get(key).get('name')
            return True if staticValue == value else False
        return True


    def set_task(self, task):
        """ set task information to context if not supply """
        self.task = task
        return task


class SGToContext(object):
    """docstring for SGToContext"""
    def __init__(self, sg, project=None, entityType=None, entityName=None, entityId=int(), entity=dict()):
        super(SGToContext, self).__init__()
        self.sg = sg
        self.project = project
        self.entityType = entityType
        self.entityId = int(entityId)
        self.entityName = entityName

        # project config
        self.projectInfo = project_info.ProjectInfo(self.project)
        self.schemaPath = self.projectInfo.schema.work(self.entityType.lower())
        self.contextDict = self.projectInfo.context_key()
        self.sgEntity = self.contextDict.get(entityType.lower()).get('entity')
        logger.debug('use config -> %s' % self.projectInfo.config_file())

        # logic to find context
        self.context = Context()
        self.entity = entity


    def context_args(self):
        """ main logic to find context """
        contextKey = extract_keys(self.context.format)
        contextArgs = OrderedDict()

        if not self.entity:
            self.entity = self.sg_entity()

        if self.entity:
            for key in contextKey:
                data = self.contextDict.get(key)
                sgKey = self.get_sg_field(data)

                # get sg value
                sgValue = self.entity.get(sgKey)

                # get static value : e.g. "asset", "scene"
                staticValue = self.get_static_value(data)

                # if add static value for non-sg data
                value = sgValue if sgValue else staticValue

                if type(value) == type(str()):
                    contextArgs[key] = value

                if type(value) == type(dict()):
                    value = value.get('name')
                    contextArgs[key] = value

                if type(value) == type(int()):
                    contextArgs[key] = str(value)

        else:
            logger.error('Cannot get shotgun entity')

        return contextArgs


    def sg_entity(self):
        """ find entity """
        # find cache first
        if not self.entity:
            fields = self.get_filed()
            filters = self.get_filter()
            entity = self.sg.find_one(self.sgEntity, filters, fields)
            logger.debug('sg_entity return %s' % entity)
            return entity
        logger.debug('sg_entity cache %s' % self.entity)
        return self.entity

    def get_filter(self):
        """ get filters """
        filters = [['project.Project.name', 'is', self.project]]
        if self.entityId:
            filters.append(['id', 'is', self.entityId])
        if self.entityName:
            filters.append(['code', 'is', self.entityName])
        logger.debug('filters %s' % filters)
        return filters

    def get_filed(self):
        """ get return value fields """
        fields = ['code']
        contextKey = extract_keys(self.context.format)
        for key in contextKey:
            data = self.contextDict.get(key)
            value = self.get_sg_field(data)
            fields.append(value) if value else ''
        logger.debug('fields %s' % fields)
        return fields


    def get_sg_field(self, data):
        attr = data.get('attr')
        name = data.get('name')

        if attr in [ContextKey.static, ContextKey.dynamic]:
            return name

        if attr in [ContextKey.group, ContextKey.dynamicGrp]:
            childValue = name.get(self.entityType.lower())
            return self.get_sg_field(childValue) if childValue else ''

        if attr == ContextKey.template:
            return ''


    def get_static_value(self, data):
        """ get value for attr == static """
        attr = data.get('attr')
        name = data.get('name')

        if attr in [ContextKey.group, ContextKey.dynamicGrp]:
            childValue = name.get(self.entityType.lower())
            return self.get_static_value(childValue) if childValue else ''

        if attr == ContextKey.static:
            return name


class ContextPathInfo(object):
    """Receive context to process path"""
    def __init__(self, context=None, path='', schemaKey=SchemaKey.workPath):
        super(ContextPathInfo, self).__init__()
        self.context = context
        start = datetime.now()
        self.rawpath = path
        self.valid = True


        if not context:
            self.context = Context()
            self.context.use_path(path=path, schemaKey=schemaKey)
            if not self.context.project: 
                self.valid = False
                logger.warning('Path is not in the Pipeline "%s"' % path)
                # raise custom_exception.PipelineError('Path is not in the Pipeline "%s"' % path)
        self.pathSep = '/'
        self.contextElems = get_context_format()
        self.sepkey = read_config().get('separator')

        # project config
        self.projectInfo = project_info.ProjectInfo(self.project, self)
        self.configData = self.projectInfo.config_data()
        self.steps = self.configData.get('steps')
        self.schema = self.configData.get('schema').get('pathLevel')
        self.contextKey = self.configData.get('schema').get('contextKey')
        self.asset = self.contextKey.get('asset').get('key')
        self.scene = self.contextKey.get('scene').get('key')
        self.vfx = self.contextKey.get('vfx').get('key')
        self.sgAsset = self.contextKey.get('asset').get('entity')
        self.sgScene = self.contextKey.get('scene').get('entity')
        self.design = self.contextKey.get('design').get('key')

        self.filenameSchema = self.projectInfo.workfile()
        self.versionKey = self.filenameSchema.get('versionFormat').get('prefix', 'v')

        self.checkFormat = False
        self.sgCaches = dict()

        # output config
        self.outputConfig = self.configData.get('output')

        # workfile config
        self.workfileConfig = self.projectInfo.schema.context_key().get('workfile').get('map')

        # root config
        self.rootConfig = self.configData.get('env').get(RFPROJECT)
        self.publConfig = self.configData.get('env').get(RFPUBL)
        # update context if scene
        self.update_scene_context(False)
        # self.projectInfo.apply_overrides(self)

    def __str__(self):
        return str(self.context)

    def __repr__(self):
        return str(self.context)

    def root(self, rel=True):
        return '$%s' % RFPROJECT if rel else os.environ.get(RFPROJECT)

    def publ(self, rel=True):
        return '$%s' % RFPUBL if rel else os.environ.get(RFPUBL)

    @property
    def project(self):
        """ get project from context """
        return self.get_value(ContextKey.project)

    @property
    def project_code(self):
        """ get project from context """
        return self.get_value(ContextKey.projectCode)

    @property
    def entity_type(self):
        """ get entity type - asset or scene """
        return self.get_value(ContextKey.entityType)

    @property
    def sg_entity_type(self): 
        """ get Shotgun entity type 'Asset' or 'Shot' """
        return SG.entity.get(self.entity_type)

    @property
    def type(self):
        """ get asset type from context """
        return self.get_value(ContextKey.entityGrp)

    @property
    def episode(self):
        """ get episode from context """
        return self.get_value(ContextKey.entityGrp)

    @property
    def parent(self):
        """ get entity_parent from context """
        return self.get_value(ContextKey.entityParent)

    @property
    def sequence(self):
        """ get sequence from context """
        value = self.get_value(ContextKey.entityParent)
        return value if not self.checkFormat else self.format_value(ContextKey.entityParent, value)

    @property
    def name(self):
        """ get entity name from context """
        return self.get_value(ContextKey.entity)

    @property
    def name_code(self):
        """ get entity name from context """
        value = self.get_value(ContextKey.entityCode)
        return value if not self.checkFormat else self.format_value(ContextKey.entityCode, value)

    @property
    def step(self):
        """ get step from context """
        return self.get_value(ContextKey.step)

    @property
    def step_code(self):
        """ get short name for step """
        return self.steps[self.entity_type][self.step]['short']

    @property
    def sg_step(self):
        """ get Shotgun name for step """
        return self.steps[self.entity_type][self.step].get('sgname')

    @property
    def sg_code(self):
        """ get Shotgun short code for step """
        return self.steps[self.entity_type][self.step].get('sgcode')

    @property
    def process(self):
        """ get process from context """
        return self.get_value(ContextKey.process)

    @property
    def app(self):
        """ get app from context """
        return self.get_value(ContextKey.app)

    @property
    def version(self):
        """ get file version from context """
        return self.get_value(ContextKey.version)

    @property
    def publishVersion(self):
        """ get file publishVersion from context """
        return self.get_value(ContextKey.publishVersion)

    @property
    def workspace(self):
        """ get workspace from context """
        return self.get_value(ContextKey.workspace)

    @property
    def task(self):
        """ get task from context """
        return self.get_value(ContextKey.task)

    @property
    def filename(self, ext=True):
        """ get filename """
        basename = self.get_value(ContextKey.workfile)
        if not ext:
            return os.path.splitext(basename)[0]
        return basename

    @property
    def res(self):
        """ get res from context """
        return self.get_value(ContextKey.res)

    @property
    def id(self):
        """ get id from context """
        return self.get_value(ContextKey.id)

    @property
    def look(self):
        """ get look from context """
        return self.get_value(ContextKey.look)

    @property
    def user(self):
        """ get user from context """
        return self.get_value(ContextKey.user)

    @property
    def mtr_namespace(self):
        # hard coded for now
        return '%s_%s%sMtr' % (self.name, self.step_code, self.look.capitalize())

    @property
    def local_user(self):
        return os.environ.get(RFUSER)

    @property
    def ad_name(self):
        name = '{0}_ad.hero.yml'.format(self.name)
        return name

    def get_value(self, key):
        index = self.contextElems.index(key)
        value = self.context.elems[index]
        return value

    def format_value(self, key, value):
        """ name format """
        textFormat = self.contextKey[key].get('format', '')
        if self.contextKey[key]['attr'] == ContextKey.dynamicGrp:
            textFormat = self.contextKey[key]['name'][self.entity_type].get('format', '')
        if textFormat:
            keys = extract_keys(textFormat)
            if Data.digit in keys:
                digit = extract_digit(value)[0] if extract_digit(value) else ''
                value = textFormat.replace('<%s>' % Data.digit, digit)
        return value

    @property
    def contextMapValues(self):
        """ map context value to dictionary """
        contextMap = OrderedDict()
        for i, key in enumerate(self.contextElems):
            contextMap[key] = self.context.elems[i]
        return contextMap

    def key(self, contextKey):
        return self.contextKey.get(contextKey).get('name')


    def set_name(self):
        """ create name from format config """
        self.context.update(entity=self.sg_name)

    def set_project_code(self, sgobject=None):
        """ find project code """ # hard coded
        global sg
        if not sg:
            sg = sgobject
        if sg:
            if not self.project in self.sgCaches.keys():
                projectEntity = sg.find_one('Project', [['name', 'is', self.project]], ['name', 'id', 'sg_project_code'])
                self.sgCaches[self.project] = projectEntity
            else:
                projectEntity = self.sgCaches[self.project]
            self.context.update(projectCode=projectEntity['sg_project_code'])

    def update_scene_context(self, updateAllKey=True):
        """ if entity == scene. extract name to fill other elements
        pr_ep01_q0010_s0010 -> q0010 -> s0010 """
        if self.entity_type == self.scene:
            if self.name:
                nameFormat = self.workfileConfig['format'][self.scene][WorkfileKey.sgName]
                data = map_format_value(nameFormat, self.name, separator='_')

                for k, v in data.items():
                    # if we update context in a loop, this will always force update other key value regardless old value.
                    # this is old method
                    if not updateAllKey: 
                        if not self.context.contextArgs.get(k) or self.context.contextArgs.get(k) == self.context.noContext:
                                self.context.update(updateDict={k: v})

                    # this new method but break so many script
                    else: 
                        if not v == self.context.noContext: 
                                self.context.update(updateDict={k: v})

                    # this logic only update when old value is empty or 'none'
                    # else:
                    #     print 'skip update', k, v
                # self.context.update(updateDict=data)
            if self.name == self.context.noContext:
                self.set_project_code()
                self.context.update(entity=self.sg_name)



    def list_step(self, dir=False):
        """ list steps """
        if dir:
            path = os.path.split(self.path.step().abs_path())[0]
            return file_utils.listFolder(path)
        return self.steps.get(self.entity_type)

    def list_res(self):
        """ list res """
        return [v for k, v in self.projectInfo.asset.list_res().items()]

    def list_process(self, workspace=ContextKey.work):
        """ list process in structure """
        path = os.path.split(self.path.process(workspace=workspace).abs_path())[0]
        return file_utils.listFolder(path)

    def list_variant(self):
        """ list variant in structure """
        path = os.path.split(self.path.name().abs_path())[0]
        return file_utils.listFolder(path)

    # path class
    @property
    def path(self):
        return ContextPath(self)

    @property
    def sg(self):
        return SGEntity(self.context)

    def name_format(self, key):
        name = self._get_naming_schema(entityType=self.entity_type, template=key)
        return name

    @property
    def sg_name(self):
        template = ''
        name = self._get_naming_schema(entityType=self.entity_type, template=WorkfileKey.sgName, workspace=ContextKey.work)
        return name

    @property
    def sg_seq_name(self):
        template = ''
        name = self._get_naming_schema(entityType=self.entity_type, template=WorkfileKey.sgSeqName, workspace=ContextKey.work)
        return name

    # file name
    def work_name(self, user=False):
        """ return work file "asset" or "scene" """
        # default template
        if self._valid_entity_type():
            template = WorkfileKey.workfileUser if user else WorkfileKey.workfile
            name = self._get_naming_schema(entityType=self.entity_type, template=template, workspace=ContextKey.work)
            return name

    def publish_name(self, hero=False, ext=''):
        # default template
        if self._valid_entity_type():
            template = WorkfileKey.hero if hero else WorkfileKey.publish
            name = self._get_naming_schema(entityType=self.entity_type, template=template, workspace=ContextKey.publish)
            name = '%s%s' % (os.path.splitext(name)[0], ext) if ext else name
            return name

    def snap_name(self, ext='.yml'):
        name, sext = os.path.splitext(self.work_name())
        filename = '%s%s' % (name, ext)
        return filename

    def daily_name(self, ext='', version='version'):
        # default template
        if self._valid_entity_type():
            template = WorkfileKey.daily
            name = self._get_naming_schema(entityType=self.entity_type, template=template, workspace=ContextKey.publish)
            name = '%s%s' % (os.path.splitext(name)[0], ext) if ext else name
            if version == 'subversion':
                if self.version: 
                    name = name.replace(self.publishVersion, self.subversion)
                else: 
                    date = datetime.now().strftime('%Y%m%d-%H%M')
                    name = name.replace(self.publishVersion, '{}{}'.format(self.subversion, date))
            return name

    def icon_name(self, ext=''):
        # default template
        if self._valid_entity_type():
            template = WorkfileKey.iconName
            name = self._get_naming_schema(entityType=self.entity_type, template=template, workspace=ContextKey.publish)
            name = '%s%s' % (os.path.splitext(name)[0], ext) if ext else name
            return name

    @property
    def subversion(self):
        diviation = -1
        publishVersion = self.publishVersion if diviation == 0 else increment_version(self.versionKey, self.publishVersion, diviation)
        subversion = '%s_%s' % (publishVersion, self.version.replace(self.versionKey, ''))
        return subversion

    def subversion_name(self, hero=False, ext=''):
        """ calculate subversion """
        if not self.publishVersion == self.context.noContext:
            publishName = self.publish_name(ext=ext)
            return publishName.replace(self.publishVersion, self.subversion)

    def output_name(self, outputKey=None, hero=False, fileType=False, ext=None):
        # default template
        if self._valid_entity_type():
            if outputKey:
                workspaceContext = ContextKey.hero if hero else ContextKey.publish
                name, outputType = self._get_output_schema(entityType=self.entity_type, step=self.step, outputKey=outputKey, workspace=workspaceContext)
                name = '%s%s' % (os.path.splitext(name)[0], ext) if ext else name
                return name if not fileType else [name, outputType]
            self.publish_name(hero=hero)

    def output(self, entityType=None, step=None, outputKey=None, hero=True, fileType=False):
        """ get output by input arguments """
        entityType = self.entity_type if not entityType else entityType
        step = self.step if not step else step
        workspace = ContextKey.hero if hero else ContextKey.publish
        keys = self._get_outputKey(entityType=entityType, workspace=workspace, step=step)
        if outputKey in keys:
            name, outputType = self._get_output_schema(entityType=entityType, step=step, outputKey=outputKey, workspace=workspace)
            return name if not fileType else [name, outputType]
        else:
            logger.error('Wrong key, %s -> %s' % (step, keys))

    def update_context_from_file(self): 
        """ update context key from information on filename 
        ex. res, look 
        """
        basename = os.path.basename(self.rawpath)
        outputList = self.guess_outputKey(basename)
        outputKey, step = outputList[0]
        self.context.update(step=step)
        self.extract_name(basename, outputKey=outputKey)

    def extract_name(self, name, outputKey=None, workfileKey=None, hero=False, force=False):
        workspace = ContextKey.hero if hero else ContextKey.publish
        if outputKey:
            outputFormat = self.outputConfig[self.entity_type][workspace][self.step][outputKey]['format']
        if workfileKey:
            outputFormat = self.workfileConfig['format'][self.entity_type][workfileKey]
        data = map_output_format_value(outputFormat, name, separators=['_', '.'])

        for k, v in data.items():
            if not self.context.contextArgs.get(k):
                self.context.update(updateDict={k: v})
            else:
                if force:
                    self.context.update(updateDict={k: v})


    def guess_outputKey(self, name):
        # self.outputConfig[self.entity_type][workspace][self.step][outputKey]['format']
        outputKeys = []
        for workspace, stepDict in self.outputConfig[self.entity_type].items():
            if workspace in [ContextKey.publish, ContextKey.hero]:
                for step, outputKeyDict in self.outputConfig[self.entity_type][workspace].items():
                    for outputKey, formatDict in self.outputConfig[self.entity_type][workspace][step].items():
                        matchValue = compare_output_format_value(formatDict['format'], name)
                        if matchValue > 90:
                            data = [outputKey, step]
                            outputKeys.append(data) if not data in outputKeys else None
        return outputKeys

    def recommended_file(self, entityType=None, step=None, hero=True, fileType=False):
        """ get recommended file """
        outputFile = ''
        entityType = self.entity_type if not entityType else entityType
        step = self.step if not step else step
        workspace = ContextKey.hero if hero else ContextKey.publish
        outputKey = self._get_recommended_key(entityType=entityType, step=step)
        if outputKey:
            outputFile = self.output(entityType=entityType, step=step, outputKey=outputKey, hero=hero, fileType=fileType)
        return outputFile

    def library(self, key=''):
        name = ''
        if key == Lib.rig:
            name, outputType = self._get_output_schema(entityType=self.asset, step=Step.rig, outputKey='rig', workspace=ContextKey.hero)
        if key == Lib.rigGrm:
            name, outputType = self._get_output_schema(entityType=self.asset, step=Step.rig, outputKey='rigGrm', workspace=ContextKey.hero)
        if key == Lib.geo:
            name, outputType = self._get_output_schema(entityType=self.asset, step=Step.model, outputKey='cache', workspace=ContextKey.hero)
        if key == Lib.gpu:
            name, outputType = self._get_output_schema(entityType=self.asset, step=Step.model, outputKey='gpu', workspace=ContextKey.hero)
        if key == Lib.mtr:
            name, outputType = self._get_output_schema(entityType=self.asset, step=Step.lookdev, outputKey='material', workspace=ContextKey.hero)
        if key == Lib.modelMtr:
            name, outputType = self._get_output_schema(entityType=self.asset, step=Step.texture, outputKey='material', workspace=ContextKey.hero)
        if key == Lib.rigMtr:
            name, outputType = self._get_output_schema(entityType=self.asset, step=Step.texture, outputKey='materialRig', workspace=ContextKey.hero)
        if key == Lib.modelTmp: 
            name, outputType = self._get_output_schema(entityType=self.asset, step=Step.texture, outputKey='materialTmp', workspace=ContextKey.hero)
        if key == Lib.techMtr: 
            name, outputType = self._get_output_schema(entityType=self.asset, step=Step.texture, outputKey='materialTech', workspace=ContextKey.hero)

        return name

    def write_env(self):
        """ write context to current env """
        os.environ[Env.context] = str(self.context)

    def read_env(self):
        """ read from current env """
        return os.environ.get(Env.context, '')

    def use_env_context(self):
        """ use context from env """
        contextStr = os.environ.get(Env.context, '')
        if contextStr:
            self.context.set_context(contextStr)
        else:
            logger.warning('No context env found')

    def copy(self):
        context = Context()
        context.set_context(self.context.context)
        entity = ContextPathInfo(context=context)
        return entity

    def resolve_pattern(self, pattern): 
        import lucidity
        template = lucidity.Template(pattern, pattern)
        # check if any key in template missing from context.contextArgs. If True, don't resolve return None 
        if any(a not in self.context.contextArgs.keys() for a in list(template.keys())): 
            return ''
        resolvePattern = template.format(self.context.contextArgs)
        return resolvePattern

    def _get_outputKey(self, entityType=None, workspace=None, step=''):
        entityType = self.entity_type if not entityType else entityType
        workspace = ContextKey.hero if not workspace else workspace
        return self.outputConfig[entityType][workspace].get(step).keys()

    def _guess_file_type(self, entityType=None, path=''):
        entityType = self.entity_type if not entityType else entityType
        types = self.outputConfig[entityType]['guessFileType']['key'].keys()
        matchType = [a for a in types if a in os.path.basename(path)]
        fileType = self.outputConfig[entityType]['guessFileType']['key'][matchType[0]] if matchType else None
        if not fileType:
            exts = self.outputConfig[entityType]['guessFileType']['ext'].keys()
            matchExt = [a for a in exts if os.path.splitext(path)[-1] == a]
            fileType = self.outputConfig[entityType]['guessFileType']['ext'][matchExt[0]] if matchExt else None

        return fileType

    def _valid_entity_type(self):
        """ check for valid entity type
        *** this is hard code, should be changed """
        valid = True if self.entity_type in [self.asset, self.scene, self.design, self.vfx] else False
        if not valid:
            logger.error('Cannot get template. Entity type invalid -> %s' % self.entity_type)
        return valid


    def _get_naming_schema(self, entityType, template=None, workspace=None):
        """ get name value from workfile config """
        nameFormat = self.workfileConfig['format'][entityType][template]
        contextKey = self.workfileConfig['contextKey']
        keys = extract_keys(nameFormat)
        name = nameFormat
        workspace = workspace if workspace else self.workspace

        for key in keys:
            element = contextKey.get(key)
            value = self._get_element_value(element) or self.context.noContext
            name = name.replace('<%s>' % key, value)

        # if user:
        #     replaceKey = self.workfileConfig.get('user').get('replace')
        #     name = name.replace(replaceKey, self.local_user)

        return self._get_ext(name, workspace)


    def _get_output_schema(self, entityType, step, outputKey, workspace):
        keyData = self.outputConfig[entityType][workspace].get(step, dict()).get(outputKey)
        keyData = self.outputConfig[entityType][workspace]['default'].get(outputKey) if not keyData else keyData

        if keyData: 
            outputFormat = keyData['format']
            outputType = keyData['filetype']
            contextKey = self.workfileConfig['contextKey']
            keys = extract_keys(outputFormat)
            name = outputFormat
            for key in keys:
                element = contextKey.get(key)
                value = self._get_element_value(element)
                name = name.replace('<%s>' % key, value)
        else: 
            raise custom_exception.PipelineError('outputKey "%s" not found in output config. Check project rf_config.' % outputKey)

        return name, outputType

    def _get_recommended_key(self, entityType, step):
        return self.outputConfig.get(entityType, dict()).get(ContextKey.recommended, dict()).get(step)
        # return self.outputConfig[entityType][ContextKey.recommended][step]

    def _get_ext(self, name, workspace):
        contextKey = self.workfileConfig.get('extFormat')
        replaceKey = contextKey.get('replace')
        ext = contextKey.get(workspace, dict()).get(self.app, self.app)
        return name.replace(replaceKey, ext)

    def _get_schema(self, workspace=ContextKey.work):
        """ return full path schema for asset or scene """
        # workspace
        if workspace == ContextKey.work:
            pathSchema = self.schema[self.entity_type][SchemaKey.workPath]

        # publish space
        elif workspace == ContextKey.publish:
            pathSchema = self.schema[self.entity_type][SchemaKey.publishPath]

        elif workspace == ContextKey.hero:
            pathSchema = self.schema[self.entity_type][SchemaKey.heroPath]

        # cache
        elif workspace == ContextKey.cache:
            pathSchema = self.schema[self.entity_type][SchemaKey.cachePath]

        # render images (outputImg)
        elif workspace == ContextKey.outputImg:
            pathSchema = self.schema[self.entity_type][SchemaKey.outputImgPath]

        elif workspace == ContextKey.texture:
            pathSchema = self.schema[self.entity_type][SchemaKey.texturePath]

        elif workspace == ContextKey.snap:
            pathSchema = self.schema[self.entity_type][SchemaKey.snapPath]

        elif workspace == ContextKey.data:
            pathSchema = self.schema[self.entity_type][SchemaKey.dataPath]

        elif workspace == ContextKey.processData:
            pathSchema = self.schema[self.entity_type][SchemaKey.processDataPath]

        elif workspace == ContextKey.shotData:
            pathSchema = self.schema[self.entity_type][SchemaKey.shotDataPath]

        else:
            pathSchema = self.schema[self.entity_type][workspace]

        return self._translate_path_config(pathSchema)

    def _combine_path(self, pathSchema, stopKey=''):
        """ combine path from schema config """
        targetIndex = [i for i, a in enumerate(pathSchema) if a.get('key') == stopKey]
        targetPath = pathSchema
        elements = []

        if targetIndex:
            targetPath = pathSchema[0: targetIndex[0] + 1]

        for element in targetPath:
            value = self._get_element_value(element)
            elements.append(value)

        joinPath = self.pathSep.join(elements)
        return RootPath(joinPath)

    def _get_element_value(self, element):
        """ filter for static, dynamic, group and return value
        static will use "name" key from schema config
        dynamic will use context value
        group will looking for child config """

        key = element.get('key')
        name = element.get('name')
        attr = element.get('attr')
        parent = element.get('parent')
        mapValue = element.get('map')
        display = element.get('display')

        # use parent key
        if parent:
            key = parent

        if attr == ContextKey.static:
            value = name
        if attr == ContextKey.dynamic or attr == ContextKey.dynamicGrp:
            value = self.contextMapValues.get(key)
        if attr == ContextKey.template:
            # find map value and try to map input to existing keyword
            value = self.contextMapValues.get(key)
            value = mapValue.get(value, dict()).get(display, value)

        if attr == ContextKey.group:
            element = name.get(self.contextMapValues.get(key))
            value = self._get_element_value(element)

        # if attr = env. get value from os.environ
        if attr == ContextKey.env:
            value = os.environ.get(name)

        return value

    def _get_map_value(self, mapValue):
        """ get mapped value from project_config.yml key """
        self.configData.get(key)

    def _translate_path_config(self, pathConfig):
        """ convert <item>/<item> format to data """
        separator = '/'
        elements = extract_keys(pathConfig)
        contextKey = self.configData['schema']['contextKey']
        translateData = []

        for element in elements:
            translateData.append(contextKey.get(element))

        return translateData


class ContextPath(object):
    """Path Context"""
    def __init__(self, main):
        super(ContextPath, self).__init__()
        self.main = main

    def __repr__(self):
        return self.path()

    def __str__(self):
        return self.path()

    def current(self): 
        return self.path()

    def type(self, workspace=ContextKey.work):
        """ return type """
        pathSchema = self.main._get_schema(workspace=workspace)
        return self.main._combine_path(pathSchema, ContextKey.assetType)

    def episode(self, workspace=ContextKey.work):
        """ return episode """
        pathSchema = self.main._get_schema(workspace=workspace)
        return self.main._combine_path(pathSchema, ContextKey.entityGrp)

    def parent(self, workspace=ContextKey.work):
        """ return assetParent or sequence """
        pathSchema = self.main._get_schema(workspace=workspace)
        return self.main._combine_path(pathSchema, ContextKey.entityParent)

    def sequence(self, workspace=ContextKey.work):
        """ return episode """
        pathSchema = self.main._get_schema(workspace=workspace)
        return self.main._combine_path(pathSchema, ContextKey.entityParent)

    def name(self, workspace=ContextKey.work):
        """ get entity asset name or shot name """
        mapDict = {self.main.asset: ContextKey.assetName, self.main.scene: ContextKey.shotName, self.main.design: ContextKey.designName}
        stopKey = mapDict[self.main.entity_type]
        pathSchema = self.main._get_schema(workspace=workspace)
        return self.main._combine_path(pathSchema, stopKey)

    def step(self, step='', workspace=ContextKey.work):
        """ get step path or replace with input step """
        stopKey = ContextKey.assetStep if self.main.entity_type == self.main.asset else ContextKey.sceneStep
        pathSchema = self.main._get_schema(workspace=workspace)
        path = self.main._combine_path(pathSchema, stopKey)
        return path.replace(self.main.step, step) if step else path

    def process(self, process='', workspace=ContextKey.work):
        """ get process path or replace with input process """
        pathSchema = self.main._get_schema(workspace=workspace)
        path = self.main._combine_path(pathSchema, ContextKey.process)
        return path.replace(self.main.process, process) if process else path

    def snap(self):
        """ get snap path """
        pathSchema = self.main._get_schema(workspace=ContextKey.snap)
        path = self.main._combine_path(pathSchema, ContextKey.snap)
        return path

    def snap_version(self):
        """ get snap path """
        pathSchema = self.main._get_schema(workspace=ContextKey.snap)
        path = self.main._combine_path(pathSchema)
        version = '%s/%s' % (path, self.main.version)
        return version

    def app(self, app=''):
        """ get app path or replace with input app """
        pathSchema = self.main._get_schema()
        path = self.main._combine_path(pathSchema, ContextKey.app)
        return path.replace(self.main.app, app) if app else path

    def path(self):
        """ get full workspace path """
        pathSchema = self.main._get_schema()
        return self.main._combine_path(pathSchema)

    def workspace(self):
        """ get work path """
        return RootPath(os.path.dirname(self.path()))

    def work(self):
        """ get work dir """
        pathSchema = self.main._get_schema(workspace=ContextKey.work)
        path = self.main._combine_path(pathSchema, ContextKey.work)
        return RootPath(path)

    def project(self):
        """ work output e.g. playblast """
        stopKey = ContextKey.project
        pathSchema = self.main._get_schema(SchemaKey.workOutputPath)
        return self.main._combine_path(pathSchema, stopKey=stopKey)

    def work_output(self):
        """ work output e.g. playblast """
        pathSchema = self.main._get_schema(SchemaKey.workOutputPath)
        return self.main._combine_path(pathSchema)

    def publish(self):
        """ get publish dir """
        pathSchema = self.main._get_schema(workspace=ContextKey.publish)
        path = self.main._combine_path(pathSchema, ContextKey.publish)
        return RootPath(path)

    def publish_workspace(self):
        """ get publish dir """
        pathSchema = self.main._get_schema(SchemaKey.publishWorkspacePath)
        return self.main._combine_path(pathSchema)

    def publish_workspace_hero(self):
        """ publish hero workspace """
        pathSchema = self.main._get_schema(SchemaKey.publishHeroWorkspacePath)
        return self.main._combine_path(pathSchema)

    def publish_output(self):
        """ get publish dir """
        pathSchema = self.main._get_schema(SchemaKey.publishOutputPath)
        return self.main._combine_path(pathSchema)

    # publish section
    def output(self):
        """ get output publish path """
        pathSchema = self.main._get_schema(ContextKey.publish)
        return self.main._combine_path(pathSchema)

    def texture_oa(self):
        """ return texture path """
        pathSchema = self.main._get_schema(ContextKey.textureMiarmyPath)
        return self.main._combine_path(pathSchema)
    def publish_texture_oa(self):
        """ return texture path """
        pathSchema = self.main._get_schema(ContextKey.publishTextureMiarmyPath)
        return self.main._combine_path(pathSchema)
    
    def texture(self):
        """ return texture path """
        pathSchema = self.main._get_schema(ContextKey.texture)
        return self.main._combine_path(pathSchema)

    def publish_texture(self):
        """ return texture path """
        pathSchema = self.main._get_schema(SchemaKey.publishTexturePath)
        return self.main._combine_path(pathSchema)

    def publish_groom_texture(self):
        """ return texture path """
        pathSchema = self.main._get_schema(SchemaKey.publishGroomTexturePath)
        return self.main._combine_path(pathSchema)

    def preview_texture(self):
        """ return texture path """
        pathSchema = self.main._get_schema(SchemaKey.previewTexturePath)
        return self.main._combine_path(pathSchema)

    def publish_version(self):
        """ get version publish path """
        pathSchema = self.main._get_schema(ContextKey.publish)
        return self.main._combine_path(pathSchema, ContextKey.version)

    def publish_hero(self):
        pathSchema = self.main._get_schema(SchemaKey.publishHeroPath)
        return self.main._combine_path(pathSchema)

    def look(self):
        pathSchema = self.main._get_schema(SchemaKey.lookPath)
        return self.main._combine_path(pathSchema)

    def scheme(self, key):
        """ get path from schema keyword - use list_key() to show all available keys """
        pathSchema = self.main._get_schema(key)
        return self.main._combine_path(pathSchema)

    # asset hero path
    def hero(self):
        """ return hero path """
        pathSchema = self.main._get_schema(ContextKey.hero)
        return self.main._combine_path(pathSchema)

    def cache(self):
        """ return scene cache path """
        pathSchema = self.main._get_schema(ContextKey.cache)
        return self.main._combine_path(pathSchema)

    def outputImg(self):
        """ return outputImg (Render images) """
        pathSchema = self.main._get_schema(ContextKey.outputImg)
        return self.main._combine_path(pathSchema)

    def output_hero_img(self):
        """ return outputImg (Render images) """
        pathSchema = self.main._get_schema(SchemaKey.outputHeroImgPath)
        return self.main._combine_path(pathSchema)

    def output_hero_img_seq(self):
        pathSchema = self.main._get_schema(SchemaKey.outputHeroImgSeqPath)
        return self.main._combine_path(pathSchema)

    def output_hero(self):
        """ return outputHero (Render images) """
        pathSchema = self.main._get_schema(SchemaKey.publishHeroOutputPath)
        return self.main._combine_path(pathSchema)

    def data(self):
        """ return dataPath """
        pathSchema = self.main._get_schema(ContextKey.data)
        return self.main._combine_path(pathSchema)
        
    def shot_data(self):
        """ return dataPath """
        pathSchema = self.main._get_schema(ContextKey.shotData)
        return self.main._combine_path(pathSchema)

    def asm_data(self):
        """ return dataPath """
        pathSchema = self.main._get_schema(SchemaKey.asmDataPath)
        return self.main._combine_path(pathSchema)

    def render_data(self):
        """ return dataPath """
        pathSchema = self.main._get_schema(SchemaKey.renderDataPath)
        return self.main._combine_path(pathSchema)

    def process_data(self):
        """ return processDataPath """
        pathSchema = self.main._get_schema(ContextKey.processData)
        return self.main._combine_path(pathSchema)

    def daily(self):
        """ get publish dir """
        pathSchema = self.main._get_schema(SchemaKey.dailyPath)
        return self.main._combine_path(pathSchema)

    def icon(self):
        """ get icon dir """
        pathSchema = self.main._get_schema(SchemaKey.iconPath)
        return self.main._combine_path(pathSchema)

    def list_key(self):
        """ list all available schema for this project """
        return self.main.schema.get(self.main.entity_type).keys()


class SGEntity(object):
    """docstring for SGEntity"""
    def __init__(self, sg, context):
        super(SGEntity, self).__init__()
        pass



class RootPath(str):
    """ Convert relative path or absolute path """
    def __init__(self, path):
        # temporary fix super issue on python 2 and 3
        if sys.version_info <= (3, 0): 
            super(RootPath, self).__init__(path)
        else: 
            super(RootPath, self).__init__()

        self.path = path
        self.keyVar = [RFPROJECT_VAR, RFPUBL_VAR, RFVERSION_VAR]

    def __repr__(self):
        return self.path

    def __str__(self):
        return self.path

    def rel_path(self):
        """ convert to relative path """
        path = self.path
        if os.environ.get(RFPROJECT) in path:
            return path.replace(os.environ.get(RFPROJECT), RFPROJECT_VAR)
        if os.environ.get(RFPUBL) in path:
            return path.replace(os.environ.get(RFPUBL), RFPUBL_VAR)
        return path

    def rel_proj(self):
        path = self.path
        var = self.find_root_var(path)
        if os.environ.get(RFPROJECT) in path:
            return path.replace(os.environ.get(RFPROJECT), RFPROJECT_VAR)
        if var: 
            return RootPath(path.replace(var, RFPROJECT_VAR))

    def rel_publ(self):
        path = self.path
        var = self.find_root_var(path)
        if os.environ.get(RFPUBL) in path:
            return path.replace(os.environ.get(RFPUBL), RFPUBL_VAR)
        if var: 
            return RootPath(path.replace(var, RFPUBL_VAR))

    def rel_version(self): 
        path = self.path
        var = self.find_root_var(path)
        if os.environ.get(RFVERSION) in path:
            return path.replace(os.environ.get(RFPUBL), RFVERSION_VAR)
        if var: 
            return RootPath(path.replace(var, RFVERSION_VAR))

    def abs_path(self):
        """ convert to absolute path """
        path = self.path
        if RFPROJECT_VAR in path:
            return path.replace(RFPROJECT_VAR, os.environ[RFPROJECT])
        if RFPUBL_VAR in path:
            return path.replace(RFPUBL_VAR, os.environ[RFPUBL])
        if RFVERSION_VAR in path:
            return path.replace(RFVERSION_VAR, os.environ[RFVERSION])
        return path

    def unc_path(self):
        """ convert to unc path """
        path = self.abs_path()
        drive = os.path.splitdrive(path)[0]
        uncPath = serverMap.get(drive, '')

        if uncPath:
            return path.replace(drive, uncPath)
        return path

    def find_root_var(self, path): 
        for var in self.keyVar: 
            if var in path: 
                return var 

    def replace_ext(self, ext):
        self.path = '%s%s' % (os.path.splitext(self.path)[-1], ext)
        return self.path

    def local_path(self): 
        """ convert to local user path """
        path = self.path
        variables = [os.environ[RFPROJECT], os.environ[RFPUBL], os.environ[RFVERSION]]
        RFLOCAL = os.environ.get('RFLOCAL')
        localPath = RFLOCAL if RFLOCAL else os.path.expanduser('~')
        if RFPROJECT_VAR in path:
            return path.replace(RFPROJECT_VAR, localPath)
        elif RFPUBL_VAR in path:
            return path.replace(RFPUBL_VAR, localPath)
        elif RFVERSION_VAR in path:
            return path.replace(RFVERSION_VAR, localPath)
        elif any(a in path for a in variables): 
            return '%s%s' % (localPath, os.path.splitdrive(path)[-1])
        return path

    def striproot(self): 
        path = self.path
        roots = [os.environ[RFPROJECT], os.environ[RFPUBL], os.environ[RFVERSION], RFPROJECT_VAR, RFPUBL_VAR, RFVERSION_VAR]
        
        for root in roots: 
            if root in path: 
                return path.replace('%s/' % root, '')
            else: 
                custom_exception.PipelineError('No root found')




class OSPath(str):
    """docstring for OSPath"""
    def __init__(self, path):
        super(OSPath, self).__init__()
        self.path = path

    def win_path(self):
        return self.path.replace('/', '\\')

    def darwin_path(self):
        return self.path.replace('\\', '/')

def clear_env(): 
    """ clear context env """
    os.environ[Env.context] = ''


def read_config():
    """ read context config """
    data = Cache.configFile
    if not data: 
        config = '{}/{}'.format(moduleDir, configFile)
        data = file_utils.ymlLoader(config)
        Cache.configFile = data
    return data

def increment_version(prefix, version, increment=0):
    num = version.replace(prefix, '')
    if num.isdigit():
        padding = len(num)
        incrementVersion = int(num) + increment
        incrementVersion = incrementVersion if incrementVersion >= 0 else 0
        versionFormat = '"%0' + str(padding) + 'd"' + ' % incrementVersion'
        versionStr = eval(versionFormat)
        return '%s%s' % (prefix, versionStr)


def get_context_format():
    """ get context format """
    data = read_config()
    contextFormat = data.get('format')
    return extract_keys(contextFormat)


def extract_keys(text):
    """ get key between <> """
    startKey = '<'
    endKey = '>'
    values = [a.split('>')[0] for a in text.split(startKey) if a]
    return values

def replace_ext(path, ext):
    return '%s%s' % (os.path.splitext(path)[0], ext)

def extract_digit(text):
    return re.findall(r'\d+', text)


def map_format_value(formatName, value, separator):
    data = OrderedDict()
    formatElems = extract_keys(formatName)
    valueElems = value.split(separator)

    for i, formatElem in enumerate(formatElems):
        data[formatElem] = valueElems[i] if i < len(valueElems) else 'none'

    return data

def map_output_format_value(formatName, value, separators=['_', '.']):
    data = OrderedDict()
    formatElems = split_element(formatName, separators)
    valueElems = split_element(value, separators)

    for i, formatElem in enumerate(formatElems):
        if formatElem[0] == '<' and formatElem[-1] == '>':
            data[formatElem[1:-1]] = valueElems[i] if i < len(valueElems) else 'none'

    return data

def compare_output_format_value(formatName, value, separators=['_', '.']):
    data = OrderedDict()
    formatElems = split_element(formatName, separators)
    valueElems = split_element(value, separators)
    match = []
    matchCount = 0.0

    for i, formatElem in enumerate(formatElems):
        if not formatElem[0] == '<' and not formatElem[-1] == '>':
            valueElem = valueElems[i] if i < len(valueElems) else 'none'
            if formatElem == valueElem:
                match.append(True)
                matchCount += 1
            else:
                match.append(False)
    if not match:
        return None

    return (matchCount/len(match))*100

def split_element(name, separators):
    elems = name.split(separators[0])
    allElems = []
    for elem in elems:
        if separators[1] in elem:
            allElems += elem.split(separators[1])
        else:
            allElems.append(elem)
    return allElems


def set(path=''): 
    return ContextPathInfo(path=path)


""" example usage
path = 'P:/ProjectRnd/projectName/asset/work/char/sunny/sunnyBase/model/main/maya/sunnyBase_model_main.v004.TA.ma'
asset = context_info.Info(path=path)
asset.work_path()
# Result: $RFPROJECT/projectName/asset/work/char/sunny/sunnyBase/model/main/maya/sunnyBase_model_main.v004.TA.ma
"""

""" example to get work file for design from shotgun
# 1. Get design work file name
context = context_info.Context()
context.use_sg(sg_utils.sg, project='projectName', entityType='design', entityId=7)
asset = context_info.ContextPathInfo(context=context)

# We need to add more information, app, process, version, step
asset.context.update(workspace='work', app='psd', process='main', version='v001', step='design')
asset.work_name() or asset.name_format('workfile')
# Result: sky_dsn.v001.psd #
"""

""" bible
path = 'P:/projectName/asset/work/char/toadMaster/toadA/model/main/maya/toadA_mdl_main.v001.TA.ma'
asset = context_info.ContextPathInfo(path=path)
asset.context.update(publishVersion='v001')


# path
asset.path.app()
# Result: $RFPROJECT/projectName/asset/work/char/toadMaster/toadA/model/main/maya #
asset.path.daily()
# Result: $RFPROJECT/projectName/daily #
asset.path.data()
# Result: $RFPUBL/projectName/asset/publ/char/toadMaster/toadA/_data #
asset.path.process_data()
# Result: $RFPUBL/projectName/asset/publ/char/toadMaster/toadA/model/main/_data #
asset.path.name()
# Result: $RFPROJECT/projectName/asset/work/char/toadMaster/toadA #

# work
asset.path.workspace()
# Result: $RFPROJECT/projectName/asset/work/char/toadMaster/toadA/model/main/maya #

asset.path.texture()
# Result: $RFPUBL/projectName/asset/publ/char/toadMaster/toadA/textures #

# publish workspace =====================
# publishWorkspacePath
asset.path.publish_workspace()
# Result: $RFPUBL/projectName/asset/publ/char/toadMaster/toadA/model/main/v001/maya #

# publishHeroWorkspacePath
asset.path.publish_workspace_hero()
# Result: $RFPUBL/projectName/asset/publ/char/toadMaster/toadA/model/main/hero/maya #

# publish output =====================
# heroPath
asset.path.hero()
# Result: $RFPUBL/projectName/asset/publ/char/toadMaster/toadA/hero #

# publishOutputPath
asset.path.output()
# Result: $RFPUBL/projectName/asset/publ/char/toadMaster/toadA/model/main/v001 #

# publishOutputPath
asset.path.publish_output()
# Result: $RFPUBL/projectName/asset/publ/char/toadMaster/toadA/model/main/v001/output #

# publishHeroOutputPath
asset.path.output_hero()
# Result: $RFPUBL/projectName/asset/publ/char/toadMaster/toadA/model/main/hero/output #

# publish outputImg =====================
# outputImgPath
asset.path.outputImg()
# Result: $RFPUBL/projectName/asset/publ/char/toadMaster/toadA/model/main/v001/outputImg #

# outputHeroImgPath
asset.path.output_hero_img()
# Result: $RFPUBL/projectName/asset/publ/char/toadMaster/toadA/model/main/hero/outputImg #

# publishPath
asset.path.publish_version()
# Result: $RFPUBL/projectName/asset/publ/char/toadMaster/toadA/model/main/v001 #

asset.path.publish()
# Result: $RFPUBL/projectName/asset/publ #

# custom schemaKey. see "schema_config.yml"
asset.path.scheme(key='publishHeroPath')
# Result: $RFPUBL/projectName/asset/publ/char/toadMaster/toadA/model/main/hero #

# file naming
asset.publish_name()
# Result: u'toadA_mdl_main_md.v001.ma' #

asset.publish_name(hero=True)
# Result: u'toadA_mdl_main_md.hero.ma' #

asset.publish_name(ext='.abc')
# Result: u'toadA_mdl_main_md.hero.abc' #

# see all keys in "workfile_config.yml"
asset.name_format(key='workfile')
# Result: u'toadA_mdl_main.v001.ma' #

# see "asset_output_config.yml"
asset.context.update(publishVersion='v001', res='md')

asset.output_name(outputKey='gpu')
# Result: u'toadA_mdl_gpu_md.v001.abc' #

asset.output_name(outputKey='gpu', hero=True)
# Result: u'toadA_mdl_gpu_md.hero.abc' #


# use with scene
# start with project, episode, sequence, shot information
context.update(project='projectName', entityType='scene', entityGrp='ep01', entityParent='q0010', entityCode='s0010')

# instance contextPathInfo
scene = context_info.ContextPathInfo(context=context)

# init project code
scene.set_project_code(sg)
# set sg_name to name
scene.set_name()
scene.name
# Result: pr_ep01_q0010_s0010 #

"""

""" scene use

from rf_utils.context import context_info
reload(context_info)
from rf_utils.sg import sg_utils
sg = sg_utils.sg
context = context_info.Context()
context.update(project='projectName', entityType='scene', entityGrp='ep01', entityParent='q0010', entityCode='s0010')
scene = context_info.ContextPathInfo(context=context)
scene.set_project_code(sg)
scene.sg_name
# Result: 'pr_ep01_q0010_s0010' #

"""

"""
# guess and distribute context value from filename
asset = context_info.ContextPathInfo(path='$RFPUBL/projectName/asset/publ/prop/chairA/hero/chairA_mtr_rig_main_md.hero.ma')
outputList = asset.guess_outputKey('chairA_mtr_rig_main_md.hero.ma')
outputKey, step = outputList[0]
asset.context.update(step=step)
asset.extract_name('chairA_mtr_rig_main_md.hero.ma', outputKey=outputKey)
asset.mtr_namespace
# Result: 'chairA_mainMtr' #

"""

""" 
# update context by shotName 
# Result: u'pr_ep01_q0070_all' # 
entity.context.update(entityCode='s0010', entity='none')
entity.update_scene_context()
print entity.name 
# Result: u'pr_ep01_q0070_s0010' # 
""" 