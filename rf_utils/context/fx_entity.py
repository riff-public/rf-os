import os 
import sys 
import logging
import lucidity
import rf_config as config
from rf_utils import project_info
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

""" 
Following structure for fx 
P:/Forbidden/vfx/comp/afterFX/FBD_VFX_EP101/FBD_VFX_EP101-010_v01_M.aep
"""


class Config:
    root = {'win32': {'server': 'P:', 'unc': config.Env.serverMap['P:']}}
    hero = 'hero'
    shot_padding = 3
    shot_inc = 10
    sequence = '{project_code}_{episode_code}_{sequence_code}'
    shot = '{project_code}_{episode_code}_{sequence_code}-{shot_code}'
    filename_user = '%s_{version}_{user}.{ext}' % shot
    filename = '%s_{version}.{ext}' % shot
    heropath = '/{project}/{entity_type}/{step}/%s/%s' % (hero, sequence)
    workpath = '/{project}/{entity_type}/{step}/{app}/{project_code}_{episode_code}_{sequence_code}/{filename}'
    entity_type_path = '/{project}/{entity_type}'
    step_path = '/{project}/{entity_type}/{step}'
    app_path = '/{project}/{entity_type}/{step}/{app}'
    sequence_path = '/{project}/{entity_type}/{step}/{app}/%s' % sequence
    # path = 'P:/Forbidden/vfx/comp/afterFX/FBD_VFX_EP101/FBD_VFX_EP101-010_v01_M.aep'


class FXEntity(object):
    """ fx path """
    def __init__(self, input_path=''):
        super(FXEntity, self).__init__()
        self.structure_level = {
            'drive': 0, 'project': 1, 'entity': 2, 'step': 3,
            'app': 4, 'sequence': 5, 'filename': 6}
        self.filename_level = {
            'project_code': 0, 'episode_code': 1, 'sequence_grp': 2,
            'version': 3, 'user': 4}
        self.shot_level = {'sequence_code': 0, 'shot_code': 1}
        self.input_path = input_path
        # self.filename = os.path.basename(input_path)
        self.context = dict()
        self.init_path()

    def init_path(self):
        self.workpath_template = lucidity.Template('workpath', Config.workpath)
        self.sequence_template = lucidity.Template('sequence', Config.sequence)
        self.shot_template = lucidity.Template('shot', Config.shot)
        self.filename_user_template = lucidity.Template('filename_user', Config.filename_user)
        self.filename_template = lucidity.Template('filename', Config.filename)

        if self.input_path:
            root, path = os.path.splitdrive(self.input_path)
            self.context = self.workpath_template.parse(path)
            self.context['root'] = root

            try:
                extra_context = self.filename_user_template.parse(self.context.get('filename'))
            except lucidity.ParseError:
                extra_context = self.filename_template.parse(self.context.get('filename'))
            finally:
                self.context.update(extra_context)

        # self.extract_user()

    def extract_user(self): 
        extra = self.context.get('extra')
        if len(extra.split('_')) > 1: 
            extra_context = self.extra_template.parse(self.context.get('extra'))
            self.context.update(extra_context)


    def init_path2(self):
        elms = self.input_path.split('/')
        if not len(elms) >= len(self.structure_level):
            logger.error('Path not support "{}"'.format(self.input_path))
        else:
            for k, index in self.structure_level.items():
                value = elms[index]
                self.context[k] = value

            filename = self.context.get('filename')
            if filename:
                filename, ext = os.path.splitext(filename)
                self.context['ext'] = ext

                elms = filename.split('_')
                for key, index in self.filename_level.items():
                    if index <= len(elms):
                        value = elms[index]
                        self.context[key] = value

            sequence_grp = self.context.get('sequence_grp')
            if sequence_grp:
                elms = sequence_grp.split('-')
                for key, index in self.shot_level.items():
                    if index <= len(elms):
                        self.context[key] = elms[index]

    @property
    def project(self):
        project = self.context.get('project', None)
        return project_info.ProjectInfo(project)

    @property
    def projectInfo(self):
        ''' for backward compatability '''
        project = self.context.get('project', None)
        return project_info.ProjectInfo(project)

    @property
    def entity_type(self):
        return self.context.get('entity_type', None)

    @property
    def project_code(self):
        return self.context.get('project_code', None)

    @property
    def episode_code(self):
        return self.context.get('episode_code', None)

    @property
    def step(self):
        return self.context.get('step', None)

    @property
    def app(self):
        return self.context.get('app', None)

    @property
    def sequence(self):
        name = self.sequence_template.format(self.context)
        return name

    @sequence.setter
    def sequence(self, text):
        data = self.sequence_template.parse(text)
        self.context.update(data)

    @property
    def sequencecode(self):
        """ FBD_VFX_EP101-010_v01_M.aep """
        return self.context.get('sequence_code', None)

    @property
    def shotcode(self):
        """ FBD_VFX_EP101-010_v01_M.aep """
        return self.context.get('shot_code', None)

    @property
    def shotname(self):
        name = None
        if all(a in self.context.keys() for a in list(self.shot_template.keys())): 
            name = self.shot_template.format(self.context)
        return name

    @shotname.setter
    def shotname(self, text):
        try:
            data = self.shot_template.parse(text)
            self.context.update(data)
        except:
            self.context['shotname'] = None

    def filename(self, user=True):
        if user: 
            return self.filename_user_template.format(self.context)
        return self.filename_template.format(self.context)

    def hero_filename(self, ext):
        context = self.context.copy()
        context.update(version=Config.hero, ext=ext)
        return self.filename_template.format(context)

    @property
    def version(self):
        return self.context.get('version', None)

    @property
    def user(self):
        return self.context.get('user', None)

    @property
    def ext(self):
        return self.context.get('ext', None)

    @property
    def path(self): 
        return Path(self)

    def update(self, **kwargs): 
        pass 

    
class Path(object):
    """docstring for Path"""
    def __init__(self, entity):
        super(Path, self).__init__()
        self.entity = entity

    def entity_type(self):
        return self._setup_path(Config.entity_type_path)

    def step(self):
        return self._setup_path(Config.step_path)

    def app(self):
        return self._setup_path(Config.app_path)

    def sequence(self):
        return self._setup_path(Config.sequence_path)

    def heropath(self):
        return self._setup_path(Config.heropath)

    def heropath(self):
        return self._setup_path(Config.heropath)

    def _setup_path(self, config): 
        template = lucidity.Template('template', config)
        if all(a in self.entity.context.keys() for a in list(template.keys())):
            path = template.format(self.entity.context)
            return RootPath(path)
        else:
            missing = [a for a in list(template.keys()) if a not in self.entity.context.keys()]
            logger.error('Missing key {}'.format(missing))


class RootPath(object):
    """docstring for RootPath"""
    def __init__(self, path):
        super(RootPath, self).__init__()
        self.path_config = Config.root[sys.platform]
        self.path = path
        self.combine_path = os.path.join(self.path_config['server'], path)

    def __repr__(self):
        return self.combine_path

    def __str__(self):
        return self.combine_path

    def unc(self): 
        return os.path.join(self.path_config['unc'], self.path)


def test(): 
    path = 'P:/Forbidden/vfx/comp/afterFX/FBD_VFX_EP101/FBD_VFX_EP101-010_v01_M.aep'
    print('input path {}'.format(path))

    def test_print(fx): 
        print('\nContext')
        print('- project {}'.format(fx.project))
        print('- entity_type {}'.format(fx.entity_type))
        print('- episode_code {}'.format(fx.episode_code))
        print('- step {}'.format(fx.step))
        print('- app {}'.format(fx.app))
        print('- sequence {}'.format(fx.sequence))
        print('- shotcode {}'.format(fx.shotcode))
        print('- shotname {}'.format(fx.shotname))
        print('- filename {}'.format(fx.filename(user=False)))
        print('- filename_user {}'.format(fx.filename(user=True)))
        print('- version {}'.format(fx.version))
        print('- user {}'.format(fx.user))
        print('- ext {}'.format(fx.ext))
        print('\nPath')
        print('- entity path {}'.format(fx.path.entity_type()))
        print('- entity path unc {}'.format(fx.path.entity_type().unc()))
        print('- step path {}'.format(fx.path.step()))
        print('- step path unc {}'.format(fx.path.step().unc()))
        print('- app path {}'.format(fx.path.app()))
        print('- app path unc {}'.format(fx.path.app().unc()))
        print('- sequence path {}'.format(fx.path.sequence()))
        print('- sequence path unc {}'.format(fx.path.sequence().unc()))

    fx = FXEntity(path)
    test_print(fx)
    sequence_code = 'EP102'
    shot_code = '020'
    user = 'Preaw'
    version = 'v02'
    print('--------------------------------')
    print('Change context \nsequence_code "{}" shot_code "{}" user "{}" version "{}"'.format(sequence_code, shot_code, user, version))
    fx.context.update(
        sequence_code=sequence_code, 
        shot_code=shot_code, 
        user=user, 
        version=version)
    test_print(fx)

    print('--------------------------------')
    print('\nBuild context')
    print('root "P:", project "Forbidden", entity_type "fx", step "comp", app "afterFX"')
    fx = FXEntity()
    fx.context.update(root='P:', project='Forbidden', entity_type='fx', step='comp', app='afterFX')
    print('shotname = "FBD_VFX_EP101-010"')
    fx.shotname = 'FBD_VFX_EP101-010'
    print('fx.path.sequence() {}'.format(fx.path.sequence()))

    print('Project info')
    print(fx.project.render.watermark(key='default'))
