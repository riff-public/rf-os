import os 
import sys 
from rf_utils import project_info


class ProjectConfig(object):
    """docstring for ProjectConfig"""
    def __init__(self, project=None):
        super(ProjectConfig, self).__init__()
        self.projectInfo = project_info.ProjectInfo(project=project)
        self.configData = self.projectInfo.config_data()

        self.schema = self.configData.get('schema')
        self.contextKey = self.schema.get('contextKey')
        self.pathLevel = self.schema.get('pathLevel')

        # entity type 
        self.asset = self.contextKey.get('asset').get('key')
        self.scene = self.contextKey.get('scene').get('key')




class Schema(ProjectConfig):
    """docstring for Schema"""
    def __init__(self, project=None, schema=None):
        super(Schema, self).__init__(project=project)
        self.project = project 
        self.schema = schema

    def __str__(self): 
        return self.schema

    def __repr__(self): 
        return self.schema

    def elems(self): 
        return extract_keys(self.schema)

    def work_schema(self, entity): 
        return self.pathLevel.get(entity).get('workPath')

    def publish_schema(self, entity): 
        return self.pathLevel.get(entity).get('publishPath')

    def hero_schema(self, entity): 
        return self.pathLevel.get(entity).get('heroPath')

    def cache_schema(self, entity): 
        return self.pathLevel.get(entity).get('cachePath')

    def outputImg_schema(self, entity): 
        return self.pathLevel.get(entity).get('outputImgPath')

    def key(self, key): 
        return self.contextKey.get(key)

def extract_keys(text): 
    """ get key between <> """
    startKey = '<'
    endKey = '>'
    values = [a.split('>')[0] for a in text.split(startKey) if a]
    return values 

