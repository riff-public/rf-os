from datetime import datetime 

class Duration():
    """docstring for Duration"""
    def __init__(self):
        self.duration = None 

    def __enter__(self): 
        self.start = datetime.now()

    def __exit__(self, *args): 
        self.end = datetime.now()
        self.duration = self.end - self.start 
        print(self.duration)


def time_diff(datetime1, datetime2): 
    diff = datetime2 - datetime1
    days, seconds = diff.days, diff.seconds
    return days, seconds


def time_to_str(datetime, showdate=True, showtime=True): 
    date_str = datetime.strftime("%m-%d-%Y") if showdate else ''
    time_str = datetime.strftime("%H:%M:%S") if showtime else ''
    result = ' '.join([date_str, time_str]).lstrip().rstrip()
    return result


def seconds_to_hms(days, seconds): 
    hours = days * 24 + seconds // 3600
    minutes = (seconds % 3600) // 60
    seconds = seconds % 60
    result = '%02d:%02d:%02d' % (hours, minutes, seconds)
    return result
