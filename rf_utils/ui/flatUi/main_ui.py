from PySide2 import QtWidgets, QtCore, QtGui
import os
import sys

dirname = os.path.dirname(__file__)
ICON = dirname + "/icons/"
FONT = dirname + "/font/"
CSS = dirname + "/css/style.css"
CSS_IMAGES = dirname + "/css/images"

with open(CSS, "r") as css_files:
    stylesheet = css_files.read().replace("PATH", CSS_IMAGES).replace("\\", "/")

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, ui_name="main_ui", size=(500,500), title = "", titleBarHeight=35, widget="", onTop=False, parent=None):
        super(MainWindow, self).__init__(parent)

        self.setObjectName(ui_name)
        self.title = title
        self.width, self.height = size
        self.maxWidth = 1920
        self.maxHeight = 1080
        self.titleBarHeight = titleBarHeight
        self.resize(self.width, self.height + self.titleBarHeight)
        self.setMaximumSize(self.maxWidth, self.maxHeight)
        self.widget = widget
        self.onTop = onTop

        if self.onTop:
            self.setWindowFlags(self.windowFlags() | QtCore.Qt.Window)
            self.setWindowModality(QtCore.Qt.WindowModal)


        ## Frameless ##
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.Window)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.setWindowOpacity(1)


        ## Initial Pos ##
        self.y = 0

        self.centralWidget = QtWidgets.QWidget(self)
        self.centralWidget_layout = QtWidgets.QHBoxLayout(self.centralWidget)
        self.setCentralWidget(self.centralWidget)

        self.__build_ui()


    def __build_ui(self):
        self.main_frame = QtWidgets.QFrame(self.centralWidget)
        self.main_frame_layout = QtWidgets.QVBoxLayout(self.main_frame)
        self.main_frame_layout.setContentsMargins(0, 0, 0, 0)
        self.main_frame_layout.setSpacing(0)

        self.centralWidget_layout.addWidget(self.main_frame)

        ## ----------------- Title Bar Area ----------------- ##

        self.top_frame = QtWidgets.QFrame(self.main_frame)
        self.top_frame_layout = QtWidgets.QHBoxLayout(self.top_frame)
        self.top_frame_layout.setContentsMargins(0, 0, 0, 0)
        self.top_frame_layout.setSpacing(0)
        self.top_frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.top_frame.setMinimumHeight(self.titleBarHeight)
        self.top_frame.setMaximumHeight(self.titleBarHeight)
        self.top_frame.setStyleSheet("background-color: #141b20;")

        ## Icon and Title ##

        ICON_CSS = """background: transparent;
        background-image: url({}/pvu.png);
        background-position: center;
        background-repeat: no-repeat;
        """.format(ICON).replace("\\", "/")


        icon_frame = QtWidgets.QFrame(self.top_frame)
        icon_frame.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred))
        icon_frame_layout = QtWidgets.QHBoxLayout(icon_frame)
        icon_frame_layout.setContentsMargins(0, 0, 0, 0)
        icon_frame_layout.setSpacing(0)

        icon = QtWidgets.QFrame(icon_frame)
        icon.setMaximumSize(self.titleBarHeight, self.titleBarHeight)
        icon.setStyleSheet(ICON_CSS)
        icon_frame_layout.addWidget(icon)

        title_label = QtWidgets.QLabel(self.title, icon_frame)
        title_label.setStyleSheet("font: 75 10pt 'Open Sans SemiBold'; color: rgb(200, 200, 200);")
        icon_frame_layout.addWidget(title_label)
        self.top_frame_layout.addWidget(icon_frame)

        ## Buttons ##

        btns_frame = QtWidgets.QFrame(self.top_frame)
        btns_frame_layout = QtWidgets.QHBoxLayout(btns_frame)
        btns_frame_layout.setContentsMargins(0, 0, 0, 0)
        btns_frame_layout.setSpacing(0)

        self.close_btn = QtWidgets.QPushButton(btns_frame)
        self.close_btn.setStyleSheet("""
            QPushButton {   
                border: none;
                background-color: transparent;
            }
            QPushButton:hover {
                background-color: rgb(52, 59, 72);
            }
            QPushButton:pressed {   
                background-color: rgb(200, 0, 0);
            }
            """)
        self.close_btn.setIcon(QtGui.QIcon(ICON + "cil-x.png"))
        self.close_btn.setFixedSize(self.titleBarHeight, self.titleBarHeight)
        self.close_btn.clicked.connect(lambda: self.close())

        btns_frame_layout.addWidget(self.close_btn)

        self.top_frame_layout.addWidget(btns_frame)

        self.main_frame_layout.addWidget(self.top_frame)
        ## -------------------------------------------------- ##


        ## ----------------- Widget Area ----------------- ##

        self.center_frame = QtWidgets.QFrame(self.main_frame)
        self.center_frame.setStyleSheet(stylesheet)
        self.center_frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.center_frame_layout = QtWidgets.QGridLayout(self.center_frame)
        self.center_frame_layout.setContentsMargins(0, 0, 0, 0)
        self.center_frame_layout.setSpacing(0)

        self.main_frame_layout.addWidget(self.center_frame)

        ## Shadow ##
        self.shadow = QtWidgets.QGraphicsDropShadowEffect(self)
        self.shadow.setBlurRadius(10)
        self.shadow.setXOffset(0)
        self.shadow.setYOffset(0)
        self.shadow.setColor(QtGui.QColor(0, 0, 0, 500))
        self.main_frame.setGraphicsEffect(self.shadow)

        ## Add Custom Widget ##
        if self.widget:
            self.widget.setAutoFillBackground(True)
            self.center_frame_layout.addWidget(self.widget)

        ## ------------------------------------------------ ##

    ## ----------------- Mouse Event ----------------- ##

    def mousePressEvent(self, event):
        self.y = 0
        self.offset = event.pos()
        self.y = self.offset.toTuple()[-1]
        
    def mouseMoveEvent(self, event):
        if event.buttons() == QtCore.Qt.LeftButton:
            if self.y and self.y <= self.titleBarHeight:
                self.move(event.globalPos() - self.offset)

    def mouseReleaseEvent(self, event):
        self.y = 0

    ## ------------------------------------------------ ##


def main(widget=""):
    app = QtWidgets.QApplication(sys.argv)
    win = MainWindow(size=(380,580), widget=widget)
    win.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
