import os

curPath = os.path.dirname(os.path.abspath(__file__))

class Icon(object):
	def __init__(self, name):
		self.icon = "{}/icons/{}.png".format(curPath, name).replace("\\", "/")
		