from PySide2 import QtWidgets, QtCore, QtGui
import os
import sys

dirname = os.path.dirname(__file__)
ICON = dirname + "/icons/"
FONT = dirname + "/font/"
CSS = dirname + "/css/style.css"
CSS_IMAGES = dirname + "/css/images"

with open(CSS, "r") as css_files:
	stylesheet = css_files.read().replace("PATH", CSS_IMAGES).replace("\\", "/")


class MainWindow(QtWidgets.QMainWindow):
	def __init__(self, size=(500,500), title = "", titleBarHeight=35, rounded=3, opacity=1, widget="", statusBar=False, enableResize=False, parent=None):
		super(MainWindow, self).__init__(parent)

		self.setObjectName("MainWindow_win")
		self.title = title
		self.width, self.height = size
		self.maxWidth = 1920
		self.maxHeight = 1080
		self.titleBarHeight = titleBarHeight
		self.resize(self.width, self.height + self.titleBarHeight)
		self.setMaximumSize(self.maxWidth, self.maxHeight)
		self.statusBar = statusBar
		self.enableResize = enableResize

		## Frameless ##
		self.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.Window)
		self.setWindowOpacity(opacity)

		## Rounded ##
		self.rounded = rounded
		self.roundedUI(self.rounded)

		self.y = 0

		self.widget = widget
		self.centralwidget = QtWidgets.QWidget()
		self.__top_layout = QtWidgets.QVBoxLayout(self.centralwidget)
		self.__top_layout.setContentsMargins(0,0,0,0)
		self.__top_layout.setSpacing(0)
		self.setCentralWidget(self.centralwidget)
		self.__build_ui()


	def __statusBar(self, resize):
		self.myStatus = QtWidgets.QStatusBar()
		self.myStatus.showMessage("Yggdrazil Group")
		self.myStatus.setSizeGripEnabled(resize)
		self.setStatusBar(self.myStatus)

	def __build_ui(self):
		if self.statusBar:
			self.__statusBar(self.enableResize)
		## Title Bar Frame ##
		top_frame = QtWidgets.QFrame()
		top_frame.setStyleSheet("background-color: #141b20;")
		top_frame.setMinimumSize(0, self.titleBarHeight)
		top_frame.setMaximumSize(self.maxWidth, self.titleBarHeight)
		top_frame_layout = QtWidgets.QHBoxLayout(top_frame)
		top_frame_layout.setSpacing(0)
		top_frame_layout.setContentsMargins(0,0,0,0)

		## Logo Frame ##
		logo_frame = QtWidgets.QFrame()
		logo_frame.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred))
		logo_frame_layout = QtWidgets.QHBoxLayout(logo_frame)
		logo_frame_layout.setContentsMargins(0, 0, 0, 0)
		logo = QtWidgets.QFrame()
		logo.setMaximumSize(self.titleBarHeight, self.titleBarHeight)
		logo.setStyleSheet(""" background: transparent;
				background-image: url(T:/series/MOTU/lib/lgt/tools/flatUi/icons/maya.png);
				background-position: center;
				background-repeat: no-repeat;
				""")
		title_label = QtWidgets.QLabel(self.title)
		title_label.setStyleSheet("font: 75 10pt 'Open Sans SemiBold'; color: rgb(200, 200, 200);")
		logo_frame_layout.addWidget(logo)
		logo_frame_layout.addWidget(title_label)
		top_frame_layout.addWidget(logo_frame)

		## Button Frame ##
		btn_frame = QtWidgets.QFrame()
		btn_frame_layout = QtWidgets.QHBoxLayout(btn_frame)
		btn_frame_layout.setContentsMargins(0, 0, 0, 0)
		btn_frame_layout.setSpacing(0)
		self.minimize_btn = QtWidgets.QPushButton()
		self.minimize_btn.setStyleSheet("""
			QPushButton {	
				border: none;
				background-color: transparent;
			}
			QPushButton:hover {
				background-color: rgb(52, 59, 72);
			}
			QPushButton:pressed {	
				background-color: rgb(85, 170, 255);
			}
			""")
		self.minimize_btn.setIcon(QtGui.QIcon(ICON + "cil-window-minimize.png"))
		self.minimize_btn.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))
		self.minimize_btn.setFixedSize(self.titleBarHeight, self.titleBarHeight)
		self.minimize_btn.clicked.connect(lambda: self.showMinimized())

		self.close_btn = QtWidgets.QPushButton()
		self.close_btn.setStyleSheet("""
			QPushButton {	
				border: none;
				background-color: transparent;
			}
			QPushButton:hover {
				background-color: rgb(52, 59, 72);
			}
			QPushButton:pressed {	
				background-color: rgb(200, 0, 0);
			}
			""")
		self.close_btn.setIcon(QtGui.QIcon(ICON + "cil-x.png"))
		self.close_btn.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding))
		self.close_btn.setFixedSize(self.titleBarHeight, self.titleBarHeight)
		self.close_btn.clicked.connect(lambda: self.close())
		#btn_frame_layout.addWidget(self.minimize_btn)
		btn_frame_layout.addWidget(self.close_btn)
		top_frame_layout.addWidget(btn_frame)


		## Center Frame ## 
		center_frame = QtWidgets.QFrame()
		center_frame.setStyleSheet(stylesheet)
		center_frame.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding))
		center_frame_layout = QtWidgets.QVBoxLayout(center_frame)
		center_frame_layout.setContentsMargins(0,0,0,0)
		if self.widget:
			#self.widget.setStyleSheet(stylesheet)
			center_frame_layout.addWidget(self.widget)
		self.__top_layout.addWidget(top_frame)
		self.__top_layout.addWidget(center_frame)

	## Mouse Event ##
	def mousePressEvent(self, event):
		self.y = 0
		self.offset = event.pos()
		self.y = self.offset.toTuple()[-1]
		
	def mouseMoveEvent(self, event):
		if event.buttons() == QtCore.Qt.LeftButton:
			if self.y and self.y <= self.titleBarHeight:
				self.move(event.globalPos() - self.offset)

	def mouseReleaseEvent(self, event):
		self.y = 0

	def resizeEvent(self, event):
		self.roundedUI(self.rounded)

	def roundedUI(self, rounded):
		radius = rounded
		path = QtGui.QPainterPath()
		path.addRoundedRect(QtCore.QRectF(self.rect()), radius, radius)
		mask = QtGui.QRegion(path.toFillPolygon().toPolygon())
		self.setMask(mask)


def main(widget=""):
	app = QtWidgets.QApplication(sys.argv)
	QtGui.QFontDatabase.addApplicationFont(FONT + "OpenSans-Regular.ttf")
	QtGui.QFontDatabase.addApplicationFont(FONT + "OpenSans-SemiBold.ttf")
	win = MainWindow(size=(380,580), titleBarHeight=35, widget=widget, statusBar=False)
	win.show()
	sys.exit(app.exec_())

if __name__ == "__main__":
	main()