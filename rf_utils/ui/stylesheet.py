import os
import sys
import rf_config as config
import logging 
is_python3 = True if sys.version_info[0] > 2 else False

from Qt.QtCore import QDir, QFile

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

def set_default(app): 
    # set styleSheet 
    styleSheetPath = config.Software.mayaStyleSheet
    if os.path.exists(styleSheetPath) :
        QDir.addSearchPath('images', os.path.join(os.path.dirname(styleSheetPath), 'images'))
        try:
            app.setStyle('plastique')
            file = QFile(styleSheetPath)
            file.open(QFile.ReadOnly | QFile.Text)
            if is_python3:
                app.setStyleSheet(str(file.readAll(), 'utf-8'))
            else:
                app.setStyleSheet(bytes(file.readAll()).decode('utf-8'))
        except Exception as e:
            print(e)
            logger.info(str(e))
