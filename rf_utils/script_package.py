import sys
import os
import shutil
import argparse
import py_compile
import imp
from glob import glob
from pprint import pprint

def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', dest='source_list', help='source directories', required=True)
    parser.add_argument('-d', dest='destination', type=str, help='destination directory', required=True)
    parser.add_argument('-i', dest='include_source', type=bool, help='To include .py source code or not', default=False)
    parser.add_argument('-ie', dest='ignore_exts', nargs='+', help='List of extensions to NOT include', default=[])
    parser.add_argument('-ip', dest='ignore_paths', nargs='+', help='List of text in a path to NOT include', default=[])
    return parser

def generate_binary(file):
    binary_path = file.replace('.py', '.pyc')
    if not os.path.exists(binary_path):
        try:
            imp.load_source('', file)
            py_compile.compile(file)
        except Exception, e:
            print('**Load Error: {}'.format(file))
            print(e)

    # in case compile failed or pyc exists
    if os.path.exists(binary_path):
        return binary_path

def get_init_files_from_path(file):
    env_root = os.environ['RFSCRIPT'].replace('\\', '/')
    results = []
    if env_root in file:
        while True:
            fdir = os.path.dirname(file)
            if fdir == env_root:
                break
            files = os.listdir(fdir)
            for f in files:
                if f in ('__init__.py', '__init__.pyc'):
                    fp = '{}/{}'.format(fdir, f)
                    results.append(fp)
            file = fdir

    return results

def main(source_list, destination, include_source=False, ignore_exts=[], ignore_paths=[]):
    '''
    source_list = list, .txt file path ie; ['core/rf_maya/..', ...]
    destination = str
    '''
    env_root = os.environ['RFSCRIPT'].replace('\\', '/')
    paths = []
    if os.path.exists(source_list):  # it's a text file
        with open(source_list, 'r') as f:
            for line in f.readlines():
                line = line.replace('\n', '')
                line = line.replace('\t', '')
                line = line.strip()
                if line:
                    paths.append(line)
    files_to_copy = set()
    for path in paths:
        hardcopy = False
        if path.startswith('*'):
            hardcopy = True
            path = path.replace('*', '')

        # add script root to path
        path = '{}/{}'.format(env_root, path)
        if not os.path.exists(path):
            print('**Error, Path does not exists: {}'.format(path))
            continue

        path = path.replace('\\', '/')

        # get files
        if os.path.isdir(path):
            all_files = [y.replace('\\', '/') for x in os.walk(path) for y in glob('{}/{}'.format(x[0], '*')) if os.path.isfile(y)]   
        elif os.path.isfile(path):
            all_files = [path]

        if hardcopy:
            for file in all_files:
                files_to_copy.add(file)
        else:
            for file in all_files:
                fn, ext = os.path.splitext(file)
                if ext in ('.py', '.pyc'):
                    # include __init__.py from path
                    inits = get_init_files_from_path(file)
                    for i in inits:
                        if i not in files_to_copy:
                            files_to_copy.add(i)

                    if ext == '.py':
                        # .py
                        if include_source:
                            files_to_copy.add(file)
                        # generate/get .pyc
                        binary_file = generate_binary(file)
                        if binary_file:
                            files_to_copy.add(binary_file)
                        else:
                            print('**Failed to compile: {}'.format(file))
                    elif ext == '.pyc':
                        if file not in files_to_copy:
                            files_to_copy.add(file)
                else:
                    if ext not in ignore_exts:
                        for ig in ignore_paths:
                            if ig in file:
                                break
                        else:
                            files_to_copy.add(file)

    files_to_copy = list(files_to_copy)
    # pprint(files_to_copy)
    print('Copying files...')
    for path in files_to_copy:
        des_path = path.replace(env_root, destination)
        des_dir = os.path.dirname(des_path)
        # print des_path
        if not os.path.exists(des_dir):
            os.makedirs(des_dir)
        shutil.copy(path, des_path)
        # print('Copied:\n    {}\n    {}'.format(path, des_path))
    print ('Copy success.')

if __name__ == '__main__':
    print('Start packing...')

    parser = setup_parser('Script file packing')
    params = parser.parse_args()
    
    main(source_list=params.source_list, 
        destination=params.destination, 
        include_source=params.include_source,
        ignore_exts=params.ignore_exts,
        ignore_paths=params.ignore_paths)

'''
python %RFSCRIPT%/core/rf_utils/script_package.py -s %RFSCRIPT%/core/rf_utils/hnm_anim_script_package.txt -d P:/Hanuman/ftp/to_OS/O/Pipeline -ip "/.git/" ".gitignore"

'''