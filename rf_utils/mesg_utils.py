#!/usr/bin/env python
# -- coding: utf-8 --
import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

def downstream_task_warning(affectedTasks): 
    messages = 'ระวัง กรุณาอ่านหน่อยครับ!! \n'
    messages += 'แผนกที่ทำงานต่อจากเรามี %s แผนกที่เริ่มไปแล้ว \n' % (len(affectedTasks))
    steps = []
    sgUsers = []

    for taskEntity in affectedTasks: 
        content = taskEntity['content']
        step = taskEntity['step']['name']
        assignees = taskEntity['task_assignees']
        sgUsers += assignees
        userString = (',').join([a.get('name') for a in assignees])
        headers = '"%s - แผนก %s" กำลังทำหรือเสร็จงาน task "%s" แล้ว\n' % (userString, step, content)
        messages += headers
        steps.append([userString, step])

    conclusionString = ('\n').join(['%s - %s' % (user, step) for user, step in steps])
    messages += 'ซึ่งเขาจะต้อง update งานจาก version ใหม่ของเรา \nดังนั้นหลัง publish อย่าลืมเช็ค status ว่าแผนกเขาเป็น fix หรือยัง\nถ้าไม่แน่ใจ ให้บอกพี่น้อง พี่ต่าย หรือบอก \n%s โดยตรงก็ได้ครับ\n' % conclusionString
    messages += 'ขอบพระคุณอย่างสูง'

    return {'users': sgUsers, 'message': messages}