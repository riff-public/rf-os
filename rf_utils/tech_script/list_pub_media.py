import os
import sys
import logging
from collections import OrderedDict
from rf_utils.context import context_info
from rf_utils.sg import sg_utils
from rf_utils import file_utils
import rf_config as config
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
sg = sg_utils.sg

IGNORE_FILE_PATTERN = ["resize"]
versionStr = '$version'
mediaExt = ['.mov']

def find_media(project, episode, step, process='main'):
    sgShots = fetch_episode(project, episode)
    info = OrderedDict()
    if sgShots:
        context = context_info.Context()
        context.use_sg(sg_utils.sg, project, 'scene', entityName=sgShots[0]['code'])
        scene = context_info.ContextPathInfo(context=context)

        for shot in sgShots:
            shotName = shot['code']
            medias = find_shot_media(scene, shotName, step, process)
            info[shotName] = medias

    return info


def find_shot_media(entity, shotName, step='anim', process='main'):
    entity.context.update(step=step, process=process, entity=shotName, publishVersion=versionStr)
    outputPath = entity.path.scheme(key='outputImgPath').abs_path()  # R:/
    mediaFiles = get_mov_files(outputPath)

    # ** backward compatibility for P: drive publishes
    if not mediaFiles:
        outputDrive, outputDir = os.path.splitdrive(outputPath)
        oldDrive = os.environ[config.Env.publVar]
        outputPath = '{}/{}'.format(oldDrive, outputDir)
        mediaFiles = get_mov_files(outputPath)

    return mediaFiles

def get_mov_files(outputPath):
    mediaFiles = []
    # version
    versionPath = outputPath.split(versionStr)[0]
    versions = file_utils.list_folder(versionPath) if os.path.exists(versionPath) else []

    for version in versions:
        path = outputPath.replace(versionStr, version)

        logger.debug('browse %s ...' % path)
        mediaFile = file_utils.list_file(path) if os.path.exists(path) else []
        movFiles = [a for a in mediaFile if os.path.splitext(a)[-1] in mediaExt]

        for mov in movFiles:
            logger.debug('media %s' % mediaFile)
            for ignore_word in IGNORE_FILE_PATTERN:
                if ignore_word not in mov:
                    mediaFiles.append('%s/%s' % (path, mov))
    return mediaFiles

def fetch_episode(project, episode):
    filters = [['project.Project.name', 'is', project], ['sg_episode.Scene.code', 'is', episode]]
    fields = ['code', 'id', 'sg_sequence_code']
    results = sg.find('Shot', filters, fields)
    return results
