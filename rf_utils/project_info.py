# project utils
import os
import sys
import re
import logging
from datetime import datetime
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import rf_config as config
from rf_utils import file_utils
RFSCRIPT_VAR = config.Env.scriptVar
PROJECT_VAR = config.Env.projectVar
PUBL_VAR = config.Env.publVar

try:
    script_root = sys._MEIPASS
except Exception:
    script_root = os.environ.get(RFSCRIPT_VAR)

caches = dict()
sgCaches = dict()

class Path:
    projectConfig = 'core/rf_config/project'
    configFile = 'project_config.yml'
    projectTemplate = 'core/rf_template'
    defaultProject = 'default'
    structurePath = '_structure'

    launcherConfig = 'core/rf_launcher/project_config'
    launcherSuffix = '_config.yml'

    assetExportConfig = 'core/rf_app/export/config'
    assetExportSuffix = '.yml'

    sceneExportConfig = 'core/rf_app/publish/scene/export/config'
    sceneExportSuffix = '_export_config.yml'

    template = 'core/rf_template'

    cleanConfig = 'clean_config.yml'

    qcConfig = 'core/rf_qc/config'
    qcSuffix = '.yml'



class ProjectInfo(object):
    """docstring for ProjectInfo"""
    def __init__(self, project=None, context=None):
        super(ProjectInfo, self).__init__()
        self.project = project
        self.context = context
        # self.caches = dict()
        self.init_project()

    def __repr__(self):
        return self.project

    def __str__(self):
        return self.project

    def init_project(self): 
        if not self.project: 
            self.project = Path.defaultProject

    def exists(self):
        """ if input project valid """
        return True if self.project in self.list_all(False) else False

    def check_root(self):
        """ check if root config match current env """
        env = self.env()
        checkVar = [PROJECT_VAR, PUBL_VAR]

        for var in checkVar:
            if not os.environ.get(var) == env.get(var):
                return False
        return True

    def list_all(self, asObject=True):
        """ list all project in db """
        if asObject:
            projects = self.list_all(False)
            return [ProjectInfo(a) for a in sorted(projects)]
        else:

            return file_utils.list_folder('{}/{}'.format(os.environ[RFSCRIPT_VAR], Path.projectConfig))
            # return ['Two_Heroes', 'SevenChickMovie', 'CloudMaker', 'projectName', 'pucca']

    @property 
    def sg(self): 
        if not sgCaches: 
            from rf_utils.sg import sg_utils 
            projects = sg_utils.sg.find('Project', [], ['name', 'id', 'sg_status'])
            for project in projects: 
                sgCaches[project.get('name')] = project 
            
            for project in self.list_all(False): 
                if not project in sgCaches.keys(): 
                    sgCaches[project] = dict()
            return sgCaches[project]

        else: 
            return sgCaches.get(self.name(), dict())

    @property 
    def active(self): 
        return self.sg.get('sg_status')

    @property
    def rootProject(self):
        return self.config_data().get('env').get(config.Env.projectVar)

    @property
    def rootPubl(self):
        return self.config_data().get('env').get(config.Env.publVar)

    def name(self):
        """ name of a project """
        return self.project

    def config_dir(self): 
        projectConfigDir = '{}/{}/{}'.format(script_root, Path.projectConfig, self.project)
        return projectConfigDir

    def config_file(self):
        """ get config file based on project. If project not exists, use default project config """
        projectSpecificConfig = self.project_config_file
        defaultProjectConfig = self.default_config_file
        configFile = projectSpecificConfig if os.path.exists(projectSpecificConfig) else defaultProjectConfig
        return configFile

    @property
    def default_config_file(self): 
        defaultProjectConfig = '{}/{}/{}/{}'.format(script_root, Path.projectConfig, Path.defaultProject, Path.configFile)
        return defaultProjectConfig

    @property
    def project_config_file(self): 
        projectSpecificConfig = '{}/{}/{}/{}'.format(script_root, Path.projectConfig, self.project, Path.configFile)
        return projectSpecificConfig

    def list_configs(self): 
        configDir = self.config_dir()
        configs = file_utils.list_file(configDir)
        configFiles = ['{}/{}'.format(configDir, a) for a in configs]
        return configFiles 

    def config_dict(self): 
        configDict = dict()
        files = self.list_configs()

        for file in files: 
            name, ext = os.path.splitext(os.path.basename(file))
            configDict[name] = file

        return configDict

    # def config_data(self):
    #     # logger.debug('reading config -> {}'.format(self.config_file()))
    #     configFile = self.config_file()
    #     if not configFile in caches.keys():
    #         data = file_utils.ymlLoader(configFile) if os.path.exists(configFile) else dict()
    #         caches[configFile] = data
    #         return data
    #     return caches[configFile]

    def apply_overrides(self, data):
        if not self.context:
            return data
        entity_name = self.context.name
        # needs to copy to new dictionary so we don't overwrite the original cache
        new_data = data.copy()
        if 'overrides' in new_data:
            for override_dict in new_data['overrides']:
                if 'name' in override_dict and 'values' in override_dict:
                    name_match = re.match(override_dict['name'], entity_name)
                    if name_match:
                        for key, value in override_dict['values'].items():
                            new_data[key] = value
        return new_data

    def config_data(self):
        """ read project config """ 
        # change order reading config 
        # read default first then override with project specific 
        configFile = self.config_file()
        data = dict()
        if not configFile in caches.keys():
            # default data 
            data = file_utils.ymlLoader(self.default_config_file)
            # project data 
            if os.path.exists(self.project_config_file): 
                overrideData = file_utils.ymlLoader(self.project_config_file)
                data.update(overrideData)

                # store data in caches
            caches[configFile] = data
        else:
            data = caches[configFile]

        return data

    def launcher_config(self): 
        # "D:\Dropbox\script_server\core\rf_launcher\project_config\SevenChickMovie_config.yml"
        config = '{}/{}/{}{}'.format(script_root, Path.launcherConfig, self.project, Path.launcherSuffix)
        return config 

    def export_asset_config(self): 
        # D:\Dropbox\script_server\core\rf_app\export\config\SevenChickMovie.yml
        config = '{}/{}/{}{}'.format(script_root, Path.assetExportConfig, self.project, Path.assetExportSuffix)
        return config 

    def export_scene_config(self): 
        # "D:\Dropbox\script_server\core\rf_app\publish\scene\export\config\SevenChickMovie_export_config.yml"
        config = '{}/{}/{}{}'.format(script_root, Path.sceneExportConfig, self.project, Path.sceneExportSuffix)
        return config 

    def qc_config(self): 
        # "D:\Dropbox\script_server\core\rf_qc\config\SevenChickMovie.yml"
        config = '{}/{}/{}{}'.format(script_root, Path.qcConfig, self.project, Path.qcSuffix)
        return config 

    def get_template_path(self):
        projectTemplate = '{}/{}/{}'.format(script_root, Path.projectTemplate, self.project)
        defaultTemplate = '{}/{}/{}'.format(script_root, Path.projectTemplate, 'default')
        return projectTemplate if os.path.exists(projectTemplate) else defaultTemplate

    def get_template_structure(self): 
        projectTemplate = '{}/{}/{}/{}'.format(script_root, Path.projectTemplate, Path.structurePath, self.project)
        defaultTemplate = '{}/{}/{}/{}'.format(script_root, Path.projectTemplate, Path.structurePath, 'default')
        return projectTemplate if os.path.exists(projectTemplate) else defaultTemplate

    def template_file(self, step, app):
        path = ('/').join([self.get_template_path(), step, app])
        return path

    def env(self):
        data = self.config_data()
        return data.get('env')

    def dcc(self): 
        return self.config_data().get('dcc', dict())

    def render_data(self):
        data = self.config_data().get('render', dict())
        data = self.apply_overrides(data)
        return data

    def permission(self): 
        return self.config_data().get('permission',  dict())

    def task_data(self):
        return self.config_data().get('task', dict())

    def schema_data(self):
        return self.config_data().get('schema', dict())

    def team_data(self): 
        return self.config_data().get('team', dict())

    def notification_trigger_data(self): 
        return self.config_data().get('notitrigger', dict())

    def notification_message_data(self): 
        return self.config_data().get('notimessage', dict())

    def output_data(self):
        # print self.config_data()
        return self.config_data().get('output').get('asset')

    def context_key(self):
        return self.schema_data().get('contextKey')

    def steps(self):
        return self.config_data().get('steps')

    def workfile(self):
        return self.schema.context_key().get('workfile').get('map')

    def schema_config(self, entity):
        return self.schema_data().get('pathLevel').get(entity)

    @property
    def render(self):
        return Render(self)

    @property
    def task(self):
        return Task(self)

    @property
    def schema(self):
        return Schema(self)

    @property
    def fileConfig(self):
        return WorkfileConfig(self)

    @property
    def step(self):
        return Step(self)

    def set_project_env(self):
        data = self.env()
        for key, path in data.items():
            os.environ[key] = path
            logger.debug('set {}:{}'.format(key, path))

    @property
    def asset(self):
        return Asset(self)

    @property
    def scene(self):
        return Scene(self)

    @property
    def version(self):
        """ version format infomation """
        return Version(self)

    @property
    def template(self): 
        return Template(self)

    @property
    def team(self):
        return Team(self)

    @property
    def software(self):
        return Software(self)
    

    def clear(self):
        caches = dict()

    def get_project_env(self, dcc, dccVersion, department): 
        # get environments for the project and use them in mayapy
        project = self.project
        if dcc == 'maya': 
            from rf_maya.environment import envparser
            
            if dccVersion in envparser.get_available_maya_versions(project): 
                logger.info('Project: {}'.format(project))
                logger.info('Version: {}'.format(dccVersion))
                envs = envparser.setupEnv(project=project, department=department, version=dccVersion,
                            setEnvironment=False, writeToFile=False)

                # merge environments with current environments
                envStr = os.environ.copy()
                for k, v in envs.items():
                    # if k in envStr:
                    #     v.append(envStr[k])
                    if k in envStr and v: 
                        envStr[k] = ';'.join(v) + ';' + envStr[k]

                return envStr
            else: 
                logger.warning('"{}" version not in env config'.format(dccVersion))

        logger.warning('"{}" not supported'.format(dcc))




class Version(object):
    """docstring for Version"""
    def __init__(self, projectInfo):
        super(Version, self).__init__()
        self.projectInfo = projectInfo
        self.data = self.projectInfo.workfile().get('versionFormat')

    @property
    def prefix(self):
        return self.data.get('prefix')

    @property
    def padding(self):
        return self.data.get('padding')

class Render(object):
    """docstring for Render"""
    def __init__(self, projectInfo):
        super(Render, self).__init__()
        self.projectInfo = projectInfo

    def fps(self):
        return self.projectInfo.render_data().get('fps')

    def use_ocio(self):
        return self.projectInfo.render_data().get('use_ocio')
        
    def fps_unit(self):
        return self.projectInfo.render_data().get('currentFPS')

    def linear(self):
        return self.projectInfo.render_data().get('linear')

    def angular(self):
        return self.projectInfo.render_data().get('angular')

    def yeti_sample(self):
        return self.projectInfo.render_data().get('yetiSample')

    def black_frame(self):
        return self.projectInfo.render_data().get('blackFrame')

    def device_aspect(self):
        return self.projectInfo.render_data().get('deviceAspect')

    def pixel_aspect(self):
        return self.projectInfo.render_data().get('pixelAspect')

    def raw_output(self):
        return self.projectInfo.render_data().get('rawOutput')

    def final_output(self):
        return self.projectInfo.render_data().get('finalOutput')

    def raw_outputH(self):
        return self.projectInfo.render_data().get('rawOutputH')

    def final_outputH(self):
        return self.projectInfo.render_data().get('finalOutputH')

    def start_frame(self):
        return self.projectInfo.render_data().get('startFrame', 1001)

    def overscan(self): 
        return self.projectInfo.render_data().get('overscan')

    def film_fit(self): 
        return self.projectInfo.render_data().get('filmFit')

    def watermark(self, key='default'): 
        pattern = self.projectInfo.render_data().get('watermark', dict()).get(key, '')
        return os.path.expandvars(pattern)

    def set_size(self, app='maya', mode='final'):
        deviceAspect = self.device_aspect()
        pixelAspect = self.pixel_aspect()
        if mode == 'final':
            w, h = self.raw_output()
        if mode == 'preview':
            w, h = self.raw_outputH()
        if app == 'maya':
            import maya.cmds as mc
            mc.setAttr('defaultResolution.pixelAspect', pixelAspect)
            mc.setAttr('defaultResolution.deviceAspectRatio', deviceAspect)
            mc.setAttr('defaultResolution.w', w)
            mc.setAttr('defaultResolution.h', h)


class Task(object):
    """docstring for Task"""
    def __init__(self, projectInfo):
        super(Task, self).__init__()
        self.projectInfo = projectInfo

    def dependency(self, entity):
        """ get task dependency """
        return self.projectInfo.task_data().get(entity)['directTrigger']

    def asset_dependency(self):
        """ get task dependency """
        return self.projectInfo.task_data().get('asset')['directTrigger']

    def scene_dependency(self):
        """ get task dependency """
        return self.projectInfo.task_data().get('scene')['directTrigger']

    def look_dependency(self):
        """ get look dependency """
        return self.projectInfo.task_data().get('asset')['look']



class Schema(object):
    """docstring for Schema"""
    def __init__(self, projectInfo):
        super(Schema, self).__init__()
        self.projectInfo = projectInfo

    def context_key(self):
        return self.projectInfo.schema_data().get('contextKey')

    def work(self, entity):
        return self.get_schema_value(entity, 'workPath')

    def publish(self, entity):
        return self.get_schema_value(entity, 'publishPath')

    def hero(self, entity):
        return self.get_schema_value(entity, 'heroPath')

    def cache(self, entity):
        return self.get_schema_value(entity, 'cachePath')

    def outputImg(self, entity):
        return self.get_schema_value(entity, 'outputImgPath')

    def texture(self, entity):
        return self.get_schema_value(entity, 'texturePath')

    def get_schema_value(self, entity, key):
        schema = self.projectInfo.schema_config(entity).get(key, '')
        return SchemaUtils(self.projectInfo, schema)

    @property
    def asset(self):
        return self.key().get('asset').get('key')

    @property
    def scene(self):
        return self.key().get('scene').get('key')


class WorkfileConfig(object):
    """docstring for WorkfileConfig"""
    def __init__(self, projectInfo):
        super(WorkfileConfig, self).__init__()
        self.projectInfo = projectInfo

    def key(self):
        return self.projectInfo.workfile().get('contextKey')

    def workfile(self, entity):
        return self.projectInfo.workfile().get('format').get(entity).get('workfile')

    def workfileUser(self, entity):
        return self.projectInfo.workfile().get('format').get(entity).get('workfileUser')

    def publish(self, entity):
        return self.projectInfo.workfile().get('format').get(entity).get('publish')

    def hero(self, entity):
        return self.projectInfo.workfile().get('format').get(entity).get('hero')


class Step(object):
    """docstring for Step"""
    def __init__(self, projectInfo):
        super(Step, self).__init__()
        self.projectInfo = projectInfo

    def list_asset(self):
        """ list asset steps """
        # return self.projectInfo.steps().get(self.projectInfo.schema.asset)
        return self.projectInfo.steps().get('asset')

    def list_scene(self):
        """ list scene steps """
        # return self.projectInfo.steps().get(self.projectInfo.schema.scene)
        return self.projectInfo.steps().get('scene')

    def sg_step(self, entityType, step): 
        return self.projectInfo.steps().get(entityType).get(step).get('sgname')


class Team(object):
    """docstring for Team"""
    def __init__(self, projectInfo):
        super(Team, self).__init__()
        self.projectInfo = projectInfo

    def supervisor(self, entityType, step):
        """ get supervisor list """ 
        return self.projectInfo.team_data().get(entityType).get(step).get('supervisor')

    def producer(self, entityType, step):
        """ get producer list """
        return self.projectInfo.team_data().get(entityType, dict()).get(step, dict()).get('producer')


class Software(object):
    """docstring for Software"""
    def __init__(self, projectInfo):
        super(Software, self).__init__()
        self.projectInfo = projectInfo

    def version(self, name): 
        return self.projectInfo.dcc().get(name)

    def executable(self, name, key='path'):
        """ name = 'Maya', 'MayaPy' """ 
        version = self.version(name)
        key = '{}{}'.format(name, version)
        path = config.Software.data.get(key).get('path')
        return path

    def maya(self): 
        name = 'Maya'
        version = self.version(name)
        key = '{}{}'.format(name, version)
        return config.Software.data.get(key).get('path')

    def mayapy(self): 
        name = 'Maya'
        version = self.version(name)
        key = '{}{}'.format(name, version)
        return config.Software.data.get(key).get('mayapy')


class SchemaUtils(object):
    """docstring for SchemaUtils"""
    def __init__(self, projectInfo, schema):
        super(SchemaUtils, self).__init__()
        self.projectInfo = projectInfo
        self.schema = schema

    def __repr__(self):
        return self.schema

    def __str__(self):
        return self.schema

    def convert_parentkey(self):
        """ convert current schema using parent key """
        schema = self.schema
        schemaKeys = extract_keys(schema)
        contextKeys = self.projectInfo.schema.context_key()
        parentKeys = []

        for key in schemaKeys:
            data = contextKeys.get(key, dict())
            parent = data.get('parent')
            value = parent if parent else key
            schema = schema.replace('<{}>'.format(key), '<{}>'.format(value))

        return schema


class Asset(object):
    """docstring for Asset"""
    def __init__(self, projectInfo):
        super(Asset, self).__init__()
        self.projectInfo = projectInfo
        self.assetConfig = self.projectInfo.config_data().get('asset', dict())

    def top_grp(self):
        return self.assetConfig.get('topGrp')

    def root_ctrl(self):
        return self.assetConfig.get('placement')

    def geo_grp(self):
        return self.assetConfig.get('geoGrp')

    def md_geo_grp(self):
        return self.assetConfig.get('mdGeoGrp')

    def lo_geo_grp(self):
        return self.assetConfig.get('loGeoGrp')

    def pr_geo_grp(self):
        return self.assetConfig.get('prGeoGrp')

    def pv_geo_grp(self):
        return self.assetConfig.get('pvGeoGrp')

    def tech_grp(self):
        return self.assetConfig.get('techGrp')

    def groom_geo_grp(self):
        return self.assetConfig.get('groomGeoGrp')

    def groom_mesh_grp(self):
        return self.assetConfig.get('groomMeshGrp')

    def groom_curve_grp(self):
        return self.assetConfig.get('groomCurveGrp')

    def smooth_set(self):
        return self.assetConfig.get('smoothSet')

    def rs_grp(self):
        return self.assetConfig.get('rsGrp')

    def set_grp(self):
        return self.assetConfig.get('set')

    def shotdress_grp(self):
        return self.assetConfig.get('shotdressGrp')

    def instance_grp(self):
        return self.assetConfig.get('instanceGrp')

    def set_instance_grp(self): 
        return self.assetConfig.get('setInstanceGrp')

    def cam_grp(self):
        return self.get('camGrp')

    def char_grp(self):
        return self.get('charGrp')

    def prop_grp(self):
        return self.get('propGrp')

    def fx_grp(self):
        return self.get('fxGrp')

    def groom_grp(self):
        return self.get('groomGrp')

    def groom_geo_grp(self):
        return self.get('groomGeoGrp')

    def path_dummy(self):
        return self.get('pathDummy')

    def groom_node_grp(self):
        return self.get('groomNodeGrp')

    def mattepaint_grp(self):
        return self.get('mattepaintGrp')

    def temp_grp(self):
        return self.get('tempGrp')

    def guide_grp(self):
        return self.get('guideGrp')

    def crowd_grp(self):
        return self.get('crowdGrp')

    def crowd_rs_grp(self):
        return self.get('crowdRsGrp')

    def previs_grp(self): 
        return self.get('previsGrp')

    def allmover_grp(self): 
        return self.get('allMover')

    def subasset_grp(self):
        return self.get('subAssetGrp')

    def export_grp(self, res='md'):
        if res == 'md': 
            return self.get('exportGrp')
        if res == 'lo': 
            return self.get('exportLoGrp')
        return self.get('exportGrp')

    def extracam_grp(self): 
        return self.get('extraCam_Grp')

    def global_asset(self, key=''):
        return self.get('globalAsset') if not key else self.get('globalAsset').get(key)

    def name_suffix(self, key=''): 
        return self.get('nameSuffix') if not key else self.get('nameSuffix').get(key)

    def info(self):
        return self.get('assetInfo')

    def ad_attr(self):
        return self.info().get('ad')

    def hero_attr(self):
        return self.info().get('hero')

    def tag_obj(self):
        return self.info().get('obj')

    def get(self, key):
        return self.assetConfig.get(key, dict())

    def list_res(self):
        return self.assetConfig.get('res', dict())

    def ue_cam(self): 
        return self.assetConfig.get('ueCam')

    def share_projects(self): 
        return self.assetConfig.get('shareProjects')

    @property
    def io(self):
        return IO(self.projectInfo, 'asset')

    @property
    def proxy(self):
        return self.assetConfig.get('res').get('proxy')

    @property
    def previs(self):
        return self.assetConfig.get('res').get('previs')

    @property
    def lo(self):
        return self.assetConfig.get('res').get('lo')

    @property
    def medium(self):
        return self.assetConfig.get('res').get('medium')

    @property
    def hi(self):
        return self.assetConfig.get('res').get('hi')

    def texture_setting(self): 
        return self.assetConfig.get('textureSetting') or dict()

    def preview_texture_size(self): 
        return self.texture_setting().get('previewTextureSize', 128) or 128


class IO(object):
    """docstring for IO"""
    def __init__(self, projectInfo, entity):
        super(IO, self).__init__()
        self.projectInfo = projectInfo
        self.entity = entity
        self.ioConfig = self.projectInfo.config_data().get('io', dict()).get(entity, dict())


    def step(self, step, task='any'):
        data = self.ioConfig.get(step, dict())
        return data.get(task) if task in data.keys() else data.get('any')
        # return self.ioConfig.get(step, dict()).get(task)


class Scene(object):
    """docstring for Scene"""
    def __init__(self, projectInfo):
        super(Scene, self).__init__()
        self.projectInfo = projectInfo
        self.sceneConfig = self.projectInfo.config_data().get('scene', dict())

    @property
    def start_frame(self):
        return self.sceneConfig.get('startFrame')

    @property
    def duration_shot(self):
        return self.sceneConfig.get('durationShot')

    @property
    def pre_roll(self):
        return self.sceneConfig.get('preRoll')

    @property
    def post_roll(self):
        return self.sceneConfig.get('postRoll')

    @property
    def sequence_key(self): 
        # if not written in the config, use all as default 
        default = 'all'
        return self.sceneConfig.get('sequenceKey', default)

    @property
    def shift_to_start(self): 
        """ is the project shift key """
        # if not written in the config, use True as default 
        default = True
        return self.sceneConfig.get('shiftToStart', default)

    @property
    def include_prepost_sequencer(self): 
        """ is the project shift key """
        # if not written in the config, use True as default 
        default = False
        return self.sceneConfig.get('includePrePostRollSequencer', default)

    @property
    def cache_at_origin(self): 
        """ is the project shift key """
        # if not written in the config, use True as default 
        default = False
        return self.sceneConfig.get('cacheAtOrigin', default)

    @property
    def prefer_built_step(self): 
        return self.sceneConfig.get('preferBuiltStep')

    @property 
    def tech_cache(self): 
        return self.sceneConfig.get('techCache')

    @property 
    def dependency_step(self): 
        default = ['layout', 'setdress']
        return self.sceneConfig.get('dependencyStep', default)

    @property
    def shade_build_step(self):
        return self.sceneConfig.get('shadeBuildStep')


class Template(object):
    """docstring for Template"""
    def __init__(self, projectInfo):
        super(Template, self).__init__()
        self.projectInfo = projectInfo
        self.templatePath = self._get_path()

    def _get_path(self): 
        templatePath = '{}/{}'.format(Path.template, self.projectInfo.project)
        if not os.path.exists(templatePath): 
            templatePath = '{}/{}'.format(Path.template, Path.defaultProject)
        return templatePath

    def scene(self, step=''): 
        path = '{}/{}/scene'.format(os.environ[RFSCRIPT_VAR], self.templatePath)
        if step: 
            path = '{}/{}'.format(path, step)
        return path


def extract_keys(config):
    """ get key between <> """
    startKey = '<'
    endKey = '>'
    values = [a.split('>')[0] for a in config.split(startKey) if a]
    return values



