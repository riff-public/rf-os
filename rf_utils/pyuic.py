import sys, pprint
from pyside2uic import compileUi

def convert(inFile): 
	outFile = inFile.replace(".ui", ".py")
	pyFile = open(outFile, "w")
	compileUi(inFile, pyFile, False, 4, False)
	pyFile.close()
	return outFile
	