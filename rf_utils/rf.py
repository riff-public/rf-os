import os 
import sys 
from ftplib import FTP 

class Server: 
    host = '172.16.120.199'
    user = 'testuser'
    password = '123456789'


def upload(src, dst): 
    """ upload from disk src to ftp dst """ 
    # src = D:/test/file.yml 
    # dst = /dst/file.yml

    if os.path.exists(src): 
        basename = os.path.basename(dst)
        targetDir = os.path.dirname(dst)

        server = Connect()
        with server: 
            with open(src, 'rb') as file: 
                makedirs(server.ftp, targetDir)
                server.ftp.cwd(targetDir)
                server.ftp.storbinary('STOR %s' % basename, file)

            result = path_exists(server.ftp, dst)
        return result


def download(src, dst): 
    """ download from ftp src to disk dst """ 
    # src = /dst/file.yml
    # dst = D:/test/file.yml 

    server = Connect()
    with server: 
        if path_exists(server.ftp, src): 
            root, filename = os.path.split(src)
            server.ftp.cwd(root)

            if not os.path.exists(os.path.dirname(dst)): 
                os.makedirs(os.path.dirname(dst))
            
            with open(dst, 'wb') as file: 
                server.ftp.retrbinary('RETR %s' % filename, file.write)

    result = os.path.exists(dst)
    return result 


def makedirs(ftp, path): 
    if not path_exists(ftp, path): 
        ftp.mkd(path)
        return path


def path_exists(ftp, path): 
    """ is path exists """
    dirList = dir_walk(path)

    for dirpath in dirList: 
        root, dirname = os.path.split(dirpath)
        ftp.cwd(root)
        dirs = ftp.nlst()

        if not dirname in dirs: 
            return False 
        # session.cwd()
    return True 


def isdir(ftp, path): 
    """ is path a dir """ 
    filelist = []
    root, dirname = os.path.split(path)
    ftp.cwd(root)
    dirs = ftp.nlst()
    ftp.retrlines('LIST', filelist.append)

    for file in filelist: 
        # 8th elements is file or folder name 
        filename = ' '.join(file.split()[8:])
        # 0 index 
        isdir = True if file.split()[0][0] == 'd' else False 

        if dirname == filename: 
            return isdir


def isfile(ftp, path): 
    """ is path a file """ 
    return not isdir(ftp, path)

    # ['drwxr-xr-x', '1', 'ftp', 'ftp', '0', 'Jan', '12', '22:25', 'dir', 'dir']
    # ['-rw-r--r--', '1', 'ftp', 'ftp', '0', 'Jan', '12', '22:25', 'file', 'file']
    # ['drwxr-xr-x', '1', 'ftp', 'ftp', '0', 'Jan', '12', '22:06', 'test']
    # ['-rw-r--r--', '1', 'ftp', 'ftp', '84', 'Jan', '10', '04:18', 'test2.yml']


def dir_walk(path): 
    # path = '/root/dir1/dir2/dir3/dir4'
    result = []
    splits = [p for p in path.split('/') if p]  # get rid of '' cause by // or /root
    num_splits = len(splits)

    for i in range(1, num_splits + 1):  # add offset of 1 to the range so we do not get [:0]
        r = '/' + '/'.join(splits[:i])
        result.append(r)
    return result


class Connect(object):
    """ Connect ftp server """
    def __init__(self):
        super(Connect, self).__init__()
        self.ftp = None

    def __enter__(self): 
        self.ftp = FTP(Server.host)
        self.ftp.login(user=Server.user, passwd=Server.password)

    def __exit__(self, *args): 
        self.ftp.quit()
        