# publish info
import os
import sys
from collections import OrderedDict
from rf_utils import file_utils
from rf_utils import project_info
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class Data:
    name = 'output'
    ext = '.yml'

    # key
    publish = 'publish'
    versionId = 'versionId'
    regVersion = 'regVersion'
    task = 'task'
    app = 'app'
    snap = 'snap'
    publishVersion = 'publishVersion'
    publishOutput = 'publishOutput'
    heroOutput = 'heroOutput'
    workspace = 'workspace'
    outputList = 'outputList'
    checkOutputList = 'checkOutputList'
    workfile = 'workfile'
    outputImg = 'outputImg'


class PreRegister(object):
    """PreRegister work version to a publish version"""
    def __init__(self, ContextPathInfo):
        super(PreRegister, self).__init__()
        if self.valid_object(ContextPathInfo):
            self.entity = ContextPathInfo
            self.res = self.entity.res

            # key data
            self.keys = [Data.publish, Data.versionId, Data.regVersion, Data.task, Data.app, Data.snap, Data.workfile, Data.publishVersion, Data.publishOutput, Data.heroOutput, Data.outputImg, Data.workspace, Data.outputList]
            self.updateKeys = [Data.publish, Data.versionId, Data.task]

            # version
            self.prefix = self.entity.projectInfo.version.prefix
            self.padding = self.entity.projectInfo.version.padding

            # pre register content
            self.content = self.pre_register()


    def valid_object(self, ContextPathInfo):
        if type(ContextPathInfo).__name__ == 'ContextPathInfo':
            return True
        else:
            logger.error('Expected object type "ContextPathInfo", Received %s' % type(ContextPathInfo).__name__)


    @property
    def data_file(self):
        dataPath = self.entity.path.process_data().abs_path()
        dataNameRes = '%s_%s%s' % (Data.name, self.entity.res, Data.ext)
        dataName = '%s%s' % (Data.name, Data.ext)
        dataName = dataNameRes if self.entity.res else dataName
        dataFile = '%s/%s' % (dataPath, dataName)
        return dataFile

    def read_data(self):
        dataFile = self.data_file

        if not os.path.exists(dataFile):
            logger.debug('Publish info not registered')
            return OrderedDict()

        else:
            self.data = file_utils.ymlLoader(dataFile) or OrderedDict()
        return self.data

    def write_data(self, data):
        dataFile = self.data_file
        if not os.path.exists(os.path.dirname(dataFile)):
            os.makedirs(os.path.dirname(dataFile))

        result = file_utils.ymlDumper(dataFile, data)
        logger.debug('write data %s' % dataFile)


    def pre_register(self):
        """ pre-register work version to output version """
        readData = OrderedDict()
        if self.entity.res:
            readData = self.read_data()
            regWorkVersion = self.entity.version # workfile version as key
            filename = self.entity.filename
            snapPath = '%s/%s' % (self.entity.path.snap(), filename)

            if not regWorkVersion in readData.keys():
                regPublishVersion = self.publish_version() # publish reg version
                self.entity.context.update(publishVersion=regPublishVersion)
                logger.debug('published version "%s" updated to context entity' % regPublishVersion)
                publishVersion = str(self.entity.path.publish_version())
                outputList = []
                workspaceFile = []

                self.regContent = self.format_data(
                                publish=False,
                                versionId=0,
                                regVersion=regPublishVersion,
                                task=self.entity.task,
                                app=self.entity.app,
                                snap=snapPath,
                                publishVersion=publishVersion,
                                publishOutput=str(self.entity.path.publish_output()),
                                heroOutput=str(self.entity.path.output_hero()),
                                workfile=None,
                                workspace=workspaceFile,
                                outputList=outputList,
                                checkOutputList=[],
                                outputImg=str(self.entity.path.outputImg())
                                )
                readData[regWorkVersion] = self.regContent

            else:
                # set publish information to entity from existing data
                regPublishVersion = readData[regWorkVersion][Data.regVersion]
                self.entity.context.update(publishVersion=regPublishVersion)
                logger.info('%s is already registered. Return registered data' % regWorkVersion)

        else:
            logger.warning('Missing res data "%s", Cannot read data' % self.entity.res)

        return readData

    def register(self):
        return self.write_data(self.content)

    def add_output(self, path):
        """ add output list to content """
        regData = self.content.get(self.entity.version)
        if not path in regData[Data.outputList]:
            regData[Data.outputList].append(path)

    def add_check_output(self, path):
        regData = self.content.get(self.entity.version)
        if not path in regData[Data.checkOutputList]:
            regData[Data.checkOutputList].append(path)


    def add_workspace(self, path):
        """ add output list to content """
        regData = self.content.get(self.entity.version)
        regData[Data.workspace].append(path)

    def add_workfile(self, path):
        regData = self.content.get(self.entity.version)
        regData[Data.workfile] = path

    def info(self, version=None):
        """ show information """
        data = self.read_data()
        if not version:
            version = self.entity.version
        return data.get(version)

    def list_key_version(self):
        data = self.read_data()
        return sorted(data.keys())

    def list_publish_version(self):
        data = self.read_data()
        return [v.get(Data.regVersion) for k, v in data.iteritems()]

    def publish_version(self):
        """ find next version """
        versions = self.list_publish_version()
        intVersion = [int(a.split(self.prefix)[-1]) for a in versions if a.split(self.prefix)[-1].isdigit()]
        maxVersion = max(intVersion) if intVersion else 0
        nextVer = maxVersion + 1
        return combine_version(self.prefix, self.padding, nextVer)

    # def publish_hero_file(self, customName=None):
    #     """ return hero dst """
    #     heroDir = self.entity.path.output_hero()
    #     heroName = self.entity.publish_name(hero=True)

    #     if customName:
    #         process = self.entity.process
    #         self.entity.context.update(process=customName)
    #         heroName = self.entity.publish_name(hero=True)
    #         self.entity.context.update(process=process)
    #         print self.entity.process
    #     return '%s/%s' % (heroDir, heroName)

    def publish_file(self, outputKey=None, hero=False):
        """ return publish file """
        regData = self.content.get(self.entity.version)
        publishDir = self.entity.path.publish_output() if not hero else self.entity.path.output_hero()
        publishName = self.entity.output_name(outputKey, hero=hero) if outputKey else self.entity.publish_name(hero=hero)
        return '%s/%s' % (publishDir, publishName)

    def publish_workspace(self):
        regData = self.content.get(self.entity.version)
        publishDir = self.entity.path.publish_workspace()
        publishName = self.entity.publish_name()

        # publishDir = self.content.get(self.entity.version).get(Data.publishVersion)
        return '%s/%s' % (publishDir, publishName)

    def publish_hero_workspace(self):
        regData = self.content.get(self.entity.version)
        publishDir = self.entity.path.scheme('publishHeroWorkspacePath')
        publishName = self.entity.publish_name(hero=True)

        # publishDir = self.content.get(self.entity.version).get(Data.publishVersion)
        return '%s/%s' % (publishDir, publishName)


    def hero_file(self, outputKey=None):
        """ return assetHero file """
        heroDir = self.entity.path.scheme('heroPath')
        heroName = self.entity.output_name(outputKey, hero=True) if outputKey else self.entity.publish_name(hero=True)
        return '%s/%s' % (heroDir, heroName)


    def format_data(self, **kwargs):
        data = OrderedDict()
        keys = self.keys

        for key in keys:
            value = kwargs.get(key)
            data[key] = value

        return data

class RegisterInfo(object):
    """docstring for RegisterInfo"""
    def __init__(self, ContextPathInfo):
        super(RegisterInfo, self).__init__()
        self.entity = ContextPathInfo

    def list_data_files(self):
        dataPath = self.entity.path.process_data().abs_path()
        dataFiles = ['%s/%s' % (dataPath, a) for a in file_utils.listFile(dataPath)]
        return dataFiles or []

    def list_res_files(self):
        res = [os.path.splitext(os.path.basename(a))[0].split('%s_' % Data.name)[-1] for a in self.list_data_files()]
        return res

    def data_file(self):
        res = self.entity.res
        dataFile = [a for a in self.list_data_files() if res in os.path.basename(a)]
        dataFile = dataFile[0] if dataFile else ''
        return dataFile

    @property
    def content(self):
        data = OrderedDict()
        if os.path.exists(self.data_file()):
            data = file_utils.ymlLoader(self.data_file())
        return data

    def register_version(self): 
        """ get publish version that match current working version """ 
        content = self.content.get(self.entity.version)
        if content: 
            regVersion = content.get(Data.regVersion)
            return regVersion
        else: 
            logger.warning('%s not registered yet' % self.entity.version)
            return False

    # def list_version(self):
    #     return self.content.keys()

    def check_publish_version(self, publish=True):
        return [k for k, v in self.content.iteritems() if v.get(Data.publish) == publish]

    def unpublish_version_content(self, latest=True):
        versions = sorted(self.check_publish_version(publish=False))
        versions = [versions[-1]] if latest else versions
        datas = []
        for version in versions:
            datas.append(self.content.get(version))

        return datas


class Register(PreRegister):
    """docstring for Register"""
    def __init__(self, ContextPathInfo):
        super(Register, self).__init__(ContextPathInfo)
        self.entity = ContextPathInfo
        self.register()

    def update(self, **kwargs):
        """ update only publish and publishId """
        readData = self.read_data()
        regWorkVersion = self.entity.version
        regContent = self.format_data(**kwargs)

        for k, v in kwargs.iteritems():
            if k in self.updateKeys:
                readData.get(regWorkVersion).update({k: v})

        self.write_data(readData)
        return readData.get(regWorkVersion)


    def remove(self):
        """ remove this version from data """
        readData = self.read_data()
        regWorkVersion = self.entity.version
        if regWorkVersion in readData.keys():
            readData.pop(regWorkVersion)

            result = self.write_data(readData)
            logger.debug('%s removed' % regWorkVersion)
            return result


    def register(self):
        """ write data to yml file """
        self.write_data(self.content)


class RegisterAsset(object):
    """docstring for RegisterAsset"""
    def __init__(self, ContextPathInfo):
        super(RegisterAsset, self).__init__()
        self.entity = ContextPathInfo
        self.regInfo = RegisterInfo(self.entity)
        self.steps = []
        self.projectInfo = project_info.ProjectInfo(project=self.entity.project)
        self.steps = self.list_steps()

    def list_steps(self):
        steps = self.projectInfo.step.list_asset()
        return [steps[a]['key'] for a in steps]


    def pre_register_content(self):
        data = self.regInfo.content.get(self.entity.version)
        return data

    def data_file(self):
        """ data file """
        filename = '%s_data.hero.yml' % self.entity.name
        dataDir = self.entity.path.data().abs_path()
        return ('/').join([dataDir, filename])

    def data_files(self):
        """ list all data files """
        dataDir = self.entity.path.data().abs_path()
        files = file_utils.listFile(dataDir)
        dataFiles = [('/').join([dataDir, a]) for a in files]
        return dataFiles

    def version_data_file(self):
        dataDir = self.entity.path.data().abs_path()
        files = file_utils.listFile(dataDir)
        files = [a for a in files if '%s_data' % self.entity.name in a]

        versions = sorted([get_version('v', 3, a) for a in files]) if files else []
        versionStr = 'v001'

        if versions:
            latestVersion = versions[-1]
            nextVersion = int(latestVersion[1:])+1 if latestVersion[1:].isdigit() else 1
            versionStr = 'v%03d' % nextVersion

        filename = '%s_data.%s.yml' % (self.entity.name, versionStr)
        return ('/').join([dataDir, filename])

    def add_data(self, currentData):
        preRegData = self.pre_register_content()
        contentDict = OrderedDict()

        if preRegData:
            contentDict[Data.publishVersion] = preRegData[Data.publishVersion]
            contentDict[Data.workspace] = preRegData[Data.workspace]
            contentDict[Data.publishOutput] = preRegData[Data.publishOutput]
            contentDict[Data.heroOutput] = preRegData[Data.heroOutput]
            contentDict[Data.outputImg] = str(self.entity.path.outputImg())
            contentDict[Data.task] = self.entity.task
            processData = OrderedDict()

            if not self.entity.step in currentData.keys():
                processData[self.entity.process] = contentDict

            else:
                processData = currentData[self.entity.step]
                processData[self.entity.process] = contentDict

            currentData[self.entity.step] = processData
            return currentData
        else:
            logger.warning('No preregister data')


    def register(self):
        currentData = self.read_data()
        newData = self.add_data(currentData)

        if newData:
            self.publish_data(newData)
            return newData

    def format_step(self, **kwargs):
        data = OrderedDict()

        for step in self.steps:
            value = kwargs.get(step)
            data[step] = value

        return data

    def publish_data(self,data):
        """ publish version only if data is new """
        heroFile = self.write_data(data)
        versionFile = self.version_data_file()
        newData = self.is_new_data(heroFile)

        if newData:
            file_utils.copy(heroFile, versionFile)

        return True

    def write_data(self, data):
        """ write asset data """
        dataFile = self.data_file()
        if not os.path.exists(os.path.dirname(dataFile)):
            os.makedirs(os.path.dirname(dataFile))
        logger.debug('write %s' % dataFile)
        result = file_utils.ymlDumper(dataFile, data)
        return dataFile

    def read_data(self):
        """ read data """
        dataFile = self.data_file()
        return file_utils.ymlLoader(dataFile) if os.path.exists(dataFile) else OrderedDict()

    def read_all_data(self):
        """ all data files """
        dataFiles = self.data_files()
        datas = []
        for dataFile in dataFiles:
            data = file_utils.ymlLoader(dataFile) if os.path.exists(dataFile) else OrderedDict()
            datas.append(data)
        return datas


    def is_new_data(self, heroFile):
        """ check if new data """
        files = file_utils.listFile(os.path.dirname(heroFile))
        files.remove(os.path.basename(heroFile))
        files = [a for a in files if '%s_data' % self.entity.name in a]

        if files:
            latestVersion = '%s/%s' % (os.path.dirname(heroFile), files[-1])
            data1 = file_utils.ymlLoader(heroFile)
            data2 = file_utils.ymlLoader(latestVersion)

            return True if not data1 == data2 else False
        return True

class ShotOutputKey:
    workspace = 'workspace'
    regVersion = 'regVersion'

class PreRegisterShot(object):
    """docstring for PreRegisterShot"""
    def __init__(self, ContextPathInfo):
        super(PreRegisterShot, self).__init__()
        self.entity = ContextPathInfo
        self.data = self.read_data()
        self.preferOrder = [ShotOutputKey.workspace]
        self.outputs = [ShotOutputKey.workspace]
        self.content = self.set_data()

    @property
    def data_file(self):
        dataPath = self.entity.path.process_data().abs_path()
        dataName = '%s_shot%s' % (Data.name, Data.ext)
        dataFile = '%s/%s' % (dataPath, dataName)
        return dataFile


    def read_data(self):
        return file_utils.ymlLoader(self.data_file) if os.path.exists(self.data_file) else OrderedDict()

    def add_output(self, output):
        self.outputs.append(output) if not output in self.outputs else None
        self.outputs = sorted_list(self.outputs, self.preferOrder)
        self.set_data()
        outputVersion = self.data.get(self.entity.version).get(output, '')
        self.add_to_snap(output, outputVersion)
        return outputVersion

    def current_version(self, output): 
        outputVersion = self.data.get(self.entity.version).get(output, '')
        return outputVersion

    def add_to_snap(self, output, version): 
        pass 

    def set_data(self):
        """ preview new data before write to file """
        localVersion = self.entity.version
        tmp = OrderedDict() if not localVersion in self.data.keys() else self.data[localVersion]

        for output in self.outputs:
            if not output in tmp.keys():
                tmp[output] = self.find_reg_version(output)

        self.data[localVersion] = tmp
        return self.data

    def register(self):
        dataFile = self.data_file
        os.makedirs(os.path.dirname(dataFile)) if not os.path.exists(os.path.dirname(dataFile)) else None
        file_utils.ymlDumper(dataFile, self.data)
        return dataFile

    def version_dict_info(self):
        info = OrderedDict()
        for version, dictValue in self.data.iteritems():
            for output, regVersion in dictValue.iteritems():
                if not output in info.keys():
                    info[output] = [regVersion]
                else:
                    current = info[output]
                    current.append(regVersion) if not regVersion in current else None
                    info[output] = sorted(current)

        return info

    def find_reg_version(self, outputKey):
        versionDict = self.version_dict_info()
        if not outputKey in versionDict.keys():
            return 'v001'
        else:
            maxVersion = sorted(versionDict[outputKey])[-1]
            newVersion = increment_version(maxVersion)
            return newVersion

class PreRegisterShot2(PreRegisterShot):
    """docstring for PreRegisterShot2"""
    def __init__(self, ContextPathInfo):
        super(PreRegisterShot2, self).__init__(ContextPathInfo)
        self.entity = ContextPathInfo
        self.snapData = self.read_snap_file()

    @property
    def snap_file(self):
        dataPath = self.entity.path.process_data().abs_path()
        dataName = '%s_snap%s' % (Data.name, Data.ext)
        dataFile = '%s/%s' % (dataPath, dataName)
        return dataFile

    def read_snap_file(self): 
        data = file_utils.ymlLoader(self.snap_file) if os.path.exists(self.snap_file) else OrderedDict()
        return data 
            
    def snap(self): 
        self.snapData = self.keep_max_data(self.snapData)
        os.makedirs(os.path.dirname(self.snap_file)) if not os.path.exists(os.path.dirname(self.snap_file)) else None
        return file_utils.ymlDumper(self.snap_file, self.snapData)

    def add_to_snap(self, output, outputVersion): 
        outputDict = self.snapData[self.entity.version] if self.entity.version in self.snapData.keys() else OrderedDict()
        outputDict[output] = outputVersion
        self.snapData[self.entity.version] = outputDict
        return self.snapData

    def keep_max_data(self, data): 
        newDict = OrderedDict()
        outputs = []
        for version in data.keys()[::-1]: 
            values = data[version]
            for output, outputVersion in values.iteritems(): 
                if not output in outputs: 
                    outputs.append(output)

                    if version in newDict.keys(): 
                        tmp = newDict[version]
                    else: 
                        tmp = OrderedDict()
                    tmp[output] = outputVersion
                    newDict[version] = tmp

        return newDict

class RegisterShotInfo(PreRegisterShot):
    """docstring for RegisterShotInfo"""
    def __init__(self, ContextPathInfo):
        super(RegisterShotInfo, self).__init__(ContextPathInfo)
        self.entity = ContextPathInfo

    def get_version(self, key):
        info = self.version_dict_info()
        return info[key] if key in info.keys() else []

    def get_latest_version(self, key):
        versions = self.get_version(key)
        return versions[-1] if versions else ''


def increment_version(version):
    num = version.replace('v', '')
    num = int(num) if num.isdigit() else 0
    newVersion = 'v%03d' % (num + 1)
    return newVersion


def sorted_list(inputList, preferOrder):
    sortedList = []
    for order in preferOrder:
        if order in inputList:
            sortedList.append(order)

    for value in inputList:
        if not value in sortedList:
            sortedList.append(value)

    return sortedList





def combine_version(prefix, padding, version):
    """ combine version format """
    cmd = '"%0' + str(padding) + 'd" % version'
    newVersion = '%s%s' % (prefix, eval(cmd))
    return newVersion


def get_version(prefix, padding, filename):
    """ find version in filename """
    versionNumber = [a[0: padding] for a in filename.split(prefix) if a[0: padding].isdigit()]
    return '%s%s' % (prefix, versionNumber[0]) if versionNumber else ''


# usage
# pre-register to preview version data
"""
asset = context_info.ContextPathInfo()
asset.context.update(res='md')
publishInfo = publish_info.PreRegister(asset)
publishInfo.publish_version()
# Result: 'v004' # return next publish version
publishInfo.publish_file()
# Result: u'$RFPUBL/ProjectName/asset/publish/character/main/sunnyBase/model/main/v003/sunnyBase_mdl_main_md.v003.ma' #
publishInfo.publish_hero_file()
# Result: u'$RFPUBL/ProjectName/asset/publish/character/main/sunnyBase/hero/sunnyBase_mdl_main_md.hero.ma' #
publishInfo.hero_file()
# Result: u'$RFPUBL/ProjectName/asset/publish/character/main/sunnyBase/hero/sunnyBase_mdl_main_md.hero.ma' #


"""

# example data
# v003:
#   publish: False
#   versionId: 0
#   publishVersion: v001
#   app: maya
#   snap: $RFPROJECT/projectName/asset/publ/char/sunny/sunnyBase/rig/main/_snap/filename.ma
#   dst: $RFPUBL/projectName/asset/publ/char/sunny/sunnyBase/rig/main/v001

# v006:
#   publish: True
#   versionId: 100
#   publishVersion: v002
#   app: maya
#   snap: $RFPROJECT/projectName/asset/publ/char/sunny/sunnyBase/rig/main/_snap/filename.ma
#   dst: $RFPUBL/projectName/asset/publ/char/sunny/sunnyBase/rig/main/v002

# shot register version

# scene = context_info.ContextPathInfo()
# scene.context.update(entityCode='s0010')
# scene.context.update(entity=scene.sg_name)
# scene.context.update(version='v007')
# reg = publish_info.PreRegisterShot(scene)
# reg.add_output('s0010_cam')
# reg.add_output('charB')
# reg.add_output('charA')
# reg.register()

# result
# v007:
#   workspace: v007
#   s0010_cam: v001
#   charB: v001
#   charA: v001
