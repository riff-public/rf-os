import sys
import os
root = '{}/core'.format(os.environ.get('RFSCRIPT'))
if root not in sys.path:
    sys.path.append(root)

from datetime import datetime
import cv2
from rf_utils.pipeline import convert_lib

def ask_user_choices(question, choices):
    response = ''
    question_str = question + '\n  '
    question_str += '\n  '.join(['{}={}'.format((i+1), choices[i]) for i in range(len(choices))])
    while True:
        print(question_str)
        response = int(input('Your answer: '))
        if isinstance(response, int) and response <= len(choices):
            return choices[response-1]

def main(paths): 
    print('\n===== This will merge multiple clips into a single clip =====')
    print('\nINPUTS:')
    print('\n{}'.format('\n'.join(paths)))

    # get dimension
    fn, ext = os.path.splitext(paths[0])
    capture = cv2.VideoCapture(paths[0])
    read_flag, img = capture.read()
    imgHeight, imgWidth, imgChannels = img.shape
    size = (imgWidth, imgHeight)

    # get FPS
    fps = capture.get(cv2.CAP_PROP_FPS)      # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
    capture.release()

    print('Using Size=[{}x{}], FPS={}'.format(imgWidth, imgHeight, fps))

    answer = ask_user_choices(question='\nStart merging?', choices=['Yes', 'No'])
    if answer != 'Yes':
        return

    # get output path
    output_dir = os.path.expanduser("~/Desktop").replace('\\', '/')
    output_path = '{}/mergeclip_{}{}'.format(output_dir, datetime.strftime(datetime.today(), '%y%m%d'), ext)

    # merge
    result = convert_lib.merge_mov(paths, output_path, fps=fps, size=size)

    print('Output: {}'.format(result))
    print('Finished.')

if __name__ == '__main__':
    paths = sys.argv[1:]
    # if it's a directory
    if len(paths) == 1 and os.path.isdir(paths[0]):
        paths = ['{}/{}'.format(paths[0], f) for f in os.listdir(paths[0]) if os.path.isfile('{}/{}'.format(paths[0], f))]
    if len(paths) > 1:
        main(paths=[i.replace('\\', '/') for i in paths])