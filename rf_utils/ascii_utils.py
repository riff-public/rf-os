import os
import collections
import re
from collections import defaultdict
from ast import literal_eval

def flag_string_value(line, flag):
    found = re.search('(%s) ("\S*")' %flag, line)
    result = ''
    if found:
        nameStr = found.group(2)
        result = nameStr[1:-1]
    return result

class MayaAscii(object):
    def __init__(self, path):
        self.path = path
        self.data = None

        self._joiner = '**_JOINER_**'

    def read(self):
        # read the lines
        data = collections.OrderedDict()
        with open(self.path, 'r') as f:
            ascii_lines = f.readlines()

            # collect line chunks
            # is_tabbed = False
            # collecting = False
            line_chunk = []
            curr_cmd = ''
            # prev_cmd = ''

            # get the title and end comment first
            data['title_comment'] = []
            title_chunks = []
            i = 0
            for i, line in enumerate(ascii_lines):
                if line.startswith('//'):
                    title_chunks.append(line)
                else:
                    break
            if title_chunks:
                data['title_comment'].append(title_chunks)

            end_chunks = []
            e = 0
            for e, line in enumerate(ascii_lines[::-1]):
                if line.startswith('//'):
                    end_chunks.append(line)
                else:
                    break
            if e > 0:
                lines_to_loop = ascii_lines[i:(e*-1)]
            else:
                lines_to_loop = ascii_lines[i:]

            for line in lines_to_loop:
                if line == '\n':
                    continue

                is_tabbed = line.startswith('\t')
                if line_chunk:
                    this_line_ends = line.endswith(';\n')
                    last_line_ends = line_chunk[-1].endswith(';\n')

                    # if the current line is tabbed
                    if is_tabbed:
                        if this_line_ends and last_line_ends:
                            line_chunk.append(line)
                        elif not this_line_ends and last_line_ends:
                            line_chunk.append(line)
                        else:
                            line_chunk[-1] += line 
                            
                    else:  # the current line isn't tabbed
                        data[curr_cmd].append(line_chunk)

                # it's the start of a chunk, ie requires, setAttr, ...
                if not is_tabbed:
                    try:
                        curr_cmd = line.split()[0]
                    except IndexError:
                        curr_cmd = line
                    if curr_cmd not in data:
                        data[curr_cmd] = []
                    line_chunk = [line]
                    
            data[curr_cmd].append(line_chunk)  # need to add the last line
            data['end_comment'] = []
            if end_chunks:
                data['end_comment'].append(end_chunks)

        self.data = data
        return data

    def write(self, path):
        result_lines = []
        for cmd, cmd_chunk in self.data.iteritems():
            for chunk in cmd_chunk:
                for line in chunk:
                    result_lines.append(line)

        with open(path, 'w') as f:
            f.writelines(result_lines)

    def frame_range(self):
        result = {'min':None, 'max':None, 'ast':None, 'aet':None}
        if not self.data or not self.data.get('createNode'):
            return result

        for chunk in self.data['createNode']:
            if 'sceneConfigurationScriptNode' in chunk[0]:
                for line in chunk:
                    if 'playbackOptions' in line:
                        for flag in ('min', 'max', 'ast', 'aet'):
                            found = re.search('-%s [0-9]*' %flag, line)
                            if found:
                                frame = found.group().split()[-1]
                                result[flag] = float(frame)

        return result

    def ls(self, search='', types=[], fullPath=True):
        result = []
        if not self.data or not self.data.get('createNode'):
            return result
        if not isinstance(types, (list, tuple)):
            types = [types]
        if search:
            search = re.compile('^%s$' %search)

        dagTypes = ('transform', 'joint')
        for i, chunk in enumerate(self.data['createNode']):
            cmd_line = chunk[0]
            line_splits = cmd_line.split()
            curr_type = line_splits[1]
            if types and curr_type not in types:
                continue

            name = flag_string_value(line=cmd_line, flag='-n')
            if search:
                match = search.match(name)
                if not match:
                    continue

            if fullPath:
                parent = flag_string_value(line=cmd_line, flag='-p')
                if parent:
                    name = '%s|%s' %(parent, name)
                else:
                    if curr_type in dagTypes:
                        name = '|%s' %name
            result.append((i, name))

        # return sorted(result, key=lambda x: x.split('|')[-1])
        return result

    def delete_node(self, search, types=[]):
        ls_results = self.ls(search=search, types=types)
        if not ls_results:
            return

        indices = [i[0] for i in ls_results]
        new_data = []
        for ci, chunk in enumerate(self.data['createNode']):
            if ci not in indices:
                new_data.append(chunk)
        self.data['createNode'] = new_data

    def createNode(self, nodeType, name, parent=None, shared=False):
        result = False
        if not self.data:
            return result

        new_lines = ['createNode %s -n "%s"' %(nodeType, name)]
        if shared:
            shared_str = '-s'
            new_lines.append(shared_str)
        if parent:
            parent_str = '-p "%s"' %(parent)
            new_lines.append(parent_str)
        new_line = ' '.join(new_lines)
        new_line += ';\n'
        if 'createNode' not in self.data:
            self.data['createNode'] = []
        self.data['createNode'].append([new_line])
        return True

    def addAttr(self, obj, shortName, attrType, longName='', niceName='', value=None, 
                dv=None, miv=None, mxv=None, lock=False, keyable=True):
        result = False
        if not self.data or not self.data.get('createNode'):
            return result

        simple_attr_types = ('short', 'long', 'float', 'double')
        new_chunk = []
        new_chunk_index = None
        line_index = 1
        for i, chunk in enumerate(self.data['createNode']):
            cmd_line = chunk[0]
            name = flag_string_value(line=cmd_line, flag='-n')
            if name == obj:
                for l, line in enumerate(chunk):
                    if line.startswith('\tsetAttr'):
                        line_index = l
                        break

                new_chunk = list(chunk)
                new_lines = ['\taddAttr -ci true -sn "%s"' %shortName]
                if not longName:
                    longName = shortName
                ln_str = '-ln "%s"' %longName
                new_lines.append(ln_str)

                if niceName:
                    nc_str = '-nn "%s"' %(niceName)
                    new_lines.append(nc_str)

                if dv != None:
                    dv_str = '-dv %s' %(dv)
                    new_lines.append(dv_str)
                if miv != None:
                    miv_str = '-min %s' %(miv)
                    new_lines.append(miv_str)
                if mxv != None:
                    mxv_str = '-max %s' %(mxv)
                    new_lines.append(mxv_str)

                if attrType in simple_attr_types:
                    type_str = '-at "%s"' %(attrType)
                else:
                    type_str = '-dt "%s"' %(attrType)
                new_lines.append(type_str)

                new_line_str = ' '.join(new_lines)
                new_line_str += ';\n'
                new_chunk.insert(line_index, new_line_str)
                new_chunk_index = i
                break

        if new_chunk:
            self.data['createNode'][new_chunk_index] = new_chunk
            # if value, 
            if value or lock or keyable:
                kwargs = {}
                if value:
                    kwargs['value'] = value
                if attrType and attrType not in simple_attr_types:
                    kwargs['attrType'] = attrType
                if lock:
                    kwargs['lock'] = lock
                if keyable:
                    kwargs['keyable'] = keyable
                self.setAttr(obj, shortName, **kwargs)
            return True
        else:
            return False

    def setAttr(self, obj, attr, value=None, attrType=None, lock=None, keyable=None):
        result = False
        if not self.data or not self.data.get('createNode'):
            return result

        bool_dict = {True: 'on', False: 'off'}
        new_chunk = []
        new_chunk_index = None
        for i, chunk in enumerate(self.data['createNode']):
            cmd_line = chunk[0]
            name = flag_string_value(line=cmd_line, flag='-n')
            if name == obj:
                new_chunk = list(chunk)
                new_lines = ['\tsetAttr']
                if lock != None:
                    lock_str = '-l %s' %(bool_dict[lock])
                    new_lines.append(lock_str)
                if keyable != None:
                    k_str = '-k %s' %(bool_dict[keyable])
                    new_lines.append(k_str)

                attr_str = '".%s"' %(attr)
                new_lines.append(attr_str)

                if attrType != None:
                    type_str = '-type "%s"' %(attrType)
                    new_lines.append(type_str)

                if value != None:
                    if isinstance(value, (str, unicode)):
                        value = '"%s"' %(value)
                    elif isinstance(value, (list, tuple)):
                        value = ' '.join([str(v) for v in value])
                    elif isinstance(value, bool):
                        value = bool_dict[value]
                    else:
                        value = str(value)
                    new_lines.append(value)
                new_line_str = ' '.join(new_lines)
                new_line_str += ';\n'

                # replace old setAttr line if exists
                for l, line in enumerate(chunk):
                    if line.startswith('\tsetAttr') and attr_str in line:
                        new_chunk[l] = new_line_str
                        break
                else:
                    new_chunk.append(new_line_str)
                new_chunk_index = i
                break

        if new_chunk:
            self.data['createNode'][new_chunk_index] = new_chunk
            return True
        else:
            return False

    def getAttr(self, obj, attr):
        result = None
        if not self.data or not self.data.get('createNode'):
            return result

        isSliced = True if re.search('[[0-9]+:.*]', attr) else False 
        for chunk in self.data['createNode']:
            cmd_line = chunk[0]
            name = flag_string_value(line=cmd_line, flag='-n')
            if name == obj:
                for line in chunk[1:]:
                    if line.startswith('\tsetAttr'):
                        line_splits = line.split(' ')
                        attr_str = '".%s"' %attr
                        if attr_str in line:
                            # get attribute type
                            attr_type = flag_string_value(line, '-type')

                            # it's a simple numeric type
                            if not attr_type:
                                value = line_splits[-1]
                                value = value.replace('\n', '')
                                value = value.replace('\t', '')
                                value = value.replace(';', '')
                                value = value.replace('"', '')
                                # in case no value to set - setAttr -l on -k off ".tx";
                                if value != attr_str[1:-1]: 
                                    value = eval_data_type(value)
                                    return value
                            else:
                                index = line_splits.index('"%s"' %attr_type)
                                raw_values = line_splits[(index+1):]
                                values = []

                                # strip unneccessary strings
                                for v in raw_values:
                                    v = v.replace('\n', '')
                                    v = v.replace('\t', '')
                                    v = v.replace(';', '')
                                    v = v.replace('"', '')
                                    v = v.replace(' ', '')
                                    if not v:
                                        continue
                                    values.append(v)

                                # convert data to python object
                                converted = []
                                for v in values:
                                    vt = eval_data_type(v, attr_type)
                                    converted.append(vt)

                                # strip the list if it's type string
                                if attr_type == 'string':
                                    return ''.join(converted)
                                # int type
                                elif attr_type in ('short2', 'long2', 'float2', 'double2'):
                                    result = []
                                    pairs = []
                                    for i, v in enumerate(converted):
                                        pairs.append(v)
                                        if i%2:  # if it's equal number
                                            result.append(pairs)
                                            pairs = []
                                    if not isSliced:
                                        result = result[0]
                                    return result
                                # float types
                                elif attr_type in ('short3', 'long3', 'float3', 'double3'):
                                    result = []
                                    coords = []
                                    for i, v in enumerate(converted):
                                        coords.append(v)
                                        if (i+1)%3 == 0:  
                                            result.append(coords)
                                            coords = []
                                    if not isSliced:
                                        result = result[0]
                                    return result
                                # 4 by 4 matrix type
                                elif attr_type == 'matrix' and len(converted) == 16:
                                    return [converted[:3], converted[4:8], converted[8:12], converted[12:]]

                                return converted

    def nodeTypes(self):
        result = set()
        if not self.data or not self.data.get('createNode'):
            return result

        for chunk in self.data['createNode']:
            cmd_line = chunk[0]
            line_splits = cmd_line.split()
            result.add(line_splits[1])

        return result

    def requires(self):
        result = []
        if not self.data or not self.data.get('requires'):
            return result

        for chunk in self.data['requires']:
            cmd_line = chunk[0]
            line_splits = cmd_line.split()
            plugin_name = line_splits[-2]
            plugin_version = line_splits[-1]

            result.append((plugin_name, plugin_version))

        return result

    def listReferences(self):
        result = []
        if not self.data or not self.data.get('file'):
            return result

        for i, chunk in enumerate(self.data['file']):
            combined_line = ''.join(chunk)
            if ' -r ' in combined_line:
                namespace = flag_string_value(line=combined_line, flag='-ns')
                refNode = flag_string_value(line=combined_line, flag='-rfn')

                splits = combined_line.split()
                path = splits[-1].replace(';', '')
                path = path.replace('\n', '')
                path = path.replace('"', '')
                result.append({'namespace': namespace, 
                                'refNode': refNode,
                                'path': path})

        return result

    def find_node(self, name):
        result = collections.defaultdict(list)
        if not self.data:
            return result

        for cmd, chunks in self.data.iteritems():
            if cmd == 'createNode':
                for chunk in chunks:
                    combined_line = self._joiner.join(chunk)
                    node_name = flag_string_value(line=combined_line, flag='-n')
                    if node_name == name:
                        result[cmd].append(chunk)
            elif cmd in ('select', 'connectAttr', 'relationship'):
                for chunk in chunks:
                    combined_line = self._joiner.join(chunk)
                    if ' "%s"' %name in combined_line or '|%s"' %name in combined_line\
                     or ' "%s.' %name in combined_line or '|%s.' %name in combined_line:
                        result[cmd].append(chunk)

        return result

    def replaceReference(self, node, path):
        result = []
        if not self.data or not self.data.get('file'):
            return result

        typ_dict = {'.abc': 'Alembic', '.ma': 'mayaAscii', '.mb': 'mayaBinary'}
        for i, chunk in enumerate(self.data['file']):
            # namespace = ''
            refNode = ''
            # for line in chunk:
            combined_line = ''.join(chunk)
            combined_line = combined_line.replace('\n', '')
            combined_line = combined_line.replace('\t', '')
            if ' -r ' in combined_line or ' -rdi ' in combined_line:
                # namespace = flag_string_value(line=line, flag='-ns')
                refNode = flag_string_value(line=combined_line, flag='-rfn')

            if refNode == node:
                last_line = chunk[-1]
                splits = last_line.split()
                old_path = splits[-1].replace(';', '')
                old_path = old_path.replace('\n', '')
                old_path = old_path.replace('"', '')
                chunk[-1] = last_line.replace(old_path, path)

                # set file type
                op, op_ext = os.path.splitext(old_path)
                np, np_ext = os.path.splitext(path)
                curr_typ = flag_string_value(line=combined_line, flag='-typ')
                if curr_typ != typ_dict[np_ext]:
                    split_line = self._joiner.join(chunk)
                    split_line = split_line.replace(' "%s"' %curr_typ, ' "%s"' %typ_dict[np_ext])
                    chunk = split_line.split(self._joiner)

                self.data['file'][i] = chunk
                result.append(chunk)

        return result

    def editRefNamespace(self, node, new_namespace):
        result = []
        if not self.data or not self.data.get('file') or not self.data.get('createNode'):
            return result

        typ_dict = {'.abc': 'Alembic', '.ma': 'mayaAscii', '.mb': 'mayaBinary'}
        repalced_refs = {}
        for i, chunk in enumerate(self.data['file']):
            # namespace = ''
            refNode = ''
            # for line in chunk:
            combined_line = ''.join(chunk)
            combined_line = combined_line.replace('\n', '')
            combined_line = combined_line.replace('\t', '')
            if ' -r ' in combined_line or ' -rdi ' in combined_line:
                # namespace = flag_string_value(line=line, flag='-ns')
                refNode = flag_string_value(line=combined_line, flag='-rfn')

            if refNode == node:
                curr_ns = flag_string_value(line=combined_line, flag='-ns')
                split_line = self._joiner.join(chunk)
                split_line = split_line.replace(' "%s"' %curr_ns, ' "%s"' %new_namespace)
                chunk = split_line.split(self._joiner)

                self.data['file'][i] = chunk
                result.append(chunk)
                repalced_refs[refNode] = curr_ns

        for i, chunk in enumerate(self.data['createNode']):
            objName = ''
            combined_line = ''.join(chunk)
            combined_line = combined_line.replace('\n', '')
            combined_line = combined_line.replace('\t', '')
            if ' -n ' in combined_line:
                objName = flag_string_value(line=combined_line, flag='-n')

            if objName in repalced_refs:
                split_line = self._joiner.join(chunk)
                split_line = split_line.replace('%s:' %repalced_refs[objName], '%s:' %new_namespace)
                chunk = split_line.split(self._joiner)

                self.data['createNode'][i] = chunk
                result.append(chunk)

        return result

    def removeNaN(self):
        if not self.data.get('createNode'):
            return

        new_data = collections.OrderedDict()
        infected = []
        names_to_removes = []

        for cmd, chunks in self.data.iteritems():
            clean_chunks = []
            if cmd == 'createNode':
                for chunk in chunks:
                    combined_line = self._joiner.join(chunk)
                    if ' -nan(ind) ' in combined_line or ' nan ' in combined_line:
                        infected.append(chunk)
                        node_name = flag_string_value(line=combined_line, flag='-n')
                        if node_name:
                            names_to_removes.append(node_name)
                    else:
                        clean_chunks.append(chunk)
            else:
                for chunk in chunks:
                    combined_line = self._joiner.join(chunk)
                    for name in names_to_removes:
                        if ' "%s"' %name in combined_line or '|%s"' %name in combined_line\
                         or ' "%s.' %name in combined_line or '|%s.' %name in combined_line:
                            infected.append(chunk)
                            break
                    else:
                        clean_chunks.append(chunk)

            if clean_chunks:
                new_data[cmd] = clean_chunks

        self.data = new_data
        return infected

def eval_data_type(value, attr_type=None):
    if attr_type in ('string', 'stringArray'):
        return value

    # try simple numeric type
    try: 
        value = literal_eval(value)
        if attr_type in ('short2', 'long2', 'short3', 'long3'):
            value = int(value)
        elif attr_type in ('float2', 'float3', 'double2', 'double3', 'matrix'):
            value = float(value)
        return value
    except ValueError:
        pass

    # try bool type
    if value in ('yes', 'no'):
        try:
            value = True if 'yes' else False
            return value
        except RuntimeError:
            pass

    # no type found just return the input
    return value

'''
from rf_utils import ascii_utils
reload(ascii_utils)

path = ''  # << edit here
output_path = ''  # << edit here
search = '' # << edit here, search = regular expression
types = [] # << edit here, file types

maya_scene = ascii_utils.MayaAscii(path)
maya_scene.read()
maya_scene.delete_node(search=search, types=types) 
maya_scene.write(output_path)

'''