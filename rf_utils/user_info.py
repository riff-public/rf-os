# publish info
import os
import sys
import getpass
import urllib
from re import findall
import uuid
try:
    from urlparse import urlparse
except ImportError:
    from urllib.parse import urlparse


import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

CACHES = dict()
IS_USING_MAC_ADDRESS = False

class Data:
    # returnFields = ['sg_localuser', 'sg_ad_account', 'name', 'department', 'groups', 'image']
    returnFields = [
            'sg_localuser', 'sg_ad_account', 'name', 'sg_name', 'login', 'department', 'groups', 
            'permission_rule_set', 'image', 'sg_reviewer', 'department.Department.code', 
            'email', 'sg_status_list','sg_local_name','sg_languages', 'sg_employee_id', 'sg_extra_accounts']


class User(object):
    """docstring for User"""
    def __init__(self, sg=None, nickname=None, adname=None):
        super(User, self).__init__()
        self.nickname = nickname
        self.sg = sg
        self.adname = self.login_user() if not adname and not nickname else adname
        self.userEntity = None

    def login_user(self):
        """ get login user """
        return getpass.getuser()

    def sg_user(self):
        """ shotgun user entity """
        if not self.sg:
            from rf_utils.sg.sg_wrapper import ShotgunClient
            sg_client = ShotgunClient()
            self.sg = sg_client.clone().instance

        if self.nickname in CACHES.keys():
            return CACHES[self.nickname]

        elif self.adname in CACHES.keys():
            return CACHES[self.adname]

        else:
            filters = [['sg_localuser', 'is', self.nickname]] if self.nickname else []
            filters.append(['sg_ad_account', 'is', self.adname]) if self.adname else None
            self.userEntity = self.sg.find_one('HumanUser', filters, Data.returnFields)
            # found no user using getuser()
            if not self.userEntity:
                mac_adress_user = self.get_user_from_mac_address()
                if mac_adress_user:
                    self.userEntity = mac_adress_user
                    self.adname = mac_adress_user.get('sg_ad_account')
                    global IS_USING_MAC_ADDRESS
                    IS_USING_MAC_ADDRESS = True

            if self.userEntity:
                # add caches
                CACHES[self.userEntity['sg_localuser']] = self.userEntity
                CACHES[self.userEntity['sg_ad_account']] = self.userEntity
                return self.userEntity
            else:
                # find all user and see if it's match field 
                # print('Find extra accounts')
                users = self.sg.find('HumanUser', [], Data.returnFields)
                for user in users: 
                    extra_accounts = user['sg_extra_accounts']
                    if extra_accounts: 
                        if self.adname in eval(extra_accounts): 
                            logger.debug('Found {} in extra account'.format(self.adname))
                            logger.debug(user)
                            self.userEntity = user 
                            break
                if self.userEntity: 
                    CACHES[self.userEntity['sg_localuser']] = self.userEntity
                    CACHES[self.userEntity['sg_ad_account']] = self.userEntity
                    return self.userEntity
                else: 
                    logger.warning('No user found')
                return dict()

    def get_user_from_mac_address(self):
        mac_address = '-'.join(findall('..', '{:012x}'.format(uuid.getnode())))
        filters = [['sg_localuser', 'is', self.nickname]] if self.nickname else []
        filters.append(['sg_mac_address', 'contains', mac_address]) if mac_address else None
        return self.sg.find_one('HumanUser', filters, Data.returnFields)

    def get_sg_field(self, field):
        """ get shotgun field """
        if not self.userEntity:
            self.userEntity = self.sg_user()
        if self.userEntity:
        	return self.userEntity.get(field)
        else:
        	logger.error('No user found in Shotgun')

    def permission_rule(self): 
        return self.get_sg_field('permission_rule_set')

    def is_producer(self): 
        name = self.permission_rule().get('name')
        return True if name == 'Producer' else False

    def is_admin(self): 
        name = self.permission_rule().get('name')
        return True if name == 'Admin' else False

    def is_artist(self): 
        name = self.permission_rule().get('name')
        return True if name == 'Artist' else False

    def is_supervisor(self): 
        name = self.permission_rule().get('name')
        return True if name == 'Supervisor' else False

    def is_hr(self): 
        name = self.permission_rule().get('name')
        return True if name == 'Hr' else False

    def department(self):
        return self.get_sg_field('department')

    def groups(self):
        return self.get_sg_field('groups')

    def local_user(self):
        return self.get_sg_field('sg_localuser')

    def name(self):
        return self.get_sg_field('sg_name')

    def image(self, ext='.jpg'):
        thumbnail = '%s%s' % (self.local_user(), ext)
        path = '%s/%s' % (self.userdir(), thumbnail)
        return path

    def get_thumbnail(self, download=True):
        if not os.path.exists(self.image()):
            return self.download_image()
        return self.image()

    def image_url(self):
        return self.get_sg_field('image')

    def download_image(self):
        path = ''
        url = self.image_url()
        if url:
            ext = self.get_ext(url)
            path = self.image(ext)
            if not os.path.exists(os.path.dirname(path)):
                os.makedirs(os.path.dirname(path))
            urllib.urlretrieve(url, path)
        return path if os.path.exists(path) else None

    def userdir(self):
        user = '%s' % (self.local_user())
        root = '%s/staffs/%s' % (os.environ['RFSCRIPT'], user)
        return root

    def get_ext(self, url):
        """Return the filename extension from url, or ''."""
        parsed = urlparse(url)
        root, ext = os.path.splitext(parsed.path)
        return ext



class SGUser(object):
    """ Shotgun user utilities class """
    def __init__(self):
        super(SGUser, self).__init__()
        self.fetch_users()

    def fetch_users(self):
        """ fetch all users information """
        # from rf_utils.sg import sg_utils
        # self.sg = sg_utils.sg
        from rf_utils.sg.sg_wrapper import ShotgunClient
        sg_client = ShotgunClient()
        self.sg = sg_client.clone().instance

        fields = Data.returnFields
        self.userEntities = self.sg.find('HumanUser', [], fields)

    def list_reviewers(self):
        userEntities = [a for a in self.userEntities if a['sg_reviewer']]
        return userEntities

    def get_supervisor(self, entity):
        supervisors = entity.projectInfo.team.supervisor(entity.entity_type, entity.step)
        supervisors = [a.lower() for a in supervisors]
        supervisorEntities = [a for a in self.userEntities if a.get('sg_name').lower() in supervisors]
        return supervisorEntities

    def get_producer(self, entity):
        producers = entity.projectInfo.team.producer(entity.entity_type, entity.step)
        producerEntities = []
        if producers:
            producers = [a.lower() for a in producers]
            producerEntities = [a for a in self.userEntities if a.get('sg_name').lower() in producers]
        return producerEntities

    def get_user_entity(self, name, key='sg_name'):
        for user in self.userEntities:
            if user.get(key).lower() == name.lower() or user.get('name').lower() == name.lower():
                return user

    def get_user_local_name(self, name, key='sg_name'):
        for user in self.userEntities:
            if user.get(key).lower() == name.lower() or user.get('name').lower() == name.lower():
                thaiName = user.get('sg_local_name')
                return thaiName

    def get_user_language(self, name, key='sg_name'):
        for user in self.userEntities:
            if user.get(key).lower() == name.lower() or user.get('name').lower() == name.lower():
                language = user.get('sg_languages')
                return language
