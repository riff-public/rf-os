import nuke

def current_path():
    return nuke.scriptName()

def take_screenshot(path):
    viewer = nuke.activeViewer()
    actInput = nuke.ViewerWindow.activeInput(viewer)
    viewNode = nuke.activeViewer().node()
    selInput = nuke.Node.input(viewNode, actInput)

    # scale inputs
    rfm = nuke.nodes.Reformat(type='to box', name='ss_reformat', box_width=1024, box_fixed=False)
    rfm.setInput(0, selInput)
    
    # png, srgb, 8bit
    write1 = nuke.nodes.Write(file=path, name='ss_write1', file_type='jpg')
    write1.setInput(0, rfm)

    # look up current frame
    curFrame = int(nuke.knob("frame"))
    # start the renders
    nuke.execute(write1.name(), curFrame, curFrame)
    # clean up
    nuke.delete(write1)
    nuke.delete(rfm)

    return True