import ctypes
import maya.cmds as mc
import maya.OpenMaya as om
import maya.OpenMayaUI as omui

def current_path():
    return mc.file(q=True, sn=True)

def take_screenshot(path):
    view = omui.M3dView.active3dView()
    width = view.portWidth()
    height = view.portHeight()
    img = om.MImage()
    view.readColorBuffer(img, True)
    img.writeToFile(path, 'jpg')

    return True