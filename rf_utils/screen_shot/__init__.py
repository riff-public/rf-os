import sys, os

import rf_config as config
from rf_utils.context import context_info

APP = None
HOOK = None
if config.isMaya:
    import maya_hook
    APP = 'maya'
    HOOK = maya_hook
elif config.isNuke:
    import nuke_hook
    APP = 'nuke'
    HOOK = nuke_hook


def capture():
    if not APP:
        return

    # check current path is within workspace
    curr_path = HOOK.current_path()
    scene = context_info.ContextPathInfo(path=curr_path)
    if not scene.valid:
        return
    ws_path = scene.path.workspace().abs_path()
    curr_dir = os.path.dirname(curr_path)
    if not os.path.exists(ws_path) and ws_path == curr_dir:
        return

    # build screenshot path
    preview_dir = '{}/.preview'.format(curr_dir)
    if not os.path.exists(preview_dir):
        os.makedirs(preview_dir)
    basename = os.path.basename(curr_path)
    fn, ext = os.path.splitext(basename)
    ss_path = '{}/.{}.jpg'.format(preview_dir, fn)

    # do the capture
    result = HOOK.take_screenshot(path=ss_path)
    if result:
        print 'Screenshot saved: {}'.format(ss_path)