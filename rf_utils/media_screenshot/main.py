import sys
import os
import cv2
import numpy as np
from math import floor

def get_frames_at(capture, indices):
    
    read_flag, frame = capture.read()
    vid_frames = []
    i = 1
    f = 0
    while (read_flag):
        if i in indices:
            vid_frames.append(frame)
            f += 1
            if f == len(indices):
                break
        read_flag, frame = capture.read()
        i += 1
    vid_frames = np.asarray(vid_frames, dtype='uint8')
    return vid_frames

def take_screenshots(input_paths, output_dir, chunk_size=120):
    outputs = []
    for pi, path in enumerate(input_paths):
        print('Getting frame: {pi}: {path}'.format(pi=pi, path=path))
        capture = cv2.VideoCapture(path)
        total = int(capture.get(cv2.CAP_PROP_FRAME_COUNT))
        num_ss = int(floor(total / float(chunk_size)))
        num_ss = 1 if num_ss==0 else num_ss
        fraction = 1.0/(num_ss+1)
        weight_list = [fraction*(i+1) for i in range(num_ss)]
        poster_indices = [int(total * w) for w in weight_list]
        fn = os.path.splitext(os.path.basename(path))[0]
        frames = get_frames_at(capture=capture, indices=poster_indices)
        for fi, f in enumerate(frames):
            output_path = '{}/{}_{}_{}.png'.format(output_dir, (pi+1), (fi+1), fn)
            print('Writing to: {output_path}'.format(output_path=output_path))
            cv2.imwrite(output_path, f)
            outputs.append(output_path)
        capture.release()

    return outputs


        

