import os 
import sys 
from oauth2client.service_account import ServiceAccountCredentials
import gspread
from pprint import pprint
import pandas as pd
from googleapiclient.discovery import build
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

MODULEDIR = os.path.dirname(__file__).replace('\\', '/')

################# install gspread ##################
######## install or upgrade oauth2client ###########


##############################------SCOPE------###################################
''' sheets readonly      = 'https://www.googleapis.com/auth/spreadsheets.readonly'
    sheets read/write    = 'https://www.googleapis.com/auth/spreadsheets'
    drive readonly       = 'https://www.googleapis.com/auth/drive.readonly'
    drive created/opened = 'https://www.googleapis.com/auth/drive.file'
    drive full access    = 'https://www.googleapis.com/auth/drive' '''
##################################################################################

class Setting: 
    gsheet_r = 'https://www.googleapis.com/auth/spreadsheets.readonly'
    gsheet_rw = 'https://www.googleapis.com/auth/spreadsheets'
    gdrive_r = 'https://www.googleapis.com/auth/drive.readonly'
    gdrive_created_opened = 'https://www.googleapis.com/auth/drive.file'
    gdrive_full = 'https://www.googleapis.com/auth/drive'
    keyfile = '{}/key/credentials.json'.format(MODULEDIR)
    

class Scope: 
    sheet_full = [Setting.gsheet_rw, Setting.gdrive_full]


def createAccount(keysfile, scope):
    """ connecting to googleAccount through keyfile """ 
    credentials = ServiceAccountCredentials.from_json_keyfile_name(keysfile,scope)
    client = gspread.authorize(credentials)
    return client


def buildService(keysfile, scope): 
    credentials = ServiceAccountCredentials.from_json_keyfile_name(keysfile,scope)
    service = build('sheets', 'v4', credentials=credentials)
    return service


def openByTitle(client, title):
    """ open googleSheet file by title name """
    sheet = client.open(title)
    return sheet


def openByURL(client, url):
    """ open googleSheet file by url """
    sheet = client.open_by_url(url)
    return sheet


def list_sheets(client): 
    sheets = list()
    for sheet in client.openall(): 
        sheets.append(sheet.title)
    return sheets


def create_new_sheet(client, name):
    """ Create a new googleSheet file """
    sheet = client.create(name)
    return sheet


def share_sheet(sheet, email, account_type, role):
    ############## account_type #################
          # user,group,domain,anyone
    ################### role ####################
            # owner, writer, reader
    sheet.share(email, perm_type=account_type, role=role)


def list_permissions(sheet):
    """ list premission of the file """
    permissions = sheet.list_permissions()
    return permissions


def remove_permissions(sheet, email):
    """ remove permission of the file """
    sheet.remove_permissions(email)


def select_worksheetByIndex(sheet, index):
    """ select tab by index """
    worksheet = sheet.get_worksheet(index)
    return worksheet    


def select_worksheetByTitle(sheet, title):
    """ select tab by tab name """
    worksheet = sheet.worksheet(title)
    return worksheet


def new_worksheet(sheet, title, row, column, index=None):
    """ new tab """
    return sheet.add_worksheet(title=title, rows=row, cols=column,index=index)


def update_val(worksheet, acell, val):
    """ update value in tab 
    acell ex. 'A1' 
    """
    worksheet.update(acell, val)


def update_val_cell(worksheet, row, column, val):
    """ update cell value """
    worksheet.update_cell(row, column, val)


def get_val_acell(worksheet, acell):
    """ get acess value """
    val = worksheet.get(acell)
    return val


def get_val_cell(worksheet, row, column):
    """ get value by row and column """
    val = worksheet.cell(row, column).value
    return val


def get_all_val_dicts(worksheet):
    """ get all value in tab """
    val_dicts = worksheet.get_all_records()
    return val_dicts


def get_all_val_list(worksheet):
    """ get all value as a list """
    val_list = worksheet.get_all_values()
    return val_list


def get_col_val(worksheet, col):
    """ get column value """
    values_list = worksheet.col_values(col)
    return values_list


def append_row(worksheet, val):
    """ add row """
    worksheet.append_row(val)


def find_item(worksheet, val):
    """ find cell by value """
    list_cell = worksheet.findall(val)
    return list_cell


def add_column(worksheet, nColumn):
    """ add n number of columns """
    worksheet.add_cols(nColumn)


def add_row(worksheet, nRow):
    """ add n number of rows """
    worksheet.add_rows(nRow)


def list_worksheet(sheet):
    """ list tabs of sheet """
    ws_list = sheet.worksheets()
    title_list = []

    for wsh in ws_list:
        title_list.append(wsh.title)
    return title_list


def write_data(service, sheet, worksheet, data):

    service.spreadsheets().values().append(
            spreadsheetId = sheet.id,
            range = "{}!A:Z".format(worksheet),
            body = {
                    "majorDimension": "ROWS",
                    "values": data, 
                    },
            valueInputOption="USER_ENTERED").execute()


def resize_cell(service, sheet, worksheet, col_start, col_end, col_size): 
    request_body = {
                      "requests": [
                        {
                          "updateDimensionProperties": {
                            "range": {
                              "sheetId": worksheet.id,
                              "dimension": "COLUMNS",
                              "startIndex": col_start,
                              "endIndex": col_end
                            },
                            "properties": {
                              "pixelSize": col_size
                            },
                            "fields": "pixelSize"
                          }
                        },
                      ]
                    }
    response = service.spreadsheets().batchUpdate(spreadsheetId=sheet.id, body=request_body).execute()


def set_cell_color(service, sheet, worksheet, col_start, col_end, row_start, row_end, red, green, blue):
    #credentials_file = credentials path(str)
    #sheets = google sheet name (str)
    #worksheet = google tab name (str)
    #col_start, col_end = range of columns (int)
    #row_start, row_end = range of row (int)
    #red, green, blue = color (int)

    request_body = {
                      "requests": [
                        {
                          "repeatCell": {
                            "range": {
                              "sheetId": worksheet.id,
                              "startRowIndex": row_start,
                              "endRowIndex": row_end,
                              'startColumnIndex': col_start,
                              'endColumnIndex': col_end
                            },
                            "cell": {
                              "userEnteredFormat": {
                                "backgroundColor": {
                                  "red": red,
                                  "green": green,
                                  "blue": blue
                                },
                              }
                            },
                            "fields": "userEnteredFormat(backgroundColor,textFormat,horizontalAlignment)"
                          }
                        },
                      ]
                    }
    response = service.spreadsheets().batchUpdate(spreadsheetId=sheet.id, body=request_body).execute()


def clear_cell(service, sheet, worksheet, fields='*'): 
    request_body = {
                      "requests": [
                        {
                          "updateCells": {
                            "range": {
                              "sheetId": worksheet.id,
                            },
                            "fields": "{}".format(fields)
                          }
                        },
                      ]
                    }
    response = service.spreadsheets().batchUpdate(spreadsheetId=sheet.id, body=request_body).execute()


class GoogleSheet(object):
    """docstring for GoogleSheet"""
    def __init__(self, url=''):
        super(GoogleSheet, self).__init__()
        self.url = url 
        self.init_service()

    def init_service(self): 
        if self.url: 
            self.client = createAccount(Setting.keyfile, Scope.sheet_full)
            self.service = buildService(Setting.keyfile, Scope.sheet_full)
            self.sheet = openByURL(client, self.url)
        else: 
            logger.warning('No url. service not start')

    def write_cell(self, worksheet, data): 
        self.service.spreadsheets().values().append(
                spreadsheetId = self.sheet.id,
                range = "{}!A:Z".format(worksheet),
                body = {
                        "majorDimension": "ROWS",
                        "values": data, 
                        },
                valueInputOption="USER_ENTERED").execute()



def test():
    """ test function """
    scope = ["https://www.googleapis.com/auth/spreadsheets","https://www.googleapis.com/auth/drive"]

    keysfile = 'D:/googlesheet/key/credentials.json'

    client = createAccount(keysfile, scope)

    sheet = openByURL(client, 'https://docs.google.com/spreadsheets/d/1c8F2L0ndzgv1qzf5on7-J3t0GHLaQ1f3gznfJ6Ad7AQ/edit?usp=sharing')

    sheet2 = openByTitle(client, 'test')

    sheet3 = create_new_sheet(client, 'test3')

    email = 'bossdevil62@gmail.com'
    share_sheet(sheet3, email, 'user', 'owner')

    premissions = list_permissions(sheet)

    remove_permissions(sheet2, email) #only owner

    ws = select_worksheetByIndex(sheet, 0)

    ws2 = select_worksheetByTitle(sheet, 'A worksheet')

    ws3 = new_worksheet(sheet, 'New Worksheet', 10, 10)

    update_val(ws, 'A1', 'Itsada')

    update_val_cell(ws,1,2,'Somnark')

    val = get_val_acell(ws, 'A1')

    val2 = get_val_cell(ws, 1, 1)

    val3 = get_all_val_dicts(ws)

    val4 = get_all_val_list(ws)


    info = [100, 200, 300, 400]
    append_row(ws, info)

    add_column(ws, 5)

    add_row(ws, 5)

    list_cell = find_item(ws, 'Itsada')
    row = list_cell[0].row
    column = list_cell[0].col





