import os 
import sys 
from rf_utils.context import context_info 
from rf_utils import file_utils
from collections import OrderedDict

def log_path(entity): 
	path = 'P:/%s/_log/scene/' % entity.project
	name = '%s_output.yml' % entity.name
	return '%s/%s' % (path, name)

def write_log(entity, namespace, path): 
	init_log(entity)
	data = read_log(entity)

	if not entity.name in data.keys(): 
		contents = OrderedDict()
		info = OrderedDict()
		info['path'] = path
		contents[namespace] = info
		data[entity.name] = contents 
	else: 
		contents = data[entity.name]
		if namespace in contents.keys(): 
			info = contents[namespace]
			info = add_dict(info, path=path)
		else: 
			info = OrderedDict()
			info = add_dict(info, path=path)
			contents[namespace] = info

	logFile = log_path(entity)
	file_utils.ymlDumper(logFile, data)

def add_dict(info, **kwargs): 
	for k, v in kwargs.iteritems(): 
		info[k] = v
	return info 

def read_log(entity): 
	logFile = log_path(entity)
	data = file_utils.ymlLoader(logFile) or OrderedDict()
	return data 

def init_log(entity): 
	logFile = log_path(entity)
	if not os.path.exists(os.path.dirname(logFile)): 
		os.makedirs(os.path.dirname(logFile))
	
	if not os.path.exists(logFile): 
		file_utils.ymlDumper(logFile, OrderedDict())




""" 
data = {'scm_act1_q0010_s0010': [{'namespace': 'arthur_001', 'path': 'path'}]}
data = {'scm_act1_q0010_s0010': arthur_001: {'path': path}}
"""