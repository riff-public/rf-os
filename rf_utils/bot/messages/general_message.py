#!/usr/bin/env python
# -- coding: utf-8 --

import os 
import sys 
import random
import lucidity
import getpass
from datetime import datetime, date, timedelta
from rf_utils.pipeline.notification import noti_module_cliq as noti
from rf_utils import user_info
from . import message_config

def timeoff_message(): 
	day = datetime.now().strftime('%a')
	if day in message_config.weekdays: 
		user = user_info.User()
		sgUser = user.sg_user()
		localname = sgUser['sg_local_name']
		email = sgUser.get('email')
		message = get_message()
		template = lucidity.Template('Message', message)
		message = template.format({'user': localname})

		if email: 
			noti.main(botName='jengchanbot', message=message, emails=[email])
		else: 
			logger.warning('No email found')


def get_message(): 
	msg_config = message_config.timeoff
	head_msgs = msg_config['head']
	body_msgs = msg_config['body']
	end_msgs = msg_config['end']
	add_msgs = list()
	workday_msg = msg_config['workday_messages']
	special_msg = msg_config['special_date']
	add_msg = ''

	# config time 
	oneday = timedelta(1, 0, 0)
	today = datetime.today()
	day = today.strftime('%a')

	if day in workday_msg.keys(): 
		add_msgs = workday_msg[day]

	for datestr, messages in special_msg.items(): 
		ref_date = datetime.strptime(datestr, '%Y-%m-%d')
		if today < ref_date: 
			diff_day = ref_date - today

			if diff_day < oneday: 
				add_msgs = messages

	if add_msgs: 
		add_msg = random_pick(add_msgs)

	head_msg = random_pick(head_msgs)
	body_msg = random_pick(body_msgs)
	end_msg = random_pick(end_msgs)
	combine_message = (' ').join([head_msg, body_msg, add_msg, end_msg])

	return combine_message


def random_pick(data_list): 
	return data_list[random.randint(0, (len(data_list) - 1))]
