#!/usr/bin/env python
# -- coding: utf-8 --

import os 
import sys 
import random
import lucidity
import getpass
from rf_utils.pipeline.notification import noti_module_cliq as noti
from rf_utils import user_info


def timeoff_message(): 
	user = user_info.User()
	sgUser = user.sg_user()
	localname = sgUser['sg_local_name']
	email = sgUser.get('email')
	messages = ['{user}คะ เลิกงานแล้วววว กลับบ้านกันเถอะ', 
				'{user} วันนี้เหนื่อยไม๊ เลิกงานแล้ว กลับบ้านพักผ่อนนะคะ', 
				'เลิกงานแล้ว {user} พักผ่อน ดูแลสุขภาพนะคะ', 
				'เลิกงานแล้ววว วันนี้{user}ทำงานได้เยอะเลย ขอบคุณนะคะ', 
				'เลิกงานแล้ว ช่วงนี้ WFH {user}ดูแลสุขภาพด้วยนะคะ', 
				'เลิกงานแล้ว {user}กลับบ้านค่าาา (ถ้างานน้องเสร็จแล้วนะ)']
	message_random = messages[random.randint(0, (len(messages) - 1))]
	template = lucidity.Template('Message', message_random)
	message = template.format({'user': localname})

	if email: 
		noti.main(botName='jengchanbot', message=message, emails=[email])
	else: 
		logger.warning('No email found')
