import os
import sys
import shutil
import logging
from datetime import datetime
from collections import OrderedDict

# pipeline
import rf_config as config
from rf_utils import file_utils
from rf_utils import admin
from rf_utils.context import context_info
from rf_utils.pipeline import version
reload(version)
cacheInfo = dict()

class Config:
    pass

class Cache: 
    renderVersion = OrderedDict()

class RenderData(object):
    def __init__(self, project, shotName, shotId='none'):
        self.project = project
        self.shotName = shotName
        self.shotId = shotId
        self.elements = OrderedDict()

    def __repr__(self):
        result = []
        for elem, vdata in self.elements.iteritems():
            versions = vdata.get_version_names()
            result.append((elem, versions))
        return str(result)

    @classmethod
    def init_from_dict(cls, data):
        new_obj = cls(project='', shotName='')
        if 'project' in data:
            new_obj.project = data['project']
        if 'shotName' in data:
            new_obj.shotName = data['shotName']
        if 'shotId' in data:
            new_obj.shotId = data['shotId']
        if 'renders' in data:
            render_dict = data['renders']
            for element in render_dict:
                for elem, versions in element.iteritems():
                    render_elem = RenderELement(name=elem)
                    for version, version_data in versions.iteritems():
                        creator = version_data['creator']
                        step = version_data['step']
                        create_date = version_data['create_date']
                        path = version_data['path']
                        overwritten = version_data['overwritten']
                        histories = version_data['histories']
                        render_ver = RenderVersion(name=version, 
                                        creator=creator, 
                                        step=step, 
                                        create_date=create_date,
                                        path=path,
                                        overwritten=overwritten,
                                        histories=histories)
                        render_elem.versions[version] = render_ver
                    new_obj.elements[elem] = render_elem
        return new_obj

    def get_element_by_name(self, element_name):
        for elem in self.elements.values():
            if elem.name == element_name:
                return elem

    def to_dict(self):
        data = OrderedDict()
        data['project'] = self.project
        data['shotName'] = self.shotName
        data['shotId'] = self.shotId

        elements = [e.to_dict() for e in self.elements.values()]
        data['renders'] = elements
        return data

    def get_element_names(self):
        return self.elements.keys()

    def get_render_element_version(self, element_name, version_name):
        if element_name in self.elements:
            element = self.elements[element_name]
            if version_name in element.versions:
                return element.versions[version_name]

class RenderELement(object):
    def __init__(self, name):
        self.name = name
        self.versions = OrderedDict()

    def __repr__(self):
        return str((self.name, self.get_version_names()))

    def get_version_by_name(self, version_name):
        for ver in self.versions.values():
            if ver.name == version_name:
                return ver

    def to_dict(self):
        data = OrderedDict()
        versions = OrderedDict()
        for ver in self.versions.values():
            version_data = ver.to_dict()
            versions[ver.name] = version_data
        data[self.name] = versions
        return data

    def get_version_names(self):
        return self.versions.keys()

    def get_version_paths(self):
        paths = []
        for version_name, version in self.versions.iteritems():
            path = version.path
            paths.append(path)
        return paths
        
class RenderVersion(object):
    def __init__(self, name, creator, step, create_date, path, overwritten=False, histories=[]):
        self.name = name
        self.creator = creator
        self.step = step
        self.create_date = create_date
        self.path = path
        self.overwritten = overwritten
        self.histories = histories

    def __repr__(self):
        return str(dict(self.to_dict()))

    def to_dict(self):
        version_data = OrderedDict()
        version_data['creator'] = self.creator
        version_data['step'] = self.step
        version_data['create_date'] = self.create_date
        version_data['path'] = self.path
        version_data['overwritten'] = self.overwritten
        version_data['histories'] = self.histories
        return version_data

class Register(object):
    """docstring for Register"""
    def __init__(self, ContextPathInfo):
        self.entity = ContextPathInfo
        self.configKey = '{0}_render'.format(self.entity.name)
        self.prodName = 'temp_render.yml'
        self.configHero = '{0}.hero.yml'.format(self.configKey)
        self.configVersion = '{0}.$version.yml'.format(self.configKey)
        self.regFile = self.tmp_config_path()
        
        # read existing data
        self.data = None
        self.read()

    def tmp_config_path(self):
        return '%s/%s' % (self.entity.path.render_data().abs_path(), self.prodName)

    def hero_config_path(self):
        return '%s/%s' % (self.entity.path.render_data().abs_path(), self.configHero)

    def config_version(self):
        configPath = self.entity.path.render_data().abs_path()
        if os.path.exists(configPath):
            return ['%s/%s' % (configPath, a) for a in file_utils.list_file(configPath)]
        return []

    def read(self):
        dataFile = self.config_path()
        data = file_utils.ymlLoader(dataFile) if os.path.exists(dataFile) else OrderedDict()
        self.data = data if not data == None else OrderedDict()

    def config_path(self):
        return self.tmp_config_path() if os.path.exists(self.tmp_config_path()) else self.hero_config_path()

    def set_hero_config(self, reset=False):
        if not reset:
            heroConfigFile = self.hero_config_path()
            data = file_utils.ymlLoader(heroConfigFile) if os.path.exists(heroConfigFile) else OrderedDict()
            self.data = data if not data == None else OrderedDict()
        if reset:
            self.read()

    def is_hero_config(self):
        return False if os.path.exists(self.tmp_config_path()) else True if self.hero_config_path() else None

    def read_version(self):
        if not Cache.renderVersion: 
            configVersions = self._get_register_version()
            if configVersions:
                for configVersion in configVersions:
                    name = os.path.basename(configVersion)
                    if not name in Cache.renderVersion.keys():
                        data = file_utils.ymlLoader(configVersion)
                        Cache.renderVersion[name] = data
        return Cache.renderVersion

    def write(self):
        dataFile = self.tmp_config_path()
        os.makedirs(os.path.dirname(dataFile)) if not os.path.exists(os.path.dirname(dataFile)) else None
        file_utils.ymlDumper(dataFile, self.data)
        return dataFile

    def register(self):
        """ write hero file and version """
        dataFile = self.write()
        publishData = self.publish_version(dataFile)
        self.remove_temp(dataFile)
        return publishData or dataFile

    def publish_version(self, dataFile):
        versionFile = self.get_publish_name(dataFile)
        heroFile = self.get_hero_name(dataFile)
        fileNew = self._is_file_new(dataFile, heroFile)
        if fileNew:
            shutil.copy2(dataFile, versionFile)
            shutil.copy2(dataFile, heroFile)
            return versionFile if os.path.exists(versionFile) else False

    def remove_temp(self, dataFile):
        admin.remove(dataFile)
        # os.remove(dataFile)

    def get_title(self):
        project = self.entity.project if self.entity else None
        shotName = self.entity.name if self.entity else None
        shotId = self.entity.id if self.entity else None
        return project, shotName, shotId

    def update_render_version(self, element, version, user, step, path, comment='',requeued=False):
        render_data = self.get_data()

        # date
        current_date = datetime.now().strftime("%d/%m/%y-%H:%M:%S")

        # history
        his = OrderedDict()
        his['modified_by'] = user
        his['create_date'] = current_date
        his['comment'] = unicode(comment)

        # first time creating this render element
        if element not in [e.name for e in render_data.elements.values()]:
            render_element = RenderELement(name=element)
        else:  # element already exists
            render_element = render_data.get_element_by_name(element_name=element)
        
        render_data.elements[render_element.name] = render_element
        # create new version for this element
        existing_versions = [v.name for v in render_element.versions.values()]
        if version not in existing_versions:
            render_version = RenderVersion(name=version, 
                            creator=user, 
                            step=step, 
                            create_date=current_date, 
                            path=path, 
                            overwritten=False, 
                            histories=[his])
            render_element.versions[render_version.name] = render_version
        else:  # modify existing version, update history
            render_version = render_element.get_version_by_name(version_name=version)
            render_version.histories.append(his)

            render_version.overwritten = requeued

        self.data = render_data.to_dict()

    def get_data(self):
        # read data from yml
        if self.data:
            render_data = RenderData.init_from_dict(self.data)
        else:  # first time
            project, shotName, shotId = self.get_title()
            render_data = RenderData(project=project, shotName=shotName, shotId=shotId)
        return render_data

    # --- Utilites functions
    def get_publish_name(self, dataFile):
        dirname = os.path.dirname(dataFile)
        replaceVersion = version.increment_version(os.path.dirname(dataFile))
        configFileVersion = self.configVersion.replace('$version', replaceVersion)
        configFile = '%s/%s' % (os.path.dirname(dataFile), configFileVersion)
        return configFile

    def get_hero_name(self, dataFile):
        dirname = os.path.dirname(dataFile)
        heroFile = '%s/%s' % (dirname, self.configHero)
        return heroFile

    def _is_file_new(self, dataFile, heroFile):
        if not os.path.exists(heroFile):
            return True
        data1 = file_utils.ymlLoader(dataFile)
        data2 = file_utils.ymlLoader(heroFile)
        return True if not data1 == data2 else False

    def _get_register_version(self):
        dataDir = self.entity.path.render_data().abs_path()
        files = file_utils.list_file(dataDir) if os.path.exists(dataDir) else []
        if files:
            files.remove(self.configHero) if self.configHero in files else None
            configFiles = [('/').join([dataDir, a]) for a in files]
            return configFiles

def combine_dict(newDict, currentDict):
    if type(currentDict) == type(OrderedDict()) and type(newDict) == type(OrderedDict()):
        for k, v in newDict.iteritems():
            if not k in currentDict.keys():
                currentDict[k] = v

            else:
                currentValue = currentDict[k]
                newValue = v
                currentDict[k] = combine_dict(newValue, currentValue)
    else:
        return newDict
    return currentDict


'''

from rf_utils.context import context_info
reload(context_info)
scene = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
from rf_utils import register_render
reload(register_render)


reg = register_render.Register(ContextPathInfo=scene)


render_data = reg.get_data()


reg.update_render_version(element='PropA_Beauty', 
        version='v001', 
        user='Ta', 
        step='Light', 
        path='P:/Hanuman/scene/publ/act1/hnm_act1_q0020_s0120/render/output/charA_Beauty/v001/hnm_act1_q0020_s0120_charA_Beauty.####.exr', 
        comment='fix black frame on 1001-1005')
reg.register()

charB = render_data.elements['charB_Beauty']
charB_v2 = charB.versions['v002']

charB_v2.creator
charB_v2.histories[-1]['create_date']


render_data.get_render_element_version('charB_Beauty', 'v004')

render_data.get_element_names()

elem = render_data.elements['charB_Beauty']
version = elem.versions['v004']
version.path
version.histories[-1]

'''