import os
import sys

sys.path.append('{}/core'.format(os.environ['RFSCRIPT']))
from rf_utils import admin

def main():
    # get computer name
    com_name = os.environ['COMPUTERNAME']
    local_path = 'C:/Program Files/Nuke11.3v4/OpticalFlaresLicense.lic'
    if os.path.exists(local_path):
        global_path = 'O:/Pipeline/plugins/Nuke/OpticalFlares_Nuke_11.3/license/OpticalFlaresLicense_{}.lic'.format(com_name)
        admin.win_copy_file(local_path, global_path)
        print 'Register success.'
    else:
    	print 'Cannot find license in local drive.'
if __name__ == '__main__':
    main()