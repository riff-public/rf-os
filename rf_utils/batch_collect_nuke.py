import sys, os
core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

import argparse
from collections import OrderedDict
# import logging
# logger = logging.getLogger(__name__)
# logger.addHandler(logging.NullHandler())

# from rf_utils.context import context_info

import getpass
import logging
import rf_config as config
from rf_utils import log_utils

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name('test_img_seq', user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-s', dest='nuke_path', type=str, help='The nuke_path to the source scene to be cached')

    return parser

def main(nuke_path='Z:/NPC/post/02_compositing/ep06/ep06_0010/ver/ep06_0010_comp_v01.nk'):
    import nuke
    # panelResult = collectPanel()
    print '1'
    print nuke_path
    print nuke
    nuke.scriptOpen(nuke_path)
    print nuke_path
    #copy script to target directory
    script2Copy = nuke.root()['name'].value()
    scriptName = os.path.basename(nuke.Root().name())

    fileNames = []
    paddings = ['%01d', '%02d', '%03d', '%04d', '%05d', '%06d', '%07d', '%08d', '%d', '%1d']
    videoExtension = ['mov', 'avi', 'mpeg', 'mpg', 'mp4', 'R3D']
    cancelCollect = 0
    # hit OK
    if nuke_path != '':
        targetPath = nuke_path

        # Make sure target path ends with a slash (for consistency)
        if not targetPath.endswith('/'):
            targetPath += '/'

        # Check if local directory already exists. Ask to create it if it doesn't
        if not os.path.exists(targetPath):
            os.makedirs(targetPath)

       
        # Get script name
        scriptName = os.path.basename(nuke.Root().name())
    
        footagePath = targetPath + 'footage/'
        if (os.path.exists(footagePath)):
            pass
        else:
            os.mkdir(footagePath)

        # task = nuke.ProgressTask("Collect Files 1.2")
        # count = 0

        for fileNode in nuke.allNodes():
            # if task.isCancelled():
            #     cancelCollect = 1
            #     break
            # count += 1
            # task.setMessage("Collecting file:   " + str(fileNode))
            # task.setProgress(count*100/len(nuke.allNodes()))


            if checkForKnob(fileNode, 'file'):
                if not checkForKnob(fileNode, 'Render'):
                    fileNodePath = fileNode['file'].value()
                    if (fileNodePath == ''):
                        continue
                    else:
                        readFilename = fileNodePath.split("/")[-1]
                        
                        if checkForKnob(fileNode, 'first'):

                            if (fileNodePath.endswith(tuple(videoExtension))):
                                newFilenamePath = footagePath + fileNodePath.split("/")[-1]
                                if (os.path.exists(newFilenamePath)):
                                    print (newFilenamePath + '     DUPLICATED')
                                else:
                                    if (os.path.exists(fileNodePath)):
                                        shutil.copy2(fileNodePath, newFilenamePath)
                                        print (newFilenamePath + '     COPIED')                                       
                                    else:
                                        print (newFilenamePath + '     MISSING')        

                            else:
                                # frame range
                                frameFirst = fileNode['first'].value()
                                frameLast = fileNode['last'].value()
                                framesDur = frameLast - frameFirst
                                
                                if (frameFirst == frameLast):
                                    newFilenamePath = footagePath + readFilename
                                    if (os.path.exists(newFilenamePath)):
                                        print (newFilenamePath + '     DUPLICATED')
                                    else:
                                        if (os.path.exists(fileNodePath)):
                                            shutil.copy2(fileNodePath, newFilenamePath)
                                            print (newFilenamePath + '     COPIED')                             
                                        else:
                                            print (newFilenamePath + '     MISSING')

                                else:
                                    dirSeq = fileNodePath.split("/")[-2] + '/'
                                    newFilenamePath = footagePath + dirSeq
                                    if (os.path.exists(newFilenamePath)):
                                        print (newFilenamePath + '     DUPLICATED')
                                    else:
                                        os.mkdir(newFilenamePath)
    
                                    # rename sequence
                                    for frame in range(framesDur + 1):
                                        for pad in paddings:
            
                                            # Copy sequence file
                                            if (re.search(pad, fileNodePath.split("/")[-1])):
                                                originalSeq = fileNodePath.replace(pad, str(pad % frameFirst))
                                                frameSeq = fileNodePath.split("/")[-1].replace(pad, str(pad % frameFirst))
                                                fileNames.append (frameSeq)
                                                newSeq = newFilenamePath + frameSeq
                                                frameFirst += 1
                                                # task.setMessage("Collecting file:   " + frameSeq)
        
                                                if (os.path.exists(newSeq)):
                                                    print (newSeq + '     DUPLICATED')
                                                else:
                                                    if (os.path.exists(originalSeq)):
                                                        shutil.copy(originalSeq, newSeq)
                                                        print (newSeq + '     COPIED')                      
                                                    else:
                                                        print (newSeq + '     MISSING')

                                    print ('\n')
                            
                        # Copy single file
                        else:
                            newFilenamePath = footagePath + fileNodePath.split("/")[-1]
                            if (os.path.exists(newFilenamePath)):
                                print (newFilenamePath + '     DUPLICATED')
                            else:
                                if (os.path.exists(fileNodePath)):
                                    shutil.copy2(fileNodePath, newFilenamePath)
                                    print (newFilenamePath + '     COPIED')
                                else:
                                    print (newFilenamePath + '     MISSING')

            else:
                pass

        
        if (cancelCollect == 0):
            # Save script to archive path
            newScriptPath = footagePath + scriptName
            nuke.scriptSaveAs(newScriptPath)
    
            #link files to new path
            for fileNode in nuke.allNodes():
                if checkForKnob(fileNode, 'file'):
                    if not checkForKnob(fileNode, 'Render'):
                        fileNodePath = fileNode['file'].value()
                        if (fileNodePath == ''):
                            continue
                        else:
                            
                            if checkForKnob(fileNode, 'first'):                            
                                if (fileNodePath.endswith(tuple(videoExtension))):
                                    fileNodePath = fileNode['file'].value()
                                    readFilename = fileNodePath.split("/")[-1]
                                    reloadPath = '[file dirname [value root.name]]/footage/' + readFilename
                                    fileNode['file'].setValue(reloadPath)
                                else:
                                    # frame range
                                    frameFirst = fileNode['first'].value()
                                    frameLast = fileNode['last'].value()
        
                                    if (frameFirst == frameLast):
                                        fileNodePath = fileNode['file'].value()
                                        readFilename = fileNodePath.split("/")[-1]
                                        reloadPath = '[file dirname [value root.name]]/footage/' + readFilename
                                        fileNode['file'].setValue(reloadPath)
                                    else:
                                        fileNodePath = fileNode['file'].value()
                                        dirSeq = fileNodePath.split("/")[-2] + '/'
                                        readFilename = fileNodePath.split("/")[-1]
                                        reloadPath = '[file dirname [value root.name]]/footage/' + dirSeq + readFilename
                                        fileNode['file'].setValue(reloadPath)
                            
                            else:
                                fileNodePath = fileNode['file'].value()
                                readFilename = fileNodePath.split("/")[-1]
                                reloadPath = '[file dirname [value root.name]]/footage/' + readFilename
                                fileNode['file'].setValue(reloadPath)
                    else:
                        pass
                else:
                    pass
    
            nuke.scriptSave()
            # del task        
            print ('COLLECT DONE!!')
            # nuke.message('COLLECT DONE!!')
        
def writeLog(log, data):
    if log:
        with open(log, 'w') as f:
            json.dump(data, f, sort_keys=True, indent=4, separators=(',', ':'))


if __name__ == "__main__":
    print ':::::: batch Collect nuke ::::::'
    logger.info(':::::: batch Collect nuke ::::::')

    parser = setup_parser('batch Collect nuke')
    params = parser.parse_args()
    main(nuke_path=params.nuke_path)

'''
"C:/Program Files/Nuke11.3v4/python.exe" "D:/pipe_script/core/rf_nuke/script/batch_collect_nuke.py" -s "Z:/NPC/post/02_compositing/ep06/ep06_0010/ver/ep06_0010_comp_v01.nk"
'''
