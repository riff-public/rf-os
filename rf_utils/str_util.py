import os
import sys

class CustomStr(object):
	"""docstring for CustomStr"""
	def __init__(self, input):
		super(CustomStr, self).__init__()
		self.input = str(input)

	def __repr__(self):
		return self.input

	def __str__(self):
		return self.input

	def basename(self):
		return os.path.basename(self.input)

	def dirname(self):
		return os.path.dirname(self.input)

	def find_version(self, prefix='v', padding=3):
		elems = self.input.split(prefix)
		version = [a[0:padding] for a in elems if a[0:padding].isdigit()]
		return '%s%s' % (prefix, version[0]) if version else ''

	def ext(self):
		return self.name.split('.')[-1] if '.' in self.name else ''
