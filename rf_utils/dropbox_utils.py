import os
import dropbox

TOKEN = 'Rh_JeDynewAAAAAAAAEJYv17Cyol2EMv1Jh5DxhT4iGJ2RX9GFOnQuQDQucAoXhJ'
dbx = dropbox.Dropbox(TOKEN)

def list_folder(path):
    ''' List what's in a folder. Return a dict mapping unicode filenames 
        to FileMetadata or FolderMetadata entries.
    '''
    try:
        res = dbx.files_list_folder(path)
    except dropbox.exceptions.ApiError as err:
        print('Folder listing failed for', path, '-- assumed empty:', err)
        return {}
    else:
        rv = {}
        for entry in res.entries:
            rv[entry.name] = entry
        return rv

def upload(local_path, dropbox_path, chunk_size=10):
    ''' Upload a file to Dropbox '''
    f = open(local_path, 'rb')
    file_size = os.path.getsize(local_path)

    res = None
    CHUNK_SIZE = chunk_size * 1024 * 1024  # chunk size in MB

    # if the file is smaller than chunk size, just use files_upload()
    if file_size <= CHUNK_SIZE:
        res = dbx.files_upload(f.read(), dropbox_path)
    else:
        # else needs to start an upload session
        upload_session_start_result = dbx.files_upload_session_start(f.read(CHUNK_SIZE))
        cursor = dropbox.files.UploadSessionCursor(session_id=upload_session_start_result.session_id,
                                                   offset=f.tell())
        commit = dropbox.files.CommitInfo(path=dropbox_path)

        while f.tell() < file_size:
            if ((file_size - f.tell()) <= CHUNK_SIZE):
                res = dbx.files_upload_session_finish(f.read(CHUNK_SIZE),
                                                cursor,
                                                commit)
            else:
                dbx.files_upload_session_append(f.read(CHUNK_SIZE),
                                                cursor.session_id,
                                                cursor.offset)
                cursor.offset = f.tell()
    f.close()
    return res

def download(dropbox_path, local_path):
    ''' Download a file from Dropbox to local destination '''
    res = None
    try:
        res = dbx.files_download_to_file(local_path, dropbox_path)
    except dropbox.exceptions.ApiError as err:
        print('*** API error', err)

    return res