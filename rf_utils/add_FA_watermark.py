import os
import sys
sys.path.append('{}/core'.format(os.environ['RFSCRIPT']))

import tempfile
import shutil
from rf_utils.pipeline import convert_lib

WATERMARK_PATH = '{}/core/rf_template/default/watermark/watermark_square_2k_FA.png'.format(os.environ['RFSCRIPT'])
print os.path.exists(WATERMARK_PATH)
OPACITY = 0.334

def main(medias):
    print '===== Media to process ====='
    num_media = len(medias)
    desktop_dir = os.path.join(os.environ['HOMEDRIVE'], os.environ["HOMEPATH"], "Desktop")
    for i, input_path in enumerate(medias):
        print 'Processing media...(%s/%s)' %(i+1, num_media)
        print '  {}'.format(input_path)
        SUPPORTED_EXT = ('.mov', '.mp4', '.jpg', '.png', '.tif')

        input_fn, input_ext = os.path.splitext(input_path)
        input_ext = input_ext.lower()
        basename = os.path.basename(input_path)
        if input_ext not in SUPPORTED_EXT:
            print '  **Unsupported media format: %s' %input_ext
            continue

        # create temp mov
        out_fh, out_temp = tempfile.mkstemp(suffix=input_ext)
        print '  Temp media created: {out_temp}'.format(out_temp=out_temp)
        
        # add watermark using ffmper
        convert_output = convert_lib.overlay_image(input_path=input_path, 
                    overlay_image=WATERMARK_PATH, 
                    output_path=out_temp, 
                    opacity=OPACITY)
        os.close(out_fh)

        # copy to desktop
        fn, ext = os.path.splitext(basename)
        desktop_path = os.path.join(desktop_dir, '{}_watermark{}'.format(fn, ext))

        shutil.copyfile(out_temp, desktop_path)
        try:
            os.remove(out_temp)
            print '  Copy success: {}'.format(desktop_path)
        except:
            print '  Cannot remove temp:{}'.format(out_temp)

        print '  Result path: %s' %(desktop_path)


if __name__ == '__main__':
    medias = sys.argv[1:]
    main(medias)