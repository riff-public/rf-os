import maya.cmds as mc
import json
from rf_utils import file_utils
import os
import shutil

def get_path():
    path = mc.file(sn=True, q=True).replace('\\', '/')
    data_path = os.path.join(os.path.dirname(path), 'data').replace('\\', '/')
    if not os.path.exists(data_path):
        os.makedirs(data_path)
    renderLayer_path = os.path.join(data_path, 'renderLayer.txt')
    renderOverride_path = os.path.join(data_path, 'renderOverride.txt')
    renderAttr_path = os.path.join(data_path, 'renderAttr.txt')
    renderShade_path = os.path.join(data_path, 'renderShade.txt')
    return renderLayer_path, renderOverride_path, renderAttr_path, renderShade_path

def backup_file(path):
    backup_path = os.path.join(os.path.dirname(path), 'backup').replace('\\', '/')
    if not os.path.exists(backup_path):
        os.makedirs(backup_path)
    name = os.path.basename(path)
    new_path = os.path.join(backup_path, name)
    shutil.copyfile(path, new_path)

def get_renderLayer():
    dictLayer = {}
    #dictConnect = {}
    dictAttr = {}
    dictValue = {}
    dictOverride = {}
    shadeList = {}

    allRenderLayer = mc.ls(type='renderLayer')
    # connectionList = []
    if allRenderLayer:
        for layer in allRenderLayer:
            if not 'defaultRenderLayer' in layer:
                mc.editRenderLayerGlobals( currentRenderLayer=layer )
                layerChild = mc.editRenderLayerMembers( layer, query=True)
                shadeList[layer] = {}
                for obj in layerChild:
                    mc.select(obj, hierarchy=True)
                    theNodes = mc.ls(sl = True, dag = True, s = True)
                    for ix in theNodes:
                        shadeEng = mc.listConnections(ix , type = "shadingEngine")
                        if shadeEng:
                            materials = mc.ls(mc.listConnections(shadeEng ), materials = True)
                            if not ix in shadeList:
                                shadeList[layer][ix] = shadeEng
                            else:
                                shadeList[layer][ix] = shadeEng
                        else:
                            shadeList[layer][ix] = []

                check_shape(layerChild)
                dictLayer[layer] = layerChild

                attrs = mc.editRenderLayerAdjustment( layer, query=True, layer=True )
                dictOverride[layer] = attrs 

                connectAttrS = mc.listConnections(layer, p=True , s=True , d=False)
                # connectAttrD = mc.listConnections(layer, p=True , s=False , d=True)
                # connectionList.append(connectAttrS)
                # connectionList.append(connectAttrD)
                # dictConnect[layer] = connectionList
                # connectionList = []
                for attr in connectAttrS:
                    if attr:
                        types = mc.getAttr(attr, type=True)
                        if 'objectGroups' in attr:
                            continue
                        elif types == 'TdataCompound':
                            continue
                        elif types == 'short':
                            continue
                        elif 'surfaceShade' in attr:
                            value = mc.listConnections(attr, d=False ,s=True,p=True)
                            dictValue[attr] = value
                        elif types == 'float3':
                            continue
                        # elif 'shader' in attr:
                        #     continue
                        # elif 'Shader' in attr:
                        #     continue
                        # elif 'Color' in attr:
                        #     continue
                        # elif 'color' in attr:
                        #     continue
                        else:
                            value = mc.getAttr(attr)
                        dictValue[attr] = value

                dictAttr[layer] = dictValue
                dictValue = {}
    renderLayer_path, renderOverride_path, renderAttr_path, renderShade_path = get_path()
    backup_file(renderLayer_path)
    with open(renderLayer_path,"w") as file:
        json.dump(dictLayer, file)
    
    backup_file(renderOverride_path)
    with open(renderOverride_path,"w") as file:
         json.dump(dictOverride, file)
    
    backup_file(renderAttr_path)
    with open(renderAttr_path,"w") as file:
        json.dump(dictAttr, file)
    
    backup_file(renderShade_path)
    with open(renderShade_path,"w") as file:
        json.dump(shadeList, file)

def create_renderLayer():
    renderLayer_path, renderOverride_path, renderAttr_path, renderShade_path = get_path()
    if renderLayer_path and renderOverride_path and renderAttr_path and renderShade_path:
        with open(renderLayer_path,"r") as file:
            dataLayer = json.load(file)
        with open(renderOverride_path,"r") as file:
            dataOverride = json.load(file)
        with open(renderAttr_path,"r") as file:
            dataAttr = json.load(file)
        with open(renderShade_path,"r") as file:
            dataShade = json.load(file)

        mc.select(cl=True)
        mc.editRenderLayerGlobals( currentRenderLayer = 'defaultRenderLayer')
        for layer, objList in dataLayer.iteritems():
            if mc.objExists(layer):
                mc.delete(layer)
            mc.createRenderLayer(name = layer)
            for obj in objList:
                mc.editRenderLayerMembers(layer, obj, noRecurse=True)

        for layer, ovrList in dataOverride.iteritems():
            for ovr in ovrList:
                mc.editRenderLayerAdjustment( ovr, layer=layer )

        for layer, dictAttr in dataAttr.iteritems():
            for attr,value in dictAttr.iteritems():
                mc.editRenderLayerGlobals( currentRenderLayer=layer )
                types = mc.getAttr(attr, type=True)
                if types == 'double3':
                    setAttrXYZ(attr, value)
                elif 'surfaceShade' in attr:
                    value2 = mc.listConnections(attr, d=False ,s=True,p=True)
                    for v2 in value2:
                        mc.disconnectAttr (v2, attr)
                    for v in value:
                        mc.connectAttr (v, attr)
                else:
                    mc.setAttr(attr, value)
        create_shade(dataShade)

def create_shade(shadeList):
    mc.select(cl=True)
    mc.editRenderLayerGlobals( currentRenderLayer = 'defaultRenderLayer')
    for layer, objList in shadeList.iteritems():
        mc.editRenderLayerGlobals( currentRenderLayer = layer)
        # print layer
        # print objList
        for obj, shade in objList.iteritems():
            if shade:
                mc.sets(obj,forceElement=shade[0], e=True)

    # for layer, connectionList in dataConnect.iteritems():
    #   mc.editRenderLayerGlobals( currentRenderLayer = layer)

def check_shape(list_obj):
    if list_obj:
        for ix in list_obj:
            if mc.nodeType(ix) == 'mesh':
                list_obj.remove(ix)

def setAttrXYZ(attr, values):
    vector = ("X","Y","Z")
    if 'shear' in attr:
        vector = ("XY","XZ","YZ")
    for channel, value in zip(vector, values[0]):
        mc.setAttr("%s%s" %(attr,channel), value)
