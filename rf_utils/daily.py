import os
import sys
import logging
from datetime import datetime
import shutil
from rf_utils import admin

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

def submit(ContextPathInfo, files, subfolder=''):
	dailyDir = get_path(ContextPathInfo)
	overall = []

	if subfolder: 
		dailyDir = '{}/{}'.format(dailyDir, subfolder)

	if not os.path.exists(dailyDir):
		os.makedirs(dailyDir)

	for filepath in files:
		dst = ('/').join([dailyDir, os.path.basename(filepath)])

		# shutil.copy2(filepath, dst)
		admin.copyfile(filepath, dst)
		result = True if os.path.exists(dst) else False
		if result:
			overall.append(dst)


	submitResult = True if len(files) == len(overall) else False
	message = 'submit daily success' if submitResult else 'submit failed'
	logger.info(message)
	return submitResult
	

def get_path(ContextPathInfo):
	entity = ContextPathInfo
	rootDailyDir = entity.path.daily().abs_path()
	step = entity.step
	date = '%s' % datetime.now().strftime("%Y_%m_%d")
	dailyDir = ('/').join([rootDailyDir, step, date])

	return dailyDir

