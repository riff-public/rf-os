import os 
import sys 

class PipelineError(Exception):
    def __init__(self, message):
        super(PipelineError, self).__init__(message)
        self.message = message