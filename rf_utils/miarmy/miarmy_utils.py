import sys
import os 
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

miarmyPath = 'C:/Program Files/Basefount/Miarmy/Maya2018/scripts'
sys.path.append(miarmyPath) if not miarmyPath in sys.path else None

import maya.cmds as mc
import maya.mel as mel
import pymel.mayautils as mu
import pymel.core as pm 
import maya.OpenMaya as om

try:
    import McdRender
    import McdGeneral
    import McdRenderRSFunctions
    import McdPlacementFunctions
    import McdSimpleCmd
    import McdBatchAgentCacheDoIt
    from mcdPipelineCommands import McdSetAgentRealIDAfterDriveShellWrapper
    import performMD2CachePreCheck
except:
    pass

import pipeline_setup
reload(pipeline_setup)
from rf_maya.rftool.utils import abc_utils
reload(abc_utils)
from rf_utils.context import context_info
reload(context_info)

miarmyContent = 'Miarmy_Contents'
agentLogo = 'Agent_loco'

groupNodeType = {'McdDecision': 'SetDecision'}

def load_miarmy(): 
    plugin = 'MiarmyExpress.mll'
    try: 
        if not mc.pluginInfo(plugin, q=True, l=True):
            mc.loadPlugin(plugin, qt=True)
    except: 
        pass 

def cache_redshift(path, file_name, start, end): 
    mc.loadPlugin( 'MiarmyPro.mll' )

    if not os.path.exists(path): 
        os.makedirs(path)
    globalNode = McdGeneral.McdGetMcdGlobalNode()
    mc.setAttr(globalNode + ".outputRSFolder", path, type='string') #set
    mc.setAttr(globalNode + ".outputRSName", file_name, type='string') #set

    currentRsNode = list_rsproxy()
    # check placement:
    if not McdRender.McdCheckAgentPlace():
        McdPlacementFunctions.placementAgent()

    McdRenderRSFunctions.McdRSCheckAndExportOA()

    nbFrame = int(end - start + 1)
    rtd = McdGeneral.McdTurnOffRTDTemp()
    for i in range(nbFrame):
        frameNumberNum = start + i
        mc.currentTime(frameNumberNum)
        mel.eval("McdRenderExportRedshiftCmd -em 1;")

    if rtd:
        McdGeneral.McdTurnOnRTDTemp()
    

    McdRenderRSFunctions.createRSInfoNode()
    
    if not currentRsNode: 
        newNodes = [a for a in list_rsproxy() if not a in currentRsNode]
    else: 
        newNodes = list_rsproxy()
    return newNodes

def cache_agent(entity=None): 
    # setup path 
    pipeline_setup.setup_cache_path(entity)
    pipeline_setup.set_cache_name(entity)
    
    # cache 
    mc.setAttr('%s.enableCache' % pipeline_setup.miarmyGlobal, 0)
    McdBatchAgentCacheDoIt.McdMakeAgentCacheBatch()
    mc.setAttr('%s.enableCache' % pipeline_setup.miarmyGlobal, 1)

def cache_mesh_drive(entity=None): 
    # setup path 
    pipeline_setup.setup_meshdrive_path(entity)
    pipeline_setup.set_meshdrive_name(entity)
    # cache_mesh_drive_cmd()

def cache_mesh_drive_cmd(path='', file_name='', start=None, end=None):

    # McdSetAgentRealIDAfterDriveShellWrapper()
    # performMD2CachePreCheck()
    globalNode = McdGeneral.McdGetMcdGlobalNode()
    if not path: 
        mc.setAttr(globalNode + ".outMD2Folder", path, type='string') #set
    if not file_name: 
        mc.setAttr(globalNode + ".outMD2Name", file_name, type='string') #set
    # export agent type list:
    mel.eval("McdMakeJointCacheCmd -actionMode 0;")

    # batch make cache: --------------------------------------------------------
    startFrame = cmds.playbackOptions(q=True, min=True) if not start else start 
    endFrame = cmds.playbackOptions(q=True, max=True) if not end else end 
    
    brainNode = mel.eval("McdSimpleCommand -execute 3")
    mc.setAttr(brainNode + ".startTime", start)
    solverFrame = mc.getAttr(brainNode + ".startTime")
    solverFrame = float(int(solverFrame))
    solverFrame -= 1
    if solverFrame > startFrame:
        solverFrame = startFrame
        
    mc.currentTime(solverFrame-1)
    mc.currentTime(solverFrame)

    counter = 0
    totalCount = endFrame - startFrame
    
    # from solverFrame to endFrame:
    while(solverFrame <= endFrame):
        
        if solverFrame >= startFrame:
            counter += 1
            mc.currentTime(solverFrame)
            # deal with batch cache
            mel.eval("McdMakeJointCacheCmd -actionMode 1;") 
            
        solverFrame += 1
        
        ## progress operation: ////////////////////////////////////////////////
        
    

def disconnect_miarmy_content(): 
    connectedAttrs = mc.listConnections(miarmyContent, p=True, c=True, d=True)

    if connectedAttrs: 
        for i in range(0, len(connectedAttrs), 2): 
            src = connectedAttrs[i]
            dst = connectedAttrs[i+1]
            mc.disconnectAttr(src, dst)
            logger.debug('disconnected %s %s' % (src, dst))
    else: 
        logger.warning('No connection found')

def miarmy_oa_namespace(entity): 
    suffix = entity.projectInfo.asset.name_suffix('miarmyAgent')
    namespace = '%s_%s' % (entity.name, suffix)
    return namespace

def miarmy_ready(agentLoco=True): 
    if not mc.objExists(miarmyContent): 
        McdSimpleCmd.McdSelectMcdGlobal(0)
        gblNode = mc.ls(type='McdGlobal')[0]
        mc.setAttr(gblNode+'.enableScale',1)

    if not agentLoco: 
        if mc.objExists(agentLogo): 
            mc.delete(agentLogo)

def parent_agent_logo(agent_grp): 
    if not mc.listRelatives(agent_grp, p=True): 
        mc.parent(agent_grp, miarmyContent)

def find_agent_loco(): 
    oas = mc.ls(type='McdAgentGroup')
    return oas[0] if oas else None

def list_agents(): 
    oas = mc.ls(type='McdAgentGroup')
    return oas

def list_contents(): 
    contents = mc.ls(type='SetMiarmyContents')
    return contents

def list_global(): 
    glob = mc.ls(type='McdGlobal')
    return glob

def list_brain(): 
    brain = mc.ls(type='McdBrain')
    return brain

def list_brain_post(): 
    brainpost = mc.ls(type='McdBrainPost')
    return brainpost

def list_actions(): 
    actions = mc.ls(type='McdAction')
    return actions 

def list_rsproxy(): 
    nodes = mc.ls(type='McdRSProxy')
    return nodes

def get_action_name(actionNode): 
    """ get first element as action name """
    return actionNode.split('_')[0]

def clear_miarmy_content_type(agentName, nodeType): 
    nodeGroup = find_content_type(agentName, nodeType)
    contents = mc.listRelatives(nodeGroup, c=True) 

    if mc.referenceQuery(nodeGroup, inr=True): 
        parentRef = mc.referenceQuery(nodeGroup, f=True)

    for content in contents: 
        if mc.referenceQuery(content, inr=True): 
            refPath = mc.referenceQuery(content, f=True)
            if not refPath == parentRef: 
                rnNode = mc.referenceQuery(refPath, referenceNode=True)
                # mc.file(refPath, unloadReference=True)
                mc.file(refPath, unloadReference=rnNode)
            else: 
                logger.warning('Content "%s" has the same ref as parent. Cannot remove.' % content)
        else: 
            mc.delete(content)

def find_content_type(agentName, nodeType): 
    groups = mc.ls(type=nodeType)

    for group in groups: 
        parents = mc.listRelatives(group, p=True)
        if agentName in parents: 
            return group


def prep_cache(): 
    brainNodes = mc.ls(type='McdBrain')
    if brainNodes: 
        # set timeline to match brain startFrame
        startTime = mc.getAttr('%s.startTime' % brainNodes[0])
        mc.playbackOptions(min=startTime)
        # place agent 
        McdPlacementFunctions.placementAgent()

        # activate cache
        mu.executeDeferred(mc.currentTime, startTime+1)
        mu.executeDeferred(mc.currentTime, startTime)

def agent_loco_name(entity): 
    return '%s_Loco' % entity.name 

def remove_miarmy_content(): 
    mcdNodes = mc.ls(type='McdBrain') + mc.ls(type='McdBrainPost') + mc.ls(type='McdGlobal')
    mcdNodes.append(miarmyContent)
    mc.delete(mcdNodes)


def create_decision(agentName, name): 
    nodeName = 'McdDecision'
    prefixAgent = agentName.split(':')[0]
    fullname = '%s_decision_%s' % (name, prefixAgent)
    agentNamePm = pm.PyNode(agentName)
    decs = pm.createNode(nodeName, n=fullname)
    groupType = groupNodeType[nodeName]
    grp = find_group_type(agentName, groupType)
    pm.parent(decs, grp)
    return decs
    

def find_group_type(agentName, groupType): 
    targetGrps = pm.ls(type=groupType)
    agentNamePm = pm.PyNode(agentName)

    for grp in targetGrps: 
        parent = grp.getParent()
        if parent: 
            if agentNamePm == parent: 
                return grp

def create_default_action(agentName, actionName): 
    name = 'default'
    decision = create_decision(agentName, name)
    mc.setAttr('%s.defaultAction' % decision, actionName, type='string')
    mc.setAttr('%s.default' % decision, 1)

def export_miarmy_decision(obj=None, path=''):
    from Qt import QtWidgets
    if not obj:
        # get the path and selection
        sels = pm.selected()
        if not sels:
            om.MGlobal.displayError('Select an angent to export decision')
            return

    if not path:
        lib_dir = 'P:/Library/crowd/miarmy/decision'
        dialog = QtWidgets.QFileDialog()
        dialog.setWindowTitle('Export to...')
        dialog.setNameFilter('*.ma')
        dialog.setDefaultSuffix('ma')
        dialog.setDirectory(lib_dir)
        dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            path = dialog.selectedFiles()[0]
        else:
            return

    obj = sels[0]

    # get agent group
    agentGroups = [node for node in obj.getAllParents() if node.nodeType() == 'McdAgentGroup']
    if len(agentGroups) != 1:
        om.MGlobal.displayError('Cannot find agent group from: %s' %obj)
        return

    # find set decision
    setDecisions = [node for node in mc.listRelatives(agentGroups[0].shortName(), ad=True, type='SetDecision')]
    if len(setDecisions) != 1:
        om.MGlobal.displayError('Cannot find set decision from: %s' %agentGroups[0])
        return

    # get decisions under set decision
    setDecision = pm.PyNode(setDecisions[0])
    decisions = [node for node in setDecision.getChildren(type='McdDecision')]
    if len(decisions) < 1:
        om.MGlobal.displayError('Cannot find decision from: %s' %setDecision)
        return

    print path, decisions
    pm.parent(decisions, w=True)
    pm.select(decisions, r=True)
    pm.exportSelected(path)
    pm.parent(decisions, setDecision)

def import_miarmy_decision(objs=[], path=''):
    from Qt import QtWidgets
    if not objs:
        # get the path and selection
        sels = pm.selected()
        if not sels:
            om.MGlobal.displayError('Select an angent to export decision')
            return

    if not path:
        lib_dir = 'P:/Library/crowd/miarmy/decision'
        dialog = QtWidgets.QFileDialog()
        dialog.setWindowTitle('Import from...')
        dialog.setNameFilter('*.ma')
        dialog.setDefaultSuffix('ma')
        dialog.setDirectory(lib_dir)
        dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptOpen)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            path = dialog.selectedFiles()[0]
        else:
            return

    objs = sels
    allSetDecisions = {}
    for obj in objs:
        # get agent group
        agentGroups = [node for node in obj.getAllParents() if node.nodeType() == 'McdAgentGroup']
        if len(agentGroups) != 1:
            om.MGlobal.displayError('Cannot find agent group from: %s' %obj)
            continue

        # find set decision
        setDecisions = [node for node in mc.listRelatives(agentGroups[0].shortName(), ad=True, type='SetDecision')]
        if len(setDecisions) != 1:
            om.MGlobal.displayError('Cannot find set decision from: %s' %agentGroups[0])
            continue

        sdNode = pm.PyNode(setDecisions[0])
        ns = sdNode.namespace()
        name = ''
        if ns:
            name = ns[:-1]

        if name and name not in allSetDecisions:
            name_txt = '_%s' %name
            allSetDecisions[name_txt] = sdNode

    for name, sd in allSetDecisions.iteritems():
        nodes = [n for n in pm.importFile(path, rnn=True) if n.nodeType() == 'McdDecision']
        if nodes:
            for node in nodes:
                # parent it
                if not node.getParent():
                    pm.parent(node, sd)
                # rename it
                namesplits = node.nodeName().split('_')
                elem = namesplits[0]
                new_name = '%s_decision%s' %(elem, name)
                node.rename(new_name)

def do_export_abc_from_agents(abcPath, agents=[]):
    import McdBatchMDCacheDoIt
    import McdBatchMeshDriveDoIt
    import McdMeshDriveSetup
    import McdMeshDriveSetup

    # get mcd globals
    mcdGlobals = pm.ls(type='McdGlobal')
    if not mcdGlobals:
        om.MGlobal.displayError('Cannot find McdGlobal')
        return
    mcdGlobal = mcdGlobals[0]

    # place agents
    if not McdRender.McdCheckAgentPlace():
        McdPlacementFunctions.placementAgent()

    if not agents:
        sels = [n for n in pm.selected(type='transform') if n.getShape() and n.getShape().nodeType()=='McdAgent']
        if not sels:
            # get all agents
            agents = [n.getParent() for n in pm.ls(type='McdAgent')]
            if not agents:
                return
        else:
            agents = sels
    # get the ID
    agentNames = []
    for ag in agents:
        try:
            ag = pm.PyNode(ag)
        except Exception, e:
            print e
            print 'error: %s' %ag
            continue
        agentNames.append(ag)
    agents = agentNames
    agentIds = [n.getShape().agentId.get() for n in agents]

    # check for mesh drive cache output dir
    outMdDir = mcdGlobal.outMD2Folder.get()
    if not os.path.exists(outMdDir):
        om.MGlobal.displayError('Output meshDrive cache directory does not exists: %s' %outMdDir)
        return
    outMD2Name = mcdGlobal.outMD2Name.get()
    if not outMD2Name:
        om.MGlobal.displayError('No meshDrive cache name set!')
        return

    # clear old meshdrive first
    McdMeshDriveSetup.McdMeshDrive2Clear()

    # create mesh drive cache
    McdBatchMDCacheDoIt.McdBatchMDCacheDoIt()
    # deplace agents
    McdPlacementFunctions.dePlacementAgent()
    # create mesh drive
    McdBatchMeshDriveDoIt.McdBatchMeshDriveDoIt()
    McdMeshDriveSetup.McdCreateMeshDriveIMNode(1, 1)

    # get meshdrive group
    mdGrp = None
    try:
        mdGrp = pm.PyNode('|MDGGRPMASTER')
    except:
        om.MGlobal.displayError('Cannot find MDGGRPMASTER!')
        return

    # get mesh drive to export alembic cache
    mdChars = mdGrp.getChildren(type='transform')
    toCache = []
    for char in mdChars:
        mdId = char.agentId.get() + 1
        if mdId in agentIds:
            toCache.append(char.shortName())

    # export abc
    start = mc.playbackOptions(q=True, min=True)
    end = mc.playbackOptions(q=True, max=True)
    result = abc_utils.export_abc(obj=toCache, path=abcPath, start=start, end=end)
    return result

def export_abc_from_agents(entity=None):
    from Qt import QtWidgets
    from rf_app.publish.scene import batch
    reload(batch)
    if not entity:
        entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
    filePath = entity.path.scheme(key='work').abs_path()

    # --- check if scene is modified then warn user to save
    if mc.file(q=True, mf=True):
        # pop up to ask for the abc element name
        qmsgBox = QtWidgets.QMessageBox()
        qmsgBox.setWindowTitle('Export ABC from agents')
        qmsgBox.setText('Sending a farm job,\nbut your scene is modified, save changes?')
        qmsgBox.setIcon(QtWidgets.QMessageBox.Warning)

        buttons = (('Save', QtWidgets.QMessageBox.AcceptRole), 
                ('Do not save', QtWidgets.QMessageBox.AcceptRole), 
                ('Cancle', QtWidgets.QMessageBox.RejectRole))
        for text, role in buttons:
            qmsgBox.addButton(text, role)
        result = qmsgBox.exec_()
        if result == 0: 
            pm.saveFile()
        elif result == 2:
            return

    # --- get user naming
    elem, ok = QtWidgets.QInputDialog.getText(None,
            'Export ABC from agents', 
            'Enter name for crowd ABC:', 
            QtWidgets.QLineEdit.Normal,
            'crowdMesh')
    if not elem or not ok:
        return

    # setup mesh drive path 
    pipeline_setup.setup_meshdrive_path(entity)
    pipeline_setup.set_meshdrive_name(entity)

    # --- get abc path
    abcPath = pipeline_setup.get_abc_path(entity=entity, elem=elem)

    # --- get selection
    sels = [n for n in pm.selected(type='transform') if n.getShape() and n.getShape().nodeType()=='McdAgent']
    if not sels:
        # get all agents
        agents = [n.getParent().shortName() for n in pm.ls(type='McdAgent')]
        if not agents:
            om.MGlobal.displayError('No selection found!')
            return
    else:
        agents = [n.shortName() for n in sels]

    # print 'DO IT!'
    batch.submit_miarmy_meshdrive(scene=filePath, destinationPath=abcPath, agents=agents)

load_miarmy()
