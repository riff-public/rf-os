import os 
import sys 
import logging 
from rf_utils.sg import sg_process 
from rf_utils.context import context_info

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

sg = sg_process.sg

try: 
	import maya.cmds as mc 
	import maya.mel as mm 
	isMaya = True 
	import McdBatchAgentCacheDoIt
	import McdMeshDriveSetup

except ImportError: 
	isMaya = False
	logger.warning('Failed to import maya') 

miarmyDir = 'miarmy'
miarmyGlobal = 'McdGlobal1'


def setup_cache_path(entity=None): 
	if not entity: 
		entity = context_info.ContextPathInfo()
	# cache workspace 
	cacheDir = '%s/%s/oa' % (entity.path.scheme(key='cacheLocalPath').abs_path(), miarmyDir)

	if not os.path.exists(cacheDir): 
		os.makedirs(cacheDir)
	# set path 
	mc.setAttr('%s.cacheFolder' % miarmyGlobal, cacheDir, type='string')


def set_cache_name(entity=None): 
	if not entity: 
		entity = context_info.ContextPathInfo()
	cacheName = '%s_oaCache' % entity.name
	mc.setAttr('%s.cacheName' % miarmyGlobal, cacheName, type='string')


def setup_meshdrive_path(entity=None): 
	if not entity: 
		entity = context_info.ContextPathInfo()
	# cache workspace 
	cacheDir = '%s/%s/meshDrive' % (entity.path.scheme(key='cacheLocalPath').abs_path(), miarmyDir)

	if not os.path.exists(cacheDir): 
		os.makedirs(cacheDir)
	# set path 
	mc.setAttr('%s.outMD2Folder' % miarmyGlobal, cacheDir, type='string')

def get_abc_path(entity=None, elem='crowdMesh'): 
	if not entity: 
		entity = context_info.ContextPathInfo()
	# cache workspace 
	cacheDir = '%s/%s/abc' % (entity.path.scheme(key='cacheLocalPath').abs_path(), miarmyDir)

	if not os.path.exists(cacheDir): 
		os.makedirs(cacheDir)

	fn = '%s_%s_%s.abc' %(entity.name, entity.step, elem)
	full_path = '%s/%s' %(cacheDir, fn)
	return full_path

def set_meshdrive_name(entity=None): 
	if not entity: 
		entity = context_info.ContextPathInfo()
	cacheName = '%s_md' % entity.name
	mc.setAttr('%s.outMD2Name' % miarmyGlobal, cacheName, type='string')


def cache_agent(entity=None): 
	if not entity: 
		entity = context_info.ContextPathInfo()

	# setup path 
	setup_cache_path(entity)
	set_cache_name(entity)
	
	# cache 
	mc.setAttr('%s.enableCache' % miarmyGlobal, 0)
	McdBatchAgentCacheDoIt.McdMakeAgentCacheBatch()
	mc.setAttr('%s.enableCache' % miarmyGlobal, 1)



def cache_meshdrive(entity=None): 
	if not entity: 
		entity = context_info.ContextPathInfo()

	# setup path 
	setup_meshdrive_path(entity)
	set_meshdrive_name(entity)
	McdMeshDriveSetup.McdExportMD2Cache()
