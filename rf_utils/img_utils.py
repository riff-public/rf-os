import cv2
import numpy as np
import os 
import sys


def overlay_image(background, overlay):
    # Get the dimensions of the background image
    bg_height, bg_width = background.shape[:2]

    # Resize the overlay image to match the dimensions of the background image
    overlay = expand_canvas(overlay, bg_width, bg_height)

    # Split out the alpha channel from the overlay image
    overlay_alpha = overlay[:, :, 3]

    # Split out the RGB channels from the overlay image
    overlay_rgb = overlay[:, :, :3]

    # Create a mask for the overlay image
    mask = cv2.merge((overlay_alpha, overlay_alpha, overlay_alpha))

    # Invert the mask
    mask_inv = cv2.bitwise_not(mask)

    # Apply the mask to the overlay image
    overlay_masked = cv2.bitwise_and(overlay_rgb, mask)

    # Apply the inverted mask to the background image
    background_masked = cv2.bitwise_and(background, mask_inv)

    # Combine the masked overlay and masked background images
    result = cv2.add(background_masked, overlay_masked)

    return result


def expand_canvas(img, new_width, new_height):
    # Load the original image with transparency
    # img = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)

    # Get the dimensions of the original image
    h, w = img.shape[:2]
    has_alpha = img.shape[2] == 4

    if not has_alpha: 
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGBA)

    # resize if input larger than expand canvas setting 
    if h > new_height or w > new_width: 
        diff_h = h - new_height
        diff_w = w - new_width

        resize_w = new_width
        resize_h = new_height

        if diff_h > diff_w: 
            resize_h = new_height
            resize_w = (new_height / h) * w

        elif diff_w > diff_h: 
            resize_w = new_width
            resize_h = (new_width / w) * h 

        img = resize_image(img, resize_w=resize_w, resize_h=resize_h)
        h, w = img.shape[:2]

    # Create a new transparent canvas with the specified size
    new_img = np.zeros((new_height, new_width, 4), dtype=np.uint8)

    # Calculate the coordinates of the top-left corner of the original image
    x, y = (new_width - w) // 2, (new_height - h) // 2

    # Insert the original image into the center of the new canvas
    new_img[y:y+h, x:x+w] = img

    return new_img


def resize_image(img, scale=1, resize_w=0, resize_h=0): 
    # Get the dimensions of the original image
    h, w = img.shape[:2]

    # Calculate the new dimensions
    if scale: 
        new_h, new_w = int(h*scale), int(w*scale)
    if resize_w > 0 and resize_h > 0: 
        new_h, new_w = int(resize_h), int(resize_w)

    # Resize the image using resize() function
    resized_img = cv2.resize(img, (new_w, new_h))
    return resized_img


def resize_from_file(image_path, dst_path, scale=1, resize_w=0, resize_h=0): 
    img = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)
    resize_img = resize_image(img, scale=scale, resize_w=resize_w, resize_h=resize_h)
    return save_to_file(resize_img, dst_path)


def get_image_size(image_path): 
    img = cv2.imread(image_path)
    h, w = img.shape[:2]
    return w, h


def expand_png_canvas(image_path, new_width, new_height): 
    img = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)
    return expand_canvas(img, new_width, new_height)


def save_to_file(img, dst): 
    if not os.path.exists(os.path.dirname(dst)): 
        os.makedirs(os.path.dirname(dst))
    return cv2.imwrite(dst, img)


def merge_images(image1_path, image2_path):
    # Load the first image with transparency
    img1 = cv2.imread(image1_path, cv2.IMREAD_UNCHANGED)
    h1, w1 = img1.shape[:2]

    # Load the second image with transparency
    img2 = cv2.imread(image2_path, cv2.IMREAD_UNCHANGED)
    h2, w2 = img2.shape[:2]

    # Calculate the dimensions of the output image
    new_h = max(h1, h2)
    new_w = max(w1, w2)

    # Create a new transparent canvas with the specified size
    new_img = np.zeros((new_h, new_w, 4), dtype=np.uint8)

    # Calculate the coordinates to place the first image on the new canvas
    x1, y1 = (new_w - w1) // 2, (new_h - h1) // 2

    # Calculate the coordinates to place the second image on the new canvas
    x2, y2 = (new_w - w2) // 2, (new_h - h2) // 2

    # Insert the first image into the new canvas
    new_img[y1:y1+h1, x1:x1+w1] = img1

    # Combine the second image with the new canvas using bitwise operations
    alpha_s = img2[:, :, 3] / 255.0
    alpha_l = 1.0 - alpha_s
    for c in range(0, 3):
        new_img[y2:y2+h2, x2:x2+w2, c] = (alpha_s * img2[:, :, c] + alpha_l * new_img[y2:y2+h2, x2:x2+w2, c])

    return new_img


def create_contact_sheet(images, rows=3, cols=3, scale=0.3, output_size=None, filename=None):
    # Create a blank contact sheet
    contact_sheet = np.zeros((rows * images[0].shape[0], cols * images[0].shape[1], 3), np.uint8)
    row = 0
    col = 0

    # Iterate over the images and add them to the contact sheet
    for img in images:
        # Resize the image to the specified scale
        img = cv2.resize(img, None, fx=scale, fy=scale)

        # Calculate the position to add the image to the contact sheet
        y_start = row * img.shape[0]
        y_end = y_start + img.shape[0]
        x_start = col * img.shape[1]
        x_end = x_start + img.shape[1]

        # Add the image to the contact sheet
        contact_sheet[y_start:y_end, x_start:x_end] = img

        # Update the row and column counters
        col += 1
        if col == cols:
            row += 1
            col = 0

        # If we have added all the images we need, stop iterating
        if row == rows:
            break

    # Resize the contact sheet to the specified output size
    if output_size is not None:
        contact_sheet = cv2.resize(contact_sheet, output_size)

    # Write the contact sheet to a file if a filename is specified
    if filename is not None:
        cv2.imwrite(filename, contact_sheet)

    return contact_sheet
