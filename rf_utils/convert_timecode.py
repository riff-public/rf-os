#######covert_timecode#########


def _seconds(value, framerate):
    if isinstance(value, str):  # value seems to be a timestamp
        _zip_ft = zip((3600, 60, 1, 1/framerate), value.split(':'))
        return sum(f * float(t) for f,t in _zip_ft)
    elif isinstance(value, (int, float)):  # frames
        return value / framerate
    else:
        return 0

def _timecode(seconds, framerate):
    return '{h:02d}:{m:02d}:{s:02d}'\
            .format(h=int(seconds/3600),
                    m=int(seconds/60%60),
                    s=int(seconds%60),)

def frame_to_timecode_by_frame(total_frames, fps_int):
    return '{hours:02d}:{minutes:02d}:{seconds:02d}.{frames:02d}'\
            .format(hours = int(total_frames / (3600 * fps_int)),
                    minutes = int(total_frames / (60 * fps_int) % 60),
                    seconds = int(total_frames / fps_int % 60),
                    frames = int(total_frames % fps_int))




    hours = int(total_frames / (3600 * fps_int))
    minutes = int(total_frames / (60 * fps_int) % 60)
    seconds = int(total_frames / fps_int % 60)
    frames = int(total_frames % fps_int)

def _frames(seconds, framerate):
    return seconds * framerate

def timecode_to_frames(timecode, framerate, start=None):
    return _frames(_seconds(timecode, framerate) - _seconds(start, framerate), framerate)

def frames_to_timecode(frames, framerate, start=None):
    return _timecode(_seconds(frames, framerate) + _seconds(start, framerate), framerate)


