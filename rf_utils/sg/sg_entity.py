import sys
import os
import platform
import logging
from datetime import datetime

logger = logging.getLogger(__name__)

if os.environ.get('RFSCRIPT', False):
    path = '%s/core' % os.environ.get('RFSCRIPT')
    if not path in sys.path:
        sys.path.append(path)

import rf_config as config
from shotgun_api3 import Shotgun
from shotgun_api3 import shotgun
from shotgun_api3 import ShotgunError

# connection to server
server = config.Shotgun.server
script = config.Shotgun.script
id = config.Shotgun.id
sg = None
ca_certs = '{}/{}'.format(os.path.dirname(shotgun.__file__.replace('\\', '/')), config.Shotgun.ca_certs)
error = ShotgunError

try:
    sg = Shotgun(server, script, id, ca_certs=ca_certs)
    connection = True
except shotgun.SSLHandshakeError as e:
    logger.error(e)
    connection = False

def init_key(inputKey):
    script, id = config.get_key(inputKey)
    global sg
    sg = Shotgun(server, script, id, ca_cert=ca_cert)


# project = Project(name)
# jax = project.asset('jax')
# jax.name
# jax.fields
# jax.project.name

# asset = Asset(project)
# jax = asset.find('jax')
# jax.status
# jax.sequence
# jax.[feilds]
# step = jax.step('model')
# task = step.task()
# task
# task = model.task()
# task.status
# task.[fields]
# version = task.version()

# model = Step(project, 'jax', 'model')
# task = model.task('model')
# task.status
class Cache:
    project = dict()
    asset = dict()


class AttributeCache:
    asset = dict()
    shot = dict()


class ReturnFields:
    project = ['name', 'id', 'sg_status_list']
    asset = ['code', 'sg_status_list', 'sg_sequence', 'project', 'sg_asset_type', 'sg_subtype']


class Base(object):
    """docstring for Base"""
    def __init__(self, name, sg_entity=None):
        super(Base, self).__init__()
        self.name = name
        self.sg_entity = sg_entity
        self.entity_type = ''

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    @property
    def _filters(self):
        return list()

    @property
    def _all_filters(self):
        return list()

    @property
    def _cache_key(self):
        return str()

    @property
    def entity(self):
        return self.find_entity()

    @property
    def status(self):
        attr = 'sg_status_list'
        return self.entity[attr]

    @property
    def id(self):
        return self.entity['id']

    @property
    def type(self):
        return self.entity['type']

    def find_entity(self):
        entity = self.cache.get(self._cache_key)
        if not entity:
            if not self.sg_entity: 
                entity = sg.find_one(self.entity_type, self._filters, self.return_fields)
            else: 
                entity = self.sg_entity
            self.cache[self._cache_key] = entity
        return self.cache[self._cache_key]

    def get_all_entities(self): 
        entities = sg.find(self.entity_type, self._all_filters, self.return_fields)
        return entities


class Project(Base):
    """docstring for Project"""
    def __init__(self, name, sg_entity=None):
        super(Project, self).__init__(name, sg_entity)
        self.name = name
        self.cache = Cache.project
        self.return_fields = ReturnFields.project
        self.entity_type = 'Project'

    @property
    def _filters(self):
        return [['name', 'is', self.name]]

    @property
    def _cache_key(self):
        return '{}'.format(self.name)

    def asset(self, name=''):
        return Asset(self, name)

    def get_all(self): 
        entities = list()
        sg_entities = self.get_all_entities()
        for entity in sg_entities: 
            entities.append(Project(entity['name'], entity))
        return entities


class Asset(Project):
    """docstring for Asset"""
    def __init__(self, project, name, sg_entity=None):
        super(Asset, self).__init__(name, sg_entity)
        self.project = self.get_project(project)
        self.cache = Cache.asset
        self.return_fields = ReturnFields.asset
        self.entity_type = 'Asset'
        self.asset_type = list()
        self.asset_subtype = list()

    def get_project(self, project):
        if isinstance(project, Project):
            return project
        elif isinstance(project, str):
            return Project(project)

    @property
    def _filters(self):
        return [['project', 'is', self.project.entity], ['code', 'is', self.name]]

    @property
    def _cache_key(self):
        project = self.project
        return '{}-{}'.format(self.project.name, self.name)

    def get_all(self): 
        entities = list()
        sg_entities = self.get_all_entities()
        for entity in sg_entities: 
            entities.append(Asset(self.project, entity['code'], entity))
            self.asset_type.append(entity['sg_asset_type'])
            self.asset_subtype.append(entity['sg_subtype'])
        return entities

    def get_types(self): 
        if not self.asset_type: 
            self.all()
        return list(set(self.asset_type))

    def get_subtypes(self): 
        if not self.asset_subtype: 
            self.all()
        return list(set(self.asset_subtype))

    @property
    def type(self): 
        return self.entity['sg_asset_type']

    @property
    def subtype(self): 
        return self.entity['sg_subtype']
