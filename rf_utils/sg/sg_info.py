import sys
import os
import platform
import logging
from datetime import datetime, timedelta

logger = logging.getLogger(__name__)

if os.environ.get('RFSCRIPT', False):
    path = '%s/core' % os.environ.get('RFSCRIPT')
    if not path in sys.path:
        sys.path.append(path)

import rf_config as config
from shotgun_api3 import Shotgun
from shotgun_api3 import shotgun
from shotgun_api3 import ShotgunError

# connection to server
server = config.Shotgun.server
script = config.Shotgun.script
id = config.Shotgun.id
sg = None
ca_certs = '{}/{}'.format(os.path.dirname(shotgun.__file__.replace('\\', '/')), config.Shotgun.ca_certs)
error = ShotgunError

try:
    sg = Shotgun(server, script, id, ca_certs=ca_certs)
    connection = True
except shotgun.SSLHandshakeError as e:
    logger.error(e)
    connection = False

def init_key(inputKey):
    script, id = config.get_key(inputKey)
    global sg
    sg = Shotgun(server, script, id, ca_cert=ca_cert)