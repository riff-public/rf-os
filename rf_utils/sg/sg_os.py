import os 
import sys 
import rf_config as config
from datetime import datetime, timedelta
from collections import OrderedDict
from rf_utils.context import context_info
from . import sg_process
sg = sg_process.sg 


def list_sync_assets(project, outsource_name, asset_sync_int, asset_task_target): 
    entities = list_os_asset_update(project, 
                                outsource_name, 
                                target_tasks=asset_task_target, 
                                interval=asset_sync_int)

    context_entities = []
    if entities: 
        context_entities = list_os_asset_entity(project, entities)
    return context_entities

def list_os_shots(project, outsource_name): 
    """ searching for tags """ 
    tag = sg.find_one('Tag', [['name', 'is', outsource_name]], ['name', 'id'])
    shots = []

    if tag: 
        filters = [['project.Project.name', 'is', project], ['tags', 'is', tag]]
        fields = ['code', 'id', 'assets', 'project', 'sg_episode', 'sg_sequence_code']
        shots = sg.find('Shot', filters, fields)

    return shots


def list_os_assets(project, outsource_name): 
    shots = list_os_shots(project, outsource_name)
    asset_entities = []
    if shots: 
        for shot in shots: 
            entities = shot['assets']
            if entities: 
                asset_entities += entities

    return asset_entities


def list_os_asset_entity(project, asset_entities): 
    asset_contexts = []
    asset_fields = ['sg_asset_type', 'sg_subtype', 'code', 'project']
    assets = sg.find('Asset', [['project.Project.name', 'is', project]], asset_fields)
    asset_dict = entity_to_dict(assets, 'code')

    if asset_entities: 
        for asset in asset_entities: 
            asset_entity = asset_dict[asset['name']]
            asset_entity['versions'] = asset['versions']
            context = context_info.Context()
            context.update(
                project=project, entityType='asset', 
                entityGrp=asset_entity['sg_asset_type'], 
                entityParent=asset_entity['sg_subtype'], 
                entity=asset_entity['code'])
            asset_context = context_info.ContextPathInfo(context=context)
            info = {'context': asset_context, 'entity': asset_entity}
            asset_contexts.append(info)

    return asset_contexts

def list_cameras(project, outsource_name, days_old): 
    from rf_utils import register_shot
    results = OrderedDict()
    shots = list_os_shots(project, outsource_name)
    if shots: 
        entities = shot_to_context(shots)
        for entity in entities: 
            reg = register_shot.Register(entity)
            cam = reg.get.asset_list(type='camera')
            if cam: 
                cam_path = reg.get.cache(cam[0], hero=True)
                if cam_path:
                    mod_time = check_file_modified_in_timerange(path=cam_path, days=days_old)
                    if mod_time:
                        results[cam[0]] = {'paths': [cam_path], 
                                        'created_at': mod_time}
    return results

def shot_to_context(shots): 
    entities = list()
    for shot in shots: 
        project = shot['project']['name']
        episode = shot['sg_episode']['name']
        sequence = shot['sg_sequence_code']
        shotname = shot['code']
        if project and episode and sequence and shotname: 
            context = context_info.Context()
            context.update(
                project=project, entityType='scene', 
                entityGrp=episode, 
                entityParent=sequence, 
                entity=shotname)
            shot_context = context_info.ContextPathInfo(context=context) 
            entities.append(shot_context)
    return entities


def list_os_asset_update(project, outsource_name, target_tasks=['rig', 'rigUv', 'texture'], interval=7): 
    # search for version of selected task 
    assets = list_os_assets(project, outsource_name)
    start = datetime.now() - timedelta(days=interval)
    end = datetime.now()
    date_range = [start, end]
    update_dict = dict()

    filters = [['project.Project.name', 'is', project], 
                {"filter_operator": "any",
                    "filters": [["entity", "is", a] for a in assets],
                    }, 
                {"filter_operator": "any",
                "filters": [["sg_task.Task.content", "is", a] for a in target_tasks],
                    }, 
                ['created_at', 'between', date_range]
                    ]
    fields = ['created_at', 'entity', 'sg_task', 'code', 'sg_status_list', 'description']
    versions = sg.find('Version', filters, fields)

    for version in versions: 
        entity = version['entity']
        if entity: 
            if not entity['id'] in update_dict.keys(): 
                entity.update({'versions': [version]})
                update_dict[entity['id']] = entity
            else: 
                update_dict[entity['id']]['versions'].append(version)

    return [v for k, v in update_dict.items()]


def entity_to_dict(entities, key='code'): 
    data = dict()
    for entity in entities: 
        data[entity[key]] = entity 
    return data

def check_file_modified_in_timerange(path, days): 
    current = datetime.fromtimestamp(os.path.getmtime(path))
    target_date = datetime.now() - timedelta(days=days)
    
    if current > target_date: 
        return current
        
def test(): 
    from rf_utils import project_info
    project = 'Hanuman'
    outsource_name = 'tuxido_cat'
    
    # set env 
    envData = project_info.ProjectInfo(project).env()
    for key, path in envData.items():
        os.environ[key] = path
    
    asset_list = list_sync_assets(project, outsource_name)

    for asset in asset_list: 
        entity = asset['context']
        print entity.path.hero().abs_path()

        for version in asset['entity']['versions']: 
            print 'This tricker from {} {}'.format(version['sg_task'], version['created_at'])


def test_list_camera(): 
    from rf_utils import register_shot
    project = 'Hanuman'
    outsource_name = 'tuxido_cat'
    cameras = list_cameras(project, outsource_name, 7)
    print cameras
