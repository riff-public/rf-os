#!/usr/bin/env python
# -- coding: utf-8 --
import sys
import os
from datetime import datetime, date
import platform
import logging
from shotgun_api3 import Shotgun
from shotgun_api3 import shotgun
import rf_config as config
from rf_utils import user_info

logger = logging.getLogger(__name__)

# connection to server
key = 'time_track'
server = config.Shotgun.server
script = config.Shotgun.getKey.get(key).get('script')
id = config.Shotgun.getKey.get(key).get('id')
sg = None
ca_certs = '{}/{}'.format(os.path.dirname(shotgun.__file__.replace('\\', '/')), config.Shotgun.ca_certs)


try:
    sg = Shotgun(server, script, id, ca_certs=ca_certs)
    connection = True
except shotgun.SSLHandshakeError as e:
    connection = False

TIMETRACK_ENTITY = 'CustomNonProjectEntity01'
TIMELOGGED_ENTITY = 'CustomNonProjectEntity02'
DATEFORMAT = '%Y:%m:%d'
USER_CACHES = dict()
PROJECT_CACHES = dict()
ENTRY_CACHES = dict()
ENTITY_CACHES = dict()


class ReturnField:
    timetrack = [
        'code', 'sg_project', 'sg_user', 'sg_department',
        'sg_assets', 'sg_shots', 'sg_tasks', 'sg_entity', 'sg_start_record',
        'sg_end_record', 'sg_duration_minute', 'sg_duration_summarry', 'sg_user_logged_items']

    logged_item = [
        'code', 'sg_project', 'sg_user', 'sg_duration_minute', 'sg_record_time', 'sg_time_track_parent']

    task = [
        'content', 'entity', 'step', 'project', 'sg_status_list',
        'task_assignees', 'sg_app', 'sg_resolution', 'sg_name',
        'sg_lockdown', 'sg_lockdown_message', 'open_notes', 'step.Step.short_name']


def create_entry(name, user, entity_type, entity, date_obj, project=None, logged_type='Automatic'):
    """
    create time logged record for user
    Args:
        name (str): Name of the entry e.g. TA:2020-12-1
        user (str): Name of user e.g. "TA" or login name "chanon.v"
        date_obj (object): Date object
        project (str): Name of a project. Leave blank if no project found
        entity_type (str): "Asset" or "Shot"
        entities (list): List of asset or shot
        tasks (list): Name of tasks
    """
    user_entity = find_user(user)
    project_entity = find_project(project) if project else None
    entity = find_entity(project, entity_type, entity)
    data = {
        'code': name,
        'sg_user': user_entity,
        'sg_project': project_entity,
        'sg_start_record': datetime.now(),
        'sg_logged_type': logged_type}
    data.update({'sg_entity': entity}) if entity else None
    entry = sg.create(TIMETRACK_ENTITY, data)
    return entry


def find_entry(name, project, user=None, date_obj=None, logged_type='Automatic'):
    """
    find user entry
    Args:
        name (str): Name of the entry e.g. TA:2020-12-1
        user (str): Name of user e.g. "TA" or login name "chanon.v"
        date_obj (object): Date object
        project (str): Name of a project. Leave blank if no project found
    """
    project_entity = find_project(project)
    fields = [
        'code', 'sg_project', 'sg_user', 'sg_department',
        'sg_assets', 'sg_shots', 'sg_tasks', 'sg_entity', 'sg_start_record',
        'sg_end_record', 'sg_duration_minute']
    filters = [['code', 'is', name], ['sg_project', 'is', project_entity], ['sg_logged_type', 'is', logged_type]]
    if user:
        user_entity = find_user(user)
        filters.append(['sg_user', 'is', user_entity]) if user_entity else None

    entry = sg.find_one(TIMETRACK_ENTITY, filters, fields)
    return entry



def update_entry(name, user, date_obj, project, entity_type='', entity=None, department='', duration=0, assets=[], shots=[], tasks=[], duration_append=True, status='', logged_type='Automatic'):
    """
    update fields data
    Args:
        name (str): Name of the entry TA:2020-12-1
        user (str): Name of user e.g. "TA" or login name "chanon.v"
        date_obj (object): Date object
        project (str): Name of project
        entity_type (str): "Asset" or "Shot"
        assets (list): List of asset
        shots (list): List of shot
        tasks (list): Name of tasks
    """
    entry = find_entry(name, project, user=user, date_obj=date_obj, logged_type=logged_type)
    if not entry:
        entry = create_entry(name, user, entity_type, entity, date_obj, project, logged_type=logged_type)

    current_duration = entry.get('sg_duration_minute', 0)
    new_duration = current_duration + duration if duration_append else duration
    current_assets = [a['name'] for a in entry.get('sg_assets', [])]
    current_shots = [a['name'] for a in entry.get('sg_shots', [])]
    current_tasks = [a['name'] for a in entry.get('sg_tasks', [])]

    new_assets = [a for a in assets if a not in current_assets]
    new_shots = [a for a in shots if a not in current_shots]
    new_tasks = [a for a in tasks if a not in current_tasks]
    # print 'new_tasks', new_tasks

    entity = find_entity(project, entity_type, entity)
    task_entities = find_entity_tasks(project, entity, new_tasks) + entity.get('sg_tasks', [])
    # print 'task_entities', task_entities
    # assets = find_entities(project, 'Asset', new_assets) + entry.get('sg_assets', [])
    # shots = find_entities(project, 'Shot', new_shots) + entry.get('sg_shots', [])

    data = {
        # 'sg_assets': assets, 'sg_shots': shots,
        'sg_duration_minute': new_duration,
        'sg_department': department}
    data.update({'sg_entity': entity}) if entity else None
    data.update({'sg_tasks': task_entities}) if task_entities else None
    data.update({'sg_status_list': status}) if status else None
    return sg.update(TIMETRACK_ENTITY, entry['id'], data)


def find_project(name):
    if isinstance(name, str):
        if name not in PROJECT_CACHES.keys():
            project = sg.find_one('Project', [['name', 'is', name]], ['id', 'name', 'sg_project_code'])
            PROJECT_CACHES[name] = project
        return PROJECT_CACHES[name]

    elif isinstance(name, dict):
        return name


def find_entity_tasks(project, entity, tasks):
    match_tasks = []
    if tasks:
        project_entity = find_project(project)
        filters = [['project', 'is', project_entity], ['entity', 'is', entity]]
        task_entities = sg.find('Task', filters, ReturnField.task)
        match_tasks = [a for a in task_entities if a['content'] in tasks]

    return match_tasks


def find_entity_task(project, sg_entity, task_name):
    project_entity = find_project(project)
    filters = [['project', 'is', project_entity], ['entity', 'is', sg_entity], ['content', 'is', task_name]]
    return sg.find_one('Task', filters, ReturnField.task)


def find_user(name):
    """
    get user entity from given name or login name
    """
    if isinstance(name, str):
        if name not in USER_CACHES.keys():
            nickname = name if '.' not in name else ''
            adname = name if '.' in name else ''
            user = user_info.User(sg, nickname=nickname, adname=adname)
            sg_user = user.sg_user()
            if sg_user:
                USER_CACHES[name] = user.sg_user()
            else:
                return sg_user
        return USER_CACHES[name]

    elif isinstance(name, dict):
        return name


def find_entities(project, entity_type, entities):
    sg_entities = []
    for entity in entities:
        entity = find_entity(project, entity_type, entity)
        if entity:
            sg_entities.append(entity)

    return sg_entities


def find_entity(project, entity_type, entity):
    if entity_type and entity:
        project_entity = find_project(project)
        key = '{}-{}'.format(project_entity['name'], entity)

        if key not in ENTITY_CACHES.keys():
            filters = [['project', 'is', project_entity], ['code', 'is', entity]]
            fields = ['id', 'code']
            entity = sg.find_one(entity_type, filters, fields)
            if entity:
                ENTITY_CACHES[key] = entity
            else:
                return dict()

        return ENTITY_CACHES[key]
    return dict()


def fetch_timetrack_data(user, project=None):
    """
    fetch data frrom time tracking entity for given user

    Args:
        user (str) : user name

    Returns (dict): Shotgun entities dictionary
    """
    user_entity = find_user(user)
    filters = [['sg_user', 'is', user_entity]]
    filters.append(['sg_project.Project.name', 'is', project])
    return sg.find(TIMETRACK_ENTITY, filters, ReturnField.timetrack)


def find_tasks(project, user):
    user_entity = find_user(user)
    project_entity = find_project(project)
    filters = [['project', 'is', project_entity], ['task_assignees', 'is', user_entity]]
    return sg.find('Task', filters, ReturnField.task)

# ==========================================================================


def create_user_task_track(user, project, entity_type, entity_name, task_name, step='', logged_type=''):
    """
    This will create an entry on Time Tracking Entity
    """
    name = user_task_track_name(user, project, entity_name, task_name)
    user_entity = find_user(user)
    project_entity = find_project(project)
    sg_entity = find_entity(project, entity_type, entity_name)
    task_entity = find_entity_task(project_entity, sg_entity, task_name)
    step = task_entity['step.Step.short_name'] if not step else step

    data = {
        'code': name,
        'sg_project': project_entity,
        'sg_user': user_entity,
        'sg_entity': sg_entity}

    data.update({'sg_task': task_entity}) if task_entity else None
    data.update({'sg_department': step}) if step else None
    data.update({'sg_logged_type': logged_type}) if logged_type else None

    entry = find_user_task_track(name, project, user)
    if not entry:
        entry = sg.create(TIMETRACK_ENTITY, data)
    else:
        entry = sg.update(TIMETRACK_ENTITY, entry['id'], data)
    return entry


def create_user_logged_item(parent_entity, project, user, duration=0, duration_append=False):
    item_name = user_logged_name(parent_entity['code'])
    project_entity = find_project(project)
    user_entity = find_user(user)
    entry = find_user_logged_item(item_name, project_entity, user_entity)
    current_duration = 0
    record_time = datetime.now()
    if entry:
        current_duration = int(entry['sg_duration_minute'])
        new_duration = current_duration + duration if duration_append else duration
        data = {
            'sg_duration_minute': new_duration}
        return sg.update(TIMELOGGED_ENTITY, entry['id'], data)

    else:
        new_duration = current_duration + duration if duration_append else duration
        data = {
            'code': item_name,
            'sg_project': project_entity,
            'sg_user': user_entity,
            'sg_duration_minute': new_duration,
            'sg_time_track_parent': parent_entity,
            'sg_record_time': record_time}

        return sg.create(TIMELOGGED_ENTITY, data)
    # TA-Hanuman-cara-model_md-2021:01:08


def user_task_track_name(user='None', project='None', entity_name='None', task_name='None'):
    user_str = user['name'] if isinstance(user, dict) else user
    project_str = project['name'] if isinstance(project, dict) else project
    name = '-'.join([user_str, project_str, entity_name, task_name])
    return name


def user_logged_name(track_name, date_string=''):
    if not date_string:
        date_string = datetime.now().date().strftime(DATEFORMAT)
    item_name = '{}-{}'.format(track_name, date_string)
    return item_name


def day_str(datetime_object):
    if not datetime_object:
        return datetime.now().date().strftime(DATEFORMAT)
    return datetime_object.date().strftime(DATEFORMAT)


# def find_user_task_track(user='None', project='None', entity_name='None', task_name='None'):
#     name = user_task_track_name(user, project, entity_name, task_name)
#     project_entity = find_project(project)
#     user_entity = find_user(user)
#     filters = [['sg_project', 'is', project_entity], ['sg_user', 'is', user_entity], ['code', 'is', name]]
#     return sg.find_one(TIMETRACK_ENTITY, filters, ReturnField.timetrack)

def find_user_task_track(name, project, user):
    project_entity = find_project(project)
    user_entity = find_user(user)
    filters = [
        ['code', 'is', name],
        ['sg_project', 'is', project_entity],
        ['sg_user', 'is', user_entity]]
    return sg.find_one(TIMETRACK_ENTITY, filters, ReturnField.timetrack)


def find_user_time_tracks(user, project=None):
    user_entity = find_user(user)
    project_entity = find_project(project) if project else None
    filters = [['sg_user', 'is', user_entity]]
    filters.append(['sg_project', 'is', project_entity]) if project_entity else None
    return sg.find(TIMETRACK_ENTITY, filters, ReturnField.timetrack)


def find_user_logged_item(name, project, user, parent_entity=None):
    project_entity = find_project(project)
    user_entity = find_user(user)
    filters = [
        ['code', 'is', name],
        ['sg_project', 'is', project_entity],
        ['sg_user', 'is', user_entity]]
    filters.append(['sg_time_track_parent', 'is', parent_entity]) if parent_entity else None
    return sg.find_one(TIMELOGGED_ENTITY, filters, ReturnField.logged_item)


def find_user_logged_by_ids(ids):
    filter_ids = [['id', 'is', id] for id in ids]
    filters = [
        {
            "filter_operator": "any",
            "filters": filter_ids
        }
    ]
    return sg.find(TIMELOGGED_ENTITY, filters, ReturnField.logged_item)


"""
User
Find
- Project
        - Tasks
            - duration on each task
Update
- Project / Entity / Task -> Duration

Create
- Create task
- Add time

"""

class UserTimeTrack(object):
    """docstring for UserTimeTrack"""
    def __init__(self, user, project=None, logged_type='User'):
        super(UserTimeTrack, self).__init__()
        self.user_entity = find_user(user)
        self.project_entity = find_project(project) if project else None
        self.logged_type = logged_type

    def list_tasks(self):
        return find_user_time_tracks(self.user_entity, self.project_entity)

    def find_task(self, entity_type, entity_name, task_name):
        return TimeTask(
            user=self.user_entity,
            project=self.project_entity,
            entity_name=entity_name,
            entity_type=entity_type,
            task_name=task_name,
            logged_type=self.logged_type)


class TimeTask(object):
    """docstring for TimeTask"""
    def __init__(self, user, project, entity_name, entity_type, task_name, logged_type='User'):
        super(TimeTask, self).__init__()
        self.user_entity = find_user(user)
        self.project_entity = find_project(project) if project else None
        self.entity_name = entity_name
        self.entity_type = entity_type
        self.task_name = task_name
        self.logged_type = logged_type

        self.name = user_task_track_name(
            self.user_entity, self.project_entity, self.entity_name, self.task_name)

        self.entity = find_user_task_track(
            name=self.name,
            project=self.project_entity,
            user=self.user_entity)

    def __str__(self):
        return self.entity['code']

    def time_logged(self):
        track_entity = self.entity
        if track_entity:
            entity_logged = TimeLoggedItem(
                self.user_entity, self.project_entity, parent_entity=track_entity)
            return entity_logged

    def create(self):
        self.sg_entity = find_entity(
            self.project_entity, self.entity_type, self.entity_name)
        track_entity = create_user_task_track(
            user=self.user_entity,
            project=self.project_entity,
            entity_type=self.entity_type,
            entity_name=self.entity_name,
            task_name=self.task_name,
            logged_type=self.logged_type)

        entity_logged = TimeLoggedItem(
            self.user_entity, self.project_entity, parent_entity=track_entity)
        return entity_logged


class TimeLoggedItem(object):
    """docstring for TimeLoggedItem"""
    def __init__(self, user, project, entity_name='', task_name='', parent_entity=None):
        super(TimeLoggedItem, self).__init__()
        self.logged_entity = None
        self.user_entity = find_user(user)
        self.project_entity = find_project(project)
        self.name = user_task_track_name(
            self.user_entity, self.project_entity, entity_name, task_name)
        self.parent_entity = find_user_task_track(
            name=self.name,
            project=self.project_entity,
            user=self.user_entity) if not parent_entity else parent_entity

    def list_all_logged(self):
        entities = self.parent_entity['sg_user_logged_items']
        ids = [a['id'] for a in entities]
        return find_user_logged_by_ids(ids)

    def add_time(self, duration=0):
        return create_user_logged_item(
            self.parent_entity,
            self.project_entity,
            self.user_entity,
            duration=duration,
            duration_append=True)

    def time(self, item_date='today'):
        track_name = self.parent_entity['code']
        date_string = day_str(datetime.now())
        name = user_logged_name(track_name, date_string=date_string)
        self.logged_entity = find_user_logged_item(name, self.project_entity, self.user_entity, parent_entity=self.track_entity)
        return self.logged_entity['sg_duration_minute']


# sg = sg_time_track.sg
# filters = [
#     {
#         "filter_operator": "any",
#         "filters": [
#             ["id", "is", 3],
#             ["id", "is", 34]
#         ]
#     }
# ]
# result = sg.find("CustomNonProjectEntity02", filters, ['code'])
