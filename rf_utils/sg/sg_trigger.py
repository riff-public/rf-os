import sys
import os
import platform
import logging

logger = logging.getLogger(__name__)

if os.environ.get('RFSCRIPT', False):
    path = '%s/core' % os.environ.get('RFSCRIPT')
    if not path in sys.path:
        sys.path.append(path)

import rf_config as config
from shotgun_api3 import Shotgun
from datetime import datetime

# connection to server
server = config.Shotgun.server
script = config.Shotgun.script
id = config.Shotgun.id
sg = Shotgun(server, script, id)
# sg = None

def init_key(inputKey):
    script, id = config.get_key(inputKey)
    global sg
    sg = Shotgun(server, script, id)


def status_update_processor(sg, event, args):
    if chekc_can_process(event):
        sg_process.sg = sg
        project_allowed = get_allow_project(event)
        project_entity = event['project']
        logger.info("%s" % str(event))

        if project_allowed and event['entity']:
            if event['meta']['old_value'] != event['meta']['new_value']:
                task = sg.find_one("Task", [
                    ['id', 'is', event['meta']['entity_id']],
                    ['project', 'is', project_entity]],
                    ['entity', 'step', 'content']
                )
                if task:
                    if possible_task_process(task):
                        proj = project_info.ProjectInfo(project=project_entity['name'])
                        if task['entity']['type'] == "Asset":
                            tasks_trigger_table = proj.task.dependency('asset')
                            look_trigger_table = proj.task.look_dependency()
                            logger.info("look_trigger_tablelook_trigger_table: %s" % str(look_trigger_table))
                            taskEntity = sg_process.get_one_task_by_step(task.get('entity'), task.get('step', {}).get('name'), task.get('content'), create=False)
                            if taskEntity:
                                result = update_dependency_event(sg, tasks_trigger_table, taskEntity, event)
                                look_dependency(sg, look_trigger_table, taskEntity, event, logger)
                        elif task['entity']['type'] == 'Shot':
                            tasks_trigger_table = proj.task.dependency('scene')
                            look_trigger_table = proj.task.look_dependency()
                            taskEntity = sg_process.get_one_task_by_step(task.get('entity'), task.get('step', {}).get('name'), task.get('content'), create=False)
                            if taskEntity:
                                result = update_dependency_event(sg, tasks_trigger_table, taskEntity, event)
                                look_dependency(sg, look_trigger_table, taskEntity, event, logger)


def update_dependency_event(sg, tasks_trigger_table, taskEntity, event_detail):
    step = taskEntity.get('step').get('name')
    taskname = taskEntity.get('content')
    triggers = []
    for task_config in tasks_trigger_table:
        if len(task_config['events']) == 1:
            if (
                task_config['events'][0]['step'] == step and
                task_config['events'][0]['task'] == taskname and
                event_detail['meta']['old_value'] in task_config['events'][0]['old_status'] and
                task_config['events'][0]['new_status'] == event_detail['meta']['new_value']
            ):
                triggers.append(task_config['trigger'])
        elif len(task_config['events']) > 1:
            match_event = event_multiple_status_checking(taskEntity, task_config, event_detail, step, taskname)
            if match_event:
                triggers.append(task_config['trigger'])

    results = []

    # update
    if triggers:
        for trigger in triggers[0]:
            triggerStep = trigger['step']
            triggerTaskName = trigger['task']
            triggerStatus = trigger['new_status']

            if triggerTaskName == '*':
                triggerTasks = sg_process.get_tasks_by_step(taskEntity['entity'], triggerStep)
            else:
                triggerTasks = [sg_process.get_one_task_by_step(taskEntity['entity'], triggerStep, triggerTaskName)]

            for triggerTask in triggerTasks:
                if triggerTask:
                    if triggerTask['sg_status_list'] in trigger['old_status']:
                        result = sg_process.set_task_status(triggerTask['id'], triggerStatus)
                        # logger.debug('trigger %s %s %s' % (triggerStep, triggerTaskName, triggerStatus))
                        results.append(result)
                        # slack_apis.api_call(
                        #     "chat.postMessage",
                        #     channel="DD7390L1Y",
                        #     text="Project {project_name} step ".format(project_name=event_detail['project']['name'], step=event_detail['entity']['name']),
                        #     username="Friday"
                        # )

    return results


def look_dependency(sg, look_trigger_table, taskEntity, event_detail):
    """ texture triggered lookdev with the same sg_name """
    step = taskEntity.get('step').get('name')
    taskname = taskEntity.get('content')
    triggers = []
    eventAttr = []
    for idx, look in enumerate(look_trigger_table):
        if len(look['events']) == 1:
            if (
                look['events'][0]['step'] == step and
                event_detail['meta']['old_value'] in look['events'][0]['old_status'] and
                look['events'][0]['new_status'] == event_detail['meta']['new_value']
            ):
                triggers.append(look['trigger'])
                eventAttr.append(look['events'][0]['attr'])
        elif len(look['events']) > 1:
            match_event = event_multiple_status_checking(taskEntity, look, event_detail, step, taskname)
            if match_event:
                triggers.append(look['trigger'])
                eventAttr.append(look['events'][idx]['attr'])

    # triggers = [a['trigger'] for a in eventList if a['event']['step'] == step and a['event']['status'] == status]
    # eventAttr = [a['event']['attr'] for a in eventList if a['event']['step'] == step and a['event']['status'] == status]
    lookname = taskEntity[eventAttr[0]] if eventAttr else ''
    if triggers and lookname:
        assetTasks = sg_process.get_tasks(taskEntity['entity'])
        for trigger in triggers[0]:
            triggerStep = trigger['step']
            matchAttr = trigger['attr']
            triggerStatus = trigger['new_status']
            trigger_old_status = trigger['old_status']
            trigger_task = trigger['task']

            triggeredTasks = [a for a in assetTasks if a[matchAttr] == lookname and a['step']['name'] == triggerStep]
            for triggeredTask in triggeredTasks:
                if triggeredTask:
                    if triggeredTask['sg_status_list'] in trigger['old_status']:
                        result = sg_process.set_task_status(triggeredTask['id'], triggerStatus)
                    # logger.debug('trigger %s %s %s' % (triggerStep, triggeredTask['content'], triggerStatus))


def event_multiple_status_checking(taskEntity, task_config, event_detail, step, taskname):
    matched_event_step_idx = None
    match_events = False
    for idx, events in enumerate(task_config['events']):
        if events['step'] == step and events['indicator_step'] == True:
            if (
                events['step'] == step and
                events['task'] == taskname and
                event_detail['meta']['old_value'] in events['old_status'] and
                events['new_status'] == event_detail['meta']['new_value']
            ):
                matched_event_step_idx = idx
    if matched_event_step_idx is not None:
        match_event_count = 0
        for idx, event in enumerate(task_config['events']):
            if idx == matched_event_step_idx:
                continue
            step_task = sg_process.get_one_task_by_step(
                taskEntity['entity'],
                event['step'],
                event['task'],
                create=False
            )
            if step_task:
                if (
                    step_task['step']['name'] == event['step'] and
                    step_task['content'] == event['task'] and
                    step_task['sg_status_list'] == event['trigger_status']
                ):
                    match_event_count = match_event_count + 1

        if len(task_config['events']) - 1 == match_event_count:
            match_events = True

    return match_events


def get_allow_project(event):
    if event['project']['name'] in ALLOW_PROJECT_PROD:
        return True
    return False


def chekc_can_process(event):
    # if event['project'] is None and event['session_uuid'] is None: #if use script update not trigger
    if event['project'] is None:
        return False
    return True


def possible_task_process(task):
    if task.get('entity') is not None and task.get('step') is not None and task.get('content') is not None:
        return True
    return False
