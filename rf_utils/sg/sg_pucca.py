from rf_utils.sg import sg_process
def pucca_chk_episode(project, episode):
    ep_entity = sg_process.get_one_episode(project, episode)
    if not ep_entity:
        sg_process.create_episode(project, episode) 

def pucca_find_entity(project, episode, seq, shotName):
    seq = 'SEQ'
    shot_entity = sg_process.get_one_shot(project, episode, seq, shotName)
    if not shot_entity:
        projectEntity = sg_process.get_project(project)
        episodeEntity = sg_process.get_one_episode(project, episode)
        seqEntity = sg_process.get_one_sequence(project, episode, seq)
        if not seqEntity:
            seqEntity = sg_process.create_sequence(projectEntity, episodeEntity, seq, None)
        shortCode = '%s_%s'%(episode, shotName)
        taks_template = {'code': 'Pucca Shot', 'type': 'TaskTemplate', 'id': 118}
        shot_entity = sg_process.create_shot(projectEntity, episodeEntity, seqEntity, shortCode, shotName, taskTemplate= taks_template)
        return projectEntity, seqEntity, shot_entity
        
    else:
        return projectEntity ,seqEntity, shot_entity