#!/usr/bin/env python
# -- coding: utf-8 --
import os
import sys
from rf_utils.sg import sg_process
from rf_utils import user_info

try:
    from urllib import urlopen, urlretrieve
except ImportError:
    from urllib.request import urlopen, urlretrieve

from datetime import datetime
from collections import OrderedDict
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

sg = sg_process.sg
projectCaches = dict()
versionIdCaches = dict()
versionCaches = dict()
taskCaches = dict()
userCaches = []
entityCaches = dict()
taskStepCaches = dict()

class Value:
    client_approved_version = 'ClientApprovedVersion'
    note = 'Note'
    brief = 'Brief'

class Attr: 
    note_link_attr = 'sg_brief'
    note_type_attr = 'note_type'

class DataField:
    project = ['name', 'id']
    note = ['project', 'note_links', 'created_at', 'updated_at', 'content', 'subject', 'sg_note_type',
            'tasks', 'user', 'sg_status_list', 'attachments', 'sg_modified_by', 'user.HumanUser.image', 'sg_brief',
            'addressings_to', 'sg_subnotes', 'note_sg_subnotes_notes', 'ticket_sg_note_tickets', 'addressings_cc', 
            'tasks.Task.due_date']
    attachment = ['image', 'filename', 'created_by', 'id']
    entity = ['code', 'id', 'project', 'sg_notes']
    user = ['sg_localuser', 'sg_ad_account', 'name', 'department', 'groups', 'image']
    version = ['user', 'entity', 'sg_task', 'sg_task.Task.step', 'sg_status_list', 'sg_uploaded_movie', 'image', 'sg_path_to_movie', 'code', 'description', 'created_at',
            'client_approved_at', 'client_approved', 'client_code', 'client_approved_by']
    asset = ['shots', 'sg_asset_type', 'code', 'id', 'image', 'project']
    shot = ['assets', 'code', 'id', 'sg_sequence_code', 'image', 'project']
    task = ['content', 'id', 'step', 'due_date', 'sg_status_list', 'task_assignees', 'project', 'sg_resolution', 'entity', 'sg_notes', 'step.Step.short_name']
    entityMap = {'asset': shot, 'scene': asset}
    ticket = ['attachments', 'updated_at', 'replies', 'id', 'sg_ticket_type', 'title', 'addressings_to', 'created_by', 'sg_status_list', 'reply_content',
            'due_date', 'sg_due_date', 'description', 'addressings_cc', 'updated_by', 'tickets', 'created_at', 'sg_related_assets', 'project', 'sg_note', 'sg_priority']


class Config:
    sgEntity = {'asset': 'Asset', 'scene': 'Shot'}
    sgSearchMap = {'asset': 'Shot', 'scene': 'Asset'}
    attrMap = {'asset': 'assets', 'scene': 'shots'}
    entityTypeMap = {'Asset': 'asset', 'Shot': 'scene'}
    keywordPrefix = {'sender': '@', 'receiver': '#', 'status': '*'}
    kwSeparator = ':'
    allProject = 'All'


def clear_caches(cacheType=''):
    """ clear caches """
    if cacheType == 'project':
        global projectCaches
        projectCaches = dict()
    elif cacheType == 'taskStep':
        global taskStepCaches
        taskStepCaches = dict()


def fetch_brief2(project, sg_entity):
    """ fetch brief from main brief and task brief
    Args : Shotgun Entity {'type': 'Asset', 'id': 0}
    Returns : (list) List of Note entities
    """
    brief_dict = dict()
    task_dict = dict()
    note_link_attr = 'sg_brief'
    # find related link -> Asset / tasks
    tasks = sg.find('Task', [['project.Project.name', 'is', project], ['entity', 'is', sg_entity]], DataField.task)

    # find note type brief
    entity_filters = [[note_link_attr, 'is', a] for a in tasks]
    entity_filters.append([note_link_attr, 'is', sg_entity])

    filters = [['project.Project.name', 'is', project], ['sg_note_type', 'is', 'Brief']]
    advance_filters = {
                        "filter_operator": "any",
                        "filters": entity_filters
                    }
    filters.append(advance_filters)
    brief_entities = sg.find('Note', filters, DataField.note)

    # rearrange data

    for task in tasks:
        step = task['step.Step.short_name']
        task_dict[task['id']] = step

    for brief in brief_entities:
        link = brief['sg_brief']
        if link['type'] in ['Asset', 'Shot']:
            if not 'entity' in brief_dict.keys():
                brief_dict['entity'] = [brief]
            else:
                brief_dict['entity'].append(brief)
        else:
            step = task_dict[link['id']]
            if step not in brief_dict.keys():
                brief_dict[step] = [brief]
            else:
                brief_dict[step].append(brief)

    return brief_dict


def create_brief2(project, link_entity, subject, content, user, attachments=[]):
    # name = 'none'
    # if link_entity['type'] in ['Asset', 'Shot']:
    #     name = link_entity['code']
    # elif link_entity['type'] == 'Task':
    #     entity = link_entity['entity']
    #     entity_name = entity['name']
    #     task = link_entity['content']
    #     name = '{}_{}'.format(entity_name, task)
    # subject = '{}_main_brief'.format(name)

    project_entity = get_project(project)
    data = {'project': project_entity,
            'sg_brief': link_entity,
            'subject': subject,
            'content': content,
            'user': user,
            'sg_note_type': 'Brief'
            }

    brief = sg.create('Note', data)

    if attachments:
        files = [a for a in attachments if os.path.exists(a)]
        upload_attachments(brief['id'], files)

    return brief

# =========================================================================================================
# change the way brief link 

def fetch_brief(project, sg_entity):
    """ fetch brief from main brief and task brief
    Args : Shotgun Entity {'type': 'Asset', 'id': 0}
    Returns : (list) List of Note entities
    """
    brief_dict = dict()
    task_dict = dict()
    note_link_attr = 'sg_brief'
    # find related link -> Asset / tasks
    tasks = sg.find('Task', [['project.Project.name', 'is', project], ['entity', 'is', sg_entity]], DataField.task)

    # find note type brief
    filters = [['project.Project.name', 'is', project], ['sg_note_type', 'is', 'Brief'], ['note_links', 'is', sg_entity]]
    brief_entities = sg.find('Note', filters, DataField.note)

    # rearrange data
    for task in tasks:
        step = task['step.Step.short_name']
        task_dict[task['id']] = step

    for brief in brief_entities:
        link = brief['sg_brief']
        # no tasks mean entity 
        tasks = brief['tasks']

        if not tasks: 
            if not 'entity' in brief_dict.keys():
                brief_dict['entity'] = [brief]
            else:
                brief_dict['entity'].append(brief)

        else: 
            for task in tasks: 
                step = task_dict[task['id']]

                if step not in brief_dict.keys():
                    brief_dict[step] = [brief]
                else:
                    if not brief in brief_dict[step]: 
                        brief_dict[step].append(brief)

    return brief_dict


def create_brief(project, entity, subject, content, user, tasks=[], attachments=[]):

    project_entity = get_project(project)
    if tasks: 
        entity_tasks = sg.find('Task', [['project.Project.name', 'is', project], ['entity', 'is', entity]], ['content'])
        tasks = [a for a in entity_tasks if a['content'] in tasks]
    data = {'project': project_entity,
            # 'sg_brief': link_entity,
            'note_links': [entity],
            'subject': subject,
            'content': content,
            'user': user,
            'tasks': tasks,
            'sg_note_type': 'Brief'
            }

    brief = sg.create('Note', data)

    if attachments:
        files = [a for a in attachments if os.path.exists(a)]
        upload_attachments(brief['id'], files)

    return brief


def find_link_entity(project, entity_type, entity_name, taskname=None):
    if not taskname:
        filters = [['project.Project.name', 'is', project], ['code', 'is', entity_name]]
        entity = sg.find_one(entity_type, filters, DataField.entity)
    else:
        filters = [
            ['project.Project.name', 'is', project], ['entity.{}.code'.format(entity_type), 'is', entity_name],
            ['content', 'is', taskname]]
        entity = sg.find_one('Task', filters, DataField.task)
    return entity


def find_entity(project, entity_type, entity_name): 
    filters = [['project.Project.name', 'is', project], ['code', 'is', entity_name]]
    entity = sg.find_one(entity_type, filters, DataField.entity)
    return entity


def fetch_users():
    """ fetch all users """
    global userCaches
    if not userCaches:
        filters = []
        userEntities = sg.find('HumanUser', filters, DataField.user)
        userCaches = userEntities
    return userCaches


def get_user_image(userId):
    allUser = fetch_users()
    userEntities = [a for a in allUser if a.get('id') == userId]
    if userEntities:
        userEntity = userEntities[0]
        user = user_info.User()
        user.userEntity = userEntity
        return user.get_thumbnail()


def fetch_all_notes(project_name):
    filters = [['project.Project.name', 'is', project_name]]
    return sg.find('Note', filters, DataField.note)


def fetch_all_project_notes(project, step_entity=None, user_entity=None):
    """
    {'note':
    {
        'tasks': [{'template_task_id': 39282, 'type': 'Task', 'id': 283373, 'name': 'anim'}],
        'note_links': [{'type': 'Shot', 'id': 9828, 'name': 'hnm_act1_q0030a_s0010'}],
        'attachments': [],
        'user.HumanUser.image': 'https://sg-media-tokyo.s3-accelerate.amazonaws.com',
        'created_at': datetime.datetime(2020, 8, 25, 20, 20, 48, ),
        'addressings_to': [],
        'updated_at': datetime.datetime(2020, 11, 6, 16, 28, 29, ),
        'content': '',
        'sg_note_type': 'Fix',
        'user': {'type': 'HumanUser', 'id': 263, 'name': 'Banmu'},
        'sg_modified_by': None,
        'sg_status_list': 'clsd',
        'sg_brief': None,
        'type': 'Note',
        'id': 14144,
        'subject': "Riff's Note on hnm_act1_q0030a_s0010"
    },
    'status': 'clsd',
    'sender': {'type': 'HumanUser', 'id': 263, 'name': 'Banmu'},
    'receivers': [{'type': 'HumanUser', 'id': 548, 'name': 'To'}]

    }
    """
    from datetime import datetime
    start = datetime.now()
    note_list = list()

    if project['name'] == Config.allProject:
        active_projects = sg.find('Project', [['sg_status', 'is', 'Active']], ['name', 'id'])
        project_filter = [{
            "filter_operator": "any",
            "filters": [["project", "is", a] for a in active_projects]
        }]
        filters = project_filter
        note_filter = list(project_filter)
    else:
        filters = [['project', 'is', project]]
        note_filter = list(filters)

    filters.append(['step', 'is', step_entity]) if step_entity else None
    tasks = sg.find('Task', filters, DataField.task)
    print('finished task {}'.format(datetime.now() - start))

    notes = sg.find('Note', note_filter, DataField.note)
    print('finished note {}'.format(datetime.now() - start))
    exclude_types = ['Brief']

    task_dict = dict()
    for task in tasks:
        task_dict[task['id']] = task

    for note in notes:
        note_dict = dict()
        receivers = list()
        senders = list()
        involve_users = list()
        note_tasks = note['tasks']
        is_related_to_step = False
        related_user = False
        note_type = note['sg_note_type']
        addressings_to = note['addressings_to']
        receivers = addressings_to if addressings_to else list()

        if note_type not in exclude_types:
            for note_task in note_tasks:
                if note_task['id'] in task_dict.keys():
                    is_related_to_step = True
                    task = task_dict[note_task['id']]
                    assignees = task['task_assignees']
                    if assignees:
                        for assignee in assignees:
                            if not assignee in receivers:
                                receivers.append(assignee)



            note_dict['note'] = note
            note_dict['sender'] = note['user'] or dict()
            note_dict['status'] = note['sg_status_list']
            note_dict['receivers'] = receivers

            # keyword building
            sender_kw = '{}{}'.format(Config.keywordPrefix['sender'], note_dict.get('sender').get('name'))
            status_kw = '{}{}'.format(Config.keywordPrefix['status'], note['sg_status_list'])
            receiver_kw_list = list()

            for receiver in receivers:
                kw = '{}{}'.format(Config.keywordPrefix['receiver'], receiver['name'])
                receiver_kw_list.append(kw)
            receiver_kw = Config.kwSeparator.join(receiver_kw_list)
            keyword = Config.kwSeparator.join([sender_kw, receiver_kw, status_kw])
            note_dict['keyword'] = keyword

            involve_users = receivers + [note['user']]

            if user_entity.get('id') in [a.get('id') for a in involve_users if a]:
                related_user = True

                # kw = [note.get('user', dict()).get('name') or '', note['sg_status_list']] + [a['name'] for a in receivers]
                # note_dict['keyword'] = ':'.join(kw)

            if is_related_to_step or related_user:
                note_list.append(note_dict)

    return note_list


def note_tree_relation(note_list):
    """ only allow root and child one level """
    note_tree = list()
    note_dict = dict()
    note_tree_dict = dict()
    note_tree_list = list()
    childs = [a['note']['id'] for a in note_list if a['note']['note_sg_subnotes_notes']]
    roots = [a['note']['id'] for a in note_list if not a['note']['id'] in childs]

    for note in note_list:
        note_dict[note['note']['id']] = note

    for note_id in roots:
        note_tree_dict[note_id] = list()
        for child_id in childs:
            parents = note_dict[child_id]['note']['note_sg_subnotes_notes']

            for parent in parents:
                if parent['id'] == note_id:
                    note_tree_dict[note_id].append(child_id)

    for note_id, child_ids in note_tree_dict.items():
        note = note_dict[note_id]

        # find keyword
        keyword = note['keyword']
        root_sender = note['sender']
        root_receivers = note['receivers']
        root_status = note['status']
        root_update = note['note']['updated_at']

        child = [{'entity': note_dict[a]} for a in child_ids]
        child_kw = [note_dict[a]['keyword'] for a in child_ids]
        child_sender = [note_dict[a]['sender'] for a in child_ids]
        child_status = [note_dict[a]['status'] for a in child_ids]
        child_update = [note_dict[a]['note']['updated_at'] for a in child_ids]
        child_receivers = list()

        for child_id in child_ids:
            for receiver in note_dict[child_id]['receivers']:
                child_receivers.append(receiver)

        keyword = combine_keywords([keyword] + child_kw)
        senders = child_sender + [root_sender]
        receivers = root_receivers + child_receivers
        statuses = [root_status] + child_status
        update = [root_update] + child_update
        latest_update = sorted(update)[-1]

        data = {
                'entity': note, 'child': child, 'keyword': keyword,
                'sender': senders, 'receivers': receivers, 'status': statuses,
                'updated_at': latest_update}
        note_tree_list.append(data)

    return note_tree_list


def combine_keywords(keywords):
    keyword_list = list()

    for keyword in keywords:
        keyword_list += keyword.split(Config.kwSeparator)

    keyword = Config.kwSeparator.join(list(set(keyword_list)))
    return keyword


def get_note_attribute(note_list, key, keystr=''):
    """ get data from keyword """
    keydict = dict()
    for note_dict in note_list:
        kw = str()
        value = note_dict.get(key)

        if isinstance(value, list):
            values = value
        else:
            values = [value]

        for v in values:
            if isinstance(v, dict):
                if v:
                    kw = v[keystr]

            elif isinstance(v, str):
                kw = v

            if kw:
                if not kw in keydict.keys():
                    keydict[kw] = {'value': v, 'count': 1}
                else:
                    keydict[kw]['count'] += 1

    return keydict


def get_sum_filter(keydict):
    count = 0
    for k, v in keydict.items():
        count += v['count']
    return count


def sort_notes_by_date(note_list):
    tmp = dict()
    tmp_list = list()

    for note in note_list:
        tmp[note['updated_at']] = note

    for k in sorted(tmp.keys()):
        tmp_list.append(tmp[k])

    return tmp_list


def filter_list(note_list, filter_dict, all_filter='All'):
    note_filter_list = list()
    kws = list()

    if filter_dict.get('sender'):
        senders = ['{}{}'.format(Config.keywordPrefix['sender'], a['name']) for a in filter_dict['sender'] if not a == all_filter]
        kws.extend(senders)
    if filter_dict.get('receiver'):
        receivers = ['{}{}'.format(Config.keywordPrefix['receiver'], a['name']) for a in filter_dict['receiver'] if not a == all_filter]
        kws.extend(receivers)
    if filter_dict.get('status'):
        statuses = ['{}{}'.format(Config.keywordPrefix['status'], a) for a in filter_dict['status'] if not a == all_filter]
        kws.extend(statuses)

    for note in note_list:
        keyword_str = note['keyword']

        if all(a in keyword_str for a in kws):
            note_filter_list.append(note)

    return note_filter_list


def fetch_notes(projectName, entityName):
    """ get note from selected project and entity name """
    project = get_project(projectName)
    filters = [['project', 'is', project], ['note_links', 'name_is', entityName]]
    notes = sg.find('Note', filters, DataField.note)
    return notes


def fetch_notes2(project, entity): 
    filters = [['project.Project.name', 'is', project], ['note_links', 'is', entity]]
    notes = sg.find('Note', filters, DataField.note)
    return notes    


def fetch_notes_information(projectName, entityName, brief=False):
    global versionIdCaches
    global taskCaches
    noteDict = OrderedDict()
    if not brief:
        notes = fetch_notes(projectName, entityName)
    else:
        notes = fetch_brief_notes(projectName, entityName)

    for note in notes:
        entities = note.get('note_links')
        steps = []
        tasks = []
        versions = []
        for entity in entities:
            if entity.get('type') == 'Version':
                if not entity.get('id') in versionIdCaches.keys():
                    versionEntity = sg.find_one('Version', [['id', 'is', entity.get('id')]], ['sg_task', 'sg_task.Task.step', 'code'])
                    versionIdCaches[versionEntity.get('id')] = versionEntity
                versionEntity = versionIdCaches[entity.get('id')]
                step = versionEntity.get('sg_task.Task.step')
                steps.append(step)
                task = versionEntity.get('sg_task')
                tasks.append(task)
                versions.append(versionEntity)
            if entity.get('type') == 'Task':
                tasks.append(entity)
                if not entity.get('id') in taskCaches.keys():
                    taskEntity = sg.find_one('Task', [['id', 'is', entity.get('id')]], ['step', 'code'])
                    taskCaches[taskEntity['id']] = taskEntity
                taskEntity = taskCaches[entity['id']]
                step = taskEntity.get('step')
                steps.append(step)

        userImage = get_user_image(note.get('user').get('id'))
        note['user']['path'] = userImage

        noteDict[note['id']] = {'note': note, 'steps': steps, 'attachments': note.get('attachments'), 'tasks': tasks, 'versions': versions}

    return noteDict


def fetch_brief_notes(projectName, entityName, extraFilters=[]):
    """ get note from selected project and entity name """
    project = get_project(projectName)
    filters = [['project', 'is', project], ['note_links', 'name_is', entityName], ['sg_note_type', 'is', 'Brief']]
    filters += extraFilters
    notes = sg.find('Note', filters, DataField.note)
    return notes


def get_note(id):
    return sg.find_one('Note', [['id', 'is', id]], DataField.note)


def fetch_versions(project, entityType, entityName):
    """ find version linked to Asset or Shot """
    import time
    global versionCaches
    if not entityName in versionCaches.keys():
        logger.debug('Fetch version from shotgun')
        projectEntity = get_project(project)
        entity = get_entity(project, Config.sgEntity.get(entityType), entityName)
        filters = [['project', 'is', projectEntity], ['entity', 'is', entity]]
        versions = sg.find('Version', filters, DataField.version)
        versionCaches[entityName] = versions
    return versionCaches[entityName]


def fetch_links(project, entityType, entityName):
    projectEntity = get_project(project)
    entity = get_entity(project, Config.sgEntity.get(entityType), entityName)

    # if asset, find Shot which has links to asset
    # if scene, find Asset which has links to shot
    filters = [['project', 'is', projectEntity], [Config.attrMap.get(entityType), 'is', entity]]
    results = sg.find(Config.sgSearchMap.get(entityType), filters, DataField.entityMap[entityType])
    return results


def fetch_tasks(project, entityType, entityName, step=''):
    # key cache
    key = '{}-{}-{}'.format(project, entityName, step)
    if key in taskStepCaches.keys():
        tasks = taskStepCaches[key]

    else:
        # fetch tasks
        projectEntity = get_project(project)
        entity = sg.find_one(Config.sgEntity[entityType], [['project', 'is', projectEntity], ['code', 'is', entityName]], DataField.entity)
        filters = [['project', 'is', projectEntity], ['entity', 'is', entity]]
        filters.append(['step.Step.code', 'is', step]) if step else None
        tasks = sg.find('Task', filters, DataField.task)

        # set caches
        taskStepCaches[key] = tasks
    return tasks

def get_attachment(id):
    return sg.find_one('Attachment', [['id', 'is', id]], DataField.attachment)


def download_attachment(attachmentEntity, dst):
    return sg.download_attachment(attachmentEntity, file_path=dst)


# def upload_attachments(noteId, files):
#     results = []
#     for file in files:
#         path = file.replace('/', '\\')
#         result = sg.upload('Note', noteId, path, 'attachments')
#         results.append(result)
#     return results


def publish_daily(entity, taskEntity, files, description):
    """ wrapper submit daily to sg """
    from rf_app.publish.asset import sg_hook
    print('version ...', entity.context.publishVersion)
    status = 'rev'
    user = user_info.User()
    userEntity = user.sg_user()
    try:
        versionResult = sg_hook.publish_daily(entity, taskEntity, status, files, userEntity, description)
        taskResult = sg_hook.update_task(entity, taskEntity, status)
        return True, versionResult
    except Exception as e:
        return False, e

def create_note(project, entityType, entityName, subject, content, step, user, noteType, status):
    """ create notes """
    projectEntity = get_project(project)
    entity = sg.find_one(Config.sgEntity[entityType], [['project', 'is', projectEntity], ['code', 'is', entityName]], DataField.entity)
    tasks = sg.find('Task', [['project', 'is', projectEntity], ['entity', 'is', entity], ['step.Step.code', 'is', step]], ['content', 'id'])
    noteLinks = [entity] + tasks if tasks else [entity]
    data = {'project': projectEntity,
            'note_links': noteLinks,
            'subject': subject,
            'content': content,
            'user': user,
            'sg_note_type': noteType,
            'sg_status_list': status
            }
    return sg.create('Note', data)

def update_note(noteId, status, userEntity):
    """ update note """
    data = {'sg_status_list': status, 'sg_modified_by': userEntity}
    return sg.update('Note', noteId, data)

def update_brief_content(briefId, content, userEntity):
    data = {'content': content, 'sg_modified_by': userEntity}
    return sg.update('Note', briefId, data)

def get_image(url):
    data = urlopen(url).read()
    return data

def download_thumbnail(url, dst):
    dirname = os.path.dirname(dst)
    if not os.path.exists(dirname):
        os.makedirs(dirname)

    urlretrieve(url, dst)

    if os.path.exists(dst):
        return dst

def get_project(projectName):
    global projectCaches
    if not projectName in projectCaches.keys():
        projectEntity = sg.find_one('Project', [['name', 'is', projectName]], DataField.project)
        projectCaches[projectName] = projectEntity
    return projectCaches[projectName]

def get_entity(projectName, entityType, entityName):
    global entityCaches
    if not entityName in entityCaches.keys():
        projectEntity = get_project(projectName)
        filters = [['project', 'is', projectEntity], ['code', 'is', entityName]]
        entity = sg.find_one(entityType, filters, DataField.entity)
        entityCaches[entityName] = entity
    return entityCaches[entityName]


# =========================================================

# use by Note Manager

class Keyword:
    prefix = {
            'project': '$',
            'sender': '#',
            'receiver': '@',
            'step': '!',
            'status': '&',
            'task': '*'}

    separator = ':'


def fetch_notes_tickets(user_entity=None, step_entity=None):
    """
    1.
    Note / `Ticket send to me
    Note on my Task
    Note on my Asset/Shot
    2.
    Note / Ticket send to my team
    Note on my Team task
    3.
    Ticket send to me
    """
    def add_entity_tag(entities, data):
        for entity in entities:
            entity.update(data)
        return entities

    # hard code for now (testing purpose)
    active_projects = ['Hanuman', 'Bikey', 'Destiny2']
    projects = [a for a in sg.find('Project', [], ['name']) if a['name'] in active_projects]

    print('fetching tickets ...')
    tickets = find_tickets(user_entity)
    print('tickets {}'.format(len(tickets)))
    print('fetching notes ...')
    notes = find_notes(projects, user_entity=user_entity, step_entity=step_entity)
    print('notes {}'.format(len(notes)))

    # these entities do not calculate in filter
    print('fetching parent tickets ...')
    parent_tickets = find_parent_tickets(notes)
    print('p ticket {}'.format(len(parent_tickets)))

    print('fetching children notes ...')
    children_notes = find_children_notes(tickets)
    print('c notes {}'.format(len(children_notes)))

    entities = add_keywords(tickets) + add_keywords(notes)
    link_entities = parent_tickets + children_notes
    # entities = add_keywords(notes)
    print('total {}'.format(len(entities)))
    print('complete')

    entities = add_decoder(entities)
    link_entities = add_decoder(link_entities)

    return [entities, link_entities]


def add_keywords(entities):
    new_list = list()
    for entity in entities:
        if entity['type'] == 'Note':
            project = entity['project']
            receivers = entity['addressings_to'] + entity['addressings_cc']
            sender = entity['user']
            tasks = entity['tasks'] if entity['tasks'] else [{'name': 'No Task'}]
            status = entity['sg_status_list']

        elif entity['type'] == 'Ticket':
            project = entity['project']
            receivers = entity['addressings_to'] + entity['addressings_cc']
            sender = entity['created_by']
            tasks = [{'name': 'ticket'}]
            status = entity['sg_status_list']

        keywords = list()
        if project:
            project_kw = '{}{}'.format(Keyword.prefix['project'], project['name'])
            keywords.append(project_kw)

        if sender:
            sender_kw = '{}{}'.format(Keyword.prefix['sender'], sender['name'])
            keywords.append(sender_kw)

        if status:
            status_kw = '{}{}'.format(Keyword.prefix['status'], status)
            keywords.append(status_kw)

        if receivers:
            kws = list()
            for receiver in receivers:
                kw = '{}{}'.format(Keyword.prefix['receiver'], receiver['name'])
                kws.append(kw)
            receiver_kw = Keyword.separator.join(kws)
            keywords.append(receiver_kw)

        if tasks:
            kws = list()
            for task in tasks:
                kw = '{}{}'.format(Keyword.prefix['task'], task['name'])
                kws.append(kw)
            task_kw = Keyword.separator.join(kws)
            keywords.append(task_kw)

        keyword = Keyword.separator.join(keywords)
        entity['keyword'] = keyword
        new_list.append(entity)

    return new_list


def add_decoder(entities): 
    # for entity in entities: 
    #     if entity['type'] in ['Note', 'Reply']: 
    #         entity['content'] = entity['content'].encode('utf-8').decode(encoding='utf-8')

    #     if entity['type'] == 'Ticket': 
    #         description = entity.get('description')
    #         if description: 
    #             entity['description'] = description.encode('utf-8').decode(encoding='utf-8')

    return entities


def find_parent_tickets(notes):
        # find parent ticket
    parent_tickets = list()
    tickets = list()
    for note in notes:
        tks = note['ticket_sg_note_tickets']
        if tks:
            parent_tickets += [a['id'] for a in tks]

    if parent_tickets:
        filters = [['id', 'is', a] for a in parent_tickets]
        advance_filters = [
            {
                "filter_operator": "any",
                "filters": filters
            }
            ]
        tickets = sg.find('Ticket', advance_filters, DataField.ticket) or list()
    return tickets


def find_children_notes(tickets):
    children_notes = list()
    notes = list()
    for ticket in tickets:
        notes = ticket['sg_note']
        children_notes += [a['id'] for a in notes]

    if children_notes:
        filters = [['id', 'is', a] for a in children_notes]
        advance_filters = [
            {
                "filter_operator": "any",
                "filters": filters
            }
            ]
        notes = sg.find('Note', advance_filters, DataField.note) or list()
    return notes


def find_tickets(user_entity):
    filters = [
        {
            "filter_operator": "any",
            "filters": [
                ['addressings_to', 'is', user_entity],
                ['addressings_cc', 'is', user_entity],
                ['created_by', 'is', user_entity],
                ['updated_by', 'is', user_entity],
                ]
        }
        ]
    tickets = sg.find('Ticket', filters, DataField.ticket)
    return tickets


def find_notes(projects, user_entity=None, step_entity=None):
    # compare
    filters = list()
    if user_entity:
        advance_filter1 = {
            "filter_operator": "any",
            "filters": [
                ['tasks.Task.task_assignees', 'is', user_entity],
                ['addressings_to', 'is', user_entity],
                ['addressings_cc', 'is', user_entity]]
        }
        filters.append(advance_filter1)

    if step_entity:
        filters.append(['tasks.Task.step', 'is', step_entity])
    if projects:
        advance_filter2 = {
            "filter_operator": "any",
            "filters": [['project', 'is', a] for a in projects]
        }
        filters.append(advance_filter2)

    notes = sg.find('Note', filters, DataField.note)
    return notes


def find_tasks(projects, user_entity=None, step_entity=None):
    filters = list()
    if user_entity:
        filters.append(['task_assignees', 'is', user_entity])
        filters.append(['sg_status_list', 'is_not', 'omt'])
    if step_entity:
        filters.append(['step', 'is', step_entity])
    if projects:
        project_filters = [['project', 'is', a] for a in projects]
        advance_filters = {
            "filter_operator": "any",
            "filters": project_filters
        }

        filters.append(advance_filters)

    tasks = sg.find('Task', filters, DataField.note)
    return tasks


def get_note_thread(note_entity, attachment_dict):
    threads = list()
    if note_entity['type'] == 'Note':
        threads = sg.note_thread_read(note_entity['id'])
        threads[0].update(note_entity)
    if note_entity['type'] == 'Ticket':
        threads.append(note_entity)

        for file in note_entity['attachments']:
            entity = attachment_dict[file['id']]
            file.update(entity)
            threads.append(file)

    threads = add_decoder(threads)
    return threads


def create_note2(project_entity, entities=[], versions=[], content='', created_by=None, task_user=None, cc_user=None, status='opn', note_type='Fix', ticket_entity=None): 
    tasks = sg.find('Task', [['project', 'is', project_entity], ['entity', 'is', entity], ['step.Step.code', 'is', step]], ['content', 'id'])
    note_links = entities + versions
    data = {'project': project_entity,
            'note_links': note_links,
            # 'subject': subject,
            'content': content,
            'tasks': tasks, 
            'user': created_by,
            'addressings_to': task_user, 
            'addressings_cc': cc_user,
            'sg_note_type': note_type,
            'sg_status_list': status, 
            'ticket_sg_note_tickets': ticket_entity
            }
    return sg.create('Note', data)


def add_entity_to_note(note_entity):
    """ add entity and tasks due date to note """ 
    tasks = note_entity.get('tasks')
    if tasks: 
        advance_filters = [
                {
                    "filter_operator": "any",
                    "filters": [['id', 'is', a['id']] for a in tasks]
                }]
        fields = ['entity', 'due_date']
        task_entities = sg.find('Task', advance_filters, fields)

        for task_new_data in task_entities: 
            for task in tasks: 
                if task_new_data['id'] == task['id']: 
                    task.update(task_new_data)
            # pick 1
            task = task_entities[0]
            note_entity['entity'] = task.get('entity')
    return note_entity


def get_file_attachments(note_entity):
    attachment_dict = dict()
    attachments = note_entity['attachments']
    file_ids = [['id', 'is', a['id']] for a in attachments] if attachments else []
    if file_ids:
        filters = [{
                "filter_operator": "any",
                "filters": file_ids
            }]
        file_entities = sg.find('Attachment', filters, DataField.attachment)
        for file in file_entities:
            attachment_dict[file['id']] = file

    return attachment_dict


def get_user_info(threads):
    user_dict = dict()
    for thread in threads:
        user = dict()

        try_attr = ['user', 'created_by']

        for attr in try_attr: 
            user = thread.get(attr)
            if user:
                if user['type'] == 'HumanUser':
                    uid = user['id']
                if user['type'] == 'ClientUser':
                    uid = 'c{}'.format(user['id'])
                if user['type'] == 'ApiUser': 
                    uid = 'a{}'.format(user['id'])

                if uid in user_dict.keys():
                    user_dict[uid].update(user)
                else:
                    user_dict[uid] = user

    # find user without image
    download_ids = list()
    download_cids = list()

    for uid, user in user_dict.items():
        if not 'image' in user.keys():
            if user['type'] == 'HumanUser':
                download_ids.append(uid)
            if user['type'] == 'ClientUser':
                download_cids.append(uid)

    if download_ids:
        fields = ['image']
        advance_filters = [
            {
                "filter_operator": "any",
                "filters": [['id', 'is', a] for a in download_ids]
            }]
        user_entities = sg.find('HumanUser', advance_filters,  fields)

        for user in user_entities:
            user_dict[user['id']].update(user)

    if download_cids:
        fields = ['image']
        advance_filters = [
            {
                "filter_operator": "any",
                "filters": [['id', 'is', int(a[1:])] for a in download_cids]
            }]
        cuser_entities = sg.find('ClientUser', advance_filters,  fields)

        for user in cuser_entities:
            user_dict['c{}'.format(user['id'])].update(user)

    return user_dict


def add_reply(note_entity, content, user_entity, attachments=[]): 
    result_list = list()
    if content: 
        data = {
            'user': user_entity, 
            'entity': note_entity, 
            'content': content
            }
        result = sg.create('Reply', data)
        result_list.append(result)
        
    if attachments: 
        result2 = upload_attachments(note_entity['id'], attachments)
        result_list += result2 

    return result_list


def upload_attachments(note_id, files): 
    results = []
    for file in files: 
        path = file.replace('/', '\\')
        result = sg.upload('Note', note_id, path, 'attachments')
        results.append(result)
    return results 


def create_ticket(project_entity, title, description, addressings_to, entity=None, created_by=None, addressings_cc=None, priority=None, sg_ticket_type=None, attachments=None): 
    data = {
        'title': title, 'project': project_entity, 
        'description': description, 'addressings_to': addressings_to, 
        'created_by': created_by
        }
    if created_by: 
        data.update({'created_by': created_by})
    if addressings_cc: 
        data.update({'addressings_cc': addressings_cc})
    if priority: 
        data.update({'sg_priority': priority})
    if sg_ticket_type: 
        data.update({'sg_ticket_type': sg_ticket_type})
    if entity: 
        data.update({'sg_link': entity})

    ticket = sg.create('Ticket', data)
    if attachments: 
        for file in attachments: 
            path = file.replace('/', '\\')
            result = sg.upload('Ticket', ticket['id'], path, 'attachments')

    return ticket


def get_user_entities(user_list): 
    user_entities = list()
    user_dict = dict()
    users = sg.find('HumanUser', [], ['sg_name'])

    for user in users: 
        user_dict[user['sg_name'].lower()] = user 

    for user in user_list: 
        # print(user, user_dict.keys())
        if user.lower() in user_dict.keys(): 
            user_entities.append(user_dict[user])
        else: 
            print('user {} not found.'.format(user))

    return user_entities


def get_field_list_value(entity_type, field_name): 
    return sg.schema_field_read(entity_type)[field_name]['properties']['valid_values']['value']


# ===================================================================================================
# user by art task manager 

def fetch_entity_notes_briefs(project, sg_entity, task_filters=[], fields=DataField.note): 
    """ fetch notes within entity 
    Args: 
        project (str): name of project 
        entity_name (str): name of asset / shot 
        entity_type (str): Asset or Shot
        task_filters (list): filter list of task entities if None return all

    Returns: dict()
    {'color': [note1, note2], 'sketch: [note3, note4]', 'entity': [note5, note6]}
    """ 

    notes = fetch_entity_notes(project, sg_entity, task_filters, fields)
    note_brief_dict = dict()
    link_versions = list()
    version_dict = dict()

    # find related version 
    for note in notes: 
        note_links = note['note_links']
        for link in note_links: 
            if link['type'] == 'Version': 
                link_versions.append(link['id'])

    if link_versions: 
        filters = [['project.Project.name', 'is', project]]
        version_filters = [['id', 'is', a] for a in link_versions]
        advance_filters = {
                            "filter_operator": "any",
                            "filters": version_filters
                        }
        filters.append(advance_filters)
        versions = sg.find('Version', filters, DataField.version)
        for v in versions: 
            version_dict[v['id']] = v
    
    # note section
    task_filter_names = [t['content'] for t in task_filters]
    for note in notes: 
        note[Attr.note_type_attr] = Value.note
        tasks = note['tasks'] # task from here has no data. need to fetch separately 
        note_links = note['note_links'] # this will be both entity and version 
        note_link_detail = list() 

        for link in note_links: 
            if link['type'] == 'Version': 
                if link['id'] in version_dict: 
                    rich_version = version_dict[link['id']]
                    note_link_detail.append(rich_version)
                else: 
                    note_link_detail.append(link)
            else: 
                note_link_detail.append(link)
        note['note_links'] = note_link_detail

        if tasks: 
            for task in tasks: 
                taskname = task['name']
                allow = True 
                if task_filter_names: 
                    allow = True if taskname in task_filter_names else False 

                if allow: 
                    if taskname in note_brief_dict.keys(): 
                        note_brief_dict[taskname].append(note)
                    else: 
                        note_brief_dict[taskname] = [note]
        else: 
            # this is entity level note 
            taskname = 'entity'
            if taskname in note_brief_dict.keys(): 
                note_brief_dict[taskname].append(note)
            else: 
                note_brief_dict[taskname] = [note]

    return note_brief_dict

def fetch_client_approved_versions(project, sg_entity, task_filters=[], fields=DataField.version): 
    filters = [['project.Project.name', 'is', project], 
                ['entity', 'is', sg_entity], 
                ['client_approved', 'is', True]]
    if task_filters:
        filters.append(['sg_task', 'in', task_filters])
    apr_versions = sg.find('Version', filters, fields)
    results = OrderedDict()
    for version in apr_versions: 
        task = version.get('sg_task')
        if task: 
            version[Attr.note_type_attr] = Value.client_approved_version
            taskname = task['name']
            if taskname not in results: 
                results[taskname] = []
            results[taskname].append(version)

    return results   

def fetch_entity_notes(project, sg_entity, task_filters=[], fields=DataField.note): 
    filters = [['project.Project.name', 'is', project], ['note_links', 'is', sg_entity]]
    notes = sg.find('Note', filters, fields)
    if task_filters:
        task_filter_names = [t.get('content') for t in task_filters]
        filtered_notes = []
        for note in notes:
            tasks = note.get('tasks')
            if not tasks:
                filtered_notes.append(note)
            else:
                for task in tasks:
                    if task.get('name') in task_filter_names:
                        filtered_notes.append(note)
        notes = filtered_notes
    return notes    

def test():
    from datetime import datetime

    # hard code for now (testing purpose)
    active_projects = ['Hanuman', 'Bikey', 'Destiny2']
    projects = [a for a in sg.find('Project', [], ['name']) if a['name'] in active_projects]
    user_entity = sg.find_one('HumanUser', [['sg_name', 'is', 'KenR']], ['sg_name'])

    start = datetime.now()
    print('start')
    tickets = find_tickets(user_entity)
    print('find_tickets {} {}'.format(((datetime.now() - start), len(tickets))))

    # find task for specific user
    step_entity = sg.find_one('Step', [['short_name', 'is', 'rig']], ['short_name', 'entity_type'])
    note_start = datetime.now()
    notes = find_notes(projects, user_entity=user_entity, step_entity=None)
    print('find_notes {} {}'.format(((datetime.now() - note_start), len(notes))))
    print('Note that on {} tasks {}'.format(user_entity['sg_name'], len(notes)))

    notes = find_notes(projects, user_entity=None, step_entity=step_entity)
    print('Note that on step {} {}'.format(step_entity['short_name'], len(notes)))



def test_brief_note(): 
    task_filters = []
    data = fetch_entity_notes_briefs('Hanuman', 'testProp2', 'Asset', task_filters)

    for task, entities in data.items(): 
        print(task)
        for entity in entities: 
            print('\t{} {}'.format(entity['note_type'], entity))


# from rf_utils.sg import sg_note
# reload(sg_note)
# from rf_utils import user_info

# entity = sg_note.sg.find_one('Asset', [['project.Project.name', 'is', 'Bikey'], ['code', 'is', 'bverDroneA']], ['code', 'sg_notes'])
# briefs = sg_note.fetch_brief('Bikey', entity)

# a = sg_note.get_attachment(briefs['entity']['attachments'][0]['id'])
# sg_note.download_thumbnail(a['image'], 'D:/test_thumbnail.png')
# sg_note.download_attachment(briefs['entity']['attachments'][0], 'D:/test_thumbnail2.png')

# sg_note.find_link_entity('Bikey', 'Asset', 'bverDroneA', 'model')
# project = 'Bikey'
# link_entity = sg_note.find_link_entity('Bikey', 'Asset', 'bverDroneA', 'rig')
# content = 'Test brief for rig'
# file = r"C:\Users\Ta\Downloads\5ce3d026021b4c39493f3427.png"
# user = user_info.User(nickname='TA')
# user = user.sg_user()
# sg_note.create_brief(project, link_entity, content, user, attachments=[file])

# patch briefs
# from rf_utils.sg import sg_note
# sg = sg_note.sg 

# notes = sg.find('Note', [['project.Project.name', 'is', 'Hanuman'], ['sg_note_type', 'is', 'Brief']], ['sg_brief', 'note_links'])
# print len(notes)

# for note in notes: 
#     link = note['sg_brief']
#     note_link = note['note_links']
#     if link and not note_link: 
#         if link['type'] in ['Asset', 'Shot']: 
#             data = {'note_links': [link]}
#             sg.update('Note', note['id'], data)
#             print 'update', data
#         if link['type'] == 'Task': 
#             task = sg.find_one('Task', [['id', 'is', link['id']]], ['entity']) 
#             entity = task['entity']
#             print 'entity', entity
#             data = {'note_links': [entity], 'tasks': [link]}
#             sg.update('Note', note['id'], data)
