from rf_utils.sg import sg_process
sg = sg_process.sg 


def run(project): 
    project = sg.find_one('Project', [['name', 'is', project]], ['name'])
    project_filter = [['project', 'is', project]]
    assets = sg.find('Asset', project_filter, ['code', 'sg_asset_type', 'sg_subtype'])
    print('Total Asset {}'.format(len(assets)))

    chars = list()
    props = list() 
    sets = list()

    for asset in assets: 
        if asset['sg_asset_type'] == 'char': 
            chars.append(asset)
        if asset['sg_asset_type'] == 'prop': 
            props.append(asset)
        if asset['sg_asset_type'] == 'set': 
            sets.append(asset)

    print('\t- Chars {}'.format(len(chars)))
    print('\t- Props {}'.format(len(props)))
    print('\t- Sets {}'.format(len(sets)))

    shot_filters = [['project', 'is', project], ['sg_status_list', 'is_not', 'omt']]
    shots = sg.find('Shot', shot_filters, ['code'])

    print('Total Shots {}'.format(len(shots)))

    steps = sg.find('Step', [], ['entity_type', 'short_name'])

    asset_steps = list()
    shot_steps = list() 
    asset_filters = ['design', 'model', 'texture', 'rig', 'groom', 'lookdev', 'anim']
    shot_filters = ['layout', 'anim', 'fx', 'sim', 'finalcam', 'setdress', 'light', 'comp', 'qc', 'techanim', 'colorscript']

    for step in steps: 
        if step['entity_type'] == 'Asset': 
            if step['short_name'] in asset_filters: 
                asset_steps.append(step)

        if step['entity_type'] == 'Shot': 
            if step['short_name'] in shot_filters: 
                shot_steps.append(step)
    
    step_dict = {'Asset': asset_steps, 'Shot': shot_steps}
    asset_tasks = list()
    shot_tasks = list()
    total_tasks = list()
    task_dict = {'Asset': asset_tasks, 'Shot': shot_tasks}

    for key in step_dict.keys(): 
        task_filters = [['project', 'is', project], ['sg_status_list', 'is_not', 'omt']]
        advancedFilter = {
                            "filter_operator": "any",
                            "filters": [['step', 'is', a] for a in step_dict[key]]
                        }
        task_filters.append(advancedFilter)
        tasks = sg.find('Task', task_filters, ['content', 'sg_type', 'step'])
        task_dict[key] = tasks
        total_tasks += tasks

    print('Total Task {}'.format(len(total_tasks)))
    print('\t - Asset Tasks {}'.format(len(task_dict['Asset'])))
    print('\t - Shot Tasks {}'.format(len(task_dict['Shot'])))

    asset_versions = list()
    shot_versions = list() 
    version_dict = {'Asset': asset_versions, 'Shot': shot_versions}

    version_filters = [['project', 'is', project]]
    versions = sg.find('Version', version_filters, ['entity', 'task'])

    for version in versions: 
        if version['entity']: 
            if version['entity']['type'] == 'Asset': 
                asset_versions.append(version)
            if version['entity']['type'] == 'Shot': 
                shot_versions.append(version)

    print('Total Versions {}'.format(len(versions)))
    print('\t - Asset versions {}'.format(len(asset_versions)))
    print('\t - Shot versions {}'.format(len(shot_versions)))

    notes = sg.find('Note', project_filter, [])
    print('Total Notes {}'.format(len(notes)))

