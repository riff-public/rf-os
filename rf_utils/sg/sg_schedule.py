import sys
import os
import platform
import logging
from datetime import datetime
import unittest

logger = logging.getLogger(__name__)

if os.environ.get('RFSCRIPT', False):
    path = '%s/core' % os.environ.get('RFSCRIPT')
    if not path in sys.path:
        sys.path.append(path)

import rf_config as config
from shotgun_api3 import Shotgun
from shotgun_api3 import shotgun
from shotgun_api3 import ShotgunError

# connection to server
server = config.Shotgun.server
script = config.Shotgun.script
id = config.Shotgun.id
sg = None
ca_certs = '{}/{}'.format(os.path.dirname(shotgun.__file__.replace('\\', '/')), config.Shotgun.ca_certs)
error = ShotgunError

try:
    sg = Shotgun(server, script, id, ca_certs=ca_certs)
    connection = True
except shotgun.SSLHandshakeError as e:
    logger.error(e)
    connection = False

def init_key(inputKey):
    script, id = config.get_key(inputKey)
    global sg
    sg = Shotgun(server, script, id, ca_cert=ca_cert)


def get_studio_schedule(start, end):
    start_date = datetime(*start).date().strftime('%Y-%m-%d')
    end_date = datetime(*end).date().strftime('%Y-%m-%d')
    result = sg.work_schedule_read(start_date, end_date)
    filters = ['STUDIO_EXCEPTION']
    data = dict()

    for date, value in result.items():
        if value['reason'] in filters:
            data[date] = value

    return data


def add_studio_holiday(date, description):
    date = datetime(*date).date().strftime('%Y-%m-%d')
    return sg.work_schedule_update (
                    date,
                    working=False,
                    description=description,
                    project=None,
                    user=None, recalculate_field=None)


def get_leave_info(user, start=None, end=None):
    user_entity = get_user(user)
    filters = [
                ['user', 'is', user_entity],
                ['vacation', 'is', True],
                ]
    if start and end: 
        start_date = datetime(*start).date()
        end_date = datetime(*end).date()
        filters.append(['start_date', 'between', [start_date, end_date]])

    fields = ['sg_leave_type', 'start_date', 'end_date', 'user', 'note', 'id', 'sg_duration']
    return sg.find('Booking', filters, fields)

def get_all_leave_info():
    from rf_utils import user_info
    all_user = user_info.SGUser()
    list_user = all_user.userEntities
    user_list = []
    for user in list_user:
        filters = [['user', 'is', user],['vacation', 'is', True]]
        fields = ['sg_leave_type', 'start_date', 'end_date', 'user', 'note', 'id', 'sg_duration']
        data = sg.find('Booking', filters, fields)
        user_list.append(data)
        
    return user_list


def assign_user_leave(user, start, end, duration, leave_type, note=''):
    """
        Args:
            leave_type (str): Personal leave, Sick leave, Absent, Vacation
    """
    user_entity = get_user(user)
    start_date = datetime(*start).date()
    end_date = datetime(*end).date()
    if user_entity:
        intersect_date = check_intersect_date(user, start, end)
        if not intersect_date: 
            data = {
                    'user': user_entity,
                    'start_date': start_date,
                    'end_date': end_date,
                    'sg_leave_type': leave_type,
                    'note': note,
                    'vacation': True, 
                    'sg_duration': duration, 
                    }
            return sg.create('Booking', data)

        elif intersect_date: 
            data = {
                    'user': user_entity,
                    'start_date': start_date,
                    'end_date': end_date,
                    'sg_leave_type': leave_type,
                    'note': note,
                    'vacation': True, 
                    'sg_duration': duration, 
                    }
            return sg.create('Booking', data)

        else: 
            logger.warning('You already taken leave on selected date {}'.format(intersect_date))


def remove_user_leave(entity_id): 
    return sg.delete('Booking', entity_id)


def check_intersect_date(user, start, end): 
    """ check if that duration intersect with another leave """ 
    start_date = datetime(*start).date()
    end_date = datetime(*end).date()
    info = get_leave_info(user, start=None, end=None)

    for entry in info: 
        start = entry['start_date']
        end = entry['end_date']

        if is_inbetween(start_date, start, end): 
            return entry 
        if is_inbetween(end_date, start, end): 
            return entry
        if is_inbetween(start, start_date, end_date): 
            return entry
        if is_inbetween(end, start_date, end_date): 
            return entry

def is_inbetween(check_date, start_date, end_date): 
    check_date = to_datetime(check_date)
    start_date = to_datetime(start_date)
    end_date = to_datetime(end_date)
    return True if start_date <= check_date <= end_date else False 
         

def get_user(name):
    from rf_utils import user_info
    user = user_info.User(sg=sg, nickname=name)
    return user.sg_user()


def to_datetime(date): 
    if isinstance(date, str): 
        return datetime.strptime(date, '%Y-%m-%d').date()
    return date

 
def test(): 
    print('Add studio holiday')
    year = 2021
    date = (2021, 6, 4)
    description = 'Test vacation'
    result = add_studio_holiday(date, description)
    print(result)

    print('Get studio holiday')
    result = get_studio_schedule((2021, 6, 4), (2021, 6, 4))
    
    for date, data in result.items(): 
        print(date)
        for k, v in data.items(): 
            print(k, v)

    print('\n')

    print('Assign artist leave date')
    user = 'TA'
    start = (2021, 6, 4)
    end = (2021, 6, 4)
    leave_types = ['personal', ' sick', 'absent', 'vacation']
    minute = 480

    assign_result = assign_user_leave(user, start, end, duration=minute, leave_type=leave_types[0], note='Sick of work')
    print(assign_result)

    print('\n')

    print('Get artist leave info')
    result = get_leave_info(user, start=None, end=None)

    for data in result: 
        for k, v in data.items(): 
            print(k, v)

    print('Remove leave entry')
    entity_id = assign_result['id']
    result = remove_user_leave(entity_id)

    print('\n')

    if result: 
        print('Remove {} success'.format(entity_id))

    # check result 
    # https://riffanimation.shotgunstudio.com/page/crew_planning_app#

    # 2021-08-12
    # ('reason', 'STUDIO_EXCEPTION')
    # ('description', None)
    # ('working', False)



