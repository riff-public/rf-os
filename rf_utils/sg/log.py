import sg_utils
from .. import user_info
reload(user_info)
from datetime import datetime
from datetime import timedelta
from collections import OrderedDict

sg = sg_utils.sg 

def active_users(): 
    user = user_info.SGUser()
    return [a for a in user.userEntities if a['sg_status_list'] == 'act']


def find_user(name): 
    return sg.find_one('HumanUser', [['name', 'is', name]], user_info.Data.returnFields)


def login_events(filterDate='today'): 
    """ date args : 'Nov 16 2020' """ 
    filters = [['event_type', 'is', 'Shotgun_User_Login']]
    fields = ['event_type', 'created_at', 'user', 'meta']
    if filterDate == 'today': 
        date_range = [datetime.today(), datetime.combine(date.today(), datetime.min.time()) - timedelta(0)]
    else: 
        date_ref = datetime.strptime(filterDate, '%b %d %Y')
        date_range = [datetime.today(), date_ref]

    filters.append(['created_at', 'between', date_range])
    return sg.find('EventLogEntry', filters, fields)


def rv_events(filterDate=True): 
    fields = ['event_type', 'created_at', 'user', 'meta']
    filters = [['event_type', 'is', 'SG_RV_Session_Validate_Success']]
    if filterDate == 'today': 
        date_range = [datetime.today(), datetime.combine(date.today(), datetime.min.time()) - timedelta(0)]
    else: 
        date_ref = datetime.strptime(filterDate, '%b %d %Y')
        date_range = [datetime.today(), date_ref]
    
    filters.append(['created_at', 'between', date_range])
    return sg.find('EventLogEntry', filters, fields)    


def activity_data(activityType='login', filterDate='today'): 
    # filterDate ='Nov 16 2020' if not filterDate else filterDate
    group = OrderedDict()
    # if today: 
    #     referenceData = datetime.now().date()
    # else: 
    #     referenceData = datetime.strptime(date, '%b %d %Y').date()

    if activityType == 'login': 
        results = login_events(filterDate=filterDate)
    if activityType == 'rv': 
        results = rv_events(filterDate=filterDate)

    if results: 
        for event in results:
            createAt = event['created_at']
            # if createAt.date() >= referenceData: 
            # diff = (datetime.now().date() - createAt.date()).days
            # print diff
            user = event['user']['name']
            loginTime = createAt.strftime('%b-%d-%Y %H:%M:%S')
            day = createAt.date().strftime('%Y-%b-%d')

            if not user in group.keys(): 
                group[user] = [loginTime]
            else: 
                group[user].append(loginTime)

    return group

def activity_data_mac(activityType='login', filterDate='today'): 
    group = OrderedDict()

    if activityType == 'login': 
        results = login_events(filterDate=filterDate)
    if activityType == 'rv': 
        results = rv_events(filterDate=filterDate)

    if results: 
        for event in results:
            createAt = event['created_at']
            # diff = (datetime.now().date() - createAt.date()).days
            # print diff
            user = event['user']['name']
            mac = event['meta']['MAC'] + ' - ' + user
            loginTime = createAt.strftime('%b-%d-%Y %H:%M:%S')
            day = createAt.date().strftime('%Y-%b-%d')

            if not mac in group.keys(): 
                group[mac] = [loginTime]
            else: 
                group[mac].append(loginTime)

    return group


def login_summary_today(activityType='login', filterDate='today'): 
    actives = active_users()
    data = activity_data(activityType=activityType, filterDate=filterDate)

    print datetime.now().strftime('%b-%d-%Y %H:%M:%S')
    print '---------------------------'
    i = 1
    for user, times in data.items(): 
        display = [a.split(' ')[-1] for a in times]
        print '{i}.{user} loggedin {num} times - {times}'.format(i=i, user=user, num=len(times), times=times)
        i+=1

    activeUsers = [a['name'] for a in actives]
    print activeUsers

    neverLogin = [k for k in activeUsers if not k in data.keys()]
    print '---------------------------'
    print '{} users never login'.format(len(neverLogin))
    print neverLogin

def login_summary_today_mac(activityType='login', filterDate='today'): 
    actives = active_users()
    data = activity_data_mac(activityType=activityType, filterDate=filterDate)

    print datetime.now().strftime('%b-%d-%Y %H:%M:%S')
    print '---------------------------'
    i = 1
    for user, times in data.items(): 
        display = [a.split(' ')[-1] for a in times]
        print '{i}. Mac {user} loggedin {num} times - {times}'.format(i=i, user=user, num=len(times), times=times)
        i+=1

    activeUsers = [a['name'] for a in actives]
    print activeUsers

    neverLogin = [k for k in activeUsers if not k in data.keys()]
    print '---------------------------'
    print '{} users never login'.format(len(neverLogin))
    print neverLogin




