import os

import rf_config as config
from shotgun_api3 import Shotgun
from shotgun_api3 import shotgun

# connection to server
server = config.Shotgun.server
script = config.Shotgun.script
id = config.Shotgun.id
ca_certs = '{}/{}'.format(os.path.dirname(shotgun.__file__.replace('\\', '/')), config.Shotgun.ca_certs)

class ShotgunClient:
    def __init__(self, sessionToken=None):
        '''Shotgun API wrapper for all your stuffs
        To access the `shotgun_api3` instance directly at any stage, use the `instance` getter
        '''
        # Use existing session token
        if sessionToken:
            self._instance = Shotgun(
                base_url = server, 
                connect = False, 
                session_token = sessionToken
            )
            self._instance._connection = None

        # Brand new connection
        else:
            self._instance = Shotgun(
                base_url = server, 
                script_name = script, 
                api_key = id
            )

    @property
    def instance(self):
        '''Returns the current `shotgun_api3.Shotgun` instance'''
        return self._instance

    def clone(self):
        '''Clone current instance and return a pre-authenticated new copy'''
        return ShotgunClient(sessionToken=self.instance.config.session_token)
       