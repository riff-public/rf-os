import sys
import os
import platform
import logging

logger = logging.getLogger(__name__)

if os.environ.get('RFSCRIPT', False):
    path = '%s/core' % os.environ.get('RFSCRIPT')
    if not path in sys.path:
        sys.path.append(path)

import rf_config as config
from shotgun_api3 import Shotgun
from datetime import datetime

# connection to server
server = config.Shotgun.server
script = config.Shotgun.script
id = config.Shotgun.id
sg = Shotgun(server, script, id)
# sg = None

def init_key(inputKey):
    script, id = config.get_key(inputKey)
    global sg
    sg = Shotgun(server, script, id)

def add_shot_task_template(projectName, templateName, shotEntities=[], shotList=[]): 
	tasks = find_task_in_template(templateName)
	# tasks = sg.find('Task', [['task_template', 'is', template]], ['content', 'id', 'step'])
	
	if shotList: 
		shotEntities = find_shot_entities(projectName, shotList)

	for shotEntity in shotEntities: 
		print shotEntity

		# create task 
		for task in tasks: 
			data = {'project': shotEntity['project'], 'content': task['content'], 'step': task['step'], 'entity': shotEntity}
			taskEntity = sg.find_one('Task', [['project', 'is', project], ['entity', 'is', shotEntity], ['content', 'is', task['content']]], ['content', 'id'])
			if not taskEntity: 
				taskResult = sg.create('Task', data)
				print 'Task created', taskResult

def find_shot_sequence(projectName, sequenceCode=''): 
	return sg.find('Shot', [['project.Project.name', 'is', projectName], ['sg_sequence_code', 'is', sequenceCode]], ['code', 'id'])


def add_task_template(projectName, templateName, sequenceCode=''): 
	shots = find_shot_sequence('SevenChickMovie', sequenceCode)
	add_shot_task_template(projectName, templateName, shots)

def update_tasks(projectName, status, shotEntities=[], shotList=[], taskNames=[]): 
	if shotList: 
		shotEntities = find_shot_entities(projectName, shotList)

	for shotEntity in shotEntities: 
		for task in taskNames: 
			taskEntity = sg.find_one('Task', [['project.Project.name', 'is', projectName], ['content', 'is', task], ['entity', 'is', shotEntity]], ['content', 'id'])
			data = {'sg_status_list': status}
			if taskEntity: 
				sg.update('Task', taskEntity['id'], data)
				print 'update status %s %s' % (taskEntity, status)


def find_shot_entities(projectName, shotList): 
	shotEntities = []
	for shot in shotList: 
		shotEntity = sg.find_one('Shot', [['project.Project.name', 'is', projectName], ['code', 'is', shot]], ['code', 'id', 'project'])
		shotEntities.append(shotEntity)

	return shotEntities

def find_task_in_template(templateName): 
	taskTemplate = sg.find_one('TaskTemplate', [['code', 'is', templateName]], ['code', 'id'])
	tasks = sg.find('Task', [['task_template', 'is', taskTemplate]], ['content', 'id', 'step'])
	return tasks 

# example 
# add_task_template('SevenChickMovie', 'scm_outsource_task', 'q0040a')

tasks = sg_batch.find_task_in_template('scm_outsource_task')


# shotList = ['scm_act1_q0040a_s0200', 
# 'scm_act1_q0040a_s0100', 
# 'scm_act1_q0040a_s0116', 
# 'scm_act1_q0040a_s0120', 
# 'scm_act1_q0040a_s0170', 
# 'scm_act1_q0040a_s0180', 
# 'scm_act1_q0040a_s0190', 
# 'scm_act1_q0040a_s0200', 
# 'scm_act1_q0040a_s0240', 
# 'scm_act1_q0040a_s0270', 
# 'scm_act1_q0040a_s0310', 
# 'scm_act1_q0040a_s0330', 
# 'scm_act1_q0040a_s0340', 
# 'scm_act1_q0040a_s0360', 
# 'scm_act1_q0040a_s0370', 
# 'scm_act1_q0040a_s0410', 
# 'scm_act1_q0040a_s0450', 
# 'scm_act1_q0040a_s0470', 
# 'scm_act1_q0040a_s0490', 
# 'scm_act1_q0040a_s0500', 
# 'scm_act1_q0040a_s0520', 
# 'scm_act1_q0040a_s0530']
# sg_batch.update_tasks('SevenChickMovie', 'apr', taskNames=[a['content'] for a in tasks], shotList=shotList)