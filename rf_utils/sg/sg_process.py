import sys
import os
import platform
import logging
from datetime import datetime, timedelta

logger = logging.getLogger(__name__)

if os.environ.get('RFSCRIPT', False):
    path = '%s/core' % os.environ.get('RFSCRIPT')
    if not path in sys.path:
        sys.path.append(path)

import rf_config as config
from shotgun_api3 import Shotgun
from shotgun_api3 import shotgun
from shotgun_api3 import ShotgunError

# connection to server
server = config.Shotgun.server
script = config.Shotgun.script
id = config.Shotgun.id
sg = None
ca_certs = '{}/{}'.format(os.path.dirname(shotgun.__file__.replace('\\', '/')), config.Shotgun.ca_certs)
error = ShotgunError

try:
    sg = Shotgun(server, script, id, ca_certs=ca_certs)
    connection = True
except shotgun.SSLHandshakeError as e:
    logger.error(e)
    connection = False

def init_key(inputKey):
    script, id = config.get_key(inputKey)
    global sg
    sg = Shotgun(server, script, id, ca_cert=ca_cert)

class FieldSetting:
    project = ['name', 'id', 'sg_project_code']
    projectAll = project + ['sg_frame_rate', 'sg_status', 'sg_description', 'sg_resolution']
    asset = ['code', 'id']
    step = ['code', 'short_name', 'entity_type', 'id']
    task = ['content', 'id', 'entity']
    entity = ['project', 'step', 'entity', 'content', 'image' ,'sg_status_list', 'type', 'id', 'task_assignees', 'sg_name']


################# find/find_one ###################

def get_projects():
    return sg.find('Project', [], ['name', 'id', 'sg_project_code'])


def get_active_projects():
    return sg.find('Project', [['sg_status', 'is', 'Active']], ['name', 'id', 'sg_project_code'])


def get_project(project):
    filters = [['name', 'is', project]]
    fields = ['code', 'id']
    return sg.find_one('Project', filters, fields)

def get_project_by_id(id):
    filters = [['id', 'is', id]]
    fields = ['name']
    return sg.find_one('Project', filters, fields)

def get_episodes(project):
    filters = [['project.Project.name', 'is', project]]
    fields = ['code', 'id', 'shortCode', 'project']
    return sg.find('Scene', filters, fields)

def get_one_episode(project, episode):
    filters = [['project.Project.name', 'is', project],
                ['code', 'is', episode]]
    fields = ['code', 'id']
    return sg.find_one('Scene', filters, fields)

def get_sequences(project, episode):
    if isinstance(project, str): 
        filters = [['project.Project.name', 'is', project]]
    else: 
        filters = [['project', 'is', project]]

    filters.append(['sg_episode', 'is', episode])
    fields = ['code', 'id', 'sg_shortcode']
    return sg.find('Sequence', filters, fields)

def get_one_sequence(project, episode, sequence, fields=[]):
    filters = [['project.Project.name', 'is', project],
                ['sg_episode.Scene.code', 'is', episode],
                ['code', 'is', sequence]]
    fields = fields + ['code', 'id']
    return sg.find_one('Sequence', filters, fields)

def get_all_sequences(project):
    filters = [['project.Project.name', 'is', project]]
    fields = ['code', 'id', 'sg_shortcode', 'sg_episode']
    return sg.find('Sequence', filters, fields)

def get_shots(project, episode, sequence):
    filters = [['project', 'is', project], ['sg_episode', 'is', episode], ['sg_sequence', 'is', sequence]]
    fields = ['code', 'id', 'sg_shortcode', 'sg_episode', 'sg_sequence', 'sg_sequence.Sequence.sg_shortcode', 'sg_cut_order']
    return sg.find('Shot', filters, fields)

def get_all_shots(project, filters=None, extraFields=[]):
    projectFilters = ['project.Project.name', 'is', project]
    if not filters:
        filters = [projectFilters]
    else:
        filters.append(projectFilters)
    fields = ['code', 'sg_shortcode', 'sg_sequence_code', 'sg_sequence', 'sg_episode', 'step', 'sg_cut_order', 'sg_shot_type']
    if extraFields:
        fields += extraFields
    return sg.find('Shot', filters, fields)


def get_one_shot(project, episode, sequence, shot):
    filters = [ ['project.Project.name', 'is', project],
                ['sg_episode.Scene.code', 'is', episode],
                ['sg_sequence.Sequence.sg_shortcode', 'is', sequence],
                ['sg_shortcode', 'is', shot]]
    fields = ['code', 'id', 'sg_shortcode', 'sg_episode', 'sg_sequence', 'sg_sequence.Sequence.sg_shortcode', 'project', 'sg_episode']
    return sg.find_one('Shot', filters, fields)

def get_shot_entity(project, code, extraFields=[]):
    filters = [ ['project.Project.name', 'is', project], ['code', 'is', code]]
    fields = ['code', 'id', 'sg_shortcode', 'sg_episode', 'sg_sequence', 'sg_sequence.Sequence.sg_shortcode', 'project']
    if extraFields:
        fields = fields + extraFields
    return sg.find_one('Shot', filters, fields)

def get_shot_entities(projectEntity, episodeEntity, sequence, extraFields, order=[]):
    filters = [['project', 'is', projectEntity], ['sg_episode' , 'is', episodeEntity], ['sg_sequence_code', 'is', sequence],['sg_shot_type', 'is' , 'Shot']]
    fields = ['code', 'id', 'sg_shortcode', 'sg_episode', 'sg_sequence', 'sg_sequence.Sequence.sg_shortcode']

    if extraFields:
        fields = fields + extraFields
    return sg.find('Shot', filters, fields, order)

def get_subtype():
    return sg.schema_field_read('Asset', 'sg_subtype')['sg_subtype']["properties"]["valid_values"]["value"]

def get_type():
    return sg.schema_field_read('Asset', 'sg_asset_type')['sg_asset_type']["properties"]["valid_values"]["value"]

def get_assets(project, filters=None, extraFields=[]):
    projectFilters = ['project.Project.name', 'is', project]
    if not filters:
        filters = [projectFilters]
    else:
        filters.append(projectFilters)
    fields = ['code', 'sg_asset_type', 'sg_subtype', 'sg_episodes', 'tags']
    if extraFields:
        fields += extraFields
    return sg.find('Asset', filters, fields)

def get_one_asset(project,assetName):
    filters = [ ['project.Project.name','is',project],
                ['code','is',assetName] ]
    fields = ['code', 'sg_asset_type', 'sg_subtype', 'sg_episodes', 'project']
    return sg.find_one('Asset',filters,fields)


def get_omit_assets(project): 
    filters = [['project.Project.name', 'is', project], ['sg_status_list', 'is', 'omt']]
    fields = ['code', 'sg_asset_type', 'sg_redirect_asset', 'sg_redirect_asset.Asset.sg_asset_type']
    return sg.find('Asset', filters, fields)


def get_tags(): 
    return sg.find('Tag', [], ['name', 'id'])

def get_step():
    filters = []
    fields = ['code', 'id', 'entity_type']
    return sg.find('Step', filters, fields)

def get_task(entityType, userEntity, projectEntity, episodeEntity, stepEntity):
    filters = taskFilters(entityType, userEntity, projectEntity, episodeEntity, stepEntity)
    fields = taskFields(entityType)
    return sg.find('Task', filters, fields)

def get_tasks(entity):
    filters = [['entity', 'is', entity]]
    fields = taskFields(entity['type'])
    return sg.find('Task', filters, fields)

def get_tasks_by_step(entity, step):
    filters = [['entity', 'is', entity], ['step.Step.code', 'is', step]]
    fields = taskFields(entity['type'])
    return sg.find('Task', filters, fields)

def get_one_task(entity, task):
    filters = [ ['entity','is',entity],
                ['content','is',task]]
    fields = taskFields(entity['type'])
    return sg.find_one('Task', filters, fields)

def get_one_task_by_step(entity, step, task, create=False):
    filters = [ ['entity','is', entity],
                ['content','is', task],
                ['step.Step.code', 'is', step]]
    fields = taskFields(entity['type'])
    taskEntity = sg.find_one('Task', filters, fields)
    if not create:
        return taskEntity
    if not taskEntity:
        return create_task(entity, step, task)
    else:
        return taskEntity

def get_one_version(version):
    return sg.find_one('Version', [['code', 'is', version]], ['code', 'id', 'sg_status_list'])

def get_entity_versions(entity):
    filters = [['entity', 'is', entity]]
    fields = ['sg_task', 'sg_task.Task.step.Step.code', 'code', 'description', 'sg_path_to_publish', 'sg_path_to_hero', 'sg_path_to_workspace', 'sg_version_type']
    return sg.find('Version', filters, fields)

def get_users():
    fields = ['name', 'id', 'sg_localuser', 'groups']
    users = sg.find('HumanUser', [], fields)

    return users

def get_local_user(user):
    users = get_users()
    userEntity = [a for a in users if a.get('sg_localuser') == user]
    return userEntity[0] if userEntity else {}


def get_list_field(field) :
    # name to add to the list
    #type = 'sg_subtype' for supType
    #type = 'sg_asset_type' for Type

    values = sg.schema_field_read(str('Asset'), str(field)) [str(field)]["properties"]["valid_values"]["value"]
    return values

def list_looks(entity):
    filters = [['entity', 'is', entity], ['step.Step.code', 'is', 'texture']]
    fields = ['sg_name', 'content', 'id']
    return sg.find('Task', filters, fields)

####################### create ########################

def create_asset(project, assetType, assetSubType, assetName, episode=None, template='default', taskTemplate=None):
    if not taskTemplate:
        if template == 'default':
            taskTemplate = {'code': 'th_ch_asset_md_template', 'type': 'TaskTemplate', 'id': 110}
        if template == 'lo':
            taskTemplate = {'code': 'dm_asset_pxy_template', 'type': 'TaskTemplate', 'id': 77}
        if template == 'hi':
            taskTemplate = {'code': 'dm_asset_hi_template', 'type': 'TaskTemplate', 'id': 76}

    data = dict()
    if episode:
        data.update({'sg_episodes': episode})

    add_list_field(assetType, 'sg_asset_type')
    add_list_field(assetSubType, 'sg_subtype')

    data.update({'project': project, 'sg_asset_type': assetType, 'sg_subtype': assetSubType, 'code': assetName, 'task_template': taskTemplate})
    return sg.create('Asset', data)


def create_episode(project, episodeName):
    data = {'project': project, 'code': episodeName}
    return sg.create('Scene', data)

def create_sequence(project, episode, sequenceName, shortCode):
    result = get_one_sequence(project.get('name'), episode.get('code'), sequenceName)
    if not result:
        projCode = project.get('sg_project_code')
        data = {'project': project, 'sg_episode': episode, 'sg_episodes': episode, 'sg_shortcode': shortCode, 'code': sequenceName}
        result = sg.create('Sequence', data)

    return result

def create_shot(project, episode, sequence, shotName, shortCode, template='default', taskTemplate=None, startFrame=None, endFrame=None, duration=None):
    result = sg.find_one('Shot', [['project', 'is', project], ['code', 'is', shotName]], ['id', 'code'])
    if not result:
        if not taskTemplate:
            if template == 'default':
                taskTemplate = {'code': 'dm_shot_template', 'type': 'TaskTemplate', 'id': 78}

        projCode = project.get('sg_project_code')
        data = {'project': project, 'sg_episode': episode, 'sg_sequence': sequence, 'sg_shortcode': shortCode, 'code': shotName, 'task_template': taskTemplate}
        if startFrame and endFrame:
            data.update({'sg_cut_in': startFrame, 'sg_cut_out': endFrame, 'sg_cut_duration': duration})
        result = sg.create('Shot', data)

    return result

def remove_look(entity, lookName):
    existing_looks = list_looks(entity)
    fields = ['content', 'id']
    for look in existing_looks:
        if look.get('sg_name') == lookName:
            # delete texture task
            sg.delete('Task', look.get('id'))
            # find and delete groom and lookdev task
            filters = [['project', 'is', entity.get('project')], 
                    ['entity', 'is', entity], 
                    ['sg_name', 'is', lookName]]
            tasks = sg.find('Task', filters, fields)
            for task in tasks:
                sg.delete('Task', task.get('id'))
            break

def create_look(entity, lookName):
    steps = get_step()
    projectEntity = entity.get('project')

    # texture
    textureStep = [a for a in steps if a['code'] == 'Texture']
    textureTaskName = 'texture_%s' % lookName if not lookName == 'main' else 'texture'
    # lookdev
    lookdevStep = [a for a in steps if a['code'] == 'Lookdev']
    lookdevTaskName = 'lookdev_%s' % lookName if not lookName == 'main' else 'lookdev'
    # groom
    groomStep = [a for a in steps if a['code'] == 'Groom']
    groomTaskName = 'groom_%s' % lookName if not lookName == 'main' else 'groom'

    if textureStep and lookdevStep and groomStep:
        taskList = [(textureStep[0], textureTaskName), (lookdevStep[0], lookdevTaskName), (groomStep[0], groomTaskName)]
        results = []

        for task in taskList:
            step, taskName = task
            data = {'project': projectEntity,
                    'entity': entity,
                    'step': step,
                    'content': taskName,
                    'sg_name': lookName}
            taskEntity = sg.create('Task', data)
            results.append(taskEntity)

        return results

def create_task(entity, step, taskName):
    stepEntity = sg.find_one('Step', [['code', 'is', step]], ['code', 'id'])
    projectEntity = entity.get('project')
    data = {'project': projectEntity,
                    'entity': entity,
                    'step': stepEntity,
                    'content': taskName}
    taskEntity = sg.create('Task', data)
    return taskEntity

########################### Update ##########################

def link_local_user(userId, localUser):
    return sg.update('HumanUser', userId, {'sg_localuser': localUser})

def set_task_status(taskId, status, description=''):
    from rf_utils import user_info
    user = user_info.User()
    userName = user.name()
    filters = [['id', 'is', taskId]]
    oldStatus = sg.find('Task', filters, ['sg_status_list'])[0]['sg_status_list']
    now = datetime.now()
    currentTime = now.strftime("%m-%d-%y %H:%M") # 03-05-21 15:51
    metaData = {'status': {'old': oldStatus, 'new': status, 'by': userName, 'time': currentTime}, 'description': description}
    metaData = str(metaData)
    return sg.update('Task', taskId, {'sg_status_list': status, 'sg_meta': metaData})

def set_version_status(versionId, status):
    return sg.update('Version', versionId, {'sg_status_list': status})

def set_task_data(taskId, data):
    return sg.update('Task', taskId, data)

def set_entity_status(entity, entityId, status):
    return sg.update(entity, entityId, {'sg_status_list': status})

def assign_task(taskId, userId):
    return sg.update('Task', taskId, {'task_assignees': [{'type': 'HumanUser', 'id': userId}]})

def set_step_task_status(project, step, entity_name, status, entity_type='Asset'): 
    # set all tasks in step
    # project = 'Bikey'
    # step = 'rig'
    # entity_name = 'treeBushA'
    # status = 'omt'
    filters = [
                ['entity.{}.code'.format(entity_type), 'is', entity_name], 
                ['step.Step.short_name', 'is', step], 
                ['project.Project.name', 'is', project]
            ]
    tasks = sg.find('Task', filters, ['content', 'id', 'sg_status_list'])
    batch_data = list()
    for task in tasks: 
        data = {"request_type": "update", 
                                "entity_type": "Task", 
                                "entity_id": task['id'], 
                                "data": {"sg_status_list": status}}
        batch_data.append(data)
    return sg.batch(batch_data)

def update_entity_thumbnail(entity, id, path):
    return sg.upload_thumbnail(entity, id, path)

def update_shot_duration(id, duration):
    data = {'sg_working_duration': duration, 'sg_cut_out': duration}
    return sg.update('Shot', id, data)


def link_local_file(entityType, id, sg_field, display, path):
    path = path.replace('/', '\\')
    data = {sg_field: {'link_type': "local", 'local_path': path, 'name': display}}
    return sg.update(entityType, id, data)


def update_subasset_list(id, entities, sg_field='assets'):
    currentEntities = sg.find_one('Asset', [['id', 'is', id]], [sg_field])
    updateEntities = entities
    if currentEntities.get('assets'):
        updateEntities = updateEntities + currentEntities.get('assets')
    data = {sg_field: updateEntities}
    return sg.update('Asset', id, data)


def update_related_design(asset_id, design_entities=[]): 
    """ sg_link_designs """ 
    data = {'sg_link_designs': design_entities}
    return sg.update('Asset', asset_id, data)


def update_asset_list(id, entities, sg_field='assets', mode='', entity='Shot'):
    updateEntities = remove_duplicated_list(entities)
    if mode == 'merge':
        currentEntities = sg.find_one(entity, [['id', 'is', id]], [sg_field])
        if currentEntities.get(sg_field):
            updateEntities = updateEntities + currentEntities.get(sg_field)
            updateEntities = remove_duplicated_list(updateEntities)

    data = {sg_field: updateEntities}
    return sg.update(entity, id, data)


def remove_duplicated_list(listData, key='id'):
    valueDict = dict()
    for data in listData:
        valueDict[data[key]] = data

    return [v for k, v in valueDict.iteritems()]


def upload_version_media(versionEntity, media, sequences=None, proxy=None) :
    # upload media
    media = media.replace('/', '\\')
    uploadMedia = media if not proxy else proxy
    mediaResult = sg.upload('Version', versionEntity['id'], uploadMedia, 'sg_uploaded_movie')

    # path to movie
    media = sequences if sequences else media
    data = {'sg_path_to_movie': media}
    sg.update('Version', versionEntity.get('id'), data)
    # logger.debug('Upload media %s' % mediaResult)

    return mediaResult

def get_media(versionEntity):
    fields = ['sg_uploaded_movie']
    projectEntity = versionEntity.get('project')
    versionName = versionEntity.get('code')
    filters = [['project', 'is', projectEntity], ['code', 'is', versionName]]
    entity = sg.find_one('Version', filters, fields)
    imgEntity = entity['sg_uploaded_movie']
    imgurl = imgEntity.get('url')
    return imgurl

def get_media_thumbnail(versionEntity):
    fields = ['image']
    projectEntity = versionEntity.get('project')
    versionName = versionEntity.get('code')
    filters = [['project', 'is', projectEntity], ['code', 'is', versionName]]
    entity = sg.find_one('Version', filters, fields)
    imgurl = entity.get('image')
    return imgurl

def daily_playlist(project, step, dailyPath=''):
    playlistDailyName = '%s_daily_%s' % (step, datetime.now().strftime("%Y-%m-%d"))
    projectEntity = get_project(project)
    playlistEntity = sg.find_one('Playlist', [['project', 'is', projectEntity], ['code', 'is', playlistDailyName]], ['code', 'id'])

    if not playlistEntity:
        data = {'project': projectEntity, 'code': playlistDailyName, 'sg_path_to_daily': dailyPath}
        playlistEntity = sg.create('Playlist', data)
    return playlistEntity

def get_weekly_name():
    import calendar
    date = datetime.now()
    year = date.strftime("%Y")
    month = date.strftime("%m")
    day = date.strftime("%d")
    calenderWeek = calendar.monthcalendar(int(year),int(month))
    weekCount = 1
    for week in calenderWeek:
        if int(day) in week:
            break
        weekCount +=1

    weekly = '%s-%s-week%s' %(year,month,weekCount)
    return weekly


def weekly_playlist(project, step, dailyPath=''):
    playlistDailyName = '%s_weekly_%s' % (step, get_weekly_name())
    projectEntity = get_project(project)
    playlistEntity = sg.find_one('Playlist', [['project', 'is', projectEntity], ['code', 'is', playlistDailyName]], ['code', 'id'])

    if not playlistEntity:
        data = {'project': projectEntity, 'code': playlistDailyName, 'sg_path_to_daily': dailyPath}
        playlistEntity = sg.create('Playlist', data)
    return playlistEntity
    # playlistDailyName = '%s_daily_%s' % (step, weekly)

def global_daily_playlist(project):
    playlistDailyName = 'daily_%s' % (datetime.now().strftime("%Y_%m_%d"))
    projectEntity = get_project(project)
    playlistEntity = sg.find_one('Playlist', [['project', 'is', projectEntity], ['code', 'is', playlistDailyName]], ['code', 'id'])

    if not playlistEntity:
        data = {'project': projectEntity, 'code': playlistDailyName}
        playlistEntity = sg.create('Playlist', data)
    return playlistEntity

########################### Other ###########################

def add_list_field(name, field) :
    # name to add to the list
    #type = 'sg_subtype' for supType
    #type = 'sg_asset_type' for Type

    values = sg.schema_field_read(str('Asset'), str(field)) [str(field)]["properties"]["valid_values"]["value"]
    if not name in values :
        values.append(name)
        properties = {'valid_values' : values}
        sg.schema_field_update(str('Asset'), str(field), properties)
        return values

def remove_list_field(name, field):
    values = sg.schema_field_read(str('Asset'), str(field)) [str(field)]["properties"]["valid_values"]["value"]
    if name in values :
        values.remove(name)
        properties = {'valid_values' : values}
        sg.schema_field_update(str('Asset'), str(field), properties)
        return values

def taskFilters(entityType, userEntity, projectEntity, episodeEntity, stepEntity) :

    filters = [['task_assignees', 'is', userEntity], ['step', 'is', stepEntity], ['project', 'is', projectEntity]]
    advancedFilter1 = {
                        "filter_operator": "any",
                        "filters": [
                        ["sg_status_list", "is", "ip"],
                        ["sg_status_list", "is", "rdy"],
                        ["sg_status_list", "is", "wtg"],
                        ["sg_status_list", "is", "apr"],
                        ["sg_status_list", "is", "rev"],
                        ["sg_status_list", "is", "fix"],
                        ]
                    }

    filters.append(advancedFilter1)

    if not episodeEntity['code'] == '-' :

        if entityType == 'Asset' :
            filters.append(['entity.Asset.scenes', 'is', episodeEntity])

        if entityType == 'Shot' :
            filters.append(['entity.Shot.sg_scene', 'is', episodeEntity])

    return filters

def taskFields(entityType) :
    fields = ['content', 'entity', 'step', 'project', 'sg_status_list', 'task_assignees', 'sg_app', 'sg_resolution', 'sg_name', 'sg_lockdown', 'sg_lockdown_message']
    assetFields = ['entity.Asset.code', 'entity.Asset.sg_asset_type', 'entity.Asset.sg_subtype', 'entity.Asset.scenes']
    shotFields = ['entity.Shot.code', 'entity.Shot.sg_sequence', 'entity.Shot.sg_scene', 'entity.Shot.sg_scene']

    if entityType == 'Asset' :
        fields = fields + assetFields

    if entityType == 'Shot' :
        fields = fields + shotFields
    return fields


def list_triggered_projects():
    filters = [['sg_event_trigger', 'is', True]]
    fields = ['name', 'id']
    projects = sg.find('Project', filters, fields)
    return [a.get('name') for a in projects]


def publish_version(projectEntity, entity, taskEntity, userEntity, versionName, status, description, playlistEntity) :
    # create version

    playlists = []
    data = { 'project': projectEntity,
             'code': versionName,
             'entity': entity,
             'sg_task': taskEntity,
             'sg_status_list': status,
             'description' : description}

    if userEntity :
        data.update({'user': userEntity})

    if playlistEntity:
        playlists.append(playlistEntity)

    if playlists:
        data.update({'playlists': playlists})

    # create version
    versionEntity = sg.create('Version', data)
    logger.debug('Version %s' % versionEntity)

    return versionEntity

def update_version(projectEntity, entity, taskEntity, userEntity, versionName, status, description, playlistEntity, step='', versionType='', path_to_movie='', workspaceFile='', publishFile='', heroFile='', recommendedFile='', data='', replace=False) :
    # create version

    playlists = []
    data = { 'project': projectEntity,
             'code': versionName,
             'entity': entity,
             'sg_task': taskEntity,
             'sg_status_list': status,
             'description' : description,
             'sg_version_type': versionType,
             'sg_department': step,
             'sg_path_to_movie': path_to_movie,
             'sg_path_to_workspace': workspaceFile,
             'sg_path_to_publish': publishFile,
             'sg_path_to_hero': heroFile,
             'sg_recommended_file': recommendedFile,
             'sg_data': data}

    if userEntity :
        data.update({'user': userEntity})

    if playlistEntity:
        if not isinstance(playlistEntity, list):
            playlists.append(playlistEntity)
        else:
            playlists += playlistEntity

    if playlists:
        data.update({'playlists': playlists})

    # create version
    versionEntity = sg.find_one('Version', [['project', 'is', projectEntity], ['code', 'is', versionName]], ['code', 'id'])
    if versionEntity:
        if replace:
            versionEntity = sg.update('Version', versionEntity.get('id'), data)
        else:
            sg.delete('Version', versionEntity.get('id'))
            logger.debug('Version exists! Remove version!')
            versionEntity = None

    if not versionEntity:
        if userEntity:
            data.update({'created_by': userEntity})
        versionEntity = sg.create('Version', data)

    logger.debug('Version %s' % versionEntity)

    return versionEntity


def publish_file(publishName, projectEntity, publishTypeEntity, taskEntity, versionEntity, path, pathCache, status, *args):
    data = { 'project': projectEntity,
             'code': publishName,
             'published_file_type': publishTypeEntity,
             'task': taskEntity,
             'sg_status_list': status,
             'version': versionEntity,
             'path' : {'local_path': path, 'name': os.path.basename(path)},
             'path_cache': pathCache}

    publishFileEntity = sg.create('PublishedFile', data)

def update_task(id, status, display, path):
    taskEntity = set_task_status(id, status)
    sg_field = 'sg_hero'
    dirpath = '%s/' % os.path.dirname(path)
    return link_local_file('Task', id, sg_field, display, dirpath)


def update_task_start(task_id):
    task = sg.find_one('Task', [['id', 'is', task_id]], ['start_date', 'content'])
    data = {'start_date': datetime.today().date()}
    return sg.update('Task', task_id, data)


def update_playlist(versionId, playlists):
    data = {'playlists': playlists}
    sg.update('Version', versionId, data)


def get_shot_duration(project, shotName):
    projectEntity = get_project(project)
    result = sg.find_one('Shot', [['project', 'is', projectEntity], ['code', 'is', shotName]], ['sg_working_duration'])
    return result.get('sg_working_duration') if result else None


def sanity_check(sgEntities, entityType):
    entities = []
    for entity in sgEntities:
        filters = [['project.Project.name', 'is', entity['project']], ['id', 'is', entity['id']]]
        fields = ['code', 'id']
        sgEntity = sg.find_one(entityType, filters, fields)
        entities.append(sgEntity)

    return entities


def sum_shot_assets(project, sequence):
    """ summary asset and shotdressses from shot """
    filters = [['project.Project.name', 'is', project], ['code', 'is', sequence],
                ['sg_shot_type', 'is', 'Sequence']]
    fields = ['code', 'sg_shot_type', 'sg_sequence_code']
    sequenceEntity = sg.find_one('Shot', filters, fields)

    # find shots
    shotFilters = [['project.Project.name', 'is', project], ['sg_shot_type', 'is', 'Shot'],
                    ['sg_sequence_code', 'is', sequenceEntity['sg_sequence_code']]]
    shotFields = ['code', 'id', 'assets', 'sg_setdresses']
    shotEntities = sg.find('Shot', shotFilters, shotFields)

    assetDict = dict()
    setdresseDict = dict()

    for shotEntity in shotEntities:
        for asset in shotEntity['assets']:
            assetDict[asset['name']] = asset

        for setdress in shotEntity['sg_setdresses']:
            setdresseDict[setdress['name']] = setdress

    assets = [v for k, v in assetDict.iteritems()]
    setdresses = [v for k, v in setdresseDict.iteritems()]

    # update sequences
    data = {'assets': assets, 'sg_setdresses': setdresses}
    return sg.update('Shot', sequenceEntity['id'], data)


def create_tasks(project, entityType, taskName, step, data=dict()):
    """ create task for all entity """
    # example
    # sg_process.create_tasks('SevenChickMovie', 'Shot', 'anim_director', 'qc', {'sg_type': 'Shot'})

    # request all tasks first
    projectEntity = sg.find_one('Project', [['name', 'is', project]], ['name', 'id'])
    stepEntity = sg.find_one('Step', [['code', 'is', step], ['entity_type', 'is', entityType]], ['code', 'id', 'entity_type'])
    if stepEntity:
        filters = [['project', 'is', projectEntity], ['content', 'is', taskName], ['step.Step.id', 'is', stepEntity['id']]]
        fields = ['content', 'entity', 'step', 'entity']
        tasks = sg.find('Task', filters, fields)

        # find Asset or Shot entities
        allEntities = sg.find(entityType, [['project.Project.name', 'is', project]], ['code', 'id'])
        allIds = [a['id'] for a in allEntities]
        batch_data = []

        if tasks:
            # entityIds that already has task
            entityIds = [a['entity']['id'] for a in tasks if a.get('entity')]
            # entity id that has no task
            noTaskIds = [a for a in allIds if not a in entityIds]
            noTaskEntities = []

            for entity in allEntities:
                if entity['id'] in noTaskIds:
                    noTaskEntities.append(entity)

        else:
            noTaskEntities = allEntities

        if noTaskEntities:
            for entity in noTaskEntities:
                taskData = dict(data)
                taskData.update({'content': taskName, 'project': projectEntity, 'entity': entity, 'step': stepEntity})
                batch_data.append({"request_type": "create", "entity_type": "Task", "data": taskData})

            return sg.batch(batch_data)


def add_seq_link_to_asset(project, mode='asset'):
    """ update sequence link of assets based on shot linked """
    projectEntity = get_project(project)
    filters = [['project', 'is', projectEntity]]
    fields = ['shots', 'sequences', 'code', 'shot_sg_setdresses_shots']
    assetEntities = sg.find('Asset', filters, fields)
    shotCaches = dict()
    shotSeqDict = get_shot_sequence_dict(project)

    for asset in assetEntities:
        if mode == 'asset':
            shotEntities = asset['shots']
        if mode == 'setdress':
            shotEntities = asset['shot_sg_setdresses_shots']

        seqs = []

        if shotEntities:
            for shot in shotEntities:
                shotId = shot['id']
                shotName = shot['name']

                if shotName in shotSeqDict.keys():
                    seq = shotSeqDict.get(shotName)
                    seqName = seq.get('name')
                    seqs.append(seq) if not seq in seqs else None

            if seqs:
                # print('asset %s \nlinked to sequences %s \n%s' % (asset['code'], len(seqs), '\n-'.join([a['name'] for a in seqs])))
                # print(asset['sequences'])
                # print('current linked %s \n%s' % (len(asset['sequences']), '\n-'.join([a['name'] for a in asset['sequences']])))
                newLink = seqs + asset['sequences']
                linkSeqs = [dict(t) for t in {tuple(a.items()) for a in newLink}]
                # print('new link %s \n%s' % (len(newLink), '\n-'.join([a['name'] for a in newLink])))

                data = {'sequences': linkSeqs}
                result = sg.update('Asset', asset['id'], data)
                logger.info('update %s sequences to %s' % (len(linkSeqs), asset['code']))
                logger.debug(result)


def read_cycle_data(project, assetName, taskName):
    """ read cycle data """
    filters = [['project.Project.name', 'is', project], ['entity.Asset.code', 'is', assetName], ['content', 'is', taskName]]
    fields = ['sg_data', 'id']
    result = sg.find_one('Task', filters, fields)
    rawText = result.get('sg_data')
    if rawText:
        data = eval(rawText)
        return data.get('cycle')
    return dict()

def write_cycle_data(project, assetName, taskName, cycleData):
    """ write cycle data """
    # cycleData = {'loopRange': [1001, 1020], 'exportRange': [1001, 1100]}
    filters = [['project.Project.name', 'is', project], ['entity.Asset.code', 'is', assetName], ['content', 'is', taskName]]
    fields = ['sg_data', 'id']
    result = sg.find_one('Task', filters, fields)

    if result:
        data = result.get('sg_data')
        sgData = dict()
        if data:
            sgData = eval(data)
        sgData['cycle'] = cycleData

        writeData = {'sg_data': str(sgData)}
        return sg.update('Task', result.get('id'), writeData)
    else:
        logger.error('Task not found: %s %s %s' % (project, assetName, taskName))


def get_shot_sequence_dict(project):
    """ find shot sequence dict """
    projectEntity = get_project(project)
    filters = [['project', 'is', projectEntity], ['sg_shot_type', 'is', 'Shot']]
    fields = ['sg_sequence', 'sg_sequence_code', 'code']
    shotEntities = sg.find('Shot', filters, fields)
    shotDict = dict()

    for shot in shotEntities:
        sequenceEntity = shot['sg_sequence']
        if sequenceEntity:
            sequenceName = sequenceEntity['name']
            shotDict.update({shot['code']: sequenceEntity})

    return shotDict


def remove_duplicated_task(project, step, taskType='Asset'):
    info = dict()
    dups = dict()

    filters = [['project.Project.name', 'is', project], ['step.Step.code', 'is', step], ['sg_type', 'is', taskType]]
    fields = ['step', 'content', 'entity']
    tasks = sg.find('Task', filters, fields)

    for task in tasks:
        assetName = task['entity']['name']
        taskName = task['content']
        if not assetName in info.keys():
            info[assetName] = [taskName]
        else:
            if not taskName in info[assetName]:
                info[assetName].append(taskName)
            else:
                if not assetName in dups.keys():
                    dups[assetName] = [task]
                else:
                    dups[assetName].append(task)

    for k in dups.keys():
        v = dups[k]
        tasks = sg.find('Task', [['entity', 'is', v[0]['entity']], ['content', 'is', v[0]['content']]], ['id', 'content', 'sg_status_list'])
        if len(tasks) > 1:
            max = sorted([(a['id'], a['sg_status_list']) for a in tasks])[-1]
            if max:
                maxId, status = max
                print(maxId, status)

                sg.delete('Task', maxId)
                print('delete', maxId, status)


def get_task_in_template(templateName):
    # Result: {'id': 61686,
#  'task_template': {'id': 160, 'name': 'scm_prop', 'type': 'TaskTemplate'},
#  'template_task': None,
#  'type': 'Task'} #
# sg.find_one('Task', [['id', 'is', 52192]], ['template_task', 'task_template'])

# # Result: {'id': 52192,
#  'task_template': None,
#  'template_task': {'id': 38726, 'name': 'modelUv', 'type': 'Task'},
#  'type': 'Task'} #
    return sg.find('Task', [['task_template.TaskTemplate.code', 'is', templateName]], ['project', 'content', 'id', 'task_template', 'template_task', 'sg_status_list', 'entity', 'step'])

def get_entity_tasks(project, entityName, entityType='Asset'):
    return sg.find('Task', [['project.Project.name', 'is', project], ['entity.%s.code' % entityType, 'is', entityName]], ['project', 'content', 'id', 'sg_status_list', 'entity', 'step'])


def sync_template(templateName, project, entityName, entityType, demo=False):
    logger.info('Project "%s" entity name "%s" compare template "%s"' % (project, entityName, templateName))
    entityTasks = get_entity_tasks(project, entityName, entityType)
    entityTaskNames = [a.get('content') for a in entityTasks]
    templateTasks = get_task_in_template(templateName)
    templateTaskNames = [a.get('content') for a in templateTasks]

    # find task not in template
    taskNotInTemplate = []
    taskNotInEntity = []
    projectEntity = None
    entity = None

    for task in entityTasks:
        projectEntity = task['project']
        entity = task['entity']

        if task.get('content') not in templateTaskNames:
            taskNotInTemplate.append(task)

    # find task in template but not in entity
    for task in templateTasks:
        if task.get('content') not in entityTaskNames:
            taskNotInEntity.append(task)

    logger.info('taskNotInTemplate (delete tasks) %s tasks' % len(taskNotInTemplate))
    logger.debug(taskNotInTemplate) if taskNotInTemplate else None
    logger.info('taskNotInEntity (add tasks) %s tasks' % len(taskNotInEntity))
    logger.debug(taskNotInEntity) if taskNotInEntity else None

    # remove tasks
    if taskNotInTemplate:
        for task in taskNotInTemplate:
            print(task.get('content'))
            if not demo:
                sg.delete('Task', task.get('id'))
            logger.info('delete %s' % task.get('content'))

    # add
    if taskNotInEntity:
        for task in taskNotInEntity:
            data = {'project': projectEntity, 'entity': entity, 'step': task['step'], 'content': task['content'], 'sg_type': entityType}
            if not demo:
                sg.create('Task', data)
            logger.info('create task %s' % data)

def rename_tasks(project, entityType, taskName, newTaskName, step='', targetEntities=[], targetKeys=[]):
    tasks = find_tasks(project, entityType, taskName, step=step, targetEntities=targetEntities, targetKeys=targetKeys)
    results = []

    if tasks:
        for task in tasks:
            data = {'content': newTaskName}
            result = sg.update('Task', task['id'], data)
            results.append(result)
            print('task', results)

    return results

def find_tasks(project, entityType, taskName, step='', targetEntities=[], targetKeys=[]):
    stepEntity = dict()
    projectEntity = get_project(project)
    if step:
        stepEntity = sg.find_one('Step', [['code', 'is', step], ['entity_type', 'is', entityType]], ['code', 'id', 'entity_type'])

    filters = [['project', 'is', projectEntity], ['content', 'is', taskName], ['step.Step.id', 'is', stepEntity['id']]]
    fields = ['content', 'entity', 'step', 'entity']
    tasks = sg.find('Task', filters, fields)
    if targetEntities or targetKeys:
        tasks = filter_tasks(tasks, targetEntities, targetKeys)

    return tasks


def filter_tasks(taskEntities, targetEntities=[], targetKeys=[]):
    """ filter task by assetName or shotName --> targetEntities=['scm_act1_q0010_s0010', 'scm_act1_q0010_s0020'] """
    tasks = []
    for task in taskEntities:
        add = False
        entity = task['entity']['name']
        if targetEntities:
            if entity in targetEntities:
                add = True

        if targetKeys:
            for key in targetKeys:
                if key in entity:
                    add= True
                    break

        if add:
            tasks.append(task)

    return tasks


def find_asset_instances(shot_entity): 
    return sg.find('AssetShotConnection', [['shot', 'is', shot_entity]], ['sg_instances', 'asset', 'id'])


def update_asset_instances(shot_entity, instance_list, force=False): 
    """ 
    Args: 
        instance list (list) : [{entity: asset_entity, instance: 2}, {entity: asset_entity, instance: 2}] 
    """ 
    shot_assets = find_asset_instances(shot_entity)
    shot_asset_ids = [a['asset']['id'] for a in shot_assets]
    batch_data = list()
    skip_data = list()
    confirm_update = False

    for asset in instance_list: 
        asset_entity = asset['entity']
        instance = asset['instance']

        if asset['entity']['id'] in shot_asset_ids: 
            for shot_asset in shot_assets: 
                asset_connection_id = shot_asset['id']
                if asset['entity']['id'] == shot_asset['asset']['id']: 
                    data = {"request_type": "update", 
                            "entity_type": "AssetShotConnection", 
                            "entity_id": asset_connection_id, 
                            "data": {"sg_instances": instance}}
                    batch_data.append(data)
        else: 
            skip_data.append(asset)

    if skip_data: 
        for asset in skip_data: 
            logger.warning('Skip update instance "{}"" not found in shot'.format(asset))

    confirm_update = True if force else True if not skip_data else False 

    if confirm_update: 
        result = sg.batch(batch_data)
        logger.debug('update instance {}/{} success'.format(len(instance_list) - len(skip_data), len(instance_list)))
        return result 


def remove(id, entity): 
    return sg.delete(entity, id)


def create_version(name, project, entityType, entity, task, step, user, **kwargs):
    projectEntity = get_project_entity(project)
    entity = get_entity(projectEntity, entityType, entity)
    stepEntity = get_step_entity(step, entityType)
    taskEntity = get_task_entity(projectEntity, entity, stepEntity, task)
    userEntity = get_user_entity(user)
    allowKw = ['sg_status_list', 'sg_status_list', 'description', 'playlists']

    data = {'project': projectEntity,
            'entity': entity,
            'sg_task': taskEntity,
            'code': name,
            'user': userEntity
            }

    for k, v in kwargs.items():
        if k in allowKw:
            data.update({k: v})

    versionEntity = sg.create('Version', data)
    return versionEntity


def get_project_entity(project):
    if isinstance(project, dict):
        return project
    filters = [['name', 'is', project]]
    return sg.find_one('Project', filters, FieldSetting.project)


def get_entity(projectEntity, entityType, entity):
    if isinstance(entity, dict):
        return entity
    filters = [['project', 'is', projectEntity], ['code', 'is', entity]]
    return sg.find_one(entityType, filters, FieldSetting.asset)


def get_step_entity(step, entityType):
    if isinstance(step, dict):
        return step
    filters = [['entity_type', 'is', entityType], ['code', 'is', step]]
    return sg.find_one('Step', filters, FieldSetting.step)


def get_task_entity(projectEntity, entity, stepEntity, task):
    if isinstance(task, dict):
        return task
    filters = [['project', 'is', projectEntity], ['entity', 'is', entity], ['step', 'is', stepEntity], ['content', 'is', task]]
    taskEntity = sg.find_one('Task', filters, FieldSetting.task)
    if not taskEntity:
        # create task
        data = {'entity': entity, 'project': projectEntity, 'step': stepEntity, 'content': task}
        return sg.create('Task', data)
    return taskEntity

def get_task_status(project, shotname, task):
    filters = [['project.Project.name', 'is', project], 
        ['entity.Shot.code', 'is', shotname], 
        ['content', 'is', task]]
    fields = ['sg_status_list', 'content', 'entity', 'project']
    return sg.find_one('Task', filters, fields)

def get_task_from_id(taskid):
    return sg.find_one('Task', [['id', 'is', taskid]], FieldSetting.entity)


def get_user_entity(user):
    from rf_utils import user_info
    if isinstance(user, dict):
        return user
    user = user_info.User()
    return user.sg_user()


def get_mastershot(project, episode, sequence):
    filters = [
        ['project.Project.name', 'is', project],
        ['sg_episode.Scene.code', 'is', episode],
        ['sg_sequence_code', 'is', sequence],
        ['sg_mastershot', 'is', True]]
    fields = ['code', 'id', 'sg_shortcode', 'sg_status_list', 'sg_episode', 'sg_sequence', 'sg_sequence_code', 'description']
    shots = sg.find('Shot', filters, fields)
    return shots


def get_shot_from_sequence(project, episode, sequence):
    filters = [
        ['project.Project.name', 'is', project],
        ['sg_episode.Scene.code', 'is', episode],
        ['sg_sequence_code', 'is', sequence]]
    fields = ['code', 'id', 'sg_shortcode', 'sg_status_list', 'sg_episode', 'sg_sequence', 'sg_sequence_code', 'description']
    shots = sg.find('Shot', filters, fields)
    return shots


def get_task_from_entities(taskname, entities):
    filters = [['content', 'is', taskname]]
    advancedFilter1 = {
                        "filter_operator": "any",
                        "filters": [['entity', 'is', a] for a in entities]
                    }
    filters.append(advancedFilter1)
    fields = ['sg_description', 'content', 'entity', 'project', 'sg_status_list', 'task_assignees']
    return sg.find('Task', filters, fields)


def get_versions_from_entities(entities, task, extraFields=[]): 
    filters = [['sg_task.Task.content', 'is', task]]
    advancedFilter1 = {
                        "filter_operator": "any",
                        "filters": [['entity', 'is', a] for a in entities]
                    }
    filters.append(advancedFilter1)
    fields = [
                'code', 'description', 'sg_status_list', 'sg_path_to_movie',
                'entity', 'user', 'sg_task', 'created_at', 'sg_task.Task.entity']
    return sg.find('Version', filters, fields + extraFields)

def get_versions_from_entities_tasks(entities, tasks, extraFields=[]): 
    filters = []

    advancedFilter1 = {
                        "filter_operator": "any",
                        "filters": [['sg_task.Task.content', 'is', a] for a in tasks]
                    }
    filters.append(advancedFilter1)
    advancedFilter2 = {
                        "filter_operator": "any",
                        "filters": [['entity', 'is', a] for a in entities]
                    }
    filters.append(advancedFilter2)
    fields = [
                'code', 'description', 'sg_status_list', 'sg_path_to_movie',
                'entity', 'user', 'sg_task', 'created_at', 'sg_task.Task.entity', 
                'sg_task.Task.task_assignees']
    return sg.find('Version', filters, fields + extraFields)


def find_sequence_from_shot(project, episode, shotname):
    filters = [['project.Project.name', 'is', project],
        ['sg_episode.Scene.code', 'is', episode],
        ['code', 'is', shotname]]
    fields = ['sg_sequence']
    shot = sg.find_one('Shot', filters, fields)
    return shot['sg_sequence']


def find_version_from_sequence(project, sequence, taskname):
    """ find versions from sequence entity """
    filters = [
        ['project.Project.name', 'is', project],
        ['entity.Shot.sg_sequence', 'is', sequence],
        ['sg_task.Task.content', 'is', taskname]]
    fields = [
                'code', 'description', 'sg_status_list', 'sg_path_to_movie',
                'entity', 'user', 'sg_task', 'created_at']
    versions = sg.find('Version', filters, fields)
    return versions


def get_versions_from_sequence(project, episode, sequence, taskname, extraFields=[]):
    """ return versions on selected task on each shot
        Args:
            project = 'Hanuman'
            episode = 'act1'
            sequence = 'q0030g'
            taskname = 'colorscript'

        Return:

        {'hnm_act1_q0030g_s0010':
            {'version': [{
                'name': 'hnm_act1_q0030g_s0010_clrscpt_main.v001',
                'user': {'type': 'HumanUser', 'name': 'TA'},
                'description': 'hello'
                'sg_path_to_movie': 'P:/'
                'sg_status_list': 'aprv',
                'created_at': 'datetime',
                'sg_task.Task.sg_status_list': 'aprv',
                'version': 'v001'}]},

            'sg_cut_order': 1}
    """
    version_dict = dict()

    # find shots first
    filters = [
        ['project.Project.name', 'is', project],
        ['sg_episode.Scene.code', 'is', episode],
        ['sg_sequence_code', 'is', sequence]]
    fields = ['code', 'sg_cut_order', 'sg_sequence', 'sg_status_list', 'sg_shot_type', 'sg_shortcode']
    shots = [s for s in sg.find('Shot', filters, fields) if s.get('sg_status_list') != 'omt']  # ignore omited shots

    # master sequence
    sequence_entity = [a['sg_sequence'] for a in shots if a['sg_shot_type'] == 'Sequence']

    if sequence_entity:
        # find versions
        filters = [
            ['project.Project.name', 'is', project],
            ['entity.Shot.sg_sequence_code', 'is', sequence],
            ['sg_task.Task.content', 'is', taskname]]
        fields = ['code', 'description', 'sg_status_list', 'sg_path_to_movie',
                'entity', 'user', 'sg_task', 'created_at', 'sg_task.Task.sg_status_list']
        fields += extraFields
        versions = sg.find('Version', filters, fields)

        # cross match and pack data to desire format

        for version in versions:
            name = version['entity']['name']
            shot_entities = [a for a in shots if a['code'] == name]
            if shot_entities:
                shot_entity = shot_entities[0]
                ver = version['code'].split('.')[-1]
                version['version'] = ver # hnm_act1_q0030g_s0220_clrscpt_main.v002

                if not name in version_dict.keys():
                    shot_entity['versions'] = [version]
                    version_dict[name] = shot_entity
                else:
                    version_dict[name]['versions'].append(version)

                # shot entity
                version_dict[name]['shot'] = version['entity']

    return version_dict


def get_task_by_step(project, step):
    """ Args:
        project (dict): entity
        entity_type (str): Asset / Shot
        step (dict): entity
    """
    entity_type = step['entity_type']
    filters = [
        ['project', 'is', project],
        ['step', 'is', step]]
    entity_group_key = ''
    fields = ['entity', 'sg_status_list', 'content', 'sg_type', 'project']
    if entity_type == 'Asset':
        filters.append(['sg_type', 'is', 'Asset'])
        entity_group_key = 'entity.Asset.sg_asset_type'
        entity_parent_key = 'entity.Asset.sg_subtype'
        nicename = 'entity.Asset.sg_shortcode'
    if entity_type == 'Shot':
        filters.append(['sg_type', 'is', 'Shot'])
        entity_group_key = 'entity.Shot.sg_episode.Scene.code'
        entity_parent_key = 'entity.Shot.sg_sequence_code'
        nicename = 'entity.Shot.sg_shortcode'
    fields.append(entity_group_key) if entity_group_key else None
    fields.append(entity_parent_key) if entity_parent_key else None
    tasks = sg.find('Task', filters, fields)

    group_dict = dict()
    # grouping data for task in tasks:
    for task in tasks:
        if task.get('entity'): 
            entity_name = task['entity']['name']
            task['type_ep_group'] = task[entity_group_key] # type / episode
            task['entity_parent_key'] = task[entity_parent_key] # subtype / sequence

            if task['entity']['type'] == 'Asset':
                task['group_key'] = '{}'.format(task['type_ep_group'])
            elif task['entity']['type'] == 'Shot':
                task['group_key'] = '{}_{}'.format(task['type_ep_group'], task['entity_parent_key'])

            task['entity'].update({
                'code': entity_name,
                'type_ep_group': task['type_ep_group'],
                'entity_parent_key': task['entity_parent_key'],
                'step': step['short_name'],
                'project': task['project'],
                'type': task['entity']['type'],
                'sg_shortcode': task['entity'].get('sg_shortcode')})

            keywords = [entity_name, task['content'], task['sg_status_list'], task['group_key']]
            if not entity_name in group_dict.keys():
                group_dict[entity_name] = {'task': [task], 'keywords': keywords, 'entity': task['entity']}
            else:
                group_dict[entity_name]['task'].append(task)
                group_dict[entity_name]['keywords'].extend([task['content'], task['sg_status_list']])

    return group_dict


def set_task(project, entity_type, entity_name, taskname, status, data=dict()):
    filters = [['project.Project.name', 'is', project], ['content', 'is', taskname]]
    fields = ['content', 'id', 'entity', 'sg_status_list']
    if entity_type == 'Asset':
        filters.append(['entity.Asset.code', 'is', entity_name])
    if entity_type == 'Shot':
        filters.append(['entity.Shot.code', 'is', entity_name])
    task_entity = sg.find_one('Task', filters, fields)

    if task_entity:
        data.update({'sg_status_list': status})
        return sg.update('Task', task_entity['id'], data)


def get_task_from_context(entity): 
    result = dict()
    if entity.task: 
        entity_map = {'asset': 'Asset', 'scene': 'Shot', 'design': 'Asset'}
        link_entity = 'entity.{}.code'.format(entity_map[entity.entity_type])
        filters = [
            ['project.Project.name', 'is', entity.project], 
            [link_entity, 'is', entity.name], ['content', 'is', entity.task]]
        fields = ['content', 'due_date', 'start_date', 'sg_status_list']
        result = sg.find_one('Task', filters, fields) or dict()
    return result 


def get_versions_from_entity(entity, task, extraFields=[]): 
    versions = list()
    task_entity = sg.find_one('Task', [['entity', 'is', entity], ['content', 'is', task]], ['content', 'id'])
    fields = [
                'code', 'description', 'sg_status_list', 'sg_path_to_movie',
                'entity', 'user', 'sg_task', 'created_at']
    if task_entity: 
        versions = sg.find('Version', [['sg_task', 'is', task_entity]], fields+extraFields)
    return versions


def sync_task_start_date(task_id):
    start_date = None
    task = sg.find_one('Task', [['id', 'is', task_id]], ['start_date', 'content', 'entity', 'sg_meta'])
    meta_dict = eval(task['sg_meta']) if task['sg_meta'] else dict()

    logger.debug('Start {}'.format(task['entity']))

    if not task['start_date']:
        events = sg.find('EventLogEntry', [['entity.Task.id', 'is', task_id]], ['meta', 'created_at'])

        if any(a['meta'].get('new_value') == 'ip' for a in events):
            for ev in events:
                if ev['meta'].get('attribute_name') == 'sg_status_list':
                    if ev['meta']['new_value'] == 'ip':
                        start_date = ev['created_at']

        if start_date:
            logger.debug('Update task start date')
            meta_dict['start_date'] = 'from ip status'
            sg.update('Task', task_id, {'start_date': start_date.date(), 'sg_meta': str(meta_dict)})
            return True
        else:
            logger.debug('"ip" not found on task {}'.format(task['entity']))
            return False


def sync_task_due_date(task_id):
    due_date = None
    task = sg.find_one('Task', [['id', 'is', task_id]], ['due_date', 'content', 'entity', 'sg_meta', 'sg_status_list'])
    meta_dict = eval(task['sg_meta']) if task['sg_meta'] else dict()

    logger.debug('Start {}'.format(task['entity']))

    if not task['due_date']:
        events = sg.find('EventLogEntry', [['entity.Task.id', 'is', task_id]], ['meta', 'created_at'])
        if task['sg_status_list'] == 'apr': 
            if any(a['meta'].get('new_value') == 'apr' for a in events):
                for ev in events:
                    if ev['meta'].get('attribute_name') == 'sg_status_list':
                        if ev['meta']['new_value'] == 'apr':
                            due_date = ev['created_at']

            if due_date:
                logger.debug('Update task start date')
                meta_dict['due_date'] = 'from apr status'
                sg.update('Task', task_id, {'due_date': due_date.date(), 'sg_meta': str(meta_dict)})
                return True
            else:
                logger.debug('"apr" not found on task {}'.format(task['entity']))
                return False


def sync_all_task_start_end_date(project, taskname, start=True, end=True, key=None):
    filters = [
        ['project.Project.name', 'is', project], 
        ['content', 'is', taskname], 
        ['sg_status_list', 'is_not', 'omt']]
        
    fields = ['id', 'entity']
    tasks = sg.find('Task', filters, fields)
    success = []

    for task in tasks:
        key = key if key else task['entity']['name']
        if key in task['entity']['name']: 
            if start:
                result = sync_task_start_date(task['id'])
            if end:
                result = sync_task_due_date(task['id'])
            success.append(True) if result else None


def assign_dependency_task(project, episode, upstream_task, downstream_task, duration): 
    tasks = sg.find('Task', [
        ['project.Project.name', 'is', project], ['entity.Shot.sg_episode.Scene.code', 'is', episode]], 
        ['content', 'entity', 'start_date', 'due_date'])

    task_dict = dict() 
    
    for task in tasks: 
        shotname = task['entity']['name']
        taskname = task['content']
        if not shotname in task_dict.keys(): 
            task_dict[shotname] = {taskname: task}
        else: 
            task_dict[shotname][taskname] = task

    i = 1
    for shot, data in task_dict.items(): 
        task = data.get(upstream_task)
        target = data.get(downstream_task)

        if task and target: 
            due_date = task['due_date']
            if due_date: 
                due_date = datetime.strptime(due_date, '%Y-%m-%d')
                extend_start_day = 1
                if due_date.strftime('%a') == 'Fri': 
                    extend_start_day = 3
                new_start_date = due_date + timedelta(days=extend_start_day)
                print('shot', shot)
                new_due_date = date_by_adding_business_days(new_start_date, duration).date()
                print('end date', task['content'], task['due_date'], due_date.strftime('%a'))
                print('start date', target['content'], new_start_date.strftime('%Y-%m-%d, %a'))
                print('new due date', target['content'], new_due_date.strftime('%Y-%m-%d, %a'))
                print('\n')
                result = sg.update('Task', target['id'], {'start_date': new_start_date.strftime('%Y-%m-%d'), 'due_date': new_due_date})
                print(i, 'update task', result)
                i+=1


def date_by_adding_business_days(from_date, add_days):
    business_days_to_add = add_days
    current_date = from_date
    while business_days_to_add > 0:
        current_date += timedelta(days=1)
        weekday = current_date.weekday()
        if weekday >= 5: # sunday = 6
            continue
        business_days_to_add -= 1
    return current_date


def setup_dependencies(project, entity_type='Shot'): 
    dependency_data = {
        'layout': ['anim'], 
        'anim': ['camPolish', 'finalCam'], 
        'finalCam': ['cache'], 
        'cache': ['sim', 'techAnim'], 
        'sim': ['techAnim'], 
        'techAnim': ['light_full'], 
        'light_full': ['comp_full']
        }

    pinne_tasks = ['anim']

    skip_status = ['omt']

    entity_dict = dict()
    tasks = sg.find('Task', [['project.Project.name', 'is', project]], ['content', 'sg_status_list', 'entity'])

    for task in tasks: 
        # hardcode 
        if task['content'] == 'finalCam': 
            task['sg_status_list'] = 'apr'

        entity = task['entity']
        if entity: 
            entity_name = entity['name']
            if not entity_name in entity_dict.keys(): 
                entity_dict[entity_name] = {task['content']: task}
            else: 
                entity_dict[entity_name].update({task['content']: task})

    for entity, task_dict in entity_dict.items(): 
        # if not entity == 'hnm_act1_q0010b_s0260': 
        #     continue
        for upstream, downstreams in dependency_data.items(): 
            data = dict()
            if upstream in task_dict.keys(): 
                upstream_task = task_dict[upstream]
                if upstream_task['sg_status_list'] in skip_status: 
                    continue
                if upstream in pinne_tasks: 
                    data.update({'pinned': True})
                downstream_tasks = [task_dict[a] for a in downstreams if a in task_dict.keys()]
                downstream_tasks = [a for a in downstream_tasks if not a['sg_status_list'] in skip_status]
                if downstream_tasks: 
                    data.update({'downstream_tasks': downstream_tasks})
                    result = sg.update('Task', upstream_task['id'], data)
                    print(result)
            else: 
                print('{} not found in {}'.format(upstream, entity))


def backup_tasks(project, entity_type='Shot'): 
    from datetime import datetime
    from rf_utils import file_utils 
    path = 'P:/{}/_data/task/task_backup_{}.json'.format(project, datetime.now().date())

    fields = [
        'content', 'entity', 'project', 'start_date', 'due_date', 
        'duration', 'sg_status_list', 'task_assignees', 
        'pinned']
    tasks = sg.find('Task', [['project.Project.name', 'is', project], ['sg_type', 'is', entity_type]], fields)
    if not os.path.exists(os.path.dirname(path)): 
        os.makedirs(os.path.dirname(path))
    return file_utils.json_dumper(path, tasks)


def restore_from_file(filepath, project, entity_filters=[], tasks=[], attrs=[]): 
    from rf_utils import file_utils
    history_tasks = file_utils.json_loader(filepath)

    for task in history_tasks: 
        if task['entity']: 
            entity = task['entity']['name']
            if task['content'] in tasks: 
                if any(a in entity for a in entity_filters): 
                    if not task['sg_status_list'] == 'omt': 
                        # print(task['content'], entity)
                        data = dict()
                        for attr in attrs: 
                            if attr in task.keys(): 
                                data.update({attr: task[attr]})
                        print('backup data', data)
                        print(sg.update('Task', task['id'], data))


def backup_timelog(): 
    from rf_utils import file_utils
    filename = '{}_TimeLog.yml'.format(datetime.now().strftime('%Y-%m-%d'))
    dst = 'O:/Pipeline/_database/timelog_backup/{}'.format(filename)
    filed_dict = sg.schema_field_read('TimeLog')
    fields = [k for k, v in filed_dict.items()]
    data = sg.find('TimeLog', [], fields)
    print('Fetch complete {} items'.format(len(data)))

    file_utils.ymlDumper(dst, data)
    print('Write complete {}'.format(dst))


def list_tasks(project_name, step_name, entities=[], fields=[]): 
    filters = [['project.Project.name', 'is', project_name], ['step.Step.short_name', 'is', step_name]]
    standard_fields = ['content', 'entity']
    fields = fields + standard_fields if fields else standard_fields
    tasks = sg.find('Task', filters, fields)
    return tasks 


def assign_object_id(project, asset_id): 
    assets = sg.find('Asset', [['project.Project.name', 'is', project]], ['sg_object_id', 'id'])
    ids = sorted([a['sg_object_id'] for a in assets if a['sg_object_id']])
    max_id = ids[-1]
    assigned_id = max_id + 25
    return sg.update('Asset', asset_id, {'sg_object_id': assigned_id})


def get_task_meta(project, entity_type, entity_name, taskname): 
    meta = dict()
    result = None
    filters = [
        ['project.Project.name', 'is', project], 
        ['entity.{}.code'.format(entity_type), 'is', entity_name], 
        ['content', 'is', taskname]]
    fields = ['sg_data']
    result = sg.find_one('Task', filters, fields)
    
    if result: 
        strvalue = result['sg_data']
        if strvalue: 
            meta = eval(strvalue)
    
    return meta, result


def get_task_outputs(project, entity_type, entity_name, taskname): 
    output_key = 'outputs'
    meta, task = get_task_meta(project, entity_type, entity_name, taskname)
    if meta: 
        if output_key in meta.keys(): 
            return meta[output_key]
        else: 
            return [taskname]
    return None


def add_task_outputs(project, entity_type, entity_name, taskname, outputs=[]): 
    output_key = 'outputs'
    meta, task = get_task_meta(project, entity_type, entity_name, taskname)
    if meta: 
        if output_key in meta.keys(): 
            for output in outputs: 
                if not output in meta[output_key]: 
                    meta[output_key].append(output)      
        else: 
            meta.update({output_key: outputs})
    else: 
        meta = {output_key: outputs}
    return sg.update('Task', task['id'], {'sg_meta': str(meta)})


def remove_task_outputs(project, entity_type, entity_name, taskname, outputs=[]): 
    output_key = 'outputs'
    meta, task = get_task_meta(project, entity_type, entity_name, taskname)
    if meta: 
        if output_key in meta.keys(): 
            for output in outputs: 
                if output in meta[output_key]: 
                    meta[output_key].pop(meta[output_key].index(output))

            return sg.update('Task', task['id'], {'sg_meta': str(meta)})

