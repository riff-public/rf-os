import sys
import os
from collections import OrderedDict
import time
from datetime import datetime

import logging
logger = logging.getLogger(__name__)

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

from rf_utils.context import context_info
from rf_utils import file_utils
from rf_utils.sg import sg_utils
sg = sg_utils.sg
if not sg: 
    # reload(sg_utils)
    sg = sg_utils.sg

sgCache = dict()
sgCacheTask = dict()
sgCacheUser = dict()

class Config:
    typeMap = {'Asset': 'asset', 'Shot': 'scene'}
    cacheFile = 'sg_manager_cache.yml'
    cacheTimeOut = 300
    cacheEnable = True
    memoryCache = True


class GetEntity(QtCore.QThread):
    value = QtCore.Signal(list)

    def __init__(self, projectEntity, entityType, extraFilters=[]):
        QtCore.QThread.__init__(self)
        self.projectEntity = projectEntity
        self.entityType = entityType
        self.extraFilters = extraFilters

    def run(self):
        project = self.projectEntity.get('name') if type(self.projectEntity) == type(dict()) else self.projectEntity
        result = None
        if Config.memoryCache: 
            result = self.read_memory_cache(project)

        if not result: 
            filters = [['project', 'is', self.projectEntity]]
            # cache = self.cache_path(self.projectEntity.get('name'))

            if type(self.projectEntity) in [type(u''), type(str())]:
                filters = [['project.Project.name', 'is', self.projectEntity]]

            if self.extraFilters: 
                filters += self.extraFilters

            fields = get_fields(self.entityType)
            # if self.entityType == 'Asset':
            #     fields = ['project', 'type', 'code', 'id', 'sg_asset_type', 'sg_subtype', 'parents', 'tags', 'image']
            # if self.entityType == 'Shot':
            #     fields = ['project', 'type', 'code', 'id', 'sg_episode.Scene.code', 'sg_sequence_code', 'sg_shortcode', 'tags', 'image']

            result = sg.find(self.entityType, filters, fields)

            # if self.entityType == 'Asset': 
            #     episodeEntities = sg.find('Scene', filters, get_fields('Scene'))
            #     sequenceEntities = sg.find('Sequence', filters, get_fields('Sequence'))
            #     result = self.pack_results(result, episodeEntities, sequenceEntities)
            logger.debug('Fetching %s %s from Shotgun' % (project, self.entityType))
            # if Config.cacheEnable:
            #     self.write_cache(project, result)

            if Config.memoryCache:
                self.write_memory_cache(project, self.entityType, result)
        
        self.value.emit(result)

    def pack_results(self, assetEntities, episodeEntities, sequenceEntities): 
        episode_dict = dict()
        sequence_dict = dict()
        assets = list()

        for ep in episodeEntities: 
            episode_dict[ep['id']] = ep 
        for seq in sequenceEntities: 
            sequence_dict[seq['id']] = seq

        for asset in assetEntities: 
            episodes = asset.get('sg_scenes')
            if episodes: 
                asset['sg_scenes'] = [episode_dict[a['id']] for a in episodes]
            sequences = asset.get('sequences')
            if sequences: 
                asset['sequences'] = [sequence_dict[a['id']] for a in sequences]
            assets.append(asset)

        return assets

    def cache_path(self, project): 
        return '%s/%s/_data/%s' % (os.environ.get('RFPROJECT'), project, Config.cacheFile) 

    def write_cache(self, project, data): 
        writeData = {self.entityType: data}
        path = self.cache_path(project)
        os.makedirs(os.path.dirname(path)) if not os.path.exists(os.path.dirname(path)) else None
        file_utils.ymlDumper(path, writeData)
        return path

    def read_data(self, project): 
        path = self.cache_path(project)
        data = dict()
        if os.path.exists(path): 
            if not self.check_cache_expire(path): 
                data = file_utils.ymlLoader(path)
                data = data.get(self.entityType, dict())
                logger.debug('read from cache')
            else: 
                logger.debug('cache expired')
        return data 

    def check_cache_expire(self, path): 
        if os.path.exists(path): 
            fileTime = os.path.getmtime(path)
            now = time.time()
            fileAge = now - fileTime
            # print 'fileAge', now, fileTime, fileAge
            
            return True if fileAge > Config.cacheTimeOut else False 
        else: 
            return True

    def check_memory_expired(self, sgTime): 
        now = time.time()
        cacheAge = now - sgTime
        return True if cacheAge > Config.cacheTimeOut else False 

    def read_memory_cache(self, project): 
        data = sgCache.get(project, dict()).get(self.entityType)
        if data: 
            sgDict = data['data']
            cacheTime = data['timestamp']

            if not self.check_memory_expired(cacheTime): 
                logger.debug('Read from memoryCache')
                return sgDict
            else: 
                logger.debug('%s %s memoryCache expired' % (project, self.entityType))


    def write_memory_cache(self, project, entityType, data): 
        global sgCache
        writeDict = {'data': data, 'timestamp': time.time()}

        if project in sgCache.keys(): 
            if entityType in sgCache[project].keys(): 
                # add data to existing data 
                sgCache[project][entityType] = writeDict 
            else: 
                # add entity to existing project
                sgCache[project] = {entityType: writeDict}
        else: 
            # add to empty cache
            sgCache = {project: {entityType: writeDict}}



class GetOneEntity(QtCore.QThread):
    value = QtCore.Signal(dict)

    def __init__(self, projectName, entityType, entityName):
        QtCore.QThread.__init__(self)
        self.projectName = projectName
        self.entityType = entityType
        self.entityName = entityName

    def run(self):
        filters = [['project.Project.name', 'is', self.projectName], ['code', 'is', self.entityName]]
        fields = get_fields(self.entityType)
        result = sg.find_one(self.entityType, filters, fields)
        self.value.emit(result)


class GetContext(QtCore.QThread):
    value = QtCore.Signal(context_info.ContextPathInfo)

    def __init__(self, project):
        QtCore.QThread.__init__(self)
        self.project = project

    def run(self):
        context = context_info.Context()
        context.update(project=self.project, entityType='asset')
        entity = context_info.ContextPathInfo(context=context)
        self.value.emit(entity)


class GetTags(QtCore.QThread):
    value = QtCore.Signal(list)

    def __init__(self):
        QtCore.QThread.__init__(self)

    def run(self):
        entities = sg.find('Tag', [], ['name'])
        self.value.emit(entities)


class GetTasks(QtCore.QThread):
    value = QtCore.Signal(list)

    def __init__(self, projectEntity, entity, step='', refresh=False):
        QtCore.QThread.__init__(self)
        self.projectEntity = projectEntity 
        self.entity = entity 
        self.step = step 
        self.refresh = refresh

    def run(self):
        assetName = self.entity.get('code')
        if self.refresh: 
            if assetName in sgCacheTask.keys(): 
                sgCacheTask.pop(assetName)
                logger.debug('Refreshing ...')

        if not assetName in sgCacheTask.keys(): 
            logger.debug('Fetching from Shotgun ...')
            filters = [['project', 'is', self.projectEntity], ['entity', 'is', self.entity]]
            fields = ['sg_status_list', 'content', 'step', 'entity', 'task_assignees']
            if self.step: 
                filters.append(['step.Step.code', 'is', self.step])
            entities = sg.find('Task', filters, fields)

            # add to cache 
            sgCacheTask[assetName] = entities
        else: 
            logger.debug('Reading cache ...')
        
        self.value.emit(sgCacheTask[assetName])
        logger.debug('Finished %s' % sgCacheTask[assetName])


class GetUsers(QtCore.QThread):
    """ get users in shotgun """
    value = QtCore.Signal(list)

    def __init__(self, refresh=False):
        QtCore.QThread.__init__(self)

    def run(self):
        if 'user' in sgCacheUser.keys(): 
            logger.debug('Loading Cache user')
            userEntities = sgCacheUser['user']
        else: 
            logger.debug('Fetching user shotgun ...')
            filters = []
            fields = ['name', 'groups', 'department', 'sg_ad_account', 'sg_localuser', 'sg_status_list', 'sg_name']
            userEntities = sg.find('HumanUser', filters, fields)
            sgCacheUser['user'] = userEntities

        self.value.emit(userEntities)
        logger.debug('Finished %s' % userEntities)


class GetEpisodes(QtCore.QThread):
    """ get users in shotgun """
    value = QtCore.Signal(list)

    def __init__(self, projectEntity=None, project=None, refresh=False):
        QtCore.QThread.__init__(self)
        self.projectEntity = projectEntity
        self.project = project

    def run(self):
        logger.debug('Fetching episodes from shotgun ...')
        if self.projectEntity: 
            projectFilter = ['project', 'is', self.projectEntity]
        if self.project: 
            projectFilter = ['project.Project.name', 'is', self.project]

        filters = [projectFilter]
        fields = ['code', 'id', 'project']
        episodeEntities = sg.find('Scene', filters, fields)

        self.value.emit(episodeEntities)
        logger.debug('Finished %s' % episodeEntities)


class CreateTask(QtCore.QThread):
    value = QtCore.Signal(list)

    def __init__(self, projectEntity, entity, step, taskName, res=''):
        QtCore.QThread.__init__(self)
        self.projectEntity = projectEntity 
        self.entity = entity 
        self.step = step 
        self.taskName = taskName
        self.res = res 

    def run(self):
        stepEntity = sg.find_one('Step', [['code', 'is', self.step]], ['code', 'id'])
        data = {'project': self.projectEntity,
                        'entity': self.entity,
                        'step': stepEntity,
                        'content': self.taskName, 
                        'sg_type': 'Asset'}
        if self.res: 
            data['sg_resolution'] = self.res 
            
        taskEntity = sg.create('Task', data)
        self.value.emit(taskEntity)
        logger.debug('Task craeted: %s' % taskEntity)


class UpdateEntity(QtCore.QThread):
    value = QtCore.Signal(context_info.ContextPathInfo)

    def __init__(self, sgEntity):
        QtCore.QThread.__init__(self)
        self.sgEntity = sgEntity
        self.typeMap = Config.typeMap

    def run(self):
        entity = update_entity(self.sgEntity)
        # stepPath = entity.path.step().abs_path()
        self.value.emit(entity)

class FetchIcon(QtCore.QThread):
    value = QtCore.Signal(str)

    def __init__(self, sgEntities):
        QtCore.QThread.__init__(self)
        self.sgEntities = sgEntities

    def run(self):
        for sgEntity in self.sgEntities:
            entity = update_entity(sgEntity)
            id = sgEntity['id']
            path = entity.path.icon().abs_path()
            iconName = entity.icon_name(ext='.png')
            iconPath = '%s/%s' % (path, iconName)
            self.value.emit([id, iconPath])

class FetchVersions(QtCore.QThread):
    value = QtCore.Signal(list)

    def __init__(self, sgEntities):
        QtCore.QThread.__init__(self)
        self.sgEntities = sgEntities

    def run(self):
        versions = self.get_entity_versions(self.sgEntities)
        self.value.emit(versions) if versions else None

    def get_entity_versions(self, entity):
        filters = [['entity', 'is', entity]]
        fields = ['sg_task', 'entity', 'entity.Asset.code', 'sg_task.Task.step.Step.code', 'code', 'description', 'sg_path_to_publish', 'sg_path_to_hero', 'sg_path_to_workspace', 'description', 'sg_recommended_file', 'sg_data']
        return sg.find('Version', filters, fields)


class DownloadImage(QtCore.QThread):
    value = QtCore.Signal(str)
    status = QtCore.Signal(str)
    update = QtCore.Signal(list)

    def __init__(self, downloadList):
        QtCore.QThread.__init__(self)
        self.downloadList = downloadList

    def run(self):
        versions = self.get_entity_versions(self.downloadList)

    def get_entity_versions(self, downloadList):
        try:
            from urllib import urlopen, urlretrieve
        except ImportError:
            from urllib.request import urlretrieve  # Python3

        for i, data in enumerate(downloadList):
            item, url, dst = data
            jpg = '%s.jpg' % os.path.splitext(dst)[0]
            message = 'Downloading ...(%s/%s) %s' % (i+1, len(downloadList), dst)

            if not os.path.exists(os.path.dirname(dst)):
                os.makedirs(os.path.dirname(dst))

            urlretrieve(url, jpg)
            convert_format(jpg, dst)
            os.remove(jpg)
            self.status.emit(message)
            self.update.emit([item, dst])
        self.status.emit('Ready')


class UpdateItems(QtCore.QThread):
    value = QtCore.Signal(str)
    status = QtCore.Signal(str)
    update = QtCore.Signal(list)

    def __init__(self, ui, sgEntities):
        QtCore.QThread.__init__(self)
        self.ui = ui
        self.sgEntities = sgEntities

    def run(self):
        self.ui.entityWidget.add_items(self.sgEntities, widget=False)


def get_fields(entityType): 
    if entityType == 'Asset':
        fields = ['project', 'type', 'code', 'id', 'sg_asset_type', 'sg_subtype', 'parents', 'tags', 'image', 'sg_scenes', 'sequences', 'shots']
    if entityType == 'Shot':
        fields = ['project', 'type', 'code', 'id', 'sg_episode.Scene.code', 'sg_sequence_code', 'sg_shortcode', 'tags', 'image']
    if entityType == 'Scene': 
        fields = ['code', 'sg_priority']
    if entityType == 'Sequence': 
        fields = ['code', 'sg_priority', 'sg_sequence_code', 'sg_episode']
    return fields


def convert_format(format1, format2):
    try:
        from PIL import Image
    except ImportError:
        from PIL_Maya import Image
    im = Image.open(format1)
    im.save(format2)


def get_type_episode(sgEntity):
    if sgEntity['type'] == 'Asset':
        return 'sg_asset_type'

    if sgEntity['type'] == 'Shot':
        return 'sg_episode.Scene.code'

def get_subtype_seq(sgEntity):
    if sgEntity['type'] == 'Asset':
        return 'sg_subtype'

    if sgEntity['type'] == 'Shot':
        return 'sg_sequence_code'

def get_shortcode(sgEntity):
    if sgEntity['type'] == 'Asset':
        return 'code'

    if sgEntity['type'] == 'Shot':
        return 'sg_shortcode'

def update_entity(sgEntity):
    context = context_info.Context()
    if not sgEntity[get_type_episode(sgEntity)]: 
        logger.error('No "%s" found' % get_type_episode(sgEntity))
    if not sgEntity[get_subtype_seq(sgEntity)]: 
        logger.error('No "%s" found' % get_subtype_seq(sgEntity))    
    if not sgEntity[get_shortcode(sgEntity)]: 
        logger.error('No "%s" found' % get_shortcode(sgEntity))

    context.update(project=sgEntity['project']['name'],
                    entityType=Config.typeMap[sgEntity['type']],
                    entity=sgEntity['code'],
                    entityGrp=sgEntity[get_type_episode(sgEntity)],
                    entityParent=sgEntity[get_subtype_seq(sgEntity)],
                    entityCode=sgEntity[get_shortcode(sgEntity)] or ''
                    )
    entity = context_info.ContextPathInfo(context=context)
    return entity

def list_dir(path):
    return ['%s/%s' % (path, a) for a in file_utils.listFolder(path)]

def list_file(path):
    return ['%s/%s' % (path, a) for a in file_utils.listFile(path)]

def calculate_version(workspacePath):
    return file_utils.calculate_version(file_utils.list_file(workspacePath))


def get_rig(entity):
    entity.context.update(step='rig', process='main', res='md')

    heroPath = entity.path.hero()
    fileName = entity.publish_name(hero=True)
    rigFile = '%s/%s' % (heroPath, fileName)

    # restore
    entity.context.update(step=step)
    return rigFile


def get_proxy(entity):
    entity.context.update(step='rig', process='main', res='pr')

    heroPath = entity.path.hero()
    fileName = entity.publish_name(hero=True)
    rigFile = '%s/%s' % (heroPath, fileName)

    # restore
    entity.context.update(step=step, res='md')
    return rigFile
