from Qt import QtWidgets, QtCore
from . import sg_process 
from rf_utils.context import context_info 
sg = sg_process.sg 


class TaskDialog(QtWidgets.QDialog):
	"""docstring for TaskDialog"""
	def __init__(self, entity):
		super(TaskDialog, self).__init__()
		# UI 
		self.layout = QtWidgets.QGridLayout()
		self.title = QtWidgets.QLabel('Please select task you are working')
		self.label = QtWidgets.QLabel('Tasks: ')
		self.task_comboBox = QtWidgets.QComboBox()
		self.button = QtWidgets.QPushButton('Set Task')
		self.layout.addWidget(self.title, 0, 0, 1, 2)
		self.layout.addWidget(self.label, 1, 0, 1, 1)
		self.layout.addWidget(self.task_comboBox, 1, 1, 1, 1)
		self.layout.addWidget(self.button, 2, 0, 1, 2)
		self.setLayout(self.layout)
		self.layout.setColumnStretch(0, 1)
		self.layout.setColumnStretch(1, 2)
		self.setWindowTitle('Select Task')

		self.entity = entity
		self.list_tasks()
		self.button.clicked.connect(self.submit)

	def list_tasks(self): 
		tasks = find_tasks(self.entity)
		for i, task in enumerate(tasks): 
			self.task_comboBox.addItem(task['content'])
			self.task_comboBox.setItemData(i, task, QtCore.Qt.UserRole)

	def submit(self): 
		index = self.task_comboBox.currentIndex()
		task = self.task_comboBox.itemData(index, QtCore.Qt.UserRole)
		self.entity.context.update(task=task['content'])
		self.entity.write_env()
		self.close()


def init_env_context(): 
	entity = context_info.ContextPathInfo()
	if entity.context.has_env(): 
		entity.use_env_context()
	return entity 


def find_tasks(entity): 
	link_entity = 'entity.{}.code'.format(entity.sg_entity_type)
	filters = [
		['project.Project.name', 'is', entity.project], 
		[link_entity, 'is', entity.name], 
		['step.Step.code', 'is', entity.step]
		]
	fields = ['content', 'id']
	tasks = sg.find('Task', filters, fields)
	return tasks 


def check(): 
	entity = init_env_context()
	if not entity.task: 
		dialog = TaskDialog(entity)
		dialog.exec_()
		