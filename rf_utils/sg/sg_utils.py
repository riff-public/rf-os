import sys
import os
from collections import OrderedDict

import logging
logger = logging.getLogger(__name__)

import rf_config as config
from shotgun_api3 import Shotgun
from shotgun_api3 import shotgun

# connection to server
server = config.Shotgun.server
script = config.Shotgun.script
id = config.Shotgun.id
sg = None
ca_certs = '{}/{}'.format(os.path.dirname(shotgun.__file__.replace('\\', '/')), config.Shotgun.ca_certs)

try: 
    connection = True
    sg = Shotgun(server, script, id, ca_certs=ca_certs)
except Exception as e: 
    print(e)
    logger.error(e)
    connection = False

# sg = None

def init_key(inputKey):
    script, id = config.Shotgun.getKey(inputKey)
    global sg
    sg = Shotgun(server, script, id, ca_cert=ca_cert)


# sg utilities =================================================================================

def find_project(projectName, fields=None):
    """ projectName string """
    defaultFields = ['id', 'name']
    fields = fields + defaultFields if fields else defaultFields
    return sg.find_one('Project', [['name', 'is', projectName]], fields)


def find_all_assets(projectEntity, fields=None):
    """ find assets """
    # filters
    defaultFields = ['code', 'sg_asset_type', 'mainAsset', 'sg_subtype', 'project']
    filters = [['project', 'is', projectEntity]]
    fields = defaultFields + fields if fields else defaultFields

    # find
    entities = sg.find('Asset', filters, fields)

    # order data
    entityDict = ordered_entity_dict(entities, attr='code')
    orderEntities = [v for k, v in entityDict.iteritems()] # back to list
    return orderEntities

def find_task(projectName='', projectEntity=None, step='', taskName='', fields=None):
    """ find task id """
    defaultFields = ['id', 'content', 'project', 'step', 'entity']
    fields = fields + defaultFields if fields else defaultFields
    filters = []

    # project
    filters.append(['project.Project.name', 'is', projectName]) if projectName else filters.append(['project', 'is', projectEntity]) if projectEntity else None
    filters.append(['step.Step.name', 'is', step])
    filters.append(['content', 'is', taskName])

    return sg.find_one(filters, fields)


def list_steps(entityType): 
    filters = [['entity_type', 'is', entityType]]
    fields = ['code', 'short_name', 'entity_type', 'id']
    return sg.find('Step', filters, fields)


def find_step(name, entityType): 
    filters = [['entity_type', 'is', entityType], ['code', 'is', name]]
    fields = ['code', 'short_name', 'entity_type', 'id']
    return sg.find_one('Step', filters, fields)



# data grouping ================================================================================
class Config:
    noData = 'No data'

def ordered_entity_dict(entities, attr='code'):
    """ make entities data sorted by input attr """
    data = OrderedDict()
    assetNames = sorted([a[attr] for a in entities])

    for assetName in assetNames:
        for entity in entities:
            if assetName == entity[attr]:
                data[assetName] = entity
    return data


def group_entity_attr(entities, attr):
    attrValues = list(set([a[attr] for a in entities]))
    data = OrderedDict()
    data['all'] = entities
    for attrValue in attrValues:
        data[attrValue] = []
        for entity in entities:
            if entity[attr] == attrValue:
                data[attrValue].append(entity)
    return data


def find_attr_relation(entities, attr1, attr2):
    """ find relation between 2 attrs (type, subtype) relation """
    types = sorted(list(set([a[attr1] for a in entities])))
    subtypes = sorted(list(set([a[attr2] for a in entities])))
    data = OrderedDict()

    for assetType in types:
        if not assetType:
            assetType = Config.noData
        data[assetType] = []
        for entity in entities:
            if entity[attr1] == assetType:
                subtype = entity[attr2]
                if not subtype:
                    subtype = Config.noData
                if not subtype in data[assetType]:
                    data[assetType].append(subtype)

    return data

def filter_entities(entities, filter1, filter2):
    """ find entities from 2 input filters """
    data = []
    filter1Value, filter1Attr = filter1
    filter2Value, filter2Attr = filter2

    filter1Value = filter1Value if not filter1Value == Config.noData else None
    filter2Value = filter2Value if not filter2Value == Config.noData else None

    for entity in entities:
        valid = False
        if filter1Value == entity[filter1Attr] or not filter1Value:
            if filter2Attr:
                if filter2Value == entity[filter2Attr] or not filter2Value:
                    data.append(entity)
                # if no data
                elif filter2Value == Config.noData and not entity[filter2Attr]:
                    data.append(entity)
            else:
                data.append(entity)
    return data

def asset_info(entity):
    data = dict()
    project = entity.get('project', dict()).get('name')
    assetType = entity.get('sg_asset_type')
    assetName = entity.get('code')
    assetSubType = entity.get('sg_subtype')
    mainAsset = entity.get('sg_mainasset')
    id = entity.get('id')
    data = {'project': project, 'assetType': assetType, 'assetSubType': assetSubType, 'name': assetName, 'assetParent': mainAsset}

    return data

def get_tags(entities):
    tagDict = OrderedDict()

    for entity in entities:
        entityTagDict = get_entity_tags(entity)

        for k, v in entityTagDict.iteritems():
            if not k in tagDict.keys():
                tagDict[k] = v

    return tagDict

def get_entity_tags(entity):
    tagDict = OrderedDict()
    tags = entity.get('tags') or dict()

    for tag in tags:
        if not tag['name'] in tagDict.keys():
            tagDict[tag['name']] = tag

    return tagDict


def get_all_projects():
    projects = sg.find('Project', [['sg_status', 'is', 'Active']], ['name', 'sg_project_code'])
    return projects


def get_episodes_by_project(projectEntity):
    episodes = sg.find('Scene', [['project', 'is', projectEntity]], ['code', 'id', 'project'])
    return episodes

def get_char():
    types = ['char']
    return types


def get_all_users():
    order_name = [{'field_name': 'name', 'direction': 'asc'}]
    users = sg.find('HumanUser', [], ["name", "department"], order_name)
    return users


def get_all_departments():
    order_name = [{'field_name': 'name', 'direction': 'asc'}]
    departments = sg.find("Department", [], ["name"], order_name)
    return departments

def read_asset_data(project, assetName): 
    filters = [['project.Project.name', 'is', project], ['code', 'is', assetName]]
    fields = ['sg_data']
    result = sg.find_one('Asset', filters, fields)
    return result.get('sg_data')

def read_task_data(project, assetName, taskName): 
    filters = [['project.Project.name', 'is', project], ['entity.Asset.code', 'is', assetName], ['content', 'is', taskName]]
    fields = ['sg_data']
    result = sg.find_one('Task', filters, fields)
    return result.get('sg_data')

def write_asset_data(project, assetName, data): 
    filters = [['project.Project.name', 'is', project], ['entity.Asset.code', 'is', assetName], ['content', 'is', taskName]]
    fields = ['sg_data', 'id']
    result = sg.find_one('Task', filters, fields)
    return sg.update('Asset', result.get('id'), data)

def write_task_data(project, assetName, data): 
    filters = [['project.Project.name', 'is', project], ['entity.Asset.code', 'is', assetName], ['content', 'is', taskName]]
    fields = ['sg_data']
    result = sg.find_one('Task', filters, fields)
    return sg.update('Task', result.get('id'), data)

# # example usages
# projectEntities = find_project('projectName')
# entities = find_all_assets(projectEntities)
# relation = find_attr_relation(entities, 'sg_asset_type', 'sg_subtype')
# filterEntities = filter_entities(entities, ('char', 'sg_asset_type'), ('all', 'sg_subtype'))