# -*- coding: utf-8 -*-
import sys
import os
import logging

logger = logging.getLogger(__name__)

if os.environ.get('RFSCRIPT', False):
    path = '%s/core' % os.environ.get('RFSCRIPT')
    if not path in sys.path:
        sys.path.append(path)

import rf_config as config
from shotgun_api3 import Shotgun
from shotgun_api3 import shotgun
from shotgun_api3 import ShotgunError
from rf_utils import custom_exception

# connection to server
script_name = 'rf_user'
server = config.Shotgun.server
script = config.Shotgun.getKey[script_name]['script']
id = config.Shotgun.getKey[script_name]['id']

sg = None
ca_certs = '{}/{}'.format(os.path.dirname(shotgun.__file__.replace('\\', '/')), config.Shotgun.ca_certs)
error = ShotgunError

try:
    sg = Shotgun(server, script, id, ca_certs=ca_certs)
    connection = True
except shotgun.SSLHandshakeError as e:
    logger.error(e)
    connection = False


class FieldSetting:
    attrs = ['sg_name', 'sg_status_list', 'department', 'sg_ad_account', 'sg_hr_local_name', 
    'sg_employee_id', 'groups', 'sg_employee_department', 'sg_languages', 'sg_local_name', 
    'sg_active', 'sg_localuser', 'login', 'email', 'permission_rule_set']

""" Example values 

sg_name: Nick name e.g. Ta
sg_status_list: dis (always)
department: 
            'art', 'id': 1, 'name': 'Art'
            'anim', 'id': 2, 'name': 'Anim'
            'design', 'id': 3, 'name': 'Design'
            'PRG', 'id': 4, 'name': 'Programming'
            'light', 'id': 5, 'name': 'Lighting'
            'prod', 'id': 6, 'name': 'Production'
            'QA', 'id': 7, 'name': 'QA'
            'layout', 'id': 8, 'name': 'Layout'
            'model', 'id': 41, 'name': 'Model'
            'rig', 'id': 42, 'name': 'Rig'
            'lookdev', 'id': 43, 'name': 'Lookdev'
            'groom', 'id': 44, 'name': 'Groom'
            'set', 'id': 45, 'name': 'Set'
            'comp', 'id': 46, 'name': 'Comp'
            'sim', 'id': 47, 'name': 'Sim'
            'techanim', 'id': 48, 'name': 'TechAnim'
            'fx', 'id': 49, 'name': 'FX'
            'setdress', 'id': 78, 'name': 'Setdress'
            'pipeline', 'id': 111, 'name': 'Pipeline'
            'realtime', 'id': 144, 'name': 'Realtime'
            'edit', 'id': 177, 'name': 'Edit'
            'hr', 'id': 210, 'name': 'HR'
sg_ad_account: login ad name e.g. chanon.v
sg_hr_local_name: Thai name e.g. ชานนท์ วิไลยุค
sg_employee_id: company employee's id
groups: 
        'Shotgun', 'id': 1
        'Editorial', 'id': 3
        'Production', 'id': 4
        'Pipeline', 'id': 5
        'Layout', 'id': 6
        'Animator', 'id': 7
        'modeler', 'id': 40
        'rigger', 'id': 41
        'lighter', 'id': 42
        'lookdev', 'id': 43
        'setdressor', 'id': 44
        'comper', 'id': 45
        'fx', 'id': 46
        'groomer', 'id': 47
        'art', 'id': 48
        'sim', 'id': 49
        'tech', 'id': 50
        'coordinator', 'id': 51
        'producer', 'id': 52
sg_employee_department: แผนกตาม HR - บัญชี
                        Art
                        Model
                        Rig
                        Groom
                        SLR
                        Layout
                        Anim
                        Light
                        Comp
                        CFX

sg_languages: Thai, English
sg_local_name: ชื่อที่แจ้งจังจะเรียก e.g. พี่ต้า 
sg_active: ยังทำงานอยู่หรือไม่ True / False 
sg_localuser: ชื่อที่จะ save ลงไฟล์ e.g. Ta 
login: Shotgun user account (obsolete)
email: company's email e.g. chanon.v@riff-studio.com
permission_rule_set: always set to "artist"
                    'admin', 'id': 5
                    'api_admin', 'id': 6
                    'manager', 'id': 7
                    'artist', 'id': 8
                    'admin_system_default', 'id': 9
                    'api_admin_system_default', 'id': 10
                    'manager_system_default', 'id': 11
                    'artist_system_default', 'id': 12
                    'vendor', 'id': 13
                    'client_user_system_default', 'id': 14
                    'client_user', 'id': 15
                    'china', 'id': 17
                    'guess', 'id': 49
                    'producer', 'id': 50
                    'supervisor', 'id': 51
                    'custom_artist', 'id': 52
                    'hr', 'id': 53
"""

class Config: 
    default_status = 'dis'
    default_active = True
    default_rule = 'artist'  # hardcode for later permission
    email_domain = '@riff-studio.com'
    # department_mapping = dict([(i.get('name'), i) for i in sg.find('Department', [], ['name', 'sg_groups'])])
    department_mapping = {
                    'Art': {'department': 'art', 'groups': 'Art'},
                    'Model': {'department': 'model', 'groups': 'modeler'}, 
                    'Rig':  {'department': 'rig', 'groups': 'rigger'}, 
                    'Groom': {'department': 'groom', 'groups': 'groomer'}, 
                    'Lookdev': {'department': 'lookdev', 'groups': 'lookdev'},
                    'Layout': {'department': 'layout', 'groups': 'Layout'}, 
                    'Anim': {'department': 'anim', 'groups': 'Animator'}, 
                    'Light': {'department': 'light', 'groups': 'lighter'}, 
                    'Comp': {'department': 'comp', 'groups': 'comper'}, 
                    'CFX': {'department': 'sim', 'groups': 'sim'}, 
                    'FX': {'department': 'fx', 'groups': 'fx'}, 
                    'Management': {'department': 'management', 'groups': 'producer'}, 
                    'Production': {'department': 'production', 'groups': 'producer'},
                    'IP Management': {'department': 'ip management', 'groups': 'ip management'},
                    'HR': {'department': 'HR', 'groups': 'producer'},
                    'Edit': {'department': 'edit', 'groups': 'Editorial'}, 
                    'Pipeline': {'department': 'pipeline', 'groups': 'Pipeline'}, 
                    'Realtime': {'department': 'realtime', 'groups': 'tech'}
    }


    # ['sg_name', 'sg_status_list', 'department', 'sg_ad_account', 'sg_hr_local_name', 
    #     'sg_employee_id', 'groups', 'sg_employee_department', 'sg_languages', 'sg_local_name', 
    #     'sg_active', 'sg_localuser', 'login', 'email', 'permission_rule_set']


def create(nickname, local_nickname, english_name, english_lastname, local_name, local_lastname, department_name, employee_id, language='Thai'): 
    nickname = nickname.lower().capitalize()
    employee_id = str(employee_id)
    sg_employee_department = department_name 
    ad_name = get_ad_account(english_name, english_lastname)
    english_fullname = '{} {}'.format(english_name, english_lastname)
    local_fullname = unicode(local_name) + u' ' + unicode(local_lastname)
    jangjung_call = u'พี่' + unicode(local_nickname)
    email = '{}{}'.format(ad_name, Config.email_domain)
    rule_entity = find_permission_rule(Config.default_rule)
    studio = 'Riff'

    if department_name in Config.department_mapping.keys(): 
        step_dict = Config.department_mapping.get(department_name)
        department_entity = find_department(step_dict['department'])
        group_entity = find_group(step_dict['groups'])

    else: 
        message = 'Department "{}" not allowed. Valid departments are {}'.format(department_name, Config.department_mapping.keys())
        raise custom_exception.PipelineError(message)

    data = {
            'name': nickname, 'sg_name': nickname, 'sg_status_list': Config.default_status, 
            'department': department_entity, 'sg_ad_account': ad_name, 
            'sg_hr_local_name': local_fullname, 'sg_employee_id': employee_id, 
            'groups': [group_entity], 'sg_employee_department': sg_employee_department, 
            'sg_languages': language, 'sg_local_name': jangjung_call, 
            'sg_active': Config.default_active, 'sg_localuser': nickname, 
            'email': email, 'permission_rule_set': rule_entity, 'sg_studio': studio}

    logger.info('Creating user ...')
    logger.debug(data)
    result = sg.create('HumanUser', data)
    logger.info('Create user success')
    logger.debug(result)

    return result 


def department_list(): 
    return Config.department_mapping.keys()


def get_ad_account(name, lastname): 
    return '{}.{}'.format(name.lower(), lastname.lower()[0])


def find_department(name): 
    return sg.find_one('Department', [['code', 'is', name]], ['name', 'code'])


def list_departments(): 
    return sg.find('Department', [], ['name', 'code'])


def list_groups(): 
    return sg.find('Group', [], ['code'])


def find_group(name): 
    return sg.find_one('Group', [['code', 'is', name]], ['code'])


def list_permission_rules(): 
    return sg.find('PermissionRuleSet', [], ['code'])


def find_permission_rule(name): 
    return sg.find_one('PermissionRuleSet', [['code', 'is', name]], ['code'])



def test(): 
    nickname = 'Jangjung'
    local_nickname = 'แจ้งจัง'
    english_name = 'Jang'
    english_lastname = 'Jung'
    local_name = 'แจ้ง'
    local_lastname = 'จัง'
    department_list = ['Art', 'Model', 'Rig', 'Groom', 'Layout', 'Anim', 'Light', 'Comp', 'CFX', 'FX', 'Management', 'Edit', 'Pipeline']
    department_name = department_list[-1]
    employee_id = 5301110
    launguage = 'Thai'
    result = create(nickname, local_nickname, english_name, english_lastname, 
        local_name, local_lastname, department_name, employee_id, launguage)
  