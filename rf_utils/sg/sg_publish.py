import sys
import os
from collections import OrderedDict

import logging
logger = logging.getLogger(__name__)

import rf_config as config
from shotgun_api3 import Shotgun
from shotgun_api3 import shotgun

# connection to server
server = config.Shotgun.server
script = config.Shotgun.script
id = config.Shotgun.id
sg = None
ca_certs = '{}/{}'.format(os.path.dirname(shotgun.__file__.replace('\\', '/')), config.Shotgun.ca_certs)

try: 
    connection = True
    sg = Shotgun(server, script, id, ca_certs=ca_certs)
except shotgun.SSLHandshakeError as e: 
    connection = False

# sg = None

def init_key(inputKey):
    script, id = config.Shotgun.getKey(inputKey)
    global sg
    sg = Shotgun(server, script, id)

class FieldSetting: 
    project = ['name', 'id', 'sg_project_code']
    projectAll = project + ['sg_frame_rate', 'sg_status', 'sg_description', 'sg_resolution']
    asset = ['code', 'id']
    step = ['code', 'short_name', 'entity_type', 'id']
    task = ['content', 'id', 'entity']


def create_version(name, project, entityType, entity, task, step, user, **kwargs): 
    projectEntity = get_project(project)
    entity = get_entity(projectEntity, entityType, entity)
    stepEntity = get_step(step, entityType)
    taskEntity = get_task(projectEntity, entity, stepEntity, task)
    userEntity = get_user(user)

    data = {'project': projectEntity,
            'entity': entity, 
            'sg_task': taskEntity, 
            'code': name, 
            'user': userEntity
            }
    versionEntity = sg.create('Version', data)
    return versionEntity


def get_project(project): 
    if isinstance(project, dict): 
        return project
    filters = [['name', 'is', project]]
    return sg.find_one('Project', filters, FieldSetting.project)


def get_entity(projectEntity, entityType, entity): 
    if isinstance(entity, dict): 
        return entity
    filters = [['project', 'is', projectEntity], ['code', 'is', entity]]
    return sg.find_one(entityType, filters, FieldSetting.asset)


def get_step(step, entityType): 
    if isinstance(step, dict): 
        return step
    filters = [['entity_type', 'is', entityType], ['code', 'is', step]]
    return sg.find_one('Step', filters, FieldSetting.step)


def get_task(projectEntity, entity, stepEntity, task): 
    if isinstance(task, dict): 
        return task
    filters = [['project', 'is', projectEntity], ['entity', 'is', entity], ['step', 'is', stepEntity], ['content', 'is', task]]
    return sg.find_one('Task', filters, FieldSetting.task)


def get_user(user): 
    if isinstance(user, dict): 
        return user 
    user = user_info.User()
    return user.sg_user()
