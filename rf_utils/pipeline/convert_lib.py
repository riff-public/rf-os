import os
import sys
import subprocess
import math
import tempfile
import shutil
import json

import rf_config as config
from rf_utils import file_utils
from rf_utils import audio_utils

si = subprocess.STARTUPINFO()
si.dwFlags |= subprocess.STARTF_USESHOWWINDOW

VIDEO_EXT = ('.mov', '.mp4', '.mpg')
IMG_EXT = ('.jpg', '.png', '.tif', '.tiff')

def img_to_mov(images, dst):
    # first_file_name, first_frame, first_ext = images[0].split('.')
    filename, first_ext = os.path.splitext(images[0])
    first_ext = first_ext[1:]
    first_file_name, first_frame = os.path.splitext(filename)
    first_frame = first_frame[1:]
    subprocess.call([
            config.Software.ffmpeg,
            '-r',
            '24',
            '-start_number',
            '{first_frame}'.format(first_frame=first_frame),
            '-i',
            '{first_file_name}.%04d.{first_ext}'.format(first_file_name=first_file_name, first_ext=first_ext),
            '-vcodec',
            'libx264',
            '-vprofile',
            'baseline',
            '-crf',
            '22',
            '-bf',
            '0',
            '-pix_fmt',
            'yuv420p',
            '-f',
            'mov',
            '{first_file_name}'.format(first_file_name=dst)
        ])

def img_to_mov2(images, dst):
    # first_file_name, first_frame, first_ext = images[0].split('.')
    filename, first_ext = os.path.splitext(images[0])
    first_ext = first_ext[1:]
    first_file_name, first_frame = os.path.splitext(filename)
    first_frame = first_frame[1:]
    subprocess.call([
            config.Software.ffmpeg,
            '-y',
            '-r',
            '24',
            '-start_number',
            '{first_frame}'.format(first_frame=first_frame),
            '-i',
            '{first_file_name}.%04d.{first_ext}'.format(first_file_name=first_file_name, first_ext=first_ext),
            '-vcodec',
            'libx264',
            '-vprofile',
            'baseline',
            '-vf',
            'scale=trunc(iw/2)*2:trunc(ih/2)*2',
            '-crf',
            '22',
            '-bf',
            '0',
            '-pix_fmt',
            'yuv420p',
            '-f',
            'mov',
            '{first_file_name}'.format(first_file_name=dst)
        ], startupinfo=si)

def img_varysize_to_mov(images, dst, width=1920, height=1080):
    filename, first_ext = os.path.splitext(images[0])
    first_ext = first_ext[1:]
    first_file_name, first_frame = os.path.splitext(filename)
    first_frame = first_frame[1:]
    subprocess.call([
            config.Software.ffmpeg,
            '-y',
            '-r',
            '24',
            '-start_number',
            '{first_frame}'.format(first_frame=first_frame),
            '-i',
            '{first_file_name}.%04d.{first_ext}'.format(first_file_name=first_file_name, first_ext=first_ext),
            '-vcodec',
            'libx264',
            '-vprofile',
            'baseline',
            '-vf',
            'scale=w={width}:h={height}:force_original_aspect_ratio=1,pad={width}:{height}:(ow-iw)/2:(oh-ih)/2'.format(width=width, height=height),
            '-crf',
            '22',
            '-bf',
            '0',
            '-pix_fmt',
            'yuv420p',
            '-f',
            'mov',
            '{first_file_name}'.format(first_file_name=dst)])

def limit_media_size(input_path, limit_size, output_path=None):
    fn, ext = os.path.splitext(input_path)
    ext = ext.lower()
    if ext not in VIDEO_EXT + IMG_EXT:
        print('Cannot resize media: Invalid format')
        return

    # get current width height
    imgHeight, imgWidth, imgChannels = get_media_dimension(input_path)
    if imgHeight > limit_size or imgWidth > limit_size:
        # get output path, output_path=None means write to temp
        if not output_path:
            fh, output_path = tempfile.mkstemp(suffix=ext)
            output_path = output_path.replace('\\', '/')
            os.close(fh)

        # video
        if ext.lower() in VIDEO_EXT:
            if imgWidth >= imgHeight:
                scale = (limit_size, -1)
            else:
                scale = (-1, limit_size)
            resized_mov = resize_mov(srcFile=input_path, dstFile=output_path, size=None, scale=scale)
            return resized_mov
        # images
        elif ext.lower() in IMG_EXT:
            if imgWidth >= imgHeight:
                scale_ratio = float(limit_size)/imgWidth
            else:
                scale_ratio = float(limit_size)/imgHeight
            dim = (int(imgWidth*scale_ratio), int(imgHeight*scale_ratio))
            resized_image = resize_img(input_path=input_path, output_path=output_path, sizes=dim)
            return resized_image
    else:
        return

def resize_img(input_path, output_path, sizes):
    from cv2 import imread, resize, imwrite, IMREAD_COLOR, INTER_AREA
    cvImg = None
    try:
        cvImg = imread(input_path, IMREAD_COLOR)
    except Exception as e:
        print(e)
        print('Error reading: {}'.format(input_path))
        return input_path

    resized_img = resize(cvImg, sizes, interpolation=INTER_AREA)
    imwrite(output_path, resized_img)
    return output_path

def resize_mov(srcFile, dstFile , size=1280, scale=None):
    scale = 'scale={size}:-1'.format(size=size) if size else 'scale=%s:%s' % (scale[0], scale[1]) if scale else ''
    subprocess.call([
            config.Software.ffmpeg,
            '-y',
            '-i',
            '{srcFile}'.format(srcFile=srcFile),
            '-filter:v',
            # 'scale={size}:-1'.format(size=size),
            str(scale),
            '-x264opts',
            'bframes=1',
            '-c:a',
            'copy',
            '{dstFile}'.format(dstFile=dstFile)
        ])
    return dstFile
    # https://superuser.com/questions/624563/how-to-resize-a-video-to-make-it-smaller-with-ffmpeg
    # ffmpeg -i input.avi -filter:v scale=720:-1 -c:a copy output.mkv

def compress_mov(srcFile, dstFile):
    '''
    D:/dev/core/rf_lib/ffmpeg_N-82947/ffmpeg.exe -i D:/__playground/test_compress/src41mb.mov 
    -c:v libx264 -crf 24 -preset veryslow -c:a copy D:/__playground/test_compress/out.mov
    '''
    subprocess.call([
            config.Software.ffmpeg,
            '-y',
            '-i',
            '{srcFile}'.format(srcFile=srcFile),
            '-c:v',
            'libx264',
            '-crf',
            '24',
            '-preset',
            'veryslow',
            '-x264opts',
            'bframes=1',
            '-c:a',
            'copy',
            '{dstFile}'.format(dstFile=dstFile)
        ])
    return dstFile

def get_duration(input_path):
    from cv2 import VideoCapture, CAP_PROP_FPS, CAP_PROP_FRAME_COUNT
    cap = VideoCapture(input_path)
    fps = cap.get(CAP_PROP_FPS)      # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
    frame_count = int(cap.get(CAP_PROP_FRAME_COUNT))
    duration = frame_count/fps
    cap.release()
    return duration

def get_video_streams(input_path):
    argList = [config.Software.ffprobe, '-i', input_path.replace('\\', '/'), 
             '-show_streams', '-select_streams', 'v', '-of', 'json', '-loglevel', 'error']
    output = None
    try:
        ps = subprocess.Popen(argList, stdout=subprocess.PIPE, 
                        stderr=subprocess.STDOUT, 
                        startupinfo=si)
        output = json.loads(ps.communicate()[0])['streams']
    except:
        pass
    return output

def is_video_codec_valid(input_path, fps, size):
    # get the video steam 
    streams = get_video_streams(input_path=input_path)
    valid = True
    if streams:
        try:
            vs = streams[0]
            if vs['codec_name']=='h264' and vs['width']==size[0] \
                and vs['height']==size[1] and eval(vs['r_frame_rate'])==fps \
                and vs['time_base']=='1/12288':
                valid = False
        except (IndexError, KeyError):
            pass
    return valid

def merge_mov(sources, output_path, fps=24, size=(1920, 1080)):
    temp_dir = tempfile.mkdtemp().replace('\\', '/')
    basename = os.path.basename(output_path)
    filename, ext = os.path.splitext(basename)

    num_srcs = len(sources)
    video_temp = '{}/{}.mov'.format(temp_dir, filename)
    audio_args = [config.Software.ffmpeg, '-i', os.path.basename(video_temp)]
    audio_filter = ''
    amix_str = ''
    converted_vids = []
    audio_timing = 0

    print('\n========== Transcoding sources...')
    # ---------- transcode each source, split video and audio
    for i, src in enumerate(sources):
        tmp_video = '{}/{}.mov'.format(temp_dir, i)
        tmp_audio = '{}/{}.wav'.format(temp_dir, i)

        # get duration
        duration = get_duration(input_path=src)

        # transcode video
        convert_args = [config.Software.ffmpeg, '-loglevel', 'warning']
        convert_args += ['-i', src.replace('\\', '/')]
        print('Transcoding: {}'.format(src))
        if is_video_codec_valid(input_path=src, fps=fps, size=size):
            print('* Warning, Invalid video attributes found')
            # video filter of fps and scale is needed if src is not correctly encoded
            convert_args += ['-vf', 'fps={fps},scale={w}:{h}'.format(fps=str(fps), w=size[0], h=size[1])]

        # encoding args
        convert_args += ['-map', '0:0', '-crf', '23','-vcodec','libx264', 
                '-tune','film','-preset', 'ultrafast', '-x264-params', 
                'bframes=1', '-pix_fmt', 'yuv420p', '-r', str(fps), tmp_video]

        # extract audio from src, if any
        if audio_utils.video_has_audio(src):
            convert_args += ['-map', '0:1', '-acodec', 'pcm_s16le', '-ac', '2', tmp_audio]
        else: # generate silent wav if src has no audio
            print('* Warning, No audio found. Generating silent: {}'.format(tmp_audio))
            tmp_audio = audio_utils.generate_silent_wav(total_time=duration, path=tmp_audio)

        # ** run transcode src 
        try:
            convert_returncode = subprocess.call(' '.join(convert_args))
            converted_vids.append(tmp_video)
        except:
            pass
        if convert_returncode != 0:
            print('** Error, transcoding source: {}'.format(src))
            continue 

        # pre construct audio placement args
        audio_args += ['-ac', '2', '-channel_layout', 'stereo', '-i', os.path.basename(tmp_audio)]
        audio_name = '[{i}a]'.format(i=str(i+1))
        amix_str += audio_name
        audio_filter += '[{i}]adelay={d}:all=1{a};'.format(i=str(i+1), d=audio_timing*1000, a=audio_name)
        audio_timing += duration

    # ---------- concatenate videos
    print('\n========== Rendering full video...')

    # make video file list
    fh, file_list_path = tempfile.mkstemp(suffix='.txt', dir=temp_dir)
    file_list_path = file_list_path.replace('\\', '/')
    os.close(fh)
    with open(file_list_path, 'w') as f:
        for i, vid in enumerate(converted_vids):
            f.write("file '{}'\n".format(vid))

    concat_argList = [config.Software.ffmpeg, '-safe', '0', '-f', 'concat', '-y', '-r', str(fps), 
            '-i', file_list_path, '-crf', '23','-vcodec','libx264', '-tune','film','-preset', 'ultrafast', 
            '-x264-params', 'bframes=1', '-pix_fmt', 'yuv420p', '-r', str(fps), video_temp]

    # ** run concat videos
    concat_returncode = subprocess.call(' '.join(concat_argList))
    if concat_returncode != 0 or not os.path.exists(video_temp):
        print('** Error, concatenating videos')
        return 

    # ---------- place audios
    print('\n========== Mixing audios...')

    # make filter complex script file
    audio_filter += '{}amix=inputs={},loudnorm[a]'.format(amix_str, str(num_srcs))
    ffh, filter_path = tempfile.mkstemp(suffix='.txt', dir=temp_dir)
    with open(filter_path, 'w') as fw:
        fw.write(audio_filter)
    os.close(ffh)

    audio_args += ['-filter_complex_script ', os.path.basename(filter_path.replace('\\', '/'))] 
    audio_args += ['-map', '0:v', '-map', '"[a]"']
    audio_args += ['-c:v', 'copy', '-c:a', 'aac', output_path]

    # ** run mix audio
    returncode = subprocess.call(' '.join(audio_args), cwd=temp_dir)

    # remove temp
    shutil.rmtree(temp_dir)

    if returncode == 0 and os.path.exists(output_path):
        return output_path

def mov_frame(srcFile, dst_path, frame=1):
    #ffmpeg -i input.mov -vframes 1 -vf scale=-1:560 testimg.jpg
    subprocess.call([
            config.Software.ffmpeg,
            '-i',
            '{srcFile}'.format(srcFile=srcFile),
            '-vf',
            'select=eq(n\\,{frame})'.format(frame=int(frame)),
            '-vframes',
            '1',
            '{dst_path}'.format(dst_path=dst_path)
        ])

    #ffmpeg.exe -i "E:/te.mp4" -vf "select=eq(n\,10)" -vframes 1 "E:/te2.jpg
    return dst_path

def read_one_frame(input_path, convert_to_rgb=False):
    from cv2 import VideoCapture, cvtColor, imread, COLOR_BGR2RGB, IMREAD_UNCHANGED
    fn, ext = os.path.splitext(input_path)
    img = None
    # get input sizes
    if ext.lower() in VIDEO_EXT:
        capture = VideoCapture(input_path)
        read_flag, img = capture.read()
        capture.release()
        if convert_to_rgb:
            img = cvtColor(img, COLOR_BGR2RGB)
    else:
        img = imread(input_path, IMREAD_UNCHANGED)

    return img

def read_media_frames(input_path, convert_to_rgb=False):
    from cv2 import VideoCapture, cvtColor, imread, COLOR_BGR2RGB, IMREAD_UNCHANGED
    fn, ext = os.path.splitext(input_path)
    # get input sizes
    if ext.lower() in VIDEO_EXT:
        capture = VideoCapture(input_path)
        read_flag, frame = capture.read()
        vid_frames = []
        while (read_flag):
            if convert_to_rgb:
                frame = cvtColor(frame, COLOR_BGR2RGB)
            vid_frames.append(frame)
            read_flag, frame = capture.read()
        capture.release()
        return vid_frames
    else:
        img = imread(input_path, IMREAD_UNCHANGED)
        if convert_to_rgb:
            img = cvtColor(img, COLOR_BGR2RGB)
        return [img]

def get_media_dimension(input_path):
    img = read_one_frame(input_path, convert_to_rgb=False)
    imgHeight, imgWidth, imgChannels = img.shape
    return imgHeight, imgWidth, imgChannels

def get_average_image_value(input_path):
    ''' return 0-1 value representing average brightness of any image/vid(first frame) '''
    from cv2 import cvtColor, COLOR_BGR2GRAY
    img = read_one_frame(input_path, convert_to_rgb=False)
    grey_img = cvtColor(img, COLOR_BGR2GRAY)
    average = grey_img.mean(axis=0).mean(axis=0)
    return average/255.0

def get_video_frame_count(path):
    from cv2 import VideoCapture, CAP_PROP_FRAME_COUNT
    v = VideoCapture(path)
    frame_count = int(v.get(CAP_PROP_FRAME_COUNT))
    return frame_count

def overlay_image(input_path, overlay_image, output_path, 
                opacity=1.0, blendmode='normal', video_quality=20, 
                video_preset='fast', callback_func=None):
    ''' Overlay an image on top of a image/video file '''

    input_fn, input_ext = os.path.splitext(input_path)
    input_ext = input_ext.lower()  # needs to make sure extension is lower case

    # get the input sizes
    imgHeight, imgWidth, imgChannels = get_media_dimension(input_path)
    div2Width = math.ceil(imgWidth/2) * 2
    div2Height = math.ceil(imgHeight/2) * 2

    cmd_list = [config.Software.ffmpeg, 
                    '-y', 
                    '-i', 
                    '{input_path}'.format(input_path=input_path),
                    '-i',
                    '{overlay_image}'.format(overlay_image=overlay_image),
                    '-filter_complex']

    # filter do
    # 1. scale the media making sure it's devisible by 2
    # 2. scale square watermark to either width or height of the media
    # 3. adjust watermark canvas to fill the media by filling it with black, transparent color
    filter_complex_str = "[0:v]scale={div2Width}:{div2Height}[main_div2];\
                    [1:v][main_div2]scale2ref=w='min(iw, ih)':h='min(iw, ih)'[watermark][main], \
                    [watermark]pad={div2Width}:{div2Height}:(ow-iw)/2:(oh-ih)/2:color=black@0.0\
                    [watermark_pad];".format(div2Width=div2Width, div2Height=div2Height)
    
    # in normal mode will adjust opacity in the watermark and overlay it on top of media        
    if blendmode == 'normal':
        filter_complex_str += "[watermark_pad]format=argb, colorchannelmixer=aa={opacity}[watermark_trans];\
                            [main][watermark_trans]overlay=0:0".format(opacity=opacity)
    else:  # in other mode will convert media to color gbrp(slow) then blend watermark on top using selected blend mode
        filter_complex_str += "[main]format=gbrp[main_converted];\
                            [main_converted][watermark_pad]blend=all_mode='{blendmode}':all_opacity={opacity}".format(blendmode=blendmode, opacity=opacity)
    cmd_list.append(filter_complex_str)

    # if it's a mov video
    if input_ext in VIDEO_EXT:
        frame_count = get_video_frame_count(path=input_path)
        cmd_list.extend(['-vcodec',
                    'libx264',
                    '-threads',
                    '8',
                    '-crf',
                    str(video_quality),
                    '-tune',
                    'film',
                    '-preset',
                    str(video_preset),
                    '-x264-params',
                    'bframes=1',
                    '-pix_fmt',
                    'yuv420p',
                    '-c:a',
                    'copy'])
    elif input_ext in IMG_EXT:
        frame_count = 1
        cmd_list.extend(['-q:v', '1'])
    else:
        print('Unsupported format for adding watermark: {}'.format(input_ext))
        return False
        
    # add output
    cmd_list.append('{output_path}'.format(output_path=output_path))

    if not callback_func:
        # run ffmpeg via subporcess
        ps = subprocess.Popen(cmd_list,
                    stdout=subprocess.PIPE, 
                    stderr=subprocess.STDOUT, 
                    startupinfo=si)
        output = ps.communicate()[0]
    else:
        import pexpect
        from pexpect import popen_spawn
        thread = popen_spawn.PopenSpawn(cmd_list)
        patterns = [pexpect.EOF,
                "frame= *(\d+)"]
        cpl = thread.compile_pattern_list(patterns)
        while True:
            i = thread.expect_list(cpl, timeout=None)
            if i == 0: # EOF
                break
            elif i == 1:  # working
                frame_number = thread.match.group(1)
                callback_func((frame_number, frame_count))

    return output_path

def convert_media_to_frames(paths, output_dir, output_filename, output_ext='.png'):
    from cv2 import VideoCapture, imread, IMREAD_UNCHANGED, imwrite
    all_frames = []
    for path in paths:
        fn, ext = os.path.splitext(path)
        if ext.lower() in VIDEO_EXT:
            capture = VideoCapture(path)
            read_flag, frame = capture.read()
            vid_frames = []
            while read_flag:
                vid_frames.append(frame)
                read_flag, frame = capture.read()
            all_frames.extend(vid_frames)
            capture.release()
        else:
            img = imread(path, IMREAD_UNCHANGED)
            all_frames.append(img)

    output_paths = []
    for i, frame in enumerate(all_frames):
        output_path = '{}/{}.{}{}'.format(output_dir, output_filename, str(i+1).zfill(4), output_ext)
        imwrite(output_path, frame)
        output_paths.append(output_path)

    return output_paths