# -------- TODO --------
# //- include set
# //- selection scriptJob
# //- sort yml
# //- popup when complete
# - analyze does not destory keys
# ----------------------

# titles 
uiName = 'CacheListUi'
_title = 'Set Cache List'
_version = 'v.1.0.0'
_des = 'main'

#Import python modules
import os 
import sys
from collections import OrderedDict
from rf_utils import file_utils
from rf_utils.context import context_info
from rf_utils.sg import sg_utils
from rf_utils.widget import shot_list_widget
reload(shot_list_widget)
from rf_app.asm import asm_lib
# reload(asm_lib)
# reload(shot_list_widget)
from functools import partial
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import shot_data 
from rf_app.publish.scene.export import cache
import maya.cmds as mc 
from functools import partial

# module vars
ICON_DIR = '{0}/core/icons'.format(os.environ['RFSCRIPT'])
ENABLE_TXT = 'Enabled'
ENABLE_STATUS_COLOR = [100, 250, 100]
ENABLE_TITLE_COLOR = [200, 200, 200]
OMIT_TXT = 'Omitted'
OMIT_STATUS_COLOR = [250, 100, 100]
OMIT_TITLE_COLOR = [100, 100, 100]

ENABLE_LOCKED_TXT = 'Locked'
ENABLE_LOCKED_STATUS_COLOR = [100, 250, 100]
ENABLE_LOCKED_TITLE_COLOR = [100, 100, 100]
OMIT_LOCKED_TXT = 'Locked'
OMIT_LOCKED_STATUS_COLOR = [250, 100, 100]
OMIT_LOCKED_TITLE_COLOR = [100, 100, 100]

class Config: 
    heroStep = 'heroStep'
    modified = 'modified'
    lock = 'lock'
    status = 'status'
    defaultStep = '*'
    envKey = 'ASSET_IN_VIEW'

class CacheList(object):
    """docstring for CacheList"""

    def __init__(self, entity=None):
        super(CacheList, self).__init__()
        self.entity = entity if entity else context_info.ContextPathInfo()
        self.entitys = self.entity if isinstance(self.entity, list) else [self.entity]
        self.entity = self.entity[-1] if isinstance(self.entity, list) else self.entity

        self.keyData = shot_data.Config.buildData
        self.currentData = shot_data.read_data(self.entity, self.keyData)
        self.newData = OrderedDict(self.currentData)

    def clear(self): 
        self.newData = OrderedDict()

    def add_asset_key(self, assetKey, status=None): 
        # if not assetKey in self.newData.keys(): 
        self.update_asset_data(assetKey, status)
        self.write()
        
    def remove_asset_key(self, assetKey): 
        if assetKey in self.newData.keys(): 
            self.newData.pop(assetKey)
            self.write()

    def update_asset_data(self, assetKey, status):
        for entity in self.entitys:
            data = OrderedDict()
            self.newData[assetKey] = {}
            data[Config.heroStep] = Config.defaultStep
            data[Config.modified] = self.entity.step

            if Config.lock in self.newData[assetKey].keys():
                status_old = self.newData[assetKey][Config.status]
                data[Config.lock] = self.newData[assetKey][Config.lock]
                data[Config.status] = status_old if data[Config.lock] else status
            else:
                data[Config.status] = status
                data[Config.lock] = False

            self.newData[assetKey] = data

    def set_hero_step(self, assetKey, step): 
        if assetKey in self.newData.keys(): 
            data = self.newData[assetKey]
            data[Config.heroStep] = step
            data[Config.modified] = self.entity.step
            self.write()

    def register(self):
        for entity in self.entitys:
            shot_data.register(entity)

    def write(self):
        for entity in self.entitys:
            shot_data.write_data(entity, self.keyData, self.newData)

    def list_keys(self): 
        return self.newData.keys()

    def list_hero_keys(self): 
        keys = []
        for key, data in self.newData.iteritems(): 
            if not data[Config.heroStep] == Config.defaultStep: 
                keys.append((key, data[Config.heroStep]))
        return keys

    def status(self): 
        return shot_data.register_status(self.entity)

    def anyEnabled(self):
        statuses = [self.newData[k][Config.status] for k in self.newData]
        if any(statuses):
            return True
        else:
            return False

    def list_keys_status(self, status): 
        return [k for k in self.newData if self.newData[k][Config.status] == status]

    def optimize_data(self, existing_keys):
        optimizedData = self.newData.copy()
        for assetKey, data in self.newData.iteritems():
            curr_step = data[Config.modified]
            locked = data[Config.lock] if Config.lock in self.newData[assetKey].keys() else False
            if curr_step == self.entity.step and assetKey not in existing_keys:
                if not locked:
                    optimizedData[assetKey][Config.status] = False
        self.newData = optimizedData

    def sort_data(self):
        enabled_assets, omitted_assets = [], []
        # separate the keys by status True, false
        for assetKey, data in self.newData.iteritems():
            curr_status = self.newData[assetKey][Config.status]
            if curr_status:
                enabled_assets.append(assetKey)
            else:
                omitted_assets.append(assetKey)

        # sort by modified and name
        sorted_enabled = sorted(enabled_assets, key=lambda k: (self.newData[k][Config.modified], k))
        sorted_omitted = sorted(omitted_assets, key=lambda k: (self.newData[k][Config.modified], k))

        # put the keys back together to form a new OrderedDict
        sorted_keys = sorted_enabled + sorted_omitted
        sorted_data = OrderedDict([(k, self.newData[k]) for k in sorted_keys])

        self.newData = sorted_data

class Ui(QtWidgets.QWidget):
    def __init__(self, parent = None) :
        super(Ui, self).__init__(parent)
        self.allLayout = QtWidgets.QVBoxLayout()

        # title
        self.titleLayout = QtWidgets.QHBoxLayout()
        self.dataLayout = QtWidgets.QHBoxLayout()
        self.currentShotLabel = QtWidgets.QLabel('Current Entity:')
        self.currentShotLabel.setFont(QtGui.QFont("Arial", 9))

        self.title = QtWidgets.QLabel()
        self.title.setFont(QtGui.QFont("Arial", 11, QtGui.QFont.Bold))

        self.titleSpacer = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.titleLayout.addWidget(self.currentShotLabel)
        self.titleLayout.addWidget(self.title)
        self.titleLayout.addItem(self.titleSpacer)
        self.titleLayout.setStretch(1, 1)
        self.allLayout.addLayout(self.titleLayout)

        # separator 
        self.titleSep = QtWidgets.QFrame()
        self.titleSep.setFrameShape(QtWidgets.QFrame.HLine)
        self.titleSep.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.allLayout.addWidget(self.titleSep)

        self.displayLayout = QtWidgets.QHBoxLayout()
        # -------------------

        # shot list widget
        self.shotLayout = QtWidgets.QVBoxLayout()
        self.shotTitle = QtWidgets.QLabel('Cameras')
        self.shotTitle.setFont(QtGui.QFont("Arial", 9))
        self.shotTitle.setAlignment(QtCore.Qt.AlignCenter)
        self.shotLayout.addWidget(self.shotTitle)
        self.shotListWidget = shot_list_widget.ShotListWidget()
        self.shotListWidget.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.shotLayout.addWidget(self.shotListWidget)

        self.shotDataButton = QtWidgets.QPushButton()
        self.shotDataButton.setText('data')
        self.shotDataButton.setEnabled(False)
        self.shotLayout.addWidget(self.shotDataButton)
 
        # set list widget
        self.setsLayout = QtWidgets.QVBoxLayout()
        self.setLabel = QtWidgets.QLabel('Sets')
        self.setLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.setLabel.setFont(QtGui.QFont("Arial", 9))
        self.setListWidget = QtWidgets.QListWidget()
        self.setListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.setsLayout.addWidget(self.setLabel)
        self.setsLayout.addWidget(self.setListWidget)
        self.shotLayout.addLayout(self.setsLayout)

        # -------------------------
        # asset layout
        self.assetLayout = QtWidgets.QVBoxLayout()
        
        # enabled title
        self.assetTitleLayout = QtWidgets.QHBoxLayout()
        self.assetListLayout = QtWidgets.QHBoxLayout()
        self.assetListEnabledLayout = QtWidgets.QVBoxLayout()
        self.enableLabel = QtWidgets.QLabel('Enabled Assets')
        self.enableLabel.setFont(QtGui.QFont("Arial", 9))
        self.enableLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.assetListEnabledLayout.addWidget(self.enableLabel)

        # enabled list widget
        self.assetListWidget_enabled = QtWidgets.QListWidget()
        self.assetListWidget_enabled.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.assetListEnabledLayout.addWidget(self.assetListWidget_enabled)
        self.assetListLayout.addLayout(self.assetListEnabledLayout)

        # button layout for add/remove
        self.buttonLayout = QtWidgets.QVBoxLayout()
        self.assetListLayout.addLayout(self.buttonLayout)

        # omitted title
        self.assetListOmittedLayout = QtWidgets.QVBoxLayout()
        self.omitLabel = QtWidgets.QLabel('Omitted Assets')
        self.omitLabel.setFont(QtGui.QFont("Arial", 9))
        self.omitLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.assetListOmittedLayout.addWidget(self.omitLabel)

        # omitted list widget
        self.assetListWidget_omitted = QtWidgets.QListWidget()
        self.assetListWidget_omitted.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.assetListOmittedLayout.addWidget(self.assetListWidget_omitted)
        self.assetListLayout.addLayout(self.assetListOmittedLayout)

        self.assetLayout.addLayout(self.assetTitleLayout)
        self.assetLayout.addLayout(self.assetListLayout)
        self.displayLayout.addLayout(self.shotLayout)
        self.displayLayout.addLayout(self.assetLayout)
        self.allLayout.addLayout(self.displayLayout)
        # -----------------

        # analyze button
        self.analyzeButton = QtWidgets.QPushButton('Get what\'s\nseen in camera')
        self.analyzeButton.setMinimumSize(QtCore.QSize(0, 40))
        self.shotLayout.addWidget(self.analyzeButton)

        # add button
        self.addButton = QtWidgets.QPushButton()
        leftArrowIcon = QtGui.QIcon()
        leftArrowIcon.addPixmap(QtGui.QPixmap('{0}/arrow_left_icon1.png'.format(ICON_DIR)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.addButton.setIcon(leftArrowIcon)
        self.addButton.setMinimumSize(QtCore.QSize(0, 30))
        self.buttonLayout.addWidget(self.addButton)

        # remove button
        self.removeButton = QtWidgets.QPushButton()
        rightArrowIcon = QtGui.QIcon()
        rightArrowIcon.addPixmap(QtGui.QPixmap('{0}/arrow_right_icon1.png'.format(ICON_DIR)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.removeButton.setIcon(rightArrowIcon)
        self.removeButton.setMinimumSize(QtCore.QSize(0, 30))
        self.buttonLayout.addWidget(self.removeButton)

        # confirm button
        self.confirmButton = QtWidgets.QPushButton('Confirm')
        self.confirmButton.setFont(QtGui.QFont("Arial", 9))
        self.confirmButton.setMinimumSize(QtCore.QSize(0, 40))
        self.assetLayout.addWidget(self.confirmButton)

        # set stretch and spacing
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(6, 6, 6, 6)
        self.displayLayout.setStretch(0, 1)
        self.displayLayout.setStretch(1, 3)
        # self.assetLayout.setStretch(0, 1)
        # self.shotLayout.setStretch(0, 1)
        self.shotLayout.setStretch(1, 2)
        self.shotLayout.setStretch(2, 1)
        self.shotLayout.setStretch(3, 1)
        self.setLayout(self.allLayout)

class Main(QtWidgets.QMainWindow):
    """docstring for Main"""
    submitted = QtCore.Signal(object)

    def __init__(self, entity=None, parent=None):
        super(Main, self).__init__(parent)

        self.cache = None
        self.cacheList = []
        self.setList = []
        self._select_from_UI = False
   
        self.ui = Ui(parent)
        self.entity = entity
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))
        self.resize(450, 400)

        self.init_functions()
        self.init_signals()
        self.init_scriptJobs()

    def init_scriptJobs(self):
        self.selChangedJobId = mc.scriptJob(e=('SelectionChanged', self.updateAssetSelection))

    def init_functions(self): 
        if self.entity:
            # set shot name according to the context given 
            self.set_context()
            # this should disable confirm button since no shot is selected and not self.cache
            self.check_register_status()

    def closeEvent(self, event):
        logger.info('Closing %s' %_title)
        for jid in [self.selChangedJobId]:
            if jid and mc.scriptJob(ex=jid):
                mc.scriptJob(kill=jid, force=True)

    def init_signals(self): 
        # listWidget 
        self.ui.shotListWidget.itemSelectionChanged.connect(self.shot_selected)
        self.ui.shotListWidget.itemSelectionChanged.connect(self.shot_data_selected)
        self.ui.assetListWidget_enabled.itemSelectionChanged.connect(self.asset_selected)
        self.ui.assetListWidget_omitted.itemSelectionChanged.connect(self.asset_selected)
        self.ui.setListWidget.itemSelectionChanged.connect(self.set_selected)
        self.ui.setListWidget.itemClicked.connect(self.set_checked)

        # button 
        self.ui.shotDataButton.clicked.connect(self.shot_data_open) 
        self.ui.analyzeButton.clicked.connect(self.analyze)
        self.ui.confirmButton.clicked.connect(self.confirm)
        self.ui.addButton.clicked.connect(self.add)
        self.ui.removeButton.clicked.connect(self.remove)

    def shot_data_selected(self, entity):
        self.modifyEntity = entity
        self.ui.shotDataButton.setEnabled(True)

    def shot_data_open(self):
        from rftool.utils.ui import maya_win
        myApp = ModifyData(self.modifyEntity, maya_win.getMayaWindow())
        myApp.show()

    def updateAssetSelection(self):
        # if no shot was selected just return
        if not self.ui.shotListWidget.listWidget.selectedItems() or self._select_from_UI:
            return

        selection = mc.ls(sl=True, type='transform')
        if not selection:
            self.ui.assetListWidget_enabled.blockSignals(True)
            self.ui.assetListWidget_omitted.blockSignals(True)
            self.ui.assetListWidget_enabled.clearSelection()
            self.ui.assetListWidget_omitted.clearSelection()
            self.ui.assetListWidget_enabled.blockSignals(False)
            self.ui.assetListWidget_omitted.blockSignals(False)
            return

        namespaces = []
        for sel in selection:
            sn = sel.split('|')[-1]
            ns = ':'.join(sn.split(':')[:-1])
            if ns in namespaces:
                continue
            else:
                namespaces.append(ns)

        if namespaces:
            self.ui.assetListWidget_enabled.blockSignals(True)
            self.ui.assetListWidget_omitted.blockSignals(True)
            # select UI
            enabled_items = [self.ui.assetListWidget_enabled.item(i) for i in xrange(self.ui.assetListWidget_enabled.count())]
            omitted_items = [self.ui.assetListWidget_omitted.item(i) for i in xrange(self.ui.assetListWidget_omitted.count())]
            enabled_widgets = [self.ui.assetListWidget_enabled.itemWidget(a) for a in enabled_items]
            omitted_widgets = [self.ui.assetListWidget_omitted.itemWidget(a) for a in omitted_items]
            allItems = enabled_items + omitted_items
            allWidgets = enabled_widgets + omitted_widgets
            for item, widget in zip(allItems, allWidgets):
                asset = widget.text1.text()
                status = False
                if asset in namespaces:
                    status = True
                item.setSelected(status)
            self.ui.assetListWidget_enabled.blockSignals(False)
            self.ui.assetListWidget_omitted.blockSignals(False)


    def set_context(self): 
        self.ui.title.setText(self.entity.name)
        self.list_shot()

    def list_shot(self): 
        self.ui.shotListWidget.clear()
        if not self.entity.name_code == 'all': 
            self.ui.shotListWidget.add_item(self.entity.name_code, self.entity.copy(), setIcon=True)
        else: 
            shots = mc.ls(type='shot')
            for shot in shots: 
                entity = self.entity.copy()
                entity.context.update(entityCode=shot, entity='none')
                entity.update_scene_context()
                self.ui.shotListWidget.add_item(shot, entity, setIcon=True)

    def shot_selected(self, entity):
        self.cache = CacheList(entity)
        self.populate_assets(entity)
        self.populate_sets(entity)
        self.check_register_status()

    def set_checked(self):
        status_map = {QtCore.Qt.Checked:True, QtCore.Qt.Unchecked:False}
        for i in xrange(self.ui.setListWidget.count()):
            item = self.ui.setListWidget.item(i)
            item_status = status_map[item.checkState()]
            setName = item.text()
            self.cache.update_asset_data(setName, status=item_status)

    def populate_sets(self, entity):
        entity = entity[-1] if isinstance(entity, list) else entity
        self.setList = asm_lib.list_set(entity)

        shotdressGrp = entity.projectInfo.asset.shotdress_grp() or 'ShotDress_Grp'
        if mc.objExists(shotdressGrp):
            self.setList.append('shotdress')

        self.ui.setListWidget.clear()
        cacheData = self.cache.newData
        status_map = {True: QtCore.Qt.Checked, False: QtCore.Qt.Unchecked}
        for s in self.setList:
            status = True
            if s in cacheData and cacheData[s][Config.status] == False:
                status = False

            item = QtWidgets.QListWidgetItem(s)
            item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
            item.setCheckState(status_map[status])
            item.setData(QtCore.Qt.UserRole, s)

            self.ui.setListWidget.addItem(item)
            self.cache.update_asset_data(s, status=status)

    def populate_assets(self, entity):
        entity = entity[-1] if isinstance(entity, list) else entity
        geoCacheGrp = entity.projectInfo.asset.geo_grp() or 'Geo_Grp'
        self.cacheList = cache.get_asset_namespace(geoCacheGrp)
        registerAssets = self.cache.list_keys()
        cacheData = self.cache.newData

        self.ui.assetListWidget_enabled.clear()
        self.ui.assetListWidget_omitted.clear()
        for asset in self.cacheList: 
            customWidget = AssetItem()
            item = QtWidgets.QListWidgetItem()
                        
            # add data 
            customWidget.set_text1(asset)
            item.setData(QtCore.Qt.UserRole, '%s:%s' % (asset, geoCacheGrp))
            item.setSizeHint(customWidget.sizeHint())

            asset = str(customWidget.text1.text())

            if asset in registerAssets and cacheData[asset][Config.status] == True: 
                statusText = ENABLE_TXT
                statusColors = ENABLE_STATUS_COLOR
                titleColors = ENABLE_TITLE_COLOR

                if asset in cacheData.keys():
                    if Config.lock in cacheData[asset].keys():
                        statusText = ENABLE_TXT if not cacheData[asset][Config.lock] else ENABLE_LOCKED_TXT
                        statusColors = ENABLE_STATUS_COLOR if not cacheData[asset][Config.lock] else ENABLE_LOCKED_STATUS_COLOR
                        titleColors = ENABLE_TITLE_COLOR if not cacheData[asset][Config.lock] else ENABLE_LOCKED_TITLE_COLOR

                self.ui.assetListWidget_enabled.addItem(item)
                self.ui.assetListWidget_enabled.setItemWidget(item, customWidget)
                self.cache.update_asset_data(asset, status=True)
            else:
                statusText = OMIT_TXT
                statusColors = OMIT_STATUS_COLOR
                titleColors = OMIT_TITLE_COLOR

                if asset in cacheData.keys():
                    if Config.lock in cacheData[asset].keys():
                        statusText = OMIT_TXT if not cacheData[asset][Config.lock] else OMIT_LOCKED_TXT
                        statusColors = OMIT_STATUS_COLOR if not cacheData[asset][Config.lock] else OMIT_LOCKED_STATUS_COLOR
                        titleColors = OMIT_TITLE_COLOR if not cacheData[asset][Config.lock] else OMIT_LOCKED_TITLE_COLOR

                self.ui.assetListWidget_omitted.addItem(item)
                self.ui.assetListWidget_omitted.setItemWidget(item, customWidget)
                self.cache.update_asset_data(asset, status=False)

            customWidget.set_text1_color(titleColors)
            customWidget.set_text2(statusText)
            customWidget.set_text2_color(statusColors)

    def asset_selected(self): 
        enabled_items = self.ui.assetListWidget_enabled.selectedItems()
        omitted_items = self.ui.assetListWidget_omitted.selectedItems()
        items = enabled_items + omitted_items
        objs = []

        for item in items: 
            obj = item.data(QtCore.Qt.UserRole)
            objs.append(obj)

        self._select_from_UI = True
        try:
            mc.select(objs)
        except:
            pass
        self._select_from_UI = False

    def set_selected(self):
        items = self.ui.setListWidget.selectedItems()
        objs = []
        for item in items: 
            obj = item.data(QtCore.Qt.UserRole)
            objs.append(obj)

        self._select_from_UI = True
        try:
            mc.select(objs)
        except:
            pass
        self._select_from_UI = False

    def check_register_status(self): 
        if self.cache and self.cache.anyEnabled():
            enable = True
        else:
            enable = False
        self.ui.confirmButton.setEnabled(enable) 

    def set_env_qc(self):
        os.environ[Config.envKey] = '1'
        self.submitted.emit(True)

    def analyze(self): 
        items = self.ui.shotListWidget.listWidget.selectedItems()
        for item in items: 
            entity = item.data(QtCore.Qt.UserRole)
            in_view_namespaces = analyze_shot(entity)
            # self.enable_assets_by_namespace(namespaces=in_view_namespaces)
            self.cache = register_assets(entity, in_view_namespaces)

            self.populate_assets(entity)
            self.populate_sets(entity)
            self.check_register_status()

    def confirm_dialog(self, title, message):
        msgBox = QtWidgets.QMessageBox(self)
        msgBox.setIcon( QtWidgets.QMessageBox.Information )
        msgBox.setText(message)
        msgBox.setWindowTitle(title)

        # msgBox.setInformativeText(information)
        msgBox.addButton( QtWidgets.QMessageBox.Ok )
        result = msgBox.exec_()

        if result == QtWidgets.QMessageBox.Yes:
            return True
        return False

    def confirm(self):
        # go thru each key and set status to false if it's modified by the same department
        # and it does not exist within the current scene
        self.cache.optimize_data(existing_keys=self.cacheList+self.setList)

        # sort the data
        self.cache.sort_data()

        # write to temp yml
        self.cache.write()
        # copy temp content to real shot build data yml
        self.cache.register()
        self.set_env_qc()
        # self.check_register_status()

        # popup
        message = '{0}: Cache list successfully set.\nFor more information, click more info'.format(self.entity.name)
        detailedTxt = '\n'.join(self.cacheList+self.setList)
        msgBox = QtWidgets.QMessageBox(self)
        msgBox.setIcon(QtWidgets.QMessageBox.Information)
        msgBox.setText(message)
        msgBox.setWindowTitle('%s: Success' %_title)
        msgBox.addButton( QtWidgets.QMessageBox.Ok )
        msgBox.setDetailedText(detailedTxt)

        msgBox.exec_()

    def add(self): 
        entity = self.ui.shotListWidget.listWidget.currentItem().data(QtCore.Qt.UserRole)
        items = self.ui.assetListWidget_omitted.selectedItems()
        widgets = [self.ui.assetListWidget_omitted.itemWidget(a) for a in items]

        if not widgets:
            return

        for widget in widgets: 
            asset = str(widget.text1.text())
            # self.cache.add_asset_key(asset, status=True)
            self.cache.update_asset_data(asset, status=True)

        self.populate_assets(entity)
        self.check_register_status()

    def remove(self): 
        entity = self.ui.shotListWidget.listWidget.currentItem().data(QtCore.Qt.UserRole)
        items = self.ui.assetListWidget_enabled.selectedItems()
        widgets = [self.ui.assetListWidget_enabled.itemWidget(a) for a in items]

        if not widgets:
            return

        for widget in widgets: 
            asset = str(widget.text1.text())
            # self.cache.add_asset_key(asset, status=False)
            self.cache.update_asset_data(asset, status=False)

        self.populate_assets(entity)
        self.check_register_status()

class UiData(QtWidgets.QWidget): 
    def __init__(self, parent=None): 
        super(UiData, self).__init__(parent)
        self.mainLayout = QtWidgets.QVBoxLayout()
        self.projectLayout = QtWidgets.QVBoxLayout()

        self.dataListWidget = QtWidgets.QListWidget()
        self.dataListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.confirm_button = QtWidgets.QPushButton()
        self.confirm_button.setText('Confirm')
        self.confirm_button.setMinimumHeight(35)

        self.mainLayout.addLayout(self.projectLayout)
        self.mainLayout.addWidget(self.dataListWidget)
        self.mainLayout.addWidget(self.confirm_button)
        self.setLayout(self.mainLayout)


class ModifyData(QtWidgets.QMainWindow):
    """docstring for ShotItemBasic"""
    def __init__(self, entity=None, parent=None):
        super(ModifyData, self).__init__(parent=parent)

        uiName = entity.name if entity else None
        self.app_status = entity.name if entity else None

        self.ui = UiData()
        self._init_signal()
        self.init_scriptJobs()

        if uiName:
            self.add_item(entity)
        else:
            uiName = 'Filter Data'
            self.add_project_widget()

        self.setMinimumSize(350, 500)
        self.setCentralWidget(self.ui)
        self.setWindowTitle('%s' % (uiName))

    def _init_signal(self):
        self.ui.confirm_button.clicked.connect(self.confirm_modify_data)

    def init_scriptJobs(self):
        self.selChangedJobId = mc.scriptJob(e=('SelectionChanged', self.updateAssetSelection))

    def updateAssetSelection(self):
        selection = mc.ls(sl=True, type='transform')
        namespaces = []
        itemList = []

        for sel in selection:
            sn = sel.split('|')[-1]
            ns = ':'.join(sn.split(':')[:-1])
            if ns in namespaces:
                continue
            else:
                namespaces.append(ns)

        for a in range(self.ui.dataListWidget.count()):
            item = self.ui.dataListWidget.item(a)
            name, statusCombo, status, lockButton, locked = item.data(QtCore.Qt.UserRole)
            if name in namespaces:
                item.setSelected(True)
            else:
                item.setSelected(False)

    def closeEvent(self, event):
        for jid in [self.selChangedJobId]:
            if jid and mc.scriptJob(ex=jid):
                mc.scriptJob(kill=jid, force=True)

    def clear_item(self):
        self.ui.dataListWidget.clear()

    def project_changed(self):
        projectEntity = self.projectWidget.project_entity()
        self.sgWidget.list_episodes(projectEntity)

    def add_project_widget(self):
        from rf_utils.widget import project_widget
        from rf_utils.widget import entity_browse_widget 
        from rf_utils.pipeline import user_pref

        self.userPref = user_pref.ToolSetting(uiName)
        self.userData = self.userPref.read()
        project = self.userData.get('project')

        self.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, defaultProject=project)
        self.sgWidget = entity_browse_widget.SGSceneNavigation2(sg=sg_utils.sg, shotFilter=True)
        self.sgWidget.shotExtraFilter = ['sg_shot_type', 'is', 'Shot']
        self.ui.projectLayout.addWidget(self.projectWidget)
        self.ui.projectLayout.addWidget(self.sgWidget)

        self.submit_button = QtWidgets.QPushButton('Add')
        self.submit_button.setStyleSheet("background-color: rgb(19, 180, 108); color: black")
        self.submit_button.setMinimumHeight(30)
        self.ui.projectLayout.addWidget(self.submit_button)

        self.submit_button.clicked.connect(self.get_shot_entity)
        self.projectWidget.projectComboBox.currentIndexChanged.connect(self.project_changed)
        self.sgWidget.shotWidget.currentIndexChanged.connect(self.clear_item)

        projectEntity = self.projectWidget.project_entity()
        self.sgWidget.list_episodes(projectEntity)

    def get_shot_entity(self):
        shotEntity = self.sgWidget.shotWidget.current_item()
        project = shotEntity['project']['name']
        episode = shotEntity['sg_episode']['name']
        sequence = shotEntity['sg_sequence_code']
        shortcode = shotEntity['sg_shortcode']

        context = context_info.Context()
        context.update(project=project, entityType='scene', entityGrp=episode, entityParent=sequence, entityCode=shortcode)
        entity = context_info.ContextPathInfo(context=context)
        entity.set_project_code(sg_utils.sg)

        self.add_item(entity)

    def add_item(self, entity):
        from rf_utils import register_shot
        entity = entity[-1] if isinstance(entity, list) else entity
        self.cache = CacheList(entity)
        cacheData = self.cache.newData
        reg = register_shot.Register(entity)

        self.ui.dataListWidget.clear()
        for asset in self.cache.list_keys():
            status = cacheData[asset][Config.status]
            locked = cacheData[asset][Config.lock] if Config.lock in cacheData[asset].keys() else False
            step = reg.get.assetData[asset]['step'] if asset in reg.get.assetData.keys() else "None"
            self.create_item(asset, step, status, locked)

    def create_item(self, assetName, step, status, locked):
        widget = QtWidgets.QWidget()
        layout = QtWidgets.QHBoxLayout(widget)

        assetLabel = QtWidgets.QLabel(assetName)
        assetLabel.setAlignment(QtCore.Qt.AlignCenter)
        assetLabel.setStyleSheet('background-color: rgb(43, 43, 43);')
        stepButton = QtWidgets.QPushButton(step)
        statusCombo = QtWidgets.QComboBox()
        statusCombo.addItem('True')
        statusCombo.addItem('False')
        statusCombo.setCurrentIndex(statusCombo.findText(str(status)))
        lockButton = QtWidgets.QPushButton()

        lock = '{}/16/lock_icon.png'.format(ICON_DIR)
        unlock = '{}/16/unlock_icon.png'.format(ICON_DIR)
        path = lock if locked else unlock
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(path), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        lockButton.setIcon(icon)

        if status:
            statusCombo.setStyleSheet("background-color: rgb(19, 180, 108); color: black")
        else:
            statusCombo.setStyleSheet("background-color: rgb(255, 0, 60); color: black")

        layout.addWidget(assetLabel)
        layout.addWidget(stepButton)
        layout.addWidget(statusCombo)
        layout.addWidget(lockButton)
        layout.setStretch(0, 2)
        layout.setStretch(1, 0)
        layout.setStretch(2, 0)
        layout.setStretch(3, 0)

        item = QtWidgets.QListWidgetItem(self.ui.dataListWidget)
        item.setSizeHint(widget.sizeHint())

        data = (assetName, statusCombo, status, lockButton, locked)
        item.setData(QtCore.Qt.UserRole, data)
        self.ui.dataListWidget.setItemWidget(item, widget)

        statusCombo.currentTextChanged.connect(partial(self.set_stystyle_status, statusCombo))
        lockButton.clicked.connect(partial(self.set_locked_icon, item, lockButton))

    def set_locked_icon(self, item, lockButton):
        assetName, statusCombo, status, lockButton, locked = item.data( QtCore.Qt.UserRole)
        locked = False if locked else True
        lock = '{}/16/lock_icon.png'.format(ICON_DIR)
        unlock = '{}/16/unlock_icon.png'.format(ICON_DIR)
        path = lock if locked else unlock
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(path), QtGui.QIcon.Normal, QtGui.QIcon.Off)

        items = []

        if not self.ui.dataListWidget.selectedItems():
            data = (assetName, statusCombo, status, lockButton, locked)
            item.setData(QtCore.Qt.UserRole, data)
            lockButton.setIcon(icon)

        else:
            for item_sel in self.ui.dataListWidget.selectedItems():
                name, combobox, status, button, lockedOld = item_sel.data(QtCore.Qt.UserRole)
                if assetName == name:
                    items.append(name)

            if items:
                for item_sel in self.ui.dataListWidget.selectedItems():
                    name, combobox, status, button, lockedOld = item_sel.data(QtCore.Qt.UserRole)
                    data = (name, combobox, status, button, locked)
                    item_sel.setData(QtCore.Qt.UserRole, data)
                    button.setIcon(icon)
            else:
                data = (assetName, statusCombo, status, lockButton, locked)
                item.setData(QtCore.Qt.UserRole, data)
                lockButton.setIcon(icon)

    def set_stystyle_status(self, combo, text):
        if not self.ui.dataListWidget.selectedItems():
            if text == 'True':
                combo.setStyleSheet("background-color: rgb(19, 180, 108); color: black")
            else:
                combo.setStyleSheet("background-color: rgb(255, 0, 60); color: black")

        else:
            for item in self.ui.dataListWidget.selectedItems():
                assetName, combobox, statusOld, lockButton, locked = item.data(QtCore.Qt.UserRole)

                if text == 'True':
                    combobox.setStyleSheet("background-color: rgb(19, 180, 108); color: black")
                    combobox.setCurrentText('True')
                else:
                    combobox.setStyleSheet("background-color: rgb(255, 0, 60); color: black")
                    combobox.setCurrentText('False')


    def confirm_modify_data(self):
        for a in range(self.ui.dataListWidget.count()):
            assetName, combobox, statusOld, button, locked = self.ui.dataListWidget.item(a).data(QtCore.Qt.UserRole)
            #statusOld = self.cache.newData[assetName]['status']
            statusNew = combobox.currentText()
            statusNew = True if statusNew == 'True' else False
            if statusNew != statusOld:
                self.cache.newData[assetName]['status'] = statusNew

            self.cache.newData[assetName]['lock'] = locked

        self.cache.write()
        self.cache.register()

        if self.app_status:
            self.close()
        else:
            from rf_utils.widget import dialog
            dialog.MessageBox.success('Confirm', 'Submit Success')

def register_assets(entity, namespaces): 
    cache = CacheList(entity)
    # cache.clear()

    for namespace in namespaces: 
        cache.update_asset_data(namespace[1:], status=True)

    return cache

def get_reference_list(entity): 
    refs = mc.file(q=True, r=True)
    keyGrp = entity.projectInfo.asset.geo_grp() or 'Geo_Grp'
    paths = []

    for ref in refs: 
        namespace = mc.referenceQuery(ref, ns=True)
        path = mc.referenceQuery(ref, f=True)
        targetGeo = '%s:%s' % (namespace[1:], keyGrp)

        if mc.objExists(targetGeo): 
            paths.append(path)

    return paths 

def analyze_shot(entity, mode='auto'): 
    from rftool.utils import objectsInCameraView as ov
    from rf_maya.lib import sequencer_lib
    from rftool.utils import maya_utils
    from rf_utils import time_utils
    reload(ov)

    namespaces = []
    processChk = time_utils.Duration()
    shotName = entity.name_code
    refPaths = get_reference_list(entity)
    lenRef = len(refPaths)
    if mode == 'auto':
        if lenRef < 30:
            mode = 'selection'
        else:
            mode = 'calculation'

    if mc.objExists(shotName): 
        startFrame, endFrame, seqStartFrame, seqEndFrame = sequencer_lib.shot_info(shotName)
        cam = mc.shot(shotName, q=True, cc=True)
        print 'shot %s %s - %s' % (entity.name_code, startFrame, endFrame)
        
        if mc.objectType(cam) == 'camera': 
            cam = mc.listRelatives(cam, p=True)[0]

        with processChk: 
            rnNodes = [mc.referenceQuery(a, referenceNode=True) for a in refPaths]
            if mode == 'calculation': 
                targetGrp = entity.projectInfo.asset.geo_grp()
                outNodes = ov.getUnseenReferenceNodes(cam, targetGrp, refs=refPaths, timeRange=[startFrame, endFrame], step=5, viewport='legacy')
            elif mode == 'selection': 
                outNodes = ov.getUnseenReferenceNodes_selection(cam, 
                        refs=refPaths, timeRange=[startFrame, endFrame], step=5)
            inNodes = [a for a in rnNodes if a not in outNodes]
            namespaces = [mc.referenceQuery(a, ns=True) for a in inNodes]
    
    return namespaces

class AssetItem(QtWidgets.QWidget):
    """docstring for ShotItemBasic"""
    def __init__(self, parent=None):
        super(AssetItem, self).__init__(parent=parent)
        self.set_ui()

    def set_ui(self):
        self.layout = QtWidgets.QHBoxLayout()
        self.vlayout = QtWidgets.QVBoxLayout()
        self.hlayout = QtWidgets.QHBoxLayout()

        
        self.text1 = QtWidgets.QLabel()
        self.text2  = QtWidgets.QLabel()
        self.textstatus = QtWidgets.QLabel()

        
        self.hlayout.addWidget(self.text2)
        self.hlayout.addWidget(self.textstatus)

        self.vlayout.addWidget(self.text1)
        self.vlayout.addLayout(self.hlayout)
        self.layout.addLayout(self.vlayout)

        self.setLayout(self.layout)

        self.layout.setContentsMargins(2, 2, 2, 2)
        self.layout.setSpacing(0)
        self.layout.setStretch(0, 1)
        self.layout.setStretch(1, 1)
        self.layout.setStretch(2, 6)

        # setStyleSheet
        self.text1.setFont(QtGui.QFont("Arial", 9, QtGui.QFont.Bold))
        self.text2.setFont(QtGui.QFont("Arial", 8))

    def set_text1(self, text):
        self.text1.setText(text)

    def set_text2(self, text):
        self.text2.setText(text)

    def set_text_status(self, message):
        self.textstatus.setText(message)

    def set_text1_color(self, colors):
        self.text1.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_text2_color(self, colors):
        self.text2.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))

    def set_status_color(self, colors):
        self.textstatus.setStyleSheet('color: rgb(%s, %s, %s)' % (colors[0], colors[1], colors[2]))


def show(entity=None):
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = Main(entity, maya_win.getMayaWindow())
    myApp.show()
    return myApp


def show2(entity=None):
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = ModifyData(entity, maya_win.getMayaWindow())
    myApp.show()
    return myApp



# examples 
# cache = cache_list.CacheList(entity)
# cache.add_asset_key('arthur_001')
# cache.add_asset_key('jacobus_003')
# cache.set_hero_step('arthur_001', 'sim')
# cache.register()

# get hero step list
# cache.list_hero_keys()
# Result: [('arthur_001', 'sim')] # 