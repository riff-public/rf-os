import os 
from collections import OrderedDict

def get_os_env(): 
	envDict = dict()

	for key, value in os.environ.iteritems(): 
		valueList = value.split(';')
		envDict[key] = valueList

	return envDict

def compare_env(dict1, dict2, root1='', root2=''): 
	""" find a difference between 2 env 
	comparing dict2 to dict1 
	""" 
	dict1 = simplify_path(dict1)
	dict2 = simplify_path(dict2)

	diff = dict()
	for key, values in dict2.iteritems(): 
		if not key in dict1.keys(): 
			diff[key] = values 
		else: 
			compareValue = dict1[key]
			if root1: 
				compareValue = [a.replace(root1, '') for a in compareValue]
			for value in values: 
				addValue = value
				if root2: 
					addValue = value.replace(root2, '')
				if not addValue in compareValue: 
					if not key in diff.keys(): 
						diff[key] = [value]
					else: 
						diff[key].append(value)
	return diff

def simplify_path(envDict): 
	newDict = dict()

	for key, values  in envDict.iteritems(): 
		newValues = [a.replace('\\', '/') for a in values]
		newDict[key] = newValues
	return newDict

