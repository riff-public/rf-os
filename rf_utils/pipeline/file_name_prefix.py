# editRenderLayer
import os
import sys
import re

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

import maya.cmds as mc
from rf_utils.context import context_info
import maya.app.renderSetup.views.overrideUtils as utils
import maya.app.renderSetup.views.renderSetup as renderSetup

def file_name_prefix(department = 'light'):
    scene = context_info.ContextPathInfo()
    if scene.step == department:
        render_layers = mc.ls(type='renderLayer')
        # set output path 
        scene.context.update(step='render')
        outputPath = scene.path.scheme(key='outputStepPath').abs_path()
        mc.workspace(fr=['images', outputPath])

        # set format 
        mc.setAttr ("defaultRenderGlobals.animation", 1)
        mc.setAttr ("redshiftOptions.imageFormat", 1)
        mc.setAttr ("redshiftOptions.exrBits", 32)
        
        for layer in render_layers:
            if not 'defaultRenderLayer' in layer:
                scene.context.update(process=layer)
                mc.editRenderLayerGlobals(crl=layer)
                publ_name = scene.output_name(outputKey='output')
                outputStr = publ_name
                mc.setAttr("defaultRenderGlobals.imageFilePrefix", outputStr ,type='string')
                mc.editRenderLayerAdjustment("defaultRenderGlobals.imageFilePrefix")

def file_name_prefix2(department = 'light'):
    scene = context_info.ContextPathInfo()
    if scene.step == department:
        render_layers = mc.ls(type='renderLayer')
        # set output path 
        scene.context.update(step='render')
        outputPath = scene.path.scheme(key='outputStepPath').abs_path()
        mc.workspace(fr=['images', outputPath])

        # set format 
        mc.setAttr ("defaultRenderGlobals.animation", 1)
        mc.setAttr ("redshiftOptions.imageFormat", 1)
        mc.setAttr ("redshiftOptions.exrBits", 32)

        outputStr = '<RenderLayer>/<Version>/<RenderLayer>'
        mc.setAttr("defaultRenderGlobals.imageFilePrefix", outputStr ,type='string')

        # create UI if UI doesn't exists
        try:
            renderSetup.createUI()

            # delete all render version override
            renVers = mc.ls('renderVersion*', type = 'absUniqueOverride')
            for renVer in renVers:
                item = renderSetup.renderSetupWindow.centralWidget.renderSetupView.model().findProxyItem(renVer)
                item.delete()

            # create override all layer
            for layer in render_layers:
                if not 'defaultRenderLayer' in layer:
                    scene.context.update(process=layer)
                    mc.editRenderLayerGlobals(crl=layer)
                    publ_name = scene.output_name(outputKey='output')
                    outputStr = publ_name

                    # split rs_ out of layerName
                    layerName = layer.split('rs_')[-1]

                    layerPath = os.path.join(outputPath, layerName)
                    newVersion = check_layer_version(layerPath)

                    # outputStr = '<RenderLayer>/{}/<RenderLayer>'.format(newVersion)
                    # utils.createAbsoluteOverride('defaultRenderGlobals', 'imageFilePrefix')
                    # mc.setAttr("defaultRenderGlobals.imageFilePrefix", outputStr ,type='string')
                    # mc.editRenderLayerAdjustment("defaultRenderGlobals.imageFilePrefix")

                    utils.createAbsoluteOverride('defaultRenderGlobals', 'renderVersion')
                    mc.setAttr("defaultRenderGlobals.renderVersion", newVersion ,type='string')
                    mc.editRenderLayerAdjustment("defaultRenderGlobals.renderVersion")

                    # set expand state off
                    mc.setAttr("{}.expandedState".format(layerName), 0)

            # delete UI
            mc.deleteUI('MayaRenderSetupWindowWorkspaceControl')
            # create UI
            renderSetup.createUI()

            # rsAov_Cryptomatte
            # rsAov_Cryptomatte.filePrefix <RenderLayer>_Cryptomatte/<Version>/<BeautyFile>_<RenderPass>
            # set path output cryptomatte
            cryps = mc.ls('*Cryptomatte')
            cryps = cryps + mc.ls('*:*Cryptomatte')
            for cryp in cryps:
                mc.setAttr('{}.filePrefix'.format(cryp), "<RenderLayer>_Cryptomatte/<Version>/<BeautyFile>_<RenderPass>" ,type='string')\

        except Exception as e:
            print e


def check_layer_version(path):
    newVersion = 'v001'
    if os.path.exists(path):
        versions = os.listdir(path)
        verList = []
        for version in versions:
            versionPath =  "{}/{}".format(path, version)
            if os.path.isdir(versionPath):
                ver = re.findall('[vV]{1}[0-9]{3}', version)
                if ver:
                    version = ver[0].lower().split('v')[-1]
                    verList.append(version)
        if verList:
            newVersion = 'v{:03d}'.format(int(verList[-1]) + 1)
    return newVersion


def file_name_prefix_playblast():
    scene = context_info.ContextPathInfo()
    scene.context.update(publishVersion=scene.version)
    publ_name = scene.publish_name(hero=True, ext='.png')
    new_publ_name = publ_name.replace('.png', '')
    mc.setAttr("defaultRenderGlobals.currentRenderer", "mayaHardware", type="string")
    mc.colorManagementPrefs(e=True , outputTransformEnabled=True)
    mc.setAttr ("defaultRenderGlobals.animation", 1)
    mc.setAttr ("defaultRenderGlobals.imageFormat", 32)
    mc.setAttr("defaultRenderGlobals.imageFilePrefix", new_publ_name ,type='string')
    mc.setAttr("defaultRenderGlobals.outFormatControl" ,0)
    mc.setAttr("defaultRenderGlobals.putFrameBeforeExt", 1)
