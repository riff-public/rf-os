import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import rf_config as config
try:
	import maya.cmds as mc
except ImportError:
	pass

class Attr: 
	cacheOffset = 'cacheOffset'

class AssetTag(object):
	"""docstring for AssetTag"""
	def __init__(self, sels):
		super(AssetTag, self).__init__()
		self.sels = sels
		self.geoGrp = 'Geo_Grp'
		self.tagGrp = self.find_tag_group(self.sels)

	def find_tag_group(self, sels):
		if sels:
			if ':' in sels:
				namespace = sels.split(':')[0]
				geoGrp = '%s:%s' % (namespace, self.geoGrp)

				if mc.objExists(geoGrp):
					return geoGrp
				else:
					logger.warning('No %s found' % geoGrp)
					return ''


	def get_look(self):
		attr = '%s.%s' % (self.tagGrp, 'look')
		return mc.getAttr(attr) if mc.objExists(attr) else ''

	def get_mtr_path(self):
		attr = '%s.%s' % (self.tagGrp, 'mtrPath')
		return mc.getAttr(attr) if mc.objExists(attr) else ''

	def get_type(self):
		attr = '%s.%s' % (self.tagGrp, 'assetType')
		return mc.getAttr(attr) if mc.objExists(attr) else ''


def add_tag(obj, project='', assetType='', assetName='', assetId='', step='', process='', look='', mtrPath='', publishVersion='', publishPath='', adPath='', refPath=''):
	add_str_attr(obj, 'project', project)
	add_str_attr(obj, 'assetType', assetType)
	add_str_attr(obj, 'assetName', assetName)
	add_str_attr(obj, 'assetId', assetId)
	add_str_attr(obj, 'step', step)
	add_str_attr(obj, 'process', process)
	add_str_attr(obj, 'look', look)
	add_str_attr(obj, 'mtrPath', mtrPath)
	add_str_attr(obj, 'publishVersion', publishVersion)
	add_str_attr(obj, 'publishPath', publishPath)
	add_str_attr(obj, 'adPath', adPath)
	add_str_attr(obj, 'refPath', refPath)


def update_tag(obj, project='', assetType='', assetName='', assetId='', step='', process='', look='', mtrPath='', publishVersion='', publishPath='', adPath='', refPath=''):
	""" update only not exists """ 
	if project: 
		update_str_attr(obj, 'project', project)
	if assetType: 
		update_str_attr(obj, 'assetType', assetType)
	if assetName: 
		update_str_attr(obj, 'assetName', assetName)
	if assetId: 
		update_str_attr(obj, 'assetId', assetId)
	if step: 
		update_str_attr(obj, 'step', step)
	if process: 
		update_str_attr(obj, 'process', process)
	if look: 
		update_str_attr(obj, 'look', look)
	if mtrPath: 
		update_str_attr(obj, 'mtrPath', mtrPath)
	if publishVersion: 
		update_str_attr(obj, 'publishVersion', publishVersion)
	if publishPath: 
		update_str_attr(obj, 'publishPath', publishPath)
	if adPath: 
		update_str_attr(obj, 'adPath', adPath)
	if refPath: 
		update_str_attr(obj, 'refPath', refPath)

def add_str_attr(obj, attr, value):
	import maya.cmds as mc
	objAttr = '%s.%s' % (obj, attr)
	if not mc.objExists(objAttr):
		mc.addAttr(obj, ln=attr, dt='string')
		mc.setAttr(objAttr, e=True, keyable=True)
	mc.setAttr(objAttr, value, type='string') if value else None


def update_str_attr(obj, attr, value): 
	import maya.cmds as mc
	mc.lockNode(obj, l=False)
	objAttr = '%s.%s' % (obj, attr)
	if not mc.objExists(objAttr):
		add_str_attr(obj, attr, value)

	else:
		oldValue = mc.getAttr(objAttr)
		if not oldValue:
			add_str_attr(obj, attr, value)
		elif not oldValue == value:
			add_str_attr(obj, attr, value)
			if not attr == 'look':
				add_str_attr(obj, attr, value)


def add_cache_tag(obj, value): 
	update_str_attr(obj, Attr.cacheOffset, str(value))

