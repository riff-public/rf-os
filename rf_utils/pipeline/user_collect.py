#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import logging
import getpass
from collections import OrderedDict
from datetime import datetime

from Qt import QtWidgets 
from rftool.utils.ui import maya_win
from rf_utils import file_utils
from rf_utils.widget import dialog

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

_UiName = 'UserUi'

class Config: 
	userInfo = 'O:/Pipeline/logs/user/user_info.yml'
	departments = ['-', 'Model', 'Groom', 'Rig', 'CFX', 'Fx', 'Pipeline', 'Layout', 'Animation', 'Lighting', 'Comp', 'Others']
	companies = ['-', 'Riff', 'Alt']
	title = 'กรุณากรอกข้อมูลต่อไปนี้ก่อนเริ่มงาน'


class Ui(QtWidgets.QDialog):
	"""docstring for Ui"""
	def __init__(self, parent=None):
		super(Ui, self).__init__(parent=parent)
		self.data = OrderedDict()
		self.ui()
		self.init_ui()
		self.init_signals()

	def ui(self): 
		layout = QtWidgets.QVBoxLayout()
		self.title = QtWidgets.QLabel()
		title1 = QtWidgets.QLabel('Login: ')
		self.login = QtWidgets.QLineEdit()

		title2 = QtWidgets.QLabel('User: ')
		self.user = QtWidgets.QLineEdit()

		title3 = QtWidgets.QLabel('แผนก: ')
		self.department = QtWidgets.QComboBox()

		title4 = QtWidgets.QLabel('บริษัท: ')
		self.company = QtWidgets.QComboBox()

		self.button = QtWidgets.QPushButton('Confirm')

		layout.addWidget(self.title)
		layout.addWidget(title1)
		layout.addWidget(self.login)
		layout.addWidget(title2)
		layout.addWidget(self.user)
		layout.addWidget(title3)
		layout.addWidget(self.department)
		layout.addWidget(title4)
		layout.addWidget(self.company)
		layout.addWidget(self.button)

		self.setLayout(layout)
		self.setObjectName(_UiName)

	def init_ui(self): 
		self.title.setText(Config.title)
		self.department.addItems(Config.departments)
		self.company.addItems(Config.companies)

		login = getpass.getuser()
		self.login.setText(login)

		user = os.environ.get('RFUSER')
		self.user.setText(user)

	def init_signals(self): 
		self.button.clicked.connect(self.submit)

	def submit(self): 
		login = str(self.login.text())
		user = str(self.user.text())
		department = str(self.department.currentText())
		company = str(self.company.currentText())

		if login and user: 
			if not department == '-' and not company == '-': 
				# read data 
				dbData = self.read_config()

				data = OrderedDict()
				data['login'] = login
				data['user'] = user 
				data['department'] = department
				data['company'] = company
				data['modify'] = "info @%s" % get_time()

				dbData[login] = data
				file_utils.ymlDumper(Config.userInfo, dbData)
				dialog.MessageBox.success('Success', 'Thank you\nlogin: %s\nuser: %s\ndepartment: %s\ncompany: %s' % (login, user, department, company))
				self.close()

			else: 
				dialog.MessageBox.error('Error', 'Please select Department / Company')
		else: 
			dialog.MessageBox.error('Error', 'Please fill in login and user')


	def read_config(self): 
		check_config()
		data = file_utils.ymlLoader(Config.userInfo)
		return data

def show(): 
	maya_win.deleteUI(_UiName)
	app = Ui(maya_win.getMayaWindow())
	app.show()

def check_config(): 
	if not os.path.exists(Config.userInfo): 
		if not os.path.exists(os.path.dirname(Config.userInfo)): 
			os.makedirs(os.path.dirname(Config.userInfo))
		data = OrderedDict()
		file_utils.ymlDumper(Config.userInfo, data)


def check_extra_info(login, key): 
	check_config()
	data = file_utils.ymlLoader(Config.userInfo)
	if login in data.keys(): 
		if not key in data[login].keys(): 
			write_extra_info(login, key, False)
			return False 
		else: 
			return data[login][key]
	else: 
		return False 

def write_extra_info(login, key, value): 
	check_config()
	data = file_utils.ymlLoader(Config.userInfo)
	if login in data.keys(): 
		data[login][key] = value 
		data[login]['modify'] = '%s @%s' % (key, get_time())
		file_utils.ymlDumper(Config.userInfo, data)
		logger.debug('Write success "%s" "%s"'  % (key, value))


def get_time(): 
	return datetime.now().strftime("%A %d/%m/%Y %H:%M:%S")


def is_alt(): 
	login = getpass.getuser()
	alts = ['tanarat.p', 
			'tulyawat.s', 
			'pisan.z', 
			'panuwat.k', 
			'chanaporn.d', 
			'natthaphong.p', 
			'sukanya.c', 
			'nithirut.j', 
			'naruret.l', 
			'nontakit.n', 
			'natthawat.j', 
			'vasanapong.c', 
			'nattapol.b', 
			'tanagon.w', 
			'nattharika.z', 
			'kanokpong.y', 
			'pawinee.p', 
			'tanawat.w', 
			'thitikron.s', 
			'voraphol.s', 
			'soapark.c', 
			'sathorn.t', 
			'sakda.w', 
			'pattanasak.w', 
			'chaloempong.b'] 
	if login in alts: 
		return True 

