import yaml


class SceneBuilder:
    def __init__(self, asm_file_path):
        self.asm_file_path = asm_file_path
        self.asm_data = []
        self.init_state()

    def init_state(self):
        self.read_asm_data()

    def read_asm_data(self):
        with open(self.asm_file_path, 'r') as stream:
            try:
                self.asm_data = yaml.load(stream)
            except yaml.YAMLError as exc:
                print(exc)

    def get_asm_data(self):
        return self.asm_data

    def get_project_name(self):
        return self.asm_data['project']
