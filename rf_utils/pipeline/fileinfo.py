import os 
import sys 
import tempfile
import getpass
from rf_utils import file_utils
from collections import OrderedDict
from datetime import datetime
from rf_utils import custom_exception
from rf_utils.context import context_info
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class FileInfo(object):
    """docstring for FileInfo"""
    def __init__(self, path, user=None):
        super(FileInfo, self).__init__()
        self.path = path
        self.fullpath = self.path if not os.path.isdir(path) else ''
        self.user = user
        self.filename = '_fileinfo.json'
        self.datafile = '{}/{}'.format(self.path, self.filename)
        self.init_data()

    def init_data(self): 
        if not os.path.exists(self.path): 
            raise custom_exception.PipelineError('File not exists {}'.format(self.path))
        self._read_fileinfo()

    @property
    def info(self): 
        return FileItem(self.data.get(self.key))

    def list_files(self): 
        files = list() 
        for key, data in self.data.items(): 
            files.append(FileItem(data))

        return files

    def _read_fileinfo(self): 
        """ read json in folder if exists """ 
        self.data = OrderedDict()
        if os.path.exists(self.datafile): 
            self.data = file_utils.json_loader(self.datafile)
        return self.data


class RegisterFile(object):
    """docstring for RegisterFile"""
    def __init__(self, filepath, user=None):
        super(RegisterFile, self).__init__()
        self.filepath = filepath
        self.key = os.path.basename(filepath)
        self.path = os.path.dirname(self.filepath)
        self.user = user
        self.meta = FileInfo(self.path)
        self._readfile()

    def _readfile(self): 
        if self.key: 
            self.info = FileItem(file_data=OrderedDict())
            # readh hash md5 
            self.info.file_data['date'] = datetime.now().strftime('%Y-%b-%d_%H:%M.%S')
            # read size 
            self.info.file_data['path'] = self.filepath
            # read date
            self.info.file_data['user'] = self.user
            # full path 
            self.info.file_data['md5'] = file_utils.md5(self.filepath)
        else: 
            logger.debug('No file specified')

    def register(self): 
        """ write data to json file """ 
        if not self.key in self.meta.data.keys(): 
            history = list()

        else: 
            history = self.meta.data[self.key]['history']
            previous_data = self.meta.data[self.key]
            previous_data.pop('history')
            previous_data.pop('path')
            history.append(previous_data)
        self.meta.data[self.key] = self.info.file_data
        self.meta.data[self.key]['history'] = history

        file_utils.json_dumper(self.meta.datafile, self.meta.data)


class FileItem(object):
    """docstring for FileItem"""
    def __init__(self, file_data):
        super(FileItem, self).__init__()
        self.file_data = file_data

    def __str__(self): 
        return self.file_data.get('path')

    def __repr__(self): 
        return self.file_data.get('path') or str()

    def md5(self): 
        return self.file_data.get('md5')

    def user(self): 
        return self.file_data.get('user')

    def date(self): 
        return self.file_data.get('date')
        

class HeroConfig: 
    dirs = ['hero', 'texture', '_data']
    exception_dirs = ['.mayaSwatches']
    exception_files = ['Thumbs.db', ]
    exception_exts = ['.rstexbin']



class HeroInfo(object):
    """docstring for HeroInfo"""
    def __init__(self, path):
        super(HeroInfo, self).__init__()
        self.herodir = ['hero', 'texture']

        self.path = path
        self.heroinfo = 'heroInfo.hero.json'
        self.entity = context_info.ContextPathInfo(path=path)
        self.asset_path = self.entity.path.scheme(key='publishAssetNamePath').abs_path()
        self.datadir = '{}/_heroData'.format(self.entity.path.scheme(key='dataPath').abs_path())
        self.datafile = '{}/{}'.format(self.datadir, self.heroinfo)
        self.tmpfile = '{}/{}_temp.json'.format(tempfile.gettempdir(), self.entity.name)
        self.date = datetime.now().strftime('%Y-%b-%d_%H:%M.%S')
        self.data = self._read()
        self.override = False

    def update(self, change_only=True): 
        """ recursively walk and update all files """ 
        paths = self.get_path_list()
        file_data = self.data['files']
        totals = len(paths)
        update_files = list()

        for path in paths: 
            new_data = self.read_file_info(path)
            if path in file_data.keys(): 
                prev_data = file_data[path]

                # something change
                update = True
                if change_only: 
                    update = False
                    print('path', path)
                    print(prev_data['md5'], new_data['md5'])
                    if not prev_data['md5'] == new_data['md5']: 
                        update = True 
                if update: 
                    history = prev_data['history']
                    prev_data.pop('history')
                    history.append(prev_data)
                    new_data['history'] = history
                    file_data[path] = new_data
                    update_files.append(path)
            
            # new file 
            else: 
                file_data[path] = new_data

        # self.data['files'] = file_data
        self.data['date'] = self.date 
        self.data['user'] = getpass.getuser()
        self.data['total'] = '{} files'.format(totals)
        self.data['update'] = update_files
        self._write()
        print('Finished update fileinfo for {}'.format(self.entity.name))

    def _read(self): 
        return self.read_from_file(self.datafile)

    def read_from_file(self, path): 
        if not path == self.datafile: 
            self.override = True

        self.data = self.header()
        if os.path.exists(path): 
            self.data = file_utils.json_loader(path)
        return self.data

    def _write(self): 
        if not os.path.exists(os.path.dirname(self.datafile)): 
            os.makedirs(os.path.dirname(self.datafile))
        file_utils.json_dumper(self.datafile, self.data)

    def header(self): 
        header = OrderedDict()
        header['date'] = self.date
        header['user'] = getpass.getuser()
        header['total'] = '' 
        header['update'] = ''
        header['files'] = OrderedDict()
        return header


    def read_file_info(self, path): 
        # logger.debug('reading {} ...'.format(path))
        # print('reading {} ...'.format(path))

        filedict = OrderedDict()
        filedict['date'] = self.date
        filedict['md5'] = file_utils.md5(path)
        filedict['history'] = []
        return filedict

    def get_path_list(self): 
        paths = list()
        for hero in HeroConfig.dirs: 
            dirname = '{}/{}'.format(self.asset_path, hero)
            for root, dirs, files in os.walk(dirname): 
                root = root.replace('\\', '/')
                if os.path.split(root)[-1] in HeroConfig.exception_dirs: 
                    break
                for file in files: 
                    name, ext = os.path.splitext(file)
                    if ext and not ext in HeroConfig.exception_exts: 
                        if not file in HeroConfig.exception_files: 
                            full_path = '{}/{}'.format(root, file)
                            paths.append(full_path)

        # if not self.datafile in paths: 
        #     if os.path.exists(self.datafile): 
        #         paths.append(self.datafile)

        return paths

    def get_md5(self, filepath): 
        file_dict = self.data['files']
        for file, info in file_dict.items(): 
            if filepath == file: 
                return info['md5']

    def compare(self, data_file): 
        """ compare between 2 data files """ 
        data = file_utils.json_loader(data_file)
        cloud_files = data['files']
        local_files = self.data['files']
        # if local > remote -> sync 

        queue_files = list()

        for file, info in local_files.items(): 
            if not file in cloud_files.keys(): 
                queue_files.append(file)
            else: 
                cloud_info = cloud_files[file]
                if not info['md5'] == cloud_info['md5']: 
                    queue_files.append(file)

        return queue_files

    def get_files(self): 
        return list(self.data['files'].keys())

    def compare_files(self, src_file, dst_file): 
        queue_files = list()
        src_data = file_utils.json_loader(src_file)
        dst_data = file_utils.json_loader(dst_file)

        src_files = src_data['files']
        dst_files = dst_data['files']

        for file, src_info in src_files.items(): 
            if not file in dst_files.keys(): 
                queue_files.append(file)
            else: 
                dst_info = dst_files[file]
                if not src_info['md5'] == dst_info['md5']: 
                    queue_files.append(file)

        return queue_files
        

def batch_heroinfo(project): 
    """ batch for all assets """ 
    path = 'P:/{}/asset/publ'.format(project)
    paths = list()

    types = file_utils.list_folder(path)

    for asset_type in types: 
        type_path = '{}/{}'.format(path, asset_type)
        assets = file_utils.list_folder(type_path)

        for asset in assets: 
            asset_path = '{}/{}'.format(type_path, asset)
            paths.append(asset_path)

    for i, path in enumerate(paths): 
        print('\nScanning {} ...'.format(path))
        info = HeroInfo(path)
        info.update()
        print('\n{}/{} Complete'.format(i+1, len(paths)))



# rig.ma: 
#   date:       
#   user: 
#   size: 
#   hash: 
#   history: []

