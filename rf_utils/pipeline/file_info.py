# add file metadata information 

import os
import sys 
import logging
from rf_utils import file_utils
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class Name: 
	metafile = '_fileinfo.json'

class Attr: 
	redirect = 'redirect'

class File(object):
	"""docstring for File"""
	def __init__(self, inputPath):
		super(File, self).__init__()
		self.inputPath = inputPath.replace('\\', '/')
		self.rawMeta = self._read()

	@property
	def _metafile(self): 
		dirname = os.path.dirname(self.inputPath)
		metafile = '%s/%s' % (dirname, Name.metafile)
		return metafile

	def _read(self): 
		""" read meta file """ 
		metafile = self._metafile
		if not os.path.exists(metafile): 
			return dict()

		data = file_utils.json_loader(metafile)
		return data

	def _write(self): 
		""" write current raw data """ 
		return file_utils.json_dumper(self._metafile, self.rawMeta)

	def commit(self): 
		""" commit write """ 
		self._write()

	def meta(self, filePath=None): 
		""" show meta file for input path """ 
		filePath = os.path.basename(self.inputPath) if not filePath else filePath
		if filePath in self.rawMeta.keys(): 
			return self.rawMeta[filePath]
		return dict()

	def set_value(self, key, value): 
		""" set key and value """ 
		metaDict = self.meta()
		metaDict[key] = value 
		filekey = os.path.basename(self.inputPath)
		self.rawMeta[filekey] = metaDict

	def read_value(self, key): 
		metaDict = self.mete()
		return metaDict.get(key, None)

	def redirect(self): 
		meta = self.meta()
		return meta.get(Attr.redirect, self.inputPath)
		
	@property
	def set(self): 
		return SetValue(self)


class SetValue(object):
	"""docstring for SetValue"""
	def __init__(self, main):
		super(SetValue, self).__init__()
		self.main = main

	def redirect(self, value): 
		self.main.set_value(Attr.redirect, value)
		



""" 
# usage 

path = 'P:/test.ma'
newPath = 'P:/test.mb'
f = file_info.File(path)
f.set.redirect(newPath)
f.commit()

print f.redirect()
# P:/test.mb

""" 

