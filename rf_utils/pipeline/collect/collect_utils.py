import maya.cmds as mc 
import os
import sys 
import shutil

def collect_workspace(project, entityType='', type='', name='', step='anim', process='main', app='maya'): 
    """ collect maya files """ 
    # hard coded 
    entityPath= 'P:/%s/%s/work/%s/%s/%s' % (project, entityType, type, name, step)
    collects = []

    for root, dirs, files in os.walk(entityPath): 
        currentDir = os.path.split(root)[-1]
        if currentDir == app: 
            if files: 
                fileDict = dict()
                for file in files: 
                    fullPath = '%s/%s' % (root.replace('\\', '/'), file)
                    ft = os.path.getmtime(fullPath)
                    fileDict[ft] = fullPath

                if fileDict: 
                    latestFile = fileDict.get(sorted(fileDict.keys())[-1])
                    collects.append(latestFile)

    return collects


def find_assets(project, entityType, type): 
    path = 'P:/%s/%s/work/%s' % (project, entityType, type)
    entities = [a for a in os.listdir(path) if os.path.isdir('%s/%s' % (path, a))]
    return entities


def sync_workspace(project, entityType, type, name, step, process, app, dstRoot, copy=True): 
    # collect sources 
    entities = find_assets(project, entityType, type)
    wsFiles = []
    for asset in entities: 
        if name:
            if not asset == name:
                continue
        files = collect_workspace(project, entityType, type, asset, step=step, process=process, app=app)
        if files: 
            wsFiles += files

    for i, src in enumerate(wsFiles): 
        path = src.replace(':', '')
        dst = '%s/%s' % (dstRoot, path)
        print '%s/%s copy %s' % (i, len(wsFiles), dst)
        dirname = os.path.dirname(dst)
        
        if copy: 
            if not os.path.exists(dirname): 
                os.makedirs(dirname)
        
            shutil.copy2(src, dst)
