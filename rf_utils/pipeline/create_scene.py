import os
import sys
import logging
import lucidity

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from . import create_structure
from rf_utils.context import context_info
from rf_utils import file_utils
from rf_utils import user_info
from rf_utils.sg import sg_utils
sg = sg_utils.sg
moduleDir = os.path.dirname(sys.modules[__name__].__file__)
config = 'template_config.yml'

# create shotgun / create folder structure / create links.

def create(project, name, episode, sequence='', shortcode='', shotType='Shot', link=True):
    dirs = create_dir(project, name, episode, sequence)
    sgEntity = create_sg(project, name, episode, sequence, shortcode, shotType)
    # update link
    if link:
        update_dir_link_sg(project, name=name)
    return (dirs, sgEntity)

def create_dir(project, name, episode, sequence):
    context = get_context(project, name, episode, sequence)
    dirs = create_structure.create_workspace(context)
    # create_nuke_folder(context, project, name, episode, sequence)
    # create_design_structure(context, project, name, episode, sequence)
    return dirs

def create_sg(project, name, episode, sequence, shortcode, shotType='Shot'):
    """ create asset sg """
    projectEntity = sg.find_one('Project', [['name', 'is', project]], ['name', 'id'])
    episodeEntity = sg.find_one('Scene', [['project', 'is', projectEntity], ['code', 'is', episode]], ['code', 'id'])
    if not episodeEntity: 
        # episodeEntity = sg.create('Scene', {'project': projectEntity, 'code': episode})
        episodeEntity = create_episode(project=projectEntity, name=episode)
        
    entity = get_shot_entity(project, name, episode, sequence, projectEntity)
    seqEntity = get_seq_entity(project, name, episode, sequence, projectEntity, episodeEntity)
    template = get_template(project, shotType)
    data = {'project': projectEntity, 'code': name, 'sg_episode': episodeEntity,
            'sg_sequence_code': sequence, 'sg_sequence': seqEntity, 'sg_shortcode': shortcode, 'sg_shot_type': shotType, 'task_template': template}

    # create if not exist   
    if not entity:
        # user only allow on new creation
        user = user_info.User().sg_user()
        if user:
            data.update({'created_by': user})
        result = sg.create('Shot', data)
        logger.debug(result)
    else:  # update if exists
        result = sg.update('Shot', entity['id'], data)
        logger.warning('"%s" exists, update data only' % name)
    # add sg_episode.Scene.code to result
    result.update({'sg_episode.Scene.code': episode})
    return result

def create_episode(project, name):
    data = {'project': project, 'code': name}
    return sg.create('Scene', data)

def update_dir_link(project, name, episode, sequence):
    context = get_context(project, name, episode, sequence)
    display = 'Open'

    # sg entity
    entity = get_shot_entity(project, name, episode, sequence)

    # shot dir
    scene = context_info.ContextPathInfo(context=context)
    assetDir = '%s/' % str(scene.path.name().abs_path()).replace('/', '\\')
    shot_sg_field = 'sg_shot_dir'
    link1 = link_local_file('Shot', entity['id'], shot_sg_field, display, assetDir)

    return link1

def update_dir_link_sg(project, name='', id=int()):
    display = 'Open'
    shot_sg_field = 'sg_shot_dir'

    # context
    context = context_info.Context()
    context.use_sg(sg, project=project, entityType='scene', entityName=name, entityId=id)

    # shot dir
    scene = context_info.ContextPathInfo(context=context)
    sceneDir = '%s/' % str(scene.path.name().abs_path()).replace('/', '\\')
    link1 = link_local_file('Shot', int(context.id), shot_sg_field, display, sceneDir)

def get_shot_entity(project, name, episode, sequence, projectEntity=None):
    projectEntity = sg.find_one('Project', [['name', 'is', project]], ['name', 'id']) if not projectEntity else projectEntity
    entity = sg.find_one('Shot', [['project', 'is', projectEntity], ['code', 'is', name], ['sg_episode.Scene.code', 'is', episode], ['sg_sequence_code', 'is', sequence]], ['code', 'id'])
    return entity

def get_seq_entity(project, name, episode, sequence, projectEntity=None, episodeEntity=None): 
    projectEntity = sg.find_one('Project', [['name', 'is', project]], ['name', 'id']) if not projectEntity else projectEntity
    episodeEntity = sg.find_one('Scene', [['project', 'is', projectEntity], ['code', 'is', episode]], ['code', 'id']) if not episodeEntity else episodeEntity
    context = get_context(project, name, episode, sequence)
    scene = context_info.ContextPathInfo(context=context)
    seqEntity = sg.find_one('Sequence', [['project', 'is', projectEntity], ['sg_episode.Scene.code', 'is', episode], ['code', 'is', scene.sg_seq_name]], ['code', 'id'])
    if not seqEntity: 
        data = {'project': projectEntity, 'sg_shortcode': sequence, 'sg_episode': episodeEntity, 'sg_episodes': episodeEntity, 'code': scene.sg_seq_name}
        seqEntity = sg.create('Sequence', data)
    return seqEntity


def get_template(project, shotType='Shot', productionType='Production'):
    """ get task template from project config """
    path = '%s/%s' % (moduleDir, config)
    data = file_utils.ymlLoader(path)
    projectData = data.get(project, data.get('Default'))
    template = projectData[productionType]['Shot'][shotType]
    return template

def get_context(project, name, episode, sequence):
    context = context_info.Context()

    context.update(project=project, entityType='scene', entity=name, entityGrp=episode, entityParent=sequence)
    return context

def link_local_file(entityType, id, sg_field, display, path):
    path = path.replace('/', '\\')
    data = {sg_field: {'link_type': "local", 'local_path': path, 'name': display}}
    return sg.update(entityType, id, data)


def create_nuke_folder(context, project, name, episode, sequence):
    context.update(
        project=project,
        entityType='scene',
        entity=name,
        entityGrp=episode,
        entityParent=sequence,
        step="Comp"
    )
    context = context_info.ContextPathInfo(context=context)
    if context.step == "Comp":
        process_path = os.path.split(context.path.process().unc_path())[0]
        nuke_dir = "{process_path}/main/nuke".format(process_path=process_path)
        if not os.path.exists(nuke_dir):
            os.makedirs(nuke_dir)


def create_design_structure_obsolete(context, project, name, episode, sequence): 
    """ design structure for shot """ 
    step = 'design'
    processes_dict = {
                        'comp': ['source', 'output', 'comment', 'ae'], 
                        'vfx': ['source', 'output', 'comment', 'ae'], 
                        'light': ['comment', 'source'], 
                        'colorscript': ['comment', 'source']
                        }
    context.update(
        project=project,
        entityType='scene',
        entity=name,
        entityGrp=episode,
        entityParent=sequence,
        step=step
    )
    scene = context_info.ContextPathInfo(context=context)

    for process in processes_dict.keys(): 
        scene.context.update(process=process)
        processPath = scene.path.process().abs_path()
        if not os.path.exists(processPath): 
            processPath = scene.path.process().unc_path()

        for app in processes_dict[process]: 
            scene.context.update(app=app)
            app_path = scene.path.app().abs_path()
            unc_app_path = scene.path.app().unc_path()
            # print app_path

            if not os.path.exists(unc_app_path): 
                os.makedirs(unc_app_path)

        design = scene.copy()
        design.context.update(entityType='design')
        designRoot = '%s/%s' % (design.path.scheme(key='workDir').unc_path(), process)
        shortcutName = '{}.lnk'.format(scene.name)
        srcShortcut = '%s/%s' % (designRoot, shortcutName)
        dstPath = '%s' % (processPath)
        file_utils.create_shortcut(srcShortcut, dstPath)


def create_design_structure(context, project, name, episode, sequence): 
    step = 'design'
    context.update(
        project=project,
        entityType='scene',
        entity=name,
        entityGrp=episode,
        entityParent=sequence,
        step=step
    )
    scene = context_info.ContextPathInfo(context=context)
    templateDir = scene.projectInfo.template.scene(step).replace('\\', '/')
    #result:// $RFSCRIPT\core\rf_template\default\scene\design
    data = {'project': project, 'sequence': scene.sequence, 'name_code': scene.name_code}
    stepPath = scene.path.step().abs_path()
    stepPath = stepPath if os.path.exists(stepPath) else scene.path.step().unc_path()

    # create structure from dir template iteration
    for root, dirs, files in os.walk(templateDir): 
        root = root.replace('\\', '/')
        template = lucidity.Template(root, root)
        resolveDst = template.format(data)
        relDst = resolveDst.replace(templateDir, '')
        dst = '{}{}'.format(stepPath, relDst)
        
        if not os.path.exists(dst): 
            os.makedirs(dst)
            logger.debug('Created dir "{}"'.format(dst))

        if files: 
            for file in files: 
                filePath = '{}/{}'.format(root, file)
                pathTemplate = lucidity.Template(file, filePath)
                resolveNameDst = pathTemplate.format(data)
                relFileDst = resolveNameDst.replace(templateDir, '')
                dstFile = '{}{}'.format(stepPath, relFileDst)
                srcFile = '{}/{}'.format(root, file)
                
                if not os.path.exists(dstFile): 
                    file_utils.copy(srcFile, dstFile)
                    logger.debug('Created file "{}"'.format(dstFile))

    # create shortcut 
    design = scene.copy()
    design.context.update(entityType='design')

    processes = file_utils.list_folder(stepPath)

    for process in processes: 
        design.context.update(process=process)
        designRoot = '%s/%s' % (design.path.scheme(key='workDir').unc_path(), process)

        shortcutName = '{}.lnk'.format(scene.name)
        srcShortcut = '%s/%s/%s/%s' % (designRoot, scene.episode, scene.sequence, shortcutName)
        dstPath = '{}/{}'.format(stepPath, process)
        file_utils.create_shortcut(srcShortcut, dstPath)
        logger.debug('Shortcut created "{}"'.format(srcShortcut))


def test(): 
    project = 'Hanuman'
    name = 'hmn_mv_q0010_s0010'
    episode = 'mv3'
    sequence = 'q0010'
    shortcode = 's0010'
    create(project, name, episode, sequence, shortcode, shotType='Shot', link=True)


def batch_create(project, episode, sequence=None): 
    filters = [
        ['project.Project.name', 'is', project], 
        ['sg_episode.Scene.code', 'is', episode]]

    filters.append(['sg_sequence_code', 'is', sequence]) if sequence else None
    shots = sg.find('Shot', filters, ['code', 'sg_shortcode', 'sg_sequence_code'])

    for shot in shots: 
        print('Create structure', shot['code'])
        name = shot['code']
        sequence = shot['sg_sequence_code']
        shortcode = shot['sg_shortcode']
        create_dir(project, name, episode, sequence)
