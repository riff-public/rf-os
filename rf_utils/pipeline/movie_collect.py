import os
import sys
script_root = '{}/core'.format(os.environ['RFSCRIPT'])
if not script_root in sys.path:
    sys.path.append(script_root)

import argparse
import tempfile
import shutil
from collections import OrderedDict
from datetime import datetime
from pprint import pprint

# import logging
# logger = logging.getLogger(__name__)
# logger.addHandler(logging.NullHandler())

from rf_utils.sg import sg_process
from rf_utils import file_utils
from rf_utils.context import context_info
from rf_utils.pipeline import convert_lib

DEFAULT_OUTPUT_DIR = os.path.expanduser('~').replace('\\', '/')

def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-p', dest='project', type=str, help='The project name', required=True)
    parser.add_argument('-t', dest='task', type=str, help='The task to look for movie', required=True)
    parser.add_argument('-e', dest='episode', help='An Episode to find shots', required=True)
    parser.add_argument('-q', dest='sequences', nargs='+', help='Sequences to find shots', default=[])
    parser.add_argument('-s', dest='shots', nargs='+', help='Specify shots', default=[])
    parser.add_argument('-dir', dest='directory', type=str, help='To output directory', default=DEFAULT_OUTPUT_DIR)
    parser.add_argument('-dl', dest='download', type=int, help='Download movie from SG if path does not exist', default=0)
    parser.add_argument('-com', dest='combine', type=int, help='Combine clips into full movie', default=0)
    parser.add_argument('-uco', dest='use_cutorder', type=int, help='To use SG cut_order or not', default=0)
    parser.add_argument('-rv', dest='remove_version', type=int, help='Remove version from filename', default=0)
    parser.add_argument('-dd', dest='create_date_dir', type=int, help='Create directory named by date and time', default=0)
    return parser

def get_movie_data(project, task, episode, sequences=[], shots=[]):
    # list all shots
    shot_filters = [['sg_shot_type', 'is', 'Shot'], ['sg_status_list', 'is_not', 'omt']]
    all_shots = sg_process.get_all_shots(project, filters=shot_filters, extraFields=['sg_status_list'])
    ep_shots = [s for s in all_shots if s['sg_episode'] and s['sg_episode'].get('name')==episode]

    # no shot specify, use episode (and sequence, if provided)
    seq_shots = OrderedDict()
    filtered_shots = []
    shot_dict = {'shot': '', 'path': '', 'url': '', 'status': '', 'updated': ''}
    if not shots:
        if sequences:
            # shots = [s for s in ep_shots if s['sg_sequence_code'] in sequences]
            for s in ep_shots:
                seq_code = s['sg_sequence_code']
                if seq_code in sequences:
                    if seq_code not in seq_shots:
                        seq_shots[seq_code] = []
                    shot_data = shot_dict.copy()
                    shot_data.update({'shot':s})
                    seq_shots[seq_code].append(shot_data)
                    filtered_shots.append(s)
        else:  # all shots in ep
            for s in ep_shots:
                seq_code = s['sg_sequence_code']
                if seq_code not in seq_shots:
                    seq_shots[seq_code] = []

                shot_data = shot_dict.copy()
                shot_data.update({'shot':s})
                seq_shots[seq_code].append(shot_data)
                filtered_shots.append(s)
    else:  # shot specify
        ep_shotcodes = ['{}_{}'.format(s.get('sg_sequence_code'), s.get('sg_shortcode')) for s in ep_shots if s.get('sg_sequence_code') and s.get('sg_shortcode')]
        for shot in shots:
            # input arg came as shot entity
            if isinstance(shot, dict):
                seq_code = shot.get('sg_sequence_code')
                shot_code = shot.get('sg_shortcode')
                if '{}_{}'.format(seq_code, shot_code) in ep_shotcodes:
                    if seq_code not in seq_shots:
                        seq_shots[seq_code] = []
                    if shot.get('sg_status_list') not in (None, 'omt'):  # only shot that's not omitted
                        shot_data = shot_dict.copy()
                        shot_data.update({'shot':shot})
                        seq_shots[seq_code].append(shot_data)
                        filtered_shots.append(shot)
            # input arg came as string
            elif isinstance(shot, (str, unicode)):
                if shot in ep_shotcodes:
                    index = ep_shotcodes.index(shot)
                    shot = ep_shots[index]
                    seq_code = shot.get('sg_sequence_code')
                    if seq_code not in seq_shots:
                        seq_shots[seq_code] = []
                    if shot.get('sg_status_list') not in (None, 'omt'):  # only shot that's not omitted
                        shot_data = shot_dict.copy()
                        shot_data.update({'shot':shot})
                        seq_shots[seq_code].append(shot_data)
                        filtered_shots.append(shot)
    if not filtered_shots:
        print('Error: No shot found!')
        return

    # pprint(seq_shots)

    # get all versions from shots
    all_versions = sg_process.get_versions_from_entities_tasks(entities=filtered_shots, 
                            tasks=[task], 
                            extraFields=['sg_task.Task.task_assignees', 
                                        'sg_task.Task.entity', 
                                        'sg_task.Task.sg_status_list', 
                                        'sg_uploaded_movie'])
    # pprint(['{} {}'.format(v['code'], v['created_at']) for v in all_versions])
    if not all_versions:
        print('Error: No version found!')
        return

    # get paths
    for seq_code, shot_data in seq_shots.iteritems(): 
        for si, shot_dict in enumerate(shot_data):
            shot = shot_dict['shot']
            shot_id = shot.get('id')
            rem_indices = []
            shot_versions = []
            for vi, version in enumerate(all_versions):
                version['sg_task.Task.entity'].get('id')
                if version['sg_task.Task.entity'].get('id') == shot_id:
                    shot_versions.append(version)
                    rem_indices.append(vi)
            if shot_versions:
                # get latest movie
                latest_version = sorted(shot_versions, key=lambda f: f['created_at'])[-1]
                
                # store url
                url = latest_version.get('sg_uploaded_movie')
                seq_shots[seq_code][si]['url'] = url

                # updated
                updated = latest_version.get('created_at', '')
                seq_shots[seq_code][si]['updated'] = updated

                # store path
                mov_path = latest_version.get('sg_path_to_movie').replace('\\', '/') if latest_version.get('sg_path_to_movie') else ''
                if mov_path:
                    if '_preview' in mov_path:
                        basename, ext = mov_path.split('_preview')
                        mov_path = '{}{}'.format(basename, ext)

                if mov_path:
                    seq_shots[seq_code][si]['path'] = mov_path

                # store status
                status = latest_version.get('sg_task.Task.sg_status_list') if latest_version.get('sg_task.Task.sg_status_list') else ''
                if status:
                    seq_shots[seq_code][si]['status'] = status

                if not os.path.exists(mov_path):
                    if url:
                        print('Warning, Movie is only available online: {}'.format(shot['code']))
                    else:
                        print('Error, Movie is not available: {}'.format(shot['code']))
            
                # remove version that has been found
                for index in sorted(rem_indices, reverse=True):
                    del all_versions[index]
            else:
                print('Warning, No version found for: {}'.format(shot['code']))

    # pprint(seq_shots)
    # for seq_code, shot_data in seq_shots.iteritems():
    #     print('=====', seq_code)
    #     pprint(shot_data)
    seq_shots = OrderedDict(sorted(seq_shots.items()))
    return seq_shots

def main(project, task, episode, sequences=[], shots=[], directory=DEFAULT_OUTPUT_DIR, download=False, combine=False, 
        use_cutorder=False, remove_version=False, create_date_dir=False, **kwargs):
    if not os.path.exists(directory):
        try:
            os.makedirs(directory)
        except Exception:
            msg = '** Error, Invalid directory!'
            print(msg)
            callback_func(msg, 'error') if callback_func else None
            return 

    directory = directory.replace('\\' ,'/')

    # create temp
    tmp_dir = ''
    if download:
        tmp_dir = tempfile.mkdtemp().replace('\\', '/') 
        msg = 'Temporary directory: {}'.format(tmp_dir)
        print(msg)

    callback_func = kwargs.get('callback')
    # movie_data arg will be passed only when calling from UI
    # movie_data = [(shot, mov_path, url), '...']
    sequence_names = []
    if 'movie_data' in kwargs:
        movie_data = kwargs['movie_data']
        for data in movie_data:
            seq = data[0].get('sg_sequence_code')
            if seq and seq not in sequence_names:
                sequence_names.append(seq)

    else:  # calling from command line will fetch seq_shot dict
        # seq_shots[seq_code] = [ {'shot':shot, 'path':'path/to/movie', 'url':'https://'}, ... ]
        seq_shots = get_movie_data(project=project, task=task, episode=episode, sequences=sequences, shots=shots)
        if not seq_shots:
            msg = '** Error, Cannot get movie data!'
            print(msg)
            callback_func(msg, 'error') if callback_func else None
            return
        sequence_names = seq_shots.keys()
        movie_data = []
        for seq_code, shot_data in seq_shots.iteritems():
            for shot in shot_data:
                data = (shot['shot'], shot['path'], shot['url'])
                movie_data.append(data)

    # reorder movie_data by cut order
    ordername = 'SG cutorder' if use_cutorder else 'name'
    msg = 'Sorting data by {}...'.format(ordername)
    print(msg)

    if use_cutorder:
        movie_data = sorted(movie_data, key=lambda f: f[0]['sg_cut_order'])
    else:  # else sort by shot code
        movie_data = sorted(movie_data, key=lambda f: f[0]['code'])
    
    # flatten movie list
    paths = []
    for shot, path, url in movie_data:
        if path:
            if not os.path.exists(path): 
                if download:
                    tmp_path = '{}/{}'.format(tmp_dir, os.path.basename(path))
                    msg = 'Downloading from SG: {}'.format(shot['code'])
                    print(msg)
                    callback_func(msg, 'working') if callback_func else None
                    try:
                        sg_process.sg.download_attachment(url, file_path=tmp_path)
                        paths.append(tmp_path)
                    except Exception:
                        msg = '** Error: Failed to download: {}'.format(shot['code'])
                        print(msg)
                        callback_func(msg, 'error') if callback_func else None
            else:
                paths.append(path)
        else:
            print('* Warning, no media path found: {}'.format(shot['code']))
    if not paths:
        msg = '** Error, No valid movie found.'
        print(msg)
        callback_func(msg, 'error') if callback_func else None
        return

    msg = 'Found {} movie(s)'.format(len(paths))
    print(msg)
    callback_func(msg, 'working') if callback_func else None
    result = None

    # init context path info
    context = context_info.Context()
    context.use_sg(sg_process.sg, project=project, entityType='scene')
    scene = context_info.ContextPathInfo(context=context)
    # construct collect name
    seq_name = '' if len(sequence_names)!=1 else sequence_names[0]
    name_elements = [scene.project_code, episode, seq_name, task.replace('_', ''), datetime.today().strftime('%y%m%d%H%M%S')]
    collect_name = '_'.join([n for n in name_elements if n!=''])
    msg = 'Collect name: {}'.format(collect_name)
    print(msg)

    if combine:  # combine clips
        # construct output path
        output_path = '{}/{}.mov'.format(directory, collect_name)
        # get fps
        fps = scene.projectInfo.render.fps()
        # get movie size
        size = scene.projectInfo.render.final_outputH()
        msg = 'Merging movies [fps:{}][size:{}x{}]: {}'.format(fps, size[0], size[1], collect_name)
        print(msg)
        callback_func(msg, 'working') if callback_func else None
        result = convert_lib.merge_mov(sources=paths, output_path=output_path, fps=fps, size=size)
    else:  # copy
        num_movies = len(paths)
        result = []
        if create_date_dir:
            directory = '{}/{}'.format(directory, collect_name)
        for i, path in enumerate(paths):
            basename = os.path.basename(path)
            if remove_version:
                fn, ext = os.path.splitext(basename)
                basename = '{}{}'.format(fn.split('.')[0], ext)
            msg = 'Copying movie ({}/{}): {}'.format(str(i+1), num_movies, basename)
            print(msg)
            callback_func(msg, 'working') if callback_func else None
            des = '{}/{}'.format(directory, basename)
            res = file_utils.copy(path, des)
            result.append(res)
         
    # remove download temp
    if os.path.exists(tmp_dir):
        shutil.rmtree(tmp_dir)

    pprint(result)
    return result

if __name__ == "__main__":
    print(':::::: Movie Collect ::::::')
    parser = setup_parser('Movie Collect')
    params = parser.parse_args()
    main(project=params.project,
        task=params.task,
        episode=params.episode,
        sequences=params.sequences,
        shots=params.shots,
        directory=params.directory,
        download=params.download,
        combine=params.combine, 
        use_cutorder=params.use_cutorder,
        remove_version=params.remove_version,
        create_date_dir=params.create_date_dir)

'''
C:\Python27\python.exe %RFSCRIPT%\core\rf_utils\pipeline\movie_collect.py -p Hanuman -t anim -e ch06a -q q0050 -dir D:\__playground\test_collect1 -dl 1 -com 1

'''
