import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from collections import OrderedDict
from rf_utils import project_info
from rf_utils.context import context_info
from rf_utils import file_utils
from rf_utils.pipeline.permission import set_permission
# reload(set_permission)
#from rf_utils.pipeline.permission import set_permission
import subprocess

caches = dict()
py = "%s/core/rf_lib/python/2.7.11/python.exe"% (os.environ.get('RFSCRIPT'))
script = "%s/core/rf_utils/pipeline/permission/set_permission.py" % (os.environ.get('RFSCRIPT'))


def create_workspace_obsolete(context):
    """ create structure need context with
    project, entityName, entityType, entityGrp, entityParent
    """
    projectInfo = project_info.ProjectInfo(context.project)
    projectInfo.set_project_env()

    if valid_context(context):
        rootWork = projectInfo.rootProject
        rootString = '$%s' % project_info.PROJECT_VAR

        dirList = generate_dir_list(context)
        dirCreated = []

        for relPath in dirList:
            # absPath = path.replace(rootString, rootWork)
            path = context_info.RootPath(relPath).unc_path()
            logger.info(path)
            try:
                if not os.path.exists(path):
                    os.makedirs(path)
                    logger.info('Create dir -> %s' % path)
                    dirCreated.append(path)
            except Exception as e:
                path = context_info.RootPath(relPath).abs_path()
                if not os.path.exists(path):
                    os.makedirs(path)
                    logger.info('Create dir -> %s' % path)
                    dirCreated.append(path)

            #set Permission
            userArtist = 'Riff-Proj-%s-Artist'%context.project
            userMGM = 'Riff-Proj-%s-MGM'%context.project
            accessPath = os.path.split(path)[0]
            accessPath = os.path.split(accessPath)[0]
            set_permission.set_permission_cmd(userArtist, filePath = [accessPath])
            # cmdArt = '"%s" "%s" -u "%s" -f %s'%(py, script, userArtist, accessPath)
            # subprocess.call(cmdArt, shell=True)
            set_permission.set_permission_cmd(userMGM, filePath = [accessPath])
            # cmdMGM = '"%s" "%s" -u "%s" -f %s'%(py, script, userMGM, accessPath)
            # subprocess.call(cmdMGM, shell=True)
            logger.info('Set Permission dir -> %s' % accessPath)

        logger.info('%s dirs created' % len(dirList))
        entity_detail = {
            'entity': context.entity,
            'dir_created': dirCreated
        }
        return entity_detail

    else:
        logger.warning('Context invalid %s' % context)


def generate_dir_list(context):
    projectInfo = project_info.ProjectInfo(context.project)
    work = projectInfo.schema.context_key().get('work').get('key')
    contextKey = projectInfo.schema.context_key()
    listData = []

    # structure config
    config = projectInfo.schema.work(context.entityType)
    # convert asset or scene structure to use generic key ->
    # "asset" -> "entityType"
    # "assetType" -> "entityType"
    schemaConfig = config.convert_parentkey()
    schemaKeys = context_info.extract_keys(schemaConfig)
    path = schemaConfig

    for key in schemaKeys:
        contextValue = context.contextArgs.get(key)
        staticValue = get_static_value(context.entityType, projectInfo.context_key().get(key), work)
        value = contextValue if contextValue else staticValue

        if type(value) == type(str()):
            value = [value]

        if value:
            listData.append(value)

    pathList = path_list(listData)
    return pathList

def create_look(sg, project, assetId):
    # do not create process folder for look, below is the old method
    return 
    # from rf_utils.sg import sg_process
    # context = context_info.Context()
    # context.use_sg(sg, project=project, entityType='asset', entityId=assetId)
    # entity = {'type': 'Asset', 'id': assetId}
    # looks = sg_process.get_tasks_by_step(entity, step='texture')
    # asset = context_info.ContextPathInfo(context=context)

    
    # for look in looks:
    #     lookname = look['sg_name'] or 'main'
    #     for step in ['texture', 'lookdev', 'groom']:
    #         asset.context.update(look=lookname, process=lookname, app='maya', step=step)

    #         try:
    #             dirname = asset.path.workspace().unc_path()
    #             if not os.path.exists(dirname):
    #                 os.makedirs(dirname)
    #                 logger.debug('create dir %s' % dirname)

    #         except Exception as e:
    #             dirname = asset.path.workspace().abs_path()
    #             if not os.path.exists(dirname):
    #                 os.makedirs(dirname)
    #                 logger.debug('create dir %s' % dirname)


def create_workspace(context): 
    create = True
    project = project_info.ProjectInfo(context.project)
    project.set_project_env()
    context.setup_root(context.contextArgs)
    templatePath = project.get_template_structure()
    permissionDict = project.permission()
    permissionArtistPaths = permissionDict.get('artist').get(context.entityType, dict()).get('full')
    permissionManagePaths = permissionDict.get('manage').get(context.entityType, dict()).get('full')
    dirCreated = []

    if context.entityType == 'asset': 
        filterKey = ['asset', '{entity}']
        dirCreated += create_from_template(context, templatePath, dstPath='', filterKey=filterKey, create=create)
        filterKey = ['design/work/{entityGrp}/{entity}'] # this is hardcode due to bad design structure
        dirCreated += create_from_template(context, templatePath, dstPath='', filterKey=filterKey, create=create)

        # permission 
        apply_permission(context, permissionArtistPaths, permissionDict.get('user').get('artist'))
        apply_permission(context, permissionManagePaths, permissionDict.get('user').get('manage'))


    if context.entityType == 'scene': 
        filterKey = ['scene', '{entity}']
        dirCreated += create_from_template(context, templatePath, dstPath='', filterKey=filterKey, create=create)
        filterKey = ['design', '{entityGrp}/{entityParent}'] # this is hardcode due to bad design structure
        dirCreated += create_from_template(context, templatePath, dstPath='', filterKey=filterKey, create=create)

        # permission 
        apply_permission(context, permissionArtistPaths, permissionDict.get('user').get('artist'))
        apply_permission(context, permissionManagePaths, permissionDict.get('user').get('manage'))

    entity_detail = {
            'entity': context.entity,
            'dir_created': sorted(dirCreated)
        }
    return entity_detail


def apply_permission(context, paths, user): 
    """ set permission at the same level of .Permission-Artist file """ 
    entity = context_info.ContextPathInfo(context=context)
    project = entity.project
    target_permission_dirs = []
    for path in paths: 
        resolvePath = entity.resolve_pattern(path)
        user = entity.resolve_pattern(user)
        resolvePath = context_info.RootPath(resolvePath).unc_path()
        if resolvePath.endswith('/*'): 
            resolvePath = resolvePath[:-2]
            dirs = file_utils.list_folder(resolvePath)
            target_permission_dirs += ['%s/%s' % (resolvePath, a) for a in dirs]
        else: 
            target_permission_dirs.append(resolvePath)

    for path in target_permission_dirs: 
        logger.debug('Permission set %s' % path)

    set_permission.set_permission_cmd(user, target_permission_dirs)
    set_permission.set_permission_cmd(user, target_permission_dirs)


def copy_file_template(context):
    asset = context_info.ContextPathInfo(context=context)
    if context.entityType == context_info.ContextKey.design:
        workspace = context_info.ContextKey.design
        app = 'psd'
        process = 'main'
        version = 'v001'
        step = 'design'

        asset.context.update(workspace=workspace, app=app, process=process, version=version, step=step)
        workfile = asset.work_name()
        workspace = asset.path.workspace()
        dst = '%s/%s' % (workspace, workfile)
        src = asset.projectInfo.template_file(step, '%s.psd' % app)

        if os.path.exists(src):
            if not os.path.exists(os.path.dirname(dst)):
                os.makedirs(os.path.dirname(dst))
            result = shutil.copy2(src, dst)

            if os.path.exists(dst):
                logger.info('Copy template success %s' % dst)
            else:
                logger.warning('Copy template failed %s' % dst)


def path_list(listData):
    combineData = []
    for i, data in enumerate(listData):
        if i == 0:
            combineData = data
        if i < len(listData)-1:
            target = listData[i+1]
            combineData = add_element(combineData, target, '/')

    return combineData


def add_element(valueA, valueB, joinStr):
    result = []
    for a in valueA:
        for b in valueB:
            result.append(joinStr.join([a, b]))
    return result


def get_static_value(entityType, data, workspace='work'):
    """ return name if attr "static" value """
    attr = data.get('attr')
    name = data.get('name')
    mapValue = data.get('map')
    staticList = data.get('staticList')

    if attr in [context_info.ContextKey.group, context_info.ContextKey.dynamicGrp]:
        return get_static_value(entityType, name.get(entityType) or name.get(workspace), workspace)

    if attr == context_info.ContextKey.static:
        if staticList:
            staticList.append(name)
            return staticList

        return name
        # return name

    if attr == context_info.ContextKey.template:
        return [mapValue[a].get('name') for a in mapValue.keys()]


def valid_context(context):
    """ check if all context are valid
        project, entityName, entityType, entityGrp, entityParent
    """
    keys = [context_info.ContextKey.project,
            context_info.ContextKey.entityType,
            context_info.ContextKey.entity,
            context_info.ContextKey.entityGrp,
            # context_info.ContextKey.entityParent]
            ]

    validContext = all(context.contextArgs.get(a) for a in keys)
    return validContext


def list_from_template(context, templatePath, dstPath='', filterKey=[], filterType='all'): 
    entity = context_info.ContextPathInfo(context=context)
    templatePath = templatePath.replace('\\', '/')
    structureList = []
    copyList = []
    ignoreFiles = ['.keep', 'Thumbs.db']

    # if any pattern don't have key in context.contextArgs.keys(), we will skip process those structures 
    # For ex. 
    # pattern = '{RFPROJECT}/{project}/asset/work/{entityGrp}/{entityParent}/{entity}'' 
    # key are ['project', 'entityGrp', 'entityParent', 'RFPROJECT', 'entity']
    # context.contextArgs.keys()
    # key are ['project', 'RFUSER', 'RFPUBL', 'RFPROJECT', 'RFVERSION']
    # from this example, we can see that entityGrp, entityParent and entity are missing from contextArgs.keys(). 
    # So this path will not be processed. 
    # This is very important for create structure to have key only "project". Otherwise it will create other path too.

    for root, dirs, files in os.walk(templatePath): 
        for d in dirs: 
            src = '{}/{}'.format(root, d).replace('\\', '/')
            proceed = True
            if filterKey: 
                if filterType == 'all': 
                    proceed = True if all(a in src for a in filterKey) else False 
                if filterType == 'any': 
                    proceed = True if any(a in src for a in filterKey) else False 

            if proceed: 
                path = entity.resolve_pattern(src)
                if path: 
                    path = path.replace('{}/'.format(templatePath), '') # remove root
                    if dstPath: 
                        path = '{}/{}'.format(dstPath, path)
                    structureList.append(path)

        for file in files: 
            if not file in ignoreFiles: 
                src = '{}/{}'.format(root, file).replace('\\', '/')
                proceed = True 
                if filterKey: 
                    if filterType == 'all': 
                        proceed = True if all(a in src for a in filterKey) else False 
                    if filterType == 'any': 
                        proceed = True if any(a in src for a in filterKey) else False 

                if proceed: 
                    filepath = entity.resolve_pattern(src)
                    if filepath: 
                        filepath = filepath.replace('{}/'.format(templatePath), '') # remove root
                        if dstPath: 
                            filepath = '{}/{}'.format(dstPath, filepath)
                        copyList.append((src, filepath))

    return structureList, copyList


def create_from_template(context, templatePath, dstPath='', filterKey=[], filterType='all', create=True): 
    structureList, copyList = list_from_template(
                                                    context, 
                                                    templatePath, 
                                                    dstPath=dstPath, 
                                                    filterKey=filterKey, 
                                                    filterType=filterType
                                                    )
    logger.info('Creating {} directories'.format(len(structureList)))
    dirCreated = []

    for path in structureList: 
        path = context_info.RootPath(path).abs_path()
        unc_path = context_info.RootPath(path).unc_path()
        if not os.path.exists(path): 
            if create: 
                try: 
                    os.makedirs(path)
                    dirCreated.append(path)
                except: 
                    logger.debug('Try create using unc path {}'.format(unc_path))
                    try: 
                        os.makedirs(unc_path)
                        dirCreated.append(path)
                    except: 
                        logger.error('cannot create path {}'.format(unc_path))
            logger.debug(path)

    logger.info('Copying {} files'.format(len(structureList)))

    for src, dst in copyList: 
        # do not copy file start with "."
        dst = context_info.RootPath(dst).abs_path()
        unc_dst = context_info.RootPath(dst).unc_path()
        if not os.path.basename(src).startswith('.'): 
            if create: 
                try: 
                    file_utils.copy(src, dst)
                    dirCreated.append(dst)
                except: 
                    logger.debug('Try copy to unc path {}'.format(unc_dst))
                    try: 
                        file_utils.copy(src, unc_dst)
                        dirCreated.append(dst)
                    except: 
                        logger.error('cannot copy to {}'.format(unc_dst))
            logger.debug(dst)

    logger.info('Complete')
    return dirCreated


def os_walk_cache( dir ):
   if dir in caches:
      for x in caches[ dir ]:
         yield x
   else:
      caches[ dir ]    = []
      for x in os.walk( dir ):
         caches[ dir ].append( x )
         yield x
   raise StopIteration()

""" 
create only asset 
so set filterKey = ['{entity}']
create_from_template(context, 'D:/Dropbox/script_server/core/rf_template/_structure/default', filterKey=['{entity}', '{entityParent}'], create=False)
"""
