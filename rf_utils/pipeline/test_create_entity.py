from rf_utils.pipeline import create_asset
from rf_utils.pipeline import create_scene
from rf_utils.sg import sg_process
reload(create_asset)
reload(create_scene)
reload(sg_process)


def asset(): 
	# create asset 
	project = 'Hanuman'
	name = 'testSet'
	assetType = 'set'
	assetSubtype = 'main'
	templateKey = assetType
	mainAsset = ''

	dirs, setA = create_asset.create(project, name, assetType, assetSubtype, mainAsset, tags=None, template=templateKey, setEntity=[], link=True)
	# setA = create_asset.create_sg(project, name, assetType, assetSubtype, mainAsset, templateKey, setEntity=None, tags=None)

	name = 'testProp'
	assetType = 'prop'
	templateKey = assetType
	dirs, propA = create_asset.create(project, name, assetType, assetSubtype, mainAsset, tags=None, template=templateKey, setEntity=[], link=True)
	# propA = create_asset.create_sg(project, name, assetType, assetSubtype, mainAsset, templateKey, setEntity=setA, tags=None)


	# list all episode 
	# link act1 to episode
	eps = sg_process.get_episodes(project)
	link_ep = 'act1'
	episodes = [a for a in eps if a['code'] == link_ep]

	# list all sequence 
	# link sequence 'hnm_act1_q9990a' to sequence
	seqs = sg_process.get_sequences(project, episodes[0])
	link_seq = 'hnm_act1_q9990a'
	sequences = [a for a in seqs if a['code'] == link_seq]

	# list all tags 
	tags = sg_process.get_tags()
	tags = [a for a in tags if a['name'] in ['sky', 'cloud']]

	# create example shot
	shot = create_shot()

	# link extra relation to asset 
	description = 'test prop asset'
	create_asset.link_asset_relation(propA['id'], episodes=episodes, sequences=sequences, shots=[shot], tags=tags, description=description)



def shot(): 
	project = 'Hanuman'
	name = 'hnm_act1_q9990a_s0010'
	episode = 'act1'
	sequence = 'q9990a'
	shortcode = 's0010'
	return create_scene.create_sg(project, name, episode, sequence, shortcode, shotType='Shot')
