#!/usr/bin/env python
# -- coding: utf-8 --
# checkin checkout for process 
import os 
import sys 
import getpass 
from rf_utils import file_utils 
from datetime import datetime
from collections import OrderedDict
from rf_utils import register_sg
from rf_utils.widget import dialog
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
language = 'EN'
register_sg.clear_cache()


def init_permission(entity, key): 
	shot = register_sg.ShotElement(entity)
	output = shot.read(key)
	if not output.elemEntity: 
		output = shot.add(key)
	return output 


def check_edit_right(output): 
	key = output.nsKey
	if output.is_lock(): 
		if not output.is_owner(): 
			message = '\t    WARNING!!\n"{}" is locked by "{}". \nYou cannot continue.'.format(output.name, output.owner)
			button = dialog.CustomMessageBox.show('Warning', message, buttons=['OK'])
			return True if button.value == 'Continue' else False 
		else: 
			return True
	else: 
		message = 'Please lock "{}" to prevent other from edit your camera?'.format(key)
		button = dialog.CustomMessageBox.show('Information', message, buttons=['Yes', 'Cancel'])
		if button.value == 'Yes': 
			result = output.lock()
			if result: 
				logger.info('"{}" successfully locked by "{}"'.format(key, output.owner))
				return True 
			else: 
				logger.error('Failed to lock')
				return False
		return False


def check_export_right(output): 
	key = output.nsKey
	if output.is_lock(): 
		if not output.is_owner(): 
			message = '"{}" is locked by "{}". You cannot export.'.format(key, output.owner)
			dialog.MessageBox.warning('Warning', message)
			return False 
		return True 
	return True


def check_owner(output): 
	status = output.status 
	lock = True if status == 'lock' else False
	return output.owner, lock


def lock(output, value, workspace=''): 
	if output.is_owner() or not output.is_lock(): 
		output.lock(workspace=workspace) if value == True else output.release()
	else: 
		if output.is_lock(): 
			message = '{} is the owner. You cannot edit this camera'.format(output.owner)
			dialog.MessageBox.warning('Warning', message)
		

def checkout(entity, force=False): 
	data = read_file(entity)
	fileLock = is_file_lock(data)
	if not fileLock:  
		result = lock_file(entity, data)
		en = 'File locked by "%s"' % getpass.getuser()
		th = 'ไฟล์พร้อมทำงานและ checkout โดย "%s"' % getpass.getuser()
		output('DEBUG', en, th)
	else: 
		lockInfo = get_active_data(data)
		checkoutUser = str(lockInfo.get('checkout_by') )
		checkoutTime = str(lockInfo.get('checkout_time'))

		if force: 
			if not lockInfo.get('checkout_by') == getpass.getuser(): 
				forced_checkout_file(entity, data)
				en = '"%s" forced check-out the file.' % getpass.getuser()
				th = '"%s" ฝืน check-out ไฟล์นี้' % getpass.getuser()
				output('WARNING', en, th)

			else:
				en = 'You already checked out the file.'
				th = 'คุณ check out ไฟล์ไปแล้ว.'
				output('WARNING', en, th)
		else: 
			if lockInfo.get('checkout_by') == getpass.getuser(): 
				en = 'You already checked out the file.'
				th = 'คุณ check out ไฟล์ไปแล้ว.'
				output('WARNING', en, th)
			else: 
				en = '"%s" is working on this process. File is locked at %s' % (checkoutUser, checkoutTime)
				th = '"%s" กำลังทำงานที่ไฟล์นี้. ไฟล์ถูก lock ตั้งแต่ %s' % (checkoutUser, checkoutTime)
				output('ERROR', en, th)


def checkin(entity, force=False): 
	data = read_file(entity)
	lockInfo = get_active_data(data)
	fileLock = lockInfo.get('lock')

	if fileLock: 
		currentUser = getpass.getuser()
		checkoutUser = lockInfo.get('checkout_by')
	
		if checkoutUser == currentUser: 
			result = unlock_file(entity, data)
			en = 'File checked in by "%s" at %s' % (currentUser, lockInfo.get('checkin_time'))
			th = '"%s" check in ไฟล์ตอน %s' % (currentUser, lockInfo.get('checkin_time'))
			output('DEBUG', en, th)

		else: 
			if not force: 
				en = '"%s" is working on this file. You cannot check in this file' % checkoutUser
				th = '"%s" กำลังทำงานที่ไฟล์นี้อยู่. ไม่สามารถ checkin ไฟล์ทำงานซ้ำกันได้' % checkoutUser
				output('ERROR', en, th)
			else: 
				forced_checkin_file(entity, data)
				en = '"%s" forced check-in the file' % getpass.getuser()
				th = '"%s" ฝืน check in ไฟล์' % getpass.getuser()
				output('WARNING', en, th)
				logger.warning()

	else: 
		en = 'Please checkout before checkin'
		th = 'กรุณา checkout ก่อนจะ checkin ไฟล์'
		output('ERROR', en, th)


def file_status(entity): 
	data = read_file(entity)
	fileLock = is_file_lock(data)
	lockInfo = get_active_data(data)
	if not fileLock:  
		en = 'Last file checked in %s' % str(lockInfo.get('checkin_file'))
		th = 'ไฟล์ล่าสุดที่ checkin คือ %s' % str(lockInfo.get('checkin_file'))
		output('DEBUG', en, th)

		en = 'This process is free to checkout.'
		th = 'ไฟล์นี้ยังไม่มีใครทำงาน สามารถทำงานได้'
		output('DEBUG', en, th)
		return True

	else: 
		lock = lockInfo.get('lock')
		checkoutUser = str(lockInfo.get('checkout_by'))
		checkoutTime = str(lockInfo.get('checkout_time'))
		checkoutFile = str(lockInfo.get('checkout_file'))
		force = lockInfo.get('force')
		forcedUser = str(lockInfo.get('forced_user'))

		en = 'Checked out process "%s %s %s" on %s' % (entity.name, entity.step, entity.process, checkoutFile)
		th = 'Checked out ขั้นตอน "%s %s %s" ไฟล์ %s' % (str(entity.name), str(entity.step), str(entity.process), str(checkoutFile))
		output('DEBUG', en, th)

		if not force: 
			en = 'This file is locked by "%s" at %s' % (checkoutUser, checkoutTime)
			th = 'ไฟล์นี้ถูก lock เพราะ "%s" กำลังทำงานอยู่ ตั้งแต่ %s' % (checkoutUser, checkoutTime)
			output('INFO', en, th)
		else: 
			en = '"%s" forced checkout file from "%s" at %s' % (checkoutUser, forcedUser, checkoutTime)
			th = '"%s" ฝืน checkout ไฟล์จาก "%s" ตอน %s' % (checkoutUser, forcedUser, checkoutTime)
			output('WARNING', en, th)
		return False 


def lock_file(entity, data): 
	newData = OrderedDict()
	newData['lock'] = True 
	newData['checkout_by'] = getpass.getuser()
	newData['checkout_time'] = datetime.now().strftime('%Y-%b-%d %H:%M.%S')
	newData['checkout_file'] = str(entity.path.path())
	data[datetime.now().strftime('%Y-%b-%d %H:%M.%S')] = newData
	return write_file(entity, data)


def force_lock_file(entity, data, user): 
	newData = OrderedDict()
	newData['lock'] = True 
	newData['checkout_by'] = getpass.getuser()
	newData['checkout_time'] = datetime.now().strftime('%Y-%b-%d %H:%M.%S')
	newData['checkout_file'] = str(entity.path.path())
	newData['force'] = True 
	newData['forced_user'] = user
	data[datetime.now().strftime('%Y-%b-%d %H:%M.%S')] = newData
	return write_file(entity, data)


def unlock_file(entity, data): 
	newData = get_active_data(data)
	newData['lock'] = False 
	newData['checkin_by'] = getpass.getuser()
	newData['checkin_time'] = datetime.now().strftime('%Y-%b-%d %H:%M.%S')
	newData['checkin_file'] = str(entity.path.path())
	data[data.keys()[-1]] = newData
	return write_file(entity, data)


def forced_checkin_file(entity, data): 
	""" deprecated use forced checkout instead """ 
	newData = get_active_data(data)
	newData['lock'] = False 
	newData['forced_checkin_by'] = getpass.getuser()
	newData['forced_checkin_time'] = datetime.now().strftime('%Y-%b-%d %H:%M.%S')
	data[data.keys()[-1]] = newData
	return write_file(entity, data)


def forced_checkout_file(entity, data): 
	newData = get_active_data(data)
	newData['lock'] = False 
	newData['forced_checkout_by'] = getpass.getuser()
	newData['forced_checkout_time'] = datetime.now().strftime('%Y-%b-%d %H:%M.%S')
	data[data.keys()[-1]] = newData
	write_file(entity, data)
	forcedUser = newData['checkout_by']
	return force_lock_file(entity, data, forcedUser)


def is_file_lock(data): 
	lock = False 
	lockInfo = get_active_data(data)
	lock = lockInfo.get('lock', False)
	return lock


def get_active_data(data): 
	if data: 
		return data[data.keys()[-1]]
	return OrderedDict()

def set_data(lock, checkoutUser='', checkInUser=''): 
	now = datetime.now()
	currentTime = now.strftime('%Y-%b-%d %H:%M.%S')
	data = OrderedDict()
	data['lock'] = lock
	data['checkout_by'] = checkoutUser
	data['checkout_time'] = currentTime if checkoutUser else data.get('checkout_time')
	data['checkin_by'] = checkInUser
	data['checkin_time'] = currentTime if checkInUser else data.get('checkin_time')
	return data 


def data_file(entity): 
	workspace = entity.path.workspace().abs_path()
	name = '.lock.yml' 
	filepath = '%s/%s' % (workspace, name)
	return filepath


def read_file(entity): 
	path = data_file(entity)
	if os.path.exists(path): 
		data = file_utils.json_loader(path)
	else: 
		data = OrderedDict()
		data[datetime.now().strftime('%Y-%b-%d %H:%M.%S')] = OrderedDict()
	return data 


def write_file(entity, data): 
	path = data_file(entity)
	return file_utils.json_dumper(path, data)


def output(level, en, th): 
	if language == 'EN': 
		message = en 
	if language == 'TH': 
		message = th.decode('utf-8') 

	if level == 'INFO': 
		logger.info(message)
	if level == 'DEBUG': 
		logger.debug(message)
	if level == 'WARNING': 
		logger.warning(message)
	if level == 'ERROR': 
		logger.error(message)



""" 
Example. 
# instantiated context_info 
from rf_utils.context import context_info
entity = context_info.set()

# NORMAL CHECKOUT ***************************************

# CHECK STATUS
filelock.file_status(entity)

# rf_utils.pipeline.filelock : Last file checked in None # 
# rf_utils.pipeline.filelock : This process is free to checkout. # 

# CHECK-OUT FILE 
filelock.checkout(entity)
# rf_utils.pipeline.filelock : File locked by "chanon.v" # 

# CHECK STATUS
filelock.file_status(entity)

# rf_utils.pipeline.filelock : Checked out process "coffeeCupB model main" on $RFPROJECT/projectName/asset/work/prop/coffeeCupB/model/main/maya/coffeeCupB_mdl_main.v001.TA.ma # 
# rf_utils.pipeline.filelock : This file is locked by "chanon.v" at 2020-Jun-29 11:50.58 # 

# NORMAL CHECKIN ***************************************

# CHECK-IN FILE
filelock.checkin(entity, force=False)
# rf_utils.pipeline.filelock : File checked in by "chanon.v" at 2020-Jun-29 11:55.29 # 


# FORCED CHECKOUT ***************************************
# NORMAL CHECK-OUT
filelock.checkout(entity)
# rf_utils.pipeline.filelock : File locked by "chaiyachat.p" # 

# SOMEONE ELSE TRY TO CHECK-OUT
filelock.checkout(entity)
# Error: rf_utils.pipeline.filelock : "chaiyachat.p" is working on this process. File is locked at 2020-Jun-29 11:56.47 # 

# FORCED CHECK-OUT
filelock.checkout(entity, force=True)
# Warning: rf_utils.pipeline.filelock : "chanon.v" forced check-out the file # 

# CHECK STATUS 
filelock.file_status(entity)

# rf_utils.pipeline.filelock : Checked out process "coffeeCupB model main" on $RFPROJECT/projectName/asset/work/prop/coffeeCupB/model/main/maya/coffeeCupB_mdl_main.v001.TA.ma # 
# Warning: rf_utils.pipeline.filelock : "chanon.v" forced checkout file from "chaiyachat.p" at 2020-Jun-29 11:58.39 # 
# Result: False # 

# TRY CHECK-IN WITH DIFFERENT USER 
filelock.checkin(entity)

# Error: rf_utils.pipeline.filelock : "chanon.v" is working on this file. You cannot check in this file # 


# NORMAL CHECKIN ***************************************
# CHECK-IN WITH OWNER 
filelock.checkin(entity)

# rf_utils.pipeline.filelock : File checked in by "chanon.v" at 2020-Jun-29 12:04.59 # 

"""
