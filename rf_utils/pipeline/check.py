import os
import sys
from pprint import pprint

from rf_utils import cask
from rf_utils import file_utils

import pymel.core as pm
import maya.cmds as mc
import maya.mel as mm
from maya.api import OpenMaya as om

from nuTools import misc

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

CURVE_PARAM_TOL = 1e-07

def getMfnMesh(meshName):
    ''' Get MfnMesh from the provided meshName '''
    mSel = om.MSelectionList()
    mSel.add(meshName)

    mDag = mSel.getDagPath(0)
    mfnMesh = om.MFnMesh(mDag)
    return mfnMesh

def getMfnNurbsCurve(curveName):
    mSel = om.MSelectionList()
    mSel.add(curveName)
    mobj = mSel.getDependNode(0)
    curveFn = om.MFnNurbsCurve(mobj)
    return curveFn

class Config: 
    match = 'match'
    mismatch = 'mismatch topology'
    missing = 'missing'
    alien = 'alien'

    valid = 'valid'
    invalid = 'invalid'


def getStaticObjInABC(abc_path, shapeTypes):
    arc = cask.Archive(abc_path)
    exit = False
    objs = [arc.top]
    result = []
    while not exit:
        next_objs = []
        for o in objs:
            # need to make use of iobject because DeepDict can't maintain object order
            for i in range(o.iobject.getNumChildren()):
                c_fullname = o.iobject.getChild(i).getFullName()
                c = arc.top.children[c_fullname]

                if c.type() == 'Xform':
                    trchildren = c.children.values()
                    shapes = []
                    for tc in trchildren:
                        if tc.type() in shapeTypes:
                            shapes.append(tc)
                    if shapes:
                        if not c.is_animated() and not any([s.is_animated() for s in shapes]):
                            result.append(c)
                    next_objs.append(c)
        if next_objs:
            objs = next_objs
        else:
            exit = True

    return result

class CheckHierarchy(object):
    ''' 
    checker = CheckHierarchy('Geo_Grp', 'P:/SevenChickMovie/asset/publ/char/arthur/model/main/hero/output/arthur_mdl_main_md.hero.abc')
    result = checker.check() '''
    
    def __init__(self, checkGrp, inputData, addNamespaceToAbc=True, includeShape=True):
        self.addNamespaceToAbc = addNamespaceToAbc
        self.includeShape = includeShape
        self.checkGrp = pm.PyNode(checkGrp)
        self.checkGrpPath = '|'.join(self.checkGrp.longName().split('|')[:-1])[1:]
        self.abcRootPath = ''
        self.constraints = []
        
        self.ns = self.checkGrp.namespace()
        self.inputData = inputData
        self.arc = None
        self.result = {Config.match:[], Config.mismatch:[], Config.missing:[], Config.alien:[]}

    def check(self, checkTopology=True):
        ''' The main check function '''
        self.arc = cask.Archive(self.inputData)
        self.result = {Config.match:[], Config.mismatch:[], Config.missing:[], Config.alien:[]}

        # get current objects
        curr_names = self.getCurrentObjects()

        # read items from abc
        abc_names, msg = self.readABC()
        if not abc_names:
            raise Exception(msg)
            
        if self.addNamespaceToAbc:
            new_names = []
            for path in abc_names:
                new_path = misc.addDagPathNamespace(path, self.ns)
                new_names.append(new_path)
            abc_names = new_names

        # check hierarchy first
        # if abc_names == curr_names:
        #     self.result[Config.match] = abc_names
        abc_set = set(abc_names)
        curr_set = set(curr_names)

        self.result[Config.missing] = list(abc_set - curr_set)  # in ABC but not in current scene
        self.result[Config.alien] = list(curr_set - abc_set)  # in current scene but not in ABC
        self.result[Config.match] = list(abc_set & curr_set)  # intersection of both sets

        # check topology
        if checkTopology:
            self.checkTopologyFromABC(self.result[Config.match])

        # need to close the cask archive
        self.arc.close()

        self.optimizeResult()
        return self.result

    def optimizeResult(self):
        new_result = {Config.match:[], Config.mismatch:[], Config.missing:[], Config.alien:[]}
        for rType, names in self.result.items():
            if rType == Config.missing:
                new_result[Config.missing] = names
                continue
            new_ls = []
            for name in names:
                if self.checkGrpPath:
                    fp = '{}|{}'.format(self.checkGrpPath, name)
                else:
                    fp = name
                pyNode = pm.PyNode(fp)
                sn = str(pyNode.shortName())
                new_ls.append(sn)
            new_result[rType] = new_ls
        self.result = new_result

    def getCurrentObjects(self):
        ''' Get transform under checkGrp as full paths '''
        topGrp = self.checkGrp
        topGrpName = str(topGrp.nodeName())
        result = [topGrpName]
        objs = [topGrp]
        exit = False
        constraints = ('pointConstraint', 'orientConstraint', 'parentConstraint', 'aimConstraint', 'scaleConstraint')
        self.constraints = []
        # Can't use listRelatives(ad=True) because we need the breadth-first object order
        while not exit:
            next_objs = []
            for o in objs:
                children = o.getChildren(ni=True)
                for c in children:
                    nodeType = c.nodeType()
                    if nodeType in constraints:
                        self.constraints.append(c.shortName())
                        continue

                    if not self.includeShape and nodeType != 'transform':
                        continue

                    childName = str(c.longName())
                    childName = '{}|{}'.format(topGrpName, childName.split('|{}|'.format(topGrpName))[-1])
                    result.append(childName)
                    next_objs.append(c)
            if next_objs:
                objs = next_objs
            else:
                exit = True

        # print self.constraints
        return result

    def checkTopologyFromABC(self, abc_names):
        ''' Performs check topology on each pair of objects ''' 
        match, mismatch = [], []
        crvTypMap = {1:1, 3:0, 2:2, 5:2, 7:2}  # {mayaType: abcType}
        crvFormMap = {1:0, 2:0, 3:1}
        for name in abc_names:
            if self.checkGrpPath:
                fp = '{}|{}'.format(self.checkGrpPath, name)
            else:
                fp = name

            shape = pm.PyNode(fp)
            if not isinstance(shape, (pm.nt.Mesh, pm.nt.NurbsCurve)):
                continue

            # get the source topology from ABC
            if self.addNamespaceToAbc:
                name = misc.removeDagPathNamespace(name)
            abc_path = '{}/{}'.format(self.abcRootPath, name)
            abc_path = str(abc_path.replace('|', '/'))

            shp = self.arc.top.children[abc_path]

            shpType = shp.type()
            geom = shp.properties['.geom']
            if shpType == 'PolyMesh':
                src_vtxCounts = list(geom.properties['.faceCounts'].get_value(0))
                src_vtxLists = list(geom.properties['.faceIndices'].get_value(0))

                # get current topology
                curr_mfn = getMfnMesh(fp)
                curr_vtxCounts, curr_vtxLists = curr_mfn.getVertices()

                # the num face. If they aren't matching, just continue
                if len(src_vtxCounts) != len(curr_vtxCounts):
                    mismatch.append(name)
                    continue

                # next, check the vertexnumber on each face
                i = 0
                for src_vc, curr_vc in zip(src_vtxCounts, curr_vtxCounts):
                    # check vertex count on each face
                    if src_vc != curr_vc:
                        mismatch.append(name)
                        break

                    # check vertex ID on each face
                    src_vtx_range = list(src_vtxLists[i:i+src_vc][::-1])  # need to invert the list for some ABC reason?
                    curr_vtx_range = list(curr_vtxLists[i:i+src_vc])
                    if src_vtx_range != curr_vtx_range:

                        mismatch.append(name)
                        break
                    i += src_vc
                else:
                    match.append(name)

            # if its a NurbsCurve
            elif shpType == 'Curve':
                curveFn = getMfnNurbsCurve(fp)
                # print fp
                # num vertices
                abc_numCVs = geom.properties['nVertices'].get_value()[0]
                if abc_numCVs != curveFn.numCVs:
                    # print abc_numCVs, curveFn.numCVs
                    mismatch.append(name)
                    continue

                # get knots
                abc_knots = geom.properties['.knots'].get_value()
                abc_knots = abc_knots[1:-1]  # omit the first and last knot due to Maya knots format
                knots_array = curveFn.knots()
                knots = []

                for ak, k in zip(abc_knots, knots):
                    if abs(ak - k) > CURVE_PARAM_TOL:
                        mismatch.append(name)
                        break

                # basisType
                basisType = geom.properties['curveBasisAndType'].get_value()
                abc_curveType = basisType[0]
                abc_periodicity = basisType[1]
                degree = curveFn.degree
                form = curveFn.form
                
                if crvTypMap[degree] != abc_curveType or crvFormMap[form] != abc_periodicity:
                    # print 'topo'
                    # print crvTypMap[degree]
                    # print abc_curveType
                    # print ''
                    # print crvFormMap[form]
                    # print abc_periodicity
                    mismatch.append(name)
                    continue

        # self.result[Config.match] = match
        for obj in mismatch:
            self.result[Config.match].remove(obj)
        self.result[Config.mismatch] = mismatch

    def readABC(self):
        ''' Gead the geo ABC using cask, returns a list of full path strings '''
        exit = False
        checkGrpName = str(self.checkGrp.nodeName())

        # abcGrpPath = str(checkGrpName.replace('|', '/'))
        if self.addNamespaceToAbc:
            checkGrpName = checkGrpName.split(':')[-1]

        objs = cask.find(self.arc.top, checkGrpName, ['Xform'])

        msg = ''
        if not objs:
            msg = 'Cannot find in ABC: {}'.format(checkGrpName)
            return False, msg
        elif len(objs) > 1:
            msg = 'More than 1 object match name in ABC: {}'.format(checkGrpName)
            return False, msg

        rootPath = objs[0].path()
        self.abcRootPath = rootPath.split(checkGrpName)[0][1:-1]
        result = [str(checkGrpName)]
        # print self.constraints
        while not exit:
            next_objs = []
            for o in objs:
                # need to make use of iobject because DeepDict can't maintain object order
                for i in range(o.iobject.getNumChildren()):
                    c_fullname = o.iobject.getChild(i).getFullName()
                    c = self.arc.top.children[c_fullname]
                    abc_path = c.path()
                    path = abc_path.replace('/', '|')
                    typ = c.type()
                    if typ == 'Xform':
                        skip = False
                        for con in self.constraints:
                            if path.endswith(con):
                                skip = True
                                break
                        if skip:
                            break
                        next_objs.append(c)
                    elif not self.includeShape:
                        continue

                    if path.startswith('|'):
                        path = path[1:]
                    path = '{}{}'.format(checkGrpName, path.split(checkGrpName)[-1])
                    result.append(path)
            if next_objs:
                objs = next_objs
            else:
                exit = True
        return result, msg

class CheckGeo(object):
    ''' 
    checker = CheckGeo('Geo_Grp', 'P:/SevenChickMovie/asset/publ/char/arthur/model/main/hero/output/arthur_mdl_main_md.hero.abc')
    result = checker.check() '''
    
    def __init__(self, checkGrp, inputData, includeShape=True):
        self.checkGrp = pm.PyNode(checkGrp)
        self.inputData = inputData
        self.arc = None
        self.includeShape = includeShape
        self.abc_paths = []
        self.result = {Config.match:[], Config.mismatch:[], Config.missing:[], Config.alien:[]}

    def check(self, checkTopology=True):
        ''' The main check function '''
        self.arc = cask.Archive(self.inputData)

         # reset the variables
        self.abc_paths = []
        self.result = {Config.match:[], Config.mismatch:[], Config.missing:[], Config.alien:[]}

        # read items from abc
        abc_names = self.readABC()  # return geo (full abc path)
        # get current objects
        curr_objs = self.getCurrentObjects()  # return geo (long name)

        # print curr_objs
        # print abc_names
        # print curr_objs
        # check hierarchy first
        for abc in abc_names:
            remove_indices = []
            abc_no_ns = '/'.join([n.split(':')[-1] for n in abc.split('/')])
            for i, curr in enumerate(curr_objs):
                curr_sn = curr.split('|')[-1]
                curr_sn = curr_sn.split(':')[-1]
                if abc_no_ns.endswith(curr_sn):  # found it!
                    self.abc_paths.append(abc)
                    self.result[Config.match].append(curr)
                    remove_indices.append(i)
                    break
            else:  # if the loop ends without breaking meaning this geo is missing
                self.result[Config.missing].append(abc)
            if remove_indices:
                for i in remove_indices:
                    curr_objs.pop(i)
        # what's left in curr_objs are alien geos
        self.result[Config.alien] = curr_objs
        
        # check topology
        if checkTopology:
            self.checkTopologyFromABC(self.abc_paths, self.result[Config.match])

        # need to close the cask archive
        self.arc.close()

        return self.result

    def getCurrentObjects(self):
        ''' Get transform under checkGrp as full paths '''
        result = []
        objs = [self.checkGrp]
        exit = False
        constraints = ('pointConstraint', 'orientConstraint', 'parentConstraint', 'aimConstraint', 'scaleConstraint')

        # Can't use listRelatives(ad=True) because we need the breadth-first object order
        while not exit:
            next_objs = []
            for o in objs:
                children = o.getChildren(type='transform')
                for c in children:
                    if c.nodeType() in constraints:
                        continue
                    shp = c.getShape(ni=True)
                    
                    if shp:  # if the transform has shape
                        sln = str(shp.longName())
                        result.append('|'.join(sln.split('|')[:-1]))  # take the transform long name
                        if self.includeShape:
                            result.append(sln)  # take the shape
                    else:
                        next_objs.append(c)
            if next_objs:
                objs = next_objs
            else:
                exit = True

        return result

    def checkTopologyFromABC(self, abc_names, curr_names):
        ''' Performs check topology on each pair of objects ''' 
        match, mismatch = [], []
        match_abc_names = []

        for src, curr in zip(abc_names, curr_names):
            # skip if it's not a poly
            # shape = pm.PyNode(curr).getShape(ni=True)
            curr_node = pm.PyNode(curr)
            if curr_node.nodeType() not in ('mesh'):
                continue

            # skip checking for Alembic geo
            inMeshConnections = curr_node.inMesh.inputs()
            if any([inc for inc in inMeshConnections if inc.nodeType()=='AlembicNode']):
                match.append(curr)
                continue

            # shp = tr.children.values()[0]
            shp = self.arc.top.children[src.replace('|', '/')]
            geom = shp.properties['.geom']
            src_vtxCounts = list(geom.properties['.faceCounts'].get_value(0))
            src_vtxLists = list(geom.properties['.faceIndices'].get_value(0))

            # get current topology
            curr_mfn = getMfnMesh(curr)
            curr_vtxCounts, curr_vtxLists = curr_mfn.getVertices()

            # the num face. If they aren't matching, just continue
            if len(src_vtxCounts) != len(curr_vtxCounts):
                mismatch.append((src, curr))
                continue

            # next, check the vertexnumber on each face
            i = 0
            for src_vc, curr_vc in zip(src_vtxCounts, curr_vtxCounts):
                # check vertex count on each face
                if src_vc != curr_vc:
                    mismatch.append((src, curr))
                    break

                # check vertex ID on each face
                src_vtx_range = list(src_vtxLists[i:(i+src_vc)][::-1])  # need to invert the list for some ABC reason?
                curr_vtx_range = list(curr_vtxLists[i:(i+src_vc)])
                if src_vtx_range != curr_vtx_range:
                    mismatch.append((src, curr))
                    break  # test
                i += src_vc
            else:
                match.append(curr)

        # remove mismatch shapes from match
        for abc, obj in mismatch:
            if obj in self.result[Config.match]:
                self.result[Config.match].remove(obj)
                self.result[Config.mismatch].append(obj)
            if abc in self.abc_paths:  # if mismatch, remove from abc_paths too 
                self.abc_paths.remove(abc)

    def readABC(self):
        ''' Read the geo ABC using cask, returns a list of full path strings '''
        exit = False
        objs = [self.arc.top]
        result = []
        while not exit:
            next_objs = []
            for o in objs:
                # need to make use of iobject because DeepDict can't maintain object order
                for i in range(o.iobject.getNumChildren()):
                    c_fullname = o.iobject.getChild(i).getFullName()
                    c = self.arc.top.children[c_fullname]

                    if c.type() == 'PolyMesh':
                        path_split = c.path().split('/')
                        path = '|'.join(path_split[:-1])  # take the parent transform path
                        result.append(path)
                        if self.includeShape:
                            path = '|'.join(path_split)  # take the parent transform path
                            result.append(path)
                    else:
                        next_objs.append(c)
            if next_objs:
                objs = next_objs
            else:
                exit = True
        return result

class CustomCheck(object):
    ''' 
    checker = CheckGeo('Geo_Grp', 'P:/SevenChickMovie/asset/publ/char/arthur/model/main/hero/output/arthur_mdl_main_md.hero.abc')
    result = checker.check() '''
    
    def __init__(self, checkGrp, abcGrp, inputData):
        self.checkGrp = pm.PyNode(checkGrp)
        self.abcGrp = abcGrp
        self.inputData = inputData
        self.arc = None
        self.abc_paths = []
        self.result = {Config.match:[], Config.mismatch:[], Config.missing:[], Config.alien:[]}

    def check(self, checkTopology=True):
        ''' The main check function '''
        self.arc = cask.Archive(self.inputData)

         # reset the variables
        self.abc_paths = []
        self.result = {Config.match:[], Config.mismatch:[], Config.missing:[], Config.alien:[]}

        # read items from abc
        abc_names = self.readABC()  # return geo (full abc path)
        # get current objects
        curr_objs = self.getCurrentObjects()  # return geo (long name)

        # check hierarchy first
        for abc in abc_names:
            remove_indices = []
            abc_no_ns = '/'.join([n.split(':')[-1] for n in abc.split('/')])
            for i, curr in enumerate(curr_objs):
                curr_sn = curr.split('|')[-1]
                curr_sn = curr_sn.split(':')[-1]
                if abc_no_ns.endswith(curr_sn):  # found it!
                    self.abc_paths.append(abc)
                    self.result[Config.match].append(curr)
                    remove_indices.append(i)
                    break
            else:  # if the loop ends without breaking meaning this geo is missing
                self.result[Config.missing].append(abc)
            if remove_indices:
                for i in remove_indices:
                    curr_objs.pop(i)
        # what's left in curr_objs are alien geos
        self.result[Config.alien] = curr_objs
        
        # check topology
        if checkTopology:
            self.checkTopologyFromABC(self.result[Config.match])

        # need to close the cask archive
        self.arc.close()
        pprint(self.result)
        return self.result

    def getCurrentObjects(self):
        ''' Get transform under checkGrp as full paths '''
        result = []
        objs = [self.checkGrp]
        exit = False
        constraints = ('pointConstraint', 'orientConstraint', 'parentConstraint', 'aimConstraint', 'scaleConstraint')

        # Can't use listRelatives(ad=True) because we need the breadth-first object order
        while not exit:
            next_objs = []
            for o in objs:
                children = o.getChildren(type='transform')
                for c in children:
                    if c.nodeType() in constraints:
                        continue
                    shp = c.getShape(ni=True)
                    if shp:  # if the transform has shape
                        result.append(str(c.longName()))  # take the transform long name
                    else:
                        next_objs.append(c)
            if next_objs:
                objs = next_objs
            else:
                exit = True

        return result

    def checkTopologyFromABC(self, abc_names):

        ''' Performs check topology on each pair of objects ''' 
        match, mismatch = [], []
        crvTypMap = {1:1, 3:0, 2:2, 5:2, 7:2}  # {mayaType: abcType}
        crvFormMap = {1:0, 2:0, 3:1}
        for name in abc_names:
            fp = name
            tr = pm.PyNode(fp)
            shape = tr.getShape(ni=True)
            if not isinstance(shape, (pm.nt.Mesh, pm.nt.NurbsCurve)):
                continue

            shp = cask.find(self.arc.top, shape.nodeName())[0]
            shpType = shp.type()
            geom = shp.properties['.geom']
            if shpType == 'PolyMesh':
                src_vtxCounts = list(geom.properties['.faceCounts'].get_value(0))
                src_vtxLists = list(geom.properties['.faceIndices'].get_value(0))

                # get current topology
                curr_mfn = getMfnMesh(fp)
                curr_vtxCounts, curr_vtxLists = curr_mfn.getVertices()

                # the num face. If they aren't matching, just continue
                if len(src_vtxCounts) != len(curr_vtxCounts):
                    mismatch.append(name)
                    continue

                # next, check the vertexnumber on each face
                i = 0
                for src_vc, curr_vc in zip(src_vtxCounts, curr_vtxCounts):
                    # check vertex count on each face
                    if src_vc != curr_vc:
                        mismatch.append(name)
                        break

                    # check vertex ID on each face
                    src_vtx_range = list(src_vtxLists[i:src_vc][::-1])  # need to invert the list for some ABC reason?
                    curr_vtx_range = list(curr_vtxLists[i:src_vc])
                    if src_vtx_range != curr_vtx_range:
                        mismatch.append(name)
                        break
                    i += src_vc
                else:
                    match.append(name)

            # if its a NurbsCurve
            elif shpType == 'Curve':
                curveFn = getMfnNurbsCurve(shape.longName())

                # num vertices
                abc_numCVs = geom.properties['nVertices'].get_value()[0]
                if abc_numCVs != curveFn.numCVs:
                    mismatch.append(name)
                    continue

                # get knots
                abc_knots = geom.properties['.knots'].get_value()
                abc_knots = abc_knots[1:-1]  # omit the first and last knot due to Maya knots format
                knots_array = curveFn.knots()
                knots = []

                for ak, k in zip(abc_knots, knots):
                    if abs(ak - k) > CURVE_PARAM_TOL:
                        mismatch.append(name)
                        break

                # basisType
                basisType = geom.properties['curveBasisAndType'].get_value()
                abc_curveType = basisType[0]
                abc_periodicity = basisType[1]
                degree = curveFn.degree
                form = curveFn.form
                
                if crvTypMap[degree] != abc_curveType or crvFormMap[form] != abc_periodicity:
                    mismatch.append(name)
                    continue

        # self.result[Config.match] = match
        for obj in mismatch:
            self.result[Config.match].remove(obj)
        self.result[Config.mismatch] = mismatch

    def readABC(self):
        ''' Read the geo ABC using cask, returns a list of full path strings '''
        exit = False

        objs = [cask.find(self.arc.top, self.abcGrp, ['Xform'])[0]]

        result = []
        while not exit:
            next_objs = []
            for o in objs:
                # need to make use of iobject because DeepDict can't maintain object order
                for i in range(o.iobject.getNumChildren()):
                    c_fullname = o.iobject.getChild(i).getFullName()
                    c = self.arc.top.children[c_fullname]

                    # only get PolyMesh type
                    if c.type() in ('PolyMesh', 'Curve'):
                        path = '|'.join(c.path().split('/')[:-1])  # take the parent transform path
                        result.append(path)
                    else:
                        next_objs.append(c)
            if next_objs:
                objs = next_objs
            else:
                exit = True
        return result


# ------------------------------------------------------------------
# ----- OLD FUNCTIONS
def hierarchy(checkGrp, inputData, order=False): 
    # simplify checkGrp 
    # bodyRig:Geo_Grp 
    # remove namespace and upper parent to compare
    data = mc.ls(checkGrp, dag=True, l=True)
    # remove intermediate shape 
    data = [a for a in data if mc.objExists('{}.intermediateObject'.format(a)) and not mc.getAttr('{}.intermediateObject'.format(a))]
    # remove parent long name
    # |bodyRig:Rig_Grp|bodyRig:Geo_Grp' -> bodyRig:Geo_Grp
    localData = ['{}{}'.format(checkGrp, a.split(checkGrp)[-1]) for a in data]
    
    # detect namespace 
    namespace = checkGrp.split(':')[0] if ':' in checkGrp else ''
    # remove namespace 
    localData = [a.replace('{}:'.format(namespace), '') for a in localData]
    compareData = file_utils.ymlLoader(inputData)
    # remove '|Geo_Grp' -> Geo_Grp
    compareData = [a[1:] for a in compareData]
    unmatchedGeo = []
    matchedGeo = []
    
    if not order: 
        for row, each in enumerate(localData): 
            if not each in compareData: 
                unmatchedGeo.append(data[row])
                logger.debug('not matched geo {}'.format(data[row]))
            else: 
                matchedGeo.append(data[row])

    # if unmatchedGeo: 
    #   mc.select(unmatchedGeo)

    return {Config.match: matchedGeo, 'unmatched': unmatchedGeo}


def pipeline_assets(): 
    def get_references():         
        refs = mc.file(q=True, r=True)
        return refs 

    from rf_utils.context import context_info
    entity = context_info.ContextPathInfo()
    share_projects = entity.projectInfo.asset.share_projects()
    assetPaths = get_references()
    validAssets = []
    invalidAssets = []

    for assetPath in assetPaths: 
        if entity.project in assetPath: 
            validAssets.append(assetPath)  
        else: 
            if any(a in assetPath for a in share_projects): 
                validAssets.append(assetPath)
            else: 
                invalidAssets.append(assetPath)

    return {Config.valid: validAssets, Config.invalid: invalidAssets}






