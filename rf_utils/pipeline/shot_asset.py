import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import ascii_utils
from rf_utils import register_sg 
from rf_utils.context import context_info

class Config: 
    char = {'SevenChickMovie': 'char', 'default': 'char'}
    mtrKey = 'Mtr'

def read_asset_elements(entity): 
    data = dict()
    reg = register_sg.ShotElement(entity)
    # keys = reg.list_keys()
    dataList = reg.list_elements()
    # lockKeys = reg.list_lock()
    for taskEntity in dataList: 
        lock = taskEntity['sg_lockdown']
        key = taskEntity['entity']['name']
        dataType = taskEntity['entity.CustomEntity09.sg_type']
        data[key] = {'namespace': key, 'lock': lock, 'dataType': dataType}

    return data

def read_ma_asset_namespace(filePath): 
    data = dict()
    if os.path.exists(filePath): 
        maFile = ascii_utils.MayaAscii(filePath)
        maFile.read()
        refList = maFile.listReferences()

        for ref in refList: 
            ns = ref['namespace']
            path = ref['path']
            asset = context_info.ContextPathInfo(path=path)

            if Config.mtrKey in ns: 
                # skip Mtr key
                continue 
            # assume Tech for char type 

            if asset.type == Config.char.get(asset.project, Config.char.get('default')): 
                techNs = '%s_Tech' % ns
                data[ns] = {'namespace': ns, 'lock': None, 'dataType': 'abc_cache'}
                data[techNs] = {'namespace': techNs, 'lock': None, 'dataType': 'abc_cache'}

            else: 
                data[ns] = {'namespace': ns, 'lock': None, 'dataType': None}
            
    return data

def read_shot_assets(entity=None): 
    from rf_app.publish.scene.export import alembic_cache 
    entity = context_info.ContextPathInfo() if not entity else entity
    data = alembic_cache.get_asset_data(entity)
    sgData = read_asset_elements(entity)
    shotData = dict()

    for ns, cacheData in data.iteritems(): 
        lock = False 
        if ns in sgData.keys(): 
            lock = sgData[ns].get('lock')

        shotData[ns] = {'namespace': ns, 'lock': lock, 'dataType': 'abc_cache'}

    return shotData
