import maya.cmds as mc 

def anim_export(func, *args, **kwargs):
    """ load plugins """
    plugin = 'animImportExport.mll'
    load_plugin(plugin)
    return func


def load_plugin(plugin): 
    if not mc.pluginInfo(plugin, q=True, l=True):
        mc.loadPlugin(plugin, qt=True)