import sys
import os
CORE_PATH = '{}/core'.format(os.environ['RFSCRIPT'])
if not CORE_PATH in sys.path:
    sys.path.append(CORE_PATH)
import shutil
import argparse
import tempfile
from datetime import datetime
from glob import glob
from collections import OrderedDict

import rf_config as config
from rf_utils.sg import sg_os
reload(sg_os)
from rf_utils.context import context_info
from rf_utils import project_info
from rf_utils import register_entity
from rf_utils import file_utils

def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', dest='project', type=str, help='The project name', required=True)
    parser.add_argument('-n', dest='outsource_name', type=str, help='Name of the outsource party', required=True)
    parser.add_argument('-z', dest='zipfile', type=bool, help='To archive folder to zip', default=False)
    parser.add_argument('-d', dest='dest_path', type=str, help='Destination path', default=None)
    parser.add_argument('-i', dest='asset_sync_int', type=int, help='Time interval to consider asset update', default=None)
    return parser

def collect_files(asset_list):
    results = OrderedDict()
    for asset in asset_list: 
        paths = []
        entity = asset['context']
        hero_dir = entity.path.hero().abs_path()

        # get rig file
        e1 = entity.copy()
        for res in ('lo', 'md'):
            e1.context.update(res=res, look='main')
            
            rig_filename = e1.output_name(outputKey='rig', hero=True)
            rig_path = '{}/{}'.format(hero_dir, rig_filename)
            if os.path.exists(rig_path):
                paths.append(rig_path)
            rig_mb_path = rig_path.replace('.ma', '.mb')
            if os.path.exists(rig_mb_path):
                paths.append(rig_mb_path)

        # mtr and textures
        e2 = entity.copy()
        reg = register_entity.Register(e2)
        try:
            looks = reg.get.list_look(step='texture')
        except AttributeError:
            looks = ['main']
        for look in looks:
            e2.context.update(res='md', look=look)
            material_filename = e2.output_name(outputKey='materialRig', hero=True)
            material_path = '{}/{}'.format(hero_dir, material_filename)
            if os.path.exists(material_path):
                paths.append(material_path)

            texture_dir = e2.path.preview_texture().abs_path()
            texture_filenames = os.listdir(texture_dir)
            for tex in texture_filenames:
                texture_path = '{}/{}'.format(texture_dir, tex)
                paths.append(texture_path)

        if paths:
            # collect results
            latest_version = asset['entity']['versions'][-1]
            results['{}/{}'.format(entity.type, entity.name)] = {'paths':paths, 
                                                                'created_at': latest_version['created_at'], 
                                                                'description': latest_version['description']}

    return results

def convert_to_unc_paths(path):
    # convert to UNC
    obj = context_info.RootPath(path)
    converted_path = obj.unc_path().replace('/', '\\')
    return converted_path

def get_update_list(project, outsource_name, asset_sync_int, asset_task_target):
    # find what asset needs to be updated
    asset_list = sg_os.list_sync_assets(project=project, 
                                    outsource_name=outsource_name, 
                                    asset_sync_int=asset_sync_int, 
                                    asset_task_target=asset_task_target)
    # collect asset files
    update_data = collect_files(asset_list=asset_list)

    # get updated cameras
    cam_updates = sg_os.list_cameras(project=project, outsource_name=outsource_name, days_old=asset_sync_int)
    update_data.update(cam_updates)
    return update_data

def main(project, outsource_name, zipfile=False, dest_path=None, asset_sync_int=None):
    '''
    Find newly updated assets and sent to outsorce
    '''
    if not outsource_name in config.Outsource.data:
        print('No outsorce of this name found in config:{}'.format(outsource_name))
        return
    outsource_config = config.Outsource.data[outsource_name]
    if not project in outsource_config:
        print('No project of this name found in config:{}'.format(project))
        return

    # get data from config
    proj_config = outsource_config[project]
    # use config value if not set in arg
    if not dest_path:
        dest_path = proj_config['dest_path']
    if not os.path.exists(dest_path):
        print('Folder created: {}'.format(dest_path))
        os.makedirs(dest_path)

    output_path = '{}/{}'.format(dest_path, datetime.now().date().strftime('%y%m%d'))
    temp_dir = ''
    if zipfile:
        temp_dir = tempfile.mkdtemp().replace('\\', '/')
        copy_path = '{}/{}'.format(temp_dir, datetime.now().date().strftime('%y%m%d'))
    else:
        copy_path = output_path
    
    if asset_sync_int == None:
        asset_sync_int = proj_config['asset_sync_int']
    asset_task_target = proj_config['asset_task_target']  # from config = [rig, rigUv, texture]

    # get update list
    update_data = get_update_list(project, outsource_name, asset_sync_int, asset_task_target)
    if not update_data:
        print('No asset needed to be updated.')
        return

    for assetname, data in update_data.iteritems():
        file_paths = data['paths']
        for path in file_paths:
            drive, p = os.path.splitdrive(path)
            destination = '{}{}'.format(copy_path, p)
            dest_dir = os.path.dirname(destination)
            print('Destination directory: {}'.format(dest_dir))
            if not os.path.exists(dest_dir):
                print('Folder created: {}'.format(dest_dir))
                os.makedirs(dest_dir)

            print('Copying: {} --> {}'.format(path, destination))
            shutil.copyfile(convert_to_unc_paths(path=path), convert_to_unc_paths(path=destination))

    print('Copy success.')

    # generate log
    log_path = generate_update_log(name=project, log_dir=copy_path, update_data=update_data)
    print('Log generated:{}'.format(log_path))

    # make zip
    if zipfile:
        print('Creating archive...')
        zip_path = output_path + '.zip'
        file_utils.zip_dir(sources=[copy_path], destination=zip_path, password=proj_config['password'])
        print('Archived file: {}'.format(zip_path))

        print('Removing temp...')
        if os.path.exists(temp_dir):
            shutil.rmtree(temp_dir)
        print('Temp removed: {}'.format(temp_dir))
        return zip_path
    else:
        return log_path

def generate_update_log(name, log_dir, update_data):
    log_path = '{}/{}_update_log.txt'.format(log_dir, name)
    if not os.path.exists(log_path):
        f = open(log_path, 'w')
        f.close()
    with open(log_path, 'r+') as file:
        # write title
        content = file.read()
        lines = ['========== Update: {} =========='.format(datetime.now())]
        i = 1
        for assetname, data in update_data.iteritems():
            created_at = data.get('created_at')
            description = data.get('description')
            lines.append('{}. {}'.format(i, assetname))
            lines.append('Updated at: {}'.format(created_at))
            lines.append('Comment: {}'.format(description))
            i += 1
        line_str = '\n'.join(lines)
        file.seek(0, 0)
        file.write(line_str + '\n\n' + content)
    return log_path

def set_project_env(project):
    # --- set env 
    envData = project_info.ProjectInfo(project).env()
    for key, path in envData.items():
        os.environ[key] = path

if __name__ == '__main__':
    print('Checking new assets...')
    parser = setup_parser('Outsource asset update')
    params = parser.parse_args()

    set_project_env(project=params.project)
    main(project=params.project, 
        outsource_name=params.outsource_name,
        zipfile=params.zipfile,
        dest_path=params.dest_path,
        asset_sync_int=params.asset_sync_int)

'''
# just copy file to dir 
python %RFSCRIPT%/core/rf_utils/pipeline/anim_outsource/asset_update.py -p "Hanuman" -n "tuxido_cat"

# create zip with password from config
python %RFSCRIPT%/core/rf_utils/pipeline/anim_outsource/asset_update.py -p "Hanuman" -n "tuxido_cat" -z on

'''