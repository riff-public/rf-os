import sys
import os
CORE_PATH = '{}/core'.format(os.environ['RFSCRIPT'])
if not CORE_PATH in sys.path:
    sys.path.append(CORE_PATH)
from pprint import pprint
from datetime import datetime

import rf_config as config
import asset_update

def ask_user_choices(question, choices):
    response = ''
    question_str = question + '\n  '
    question_str += '\n  '.join(['{}.{}'.format((i+1), choices[i]) for i in range(len(choices))])
    while True:
        print(question_str)
        response = input('Your answer: ')
        if isinstance(response, int) and response >= 0 and response <= len(choices):
            return choices[response-1]

def ask_user_input(question, input_type):
    response = None
    while type(response) != input_type:
        response = input(question)
    return response

def main(): 
    print('\n===== This will check for updated asset for an outsource in a given time frame =====')
    print('INSTRUCTION: Please input the number corresponding to your answer.')
    config_data = config.Outsource.data

    # project
    all_projs = []
    for outsource_name in config_data:
        all_projs.extend(config_data[outsource_name].keys())
    all_projs = list(set(all_projs))
    project = ask_user_choices(question='\n- Which project?', choices=all_projs)
    
    # outsource name
    outsource_name = ask_user_choices(question='\n- Outsource company/person name?', choices=config_data.keys())

    # asset sync int
    asset_sync_int = ask_user_input(question='\n- How many days to look back in time?: ', input_type=int)

    # run query
    proj_config = config_data[outsource_name][project]
    asset_update.set_project_env(project=project)
    asset_task_target = proj_config['asset_task_target']
    update_data = asset_update.get_update_list(project, outsource_name, asset_sync_int, asset_task_target)
    if update_data:
        print('===== Result at {} ====='.format(datetime.now()))
        for assetname, data in update_data.iteritems():
            print('  {} {}'.format(data['created_at'], assetname))

        # copy
        copy = ask_user_choices(question='\n- Would you like to copy these assets to new location?', choices=['Yes', 'No'])
        if copy == 'Yes':
            # zip
            zipfile = ask_user_choices(question='\n- Zip the directory?', choices=['Yes', 'No'])
            zipfile_dict = {'Yes': 1, 'No': 0}

            # copy dir
            exit = False
            while not exit:
                dest_path = str(raw_input('\n- Please enter directory to copy to: '))
                if os.path.isdir(dest_path):
                    exit = True

            # do the call
            log_path = asset_update.main(project=project, outsource_name=outsource_name, zipfile=zipfile_dict[zipfile], dest_path=dest_path, asset_sync_int=asset_sync_int)
            print('Output: {}'.format(log_path))
        else:
            # log
            log = ask_user_choices(question='\n- Would you like see log file?', choices=['Yes', 'No'])
            if log == 'Yes':
                exit = False
                while not exit:
                    dest_path = str(raw_input('\n- Please enter directory for log file: '))
                    if os.path.isdir(dest_path):
                        exit = True
                log_path = asset_update.generate_update_log(name=project, log_dir=dest_path, update_data=update_data)
                print('Output: {}'.format(log_path))
    else:
        print('No asset needed to be updated.')
    print('Finished.')

main()
