from PySide2.QtGui import *
from PySide2.QtWidgets import *
from PySide2.QtCore import *

import time
from rf_utils import thread_pool
reload(thread_pool)

class MainWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.counter = 0

        layout = QVBoxLayout()
    
        self.l = QLabel("Start")
        b = QPushButton("DANGER!")
        b.pressed.connect(self.oh_no)

        c = QPushButton("?")
        c.pressed.connect(self.change_message)

        layout.addWidget(self.l)
        layout.addWidget(b)

        layout.addWidget(c)

        w = QWidget()
        w.setLayout(layout)

        self.setCentralWidget(w)

        self.show()

        self.threadpool = QThreadPool()
        self.timer = QTimer()
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.recurring_timer)
        # self.timer.start()
    
    def change_message(self):
        self.message = "OH NO"
        self.interupt = True
        print 'interupt'

    def run_this(self, **kwargs): 
        self.interupt = False
        for i in range(10): 
            if not self.interupt: 
                print i
                time.sleep(1)

        print 'Finished loop' 
        # self.threadpool.waitForDone()


    
    def oh_no(self):
        worker = thread_pool.Worker(self.run_this)
        self.threadpool.start(worker)
        return 
        self.message = "Pressed"        
        
        for n in range(100):
            time.sleep(0.1)
            self.l.setText(str(n))
            QApplication.processEvents()

    def recurring_timer(self):
        self.counter +=1
        self.l.setText("Counter: %d" % self.counter)
