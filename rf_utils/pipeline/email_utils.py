#!/usr/bin/env python
# -- coding: utf-8 --
import smtplib

def send_mail(
    sender_email="pipeline.riff@gmail.com",
    title='', 
    password="Riff2018!",
    to=[],
    subject="Pipeline Noti",
    body="Hey, check it out"
):
    sent_from = sender_email
    # to = ['kenshero.k@gmail.com']
    # subject = 'OMG Super Important Message'
    # body = "Hey, what's up? You"

    email_text = "From: {title} <{sent_from}>\nTo: {to}\nSubject: {subject}\n\n{body}".format(
        title=title, 
        sent_from=sent_from,
        to=", ".join(to),
        subject=subject,
        body=body
    )

    try:
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(sender_email, password)
        server.sendmail(sent_from, to, email_text)
        server.close()
        print('Email sent!')
    except:
        print('Error cannot send mail')



# pipeline.riff@gmail.com To: Subject: Pipeline Noti <pipeline.riff@gmail.com>