# editRenderLayer
import os
import sys

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

import maya.cmds as mc
from rf_utils.context import context_info

def file_name_prefix(department = 'light'):
    scene = context_info.ContextPathInfo()
    if scene.step == department:
        render_layer = mc.ls(type='renderLayer')
        for layer in render_layer:
            if not 'defaultRenderLayer' in layer:
                scene.context.update(process = layer)
                mc.editRenderLayerGlobals(crl = layer)
                publ_name = scene.publish_name(hero=True, ext='.exr')
                new_publ_name = publ_name.replace('.exr', '')
                mc.setAttr ("defaultRenderGlobals.animation", 1)
                mc.setAttr ("redshiftOptions.imageFormat", 1)
                mc.setAttr("defaultRenderGlobals.imageFilePrefix", new_publ_name ,type='string')
                mc.editRenderLayerAdjustment("defaultRenderGlobals.imageFilePrefix")


def file_name_prefix_playblast():
    scene = context_info.ContextPathInfo()
    scene.context.update(publishVersion=scene.version)
    publ_name = scene.publish_name(hero=True, ext='.exr')
    new_publ_name = publ_name.replace('.exr', '')
    mc.setAttr ("defaultRenderGlobals.animation", 1)
    mc.setAttr ("defaultRenderGlobals.imageFormat", 32)
    mc.setAttr("defaultRenderGlobals.imageFilePrefix", new_publ_name ,type='string')
