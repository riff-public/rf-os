#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import logging 

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

from rf_utils.context import context_info
from rf_utils.pipeline import email_utils
reload(email_utils)
from rf_utils import file_utils
from rf_utils.sg import sg_process
sg = sg_process.sg

class Cache: 
    project = dict()
    step = dict()
    message = dict()

class Config: 
    messagePath = '%s/noti_message.yml' % moduleDir
    ignoreStatus = ['omt', 'wtg']

class SGConfig: 
    entityMap = {'asset': 'Asset', 'scene': 'Shot'}


def run(entity, additionalData=None): 
    body2 = ''
    notiDict = get_downstream_trigger(entity)
    if notiDict: 
        if additionalData: 
            body2 = additionalData.get('emailbody')
        return send_email(entity, notiDict, body2)

# additionalData = {'emailbody': 'xxxx'}

def setup_email_body(entity, description, path, userName): 
    messages = 'Project: %s\n' % entity.project
    messages += 'Type: %s\n' % entity.entity_type
    messages += 'Name: %s\n' % entity.name
    messages += 'Task: %s\n' % entity.task
    messages += 'User: %s\n' % userName
    messages += '------------------\n'
    messages += 'Description: %s\n' % description
    messages += '------------------\n'
    messages += 'Path: %s\n' % path
    data = {'emailbody': messages}
    return data


def send_email(entity, notiDict, body2=''): 
    messageDict = get_project_config(entity)

    for dstStep, data in notiDict.iteritems(): 
        srcStep = data['from']
        sgEmails = data['emails']
        emailInfo = messageDict.get(srcStep, dict()).get(dstStep, dict())

        if emailInfo: 
            to = emailInfo['to']
            title = emailInfo['title'].encode('utf-8')
            subject = emailInfo['subject']
            body = emailInfo['body']
            body = replace_context_key(entity, body)
            body = '%s\n\n%s' % (body, body2) if body2 else body
            subject = replace_context_key(entity, subject).encode('utf-8')
            recipients = sgEmails + to
            recipients = [a for a in recipients if not a == 'changeme@email.com']
            email_utils.send_mail(title=title, to=recipients, subject=subject, body=body.encode('utf-8'))
            logger.info('Email sent: from "%s" to "%s" to emails %s' % (srcStep, dstStep, recipients))
            return True


def get_downstream_step(entity): 
    downstreamSteps = []
    origin = entity.step 
    dependencies = get_project_config(entity)

    if origin in dependencies.keys(): 
        downstreamSteps = dependencies[origin].keys()

    return downstreamSteps

def get_downstream_trigger(entity): 
    """ check if status from downstream steps match status condition, if yes, generate trigger dict """
    triggerDict = dict()
    downstreamSteps = get_downstream_step(entity)
    sgEntity = find_entity(entity)

    if downstreamSteps: 
        for step in downstreamSteps: 
            taskEntities = find_step_tasks(entity.project, step, sgEntity, SGConfig.entityMap[entity.entity_type])
            statuses = [a.get('sg_status_list') for a in taskEntities]
            assignees = [a.get('task_assignees') for a in taskEntities if a.get('task_assignees')]
            emails = []

            if any([a not in Config.ignoreStatus for a in statuses]): 
                if assignees: 
                    for users in assignees: 
                        for user in users: 
                            userEntity = find_user_email(user['id'])
                            
                            if userEntity: 
                                email = userEntity.get('email')
                                emails.append(email) if email else None
                triggerDict[step] = {'from': entity.step, 'assignees': assignees, 'emails': list(set(emails))}

    return triggerDict

def find_step(step, entityType): 
    stepEntity = Cache.step.get(step)
    if not stepEntity: 
        stepEntity = sg.find_one('Step', [['code', 'is', step], ['entity_type', 'is', entityType]], ['code', 'id'])
        Cache.step[step] = stepEntity
        logger.debug('Fetch step from Shotgun')
    return stepEntity

def find_user_email(userId): 
    filters = [['id', 'is', userId]]
    fields = ['email', 'name', 'code']
    return sg.find_one('HumanUser', filters, fields)

def find_project(project): 
    projectEntity = Cache.project.get(project)
    if not projectEntity: 
        projectEntity = sg.find_one('Project', [['name', 'is', project]], ['name', 'id'])
        Cache.project[project] = projectEntity
        logger.debug('Fetch project from Shotgun')
    return projectEntity

def find_entity(entity): 
    sgEntity = SGConfig.entityMap[entity.entity_type] 
    projectEntity = find_project(entity.project)
    filters = [['project', 'is', projectEntity], ['code', 'is', entity.name]]
    fields = ['code', 'id']
    return sg.find_one(sgEntity, filters, fields)


def find_step_tasks(project, step, sgEntity, entityType): 
    projectEntity = find_project(project)
    stepEntity = find_step(step, entityType)
    filters = [['project', 'is', projectEntity], ['step', 'is', stepEntity], ['entity', 'is', sgEntity]]
    fields = ['content', 'id', 'sg_status_list', 'task_assignees']
    taskEntities = sg.find('Task', filters, fields)
    return taskEntities


def get_config_message(): 
    data = Cache.message
    if not data: 
        data = file_utils.ymlLoader(Config.messagePath)
    return data 

def get_project_config(entity): 
    data = get_config_message()
    projectData = data.get(entity.project, dict())
    entityData = projectData.get(entity.entity_type, dict())
    return entityData


def replace_context_key(entity, message): 
    prefix = '$'
    keys = []
    replaceDict = dict()

    for chunk in message.split(' '): 
        if chunk[0] == prefix: 
            keys.append(chunk)

    for key in keys: 
        value = eval('entity.%s' % key[1:])
        replaceDict[key] = value

    for k, v in replaceDict.iteritems(): 
        message = message.replace(k, v)

    return message
