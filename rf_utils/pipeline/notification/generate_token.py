import requests, json, sys, os

from rf_utils import file_utils

import rf_config as config
PIPELINEGLOBAL = config.Env.pipelineGlobal

# moduleDir = os.path.dirname(sys.modules[__name__].__file__)
# tokenPath = '%s/token_config.yml' % moduleDir
tokenPath = '%s/zohoToken/notification_token.yml'% PIPELINEGLOBAL

redirect_uri= 'https://cliq.zoho.com/api/v2/bots/jengchanbot/message'
client_id = '1000.QAIE3X8Q3IF5B7ZUMESDCTIGAVXUGP'
client_secret = '4c6ee9bbf3faba2e88c8e104fbbd00587890b02ddd'  # <----READ BELOW
# to get the id and secret above, use web browser and go to,
# https://accounts.zoho.com/developerconsole  Client secret tab


# # ---- GRANT TOKEN
grant_token = '1000.053b8b433fb0b23a692416c3f3e89965.a332c7d73f7b16217eaf2e70845057e8'  # <----READ BELOW
# to get grant token, 
# go to developerconsole url above
# go to Generate code tab, in Scope field, use: 
# ZohoCliq.Channels.CREATE, ZohoCliq.Channels.READ, ZohoCliq.Channels.UPDATE 
# ZohoCliq.Webhooks.CREATE, ZohoCliq.Webhooks.READ, ZohoCliq.Webhooks.UPDATE 
# select time duration and put some description hit create, copy and paste above
if os.path.exists(tokenPath):
	tokenDict = file_utils.ymlLoader(tokenPath)
	print('tokenDict', tokenDict)
	refresh_token = tokenDict.get('refresh_token')
else:
	refresh_token = None
	tokenDict = None


# # ------- GENERATE BOTH ACCESS AND REFRESH TOKEN
def generate_token():
	if refresh_token:
		res = requests.post(url='https://accounts.zoho.com/oauth/v2/token?code={grant_token}&redirect_uri={redirect_uri}&client_id={client_id}&client_secret={client_secret}&grant_type=authorization_code'.format(grant_token=grant_token, redirect_uri=redirect_uri, client_id=client_id, client_secret=client_secret))
		res_json = res.json()
		print(res_json)
		access_token = res_json['access_token']
		refresh_token = res_json['refresh_token']


# # ---- ACCESS TOKEN HAS TIME OFF, WE NEED TO USE REFRESH TOKEN TO GET ACCESS TOKEN
def refresh_access_token():
	if refresh_token:
		res = requests.post(url='https://accounts.zoho.com/oauth/v2/token?refresh_token={refresh_token}&redirect_uri={redirect_uri}&client_id={client_id}&client_secret={client_secret}&grant_type=refresh_token'.format(refresh_token=refresh_token, redirect_uri=redirect_uri, client_id=client_id, client_secret=client_secret))

		res_json = res.json()
		access_token = res_json['access_token']
		print('access_token: {}'.format(access_token))
		write_config(access_token, refresh_token)
		return access_token


def get_access_token():
	if tokenDict:
		# tokenDict = file_utils.ymlLoader(tokenPath)
		access_token = tokenDict.get('access_token')
		return access_token
	else:
		return None

def write_config(access_token, refresh_token):
	if refresh_token:
		tokenData = {'access_token':access_token, 'refresh_token': refresh_token}
		file_utils.ymlDumper(tokenPath, tokenData)


# # ----- TO USE ACCESS TOKEN TO CALL ANY REST API
'''
Request Header:
 Authorization:Zoho-oauthtoken  dd7e47321d48b8a7e312e3d6eb1a9bb8.b6c07ac766ec11da98bf
 Content-Type: application/json

Request URL
 GET
 https://cliq.zoho.com/api/v2/channels

           
The access token should be passed as a header when you call any Cliq REST API. Please note that the access token can be passed only as a request header and not as a request parameter.
The list of mandatory headers are:

Header Name     Header Value
Authorization   Zoho-oauthtoken {{access token}}
contentType     application/json





# ----- WHEN REFRESH TOKEN REACHES ITS LIMIT, REVOKE IT
res = requests.post(url='https://accounts.zoho.com/oauth/v2/token/revoke?refresh_token={refresh_token}'.format(refresh_token=refresh_token))
print res
# res_json = res.json()
# print res_json

'''


### TEST SEND NOTIFICATION ###

# from rf_utils.pipeline.notification import noti_module_cliq as noti
# reload(noti)
    
# botName = 'jengchanbot'
    
# for i in range(11):
#     print i
#     noti.main(botName=botName, subject='test token', message = 'TEST TOKEN 1 2 3 {%s}' % i, emails=['Chaiyachat.r@riff-studio.com'])


