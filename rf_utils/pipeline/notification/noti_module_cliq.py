#!/usr/bin/env python
# -- coding: utf-8 --

import requests,json,sys,os

import lucidity

from rf_utils import user_info
from rf_utils.context import context_info
from rf_utils.sg import sg_process
from . import generate_token
import importlib

sentby = "แจ้งจัง Jengchan Bot"

def main(botName='', subject='', message='', boardcast=False, emails=[], urlImage = []):
    
    if sys.version_info.major == 2 :
        reload(generate_token)
    elif sys.version_info.major == 3 :
        importlib.reload(generate_token)

    access_token = generate_token.get_access_token()
    
    if access_token:

        destination = botName
        
        subject = subject

        message = message

        userids =  ','.join(emails)

        urldest="https://cliq.zoho.com/api/v2/bots/" + (destination) + "/message"

        headers = {
                "Authorization": "Zoho-oauthtoken " + (access_token),
                "contentType" : "application/json"
                }

        content = {
                "text": (message),
                "broadcast" : boardcast,
                "bot": {
                    "name": (sentby),
                    "image": 'https://previewengine-accl.zohoexternal.com/image/WD/jai7n2a6503fe634349a1abee21fd14627cff?width=2046&height=1536'
                    },
                "userids": userids
                }
        if subject:
            content['card']={
                            "title": (subject),
                            "theme": "modern-inline"
                            }

        if urlImage:
            if len(urlImage[0]) > 1000:
                urlImage = ['https://previewengine-accl.zoho.com/image/WD/54mxc9836fa8a51d545eab56f79f735e2029c?width=2046&height=1536']
            content["slides"] = [{"type":"images","data":urlImage}]

        post = requests.post(url=urldest,data=json.dumps(content), headers=headers)
        print(post)
        print("Response HTTP Status Code : ", post.status_code)


        limitCount = 0
        while post.status_code == 401:
            print('Refreshing Access Token . . . . .')
            access_token = generate_token.refresh_access_token()

            headers = {
                    "Authorization": "Zoho-oauthtoken " + (access_token),
                    "contentType" : "application/json"
                    }
            post = requests.post(url=urldest,data=json.dumps(content), headers=headers)
            print(post)
            print("Response HTTP Status Code : ", post.status_code)
            limitCount += 1

            if limitCount == 3:
                print('FAILED TO SEND MESSAGE TRY TO CHECK YOUR ACCESS.')
                break


def boardcast(message):

    destination = 'jengchanbot'

    message = message

    # userids =  ','.join(['Chaiyachat.r@riff-studio.com'])

    urldest="https://cliq.zoho.com/api/v2/bots/" + (destination) + "/message"

    headers = {
            "Authorization": "Zoho-authtoken " + (token)
            }

    content = {
            "text": (message),
            "broadcast" : True,
            "bot": {
                "name": (sentby),
                "image": 'https://previewengine-accl.zohoexternal.com/image/WD/jai7n2a6503fe634349a1abee21fd14627cff?width=2046&height=1536'
                }
            }

    post = requests.post(url=urldest,data=json.dumps(content), headers=headers)
    print(post)
    print("Response HTTP Status Code : ", post.status_code)

def get_notification_data(taskResult, taskEntity, description, userEntity):
    emails = {}
    to = {}
    # cache = {'step':{'name':'cache'}, 'task_assignees':[{'name':'-'}], 'project':taskEntity['project'], 'sg_status_list':'def' }
    # taskResult.append(cache)
    sgUser = user_info.SGUser()
    artistName = userEntity.get('sg_name')
    artistLocalName = sgUser.get_user_local_name(name=artistName)

    entityName = taskEntity['entity'].get('name')
    stepName = taskEntity['step'].get('name') if taskEntity.get('step') else taskEntity.get('sg_department')
    taskName = taskEntity.get('content') if taskEntity.get('content') else taskEntity['sg_task'].get('name')

    for tirggerTask in taskResult:
        assignList = []
        assigns = []
        if tirggerTask:
            if 'task_assignees' in tirggerTask:
                assigns = tirggerTask['task_assignees'] if tirggerTask.get('task_assignees') else []

            assigns.append(userEntity)

            step =  tirggerTask['step'].get('name') if tirggerTask.get('step') else tirggerTask.get('sg_department')
            step = step.lower()
            project = tirggerTask['project'].get('name')
            status = tirggerTask.get('sg_status_list')
            context = context_info.Context()
            context.update(entityType=taskEntity['entity']['type'].lower(), step=step, project=project)
            entity = context_info.ContextPathInfo(context=context)
            messageConfig = entity.projectInfo.notification_message_data()

            # data =  {'entityName': str(entityName), 
            #         'stepName':str(taskEntity['step'].get('name')), 
            #         'taskName':str(taskEntity.get('content')), 
            #         'artist':str(artistName),
            #         'artistLocal':str(artistLocalName),
            #         'project':str(project),
            #         'status':str(status),
            #         'srcStatus':str(taskEntity['sg_status_list']),
            #         'desc':description.encode(encoding='UTF-8',errors='strict')}
            for assign in assigns:
                name = assign.get('name')
                mail = sgUser.get_user_entity(name=name)
                localName = sgUser.get_user_local_name(name=name)
                language = sgUser.get_user_language(name=name)
                if mail:
                    mail = mail.get('email')
                    if not mail in emails:
                        assignList.append(localName)

                        data =  {'entityName': str(entityName), 
                                'stepName':str(stepName), 
                                'taskName':str(taskName), 
                                'artist':str(artistName),
                                'artistLocal':str(artistLocalName),
                                'project':str(project),
                                'status':str(status),
                                'srcStatus':str(taskEntity['sg_status_list']),
                                'desc':description.encode(encoding='UTF-8',errors='strict'),
                                'messageConfig':messageConfig}
                        
                        data['name'] = localName
                        data['dstStepName'] = str(step)
                        data['dstArtist'] = str(localName)
                        data['language'] = str(language)
                        emails[mail] = data
            supList = sgUser.get_supervisor(entity)
            prodList = sgUser.get_producer(entity)
            for sup in supList:
                mail = sup.get('email')
                name = sup.get('name')
                localName = sup.get('sg_local_name')
                language = sup.get('sg_languages')
                assignName = ''
                if not mail in emails:
                    if assignList:
                        for assignArt in assignList:
                            assignName = assignName+assignArt+' '
                    else:
                        assignName = '-'
                    # assignName = assignList[0] if assignList else '-'
                    data =  {'entityName': str(entityName), 
                            'stepName':str(stepName), 
                            'taskName':str(taskName), 
                            'artist':str(artistName),
                            'artistLocal':str(artistLocalName),
                            'project':str(project),
                            'dstArtist':str(assignName),
                            'language':str(language),
                            'status':str(status),
                            'srcStatus':str(taskEntity['sg_status_list']),
                            'desc':description.encode(encoding='UTF-8',errors='strict'),
                            'messageConfig':messageConfig}
                    data['name'] = localName
                    data['dstStepName'] = str(step)
                    emails[mail] = data
            for prod in prodList:
                mail = prod.get('email')
                name = prod.get('name')
                localName = prod.get('sg_local_name')
                language = prod.get('sg_languages')
                if not mail in emails:
                    data =  {'entityName': str(entityName), 
                            'stepName':str(stepName), 
                            'taskName':str(taskName), 
                            'artist':str(artistName),
                            'artistLocal':str(artistLocalName),
                            'project':str(project),
                            'language':str(language),
                            'status':str(status),
                            'srcStatus':str(taskEntity['sg_status_list']),
                            'desc':description.encode(encoding='UTF-8',errors='strict'),
                            'messageConfig':messageConfig}
                    data['name'] = localName
                    data['dstStepName'] = 'prod'
                    data['dstArtist'] = '-'
                    # assignName = assignList[0] if assignList else '-'
                    effectMessage = '{} :'.format(step) if not step == 'prod' else '-'
                    if assignList:
                        for assignArt in assignList:
                            assignName = assignArt
                            effectMessage += '\n\tAssigned Artist : {}'.format(assignName) if not step == 'prod' else '-'
                    else:
                        assignName = '-'
                        effectMessage += '\n\tAssigned Artist : {}'.format(assignName) if not step == 'prod' else '-'
                    data['effected'] = effectMessage
                    emails[mail] = data
                    # emails[name] = mail
                else:
                    data = emails.get(mail)
                    effectMessage = data.get('effected')
                    if effectMessage:
                        if not str(step)in effectMessage:
                            # assignName = assignList[0] if assignList else '-'
                            # effectMessage += '\n{} :\n\tAssigned Artist : {}'.format(step, assignName)
                            effectMessage += '\n{} :'.format(step) if not step == 'prod' else '-'
                            if assignList:
                                for assignArt in assignList:
                                    assignName = assignArt
                                    effectMessage += '\n\tAssigned Artist : {}'.format(assignName) if not step == 'prod' else '-'
                            else:
                                assignName = '-'
                                effectMessage += '\n\tAssigned Artist : {}'.format(assignName) if not step == 'prod' else '-'
                            
                            data['effected'] = effectMessage

            # if assignList:
            #     to[step] = assignList
            # else:
            #     to[step] = ['-']

    return emails

def send_notification(emails, taskEntity, urlImage=[], boardcast=False):
    botName = 'jengchanbot'

    # TEST DATA
    # emails = emails
    # emails = {'Pia':'chaiyachat.r@riff-studio.com'}
    # print emails
    # data = {'name': 'Pia', 
    # 'assetName': str(asset.name), 
    # 'stepName':str(stepName), 
    # 'taskName':str(taskName), 
    # 'artist':'Pia',
    # 'dstStepName':'rig',
    # 'dstArtist':'Tan',
    # 'desc':description.encode(encoding='UTF-8',errors='strict')}
    # emails = {'chaiyachat.r@riff-studio.com':data}

    for mail,data in emails.items():
        messageConfig = data['messageConfig']
        mailtest = ['chanon.v@riff-studio.com','chaiyachat.r@riff-studio.com','nattapong.n@riff-studio.com','jittarin.j@riff-studio.com']
        mailtest.append(mail)
        mail = mailtest
        # mail = ['chaiyachat.r@riff-studio.com']
        dstStep = data['dstStepName']
        status = data['status']
        srcStatus = data['srcStatus']
        srcTask = messageConfig.get(taskEntity['entity']['type'].lower()).get(data['stepName'].lower())
        if srcTask:
            dstTask = srcTask.get(dstStep)
            if not dstTask:
                dstTask = srcTask.get('def')
            if dstTask:
                statusMessage = dstTask.get(status)
                if not statusMessage:
                    statusMessage = dstTask.get('def')

                if sys.version_info.major == 2 :
                    dstTask = statusMessage.get(data['language'])
                    title = dstTask.get('title')[0]
                    title = title.encode(encoding='UTF-8',errors='strict')
                    titleTemplate = lucidity.Template('title', title)
                    subject = titleTemplate.format(data)
                    subject = subject.decode(encoding='UTF-8',errors='strict')

                    message = dstTask.get('message')[0]
                    message = message.encode(encoding='UTF-8',errors='strict')
                    messageTemplate = lucidity.Template('message', message)
                    messageSend = messageTemplate.format(data)
                    messageSend = messageSend.decode(encoding='UTF-8',errors='strict')

                elif sys.version_info.major == 3 :
                    data['desc'] = data['desc'].decode(encoding='UTF-8',errors='strict')
                    dstTask = statusMessage.get(data['language'])
                    title = dstTask.get('title')[0]
                    titleByte = title.encode(encoding='UTF-8',errors='strict')
                    title = titleByte.decode(encoding='UTF-8',errors='strict')
                    titleTemplate = lucidity.Template('title', title)
                    subject = titleTemplate.format(data)

                    message = dstTask.get('message')[0]
                    messageByte = message.encode(encoding='UTF-8',errors='strict')
                    message = messageByte.decode(encoding='UTF-8',errors='strict')
                    message = str(message)
                    messageTemplate = lucidity.Template('message', message)
                    messageSend = messageTemplate.format(data)

                if not urlImage:
                    urlImage = ['https://previewengine-accl.zoho.com/image/WD/54mxc9836fa8a51d545eab56f79f735e2029c?width=2046&height=1536']
                # print imgurl
                # urlImage = [imgurl]
                # print urlImage
                # print 'SENT MESSAGE TO BOT TEST '
                # print mail
                # print subject
                # print messageSend
                # noti_module_cliq.main(botName=botName, subject=subject, message=messageSend, boardcast=boardcast, emails=[mail], urlImage=urlImage)
                main(botName=botName, subject=subject, message=messageSend, boardcast=boardcast, emails=mail, urlImage=urlImage)
                # break

def send_scene_notification(taskResult, taskEntity, entity, userEntity, versionResult=None, description='', imgurl=None):
    triggerConfig = entity.projectInfo.notification_trigger_data()
    srcStep = triggerConfig.get(taskEntity['step'].get('name').lower())
    # srcTask = triggerConfig.get(entity.entity_type).get(taskEntity['step'].get('name').lower()

    if srcStep: 
        for step, task in srcStep.iteritems():
            triggerStep = step
            triggerTaskName = task
            if step == 'prod':
                pass
                # prod = {'step':{'name':'prod'}, 'task_assignees':[{'name':'-'}], 'project':taskEntity['project'], 'sg_status_list':'def' }
                # taskResult.append(prod)
            elif not taskEntity['sg_status_list'] == 'rev':
                taskResult.append(sg_process.get_one_task_by_step(taskEntity['entity'], triggerStep, triggerTaskName))

        emails = get_notification_data(taskResult, taskEntity, description, userEntity)
        # get image
        if versionResult:
            imgurl = sg_process.get_media(versionResult)
            if '.mov' in imgurl:
                imgurl = sg_process.get_media_thumbnail(versionResult)
            if 'thumbnail_pending' in imgurl:
                imgurl = 'https://previewengine-accl.zoho.com/image/WD/54mxc9836fa8a51d545eab56f79f735e2029c?width=2046&height=1536'
        elif imgurl:
            imgurl = imgurl
        else:
            imgurl = 'https://previewengine-accl.zoho.com/image/WD/54mxc9836fa8a51d545eab56f79f735e2029c?width=2046&height=1536'
        # print imgurl
        urlImage = [imgurl]

        send_notification(emails, taskEntity, urlImage=urlImage)

def send_announcer_notification(taskResult, taskEntity, userEntity, description='', imgurl=None):

        emails = get_notification_data(taskResult, taskEntity, description, userEntity)
        # get image
        # imgurl = 'https://previewengine-accl.zoho.com/image/WD/54mxc9836fa8a51d545eab56f79f735e2029c?width=2046&height=1536'
        imgurl = 'https://previewengine-accl.zoho.com/image/WD/9j2a79cd789999504432db097b045c36bebd9?width=2046&height=1536'
        # print imgurl
        urlImage = [imgurl]

        send_notification(emails, taskEntity, urlImage=urlImage)

def send_failed_upload_notification(emails, boardcast=False):
    botName = 'jengchanbot'

    # TEST DATA
    # emails = emails
    # emails = {'Pia':'chaiyachat.r@riff-studio.com'}
    # print emails
    # data = {'name': 'Pia', 
    # 'assetName': str(asset.name), 
    # 'stepName':str(stepName), 
    # 'taskName':str(taskName), 
    # 'artist':'Pia',
    # 'dstStepName':'rig',
    # 'dstArtist':'Tan',
    # 'desc':description.encode(encoding='UTF-8',errors='strict')}
    # emails = {'chaiyachat.r@riff-studio.com':data}

    # messageConfig = entity.projectInfo.notification_message_data()
    for mail,data in emails.items():
        mailtest = ['chanon.v@riff-studio.com','chaiyachat.r@riff-studio.com','nattapong.n@riff-studio.com','jittarin.j@riff-studio.com']
        mailtest.append(mail)
        mail = mailtest
        # mail = ['chaiyachat.r@riff-studio.com']
        subject = '{entityName} Publish Failed ! ! !'.format(entityName=data['entityName'])
        message = 'แจ้ง {name} เมื่อกี๊ {artistLocal} publish {stepName} {entityName} ใน task {taskName} ไม่ผ่านค่ะ'.format(name=data['name'],artistLocal=data['artistLocal'],stepName=data['stepName'],entityName=data['entityName'],taskName=data['taskName'])
        # message = message.encode(encoding='UTF-8',errors='strict')
        # messageSend = messageSend.decode(encoding='UTF-8',errors='strict')

        # title = title.encode(encoding='UTF-8',errors='strict')
        # subject = subject.decode(encoding='UTF-8',errors='strict')

        # failed icon
        urlImage = ['https://previewengine-accl.zoho.com/image/WD/2ni53a807a8e547de4ff7ac0ae1d5a4fbd0ca?width=2046&height=1536']
        # print imgurl
        # urlImage = [imgurl]
        # print 'SENT MESSAGE TO BOT TEST '
        # print mail
        # print subject
        # print messageSend
        # noti_module_cliq.main(botName=botName, subject=subject, message=messageSend, boardcast=boardcast, emails=[mail], urlImage=urlImage)
        main(botName=botName, subject=subject, message=message, boardcast=boardcast, emails=mail, urlImage=urlImage)
        # break