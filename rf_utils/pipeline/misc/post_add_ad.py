import os
import sys
from rf_utils import file_utils
from rf_utils import register_entity
from rf_utils.context import context_info
reload(register_entity)

def update_design(path='P:/SevenChickMovie/design/publ/rnd/arthurTest'):
	processes = file_utils.list_folder(path)
	processes = [a for a in processes if not a in ['hero', '_data']]
	workPath = convertPath(path)
	asset = context_info.ContextPathInfo(path=workPath)
	print 'asset', asset.name

	if processes:
		for process in processes:
			print 'process', process
			asset.context.update(res='md', step='design', process=process)
			processPath = '%s/%s' % (path, process)
			heroFile = get_published_file(processPath, hero=True)
			heroFile = heroFile[0] if heroFile else ''
			publishedFile = get_published_file(processPath, hero=False)
			publishedFile = publishedFile[0] if publishedFile else ''

			regInfo = register_entity.Register(asset)
			output = register_entity.Output()
			output.add_design(publishedFile, heroFile)
			output.add_process(asset.process)
			output.add_media(os.path.dirname(publishedFile), os.path.dirname(heroFile))
			regInfo.set(output=output)
			# regInfo.add_asset_id(100)
			regInfo.write()
		regInfo.register()
		print 'done register'

def convertPath(path):
	return path.replace('/publ/', '/work/')

def list_asset_path(path='P:/SevenChickMovie/design/publ/char'):
	assetNames = [('/').join([path, a]) for a in file_utils.list_folder(path)]
	return assetNames

def get_published_file(path, hero=True):
	if hero:
		heroDir = '%s/%s/output' % (path, 'hero')
		heroFile = ['%s/%s' % (heroDir, a) for a in file_utils.list_file(heroDir)] if os.path.exists(heroDir) else []
		heroFile = heroFile[0] if heroFile else ''
		return [heroFile]

	else:
		versions = [a for a in file_utils.list_folder(path) if a[0] == 'v']
		publishedFiles = []
		version = sorted(versions)[-1] if versions else ''
		if version:
			versionDir = '%s/%s/output' % (path, version)
			publishedFile = ['%s/%s' % (versionDir, a) for a in file_utils.list_file(versionDir)] if os.path.exists(versionDir) else []

			return publishedFile
			# heroDir = '%s/%s' % (path, 'hero/output')
