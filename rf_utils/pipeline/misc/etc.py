import sys
import os 
import maya.cmds as mc 

from rf_utils.context import context_info
from rf_utils import file_utils

def get_gpu_asset(): 
	paths = []
	abcs = []

	# add pack asm data
	asmPaths = []
	for node in mc.ls(type='gpuCache'): 
		gpuNode = mc.listRelatives(node, p=True, fullPath=True)
		locator = mc.listRelatives(gpuNode, p=True, fullPath=True)[0]
		if mc.objExists('%s.hidden' % locator):
			if not mc.getAttr('%s.hidden' % locator):
				path = mc.getAttr('%s.cacheFileName' % node)
				paths.append(path) if not path in paths else None
				asmDataPath = mc.getAttr('%s.description' % locator)
				asmPaths.append(asmDataPath) if not asmDataPath in asmPaths else None 
		
	for path in paths: 
		entity = context_info.ContextPathInfo(path=path)
		# entity.context.update(step='lookdev', look='main', process='main', app='maya')
		entity.context.update(step='model', process='main', res='md')
		# heroPath = entity.path.output_hero().abs_path()
		heroPath = entity.path.output_hero().abs_path()
		outputName = entity.output_name(outputKey='cache', hero=True)

		abc = '%s/%s' % (heroPath, outputName)
		if not abc in abcs: 
			abcs.append(abc)

		# add pack rig from gpu
		entity.context.update(step='rig', process='main', res='md', look='main')
		heroPath = entity.path.hero().abs_path()
		outputName = entity.output_name(outputKey='rig', hero=True)
		mtrName = entity.output_name(outputKey='materialRig', hero=True)

		rig = '%s/%s' % (heroPath, outputName)
		mtrRig = '%s/%s' % (heroPath, mtrName)
		if not rig in abcs: 
			abcs.append(rig)
		if not mtrRig in abcs: 
			abcs.append(mtrRig)


	abcs = abcs + asmPaths

	return abcs

		# workspacePath = entity.path.app().abs_path()
		# entity.output_name()
		# files = file_utils.list_file(workspacePath)
		# if files: 
		#     print files[-1]
		# else: 
		#     print entity.name

def main(): 
	from rf_app.publish.scene import app
	reload(app)
	from rf_utils.context import context_info
	
	exportData = app.get_export_dict(scene, 's0030', ['camera', 'alembic_cache', 'shotdress'])
	app.do_export(scene, itemDatas=exportData, uiMode=False, publish=True)