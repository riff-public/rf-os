import os 
import sys 
from rf_utils.context import context_info 
from datetime import datetime
from rf_utils import file_utils 
from rf_utils.sg import sg_process
sg = sg_process.sg 


def model_time(project='Hanuman', step='model'): 
    step = 'model'
    entities = get_all_assets(project, step)
    
    for entity in entities: 
        # first workspace version 
        start_date = None
        publish_date = None
        last_publish_date = None
        duration = 0 
        logs = get_from_time_log(project, entity.name, step)
        duration = sum([a['duration'] for a in logs]) if logs else 0
        dur_day = (duration / 60.0) / 8.0
        # number of versions 
        vers = list_publish_versions(entity)

        firstws = get_workspace_file(entity)
        if os.path.exists(firstws): 
            start_date = datetime.fromtimestamp(os.path.getmtime(firstws))
            # print firstws, start_date
        # first proxy publish
        # last proxy publish  
        # first md publish 
        first_md = get_publish_file(entity, version='v001')
        if os.path.exists(first_md): 
            publish_date = datetime.fromtimestamp(os.path.getmtime(first_md))
            # print first_md, publish_date
        # last md publish 
        if vers: 
            last_md = get_publish_file(entity, version=vers[-1])
            if os.path.exists(last_md): 
                last_publish_date = datetime.fromtimestamp(os.path.getmtime(last_md))

        diff = (publish_date - start_date).days if start_date and publish_date else 0
        diff2 = (last_publish_date - start_date).days if start_date and last_publish_date else 0
        rev_diff = (last_publish_date - publish_date).days if last_publish_date and publish_date else 0
        print '{} {} {} {} {} {} {}'.format(entity.name, entity.type, dur_day, diff, diff2, rev_diff, len(vers))


def get_workspace_file(entity): 
    file = ''
    workspace = entity.path.workspace().abs_path()
    files = sorted(file_utils.list_file(workspace))
    if files: 
        ws = [a for a in files if os.path.splitext(a)[-1] == '.ma']
        file = '{}/{}'.format(workspace, ws[0])
    return file


def get_publish_file(entity, version='v001', res='md'): 
    # pubpath = entity.path.scheme(key='publishPath').abs_path()
    asset = entity.copy()
    asset.context.update(publishVersion=version, res='md')
    output_path = asset.path.scheme(key='publishOutputPath').abs_path()
    publish_file = asset.output_name(outputKey='cache')
    first_publish = '{}/{}'.format(output_path, publish_file)
    return first_publish
    # version_path = os.path.split(pubpath)[0]
    # vers = sorted(file_utils.list_folder(version_path))
    # first 

    # for ver in vers: 
    #     asset = entity.copy()
    #     asset.context.update(publishVersion=ver, res=res)
    #     output_path = asset.path.scheme(key='publishOutputPath').abs_path()
    #     publish_file = asset.output_name(outputKey='cache')
    #     print publish_file

def list_publish_versions(entity): 
    pubpath = entity.path.scheme(key='publishPath').abs_path()
    version_path = os.path.split(pubpath)[0]
    vers = sorted(file_utils.list_folder(version_path))
    return vers


def get_all_assets(project, step): 
    entities = list()
    fields = ['sg_asset_type', 'sg_subtype', 'project', 'code']
    assets = sg.find('Asset', [['project.Project.name', 'is', project]], fields)

    for sg_asset in assets: 
        context = context_info.Context()
        context.update(project=project, entityType='asset', step=step, process='main', app='maya')
        subtype = sg_asset['sg_subtype'] or ''
        context.update(
            entityGrp=sg_asset['sg_asset_type'], 
            entityParent=subtype, 
            entity=sg_asset['code'])
        asset = context_info.ContextPathInfo(context=context)
        entities.append(asset)

    return entities 


def get_from_time_log(project, entity_name, step): 
    logtype = None
    filters = [
        ['project.Project.name', 'is', project], 
        ['entity.Task.entity.Asset.code', 'is', entity_name], 
        ['entity.Task.step.Step.code', 'is', step]]
    fields = [
        'project', 'entity', 'user', 'duration', 'date', 'sg_type', 'description',
        'entity.Task.content', 'entity.Task.entity', 'entity.Task.updated_at',
        'entity.Task.task_assignees', 'entity.Task.project', 'user.HumanUser.department', 
        'entity.Ticket.sg_ticket_type', 'entity.Ticket.title', 'entity.Ticket.addressings_to', 
        'entity.Ticket.updated_at', 'entity.Ticket.sg_link']
    filters.append(['sg_type', 'is', logtype]) if logtype else None
    logs = sg.find('TimeLog', filters, fields)
    return logs

