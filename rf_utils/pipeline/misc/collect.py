import os 
import sys

from rf_utils import file_utils
from rf_utils.context import context_info
reload(context_info.project_info)


def lookdev_media(path): 
    # path = 'P:/SevenChickMovie/asset/publ/char'
    step = 'lookdev'
    assetNames = file_utils.list_folder(path)
    skipExts = ['.db']
    dst = ''
    entity = context_info.ContextPathInfo(path=path)
    artbookPath= entity.path.scheme(key='artbookPath').abs_path()
    dstPath = '%s/%s/%s' % (artbookPath, step, entity.type)
    
    if not os.path.exists(dstPath): 
        os.makedirs(dstPath)

    srcFiles = []

    for name in assetNames: 
        assetPath = '%s/%s' % (path, name)
        asset = context_info.ContextPathInfo(path=assetPath)
        asset.context.update(step=step, process='main')
        outputPath = asset.path.scheme(key='outputHeroImgPath').abs_path()
        files = file_utils.list_file(outputPath)
        exts = list(set([os.path.splitext(a)[-1] for a in files]))
        targets = []

        for ext in exts: 
            if not ext in skipExts: 
                target = sorted([a for a in files if ext in a])[-1]
                targets.append('%s/%s' % (outputPath, target))
            
        if targets: 
            srcFiles += targets

    if srcFiles: 
        for i, src in enumerate(srcFiles): 
            dst = '%s/%s' % (dstPath, os.path.basename(src))
            file_utils.xcopy_file(src, dst)
            print 'copy %s %s' % (i, dst)

# path = 'P:/SevenChickMovie/asset/publ/prop'
# collect.lookdev_media(path)
