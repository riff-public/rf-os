import os
import sys
import shutil
import json

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class Config:
	tempStorage = 'RF_temp_storage'


def local_storage_path():
	tempDir = os.environ['TEMP'].replace('\\', '/')
	tempStorage = '%s/%s' % (tempDir, Config.tempStorage)
	return tempStorage

def project_local_storage(project, entityType='asset', entityName=''):
	path = local_storage_path()
	local = '%s/project/%s/%s' % (project, entityType, entityName)
	return local

def export_local(entity, dstPath):
	localPath = project_local_storage(entity.project, entity.entity_type, entity.work_name())
	filename = os.path.basename(dstPath)
	localFile = '%s/%s' % (localPath, filename)

def local_path(path):
	root = local_storage_path()
	drive, relPath = os.path.splitdrive(path)
	return '%s%s' % (root, relPath)


def clear_temp_storage():
	path = local_storage_path()
	files = [('/').join([path, a]) for a in os.listdir(path) if os.path.isfile(('/').join([path, a]))]
	dirs = [('/').join([path, a]) for a in os.listdir(path) if os.path.isdir(('/').join([path, a]))]

	for eachfile in files:
		os.remove(eachfile)

	for dir in dirs:
		shutil.rmtree(dir)

	logger.debug('clear temp storage \n%s dirs %s files' % (len(files), len(dirs)))
	logger.debug(files)
	logger.debug(dirs)


def local_name(filename, makedir=True):
	name = os.path.basename(filename)
	tempDir = local_storage_path()
	os.makedirs(tempDir) if not os.path.exists(tempDir) and makedir else None
	tempFile = '%s/%s' % (tempDir, name)
	return tempFile


def copy_json(data):
    jsonData = json_loader(data)
    failedResult = []
    for src, dst in jsonData.iteritems:
        result = copy(src, dst)
        if not os.path.exists(dst):
            logger.warning('failed to copy %s' % dst)
            failedResult.append(dst)

    return True if not failedResult else False

def dump_copy_register(src, dst):
	jsonName = 'local_copy.json'

def json_dumper(path, data):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    with open(path, 'w') as outfile:
        json.dump(data, outfile)

def json_loader(path):
    with open(path) as configData:
        config = json.load(configData)
        return config
