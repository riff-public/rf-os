import rf_config as config
import json
# from rf_utils import project_info
# from rf_utils.context import context_info
from rf_maya.rftool.utils import redshift_hook
from rf_utils.context import context_info 

if config.isMaya:
    import maya.mel as mm
    import maya.cmds as mc


def process_set_matee_id(matteDataPath, target):
    asset = context_info.ContextPathInfo(path=matteDataPath)
    current_nodes = mc.ls(type='RedshiftAOV')
    with open(matteDataPath) as f:
        matte_data = json.load(f)
    create_node(matte_data, asset.name, target, current_nodes)
    create_object_id_node(matte_data, asset.name, target)


def create_node(matte_data, asset_name, target, current_nodes):
    for node in matte_data:
        if node['node_name'] not in current_nodes:
            if node['node_name'] == "ObjectID":
                mode = 1
            else:
                mode = 0
            redshift_hook.create_puzzle_matte(node['node_name'], mode)
            set_rgb_id(node['node_name'], node['red_id'], node['green_id'], node['blue_id'])


def set_rgb_id(node_name, red_id, green_id, blue_id):
    red_id = mc.setAttr('{node_name}.redId'.format(node_name=node_name), red_id)
    green_id = mc.setAttr('{node_name}.greenId'.format(node_name=node_name), green_id)
    blue_id = mc.setAttr('{node_name}.blueId'.format(node_name=node_name), blue_id)


def create_object_id_node(matte_data, asset_name, target):
    for node in matte_data:
        if node['node_name'] == "ObjectID":
            redshift_hook.add_object_id(target, node['red_id'], asset_name)
            break
