#!/usr/bin/env python
# -- coding: utf-8 --

import os
import sys
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.context import context_info
from rf_utils.sg import sg_process
reload(sg_process)
from rf_utils.widget import dialog

class Config: 
	lockTaskMap = {'model': 'model', 'texture': 'texture', 'groom': 'groom', 'anim': 'anim'}

def lock_asset(entity, step): 
	""" set task name sg_lockdown to True to lockdown asset """ 
	assetPaths = list_cache_assets(entity)
	failedAssets = []

	for assetPath in assetPaths: 
		asset = context_info.ContextPathInfo(path=assetPath)
		if asset.entity_type == context_info.ContextKey.asset: 
			asset.context.use_sg(sg_process.sg, project=entity.project, entityType='asset', entityName=asset.name)
			lockSuccess = False 
			result = lock_task(asset, step)

			if result == True: 
				lockSuccess = True 

			if not lockSuccess: 
				failedAssets.append(assetPath)

	return True if not failedAssets else failedAssets


def lock_shot(entity, step): 
	entity.context.use_sg(sg_process.sg, project=entity.project, entityType='scene', entityName=entity.name)
	result = lock_task(entity, step)
	return True if result == True else False 


def lock_task(entity, step): 
	tasks = sg_process.get_tasks_by_step(entity.context.sgEntity, step)

	# get task lock 
	lockTask = Config.lockTaskMap.get(step)

	if lockTask and tasks: 
		taskEntity = [a for a in tasks if a.get('content') == lockTask]
		
		if taskEntity: 
			data = {'sg_lockdown': True}
			result = sg_process.sg.update('Task', taskEntity[0].get('id'), data)
			
			if result: 
				logger.info('Lockdown %s' % (entity.name))
				return True

	 

def list_cache_assets(entity): 
	""" list cache asset in shot """ 
	import maya.cmds as mc

	geoGrp = entity.projectInfo.asset.geo_grp() or 'Geo_Grp'
	
	# get refs 
	refs = mc.file(q=True, r=True)
	# assetPaths = list(set([a.split('{')[0] for a in refs])) if refs else []
	cacheAssetPath = []

	for assetPath in refs: 
		namespace = mc.referenceQuery(assetPath, ns=True)
		targetGrp = '%s:%s' % (namespace[1:], geoGrp)

		if mc.objExists(targetGrp): 
			path = assetPath.split('{')[0]
			cacheAssetPath.append(path) if not path in cacheAssetPath else None

	return cacheAssetPath 


def check_status(entity, step, includeStatus=False): 
    entity.context.use_sg(sg_process.sg, project=entity.project, entityType=entity.entity_type, entityName=entity.name)
    tasks = sg_process.get_tasks_by_step(entity.context.sgEntity, step)
    validStatus = 'apr'
    allLockStatus = []

    for task in tasks: 
    	lockdown = task.get('sg_lockdown')
    	statusValue = task.get('sg_status_list')
    	status = True if statusValue == validStatus else False 
    	taskLock = all([lockdown, status]) if includeStatus else lockdown
    	allLockStatus.append(taskLock)

    lockdown = any(allLockStatus)
    return lockdown


def shot_lockdown_dialog(): 
	dialog.MessageBox.warning('Cannot open', '')
