import os 
import sys 
import logging
import maya.cmds as mc 
import pymel.core as pm 
from rf_utils import custom_exception
from rf_utils.context import context_info
from rf_utils import file_utils

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class Cache(object):
    """docstring for Cache"""
    def __init__(self, inputData):
        super(Cache, self).__init__()
        self.inputData = inputData
        self.path = self.get_path()
        self.namespace = self.get_namespace()
        self.entity = context_info.ContextPathInfo(path=self.path)

    def is_path(self): 
        if '/' in self.inputData: 
            return True 

    def is_namespace(self): 
        return mc.namespace(ex=self.inputData)

    def get_path(self): 
        if self.is_path(): 
            return self.inputData
        else: 
            # namespace 
            if self.is_namespace(): 
                # remove ':'
                inputData = self.inputData[1:] if self.inputData.startswith(':') else self.inputData
                ref = [a for a in self._get_reference() if inputData == a.referenceFile().namespace]
                if ref: 
                    return ref[0].referenceFile().withCopyNumber()
                else: 
                    raise custom_exception.PipelineError('"%s" is a namespace but not a reference' % self.inputData)
            else: 
                raise custom_exception.PipelineError('"%s" is not a namespace' % self.inputData)

    def _get_reference(self): 
        refs = [a for a in pm.ls(r=True, type='reference') if not a == 'sharedReferenceNode' and not a == '_UNKNOWN_REF_NODE_']
        return refs 

    def get_namespace(self): 
        if self.is_namespace(): 
            return self.inputData

        if self.is_path(): 
            fileRef = pm.FileReference(self.inputData)
            return fileRef.namespace

    def is_publish(self): 
        outputPath = self.entity.path.output().striproot()
        return True if outputPath in self.path else False

    def workspace_path(self): 
        entity = context_info.ContextPathInfo()
        self.entity.context.update(step=entity.step, process=entity.process)
        path = self.entity.path.scheme(key='cacheLocalPath')
        return path 

    def is_workspace(self): 
        path = self.workspace_path().striproot()
        return True  if path in self.path else False
            

    def workspace(self): 
        return '%s/%s' % (self.workspace_path().abs_path(), os.path.basename(self.path))

    def publish_path(self): 
        outputKey = 'cache'
        publishPath = self.entity.path.output()
        entity = self.entity.copy()
        guessProcess = self._find_namespace_from_filename()
        entity.context.update(process=guessProcess)
        return entity.path.scheme(key='cacheGlobalHero')

    def publish(self): 
        return '%s/%s' % (self.publish_path().abs_path(), os.path.basename(self.path))

    def _find_namespace_from_filename(self): 
        # hard coded 
        return os.path.basename(self.path).replace('%s_' % self.entity.name, '').split('.')[0]

    def is_asset(self): 
        if context_info.ContextKey.asset in self.path: 
            return True

    def checkout(self): 
        if self.is_publish(): 
            # copy 
            logger.debug('Checking out "%s"...' % self.namespace)
            dst = self.copy_to_workspace()
            rnNode = mc.referenceQuery(self.path, referenceNode=True)
            result = mc.file(dst, loadReference=rnNode, prompt=False)
            logger.info('Check out complete %s' % dst)
            return result

        if self.is_workspace():
            logging.warning('"%s" already checked out' % self.path)

        if self.is_asset(): 
            logger.warning('"%s" is an active asset. No need to checkout' % self.path)


    def copy_to_workspace(self): 
        dst = self.workspace()

        if not self.path == dst: 
            src = self.path.split('{')[0]
            file_utils.copy(src, dst)

        return dst 

