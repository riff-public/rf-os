import os
import sys 
from collections import OrderedDict

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.sg import sg_process
from rf_utils import file_utils
from rf_utils.context import context_info
sg = sg_process.sg


def collect_assets(project, assetNames, dst, hero=True, texture=False, process='main', res='md'): 
	assets, badAssets = get_asset_entities(project, assetNames)
	copyDirs = get_copy_dirs(assets, dst, hero=hero, texture=texture, process=process, res=res)

	for srcDir in copyDirs: 
		dstDir = '%s/%s' % (dst, os.path.splitdrive(srcDir)[-1])
		copy(srcDir, dstDir)


	if badAssets: 
		logger.warning('%s assets not found in shotgun and will not copy' % len(badAssets))
		for each in badAssets: 
			logger.warning(each)


def get_asset_entities(project, assetNames): 
	assets = []
	badAssets = []
	for assetName in assetNames: 
		asset = asset_entity(project, assetName)
		assets.append(asset) if asset else badAssets.append(assetName)

	return assets, badAssets

def get_copy_dirs(assets, dst, hero=True, texture=True, process='main', look='main', res='md'): 
	copyDirs = []
	
	for asset in assets: 
		asset.context.update(process=process, look=look, res=res)

		if hero: 
			heroDir = asset.path.hero().abs_path()
			copyDirs.append(heroDir)
		if texture: 
			textureDir = asset.path.publish_texture().abs_path()
			copyDirs.append(textureDir)

	logger.info('Copying %s dirs' % len(copyDirs))
	return copyDirs


def asset_entity(project, assetName): 
	context = context_info.Context()
	context.use_sg(sg, project=project, entityType='asset', entityName=assetName)
	asset = context_info.ContextPathInfo(context=context)
	if not asset.name == context.noContext: 
		return asset 

def copy(srcDir, dstDir): 
	logger.info('Copying %s -> %s ...' % (srcDir, dstDir))
	return file_utils.xcopy_directory(srcDir, dstDir)
