import os
import sys
import logging
import shutil

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import create_structure
from rf_utils.context import context_info
from rf_utils import project_info
from rf_utils import file_utils
from rf_utils.sg import sg_utils
sg = sg_utils.sg
moduleDir = os.path.dirname(sys.modules[__name__].__file__)
config = 'template_config.yml'

from rf_utils.pipeline.permission import set_permission
reload(set_permission)
# create shotgun / create folder structure / create links.

def create(projectName, templateProject='default'):
    """ create project """ 
    projectTmp = project_info.ProjectInfo(templateProject)
    project = project_info.ProjectInfo(projectName)
    permission = project.permission()

    # set active project 
    set_project_active(projectName)

    # copy rf_config template 
    template = project_info.ProjectInfo('template')
    copy_config(project, template)

    # copy config launcher 
    copy_launcher_config(project, projectTmp)

    # copy config export 
    copy_export_config(project, projectTmp, 'asset')
    copy_export_config(project, projectTmp, 'scene')

    # copy config qc  
    copy_qc_config(project, projectTmp)

    # create folder structure 
    # copy_structure(project, projectTmp)
    create_project_structure(project)
    set_permission.set_permission_create_project(projectName)

    logger.info('All config "%s" created', projectName)


def set_project_active(projectName): 
    projectEntity = sg.find_one('Project', [['name', 'is', projectName]], ['id'])
    data = {'sg_status': 'Active'}
    result = sg.update('Project', projectEntity['id'], data)
    if result: 
        logger.info('Set project %s (%s) active' % (projectName, projectEntity['id']))
    else: 
        logger.error('Project not found %s' % projectName)


def copy_config(project, projectTmp): 
    logger.info('Copying project config from %s...' % projectTmp.name())
    src = projectTmp.config_dir()
    dst = project.config_dir()
    # copy 
    copy_template(src, dst)
    logger.info('Project config created\n')


def copy_launcher_config(project, projectTmp): 
    logger.info('Copying launcher config from %s ...' % projectTmp.name())
    src = projectTmp.launcher_config() 
    dst = project.launcher_config()
    # copy 
    copy_template(src, dst)
    logger.info('Launcher config created\n')


def copy_export_config(project, projectTmp, entityType): 
    if entityType == 'asset': 
        logger.info('Copying asset config from %s ...' % projectTmp.name())
        src = projectTmp.export_asset_config()
        dst = project.export_asset_config()
        # copy 
        copy_template(src, dst)
        logger.info('Export asset config created %s\n' % dst)

    if entityType == 'scene': 
        logger.info('Copying scene config from %s ...' % projectTmp.name())
        src = projectTmp.export_scene_config()
        dst = project.export_scene_config()
        # copy 
        copy_template(src, dst)
        logger.info('Export scene config created %s\n' % dst) 


def copy_qc_config(project, projectTmp): 
    logger.info('Copying qc config from %s ...' % projectTmp.name())
    src = projectTmp.qc_config()
    dst = project.qc_config()
    # copy 
    copy_template(src, dst)
    logger.info('Qc Config created\n')


def copy_structure(project, projectTmp): 
    logger.info('Copying structure from %s ...' % projectTmp.name())
    srcTemplate = projectTmp.get_template_path()
    dst = '{}/{}'.format(project.rootProject, project.name())
    # copy 
    copy_template(srcTemplate, dst)
    logger.info('Structure created\n')
    

def copy_template(src, dst): 
    """ copy template members to dst """ 
    if os.path.exists(src): 
        # if input is file 
        if os.path.isfile(src): 
            if not os.path.exists(dst): 
                if not os.path.exists(os.path.dirname(dst)): 
                    os.makedirs(os.path.dirname(dst))
                
                shutil.copy2(src, dst)
                logger.info('%s created' % dst)
            else: 
                logger.debug('File exists, Skipped %s' % dst)

        # if input is dir 
        if os.path.isdir(src): 
            srcSubDirs = os.listdir(src)

            for each in srcSubDirs: 
                srcDir = '{}/{}'.format(src, each)
                dstDir = '{}/{}'.format(dst, each)
                
                if not os.path.exists(dstDir): 
                    if os.path.isdir(srcDir): 
                        shutil.copytree(srcDir, dstDir, symlinks=False, ignore=None)
                    
                    if os.path.isfile(srcDir): 
                        if not os.path.exists(os.path.dirname(dstDir)): 
                            os.makedirs(os.path.dirname(dstDir))
                        
                        shutil.copy2(srcDir, dstDir)
                    
                    logger.debug('%s created' % dstDir)
                else: 
                    logger.debug('Dir exists, Skipped %s' % dst)

    else: 
        logger.error('Template does not exists %s' % src)


def create_project_structure(project): 
    srcTemplate = project.get_template_structure()
    logger.info('Copying structure from %s ...' % srcTemplate)
    dst = '{}/{}'.format(project.rootProject, project.name())
    
    context = context_info.Context()
    context.update(project=project.name())
    create_structure.create_from_template(context, templatePath=srcTemplate, dstPath='', create=True)
