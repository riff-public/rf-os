import os
import sys
import logging
from Qt import QtCore
from datetime import datetime

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from collections import OrderedDict
from rf_utils import file_utils
from rf_utils.context import context_info

def write_media_sg(entity=None, media='', shotName='', description='', step='', task='', status='', **kwargs):
    path = snap_file(context_info.ContextPathInfo())
    data = OrderedDict()
    sgDict = OrderedDict()

    if os.path.exists(path):
        data = file_utils.ymlLoader(path)

    sgDict = {'context': str(entity.context), 'media': media, 'step': step, 'description': description, 'status': False, 'shotName': shotName}

    if 'sg' in data.keys():
        if 'mov' in data['sg'].keys():
            if not sgDict in data['sg']['mov']:
                data['sg']['mov'].append(sgDict)
        else:
            data['sg']['mov'] = [sgDict]
    else:
        data['sg'] = {'mov': [sgDict]}

    os.makedirs(os.path.dirname(path)) if not os.path.exists(os.path.dirname(path)) else None
    file_utils.ymlDumper(path, data)
    return path

def snap_file(entity):
    snapPath = entity.path.snap().abs_path()
    snapFile = entity.snap_name()
    path = '%s/%s' % (snapPath, snapFile)
    return path

def read(entity):
    # entity = context_info.ContextPathInfo()
    path = snap_file(entity)
    return file_utils.ymlLoader(path) if os.path.exists(path) else dict()

def read_data(entity):
    path = snap_file(entity)
    return file_utils.ymlLoader(path) if os.path.exists(path) else dict()


def publish(data):
    from rf_utils.sg import sg_process
    from rf_app.publish.asset import sg_hook
    from rf_utils import user_info
    from rf_utils import project_info
    from rf_shotgun_event_daemon.src.plugins import status_update
    reload(status_update)
    
    user = user_info.User()
    userEntity = user.sg_user()

    if 'sg' in data.keys():
        allList = data['sg'].get('mov')
        publishList = [a for a in allList if not a['status']]
        sgStatus = 'apr'

        if publishList:
            output('Publish %s shots' % (len(publishList)))
            for shotData in publishList:
                contextStr = shotData['context']
                media = shotData['media']
                step = shotData['step']
                description = shotData['description']
                status = shotData['status']
                shotName = shotData['shotName']

                # do upload
                context = context_info.Context()
                context.set_context(contextStr)
                scene = context_info.ContextPathInfo(context=context)

                # project info task trigger table dat for update dependency event
                proj = project_info.ProjectInfo(project=scene.project)
                tasks_trigger_table = proj.task.dependency('scene')

                # sg
                entity = sg_process.get_shot_entity(scene.project, scene.name)
                taskEntity = sg_process.get_one_task_by_step(entity, step, scene.task, create=True)
                output('Uploading to shotgun ... %s' % shotName)
                versionEntity = sg_hook.publish_version(scene, taskEntity, sgStatus, [media], userEntity, description, uploadOption='default')
                # output('Updating status ... %s' % shotName)
                # sg_hook.update_task(scene, taskEntity, sgStatus)
                taskEntity = sg_process.get_one_task_by_step(entity, step, scene.task, create=False)
                status_update.update_dependency_event_api(tasks_trigger_table, taskEntity)
                output('Publish %s finished' % shotName)
                shotData['status'] = True

            output('Publish complete')
        else:
            output('No data to publish')


def output(message):
    print message


class PublishSnap(QtCore.QThread):
    complete = QtCore.Signal(bool)
    status = QtCore.Signal(str)

    def __init__(self, scene):
        super(PublishSnap, self).__init__()
        self.scene = scene
        output = self.output

    def run(self):
        path = snap_file(self.scene)
        data = file_utils.ymlLoader(path) if os.path.exists(path) else dict()
        publish(data)
        self.complete.emit(True)
        timeName = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        newName = '%s/%s.%s' % (os.path.dirname(path), os.path.basename(path), timeName)
        os.rename(path, newName) if os.path.exists(path) else None

    def output(self, message):
        self.status.emit(message)

# sg:
#   mov:
#   - context: projectName::scene:ep01:q0040:pr_ep01_q0040_all:layout:main:maya:pr_ep01_q0040_all_layout_main.v003.TA.ma:work:v003:::none:all:pr:none:none
#     description: ''
#     media: P:/projectName/scene/publ/ep01/pr_ep01_q0040_s0020/layout/main/hero/outputMedia/pr_ep01_q0040_s0020_layout_main.hero.mov

