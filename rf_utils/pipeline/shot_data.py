#Import python modules
import os 
import sys
from collections import OrderedDict
from rf_utils import file_utils
from functools import partial
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import admin

class Config: 
	buildData = 'buildData'
	tempFile = 'temp_data.yml'


def data_path(entity, hero=True): 
	key = 'heroPath' if hero else 'dataPath'
	path = entity.path.scheme(key=key)
	path = path if hero else path.rel_version()
	# version 
	if not hero: 
		version = find_version(path.abs_path())
		entity.context.update(publishVersion=version)
	filename = entity.output_name(outputKey='data', hero=hero)
	dataPath = '%s/%s' % (path.abs_path(), filename)
	return dataPath

def get_shot_data_path(entity):
    dataDir = entity.path.scheme(key='dataPath').abs_path()
    dataFn = entity.output_name(outputKey='shot_data', hero=True)
    path = '%s/%s' %(dataDir, dataFn)
    return path

def get_workspace_data_path(entity):
    dataDir = entity.path.scheme(key='processDataPath').abs_path()
    dataFn = entity.output_name(outputKey='workspace_data', hero=True)
    path = '%s/%s' %(dataDir, dataFn)
    return path
    
def temp_path(entity): 
	path = data_path(entity)
	tempPath = '%s/%s' % (os.path.dirname(path), Config.tempFile)
	return tempPath

def register_status(entity): 
	""" check if temp exists, not register else register """ 
	if os.path.exists(temp_path(entity)): 
		return False 
	if os.path.exists(data_path(entity)): 
		return True 
	else: 
		return False 


def find_version(dirname): 
	version = 'v001'
	if os.path.exists(dirname): 
		files = file_utils.list_file(dirname)
		if files: 
			versions = [search_for_version(a) for a in files]
			maxVersion = sorted([int(a[1:]) for a in versions if a[1:].isdigit()])
			nextVersion = maxVersion[-1] + 1
			version = 'v%03d' % nextVersion

	return version


def write_data(entity, key, data): 
	""" write key data """
	path = temp_path(entity)
	readData = read(entity)
	newData = OrderedDict(readData)
	newData[key] = data
	
	# backup before write 
	# if not newData == readData: 
	# 	bkPath = data_path(entity, hero=False)
	# 	file_utils.copy(path, bkPath)

	# write new data
	os.makedirs(os.path.dirname(path)) if not os.path.exists(os.path.dirname(path)) else None
	return file_utils.ymlDumper(path, newData)

def register(entity): 
	tempPath = temp_path(entity)
	heroPath = data_path(entity)

	tmpData = file_utils.ymlLoader(tempPath) if os.path.exists(tempPath) else OrderedDict()
	heroData = file_utils.ymlLoader(heroPath) if os.path.exists(heroPath) else OrderedDict()

	if tmpData: 
		# write to hero file 
		os.makedirs(os.path.dirname(heroPath)) if not os.path.exists(os.path.dirname(heroPath)) else None
		file_utils.ymlDumper(heroPath, tmpData)

		if not tmpData == heroData: 
			# increment version
			bkPath = data_path(entity, hero=False)
			file_utils.copy(heroPath, bkPath)

		if os.path.exists(tempPath): 
			# os.remove(tempPath)
			admin.remove(tempPath)


def read(entity): 
	""" read key data """
	# try to find temp first 
	path = temp_path(entity)
	if not os.path.exists(path): 
		path = data_path(entity, hero=True)
	readData = file_utils.ymlLoader(path) if os.path.exists(path) else OrderedDict()
	return readData

def read_data(entity, key): 
	data = read(entity)
	return data[key] if key in data.keys() else OrderedDict()


def search_for_version(filename, prefix='v', padding=3): 
    versionNumber = [a[0: padding] for a in filename.split(prefix) if a[0: padding].isdigit()]
    return '%s%s' % (prefix, versionNumber[0]) if versionNumber else ''

