import os
import sys 
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget import dialog

class Password: 
	anim = 'riff1234'
	admin = 'Riff1234'

def anim(password): 
	if password in [Password.anim, Password.admin]: 
		return True 
	return False 

def anim_dialog(): 
	title = 'Enter Admin Password'
	message = 'Enter Password to Unlock'
	messageBox = dialog.PasswordDialog.show(title, message)

	if messageBox.value: 
		password = messageBox.field_value()
		result = anim(password)

		if result: 
			logger.info('Granted')
			return result 

		else: 
			dialog.MessageBox.error('Wrong Password', 'Wrong Password')
