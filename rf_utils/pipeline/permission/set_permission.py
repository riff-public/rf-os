import os
import argparse
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import subprocess
os.environ.get('RFSCRIPT')
py = "%s/core/rf_lib/python/2.7.11/python.exe"% (os.environ.get('RFSCRIPT'))
script = __file__

def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-u', dest='user', type=str, help='The group user to set permission', required=True)
    parser.add_argument('-f', dest='filePath' ,nargs='+', help='The path to the dir to be set permission', default=[])
    parser.add_argument('-p', dest='perm', type=str, help='The permission to set permission', default=True)
    parser.add_argument('-i', dest='inh', type=str, help='The inherit to set permission', default=True)
    return parser

def set_access_permission_by_project(project, user):
    from rf_utils.context import context_info
    from rf_utils import project_info
    context = context_info.Context()
    context.update(project=project)
    projectInfo = project_info.ProjectInfo(context.project)
    rootWork = projectInfo.rootProject +'\\'
    projPath = os.path.join(rootWork, project)
    depList = []

    modes = ['asset', 'scene']
    works = ['work', 'publ']
    for m in modes:
        modePath = os.path.join(projPath, m)
        for w in works:
            workPath = os.path.join(modePath, w)
            if os.path.exists(workPath):
                types = os.listdir(workPath)
                for t in types:
                    typePath = os.path.join(workPath, t)
                    if os.path.exists(typePath):
                        assets = os.listdir(typePath)
                        for asset in assets:
                            assetPath = os.path.join(typePath, asset)
                            if os.path.exists(assetPath):
                                departments = os.listdir(assetPath)
                                for dep in departments:
                                    depPath = os.path.join(assetPath, dep)
                                    if os.path.exists(depPath):
                                        if w == 'publ':
                                            if dep == '_data':
                                                depList.append(depPath)
                                        else:
                                            depList.append(depPath)
    set_permission_cmd(user, filePath=depList)

def set_permission_cmd(user, filePath=[], perm=None, inh=None):
    pathStr = ' '.join(filePath)    
    cmd = '"%s" "%s" -u "%s" -f %s -p %s -i %s'%(py,script,user,pathStr,perm,inh)
    si = subprocess.STARTUPINFO()
    si.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, startupinfo=si)
    stdout, stderr = p.communicate()
    logger.debug(stdout)
    if stderr: 
        logger.error(stderr)

def set_permission_create_project(projectName):
    from rf_utils import project_info
    reload(project_info)
    from rf_utils.context import context_info
    project = project_info.ProjectInfo(projectName)
    project.set_project_env()
    permission = project.permission()
    context = context_info.Context()
    context.update(project=projectName)
    asset = context_info.ContextPathInfo(context)
    for userKey,user in permission.get('user').items():
        user = asset.resolve_pattern(user)
        for perm,paths in  permission[userKey]['project'].items():
            filePath = []
            for path in paths:
                relPath = asset.resolve_pattern(path)
                filePath.append(relPath)
                print('\nSET %s %s ' % (user, perm))
                print(relPath)
            set_permission_cmd(user, filePath, perm)

def main(user, filePath=[], perm=None, inh=None):
    import win32security
    import ntsecuritycon as con
    try:
        userx, domain, type = win32security.LookupAccountName ("", "%s"% user)
    except Exception as e:
        logger.warning('User %s does not exists.' % user)
        return e

    #usery, domain, type = win32security.LookupAccountName ("", "User Y")
    if inh:
        inh = eval(inh)
    for fp in filePath:
        if os.path.exists(fp):
            sd = win32security.GetFileSecurity(fp, win32security.DACL_SECURITY_INFORMATION)
            dacl = sd.GetSecurityDescriptorDacl()   # instead of dacl = win32security.ACL()

            #dacl.AddAccessAllowedAce(win32security.ACL_REVISION, con.FILE_GENERIC_READ | con.FILE_GENERIC_WRITE, userx)
            num_delete = 0
            for index in range(dacl.GetAceCount()):
                ace = dacl.GetAce(index - num_delete)
                if userx == ace[2]:
                    dacl.DeleteAce(index - num_delete)
                    num_delete += 1
            if perm == 'read':
                dacl.AddAccessAllowedAceEx(win32security.ACL_REVISION,win32security.CONTAINER_INHERIT_ACE|win32security.OBJECT_INHERIT_ACE, con.FILE_GENERIC_READ|con.FILE_EXECUTE, userx)
            elif perm == 'write':
                dacl.AddAccessAllowedAceEx(win32security.ACL_REVISION,win32security.CONTAINER_INHERIT_ACE|win32security.OBJECT_INHERIT_ACE, con.FILE_GENERIC_READ|con.FILE_EXECUTE|con.FILE_GENERIC_WRITE, userx)
            elif perm == 'deny':
                dacl.AddAccessDeniedAceEx(win32security.ACL_REVISION,win32security.CONTAINER_INHERIT_ACE|win32security.OBJECT_INHERIT_ACE, con.FILE_ALL_ACCESS, userx)
            else:
                dacl.AddAccessAllowedAceEx(win32security.ACL_REVISION,win32security.CONTAINER_INHERIT_ACE|win32security.OBJECT_INHERIT_ACE, con.FILE_ALL_ACCESS, userx)


            sd.SetSecurityDescriptorDacl(1, dacl, 0)   # may not be necessary
            win32security.SetFileSecurity(fp, win32security.DACL_SECURITY_INFORMATION, sd)

            subFolders = os.listdir(fp)
            if inh:
                if subFolders:
                    logger.debug('Recursively set subfolder to inherit parent permission')
                    for fold in subFolders:
                        path = os.path.join(fp, fold)
                        path = path.replace('/', '\\')
                        cmd = 'icacls "%s" /q /c /t /reset' %(path)
                        logger.debug('- %s' % path)
                        subprocess.call(cmd, shell=True)
            else:
                if perm != 'read' and perm != 'write':
                    if subFolders:
                        logger.debug('Recursively set subfolder to inherit parent permission')
                        for fold in subFolders:
                            path = os.path.join(fp, fold)
                            path = path.replace('/', '\\')
                            cmd = 'icacls "%s" /q /c /t /reset' %(path)
                            logger.debug('- %s' % path)
                            subprocess.call(cmd, shell=True)


if __name__ == "__main__":
    print(':::::: Set Permission ::::::')
    logger.info(':::::: Set Permission::::::')

    parser = setup_parser('Set Permission')
    params = parser.parse_args()
    
    main(user=params.user, 
        filePath=params.filePath,
        perm=params.perm,
        inh=params.inh)