# -- coding: utf-8 --
import requests, json, sys
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
logger.setLevel(logging.INFO)

from rf_utils.pipeline.zoho_connect import *

class Board():
    ''' storage class for board IDs '''
    pipeline = '369390000000306051'

class ZohoConnectTasks(ZohoConnect):
    
    def __init__(self):
        super(ZohoConnectTasks, self).__init__()

    def get_board_sections(self, boardId):
        access_token, headers = self.generate_access_token(scope='tasks_all')
        requestUrl = 'https://connect.zoho.com/pulse/api/boardSections'
        params = {'scopeID': SCOPE_ID, 
                'boardId': boardId}
        res = requests.get(url=requestUrl, headers=headers, params=params)
        logger.debug('Get request status_code: {}'.format(res.status_code))
        if res.status_code:
            res_json = res.json()
            logger.debug(res_json)
            board_sections_info = res_json['boardSections']['sections']
            board_sections = {}  # {section_name: {data}}
            for info in board_sections_info:
                name = info['name']
                values = {}
                for k, v in info.iteritems():
                    if k != 'name':
                        values[k] = v
                board_sections[name] = values
            logger.info('Get board sections success: {}'.format(boardId))
            return board_sections
        else:
            err_msg = 'Error getting board sections on boardId: {}'.format(boardId)
            logger.error(err_msg)
            logger.debug(res.text)
    
    def get_board_sectionId_by_name(self, boardId, section_name):
        board_sections = self.get_board_sections(boardId=boardId)
        for name, info in board_sections.iteritems():
            if name == section_name:
                return info['id']
        else:
            logger.error('Cannot find section of name {} in boardId {}.'.format(section_name, boardId))

    def create_task(self, boardId, sectionId, title, desc='', userIds=[], priority='None', position=0, due_date=[], checkList=[]):
        access_token, headers = self.generate_access_token(scope='tasks_all')
        requestUrl = 'https://connect.zoho.com/pulse/api/addTask'
        params = {'scopeID': SCOPE_ID, 
            'boardId': boardId, 
            'sectionId': sectionId,
            'title': title,
            'userIds': userIds,
            'desc': desc,
            'priority': priority,
            'position': position}

        # add due date (dd, mm, yyyy)
        if due_date:
            params['edate'] = due_date[0]
            params['emonth'] = due_date[1]
            params['eyear'] = due_date[2]

        # add check list, if any
        if checkList:
            params['checkList'] = checkList

        # # add tags, if any
        # if tagNames:
        #     params['tagNames'] = tagNames

        res = requests.post(url=requestUrl, headers=headers, params=params)
        logger.debug('Post request status_code: {}'.format(res.status_code))
        if res.status_code:
            res_json = res.json()
            logger.debug(res_json)
            logger.info('Task created to board: {}'.format(boardId))
        else:
            err_msg = 'Error creating task on boardId: {}'.format(boardId)
            logger.error(err_msg)
            logger.debug(res.text)


'''
from rf_utils.pipeline.zoho_connect import tasks
reload(tasks)

# initialize Connect Task object
ztask = tasks.ZohoConnectTasks()

# get board ID, section ID
boardId = tasks.Board.pipeline
sectionId = ztask.get_board_sectionId_by_name(boardId=boardId, section_name='Etc')

# get user ID
email_address = 'nattapong.n@riff-studio.com'
member_details = ztask.get_member_from_email(emailId=email_address)
userId = member_details['id']

# create a task
ztask.create_task(boardId=boardId, 
    sectionId=sectionId, 
    title='Testing from python API', 
    desc='Yo you wassup!', 
    userIds=[userId], 
    priority='None', 
    position=0)

'''