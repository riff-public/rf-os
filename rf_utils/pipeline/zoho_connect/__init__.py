import os
import json
import requests
from collections import OrderedDict

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
logger.setLevel(logging.INFO)
script_dir = os.path.dirname(__file__)

GENERATE_TOKEN_URL = 'https://accounts.zoho.com/oauth/v2/token'
CLIENT_ID = '1000.RXPVQ15GK7ZW4DBJIHYQOL6CUM3HKU'
CLIENT_SECRET = 'dc613452658dd39db9c0fc31b8c10e6e1d2d55305b'

REDIRECT_URI = 'https://www.connect.riff-studio.com/'
SCOPE_ID = '369390000000002002'  # riff studio's Connect scope id

def read_refresh_token_config():
    config_path = os.path.join(script_dir, 'config.json')
    current_config = {}
    if os.path.exists(config_path):
        with open(config_path, 'r') as read_config:
            try:
                current_config = json.load(read_config)
            except ValueError, e:
                pass
    return current_config

def write_refresh_token_config(refresh_token_config):
    config_path = os.path.join(script_dir, 'config.json')
    with open(config_path, 'w') as write_config:
        json.dump(refresh_token_config, write_config)

def update_refresh_token_config(grant_token, scope):
    refresh_token = generate_refresh_token(grant_token=grant_token)
    current_config = read_refresh_token_config()
    current_config[scope] = refresh_token
    write_refresh_token_config(refresh_token_config=current_config)

def generate_refresh_token(grant_token):
    params = {'code': grant_token, 
        'redirect_uri': REDIRECT_URI, 
        'client_id': CLIENT_ID, 
        'client_secret': CLIENT_SECRET,
        'grant_type': 'authorization_code'}
    res = requests.post(url=GENERATE_TOKEN_URL, params=params)
    logger.debug('Post request status_code: {}'.format(res.status_code))
    if res.status_code:
        res_json = res.json()
        logger.debug(res_json)
        refresh_token = res_json['refresh_token']
        logger.info('refresh_token: {}'.format(refresh_token))
        return refresh_token
    else:
        err_msg = 'Error generating refresh token from: {}'.format(grant_token)
        logger.error(err_msg)
        logger.debug(res.text)
        raise Exception(err_msg) 

class ZohoConnect(object):
    def __init__(self):
        self.refresh_token_configs = None
        self.members = []

        # read token config.json
        self.read_refresh_token_config()

    def read_refresh_token_config(self):
        self.refresh_token_configs = read_refresh_token_config()

    def generate_access_token(self, scope):
        refresh_token = self.refresh_token_configs[scope]

        params = {'refresh_token': refresh_token, 
        'redirect_uri': REDIRECT_URI, 
        'client_id': CLIENT_ID, 
        'client_secret': CLIENT_SECRET,
        'grant_type': 'refresh_token'}

        res = requests.post(url=GENERATE_TOKEN_URL, params=params)
        logger.debug('Post request status_code: {}'.format(res.status_code))
        if res.status_code:
            res_json = res.json()
            logger.debug(res_json)

            access_token = res_json['access_token']
            logger.debug('access_token: {}'.format(access_token))
            headers = {"Authorization": "Zoho-oauthtoken " + access_token, 
                    'contentType':"application/json"}
            return access_token, headers
        else:
            err_msg = 'Error generating access token of scope {} from: {}'.format(scope, refresh_token)
            logger.error(err_msg)
            logger.debug(res.text)
            raise Exception(err_msg) 

    def get_all_network_members(self):
        access_token, headers = self.generate_access_token(scope='networklist_read')

        requestUrl = 'https://connect.zoho.com/pulse/api/orgMembers'
        params = {'scopeID': SCOPE_ID}
        res = requests.get(url=requestUrl, headers=headers, params=params)
        logger.debug('Get request status_code: {}'.format(res.status_code))
        self.members = {}
        if res.status_code:
            res_json = res.json()
            logger.debug(res_json)
            members = res_json['orgMembers']['usersDetails']
            # restructure the data to: {email:{data}}
            for mem in members:
                emailId = mem['emailId']
                values = {}
                for k, v in mem.iteritems():
                    if k != 'emailId':
                        values[k] = v
                self.members[emailId] = values
        else:
            err_msg = 'Error getting network members.'
            logger.error(err_msg)
            logger.debug(res.text)
            raise Exception(err_msg) 

    def get_member_from_email(self, emailId, refresh=False):
        if refresh or not self.members:
            self.get_all_network_members()
        try:
            return self.members[emailId]
        except KeyError:
            logger.error('Cannot find user with emailId: {}'.format(emailId))
