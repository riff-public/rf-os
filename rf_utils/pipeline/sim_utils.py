import os 


def find_sim_asset(entity): 
    from rf_utils.sg import sg_process 
    sg = sg_process.sg 

    simshot = False 
    sim_task = 'sim' 
    sim_types = ['cloth', 'hair']
    sim_dict = dict()
    asset_names = list()

    filters = [['entity.Shot.code', 'is', entity.name], 
        ['project.Project.name', 'is', entity.project], 
        ['step.Step.short_name', 'is', 'sim']]
    fields = ['content', 'sg_status_list']
    tasks = sg.find('Task', filters, fields)

    for sim_type in sim_types: 
        sim_dict[sim_type] = list()

    if tasks: 
        for task in tasks: 
            if sim_task == task['content']: 
                if not task['sg_status_list'] == 'omt': 
                    simshot = True 

        if simshot: 
            for task in tasks: 
                name = task['content']
                # this rely on naming on the first element which should be changed to something more solid
                asset_name = name.split('_')[0]
                for sim_type in sim_types: 
                    if sim_type in name: 
                        sim_dict[sim_type].append(asset_name)
            return sim_dict
        return False
    else: 
        return False  
