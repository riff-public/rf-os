import os
import sys
import logging
import maya.cmds as mc 
import maya.mel as mm

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_maya.lib import camera_lib
from rf_utils.context import context_info 
from rf_utils.sg import sg_process
from rf_maya.rftool.utils import maya_utils
reload(sg_process)
sg = sg_process.sg
from rf_maya.lib import sequencer_lib
reload(sequencer_lib)

moduleDir = os.path.dirname(sys.modules[__name__].__file__)


# DEFAULT_PROJECT = {
#     "type": "Project",
#     "id": 134,
#     "name": "projectName"
# }

# DEFAULT_EPISODE = {'project': {'type': 'Project', 'id': 134, 'name': 'projectName'}, 'code': 'ep03', 'type': 'Scene', 'id': 667}

def get_shots(project, sg_sequence_code, shot_type, episode):
    project_filtered = {
        "type": project["type"],
        "id": project["id"],
        "name": project["name"]
    }

    episode_filtered = {
        "type": episode["type"],
        "id": episode["id"],
        "code": episode["code"]
    }

    shots_filter = [
        ['project', 'is', project_filtered],
        ['sg_episode', 'is', episode_filtered],
        ['sg_sequence_code', 'is', sg_sequence_code],
        ['sg_shot_type', 'is', shot_type]
    ]

    fields = ['id', 'code', 'sg_cut_in', 'sg_cut_out', 'sg_cut_duration']
    shots = sg.find('Shot', shots_filter, fields)

    return shots

def sync_shot(project, episode, sgname): 
    from rf_utils.sg import sg_process
    extraFields = ['sg_cut_in', 'sg_cut_out', 'sg_cut_duration', 'sg_working_duration']
    sgData = sg_process.get_shot_entity(project, sgname, extraFields=extraFields)
    return sgData

# get_shots(DEFAULT_PROJECT, "q0040", "Shot")


def create_sequencer_shots(sequence, rigCamPath): 
    chk = mc.confirmDialog( title='create_sequencer_shots', message='create_sequencer_shots', button=['Yes','No'], defaultButton='Yes', cancelButton='No', dismissString='No',messageAlign= 'center' )
    if chk == 'Yes':
        scene = context_info.ContextPathInfo()
        context = context_info.Context()
        context.use_sg(sg, project=scene.project, entityType='scene', entityName=scene.name)
        scene = context_info.ContextPathInfo(context=context)

        if not mc.objExists('Cam_Grp'):
            setResolution(scene.project)
            mc.setAttr('defaultResolution.deviceAspectRatio', 2.353)
            #mc.setAttr('defaultResolution.pixelAspect', 1)

            # get sg project entity 
            projectEntity = sg_process.get_project(scene.project)
            episodeEntity = sg_process.get_one_episode(scene.project, scene.episode)

            DEFAULT_RANGE = scene.projectInfo.scene.start_frame -1
            DEFAULT_DURATION_SHOT = scene.projectInfo.scene.duration_shot

            extraFields = ['sg_cut_in', 'sg_cut_out', 'sg_cut_duration', 'sg_working_duration', 'sg_shortcode', 'sg_sequence_code', 'sg_status_list']
            orderOption = [{'field_name':'sg_shortcode', 'direction':'asc'}]
            shots = sg_process.get_shot_entities(projectEntity, episodeEntity, sequence, extraFields=extraFields, order= orderOption)
            # shots = get_shots(projectEntity, "q0010", "Shot", DEFAULT_EPISODE)
            start = True
            groupShot = []
            start_seq_time = ''
            for shot in shots:
                if not shot['sg_status_list'] == 'omt':
                    if start:
                        start_time = shot['sg_cut_in'] + DEFAULT_RANGE 
                        start = False
                        
                    shotCode = shot['sg_shortcode']
                    cameraShape = referenceCam(rigCamPath, shotCode, scene)

                    end_time = start_time + shot['sg_working_duration'] - 1
                    ####create shot 
                    cam_seq_shot = camera_lib.create_shot(shotCode, cameraShape, start_time, end_time)
                    print start_time,end_time
                    if start_seq_time:
                        mc.setAttr('%s.sequenceStartFrame'% shotCode, start_seq_time)
                        mc.setAttr('%s.scale'% shotCode, 1)
                    end_seq_time = mc.getAttr('%s.sequenceEndFrame'% shotCode)
                    context.update(entity = shot['code'], step='edit', process='sound')
                    path = scene.path.output_hero().abs_path()
                    filename = scene.publish_name(True, ext='.wav')
                    soundPath = '%s/%s' % (path, filename)

                    #create sound 
                    if os.path.exists(soundPath):
                        sound_name ='%s_sound' %shotCode
                        sound_tranform = mc.sound(file = soundPath, n = sound_name, offset = start_time)
                        mc.sequenceManager(ata=sound_tranform)
                        mc.setAttr('%s.sourceStart'%sound_tranform, 0)
                        mc.setAttr('%s.sourceEnd'%sound_tranform, shot['sg_working_duration'])
                        if start_seq_time:
                            mc.setAttr('%s.offset'%sound_tranform, start_seq_time)
                        else:
                            mc.setAttr('%s.offset'%sound_tranform, start_time)
                    
                        # mc.shot(shotCode, e=True, mute=True)
                    start_time = end_time + DEFAULT_DURATION_SHOT
                    start_seq_time = end_seq_time + 1
        else:
            mc.warning('Already cam grp')
            update_sequencer_shots(sequence, rigCamPath, scene)

            # setResolution(scene.project)
            # mc.setAttr('defaultResolution.pixelAspect', 1)

def referenceCam(rigCamPath, shotCode, scene):
    from rf_utils import project_info
    reload(project_info)
    # proj = project_info.ProjectInfo('Quantum')
    camGroup = scene.projectInfo.asset.get('camGroup')
    ref_in_sence = maya_utils.list_all_ref_namespace()
    namespace = '%s_cam'%shotCode
    cam_shot_grp = '%s:%s'%(namespace, camGroup)
    if not namespace in ref_in_sence:
        if not mc.objExists('Cam_Grp'):
            pathImport = mc.file(rigCamPath, reference=True , type="mayaAscii", namespace=namespace)
            mc.group( em=True, name='Cam_Grp' )
            mc.parent(cam_shot_grp, 'Cam_Grp')
        else:
            pathImport = mc.file(rigCamPath, reference=True , type="mayaAscii", namespace=namespace)
            mc.parent(cam_shot_grp, 'Cam_Grp')
        mc.select('%s:*'%namespace)
        cameraShape = mc.ls(sl=True, type='camera')[0]
        # lock camera pivots
        cam_tr = mc.listRelatives(cameraShape, parent=True, pa=True)[0]
        maya_utils.lockPivots(cam_tr)
        return cameraShape
    else:
        mc.select('%s:*'%namespace)
        cameraShape = mc.ls(sl=True, type='camera')[0]
        return cameraShape


def setResolution(project):
    from rf_utils import project_info
    reload(project_info)
    proj = project_info.ProjectInfo(project)
    proj.render.set_size(mode='preview')

def update_sequencer_shots(sequence, rigCamPath, scene):
    chk = mc.confirmDialog( title='update_sequencer_shots', message='update_sequencer_shots', button=['Yes','No'], defaultButton='Yes', cancelButton='No', dismissString='No',messageAlign= 'center' )
    if chk == 'Yes':
        #list_shot_in_scene
        setResolution(scene.project)
        mc.setAttr('defaultResolution.deviceAspectRatio', 2.353)
        #mc.setAttr('defaultResolution.pixelAspect', 1)
        # get sg project entity 
        projectEntity = sg_process.get_project(scene.project)
        episodeEntity = sg_process.get_one_episode(scene.project, scene.episode)
        DEFAULT_RANGE = 1000
        DEFAULT_DURATION_SHOT = 100
        extraFields = ['sg_cut_in', 'sg_cut_out', 'sg_cut_duration', 'sg_working_duration', 'sg_shortcode', 'sg_sequence_code', 'sg_status_list']
        orderOption = [{'field_name':'sg_shortcode', 'direction':'asc'}]
        shots = sg_process.get_shot_entities(projectEntity, episodeEntity, sequence, extraFields=extraFields, order= orderOption)

        start = True
        groupShot = []
        start_seq_time = ''
        for index, shot in enumerate(shots):
            if start:
                start_time = shot['sg_cut_in'] + DEFAULT_RANGE 
                start = False
            shotCode = shot['sg_shortcode']
            shot_in_scene = sorted(mc.sequenceManager(lsh=True))
            #havn't shot in cam_seq
            if not shotCode in shot_in_scene:
                prevStartFrame, prevEndFrame, prevSeqStartFrame, prevSeqEndFrame = sequencer_lib.shot_info(shot_in_scene[index-1])
                if index < (len(shot_in_scene)):
                    curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = sequencer_lib.shot_info(shot_in_scene[index])
                    clear_space = (curSeqStartFrame - prevSeqEndFrame) -1 
                    duration = shot['sg_working_duration']
                    if duration > clear_space and duration != 0 and index != 0:
                        duration = duration - clear_space # 80 -250 = -170
                        sequencer_lib.shift_shot_seq(shot_in_scene[index] ,duration, duration, False) 
                        sequencer_lib.shift_key(type='start',frameRange=[curStartFrame, 100000], frame = duration)
                cameraShape = referenceCam(rigCamPath, shotCode, scene)
                if index == 0:                
                    start_time = start_time
                    end_time = start_time + shot['sg_working_duration'] - 1
                    seq_start = start_time
                    seq_end = end_time
                else:
                    prevStartFrame, prevEndFrame, prevSeqStartFrame, prevSeqEndFrame = sequencer_lib.shot_info(shot_in_scene[index-1])
                    start_time = prevEndFrame + 100
                    end_time = start_time + shot['sg_working_duration'] - 1
                    seq_start = prevSeqEndFrame +1
                    seq_end = seq_start + shot['sg_working_duration'] - 1
                cam_seq_shot = camera_lib.create_shot(shotCode, cameraShape, start_time, end_time, seq_start, seq_end)
                shot_in_scene_cache = sorted(mc.sequenceManager(lsh=True))
                curStartFrame, curEndFrame, curSeqStartFrame, curSeqEndFrame = sequencer_lib.shot_info(shot_in_scene_cache[index])
                clear_space =(curSeqStartFrame - prevSeqEndFrame) -1 
                duration = duration - clear_space
                if duration < 0: # duration > clear_space:
                    sequencer_lib.shift_shot_seq(shotCode ,duration, duration, False)
                    sequencer_lib.shift_key(type='start',frameRange=[curStartFrame, 100000], frame = duration)
                    offset_first_frame = mc.getAttr('%s.startFrame'%shot_in_scene[0])
                    offset_frame = 1001 - offset_first_frame
                    if offset_frame != 0:
                        sequencer_lib.shift_shot_seq(shot_in_scene[0] ,offset_frame, offset_frame, False)
                        sequencer_lib.shift_key(type='start',frameRange=[1001, 100000], frame = offset_frame)

            # # shot already in secne update duration
            else:
                start_time, end_time, seq_start, seq_end = sequencer_lib.shot_info(shotCode)
                duration = shot['sg_working_duration']
                cur_duration = ((end_time - start_time)+1)
                if duration != cur_duration:
                    if duration < cur_duration:
                        offset_time = duration - cur_duration
                        #180 - 190 = -10
                        seq_end = seq_end + offset_time
                        end_time = end_time + offset_time
                        mc.setAttr('%s.sequenceEndFrame'% shotCode, seq_end)
                        mc.setAttr('%s.endFrame'% shotCode, end_time)
                        mc.setAttr('%s.scale'% shotCode, 1)
                        if shotCode != shot_in_scene[-1]:
                            shot_index = shot_in_scene.index(shotCode)+1
                            sequencer_lib.shift_shot_seq(shot_in_scene[shot_index] ,offset_time, offset_time, False)
                            sequencer_lib.shift_key(type='start',frameRange=[start_time, 100000], frame = offset_time)

                    elif duration > cur_duration:
                        offset_time = duration - cur_duration
                        if shotCode != shot_in_scene[-1]:
                            shot_index = shot_in_scene.index(shotCode)+1
                            print 'duration > cur_duration', offset_time
                            sequencer_lib.shift_shot_seq(shot_in_scene[shot_index] ,offset_time, offset_time, False)
                            sequencer_lib.shift_key(type='start',frameRange=[start_time, 100000], frame = offset_time)
                        seq_end = seq_end + offset_time
                        end_time = end_time + offset_time
                        mc.setAttr('%s.sequenceEndFrame'% shotCode, seq_end)
                        mc.setAttr('%s.endFrame'% shotCode, end_time)
                        mc.setAttr('%s.scale'% shotCode, 1)

                prevStartFrame, prevEndFrame, prevSeqStartFrame, prevSeqEndFrame = sequencer_lib.shot_info(shot_in_scene[index-1])
                clear_space =  seq_start - prevSeqEndFrame
                if clear_space != 1 and index != 0 :
                    sequencer_lib.shift_shot_seq(shot_in_scene[index] ,-(clear_space-1), -(clear_space-1), False)
                    curStart_time, curEnd_time, curSeq_start, curSeq_end = sequencer_lib.shot_info(shotCode)
                    sequencer_lib.shift_key(type='start',frameRange=[start_time, 100000], frame = -(clear_space-1))
                    seq_clear_space = curStart_time - prevEndFrame
                    # print seq_clear_space,'seq_clear_space'
                    # if seq_clear_space != 100:
                    #     mc.setAttr('%s.sequenceStartFrame'%shot_in_scene[index], prevSeqEndFrame+1)
                    

            print index, start_time, end_time, seq_start, seq_end

            #sound part
            scene.context.update(entity = shot['code'], step='edit', process='sound')
            path = scene.path.output_hero().abs_path()
            filename = scene.publish_name(True, ext='.wav')
            soundPath = os.path.join(path, filename).replace('\\', '/')
            if os.path.exists(soundPath):
                sound_name ='%s_sound' %shotCode
                if not mc.objExists(sound_name):
                    sound_tranform = mc.sound(file = soundPath, n = sound_name, offset = start_time)
                    mc.sequenceManager(ata=sound_tranform)
                    mc.setAttr('%s.sourceStart'%sound_tranform, 0)
                    mc.setAttr('%s.sourceEnd'%sound_tranform, shot['sg_working_duration'])
                    mc.setAttr('%s.offset'%sound_tranform, seq_start)
                else:
                    mc.setAttr('%s.sourceStart'%sound_name, 0)
                    mc.setAttr('%s.sourceEnd'%sound_name, shot['sg_working_duration'])
                    mc.setAttr('%s.offset'%sound_name, seq_start)

            if shot['sg_status_list'] == 'omt':
                mc.shot(shotCode, e=True, mute=True)


   