import os 
import sys 
import json
from collections import OrderedDict
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

class ToolSetting(object):
    """ write setting on local TEMP dir for tool """
    def __init__(self, toolName):
        super(ToolSetting, self).__init__()
        self.toolName = toolName
        self.pipelineDir = 'RF_TOOL_PREF'
        self.tmpDir = os.environ['TEMP']
        self.prefFile = '%s/%s/%s.json' % (self.tmpDir, self.pipelineDir, self.toolName)

    def read(self): 
        if os.path.exists(self.prefFile): 
            try: 
                return json_loader(self.prefFile) or dict()
            except Exception as e: 
                logger.error(e)
                return dict()
        return dict()

    def write(self, data): 
        if not os.path.exists(os.path.dirname(self.prefFile)): 
            os.makedirs(os.path.dirname(self.prefFile))
        return json_dumper(self.prefFile, data)

    def reset(self): 
        os.remove(self.prefFile)

def json_dumper(path, data): 
    if not os.path.exists(os.path.dirname(path)): 
        os.makedirs(os.path.dirname(path))
    with open(path, 'w') as outfile:
        json.dump(data, outfile) 
        return path


def json_loader(path): 
    with open(path) as configData: 
        config = json.load(configData)
        return config   
        