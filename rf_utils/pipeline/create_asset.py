import os
import sys
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import create_structure
from rf_utils.context import context_info
from rf_utils import file_utils
from rf_utils.sg import sg_utils
from rf_utils.sg import sg_process
from rf_utils import user_info
sg = sg_utils.sg
moduleDir = os.path.dirname(sys.modules[__name__].__file__)
config = 'template_config.yml'

# create shotgun / create folder structure / create links.

def create(project, name, assetType, assetSubtype='', mainAsset='', tags='', template='', 
            setEntity=[], link=True, episodes=[], sequences=[], shots=[], design_entities=[], 
            looks=[], description='', productionType='Production', omit_steps=[]):
    # create asset dir
    dirs = create_dir(project, name, assetType, mainAsset)

    # create asset in SG
    sgEntity = create_sg(project, name, assetType, assetSubtype, mainAsset, 
            templateKey=template, tags=tags, setEntity=setEntity, 
            episodes=episodes, sequences=sequences, shots=shots, 
            design_entities=design_entities, description=description, productionType=productionType)

    # create additional looks, if any
    if looks:
        for look in looks:
            sg_process.create_look(sgEntity, look)
    ## don't need to create look dir anymore, use render layer in main workspace
    # result = create_look_dir(project, name, assetType, assetId=sgEntity['id'])

    # update Object ID to ASSET
    # sg_process.assign_object_id(project, sgEntity['id']) # removed not use any more

    # update link
    if link:
        update_dir_link(project, name, assetType, mainAsset)

    if omit_steps:
        for step in omit_steps:
            sg_process.set_step_task_status(project, step, name, 'omt', entity_type='Asset')

    return (dirs, sgEntity)


def create_dir(project, name, assetType, mainAsset, look=False):
    context = get_context(project, name, assetType, mainAsset)
    assetDirs = create_structure.create_workspace(context)
    # context.update(entityType='design')
    # designDirs = create_structure.create_workspace(context)
    # if look:
    #     lookResult = create_look_dir(project, name, assetType)
    return assetDirs


# def create_look_dir(project, name, assetType, assetId=None):
#     if not assetId:
#         entity = get_asset_entity(project, name, assetType)
#         assetId = entity.get('id') if entity else None
#     if assetId:
#         return create_structure.create_look(sg, project, assetId)


def create_sg(project, name, assetType, assetSubtype, mainAsset, templateKey, 
            setEntity=None, tags=None, episodes=[], sequences=[], shots=[], 
            design_entities=[], description='', productionType='Production'):
    """ create asset sg """
    entity = get_asset_entity(project, name, assetType)
    projectEntity = sg.find_one('Project', [['name', 'is', project]], ['name', 'id'])
    template = get_template(project, templateKey, productionType)
    data = {'project': projectEntity, 'code': name, 'sg_asset_type': assetType,
            'sg_subtype': assetSubtype, 'sg_mainasset': mainAsset, 'task_template': template, 
            'description': description, 'sg_production_type': productionType}
    # parent asset
    if setEntity:
        data.update({'parents': setEntity})
    # tags
    if tags:
        valid_tags = add_tags(tags=tags)
        data.update({'tags': valid_tags})
    # episodes
    if episodes:
        data.update({'sg_scenes': episodes})
    # sequences
    if sequences: 
        data.update({'sequences': sequences})
    # shots
    if shots:
        data.update({'shots': shots})
    # parent design
    if design_entities:
        data.update({'sg_link_designs': design_entities})
    # user
    user = user_info.User().sg_user()
    if user:
        data.update({'created_by': user})

    # create if not exist
    if not entity:
        result = sg.create('Asset', data)
        logger.debug(result)
    else:  # update if exists
        result = sg.update('Asset', entity['id'], data)
        logger.warning('"%s" exists, update data only' % name)
    return result

def add_tags(tags):
    all_tags = sg.find('Tag',[],['name'])
    all_ids = [t['id'] for t in all_tags]
    existing_tags, existing_names_lower = [], []
    for tag in all_tags:
        tag_name = tag['name']
        # try to encode tag name to filter out non-English tags
        try:
            tag_name.encode(encoding='utf-8').decode('ascii')
            existing_tags.append(tag)
            existing_names_lower.append(tag_name.lower())
        except UnicodeDecodeError:
            pass

    valid_tags = []
    for tag in tags:
        if isinstance(tag, dict):
            tag_id = tag.get('id')
            # check for existance
            if tag_id not in all_ids:
                tag = sg.create('Tag', {'name':tag['name']})
                valid_tags.append(tag)
            else:
                index = all_ids.index(tag_id)
                valid_tags.append(all_tags[index])
        elif isinstance(tag, (str, unicode)):
            tag_name_lower = tag.lower()
            if tag_name_lower not in existing_names_lower:
                print(tag_name_lower)
                print(existing_names_lower)
                tag = sg.create('Tag', {'name':tag})
                valid_tags.append(tag)
            else:
                index = existing_names_lower.index(tag_name_lower)
                valid_tags.append(existing_tags[index])
    return valid_tags

def link_asset_relation(id, episodes=[], sequences=[], shots=[], parents=[], tags=[], designs=[], description=''): 
    data = dict()
    data.update({'sg_scenes': episodes}) if episodes else None 
    data.update({'sequences': sequences}) if sequences else None 
    data.update({'shots': shots}) if shots else None 
    data.update({'parents': parents}) if parents else None 
    data.update({'tags': tags}) if tags else None
    data.update({'description': description}) if description else None
    data.update({'sg_link_designs': designs}) if designs else None
    return sg.update('Asset', id, data)


def update_dir_link(project, name, assetType, mainAsset):
    context = get_context(project, name, assetType, mainAsset)
    display = 'Open'

    # sg entity
    entity = get_asset_entity(project, name, assetType)

    # asset dir
    asset = context_info.ContextPathInfo(context=context)
    assetDir = '%s/' % str(asset.path.name().abs_path()).replace('/', '\\')
    asset_sg_field = 'sg_asset_dir'
    link1 = link_local_file('Asset', entity['id'], asset_sg_field, display, assetDir)

    # design dir
    asset.context.update(entityType='design')
    designDir = '%s/' % str(asset.path.name().abs_path()).replace('/', '\\')
    design_sg_field = 'sg_design_dir'
    link2 = link_local_file('Asset', entity['id'], design_sg_field, display, designDir)
    return (link1, link2)


def get_asset_entity(project, name, assetType):
    projectEntity = sg.find_one('Project', [['name', 'is', project]], ['name', 'id'])
    entity = sg.find_one('Asset', [['project', 'is', projectEntity], ['code', 'is', name], ['sg_asset_type', 'is', assetType]], ['code', 'id'])
    return entity


def get_template(project, templateKey='default', productionType='Production'):
    """ get task template from project config """
    path = '%s/%s' % (moduleDir, config)
    data = file_utils.ymlLoader(path)
    projectData = data.get(project, data.get('Default'))
    templateData = projectData[productionType]['Asset']
    if templateKey in templateData.keys(): 
        template = projectData[productionType]['Asset'][templateKey]
    else: 
        template = projectData[productionType]['Asset']['default']
    return template


def get_context(project, name, assetType, mainAsset):
    context = context_info.Context()
    context.update(project=project, entityType='asset', entity=name, entityGrp=assetType, entityParent=mainAsset)
    return context


def link_local_file(entityType, id, sg_field, display, path):
    path = path.replace('/', '\\')
    data = {sg_field: {'link_type': "local", 'local_path': path, 'name': display}}
    return sg.update(entityType, id, data)
