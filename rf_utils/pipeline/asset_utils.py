import os
import sys 
from rf_utils.context import context_info
from rf_utils.pipeline import create_asset
from rf_utils import file_utils
from rf_utils import admin
from rf_utils.sg import sg_process
sg = sg_process.sg


class Config: 
	readable_exts = ['.ma', '.json', '.yml']


def copy_to_project(project, asset_name, dst_project, new_name=None): 
	context = context_info.Context()
	context.use_sg(sg, project=project, entityType='asset', entityName=asset_name)
	src_asset = context_info.ContextPathInfo(context=context)
	print(context.sgEntity)

	# check dst asset 
	new_name = asset_name if not new_name else new_name
	context2 = context_info.Context()
	context2.use_sg(sg, project=dst_project, entityType='asset', entityName=new_name)
	dst_asset = context_info.ContextPathInfo(context=context2)
	
	print(context2.sgEntity)
	print('---------------------')
	if not context2.sgEntity: 
		# create asset 
		create_asset.create(dst_project, new_name, src_asset.type)
		context2.use_sg(sg, project=dst_project, entityType='asset', entityName=new_name)
		dst_asset = context_info.ContextPathInfo(context=context2)

	# copy hero 
	hero_files = copy_hero(src_asset, dst_asset)
	return 
	# copy _data
	datafile = list_data(src_asset)
	# copy hero publish 
	process_files = copy_hero_process(src_asset, datafile)
	# copy texture 
	dirs = copy_texture_dirs(src_asset)
	# copy latest workspace
	print('hero_files')
	for i in hero_files: 
		print(i)

	print('hero_files')
	for i in datafile: 
		print(i)

	print('process_files')
	for i in process_files: 
		print(i)

	print('texture')
	for i in dirs: 
		print(i)

def copy_hero(src_asset, dst_asset): 
	# list hero files 
	failed = list()
	src_hero = src_asset.path.hero().abs_path()
	files = file_utils.list_file(src_hero)
	dst_hero = dst_asset.path.hero().abs_path()

	# copy 
	for i, file in enumerate(files): 
		src = '{}/{}'.format(src_hero, file)
		dst = '{}/{}'.format(dst_hero, file)
		print(os.path.exists(src))
		print('Copying ({}/{}) to {}'.format(i+1, len(files), dst))
		result = file_utils.copy(src, dst)
		if not result: 
			failed.append(src)

	if failed: 
		print('Failed files: ', failed)

	return files


def list_data(asset): 
	data = asset.path.scheme(key='asmDataPath').abs_path()
	# files = file_utils.list_file(data)
	filename = asset.output_name(outputKey='ad', hero=True)
	data_file = '{}/{}'.format(data, filename)
	return data_file


def copy_hero_process(asset, datafile=None): 
	if datafile: 
		data_dict = file_utils.ymlLoader(datafile)
		process_hero_files = find_key_value(data_dict, 'heroFile')
		return process_hero_files


def copy_texture_dirs(asset): 
	texren = asset.path.scheme(key='publishTexturePath').abs_path()
	texgroom = asset.path.scheme(key='publishGroomTexturePath').abs_path()
	texpreview = asset.path.scheme(key='previewTexturePath').abs_path()

	return [texren, texgroom, texpreview]


def find_key_value(dict_data, kw): 
	values = list()
	for k, v in dict_data.items(): 
		if isinstance(v, str): 
			if k == kw:
				values.append(v) 
		if isinstance(v, dict): 
			val = find_key_value(v, kw)
			# print(val)
			values.extend(val)
		if isinstance(v, list): 
			for item in v: 
				if isinstance(item, dict): 
					val = find_key_value(item, kw)
					values.extend(val)
	return values

