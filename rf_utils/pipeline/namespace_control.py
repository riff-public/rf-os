""" 
This module control namespace per shot basis to make sure each parallel departments 
get a unique namespace 
This module will control distribution of namespaces on each referencing

"""
import os 
import sys
from rf_utils import file_utils 
from rf_utils.context import context_info
from collections import OrderedDict
from datetime import datetime
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class NamespaceControl(object):
	"""docstring for NamespaceControl"""
	def __init__(self, entity=None):
		super(NamespaceControl, self).__init__()
		self.entity = entity if entity else self.get_entity()
		self.data = self.read() if self.entity.valid else OrderedDict()

	def get_entity(self): 
		return context_info.ContextPathInfo()
		
	def datafile(self): 
		""" get a data file per shot """ 
		subdir = 'namespaceData'
		dataDir = '{}/{}'.format(self.entity.path.data().abs_path(), subdir)
		filename = self.entity.output_name('namespaceData', hero=True)
		datafile = '{}/{}'.format(dataDir, filename)
		return datafile 

	def data_format(self, workspace, refPath, user): 
		""" format key data """ 
		keyData = OrderedDict()
		keyData['workspace'] = workspace 
		keyData['path'] = refPath 
		keyData['user'] = user 
		keyData['time'] = datetime.now().strftime("%Y %b %d, %H:%M:%S") 
		return keyData

	def add_record(self, namespace, workspace, refPath, user, override=False): 
		dataFormat = self.data_format(workspace, refPath, user)
		writeData = True if not namespace in self.data.keys() else True if override else False 

		if writeData: 
			self.data[namespace] = dataFormat
			self.data = self.write(self.data)
			logger.info('Write namespace record "{}" successfully'.format(namespace))
			return dataFormat

		else: 
			logger.debug('Skip. Key existed "{}" - {}'.format(namespace, self.data[namespace]))

	def list_namespaces(self): 
		if hasattr(self.data, 'keys'): 
			return self.data.keys()
		return list()
	
	def read(self): 
		dataPath = self.datafile()
		data = OrderedDict()
		if os.path.exists(dataPath): 
			data = file_utils.ymlLoader(dataPath)
		else: 
			logger.debug('Data not exists "{}"'.format(dataPath))
		return data 

	def write(self, data): 
		dataPath = self.datafile()
		dirpath = os.path.dirname(dataPath)
		if not os.path.exists(dirpath): 
			os.makedirs(dirpath)

		file_utils.ymlDumper(dataPath, data)
		return data

	def sorted(self): 
		sortOrder = OrderedDict()
		for k in sorted(self.data.keys()): 
			sortOrder[k] = self.data[k]


def test_run(): 
	from rf_utils.context import context_info
	reload(context_info)
	path = 'P:/projectName/scene/work/ep01/pr_ep01_q040a_s0130/setdress/main/maya/test.ma'
	entity = context_info.ContextPathInfo(path=path)
	ncontrol = NamespaceControl(entity)
	ncontrol.add_record('arthur_004', 'P:/', 'P:/', 'TA')

	# output 
	# Result: 'P:/projectName/scene/publ/ep01/pr_ep01_q040a_s0130/_data/namespaceData/pr_ep01_q040a_s0130_namespaceData.hero.yml' # 
