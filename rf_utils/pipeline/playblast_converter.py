import os
import time
from datetime import datetime
import subprocess
import sys

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils.pipeline import watermark
from rf_utils.widget.status_widget import Icon
# reload(watermark)

from PIL_Maya import Image, ImageFont, ImageDraw
if 'PIL_Maya.PngImagePlugin' in logging.Logger.manager.loggerDict.keys():
    logging.Logger.manager.loggerDict['PIL_Maya.PngImagePlugin'].setLevel(logging.INFO)


FONT_SIZE = 26

si = subprocess.STARTUPINFO()
si.dwFlags |= subprocess.STARTF_USESHOWWINDOW

CRF_QUALITY = {'poor': 30, 
            'low': 25,
            'medium': 22,
            'high': 17,
            'ultra': 10}

# images_path = "./images3"
# animation_name = "SevenChickMovie"
# user = "TA"
# department = "Layout"
# file_name = "test_scm_act_q0010_all_anim_v001.ma"
# shot = "s0010"

def get_images(images_path, ext ='.png'):
    logger.debug(images_path)
    logger.debug(('files', sorted(filter(lambda x: os.path.splitext(x)[1] == ext, os.listdir(images_path)))))
    img_list = sorted(filter(lambda x: os.path.splitext(x)[1] == ext, os.listdir(images_path)))
    frame_list = sorted([int(x.split('.')[1]) for x in img_list])

    # shot_name = img_list[0].split('.')[0]
    # for i in range(len(frame_list)):
    #     img_name = '{}.{}{}'.format(shot_name,frame_list[i],ext)
    #     frame_list[i] = img_name
    # return frame_list

    img_sorted_list = []
    for fr in frame_list:
        for img in img_list:
            if img.split('.')[1] == str(fr):
                img_sorted_list.append(img)
    return img_sorted_list


def burnin_text(
    images_path,
    animation_name,
    user,
    department,
    file_name,
    shot,
    compWidth,
    compHeight,
    focalLength="unknown"
):
    files = get_images(images_path)
    end_file_name, end_frame, end_ext = files[-1].split('.')
    for idx, file in enumerate(files):
        image_path = "{images_path}/{file}".format(images_path=images_path, file=file)
        modified_file_time = time.strftime('%d %b %Y %H.%M.%S', time.gmtime(os.path.getmtime(image_path)))
        filename, current_frame, ext = file.split('.')

        old_im = Image.open(image_path)
        old_img_size = old_im.size

        new_size = (compWidth, compHeight)
        new_im = Image.new("RGB", new_size)

        font = ImageFont.truetype("arial.ttf", FONT_SIZE)
        user_text = "user: {}".format(user)
        department_text = "department: {}".format(department)
        file_name_text = "file: {}".format(file_name)
        shot_text = "shot: {}".format(shot)
        date_text = "Date: {}".format(modified_file_time)
        frame_text = "frame: {current_num_frame}/{end_num_file}".format(current_num_frame=idx+1, end_num_file=len(files))
        frame_image_text = "( {current_frame}/{end_frame} )".format(
            current_frame=current_frame,
            end_frame=end_frame
        )
        focal_length_text = "focalLength: {focalLength}".format(focalLength=str(focalLength))

        new_im.paste(old_im, ((new_size[0] - old_img_size[0]) / 2, (new_size[1] - old_img_size[1]) / 2))
        
        draw = ImageDraw.Draw(new_im)
        draw.text((80, 70), animation_name, (255, 255, 255), font=font)
        draw.text((compWidth * 0.8, 70), date_text, (255, 255, 255), font=font)

        draw.text((80, compHeight - 120), user_text, (255, 255, 255), font=font)
        draw.text((80, compHeight - 80), department_text, (255, 255, 255), font=font)
        draw.text((80, compHeight - 40), file_name_text, (255, 255, 255), font=font)
        draw.text((compWidth * 0.47, compHeight - 120), shot_text, (255, 255, 255), font=font)

        draw.text((compWidth * 0.9, compHeight - 120), frame_text, (255, 255, 255), font=font)
        draw.text((compWidth * 0.9, compHeight - 80), frame_image_text, (255, 255, 255), font=font)
        draw.text((compWidth * 0.9, compHeight - 40), focal_length_text, (255, 255, 255), font=font)

        new_im.save('{images_path}/{file}'.format(images_path=images_path, file=file), quality=100)
        # print 'save %s', file

def burnin_text2(images_path, animation_name, user, department, file_name, shot, compWidth, compHeight, start_date='N/A', due_date='N/A', finalcam_status='N/A', focalLength="unknown"):
    files = get_images(images_path)
    end_file_name, end_frame, end_ext = files[-1].split('.')

    # ---------- burn in contents
    # user
    user_text = "user: {}".format(user)
    # departemnt
    department_text = "department: {}".format(department)
    # shot name
    shot_text = "shot: {}".format(shot)
    # focal length
    focal_length_text = "focalLength: {focalLength}".format(focalLength=str(focalLength))

    # start date
    start_date_text = 'Start: N/A'
    if start_date not in ('', None, 'N/A'):
        try:
            dt = datetime.strptime(start_date, '%Y-%m-%d').date()
            start_date = dt.strftime('%d %b %Y')
        except Exception:
            start_date = 'N/A'
        start_date_text = 'Start: {}'.format(start_date)

    # due date
    due_date_text = 'Due: N/A'
    if due_date not in ('', None, 'N/A'):
        
        try:
            dt = datetime.strptime(due_date, '%Y-%m-%d').date()
            due_date = dt.strftime('%d %b %Y')
        except Exception:
            due_date = 'N/A'
        due_date_text = 'Due: {}'.format(due_date)
    start_due_date_text = '{} {}'.format(start_date_text, due_date_text)

    # finalcam status
    finalcam_status_text = '** Final Cam Status: N/A **'
    if finalcam_status in Icon.statusMap.keys():
        finalcam_status_text = 'Final Cam Status: {}'.format(Icon.statusMap[finalcam_status]['display'])
        if finalcam_status != 'apr':
            finalcam_status_text = '** ' + finalcam_status_text + ' **'


    for idx, file in enumerate(files):
        image_path = "{images_path}/{file}".format(images_path=images_path, file=file)
        modified_file_time = time.strftime('%d %b %Y %H.%M.%S', time.localtime(os.path.getmtime(image_path)))
        filename, current_frame, ext = file.split('.')

        # overlay text on top of the new image
        file_name_text = "file: {}".format(file_name)
        
        date_text = "Date: {}".format(modified_file_time)
        frame_text = "frame: {current_num_frame}/{end_num_file}".format(current_num_frame=idx+1, end_num_file=len(files))
        frame_image_text = "( {current_frame}/{end_frame} )".format(
            current_frame=current_frame,
            end_frame=end_frame
        )

        res = watermark.add_border_hud_to_image(input_path=image_path, 
                                            output_path=image_path,
                                            topLeft=[animation_name],
                                            topMid=[finalcam_status_text],
                                            topRight=[date_text, start_due_date_text],
                                            bottomLeft=[user_text, 
                                                    department_text, 
                                                    file_name_text],
                                            bottomMid=[shot_text],
                                            bottomRight=[frame_text, 
                                                        frame_image_text,
                                                        focal_length_text],
                                            canvas=[compWidth, compHeight])
        # logger.debug('Burnin complete: {}'.format(image_path))

def make_vdo_mov(images_path, wav_path, filename, framerate='24', ext_file ='.png', quality='22'):
    mov_name, ext = os.path.splitext(filename)
    files = get_images(images_path, ext=ext_file)
    # print files
    first_file_name, first_frame, first_ext = files[0].split('.')
    wav_path = '' if wav_path == None else wav_path
    # print "mov_name", mov_name
    # print "wav_path", wav_path
    # print "sound_offset", sound_offset
    output_path = '{images_path}/{mov_name}.mov'.format(images_path=images_path, mov_name=mov_name)
    if os.path.exists(wav_path):
        ps = subprocess.Popen([
            config.Software.ffmpeg,
            '-y',
            '-r',
            '{framerate}'.format(framerate=framerate),
            '-start_number',
            '{first_frame}'.format(first_frame=first_frame),
            '-i',
            '{images_path}/{first_file_name}.%04d.{first_ext}'.format(images_path=images_path, first_file_name=first_file_name, first_ext=first_ext),
            '-ss',
            '00:00:00.00',
            '-i',
            '{wav_path}'.format(wav_path=wav_path),
            '-vcodec',
            'libx264',
            '-vprofile',
            'baseline',
            '-crf',
            '{quality}'.format(quality=quality),
            '-bf',
            '0',
            '-pix_fmt',
            'yuv420p',
            '-f',
            'mov',
            output_path
        ],shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, startupinfo=si)
        output = ps.communicate()[0]
        print output
    else:
        ps = subprocess.Popen([
            config.Software.ffmpeg,
            '-y',
            '-r',
            '{framerate}'.format(framerate=framerate),
            '-start_number',
            '{first_frame}'.format(first_frame=first_frame),
            '-i',
            '{images_path}/{first_file_name}.%04d.{first_ext}'.format(images_path=images_path, first_file_name=first_file_name, first_ext=first_ext),
            '-vcodec',
            'libx264',
            '-vprofile',
            'baseline',
            '-crf',
            '{quality}'.format(quality=quality),
            '-bf',
            '0',
            '-pix_fmt',
            'yuv420p',
            '-f',
            'mov',
            output_path
        ],shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, startupinfo=si)
        output = ps.communicate()[0]
        print output
    return output_path

def remove_images(images_path):
    for file in get_images(images_path):
        if file.endswith('.png') or file.endswith('.jpg'):
                os.remove("{images_path}/{file}".format(images_path=images_path, file=file))

def playblast_convert(
    images_path,
    wav_path,
    animation_name,
    user,
    department,
    file_name,
    shot,
    compWidth,
    compHeight,
    start_date='N/A',
    due_date='N/A',
    finalcam_status='N/A',
    showHud=True,
    focalLength='unknown',
    framerate = '24',
    ext='.png',
    quality='medium'
):
    images_path = images_path.replace("\\", "/")
    wav_path = wav_path.replace("\\", "/") if wav_path else ''
    # print "Burnin text ...."
    logger.debug('Burning text ...')
    if showHud:
        burnin_text2(images_path=images_path, 
            animation_name=animation_name, 
            user=user, 
            department=department, 
            file_name=file_name, 
            shot=shot, 
            compWidth=compWidth, 
            compHeight=compHeight, 
            start_date=start_date, 
            due_date=due_date, 
            finalcam_status=finalcam_status, 
            focalLength=focalLength)

    logger.debug('Rendering video ...')
    video_path = make_vdo_mov(images_path, wav_path, file_name, framerate, ext_file= ext, quality=CRF_QUALITY[quality])

    # video_path = "%s/%s" % (images_path, file_name)
    print "Render finished : %s" %video_path

    return video_path

def merge_img_seq_to_mov(images_path, filename,  framerate='24',ext_file ='.png'):
    mov_name, ext = os.path.splitext(filename)
    # print images_path, ext_file
    files = get_images(images_path, ext=ext_file)
    # print files[0]
    first_file_name, first_frame, first_ext = files[0].split('.')
    # print first_file_name, first_frame, first_ext
    # print "mov_name", mov_name
    subprocess.call([
        config.Software.ffmpeg,
        '-y',
        '-r',
        '{framerate}'.format(framerate=framerate),
        '-start_number',
        '{first_frame}'.format(first_frame=first_frame),
        '-i',
        '{images_path}/{first_file_name}.%04d.{first_ext}'.format(images_path=images_path, first_file_name=first_file_name, first_ext=first_ext),
        '-vcodec',
        'libx264',
        '-vprofile',
        'baseline',
        '-crf',
        '22',
        '-bf',
        '0',
        '-pix_fmt',
        'yuv420p',
        '-f',
        'mov',
        '{images_path}/{mov_name}.mov'.format(images_path=images_path, mov_name=mov_name)
    ], startupinfo=si)
    return '{images_path}/{mov_name}.mov'.format(images_path=images_path, mov_name=mov_name)


# def add_wav_to_vdo(images_path, wav_path):
#     vdo_file = os.listdir(images_path)[0]
#     print "vdo_file %s", vdo_file
#     file_name, ext = vdo_file.split('.')
#     print "file_name %s", file_name
#     subprocess.call([
#         'O:/Pipeline/lib/ffmpeg-20161227-0ff8c6b-win64-static/bin/ffmpeg.exe',
#         '-i',
#         '{images_path}/{vdo_file}'.format(images_path=images_path, vdo_file=vdo_file),
#         '-i',
#         '{wav_path}'.format(wav_path=wav_path),
#         '-codec',
#         'copy',
#         '-shortest',
#         '{images_path}/{file_name}_wav.mov'.format(images_path=images_path, file_name=file_name)
#     ])


def offset_audio(wav_path, dst_path, offsetTime, endTime):
    subprocess.call([
        config.Software.ffmpeg,
        '-y',
        '-ss',
        '{offsetTime}'.format(offsetTime=offsetTime),
        '-t',
        '{endTime}'.format(endTime=endTime),
        '-i',
        '{wav_path}'.format(wav_path=wav_path),
        '{dst_path}'.format(dst_path=dst_path)
    ], startupinfo=si)
    return dst_path
    ######### offsetTime and endTime  must be timecode patter ex: 00:00:00.02 (.02 is frame_Count)

# def convert_video_to_wav(src_video_path, dst_sound_path):
#     subprocess.call([
#         'O:/Pipeline/lib/ffmpeg-20161227-0ff8c6b-win64-static/bin/ffmpeg.exe',
#         '-y',
#         '-i',
#         '{src_video_path}'.format(src_video_path=src_video_path),
#         '-vn',
#         '-acodec',
#         'pcm_s16le',
#         '{dst_sound_path}'.format(dst_sound_path=dst_sound_path)
#     ])
#     return dst_sound_path

# convert_video_to_wav("C:/Users/pawaris.n/Desktop/scm_act1_q0050_s0410_layout_Nuke.v016.mov", "C:/Users/pawaris.n/Desktop/scm_act1_q0050_s0410_layout_Nuke.v016.wav")

# img_path = "C:/Users/teeruk.i/tutorial/burnintext/images_tests"
# wav_path = "C:/Users/teeruk.i/projects/rf_pipleline_tools/core/rf_app/edit/xml_view/split_shots_media/q0010/s0010/s0010.wav"
# ffmpeg -i video.avi -i C:\Users\teeruk.i\projects\rf_pipleline_tools\core\rf_app\edit\xml_view\split_shots_media\q0010\s0010\s0010.wav -codec copy -shortest test_wav.mov
