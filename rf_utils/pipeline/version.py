import os
import sys


def find_version(filename):
	basename = os.path.basename(filename)
	for a in basename.split('_'):
		for b in a.split('.'):
			if b[0] == 'v' and b[1:].isdigit():
				return b


def max_version(path):
	files = [('/').join([path, a]) for a in os.listdir(path) if os.path.isfile(os.path.join(path, a))]
	if files:
		maxVersion = sorted([find_version(a) for a in files])[-1]
		return maxVersion


def increment_version(path):
	maxVersion = max_version(path)
	incrementVersion = 'v001'
	if maxVersion:
		nextVersion = version_int(maxVersion) + 1
		incrementVersion = version(nextVersion)
	return incrementVersion


def version(num):
	return 'v%03d' % num

def version_int(version):
	if version[1:].isdigit():
		return int(version[1:])