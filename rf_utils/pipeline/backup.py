import os 
import sys 
import re
from rf_utils.sg import sg_utils
from rf_utils.context import context_info
from rf_utils import file_utils
sg = sg_utils.sg 


def extract_asset_hero(project, dst='', copy=False): 
    """ 
    This will only list final publish output of the asset 
    - hero path 
    - texture path 
    """ 
    asset_entities = list_asset_entities(project)
    context = context_info.Context()
    context.use_sg(sg, project, entityType='asset', entity=asset_entities[0])
    asset = context_info.ContextPathInfo(context=context)
    
    for i, entity in enumerate(asset_entities): 
        asset_type = entity.get('sg_asset_type')
        asset.context.update(entity=entity.get('code'), entityGrp=asset_type)
        copy_dirs = []

        if not asset_type == 'set': 
            hero_path = asset.path.hero().abs_path()
            texture_path = ('/').join(asset.path.scheme(key='publishTexturePath').abs_path().split('/')[0:-3])
            copy_dirs.append(hero_path)
            copy_dirs.append(texture_path)

        if asset_type == 'set': 
            asset.context.update(step='set', process='main', app='maya')
            workspace_hero_path = asset.path.scheme(key='publishHeroWorkspacePath').abs_path()
            copy_dirs.append(workspace_hero_path)

        print 'Copying {}/{} items {}'.format(i+1, len(asset_entities), asset.path.hero())
        for src_path in copy_dirs: 
            if os.path.exists(src_path): 
                dst_path = get_dst_path(src_path, dst)
                if not os.path.exists(dst_path): 
                    print '\t Copy content {}'.format(dst_path)
                    if copy: 
                        file_utils.xcopy_directory(src_path, dst_path)
                else: 
                    print '\t Content exists {}'.format(dst_path)
            else: 
                print '\t src not exists {}'.format(src_path)


def extract_shot_hero(project, dst='', copy=False, episodes=[]): 
    """ 
    This will copy only anim / light workspace and output cache 
    """ 
    shots = list_shot_paths(project, episodes)
    for i, shot in enumerate(shots): 
        copy_files = []
        scene = context_info.ContextPathInfo(path=shot)
        # copy anim workspace 
        anim_file = get_anim_workspace(scene)
        copy_files.append(anim_file) if anim_file else None

        light_file = get_light_workspace(scene)
        copy_files.append(light_file) if light_file else None

        output_path = scene.path.scheme(key='outputPath').abs_path()
        copy_files.append(output_path)

        if copy_files: 
            print 'Copying {}/{} items {}'.format(i+1, len(shots), os.path.basename(shot))
            for src_path in copy_files: 
                dst_path = get_dst_path(src_path, dst)
                if os.path.isdir(src_path): 
                    print '\t Copy output content {}'.format(src_path)
                    if copy: 
                        if not os.path.exists(dst_path): 
                            file_utils.xcopy_directory(src_path, dst_path)
                        else: 
                            print '\t Skip exists'

                if os.path.isfile(src_path): 
                    print '\t Copy file content {}'.format(src_path)
                    if copy: 
                        if not os.path.exists(dst_path): 
                            file_utils.copy(src_path, dst_path)
                        else: 
                            print '\t Skip exists'


def get_anim_workspace(scene): 
    scene.context.update(step='anim', process='main', app='maya')
    workspace_path = scene.path.workspace().abs_path()

    if os.path.exists(workspace_path): 
        anim_file = find_latest_versions(workspace_path)
        return anim_file


def get_light_workspace(scene): 
    scene.context.update(step='light', process='main', app='maya')
    workspace_path = scene.path.workspace().abs_path()

    if os.path.exists(workspace_path): 
        anim_file = find_latest_versions(workspace_path)
        return anim_file


def find_latest_versions(path): 
    files = file_utils.list_file(path)
    if files: 
        versions = [re.findall(r'v\d{3}', a)[0] for a in files if re.findall(r'v\d{3}', a)]
        highest = sorted(versions)[-1] if versions else ''
        pickFiles = [a for a in files if highest in a]
        pickFile = pickFiles[0] if pickFiles else files[-1]
        return '{}/{}'.format(path, pickFile)



def get_dst_path(path, dst): 
    path_without_drive = os.path.splitdrive(path)[-1]
    dst_path = '{}/{}'.format(dst, path_without_drive)
    return dst_path



def list_asset_entities(project, asset_type=''): 
    """ this will list all the asset in the project """ 
    filters = [['project.Project.name', 'is', project]]
    filters.append(['sg_asset_type', 'is', asset_type]) if asset_type else None
    fields = ['code', 'id', 'sg_asset_type', 'project']

    assets = sg.find('Asset', filters, fields)
    return assets 


def list_shot_paths(project, episodes=[]): 
    """ this will list all the asset in the project """ 
    ep_path = 'P:/{project}/scene/work'.format(project=project)
    eps = file_utils.list_folder(ep_path)
    all_shots = []

    for ep in eps: 
        check = True if episodes else False 
        con = True 
        if check: 
            con = False 
            if ep in episodes: 
                con =True

        if con: 
            shot_path = '{}/{}'.format(ep_path, ep)
            print 'shot_path', shot_path
            shots = sorted(file_utils.list_folder(shot_path))

            for i, shot in enumerate(shots): 
                if len(shot.split('_')) >= 4: 
                    path = '{}/{}'.format(shot_path, shot)
                    print '{}/{} {}'.format(i+1, len(shots), path)
                    all_shots.append(path)

    return all_shots 
