import threading
import subprocess


def xcopy_file(src, dst):
    cmd = "echo f | xcopy {src} {dst}".format(src=src, dst=dst)
    ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = ps.communicate()[0]
    print output


def xcopy_directory(src, dst):
    cmd = "echo d | xcopy /s {src} {dst}".format(src=src, dst=dst)
    ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = ps.communicate()[0]
    print output



# command = ["C:/Program Files/Shotgun/RV-7.2.2/bin/rv.exe"]
class ThreadSubprocess(threading.Thread):
    def __init__(self, command):
        self.stdout = None
        self.stderr = None
        self.command = command
        threading.Thread.__init__(self)

    def run(self):
        subprocess.call(self.command)
