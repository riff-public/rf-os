import sys
import os
import logging

logger = logging.getLogger(__name__)

if os.environ.get('RFSCRIPT', False):
    path = '%s/core' % os.environ.get('RFSCRIPT')
    if not path in sys.path:
        sys.path.append(path)

import rf_config as config
from rf_utils.context import context_info


def convert_rdi_info(rdi_line):
    rdi_split = rdi_line.split(' ')
    key = rdi_line.split('"mayaAscii"')[1].strip(' ''\"')
    attr = {}
    attr['scene_file'] = key
    for idx, val in enumerate(rdi_split):
        if "-" in val:
            if "mayaAscii" in rdi_split[idx + 1]:
                rdi_split[idx + 1] = "mayaAscii"
            attr_val = {
                val: rdi_split[idx + 1].strip('\"')
            }
            attr.update(attr_val)

    return attr


def get_file_reference_info(path):
    txt_file = ""
    result = []
    start_read = False
    with open(path, 'r') as _file:
        for line in _file:
            if "file -rdi" in line:
                start_read = True
            if start_read:
                txt_file += "".join(line.strip())
            if "requires" in line:
                break

        txt_file_filtered = txt_file.split(";")
        for line in txt_file_filtered:
            if "file -rdi" in line and '"mayaAscii"' in line:
                rdi_info = convert_rdi_info(line)
                result.append(rdi_info)
    print "result length: ", len(result)
    return result


def get_read_file(path):
    with open(path, "r") as infile:
        lines = infile.readlines()
    return lines


def write_overide_remove_ai(ref_file, lines):
    with open(ref_file['scene_file'], "w") as outfile:
        for pos, line in enumerate(lines):
            if ".ai_translator" in line:
                continue
            outfile.write(line)


def clean_arnold_attr(file_name):
    print "clean arnold attr processing... : ", file_name
    info = get_file_reference_info(file_name)
    for ref_file in info:
        print "cleaning ref_file['scene_file']: ", ref_file['scene_file']
        ref_file['scene_file'] = context_info.RootPath(ref_file['scene_file']).abs_path()
        lines = get_read_file(ref_file['scene_file'])
        write_overide_remove_ai(ref_file, lines)


def process_clean_attr(file_name):
    clean_arnold_attr(file_name)
