import os
import subprocess
import sys
from collections import OrderedDict
import getpass

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# os.environ["PYTHONPATH"] = "C:/Python27/Lib"
import rf_config as config
from rf_utils.context import context_info

log_job_info_path = "{pipeline_path}/logs/submission_job/{user}".format(pipeline_path=os.environ.get('RFSCRIPT'), user=getpass.getuser())
if not os.path.exists(log_job_info_path):
    os.makedirs(log_job_info_path)

cache_all_job_info_path = "{log_job_info_path}/cache_all_job_info.txt".format(log_job_info_path=log_job_info_path)
deadline_cmd_path = config.Software.deadline

cache_all_job_info_params = "{log_job_info_path}/cache_all_job_info.job".format(log_job_info_path=log_job_info_path)
cache_all_job_props_params = "{log_job_info_path}/cache_all_job.job".format(log_job_info_path=log_job_info_path)
cache_all_job_params = "{log_job_info_path}/cache_all_job.job".format(log_job_info_path=log_job_info_path)


def write_job_info(job_info, job_props):
    with open(cache_all_job_info_params, "w+") as f:
        f.seek(0)
        f.truncate()

    with open(cache_all_job_props_params, "w+") as f:
        f.seek(0)
        f.truncate()

    with open(cache_all_job_info_path, 'w') as out:
        out.write(cache_all_job_info_params + "\n")
        out.write(cache_all_job_props_params)

    for key, val in job_info.iteritems():
        with open(cache_all_job_info_params, 'a') as out:
            out.write('{}={}\n'.format(key, val))

    for key, val in job_props.iteritems():
        with open(cache_all_job_params, 'a') as out:
            out.write('{}={}\n'.format(key, val))


def send_to_farm(job_info, job_props):
    i = 0
    retries = 5
    write_job_info(job_info, job_props)
    sended_job = False
    custom_env = os.environ.copy()
    custom_env["PYTHONPATH"] = "C:/Python27/Lib"
    while i < retries:
        print("sending job round {}:".format(i))
        print("submit job by call {deadline_cmd_path} {cache_all_job_info_path}".format(deadline_cmd_path=deadline_cmd_path, cache_all_job_info_path=cache_all_job_info_path))
        cmd = "call \"{deadline_cmd_path}\" \"{cache_all_job_info_path}\"".format(deadline_cmd_path=deadline_cmd_path, cache_all_job_info_path=cache_all_job_info_path)
        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=custom_env)
        while proc.poll() is None:
            print(proc.stdout.readline()) #give output from your execution/your own message
        commandResult = proc.wait() #catch return code
        print("commandResult: ", commandResult)
        if commandResult:
            i = i + 1
            sended_job = False
            # retry send job to deadline if cannot send job
            continue
        else:
            sended_job = True
            break

    if not sended_job:
        import maya.cmds as mc
        mc.confirmDialog(title='Error', message='Error Send job to deadline. Please contact admin.')


def get_job_params(cmds):
    # ['C:/Program Files/Autodesk/Maya2017/bin/mayapy.exe',
    # 'C:/Users/teeruk.i/projects/rf_pipleline_tools/core/rf_app/batchTool/command/export_cam.py',
    # 'P:/projectName/scene/work/ep01/pr_ep01_q0070_s0010/anim/main/maya/pr_ep01_q0070_s0010_anim_main.v007.TA.ma']
    # context = context_info.ContextPathInfo()
    data_job_info = OrderedDict()
    data_job_info["Plugin"] = "CommandLine"
    data_job_info["Name"] = "test cache"
    data_job_info["Comment"] = ""
    data_job_info["EventOptIns"] = "CacheAll"
    data_job_info["Pool"] = "rnd"
    # data_job_info["Frames"] = "20"
    data_job_info["SecondaryPool"] = "none"
    # data_job_info["ExtraInfoKeyValue0"] = "mayapy_path={mayapy_path}".format(mayapy_path=cmds[0])
    # data_job_info["ExtraInfoKeyValue1"] = "python_file={python_file}".format(python_file=cmds[1])
    # data_job_info["ExtraInfoKeyValue2"] = "scene_file={scene_file}".format(scene_file=cmds[2])

    data_job_props = OrderedDict()
    data_job_props["Arguments"] = "{cmds}".format(cmds=" ".join(cmds))
    data_job_props["Executable"] = "C:/Program Files/Autodesk/Maya2017/bin/mayapy.exe"
    data_job_props["Shell"] = "default"
    data_job_props["ShellExecute"] = "False"
    data_job_props["SingleFramesOnly"] = "False"
    data_job_props["StartupDirectory"] = ""

    return data_job_info, data_job_props


def process_send_to_farm(cmds):
    # import maya.cmds as mc
    data_job_info, data_job_props = get_job_params(cmds)
    send_to_farm(data_job_info, data_job_props)


# scene_name = "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0010/anim/main/maya/pr_ep01_q0010_s0010_anim_main.v002.TA.ma"
# start = "1"
# end = "10"
# camera = "camera1"
# dst = "P:\projectName\deadline_lab"


# plablast(scene_name, start, end, camera, dst)
