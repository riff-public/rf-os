import os
import subprocess
import sys
from collections import OrderedDict
import getpass
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# os.environ["PYTHONPATH"] = "C:/Python27/Lib"
import rf_config as config
from rf_utils.context import context_info
from rf_maya.environment import envparser
reload(envparser)

# global vars
deadline_cmd_path = config.Software.deadline

log_job_info_path = "{pipeline_path}/logs/submission_job/{user}".format(pipeline_path=os.environ.get('RFSCRIPT'), user=getpass.getuser())
if not os.path.exists(log_job_info_path):
    os.makedirs(log_job_info_path)
job_info_path = "{log_job_info_path}/asset_render_job_info.txt".format(log_job_info_path=log_job_info_path)
job_info_params = "{log_job_info_path}/asset_render_job_info.job".format(log_job_info_path=log_job_info_path)
job_props_params = "{log_job_info_path}/asset_render_job.job".format(log_job_info_path=log_job_info_path)
job_params = "{log_job_info_path}/asset_render_job.job".format(log_job_info_path=log_job_info_path)
script_root = 'O:/Pipeline'

def write_job_info(job_info, job_props):
    with open(job_info_params, "w+") as f:
        f.seek(0)
        f.truncate()

    with open(job_props_params, "w+") as f:
        f.seek(0)
        f.truncate()

    with open(job_info_path, 'w') as out:
        out.write(job_info_params + "\n")
        out.write(job_props_params)

    for key, val in job_info.iteritems():
        with open(job_info_params, 'a') as out:
            out.write('{}={}\n'.format(key, val))

    for key, val in job_props.iteritems():
        with open(job_params, 'a') as out:
            out.write('{}={}\n'.format(key, val))


def send_to_deadline_cmd(job_info, job_props, config_path):
    i = 0
    write_job_info(job_info, job_props)
    sended_job = False

    # copy and modify current machine environment
    custom_env = os.environ.copy()
    custom_env["PYTHONPATH"] = "C:/Python27/Lib"
    # set config path so deadline launches using the prefered config
    custom_env['DEADLINE_CONFIG_FILE'] = config_path  

    print("sending job round {}:".format(i))
    print("submit job by call {deadline_cmd_path} {job_info_path}".format(
        deadline_cmd_path=deadline_cmd_path,
        job_info_path=job_info_path)
    )
    cmd = "call \"{deadline_cmd_path}\" \"{job_info_path}\"".format(
        deadline_cmd_path=deadline_cmd_path,
        job_info_path=job_info_path
    )
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=custom_env)
    while proc.poll() is None:
        print(proc.stdout.readline())
    commandResult = proc.wait()
    print("commandResult", commandResult)
    if commandResult:
        sended_job = False
    else:
        sended_job = True

    if not sended_job:
        pass 
        # from maya.cmds import confirmDialog
        # confirmDialog(title='Error', message='Error Send job to deadline. Please contact admin.')

    return sended_job


def setup_render_environments(job_info, context):
    # from maya.cmds import about
    # drive envs
    drive_envs = OrderedDict([('RFPROJECT', 'P:'), 
                            ('RFPUBL', 'P:'), 
                            ('RFPROD', 'P:'), 
                            ('RFVERSION', 'R:')])

    # project env 
    project = context.project
    department = 'Model'
    version = context.dcc_version
    with envparser.OverrideEnvironment(RFSCRIPT=script_root) as override_env:
        envs = envparser.setupEnv(project=project, department=department, version=version, 
                                mergeWithDefault=True, setEnvironment=False, writeToFile=False)
    envs = OrderedDict([(i[0], ';'.join(i[1])) for i in envs.items()])
    envs.update(drive_envs)
    for i, key in enumerate(envs):
        value = envs[key]
        job_info['EnvironmentKeyValue{}'.format(i)] = '{}={}'.format(key, value)

    return job_info

def get_job_params(entity, scene_file, start, end, camera, dst, audio_path, shot_name, task_name, sg_upload, focal_length, framerate, remove_images, renderer='redshift', dcc=('maya', '2020')):

    entity.dcc = dcc[0]
    entity.dcc_version = dcc[1]

    # ----- job info
    data_job_info = OrderedDict()
    data_job_info["Plugin"] = "MayaBatch"
    data_job_info["Name"] = os.path.basename(scene_file)
    data_job_info["Comment"] = ""
    data_job_info["EventOptIns"] = ""
    data_job_info["Pool"] = "asset_redshift"
    data_job_info["SecondaryPool"] = ""
    data_job_info["MachineLimit"] = "0"
    data_job_info["Priority"] = "50"
    data_job_info["OnJobComplete"] = "Nothing"
    data_job_info["TaskTimeoutMinutes"] = "0"
    data_job_info["MinRenderTimeMinutes"] = "0"
    data_job_info["EnableAutoTimeout"] = "0"
    data_job_info["ConcurrentTasks"] = "1"
    data_job_info["Department"] = ""
    data_job_info["Group"] = "none"
    data_job_info["LimitGroups"] = ""
    data_job_info["JobDependencies"] = ""
    data_job_info["Whitelist"] = ""
    data_job_info["OutputDirectory0"] = dst
    data_job_info["Frames"] = "{start}-{end}".format(start=start, end=end)
    data_job_info["ChunkSize"] = "5" # task per frame
    data_job_info["ExtraInfoKeyValue0"] = "image_path={dst}".format(dst=dst)
    data_job_info["ExtraInfoKeyValue1"] = "wav_path={audio_path}".format(audio_path=audio_path)
    data_job_info["ExtraInfoKeyValue2"] = "animation_name={animation_name}".format(animation_name=entity.project)
    data_job_info["ExtraInfoKeyValue3"] = "user={user}".format(user=entity.local_user)
    data_job_info["ExtraInfoKeyValue4"] = "step={step}".format(step=entity.step)
    data_job_info["ExtraInfoKeyValue5"] = "filename={filename}".format(filename=entity.filename)
    data_job_info["ExtraInfoKeyValue6"] = "shot={shot}".format(shot=shot_name)
    data_job_info["ExtraInfoKeyValue7"] = "focal_length={focal_length}".format(focal_length=focal_length)
    data_job_info["ExtraInfoKeyValue8"] = "shot_code={shot_code}".format(shot_code=entity.name)
    data_job_info["ExtraInfoKeyValue9"] = "task_name={task_name}".format(task_name=task_name)
    data_job_info["ExtraInfoKeyValue10"] = "sg_upload={sg_upload}".format(sg_upload=sg_upload)
    data_job_info["ExtraInfoKeyValue11"] = "framerate={framerate}".format(framerate=framerate)
    data_job_info["ExtraInfoKeyValue12"] = "remove_images={remove_images}".format(remove_images=remove_images)
    # data_job_info["PreJobScript"] = "O:/Pipeline/core/rf_env/Donut/deadline/v10/pre_job.py"
    # setup Maya environments
    data_job_info = setup_render_environments(job_info=data_job_info, context=entity)
    
    # ----- job props
    data_job_props = OrderedDict()
    data_job_props["Animation"] = "1"
    data_job_props["Renderer"] = renderer
    data_job_props["UsingRenderLayers"] = "1"
    data_job_props["RenderLayer"] = ""
    data_job_props["RenderHalfFrames"] = "0"
    data_job_props["FrameNumberOffset"] = "0"
    data_job_props["LocalRendering"] = "0"
    data_job_props["StrictErrorChecking"] = "1"
    data_job_props["Version"] = "2020"
    data_job_props["UseLegacyRenderLayers"] = "1"
    data_job_props["Build"] = "64bit"
    data_job_props["StartupScript"] = ""
    data_job_props["SkipExistingFrames"] = "0"
    data_job_props["OutputFilePath"] = dst
    data_job_props["Camera"] = camera
    data_job_props["Camera0"] = ""
    data_job_props["Camera1"] = camera
    data_job_props["Camera2"] = "front"
    data_job_props["Camera3"] = "persp"
    data_job_props["Camera4"] = "side"
    data_job_props["Camera5"] = "top"
    data_job_props["CountRenderableCameras"] = "1"
    data_job_props["SceneFile"] = scene_file
    data_job_props["IgnoreError211"] = "0"
    data_job_props["UseLocalAssetCaching"] = "0"

    return data_job_info, data_job_props

def get_config_path(context):
    ''' Get deadline.ini for deadlinecommand.exe to use. Setup is project-based, will use default if not found'''
    config_path = '{root}/core/rf_env/{project}/deadline/v10/assetserver.ini'.format(root=os.environ.get('RFSCRIPT'), project=context.project)
    if not os.path.exists(config_path):
        config_path = '{root}/core/rf_env/default/deadline/v10/assetserver.ini'.format(root=os.environ.get('RFSCRIPT'))
    return config_path

def send_to_deadline(scene_file, start, end, camera, dst, audio_path, shot_name, task_name="unknown", sg_upload="unknown", focalLength="unknown", framerate='24', remove_images=True, renderer='redshift', dcc=('maya', '2020')):    
    # init context 
    entity = context_info.ContextPathInfo(path=scene_file)

    # get job parameter
    data_job_info, data_job_props = get_job_params(entity, 
                                        scene_file,
                                        start,
                                        end,
                                        camera,
                                        dst,
                                        audio_path,
                                        shot_name,
                                        task_name,
                                        sg_upload,
                                        focalLength,
                                        framerate,
                                        remove_images, 
                                        renderer, 
                                        dcc
                                    )

    # get deadline config
    #config_path = get_config_path(entity)

    # override config path to deadlineserver
    config_path = "O:/Pipeline/core/rf_env/default/deadline/v10/deadlineserver.ini"

    # send to deadline
    sended_job = send_to_deadline_cmd(data_job_info, data_job_props, config_path)

    return sended_job

def test(scene_file): 
    send_to_deadline(
        scene_file=scene_file, 
        start=1, 
        end=2, 
        camera='CU1:Turn_Cam', 
        dst='P:/Bikey/_tmp/asset_farm/nattapong.n/2022-11-18/ieuji',
        audio_path='', 
        shot_name='', 
        task_name="unknown", 
        sg_upload="unknown", 
        focalLength="unknown"
        )