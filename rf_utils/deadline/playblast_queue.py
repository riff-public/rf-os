import os
import subprocess
import sys
from collections import OrderedDict
import getpass

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# os.environ["PYTHONPATH"] = "C:/Python27/Lib"
import rf_config as config
from rf_utils.context import context_info
from rf_maya.lib import playblast_lib
from rf_utils.pipeline import file_name_prefix
from rf_maya.environment import envparser

# reload(file_name_prefix)

log_job_info_path = "{pipeline_path}/logs/submission_job/{user}".format(pipeline_path=os.environ.get('RFSCRIPT'), user=getpass.getuser())
if not os.path.exists(log_job_info_path):
    os.makedirs(log_job_info_path)

playblast_job_info_path = "{log_job_info_path}/playblast_job_info.txt".format(log_job_info_path=log_job_info_path)
deadline_cmd_path = config.Software.deadline

playblast_job_info_params = "{log_job_info_path}/playblast_job_info.job".format(log_job_info_path=log_job_info_path)
playblast_job_props_params = "{log_job_info_path}/playblast_job.job".format(log_job_info_path=log_job_info_path)
playblast_job_params = "{log_job_info_path}/playblast_job.job".format(log_job_info_path=log_job_info_path)

script_root = 'O:/Pipeline'


def write_job_info(job_info, job_props):
    with open(playblast_job_info_params, "w+") as f:
        f.seek(0)
        f.truncate()

    with open(playblast_job_props_params, "w+") as f:
        f.seek(0)
        f.truncate()

    with open(playblast_job_info_path, 'w') as out:
        out.write(playblast_job_info_params + "\n")
        out.write(playblast_job_props_params)

    for key, val in job_info.iteritems():
        with open(playblast_job_info_params, 'a') as out:
            out.write('{}={}\n'.format(key, val))

    for key, val in job_props.iteritems():
        with open(playblast_job_params, 'a') as out:
            out.write('{}={}\n'.format(key, val))


def playblast_convert(job_info, job_props):
    i = 0
    write_job_info(job_info, job_props)
    sended_job = False
    custom_env = os.environ.copy()
    custom_env["PYTHONPATH"] = "C:/Python27/Lib"
    print("sending job round {}:".format(i))
    print("submit job by call {deadline_cmd_path} {playblast_job_info_path}".format(
        deadline_cmd_path=deadline_cmd_path,
        playblast_job_info_path=playblast_job_info_path)
    )
    cmd = "call \"{deadline_cmd_path}\" \"{playblast_job_info_path}\"".format(
        deadline_cmd_path=deadline_cmd_path,
        playblast_job_info_path=playblast_job_info_path
    )
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=custom_env)
    while proc.poll() is None:
        print(proc.stdout.readline())
    commandResult = proc.wait()
    print("commandResult", commandResult)
    if commandResult:
        sended_job = False
    else:
        sended_job = True

    if not sended_job:
        import maya.cmds as mc
        mc.confirmDialog(title='Error', message='Error Send job to deadline. Please contact admin.')

    return sended_job


def get_job_params(
    scene_file,
    start,
    end,
    camera,
    dst,
    audio_path,
    shot_name,
    shot_entity,
    task_name,
    sg_upload,
    output_filename,
    focal_length,
    # wav_offset= '00:00:00.0000',
    framerate,
    remove_images
):
    context = context_info.ContextPathInfo()
    outputW, outputH = context.projectInfo.render.raw_outputH()
    convertW, convertH = context.projectInfo.render.final_outputH()
    context.dcc = 'maya'
    context.dcc_version = str(context.projectInfo.dcc().get('Maya', 2020))
    #context.department = 'Py' # use enviroment from Py to playblast v.2.0
    context.department = 'Farm'

    data_job_info = OrderedDict()
    data_job_info["Plugin"] = "MayaBatch"
    data_job_info["Name"] = "{scene_file} playblast".format(scene_file=os.path.basename(scene_file))
    data_job_info["Comment"] = ""
    data_job_info["EventOptIns"] = "BurnIn"
    data_job_info["Pool"] = "rnd"
    data_job_info["SecondaryPool"] = "none"
    data_job_info["MachineLimit"] = "0"
    data_job_info["Priority"] = "50"
    data_job_info["OnJobComplete"] = "Nothing"
    data_job_info["TaskTimeoutMinutes"] = "0"
    data_job_info["MinRenderTimeMinutes"] = "0"
    data_job_info["EnableAutoTimeout"] = "0"
    data_job_info["ConcurrentTasks"] = "1"
    data_job_info["Department"] = ""
    data_job_info["Group"] = "none"
    data_job_info["LimitGroups"] = ""
    data_job_info["JobDependencies"] = ""
    data_job_info["Whitelist"] = ""
    data_job_info["OutputFilename0"] = "{dst}/{output_filename}".format(dst=dst, output_filename=output_filename)
    data_job_info["Frames"] = "{start}-{end}".format(start=start, end=end)
    data_job_info["ChunkSize"] = "5"
    data_job_info["ExtraInfoKeyValue0"] = "image_path={dst}".format(dst=dst)
    data_job_info["ExtraInfoKeyValue1"] = "wav_path={audio_path}".format(audio_path=audio_path)
    data_job_info["ExtraInfoKeyValue2"] = "animation_name={animation_name}".format(animation_name=context.project)
    data_job_info["ExtraInfoKeyValue3"] = "user={user}".format(user=context.local_user)
    data_job_info["ExtraInfoKeyValue4"] = "step={step}".format(step=context.step)
    data_job_info["ExtraInfoKeyValue5"] = "filename={filename}".format(filename=context.filename)
    data_job_info["ExtraInfoKeyValue6"] = "shot={shot}".format(shot=shot_name)
    data_job_info["ExtraInfoKeyValue7"] = "outputWidth={convertW}".format(convertW=convertW)
    data_job_info["ExtraInfoKeyValue8"] = "outputHeight={convertH}".format(convertH=(convertH))
    data_job_info["ExtraInfoKeyValue9"] = "focal_length={focal_length}".format(focal_length=focal_length)
    # data_job_info["ExtraInfoKeyValue10"] = "wav_offset={wav_offset}".format(wav_offset=wav_offset)
    data_job_info["ExtraInfoKeyValue10"] = "shot_code={shot_code}".format(shot_code=shot_entity.name)
    data_job_info["ExtraInfoKeyValue11"] = "task_name={task_name}".format(task_name=task_name)
    data_job_info["ExtraInfoKeyValue12"] = "sg_upload={sg_upload}".format(sg_upload=sg_upload)
    data_job_info["ExtraInfoKeyValue13"] = "framerate={framerate}".format(framerate=framerate)
    data_job_info["ExtraInfoKeyValue14"] = "remove_images={remove_images}".format(remove_images=remove_images)

    data_job_info = envparser.setup_render_environments(data_job_info, context)

    data_job_props = OrderedDict()
    data_job_props["Animation"] = "1"
    data_job_props["Renderer"] = "mayaHardware2"
    data_job_props["UsingRenderLayers"] = "0"
    data_job_props["RenderLayer"] = ""
    data_job_props["RenderHalfFrames"] = "0"
    data_job_props["FrameNumberOffset"] = "0"
    data_job_props["LocalRendering"] = "0"
    data_job_props["StrictErrorChecking"] = "1"
    data_job_props["Version"] = context.dcc_version
    data_job_props["UseLegacyRenderLayers"] = "1"
    data_job_props["Build"] = "64bit"
    data_job_props["StartupScript"] = ""
    data_job_props["ImageWidth"] = str(outputW)
    data_job_props["ImageHeight"] = str(outputH)
    data_job_props["SkipExistingFrames"] = "0"
    data_job_props["OutputFilePath"] = dst
    data_job_props["OutputFilePrefix"] = output_filename.split('.')[0]
    data_job_props["Camera"] = camera
    data_job_props["Camera0"] = ""
    data_job_props["Camera1"] = camera
    data_job_props["Camera2"] = "front"
    data_job_props["Camera3"] = "persp"
    data_job_props["Camera4"] = "side"
    data_job_props["Camera5"] = "top"
    data_job_props["CountRenderableCameras"] = "1"
    data_job_props["SceneFile"] = scene_file
    data_job_props["IgnoreError211"] = "0"
    data_job_props["UseLocalAssetCaching"] = "0"

    return data_job_info, data_job_props


def clean_attribute():
    import maya.cmds as mc
    mc.setAttr('defaultRenderGlobals.preMel', '', type='string')
    mc.setAttr('defaultRenderGlobals.postMel', '', type='string')
    file_name_prefix.file_name_prefix_playblast()
    mc.file(save=True, type="mayaAscii")


def playblast(
    scene_file,
    start,
    end,
    camera,
    dst,
    shot_name,
    output_filename,
    audio_path,
    shot_entity,
    task_name="unknown",
    sg_upload="unknown",
    focalLength="unknown",
    # wav_offset='00:00:00.0000',
    framerate='24',
    remove_images=True
):
    import maya.cmds as mc
    clean_attribute()

    # # miarmy playblast
    # print 'MIARMY TEST'
    # try:
    #     mc.loadPlugin( 'MiarmyPro.mll' )
    #     import McdGeneral

    #     McdGeneral.McdGetMcdGlobalNode()
    #     if not McdRender.McdCheckAgentPlace():
    #         McdPlacementFunctions.placementAgent()
    # except:
    #     pass
        
    mc.setAttr("%s.backgroundColor"%camera ,0.85, 0.85, 0.85, type='double3')
    # dir_path, filename = os.path.split(dst) ## filename .ext. = .wav  ###dir_path = scene.path.work_output().abs_path()
    # wav_path = '%s/%s/%s'%(dst, 'sound', audio_name)
    # audio_path = playblast_lib.gen_audio_cutting(shot_name, wav_path)
    audio_path = playblast_lib.generate_shot_audio(start=start, end=end, output_path=audio_path, fps=framerate)
    #audio_path = ''
    # print '==audio path %s' %audio_path
    data_job_info, data_job_props = get_job_params(
        scene_file,
        start,
        end,
        camera,
        dst,
        audio_path,
        shot_name,
        shot_entity,
        task_name,
        sg_upload,
        output_filename,
        focalLength,
        # wav_offset,
        framerate,
        remove_images
    )

    sended_job = playblast_convert(data_job_info, data_job_props)

    return sended_job


# scene_name = "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0010/anim/main/maya/pr_ep01_q0010_s0010_anim_main.v002.TA.ma"
# start = "1"
# end = "10"
# camera = "camera1"
# dst = "P:\projectName\deadline_lab"


# plablast(scene_name, start, end, camera, dst)
