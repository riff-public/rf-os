import os
import subprocess
import sys
from collections import OrderedDict
import getpass

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# os.environ["PYTHONPATH"] = "C:/Python27/Lib"
import rf_config as config
from rf_utils.context import context_info

log_job_info_path = "{pipeline_path}/logs/submission_job/{user}".format(pipeline_path=os.environ.get('RFSCRIPT'), user=getpass.getuser())
if not os.path.exists(log_job_info_path):
    os.makedirs(log_job_info_path)

cache_all_job_info_path = "{log_job_info_path}/cache_all_job_info.txt".format(log_job_info_path=log_job_info_path)
deadline_cmd_path = config.Software.deadline

cache_all_job_info_params = "{log_job_info_path}/cache_all_job_info.job".format(log_job_info_path=log_job_info_path)
cache_all_job_props_params = "{log_job_info_path}/cache_all_job.job".format(log_job_info_path=log_job_info_path)
cache_all_job_params = "{log_job_info_path}/cache_all_job.job".format(log_job_info_path=log_job_info_path)


def write_job_info(job_info, job_props):
    with open(cache_all_job_info_params, "w+") as f:
        f.seek(0)
        f.truncate()

    with open(cache_all_job_props_params, "w+") as f:
        f.seek(0)
        f.truncate()

    with open(cache_all_job_info_path, 'w') as out:
        out.write(cache_all_job_info_params + "\n")
        out.write(cache_all_job_props_params)

    for key, val in job_info.iteritems():
        with open(cache_all_job_info_params, 'a') as out:
            out.write('{}={}\n'.format(key, val))

    for key, val in job_props.iteritems():
        with open(cache_all_job_params, 'a') as out:
            out.write('{}={}\n'.format(key, val))


def send_to_farm(job_info, job_props):
    i = 0
    write_job_info(job_info, job_props)
    sended_job = False
    custom_env = os.environ.copy()
    custom_env["PYTHONPATH"] = "C:/Python27/Lib"
    print("sending job round {}:".format(i))
    print("submit job by call {deadline_cmd_path} {cache_all_job_info_path}".format(
        deadline_cmd_path=deadline_cmd_path,
        cache_all_job_info_path=cache_all_job_info_path)
    )
    cmd = "call \"{deadline_cmd_path}\" \"{cache_all_job_info_path}\"".format(
        deadline_cmd_path=deadline_cmd_path,
        cache_all_job_info_path=cache_all_job_info_path
    )
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=custom_env)
    while proc.poll() is None:
        print(proc.stdout.readline())
    commandResult = proc.wait()
    print("commandResult: ", commandResult)
    if commandResult:
        sended_job = False
    else:
        sended_job = True

    if not sended_job:
        import maya.cmds as mc
        mc.confirmDialog(title='Error', message='Error Send job to deadline. Please contact admin.')

    return sended_job


def get_job_params(cmds, jobname='', executable=''):
    # ['C:/Program Files/Autodesk/Maya2017/bin/mayapy.exe',
    # 'C:/Users/teeruk.i/projects/rf_pipleline_tools/core/rf_app/batchTool/command/export_cam.py',
    # 'P:/projectName/scene/work/ep01/pr_ep01_q0070_s0010/anim/main/maya/pr_ep01_q0070_s0010_anim_main.v007.TA.ma']
    filename = [cmds[i+1] for i, v in enumerate(cmds) if '-s' == v]
    camera = [cmds[i+1] for i, v in enumerate(cmds) if '-c' == v]
    fcl = [cmds[i+1] for i, v in enumerate(cmds) if '-fcl' == v]
    build = [cmds[i+1] for i, v in enumerate(cmds) if '-build' == v]
    entity = context_info.ContextPathInfo(path = filename[0])
    jobname = 'None' if not jobname else jobname
    executable = executable if executable else 'C:/Program Files/Autodesk/Maya2017/bin/mayapy.exe'
    data_job_info = OrderedDict()
    data_job_info["Plugin"] = "CommandLine"
    data_job_info["Name"] = jobname
    data_job_info["Comment"] = ""
    data_job_info["EventOptIns"] = "CacheAll"
    data_job_info["Pool"] = "rnd"
    # data_job_info["Frames"] = "20"
    data_job_info["SecondaryPool"] = "none"

    if 'False' in build:
        data_job_info["EventOptIns"] = ""

    if 'crowd' in jobname:
        data_job_info["Pool"] = "miarmy"
        data_job_info["SecondaryPool"] = "miarmy"
        data_job_info["EventOptIns"] = ""

    if entity.project:
        data_job_info["PreJobScript"]= "O:/Pipeline/core/rf_env/" + entity.project + "/deadline/v8/pre_job.py" 
    # data_job_info["ExtraInfoKeyValue0"] = "mayapy_path={mayapy_path}".format(mayapy_path=cmds[0])
    # data_job_info["ExtraInfoKeyValue1"] = "python_file={python_file}".format(python_file=cmds[1])
    # data_job_info["ExtraInfoKeyValue2"] = "scene_file={scene_file}".format(scene_file=cmds[2])

    data_job_props = OrderedDict()
    data_job_props["Arguments"] = "{cmds}".format(cmds=" ".join(cmds))
    data_job_props["Executable"] = executable
    data_job_props["Shell"] = "default"
    data_job_props["ShellExecute"] = "False"
    data_job_props["SingleFramesOnly"] = "False"
    data_job_props["StartupDirectory"] = ""
    data_job_props["SceneFile"] = filename[0]
    if camera:
        data_job_props["Camera"] = camera[0]
    if fcl:
        data_job_props["FocalLength"] = fcl[0]

    return data_job_info, data_job_props


def process_send_to_farm(cmds, jobname='', executable='', **kwargs):
    # import maya.cmds as mc
            
    data_job_info, data_job_props = get_job_params(cmds, jobname=jobname, executable=executable)
    if kwargs: 
        for k, v in kwargs.iteritems(): 
            data_job_info[k] = v
    sended_job = send_to_farm(data_job_info, data_job_props)

    return sended_job


# scene_name = "P:/projectName/scene/work/ep01/pr_ep01_q0010_s0010/anim/main/maya/pr_ep01_q0010_s0010_anim_main.v002.TA.ma"
# start = "1"
# end = "10"
# camera = "camera1"
# dst = "P:\projectName\deadline_lab"


# plablast(scene_name, start, end, camera, dst)
