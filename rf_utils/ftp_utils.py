import sys
import os
import ftplib

_SERVER = '110.171.40.48'
_USER = 'riff_api'
_PWD = 'admin123'


class FtpBase(object):
    """docstring for FtpBase"""
    def __init__(self, server=_SERVER, user=_USER, pwd=_PWD):
        super(FtpBase, self).__init__()
        self.server = server 
        self.user = user 
        self.pwd = pwd
        self.session = ftplib.FTP(self.server, self.user, self.pwd)

    def list_dirs(self, path='/', relative=False): 
        results = self.session.nlst(path)
        dirs = [a for a in results if self.is_dir(a)]
        if relative: 
            dirs = [os.path.split(a)[-1] for a in dirs]
        return dirs

    def list_files(self, path): 
        results = self.session.nlst(path)
        files = [a for a in results if not self.is_dir(a)]
        return files

    def is_dir(self, path): 
        """ 
            check if path a dir or file 
            nlst will return [path] if path is a file
            or else it's a directory
        """ 
        # this is too slow 
        # results = self.session.nlst(path)
        # if not results: 
        #     return True 
        # else: 
        #     if not results[0] == path: 
        #         return True 
        # return False
        # change to detect extension instead
        base, ext = os.path.splitext(path)
        return False if ext else True

    def size(self, filename): 
        return self.session.size(filename)

    def upload(self, src, dst): 
        if os.path.exists(src): 
            with open(src,'rb') as f:      
                if not self.exists(os.path.dirname(dst)): 
                    self.makedirs(os.path.dirname(dst))
                result = self.session.storbinary('STOR {}'.format(dst), f)  
                print('Upload complete {}'.format(dst)) 
                return self.check_result(result)

    def download(self, src, dst): 
        if not os.path.exists(os.path.dirname(dst)): 
            os.makedirs(os.path.dirname(dst))
        with open(dst, 'wb') as f: 
            result = self.session.retrbinary('RETR {}'.format(src), f.write) 
        return self.check_result(result)

    def upload_folder(self, src, dst, callback=None): 
        # localpath 
        for root, dirs, files in os.walk(src): 
            for file in files: 
                localpath = '{}/{}'.format(root.replace('\\', '/'), file)
                relpath = localpath.replace(src, '')
                remotepath = '{}{}'.format(dst, relpath)
                self.upload(localpath, remotepath)
                print('upload {}'.format(remotepath))
                if callback: 
                    callback.emit('upload {}'.format(remotepath))

    def rename(self, src, dst): 
        result = self.session.rename(src, dst) 
        return self.check_result(result)

    def makedirs(self, path): 
        parent_dir, name = os.path.split(path)
        if not self.exists(parent_dir): 
            self.makedirs(parent_dir)
        self.session.mkd(path)

        # result = self.session.mkd(path) 
        # return self.check_result(result)

    def check_result(self, result): 
        success = '226'
        return True if success in result else False

    def exists(self, path): 
        """ 
            check if path exists in ftp 
            args: 
                path(str): ex. '/test/sub/test.ma' 
            returns: (bool)
        """ 
        def list_root(path, dirs=list()): 
            """ 
                function list root 
                args: 
                    path(str): ex. '/test/sub/test.ma'
                returns: list() ex. ['/test', '/test/sub', '/test/sub/test.ma']
            """ 
            root, file = os.path.split(path)
            if not file: 
                return dirs 
            else: 
                list_root(root)
                dirs.append(path)
            return dirs

        dirs = list_root(path)
        # dirs = ['/test', '/test/sub', '/test/sub/test.ma']

        for d in dirs: 
            root, file = os.path.split(d)
            self.session.cwd(root)
            files = self.session.nlst()
            if not file in files: 
                return False 
        self.session.cwd('/')
        return True

    def test(self): 
        with open("D:/wood_ad.ma", 'rb') as f:      
            self.session.storbinary('STOR {}'.format('/test/image.jpg'), f)   


class RFFtp(FtpBase):
    """docstring for RFFtp"""
    def __init__(self, server=_SERVER, user=_USER, pwd=_PWD):
        super(RFFtp, self).__init__(server=_SERVER, user=_USER, pwd=_PWD)
        self.local_root = ''
        self.upload_root = ''
        self.download_root = ''
        
    def upload_files(self, files): 
        if self.upload_root and self.download_root: 
            for src in files: 
                dst = src.replace(self.local_root, self.upload_root)
                self.upload(src, dst)
        else: 
            print('upload root and download root not set')


        

def test(): 
    ftp = RFFtp()

    # create folder 
    result = ftp.makedirs('test')
    if result: 
        print('Create folder success')

    # transfer file to test 
    src = 'D:/test.ma'
    dst = '/test/test.ma'
    result = ftp.upload(src, dst)
    if result: 
        print('upload success')

    # rename file 
    dst = 'test/test2.ma'
    new_name = '/test/test3.ma'
    ftp.upload(src, dst)
    result = ftp.rename(dst, new_name)
    if result: 
        print('rename success')

    # download from ftp 
    src = new_name
    local_dst = 'D:/from_ftp/test2.ma'
    result = ftp.download(src, local_dst)
    if result: 
        print('download success')



def test2(): 
    server = '110.171.40.48'
    user = 'user5'
    pw = 'admin123'
    # create connection
    session = ftplib.FTP(server, user, pw)

    # make dir
    session.mkd('/TEST')

    # send file 1
    with open("D:/__playground/b1.jpg",'rb') as f:      
        session.storbinary('STOR {}'.format('/Riff/image.jpg'), f)   

    # send file b.txt as b.txt
    with open("D:/__playground/b.txt",'rb') as f:      
        session.storbinary('STOR {}'.format('/b.txt'), f)   

    # send file b.txt as b2.txt
    with open("D:/__playground/b.txt",'rb') as f:      
        session.storbinary('STOR {}'.format('/b2.txt'), f)   

    # remove file 3
    session.delete('/b2.txt')

    # close connection                      
    session.quit()


    # ------------------------------------
    handle = open('D:/__playground/image.jpg', 'wb')
    session.retrbinary('RETR /TEST/image.jpg', handle.write)
