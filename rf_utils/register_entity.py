import os
import sys
import shutil
import logging
from collections import OrderedDict

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# pipeline
import rf_config as config
from rf_utils import file_utils
from rf_utils.context import context_info
from rf_utils.pipeline import version
from rf_utils import admin 

class Config:
    tempFile = 'temp_ad.yml'
    mdl = 'mdl'
    rig = 'rig'
    mtr = 'mtr'
    mtrData = 'mtrData'
    default = 'default'
    workspace = 'workspace'
    media = 'media'
    preview = 'preview'
    id = 'versionId'
    version = 'version'
    mtr = 'mtr'
    mtrPreview = 'mtrPreview'
    mtrRender = 'mtrRender'
    yetiGroom = 'yetiGroom'
    yetiGroomDisplay = 'yetiGroomDisplay'
    techGeo = 'techGeo'
    xgenGroom = 'xgenGroom'
    cache = 'cache'
    maya = 'maya'
    gpu = 'gpu'
    animData = 'animData'
    look = 'look'
    process = 'process'
    task = 'task'
    design = 'design'
    rsproxy = 'rsproxy'
    mayaRsProxy = 'mayaRsProxy'
    aistandin = 'aistandin'
    asmset = 'set'
    camera = 'camera'
    project = 'project'
    assetName = 'assetName'
    assetId = 'assetId'
    description = 'description'
    heroFile = 'heroFile'
    heroPath = 'heroPath'
    publishFile = 'publishFile'
    masterOrder = [project, assetName, assetId, heroPath, 'design', 'model', 'rig', 'texture', 'lookdev']
    nonStepKey = [project, assetName, assetId, heroPath]
    preferSortingTitle = [project, assetName, assetId, heroPath]
    preferSortingStep = ['design', 'model', 'rig', 'texture', 'lookdev']
    preferSortingOutput = [process, look, version, id, task, description, workspace, design, gpu, cache, rig, mtr, mtrPreview, mtrRender, yetiGroom, yetiGroomDisplay, media, preview]
    preferSortingRes = ['pr', 'md']

    fileType = {
        'gpu': ('model', 'gpu'), 
        'cache': ('model', 'cache'), 
        'rig': ('rig', 'rig'), 
        'preview_mtr': ('model', 'preview'), 
        'smooth_data': ('model', 'smoothData')
    }


class Register(object):
    """docstring for Register"""
    def __init__(self, ContextPathInfo):
        super(Register, self).__init__()
        self.entity = ContextPathInfo
        self.configKey = '{0}_ad'.format(self.entity.name)
        self.prodName = Config.tempFile
        self.configHero = '{0}.hero.yml'.format(self.configKey)
        self.configVersion = '{0}.$version.yml'.format(self.configKey)
        self.regFile = self.tmp_config_path()
        self.cacheData = OrderedDict()
        self.read()

    def tmp_config_path(self):
        return '%s/%s' % (self.entity.path.asm_data().abs_path(), self.prodName)

    def hero_config_path(self):
        return '%s/%s' % (self.entity.path.asm_data().abs_path(), self.configHero)

    def read(self):
        # dataFile = self.tmp_config_path()
        dataFile = self.tmp_config_path() if os.path.exists(self.tmp_config_path()) else self.hero_config_path()
        data = file_utils.ymlLoader(dataFile) if os.path.exists(dataFile) else OrderedDict()
        self.data = data

    def read_version(self):
        configVersions = self._get_register_version()
        if configVersions:
            for configVersion in configVersions:
                name = os.path.basename(configVersion)
                if not name in self.cacheData.keys():
                    data = file_utils.ymlLoader(configVersion)
                    self.cacheData[name] = data
        return self.cacheData

    def write(self):
        dataFile = self.tmp_config_path()
        os.makedirs(os.path.dirname(dataFile)) if not os.path.exists(os.path.dirname(dataFile)) else None
        file_utils.ymlDumper(dataFile, self.data)
        return dataFile

    def register(self):
        """ write hero file and version """
        dataFile = self.write()
        self.publish_version(dataFile)
        self.remove_temp(dataFile)

    def publish_version(self, dataFile):
        versionFile = self.get_publish_name(dataFile)
        heroFile = self.get_hero_name(dataFile)
        fileNew = self._is_file_new(dataFile, heroFile)
        if fileNew:
            shutil.copy2(dataFile, versionFile)
            shutil.copy2(dataFile, heroFile)
            return versionFile if os.path.exists(versionFile) else False

    def remove_temp(self, dataFile):
        # os.remove(dataFile)
        admin.remove(dataFile)

    def get_types(self):
        keys = self.data.keys()
        types = [a for a in keys if not a in Config.preferSortingTitle]
        return types

    def get_res(self):
        types = self.get_types()
        res = []
        for type in types:
            res = res + self.data[type].keys()
        return list(set(res))

    def get_processes(self, step, res):
        outputs = GetInfo(self.data).get_outputs(step, res)
        processes = list(set([a.get('process') for a in outputs]))
        return processes

    def get_looks(self, step, res):
        outputs = GetInfo(self.data).get_outputs(step, res)
        looks = list(set([a.get('look') for a in outputs]))
        return looks

    def get_publish_name(self, dataFile):
        dirname = os.path.dirname(dataFile)
        replaceVersion = version.increment_version(os.path.dirname(dataFile))
        configFileVersion = self.configVersion.replace('$version', replaceVersion)
        configFile = '%s/%s' % (os.path.dirname(dataFile), configFileVersion)
        return configFile

    def get_hero_name(self, dataFile):
        dirname = os.path.dirname(dataFile)
        heroFile = '%s/%s' % (dirname, self.configHero)
        return heroFile

    def _is_file_new(self, dataFile, heroFile):
        if not os.path.exists(heroFile):
            return True
        data1 = file_utils.ymlLoader(dataFile)
        data2 = file_utils.ymlLoader(heroFile)
        return True if not data1 == data2 else False

    def _get_register_version(self):
        dataDir = self.entity.path.asm_data().abs_path()
        files = file_utils.list_file(dataDir) if os.path.exists(dataDir) else []
        if files:
            files.remove(self.configHero) if self.configHero in files else None
            configFiles = [('/').join([dataDir, a]) for a in files]
            return configFiles

    def set(self, step='', res='', output=None):
        """ set data """
        step = step if step else self.entity.step
        res = res if res else self.entity.res
        process = self.entity.process
        outputDict = output.data or OrderedDict()
        self.add_title(self.data)

        process = outputDict.get('process')
        look = outputDict.get('look')
        checkKey = Config.process if not step in [context_info.Step.texture, context_info.Step.lookdev, context_info.Step.groom] else Config.look
        checkStr = process if not step in [context_info.Step.texture, context_info.Step.lookdev, context_info.Step.groom] else look

        # level 1
        if step in self.data.keys():
            if res in self.data[step].keys():
                outputList = self.data[step][res]
                existingItem = [a for a in outputList if a.get(checkKey) == checkStr]
                newDict = self._combine_dict(outputDict, existingItem[0]) if existingItem else outputDict
                outputList.remove(existingItem[0]) if existingItem else None
                outputList.append(newDict)

            else:
                self.data[step][res] = [outputDict]
        else:
            tmpDict = OrderedDict()
            tmpDict[res] = [outputDict]
            self.data[step] = tmpDict

        self.data = self.sorting_dict(self.data)

    def add_title(self, data):
        data[Config.project] = self.entity.project
        data[Config.assetName] = self.entity.name
        data[Config.assetId] = self.entity.id
        data[Config.heroPath] = self.entity.path.hero().abs_path()
        return data

    def add_asset_id(self, id):
        self.data['assetId'] = id

    @property
    def get(self):
        return GetInfo(self.data)

    @property
    def list(self):
        return ListInfo(self.data)    

    @property
    def version(self):
        return GetVersion(self.read_version())

    def _combine_dict(self, dictData1, dictData2):
        for k, v in dictData2.iteritems():
            if not k in dictData1.keys():
                dictData1[k] = v
        return dictData1

    def sorting_dict(self, data):
        newDict = self.sorting_data(data, Config.masterOrder)

        for key1, value1 in newDict.iteritems():
            sortDict1 = self.sorting_data(value1, Config.preferSortingRes) if type(value1) == type(OrderedDict()) else value1
            newDict[key1] = sortDict1
            if type(sortDict1) == type(OrderedDict()):
                for key2 in sortDict1.keys():
                    outputList = sortDict1[key2]
                    orderList = []
                    for value2 in outputList:
                        sortDict2 = self.sorting_data(value2, Config.preferSortingOutput) if type(value2) == type(OrderedDict()) else value2
                        orderList.append(sortDict2)
                    sortDict1[key2] = orderList
                newDict[key1] = sortDict1
        return newDict

    def sorting_data(self, dictData, orders):
        sortDict = OrderedDict()
        for order in orders:
            if order in dictData.keys():
                sortDict[order] = dictData[order]

        for key in dictData.keys():
            if not key in sortDict.keys():
                sortDict[key] = dictData[key]
        return sortDict

class RegisterInfo(object):
    """docstring for RegisterInfo"""
    def __init__(self, regFile):
        super(RegisterInfo, self).__init__()
        self.regFile = regFile
        self.data = self.read()

    def read(self):
        data = OrderedDict()
        if os.path.exists(self.regFile): 
            data = file_utils.ymlLoader(self.regFile)
        else: 
            logger.error('{} does not exists'.format(self.regFile))
        return data

    @property
    def project(self):
        return self.data.get(Config.project)

    @property
    def asset_name(self):
        return self.data.get(Config.assetName)

    @property
    def asset_id(self):
        return self.data.get(Config.assetId)

    @property
    def get(self):
        return GetInfo(self.data)

    @property
    def list(self):
        return ListInfo(self.data)    


class GetInfo(object):
    """docstring for GetInfo"""
    def __init__(self, data):
        super(GetInfo, self).__init__()
        self.data = data

    def project(self): 
        return self.data[Config.project]

    def asset_name(self): 
        return self.data[Config.assetName]

    def asset_id(self): 
        return self.data[Config.assetId]

    def get_output(self, step='', res='', process='', look='', outputAttr=''):
        checkKey = Config.process if not step in [context_info.Step.texture, context_info.Step.lookdev, context_info.Step.groom] else Config.look
        checkStr = process if not step in [context_info.Step.texture, context_info.Step.lookdev, context_info.Step.groom] else look
        outputList = self.data.get(step, dict()).get(res, dict())
        matchItem = [a for a in outputList if a.get(checkKey) == checkStr]
        if matchItem:
            return matchItem[0] if not outputAttr else matchItem[0].get(outputAttr)

    def get_outputs(self, step, res='md'):
        return self.data.get(step, dict()).get(res, dict())

    def output(self, step='', process='main', look='', res='md'):
        outputDict = self.get_output(step=step, process=process, res=res)
        if look:
            outputDict = self.get_output(step=step, look=look, res=res)
        return outputDict

    def list_step(self): 
        nonStepKey = Config.nonStepKey
        if self.data.keys(): 
            return [a for a in self.data.keys() if not a in nonStepKey]
        return []

    def list_process(self, step, res='md'): 
        stepData = self.data.get(step).get(res)
        processes = []
        for data in stepData: 
            process = data.get('process')
            processes.append(process) if process else None

        return processes

    def list_look(self, step, res='md'): 
        stepData = self.data.get(step).get(res)
        processes = []
        for data in stepData: 
            process = data.get('look')
            processes.append(process) if process else None

        return processes

    def list_step_data_key(self, step, process, dataKey, res='md'): 
        stepData = self.data.get(step).get(res)

        for data in stepData: 
            if data.get('process') == process: 
                return data.get(dataKey)


    def look(self):
        outputDict = self.get_output(step=context_info.Step.texture)
        output = Output(data=outputDict)
        return output.get_look()

    def gpu(self, process='main', res='md'):
        outputDict = self.get_output(step=context_info.Step.model, process=process, res=res)
        output = Output(data=outputDict)
        return output.get_gpu()

    def mtr(self, look='main', res='md'):
        outputDict = self.get_output(step=context_info.Step.texture, look=look, res=res)
        output = Output(data=outputDict)
        return output.get_mtr()

    def mtr_render(self, look='main', res='md'):
        outputDict = self.get_output(step=context_info.Step.lookdev, look=look, res=res)
        output = Output(data=outputDict)
        return output.get_mtr_render()

    def mtr_preview(self, look='main', res='md'):
        outputDict = self.get_output(step=context_info.Step.texture, look=look, res=res)
        output = Output(data=outputDict)
        return output.get_mtr_preview()

    def groom_yeti(self, look='main', res='md'):
        outputDict = self.get_output(step=context_info.Step.groom, look=look, res=res)
        output = Output(data=outputDict)
        return output.get_groom_yeti()

    def groom_yeti_display(self, look='main', res='md'):
        outputDict = self.get_output(step=context_info.Step.groom, look=look, res=res)
        output = Output(data=outputDict)
        return output.get_groom_yeti_display()

    def rsproxy(self, look='main', res='md'):
        outputDict = self.get_output(step=context_info.Step.lookdev, look=look, res=res)
        output = Output(data=outputDict)
        return output.get_rsproxy()

    def cache(self, process='main', res='md'):
        outputDict = self.get_output(step=context_info.Step.model, process=process, res=res)
        output = Output(data=outputDict)
        return output.get_cache()

    def rig(self, process='main', res='md'):
        outputDict = self.get_output(step=context_info.Step.rig, process=process, res=res)
        output = Output(data=outputDict)
        return output.get_rig()

    def workspace(self, step='', process='main', res='md', look='main'):
        outputDict = self.get_output(step=step, process=process, res=res, look=look)
        output = Output(data=outputDict)
        return output.get_workspace()

    def preview(self, step='', process='', look='', res='md'):
        outputDict = self.get_output(step=step, process=process, look=look, res=res)
        output = Output(data=outputDict)
        return output.get_preview()

    def media(self, step='', process='', look='', res='md'):
        outputDict = self.get_output(step=step, process=process, look=look, res=res)
        output = Output(data=outputDict)
        return output.get_preview()

    def looks(self, res=''):
        res = res if res else self.res
        return self.data[context_info.Step.texture][res]

    def hero_path(self):
        return self.data[Config.heroPath]

class ListInfo(object): 
    """docstring for ListInfo"""
    def __init__(self, data):
        super(ListInfo, self).__init__()
        self.data = data

    def steps(self): 
        return [a for a in self.data.keys() if not a in Config.nonStepKey]

    def caches(self, res='md', hero=True): 
        return self.files(key=Config.cache, res=res, hero=True)

    def gpus(self, res='md', hero=True): 
        return self.files(key=Config.gpu, res=res, hero=True)

    def files(self, key='', res='md', hero=True): 
        targetFiles = []
        steps = self.steps()
        for step in steps: 
            datas = self.data.get(step).get(res)
            if datas: 
                for data in datas: 
                    if key in data.keys(): 
                        outputDict = data[key]
                        if outputDict: 
                            targetFile = outputDict.get('heroFile') if hero else outputDict.get('publishedFile')
                            targetFiles.append(targetFile) if not targetFile in targetFiles else None

        return targetFiles

class GetVersion(object):
    """docstring for GetVersion"""
    def __init__(self, datas):
        super(GetVersion, self).__init__()
        self.datas = datas

    def outputs(self, step='', process='main', look='', res='md'):
        outputList = []
        versions = [GetInfo(v).output(step=step, process=process, res=res) for k, v in self.datas.iteritems()]
        if look:
            versions = [GetInfo(v).output(step=step, look=look, res=res) for k, v in self.datas.iteritems()]

        for version in versions:
            if version:
                if not version in outputList:
                    outputList.append(version)
        return outputList

    def gpus(self, process='main', res='md'):
        # output from this method
        # [{'publishedFile': path, 'heroFile': path}, {'publishedFile': path, 'heroFile': path}]
        versions = [GetInfo(v).gpu(process=process, res=res) for k, v in self.datas.iteritems()]
        return self.cleanup_ducpliate_data(versions)

    def caches(self, process='main', res='md'):
        versions = [GetInfo(v).cache(process=process, res=res) for k, v in self.datas.iteritems()]
        return self.cleanup_ducpliate_data(versions)

    def mtrs(self, look='main', res='md'):
        versions = [GetInfo(v).mtr(look=look, res=res) for k, v in self.datas.iteritems()]
        return self.cleanup_ducpliate_data(versions)

    def mtrs_preview(self, look='main', res='md'):
        versions = [GetInfo(v).mtr_preview(look=look, res=res) for k, v in self.datas.iteritems()]
        return self.cleanup_ducpliate_data(versions)

    def mtrs_render(self, look='main', res='md'):
        versions = [GetInfo(v).mtr_render(look=look, res=res) for k, v in self.datas.iteritems()]
        return self.cleanup_ducpliate_data(versions)

    def rigs(self, process='main', res='md'):
        versions = [GetInfo(v).rig(process=process, res=res) for k, v in self.datas.iteritems()]
        return self.cleanup_ducpliate_data(versions)

    def workspaces(self, step='', process='main', res='md', look='main'):
        versions = [GetInfo(v).workspace(step=step, process=process, res=res, look=look) for k, v in self.datas.iteritems()]
        return self.cleanup_ducpliate_data(versions)

    def cleanup_ducpliate_data(self, dataList):
        """ group list in 1 dict data """
        # output from this method
        # {'publishedFile': [path1, path2], 'heroFile': [path]}
        newDict = OrderedDict()
        for data in dataList:
            for k, v in data.iteritems():
                if k in newDict.keys():
                    if not v in newDict[k]:
                        newDict[k].append(v)
                else:
                    newDict[k] = [v]
        return newDict


class Output(object):
    """docstring for Output"""
    def __init__(self, data=None):
        super(Output, self).__init__()
        self.data = OrderedDict() if not data else data

    def add_process(self, value):
        self.add_key(Config.process, value)

    def add_look(self, value):
        self.add_key(Config.look, value)

    def add_task(self, value):
        self.add_key(Config.task, value)

    def is_default(self):
        self.add_key(Config.default, True)

    def add_preview(self, value):
        self.data[Config.preview] = value

    def add_version(self, value):
        self.data[Config.version] = value

    def add_version_id(self, value):
        self.data[Config.id] = value

    def add_description(self, value):
        self.data[Config.description] = value

    def add_workspace(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.workspace, value)

    def add_gpu(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.gpu, value)

    def add_mtr(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.mtr, value)

    def add_mtr_data(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.mtrData, value)

    def add_groom_yeti(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.yetiGroom, value)

    def add_groom_yeti_display(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.yetiGroomDisplay, value)

    def add_tech_geo(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.techGeo, value)

    def add_maya(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.maya, value)

    def add_mtr_preview(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.mtrPreview, value)

    def add_mtr_render(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.mtrRender, value)

    def add_cache(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.cache, value)

    def add_anim_data(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.animData, value)

    def add_rig(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.rig, value)

    def add_media(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.media, value)

    def add_design(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.design, value)

    def add_rsproxy(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.rsproxy, value)

    def add_maya_rsproxy(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.mayaRsProxy, value)

    def add_aistandin(self, publishedFile='', heroFile=''): 
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.aistandin, value)

    def add_set(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.asmset, value)

    def add_cam(self, publishedFile='', heroFile=''):
        value = self.add_files(publishedFile, heroFile)
        self.add_key(Config.camera, value)

    def add_key(self, key, value):
        self.data[key] = value

    def add_files(self, publishedFile, heroFile):
        data = OrderedDict()
        data['publishedFile'] = publishedFile
        data['heroFile'] = heroFile
        return data

    def get_task(self):
        return self.get(Config.task)

    def get_preview(self):
        return self.get(Config.preview)

    def get_version(self):
        return self.get(Config.version)

    def get_version_id(self):
        return self.get(Config.id)

    def get_description(self):
        return self.get(Config.description) or ''

    def get_process(self):
        return self.get(Config.process)

    def get_look(self):
        return self.get(Config.look)

    def get_gpu(self):
        return self.get(Config.gpu)

    def get_mtr_render(self):
        return self.get(Config.mtrRender)

    def get_mtr(self):
        return self.get(Config.mtr)

    def get_mtr_preview(self):
        return self.get(Config.mtrPreview)

    def get_rsproxy(self):
        return self.get(Config.rsproxy)

    def get_cache(self):
        return self.get(Config.cache)

    def get_rig(self):
        return self.get(Config.rig)

    def get_set(self):
        return self.get(Config.asmset)

    def get_camera(self):
        return self.get(Config.camera)

    def get_groom_yeti(self):
        return self.get(Config.yetiGroom)

    def get_groom_yeti_display(self):
        return self.get(Config.yetiGroomDisplay)

    def get_tech_geo(self):
        return self.get(Config.techGeo)

    def get_workspace(self):
        return self.get(Config.workspace)

    def get_maya(self): 
        return self.get(Config.maya)

    def get_media(self):
        return self.get(Config.media)

    def get_design(self):
        return self.get(Config.design)

    def keys(self):
        return self.data.keys()

    def get(self, key):
        return self.data[key] if key in self.data.keys() else OrderedDict()


class Member(object):
    """docstring for Member"""
    def __init__(self, name='', data=None):
        super(Member, self).__init__()
        if not data:
            self.data = OrderedDict()
            self.name = name
            self.data[self.name] = OrderedDict()
        else:
            self.data = self.init_data(data)

    def init_data(self, inputData):
        data = OrderedDict()
        self.name = inputData.keys()[0]
        data[self.name] = inputData[self.name]
        return

    def set_member(self, member):
        """ set dict member """
        self.set_object(member)
        data = member.data if type(member) == type(Member()) else member
        self.data[self.name][member.name] = member.data[member.name]

    def set_add(self):
        """ set to list mode """
        self.data[self.name] = []

    def add_member(self, member):
        """ append for list mode """
        self.set_object(member)
        appendData = member.data if type(member) == type(Member()) or type(member) == type(Output()) else member
        self.data[self.name].append(appendData)

    def set_object(self, member):
        self.child = member

    def get_data(self):
        return self.data[self.name]

    def get(self, key):
        return self.get_data().get(key)

    def member_keys(self):
        return self.get_data().keys()


class Step(Member):
    """docstring for Step"""
    def __init__(self, name):
        super(Step, self).__init__(name=name)

    def set_object(self, member):
        self.res = member

class Res(Member):
    """docstring for Res"""
    def __init__(self, name):
        super(Res, self).__init__(name=name)

    def set_object(self, member):
        self.process = member

class Process(Member):
    """docstring for Process"""
    def __init__(self, name):
        super(Process, self).__init__(name=name)

    def set_object(self, member):
        self.output = member


def compare_dict(dict1, dict2):
    pass