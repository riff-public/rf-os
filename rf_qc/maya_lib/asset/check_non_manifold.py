import maya.cmds as mc

def run(entity=None):
    """ Check for Non-Manifold polygon. """
    mdGeoGrp = '%s' % (entity.projectInfo.asset.md_geo_grp()) if entity else 'MdGeo_Grp'
    nonManifold = []
    if mc.objExists(mdGeoGrp):
        geos = []
        for tr in mc.listRelatives(mdGeoGrp, ad=True, pa=True, type='transform'):
            shapes = mc.listRelatives(tr, shapes=True, ni=True, pa=True)
            if shapes:
                geos.append(tr)
        mc.select(geos, r=True)
        nonManifold = mc.polyInfo( nonManifoldEdges=True , nonManifoldUVEdges=True , nonManifoldUVs=True , nonManifoldVertices=True)
        if nonManifold:
            mc.select(cl=True)
            nf_geos = set()
            for nf in nonManifold:
                nfg = nf.split('.')[0]
                nf_geos.add(nfg)
            mc.select(list(nf_geos) + nonManifold, r=True)
    else: 
        return [False, 'MdGeoGrp: %s not found!' %mdGeoGrp]

    if not nonManifold:
        return [True, 'No Non-Manifold Polygon.']
    else:
        msg = 'Found Non-Manifold polygon!\nTo select, Right-click, choose Resolve.\n======== Non-Manifold ========'
        return [False, '%s\n%s' %(msg, '\n'.join(nonManifold))]

def resolve(entity=None):
    result, msg = run(entity)
    if not result:
        nonManifold = msg.split('\n')[3:]
        mc.select(nonManifold, r=True)
        # mc.hilite(nGons)
    return None