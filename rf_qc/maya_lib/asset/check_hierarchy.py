# qc duplicated name
import sys
import os
import shutil
from collections import OrderedDict
from pprint import pprint

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_utils.context import context_info
from rf_utils.pipeline import check 


def run(entity=None):
    """ Check if hierarchy against Groom's TechGeo_Grp """
    result = False 
    checkGrp, dataPath = get_data(entity)

    if not entity.res: 
        message = 'Check if you select "resolution", pr, md, hi'
        result = False
    else:
        if entity.res != 'md':
            return [True, 'Check skipped. Resolution is not md']

        if os.path.exists(dataPath):
            if not mc.objExists(checkGrp):
                message = 'Cannot find %s' %checkGrp
                result = False
            else:
                checker = check.CheckGeo(checkGrp, str(dataPath))
                checkResult = checker.check()

                # details 
                if checkResult[check.Config.mismatch] or checkResult[check.Config.missing] or checkResult[check.Config.alien]: 
                    message = 'Check failed, see script editor for details.'
                    result = False 
                    pprint(checkResult)
                else: 
                    message = 'All geo matched.'
                    result = True       
        else:
            message = 'Model ABC does not exits: %s' %dataPath
            result = True

    return [result, message]

def resolve(entity=None):
    """ Select non matching geos """
    checkGrp, dataPath = get_data(entity)
    if os.path.exists(dataPath) and mc.objExists(checkGrp): 
        checker = check.CheckGeo(checkGrp, str(dataPath))
        checkResult = checker.check()

        # we do not include missing geo here cuz we can't select them
        unmatchedGeo = checkResult[check.Config.mismatch] + checkResult[check.Config.alien]
        if unmatchedGeo: 
            mc.select(unmatchedGeo)
        return True 
    else:
        return False


def skip(entity=None):
    if entity.step == context_info.Step.rig:
        return True
    else:
        logger.error('Cannot skip this QC')
        return False


def get_data(entity): 
    # checkGrp 
    localEntity = entity.copy()
    checkGrp = localEntity.projectInfo.asset.md_geo_grp()
    # with namespace 
    lsGrp = mc.ls('*:%s' % (checkGrp))
    if lsGrp:
        checkGrp = lsGrp[0]

    # find data path 
    localEntity.context.update(step='model', process='main')
    publishPath = localEntity.path.output_hero().abs_path()
    # dataFile = localEntity.output(outputKey='hierarchy')
    dataFile = localEntity.output(outputKey='cache')
    dataPath = '%s/%s' % (publishPath, dataFile)
    print dataPath

    return checkGrp, dataPath
