# qc duplicated name
import maya.cmds as mc
import maya.mel as mm

def run(entity=None):
	""" Remove unknown reference nodes """
	refs = mc.ls(type='reference')
	removeRefs = [a for a in refs if '_UNKNOWN_REF_NODE_' in a]

	for node in removeRefs:
		try:
			mc.delete(node)
		except Exception as e:
			logger.error(e)
			logger.warning('%s failed to delete' % node)

	return True

def resolve(entity=None):
	""" Remove unknown nodes """
	return run()