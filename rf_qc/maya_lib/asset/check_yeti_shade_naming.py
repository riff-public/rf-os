import maya.cmds as mc
import pymel.core as pm
import maya.mel as mm
# from rf_utils import custom_exception

except_names = ('lambert1')

def run(entity=None):
    """ Check Yeti shader naming """ 
    invalid_shaders = get_invalid_names()
    if invalid_shaders:
        return [False, 'Found groom shaders with incorrect naming.\nCorrect naming is: \n"<variant>_Fur_<element>_Shd"\nexample\n  Duck001:Yellow_Fur_Eyebrow_Shd\n  Duck001:Main_Fur_Eyebrow_Shd\n\nShader List:\n%s' %('\n'.join([n.nodeName() for n in invalid_shaders]))]
    else:
        return [True, 'All groom shaders has correct naming.']

def get_invalid_names():
    yetiNodes = pm.ls(type='pgYetiMaya')
    result = set()
    for node in yetiNodes:
        sgs = node.outputs(type='shadingEngine')
        nodeTr = node.getParent()
        elem = nodeTr.nodeName().split('_')[0]
        for sg in sgs:
            shaders = sg.surfaceShader.inputs()
            if shaders:
                curr_name = shaders[0].nodeName()
                if curr_name in except_names:
                    continue
                splits = curr_name.split('_')
                if len(splits) < 2 or splits[1] != 'Fur' or not curr_name.endswith('_Shd'):
                    result.add(shaders[0])

    return result

def resolve(entity=None):
    invalid_shaders = get_invalid_names()
    if invalid_shaders:
        pm.select(invalid_shaders, r=True)
    return False