# qc duplicated name
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_utils.context import context_info

def run(entity=None): 
    """ Check if there is only 1 asset per cycle """ 
    # project start frame 
    # looking for AllMover_Grp
    targets = get_target_grp(entity)
    if entity.type == 'animated': 
        return [True, '']

    if len(targets) == 1: 
        target = targets[0]
        status, refEntity = check_asset_match(entity, target)
        if status: 
            return [True, '']
        else: 
            return [False, 'Wrong asset inuse "%s". Please replace to "%s"' % (refEntity.name, entity.name)]
    else: 
        return [False, 'More than 1 rig found!. Please keep only 1 rig']

def resolve(entity=None): 
    targets = get_target_grp(entity)
    if not len(targets) == 1: 
        message = 'Please keep only 1 rig and remove others to fix this problem'
        mc.confirmDialog( title='Confirm', message=message, button=['OK'])
        return False 

    else: 
        target = targets[0]
        status, refEntity = check_asset_match(entity, target)
        if not status: 
            message = 'Wrong asset inuse "%s". Please replace to "%s"' % (refEntity.name, entity.name)
            mc.confirmDialog( title='Confirm', message=message, button=['OK'])
            return False 
        else: 
            return True



def check_asset_match(entity, target): 
    """ check if asset inuse match asset workspace """ 

    refPath = mc.referenceQuery(target, f=True)
    refEntity = context_info.ContextPathInfo(path=refPath)
    if not entity.name == refEntity.name: 
        return False, refEntity
    return True, refEntity

def get_target_grp(entity): 
    targetGrp = entity.projectInfo.asset.allmover_grp() or 'AllMover_Grp'
    targets = mc.ls('*:%s' % targetGrp)
    return targets
