# template for export mod
import os
import sys
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.sg import sg_process
sg = sg_process.sg

class SGConfig: 
	taskMap = {'crowd': ['oa'], 'model': ['model'], 'anim': []}
	entityMap = {'asset': 'Asset', 'scene': 'Shot'}

def run(entity=None): 
	""" Check if tasks are ready for publish on this process """
	result, messages = check_asset_task(entity)
	return result, messages

def resolve(entity=None): 
	result, messages = check_asset_task(entity, resolve=True)
	return result


def skip(entity=None): 
	return True


def check_asset_task(entity=None, resolve=False): 
	entity.context.use_sg(sg, entity.project, entityType=entity.entity_type, entityName=entity.name)
	entity.context.sgEntity 
	taskNames = SGConfig.taskMap.get(entity.step)
	if taskNames: 
		filters = [['project', 'is', entity.context.sgEntity.get('project')], 
				['entity', 'is', entity.context.sgEntity], 
				['step.Step.code', 'is', entity.step]]
		fields = ['content', 'id', 'sg_status_list', 'step']
		taskEntities = sg.find('Task', filters=filters, fields=fields)
		
		missingTasks = [a for a in taskNames if not a in [a.get('content') for a in taskEntities]]
		
		if not missingTasks: 
			return True, 'Task ready'
		else: 
			if resolve: 
				create_task(entity, missingTasks)
				return True, 'Task ready'
			return False, 'Task missing %s' % missingTasks
	return False, 'Task missing'


def create_task(entity, tasks): 
	stepEntity = find_step(entity, entity.step)
	results = []
	if stepEntity: 
		for task in tasks: 
			data = {'project': entity.context.sgEntity.get('project'), 
					'content': task, 
					'sg_type': SGConfig.entityMap.get(entity.entity_type), 
					'step': stepEntity, 
					'entity': entity.context.sgEntity}
			result = sg.create('Task', data)
			logger.debug('Task created')
			logger.debug(result)
			results.append(result)
	return results 



def find_step(entity, step): 
	stepEntity = sg.find_one('Step', [['code', 'is', step], 
									['entity_type', 'is', SGConfig.entityMap.get(entity.entity_type)]], 
									['code', 'id', 'entity_type'])
	
	return stepEntity
