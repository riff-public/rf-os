#!/usr/bin/env python
# -- coding: utf-8 --
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_maya.lib import maya_lib
reload(maya_lib)

def run(entity=None):
	""" Remove non reference namespaces """
	# check if any references
	refs = mc.file(q=True, r=True)
	namespaces = maya_lib.non_ref_namespace()
	return True if not namespaces else False


def resolve(entity=None):
	""" Checking for Rig_Grp and Geo_Grp """
	namespaces = maya_lib.non_ref_namespace()
	maya_lib.remove_namespaces2(namespaces)
	result = maya_lib.non_ref_namespace()
	return True if not result else False

def doc(entity=None): 
	""" ลบ namespace ของที่ไม่ใช่ reference ออก """
	return 