import os

import maya.cmds as mc
import pymel.core as pm

from rf_utils import ascii_utils
reload(ascii_utils)

def run(entity=None):
    """ Check existing controller names against previous version & proxy rig"""
    missing_in_md, missing_in_pxy = check(entity=entity)

    if missing_in_md or missing_in_pxy:
        missing_list_text = 'Found missing control(s)'
        if missing_in_md:
            missing_list_text += '\nRig_Md: \n - {}'.format('\n - '.join(missing_in_md))
        if missing_in_pxy:
            missing_list_text += '\nRig_Pr: \n - {}'.format('\n - '.join(missing_in_pxy))
        # print(missing_list_text)
        return [False, missing_list_text]
    else:
        return [True, 'No missing controller found.']

def check(entity):
    existing_ctrls = [removeDagPathNamespace(path=c) for c in mc.ls(type=['transform', 'joint']) if c.endswith('_ctrl') or c.endswith('_Ctrl')]
    # print('existing', existing_ctrls)
    # check against rig_md
    md_rig_path = get_rig_path(entity, res='md')
    missing_in_md = []
    if md_rig_path and os.path.exists(md_rig_path):
        md_ctrls = read_ascii_ctrls(path=md_rig_path)
        # print('rig_md', md_ctrls)
        missing_in_md = [c for c in md_ctrls if c not in existing_ctrls]

    # check against rig_pr
    pxy_rig_path = get_rig_path(entity, res='pr')
    missing_in_pxy = []
    if pxy_rig_path and os.path.exists(pxy_rig_path):
        pxy_ctrls = read_ascii_ctrls(path=pxy_rig_path)
        # print('rig_pr', pxy_ctrls)
        missing_in_pxy = [c for c in pxy_ctrls if c not in existing_ctrls]
    return missing_in_md, missing_in_pxy

def skip(entity=None):
    return True

def resolve(entity=None):
    return run(entity)

def removeDagPathNamespace(path):
    root = False
    if path.startswith('|'):
        root = True
        path = path[1:]
    splits = path.split('|')
    cleaned_elems = []
    for s in splits:
        if '.f[' in s:
            fSplit = s.split('.f[')
            elem = fSplit[0]
            fidx = '.f[%s' %fSplit[1]
        else:
            elem = s
            fidx = ''

        elem = '%s%s' %(elem.split(':')[-1], fidx)
        cleaned_elems.append(elem)

    result = '|'.join(cleaned_elems)
    if root:
        result = '|%s' %(result)
    return result

def read_ascii_ctrls(path):
    ma = ascii_utils.MayaAscii(path=path)
    ma.read()
    ctrls = [c[1] for c in ma.ls(search='', types=['transform', 'joint'], fullPath=False) if c[1].endswith('_ctrl') or c[1].endswith('_Ctrl')]
    return ctrls

def get_rig_path(entity, res):
    entity2 = entity.copy()
    entity2.context.update(step='rig', process='main', res=res)
    name = entity2.output_name(outputKey='rig', hero=True, ext='.ma')
    path = '%s/%s' % (entity2.path.hero().abs_path(), name)
    return path