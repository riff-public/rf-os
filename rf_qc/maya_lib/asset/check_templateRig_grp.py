# qc Grp in Export_Grp
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_utils.context import context_info
from rf_utils import file_utils
import os , sys

#path project config
rootScript = os.environ['RFSCRIPT']
pathProcessJson = r"{}\core\rf_maya\rftool\rig\config\export_rig_config.json".format(rootScript)
processData = file_utils.json_loader(pathProcessJson)

def run(entity=None): 
	"""Check template rig grp in export group"""
	# search for grp in export grp, check from current process
	processName = entity.process

	expGrp = 'Export_Grp'
	delGrp = 'Delete_Grp'
	
	#check export group in delete group
	if mc.objExists(expGrp):
		prnt = mc.listRelatives(expGrp,ap=True)
		if prnt:
			return [ False , 'Found export group in other group.']

	if mc.objExists(expGrp):
		allExprtGrp = mc.listRelatives(expGrp,c=True)
		dsntFndGrp = []
		delGrp = []
		if allExprtGrp:
			for eachData in processData[processName]:
				if not eachData in allExprtGrp:
					dsntFndGrp.append(eachData)
				else:
					delGrp.append(eachData)

			#remove grp from allExportGrp
			for eachDel in delGrp:
				allExprtGrp.remove(eachDel)

			if dsntFndGrp != []:
				return [False, 'This asset dose not have {} in Export_Grp'.format(dsntFndGrp)]
			else:
				return [True, '']
		else:
			return [False, 'Does not have object in Export_Grp']
				
	else:
		return [False, 'Does not found Export_Grp!. Please create Export_Grp']


def skip(entity=None):
	return True


def resolve(entity=None):
	""" Rename Grp """
	# keep only 1 shot that connect to sequencer
	processName = entity.process
	tmpGrp = processData[processName][:]
	delGrp = []
	expGrp ='Export_Grp'
	check = mc.objExists(expGrp)

	for each in tmpGrp:
		#if export grp does not have child
		if not mc.objExists(each):
			return [False, '{} Does not exist, Please recheck grp in outliner.'.format(each)]

	if check:
		prnt = mc.listRelatives(expGrp,ap=True)
		if prnt:
			mc.parent(expGrp,w=True)

		#check child in export grp
		childGrp = mc.listRelatives(expGrp,c=True)
		if childGrp:
			for eachData in tmpGrp:
				if mc.objExists(eachData):
					if not eachData in childGrp:
						mc.parent(eachData,expGrp)
					delGrp.appends(eachData)
		if not childGrp:
			mc.parent(tmpGrp,expGrp)
			delGrp = tmpGrp[:]

		#remove delGrp from tmp
		for eachDel in delGrp:
			tmpGrp.remove(eachDel)

		if tmpGrp == []:
			return True

		else:
			return [False, '{} not found or does not parent to export group.'.format(tmpGrp)]
		
	else:
		return [False, 'Export_Grp does not exist.']
