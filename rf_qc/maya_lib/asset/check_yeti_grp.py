# qc duplicated name
import maya.cmds as mc
import maya.mel as mm
# from rf_utils import custom_exception

def run(entity=None):
    """ Check for Yeti_Grp """
    yetiGrp = '%s' % (entity.projectInfo.asset.groom_grp()) if entity else 'Yeti_Grp'
    yetiGeoGrpName = entity.projectInfo.asset.groom_geo_grp() if entity else 'YetiGeo_Grp'
    yetiNodeGrpName = entity.projectInfo.asset.groom_node_grp() if entity else 'YetiNode_Grp'

    yetiGrp_ls = mc.ls('|%s' %yetiGrp)
    result = False
    if yetiGrp_ls:
        children = mc.listRelatives(yetiGrp_ls[0], children=True)
        
        if yetiGeoGrpName in children and yetiNodeGrpName in children and len(children) == 2:
            result = True

    if not result:
        return [False, '%s, with 2 children (%s and %s) does not exist.' %(yetiGrp, yetiGeoGrpName, yetiNodeGrpName)]
    else:
        return [True, '%s found.' %yetiGrp]

def resolve(entity=None):
    return run(entity)
