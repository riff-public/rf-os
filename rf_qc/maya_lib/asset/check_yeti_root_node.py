import maya.cmds as mc
# import pymel.core as pm

def run(entity=None):
    """ Check Yeti root node must ends with attribute node --> transform node """
    yetiNodes = mc.ls(type='pgYetiMaya')
    incorrect_nodes = []

    for yetiNode in yetiNodes:
        rootNode = mc.pgYetiGraph(yetiNode, getRootNode=True)
        nodeType = mc.pgYetiGraph(yetiNode, node=rootNode, nodeType=True)
        if not nodeType == 'transform':
            incorrect_nodes.append(yetiNode)
            continue
        
        for node in mc.pgYetiGraph(yetiNode, listNodes=True):
            nodeType = mc.pgYetiGraph(yetiNode, node=node, nodeType=True)
            if nodeType == 'attribute':
                break
        else:
            incorrect_nodes.append(yetiNode)

    if incorrect_nodes:
        return [False, 'Check these nodes for attibute and transform node at the end\n%s' %('\n'.join(incorrect_nodes))]
    else:
        return [True, 'All yeti node has attribute node and transform node as root.']
    
def skip(entity=None):
    return False

def resolve(entity=None):
    status, result = run(entity)
    if not status:
        yetiNodes = result.split('\n')[1:]
        yetiTrs = []
        for yn in yetiNodes:
            tr = mc.listRelatives(yn, pa=True, parent=True)[0]
            yetiTrs.append(tr)
        mc.select(yetiTrs)
    return False