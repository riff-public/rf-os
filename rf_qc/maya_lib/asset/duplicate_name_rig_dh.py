# qc duplicated name
from collections import defaultdict

import pymel.core as pm
import maya.cmds as mc
import maya.mel as mm
from rf_maya.lib import maya_lib

def run(entity=None):
    """ Check for node with same name """
    if 'bsh' in entity.process:
        return [True, 'Ignored in blendShape process']

    found_duplicate, same_names = check()

    # create display string
    if found_duplicate:
        msg = ''
        for sn, objs in same_names.iteritems():
            if len(objs) > 1:
                msg += sn
                for obj in objs:
                    msg += '\n  - %s' %obj.shortName()
                msg += '\n'
        return [False, msg]
    else:
        return [True, 'No duplicated names']

def check():
    same_names = defaultdict(list)  # {nodeName:[pyObj1, pyObj2, ...]}
    found_duplicate = False
    excepts = [ 'Delete_Grp' , 'Bsh_Grp' ]
    for obj in pm.ls(dag=True):
        pars = obj.longName().split('|')[1]
        if not pars in excepts :
            objName = obj.nodeName()
            same_names[objName].append(obj)
            if not found_duplicate and len(same_names[objName]) > 1:
                found_duplicate = True

    return found_duplicate, same_names

def resolve(entity=None):
    """ run resolution return check result """
    found_duplicate, same_names = check()

    for name, trs in same_names.iteritems():  # {nodeName:[pyObj1, pyObj2, ...]}
        if len(trs) > 1:
            for i, tr in enumerate(trs):
                ii = i + 1
                exit = False

                # naming 
                nsplits = name.split('_')
                while not exit:
                    # try not to break the naming convention
                    if len(nsplits) > 1:
                        new_name = '%s%s' %(nsplits[0], ii)
                        new_name += '_%s' %'_'.join(nsplits[1:])
                    else:
                        new_name = '%s%s' %(name, ii)

                    # if the new name already exists
                    if pm.objExists(new_name):
                        ii += 1
                    else:
                        exit = True

                tr.rename(new_name)

                # shp = tr.getShape(ni=True)
                # if shp:
                #     new_name_shp = ('%sShape'%new_name)
                #     shp.rename(new_name_shp)

    return True
