import sys
import os 
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import pymel.core as pm
from rftool.utils import maya_utils
reload(maya_utils)
from rf_utils import file_utils
from rf_utils import custom_exception

def run(entity=None):
    """ Set unique texture name for yeti """
    replaceDict = get_texture_info(entity)
    message = str()

    for k, v in replaceDict.iteritems(): 
        message += '%s -> %s\n' % (os.path.basename(k), os.path.basename(v))

    if not replaceDict: 
        return True, 'Names are unique'

    else: 
        return False, 'Texture name not unique, rename the following\n%s' % message


def skip(entity=None):
    return True

def resolve(entity=None):
    replaceDict = get_texture_info(entity)
    targetGrp = entity.projectInfo.asset.groom_grp() or 'Yeti_Grp'
    nodes = maya_utils.find_texture_node(targetGrp=targetGrp)

    if replaceDict: 
        for k, v in replaceDict.iteritems(): 
            if not os.path.dirname(v): 
                os.makedirs(os.path.dirname(v))
            file_utils.xcopy_file(k, v)
            logger.debug('Copy %s -> %s' % (k, v))
        maya_utils.relink_texture_node(list(set(nodes)), replaceDict)

    result = run(entity)
    
    if not result: 
        raise custom_exception.PipelineError(result[1])

    return result[0]


def get_texture_info(entity=None): 
    targetGrp = entity.projectInfo.asset.groom_grp() or 'Yeti_Grp'
    nodes = maya_utils.find_texture_node(targetGrp=targetGrp)
    textures = maya_utils.collect_textures(targetGrp=targetGrp)
    replaceDict = dict()
    localTextureWs = entity.path.scheme('localTexturePath').abs_path()

    for texture in textures: 
        basename = os.path.basename(texture)
        newName = add_groom_prefix(basename)
        
        if newName: 
            dst = '%s/%s' % (localTextureWs, newName)
            src = texture
            replaceDict[src] = dst

    return replaceDict


def detect_last_element(filename): 
    """ find last element that is not digit or UDIM convention """ 
    name, ext = os.path.splitext(filename)
    if name.isdigit(): 
        return None 

    elems = name.split('_')
    if elems[-1][-2:-1] in ['u', 'v']: 
        return detect_last_element(('_').join(elems[:-1]))

    if not elems[-1].isdigit(): 
        return elems[-1]
    else: 
        return detect_last_element(('_').join(elems[:-1]))

def add_groom_prefix(filename): 
    """ add Gr at element 3 """ 
    suffix = 'Gr'
    # targetElement = filename.split('_')[2]
    targetElement = detect_last_element(filename)
    if not targetElement[-2:] == suffix: 
        name = '%s%s' % (targetElement, suffix)
        newName = filename.replace(targetElement, name)
        return newName

    # return filename

