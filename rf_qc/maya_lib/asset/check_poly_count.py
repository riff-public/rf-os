# qc scene poly count
import maya.cmds as mc
from maya.api import OpenMaya as om

LOCAL_POLY_COUNT_LIMIT = 1000000

def run(entity=None):
    """ Check polygon count """
    limit = get_poly_limit(entity)
    if not limit:
        limit = LOCAL_POLY_COUNT_LIMIT
    curr_poly_count = getScenePolyCount()
    if curr_poly_count > limit:
        return [False, 'Polygon count is more than %s.' %limit]
    else:
        return [True, 'Polygon count is acceptable.']


def skip(entity=None):
    return True

def resolve(entity=None):
    return None

def get_poly_limit(entity): 
    from rf_utils.sg import sg_utils
    sg = sg_utils.sg 
    filters = [['project.Project.name', 'is', entity.project], ['code', 'is', entity.name]]
    fields = ['sg_poly_limit']
    result = sg.find_one('Asset', filters, fields)
    if result:
        return result.get('sg_poly_limit')  

def getScenePolyCount():
    dagIt = om.MItDag(om.MItDag.kDepthFirst, om.MFn.kMesh)

    poly_count = 0
    while not dagIt.isDone():
        mDag = dagIt.getPath()
        meshFn = om.MFnMesh(mDag)
        poly_count += meshFn.numPolygons
        dagIt.next()

    return poly_count