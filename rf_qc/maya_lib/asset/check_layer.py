# qc duplicated name 
import maya.cmds as mc 
import maya.mel as mm 

allowLayers = ['defaultLayer']

def run(entity=None): 
	""" Check for display layers """ 
	layers = mc.ls(type='displayLayer')
	result = check_layers(layers)
	return True if result == 0 else False 


def resolve(entity=None): 
	""" Please remove all display layers """ 
	layers = mc.ls(type='displayLayer')
	result = check_layers(layers)

	if result != 0: 
		for layer in result:
			if not layer.endswith(('DynFk_dlyr')):
				mc.delete(layer)
	return run() 

def check_layers(layers): 
	invalidLayers = []
	for layer in layers: 
		if not any(a in layer for a in allowLayers):
			invalidLayers.append(layer)

	return 0 if not invalidLayers else invalidLayers