import math

import maya.cmds as mc
import pymel.core as pm
import maya.OpenMaya as om

MAX_VERT_COUNT = 500000

def run(entity=None):
    """ Check if there's any UV shell outside of a space """
    checkGrp = entity.projectInfo.asset.md_geo_grp()
    geoGrp = pm.PyNode(checkGrp)

    invalidUVGeos = set()
    for mesh in geoGrp.getChildren(ad=True, type='mesh', ni=True):
        meshName = mesh.getParent().shortName()
        if mesh.numVertices() > MAX_VERT_COUNT:
            invalidUVGeos.add('SKIPPED, POLY TOO DENSE: %s' %meshName)
            continue
        shells = getUvShelList(mesh.longName())
        for sid, uvs in shells.iteritems():
            us = sorted(uvs['u'])
            vs = sorted(uvs['v'])
            minU = us[0]
            maxU = us[-1]
            minV = vs[0]
            maxV = vs[-1]
            if minU < 0.0 or minV < 0.0 or math.ceil(minU) != math.ceil(maxU) or math.ceil(minV) != math.ceil(maxV):
                invalidUVGeos.add(meshName)

    if invalidUVGeos:
        msg = 'These geos has invalid UV layout\n'
        msg += '\n'.join(list(invalidUVGeos))
        return [False, msg]
    else:
        return [True, 'No overlapping UV shell or any negative UV.']

def skip(entity=None):
    return True

def resolve(entity=None):
    status, result = run(entity)
    if not status:
        objs = result.split('\n')[1:]
        pm.select(objs, r=True)

    return None

def getUvShelList(name, uvSet='map1'):
    selList = om.MSelectionList()
    selList.add(name)
    pathToShape = om.MDagPath()
    selList.getDagPath(0, pathToShape)

    meshNode = pathToShape.fullPathName()
    uvSets = mc.polyUVSet(meshNode, query=True, allUVSets=True)

    shells = {}  # {shellId: {'u':[us..], 'v':[vs..]}}
    if uvSet in uvSets:
        shapeFn = om.MFnMesh(pathToShape)
        shellMSUtil = om.MScriptUtil()
        shellMSUtil.createFromInt(0)
        nbUvShells = shellMSUtil.asUintPtr()

        uArray = om.MFloatArray()   #array for U coords
        vArray = om.MFloatArray()   #array for V coords
        uvShellIds = om.MIntArray() #The container for the uv shell Ids

        shapeFn.getUVs(uArray, vArray)
        shapeFn.getUvShellsIds(uvShellIds, nbUvShells, uvSet)
        for i, n in enumerate(uvShellIds):
            if n not in shells:
                shells[n] = {'u':[], 'v':[]}
            shells[n]['u'].append(uArray[i])
            shells[n]['v'].append(vArray[i])

    return shells