import pymel.core as pm
from rf_utils.context import context_info

def run(entity=None):
    """ Check if there's any negative scale value on any of the geo transform. """
    geoGrp = str(entity.projectInfo.asset.geo_grp()) if entity else 'Geo_Grp'
    if not pm.objExists(geoGrp):
        raise Exception, 'Cannot find: %s' %(geoGrp)

    geoGrp = pm.PyNode(geoGrp)
    negative_objs = []
    for tr in [geoGrp] + geoGrp.getChildren(ad=True, type='transform'):
        if anyNegativeScale(tr):
            negative_objs.append(tr)

    if negative_objs:
        return [False, 'Objects with negative scale:\n%s' %('\n'.join([o.shortName() for o in negative_objs]))]
    else:
        return [True, 'No object has negative scale.']

def skip(entity=None):
    return False

def anyNegativeScale(obj):
    for axis in ('sx', 'sy', 'sz'):
        if obj.attr(axis).get() < 0.0:
            return True

def resolve(entity=None):
    status, result = run(entity)
    objs = [pm.PyNode(r) for r in result.split('\n')[1:]]
    # for obj in objs:
    pm.makeIdentity(objs, apply=True, t=True, r=True, s=True, n=0, pn=True)

    return True