# qc duplicated name
import sys
import os
import shutil
from collections import OrderedDict

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_utils.context import context_info

globalTextureFolder = "P:/Bikey/asset/publ/_global/textures/shares"

def run(entity=None):
    """ Check if textures are in the pipeline """
    texturePath1 = entity.path.texture().abs_path()
    texturePath2 = entity.path.publish_texture().abs_path()
    wrongPath = check_path(texturePath1, texturePath2)
    
    if not wrongPath:
        return True

    else:
        pathList = ('\n').join(['\nnode "%s" path "%s"' % (k, v) for k, v in wrongPath.iteritems()])
        message = 'wrong path %s' % pathList
        raise custom_exception.PipelineError(message)
        return False


def resolve(entity=None):
    """ fix textures not in the pipeline """
    from rftool.clean.texture_checker import app
    reload(app)
    reload(app.texture_widget)
    myApp = app.show(entity)
    return run(entity)
    # texturePath = entity.path.texture().abs_path()
    # texturePathRes = texturePath
    # # texturePathRes = '%s/%s' % (texturePath, entity.res)
    # wrongPath = check_path(texturePath)

    # for node, path in wrongPath.iteritems():
    #     if os.path.exists(path):
    #         filename = os.path.basename(path)
    #         dstTexturePath = '%s/%s' % (texturePathRes, filename)

    #         if not os.path.exists(texturePathRes):
    #             os.makedirs(texturePathRes)

    #         shutil.copy2(path, dstTexturePath)
    #         mc.setAttr('%s.fileTextureName' % node, dstTexturePath, type='string')
    #         logger.debug('Copy texture %s -> %s' % (path, dstTexturePath))

    # return run(entity)


def skip(entity=None):
    if entity.step == context_info.Step.rig:
        return True
    else:
        logger.error('Cannot skip this QC')
        return False

def check_path(texturePath1, texturePath2):
    """ get textures files in scene """
    fileNodes = mc.ls(type='file')
    data = OrderedDict()

    for fileNode in fileNodes:
        path = mc.getAttr('%s.fileTextureName' % fileNode)

        if globalTextureFolder in path:
            continue

        if not texturePath1.lower() in path.lower():
            if not texturePath2.lower() in path.lower():
                data[fileNode] = path

    return data
