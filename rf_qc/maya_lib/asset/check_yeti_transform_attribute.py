import maya.cmds as mc

def run(entity=None):
    """ Check for Yeti user float attribute tranX, tranY and tranZ and the expression '[tranX,tranY,tranZ]' on root transform node. """
    yetiNodes = mc.ls(type='pgYetiMaya')
    incorrect_nodes = []

    for yn in yetiNodes:
        if not mc.objExists('%s.yetiVariableF_tranX' %yn) or \
         not mc.objExists('%s.yetiVariableF_tranY' %yn) or \
          not mc.objExists('%s.yetiVariableF_tranZ' %yn):
            incorrect_nodes.append(yn)
            continue

        rootNode = mc.pgYetiGraph(yn, getRootNode=True)
        nodeType = mc.pgYetiGraph(yn, node=rootNode, nodeType=True)
        if nodeType != 'transform' or str(mc.pgYetiGraph(yn, node=rootNode, param='translate', getParamValue=True)).replace(' ', '') != '[tranX,tranY,tranZ]':
            incorrect_nodes.append(yn)
            continue

    if incorrect_nodes:
        return [False, 'Check these nodes for tranX, tranY and tranZ user attibutes and expression on transform root node.\n%s' %('\n'.join(incorrect_nodes))]
    else:
        return [True, 'All yeti node has tranX, tranY and tranZ user attributes and expression on transform root node.']
    
def skip(entity=None):
    return False

def resolve(entity=None):
    status, result = run(entity)
    if not status:
        failed = False
        yetiNodes = result.split('\n')[1:]
        for yn in yetiNodes:
            rootNode = mc.pgYetiGraph(yn, getRootNode=True)
            nodeType = mc.pgYetiGraph(yn, node=rootNode, nodeType=True)
            if nodeType != 'transform':
                print 'Root node on %s is not of typed transform' %yn
                failed = True
                continue
            
            for axis in 'XYZ':
                mc.addAttr(yn, ln=("yetiVariableF_tran" + axis), keyable=True, at='double', defaultValue=0.0, softMinValue=0.0, softMaxValue=100.0)
            mc.pgYetiGraph(yn, node=rootNode, param='translate', setParamValueExpr ='[tranX,tranY,tranZ]')

    if failed:
        return False
    else:
        return True