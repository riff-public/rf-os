# qc duplicated name
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_utils.pipeline import asset_tag

tutorial = 'vidpath'

def run(entity=None):
	""" Check for Geo_Grp """
	# check Rig_Grp
	# topGrp = '|%s' % entity.projectInfo.asset.top_grp() if entity else '|Rig_Grp'
	if entity.process == 'main':
		geoGrp = '%s' % (entity.projectInfo.asset.geo_grp()) if entity else 'Geo_Grp'
		if entity.step == 'rig': 
			geoGrp = mc.ls('*:%s' % (geoGrp))
			if (geoGrp == []):
				geoGrp = 'Geo_Grp'

		if mc.ls(geoGrp): 
			check = mc.ls(geoGrp)[0]
			if check:
				project = entity.project
				assetType = entity.type
				assetName = entity.name
				assetId = entity.id
				step = entity.step
				process = entity.process
				publishVersion = entity.publishVersion
				publishPath = entity.path.publish_version()
				asset_tag.add_tag(check, project=project, assetType=assetType, assetName=assetName, assetId=assetId, step=step, process=process, publishVersion=publishVersion, publishPath=publishPath)

			if not check:
				return [False, 'Geo_Grp not found']

			if entity.publishVersion == 'none':
				return [False, 'Resolution not choose']

		else:
			return [False, 'Geo_Grp not found']

		return True if check else False
	return True


def resolve(entity=None):
	""" Checking for Rig_Grp and Geo_Grp """
	return run(entity)

def skip(entity=None): 
	return True

def doc(entity=None): 
	""" Help """ 
	
