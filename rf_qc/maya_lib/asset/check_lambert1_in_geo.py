#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.context import context_info
import logging 
import re
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget import dialog

def run(entity=None): 
    """ Check if lambert1 is assigned in Geo """ 
    con = mc.listConnections('lambert1.outColor')
    name = mc.listConnections(con, type="mesh")
    return True if not name else False 

def skip(entity=None):
    return True

def resolve(entity=None): 
    """ list Geo with lambert1 """ 
    con = mc.listConnections('lambert1.outColor')
    name = mc.listConnections(con, type="mesh")

    if name:
        #-*-coding: utf-8 -*-
        # message = ', '.join(name)
        dialog.MessageBox.warning('Select Geos with lambert1')

        mc.select(name)

        return False
    else:
        return True

def doc(entity=None): 
    """ เช็คว่ามี lambert1 ถูก assign อยู่ใน Geo ไหม  """ 
    return 