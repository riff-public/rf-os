import pymel.core as pm
from rf_utils.context import context_info

def run(entity=None):
    """ Check if there's any connection (key, constraint, direct connect) to any of the geo transform. """
    if entity.step == 'groom':
        geoGrp = str(entity.projectInfo.asset.groom_geo_grp()) if entity else 'YetiGeo_Grp'
    else:
        geoGrp = str(entity.projectInfo.asset.geo_grp()) if entity else 'Geo_Grp'
    if not pm.objExists(geoGrp):
        raise Exception, 'Cannot find: %s' %(geoGrp)

    geoGrp = pm.PyNode(geoGrp)
    connected_objs = []
    for tr in [geoGrp] + geoGrp.getChildren(ad=True, type='transform'):
        if anyConnection(tr):
            connected_objs.append(tr)

    if connected_objs:
        return [False, 'Objects with keys or connections:\n%s' %('\n'.join([o.shortName() for o in connected_objs]))]
    else:
        return [True, 'No keys or connections found.']

def skip(entity=None):
    return True

def anyConnection(obj):
    for attr in ('t', 'r', 's'):
        if obj.attr(attr).inputs():
            return True
        for axis in ('x', 'y', 'z'):
            if obj.attr('%s%s' %(attr, axis)).inputs():
                return True

def resolve(entity=None):
    status, result = run(entity)
    objs = [pm.PyNode(r) for r in result.split('\n')[1:]]
    for obj in objs:
        for attr in ('t', 'r', 's'):
            trAttr = obj.attr(attr)
            if trAttr.inputs():
                trAttr.disconnect()
        for axis in ('x', 'y', 'z'):
            axisAttr = obj.attr('%s%s' %(attr, axis))
            if axisAttr.inputs():
                axisAttr.disconnect()
        if obj.visibility.isConnected():
            obj.visibility.disconnect()
    
    return True