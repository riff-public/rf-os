#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys
from datetime import datetime
import maya.cmds as mc 
import maya.mel as mm 

from rf_utils.widget import dialog 

allowLayers = ['defaultLayer']

def run(entity=None): 
    """ Check asset lockdown """ 
    lockdown = check_status(entity.copy())
    messages = check_status_message(entity.copy())
    mesg = 'Asset นี้ ไม่อนุญาติให้แก้ไข'
    mesg += '\nเนื่องจากส่งไป render นอกบ้านแล้ว แก้ไขตอนนี้จะเป็นการเพิ่มงานให้ทุกฝ่าย'

    if messages: 
        mesg = messages[-1]

    if lockdown: 
        write_log(entity)
        dialog.MessageBox.error('Update NOT allow', mesg)
    return False if lockdown else True 


def resolve(entity=None): 
    """ No cannot resolve """ 
    mesg = 'หากจำเป็นต้องแก้จริงๆ ติอต่อพี่ต่ายเพื่อวางแผนการแก้งานของแผนกที่เกี่ยวข้อง'
    dialog.MessageBox.warning('Cannot Publish!', mesg)
    return False


def check_status(entity): 
    from rf_utils.sg import sg_process 
    entity.context.use_sg(sg_process.sg, project=entity.project, entityType='asset', entityName=entity.name)
    tasks = sg_process.get_tasks_by_step(entity.context.sgEntity, entity.step)
    lockdown = any([a.get('sg_lockdown') for a in tasks])
    return lockdown


def check_status_message(entity): 
    from rf_utils.sg import sg_process 
    entity.context.use_sg(sg_process.sg, project=entity.project, entityType='asset', entityName=entity.name)
    tasks = sg_process.get_tasks_by_step(entity.context.sgEntity, entity.step)
    lockdown = any([a.get('sg_lockdown') for a in tasks])
    messages = [a.get('sg_lockdown_message') for a in tasks if a.get('sg_lockdown_message')]
    return messages


def write_log(entity): 
    date = str(datetime.now()).split(' ')[0]
    logName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]
    logPath = '%s/%s/_data/%s/%s/%s/%s.txt' % (os.environ['RFPROJECT'], entity.project, logName, date, entity.name, logName)
    data = '%s: %s was trying to update model %s\n' % (str(datetime.now()), os.environ.get('RFUSER'), entity.name)

    if not os.path.exists(os.path.dirname(logPath)): 
        os.makedirs(os.path.dirname(logPath))
    writeFile(logPath, data)


def writeFile(file, data) :
    mode = 'a' if os.path.exists(file) else 'w'
    f = open(file, mode)
    f.write(data)
    f.close()
    return True

    # [
    # {'project': {'type': 'Project', 'id': 204, 'name': 'SevenChickMovie'}, 
    # 'sg_name': None, 'step': {'type': 'Step', 'id': 14, 'name': 'Model'}, 
    # 'sg_lockdown': False, 'entity.Asset.sg_asset_type': 'prop', 
    # 'entity': {'type': 'Asset', 'id': 6774, 'name': 'bonsaiCourtyardDisplay'}, 
    # 'content': 'modelUv', 'entity.Asset.sg_subtype': 'decorate', 'sg_resolution': 'md', 'entity.Asset.scenes': [], 
    # 'task_assignees': [{'type': 'Group', 'id': 40, 'name': 'modeler'}], 
    # 'sg_app': None, 'sg_status_list': 'wtg', 'type': 'Task', 'id': 115935, 'entity.Asset.code': 'bonsaiCourtyardDisplay'}, 
    # {'project': {'type': 'Project', 'id': 204, 'name': 'SevenChickMovie'}, 'sg_name': None, 'step': {'type': 'Step', 'id': 14, 'name': 'Model'}, 'sg_lockdown': False, 'entity.Asset.sg_asset_type': 'prop', 'entity': {'type': 'Asset', 'id': 6774, 'name': 'bonsaiCourtyardDisplay'}, 'content': 'model_pr', 'entity.Asset.sg_subtype': 'decorate', 'sg_resolution': 'pr', 'entity.Asset.scenes': [], 'task_assignees': [{'type': 'HumanUser', 'id': 196, 'name': 'Menga'}, {'type': 'Group', 'id': 40, 'name': 'modeler'}], 'sg_app': None, 'sg_status_list': 'apr', 'type': 'Task', 'id': 115937, 'entity.Asset.code': 'bonsaiCourtyardDisplay'}, {'project': {'type': 'Project', 'id': 204, 'name': 'SevenChickMovie'}, 'sg_name': None, 'step': {'type': 'Step', 'id': 14, 'name': 'Model'}, 'sg_lockdown': True, 'entity.Asset.sg_asset_type': 'prop', 'entity': {'type': 'Asset', 'id': 6774, 'name': 'bonsaiCourtyardDisplay'}, 'content': 'model', 'entity.Asset.sg_subtype': 'decorate', 'sg_resolution': 'md', 'entity.Asset.scenes': [], 'task_assignees': [{'type': 'Group', 'id': 40, 'name': 'modeler'}], 'sg_app': None, 'sg_status_list': 'rdy', 'type': 'Task', 'id': 115945, 'entity.Asset.code': 'bonsaiCourtyardDisplay'}, {'project': {'type': 'Project', 'id': 204, 'name': 'SevenChickMovie'}, 'sg_name': None, 'step': {'type': 'Step', 'id': 14, 'name': 'Model'}, 'sg_lockdown': False, 'entity.Asset.sg_asset_type': 'prop', 'entity': {'type': 'Asset', 'id': 6774, 'name': 'bonsaiCourtyardDisplay'}, 'content': 'modelBsh', 'entity.Asset.sg_subtype': 'decorate', 'sg_resolution': 'md', 'entity.Asset.scenes': [], 'task_assignees': [{'type': 'Group', 'id': 40, 'name': 'modeler'}], 'sg_app': None, 'sg_status_list': 'wtg', 'type': 'Task', 'id': 115951, 'entity.Asset.code': 'bonsaiCourtyardDisplay'}, {'project': {'type': 'Project', 'id': 204, 'name': 'SevenChickMovie'}, 'sg_name': None, 'step': {'type': 'Step', 'id': 14, 'name': 'Model'}, 'sg_lockdown': False, 'entity.Asset.sg_asset_type': 'prop', 'entity': {'type': 'Asset', 'id': 6774, 'name': 'bonsaiCourtyardDisplay'}, 'content': 'modelGr', 'entity.Asset.sg_subtype': 'decorate', 'sg_resolution': 'md', 'entity.Asset.scenes': [], 'task_assignees': [{'type': 'Group', 'id': 40, 'name': 'modeler'}], 'sg_app': None, 'sg_status_list': 'wtg', 'type': 'Task', 'id': 115960, 'entity.Asset.code': 'bonsaiCourtyardDisplay'}]
