# qc duplicated name
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_maya.lib import maya_lib
reload(maya_lib)
from rf_utils.widget import dialog 

def run(entity=None):
	""" Remove non reference namespaces """
	# check if any references
	nodes = get_non_ref()
	ns = get_namespace_node(nodes)
	return False if ns else True

def resolve(entity=None):
	""" Checking for Rig_Grp and Geo_Grp """
	action1 = 'Remove'
	action2 = 'Select nodes'
	action3 = 'Cancel'

	nodes = get_non_ref()
	ns = get_namespace_node(nodes)

	if ns: 
		result = dialog.CustomMessageBox.show(title='Remove namespace', message='Remove namespace for non-ref?', buttons=[action1, action2, action3])

		if result.value == action1: 
			remove_namespace(ns)

		if result.value == action2: 
			mc.select(ns)

	return run(entity)

def get_non_ref(): 
	return [a for a in mc.ls() if not mc.referenceQuery(a, inr=True)]


def get_namespace_node(nodes): 
	nsNodes = [a for a in nodes if ':' in a]
	return nsNodes

def remove_namespace(nodes): 
	for node in nodes: 
		lastElm = node.split(':')[-1]
		mc.rename(node, lastElm)
