import maya.cmds as mc

def run(entity=None):
    """ Check for faces with more than 4 sides (N-Sided polygon). """
    mdGeoGrp = '%s' % (entity.projectInfo.asset.md_geo_grp()) if entity else 'MdGeo_Grp'
    nGons = []
    if mc.objExists(mdGeoGrp):
        geos = []
        for tr in mc.listRelatives(mdGeoGrp, ad=True, pa=True, type='transform'):
            shapes = mc.listRelatives(tr, shapes=True, ni=True, pa=True)
            if shapes:
                geos.append(tr)
        
        preSel = mc.ls(sl=True)
        mc.select(geos, r=True)
        mc.polySelectConstraint(m=3, t=8, sz=3)
        nSidedSels = mc.ls(sl=True)
        if nSidedSels:
            trs = set()
            for fid in nSidedSels:
                splits = fid.split('.f[')
                if splits:
                    trs.add(splits[0])

            nGons.extend(nSidedSels)
            nGons.extend(list(trs))
        mc.polySelectConstraint(m=0, t=8, sz=0)
        mc.select(preSel, r=True)
    else: 
        return [False, 'MdGeoGrp: %s not found!' %mdGeoGrp]

    if not nGons:
        return [True, 'No face with more than 4 sides found.']
    else:
        msg = 'Found N-Sided polygon!\nTo select, Right-click, choose Resolve.\n======== N-Sided Geos ========'
        return [False, '%s\n%s' %(msg, '\n'.join(nGons))]

def resolve(entity=None):
    result, msg = run(entity)
    if not result:
        nGons = msg.split('\n')[3:]
        mc.select(nGons, r=True)
        # mc.hilite(nGons)
    return False


def skip(entity=None): 
    return True