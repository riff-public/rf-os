# qc duplicated name
import maya.cmds as mc
import maya.mel as mm
# from rf_utils import custom_exception

def run(entity=None):
	""" Check for Geo_Grp, MdGeo_Grp, PrGeo_Grp or Export_Grp for texture department """
	if entity.step in ['texture']:
		exportGrp = '%s' % (entity.projectInfo.asset.export_grp()) if entity else 'Export_Grp'
		if mc.objExists(exportGrp):
			checkRes = True
			msg = '%s found' %(exportGrp)
		else:
			checkRes = False
			msg = 'Cannot find %s' %(exportGrp)
	else:
		checkRes, msg = check_res_grp(entity)

	if not checkRes:
		return [False, msg]
	else:
		return [True, msg]


def resolve(entity=None):
	""" Checking for Rig_Grp and Geo_Grp """
	from lib import maya_lib
	reload(maya_lib)
	maya_lib.create_group()

	return run(entity)

def check_res_grp(entity=None):
	""" Checking for MdGeo_Grp and PrGeo_Grp """
	geoGrp = '%s' % (entity.projectInfo.asset.geo_grp()) if entity else 'Geo_Grp'
	if not mc.objExists(geoGrp):
		return False, '%s not found' %geoGrp

	mdGeoGrp = '%s' % (entity.projectInfo.asset.md_geo_grp()) if entity else 'MdGeo_Grp'
	prGeoGrp = '%s' % (entity.projectInfo.asset.pr_geo_grp()) if entity else 'PrGeo_Grp'
	pvGeoGrp = entity.projectInfo.asset.pv_geo_grp() if entity else 'PvGeo_Grp'

	if not pvGeoGrp:
		pvGeoGrp = 'PvGeo_Grp'

	checkMd = mc.ls(mdGeoGrp)[0] if mc.objExists(mdGeoGrp) else ''
	checkPr = mc.ls(prGeoGrp)[0] if mc.objExists(prGeoGrp) else ''
	checkPv = mc.ls(pvGeoGrp)[0] if mc.objExists(pvGeoGrp) else ''
	list_child = mc.listRelatives(geoGrp, c=True)
	if checkMd in list_child and checkPr in list_child and checkPv in list_child:
		res = entity.res
		result = False
		msg = 'Model groups found.'
		if res in ('pr', 'md', 'pv'):
			if res == 'md':
				resGrp = mdGeoGrp
			elif res == 'pr':
				resGrp = prGeoGrp
			elif res == 'pv':
				resGrp = pvGeoGrp

			grp_children = mc.listRelatives(resGrp, c=True)
			if grp_children:
				result = True
			else:
				msg = 'No children in %s' %resGrp
		else:
			result = True
			
		return result, msg
	else:
		return False, '%s or %s or %s not found under %s' %(mdGeoGrp, prGeoGrp, pvGeoGrp, geoGrp)
