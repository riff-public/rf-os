# qc duplicated name
import os
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_utils.context import context_info
from rf_utils.widget import dialog

def run(entity=None): 
    """ Check if prep rig is ready """ 
    if entity.type == 'animated': 
        return check_prep_multiple_rig(entity)
    else: 
        return check_prep_rig(entity)


def resolve(entity=None): 
    if not run(entity): 
        mc.confirmDialog(title='Confirm', message='Prep rig is missing! Inform Rig department.', button=['OK'])
    return True 

def skip(entity=None): 
    if entity.type == 'animated': 
        return True 
    else: 
        return False 

def check_prep_rig(entity): 
    """ check if asset inuse match asset workspace """ 
    actionName = entity.process
    entity.context.update(process=actionName)
    asset = entity.copy()
    asset.context.update(step='rig', process='prep', res='md', app='maya')
    heroPath = asset.path.hero()
    prepFile = asset.publish_name(hero=True)
    rigPrepFile = '%s/%s' % (heroPath, prepFile)
    rigPrepFileAbs = '%s/%s' % (asset.path.hero().abs_path(), prepFile)
    print rigPrepFileAbs

    return True if os.path.exists(rigPrepFileAbs) else False


def check_prep_multiple_rig(entity): 
    geoGrp = entity.projectInfo.asset.geo_grp() or 'Geo_Grp'
    listAssets = mc.ls('*:%s' % geoGrp)
    results = []
    
    for each in listAssets: 
        if mc.referenceQuery(each, inr=True): 
            refPath = mc.referenceQuery(each, f=True)
            asset = context_info.ContextPathInfo(path=refPath)
            result = check_prep_rig(asset.copy())
            print 'result ----', result
            results.append(result)

    return all(results)
