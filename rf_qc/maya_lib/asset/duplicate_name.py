# qc duplicated name
from collections import defaultdict

import pymel.core as pm
import maya.cmds as mc
import maya.mel as mm
from rf_maya.lib import maya_lib

def run(entity=None):
    """ Check for node with same name """
    if 'bsh' in entity.process:
        return [True, 'Ignored in blendShape process']

    checkGrp = get_checkGrp(entity)
    # print entity.step
    if not pm.objExists(checkGrp):
        return [False, 'Cannot find: %s' %checkGrp]

    found_duplicate, same_names = check(checkGrp)

    # create display string
    if found_duplicate:
        msg = ''
        for sn, objs in same_names.iteritems():
            if len(objs) > 1:
                msg += sn
                for obj in objs:
                    msg += '\n  - %s' %obj.shortName()
                msg += '\n'
        return [False, msg]
    else:
        return [True, 'No duplicated names']

def get_checkGrp(entity):
    checkGrp = None
    if entity.step == 'model':       
        if entity.res == 'pr':  
            checkGrp = entity.projectInfo.asset.pr_geo_grp()
        elif entity.res == 'lo':  
            checkGrp = entity.projectInfo.asset.lo_geo_grp()
        elif entity.res == 'md':  
            checkGrp = entity.projectInfo.asset.md_geo_grp()
        else: 
            checkGrp = entity.projectInfo.asset.geo_grp()

    elif entity.step == 'sim':
        checkGrp = '%s_CIN' %entity.name
    elif entity.step == 'groom':
        checkGrp = entity.projectInfo.asset.groom_grp()
        # print checkGrp
    else:
        checkGrp = entity.projectInfo.asset.export_grp()

    return checkGrp

def check(checkGrp):
    checkGrp = pm.PyNode(checkGrp)
    same_names = defaultdict(list)  # {nodeName:[pyObj1, pyObj2, ...]}
    found_duplicate = False
    for obj in checkGrp.getChildren(type='transform', ad=True):
        objName = obj.nodeName()
        same_names[objName].append(obj)
        if not found_duplicate and len(same_names[objName]) > 1:
            found_duplicate = True

    return found_duplicate, same_names

def resolve(entity=None):
    """ run resolution return check result """
    checkGrp = get_checkGrp(entity)
    if not pm.objExists(checkGrp):
        return False

    found_duplicate, same_names = check(checkGrp)

    for name, trs in same_names.iteritems():  # {nodeName:[pyObj1, pyObj2, ...]}
        if len(trs) > 1:
            for i, tr in enumerate(trs):
                ii = i + 1
                exit = False

                # naming 
                nsplits = name.split('_')
                while not exit:
                    # try not to break the naming convention
                    if len(nsplits) > 1:
                        new_name = '%s%s' %(nsplits[0], ii)
                        new_name += '_%s' %'_'.join(nsplits[1:])
                    else:
                        new_name = '%s%s' %(name, ii)

                    # if the new name already exists
                    if pm.objExists(new_name):
                        ii += 1
                    else:
                        exit = True

                tr.rename(new_name)

                shp = tr.getShape(ni=True)
                if shp:
                    new_name_shp = ('%sShape'%new_name)
                    shp.rename(new_name_shp)

    return True
