# check geo for incorrect naming
import pymel.core as pm

def run(entity=None):
    ''' Check geo for incorrect naming. '''
    checkGrp = entity.projectInfo.asset.groom_grp()
    yetiGrp = pm.PyNode(checkGrp)
    invalid_objs = []
    for i in [m for m in yetiGrp.getChildren(ad=True, type='transform')]:
        curr_name = i.nodeName().split(':')[-1]
        splits = curr_name.split('_')
        len_splits = len(splits)
        if len_splits in (2, 3) and splits[-1] in ('Geo', 'Grp', 'CRV', 'YetiNode', 'YetiGroom', 'reference', 'Ref', 'YetiBraid', 'YetiFeather'):
            if len_splits == 2:
                continue
            elif len_splits == 3 and splits[1] in ('L', 'R', 'Geo', 'Tex', '1', '2'):
                continue 
            
        # if the loop didn't continue, its incorrect
        invalid_objs.append(i.shortName())

    if invalid_objs:
        return [False, 'Incorrect geo names:\n%s' %('\n'.join(invalid_objs))]
    else:
        return [True, 'All geo names are correct.']

    return True

def resolve(entity=None):
    """ Rename shapes with incorrect names """
    status, check_res = run(entity)
    result = True
    if not status:
        invalid_objs = check_res.split('\n')[1:]
        pm.select(invalid_objs)
 

def skip(entity=None):
    return True
