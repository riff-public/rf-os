# check for yetiGroom connections
import pymel.core as pm

def run(entity=None):
    ''' Check for yetiGroom connections. '''
    result = check_connections()

    if result:
        return False
    else:
        return True

def resolve(entity=None):
    """ Select yetiGroom didn't have connection """
    result = check_connections()
    if result:
        pm.select(result)
        for obj in result:
            print '%s did not have connections' % obj
        return False
    else:
        return [True, 'All yetiGroom are correct.']
 

def skip(entity=None):
    return True

def check_connections():
    yetiGroomList = pm.ls(type='pgYetiGroom')
    illegal = []
    for yetiGroom in yetiGroomList:
        attr = '%s.inputGeometry' % yetiGroom
        if pm.objExists(attr):
            connect = pm.listConnections(attr, connections=True)
            if not connect:
                illegal.append(yetiGroom)

    return illegal
