# qc duplicated name
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_maya.lib import maya_lib
reload(maya_lib)

def run(entity=None):
	""" Check for smoothPly_set """
	# check if any references
	setName = entity.projectInfo.asset.smooth_set()
	# members = mc.sets(setName, q=True)
	return True if mc.objExists(setName) else False


def resolve(entity=None):
	""" Checking for Rig_Grp and Geo_Grp """
	from rf_app.model.smooth_data import app
	reload(app)
	app.show()
	return False


def skip(entity=None):
	return True