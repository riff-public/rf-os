# qc duplicated name
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_utils.context import context_info

def run(entity=None): 
	""" check if cycle start frame at 1001 """ 
	# project start frame 
	startFrame = entity.projectInfo.scene.start_frame or 1001 
	currentStartFrame = mc.playbackOptions(q=True, min=True)
	
	if not startFrame == currentStartFrame: 
		return [False, 'Startframe is not %s' % startFrame]
	return [True, '']

def resolve(entity=None): 
	startFrame = entity.projectInfo.scene.start_frame or 1001 
	message = 'Frame not start at %s. Please set time range to start at %s' % (startFrame, startFrame)
	mc.confirmDialog( title='Confirm', message=message, button=['OK'])

