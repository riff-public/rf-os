from collections import defaultdict

import pymel.core as pm
import maya.cmds as mc

from rf_maya.lib import material_layer
reload(material_layer)
from rf_maya.rftool.shade import shade_utils
reload(shade_utils)

def getFaceCount(geoGrp):
    face_counts = {}
    for shp in mc.listRelatives(geoGrp, ad=True, type='mesh', pa=True):
        numFace = mc.polyEvaluate(shp, face=True)
        tr = mc.listRelatives(shp, parent=True, pa=True)[0]
        face_counts[tr] = numFace - 1
    return face_counts

def run(entity_ctx=None):
    """ Check for geo with multiple face-assinged shader """
    if entity_ctx:
        entity = entity_ctx.copy()
    mat_layers = material_layer.list_layer_info(entity=entity)
    current_layer = pm.nt.RenderLayer.currentLayer()
    geoGrp = get_geo_grp(entity=entity)
    if not geoGrp:
        return [False, 'Cannot find geo group!']

    face_counts = getFaceCount(geoGrp=geoGrp)

    display_msgs = []
    for lyr_name, info in mat_layers.iteritems():
        typ = info['materialType']
        # skipping preview layers
        if typ == material_layer.Config.preview:
            continue

        # move to the layer
        lyr = pm.nt.RenderLayer(lyr_name)
        lyr.setCurrent()

        # get assignments
        assignments = shade_utils.list_shader(targetGrp=geoGrp, collapseFaceAssignment=False)
        invalid_items = set()

        for shadeName, data in assignments.iteritems():
            for asg in data['objs']:
                if '.f[' in asg:
                    splits = asg.split('.f[')
                    geoName = splits[0]
                    fIndxStr = splits[1][:-1]
                    if ':' in fIndxStr:
                        fIndexSplits = fIndxStr.split(':')
                        minFaceIndex = int(fIndexSplits[0])
                        maxFaceIndex = int(fIndexSplits[-1])
                        if minFaceIndex != 0 or maxFaceIndex != face_counts[geoName]:
                            invalid_items.add('\n  %s' %geoName)
                    else:
                        faceIndex = int(fIndxStr)
                        if faceIndex != face_counts[geoName]:
                            invalid_items.add('\n  %s' %geoName)

        if invalid_items:
            invalid_items = list(invalid_items)
            invalid_items.insert(0, lyr_name)
            display_msgs += invalid_items

    # go back to previously set layer
    current_layer.setCurrent()

    if display_msgs:
        display_msgs.insert(0, '** Geos with multiple shaders found.\n\n')
        msg = ''.join(display_msgs)
        # print '\n%s' %msg
        return [False, msg]
    else:
        return [True, 'Shader assignments are valid.']

def get_geo_grp(entity):
    res = entity.res
    name = ''
    if res == 'md':
        name = entity.projectInfo.asset.md_geo_grp()
    elif res == 'pr':
        name = entity.projectInfo.asset.pr_geo_grp()
    # else:
    if not name:
        name = entity.projectInfo.asset.geo_grp()

    grps = mc.ls(name, type='transform') + mc.ls('*:%s' %name, type='transform')
    if grps:
        return grps[0]

def skip(entity=None):
    return True

def resolve(entity=None):
    """ Check for face assignment of shaders """
    current_layer = pm.nt.RenderLayer.currentLayer()
    geoGrp = get_geo_grp(entity=entity)

    if not geoGrp:
        return [False, 'Cannot find geo group!']

    status, result = run(entity)
    if not status:
        resolved = True
        initSg = pm.PyNode('initialShadingGroup')
        for res in result.split('\n')[2:]:
            
            # it's an object
            if res.startswith('  '): 
                geoName = res[2:]
                geo = pm.PyNode(geoName)
                geoShp = geo.getShape(ni=True)
                sgOuts = list(set(geoShp.outputs(type='shadingEngine')))
                # print sgOuts
                if sgOuts:
                    if len(sgOuts) > 1 and initSg in sgOuts:
                        sgOuts.remove(initSg)
                    shaders = sgOuts[0].surfaceShader.inputs()
                    if shaders:
                        pm.select(geo, r=True)
                        try:
                            pm.hyperShade(assign=shaders[0])
                        except:
                            resolved = False
            else:
                # it's a layer
                lyr = pm.nt.RenderLayer(res)
                lyr.setCurrent()

        # go back to previously set layer
        current_layer.setCurrent()
    else:
        return True

    if resolved:
        msg = 'Resolve success!\nThe first shader has been assigned to all geos with multiple shaders.'
        msg += '\n\nPLEASE CHECK THE RESULTS.'
        pm.confirmDialog(title='Resolve multiple per-face assignment', message=msg)
        return True
    else:
        return False

