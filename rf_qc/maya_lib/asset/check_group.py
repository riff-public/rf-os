# qc duplicated name
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception

def run(entity=None):
	""" Check for Main Group (Rig_Grp/Geo_Grp/Export Grp) """
	topGrp = '%s' % (entity.projectInfo.asset.geo_grp()) if entity else 'Geo_Grp'

	if entity.step in ['texture', 'anim']:
		topGrp = '%s' % (entity.projectInfo.asset.export_grp()) if entity else 'Export_Grp'

	check = mc.ls(topGrp)

	if not check:
		return [False, '%s not found' %(topGrp)]
	else:
		return [True, 'Main group found.']

def skip(entity=None):
	return True

def resolve(entity=None):
	""" Checking for Rig_Grp and Geo_Grp """
	res, msg = run(entity)
	if not res:
		topGrp = msg.split(' ')[0]
		mc.group(em=True, n=topGrp)
		if entity.step in ['texture']:
			geoGrp = '%s' % (entity.projectInfo.asset.geo_grp()) if entity else 'Geo_Grp'
			geoGrps = mc.ls('*:%s' %geoGrp) + mc.ls(geoGrp)
			if geoGrps:
				mc.parent(geoGrps[0], topGrp)

	return True