import maya.cmds as mc
import maya.mel as mm

PLUGINS_TO_REMOVE = ['mtoa.mll']

def run(entity=None):
    """ Remove unknown nodes """
    unknownNodes = mc.ls(type='unknown')

    if unknownNodes:
        return [False, '%s unknown node(s) found.\n%s' %(len(unknownNodes), '\n'.join(unknownNodes))]
    else:
        return [True, 'No unknown node found.']

def skip(entity=None):
    return False

def resolve(entity=None):
    unknownNodes = mc.ls(type='unknown')
    success = True
    for node in unknownNodes:
        # plugin = mc.unknownNode(node, q=True, plugin=True)
        # if plugin in PLUGINS_TO_REMOVE:
        mc.lockNode(node, l=False)
        try:
            mc.delete(node)
        except:
            success = False
    mm.eval('flushUndo;')

    unknownPlugins = mc.unknownPlugin(q=True, list=True)
    if unknownPlugins:
        for plugin in unknownPlugins:
            mc.unknownPlugin(plugin, remove=True)

    return success
