# qc duplicated name
import sys
import os
import shutil
from collections import OrderedDict

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_utils.context import context_info


def run(entity=None):
    """ Check if textures are in the pipeline """
    from rftool.asset.check_copy_texture import app
    reload(app)
    myApp = app.show()
    return True


def resolve(entity=None):
    """ fix textures not in the pipeline """
    from rftool.asset.check_copy_texture import app
    reload(app)
    myApp = app.show()
    return True
    # texturePath = entity.path.texture().abs_path()
    # texturePathRes = texturePath
    # # texturePathRes = '%s/%s' % (texturePath, entity.res)
    # wrongPath = check_path(texturePath)

    # for node, path in wrongPath.iteritems():
    #     if os.path.exists(path):
    #         filename = os.path.basename(path)
    #         dstTexturePath = '%s/%s' % (texturePathRes, filename)

    #         if not os.path.exists(texturePathRes):
    #             os.makedirs(texturePathRes)

    #         shutil.copy2(path, dstTexturePath)
    #         mc.setAttr('%s.fileTextureName' % node, dstTexturePath, type='string')
    #         logger.debug('Copy texture %s -> %s' % (path, dstTexturePath))

    # return run(entity)


def skip(entity=None):
    if entity.step == context_info.Step.rig:
        return True
    else:
        logger.error('Cannot skip this QC')
        return False

def check_path(texturePath, texturePublPath):
    """ get textures files in scene """
    fileNodes = mc.ls(type='file')
    data = OrderedDict()

    for fileNode in fileNodes:
        path = mc.getAttr('%s.fileTextureName' % fileNode)

        if texturePath in path or texturePublPath in path:
            pass
        else:
            data[fileNode] = path
        # if not texturePath in path :
        #     data[fileNode] = path

    return data
