import maya.cmds as mc
import pymel.core as pm
import maya.mel as mm
# from rf_utils import custom_exception

def run(entity=None):
    """ Check geo naming in PrGeo_Grp """
    prGeoGrp = '%s' % (entity.projectInfo.asset.pr_geo_grp()) if entity else 'PrGeo_Grp'
    result = True
    if mc.objExists(prGeoGrp):
        invalid_names = get_invalid_name(prGeoGrp)
        if invalid_names:
            result = False
    else: 
        return [False, 'Proxy geo group not found']

    if result:
        return [True, 'Proxy geo names are correct']
    else:
        return [False, 'Invalid proxy geo names:\n%s' %('\n'.join(invalid_names))]

def get_invalid_name(prGeoGrp):
    invalid_names = []
    prGeoGrp = pm.PyNode(prGeoGrp)
    for tr in prGeoGrp.getChildren(ad=True, pa=True, ni=True, type='transform'):
        tr_name = tr.nodeName()
        if tr.getShape(ni=True):
            if not tr_name.endswith('_PrGeo'):
                invalid_names.append(tr.shortName())
        else:
            if not tr_name.endswith('_PrGrp'):
                invalid_names.append(tr.shortName())

    return invalid_names

def resolve(entity=None):
    result, msg = run(entity)
    if not result:
        invalid_names = msg.split('\n')[1:]
        for geo in invalid_names:
            node = pm.PyNode(geo)
            nodeName = node.nodeName()
            name_splits = nodeName.split('_')
            if node.getShape(ni=True):
                typ = 'PrGeo'
            else:
                typ = 'PrGrp'
            if len(name_splits) > 1:
                new_name = '_'.join(name_splits[:-1]) + '_' + typ
            else:
                new_name = '%s_%s' %(nodeName, typ)
            node.rename(new_name)

    return True