# qc duplicated name
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_utils.pipeline import asset_tag
from rf_utils.context import context_info

def run(entity=None):
    """ Check for Geo_Grp """
    # check Rig_Grp
    # topGrp = '|%s' % entity.projectInfo.asset.top_grp() if entity else '|Rig_Grp'
    geoGrp = '%s' % (entity.projectInfo.asset.geo_grp()) if entity else 'Geo_Grp'
    if entity.step == 'anim': 
        if entity.type == 'animated': 
            if entity.process == 'main':
                # ref mode 
                refs = mc.file(q=True, r=True)

                if refs: 
                    for ref in refs: 
                        if mc.referenceQuery(ref, isLoaded=True): 
                            namespace = mc.referenceQuery(ref, ns=True)[1:]
                            targetGeo = '%s:%s' % (namespace, geoGrp)

                            if mc.objExists(targetGeo): 
                                asset = context_info.ContextPathInfo(path=ref)  
                                asset.context.update(step=entity.step, process=entity.process)
                                project = asset.project
                                assetType = asset.type 
                                assetName = asset.name
                                assetId = asset.id
                                step = asset.step
                                process = asset.process
                                publishVersion = asset.publishVersion
                                publishPath = asset.path.publish_version()
                                adPath = '%s/%s' % (asset.path.asm_data().abs_path(), asset.output_name(outputKey='ad', hero=True))  
                                refPath = ref.split('{')[0]
                                asset_tag.add_tag(targetGeo, project=project, assetType=assetType, assetName=assetName, assetId=assetId, step=step, process=process, publishVersion=publishVersion, publishPath=publishPath, refPath=refPath, adPath=adPath)

                    return True

                if not refs: 
                    if mc.objExists(geoGrp): 
                        project = entity.project
                        assetType = entity.type 
                        assetName = entity.name
                        assetId = entity.id
                        step = entity.step
                        process = entity.process
                        publishVersion = entity.publishVersion
                        publishPath = entity.path.publish_version()
                        adPath = '%s/%s' % (entity.path.asm_data().abs_path(), entity.output_name(outputKey='ad', hero=True))  
                        asset_tag.add_tag(geoGrp, project=project, assetType=assetType, assetName=assetName, assetId=assetId, step=step, process=process, publishVersion=publishVersion, publishPath=publishPath, adPath=adPath)

                    return True

    return True


def resolve(entity=None):
    """ Checking for Rig_Grp and Geo_Grp """
    return run(entity)

def skip(entity=None): 
    return True
