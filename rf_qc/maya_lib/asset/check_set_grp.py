# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
reload(publish_info)
from rf_utils import custom_exception
from rf_app.asm import asm_lib
from rf_utils import file_utils
from rf_utils.sg import sg_utils
sg = sg_utils.sg
import maya.cmds as mc 

def run(entity=None): 
    """ check if non-gpu asset in Set_Grp """ 
    wrongAssets = check_ref(entity)

    return True if not wrongAssets else False

def resolve(entity=None): 
    """ check if non-gpu asset in Set_Grp """ 
    wrongAssets = check_ref(entity)
    if wrongAssets: 
        mc.select(wrongAssets)
        return False
    else: 
        return True


def check_ref(entity): 
    wrongAssets = []
    checkGrps = ['Switch_Grp']
    targetGrp = entity.projectInfo.asset.get('set') or 'Set_Grp'
    rigGrp = entity.projectInfo.asset.get('topGrp') or 'Rig_Grp'
    refs = mc.file(q=True, r=True)

    for ref in refs: 
        namespace = mc.referenceQuery(ref, namespace=True)[1:]
        topGrp = '%s:%s' % (namespace, rigGrp)
        checkGrps.append(topGrp) 

    if checkGrps: 
        for topGrp in checkGrps: 
            grps = mc.ls(topGrp, l=True)

            for grp in grps: 
                if targetGrp in grp: 
                    wrongAssets.append(topGrp)

    return wrongAssets
