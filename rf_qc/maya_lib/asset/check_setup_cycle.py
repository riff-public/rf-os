#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.context import context_info
from rf_utils.sg import sg_process
from rf_app.publish.scene.utils import maya_hook as hook
import logging 
import re
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


def run(entity=None): 
    """ Check if camera is correct """
    entity = context_info.ContextPathInfo() if not entity else entity
    entity.context.use_sg(sg_process.sg, project=entity.project, entityType=entity.entity_type, entityName=entity.name)

    return False

def resolve(entity):
    statusRenderLayer = check_renderLayer(entity)
    statusSequencer = check_sequencer(entity)
    check_grp()
    
    if not statusRenderLayer:
        create_renderLayer(entity)

    if not statusSequencer:
        create_sequencer(entity)

    return True 

def check_renderLayer(entity):
    statusLayer = True
    tasks = sg_process.list_looks(entity.context.sgEntity)
    looks = [a.get('sg_name') for a in tasks]
    renderLayer = mc.ls(type='renderLayer')
    for look in looks:
        name = '%s_lookdev'%look
        if not name in renderLayer:
            statusLayer = False

    return statusLayer

def check_sequencer(entity):
    statusSequencer = True
    shots = hook.list_shot()

    if not shots:
        statusSequencer = False

    if not entity.name in shots:
        statusSequencer = False

    return statusSequencer

def check_grp():
    grp = 'Export_Grp'
    if not mc.ls(grp, l=True):
        grp = mc.group( em=True, name=grp )
        targets = mc.ls('*:%s' % 'Rig_Grp')

        if targets:
            mc.parent(targets, grp)

def create_renderLayer(entity):
    from rf_maya.rftool.asset.setup import setup_app
    setup = setup_app.AssetSetup()
    setup.setup_layer(step='lookdev')
    setup_app.assign_shader(entity)

def create_sequencer(entity):
    cycleName = entity.process
    data = sg_process.read_cycle_data(entity.project, entity.name, cycleName)
    previousDuration = ''
    endTimeSlider = mc.playbackOptions(q=True, max=True)
    if data:
        loopRange = data.get('loopRange')
        exportRange = data.get('exportRange')
        previousDuration = loopRange[1] - loopRange[0] + 1

    startTime = 1001
    endTime = previousDuration if previousDuration else endTimeSlider
    sequenceStartTime = 1001
    sequenceEndTime = previousDuration if previousDuration else endTimeSlider
    mc.shot(entity.name, startTime=startTime, endTime=endTime, sequenceStartTime=sequenceStartTime, sequenceEndTime=sequenceEndTime)