import pymel.core as pm
from maya.api import OpenMaya as om

def run(entity=None):
    """ Check geos for flipped UVs (displayed red in UV editor) """
    geoGrp = getGeoGrp(entity)

    flipGeos = getFlipUVGeo(geoGrp)
    if flipGeos:
        msg = 'These geos has flipped UVs\n'
        msg += '\n'.join(flipGeos)
        return [False, msg]
    else:
        return [True, 'No flipped UV on any geo.']

def skip(entity=None):
    return True

def resolve(entity=None):
    geoGrp = getGeoGrp(entity)
    flipGeos = getFlipUVGeo(geoGrp)
    if flipGeos:
        pm.select(flipGeos)

    return None

def getGeoGrp(entity):
    checkGrp = entity.projectInfo.asset.md_geo_grp()
    geoGrp = pm.PyNode(checkGrp)
    return geoGrp

def getFlipUVGeo(geoGrp):
    # create MSelectionList
    mSel = om.MSelectionList()
    for g in geoGrp.getChildren(ad=True, type='mesh'):
        if not g.isIntermediate():
            mSel.add(g.longName())

    flipUVGeos = []
    for i in xrange(mSel.length()):
        mDag = mSel.getDagPath(i)
        meshFn = om.MFnMesh(mDag)
        for f in xrange(meshFn.numPolygons):
            if meshFn.isPolygonUVReversed(f):
                trMObj = mDag.transform()
                trDag = om.MDagPath.getAPathTo(trMObj)
                flipUVGeos.append(trDag.partialPathName())
                break
    return flipUVGeos