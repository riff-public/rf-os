import math

import pymel.core as pm
import maya.OpenMaya as om

def run(entity=None):
    """ Check objects with NaN or INF boundingBox """
    if entity.res != 'md':
        return [True, 'Check skipped. Resolution is not md']

    invalid_names = []
    dagIt = om.MItDag(om.MItDag.kDepthFirst, om.MFn.kInvalid)
    
    while not dagIt.isDone():
        dagObject = dagIt.currentItem()
        mDagPath = om.MDagPath()
        dagIt.getPath(mDagPath)
        dagNodeFn = om.MFnDagNode(mDagPath)
        incMatrix = mDagPath.inclusiveMatrix()
        bb = dagNodeFn.boundingBox()
        bb.transformUsing(incMatrix)
        bb_min = tuple(bb.min())
        bb_max = tuple(bb.max())
        for value in bb_min + bb_max:
            if math.isnan(value) or math.isinf(value):
                path = dagNodeFn.fullPathName()
                to_remove = []
                for i, inv in enumerate(invalid_names):
                    if path.startswith(inv):
                        to_remove.append(i)

                for r in to_remove:
                    invalid_names.pop(r)
                invalid_names.append(path)
                break

        dagIt.next()

    # convert to shortName
    invalid_objs = []
    for inv in invalid_names:
        node = pm.PyNode(inv)
        if isinstance(node, pm.nt.Shape):  # if its a shape, get the transform parent
            node = node.getParent()
        sn = node.shortName()
        invalid_objs.append(sn)

    if invalid_objs:
        return [False, 'Objects with NaN or INF boundingBox found:\n%s' %('\n'.join(invalid_objs))]
    else:
        return [True, 'All geos boundingBox is good.']

def skip(entity=None):
    return False

def resolve(entity=None):
    status, check_result = run(entity)
    result = True
    if not status:
        objs = check_result.split('\n')[1:]
        pm.select(objs)

    return None