# qc SetAttr On-Off Visibility
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from utaTools.utapy import utaCore
reload(utaCore)

def run(entity=None):
    """ Check Group for setAttr On-Off Visibility """
    checkListGrp = ['Ctrl_Grp', 'Skin_Grp', 'Jnt_Grp', 'Still_Grp', 'Ikh_Grp', 'TechGeo_Grp']

    nameProcess = entity.process
    checkGrp = []
    if nameProcess == 'main':
        for each in checkListGrp:
            a = mc.ls('*:{}'.format(each),'{}'.format(each))
            for x in a:
                if ':' in x:
                    checkGrp.append(x)
                else:
                    checkGrp.append(a[0])                    
            for each in checkGrp:
                if mc.objExists(each):

                    if not 'FclRigSkin_Grp' == each:
                        if 'Ctrl_Grp' in each:
                            mc.setAttr('{}.v'.format(each), 1)
                        else:
                            mc.setAttr('{}.v'.format(each), 0)
    return True

def resolve(entity=None):
    """ Run """
    return run()