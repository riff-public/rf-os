# qc geo with same bounding box
from collections import defaultdict

import pymel.core as pm
from maya.api import OpenMaya as om

def run(entity=None):
    """ Check same geos sitting exactly at the same position """
    checkGrp = entity.projectInfo.asset.geo_grp()
    geoGrp = pm.PyNode(checkGrp)

    # create MSelectionList
    mSel = om.MSelectionList()
    for g in geoGrp.getChildren(ad=True, type='mesh'):
        if not g.isIntermediate():
            mSel.add(g.longName())

    bbs = defaultdict(list)
    for i in xrange(mSel.length()):
        mDag = mSel.getDagPath(i)
        incMatrix = mDag.inclusiveMatrix()
        dagNode = om.MFnDagNode(mDag)
        bb = dagNode.boundingBox
        bb.transformUsing(incMatrix)
        bb_vals = (tuple(bb.min), tuple(bb.max))
        bbs[bb_vals].append(mDag.partialPathName())

    same_bb = []
    msgs = ['Some geos are sitting exactly at the same position:']
    for bb, names in bbs.iteritems():
        if len(names) > 1:
            same_bb.append(names)
            msgs.append(str(names))
    if same_bb:
        return [False, '\n'.join(msgs)]
    else:
        return [True, 'All geos position is good.']

def skip(entity=None):
    return True

def resolve(entity=None):
    status, check_result = run(entity)
    result = True
    if not status:
        objs = check_result.split('\n')[1:]
        flat_list = []
        for strPair in objs:
            exec('meshes = %s' %strPair)
            for mesh in meshes:
                flat_list.append(mesh)
        pm.select(flat_list)

    return None

