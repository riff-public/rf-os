# qc duplicated name
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception

def run(entity=None):
    """ Check for Export Group """

    expGrp = 'Export_Grp'
    delGrp = 'Delete_Grp'
    check = mc.objExists( expGrp )

    if check :
        #check export group in other group
        prnt = mc.listRelatives(expGrp,ap=True)
        if prnt:
            return [ False , 'Found export group in other group.']
        return [ True , 'Export group found.' ]
    else :
        return [ False , 'Export group not found.' ]

def resolve(entity=None):
    """ Check for Export Group """

    res , msg = run(entity)

    expGrp = 'Export_Grp'
    check = mc.objExists(expGrp)

    #rename export group in other group
    if check:
        prnt = mc.listRelatives(expGrp,ad=True)
        if prnt:
            mc.rename(expGrp,'unk_Export_Grp')

    if not res:
        mc.createNode( 'transform' , n = 'Export_Grp' )

    return True