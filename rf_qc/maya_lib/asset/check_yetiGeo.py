# qc duplicated name
import sys
import os
import shutil
from collections import OrderedDict
from pprint import pprint

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import pymel.core as pm
import maya.mel as mm
import maya.OpenMaya as om

from rf_utils import custom_exception
from rf_utils.context import context_info
from rf_utils.pipeline import check 
from rf_utils import cask
from rf_maya.rftool.utils import maya_utils

def run(entity=None):
    """ Check hierarchy and boundingBox of geos in TechGeo_Grp against Groom's TechGeo_Grp """
    global checkResult
    checkResult = {'invalid':[]}
    if entity.process != 'main':
        return [True, 'Ignored in all other process except "main"']

    result = False 
    checkGrp, dataPath = get_data(entity)
    
    if not entity.res: 
        message = 'Check if you select "resolution", pr, md, hi'
        result = False
    else:
        if entity.res != 'md':
            return [True, 'Check skipped. Resolution is not md']
            
        if os.path.exists(dataPath):
            if not mc.objExists(checkGrp):
                ls = mc.ls(maya_utils.addDagPathNamespace(checkGrp, '*:'))
                if not ls:
                    message = 'Cannot find %s' %checkGrp
                    result = False
                else:
                    checkGrp = ls[0]
                
            if checkGrp:
                # make sure checkGrp is under techGeo_Grp
                techGrp = entity.projectInfo.asset.tech_grp()
                yetiGrpPars = mc.listRelatives(checkGrp.split('|')[0], parent=True)
                if yetiGrpPars and yetiGrpPars[0].endswith(techGrp):

                    checker = check.CheckHierarchy(checkGrp, str(dataPath))
                    topoResult = checker.check()

                    # details 
                    if topoResult[check.Config.mismatch] or topoResult[check.Config.missing] or topoResult[check.Config.alien]: 
                        message = 'Check topology against groom TechGeo failed, see script editor for details.'
                        result = False 
                        pprint(topoResult)
                        checkResult.update(topoResult)
                    else:  # check success
                        # check bounding boxes
                        diff_tol = 1e-04  # difference allowed 0.0001
                        mismatchBBs = []
                        groomBBs = getAbcBB(str(dataPath))  # {name:BB, ...}
                        # print groomBBs
                        for name, grbb in groomBBs.iteritems():

                            box_min = grbb.min()
                            box_max = grbb.max()

                            # get current BB
                            curr_mesh = pm.PyNode(name)
                            curr_mesh = curr_mesh.getShape(ni=True)  # need to get shape or it won't work with hidden geo
                            parent = curr_mesh.getParent()
                            curr_meshpDag = curr_mesh.__apimdagpath__()
                            dagFn = om.MFnDagNode(curr_meshpDag)
                            md_bb = dagFn.boundingBox()   
                            if parent:
                                parentIncMat = parent.__apimdagpath__()
                                incMatrix = parentIncMat.inclusiveMatrix()
                                md_bb.transformUsing(incMatrix)

                            bb_min = md_bb.min()
                            bb_max = md_bb.max()
                            curr_width = md_bb.width()
                            curr_height = md_bb.height()
                            curr_depth = md_bb.depth()
                            
                            minx_delta = abs(box_min.x - bb_min.x)
                            miny_delta = abs(box_min.y - bb_min.y)
                            minz_delta = abs(box_min.z - bb_min.z)

                            maxx_delta = abs(box_max.x - bb_max.x)
                            maxy_delta = abs(box_max.y - bb_max.y)
                            maxz_delta = abs(box_max.z - bb_max.z)

                            if minx_delta > diff_tol or miny_delta > diff_tol or minz_delta > diff_tol\
                                or maxx_delta > diff_tol or maxy_delta > diff_tol or maxz_delta > diff_tol:
                                # print name, minx_delta, miny_delta, minz_delta, maxx_delta, maxy_delta, maxz_delta
                                mismatchBBs.append(name)

                        if mismatchBBs:
                            message = 'Too much difference between TechGeo and groom TechGeo:\n%s' %('\n'.join(mismatchBBs))
                            checkResult['invalid'] = mismatchBBs
                            result = False
                        else:
                            message = 'Yeti_Grp matched.'
                            result = True
                else:  
                    message = 'Invalid parent: %s, %s must be the only child of %s!' %(yetiGrpPars[0], checkGrp, techGrp)
                    checkResult['invalid'] = [checkGrp]
                    result = False
        else:
            message = 'Groom techGeo ABC does not exits: %s' %dataPath
            result = True

    return [result, message]

def resolve(entity=None):
    """ Select non matching geos """
    result, message = run(entity)
    toSel = []
    global checkResult
    for k, v in checkResult.iteritems():
        toSel.extend(v)
    if toSel:
        mc.select(toSel, r=True)
        return False
    else:
        return True

def skip(entity=None):
    if entity.step == context_info.Step.rig:
        return True
    else:
        logger.error('Cannot skip this QC')
        return False

def get_data(entity): 
    localEntity = entity.copy()
    groomGrp = localEntity.projectInfo.asset.groom_grp()
    groomGeoGrp = localEntity.projectInfo.asset.groom_geo_grp()


    checkGrp = '%s|%s' %(groomGrp, groomGeoGrp)

    localEntity.context.update(step='groom', look='main', process='main')
    publishPath = localEntity.path.output_hero().abs_path()
    dataFile = localEntity.output(outputKey='groomGeo')
    dataPath = '%s/%s' % (publishPath, dataFile)

    return checkGrp, dataPath


def getAbcBB(abcPath):
    arc = cask.Archive(abcPath)

    gbnds = {}
    exit = False
    objs = [arc.top]

    while not exit:
        next_objs = []
        for o in objs:
            for c in o.children.values():
                if c.type() in ('PolyMesh', 'Curve'):
                    transform = c.parent
                    bnds_prop = c.properties['.geom/.selfBnds']
                    bnds = bnds_prop.get_value()
                    wbnds = bnds * transform.global_matrix()
                    # gbnds.extendBy(wbnds)
                    gbnds[transform.name] = wbnds
                else:
                    next_objs.append(c)

        if next_objs:
            objs = next_objs
        else:
            exit = True 

    return gbnds