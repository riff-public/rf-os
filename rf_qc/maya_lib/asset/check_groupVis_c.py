# qc SetAttr On-Off Visibility
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from utaTools.utapy import utaCore
reload(utaCore)
# from rigScript import core 
# reload(core)

def run(entity=None):
    """ Check Group for setAttr On-Off Visibility """
    checkListGrp = ['Ctrl_Grp', 'Skin_Grp', 'Jnt_Grp', 'Still_Grp', 'Ikh_Grp', 'TechGeo_Grp', 'X_world', 'C_world']

    nameProcess = entity.process
    checkGrp = []
    if nameProcess == 'main':
        for each in checkListGrp:
            a = mc.ls('*:{}'.format(each),'*{}'.format(each))
            for x in a:
                if ':' in x:
                    checkGrp.append(x)
                else:
                    checkGrp.append(a[0])                    

            for each in checkGrp:
                if mc.objExists(each):

                    if not 'FclRigSkin_Grp' == each:

                        if 'Ctrl_Grp' in each:
                            mc.setAttr('{}.v'.format(each), 1)

                        ## Crusher Project
                        elif 'C_world' in each:
                            mc.setAttr('{}.jointVis'.format(each), 0)

                        else:
                            mc.setAttr('{}.v'.format(each), 0)




    ## Set Attribute v = 1 of some Character
    toeIkhLists = ['toe_RikHandle', 'toe_LikHandle']
    for each in toeIkhLists:
        if mc.objExists(each):
            # mc.setAttr(each + ".ikBlend", 0)
            # mc.setAttr(each + ".ikBlend", 1)
            mc.setAttr(each + ".v", 1)

    return True


def resolve(entity=None):
    """ Run """
    return run()