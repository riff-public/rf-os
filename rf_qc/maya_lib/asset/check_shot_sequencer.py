# qc shot sequencer name
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_utils.context import context_info

def run(entity=None): 
    """ Check sequence shot """
    # only 1 shot node and same name as asset name
    targets = mc.ls(type = 'shot')
    if len(targets) ==1:
        target = targets[0]
        if entity.name == target:
            return [True, '']
        else:
            return [False, 'Wrong camera sequencer shot name "{}". Please replace to "{}"'.format(target, entity.name)]
    else:
        return [False, 'More than 1 camera sequencer shot found!. Please keep only 1 shot']


def resolve(entity=None):
    """ Rename shot to asset name """
    # keep only 1 shot that connect to sequencer
    targets = mc.ls(type = 'shot')
    if len(targets) >= 1:
        for target in targets:
            sequencerConnect = mc.listConnections(target,d=True,t='sequencer')
            if sequencerConnect == None:
                mc.delete(target)
            else:
                if not entity.name == target:
                    mc.rename(target,'{}'.format(entity.name))
                    return True
    else:
        return [False, 'This asset does not have camera sequencer shot. Please keep only 1 shot']

