# template for export mod
import os
import sys
import shutil
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.export.utils import export_utils
from rf_utils import publish_info
from rf_utils.context import context_info
reload(publish_info)
from rf_utils import custom_exception
from rf_app.asm import asm_lib
from rf_utils import file_utils
from rf_utils.sg import sg_utils
sg = sg_utils.sg

def run(entity=None): 
	""" check asset id of setdress """
	assetDict = asm_lib.collect_asset_description(entity)
	result, messages = check_id(assetDict)
	if not result: 
		raise custom_exception.PipelineError(messages)
	return result 

def resolve(entity=None): 
	""" collect asset id """
	assetDict = asm_lib.collect_asset_description(entity)
	result, messages = check_id(assetDict)
	return result

def check_id(assetDict): 
	from rf_utils.sg import sg_utils
	sg = sg_utils.sg
	fields = ['code', 'id']
	statuses = []
	errorMessages = []
	messages = []
	for assetName, data in assetDict.iteritems(): 
		print assetName
		try: 
			filters = [['project.Project.name', 'is', data['project']], ['id', 'is', data['id']]]
			result = sg.find_one('Asset', filters, fields)
			print assetName, result
			status = True if result else False
		except Exception as e: 
			status = False 
			logger.error(e)
			errorMessages.append(e)
			mesg = 'Failed %s - %s' % (assetName, data['id'])
			logger.info(mesg)
			messages.append(mesg)
		
		statuses.append(status)
		logger.info(errorMessages)

	return all(statuses), messages

def skip(entity=None): 
	return True
