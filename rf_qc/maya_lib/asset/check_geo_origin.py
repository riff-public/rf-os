import pymel.core as pm
from rf_utils.context import context_info

def run(entity=None):
    """ Check if all geo and groups pivot are at the origin"""
    if entity.res != 'md':
        return [True, 'Check skipped. Resolution is not md']

    geoGrp = str(entity.projectInfo.asset.geo_grp()) if entity else 'Geo_Grp'
    if not pm.objExists(geoGrp):
        raise Exception, 'Cannot find: %s' %(geoGrp)

    geoGrp = pm.PyNode(geoGrp)
    origin = pm.dt.Vector([0.0, 0.0, 0.0])
    moved_objs = []
    for tr in [geoGrp] + geoGrp.getChildren(ad=True, type='transform'):
        roPiv, scPiv = tr.getPivots()
        if roPiv != origin or scPiv != origin:
            moved_objs.append(tr)

    if moved_objs:
        return [False, 'Objects with moved pivots:\n%s' %('\n'.join([o.shortName() for o in moved_objs]))]
    else:
        return [True, 'All objects pivot are good']

def skip(entity=None):
    return True

def resolve(entity=None):
    geoGrp = str(entity.projectInfo.asset.geo_grp()) if entity else 'Geo_Grp'
    if not pm.objExists(geoGrp):
        raise Exception, 'Cannot find: %s' %(geoGrp)

    geoGrp = pm.PyNode(geoGrp)
    for tr in [geoGrp] + geoGrp.getChildren(ad=True, type='transform'):
        pm.xform(tr, ws=True, preserve=True, piv=[0.0, 0.0, 0.0])
    pm.makeIdentity(geoGrp, apply=True, t=True, r=True, s=True, n=False, pn=True)
    status, result = run(entity)

    return True