# check for incorrect shape name
import maya.mel as mm

def run(entity=None):
    ''' Fix render layer adjustment errors that cause texture publish to fail. '''
    try:
    	mm.eval("fixRenderLayerOutAdjustmentErrors;")
    except:
    	pass
    return True

def resolve(entity=None):
    return True


def skip(entity=None):
    return True
