import pymel.core as pm
from rf_utils import custom_exception


def getInvalidUvObjects(entity):
    checkGrp = entity.projectInfo.asset.geo_grp()
    geoGrp = pm.PyNode(checkGrp)
    invalid_names, invalid_objs = [], []
    for i in [m for m in geoGrp.getChildren(ad=True, type='transform')]:
        shp = i.getShape(type='mesh', ni=True)
        if not shp:
            continue
        uvSetNames = shp.getUVSetNames()
        currentUV = shp.getCurrentUVSetName()

        # if no UV set, first UV set is not map1, current UV is not map1 or number of UV set is more than 1
        if not uvSetNames or uvSetNames[0] != 'map1' or currentUV != 'map1' or len(uvSetNames)>1:
            invalid_objs.append(i)
            invalid_names.append(i.shortName())

    pm.select(invalid_objs, r=True)
    return invalid_names, invalid_objs

def run(entity=None):
    """ Look for meshs that does NOT have map1 as its default UV """
    invalid_names, invalid_objs = getInvalidUvObjects(entity)
    if invalid_names:
        msg = 'Mesh(s) UVSet is not map1: \n%s' %invalid_names
        return [False, msg]
    else:
        return [True, 'All objects default UV is map1']

def resolve(entity=None):
    """ Fix meshs that does NOT have map1 as its default UV """
    invalid_names, invalid_objs = getInvalidUvObjects(entity)
    
    result = True
    if invalid_objs:
        failedObjs = []
        for name, obj in zip(invalid_names, invalid_objs):
            shp = obj.getShape(type='mesh', ni=True)
            uvSetNames = shp.getUVSetNames()
            currentUV = shp.getCurrentUVSetName()
            # it has no UV set
            if not uvSetNames:
                try:
                    shp.createUVSet('map1')
                except Exception, e:
                    print e
                    result = False
                    print 'Cannot create UVset map1 on %s' %(name)

            elif currentUV != 'map1':  # need a rename on the first UV set
                # in case we already have 'map1', just copy the UV over to map1 and set map1 to current UV set
                if 'map1' in uvSetNames:
                    try:
                        pm.polyCopyUV(shp, uvSetNameInput=currentUV, uvSetName="map1", ch=False)
                        shp.setCurrentUVSetName('map1')
                        # shp.deleteUVSet(currentUV)
                    except Exception, e:
                        print e
                        result = False
                        print 'Cannot rename UVset %s to map1 on %s' %(currentUV, name)
                # case we don't have map1, rename current UVset to map1
                else:
                    pm.polyUVSet(shp, rename=True, newUVSet="map1")
                
            # elif currentUV == 'map1' and len(uvSetNames) > 1:
            #     for uvSet in uvSetNames:
            #         if uvSet == 'map1':
            #             continue
            #         shp.deleteUVSet(uvSet)

                
    return result


def skip(entity=None):
    return True
