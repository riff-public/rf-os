# qc scene poly count
import pymel.core as pm

def run(entity=None):
    """ Check for transform under Geo_Grp with more than 1 shape """
    if entity.step == 'groom':
        checkGrp = str(entity.projectInfo.asset.groom_geo_grp()) if entity else 'YetiGeo_Grp'
    else:
        checkGrp = str(entity.projectInfo.asset.geo_grp()) if entity else 'Geo_Grp'
    if not pm.objExists(checkGrp):
        raise Exception, 'Cannot find: %s' %(checkGrp)

    geoGrp = pm.PyNode(checkGrp)
    multiple_shapes_trs = [t.shortName() for t in geoGrp.getChildren(ad=True, type='transform') if len(t.getShapes()) > 1]
    if multiple_shapes_trs:
        return [False, 'These transform has multiple shapes:\n%s' %('\n'.join(multiple_shapes_trs))]
    else:
        return [True, 'All geos contain only 1 shape.']

def skip(entity=None):
    return True


def resolve(entity=None):
    status, check_result = run(entity)
    result = True
    if not status:
        # try to delete mulitple shapes
        objs = check_result.split('\n')[1:]
        for name in objs:
            node = pm.PyNode(name)
            shapes = node.getShapes()
            for shp in shapes:
                if shp.isIntermediate():
                    try:
                        pm.delete(shp)
                    except Exception, e:
                        print e
                        result = False

        # run another check after fix
        status, check_result = run(entity)
        if not status:
            objs = check_result.split('\n')[1:]
            pm.select(objs, r=True)
            result = False

    return result

