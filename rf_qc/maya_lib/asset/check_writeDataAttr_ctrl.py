# qc SetAttr On-Off Visibility
# import maya.cmds as mc
# import maya.mel as mm
# from rf_utils import custom_exception
# from utaTools.utapy import utaCore
# reload(utaCore)
# from rigScript import core 
# reload(core)

from utaTools.utapy import utaAttrWriteRead
reload(utaAttrWriteRead)
#write attribute from process name
utaAttrWriteRead.attrWrite()


def run(entity=None):
    """ Check write attribute from process name """
    utaAttrWriteRead.attrWrite()

    return True

def resolve(entity=None):
    """ Run """
    return run()