# qc duplicated name
from collections import defaultdict

import pymel.core as pm
import maya.cmds as mc
import maya.mel as mm
from rf_maya.lib import maya_lib

def run(entity=None):
    """ Check for unused intermediate shapes """
    checkGrp = get_checkGrp(entity)
    if not pm.objExists(checkGrp):
        return [False, 'Cannot find: %s' %checkGrp]

    int_shps, int_names = check(checkGrp)
    if int_shps:
        return [False, 'Unused intermediate shapes:\n%s' %('\n'.join(int_names))]
    else:
        return [True, 'No unused intermediate shape found.']

def check(checkGrp):
    checkGrp = pm.PyNode(checkGrp)
    int_shps, int_names = [], []
    for shp in checkGrp.getChildren(ad=True, type='mesh'):
        if shp.isIntermediate() == True:
            connections = shp.worldMesh.outputs() + shp.outMesh.outputs()
            if not connections:
                int_names.append(shp.shortName())
                int_shps.append(shp)

    return int_shps, int_names

def get_checkGrp(entity):
    checkGrp = None
    if entity.step == 'model':        
        checkGrp = entity.projectInfo.asset.geo_grp()
    elif entity.step == 'sim':
        checkGrp = '%s_CIN' %entity.projectInfo.asset.geo_grp()
    else:
        checkGrp = entity.projectInfo.asset.export_grp()

    return checkGrp

def resolve(entity=None):
    """ run resolution return check result """
    checkGrp = get_checkGrp(entity)
    if not pm.objExists(checkGrp):
        return [False, 'Cannot find: %s' %checkGrp]

    int_shps, int_names = check(checkGrp)
    try:
        pm.delete(int_shps)
        return True
    except:
        return False