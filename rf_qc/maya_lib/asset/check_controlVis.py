# qc SetAttr On-Off Visibility
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
# from utaTools.utapy import utaCore
# reload(utaCore)
from rigScript import core 
reload(core)

def run(entity=None):
    """ Check setAttr On-Off Contrl Visibility """
    nameType = entity.type
    nameCharCrv = '{}_Crv'.format(entity.name)

    a = core.Node()
    ns = a.nameSpaceLists()
    for each in ns:
        if 'bodyRig' in each:
            ns = each

    if nameType == 'char':
        if 'bodyRig' in ns:
            if mc.objExists('{}:{}'.format(ns,nameCharCrv)):
                attrLists = mc.listAttr( '{}:{}'.format(ns,nameCharCrv), k = True, u = True, l = False)
                for each in attrLists:
                    if not 'MasterVis' in each:
                        if not 'GeoVis' in each:
                            mc.setAttr('{}:{}.{}'.format(ns, nameCharCrv, each), 0)
    else:
        print 'This asset is :', nameType

    return True

def resolve(entity=None):
    """ Run """
    return run()