# qc duplicated name
import sys
import os
import shutil
from collections import OrderedDict

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_utils.context import context_info
from rf_maya.lib import material_layer 
reload(material_layer)


def run(entity=None):
    """ Check material layers are setup correctly """
    layerInfo = material_layer.list_layers()

    if layerInfo: 
        return True 

    else:
        message = 'Material layer not setup correctly'
        raise custom_exception.PipelineError(message)
        return False


def resolve(entity=None):
    """ fix textures not in the pipeline """
    setup_layer(entity)
    return run(entity)



def skip(entity=None):
    if entity.step == context_info.Step.rig:
        return True
    else:
        logger.error('Cannot skip this QC')
        return False

def setup_layer(entity): 
    from rf_maya.lib import material_layer 
    reload(material_layer)
    from rf_utils.sg import sg_process
    
    entity.context.use_sg(sg_process.sg, project=entity.project, entityType=entity.entity_type, entityName=entity.name)
    tasks = sg_process.list_looks(entity.context.sgEntity)
    looks = [a.get('sg_name') for a in tasks]
    
    for look in looks: 
        material_layer.create_layer(look, mode=entity.step)
