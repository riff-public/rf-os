# qc geo input hisotry
import pymel.core as pm

def run(entity=None):
    """ Check for Geo input history """
    checkGrp = get_check_grp(entity)
    if not pm.objExists(checkGrp):
        raise Exception, 'Cannot find: %s' %(checkGrp)

    geoGrp = pm.PyNode(checkGrp)
    geo_with_his = []
    for tr in geoGrp.getChildren(ad=True, type='transform'):
        shps = tr.getShapes()
        if not shps:
            continue
        sn = tr.shortName()

        # loop over shapes
        for shp in shps:
            # it's a mesh
            if isinstance(shp, pm.nt.Mesh):
                hiss = shp.inMesh.inputs()
                if hiss:
                    geo_with_his.append(sn)
                    break
            # it's a NurbsCurve
            elif isinstance(shp, pm.nt.NurbsCurve):
                hiss = shp.create.inputs()
                if hiss:
                    geo_with_his.append(sn)
                    break

    if geo_with_his:
        return [False, 'These geos has input history:\n%s' %('\n'.join(geo_with_his))]
    else:
        return [True, 'All geos are history free.']

def skip(entity=None):
    return True

def get_check_grp(entity=None):
    if entity.step == 'groom':
        checkGrp = str(entity.projectInfo.asset.groom_geo_grp()) if entity else 'YetiGeo_Grp'
    else:
        checkGrp = str(entity.projectInfo.asset.geo_grp()) if entity else 'Geo_Grp'

    return checkGrp

def resolve(entity=None):
    status, check_result = run(entity)
    if not status:
        try:
            checkGrp = get_check_grp(entity)
            geoGrp = pm.PyNode(checkGrp)
            pm.delete(geoGrp, ch=True)
        except Exception, e:
            print e
            return False

    return True

