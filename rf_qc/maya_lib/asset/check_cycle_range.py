# template for export mod
import os
import sys
import logging
from Qt import QtWidgets

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.sg import sg_process
sg = sg_process.sg
loopLimit = 400

def run(entity=None): 
    """ Setup cycle frame range and export frame """
    # force enter and confirm range information everytime qc recheck
    return False 

def resolve(entity=None): 
    import maya.cmds as mc 
    cycleName = entity.process # taskName 
    data = sg_process.read_cycle_data(entity.project, entity.name, cycleName)
    loopRange = data.get('loopRange')
    exportRange = data.get('exportRange')
    message = 'Enter cycle duration: '

    if loopRange and exportRange: 
        previousDuration = loopRange[1] - loopRange[0] + 1
        message = '(Previous range was %s) Enter cycle duration:' % (previousDuration)
    
    result = mc.promptDialog(title='Set cycle range', message=message, button=['Confirm'])
    if result == 'Confirm': 
        duration = mc.promptDialog(query=True, text=True)

        if duration.isdigit(): 
            duration = int(duration)
            startFrame = entity.projectInfo.scene.start_frame or 1001 
            endFrame = startFrame + duration - 1 

            # cycle multiplier 
            mul = loopLimit / duration if loopLimit > duration else 1
            startRange = startFrame
            endRange = startFrame + (duration * mul)

            # data 
            cycleData = {'loopRange': [startFrame, endFrame], 'exportRange': [startRange, endRange]}
            # write data 
            sg_process.write_cycle_data(entity.project, entity.name, cycleName, cycleData)
            mc.playbackOptions(min=startFrame, max=endFrame, ast=startFrame, aet=endRange)

            return True 

        else: 
            logger.warning('%s is not number. Please enter number' % duration)
            return False 
    else: 
        return False 
 
