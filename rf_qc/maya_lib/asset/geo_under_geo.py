# qc geo under geo
import pymel.core as pm

def run(entity=None):
    """ Check if there's any geo transform under another geo transform """
    checkGrp = entity.projectInfo.asset.geo_grp()
    geoGrp = pm.PyNode(checkGrp)
    invalid_children = []
    for i in [m for m in geoGrp.getChildren(ad=True, type='transform')]:
        shp = i.getShape(ni=True)
        tr_children = i.getChildren(type='transform')
        if shp and tr_children:
            invalid_children.extend([t.shortName() for t in tr_children])

    if invalid_children:
        return [False, 'Illegal geo under geo:\n%s' %('\n'.join(invalid_children))]
    else:
        return [True, 'No geo under another geo']

def resolve(entity=None):
    """ Select illegal geos """
    status, result = run(entity)
    if not status:
        objs = result.split('\n')[1:]
        pm.select(objs, r=True)


