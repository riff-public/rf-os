# import maya.cmds as mc
import pymel.core as pm

def run(entity=None):
    """ Check Yeti node and Groom node connection from time1"""
    timeNode = pm.nt.Time('time1')
    yetiNodes = pm.ls(type='pgYetiMaya') + pm.ls(type='pgYetiGroom')
    node_wo_connections = []
    for yn in yetiNodes:
        inConnections = yn.currentTime.inputs(type='time')
        if not inConnections:
            node_wo_connections.append(yn)

    if node_wo_connections:
        return [False, 'Yeti node without time1 connection found:\n%s' %('\n'.join([n.shortName() for n in node_wo_connections]))]
    else:
        return [True, 'All Yeti node has time1 connection.']

def skip(entity=None):
    return False

def resolve(entity=None):
    timeNode = pm.nt.Time('time1')
    status, result = run(entity)
    fix_result = True
    if not status:
        nodeNames = result.split('\n')[1:]
        for nodeName in nodeNames:
            yn = pm.PyNode(nodeName)
            try:
                pm.connectAttr(timeNode.outTime, yn.currentTime, f=True)
            except Exception, e:
                print e
                fix_result = False
    
    return fix_result