# qc scene poly count
import maya.cmds as mc
from maya.api import OpenMaya as om

def run(entity=None):
    """ Check for geo with zero face """
   
    invalidGeos = getZeroFaceGeos()
    if invalidGeos:
        return [False, 'Found geo with no face.\n%s' %'\n'.join(invalidGeos)]
    else:
        return [True, 'All geo has at least 1 face count']

def resolve(entity=None):
    invalidGeos = getZeroFaceGeos()
    if invalidGeos:
        mc.select(invalidGeos, r=True)
    return None

def getZeroFaceGeos():
    dagIt = om.MItDag(om.MItDag.kDepthFirst, om.MFn.kMesh)
    invalid_geos = []
    while not dagIt.isDone():
        mDag = dagIt.getPath()
        meshFn = om.MFnMesh(mDag)
        try:
            poly_count = meshFn.numPolygons
        except Exception, e:
            invalid_geos.append(mDag.partialPathName())
        dagIt.next()

    return invalid_geos