# check locked normals objects
import pymel.core as pm
import maya.OpenMaya as om

def run(entity=None):
    ''' Find geo with locked normals. '''
    checkGrp = entity.projectInfo.asset.geo_grp()
    geoGrp = pm.PyNode(checkGrp)
    locked_geos = []
    for shp in geoGrp.getChildren(ad=True, type='mesh', ni=True):

        meshFn = shp.__apimfn__()
        for ni in xrange(meshFn.numNormals()):
            # isLocked = shp.isNormalLocked(ni.index())
            isLocked = meshFn.isNormalLocked(ni)
            if isLocked:
                locked_geos.append(shp.getParent().shortName())
                break

    if locked_geos:
        return [False, 'Geo with locked normals:\n%s' %('\n'.join(locked_geos))]
    else:
        return [True, 'All geo normals are not locked.']

def resolve(entity=None):
    status, result = run(entity)
    check = True
    if not status:
        objs = result.split('\n')[1:]
        
        for obj in objs:
            try:
                shp = pm.PyNode(obj).getShape()
                shp.unlockVertexNormals(range(shp.numVertices()))
            except Exception, e:
                print e
                if check == True:
                    check = False

    return check


        
