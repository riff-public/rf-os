import maya.cmds as mc

from rf_app.utils import nonSkinGeoWriter as nsw_app
reload(nsw_app)

def run(entity=None):
    """ Check hidden geo data. Hidden geo is a list of geometries that will be hidden at light build stage."""
    if entity.res != 'md':
        return [True, 'Check skipped. Resolution is not md']

    resolve(entity)
    return [True, 'Please confirm the list of Hidden Geo.']

def skip(entity=None):
    return True

def resolve(entity=None):
    nsw_app.show()

    return True