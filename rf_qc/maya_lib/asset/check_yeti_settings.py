import maya.cmds as mc
import pymel.core as pm

def run(entity=None):
    """ Check Yeti node display setting. SubD Display = Display&Render, Viewport Density <= 1.0 """
    yetiNodes = pm.ls(type='pgYetiMaya')
    incorrect_subd = []
    incorrect_dens = []
    for yn in yetiNodes:
        current_subdDisplay = yn.subdDisplay.get()
        if current_subdDisplay != 1:
            incorrect_subd.append(yn)
        current_dens = yn.viewportDensity.get()
        if current_dens > 1.0:
            incorrect_dens.append(yn)
    if incorrect_subd or incorrect_dens:
        msg = ''
        if incorrect_subd:
            inc_subd_names = '\n  '.join([n.nodeName() for n in incorrect_subd])
            msg += 'Incorrect Subd Display nodes:\n  %s' %(inc_subd_names)
        if incorrect_dens:
            inc_dens_names = '\n  '.join([n.nodeName() for n in incorrect_dens])
            msg += '\n\nToo high Viewport Density:\n  %s' %(inc_dens_names)
        return [False, msg]
    else:
        return [True, 'All Yeti Node display settings are good.']

def skip(entity=None):
    return True

def resolve(entity=None):
    yetiNodes = pm.ls(type='pgYetiMaya')
    for yn in yetiNodes:
        current_subdDisplay = yn.subdDisplay.get()
        if current_subdDisplay != 1:
            yn.subdDisplay.set(1)
        current_dens = yn.viewportDensity.get()
        if current_dens > 1.0:
            yn.viewportDensity.set(1.0)
    
    return True