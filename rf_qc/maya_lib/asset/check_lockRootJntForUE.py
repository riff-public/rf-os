# qc item name
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from utaTools.utapy import utaCore
reload(utaCore)
from rf_utils.context import context_info

def run(entity=None):
	# """ Check for item in Export Group """
	# expGrp = 'Export_Grp'
	# delGrp = 'Delete_Grp'

	# if not mc.objExists(expGrp):
	#     return [ False , 'Export_Grp not found.']

	# #check export group in delete group
	# if mc.objExists(delGrp):
	#     child = mc.listRelatives(delGrp,ad=True)
	#     if expGrp in child:
	#         return [ False , 'Found export group in delete group.']

	# child = mc.listRelatives(expGrp,c=True)

	# if child:

	#     #for process bsh rig
	#     if entity.process == 'bsh':
	#         if not delGrp in child:
	#             return [ False , 'Export group does not have delete group.' ]

	#     return [ True , 'Item in export group found.' ]
	# else :
	#     return [ False , 'Item in export group not found.' ]

	asset = context_info.ContextPathInfo() 
	charName = asset.name
	projectName = asset.project 
	assetType = asset.type 
	if assetType == 'prop':
		if mc.objExists('Root_Jnt'):
			utaCore.lockAttrObj(listObj = ['Root_Jnt'], lock = True, keyable = False)
	return True

def resolve(entity=None):
	""" Check for item in Export Group """
	# allGrp = mc.ls(tr=True,o=True)
	# expGrp = 'Export_Grp'

	# delGrp = [expGrp,'Delete_Grp','persp','top','front','side','bottom']
	# for each in allGrp:
	# 	prnt = mc.listRelatives(each,p=True)
	# 	chd = mc.listRelatives(each,c=True,type='camera')
	# 	if prnt or chd:
	# 		delGrp.append(each)

	# #remove delGrp from allGrp
	# for each in delGrp:
	# 	if each in allGrp:
	# 		allGrp.remove(each)

	# mc.parent(allGrp,expGrp)

	# #for process bsh rig
	# if entity.process == 'bsh':
	# 	child = mc.listRelatives(expGrp,c=True)
	# 	if not 'Delete_Grp' in child:
	# 		mc.parent('Delete_Grp',expGrp)
	asset = context_info.ContextPathInfo() 
	charName = asset.name
	projectName = asset.project 
	assetType = asset.type 
	if assetType == 'prop':
		if mc.objExists('Root_Jnt'):
			utaCore.lockAttrObj(listObj = ['Root_Jnt'], lock = True, keyable = False)
	return True
