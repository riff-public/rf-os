import maya.cmds as mc
import maya.OpenMaya as om

TOLERANCE = 0.000010

def run(entity=None):
    """ Check for asymmetry geo """
    if entity.type == 'char':
        mdGeoGrp = '%s' % (entity.projectInfo.asset.md_geo_grp()) if entity else 'MdGeo_Grp'
        asymmetryGeos = []
        if mc.objExists(mdGeoGrp):
            for tr in mc.listRelatives(mdGeoGrp, ad=True, pa=True, type='transform'):
                shapes = mc.listRelatives(tr, shapes=True, ni=True, pa=True)
                if shapes:
                    sym = checkSymmetry(mesh=shapes[0])
                    if sym == None:
                        continue
                    elif sym == False:
                        asymmetryGeos.append(tr)
        else: 
            return [False, 'MdGeoGrp: %s not found!' %mdGeoGrp]

        if not asymmetryGeos:
            return [True, 'Geos are symmetry.']
        else:
            msg = 'Found asymmetry geos!\nTo select, Right-click, choose Resolve.\n======== Asymmetry Geos ========'
            return [False, '%s\n%s' %(msg, '\n'.join(asymmetryGeos))]

    return [True, 'Skipped, only check for character asymmetries.']

def checkSymmetry(mesh):
    mfnMesh = None
    dagPath = om.MDagPath()
    msl = om.MSelectionList()
    msl.add(mesh)
    msl.getDagPath(0, dagPath)
    try:
        mfnMesh = om.MFnMesh(dagPath)
    except:
        return

    # check bounding box
    geoIncMatrix = dagPath.inclusiveMatrix()

    # get BoundingBox
    geoDagFn = om.MFnDagNode(dagPath)
    geoMBB = geoDagFn.boundingBox()  # get bounding box in object space
    geoMBB.transformUsing(geoIncMatrix)
    bb_min = geoMBB.min()
    bb_max = geoMBB.max()

    # skip this one if min X is on the positive side meaning it's a eyeball
    # or something intentionally put to the side
    if bb_min.x > TOLERANCE:
        return

    # find the mesh center
    meshCenter = mc.objectCenter(mesh, l=False)

    # check for bb symmetry
    diff = abs(bb_min.x) - abs(bb_max.x)
    if abs(diff) > TOLERANCE*10:
        return False

    # get points
    pWArray = om.MPointArray()   
    mfnMesh.getPoints(pWArray, om.MSpace.kWorld)
    pos, neg = [], []

    # find vertex count
    vtxNum = pWArray.length()

    # get positive and negative vertices
    for i in xrange(0, vtxNum):
        x_value = pWArray[i].x
        # it is in the middle
        if abs(x_value - meshCenter[0]) <= TOLERANCE:
            continue

        # it's either on the left or right side
        if x_value > meshCenter[0]:
            pos.append(i)
        else:
            neg.append(i)

    # the mesh is asymmetry if number of positive and negative list is not equal
    if len(pos) != len(neg):
        return False

    # sort negative vertices by it's x value for speed
    neg.sort(key=lambda n:pWArray[n].x, reverse=True)
    pos.sort(key=lambda p:pWArray[p].x)

    # matching left and right vertex
    for p in pos:
        pPt = pWArray[p]
        xMirrored = pPt.x*-1
        find = om.MPoint(xMirrored, pPt.y, pPt.z)

        for ni, n in enumerate(neg):
            # found matching vertex on the other side
            if pWArray[n].isEquivalent(find, TOLERANCE):
                neg.pop(ni)
                break
        else:
            # return False if only 1 vertex cannot find match
            return False

    return True

def skip(entity):
    return True

def resolve(entity=None):
    result, msg = run(entity)
    if not result:
        asymmetryGeos = msg.split('\n')[3:]
        mc.select(asymmetryGeos, r=True)
    return False