import maya.cmds as mc
import pymel.core as pm
from maya.api import OpenMaya as om

def run(entity=None):
    """ Check for imported nodes with namespace or Pasted__ """
   
    ns_nodes, pasted_nodes = getInvalidNodes()
    if ns_nodes or pasted_nodes:
        return [False, 'Found node with namespace or pasted__.\n%s' %'\n'.join(ns_nodes + pasted_nodes)]
    else:
        return [True, 'All node names are good.']


def skip(entity=None):
    return True

def resolve(entity=None):
    ns_nodes, pasted_nodes = getInvalidNodes()

    # remove pasted
    if pasted_nodes:
        for node in pasted_nodes:
            mc.lockNode(node, l=False)
            newName = node.replace('pasted__', '')
            mc.rename(node, newName)

     # remove all imported node namespaces..
    if ns_nodes:
        nss = set()
        for node in ns_nodes:
            nss.add(':'.join(node.split(':')[:-1]))
        # print nss
        removeNameSpaces(nss)
    return True

def removeNameSpaces(nss):
    allns = [ns for ns in pm.listNamespaces() if str(ns)[1:] in nss]
    # print allns
    if allns:
        removed = set()
        for ns in allns:
            nestedNss = ns.listNamespaces(recursive=True)
            if nestedNss:
                for cns in nestedNss[::-1]:
                    try:
                        pm.namespace(mergeNamespaceWithParent=True, removeNamespace=cns)
                    except:
                        pass
            try:
                pm.namespace(mergeNamespaceWithParent=True, removeNamespace=ns)
                removed.add(ns)
            except:
                pass
    else:
        print '\tNo namespace found.'

def getInvalidNodes():
    ns_nodes, pasted_nodes = [], []
    for node in mc.ls():  # list all nodes
        if not mc.referenceQuery(node, inr=True):
            if len(node.split(':')) > 1:
                ns_nodes.append(node)
            if node.split(':')[-1].startswith('pasted__'):
                pasted_nodes.append(node)

    return ns_nodes, pasted_nodes