import os
import imath

from rf_utils import cask 

import pymel.core as pm
import maya.OpenMaya as om

def run(entity=None):
    """ Check Geo_Grp boundingBox against Proxy abc"""
    diff_percentage = 5.0

    # get abc BB
    prAbcPath = getPrAbcPath(entity)

    if not os.path.exists(prAbcPath):
        return [True, 'Proxy ABC does not exists: %s' %prAbcPath]
    # prGeoGrpName = entity.projectInfo.asset.pr_geo_grp()
    pr_box3d = getAbcBB(str(prAbcPath))
    box_min = pr_box3d.min()
    box_max = pr_box3d.max()
    # pr_bb = om.MBoundingBox(om.MPoint(box_min.x, box_min.y, box_min.z), om.MPoint(box_max.x, box_max.y, box_max.z))

    # get current BB
    geoGrpName = entity.projectInfo.asset.geo_grp()
    geoGrp = pm.PyNode(geoGrpName)
    parent = geoGrp.getParent()
    geoGrpDag = geoGrp.__apimdagpath__()
    
    dagFn = om.MFnDagNode(geoGrpDag)
    md_bb = dagFn.boundingBox()  

    if parent:
        parentIncMat = parent.__apimdagpath__()
        incMatrix = parentIncMat.inclusiveMatrix()
        md_bb.transformUsing(incMatrix)

    bb_min = md_bb.min()
    bb_max = md_bb.max()
    curr_width = md_bb.width()
    curr_height = md_bb.height()
    curr_depth = md_bb.depth()
    
    minx_delta = abs(box_min.x - bb_min.x)
    miny_delta = abs(box_min.y - bb_min.y)
    minz_delta = abs(box_min.z - bb_min.z)

    maxx_delta = abs(box_max.x - bb_max.x)
    maxy_delta = abs(box_max.y - bb_max.y)
    maxz_delta = abs(box_max.z - bb_max.z)

    percent_mult = (diff_percentage/100.0)
    if minx_delta > curr_width*percent_mult or minx_delta > curr_height*percent_mult or minx_delta > curr_depth*percent_mult\
        or maxx_delta > curr_width*percent_mult or maxy_delta > curr_height*percent_mult or maxz_delta > curr_depth*percent_mult:
        return [False, 'Too much difference between current geo and proxy geo.']
    else:
        return [True, 'Geo bounding box matches with proxy geo.']

def skip(entity=None):
    return True

def resolve(entity=None):
    prAbcPath = getPrAbcPath(entity)
    if os.path.exists(prAbcPath):
        ns = '%s_%s_pr' %(entity.name, entity.step_code)
        pm.createReference(prAbcPath, namespace=ns, rnn=True)
        prGeoGrp = pm.ls('%s:%s' %(ns, entity.projectInfo.asset.geo_grp()))
        if prGeoGrp:
            pm.select(prGeoGrp[0], r=True)

    return None

def getPrAbcPath(entity):
    entity2 = entity.copy()
    entity2.context.update(step='model', process='main', res='pr')
    name = entity2.output_name(outputKey='cache', hero=True)
    prAbcPath = '%s/%s' % (entity2.path.output_hero().abs_path(), name)

    return prAbcPath

def getAbcBB(abcPath):
    arc = cask.Archive(abcPath)

    gbnds = imath.Box3d()
    exit = False
    objs = [arc.top]

    while not exit:
        next_objs = []
        for o in objs:
            for c in o.children.values():
                if c.type() == 'PolyMesh':
                    transform = c.parent
                    bnds_prop = c.properties['.geom/.selfBnds']
                    bnds = bnds_prop.get_value()
                    wbnds = bnds * transform.global_matrix()
                    gbnds.extendBy(wbnds)
                else:
                    next_objs.append(c)

        if next_objs:
            objs = next_objs
        else:
            exit = True 

    return gbnds