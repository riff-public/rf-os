# check for incorrect shape name
import pymel.core as pm

def run(entity=None):
    ''' Find shape node with incorrect naming. '''
    checkGrp = entity.projectInfo.asset.geo_grp()
    geoGrp = pm.PyNode(checkGrp)
    invalid_shapes = []
    for i in [m for m in geoGrp.getChildren(ad=True, type='transform')]:
        shp = i.getShape(ni=True)
        if not shp:
            continue

        correct_name = '%sShape' %i.nodeName()
        if shp.nodeName() != correct_name:
            invalid_shapes.append(shp.shortName())

    if invalid_shapes:
        return [False, 'Incorrect shape names:\n%s' %('\n'.join(invalid_shapes))]
    else:
        return [True, 'No geo under another geo']

    return True


def resolve(entity=None):
    """ Rename shapes with incorrect names """
    status, check_res = run(entity)
    result = True
    if not status:
        invalid_shapes = check_res.split('\n')[1:]
        for shp in invalid_shapes:
            shp = pm.PyNode(shp)
            tr = shp.getParent()
            correct_name = '%sShape' %tr.nodeName()
            try:
                shp.rename(correct_name)
            except Exception, e:
                print e
                result = False

    return result

# def skip(entity=None):
#     return True
