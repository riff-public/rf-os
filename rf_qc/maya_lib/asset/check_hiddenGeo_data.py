import maya.cmds as mc

from rf_app.utils import hiddenGeoWriter as hgw_app
reload(hgw_app)

def run(entity=None):
    """ Check hidden geo data. Hidden geo is a list of geometries that will be hidden at light build stage."""
    if entity.res != 'md':
        return [True, 'Check skipped. Resolution is not md']

    resolve(entity)
    return [True, 'Please confirm the list of Hidden Geo.']

def skip(entity=None):
    return True

def resolve(entity=None):
    hgw_app.show()

    return True