# qc duplicated name
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception

def run(entity=None):
    """ Check for Export Group Value """

    expGrp = 'Export_Grp'
    check = mc.objExists( expGrp )

    if check :

        ts = mc.xform( expGrp,q=True, t=True)
        ro = mc.xform( expGrp,q=True, ro=True )
        s = mc.xform( 'Export_Grp',q=True, s=True )
        #check export group ts , ro value
        if ts == [0,0,0] and ro == [0,0,0] and s == [1,1,1]:
            return True
        else:
            return [ False , 'Export group transform or rotate value not [0,0,0].' ]
    else :
        return [ False , 'Export group not found.' ]

def resolve(entity=None):
    """ Check for Export Group Value """

    res , msg = run(entity)

    expGrp = 'Export_Grp'
    check = mc.objExists(expGrp)

    #rename export group in other group
    if check:
        mc.select(expGrp)