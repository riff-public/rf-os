#!/usr/bin/env python
# -- coding: utf-8 --
import maya.cmds as mc
import maya.OpenMaya as om
import maya.OpenMayaAnim as oma

from rf_utils.context import context_info
from rf_utils.sg import sg_process
from rf_maya.rftool.clean.ref_cleaner import preroll_default_pose
reload(preroll_default_pose)

def run(entity=None):
    ''' Check if there's any key for pre roll for sim. '''
    noPreRollList, namespaceList = preroll_default_pose.check()
    if not noPreRollList:
        return True

    # if any key outside anim range, there's pre roll for sim
    '''
    if not noPreRollList:
        return [True, 'Pre roll found.']
    else:
        mc.confirmDialog( title='Warning', message='This shot need Pre roll for SIM Please create Pre roll in frame %s . \n Char : %s' %(framePre,(', '.join(noPreRollList))))
        return [False, 'No Pre roll found.']
    '''

def resolve(entity=None):
    app = preroll_default_pose.show()
    app.submitted.connect(set_item_status)

# def skip(entity=None):
#     return True

def doc(entity=None): 
    """ ตรวจสอบว่าได้ทำ preroll สำหรับ sim หรือยัง หากไม่ผ่าน แปลว่ายังไม่ได้ทำ กรุณาทำด้วย """ 
    return 

def set_item_status(): 
    from rf_utils import icon
    from rftool.utils.ui import maya_win 
    mayaWindow = maya_win.getMayaWindow()

    for w in mayaWindow.children(): 
        if w.objectName() in ['ShotExportUI', 'SubmitQueueUi']: 
            item = w.ui.qcWidget.get_item('check_pre_roll_sim')
            w.ui.qcWidget.update_status(item, icon.ok, True)

