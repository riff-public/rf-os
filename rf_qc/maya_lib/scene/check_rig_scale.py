import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.context import context_info
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# from utaTools.utapy import setAttributeDefault
from rf_maya.rftool.rig.utaTools.utapy import setAttributeDefault

reload(setAttributeDefault)


def run(entity=None): 
	""" Check if the asset is not proxy """ 
	setAttributeDefault.setAttributeDefault()
	return True

def resolve(entity=None): 
	""" Please remove all display layers """ 
	
	return False 
