#!/usr/bin/env python
# -- coding: utf-8 --
from rf_utils.context import context_info
import pymel.core as pm
import maya.cmds as mc
import re
from rftool.on2 import on2_utils as on2
reload(on2)


def run(entity=None):
    """ Rename namespace that is not match asset Name """ 
    tsns = on2.list_time_step_nodes()
    wrong_assets = list()
    
    for tsn in tsns: 
        if not tsn.is_correct_asset(): 
            wrong_assets.append(tsn)

    if wrong_assets: 
        for tsn in wrong_assets: 
            print('wrong connected asset node {}'.format(tsn))

    return False if wrong_assets else True


def resolve(entity=None):
    tsns = on2.list_time_step_nodes()
    for a in tsns: 
        on2.rename_node_to_namespace(a)

    return run(entity)

def skip(entity=None):
    return True

def doc(entity=None): 
    """ แก้ชื่อ on2 node ที่ไม่ตรงกับ asset """ 
    return 
