#!/usr/bin/env python
# -- coding: utf-8 --
import maya.cmds as mc
import maya.OpenMaya as om
import maya.OpenMayaAnim as oma
from rf_utils.widget import dialog

def run(entity=None):
    ''' Check if there's any key for pre/post roll. '''
    try:
        shot_node = entity.name_code
        startFrame, endFrame = (mc.shot(shot_node, q=True, st=True), mc.shot(shot_node, q=True, et=True))
    except:
        return [False, 'Cannot get frame range.']

    dgIt = om.MItDependencyNodes(om.MFn.kAnimCurve)
    result = set()

    while not dgIt.isDone():
        curveFn = oma.MFnAnimCurve(dgIt.thisNode())
        if not curveFn.isFromReferencedFile() and curveFn.animCurveType() in (0, 1, 2, 3):
            numKey = curveFn.numKeyframes()
            if numKey:
                keyStart = curveFn.time(0).value()
                if numKey < 2:
                    endIndex = 0
                else:
                    endIndex = numKey-1
                keyEnd = curveFn.time(endIndex).value()
                curve_name = str(curveFn.name())

                # collect keys outside the anim range
                if keyStart < startFrame and keyEnd > endFrame:
                    result.add(curve_name)
        dgIt.next()

    # if any key outside anim range, there's pre/post roll
    if result:
        return [True, 'Pre/Post roll found.']
    else:
        return [False, 'No Pre/Post roll found.']


def resolve(entity=None):
    status, result = run(entity)
    if not status:
        #-*-coding: utf-8 -*-
        message = 'ยังไม่มี Pre/Post roll                      '
        dialog.MessageBox.warning('Check Preroll & Postroll',message)
    return True


def skip(entity=None):
    return True

def doc(entity=None): 
    """ ตรวจสอบว่าได้ทำ preroll และ postroll หรือยัง หากไม่ผ่าน แปลว่ายังไม่ได้ทำ กรุณาทำด้วย """ 
    return 
