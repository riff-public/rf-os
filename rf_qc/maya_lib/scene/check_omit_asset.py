#!/usr/bin/env python
# -- coding: utf-8 --

# qc duplicated name 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.widget import dialog
from rf_utils.context import context_info
from rftool.utils import maya_utils
from rftool.shade import shade_utils


def run(entity=None): 
	""" Check and remove all display layers and renderLayer """ 
	omit_assets = list_omit_assets(entity)
	if omit_assets: 
		mc.select(*omit_assets)
	return True if omit_assets else False 


def resolve(entity=None): 
	""" Please remove all display layers and renderLayer """ 
	# window = dialog.ScrollMessageBox.show(tilte, message, ls)
	# maya_utils.replace_reference()
	from rf_qc.maya_lib.scene import check_unmatch_namespace
	from rf_qc.maya_lib.scene import add_asset_tag
	from rf_utils.sg import sg_process 
	from Qt import QtWidgets
	omit_dict = dict()
	omit_list = sg_process.get_omit_assets(entity.project)

	if omit_list: 
		replaceable_list = [a for a in omit_list if a['sg_redirect_asset']]
		# title = 'Omit asset list'
		# message = '{} asset will be replaced'.format(len(omit_list))
		# ls = [a['code'] for a in omit_list]
		# win = dialog.ScrollMessageBox.show(title, message, ls, parent=None) 
		for asset in replaceable_list: 
			omit_dict[asset['code']] = asset

		print('{} Replaceable asset'.format(len(replaceable_list)))
		print(replaceable_list)

		print('---- start replacing ----')

		refs = list_refs(entity)
		replace_dict = dict()

		for ref in refs: 
			ns = mc.referenceQuery(ref, ns=True)[1:]
			ref_path = ref.split('{')[0]
			asset = context_info.ContextPathInfo(path=ref_path)

			asset.update_context_from_file()
			if asset.name in omit_dict.keys(): 
				omit_asset = omit_dict[asset.name]
				replace_asset = omit_asset.get('sg_redirect_asset').get('name')
				replace_type = omit_asset.get('sg_redirect_asset.Asset.sg_asset_type')
				replace_entity = asset.copy()
				replace_entity.context.update(entityGrp=replace_type, entity=replace_asset, look='main')
				rig_path = '{}/{}'.format(replace_entity.path.hero(), replace_entity.output_name(outputKey='rig', hero=True))

				# print('----', replace_entity.path.hero())
				# print('----', replace_entity.output_name(outputKey='materialRig', hero=True))

				# replace ref 
				print('replacing {} -> {}'.format(ref, rig_path))
				maya_utils.replace_reference(ref, rig_path)
				# fix namespace 
				# apply shade
				shade_utils.ref_shade(entity.projectInfo, ns, 'rig')
				print('replace success')

		check_unmatch_namespace.resolve_namespace(entity)
		add_asset_tag.run(entity)


def doc(entity=None): 
	""" เช็ค asset ที่ถูก omit ไปแล้ว resolve จะทำการ replace เป็น asset ใหม่ (ถ้ามีข้อมูล) """ 
	return 

def list_omit_assets(entity): 
	from rf_utils.sg import sg_process 
	# fetch omit list 
	omit_assets = list()
	omit_dict = dict()
	omit_list = sg_process.get_omit_assets(entity.project)
	if omit_list: 
		for asset in omit_list: 
			omit_dict[asset['code']] = asset

		# references 
		refs = list_refs(entity)
		for ref in refs: 
			ns = mc.referenceQuery(ref, ns=True)[1:]
			asset = context_info.ContextPathInfo(path=ref)
			rig_grp = '{}:{}'.format(ns, entity.projectInfo.asset.top_grp())

			if asset.name in omit_dict.keys(): 
				omit_assets.append(rig_grp)

	return omit_assets


def list_refs(entity): 
	refs = mc.file(q=True, r=True)
	assets = list()
	for ref in refs: 
		ns = mc.referenceQuery(ref, ns=True)[1:]
		rig_grp = '{}:{}'.format(ns, entity.projectInfo.asset.top_grp())
		if mc.objExists(rig_grp): 
			assets.append(ref)

	return assets

	

