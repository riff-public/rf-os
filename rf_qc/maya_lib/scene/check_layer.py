#!/usr/bin/env python
# -- coding: utf-8 --

# qc duplicated name 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.widget import dialog

allowLayers = ['defaultLayer','defaultRenderLayer']

def run(entity=None): 
	""" Check and remove all display layers and renderLayer """ 
	layers = mc.ls(type=['displayLayer','renderLayer'])
	result = check_layers(layers)
	return True if result == 0 else False 


def resolve(entity=None): 
	""" Please remove all display layers and renderLayer """ 
	layers = mc.ls(type=['displayLayer','renderLayer'])
	result = check_layers(layers)
	message = ''

	if result != 0:
		#-*-coding: utf-8 -*-
		message = 'เช็ค QC : check_bake_dynFk'
		for layer in result:
			if not layer.endswith(('DynFk_dlyr')) and not layer.endswith(('DynFk_DLyr')):
				if mc.objectType(layer) == 'renderLayer':
					mc.editRenderLayerGlobals( currentRenderLayer='defaultRenderLayer')
				mc.delete(layer)
			else:
				dialog.MessageBox.warning('Check Layer',message)

	if run(entity):
		return True

def doc(entity=None): 
	""" เช็ค และลบ display layer ออกทั้งหมด เพราะ asset ที่ถูกซ่อนจาก display layer 
	จะไม่ได้ถูกซ่อนใน Lighting """ 
	return 

def check_layers(layers): 
	invalidLayers = []
	for layer in layers: 
		if not any(a in layer for a in allowLayers):
			invalidLayers.append(layer)

	return 0 if not invalidLayers else invalidLayers