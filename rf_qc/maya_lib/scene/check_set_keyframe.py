#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.context import context_info
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils.widget import dialog 

from rf_app.asm import asm_lib

def run(entity=None): 
	""" Check if set is cleaned from keyframe """ 
	keys = get_key()
	return False if keys else True 

def resolve(entity=None): 
	allAsms = asm_lib.list_asms()
	keyObjs = []
	keys = []
	action1 = 'Show keys'
	action2 = 'Delete keys'

	for asm in allAsms: 
		key = mc.keyframe(asm, q=True, n=True)
		if key: 
			keyObjs.append(asm)
			keys += key

	if keys: 
		result = dialog.CustomMessageBox.show(title='Clear keys', message='Choose Action', buttons=[action1, action2])

		if result.value == action1: 
			mc.select(keyObjs)

		if result.value == action2: 
			mc.delete(keys)
		
	return run(entity)

def doc(entity=None): 
	""" ห้าม key set ถ้าข้อนี้ไม่ผ่าน ให้ resolve ว่าไป key asset ชิ้นไหนใน set """
	return 

def get_key(): 
	allAsms = asm_lib.list_asms()
	keys = []

	for asm in allAsms: 
		key = mc.keyframe(asm, q=True, n=True)

		if key: 
			keys+=key

	return keys
