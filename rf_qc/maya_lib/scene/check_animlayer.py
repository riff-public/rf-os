#!/usr/bin/env python
# -- coding: utf-8 --

# qc duplicated name 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.widget import dialog

def run(entity=None): 
	""" Check and Merge all anim Layer """ 
	layers = mc.ls(type=['animLayer'])
	result = len(layers)
	return True if result <= 1 else False 


def resolve(entity=None): 
	""" Please remove all display layers and renderLayer """ 
	layers = mc.ls(type=['animLayer'])
	message = ''

	if len(layers) > 1:
		#-*-coding: utf-8 -*-
		message = 'ยังไม่ได้ merge anim Layer \n merge anim Layer ด้วย'
		dialog.MessageBox.warning('Check Anim Layer', message)

	if run(entity):
		return True

def doc(entity=None): 
	""" เช็ค และ Merge anim layer ทั้งหมด""" 
	return 