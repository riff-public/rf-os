#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
import pymel.core as pm
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
import sum_utils
reload(sum_utils)

# Project Quantum
def run(entity=None):
    """ """
    result = check_cam(entity)
    return False if result else True 

def resolve(entity=None): 
    """ """
    cams = check_cam(entity)

    for cam in cams: 
        mc.file(cam, rr=True)

    return False if check_cam(entity) else True


def doc(entity=None): 
	""" ลบกล้อง reference ของเราออก """ 
	return 


def check_cam(entity): 
    key = 'cam_rig'
    refs = mc.file(q=True, r=True)
    cams = []
    for ref in refs: 
        ns = mc.referenceQuery(ref, ns=True)
        if key in os.path.basename(ref): 
            if not entity.name_code in ns: 
                cams.append(ref)

    return cams 

