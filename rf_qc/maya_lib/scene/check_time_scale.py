#!/usr/bin/env python
# -- coding: utf-8 --

import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import maya.mel as mm 
from rf_maya.lib import sequencer_lib

def run(entity=None): 
	""" Check if sequencer time is scaling """ 
	scale = mc.getAttr('%s.scale' % entity.name_code)
	return True if scale == 1 else False

def resolve(entity=None): 
	""" Shift shot to start at 1001 """ 
	mc.setAttr('%s.scale' % entity.name_code, 1)
	return run(entity)

def doc(entity=None): 
	""" เช็คว่า sequencer มีการยืดหรือหดเวลาหรือไม่ """ 
	return 