#!/usr/bin/env python
# -- coding: utf-8 --
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_utils.pipeline import asset_tag
reload(asset_tag)
from rftool.utils import pipeline_utils
from rf_utils.context import context_info
from rf_app.asm import asm_lib
import check_asset
from rf_utils.widget import dialog
from rf_maya.rftool.shade import shade_utils
from rf_maya.rftool.utils import maya_utils
import os

def run(entity=None):
	""" Add information to asset before cache """
	# check Rig_Grp
	# topGrp = '|%s' % entity.projectInfo.asset.top_grp() if entity else '|Rig_Grp'
	assets = cache_asset(entity)
	add_set_description(entity)

	for each in assets:
		asset, assetGeoGrp, path = each 
		project = asset.project
		assetType = asset.type
		assetName = asset.name
		assetId = 0
		step = ''
		process = ''
		publishVersion = ''
		publishPath = ''
		look = get_look_mtr(assetGeoGrp)
		refPath = filter_ref_path(path)
		adPath = ''
		if refPath: 
			adPath = '%s/%s' % (asset.path.asm_data().abs_path(), asset.output_name(outputKey='ad', hero=True))
		asset_tag.update_tag(assetGeoGrp, project=project, assetType=assetType, assetName=assetName, assetId=assetId, step=step, process=process, publishVersion=publishVersion, publishPath=publishPath, adPath=adPath, refPath=refPath, look=look)

	if check_status(entity):
		return True


def resolve(entity=None):
	""" Checking for Rig_Grp and Geo_Grp """
	#-*-coding: utf-8 -*-
	if check_status(entity):
		return run(entity)
	else:
		dialog.MessageBox.warning('Check Asset','ยังไม่ผ่าน QC check_asset')

def doc(entity=None): 
	""" add information สำหรับ cache เข้า asset """ 
	return 


def cache_asset(entity=None): 
	assets = pipeline_utils.list_cache_asset(entity)
	return assets 


def filter_ref_path(path): 
	entity = context_info.ContextPathInfo(path=path)
	if entity.entity_type == 'asset': 
		heroPath = entity.path.hero().abs_path()
		if heroPath in path: 
			return path 

		else: 
			entity.context.update(step='rig', process='main', res='md')
			rigFile = entity.output_name(outputKey='rig', hero=True)
			return '%s/%s' % (heroPath, rigFile)

def add_set_description(entity):
	setList = asm_lib.list_set(entity)
	setGrp = entity.projectInfo.asset.set_grp() if entity else 'Set_Grp'

	if mc.objExists(setGrp):
		root = mc.listRelatives(setGrp, c=True)
		if root:
			for asm in setList:
				asm = asm_lib.MayaRepresentation(asm)
				asmPath = asm.asm_data
				asset = context_info.ContextPathInfo(path = asmPath)
				adPath = '%s/%s' % (asset.path.asm_data().abs_path(), asset.output_name(outputKey='ad', hero=True))
				asm.set_description(adPath)

def check_status(entity):
	status_check_asset = check_asset.run(entity)

	if status_check_asset:
		return True
	else:
		return False

def get_look_mtr(assetGeoGrp):
	look = 'main'
	obj = maya_utils.find_ply(assetGeoGrp)
	if obj:
		shadeFilePath, shadeNamespace, geoNameSpace = shade_utils.get_assigned_ref_shade(obj[0])
		asset = context_info.ContextPathInfo(path=shadeFilePath)
		outputList = asset.guess_outputKey(os.path.basename(shadeFilePath))
		if outputList:
			outputKey, step = outputList[0]
			asset.context.update(step=step)
			asset.extract_name(os.path.basename(shadeFilePath), outputKey=outputKey)
			look = asset.look

	return look