#!/usr/bin/env python
# -- coding: utf-8 --
# qc duplicated name
import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
from rf_utils import custom_exception
from rf_maya.lib import maya_lib
from rf_utils.context import context_info

def run(entity=None):
	""" Remove non reference namespaces """
	# check if any references
	refs = mc.file(q=True, r=True)
	namespaces = maya_lib.non_ref_namespace()
	return True if not namespaces else False


def resolve(entity=None):
	""" Checking for Rig_Grp and Geo_Grp """
	maya_lib.clear_stacked_namespace_in_scene()
	refresh_namespace()
	namespaces = maya_lib.non_ref_namespace()
	if not namespaces:
		return True

def refresh_namespace(entity=None):
    # update new namespace
    from rf_app.publish.scene import app
    reload(app)
    import rf_app.publish.scene.export as export
    entity = context_info.ContextPathInfo(path=str(pm.sceneName()))
    shotName = entity.name_code
    config = export.read_config(entity.project)
    filterMods = config.get(entity.step, config.get('default'))
    data = export.export_list(entity, shotName, filterMods)

def doc(entity=None): 
    """ ลบ namespace ที่ซ้อนกันและไม่ได้ใช้งาน """ 
    return 

