#!/usr/bin/env python
# -- coding: utf-8 --
from collections import defaultdict

import maya.cmds as mc
import pymel.core as pm
import maya.OpenMaya as om
import maya.OpenMayaAnim as oma

tol = 0.001

def run(entity=None):
    ''' Check if controller scale is keyed to 0. '''
    nodes, curves = get_zero_scale_curves()
    if nodes:
        return [False, 'Zero scale key found on controllers: \n%s' %('\n'.join(nodes))]
    else:
        return [True, 'No zero scale key found on controllers.']

def get_zero_scale_curves():
    dgIt = om.MItDependencyNodes(om.MFn.kAnimCurve)
    
    nodes = set()
    curves = defaultdict(list)
    while not dgIt.isDone():
        curveFn = oma.MFnAnimCurve(dgIt.thisNode())
        
        # non referenced animate type curve
        if not curveFn.isFromReferencedFile() and curveFn.animCurveType() in (0, 1, 2, 3):
            # find what's output attr is connected to
            outPlug = curveFn.findPlug('output')
            plugArray = om.MPlugArray()
            outPlug.connectedTo(plugArray, False, True)
            numConnections = plugArray.length()

            # must connect to only one node
            if numConnections == 1:
                connectedPlug = plugArray[0]
                mObj = connectedPlug.node()
                # the node of type transform
                if mObj.hasFn(om.MFn.kTransform):
                    # get attr name - scaleX, ...
                    attr = connectedPlug.partialName(False, False, False, False, False, True)
                    # attribute name startswith scale
                    if attr.startswith('scale'):
                        dagFn = om.MFnDagNode(mObj)
                        node = dagFn.partialPathName()
                        # node name ends with Ctrl
                        if node.endswith('_Ctrl') or node.endswith('_ctrl'):
                            keyRange = range(curveFn.numKeyframes())
                            for k in keyRange:
                                currValue = curveFn.value(k)
                                if currValue < tol:
                                    curveName = curveFn.name()
                                    time = curveFn.time(k).value()
                                    nodes.add(node)
                                    curves[curveName].append(time)
        dgIt.next()
    # print curves
    return nodes, curves

def resolve(entity=None):
    nodes, curves = get_zero_scale_curves()
    if nodes:
        for crv, times in curves.iteritems():
            for t in times:
                pm.keyframe(crv, e=True, t=t, vc=tol)
        mc.select(list(nodes))
    return True

def skip(entity=None):
    return True

def doc(entity=None): 
    """ เช็คว่า ctrl ชิ้นไหนมีการ key scale เป็น 0 หรือไม่ หากมีกด resolve จะเป็นการ set scale -> 0.001 """ 
    return 
