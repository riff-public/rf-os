#!/usr/bin/env python
# -- coding: utf-8 --
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.pipeline import check

def run(entity=None): 
	""" Check if references are in the pipeline """ 
	info = check.pipeline_assets()
	validAssets = info[check.Config.valid]
	invalidAssets = info[check.Config.invalid]
	return True if not invalidAssets else False

def resolve(entity=None): 
	""" Shift shot to start at 1001 """ 
	info = check.pipeline_assets()
	validAssets = info[check.Config.valid]
	invalidAssets = info[check.Config.invalid]
	message = 'All assets are valid'
	
	if invalidAssets: 
		message = '%s Invalid asset path \n- %s' % (len(invalidAssets), ('\n- ').join(invalidAssets))

	removeButton = 'Remove'
	closeButton = 'Cancel'
	result = mc.confirmDialog( title='Warning', message=message, button=[removeButton, closeButton] , defaultButton=removeButton , cancelButton=closeButton)
	if result == 'Remove':
		for asset in invalidAssets:
			print 'Remove Reference %s' % (asset)
			mc.file( asset, removeReference=True )

	return run(entity)

def doc(entity=None): 
	""" เช็คว่า reference ที่ใช้อยู่ในระบบหรือไม่ หากอยู่ผิดที่จะไม่สามารถส่งไป cache ได้ """
	return 

