#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.context import context_info
from rf_app.publish.scene import app
from rf_utils import publish_info

import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.publish.scene.utils import maya_hook as hook
from rftool.scene.playblast import utils
from rftool.scene.playblast import thread

import check_asset

class Config: 
    taskMap = {'main': 'anim', 'qc': 'cache', 'default': 'anim'}

def run(entity=None):
    """ Check publish playblast """ 
    if check_status(entity):
        mc.file( save=True, f=True, type='mayaAscii')
        result = check_publish(entity)
        return True if result else False

def resolve(entity=None): 
    """ Please remove all display layers """
    if check_status(entity): 
        result = check_publish(entity)
        if not result:
            mc.file( save=True, f=True, type='mayaAscii')
            result = publish_mov_queue(entity)
        return True if result else False

def doc(entity=None): 
    """ เช็คว่าเคย publish playblast หรือยัง ถ้ายังจะถูกบังคับ publish playblast """
    return 

def publish_mov(entity): 
    taskName = Config.taskMap.get(entity.process, Config.taskMap.get('default'))
    panel = hook.get_panel()
    hook.set_panel_show_only_polygon(panel)
    hook.viewport_update(True)

    dst = utils.playblast_wip(entity, entity.name_code, showHud=True, light=False, subversion=False)
    inputDatas = [{'shotName': entity.name_code, 'filePath': dst}]
    submitThread = thread.SubmitPublish(entity, taskName, inputDatas)
    submitThread.run()

    # shotName = entity.name_code
    # data = app.get_export_dict(entity, shotName, ['anim_mov'])
    # app.do_export(entity, itemDatas=data, uiMode=False, publish=True)
    return True

def publish_mov_queue(entity):
    taskName = Config.taskMap.get(entity.process, Config.taskMap.get('default'))
    panel = hook.get_panel()
    hook.set_panel_show_only_polygon(panel)
    hook.viewport_update(True)

    scene = context_info.ContextPathInfo()
    shotName = scene.name_code
    startFrame, endFrame, seqStartFrame, seqEndFrame = hook.shot_info(shotName)
    framerate = scene.projectInfo.render.fps()
    removeImgs = True
    upload = True
    camera = hook.camera_info(shotName)
    dst = utils.playblast_to_queue(scene,startFrame, endFrame, camera, shotName, task_name = taskName, sg_upload=upload, framerate=framerate,remove_images=removeImgs)
    
    return True if dst[0] else False


def check_publish(entity): 
    # reg = publish_info.PreRegisterShot(entity)
    # pubVersion = reg.current_version('workspace')
    pubVersion = entity.version
    entity.context.update(publishVersion=pubVersion)
    versionName, ext = os.path.splitext(entity.publish_name())
    versionEntity = find_version(entity, versionName)
    return True if versionEntity else False

    
def find_version(entity, versionName): 
    from rf_utils.sg import sg_utils
    sg = sg_utils.sg 
    taskName = Config.taskMap.get(entity.process, Config.taskMap.get('default'))
    filters = [['project.Project.name', 'is', entity.project], ['entity.Shot.code', 'is', entity.name], ['code', 'is', versionName], ['sg_task.Task.content', 'is', taskName]]
    fields = ['code', 'id', 'sg_task']
    result = sg.find_one('Version', filters, fields)
    return result 

def check_status(entity):
    c_asset = check_asset.run(entity)
    if c_asset :
        return True
    else:
        return False
