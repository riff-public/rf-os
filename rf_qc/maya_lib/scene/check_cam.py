#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.context import context_info
import logging 
import re
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.publish.scene.utils import maya_hook as hook
from rf_utils.widget import dialog

def run(entity=None): 
	""" Check if camera is correct """ 
	check_connect_sequencer(entity)
	emptyShots, wrongName, cam_status = check_camshot(entity)
	return True if not emptyShots and not wrongName and cam_status else False 

def resolve(entity=None): 
	""" Please remove all display layers """ 
	check_connect_sequencer(entity)
	emptyShots, wrongName, cam_status = check_camshot(entity)
	camShot = 'camshot'

	for shot in emptyShots:
		namespace = '%s_cam' % shot
		objs = mc.ls('%s:*' % namespace)
		camera = [a for a in objs if mc.objectType(a, isType='camera')]

		if camera: 
			hook.set_current_cam(shot, camera[0])

	for cam in wrongName:
		mc.rename(cam, camShot)

	if not cam_status:
		#-*-coding: utf-8 -*-
		message = 'camera ไม่ใช่ reference \nbuild camera ใหม่ / cache camera ใหม่'
		dialog.MessageBox.warning('Check Camera',message)

	if not emptyShots and not wrongName and cam_status:
		return True

def doc(entity=None): 
	""" เช็คว่า camera sequencer และกล้องชื่อถูกต้องไหม 
	sequencer ต้องชื่อตาม shot เช่น scm_act1_q0010_s0010 sequencer จะชื่อ s0010 """ 
	return 

def check_connect_sequencer(entity):
	shotName = entity.name_code
	sequencerName = 'sequencer1'
	sequencerManager = 'sequenceManager1'
	sequencerList = mc.ls(type="sequencer")

	shotConnect = '%s.message'%(shotName)
	shotConnectFrme = '%s.sequenceEndFrame'%(shotName)
	sequencerConnect = '%s.message'%(sequencerName) 
	sequencerAttr = '%s.shots[0]'%(sequencerName)
	sequencerManagerAttr = '%s.sequences[0]'%(sequencerManager)

	if not mc.isConnected(sequencerConnect, sequencerManagerAttr):
		for seq in sequencerList:
			if mc.isConnected('%s.message'%seq, sequencerManagerAttr):
				mc.disconnectAttr('%s.message'%seq, sequencerManagerAttr)

		if mc.objExists(shotName):
			if not mc.isConnected(shotConnect, sequencerAttr) and not  mc.isConnected(shotConnectFrme, sequencerAttr):
				mc.connectAttr(shotConnect, sequencerAttr)

			if not mc.isConnected(sequencerConnect, sequencerManagerAttr):
				mc.connectAttr(sequencerConnect, sequencerManagerAttr)

		else:
			#-*-coding: utf-8 -*-
			message = 'ไม่มี Shot Node \nbuild camera ใหม่'
			dialog.MessageBox.warning('Check Camera',message)

def check_shot(): 
	shots = hook.list_shot()
	emptyShots = []
	wrongName = []
	camShot = 'camshot'

	for shot in shots:
		cam = hook.find_cam(shot)

		emptyShots.append(shot) if not cam else None
		find = re.findall("camshot$",cam[0])
		wrongName.append(cam[0]) if not find else None

	return emptyShots, wrongName

def check_camshot(entity): 
	shots = hook.list_shot()
	cam_status = True
	emptyShots = []
	wrongName = []
	camShot = 'camshot'

	for shot in shots:
		cam = hook.find_cam(shot)
		shotName = str(entity.name_code)

		if not shot == shotName:
			mc.rename(shot,shotName)

		cam_status = set_camera(shotName)
		cam = hook.find_cam(shotName)

		emptyShots.append(shot) if not cam else None
		find = re.findall("camshot$",cam[0])
		wrongName.append(cam[0]) if not find else None

	return emptyShots, wrongName, cam_status

def set_camera(shotName):
	cam_ns = '%s_cam' %shotName

	namespace_list = mc.namespaceInfo(listOnlyNamespaces=True)
	if cam_ns not in namespace_list:
		cameras = mc.ls(type=('camera'))
		camera_list = [cam for cam in cameras if 'camshot' in cam]
	else:
		camera_list = mc.namespaceInfo(cam_ns,listNamespace=True )

	cameraShape = [i for i in camera_list if mc.ls(i,type=('camera'))][-1]
	mc.shot(shotName,shotName=shotName,currentCamera=cameraShape,e=True)

	return True if mc.referenceQuery(cameraShape, inr=True) else False