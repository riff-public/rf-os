#!/usr/bin/env python
# -- coding: utf-8 --
import os
import sys 
from Qt import QtCore
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.context import context_info
from rf_utils.pipeline import cache_list
reload(cache_list)
from rftool.utils.ui import maya_win 

envKey = 'ASSET_IN_VIEW'
allowLayers = ['defaultLayer']
os.environ[envKey] = '0'


def run(entity=None): 
	""" Check for display layers """ 
	entity = context_info.ContextPathInfo()
	cache = cache_list.CacheList(entity)
	#return cache.status()
	return check_user_interaction()

def resolve(entity=None): 
	""" Please remove all display layers """ 
	app = run_check_asset()
	app.submitted.connect(set_item_status)
	cache = cache_list.CacheList(entity)
	return check_user_interaction()

def doc(entity=None): 
	""" ต้องเช็คว่า asset ที่ใช้ใน shot นี้มีอะไรบ้าง ไม่อย่างนั้น render จะได้ asset ที่ผิด """ 
	return

def run_check_asset(): 
	entity = context_info.ContextPathInfo()
	return cache_list.show(entity)


def check_user_interaction(): 
	return True if os.environ[envKey] == '1' else False


def set_item_status(): 
	from rf_utils import icon
	mayaWindow = maya_win.getMayaWindow()

	for w in mayaWindow.children(): 
	    if w.objectName() in ['ShotExportUI', 'SubmitQueueUi']: 
	        item = w.ui.qcWidget.get_item('check_asset_inview')
	        w.ui.qcWidget.update_status(item, icon.ok, True)
