#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.context import context_info
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_app.asm import asm_lib

def run(entity=None): 
	""" Check if all asm correct hidden """ 
	result_set = check_hide_asm('Set_Grp')
	result_shotdress = check_hide_asm('ShotDress_Grp')
	return True if not result_set and not result_shotdress else False 

def resolve(entity=None): 
	""" Please check hidden all asm """ 
	result_set = check_hide_asm('Set_Grp')
	result_shotdress = check_hide_asm('ShotDress_Grp')

	assets = []
	if result_set:
		for asm in result_set:
			asm_lib.MayaRepresentation(asm).set_hidden(True)
			print 'set %s hidden' % asm

	if result_shotdress:
		for asm in result_shotdress:
			asm_lib.MayaRepresentation(asm).set_hidden(True)
			print 'shotdress %s hidden' % asm

	return run(entity=entity) 

def doc(entity=None): 
	""" ตรวจสอบว่า ซ่อนของใน set ถูกวิธีหรือไม่ ถ้าไม่ผ่านกด resolve เพื่อเช็คว่าตัวไหนซ่อนผิดวิธี และซ่อนให้ถูก """
	return 

def check_hide_asm(grp): 
	invalid = []
	asms = asm_lib.sweep_hide_asm(grp)
	if asms: 
		for asm in asms:
			invalid.append(asm)
			print '%s is wrong hidden' % asm

	return invalid