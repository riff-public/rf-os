#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.context import context_info
import logging 
import re
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget import dialog

def run(entity=None): 
    """ Check if lambert1 is assigned in OA """ 
    lam = []
    con = mc.listConnections('lambert1.outColor')
    name = mc.listConnections(con, type="mesh")
    for n in name:
        if 'LocoGeo' in n:
            lam.append(n)
    return True if not lam else False 

def resolve(entity=None): 
    """ list locoGeo with lambert1 """ 
    lam = []
    con = mc.listConnections('lambert1.outColor')
    name = mc.listConnections(con, type="mesh")
    for n in name:
        if 'LocoGeo' in n:
            lam.append(n)

    if lam:
        #-*-coding: utf-8 -*-
        message = ', '.join(lam)
        dialog.MessageBox.warning('Check lambert1',message)

        mc.select(lam)

        return False
    else:
        return True

def doc(entity=None): 
    """ เช็คว่ามี lambert1 ถูก assign อยู่ใน LocoGeo ไหม  """ 
    return 