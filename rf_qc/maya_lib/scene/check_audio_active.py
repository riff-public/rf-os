#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys
import shutil
import maya.cmds as mc 
import maya.mel as mm 
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# Project Quantum
def run(entity=None):
	"""  """ 
	active_audio()
	return True

def resolve(entity=None):
	"""  """ 
	active_audio()
	return True

def doc(entity=None): 
	""" เปิด audio """ 
	return 

def active_audio(): 
	""" turn on audio track """ 
	sounds = mc.ls(type='audio') 
	# assume that there's 1 sound node 
	if sounds: 
		node = sounds[0]

		# copy 
		filename = mc.getAttr('%s.filename' % node)
		currentPath = mc.file(q=True, loc=True)
		dst = '%s/%s' % (os.path.dirname(currentPath), os.path.basename(filename))

		if not os.path.exists(dst): 
			shutil.copy2(filename, dst)
			print 'Copy sound to %s' % dst

		# repath audio 
		mc.setAttr('%s.filename' % node, dst, type='string')
		# set active 
		mm.eval('setSoundDisplay %s 1;' % node)