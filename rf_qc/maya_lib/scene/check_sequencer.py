#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.context import context_info
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.publish.scene.utils import maya_hook as hook
from rf_maya.lib import sequencer_lib

def run(entity=None): 
	""" Check if camera is correct """ 
	seq = detect_sequencer(entity)
	return True if seq else False 

def resolve(entity=None): 
	""" Please remove all display layers """ 
	# select a camera 
	camera = detect_camera()
	if camera: 
		name = entity.name_code
		startTime = mc.playbackOptions(q=True, min=True)
		endTime = mc.playbackOptions(q=True, max=True)
		sequencer_lib.create_shot(name, camera, startTime=startTime, endTime=endTime, sequenceStartTime=startTime, sequenceEndTime=endTime)
		return run(entity)
	else: 
		mc.confirmDialog(title='Wrong Selection', message='Please select camera and run "resolve"', button=['OK'])
  

def doc(entity=None): 
	""" เช็คว่า camera sequencer และกล้องชื่อถูกต้องไหม 
	sequencer ต้องชื่อตาม shot เช่น scm_act1_q0010_s0010 sequencer จะชื่อ s0010 """ 
	return 

def detect_camera(): 
	sels = mc.ls(sl=True)
	if sels: 
		cam = sels[0]
		if mc.objectType(cam, isType='camera'): 
			return cam 

		if mc.objectType(cam, isType='transform'): 
			shape = mc.listRelatives(cam, s=True)
			if mc.objectType(shape[0], isType='camera'): 
				return shape[0] 
	return False

def detect_sequencer(entity): 
	shots = mc.ls(type='shot')
	preferredShotname = entity.name_code
	if shots: 
		for shot in shots: 
			name = mc.getAttr('%s.shotName' % shot)
			if name == preferredShotname: 
				return True 

def check_shot(): 
	shots = hook.list_shot()
	emptyShots = []

	if shots: 
		for shot in shots: 
			cam = hook.find_cam(shot)
			emptyShots.append(shot) if not cam else None

	return emptyShots
