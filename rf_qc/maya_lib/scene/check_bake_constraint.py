#!/usr/bin/env python
# -- coding: utf-8 --
import pymel.core as pm
import maya.cmds as mc
from rf_utils.context import context_info
reload(context_info)
from rf_utils.sg import sg_process
reload(sg_process)

def run(entity=None):
	""" Check for unbaked constrainted objects """
	bakeList = list_bake_constraint()
	if bakeList:
		return [False, 'Please bake keyframe constrainted objects:\n%s' %'\n'.join(bakeList)]
	else:
		return [True, 'No unbaked constrainted objects.']


def resolve(entity=None):
	bakeList = list_bake_constraint()
	pm.select(bakeList)
	print 'Please bake keyframe constrainted objects:\n%s' %'\n'.join(bakeList)

def skip(entity=None):
	return True

def list_bake_constraint():
	conObj = get_constraint_obj()
	bakeList = []
	if conObj:
		scene = mc.file(q=True, loc=True)
		entity = context_info.ContextPathInfo(path = scene)
		shotName = entity.name
		projName = entity.project
		duration = sg_process.get_shot_duration(projName, shotName)
		startFrame = entity.projectInfo.render.start_frame()
		endFrame = startFrame + (duration-1)
		preFrame = startFrame

		for bakeObj in conObj:
			currentTime = startFrame
			for i in range(startFrame,endFrame+1):
				if i == endFrame:
					break
				nextFrame = pm.findKeyframe( bakeObj, time=(currentTime,currentTime),which="next")
				preFrame = currentTime
				result = nextFrame - preFrame
				if not result == 1:
					bakeList.append(str(bakeObj))
					break
				else:
					currentTime = currentTime + 1
	return bakeList

def get_constraint_obj():
	conObj = []
	allCon = mc.ls(type = 'constraint')
	#bakeCon = []
	for con in allCon:
		if not mc.referenceQuery(con, isNodeReferenced=True):
			constraint = pm.PyNode(con)
			obj = constraint.getParent()
			target = pm.parentConstraint(constraint, q=True, targetList=True)
			if target:
				if not obj.namespace() == target[0].namespace():
					conObj.append(obj)
	return conObj

def doc(entity=None): 
	""" เช็ค object ที่มีการ constraint แล้วยังไม่ได้ bake key """ 
	return 

