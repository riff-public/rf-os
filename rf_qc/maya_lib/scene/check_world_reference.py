#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# Project Quantum
def run(entity=None):
    """ """
    root = root_rig()
    return False if root else True

def resolve(entity=None): 
    """ """
    root = root_rig()
    return False if root else True

def doc(entity=None): 
	""" เอา reference ออกจาก group ที่สร้างเอง """ 
	return 

def root_rig(): 
    """ unparent rig to root """ 
    refs = mc.file(q=True, r=True) 

    for ref in refs: 
        ns = mc.referenceQuery(ref, ns=True)[1:]
        nodes = top_ref(ns)

        for node in nodes: 
            if mc.listRelatives(node, p=True): 
                mc.parent(node, w=True)

def top_ref(ns): 
    """ find top node references """ 
    sels = mc.ls('%s:*' % ns, l=True, type='transform', dag=True) + mc.ls('%s:*:*' % ns, l=True, type='transform', dag=True)
    topGrp = []
    for sel in sels: 
        parents = mc.listRelatives(sel, p=True, f=True)
        if not parents: 
            topGrp.append(sel)
        else: 
            if not mc.referenceQuery(parents[0], inr=True): 
                topGrp.append(sel)
                
    return topGrp
