#!/usr/bin/env python
# -- coding: utf-8 --
import os
import sys
from collections import defaultdict
import json

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import pymel.core as pm
from rf_utils.context import context_info
from rf_utils.pipeline import check
from rf_utils.widget import dialog

# result_dict = {namespace: {error:..., data:{} }}


def update_result(result_dict, key, value):
    result_dict['error'] = ''
    result_dict['data'] = {}
    result_dict[key] = value

    return result_dict

def run(entity=None):
    """ Check if all cacheable asset has the latest geo  """
    result = check_latest(entity)
    return True if not result else False

def skip(entity=None):
    result = run(entity)
    logName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]
    logPath = '%s/%s/_data/cache_log/%s/%s.txt' % (os.environ['RFPROJECT'], entity.project, entity.name, logName)

    if not os.path.exists(os.path.dirname(logPath)): 
        os.makedirs(os.path.dirname(logPath))

    if result:
        with open(logPath, 'w') as log:
            json.dump(result, log, indent=4)
    return True

def resolve(entity=None):
    result = check_latest(entity)
    message = ''
    if result:
        for asset_n in result:
            if result[asset_n]['data']['alien']:
                message += 'Geo Alien \n %s : %s \n' %(asset_n, (result[asset_n]['data']['alien']))

            if result[asset_n]['data']['mismatch topology']:
                message += 'Geo MisMatch Topology\n %s : %s \n' %(asset_n, (result[asset_n]['data']['mismatch topology']))

            if result[asset_n]['data']['missing']:
                message += 'Geo Missing \n %s : %s \n' %(asset_n, (result[asset_n]['data']['missing']))

        #-*-coding: utf-8 -*-
        message = '%s \n%s'%(str(message),'ติดต่อแผนก Rig') 

        dialog.MessageBox.warning('Check Asset',message)
    else:
        return True

def doc(entity=None): 
    """ เช็คว่า rig ที่ใช้อยู่ เป็น geometry ล่าสุดจาก model หรือไม่ """ 
    return 

def check_latest(entity=None):
    if not entity:
        entity = context_info.ContextPathInfo(path=str(pm.sceneName()))
    geoGrpName = entity.projectInfo.asset.geo_grp()
    mdGeoGrpName = entity.projectInfo.asset.md_geo_grp()
    step = entity.step

    refs = pm.listReferences()
    result_dict = defaultdict(dict)
    for ref in refs:
        ns = ref.namespace
        if not ns:
            continue
        lsGrps = mc.ls('%s:%s|%s:%s' %(ns, geoGrpName, ns, mdGeoGrpName))
        if not lsGrps:
            continue
        checkGrp = lsGrps[0]

        asset = context_info.ContextPathInfo(path=str(ref.path))
        isAsset = True if asset.entity_type == 'asset' else False

        # if the asset is already cached, find refPath attribute on geoGrp
        if isAsset:
            refPath = ref.path 
        else:
            geoGrp = pm.PyNode(checkGrp).getParent()
            if geoGrp.hasAttr('refPath'):
                refPath = geoGrp.refPath.get()
            else:
                logger.warning('Skipping: %s, cannot find refPath attribute in %s' %(ns, geoGrpName))
                continue

        # find model ABC
        asset = context_info.ContextPathInfo(path=str(refPath))
        asset.context.update(step='model', process='main', res='md')
        publishPath = asset.path.output_hero().abs_path()
        dataFile = asset.output(outputKey='cache')

        abcPath = '%s/%s' % (publishPath, dataFile)
        if not os.path.exists(abcPath):
            result_dict[ns] = update_result(result_dict[ns], 'error', 'ABC does not exists: %s' %abcPath)
            continue

        sn_result = None
        try:
            sn_checker = check.CheckGeo(checkGrp=checkGrp, inputData=abcPath, includeShape=False)
            sn_result = sn_checker.check()
        except Exception, e:
            print e
            result_dict[ns] = update_result(result_dict[ns], 'error', 'Error checking geo:\n%s' %(e))
            continue

        if sn_result:
            if sn_result['alien'] or sn_result['mismatch topology'] or sn_result['missing']:
                result_dict[ns] = update_result(result_dict[ns], 'data', sn_result)

    if result_dict:
        status = False
        print dict(result_dict)
    else:
        status = True
        print 'All assets geos are up to date.',

    return result_dict
