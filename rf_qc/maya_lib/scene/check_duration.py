#!/usr/bin/env python
# -- coding: utf-8 --

import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import maya.mel as mm 
from rf_maya.lib import sequencer_lib
sgDuration = 0

def run(entity=None): 
	""" Check if duration match Shotgun """ 
	# this only work in shot not work in sequence 
	sgDuration = get_duration(entity)
	duration = get_scene_duration(entity)

	if duration == sgDuration: 
		return True 
	logger.info('Duration mismatch - Shotgun - %s Scene - %s' % (sgDuration, duration))
	return False 


def resolve(entity=None): 
	""" Adjust duration to match Shotgun """ 
	sgDuration = get_duration(entity)
	duration = get_scene_duration(entity)
	confirmMessage = 'Duration not match. Shotgun (%s) / Maya (%s)' % (sgDuration, duration)

	if not sgDuration == duration: 
		if sgDuration > duration: 
			# extend tail 
			value = sgDuration - duration
			result = confirm_action('%s\nExtend sequencer "%s" frame?' % (confirmMessage, value))
			
			if result: 
				sequencer_lib.extend_shot(entity.name_code, value, tail=True)

		if sgDuration < duration: 
			# trim tail
			value = duration - sgDuration
			result = confirm_action('%s\nTrim sequencer "%s" frame?' % (confirmMessage, value))

			if result: 
				sequencer_lib.trim_shot(entity.name_code, value, tail=True)

	return run(entity)

def doc(entity=None): 
	""" ตรวจสอบความยาว Shot ว่าถูกเปลี่ยนแปลกจาก Edit หรือตรงกับ Shotgun หรือไม่ 
	หากไม่ตรงให้กด Resolve เพื่อแก้ไข """ 
	return 


def get_duration(entity): 
	from rf_utils.sg import sg_utils 
	sg = sg_utils.sg
	filters = [['project.Project.name', 'is', entity.project], ['code', 'is', entity.name]]
	fields = ['sg_working_duration']
	result = sg.find_one('Shot', filters, fields)
	global sgDuration 
	sgDuration = result.get('sg_working_duration') or 0
	return sgDuration


def get_scene_duration(entity): 
	startFrame, endFrame, seqStartFrame, seqEndFrame = sequencer_lib.shot_info(entity.name_code)
	duration = endFrame - startFrame + 1 
	return duration


def confirm_action(message): 
	result = mc.confirmDialog( title='Confirm', message=message, button=['Yes','No'])
	return True if result == 'Yes' else False


