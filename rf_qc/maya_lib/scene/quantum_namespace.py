#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# Project Quantum
def run(entity=None):
	""" """
	control_namespace()
	return True

def resolve(entity=None): 
	""" """
	control_namespace()
	return True

def doc(entity=None): 
	""" เปลี่ยน namespace ให้ตรงกับ ชื่อ file reference """ 
	return 

def control_namespace(): 
	""" rename namespace to match file name """ 
	refs = mc.file(q=True, r=True)
	for ref in refs:
		namespace = mc.referenceQuery(ref, ns=True)[1:]
		newNs, ext = os.path.splitext(os.path.basename(ref))
		if not newNs == namespace: 
			if not mc.namespace(ex=newNs): 
				mc.namespace(rename=(namespace, newNs))
				print 'rename %s' % newNs
			else: 
				print 'namespace exists', newNs