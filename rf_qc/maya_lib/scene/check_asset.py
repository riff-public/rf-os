#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.context import context_info
from rf_utils.widget import dialog
from rf_maya.rftool.clean.ref_cleaner import app2 as app
import logging
reload(app)

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

def run(entity=None): 
	""" Check if the asset is md not proxy """ 
	result = check_ref(entity)
	return True if not result else False 

def resolve(entity=None): 
	""" Please remove all display layers """ 
	action1 = 'Select'
	action2 = 'Swicth to md'
	message_text = ''
	result = check_ref(entity)
	assets = []
	geoGrp = entity.projectInfo.asset.geo_grp()

	if result: 
		for path in result: 
			namespace = mc.referenceQuery(path, ns=True)
			grp = '%s:%s' % (namespace[1:], geoGrp)

			if mc.objExists(grp): 
				longname = mc.ls(grp)
				grp = longname[0]
				assets.append(grp)

				if len(longname) > 1: 
					logger.error('Duplicated name %s' % grp)
			else:
				logger.debug('Asset not in pipeline%s' % path)
				dialog.MessageBox.warning('asset not in pipeline', 'Check Reference \n'+ path)
				refNode  = mc.referenceQuery(path, rfn=True)
				mc.file(path, loadReference=refNode, prompt=False)

	if assets:
		for path in result:
			message_text += path+'\n'
		result_dialog = dialog.CustomMessageBox.show(title='Check Asset', message=message_text, buttons=[action1, action2])
		if result_dialog.value == action1: 
			mc.select(assets)

		if result_dialog.value == action2: 
			asset_switch = app.RefCleaner()
			status ,missing = asset_switch.switch_md()
			if not status:
				message = ''
				for ms in missing:
					message += str(ms)+'\n'
				#-*-coding: utf-8 -*-
				message += "\nasset ยังไม่มี rig md ติดต่อพี่มิ้ม"
				dialog.MessageBox.warning('Check Asset',message)

	result = check_ref(entity)
	if not result:
		return True

def doc(entity=None): 
	""" เช็คว่า asset ไม่ใช่ proxy ถ้าเป็น proxy cache ไปก็ใช้ไม่ได้ """ 
	return 

def check_ref(entity): 
	invalid = []
	paths = mc.file(q=True, r=True)
	# hard coded 'pr'
	keyMd = '_md'
	keyHero = '.hero'
	path_output = entity.path.hero().abs_path()

	for path in paths: 
		name = os.path.basename(path)
		if not path_output in path:
			if keyMd not in path or not keyHero in path:
				if not '_mtr' in name and  not '_cam' in name and not '.abc' in name and  not '_instdress' in name:
					invalid.append(path)

	return invalid