#!/usr/bin/env python
# -- coding: utf-8 --
import maya.cmds as mc 

def run(entity=None): 
    """ Check and remove all unused reference nodes """ 
    result = get_unused_refNodes()
    return True if not result else False 

def resolve(entity=None): 
    """ Please remove all display layers and renderLayer """ 
    unused_refNodes = get_unused_refNodes()
    for refNode in unused_refNodes:
        mc.lockNode(refNode, l=False)
    try:
        mc.delete(unused_refNodes)
    except:
        print "Cannot delete all reference nodes"

    return run(entity)

def doc(entity=None): 
    """ เช็คหา และ ลบ reference node เปล่าที่ไม่ได้ถูก link กับ file ใดๆ (__pasted) ซึ่งอาจทำให้เกิดปัญหากับการ export camera """ 
    return 

def get_unused_refNodes():
    unused_refNodes = []
    for refNode in mc.ls(type='reference'):
        if refNode == 'sharedReferenceNode':
            continue

        try:
            mc.referenceQuery(refNode, f=True)
        except RuntimeError:
            unused_refNodes.append(refNode)

    return unused_refNodes