#!/usr/bin/env python
# -- coding: utf-8 --
import maya.cmds as mc
import maya.mel as mm
from rf_utils import custom_exception
from rf_utils.pipeline import asset_tag
reload(asset_tag)
from rftool.utils import pipeline_utils
reload(pipeline_utils)

def run(entity=None):
	""" add tag information for mattePaint asset """
	# check Rig_Grp
	# topGrp = '|%s' % entity.projectInfo.asset.top_grp() if entity else '|Rig_Grp'
	assets = cache_asset(entity)

	for each in assets: 
		asset, assetGeoGrp, path = each 
		if asset.type == 'mattepaint': 
			pipeline_utils.add_geo_texture_tag(assetGeoGrp)

	return True


def resolve(entity=None):
	""" Checking for Rig_Grp and Geo_Grp """
	return run(entity)

def doc(entity=None): 
	""" เพิ่ม information สำหรับ cache ลงใน mattePaint asset """ 
	return 


def cache_asset(entity=None): 
	assets = pipeline_utils.list_cache_asset(entity)
	return assets 
