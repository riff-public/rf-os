#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
import pymel.core as pm
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
import sum_utils
reload(sum_utils)

# Project Quantum
def run(entity=None):
    """ """
    result = check_cam(entity)
    return result

def resolve(entity=None): 
    """ """
    shotName = entity.name_code
    sumSceneName = sum_utils.sumShotName(entity)
    sumCam = sum_utils.cameraName(sumSceneName)

    camera = mc.shot(shotName, q=True, cc=True) 
    DummyCam = sum_utils.DummyCamera(camera, sumCam, False)
    st = mc.playbackOptions(q=True, min=True)
    et = mc.playbackOptions(q=True, max=True)

    
    with DummyCam: 
        mc.bakeResults(DummyCam.dupcam, simulation=True, t=(st, et))

    nodes = [a for a in mc.listRelatives(DummyCam.dupcam, c=True) if mc.objectType(a) in ['parentConstraint', 'scaleConstraint']]
    pm.delete(nodes) if nodes else None
    
    camera = mc.shot(shotName, q=True, cc=True)
    pm.shot(shotName, e=True, cc=DummyCam.dupcam[0].getShape())

    if mc.referenceQuery(camera, inr=True): 
        path = mc.referenceQuery(camera, f=True)
        mc.file(path, rr=True)

    return check_cam(entity)


def doc(entity=None): 
	""" covert กล้องของเราเป็นของ Ultra """ 
	return 


def check_cam(entity): 
    shotName = entity.name_code
    camera = mc.shot(shotName, q=True, cc=True)

    if not mc.referenceQuery(camera, inr=True): 
        return True 
    return False

