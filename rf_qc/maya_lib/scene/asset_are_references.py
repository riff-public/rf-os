#!/usr/bin/env python
# -- coding: utf-8 --
import os
import sys 
import maya.cmds as mc 
import maya.mel as mm 
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# Project Quantum
def run(entity=None):
    """ """
    nonRef = non_reference_scene(entity)
    layer = deleteLayers(entity)
    mc.delete(layer)
    print nonRef
    if nonRef:
        mc.select(nonRef)
    return True if nonRef==[] else False

def resolve(entity=None):
    """ """
    nonRef = non_reference_scene(entity)
    layer = deleteLayers(entity)
    mc.delete(layer)
    if nonRef:
        msg = 'remove grp obj non reference: \n\n%s' %('\n'.join(nonRef))
        result = confirm_action(msg)
        if result:
            mc.delete(nonRef)
            nonRef = non_reference_scene(entity)
    return True if nonRef==[] else False

def non_reference_scene(entity=None):
    """ list obj non reference """ 
    allObjs = mc.ls(lf=True, dag=True)
    grpNonRef = []

    for obj in allObjs:
        if not mc.referenceQuery(obj,inr=True):
            if mc.objectType(obj) != 'camera' and 'Constraint' not in mc.objectType(obj):
                if mc.listRelatives(obj,p=True,f=True):
                    listObj = mc.listRelatives(obj,p=True,f=True)[0]
                    obj = listObj.split('|')
                    if obj[1] == 'previs_grp':
                        if len(obj) > 2:
                            if not obj[2] in grpNonRef:
                                grpNonRef.append(obj[2])
                                if not obj[1] in grpNonRef:
                                    grpNonRef.append(obj[1])
                        elif not obj[1] in grpNonRef:
                            grpNonRef.append(obj[1])
                    else:
                        if not obj[1] in grpNonRef:
                            if not mc.referenceQuery(obj[1],inr=True):
                                grpNonRef.append(obj[1])
                            else:
                                objN = mc.listRelatives(obj[1],f=True)
                                for obj_n in objN:
                                    if not mc.referenceQuery(obj_n,inr=True) and 'Constraint' not in obj_n:
                                        grpNonRef.append(obj_n)
                else:
                    if not obj[1] in grpNonRef:
                        grpNonRef.append(obj)

    return grpNonRef

def deleteLayers(entity=None):
    """ list display layer """ 
    layer = []
    layers = mc.ls(type='displayLayer')
    allowLayers = 'defaultLayer'

    for layer_n in layers:
        if layer_n != allowLayers and not ':' in layer_n:
            layer.append(layer_n)

    return layer

def confirm_action(message):
    result = mc.confirmDialog( title='Confirm', message=message, button=['Yes','No'])
    return True if result == 'Yes' else False

def doc(entity=None): 
    """ เช็คว่ามี obj ที่ไม่ใช่ reference หรือไม่ """ 
    return 