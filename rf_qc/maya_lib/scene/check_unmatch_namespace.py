#!/usr/bin/env python
# -- coding: utf-8 --
from rf_utils.context import context_info
import pymel.core as pm
import maya.cmds as mc
import re

def run(entity=None):
    """ Rename namespace that is not match asset Name """ 
    entity = context_info.ContextPathInfo(path=str(pm.sceneName()))
    geoGrpName = entity.projectInfo.asset.geo_grp()
    mdGeoGrpName = entity.projectInfo.asset.md_geo_grp()
    step = entity.step

    # # check Except Seq
    # if 'q0090' in entity.sequence or 'q0130' in entity.sequence:
    #     return True
    # # check Except Seq
    
    refs = pm.listReferences()
    unMatch = []

    for ref in refs:
        ns = ref.namespace
        if not ns:
            continue
        lsGrps = mc.ls('%s:%s|%s:%s' %(ns, geoGrpName, ns, mdGeoGrpName))
        if not lsGrps:
            continue
        checkGrp = lsGrps[0]

        asset = context_info.ContextPathInfo(path=str(ref.path))
        isAsset = True if asset.entity_type == 'asset' else False

        # if the asset is already cached, find refPath attribute on geoGrp
        if isAsset:
            refPath = ref.path 
        else:
            geoGrp = pm.PyNode(checkGrp).getParent()
            if geoGrp.hasAttr('refPath'):
                refPath = geoGrp.refPath.get()
            else:
                logger.warning('Skipping: %s, cannot find refPath attribute in %s' %(ns, geoGrpName))
                continue
        asset = context_info.ContextPathInfo(path=str(refPath))

        if not ns.split('_')[0] == asset.name and not asset.type == 'mattepaint' and not 'Mtr' in ns:
            unMatch.append(ns)
        elif not re.findall(r'\d{3}', ns) and not asset.type == 'mattepaint' and not 'Mtr' in ns:
            unMatch.append(ns)

    return True if not unMatch else False

def resolve(entity=None):
    resolve_namespace(entity)
    refresh_namespace(entity)
    
    return True


def resolve_namespace(entity=None): 
    entity = context_info.ContextPathInfo(path=str(pm.sceneName()))
    geoGrpName = entity.projectInfo.asset.geo_grp()
    mdGeoGrpName = entity.projectInfo.asset.md_geo_grp()
    step = entity.step

    refs = pm.listReferences()

    for ref in refs:
        ns = ref.namespace
        if not ns:
            continue
        lsGrps = mc.ls('%s:%s|%s:%s' %(ns, geoGrpName, ns, mdGeoGrpName))
        if not lsGrps:
            continue
        checkGrp = lsGrps[0]

        asset = context_info.ContextPathInfo(path=str(ref.path))
        isAsset = True if asset.entity_type == 'asset' else False

        # if the asset is already cached, find refPath attribute on geoGrp
        if isAsset:
            refPath = ref.path 
        else:
            geoGrp = pm.PyNode(checkGrp).getParent()
            if geoGrp.hasAttr('refPath'):
                refPath = geoGrp.refPath.get()
            else:
                logger.warning('Skipping: %s, cannot find refPath attribute in %s' %(ns, geoGrpName))
                continue
        asset = context_info.ContextPathInfo(path=str(refPath))
        
        if not ns.split('_')[0] == asset.name:
            suffix = re.findall('[0-9]+', ns)
            if suffix:
                suffix = int(suffix[-1])
                newns = '{}_{:03}'.format(asset.name, suffix)
                while mc.namespace(exists = newns ):
                    suffix += 1
                    newns = '{}_{:03}'.format(asset.name, suffix)
                mc.file( ref, e=True, namespace = newns )
                print 'RENAME %s to %s' % (ns , newns)

        elif not re.findall(r'\d{3}', ns):
            suffix = 1
            newns = '{}_{:03}'.format(asset.name, suffix)
            while mc.namespace(exists = newns ):
                suffix += 1
                newns = '{}_{:03}'.format(asset.name, suffix)
            mc.file( ref, e=True, namespace = newns )
            print 'RENAME %s to %s' % (ns , newns)


def refresh_namespace(entity=None):
    # update new namespace
    from rf_app.publish.scene import app
    reload(app)
    import rf_app.publish.scene.export as export
    shotName = entity.name_code
    config = export.read_config(entity.project)
    filterMods = config.get(entity.step, config.get('default'))
    data = export.export_list(entity, shotName, filterMods)

def doc(entity=None): 
    """ แก้ชื่อ namespace ที่ไม่ตรงกับ asset """ 
    return 
