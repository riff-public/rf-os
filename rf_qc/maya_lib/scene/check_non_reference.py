#!/usr/bin/env python
# -- coding: utf-8 --
import maya.cmds as mc 
import maya.mel as mm 
from rftool.utils.ui import maya_win 
from rf_maya.rftool.clean.ref_cleaner import non_ref_cleaner
cleaner =  non_ref_cleaner.Cleaner()

def run(entity=None): 
	""" Check for non-pipeline objects """ 
	#nonRefObjs = get_non_pipeline_objs(entity)
	nonRefObjs = cleaner.get_non_pipeline_objs()
	return False if nonRefObjs else True

def resolve(entity=None): 
	""" Group guide geometry in 'Temp_Grp' """
	nonRefObjs = cleaner.get_non_pipeline_objs()
	if nonRefObjs:
		app = non_ref_cleaner.show()
		app.submitted.connect(set_item_status)
	else:
		return True

def doc(entity=None): 
	""" ถ้า asset ไม่ใช่ reference จะไม่ผ่านการ check หากมี polygon นอกระบบที่ต้องการใช้เพื่อไกด์ 
	ให้ใส่ไว้ใน group ชื่อ Guide_Grp """ 
	return

def set_item_status(): 
	from rf_utils import icon
	mayaWindow = maya_win.getMayaWindow()

	for w in mayaWindow.children(): 
	    if w.objectName() in ['ShotExportUI', 'SubmitQueueUi']: 
	        item = w.ui.qcWidget.get_item('check_non_reference')
	        w.ui.qcWidget.update_status(item, icon.ok, True)
