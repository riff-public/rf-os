#!/usr/bin/env python
# -- coding: utf-8 --
# qc optimized set
import maya.cmds as mc 
from rf_maya.rftool.clean.ref_cleaner import app2
from rf_utils.widget import dialog
from rf_app.asm import asm_lib

clean = app2.RefCleaner()

def run(entity=None): 
	""" Check for optimized set """ 
	
	return False


def resolve(entity=None): 
	""" Hide selected set """
	action1 = 'Optimized'
	action2 = 'Skip'
	#-*-coding: utf-8 -*-
	message_text = 'จัดการกับ Set, ShotDress ให้อัตโนมัติ (hide gpu ที่ไม่เห็นในกล้อง)'
	result = dialog.CustomMessageBox.show(title='Optimized Set', message=message_text, buttons=[action1, action2])

	if result.value == action1:
		roots = check_root()
		if roots:
			isolate_select(roots)
			clean.optimized_set()
			clean.hide_asm()
			isolate_not_select()

		clean.set_asset_list()
		return True

	if result.value == action2:
		clean.set_asset_list()
		return True

def doc(entity=None): 
	""" optimized set ก่อนการ cache """
	return 

def isolate_select(roots):
	panel_act = mc.paneLayout('viewPanes', q=True, pane1=True)
	mc.select(d=True)

	if roots:
		mc.select(roots)
		mc.isolateSelect( panel_act, state=True )
		mc.isolateSelect( panel_act, addSelected=True)


def isolate_not_select():
	panel_act = mc.paneLayout('viewPanes', q=True, pane1=True)
	mc.isolateSelect( panel_act, state=False )
	mc.select(d=True)


def check_root():
	roots = []
	sets = asm_lib.find_set_root() if mc.ls('Set_Grp') else []
	sets = [loc for loc in sets if not mc.referenceQuery(loc, inr=True)]
	roots.extend(sets)

	shotdress = asm_lib.find_set_root('ShotDress_Grp') if mc.ls('ShotDress_Grp') else []
	shotdress = [loc for loc in shotdress if not mc.referenceQuery(loc, inr=True)]
	roots.extend(shotdress)

	return roots
