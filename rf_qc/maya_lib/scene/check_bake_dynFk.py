#!/usr/bin/env python
# -- coding: utf-8 --
# import maya.cmds as mc
import pymel.core as pm
from rf_maya.nuTools.util.dynamicFk import core

def run(entity=None): 
	""" Check for unbaked FK Dynamic in the scene """

	networks = []
	for node in pm.ls(type='network'):
		if node.hasAttr('__NODE_TYPE') and node.hasAttr('name') and node.hasAttr('rootLocators'):
			nt = node.attr('__NODE_TYPE').get()
			# name = node.attr('name').get()
			# if nt == core.DynamicFkRig.__name__:
			networks.append(node.nodeName())
	if networks:
		return [False, 'Please open DynamicFk Tool and bake all chain under the following systems:\n%s' %'\n'.join(networks)]
	else:
		return [True, 'No unbaked DynamicFK.']

def skip(entity=None): 
	return False

def resolve(entity=None):
	from nuTools.util.dynamicFk import run as dfkRun
	status, result = run(entity)
	networks = result.split('\n')[1:]
	to_sels = []
	for node in networks:
		node = pm.PyNode(node)
		locs = node.rootLocators.inputs()
		to_sels.extend(locs)

	if to_sels:
		pm.select(to_sels, r=True)

	networks = result.split('\n')[1:]
	if not networks:
		return True
	else:
		dfkApp = dfkRun.mayaRun()

def doc(entity=None): 
	""" dynamic fk ที่ใช้ จะต้อง bake ก่อน ไม่อย่างนั้นจะไม่ผ่าน qc นี้ """ 
	return 
