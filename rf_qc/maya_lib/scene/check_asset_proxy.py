import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.context import context_info
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

def run(entity=None): 
	""" Check if the asset is not proxy """ 
	result = check_ref()
	return True if not result else False 

def resolve(entity=None): 
	""" Please remove all display layers """ 
	result = check_ref()
	assets = []
	geoGrp = entity.projectInfo.asset.geo_grp()
	if result: 
		for path in result: 
			namespace = mc.referenceQuery(path, ns=True)
			grp = '%s:%s' % (namespace[1:], geoGrp)

			if mc.objExists(grp): 
				longname = mc.ls(grp)
				grp = longname[0]
				assets.append(grp)

				if len(longname) > 1: 
					logger.error('Duplicated name %s' % grp)
			else: 
				logger.debug('asset not in pipeline %s' % path)

	mc.select(assets)
	return False 



def check_ref(): 
	invalid = []
	paths = mc.file(q=True, r=True)
	# hard coded 'pr'
	keyPr = '_pr'
	keyHero = '.hero'
	for path in paths: 
		name = os.path.basename(path)
		if keyPr in name or not keyHero in name:
			if not '_mtr' in name:
				invalid.append(path)

	return invalid