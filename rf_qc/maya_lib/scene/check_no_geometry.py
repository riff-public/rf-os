#!/usr/bin/env python
# -- coding: utf-8 --
import maya.cmds as mc
import pymel.core as pm
import maya.OpenMaya as om
from nuTools.pipeline import cleanup


def run(entity=None): 
    """ Check shape with null geometry (disconnected inMesh) """
    geoGrpName = entity.projectInfo.asset.geo_grp()
    allGeoGrps = mc.ls('*:%s' %(geoGrpName))
    no_geometries = []
    
    mSel = om.MSelectionList()
    for geoGrp in allGeoGrps:
        for tr in mc.listRelatives(geoGrp, ad=True, type='transform', f=True):
            shps = mc.listRelatives(tr, shapes=True, pa=True, ni=True, type='mesh')
            if not shps:
                continue
            for shp in shps:
                mSel.add(shp)

    selIt = om.MItSelectionList(mSel)
    while not selIt.isDone():
        mObj = om.MObject()
        selIt.getDependNode(mObj)
        try:
            meshFn = om.MFnMesh(mObj)
        except RuntimeError, e:
            mDag = om.MDagPath()
            selIt.getDagPath(mDag)
            no_geometries.append(mDag.partialPathName())
        selIt.next()

    if no_geometries:
        mc.select(no_geometries)
        return [False, 'Null geometry found:\n%s' %('\n'.join(no_geometries))]
    else:
        return [True, 'No null geometry found.']

def resolve(entity=None):

    status, result = run(entity)
    if not status and result:
        no_geometries = result.split('\n')[1:]
        refs = getFileRefFromObjects(objs=no_geometries)
        for ref in refs:
            edits = ref.getReferenceEdits(editCommand='disconnectAttr')
            if not edits:
                continue

            # disConPartitionEdits = [e for e in edits if e.startswith('disconnectAttr')]
            # if not disConPartitionEdits:
            #     continue

            for d in edits:
                splits = d.split(' ')
                src = splits[-2][1:-1]
                des = splits[-1][1:-1]

                srcNode = src.split('.')[0]
                desNode = des.split('.')[0]
                for ng in no_geometries:
                    if desNode.endswith(ng) and des.endswith('.inMesh'):
                        print 'Reconnecting: %s --> %s' %(src, des)
                        try:
                            srcAttr = pm.PyNode(src)
                            desAttr = pm.PyNode(des)
                            pm.connectAttr(src, des, f=True)
                        except:
                            print 'Cannot reconnect %s %s' %(src, des)
                        break
                        
    if result:
        return True

def doc(entity=None): 
    """ เช็คว่ามี polygon อันไหนเป็น polygon เปล่า และเจ๊งในซีนหรือไม่ """ 
    return 

def getFileRefFromObjects(objs=[]):
    fileRefs = set()
    for obj in objs:
        try:
            refPath = pm.referenceQuery(obj, f=True)
            fileRef = pm.FileReference(refPath)
            fileRefs.add(fileRef)
        except:
            pass

    return fileRefs

def skip(entity=None):
    return False


def get_path(entity=None):
    status, result = run(entity)
    select_geo = []
    if not status:
        no_geometries = result.split('\n')[1:]
        refs = getFileRefFromObjects(objs=no_geometries)
        geoGrp = entity.projectInfo.asset.geo_grp()
        for path in refs:
            namespace = mc.referenceQuery(path, ns=True)
            grp = '%s:%s' % (namespace[1:], geoGrp)

            if mc.objExists(grp): 
                longname = mc.ls(grp)
                grp = longname[0]
                select_geo.append(grp)
    return select_geo