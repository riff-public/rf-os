#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys
from datetime import datetime
import maya.cmds as mc 
import maya.mel as mm 

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils.widget import dialog 
from rf_utils.pipeline import lockdown
reload(lockdown)


allowLayers = ['defaultLayer']

class Message: 
    animMesg = 'Shot นี้ถูกส่งไปเรนเดอร์แล้ว ห้ามแก้ไขอนิเมชันเด็ดขาด'

def run(entity=None): 
    """ Check if this shot is allowed to be modified """ 
    allow = check(entity)
    
    if not allow: 
        write_log(entity)
        logger.warning('This shot is lockdown!!')
        message_box()

    return True if allow else False


def resolve(entity=None): 
    """ No cannot resolve """ 
    allow = check(entity)
    if not allow:
        mesg = 'หากจำเป็นต้องแก้จริงๆ ติอต่อ พี่น้อง/มิ้ม และแจ้ง Alt / Chaya เพื่อวางแผนการแก้งานของแผนกที่เกี่ยวข้อง'
        dialog.MessageBox.warning('Cannot Publish!', mesg)
    else:
        return True

def doc(entity=None): 
    """ ช็อตจะถูก lock หากแปนกต่อไปเริ่มไปแล้ว หากต้องการแก้ 
    ต้องแจ้งแผนกต่อไปหรือคนที่เกี่ยวข้องให้ปลดล็อค """
    return 


def check(entity=None): 
    """ Adjust duration to match Shotgun """ 
    result = lockdown.check_status(entity, entity.step, includeStatus=False)
    return True if not result else False


def message_box(): 
    dialog.MessageBox.warning('WARNING', Message.animMesg)
    

def write_log(entity): 
    date = str(datetime.now()).split(' ')[0]
    logName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]
    logPath = '%s/%s/_data/%s/%s/%s/%s.txt' % (os.environ['RFPROJECT'], entity.project, logName, date, entity.name, logName)
    data = '%s: %s was trying to open animation scene %s\n' % (str(datetime.now()), os.environ.get('RFUSER'), entity.name)

    if not os.path.exists(os.path.dirname(logPath)): 
        os.makedirs(os.path.dirname(logPath))
    writeFile(logPath, data)


def writeFile(file, data) :
    mode = 'a' if os.path.exists(file) else 'w'
    f = open(file, mode)
    f.write(data)
    f.close()
    return True

    # [
    # {'project': {'type': 'Project', 'id': 204, 'name': 'SevenChickMovie'}, 
    # 'sg_name': None, 'step': {'type': 'Step', 'id': 14, 'name': 'Model'}, 
    # 'sg_lockdown': False, 'entity.Asset.sg_asset_type': 'prop', 
    # 'entity': {'type': 'Asset', 'id': 6774, 'name': 'bonsaiCourtyardDisplay'}, 
    # 'content': 'modelUv', 'entity.Asset.sg_subtype': 'decorate', 'sg_resolution': 'md', 'entity.Asset.scenes': [], 
    # 'task_assignees': [{'type': 'Group', 'id': 40, 'name': 'modeler'}], 
    # 'sg_app': None, 'sg_status_list': 'wtg', 'type': 'Task', 'id': 115935, 'entity.Asset.code': 'bonsaiCourtyardDisplay'}, 
    # {'project': {'type': 'Project', 'id': 204, 'name': 'SevenChickMovie'}, 'sg_name': None, 'step': {'type': 'Step', 'id': 14, 'name': 'Model'}, 'sg_lockdown': False, 'entity.Asset.sg_asset_type': 'prop', 'entity': {'type': 'Asset', 'id': 6774, 'name': 'bonsaiCourtyardDisplay'}, 'content': 'model_pr', 'entity.Asset.sg_subtype': 'decorate', 'sg_resolution': 'pr', 'entity.Asset.scenes': [], 'task_assignees': [{'type': 'HumanUser', 'id': 196, 'name': 'Menga'}, {'type': 'Group', 'id': 40, 'name': 'modeler'}], 'sg_app': None, 'sg_status_list': 'apr', 'type': 'Task', 'id': 115937, 'entity.Asset.code': 'bonsaiCourtyardDisplay'}, {'project': {'type': 'Project', 'id': 204, 'name': 'SevenChickMovie'}, 'sg_name': None, 'step': {'type': 'Step', 'id': 14, 'name': 'Model'}, 'sg_lockdown': True, 'entity.Asset.sg_asset_type': 'prop', 'entity': {'type': 'Asset', 'id': 6774, 'name': 'bonsaiCourtyardDisplay'}, 'content': 'model', 'entity.Asset.sg_subtype': 'decorate', 'sg_resolution': 'md', 'entity.Asset.scenes': [], 'task_assignees': [{'type': 'Group', 'id': 40, 'name': 'modeler'}], 'sg_app': None, 'sg_status_list': 'rdy', 'type': 'Task', 'id': 115945, 'entity.Asset.code': 'bonsaiCourtyardDisplay'}, {'project': {'type': 'Project', 'id': 204, 'name': 'SevenChickMovie'}, 'sg_name': None, 'step': {'type': 'Step', 'id': 14, 'name': 'Model'}, 'sg_lockdown': False, 'entity.Asset.sg_asset_type': 'prop', 'entity': {'type': 'Asset', 'id': 6774, 'name': 'bonsaiCourtyardDisplay'}, 'content': 'modelBsh', 'entity.Asset.sg_subtype': 'decorate', 'sg_resolution': 'md', 'entity.Asset.scenes': [], 'task_assignees': [{'type': 'Group', 'id': 40, 'name': 'modeler'}], 'sg_app': None, 'sg_status_list': 'wtg', 'type': 'Task', 'id': 115951, 'entity.Asset.code': 'bonsaiCourtyardDisplay'}, {'project': {'type': 'Project', 'id': 204, 'name': 'SevenChickMovie'}, 'sg_name': None, 'step': {'type': 'Step', 'id': 14, 'name': 'Model'}, 'sg_lockdown': False, 'entity.Asset.sg_asset_type': 'prop', 'entity': {'type': 'Asset', 'id': 6774, 'name': 'bonsaiCourtyardDisplay'}, 'content': 'modelGr', 'entity.Asset.sg_subtype': 'decorate', 'sg_resolution': 'md', 'entity.Asset.scenes': [], 'task_assignees': [{'type': 'Group', 'id': 40, 'name': 'modeler'}], 'sg_app': None, 'sg_status_list': 'wtg', 'type': 'Task', 'id': 115960, 'entity.Asset.code': 'bonsaiCourtyardDisplay'}]
