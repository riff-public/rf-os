#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

ATTR_TO_CHECK = ['sx', 'sy', 'sz','scaleRig']
TOL = 1e-05

# Project Quantum
def run(entity=None):
    """ """
    get_ctrl = check_scale_ctrl()
    print get_ctrl
    return False if get_ctrl else True

def resolve(entity=None): 
    """ """
    get_ctrl = check_scale_ctrl()
    if get_ctrl: 
        mc.select(get_ctrl)
        print get_ctrl
        msg = 'check ctrl and keyframe zero scale \n\n%s' %('\n'.join(get_ctrl))
        confirm_action(msg)

    return False if get_ctrl else True

def check_scale_ctrl():
    allObjs = mc.ls(dag=True, type='transform')
    ctrlkey = set()

    # get start end
    start = mc.playbackOptions(q=True, min=True)
    end = mc.playbackOptions(q=True, max=True)

    # move to BaseAnimation Layer
    base_anim_layer = mc.animLayer(q=True, r=True)
    pre_selection = {}
    if base_anim_layer:
        # deselect all layers
        for lyr in mc.ls(type='animLayer'):
            curr_sel = mc.animLayer(lyr, q=True, sel=True)
            pre_selection[lyr] = int(curr_sel)

            # mc.animLayer(lyr, e=True, sel=sel)
            mm.eval('animLayerEditorOnSelect "%s" 0;' %lyr)
        mm.eval('animLayerEditorOnSelect "BaseAnimation" 1;')

    for obj in allObjs:
        # only reference node
        if not mc.referenceQuery(obj,inr=True) or mc.objectType(obj, isAType='constraint'):
            continue

        for attr in ATTR_TO_CHECK:
            obj_attr = '%s.%s' %(obj, attr)

            # if the attribute exists and NOT locked
            if not mc.objExists(obj_attr) or mc.getAttr(obj_attr, l=True):
                continue

            values = []
            # list connections
            base_keys = mc.keyframe(obj_attr, q=True)
            # if there's key
            if base_keys:
                anim_crvs = mc.listConnections(obj_attr, s=True, d=False, type='animCurve') or []
                # the attr is not using anim layer
                if anim_crvs:
                    values = mc.keyframe(anim_crvs[0], q=True, valueChange=True)
                    if [v for v in values if abs(v) <= TOL]:
                        ctrlkey.add(obj)
                else: 
                    anim_blends = mc.listConnections(obj_attr, s=True, d=False, type='animBlendNodeBase') or []
                    if anim_blends:
                        a_crvs = mc.listConnections('%s.inputA' %anim_blends[0], s=True, d=False, type='animCurve') or []
                        b_crvs = mc.listConnections('%s.inputB' %anim_blends[0], s=True, d=False, type='animCurve') or []
                        if a_crvs and b_crvs:
                            a_times = mc.keyframe(a_crvs[0], q=True) or []
                            b_times = mc.keyframe(b_crvs[0], q=True) or []
                            times = sorted(list(set(a_times + b_times)))
                            # values = [mc.getAttr('%s.output' %anim_blends[0], time=t) for t in times]
                            for t in times:
                                value = mc.getAttr('%s.output' %anim_blends[0], time=t)
                                if abs(value) <= TOL:
                                    ctrlkey.add(obj)
                                    break
                
            else: # no connections
                attrInputs = mc.listConnections(obj_attr, s=True, d=False)
                attrInputs = attrInputs if attrInputs else []
                attrParents = mc.attributeQuery(attr, node=obj, lp=True)
                if attrParents:  # let's include parent attr such as translate being parent of translateX
                    parentInputs = mc.listConnections('%s.%s' %(obj, attrParents[0]), s=True, d=False, c=True, p=True)
                    parentInputs = parentInputs if parentInputs else []
                    attrInputs += parentInputs

                # only if no input connection on both the attribute and the parent attribute, then get the value
                if not attrInputs:
                    value = mc.getAttr(obj_attr)
                    if abs(value) <= TOL:
                        ctrlkey.add(obj)

    # set anim layer selection back
    for lyr, sel in pre_selection.iteritems():
        mm.eval('animLayerEditorOnSelect "%s" %s;' %(lyr, sel))
                            
    ctrlkey = list(ctrlkey)
    return ctrlkey


def confirm_action(message): 
    result = mc.confirmDialog( title='Confirm', message=message, button='select')

def doc(entity=None): 
    """ เช็คว่ามี ctrl ที่มี scale 0 หรือไม่ """ 
    return 