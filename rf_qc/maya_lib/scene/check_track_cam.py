#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.context import context_info
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.publish.scene.utils import maya_hook as hook

def run(entity=None): 
	""" Check if camera is correct """ 
	emptyShots = check_shot()
	return True if not emptyShots else False 

def resolve(entity=None): 
	""" Please remove all display layers """ 
	emptyShots = check_shot()

	for shot in emptyShots: 
		namespace = '%s_cam' % shot
		objs = mc.ls('%s:*' % namespace)
		camera = [a for a in objs if mc.objectType(a, isType='camera')]

		if camera: 
			hook.set_current_cam(shot, camera[0])
	# return run(entity=entity) 

def doc(entity=None): 
	""" เช็คว่า camera sequencer และกล้องชื่อถูกต้องไหม 
	sequencer ต้องชื่อตาม shot เช่น scm_act1_q0010_s0010 sequencer จะชื่อ s0010 """ 
	return 

def check_shot(): 
	shots = hook.list_shot()
	emptyShots = []

	if shots: 
		for shot in shots: 
			cam = hook.find_cam(shot)
			emptyShots.append(shot) if not cam else None

	return emptyShots
