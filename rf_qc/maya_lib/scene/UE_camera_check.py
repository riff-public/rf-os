#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# Project Quantum
def run(entity=None):
    """ """
    cam_list = set_visCam(entity)
    print cam_list
    return True

def resolve(entity=None):
    """ set visibility camera """
    cam_list = set_visCam(entity)
    return True

def set_visCam(entity=None):
    """ set visibility camera """ 
    cam_s = ''
    camshot = mc.sequenceManager(listShots=True)
    for shot in camshot:
        for camera in mc.listConnections(shot + '.currentCamera') or ['']:
            cam_s = camera.split(':')[0]

    allCam = mc.ls(type='camera',l=True,dag=True)
    for cam in allCam:
        if mc.referenceQuery(cam,isNodeReferenced=True):
            namsplit = cam.split('|')[:-1:]
            for ns in namsplit:
                if ns and mc.keyframe(ns,query=True,attribute='visibility') == None and cam_s in ns:
                    print 'SET vis',ns
                    mc.setAttr('%s.visibility' %ns,1)

def doc(entity=None): 
    """ เช็คว่ากล้องปิดอยู่หรืเปล่า ถ้าปิดให้เปิดไว้ """ 
    return 