import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rf_utils.context import context_info
import logging 
logger = logging.getLogger(__name__)

logger.addHandler(logging.NullHandler())
context_info.logger.setLevel(logging.INFO)

_caches = dict()

def run(entity=None): 
    """ Check if rig and rig_uv status to see if rig is latest """ 
    paths = get_cache_paths(entity)
    statuses = []

    for path in paths: 
        taskInfo = check_sg_status(path)
        statuses.append(all([v[0] for k, v in taskInfo.iteritems()]))
    
    return all(statuses)


def resolve(entity=None): 
    """ print details """ 
    paths = get_cache_paths(entity)
    count = 0
    assets = []
    for path in paths: 
        asset = context_info.ContextPathInfo(path=path)
        taskInfo = check_sg_status(path)
        status = all([v[0] for k, v in taskInfo.iteritems()])

        if not status: 
            ns = mc.referenceQuery(path, ns=True)
            geo = '%s:%s' % (ns[1:], entity.projectInfo.asset.geo_grp())
            assets.append(geo)
            print '-------------'
            print '%s.%s' % ((count+1), asset.name)
            for k, v in taskInfo.iteritems(): 
                print 'Task: %s - %s' % (k, v[1])
            count+=1
    print '==========================='
    print '%s assets not ready' % count
    mc.select(assets)

    return False 


def check_sg_status(assetPath): 
    from rf_utils.sg import sg_process
    checkTasks = ['rig', 'rigUv']
    passStatuses = ['apr']
    fetch = True 
    fetchTask = True 

    asset = context_info.ContextPathInfo(path=assetPath)

    if asset.name in _caches.keys(): 
        sgEntity = _caches[asset.name].get('asset')
        fetch = True if not sgEntity else False 
                
    # fetch for asset sgEntity 
    if fetch: 
        asset.context.use_sg(sg_process.sg, asset.project, 'asset', entityName=asset.name)
        sgEntity = asset.context.sgEntity
        if not asset.name in _caches.keys(): 
            _caches[asset.name] = {'asset': sgEntity, 'tasks': []}
        else: 
            _caches[asset.name].update({'asset': sgEntity})

    if asset.name in _caches.keys(): 
        tasks = _caches[asset.name].get('tasks')
        fetchTask = True if not tasks else False 

    # fetch for tasks 
    if fetchTask: 
        tasks = sg_process.get_tasks_by_step(sgEntity, 'Rig')
        
        if not asset.name in _caches.keys(): 
            _caches[asset.name] = {'tasks': tasks}

        else: 
            _caches[asset.name].update({'tasks': tasks})

    info = dict()
    
    for task in tasks: 
        taskName = task.get('content')
        
        if taskName in checkTasks: 
            status = task.get('sg_status_list')
            value = True if status in passStatuses else False 
            info.update({taskName: [value, status]})


    return info 


def compare(entity): 
    cachePaths = get_cache_paths(entity)
    geoGrp = entity.projectInfo.asset.geo_grp()

    for path in cachePaths: 
        ns = mc.referenceQuery(path, ns=True)
        checkGrp = '%s:%s' % (ns[1:], geoGrp)
        abcPath = get_model_abc(path)
        print checkGrp, abcPath

        # check geo grp and abc model

def get_model_abc(assetPath): 
    asset = context_info.ContextPathInfo(path=assetPath)
    asset.context.update(step='model', process='main', res='md')
    outputDir = asset.path.scheme(key='publishHeroOutputPath').abs_path()
    outputName = asset.output_name(outputKey='cache', hero=True)
    abcPath = '%s/%s' % (outputDir, outputName)
    return abcPath

def get_cache_paths(entity): 
    geoGrp = entity.projectInfo.asset.geo_grp()
    # all refs 
    refs = list(set([a.split('{')[0] for a in mc.file(q=True, r=True)]))
    cacheAssets = []
    
    # only filter that has geo grp 
    for ref in refs: 
        ns = mc.referenceQuery(ref, ns=True)
        checkGrp = '%s:%s' % (ns[1:], geoGrp)
        cacheAssets.append(ref) if mc.objExists(checkGrp) else None
            
    return cacheAssets
