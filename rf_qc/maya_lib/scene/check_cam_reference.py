#!/usr/bin/env python
# -- coding: utf-8 --
import os
import quantum_namespace as qutm_namespace
import sys 
import maya.cmds as mc 
import maya.mel as mm 
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# Project Quantum
def run(entity=None):
    """ """
    cam_list = getname_camera(entity)
    return True if cam_list==[] else False

def resolve(entity=None):
    """ delelte camera """
    cam_list = getname_camera(entity)
    if cam_list:
        msg = 'remove camera : \n\n%s' %('\n'.join(cam_list))
        result = confirm_action(msg)
        if result:
            getpath_camera(cam_list)
            qutm_namespace.run(entity)
    else:
        print 'No camera'

    cam_list = get_namecamera(entity)
    return True if cam_list==[] else False

def getname_camera(entity=None):
    """ list camera """
    cam_s = ''
    objdel = []
    objlist = []
    camshot = mc.sequenceManager(listShots=True)
    for shot in camshot:
        for camera in mc.listConnections(shot + '.currentCamera') or ['']:
            cam_s = camera.split(':')[0]
            
    allCam = mc.ls(type='camera')
    for cam in allCam:
        if mc.referenceQuery(cam,isNodeReferenced=True):
            namespace = mc.referenceQuery(cam,namespace=True)
            if cam_s not in namespace and ("Shot" in namespace or 'CameraRig' in namespace):
                if namespace not in objlist:
                    objdel.append(cam)
                    objlist.append(namespace)
    print objdel
    return objdel

def getpath_camera(cam_list):
    """ delelte camera """
    pathFile = mc.referenceQuery(cam_list,filename=True)
    mc.file(pathFile,rr=True)

def confirm_action(message):
    result = mc.confirmDialog( title='Confirm', message=message, button=['Yes','No'])
    return True if result == 'Yes' else False

def doc(entity=None): 
    """ เช็คว่ากล้องตรงกับshotหรือเปล่า """ 
    return 