#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys 
import maya.cmds as mc 
import maya.mel as mm 
import pymel.core as pm
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
import sum_utils
reload(sum_utils)
from rf_maya.lib import sequencer_lib as seq

# Project Quantum
def run(entity=None):
    """ check preroll and postroll """
    result = check_roll(entity)
    return result

def resolve(entity=None): 
    """ fix preroll and postroll """
    preValue = entity.projectInfo.scene.pre_roll or 24
    postValue = entity.projectInfo.scene.post_roll or 24

    shots = mc.ls(type='shot')
    
    for shotName in shots: 
        mc.setAttr('%s.transitionInLength' % shotName, preValue)
        mc.setAttr('%s.transitionOutLength' % shotName, postValue)

        logger.info('Add "%s" preroll "%ss" frame and postroll "%s" frames' % (shotName, preValue, postValue))

    result = check_roll(entity)
    return result


def doc(entity=None): 
	""" ลบกล้อง reference ของเราออก """ 
	return 


def check_roll(entity): 
    preValue = entity.projectInfo.scene.pre_roll or 24
    postValue = entity.projectInfo.scene.post_roll or 24
    shotName = entity.name_code
    inTransition = mc.getAttr('%s.transitionInLength' % shotName)
    outTransition = mc.getAttr('%s.transitionOutLength' % shotName)

    if not preValue == inTransition or not postValue == outTransition: 
        return False 
    return True 

