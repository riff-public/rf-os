# v.0.0.1 polytag switcher
_title = 'Qc Tool'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'QcUI'

# This tool run in DCC package not run in standalone

#Import python modules
import sys
import os 
import json 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

import rf_config as config

# framework modules 
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils.context import context_info

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# import widget 
import qc_widget
reload(qc_widget)


class QcToolUI(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(QcToolUI, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        if config.isMaya: 
            self.ui = load.setup_ui_maya(uiFile, parent)
        else: 
            self.ui = load.setup_ui(uiFile, self)
        
        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.parent = parent 
        self.mainLayout = self.ui.verticalLayout
        self.add_options()
        self.setup_qc_widget()
        self.init_signals()


    def setup_qc_widget(self): 
        """ add widget and set signal """ 
        # get context form current scene 
        entity = context_info.ContextPathInfo()
        configFilter = self.useConfigCheckBox.isChecked()

        self.qcWidget = qc_widget.QcWidget(configFilter=configFilter, project='default', entityType=qc_widget.Config.asset, step='default', parent=self.parent)
        self.mainLayout.addWidget(self.qcWidget) 
        
        # set signal 
        self.qcWidget.check.connect(self.show_result)


    def add_options(self): 
        # add config checkBox 
        self.useConfigCheckBox = QtWidgets.QCheckBox()
        self.useConfigCheckBox.setText('Use Config')

        # add asset scene radioButton 
        self.entity_layout = QtWidgets.QHBoxLayout()
        self.asset_radioButton = QtWidgets.QRadioButton('asset')
        self.scene_radioButton = QtWidgets.QRadioButton('scene')
        
        self.entity_layout.addWidget(self.asset_radioButton)
        self.entity_layout.addWidget(self.scene_radioButton)
        self.entity_layout.setStretch(1, 2)
        self.asset_radioButton.setChecked(True)

        # parent to layout
        self.mainLayout.addLayout(self.entity_layout)
        self.mainLayout.addWidget(self.useConfigCheckBox)


    def init_signals(self): 
        self.useConfigCheckBox.stateChanged.connect(self.use_config)
        self.asset_radioButton.clicked.connect(self.use_config)
        self.scene_radioButton.clicked.connect(self.use_config)

    
    def use_config(self): 
        # use config 
        state = True if self.useConfigCheckBox.isChecked() else False
        self.qcWidget.configFilter = state

        # entityTyep 
        entityType = qc_widget.Config.asset if self.asset_radioButton.isChecked() else qc_widget.Config.scene if self.scene_radioButton.isChecked() else ''
        self.qcWidget.entityType = entityType

        self.qcWidget.list_modules()


    
    def show_result(self, status): 
        if status: 
            logger.info('All QCs passed')
        else: 
            logger.warning('Some check are failed. Please resolve problems. Try right click on failed item and select "Resolve"')


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = QcToolUI(maya_win.getMayaWindow())
        return myApp

    else: 
        logger.info('Run in QcToolUI\n')
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        myApp = QcToolUI()
        stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__': 
    show()
