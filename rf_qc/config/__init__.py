import os 
import sys 
import logging 
from rf_utils import file_utils
from collections import OrderedDict

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

default = 'default.yml'
modulePath = sys.modules[__name__].__file__
moduleDir  = os.path.dirname(modulePath).replace('\\', '/')


def read_config(project=''): 
	""" read config """ 
	configFile = '%s/%s.yml' % (moduleDir, project)
	if not os.path.exists(configFile): 
		configFile = '%s/%s' % (moduleDir, default)

	logger.debug('reading qc config from %s' % configFile)
	return file_utils.ymlLoader(configFile)


def get_qc_list(project, entityType, step, app, res='md', process='default'): 
	""" list qc from project and step (department) """ 
	config = read_config(project)
	# print 'qc update', entityType, step, app, res, process
	# print '------------------------------------------------'
	defaultProcess = config.get(app, 'default').get(entityType, OrderedDict()).get(step, OrderedDict()).get('default', OrderedDict())
	# print defaultProcess
	processData = config.get(app, 'default').get(entityType, OrderedDict()).get(step, OrderedDict()).get(process, defaultProcess)
	# print 'processData', processData

	return processData.get(res, 'default')
