_title = 'Qc Window'
_version = '1.0.0' 
_des = 'stable QC'
uiName = 'QCTool'

import os
import sys
import traceback
from collections import OrderedDict
from functools import partial

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# import framework modules
from rf_utils import file_utils
from rf_utils import icon
import rf_config
from rf_qc import config
from rf_utils.widget import dialog
# reload(dialog)

class Config:
    widgetName = 'RFQcWidget'
    mayaLib = '%s/maya_lib' % module_dir
    runMenu = 'Run'
    resolveMenu = 'Resolve'
    skipMenu = 'Force Skip'
    helpMenu = 'Help / Tutorial'
    asset = 'asset'
    scene = 'scene'
    videoKeyword = 'tutorial'

    iconMap = {runMenu: '%s/icons/play-6-16.png' % module_dir, 
                resolveMenu: '%s/icons/support-16.png' % module_dir, 
                helpMenu: '%s/icons/help-16.png' % module_dir, 
                skipMenu: '%s/icons/skip_icon.png' % module_dir}

class App:
    maya = 'maya'

class Color:
    red = 'color: rgb(200, 0, 0);'
    green = 'color: rgb(0, 240, 0);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgLightRed = 'background-color: rgb(140, 60, 60);'


class QcWidgetUI(QtWidgets.QWidget) :

    def __init__(self, app=App.maya, parent = None) :
        super(QcWidgetUI, self).__init__(parent)
        self.allLayout = QtWidgets.QVBoxLayout()
        self.setObjectName(Config.widgetName)
        # label
        self.label = QtWidgets.QLabel('QC List')
        # display listWidget
        self.qcDisplayListWidget = QtWidgets.QListWidget()
        # pushButton
        self.checkButton = QtWidgets.QPushButton()

        # add layout
        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.qcDisplayListWidget)
        self.allLayout.addWidget(self.checkButton)
        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(2, 2, 2, 2)

        # set display
        self.checkButton.setText('Check')
        self.checkButton.setMinimumSize(QtCore.QSize(0, 30))

        # console
        self.consoleLayout = QtWidgets.QVBoxLayout()
        self.consoleLabel = QtWidgets.QLabel('Information')
        self.consoleWidget = QtWidgets.QPlainTextEdit()
        self.consoleLayout.addWidget(self.consoleLabel)
        self.consoleLayout.addWidget(self.consoleWidget)
        self.allLayout.addLayout(self.consoleLayout)

        self.allLayout.setStretch(0, 0)
        self.allLayout.setStretch(1, 3)
        self.allLayout.setStretch(2, 0)
        self.allLayout.setStretch(3, 1)

        self.setLayout(self.allLayout)


class QcWidget(QcWidgetUI):
    check = QtCore.Signal(bool)
    itemSelectionChanged = QtCore.Signal(dict)

    def __init__(self, app=App.maya, configFilter=False, project='default', entityType='asset', step='default', res='md', parent = None):
        super(QcWidget, self).__init__(parent)
        self.app = app
        self.project = project
        self.entityType = entityType
        self.step = step
        self.res = res
        self.configFilter = configFilter

        # qc list
        self.qcList = config.get_qc_list(project=project, entityType=entityType, step=step, app=self.app)

        # set customContextMenu
        self.qcDisplayListWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.qcDisplayListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        # context choices
        self.contextChoices = [Config.runMenu, Config.resolveMenu, Config.skipMenu, Config.helpMenu]
        self.contextFuncMap = {Config.runMenu: 'run', Config.resolveMenu: 'resolve', Config.skipMenu: 'skip', Config.helpMenu: 'doc'}

        self.enable_console(False)

        # start listing modules
        self.list_modules()
        self.init_signals()

    def update(self, project, entityType, step, res='', entity=None, process=''):
        """ update later """
        self.project = project
        self.entityType = entityType
        self.step = step
        self.entity = entity
        self.res = res
        self.process = process
        self.qcList = config.get_qc_list(project=project, entityType=entityType, step=step, app=self.app, res=self.res, process=self.process)
        self.list_modules()


    def list_modules(self):
        """ display module list """
        modules = self.list_module_app() or []
        # modules = self.sorted_module(modules, self.qcList)
        modules = [a for a in self.qcList if a in modules]
        self.qcDisplayListWidget.clear()

        for name in modules:
            # if filter == True, use filter if not show all
            add = True if not self.configFilter else True if name in self.qcList else False

            if add:
                # icon
                func = self.get_func(name)
                if func: 
                    reload(func)
                    self.add_qc_item(name, func)

    def sorted_module(self, modules, sortedList): 
        """ sorted moudles from config order """
        sortedData = OrderedDict()
        if not sortedList: 
            return modules
        
        for name in sortedList: 
            if name in modules.keys(): 
                sortedData[name] = modules[name]

        for name, data in modules.iteritems(): 
            if not name in sortedData.keys(): 
                sortedData[name] = data
        return sortedData

    def enable_console(self, value=True): 
        self.consoleLabel.setVisible(value)
        self.consoleWidget.setVisible(value)

    def console(self, data):
        """ display console """
        display = []
        func = data.get('func')
        doc = func.run.__doc__ if func else ''
        traceback = data.get('traceback')
        message = data.get('message')
        consoleColor = ''

        display.append(doc)
        print '--', display, doc

        if traceback:
            display = []
            display.append(traceback)
            consoleColor = Color.red

        if message:
            display = []
            display.append(message)
            consoleColor = ''

        displayMessage = ('\n').join(display)

        self.consoleWidget.setPlainText(displayMessage)
        self.consoleWidget.setStyleSheet(consoleColor)


    def add_qc_item(self, name, func):
        """ add item to qc """
        # icon
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(icon.gear),QtGui.QIcon.Normal,QtGui.QIcon.Off)
        data = {'func': func, 'traceback': '', 'status': False, 'info': func.run.__doc__}

        item = QtWidgets.QListWidgetItem(self.qcDisplayListWidget)
        item.setData(QtCore.Qt.UserRole, data)
        item.setText(name)
        item.setIcon(iconWidget)
        item.setToolTip(func.run.__doc__)


    def list_module_app(self):
        """ list module from input app """
        if self.app == App.maya:
            if self.entityType == Config.asset:
                import maya_lib.asset as lib
                reload(lib)
                modules = lib.get_module()

                return modules

            if self.entityType == Config.scene:
                import maya_lib.scene as lib
                reload(lib)
                modules = lib.get_module()

                return modules

    def get_func(self, name): 
        if self.app == App.maya:
            if self.entityType == Config.asset:
                import maya_lib.asset as lib
                reload(lib)
                try: 
                    func = lib.get_func(name)
                    return func
                except ImportError: 
                    logger.warning('Some module cannot be imported "%s"' % name)


            if self.entityType == Config.scene:
                import maya_lib.scene as lib
                reload(lib)
                func = lib.get_func(name)

                return func


    def init_signals(self):
        """ set up signals """
        self.checkButton.clicked.connect(self.run_check)
        self.qcDisplayListWidget.customContextMenuRequested.connect(self.show_menu)
        self.qcDisplayListWidget.itemSelectionChanged.connect(self.emit_signal)


    def show_menu(self, pos):
        """ show menu """
        menu = QtWidgets.QMenu(self)
        selectedItems = self.qcDisplayListWidget.selectedItems()
        menu.triggered.connect(partial(self.menu_action, selectedItems))

        if selectedItems:
            titles = []
            for item in selectedItems:
                data = item.data(QtCore.Qt.UserRole)
                func = data.get('func')

                for title in self.contextChoices:
                    attr = self.contextFuncMap[title]
                    if hasattr(func, attr):
                        titles.append(title) if not title in titles else None

            for title in titles:
                menu.addSeparator() if title == Config.helpMenu else None 
                iconPath = Config.iconMap.get(title)
                menuItem = QtWidgets.QAction(self)
                menuItem.setText(title)
                if iconPath: 
                    iconWidget = QtGui.QIcon()
                    iconWidget.addPixmap(QtGui.QPixmap(iconPath))
                    menuItem.setIcon(iconWidget)
                menu.addAction(menuItem)

            menu.popup(self.qcDisplayListWidget.mapToGlobal(pos))
            # selMenuItem = menu.exec_(self.ui.entity_listWidget.mapToGlobal(pos))

    def menu_action(self, items, menu):
        """ run or resolve """
        for item in items:
            data = item.data(QtCore.Qt.UserRole)
            func = data.get('func')

            if menu.text() == Config.runMenu:
                self.run_func(item)
                

            if menu.text() == Config.resolveMenu:
                result = func.resolve(self.entity)
                if isinstance(result, (tuple, list)): 
                    result, message = result

                if result == True: 
                    iconPath = icon.ok
                elif result == False: 
                    iconPath = icon.no
                elif result == None: 
                    iconPath = icon.needFix
                else: 
                    iconPath = ''
                    
                status = True if result else False
                if not result:
                    logger.warning('Cannot resolve problem. Check information dialog below or contact Technical team.')
                self.update_status(item, iconPath, status)

            if menu.text() == Config.skipMenu:
                if hasattr(func, 'skip'):
                    result = func.skip(self.entity)
                    if isinstance(result, (tuple, list)): 
                        result, message = result
                    
                    iconPath = icon.ok if result else icon.no
                    status = True if result else False
                    self.update_status(item, iconPath, status)
                else:
                    logger.warning('Cannot skip this QC')

            if menu.text() == Config.helpMenu: 
                if hasattr(func, 'doc'): 
                    result = func.doc(self.entity)
                    buttons = ['OK']
                    buttons.append('View Video') if hasattr(func, Config.videoKeyword) else None
                    
                    if not result: 
                        messageBox = dialog.CustomMessageBox(title='Help', message=func.doc.__doc__, buttons=buttons, parent=self)
                        messageBox.clicked.connect(self.dialog_command)
                        messageBox.exec_()
                        # dialog.MessageBox.warning('Help', func.doc.__doc__)

        self.emit_signal()
        self.check_qc_pass()
        
    def emit_signal(self):
        """ emit signal """
        item = self.qcDisplayListWidget.currentItem()
        if item:
            data = item.data(QtCore.Qt.UserRole)
            # print data
            self.itemSelectionChanged.emit(data)
            self.console(data)

    def dialog_command(self, value): 
        """ execute dialog command """ 
        print value

    def run_check(self, items=[]):
        """ run all check in the list """
        items = [self.qcDisplayListWidget.item(a) for a in range(self.qcDisplayListWidget.count())] if not items else items
        allResult = []
        logger.info('Runing check ...')

        for item in items:
            result = self.run_func(item)
            allResult.append(result)

        checkResult = True if all(allResult) else False

        if checkResult:
            logger.info('Qc complete')

        # emit signal
        self.check_qc_pass()
        self.emit_signal()
        # self.check.emit(checkResult)

    def run_func(self, item):
        data = item.data(QtCore.Qt.UserRole)
        func = data.get('func')
        logger.info(func.__name__)
        message = ''

        try:
            # run
            result = func.run(self.entity)

            if isinstance(result, (tuple, list)): 
                result, message = result

            # set status
            iconPath = icon.ok if result else icon.needFix
            status = True if result else False

            # set data
            data['traceback'] = message
            item.setData(QtCore.Qt.UserRole, data)
            self.update_status(item, iconPath, status)
            item.setToolTip(func.resolve.__doc__) if not status else None
            

        except Exception as e:
            # store traceback to var
            error = traceback.format_exc()
            traceback.print_exc()
            logger.error(error)

            iconPath = icon.no
            self.update_status(item, iconPath, False)

            # set data
            data['traceback'] = str(error)
            item.setData(QtCore.Qt.UserRole, data)
            item.setToolTip(func.resolve.__doc__)
            result = False

        return result

    def get_item(self, name): 
        allItems = [self.qcDisplayListWidget.item(a) for a in range(self.qcDisplayListWidget.count())]

        for item in allItems: 
            if item.text() == name: 
                return item

    def check_qc_pass(self):
        allItems = [self.qcDisplayListWidget.item(a) for a in range(self.qcDisplayListWidget.count())]
        status = all([a.data(QtCore.Qt.UserRole).get('status') for a in allItems])
        self.check.emit(status)


    def update_status(self, item, iconPath, status):
        """ change status func """
        iconWidget = QtWidgets
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(iconPath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
        item.setIcon(iconWidget)
        data = item.data(QtCore.Qt.UserRole)
        data['status'] = status
        item.setData(QtCore.Qt.UserRole, data)


class MainWindow(QtWidgets.QMainWindow):
    """docstring for MainWindow"""
    def __init__(self, app=App.maya, configFilter=False, project='default', entityType='asset', step='default', parent=None):
        super(MainWindow, self).__init__(parent=parent)
        self.ui = QcWidget(app=app, configFilter=configFilter, project=project, entityType=entityType, step=step, parent=parent)
        self.setCentralWidget(self.ui)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setObjectName(uiName)
        self.resize(300, 400)
        

def show(app=App.maya, configFilter=False, project='default', entityType='asset', step='default'):
    import rf_config as config 
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = MainWindow(app=app, configFilter=configFilter, project=project, entityType=entityType, step=step, parent=maya_win.getMayaWindow())
        myApp.show()
        return myApp
