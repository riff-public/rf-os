import os 
import sys
from collections import OrderedDict
from rf_utils import file_utils
moduleDir = os.path.dirname(sys.modules[__name__].__file__)


def read(projectName): 
	defaultConfig = '{}/default_config.yml'.format(moduleDir)
	default_data = file_utils.ymlLoader(defaultConfig)

	projectConfig = '{}/{}_config.yml'.format(moduleDir, projectName)
	project_data = OrderedDict()
	result_data = OrderedDict()
	if os.path.exists(projectConfig):
		project_data = file_utils.ymlLoader(projectConfig)

	# if any project config is provided, merge it with default
	if project_data:
		result_data = default_data.copy()
		for dept, app_types in project_data.items():
			if dept not in default_data:
				result_data[dept] = app_types
			else:
				# check for override key value
				override = True if 'override' in app_types.keys() and app_types['override'] == True else False
				for typ, apps in app_types.items():
					if typ not in default_data[dept].keys():
						result_data[dept][typ] = apps
					else:
						# if override key is set to True on this department 
						if override:
							result_apps = project_data[dept][typ]
						else:
							result_apps = list(set(project_data[dept][typ] + default_data[dept][typ]))
						result_data[dept][typ] = result_apps
	else:
		# no project specific config is set, use default
		result_data = default_data

	return result_data 

