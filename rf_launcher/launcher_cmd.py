#Import python modules
import sys
import os
import subprocess
import argparse

import launcher_app
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import file_utils
projects = launcher_app.project_info.ProjectInfo().list_all(False)

def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-p', dest='project', type=str, help='Project name {}'.format(projects), default='Hanuman')
    parser.add_argument('-t', dest='appType', type=str, help='App type {}'.format(launcher_app.LocalConfig.appTypes), default='Software')
    parser.add_argument('-a', dest='appName', type=str, help='App name', default='')
    parser.add_argument('-d', dest='department', type=str, help='department', default='default')
    parser.add_argument('-c', dest='console', type=bool, help='Open console', default=True)
    return parser


def main(project, appType, appName, department, console, *args): 
    # run app
    if not project in projects: 
        print('project "{}" is invalid'.format(project))
        print('Available projecta are...\n{}'.format('\n'.join(projects)))
        return 

    if not appType in launcher_app.LocalConfig.appTypes: 
        print('appType {} is invalid'.format(appType))
        print('See {}'.format(launcher_app.LocalConfig.appTypes))
        return 

    apps = list_apps(appType)
    if appName in apps: 
        startupinfo = None
        if not console:
            startupinfo = subprocess.STARTUPINFO()
            startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        print('Starting Launcher from commandline...')
        print('project = {}\nappType = {}\nappName = {}\ndepartment = {}\nconsole = {}'.format(project, appType, appName, department, console))
        appPath = apps[appName]
        py3app = launcher_app.is_python3_app(appPath)  
        subprocess.Popen([launcher_app.config.Software.pythonPath, launcher_app.LocalConfig.preload, project, appPath, department, py3app], startupinfo=startupinfo)
    
    else: 
        print('"{}" is not in the list'.format(appName))
        print('Available apps in "{}" "{}" "{}" are...'.format(project, department, appType))
        for app in apps: 
            print('- {}'.format(app))

def list_apps(widgetType): 
    """ list app type """ 
    appDict = dict()
    appPath = '%s/%s' % (launcher_app.LocalConfig.appDir, widgetType)
    appList = file_utils.listFolder(appPath)

    for appDir in appList:
        appDirPath = '%s/%s' % (appPath, appDir)
        files = file_utils.listFile(appDirPath)
        pyFile = ['%s/%s' % (appDirPath, a) for a in files if os.path.splitext(a)[-1] == launcher_app.LocalConfig.pyext]
        pyFile = pyFile[0] if pyFile else ''
        displayName = os.path.splitext(os.path.basename(pyFile))[0]

        appDict[displayName] = pyFile

    return appDict


if __name__ == '__main__':
    print(':::::: Launcher cmd ::::::')
    logger.info(':::::: Launcher cmd ::::::')

    parser = setup_parser('Launcher cmd mode')
    params = parser.parse_args()
    
    main(project=params.project, 
        appType=params.appType, 
        appName=params.appName, 
        department=params.department, 
        console=params.console)
"""
python launcheer_cmd.py -p "Ultra" -a "MayaUltra" -t "Software"
"""

