# v.0.0.1 wip
# v.0.0.1 library wip
# v.0.2.0 rearrange categories
# v.0.4.0 config project / step and save setting
_title = 'RF Launcher'
_version = 'v.0.6.0'
_des = 'production'
uiName = 'RFLauncher'

#Import python modules
import os 
import sys
import logging
from functools import partial

# read global config
import env

# import config

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

class SplashScreen(QtWidgets.QWidget):
    """docstring for UI"""
    def __init__(self, parent=None):
        super(SplashScreen, self).__init__(parent=parent)
        self.setWindowFlags(QtCore.Qt.SplashScreen | QtCore.Qt.FramelessWindowHint)
        self.layout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Test')
        self.layout.addWidget(self.label)
        self.setLayout(self.layout)
        self.show()
        QtCore.QTimer.singleShot(2000, self.close)


class MainUI(QtWidgets.QMainWindow):
    """docstring for M"""
    def __init__(self):
        super(MainUI, self).__init__()
        self.widget = QtWidgets.QWidget()
        self.layout = QtWidgets.QVBoxLayout(self.widget)
        self.label = QtWidgets.QLabel('Main Window')
        self.layout.addWidget(self.label)
        self.setCentralWidget(self.widget)


        


if __name__ == '__main__': 
    app = QtWidgets.QApplication(sys.argv)
    
    # app = QtWidgets.QApplication.instance()
    # if not app:
    #     app = QtWidgets.QApplication(sys.argv)
    
    myApp2 = MainUI()
    myApp2.show()

    sys.exit(app.exec_())
