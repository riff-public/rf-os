# v.0.0.1 wip
# v.0.0.1 library wip
# v.0.2.0 rearrange categories
# v.0.4.0 config project / step and save setting
# v.0.6.0 clean up app, add Management department
# v.1.0.0 add Splash screen
# v.1.0.1 fix bug: app not launching when no SG user found
# v.1.0.2 fix bug: timelogged scheduler creation
#         update: new python3 syntaxes
# v.1.1.0 add support for Python3 app
# v.1.2.0 add support for running Launcher in Python3

_title = 'RF Launcher'
_version = 'v.1.2.0'
_des = ''
uiName = 'RFLauncher'

#Import python modules
import sys
import os
import subprocess
import json
import logging

is_python3 = True if sys.version_info[0] > 2 else False
# read global config
import env
config = env.config
moduleDir = env.moduleDir
from rf_utils import log_utils
from rf_utils import project_info

import project_config
RFSCRIPT_VAR = config.Env.scriptVar
RFSCRIPT = os.environ.get(RFSCRIPT_VAR)

RFSTUDIO = config.Studio.current


logFile = log_utils.name(uiName, user=config.Env.localuser)
logger = log_utils.init_logger(logFile)

from rf_utils.ui import load
from rf_utils import file_utils
from rf_utils.ui import stylesheet
from rf_utils import icon
from rf_utils.widget import user_widget
from rf_utils import user_info
from rf_app.timer import dcc_timer

# import config

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

logger.info('Running RFSCRIPT from {}'.format(os.environ.get(RFSCRIPT_VAR)))
logger.info('\n\n==============================================')

USER = user_info.User()

def get_path(path):
    return '{}/{}'.format(os.environ.get(RFSCRIPT_VAR), path)

class LocalConfig:
    localconfig = 'local_config.yml'
    data = file_utils.ymlLoader('{}/{}'.format(moduleDir, localconfig))
    path = get_path(data.get('launcherPath'))
    python = config.Software.pythonPath
    pyext = data.get('appExt')
    steps = data.get('steps')
    appTypes = data.get('appTypes')
    iconext = data.get('appIconExt')
    titleImg = data.get('titleImg')
    desExt = data.get('descriptionExt')
    pdfExt = data.get('pdfExt')
    defaultIcon = get_path(data.get('defaultIcon'))
    iconSize = data.get('defaultIconSize')
    appDir = get_path(data.get('appDir'))
    preload = '{0}/{1}'.format(moduleDir, data.get('preload'))
    pipelineTmp = '{}/rf_tmp/rf_desktop.json'.format(os.environ.get('TEMP'))

    mainPath = '{}/core'.format(os.environ['RFSCRIPT'])
    iconTime = '{}/icons/timeIcon.png'.format(mainPath)

class ValueConfig:
    size_splitter = [700, 2000, 0]
    width_grid = 12
    height_grid = 55
    width_title = 120
    height_title = 100
    padding = 10
    min_pos = 3
    max_pos = 14
    py3filename = '_use_python3'

def is_python3_app(appPath):
    app_dir = os.path.dirname(appPath)
    if ValueConfig.py3filename in os.listdir(app_dir):
        return '1'
    return '0'

class RFLauncher(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(RFLauncher, self).__init__(parent)

        # ui read
        uiFile = '{}/ui.ui'.format(moduleDir)
        self.ui = load.setup_ui(uiFile, QtWidgets.QWidget)
        self.setCentralWidget(self.ui)
        titleName = '{} {} {}  @{}'.format(_title, _version, _des, moduleDir)
        self.setWindowTitle(titleName)
        self.setWindowIcon(QtGui.QIcon('{}/icons/app_icon.png'.format(moduleDir)))
        self.resize(850, 700)

        self.set_logo()
        self.set_user()
        self.set_timer()
        self.set_time_logged()
        self.set_department()
        self.set_widgets()
        self.set_style_infomation()
        self.set_info()
        self.init_signals()

        # setup time logged scheduler
        if RFSTUDIO == 'Riff':
            self.setup_timelogged_scheduler()

    def setup_timelogged_scheduler(self):
        from rf_app import time_logged
        from rf_utils import admin
        print('Setting up Time Logged Scheduler...')

        # get scheduler bat paths
        time_logged_module_dir = os.path.dirname(time_logged.__file__)
        remove_scheduler = '{}\\remove_scheduler.bat'.format(time_logged_module_dir)
        create_scheduler = '{}\\scheduler.bat'.format(time_logged_module_dir)

        # remove old scehduler via admin access
        admin.run_cmd(cmd='CALL {}'.format(remove_scheduler))

        # create a new scheduler
        subprocess.call(create_scheduler)

    def closeEvent(self, event):
        # do stuff
        print('Launcher is closing ...')
        self.timer_widget.stop_timer()
        event.accept()

    def set_logo(self):
        self.ui.logo_label.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

    def set_user(self):
        self.userWidget = user_widget.UserComboBox()
        self.userWidget.comboBox.setMinimumSize(85,20)
        self.ui.user_layout.addWidget(self.userWidget)

    def set_timer(self): 
        self.timer_widget = dcc_timer.TimerWidget(dcc='launcher')
        self.ui.header_layout.insertWidget(2, self.timer_widget)
        # rearrange widget 
        self.timer_widget.layout.addWidget(self.timer_widget.time_label, 0, 0)
        self.timer_widget.layout.addWidget(self.timer_widget.current_time_label, 0, 1)
        self.timer_widget.layout.addWidget(self.timer_widget.elapse_label, 0, 2)
        self.timer_widget.layout.addWidget(self.timer_widget.elapse_time_label, 0, 3)

        self.timer_widget.status_label.setVisible(False)
        self.timer_widget.status_time_label.setVisible(False)
        self.timer_widget.start_stop_button.setVisible(False)
        self.timer_widget.setting_button.setVisible(False)

    def set_time_logged(self):
        self.timeButton = QtWidgets.QPushButton()
        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(LocalConfig.iconTime),QtGui.QIcon.Normal,QtGui.QIcon.Off)
        self.timeButton.setIcon(iconWidget)
        self.timeButton.setFixedSize(QtCore.QSize(48, 40))
        self.timeButton.setIconSize(QtCore.QSize(40, 40))
        self.timeButton.setStyleSheet("QPushButton {color: #F6F6F6; background: transparent;border-width: 1px;border-radius: 10px;}")
        self.ui.time_layout.addWidget(self.timeButton)

    def set_department(self):
        all_steps = LocalConfig.steps

        # keep a try block in case no SG user
        try:
            if USER.is_hr() or USER.is_admin() or USER.is_producer() or USER.is_supervisor():
                all_steps.append('Management')
        except AttributeError as e:
            pass

        self.ui.department_comboBox.addItems(all_steps)
        lastSelection = self.read_setting().get('department')
        index = LocalConfig.steps.index(lastSelection) if lastSelection in LocalConfig.steps else 0
        self.ui.department_comboBox.setCurrentIndex(index)

    def set_info(self):
        """ set display information on the tool """
        self.set_script_root()
        self.check_user()
        self.list_env()
        self.list_project()
        self.list_apps()
        self.set_root()

    def set_style_infomation(self):
        self.ui.text_description.setStyleSheet("QTextEdit{font-family:MS Shell Dlq 2; font-size:9pt;background-color: rgb(68, 68, 68);}"
                                                "QTextEdit::disabled { color: white;}")
        self.ui.help_button.setStyleSheet("QPushButton{border-radius:11px; background-color: rgb(100, 100, 100);}")
        self.ui.run_button.setStyleSheet("QPushButton{border-radius:5px;}")
        self.ui.splitter.setStyleSheet("QSplitter::handle:horizontal {background-color: rgb(40, 40, 40);}"
                                        "QSplitter::handle:hover {background-color: rgb(90, 90, 90);}"
                                        "QSplitter::handle:vertical {border: 0px dashed black;}")

        self.ui.splitter.setStretchFactor(0,3)
        self.ui.splitter.setStretchFactor(1,1)

    def set_widgets(self):
        widgetTypes = self.widget_types()
        self.appWidgets = dict()

        vsplitter = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        self.ui.app_verticalLayout.addWidget(vsplitter)

        for i,widgetType in enumerate(widgetTypes):
            # instance widget
            widget = QtWidgets.QListWidget()
            vsplitter.addWidget(widget)

            # set size splitter
            if i%2 == 0:
                vsplitter.setStretchFactor(i,1)
            else:
                vsplitter.setStretchFactor(i,3)
            
            #self.ui.app_verticalLayout.addWidget(widget)
            self.appWidgets.update({widgetType: widget})

            # set widget
            widget.setMovement(QtWidgets.QListView.Static)
            widget.setResizeMode(QtWidgets.QListView.Adjust)
            widget.setViewMode(QtWidgets.QListView.IconMode)

            value = self.ui.icon_slider.value()
            widget.setGridSize(QtCore.QSize(value+ValueConfig.width_grid, value+ValueConfig.height_grid))

            # signal
            widget.itemDoubleClicked.connect(self.run_app)
            widget.itemClicked.connect(self.set_infomation)

        for i, widgetType in enumerate(widgetTypes): 
            ratio = 1 if not widgetType == 'Tool' else 2
            self.ui.app_verticalLayout.setStretch(i, ratio)

        # set size splitter
        if len(widgetTypes) > 2:
            vsplitter.setSizes(ValueConfig.size_splitter)

    def set_infomation(self,index):
        widgetTypes = self.widget_types()
        app_text = index.text()
        description = ''
        for widgetType in widgetTypes:
            appListWidget = self.appWidgets.get(widgetType)

            self.clear_selection(appListWidget,app_text)
            if appListWidget.selectedItems():
                item = appListWidget.selectedItems()[0]
                titlePath = item.data(QtCore.Qt.UserRole)[1]
                desPath = item.data(QtCore.Qt.UserRole)[2]
                iconPath = item.data(QtCore.Qt.UserRole)[4]

                if not titlePath:
                    titlePath = iconPath
                self.ui.img_label.setPixmap(QtGui.QPixmap(titlePath).scaled(ValueConfig.width_title, ValueConfig.height_title, QtCore.Qt.KeepAspectRatio))

                if os.path.isfile(desPath):
                    description = file_utils.readFile(desPath)
                    if not is_python3:
                        description = self.encode(description)

        app_text = app_text.replace('\n',' ')
        self.ui.text_description.setText(description)
        self.ui.appName_label.setText(app_text)

    def init_signals(self):
        # self.ui.app_listWidget.itemDoubleClicked.connect(self.run_app)
        self.ui.env_comboBox.currentIndexChanged.connect(self.set_env)
        self.ui.project_comboBox.currentIndexChanged.connect(self.project_signal)
        self.ui.department_comboBox.currentIndexChanged.connect(self.step_signal)
        self.ui.active_checkBox.stateChanged.connect(self.list_project)
        self.timeButton.clicked.connect(self.open_time_logged_app)
        self.ui.icon_slider.valueChanged.connect(self.set_icon_size)
        self.ui.run_button.clicked.connect(self.select_run_app)
        self.ui.help_button.clicked.connect(self.run_help)

    def widget_types(self):
        """ all widget types """
        widgetTypes = LocalConfig.appTypes
        return widgetTypes

    def open_time_logged_app(self):
        """open time logged app """
        appPath = "{}/rf_launcher_apps/Tool/Time Logged/TimeLogged.py".format(LocalConfig.mainPath)
        project = str(self.ui.project_comboBox.currentText())
        department = str(self.ui.department_comboBox.currentText())
        console = self.ui.console_checkBox.isChecked()
        startupinfo = None
        if not console:
            startupinfo = subprocess.STARTUPINFO()
            startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW

        # check for app python version
        py3app = is_python3_app(appPath)  
        subprocess.Popen([config.Software.pythonPath, LocalConfig.preload, project, appPath, department, py3app], startupinfo=startupinfo)

    def set_icon_size(self): 
        value = self.ui.icon_slider.value()
        widgetTypes = self.widget_types()

        for widgetType in widgetTypes:
            appListWidget = self.appWidgets.get(widgetType)
            appListWidget.setIconSize(QtCore.QSize(value, value))

    def set_script_root(self):
        """ get script root """
        root = os.environ.get(RFSCRIPT_VAR)
        self.ui.scriptRoot_label.setText(root)

    def check_user(self):
        """ check user """
        # localUser, sgUser = check.run()
        return

        if localUser:
            self.ui.user_comboBox.addItem(localUser)

        else:
            result = QtWidgets.QInputDialog.getText(self, 'User name', 'Please set your local user:')

            if result[1]:
                user = result[0]
                os.environ[config.get('USER')] = user
                env.set_env(config.get('USER'), user)
            self.check_user()

        if sgUser:
            self.ui.sg_pushButton.setEnabled(False)
            self.ui.user_comboBox.setItemData(0, sgUser, QtCore.Qt.UserRole)


    def project_signal(self):
        """ callback for project comboBox """
        currentProject = str(self.ui.project_comboBox.currentText())
        self.set_root()
        self.list_apps()
        self.save_setting()

    def step_signal(self):
        self.list_apps()
        self.save_setting()

    def set_root(self):
        # set root
        projectInfo = self.ui.project_comboBox.itemData(self.ui.project_comboBox.currentIndex(), QtCore.Qt.UserRole)
        if hasattr(projectInfo, 'rootProject'): 
            self.ui.projectRoot_label.setText(projectInfo.rootProject)

    def list_env(self):
        """ list available env """
        data = LocalConfig.data['env']
        self.ui.env_comboBox.clear()
        currentEnv = os.environ.get(RFSCRIPT_VAR).replace('\\', '/')

        if not currentEnv in [data[a] for a in data.keys()]:
            data.update({'custom': currentEnv})

        setCurrent = 0
        i = 0

        for key, path in sorted(data.items()):
            self.ui.env_comboBox.addItem(key)
            self.ui.env_comboBox.setItemData(i, path, QtCore.Qt.UserRole)

            if currentEnv == path:
                setCurrent = i
            i+=1

        if setCurrent:
            self.ui.env_comboBox.setCurrentIndex(setCurrent)


    def list_project(self):
        """ list project from db """
        projects = project_info.ProjectInfo().list_all(asObject=True) # fetch from shotgun 
        active = self.ui.active_checkBox.isChecked()

        if active: 
            projects = [a for a in projects if a.active == 'Active']

        self.ui.project_comboBox.clear()
        lastSelection = self.read_setting().get('project')
        currentIndex = 0

        for i, project in enumerate(projects):
            # data = project.config_data()
            self.ui.project_comboBox.addItem(project.name())
            self.ui.project_comboBox.setItemData(i, project, QtCore.Qt.UserRole)

            if project.name() == lastSelection:
                currentIndex = i

        self.ui.project_comboBox.setCurrentIndex(currentIndex)


    def list_apps(self):
        """ list apps filter from project_config """
        # project config
        currentProject = str(self.ui.project_comboBox.currentText())
        projectConfig = self.ui.project_comboBox.itemData(self.ui.project_comboBox.currentIndex(), QtCore.Qt.UserRole)
        data = project_config.read(currentProject)

        # widget types
        widgetTypes = self.widget_types()
        step = str(self.ui.department_comboBox.currentText())

        for widgetType in widgetTypes:
            appPath = '{}/{}'.format(LocalConfig.appDir, widgetType)
            # list apps
            # read from config instead
            appList = data.get(step, data['default']).get(widgetType) or []
            # files = file_utils.listFile(LocalConfig.appDir)
            # pyFiles = [os.path.splitext(a)[0] for a in appDirs if os.path.splitext(a)[-1] == LocalConfig.pyext]

            # ui view
            appListWidget = self.appWidgets.get(widgetType)
            appListWidget.clear()

            for appDir in appList:
                appDirPath = '{}/{}'.format(appPath, appDir)
                if os.path.exists(appDirPath): 
                    files = file_utils.listFile(appDirPath)
                    iconPath = ['{}/{}'.format(appDirPath, a) for a in files if os.path.splitext(a)[-1] == LocalConfig.iconext and not os.path.splitext(a)[0] == LocalConfig.titleImg]
                    iconPath = iconPath[0] if iconPath else ''

                    titlePath = ['{}/{}'.format(appDirPath, a) for a in files if os.path.splitext(a)[0] == LocalConfig.titleImg]
                    titlePath = titlePath[0] if titlePath else ''

                    desPath = ['{}/{}'.format(appDirPath, a) for a in files if os.path.splitext(a)[-1] == LocalConfig.desExt]
                    desPath = desPath[0] if desPath else ''

                    pdfPath = ['{}/{}'.format(appDirPath, a) for a in files if os.path.splitext(a)[-1] == LocalConfig.pdfExt]
                    pdfPath = pdfPath[0] if pdfPath else ''

                    pyFile = ['{}/{}'.format(appDirPath, a) for a in files if os.path.splitext(a)[-1] == LocalConfig.pyext]
                    pyFile = pyFile[0] if pyFile else ''
                    toolName = os.path.splitext(os.path.basename(pyFile))[0]

                    displayName = self.word_Wrapping_Upper(toolName, ValueConfig.padding, ValueConfig.min_pos, ValueConfig.max_pos)

                    self.add_item(appListWidget, displayName, iconPath, titlePath, desPath, pdfPath, pyFile, toolName)


    def add_item(self, widget, display, iconPath, titlePath, desPath, pdfPath, pyFile, toolName):
        iconPath = LocalConfig.defaultIcon if not os.path.exists(iconPath) else iconPath
        item = QtWidgets.QListWidgetItem(widget)
        data = [pyFile, titlePath, desPath, pdfPath, iconPath, toolName]

        iconWidget = QtGui.QIcon()
        iconWidget.addPixmap(QtGui.QPixmap(iconPath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
        item.setIcon(iconWidget)
        item.setText(display)
        item.setData(QtCore.Qt.UserRole, data)
        widget.setIconSize(QtCore.QSize(LocalConfig.iconSize, LocalConfig.iconSize))

    def clear_selection(self, widget, text_select):
        if widget.selectedItems():
            item = widget.selectedItems()[0].text()
            if item != text_select:
                widget.clearSelection()

    def encode(self, text):
        text = text.decode(encoding='UTF-8', errors='strict')
        return text

    def userLogs(serlf, user, toolName, projectName, department):
        from datetime import datetime
        now = datetime.now()
        dt_string  = now.strftime("%d/%m/%Y %H:%M:%S")

        logExt = '.json'
        root = '%s/logs/RFLauncherLogs' % 'O:/Pipeline'
        logDir = '%s' %(root)
        fileName = '%s%s' %(user, logExt)
        logPath = '%s/%s' %(logDir, fileName)
        newData = {dt_string:{'dcc':toolName, 'project':projectName, 'department':department}}
        data = newData

        if not os.path.exists(logDir):
            os.makedirs(logDir)

        if os.path.exists(logPath):
            data = file_utils.json_loader(logPath)
            data.update(newData)

        file_utils.json_dumper(logPath, data)

    def run_help(self):
        widgetTypes = self.widget_types()
        for widgetType in widgetTypes:
            appListWidget = self.appWidgets.get(widgetType)
            if appListWidget.selectedItems():
                item = appListWidget.selectedItems()[0]
                data = item.data(QtCore.Qt.UserRole)[3]
                if data:
                    os.startfile(data) 

    def select_run_app(self):
        widgetTypes = self.widget_types()
        for widgetType in widgetTypes:
            appListWidget = self.appWidgets.get(widgetType)
            if appListWidget.selectedItems():
                item = appListWidget.selectedItems()[0]
                self.run_app(item)

    def run_app(self, item):
        project = str(self.ui.project_comboBox.currentText())
        department = str(self.ui.department_comboBox.currentText())

        appPath = item.data(QtCore.Qt.UserRole)[0]
        toolName = item.data(QtCore.Qt.UserRole)[5]
        console = self.ui.console_checkBox.isChecked()
        startupinfo = None

        if not console:
            startupinfo = subprocess.STARTUPINFO()
            startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW

        # check for app python version
        py3app = is_python3_app(appPath)  
        subprocess.Popen([config.Software.pythonPath, LocalConfig.preload, project, appPath, department, py3app], startupinfo=startupinfo)

        try:
            self.userLogs(USER.name(), toolName, project, department)
        except:
            pass

    def set_env(self):
        """ set environment for script root """
        path = self.ui.env_comboBox.itemData(self.ui.env_comboBox.currentIndex(), QtCore.Qt.UserRole)
        os.environ[RFSCRIPT_VAR] = path
        self.set_script_root()

    def save_setting(self):
        """ save ui selection """
        project = str(self.ui.project_comboBox.currentText())
        department = str(self.ui.department_comboBox.currentText())
        data = {'project': project, 'department': department}
        if not os.path.exists(os.path.dirname(LocalConfig.pipelineTmp)):
            os.makedirs(os.path.dirname(LocalConfig.pipelineTmp))

        return file_utils.json_dumper(LocalConfig.pipelineTmp, data)

    def read_setting(self):
        if not os.path.exists(LocalConfig.pipelineTmp):
            self.save_setting()
            return self.read_setting()

        return file_utils.json_loader(LocalConfig.pipelineTmp)

    def word_Wrapping_Upper(self, text, padding, min_pos, max_pos):
        if len(text) >= padding:
            pos = [i for i,e in enumerate(text+'A') if e.isupper()]
            listUpper = [p for p in pos if p >= min_pos]

            if not listUpper:
                listUpper = [padding]

            if len(text) > max_pos and len(text[listUpper[0]::]) > padding:
                baseName = text[0:listUpper[0]]
                midName = text[listUpper[0]:listUpper[-2]]
                lastName = text[listUpper[-2]::]
                text = "{}\n{}\n{}".format(baseName,midName,lastName)
            else:
                baseName = text[0:listUpper[0]]
                lastName = text[listUpper[0]::]
                text = "{}\n{}".format(baseName,lastName)

        return text

def show():
    """ call this function to run """
    logger.info('Run in standalone\n')
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    stylesheet.set_default(app)
    pixmap = QtGui.QPixmap('{}/icons/splash_image.png'.format(moduleDir).replace('\\', '/'))
    splash = QtWidgets.QSplashScreen(pixmap)
    splash.setWindowFlags(QtCore.Qt.FramelessWindowHint|QtCore.Qt.WindowStaysOnTopHint)
    splash.show()

    splash.showMessage('{} {} is loading, please wait..'.format(_title, _version), 
                    QtCore.Qt.AlignBottom, 
                    QtCore.Qt.white)
    app.processEvents()
    window = RFLauncher()

    splash.showMessage('Showing UI...', 
                    QtCore.Qt.AlignBottom, 
                    QtCore.Qt.white)
    app.processEvents()
    window.show()
    splash.finish(window)
    sys.exit(app.exec_())

if __name__ == '__main__':
    print('Loading Launcher ...')
    show()
