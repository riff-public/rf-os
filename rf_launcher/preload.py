# this module run before calling app
import sys
import os
import subprocess
import env
from rf_utils import project_info
from rf_utils import network_utils

config = env.config


def set_env(data):
    # set env
    for key, path in data.items():
        os.environ[key] = path

def riff_server(): 
    # detech server env 
    rf_server = "1" if network_utils.is_network_drive(config.Env.pipelineGlobal) else "0"
    return rf_server

def main():
    projectName = sys.argv[1]
    appPath = sys.argv[2]
    deptartment = sys.argv[3]
    try:  # for backward compatibility
        use_python3 = sys.argv[4]
    except IndexError:
        use_python3 = '0'

    # project config
    project = project_info.ProjectInfo(projectName)
    envData = project.env()
    set_env(envData)

    # setup executable and env
    custom_env = os.environ.copy()
    custom_env["rf_launcher"] = "1"
    custom_env["rf_server"] = riff_server()
    custom_env["active_project"] = projectName
    custom_env["active_department"] = deptartment
    if use_python3 == '1':
        py_exe = config.Software.python3Path
        custom_env['QT_PREFERRED_BINDING'] = ''  # need to remove QT_PREFERRED_BINDING for Python3
    else:
        py_exe = config.Software.pythonPath

    # run app
    print('Executable = {}\nApplication = {}'.format(py_exe, appPath))
    print('================')
    subprocess.Popen([py_exe, appPath], env=custom_env)

main()
