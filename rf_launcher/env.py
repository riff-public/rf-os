import sys
import os 

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
root = os.path.split(moduleDir)[0]
sys.path.append(root)
import rf_config as config 