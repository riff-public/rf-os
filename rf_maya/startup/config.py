import sys
import os
from rftool.utils import icon
from collections import OrderedDict

# maya env
RFPROJECT = 'RFPROJECT'
RFPUBL = 'RFPUBL'
RFVERSION = 'RFVERSION'
RFPROD = 'RFPROD'
RFSCRIPT = 'RFSCRIPT'

# server env
# rootWork = os.environ[RFPROJECT].replace('\\', '/')
# rootPubl = os.environ[RFPUBL].replace('\\', '/')
# rootProd = os.environ[RFPROD].replace('\\', '/')
scriptDrive = os.environ[RFSCRIPT].replace('\\', '/')

# env file
pipelineEnvFile = '%s/core/maya/Maya.env' % scriptDrive

# res
res = ['md', 'lo', 'hi', 'pr']
medRes = 'md'
lowRes = 'lo'
hiRes = 'hi'
proxyRes = 'pr'

# entity
asset = 'asset'
scene = 'scene'

# reference hero
ref = 'lib'
media = '_media'
texture = 'textures'

refExt = 'ma'
mayaExt = ['.ma', '.mb']
stillExt = 'png'
gpuExt = 'abc'
cacheExt = 'abc'

# audio
audioDir = '_audio'

# maya store var
projectVar = 'FMProject'
stepVar = 'FMStep'
modeVar = 'FMMode'
serverVar = 'FMLocalSg'
localUser = 'RFUser'

# work dir
workDir = {RFPROJECT: 'work', RFPUBL: 'publish', RFVERSION: 'version'}
# image dir
publishImgDir = 'publishImg'
publishMovDir = 'publishMov'
publishOutputDir = 'publishOutput'

# data dir
publishData = '_data'
dataExt = 'yml'

# rig grp
rigGrp = 'Rig_Grp'
geoGrp = 'Rig_Grp|Geo_Grp'
ctrlGrp = 'Place_Ctrl'
libName = {'gpu': 'gpu'}


# server step
steps = ['model', 'rig', 'uv', 'shading', 'fur', 'hair', 'cloth']

# sg pipeline step map
sgSteps = {'Model': 'model', 'Rig': 'rig', 'Shader': 'shading', 'Texture': 'uv', 'Design': 'design', 'Cloth': 'cloth',
		'FX': 'fx', 'Light': 'light', 'Comp': 'comp', 'Layout': 'layout', 'Anim': 'anim', 'Hair': 'hair', 'Fur': 'fur',
		'SetDress': 'setDress', 'FinalCam': 'finalCam', 'TechAnim': 'techAnim'}

# sg default task statuses
sgStatus = ['wtg', 'rdy', 'ip', 'apr', 'rev', 'fix']

sgTemplate = {'asset': {'default': {'code': 'th_standard_asset', 'type': 'TaskTemplate', 'id': 110},
						'th_standard_asset': {'code': 'th_standard_asset', 'type': 'TaskTemplate', 'id': 110},
						'th_stand_asset_fur': {'code': 'th_stand_asset_fur', 'type': 'TaskTemplate', 'id': 111},
						'th_simple_asset_md': {'code': 'th_simple_asset_md', 'type': 'TaskTemplate', 'id': 112},
						'th_standard_set': {'code': 'th_standard_set', 'type': 'TaskTemplate', 'id': 113}}
						}

sgSortTask = {asset: ['zbrush', 'paintover', 'model_pr', 'model_md', 'modelGr_md', 'modelUv_md', 'rig_pr', 'rig_md', 'rigRen_md', 'shade_md'],
				scene: ['previs', 'layout', 'anim', 'cloth', 'setDress', 'finalCam', 'techAnim', 'fx', 'light', 'render', 'comp']}

sgIconMap = {'wtg': icon.sgWtg, 'apr': icon.sgAprv, 'rdy': icon.sgRdy, 'fix': icon.sgFix, 'hld': icon.sgHold, 'ip': icon.sgIp, 'late': icon.sgLate, 'rev': icon.sgRevise}

# sg type
assetTypes = ['character', 'prop', 'setdress', 'vehicle', 'set']

# ref template section ============================
libFileReplaceKw = 'asset'

# assembly map section ============================
asmSuffix = 'ad'
assemblyMap = {'assetName': '$nodeName', 'locatorLabel': '$locatorLabel'}
representationMap = {'rsproxy_md': '$rsproxy_md',
					'ren_md': '$ren_md',
					'model_md': '$model_md',
					'model_pr': '$model_pr',
					'gpu_md': '$gpu_md',
					'gpu_pr': '$gpu_pr'
					}

shotStartFrame = 1001
