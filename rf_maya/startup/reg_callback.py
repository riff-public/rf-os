import maya.OpenMaya as om

CALLBACK_ID = dict()

def register():
    idle_callback = idle_check()
    CALLBACK_ID['idle_check'] = idle_callback


def idle_check():
    from rf_app.time_logged import interval_record as ir
    reload(ir)
    idle_callback = om.MEventMessage.addEventCallback("SelectionChanged", ir.set_last_active_time)
    return idle_callback


def remove_idle_check():
    om.MMessage.removeCallback(CALLBACK_ID['idle_check'])
