# pyside package
import sys
import os
import logging
import os

import maya.cmds as mc
import maya.mel as mm

from rf_utils import file_utils

# set log
logger = logging.getLogger(__name__)

def trace(message) :
    mm.eval('trace"%s";' % message)


def report_plugin(): 
    print os.environ['REDSHIFT_COREDATAPATH']

def sync_plugin():
    """ load default plugins for every startup """
    # maya version
    version = mc.about(v=True)

    dir_name = os.path.dirname(__file__)
    config_path = os.path.join(dir_name, 'plugin_config.yml')
    if not os.path.exists(config_path):
        trace('Plugin config not found: %s' %config_path)
        return 
    plugin_data = file_utils.ymlLoader(config_path)
    script_path = os.environ.get('RFSCRIPT')
    summary = str()

    logger.info('Loading plugin ...')
    loads, unloads = [], []
    for plugin in plugin_data.get(version, []):
        
        path = plugin_data[version][plugin]['plugin_path']
        if 'RFSCRIPT' in path:
            path = path.replace('RFSCRIPT', script_path)
        pluginName = os.path.basename(path)

        load = plugin_data[version][plugin]['load_state']
        if load == True:
            loads.append((plugin, pluginName))
        elif load == False:
            unloads.append((plugin, pluginName))

    pluginPref_path = os.path.join(os.environ['HOME'], 'maya', version, 'prefs', 'pluginPrefs.mel')
    lines = []

    if os.path.exists(pluginPref_path): 
        with open(pluginPref_path, 'r') as f:
            lines = f.readlines()

        new_lines = []
        # unloads
        for line in lines:
            # collect the comments
            if line.startswith('//'):
                new_lines.append(line)
                continue

            skip = False
            for name, nameExt in unloads:
                if '\\"%s\\"' %name in line:
                    skip = True
                    break
            if not skip:
                new_lines.append(line)

        # loads
        add_lines = []
        for name, nameExt in loads:
            for line in new_lines:
                if '\\"%s\\"' %name in line:
                    break
            else: # adding line
                print name ,nameExt
                nl = 'evalDeferred("autoLoadPlugin(\\"\\", \\"%s\\", \\"%s\\")");\n' %(name, nameExt)
                add_lines.append(nl)
        new_lines += add_lines
        with open(pluginPref_path, 'w') as f:
            f.writelines(new_lines)
            
    return summary
