import config


# folder template path
templatePath = '%s/template/dir_structure' % (config.scriptDrive)

RFPROJECT = '%s/RFPROJECT' % templatePath
RFPUBL = '%s/RFPUBL' % templatePath
RFPROD = '%s/RFPROD' % templatePath

workAssetSteps = ['model', 'uv', 'shading', 'rig', 'design', 'hair', 'cloth', 'lib', 'textures']
publAssetSteps = ['model', 'uv', 'shading', 'rig', 'textures', 'lib', 'design', 'hair', 'fur']
prodAssetSteps = ['model', 'uv', 'shading', 'rig', 'design', 'hair', 'fur']

workSceneSteps = ['layout', 'anim', 'fx', 'light', 'comp', 'techAnim', 'hairDyn', 'setDress', 'finalCam', 'cloth']
publSceneSteps = ['layout', 'anim', 'fx', 'light', 'comp', 'render']
prodSceneSteps = ['layout', 'anim', 'fx', 'light', 'comp', 'render']

asset = 'asset'
scene = 'scene'