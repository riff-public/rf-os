import os
import maya.cmds as mc
import maya.mel as mel

PARENT_FORM = "RangeSlider|MainPlaybackRangeLayout|formLayout9|formLayout14"
UI_NAME = 'evModeToggle_symbolCheckBox'
scriptRoot = os.environ['RFSCRIPT']
DG_ICON = '%s/core/turtle.png' %scriptRoot
PAR_ICON = '%s/core/rabbit.png' %scriptRoot


def show():
    if mc.symbolCheckBox(UI_NAME, q=True, ex=True): 
        mc.deleteUI(UI_NAME)

    mc.symbolCheckBox(UI_NAME, 
        offImage=DG_ICON,
        onImage=PAR_ICON,
        value=False,
        onc=lambda x:parallelMode(), 
        ofc=lambda x:dgMode(), 
        parent=PARENT_FORM)
    dgMode()

def dgMode():
    mel.eval('optionVar -iv evaluationMode 0')
    mc.evaluationManager(mode='off')

def parallelMode():
    mel.eval('optionVar -iv evaluationMode 3')
    mc.evaluationManager(mode='parallel')
    mel.eval('turnOffOpenCLEvaluatorActive()')
    mc.evaluationManager(manipulation=True)
