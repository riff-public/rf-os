# This module is called from userSetup.py to start a scriptJob when maya start

import os, sys
import logging
import getpass

import maya.cmds as mc
import maya.mel as mm


from rftool.control import clean
from rftool.control import clean_vaccine
from rftool.utils import log_utils
from rf_utils import screen_shot
from rf_app.save_plus import save_utils
from rf_utils.context.context_info import clear_env

user = '%s-%s' % (os.environ.get('RFUSER'), getpass.getuser()) or 'unknown'
logFile = log_utils.name('maya_clean_job', user=user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.INFO)


# dict['pipelineData']['scriptJob'] = {'jobName':id}

def start():
    scene_open()
    # check_key()
    scene_saved()
    turnOff_antiAlias()
    detect_wrong_project()
    clear_context_env()

def scene_open():
    jobid = mc.scriptJob(event=["SceneOpened", clean.scene_open], protected=True)
    jobid2 = mc.scriptJob(event=["SceneOpened", clean_vaccine.scene_open], protected=True)
    # os.environ['pipelineData'] = str({'scriptJob': {'scene_open': jobid}})
    update_dict('scene_open', jobid)
    update_dict('clean_vaccine', jobid2)

def scene_saved():
    jobid = mc.scriptJob(e=['SceneSaved', screen_shot.capture], protected=True)
    update_dict('scene_save', jobid)

def clear_context_env():
    jobid = mc.scriptJob(event=["NewSceneOpened", clear_env], protected=True)
    update_dict('clear_context_env', jobid)

def check_key():
    jobid = mc.scriptJob(timeChange=clean.check_key, protected=True)
    # os.environ['pipelineData'] = str({'scriptJob': {'check_key': jobid}})
    update_dict('check_key', jobid)

def turnOff_antiAlias():
    def turnOff():
        mc.setAttr("hardwareRenderingGlobals.multiSampleEnable", 0)
    jobid = mc.scriptJob(event=['SceneOpened', turnOff], protected=True)
    # os.environ['pipelineData'] = str({'scriptJob': {'turnOff_antiAlias': jobid}})
    update_dict('turnOff_antiAlias', jobid)

def update_dict(jobName, jobId):
    if 'pipelineData' not in os.environ:
        os.environ['pipelineData'] = str({})
        
    pipelineDataContent = eval(os.environ['pipelineData'])
    
    if 'scriptJob' not in pipelineDataContent:
        pipelineDataContent['scriptJob'] = {}
        os.environ['pipelineData'] = str(pipelineDataContent)
    
    curr_data = eval(os.environ['pipelineData'])
    curr_data['scriptJob'].update({jobName: str(jobId)})
    os.environ['pipelineData'] = str(curr_data)

# def scene_data_eva():
#     if mc.about(v=True) == '2016 Extension 2 SP2':
#         # print 'tests'
#         jobid = mc.scriptJob(event=["SceneSaved", save_utils.publish_data_scene_eva], protected=True)
#         # os.environ['pipelineData'] = str({'scriptJob': {'scene_open': jobid}})
#         update_dict('scene_save', jobid)

def detect_wrong_project():
    # only have 'active_project' env only if Maya is called from launcher
    if 'active_project' not in os.environ:
        return
    jobid = mc.scriptJob(event=["SceneOpened", clean.check_wrong_project], protected=True)
    update_dict('detect_wrong_project', jobid)