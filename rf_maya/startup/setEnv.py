import os, sys
import shutil
import getpass
import logging
import rf_config as config
from datetime import datetime
from collections import OrderedDict
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
logger.setLevel(logging.INFO)

try:
    import maya.cmds as mc
    import maya.mel as mm
    isMaya = True
except ImportError:
    isMaya = False

user = getpass.getuser()
envUser = os.environ.get('RFUSER', '')

joinString = ';'
pathSplit = '/'
versionMap = {'2012': '2012-x64', '2015': '2015-x64', '2016': '2016-x64', '2017': '2017', '2018': '2018'}

def trace(message) :
    if isMaya:
        mm.eval('trace"%s";' % message)

def set():
    # set project
    setProject()

    # default env
    setSGAPI()


def setProject():
    # project environment
    os.environ['RFPROJECT'] = 'P:'
    os.environ['RFPUBL'] = 'P:'
    os.environ['RFPROD'] = 'P:'
    os.environ['RFVERSION'] = 'P:'

    trace('# Environment set')
    trace('user is %s' % user)
    trace('$RFPROJECT set %s' % os.environ['RFPROJECT'])
    trace('$RFPUBL set %s' % os.environ['RFPUBL'])
    trace('$RFPROD set %s' % os.environ['RFPROD'])
    trace('$RFVERSION set %s' % os.environ['RFVERSION'])

    logger.info('Environment set')
    logger.info('User : %s' % user)
    logger.info('Env User : %s' % envUser)
    logger.info('RFPROJECT : %s' % os.environ['RFPROJECT'])


def setSGAPI():
    sys.path.append('%s/python/2.7/site-packages/python-api-master' % os.environ['RFSCRIPT'])
    logger.info('Add Shotgun API env')



def setQt():
    trace('\\n# Set Qt')
    scriptPath = os.environ['RFSCRIPT']
    QtPath = '%s/lib/Qt.py' % scriptPath
    if not QtPath in sys.path:
        sys.path.append(QtPath)
        trace('Append Qt path success')
        logger.info('Add Qt Path success %s' % QtPath)


def setPlugins():
    ''' set plugin path '''
    if isMaya:
        trace('\\n# Set plugins path')
        version = mc.about(v = True)
        scriptPath = os.environ['RFSCRIPT'].replace('\\', '/')
        versionDir = mayaVersion()
        versionPluginPath = '%s/core/rf_maya/plugins/%s' % (scriptPath, version)
        allPluginPath = '%s/core/rf_maya/plugins/all' % (scriptPath)
        modulePath = '%s/core/rf_maya/modules/%s' % (scriptPath, version)

        putEnv('MAYA_MODULE_PATH', [modulePath])
        putEnv('MAYA_PLUG_IN_PATH', [versionPluginPath, allPluginPath])


        trace('add %s' % versionPluginPath)
        trace('add %s' % allPluginPath)
        trace('Add plugin path success')
        logger.info('MAYA_PLUG_IN_PATH : %s;%s' % (versionPluginPath, allPluginPath))


def putEnv(env, values, mode='a'):
    """ mode a = append, w = write """
    if mode == 'w':
        strValue = joinString.join(values)

    if mode == 'a':
        curValue = mm.eval('getenv "%s";' % env)
        curValues = curValue.split(joinString)
        values = values + curValues
        strValue = joinString.join(values)

    mm.eval('putenv "%s" "%s";' % (env, strValue))


def mayaVersion() :
    version = mc.about(v = True)
    versionDir = str()

    for each in versionMap.keys() :
        if each in version :
            versionDir = versionMap[each]

    return versionDir


def copyPipelineEnv(option='merge'):
    userScriptDir = mc.internalVar(userScriptDir=True)
    envDir = ('/').join(userScriptDir.split('/')[0:-2])
    userEnvFile = '%s/Maya.env' % envDir

    pipelineEnvFile = config.Maya.env

    if mc.about(v=True) == '2015':
        pipelineEnvFile = config.Maya.env2015

    if mc.about(v=True) == '2017':
        pipelineEnvFile = config.Maya.env2017

    pipelineEnvFile = '%s/%s' % (os.environ['RFSCRIPT'], pipelineEnvFile)

    if os.path.exists(pipelineEnvFile):

        if option == 'merge':
            newDataEnv = mergeEnv(pipelineEnvFile, userEnvFile)
            userDataEnv = readEnv(userEnvFile)

            if not newDataEnv == userDataEnv:
                backup(userEnvFile)
                writeEnv(userEnvFile, newDataEnv)
                logger.info('merge success')
            else:
                logger.info('Nothing new to merge')

        if option == 'replace':
            userDataEnv = readEnv(userEnvFile)
            pipelineDataEnv = readEnv(pipelineEnvFile)

            if not userDataEnv == pipelineDataEnv:
                backup(userEnvFile)
                shutil.copy2(pipelineEnvFile, userEnvFile)
                logger.info('replace success')

            else:
                logger.info('Nothing new to copy')

    else:
        logger.warning('%s not exists' % pipelineEnvFile)


def backup(filepath):
    if os.path.exists(filepath):
        bkDir = '%s/_bk' % os.path.dirname(filepath)
        basename = os.path.basename(filepath)

        if not os.path.exists(bkDir):
            os.makedirs(bkDir)

        src = filepath
        dst = '%s/%s_%s%s' % (bkDir, os.path.splitext(basename)[0], datetime.now().strftime("%Y-%m-%d_%H-%M-%S"), os.path.splitext(basename)[-1])

        logger.debug('src %s' % src)
        logger.debug('dst %s' % dst)
        result = shutil.copy2(src, dst)
        logger.info('backup success %s' % dst)

        return dst



def readEnv(path):
    ''' read Maya.env and return dict '''
    f = open(path, 'r')
    data = f.read()
    f.close()

    lines = data.splitlines()

    envDict = OrderedDict()
    for line in lines:
        key = line.split('=')[0].strip()
        values = [a.strip() for a in line.split('=')[-1].strip().split(';') if a]
        envDict.update({key: values})

    return envDict

def writeEnv(path, data):
    ''' write Maya.env with supply dict '''
    logger.info('write env %s' % path)

    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))

    f = open(path, 'w')
    dataStr = str()
    strLines = []

    for k, vs in data.iteritems():
        value = (';').join(vs)
        strLines.append('%s = %s' % (k, value))

    dataStr = ('\n').join(strLines)

    f.write(dataStr)
    f.close()
    logger.info('success')

    return path


def mergeEnv(env1, env2):
    ''' merge env1 to env2 and write new output Maya.env '''
    logger.info('merge env %s->%s' % (env1, env2))

    a = readEnv(env1)
    b = readEnv(env2)
    c = b.copy()

    logger.debug('merge %s' % c)

    for k, vs in a.iteritems():
        if k in c.keys():
            for v in vs:
                if not v in c[k]:
                    c[k].append(v)
        else:
            c.update({k: vs})

    return c
