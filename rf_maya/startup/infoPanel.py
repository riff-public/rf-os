# Copyright 2017 Autodesk, Inc. All rights reserved.
#
# Use of this software is subject to the terms of the Autodesk license
# agreement provided at the time of installation or download, or which
# otherwise accompanies this software in either electronic or hard copy form.

# -*- coding: utf-8 -*-
import os 
import sys 
from collections import OrderedDict
from maya.app.general.mayaMixin import MayaQWidgetDockableMixin
from maya import OpenMayaUI as omui
import maya.cmds as mc 
from Qt import QtCore
from Qt import QtGui 
from Qt import QtWidgets
from rf_utils import icon
from rf_utils import project_info
from rf_app.timer import dcc_timer
reload(dcc_timer)

customMixinWindow = None
uiName = 'pipelinePanel'
workspaceControlName = '{}WorkspaceControl'.format(uiName)


class Color:
    red = 'color: rgb(255, 0, 0);'
    lightRed = 'color: rgb(255, 140, 140);'
    green = 'color: rgb(0, 240, 0);'
    bgGreen = 'background-color: rgb(0, 140, 0);'
    bgYellow = 'background-color: rgb(200, 120, 0);'
    bgRed = 'background-color: rgb(140, 0, 0);'
    bgLightRed = 'background-color: rgb(140, 60, 60);'
    title = 'color: rgb(140, 180, 200);'
    textKey = 'color: rgb(190, 190, 190);'
    textValue = 'color: rgb(180, 180, 180);'
    bgDark = 'background-color: rgb(60, 60, 60);'


class Style: 
    button = """color: rgb(40, 40, 40);
                background-color: rgb(240, 160, 100);
                border-style: outset;
                border-width: 1px;
                border-radius: 10px;
                border-color: beige;
                font: bold 14px;
                min-width: 10em;
                padding: 6px;
            """


class DockableWidget(MayaQWidgetDockableMixin, QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(DockableWidget, self).__init__(parent=parent)

        # center layout 
        self.layout = QtWidgets.QVBoxLayout()
        self.scrollArea = QtWidgets.QScrollArea()
        self.scrollWidget = QtWidgets.QWidget()
        self.scrollLayout = QtWidgets.QVBoxLayout(self.scrollWidget)
        self.scrollArea.setWidget(self.scrollWidget)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setFrameShape(QtWidgets.QFrame.VLine)

        # title 
        self.titleLayout = QtWidgets.QHBoxLayout()
        self.logo = QtWidgets.QLabel()
        self.logo.setPixmap(QtGui.QPixmap(icon.logo).scaled(64, 64, QtCore.Qt.KeepAspectRatio))

        # separator 
        self.line = QtWidgets.QFrame()
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Raised)

        # project 
        self.projectLabel = QtWidgets.QLabel('Project')
        font = QtGui.QFont()
        font.setPointSize(20)
        self.projectLabel.setFont(font)

        self.titleLayout.addWidget(self.logo)
        # self.titleLayout.addWidget(self.line)
        self.titleLayout.addWidget(self.projectLabel)

        self.titleLayout.setStretch(0, 0)
        self.titleLayout.setStretch(1, 1)
        # self.titleLayout.setStretch(2, 1)

        # info layout 
        self.infoLayout = QtWidgets.QVBoxLayout()

        # separator 
        self.line2 = QtWidgets.QFrame()
        self.line2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line2.setFrameShadow(QtWidgets.QFrame.Sunken)

        # widgets 
        self.widgetLayout = QtWidgets.QVBoxLayout()
        self.button1 = QtWidgets.QPushButton('Start Working')
        self.button1.setMinimumSize(QtCore.QSize(0, 40))
        # self.button1.setStyleSheet(Style.button)
        self.widgetLayout.addWidget(self.button1)

        # spacer 
        self.spacerItem = QtWidgets.QSpacerItem(20, 460, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        # add to main layout 
        self.layout.addLayout(self.titleLayout)
        self.layout.addWidget(self.line2)
        self.layout.addWidget(self.scrollArea)
        self.layout.addLayout(self.widgetLayout)
        self.scrollLayout.addLayout(self.infoLayout)
        self.scrollLayout.addItem(self.spacerItem)

        self.setLayout(self.layout)
        self.setWindowTitle('Pipeline Info')

        self.setup_ui()

    def setup_ui(self): 
        """ setup all information """   
        # detech project 
        self.set_project()

        if self.project: 
            self.proj = project_info.ProjectInfo(self.project)
            
            # display project info 
            self.set_timer()
            self.set_render_info()
            self.set_dcc()
            self.set_env()
            self.set_scene()
            self.set_widget()
        else: 
            self.projectLabel.setText('Unknown')
            self.set_description('Error', 'No Pipeline support. \nPlease Open Maya with Launcher', 'error')
            self.button1.setVisible(False)

    def set_project(self): 
        """ find project by environment """ 
        self.project = os.environ.get('active_project')
        self.projectLabel.setText(self.project)

    def set_timer(self): 
        # self.timer_widget = dcc_timer.TimerWidget(dcc='maya')
        # self.timer_widget.layout.setContentsMargins(0, 0, 0, 0)
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        # self.infoLayout.addWidget(self.timer_widget)
        self.infoLayout.addWidget(line)

    def set_render_info(self): 
        infoDict = self.proj.render_data()
        self.set_info('Basic', infoDict)

    def set_env(self): 
        envDict = OrderedDict()
        envDict['RFSCRIPT'] = os.environ.get('RFSCRIPT')
        envDict['RFUSER'] = os.environ.get('RFUSER')

        infoDict = self.proj.env()
        self.set_info('Drive', infoDict)
        self.set_info('Env', envDict)

    def set_scene(self): 
        infoDict = self.proj.scene.sceneConfig
        self.set_info('Shot', infoDict)

    def set_dcc(self): 
        infoDict = self.proj.dcc()
        self.set_info('Apps', infoDict)

    def set_widget(self): 
        from rf_app.task_manager import app
        self.button1.clicked.connect(app.show)

    def convert_to_display(self, raw_value):
        result = ''
        if isinstance(raw_value, (list, tuple, dict, OrderedDict)):
            results = []
            if isinstance(raw_value, (dict, OrderedDict)):
                for k, v in raw_value.items():
                    if isinstance(v, (list, tuple, dict, OrderedDict)):
                        v = self.convert_to_display(raw_value=v)
                    results.append('{}: {}'.format(k, v))
            elif isinstance(raw_value, (list, tuple)):  # convert list to str
                for v in raw_value:
                    if isinstance(v, (list, tuple, dict, OrderedDict)):
                        v = self.convert_to_display(raw_value=v)
                    results.append(v)

            results = [str(r) for r in results]
            if len(''.join(results)) > 30:
                result = '\n'.join(results) 
            else:
                result = ', '.join(results)
        else:
            result = str(raw_value)

        return result

    def set_info(self, titleText, infoDict): 
        subLayout = QtWidgets.QGridLayout()
        self.infoLayout.addLayout(subLayout)

        title = QtWidgets.QLabel(titleText)
        title.setStyleSheet(Color.title)
        font = QtGui.QFont()
        # font.setBold(True)
        font.setPointSize(12)
        title.setFont(font)

        subLayout.addWidget(title, 0, 0)

        row = 1 
        for k, v in infoDict.items(): 
            k = k if not len(k) > 20 else k[0:20]
            keyLabel = QtWidgets.QLabel(k)
            keyLabel.setStyleSheet(Color.textKey)

            # convert dict to str
            v = self.convert_to_display(v)

            valueLabel = QtWidgets.QLabel(v[0:30] + '...' if len(v) > 30 else v)
            valueLabel.setStyleSheet(Color.textValue)
            valueLabel.setToolTip(str(v))

            font = QtGui.QFont()
            font.setBold(True)
            keyLabel.setFont(font)
            
            subLayout.addWidget(keyLabel, row, 0)
            subLayout.addWidget(valueLabel, row, 1)

            row+=1 

        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.infoLayout.addWidget(line)


    def set_description(self, titleText, message, messageType='normal'): 
        titleColor = ''
        messageColor = ''
        if messageType == 'error': 
            titleColor = Color.red 
            messageColor = ''

        subLayout = QtWidgets.QVBoxLayout()
        self.infoLayout.addLayout(subLayout)

        title = QtWidgets.QLabel(titleText)
        title.setStyleSheet(titleColor)

        font = QtGui.QFont()
        # font.setBold(True)
        font.setPointSize(12)
        title.setFont(font)

        message = QtWidgets.QLabel(message)
        message.setStyleSheet(messageColor)

        subLayout.addWidget(title)
        subLayout.addWidget(message)

        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.infoLayout.addWidget(line)

 
def DockableWidgetUIScript(restore=False):
    global customMixinWindow

    ''' When the control is restoring, the workspace control has already been created and
      all that needs to be done is restoring its UI.
    '''
    if restore == True:
        # Grab the created workspace control with the following.
        restoredControl = omui.MQtUtil.getCurrentParent()

    if customMixinWindow is None:
        # Create a custom mixin widget for the first time
        customMixinWindow = DockableWidget()     
        customMixinWindow.setObjectName(uiName)
      
    if restore == True:
        # Add custom mixin widget to the workspace control
        mixinPtr = omui.MQtUtil.findControl(customMixinWindow.objectName())
        omui.MQtUtil.addWidgetToMayaLayout(long(mixinPtr), long(restoredControl))
    else:
        # Create a workspace control for the mixin widget by passing all the needed parameters. See workspaceControl command documentation for all available flags.
        customMixinWindow.show(dockable=True, area='right', floating=False, height=900, width=300, uiScript='DockableWidgetUIScript(restore=True)')
        try:
            mc.workspaceControl(workspaceControlName, e=True, ttc=["AttributeEditor", -1], wp="preferred", mw=200, initialWidth=200, l=customMixinWindow.windowTitle())
        except:
            pass
    return customMixinWindow



def show():
    if mc.workspaceControl(workspaceControlName, ex=True): 
        mc.deleteUI(workspaceControlName)
    ui = DockableWidgetUIScript()
    return ui


