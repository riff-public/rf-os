import maya.cmds as mc 
import maya.mel as mm 

def overridePref() :
    # Viewport 2.0 Setting
    mc.evalDeferred('mc.setAttr("hardwareRenderingGlobals.consolidateWorld", 0)')
    mc.evalDeferred('mc.setAttr("hardwareRenderingGlobals.transparencyAlgorithm", 0)')
    mc.evalDeferred('mc.setAttr("hardwareRenderingGlobals.enableTextureMaxRes", 1)')
    # limit texture memory
    mc.evalDeferred('mc.setAttr("hardwareRenderingGlobals.textureMaxResolution", 1024)')
    mc.evalDeferred('mc.setAttr("hardwareRenderingGlobals.transparencyAlgorithm", 0)')
    mc.evalDeferred('mc.setAttr("hardwareRenderingGlobals.enableTextureMaxRes", 1)')
    mc.evalDeferred('mc.setAttr("hardwareRenderingGlobals.textureMaxResolution", 1024)')
    # set weightedTangents on for Rig
    mc.evalDeferred('mc.keyTangent(e=True, g=True, weightedTangents=True)')


    # Override scene configurations
    # mm.eval('trace "Turning off scene panel config";')
    mm.eval('optionVar -iv "useScenePanelConfig" 0;')
    mm.eval('optionVar -iv "useSaveScenePanelConfig" 0;')
    mm.eval('optionVar -iv evaluationMode 1')
    mm.eval('optionVar -iv viewportRenderer 1')
    mm.eval('optionVar -iv "colorManagementColorPickerColorMgtEnabled" 0;')
    mm.eval('setAttr "hardwareRenderingGlobals.multiSampleEnable" 0;')

    # prevent Maya crashing when open scene with hyperShade window opened
    mm.eval('optionVar -iv "minorNodeTypesDisplay" 0;')