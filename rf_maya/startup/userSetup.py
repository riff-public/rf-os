# PT userSetup.py for Maya 2016
import sys, os 
import maya.cmds as mc 
import maya.mel as mm

import startup.env as env
reload(env)
import startup.set_default as default
reload(default)
import startup.pt_menu as menu
reload(menu)

# trace print 

user = mc.optionVar(q='PTuser')

def trace(message) : 
    mm.eval('trace"%s";' % message)
    # print message

# print header 
trace('# ----------------------------------')
trace('# Running startup on PT userSetup.py')
trace('# ----------------------------------')

# set env -> move to run in userSetup.mel
# trace('- Set env')
# env.set()

trace('- Start default plugin')
default.plugin()


trace('- Override scene configurations')
default.sceneConfig()

# set 
# Ob turn this plugin off (11-01-16) by Andreas requested.
if not user.lower() == 'andreas' :
    trace('- Start remap UNC path')
    mm.eval('ptUNCRemap();')

# start PT Menu -> move to run in userSetup.mel
# trace('- Start PT Menu')
# mc.evalDeferred('menu.run()')

# force new env
trace('- Force pipeline environment')
env.copyEnv()

# check user 
trace('- Check user')
# mm.eval('startupJob();')
