import maya.cmds as mc
import rf_maya.contextMenu.dag as dg
from datetime import datetime 
mods = None


# def add_menu(parent=None):
# 	# Menu setting menu area
# 	mods = dg.get_module_data()

# 	for name, func in mods.iteritems(): 
# 		if hasattr(func, 'show'): 
# 			mc.menuItem(divider = True)
# 			func.show()
	# mc.menuItem(label = 'Riff Pipeline', subMenu = True)
	# mc.menuItem(label = 'Refresh Menu', c = '')
	# mc.setParent( '..', menu=True )
	# mc.menuItem(divider = True)

def add_menu(parent=None):
	# Menu setting menu area
	global mods 
	if not mods: 
		mods = dg.get_module_data()
	menu_list = dg.get_project_config()
	mc.menuItem(divider = True)
	create_menu(menu_list)
	mc.menuItem(divider = True)


def create_menu(menu_list): 
	global mods
	for v in menu_list: 
		if isinstance(v, dict): 
			for kk, vv in v.items(): 
				mc.menuItem(label = kk, subMenu = True)
				create_menu(vv)
				mc.setParent( '..', menu=True )
				# mc.menuItem(divider = True)
		else: 
			if v in mods.keys(): 
				func = mods[v]
				if hasattr(func, 'show'): 
					func.show()
					mc.menuItem(divider = True)


def window(): 
	ui = 'testMenu'
	if mc.window(ui, exists=True): 
		mc.deleteUI(ui)
	mc.window(ui, menuBar=True, width=200)
	mc.menu(l='Test')
	mods = dg.get_module_data()

	for name, func in mods.iteritems(): 
		if hasattr(func, 'show'): 
			mc.menuItem(divider = True)
			func.show()
	mc.showWindow()

