uiName = 'sendToDaily'
_title = 'Send To Daily'
_version = 'v.0.0.1'
_description = 'wip'

import os
import sys
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc

from functools import partial
# from rf_maya.rftool.shade import shade_utils

is_valid = True
try:
    from rf_app.asm import asm_lib
    from rf_maya.rftool.utils import maya_utils
    from rf_maya.rftool.shade import shade_utils
    from rf_utils import file_utils
    from rf_utils.pipeline import set_matte_id
    # reload(shade_utils)
    # reload(set_matte_id)
    from rf_utils.context import context_info
    from rf_utils import publish_info
    from rf_utils import daily
    # reload(daily)
    from rf_app.publish.utils import design_publish_utils
    # reload(design_publish_utils)
    from rf_app.publish.asset import sg_hook
    from rf_utils.sg import sg_process
    from rf_utils import user_info
except:
    is_valid = False

def show(parent=None):
    # Menu setting menu area
    if not is_valid:
        return
    mc.menuItem(divider = True)
    send_to_daily_menu()
    mc.menuItem(divider = True)

def send_to_daily_menu():
    mc.menuItem(l= 'Send To Daily', c= send_to_daily)
    mc.setParent( '..', menu=True)


def send_to_daily(*args):
    file = mc.file(sn=True, q= True)
    if file:
        scene_context = context_info.ContextPathInfo()
        img_path = scene_context.path.output_hero_img().abs_path()
        img_= os.path.join(img_path, scene_context.work_name()).replace('\\', '/')
        img_path = os.path.splitext(img_)[0]
        panel, image = mc.renderWindowEditor('renderView', e=True, writeImage= img_, com=True)
        print image, 'test'
        if image:
            shotEntity = sg_process.get_shot_entity(scene_context.project,scene_context.name)
            taskEntity = sg_process.get_one_task(shotEntity, 'light')
            user =user_info.User()
            user_name = user.name()
            userEntity = sg_process.get_local_user(user_name)
            status = 'rev'

            print image, img_path
            publish(scene_context, shotEntity, taskEntity, userEntity, status,version='',publImgs=[image])



def publish(scene_context, shotEntity, taskEntity, userEntity, status, version='',publImgs=[]):
    # input ContextPathInfo
    projectEntity = shotEntity['project']
    shotName = shotEntity['code']
    outputExt = publImgs[0].split('.')[-1] if publImgs else 'png'
    option = 'default'

    # calculate version
    print version
    version = design_publish_utils.find_next_version(projectEntity, taskEntity, shotEntity) if not version else version
    # context
    scene_context.context.update(step='light', process=scene_context.process, app='png', version=version, res='scene')
    # register process to get publish version
    reg = publish_info.PreRegister(scene_context)
    reg.register()
    # self.print_out('Registered process')
    print publImgs
    # publish File
    # self.print_out('Publishing files ...')
    publishedImgs, publishedHeroImgs = design_publish_utils.publish_files(scene_context, publImgs)
    # self.print_out('Published files')
    # publishShotgun
    description = ''
    # self.print_out('Publishing / Uploading media to Shotgun ...')
    versionResult = sg_hook.publish_version(scene_context, taskEntity, status, publishedImgs, userEntity, description, uploadOption=option)
    # self.print_out('Published version')
    taskResult = sg_hook.update_task(scene_context, taskEntity, status)
    # self.print_out('Update task')
    # register shot
    # self.print_out('Registering Shot data ...')
    publishAsset = publish_info.RegisterAsset(scene_context)
    regData = publishAsset.register()
    # self.print_out('Complete')



