import maya.cmds as mc
import rf_maya.contextMenu.sequencer.nothing_sel as nothing_sel
import rf_maya.contextMenu.sequencer.shot_sel as shot_sel
reload(shot_sel)

def shot_sel_menu(shotname='', parent=None):
    # Menu setting menu area
    print 'shotname', shotname
    mods = shot_sel.get_module_data()

    mc.menuItem(l='Rf Pipeline', subMenu=True)
    for name, func in mods.iteritems(): 
        if hasattr(func, 'show'): 
            mc.menuItem(divider = True)
            func.show(shotname)
    mc.setParent( '..', menu=True )
    mc.menuItem(divider = True)
    # mc.menuItem(label = 'Riff Pipeline', subMenu = True)
    # mc.menuItem(label = 'Refresh Menu', c = '')
    # mc.setParent( '..', menu=True )
    # mc.menuItem(divider = True)

def nothing_sel_menu(parent=None): 
    # Menu setting menu area
    mods = nothing_sel.get_module_data()

    mc.menuItem(l='Rf Pipeline', subMenu=True)
    for name, func in mods.iteritems(): 
        if hasattr(func, 'show'): 
            mc.menuItem(divider = True)
            func.show()
    mc.setParent( '..', menu=True )


def window(): 
    ui = 'testMenu'
    if mc.window(ui, exists=True): 
        mc.deleteUI(ui)
    mc.window(ui, menuBar=True, width=200)
    mc.menu(l='Test')
    mods = dg.get_module_data()

    for name, func in mods.iteritems(): 
        if hasattr(func, 'show'): 
            mc.menuItem(divider = True)
            func.show()
    mc.showWindow()

