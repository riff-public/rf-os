uiName = 'switchMenu'
_title = 'Switch Menu'
_version = 'v.0.0.1'
_description = 'wip'

import os
import sys
import logging
from datetime import datetime
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

is_valid = True
try:
    import maya.cmds as mc
    from functools import partial
    # from rf_maya.rftool.shade import shade_utils
    # =======

    # from rf_utils.context import context_info
    # context_info.logger.propagate = False
    from rf_utils import project_info
except:
    is_valid = False

def show(parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    if show_menu(): 
        mc.menuItem(divider = True)
        menu()
        mc.menuItem(divider = True)

def menu():
    item = mc.menuItem(l='Mocap Library', c=partial(start_mocap_tool))
    if not menu_enable(): 
        mc.menuItem(item, e=True, en=False, l='Mocap not available')


def show_menu(): 
    """ condition to show menu """ 
    allowType = ['char']
    key = 'Geo_Grp'
    sels = mc.ls(sl=True)
    if sels: 
        selObject = sels[0]
        # ref only 
        if mc.referenceQuery(selObject, inr=True): 
            ns = mc.referenceQuery(selObject, ns=True)
            # char only 
            # find Geo_Grp 
            detectGrp = '%s:%s.assetType' % (ns[1:], key)
            if mc.objExists(detectGrp): 
                assetType = mc.getAttr(detectGrp) 
                if assetType in allowType: 
                    return True 
            else: 
                logger.warning('"%s" not found. Asset is not in Pipeline' % detectGrp)
    return False

def menu_enable(): 
    """ enable or disable menu on condition """ 
    return True 


def start_mocap_tool(*args): 
    from rf_app.mocap.library import app
    reload(app)
    app.show() 
    