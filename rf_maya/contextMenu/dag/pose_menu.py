# show default pose 
# hard code without config 

import os
import sys
import logging
from datetime import datetime
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

is_valid = True
try:
    import maya.cmds as mc
    from functools import partial
    from rf_utils.context import context_info
    # from rf_utils import project_info
    from rf_app.anim.animIO import core as animIO
    from rf_app.export.pose import pose_utils
except:
    is_valid = False

def show(parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    selection = get_selection()
    if selection: 
        path, namespace = selection 
        mc.menuItem(divider = True)
        menu(path, namespace)
        mc.menuItem(divider = True)


def menu(path, namespace):
    posePath = pose_utils.pose_dir(path)
    poses = pose_utils.list_pose(posePath)
    item = mc.menuItem(l='Pose', subMenu=True)
    if poses: 
        for path in poses: 
            displayName = os.path.basename(path).split('.')[0]
            mc.menuItem(l=displayName, c=partial(apply_pose, namespace, path))
    else: 
        mc.menuItem(l='No poses', en=False)
    mc.menuItem(divider=True)
    mc.menuItem(l='Reset zero pose', c=partial(reset_pose, namespace, path))
    mc.menuItem(divider=True)
    mc.menuItem(l='Setup Pose', c=partial(run_setup_pose, namespace, path))
    mc.setParent( '..', menu=True)


def apply_pose(namespace, path, *args): 
    print 'apply', namespace, path
    logger.info('Apply pose "%s" to %s:*' % (path, namespace))
    animIO.import_pose(namespace=namespace, path=path)


def run_setup_pose(namespace, path, *args): 
    from rf_app.export.pose import pose_exporter
    reload(pose_exporter)
    pose_exporter.show(namespace, path)


def reset_pose(namespace, path, *args): 
    asset = context_info.ContextPathInfo(path=path)
    suffixCtrl = asset.projectInfo.asset.name_suffix('ctrl') or 'Ctrl'
    skipCtrls = [(asset.projectInfo.asset.get('allMoverCtrl') or 'AllMover_Ctrl')]

    ctrls = mc.ls('%s:*_%s' % (namespace, suffixCtrl))
    attrs = {'tx': 0, 'ty': 0, 'tz': 0, 'rx': 0, 'ry': 0, 'rz': 0, 'sx': 1, 'sy': 1, 'sz': 1}

    for ctrl in ctrls: 
        if not ctrl.split(':')[-1] in skipCtrls: 
            for attr, v in attrs.iteritems(): 
                isLock = mc.getAttr('%s.%s' % (ctrl, attr), l=True)
                if not isLock: 
                    mc.setAttr('%s.%s' % (ctrl, attr), v)


def get_selection(): 
    sels = mc.ls(sl=True)
    if sels: 
        sel = sels[0]
        if mc.referenceQuery(sel, inr=True): 
            path = mc.referenceQuery(sel, f=True)
            namespace = mc.referenceQuery(sel, ns=True)[1:]
            return (path, namespace)
