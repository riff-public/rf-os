uiName = 'asmMenu'
_title = 'Asm Menu'
_version = 'v.0.0.1'
_description = 'wip'

import os
import sys
from datetime import datetime
from functools import partial
from collections import OrderedDict
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import maya.cmds as mc
is_valid = True
try:
    from rf_app.asm import asm_lib
    from rf_utils import register_entity
    from rf_utils import file_utils
    # from rf_utils.context import context_info
    # context_info.logger.propagate = False
    from rf_maya.rftool.utils import pipeline_utils
except:
    is_valid = False

def show(parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    mc.menuItem(divider = True)
    root_menu()
    mc.menuItem(divider = True)

def get_selection():
    root = '|Set_Grp'
    sels = mc.ls(sl=True, l=True)
    asms = [a for a in sels if asm_lib.MayaRepresentation(a).is_asm()]
    nonAsms = [a for a in sels if not asm_lib.MayaRepresentation(a).is_asm()]
    parentAsms = list(set([asm_lib.find_asm_root(a) for a in nonAsms]))
    mesgAsms = list(set([asm_lib.find_asm_message(a) for a in nonAsms]))
    asms = asms + parentAsms + mesgAsms
    asms = [a for a in asms if a]
    isRoot = True if root in sels else False

    return sels, list(set(asms)), isRoot

def root_menu():
    sels, asms, isRoot = get_selection()
    if asms:
        asms = [a for a in asms if asm_lib.MayaRepresentation(a).is_asm()]
        if asms:
            refresh_menu()
            remove_set_menu()
            mc.menuItem(divider = True)
            build_menu()
            switch_menu()
            utils_menu()
            mc.menuItem(divider = True)
            show_hide_menu()
            reset_menu()
            remove_child_menu()
            mc.menuItem(divider = True)
            remove_rep_menu()

    if isRoot:
        mc.menuItem(divider = True)
        utils_menu()
        mc.menuItem(divider = True)

def refresh_menu():
    mc.menuItem(label = 'Refresh Set_Grp', c=partial(update, 'all'))
    mc.menuItem(label = 'Refresh Selected Set', c=partial(update, 'selected'))

def remove_set_menu():
    setGrp = 'Set_Grp'
    if mc.objExists(setGrp):
        roots = mc.listRelatives(setGrp, c=True)
        if roots:
            show = any(asm_lib.MayaRepresentation(a).is_root() for a in roots)
            mc.menuItem(label = 'Remove Set', c=partial(remove)) if show else None


def build_menu():
    mc.menuItem(label = 'Build', subMenu = True)
    mc.menuItem(label = 'Gpu proxy', c=partial(build, res='pr'))
    mc.menuItem(label = 'Gpu md', c=partial(build, res='md'))
    mc.setParent( '..', menu=True )

def remove_child_menu():
    mc.menuItem(label = 'Remove', c=partial(remove_child))

def reset_menu():
    mc.menuItem(label = 'Reset Position', c=partial(reset_pos))

def switch_menu():
    mc.menuItem(label = 'Switch', subMenu = True)
    mc.menuItem(label = 'Gpu', c=partial(switch_gpu, 'md'))
    mc.menuItem(label = 'Cache', c=partial(switch_cache, 'md'))
    mc.menuItem(label = 'Rig', c=partial(switch_rig, 'md'))
    mc.menuItem(label = 'RsProxy', c=partial(switch_rsproxy))
    mc.menuItem(divider = True)
    mc.menuItem(label = 'Gpu proxy', c=partial(switch_gpu, 'pr'))
    mc.menuItem(label = 'Rig proxy', c=partial(switch_rig, 'pr'))
    mc.menuItem(label = 'Cache proxy', c=partial(switch_cache, 'pr'))
    mc.menuItem(divider = True)
    mc.menuItem(label = 'Variant...', c=partial(launch_asset_variant))
    mc.menuItem(divider = True)
    custom_switch_menu()

    mc.setParent( '..', menu=True )

def launch_asset_variant(*args):
    from rf_app.asset_variant import app as asset_variant_app
    reload(asset_variant_app)
    asset_variant_app.show()

def custom_switch_menu(): 
    if len(mc.ls(sl=True)) == 1: 
        mc.menuItem(label = 'as Alembic', subMenu = True)
        list_abc_menu()
        mc.setParent( '..', menu=True )
        mc.menuItem(label = 'as Gpu', subMenu = True)
        list_gpu_menu()
        mc.setParent( '..', menu=True )
        mc.menuItem(label = 'as rsProxy', subMenu = True)
        list_rsproxy_menu()
        mc.setParent( '..', menu=True )

def utils_menu():
    mc.menuItem(label = 'Set Utils', subMenu = True)
    mc.menuItem(label='Lock all locators', c=partial(lock_asm_pos, True))
    mc.menuItem(label='Unlock all locators', c=partial(lock_asm_pos, False))
    mc.menuItem(label='Lock all childs', c=partial(lock_child_pos, True))
    mc.menuItem(label='Unlock all childs', c=partial(lock_child_pos, False))
    mc.setParent( '..', menu=True )

def show_hide_menu():
    mc.menuItem(label = 'Show Asm', c=partial(show_asm))
    mc.menuItem(label = 'Hide Asm', c=partial(hide_asm))

def remove_rep_menu():
    mc.menuItem(label = 'Remove Locator', c=partial(remove_asm))

def update(mode, *args):
    if mode == 'all':
        asm_lib.UiCmd().refresh_scene()
        # update(build=False)
    if mode == 'selected':
        asm_lib.UiCmd().update_selected()

def remove(*args):
    setGrp = 'Set_Grp'
    roots = mc.listRelatives(setGrp, c=True)
    for root in roots:
        if asm_lib.MayaRepresentation(root).is_root():
            asm_lib.Asm(root).remove_root()

def build(self, res, *args):
    """ build below """
    asm_lib.UiCmd().build(res=res)

def remove_child(self, *args):
    """ remove below """
    asm_lib.UiCmd().remove()

def remove_asm(self, *args):
    asm_lib.UiCmd().remove_rep()

def switch_rig(res='md', *args):
    asm_lib.UiCmd().switch_rig(res)

def switch_gpu(res='md', *args):
    asm_lib.UiCmd().switch_gpu(res)

def switch_cache(res='md', *args):
    asm_lib.UiCmd().switch_cache(res)

def switch_rsproxy(*args):
    asm_lib.UiCmd().switch_rsproxy()

def show_asm(*args):
    asm_lib.UiCmd().show_asm()

def hide_asm(*args):
    asm_lib.UiCmd().hide_asm()

def reset_pos(*args):
    asm_lib.UiCmd().reset_override()

def lock_asm_pos(state, *args):
    sels = mc.ls(sl=True)
    for sel in sels:
        asm = asm_lib.Asm(sel)
        asm.lock_pos(state, asm=True)

def lock_child_pos(state, *args):
    sels = mc.ls(sl=True)
    for sel in sels:
        asm = asm_lib.Asm(sel)
        asm.lock_pos(state, child=True)

def list_abc_menu(*args): 
    cmd = asm_lib.UiCmd()
    sels, asms = cmd.list_selection()

    files = list_hero_files(asms[0]) or []
    # create menu
    mc.radioMenuItemCollection()

    for file in files:
        state = False
        display = os.path.basename(file)
        mc.menuItem(l=display, rb=state, c=partial(custom_switch, file, 'cache'))

def list_gpu_menu(*args): 
    cmd = asm_lib.UiCmd()
    sels, asms = cmd.list_selection()

    files = list_hero_files(asms[0]) or []
    # create menu
    mc.radioMenuItemCollection()

    for file in files:
        state = False
        display = os.path.basename(file)
        mc.menuItem(l=display, rb=state, c=partial(custom_switch, file, 'gpu'))

def list_rsproxy_menu(*args): 
    cmd = asm_lib.UiCmd()
    sels, asms = cmd.list_selection()

    files = list_rsproxy_files(asms[0]) or []
    # create menu
    mc.radioMenuItemCollection()

    for file in files:
        state = False
        display = os.path.basename(file)
        mc.menuItem(l=display, rb=state, c=partial(custom_switch, file, 'rsproxy'))


def custom_switch(switchFile, buildType, *args): 
    cmd = asm_lib.UiCmd()
    if buildType == 'cache': 
        cmd.switch_cache(overrideFile=switchFile)
    if buildType == 'gpu': 
        cmd.switch_gpu(overrideFile=switchFile)
    if buildType == 'rsproxy': 
        cmd.switch_rsproxy(overrideFile=switchFile)

def list_hero_files(asm):
    caches = []

    loc = asm_lib.MayaRepresentation(asm)
    asmPath = loc.asset_description()
    asmPath = get_asm_path(asmPath)

    if os.path.exists(asmPath): 
        reg = register_entity.RegisterInfo(asmPath)
        
        if asmPath: 
            for res in ['md', 'pr', 'lo']: 
                cacheFiles = reg.list.caches(res=res, hero=True)
                cacheFiles += reg.list.gpus(res=res, hero=True)
                for file in cacheFiles: 
                    if not file in caches: 
                        caches.append(file)
    else: 
        return ['No file']

    return caches

def list_rsproxy_files(asm): 
    files = []
    loc = asm_lib.MayaRepresentation(asm)
    asmPath = loc.asset_description()
    asmPath = get_asm_path(asmPath)
    if os.path.exists(asmPath): 
        reg = register_entity.RegisterInfo(asmPath)

        if asmPath: 
            for res in ['md', 'pr']: 
                rsproxys = reg.list.files(key='rsproxy', res=res, hero=True) 
                for file in rsproxys: 
                    if not file in files: 
                        files.append(file)
    else: 
        return ['No file']

    return files
        

def get_asm_path(asmPath): 
    # if temp file exists, use temp_ad.yml 
    tmpFile = register_entity.Config.tempFile
    tmpPath = '%s/%s' % (os.path.dirname(asmPath), tmpFile)
    asmPath = tmpPath if os.path.exists(tmpPath) else asmPath
    return asmPath


