uiName = 'shadeMenu'
_title = 'Shade Menu'
_version = 'v.0.0.1'
_description = 'wip'

import os
import sys
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

is_valid = True
try:
    import maya.cmds as mc
    from rf_app.asm import asm_lib
    from functools import partial
    # from rf_maya.rftool.shade import shade_utils

    # =======
    from rf_maya.rftool.utils import maya_utils
    from rf_maya.rftool.shade import shade_utils
    from rf_utils.context import context_info
    from rf_utils import file_utils
    from rf_utils.pipeline import asset_tag
    from rf_app.info_panel import maya_dock
except:
    is_valid = False

def show(parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    info_menu()

def info_menu():
    sels = mc.ls(sl=True) 
    # pick first one 
    if sels: 
        sel = sels[0]
        if mc.referenceQuery(sel, inr=True): 
            path = mc.referenceQuery(sel, f=True)

            mc.menuItem(divider = True)
            mc.menuItem(l='Show Info', c=partial(show_panel, path))
            mc.menuItem(divider = True)

def show_panel(path, *args): 
    entity = context_info.ContextPathInfo(path=path)
    app = maya_dock.show(entity=entity, restore=True)
