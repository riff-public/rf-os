# import all modules in maya_lib 
import os
import sys 
import glob
from rf_maya.contextMenu import *
from collections import OrderedDict
from rf_utils import file_utils


def get_module(): 
    modules = glob.glob(os.path.dirname(__file__)+"/*.py")
    return [ os.path.basename(f)[:-3] for f in modules if os.path.isfile(f) and not f.endswith('__init__.py')]

def get_module_data(): 
    mods = OrderedDict()
    allModules = get_module()
    
    for name in allModules: 
        func = __import__(name, globals(), locals(), [], -1)
        reload(func)
        mods[name] = func

    return mods


def get_project_config(): 
	active_project = os.environ.get('ACTIVE_PROJECT') or 'default'
	config = config_path(active_project)
	config_data = file_utils.ymlLoader(config)
	return config_data


def config_path(project): 
	scriptRoot = os.environ['RFSCRIPT']
	configDir = '%s/core/rf_env/%s/maya/2018/config' % (scriptRoot, project)
	configFile = 'dag_menu_config.yml'
	configPath = '%s/%s' % (configDir, configFile)
	
	if not os.path.exists(configPath): 
		return config_path('default')

	return configPath
