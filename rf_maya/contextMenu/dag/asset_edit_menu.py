uiName = 'asmMenu'
_title = 'Asm Menu'
_version = 'v.0.0.1'
_description = 'wip'

import os
import sys
from datetime import datetime
from collections import OrderedDict
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

is_valid = True
try:
    import maya.cmds as mc
    from rf_app.asm import asm_lib
    from functools import partial
    from rf_utils.context import context_info
    from rf_utils import file_utils
except:
    is_valid = False

def show(parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    mc.menuItem(divider = True)
    get_selection()
    mc.menuItem(divider = True)

def get_selection():
    # detect selection if Ref or GPU
    sels = mc.ls(sl=True)
    if sels and len(sels) == 1:
        obj = sels[0]
        path = is_ref_or_gpu(obj)
        edit_menu(path)

def is_ref_or_gpu(obj):
    if mc.referenceQuery(obj, inr=True):
        return mc.referenceQuery(obj, f=True)
    if mc.objectType(obj, isType='transform'):
        shape = mc.listRelatives(obj, s=True, f=True)
        if shape:
            if mc.objectType(shape[0], isType='gpuCache'):
                return mc.getAttr('%s.cacheFileName' % shape[0])
    if mc.objectType(obj, isType='gpuCache'):
        return mc.getAttr('%s.cacheFileName' % obj)
    return False

def edit_menu(path):
    if path:
        mc.menuItem(label='Edit Asset', subMenu=True)
        mc.menuItem(label = 'Model', c=partial(edit_model, path))
        mc.menuItem(label = 'Lookdev', c=partial(edit_lookdev, path))
        mc.setParent( '..', menu=True )

def edit_model(path, *args):
    # assume it's hero dir
    heroDir = os.path.dirname(path)
    output = os.path.basename(path)
    files = list_file(heroDir)

    asset = context_info.ContextPathInfo(path=path)
    # fill context by output
    outputList = asset.guess_outputKey(output)

    if outputList:
        # setup context
        outputKey, step = outputList[0]
        asset.context.update(step='model', process='main', app='maya')
        asset.extract_name(output, outputKey=outputKey)

        heroOutout = get_model_hero(asset)
        if os.path.exists(heroOutout):
            saveWorkspace = get_model_file_workspace(asset)
            result = confirm_dialog(asset)
            if result == 'Yes':
                clear_workspace(asset)
                import_asset(heroOutout)
                save_workspace(saveWorkspace)


def get_model_hero(asset):
    # get model hero
    outputDir = asset.path.output_hero()
    outputName = asset.output_name(outputKey='cache')
    relPath = '%s/%s' % (outputDir, outputName)
    absPath = '%s/%s' % (outputDir.abs_path(), outputName)

    if os.path.exists(absPath):
        return absPath

def get_model_file_workspace(asset):
    # workspace
    asset.context.update(version='v001')
    path = asset.path.workspace().abs_path()
    filename = asset.work_name(user=True)
    workspaceFile = '%s/%s' % (path, filename)
    incrementFile = file_utils.increment_file(workspaceFile)
    return incrementFile


def clear_workspace(asset):
    geoGrp = asset.projectInfo.asset.geo_grp()
    print geoGrp
    if mc.objExists(geoGrp):
        if mc.referenceQuery(geoGrp, inr=True):
            path = mc.referenceQuery(geoGrp, f=True)
            mc.file(path, rr=True)
        else:
            mc.delete(geoGrp)


def edit_lookdev(path, *args):
    pass

def confirm_dialog(asset):
    result = mc.confirmDialog( title='Confirm Switch workspace',
                        message='Switch workspace to "%s"?' % asset.name,
                        button=['Yes','Canel'],
                        defaultButton='Yes',
                        cancelButton='Cancel',
                        dismissString='Cancel'
                        )
    return result

def list_file(path):
    return [d for d in os.listdir(path) if os.path.isfile(os.path.join(path,d))]

def import_asset(path):
    print 'path', path
    return mc.file(path, i=True, options='v=0', pr=True, loadReferenceDepth='all')


def save_workspace(path):
    mc.file(rename=path)
    return mc.file(save=True, f=True, type='mayaAscii')
