uiName = 'shadeMenu'
_title = 'Shade Menu'
_version = 'v.0.0.1'
_description = 'wip'

import os
import sys
import re
from functools import partial
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

is_valid = True
try:

    import maya.cmds as mc
    from rf_app.asm import asm_lib
    # from rf_maya.rftool.shade import shade_utils

    # =======
    from rf_maya.rftool.utils import maya_utils
    from rf_maya.rftool.shade import shade_utils
    from rf_utils.context import context_info
    # reload(context_info)
    from rf_utils import file_utils
    from rf_utils.pipeline import asset_tag
    # reload(shade_utils)
    from rf_maya.rftool.shade import intersect_check_shader
    # reload(intersect_check_shader)
except:
    is_valid = False

def show(parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    mc.menuItem(divider = True, dividerLabel='material menu')
    shade_menu()
    mc.menuItem(divider = True)

def shade_menu():
    entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
    geoGrpName = entity.projectInfo.asset.geo_grp() or 'Geo_Grp'
    # check ref
    sels = mc.ls(sl=True, l=True)
    if sels:
        namespaces = [mc.referenceQuery(a, ns=True) for a in sels if mc.referenceQuery(a, inr=True)]
        # find a valid Rig by searching for Rig_Grp
        showMenu = False
        rigGrps = set()
        for namespace in namespaces:
            rigGrp = '%s:%s' % (namespace[1:], geoGrpName)

            if mc.objExists(rigGrp):
                showMenu = True
                rigGrps.add(rigGrp)

        rigGrps = list(rigGrps)
        if showMenu:
            switch_menu(rigGrps=rigGrps, geoGrpName=geoGrpName)
            mc.menuItem(divider=True)
            fix_shade_menu(rigGrps)
            mc.menuItem(divider=True)
            intersect_check_menu(rigGrps=rigGrps, sels=sels)
            

def fix_shade_menu(rigGrps):
    mc.menuItem(l='Fix shader', c=partial(fix_shade, rigGrps))
    mc.menuItem(l='Clear shader', c=partial(clear_shade, rigGrps))

def intersect_check_menu(rigGrps, sels): 
    mc.menuItem(l='Check intersection', subMenu=True)
    mc.menuItem(l='Apply intersect Shader (All)', c=partial(apply_intersect_shade, rigGrps))
    mc.menuItem(l='Apply intersect Shader (Selected)', c=partial(apply_intersect_shade_selected, sels))
    mc.menuItem(divider=True)
    mc.menuItem(l='Restore shader', c=partial(restore_intersect_shade, rigGrps))
    mc.setParent( '..', menu=True )

def apply_intersect_shade(rigGrps, *args): 
    intersect_check_shader.apply_random_shader(objs=rigGrps, all_geo=True, n=6)

def apply_intersect_shade_selected(sels, *args): 
    intersect_check_shader.apply_random_shader(objs=sels, all_geo=False, n=6)

def restore_intersect_shade(rigGrps, *args): 
    if not rigGrps:
        rigGrps = [s for s in mc.ls(sl=True, type='transform') if mc.referenceQuery(s, inr=True)]
        if not rigGrps:
            return

    namespaces = set([obj.split('|')[-1].rpartition(':')[0] + ':' for obj in rigGrps])
    # print namespaces
    shader_to_del = []
    for ns in namespaces:
        shadeFile, topGrpName = shade_utils.get_recorded_shade_data(namespace=ns)
        if shadeFile and os.path.exists(os.path.expandvars(shadeFile)):
            connectRef = shade_utils.find_connect_ref_shade(topGrpName)
            switch_shade(shadeFile=shadeFile , geoNamespace=ns, connectRef=connectRef)

        shaders = intersect_check_shader.list_all_intersect_shaders(namespace=ns)
        shader_to_del.extend(shaders)

    if shader_to_del:
        mc.delete(shader_to_del)

def switch_menu(rigGrps, geoGrpName):
    # create menu
    mc.menuItem(l='Switch shader', subMenu=True)

    # if there's only 1 selection...
    if len(rigGrps) == 1:
        mc.radioMenuItemCollection()

        sel = rigGrps[-1]
        # geo namespace
        geoNamespace = mc.referenceQuery(sel, ns=True) if mc.referenceQuery(sel, inr=True) else ''

        # find mtr files
        heroDirRel, files, connectRef = list_mtr_files(sel)
        connectRef = context_info.RootPath(connectRef).rel_path() if connectRef else ''
        # heroDirRel = context_info.RootPath(heroDir).rel_path()
        for mtrFile in files:
            relPath = '%s/%s' % (heroDirRel, mtrFile)
            state = False
            connectRefPath = connectRef.split('{')[0]

            if context_info.RootPath(connectRefPath).striproot() == context_info.RootPath(relPath).striproot(): 
            # if connectRefPath == relPath:
                state = True

            mc.menuItem(l=mtrFile, rb=state, c=partial(switch_shade, relPath, geoNamespace, connectRef))

        # mc.setParent( '..', menu=True)
        mc.menuItem(divider=True)

    available_shade_types = ('groom_main_md', 'ldv_main_md', 'mdl_main_md', 'rig_main_md', 'tmp_main_md')
    # switch only selection to
    mc.menuItem(l='Switch selected to...', subMenu=True) 
    for shadeType in available_shade_types:
        mc.menuItem(l=shadeType, c=partial(switch_shader_to, shadeType, rigGrps))
    mc.setParent( '..', menu=True )

    # switch all to
    mc.menuItem(l='Switch all to...', subMenu=True)
    all_assets = mc.ls('*:{}'.format(geoGrpName)) 
    for shadeType in available_shade_types:
        mc.menuItem(l=shadeType, c=partial(switch_shader_to, shadeType, all_assets))

    mc.setParent( '..', menu=True )
    mc.setParent( '..', menu=True )

def switch_shader_to(shadeType, rigGrps, *args):
    failed_assets = []
    for rigGrp in rigGrps:
        # geo namespace
        geoNamespace = mc.referenceQuery(rigGrp, ns=True) if mc.referenceQuery(rigGrp, inr=True) else ''
        if not geoNamespace:
            continue

        heroDirRel, files, connectRef = list_mtr_files(rigGrp)
        asset_entity = context_info.ContextPathInfo(path=heroDirRel)
        asset_name = asset_entity.name
        connectRef = context_info.RootPath(connectRef).rel_path() if connectRef else ''
        for mtrFile in files:
            if mtrFile == '{}_mtr_{}.hero.ma'.format(asset_name, shadeType):
                relPath = '%s/%s' % (heroDirRel, mtrFile)
                switch_shade(relPath, geoNamespace, connectRef)
                break
        else:
            failed_assets.append(geoNamespace[1:])  # [1:] to  get rid of ":" at the start of the string
    if failed_assets:
        logger.warning('{} asset(s) failed to switch shader. {}'.format(len(failed_assets), ', '.join(failed_assets)))
    else:
        logger.info('Switch shader to "{}" success on all {} asset(s).'.format(shadeType, len(rigGrps)))


def fix_shade(rigGrps, *args):
    for rigGrp in rigGrps:
        trs = maya_utils.find_ply(rigGrp, True)
        if trs:
            for tr in trs:
                shapes = mc.listRelatives(tr, s=True, pa=True)
                if not shapes:
                    continue
                sgs = mc.listConnections(shapes[0], type='shadingEngine')
                if sgs and 'initialShadingGroup' not in sgs:
                    shade_utils.reconnect_selected_shader(tr)
                    break
            else:
                shade_utils.reconnect_selected_shader(trs[0])

        shadeFilePath, shadeNamespace, geoNameSpace = shade_utils.get_assigned_ref_shade(trs[0])
        if 'rig_main_md' in shadeFilePath :
            print geoNameSpace
            rigSE = mc.ls('{}:*'.format(geoNameSpace), type= 'shadingEngine')
            if rigSE:
                shade_utils.assignRigShade(geoNameSpace)

def clear_shade(rigGrps, *args):
    for rigGrp in rigGrps:
        trs = maya_utils.find_ply(rigGrp)
        if trs:
            for tr in trs:
                shapes = mc.listRelatives(tr, s=True)
                if not shapes:
                    continue
                sgs = mc.listConnections(shapes[0], type='shadingEngine')
                if sgs and 'initialShadingGroup' not in sgs:
                    shade_utils.clear_selected_shader(tr)
                    break
            else:
                shade_utils.clear_selected_shader(trs[0])

def list_mtr_files(obj):
    # use only last one
    # get ref
    attr = '%s.refPath' % obj
    path = mc.getAttr(attr) or mc.referenceQuery(obj, f=True) if mc.objExists(attr) else mc.referenceQuery(obj, f=True)
    heroDirRel = context_info.ContextPathInfo(path=path).path.hero()
    heroDir = heroDirRel.abs_path()
    # heroDir = os.path.dirname(path)
    files = file_utils.list_file(heroDir)

    plys = maya_utils.find_ply(obj, f=True)

    connectRef = shade_utils.find_connect_ref_shade(plys[0]) if plys else ''

    # hard code only mtr
    key = '_mtr_'
    files = [a for a in files if key in a] if files else []

    return heroDirRel, files, connectRef

def switch_shade(shadeFile, geoNamespace, connectRef, *args):
    print 'Switching shader...', shadeFile, geoNamespace, connectRef, args
    asset = context_info.ContextPathInfo(path=shadeFile)
    shadeFileExt = os.path.basename(shadeFile)
    shadeFileName, ext = os.path.splitext(shadeFileExt)

    # there is a bug when asset name has "_" which will fall into wrong naming convention
    # fix asset name temporary to get the correct result
    if len(asset.name.split('_')) > 1: 
        shadeFileExt = shadeFileExt.replace(asset.name, 'assetName')
    outputList = asset.guess_outputKey(shadeFileExt)
    if outputList:
        # --- assign shaders
        outputKey, step = outputList[0]
        asset.context.update(step=step)
        asset.extract_name(os.path.basename(shadeFile), outputKey=outputKey)
        shadeNamespace = asset.mtr_namespace
        geoGrpName = asset.projectInfo.asset.geo_grp() or 'Geo_Grp'
        geoGrp = '%s:%s' %(geoNamespace, geoGrpName)

        # separate shader 
        separateShaderAttr = '%s.separateShader' % geoGrp
        separateShader = False
        if mc.objExists(separateShaderAttr): 
            separateShader = mc.getAttr(separateShaderAttr)

            if separateShader: 
                instance_num = re.findall(r'\d{3}', geoNamespace)[0]
                shadeNamespace = '{}_{}'.format(shadeNamespace, instance_num)
        # separate shader End

        connectionData = []
        dataFile = asset.output_name(outputKey='shdConnectData', hero=True)
        heroPath = asset.path.hero().abs_path()
        dataFile = '%s/%s' % (heroPath, dataFile)
        if os.path.isfile(dataFile):
            print 'found Connection Data assign it!'
            connectionData = file_utils.ymlLoader(dataFile)
        else:
            print 'ConnectionData not found!'
            if mc.objExists('%s.shdConnect' %geoGrp):
                connectionStr = mc.getAttr('%s.shdConnect' %geoGrp)
                connectionData = eval(connectionStr)


        shadeFileName = shadeFileName.split('.')[0]  # get rid of .hero
        shadeKey = shadeFileName.split('_mtr_')[-1]
        connectionInfo = []

        if shadeKey in connectionData:
            connectionInfo = connectionData[shadeKey]

        # apply shader and connections
        shade_utils.apply_ref_shade(shadeFile=shadeFile, 
                                shadeNamespace=shadeNamespace, 
                                targetNamespace=geoNamespace,
                                connectionInfo=connectionInfo)
        # add tag
        asset_tag.add_tag(geoGrp, look=asset.look, mtrPath=shadeFile) if mc.objExists(geoGrp) else None

        if shade_utils.unconnected_shader(connectRef):
            mc.file(connectRef, rr=True)

        if 'rig_main_md' in shadeFile :
            print geoNamespace
            rigSE = mc.ls('{}:*'.format(geoNamespace), type= 'shadingEngine')
            if rigSE:
                shade_utils.assignRigShade(geoNamespace)
    else:
        logger.warning('Cannot switch %s. Unknown naming convention' % shadeFile)

def multiple_switch(shadeKey='_rig_', selection=[]): 
    entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
    geoGrpName = entity.projectInfo.asset.geo_grp() or 'Geo_Grp'
    assets = mc.ls('*:{}'.format(geoGrpName))

    if selection:
        assets = []
        for sel in selection:
            ns = sel.split(':')[0]
            asset = ns + ':' + geoGrpName
            if not asset in assets:
                assets.append(asset)

    for asset in assets: 
        heroRel, files, connectRef = list_mtr_files(asset)
        mtrFile = [a for a in files if shadeKey in a]
        if mtrFile: 
            geoNamespace = mc.referenceQuery(asset, ns=True)
            # heroRel = context_info.RootPath(heroDir).rel_path()
            relPath = '%s/%s' % (heroRel, mtrFile[0])
            # print geoNamespace, relPath
            switch_shade(relPath, geoNamespace, connectRef)
            print 'switch %s complete' % asset
        else:
            print 'Asset %s : Dont have %s mtr .' % (asset, shadeKey)

