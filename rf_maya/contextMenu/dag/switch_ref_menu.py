uiName = 'switchMenu'
_title = 'Switch Menu'
_version = 'v.0.0.1'
_description = 'wip'

import os
import sys
import logging
from functools import partial
from datetime import datetime
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

is_valid = True
try:
    import maya.cmds as mc
    import pymel.core as pm
    from rf_app.asm import asm_lib
    # from rf_maya.rftool.shade import shade_utils
    # =======
    from rf_maya.rftool.utils import maya_utils
    from rf_maya.rftool.shade import shade_utils
    from rf_utils.context import context_info
    from rf_utils import register_shot
    from rf_utils import file_utils
    from rf_app.anim.animIO import core as animIOCore
    from rf_maya.lib import sequencer_lib
    from rf_maya.rftool.subasset import subasset_utils
    # reload(animIOCore)
    # reload(shade_utils)
except:
    is_valid = False

def show(parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    mc.menuItem(divider = True)
    menu()
    mc.menuItem(divider = True)

def menu():
    key = 'Geo_Grp'
    # check ref
    sels = mc.ls(sl=True, l=True)
    if sels:
        refSels = [a for a in sels if mc.referenceQuery(a, inr=True)]
        namespaces = [mc.referenceQuery(a, ns=True) for a in refSels]
        # find a valid Rig by searching for Rig_Grp
        showMenu = False
        rigGrps = []
        for namespace in namespaces:
            rigGrp = '%s:%s' % (namespace[1:], key)

            if mc.objExists(rigGrp):
                showMenu = True
                rigGrps.append(rigGrp)
        if showMenu:
            mc.menuItem(l='Switch asset', subMenu=True)
            for func in (switch_refCurve_menu, switch_ref_menu, switch_cache_menu, switch_variant_menu):
                try:
                    func(rigGrps)
                except Exception as e:
                    pass
            switch_subasset_menu(rigGrps)
            mc.setParent( '..', menu=True )

def switch_subasset_menu(rigGrps):
    mc.menuItem(l='Switch subasset', subMenu=True)
    subassetAllList = []
    for rigGrp in rigGrps:
        fp = mc.referenceQuery(rigGrp, f=True)
        entity = context_info.ContextPathInfo(path=fp)
        try:
            subassetData = subasset_utils.read_data(entity)
        except:
            return
        listsubasset = subasset_utils.list_subassets(rigGrp)
        subassetList =  []
        if subassetData:
            for name in subassetData.keys():
                subassetList.append(name)
        else:
            return

        if rigGrp == rigGrps[0]:
            subassetAllList= subassetList
        else:
            subassetAllList = list(set(subassetList) & set(subassetAllList))

    for name in subassetAllList:
        state = False
        if name in listsubasset:
            state = True
        box = mc.menuItem('{}_box'.format(name) ,l=name, checkBox=state, c=partial(load_subasset, rigGrps ,name, entity))

def load_subasset(rigGrps, name, entity, *args):
    state = mc.menuItem('{}_box'.format(name) ,q=True, checkBox=True)
    for rigGrp in rigGrps:
        listsubasset = subasset_utils.list_subassets(rigGrp)
        if state == True:
            if not name in listsubasset:
                loadedns = subasset_utils.load_named_subasset(rigGrp, name)
        else:
            if name in listsubasset:
                subasset_utils.unload_subasset(rigGrp, [name])
        propGrp = entity.projectInfo.asset.prop_grp()
        rigSubAssetGrp = entity.projectInfo.asset.top_grp()
        objs = '{}:{}'.format(loadedns ,rigSubAssetGrp)
        if not mc.objExists(propGrp):
            propGrp = mc.group(em=True, n=propGrp)
        mc.parent(objs, propGrp)


def switch_refCurve_menu(rigGrps, *args):
    if len(rigGrps) == 1:
        files = list_ref(rigGrps[0])
        rnNode = mc.referenceQuery(rigGrps[0], referenceNode=True)
        namespace = mc.referenceQuery(rnNode, ns=True)[1:]
        scene = context_info.ContextPathInfo()
        reg = register_shot.Register(scene)
        data = reg.get.asset(namespace)
        animFile = data.get('animCurve').get('heroFile') if 'animCurve' in data.keys() else ''

        if files and animFile:
            mc.menuItem(l='Rig + animCurve', subMenu=True)
            mc.radioMenuItemCollection()

            for rigFile in files:
                display = os.path.basename(rigFile)
                state = False
                connectRefPath = mc.referenceQuery(rigGrps[0], f=True).split('{')[0]

                if connectRefPath == rigFile:
                    state = True
                mc.menuItem(l=display, rb=state, c=partial(switch_rig_animcurve, rigFile, rnNode, animFile))

            mc.setParent( '..', menu=True )

def switch_ref_menu(rigGrps, *args):
    if len(rigGrps) == 1:
        files = list_ref(rigGrps[0])
        rnNode = mc.referenceQuery(rigGrps[0], referenceNode=True)
        if files:
            mc.menuItem(l='Rig', subMenu=True)
            mc.radioMenuItemCollection()
            for rigFile in files:
                display = os.path.basename(rigFile)
                state = False
                connectRefPath = mc.referenceQuery(rigGrps[0], f=True).split('{')[0]

                if connectRefPath == rigFile:
                    state = True
                mc.menuItem(l=display, rb=state, c=partial(switch_rig, rigFile, rnNode))
            mc.setParent( '..', menu=True )

def switch_cache_menu(rigGrps, *args):
    if len(rigGrps) == 1:
        files = list_cache(rigGrps[0], mode='hardcode')
        currentCache = mc.referenceQuery(rigGrps[0], f=True).split('{')[0]
        rnNode = mc.referenceQuery(rigGrps[0], referenceNode=True)
        if files:
            mc.menuItem(l='Cache', subMenu=True)
            mc.radioMenuItemCollection()
            for cacheFile in files:
                state = False
                if cacheFile == currentCache:
                    state = True
                display = os.path.basename(cacheFile)
                mc.menuItem(l=display, rb=state, c=partial(switch_cache, cacheFile, rnNode))

            mc.setParent( '..', menu=True )

def switch_variant_menu(rigGrps, *args):
    mc.menuItem(l='Variant...', subMenu=False, c=partial(launch_asset_variant))
    mc.setParent( '..', menu=True )

def launch_asset_variant(*args):
    from rf_app.asset_variant import app as asset_variant_app
    reload(asset_variant_app)
    asset_variant_app.show()

def switch_rig(rigFile, rnNode, *args):
    mc.file(rigFile, loadReference=rnNode, prompt=False)

def switch_cache(cacheFile, rnNode, *args):
    from rf_maya.rftool.fix import moveSceneToOrigin as msto

    namespace = mc.referenceQuery(rnNode, ns=True)[1:]
    assetEntity = context_info.ContextPathInfo(path=cacheFile)

    mc.file(cacheFile, loadReference=rnNode, prompt=False)

    scene = context_info.ContextPathInfo()
    offsetValue, allTransforms = msto.get_cameraDistanceFromOrigin(scene)
    ctrl = '%s:%s' % (namespace, assetEntity.projectInfo.asset.geo_grp())
    msto.offset_asset(obj=ctrl, offset=offsetValue, toOrigin=False, isCache=True)

def switch_rig_animcurve(rigFile, rnNode, animFile, *args): 
    namespace = mc.referenceQuery(rnNode, ns=True)[1:]
    scene = context_info.ContextPathInfo()
    sf, ef, ssf, sef = sequencer_lib.shot_info(scene.name_code)
    rootGrp = scene.projectInfo.asset.get('allMoverCtrl') or 'AllMover_Ctrl'
    rootNode = '%s:%s' % (namespace, rootGrp)

    if os.path.exists(animFile): 
        refObj = pm.FileReference(rnNode)
        refObj.unload()
        refObj.clean()
        refObj.load()
        mc.file(rigFile, loadReference=rnNode, prompt=False)
        metaData = animIOCore.readMetaData(path=animFile)
        animData = animIOCore.AnimData(name=namespace, path=animFile, start=sf, end=ef)
        animAsset = animIOCore.AnimAsset(root=rootNode)
        import_result = animAsset.import_anim(data=animData, replaceCompletely=True)

    else:
        logger.warning('No published anim data yet')

def list_ref(rigGrp):
    # find hero path
    heroPath = get_hero_path(rigGrp, mode='ref')
    files = list_switchable_files(heroPath)
    if not files:
        heroPath = get_hero_path(rigGrp, mode='attr')
        files = list_switchable_files(heroPath)

    return files

def list_cache(rigGrp, mode='hardcode'):
    namespace = mc.referenceQuery(rigGrp, ns=True)
    namespace = namespace.replace(':', '')
    cacheFiles = []
    cachePath = ''
    # hard code
    if mode == 'hardcode':
        sn = mc.file(q=True, sn=True)
        publ = sn.replace('/work/', '/publ/')
        # stepPath = ('/').join(publ.split('/')[0:-3])
        stepPath = ('/').join(publ.split('/')[0:-4])
        cachePath = '%s/output/%s' % (stepPath, namespace)

    # context
    if mode == 'context':
        context = context_info.Context()
        if hasattr(context, 'use_env'):
            context.use_env()
            scene = context_info.ContextPathInfo(context=context)
            scene.context.update(process=namespace)
            cachePath = os.path.split(scene.path.cache().abs_path())[0]

    if cachePath:
        for root, dirs, files in os.walk(cachePath):
            abcFiles = [a for a in files if os.path.splitext(a)[-1] == '.abc']
            root = root.replace('\\', '/')
            for cacheFile in abcFiles:
                cacheFiles.append('%s/%s' % (root, cacheFile))
    return convert_rel_path(cacheFiles)

def get_hero_path(rigGrp, mode=''):
    heroPath = ''
    # 1. get from attribute Geo_Grp.refPath
    if mode == 'attr':
        keyHeroPath = '%s.refPath' % rigGrp
        if mc.objExists(keyHeroPath):
            heroPath = mc.getAttr(keyHeroPath)

    # 2. get from ref path directly
    if mode == 'ref':
        heroPath = mc.referenceQuery(rigGrp, f=True)

    # 3. get from ad file, key "heroPath"
    if mode == 'ad':
        heroPath = ''

    return heroPath

def list_switchable_files(heroPath):
    key = 'rig' # hard coded key -> asset_rig_ expect 2nd elements
    switchFiles = []
    if heroPath:
        heroDir = os.path.dirname(heroPath)
        files = list_file(heroDir, ext=['.ma'])
        switchFiles = [a for a in files if key in os.path.basename(a).split('_')[1]]
    return convert_rel_path(switchFiles)

def list_file(path, ext=[]):
    files = ['/'.join([path, d]) for d in os.listdir(path) if os.path.isfile('/'.join([path, d]))]
    if ext:
        return [a for a in files if os.path.splitext(a)[-1] in ext]
    return files

def list_dir(path):
    dirs = ['/'.join([path, d]) for d in os.listdir(path) if os.path.isdir('/'.join([path, d]))]
    return dirs

def convert_rel_path(files):
    return [context_info.RootPath(a).rel_path() for a in files] if files else []

