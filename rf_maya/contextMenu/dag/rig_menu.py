uiName = 'rigMenu'
_title = 'Rig Menu'
_version = 'v.0.0.1'
_description = 'wip'

import os
import sys
from functools import partial
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

is_valid = True
try:
    import maya.cmds as mc
    from rf_app.asm import asm_lib
    # from rf_maya.rftool.shade import shade_utils
    from rftool.rig.ncmel import match
    # reload( match )
    # =======
    from rf_maya.rftool.utils import maya_utils
    from rf_maya.rftool.shade import shade_utils
    # from rf_utils.context import context_info
    # context_info.logger.propagate = False
    from rf_utils import file_utils
    # reload(shade_utils)
except:
    is_valid = False
    
def show(parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    mc.menuItem(divider = True)
    rig_menu()
    mc.menuItem(divider = True)

def rig_menu():
    key = 'Geo_Grp'
    # check ref
    sels = mc.ls(sl=True, l=True)

    if sels:
        refSels = [a for a in sels if mc.referenceQuery(a, inr=True)]
        namespaces = [mc.referenceQuery(a, ns=True) for a in refSels]

        # find a valid Rig by searching for Rig_Grp
        showMenu = False
        rigGrps = []
        for namespace in namespaces:
            rigGrp = '%s:%s' % (namespace[1:], key)

            if mc.objExists(rigGrp):
                showMenu = True
                rigGrps.append(rigGrp)

        if showMenu:
            mc.menuItem(l='Rig', subMenu=True)
            space_menu(rigGrps)
            fkik_menu(rigGrps)
            mc.setParent( '..', menu=True)

def space_menu(rigGrps, *args):

    ctrl = mc.ls( sl = True )[0]
    if mc.objExists( '%s.spaceFollow' %ctrl ) :
        mc.menuItem(l='Switch Space', subMenu=True)
        attrEnums = mc.attributeQuery( 'spaceFollow' , node = ctrl , listEnum = True )[0].split(':')
    
        for spaceAttr in attrEnums :
            list_space( ctrl , spaceAttr )

        mc.setParent( '..', menu=True)

    elif mc.objExists( '%s.localWorld' %ctrl ) :
        list_space( ctrl , 'Local World' )

def fkik_menu(rigGrps, *args):
    
    ctrl = mc.ls( sl = True )[0]
    if mc.objExists( '%s.fkIk' %ctrl ) :
        mc.menuItem( l = 'Switch FkIk' , c = fkIkMatch )
        mc.setParent( '..', menu=True)

def list_space(ctrl, space, *args):

    def switchSpace(*args) :
        match.localWorld( ctrl , space )

    mc.menuItem( l = space , c =  switchSpace )

def fkIkMatch(*args):
    match.run()
