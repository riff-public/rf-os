uiName = 'shadeMenu'
_title = 'Shade Menu'
_version = 'v.0.0.1'
_description = 'wip'

import os
import sys
from functools import partial
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

is_valid = True
try:
    import maya.cmds as mc
    from rf_app.asm import asm_lib
    # from rf_maya.rftool.shade import shade_utils

    # =======
    from rf_maya.rftool.utils import maya_utils
    from rf_maya.rftool.shade import shade_utils
    # from rf_utils.context import context_info
    # context_info.logger.propagate = False
    from rf_utils import file_utils
    # reload(shade_utils)
except:
    is_valid = False

def show(parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    sels = mc.ls(sl=True)
    show = False
    # if selection is curve
    for sel in sels:
        shape = mc.listRelatives(sel, s=True, f=True)
        if shape:
            if mc.objectType(shape[0], isType='nurbsCurve'):
                show = True

    if show:
        select_menu()
        anim_menu()

def select_menu():
    mc.menuItem(divider = True)
    mc.menuItem(divider = True, dividerLabel='material menu')
    mc.menuItem(l='Select All Ctrls', c=partial(select))

def anim_menu(): 
    mc.menuItem(l='Anim', subMenu=True)
    mc.menuItem(l='Clear selected Keyframes', c=partial(clear_key))
    mc.menuItem(l='Clear Asset Keyframes', c=partial(clear_key, True))
    mc.menuItem(divider = True)
    mc.menuItem(l='Fix Asset', c=partial(fix_asset))
    mc.menuItem(l='Renew', c=partial(renew))
    mc.setParent( '..', menu=True )
    mc.menuItem(divider = True)
    

def select(*args):
    sels = mc.ls(sl=True)
    ctrls = []
    namespaces = []
    for sel in sels:
        namespaces.append(sel.split(':')[0]) if ':' in sel else None
    namespaces = list(set(namespaces))

    for namespace in namespaces:
        ctrls += mc.ls('%s:*Ctrl' % namespace)
        ctrls += mc.ls('%s:*ctrl' % namespace)

    mc.select(ctrls)


def clear_key(all=False, *args): 
    select() if all else None
    mc.cutKey(cl=True)

def fix_asset(*args): 
    from nuTools.pipeline import cleanup
    reload(cleanup)
    cleanup.cleanAnimReference()

def renew(*args): 
    # remove all edits 
    sel = mc.ls(sl=True)
    if sel: 
        path = mc.referenceQuery(sel[0], f=True)
        mc.file(path, unloadReference=path)
        mc.referenceEdit(path, removeEdits=True)
        mc.file(path, loadReference=path)
        
        # shadeFilepath, shadeNamespace, geoNameSpace = shade_utils.fix_reference_shader(sel[0], False)
        # shade_utils.apply_ref_shade(shadeFilepath, shadeNamespace, geoNameSpace)
