uiName = 'lightMenu'
_title = 'Light Menu'
_version = 'v.0.0.1'
_description = 'wip'

import os
import sys
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

is_valid = True
try:
    import maya.cmds as mc
    from rf_app.asm import asm_lib
    from functools import partial
    # from rf_maya.rftool.shade import shade_utils

    # =======
    from rf_maya.rftool.utils import maya_utils
    from rf_maya.rftool.shade import shade_utils
    from rf_utils.context import context_info
    from rf_utils import file_utils
    from rf_utils.pipeline import set_matte_id
    # reload(shade_utils)
    # reload(set_matte_id)
except:
    is_valid = False

def show(parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    mc.menuItem(divider = True)
    light_menu()
    mc.menuItem(divider = True)

def light_menu():
    key = 'Geo_Grp'
    # check ref
    sels = mc.ls(sl=True, l=True)

    if sels:
        refSels = [a for a in sels if mc.referenceQuery(a, inr=True)]
        namespaces = [mc.referenceQuery(a, ns=True) for a in refSels]

        # find a valid Rig by searching for Rig_Grp
        showMenu = False
        rigGrps = []
        for namespace in namespaces:
            rigGrp = '%s:%s' % (namespace[1:], key)

            if mc.objExists(rigGrp):
                showMenu = True
                rigGrps.append(rigGrp)

        if showMenu:
            mc.menuItem(l='Light', subMenu=True)
            aov_menu(rigGrps)
            mc.setParent( '..', menu=True)


def aov_menu(rigGrps):
    mc.menuItem(l='Set Redshift MatteID', c=partial(create_redshift_matte, rigGrps))


def create_redshift_matte(rigGrps, *args):
    for rigGrp in rigGrps:
        path = mc.referenceQuery(rigGrp, f=True)
        asset = context_info.ContextPathInfo(path=path)
        asset.context.update(step='lookdev', look='main', res='md')
        heroDir = asset.path.hero().abs_path()
        name = asset.output_name(outputKey='matteData', hero=True)
        matteDataPath = '%s/%s' % (heroDir, name)
        set_matte_id.process_set_matee_id(matteDataPath, rigGrp)
