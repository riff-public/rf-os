# Riff Studio
# Handle preroll and postroll by using clip transition 
from PySide2 import QtWidgets
from PySide2 import QtCore
import os
import sys
from functools import partial 


import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 

is_valid = True
try:
    from rf_maya.lib import sequencer_utils as seq
    from rf_maya.lib import camera_lib
    from rf_utils.context import context_info
except:
    is_valid = False


def show(shotname='', parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    mc.menuItem(l='Fill space', c=partial(fill_gap, shotname))


def fill_gap(shotname, *args): 
    seq.ripple_delete_gap(shotname)
