# Riff Studio
# Handle preroll and postroll by using clip transition 
from PySide2 import QtWidgets
from PySide2 import QtCore
import os
import sys
from functools import partial 

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import pymel.core as pm 

is_valid = True
try:
    from rf_maya.lib import sequencer_utils as seq
    from rf_maya.lib import camera_lib
    from rf_utils.context import context_info
except:
    is_valid = False   

def show(shotname='', parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    mc.menuItem(l='Extend "{}"'.format(shotname), c=partial(extend, shotname))
    mc.menuItem(l='Split "{}"'.format(shotname), c=partial(split_shot_ui, shotname))

    shots = selected_shots()
    if shots: 
        if len(shots) >= 2: 
            mc.menuItem(l='Merge "{}" - "{}"'.format(shots[0], shots[1]), c=partial(merge, shots))


def extend(shotname, *args): 
    current = mc.sequenceManager(q=True, ct=True)
    dialog = InputDialog(frame=current)
    dialog.result.connect(partial(extend_shot, shotname))
    dialog.exec_()


def extend_shot(shot_name, input_dict): 
    num_frame = int(input_dict['frame'])
    head = int(input_dict['head'])
    tail = int(input_dict['tail'])
    pm.undoInfo(chunkName='extend shot', openChunk=True)
    seq.extend_shot_ripple(shot_name, num_frame, tail=tail, head=head)
    pm.undoInfo(closeChunk=True)


def merge(shots, *args): 
    """ 
    merge 2+ selected shots 
    add childs duration to master, disregard maya time range 
    """ 
    shots = pm.selected()
    if len(shots) == 2: 
        seq.merge_shots(shots[0], shots[1])
    else: 
        QtWidgets.QMessageBox.warning(None, 'Error Selection', 'Select only 2 shots. Start shot and end shot.')
    

def split_shot_ui(shot_name, *args): 
    """ split shot into 2 shots at the current timeframe """ 
    if seq.is_current_frame_at(shot_name): 
        frame = mc.sequenceManager(q=True, ct=True)
        dialog = SplitShotUi(frame)
        dialog.result.connect(partial(split_shot, shot_name))
        dialog.exec_()
    else: 
        QtWidgets.QMessageBox.warning(None, 'Warning', 'Current frame not in range of shot "{}"'.format(shot_name))


def split_shot(shot_name, input_dict): 
    new_shot = input_dict['shotName']
    current = seq.current_frame() 
    camera = create_cam(new_shot)
    seq.split_shot(split_frame=current, shot_name=shot_name, new_shot=new_shot, camera=camera)


def create_cam(shot_name): 
    entity = context_info.ContextPathInfo()
    project = entity.project

    camera = camera_lib.create_camera(shot_name, projectName=project)
    return camera


def fill_gap(shotname, *args): 
    seq.ripple_delete_gap(shotname)


def selected_shots(): 
    nodes = [a.name() for a in pm.selected() if a.nodeType() == 'shot']
    return nodes if nodes else []


class InputDialog(QtWidgets.QDialog):
    """docstring for InputDialog"""
    result = QtCore.Signal(dict)

    def __init__(self, frame, parent=None):
        super(InputDialog, self).__init__(parent=parent)
        self.current_frame = frame
        self.layout = QtWidgets.QVBoxLayout()
        self.gridLayout = QtWidgets.QGridLayout()
        self.buttonLayout = QtWidgets.QHBoxLayout()
        
        self.label1 = QtWidgets.QLabel('Frame : ')
        self.lineEdit1 = QtWidgets.QLineEdit()
        self.tail_radioButton = QtWidgets.QRadioButton('End')
        self.tail_radioButton.setChecked(True)
        self.head_radioButton = QtWidgets.QRadioButton('Start')
        self.checkBox = QtWidgets.QCheckBox('From current frame ({})'.format(self.current_frame))

        self.ok_button = QtWidgets.QPushButton('Extend')
        self.cancel_button = QtWidgets.QPushButton('Cancel')
        self.ok_button.setMinimumSize(QtCore.QSize(0, 30))
        self.cancel_button.setMinimumSize(QtCore.QSize(0, 30))

        self.gridLayout.addWidget(self.label1, 0, 0 , 1, 1)
        self.gridLayout.addWidget(self.lineEdit1, 0, 1, 1, 1)
        self.gridLayout.addWidget(self.head_radioButton, 0, 2, 1, 1)
        self.gridLayout.addWidget(self.tail_radioButton, 0, 3, 1, 1)

        self.buttonLayout.addWidget(self.ok_button)
        self.buttonLayout.addWidget(self.cancel_button)

        self.layout.addLayout(self.gridLayout)
        self.layout.addWidget(self.checkBox)
        self.layout.addLayout(self.buttonLayout)
        self.setLayout(self.layout)

        self.cancel_button.clicked.connect(self.close)
        self.ok_button.clicked.connect(self.submit)
        self.resize(260, 60)

    def submit(self): 
        self.result.emit({
                            'frame': self.lineEdit1.text(), 
                            'use_current_frame': self.checkBox.isChecked(), 
                            'current_frame': self.current_frame, 
                            'head': self.head_radioButton.isChecked(), 
                            'tail': self.tail_radioButton.isChecked()
                        })
        self.close()


class SplitShotUi(InputDialog):
    """docstring for SplitShotUi"""
    def __init__(self, frame, parent=None):
        super(SplitShotUi, self).__init__(frame, parent=parent)
        self.label1.setText('Shot Name : ')
        self.ok_button.setText('Split Shot')
        self.checkBox.setVisible(False)
        self.tail_radioButton.setVisible(False)
        self.head_radioButton.setVisible(False)


    def submit(self): 
        self.result.emit({
                            'shotName': self.lineEdit1.text(), 
                            'use_current_frame': self.checkBox.isChecked(), 
                            'current_frame': self.current_frame
                        })
        self.close()

        
        



# from rf_maya.lib import sequencer_lib as seq
# reload(seq)
# shotName = mc.ls(sl=True)[0]
# head=False 
# tail=True
# value = 2
# # seq.extend_shot_seq(shotName, value, head=head, tail=tail, timeslider=True)
# seqTargetFrame=1041
# timeOffset=2
# seqOffset=2
# seq.extend_shot_seq(shotName, seqTargetFrame=1044, timeOffset=timeOffset, seqOffset=seqOffset, mute=False, shiftKey=True, head=head, tail=tail)