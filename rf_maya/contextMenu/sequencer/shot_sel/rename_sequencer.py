# Riff Studio
# Handle preroll and postroll by using clip transition 
import os
import sys
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc

def show(shotname='', parent=None):
    # Menu setting menu area
    mc.menuItem(l='rename', c=rename)
    
def rename(*args):
    from rf_app.layout import rename_manager
    reload(rename_manager)
    rename_manager.show()
