# Riff Studio
# Handle preroll and postroll by using clip transition 
import os
import sys
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import pymel.core as pm 



def show(shotname='', parent=None):
    # Menu setting menu area
    mc.menuItem(l='Run Sequencer Manager', c=run_sequencer_manager)
    

def run_sequencer_manager(*args): 
    from rf_app.layout.sequencer_manager import app
    reload(app)
    reload(app.ui)
    reload(app.controls_program)
    app.show()