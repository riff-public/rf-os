# Riff Studio
# Handle preroll and postroll by using clip transition 
from PySide2 import QtWidgets
from PySide2 import QtCore
import os
import sys
from functools import partial 

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 

is_valid = True
try:
    from rf_maya.lib import sequencer_utils as seq
    from rf_maya.lib import camera_lib
    from rf_utils.context import context_info
except:
    is_valid = False

def show(shotname='', parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    mc.menuItem(l='Insert Shot after "{}"'.format(shotname), c=partial(insert_shot, shotname))


def insert_shot(shotname, *args): 
    dialog = InputDialog()
    dialog.result.connect(partial(create_shot_cmd, shotname))
    dialog.exec_()


def create_shot_cmd(anchor_shot, input_dict): 
    entity = context_info.ContextPathInfo()
    project = entity.project
    shot_name = input_dict['shotName']
    duration = int(input_dict['duration'])
    shot_buffer = entity.projectInfo.scene.duration_shot or 100

    camera = camera_lib.create_camera(shot_name, projectName=project)
    seq.insert_shot(shot_name, camera, anchor_shot, duration, shot_buffer, round_number=False)


class InputDialog(QtWidgets.QDialog):
    """docstring for InputDialog"""
    result = QtCore.Signal(dict)

    def __init__(self, parent=None):
        super(InputDialog, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.gridLayout = QtWidgets.QGridLayout()
        self.buttonLayout = QtWidgets.QHBoxLayout()


        self.name_label = QtWidgets.QLabel('Shot Name : ')
        self.name_lineEdit = QtWidgets.QLineEdit()
        
        self.duration_label = QtWidgets.QLabel('Duration : ')
        self.duration_lineEdit = QtWidgets.QLineEdit()

        self.ok_button = QtWidgets.QPushButton('Create')
        self.cancel_button = QtWidgets.QPushButton('Cancel')
        self.ok_button.setMinimumSize(QtCore.QSize(0, 26))
        self.cancel_button.setMinimumSize(QtCore.QSize(0, 26))

        self.gridLayout.addWidget(self.name_label, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.name_lineEdit, 0, 1, 1, 1)
        self.gridLayout.addWidget(self.duration_label, 1,0 , 1, 1)
        self.gridLayout.addWidget(self.duration_lineEdit, 1, 1, 1, 1)

        self.buttonLayout.addWidget(self.ok_button)
        self.buttonLayout.addWidget(self.cancel_button)

        self.layout.addLayout(self.gridLayout)
        self.layout.addLayout(self.buttonLayout)
        self.setLayout(self.layout)
        self.resize(260, 100)

        self.cancel_button.clicked.connect(self.close)
        self.ok_button.clicked.connect(self.submit)


    def submit(self): 
        self.result.emit({'shotName': self.name_lineEdit.text(), 'duration': self.duration_lineEdit.text()})
        self.close()
