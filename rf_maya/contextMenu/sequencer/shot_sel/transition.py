# Riff Studio
# Handle preroll and postroll by using clip transition

import os
import sys
from datetime import datetime
from functools import partial

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc

is_valid = True
try:
    from rf_maya.lib import sequencer_lib as seq
except:
    is_valid = False

def show(parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    # mc.menuItem(divider = True)
    mc.menuItem(l='Add Preroll Postroll', c=partial(add))
    mc.menuItem(l='Remove Preroll Postroll', c=partial(remove))
    # mc.menuItem(divider = True)


def add(*args):
    from rf_utils.context import context_info
    entity = context_info.ContextPathInfo()
    preValue = entity.projectInfo.scene.pre_roll or 24
    postValue = entity.projectInfo.scene.post_roll or 24

    shots = mc.ls(sl=True, type='shot')

    for shotName in shots:
        seq.add_transition_roll(shotName, preRoll=preValue, postRoll=postValue)
        logger.info('Add "%s" preroll "%ss" frame and postroll "%s" frames' % (shotName, preValue, postValue))


def remove(*args):
    shots = mc.ls(sl=True, type='shot')
    for shotName in shots:
        seq.remove_transition_roll(shotName)
        logger.info('Remove preroll and postroll %s' % shotName)
