# Riff Studio
# Handle preroll and postroll by using clip transition 

import os
import sys
from datetime import datetime
from functools import partial 

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 

is_valid = True
try:
    from rf_maya.lib import sequencer_lib as seq
    # reload(seq)
    from rf_app.layout.sequencer import shot_widget
    # reload(shot_widget)
    from rftool.utils.ui import maya_win
except:
    is_valid = False

def show(parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    # mc.menuItem(divider = True)
    mc.menuItem(l='Extend', c=partial(extend))
    mc.menuItem(l='Trim', c=partial(trim))
    # mc.menuItem(divider = True)


def extend(*args): 
    modify_shot(extend=True)


def trim(*args): 
    modify_shot(trim=True) 


def modify_shot(extend=False, trim=False): 
    mode = 'Extend' if extend else 'Trim'
    selections = mc.ls(sl=True, type='shot')
    shotName = selections[0] if selections else None
    if shotName: 
        st, et, sst, sse = seq.shot_info(shotName)
        currentTime = int(mc.sequenceManager(q=True, currentTime=True))
        dialog = shot_widget.ShotModifyUi.show(mode, parent=maya_win.getMayaWindow()) 
        if dialog.confirm: 
            dictValue = dialog.dictValue
            head = dictValue.get('head')
            tail = dictValue.get('tail')
            value = dictValue.get('value')
            shift = dictValue.get('shift')
            shiftCurrent = dictValue.get('shiftCurrent')
            timeslider = dictValue.get('fitRange')
            if extend: 
                value = abs(value)
            if trim: 
                value = -abs(value)
            
            timeOffset = int(value)
            seqOffset = int(value)

            targetFrame = sse if tail else sst
            targetFrame = currentTime if shiftCurrent else targetFrame
            seqTargetFrame = targetFrame+1 if tail else targetFrame-1
            print seqTargetFrame

            seq.extend_shot_seq(shotName, seqTargetFrame=seqTargetFrame, timeOffset=timeOffset, seqOffset=seqOffset, mute=False, shiftKey=True, head=head, tail=tail, timeslider=timeslider)
    


# from rf_maya.lib import sequencer_lib as seq
# reload(seq)
# shotName = mc.ls(sl=True)[0]
# head=False 
# tail=True
# value = 2
# # seq.extend_shot_seq(shotName, value, head=head, tail=tail, timeslider=True)
# seqTargetFrame=1041
# timeOffset=2
# seqOffset=2
# seq.extend_shot_seq(shotName, seqTargetFrame=1044, timeOffset=timeOffset, seqOffset=seqOffset, mute=False, shiftKey=True, head=head, tail=tail)