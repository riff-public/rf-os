# Riff Studio
# Handle preroll and postroll by using clip transition 
from PySide2 import QtWidgets
from PySide2 import QtCore
from PySide2 import QtGui
import os
import sys
from functools import partial 

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 

is_valid = True
try:
    from rf_maya.lib import sequencer_utils as seq
    from rf_maya.lib import camera_lib
    from rf_utils.context import context_info
    # reload(camera_lib)
    # reload(seq)
except:
    is_valid = False

def show(parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    mc.menuItem(l='Create New Shot', c=partial(create_shot))
    mc.menuItem(l='Create Multiple Shots', c=partial(create_multiple_shots))
    mc.menuItem(divider = True)
    mc.menuItem(l='Run Sequencer Manager', c=partial(run_sequencer_manager))


def create_shot(*args): 
    dialog = InputDialog(title='Create shot')
    dialog.result.connect(create_shot_cmd)
    dialog.exec_()


def create_multiple_shots(*args): 
    dialog = MultipleShotDialog(title='Create multiple shots')
    dialog.result.connect(create_multiple_shot_cmd)
    dialog.exec_()


def create_shot_cmd(input_dict): 
    entity = context_info.ContextPathInfo()
    project = entity.project
    shotName = input_dict['shotName']
    duration = input_dict['duration']
    shot_buffer = entity.projectInfo.scene.duration_shot or 100

    # create shsot 
    camera = camera_lib.create_camera(shotName, projectName=project)
    seq.create_after_last_shot(shotName, camera, duration=int(duration), shot_buffer=shot_buffer, round_number=False)


def create_multiple_shot_cmd(input_list): 
    entity = context_info.ContextPathInfo()
    project = entity.project

    for input_dict in input_list: 
        shotName = input_dict['shotName']
        duration = input_dict['duration']
        shot_buffer = input_dict['space']
        # shot_buffer = entity.projectInfo.scene.duration_shot or 100

        # create shsot 
        camera = camera_lib.create_camera(shotName, projectName=project)
        seq.create_after_last_shot(shotName, camera, duration=int(duration), shot_buffer=shot_buffer, round_number=False)


def run_sequencer_manager(*args): 
    from rf_app.layout.sequencer_manager import app
    reload(app)
    reload(app.ui)
    reload(app.controls_program)
    app.show()


class InputDialog(QtWidgets.QDialog):
    """docstring for InputDialog"""
    result = QtCore.Signal(dict)

    def __init__(self, parent=None, title=None):
        super(InputDialog, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.gridLayout = QtWidgets.QGridLayout()
        self.buttonLayout = QtWidgets.QHBoxLayout()


        self.name_label = QtWidgets.QLabel('Shot Name: ')
        self.name_lineEdit = QtWidgets.QLineEdit()
        self.name_lineEdit.setValidator(QtGui.QRegExpValidator(QtCore.QRegExp("s[0-9][0-9][0-9][0-9][A-Za-z]*$")))

        self.duration_label = QtWidgets.QLabel('Duration: ')
        self.duration_lineEdit = QtWidgets.QLineEdit()
        self.duration_lineEdit.setValidator(QtGui.QIntValidator())

        self.ok_button = QtWidgets.QPushButton('Create')
        self.ok_button.setMinimumSize(QtCore.QSize(0, 26))
        self.ok_button.setDefault(True)  # make button pressed when Enter key is pressed

        self.cancel_button = QtWidgets.QPushButton('Cancel')
        self.cancel_button.setMinimumSize(QtCore.QSize(0, 26))

        self.gridLayout.addWidget(self.name_label, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.name_lineEdit, 0, 1, 1, 1)
        self.gridLayout.addWidget(self.duration_label, 1,0 , 1, 1)
        self.gridLayout.addWidget(self.duration_lineEdit, 1, 1, 1, 1)

        self.buttonLayout.addWidget(self.ok_button)
        self.buttonLayout.addWidget(self.cancel_button)

        self.layout.addLayout(self.gridLayout)
        self.layout.addLayout(self.buttonLayout)
        self.setLayout(self.layout)
        self.resize(260, 100)
        self.setWindowTitle(title)

        self.cancel_button.clicked.connect(self.close)
        self.ok_button.clicked.connect(self.submit)

    def submit(self): 
        self.result.emit({'shotName': self.name_lineEdit.text(), 'duration': self.duration_lineEdit.text()})
        self.close()


class MultipleShotDialog(QtWidgets.QDialog):
    """docstring for MultipleShotDialog"""
    result = QtCore.Signal(dict)

    def __init__(self, parent=None, title=None):
        super(MultipleShotDialog, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.gridLayout = QtWidgets.QGridLayout()
        self.buttonLayout = QtWidgets.QHBoxLayout()

        self.number_label = QtWidgets.QLabel('Number of Shots: ')
        self.number_lineEdit = QtWidgets.QLineEdit()
        self.number_lineEdit.setText('10')
        self.number_lineEdit.setValidator(QtGui.QIntValidator())


        self.start_label = QtWidgets.QLabel('Start from: ')
        self.start_from_lineEdit = QtWidgets.QLineEdit()
        self.start_from_lineEdit.setText('s0010')
        self.start_from_lineEdit.setEnabled(False)
        self.start_from_lineEdit.setValidator(QtGui.QRegExpValidator(QtCore.QRegExp("s[0-9][0-9][0-9][0-9][A-Za-z]*$")))

        self.duration_label = QtWidgets.QLabel('Duration: ')
        self.duration_lineEdit = QtWidgets.QLineEdit()
        self.duration_lineEdit.setText('100')
        # self.duration_lineEdit.setValidator(QtGui.QIntValidator())
        self.shot_space_label = QtWidgets.QLabel('Space: ')
        self.shot_space_lineEdit = QtWidgets.QLineEdit()
        self.shot_space_lineEdit.setText('100')

        self.ok_button = QtWidgets.QPushButton('Create')
        self.ok_button.setMinimumSize(QtCore.QSize(0, 26))
        self.ok_button.setDefault(True)  # make button pressed when Enter key is pressed

        self.cancel_button = QtWidgets.QPushButton('Cancel')
        self.cancel_button.setMinimumSize(QtCore.QSize(0, 26))

        self.gridLayout.addWidget(self.number_label, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.number_lineEdit, 0, 1, 1, 1)
        self.gridLayout.addWidget(self.start_label, 1, 0 , 1, 1)
        self.gridLayout.addWidget(self.start_from_lineEdit, 1, 1, 1, 1)
        self.gridLayout.addWidget(self.duration_label, 2, 0 , 1, 1)
        self.gridLayout.addWidget(self.duration_lineEdit, 2, 1 , 1, 1)
        self.gridLayout.addWidget(self.shot_space_label, 3, 0 , 1, 1)
        self.gridLayout.addWidget(self.shot_space_lineEdit, 3, 1, 1, 1)

        self.buttonLayout.addWidget(self.ok_button)
        self.buttonLayout.addWidget(self.cancel_button)

        self.layout.addLayout(self.gridLayout)
        self.layout.addLayout(self.buttonLayout)
        self.setLayout(self.layout)
        self.resize(260, 100)
        self.setWindowTitle(title)

        self.cancel_button.clicked.connect(self.close)
        self.ok_button.clicked.connect(self.submit)

    def submit(self): 
        number = int(self.number_lineEdit.text())
        start_shot = self.start_from_lineEdit.text()
        duration = int(self.duration_lineEdit.text())
        space = int(self.shot_space_lineEdit.text())
        input_list = list()
        pad = 10

        start_shot_strip = int(start_shot[1:])

        for i in range(start_shot_strip, (number * pad) + 1, pad): 
            shotnum = i
            padding = '%04d' % shotnum
            shotname = 's{}'.format(padding)
            # print 'shotname', number, shotname 
            # continue
            temp = {'shotName': shotname, 'duration': duration, 'space': space}
            input_list.append(temp)

        self.result.emit(input_list)
        self.close()



# from rf_maya.lib import sequencer_lib as seq
# reload(seq)
# shotName = mc.ls(sl=True)[0]
# head=False 
# tail=True
# value = 2
# # seq.extend_shot_seq(shotName, value, head=head, tail=tail, timeslider=True)
# seqTargetFrame=1041
# timeOffset=2
# seqOffset=2
# seq.extend_shot_seq(shotName, seqTargetFrame=1044, timeOffset=timeOffset, seqOffset=seqOffset, mute=False, shiftKey=True, head=head, tail=tail)