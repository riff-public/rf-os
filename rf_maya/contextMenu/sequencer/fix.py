# Riff Studio
# Handle preroll and postroll by using clip transition 

import os
import sys
from datetime import datetime
from functools import partial 
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 

is_valid = True
try:
    from rf_maya.lib import sequencer_lib as seq
    reload(seq)
except:
    is_valid = False

def show(parent=None):
    if not is_valid:
        return
    # Menu setting menu area
    mc.menuItem(l='Fix', subMenu=True)
    mc.menuItem(l='Relinked camera', c=partial(relink_camera))
    mc.setParent( '..', menu=True )


def relink_camera(*args): 
    shots = mc.ls(sl=True, type='shot')
    seq.relink_shot_camera(shots)
