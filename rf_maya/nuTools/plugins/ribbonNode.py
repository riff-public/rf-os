import sys
import maya.OpenMaya as om
import maya.OpenMayaMPx as ompx

nodeName = 'ribbon'

class Ribbon(ompx.MPxNode):
    kPluginNodeId = om.MTypeId(0x25053766)
    parentMatrix = om.MObject()
    baseIndex = om.MObject()

    def __init__(self):
        ompx.MPxNode.__init__(self)
        

    def deform(self, data, geoIter, matrix, geometry_index):
        
def nodeCreator():
    return ompx.asMPxPtr(Ribbon())

def nodeInitializer():
    tAttrFn = om.MFnTypedAttribute()
    nAttrFn = om.MFnNumericAttribute()
    mAttrFn = om.MFnMatrixAttribute()

    PartialBlendShape.parentMatrix = mAttrFn.create('parentMatrix', 'pm', om.MFnMatrixData.kMatrix)
    mAttrFn.setKeyable(False)
    mAttrFn.setStorable(False)
    mAttrFn.setHidden(False)
    mAttrFn.setConnectable(True)

    PartialBlendShape.baseIndex = nAttrFn.create('baseIndex', 'bi', om.MFnNumericData.kInt, -1)
    nAttrFn.setKeyable(False)
    nAttrFn.setStorable(True)
    nAttrFn.setHidden(True)
    nAttrFn.setConnectable(False)
    nAttrFn.setArray(True)
    nAttrFn.setUsesArrayDataBuilder(True)


    try:
        PartialBlendShape.addAttribute(PartialBlendShape.parentMatrix)
        PartialBlendShape.addAttribute(PartialBlendShape.baseIndex)
    
        PartialBlendShape.attributeAffects(PartialBlendShape.parentMatrix, outputGeom)
        PartialBlendShape.attributeAffects(PartialBlendShape.targets, outputGeom)

    except Exception, e:
        sys.stderr.write('Failed to create attributes for node:  %s\n' %nodeName)
        sys.stderr.write('%s\n' %e)


def initializePlugin(mobject):
    mplugin = ompx.MFnPlugin(mobject, 'Nuternativ', 'beta', 'Any')
    try:
        mplugin.registerNode(nodeName, typeId, 
            nodeCreator, nodeInitializer, ompx.MPxNode.kDeformerNode)
    except Exception, e:
        sys.stderr.write('Failed to register node:  %s\n' %nodeName)
        sys.stderr.write('%s\n' %e)


def uninitializePlugin(mobject):
    mplugin = ompx.MFnPlugin(mobject)
    try:
        mplugin.deregisterNode(typeId)
    except Exception, e:
        sys.stderr.write('Failed to deregister node:  %s\n' %nodeName)
        sys.stderr.write('%s\n' %e)


