import sys
import maya.OpenMaya as om
import maya.OpenMayaMPx as ompx

nodeName = 'partialBlendShape'
typeId = om.MTypeId(0x25050066)

DEFAULT_TOL = 0.0001
inputs = ompx.cvar.MPxGeometryFilter_input
inputGeom = ompx.cvar.MPxGeometryFilter_inputGeom
outputGeom = ompx.cvar.MPxGeometryFilter_outputGeom
envelope = ompx.cvar.MPxGeometryFilter_envelope

'''
TO DO:
- get it to work in world space
- 
'''

class PartialBlendShape(ompx.MPxDeformerNode):
    targets = om.MObject()
    baseIndex = om.MObject()
    pairIndex = om.MObject()
    pairVertex = om.MObject()
    numInfluence = om.MObject()
    targetWeights = om.MObject()
    tolerance = om.MObject()

    def __init__(self):
        ompx.MPxDeformerNode.__init__(self)
        self.tol = DEFAULT_TOL
        self.resultPoints = None
        # self.target_meshes = []
        # self.pair_index = []
        # self.pair_vertex = []

    def get_orig_mesh(self, data, geometry_index):
        input_attr_handle = data.outputArrayValue(inputs)
        input_attr_handle.jumpToElement(geometry_index)
        input_geom_handle = input_attr_handle.outputValue().child(inputGeom)
        orig_mesh = input_geom_handle.asMesh()
        return orig_mesh

    def get_target_meshes(self, data):
        target_attr_handle = data.inputArrayValue(self.targets)
        target_meshes = []
        for i in xrange(target_attr_handle.elementCount()):
            target_attr_handle.jumpToElement(i)
            target_mesh = target_attr_handle.inputValue().asMesh()
            target_meshes.append(target_mesh)
        return target_meshes

    def get_pair_data(self, base_points, target_points):
        # reset pair data
        base_vtx_count = base_points.length()

        # target_count = len(target_meshes)
        base_index = []
        pair_index = []   # [target_id, ...]
        pair_vertex = []  # [target_vtx, ...]
        

        # loop over each vertex on the base mesh
        for bi in xrange(base_vtx_count):
            # loop over each point in target mesh
            for ti, tgPoints in enumerate(target_points):
                found = False
                for pi in xrange(tgPoints.length()):
                    vec = tgPoints[pi] - base_points[bi]
                    if vec.length() <= self.tol:
                        base_index.append(bi)
                        pair_index.append(ti)
                        pair_vertex.append(pi)
                        found = True
                        break
                if found:
                    break

        # print 'ending'
        return base_index, pair_index, pair_vertex

    def readPairData(self, baseIndexHandle, pairIndexHandle, pairVertexHandle):
        base_index = []
        pair_index = []
        pair_vertex = []
        for i in xrange(baseIndexHandle.elementCount()):
            baseIndexHandle.jumpToElement(i)
            bIndx = baseIndexHandle.inputValue().asInt()
            base_index.append(bIndx)

            pairIndexHandle.jumpToElement(i)
            pIndx = pairIndexHandle.inputValue().asInt()
            pair_index.append(pIndx)

            pairVertexHandle.jumpToElement(i)
            pVtx = pairVertexHandle.inputValue().asInt()
            pair_vertex.append(pVtx)
        return base_index, pair_index, pair_vertex

    def deform(self, data, geoIter, matrix, geometry_index):
        # get envelope value
        envValue = data.inputValue(envelope).asFloat()
        
        # input mesh
        input_mesh = self.get_orig_mesh(data, geometry_index)

        # get target meshes
        target_meshes = self.get_target_meshes(data)
        if not target_meshes:
            return
        # print target_meshes
        numTargets = len(target_meshes)

        # get vertex count
        vert_count = geoIter.count()

        # get tolerance
        tolHandle = data.inputValue(PartialBlendShape.tolerance)
        tolValue = tolHandle.asDouble()

        # get numInfluence
        niHandle = data.inputValue(PartialBlendShape.numInfluence)
        numInf = niHandle.asInt()

        # pair data
        baseIndexHandle = data.inputArrayValue(PartialBlendShape.baseIndex)
        pairIndexHandle = data.inputArrayValue(PartialBlendShape.pairIndex)
        pairVertexHandle = data.inputArrayValue(PartialBlendShape.pairVertex)

        # read stored pair data attributes
        base_index, pair_index, pair_vertex = self.readPairData(baseIndexHandle, 
                                            pairIndexHandle, 
                                            pairVertexHandle)

        if envValue == 0.0:
            return

        # loop over each target mesh, get vertex positions
        target_points = []
        for ti, tg in enumerate(target_meshes):
            tgMeshFn = om.MFnMesh(tg)
            tgPointArray = om.MPointArray()
            tgMeshFn.getPoints(tgPointArray)
            target_points.append(tgPointArray)

        # if new connections to target attribute are made, or tolerance changed, get pair data
        if numInf != numTargets or tolValue != self.tol:
            print 'recomputing'
            # get input mesh vertex positions
            currPoints = om.MPointArray()
            inputMeshFn = om.MFnMesh(input_mesh)
            inputMeshFn.getPoints(currPoints, om.MSpace.kWorld)

            self.tol = tolValue
            base_index, pair_index, pair_vertex = self.get_pair_data(currPoints, target_points)
            numDeformed = len(base_index)

            # store pair data in the node
            biBuilder = om.MArrayDataBuilder(data, PartialBlendShape.baseIndex, numDeformed)
            piBuilder = om.MArrayDataBuilder(data, PartialBlendShape.pairIndex, numDeformed)
            pvBuilder = om.MArrayDataBuilder(data, PartialBlendShape.pairVertex, numDeformed)

            for i in xrange(numDeformed):
                biHandle = biBuilder.addElement(i)
                biHandle.setInt(base_index[i])

                piHandle = piBuilder.addElement(i)
                piHandle.setInt(pair_index[i])

                pvHandle = pvBuilder.addElement(i)
                pvHandle.setInt(pair_vertex[i])

            baseIndexHandle.set(biBuilder)
            pairIndexHandle.set(piBuilder)
            pairVertexHandle.set(pvBuilder)
            niHandle.setInt(numTargets)
            
        # print base_index, pair_index, pair_vertex
        d = 0
        self.resultPoints = om.MPointArray()
        while not geoIter.isDone():
            vid = geoIter.index()
            currPt = geoIter.position(om.MSpace.kObject)
            # currPt *= matrix
            # print currPt.x, currPt.y, currPt.z
            if vid in base_index:
                target_index = pair_index[d]
                target_vertex = pair_vertex[d]
                targetPt = target_points[target_index][target_vertex]
                # print 'tg %s %s %s' %(targetPt.x, targetPt.y, targetPt.z)
                currPt.x += (targetPt.x - currPt.x)  * envValue
                currPt.y += (targetPt.y - currPt.y)  * envValue
                currPt.z += (targetPt.z - currPt.z)  * envValue
                deformPt = currPt
                d += 1
            else:
                deformPt = om.MPoint(currPt)

            self.resultPoints.append(deformPt)
            geoIter.next()

        geoIter.setAllPositions(self.resultPoints)

def nodeCreator():
    return ompx.asMPxPtr(PartialBlendShape())

def nodeInitializer():
    tAttrFn = om.MFnTypedAttribute()
    nAttrFn = om.MFnNumericAttribute()

    PartialBlendShape.targets = tAttrFn.create('targets', 'tg', om.MFnGeometryData.kMesh)
    tAttrFn.setKeyable(False)
    tAttrFn.setStorable(False)
    tAttrFn.setHidden(False)
    tAttrFn.setConnectable(True)
    tAttrFn.setArray(True) 

    # PartialBlendShape.targetMatrix = tAttrFn.create('targets', 'tg', om.MFnGeometryData.kMesh)
    # tAttrFn.setKeyable(False)
    # tAttrFn.setStorable(False)
    # tAttrFn.setHidden(False)
    # tAttrFn.setConnectable(True)
    # tAttrFn.setArray(True) 

    PartialBlendShape.baseIndex = nAttrFn.create('baseIndex', 'bi', om.MFnNumericData.kInt, -1)
    nAttrFn.setKeyable(False)
    nAttrFn.setStorable(True)
    nAttrFn.setHidden(True)
    nAttrFn.setConnectable(False)
    nAttrFn.setArray(True)
    nAttrFn.setUsesArrayDataBuilder(True)

    PartialBlendShape.pairIndex = nAttrFn.create('pairIndex', 'pi', om.MFnNumericData.kInt, -1)
    nAttrFn.setKeyable(False)
    nAttrFn.setStorable(True)
    nAttrFn.setHidden(True)
    nAttrFn.setConnectable(False)
    nAttrFn.setArray(True)
    nAttrFn.setUsesArrayDataBuilder(True)

    PartialBlendShape.pairVertex = nAttrFn.create('pairVertex', 'pv', om.MFnNumericData.kInt, -1)
    nAttrFn.setKeyable(False)
    nAttrFn.setStorable(True)
    nAttrFn.setHidden(True)
    nAttrFn.setConnectable(False)
    nAttrFn.setArray(True)
    nAttrFn.setUsesArrayDataBuilder(True)

    PartialBlendShape.numInfluence = nAttrFn.create('numInfluence', 'ni', om.MFnNumericData.kInt, 0)
    nAttrFn.setKeyable(False)
    nAttrFn.setStorable(True)
    nAttrFn.setHidden(True)
    nAttrFn.setConnectable(False)

    PartialBlendShape.targetWeights = nAttrFn.create('targetWeights', 'tw', om.MFnNumericData.kFloat, 0.0)
    nAttrFn.setKeyable(True)
    nAttrFn.setStorable(True)
    nAttrFn.setHidden(False)
    nAttrFn.setConnectable(True)
    nAttrFn.setArray(True) 

    PartialBlendShape.tolerance = nAttrFn.create('tolerance', 'tol', om.MFnNumericData.kDouble, DEFAULT_TOL)
    nAttrFn.setMin(0.0)
    # nAttrFn.setMax(9999999.0)
    nAttrFn.setKeyable(True)
    nAttrFn.setStorable(True)
    nAttrFn.setHidden(False)

    try:
        PartialBlendShape.addAttribute(PartialBlendShape.targets)
        PartialBlendShape.addAttribute(PartialBlendShape.baseIndex)
        PartialBlendShape.addAttribute(PartialBlendShape.pairIndex)
        PartialBlendShape.addAttribute(PartialBlendShape.pairVertex)
        PartialBlendShape.addAttribute(PartialBlendShape.numInfluence)
        PartialBlendShape.addAttribute(PartialBlendShape.targetWeights)
        PartialBlendShape.addAttribute(PartialBlendShape.tolerance)
        
        # PartialBlendShape.attributeAffects(PartialBlendShape.baseIndex, outputGeom)
        # PartialBlendShape.attributeAffects(PartialBlendShape.pairIndex, outputGeom)
        # PartialBlendShape.attributeAffects(PartialBlendShape.pairVertex, outputGeom)
        PartialBlendShape.attributeAffects(PartialBlendShape.targets, outputGeom)
        PartialBlendShape.attributeAffects(PartialBlendShape.targetWeights, outputGeom)
        PartialBlendShape.attributeAffects(PartialBlendShape.tolerance, outputGeom)

    except Exception, e:
        sys.stderr.write('Failed to create attributes for node:  %s\n' %nodeName)
        sys.stderr.write('%s\n' %e)


def initializePlugin(mobject):
    mplugin = ompx.MFnPlugin(mobject, 'Nuternativ', 'beta', 'Any')
    try:
        mplugin.registerNode(nodeName, typeId, 
            nodeCreator, nodeInitializer, ompx.MPxNode.kDeformerNode)
    except Exception, e:
        sys.stderr.write('Failed to register node:  %s\n' %nodeName)
        sys.stderr.write('%s\n' %e)


def uninitializePlugin(mobject):
    mplugin = ompx.MFnPlugin(mobject)
    try:
        mplugin.deregisterNode(typeId)
    except Exception, e:
        sys.stderr.write('Failed to deregister node:  %s\n' %nodeName)
        sys.stderr.write('%s\n' %e)



'''
import pymel.core as pm
import maya.cmds as mc
import maya.OpenMaya as om 
import maya.mel as mel

mel.eval('flushUndo')
try:
    pm.unloadPlugin("partialBlendShape.py")
except: 
    pass
pm.loadPlugin(r"D:\dev\core\rf_maya\nuTools\plugins\partialBlendShape.py")

bTr, baseMesh = pm.polyCube()
tg1Tr = pm.polyCube()[0]
tg2Tr = pm.polyCube()[0]
pm.delete((tg1Tr.f[0:1], tg1Tr.f[3:5]))
pm.delete(tg2Tr.f[1:5])
# pm.move(tg1Tr, (-3, 0, 0))
# pm.move(tg2Tr, (-6, 0 ,0 ))
pm.select(bTr)
node = pm.deformer(type='partialBlendShape')[0]

pm.connectAttr(tg1Tr.getShape().outMesh, node.targets[0])
pm.connectAttr(tg2Tr.getShape().outMesh, node.targets[1])


node = pm.PyNode('partialBlendShape1')
pm.connectAttr(pm.PyNode('pCube3').getShape().outMesh, node.targets[1])
'''