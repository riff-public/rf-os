import sys
import maya.OpenMaya as om
import maya.OpenMayaMPx as ompx

nodeName = 'partialBlendShape'
typeId = om.MTypeId(0x25050066)

DEFAULT_TOL = 0.0001
inputs = ompx.cvar.MPxGeometryFilter_input
inputGeom = ompx.cvar.MPxGeometryFilter_inputGeom
outputGeom = ompx.cvar.MPxGeometryFilter_outputGeom
envelope = ompx.cvar.MPxGeometryFilter_envelope

'''
TO DO:
//- get it to work in world space
- target base connections (get rid of calculate attr)
- accessory connections
'''

class PartialBlendShape(ompx.MPxDeformerNode):
    parentMatrix = om.MObject()
    targets = om.MObject()
    targetBases = om.MObject()

    baseIndex = om.MObject()
    pairIndex = om.MObject()
    pairVertex = om.MObject()

    targetWeights = om.MObject()
    tolerance = om.MObject()
    calculate = om.MObject()

    def __init__(self):
        ompx.MPxDeformerNode.__init__(self)
        # self.__initialized = False
        # self.tol = DEFAULT_TOL
        # self.resultPoints = None
        # self.target_meshes = []
        # self.pair_index = []
        # self.pair_vertex = []

    def get_orig_mesh(self, data, geometry_index):
        input_attr_handle = data.outputArrayValue(inputs)
        input_attr_handle.jumpToElement(geometry_index)
        input_geom_handle = input_attr_handle.outputValue().child(inputGeom)
        orig_mesh = input_geom_handle.asMesh()
        return orig_mesh

    def get_target_meshes(self, data):
        target_attr_handle = data.inputArrayValue(PartialBlendShape.targets)
        target_meshes = []
        for i in xrange(target_attr_handle.elementCount()):
            target_attr_handle.jumpToElement(i)
            target_mesh = target_attr_handle.inputValue().asMesh()
            if target_mesh.isNull():
                continue
            target_meshes.append(target_mesh)
        return target_meshes

    def get_base_meshes(self, data):
        base_attr_handle = data.inputArrayValue(PartialBlendShape.targetBases)
        base_meshes = []
        for i in xrange(base_attr_handle.elementCount()):
            base_attr_handle.jumpToElement(i)
            base_mesh = base_attr_handle.inputValue().asMesh()
            if base_mesh.isNull():
                continue
            base_meshes.append(base_mesh)
        return base_meshes

    def read_pair_data(self, data):
        baseIndexHandle = data.outputArrayValue(PartialBlendShape.baseIndex)
        pairIndexHandle = data.outputArrayValue(PartialBlendShape.pairIndex)
        pairVertexHandle = data.outputArrayValue(PartialBlendShape.pairVertex)

        base_index = []
        pair_index = []
        pair_vertex = []
        for i in xrange(baseIndexHandle.elementCount()):
            baseIndexHandle.jumpToElement(i)
            bIndx = baseIndexHandle.inputValue().asInt()
            base_index.append(bIndx)

            pairIndexHandle.jumpToElement(i)
            pIndx = pairIndexHandle.inputValue().asInt()
            pair_index.append(pIndx)

            pairVertexHandle.jumpToElement(i)
            pVtx = pairVertexHandle.inputValue().asInt()
            pair_vertex.append(pVtx)

        return base_index, pair_index, pair_vertex

    def calculate_pairs(self, data, geometry_index, target_points, boundingBoxs):
        baseIndexHandle = data.outputArrayValue(PartialBlendShape.baseIndex)
        pairIndexHandle = data.outputArrayValue(PartialBlendShape.pairIndex)
        pairVertexHandle = data.outputArrayValue(PartialBlendShape.pairVertex)

        # get tolerance
        tolHandle = data.inputValue(PartialBlendShape.tolerance)
        tolValue = tolHandle.asDouble()

        # input mesh
        input_mesh = self.get_orig_mesh(data, geometry_index)

        # get input mesh vertex positions
        currPoints = om.MPointArray()
        inputMeshFn = om.MFnMesh(input_mesh)
        inputMeshFn.getPoints(currPoints, om.MSpace.kWorld)

        # reset pair data
        input_vtx_count = currPoints.length()

        # store pair data in the node
        biBuilder = om.MArrayDataBuilder(data, PartialBlendShape.baseIndex, 0)
        piBuilder = om.MArrayDataBuilder(data, PartialBlendShape.pairIndex, 0)
        pvBuilder = om.MArrayDataBuilder(data, PartialBlendShape.pairVertex, 0)

        # loop over each vertex on the base mesh
        for bi in xrange(input_vtx_count):
            currPt = currPoints[bi]
            # loop over each point in target mesh
            for ti, tgPoints in enumerate(target_points):
                
                found = False
                # mi = boundingBoxs[ti].min()
                # mx = boundingBoxs[ti].max()
                # print mi.x, mi.y, mi.z, mx.x, mx.y, mx.z
                if boundingBoxs[ti].contains(currPt):
                    
                    for pi in xrange(tgPoints.length()):
                        vec = tgPoints[pi] - currPt
                        if vec.length() <= tolValue:
                            biHandle = biBuilder.addLast()
                            biHandle.setInt(bi)

                            piHandle = piBuilder.addLast()
                            piHandle.setInt(ti)

                            pvHandle = pvBuilder.addLast()
                            pvHandle.setInt(pi)

                            found = True
                            break
                if found:
                    break

        baseIndexHandle.set(biBuilder)
        pairIndexHandle.set(piBuilder)
        pairVertexHandle.set(pvBuilder)

    def deform(self, data, geoIter, matrix, geometry_index):
        # --- get target meshes, if no target connects, just return
        target_meshes = self.get_target_meshes(data)
        if not target_meshes:
            return

        # print boundingBoxs
        # --- if calculate switch is on, calculate pair data and store in node
        calculateValue = data.inputValue(PartialBlendShape.calculate).asBool()
        if calculateValue:
            base_meshes = self.get_base_meshes(data)
            if not base_meshes or len(target_meshes) != len(base_meshes):
                return
            base_points = []
            boundingBoxs = []
            for bs in base_meshes:
                bsMeshFn = om.MFnMesh(bs)
                bsPointArray = om.MPointArray()
                bsMeshFn.getPoints(bsPointArray, om.MSpace.kWorld)
                base_points.append(bsPointArray)

                # get bb
                bsBB = om.MBoundingBox()
                for i in xrange(bsPointArray.length()):
                    point = bsPointArray[i]
                    bsBB.expand(point)
 
                boundingBoxs.append(bsBB)
            self.calculate_pairs(data, geometry_index, base_points, boundingBoxs)

        # --- get envelope value
        envValue = data.inputValue(envelope).asFloat()
        # if envelope is turned off, return
        if envValue == 0.0:
            return

        # --- loop over each target mesh, get vertex positions
        target_points = []
        for tg in target_meshes:
            tgMeshFn = om.MFnMesh(tg)
            tgPointArray = om.MPointArray()
            tgMeshFn.getPoints(tgPointArray, om.MSpace.kWorld)
            target_points.append(tgPointArray)

        # --- read stored pair data attributes
        base_index, pair_index, pair_vertex = self.read_pair_data(data)
        
        # --- get parent matrix
        parentMatrix = data.inputValue(PartialBlendShape.parentMatrix).asMatrix()

        # --- deform the points
        resultPoints = om.MPointArray()
        d = 0
        # printed = False
        while not geoIter.isDone():
            vid = geoIter.index()
            
            # print currPt.x, currPt.y, currPt.z
            if vid in base_index:
                target_index = pair_index[d]
                target_vertex = pair_vertex[d]
                targetPt = target_points[target_index][target_vertex]
                currPt = geoIter.position() * parentMatrix
                currPt.x += (targetPt.x - currPt.x)  * envValue
                currPt.y += (targetPt.y - currPt.y)  * envValue
                currPt.z += (targetPt.z - currPt.z)  * envValue
                deformPt = currPt
                d += 1
            else:
                deformPt = geoIter.position()
                # if not printed:
                #     print vid, deformPt.x, deformPt.y, deformPt.z
                #     printed = True
            resultPoints.append(deformPt)
            geoIter.next()

        geoIter.setAllPositions(resultPoints)

def nodeCreator():
    return ompx.asMPxPtr(PartialBlendShape())

def nodeInitializer():
    tAttrFn = om.MFnTypedAttribute()
    nAttrFn = om.MFnNumericAttribute()
    mAttrFn = om.MFnMatrixAttribute()

    PartialBlendShape.parentMatrix = mAttrFn.create('parentMatrix', 'pm', om.MFnMatrixData.kMatrix)
    mAttrFn.setKeyable(False)
    mAttrFn.setStorable(False)
    mAttrFn.setHidden(False)
    mAttrFn.setConnectable(True)

    PartialBlendShape.targets = tAttrFn.create('targets', 'tg', om.MFnGeometryData.kMesh)
    tAttrFn.setKeyable(False)
    tAttrFn.setStorable(False)
    tAttrFn.setHidden(False)
    tAttrFn.setConnectable(True)
    tAttrFn.setArray(True)

    PartialBlendShape.targetBases = tAttrFn.create('targetBases', 'tb', om.MFnGeometryData.kMesh)
    tAttrFn.setKeyable(False)
    tAttrFn.setStorable(False)
    tAttrFn.setHidden(False)
    tAttrFn.setConnectable(True)
    tAttrFn.setArray(True)

    PartialBlendShape.baseIndex = nAttrFn.create('baseIndex', 'bi', om.MFnNumericData.kInt, -1)
    nAttrFn.setKeyable(False)
    nAttrFn.setStorable(True)
    nAttrFn.setHidden(True)
    nAttrFn.setConnectable(False)
    nAttrFn.setArray(True)
    nAttrFn.setUsesArrayDataBuilder(True)

    PartialBlendShape.pairIndex = nAttrFn.create('pairIndex', 'pi', om.MFnNumericData.kInt, -1)
    nAttrFn.setKeyable(False)
    nAttrFn.setStorable(True)
    nAttrFn.setHidden(True)
    nAttrFn.setConnectable(False)
    nAttrFn.setArray(True)
    nAttrFn.setUsesArrayDataBuilder(True)

    PartialBlendShape.pairVertex = nAttrFn.create('pairVertex', 'pv', om.MFnNumericData.kInt, -1)
    nAttrFn.setKeyable(False)
    nAttrFn.setStorable(True)
    nAttrFn.setHidden(True)
    nAttrFn.setConnectable(False)
    nAttrFn.setArray(True)
    nAttrFn.setUsesArrayDataBuilder(True)

    PartialBlendShape.targetWeights = nAttrFn.create('targetWeights', 'tw', om.MFnNumericData.kFloat, 0.0)
    nAttrFn.setKeyable(True)
    nAttrFn.setStorable(True)
    nAttrFn.setHidden(False)
    nAttrFn.setConnectable(True)
    nAttrFn.setArray(True) 

    PartialBlendShape.tolerance = nAttrFn.create('tolerance', 'tol', om.MFnNumericData.kDouble, DEFAULT_TOL)
    nAttrFn.setMin(0.0)
    # nAttrFn.setMax(9999999.0)
    nAttrFn.setKeyable(True)
    nAttrFn.setStorable(True)
    nAttrFn.setHidden(False)

    PartialBlendShape.calculate = nAttrFn.create('calculate', 'c', om.MFnNumericData.kBoolean, False)
    nAttrFn.setKeyable(True)
    nAttrFn.setStorable(True)
    nAttrFn.setHidden(False)
    nAttrFn.setConnectable(False)

    try:
        PartialBlendShape.addAttribute(PartialBlendShape.parentMatrix)
        PartialBlendShape.addAttribute(PartialBlendShape.targets)
        PartialBlendShape.addAttribute(PartialBlendShape.targetBases)
        PartialBlendShape.addAttribute(PartialBlendShape.baseIndex)
        PartialBlendShape.addAttribute(PartialBlendShape.pairIndex)
        PartialBlendShape.addAttribute(PartialBlendShape.pairVertex)
        PartialBlendShape.addAttribute(PartialBlendShape.targetWeights)
        PartialBlendShape.addAttribute(PartialBlendShape.tolerance)
        PartialBlendShape.addAttribute(PartialBlendShape.calculate)
        
        PartialBlendShape.attributeAffects(PartialBlendShape.parentMatrix, outputGeom)
        PartialBlendShape.attributeAffects(PartialBlendShape.targets, outputGeom)
        PartialBlendShape.attributeAffects(PartialBlendShape.targetBases, outputGeom)
        PartialBlendShape.attributeAffects(PartialBlendShape.targetWeights, outputGeom)
        PartialBlendShape.attributeAffects(PartialBlendShape.tolerance, outputGeom)
        PartialBlendShape.attributeAffects(PartialBlendShape.calculate, outputGeom)

    except Exception, e:
        sys.stderr.write('Failed to create attributes for node:  %s\n' %nodeName)
        sys.stderr.write('%s\n' %e)


def initializePlugin(mobject):
    mplugin = ompx.MFnPlugin(mobject, 'Nuternativ', 'beta', 'Any')
    try:
        mplugin.registerNode(nodeName, typeId, 
            nodeCreator, nodeInitializer, ompx.MPxNode.kDeformerNode)
    except Exception, e:
        sys.stderr.write('Failed to register node:  %s\n' %nodeName)
        sys.stderr.write('%s\n' %e)


def uninitializePlugin(mobject):
    mplugin = ompx.MFnPlugin(mobject)
    try:
        mplugin.deregisterNode(typeId)
    except Exception, e:
        sys.stderr.write('Failed to deregister node:  %s\n' %nodeName)
        sys.stderr.write('%s\n' %e)



'''
# P:/SevenChickMovie/asset/publ/char/frederick/model/main/hero/output/frederick_mdl_main_md.hero.abc

import pymel.core as pm
import maya.cmds as mc
import maya.OpenMaya as om 
import maya.mel as mel

mel.eval('flushUndo')
try:
    pm.unloadPlugin("partialBlendShape.py")
except: 
    pass
pm.loadPlugin(r"D:\dev\core\rf_maya\nuTools\plugins\partialBlendShape.py")

bTr, baseMesh = pm.polyCube()
# pm.move(bTr, (0, 1, 0))
tg1Tr = pm.polyCube()[0]
tg2Tr = pm.polyCube()[0]
pm.delete((tg1Tr.f[0:1], tg1Tr.f[3:5]))
pm.delete(tg2Tr.f[1:5])
tg1Bs = tg1Tr.duplicate()[0]
tg1Bs.v.set(False)
tg2Bs = tg2Tr.duplicate()[0]
tg2Bs.v.set(False)
# pm.move(tg1Tr, (1, 0, 0))
# pm.move(tg2Tr, (-6, 0 ,0 ))
pm.select(bTr)
node = pm.deformer(type='partialBlendShape')[0]

pm.connectAttr(bTr.worldMatrix[0], node.parentMatrix)

pm.connectAttr(tg1Tr.getShape().worldMesh, node.targets[0])
pm.connectAttr(tg2Tr.getShape().worldMesh, node.targets[1])
pm.connectAttr(tg1Bs.getShape().worldMesh, node.targetBases[0])
pm.connectAttr(tg2Bs.getShape().worldMesh, node.targetBases[1])

node.calculate.set(True)

################################
mel.eval('flushUndo')
try:
    pm.unloadPlugin("partialBlendShape.py")
except: 
    pass
pm.loadPlugin(r"D:\dev\core\rf_maya\nuTools\plugins\partialBlendShape.py")

baseGeo = pm.PyNode('ThornMain_Geo')
node = pm.deformer(baseGeo, type='partialBlendShape')[0]
pm.connectAttr(baseGeo.worldMatrix[0], node.parentMatrix)
for i, o in enumerate(objs):
    ob = o.duplicate()[0]
    ob.v.set(False)
    

    pm.connectAttr(o.getShape().worldMesh, node.targets[i])
    pm.connectAttr(ob.getShape().worldMesh, node.targetBases[i])

node.calculate.set(True)

# node = pm.PyNode('partialBlendShape1')
# pm.connectAttr(pm.PyNode('pSphere2').getShape().outMesh, node.targets[0])
# pm.connectAttr(pm.PyNode('pSphere3').getShape().outMesh, node.targets[1])



'''