import maya.OpenMaya as om
import maya.OpenMayaAnim as oma
import maya.OpenMayaMPx as ompx

kPluginCmdName = "setMeshVertex"
kPointsFlag = "-p"
kPointsLongFlag = "-points"

# command
class SetMeshVertexCmd(ompx.MPxCommand):
    def __init__(self):
        ompx.MPxCommand.__init__(self)
        self.origPoints = om.MPointArray()
        self.points = om.MPointArray()
        self.meshFn = None
        self.numVtx = 0

        self.__isQueryUsed = True

    def isUndoable(self):
        return not self.__isQueryUsed

    def undoIt(self):
        if self.meshFn and self.origPoints.length() == self.meshFn.numVertices():
            self.meshFn.setPoints(self.origPoints, om.MSpace.kObject)

    def doIt(self, args):
        # parse the arguments
        argData = om.MArgDatabase(self.syntax(), args)
        selList = om.MSelectionList()
        argData.getObjects(selList)

        # get meshFn
        mDag = om.MDagPath()
        try:
            selList.getDagPath(0, mDag)
            self.meshFn = om.MFnMesh(mDag)
        except RuntimeError:
            om.MGlobal.displayError('Select a polygon object.')
            return

        self.numVtx = self.meshFn.numVertices()
        if argData.isQuery():
            pts = om.MPointArray()
            self.meshFn.getPoints(pts, om.MSpace.kObject)

            ptStrArgs = []
            for i in xrange(self.numVtx):
                pt = om.MPoint()
                self.meshFn.getPoint(i, pt, om.MSpace.kObject)
                ptStr = '%s %s %s' %(pt.x, pt.y, pt.z)
                ptStrArgs.append(ptStr)

            arg = ','.join(ptStrArgs)
            self.setResult(arg)
        else:
            self.__isQueryUsed = False
            if argData.isFlagSet(kPointsFlag):
                pointFlagStr = argData.flagArgumentString(kPointsFlag, 0)
                pointStrs = pointFlagStr.split(',')
                for ps in pointStrs:
                    values = ps.split(' ')
                    try:
                        mPt = om.MPoint(float(values[0]), float(values[1]), float(values[2]))
                    except ValueError:
                        om.MGlobal.displayError('Invalid -points argrument.')
                        return
                    self.points.append(mPt)

            if self.points.length() == self.numVtx:
                self.meshFn.getPoints(self.origPoints, om.MSpace.kObject)
                self.redoIt()

    def redoIt(self):
        self.meshFn.setPoints(self.points, om.MSpace.kObject)

# Creator
def cmdCreator():
    # Create the command
    return ompx.asMPxPtr(SetMeshVertexCmd())

# Syntax creator
def syntaxCreator():
    syntax = om.MSyntax()
    syntax.useSelectionAsDefault(True)
    syntax.setObjectType(om.MSyntax.kSelectionList, 0)
    syntax.enableQuery()
    syntax.addFlag(kPointsFlag, kPointsLongFlag, om.MSyntax.kString)
    return syntax

# Initialize the script plug-in
def initializePlugin(mobject):
    mplugin = ompx.MFnPlugin(mobject, "Nuternativ", "1.0", "Any")
    try:
        mplugin.registerCommand(kPluginCmdName, cmdCreator, syntaxCreator)
    except:
        sys.stderr.write( "Failed to register command: %s\n" % kPluginCmdName )
        raise

# Uninitialize the script plug-in
def uninitializePlugin(mobject):
    mplugin = ompx.MFnPlugin(mobject)
    try:
        mplugin.deregisterCommand(kPluginCmdName)
    except:
        sys.stderr.write( "Failed to unregister command: %s\n" % kPluginCmdName )
        raise

    
