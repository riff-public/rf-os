import maya.OpenMaya as om
import maya.OpenMayaAnim as oma
import maya.OpenMayaMPx as ompx

kPluginCmdName = "resetTweak"

# command
class ResetTweakCmd(ompx.MPxCommand):
    def __init__(self):
        ompx.MPxCommand.__init__(self)
        self.oldValues = {}
        self.fnTweak = None
        self.vertexPlug = None
        self.selection = om.MSelectionList()

    def isUndoable(self):
        return True

    def undoIt(self):
        for i, value in self.oldValues.iteritems():
            viPlug = self.vertexPlug.elementByLogicalIndex(i)
            xPlug = viPlug.child(0)
            yPlug = viPlug.child(1)
            zPlug = viPlug.child(2)
            xPlug.setFloat(value[0])
            yPlug.setFloat(value[1])
            zPlug.setFloat(value[2])

    def doIt(self, args):
        om.MGlobal.getActiveSelectionList(self.selection)

        if self.selection.isEmpty():
            argData = om.MArgDatabase(self.syntax(), args)
            argData.getObjects(self.selection)
        if self.selection:
            self.redoIt()

    def redoIt(self):       
        # get dag path for selection
        dagPath = om.MDagPath()
        try:
            self.selection.getDagPath(0, dagPath)
            dagPath.extendToShape()
        except RuntimeError, e:
            print e
            om.MGlobal.displayError('Please select a skinned mesh.')
            return
        
        itTweak = om.MItDependencyGraph(dagPath.node(), 
                                        om.MFn.kTweak, 
                                        om.MItDependencyGraph.kUpstream, 
                                        om.MItDependencyGraph.kBreadthFirst, 
                                        om.MItDependencyGraph.kPlugLevel)

        while not itTweak.isDone():
            tweakMObj = itTweak.currentItem()
            self.fnTweak = om.MFnDependencyNode(tweakMObj)
            break

        # check for skinCluster
        if not self.fnTweak:
            om.MGlobal.displayError('Cannot find tweak node.')
            return

        vlistPlug = self.fnTweak.findPlug('vlist')
        vlistPlug = vlistPlug.elementByLogicalIndex(0)
        self.vertexPlug = vlistPlug.child(0)
        intArray = om.MIntArray()
        self.vertexPlug.getExistingArrayAttributeIndices(intArray)
        for i in intArray:
            viPlug = self.vertexPlug.elementByLogicalIndex(i)
            xPlug = viPlug.child(0)
            yPlug = viPlug.child(1)
            zPlug = viPlug.child(2)

            # store old values
            oldX = xPlug.asFloat()
            oldY = yPlug.asFloat()
            oldZ = zPlug.asFloat()

            self.oldValues[i] = (oldX, oldY, oldZ)
            xPlug.setFloat(0.0)
            yPlug.setFloat(0.0)
            zPlug.setFloat(0.0)


# Creator
def cmdCreator():
    # Create the command
    return ompx.asMPxPtr(ResetTweakCmd())

# Syntax creator
def syntaxCreator():
    syntax = om.MSyntax()
    syntax.setObjectType(om.MSyntax.kSelectionList, 0)
    return syntax

# Initialize the script plug-in
def initializePlugin(mobject):
    mplugin = ompx.MFnPlugin(mobject, "Nuternativ", "1.0", "Any")
    try:
        mplugin.registerCommand(kPluginCmdName, cmdCreator, syntaxCreator)
    except:
        sys.stderr.write( "Failed to register command: %s\n" % kPluginCmdName )
        raise

# Uninitialize the script plug-in
def uninitializePlugin(mobject):
    mplugin = ompx.MFnPlugin(mobject)
    try:
        mplugin.deregisterCommand(kPluginCmdName)
    except:
        sys.stderr.write( "Failed to unregister command: %s\n" % kPluginCmdName )
        raise

    
