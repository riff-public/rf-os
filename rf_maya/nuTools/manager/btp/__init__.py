import pymel.core as pm
from nuTools import misc, ui

class BshTarget(object):
	def __init__(self, origObj, nameDic, partName, partGrp, position, gap):
		self.origObj = origObj
		self.nameDic = nameDic
		self.partName = partName
		self.partGrp = partGrp

		self.gap = gap
		self.x = position[0]
		self.y = position[1]
		self.z = position[2]

		self.obj = {'p':None, 'n':None, 'txt':[]}

	def report(self, pObj, cObj, pAttr, cAttr):
		source = misc.addMsgAttr(pObj, pAttr)
		destination = misc.addMsgAttr(cObj, cAttr)
		if pm.isConnected(source, destination) == False:
			pm.connectAttr(source, destination, f=True)

	def create(self):

		i = 0
		b = 0
		#get width
		bb = self.origObj.getBoundingBox()
		width = abs(bb[1][0] - bb[0][0])
		self.meshWidth = width * b
		#if there's only positive target move to x 1 gap
		if self.nameDic['p'] and not self.nameDic['n']:
			if self.x > self.gap * 2:
				self.x += self.gap
		for k, v in sorted(self.nameDic.iteritems()):
			self.x += self.meshWidth + (self.gap*i)
			b = 0
			inbs = []
			#if there's only positive value
			if not v:
				continue
			if not isinstance(v, (list, tuple)):
				v = [v]

			#inbs
			targets = []
			for t in v:
				target = (self.origObj.duplicate(n=t)[0])
				self.removeUnusedAttr(target)
				self.meshWidth = width*b
				pm.xform(target, ws=True, t=[self.x+self.meshWidth, self.y, self.z])
				targets.append(target)
				b += 1	
			

			self.obj[k] = targets

			#create text curves
			self.createText(targets[-1], v[-1])
			#report
			self.report(self.partGrp, targets[-1], self.partName, k)
			self.report(targets[-1], self.obj['txt'][i], 'TXT', 'GEO')

			#report inb
			numInbs = len(targets)
			if numInbs > 1:
				inbs = targets[0:-1]
				n = 1
				for obj in inbs:
					nStr = str(n)
					self.report(targets[-1], obj, 'INB', 'inb_%s' %nStr.zfill(2))
					n += 1

			i += 1
			
	def removeUnusedAttr(self, obj):
		try:
			if obj.hasAttr('rootCtrl'):
				obj.rootCtrl.delete()
			if obj.hasAttr('TYPE'):
				obj.TYPE.setLocked(False)	
				obj.TYPE.delete()
			if obj.hasAttr('networkParent'):
				obj.networkParent.delete()
			if obj.hasAttr('NODE'):
				obj.NODE.delete()
			if obj.hasAttr('BSH'):
				obj.BSH.delete()
		except:
			pass

	def createText(self, grp, name):
		#calc scale and trans values from self.gap
		scale = (self.gap*0.02, self.gap*0.02, self.gap*0.02)
		tenthGap = self.gap * -0.1

		#crate the text curves
		txtCrvs = pm.textCurves(n='%s_txt' %name, f='Arial', t=name, ch=True)
		txtCrv = pm.PyNode(txtCrvs[0])
		pm.delete(txtCrvs[1])
		for c in pm.listRelatives(txtCrv, children=True, type='transform'):
			c.translate.disconnect()

		#scale, freeze
		pm.xform(txtCrv, cp=True)
		pm.move(txtCrv, (0,0,0), rpr=True)
		pm.scale(txtCrv, scale , a=True)
		pm.makeIdentity(a=True, t=True, r=False, s=False)

		#calc position, place it and set it to displayType 'reference'
		bb = grp.getBoundingBox()
		grpWs = pm.xform(grp, q=True, ws=True, t=True)
		lowY = bb.min().y
		pm.xform(txtCrv, ws=True, t=(grpWs[0], lowY+tenthGap, grpWs[2]))
		# misc.lockAttr(txtCrv, t=True, r=True, s=True, v=True)
		misc.setDisplayType(txtCrv, False, 'ref')

		self.obj['txt'].append(txtCrv)

class BtpBase(object):

	def __init__(self, gap=5, txtGrp=None):
		#BshTarget.__init__(self)

		self.gap = gap
		self.txts = []

		self.x = self.gap
		self.y = self.gap
		self.z = self.gap

		self.parts = []
		self.partGrps = {}
		self.txtGrp = txtGrp

	def getParts(self):
		self.parts = []
		excludes = ['create', 'getParts', 'gap', 'txts', 'x', 'y', 'z', 'parts', 'partGrps', 'txtGrp']
		for i in [v for v in dir(self) if '__' not in v and v not in excludes]:
			exec('self.parts.append(self.%s)' %i) 

	def create(self, origObj):
		self.x = self.gap
		self.y = self.gap
		self.z = self.gap
		self.getParts()

		#create text grp
		self.txtGrp = pm.group(n='text_grp', em=True)

		p, b = 0, 0

		for part in self.parts:
			partName = part['NAME']
			partSide = part['SIDE']
			del(part['NAME'])
			del(part['SIDE'])
			#pBar
			pBar = ui.SmallPBarWindow(txt='Creating %s' %partName, times=len(part))

			if partSide in ['Rht', 'RHT', 'rt', 'RGT', 'Rgt']:
				b = -1
			else:
				b = 0
				p += 1

			partGrp = pm.group(n=partName, em=True)
			misc.addStrAttr(partGrp, 'PART', partName, lock=True)

			#reset x back to the start
			offset = 0

			for key, partDict in sorted(part.iteritems()):
				pBar.end(pBar.getCancelled())

				# # if k is info element, skip.
				# if key in ['NAME', 'SIDE']:
				# 	pBar.increment()
				# 	continue

				#calc position
				position = [(self.x*2)+offset, self.y*p, self.z*b]

				#cut off number infront of key
				if isinstance(key, str):
					if key[0:1].isdigit():
						key = key[2:]
				#create
				bshObj = BshTarget(origObj, partDict, key, partGrp, position, self.gap)
				bshObj.create()
				offset = bshObj.x + bshObj.meshWidth

				if bshObj.obj['p']:
					pm.parent(bshObj.obj['p'], partGrp)
				if bshObj.obj['n']:
					pm.parent(bshObj.obj['n'], partGrp)
				pm.parent(bshObj.obj['txt'], self.txtGrp)

				pBar.increment()
			
			#done one part, store partGrp in partGrp dic. Parent to target grp
			self.partGrps[partName] = partGrp

			#kill this part pBar
			pBar.end()



			
		


