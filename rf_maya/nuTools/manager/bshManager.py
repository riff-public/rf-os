import pymel.core as pm
import maya.OpenMaya as om
import maya.mel as mel
import os, socket, pprint ,inspect, sys, subprocess, pickle
from nuTools.manager import btp
from nuTools import misc, config, ui
from nuTools.util import symMeshReflector as smr

reload(misc)
reload(config)
reload(ui)
reload(smr)
reload(btp)

CTRL_PATH = 'P:/library/rigging/template'
DEFAULT_ATTRS = ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']
BSH_NAME = 'facialBuffer_bsn'

class BshManager(object):

	def __init__(self):
		self.WINDOW_TITLE = 'Blendshape Manager v2.0'
		self.WINDOW_NAME = 'bmWin'

		self.gap = 5.0
		self.width = 0
		self.height = 0

		self.exportPath = ''
		self.importPath = ''
		self.focusPartName = ''
		self.focusProjName = ''
		self.focusBtpName = ''
		self.currentRangeVal = 'p'

		self.focusInbs = []
		self.tAttrs = []
		self.nodes = []
		self.selTargets =[]
		self.focusParts = []
		self.focusPartNames = []
		self.allTargets = []
		self.sepAttrs = []
		self.selInbs = []

		self.projs = {}	
		self.btps = {}
		self.partGrps = {}
		self.targetDic = {}
		self.focusTgts = {}
		self.focusInbsDic = {}
		self.baseMeshesDict = {}

		self.currentProj = None
		self.btp = None
		self.bufferGrp = None
		self.tmpGrp = None
		self.txtGrp = None
		self.bsh = None
		self.focusPart = None
		self.selTgt = None
		self.currentBaseGeo = None
		self.bshTgtGrp = None
		self.targetCtrl = None


		#get all patterns avaliable in btp
		self.utilizePattern()

	def UI(self):
		"""
		The main UI function

		"""

		if pm.window(self.WINDOW_NAME, ex=True):
			# pm.windowPref(self.WINDOW_TITLE, r=True)
			pm.deleteUI(self.WINDOW_NAME, window=True)

		with pm.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, w=300, h=350, s=True, mnb=True, mxb=True) as self.mainWindow:
			# with pm.columnLayout(adj=0):
			with pm.frameLayout(label='Create, manage and modify blendshape targets', fn='smallObliqueLabelFont', mh=5, mw=5):
				with pm.rowColumnLayout(nc=3, co=[(1, 'left', 5), (2, 'left', 5), (3, 'left', 5)]):
					self.exportGrpTxt = pm.text(l='Bffr Group: ')
					self.exportGrpTxtFld = pm.textField(w=300, ed=False)
					self.exportGrpButt = pm.button(l='<<', c=pm.Callback(self.loadExportGrpUi))
				with pm.rowColumnLayout(nc=3, co=[(1, 'left', 5), (2, 'left', 5), (3, 'left', 160)]):
					pm.text(l='Global Scale')
					self.gsFloatFld = pm.floatField(w=50, v=self.gap, cc=pm.Callback(self.updateGlobalScale))
					self.selBufferGrpButt = pm.button(l='Select Buffer Grp', w=120, h=25, c=pm.Callback(self.selectBufferGrp))
				with pm.tabLayout() as self.mainTabLayout:

					# Manage
					with pm.columnLayout(adj=True, rs=0, co=['both', 0]) as self.manageColumnlayout:
						with pm.frameLayout( label='Start', mh=5, mw=5):
							with pm.rowColumnLayout(nc=4, co=[(1, 'left', 50), (2, 'left', 5), (3, 'left', 10), (4, 'left', 5)]):
								pm.text(l='Proj: ')
								with pm.optionMenu(w=60, cc=pm.Callback(self.setProject)) as self.projMenu:
									pm.menuItem(l='')
								pm.text(l='BTP: ')
								with pm.optionMenu(w=160, cc=pm.Callback(self.selectBtpUi)) as self.btpMenu:
									pm.menuItem(l='')
							with pm.rowColumnLayout(nc=3, rs=[1, 5], co=[(1, 'left', 50), (2, 'left', 5), (3, 'left', 5)]):
								self.bufferGrpTxt = pm.text(l='DUPLICATE: ')
								self.bufferGrpTxtFld = pm.textField(w=200, ed=False)
								self.bufferGrpButt = pm.button(l='<<', c=pm.Callback(self.loadBufferGrp))

							with pm.columnLayout(adj=True, rs=5, co=['both', 70]):
								self.prepareForSculptButt = pm.button(l='Create Targets', h=25, c=pm.Callback(self.prepareForSculpt))
								self.importCtrlButt = pm.button(l='Import Ctrl', h=25, c=pm.Callback(self.importCtrl))

							
								with pm.rowColumnLayout(nc=2, rs=[1, 5], co=[(1, 'left', 60), (2, 'left', 10)]):
									
									self.connectButt = pm.button(l='Connect', h=25, c=pm.Callback(self.connect))
									self.disconnectButt = pm.button(l='Disconnect', h=25, c=pm.Callback(self.disconnect))

						with pm.frameLayout( label='Parts', mh=5, mw=5, cll=True, cl=False,ec=pm.Callback(self.collapseFrame), cc=pm.Callback(self.collapseFrame)) as self.partsFrame:
							# with pm.columnLayout(adj=True, rs=5, co=['left', 10]):
							with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 110), (2, 'left', 5)]):
								self.partTSL = pm.textScrollList(ams=True, h=70, w=150, sc=pm.Callback(self.updateInfo))
								with pm.columnLayout(adj=True , rs= 10):
									self.connectToCtrlButt = pm.button(l='Connect', w=75, h=20, c=pm.Callback(self.connectToCtrl))
									# self.exportButt = pm.button(l='Export  >>', w=75, h=20, c=pm.Callback(self.exportPart))
									# self.importButt = pm.button(l='<<  Import', w=75, h=20, c=pm.Callback(self.importPart))
									pm.text(l='', h=17)
									with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 0), (2, 'left', 5)]):
										self.hidePartButt = pm.button(l='hide', w=33, h=20, c=pm.Callback(self.setPartVisibility, False))
										self.showPartButt = pm.button(l='show', w=36, h=20, c=pm.Callback(self.setPartVisibility, True))
							with  pm.frameLayout(label='Add/Remove', mh=0, mw=0, cll=True, cl=True,ec=pm.Callback(self.collapseFrame), cc=pm.Callback(self.collapseFrame)) as self.addRemovePartFrame:
								with pm.rowColumnLayout(nc=2, co=[(1, 'left', 90), (2, 'left', 5)]):
									self.addNewPartButt = pm.button(l='New Part', w=88, h=25, c=pm.Callback(self.createNewPart))
									self.removePartButt = pm.button(l='Remove Part', w=88, h=20, c=pm.Callback(self.removePart))
						with  pm.frameLayout(label='Targets', mh=5, mw=5, cll=True, cl=True,ec=pm.Callback(self.collapseFrame), cc=pm.Callback(self.collapseFrame)) as self.targetFrame:
							with pm.columnLayout(adj=True, rs=0):
								with pm.rowColumnLayout(nc=2, co=([1, 'left', 123], [2, 'left', 145])):
									pm.text(l='-', h=3)
									pm.text(l='+', h=3)
							with pm.columnLayout(adj=True, rs=5, co=['both', 5]):
								with pm.rowColumnLayout(nc=2, co=([1, 'left', 40], [2, 'left', 10])):
									self.targetTSLN = pm.textScrollList(ams=True, h=80, w=150, sc=pm.Callback(self.selectFocusTarget, 'n'))
									self.targetTSLP = pm.textScrollList(ams=True, h=80, w=150, sc=pm.Callback(self.selectFocusTarget, 'p'))
									
							with  pm.frameLayout(label='Connect target', mh=0, mw=0, cll=True, cl=True, ec=pm.Callback(self.collapseFrame), cc=pm.Callback(self.collapseFrame)) as self.connectTargetFrame:
								with pm.rowColumnLayout(nc=2, co=([1, 'left', 125], [2, 'left', 20])):
									self.saveTargetPresetButt = pm.button(l='Save Preset', h=20, c=pm.Callback(self.saveTargetPreset))
									self.loadTargetPresetButt = pm.button(l='Load Preset', h=20, c=pm.Callback(self.loadTargetPreset))


								with pm.rowColumnLayout(nc=4, rs=[1, 5], co=[(1, 'left', 10), (2, 'left', 6), (3, 'left', 5), (4, 'left', 5)]):
									self.targetCtrlTxt = pm.text(l='Ctrl: ')
									self.targetCtrlTxtFld = pm.textField(w=240, ed=False)
									self.targetCtrlButt = pm.button(l='<<', c=pm.Callback(self.loadTargetCtrl))
									self.connectTargetCtrlButt = pm.button(l='Connect', c=pm.Callback(self.connectTargetCtrlUi))

								with pm.rowColumnLayout(nc=6, co=[(1, 'left', 10), (2, 'left', 5), (3, 'left', 10), (4, 'left', 5), (5, 'left', 5), (6, 'left', 5)]):
									pm.text(l='Attr: ')
									with pm.optionMenu(w=90) as self.attrOptionMenu:
										pm.menuItem(l='')
									pm.text(l='Override: ')
									self.aliasTxtFld = pm.textField(w=90)
									pm.text(l='Mult')
									self.multFloatFld = pm.floatField(w=45, v=0.1)

							with  pm.frameLayout(label='Add/Remove', mh=0, mw=0, cll=True, cl=True, ec=pm.Callback(self.collapseFrame), cc=pm.Callback(self.collapseFrame)) as self.addRemoveTargetFrame:
								with pm.rowColumnLayout(nc=2, rs=([1, 5]), co=([1, 'left', 18])):
									with pm.columnLayout(adj=True, rs=5, co=['left', 5]):
										with pm.rowColumnLayout(nc=2, rs=([1, 5]), co=([1, 'left', 5], [2, 'left', 5])):
											pm.text(l='Attribute')
											self.addTgtAttrTxtFld = pm.textField(w=195)
										with pm.rowColumnLayout(nc=3, co=([1, 'left', 22], [2, 'left', 5], [3, 'left', 7])):
											pm.text(l='Name')
											self.negTgtNameTxtFld = pm.textField(w=94)
											self.posTgtNameTxtFld = pm.textField(w=94)
										with pm.rowColumnLayout(nc=2, co=([1, 'left', 75], [2, 'left', 40])):
											self.removeButtN = pm.button(l='Remove-', w=60, h=20, c=pm.Callback(self.removeTarget, self.targetTSLN))
											self.removeButtP = pm.button(l='Remove+', w=60, h=20, c=pm.Callback(self.removeTarget, self.targetTSLP))

									with pm.columnLayout(adj=True, rs=5, co=['left', 0]):
										self.addTgtButt = pm.button(l='Add', w=50, h=20, c=pm.Callback(self.addNewTarget))
										
						with  pm.frameLayout(label='Inbetweens', mh=5, mw=5, cll=True, cl=True, ec=pm.Callback(self.collapseFrame), cc=pm.Callback(self.collapseFrame)) as self.inbFrame:
							with pm.columnLayout(adj=False, rs=5, co=['left', 100]):
								self.inbTSL = pm.textScrollList(ams=True, h=50, w=195, sc=pm.Callback(self.selectFocusInb))
							with pm.rowColumnLayout(nc=2, co=([1, 'left', 96], [2, 'left', 10])):
								self.addInbTgtButt = pm.button(l='Add Inb', w=95, h=20, c=pm.Callback(self.addInbTarget))
								self.removeInbTgtButt = pm.button(l='Remove Inb', w=95, h=20, c=pm.Callback(self.removeInbTarget))
								
					pm.tabLayout( self.mainTabLayout, edit=True, selectTabIndex=1, tabLabel=((self.manageColumnlayout, ' Manage ')))

		for name in sorted(self.projs.keys()):
			pm.menuItem(l=name, p=self.projMenu)

		#set gap value
		# self.gap = self.gap*self.gsFloatFld.getValue()

		self.collapseFrame()
	
	def saveTargetPreset(self):
		ctrls = misc.getSel(num='inf')
		if not ctrls:
			return

		data = {}
		for ctrl in ctrls:
			if not ctrl.hasAttr('_TARGET'):
				continue
			ctrlStrVal = 1
			ctrlShp = ctrl.getShape()
			if ctrlShp and ctrlShp.hasAttr('ctrlStrength'):
				ctrlStrVal = ctrlShp.ctrlStrength.get()

			clamps = ctrl._TARGET.outputs(type='clamp')
			datas = []
			if clamps:
				for cmpNode in clamps:
					partName = cmpNode._PART.get()
					targetName = cmpNode._TARGETNAME.get()
					attrName = cmpNode._ATTRNAME.get()
					aliasName = cmpNode._ALIASNAME.get()

					multVal = 0.1
					mdvNodes = cmpNode.inputs(type='multiplyDivide')
					if mdvNodes:
						mdvNode = mdvNodes[0]
						multVal = mdvNode.input2Y.get() if mdvNode.input2Y.isConnected() else abs(mdvNode.input2X.get())
					# {ctrl:{'strength':value, 'data':[(partName, targetName, attr, alias, mult), (...)]}}
					datas.append((partName, targetName, attrName, aliasName, multVal))

			data[ctrl.nodeName()] = {'ctrlStrength':ctrlStrVal, 'data':datas}


		mydoc_path = os.path.expanduser('~')
		path = pm.fileDialog2(ds=1, cap='Save preset', dir=mydoc_path, fm=0, ff='*.pkl')
		if not path:
			return
		# write out settings
		print data
		with open(path[0], 'wb') as handle:
			pickle.dump(data, handle)


	def loadTargetPreset(self):
		sels = misc.getSel(num='inf')
		if not sels:
			return
		selDict = dict((s.nodeName(),s) for s in sels)

		mydoc_path = os.path.expanduser('~')
		path = pm.fileDialog2(ds=1, cap='Load preset', dir=mydoc_path, fm=1, ff='*.pkl')
		if not path:
			return

		data = None  # {ctrl:{'strength':value, 'data':[(partName, targetName, attr, alias, mult), (...)]}}
		with open(path[0], 'rb') as handle:
			data = pickle.load(handle)
		if not data:
			return
			
		for ctrl, dataDict in data.iteritems():
			if ctrl in selDict:
				targetCtrl = selDict[ctrl]
			else:
				continue
			targetCtrlShp = targetCtrl.getShape()
			if targetCtrlShp:
				strValue = dataDict['ctrlStrength']
				if strValue and targetCtrlShp.hasAttr('ctrlStrength'):
					strAttr = targetCtrlShp.attr('ctrlStrength')
					if not strAttr.inputs():
						if strAttr.isLocked():
							strAttr.unlock()
						strAttr.set(strValue)
						strAttr.lock()
			for data in dataDict['data']:
				self.connectTargetCtrl(ctrl=targetCtrl,
									partName=data[0], 
									targetName=data[1],
									attrTxt=data[2], 
									aliasTxt=data[3], 
									multValue=data[4])

	def connectTargetCtrl(self, ctrl, partName, targetName, attrTxt, aliasTxt, multValue):
		partGrp = self.partGrps[partName]
		partName = partGrp.PART.get()
		selTarget = None
		for tgName, targets in self.targetDic[partName].iteritems():
			if tgName.lstrip('0123456789') == targetName:
				selTarget = [targets['n'], targets['p']]
				break
		else:
			print 'Can not find %s' %targetName
			return

		names = (partName, attrTxt, aliasTxt)
		self.doConnectTargetCtrl(ctrl, partGrp, selTarget, attrTxt, aliasTxt, multValue, names, targetName)

	def connectTargetCtrlUi(self):
		if not self.targetCtrl or not self.bsh:
			return
		attrTxt = self.attrOptionMenu.getValue()
		aliasTxt = self.aliasTxtFld.getText()
		multValue = self.multFloatFld.getValue()

		if not attrTxt and not aliasTxt:
			return

		if not self.focusPart or not self.selTargets:
			return
		partGrp = self.focusPart[0]
		partName = partGrp.PART.get()
		targetName = ''
		targets = []

		for tgtName, targetGrps in self.targetDic[partName].iteritems():
			if self.selTargets[0] in targetGrps.values():
				targetName = tgtName.lstrip('0123456789')
				targets = [targetGrps['n'], targetGrps['p']]
				break
		else:
			return

		names = (partName, attrTxt, aliasTxt)
		self.doConnectTargetCtrl(self.targetCtrl, partGrp, targets, attrTxt, aliasTxt, multValue, names, targetName)

	def doConnectTargetCtrl(self, ctrl, partGrp, targets, attrTxt, aliasTxt, multValue, names, targetName):
		name = '%s_%s_%s' %(names[0], names[1], names[2])
		if attrTxt in DEFAULT_ATTRS and aliasTxt:
			ctrlAttr = ctrl.attr(attrTxt)
			if not ctrl.hasAttr(aliasTxt):
				ctrlAttr.setAlias(aliasTxt)
		elif attrTxt in DEFAULT_ATTRS and not aliasTxt:
			ctrlAttr = ctrl.attr(attrTxt)
			aliasTxt = attrTxt
		else:
			ctrlAttr = misc.addNumAttr(ctrl, aliasTxt, 'double', hide=False)

		ctrlAttr.unlock()
		ctrlAttr.setKeyable(True)

		ctrlShp = ctrl.getShape(ni=True)
		ctrlShpAttr = misc.addNumAttr(ctrlShp, '%sAdd' %aliasTxt, 'double', hide=False)
		ctrlShpMult = misc.addNumAttr(ctrlShp, '%sMult' %aliasTxt, 'double', hide=True, dv=multValue)

		adlNodeName = '%s_adl' %name
		adlNode = pm.createNode('addDoubleLinear', n=adlNodeName)
		self.reportBuff(adlNode, '_NODE')

		pm.connectAttr(ctrlAttr, adlNode.input1)
		pm.connectAttr(ctrlShpAttr, adlNode.input2)

		nTarget = targets[0]
		pTarget = targets[1]
		
		mdvNode, cmpNode = self.createConnection(adlNode.output, nTarget, pTarget, name, multAttr=ctrlShpMult)
		misc.addStrAttr(cmpNode, '_PART', txt=names[0], lock=True)
		misc.addStrAttr(cmpNode, '_TARGETNAME', txt=targetName, lock=True)
		misc.addStrAttr(cmpNode, '_ATTRNAME', txt=names[1], lock=True)
		misc.addStrAttr(cmpNode, '_ALIASNAME', txt=names[2], lock=True)
		self.report(ctrl, cmpNode, '_TARGET', '_CTRL')
		pm.select(ctrl)
	
	def createConnection(self, source, nTarget, pTarget, attrName, multAttr=None):
			mdvNodeName = '%s_mdv' %attrName
			mdvNode = pm.createNode('multiplyDivide', n=mdvNodeName)

			clampNodeName = '%s_cmp' %attrName
			clampNode = pm.createNode('clamp', n=clampNodeName)

			clampNode.minR.set(0)
			clampNode.minG.set(0)
			clampNode.maxR.set(999)
			clampNode.maxG.set(999)

			pm.connectAttr(source, mdvNode.input1X, f=True)
			pm.connectAttr(source, mdvNode.input1Y, f=True)

			revMdl = None
			nStatus, pStatus = False, False
			if nTarget:
				nPlug = self.getBshPlug(nTarget)
				if nPlug:
					pm.connectAttr(mdvNode.outputX, clampNode.inputR, f=True)
					pm.connectAttr(clampNode.outputR, nPlug, f=True)

					revMdlNodeName = '%s_mdl' %attrName
					revMdl = pm.createNode('multDoubleLinear', n=revMdlNodeName)
					revMdl.input2.set(-1)

					pm.connectAttr(multAttr, revMdl.input1)
					pm.connectAttr(revMdl.output, mdvNode.input2X)
					nStatus = True
			else:
				clampNode.minG.set(-999)

			if pTarget:
				pPlug = self.getBshPlug(pTarget)
				if pPlug:
					pm.connectAttr(mdvNode.outputY, clampNode.inputG, f=True)
					pm.connectAttr(clampNode.outputG, pPlug, f=True)
					# mdvNode.input2Y.set(mult)
					pm.connectAttr(multAttr, mdvNode.input2Y)
					pStatus = True
			else:
				clampNode.minR.set(-999)


			if nStatus == True or pStatus == True:
				self.reportBuff(mdvNode, '_NODE')
				self.reportBuff(clampNode, '_NODE')

				if nStatus == True:
					self.reportBuff(revMdl, '_NODE')
				print 'connected: %s (%s, %s)' %(attrName, nTarget, pTarget)
				return mdvNode, clampNode
			else:
				pm.delete([mdvNode, clampNode])
				pm.warning('Cannot find plug to connect  %s  to  %s and %s' %(attrName, nTarget, pTarget))
	
	def loadTargetCtrl(self, sel=None):
		if not sel:
			sel = misc.getSel()
		try:
			self.targetCtrlTxtFld.setText('')
			self.targetCtrlTxtFld.setText(sel.nodeName())
		except:
			pass

		self.targetCtrl = sel
		selectedMenu = self.attrOptionMenu.getValue()
		self.attrOptionMenu.clear()
		if self.targetCtrl:
			pm.menuItem(l='', p=self.attrOptionMenu)
			udAttrs = self.targetCtrl.listAttr(k=True, u=True, se=True, ud=True)
			for attr in DEFAULT_ATTRS:
				pm.menuItem(l=attr, p=self.attrOptionMenu)
			for attr in udAttrs:
				pm.menuItem(l=attr.shortName(), p=self.attrOptionMenu)

			try:
				self.attrOptionMenu.setValue(selectedMenu)
			except:
				pass


	def collapseFrame(self):
		# currH = pm.window(self.mainWindow, q=True, h=True)
		baseH = 300
		if not pm.frameLayout(self.partsFrame, q=True, cl=True):
			baseH += 30
		if not pm.frameLayout(self.addRemovePartFrame, q=True, cl=True):
			baseH += 30

		if not pm.frameLayout(self.targetFrame, q=True, cl=True):
			baseH += 30
		if not pm.frameLayout(self.addRemoveTargetFrame, q=True, cl=True):
			baseH += 30
		if not pm.frameLayout(self.connectTargetFrame, q=True, cl=True):
			baseH += 30

		if not pm.frameLayout(self.inbFrame, q=True, cl=True):
			baseH += 30
		# print baseH
		pm.window(self.mainWindow, e=True, h=baseH)

	def importCtrl(self):
		importPath = self.browseFile(stDir=CTRL_PATH, mode=1, 
			title='Import blendshape controls', 
			okTitle='Import')
		if not importPath:
			return
		importedObjs = pm.importFile(importPath, 
				loadNoReferences=True, 
				returnNewNodes=True, 
				namespace='')
		misc.removeNameSpace(importedObjs)


	def addInbTarget(self):
		# self.updateInfo()
		self.getCurrentTgtTSL()

		if not self.focusTgts:
			return

		#get width again to make sure
		self.getWidth(self.bufferGrp)

		for name in self.selTgt:
			toAdd = self.focusTgts[name]
			inbs = self.getInbTargets(toAdd)

			inbNum = 0
			if inbs:
				inbNum = len(inbs)
				for i in inbs:
					pm.xform(i, t=[self.width*-1, 0, 0], r=True)

			#recentTgt = toAdd
				
			oldPos = pm.xform(toAdd, q=True, ws=True, t=True)

			position = [oldPos[0] - self.width, oldPos[1], oldPos[2]]

			#duplicate
			inbOrder = str(inbNum+1).zfill(2)
			name = '%s_inb%s' %(toAdd.nodeName(), inbOrder)
			newInb = pm.duplicate(toAdd, n=name)[0]
			pm.xform(newInb, ws=True, t=position)

			self.removeUnusedAttr(newInb, 'TXT')
			self.removeUnusedAttr(newInb, 'p')
			self.removeUnusedAttr(newInb, 'n')
			self.removeUnusedAttr(newInb, 'INB')

			#report
			self.report(toAdd, newInb, 'INB', 'inb_%s' %inbOrder)

		#move all in row
			part =  self.getPartFromTarget(toAdd)
			allTargets = self.getTargetsFromPart(part)
			toMove = []
			txts = []
			for t in allTargets:
				if pm.xform(t, q=True, ws=True, t=True)[0] < oldPos[0]:
					continue
				toMove.append(t)
				txt = self.getTxtFromTarget(t)
				if txt:
					txts.append(txt)
				inbetweens = self.getInbTargets(t)
				if inbetweens:
					toMove.extend(inbetweens)

			pm.xform(toMove, t=(self.width, 0, 0), r=True)
			if txts:
				for txt in txts:
					misc.lockAttr(txt, t=True, lock=False)
				pm.xform(txts, t=(self.width, 0, 0), r=True)
				for txt in txts:
					misc.lockAttr(txt, t=True, lock=True)
			
		#select all in target including inbs
		self.currentTsl.setSelectItem(self.selTgt)
		self.selectFocusTarget(self.currentRangeVal)

	def addNewTarget(self):
		#disconnect rig first
		self.disconnect()

		#get name dic from ui
		self.getFocusPart()
		if not self.focusPart:
			return
		negName = self.negTgtNameTxtFld.getText()
		posName = self.posTgtNameTxtFld.getText()
		attr = self.addTgtAttrTxtFld.getText()

		targetsInPart = self.getTargetsFromPart(self.focusPart)

		#perform security check for crashing target name
		if negName in targetsInPart or posName in targetsInPart:
			om.MGlobal.displayError('Target with this name already exists!, try removing it before adding new one.')
			return

		nameDic = {'p':posName, 'n':negName}

		#calc position
		targetX, targetY = [] ,[]
		#adding to existing part
		if targetsInPart:
			for t in targetsInPart:
				pos = pm.xform(t, q=True, ws=True, t=True)
				targetX.append(pos[0])

			maxX = max(targetX)
			oldPos = pm.xform(targetsInPart[0], q=True, ws=True, t=True)
			position = [maxX+(self.gap*2), oldPos[1], oldPos[2]]
		# adding to newly created part
		else:
			for p in self.partGrps.values():
				targetsInPart = self.getTargetsFromPart(p)
				if not targetsInPart:
					continue
				pos = pm.xform(targetsInPart[0], q=True, ws=True, t=True)
				targetY.append(pos[1])

			maxY = max(targetY)
			z = 0
			nameParts = misc.nameSplit(self.focusPartName)

			if nameParts['pos'] in ['Rht', 'RHT', 'rt', 'RGT', 'Rgt']:
				z = -1
			position = [self.gap*2, maxY+self.gap, self.gap*z]

		#duplicate
		bshObj = btp.BshTarget(self.bufferGrp, nameDic, attr, self.focusPart, position, self.gap)
		bshObj.create()

		toSel = []
		#parent
		if bshObj.obj['p']:
			pm.parent(bshObj.obj['p'], self.focusPart)
			toSel.append(bshObj.obj['p'])

		if bshObj.obj['n']:
			pm.parent(bshObj.obj['n'], self.focusPart)
			toSel.append(bshObj.obj['n'])

		pm.parent(bshObj.obj['txt'], self.txtGrp)
		toSel.extend(bshObj.obj['txt'])

		#update info
		self.updateInfo()
		pm.select(toSel, r=True)

	def browseFile(self, stDir, mode, title, okTitle):
		multipleFilters = "Maya Files (*.ma *.mb);;Maya ASCII (*.ma);;Maya Binary (*.mb);;All Files (*.*)"
		path = pm.fileDialog2(ff=multipleFilters, ds=2, fm=1, dir=stDir, cap=title, okc=okTitle)
		if path:
			return path[0]

	def clearVal(self):
		self.exportPath = ''
		self.importPath = ''
		self.focusPartName = ''

		self.bufferGrp = None
		self.exportGrp = None
		self.tmpGrp = None
		self.txtGrp = None
		self.focusPart = None
		self.bsh = None
		self.selTgt = None

		self.partGrps = {}
		self.targetDic = {}
		self.focusInbsDic = {}
		self.focusTgts = {}

		self.nodes = []		
		self.allTargets = []
		self.sepAttrs = []	
		self.focusParts = []
		self.focusPartNames = []
		self.selTargets = []	
		self.focusInbs = []
		self.selInbs = []

	def clearUi(self):
		self.partTSL.removeAll()
		self.targetTSLN.removeAll()
		self.targetTSLP.removeAll()
		self.inbTSL.removeAll()
		self.bufferGrpTxtFld.setText('')
		# self.gsFloatFld.setValue(1.0)

	def createNewPart(self):
		if not self.partGrps or not self.bufferGrp:
			return

		newName = self.showNewPartPromptWin()

		if not newName:
			return
		self.focusPartName = newName
		#create new part group
		partGrp = pm.group(n=self.focusPartName, em=True)
		misc.addStrAttr(partGrp, 'PART', self.focusPartName, lock=True)
		self.partGrps[self.focusPartName] = partGrp

		#parent to bshTgtGrp
		pm.parent(self.partGrps[self.focusPartName], self.bshTgtGrp)

		#update part tsl
		self.updatePartTSL()
		self.partTSL.setSelectItem(self.partGrps[self.focusPartName].nodeName())

	def connect(self):
		if not self.targetDic or not self.bufferGrp:
			return

		#disconnect first
		self.disconnect()

		bshNode = pm.blendShape(self.allTargets, self.bufferGrp, n=BSH_NAME, bf=True)[0]
		self.reportBuff(bshNode, '_BSH')
		self.getBsh()

		#add inb targets
		for target in self.allTargets:
			inbTargets = []
			if target.hasAttr('INB'):
				inbTargets = self.getInbTargets(target)
			if inbTargets:
				n = 1
				inbTargetNum = len(inbTargets) + 1
				indx = self.getBshIndexFromTarget(target)
				for inb in inbTargets:
					w = round(float(n)/inbTargetNum, 3)
					pm.blendShape(self.bsh, e=True, ib=True, t=(self.bufferGrp, indx, inb, w))
					n += 1

		self.updateInfo()
		self.getCurrentTgtTSL()
		self.selectFocusTarget(self.currentRangeVal)
		pm.select(self.bufferGrp, r=True)
		print 'Create blendShape: %s' %self.bsh.nodeName()

	def createConnection(self, source, nTarget, pTarget, attrName, multAttr=None):
		mdvNodeName = '%s_mdv' %attrName
		mdvNode = pm.createNode('multiplyDivide', n=mdvNodeName)

		clampNodeName = '%s_cmp' %attrName
		clampNode = pm.createNode('clamp', n=clampNodeName)

		clampNode.minR.set(0)
		clampNode.minG.set(0)
		clampNode.maxR.set(999)
		clampNode.maxG.set(999)

		pm.connectAttr(source, mdvNode.input1X, f=True)
		pm.connectAttr(source, mdvNode.input1Y, f=True)

		pm.connectAttr(mdvNode.outputX, clampNode.inputR, f=True)
		pm.connectAttr(mdvNode.outputY, clampNode.inputG, f=True)

		revMdl = None
		nStatus, pStatus = False, False
		if nTarget:
			nPlug = self.getBshPlug(nTarget)
			if nPlug:
				pm.connectAttr(clampNode.outputR, nPlug, f=True)

				revMdlNodeName = '%sRev_mdl' %attrName
				revMdl = pm.createNode('multDoubleLinear', n=revMdlNodeName)
				revMdl.input2.set(-1)

				pm.connectAttr(multAttr, revMdl.input1)
				pm.connectAttr(revMdl.output, mdvNode.input2X)
				nStatus = True
		else:
			clampNode.minG.set(-999)

		if pTarget:
			pPlug = self.getBshPlug(pTarget)
			if pPlug:
				pm.connectAttr(clampNode.outputG, pPlug, f=True)
				# mdvNode.input2Y.set(mult)
				pm.connectAttr(multAttr, mdvNode.input2Y)
				pStatus = True
		else:
			clampNode.minR.set(-999)


		if nStatus == True or pStatus == True:
			self.reportBuff(mdvNode, '_NODE')
			self.reportBuff(clampNode, '_NODE')

			if nStatus == True:
				self.reportBuff(revMdl, '_NODE')
			print 'connected: %s (%s, %s)' %(attrName, nTarget, pTarget)
			return mdvNode, clampNode
		else:
			pm.delete([mdvNode, clampNode])
			pm.warning('Cannot find plug to connect  %s  to  %s and %s' %(attrName, nTarget, pTarget))

	def connectToCtrl(self, multValue=0.1):
		sel = misc.getSel()
		if not sel:
			return

		selParts = [i for i in self.getSelectedParts() if i and i.nodeName() in self.partGrps.keys()]
		if not selParts:
			return

		ctrlShp = sel.getShape(ni=True)
		for partName in [i.nodeName() for i in selParts]:
			partGrp = self.partGrps[partName]
			attrs = [i for i in pm.listAttr(partGrp, ud=True) if i != "PART"]

			#pBar
			pBar = ui.SmallPBarWindow(txt='Connecting %s' %partGrp.nodeName(), times=len(attrs))
			for attr in attrs:
				#if user hit cancel
				pBar.end(pBar.getCancelled())

				ctrlAttr = misc.addNumAttr(sel, attr, 'double', hide=False)

				ctrlShpAttr = misc.addNumAttr(ctrlShp, '%sAdd' %attr, 'double', hide=False)
				ctrlShpMult = misc.addNumAttr(ctrlShp, '%sMult' %attr, 'double', hide=True, dv=multValue)
				

				adlNodeName = '%s_%sSum_adl' %(partName, attr)
				adlNode = pm.createNode('addDoubleLinear', n=adlNodeName)
				self.reportBuff(adlNode, '_NODE')
				
				pm.connectAttr(ctrlAttr, adlNode.input1)
				pm.connectAttr(ctrlShpAttr, adlNode.input2)

				# pm.connectAttr(adlNode.output, attr, f=True)

				# tAttr = misc.addNumAttr(self.bufferGrp, '%s__%s' %(partName, attr), 'double', hide=False)
				# self.tAttrs.append(tAttr)

				pTarget, nTarget = None, None

				for plug in pm.listConnections(partGrp.attr(attr), d=True, s=False, p=True):
					if plug.shortName() == 'p':
						pTarget = plug.plugNode()
					elif plug.shortName() == 'n':
						nTarget = plug.plugNode()

				self.createConnection(adlNode.output, nTarget, pTarget, '%s_%s' %(partName, attr), multAttr=ctrlShpMult)

				#increment pBar for this attr
				pBar.increment()

			#kill pBar for this part
			pBar.end()

	def disconnect(self):
		self.updateInfo()

		#remove existing nodes and attr
		if self.nodes:
			pm.delete(self.nodes)
			self.nodes = []
			print ('Utility Nodes Deleted!')

		if self.bsh:
			pm.delete(self.bsh)
			self.bsh = None
			print ('Blendshape Nodes Deleted!')

		if self.tAttrs:
			for i in self.tAttrs:
				i.delete()
			self.tAttrs = []
			print ('Attribute Deleted!')

		if self.sepAttrs:
			for i in self.sepAttrs:
				i.setLocked(False)
				i.delete()
			self.sepAttrs = []
			print ('Separator Attribute  Deleted!')

		pm.delete(self.bufferGrp, ch=True)

	def exportPart(self):
		if not self.bufferGrp:
			return

		toExport = self.getSelectedParts(getTxt=True)

		if not toExport:
			return

		#add buffer grp
		toExport.append(self.bufferGrp)

		#select it first
		pm.select(toExport, r=True)

		#get current dir as export path
		currentDir = '/'.join(pm.sceneName().split('/')[0:-1])
		self.exportPath = self.browseFile(stDir=currentDir, mode=0, title='Share sculpt work with other modeller.', okTitle='Export')

		if not self.exportPath:
			return

		#export selected
		pm.exportSelected(self.exportPath, f=True, ch=False, constraints=False, expressions=False, type='mayaAscii')

		pm.select(cl=True)

	def get(self, pNetwork, attr):

		if pNetwork.hasAttr(attr):
			return pm.listConnections(pNetwork.attr(attr), d=True, s=False)

	def getParentNetwork(self, cNode, attr):
		if cNode.hasAttr(attr):
			return pm.listConnections(cNode.attr(attr), s=True, d=False)

	def getTxtGrp(self):
		try:
			txtGrp = pm.listConnections(self.bufferGrp.txtGrp, d=True, s=False)[0]
		except:
			om.MGlobal.displayError('Cannot find textGrp!')
			return

		self.txtGrp = txtGrp 

	def getTxt(self, target):
		try:
			txt = pm.listConnections(target.attr('TXT'), d=True, s=False)[0]
		except:
			return

		return txt

	def getAllTxtInPart(self, part):
		targets = self.getTargetsFromPart(part)
		if not targets:
			return
		txts = []
		for t in targets:
			txt = self.getTxt(t)
			if txt:
				txts.append(txt)
		return txts

	def getTmpGrp(self):
		try:
			tmpGrp = pm.listConnections(self.bufferGrp.tmpGrp, d=True, s=False)[0]
		except:
			om.MGlobal.displayError('Cannot find tmpGrp!')
			return

		self.tmpGrp = tmpGrp

	def getBshTgtGrp(self):
		try:
			bshTgtGrp = pm.listConnections(self.bufferGrp.bshTgtGrp, d=True, s=False)[0]
		except:
			om.MGlobal.displayError('Cannot find blendShape Target Group!')
			return
			
		self.bshTgtGrp = bshTgtGrp 

	def getBufferGrp(self):
		try:
			bufferGrp = pm.listConnections(self.bufferGrp, d=True, s=False)[0]
		except:
			om.MGlobal.displayError('Cannot find bufferGrp!')
			return
			
		self.bufferGrp = bufferGrp
		try:
			self.bufferGrpTxtFld.setText(self.bufferGrp.nodeName())
		except: pass

	def getBshPlug(self, target):
		attrName = target.nodeName()
		if self.bsh.hasAttr(attrName):
			return self.bsh.attr(attrName)
		else:
			return None

	def getInbTargets(self, target):
		if not target.hasAttr('INB'):
			return []
		inbTargetPlugs = pm.listConnections(target.INB, d=True, s=False, p=True)
		inbTargets =  map(lambda plug: plug.plugNode(), sorted(inbTargetPlugs))
		return inbTargets

	def getBshIndexFromTarget(self, target):
		if not self.bsh:
			return
		#get a shape
		shps = filter(lambda b: isinstance(b, pm.nt.Mesh)==True, pm.listRelatives(self.bufferGrp, ad=True))
		tshps = filter(lambda x: isinstance(x, pm.nt.Mesh)==True, pm.listRelatives(target, ad=True))
		if not tshps or not shps:
			return
		shp = shps[0]
		tshp = tshps[0]
		for i in range(self.bsh.numWeights()):
			t = self.bsh.getTargets(shp, i)
			if t:
				t = t[0]
			else: continue
			if t == tshp:
				return i

	def getParts(self):
		if not self.bshTgtGrp:
			return
		children = self.bshTgtGrp.getChildren()
		for c in children:
			if c.hasAttr('PART'):
				self.partGrps[c.nodeName()] = c

	def getTxtFromPart(self, part):
		txts = []
		try:
			targets = part.getChildren()
		except: return []

		for i in targets:
			txts.append(self.getTxt(i))
		return txts

	def getTxtFromTarget(self, target):
		txt = None
		try:
			txt = target.TXT.outputs()[0]
		except: pass
		return txt

	def getPartFromTarget(self, target):
		part = []
		valRangeAttr = [i for i in pm.listAttr(target, ud=True) if i in ['p', 'n']]
		if not valRangeAttr:
			return
		valRangeAttr = valRangeAttr[0]
		try:
			part = pm.listConnections(target.attr(valRangeAttr), s=True)[0]
		except: pass

		return part

	def getSelectedParts(self, getTxt=True):
		#get tsl sel
		selParts = self.partTSL.getSelectItem()

		retList = []
		txts = []
		for s in selParts:
			retList.append(self.partGrps[s])
			if getTxt == True:
				txt = self.getTxtFromPart(self.partGrps[s])
				txts.extend(txt)
		retList.extend(txts)

		return retList

	def getWidth(self, obj):		
		bb = obj.getBoundingBox()
		self.width = abs(bb[1][0] - bb[0][0])

	def getHeight(self, obj):		
		bb = obj.getBoundingBox()
		self.height = abs(bb[1][1] - bb[0][1])

	def getCurrentTgtTSL(self):
		pSels = self.targetTSLP.getSelectItem()
		nSels = self.targetTSLN.getSelectItem()
		if not self.selTgt:
			self.currentRangeVal = 'p'
			return

		if self.selTgt[0] in pSels:
			self.currentTsl = self.targetTSLP
			self.currentRangeVal = 'p'
		else:
			self.currentTsl = self.targetTSLN
			self.currentRangeVal = 'n'

	def getTargetsFromPart(self, part=None):
		if not part:
			return
		attrs = pm.listAttr(part, ud=True)

		targets = []
		for a in attrs:
			attr = part.attr(a)
			if attr.get(type=True) == 'message':
				tgt = pm.listConnections(attr, d=True)
				if tgt:
					targets.extend(tgt)
		return targets

	def getFocusPart(self):
		tslSel = self.partTSL.getSelectItem()
		if not tslSel:
			return
		self.focusPartName = tslSel[0]
		self.focusPart = self.partGrps[self.focusPartName]

	def getFocusParts(self):
		tslSel = self.partTSL.getSelectItem()
		if tslSel:
			self.focusPartNames = tslSel
			self.focusParts = map(lambda x: self.partGrps[x], self.focusPartNames)

	def getAttrFromTarget(self, target):
		valRangeAttr = [i for i in pm.listAttr(target, ud=True) if i in ['p', 'n']]
		if not valRangeAttr:
			return
		valRangeAttr = valRangeAttr[0]
		partAttr = pm.listConnections(target.attr(valRangeAttr), s=True, p=True)
		if not partAttr:
			return []
		return partAttr[0]

	def getSepAttrs(self):
		if not self.bufferGrp:
			return 
		udAttrs = pm.listAttr(self.bufferGrp, ud=True, s=True, k=False, v=True, u=False)
		attrs =  map(lambda name: self.bufferGrp.attr(name), udAttrs)
		self.sepAttrs = [i for i in attrs if '___' in i.name()]

	def getNodes(self):
		if not self.bufferGrp:
			return

		self.nodes = self.get(self.bufferGrp, '_NODE')

	def getBsh(self):
		if not self.bufferGrp:
			return
		bshNode = self.get(self.bufferGrp, '_BSH')
		if bshNode:
			self.bsh = bshNode[0]

	def getTargetFromInb(self, inb):
		udAttrs = pm.listAttr(inb, ud=True)
		for attr in udAttrs:
			if attr.startswith('inb_'):
				return self.getParentNetwork(inb, attr)[0]

	def importPart(self):
		if not self.partGrps:
			return

		#get current dir as export path
		currentDir = '/'.join(pm.sceneName().split('/')[0:-1])
		self.importPath = self.browseFile(stDir=currentDir, mode=1, title='Import sculpted blendshape targets.', okTitle='Import')
		if not self.importPath:
			return

		fileName = ''
		#import the file
		try:
			fileName = self.importPath.split('/')[-1]
			ns = fileName.split('.')[0]
		except:
			ns = 'IMPORT_BSH'

		if not os.path.exists(self.importPath):
			print self.importPath
			return

		importedObjs = pm.importFile(self.importPath, loadNoReferences=True, returnNewNodes=True, namespace=ns)
		
		#delete existing ,batch remove prefix
		#parent into proper group
		keep, toRemove, toSel = [], [], []
		iImportGrp, iTmpGrp = None, None

		pBar = ui.SmallPBarWindow(txt='Identifying Nodes...', times=len(importedObjs))
		allShaderTypes = pm.listNodeTypes('shader')
		# print self.partGrps
		for obj in [o for o in importedObjs if o.nodeType()=='transform']:
			#if cancelled
			pBar.end(pBar.getCancelled())
			if obj in keep:
				pBar.increment()
				continue

			#part grp found
			if obj.hasAttr('PART'):
				partName = obj.PART.get().strip()

				if self.partGrps[partName]:
					txts = self.getAllTxtInPart(self.partGrps[partName])
					pm.delete(self.partGrps[partName])
					if txts:
						pm.delete(txts)
				pm.parent(obj, self.bshTgtGrp)
				toSel.append(obj)
				geos = pm.listRelatives(obj, ad=True)
				geos.insert(0, obj)
				keep.extend(geos)
			elif obj.hasAttr('GEO'):
				pm.parent(obj, self.txtGrp)
				crvs = pm.listRelatives(obj, ad=True)
				crvs.insert(0, obj)
				keep.extend(crvs)
				toSel.append(obj)
			# elif obj.type() in allShaderTypes:
				# continue

			# else:
			#  	toRemove.append(obj)

			pBar.increment()
		pBar.end()

		# toRemove.extend([iImportGrp, iTmpGrp, etc])
		# pm.delete(toRemove)

		misc.removeNameSpace(keep)
		self.updateInfo()

		pm.select(toSel, r=True)

	def loadBufferGrp(self, sel=None):
		if not sel:
			sel = misc.getSel()
		try:
			self.bufferGrpTxtFld.setText('')
			self.bufferGrpTxtFld.setText(sel.nodeName())
		except:
			pass

		self.bufferGrp = sel

		if self.bufferGrp:
			misc.addStrAttr(self.bufferGrp, '_TYPE', txt='bufferGrp', lock=True)
			misc.addMsgAttr(self.bufferGrp, '_NODE')
			misc.addMsgAttr(self.bufferGrp, '_BSH')
			# misc.hideAttr(self.bufferGrp, t=True, r=True, s=True, v=True)

	def loadExportGrpUi(self, sel=None):
		self.clearUi()
		if not sel:
			sel = misc.getSel()
		try:
			if not sel.hasAttr('_TYPE'):
				self.exportGrpTxtFld.setText('')
				return
			if sel._TYPE.get() != 'bufferGrp':
				self.exportGrpTxtFld.setText('')
				return
			self.exportGrpTxtFld.setText(sel.nodeName())
		except:
			self.exportGrpTxtFld.setText('')
			pass

		self.loadExportGrp(sel)

	def loadExportGrp(self, sel):
		#clear everything first
		self.clearVal()
		#load
		self.bufferGrp = sel
		self.updateInfo()

		pm.select(cl=True)

	def prepareForSculpt(self):

		if not self.btp:
			om.MGlobal.displayError('No blendshape pattern defined! Set project and blendShape template pattern(BTP) first.')
			return
		
		if not self.bufferGrp:
			om.MGlobal.displayError('Please load Buffer Group to be used as original object for duplicating blendShapes.')
			return

		# self.exportGrp = pm.group(n='export_grp', em=True)
		if pm.objExists('|bshTgt_grp'):
			self.bshTgtGrp = pm.PyNode('|bshTgt_grp')
		else:
			self.bshTgtGrp = pm.group(n='bshTgt_grp', em=True)
			
		if pm.objExists('|delete_grp'):
			self.tmpGrp = pm.PyNode('|delete_grp')
		else:
			self.tmpGrp = pm.group(n='delete_grp', em=True)

		currentGap = float(self.gsFloatFld.getValue())

		# misc.addStrAttr(self.bufferGrp, '_TYPE', txt='exportGrp', lock=True)
		misc.addStrAttr(self.bufferGrp, '_PROJ', txt=self.focusProjName, lock=True)
		misc.addStrAttr(self.bufferGrp, '_BTP', txt=self.focusBtpName, lock=True)
		misc.addStrAttr(self.bufferGrp, '_GAP', txt=currentGap, lock=True)
		misc.addStrAttr(self.tmpGrp, '_TYPE', txt='tmpGrp', lock=True)

		self.btp.gap = currentGap
		self.btp.create(self.bufferGrp)

		pm.parent(self.btp.partGrps.values(), self.bshTgtGrp)
		pm.parent([self.bshTgtGrp, self.btp.txtGrp], self.tmpGrp)

		self.reportBuff(self.bshTgtGrp, 'bshTgtGrp')
		self.reportBuff(self.tmpGrp, 'tmpGrp')
		self.reportBuff(self.btp.txtGrp, 'txtGrp')

		#load export grp
		self.loadExportGrpUi(self.bufferGrp)

		#clear selection
		pm.select(cl=True)

	def reportBuff(self, obj, attr):
		source = misc.addMsgAttr(self.bufferGrp, attr)
		destination = misc.addMsgAttr(obj, 'networkParent')
		if pm.isConnected(source, destination) == False:
			pm.connectAttr(source, destination, f=True)

	def report(self, pObj, cObj, pAttr, cAttr):
		source = misc.addMsgAttr(pObj, pAttr)
		destination = misc.addMsgAttr(cObj, cAttr)
		if pm.isConnected(source, destination) == False:
			pm.connectAttr(source, destination, f=True)

	def removeUnusedAttr(self, obj, attr):
		try:
			if obj.hasAttr(attr):
				obj.attr(attr).delete()
		except:
			pass

	def removePart(self):
		self.getFocusParts()
		if not self.focusPartNames:
			return
		toDel = None
		for part in self.focusPartNames:
			partGrp = self.partGrps[part]
			toDel = self.getTxtFromPart(partGrp)
			toDel.append(partGrp)
			pm.delete(toDel)
			del self.partGrps[part]

		self.updateInfo()

	def removeTarget(self, tsl):
		if not self.focusTgts:
			return
		selTgt = tsl.getSelectItem()
		if not selTgt:
			return
		i = 0
		for t in selTgt:
			if t == '':
				continue
			partAttr = self.getAttrFromTarget(self.focusTgts[t])
			txt = self.getTxtFromTarget(self.focusTgts[t])
			inbs = self.getInbTargets(self.focusTgts[t])
			pm.delete(self.focusTgts[t], txt)
			if inbs:
				pm.delete(inbs)
			if partAttr.isConnected() == False:
				partAttr.delete()
			i += 1
		if i > 0:
			self.updateInfo()

	def removeInbTarget(self):
		self.updateInfo()
		self.getCurrentTgtTSL()

		if not self.focusTgts:
			return

		self.getWidth(self.bufferGrp)
		self.getFocusParts()
		for part in self.focusParts:
			for target in self.getTargetsFromPart(part):
				inbs = self.getInbTargets(target)
				if not inbs:
					continue
				for inb in inbs:
					if inb in self.selInbs:
						delPos = pm.xform(inb, q=True, ws=True, t=True)
						pm.delete(inb)
						toMove, txts = [], []
						for t in self.getTargetsFromPart(part):		
							if pm.xform(t, q=True, ws=True, t=True)[0] > delPos[0]:
								toMove.append(t)
								inbetweens = self.getInbTargets(t)
								if inbetweens:
									moveInb = [tInb for tInb in inbetweens if pm.xform(tInb, q=True, ws=True, t=True)[0] > delPos[0]]
									toMove.extend(moveInb)
								txt = self.getTxtFromTarget(t)
								if txt:
									txts.append(txt)

						pm.xform(toMove, t=[self.width*-1, 0, 0], r=True)
						if txts:
							for txt in txts:
								misc.lockAttr(txt, t=True, lock=False)
							pm.xform(txts, t=[self.width*-1, 0, 0], r=True)
							for txt in txts:
								misc.lockAttr(txt, t=True, lock=False)

				if not self.getInbTargets(target):
					target.INB.delete()

		#select all in target including inbs
		self.currentTsl.setSelectItem(self.selTgt)
		self.selectFocusTarget(self.currentRangeVal)

	def setProject(self):
		try:
			self.focusProjName = self.projMenu.getValue()
			self.btpMenu.clear()
			pm.menuItem(l='', p=self.btpMenu)
		except: pass

		if not self.focusProjName:
			self.currentProj = None
			return
		self.currentProj = self.projs[self.focusProjName]
		reload(self.currentProj)

		members = inspect.getmembers(sys.modules[self.currentProj.__name__], inspect.isclass)
		classes = []
		for m in members:
			classes.append(m[1])

		for c in sorted(classes, key=lambda cls: cls.__name__):
			clsName = c.__name__
			self.btps[clsName] = c
			try:
				pm.menuItem(l=clsName, p=self.btpMenu)
			except:
				pass

	def setPartVisibility(self, vis=True):
		self.getFocusParts()
		toHide = []
		for part in self.focusParts:
			toHide.append(part)
			txts = self.getTxtFromPart(part)
			if txts:
				toHide.extend(txts)

		for item in toHide:
			if not item:
				continue
			if item.hasAttr('visibility'):
				vAttr = item.attr('visibility')
				if vAttr.isLocked() == True:
					vAttr.setLocked(False)
				vAttr.set(vis)

	def showNewPartPromptWin(self):
		pTxtFld = ui.PromptTxtFld(title='New Part', message='Name:')
		return pTxtFld.txt

	def selectBtpUi(self):
		self.focusBtpName = self.btpMenu.getValue()
		self.selectBtp()
		# self.updatePatternDetail()

	def selectBtp(self):
		if not self.focusBtpName:
			self.btp = None
			return

		self.btp = self.btps[self.focusBtpName](gap=self.gap)

	def selectBufferGrp(self):
		if not self.bufferGrp:
			return
		pm.select(self.bufferGrp, r=True)

	def selectFocusInb(self):
		self.selInbs = []
		selected = self.inbTSL.getSelectItem()
		for inb in self.focusInbs:
			if inb.nodeName() in selected:
				self.selInbs.append(inb)
		if self.selInbs:
			pm.select(self.selInbs, r=True)

	def updateAliasTxtFld(self):
		self.aliasTxtFld.setText('')

		partPlug = None
		if self.selTargets:
			selTarget = self.selTargets[0]
			if selTarget.hasAttr('n'):
				partPlug = selTarget.n
			elif selTarget.hasAttr('p'):
				partPlug = selTarget.p
			else:
				return

			targetIns = partPlug.inputs(type='transform', p=True)
			if targetIns:
				ctrlAttrName = targetIns[0].longName()
				self.aliasTxtFld.setText(ctrlAttrName)

	def selectFocusTarget(self, rangeVal):
		# self.resetTestSlider()

		if rangeVal == 'p':
			tsl = self.targetTSLP
			ptsl = self.targetTSLN
		elif rangeVal == 'n':
			tsl = self.targetTSLN
			ptsl = self.targetTSLP

		self.selTgt = tsl.getSelectItem()
		selTgtIndex = tsl.getSelectIndexedItem()
		ptsl.deselectAll()
		ptsl.setSelectIndexedItem(selTgtIndex)

		self.selTargets = []
		self.focusInbs = []
		self.focusInbsDic = {}
		self.testTargets = []
		toSel = []

		for sel in self.selTgt:
			if sel not in self.focusTgts.keys() or not sel:
				continue
			target = self.focusTgts[sel]
			if target:
				inbs = self.getInbTargets(target)
				if inbs:
					toSel.extend(inbs)
				toSel.append(target)
				self.selTargets.append(target)
				self.focusInbsDic[target] = inbs

		# if not toSel:
		# 	pm.select(cl=True)
		# else:
		# 	pm.select(toSel, r=True)

		self.updateInbTSL()
		self.updateAliasTxtFld()

	def updateGlobalScale(self):
		fldVal = self.gsFloatFld.getValue()
		self.gap = fldVal
		try:
			self.btp.gap = self.gap 
		except Exception:
			pass

	def utilizePattern(self):
		#get file in btp
		for f in os.listdir(os.path.dirname(btp.__file__)):
			if f.endswith('.py') and not f.startswith('__') and f != 'btpBase.py':
				modName = f.split('.')[0]
				exec('from btp import %s' %modName)
				exec('self.projs[modName] = %s' %modName)

	def updateTargetTSL(self):
		preSelN = self.targetTSLN.getSelectItem()
		preSelP = self.targetTSLP.getSelectItem()

		self.targetTSLP.removeAll()
		self.targetTSLN.removeAll()
		self.focusTgts = {}

		selParts = self.getSelectedParts(getTxt=False)
		if not selParts:
			return

		self.focusPart = selParts
		for part in selParts:
			targetDic = self.targetDic[part.nodeName()]
			for kAttr, dic in sorted(targetDic.iteritems()):
				for rangeVal, target in sorted(dic.iteritems()):
					if target:
						if rangeVal == 'p':
							self.targetTSLP.append(target.nodeName())
							self.focusTgts[target.nodeName()] = target
							continue
						elif rangeVal == 'n':
							self.targetTSLN.append(target.nodeName())
							self.focusTgts[target.nodeName()] = target
							continue
					else:
						if rangeVal == 'p':
							self.targetTSLP.append('')
						elif rangeVal == 'n':
							self.targetTSLN.append('')

		toSelN = [i for i in preSelN if i in self.targetTSLN.getAllItems()]
		self.targetTSLN.setSelectItem(toSelN)
		toSelP = [i for i in preSelP if i in self.targetTSLP.getAllItems()]
		self.targetTSLP.setSelectItem(toSelP)

		self.selectFocusTarget(self.currentRangeVal)

	def updatePartTSL(self):
		preSel = self.partTSL.getSelectItem()
		self.partTSL.removeAll()
		for part in sorted(self.partGrps.keys()):
			self.partTSL.append(part)
		toSel = [sel for sel in preSel if sel in self.partTSL.getAllItems()]
		if toSel:
			self.partTSL.setSelectItem(toSel)

	def updateInbTSL(self):
		self.inbTSL.removeAll()
		self.focusInbs = []
		for inbs in self.focusInbsDic.values():
			for inb in inbs:
				self.inbTSL.append(inb.nodeName())
				self.focusInbs.append(inb)

	def updateInfo(self):
		if not self.bufferGrp:
			return
		
		self.focusProjName = self.bufferGrp._PROJ.get()
		self.focusBtpName = self.bufferGrp._BTP.get()
		self.gap = float(self.bufferGrp._GAP.get())


		#set proj and btp

		try:
			self.projMenu.setValue(self.focusProjName)
			self.setProject()	
		except: pass
		
		try:
			self.btpMenu.setValue(self.focusBtpName)
			self.selectBtp()
		except: pass

		self.getTmpGrp()
		self.getTxtGrp()

		self.getBshTgtGrp()
		# self.getBufferGrp()
		self.getParts()

		self.getWidth(self.bufferGrp)
		self.getHeight(self.bufferGrp)
		
		self.targetDic = {}
		self.allTargets = []


		#get connection info
		self.getNodes()
		self.getBsh()
		# self.getTAttrs()
		# self.getSepAttrs()

		for part in sorted(self.partGrps.values()):
			targetsAttr = []
			attrDic = {}
			tNum = 1
			attrs = [attr for attr in pm.listAttr(part, ud=True) if attr != "PART"]
			if not attrs:
				self.targetDic[part.nodeName()] = {}
			for a in attrs:
				attr = part.attr(a)
				if not attr.get(type=True) == 'message':
					continue
				targetsAttr = pm.listConnections(attr, d=True, s=False, p=True)
				tDic = {'p':None, 'n':None}

				for t in sorted(targetsAttr):
					targetRange = t.shortName()
					target = t.plugNode()

					tDic[targetRange] = target
					self.allTargets.append(target)
					tNumStr = str(tNum)
					attrDic['%s%s' %(tNumStr.zfill(2), attr.shortName())] =  tDic
					self.targetDic[part.nodeName()] = attrDic
					try: 
						bshPlug = self.getBshPlug(target)
					except: pass
				tNum += 1

		try:
			#update part txtFld
			self.updatePartTSL()

			#update target TSL
			self.updateTargetTSL()

			

			#update inb TSL
			self.updateInbTSL()

			#update gs floatFld
			self.gsFloatFld.setValue(self.gap)

		except: pass



	


