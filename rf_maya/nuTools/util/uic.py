import sys, os
from pysideuic import compileUi


input_path = sys.argv[1]

outpout_path = '%s.py' %os.path.splitext(input_path)[0]
pyfile = open(outpout_path, 'w')

compileUi(input_path, pyfile, False, 4, False)
pyfile.close()