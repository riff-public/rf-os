import maya.mel as mel
from maya.cmds import pluginInfo as pluginInfo
from maya.cmds import loadPlugin as loadPlugin

pluginNames = ['averageVertexSkinWeightCmd.mll', 'averageVertexSkinWeightCmd.py']
for pluginName in pluginNames:
	try:
		if not pluginInfo(pluginName, q=True, l=True):
			loadPlugin(pluginName, qt=True)
	except:
		continue

def initPaint():
	cmd = '''
	global string $avgVertexSkinWeightsBrushSel[];

	global proc averageVertexWeightBrush(string $context) {
			artUserPaintCtx -e -ic "init_averageVertexWeightBrush" -svc "set_averageVertexWeightBrush"
			-fc "" -gvc "" -gsc "" -gac "" -tcc "" $context;
	}

	global proc init_averageVertexWeightBrush(string $name) {
	
		global string $avgVertexSkinWeightsBrushSel[];

		$avgVertexSkinWeightsBrushSel = {};
		string $sel[] = `ls -sl -fl`;
		string $vtx;
		int $i = 0;
		for($vtx in $sel) {
			string $buffer[];
			int $number = `tokenize $vtx ".[]" $buffer`;
			$avgVertexSkinWeightsBrushSel[$i] = $buffer[2];
			$i++;
		}  
	}

	global proc set_averageVertexWeightBrush(int $slot, int $index, float $val) {
		global string $avgVertexSkinWeightsBrushSel[];

		if($avgVertexSkinWeightsBrushSel[0] != "") {
			if(!stringArrayContains($index, $avgVertexSkinWeightsBrushSel)) {
				init_averageVertexWeightBrush("");
			}
		}
		averageVertexSkinWeight -i $index -v $val;     
	}
	'''
	mel.eval(cmd)



def paint():
	cmd = '''
	ScriptPaintTool;
	artUserPaintCtx -e -tsc "averageVertexWeightBrush" `currentCtx`;
	'''
	mel.eval(cmd)


initPaint()

