import maya.cmds as mc
import maya.OpenMaya as om
import time
import random

DENSITY = 10        # density of voxels
BORDER = 0.01       # shift border voxels inside a mesh for this amount,
                    # to avoid misses of border voxels due to math on floats

# Float range generator
def frange(start, stop, step):
    r = start
    while not r > stop:
        yield r
        r += step


def main():
    if not mc.ls(sl = True):
        mc.warning('Select mesh to generate voxels.')
        return

    # Get MFnMesh function set for selected mesh
    obj = mc.ls(sl = True)[0]
    selection = om.MSelectionList()
    selection.add(obj)
    obj_mdagpath = om.MDagPath()
    selection.getDagPath(0, obj_mdagpath)
    obj_mfnMesh = om.MFnMesh(obj_mdagpath)

    # Get bounding box for selected mesh and calculate grid step
    bbox = mc.xform (obj, q = True, ws = True, boundingBox = True)
    grp = mc.group(em = True, name = 'grp')
    xlen = bbox[3] - bbox[0]
    ylen = bbox[4] - bbox[1]
    zlen = bbox[5] - bbox[2]
    step = round(((bbox[3] - bbox[0]) - 2* xlen * BORDER) /(DENSITY -1), 3)
    print 'step:', step

    # Init all required flags of MFnMesh.allIntersections()
    rayDirection =  om.MFloatVector(bbox[3]*2, bbox[4]*2, bbox[5]*2) # outer ray point
    faceIds = None
    triIds =None
    idsSorted = False
    space = om.MSpace.kWorld
    maxParam = 999999
    testBothDirections = False
    accelParams = None
    sortHits = True
    hitPoints =  om.MFloatPointArray()
    hitRayParams = om.MFloatArray()
    hitFaces = om.MIntArray()

    voxelCount = 0

    # it's not necessary but we want to know how many time computation takes.
    start_time = time.time()

    # Iterate through 3D grid based on mesh bounding box
    for x in frange(bbox[0] + xlen*BORDER, bbox[3], step):
        for y in frange(bbox[1]+ ylen*BORDER, bbox[4], step):
            for z in frange(bbox[2] + zlen*BORDER, bbox[5], step):
                raySource = om.MFloatPoint(random.random()*x, random.random()*y, random.random()*z)
                # Don't forget to clear hitPoints variable or get unpredictable result
                hitPoints.clear()
                obj_mfnMesh.allIntersections(raySource,
                                             rayDirection,
                                             faceIds,
                                             triIds,
                                             idsSorted,
                                             space,
                                             maxParam,
                                             testBothDirections,
                                             accelParams,
                                             sortHits,
                                             hitPoints,
                                             hitRayParams,
                                             hitFaces,
                                             None,None,None,
                                             0.000001)

                # if number of intersections is odd, point of grid is inside mesh volume
                print hitPoints.length()
                if hitPoints.length()%2 ==1:
                    loc = mc.spaceLocator()[0]
                    mc.setAttr(loc + '.scale',0.05,0.05,0.05)
                    mc.xform(loc, ws = True, t = [hitPoints[0].x, hitPoints[0].y, hitPoints[0].z])
                    mc.parent(loc, grp)
                    voxelCount +=1
    print 'Number of created voxel primitives:', voxelCount
    print 'Script completed in ', round(time.time() - start_time, 4), "seconds"
