import collections

def dict_merge(dicts):
    dct = dicts[0].copy()
    
    for d in dicts[1:]:
        for k, v in d.items():
            if isinstance(dct.get(k), dict) and isinstance(v, collections.Mapping):
                dct[k] = dict_merge([dct[k], v])
            else:
                dct[k] = v

    return dct

'''
a = {u'Geo_Grp': {u'andreaGeo_grp': {u'AndreaHairGeo_grp': {u'Hair01_ply': u'L:/F18/CHARACTER/ANDREA/PASSIVE/PAS_F18_CHAR_ANDREA__Hair01.ma'}}}}
b = {u'Geo_Grp': {u'andreaGeo_grp': {u'AndreaHairGeo_grp': {u'Hair02_ply': u'L:/F18/CHARACTER/ANDREA/PASSIVE/PAS_F18_CHAR_ANDREA__Hair02.ma'}}}}
c = {u'Geo_Grp': {u'andreaGeo_grp': {u'AndreaHairGeo_grp': {u'Hair03_ply': u'L:/F18/CHARACTER/ANDREA/PASSIVE/PAS_F18_CHAR_ANDREA__Hair03.ma'}}}}
d = {u'Geo_Grp': {u'andreaGeo_grp': {u'AndreaHairGeo_grp': {u'Hair04_ply': u'L:/F18/CHARACTER/ANDREA/PASSIVE/PAS_F18_CHAR_ANDREA__Hair04.ma'}}}}
e = {u'Geo_Grp': {u'andreaGeo_grp': {u'AndreaHairGeo_grp': {u'Hair05_ply': u'L:/F18/CHARACTER/ANDREA/PASSIVE/PAS_F18_CHAR_ANDREA__Hair05.ma'}}}}

dict_merge([a, b, c, d, e])
'''