import sys

from PySide import QtCore , QtGui
from shiboken import wrapInstance

import app
reload(app)

def mayaRun():
	'''
	from nuTools.cache.animGPUCacher import run as agcRun
	reload(agcRun)
	agcApp = agcRun.mayaRun()
	'''
	import maya.OpenMayaUI as omui
	global animGPUCacherApp

	try:
		animGPUCacherApp.ui.close()
	except:
		pass
	
	ptr = omui.MQtUtil.mainWindow()
	animGPUCacherApp = app.AnimGPUCacher(parent=wrapInstance(long(ptr), QtGui.QWidget))
	animGPUCacherApp.ui.show()

	return animGPUCacherApp