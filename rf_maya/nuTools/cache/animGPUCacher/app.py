import os
import ctypes
import platform
import subprocess
import re

# maya modules
import pymel.core as pm
import maya.OpenMaya as om
import maya.mel as mel

# QT modules
from PySide import QtCore, QtGui

# ui modules
import ui
reload(ui)

from maya.cmds import pluginInfo, loadPlugin
pluginName = 'gpuCache.mll'
if not pluginInfo(pluginName, q=True, l=True):
	loadPlugin(pluginName, qt=True)

VERSION = 'beta'
PACKAGE_DIR = os.path.dirname(__file__)
BYTE_SIZES = {"GB": 1073741824,
			"MB": 1048576,
			"KB": 1024,
			"B": 1}
OS_DRIVE = os.environ['SYSTEMDRIVE']
CACHE_WF_COL = 7
BASE_DIR_NAME = 'animGpuCache'
TMP_FOLDER_NAME = 'tmp'
REFNODE_ATTR_NAME = '_refNode'

def addMsgAttr(obj, attr, multi=False):
	if obj.hasAttr(attr):
		existingAttr = obj.attr(attr) 
		if existingAttr.type() == 'message':
			return existingAttr
		else:
			om.MGlobal.displayError('Same attibute name of other type exists!')
			return existingAttr

	pm.addAttr(obj, ln=attr, at='message', m=multi)
	newAttr = obj.attr(attr)
	return newAttr

def get_free_space(folder, format="MB"):
	if platform.system() == 'Windows':
		free_bytes = ctypes.c_ulonglong(0)
		ctypes.windll.kernel32.GetDiskFreeSpaceExW(ctypes.c_wchar_p(folder), None, None, ctypes.pointer(free_bytes))
		return (int(free_bytes.value/BYTE_SIZES[format.upper()]), format)
	else:
		return (int(os.statvfs(folder).f_bfree*os.statvfs(folder).f_bsize/BYTE_SIZES[format.upper()]), format)

def disk_status(folder, format='MB'):
	process = subprocess.Popen('fsutil volume diskfree %s' %folder, shell=True, stdout=subprocess.PIPE)
	out, err = process.communicate()
	lines = out.splitlines()
	all_bytes = int(lines[1].split(': ')[-1])
	free_bytes = int(lines[2].split(': ')[-1])

	free_bytes = float(free_bytes/BYTE_SIZES[format])
	all_bytes = float(all_bytes/BYTE_SIZES[format])

	percentage = ((all_bytes - free_bytes)/all_bytes) * 100
	return (free_bytes, all_bytes, round(percentage, 2))


class AnimGPUCacheAsset(pm.FileReference):
	'''
		Subclass of pymel's File Reference object
		This is Per-asset Cached object class
	'''
	def __init__(self, pathOrRefNode, **kwargs):
		super(AnimGPUCacheAsset, self).__init__(pathOrRefNode, **kwargs)

		self.allNodes = []
		self.topObjects = []
		self.geoGrp = None

		self.gpuCacheTr = None
		self.gpuCacheNode = None
		self.displayLayer = None

		# filename
		basename = os.path.basename(self.path)
		self.fileName = os.path.splitext(basename)[0]

		# copy num
		pathWithCpNum = self.withCopyNumber()
		reFind = re.findall(r'{\d*}$', pathWithCpNum)
		self.copyNum = ''
		if reFind:
			self.copyNum = reFind[0][1:-1]
		

	def getNodes(self):
		self.allNodes = self.nodes()
		self.topObjects = []
		self.geos = []
		self.geoGrp = None
		self.displayLayer = None

		# get topObjects and geos
		for node in self.allNodes:
			if not isinstance(node, pm.nt.Transform):
				continue

			parent = node.getParent()
			if not parent or not parent.isReferenced():
				self.topObjects.append(node)

			nodeName = node.nodeName().split(':')[-1]
			if nodeName == 'Geo_Grp':
				self.geoGrp = node

			if not self.displayLayer:
				dispInputs = node.inputs(type='displayLayer')
				if dispInputs:
					self.displayLayer = dispInputs[0]

		self.gpuCacheTr = None
		self.gpuCacheNode = None

		# get gpu cache node
		refNodeOutputs = self.refNode.message.outputs()
		for node in refNodeOutputs:
			shape = node.getShape(type='gpuCache')
			if shape:
				self.gpuCacheTr = node
				self.gpuCacheNode = shape
				break


class AnimGPUCacherUI(QtGui.QMainWindow, ui.Ui_animatorsGPUCacher_MainWindow):
	def __init__(self, parent):
		super(AnimGPUCacherUI, self).__init__(parent)
		self.setupUi(self)

		# modify the title
		self.setWindowTitle(QtGui.QApplication.translate("animatorsGPUCacher_MainWindow", 
													"Anim GPU Cacher %s" %VERSION, None, 
													QtGui.QApplication.UnicodeUTF8))
		# window icon
		self.setWindowIcon(QtGui.QIcon('%s\\ui\\img\\icon.png' %(PACKAGE_DIR)))
		self.clear_pushButton.setIcon(QtGui.QIcon('%s\\ui\\img\\cross_icon.png' %(PACKAGE_DIR)))
		self.openDir_pushButton.setIcon(QtGui.QIcon('%s\\ui\\img\\dir_icon.png' %(PACKAGE_DIR)))

		self.cache_pushButton.setIcon(QtGui.QIcon('%s\\ui\\img\\caution_icon.png' %(PACKAGE_DIR)))
		self.rig_pushButton.setIcon(QtGui.QIcon('%s\\ui\\img\\check_icon.png' %(PACKAGE_DIR)))

class AnimGPUCacher(object):
	'''
	the main application class
	'''
	def __init__(self, parent):
		self.baseDir = self.getBaseDir()

		# ------ UI
		self.ui = AnimGPUCacherUI(parent)

		self.ui.cache_pushButton.clicked.connect(self.cache)
		self.ui.rig_pushButton.clicked.connect(self.unCache)
		self.ui.clear_pushButton.clicked.connect(self.unCacheAll)
		self.ui.openDir_pushButton.clicked.connect(self.openBaseDir)

		# update available space
		self.updateDiskSpace()
	
	def openBaseDir(self):
		os.startfile(self.baseDir)

	def updateDiskSpace(self):
		try:
			free_bytes, all_bytes, percentage = disk_status(OS_DRIVE, 'GB')
			self.ui.diskSpace_progressBar.setValue(percentage)
			self.ui.diskSpace_progressBar.setFormat('%s - %sGB free of %sGB (%%p%%)' %(OS_DRIVE, free_bytes, all_bytes))
		except Exception:
			self.ui.diskSpace_progressBar.setValue(0)
			self.ui.diskSpace_progressBar.setFormat('N/A')

	def getBaseDir(self):
		baseDir = '%s/%s' %(OS_DRIVE, BASE_DIR_NAME)
		if not os.path.exists(baseDir):
			os.mkdir(baseDir)
		return baseDir

	def populate(self):
		refs = pm.listReferences()
		# instance AnimGPUCacheAsset on each ref
		for ref in refs:
			asset = AnimGPUCacheAsset(ref.path)
			asset.getNodes()
			if asset.geoGrp:
				assets[asset.namespace] = asset

		return assets

	def unCacheAll(self):
		gpuCacheNodes = pm.ls(type='gpuCache')
		nodes = []
		for node in gpuCacheNodes:
			nodeTr = node.getParent()
			if nodeTr.hasAttr(REFNODE_ATTR_NAME):
				nodes.append(nodeTr)
		self.unCache(objs=nodes)

	def cache(self):
		# get sel, convert each sel to reference (no duplicates)
		refPaths = set()
		for sel in pm.selected():
			try:
				refPath = pm.referenceQuery(sel, f=True)
				refPaths.add(refPath)
			except RuntimeError:
				continue

		assets = []
		geoGrps = []
		for path in refPaths:
			asset = AnimGPUCacheAsset(path)
			asset.getNodes()

			if asset.geoGrp:
				assets.append(asset)
				geoGrps.append(asset.geoGrp.longName())

		if not geoGrps:
			return

		# pop up to cache table
		cachePaths = self.exportCache(objs=geoGrps)
		pm.namespace(set=':')
		for path, asset in zip(cachePaths, assets):
			dirname = os.path.dirname(path)
			basename = os.path.basename(path)

			newname = '%s__%s%s' %(asset.namespace, asset.fileName, asset.copyNum)
			# newpath = '%s/%s.abc' %(dirname, newname)
			# os.rename('%s/%s' %(dirname, basename), newpath)

			# bring in gpu cache
			gpuNode = pm.createNode('gpuCache')
			gpuTr = gpuNode.getParent()

			gpuTr.rename('%s_gpuCache' %newname)
			gpuNode.rename('%s_gpuCacheShape' %newname)

			# change wireframe colors
			gpuNode.overrideEnabled.set(True)
			gpuNode.overrideColor.set(CACHE_WF_COL)

			# add to display layer (if any)
			if asset.displayLayer:
				asset.displayLayer.addMembers(gpuTr)

			# make connection to refNode
			refNodeAttr =addMsgAttr(gpuTr, REFNODE_ATTR_NAME, multi=False)
			pm.connectAttr(asset.refNode.message, refNodeAttr)

			# set path
			gpuNode.cacheFileName.set(path)

			# hide ref objs
			for obj in asset.topObjects:
				try:
					obj.visibility.set(False)
				except:
					pass

		self.updateDiskSpace()

	def exportCache(self, objs):
		minFrame = pm.playbackOptions(q=True, min=True)
		maxFrame = pm.playbackOptions(q=True, max=True)
		sceneName = pm.sceneName()
		if not sceneName:
			folName = TMP_FOLDER_NAME
		else:
			fileName = os.path.basename(sceneName)
			folName = os.path.splitext(fileName)[0]
		cacheDir = '%s/%s' %(self.baseDir, folName)
		objStr = '\n'.join(objs)

		cmd = '''gpuCache 
		-startTime {} 
		-endTime {}
		-optimize 
		-optimizationThreshold 20000 
		-writeMaterials 
		-dataFormat ogawa 
		-useBaseTessellation 
		-directory "{}" 
		-filePrefix "{}" 
		-clashOption nodeName 
		{}'''
		cmd = cmd.format(minFrame, 
				maxFrame,
				cacheDir,
				folName,
				objStr
				)

		ret = mel.eval(cmd)
		return ret

	def unCache(self, objs=[]):
		if not objs:
			objs = pm.selected(type='transform')

		gpuNodes = []
		cachePaths = []
		refNodes = []
		for obj in objs:
			shp = obj.getShape(type='gpuCache')
			if not shp or not obj.hasAttr(REFNODE_ATTR_NAME):
				continue

			refNodeAttr = obj.attr(REFNODE_ATTR_NAME)
			gpuTrInputs = refNodeAttr.inputs(type='reference')

			# continue if no ref node is connected
			if not gpuTrInputs:
				continue

			gpuNodes.append(obj)
			cachePath = shp.cacheFileName.get()
			cachePaths.append(cachePath)
			refNodes.append(gpuTrInputs[0])

		# delete all gpu nodes
		pm.delete(gpuNodes)
		for path in cachePaths:
			if os.path.exists(path):
				try:
					os.remove(path)
				except:
					om.MGlobal.displayError('Cannot remove cache from disk: %s' %path)
				
		for refNode in refNodes:
			asset = AnimGPUCacheAsset(refNode)

			# if the ref is not loaded
			if not asset.isLoaded():
				asset.load()
			asset.getNodes()

			# turn visibility back on for top nodes
			for topObj in asset.topObjects:
				try:
					topObj.visibility.set(True)
				except Exception, e:
					print e 

