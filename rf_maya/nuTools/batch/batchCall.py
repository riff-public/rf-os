import sys
import maya.standalone as std

std.initialize(name='python')
module = sys.argv[1]
input = sys.argv[2]

from nuTools.batch import batchOperator as batOp
reload(batOp)


def doBatch(module, input):
	# instance the class
	batchOperator = batOp.BatchOperator(module=module, inputs=input)
	status = batchOperator.executeFunc(input)

	stdResult = '_STD_BATCH_RESULT_%s_STD_BATCH_RESULT_' %(int(bool(status)))
	sys.stdout.write(stdResult)

doBatch(module, input)


	