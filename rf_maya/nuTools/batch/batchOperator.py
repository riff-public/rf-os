import pymel.core as pm 
import maya.OpenMaya as om
import subprocess, datetime, pkgutil, sys, os
import logging

import modules as bm

PYTHON_PATH = 'C:/program files/autodesk/maya2016/bin/mayapy.exe'

class BatchOperator(object):
	def __init__(self, 
				module='', 
				inputs=[], 
				pythonPath=PYTHON_PATH):

		self._VERSION = '2.0'
		self.WINDOW_NAME = 'batchOperatorWin'
		self.WINDOW_TITLE = 'Batch Operator %s' %self._VERSION

		self.moduleName = module
		self.module = None
		self.inputs = inputs

		self.pythonPath = pythonPath
		self.batchPath = '%s/batchCall.py' %os.path.dirname(__file__).replace('\\', '/')

		self.ui = False

		# if input is not a list - str, make it a list
		if isinstance(self.inputs, (tuple, list)) == False:
			self.inputs = [str(self.inputs)]

		# try to load function
		if self.moduleName:
			# try:
			self.loadModule(self.moduleName)
			# except Exception, e:
				# print e
				# om.MGlobal.displayWarning('Cannot load batch function specified.')
				# return

	def UI(self):
		if pm.window(self.WINDOW_NAME, ex=True):
			pm.deleteUI(self.WINDOW_NAME, window=True)
		with pm.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, s=True, mnb=True, mxb=True) as self.mainWindow:
			with pm.columnLayout(adj=True, rs=5, co=['both', 0]):
				with pm.columnLayout(adj=True, rs=3, co=['both', 5]):
					with pm.rowColumnLayout(nc=3, rs=(1, 5), co=[(1, 'left', 5), (2, 'left', 5), (3, 'left', 5)]):
						pm.text(l='Operation')
						self.moduleMenu = pm.optionMenu(w=185, cc=pm.Callback(self.selectModule))
						pm.button(l='refresh', w=53, c=pm.Callback(self.getAllModules))

					with pm.frameLayout(l='Inputs', mh=10, cll=True, cl=False):
						with pm.columnLayout(adj=True, rs=5, co=['both', 10]):
							self.inputScrollField = pm.scrollField(ed=True, h=300, wordWrap=False,
													ec=pm.Callback(self.fillInput), cc=pm.Callback(self.fillInput))
							self.inputTSL = pm.textScrollList(vis=False, h=300, fn='fixedWidthFont')

					with pm.frameLayout(lv=False, mh=10, mw=10):
						with pm.columnLayout(adj=True, rs=5, co=['both', 20]):
							self.currentProcessTxt = pm.text(l='No operation')
							self.currentInputTxt = pm.text(l='', h=30, bgc=[0.0, 0.0, 0.0])

					with pm.columnLayout(adj=True, rs=5, cat=['both', 150]):
						with pm.rowColumnLayout(nc=2, co=[(1, 'left', 0), (2, 'left', 5)]):
							pm.text(l='confirm')
							with pm.optionMenu(w=80) as self.confirmMenu:
								pm.menuItem(l='Everytime', parent=self.confirmMenu)
								pm.menuItem(l='If failed', parent=self.confirmMenu)
								pm.menuItem(l='No', parent=self.confirmMenu)
						with pm.rowColumnLayout(nc=1, co=[(1, 'left', 0)]):
							self.stdChkBox = pm.checkBox(l='standalone mode', v=False)
						pm.button(l='Batch!', h=35, c=pm.Callback(self.batch))

					with pm.frameLayout(l='Result', mh=15, cll=True, cl=False):
						with pm.columnLayout(adj=True, rs=5, co=['both', 15]):
							with pm.frameLayout(lv=False, mh=5, mw=5):
								pm.text(l='Success')
								self.successScrollField = pm.scrollField(ed=False, w=240, h=200, wordWrap=False)
							with pm.frameLayout(lv=False, mh=5, mw=5):
								pm.text(l='Failed')
								self.failedScrollField = pm.scrollField(ed=False, w=240, h=200, wordWrap=False)


		self.getAllModules()
		self.ui = True

	def batch(self):
		if not self.inputs or not self.module:
			return

		if self.ui == True:
			self.successScrollField.clear()
			self.failedScrollField.clear()

		status = True
		inputNum = len(self.inputs)
		confirmMode = self.confirmMenu.getSelect()

		for i in range(inputNum):

			if confirmMode == 1:  # confirm everytime
				confirm = True
			elif confirmMode == 2:  # confirm only if Failed
				confirm = not status
			elif confirmMode == 3:  # do not confirm
				confirm = False

			proceed = True
			if confirm == True:
				proceed = self.confirmUser(self.inputs[i])

			if proceed == True:
				if i == 0:
					self.switchInputToTSL()

				self.selectInputTSL(i)
				self.setProcessText(self.inputs[i], i, inputNum)

				if self.stdChkBox.getValue() == True:
					maya = subprocess.Popen([self.pythonPath, self.batchPath, self.moduleName, self.inputs[i]], 
				   		   stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
					out, err = maya.communicate()
					if maya.returncode:
						status = 0
						print err
					else:
						status = int(out.split('_STD_BATCH_RESULT_')[-2])
					# print out, err
					# print 'status is %s' %status
				else:
					status = self.executeFunc(self.inputs[i])

				self.fillResultScrollField(status, self.inputs[i], i)
				
			else:
				self.switchInputToScrollField()
				self.resetProcessText()
				return

		self.switchInputToScrollField()
		self.resetProcessText()

	def confirmUser(self, input):
		press = pm.confirmDialog( title='Confirm Continue', 
				message='Proceed?\n%s' %input,
				button=['Yes', 'No'], defaultButton='Yes', cancelButton='No', dismissString='No' )
		if press == 'Yes':
			return True
		else:
			return False

	def executeFunc(self, input):
		status = self.module.main(input)
		return status

	def fillInput(self):
		txts = self.inputScrollField.getText()

		if txts:
			self.inputs = txts.split()
		else:
			self.inputs = []

	def fillResultScrollField(self, status, input, index):
		if self.ui == False:
			return

		if index > 0:
			toFill = '\n%s' %input
		else:
			toFill = input

		if status:
			field = self.successScrollField
		else:
			field = self.failedScrollField

		field.insertText(toFill)

	def getAllModules(self):
		# reload(bm)
		# modules = inspect.getmembers(sys.modules[bm.__name__], inspect.ismodule)
		# allModules = zip(*modules)[0]
		allModules = []
		for importer, modname, ispkg in pkgutil.iter_modules(bm.__path__):
			allModules.append(modname)
		
		self.moduleMenu.clear()
		pm.menuItem(l='', parent=self.moduleMenu)

		for mod in allModules:
			pm.menuItem(l=mod, parent=self.moduleMenu)

	def loadModule(self, module):
		exec('import modules.%s as currModule;reload(currModule)' %(module))
		self.module = currModule
		self.moduleName = module
		# self.funcName = '%s.%s' %(self.func.__module__, self.func.__name__)

	def resetProcessText(self):
		if self.ui == False:
			return
		self.currentProcessTxt.setLabel('No operation')
		self.currentInputTxt.setLabel('')
		self.currentInputTxt.setBackgroundColor([0.0, 0.0, 0.0])

	def selectModule(self):
		module = self.moduleMenu.getValue()
		if module:
			self.loadModule(module)
			# print (self.func.__doc__)

	def selectInputTSL(self, i):
		self.inputTSL.setSelectIndexedItem(i+1)

	def switchInputToTSL(self):
		if self.ui == False:
			return

		self.inputScrollField.setVisible(False)
		self.inputTSL.setVisible(True)

		self.inputTSL.removeAll()
		for i in self.inputs:
			self.inputTSL.append(i)

	def switchInputToScrollField(self):
		if self.ui == False:
			return

		self.inputTSL.setVisible(False)
		self.inputScrollField.setVisible(True)

	def setProcessText(self, input, current, lenInputs):
		if self.ui == False:
			return

		self.currentProcessTxt.setLabel('Now processing...(%s/%s)' %(current+1, lenInputs))
		self.currentInputTxt.setLabel(input)
		self.currentInputTxt.setBackgroundColor([0.7, 0.7, 0.0])

		pm.refresh(cv=self.currentProcessTxt, fe=True)
		pm.refresh(cv=self.currentInputTxt, fe=True)

