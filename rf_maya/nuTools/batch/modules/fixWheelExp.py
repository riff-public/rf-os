import shutil, os, sys
import re, datetime
sys.path.append('M:/SCRIPTS/MEME/memeXplorer')

import pymel.core as pm
from tool.mergeUv import core as muvCore
import sanitizeFile as sf

# logger
import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
for each in logger.handlers[::-1] :
	if type(each).__name__ == 'StreamHandler':
		logger.removeHandler(each)

	if type(each).__name__== 'FileHandler': 
		logger.removeHandler(each)
		each.flush()
		each.close()
	
def main(inputPath):
	# --- logger
	logDir = 'C:/log'
	if not os.path.exists(logDir):
		os.makedirs(logDir)
	logPath = '%s/fixWheelExp.log' %(logDir)

	# create file handler which logs even debug messages
	fh = logging.FileHandler(logPath)
	fh.setLevel(logging.DEBUG)
	# create console handler with a higher log level
	ch = logging.StreamHandler()
	ch.setLevel(logging.ERROR)
	# create formatter and add it to the handlers
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	fh.setFormatter(formatter)
	ch.setFormatter(formatter)
	# add the handlers to the logger
	logger.addHandler(fh)
	logger.addHandler(ch)

	if not os.path.exists(inputPath):
		logger.error('Input path does not exists: %s' %inputPath)
		return

	pathSplits = os.path.dirname(inputPath).split('/')
	animMdPath = ''
	assetName = ''
	animMdDir = ''
	try:
		assetName = pathSplits[6]
		assetPath = '/'.join(pathSplits[0:7])
		animMdDir = '%s/ref' %assetPath
		animMdPath = '%s/%s_AnimMd.ma' %(animMdDir, assetName)
	except:
		logger.error('Failed getting AmimMd path')
		return

	logger.info('AnimMd path: %s' %(animMdPath))
	if not os.path.exists(animMdPath):
		logger.error('AnimMd does not exists: %s' %animMdPath)
		return
	
	pm.openFile(inputPath, f=True)

	exps = [n for n in pm.ls(type='expression') if n.nodeName().endswith('_exp')]
	if not exps:
		logger.error('No expression with the _exp naming found')
		return

	fixExps = []
	for node in exps:
		logger.info('fixing: %s' %node.nodeName())
		expStr = node.getExpression()
		expLines = expStr.splitlines()
		fixLine = expLines[-3]
		match = re.match('^\tsetAttr .* \$result;$', fixLine)
		if match:
			logger.info('Found broken expression: %s' %match.group())
			fixLine = fixLine.replace('setAttr ', '')
			fixLine = fixLine.replace(' $result;', ' = $result;')
			expLines[-3] = fixLine
			newExp = '\n'.join(expLines)

			logger.info('New expression is: %s' %newExp)
			node.setExpression(newExp)
			fixExps.append(node)
		else:
			logger.info('Found no valid expression string')

	if not fixExps:
		logger.error('No expression with bad setup found')
		return 

	# merge UV
	logger.info('Merging UVs')
	try:
		pm.select('Geo_Grp')
		muvCore.readUVs2(bypass=True)
		
	except:
		logger.error('Merge UV failed')
		return
	
	logger.info('Saving scene')
	pm.saveFile()

	logger.info('Running pose process...')
	pipeTools.importAllRefs()
	pipeTools.removeAllNameSpace()
	pipeTools.deleteAllTurtleNodes()
	pipeTools.deleteAllUnknownNodes()
	pipeTools.deleteExtraDefaultRenderLayer()
	pipeTools.deleteUnusedNodes()
	pipeTools.parentPreConsObj()

	# backup old animMd first
	bkupFol = '%s/.publish' %animMdDir
	dt = datetime.datetime.now()
	bkUpName = '%s_AnimMd_%s-%s-%s_%s-%s-%s.ma' %(assetName, dt.year, 
				dt.month, dt.day, dt.hour, dt.minute, dt.second)
	bkUpPath = '%s/%s' %(bkupFol, bkUpName)
	shutil.copy2(animMdPath, '%s/%s' %(animMdDir, bkUpPath))

	logger.info('Sanitizing file to: %s' %animMdPath)
	sf.sanitizeFile(inputPath, animMdPath)

	# ------------
	logger.removeHandler(fh)

	return True