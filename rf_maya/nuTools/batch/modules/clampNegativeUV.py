import shutil, os
import pymel.core as pm
from nuTools.util import meshDataUtil as mdu
reload(mdu)
from tool.mergeUv import core as muvc
reload(muvc)


# logger
import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
for each in logger.handlers[::-1] :
	if type(each).__name__ == 'StreamHandler':
		logger.removeHandler(each)

	if type(each).__name__== 'FileHandler': 
		logger.removeHandler(each)
		each.flush()
		each.close()

def main(inputPath):
	# --- logger
	inputDir = os.path.dirname(inputPath)
	baseName = os.path.basename(inputPath)
	inputLogPath = '%s/log/%s.log' %(inputDir, os.path.splitext(baseName)[0])

	# create file handler which logs even debug messages
	fh = logging.FileHandler(inputLogPath)
	fh.setLevel(logging.DEBUG)
	# create console handler with a higher log level
	ch = logging.StreamHandler()
	ch.setLevel(logging.ERROR)
	# create formatter and add it to the handlers
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	fh.setFormatter(formatter)
	ch.setFormatter(formatter)
	# add the handlers to the logger
	logger.addHandler(fh)
	logger.addHandler(ch)

	# --------------------------------------------------------------------
	# --------------------------------------------------------------------
	# --- main vars
	GEO_GRP = 'Geo_Grp'
	UV_DATA = 'uv_coords.pkl'
	TOPOLOGY_DATA = 'uv_topology.pkl'
	NORMALS_DATA = 'uv_normals.pkl'
	PROJ_PATH = 'P:/Lego_FRD'
	# --------------------------------------------------------------------
	# --------------------------------------------------------------------
	logger.info('\n\n')
	logger.info('# --------------------------------------------------------------------#')
	logger.info('# %s #' %inputPath)
	logger.info('# --------------------------------------------------------------------#')
	paths = inputPath.split('/')
	logger.info('Split input paths: %s' %paths)
	try:
		imgName = paths[-1].split('.')[0]
	except IndexError, e:
		return

	# get animMd path
	imgs = imgName.split('_')
	logger.info('Split file name with _: %s' %imgs)
	try:
		typ = imgs[0]
		sTyp = imgs[1]
		assetName = '_'.join(imgs[2:])
	except IndexError, e:
		logger.error('Failed getting type, stype, assetName')
		return 

	assetPath = '%s/asset/3D/%s/%s/%s' %(PROJ_PATH, typ, sTyp, assetName)
	animMdPath = '%s/ref/%s_AnimMd.ma' %(assetPath, assetName)
	logger.info('AnimMd path: %s' %animMdPath)
	if not os.path.exists(animMdPath):
		logger.error('AnimMd does not exists: %s' %(animMdPath))
		return 

	logPath = '%s/asset/mapping/UV_LOGS/%s_RenderRsMd.ma' %(PROJ_PATH, assetName)
	logger.info('Log path: %s' %logPath)
	if not os.path.exists(logPath):
		logger.error('Cannot find affected polygon log file: %s' %(logPath))
		return 

	# copy animMd to local
	myDocPath = os.path.expanduser('~')
	locProjDir = '%s/Lego_FRD_fixNegUV' %myDocPath
	if not os.path.exists(locProjDir):
		logger.info('Creating folder: %s' %(locProjDir))
		os.mkdir(locProjDir)
		logger.info('Folder created: %s' %(locProjDir))
	
	locAssetPath = '%s/%s_%s_%s' %(locProjDir, typ, sTyp, assetName)
	logger.info(locAssetPath)
	if not os.path.exists(locAssetPath):
		os.mkdir(locAssetPath)
		logger.info('Folder created: %s' %(locAssetPath))

	localAnimMdPath = '%s/%s_AnimMd.ma' %(locAssetPath, assetName)
	shutil.copy(animMdPath, localAnimMdPath)
	logger.info('Copied from: %s to %s' %(animMdPath, localAnimMdPath))

	# --------------------------------------------------------------------
	# open local animMd
	logger.info('Opening: %s' %localAnimMdPath)
	pm.openFile(localAnimMdPath, f=True)
	logger.info('Opened: %s' %localAnimMdPath)

	# export Geo_Grp to local as uv work file
	if not pm.objExists(GEO_GRP) or len(pm.ls(GEO_GRP)) != 1:
		logger.error('Cannot find Geo_Grp')
		return 

	pm.select(GEO_GRP)
	logger.info('Selected: %s' %GEO_GRP)
	localUvWorkPath = '%s/%s_UVWork.ma' %(locAssetPath, assetName)
	logger.info('UV work path: %s' %localUvWorkPath)
	pm.exportSelected(localUvWorkPath, f=True, ch=False, shader=False, preserveReferences=False, type='mayaAscii')
	logger.info('Exported %s to %s' %(GEO_GRP, localUvWorkPath))

	# ----------------------------------------
	# open local uv work file
	logger.info('Opening local UV work file: %s' %localUvWorkPath)
	pm.openFile(localUvWorkPath, f=True)
	logger.info('Opened local UV work file: %s' %localUvWorkPath)

	logger.info('Getting geos in  local UV work file...')
	allGeos = muvc.getGeoInGrp(GEO_GRP)
	logger.info('\n'.join([n.longName() for n in allGeos]))
	if not allGeos:
		logger.error('No geometry found in local UVWork')
		return

	# read affected geo from data
	aMeshes = []
	logger.info('Reading affected polygon log file...')
	with open(logPath, 'r') as logfile:
		aMeshes = logfile.readlines()
	aMeshes = [m.replace('\n', '') for m in aMeshes]
	aMeshes = ['|'.join(m.split('|')[:-1]) for m in aMeshes]
	logger.info('\n'.join(aMeshes))

	# apply UV clamp
	logger.info('Clamping meshes UV...')
	for meshName in aMeshes:
		if pm.objExists(meshName):
			logger.info('Clamping: %s' %meshName)
			tr = pm.PyNode(meshName)
			shp = tr.getShape(ni=True)
			clampUvToPositiveSpace(shp)
			logger.info('Clamp done: %s' %meshName)
		else:
			logger.error('Cannot find: %s' %meshName)

	# export UV data to local
	locDataPath = '%s/meshData' %locAssetPath
	logger.info('Local UV data path: %s' %locDataPath)
	if not os.path.exists(locDataPath):
		logger.info('Creating folder: %s' %(locDataPath))
		os.mkdir(locDataPath)
		logger.info('Folder created: %s' %(locDataPath))
		
	topoPath = '%s/%s' %(locDataPath, TOPOLOGY_DATA)
	nmlPath = '%s/%s' %(locDataPath, NORMALS_DATA)
	uvPath = '%s/%s' %(locDataPath, UV_DATA)

	topoData = mdu.writeTopologyData(objs=allGeos, path=topoPath)
	logger.info('Wrote to local topology data path: %s' %(topoPath))

	normalsData = mdu.writeNormalsData(objs=allGeos, path=nmlPath)
	logger.info('Wrote to local normals data path: %s' %(nmlPath))

	uvData = mdu.writeUvData(objs=allGeos, path=uvPath)
	logger.info('Wrote to local UV data path: %s' %(uvPath))

	# save overwrite local uv work file
	pm.saveFile()
	logger.info('Scene saved: %s' %localUvWorkPath)

	# ----------------------------------------
	# open local animMd
	logger.info('Opening: %s' %localAnimMdPath)
	pm.openFile(localAnimMdPath, f=True)
	logger.info('Opened: %s' %localAnimMdPath)

	# apply UV data to local animMd
	allGeos = muvc.getGeoInGrp(GEO_GRP)
	logger.info('\n'.join([n.longName() for n in allGeos]))

	datas = None
	logger.info('Reading local UV datas...')
	if all([os.path.exists(path) for path in (topoPath, nmlPath, uvPath)]):
		topoData = mdu.readData(path=topoPath)
		logger.info('Read topology data from: %s' %topoPath)

		normalsData = mdu.readData(path=nmlPath)
		logger.info('Read normals data from: %s' %nmlPath)

		uvData = mdu.readData(path=uvPath)
		logger.info('Read UV data from: %s' %uvPath)


	datas = (topoData, normalsData, uvData)
	result = muvc.mergeUVs(allGeos, datas, bypass=True)
	all_result = all(result.values())

	pm.saveFile()

	logger.info('readUVs2 result: %s' %all_result)
	if not all_result:
		return 

	# ------------
	# backup uv data on server*
	meshDataFolPath = '%s/uv/uv_md/data/meshData' %(assetPath)
	if os.path.exists(meshDataFolPath):
		meshDataBkFolPath = '%s/uv/uv_md/data/_bkup' %(assetPath)
		if not os.path.exists(meshDataBkFolPath):
			os.mkdir(meshDataBkFolPath)
			logger.info('Folder created: %s' %meshDataBkFolPath)

		# only back up if there isn't already one
		meshDataDes = '%s/meshData_fixNegUVBkUp' %meshDataBkFolPath
		if not os.path.exists(meshDataDes):
			shutil.copytree(meshDataFolPath, meshDataDes)
			logger.info('Backup: %s to %s' %(meshDataFolPath, meshDataDes))

	# backup animMd on server*
	animMdBkFolPath = '%s/ref/_bkup' %(assetPath)
	if not os.path.exists(animMdBkFolPath):
		logger.info('Creating folder: %s' %(animMdBkFolPath))
		os.mkdir(animMdBkFolPath)
		logger.info('Folder created: %s' %animMdBkFolPath)

	animMdDes = '%s/%s_AnimMd_fixNegUVBkUp.ma' %(animMdBkFolPath, assetName)
	shutil.copyfile(animMdPath, animMdDes)
	logger.info('Backup: %s to %s' %(animMdPath, animMdDes))

	# copy local UV data to server (overwrite)**
	logger.info('----- PUBLISHING -----')

	if os.path.exists(meshDataFolPath):
		shutil.rmtree(meshDataFolPath)
	shutil.copytree(locDataPath, meshDataFolPath)
	logger.info('Copied: %s to %s' %(locDataPath, meshDataFolPath))

	# copy local animMd to overwrite one on the server (overwrite)**
	shutil.copyfile(localAnimMdPath, animMdPath)
	logger.info('Copied: %s to %s' %(localAnimMdPath, animMdPath))

	# ------------
	logger.removeHandler(fh)

	return True

def clampUvToPositiveSpace(mesh):
	us, vs = mesh.getUVs()
	uMin = 0.0
	uMax = 1.0
	vMin = 0.0
	vMax = 1.0
	newUs, newVs = [], []
	i = 0
	for u, v in zip(us, vs):
		newU = u 
		if u < 0:
			newU = max(min(u, uMax), uMin)
		newV = v
		if v < 0:
			newV = max(min(v, vMax), vMin)

		if [newU, newV] != [u, v]:
			pm.polyEditUV(mesh.map[i], relative=False, uValue=newU, vValue=newV)
			
		i += 1