import os
import maya.cmds as mc

# logger
import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
for each in logger.handlers[::-1] :
	if type(each).__name__ == 'StreamHandler':
		logger.removeHandler(each)

	if type(each).__name__== 'FileHandler': 
		logger.removeHandler(each)
		each.flush()
		each.close()

def main(inputPath):
	if not os.path.exists(inputPath):
		return 'Failed', 'Input path does not exits!: %s' %inputPath

	#bad UV files are stored here
	logDir = 'P:/Lego_FRD/asset/mapping/UV_LOGS'
	#get name of any asset
	pathSplits = inputPath.split('/')
	assetType = pathSplits[4]
	assetSubType = pathSplits[5]
	assetName = pathSplits[6]
	#get path to any asset in UV_LOGS
	logPath = '%s/%s_RenderRsMd.ma' %(logDir, assetName)
	
	#check if file exists
	if not os.path.exists(logPath):
		return 

	#open UV_LOGS/*_RenderRsMd.ma, get mesh names
	with open(logPath, 'r') as logfile:
		meshes = logfile.readlines()
	meshes = [m.replace('\n', '') for m in meshes]
	meshes = ['|'.join(m.split('|')[:-1]) for m in meshes]

	# open the animMd scene
	animMDFile = inputPath
	mc.file(animMDFile, f=True, typ="mayaAscii", o=True)

	missingMeshes = [m for m in meshes if not mc.objExists(m)]
	if missingMeshes: 
		return 
		
	# get the affected polygons
	visPlane = mc.polyPlane(ch=False)[0]
	meshes.append(visPlane)
	mc.select(meshes, r=True) 

	# make up UV snapshot path
	snapShDir = "P:/Lego_FRD/asset/mapping/UV_SNAPSHOTS"
	snapShPath = snapShDir + '/' + assetType + '_' + assetSubType + '_' + assetName + ".jpg"  
	# print snapShPath

	# do UV snapShot
	mc.uvSnapshot( o=True, euv = True, n=snapShPath, xr=1024, yr=1024, ff="jpg")
	# mc.delete(visPlane)



