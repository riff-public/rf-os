import shutil
import os


# logger
import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
for each in logger.handlers[::-1] :
	if type(each).__name__ == 'StreamHandler':
		logger.removeHandler(each)

	if type(each).__name__== 'FileHandler': 
		logger.removeHandler(each)
		each.flush()
		each.close()

# srcPath = P:/Lego_FRD/asset/3D/base/animal/
# desPath = C:/WORK/F18/RIGLIBRARY/ANIMAL
def move_ctrlRig_work(srcPath, desPath):
	# --- logger
	inputDir = os.path.dirname(srcPath)

	inputLogPath = '%s/move.log' %desPath
	# create file handler which logs even debug messages
	fh = logging.FileHandler(inputLogPath)
	fh.setLevel(logging.DEBUG)
	# create console handler with a higher log level
	ch = logging.StreamHandler()
	ch.setLevel(logging.ERROR)
	# create formatter and add it to the handlers
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	fh.setFormatter(formatter)
	ch.setFormatter(formatter)
	# add the handlers to the logger
	logger.addHandler(fh)
	logger.addHandler(ch)

	for assetName in os.path.listdir(srcPath):
		rigPath = '%s/%s/rig' %(srcPath, assetName)
		if not os.path.exists(rigPath):
			logger.error('path doesnt exist: %s' %rigPath)
			continue

		files = os.path.listdir('/hero' %rigPath)
		# move the control rig
		


