# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:/Users/TA/Dropbox/script_server/core/maya/rftool/prop_it/ui.ui'
#
# Created: Mon Apr 03 14:24:44 2017
#      by: pyside-uic 0.2.14 running on PySide 1.2.0
#
# WARNING! All changes made in this file will be lost!

from Qt import QtCore, QtWidgets

class Ui_PropItUI(object):
    def setupUi(self, PropItUI):
        PropItUI.setObjectName("PropItUI")
        PropItUI.resize(315, 509)
        self.centralwidget = QtWidgets.QWidget(PropItUI)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.gridLayout_3 = QtWidgets.QGridLayout()
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.project_label_2 = QtWidgets.QLabel(self.centralwidget)
        font = QtWidgets.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.project_label_2.setFont(font)
        self.project_label_2.setObjectName("project_label_2")
        self.gridLayout_3.addWidget(self.project_label_2, 0, 0, 1, 1)
        self.project_label = QtWidgets.QLabel(self.centralwidget)
        self.project_label.setObjectName("project_label")
        self.gridLayout_3.addWidget(self.project_label, 0, 1, 1, 1)
        self.project_label_3 = QtWidgets.QLabel(self.centralwidget)
        font = QtWidgets.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.project_label_3.setFont(font)
        self.project_label_3.setObjectName("project_label_3")
        self.gridLayout_3.addWidget(self.project_label_3, 1, 0, 1, 1)
        self.type_label = QtWidgets.QLabel(self.centralwidget)
        self.type_label.setObjectName("type_label")
        self.gridLayout_3.addWidget(self.type_label, 1, 1, 1, 1)
        self.project_label_4 = QtWidgets.QLabel(self.centralwidget)
        font = QtWidgets.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.project_label_4.setFont(font)
        self.project_label_4.setObjectName("project_label_4")
        self.gridLayout_3.addWidget(self.project_label_4, 2, 0, 1, 1)
        self.project_label_5 = QtWidgets.QLabel(self.centralwidget)
        font = QtWidgets.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.project_label_5.setFont(font)
        self.project_label_5.setObjectName("project_label_5")
        self.gridLayout_3.addWidget(self.project_label_5, 3, 0, 1, 1)
        self.subtype_label = QtWidgets.QLabel(self.centralwidget)
        self.subtype_label.setObjectName("subtype_label")
        self.gridLayout_3.addWidget(self.subtype_label, 2, 1, 1, 1)
        self.asset_label = QtWidgets.QLabel(self.centralwidget)
        self.asset_label.setObjectName("asset_label")
        self.gridLayout_3.addWidget(self.asset_label, 3, 1, 1, 1)
        self.project_label_6 = QtWidgets.QLabel(self.centralwidget)
        font = QtWidgets.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.project_label_6.setFont(font)
        self.project_label_6.setObjectName("project_label_6")
        self.gridLayout_3.addWidget(self.project_label_6, 4, 0, 1, 1)
        self.id_label = QtWidgets.QLabel(self.centralwidget)
        self.id_label.setObjectName("id_label")
        self.gridLayout_3.addWidget(self.id_label, 4, 1, 1, 1)
        self.gridLayout_3.setColumnStretch(0, 1)
        self.gridLayout_3.setColumnStretch(1, 3)
        self.verticalLayout_2.addLayout(self.gridLayout_3)
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout_2.addWidget(self.line_2)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.ref_checkBox = QtWidgets.QCheckBox(self.centralwidget)
        self.ref_checkBox.setChecked(True)
        self.ref_checkBox.setObjectName("ref_checkBox")
        self.gridLayout.addWidget(self.ref_checkBox, 7, 0, 1, 1)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 2, 0, 1, 1)
        self.export_pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.export_pushButton.setMinimumSize(QtCore.QSize(0, 40))
        self.export_pushButton.setObjectName("export_pushButton")
        self.gridLayout.addWidget(self.export_pushButton, 5, 0, 1, 1)
        self.center_pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.center_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.center_pushButton.setObjectName("center_pushButton")
        self.gridLayout.addWidget(self.center_pushButton, 0, 0, 1, 1)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.horizontalLayout_4.addWidget(self.label)
        self.res_comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.res_comboBox.setObjectName("res_comboBox")
        self.horizontalLayout_4.addWidget(self.res_comboBox)
        self.horizontalLayout_4.setStretch(0, 1)
        self.horizontalLayout_4.setStretch(1, 2)
        self.gridLayout.addLayout(self.horizontalLayout_4, 3, 0, 1, 1)
        self.deleteProxy_checkBox = QtWidgets.QCheckBox(self.centralwidget)
        self.deleteProxy_checkBox.setChecked(True)
        self.deleteProxy_checkBox.setObjectName("deleteProxy_checkBox")
        self.gridLayout.addWidget(self.deleteProxy_checkBox, 6, 0, 1, 1)
        self.createRig_pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.createRig_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.createRig_pushButton.setObjectName("createRig_pushButton")
        self.gridLayout.addWidget(self.createRig_pushButton, 1, 0, 1, 1)
        self.asm_checkBox = QtWidgets.QCheckBox(self.centralwidget)
        self.asm_checkBox.setChecked(True)
        self.asm_checkBox.setObjectName("asm_checkBox")
        self.gridLayout.addWidget(self.asm_checkBox, 8, 0, 1, 1)
        self.snap_verticalLayout = QtWidgets.QVBoxLayout()
        self.snap_verticalLayout.setObjectName("snap_verticalLayout")
        self.gridLayout.addLayout(self.snap_verticalLayout, 4, 0, 1, 1)
        self.verticalLayout_2.addLayout(self.gridLayout)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.line_3 = QtWidgets.QFrame(self.centralwidget)
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.verticalLayout_2.addWidget(self.line_3)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.propCam_pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.propCam_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.propCam_pushButton.setObjectName("propCam_pushButton")
        self.horizontalLayout_6.addWidget(self.propCam_pushButton)
        self.currentCam_pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.currentCam_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.currentCam_pushButton.setObjectName("currentCam_pushButton")
        self.horizontalLayout_6.addWidget(self.currentCam_pushButton)
        self.verticalLayout_2.addLayout(self.horizontalLayout_6)
        self.line_4 = QtWidgets.QFrame(self.centralwidget)
        self.line_4.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_4.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.verticalLayout_2.addWidget(self.line_4)
        self.duplicateReference_pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.duplicateReference_pushButton.setMinimumSize(QtCore.QSize(0, 40))
        self.duplicateReference_pushButton.setObjectName("duplicateReference_pushButton")
        self.verticalLayout_2.addWidget(self.duplicateReference_pushButton)
        PropItUI.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(PropItUI)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 315, 21))
        self.menubar.setObjectName("menubar")
        PropItUI.setMenuBar(self.menubar)

        self.retranslateUi(PropItUI)
        QtCore.QMetaObject.connectSlotsByName(PropItUI)

    def retranslateUi(self, PropItUI):
        PropItUI.setWindowTitle(QtWidgets.QApplication.translate("PropItUI", "MainWindow", None, QtWidgets.QApplication.UnicodeUTF8))
        self.project_label_2.setText(QtWidgets.QApplication.translate("PropItUI", "Project : ", None, QtWidgets.QApplication.UnicodeUTF8))
        self.project_label.setText(QtWidgets.QApplication.translate("PropItUI", "-", None, QtWidgets.QApplication.UnicodeUTF8))
        self.project_label_3.setText(QtWidgets.QApplication.translate("PropItUI", "Type : ", None, QtWidgets.QApplication.UnicodeUTF8))
        self.type_label.setText(QtWidgets.QApplication.translate("PropItUI", "-", None, QtWidgets.QApplication.UnicodeUTF8))
        self.project_label_4.setText(QtWidgets.QApplication.translate("PropItUI", "SubType : ", None, QtWidgets.QApplication.UnicodeUTF8))
        self.project_label_5.setText(QtWidgets.QApplication.translate("PropItUI", "SubType : ", None, QtWidgets.QApplication.UnicodeUTF8))
        self.subtype_label.setText(QtWidgets.QApplication.translate("PropItUI", "-", None, QtWidgets.QApplication.UnicodeUTF8))
        self.asset_label.setText(QtWidgets.QApplication.translate("PropItUI", "-", None, QtWidgets.QApplication.UnicodeUTF8))
        self.project_label_6.setText(QtWidgets.QApplication.translate("PropItUI", "ID : ", None, QtWidgets.QApplication.UnicodeUTF8))
        self.id_label.setText(QtWidgets.QApplication.translate("PropItUI", "-", None, QtWidgets.QApplication.UnicodeUTF8))
        self.ref_checkBox.setText(QtWidgets.QApplication.translate("PropItUI", "Replace Proxy with Reference", None, QtWidgets.QApplication.UnicodeUTF8))
        self.export_pushButton.setText(QtWidgets.QApplication.translate("PropItUI", "Export", None, QtWidgets.QApplication.UnicodeUTF8))
        self.center_pushButton.setText(QtWidgets.QApplication.translate("PropItUI", "Center Asset", None, QtWidgets.QApplication.UnicodeUTF8))
        self.label.setText(QtWidgets.QApplication.translate("PropItUI", "Res", None, QtWidgets.QApplication.UnicodeUTF8))
        self.deleteProxy_checkBox.setText(QtWidgets.QApplication.translate("PropItUI", "Delete Proxy", None, QtWidgets.QApplication.UnicodeUTF8))
        self.createRig_pushButton.setText(QtWidgets.QApplication.translate("PropItUI", "Create Rig", None, QtWidgets.QApplication.UnicodeUTF8))
        self.asm_checkBox.setText(QtWidgets.QApplication.translate("PropItUI", "Assembly Reference", None, QtWidgets.QApplication.UnicodeUTF8))
        self.propCam_pushButton.setText(QtWidgets.QApplication.translate("PropItUI", "-", None, QtWidgets.QApplication.UnicodeUTF8))
        self.currentCam_pushButton.setText(QtWidgets.QApplication.translate("PropItUI", "-", None, QtWidgets.QApplication.UnicodeUTF8))
        self.duplicateReference_pushButton.setText(QtWidgets.QApplication.translate("PropItUI", "Duplicate Reference", None, QtWidgets.QApplication.UnicodeUTF8))

