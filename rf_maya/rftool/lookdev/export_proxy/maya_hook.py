import os
import sys 
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
from rf_utils.context import context_info 
from rftool.utils import maya_utils


def export_selection(level): 
	""" export selected object to input level """ 
	selection = mc.ls(sl=True, l=True)[0] if mc.ls(sl=True, l=True) else []
	asset = context_info.ContextPathInfo()
	path = asset.path.hero().abs_path() # $RFPROJECT/Two_Heroes/asset/setDress/garbage/ruinConstructionB/lib 
	asset.context.update(task=level)
	filename = asset.publish_name(hero=True) # ruinConstructionB_rigRen_md.ma
	exportPath = '%s/%s' % (path, filename)

	if mc.objExists(selection): 
		mc.select(selection)
		result = maya_utils.export_selection(exportPath, selection)
		logger.info(exportPath)

	else: 
		logger.warning('Select an object to export')

