_uiName = 'exportProxy'
import os 
import sys 
from functools import partial 
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 

from rftool.utils.ui import maya_win
from rftool.render.redshift import rs_utils
from rftool.utils import path_info 
import maya_hook
reload(maya_hook)

class Config: 
	geoGrp = 'Geo_Grp'
	step = 'rsproxy'
	rsExt = 'rs'
	res = 'md'
	buildMode = 1
	displayPercentage = 10

class ExportProxyUI(object):
	"""docstring for UI"""
	def __init__(self):
		super(ExportProxyUI, self).__init__()
		

	def ui(self): 
		mc.window(_uiName, t='Export Proxyy Tool')
		mc.columnLayout(adj=1, rs=4)
		mc.text(l='Export Proxy')
		self.geoInputTX = mc.textField(tx=Config.geoGrp)
		mc.button(l='Create', h=30, c=partial(self.create))
		mc.button(l='Export', h=30, c=partial(self.export))
		mc.separator()
		mc.text(l='Export file selection', al='center')
		mc.button(l='RigRen.ma', h=30, c=partial(self.export_selection, 'rigRen_md'))
		mc.button(l='rsproxy.ma', h=30, c=partial(self.export_selection, 'rsproxy_md'))
		mc.showWindow()
		mc.window(_uiName, e=True, wh=[200, 200])


	def proxy_path(self): 
		asset = path_info.PathInfo()
		path = '%s/%s' % (asset.libPath(), asset.libName(Config.step, Config.res, ext=Config.rsExt))
		return path 

	def create(self, *args): 
		""" create proxy node """
		proxyPath = self.proxy_path()
		
		if os.path.exists(proxyPath): 
			asset = path_info.PathInfo()
			result = rs_utils.create_proxy(nodeName=asset.name, proxyPath=proxyPath, displayMode=Config.buildMode, displayPercentage=Config.displayPercentage, force=True)

		else: 
			logger.info('Path not exists: %s' % proxyPath)

	def export(self, *args): 
		""" export rs only """ 
		geoGrp = mc.textField(self.geoInputTX, q=True, tx=True)
		path = self.proxy_path()
		result = rs_utils.export_rsProxy(path, geoGrp, connectivity=0, animation=0, start=0, end=0)

	def export_selection(self, level, *args): 
		return maya_hook.export_selection(level)



def show(): 
	maya_win.deleteUI(_uiName)
	app = ExportProxyUI()
	app.ui()
		