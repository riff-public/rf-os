# v.0.0.1 polytag switcher
_title = 'Lookdev Export'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'LookdevExportUI'

#Import python modules
import sys
import os 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework modules 
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui


class LookdevExport(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(LookdevExport, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        
        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.ui.exportProxy_pushButton.clicked.connect(self.export_proxy)
        self.ui.updateProxy_pushButton.clicked.connect(self.update_proxy)
        self.ui.exportLookdev_pushButton.clicked.connect(self.export)

    def export(self): 
        print 'OK'

    def export_proxy(self): 
        pass 
        
    def update_proxy(self): 
        pass 
        


def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = Tool(maya_win.getMayaWindow())
    return myApp
