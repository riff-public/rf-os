# python modules
import sys
import os
root = '{}/core'.format(os.environ['RFSCRIPT'])
if not root in sys.path:
    sys.path.append(root)
import glob
from collections import defaultdict
from pprint import pprint
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# maya modules
import pymel.core as pm
import maya.OpenMaya as om
import maya.mel as mel
from maya.app.general import fileTexturePathResolver

# pipeline modules
import rf_config as config
ACES_CG = 'ACES - ACEScg'
NODE_ATTR_MAP = {'file': 'fileTextureName', 'RedshiftNormalMap': 'tex0'}
ACES_EXT = '.exr'

def list_file_textures(texture_nodes):
    # get all their textures
    file_textures = defaultdict(list) 
    for node in texture_nodes:
        nodeType = pm.nodeType(node)
        full_path = node.attr(NODE_ATTR_MAP[nodeType]).get().replace('\\', '/')
        pattern_paths = fileTexturePathResolver.findAllFilesForPattern(full_path, None)
        if pattern_paths:  # it has UDIM pattern
            file_textures[node] = {'path': full_path, 'files': pattern_paths}
        else:  # means it's not UDIM or it doesn't exists
            if os.path.exists(full_path):
                file_textures[node] = {'path': full_path, 'files': [full_path]}
    return dict(file_textures)

def convert_shaders_to_aces():
    selected = pm.selected()
    texture_nodes = set()
    for sel in selected:
        nodes = [n for n in pm.listHistory(sel) if n.nodeType() in NODE_ATTR_MAP]
        texture_nodes.update(nodes)
    file_textures = list_file_textures(texture_nodes=texture_nodes)
    pprint(file_textures)
    if not file_textures:
        om.MGlobal.displayError('Cannot find any texture. Please select a shader.')
        return

    missing_cs_nodes = pm.colorManagementPrefs(q=True, missingColorSpaceNodes=True)
    for node, textures in file_textures.items():
        curr_path = textures['path']
        fn, ext = os.path.splitext(curr_path)
        # set extension to .exr only if all the .exr exist next to the original texture
        if ext != ACES_EXT:
            for tex in textures['files']:
                tex_path, tex_ext = os.path.splitext(tex)
                tex_ext_path = '{}{}'.format(tex_path, ACES_EXT)
                if not os.path.exists(tex_ext_path):
                    break
            else:
                nodeType = pm.nodeType(node)
                ext_path = '{}{}'.format(fn, ACES_EXT)
                node.attr(NODE_ATTR_MAP[nodeType]).set(ext_path)

        # change color space if node is missing a valid colorspace (coming from sRGB, file node only)
        if node in missing_cs_nodes:
            node.colorSpace.set(ACES_CG)

    om.MGlobal.displayInfo('Convert success.')


# if __name__ == '__main__':
#     convert_shaders_to_aces()