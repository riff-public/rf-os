# v.0.0.1 polytag switcher
_title = 'Shot Camera Manager'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'CameraManagerUI'

#Import python modules
import sys
import os
import getpass
import subprocess
from collections import OrderedDict
from functools import partial

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# framework modules
import rf_config as config
reload(config)
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.context import context_info
from rf_app.asm import asm_lib

import maya.cmds as mc 


user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

class Color:
    red = [255, 100, 100]
    grey = [100, 100, 100]
    green = [100, 255, 100]
    lightGrey = [200, 200, 200]

class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(Ui, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.headerLayout = QtWidgets.QHBoxLayout()
        self.assetLabel = QtWidgets.QLabel('')
        self.getButton = QtWidgets.QPushButton('Load Set')
        self.camListWidget = QtWidgets.QListWidget()
        self.referenceButton = QtWidgets.QPushButton('Reference')
        self.removeButton = QtWidgets.QPushButton('Remove')
        self.activeViewCheckBox = QtWidgets.QCheckBox('View Camera')

        self.allLayout.addLayout(self.headerLayout)
        self.headerLayout.addWidget(self.assetLabel)
        self.headerLayout.addWidget(self.getButton)
        self.allLayout.addWidget(self.camListWidget)
        self.allLayout.addWidget(self.activeViewCheckBox)
        self.allLayout.addWidget(self.referenceButton)
        self.allLayout.addWidget(self.removeButton)
        self.setLayout(self.allLayout)

        # set size 
        self.referenceButton.setMinimumSize(0, 30)
        self.removeButton.setMinimumSize(0, 30)

        self.headerLayout.setStretch(0, 2)
        self.headerLayout.setStretch(1, 1)

        self.camListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)


class CameraManager(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(CameraManager, self).__init__(parent)

        # ui read
        self.ui = Ui()
        self.setCentralWidget(self.ui)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setObjectName(uiName)
        self.resize(300, 500)

        self.caches = OrderedDict()
        self.activeColor = [160, 220, 160]
        self.inactiveColor = [100, 100, 100]
        self.init_functions()
        self.init_signals()

    def init_functions(self): 
        self.setup_context()
        self.fetch_camera_list()

    def init_signals(self): 
        self.ui.getButton.clicked.connect(self.get_selected_set) 
        self.ui.referenceButton.clicked.connect(self.create_reference)
        self.ui.removeButton.clicked.connect(self.remove_reference)

        self.ui.camListWidget.itemSelectionChanged.connect(self.cam_list_selected)
        self.ui.activeViewCheckBox.stateChanged.connect(self.active_checked)

    def setup_context(self): 
        self.asset = context_info.ContextPathInfo()
        self.set_asset_label(self.asset)

    def set_asset_label(self, asset): 
        self.ui.assetLabel.setText(asset.name)

    def get_selected_set(self): 
        sels = mc.ls(sl=True)
        if sels: 
            asm = asm_lib.MayaRepresentation(sels[0])
            asmPath = asm.asm_data
            
            if os.path.exists(asmPath): 
                self.asset = context_info.ContextPathInfo(path=asmPath)
                self.set_asset_label(self.asset)

    def fetch_camera_list(self): 
        self.thread = GetAssetShots(self.asset)
        self.thread.value.connect(self.set_camera_list)
        self.thread.start()

    def create_reference(self): 
        items = self.ui.camListWidget.selectedItems()

        for item in items: 
            data =  item.data(QtCore.Qt.UserRole)
            path = data['path']
            exists = self.check_cam_state(path)
            namespace = ('_').join(str(item.text()).split('_')[2:])

            if not exists: 
                mc.file(path, r=True, ns=namespace)
                self.edit_item(item, state=True)

    def remove_reference(self): 
        items = self.ui.camListWidget.selectedItems()

        for item in items: 
            data =  item.data(QtCore.Qt.UserRole)
            path = data['path']
            refPaths = self.get_ref_path(path)

            for refPath in refPaths: 
                mc.file(refPath, rr=True)

            self.edit_item(item, state=False)

    def cam_list_selected(self): 
        if self.ui.activeViewCheckBox.isChecked(): 
            item = self.ui.camListWidget.currentItem()
            data = item.data(QtCore.Qt.UserRole) 
            path = data['path']
            refPaths = self.get_ref_path(path)

            if refPaths: 
                namespace = mc.referenceQuery(refPaths[0], ns=True)
                cams = mc.ls('%s:*' % namespace[1:], type='camera')
                if cams: 
                    mc.lookThru(cams[0])


    def active_checked(self, value): 
        state = True if value else False 
        if state: 
            self.ui.camListWidget.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        else: 
            self.ui.camListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        

    def set_camera_list(self, data): 
        if data: 
            assets = data['assets']
            self.ui.camListWidget.clear()

            for asset in assets: 
                shotEntities = asset.get('shots')

                for shot in shotEntities: 
                    shotKey = shot['name']
                    path = self.camera_cache_path(shotKey)
                    state = self.check_cam_state(path)
                    self.add_item(shotKey, {'entity': shot, 'path': path}, state)

    def check_cam_state(self, path): 
        refs = [a.split('{')[0] for a in mc.file(q=True, r=True)]
        return True if path in refs else False

    def get_ref_path(self, path):
        refs = mc.file(q=True, r=True)
        return [a for a in refs if path in a]



    def camera_cache_path(self, shotName): 
        context = context_info.Context()
        context.update(project=self.asset.project, entityType=context_info.ContextKey.scene, entity=shotName, step='layout')
        scene = context_info.ContextPathInfo(context=context)
        scene.update_scene_context()
        outputDir = scene.path.hero().abs_path()
        camName = scene.output_name(outputKey='camCacheHero', hero=True)
        cachePath = '%s/%s' % (outputDir, camName)
        return cachePath


    def add_item(self, display, data, state=True): 
        item = QtWidgets.QListWidgetItem(self.ui.camListWidget)
        self.edit_item(item, display, data, state)

    def edit_item(self, item, display=None, data=None, state=None): 
        colors = self.activeColor if state else self.inactiveColor
        item.setText(display) if display else None
        item.setData(QtCore.Qt.UserRole, data) if data else None
        item.setTextColor(QtGui.QColor(colors[0], colors[1], colors[2])) if not state == None else None


class GetAssetShots(QtCore.QThread):
    value = QtCore.Signal(dict)

    def __init__(self, asset):
        QtCore.QThread.__init__(self)
        self.asset = asset

    def run(self):
        from rf_utils.sg import sg_utils
        assets = sg_utils.sg.find('Asset', [['project.Project.name', 'is', self.asset.project], ['code', 'is', self.asset.name]], ['shots'])
        self.value.emit({'assets': assets})


def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = CameraManager(maya_win.getMayaWindow())
    myApp.show()
    return myApp