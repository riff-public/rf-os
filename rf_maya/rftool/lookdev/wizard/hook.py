import os 
import sys 
import logging 
import maya.cmds as mc 

from rf_utils.context import context_info
from rftool.render.redshift import rs_utils 
from rf_utils import file_utils
reload(rs_utils)

def apply_smooth(res='md'): 
	asset = context_info.ContextPathInfo()
	asset.context.update(step='model', res=res)
	heroDir = asset.path.output_hero().abs_path()
	fileName = asset.output_name(outputKey='smoothData', hero=True)
	smoothFile = '%s/%s' % (heroDir, fileName)
	plys = file_utils.ymlLoader(smoothFile)
	namespace = detech_namespace()
	plys = ['%s:%s' % (namespace, a) for a in plys]

	for ply in plys: 
		rs_utils.add_smooth_ply('%s_rsMeshParam' % asset.name, ply)

def detech_namespace(): 
	detect = 'Geo_Grp'
	geos = mc.ls('*:%s' % detect)

	if geos: 
		namespace = mc.referenceQuery(geos[0], ns=True)
		return namespace[1:]
