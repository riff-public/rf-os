# v.0.0.1 polytag switcher
_title = 'Lookdev Tool'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'LookdevToolUI'

#Import python modules
import sys
import os 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework modules 
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
import hook 
reload(hook)
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui


class LookdevTool(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(LookdevTool, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        self.setCentralWidget(self.ui)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.init_signals()

    def init_signals(self): 
        self.ui.smooth_pushButton.clicked.connect(self.apply_smooth)
        self.ui.matteId_pushButton.clicked.connect(self.matteId_tool)
        self.ui.pub_pushButton.clicked.connect(self.run_publish)

    def apply_smooth(self): 
        hook.apply_smooth() 

    def matteId_tool(self): 
        from rf_app.lookdev.matte_tool import app
        reload(app)
        app.show()


    def run_publish(self): 
        from rf_app.publish.asset import asset_app
        reload(asset_app)
        app = asset_app.show()


def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = LookdevTool(maya_win.getMayaWindow())
    myApp.show()
    return myApp
