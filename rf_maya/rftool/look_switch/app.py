# v.0.0.1 beta
# v.0.0.2 support GPU
_title = 'RF Look Switch'
_version = 'v.0.0.2'
_des = 'support GPU'
uiName = 'RFLookSwitch'

#Import python modules
import sys
import os
import logging
import getpass
import json
from collections import OrderedDict
import random

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

import maya.cmds as mc
import maya.mel as mm
import maya.OpenMayaUI as mui

# initial env setup
user = '%s-%s' % (os.environ.get('RFUSER'), getpass.getuser())

from rf_utils import log_utils

# set logger
logFile = log_utils.name(appName, user=user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.INFO)

from rftool.utils.ui import load
from rf_utils.context import context_info
from rf_utils import register_entity
from rf_maya.contextMenu.dag import shade_menu
from rf_app.asm import asm_lib
from rftool.utils import maya_utils
from rftool.shade import shade_utils

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui


# If inside Maya open Maya GUI
def getMayaWindow():
    ptr = mui.MQtUtil.mainWindow()
    return wrapInstance(long(ptr), QtWidgets.QWidget)

logger.info('Running RFSCRIPT from %s' % os.environ.get('RFSCRIPT'))
logger.info('\n\n==============================================')

uiFile = '%s/ui.ui' % moduleDir
form_class, base_class = load.load_ui_type(uiFile)


class RFLookSwitch(base_class):

    def __init__(self, parent=None):
        #Setup Window
        super(RFLookSwitch, self).__init__(parent)
        # ui read
        self.ui = form_class()
        self.ui.setupUi(self)
        self.setWindowTitle('%s %s - %s' % (_title, _version, _des))
        self.setObjectName(uiName)

        self.init_signals()
        self.refresh()

    def init_signals(self): 
        self.ui.refresh_button.clicked.connect(self.refresh)
        self.ui.assign_button.clicked.connect(self.assign_preview)
        self.ui.ns_listwidget.itemSelectionChanged.connect(self.select_namespace)
        self.ui.apply_button.clicked.connect(self.apply)

    def refresh(self): 
        # get selected geo 
        sels, asms = asm_lib.UiCmd().list_selection()
        if not sels: 
            self.set_error('No asset selected')
        else: 
            self.ui.apply_button.setEnabled(False)
            if mc.referenceQuery(sels[0], inr=True): 
                self.find_asset(geo=sels[0])
            else: 
                if asms: 
                    asm = asm_lib.MayaRepresentation(asms[0])
                    self.find_asset(asm=asm)
                else: 
                    self.set_error('Object is not a reference')

    def find_asset(self, geo=None, asm=None): 
        if geo: 
            asset_path = mc.referenceQuery(geo, f=True).split('{')[0]
        if asm: 
            asset_path = asm.gpu_path
        self.asset = context_info.ContextPathInfo(path=asset_path)
        self.asset.update_context_from_file()

        if geo: 
            item_type = 'geo'
            namespaces = list_namespace(geo)
        else: 
            item_type = 'gpu'
            asms = list_gpus(asm)
            namespaces = [a.name for a in asms]
        
        self.ui.ns_listwidget.clear()
        for namespace in namespaces: 
            display = namespace
            item = QtWidgets.QListWidgetItem(self.ui.ns_listwidget)
            if geo: 
                geo_grp = '{}:{}'.format(namespace, self.asset.projectInfo.asset.geo_grp())
            if asm: 
                geo_grp = namespace
            item.setData(QtCore.Qt.UserRole, (item_type, namespace, geo_grp, None))
            item.setText(display)

        # get looks
        looks = get_looks(geo=geo, asm=asm)
        self.ui.look_listwidget.clear()
        for look in looks: 
            item = QtWidgets.QListWidgetItem(self.ui.look_listwidget)
            item.setData(QtCore.Qt.UserRole, (look, None))
            item.setText(look)

        # set ui
        self.ui.asset_label.setStyleSheet('')
        self.ui.asset_label.setText('{} ({})'.format(self.asset.name, item_type))
        

    def assign_preview(self): 
        selected_ns = self.ui.ns_listwidget.selectedItems()
        selected_looks = self.ui.look_listwidget.selectedItems() 
        if not selected_ns: 
            selected_ns = [self.ui.ns_listwidget.item(a) for a in range(self.ui.ns_listwidget.count())]
        if not selected_looks: 
            selected_looks = [self.ui.look_listwidget.item(a) for a in range(self.ui.look_listwidget.count())]
        looks = [a.data(QtCore.Qt.UserRole)[0] for a in selected_looks]
        ns = [a.data(QtCore.Qt.UserRole)[0] for a in selected_ns]
        rand_list = list()

        num_target = len(ns)
        num_look = len(looks)

        # create random list 
        for item in selected_ns: 
            if len(rand_list) == 0: 
                rand_list = list(looks)
            item_type, namespace, geo_grp, look = item.data(QtCore.Qt.UserRole)
            pick_look = random.choice(rand_list)
            rand_list.pop(rand_list.index(pick_look))
            display = '{} - {}'.format(namespace, pick_look)
            item.setData(QtCore.Qt.UserRole, (item_type, namespace, geo_grp, pick_look))
            item.setText(display)

        self.ui.apply_button.setEnabled(True)

    def select_namespace(self): 
        items = self.ui.ns_listwidget.selectedItems()
        selections = list()
        for item in items: 
            item_type, namespace, geo_grp, look = item.data(QtCore.Qt.UserRole)
            selections.append(geo_grp)

        mc.select(selections)

    def apply(self): 
        fixshade = self.ui.fixshade_cb.isChecked()
        selected_ns = self.ui.ns_listwidget.selectedItems()
        for item in selected_ns: 
            item_type, namespace, geo_grp, look = item.data(QtCore.Qt.UserRole)
            if look: 
                if item_type == 'geo': 
                    if fixshade: 
                        fix_shade(geo_grp)
                    switch(geo_grp, look, mtrkey='mtrPreview')
                else: 
                    asm_lib.MayaRepresentation(geo_grp).set_look(look)


    def set_error(self, text): 
        self.ui.asset_label.setText(text)
        self.ui.asset_label.setStyleSheet('background-color: red')


def get_looks(geo=None, asm=None): 
    if geo: 
        path = mc.referenceQuery(geo, f=True).split('{')[0]
    if asm: 
        path = asm.gpu_path
    asset = context_info.ContextPathInfo(path=path)
    asset.update_context_from_file()
    reg = register_entity.Register(asset)
    looks = reg.get.looks(res=asset.res)
    look_dict = dict()

    for look in looks: 
        mtrkey = 'mtr'
        lookname = look.get('look')
        keys = [a for a in look.keys() if mtrkey in a]
        look_dict[lookname] = dict()

        for key in keys: 
            look_dict[lookname][key] = look.get(key).get('heroFile')

    return look_dict


def switch(geo, look, mtrkey='mtrPreview'): 
    look_dict = get_looks(geo)
    a, b, connectRef = shade_menu.list_mtr_files(geo)
    if look in look_dict.keys(): 
        if mtrkey in look_dict[look].keys(): 
            shadeFile = look_dict[look][mtrkey]
            geoNamespace = mc.referenceQuery(geo, ns=True)[1:]
            shade_menu.switch_shade(shadeFile, geoNamespace, connectRef)
        else: 
            print('Key {} {} not publish'.format(look, mtrkey))


def fix_shade(geo): 
    trs = maya_utils.find_ply(geo, True)
    if trs:
        for tr in trs:
            shapes = mc.listRelatives(tr, s=True, pa=True)
            if not shapes:
                continue
            sgs = mc.listConnections(shapes[0], type='shadingEngine')
            if sgs and 'initialShadingGroup' not in sgs:
                shade_utils.reconnect_selected_shader(tr)
                break
        else:
            shade_utils.reconnect_selected_shader(trs[0])


def list_namespace(geo): 
    asset_path = mc.referenceQuery(geo, f=True).split('{')[0]
    refs = mc.file(q=True, r=True)
    asset_refs = [a for a in refs if asset_path in a]
    ns = sorted([mc.referenceQuery(a, ns=True)[1:] for a in asset_refs])
    return ns


def list_gpus(asm): 
    asms = asm_lib.list_asms()
    assets = list()

    for loc in asms: 
        loc = asm_lib.MayaRepresentation(loc)
        if loc.gpu_path == asm.gpu_path: 
            assets.append(loc)
    # assets = [a for a in asms if a.gpu_path == asm.gpu_path]
    return assets


def show():
    deleteUI(uiName)
    app = RFLookSwitch(getMayaWindow())
    app.show()
    return app

def deleteUI(ui):
    if mc.window(ui, exists=True):
        mc.deleteUI(ui)
        deleteUI(ui)
