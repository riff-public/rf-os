#Import python modules
import sys
import os
import logging
import getpass
import json
from collections import OrderedDict
import random
import re

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

import maya.cmds as mc
import maya.mel as mm
import maya.OpenMayaUI as mui
import pymel.core as pm

# initial env setup
user = '%s-%s' % (os.environ.get('RFUSER'), getpass.getuser())

from rf_utils import log_utils

# set logger
logFile = log_utils.name(appName, user=user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.INFO)

from rftool.utils.ui import load
from rf_utils.context import context_info
from rf_utils import register_entity
from rf_maya.contextMenu.dag import shade_menu
from rf_app.asm import asm_lib

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.pipeline import user_pref

from rf_utils.widget import project_widget
from rf_utils.widget import sg_entity_widget
from rf_utils.sg import sg_utils

from rf_app.cycle_library import utils

from rf_utils.sg import sg_thread
reload(sg_thread)

from rf_app.asm import asm_maya
from rf_app.sg_manager import dcc_hook
reload(dcc_hook)
from rf_utils import file_utils


# # If inside Maya open Maya GUI
# def getMayaWindow():
#     ptr = mui.MQtUtil.mainWindow()
#     return wrapInstance(long(ptr), QtWidgets.QWidget)

# logger.info('Running RFSCRIPT from %s' % os.environ.get('RFSCRIPT'))
# logger.info('\n\n==============================================')

# uiFile = '%s/ui.ui' % moduleDir
# form_class, base_class = load.load_ui_type(uiFile)

def build(locators, buildType, materialCheck=True):
    newLocList = []
    for loc in locators:
        if pm.objectType(pm.listRelatives(loc,c=True)[0], isType='locator'):
            newLocList.append(loc)
    locators = newLocList
    for loc in locators:
        check = True
        if not pm.objExists(loc + '.assetName'):
            check = False
        if not pm.objExists(loc + '.action'):
            check = False
        if not pm.objExists(loc + '.costume'):
            check = False
        if not pm.objExists(loc + '.rsPath'):
            check = False
        if not pm.objExists(loc + '.cachePath'):
            check = False
        if not pm.objExists(loc + '.offset'):
            check = False
        if not pm.objExists(loc + '.hue'):
            check = False

        if not check:
            logger.warning('object do not have some attribute')
            continue

        assetName = pm.getAttr(loc + '.assetName')
        action = pm.getAttr(loc + '.action')
        costume = pm.getAttr(loc + '.costume')
        rsPath = pm.getAttr(loc + '.rsPath')
        cachePath = pm.getAttr(loc + '.cachePath')
        offset = pm.getAttr(loc + '.offset')
        hue = pm.getAttr(loc + '.hue')

        children = pm.listRelatives(loc, children=True, type="transform")
        if children:
            for child in children:
                if pm.referenceQuery(child, isNodeReferenced=True):
                    ns = child.split(':')[0]
                    meshs = pm.listRelatives(child, allDescendents=True, type='mesh')
                    shadingNodes = pm.listConnections(meshs[0], type='shadingEngine')
                    nsMtr = shadingNodes[0].split(':')[0]

                    nsPath = pm.referenceQuery("{}RN".format(ns), f=True)
                    mc.file(nsPath, removeReference=True)

                    nsMtrPath = pm.referenceQuery("{}RN".format(nsMtr), f=True)
                    mc.file(nsMtrPath, removeReference=True)

                else:
                    pm.delete(child)

        if buildType == 'rsProxy':
            objs = create_crowd_loop(assetName, rsPath, costume, action)
            if not pm.objExists(objs[0]+'.hue'):
                pm.addAttr(objs[0] , ln='hue', at = 'float' , dv=0 , min=0 , max=360)
            # connect hue from locator to redshiftColorCorrection
            pm.connectAttr(loc+'.hue' , objs[0]+'.hue' , f=1)

            # rsProxyNodes = pm.listConnections(objs[0], type = 'RedshiftProxyMesh')
            rsShape = pm.listRelatives(objs[0], c=True)
            rsProxyNodes = pm.listConnections(rsShape[0], type = 'RedshiftProxyMesh')
            if rsProxyNodes:
                rsProxyNode = rsProxyNodes[0]
                # pm.connectAttr(loc+'.offset' , rsProxyNode+'.frameOffset' , f=1)



                # add expression for loop Frame rsProxy

                # get end frame number
                fp = mc.getAttr('{}.fileName'.format(rsProxyNode))
                name = os.path.basename(fp)
                listFiles = os.listdir(os.path.dirname(fp))

                lookFpList = []
                key = name.split('####')[0]
                for f in listFiles:
                    if key in f and '.rs' in f:
                         lookFpList.append(f)
                frameNum = len(lookFpList)


                expressions = pm.listConnections('{}.frameExtension'.format(rsProxyNode) ,type='expression')
                if expressions:
                    convert_exp_to_anim(rsProxyNode)

                    # set Offset
                    kfNode = pm.listConnections(rsProxyNode , type = 'animCurve')[0]
                    offset = pm.getAttr(loc+'.offset')
                    tfNow = pm.keyframe(kfNode , q=True , tc=True)
                    # mc.keyframe(str(kfNode) , t=(), tc=-tfNow[0]+1 , r=True ,edit=True)
                    mc.keyframe(str(kfNode) , t=(), tc=offset , r=True ,edit=True)

                # Hide unused Geo///////////
                rsNode = pm.ls(rsProxyNode)[0]

                dataPath = os.path.dirname(cachePath) + '/{}_{}_look_geo_data.hero.yml'.format(assetName, action)

                if os.path.exists(dataPath):
                    data = file_utils.ymlLoader(dataPath)
                    geos = data[costume]

                    count = rsNode.includeExcludeMeshNames.getNumElements()

                    # obj = "baseAdultMaleCrowd_001:Body_GeoShape"
                    state = False

                    for i in range(count):
                        objName = rsNode.includeExcludeMeshNames[i].get()
                        # if objName == obj:
                        objNameSplitNS = objName.split(':')[-1]
                        if not objNameSplitNS in geos:
                            rsNode.includeExcludeMeshStates[i].set(state)
                            pm.setAttr("{}.materialMode".format(rsNode) ,2)
                            pm.setAttr("{}.nameMatchPrefix".format(rsNode) ,":", type='string')
                            pm.setAttr("{}.objectIdMode".format(rsNode) ,1)
                            pm.setAttr("{}.tessellationMode".format(rsNode) ,1)
                            pm.setAttr("{}.userDataMode".format(rsNode) ,1)
                            pm.setAttr("{}.traceSetMode".format(rsNode) ,1)
                            pm.setAttr("{}.visibilityMode".format(rsNode) ,1)

                asset = context_info.ContextPathInfo(path=cachePath)
                res = 'md'
                look = 'main'

                asset.context.update(res=res, look=look)
                asset.context.update(step='lookdev') # use lookdev mtr for lookdev arthur_ldvMainMtr

                mtrFile = asset.library(context_info.Lib.mtr)
                mtrRel = '%s/%s' % (asset.path.hero(), mtrFile)
                shadeNamespace = asset.mtr_namespace

                existsNs = mc.namespace(exists = shadeNamespace)
                if not existsNs:
                    ref = mc.file(mtrRel, r=True, f=True, ns=shadeNamespace)
                # End Hide unused Geo//////////

                    # offsetKeyframe(objs[0])

                    # expText = pm.expression(expressions[0] ,q=True, s=True)
                    # frameExt = '{}.frameExtension'.format(rsProxyNode)
                    # frameOff = '{}.frameOffset'.format(rsProxyNode)
                    # # expText = 'if(frame>({frameNum}+1000)&&)\n\t{frameExt}=(frame-1000)%{frameNum}+1000;\nelse\n\t{frameExt}=frame;'.format(frameNum=frameNum, frameExt=frameExt)
                    # expText = 'if(((frame-1000)%{frameNum}-{frameOff})==0)\n\t{frameExt}={frameNum}-{frameOff}+1000;\nelse\n\t{frameExt}=(frame-1000)%{frameNum}-{frameOff}+1000;'.format(frameNum=frameNum, frameExt=frameExt,frameOff=frameOff)
                    # print expText

                    # pm.expression(expressions[0] ,e=True, s=expText)

            objs = [objs[0] + '_Geo_Grp']

        elif buildType == 'cache':
            objs = create_crowd_cache(assetName, cachePath, materialCheck=materialCheck)
            meshs = pm.listRelatives(objs[0], allDescendents=True, type='mesh')

            abcNs = objs[0].split(':')[0]
            abcNode = pm.ls('{}:*'.format(abcNs), type='AlembicNode')
            if abcNode:
                for node in abcNode:
                    pm.setAttr('{}.cycleType'.format(node), 1) # set cycle type to loop

            dataPath = os.path.dirname(cachePath) + '/{}_{}_look_geo_data.hero.yml'.format(assetName, action)
            # print 'DATA'
            # print dataPath
            if os.path.exists(dataPath):
                data = file_utils.ymlLoader(dataPath)
                geos = data[costume]
                refGeos = pm.listRelatives(objs[0], allDescendents=True)
                showGeos = []
                for refGeo in refGeos:
                    refGeoSplitNS = refGeo.split(':')[-1]
                    if not refGeoSplitNS in geos:
                        pm.hide(refGeo)
                        # print 'HIDE'
                        # print refGeo
                    else:
                        showGeos.append(refGeo)
                for geo in showGeos:
                    pm.showHidden(geo, above=True)

            shadingNodes = pm.listConnections(meshs[0], type='shadingEngine')
            if shadingNodes:
                shadingNode = shadingNodes[0]
                shadingNs = shadingNode.split(':')[0]
                # rsColors = pm.listConnections(shadingNode, type='RedshiftColorCorrection')
                rsColors = pm.ls('{}:*'.format(shadingNs), type='RedshiftColorCorrection')
                if rsColors:
                    for rsColor in rsColors:

                        # rsColor = rsColors[0]
                        pm.connectAttr(loc+'.hue' , rsColor+'.hue' , f=1)

            abcNodes = pm.listConnections(meshs[0],type = 'AlembicNode')
            if abcNodes:
                abcNode = abcNodes[0]
                pm.connectAttr(loc+'.offset' , abcNode+'.offset' , f=1)
            # frameOffset

        # renderProxy = asm_maya.create_render_proxy(name , path)
        # if renderProxy:
        #     rsNode, shape, placeHolder = renderProxy
        #     renderProxyObject = MayaRepresentation(placeHolder)
        #     renderProxyObject.set_parent(self.name)
        #     renderProxyObject.reset_local_pos()
        #     asm_maya.override_instance_material(rsNode, entity)

        if pm.objExists(loc):
            currentParents = pm.listRelatives(objs, p=True, f=True)
            currentParent = currentParents[0] if currentParents else ''
            locName = loc.name()

            pm.delete(pm.parentConstraint(loc,objs,mo=False))

            if not locName in currentParent :
                objs = pm.parent(objs, loc)[0]
            # if resolveNameClash and self.nameClash:
            #     self.lock_node(False)
            #     self.name = self.pm.rename(self.name, self.preferName)

        # must create attr name "hue" in rsproxyGeo this will match name "hue" in userdata that link to rsproxy colorCorrection set in LDV
        # Hue will connect to "hue" in rsproxyGeo , then will drive userdata from rsproxy


def delete(locators):
    newLocList = []
    for loc in locators:
        if pm.objectType(pm.listRelatives(loc,c=True)[0], isType='locator'):
            newLocList.append(loc)
    locators = newLocList
    for loc in locators:
        check = True
        if not pm.objExists(loc + '.assetName'):
            check = False
        if not pm.objExists(loc + '.action'):
            check = False
        if not pm.objExists(loc + '.costume'):
            check = False
        if not pm.objExists(loc + '.rsPath'):
            check = False
        if not pm.objExists(loc + '.cachePath'):
            check = False
        if not pm.objExists(loc + '.offset'):
            check = False
        if not pm.objExists(loc + '.hue'):
            check = False

        if not check:
            logger.warning('object do not have some attribute')
            continue
        children = pm.listRelatives(loc, children=True, type="transform")
        if children:
            for child in children:
                if pm.referenceQuery(child, isNodeReferenced=True):
                    ns = child.split(':')[0]
                    nsMtr = None
                    meshs = pm.listRelatives(child, allDescendents=True, type='mesh')
                    shadingNodes = pm.listConnections(meshs[0], type='shadingEngine')
                    if shadingNodes:
                        nsMtr = shadingNodes[0].split(':')[0]

                    nsPath = pm.referenceQuery("{}RN".format(ns), f=True)
                    mc.file(nsPath, removeReference=True)

                    if nsMtr and nsMtr != 'initialShadingGroup':
                        nsMtrPath = pm.referenceQuery("{}RN".format(nsMtr), f=True)
                        mc.file(nsMtrPath, removeReference=True)

                else:
                    pm.delete(child)


def create_crowd_loop(name, path , look, process): 
    name = '%s_%s_%s_rsProxy' % (name, process, look)
    namespace = dcc_hook.get_available_namespace(name)
    obj = dcc_hook.import_crowd_loop_file(path, namespace)
    return [namespace[:-1]]

# def create_crowd_cache(name, path , look, process): 
#     name = '%s_%s_%s_rsProxy' % (name, process, look)
#     namespace = dcc_hook.get_available_namespace(name)
#     obj = dcc_hook.import_crowd_loop_file(path, namespace)
#     return obj

def create_crowd_cache(name, path, entity=None, materialCheck=True):
    # entity = self.entity if not entity else entity
    # look = self.ui.infoWidget.look()
    # look = look if look else 'main'


    if not entity:
        entity = context_info.ContextPathInfo(path=path)

    res = 'md'
    look = 'main'

    entity.context.update(res=res, look=look)

    fp = mc.file(q=True, loc=True)
    if fp:
        file_entity = context_info.ContextPathInfo(path=fp)
        step = file_entity.step
    else:
        step = 'crowd'

    if step == 'light':
        refs = dcc_hook.create_reference(entity, path, material=materialCheck, separateShader=True, materialType='render')
    else:
        refs = dcc_hook.create_reference(entity, path, material=materialCheck, separateShader=False, materialType='preview')
    return refs
    #     self.group_refs(entity, refs)
    #     subassets = subasset_utils.load_default_subasset(refs)
    #     for subasset in subassets:
    #         subassetGeo = ['{}:Rig_Grp'.format(subasset)]
    #         entity.context.update(entityGrp='prop')
    #         self.group_refs(entity, subassetGeo)
    # else: 
    #     logger.info('Create reference %s cancelled' % path)

def convert_exp_to_anim(rsProxyNd):
    condNd = pm.listConnections('{}.frameExtension'.format(rsProxyNd))
    frt , lst = check_lenght_seq(pm.getAttr('{}.fileName'.format(rsProxyNd)))
    
    if condNd:
        pm.delete(condNd)
        
    pm.setKeyframe(rsProxyNd, attribute = 'frameExtension', v = frt, t = [frt,frt])
    pm.setKeyframe(rsProxyNd, attribute = 'frameExtension', v = lst, t = [lst,lst])
    
    animNd = pm.listConnections('{}.frameExtension'.format(rsProxyNd))[0]
    pm.setAttr('{}.preInfinity'.format(animNd), 3)
    pm.setAttr('{}.postInfinity'.format(animNd), 3)
    
    pm.keyTangent(rsProxyNd, e = True, at = 'frameExtension', itt = 'linear', ott = 'linear')

def check_lenght_seq(files = ''):
    filesAry = files.split("/")
    dir_list = os.listdir('/'.join(filesAry[0:-1]))
    pattern = []
    
    for dl in dir_list:
        if re.findall(filesAry[-1].split('.hero.')[0] + '.hero.1', dl):
            pattern.append(dl)
            
    pattern = sorted(pattern)
    numberSeq = re.findall(r'.([0-9]{4})', pattern[0])[0]
    exit = False
    listSeq = []
    lastSeq = numberSeq
    
    while not exit:
        if os.path.exists('{}/{}.{}.rs'.format('/'.join(filesAry[0:-1]), '.'.join(filesAry[-1].split('.')[0:-2]), lastSeq)):
            listSeq.append(lastSeq)
            lastSeq = '%04d'%(int(lastSeq)+1)
        else :
            exit = True
    
    return int(listSeq[0]), int(listSeq[-1])