# v.0.0.1 beta
# v.0.0.2 support GPU
_title = 'RF Crowd Generate'
_version = 'v.0.0.1'
_des = 'support locator'
uiName = 'RFCrowdGenerate'

#Import python modules
import sys
import os
import logging
import getpass
import json
from collections import OrderedDict
import random

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

import maya.cmds as mc
import maya.mel as mm
import maya.OpenMayaUI as mui
import pymel.core as pm

# initial env setup
user = '%s-%s' % (os.environ.get('RFUSER'), getpass.getuser())

from rf_utils import log_utils

# set logger
logFile = log_utils.name(appName, user=user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.INFO)

from rftool.utils.ui import load
from rf_utils.context import context_info
from rf_utils import register_entity
from rf_maya.contextMenu.dag import shade_menu
from rf_app.asm import asm_lib

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.pipeline import user_pref

from rf_utils.widget import project_widget
from rf_utils.widget import sg_entity_widget
from rf_utils.sg import sg_utils

from rf_app.cycle_library import utils

from rf_utils.sg import sg_thread
reload(sg_thread)

from rf_maya.rftool.crowd_generate import build
reload(build)

from rf_utils import file_utils


# If inside Maya open Maya GUI
def getMayaWindow():
    ptr = mui.MQtUtil.mainWindow()
    return wrapInstance(long(ptr), QtWidgets.QWidget)

logger.info('Running RFSCRIPT from %s' % os.environ.get('RFSCRIPT'))
logger.info('\n\n==============================================')

uiFile = '%s/ui.ui' % moduleDir
form_class, base_class = load.load_ui_type(uiFile)


class RFCrowdGenerate(base_class):

    def __init__(self, parent=None):
        #Setup Window
        super(RFCrowdGenerate, self).__init__(parent)
        # ui read
        self.ui = form_class()
        self.ui.setupUi(self)
        self.setWindowTitle('%s %s - %s' % (_title, _version, _des))
        self.setObjectName(uiName)

        self.resize(1600, 570)


        self.add_project_widget()
        self.add_asset_widget()

        self.init_functions()
        self.init_signals()

        self.fetch_asset()

        # # override ui from designer 
        # self.overrideUi = override_ui.PipelineWidget(self.ui)
        # self.overrideUi.setupWidget()

        # self.refresh()

    def init_functions(self): 
        # prefs
        self.pref = user_pref.ToolSetting(uiName)

        # check scene context 
        self.restoreData = self.check_restore_data()

    def init_signals(self): 
        # self.ui.refresh_button.clicked.connect(self.refresh)
        # self.ui.assign_button.clicked.connect(self.assign_preview)
        # self.ui.ns_listwidget.itemSelectionChanged.connect(self.select_namespace)
        # self.ui.apply_button.clicked.connect(self.apply)
        self.ui.projectWidget.projectChanged.connect(self.project_changed)
        self.ui.searchAsset_lineEdit.textChanged.connect(self.searchAsset_changed)
        self.ui.asset_listwidget.selected.connect(self.asset_selected)
        # self.ui.animation_listwidget.itemSelectionChanged.connect(self.animation_selected)

        self.ui.addSrc_button.clicked.connect(self.add_src)
        self.ui.removeSrc_button.clicked.connect(self.remove_src)
        self.ui.addDst_button.clicked.connect(self.add_dst)
        self.ui.removeDst_button.clicked.connect(self.remove_dst)
        self.ui.dst_listwidget.itemSelectionChanged.connect(self.dst_selected)
        self.ui.generate_button.clicked.connect(self.generate_selected)
        self.ui.randomDst_button.clicked.connect(self.random_dst)
        self.ui.offset_button.clicked.connect(self.random_offsest)
        self.ui.color_button.clicked.connect(self.random_color)
        self.ui.buildCache_button.clicked.connect(self.build_cache_crowd)
        self.ui.buildProxy_button.clicked.connect(self.build_proxy_crowd)   
        self.ui.delete_button.clicked.connect(self.delete_crowd)      
        self.ui.load_costume_button.clicked.connect(self.show_task_costume) 

    def project_changed(self, projectEntity):
        """ project changed """
        self.fetch_asset()

    def searchAsset_changed(self): 
        self.populate_asset()

    def add_project_widget(self):
        self.ui.projectWidget = project_widget.SGProjectComboBox(sg=sg_utils.sg, layout=QtWidgets.QHBoxLayout())
        self.ui.projectWidget.allLayout.setSpacing(8)
        self.ui.horizontalLayout_5.insertWidget(0, self.ui.projectWidget)

    def add_asset_widget(self): 
        # asset layout 
        self.ui.asset_layout = QtWidgets.QVBoxLayout()
        self.ui.search_layout = QtWidgets.QHBoxLayout()

        # widgets 
        self.ui.searchAsset_label = QtWidgets.QLabel('Search : ')
        self.ui.searchAsset_lineEdit = QtWidgets.QLineEdit()
        self.ui.asset_listwidget = sg_entity_widget.EntityListWidget()

        self.ui.search_layout.addWidget(self.ui.searchAsset_label)
        self.ui.search_layout.addWidget(self.ui.searchAsset_lineEdit)
        self.ui.asset_layout.addLayout(self.ui.search_layout)
        self.ui.asset_layout.addWidget(self.ui.asset_listwidget)
        self.ui.gridLayout.addLayout(self.ui.asset_layout, 1, 0)
        self.ui.gridLayout


    def fetch_asset(self): 
        projectEntity = self.ui.projectWidget.current_item()
        assetType = 'Char'
        sgEntityType = 'Asset'
        extraFilters = [['sg_asset_type', 'is', assetType]]

        # thread 
        sg_thread.Config.memoryCache = False 
        self.sgThread = sg_thread.GetEntity(projectEntity, sgEntityType, extraFilters=extraFilters)
        self.sgThread.value.connect(self.fetch_finished)
        self.sgThread.start()
        self.set_loading()

    def fetch_finished(self, sgEntities): 
        """ shotgun send fetching data """ 
        sortedDict = sg_thread.sg_utils.ordered_entity_dict(sgEntities)
        self.sortedEntities = [v for k, v in sortedDict.iteritems()]
        self.populate_asset(self.sortedEntities)

        # unlock comboBox 
        # self.ui.assetType_comboBox.setEnabled(True)
        self.ui.searchAsset_lineEdit.setEnabled(True)

    def set_loading(self): 
        # loading page 
        self.ui.asset_listwidget.clear()
        self.ui.asset_listwidget.listWidget.addItem('Loading ...')

        # lock comboBox 
        # self.ui.assetType_comboBox.setEnabled(False)
        self.ui.searchAsset_lineEdit.setEnabled(False)

    def populate_asset(self, entities=None): 
        """ populate asset list """ 
        if not entities: 
            entities = self.sortedEntities

        # filter search 
        searchKey = str(self.ui.searchAsset_lineEdit.text())
        if searchKey: 
            entities = [a for a in entities if searchKey.lower() in a['code'].lower()]

        self.ui.asset_listwidget.clear()
        self.ui.asset_listwidget.add_items(entities)
        self.ui.asset_listwidget.set_text_mode()

        # restore pref 
        self.restore_asset_pref()

    def restore_project_pref(self): 
        """ restore project pref """ 
        self.ui.projectWidget.set_current_item(self.restoreData['project'], signal=False) if self.restoreData.get('project') else None

    def restore_asset_pref(self): 
        """ restore type pref """ 
        assetName = self.restoreData.get('assetName')
        self.ui.asset_listwidget.set_current(assetName, signal=False)

    def check_restore_data(self): 
        self.prefData = self.pref.read()
        print self.prefData, 'load'
        asset = self.scene_context()
        if not asset: 
            return self.prefData
        else: 
            prefDict = OrderedDict()
            prefDict['project'] = asset.project
            prefDict['assetType'] = asset.type
            prefDict['assetName'] = asset.name
            prefDict['taskName'] = asset.process
            return prefDict

    def scene_context(self): 
        try: 
            import maya.cmds as mc 
            if mc.file(q=True, sn=True): 
                return context_info.ContextPathInfo()

        except: 
            return 

    def asset_selected(self, sgEntity): 
        self.asset = self.setup_context()
        self.fetch_tasks_anim_loop(sgEntity)
        self.save_pref()

    def setup_context(self): 
        """ setup context based on ui selection """ 
        projectEntity = self.ui.projectWidget.current_item()
        projectName = projectEntity.get('name')
        # assetType = str(self.ui.assetType_comboBox.currentText())
        assetType = 'char'
        assetName = self.ui.asset_listwidget.current_text()
        taskEntity = self.animation_current_item()
        taskName = taskEntity.get('content') if taskEntity else ''
        step = 'anim'
        app = 'maya'

        if projectName and assetType and assetName: 
            context = context_info.Context()
            context.update(project=projectName, entityType='asset', step=step, res='md')
            context.update(entity=assetName, entityGrp=assetType, app=app)
            context.update(process=taskName)
            asset = context_info.ContextPathInfo(context=context)
            return asset
        else: 
            logger.debug('Context input %s %s %s %s' % (projectName, assetType, assetName, taskName))
            logger.warning('Cannot generate path from current context')

    def animation_current_item(self):
        # currentItems
        if self.ui.animation_listwidget.currentItem(): 
            return self.ui.animation_listwidget.currentItem().data(QtCore.Qt.UserRole)

    def animation_selected_item(self):
        # selectedItems
        # .data(QtCore.Qt.UserRole)
        if self.ui.animation_listwidget.selectedItems(): 
            return self.ui.animation_listwidget.selectedItems()

    def save_pref(self):
        """ save selection prefs """ 
        project = self.ui.projectWidget.current_project()
        # assetType = str(self.ui.assetType_comboBox.currentText())
        assetType = 'char'
        assetName = self.ui.asset_listwidget.current_text()
        taskItem = self.animation_current_item()
        taskName = ''
        if taskItem: 
            taskName = taskItem.get('content')

        prefDict = OrderedDict()
        prefDict['project'] = project
        prefDict['assetType'] = assetType or str()
        prefDict['assetName'] = assetName or str()
        if taskName: 
            logger.debug('write task %s' % taskName)
            prefDict['taskName'] = taskName 

        self.pref.write(prefDict)
        print prefDict
        logger.debug('Saved preference')
        


    def fetch_tasks_anim_loop(self, sgEntity=None, refresh=False): 
        """ fetching task (loop) from shotgun """ 
        # refresh mean do not use cache 
        projectEntity = self.ui.projectWidget.current_item()
        if not sgEntity: 
            sgEntity = self.ui.asset_listwidget.current_item()

        step = 'anim'

        # thread fetching tasks 
        self.sgThreadTask = sg_thread.GetTasks(projectEntity, sgEntity, step=step, refresh=refresh)
        self.sgThreadTask.value.connect(self.fetch_task_finished)
        self.sgThreadTask.start()
        self.set_task_loading()

    def set_task_loading(self): 
        self.ui.animation_listwidget.clear()
        self.ui.animation_listwidget.addItem('Loading ...')

    def fetch_task_finished(self, taskEntities): 
        # print 'taskEntities', taskEntities
        # asset entity now contain only asset 
        # self.taskEntities = taskEntities
        self.taskEntities = utils.combine_additional_data(self.asset, taskEntities)
        self.populate_tasks(self.taskEntities)

    def populate_tasks(self, taskEntities=None): 
        if not taskEntities: 
            taskEntities = self.taskEntities
        self.ui.animation_listwidget.clear() 
        
        if taskEntities: 
            self.animation_add_items(taskEntities, widget=True)

            # restore selection 
            taskName = self.restoreData.get('taskName')
            self.animation_set_current_item(taskName)
            taskEntity = self.animation_current_item()
            # self.animation_selected(taskEntity)

        else: 
            self.ui.animation_listwidget.addItem('No Item')

    def animation_add_items(self, sgEntities, widget=False):
        for i, entity in enumerate(sgEntities):
            item = self.animation_add_item(entity)
            # self.ui.animation_listwidget.setItemWidget(item, entity)

    def animation_set_current_item(self, taskName): 
        taskNames = [a.get('content') for a in self.animation_all_items()]
        if taskName in taskNames: 
            index = taskNames.index(taskName)
            self.ui.animation_listwidget.setCurrentRow(index)
        else:  # EDITED BY NU: if taskName not in taskNames, just select the first row
            self.ui.animation_listwidget.setCurrentRow(0)

    def animation_add_item(self, sgEntity, iconPath='', showIcon=True, widget=False):
        # normal widget mode
        item = QtWidgets.QListWidgetItem(self.ui.animation_listwidget)
        # print sgEntity
        item.setText(sgEntity.get('content'))
        item.setData(QtCore.Qt.UserRole, sgEntity)

        # iconPath = self.iconNoPreview if not iconPath else iconPath
        # sgEntity.update({'iconPath': iconPath})
        # entityType = sgEntity.get('type')

        # if not widget:
        #     item.setText(sgEntity[self.readAttr])

        #     if showIcon:
        #         iconWidget = QtGui.QIcon()
        #         iconWidget.addPixmap(QtGui.QPixmap(iconPath),QtGui.QIcon.Normal,QtGui.QIcon.Off)
        #         item.setIcon(iconWidget)

        # else:
        # itemWidget = QtWidgets.QWidget()

        # item.setSizeHint(itemWidget.sizeHint())
        # self.ui.animation_listwidget.setItemWidget(item, itemWidget)

        return item

    def animation_all_items(self): 
        count = self.ui.animation_listwidget.count()
        return [self.ui.animation_listwidget.item(a).data(QtCore.Qt.UserRole) for a in range(count)]

    def animation_selected(self): 
        taskEntities = self.animation_selected_item()
        allAnimLoopFiles = []
        if taskEntities:
            for taskEntity in taskEntities:
                taskEntity = taskEntity.data(QtCore.Qt.UserRole)
                process = taskEntity.get('content') if taskEntity else ''
                # process = self.ui.animation_listwidget.current_text()
                allAnimLoopFiles += (self.get_anim_loop(process))
                # self.asset = self.setup_context()
                # self.fetch_tasks_anim_loop(sgEntity)
                # self.save_pref()
        # self.ui.costume_listwidget.clear()
        self.allAnimLoopFiles = allAnimLoopFiles
        # self.show_task_costume(allAnimLoopFiles)

    def get_anim_loop(self, process, absPath=False): 
        res = 'md'

        look = 'split'
        # print sgEntity
        # entity = sg_thread.update_entity(sgEntity)
        entity = self.asset

        reg = register_entity.Register(entity)

        actions = reg.get_processes('anim', res)
        if 'sgVersion' in actions:
            actions.remove('sgVersion')
        if None in actions:
            actions.remove(None)

        step = entity.step
        # prevRes = entity.res
        # process = entity.process
        entity.context.update(step='anim', process=process, res=res, look=look)

        heroPath = entity.path.hero().abs_path()
        heroRelPath = entity.path.hero()
        maLoopFile = entity.output_name(outputKey='mayarsproxy', hero=True)
        # abcLoopFile = entity.output_name(outputKey='cache', hero=True) # <assetName>_anim_Cheer_md.hero.abc
        # abcPath = '%s/%s' % (heroPath, abcLoopFile)

        costumeList = []
        animLoopFileList = []
        key = maLoopFile.split('split')[0]
        listFiles = os.listdir(heroPath)
        for f in listFiles:
            if key in f:
                animLoopFile = '%s/%s' % (heroPath, f)
                animLoopFileList.append(animLoopFile)
                costumeList.append(f)

        # from rf_app.sg_manager import dcc_hook
        # reload(dcc_hook)
        # namespace = 'name_look_process_rsProxy_'
        # obj = dcc_hook.import_crowd_loop_file(animLoopFile, namespace)

        # animLoopFile = '%s/%s' % (heroPath, maLoopFile)
        # animLoopRelFile = '%s/%s' % (heroRelPath, maLoopFile)

        # print animLoopFile
        # print animLoopRelFile

        # for loop in animLoopFileList:
        #     print 'Loop'
        #     print loop

        # if absPath: 
        return animLoopFileList

        # else: 
            # return animLoopRelFile

    def get_anim_loop_cache(self, process, absPath=False): 
        res = 'md'

        look = 'main'
        # print sgEntity
        # entity = sg_thread.update_entity(sgEntity)
        entity = self.asset

        reg = register_entity.Register(entity)

        actions = reg.get_processes('anim', res)
        if 'sgVersion' in actions:
            actions.remove('sgVersion')
        if None in actions:
            actions.remove(None)

        step = entity.step
        # prevRes = entity.res
        # process = entity.process
        entity.context.update(step='anim', process=process, res=res, look=look)
        # entity.context.update(step='anim', process=process, res=res)

        heroPath = entity.path.hero().abs_path()
        heroRelPath = entity.path.hero()
        # maLoopFile = entity.output_name(outputKey='mayarsproxy', hero=True)
        abcLoopFile = entity.output_name(outputKey='cache', hero=True) # <assetName>_anim_Cheer_md.hero.abc
        abcPath = '%s/%s' % (heroPath, abcLoopFile)

        # from rf_app.sg_manager import dcc_hook
        # reload(dcc_hook)


        # refs = dcc_hook.create_reference(entity, abcPath, material=True, separateShader=True)
        # print 'CSS'
        # print refs

        # costumeList = []
        # animCaceFileList = []
        # key = maLoopFile.split('split')[0]
        # listFiles = os.listdir(heroPath)
        # for f in listFiles:
        #     if key in f:
        #         animCaceFile = '%s/%s' % (heroPath, f)
        #         animCaceFileList.append(animCaceFile)
        #         costumeList.append(f)

        return abcPath


    def show_task_costume(self):
        # thread fetching tasks 
        projectEntity = self.ui.projectWidget.current_item()
        sgEntity = self.ui.asset_listwidget.current_item()
        step = 'anim'
        refresh = False

        self.sgThreadTaskCostume = sg_thread.GetTasks(projectEntity, sgEntity, step=step, refresh=refresh)
        self.sgThreadTaskCostume.value.connect(self.fetch_task_costume_finished)
        self.sgThreadTaskCostume.start()
        self.set_task_costume_loading()

    def fetch_costume(self):
        # dataPath = os.path.dirname(cachePath) + '/{}_{}_look_geo_data.hero.yml'.format(assetName, action)
        self.ui.costume_listwidget.clear()

        taskEntities = self.animation_selected_item()
        allAnimLoopFiles = []
        if taskEntities:
            for taskEntity in taskEntities:
                taskEntity = taskEntity.data(QtCore.Qt.UserRole)
                process = taskEntity.get('content') if taskEntity else ''
                # process = self.ui.animation_listwidget.current_text()
                allAnimLoopFiles += (self.get_anim_loop(process))
                # self.asset = self.setup_context()
                # self.fetch_tasks_anim_loop(sgEntity)
                # self.save_pref()
        # self.ui.costume_listwidget.clear()
        self.allAnimLoopFiles = allAnimLoopFiles

        files = self.allAnimLoopFiles
        # costumeList = []
        for f in files:

            fn = f.split('/')[-1]
            costume_look = fn.split('_')[3]
            action = fn.split('_')[2]
            assetName = fn.split('_')[0]

            dataPath = os.path.dirname(f) + '/{}_{}_look_geo_data.hero.yml'.format(assetName, action)
            if os.path.exists(dataPath):
                data = file_utils.ymlLoader(dataPath)
                for costume, geos in data.items():
                    costume_look = costume

                    costume_dict = {}

                    item = QtWidgets.QListWidgetItem(self.ui.costume_listwidget)
                    # print sgEntity
                    text = assetName + ' - ' + action + ' - ' + costume_look
                    item.setText(text)
                    costume_dict['costume'] = costume_look
                    costume_dict['action'] = action
                    costume_dict['assetName'] = assetName
                    costume_dict['rsPath'] = f
                    costume_dict['abcPath'] = self.get_anim_loop_cache(action)
                    item.setData(QtCore.Qt.UserRole, costume_dict)

                    # costumeList.append(costume_dict)
        # return costumeList

    def set_task_costume_loading(self): 
        self.ui.costume_listwidget.clear()
        self.ui.costume_listwidget.addItem('Loading ...')

    def fetch_task_costume_finished(self, costumeList): 
        # print 'taskEntities', taskEntities
        # asset entity now contain only asset 
        # self.taskEntities = taskEntities
        # self.taskEntities = utils.combine_additional_data(self.asset, taskEntities)
        self.fetch_costume()

        # self.populate_tasks_costume(costumeList)

    def populate_tasks_costume(self, costumeList): 
        self.ui.costume_listwidget.clear() 
        
        if costumeList: 
            self.costume_add_items(costumeList)

            # # restore selection 
            # taskName = self.restoreData.get('taskName')
            # self.animation_set_current_item(taskName)
            # taskEntity = self.animation_current_item()
            # # self.animation_selected(taskEntity)

        else: 
            self.ui.costume_listwidget.addItem('No Item')

    def costume_add_items(self, costumeList):
        for costume_dict in costumeList:
            # costume_look = costume

            item = QtWidgets.QListWidgetItem(self.ui.costume_listwidget)
            # print sgEntity
            # costume_dict['costume'] = costume_look
            # costume_dict['action'] = action
            # costume_dict['assetName'] = assetName
            # costume_dict['rsPath'] = f
            # costume_dict['abcPath'] = self.get_anim_loop_cache(action)
            text = costume_dict['assetName'] + ' - ' + costume_dict['action'] + ' - ' + costume_dict['costume']
            item.setText(text)
            item.setData(QtCore.Qt.UserRole, costume_dict)

            costumeList.append(costume_dict)

    def add_src(self):
        if self.ui.costume_listwidget.selectedItems(): 
            items = self.ui.costume_listwidget.selectedItems()

            for item in items:
                itemName = item.text()
                if not self.ui.src_listwidget.findItems(itemName, QtCore.Qt.MatchExactly):
                    src_dict = item.data(QtCore.Qt.UserRole)
                    srcItem = QtWidgets.QListWidgetItem(self.ui.src_listwidget)
                    # print sgEntity
                    srcItem.setText(itemName)
                    item.text()

                    srcItem.setData(QtCore.Qt.UserRole, src_dict)

    def remove_src(self):
        items = self.ui.src_listwidget.selectedItems()
        if items:
            for item in items:
                row = self.ui.src_listwidget.row(item)
                self.ui.src_listwidget.takeItem(row)

    def add_dst(self):
        sels = pm.ls(sl=True, l=True)
        for sel in sels:
            dst_dict = {}
            shape = sel.getShape()

            dst_allItems = self.destination_all_item()
            dst_name_list = []
            for item in dst_allItems:
                dst_name_dict = item.data(QtCore.Qt.UserRole)
                py = dst_name_dict['pyNode']
                name = py.longName()
                dst_name_list.append(name)

            if shape:
                if shape.nodeType() == 'locator':
                    # name = sel.nodeName()
                    name = sel.longName()

                    if not self.ui.dst_listwidget.findItems(name, QtCore.Qt.MatchExactly) and not name in dst_name_list:
                        dstItem = QtWidgets.QListWidgetItem(self.ui.dst_listwidget)
                        # print sgEntityType
                        dstItem.setText(name)
                        dst_dict['pyNode'] = sel
                        dstItem.setData(QtCore.Qt.UserRole, dst_dict)

    def remove_dst(self):
        items = self.ui.dst_listwidget.selectedItems()
        if items:
            for item in items:
                row = self.ui.dst_listwidget.row(item)
                self.ui.dst_listwidget.takeItem(row)

    def dst_selected(self):
        if self.ui.selectCheckBox.isChecked():
            pm.select(clear=True)
            if self.ui.dst_listwidget.selectedItems(): 
                items = self.ui.dst_listwidget.selectedItems()

                for item in items:
                    dst_dict = item.data(QtCore.Qt.UserRole)
                    obj = dst_dict['pyNode']
                    pm.select(obj , add=True)

    def source_all_item(self): 
        count = self.ui.src_listwidget.count()
        return [self.ui.src_listwidget.item(a) for a in range(count)]

    def destination_all_item(self): 
        count = self.ui.dst_listwidget.count()
        return [self.ui.dst_listwidget.item(a) for a in range(count)]

    def random_dst(self):
        selected_obj = self.ui.dst_listwidget.selectedItems()
        selected_looks = self.ui.src_listwidget.selectedItems() 
        if not selected_obj: 
            selected_obj = self.destination_all_item()
        if not selected_looks: 
            selected_looks = self.source_all_item()

        sels = pm.ls(sl=True, l=True)

        rand_list = list()

        num_target = len(selected_obj)
        num_look = len(selected_looks)

        # create random list 
        for i,item in enumerate(selected_obj):
            if len(rand_list) == 0: 
                rand_list += selected_looks


            pick_look_index = random.randint(0,len(rand_list)-1)
            pick_look = rand_list[pick_look_index]
            # rand_list.pop(rand_list.index(pick_look_index))
            rand_list.pop(pick_look_index)
            display = '{} - {}'.format(sels[i].longName(), pick_look.text())

            # if self.ui.dst_listwidget.findItems(item.text(), QtCore.Qt.MatchExactly):
            #     row = self.ui.dst_listwidget.row(item)
            #     self.ui.dst_listwidget.takeItem(row)

            # item_obj = QtWidgets.QListWidgetItem(self.ui.dst_listwidget)
            # dst_dict = item.data(QtCore.Qt.UserRole)
            src_dict = pick_look.data(QtCore.Qt.UserRole)
            # dst_dict['variant'] = src_dict
            # item_obj.setText(display)
            # item_obj.setData(QtCore.Qt.UserRole , dst_dict)

            item.setText(display)
            dst_dict = item.data(QtCore.Qt.UserRole)
            dst_dict['variant'] = src_dict
            item.setData(QtCore.Qt.UserRole , dst_dict)



    def generate_selected(self):
        items = self.ui.dst_listwidget.selectedItems()
        for item in items:
            obj , assetName , action , look = (item.text()).split(' - ')
            # print obj
            name = '{}_{}_{}_001'.format(assetName , action , look)

            loc = obj

            # print 'CHECK'
            # print obj
            # print '{}_{}_{}'.format(assetName , action , look)

            if not '{}_{}_{}'.format(assetName , action , look) in obj:

                # if pm.objExists(name):
                    # types = pm.nodeType(newName)
                    # if types == 'reference' and not rn.endswith('sharedReferenceNode') :
                    # fm = pm.referenceQuery(rn ,filename=True)
                    # oldNs = pm.file( fm, q=True, namespace=True)
                    # ns = oldNs
                    # if ns in allNs:
                while pm.objExists(name):
                    version = ''
                    for num in name[::-1]:
                        if num.isdigit():
                            version = num + version
                        else:
                            break
                    if version:
                        newVersion = int(version) + 1
                        newName = name.replace(version, '%03d'%newVersion)
                    else:
                        newName = name + '_001'
                    name = newName


                loc = pm.rename(obj,name)

            else:
                name = obj.split('|')[-1]


            # print name
            locObj = pm.ls(loc,ln=True)

            attrList = ['assetName' , 'action' , 'costume' , 'rsPath', 'cachePath']
            for attr in attrList:
                if not pm.objExists(loc+'.'+attr):
                    pm.addAttr(loc , ln=attr, dt = 'string')

            if not pm.objExists(loc+'.hue'):
                pm.addAttr(loc , ln='hue', at = 'float' , dv=0 , min=0 , max=360)
            if not pm.objExists(loc+'.offset'):
                pm.addAttr(loc , ln='offset', at = 'double')

            pm.setAttr(loc+'.assetName' , assetName)
            pm.setAttr(loc+'.action' , action)
            pm.setAttr(loc+'.costume' , look)
            pm.setAttr(loc+'.rsPath' , item.data(QtCore.Qt.UserRole)['variant']['rsPath'])
            pm.setAttr(loc+'.cachePath' , item.data(QtCore.Qt.UserRole)['variant']['abcPath'])

            # if self.ui.dst_listwidget.findItems(item.text(), QtCore.Qt.MatchExactly):
            #     row = self.ui.dst_listwidget.row(item)
            #     self.ui.dst_listwidget.takeItem(row)

            display = '{} - {} - {} - {}'.format(name , assetName , action , look)
            # item_obj = QtWidgets.QListWidgetItem(self.ui.dst_listwidget)
            
            dst_dict = item.data(QtCore.Qt.UserRole)

            dst_dict['locName'] = name
            dst_dict['assetName'] = assetName
            dst_dict['action'] = action
            dst_dict['costume'] = look
            dst_dict['rsPath'] = item.data(QtCore.Qt.UserRole)['variant']['rsPath']
            dst_dict['cachePath'] = item.data(QtCore.Qt.UserRole)['variant']['abcPath']

            # dst_dict['variant'] = src_dict
            # item_obj.setText(display)
            # item_obj.setData(QtCore.Qt.UserRole , dst_dict)

            item.setText(display)
            item.setData(QtCore.Qt.UserRole , dst_dict)



    def random_offsest(self):
        offsetStartValue = int(self.ui.offsetStart_lineEdit.text())
        offsetEndValue = int(self.ui.offsetEnd_lineEdit.text())
        # lenValue = range(0,offsetValue+1)
        lenValue = range(offsetStartValue,offsetEndValue+1)
        sels = pm.ls(sl=True, l=True)
        for sel in sels:
            shape = sel.getShape()
            if shape:
                if shape.nodeType() == 'locator':
                    if mc.objExists(sel+'.offset'):
                        mc.setAttr(sel+'.offset',random.choice(lenValue))

    def random_color(self):
        minValue = int(self.ui.lineEdit.text())
        maxValue = int(self.ui.lineEdit_2.text())
        lenValueMax = range(0,maxValue+1)
        lenValueMin = range(360,360+minValue,-1)
        lenValue = lenValueMax+lenValueMin
        sels = pm.ls(sl=True, l=True)
        for sel in sels:
            shape = sel.getShape()
            if shape:
                if shape.nodeType() == 'locator':
                    if mc.objExists(sel+'.hue'):
                        mc.setAttr(sel+'.hue',random.choice(lenValue))

    def build_cache_crowd(self):
        buildType = 'cache'
        locs = pm.ls(sl=True)
        materialCheck = self.ui.materialCheckBox.checkState()
        build.build(locs, buildType, materialCheck)

    def build_proxy_crowd(self):
        buildType = 'rsProxy'
        locs = pm.ls(sl=True)
        build.build(locs, buildType)

    def delete_crowd(self):
        locs = pm.ls(sl=True)
        build.delete(locs)




    # def keyPressEvent(self, event):
    #     if event.key() == QtCore.Qt.Key_Delete:
    #         self.remove_queue()

    # def remove_queue(self):
    #     items = self.queueWidget.selectedItems()
    #     if items:
    #         for item in items:
    #             row = self.queueWidget.row(item)
    #             self.queueWidget.takeItem(row)




#     def init_signals(self): 
#         self.ui.refresh_button.clicked.connect(self.refresh)
#         self.ui.assign_button.clicked.connect(self.assign_preview)
#         self.ui.ns_listwidget.itemSelectionChanged.connect(self.select_namespace)
#         self.ui.apply_button.clicked.connect(self.apply)

#     def refresh(self): 
#         # get selected geo 
#         sels, asms = asm_lib.UiCmd().list_selection()
#         if not sels: 
#             self.set_error('No asset selected')
#         else: 
#             self.ui.apply_button.setEnabled(False)
#             if mc.referenceQuery(sels[0], inr=True): 
#                 self.find_asset(geo=sels[0])
#             else: 
#                 if asms: 
#                     asm = asm_lib.MayaRepresentation(asms[0])
#                     self.find_asset(asm=asm)
#                 else: 
#                     self.set_error('Object is not a reference')

#     def find_asset(self, geo=None, asm=None): 
#         if geo: 
#             asset_path = mc.referenceQuery(geo, f=True).split('{')[0]
#         if asm: 
#             asset_path = asm.gpu_path
#         self.asset = context_info.ContextPathInfo(path=asset_path)
#         self.asset.update_context_from_file()

#         if geo: 
#             item_type = 'geo'
#             namespaces = list_namespace(geo)
#         else: 
#             item_type = 'gpu'
#             asms = list_gpus(asm)
#             namespaces = [a.name for a in asms]
        
#         self.ui.ns_listwidget.clear()
#         for namespace in namespaces: 
#             display = namespace
#             item = QtWidgets.QListwidgetItem(self.ui.ns_listwidget)
#             if geo: 
#                 geo_grp = '{}:{}'.format(namespace, self.asset.projectInfo.asset.geo_grp())
#             if asm: 
#                 geo_grp = namespace
#             item.setData(QtCore.Qt.UserRole, (item_type, namespace, geo_grp, None))
#             item.setText(display)

#         # get looks
#         looks = get_looks(geo=geo, asm=asm)
#         self.ui.look_listwidget.clear()
#         for look in looks: 
#             item = QtWidgets.QListwidgetItem(self.ui.look_listwidget)
#             item.setData(QtCore.Qt.UserRole, (look, None))
#             item.setText(look)

#         # set ui
#         self.ui.asset_label.setStyleSheet('')
#         self.ui.asset_label.setText('{} ({})'.format(self.asset.name, item_type))
        

#     def assign_preview(self): 
#         selected_ns = self.ui.ns_listwidget.selectedItems()
#         selected_looks = self.ui.look_listwidget.selectedItems() 
#         if not selected_ns: 
#             selected_ns = [self.ui.ns_listwidget.item(a) for a in range(self.ui.ns_listwidget.count())]
#         if not selected_looks: 
#             selected_looks = [self.ui.look_listwidget.item(a) for a in range(self.ui.look_listwidget.count())]
#         looks = [a.data(QtCore.Qt.UserRole)[0] for a in selected_looks]
#         ns = [a.data(QtCore.Qt.UserRole)[0] for a in selected_ns]
#         rand_list = list()

#         num_target = len(ns)
#         num_look = len(looks)

#         # create random list 
#         for item in selected_ns: 
#             if len(rand_list) == 0: 
#                 rand_list = list(looks)
#             item_type, namespace, geo_grp, look = item.data(QtCore.Qt.UserRole)
#             pick_look = random.choice(rand_list)
#             rand_list.pop(rand_list.index(pick_look))
#             display = '{} - {}'.format(namespace, pick_look)
#             item.setData(QtCore.Qt.UserRole, (item_type, namespace, geo_grp, pick_look))
#             item.setText(display)

#         self.ui.apply_button.setEnabled(True)

#     def select_namespace(self): 
#         items = self.ui.ns_listwidget.selectedItems()
#         selections = list()
#         for item in items: 
#             item_type, namespace, geo_grp, look = item.data(QtCore.Qt.UserRole)
#             selections.append(geo_grp)

#         mc.select(selections)

#     def apply(self): 
#         selected_ns = self.ui.ns_listwidget.selectedItems()
#         for item in selected_ns: 
#             item_type, namespace, geo_grp, look = item.data(QtCore.Qt.UserRole)
#             if look: 
#                 if item_type == 'geo': 
#                     switch(geo_grp, look, mtrkey='mtrPreview')
#                 else: 
#                     asm_lib.MayaRepresentation(geo_grp).set_look(look)


#     def set_error(self, text): 
#         self.ui.asset_label.setText(text)
#         self.ui.asset_label.setStyleSheet('background-color: red')


# def get_looks(geo=None, asm=None): 
#     if geo: 
#         path = mc.referenceQuery(geo, f=True).split('{')[0]
#     if asm: 
#         path = asm.gpu_path
#     asset = context_info.ContextPathInfo(path=path)
#     asset.update_context_from_file()
#     reg = register_entity.Register(asset)
#     looks = reg.get.looks(res=asset.res)
#     look_dict = dict()

#     for look in looks: 
#         mtrkey = 'mtr'
#         lookname = look.get('look')
#         keys = [a for a in look.keys() if mtrkey in a]
#         look_dict[lookname] = dict()

#         for key in keys: 
#             look_dict[lookname][key] = look.get(key).get('heroFile')

#     return look_dict


# def switch(geo, look, mtrkey='mtrPreview'): 
#     look_dict = get_looks(geo)
#     a, b, connectRef = shade_menu.list_mtr_files(geo)
#     if look in look_dict.keys(): 
#         if mtrkey in look_dict[look].keys(): 
#             shadeFile = look_dict[look][mtrkey]
#             geoNamespace = mc.referenceQuery(geo, ns=True)[1:]
#             shade_menu.switch_shade(shadeFile, geoNamespace, connectRef)
#         else: 
#             print('Key {} {} not publish'.format(look, mtrkey))


# def list_namespace(geo): 
#     asset_path = mc.referenceQuery(geo, f=True).split('{')[0]
#     refs = mc.file(q=True, r=True)
#     asset_refs = [a for a in refs if asset_path in a]
#     ns = sorted([mc.referenceQuery(a, ns=True)[1:] for a in asset_refs])
#     return ns


# def list_gpus(asm): 
#     asms = asm_lib.list_asms()
#     print asm.gpu_path
#     assets = list()

#     for loc in asms: 
#         loc = asm_lib.MayaRepresentation(loc)
#         if loc.gpu_path == asm.gpu_path: 
#             assets.append(loc)
#     # assets = [a for a in asms if a.gpu_path == asm.gpu_path]
#     return assets


def show():
    deleteUI(uiName)
    app = RFCrowdGenerate(getMayaWindow())
    app.show()
    return app

def deleteUI(ui):
    if mc.window(ui, exists=True):
        mc.deleteUI(ui)
        deleteUI(ui)


# from rf_maya.rftool.crowd_generate import app
# reload(app)
# app.show()
