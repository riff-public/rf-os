import os
import pymel.core as pm
import glob
import re
from shutil import copy2


def atoi(text):
    return int(text) if text.isdigit() else text


def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split('(\d+)', text) ]


def order_file_name(filename, order_id):
    name, ext = os.path.splitext(filename)
    return "{name}_{order_id}{ext}".format(name=name, order_id=order_id, ext=ext)


def next_order_id(path_file):
    filename = os.path.basename(path_file)
    file_name, ext = os.path.splitext(filename)
    name, order_id = file_name.split('_')
    return int(order_id) + 1


def rename_file(backup_dir_path, file_name, new_file_name):
    dst_file = os.path.join(backup_dir_path, file_name)
    new_dst_file_name = os.path.join(backup_dir_path, new_file_name)
    os.rename(dst_file, new_dst_file_name)
    print("Backup file: {}".format(new_dst_file_name))


def process_save_backup():
    current_dir = pm.sceneName()
    file_name = os.path.basename(pm.sceneName())
    prefix_file_name, ext = os.path.splitext(file_name)
    backup_dir_path = "{current_dir}/_bk".format(current_dir=os.path.dirname(current_dir))
    if not os.path.exists(backup_dir_path):
        os.makedirs(backup_dir_path)
    print("Saving file to Path: {}".format(current_dir))
    copy2(current_dir, backup_dir_path)
    files = glob.glob(os.path.join(backup_dir_path, "{prefix_file_name}_*.ma".format(prefix_file_name=prefix_file_name)))
    files.sort(key=natural_keys) # sort by number
    if len(files) == 0:
        new_file_name = order_file_name(file_name, "001")
        rename_file(backup_dir_path, file_name, new_file_name)
        pm.saveFile()
        print("saved file")
        return
    else:
        order_id = next_order_id(files[-1])
        new_file_name = order_file_name(file_name, "00{order_id}".format(order_id=order_id))
        rename_file(backup_dir_path, file_name, new_file_name)
        pm.saveFile()
        print("saved file")
        return
