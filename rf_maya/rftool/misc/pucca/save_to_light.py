import os
import sys
import maya.cmds as mc
import shutil
from rf_utils import file_utils

def run(mode='light'):
    if mode == 'light':
        process = '06_lighting'
        name = 'light'
    if mode == 'mastershot':
        process = '08_mastershot'
        name = 'mastershot'

    currentScene = mc.file(q=True, sn=True)
    saveAnimFile, dst = light_dst(currentScene, process, name)

    mc.file(rename=saveAnimFile)
    mc.file(save=True, f=True)


    if not os.path.exists(os.path.dirname(dst)):
        os.makedirs(os.path.dirname(dst))

    shutil.copy2(saveAnimFile, dst)

    if os.path.exists(dst):
        mc.confirmDialog( title='Complete', message='Save to "%s" complete' % dst, button=['OK'])


def light_dst(animFile, process='06_lighting', name='light'):
    ep = animFile.split('/')[2].lower()
    shot = animFile.split('/')[5].lower()
    saveAnimFile = '%s/%s_ldv.ma' % (os.path.dirname(animFile), os.path.splitext(os.path.basename(animFile))[0])

    epName = '%s_%s' % (ep, shot.replace('s', ''))
    dst = get_name(ep, epName, 1, process, name)

    return saveAnimFile, dst


def get_name(ep, epName, version, process='06_lighting', name='light'):
    dst = 'Z:/NPC/post/%s/%s/%s/ver/%s_%s_v%02d.ma' % (process, ep, epName, epName, name, version)

    if not os.path.exists(dst):
        return dst
    else:
        return get_name(ep, epName, version+1, process, name)


# def find_ldv_version(inputPath, increment=0):
#   if os.path.exists(inputPath):
#       name = os.path.basename(inputPath)
#       name, ext = os.path.splitext(name)

#   return inputPath

def light_to_anim():
    src_path = mc.file(sn=True, q=True)
    if src_path:
        root, npc, post, step, ep, ep_shot, ver_fol, name = src_path.split('/')
        ep = ep.upper()
        shot_num = ep_shot.split('_')[1]
        shot_name = 's%s'%(ep_shot.split('_')[1])
        new_dir_path = 'P:/pucca/%s/scene/anim/%s/version'%(ep, shot_name)
        file_increment = os.listdir(new_dir_path)[-1]
        name_split = file_increment.split('_') ###[u's0050', u'anim', u'v004', u'ldv.ma'] 
        increment_ver = int(name_split[2][3]) +1 ##u'v004' (4)+1
        ver_ = 'v%03d'%increment_ver ## v005
        new_name = '%s_%s_%s_.ma'%(name_split[0], name_split[1], ver_)
        dst_path = os.path.join(new_dir_path, new_name).replace('\\', '/')
        shutil.copy2(src_path, dst_path)
        mc.confirmDialog( title='COPY COMPLETE', message='COPY COMPLETE %s'%dst_path, button='OK' )
    else:
        mc.confirmDialog( title='OPEN FILE', message='OPEN FILE', button='OK' )