import sys 
import os 
import maya.cmds as mc
from rf_maya.rftool.polytag import polytag_core


#
def main(path_pack): ###pack =G:/3D Asset/Model_Storage/Stock models and texture download/Buy/Megascans all/Forest Undergrowth Pack_MEGASCANS
	list_folder = os.listdir(path_pack)
	range_next_tran=0
	for folder in list_folder:
		if 'Rock' in folder:
			list_files = os.listdir(os.path.join(path_pack,folder))
			fbx_file = [x for x in list_files if x.endswith('fbx')]
			list_grp =[]
			range_tran = 0
			print folder, range_tran
			for index, fbx in enumerate(fbx_file):
				if not 'High' in fbx:
					path_folder = os.path.join(path_pack,folder).replace('\\', '/')
					path_file = os.path.join(path_folder,fbx).replace('\\', '/')
					print path_file
					if 'LOD1' in path_file or 'LOD5' in path_file:

						geo_list, mat_list = import_fbx(path_file, folder)
						polytag_core.set_root_tag(geo_list[0], 'SevenChickMovie', assetName=fbx, path=path_file)
						jpg_files = [x for x in list_files if x.endswith('jpg')]
						lod = geo_list[0].split('_')[-1]

						diffuse = [x for x in jpg_files if 'Albedo' in x][0]
						diffuse_path = os.path.join(path_folder,diffuse)

						bump = [x for x in jpg_files if lod in x][0]
						bump_path = os.path.join(path_folder,bump)

						spc = [x for x in jpg_files if 'Specular' in x][0]
						spc_path = os.path.join(path_folder,spc)

						connect_shader(mat_list[0], diffuse_path, bump_path, spc_path)

						grp_name = mc.group(n= fbx ,em=True)	
						mc.parent(geo_list, grp_name)
						mc.move(range_tran,0,0, grp_name)
						range_tran = range_tran +100
						list_grp.append(grp_name)

			mc.move(0,0,range_next_tran, list_grp)
			range_next_tran = range_next_tran+150

def run (path):
	pass

def import_fbx(file_import, assetName): 
		imp_list = mc.file(file_import, i= True, returnNewNodes=True )
		mat_list = mc.ls(imp_list, materials =True)
		geo_list = mc.ls(imp_list, type = 'transform')
		return geo_list, mat_list

def connect_shader(materials, diffuse_path, spc_path, bump_path): 
	connection = mc.listConnections(materials, s=True, d=False)
	if connection:
	    mc.delete(connection)
	color_file_node =mc.shadingNode('file',asTexture=True, icm=True)
	mc.setAttr('%s.fileTextureName'%color_file_node, diffuse_path, type='string')
	mc.connectAttr('%s.outColor'%color_file_node, '%s.color'%materials) 
	print 11111111
	spec_file_node =mc.shadingNode('file',asTexture=True, icm=True)
	mc.setAttr('%s.fileTextureName'%spec_file_node, spc_path, type='string')
	mc.connectAttr('%s.outColor'%spec_file_node, '%s.specularColor'%materials) 
	print 22222222
	bump_file_node = mc.shadingNode('file',asTexture=True, icm=True)
	bump2d_node = mc.shadingNode('bump2d',au=1,n='bump2d')
	mc.connectAttr('%s.o'%bump2d_node ,'%s.n'%materials)
	mc.setAttr('%s.bumpInterp'%bump2d_node, 1)
	mc.connectAttr('%s.outAlpha'%bump_file_node, '%s.bumpValue'%bump2d_node)
	mc.setAttr('%s.fileTextureName'%bump_file_node, bump_path, type='string')
	# bump_file_node = mc.shadingNode('file',asTexture=True, icm=True)
	# mc.setAttr('%s.fileTextureName'%bump_file_node

	# bump2d_node = mc.shadingNode('bump2d',au=1,n='bump2d')
	# mc.connectAttr('%s.o'%bump2d_node ,'%s.n'%materials)
	# mc.setAttr('%s.bumpInterp'%bump2d_node, 1)