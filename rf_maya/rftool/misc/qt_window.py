from PySide2 import QtGui, QtWidgets
from shiboken2 import wrapInstance 
import inspect
from functools import partial
import maya.OpenMayaUI as mui

from rftool.utils import repeat
reload(repeat)

def getMayaWindow():
    """ Maya Qt pointer """
    ptr = mui.MQtUtil.mainWindow()
    return wrapInstance(long(ptr), QtWidgets.QWidget)

class Cmd(): 
    def OK(self): 
        print 'OK'
        
    @repeat.repeatable
    def Hello(self): 
        print 'Hello'

    def Test(self): 
        print 'test'

    def test2(self): 
        print 'test2'
            
class Ui(QtWidgets.QWidget): 
    def __init__(self, parent=None) :
        super(Ui, self).__init__(parent=parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('test')
        self.layout.addWidget(self.label)
        self.setLayout(self.layout)
        
        
class Main(QtWidgets.QMainWindow): 
    def __init__(self, parent=None) :
        super(Main, self).__init__(parent=parent)
        self.ui = Ui(parent)
        self.setCentralWidget(self.ui)
        
        self.build_buttons()
        
    def build_buttons(self): 
        cmdIns = Cmd()
        methodObjs = inspect.getmembers(cmdIns, predicate=inspect.ismethod)
        
        for method in methodObjs: 
            print method
            button = QtWidgets.QPushButton(method[0])
            button.clicked.connect(partial(method[1]))
            self.ui.layout.addWidget(button)


def show(): 
    app = Main(parent=getMayaWindow())
    app.show()
