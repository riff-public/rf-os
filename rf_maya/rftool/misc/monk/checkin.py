import os 
import sys 
from rf_utils import file_utils
from rf_utils.sg import sg_process
sg = sg_process.sg 

def set_model_status(project, root): 
	""" set status to "out" """ 
	# root P:/SevenChickMovie/ftp/receive\theMonk\20190426_file_px\20190426\SevenChickMovie\asset\work\prop
	status = 'out'
	assetNames = file_utils.list_folder(root)
	projectEntity = sg.find_one('Project', [['name', 'is', project]], [])
	assetCount = len(assetNames)

	print 'Found %s assets' % assetCount

	for i, assetName in enumerate(assetNames): 
		print '%s/%s.' % (i+1, assetCount)
		filters = [['project', 'is', projectEntity], ['entity.Asset.code', 'is', assetName], ['content', 'is', 'model']]
		fields = ['content']
		taskEntity = sg.find_one('Task', filters, fields)

		if taskEntity: 
			# update data
			data = {'sg_status_list': status}
			result = sg.update('Task', taskEntity['id'], data)
			# print 'status complete'

		else: 
			print '# Missing %s' % assetName


def copy_workspace(root): 
	""" copy file from monk to workspace """
	assetNames = file_utils.list_folder(root)
	dstRoot = 'P:/SevenChickMovie/asset/work/prop'
	assetCount = len(assetNames)

	for i, assetName in enumerate(assetNames): 
		outsrcWs = '%s/%s/model/main/maya' % (root, assetName)
		files = file_utils.list_file(outsrcWs)

		for file in files: 
			src = '%s/%s' % (outsrcWs, file)
			dst = '%s/%s/model/main/maya/%s' % (dstRoot, assetName, file)
			file_utils.xcopy_file(src, dst)
			print '%s/%s Copy %s' % (i+1, assetCount, dst)
