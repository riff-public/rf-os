import os
import sys 
import maya.cmds as mc 
from rf_utils import file_utils
from rf_utils.context import context_info
from rf_utils import register_shot

def list_shots(project='SevenChickMovie', ep='act1', seq=''): 
	root = 'P:/%s/scene/publ/%s' % (project, ep)
	seqs = file_utils.list_folder(root)

	seqPath = ['%s/%s' % (root, a) for a in seqs if seq in a]
	return seqPath

def move_cache_global(path): 
	output = '%s/anim/output' % path
	if os.path.exists(output): 
		keys = file_utils.list_folder(output)
		scene = context_info.ContextPathInfo(path=path)
		reg = register_shot.Register(scene)

		for key in keys: 
			src = '%s/%s/hero' % (output, key)
			dst = '%s/output/%s' % (path, key)
			result = file_utils.xcopy_directory(src, dst)
			# re_reg(reg, scene, key, newHeroDir=dst)
			print dst

		reg.register()
	else: 
		print 'Skip %s' % path

def re_reg(reg, scene, key, newHeroDir=''): 
	output = register_shot.Output(reg.get.assetData.get(key))
	type = output.get_type()
	step = output.get_step()
	write = False 
	if step == 'anim': 

		if type in ['abc_cache', 'abc_tech_cache', 'yeti_cache']: 
			publishFile = output.get_cache().get('publishedFile')
			heroFile = output.get_cache().get('heroFile')
			heroName = os.path.basename(heroFile)
			newHero = '%s/%s' % (newHeroDir, heroName)
			output.add_cache(publishedFile=publishFile, heroFile=newHero)
			write = True

		if type in ['set']: 
			publishFile = output.get_setoverride().get('publishedFile')
			heroFile = output.get_setoverride().get('heroFile')
			heroName = os.path.basename(heroFile)
			newHero = '%s/%s' % (newHeroDir, heroName)
			output.add_setoverride(publishedFile=publishFile, heroFile=newHero)

			publishFile = output.get_setshot().get('publishedFile')
			heroFile = output.get_setshot().get('heroFile')
			heroName = os.path.basename(heroFile)
			newHero = '%s/%s' % (newHeroDir, heroName)
			output.add_setshot(publishedFile=publishFile, heroFile=newHero)
			write = True 

	if write: 
		print key, heroFile
		title = register_shot.Title()
		title.set_title(scene)
		title.set_output(register_shot.RegType.asset, key, step, output)
		reg.set(title)
		reg.write()
