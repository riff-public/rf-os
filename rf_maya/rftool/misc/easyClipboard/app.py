import maya.cmds as mc 
import maya.mel as mm
from functools import partial
import os
import sys
import json


def run() : 
    ui = TA_quickPasteBoard()
    ui.show()
    return ui

class TA_quickPasteBoard : 
    def __init__(self) : 
        self.win = 'TA_quickPasteBoard'
        self.ui = '%s_Win' % self.win
        if sys.platform == 'win32' :
            self.path = 'P:/Library/GeneralLibrary/clipboard/'

        if sys.platform == 'linux2' :
            self.path = '/playground/USERS/Ta/clipBoard/'

        self.w = 316
        self.h = 416
        self.offset = 124
        

    def show(self) : 
        if mc.window(self.ui, exists = True) : 
            mc.deleteUI(self.ui)
            
        mc.window(self.ui, title = 'Easy Clip Board v.2.0 - Support Shader', s = True, wh = [600, 500])
        mainForm = mc.formLayout()
        
        mc.columnLayout(adj = 1, rs = 4)
        mc.text(l = 'Clip Board Lists')
        mc.textScrollList('%s_mainTSL' % self.win, numberOfRows = 16)
        mc.textField('%s_pathTX' % self.win, ed = False)
        
        mc.checkBox('%s_deleteCB' % self.win, l = 'Delete After Paste')
        
        mc.frameLayout(borderStyle = 'etchedIn', lv = False)
        mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 100), (3, 100)])

        mc.radioCollection('%s_typeRC' % self.win)
        mc.radioButton('%s_geoRB' % self.win, label='Geometry', onc = partial(self.list, 'geo'), sl = True )
        mc.radioButton('%s_animRB' % self.win, label='Animation', onc = partial(self.list, 'anim'))
        mc.radioButton('%s_shdRB' % self.win, label='Shade', onc = partial(self.list, 'shade'))
        
        mc.setParent('..')
        mc.setParent('..')

        mc.frameLayout(borderStyle = 'etchedIn', lv = False)
        mc.rowColumnLayout(nc = 2, cw = [(1, 100), (2, 100)])

        mc.radioCollection('%s_modeRC' % self.win)
        mc.radioButton('%s_publicRB' % self.win, label='Public Path', cc = partial(self.updateUI), sl = True )
        mc.radioButton('%s_privateRB' % self.win, label='Local Path', cc = partial(self.updateUI) )

        mc.setParent('..')
        mc.setParent('..')

        mc.frameLayout(borderStyle = 'etchedIn', lv = False)
        mc.rowColumnLayout(nc = 2, cw = [(1, 150), (2, 150)], cs=[(1, 4), (2, 4)], rs=[(1, 4)])

        mc.button(l = 'Copy', h = 30, c = partial(self.exportClipBoard))
        mc.button(l = 'Paste', h = 30, c = partial(self.importClipboard))
        mc.button(l = 'Clear User', c = partial(self.clearUser, 'user'))
        mc.button(l = 'Clear All', c = partial(self.clearUser, 'all'))

        mc.setParent('..')
        mc.setParent('..')
                


        mc.frameLayout(borderStyle = 'etchedIn', lv = True, collapsable = True, l = 'Setting', cc = partial(self.adjustUI, 0), ec = partial(self.adjustUI, 1), cl = True)
        mc.columnLayout(adj = True, rs = 0)
        
        mc.text(l = 'User')
        mc.textField('%s_UserTX' % self.win)
        mc.text(l = 'Local Path')
        mc.textField('%s_localPathTX' % self.win)
        mc.text(l = 'Public Path')
        mc.textField('%s_publicPathTX' % self.win, tx = self.path)
        mc.button(l = 'Save Setting', c = partial(self.savePref))
        
        mc.setParent('..')
        mc.setParent('..')

        
        mc.showWindow()
        mc.window(self.ui, e = True, wh = [self.w, self.h])
        
        self.updateUI()


    #   loop thru project folder
        
    def saveUser(self, arg = None) : 
        user = mc.textField('%s_UserTX' % self.win, q = True, tx = True)
        mc.optionVar(sv=('PTuser', user) )      
        mm.eval('print "save user as %s ";' % user)
        
    
    def getUser(self, arg = None) : 
        # user = mc.optionVar(q = 'PTuser')
        user = os.environ.get('RFUSER')
        mc.textField('%s_UserTX' % self.win, e = True, tx = user)
        
        
    def savePath(self, arg = None) : 
        path = mc.textField('%s_localPathTX' % self.win, q = True, tx = True)
        path = mc.optionVar(sv=('PTCopyToolLocalPath', path) )  
        mm.eval('\nprint "save path %s ";' % path)
        
    def getPath(self, arg = None) : 
        path = mc.optionVar(q = 'PTCopyToolLocalPath')  
        if path == 0 : 
            mc.textField('%s_localPathTX' % self.win, e = True, tx = self.path)
        mc.textField('%s_localPathTX' % self.win, e = True, tx = path)
        
    def updateUI(self, arg = None) : 
        self.getUser()
        self.getPath()
        
        type = mc.radioCollection('%s_typeRC' % self.win, q = True, sl = True)
        if type == '%s_geoRB' % self.win : 
            list = 'geo'
            
        if type == '%s_animRB' % self.win : 
            list = 'anim'
        
        self.list(list)
        
    def savePref(self, arg = None) : 
        self.saveUser()
        self.savePath()
        
    def list(self, type = 'geo', arg = None) : 
        mode = mc.radioCollection('%s_modeRC' % self.win, q = True, sl = True)
        if mode == '%s_publicRB' %self.win : 
            path = mc.textField('%s_publicPathTX' % self.win, q = True, tx = True)
            
        if mode == '%s_privateRB' % self.win : 
            path = mc.textField('%s_localPathTX' % self.win, q = True, tx = True)
            
        mc.textField('%s_pathTX' % self.win, e = True, tx = path, ed = False)
            
        if type == 'geo' : 
            files = sorted(mc.getFileList(folder = path, filespec = '*.ma'))

        if type == 'anim' : 
            files = sorted(mc.getFileList(folder = path, filespec = '*.anim'))

        if type == 'shade' : 
            path = '%s/shade' % path
            if not os.path.exists(path): 
                os.makedirs(path)
            files = sorted(mc.getFileList(folder = path, filespec = '*.ma'))
            
        mc.textScrollList('%s_mainTSL' % self.win, e = True, ra = True)
        
        if files : 
        
            for each in files : 
                mc.textScrollList('%s_mainTSL' % self.win, e = True, append = each)
                
        else : 
            mc.textScrollList('%s_mainTSL' % self.win, e = True, append = 'Empty')
            
    def exportClipBoard(self, arg = None) : 
        objs = mc.ls(sl = True)

        type = mc.radioCollection('%s_typeRC' % self.win, q = True, sl = True)
        if type == '%s_geoRB' % self.win : 
            fileName = self.fileName('ma')
            mc.file(fileName, force = True, options = 'v=0', typ = 'mayaAscii', pr = True, es = True)
            self.list('geo')
            
        if type == '%s_animRB' % self.win : 
            fileName = self.fileName('anim')
            mm.eval('file -force -options "precision=8;intValue=17;nodeNames=1;verboseUnits=0;whichRange=1;range=0:10;options=keys;hierarchy=below;controlPoints=0;shapes=0;helpPictures=0;useChannelBox=0;copyKeyCmd=-animation objects -option keys -hierarchy below -controlPoints 0 -shape 0 " -typ "animExport" -pr -es "%s";' % fileName) 
            self.list('anim')

        if type == '%s_shdRB' % self.win : 
            fileName = self.fileName('ma')
            fileName = '%s/shade/%s' % (os.path.dirname(fileName), os.path.basename(fileName))
            self.exportShade(fileName)
            self.list('shade')
        
        mm.eval('print "success";')
        
        selFile = fileName.split('/')[-1]
        
        mc.textScrollList('%s_mainTSL' % self.win, e = True, si = selFile) 
        
        
    def fileName(self, type, arg = None) : 
        files = mc.textScrollList('%s_mainTSL' % self.win, q = True, ai = True)
        users = mc.textField('%s_UserTX' % self.win, q = True, tx = True)
        number = 0
        if not files[0] == 'Empty' : 
            for each in files : 
                crr = int(each.split('.')[0].split('_')[-1])
                if number < crr : 
                    number = crr
        
        number = number + 1         
        
        path = mc.textField('%s_pathTX' % self.win, q = True, tx = True)
        name = '%s_copy_%04d.%s' % (users, number, type)

        return '%s%s' % (path, name)
                
    def importClipboard(self, arg = None ) :
        path = mc.textField('%s_pathTX' % self.win, q = True, tx = True)
        sel = mc.textScrollList('%s_mainTSL' % self.win, q = True, si = True)
        selFile = '%s%s' % (path, sel[0])

        type = mc.radioCollection('%s_typeRC' % self.win, q = True, sl = True)
        if type == '%s_geoRB' % self.win : 
            mc.file(selFile,  i = True, type = 'mayaAscii', options = 'v=0', pr = True, loadReferenceDepth = 'all')
            self.list('geo')
            
        if type == '%s_animRB' % self.win : 
            mm.eval('file -import -type "animImport" -ra true -options "targetTime=4;copies=1;option=replace;pictures=0;connect=0;"  -pr -loadReferenceDepth "all" "%s";' % selFile)    
            self.list('anim')

        if type == '%s_shdRB' % self.win : 
            selFile = '%sshade/%s' % (path, sel[0])
            self.importShade(selFile)
            self.list('shade')
        
        if mc.checkBox('%s_deleteCB' % self.win, q = True, v = True) :
            mc.sysFile(selFile, delete = True)
             
    
    def clearUser(self, mode = 'user', arg = None) : 
        allClips = mc.textScrollList('%s_mainTSL' % self.win, q = True, ai = True)
        user = mc.textField('%s_UserTX' % self.win, q = True, tx = True)
        filePath = mc.textField('%s_pathTX' % self.win, q = True, tx = True)

        type = mc.radioCollection('%s_typeRC' % self.win, q = True, sl = True)
        if type == '%s_shdRB' % self.win : 
            filePath = '%sshade/' % filePath
            refresh = 'shade'
        
        if type == '%s_geoRB' % self.win : 
            refresh = 'geo'
        
        if type == '%s_animRB' % self.win : 
            refresh = 'anim'

        if not allClips == 'Empty' :
            for each in allClips : 
                if mode == 'user' : 
                    if user in each : 
                        delFile = '%s%s' % (filePath, each)
                        mc.sysFile(delFile, delete = True)
                        
                if mode == 'all' : 
                    delFile = '%s%s' % (filePath, each)
                    mc.sysFile(delFile, delete = True)
                    
        self.list(refresh)


    def adjustUI(self, type, arg = None) : 
        if type == 0 : 
            mc.window(self.ui, e = True, wh = [self.w, self.h])

        if type == 1 :
            mc.window(self.ui, e = True, wh = [self.w, self.h + self.offset])


    def exportShade(self, fileName, arg=None): 
        groupName = mc.ls(sl=True)
        if groupName: 
            exportGrp = groupName[0]
            dataFile = '%s.json' % os.path.splitext(fileName)[0]
            fileResult = exportShadeNode(fileName, exportGrp)
            dataResult = exportData(dataFile, exportGrp)

        mc.select(groupName)

    def importShade(self, selFile): 
        json = '%s.json' % (os.path.splitext(selFile)[0])
        targetObj = mc.ls(sl=True)
        namespace = None
        mtrNamespace = None
        

        if targetObj: 
            if ':' in targetObj[0]: 
                namespace = targetObj[0].split(':')[0]

            if os.path.exists(selFile) and os.path.exists(json): 
                data = jsonLoader(json)

                mtrs = [a for a in data.keys()]
                mtrExists = any(mc.objExists(a) for a in mtrs)

                if mtrExists: 
                    # user namespace 
                    mtrNamespace = os.path.basename(selFile).split('.')[0]
                    mtrsNamespace = ['%s:%s' % (mtrNamespace, a) for a in mtrs if mc.objExists('%s:%s' % (mtrNamespace, a))]

                    if mtrsNamespace: 
                        mc.delete(mtrsNamespace)

                    # check namespace exists. If not, create one 
                    if not mc.namespace(exists=mtrNamespace) : 
                        mc.namespace(add=mtrNamespace)

                    # set to asset namespace 
                    mc.namespace(set = mtrNamespace)
                    mc.file(selFile, i=True, type = 'mayaAscii', options = 'v=0', pr = True)
                    mc.namespace(set = ':')

                if not mtrExists: 
                    # import without namespace 
                    mc.file(selFile,  i=True, type='mayaAscii', options='v=0', pr=True, loadReferenceDepth='all')


                for mtr, value in data.iteritems(): 
                    objs = value['objs']

                    assignMtr = mtr
                    if mtrNamespace: 
                        assignMtr = '%s:%s' % (mtrNamespace, mtr)
                        print assignMtr

                    targetObjs = objs 
                    if namespace: 
                        targetObjs = ['%s:%s' % (namespace, a) for a in objs]

                    targetValid = [a for a in targetObjs if mc.objExists(a)]
                    targetMissing = [a for a in targetObjs if not mc.objExists(a)]

                    mc.select(targetValid)
                    mc.hyperShade(assign=assignMtr)
                    print 'assigned %s to %s' % (assignMtr, targetValid)

                    if targetMissing: 
                        print 'missing target %s' % targetMissing

        else: 
            mm.eval('warning "Select target object";')


def listShader(groupName = None) : 
    seNodes = mc.ls(type = 'shadingEngine')
    shaderInfo = dict()

    for eachNode in seNodes : 
        shaders = mc.listConnections('%s.surfaceShader' % eachNode, s = True)
        shapes = mc.listConnections('%s.dagSetMembers' % eachNode, s = True)

        if shaders and shapes : 
            if groupName : 
                objs = findObjGrp(groupName)

                if objs : 
                    shapes = [a for a in shapes if a in objs]

            if shapes : 
                strShapes = [str(a.encode('ascii', 'ignore')) for a in shapes]
                strShaders = [str(a.encode('ascii', 'ignore')) for a in shaders]
                # shaderInfo[str(shaders[0])] = {'shapes': strShapes, 'shadingEngine': str(eachNode), 'shaders': strShaders}
                shaderInfo[str(shaders[0])] = {'objs': strShapes, 'shadingEngine': str(eachNode), 'shaders': strShaders}

    return shaderInfo 


def findObjGrp(groupName) : 
    curSel = mc.ls(sl = True)
    mc.select(groupName, hi = True)
    objs = mc.ls(sl = True)

    return objs 


def exportShadeNode(exportPath, groupName = None) : 
    shaderInfo = listShader(groupName)

    if shaderInfo : 
        shadeNodes = [shadeNode for shadeNode in shaderInfo if not mc.referenceQuery(shadeNode, isNodeReferenced = True)]

        if not os.path.exists(os.path.dirname(exportPath)): 
            os.makedirs(os.path.dirname(exportPath))
        mc.select(shadeNodes)
        result = mc.file(exportPath, force = True, options = 'v=0', typ = 'mayaAscii', pr = True, es = True)
        print shadeNodes

        return result 


def exportData(logPath, groupName = None) : 
    shaderInfo = listShader(groupName)
    data = json.dumps(shaderInfo, sort_keys=True, indent=4)
    result = writeFile(logPath, data)

    return logPath 


def jsonDumper(file, dictData) : 
    data = json.dumps(dictData, sort_keys=True, indent=4)
    result = writeFile(file, data)

    return result


def jsonLoader(file) : 
    data = readFile(file)
    dictData = json.loads(data)

    return dictData


def writeFile(file, data) : 
    f = open(file, 'w')
    f.write(data)
    f.close()
    return True

def readFile(file) : 
    f = open(file, 'r')
    data = f.read()
    f.close()
    return data
