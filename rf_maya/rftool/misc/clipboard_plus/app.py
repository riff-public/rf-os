# v.0.0.1 polytag switcher
_title = 'Easy Clipboard ++'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'ClipboardPlusUI'

#Import python modules
import sys
import os
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# framework modules
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rf_utils.widget import file_widget
from rf_utils.widget import browse_widget
from rf_utils.pipeline import user_pref
from rf_utils import file_utils
import utils
reload(utils)

class Config:
    globalPath = 'P:/Library/GeneralLibrary/clipboard'
    localPath = '%s/clipboard' % os.environ['TEMP'].replace('\\', '/')
    user = os.environ['RFUSER'] or getpass.getuser
    globalMode = 'global'
    localMode = 'local'


class Tool(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(Tool, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.pref = user_pref.ToolSetting(uiName)
        self.geo = 'geo'
        self.anim = 'anim'
        self.mtr = 'mtr'
        self.asm = 'asm'
        self.ext = {self.geo: '.ma', self.anim: '.anim', self.mtr: '.ma', self.asm: '.yml'}
        self.subdir = [self.geo, self.anim, self.mtr, self.asm]
        self.activePath = Config.globalPath
        self.geoFormat = 'copy_<user>.v001'
        self.animFormat = 'copy_<user>.v001'
        self.mtrFormat = 'mtr_copy_<user>.v001'
        self.asmFormat = 'copy_<user>.v001'

        self.setup_widget()
        self.set_default()
        self.restore_selection()
        self.init_ui_data()
        self.init_signals()

    def setup_widget(self):
        self.browseWidget = browse_widget.BrowseFolder()
        self.browseWidget.lineEdit.setVisible(False)
        self.ui.config_layout.addWidget(self.browseWidget)

        self.fileWidget = file_widget.FileListWidget()
        self.ui.file_layout.addWidget(self.fileWidget)

    def set_default(self):
        self.ui.clear_pushButton.setEnabled(False)
        self.ui.showUser_checkBox.setChecked(True)

    def restore_selection(self):
        data = self.pref.read()
        mode = data.get('mode') or Config.globalMode
        user = data.get('user') or Config.user
        copyMode = data.get('copyMode') or self.geo

        self.set_mode(mode)
        self.ui.user_lineEdit.setText(user)
        self.set_copy_mode(copyMode)


    def set_mode(self, mode):
        if mode == Config.globalMode:
            self.ui.global_radioButton.setChecked(True)

        if mode == Config.localMode:
            self.ui.local_radioButton.setChecked(True)

    def get_ui_mode(self):
        if self.ui.global_radioButton.isChecked():
            return Config.globalMode
        if self.ui.local_radioButton.isChecked():
            return Config.localMode

    def set_copy_mode(self, copyMode):
        if copyMode == self.geo:
            self.ui.geo_radioButton.setChecked(True)
        if copyMode == self.anim:
            self.ui.anim_radioButton.setChecked(True)
        if copyMode == self.mtr:
            self.ui.material_radioButton.setChecked(True)
        if copyMode == self.asm:
            self.ui.asm_radioButton.setChecked(True)

    def get_copy_mode(self):
        if self.ui.geo_radioButton.isChecked():
            return self.geo
        if self.ui.anim_radioButton.isChecked():
            return self.anim
        if self.ui.material_radioButton.isChecked():
            return self.mtr
        if self.ui.asm_radioButton.isChecked():
            return self.asm

    def get_user(self):
        return str(self.ui.user_lineEdit.text())

    def init_ui_data(self):
        mode = self.get_ui_mode()
        if mode == Config.globalMode:
            self.activePath = Config.globalPath
            self.browseWidget.button.setEnabled(False)

        if mode == Config.localMode:
            self.activePath = self.pref.read().get('localPath') or Config.localPath
            self.browseWidget.button.setEnabled(True)

        self.ui.path_label.setText(self.activePath)
        self.list_files()
        self.set_copy_filename()

    def init_signals(self):
        # set path
        self.browseWidget.textChanged.connect(self.set_local_path)

        # mode
        self.ui.global_radioButton.clicked.connect(self.refresh_ui)
        self.ui.local_radioButton.clicked.connect(self.refresh_ui)

        # user
        self.ui.user_lineEdit.editingFinished.connect(self.refresh_ui)

        # copy mode
        self.ui.geo_radioButton.clicked.connect(self.refresh_ui)
        self.ui.anim_radioButton.clicked.connect(self.refresh_ui)
        self.ui.material_radioButton.clicked.connect(self.refresh_ui)
        self.ui.asm_radioButton.clicked.connect(self.refresh_ui)
        self.ui.showUser_checkBox.stateChanged.connect(self.user_checkbox_toggle)
        self.ui.refresh_pushButton.clicked.connect(self.refresh_ui)

        # button
        self.ui.copy_pushButton.clicked.connect(self.copy)
        self.ui.paste_pushButton.clicked.connect(self.paste)
        self.ui.clear_pushButton.clicked.connect(self.remove_files)

    def set_local_path(self, path):
        """ set local path """
        data = self.pref.read()
        data['localPath'] = path
        self.pref.write(data)
        self.init_ui_data()

    def set_copy_filename(self):
        copyMode = self.get_copy_mode()
        user = self.get_user()
        userPath = self.get_list_path(user=True)

        if copyMode == self.geo:
            nameFormat = self.geoFormat
        if copyMode == self.anim:
            nameFormat = self.animFormat
        if copyMode == self.mtr:
            nameFormat = self.mtrFormat
        if copyMode == self.asm:
            nameFormat = self.asmFormat


        # filename
        filename = '%s%s' % (nameFormat.replace('<user>', user), self.ext[copyMode])
        filename = utils.copy_name(filename, userPath, user=user)
        self.ui.preview_lineEdit.setText(filename)

    def save_selection(self):
        # get data
        data = self.pref.read()
        mode = self.get_ui_mode()
        user = self.get_user()
        copyMode = self.get_copy_mode()

        # save data
        data['mode'] = mode
        data['user'] = user
        data['copyMode'] = copyMode
        self.pref.write(data)

    def user_checkbox_toggle(self, value):
        state = True if value else False
        self.ui.clear_pushButton.setEnabled(state)
        self.refresh_ui()

    def refresh_ui(self):
        self.save_selection()
        self.init_ui_data()

    def list_files(self):
        """ list files in ui """
        copyMode = self.get_copy_mode()
        user = self.get_user()
        userCheckbox = self.ui.showUser_checkBox.isChecked()
        userFilter = user if userCheckbox else ''

        userPath = self.get_list_path(user=True)
        utils.makedirs(userPath)
        sharePath = self.get_list_path(user=False)

        files = utils.list_files(sharePath, userFilter=userFilter, ext=self.ext[copyMode])
        self.fileWidget.clear()
        self.fileWidget.add_files(files)

    def get_list_path(self, user=True):
        copyMode = self.get_copy_mode()
        userDir = self.get_user()
        path = '%s/%s/%s' % (self.activePath, copyMode, userDir) if user else '%s/%s' % (self.activePath, copyMode)
        return path

    def get_preview_file(self):
        return str(self.ui.preview_lineEdit.text())

    def copy(self):
        """ copy command """
        copyMode = self.get_copy_mode()
        dstPath = self.get_list_path(user=True)
        filename = self.get_preview_file()
        copyFile = '%s/%s' % (dstPath, filename)
        objSelected = utils.check_object_selected()

        if objSelected:
            if copyMode == self.geo:
                utils.export_geo(copyFile)
            if copyMode == self.anim:
                print 'export anim'
                utils.export_anim(copyFile)
            if copyMode == self.mtr:
                utils.export_shade(copyFile)
            if copyMode == self.asm:
                utils.export_asm(copyFile)
        else:
            QtWidgets.QMessageBox.information(self, 'Error', 'Please select objects to copy')

        self.refresh_ui()

    def paste(self):
        """ paste command """
        copyMode = self.get_copy_mode()
        filename = self.fileWidget.current_data()

        if filename:
            path = filename.replace('\\', '/')
            if copyMode == self.geo:
                utils.import_geo(path)
            if copyMode == self.anim:
                utils.import_anim(path)
            if copyMode == self.mtr:
                utils.import_shade(path)
            if copyMode == self.asm:
                utils.import_asm(path)
        else:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Please select file to paste')

    def remove_files(self):
        files = self.fileWidget.get_all_files()
        result = QtWidgets.QMessageBox.question(self, 'Confirm clear files', 'Clear all files?', QtWidgets.QMessageBox.Ok, QtWidgets.QMessageBox.Cancel)

        if result == QtWidgets.QMessageBox.Ok:
            utils.remove_files(files)
        self.refresh_ui()


def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = Tool(maya_win.getMayaWindow())
    myApp.show()
    return myApp
