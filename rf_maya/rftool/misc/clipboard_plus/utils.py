import os
import sys
from rf_utils import file_utils
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import pymel.core as pm
from rf_utils.pipeline import local_hook
reload(local_hook)
from rftool.utils import maya_utils
reload(maya_utils)

from rf_app.asm import asm_lib

def list_files(path, userFilter='', ext=''):
    targetFiles = []
    for root, dir, files in os.walk(path):
        if files:
            for filename in files:
                if os.path.splitext(filename)[-1] == ext:
                    targetFiles.append('%s/%s' % (root, filename))

    if userFilter:
        return [a for a in targetFiles if os.path.split(os.path.dirname(a))[-1] == userFilter]
    return targetFiles


def makedirs(path):
    if not os.path.exists(path):
        return os.makedirs(path)


def remove_files(files):
    for eachfile in files:
        os.remove(eachfile)
        logger.debug('Delete %s' % eachfile)

def copy_name(filename, path, user):
    version = find_version(filename)
    incrementVersion = increment_version(path)
    newname = filename.replace(version, incrementVersion)
    return newname


def find_version(filename):
    basename = os.path.basename(filename)
    for a in basename.split('_'):
        for b in a.split('.'):
            if b:
                if b[0] == 'v' and b[1:].isdigit():
                    return b

def max_version(path):
    files = [('/').join([path, a]) for a in os.listdir(path) if os.path.isfile(os.path.join(path, a))]
    if files:
        maxVersion = sorted([find_version(a) for a in files])[-1]
        return maxVersion

def increment_version(path):
    maxVersion = max_version(path)
    incrementVersion = 'v001'
    if maxVersion:
        nextVersion = version_int(maxVersion) + 1
        incrementVersion = version(nextVersion)
    return incrementVersion

def version(num):
    return 'v%03d' % num

def version_int(version):
    if version[1:].isdigit():
        return int(version[1:])

def export_geo(filename):
    from rf_maya.lib import maya_file
    objs = mc.ls(sl=True, type='transform')
    fileType = 'mayaAscii'

    # this command move objs to world first then reparent back
    move_to_world = maya_utils.MoveToWorld(objs)
    with move_to_world: 
        # # get temp path
        # localDst = local_hook.local_name(filename)
        # if not os.path.exists(os.path.dirname(localDst)): 
        #     os.makedirs(os.path.dirname(localDst))

        # new string path for objects after they were moved to world parent
        world_objs = [node.shortName() for node in move_to_world.objs]

        # get anim helper nodes
        anim_helper_nodes = maya_utils.get_dependency_anim_nodes(objects=world_objs)
        # select both pre-selections + anim helpers
        mc.select(world_objs + anim_helper_nodes)
        # export file
        localFile = mc.file(filename, force=True, options='v=0', typ=fileType, pr=True, es=True, con=True, chn=True, sh=False, ch=False, exp=False)
        
        # shutil.copy2(localFile, filename)

    # reselect old selection
    mc.select(objs, r=True)
    return filename

def import_geo(filename):
    allNs = mc.namespaceInfo(listOnlyNamespaces=True, recurse=True)
    rfns = mc.file(filename,  i=True, type='mayaAscii', options='v=0', pr=True, loadReferenceDepth='all',returnNewNodes=True ,namespace='temp_namespace')
    for rn in rfns:
        if mc.objExists(rn):
            types = mc.nodeType(rn)
            if types == 'reference' and not rn.endswith('sharedReferenceNode') :
                fm = mc.referenceQuery(rn ,filename=True)
                oldNs = mc.file( fm, q=True, namespace=True)
                ns = oldNs
                if ns in allNs:
                    while ns in allNs or mc.objExists(ns):
                        version = ''
                        for num in ns[::-1]:
                            if num.isdigit():
                                version = num + version
                            else:
                                break
                        if version:
                            newVersion = int(version) + 1
                            newNs = ns.replace(version, '%03d'%newVersion)
                        else:
                            newNs = ns + '_001'
                        ns = newNs
                    try:
                        mc.namespace(rename=['temp_namespace:%s'%(oldNs),ns])
                    except Exception as e:
                        logger.error(e)
                    allNs.append(ns)
    try:
        mc.namespace(removeNamespace='temp_namespace', mergeNamespaceWithRoot=True)
    except Exception as e:
        logger.error(e)

def export_shade(filename):
    from rftool.shade import shade_utils
    sels = mc.ls(sl=True)

    if len(sels) == 1:
        targetGrp = sels[0]
        objs = None
    if len(sels) > 1:
        targetGrp = None
        objs = sels
    return shade_utils.export_shade_with_data(filename, targetGrp=targetGrp, objs=objs)

def import_shade(filename):
    from rftool.shade import shade_utils
    reload(shade_utils)
    obj = mc.ls(sl=True)[0] if mc.ls(sl=True, type='transform') else ''
    if obj:
        objSn = obj.split('|')[-1]
        namespace = ':'.join(objSn.split(':')[:-1])
    else:
        namespace = ''

    return shade_utils.apply_import_shade(filename, targetNamespace=namespace)

def export_anim(filename):
    import maya.mel as mm
    check_plugin()
    mm.eval('file -force -options "precision=8;intValue=17;nodeNames=1;verboseUnits=0;whichRange=1;range=0:10;options=keys;hierarchy=below;controlPoints=0;shapes=0;helpPictures=0;useChannelBox=0;copyKeyCmd=-animation objects -option keys -hierarchy below -controlPoints 0 -shape 0 " -typ "animExport" -pr -es "%s";' % filename)

def import_anim(filename):
    import maya.mel as mm
    check_plugin()
    mm.eval('file -import -type "animImport" -ra true -options "targetTime=4;copies=1;option=replace;pictures=0;connect=0;"  -pr -loadReferenceDepth "all" "%s";' % filename)

def export_asm(filename):
    objs = mc.ls(sl=True)
    asm_lib.export(objs[0], filename)

def import_asm(filename):
    objs = mc.ls(sl=True)
    if objs:
        asm_lib.paste(objs[0], filename)
        asm_lib.build(objs[0])
    else:
        mc.group(n='pasted_asm', em=True)
        mc.select('pasted_asm')
        objs = mc.ls(sl=True)
        asm_lib.create(objs[0], filename)
        asm_lib.build(objs[0])

def check_plugin():
    """ load plugins """
    plugin = 'animImportExport.mll'
    if not mc.pluginInfo(plugin, q=True, l=True):
        mc.loadPlugin(plugin, qt=True)

def check_object_selected():
    return True if mc.ls(sl=True) else False

