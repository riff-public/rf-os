import os 
import sys 
import maya.cmds as mc 

def export_selection(filename, obj): 
	if mc.objExists(obj): 
		mc.select(obj)
		mc.file(fileName, force = True, options = 'v=0', typ = 'mayaAscii', pr = True, es = True)