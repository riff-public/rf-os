import os 
import sys 
import getpass
from functools import partial
import maya.cmds as mc 
import maya.mel as mm 

import rf_config as config 
from rf_utils import log_utils
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name('cache_export_2hero', user)
logger = log_utils.init_logger(logFile)

from rf_utils.context import context_info
from rftool.utils import yeti_lib
from rftool.utils import abc_utils
from rftool.utils import file_utils
reload(yeti_lib)
reload(abc_utils)


def export_alembic(): 
    refs = [a.split('{')[0] for a in mc.file(q=True, r=True)]
    scene = context_info.ContextPathInfo()

    for ref in refs: 
        namespace = mc.referenceQuery(ref, ns=True)[1:]
        cacheGrp = '%s:Geo_Grp' % namespace
        cachePath = scene.path.cache().abs_path()

        asset = context_info.ContextPathInfo(path=ref)
        if asset.type == 'character': 
            cacheFile = '%s.abc' % namespace 
            path = '/'.join([cachePath, namespace, cacheFile])
            path = export_dir(path)
            abc_utils.exportABC(cacheGrp, path)
            logger.info('Export %s' % path)



def export_yeti(): 
    yetiNodes = mc.ls(type='pgYetiMaya')
    scene = context_info.ContextPathInfo()

    for node in yetiNodes: 
        path = mc.referenceQuery(node, f=True).split('{')[0]
        asset = context_info.ContextPathInfo(path=path)
        
        if asset.type == 'character':   
            transform = mc.listRelatives(node, p=True)[0]
            namespace = mc.referenceQuery(transform, ns=True)[1:]
            name = transform.split(':')[-1]
            cachePath = scene.path.cache().abs_path()
            cachePathNode = '/'.join([cachePath, namespace, name])
            cacheName = '%s.' % name + '%04d.fur'
            path = '/'.join([cachePathNode, cacheName])
            startframe = mc.playbackOptions(q=True, min=True)
            endframe = mc.playbackOptions(q=True, max=True)
            path = export_dir(path)

            yeti_lib.export_cache(node, path, startframe, endframe)
            logger.info('Export yeti %s' % path)


def export_dir(filename): 
    dirname = os.path.dirname(filename)
    if not os.path.exists(dirname): 
        os.makedirs(dirname)
    return filename 

def import_content(): 
    scene = context_info.ContextPathInfo()
    cachePath = scene.path.cache().abs_path()
    namespaces = file_utils.listFolder(cachePath)

    for namespace in namespaces: 
        contentPath = ('/').join([cachePath, namespace])
        abcFiles = file_utils.listFile(contentPath)
        yetiDirs = file_utils.listFolder(contentPath)

        if abcFiles: 
            obj = ''
            for abcFile in abcFiles: 
                abcPath = '/'.join([contentPath, abcFile])
                node = import_abc(namespace, abcPath)
                
                print node

        if yetiDirs: 
            for yetiDir in yetiDirs: 
                nodeName = yetiDir
                yetiFilePath = '/'.join([contentPath, yetiDir])
                files = file_utils.listFile(yetiFilePath)

                if files: 
                    fileFormat = get_format('%s/%s' % (yetiFilePath, files[0]))
                    import_yeti(namespace, nodeName, fileFormat)
                    

def import_abc(namespace, path): 
    # set namespace 
    if not mc.namespace(exists=namespace): 
        mc.namespace(add=namespace)
    
    mc.namespace(set=namespace)
    node = get_return(abc_utils.importABC, obj='', path=path, mode='new')

    # grouping 
    rigGrp = mc.group(em=True, n='Rig_Grp')
    mc.parent(node, rigGrp)
    
    mc.namespace(set=':')
    return node 

def import_yeti(namespace, nodeName, fileFormat): 
    rigGrp = '%s:%s' % (namespace, 'Rig_Grp')
    yetiGrp = ''
    if mc.objExists(rigGrp): 
        yetiGrp = '%s:yeti_Grp' % namespace
        if not mc.objExists(yetiGrp): 
            yetiGrp = mc.group(em=True, n=yetiGrp)
            mc.parent(yetiGrp, rigGrp)

    node = yeti_lib.import_cache(nodeName, fileFormat)
    
    if yetiGrp: 
        mc.parent(node, yetiGrp)


def get_format(filepath): 
    basename = os.path.basename(filepath)
    digit = ''
    for elem in basename.split('.'): 
        if elem.isdigit(): 
            digit = elem

    formatName = basename.replace(digit, '%04d')
    return ('/').join([os.path.dirname(filepath), formatName])

def get_return(func, **kwargs): 
    currents = mc.ls(assemblies=True)
    func(**kwargs)
    news = [a for a in mc.ls(assemblies=True) if not a in currents]
    return news 

# example 

# from rftool.misc.export_cache import cache_export_2hero as ec
# reload(ec)
# reload(ec.yeti_lib)

# export 
# ec.export_alembic()
# ec.export_yeti()

# import 
# ec.import_content()

