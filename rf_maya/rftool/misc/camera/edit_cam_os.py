uiName = 'EditCamUi'
_title = 'Edit Camera'
_version = 'v.1.0.0'
_des = ''

import os
import sys
import logging
import maya.cmds as mc
import maya.mel as mm
import maya.OpenMaya as om

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.publish.scene.utils import maya_hook
reload(maya_hook)
from rf_utils.context import context_info
from rf_utils.widget import dialog
reload(dialog)


os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

ICONS_DIR = '{0}/core/icons'.format(os.environ['RFSCRIPT'])

class Ui(QtWidgets.QWidget) :
    app_font = QtGui.QFont("Tahoma", 8)
    button_font = QtGui.QFont("Tahoma", 9)
    button_font.setLetterSpacing(QtGui.QFont.AbsoluteSpacing, 1)
    def __init__(self, parent = None) :
        super(Ui, self).__init__(parent)
        self.allLayout = QtWidgets.QVBoxLayout()

        self.layout1 = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel('Camera:')
        self.label.setFont(self.app_font)
        self.cameraComboBox = QtWidgets.QComboBox()
        self.cameraComboBox.setFont(self.app_font)
        self.spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.layout1.addWidget(self.label)
        self.layout1.addWidget(self.cameraComboBox)
        self.layout1.addItem(self.spacer)

        self.button = QtWidgets.QPushButton()
        self.button.setFont(self.button_font)
        self.exportButton = QtWidgets.QPushButton()
        self.exportButton.setFont(self.button_font)

        # passive
        self.keepRigCheckBox = QtWidgets.QCheckBox('Keep editing')
        self.keepRigCheckBox.setFont(self.app_font)

        # add layout
        self.allLayout.addLayout(self.layout1)
        self.allLayout.addWidget(self.button)
        self.allLayout.addWidget(self.exportButton)
        self.allLayout.addWidget(self.keepRigCheckBox)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(5, 5, 5, 5)
        self.setLayout(self.allLayout)

        # set display
        self.button.setText('Edit')
        self.button.setMinimumSize(QtCore.QSize(0, 30))

        self.exportButton.setText('Export')
        self.exportButton.setMinimumSize(QtCore.QSize(0, 30))
        self.exportButton.setEnabled(False)

class Main(QtWidgets.QMainWindow):
    """docstring for Main"""
    def __init__(self, parent=None):
        super(Main, self).__init__(parent)
        self.ui = Ui(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))
        self.resize(275, 70)
        self.setWindowIcon(QtGui.QIcon('%s/cameraEdit_icon.png' %ICONS_DIR))

        self.init_functions()
        self.init_signals()
        self.init_scriptJob()

    def init_scriptJob(self):
        self.afterLoadRef = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterLoadReference , self.init_functions)
        self.afterUnloadRef = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterUnloadReference , self.init_functions)
        self.afterOpen = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterOpen , self.init_functions)
        self.afterNew = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterNew , self.init_functions)
        self.afterCreateRef = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterCreateReference , self.init_functions)
        self.afterRemoveRef = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterRemoveReference , self.init_functions)

    def closeEvent(self, event):
        logger.info('Closing %s' %_title)
        for callback in (self.afterLoadRef, self.afterUnloadRef, self.afterOpen, self.afterNew, self.afterCreateRef, self.afterRemoveRef):
            try:
                om.MMessage.removeCallback(callback)
            except:
                pass

    def init_functions(self, *args):
        self.scene = context_info.ContextPathInfo()
        self.list_shot()
        self.check_status()

    def init_signals(self):
        self.ui.button.clicked.connect(self.edit)
        self.ui.exportButton.clicked.connect(self.export)
        self.ui.cameraComboBox.currentIndexChanged.connect(self.check_status)

    def edit(self):
        shotName = str(self.ui.cameraComboBox.currentText())
        result = maya_hook.edit_camera(shotName)
        self.check_status()

    def export(self): 
        from rf_app.publish.scene import app
        reload(app)
        
        keepRig = self.ui.keepRigCheckBox.isChecked()
        shotName = str(self.ui.cameraComboBox.currentText())

        # try to bake the camera to free it from any constraints
        obj_toBake, constraints = maya_hook.get_cam_dependencies(shotName)
        if obj_toBake:
            # pop up warning
            dial = dialog.ScrollMessageBox.show(title='Caution!', 
                    message='Camera is a part of constraints, bake keys?', 
                    ls=[n.shortName() for n in obj_toBake])

            if dial.clickedButton() == dial.yes_button:
                maya_hook.bake_transform(shotName, obj_toBake)
                maya_hook.delete_nodes([c.shortName() for c in constraints])
            else:
                return

        # export .abc and .ma
        data = app.get_export_dict(self.scene, shotName, ['camera'])
        app.do_export(self.scene, itemDatas=data, uiMode=False, publish=True)

        # if keepRig is not checked, swap reference with .abc, else the .ma rig stay the same
        if not keepRig:
            maya_hook.switch_hero_cam(self.scene.copy(), shotName)

        # make sure it's under Cam_Grp
        maya_hook.group_cam(shotName)
        self.check_status()

    def list_shot(self):
        self.ui.cameraComboBox.clear()
        shots = maya_hook.list_shot()
        if not shots:
            return
        self.ui.cameraComboBox.addItems(shots)

        # set active shot
        currentShot = self.scene.name_code
        if currentShot:
            activeIndex = [i for i, a in enumerate(shots) if currentShot in a]
        self.ui.cameraComboBox.setCurrentIndex(activeIndex[0]) if activeIndex else None

    def check_status(self):
        # set default state of No camera
        self.ui.button.setEnabled(False)
        self.ui.exportButton.setEnabled(False)

        # get selected shot in shot comboBox
        shotName = str(self.ui.cameraComboBox.currentText())
        if not shotName:
            return

        # find camera
        camGrp = maya_hook.find_cam_grp(shotName)

        # if we find any shot camera
        if camGrp:
            # if it's an active cam, disable edit button and enable export button
            if maya_hook.is_active_cam(camGrp):
                self.ui.button.setEnabled(False)
                self.ui.exportButton.setEnabled(True)
            # if it's a passive cam, enable edit button and disable export button
            else:
                self.ui.button.setEnabled(True)
                self.ui.exportButton.setEnabled(False)

def show():
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = Main(maya_win.getMayaWindow())
    myApp.show()
    return myApp
