uiName = 'EditCamUi'
_title = 'Edit Camera'
_version = 'v.1.0.0'
_des = ''

import os
import sys
import logging
import maya.cmds as mc
import maya.mel as mm
import maya.OpenMaya as om

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_app.publish.scene.utils import maya_hook
reload(maya_hook)
from rf_maya.rftool.utils import maya_utils
from rf_utils.context import context_info
from rf_utils.widget import dialog
reload(dialog)
from rf_maya.lib.sequencer_lib import *
from rf_utils.pipeline import filelock
reload(filelock)
from rf_utils import register_shot
from rf_maya.lib import shot_lib
from rf_app.builder.model.data_format import camera
from rf_utils.widget import history_widget
reload(history_widget)


LOCALSERVER = True 
if os.environ.get('RFSTUDIO') == 'OS': 
    LOCALSERVER = False

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

ICONS_DIR = '{0}/core/icons'.format(os.environ['RFSCRIPT'])
LOCAL_ICON = '{}/icons'.format(os.path.dirname(__file__).replace('\\', '/'))


class Ui(QtWidgets.QWidget) :
    app_font = QtGui.QFont("Tahoma", 8)
    button_font = QtGui.QFont("Tahoma", 9)
    button_font.setLetterSpacing(QtGui.QFont.AbsoluteSpacing, 1)
    def __init__(self, parent = None) :
        super(Ui, self).__init__(parent)
        self.allLayout = QtWidgets.QVBoxLayout()

        self.layout1 = QtWidgets.QHBoxLayout()
        self.lockLayout = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel('Camera:')
        self.label.setFont(self.app_font)
        self.cameraComboBox = QtWidgets.QComboBox()
        self.cameraComboBox.setFont(self.app_font)
        self.refreshButton = QtWidgets.QPushButton('')
        self.refreshButton.setFlat(True)
        self.owner = QtWidgets.QLabel('-')
        self.lockButton = QtWidgets.QPushButton('')
        self.lockButton.setFlat(True)
        self.historyButton = QtWidgets.QPushButton('')
        self.historyButton.setFlat(True)
        self.historyButton.setMaximumSize(QtCore.QSize(20, 20))
        self.spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.layout1.addWidget(self.label)
        self.layout1.addWidget(self.cameraComboBox)
        self.layout1.addWidget(self.lockButton)
        self.layout1.addWidget(self.owner)
        self.layout1.addItem(self.spacer)
        self.layout1.addLayout(self.lockLayout)
        self.lockLayout.addWidget(self.historyButton)
        self.lockLayout.addWidget(self.refreshButton)
        self.lockLayout.setSpacing(2)

        self.button = QtWidgets.QPushButton()
        self.button.setFont(self.button_font)
        self.exportButton = QtWidgets.QPushButton()
        self.exportButton.setFont(self.button_font)

        # status layout 
        self.statusLayout = QtWidgets.QHBoxLayout()

        # passive
        self.keepRigCheckBox = QtWidgets.QCheckBox('Keep editing')
        self.keepRigCheckBox.setFont(self.app_font)

        # publish 
        self.publishCheckBox = QtWidgets.QCheckBox('Publish')
        self.publishCheckBox.setFont(self.app_font)
        self.publishCheckBox.setChecked(False)

        self.statusLayout.addWidget(self.keepRigCheckBox)
        self.statusLayout.addWidget(self.publishCheckBox)

        # add layout
        self.allLayout.addLayout(self.layout1)
        self.allLayout.addWidget(self.button)
        self.allLayout.addWidget(self.exportButton)
        self.allLayout.addLayout(self.statusLayout)

        self.allLayout.setSpacing(8)
        self.allLayout.setContentsMargins(5, 5, 5, 5)
        self.setLayout(self.allLayout)

        # set display
        self.button.setText('Edit')
        self.button.setMinimumSize(QtCore.QSize(0, 30))

        self.exportButton.setText('Export')
        self.exportButton.setMinimumSize(QtCore.QSize(0, 30))
        self.exportButton.setEnabled(False)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap('{}/refresh.png'.format(LOCAL_ICON)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.refreshButton.setIcon(icon)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap('{}/16/history_icon.png'.format(ICONS_DIR)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.historyButton.setIcon(icon)


class Main(QtWidgets.QMainWindow):
    """docstring for Main"""
    def __init__(self, parent=None):
        super(Main, self).__init__(parent)
        self.ui = Ui(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s %s' % (_title, _version, _des))
        self.resize(300, 70)
        self.setWindowIcon(QtGui.QIcon('%s/cameraEdit_icon.png' % ICONS_DIR))

        self.init_functions()
        self.init_signals()
        self.init_scriptJob()

    def init_scriptJob(self):
        self.afterLoadRef = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterLoadReference , self.refresh_data)
        self.afterUnloadRef = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterUnloadReference , self.refresh_data)
        self.afterOpen = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterOpen , self.refresh_data)
        self.afterNew = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterNew , self.refresh_data)
        self.afterCreateRef = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterCreateReference , self.refresh_data)
        self.afterRemoveRef = om.MSceneMessage.addCallback(om.MSceneMessage.kAfterRemoveReference , self.refresh_data)

    def closeEvent(self, event):
        logger.info('Closing %s' %_title)
        for callback in (self.afterLoadRef, self.afterUnloadRef, self.afterOpen, self.afterNew, self.afterCreateRef, self.afterRemoveRef):
            try:
                om.MMessage.removeCallback(callback)
            except:
                pass

    def init_functions(self, *args):
        self.refresh_data()
        self.output = self.init_file_permission()
        self.check_lock_state()

    def refresh_data(self, *args): 
        self.scene = context_info.ContextPathInfo()
        self.list_shot()
        self.check_status()

    def check_lock_state(self): 
        self.owner, self.lock = filelock.check_owner(self.output)
        self.ui.owner.setText(self.owner)
        self.set_lock_icon(self.lock)

    def init_file_permission(self): 
        shotName = str(self.ui.cameraComboBox.currentText())
        namespace = maya_hook.find_cam_namespace(shotName)
        return filelock.init_permission(self.scene, namespace)

    def init_signals(self):
        self.ui.button.clicked.connect(self.edit)
        self.ui.exportButton.clicked.connect(self.export)
        self.ui.cameraComboBox.currentIndexChanged.connect(self.check_status)
        self.ui.publishCheckBox.stateChanged.connect(self.publish_checked)
        self.ui.lockButton.clicked.connect(self.lock_clicked)
        self.ui.refreshButton.clicked.connect(self.reload)
        self.ui.historyButton.clicked.connect(self.show_history)

    def edit(self):
        shotName = str(self.ui.cameraComboBox.currentText())
        permission = filelock.check_edit_right(self.output) if LOCALSERVER else False
        if permission: 
            result = maya_hook.edit_camera(shotName)
            self.check_status()
            self.check_lock_state()
        else:
            result = maya_hook.edit_camera(shotName)

    def export(self): 
        from rf_app.publish.scene import app
        reload(app)
        from rf_app.publish.scene import export_cmd
        reload(export_cmd)

        permission = filelock.check_export_right(self.output) if LOCALSERVER else True
        if permission: 
            keepRig = self.ui.keepRigCheckBox.isChecked()
            shotName = str(self.ui.cameraComboBox.currentText())

            # try to bake the camera to free it from any constraints
            camshot = camera_info(shotName)
            maya_hook.set_overscan(camshot, 1)
            
            obj_toBake, constraints = maya_hook.get_cam_dependencies(shotName)
            if obj_toBake:
                # pop up warning
                dial = dialog.ScrollMessageBox.show(title='Caution!', 
                        message='Camera is a part of constraints, bake keys?', 
                        ls=[n.shortName() for n in obj_toBake])

                if dial.clickedButton() == dial.yes_button:
                    maya_hook.bake_transform(shotName, obj_toBake)
                    maya_hook.delete_nodes([c.shortName() for c in constraints])
                else:
                    return

            # export .abc and .ma
            # publish 
            if self.ui.publishCheckBox.isChecked(): 
                data = app.get_export_dict(self.scene, shotName, ['camera'])
                app.do_export(self.scene, itemDatas=data, uiMode=False, publish=True)
                # if keepRig is not checked, swap reference with .abc, else the .ma rig stay the same
                if LOCALSERVER:
                    if not keepRig:
                        maya_hook.switch_hero_cam(self.scene.copy(), shotName)
                        self.set_sg_status(self.scene, status='apr')

                    filelock.lock(self.output, False, self.scene.path)
                    self.check_lock_state()
            else: 
                # export only, file will be in /work/.../_output
                export_cmd.run(self.scene, module='camera', processes=['mayaCam', 'cacheCam'], dst_type='local')
                self.set_sg_status(self.scene, status='pub')

            # make sure it's under Cam_Grp
            maya_hook.group_cam(shotName)
            # lock node camera
            cam = maya_hook.find_cam(self.scene.name_code)
            maya_utils.lock_transform_rotate(cam)
            self.check_status()

    def set_sg_status(self, scene, status): 
        # works only for finalcam
        from rf_utils.sg import sg_process
        reload(sg_process)
        if scene.step == 'finalcam': 
            taskname = 'finalCam'
            logger.info('Set status for "{}" to "Pending Publish"'.format(taskname))
            return sg_process.set_task(scene.project, 'Shot', scene.name, taskname, status)

    def list_shot(self):
        self.ui.cameraComboBox.clear()
        shots = maya_hook.list_shot()
        if not shots:
            return
        self.ui.cameraComboBox.addItems(shots)

        # set active shot
        currentShot = self.scene.name_code
        if currentShot:
            activeIndex = [i for i, a in enumerate(shots) if currentShot in a]
        self.ui.cameraComboBox.setCurrentIndex(activeIndex[0]) if activeIndex else None

    def check_status(self):
        # set default state of No camera
        self.ui.button.setEnabled(False)
        self.ui.exportButton.setEnabled(False)

        # get selected shot in shot comboBox
        shotName = str(self.ui.cameraComboBox.currentText())
        if not shotName:
            return

        # find camera
        camGrp = maya_hook.find_cam_grp(shotName)

        # if we find any shot camera
        if camGrp:
            # if it's an active cam, disable edit button and enable export button
            if maya_hook.is_active_cam(camGrp):
                self.ui.button.setEnabled(False)
                self.ui.exportButton.setEnabled(True)
            # if it's a passive cam, enable edit button and disable export button
            else:
                self.ui.button.setEnabled(True)
                self.ui.exportButton.setEnabled(False)

    def publish_checked(self): 
        state = self.ui.publishCheckBox.isChecked()
        self.ui.keepRigCheckBox.setEnabled(state)

    def set_lock_icon(self, value): 
        lock = '{}/16/lock_icon.png'.format(ICONS_DIR)
        unlock = '{}/16/unlock_icon.png'.format(ICONS_DIR)
        path = lock if value else unlock
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(path), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.lockButton.setIcon(icon)

    def lock_clicked(self): 
        filelock.lock(self.output, not self.lock)
        self.check_lock_state()

    def reload(self): 
        shotName = str(self.ui.cameraComboBox.currentText())
        camGrp = maya_hook.find_cam_grp(shotName)

        if maya_hook.is_active_cam(camGrp): 
            result = dialog.CustomMessageBox.show('Warning', 'Current edit will be lost. Continue?', ['Yes', 'Cancel'])
            if result.value == 'Cancel': 
                return 
        reg = register_shot.Register(self.scene)
        namespace = maya_hook.find_cam_namespace(shotName)
        camera.remove(namespace, reg, self.scene)
        camera.build(namespace, reg, self.scene)
        cam = maya_hook.find_cam(self.scene.name_code)
        maya_utils.lock_transform_rotate(cam)
        self.check_status()

    def show_history(self): 
        [a.deleteLater() for a in self.findChildren(QtWidgets.QMainWindow)]
        win = QtWidgets.QMainWindow(self)
        self.history_widget = history_widget.History(self)
        win.setCentralWidget(self.history_widget)
        win.setObjectName('history')
        win.setWindowTitle('History')
        win.show()
        self.history_widget.set_data(self.output.meta)


def show():
    from rftool.utils.ui import maya_win
    maya_win.deleteUI(uiName)
    myApp = Main(maya_win.getMayaWindow())
    myApp.show()
    return myApp
