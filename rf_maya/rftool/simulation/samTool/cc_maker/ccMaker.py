import pymel.core as pm

from rf_utils.ui import load as loadUi
from rftool.utils.ui import maya_win
from Qt import wrapInstance, QtGui, QtCore, QtWidgets

uiPath = 'O:/Pipeline/core/rf_maya/rftool/simulation/samTool/cc_maker/ccMaker.ui'
form_class, base_class = loadUi.loadUiType(uiPath)

class CC_Maker_Class(form_class, base_class):
	def __init__(self, parent):
		super(CC_Maker_Class, self).__init__(parent)
		self.setupUi(self)
		self.clusterMaker_button.clicked.connect(self.clusterMaker)
		self.clusterMaker_button.setStyleSheet("background-color:#0047b3")
		self.crvMaker_button.clicked.connect(self.crvMaker)
		self.crvMaker_button.setStyleSheet("background-color:#cc0000")
		self.show()
	
	def clusterMaker ( self ) :
		selected = pm.selected ()
		ring_lst = []
		loop_lst = []
		cluster_lst = []
		for sel in selected :
			ring = pm.polySelectSp( sel , r = True )
			grp_n = sel.split('.')[0]+'_GRP'
			if not pm.objExists ( grp_n ) :
				grp_n = pm.group ( n = grp_n , em = True , w = True )
		for rng in ring :
			edge = rng.split( '[' )[1].split( ']' )[0]
			ring_lst.append(edge)
		for rng_lst in ring_lst :
			if ':' in rng_lst :
				first = rng_lst.split( ':' )[0]
				end = rng_lst.split( ':' )[1]
				rows = range ( int ( first ) , int ( end ) + 1 )
				for row in rows :
					loop_lst.append ( str ( row ) )
			else :
				loop_lst.append ( rng_lst ) 
		for loop in loop_lst :
			ply = pm.polySelect ( sel.split('.')[0] , elb = int ( loop ))
			clus = pm.cluster ( n = sel.split('.')[0]+'_Cluster' )
			cluster_lst.append ( clus[1] )
			pm.parent ( clus[1] , grp_n )
#######################################################################
	def crvMaker ( self ) :
		loc = []
		cl_sel = pm.selected()
		for cl in cl_sel :
			name = cl.split('_')[0] + '_CRV'
			x_cl = pm.xform ( cl , q = True , rp = True , ws = True )
			loc.append ( x_cl )
		pm.curve ( n = name , p = loc , d = 3 , ws = 1 )
####################################################################
def show () :
	global cc_maker
	try:
		cc_maker.close()
	except Exception, e:
		pass
	cc_maker = CC_Maker_Class ( parent = maya_win.getMayaWindow() )
	return cc_maker
show ()