import pymel.core as pm ;
import maya.cmds as mc ;
import ast ;

##################
''' append item commands '''
##################

def appendItem_append_cmd ( *args ) :
    selection = mc.ls ( sl = True ) ;
    selectionList = [] ;
    
    list = pm.textScrollList ( 'appendItem_textScrollList' , q = True , allItems = True ) ;
    if str ( selection ) not in list :
        pm.textScrollList ( 'appendItem_textScrollList' , e = True , append = str ( selection ) ) ;

def appendItem_removeItem_cmd ( *args ) :
    
    selectItem = pm.textScrollList ( 'appendItem_textScrollList' , q = True , selectItem = True ) ;
    
    for each in selectItem :
        
        pm.textScrollList ( 'appendItem_textScrollList' , e = True , removeItem = selectItem ) ;

def appendItem_clear_cmd ( *args ) :
    pm.textScrollList ( 'appendItem_textScrollList' , e = True , ra = True ) ;

##################
''' aim object commands '''
##################

def aimObject_get_cmd ( *args ) :
    selection = pm.ls ( sl = True ) ;
    if len ( selection ) != 1 :
        pass ;
    else :
        selection = selection[0] ;
        pm.textField ( 'aimObject_textField' , e = True , text = selection ) ;

def aimObject_createAimLoc_cmd ( *args ) :
    aim_LOC = pm.spaceLocator ( p = ( 0 , 0 , 0 ) , n = 'aim_LOC' ) ;
    pm.textField ( 'aimObject_textField' , e = True , text = aim_LOC ) ;

def aimObject_clear_cmd ( *args ) :
    pm.textField ( 'aimObject_textField' , e = True , text = '' ) ;

##################
''' create joint commands '''
##################

def createJoint_create_cmd ( selection = [] , *args ) :

    aimObject = pm.textField ( 'aimObject_textField' , q = True , text = True ) ;
       
    jointList = [] ;
    
    for each in selection :
        
        cluster = pm.cluster ( each ) ;
    
        pos = pm.xform ( cluster , q = True , ws = True , rp = True ) ;
        print pos ;
        
        joint = pm.createNode ( 'joint' ) ;
        
        pm.xform ( joint , t = pos ) ;
     
        jointList.append ( joint ) ;
        
        pm.delete ( cluster ) ;
        
    for i in range ( 0 , len ( jointList ) ) :
        
        if i != len ( jointList ) -1 :
                
            orientOption = pm.optionMenu( 'orient_optionMenu' , q = True , v = True ) ;
            print orientOption ;
            
            if orientOption == 'y up' :
                pm.select ( jointList [i] , jointList [i+1] ) ;
            
            elif orientOption == 'y down' :
                pm.select ( jointList [i+1] , jointList [i] ) ;
            

            if aimObject == '' :
                con = pm.aimConstraint (
                    offset = [ 0.0 , 0.0 , 0.0 ] ,
                    weight = 1 ,
                    aimVector = [ 0.0 , 1.0 , 0.0 ] ,
                    upVector = [ 0.0 , 0.0 , 1.0 ] ,
                    worldUpType = 'scene' ,
                    skip = 'none'
                    ) ;
            else :
                con = pm.aimConstraint (
                    offset = [ 0.0 , 0.0 , 0.0 ] ,
                    weight = 1 ,
                    aimVector = [ 0.0 , 1.0 , 0.0 ] ,
                    upVector = [ 0.0 , 0.0 , 1.0 ] ,
                    worldUpType = 'object' ,
                    worldUpObject = aimObject ,
                    skip = 'none'
                    ) ;

            #con = pm.mel.eval ( 'aimConstraint -offset 0 0 0 -weight 1 -aimVector 0 1 0 -upVector 0 0 1 -worldUpType "scene" -skip "none";' ) ;
            pm.delete ( con ) ;
        
        else: pass ;
       
        if orientOption == 'y up' :
            pm.select ( jointList[1] , jointList [0] ) ; 
            
            if aimObject == '' :
                con = pm.aimConstraint (
                    offset = [ 0.0 , 0.0 , 0.0 ] ,
                    weight = 1 ,
                    aimVector = [ 0.0 , -1.0 , 0.0 ] ,
                    upVector = [ 0.0 , 0.0 , 1.0 ] ,
                    worldUpType = 'scene' ,
                    skip = 'none'
                    ) ;
            else :
                con = pm.aimConstraint (
                    offset = [ 0.0 , 0.0 , 0.0 ] ,
                    weight = 1 ,
                    aimVector = [ 0.0 , -1.0 , 0.0 ] ,
                    upVector = [ 0.0 , 0.0 , 1.0 ] ,
                    worldUpType = 'object' ,
                    worldUpObject = aimObject ,
                    skip = 'none'
                    ) ;

            #con = pm.mel.eval ( 'aimConstraint -offset 0 0 0 -weight 1 -aimVector 0 -1 0 -upVector 0 0 1 -worldUpType "scene" -skip "none";' ) ;
            pm.delete ( con ) ;
        elif orientOption == 'y down' :
            pm.select ( jointList[-2] , jointList [-1] ) ;

            if aimObject == '' :
                con = pm.aimConstraint (
                    offset = [ 0.0 , 0.0 , 0.0 ] ,
                    weight = 1 ,
                    aimVector = [ 0.0 , -1.0 , 0.0 ] ,
                    upVector = [ 0.0 , 0.0 , 1.0 ] ,
                    worldUpType = 'scene' ,
                    skip = 'none'
                    ) ;
            else :
                con = pm.aimConstraint (
                    offset = [ 0.0 , 0.0 , 0.0 ] ,
                    weight = 1 ,
                    aimVector = [ 0.0 , -1.0 , 0.0 ] ,
                    upVector = [ 0.0 , 0.0 , 1.0 ] ,
                    worldUpType = 'object' ,
                    worldUpObject = aimObject ,
                    skip = 'none'
                    ) ;

            #con = pm.mel.eval ( 'aimConstraint -offset 0 0 0 -weight 1 -aimVector 0 -1 0 -upVector 0 0 1 -worldUpType "scene" -skip "none";' ) ;
            pm.delete ( con ) ;
        else : pass ;

# aimConstraint -offset 0 0 0 -weight 1 -aimVector 0 1 0 -upVector 0 0 1 -worldUpType "object" -worldUpObject locator2;

def createJoint_button_cmd ( *args ) :
    toClusterList = pm.textScrollList ( 'appendItem_textScrollList' , q = True , allItems = True ) ;
    workingList = [] ;
       
    for each in toClusterList :   
        workingList.append ( ast.literal_eval ( each ) ) ;
        
    createJoint_create_cmd ( selection = workingList ) ;

######################################################
######################################################
''' UI '''
######################################################
######################################################

##################
''' CONFIG '''
##################

width = 500.00 ;

title = 'bHairJoint creator' ;
version = 'v2.0' ;

##################
''' UI COMMAND '''
##################

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

##################
''' UI '''
##################

def createHairJointUI ( *args ) :
    
    # check if window exists
    if pm.window ( 'createHairJointUI' , exists = True ) :
        pm.deleteUI ( 'createHairJointUI' ) ;
    else : pass ;
   
    window = pm.window ( 'createHairJointUI', title = title + ' ' + version , mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    pm.window ( 'createHairJointUI' , e = True , w = width , h = 10 ) ;

    with window :
        
        mainLayout = pm.rowColumnLayout ( nc = 1 , w = width ) ;
        with mainLayout :

            s1_width = width/10*7 ;
            s2_width = width/10*3 ;

            ##################
            ''' append item section '''
            ##################

            pm.text ( label = 'APPEND ITEM LIST' , font = 'boldLabelFont' , bgc = ( 0 , 0 , 0.25 ) ) ;
            
            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , s1_width )  , ( 2 , s2_width ) ] ) :

                appendItemHeight = 160.00 ;

                pm.textScrollList ( 'appendItem_textScrollList' , ams = True , w = s1_width , h = appendItemHeight ) ;

                with pm.rowColumnLayout ( nc = 1 , w = s2_width ) :
                    pm.button ( 'appendButton' , label = 'append' , w = s2_width , h = appendItemHeight/3 , c = appendItem_append_cmd ) ;
                    pm.button ( 'deleteButton' , label = 'remove' , w = s2_width , h = appendItemHeight/3 , c = appendItem_removeItem_cmd ) ;
                    pm.button ( 'clearButton' , label = 'clear' , w = s2_width , h = appendItemHeight/3 , c = appendItem_clear_cmd ) ;

            separator ( ) ;

            ##################
            ''' aim object '''
            ##################

            pm.text ( label = 'AIM OBJECT (z-axis) (optional)' , font = 'boldLabelFont' , bgc = ( 0 , 0 , 0.25 ) ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , s1_width )  , ( 2 , s2_width ) ] ) :

                aimObjectHeight = 80.00 ;

                pm.textField ( 'aimObject_textField' , ed = True , h = aimObjectHeight , w = s1_width ) ;

                with pm.rowColumnLayout ( nc = 1 , w = s2_width ) :
                    pm.button ( label = 'get aim object' , w = s2_width , h = aimObjectHeight/3 , c = aimObject_get_cmd ) ;
                    pm.button ( label = 'create aim locator' , w = s2_width , h = aimObjectHeight/3 , c = aimObject_createAimLoc_cmd ) ;
                    pm.button ( label = 'clear' , w = s2_width , h = aimObjectHeight/3 , c = aimObject_clear_cmd ) ;

            separator ( ) ;

            ##################
            ''' create joints section '''
            ##################

            pm.text ( label = 'CREATE JOINTS' , font = 'boldLabelFont' , bgc = ( 0 , 0.25 , 0.25 ) ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , s1_width ) , ( 2 , s2_width ) ] ) :

                orient_OptionMenu = pm.optionMenu( 'orient_optionMenu' , label = '  orient option  ' ) ;
                with orient_OptionMenu :
                    pm.menuItem ( label='y down' ) ;
                    pm.menuItem ( label='y up' ) ;

                pm.button ( 'createJointButton' , label = 'create joint' , c = createJoint_button_cmd , bgc = ( 0.678431 , 1 , 0.184314 ) ) ;

    window.show () ;

#createHairJointUI ( ) ;