//Maya ASCII 2018 scene
//Name: joints.ma
//Last modified: Sun, May 06, 2018 04:12:36 AM
//Codeset: 1252
requires maya "2018";
requires "stereoCamera" "10.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
createNode joint -n "root_jnt";
	rename -uid "5628095E-4120-FE23-1331-C2A0A0845BC6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 9.0474036391524066 0.46364678601588916 ;
	setAttr ".ro" 2;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 9.0474036391524066 0.46364678601588916 1;
createNode joint -n "spine1_jnt" -p "|root_jnt";
	rename -uid "E6041ED5-406E-EC0B-FCB7-5CB72A634BDD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.14702947047595849 0.025390751104005405 ;
	setAttr ".ro" 2;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 9.1944331096283651 0.48903753711989456 1;
	setAttr ".liw" yes;
createNode joint -n "spine2_jnt" -p "|root_jnt|spine1_jnt";
	rename -uid "96DAD1A4-4F99-C172-7F97-96B5201A8A27";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 4.8148248609680896e-35 0.61738640069961548 0.0083066222588729643 ;
	setAttr ".ro" 2;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 4.8148248609680896e-35 9.8118195103279806 0.49734415937876753 1;
	setAttr ".liw" yes;
createNode joint -n "spine3_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt";
	rename -uid "14BC7D50-4504-A79A-914A-019F543EDBF7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.9726337455386263e-31 0.57628375291824518 -0.0095053938814134709 ;
	setAttr ".ro" 2;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -1.9721522630525295e-31 10.388103263246226 0.48783876549735405 1;
	setAttr ".liw" yes;
createNode joint -n "spine4_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt";
	rename -uid "EA9D3958-464E-0F0D-46C7-B9B619F172CD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.9721522630525295e-31 0.625671446323393 -0.072969996200875686 ;
	setAttr ".ro" 2;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 11.013774709569619 0.41486876929647837 1;
	setAttr ".liw" yes;
createNode joint -n "spine5_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt";
	rename -uid "80D2E628-426E-1D27-CF2B-859E51EBFD75";
	setAttr ".t" -type "double3" 2.8349688854848509e-31 0.63683989551238795 -0.098406961776039692 ;
	setAttr ".ro" 2;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 2.8349688854848509e-31 11.650614605082007 0.31646180752043868 1;
createNode joint -n "chest1_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt";
	rename -uid "4B01AEF2-440D-F2A7-15BF-ECAB3268B6D8";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.8349688854848509e-31 -2.4092708272860364e-08 -2.7755575615628914e-16 ;
	setAttr ".ro" 2;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 11.650614580989298 0.3164618075204384 1;
	setAttr ".liw" yes;
createNode joint -n "chest2_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt";
	rename -uid "582E4153-4E5D-F036-ED9F-5B90AA4EB1B8";
	setAttr ".t" -type "double3" 0 1.0524113178253174 -0.19728429691847735 ;
	setAttr ".ro" 2;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 12.703025898814616 0.11917751060196105 1;
createNode joint -n "neck1_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt";
	rename -uid "234FCCD7-4D98-0C17-49B4-638BB57FBE8A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 4.7814907588872302e-09 -1.1102230246251565e-16 ;
	setAttr ".ro" 2;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 12.703025903596107 0.11917751060196094 1;
	setAttr ".liw" yes;
createNode joint -n "neck2_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt";
	rename -uid "9A05860D-4270-9E42-DDB9-3EAE3730393A";
	setAttr ".t" -type "double3" 0 0.59665977954864502 0.056067829925552148 ;
	setAttr ".ro" 2;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 13.299685683144752 0.17524534052751309 1;
createNode joint -n "head1_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt";
	rename -uid "326F2981-4B61-7DD7-004C-899A583C6BFA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -1.7763568394002505e-15 0 ;
	setAttr ".ro" 2;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 13.29968568314475 0.17524534052751309 1;
	setAttr ".liw" yes;
createNode joint -n "head2_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt";
	rename -uid "86BC471C-49CF-B31E-5CF7-AD902CB5218C";
	setAttr ".t" -type "double3" 0 3.6051693522310995 0 ;
	setAttr ".ro" 2;
	setAttr ".ssc" no;
createNode parentConstraint -n "head1_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt";
	rename -uid "D8BB1EDA-45D4-1266-6514-15804275B3C0";
	addAttr -ci true -k true -sn "w0" -ln "headGmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 13.299685683144752 0.17524534052751309 1;
	setAttr ".tg[0].trp" -type "double3" 0 -1.7763568394002505e-15 0 ;
	setAttr ".tg[0].tro" 2;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -13.299685683144752 -0.17524534052751309 1;
	setAttr ".rst" -type "double3" 0 13.29968568314475 0.17524534052751309 ;
	setAttr ".cro" 2;
	setAttr -k on ".w0";
createNode scaleConstraint -n "head1_jnt_scaleConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt";
	rename -uid "BABF570D-47A1-7CDD-CADE-859DF1F244B1";
	addAttr -ci true -k true -sn "w0" -ln "headGmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 13.299685683144752 0.17524534052751309 1;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -13.299685683144752 -0.17524534052751309 1;
	setAttr -k on ".w0";
createNode joint -n "eyeLFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt";
	rename -uid "034B6A2C-4FFC-E761-AC85-29A9D66317C9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.53303895393666756 1.3244769418262248 0.73540792166978142 ;
	setAttr ".ro" 2;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.53303895393666756 14.624162624970975 0.91065326219729448 1;
createNode parentConstraint -n "eyeLFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|eyeLFT_jnt";
	rename -uid "58A5E1EA-42F6-E8E1-8ADA-4AA0A858775F";
	addAttr -ci true -k true -sn "w0" -ln "eyeLFTGmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.53303895393666756 14.624162624970971 0.9106532621972947 1;
	setAttr ".tg[0].trp" -type "double3" 0 3.5527136788005009e-15 -2.2204460492503131e-16 ;
	setAttr ".tg[0].tro" 2;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -13.29968568314475 -0.17524534052751309 1;
	setAttr ".rst" -type "double3" 0.53303895393666756 14.624162624970975 0.91065326219729448 ;
	setAttr ".cro" 2;
	setAttr -k on ".w0";
createNode joint -n "eyeRGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt";
	rename -uid "BF68B0DA-4DEC-DD6F-D700-E8B3CB1DBC13";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.53303895393666334 1.324476941826223 0.73540792166978453 ;
	setAttr ".ro" 2;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.53303895393666334 14.624162624970973 0.91065326219729759 1;
createNode parentConstraint -n "eyeRGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|eyeRGT_jnt";
	rename -uid "44B8449B-4979-9849-C4D4-2C967B8BA624";
	addAttr -ci true -k true -sn "w0" -ln "eyeRGTGmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.53303895393666334 14.624162624970973 0.91065326219729759 1;
	setAttr ".tg[0].tro" 2;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -13.29968568314475 -0.17524534052751309 1;
	setAttr ".rst" -type "double3" -0.53303895393666334 14.624162624970973 0.91065326219729759 ;
	setAttr ".cro" 2;
	setAttr -k on ".w0";
createNode joint -n "jaw1UPR_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt";
	rename -uid "BF3E2BC4-4E53-99A5-542F-FAB010413E71";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.5151133063232276 1.5566439332355393 ;
	setAttr ".ro" 5;
	setAttr ".jo" -type "double3" 0 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".liw" yes;
createNode joint -n "jaw2UPR_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1UPR_jnt";
	rename -uid "DA1793C0-4319-DD73-10AB-7897F9962FDC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0 0.13437547470475919 ;
	setAttr ".ro" 5;
	setAttr ".liw" yes;
createNode parentConstraint -n "jaw1UPR_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1UPR_jnt";
	rename -uid "6B28B692-4E43-8911-AD3D-8EA055F0EB12";
	addAttr -ci true -k true -sn "w0" -ln "jawGmblUPR_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" -1 -1.2246467991473532e-16 0 0 1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0 13.814798989467977 1.7318892737630522 1;
	setAttr ".tg[0].trp" -type "double3" 0 0 2.2204460492503131e-16 ;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -13.29968568314475 -0.17524534052751309 1;
	setAttr ".rst" -type "double3" 0 0.5151133063232276 1.5566439332355393 ;
	setAttr ".cro" 5;
	setAttr ".cjo" -type "double3" 0 0 -180 ;
	setAttr -k on ".w0";
createNode joint -n "jaw1LWR_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt";
	rename -uid "08FD4C11-40F5-C640-83CF-0F8B23E720D0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.52169753756365722 0.060512422251956993 ;
	setAttr ".ro" 5;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 13.821383220708407 0.23575776277947008 1;
	setAttr ".liw" yes;
createNode joint -n "jaw2LWR_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt";
	rename -uid "304B9938-4018-5244-EE4A-5986AF6D142C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.12356993863045496 1.4961315109835822 ;
	setAttr ".ro" 5;
	setAttr ".liw" yes;
createNode joint -n "jaw3LWR_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|jaw2LWR_jnt";
	rename -uid "20DFE8F4-420E-B2E0-79F6-968D2F7FBC1C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0 0.13437547470475919 ;
	setAttr ".ro" 5;
	setAttr ".liw" yes;
createNode parentConstraint -n "jaw2LWR_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|jaw2LWR_jnt";
	rename -uid "D177E0D7-440D-F7B7-6340-3880CFED8A01";
	addAttr -ci true -k true -sn "w0" -ln "jaw2GmblLWR_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 13.697813282077952 1.7318892737630522 1;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -13.821383220708407 -0.23575776277947008 1;
	setAttr ".rst" -type "double3" 0 -0.12356993863045496 1.4961315109835822 ;
	setAttr ".cro" 5;
	setAttr -k on ".w0";
createNode joint -n "togue1_posJnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt";
	rename -uid "826C763D-4A4D-8715-5EDD-01B7EF943A7A";
	setAttr ".t" -type "double3" 3.944304526105059e-31 -0.17649958887574968 0.41883006060950895 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 146.85706213730634 -3.5311250384401269e-31 180 ;
createNode joint -n "togue1_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt";
	rename -uid "32C19E9B-4009-B945-039E-7FA33221B87E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 9.774518331083222e-16 -7.1054273576010019e-15 0 ;
	setAttr ".ro" 1;
createNode joint -n "togue2_posJnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt";
	rename -uid "E2FAC23A-489F-6E83-9D68-1DA0178A7204";
	setAttr ".t" -type "double3" -9.8607613152626476e-31 0.17444805800914764 -0.065627077695556757 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -56.857062137306343 0 0 ;
createNode joint -n "togue2_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt";
	rename -uid "3826CBCE-4D93-6988-7F94-13B210FAF380";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.944304526105059e-31 3.219646771412954e-15 3.5527136788005009e-15 ;
	setAttr ".ro" 1;
createNode joint -n "togue3_posJnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt";
	rename -uid "133FF810-4C57-3DC0-0A57-B8B145F90C97";
	setAttr ".t" -type "double3" 0 0.26332131028175365 -0.075634846457901972 ;
	setAttr ".ro" 1;
createNode joint -n "togue3_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt";
	rename -uid "ADC41981-4C75-1A3D-ACB1-BAB0DC1CB38C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.944304526105059e-31 -2.2204460492503131e-16 0 ;
	setAttr ".ro" 1;
createNode joint -n "togue4_posJnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt|togue3_jnt";
	rename -uid "9D297B07-415E-1AA4-868F-E48263E6D99A";
	setAttr ".t" -type "double3" 5.9164567891575885e-31 0.25051537156104997 -0.057367277801956718 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -5.9999999999999885 0 0 ;
createNode joint -n "togue4_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt|togue3_jnt|togue4_posJnt";
	rename -uid "F45EA450-4969-56C9-932B-76809196D9B7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1832913578315177e-30 6.3837823915946501e-16 1.4210854715202004e-14 ;
	setAttr ".ro" 1;
createNode joint -n "togue5_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt|togue3_jnt|togue4_posJnt|togue4_jnt";
	rename -uid "EA3206F5-4E7A-8A1F-04DB-DAA215DC671E";
	setAttr ".t" -type "double3" 5.9164567891575885e-31 0.19621138274669647 0.0055758477807668072 ;
	setAttr ".ro" 1;
createNode parentConstraint -n "togue4_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt|togue3_jnt|togue4_posJnt|togue4_jnt";
	rename -uid "AC1009BC-4F32-609A-4136-9EB61EB38D76";
	addAttr -ci true -k true -sn "w0" -ln "togue4Gmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" -1 1.2246467991473535e-16 6.1629758220391547e-33 0
		 -1.2801044796052369e-17 -0.10452846326765362 0.99452189536827318 0 1.2179380558447149e-16 0.99452189536827318 0.10452846326765362 0
		 -2.7940832648520404e-18 13.622068210547125 1.3187505800081489 1;
	setAttr ".tg[0].tt" -type "double3" 1.9721522630525295e-31 1.9428902930940239e-16 
		1.7763568394002505e-15 ;
	setAttr ".tg[0].trp" -type "double3" 9.7745183310832299e-16 4.163336342344337e-16 
		5.3290705182007514e-15 ;
	setAttr ".tg[0].tro" 1;
	setAttr ".cpim" -type "matrix" -1 -1.2801044796052352e-17 1.2179380558447149e-16 -0
		 1.2246467991473535e-16 -0.10452846326765351 0.99452189536827318 0 9.2444637330587294e-33 0.99452189536827318 0.10452846326765351 -0
		 -2.6484681395545194e-15 0.11236753022793186 -13.685292067150801 1;
	setAttr ".rst" -type "double3" 1.1832913578315177e-30 6.3837823915946501e-16 1.2434497875801753e-14 ;
	setAttr ".cro" 1;
	setAttr ".cjo" -type "double3" 2.3854160110976384e-15 0 8.8278125961003172e-32 ;
	setAttr -k on ".w0";
createNode pointConstraint -n "togue4_posJnt_pointConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt|togue3_jnt|togue4_posJnt";
	rename -uid "44CFA18F-44C2-E308-1D6C-2C8E5CD632EE";
	addAttr -ci true -k true -sn "w0" -ln "togue4Gmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tt" -type "double3" 1.9721522630525295e-31 1.9428902930940239e-16 
		1.7763568394002505e-15 ;
	setAttr ".tg[0].trp" -type "double3" 9.7745183310832299e-16 4.163336342344337e-16 
		5.3290705182007514e-15 ;
	setAttr ".tg[0].tpm" -type "matrix" -1 1.2246467991473535e-16 6.1629758220391547e-33 0
		 -1.2801044796052369e-17 -0.10452846326765362 0.99452189536827318 0 1.2179380558447149e-16 0.99452189536827318 0.10452846326765362 0
		 -2.7940832648520404e-18 13.622068210547125 1.3187505800081489 1;
	setAttr ".cpim" -type "matrix" -1 -2.7827801014133128e-32 1.2246467991473535e-16 -0
		 1.2246467991473535e-16 -2.7755575615628914e-16 1 0 9.3388735480156643e-33 1 2.7755575615628914e-16 -0
		 -2.648468139554519e-15 -1.0682352084470947 -13.679435488349077 1;
	setAttr ".o" -type "double3" -9.8607613152626476e-31 -1.7763568394002505e-15 -1.2434497875801753e-14 ;
	setAttr ".rst" -type "double3" 5.9164567891575885e-31 0.25051536925312523 -0.057367277801956718 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "togue3_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt|togue3_jnt";
	rename -uid "EBA900ED-4573-A347-5F37-48BCA38287B6";
	addAttr -ci true -k true -sn "w0" -ln "togue3Gmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" -1 1.2246467991473535e-16 6.1629758220391547e-33 0
		 -3.6977854932234928e-32 -3.3306690738754696e-16 0.99999999999999978 0 1.2246467991473532e-16 0.99999999999999978 3.3306690738754696e-16 0
		 4.2313820487448709e-18 13.67943548834908 1.0682352084470985 1;
	setAttr ".tg[0].trp" -type "double3" 9.774518331083224e-16 0 -3.5527136788005009e-15 ;
	setAttr ".tg[0].tro" 1;
	setAttr ".cpim" -type "matrix" -1 -2.7827801014133128e-32 1.2246467991473535e-16 -0
		 1.2246467991473535e-16 -2.7755575615628914e-16 1 0 9.3388735480156643e-33 1 2.7755575615628914e-16 -0
		 -2.6484681395545186e-15 -1.068235208447095 -13.679435488349077 1;
	setAttr ".rst" -type "double3" 7.8886090522101181e-31 -2.2204460492503131e-16 0 ;
	setAttr ".cro" 1;
	setAttr -k on ".w0";
createNode pointConstraint -n "togue3_posJnt_pointConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt";
	rename -uid "45BF8D2B-472D-BD52-53F2-3C9F4578AF21";
	addAttr -ci true -k true -sn "w0" -ln "togue3Gmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].trp" -type "double3" 9.774518331083224e-16 0 -3.5527136788005009e-15 ;
	setAttr ".tg[0].tpm" -type "matrix" -1 1.2246467991473535e-16 6.1629758220391547e-33 0
		 -3.6977854932234928e-32 -3.3306690738754696e-16 0.99999999999999978 0 1.2246467991473532e-16 0.99999999999999978 3.3306690738754696e-16 0
		 4.2313820487448709e-18 13.67943548834908 1.0682352084470985 1;
	setAttr ".cpim" -type "matrix" -1 -2.7827801014133128e-32 1.2246467991473535e-16 -0
		 1.2246467991473535e-16 -2.7755575615628914e-16 1 0 9.3388735480156643e-33 1 2.7755575615628914e-16 -0
		 -2.6484681395545182e-15 -0.8049138981653412 -13.755070334806978 1;
	setAttr ".o" -type "double3" -7.8886090522101181e-31 1.1102230246251565e-16 0 ;
	setAttr ".rst" -type "double3" 0 0.26332131729788144 -0.075634846457901972 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "togue2_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt";
	rename -uid "B8D5A024-4F3F-5D34-2559-40ACD6137612";
	addAttr -ci true -k true -sn "w0" -ln "togue2Gmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" -1 1.2246467991473535e-16 6.1629758220391547e-33 0
		 -3.6977854932234928e-32 -3.3306690738754696e-16 0.99999999999999978 0 1.2246467991473532e-16 0.99999999999999978 3.3306690738754696e-16 0
		 1.3493979310611997e-17 13.755070334806982 0.80491389816534498 1;
	setAttr ".tg[0].trp" -type "double3" 9.77451833108322e-16 0 -3.5527136788005009e-15 ;
	setAttr ".tg[0].tro" 1;
	setAttr ".cpim" -type "matrix" -1 -2.7827801014133128e-32 1.2246467991473535e-16 -0
		 1.2246467991473535e-16 -2.7755575615628914e-16 1 0 9.3388735480156643e-33 1 2.7755575615628914e-16 -0
		 -2.648468139554519e-15 -0.80491389816533798 -13.755070334806975 1;
	setAttr ".rst" -type "double3" -3.9443045261050599e-31 3.219646771412954e-15 3.5527136788005009e-15 ;
	setAttr ".cro" 1;
	setAttr -k on ".w0";
createNode pointConstraint -n "togue2_posJnt_pointConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt";
	rename -uid "0AF8FC46-44E6-C1A7-E8F3-949DAC549362";
	addAttr -ci true -k true -sn "w0" -ln "togue2Gmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].trp" -type "double3" 9.77451833108322e-16 0 -3.5527136788005009e-15 ;
	setAttr ".tg[0].tpm" -type "matrix" -1 1.2246467991473535e-16 6.1629758220391547e-33 0
		 -3.6977854932234928e-32 -3.3306690738754696e-16 0.99999999999999978 0 1.2246467991473532e-16 0.99999999999999978 3.3306690738754696e-16 0
		 1.3493979310611997e-17 13.755070334806982 0.80491389816534498 1;
	setAttr ".cpim" -type "matrix" -1 1.0254080668827485e-16 6.6955065456890039e-17 -0
		 1.2246467991473532e-16 0.83730922874797642 0.54672959994266712 -0 1.2325951644078309e-32 0.54672959994266712 -0.83730922874797642 -0
		 -2.6484681395545194e-15 -11.78286952893448 -6.9119693437464731 1;
	setAttr ".o" -type "double3" 1.9721522630525295e-31 -5.3290705182007514e-15 8.8817841970012523e-16 ;
	setAttr ".rst" -type "double3" -9.8607613152626476e-31 0.17444806474709829 -0.065627077695556757 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "togue1_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt";
	rename -uid "17C605DC-45C7-09B8-1CB3-5AB345450E71";
	addAttr -ci true -k true -sn "w0" -ln "togue1Gmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" -1 1.2246467991473535e-16 6.1629758220391547e-33 0
		 1.0254080668827485e-16 0.83730922874797642 0.54672959994266712 0 6.6955065456890027e-17 0.54672959994266712 -0.83730922874797642 0
		 1.9721522630525295e-31 13.644883631832657 0.65458782338897903 1;
	setAttr ".tg[0].tt" -type "double3" -1.9721522630525295e-31 0 0 ;
	setAttr ".tg[0].trp" -type "double3" 9.774518331083224e-16 -5.3290705182007514e-15 
		-8.8817841970012523e-16 ;
	setAttr ".tg[0].tro" 1;
	setAttr ".cpim" -type "matrix" -1 1.0254080668827485e-16 6.6955065456890039e-17 -0
		 1.2246467991473532e-16 0.83730922874797642 0.54672959994266712 -0 1.2325951644078309e-32 0.54672959994266712 -0.83730922874797642 -0
		 -1.6710163064461976e-15 -11.782869528934489 -6.9119693437464722 1;
	setAttr ".rst" -type "double3" 9.774518331083222e-16 -7.1054273576010019e-15 0 ;
	setAttr ".cro" 1;
	setAttr -k on ".w0";
createNode pointConstraint -n "togue1_posJnt_pointConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt";
	rename -uid "3DCC9744-49AF-6D9F-A8EE-EF962073ABD4";
	addAttr -ci true -k true -sn "w0" -ln "togue1Gmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tt" -type "double3" -1.9721522630525295e-31 0 0 ;
	setAttr ".tg[0].trp" -type "double3" 9.774518331083224e-16 -5.3290705182007514e-15 
		-8.8817841970012523e-16 ;
	setAttr ".tg[0].tpm" -type "matrix" -1 1.2246467991473535e-16 6.1629758220391547e-33 0
		 1.0254080668827485e-16 0.83730922874797642 0.54672959994266712 0 6.6955065456890027e-17 0.54672959994266712 -0.83730922874797642 0
		 1.9721522630525295e-31 13.644883631832657 0.65458782338897903 1;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -13.821383220708407 -0.23575776277947008 1;
	setAttr ".o" -type "double3" 9.7745183310832299e-16 5.3290705182007514e-15 2.2204460492503131e-15 ;
	setAttr ".rst" -type "double3" 3.9443045261050599e-31 13.644883627051167 0.65458782338897914 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "jaw1LWR_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt";
	rename -uid "905457A4-45D0-64FD-FAE4-829F30DC084F";
	addAttr -ci true -k true -sn "w0" -ln "jaw1GmblLWR_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 13.821383220708407 0.23575776277947008 1;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -13.29968568314475 -0.17524534052751309 1;
	setAttr ".rst" -type "double3" 0 0.52169753756365722 0.060512422251956993 ;
	setAttr ".cro" 5;
	setAttr -k on ".w0";
createNode parentConstraint -n "neck1_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt";
	rename -uid "83BB6377-4867-677B-18CF-369645B8460E";
	addAttr -ci true -k true -sn "w0" -ln "neckGmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 12.703025898336158 0.11917751060196094 1;
	setAttr ".tg[0].trp" -type "double3" 0 5.2599489208660088e-09 0 ;
	setAttr ".tg[0].tro" 2;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -12.703025898814616 -0.11917751060196105 1;
	setAttr ".rst" -type "double3" 0 12.703025898814616 0.11917751060196105 ;
	setAttr ".cro" 2;
	setAttr -k on ".w0";
createNode parentConstraint -n "chest2_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt";
	rename -uid "D8A5D3BE-4101-393E-51E3-5F9DCFB758E6";
	addAttr -ci true -k true -sn "w0" -ln "chest2Fk_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "chest2Ik_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 2.8349688854848509e-31 11.650614605082009 0.31646180752043873 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" 0 1.0524113178253174 -0.19728429691847735 ;
	setAttr ".tg[0].tro" 2;
	setAttr ".tg[1].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 11.650614580989298 0.3164618075204384 1;
	setAttr ".tg[1].tt" -type "double3" 0 1.0524113178253174 -0.19728429691847735 ;
	setAttr ".tg[1].tro" 2;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -11.650614580989298 -0.3164618075204384 1;
	setAttr ".rst" -type "double3" 0 1.0524113125653685 -0.19728429691847735 ;
	setAttr ".cro" 2;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode parentConstraint -n "chest1_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt";
	rename -uid "5BEB89C5-446E-1EB8-04B2-BD9CA9975A59";
	addAttr -ci true -k true -sn "w0" -ln "chest1Fk_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "chest1Ik_jnt2W1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 2.8349688854848509e-31 11.650614605082007 0.31646180752043868 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" 0 1.7763568394002505e-15 5.5511151231257827e-17 ;
	setAttr ".tg[0].tro" 2;
	setAttr ".tg[1].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 11.650614580989298 0.31646180752043812 1;
	setAttr ".tg[1].tt" -type "double3" 0 0 2.7755575615628914e-16 ;
	setAttr ".tg[1].tro" 2;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -2.8349688854848509e-31 -11.650614605082007 -0.31646180752043868 1;
	setAttr ".rst" -type "double3" 0 11.650614580989298 0.3164618075204384 ;
	setAttr ".cro" 2;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode joint -n "clav1LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt";
	rename -uid "F7F7C85C-4512-915D-D65D-D3901A36C946";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.42673607897087618 0.46176836947943656 0.21883541452000377 ;
	setAttr ".ro" 4;
	setAttr ".jo" -type "double3" 0 0 -89.999999999999986 ;
	setAttr ".liw" yes;
createNode joint -n "clav2LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt";
	rename -uid "753DC952-4B91-D0F5-5B4F-2EB21DEAD8D9";
	setAttr ".t" -type "double3" -0.093664200361738281 0.57385057210922241 -0.56456175931568509 ;
	setAttr ".ro" 4;
createNode joint -n "upArmLFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt";
	rename -uid "1A767CFD-45E4-BF1D-AC21-73BE62067D53";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -7.4587254150770832e-09 0 ;
	setAttr ".ro" 4;
	setAttr ".liw" yes;
createNode joint -n "foreArmLFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt";
	rename -uid "26E751EE-42F5-739E-0A15-55AE80537FFF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 2.1908121109008771 -0.046354234266098082 ;
	setAttr ".ro" 1;
	setAttr ".liw" yes;
createNode joint -n "wristLFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt";
	rename -uid "87AADBD0-484A-1BBE-4A7C-44816367D59A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 8.8817841970012523e-15 2.3390679359436035 0.046354234266098082 ;
	setAttr ".ro" 4;
	setAttr ".liw" yes;
createNode joint -n "handLFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt";
	rename -uid "09EB0420-47A0-3A79-B785-EEAC51D7328C";
	setAttr ".t" -type "double3" -0.072538943962255686 0.93553352355957031 0.035088956983990907 ;
	setAttr ".ro" 4;
createNode joint -n "fingerLFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|handLFT_jnt";
	rename -uid "F68D0384-41D4-6F22-90EF-D6A10965CB2F";
	setAttr ".t" -type "double3" 5.3290705182007514e-15 1.2053323552331783 -0.0020237637465749386 ;
	setAttr ".ro" 4;
createNode parentConstraint -n "fingerLFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|handLFT_jnt|fingerLFT_jnt";
	rename -uid "AF44F206-425A-C97E-3DB7-E89B9DD742F5";
	addAttr -ci true -k true -sn "w0" -ln "fingerFkLFT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "fingerIkLFT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 6.4660002140254242 12.27858609479272 0.0058244197087479882 1;
	setAttr ".tg[0].tt" -type "double3" 5.3290705182007514e-15 1.2053323552331783 -0.0020237637465749386 ;
	setAttr ".tg[0].tro" 4;
	setAttr ".tg[1].tpm" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 6.4660002049490037 12.278586094792724 0.0058244197087479882 1;
	setAttr ".tg[1].tw" 0;
	setAttr ".tg[1].tt" -type "double3" 5.3290705182007514e-15 1.2053323552331783 -0.0020237637465749386 ;
	setAttr ".tg[1].tro" 4;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 0 -0 -1 2.2204460492503131e-16 -0 0
		 0 -0 1 -0 12.278586094792718 -6.4660002140254269 -0.0058244197087479882 1;
	setAttr ".rst" -type "double3" 5.3290705182007514e-15 1.2053323552331783 -0.0020237637465749386 ;
	setAttr ".cro" 4;
	setAttr -k on ".w0";
	setAttr -k on ".w1" 0;
createNode parentConstraint -n "handLFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|handLFT_jnt";
	rename -uid "BA7FC889-46D3-2251-CC8E-F386A657D427";
	addAttr -ci true -k true -sn "w0" -ln "handFkLFT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "handIkLFT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 5.5304666904658539 12.206047150830466 -0.029264537275242919 1;
	setAttr ".tg[0].tt" -type "double3" -0.07253894396225391 0.93553352355957031 0.035088956983990907 ;
	setAttr ".tg[0].tro" 4;
	setAttr ".tg[1].tpm" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 5.5304666593741532 12.20604715083047 -0.029264537275242919 1;
	setAttr ".tg[1].tw" 0;
	setAttr ".tg[1].tt" -type "double3" -0.07253894396225391 0.93553354557485058 0.035088956983990907 ;
	setAttr ".tg[1].tro" 4;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -0 -0 -1 2.2204460492503131e-16 -0 0
		 0 -0 1 -0 12.206047150830463 -5.5304666904658566 0.029264537275242919 1;
	setAttr ".rst" -type "double3" -0.072538943962255686 0.93553354557485058 0.035088956983990907 ;
	setAttr ".cro" 4;
	setAttr -k on ".w0";
	setAttr -k on ".w1" 0;
createNode parentConstraint -n "wristLFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt";
	rename -uid "CE051D54-4D00-ABCF-B599-09A6879334FA";
	addAttr -ci true -k true -sn "w0" -ln "wristFkLFT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "wristIkLFT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 3.1913987545222504 12.206047150830473 -0.075618771541341001 1;
	setAttr ".tg[0].tt" -type "double3" 7.1054273576010019e-15 2.3390679359436035 0.046354234266098082 ;
	setAttr ".tg[0].tro" 4;
	setAttr ".tg[1].tpm" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 3.1913986620695418 12.206047150830475 -0.075618771541341001 1;
	setAttr ".tg[1].tw" 0;
	setAttr ".tg[1].tt" -type "double3" 5.3290705182007514e-15 2.3390679973046113 0.046354234266098082 ;
	setAttr ".tg[1].tro" 4;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -0 -0 -1 2.2204460492503131e-16 -0 0
		 0 -0 1 -0 12.206047150830473 -3.1913987545222531 0.075618771541341001 1;
	setAttr ".rst" -type "double3" 8.8817841970012523e-15 2.3390679973046113 0.046354234266098082 ;
	setAttr ".cro" 4;
	setAttr -k on ".w0";
	setAttr -k on ".w1" 0;
createNode joint -n "thumb1LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt";
	rename -uid "C6EF2A41-431A-5D53-FBDE-4C8DDC694B1D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.023405835025812394 0.38085815771172804 0.26843433695594543 ;
	setAttr ".r" -type "double3" 3.180554681463516e-15 -6.361109362927032e-15 -5.5659706925611528e-15 ;
	setAttr ".jot" -type "string" "yzx";
	setAttr ".jo" -type "double3" 38.305304903148055 0 -59.800036700100236 ;
	setAttr ".pa" -type "double3" 3.180554681463516e-15 -6.361109362927032e-15 -5.5659706925611528e-15 ;
	setAttr ".liw" yes;
createNode joint -n "thumb2LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|thumb1LFT_jnt";
	rename -uid "2648F234-4EA0-53B9-7FDC-EBAC40B50993";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.29708179823689029 4.4408920985006262e-15 ;
	setAttr ".r" -type "double3" -2.3854160110976459e-15 -3.6390018210963456e-14 2.3642951401660448e-14 ;
	setAttr ".jot" -type "string" "yzx";
	setAttr ".jo" -type "double3" 3.9222162624624954 16.846993414471473 22.541639129661721 ;
	setAttr ".pa" -type "double3" -2.3854160110976459e-15 -3.6390018210963456e-14 2.3642951401660448e-14 ;
	setAttr ".liw" yes;
createNode joint -n "thumb3LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|thumb1LFT_jnt|thumb2LFT_jnt";
	rename -uid "9060D233-4E71-2A01-9064-7FB6473E28BE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 7.1054273576010019e-15 0.42452939619509172 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" -6.5350459470695714e-15 2.248751552128502e-14 -4.7335598970218761e-15 ;
	setAttr ".jot" -type "string" "yzx";
	setAttr ".jo" -type "double3" 2.7453073834016002 3.7890408502403146 4.7169620290734624 ;
	setAttr ".pa" -type "double3" -6.5350459470695714e-15 2.248751552128502e-14 -4.7335598970218761e-15 ;
	setAttr ".liw" yes;
createNode joint -n "thumb4LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|thumb1LFT_jnt|thumb2LFT_jnt|thumb3LFT_jnt";
	rename -uid "11F25080-4713-9322-BC1C-F1BF0A1D241B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.5527136788005009e-15 0.27819097991060882 -7.4940054162198066e-16 ;
	setAttr ".jo" -type "double3" -3.4113763043823431 2.9896944288778768 3.3919257830217044 ;
	setAttr ".liw" yes;
createNode parentConstraint -n "thumb3LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|thumb1LFT_jnt|thumb2LFT_jnt|thumb3LFT_jnt";
	rename -uid "8B0F64D6-4ED9-2036-9DFB-80961FBB8C4E";
	addAttr -ci true -k true -sn "w0" -ln "thumb3LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" -0.43643214819229481 -0.89973717274783005 2.4286128663675299e-16 0
		 0.67453032801621893 -0.3271919055849431 0.66178115227469725 0 -0.59542910292543738 0.2888225699204186 0.7496970764875468 0
		 6.3092179170449221 11.806318166273 0.68959755321960747 1;
	setAttr ".tg[0].tt" -type "double3" 1.7763568394002505e-15 0 -1.3877787807814459e-15 ;
	setAttr ".tg[0].trp" -type "double3" -1.7763568394002509e-15 -6.6613381477509392e-16 
		3.8857805861880479e-16 ;
	setAttr ".tg[0].ts" -type "double3" 1.0000000000000002 1 1 ;
	setAttr ".cpim" -type "matrix" -0.52879555439278803 0.66102915343608426 -0.53236803055968485 0
		 -0.84874923366941779 -0.41184046334842062 0.33168082714340652 0 -6.106226635438361e-16 0.62723830483838594 0.77882739354973141 -0
		 13.356889882310222 0.68372996138169473 -1.0941809230108703 1;
	setAttr ".lr" -type "double3" -6.5350459470695714e-15 2.248751552128502e-14 -4.7335598970218761e-15 ;
	setAttr ".rst" -type "double3" 5.3290705182007514e-15 0.42452939619508789 3.9968028886505635e-15 ;
	setAttr ".cjo" -type "double3" 2.7453073834016002 3.7890408502403146 4.7169620290734624 ;
	setAttr ".rsrr" -type "double3" -8.075627120903467e-15 2.8550447882824851e-14 -2.2450243396111624e-14 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "thumb2LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|thumb1LFT_jnt|thumb2LFT_jnt";
	rename -uid "3FA0850A-4765-D2DA-6A7C-179CAD3B2916";
	addAttr -ci true -k true -sn "w0" -ln "thumb2LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" -0.52879555439278803 -0.84874923366941757 2.2204460492503131e-16 0
		 0.6610291534360847 -0.41184046334842034 0.62723830483838594 0 -0.53236803055968474 0.33168082714340708 0.77882739354973163 0
		 6.0285916096693537 11.981156549507009 0.42331645439614096 1;
	setAttr ".tg[0].tt" -type "double3" -7.8886090522101181e-31 0 -1.4432899320127035e-15 ;
	setAttr ".tg[0].trp" -type "double3" -5.3290705182007498e-15 -8.1046280797636427e-15 
		-2.442490654175344e-15 ;
	setAttr ".tg[0].ts" -type "double3" 0.99999999999999989 1 0.99999999999999989 ;
	setAttr ".cpim" -type "matrix" -0.86427512415670893 0.39472886655366296 -0.31179742088921458 -0
		 -0.50301939302963805 -0.67821309650541273 0.53572239636273788 0 -5.5511151231257827e-17 0.61985169003383422 0.78471898305202215 -0
		 11.237115857263166 5.7808065287178714 -4.8710590400126641 1;
	setAttr ".lr" -type "double3" -2.3854160110976459e-15 -3.6390018210963456e-14 2.3642951401660448e-14 ;
	setAttr ".rst" -type "double3" 0 0.29708179823689651 5.3290705182007514e-15 ;
	setAttr ".cjo" -type "double3" 3.9222162624624946 16.846993414471481 22.541639129661721 ;
	setAttr ".rsrr" -type "double3" 6.3611093629270304e-15 -3.7719390675481403e-14 1.5082786653502775e-14 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "thumb1LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|thumb1LFT_jnt";
	rename -uid "C770C481-4FF0-7336-79C2-AC9C8F2FFC38";
	addAttr -ci true -k true -sn "w0" -ln "thumb1LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" -0.86427512415670893 -0.50301939302963805 0 0
		 0.39472886655366296 -0.67821309650541273 0.61985169003383411 0 -0.31179742088921464 0.53572239636273777 0.78471898305202203 0
		 5.9113248481775722 12.182641315804654 0.23916979968070651 1;
	setAttr ".tg[0].tt" -type "double3" -1.7763568394002501e-15 0 -9.9920072216264286e-16 ;
	setAttr ".tg[0].trp" -type "double3" -5.329070518200753e-15 2.6645352591003765e-15 
		-6.2172489379008782e-15 ;
	setAttr ".tg[0].ts" -type "double3" 1.0000000000000002 1.0000000000000002 1.0000000000000002 ;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -0 -0 -1 2.2204460492503131e-16 -0 0
		 0 -0 1 -0 12.206047150830463 -5.5304666904658566 0.029264537275242919 1;
	setAttr ".lr" -type "double3" 3.180554681463516e-15 -6.361109362927032e-15 -5.5659706925611528e-15 ;
	setAttr ".rst" -type "double3" 5.9113248170858794 12.182641311023165 0.2391697996807034 ;
	setAttr ".cjo" -type "double3" 38.305304903148048 0 -59.800036700100208 ;
	setAttr -k on ".w0";
createNode joint -n "index1LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt";
	rename -uid "9ED3AE29-4484-9D80-6C03-AC8D03C955CA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.057687815221989069 0.61843004023539372 0.35056708371341505 ;
	setAttr ".jo" -type "double3" 0 -89.999999999999986 0 ;
	setAttr ".pa" -type "double3" 1.5873196225042571e-15 -6.1598805786420021e-15 -28.899998868995507 ;
createNode joint -n "index2LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt";
	rename -uid "C5D9FA05-4935-8D5E-6B66-D788083D6508";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.0091721192326905676 0.31508620283845268 5.3290705182007514e-15 ;
createNode joint -n "index3LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt|index2LFT_jnt";
	rename -uid "659CB34C-44DB-7019-3331-B99F66DDB778";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.0020237637465747582 0.53396337437749164 0 ;
	setAttr ".liw" yes;
createNode joint -n "index4LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt|index2LFT_jnt|index3LFT_jnt";
	rename -uid "235BA949-443D-AE30-3FAF-29B77DB321CE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.5511151231257827e-17 0.32331645835725453 -1.7763568394002505e-15 ;
	setAttr ".liw" yes;
createNode joint -n "index5LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt|index2LFT_jnt|index3LFT_jnt|index4LFT_jnt";
	rename -uid "DB5763D3-4576-9A7C-8FF1-2D98CE1FC283";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1274836189554914e-17 0.27066586699912243 5.2908199612991185e-17 ;
	setAttr ".liw" yes;
createNode parentConstraint -n "index4LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt|index2LFT_jnt|index3LFT_jnt|index4LFT_jnt";
	rename -uid "7CBA59BB-4002-F374-E7F6-018E089E5306";
	addAttr -ci true -k true -sn "w0" -ln "index4LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 7.3212627662744474 12.263734966052457 0.32845090192428789 1;
	setAttr ".tg[0].tt" -type "double3" 0 -8.8817841970012523e-16 0 ;
	setAttr ".cpim" -type "matrix" 4.9303806576313238e-32 1 -2.2204460492503131e-16 -0
		 -2.2204460492503131e-16 2.2204460492503131e-16 1 0 1 -0 2.2204460492503131e-16 -0
		 -0.32845090192428522 -6.9979463079171946 -12.263734966052457 1;
	setAttr ".rst" -type "double3" 0 0.32331645835725542 -1.7763568394002505e-15 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "index3LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt|index2LFT_jnt|index3LFT_jnt";
	rename -uid "D043F16A-4F1E-622C-6F98-2294083FA4B4";
	addAttr -ci true -k true -sn "w0" -ln "index3LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 6.997946307917192 12.263734966052459 0.32845090192428794 1;
	setAttr ".cpim" -type "matrix" 4.9303806576313238e-32 1 -2.2204460492503131e-16 -0
		 -2.2204460492503131e-16 2.2204460492503131e-16 1 0 1 -0 2.2204460492503131e-16 -0
		 -0.33047466567085998 -6.463982933539703 -12.263734966052457 1;
	setAttr ".rst" -type "double3" -0.0020237637465746472 0.53396337437749164 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "index2LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt|index2LFT_jnt";
	rename -uid "17080F50-4001-1A95-108D-2FB65CA94FDD";
	addAttr -ci true -k true -sn "w0" -ln "index2LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 6.4639829335397003 12.263734966052457 0.33047466567086276 1;
	setAttr ".tg[0].tt" -type "double3" -5.5511151231257827e-17 0 1.7763568394002505e-15 ;
	setAttr ".cpim" -type "matrix" 4.9303806576313238e-32 1 -2.2204460492503131e-16 -0
		 -2.2204460492503131e-16 2.2204460492503131e-16 1 0 1 -0 2.2204460492503131e-16 -0
		 -0.32130254643816941 -6.1488967307012503 -12.263734966052452 1;
	setAttr ".rst" -type "double3" 0.0091721192326907341 0.31508622485373738 3.5527136788005009e-15 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "index1LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt";
	rename -uid "4DEB103F-4D2B-E5C7-DB65-71BF91CCCC8F";
	addAttr -ci true -k true -sn "w0" -ln "index1LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 6.1488967307012485 12.263734966052455 0.32130254643817213 1;
	setAttr ".tg[0].tt" -type "double3" 0 -8.8817841970012523e-16 -1.7763568394002505e-15 ;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -0 -0 -1 2.2204460492503131e-16 -0 0
		 0 -0 1 -0 12.206047150830463 -5.5304666904658566 0.029264537275242919 1;
	setAttr ".rst" -type "double3" 6.1488966996095478 12.263734961270968 0.32130254643817224 ;
	setAttr ".cjo" -type "double3" 0 -89.999999999999986 0 ;
	setAttr -k on ".w0";
createNode joint -n "middle1LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt";
	rename -uid "65653421-4FC8-49E3-CA66-01AF8D6227EF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.075603433471071568 0.62686004056767075 0.14268926300735918 ;
	setAttr ".jo" -type "double3" 0 -89.999999999999986 0 ;
	setAttr ".liw" yes;
createNode joint -n "middle2LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt";
	rename -uid "01211392-4D13-26C6-3BEC-1C943298E7BF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.0025744490853067264 0.32183020310427413 8.8817841970012523e-15 ;
	setAttr ".liw" yes;
createNode joint -n "middle3LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt|middle2LFT_jnt";
	rename -uid "8F5798B7-4569-C81A-569C-FD8819BB3809";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.0020237637465748137 0.58454337637115383 -3.5527136788005009e-15 ;
	setAttr ".liw" yes;
createNode joint -n "middle4LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt|middle2LFT_jnt|middle3LFT_jnt";
	rename -uid "53D50FE1-4A2E-35EE-026A-608566F36D19";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1102230246251565e-16 0.35334743389993051 -3.5527136788005009e-15 ;
	setAttr ".liw" yes;
createNode joint -n "middle5LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt|middle2LFT_jnt|middle3LFT_jnt|middle4LFT_jnt";
	rename -uid "C5AEA081-4FF3-FC29-237F-F69E5E5CB631";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.2576918032924176e-17 0.30822860352243175 -1.7317892427784803e-15 ;
	setAttr ".liw" yes;
createNode parentConstraint -n "middle4LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt|middle2LFT_jnt|middle3LFT_jnt|middle4LFT_jnt";
	rename -uid "F97C0156-43E5-BEEB-8B65-089CFA1353AC";
	addAttr -ci true -k true -sn "w0" -ln "middle4LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 7.417047744408884 12.28165058430154 0.10882651290023461 1;
	setAttr ".tg[0].tt" -type "double3" 0 -8.8817841970012523e-16 -1.7763568394002505e-15 ;
	setAttr ".cpim" -type "matrix" 4.9303806576313238e-32 1 -2.2204460492503131e-16 -0
		 -2.2204460492503131e-16 2.2204460492503131e-16 1 0 1 -0 2.2204460492503131e-16 -0
		 -0.10882651290023199 -7.0637003105089553 -12.28165058430154 1;
	setAttr ".rst" -type "double3" -2.7755575615628914e-17 0.3533474338999314 -1.7763568394002505e-15 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "middle3LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt|middle2LFT_jnt|middle3LFT_jnt";
	rename -uid "F23468B6-485F-A553-10A6-66998116D11D";
	addAttr -ci true -k true -sn "w0" -ln "middle3LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 7.0637003105089535 12.281650584301543 0.10882651290023472 1;
	setAttr ".tg[0].tt" -type "double3" 0 -8.8817841970012523e-16 -1.7763568394002505e-15 ;
	setAttr ".cpim" -type "matrix" 4.9303806576313238e-32 1 -2.2204460492503131e-16 -0
		 -2.2204460492503131e-16 2.2204460492503131e-16 1 0 1 -0 2.2204460492503131e-16 -0
		 -0.1108502766468068 -6.4791569341378015 -12.281650584301543 1;
	setAttr ".rst" -type "double3" -0.0020237637465746888 0.58454337637115472 -1.7763568394002505e-15 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "middle2LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt|middle2LFT_jnt";
	rename -uid "B33D82BA-447B-FB8C-4119-BC846AE03D62";
	addAttr -ci true -k true -sn "w0" -ln "middle2LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 6.4791569341377997 12.281650584301545 0.11085027664680955 1;
	setAttr ".tg[0].tt" -type "double3" -1.3877787807814457e-17 -8.8817841970012523e-16 
		0 ;
	setAttr ".cpim" -type "matrix" 4.9303806576313238e-32 1 -2.2204460492503131e-16 -0
		 -2.2204460492503131e-16 2.2204460492503131e-16 1 0 1 -0 2.2204460492503131e-16 -0
		 -0.11342472573211353 -6.1573267310335273 -12.281650584301534 1;
	setAttr ".rst" -type "double3" -0.0025744490853065599 0.32183022511955972 8.8817841970012523e-15 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "middle1LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt";
	rename -uid "CC3D62B9-42BF-5776-E5F3-2C9E6354ED88";
	addAttr -ci true -k true -sn "w0" -ln "middle1LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 6.1573267310335256 12.281650584301538 0.11342472573211626 1;
	setAttr ".tg[0].tt" -type "double3" -1.3877787807814457e-17 -8.8817841970012523e-16 
		-1.7763568394002505e-15 ;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -0 -0 -1 2.2204460492503131e-16 -0 0
		 0 -0 1 -0 12.206047150830463 -5.5304666904658566 0.029264537275242919 1;
	setAttr ".rst" -type "double3" 6.1573266999418248 12.281650579520051 0.11342472573211637 ;
	setAttr ".cjo" -type "double3" 0 -89.999999999999986 0 ;
	setAttr -k on ".w0";
createNode joint -n "ring1LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt";
	rename -uid "82EC3C3D-4D92-5A97-8D46-5CA3A6771247";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.069474464016401782 0.61223121720998908 -0.052733627511084458 ;
	setAttr ".jo" -type "double3" 0 -89.999999999999986 0 ;
	setAttr ".pa" -type "double3" -1.5873196225042571e-15 -6.1598805786420021e-15 28.899998868995507 ;
	setAttr ".liw" yes;
createNode joint -n "ring2LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt";
	rename -uid "9D0E131C-41A5-4484-343A-FBBDA9404EE6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.017203272442986459 0.31014557131974207 8.8817841970012523e-15 ;
	setAttr ".liw" yes;
createNode joint -n "ring3LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt|ring2LFT_jnt";
	rename -uid "F0315A15-41CC-19A6-EDB5-3384A08A56AD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.0020237637465748137 0.52674109720670703 -3.5527136788005009e-15 ;
	setAttr ".liw" yes;
createNode joint -n "ring4LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt|ring2LFT_jnt|ring3LFT_jnt";
	rename -uid "CB76FB67-41A8-46D6-B3A0-6D86715E60CF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.5511151231257827e-17 0.34237581638167036 -1.7763568394002505e-15 ;
	setAttr ".liw" yes;
createNode joint -n "ring5LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt|ring2LFT_jnt|ring3LFT_jnt|ring4LFT_jnt";
	rename -uid "F09C73DA-47C1-B81E-BCFE-7293A358C893";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.1979122527192602e-17 0.2954283830844604 -3.5053038622886276e-15 ;
	setAttr ".liw" yes;
createNode parentConstraint -n "ring4LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt|ring2LFT_jnt|ring3LFT_jnt|ring4LFT_jnt";
	rename -uid "21FAA6C0-4475-14A2-E6E8-39A3203B6632";
	addAttr -ci true -k true -sn "w0" -ln "ring4LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 7.3219603925839634 12.275521614846872 -0.10122520097588872 1;
	setAttr ".tg[0].tt" -type "double3" 1.3877787807814457e-17 -8.8817841970012523e-16 
		-1.7763568394002505e-15 ;
	setAttr ".cpim" -type "matrix" 4.9303806576313238e-32 1 -2.2204460492503131e-16 -0
		 -2.2204460492503131e-16 2.2204460492503131e-16 1 0 1 -0 2.2204460492503131e-16 -0
		 0.10122520097589137 -6.9795845762022948 -12.27552161484687 1;
	setAttr ".rst" -type "double3" -1.3877787807814457e-17 0.34237581638167125 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ring3LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt|ring2LFT_jnt|ring3LFT_jnt";
	rename -uid "ED6B5663-41CC-54AF-5274-A7BFCBF69E6A";
	addAttr -ci true -k true -sn "w0" -ln "ring3LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 6.9795845762022921 12.275521614846873 -0.10122520097588866 1;
	setAttr ".tg[0].tt" -type "double3" 1.3877787807814457e-17 0 -1.7763568394002505e-15 ;
	setAttr ".cpim" -type "matrix" 4.9303806576313238e-32 1 -2.2204460492503131e-16 -0
		 -2.2204460492503131e-16 2.2204460492503131e-16 1 0 1 -0 2.2204460492503131e-16 -0
		 0.099201437229316555 -6.4528434789955877 -12.275521614846873 1;
	setAttr ".rst" -type "double3" -0.0020237637465747027 0.52674109720670703 -1.7763568394002505e-15 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ring2LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt|ring2LFT_jnt";
	rename -uid "7D519AE6-4F8A-E19E-B17D-45A52EFC0AA9";
	addAttr -ci true -k true -sn "w0" -ln "ring2LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 6.4528434789955851 12.275521614846875 -0.099201437229313835 1;
	setAttr ".cpim" -type "matrix" 4.9303806576313238e-32 1 -2.2204460492503131e-16 -0
		 -2.2204460492503131e-16 2.2204460492503131e-16 1 0 1 -0 2.2204460492503131e-16 -0
		 0.081998164786330097 -6.1426979076758457 -12.275521614846864 1;
	setAttr ".rst" -type "double3" -0.017203272442986445 0.31014559333502589 7.1054273576010019e-15 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ring1LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt";
	rename -uid "0BF9A088-4041-179F-840C-8D917F762A80";
	addAttr -ci true -k true -sn "w0" -ln "ring1LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 6.1426979076758439 12.275521614846868 -0.081998164786327377 1;
	setAttr ".tg[0].tt" -type "double3" 0 -8.8817841970012523e-16 -1.7763568394002505e-15 ;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -0 -0 -1 2.2204460492503131e-16 -0 0
		 0 -0 1 -0 12.206047150830463 -5.5304666904658566 0.029264537275242919 1;
	setAttr ".rst" -type "double3" 6.1426978765841431 12.275521610065381 -0.081998164786327266 ;
	setAttr ".cjo" -type "double3" 0 -89.999999999999986 0 ;
	setAttr -k on ".w0";
createNode joint -n "pinky1LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt";
	rename -uid "919C1F8E-49C8-5554-3CEB-7888A4B0F1CB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.044183288437254831 0.55764847820845098 -0.23907467431884621 ;
	setAttr ".jo" -type "double3" 0 -89.999999999999986 0 ;
	setAttr ".pa" -type "double3" 5.5689256602944837e-15 -3.0742119831318724e-15 57.799997737990992 ;
	setAttr ".liw" yes;
createNode joint -n "pinky2LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt";
	rename -uid "9090C094-42A7-A2DB-25F4-76B4DEC4915B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.031651644531628276 0.29577782462103919 0 ;
	setAttr ".pa" -type "double3" -36.16698559668329 0 19.999999418185844 ;
	setAttr ".liw" yes;
createNode joint -n "pinky3LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt|pinky2LFT_jnt";
	rename -uid "84F120F4-415D-BC43-4811-63BAD0E72CFB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.0020237637465749247 0.43033799122582028 -1.7763568394002505e-15 ;
	setAttr ".liw" yes;
createNode joint -n "pinky4LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt|pinky2LFT_jnt|pinky3LFT_jnt";
	rename -uid "FF40A4F2-465C-FD06-B978-F8B9D07AFF8C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1102230246251565e-16 0.29421457608619406 -1.7763568394002505e-15 ;
	setAttr ".liw" yes;
createNode joint -n "pinky5LFT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt|pinky2LFT_jnt|pinky3LFT_jnt|pinky4LFT_jnt";
	rename -uid "32D1A35A-42FF-0670-689D-C9AA351773AA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.5511151231257827e-17 0.24797537830994898 0 ;
	setAttr ".liw" yes;
createNode parentConstraint -n "pinky4LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt|pinky2LFT_jnt|pinky3LFT_jnt|pinky4LFT_jnt";
	rename -uid "2F7B457F-4759-8417-810D-6A8B6075D474";
	addAttr -ci true -k true -sn "w0" -ln "pinky4LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 7.1084455606073593 12.250230439267717 -0.30201461987229239 1;
	setAttr ".tg[0].tt" -type "double3" -5.5511151231257827e-17 -8.8817841970012523e-16 
		-1.7763568394002505e-15 ;
	setAttr ".cpim" -type "matrix" 4.9303806576313238e-32 1 -2.2204460492503131e-16 -0
		 -2.2204460492503131e-16 2.2204460492503131e-16 1 0 1 -0 2.2204460492503131e-16 -0
		 0.30201461987229505 -6.814230984521167 -12.250230439267716 1;
	setAttr ".rst" -type "double3" 5.5511151231257827e-17 0.29421457608619495 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "pinky3LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt|pinky2LFT_jnt|pinky3LFT_jnt";
	rename -uid "44DAF2D3-4E36-0555-E5DD-0BB282F3F411";
	addAttr -ci true -k true -sn "w0" -ln "pinky3LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 6.8142309845211644 12.250230439267719 -0.30201461987229228 1;
	setAttr ".tg[0].tt" -type "double3" -5.5511151231257827e-17 0 -1.7763568394002505e-15 ;
	setAttr ".cpim" -type "matrix" 4.9303806576313238e-32 1 -2.2204460492503131e-16 -0
		 -2.2204460492503131e-16 2.2204460492503131e-16 1 0 1 -0 2.2204460492503131e-16 -0
		 0.29999085612572013 -6.3838929932953468 -12.250230439267717 1;
	setAttr ".rst" -type "double3" -0.0020237637465748692 0.43033799122582028 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "pinky2LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt|pinky2LFT_jnt";
	rename -uid "CA8B0CA9-40BB-2898-C27C-2EBB84364B76";
	addAttr -ci true -k true -sn "w0" -ln "pinky2LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 6.3838929932953441 12.250230439267719 -0.29999085612571741 1;
	setAttr ".cpim" -type "matrix" 4.9303806576313238e-32 1 -2.2204460492503131e-16 -0
		 -2.2204460492503131e-16 2.2204460492503131e-16 1 0 1 -0 2.2204460492503131e-16 -0
		 0.26833921159409185 -6.0881151686743076 -12.250230439267717 1;
	setAttr ".rst" -type "double3" -0.031651644531628054 0.29577784663632389 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "pinky1LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt";
	rename -uid "AF324582-41BE-11A4-CF0A-EE94F48AF9AF";
	addAttr -ci true -k true -sn "w0" -ln "pinky1LFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 4.9303806576313238e-32 -2.2204460492503131e-16 1 0
		 1 2.2204460492503131e-16 0 0 -2.2204460492503131e-16 1 2.2204460492503131e-16 0 6.0881151686743058 12.250230439267721 -0.26833921159408913 1;
	setAttr ".tg[0].tt" -type "double3" 0 -8.8817841970012523e-16 -1.7763568394002505e-15 ;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -0 -0 -1 2.2204460492503131e-16 -0 0
		 0 -0 1 -0 12.206047150830463 -5.5304666904658566 0.029264537275242919 1;
	setAttr ".rst" -type "double3" 6.088115137582605 12.250230434486234 -0.26833921159408902 ;
	setAttr ".cjo" -type "double3" 0 -89.999999999999986 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "foreArmLFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt";
	rename -uid "179B709E-49F0-FCE8-BD32-C58210E8A88B";
	addAttr -ci true -k true -sn "w0" -ln "foreArmFkLFT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "foreArmIkLFT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 1.0005866436213731 12.206047150830473 -0.029264537275242919 1;
	setAttr ".tg[0].tt" -type "double3" 0 2.1908121109008771 -0.046354234266098082 ;
	setAttr ".tg[0].tro" 1;
	setAttr ".tg[1].tpm" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 1.0005866510800983 12.206047150830475 -0.029264537275242922 1;
	setAttr ".tg[1].tw" 0;
	setAttr ".tg[1].tt" -type "double3" 0 2.1908120109894433 -0.046354234266098082 ;
	setAttr ".tg[1].tro" 1;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -0 -0 -1 2.2204460492503131e-16 -0 0
		 0 -0 1 -0 12.206047150830473 -1.0005866436213757 0.029264537275242919 1;
	setAttr ".rst" -type "double3" 0 2.1908120109894433 -0.046354234266098082 ;
	setAttr ".cro" 1;
	setAttr -k on ".w0";
	setAttr -k on ".w1" 0;
createNode parentConstraint -n "upArmLFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt";
	rename -uid "A3D2E223-445D-8953-5FF1-D0BB969213D4";
	addAttr -ci true -k true -sn "w0" -ln "upArmFkLFT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "upArmIkLFT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 1.0005866510800985 12.206047150830473 -0.029264537275242919 1;
	setAttr ".tg[0].tt" -type "double3" 0 -7.4587254150770832e-09 0 ;
	setAttr ".tg[0].tro" 4;
	setAttr ".tg[1].tpm" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 1.0005866510800985 12.206047150830473 -0.029264537275242919 1;
	setAttr ".tg[1].tw" 0;
	setAttr ".tg[1].tt" -type "double3" -1.7763568394002505e-15 -2.2204460492503131e-16 
		-3.4694469519536142e-18 ;
	setAttr ".tg[1].tr" -type "double3" -1.0044018900965677e-21 -8.5377366028501255e-07 
		-7.4833773568708632e-30 ;
	setAttr ".tg[1].tro" 4;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -0 -0 -1 2.2204460492503131e-16 -0 0
		 0 -0 1 -0 12.206047150830473 -1.0005866510801011 0.029264537275242919 1;
	setAttr ".rst" -type "double3" 1.0005866510800985 12.206047150830475 -0.029264537275242919 ;
	setAttr ".cro" 4;
	setAttr -k on ".w0";
	setAttr -k on ".w1" 0;
createNode parentConstraint -n "clav1LFT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt";
	rename -uid "C1B9466A-4865-9316-38D7-888F0B6CE8CB";
	addAttr -ci true -k true -sn "w0" -ln "clavGmblLFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 0.42673607897087606 12.112382950468737 0.53529722204044217 1;
	setAttr ".tg[0].trp" -type "double3" 1.7763568394002505e-15 1.1102230246251565e-16 
		0 ;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -11.650614580989298 -0.3164618075204384 1;
	setAttr ".rst" -type "double3" 0.42673607897087612 12.112382950468735 0.53529722204044217 ;
	setAttr ".cro" 4;
	setAttr ".cjo" -type "double3" 0 0 -89.999999999999986 ;
	setAttr -k on ".w0";
createNode joint -n "clav1RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt";
	rename -uid "E6AC9C3B-4C65-94C5-3077-3BBAF6438E9E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.42673607897087618 0.46176836947943656 0.21883541452000377 ;
	setAttr ".ro" 4;
	setAttr ".jo" -type "double3" -180 0 89.999999999999986 ;
	setAttr ".liw" yes;
createNode joint -n "clav2RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt";
	rename -uid "5C9A7AC4-4667-A055-4C6A-DA814493D1D5";
	setAttr ".t" -type "double3" 0.09360000000000035 -0.57385402917861938 0.56456150000000016 ;
	setAttr ".ro" 4;
createNode joint -n "upArmRGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt";
	rename -uid "11A5667D-4946-247E-E30F-93A61A0D24B4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".ro" 4;
	setAttr ".pa" -type "double3" 0 0.00054249237018912241 0 ;
	setAttr ".liw" yes;
createNode joint -n "foreArmRGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt";
	rename -uid "900F073A-4C27-6303-B8CD-BB9C8A954357";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.7054312754893886e-05 -2.1908098569841714 0.046354522040442478 ;
	setAttr ".ro" 1;
	setAttr ".liw" yes;
createNode joint -n "wristRGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt";
	rename -uid "B99D8CED-4B52-42B3-9591-EF8B371FACEF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -7.1054273576010019e-15 -2.3390700817108154 -0.046354299999999696 ;
	setAttr ".ro" 4;
	setAttr ".pa" -type "double3" 4.1788720079408509e-05 -0.00053849190040431327 -2.0197017474648806e-05 ;
	setAttr ".liw" yes;
createNode joint -n "handRGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt";
	rename -uid "1617D3B7-435E-7BF8-5771-2A9A56F622C6";
	setAttr ".t" -type "double3" 0.072600000000004883 -0.93553000688552856 -0.035088919999999892 ;
	setAttr ".ro" 4;
	setAttr ".pa" -type "double3" 0 -4.0343626906853266e-06 0 ;
createNode joint -n "fingerRGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|handRGT_jnt";
	rename -uid "4ECBC18D-4A77-25ED-ADA8-489585C0D867";
	setAttr ".t" -type "double3" 0 -1.20533 0.0020237600000001512 ;
	setAttr ".ro" 4;
createNode parentConstraint -n "fingerRGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|handRGT_jnt|fingerRGT_jnt";
	rename -uid "B57DF29F-4AC7-4A9B-7618-8481183CD8E9";
	addAttr -ci true -k true -sn "w0" -ln "fingerFkRGT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "fingerIkRGT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 2.2204460492503131e-16 1 0 0 1 -2.2204460492503131e-16 -1.2246467991473532e-16 0
		 -1.2246467991473532e-16 2.4651903288156619e-32 -1 0 -6.4660000537300109 12.278600004781488 0.0058244199999998927 1;
	setAttr ".tg[0].tt" -type "double3" 0 -1.20533 0.0020237600000001512 ;
	setAttr ".tg[0].tro" 4;
	setAttr ".tg[1].tpm" -type "matrix" -3.5251135990477241e-07 0.99999999999993783 -5.9154279867918723e-10 0
		 0.99999999999967193 3.525113594732384e-07 -7.293474180594758e-07 0 -7.2934741785090484e-07 -5.917999019294653e-10 -0.99999999999973399 0
		 -6.4660000000000002 12.278599672546587 0.0058251073954117757 1;
	setAttr ".tg[1].tw" 0;
	setAttr ".tg[1].tt" -type "double3" 0 -1.20533 0.0020237600000001512 ;
	setAttr ".tg[1].tro" 4;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -1.2246467991473532e-16 -0
		 1 -2.2204460492503131e-16 2.7192621468937821e-32 0 -2.5407181807812022e-33 -1.2246467991473532e-16 -1 0
		 -12.278600004781486 6.4660000537300135 0.0058244199999991008 1;
	setAttr ".rst" -type "double3" 0 -1.20533 0.0020237600000001512 ;
	setAttr ".cro" 4;
	setAttr -k on ".w0";
	setAttr -k on ".w1" 0;
createNode parentConstraint -n "handRGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|handRGT_jnt";
	rename -uid "EDEBD8A9-4965-8E0A-EB69-4B9D1D898EDC";
	addAttr -ci true -k true -sn "w0" -ln "handFkRGT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "handIkRGT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 2.2204460492503131e-16 1 0 0 1 -2.2204460492503131e-16 -1.2246467991473532e-16 0
		 -1.2246467991473532e-16 2.4651903288156619e-32 -1 0 -5.5304700468444823 12.206000004781485 -0.029264500000000117 1;
	setAttr ".tg[0].tt" -type "double3" 0.072600000000003106 -0.93553000688552856 -0.035088919999999892 ;
	setAttr ".tg[0].tro" 4;
	setAttr ".tg[1].tpm" -type "matrix" -3.5251130854929584e-07 0.99999999999993539 6.9821369373378544e-08 0
		 0.99999999999967193 3.525113594732384e-07 -7.293474180594758e-07 0 -7.2934744267225446e-07 6.9821112270142613e-08 -0.99999999999973155 0
		 -5.5304700000000002 12.206000004781488 -0.029264500000000124 1;
	setAttr ".tg[1].tw" 0;
	setAttr ".tg[1].tt" -type "double3" 0.072600000000003106 -0.93553 -0.035088919999999892 ;
	setAttr ".tg[1].tr" -type "double3" -2.3847908205401571e-16 -4.0343626906853266e-06 
		6.7737315414881746e-09 ;
	setAttr ".tg[1].tro" 4;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -1.2246467991473532e-16 -0
		 1 -2.2204460492503131e-16 2.7192621468937821e-32 0 -2.5407181807812022e-33 -1.2246467991473532e-16 -1 0
		 -12.206000004781481 5.530470046844485 -0.029264500000000793 1;
	setAttr ".rst" -type "double3" 0.072600000000004883 -0.93553 -0.035088919999999892 ;
	setAttr ".cro" 4;
	setAttr -k on ".w0";
	setAttr -k on ".w1" 0;
createNode parentConstraint -n "wristRGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt";
	rename -uid "797D12BD-4BD8-84E8-12A8-A281ECC34A28";
	addAttr -ci true -k true -sn "w0" -ln "wristFkRGT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "wristIkRGT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 2.2204460492503131e-16 1 0 0 1 -2.2204460492503131e-16 -1.2246467991473532e-16 0
		 -1.2246467991473532e-16 2.4651903288156619e-32 -1 0 -3.1913999651336669 12.20600000478149 -0.075618800000000097 1;
	setAttr ".tg[0].tt" -type "double3" -5.3290705182007514e-15 -2.3390700817108154 
		-0.046354299999999696 ;
	setAttr ".tg[0].tro" 4;
	setAttr ".tg[1].tpm" -type "matrix" 2.2204576444471623e-16 0.99999999995517586 9.4682780266066486e-06 0
		 1 -2.2204460492503131e-16 -1.2246467991473532e-16 0 -1.2246257752919222e-16 9.4682780266066486e-06 -0.99999999995517586 0
		 -3.1913999999999998 12.20600044367689 -0.075618799997922315 1;
	setAttr ".tg[1].tw" 0;
	setAttr ".tg[1].tt" -type "double3" -1.7763568394002505e-15 -2.3390700000000004 
		-0.046354299999999696 ;
	setAttr ".tg[1].tr" -type "double3" 4.1788720079408502e-05 -0.00053849190040431338 
		-2.0197017474648803e-05 ;
	setAttr ".tg[1].tro" 4;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -1.2246467991473532e-16 -0
		 1 -2.2204460492503131e-16 2.7192621468937821e-32 0 -2.5407181807812022e-33 -1.2246467991473532e-16 -1 0
		 -12.20600000478149 3.1913999651336695 -0.075618800000000486 1;
	setAttr ".rst" -type "double3" -3.5527136788005009e-15 -2.3390700000000004 -0.046354299999999696 ;
	setAttr ".cro" 4;
	setAttr -k on ".w0";
	setAttr -k on ".w1" 0;
createNode joint -n "thumb1RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt";
	rename -uid "E466188F-4927-E9BD-79DC-9DA308A191D3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.023399846988255035 -0.38085020405111969 -0.26843422383406601 ;
	setAttr ".r" -type "double3" 1.5902773407317602e-15 -1.2722218725854067e-14 -1.5902773407317584e-14 ;
	setAttr ".jot" -type "string" "yzx";
	setAttr ".jo" -type "double3" 38.305287338439044 -3.8132918262489776e-05 -59.800016500656717 ;
	setAttr ".pa" -type "double3" 4.7708320221952767e-15 -9.5416640443905503e-15 -1.6697912077683464e-14 ;
	setAttr ".liw" yes;
createNode joint -n "thumb2RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|thumb1RGT_jnt";
	rename -uid "7D886F84-4C4B-0E82-2101-83A44EE89795";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 4.5438053691171376e-05 -0.29702518112990539 -4.3887677963816429e-05 ;
	setAttr ".r" -type "double3" -1.5505204072134647e-14 -7.789874161240717e-15 4.0129654770027963e-14 ;
	setAttr ".jot" -type "string" "yzx";
	setAttr ".jo" -type "double3" 3.9222162624623507 16.846993414471481 22.541639129661721 ;
	setAttr ".pa" -type "double3" 8.7465253740246687e-15 -5.9262679025706969e-15 4.3695354744949957e-14 ;
	setAttr ".liw" yes;
createNode joint -n "thumb3RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|thumb1RGT_jnt|thumb2RGT_jnt";
	rename -uid "6BA928B0-4686-CD31-DE13-6A967DCDF3FD";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.034453952745821e-05 -0.42455777865737721 2.1701074131241782e-05 ;
	setAttr ".r" -type "double3" -4.308657670045108e-14 5.2590968619668217e-14 -8.2495637050460143e-15 ;
	setAttr ".jot" -type "string" "yzx";
	setAttr ".jo" -type "double3" 2.745307383402023 3.7890408502402781 4.7169620290734562 ;
	setAttr ".pa" -type "double3" -2.7208651376582422e-14 5.6044852219070003e-14 -2.2363275104040481e-15 ;
	setAttr ".liw" yes;
createNode joint -n "thumb4RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|thumb1RGT_jnt|thumb2RGT_jnt|thumb3RGT_jnt";
	rename -uid "D8FB9EEE-4AAB-EE53-D4D6-A59B3CF2F7FA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.0409888231043283e-05 -0.27818465137539827 -4.856451715079535e-06 ;
	setAttr ".jo" -type "double3" -3.4113763043827867 2.9896944288778466 3.3919257830216383 ;
	setAttr ".liw" yes;
createNode parentConstraint -n "thumb3RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|thumb1RGT_jnt|thumb2RGT_jnt|thumb3RGT_jnt";
	rename -uid "A3680EA5-47BC-0313-502E-8EA84D854A84";
	addAttr -ci true -k true -sn "w0" -ln "thumb3RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" -0.43643183099276311 0.89973732661047223 -3.8116955136668906e-07 0
		 0.67453092608321763 0.32719162157039955 -0.66178068310539984 0 -0.59542865790422095 -0.28882241235398176 -0.74969749063766089 0
		 -6.3092207121102497 11.806300329524232 0.68959745987380661 1;
	setAttr ".tg[0].tt" -type "double3" 1.7763568394002505e-15 -8.8817841970012523e-16 
		-7.2164496600635175e-16 ;
	setAttr ".tg[0].trp" -type "double3" 5.3290705182007498e-15 1.3322676295501875e-15 
		1.3045120539345589e-15 ;
	setAttr ".tg[0].tr" -type "double3" 6.3611093629270351e-15 4.980455668738021e-16 
		-8.126196467454465e-16 ;
	setAttr ".tg[0].ts" -type "double3" 0.99999999999999978 0.99999999999999978 1 ;
	setAttr ".cpim" -type "matrix" -0.5287952551688565 0.66102975614939907 -0.53236757939976975 -0
		 0.84874942009447407 0.41184018650573861 -0.33168069384305809 0 -4.4498093038081166e-07 -0.62723785142797683 -0.77882775870908538 0
		 -13.35692657220368 -0.68374245041098836 1.0941966717702243 1;
	setAttr ".lr" -type "double3" -4.308657670045108e-14 5.2590968619668217e-14 -8.2495637050460143e-15 ;
	setAttr ".rst" -type "double3" -5.034453952745821e-05 -0.42455777865737537 2.1701074134572451e-05 ;
	setAttr ".cjo" -type "double3" 2.7453073834020216 3.7890408502402764 4.7169620290734544 ;
	setAttr ".rsrr" -type "double3" -5.2031886742067211e-14 2.8848624884212044e-14 -1.8350309627037564e-14 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "thumb2RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|thumb1RGT_jnt|thumb2RGT_jnt";
	rename -uid "EED70901-4DC8-2564-3E00-66B9CE063497";
	addAttr -ci true -k true -sn "w0" -ln "thumb2RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" -0.52879525516885628 0.84874942009447474 -4.4498093079714529e-07 0
		 0.66102975614939974 0.41184018650573823 -0.62723785142797683 0 -0.53236757939977009 -0.33168069384305843 -0.77882775870908616 0
		 -6.0285904562183523 11.981200211995009 0.4233156523582452 1;
	setAttr ".tg[0].tt" -type "double3" 0 6.6613381477509392e-16 1.1102230246251565e-15 ;
	setAttr ".tg[0].trp" -type "double3" 0 -2.9976021664879227e-15 1.3322676295501878e-15 ;
	setAttr ".tg[0].tr" -type "double3" 3.1805546814635168e-15 -7.0345156885688586e-16 
		1.1855889546165554e-15 ;
	setAttr ".cpim" -type "matrix" -0.86427494681858841 0.39472955778791274 -0.31179703736611281 -0
		 0.50301969772692134 0.67821291406178685 -0.53572234123521101 0 -6.6554497699100601e-07 -0.61985144946864335 -0.78471917307478778 0
		 -11.237093685193326 -5.7807742253002141 4.8710400056254786 1;
	setAttr ".lr" -type "double3" -1.5505204072134647e-14 -7.789874161240717e-15 4.0129654770027963e-14 ;
	setAttr ".rst" -type "double3" 4.5438053687618662e-05 -0.29702518112991072 -4.3887677966480965e-05 ;
	setAttr ".cjo" -type "double3" 3.9222162624623524 16.846993414471481 22.541639129661721 ;
	setAttr ".rsrr" -type "double3" -2.9817700138720474e-14 -1.8027284542201411e-14 
		2.5456861493432607e-14 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "thumb1RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|thumb1RGT_jnt";
	rename -uid "0AB36823-4706-A9FE-6A5F-91910787D27C";
	addAttr -ci true -k true -sn "w0" -ln "thumb1RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" -0.86427494681858896 0.50301969772692123 -6.6554497702394119e-07 0
		 0.39472955778791263 0.67821291406178719 -0.61985144946864346 0 -0.31179703736611269 -0.53572234123521145 -0.78471917307478822 0
		 -5.9113202508955975 12.182600157793228 0.23916972383406995 1;
	setAttr ".tg[0].tt" -type "double3" 1.7763568394002505e-15 1.7763568394002505e-15 
		0 ;
	setAttr ".tg[0].trp" -type "double3" 1.7763568394002505e-15 -8.8817841970012523e-16 
		4.440892098500627e-15 ;
	setAttr ".tg[0].tr" -type "double3" 3.1805546814635168e-15 -3.4348965707884777e-16 
		2.7836523471165211e-15 ;
	setAttr ".tg[0].ts" -type "double3" 1 1 1.0000000000000002 ;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -1.2246467991473532e-16 -0
		 1 -2.2204460492503131e-16 2.7192621468937821e-32 0 -2.5407181807812022e-33 -1.2246467991473532e-16 -1 0
		 -12.206000004781481 5.530470046844485 -0.029264500000000793 1;
	setAttr ".lr" -type "double3" 1.5902773407317602e-15 -1.2722218725854067e-14 -1.5902773407317584e-14 ;
	setAttr ".rst" -type "double3" -5.9113200000000035 12.182599999999997 0.23916999999999711 ;
	setAttr ".cjo" -type "double3" 38.305287338439044 -3.8132918265981049e-05 -59.800016500656717 ;
	setAttr ".rsrr" -type "double3" -6.3611093629270217e-15 -3.0215269473903408e-14 
		-4.4527765540489235e-14 ;
	setAttr -k on ".w0";
createNode joint -n "index1RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt";
	rename -uid "0CC9AA9D-4815-0D7C-6063-43B43CDC8C55";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.05770024250499084 -0.61843023536899899 -0.35056704487524137 ;
	setAttr ".r" -type "double3" 1.5293032025637217e-13 4.5984644374050814e-05 -4.179273675652796e-05 ;
	setAttr ".jo" -type "double3" 2.0199438975631948e-05 -89.999958016211949 0 ;
	setAttr ".pa" -type "double3" -2.1144256088436699e-09 7.1322597707461256e-15 -1.3420144426320223e-14 ;
	setAttr ".liw" yes;
createNode joint -n "index2RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt";
	rename -uid "3783CF04-4A6A-E1D6-1001-DB9FDC341D5D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.0091715392989262234 -0.31508002026567627 8.1277971020199402e-09 ;
	setAttr ".r" -type "double3" 2.0197222436011681e-05 -9.2540098482121091e-05 8.3585457247977132e-05 ;
	setAttr ".liw" yes;
createNode joint -n "index3RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt|index2RGT_jnt";
	rename -uid "36AFC975-407D-2158-F8A8-73A24C2E492E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.0020239999999999148 -0.53397000000000006 1.7763568394002505e-15 ;
	setAttr ".liw" yes;
createNode joint -n "index4RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt|index2RGT_jnt|index3RGT_jnt";
	rename -uid "27F96222-4FDD-7E28-5EE0-7BA7489DA9F4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.5511151231257827e-17 -0.32330999999999843 1.7763568394002505e-15 ;
	setAttr ".liw" yes;
createNode joint -n "index5RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt|index2RGT_jnt|index3RGT_jnt|index4RGT_jnt";
	rename -uid "C500C20A-4D68-5C95-96A6-08BED3658334";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.5511151231257827e-17 -0.27066999999999997 0 ;
	setAttr ".liw" yes;
createNode parentConstraint -n "index4RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt|index2RGT_jnt|index3RGT_jnt|index4RGT_jnt";
	rename -uid "E522B8F9-4C82-2302-D1FF-0DA2FADF7EFB";
	addAttr -ci true -k true -sn "w0" -ln "index4RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1.4588408662286128e-06 -8.8237458919979363e-07 -0.99999999999854683 0
		 0.99999999999868761 -7.05055817641764e-07 1.4588414883521555e-06 0 -7.0505710488539832e-07 -0.9999999999993624 8.8237356066156071e-07 0
		 -7.3212602995253206 12.263700946162933 0.32844883353861237 1;
	setAttr ".tg[0].tt" -type "double3" 5.5511151231257827e-17 0 0 ;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" 1.4588408662286128e-06 0.99999999999868716 -7.05057104885398e-07 -0
		 -8.8237458919974811e-07 -7.0505581764176357e-07 -0.99999999999936184 0 -0.99999999999854638 1.4588414883521553e-06 8.8237356063439573e-07 -0
		 0.32847033526993663 6.9979584669546231 12.26369549443395 1;
	setAttr ".rst" -type "double3" 0 -0.32330999999999932 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "index3RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt|index2RGT_jnt|index3RGT_jnt";
	rename -uid "139ABDBF-4569-82E3-FC46-14AC48F57729";
	addAttr -ci true -k true -sn "w0" -ln "index3RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1.4588408662286128e-06 -8.8237458919979363e-07 -0.99999999999854683 0
		 0.99999999999868761 -7.05055817641764e-07 1.4588414883521555e-06 0 -7.0505710488539832e-07 -0.9999999999993624 8.8237356066156071e-07 0
		 -6.9979502995257459 12.263700718211338 0.328449305196654 1;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" 1.4588408662286128e-06 0.99999999999868716 -7.05057104885398e-07 -0
		 -8.8237458919974811e-07 -7.0505581764176357e-07 -0.99999999999936184 0 -0.99999999999854638 1.4588414883521553e-06 8.8237356063439573e-07 -0
		 0.33049433526993655 6.4639884669546221 12.263695494433952 1;
	setAttr ".rst" -type "double3" 0.0020239999999998592 -0.53397000000000094 -1.7763568394002505e-15 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "index2RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt|index2RGT_jnt";
	rename -uid "65682EC6-4E70-A1FA-9779-C1AC06FDD877";
	addAttr -ci true -k true -sn "w0" -ln "index2RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1.4588408662286128e-06 -8.8237458919979363e-07 -0.99999999999854683 0
		 0.99999999999868761 -7.05055817641764e-07 1.4588414883521555e-06 0 -7.0505710488539832e-07 -0.9999999999993624 8.8237356066156071e-07 0
		 -6.4639803024791398 12.263700343518607 0.33047408417424046 1;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" -1.2246444046379498e-16 0.99999999999993783 -3.5254671740250531e-07 -0
		 7.3275533374333003e-07 -3.525467174024106e-07 -0.99999999999966926 0 -0.99999999999973133 -2.5845295192876381e-13 -7.3275533374323887e-07 0
		 0.32129355858338654 6.1489046057404471 12.26369831494396 1;
	setAttr ".lr" -type "double3" 2.0197222436011681e-05 -9.2540098482121091e-05 8.3585457247977132e-05 ;
	setAttr ".rst" -type "double3" -0.009172000000000069 -0.31507999999999914 1.7763568394002505e-15 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "index1RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt";
	rename -uid "9B320784-400C-9391-EB8E-5AB30D26D155";
	addAttr -ci true -k true -sn "w0" -ln "index1RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 7.2942057659292439e-07 -6.982837286173566e-08 -0.99999999999973177 0
		 0.99999999999967204 -3.5254666913738981e-07 7.2942060121064115e-07 0 -3.525467200715489e-07 -0.99999999999993561 6.9828115734083038e-08 0
		 -6.1489002822134813 12.263700247286474 0.32130254487524135 1;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -1.2246467991473532e-16 -0
		 1 -2.2204460492503131e-16 2.7192621468937821e-32 0 -2.5407181807812022e-33 -1.2246467991473532e-16 -1 0
		 -12.206000004781481 5.530470046844485 -0.029264500000000793 1;
	setAttr ".lr" -type "double3" 1.5293032025637217e-13 4.5984644374050814e-05 -4.179273675652796e-05 ;
	setAttr ".rst" -type "double3" -6.1489000000000011 12.2637 0.32130300000000006 ;
	setAttr ".cjo" -type "double3" -95.46831451815811 -89.999958016211949 95.468334717595624 ;
	setAttr ".rsrr" -type "double3" 1.5902773407317584e-14 1.7655625192200634e-30 -1.2722218725854067e-14 ;
	setAttr -k on ".w0";
createNode joint -n "middle1RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt";
	rename -uid "87081C9A-4343-C2D6-FDD5-DCBBF251035E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.075700230961176729 -0.62686007739266447 -0.14268903746937536 ;
	setAttr ".r" -type "double3" 1.5293032025637217e-13 4.5984644374050814e-05 -4.179273675652796e-05 ;
	setAttr ".jo" -type "double3" 2.0199438975631948e-05 -89.999958016211949 0 ;
	setAttr ".pa" -type "double3" -2.1144256088436699e-09 7.1322597707461256e-15 -1.3420144426320223e-14 ;
	setAttr ".liw" yes;
createNode joint -n "middle2RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt";
	rename -uid "13836462-4163-EB1C-8C77-9891380902B7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.0025754692806923135 -0.32183000312866472 1.1517455433818213e-08 ;
	setAttr ".r" -type "double3" 2.019719090964487e-05 -9.2540098488470707e-05 8.3585457243479143e-05 ;
	setAttr ".liw" yes;
createNode joint -n "middle3RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt|middle2RGT_jnt";
	rename -uid "23C1F181-4621-A1B4-DF40-78B1B663421C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.002022999999999886 -0.5845400000000005 3.5527136788005009e-15 ;
	setAttr ".liw" yes;
createNode joint -n "middle4RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt|middle2RGT_jnt|middle3RGT_jnt";
	rename -uid "DB11DFBA-4404-7450-E149-A897775A68F4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.7755575615628914e-17 -0.35335000000000072 -1.7763568394002505e-15 ;
	setAttr ".liw" yes;
createNode joint -n "middle5RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt|middle2RGT_jnt|middle3RGT_jnt|middle4RGT_jnt";
	rename -uid "67230C40-4BC7-8C8E-9CA5-05B177CD93F6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.9388939039072284e-17 -0.30823 0 ;
	setAttr ".liw" yes;
createNode parentConstraint -n "middle4RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt|middle2RGT_jnt|middle3RGT_jnt|middle4RGT_jnt";
	rename -uid "F0A6AAD5-4765-DED4-9657-348C9160B185";
	addAttr -ci true -k true -sn "w0" -ln "middle4RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1.4588408661501081e-06 -8.8237458931061519e-07 -0.99999999999854683 0
		 0.99999999999868761 -7.0505526740286186e-07 1.4588414882731654e-06 0 -7.050565546464964e-07 -0.9999999999993624 8.8237356077318515e-07 0
		 -7.41705012441333 12.281700999051743 0.10882469995591811 1;
	setAttr ".tg[0].tt" -type "double3" 1.3877787807814457e-17 0 0 ;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" 1.4588408661501077e-06 0.99999999999868716 -7.0505655464649608e-07 -0
		 -8.8237458931056987e-07 -7.0505526740286155e-07 -0.99999999999936184 0 -0.99999999999854638 1.458841488273165e-06 8.8237356074602006e-07 -0
		 0.10884635731246273 7.0637086249235868 12.28169567358006 1;
	setAttr ".rst" -type "double3" -8.3266726846886741e-17 -0.35334999999999983 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "middle3RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt|middle2RGT_jnt|middle3RGT_jnt";
	rename -uid "51ADDF19-4515-2761-851D-75A66B5816C3";
	addAttr -ci true -k true -sn "w0" -ln "middle3RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1.4588408661501081e-06 -8.8237458931061519e-07 -0.99999999999854683 0
		 0.99999999999868761 -7.0505526740286186e-07 1.4588414882731654e-06 0 -7.050565546464964e-07 -0.9999999999993624 8.8237356077318515e-07 0
		 -7.0637001244137938 12.281700749920466 0.10882521543755792 1;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" 1.4588408661501077e-06 0.99999999999868716 -7.0505655464649608e-07 -0
		 -8.8237458931056987e-07 -7.0505526740286155e-07 -0.99999999999936184 0 -0.99999999999854638 1.458841488273165e-06 8.8237356074602006e-07 -0
		 0.11086935731246259 6.4791686249235871 12.281695673580066 1;
	setAttr ".rst" -type "double3" 0.0020229999999999276 -0.5845400000000005 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "middle2RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt|middle2RGT_jnt";
	rename -uid "D17E525D-41CC-5FA4-54F5-74815E115F5A";
	addAttr -ci true -k true -sn "w0" -ln "middle2RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1.4588408661501081e-06 -8.8237458931061519e-07 -0.99999999999854683 0
		 0.99999999999868761 -7.0505526740286186e-07 1.4588414882731654e-06 0 -7.050565546464964e-07 -0.9999999999993624 8.8237356077318515e-07 0
		 -6.4791601273657955 12.281700339572504 0.11084906818875849 1;
	setAttr ".tg[0].tt" -type "double3" 1.3877787807814457e-17 0 0 ;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" -1.2246444046379498e-16 0.99999999999993783 -3.5254671740250531e-07 -0
		 7.3275533374333003e-07 -3.525467174024106e-07 -0.99999999999966926 0 -0.99999999999973133 -2.5845295192876381e-13 -7.3275533374323887e-07 0
		 0.11341553798798895 6.1573344541098951 12.281698148104509 1;
	setAttr ".lr" -type "double3" 2.019719090964487e-05 -9.2540098488470707e-05 8.3585457243479143e-05 ;
	setAttr ".rst" -type "double3" 0.0025749999999998968 -0.32183000000000028 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "middle1RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt";
	rename -uid "2732FCF7-4B46-D1E9-88E3-5B9351F195FD";
	addAttr -ci true -k true -sn "w0" -ln "middle1RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 7.2942057659292439e-07 -6.982837286173566e-08 -0.99999999999973177 0
		 0.99999999999967204 -3.5254666913738981e-07 7.2942060121064115e-07 0 -3.525467200715489e-07 -0.99999999999993561 6.9828115734083038e-08 0
		 -6.1573301242371468 12.28170023574266 0.11342453746937534 1;
	setAttr ".tg[0].tt" -type "double3" 1.3877787807814457e-17 0 0 ;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -1.2246467991473532e-16 -0
		 1 -2.2204460492503131e-16 2.7192621468937821e-32 0 -2.5407181807812022e-33 -1.2246467991473532e-16 -1 0
		 -12.206000004781481 5.530470046844485 -0.029264500000000793 1;
	setAttr ".lr" -type "double3" 1.5293032025637217e-13 4.5984644374050814e-05 -4.179273675652796e-05 ;
	setAttr ".rst" -type "double3" -6.1573300000000009 12.2817 0.11342500000000001 ;
	setAttr ".cjo" -type "double3" -95.46831451815811 -89.999958016211949 95.468334717595624 ;
	setAttr ".rsrr" -type "double3" 1.5902773407317584e-14 1.7655625192200634e-30 -1.2722218725854067e-14 ;
	setAttr -k on ".w0";
createNode joint -n "ring1RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt";
	rename -uid "2782C5CE-4D59-82DA-524E-0F949ED04842";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.069500212157334929 -0.61222993703275552 0.052734151426214515 ;
	setAttr ".r" -type "double3" 1.5293032025637217e-13 4.5984644374050814e-05 -4.179273675652796e-05 ;
	setAttr ".jo" -type "double3" 2.0199438975631948e-05 -89.999958016211949 0 ;
	setAttr ".pa" -type "double3" -2.1144256088436699e-09 7.1322597707461256e-15 -1.3420144426320223e-14 ;
	setAttr ".liw" yes;
createNode joint -n "ring2RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt";
	rename -uid "29108727-4B9F-D60A-1F26-C09BED09A5BF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.0172036526634258 -0.31013998178845448 2.0206778472697806e-08 ;
	setAttr ".r" -type "double3" 2.0197086542932197e-05 -9.2540098482071529e-05 8.3585457238981127e-05 ;
	setAttr ".liw" yes;
createNode joint -n "ring3RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt|ring2RGT_jnt";
	rename -uid "BAA57786-48DE-099F-62DE-A2965122D8A1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.002023599999999931 -0.52674000000000198 -3.5527136788005009e-15 ;
	setAttr ".liw" yes;
createNode joint -n "ring4RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt|ring2RGT_jnt|ring3RGT_jnt";
	rename -uid "424E411B-49AF-21C6-4D42-B79412DA0BDE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.5511151231257827e-17 -0.3423799999999968 0 ;
	setAttr ".liw" yes;
createNode joint -n "ring5RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt|ring2RGT_jnt|ring3RGT_jnt|ring4RGT_jnt";
	rename -uid "558FB637-4E0F-C95C-8BBA-F5BBD0A4434D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.9388939039072284e-17 -0.29543000000000053 0 ;
	setAttr ".liw" yes;
createNode parentConstraint -n "ring4RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt|ring2RGT_jnt|ring3RGT_jnt|ring4RGT_jnt";
	rename -uid "2E4A2BA4-494A-E598-054D-51A50959303B";
	addAttr -ci true -k true -sn "w0" -ln "ring4RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1.458840866071603e-06 -8.823745891989286e-07 -0.99999999999854683 0
		 0.99999999999868761 -7.0505344586009613e-07 1.4588414881930528e-06 0 -7.0505473310373024e-07 -0.9999999999993624 8.8237356066415581e-07 0
		 -7.3219599627124285 12.275500929667418 -0.10122717199788153 1;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" 1.4588408660716032e-06 0.99999999999868738 -7.0505473310373024e-07 -0
		 -8.8237458919888329e-07 -7.0505344586009613e-07 -0.99999999999936207 0 -0.9999999999985465 1.4588414881930528e-06 8.8237356063699115e-07 -0
		 -0.10120565883323109 6.9795887652614486 12.275495856597241 1;
	setAttr ".rst" -type "double3" -4.163336342344337e-17 -0.34237999999999857 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ring3RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt|ring2RGT_jnt|ring3RGT_jnt";
	rename -uid "341D21A8-4CD8-AE3A-D7ED-ABBF5496485A";
	addAttr -ci true -k true -sn "w0" -ln "ring3RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1.458840866071603e-06 -8.823745891989286e-07 -0.99999999999854683 0
		 0.99999999999868761 -7.0505344586009613e-07 1.4588414881930528e-06 0 -7.0505473310373024e-07 -0.9999999999993624 8.8237356066415581e-07 0
		 -6.9795799627128794 12.275500688271221 -0.10122667251973286 1;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" 1.4588408660716032e-06 0.99999999999868738 -7.0505473310373024e-07 -0
		 -8.8237458919888329e-07 -7.0505344586009613e-07 -0.99999999999936207 0 -0.9999999999985465 1.4588414881930528e-06 8.8237356063699115e-07 -0
		 -0.099182058833231154 6.4528487652614457 12.275495856597239 1;
	setAttr ".rst" -type "double3" 0.0020235999999998755 -0.52674000000000021 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ring2RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt|ring2RGT_jnt";
	rename -uid "D28447FF-4610-8DDF-81F8-AEB75509E604";
	addAttr -ci true -k true -sn "w0" -ln "ring2RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1.458840866071603e-06 -8.823745891989286e-07 -0.99999999999854683 0
		 0.99999999999868761 -7.0505344586009613e-07 1.4588414881930528e-06 0 -7.0505473310373024e-07 -0.9999999999993624 8.8237356066415581e-07 0
		 -6.4528399656656807 12.275500318676942 -0.099202304089570412 1;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" -1.2246444046379498e-16 0.99999999999993783 -3.5254671740250531e-07 -0
		 7.3275533374333003e-07 -3.525467174024106e-07 -0.99999999999966926 0 -0.99999999999973133 -2.5845295192876381e-13 -7.3275533374323887e-07 0
		 -0.082007646364451625 6.1427043115641409 12.275497991261094 1;
	setAttr ".lr" -type "double3" 2.0197086542932197e-05 -9.2540098482071529e-05 8.3585457238981127e-05 ;
	setAttr ".rst" -type "double3" 0.017203199999999919 -0.31014000000000053 1.7763568394002505e-15 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "ring1RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt";
	rename -uid "DC2ED5B8-4D0C-3A46-0C9A-13A06EA498F2";
	addAttr -ci true -k true -sn "w0" -ln "ring1RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 7.2942057659292439e-07 -6.982837286173566e-08 -0.99999999999973177 0
		 0.99999999999967204 -3.5254666913738981e-07 7.2942060121064115e-07 0 -3.525467200715489e-07 -0.99999999999993561 6.9828115734083038e-08 0
		 -6.1426999838772378 12.275500216938818 -0.081998651426214555 1;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -1.2246467991473532e-16 -0
		 1 -2.2204460492503131e-16 2.7192621468937821e-32 0 -2.5407181807812022e-33 -1.2246467991473532e-16 -1 0
		 -12.206000004781481 5.530470046844485 -0.029264500000000793 1;
	setAttr ".lr" -type "double3" 1.5293032025637217e-13 4.5984644374050814e-05 -4.179273675652796e-05 ;
	setAttr ".rst" -type "double3" -6.1427000000000005 12.275500000000001 -0.081998199999999993 ;
	setAttr ".cjo" -type "double3" -95.46831451815811 -89.999958016211949 95.468334717595624 ;
	setAttr ".rsrr" -type "double3" 1.5902773407317584e-14 1.7655625192200634e-30 -1.2722218725854067e-14 ;
	setAttr -k on ".w0";
createNode joint -n "pinky1RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt";
	rename -uid "59C8B04A-4AB8-4933-47A7-92A3550EB58C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.044200179903462811 -0.55764981003139269 0.23907490984773677 ;
	setAttr ".r" -type "double3" 1.5293032025637217e-13 4.5984644374050814e-05 -4.179273675652796e-05 ;
	setAttr ".jo" -type "double3" 2.0199438975631948e-05 -89.999958016211949 0 ;
	setAttr ".pa" -type "double3" -2.1144256088436699e-09 7.1322597707461256e-15 -1.3420144426320223e-14 ;
	setAttr ".liw" yes;
createNode joint -n "pinky2RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt";
	rename -uid "E319684C-455A-A491-F88A-A6BEFB123CF4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.031652433481485776 -0.29576996070995953 2.9260435852052069e-08 ;
	setAttr ".r" -type "double3" 2.0196950487925265e-05 -9.25400984820219e-05 8.3585457247977051e-05 ;
	setAttr ".liw" yes;
createNode joint -n "pinky3RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt|pinky2RGT_jnt";
	rename -uid "24620031-4BD3-CF0D-77DA-9C87CB042A9C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.0020239999999998592 -0.43034000000000017 -1.7763568394002505e-15 ;
	setAttr ".liw" yes;
createNode joint -n "pinky4RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt|pinky2RGT_jnt|pinky3RGT_jnt";
	rename -uid "84AFF932-46DE-5BD6-4BB3-64A4E893BB68";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.5511151231257827e-17 -0.29421999999999926 3.5527136788005009e-15 ;
	setAttr ".liw" yes;
createNode joint -n "pinky5RGT_jnt" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt|pinky2RGT_jnt|pinky3RGT_jnt|pinky4RGT_jnt";
	rename -uid "8078E13A-4E9F-291B-8A26-5CBFF564CA41";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.5511151231257827e-17 -0.24796999999999958 0 ;
	setAttr ".liw" yes;
createNode parentConstraint -n "pinky4RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt|pinky2RGT_jnt|pinky3RGT_jnt|pinky4RGT_jnt";
	rename -uid "F0C641A2-4B04-0C10-EB2D-10AB9BDCE347";
	addAttr -ci true -k true -sn "w0" -ln "pinky4RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1.4588408662286114e-06 -8.8237458919806251e-07 -0.99999999999854683 0
		 0.99999999999868761 -7.0505107125226155e-07 1.4588414883479661e-06 0 -7.0505235849589587e-07 -0.9999999999993624 8.8237356066675398e-07 0
		 -7.1084498146321797 12.250200791956605 -0.3020169003473448 1;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" 1.4588408662286114e-06 0.99999999999868738 -7.0505235849589576e-07 -0
		 -8.823745891980173e-07 -7.0505107125226144e-07 -0.99999999999936207 0 -0.9999999999985465 1.4588414883479658e-06 8.8237356063958889e-07 -0
		 -0.30199572098392935 6.814238892234826 12.250196046611215 1;
	setAttr ".rst" -type "double3" -1.6653345369377348e-16 -0.29422000000000015 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "pinky3RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt|pinky2RGT_jnt|pinky3RGT_jnt";
	rename -uid "C79251CE-4611-79A3-EDF1-DC9609A951F5";
	addAttr -ci true -k true -sn "w0" -ln "pinky3RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1.4588408662286114e-06 -8.8237458919806251e-07 -0.99999999999854683 0
		 0.99999999999868761 -7.0505107125226155e-07 1.4588414883479661e-06 0 -7.0505235849589587e-07 -0.9999999999993624 8.8237356066675398e-07 0
		 -6.8142298146325659 12.250200584516479 -0.3020164711270022 1;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" 1.4588408662286114e-06 0.99999999999868738 -7.0505235849589576e-07 -0
		 -8.823745891980173e-07 -7.0505107125226144e-07 -0.99999999999936207 0 -0.9999999999985465 1.4588414883479658e-06 8.8237356063958889e-07 -0
		 -0.29997172098392955 6.383898892234825 12.250196046611212 1;
	setAttr ".rst" -type "double3" 0.0020240000000000258 -0.43033999999999928 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "pinky2RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt|pinky2RGT_jnt";
	rename -uid "10D64BAE-4577-3FD3-B172-9FA0FD90957A";
	addAttr -ci true -k true -sn "w0" -ln "pinky2RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1.4588408662286114e-06 -8.8237458919806251e-07 -0.99999999999854683 0
		 0.99999999999868761 -7.0505107125226155e-07 1.4588414883479661e-06 0 -7.0505235849589587e-07 -0.9999999999993624 8.8237356066675398e-07 0
		 -6.3838898175858256 12.250200282890727 -0.29999184332915912 1;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" -1.2246444046379498e-16 0.99999999999993783 -3.5254671740250531e-07 -0
		 7.3275533374333003e-07 -3.525467174024106e-07 -0.99999999999966926 0 -0.99999999999973133 -2.5845295192876381e-13 -7.3275533374323887e-07 0
		 -0.26834838624719026 6.0881241756432889 12.250197841707088 1;
	setAttr ".lr" -type "double3" 2.0196950487925265e-05 -9.25400984820219e-05 8.3585457247977051e-05 ;
	setAttr ".rst" -type "double3" 0.031651999999999902 -0.29577000000000009 0 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "pinky1RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt";
	rename -uid "189D0A42-47D3-905A-3B6B-C895E7A3A7CF";
	addAttr -ci true -k true -sn "w0" -ln "pinky1RGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 7.2942057659292439e-07 -6.982837286173566e-08 -0.99999999999973177 0
		 0.99999999999967204 -3.5254666913738981e-07 7.2942060121064115e-07 0 -3.525467200715489e-07 -0.99999999999993561 6.9828115734083038e-08 0
		 -6.088119856875875 12.250200184684946 -0.26833940984773685 1;
	setAttr ".tg[0].tr" -type "double3" -1.7360107021273429e-30 0 0 ;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -1.2246467991473532e-16 -0
		 1 -2.2204460492503131e-16 2.7192621468937821e-32 0 -2.5407181807812022e-33 -1.2246467991473532e-16 -1 0
		 -12.206000004781481 5.530470046844485 -0.029264500000000793 1;
	setAttr ".lr" -type "double3" 1.5293032025637217e-13 4.5984644374050814e-05 -4.179273675652796e-05 ;
	setAttr ".rst" -type "double3" -6.0881200000000018 12.2502 -0.268339 ;
	setAttr ".cjo" -type "double3" -95.46831451815811 -89.999958016211949 95.468334717595624 ;
	setAttr ".rsrr" -type "double3" 1.5902773407317584e-14 1.7655625192200634e-30 -1.2722218725854067e-14 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "foreArmRGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt";
	rename -uid "12AB0B40-407D-97E7-3CF3-01B457647D78";
	addAttr -ci true -k true -sn "w0" -ln "foreArmFkRGT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "foreArmIkRGT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 2.2204460492503131e-16 1 0 0 1 -2.2204460492503131e-16 -1.2246467991473532e-16 0
		 -1.2246467991473532e-16 2.4651903288156619e-32 -1 0 -1.0005901081494957 12.205982950468735 -0.029264277959557883 1;
	setAttr ".tg[0].tt" -type "double3" 1.7054312754893886e-05 -2.1908098569841714 0.046354522040442478 ;
	setAttr ".tg[0].tro" 1;
	setAttr ".tg[1].tpm" -type "matrix" 2.2204576444471623e-16 0.99999999995517586 9.4682780266066486e-06 0
		 1 -2.2204460492503131e-16 -1.2246467991473532e-16 0 -1.2246257752919222e-16 9.4682780266066486e-06 -0.99999999995517586 0
		 -1.0005900000000001 12.20600000478149 -0.02926450000000011 1;
	setAttr ".tg[1].tw" 0;
	setAttr ".tg[1].tt" -type "double3" 0 -2.19081 0.046354300000000265 ;
	setAttr ".tg[1].tr" -type "double3" -4.8455231032484517e-12 1.9000192799695485e-27 
		8.9810438464807733e-26 ;
	setAttr ".tg[1].tro" 1;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -1.2246467991473532e-16 -0
		 1 -2.2204460492503131e-16 2.7192621468937821e-32 0 -2.5407181807812022e-33 -1.2246467991473532e-16 -1 0
		 -12.205982950468735 1.0005901081494983 -0.029264277959558004 1;
	setAttr ".rst" -type "double3" 0 -2.19081 0.046354300000000251 ;
	setAttr ".cro" 1;
	setAttr -k on ".w0";
	setAttr -k on ".w1" 0;
createNode parentConstraint -n "upArmRGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt";
	rename -uid "8C12A177-41D4-D6E1-CDD6-B3AEC1579AAA";
	addAttr -ci true -k true -sn "w0" -ln "upArmFkRGT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "upArmIkRGT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 2.2204460492503131e-16 1 0 0 1 -2.2204460492503131e-16 -1.2246467991473532e-16 0
		 -1.2246467991473532e-16 2.4651903288156619e-32 -1 0 -1.0005901081494957 12.205982950468735 -0.029264277959557883 1;
	setAttr ".tg[0].tro" 4;
	setAttr ".tg[1].tpm" -type "matrix" 2.2204460492503131e-16 1 0 0 1 -2.2204460492503131e-16 -1.2246467991473532e-16 0
		 -1.2246467991473532e-16 2.4651903288156619e-32 -1 0 -1.0005901081494957 12.205982950468735 -0.029264277959557883 1;
	setAttr ".tg[1].tw" 0;
	setAttr ".tg[1].tt" -type "double3" 1.7054312754893886e-05 1.0814949558124454e-07 
		2.220404422270672e-07 ;
	setAttr ".tg[1].tr" -type "double3" 2.127338084580129e-19 0.0005424923701891223 
		-6.5649426724197384e-20 ;
	setAttr ".tg[1].tro" 4;
	setAttr ".cpim" -type "matrix" 2.2204460492503131e-16 1 -1.2246467991473532e-16 -0
		 1 -2.2204460492503131e-16 2.7192621468937821e-32 0 -2.5407181807812022e-33 -1.2246467991473532e-16 -1 0
		 -12.205982950468735 1.0005901081494983 -0.029264277959558004 1;
	setAttr ".rst" -type "double3" -1.00059 12.206 -0.029264499999999999 ;
	setAttr ".cro" 4;
	setAttr -k on ".w0";
	setAttr -k on ".w1" 0;
createNode parentConstraint -n "clav1LFT_jnt_parentConstraint2" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt";
	rename -uid "C6DB01F2-4AD6-6722-D7A5-C8A92B4F39AD";
	addAttr -ci true -k true -sn "w0" -ln "clavGmblLFT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 2.2204460492503131e-16 -1 0 0 1 2.2204460492503131e-16 0 0
		 0 0 1 0 0.42673607897087601 12.112382950468737 0.53529722204044217 1;
	setAttr ".tg[0].trp" -type "double3" 1.7763568394002505e-15 1.1102230246251565e-16 
		0 ;
	setAttr ".cpim" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".rst" -type "double3" 0.42673607897087612 12.112382950468735 0.53529722204044217 ;
	setAttr ".cro" 4;
	setAttr ".cjo" -type "double3" 0 0 -89.999999999999986 ;
createNode parentConstraint -n "clav1RGT_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt";
	rename -uid "DCEC306F-4C2E-3EBA-0486-D69A8F6C6C7E";
	addAttr -ci true -k true -sn "w0" -ln "clavGmblRGT_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 2.2204460492503131e-16 1 0 0 1 -2.2204460492503131e-16 -1.2246467991473532e-16 0
		 -1.2246467991473532e-16 2.4651903288156619e-32 -1 0 -1.0005900000000003 12.206 -0.029264500000000027 1;
	setAttr ".tg[0].trp" -type "double3" -0.093617049531264485 0.57385392102912403 -0.5645617220404423 ;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -11.650614580989298 -0.3164618075204384 1;
	setAttr ".rst" -type "double3" -0.42673607897087618 12.112382950468735 0.53529722204044217 ;
	setAttr ".cro" 4;
	setAttr ".cjo" -type "double3" -180 0 89.999999999999986 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "spine5_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt";
	rename -uid "98137390-4AFC-B13A-7D39-AFBFC6D0A3B7";
	addAttr -ci true -k true -sn "w0" -ln "spine5Fk_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "spine5Ik_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 11.013774709569622 0.41486876929647815 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" 0 0.63683986663818359 -0.098406961776039914 ;
	setAttr ".tg[0].tro" 2;
	setAttr ".tg[1].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 11.013774709569619 0.41486876929647837 1;
	setAttr ".tg[1].tt" -type "double3" 2.8349688854848509e-31 0.63683989551238795 -0.098406961776039692 ;
	setAttr ".tg[1].tro" 2;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -11.013774709569619 -0.41486876929647837 1;
	setAttr ".rst" -type "double3" 0 0.63683989551238795 -0.098406961776039914 ;
	setAttr ".cro" 2;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode parentConstraint -n "spine4_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt";
	rename -uid "256249B8-409A-6311-EFBE-2C96A82953C4";
	addAttr -ci true -k true -sn "w0" -ln "spine4Fk_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "spine4Ik_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 11.013774689903896 0.41486876929647815 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" 0 1.9665726824769081e-08 0 ;
	setAttr ".tg[0].tro" 2;
	setAttr ".tg[1].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -1.9721522630525295e-31 11.013774689903894 0.41486876929647831 1;
	setAttr ".tg[1].tt" -type "double3" 1.9721522630525295e-31 1.9665725048412241e-08 
		5.5511151231257827e-17 ;
	setAttr ".tg[1].tro" 2;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 1.9721522630525295e-31 -10.388103263246226 -0.48783876549735405 1;
	setAttr ".rst" -type "double3" 0 0.62567142665766795 -0.072969996200875853 ;
	setAttr ".cro" 2;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode parentConstraint -n "spine3_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt";
	rename -uid "D41A93B9-443B-B10F-EFBD-FE83661DFF5D";
	addAttr -ci true -k true -sn "w0" -ln "spine3Fk_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "spine3Ik_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 10.388103253511369 0.487838765497354 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" 0 9.7348582528411498e-09 0 ;
	setAttr ".tg[0].tro" 2;
	setAttr ".tg[1].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 4.8148248609680896e-35 10.388103253511366 0.48783876549735405 1;
	setAttr ".tg[1].tt" -type "double3" -1.9726337455386263e-31 9.7348600291979892e-09 
		0 ;
	setAttr ".tg[1].tro" 2;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -4.8148248609680896e-35 -9.8118195103279806 -0.49734415937876753 1;
	setAttr ".rst" -type "double3" 0 0.57628374318338516 -0.0095053938814134709 ;
	setAttr ".cro" 2;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode parentConstraint -n "spine2_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt|spine2_jnt";
	rename -uid "A3A1271C-4033-EF14-54F7-59AFA3CD7725";
	addAttr -ci true -k true -sn "w0" -ln "spine2Fk_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "spine2Ik_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 9.8118195156358592 0.49734415937876747 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" 0 -5.3078750283930276e-09 0 ;
	setAttr ".tg[0].tro" 2;
	setAttr ".tg[1].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 9.8118195156358556 0.49734415937876758 1;
	setAttr ".tg[1].tt" -type "double3" 4.8148248609680896e-35 -5.3078750283930276e-09 
		-5.5511151231257827e-17 ;
	setAttr ".tg[1].tro" 2;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -9.1944331096283651 -0.48903753711989456 1;
	setAttr ".rst" -type "double3" 0 0.61738640600749051 0.0083066222588729088 ;
	setAttr ".cro" 2;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode parentConstraint -n "spine1_jnt_parentConstraint1" -p "|root_jnt|spine1_jnt";
	rename -uid "706D39FC-475A-38EA-4D33-A6ADCA903DB3";
	addAttr -ci true -k true -sn "w0" -ln "spine1Fk_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "spine1Ik_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 9.1944331096283669 0.48903753711989451 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" 0 1.7763568394002505e-15 5.5511151231257827e-17 ;
	setAttr ".tg[0].tro" 2;
	setAttr ".tg[1].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 9.1944331096283669 0.48903753711989451 1;
	setAttr ".tg[1].tt" -type "double3" 0 -1.7763568394002505e-15 5.5511151231257827e-17 ;
	setAttr ".tg[1].tro" 2;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -9.0474036391524066 -0.46364678601588916 1;
	setAttr ".rst" -type "double3" 0 9.1944331096283669 0.48903753711989473 ;
	setAttr ".cro" 2;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode parentConstraint -n "root_jnt_parentConstraint1" -p "|root_jnt";
	rename -uid "FC606646-48D3-AC42-3013-CBAA77D48031";
	addAttr -ci true -k true -sn "w0" -ln "rootGmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 9.0474036391524066 0.46364678601588916 1;
	setAttr ".tg[0].tro" 2;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 0 -0 1;
	setAttr ".rst" -type "double3" 0 9.0474036391524066 0.46364678601588938 ;
	setAttr ".cro" 2;
	setAttr -k on ".w0";
createNode joint -n "pelvis_jnt" -p "|root_jnt";
	rename -uid "CC830E0D-4080-A867-0470-BEBC87FDFC70";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.13700887608035828 -0.019181242651250319 ;
	setAttr ".ro" 2;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 8.9103947630720484 0.44446554336463884 1;
	setAttr ".liw" yes;
createNode parentConstraint -n "pelvis_jnt_parentConstraint1" -p "|root_jnt|pelvis_jnt";
	rename -uid "19568B46-42E2-CB00-3C26-189A021CB2C7";
	addAttr -ci true -k true -sn "w0" -ln "pelvisGmbl_ctrlW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 8.9103947630720501 0.44446554336463884 1;
	setAttr ".tg[0].trp" -type "double3" 0 -1.7763568394002505e-15 0 ;
	setAttr ".tg[0].tro" 2;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -9.0474036391524066 -0.46364678601588916 1;
	setAttr ".rst" -type "double3" 0 8.9103947630720484 0.44446554336463906 ;
	setAttr ".cro" 2;
	setAttr -k on ".w0";
createNode joint -n "upLegLFT_jnt" -p "|root_jnt|pelvis_jnt";
	rename -uid "6B3C48FC-4C62-52F4-50E0-6A93EC8A449C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.53507476617022964 -0.8876544753789144 -0.075506157549423025 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-16 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0.53507476617022964 8.022740287693134 0.36895938581521603 1;
	setAttr ".liw" yes;
createNode joint -n "lowLegLFT_jnt" -p "|root_jnt|pelvis_jnt|upLegLFT_jnt";
	rename -uid "73546187-4852-ED9E-6C7E-EFAD3176E65C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -4.4408920985006262e-16 3.5058895835072974 0.076940176673593763 ;
	setAttr ".ro" 1;
	setAttr ".liw" yes;
createNode joint -n "ankleLFT_jnt" -p "|root_jnt|pelvis_jnt|upLegLFT_jnt|lowLegLFT_jnt";
	rename -uid "F0D7AF61-41A6-8A60-364C-DCAC3736B661";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.3306690738754696e-16 3.9466648116917931 -0.43281458396227523 ;
	setAttr ".ro" 4;
	setAttr ".jo" -type "double3" 90.000000000000014 0 0 ;
	setAttr ".liw" yes;
createNode joint -n "ballLFT_jnt" -p "|root_jnt|pelvis_jnt|upLegLFT_jnt|lowLegLFT_jnt|ankleLFT_jnt";
	rename -uid "A2A68326-4916-3802-15C1-628A427479DB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.2204460492503131e-16 1.0465532167313603 -0.5701858924940435 ;
	setAttr ".ro" 1;
	setAttr ".liw" yes;
createNode joint -n "toeLFT_jnt" -p "|root_jnt|pelvis_jnt|upLegLFT_jnt|lowLegLFT_jnt|ankleLFT_jnt|ballLFT_jnt";
	rename -uid "E4816EF5-4BE1-7C78-E4D8-6B889E129914";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.64096693617542755 1.1102230246251563e-16 ;
	setAttr ".ro" 1;
	setAttr ".liw" yes;
createNode parentConstraint -n "toeLFT_jnt_parentConstraint1" -p "|root_jnt|pelvis_jnt|upLegLFT_jnt|lowLegLFT_jnt|ankleLFT_jnt|ballLFT_jnt|toeLFT_jnt";
	rename -uid "A663D4BC-4A2E-5028-392A-84816A571462";
	addAttr -ci true -k true -sn "w0" -ln "toeFkLFT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "toeIkLFT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" -1 1.2246467991473532e-16 0 0 2.7192621468937821e-32 2.2204460492503131e-16 1 0
		 1.2246467991473532e-16 1 -2.2204460492503131e-16 0 0.53507476617022931 1.9902570147500853e-08 1.059638184426219 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" 0 0.64096695184707642 -4.9303806576313238e-32 ;
	setAttr ".tg[0].tro" 1;
	setAttr ".tg[1].tpm" -type "matrix" -1 1.2246467991473532e-16 0 0 2.7192621468937821e-32 2.2204460492503131e-16 1 0
		 1.2246467991473532e-16 1 -2.2204460492503131e-16 0 0.5350747661702292 3.3306690738754696e-16 1.0596381952578948 1;
	setAttr ".tg[1].tt" -type "double3" 0 0.64096693617542755 -4.9303806576313238e-32 ;
	setAttr ".tg[1].tro" 1;
	setAttr ".cpim" -type "matrix" -1 2.7192621468937821e-32 1.2246467991473532e-16 -0
		 1.2246467991473532e-16 2.2204460492503131e-16 1 -0 0 1 -2.2204460492503131e-16 -0
		 0.5350747661702292 -1.0596381952578948 -5.2285420465007835e-17 1;
	setAttr ".rst" -type "double3" 0 0.64096693617542755 1.1102230246251563e-16 ;
	setAttr ".cro" 1;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode parentConstraint -n "ballLFT_jnt_parentConstraint1" -p "|root_jnt|pelvis_jnt|upLegLFT_jnt|lowLegLFT_jnt|ankleLFT_jnt|ballLFT_jnt";
	rename -uid "09563C09-4450-83CD-02DF-2B857C4D1370";
	addAttr -ci true -k true -sn "w0" -ln "ballFkLFT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "ballIkLFT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" -1 1.2246467991473532e-16 0 0 2.7192621468937821e-32 2.2204460492503131e-16 1 0
		 1.2246467991473532e-16 1 -2.2204460492503131e-16 0 0.53507476617022964 0.57018591239661376 0.013084978526534397 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" 2.2204460492503131e-16 1.0465532058996845 -0.57018589249404383 ;
	setAttr ".tg[0].tro" 1;
	setAttr ".tg[1].tpm" -type "matrix" -1 1.2246467991473532e-16 0 0 2.7192621468937821e-32 2.2204460492503131e-16 1 0
		 1.2246467991473532e-16 1 -2.2204460492503131e-16 0 0.53507476617022953 0.5701858924940435 0.013084978526534341 1;
	setAttr ".tg[1].tt" -type "double3" 2.2204460492503131e-16 1.0465532167313603 -0.57018589249404339 ;
	setAttr ".tg[1].tro" 1;
	setAttr ".cpim" -type "matrix" -1 2.7192621468937821e-32 1.2246467991473532e-16 -0
		 1.2246467991473532e-16 2.2204460492503131e-16 1 -0 0 1 -2.2204460492503131e-16 -0
		 0.53507476617022942 -0.013084978526534468 -0.57018589249404361 1;
	setAttr ".rst" -type "double3" 2.2204460492503131e-16 1.0465532167313603 -0.5701858924940435 ;
	setAttr ".cro" 1;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode parentConstraint -n "ankleLFT_jnt_parentConstraint1" -p "|root_jnt|pelvis_jnt|upLegLFT_jnt|lowLegLFT_jnt|ankleLFT_jnt";
	rename -uid "93BF10F3-4573-55AC-48F0-DC8E27F3EF9D";
	addAttr -ci true -k true -sn "w0" -ln "ankleFkLFT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "ankleIkLFT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" -1 1.2246467991473532e-16 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0.53507476617022998 4.5168507996784308 0.44589956248880963 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" -1.1102230246251565e-16 3.9466648872818171 -0.43281458396227523 ;
	setAttr ".tg[0].tro" 4;
	setAttr ".tg[0].tjo" -type "double3" 90.000000000000014 0 0 ;
	setAttr ".tg[1].tpm" -type "matrix" -1 1.2246467991473532e-16 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0.53507476617022964 4.5168507041858366 0.44589956248880958 1;
	setAttr ".tg[1].tt" -type "double3" -3.3306690738754696e-16 3.9466648116917931 -0.43281458396227523 ;
	setAttr ".tg[1].tro" 4;
	setAttr ".tg[1].tjo" -type "double3" 90.000000000000014 0 0 ;
	setAttr ".cpim" -type "matrix" -1 -1.2246467991473532e-16 0 -0 1.2246467991473532e-16 -1 -0 0
		 0 -0 1 -0 0.53507476617022909 4.5168507041858366 -0.44589956248880958 1;
	setAttr ".rst" -type "double3" -3.3306690738754696e-16 3.9466648116917931 -0.43281458396227523 ;
	setAttr ".cro" 4;
	setAttr ".cjo" -type "double3" 90.000000000000014 0 1.4033418597069752e-14 ;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode parentConstraint -n "lowLegLFT_jnt_parentConstraint1" -p "|root_jnt|pelvis_jnt|upLegLFT_jnt|lowLegLFT_jnt";
	rename -uid "72EC410D-4FC9-4C4F-FBC0-2E9894C11732";
	addAttr -ci true -k true -sn "w0" -ln "lowLegFkLFT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "lowLegIkLFT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" -1 1.2246467991473532e-16 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0.53507476617022998 8.0227402876931357 0.36895938581521581 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" -4.4408920985006262e-16 3.5058894880147049 0.076940176673593819 ;
	setAttr ".tg[0].tro" 1;
	setAttr ".tg[1].tpm" -type "matrix" -1 1.2246467991473532e-16 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0.53507476617022964 8.022740287693134 0.36895938581521581 1;
	setAttr ".tg[1].tt" -type "double3" -4.4408920985006262e-16 3.5058895835072974 0.076940176673593763 ;
	setAttr ".tg[1].tro" 1;
	setAttr ".cpim" -type "matrix" -1 -1.2246467991473532e-16 0 -0 1.2246467991473532e-16 -1 -0 0
		 0 -0 1 -0 0.53507476617022864 8.022740287693134 -0.36895938581521581 1;
	setAttr ".rst" -type "double3" -4.4408920985006262e-16 3.5058895835072974 0.076940176673593763 ;
	setAttr ".cro" 1;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode parentConstraint -n "upLegLFT_jnt_parentConstraint1" -p "|root_jnt|pelvis_jnt|upLegLFT_jnt";
	rename -uid "1788A19B-4988-6638-7AA3-25BF61E9EF7D";
	addAttr -ci true -k true -sn "w0" -ln "upLegFkLFT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "upLegIkLFT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 8.9103947630720484 0.44446554336463884 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" 0.53507476617022998 -0.88765447537891262 -0.075506157549423025 ;
	setAttr ".tg[0].tro" 1;
	setAttr ".tg[0].tjo" -type "double3" 0 0 180 ;
	setAttr ".tg[1].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 8.9103947630720484 0.44446554336463884 1;
	setAttr ".tg[1].tt" -type "double3" 0.53507476617022964 -0.8876544753789144 -0.075506157549423025 ;
	setAttr ".tg[1].tro" 1;
	setAttr ".tg[1].tjo" -type "double3" 0 0 180 ;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -8.9103947630720484 -0.44446554336463884 1;
	setAttr ".rst" -type "double3" 0.53507476617022964 -0.8876544753789144 -0.075506157549423025 ;
	setAttr ".cro" 1;
	setAttr ".cjo" -type "double3" 0 0 180 ;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode joint -n "upLegRGT_jnt" -p "|root_jnt|pelvis_jnt";
	rename -uid "40678F38-4D67-2E1F-8EF3-3ABDB94A5BBA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.53507476617022953 -0.8876544753789144 -0.075506157549423081 ;
	setAttr ".r" -type "double3" -0.00041575522166032493 -0.99886119960208097 0.047695144886336853 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".bps" -type "matrix" -0.99984769541645258 0.00083243721934248363 -0.017432527604600911 0
		 0.00083243721934248363 0.999999653497751 7.2562949861355707e-06 0 0.017432527604600911 -7.2562949861354606e-06 -0.99984804191870169 0
		 -0.53507476617022953 8.022740287693134 0.36895938581521598 1;
	setAttr ".liw" yes;
createNode joint -n "lowLegRGT_jnt" -p "|root_jnt|pelvis_jnt|upLegRGT_jnt";
	rename -uid "97794DE0-466E-F9B3-F955-A390EDB763C9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -3.5058900000000017 -0.076941000000000093 ;
	setAttr ".ro" 1;
	setAttr ".liw" yes;
createNode joint -n "ankleRGT_jnt" -p "|root_jnt|pelvis_jnt|upLegRGT_jnt|lowLegRGT_jnt";
	rename -uid "8E83BEE2-4080-52D6-61DF-2DB034C77288";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -3.9466639999999997 0.43281500000000006 ;
	setAttr ".r" -type "double3" 0.00041575507759211721 -0.047695144887595936 -0.99886085351200959 ;
	setAttr ".ro" 4;
	setAttr ".jo" -type "double3" 90.000000000000014 0 0 ;
	setAttr ".liw" yes;
createNode joint -n "ballRGT_jnt" -p "|root_jnt|pelvis_jnt|upLegRGT_jnt|lowLegRGT_jnt|ankleRGT_jnt";
	rename -uid "78F4F747-48FD-1552-54BD-39854B23C76B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1102230246251565e-16 -1.0465549999999997 0.57018599999999897 ;
	setAttr ".ro" 1;
	setAttr ".liw" yes;
createNode joint -n "toeRGT_jnt" -p "|root_jnt|pelvis_jnt|upLegRGT_jnt|lowLegRGT_jnt|ankleRGT_jnt|ballRGT_jnt";
	rename -uid "4DC65AE3-4599-5A5F-0633-52BAAB3C0E42";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1102230246251565e-16 -0.64097 1.1101939078675947e-16 ;
	setAttr ".ro" 1;
	setAttr ".liw" yes;
createNode parentConstraint -n "toeRGT_jnt_parentConstraint1" -p "|root_jnt|pelvis_jnt|upLegRGT_jnt|lowLegRGT_jnt|ankleRGT_jnt|ballRGT_jnt|toeRGT_jnt";
	rename -uid "F70FB881-4C17-6A19-F19C-A38648A54551";
	addAttr -ci true -k true -sn "w0" -ln "toeFkRGT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "toeIkRGT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" -1 0 -1.2246467991473532e-16 0 1.2246467991473532e-16 -2.2204460492503131e-16 -1 0
		 -2.7192621468937821e-32 -1 2.2204460492503131e-16 0 -0.53507476617022931 1.249694936422685e-08 1.0596382375247411 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" -1.1102230246251565e-16 -0.64096999168395996 
		8.3780634425658218e-22 ;
	setAttr ".tg[0].tro" 1;
	setAttr ".tg[1].tpm" -type "matrix" -1.0000000000000002 -5.88364437635006e-17 7.3899220076611982e-16 0
		 -7.3552275381416621e-16 1.4506455661266598e-16 -1.0000000000000004 0 5.9048254939873378e-17 -0.99999999999999978 -1.8772537100112882e-16 0
		 -0.53507476617022853 2.8769313342724701e-07 1.0596403858152157 1;
	setAttr ".tg[1].tt" -type "double3" -1.1102230246251565e-16 -0.64097 8.3780634425658218e-22 ;
	setAttr ".tg[1].tro" 1;
	setAttr ".cpim" -type "matrix" -0.99999999999999978 -7.4593109467002656e-16 5.5367023281326477e-17 0
		 -5.7313504994117105e-17 1.8775840528607167e-16 -1.0000000000000002 0 7.4246164771807294e-16 -0.99999999999999956 -1.4507048584329676e-16 -0
		 -0.5350747661702292 1.0596403858152148 2.8769313372161742e-07 1;
	setAttr ".rst" -type "double3" -1.1102230246251565e-16 -0.64097 8.3780634425658218e-22 ;
	setAttr ".cro" 1;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode parentConstraint -n "ballRGT_jnt_parentConstraint1" -p "|root_jnt|pelvis_jnt|upLegRGT_jnt|lowLegRGT_jnt|ankleRGT_jnt|ballRGT_jnt";
	rename -uid "36DC1AAD-4C5A-D87E-0AAA-5A8EEB1C497B";
	addAttr -ci true -k true -sn "w0" -ln "ballFkRGT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "ballIkRGT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" -1 0 -1.2246467991473532e-16 0 1.2246467991473532e-16 -2.2204460492503131e-16 -1 0
		 -2.7192621468937821e-32 -1 2.2204460492503131e-16 0 -0.53507476617022942 0.57018590499099275 0.013084978526534341 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" -2.2204460492503131e-16 -1.0465532589982065 
		0.57018589249404361 ;
	setAttr ".tg[0].tro" 1;
	setAttr ".tg[1].tpm" -type "matrix" -1.0000000000000002 -5.88364437635006e-17 7.3899220076611982e-16 0
		 -7.3552275381416621e-16 1.4506455661266598e-16 -1.0000000000000004 0 5.9048254939873378e-17 -0.99999999999999978 -1.8772537100112882e-16 0
		 -0.53507476617022942 0.5701862876931334 0.013085385815215678 1;
	setAttr ".tg[1].tt" -type "double3" -1.1102230246251565e-16 -1.0465549999999997 
		0.570186 ;
	setAttr ".tg[1].tr" -type "double3" 4.4072265952417451e-30 2.5444437451708134e-14 
		-3.3258204165106182e-35 ;
	setAttr ".tg[1].tro" 1;
	setAttr ".cpim" -type "matrix" -0.99999999999999978 -7.4593109467002656e-16 5.5367023281326477e-17 0
		 -5.7313504994117105e-17 1.8775840528607167e-16 -1.0000000000000002 0 7.4246164771807294e-16 -0.99999999999999956 -1.4507048584329676e-16 -0
		 -0.53507476617022931 0.013085385815215166 0.57018628769313262 1;
	setAttr ".rst" -type "double3" -1.1102230246251565e-16 -1.0465549999999997 0.570186 ;
	setAttr ".cro" 1;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode parentConstraint -n "ankleRGT_jnt_parentConstraint1" -p "|root_jnt|pelvis_jnt|upLegRGT_jnt|lowLegRGT_jnt|ankleRGT_jnt";
	rename -uid "BBC14E01-4F4A-CFBD-2E47-63B3BCA65076";
	addAttr -ci true -k true -sn "w0" -ln "ankleFkRGT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "ankleIkRGT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" -1 0 -1.2246467991473532e-16 0 0 1 0 0 1.2246467991473532e-16 0 -1 0
		 -0.53507476617022964 4.5168508116077124 0.44589956248880958 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" -2.2204460492503131e-16 -3.9466649066167196 
		0.43281458396227523 ;
	setAttr ".tg[0].tro" 4;
	setAttr ".tg[0].tjo" -type "double3" 90.000000000000014 0 0 ;
	setAttr ".tg[1].tpm" -type "matrix" -0.99984769541645258 0.00083243721934248385 -0.017432527604600918 0
		 0.00083243721934248385 0.999999653497751 7.2562949861355724e-06 0 0.017432527604600918 -7.2562949861354674e-06 -0.99984804191870169 0
		 -0.5393344755995757 4.5168520607984952 0.44586325423645368 1;
	setAttr ".tg[1].tt" -type "double3" 0 -3.9466639999999997 0.432815 ;
	setAttr ".tg[1].tr" -type "double3" 0.00041575507759370649 -0.047695144887595936 
		-0.99886085351200937 ;
	setAttr ".tg[1].tro" 4;
	setAttr ".tg[1].tjo" -type "double3" 90.000000000000014 0 0 ;
	setAttr ".cpim" -type "matrix" -0.99984769541645202 0.00083243721934248363 0.017432527604600904 -0
		 0.00083243721934248363 0.99999965349775111 -7.2562949861354589e-06 0 -0.017432527604600904 7.2562949861355673e-06 -0.99984804191870091 0
		 -0.53523980476919475 -4.5164047689232305 0.45523024045678268 1;
	setAttr ".lr" -type "double3" 0.00041575507759211721 -0.047695144887595936 -0.99886085351200959 ;
	setAttr ".rst" -type "double3" 0 -3.9466639999999997 0.432815 ;
	setAttr ".cro" 4;
	setAttr ".cjo" -type "double3" 90.000000000000014 1.4124500153760508e-30 -1.4033418597069755e-14 ;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode parentConstraint -n "lowLegRGT_jnt_parentConstraint1" -p "|root_jnt|pelvis_jnt|upLegRGT_jnt|lowLegRGT_jnt";
	rename -uid "F9A6F4C2-4F18-4FC2-AFB8-2BAED4AB4015";
	addAttr -ci true -k true -sn "w0" -ln "lowLegFkRGT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "lowLegIkRGT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" -1 0 -1.2246467991473532e-16 0 0 1 0 0 1.2246467991473532e-16 0 -1 0
		 -0.53507476617022964 8.022740287693134 0.36895938581521581 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" 0 -3.5058894760854216 -0.076940176673593763 ;
	setAttr ".tg[0].tro" 1;
	setAttr ".tg[1].tpm" -type "matrix" -0.99984769541645258 0.00083243721934248385 -0.017432527604600918 0
		 0.00083243721934248385 0.999999653497751 7.2562949861355724e-06 0 0.017432527604600918 -7.2562949861354674e-06 -0.99984804191870169 0
		 -0.53507476617022953 8.022740287693134 0.36895938581521576 1;
	setAttr ".tg[1].tt" -type "double3" 0 -3.5058900000000008 -0.076941000000000037 ;
	setAttr ".tg[1].tro" 1;
	setAttr ".cpim" -type "matrix" -0.99984769541645202 0.00083243721934248363 0.017432527604600904 -0
		 0.00083243721934248363 0.99999965349775111 -7.2562949861354589e-06 0 -0.017432527604600904 7.2562949861355673e-06 -0.99984804191870091 0
		 -0.53523980476919475 -8.0222947689232331 0.37828924045678247 1;
	setAttr ".rst" -type "double3" 0 -3.5058900000000008 -0.076941000000000037 ;
	setAttr ".cro" 1;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
createNode parentConstraint -n "upLegRGT_jnt_parentConstraint1" -p "|root_jnt|pelvis_jnt|upLegRGT_jnt";
	rename -uid "C6F57211-4238-5A29-DC2B-5DA99BB634C1";
	addAttr -ci true -k true -sn "w0" -ln "upLegFkRGT_jntW0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "upLegIkRGT_jntW1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".tg[0].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 8.9103947630720484 0.44446554336463884 1;
	setAttr ".tg[0].tw" 0;
	setAttr ".tg[0].tt" -type "double3" -0.53507476617022964 -0.8876544753789144 -0.075506157549423025 ;
	setAttr ".tg[0].tr" -type "double3" 0 -2.5444437451708134e-14 0 ;
	setAttr ".tg[0].tro" 1;
	setAttr ".tg[0].tjo" -type "double3" 0 180 0 ;
	setAttr ".tg[1].tpm" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 8.9103947630720484 0.44446554336463884 1;
	setAttr ".tg[1].tt" -type "double3" -0.53507476617022953 -0.8876544753789144 -0.075506157549423081 ;
	setAttr ".tg[1].tr" -type "double3" -0.00041575522166032488 -0.9988611996020812 
		0.047695144886336853 ;
	setAttr ".tg[1].tro" 1;
	setAttr ".tg[1].tjo" -type "double3" 0 180 0 ;
	setAttr ".cpim" -type "matrix" 1 -0 0 -0 -0 1 -0 0 0 -0 1 -0 -0 -8.9103947630720484 -0.44446554336463884 1;
	setAttr ".lr" -type "double3" -0.00041575522166032493 -0.99886119960208097 0.047695144886336853 ;
	setAttr ".rst" -type "double3" -0.535075 -0.88765476307204771 -0.075506543364639078 ;
	setAttr ".cro" 1;
	setAttr ".cjo" -type "double3" 0 180 0 ;
	setAttr -k on ".w0" 0;
	setAttr -k on ".w1";
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".vac" 2;
	setAttr ".etmr" no;
	setAttr ".tmr" 4096;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 12 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 14 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 56 ".dsm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :defaultRenderGlobals;
	addAttr -ci true -sn "shave_old_preRenderMel" -ln "shave_old_preRenderMel" -dt "string";
	addAttr -ci true -sn "shave_old_postRenderMel" -ln "shave_old_postRenderMel" -dt "string";
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl" no;
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf" 3;
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -k on ".an" yes;
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 0.96;
	setAttr -k on ".ef" 9.6;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe" yes;
	setAttr -cb on ".pff" yes;
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm" -type "string" "shave_MRFrameStart";
	setAttr -cb on ".pom" -type "string" "shave_MRFrameEnd";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -k on ".bls";
	setAttr -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl" -type "string" "Terry";
	setAttr ".shave_old_preRenderMel" -type "string" "";
	setAttr ".shave_old_postRenderMel" -type "string" "";
select -ne :defaultRenderQuality;
	setAttr -k on ".cch";
	setAttr -k on ".nds";
	setAttr ".rfl" 10;
	setAttr ".rfr" 10;
	setAttr ".sl" 10;
	setAttr ".eaa" 0;
	setAttr ".ufil" yes;
	setAttr ".ss" 2;
	setAttr -av ".mss";
	setAttr -k on ".mvs";
	setAttr -k on ".mvm";
	setAttr -k on ".vs";
	setAttr -k on ".pss";
	setAttr ".ert" yes;
	setAttr -k on ".rct";
	setAttr -k on ".gct";
	setAttr -k on ".bct";
	setAttr -k on ".cct";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w" 1920;
	setAttr -av -k on ".h" 1080;
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar" 1.7777777910232544;
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
select -ne :defaultColorMgtGlobals;
	setAttr ".cme" no;
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf" 0.80000001192092896;
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "|root_jnt.s" "|root_jnt|spine1_jnt.is";
connectAttr "|root_jnt|spine1_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt.is";
connectAttr "|root_jnt|spine1_jnt|spine2_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|head2_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|eyeLFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|eyeRGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1UPR_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1UPR_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1UPR_jnt|jaw2UPR_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|jaw2LWR_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|jaw2LWR_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|jaw2LWR_jnt|jaw3LWR_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt|togue3_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt|togue3_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt|togue3_jnt|togue4_posJnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt|togue3_jnt|togue4_posJnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt|togue3_jnt|togue4_posJnt|togue4_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt|togue3_jnt|togue4_posJnt|togue4_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|chest2_jnt|neck1_jnt|neck2_jnt|head1_jnt|jaw1LWR_jnt|togue1_posJnt|togue1_jnt|togue2_posJnt|togue2_jnt|togue3_posJnt|togue3_jnt|togue4_posJnt|togue4_jnt|togue5_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|handLFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|handLFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|handLFT_jnt|fingerLFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|thumb1LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|thumb1LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|thumb1LFT_jnt|thumb2LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|thumb1LFT_jnt|thumb2LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|thumb1LFT_jnt|thumb2LFT_jnt|thumb3LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|thumb1LFT_jnt|thumb2LFT_jnt|thumb3LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|thumb1LFT_jnt|thumb2LFT_jnt|thumb3LFT_jnt|thumb4LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt|index2LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt|index2LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt|index2LFT_jnt|index3LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt|index2LFT_jnt|index3LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt|index2LFT_jnt|index3LFT_jnt|index4LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt|index2LFT_jnt|index3LFT_jnt|index4LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|index1LFT_jnt|index2LFT_jnt|index3LFT_jnt|index4LFT_jnt|index5LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt|middle2LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt|middle2LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt|middle2LFT_jnt|middle3LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt|middle2LFT_jnt|middle3LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt|middle2LFT_jnt|middle3LFT_jnt|middle4LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt|middle2LFT_jnt|middle3LFT_jnt|middle4LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|middle1LFT_jnt|middle2LFT_jnt|middle3LFT_jnt|middle4LFT_jnt|middle5LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt|ring2LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt|ring2LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt|ring2LFT_jnt|ring3LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt|ring2LFT_jnt|ring3LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt|ring2LFT_jnt|ring3LFT_jnt|ring4LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt|ring2LFT_jnt|ring3LFT_jnt|ring4LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|ring1LFT_jnt|ring2LFT_jnt|ring3LFT_jnt|ring4LFT_jnt|ring5LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt|pinky2LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt|pinky2LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt|pinky2LFT_jnt|pinky3LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt|pinky2LFT_jnt|pinky3LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt|pinky2LFT_jnt|pinky3LFT_jnt|pinky4LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt|pinky2LFT_jnt|pinky3LFT_jnt|pinky4LFT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1LFT_jnt|clav2LFT_jnt|upArmLFT_jnt|foreArmLFT_jnt|wristLFT_jnt|pinky1LFT_jnt|pinky2LFT_jnt|pinky3LFT_jnt|pinky4LFT_jnt|pinky5LFT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|handRGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|handRGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|handRGT_jnt|fingerRGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|thumb1RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|thumb1RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|thumb1RGT_jnt|thumb2RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|thumb1RGT_jnt|thumb2RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|thumb1RGT_jnt|thumb2RGT_jnt|thumb3RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|thumb1RGT_jnt|thumb2RGT_jnt|thumb3RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|thumb1RGT_jnt|thumb2RGT_jnt|thumb3RGT_jnt|thumb4RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt|index2RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt|index2RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt|index2RGT_jnt|index3RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt|index2RGT_jnt|index3RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt|index2RGT_jnt|index3RGT_jnt|index4RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt|index2RGT_jnt|index3RGT_jnt|index4RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|index1RGT_jnt|index2RGT_jnt|index3RGT_jnt|index4RGT_jnt|index5RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt|middle2RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt|middle2RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt|middle2RGT_jnt|middle3RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt|middle2RGT_jnt|middle3RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt|middle2RGT_jnt|middle3RGT_jnt|middle4RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt|middle2RGT_jnt|middle3RGT_jnt|middle4RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|middle1RGT_jnt|middle2RGT_jnt|middle3RGT_jnt|middle4RGT_jnt|middle5RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt|ring2RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt|ring2RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt|ring2RGT_jnt|ring3RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt|ring2RGT_jnt|ring3RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt|ring2RGT_jnt|ring3RGT_jnt|ring4RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt|ring2RGT_jnt|ring3RGT_jnt|ring4RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|ring1RGT_jnt|ring2RGT_jnt|ring3RGT_jnt|ring4RGT_jnt|ring5RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt|pinky2RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt|pinky2RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt|pinky2RGT_jnt|pinky3RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt|pinky2RGT_jnt|pinky3RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt|pinky2RGT_jnt|pinky3RGT_jnt|pinky4RGT_jnt.is"
		;
connectAttr "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt|pinky2RGT_jnt|pinky3RGT_jnt|pinky4RGT_jnt.s" "|root_jnt|spine1_jnt|spine2_jnt|spine3_jnt|spine4_jnt|spine5_jnt|chest1_jnt|clav1RGT_jnt|clav2RGT_jnt|upArmRGT_jnt|foreArmRGT_jnt|wristRGT_jnt|pinky1RGT_jnt|pinky2RGT_jnt|pinky3RGT_jnt|pinky4RGT_jnt|pinky5RGT_jnt.is"
		;
connectAttr "|root_jnt.s" "|root_jnt|pelvis_jnt.is";
connectAttr "|root_jnt|pelvis_jnt.s" "|root_jnt|pelvis_jnt|upLegLFT_jnt.is";
connectAttr "|root_jnt|pelvis_jnt|upLegLFT_jnt.s" "|root_jnt|pelvis_jnt|upLegLFT_jnt|lowLegLFT_jnt.is"
		;
connectAttr "|root_jnt|pelvis_jnt|upLegLFT_jnt|lowLegLFT_jnt.s" "|root_jnt|pelvis_jnt|upLegLFT_jnt|lowLegLFT_jnt|ankleLFT_jnt.is"
		;
connectAttr "|root_jnt|pelvis_jnt|upLegLFT_jnt|lowLegLFT_jnt|ankleLFT_jnt.s" "|root_jnt|pelvis_jnt|upLegLFT_jnt|lowLegLFT_jnt|ankleLFT_jnt|ballLFT_jnt.is"
		;
connectAttr "|root_jnt|pelvis_jnt|upLegLFT_jnt|lowLegLFT_jnt|ankleLFT_jnt|ballLFT_jnt.s" "|root_jnt|pelvis_jnt|upLegLFT_jnt|lowLegLFT_jnt|ankleLFT_jnt|ballLFT_jnt|toeLFT_jnt.is"
		;
connectAttr "|root_jnt|pelvis_jnt.s" "|root_jnt|pelvis_jnt|upLegRGT_jnt.is";
connectAttr "|root_jnt|pelvis_jnt|upLegRGT_jnt.s" "|root_jnt|pelvis_jnt|upLegRGT_jnt|lowLegRGT_jnt.is"
		;
connectAttr "|root_jnt|pelvis_jnt|upLegRGT_jnt|lowLegRGT_jnt.s" "|root_jnt|pelvis_jnt|upLegRGT_jnt|lowLegRGT_jnt|ankleRGT_jnt.is"
		;
connectAttr "|root_jnt|pelvis_jnt|upLegRGT_jnt|lowLegRGT_jnt|ankleRGT_jnt.s" "|root_jnt|pelvis_jnt|upLegRGT_jnt|lowLegRGT_jnt|ankleRGT_jnt|ballRGT_jnt.is"
		;
connectAttr "|root_jnt|pelvis_jnt|upLegRGT_jnt|lowLegRGT_jnt|ankleRGT_jnt|ballRGT_jnt.s" "|root_jnt|pelvis_jnt|upLegRGT_jnt|lowLegRGT_jnt|ankleRGT_jnt|ballRGT_jnt|toeRGT_jnt.is"
		;
// End of joints.ma
