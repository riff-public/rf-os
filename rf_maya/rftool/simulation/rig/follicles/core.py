import pymel.core as pm ;

class FollicleUtil ( object ) :

	def __init__ ( self ) :
		pass ;

	def __str__ ( self ) :
		pass ;

	def __repr__ ( self ) :
		pass ;

	def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

	def gui ( self ) :

		self.width		= 400.00 ;
		self.title 		= 'Follicle Util'
		self.version 	= 2.0 ;