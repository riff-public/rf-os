import pymel.core as pm ;

def snapCurveJoint ( type = 'fk' , gimbal = True , *args ) :
    
    # make controller by selecting joint then curve ;
    # also copy roo
    
    selection = pm.ls ( sl = True ) ;
    joint = pm.general.PyNode ( selection [0] ) ;
    controller = pm.general.PyNode ( selection [1] ) ;
    controllerShape = pm.listRelatives ( controller , shapes = True ) [0] ;
    controllerShape = pm.general.PyNode ( controllerShape ) ;
    pm.makeIdentity ( controller , apply = True ) ;
    
    name = joint.split ( '_' ) [0] ;
    
    pm.rename ( controller , name + '_ctrl' ) ;
    
    group = pm.group ( em = True , n = name + 'CtrlZro_grp' ) ;
    group = pm.general.PyNode ( group ) ;
    
    if type == 'fk' :

        roo = pm.xform ( joint , q = True , roo = True ) ;
        pm.xform ( controller , roo = roo ) ;
        pm.xform ( group , roo = roo ) ;

        par = pm.parentConstraint ( joint , group , mo = False , sr = 'none' , st = 'none' ) ;
        pm.delete ( par ) ;
        
        pm.parent ( controller , group ) ;
        controller.translate.set ( 0 , 0 , 0 ) ;
        controller.rotate.set ( 0 , 0 , 0 ) ;
            
        controllerShape.overrideEnabled.set ( 1 ) ;
        controllerShape.overrideColor.set ( 13 ) ;
    
        if gimbal == True :
            gimbalController = pm.duplicate ( controller ) ;
            gimbalController = pm.rename ( gimbalController , name + 'Gimbal_ctrl' ) ;
            gimbalController = pm.general.PyNode ( gimbalController ) ;
            gimbalController.scale.set ( 0.85 , 0.85 , 0.85 ) ;
            pm.makeIdentity ( gimbalController , apply = True ) ;
        
            gimbalControllerShape = pm.listRelatives ( gimbalController , shapes = True ) [0] ;
            gimbalControllerShape = pm.general.PyNode ( gimbalControllerShape ) ;
            
            gimbalControllerShape.overrideEnabled.set ( 1 ) ;
            gimbalControllerShape.overrideColor.set ( 16 ) ;
            
            controllerShape.addAttr ( 'gimbalController' , keyable = True , attributeType = 'bool' ) ;
            controllerShape.gimbalController >> gimbalControllerShape.v ;

            pm.parent ( gimbalController , controller ) ;
            
            for each in [ controller , gimbalController ] :
                
                for axis in [ 'x' , 'y' , 'z' ] :
                    pm.setAttr ( each + '.s' + axis , lock = True , keyable = False , channelBox = False ) ;
                    
                pm.setAttr ( each + '.v' , lock = True , keyable = False , channelBox = False ) ;

        elif gimbal == False :

            for axis in [ 'x' , 'y' , 'z' ] :
                pm.setAttr ( controller + '.s' + axis , lock = True , keyable = False , channelBox = False ) ;
                
            pm.setAttr ( controller + '.v' , lock = True , keyable = False , channelBox = False ) ;

    elif type == 'ik' :

        controllerShape.overrideEnabled.set ( 1 ) ;
        controllerShape.overrideColor.set ( 6 ) ;

        par = pm.pointConstraint ( joint , group , mo = False , skip = 'none' ) ;
        pm.delete ( par ) ;
        
        pm.parent ( controller , group ) ;
        controller.translate.set ( 0 , 0 , 0 ) ;
        controller.rotate.set ( 0 , 0 , 0 ) ;
    
        for axis in [ 'x' , 'y' , 'z' ] :
            pm.setAttr ( controller + '.r' + axis , lock = True , keyable = False , channelBox = False ) ;
            pm.setAttr ( controller + '.s' + axis , lock = True , keyable = False , channelBox = False ) ;

            pm.setAttr ( controller + '.v' , lock = True , keyable = False , channelBox = False ) ;
             
def snapCurveJointFk ( *args ) :    
    snapCurveJoint ( type = 'fk' ) ;

def snapCurveJointFkNG ( *args ) :    
    snapCurveJoint ( type = 'fk' , gimbal = False ) ;

def snapCurveJointIk ( *args ) :    
    snapCurveJoint ( type = 'ik' ) ;

def outliner ( *args ) :
    pm.mel.eval ( 'OutlinerWindow' ) ;

def makeRigGrp ( parent = False , *args ) :
    
    selection = pm.ls ( sl = True ) [0] ;
    
    if pm.objExists ( 'anim_grp' ) == True :
        pass ;
    else :
        animGrp = pm.group ( w = True , em = True , n = 'anim_grp' ) ;
        pm.parent ( animGrp , 'fly_ctrl' ) ;
        
    animGrp = 'anim_grp' ;
    
    ###

    parent = pm.listRelatives ( selection , p = True ) ;
    # print parent >>> [nt.Joint(u'root_jnt')]
    # paint parent [0] >>> root_jnt

    groupName = selection.split ( '_' ) [0] ;
    group = pm.group ( w = True , em = True , n = groupName + 'Rig_grp' ) ;
    group = pm.general.PyNode ( group ) ;
    
    if parent[0].split ( '_' ) [-1] != 'jnt' :
        pass ;
    else :
        roo = pm.xform ( parent , q = True , roo = True ) ;
        pm.xform ( group , roo = roo ) ;
        par = pm.parentConstraint ( parent , group , mo = False , sr = 'none' , st = 'none' ) ;
    
    if parent != True :
        pass ;
    else :
        pm.parent ( group , animGrp ) ;

    for axis in [ 'x' , 'y' , 'z' ] :
            pm.setAttr ( group + '.t' + axis , lock = True , keyable = False , channelBox = False ) ;
            pm.setAttr ( group + '.r' + axis , lock = True , keyable = False , channelBox = False ) ;
            pm.setAttr ( group + '.s' + axis , lock = True , keyable = False , channelBox = False ) ;

def parentCurveShape2Joint ( *args ) :

    selection = pm.ls ( sl = True ) ;

    driver = selection [0] ;
    driven = selection [1] ;

    drivenShape = pm.listRelatives ( driven , shapes = True ) ;

    pm.parent ( drivenShape , driver , s = True , r = True ) ;

    pm.rename ( drivenShape , driver + 'Shape' ) ;
    pm.delete ( driven ) ;

#####################################################################################################################################

def run ( *args ) :
    
    # check if window exists
    if pm.window ( 'rigUI' , exists = True ) :
        pm.deleteUI ( 'rigUI' ) ;
    else : pass ;
   
    window = pm.window ( 'rigUI', title = "rig" ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'rigUI' , e = True , w = 300 , h = 100 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            pm.button ( label = 'snap ctrl to joint FK' , c = snapCurveJointFk ) ;
            pm.button ( label = 'snap ctrl to joint FK no gimbal' , c = snapCurveJointFkNG ) ;
            pm.button ( label = 'snap ctrl to joint IK' , c = snapCurveJointIk ) ;
            pm.text ( align = 'left' , label = '# select joint > curve > run' ) ;
            
            pm.separator ( vis = False , h = 10 ) ;

            pm.button ( label = 'make rig group' , c = makeRigGrp ) ; 
            pm.text ( align = 'left' , label = '# select joint you want rig group of > run' ) ;

            pm.separator ( vis = False , h = 10 ) ;

            pm.button ( label = 'parent curve shape to joint' , c = parentCurveShape2Joint ) ;
            pm.text ( align = 'left' , label = '# select joint > curve > run' ) ;

            pm.separator ( vis = False , h = 10 ) ;

            pm.button ( label = 'outliner' , c = outliner ) ;

    window.show () ;

run ( ) ;    

'''
# roll joint 
import pymel.core as pm ;

selection = pm.ls ( sl = True ) ;

driver = selection [0] ;

driven = selection [ 1 : 4 ] ;

driver = pm.general.PyNode ( driver ) ;
value = 0.75 ;

for each in driven :
    joint = pm.general.PyNode ( each ) ;
    name = joint.split ( '_' ) [0] ;
    mdv = pm.createNode ( 'multiplyDivide' , n = name + '_mdv' ) ;
    
    mdv.operation.set ( 1 ) ;
    
    driver.rx >> mdv.i1y ;
    mdv.i2y.set ( value ) ;
    value -= 0.25 ;
    
    mdv.oy >> joint.ry ;
'''

'''
#select skin joint

import maya.cmds as mc ;

selectionList = [u'spine1_jnt', u'spine2_jnt', u'spine3_jnt', u'spine4_jnt', u'chest1_jnt', u'neck1_jnt', u'head1_jnt', u'jaw1LWR_jnt', u'jaw2LWR_jnt', u'jaw1UPR_jnt', u'eyeLFT_jnt', u'eyeRGT_jnt', u'clav1LFT_jnt', u'upArmLFT_jnt', u'upArmRoll1LFT_jnt', u'upArmRoll2LFT_jnt', u'upArmRoll3LFT_jnt', u'foreArmLFT_jnt', u'foreArmRoll1LFT_jnt', u'foreArmRoll2LFT_jnt', u'foreArmRoll3LFT_jnt', u'wristLFT_jnt', u'thumb1LFT_jnt', u'thumb2LFT_jnt', u'thumb3LFT_jnt', u'index1LFT_jnt', u'index2LFT_jnt', u'index3LFT_jnt', u'index4LFT_jnt', u'middle1LFT_jnt', u'middle2LFT_jnt', u'middle3LFT_jnt', u'middle4LFT_jnt', u'ring1LFT_jnt', u'ring2LFT_jnt', u'ring3LFT_jnt', u'ring4LFT_jnt', u'pinky1LFT_jnt', u'pinky2LFT_jnt', u'pinky3LFT_jnt', u'pinky4LFT_jnt', u'clav1RGT_jnt', u'upArmRGT_jnt', u'upArmRoll1RGT_jnt', u'upArmRoll2RGT_jnt', u'upArmRoll3RGT_jnt', u'foreArmRGT_jnt', u'foreArmRoll1RGT_jnt', u'foreArmRoll2RGT_jnt', u'foreArmRoll3RGT_jnt', u'wristRGT_jnt', u'thumb1RGT_jnt', u'thumb2RGT_jnt', u'thumb3RGT_jnt', u'index1RGT_jnt', u'index2RGT_jnt', u'index3RGT_jnt', u'index4RGT_jnt', u'middle1RGT_jnt', u'middle2RGT_jnt', u'middle3RGT_jnt', u'middle4RGT_jnt', u'ring1RGT_jnt', u'ring2RGT_jnt', u'ring3RGT_jnt', u'ring4RGT_jnt', u'pinky1RGT_jnt', u'pinky2RGT_jnt', u'pinky3RGT_jnt', u'pinky4RGT_jnt', u'pelvis_jnt', u'upLegLFT_jnt', u'upLegRoll1LFT_jnt', u'upLegRoll2LFT_jnt', u'upLegRoll3LFT_jnt', u'lowLegLFT_jnt', u'lowLegRoll1LFT_jnt', u'lowLegRoll2LFT_jnt', u'lowLegRoll3LFT_jnt', u'ankleLFT_jnt', u'ballLFT_jnt', u'upLegRGT_jnt', u'upLegRoll1RGT_jnt', u'upLegRoll2RGT_jnt', u'upLegRoll3RGT_jnt', u'lowLegRGT_jnt', u'lowLegRoll1RGT_jnt', u'lowLegRoll2RGT_jnt', u'lowLegRoll3RGT_jnt', u'ankleRGT_jnt', u'ballRGT_jnt', u'brow1LFT_jnt1', u'brow2LFT_jnt1', u'brow3LFT_jnt1', u'brow1RGT_jnt1', u'brow2RGT_jnt1', u'brow3RGT_jnt1', u'eye1LFT_jnt1', u'eye2LFT_jnt1', u'eye3LFT_jnt1', u'eye4LFT_jnt1', u'eye5LFT_jnt1', u'eye6LFT_jnt1', u'eye7LFT_jnt1', u'eye8LFT_jnt1', u'eye1RGT_jnt1', u'eye2RGT_jnt1', u'eye3RGT_jnt1', u'eye4RGT_jnt1', u'eye5RGT_jnt1', u'eye6RGT_jnt1', u'eye7RGT_jnt1', u'eye8RGT_jnt1', u'cheekLFT_jnt1', u'cheekRGT_jnt1']

mc.select ( selectionList ) ;
'''

'''
# for selection skirt

import maya.cmds as mc ;

selectionList = [u'spine1_jnt', u'spine2_jnt', u'spine3_jnt', u'spine4_jnt', u'chest1_jnt', u'neck1_jnt', u'head1_jnt', u'jaw1LWR_jnt', u'jaw2LWR_jnt', u'jaw1UPR_jnt', u'eyeLFT_jnt', u'eyeRGT_jnt', u'clav1LFT_jnt', u'upArmLFT_jnt', u'upArmRoll1LFT_jnt', u'upArmRoll2LFT_jnt', u'upArmRoll3LFT_jnt', u'foreArmLFT_jnt', u'foreArmRoll1LFT_jnt', u'foreArmRoll2LFT_jnt', u'foreArmRoll3LFT_jnt', u'wristLFT_jnt', u'thumb1LFT_jnt', u'thumb2LFT_jnt', u'thumb3LFT_jnt', u'index1LFT_jnt', u'index2LFT_jnt', u'index3LFT_jnt', u'index4LFT_jnt', u'middle1LFT_jnt', u'middle2LFT_jnt', u'middle3LFT_jnt', u'middle4LFT_jnt', u'ring1LFT_jnt', u'ring2LFT_jnt', u'ring3LFT_jnt', u'ring4LFT_jnt', u'pinky1LFT_jnt', u'pinky2LFT_jnt', u'pinky3LFT_jnt', u'pinky4LFT_jnt', u'clav1RGT_jnt', u'upArmRGT_jnt', u'upArmRoll1RGT_jnt', u'upArmRoll2RGT_jnt', u'upArmRoll3RGT_jnt', u'foreArmRGT_jnt', u'foreArmRoll1RGT_jnt', u'foreArmRoll2RGT_jnt', u'foreArmRoll3RGT_jnt', u'wristRGT_jnt', u'thumb1RGT_jnt', u'thumb2RGT_jnt', u'thumb3RGT_jnt', u'index1RGT_jnt', u'index2RGT_jnt', u'index3RGT_jnt', u'index4RGT_jnt', u'middle1RGT_jnt', u'middle2RGT_jnt', u'middle3RGT_jnt', u'middle4RGT_jnt', u'ring1RGT_jnt', u'ring2RGT_jnt', u'ring3RGT_jnt', u'ring4RGT_jnt', u'pinky1RGT_jnt', u'pinky2RGT_jnt', u'pinky3RGT_jnt', u'pinky4RGT_jnt', u'pelvis_jnt', u'upLegLFT_jnt', u'upLegRoll1LFT_jnt', u'upLegRoll2LFT_jnt', u'upLegRoll3LFT_jnt', u'lowLegLFT_jnt', u'lowLegRoll1LFT_jnt', u'lowLegRoll2LFT_jnt', u'lowLegRoll3LFT_jnt', u'ankleLFT_jnt', u'ballLFT_jnt', u'upLegRGT_jnt', u'upLegRoll1RGT_jnt', u'upLegRoll2RGT_jnt', u'upLegRoll3RGT_jnt', u'lowLegRGT_jnt', u'lowLegRoll1RGT_jnt', u'lowLegRoll2RGT_jnt', u'lowLegRoll3RGT_jnt', u'ankleRGT_jnt', u'ballRGT_jnt', u'brow1LFT_jnt1', u'brow2LFT_jnt1', u'brow3LFT_jnt1', u'brow1RGT_jnt1', u'brow2RGT_jnt1', u'brow3RGT_jnt1', u'eye1LFT_jnt1', u'eye2LFT_jnt1', u'eye3LFT_jnt1', u'eye4LFT_jnt1', u'eye5LFT_jnt1', u'eye6LFT_jnt1', u'eye7LFT_jnt1', u'eye8LFT_jnt1', u'eye1RGT_jnt1', u'eye2RGT_jnt1', u'eye3RGT_jnt1', u'eye4RGT_jnt1', u'eye5RGT_jnt1', u'eye6RGT_jnt1', u'eye7RGT_jnt1', u'eye8RGT_jnt1', u'cheekLFT_jnt1', u'cheekRGT_jnt1' , u'skirtA1LFT_jnt', u'skirtA2LFT_jnt', u'skirtA3LFT_jnt', u'skirtB1LFT_jnt', u'skirtB2LFT_jnt', u'skirtB3LFT_jnt', u'skirtC1LFT_jnt', u'skirtC2LFT_jnt', u'skirtC3LFT_jnt', u'skirtD1LFT_jnt', u'skirtD2LFT_jnt', u'skirtD3LFT_jnt', u'skirtD4LFT_jnt', u'skirtD1RGT_jnt', u'skirtD2RGT_jnt', u'skirtD3RGT_jnt', u'skirtD4RGT_jnt', u'skirtC1RGT_jnt', u'skirtC2RGT_jnt', u'skirtC3RGT_jnt', u'skirtB1RGT_jnt', u'skirtB2RGT_jnt', u'skirtB3RGT_jnt', u'skirtA1RGT_jnt', u'skirtA2RGT_jnt', u'skirtA3RGT_jnt']

mc.select ( selectionList ) ;

'''