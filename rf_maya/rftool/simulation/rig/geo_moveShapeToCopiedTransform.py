import pymel.core as pm ;


class GNode ( object ) :

    def __init__ ( self , node ) :
        self.node = node ;

    def __str__( self) : 
        return str ( self.node ) ;
    
    def __repr__( self) : 
        return str ( self.node ) ;

    def freezeTransform ( self ) :
        pm.makeIdentity ( self.node , apply = True ) ;

    def deleteShapeOrig ( self ) :
    
        shapes = pm.listRelatives ( self.node , shapes = True ) ;

        for shape in shapes :
            if 'Orig' in str ( shape ) :
                pm.delete ( shape ) ;
            else : pass ;

def run ( *args  ) :
    # select transform then geometry
    selection   = pm.ls ( sl = True ) ;
    transform   = selection[0] ;
    geo         = selection[1] ;
    
    geo = GNode ( geo ) ;
    geo.freezeTransform ( ) ;
    geo.deleteShapeOrig ( ) ;
    geoName = str ( geo ) ;

    newTransform = pm.group ( em = True , w = True ) ;
    parCon = pm.parentConstraint ( transform , newTransform , mo = False , skipTranslate = 'none' , skipRotate = 'none' ) ;
    pm.delete ( parCon ) ;
 
    pm.parent ( geo , newTransform ) ;
    geo.freezeTransform ( ) ;
    pm.parent ( geo , w = True ) ;
    
    geoShape = pm.listRelatives ( geo , shapes = True ) ;
    
    pm.parent ( geoShape , newTransform , s = True , r = True );
    pm.delete ( geo ) ;
    pm.rename ( newTransform , geoName ) ;