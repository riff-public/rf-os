import sys ;
import os ;
import pymel.core as pm ;
import maya.cmds as mc ;
main_project = ''

# from context_path import context
# reload(context)

# sim_context = context.SimulationContext()

##################
''' PLUGIN '''
##################

### '''ABC''' ###
if pm.pluginInfo ( 'AbcExport.mll' , q = True , loaded = True ) == True : pass ;
else : pm.loadPlugin ( 'AbcExport.mll' ) ;

if pm.pluginInfo ( 'AbcImport.mll' , q = True , loaded = True ) == True : pass ;
else : pm.loadPlugin ( 'AbcImport.mll' ) ;

### '''matrix''' ###
if pm.pluginInfo ( 'matrixNodes.mll' , q = True , loaded = True ) == True : pass ;
else : pm.loadPlugin ( 'matrixNodes.mll' ) ;

# ### '''yeti''' ###
# if pm.pluginInfo ( 'pgYetiMaya.mll' , q = True , loaded = True ) == True : pass ;
# else : pm.loadPlugin ( 'pgYetiMaya.mll' ) ;

### '''fbx''' ###
if pm.pluginInfo ( 'fbxmaya.mll' , q = True , loaded = True ) == True : pass ;
else : pm.loadPlugin ( 'fbxmaya.mll' ) ;


##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################
''' preferences '''
##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################

def getUsername_cmd ( *args ) :

    if os.environ['RFuser']:
        return os.environ['RFuser']
    else:
        return ''

##################
''' SCENE INIT '''
##################
# turn on undo
# pm.undoInfo ( state = True ) ;
# # set fps to 24
# pm.currentUnit ( time = 'film' ) ;

##################
''' PATH '''
##################
# return : C:/Users/Administrator/Desktop/script/birdScript/library/
selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

mp = selfPath.split('/library')[0] ;
# mp = D:/Dropbox/script/birdScript
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
else : pass ;

##################
''' COMMON UTILITIES '''
##################
def readText ( path , *args ) :
    # return text
    file = open ( path , 'r' ) ;
    text = file.read ( ) ;
    file.close ( ) ;
    return text ;

def writeText ( path , text , *args ) :
    file = open ( path , 'w+' ) ;
    file.write ( text ) ;
    file.close ( ) ;

##################
''' CONFIG '''
##################
# get seting from config.txt
configText = readText ( path = selfPath + '/config.txt' ) ;
exec ( configText ) ;

width = config_dict [ 'width' ] ;
#width += 40.00 ;
title = config_dict [ 'title' ] ;

def updateDate_cmd ( *args ) :

    import time

    date = (time.strftime("%y.%m.%d"))

    configText = readText ( path = selfPath + '/config.txt' ) ;
    exec ( configText ) ;
    config_dict [ 'title' ] = 'character effects utilities %s' % date ;

    writeText ( selfPath + '/config.txt' ,  "config_dict = " + str(config_dict) ) ;

##################
''' UI COMMAND '''
##################

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

def frameLayout_cc ( *args ) :
    pm.window ( 'birdMainUI', e = True , h = 10 ) ;

def birdScript ( *args ) :
    import os ;
    os.startfile ( r'O:\Pipeline\core\rf_maya\rftool\simulation' ) ;

def addButtonToShelf_cmd ( *args ) :
    import library.mainUI.addButtonToShelf ;
    reload ( library.mainUI.addButtonToShelf ) ;
    library.mainUI.addButtonToShelf.run ( ) ;


##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################
''' MAIN UI '''
##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################

def run ( ) :
    
    # check if window exists

    if pm.window ( 'birdMainUI' , exists = True ) :
        pm.deleteUI ( 'birdMainUI' ) ;
        
    # create window 

    window = pm.window ( 'birdMainUI', title =  title , mnb = True , mxb = False , sizeable = False , rtf = True ) ;
    window = pm.window ( 'birdMainUI', e = True , width = width , h = 10 , menuBar=True) ;
    with window :

        mainLayout = pm.rowColumnLayout ( nc = 1 , w = width) ;
        pm.setParent()
        with mainLayout :

            ### title ###
            pm.symbolButton ( image = selfPath + '/' + 'media/title.v02.jpg' , width = width , height = 50 , c = birdScript ) ;

            ### username ###
            # with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/2/6 ) , ( 2 , width/2/6*4 ) , ( 3 , width/2/6  ) ] ):
            #     filler ( ) ;
            #     with pm.optionMenu(label='Projects'):
            #         pm.menuItem(label='Two Heros')
            #         pm.menuItem(label='SevenChicks')
            #         pm.menuItem(label='Pucca')
            #         pm.menuItem(label='CloudMaker')
            #     filler ( ) ;

            ### username ###
            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ]) :

                with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/2/6 ) , ( 2 , width/2/6*4 ) , ( 3 , width/2/6  ) ] ) :
                    filler ( ) ;
                    pm.button ( label = 'add button to shelf' , c = addButtonToShelf_cmd ) ;
                    filler ( ) ;

                with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/2/10*2.5 )  , ( 2 , width/2/10*5 ) , ( 3 , width/2/10*2.5 ) ] ) :
                    pm.text ( label = 'username' ) ;
                    user = getUsername_cmd()
                    pm.textField ( 'username_textField' , text = user, editable=False ) ;
                    
                    # pm.button ( label = 'save' , c =  ) ;

            with pm.rowColumnLayout ( nc = 2 , w = width/2  , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                ##################
                ##################
                ''' PROJECT '''
                ##################
                ##################
                with pm.tabLayout ( 'projects' , innerMarginWidth = width/100 , innerMarginHeight = width/100 ) : 
                    ##################
                    ''' SEVENCHICK '''
                    ##################
                    with pm.rowColumnLayout ( 'ProjectData' , nc = 1 , cw = ( 1 , width/2 ) ) :
                        import library.mainUI.project_data as project_data ;
                        reload ( project_data ) ;
                        main_project = 'project_data' 
                        project_data.insert (project = main_project) ;

                    ##################
                    ''' Dark Horse '''
                    ##################
                    with pm.rowColumnLayout ( 'Dark Horse' , nc = 1 , cw = ( 1 , width/2 ) ) :
                        import library.mainUI.project_darkHorse as project_darkHorse ;
                        reload ( project_darkHorse ) ;
                        main_project = 'Dark Horse'
                        project_darkHorse.insert ( ) ;
                # ##################
                ##################
                ''' UTILITIES '''
                ##################
                ##################
                with pm.tabLayout ( 'utilities' , innerMarginWidth = width/100 , innerMarginHeight = width/100 ) : 

                    ##################
                    ''' SIM '''
                    ##################
                    with pm.rowColumnLayout ( 'sim' , nc = 1 , cw = ( 1 , width/2 ) ) :
                        import library.mainUI.utilities_sim as utilities_sim ;
                        reload ( utilities_sim ) ;
                        utilities_sim.insert ( ) ;
                                            
                    ##################
                    ''' RIG '''
                    ##################
                    with pm.rowColumnLayout ( 'rig' , nc = 1 , cw = ( 1 , width/2 ) ) :
                        import library.mainUI.utilities_rig as utilities_rig ;
                        reload ( utilities_rig ) ;
                        utilities_rig.insert ( ) ;

                    ##################
                    ''' RESOURCE '''
                    ##################
                    with pm.rowColumnLayout ( 'resources' , nc = 1 , cw = ( 1 , width/2 ) ) :

                        with pm.scrollLayout ( w = width/2 , h = 500 ) :                    
                            import library.mainUI.utilities_resources as utilities_resources ;
                            reload ( utilities_resources ) ;
                            utilities_resources.insert ( ) ;              

                    # ---------- #
                    ### scriptTemplate tab ###
                    # ---------- #

                    template_tab = pm.rowColumnLayout (  'template' , nc = 1 , cw = [ ( 1 , width/2 ) ] ) ;
                    with template_tab :

                        import resources.codeReader.core as codeReader ;
                        reload ( codeReader ) ;
                        codeReader.run ( width = width/2 ) ;

          

    pm.showWindow ( window ) ;


# def ChangeProject_cmd(*args):
#     project = pm.optionMenu( 'Project' ,v=True,q = True ) 
#     if project =='select Project':
#         if mc.layout('layoutproject',ex =True):
#             mc.deleteUI('layoutproject', layout = True)
        
#     elif project =='SevenChickMovie':
#         if mc.layout('layoutproject',ex =True):
#             mc.deleteUI('layoutproject', layout = True)
#         with pm.rowColumnLayout ( 'layoutproject' , nc = 1 , cw = ( 1 , width/2 ) ,p='projectLayout') :
#             import library.mainUI.project_sevenChick as project_sevenChick ;
#             reload ( project_sevenChick ) ;
#             main_project = 'SevenChickMovie' 
#             project_sevenChick.insert (project = main_project) ;

#     elif project =='Dark Horse':
#         if mc.layout('layoutproject',ex =True):
#             mc.deleteUI('layoutproject', layout = True)
#         with pm.rowColumnLayout ( 'layoutproject' , nc = 1 , cw = ( 1 , width/2 ) ,p='projectLayout') :
#             import library.mainUI.project_darkHorse as project_darkHorse ;
#             reload ( project_darkHorse ) ;
#             main_project = 'Dark Horse'
#             project_darkHorse.insert ( ) ;
    # elif project =='ThreeEyes':  
    # # with pm.rowColumnLayout ( 'SevenChickMovie' , nc = 1 , cw = ( 1 , width/2 ) ,p='projectLayout') :
    #     import library.mainUI.project_sevenChick as project_sevenChick ;
    #     reload ( project_sevenChick ) ;
    #     main_project = 'SevenChickMovie' 
    #     project_sevenChick.insert (project = main_project) ;

#birdMainUI () ;