##############################################################################################################################
##############################################################################################################################
''' TABLE OF CONTENT '''
##############################################################################################################################
##############################################################################################################################

import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

import general.utilities as utl ;
reload ( utl ) ;

##################
''' PATH '''
##################
# return : C:/Users/Administrator/Desktop/script/birdScript/library/
selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

mp = selfPath.split('/library')[0] ;
# mp = D:/Dropbox/script/birdScript
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
else : pass ;

##################
'''CONFIG'''
##################

def readText ( path , *args ) :
    # return text
    file = open ( path , 'r' ) ;
    text = file.read ( ) ;
    file.close ( ) ;
    return text ;

configText = readText ( path = selfPath + '/config.txt' ) ;
exec ( configText ) ;

globalWidth = config_dict [ 'width' ] ;
layoutWidth = globalWidth/2 ;
width = globalWidth/2*0.935 ;

##################
'''Utils'''
##################

def udim_u1v1_converter_call ( *args ) :
    import model.udim_u1v1_converter.ui as uuc ;
    reload ( uuc ) ;
    uuc_ui = uuc.UI();
    uuc_ui.run();

##############################################################################################################################
##############################################################################################################################
''' [ UIS_IDX ] UI '''
##############################################################################################################################
##############################################################################################################################

##################
''' [ UIF_IDX ] UI FUNCTIONS '''
##################

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

##################
''' [ UIC_IDX ] UI CONTENT '''
##################

def insert ( *args ) :

    with pm.scrollLayout ( w = layoutWidth , h = 450 ) :

        with pm.rowColumnLayout ( nc = 1 , w = width ) :

            with pm.frameLayout ( label = 'model' , collapsable = True , collapse = True , bgc = ( 0 , 0 , 0.2 ) , w = width ) :

                with pm.rowColumnLayout ( nc = 1 , w = width ) :

                    pm.button ( label = 'udim u1v1 converter' , c = udim_u1v1_converter_call , w = width ) ;