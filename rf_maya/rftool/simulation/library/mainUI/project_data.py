##############################################################################################################################
##############################################################################################################################
''' TABLE OF CONTENT '''
##############################################################################################################################
##############################################################################################################################

'''

navigate with "ctrl + f"

[ ISL_IDX ] INFO SECTION
[ DOC_IDX ] DOC SECTION
[ UTL_IDX ] UTILITIES SECTION
[ NAV_IDX ] NAVIGATION SECTION
[ NMC_IDX ] NAMING CONVENSTION SECTION

[ UIS_IDX ] UI
    [ UIF_IDX ] UI FUNCTIONS
    [ UIC_IDX ] UI CONTENT

'''

import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

##################
''' PATH '''
##################
# return : C:/Users/Administrator/Desktop/script/birdScript/library/
selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;



##################
'''CONFIG'''
##################

def readText ( path , *args ) :
    # return text
    file = open ( path , 'r' ) ;
    text = file.read ( ) ;
    file.close ( ) ;
    return text ;

configText = readText ( path = selfPath + '/config.txt' ) ;
exec ( configText ) ;

globalWidth = config_dict [ 'width' ] ;
layoutWidth = globalWidth/2 ;
width = globalWidth/2*0.95 ;


def setResolution_cmd  ( *args ) :

    defaultResolution = pm.general.PyNode ( 'defaultResolution' ) ;

    defaultResolution.aspectLock.set ( 0 ) ;
    defaultResolution.width.set ( 3840 ) ;
    defaultResolution.height.set ( 1632 ) ;
    defaultResolution.dotsPerInch.set ( 72 ) ;
    defaultResolution.deviceAspectRatio.set( 2.35294127464 );

#updateMayaSoftwareDeviceAspectRatio;
#updateMayaSoftwarePixelAspectRatio;

    pm.currentUnit ( time = 'film' ) ;

    pm.select ( defaultResolution ) ;

    #pm.mel.eval ( 'ToggleAttributeEditor' ) ;
    #pm.mel.eval ( 'checkAspectLockHeight2 "defaultResolution"' ) ;
    #pm.mel.eval ( 'checkAspectLockWidth2 "defaultResolution"' ) ;

def resolutionGate_cmd ( *args ) :

    import maya.OpenMaya as OpenMaya
    import maya.OpenMayaUI as OpenMayaUI

    view = OpenMayaUI.M3dView.active3dView ( ) ;
    cam = OpenMaya.MDagPath()
    view.getCamera(cam)
    camPath = cam.fullPathName()

    camNode = pm.general.PyNode ( camPath ) ;

    status = pm.camera ( camPath , q = True , displayResolution = True )

    if camNode.overscan.get() != 1 :
        camNode.overscan.unlock() ;
        pm.disconnectAttr ( camNode.overscan ) ;

    if not status :
        pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = True , overscan = 1.0 , lockTransform = True ) ;
    else :
        pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = False , overscan = 1.0 , lockTransform = False ) ;

    camera = pm.general.PyNode ( camPath ) ;
    #cameraShape = pm.listRelatives ( camera , shapes = True ) [0] ;
    camera.displayGateMaskColor.set ( 0 , 0 , 0 ) ;
    camera.displayGateMaskOpacity.set ( 1 ) ;

    #setAttr "CAM:cam0030Shape.displayGateMaskColor" -type double3 0 0 0 ;
    #setAttr CAM:cam0030Shape.displayGateMaskOpacity 1;

def toggleAntiAliasing_cmd ( *args ) :
    
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    
    if hardwareRenderingGlobals.multiSampleEnable.get() == 0 :
        hardwareRenderingGlobals.multiSampleEnable.set(1) ;
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = True ) ;

    elif hardwareRenderingGlobals.multiSampleEnable.get() == 1 :
        hardwareRenderingGlobals.multiSampleEnable.set(0) ;
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = False ) ;

    sampleCountChange_cmd ( ) ;

def sampleCountChange_cmd ( *args ) :
    
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    
    multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , q = True , value = True ) ;
    multiSampleCount = int ( multiSampleCount ) ;
    hardwareRenderingGlobals.multiSampleCount.set ( multiSampleCount ) ;

def setDefaultMultiSampling_cmd ( *args ) :
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    pm.optionMenu ( 'sampleCount_ptm' , e = True , enable = hardwareRenderingGlobals.multiSampleEnable.get() ) ;

##################
##################
''' [ DOC_IDX ] DOC SECTION '''
##################
##################

def sevenChick_workflow_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/presentation/d/1-nQLY75hsQQE-y3dqvKCH-55O-zk8JStChUts062mVc/edit#slide=id.gc6f59039d_0_0' ) ;

def shotgun_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://riffanimation.shotgunstudio.com/projects/' ) ;

def autonaming_cmd ( *args ) :
    from rf_maya.rftool.sim.late_tool import autoProjectLocal
    reload(autoProjectLocal)
    autoProjectLocal.run()


def autonaming_toShelf_cmd ( *args ) :

    # self_path = D:/Dropbox/script/birdScript/projects/twoHeroes/

    image_path = "D:/script_server/core/rf_maya/rftool/sim/late_tool/media/autonaming_icon.png"

    commandHeader = '''
import maya.cmds as mc
import sys
mp = "D:/script_server/core/rf_maya/rftool/sim/late_tool" ;
if not mp in sys.path :
    sys.path.insert( 0 , mp );
    
import autoProjectLocal as auto
reload( auto )

auto.run()
'''
    mel_cmd = '''
global string $gShelfTopLevel;
string $shelves = `tabLayout -q -selectTab $gShelfTopLevel`;
'''
    currentShelf = pm.mel.eval ( mel_cmd );
    pm.shelfButton ( style = 'iconOnly' , image = image_path , command = commandHeader , parent = currentShelf ) ;

def sevenChick_cacheOutUtil_cmd ( *args ) :
    import project.sevenChickMovie.cacheOutUtilities as cou ;
    reload ( cou ) ;
    cou.animCacheOutUI ( ) ;

def cache_out_cmd ( *args ) :
    from rf_maya.rftool.sim.late_tool import UI_Preroll
    reload(UI_Preroll)
    
    UI_Preroll.run ( ) ;

def browseTechAnim_cmd ( *args ) :
    import os ;
    os.startfile ( r'N:\Staff\_sim' ) ;


def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

def setFrameRate ( *args ) :
    ###June Edit###
    # set fps to 24
    pm.currentUnit ( time = 'film' ) ;
    
def insert ( project='', *args ) :

    with pm.scrollLayout ( w = layoutWidth , h = 500 ) :

        with pm.rowColumnLayout (  nc = 5 , cw = [ ( 1 , width*0.05 ) , ( 2 , width*0.55 ) , ( 3 , width*0.05 ) , ( 4 , width*0.30 ) , ( 5 , width*0.05 )] ) :

            filler ( ) ;
            pm.text ( label = 'resolution' ) ;
            filler ( ) ;
            pm.text ( label = 'fps' ) ;
            filler ( ) ;

            filler ( ) ;      
            with pm.rowColumnLayout (  nc = 2 , cw = [ ( 1 , width*0.55/2 ) , ( 2 , width*0.55/2 ) ] ) :       
                pm.intField ( v = 3840 ) ;
                pm.intField ( v = 1632 ) ;
            filler ( ) ;
            pm.intField ( v = 24 ) ;

        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 )  , ( 2 , width/2 ) ] ) :
            pm.button ( label = 'set scene' , c = setResolution_cmd   ) ;
            pm.button ( label = 'resolution gate' , c = resolutionGate_cmd  ) ;

     
            with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :
                filler ( ) ;
                pm.button ( label = 'toggle anti-aliasing' , w = width/2 , c = toggleAntiAliasing_cmd ) ;

            with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :
                
                pm.text ( label = 'sample count' , w = width/2 ) ;

                with pm.optionMenu( 'sampleCount_ptm' , w = width/2 , changeCommand = sampleCountChange_cmd ) :
                    pm.menuItem ( label= '16' ) ;
                    pm.menuItem ( label= '8' ) ;
                    pm.menuItem ( label= '4' ) ;


        separator ( ) ;

                
        pm.separator ( vis = True , h = 20 ) ;
        pm.button ( label = 'Sim Workflow' ,  c = sevenChick_workflow_cmd ,width = width    ) ;

       
        with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/6*5 ) , ( 2 , width/6 ) ] ) :
            pm.button ( label = 'autonaming project' , c = autonaming_cmd , bgc = ( 0 , 1 , 1 ) , w = width/6*5) ;
            pm.symbolButton ( image = selfPath + '/' + 'media/toShelf.png' , width = width/6 , c = autonaming_toShelf_cmd ) ;

        pm.separator ( vis = True , h = 10 ) ;    
        with pm.rowColumnLayout ( nc = 1 , w = width ) :
            pm.text ( label = 'Fix anim' ) ;

            with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width) ] ) :
                pm.button ( label = 'cache Out' , c = cache_out_cmd , bgc = ( 1 , 0.9 , 1 ) , w = width/6*5) ;
          
        pm.separator ( vis = True , h = 10 ) ;

        
        pm.button ( label = 'staff' , c = browseTechAnim_cmd  , w = width ) ;
        pm.button ( label = 'SHOTGUN' , c = shotgun_cmd  , w = width ) ;

        separator ( ) ;
