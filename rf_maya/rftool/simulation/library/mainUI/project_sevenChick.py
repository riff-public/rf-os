##############################################################################################################################
##############################################################################################################################
''' TABLE OF CONTENT '''
##############################################################################################################################
##############################################################################################################################

'''

navigate with "ctrl + f"

[ ISL_IDX ] INFO SECTION
[ DOC_IDX ] DOC SECTION
[ UTL_IDX ] UTILITIES SECTION
[ NAV_IDX ] NAVIGATION SECTION
[ NMC_IDX ] NAMING CONVENSTION SECTION

[ UIS_IDX ] UI
    [ UIF_IDX ] UI FUNCTIONS
    [ UIC_IDX ] UI CONTENT

'''

import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

##################
''' PATH '''
##################
# return : C:/Users/Administrator/Desktop/script/birdScript/library/
selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

mp = selfPath.split('/library')[0] ;
# mp = D:/Dropbox/script/birdScript
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
else : pass ;

##################
'''CONFIG'''
##################

def readText ( path , *args ) :
    # return text
    file = open ( path , 'r' ) ;
    text = file.read ( ) ;
    file.close ( ) ;
    return text ;

configText = readText ( path = selfPath + '/config.txt' ) ;
exec ( configText ) ;

globalWidth = config_dict [ 'width' ] ;
layoutWidth = globalWidth/2 ;
width = globalWidth/2*0.95 ;

##################
##################
''' [ ISL_IDX ] INFO SECTION '''
##################
##################

def sevenChick_setResolution_cmd ( *args ) :

    defaultResolution = pm.general.PyNode ( 'defaultResolution' ) ;

    defaultResolution.aspectLock.set ( 0 ) ;
    defaultResolution.width.set ( 3840 ) ;
    defaultResolution.height.set ( 1632 ) ;
    defaultResolution.dotsPerInch.set ( 72 ) ;
    defaultResolution.deviceAspectRatio.set( 2.35294127464 );

#updateMayaSoftwareDeviceAspectRatio;
#updateMayaSoftwarePixelAspectRatio;

    pm.currentUnit ( time = 'film' ) ;

    pm.select ( defaultResolution ) ;

    #pm.mel.eval ( 'ToggleAttributeEditor' ) ;
    #pm.mel.eval ( 'checkAspectLockHeight2 "defaultResolution"' ) ;
    #pm.mel.eval ( 'checkAspectLockWidth2 "defaultResolution"' ) ;

def resolutionGate_cmd ( *args ) :

    import maya.OpenMaya as OpenMaya
    import maya.OpenMayaUI as OpenMayaUI

    view = OpenMayaUI.M3dView.active3dView ( ) ;
    cam = OpenMaya.MDagPath()
    view.getCamera(cam)
    camPath = cam.fullPathName()

    camNode = pm.general.PyNode ( camPath ) ;

    status = pm.camera ( camPath , q = True , displayResolution = True )

    if camNode.overscan.get() != 1 :
        camNode.overscan.unlock() ;
        pm.disconnectAttr ( camNode.overscan ) ;

    if not status :
        pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = True , overscan = 1.0 , lockTransform = True ) ;
    else :
        pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = False , overscan = 1.0 , lockTransform = False ) ;

    camera = pm.general.PyNode ( camPath ) ;
    #cameraShape = pm.listRelatives ( camera , shapes = True ) [0] ;
    camera.displayGateMaskColor.set ( 0 , 0 , 0 ) ;
    camera.displayGateMaskOpacity.set ( 1 ) ;

    #setAttr "CAM:cam0030Shape.displayGateMaskColor" -type double3 0 0 0 ;
    #setAttr CAM:cam0030Shape.displayGateMaskOpacity 1;

def toggleAntiAliasing_cmd ( *args ) :
    
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    
    if hardwareRenderingGlobals.multiSampleEnable.get() == 0 :
        hardwareRenderingGlobals.multiSampleEnable.set(1) ;
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = True ) ;

    elif hardwareRenderingGlobals.multiSampleEnable.get() == 1 :
        hardwareRenderingGlobals.multiSampleEnable.set(0) ;
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = False ) ;

    sampleCountChange_cmd ( ) ;

def sampleCountChange_cmd ( *args ) :
    
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    
    multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , q = True , value = True ) ;
    multiSampleCount = int ( multiSampleCount ) ;
    hardwareRenderingGlobals.multiSampleCount.set ( multiSampleCount ) ;

def setDefaultMultiSampling_cmd ( *args ) :
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    pm.optionMenu ( 'sampleCount_ptm' , e = True , enable = hardwareRenderingGlobals.multiSampleEnable.get() ) ;

##################
##################
''' [ DOC_IDX ] DOC SECTION '''
##################
##################

def sevenChick_WIPSequences_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1218t3_T5wtQ678vSW5FHuy0bEPSdETS3HO-L5lmozeA/edit?ts=5c346e1c#gid=1709744959' ) ;

def sevenChick_workflow_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/presentation/d/1-nQLY75hsQQE-y3dqvKCH-55O-zk8JStChUts062mVc/edit#slide=id.gc6f59039d_0_0' ) ;

def shotgun_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://riffanimation.shotgunstudio.com/page/7429' ) ;

def workplace_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://riffstudio.facebook.com' ) ;

def sevenChick_completedSequences_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1218t3_T5wtQ678vSW5FHuy0bEPSdETS3HO-L5lmozeA/edit?ts=5c346e1c#gid=783098080' ) ;

def sevenChick_simulationBreakdown_cmd ( *args ) :
	import general.utilities as utl ;
	reload ( utl ) ;
	utl.openLink ( link = r'https://docs.google.com/spreadsheets/d/1DQe23etWowANUwMmBvlNiUtY5PHR8uapLzvWxRAxO2A/edit?ts=5b222473#gid=2123138132' )  ;

def sevenChick_simulationBreakdownBackup_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = r'https://docs.google.com/spreadsheets/d/1y7yx7s3YZCgV35NOH6Sc187OQsm6bpkOw_fPxSY-p4w/edit#gid=1866481003' )  ;

def sevenChick_openLayoutDoc_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1Aa8q0E6L8LZ-uFeN58gPKKH7ybQhGI_ee5twAubgdwI/edit#gid=0' ) ;

def sevenChick_openFacebook_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://www.facebook.com/groups/665802640258497/' ) ;

def sevenChick_openProjectTimeline_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1ywLsNq_yuS63o6uosc8Tzs66EoO1XeJ-9DljH4uJmIg/' ) ;

def sevenChick_openTeaserBreakdownDoc_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1j-spZIgdJoUcOys0PbWttzd5wtoXMmjxdhaZb73h-gI/edit?ts=59d70bfc#gid=1097980972' ) ;

def sevenChick_openShotList_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/19_B9R0fvifJro6eycBZcYjrvRva2NZZhqQSTxu4slWM/' ) ;

def sevenChick_open_SLR_timeline_cmd ( *args ) :
    import os ;
    os.startfile ( r'P:\Doublemonkeys\all\doc\timeline\SLR_timeline' ) ;

def sevenChick_open_SLR_April2018 ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/17propT0lLUamureakZ1pCb5G4IiJWZNdt71vB_xdhBo/' ) ;\

def twoHeroes_cacheOutUtilGen2_cmd ( *args ) :
    import projects.twoHeroes.cacheOutUtilities_generation2.core as cacheOutUtilities_generation2 ;
    reload ( cacheOutUtilities_generation2 ) ;
    cacheOutUtilities_generation2.run ( ) ;
    

##################
##################
''' [ UTL_IDX ] UTILITIES SECTION '''
##################
##################

def sevenChick_autoNaming_cmd ( *args )  :
    import project.sevenChickMovie.autonaming as thAN ;
    reload ( thAN ) ;
    thAN.TLo2H_ANUI ( ) ;

def sevenChick_autoNamingGeneration1_cmd ( *args )  :
    import project.sevenChickMovie.autonaming_generation1 as thAN ;
    reload ( thAN ) ;
    thAN.TLo2H_ANUI ( ) ;

def sevenChick_autonamingGeneration2_cmd ( *args ) :
    import projects.sevenChickMovie.autonaming_generation2 as autonaming_generation2 ;
    reload ( autonaming_generation2 ) ;
    project = 'SevenChickMovie'
    autonaming_generation2.run ( project ) ;

def sevenChick_autonamingGeneration2_toShelf_cmd ( *args ) :
    import projects.sevenChickMovie.autonaming_generation2 as autonaming_generation2 ;
    reload ( autonaming_generation2 ) ;
    autonaming_generation2.toShelf ( ) ;

def sevenChick_playblastViewer_cmd ( *args ) :
    import projects.sevenChickMovie.playblastViewer as sevenChick_playblastViewer ;
    reload ( sevenChick_playblastViewer ) ;
    sevenChick_playblastViewer.run (  ) ;

def sevenChick_playblastViewer_toShelf_cmd ( *args ) :
    import projects.sevenChickMovie.playblastViewer as sevenChick_playblastViewer ;
    reload ( sevenChick_playblastViewer ) ;
    sevenChick_playblastViewer.toShelf ( ) ;

def sevenChick_fileManager_cmd ( *args ) :

    rp = ('O:/Pipeline/core/maya') ;

    if not rp in sys.path :
        sys.path.insert ( 0 , rp ) ;

    from rftool.file_manager import fm_express_app as fm_app ;

    reload(fm_app) ;
    reload(fm_app.file_utils) ;
    reload(fm_app.path_info) ;

    reload(fm_app.config) ;

    reload(fm_app.path_info) ;
    fm_app.show() ;

def sevenChick_cacheOutUtil_cmd ( *args ) :
    import project.sevenChickMovie.cacheOutUtilities as cou ;
    reload ( cou ) ;
    cou.animCacheOutUI ( ) ;

def sevenChick_cacheOutUtilGen2_cmd ( *args ) :
    import projects.sevenChickMovie.cacheOutUtilities_generation2.core as cacheOutUtilities ;
    print 100000
    
    reload ( cacheOutUtilities ) ;
    cacheOutUtilities.run ( ) ;

def sevenChick_layoutOrganizer_cmd ( *args ) :
    import project.sevenChickMovie.layoutOrganizer as sevenChick_layoutOrganizer ;
    reload ( sevenChick_layoutOrganizer ) ;
    sevenChick_layoutOrganizer.run ( ) ;

def sevenChick_layoutOrganizer_toShelf_cmd ( *args ) :
    import project.sevenChickMovie.layoutOrganizer as sevenChick_layoutOrganizer ;
    reload ( sevenChick_layoutOrganizer ) ;
    sevenChick_layoutOrganizer.toShelf ( ) ;

def sevenChick_fee_colorAssigner_cmd ( *args ) :
    import external.RiFF.fee_colorAssigner.core as fee_colorAssigner ;
    reload ( fee_colorAssigner ) ;
    fee_colorAssigner.run ( ) ;

def sevenChick_groomShaderImporter_caller ( *args ) :
	import project.sevenChickMovie.hair_groomShaderUtil.core as sevenChick_hair_groomShaderUtil ;
	reload ( sevenChick_hair_groomShaderUtil ) ;
	sevenChick_hair_groomShaderUtil.run() ;

# def sevenChick_xGenShaderImporter_cmd ( *args ) :
#     import project.sevenChickMovie.xgenShaderImporter as sevenChick_xgenShaderImporter ;
#     reload ( sevenChick_xgenShaderImporter ) ;
#     sevenChick_xgenShaderImporter.run ( ) ;

def lillyCollider_cmd ( *args ) :
    import project.sevenChickMovie.lillyCollider as lillyCollider ;
    reload ( lillyCollider ) ;
    lillyCollider.run ( )  ;

def godEmperorSlideOnMesh_cmd ( *args ) :
    import project.sevenChickMovie.specific_godEmperorSlideOnMesh.core as specific_godEmperorSlideOnMesh ;
    reload ( specific_godEmperorSlideOnMesh ) ;
    specific_godEmperorSlideOnMesh.run ( )  ;

def sevenChick_detachRig_run ( *args ) :
    import project.sevenChickMovie.rig_detachRig.core as sevenChick_rig_detachRig ;
    reload ( sevenChick_rig_detachRig ) ;
    sevenChick_rig_detachRig.run ( ) ;

def sevenChick_positionPuller_run ( *args ) :
	import project.sevenChickMovie.setdress_positionPuller.core as setdress_positionPuller ;
	reload ( setdress_positionPuller ) ;
	setdress_positionPuller.run() ;

##################
##################
''' [ NAV_IDX ] NAVIGATION SECTION '''
##################
##################

def browseTechAnim_cmd ( *args ) :
    import os ;
    # os.startfile ( r'P:\Temp\Shared\_techAnim' ) ;
    os.startfile ( r'N:\Staff\_techAnim' ) ;

def SevenChick_browseDaily_cmd ( *args ) :
    import os ;
    os.startfile ( r'P:\SevenChickMovie\daily\sim' ) ;



def SevenChick_browseScene_cmd ( *args ) :
    import os ;
    os.startfile ( r'P:\SevenChickMovie\scene\work' ) ;



##############################################################################################################################
##############################################################################################################################
''' [ UIS_IDX ] UI '''
##############################################################################################################################
##############################################################################################################################

def initialized_cmd ( *args ) :
    setDefaultMultiSampling_cmd ( ) ;

##################
''' [ UIF_IDX ] UI FUNCTIONS '''
##################

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

##################
''' [ UIC_IDX ] UI CONTENT '''
##################

def setFrameRate ( *args ) :
    ###June Edit###
    # set fps to 24
    pm.currentUnit ( time = 'film' ) ;
    
def insert ( project='', *args ) :

    ##################
    ##################
    ''' [ ISL_IDX ] INFO SECTION '''
    ##################
    ##################

    with pm.scrollLayout ( w = layoutWidth , h = 500 ) :

        with pm.rowColumnLayout (  nc = 5 , cw = [ ( 1 , width*0.05 ) , ( 2 , width*0.55 ) , ( 3 , width*0.05 ) , ( 4 , width*0.30 ) , ( 5 , width*0.05 )] ) :

            filler ( ) ;
            pm.text ( label = 'resolution' ) ;
            filler ( ) ;
            pm.text ( label = 'fps' ) ;
            filler ( ) ;

            filler ( ) ;      
            with pm.rowColumnLayout (  nc = 2 , cw = [ ( 1 , width*0.55/2 ) , ( 2 , width*0.55/2 ) ] ) :       
                pm.intField ( v = 3840 ) ;
                pm.intField ( v = 1632 ) ;
            filler ( ) ;
            pm.intField ( v = 24 ) ;

        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 )  , ( 2 , width/2 ) ] ) :
            pm.button ( label = 'set scene' , c = sevenChick_setResolution_cmd  ) ;
            pm.button ( label = 'resolution gate' , c = resolutionGate_cmd  ) ;

     
            with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :
                filler ( ) ;
                pm.button ( label = 'toggle anti-aliasing' , w = width/2 , c = toggleAntiAliasing_cmd ) ;

            with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :
                
                pm.text ( label = 'sample count' , w = width/2 ) ;

                with pm.optionMenu( 'sampleCount_ptm' , w = width/2 , changeCommand = sampleCountChange_cmd ) :
                    pm.menuItem ( label= '16' ) ;
                    pm.menuItem ( label= '8' ) ;
                    pm.menuItem ( label= '4' ) ;


        separator ( ) ;

        ##################
        ##################
        ''' [ DOC_IDX ] DOC SECTION '''
        ##################
        ##################

        with pm.rowColumnLayout ( nc = 1 , w = width ) :
            pm.text ( label = 'Simulation Document' ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width / 2 ) ] ) :
                pm.button ( label = 'WIP' ,  c = sevenChick_WIPSequences_cmd , bgc = ( 1 , 1 , 0 ) , w = width/2 ) ;
                pm.button ( label = 'Set Up' ,  c = sevenChick_completedSequences_cmd , w = width/2 ) ;

        
        
        ##################
        ##################
        ''' [ UTL_IDX ] UTILITIES SECTION '''
        ##################
        ##################
                
        pm.separator ( vis = True , h = 10 ) ;
        pm.button ( label = 'Sim Workflow' ,  c = sevenChick_workflow_cmd ,width = width    ) ;

       
        with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/6*5 ) , ( 2 , width/6 ) ] ) :
            pm.button ( label = 'autonaming generation 2' , c = sevenChick_autonamingGeneration2_cmd , bgc = ( 0 , 1 , 1 ) , w = width/6*5) ;
            pm.symbolButton ( image = selfPath + '/' + 'media/toShelf.png' , width = width/6 , c = sevenChick_autonamingGeneration2_toShelf_cmd ) ;

        with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/6*5 ) , ( 2 , width/6 ) ] ) :
            pm.button ( label = 'cacheOutUtilGen2' , c = sevenChick_cacheOutUtilGen2_cmd , bgc = ( 0 , 1 , 1 ) , w = width/6*5) ;
            pm.symbolButton ( image = selfPath + '/' + 'media/toShelf.png' , width = width/6 , c = sevenChick_autonamingGeneration2_toShelf_cmd ) ;
        pm.separator ( vis = True , h = 10 ) ;

        
        ##################
        ##################
        ''' [ NAV_IDX ] NAVIGATION SECTION '''
        ##################
        ##################

        pm.button ( label = 'tech anim share' , c = browseTechAnim_cmd  , w = width ) ;

        with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
        # Two Heroes Browse

            pm.button ( label = 'daily' , c = SevenChick_browseDaily_cmd , w = width/2 ,bgc = ( 1 , 0.4 , 0.7 )) ;
            pm.button ( label = 'project scene' ,  c = SevenChick_browseScene_cmd , w = width/2 ) ;
        
        pm.button ( label = 'SHOTGUN' , c = shotgun_cmd  , w = width ) ;
        pm.button ( label = 'Workplace' , c = workplace_cmd  , w = width ) ;
        separator ( ) ;
