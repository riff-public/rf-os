##############################################################################################################################
##############################################################################################################################
''' TABLE OF CONTENT '''
##############################################################################################################################
##############################################################################################################################

'''

navigate with "ctrl + f"

[ ISL_IDX ] INFO SECTION
[ DOC_IDX ] DOC SECTION
[ UTL_IDX ] UTILITIES SECTION
[ NAV_IDX ] NAVIGATION SECTION
[ NMC_IDX ] NAMING CONVENSTION SECTION

[ UIS_IDX ] UI
    [ UIF_IDX ] UI FUNCTIONS
    [ UIC_IDX ] UI CONTENT

'''

import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

##################
''' PATH '''
##################
# return : C:/Users/Administrator/Desktop/script/birdScript/library/
selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

mp = selfPath.split('/library')[0] ;
# mp = D:/Dropbox/script/birdScript
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
else : pass ;

##################
'''CONFIG'''
##################

def readText ( path , *args ) :
    # return text
    file = open ( path , 'r' ) ;
    text = file.read ( ) ;
    file.close ( ) ;
    return text ;

configText = readText ( path = selfPath + '/config.txt' ) ;
exec ( configText ) ;

globalWidth = config_dict [ 'width' ] ;
layoutWidth = globalWidth/2 ;
width = globalWidth/2*0.95 ;

##################
##################
''' [ ISL_IDX ] INFO SECTION '''
##################
##################

def twoHeroes_setResolution_cmd ( *args ) :

    defaultResolution = pm.general.PyNode ( 'defaultResolution' ) ;

    defaultResolution.aspectLock.set ( 0 ) ;
    defaultResolution.width.set ( 3840 ) ;
    defaultResolution.height.set ( 1632 ) ;
    defaultResolution.dotsPerInch.set ( 72 ) ;
    defaultResolution.deviceAspectRatio.set( 2.35294127464 );

#updateMayaSoftwareDeviceAspectRatio;
#updateMayaSoftwarePixelAspectRatio;

    pm.currentUnit ( time = 'film' ) ;

    pm.select ( defaultResolution ) ;

    #pm.mel.eval ( 'ToggleAttributeEditor' ) ;
    #pm.mel.eval ( 'checkAspectLockHeight2 "defaultResolution"' ) ;
    #pm.mel.eval ( 'checkAspectLockWidth2 "defaultResolution"' ) ;

def resolutionGate_cmd ( *args ) :

    import maya.OpenMaya as OpenMaya
    import maya.OpenMayaUI as OpenMayaUI

    view = OpenMayaUI.M3dView.active3dView ( ) ;
    cam = OpenMaya.MDagPath()
    view.getCamera(cam)
    camPath = cam.fullPathName()

    camNode = pm.general.PyNode ( camPath ) ;

    status = pm.camera ( camPath , q = True , displayResolution = True )

    if camNode.overscan.get() != 1 :
        camNode.overscan.unlock() ;
        pm.disconnectAttr ( camNode.overscan ) ;

    if not status :
        pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = True , overscan = 1.0 , lockTransform = True ) ;
    else :
        pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = False , overscan = 1.0 , lockTransform = False ) ;

    camera = pm.general.PyNode ( camPath ) ;
    #cameraShape = pm.listRelatives ( camera , shapes = True ) [0] ;
    camera.displayGateMaskColor.set ( 0 , 0 , 0 ) ;
    camera.displayGateMaskOpacity.set ( 1 ) ;

    #setAttr "CAM:cam0030Shape.displayGateMaskColor" -type double3 0 0 0 ;
    #setAttr CAM:cam0030Shape.displayGateMaskOpacity 1;

def toggleAntiAliasing_cmd ( *args ) :
    
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    
    if hardwareRenderingGlobals.multiSampleEnable.get() == 0 :
        hardwareRenderingGlobals.multiSampleEnable.set(1) ;
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = True ) ;

    elif hardwareRenderingGlobals.multiSampleEnable.get() == 1 :
        hardwareRenderingGlobals.multiSampleEnable.set(0) ;
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = False ) ;

    sampleCountChange_cmd ( ) ;

def sampleCountChange_cmd ( *args ) :
    
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    
    multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , q = True , value = True ) ;
    multiSampleCount = int ( multiSampleCount ) ;
    hardwareRenderingGlobals.multiSampleCount.set ( multiSampleCount ) ;

def setDefaultMultiSampling_cmd ( *args ) :
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    pm.optionMenu ( 'sampleCount_ptm' , e = True , enable = hardwareRenderingGlobals.multiSampleEnable.get() ) ;

##################
##################
''' [ DOC_IDX ] DOC SECTION '''
##################
##################

def twoHeroes_WIPSequences_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/14Lawz9PybwGsx1Y9oZauonv44UzKEZFSzvH9tDKeb84/edit?usp=sharing' ) ;

def twoHeroes_completedSequences_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1H6tig3gw843nu0IRl7b7LnGcpoJ4l1K9O9yeSImumwY/edit?usp=sharing' ) ;

def twoHeroes_simulationBreakdown_cmd ( *args ) :
	import general.utilities as utl ;
	reload ( utl ) ;
	utl.openLink ( link = r'https://docs.google.com/spreadsheets/d/1DQe23etWowANUwMmBvlNiUtY5PHR8uapLzvWxRAxO2A/edit?ts=5b222473#gid=2123138132' )  ;

def twoHeroes_simulationBreakdownBackup_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = r'https://docs.google.com/spreadsheets/d/1y7yx7s3YZCgV35NOH6Sc187OQsm6bpkOw_fPxSY-p4w/edit#gid=1866481003' )  ;

def twoHeroes_openLayoutDoc_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1Aa8q0E6L8LZ-uFeN58gPKKH7ybQhGI_ee5twAubgdwI/edit#gid=0' ) ;

def twoHeroes_openFacebook_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://www.facebook.com/groups/665802640258497/' ) ;

def twoHeroes_openProjectTimeline_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1ywLsNq_yuS63o6uosc8Tzs66EoO1XeJ-9DljH4uJmIg/' ) ;

def twoHeroes_openTeaserBreakdownDoc_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1j-spZIgdJoUcOys0PbWttzd5wtoXMmjxdhaZb73h-gI/edit?ts=59d70bfc#gid=1097980972' ) ;

def twoHeroes_openShotList_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/19_B9R0fvifJro6eycBZcYjrvRva2NZZhqQSTxu4slWM/' ) ;

def twoHeroes_open_SLR_timeline_cmd ( *args ) :
    import os ;
    os.startfile ( r'P:\Doublemonkeys\all\doc\timeline\SLR_timeline' ) ;

def twoHeroes_open_SLR_April2018 ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/17propT0lLUamureakZ1pCb5G4IiJWZNdt71vB_xdhBo/' ) ;
    

##################
##################
''' [ UTL_IDX ] UTILITIES SECTION '''
##################
##################

def twoHeroes_autoNaming_cmd ( *args )  :
    import projects.twoHeroes.autonaming as thAN ;
    reload ( thAN ) ;
    thAN.TLo2H_ANUI ( ) ;

def twoHeroes_autoNamingGeneration1_cmd ( *args )  :
    import projects.twoHeroes.autonaming_generation1 as thAN ;
    reload ( thAN ) ;
    thAN.TLo2H_ANUI ( ) ;

def twoHeroes_autonamingGeneration2_cmd ( *args ) :
    import projects.twoHeroes.autonaming_generation2 as autonaming_generation2 ;
    reload ( autonaming_generation2 ) ;
    autonaming_generation2.run ( ) ;

def twoHeroes_autonamingGeneration2_toShelf_cmd ( *args ) :
    import projects.twoHeroes.autonaming_generation2 as autonaming_generation2 ;
    reload ( autonaming_generation2 ) ;
    autonaming_generation2.toShelf ( ) ;

def twoHeroes_playblastViewer_cmd ( *args ) :
    import projects.twoHeroes.playblastViewer as twoHeroes_playblastViewer ;
    reload ( twoHeroes_playblastViewer ) ;
    twoHeroes_playblastViewer.run ( ) ;

def twoHeroes_playblastViewer_toShelf_cmd ( *args ) :
    import projects.twoHeroes.playblastViewer as twoHeroes_playblastViewer ;
    reload ( twoHeroes_playblastViewer ) ;
    twoHeroes_playblastViewer.toShelf ( ) ;

def twoHeroes_fileManager_cmd ( *args ) :

    rp = ('O:/Pipeline/core/maya') ;

    if not rp in sys.path :
        sys.path.insert ( 0 , rp ) ;

    from rftool.file_manager import fm_express_app as fm_app ;

    reload(fm_app) ;
    reload(fm_app.file_utils) ;
    reload(fm_app.path_info) ;

    reload(fm_app.config) ;

    reload(fm_app.path_info) ;
    fm_app.show() ;

def twoHeroes_cacheOutUtil_cmd ( *args ) :
    import projects.twoHeroes.cacheOutUtilities as cou ;
    reload ( cou ) ;
    cou.animCacheOutUI ( ) ;

def twoHeroes_cacheOutUtilGen2_cmd ( *args ) :
    import projects.twoHeroes.cacheOutUtilities_generation2.core as cacheOutUtilities_generation2 ;
    reload ( cacheOutUtilities_generation2 ) ;
    cacheOutUtilities_generation2.run ( ) ;

def twoHeroes_layoutOrganizer_cmd ( *args ) :
    import projects.twoHeroes.layoutOrganizer as twoHeroes_layoutOrganizer ;
    reload ( twoHeroes_layoutOrganizer ) ;
    twoHeroes_layoutOrganizer.run ( ) ;

def twoHeroes_layoutOrganizer_toShelf_cmd ( *args ) :
    import projects.twoHeroes.layoutOrganizer as twoHeroes_layoutOrganizer ;
    reload ( twoHeroes_layoutOrganizer ) ;
    twoHeroes_layoutOrganizer.toShelf ( ) ;

def twoHeroes_fee_colorAssigner_cmd ( *args ) :
    import external.RiFF.fee_colorAssigner.core as fee_colorAssigner ;
    reload ( fee_colorAssigner ) ;
    fee_colorAssigner.run ( ) ;

def twoHeroes_groomShaderImporter_caller ( *args ) :
	import projects.twoHeroes.hair_groomShaderUtil.core as twoHeroes_hair_groomShaderUtil ;
	reload ( twoHeroes_hair_groomShaderUtil ) ;
	twoHeroes_hair_groomShaderUtil.run() ;

# def twoHeroes_xGenShaderImporter_cmd ( *args ) :
#     import projects.twoHeroes.xgenShaderImporter as twoHeroes_xgenShaderImporter ;
#     reload ( twoHeroes_xgenShaderImporter ) ;
#     twoHeroes_xgenShaderImporter.run ( ) ;

def lillyCollider_cmd ( *args ) :
    import projects.twoHeroes.lillyCollider as lillyCollider ;
    reload ( lillyCollider ) ;
    lillyCollider.run ( )  ;

def godEmperorSlideOnMesh_cmd ( *args ) :
    import projects.twoHeroes.specific_godEmperorSlideOnMesh.core as specific_godEmperorSlideOnMesh ;
    reload ( specific_godEmperorSlideOnMesh ) ;
    specific_godEmperorSlideOnMesh.run ( )  ;

def twoHeroes_restShapeRig_run ( *args ) :
	import projects.twoHeroes.rig_restShapeRig.core as twoHeroes_rig_restShapeRig ;
	reload ( twoHeroes_rig_restShapeRig ) ;
	twoHeroes_rig_restShapeRig.run ( ) ;

def twoHeroes_detachRig_run ( *args ) :
    import projects.twoHeroes.rig_detachRig.core as twoHeroes_rig_detachRig ;
    reload ( twoHeroes_rig_detachRig ) ;
    twoHeroes_rig_detachRig.run ( ) ;

def twoHeroes_positionPuller_run ( *args ) :
	import projects.twoHeroes.setdress_positionPuller.core as setdress_positionPuller ;
	reload ( setdress_positionPuller ) ;
	setdress_positionPuller.run() ;

##################
##################
''' [ NAV_IDX ] NAVIGATION SECTION '''
##################
##################

def browseTechAnim_cmd ( *args ) :
    import os ;
    # os.startfile ( r'P:\Temp\Shared\_techAnim' ) ;
    os.startfile ( r'N:\Staff\_techAnim' ) ;

def twoHeroes_browseDaily_cmd ( *args ) :
    import os ;
    os.startfile ( r'P:\Doublemonkeys\all\daily' ) ;

def twoHeroes_browseAlternate_cmd ( *args ) :
    import os ;
    os.startfile ( r'P:\Two_Heroes\FTP\ATL' ) ;

def twoHeroes_browseScene_cmd ( *args ) :
    import os ;
    os.startfile ( r'P:\Two_Heroes\scene' ) ;

def twoHeroes_browseMaya2017Updates_cmd ( *args ) :
    import os ;
    os.startfile ( r'\\riffarchive\wares\3D\Autodesk\Maya\2017' ) ;

##############################################################################################################################
##############################################################################################################################
''' [ UIS_IDX ] UI '''
##############################################################################################################################
##############################################################################################################################

def initialized_cmd ( *args ) :
    setDefaultMultiSampling_cmd ( ) ;

##################
''' [ UIF_IDX ] UI FUNCTIONS '''
##################

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

##################
''' [ UIC_IDX ] UI CONTENT '''
##################

def setFrameRate ( *args ) :
    ###June Edit###
    # set fps to 24
    pm.currentUnit ( time = 'film' ) ;
    
def insert ( *args ) :

    ##################
    ##################
    ''' [ ISL_IDX ] INFO SECTION '''
    ##################
    ##################

    with pm.scrollLayout ( w = layoutWidth , h = 500 ) :

        with pm.rowColumnLayout (  nc = 5 , cw = [ ( 1 , width*0.05 ) , ( 2 , width*0.55 ) , ( 3 , width*0.05 ) , ( 4 , width*0.30 ) , ( 5 , width*0.05 ) ] ) :

            filler ( ) ;
            pm.text ( label = 'resolution' ) ;
            filler ( ) ;
            pm.text ( label = 'fps' ) ;
            filler ( ) ;

            filler ( ) ;      
            with pm.rowColumnLayout (  nc = 2 , cw = [ ( 1 , width*0.55/2 ) , ( 2 , width*0.55/2 ) ] ) :       
                pm.intField ( v = 3840 ) ;
                pm.intField ( v = 1632 ) ;
            filler ( ) ;
            pm.intField ( v = 24 ) ;

        with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/3 )  , ( 2 , width/3 ) , ( 3 , width/3 ) ] ) :
            pm.button ( label = 'set scene' , c = twoHeroes_setResolution_cmd ) ;
            pm.button ( label = 'resolution' , c = resolutionGate_cmd  ) ;
            pm.button ( label = 'set fps', c = setFrameRate  ) ;

            with pm.rowColumnLayout ( nc = 1 , w = width ) :
                filler ( ) ;
                pm.button ( label = 'toggle anti-aliasing' , w = width , c = toggleAntiAliasing_cmd ) ;

                with pm.rowColumnLayout ( nc = 1 , w = width ) :
                    
                    pm.text ( label = 'sample count' , w = width ) ;

                    with pm.optionMenu( 'sampleCount_ptm' , w = width , changeCommand = sampleCountChange_cmd ) :
                        pm.menuItem ( label= '16' ) ;
                        pm.menuItem ( label= '8' ) ;
                        pm.menuItem ( label= '4' ) ;

        separator ( ) ;

        ##################
        ##################
        ''' [ DOC_IDX ] DOC SECTION '''
        ##################
        ##################

        with pm.rowColumnLayout ( nc = 1 , w = width ) :

            pm.text ( label = 'Simulation Document' ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width / 2 ) ] ) :
                pm.button ( label = 'WIP' ,  c = twoHeroes_WIPSequences_cmd , bgc = ( 1 , 1 , 0 ) , w = width/2 ) ;
                pm.button ( label = 'Completed' ,  c = twoHeroes_completedSequences_cmd , w = width/2 ) ;

        with pm.rowColumnLayout ( nc = 1 , w = width ) :

            pm.text ( label = 'Simulation Breakdown' ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width / 2 ) ] ) :
                pm.button ( label = 'Main' , c = twoHeroes_simulationBreakdown_cmd , w = width/2 ) ;
                pm.button ( label = 'Backup' , c = twoHeroes_simulationBreakdownBackup_cmd , w = width/2 )

        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
            pm.button ( label = 'project timeline' , c = twoHeroes_openProjectTimeline_cmd , w = width/2 ) ;
            pm.button ( label = 'SLR timeline' , c = twoHeroes_open_SLR_timeline_cmd , w = width/2 ) ;

            pm.button ( label = 'SRL April' , c = twoHeroes_open_SLR_April2018 , w = width/2 ) ;

        pm.button ( label = 'layout document' , c = twoHeroes_openLayoutDoc_cmd , w = width ) ;
        pm.button ( label = 'project facebook' , c = twoHeroes_openFacebook_cmd , w = width ) ;
        # pm.button ( label = 'teaser breakdown (admin)' ,  c = twoHeroes_openTeaserBreakdownDoc_cmd ) ;
        pm.button ( label = 'official shot breakdown' , c = twoHeroes_openShotList_cmd , w = width ) ; 

        separator ( ) ;
        
        ##################
        ##################
        ''' [ UTL_IDX ] UTILITIES SECTION '''
        ##################
        ##################
                
        pm.separator ( vis = True , h = 10 ) ;

        with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/6*5 ) , ( 2 , width/6 ) ] ) :
            pm.button ( label = 'playblast viewer' , w = width/6*5 , c = twoHeroes_playblastViewer_cmd ) ;
            pm.symbolButton ( image = selfPath + '/' + 'media/toShelf.png' , width = width/6 , c = twoHeroes_playblastViewer_toShelf_cmd ) ;

        with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/6*5 ) , ( 2 , width/6 ) ] ) :
            pm.button ( label = 'autonaming generation 2' , c = twoHeroes_autonamingGeneration2_cmd , bgc = ( 0 , 1 , 1 ) , w = width/6*5) ;
            pm.symbolButton ( image = selfPath + '/' + 'media/toShelf.png' , width = width/6 , c = twoHeroes_autonamingGeneration2_toShelf_cmd ) ;
        
        with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/6*5 ) , ( 2 , width/6 ) ] ) :
            pm.button ( label = 'layout organizer' , c = twoHeroes_layoutOrganizer_cmd , w = width/6*5 ) ;
            pm.symbolButton ( image = selfPath + '/' + 'media/toShelf.png' , width = width/6 , c = twoHeroes_layoutOrganizer_toShelf_cmd ) ;

        pm.button ( label = 'cache out utilities gen 2' , c = twoHeroes_cacheOutUtilGen2_cmd , w = width ) ;

        pm.button ( label = 'position puller' , c = twoHeroes_positionPuller_run , w = width ) ;

        with pm.frameLayout ( label = 'generation 1 (obsolete)' , collapsable = True , collapse = True , bgc = ( 0 , 0 , 0 ) , width = width ) :
        
            with pm.rowColumnLayout ( nc = 1 , cw = [ ( 1 , width ) ] ) :

                pm.button ( label = 'autonaming gen 1' , c = twoHeroes_autoNaming_cmd , w = width ) ;
                
                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                    filler ( ) ;
                    pm.button ( label = 'generation 0.5' , c = twoHeroes_autoNamingGeneration1_cmd , bgc = ( 0 , 0 , 0 ) , w = width/2 ) ;

                pm.button ( label = 'cache out utilities gen 1' , c = twoHeroes_cacheOutUtil_cmd , w = width ) ;

        pm.separator ( vis = True , h = 10 ) ;

        #pm.button ( label = "ta's file manager (2015)" , c = twoHeroes_fileManager_cmd , w = width ) ;
        pm.button ( label = 'groom shader importer' , c = twoHeroes_groomShaderImporter_caller , w = width ) ;
        pm.button ( label = "fee's color assigner" , c = twoHeroes_fee_colorAssigner_cmd , w = width ) ;
        
        pm.separator ( vis = True , h = 10 ) ;

        pm.button ( label = 'rest shape rig', c = twoHeroes_restShapeRig_run , w = width ) ;
        pm.button ( label = 'detach controllers' , c = twoHeroes_detachRig_run , w = width ) ;

        pm.separator ( vis = True , h = 10 ) ;

        pm.button ( label = 'God Emperor Slide on Mesh' , c = godEmperorSlideOnMesh_cmd , w = width ) ;
        pm.button ( label = 'lillyCollider' , c = lillyCollider_cmd , w = width ) ;
        
        pm.separator ( vis = True , h = 10 ) ;
        
        ##################
        ##################
        ''' [ NAV_IDX ] NAVIGATION SECTION '''
        ##################
        ##################

        pm.button ( label = 'tech anim share' , c = browseTechAnim_cmd , bgc = ( 1 , 0.4 , 0.7 ) , w = width ) ;

        with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
        # Two Heroes Browse

            pm.button ( label = 'daily' , c = twoHeroes_browseDaily_cmd , w = width/2 ) ;
            pm.button ( label = 'client (alternate)' ,  c = twoHeroes_browseAlternate_cmd , w = width/2 ) ;
            pm.button ( label = 'project scene' ,  c = twoHeroes_browseScene_cmd , w = width/2 ) ;
            pm.button ( label = 'maya 2017 updates', c = twoHeroes_browseMaya2017Updates_cmd , w = width/2 ) ;

        separator ( ) ;

        ##################
        ##################
        ''' [ NMC_IDX ] NAMING CONVENSTION SECTION '''
        ##################
        ##################

        with pm.frameLayout ( label = 'naming convention' , collapsable = True , collapse = False , bgc = ( 0.2 , 0.3 , 0.2 ) ) :
        
            with pm.rowColumnLayout ( nc = 1 , cw = [ ( 1 , width ) ] ) :

                pm.text ( label = 'cloth ( and body ) cache' , align = 'left' ) ;
                pm.textField ( text = 'v001_001.sunnyCasual.geoGrp.abc' ) ;

                separator ( ) ;

                pm.text ( label = 'xGen cache' , align = 'left' ) ;
                pm.textField ( text = 'v001_001.sunnyCasual.xGen_eyeBrow.abc' ) ;

                separator ( ) ;
                
                pm.text ( label = 'transform group' , align = 'left' ) ;
                pm.textField ( text = 'v001_001.transformGrp.ma' ) ;

    initialized_cmd ( ) ;