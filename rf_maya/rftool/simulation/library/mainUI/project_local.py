##############################################################################################################################
##############################################################################################################################
''' TABLE OF CONTENT '''
##############################################################################################################################
##############################################################################################################################

'''

navigate with "ctrl + f"

[ UTL_IDX ] UTILITIES

[ RYB_IDX ] ROYAL BLOOD

[ UIS_IDX ] UI
    [ UIF_IDX ] UI FUNCTIONS
    [ UIC_IDX ] UI CONTENT

'''

import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

##################
''' PATH '''
##################
# return : C:/Users/Administrator/Desktop/script/birdScript/library/
selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

mp = selfPath.split('/library')[0] ;
# mp = D:/Dropbox/script/birdScript
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
else : pass ;

##################
'''CONFIG'''
##################

def readText ( path , *args ) :
    # return text
    file = open ( path , 'r' ) ;
    text = file.read ( ) ;
    file.close ( ) ;
    return text ;

configText = readText ( path = selfPath + '/config.txt' ) ;
exec ( configText ) ;

globalWidth = config_dict [ 'width' ] ;
layoutWidth = globalWidth/2 ;
width = globalWidth/2*0.975 ;

##################
''' [ UTL_IDX ] UTILITIES '''
##################

def local_projectManager_cmd ( *args ) :
    import projects.local.projectManager as projectManager ;
    reload ( projectManager ) ;
    projectManager.projectManagerUI ( ) ;

##################
''' [ RYB_IDX ]  ROYAL BLOOD '''
##################
# closed

def local_FMP_royalBloodDoc_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1ziDGxJK0f_3Tmb99dDf6fYI2e2qSg1ntO0iK8WBXk04/edit?usp=sharing' ) ;

def local_royalBloodDoc_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1t3r_TFQPkMEMYGHLiAhv9QoG0Wp3NlR1Ft-zAWd__Ts/edit?usp=sharing' ) ;

##############################################################################################################################
##############################################################################################################################
''' [ UIS_IDX ] UI '''
##############################################################################################################################
##############################################################################################################################

##################
''' [ UIF_IDX ] UI FUNCTIONS '''
##################

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

##################
''' [ UIC_IDX ] UI CONTENT '''
##################

def insert ( *args ) :

    pm.button ( label = 'project manager' , c = local_projectManager_cmd , bgc = ( 1 , 0.65 , 0 ) ) ;

    separator ( ) ;

    pm.text ( label = 'Royal Blood' ) ;

    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
        pm.button ( label = "client doc" , c = local_FMP_royalBloodDoc_cmd , bgc = ( 0 , 0 , 0 ) ) ;
        pm.button ( label = 'self doc' , c = local_royalBloodDoc_cmd , bgc = ( 0 , 0 , 0 ) ) ;