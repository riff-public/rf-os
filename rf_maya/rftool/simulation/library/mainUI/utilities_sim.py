##############################################################################################################################
##############################################################################################################################
''' TABLE OF CONTENT '''
##############################################################################################################################
##############################################################################################################################

'''

navigate with "ctrl + f"

[ SMD_IDX ] SIM DOC
[ SMU_IDX ] SIM UTILITIES
[ HRU_IDX ] HAIR UTILITIES
[ OTU_IDX ] OTHER UTILITIES

[ CSU_IDX ] CLOTH SETUP
[ HSU_IDX ] HAIR SETUP
[ SUU_IDX ] SETUP UTILITIES

[ CLR_IDX ] COLOR UTILITIES
[ WDU_IDX ] WIND UTILITIES
[ CLU_IDX ] CLEANING UTILITIES
[ TST_IDX ] TEST / WIP
[ ETC_IDX ] ETC

[ UIS_IDX ] UI
    [ UIF_IDX ] UI FUNCTIONS
    [ UIC_IDX ] UI CONTENT

'''

import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

import general.utilities as utl ;
reload ( utl ) ;

import simulationTools.sim.core as simCore ;
reload ( simCore ) ;
sim = simCore.Sim() ;

import simulationTools.external.riffTools.core as riffTools ;
reload ( riffTools ) ;
riff = riffTools.Riff() ;

from rf_utils.context import context_info
reload(context_info)
from rf_utils.pipeline import shot_data
reload(shot_data)
from rf_utils import cask
reload(cask)
from rf_utils import file_utils
reload(file_utils) 

##################
''' PATH '''
##################
# return : C:/Users/Administrator/Desktop/script/birdScript/library/
selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

mp = selfPath.split('/library')[0] ;
# mp = D:/Dropbox/script/birdScript
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
else : pass ;

##################
'''CONFIG'''
##################

def readText ( path , *args ) :
    # return text
    file = open ( path , 'r' ) ;
    text = file.read ( ) ;
    file.close ( ) ;
    return text ;

configText = readText ( path = selfPath + '/config.txt' ) ;
exec ( configText ) ;

globalWidth = config_dict [ 'width' ] ;
layoutWidth = globalWidth/2 ;
width = globalWidth/2*0.95 ;
#width = globalWidth/2*0.935 ;


##################
''' [ SMU_IDX ] SIM UTILITIES '''
##################

def simUtilities_cmd ( *args ) :
    import sim.sim_simUtilities.core as sim_simUtilities ;
    reload ( sim_simUtilities ) ;
    sim_simUtilities.run ( ) ;

def simUtilities_toShelf_cmd ( *args ) :
    import sim.sim_simUtilities.core as sut ;
    reload ( sut ) ;
    sut.toShelf ( ) ;

def rivet_cmd ( *args ) :
    import rig.rivet as riv ;
    reload ( riv ) ;
    riv.rivet ( ) ;

def absoluteTrack_cmd ( *args ) :
    import sim.absoluteTrack.absoluteTrackUI as abt ;
    reload ( abt ) ;
    abt.absoluteTrackUI ( ) ;

def attribute_randomizer_run ( *args ) :
    import general.attribute_randomizer.core as attribute_randomizer ;
    reload ( attribute_randomizer ) ;
    attribute_randomizer.run ( ) ; 

##################
''' [ HRU_IDX ] HAIR UTILITIES '''
##################

def hairSettingUtilities_cmd ( *args ) :
    import sim.hairSettingUtilities as hairSettingUtilities ;
    reload ( hairSettingUtilities ) ;
    hairSettingUtilities.run ( ) ;

def moveyetiori_cmd ( *args ):
    import logging
    logger = logging.getLogger()
    logger.addHandler(logging.NullHandler())

    def get_non_transfrom_yeti():
        non_transforms = []

        for yetiNode in pm.ls(type='pgYetiMaya'):
            rootNode = pm.pgYetiGraph(yetiNode, getRootNode=True)
            nodeType = pm.pgYetiGraph(yetiNode, node=rootNode, nodeType=True)
            if nodeType != 'transform':
                non_transforms.append(yetiNode.shortName())
        return non_transforms
        
    def export_yeti_sim_origin():
        
        non_transforms = get_non_transfrom_yeti()
        if non_transforms:
            msg = 'Not all yeti node has transform node as root node. %s' %(non_transforms)
            print msg
            logger.error(msg)
            
        else:
            CAM_DIST_KEY = 'cameraDistanceFromOrigin'
            
            fileName = str(mc.file(q=True, sn=True))
            shotName = fileName.split('/')[4]
            #P:\SevenChickMovie\scene\work\act2\scm_act2_q0130a_s0160\anim\main\maya\scm_act2_q0130a_s0160_anim_main.v030.Guide.ma
            proj = fileName.split('/')[1]
            act = fileName.split('/')[2]
            shot = fileName.split('/')[3]
            scene = 'P:/%s/scene/work/%s/%s/anim/main/maya' % (proj, act, shot)

            scene = context_info.ContextPathInfo(path=scene)
            
            offset = None
            ws_data_path = None
            try:
                ws_data_path = shot_data.get_workspace_data_path(entity=scene)
            except Exception as e:
                logger.warning('Error getting workspace path')
            
            # get offset from shot ws_data
            ws_data = None
            if ws_data_path and os.path.exists(ws_data_path):
                ws_data = file_utils.ymlLoader(ws_data_path)
                logger.info('Workspace data path: %s' %(ws_data_path))
                if CAM_DIST_KEY in ws_data.keys():
                    offset = ws_data[CAM_DIST_KEY]
                    logger.info('Offset from YML data is: %s, %s, %s' %(offset[0], offset[1], offset[2]))
                    
            # if not exists, get camera ABC and extract position
            if not offset:
                logger.info('No %s found in data, extracting from ABC...' %CAM_DIST_KEY)
                shot_hero_path = scene.path.hero().abs_path()
                cam_hero_file = scene.output_name(outputKey='camHero', hero=True, ext='.abc')
                cam_hero_path = '%s/%s' %(shot_hero_path, cam_hero_file)
            
                if not os.path.exists(cam_hero_path):
                    logger.error('Cannot find cam ABC: %s' %cam_hero_path)
                    return
            
                try:
                    arc = cask.Archive(cam_hero_path)
                    camShp = [c for c in cask.find(arc.top, name='.*camshot') if c.type()=='Camera']
                    cam = camShp[0].parent
                    # cam = arc.top.children.values()[0]
                    vals = list(cam.properties['.xform/.vals'].get_value())
                    offset = [vals[0], vals[1], vals[2]]
                except Exception, e:
                    logger.error('Error extracting cam positon from ABC: %s' %cam_hero_path)
                    logger.error(e)
                    return
            
                logger.info('Offset from ABC is: %s, %s, %s' %(offset[0], offset[1], offset[2]))
                # update shot data with extracted offset
                if not ws_data:
                    ws_data = {CAM_DIST_KEY: offset}
                else:
                    ws_data[CAM_DIST_KEY] = offset
                # write YML
                data_dir = os.path.dirname(ws_data_path)
                if not os.path.exists(data_dir):
                    os.makedirs(data_dir)
                file_utils.ymlDumper(ws_data_path, ws_data)
                
                # ------------------------------------------------------ #
                
                # move techGeoGrp with negative offset
                
            neg_offset = (offset[0]*-1, offset[1]*-1, offset[2]*-1)
            if not mc.objExists('ORI_GRP'):
                mc.group(name = 'ORI_GRP',empty=True)
            mc.setAttr('ORI_GRP.tx',neg_offset[0])
            mc.setAttr('ORI_GRP.ty',neg_offset[1])
            mc.setAttr('ORI_GRP.tz',neg_offset[2])
            mc.parent('BWD_GRP', 'ORI_GRP')
            
            # apply offset value to all yeti node transform node
            for yetiNode in pm.ls(type='pgYetiMaya'):
                logger.debug('** Setting offset on: %s' %yetiNode.shortName())
                rootNode = pm.pgYetiGraph(yetiNode, getRootNode=True)
                pm.pgYetiGraph(yetiNode, node=rootNode, param='translate', setParamValueVector=offset)
                logger.info('Offset set on %s/%s: %s, %s, %s' %(yetiNode.shortName(), rootNode, offset[0], offset[1], offset[2]))
        
            
    export_yeti_sim_origin()

##############Sam##########################
###########################################
def SamColorYeti_cmd ( *args ) :
    from rftool.sim.sam_tool.attrYeti import attrYetiData
    reload ( attrYetiData )

def SamYetiExport_cmd ( *args ) :
    from rftool.sim.sam_tool.yetiCache import yetiExport
    reload ( yetiExport )

def SamFXStatus_cmd ( *args ) :
    from rftool.sim.sam_tool.fxStatus import FXStatus
    reload ( FXStatus )
    
def samWindCtrlKeyframe_cmd ( *args ) :
    from rftool.sim.sam_tool.keyWindCtrl import keyframeWind
    reload ( keyframeWind )

def samSavePreset_cmd ( *args ) :
    from rftool.sim.sam_tool.preset import corePreset
    reload ( corePreset )
def samAutoOrigin_cmd ( *args ) :
    from rftool.sim.sam_tool.origin import auto_origin
    reload ( auto_origin )
###################fee#####################
###########################################

def fee_multipleNucleusCollider_cmd ( *args ) :
    import sim.fee_makeMNCOL as fee_makeMNCOL ;
    reload ( fee_makeMNCOL ) ;
    fee_makeMNCOL.makeMulCOL( ) ;

def fee_colorAssigner_cmd ( *args ) :
    import external.RiFF.fee_colorAssigner.core as fee_colorAssigner ;
    reload ( fee_colorAssigner ) ;
    fee_colorAssigner.run ( ) ;

def fee_nucleusSettingUtilities_cmd ( *args ) :
    import sim.fee_nucleusSettingUtilities as fee_nucleusSettingUtilities ;
    reload ( fee_nucleusSettingUtilities ) ;
    fee_nucleusSettingUtilities.run ( ) ;

def fee_addNewNucleus ( *args ) :
    from rftool.sim.fee_tool.addNewNucleus import addNewNucleus
    reload ( addNewNucleus )
    addNewNucleus.assNSolvUI()

###################June###################
##########################################
def june_setupSim_cmd ( *args ) :
    from rf_maya.rftool.sim.june_tool import core
    reload(core)
    core.Ui().show()

def june_playblast_cmd ( *args ) :
    from rf_maya.rftool.sim.june_tool.playblast import playblastSim;
    reload( playblastSim );
    playblastSim.playblastTools();
    playblastSim.setDefaultUI();

def june_abcCache_cmd ( *args ) :
    import external.RiFF.june_abcCache.abcCacheJune as june_abcCache ;
    reload ( june_abcCache ) ;
    june_abcCache.cacheTools( );
    june_abcCache.setDefaultIC( );

def june_rivetTransform_cmd ( *args ) :
    import external.RiFF.june_rivetTransform as june_rivetTransform ;
    reload ( june_rivetTransform ) ;
    june_rivetTransform.rivetTransform( );
###################nu######################
###########################################
def nu_BlendShapeCurveNode_cmd( ) :
    gcrvs = pm.ls('Hair*_CRV', type='transform') 
    for gc in gcrvs: 
        name = gc.nodeName() 
        pair = name.replace('_CRV', '_BSH') 
        pairObjs = pm.ls(pair) 
    if pairObjs: 
        pm.connectAttr(gc.worldSpace[0], pariObjs[0].create, f=True) 

def nu_FixCurveUV_cmd( *args ) :
    from nuTools.util import hairCurves as uhc 
    reload(uhc)
    uhc.fix_curves_on_border(fix=True,tol=0.00001, moveDist=0.001)

###########################################
##################
''' [ OTU_IDX ] OTHER UTILITIES '''
##################

def cometRename_cmd ( *args ) :
    import external.cometRename as cometRename ;
    reload ( cometRename ) ;
    cometRename.cometRename ( ) ;

def customBlendShape_cmd ( *args ) :
    selection = pm.ls ( sl = True ) ;
    
    driver = selection [0] ;
    driven = selection [1] ;
    
    bsh = pm.blendShape ( driver , driven , o = 'world', n = '%s_srcBSH' % driver ,bf = 1) ;

    if ":" in driver:
        dni  = driver.split(":")
        pm.setAttr ( '%s.%s' % ( bsh[0] , dni[1] ) , 1 ) ; 
        pm.setAttr ( '%s.envelope' % bsh[0] , 1 ) ;
    else:

        pm.setAttr ( '%s.%s' % ( bsh[0] , driver ) , 1 ) ; 
        pm.setAttr ( '%s.envelope' % bsh[0] , 1 ) ;

def softCluster_cmd ( *args ) :
    import external.softClusterCaller as softClusterCaller ;
    reload ( softClusterCaller ) ;
    softClusterCaller.run ( ) ;
def Cluster_cmd ( *args ) :
    import maya.cmds as mc ;
    import sys ;

    sels = mc.ls(sl=True)

    if '.' in sels[0]:
    
        b = sels[0].split(".")
    
    
        mc.cluster(n = b[0]+'Cluster')
    else:
        mc.cluster(n = sels[0]+'Cluster')

def stickyCluster_cmd(*args):
    from nuTools.rigTools import bpmRig
    # reload(bpmRig)

    stickyCluster = bpmRig.BpmClusterRig()
    stickyCluster.rig()

def makeClusterGrp_call ( *args ) :
    import sim.cleanup_clusterGrp.core as cleanup_clusterGrp ;
    reload ( cleanup_clusterGrp ) ;
    cleanup_clusterGrp.run() ;

def softClusterToShelf_cmd ( *args ) :
    import external.softClusterCaller as softClusterCaller ;
    reload ( softClusterCaller ) ;
    softClusterCaller.addButtonToShelf_cmd ( ) ;


def multipleNucleus_colliderSetUp_cmd ( *args ) :
    import sim.multipleNucleus_colliderSetUp as multipleNucleus_colliderSetUp ;
    reload ( multipleNucleus_colliderSetUp ) ;
    multipleNucleus_colliderSetUp.run ( ) ;


def tube2ORISelection_cmd ( *args ) :
    import sim.hair_general as hair_general ;
    reload ( hair_general ) ;
    hair_general.tube2OriSelection_cmd ( ) ;


    
''' [ CSU_IDX ] CLOTH SETUP '''
##################
def restShapeRig_run ( *args ) :
    import project.sevenChickMovie.rig_restShapeRig.core as rig_restShapeRig ;
    reload ( rig_restShapeRig ) ;
    rig_restShapeRig.run ( ) ;

def clothSimSetUp_cmd ( *arg ) :
    bp = mp + '/external/BOUTPython' ;    
    if not bp in sys.path :
        sys.path.append ( bp ) ;
    import ClothSimScript.ClothSimSetUp as bpcs ;
    reload ( bpcs ) ;

def makeDYNSetUp_cmd ( *args ) :
    bp = mp + '/external/BOUTPython' ;    
    if not bp in sys.path :
        sys.path.append ( bp ) ;

    import ClothSimScript.MakeDYNSetUp as bmdyn ;
    reload ( bmdyn ) ;

def makeNRigidSetUp_cmd ( *args ) :
    bp = mp + '/external/BOUTPython' ;    
    if not bp in sys.path :
        sys.path.append ( bp ) ;

    import ClothSimScript.MakeNRigidSetUp as bmnrg ;
    reload ( bmnrg ) ;
    bmnrg.main ( ) ;

def attachButton_cmd ( *args ) :
    import external.RiFF.fiw_attachButton.core as fiw_attachButton ;
    reload ( fiw_attachButton ) ;
    fiw_attachButton.attachFollicle() ;

def duplicateNClothMesh_cmd ( *args ) :
    import sim.sim_duplicateNClothMesh.core as sim_duplicateNClothMesh ;
    reload ( sim_duplicateNClothMesh ) ;
    sim_duplicateNClothMesh.run() ;

##################
''' [ HSU_IDX ] HAIR SETUP '''
##################

def hair_blendshapeCV_cmd ( *args ) :
    import sim.hair_blendshapeCV.core as hair_blendshapeCV ;
    reload ( hair_blendshapeCV ) ;
    hair_blendshapeCV.run ( ) ;

def hair_blendshapePercentage_run ( *args ) :
    import sim.hair_blendshapePercentage.ui as hair_blendshapePercentage ;
    reload ( hair_blendshapePercentage ) ;
    hair_blendshapePercentage.run ( ) ;

def hair_qchaircurves_cmd(*args):    
    import pymel.core as pm
    from rf_utils.pipeline import check
    reload(check)

    sels = pm.selected()
    curr_grp = sels[0].nodeName()
    abc_grp = sels[1].nodeName().split(':')[-1]
    abc_path = str(pm.referenceQuery(sels[1], f=True))
    if not abc_path.endswith('.abc'):
        print 'Only work with .abc reference'
    else:
        checker = check.CustomCheck(curr_grp, abc_grp, abc_path)
        res = checker.check()

def hair_fixCurveShapeName_cmd ( *args ) :
    import sim.hair_fixCurveShapeName.core as hair_fixCurveShapeName ;
    reload ( hair_fixCurveShapeName ) ;
    hair_fixCurveShapeName.run ( ) ;

def hairSetup_rebuildCurveUtil_cmd ( *args ) :
    import sim.hairSetup_rebuildCurveUtil as hairSetup_rebuildCurveUtil ;
    reload ( hairSetup_rebuildCurveUtil ) ;
    hairSetup_rebuildCurveUtil.run ( ) ;

def hairSetupUtilities_cmd ( *args ) :
    import sim.hairSetupUtilities as hairSetupUtilities ;
    reload ( hairSetupUtilities ) ;
    hairSetupUtilities.run ( ) ;

def hairSetupRef_cmd ( *args ) :    
    import sim.setupHairRef as setupHairRef ;
    reload ( setupHairRef ) ;
    setupHairRef.run( ) ;

def connectVisibility (*args):
    geoGrps = pm.namespaceInfo( lon=True );
    cinGrp = geoGrps[0] + ':Geo_Grp'
    geoGrp = geoGrps[1] + ':Geo_Grp'
    childrenCIN = pm.listRelatives ( cinGrp, ad=True, typ='transform' );
    childrenCOL = pm.listRelatives ( geoGrp, ad=True, typ='transform' );

    for childrenChildCIN, childrenChildCOL in zip( childrenCIN, childrenCOL ):
        driver = childrenChildCIN + '.visibility';
        driven = childrenChildCOL + '.visibility';
        mc.connectAttr(driver, driven);
def extrudeHairTube_cmd ( *args ) :
    import sim.extrudeHairTube as eht ;
    reload ( eht ) ;
    eht.extrudeHairTube ( selection = pm.ls ( sl = True ) ) ;


##################
''' [ SUU_IDX ] SETUP UTILITIES '''
##################

def preRollCluster_cmd ( *args ) :
    import sim.prerollCluster as prerollCluster ;
    reload ( prerollCluster ) ;
    prerollCluster.run ( ) ;

##################
''' [ CLR_IDX ] COLOR UTILITIES '''
##################

def assignDNshader_cmd ( *args ) :        
    bp = mp + '/external/BOUTPython' ;    
    if not bp in sys.path :
        sys.path.append ( bp ) ;
    import ClothSimScript.AssignDNshader as badns ;
    reload ( badns ) ;

def outlinerColorAssigner_cmd ( *args ) :
    import sim.outlinerColorAssigner as outlinerColorAssigner ;
    reload ( outlinerColorAssigner ) ;
    outlinerColorAssigner.run ( ) ;

def importRampShader_cmd ( *args ) :
    rampShaderPath = mp + '/' + 'asset/shader_lambertRamp.ma' ;
    pm.system.importFile ( rampShaderPath ) ;

def importCheckerShader_cmd ( *args ) :
    rampShaderPath = mp + '/' + 'asset/shader_checker.ma' ;
    pm.system.importFile ( rampShaderPath ) ;

def randomCurveColor_cmd ( *args ) :
    import sim.randomCurveColor as randomCurveColor ;
    reload ( randomCurveColor ) ;
    randomCurveColor.randomCurveColor ( operation = 'enable' ) ;

def disableRandomCurveColor_cmd ( *args ) :
    import sim.randomCurveColor as randomCurveColor ;
    reload ( randomCurveColor ) ;
    randomCurveColor.randomCurveColor ( operation = 'disable' ) ;

##################
''' [ WDU_IDX ] WIND UTILITIES '''
##################

def ffWind_cmd ( *args ) :
    ffwp = mp + '/external/FFWind' ;    
    if not ffwp in sys.path :
        sys.path.append ( ffwp ) ;

    import connectWind ;
    reload ( connectWind ) ;
    connectWind.main ( ) ;

def createWindRig_cmd ( *args ) :
    import sim.createWindRig as createWindRig ;
    reload ( createWindRig ) ;
    createWindRig.run ( ) ;

def windPresetManager_cmd ( *args ) :
    import sim.presetManager.windPresetManager as windPresetManager ;
    reload ( windPresetManager ) ;
    windPresetManager.run ( ) ;

##################
''' [ CLU_IDX ] CLEANING UTILITIES '''
##################

def unlockSoftenEdge_cmd ( *arg ) :

    bp = mp + '/external/BOUTPython' ;    
    if not bp in sys.path :
        sys.path.append ( bp ) ;

    import ClothSimScript.UnlockSoftenEdge as buse ;
    reload ( buse ) ;
    buse.main ( ) ;

def deletePolyAV_cmd ( *arg ) :

    bp = mp + '/external/BOUTPython' ;    
    if not bp in sys.path :
        sys.path.append ( bp ) ;

    import ClothSimScript.DeletePolyAV as bdav ;
    reload ( bdav ) ;
    bdav.main ( ) ;

def reOrder_cmd ( *arg ) :

    bp = mp + '/external/BOUTPython' ;    
    if not bp in sys.path :
        sys.path.append ( bp ) ;
    import ClothSimScript.ReOrder as bro ;
    reload ( bro ) ;
    bro.main ( ) ;

def deleteRebuiltShape_cmd ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.deleteRebuiltShape_run ( ) ;

def deleteShapeOrig_cmd ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.deleteShapeOrig_run ( ) ;

def freezetransformDeletehistory_cmd ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.freezeTransform_deleteHistory_run ( ) ;

def freezeTransform_cmd ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.freezeTransform_run ( ) ;

def deleteHistory_cmd ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.deleteHistory_run ( ) ;

def centerPivot_cmd ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.centerPivot_run ( ) ;

def freezeTransform_deleteHistory_centerPivot_toShelf_cmd ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.freezeTransform_deleteHistory_centerPivot_toShelf ( ) ;

def removeNullReferebce_cmd ( *args ) :
    import sim.cleanup_removeNullReference.core as cleanup_removeNullReference ;
    reload ( cleanup_removeNullReference ) ;
    cleanup_removeNullReference.run() ;

def deleteAbcBlendShape_cmd ( *args ) :
    import sim.cleanup_deleteAbcBlendShape.core as cleanup_deleteAbcBlendShape ;
    reload ( cleanup_deleteAbcBlendShape ) ;
    cleanup_deleteAbcBlendShape.run() ;

def findDuplicated_cmd ( *args ) :
    import sim.cleanup_findDuplicated.core as cleanup_findDuplicated ;
    reload ( cleanup_findDuplicated ) ;
    cleanup_findDuplicated.run() ;
    



##############################################################################################################################
##############################################################################################################################
''' [ UIS_IDX ] UI '''
##############################################################################################################################
##############################################################################################################################

##################
''' [ UIF_IDX ] UI FUNCTIONS '''
##################

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

##################
''' [ UIC_IDX ] UI CONTENT '''
##################

def insert ( *args ) :
###################### main ########################################
    with pm.scrollLayout ( w = layoutWidth , h = 500 ) :

        with pm.rowColumnLayout ( nc = 1 , w = width ) :
            pm.button ( label = 'cometRename' , c = cometRename_cmd , bgc = ( 1 , 0.9 , 0.7 ) ) ;

            pm.button ( label = 'blendshape' , c = customBlendShape_cmd , bgc = ( 1 , 1 , 1 ) ) ;
            separator ( ) ;

            with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                pm.button ( label = 'cluster' , c = Cluster_cmd , bgc = ( 1 , 0.5 , 0 )  ) ;
                pm.button ( label = 'soft cluster' , c = softCluster_cmd ,bgc = ( 1 , 0.5 , 0 ) ) ;
                #pm.symbolButton ( image = selfPath + '/' + 'media/toShelf.png' , width = width/6 , c = softClusterToShelf_cmd ) ;
                #pm.button ( label = 'add to shelf' , c = softClusterToShelf_cmd ) ;
            pm.button ( label = 'sticky custer' , c = stickyCluster_cmd ,bgc = ( 1 , 0.5 , 0 ) ) ;
            pm.button ( label = 'make cluster group' , c = makeClusterGrp_call ) 
            separator ( ) ;

        ############ cloth setup ################
            pm.text ( label = 'Setup' )
            with pm.frameLayout ( label = 'Setup Cloth' , collapsable = True , collapse = True , bgc = ( 0.4 , 0.5 , 0.5 ) , width = width ) :
                pm.button ( label = 'Attach Follicles' , c = sim.attachFollicles ) ;
                pm.button ( label = 'Connect Visibility' , c = connectVisibility ) ;
                with pm.frameLayout ( label = 'DYN' , collapsable = True , collapse = False , bgc = ( 0.8 , 0.6 , 0.1 ) ) :
                    with pm.rowColumnLayout ( nc = 1 , cw = [ ( 1 , width ) ] ) :
                        
                        pm.button ( label = 'Cloth Sim Setup (Monk)' , c = clothSimSetUp_cmd ) ;
                        pm.button ( label = 'Duplicate NCloth Mesh' , c = duplicateNClothMesh_cmd ) ;  
                        pm.button ( label = 'rest shape rig', c = restShapeRig_run , w = width ) ;    
                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = 'Make DYN Setup' , c = makeDYNSetUp_cmd ) ;
                        pm.button ( label = 'Make nRigid Setup' , c = makeNRigidSetUp_cmd ) ;
                with pm.frameLayout ( label = 'Button' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0.5 ) ) :
                    with pm.rowColumnLayout ( nc = 1 , cw = [ ( 1 , width ) ] ) :
                        pm.button ( label = "Fiw's Attach Button" , c = attachButton_cmd ) ;
            separator ( ) ;
        ################## hair setup #####################
            with pm.frameLayout ( label = 'Setup Hair' , collapsable = True , collapse = True , bgc = ( 0.4 , 0.5 , 0.5 ) ) :
        
                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :

                    pm.button ( label = 'hair setup (ref)' , c = hairSetupRef_cmd ) ;
                    pm.button ( label = 'hair setup (import)' , c = hairSetupUtilities_cmd ) ;
                    pm.button ( label = "qc hair curves" , c = hair_qchaircurves_cmd , w = width ) ;
                    pm.button ( label = "fix curves' shape name" , c = hair_fixCurveShapeName_cmd , w = width ) ;
                    pm.button ( label = 'rebuild curve util' , c = hairSetup_rebuildCurveUtil_cmd , w = width ) ;
                    pm.button ( label = 'extrude hair tube along curve' , w = width , c = extrudeHairTube_cmd ) ;
                    pm.button ( label = 'Copy Curve Shape' , w = width , c = sim.copyCurveShape ) ;

                    with pm.frameLayout ( label = 'YETI' , collapsable = True , collapse = False , bgc = ( 0.7 , 0 , 0.5 ) ) :

                        with pm.rowColumnLayout ( nc = 1 , cw = [ ( 1 , width ) ] ) :

                            import sim.yeti.curve_setAttraction.core as yeti_curve_setAttraction ;
                            reload ( yeti_curve_setAttraction ) ;
                            yeti_curve_setAttraction.insert ( width = width ) ;
            separator ( ) ;
########################## sim cloth #################################
            pm.text ( label = 'For Sim' )
            with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/6*5 ) , ( 2 , width/6 ) ] ) :
                pm.button ( label = 'sim utilities' , c = simUtilities_cmd , bgc = ( 0 , 1 , 1 ) ) ;
                pm.symbolButton ( image = selfPath + '/' + 'media/toShelf.png' , width = width/6 , c = simUtilities_toShelf_cmd ) ;
            pm.button ( label = 'hair setting utilities' , c = hairSettingUtilities_cmd , bgc = ( 1 , 0.4 , 0.1 ) ) ;
            separator ( ) ;
            with pm.frameLayout ( label = 'Track' , collapsable = True , collapse = True , bgc = ( 0.7 , 0.4 , 0.4 ) ) :
        
                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :
                    pm.button ( label = "rivet transform" , c = june_rivetTransform_cmd  ) ;
                    with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = 'rivet' , c = rivet_cmd ) ;
                        pm.button ( label = 'absolute track' , c = absoluteTrack_cmd ) ;
                    
            separator ( ) ;
    #################### sim hair ######################
            
            with pm.frameLayout ( label = 'Hair' , collapsable = True , collapse = True , bgc = ( 0.5 ,0 ,0.501961) ) :
                
                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :
                    pm.button ( label = 'hair CV blendshape' , c = hair_blendshapeCV_cmd , width = width ) ;
                    pm.button ( label = 'select ORI from hair tube', c = tube2ORISelection_cmd ) ;
                    pm.button ( label = 'hair percentage blendshape' , c = hair_blendshapePercentage_run , width = width ) ;
                    pm.button ( label = 'create multiple nucleus collider' , c = multipleNucleus_colliderSetUp_cmd ) ;
                    pm.button ( label = 'Attach Tfm to Curve' , c = sim.attachTfmToCurve ) ;
                    pm.button ( label = 'BlendShape (CurveNode)' , c = nu_BlendShapeCurveNode_cmd)
                    pm.button ( label = 'FixCurveUV', c = nu_FixCurveUV_cmd)
            separator ( ) ;


    #################### sim hair ######################
            with pm.frameLayout ( label = 'Yeti' , collapsable = True , collapse = True , bgc = ( 0.294118 ,0 ,0.509804) ) :
        
                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :
                    pm.button ( label = 'Move to ORI' , c = moveyetiori_cmd  ) ;
                    pm.button ( label = "Sam's Yeti Export" , c = SamYetiExport_cmd ) ;
                    pm.button ( label = "Sam's Origin" , c = samAutoOrigin_cmd ) ;

    #################### wind utilities #######################
            pm.text ( label = 'Wind' )
            with pm.frameLayout ( label = 'Wind Utilities' , collapsable = True , collapse = True , bgc = ( 0.2 , 0.3 , 0.3 ) ) :

                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :

                    with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = 'connect wind' , c = ffWind_cmd ) ;
                        pm.button ( label = 'create wind rig' , c = createWindRig_cmd ) ;
                    pm.text ( label = 'Nucleus Rig' ) ;
                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = 'Create' , c = sim.nucleusRig_attach ) ;
                        pm.button ( label = 'Delete' , c = sim.nucleusRig_detach ) ;
                    pm.button ( label = 'attribute randomizer' , c = attribute_randomizer_run ) ;
                    
            separator ( ) ;
    ################## June #####################
            with pm.frameLayout ( label = "June's Tools" , collapsable = True , collapse = True ,bgc = ( 0.61569 , 0.49804 , 0.61569 )) :

                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :
                    pm.button ( label = "june's Setup Sim" , c = june_setupSim_cmd ) ;
                    pm.button ( label = "june's playblast" , c = june_playblast_cmd ) ;
                    pm.button ( label = "june's abcCache" , c = june_abcCache_cmd ) ;
                    pm.button ( label = "June's Show Menu" , c = riff.june_updateShowMenu ) ;
            separator ( ) ;
    ##################### Fee #########################
            with pm.frameLayout ( label = "Fee's Tools" , collapsable = True , collapse = True ,bgc = ( 0.42745 , 0.48627 , 0.34118 ) ) :

                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :
                    pm.button ( label = "fee's multiple nucleus collider" , c = fee_multipleNucleusCollider_cmd ) ;
                    pm.button ( label = "fee's nucleus setting utilities" , c = fee_nucleusSettingUtilities_cmd ) ;
                    pm.button ( label = "fee's color assigner" , c = fee_colorAssigner_cmd , w = width ) ;
            separator ( ) ;
    ##################### Sam #########################
            with pm.frameLayout ( label = "Sam's Tools" , collapsable = True , collapse = True ,bgc = ( 0.7 , 0.48627 , 0.34118 ) ) :

                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :
                    pm.button ( label = "Sam's Asset Yeti" , c = SamColorYeti_cmd ) ;
                    pm.button ( label = "Sam's Yeti Export" , c = SamYetiExport_cmd ) ;
                    pm.button ( label = "Sam's FX Status" , c = SamFXStatus_cmd ) ;
                    pm.button ( label = "Sam's windCtrlKeyframe" , c = samWindCtrlKeyframe_cmd )
                    pm.button ( l = "Sam's SavePreset" , c = samSavePreset_cmd )
                    pm.button ( l = "Sam's Origin" , c = samAutoOrigin_cmd )
            separator ( ) ;
    ####################### Cleaning Utilities #####################
            with pm.frameLayout ( label = 'Cleaning Utilities' , collapsable = True , collapse = True , bgc = ( 0 , 0 , 0 ) ) :

                with pm.rowColumnLayout ( nc = 1 , w = width ) :

                    with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , width/6*5/3 ) , ( 2 , width/6*5/3 ) , ( 3 , width/6*5/3 ) , ( 4 , width/6 ) ] ) :
                        pm.button ( label = 'FT' , c = freezeTransform_cmd , w = width/6*5/3 ) ;
                        pm.button ( label = 'Hist' , c = deleteHistory_cmd , w = width/6*5/3 ) ;
                        pm.button ( label = 'CP' , c = centerPivot_cmd , w = width/6*5/3 ) ;
                        pm.symbolButton ( image = selfPath + '/' + 'media/toShelf.png' , c = freezeTransform_deleteHistory_centerPivot_toShelf_cmd , width = width/6 ) ;

                    separator ( ) ;

                    pm.button ( label = 'Delete Abc BlendShape' , c = deleteAbcBlendShape_cmd , w = width ) ;
                    pm.text ( label = 'select ABC:Geo_Grp then press the button' ) ;

                    separator ( ) ;

                    pm.button ( label = 'Rename nObjects' , c = sim.renameNObects , w = width ) ;
                    pm.text ( label = 'supported nObjs: nCloth, nHair, nConstraint' ) ;

                    separator ( ) ;                    

                    pm.button ( label = 'Remove Null Reference' , c = removeNullReferebce_cmd , w = width ) ;
                    pm.button ( label = 'Find Duplicated' , c = findDuplicated_cmd , w = width ) ;

                    separator ( ) ;    

                    pm.button ( label = 'Unlock and Soften Edge' , c = unlockSoftenEdge_cmd , w = width ) ;
                    pm.button ( label = 'Poly Average Vertex Util' , c = sim.polyAverageVertexUtil , w = width ) ;
                    pm.button ( label = 'Delete Hierarchy Constraint' , c = sim.deleteHierarchyConstraints , w = width ) ;
                    pm.button ( label = 'Reorder' , c = reOrder_cmd , w = width ) ;

                    separator ( ) ;

                    pm.button ( label = "Delete Curves' Rebuilt Shape" , c = deleteRebuiltShape_cmd , w = width ) ;
                    pm.button ( label = 'Delete Orig Shape' , c = deleteShapeOrig_cmd , w = width ) ;

                    with pm.frameLayout ( label = 'obsolete tools' , collapsable = True , collapse = True , bgc = ( 0 , 0 , 0 ) , width = width ) :
                        pm.button ( label = 'Delete Average Vertex' , c = deletePolyAV_cmd , w = width ) ;


           
    #################### color utilities ########################
            with pm.frameLayout ( label = 'Color Utilities' , collapsable = True , collapse = True , bgc = ( 0.4 , 0 , 0 ) ) :

                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :

                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = "Kheng's Color" , c = riff.kheng_colorAssigner ) ;
                        pm.button ( label = "Old Color" , c = assignDNshader_cmd ) ;

                    pm.button ( label = 'outliner color assigner' , c = outlinerColorAssigner_cmd ) ;
                    

                    separator ( ) ;

                    pm.text ( label = 'import ...' )
                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = 'ramp shaders' , c = importRampShader_cmd ) ;
                        pm.button ( label = 'checker shader' , c = importCheckerShader_cmd ) ;

                    separator ( ) ;

                    pm.text ( label = "help: select curves' shape then run the script" ) ;
                    with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = 'randomize curve color' , c = randomCurveColor_cmd )  ;
                        pm.button ( label = 'disable curve color' , c = disableRandomCurveColor_cmd )  ;
            separator ( ) ;
            # self.mergePath_textField = pm.textField ( 'mergePath_textField' , w = width ) ;
            # with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/3*2 ) , ( 2 , width/3 ) ] ) :
            #pm.button ( 'merge_pic' , label = 'merge_pic' , c = merge_sequence_picture , w = width) ;

    ##################################################
# def merge_sequence_picture(self, *args):
#     import glob
#     from rf_utils.pipeline import playblast_converter
#     reload(playblast_converter)
#     path  = pm.textField ( self.mergePath_textField , q = True , tx = True ).replace('\\', '/')
#     ext   = '.tif'
#     name  = pm.textField ( self.playblastName_textField , q = True , tx = True ) ;
#     print path
#     if os.path.exists ( path ) :
#        list_files = sorted(filter(lambda x: os.path.splitext(x)[1] == ext, os.listdir(path)))
#        print list_files
#        first_file_name, first_frame, first_ext = list_files[0].split('.')
#        if list_files:
#             playblast_converter.merge_img_seq_to_mov(path, '%s.mov'%first_file_name, ext_file =ext)
