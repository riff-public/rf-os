import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

### ### ###
### ### ###
### ### ###

selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;
# C:/Users/Administrator/Desktop/script/birdScript/library/

### ### ###
### ### ###
### ### ###

configTextPath = selfPath + '/mainUI_config.txt' ;

configFile = open ( configTextPath , 'a+' ) ;
configText = configFile.read ( ) ;
configFile.close ( ) ;

globalWidth = config_dict [ 'width' ] ;
mainLayoutWidth = globalWidth/2 ;
width = mainLayoutWidth*0.975 ;

### ### ###
### ### ###
### ### ###

def separator ( h = 10 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

def frameLayout_cc ( *args ) :
    pm.window ( 'birdMainUI', e = True , h = 10 ) ;

### ### ###
### ### ###
### ### ###

def twoHeroes_InfoLayout_grp ( *args ) :

    def twoHeroes_setResolution_cmd ( *args ) :

        defaultResolution = pm.general.PyNode ( 'defaultResolution' ) ;

        defaultResolution.aspectLock.set ( 0 ) ;
        defaultResolution.width.set ( 3840 ) ;
        defaultResolution.height.set ( 1632 ) ;
        defaultResolution.dotsPerInch.set ( 72 ) ;
        pm.currentUnit ( time = 'film' ) ;

        pm.select ( defaultResolution ) ;

        #pm.mel.eval ( 'ToggleAttributeEditor' ) ;
        pm.mel.eval ( 'checkAspectLockHeight2 "defaultResolution"' ) ;
        pm.mel.eval ( 'checkAspectLockWidth2 "defaultResolution"' ) ;

    def resolutionGate_cmd ( *args ) :

        import maya.OpenMaya as OpenMaya
        import maya.OpenMayaUI as OpenMayaUI

        view = OpenMayaUI.M3dView.active3dView ( ) ;
        cam = OpenMaya.MDagPath()
        view.getCamera(cam)
        camPath = cam.fullPathName()

        status = pm.camera ( camPath , q = True , displayResolution = True )

        if status != True :
            pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = True , overscan = 1.3 ) ;
        else :
            pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = False , overscan = 1.0 ) ;

        camera = pm.general.PyNode ( camPath ) ;
        #cameraShape = pm.listRelatives ( camera , shapes = True ) [0] ;
        camera.displayGateMaskColor.set ( 0 , 0 , 0 ) ;
        camera.displayGateMaskOpacity.set ( 1 ) ;

        #setAttr "CAM:cam0030Shape.displayGateMaskColor" -type double3 0 0 0 ;
        #setAttr CAM:cam0030Shape.displayGateMaskOpacity 1;

    def twoHeroes_Info_layout ( *args ) :

        with pm.rowColumnLayout (  nc = 5 , cw = [ ( 1 , width*0.05 ) , ( 2 , width*0.55 ) , ( 3 , width*0.05 ) , ( 4 , width*0.30 ) , ( 5 , width*0.05 ) ] ) :

            filler ( ) ;
            pm.text ( label = 'resolution' ) ;
            filler ( ) ;
            pm.text ( label = 'fps' ) ;
            filler ( ) ;

            filler ( ) ;      
            with pm.rowColumnLayout (  nc = 2 , cw = [ ( 1 , width*0.55/2 ) , ( 2 , width*0.55/2 ) ] ) :       
                pm.intField ( v = 3840 ) ;
                pm.intField ( v = 1632 ) ;
            filler ( ) ;
            pm.intField ( v = 24 ) ;

        pm.button ( label = 'set scene' , c = twoHeroes_setResolution_cmd ) ;
        pm.button ( label = 'resolution gate' , c = resolutionGate_cmd  ) ;

    twoHeroes_Info_layout ( ) ;

def twoHeroes_projectDocLayout_grp ( *args ) :

    def twoHeroes_openSimulationDoc_cmd ( *args ) :
        import general.utilities as utl ;
        reload ( utl ) ;
        utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/14Lawz9PybwGsx1Y9oZauonv44UzKEZFSzvH9tDKeb84/edit?usp=sharing' ) ;

    def twoHeroes_openFacebook_cmd ( *args ) :
        import general.utilities as utl ;
        reload ( utl ) ;
        utl.openLink ( link = 'https://www.facebook.com/groups/665802640258497/' ) ;

    def twoHeroes_openTeaserBreakdownDoc_cmd ( *args ) :
        import general.utilities as utl ;
        reload ( utl ) ;
        utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1j-spZIgdJoUcOys0PbWttzd5wtoXMmjxdhaZb73h-gI/edit?ts=59d70bfc#gid=1097980972' ) ;

    def twoHeroes_openShotList_cmd ( *args ) :
        import general.utilities as utl ;
        reload ( utl ) ;
        utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/19_B9R0fvifJro6eycBZcYjrvRva2NZZhqQSTxu4slWM/' ) ;

    def twoHeroes_projectDoc_layout ( *args ) :
        pm.button ( label = 'simulation document' ,  c = twoHeroes_openSimulationDoc_cmd , bgc = ( 1 , 1 , 0 ) ) ;
        pm.button ( label = 'The Legend of Two Heroes facebook' , c = twoHeroes_openFacebook_cmd ) ;
        pm.button ( label = 'teaser breakdown (admin)' ,  c = twoHeroes_openTeaserBreakdownDoc_cmd ) ;
        pm.button ( label = 'official shot breakdown' , c = twoHeroes_openShotList_cmd ) ; 

    twoHeroes_projectDoc_layout ( ) ;
    
def twoHeroes_utilitiesLayout_grp ( *args ) :

    def twoHeroes_autoNaming_cmd ( *args )  :
        import projects.twoHeroes.autonaming as thAN ;
        reload ( thAN ) ;
        thAN.TLo2H_ANUI ( ) ;

    def twoHeroes_autoNamingGeneration1_cmd ( *args )  :
        import projects.twoHeroes.autonaming_generation1 as thAN ;
        reload ( thAN ) ;
        thAN.TLo2H_ANUI ( ) ;

    def twoHeroes_cacheOutUtil_cmd ( *args ) :
        import projects.twoHeroes.cacheOutUtilities as cou ;
        reload ( cou ) ;
        cou.animCacheOutUI ( ) ;

    def twoHeroes_xGenShaderImporter_cmd ( *args ) :
        import projects.twoHeroes.xgenShaderImporter as twoHeroes_xgenShaderImporter ;
        reload ( twoHeroes_xgenShaderImporter ) ;
        twoHeroes_xgenShaderImporter.run ( ) ;

    def twoHeroes_utilities_layout ( *args ) :
        pm.button ( label = 'autonaming' , c = twoHeroes_autoNaming_cmd , bgc = ( 0 , 1 , 1 ) ) ;
        pm.button ( label = 'autonaming (generation 1)' , c = twoHeroes_autoNamingGeneration1_cmd  ) ;
        pm.button ( label = 'cache out utilities' , c = twoHeroes_cacheOutUtil_cmd ) ;
        pm.button ( label = 'xGen shader importer' , c = twoHeroes_xGenShaderImporter_cmd ) ;

    twoHeroes_utilities_layout ( ) ;

def twoHeroes_shortcutLayout_grp ( *args ) :

    def browseTechAnim_cmd ( *args ) :
        import os ;
        os.startfile ( r'P:\_TechAnim' ) ;

    def twoHeroes_browseDaily_cmd ( *args ) :
        import os ;
        os.startfile ( r'P:\Doublemonkeys\all\daily' ) ;

    def twoHeroes_browseAlternate_cmd ( *args ) :
        import os ;
        os.startfile ( r'P:\Two_Heroes\FTP\ATL' ) ;

    def twoHeroes_browseScene_cmd ( *args ) :
        import os ;
        os.startfile ( r'P:\Two_Heroes\scene' ) ;

    def twoHeroes_browseMaya2017Updates_cmd ( *args ) :
        import os ;
        os.startfile ( r'\\riffarchive\wares\3D\Autodesk\Maya\2017' ) ;

    def twoHeroes_shortcut_layout ( *args ) :

        pm.button ( label = 'tech anim share' , c = browseTechAnim_cmd , bgc = ( 1 , 0.4 , 0.7 ) , w = width ) ;

        with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
        # Two Heroes Browse

            pm.button ( label = 'daily' , c = twoHeroes_browseDaily_cmd , w = width/2 ) ;
            pm.button ( label = 'client (alternate)' ,  c = twoHeroes_browseAlternate_cmd , w = width/2 ) ;
            pm.button ( label = 'project scene' ,  c = twoHeroes_browseScene_cmd , w = width/2 ) ;
            pm.button ( label = 'maya 2017 updates', c = twoHeroes_browseMaya2017Updates_cmd , w = width/2 ) ;

    twoHeroes_shortcut_layout ( ) ;

def twoHeroes_namingConvention_layout ( *args ) :

    with pm.frameLayout ( label = 'naming convention' , collapsable = True , collapse = False , cc = frameLayout_cc , bgc = ( 0.2 , 0.3 , 0.2 ) ) :
    
        with pm.rowColumnLayout ( nc = 1 , cw = [ ( 1 , width ) ] ) :

            pm.text ( label = 'cloth ( and body ) cache' , align = 'left' ) ;
            pm.textField ( text = 'v001_001.sunnyCasual.geoGrp.abc' ) ;

            separator ( ) ;

            pm.text ( label = 'xGen cache' , align = 'left' ) ;
            pm.textField ( text = 'v001_001.sunnyCasual.xGen_eyeBrow.abc' ) ;

            separator ( ) ;
            
            pm.text ( label = 'transform group' , align = 'left' ) ;
            pm.textField ( text = 'v001_001.transformGrp.ma' ) ;

def twoHeroes_tabLayout_grp ( *args ) :

    with pm.scrollLayout (
        w               = mainLayoutWidth ,
        h               = 400 ,
        childResizable  = True ,
        minChildWidth   = width , 
        borderVisible   = False
        ) :

        twoHeroes_InfoLayout_grp ( ) ;
        separator ( ) ;
        twoHeroes_projectDocLayout_grp ( ) ;
        separator ( ) ;
        twoHeroes_utilitiesLayout_grp ( ) ;
        separator ( ) ;
        twoHeroes_shortcutLayout_grp ( ) ;
        separator ( ) ;
        twoHeroes_namingConvention_layout ( ) ;