import sys ;
import os ;
import pymel.core as pm ;
import maya.cmds as mc ;

selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;
# C:/Users/Administrator/Desktop/script/birdScript/library/

configTextPath = selfPath + '/mainUI_elem/mainUI_config.txt' ;

configFile = open ( configTextPath , 'a+' ) ;
configText = configFile.read ( ) ;
configFile.close ( ) ;

exec ( configText ) ;

width = config_dict [ 'width' ] ;
#print width ;

###

title = 'technical animation utilities'
title += ' ' ;
title += config_dict [ 'scriptVersion' ] ;
title += ' ' ;
title += config_dict [ 'lastModified' ] ;

#mp = "C:/Users/Administrator/Desktop/script/birdScript" ;
mp = selfPath.split('/library')[0] ;
#C:/Users/Administrator/Desktop/script/birdScript

if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
else : pass ;

'''
# import SoftClusterEX ;
scp = ( os.path.expanduser('~/Documents/maya/scripts/SoftClusterEX') ) ;
if not scp in sys.path :
    sys.path.append ( mp ) ;
'''
 
import library.mayaLibrary as mlb ;

### turn on neccessary plugin

import pymel.core as pm ;

if pm.pluginInfo ( 'AbcExport.mll' , q = True , loaded = True ) == True :
    pass ;
else :
    pm.loadPlugin ( 'AbcExport.mll' ) ;

if pm.pluginInfo ( 'AbcImport.mll' , q = True , loaded = True ) == True :
    pass ;
else :
    pm.loadPlugin ( 'AbcImport.mll' ) ;

if pm.pluginInfo ( 'matrixNodes.mll' , q = True , loaded = True ) == True :
    pass ;
else :
    pm.loadPlugin ( 'matrixNodes.mll' ) ;

pm.undoInfo ( state = True ) ;

fpsDic = {
    15 : 'game' ,
    24 : 'film' , 
    25 : 'pal' ,
    30 : 'ntsc' ,
    48 : 'show' ,
    50 : 'palf' ,
    60 : 'ntscf' ,
    } ;

pm.currentUnit ( time = fpsDic [ 24 ] ) ;

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

def birdScript ( *args ) :
    import os ;
    os.startfile ( r'P:\_bird\birdScript' ) ;

##### Projects TAB #####
    
##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################
''' Two Heroes '''
##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################

def twoHeroes_tabLayout_grp ( *args ) :
    import library.mainUI_elem.mainUI_project_twoHeroes as mainUI_project_twoHeroes ;
    reload ( mainUI_project_twoHeroes ) ;
    mainUI_project_twoHeroes.twoHeroes_tabLayout_grp ( ) ;

##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################
''' LOCAL '''
##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################

def royalBloodDoc ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1t3r_TFQPkMEMYGHLiAhv9QoG0Wp3NlR1Ft-zAWd__Ts/edit?usp=sharing' ) ;

def FMP_royalBloodDoc ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1ziDGxJK0f_3Tmb99dDf6fYI2e2qSg1ntO0iK8WBXk04/edit?usp=sharing' ) ;

def projectManager ( *args ) :
    import projects.local.projectManager as projectManager ;
    reload ( projectManager ) ;
    projectManager.projectManagerUI ( ) ;

##### GENERAL TAB #####

def printScenePath ( *args ) :
    import general.printSelfPath as psp ;
    reload ( psp ) ;
    psp.printPath ( ) ;

def showHiddenAttribute ( *args ) :
    import general.showHiddenAttribute as sha ;
    reload ( sha ) ;
    sha.showHiddenAttribute ( ) ;

##### RIG TAB #####

def follicles ( *args ) :
    import rig.follicles.folliclesUI as fol ;
    reload ( fol ) ;
    fol.folUI ( ) ;

def sk_attrShiftUp ( *args ) :
    import external.sk_attrShift as ats ;
    reload ( ats ) ;
    ats.shiftUp ( ) ;

def sk_attrShiftDown ( *args ) :
    import external.sk_attrShift as ats ;
    reload ( ats ) ;
    ats.shiftDown ( ) ;

def createHairJoint ( *args ) :
    import rig.createHairJoint as createHairJoint ;
    reload ( createHairJoint ) ;
    createHairJoint.createHairJointUI ( ) ;

def cometRename ( *args ) :
    import external.cometRename as cometRename ;
    reload ( cometRename ) ;
    cometRename.cometRename ( ) ;

def jointSizeSlider_cmd ( *args ) :
    value = pm.floatSliderGrp ( 'jointSizeSlider' , q = True , v = True ) ;
    pm.jointDisplayScale ( value ) ;


##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################
''' SIM TAB '''
##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################

def simulationDocumentLayout_grp ( *args ) :

    import general.utilities as utl ;
    reload ( utl ) ;

    def simulationFAQ_cmd ( *args ) :
        utl.openLink ( link = r'https://docs.google.com/document/d/1wBDtlqjuhFR1ygm1Hj-C71ezWId8k2jaAlAz-DVq0MI/' ) ;

    def simulationDocument_cmd ( *args ) :
        utl.openLink ( link = r'https://docs.google.com/document/d/18fHgP6jI7YU7mL0VrnPAXsR9yYLcsD0ITXbMFszr_mY/' ) ;

    def juneSimulationFAQ_cmd ( *args ) :
        utl.openLink ( link = r'https://docs.google.com/document/d/1BO6fZQO8TwExuCJrqmGWL7sKtEykKVDdH1S7bube_KE/' ) ;

    ##################
    ''' layout '''
    ##################
    pm.button ( label = 'simulation FAQ' , c = simulationFAQ_cmd , bgc = ( 1 , 1 , 0 ) , w = width/2 ) ;
    pm.button ( label = 'simulation document' , c = simulationDocument_cmd , w = width/2 ) ;
    pm.button ( label = 'simulation attr definition (June)' , c = juneSimulationFAQ_cmd , w = width/2 ) ;


def simUtilitiesLayout_grp ( *args ) :

    def simUtilities_cmd ( *args ) :
        import sim.simUtilities as sut ;
        reload ( sut ) ;
        sut.simUtilities ( ) ;

    def rivet_cmd ( *args ) :
        import rig.rivet as riv ;
        reload ( riv ) ;
        riv.rivet ( ) ;

    def absoluteTrack_cmd ( *args ) :
        import sim.absoluteTrack.absoluteTrackUI as abt ;
        reload ( abt ) ;
        abt.absoluteTrackUI ( ) ;

    def attributeTuner_cmd ( *args ) :
        import sim.attributeTuner.general.attributeTunerUI as att ;
        reload ( att ) ;
        att.attrTunerUI ( ) ;

    ##################
    ''' layout '''
    ##################
    pm.button ( label = 'sim utilities' , c = simUtilities_cmd , bgc = ( 0 , 1 , 1 ) ) ;

    with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/4 ) , ( 2 , width/4 ) ] ) :
        pm.button ( label = 'rivet' , c = rivet_cmd ) ;
        pm.button ( label = 'absolute track' , c = absoluteTrack_cmd ) ;

    pm.button ( label = 'attibute tuner' , c =  attributeTuner_cmd ) ;
    
def hairUtilitiesLayout_grp ( *args ) :

    def hairSettingUtilities_cmd ( *args ) :
        import sim.hairSettingUtilities as hairSettingUtilities ;
        reload ( hairSettingUtilities ) ;
        hairSettingUtilities.run ( ) ;

    def fee_nucleusSettingUtilities_cmd ( *args ) :
        import sim.fee_nucleusSettingUtilities as fee_nucleusSettingUtilities ;
        reload ( fee_nucleusSettingUtilities ) ;
        fee_nucleusSettingUtilities.run ( ) ;

    def hairBlendShape_cmd ( *args ) :
        import sim.hairBlendShape as hairBlendShape ;
        reload ( hairBlendShape ) ;
        hairBlendShape.run ( ) ;

    ##################
    ''' layout '''
    ##################
    pm.button ( label = 'hair setting utilities' , c = hairSettingUtilities_cmd , bgc = ( 1 , 0.4 , 0.1 ) ) ;
    pm.button ( label = "fee's nucleus setting utilities" , c = fee_nucleusSettingUtilities_cmd )
    pm.button ( label = 'hair blendshape' , c = hairBlendShape_cmd ) ;

def otherUtilitiesLayout_grp ( *args ) :

    def customBlendShape_cmd ( *args ) :
        import general.utilities as utl ;
        reload ( utl ) ;
        utl.customBlendShape ( ) ;

    def createBwdFwdGrp_cmd ( *args ) :
        import sim.createBwdFwdGrp as createBwdFwdGrp ;
        reload ( createBwdFwdGrp ) ;
        createBwdFwdGrp.run ( ) ;

    def multipleNucleus_colliderSetUp_cmd ( *args ) :
        import sim.multipleNucleus_colliderSetUp as multipleNucleus_colliderSetUp ;
        reload ( multipleNucleus_colliderSetUp ) ;
        multipleNucleus_colliderSetUp.run ( ) ;

    def setTextureFilePath_cmd ( *args ) :
        import pymel.core as pm ;
        selection = pm.ls ( sl = True ) ;
        for each in selection :
            try :
                file = pm.general.PyNode ( each ) ;
                name = file.fileTextureName.get ( ) ;
                name = name.split ( 'data' ) [1] ;
                name = 'data' + name ;
                file.fileTextureName.set ( name ) ;
                newName = file = pm.general.PyNode ( each ) ;
                print newName ;
            except : pass ;

    ##################
    ''' layout '''
    ##################
    pm.button ( label = 'blendshape' , c = customBlendShape_cmd , bgc = ( 1 , 1 , 1 ) ) ;
    pm.button ( label = 'create origin / anim group' , c = createBwdFwdGrp_cmd ) ;
    pm.button ( label = 'create multiple nucleus collider' , c = multipleNucleus_colliderSetUp_cmd ) ;
    pm.button ( label = 'set texture file path' , c = setTextureFilePath_cmd ) ;


def clothSetupLayout_grp ( *args ) :

    def clothSimSetUp_cmd ( *arg ) :
        bp = mp + '/external/BOUTPython' ;    
        if not bp in sys.path :
            sys.path.append ( bp ) ;
        import ClothSimScript.ClothSimSetUp as bpcs ;
        reload ( bpcs ) ;

    def makeDYNSetUp_cmd ( *args ) :
        bp = mp + '/external/BOUTPython' ;    
        if not bp in sys.path :
            sys.path.append ( bp ) ;

        import ClothSimScript.MakeDYNSetUp as bmdyn ;
        reload ( bmdyn ) ;

    def makeNRigidSetUp_cmd ( *args ) :
        bp = mp + '/external/BOUTPython' ;    
        if not bp in sys.path :
            sys.path.append ( bp ) ;

        import ClothSimScript.MakeNRigidSetUp as bmnrg ;
        reload ( bmnrg ) ;
        bmnrg.main ( ) ;

    def renameNCloth_cmd ( *args ) :
        bp = mp + '/external/BOUTPython' ;    
        if not bp in sys.path :
            sys.path.append ( bp ) ;
        import ClothSimScript.RenameNClothNRigid as brncr ;
        reload ( brncr ) ;
        brncr.main ( ) ;

    def renameNConstraint_cmd ( *args ) :
        bp = mp + '/external/BOUTPython' ;    
        if not bp in sys.path :
            sys.path.append ( bp ) ;
        import ClothSimScript.RenameNConstraint as brnct ;
        reload ( brnct ) ;
        brnct.main ( ) ;

    ##################
    ''' layout '''
    ##################
    clothSetup_layout = pm.frameLayout ( label = 'cloth setup' , collapsable = True , collapse = True , cc = frameLayout_cc , bgc = ( 0.2 , 0.3 , 0.2 ) ) ;
    with clothSetup_layout :
        
        with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width/2 ) ] ) :    
            pm.button ( label = 'cloth sim setup (monk)' , c = clothSimSetUp_cmd , bgc = ( 0.5 , 1 , 0 ) ) ;
            
            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/4 ) , ( 2 , width/4 ) ] ) :
                pm.button ( label = 'make DYN setup' , c = makeDYNSetUp_cmd ) ;
                pm.button ( label = 'make nRigid setup' , c = makeNRigidSetUp_cmd ) ;

                pm.button ( label = 'rename nCloth' , c = renameNCloth_cmd ) ;
                pm.button ( label = 'rename nConstraint' , c = renameNConstraint_cmd  ) ;

def hairSetupLayout_grp ( *args ) :

    def hairSetupUtilities_cmd ( *args ) :
        import sim.hairSetupUtilities as hairSetupUtilities ;
        reload ( hairSetupUtilities ) ;
        hairSetupUtilities.run ( ) ;

    def extrudeHairTube_cmd ( *args ) :
        import sim.extrudeHairTube as eht ;
        reload ( eht ) ;
        eht.extrudeHairTube (selection = pm.ls ( sl = True )) ;

    ##################
    ''' layout '''
    ##################
    hairSetup_layout = pm.frameLayout ( label = 'hair setup' , collapsable = True , collapse = True , cc = frameLayout_cc , bgc = ( 0.3 , 0.3 , 0 ) ) ;
    with hairSetup_layout :
        with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width/2 ) ] ) :
            pm.button ( label = 'hair setup utilities' , c = hairSetupUtilities_cmd , bgc = ( 1 , 1 , 0 )  ) ;
            pm.button ( label = 'extrude hair tube along curve' , c = extrudeHairTube_cmd ) ;

def pMuscleLayout_grp ( *args ) :

    def pMuscleSetup_cmd ( *args ) :
        import sim.pMuscleSetup as pMuscleSetup ;
        reload ( pMuscleSetup ) ;
        pMuscleSetup.run ( ) ;

    ##################
    ''' layout '''
    ##################
    pMuscle_layout = pm.frameLayout ( label = 'pMuscle setup' , collapsable = True , collapse = True , cc = frameLayout_cc , bgc = ( 0.6 , 0.3 , 0.2 ) ) ;
    with pMuscle_layout :
        with pm.rowColumnLayout ( nc = 1 , cw = [ ( 1 , width/2 )] ) :
            pm.button ( label = 'pMuscle setup' , c = pMuscleSetup_cmd , bgc = ( 1 , 0.55 , 0 ) ) ;

def curveUtilitiesLayout_grp ( *args ) :

    def controlMaker_cmd ( *args ) :
        import external.pk.controlMaker as controlMaker ;
        reload ( controlMaker ) ;
        controlMaker.run ( ) ;

    def curveEPMelToPython_cmd ( *args ) :
        import general.curveEPMelToPython as curveEPMelToPython ;
        reload ( curveEPMelToPython ) ;
        curveEPMelToPython.run ( ) ;

    def curveCreator_cmd ( *args ) :
        import general.curveCreator as curveCreator ;
        reload ( curveCreator ) ;
        curveCreator.run ( ) ;

    ##################
    ''' layout '''
    ##################
    curveUtilities_layout = pm.frameLayout ( label = 'curve utilities' , collapsable = True , collapse = True , cc = frameLayout_cc , bgc = ( 0 , 0 , 0.2 ) ) ;
    with curveUtilities_layout :

        with pm.rowColumnLayout ( nc = 1 ) :

            pm.button ( label = 'pk controller maker' , c = controlMaker_cmd ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/4 ) , ( 2 , width/4 ) ] ) :
                pm.button ( label = 'curve EP mel to python' , c = curveEPMelToPython_cmd ) ;
                pm.button ( label = 'curve creator' , c = curveCreator_cmd ) ;

def colorUtilitiesLayout_grp ( *args ) :

    def assignDNshader_cmd ( *args ) :        
        bp = mp + '/external/BOUTPython' ;    
        if not bp in sys.path :
            sys.path.append ( bp ) ;
        import ClothSimScript.AssignDNshader as badns ;
        reload ( badns ) ;

    def outlinerColorAssigner_cmd ( *args ) :
        import sim.outlinerColorAssigner as outlinerColorAssigner ;
        reload ( outlinerColorAssigner ) ;
        outlinerColorAssigner.run ( ) ;

    def importRampShader_cmd ( *args ) :
        rampShaderPath = mp + '/' + 'asset/shader_lambertRamp.ma' ;
        pm.system.importFile ( rampShaderPath ) ;

    def importCheckerShader_cmd ( *args ) :
        rampShaderPath = mp + '/' + 'asset/shader_checker.ma' ;
        pm.system.importFile ( rampShaderPath ) ;

    def randomCurveColor_cmd ( *args ) :
        import sim.randomCurveColor as randomCurveColor ;
        reload ( randomCurveColor ) ;
        randomCurveColor.randomCurveColor ( operation = 'enable' ) ;

    def disableRandomCurveColor_cmd ( *args ) :
        import sim.randomCurveColor as randomCurveColor ;
        reload ( randomCurveColor ) ;
        randomCurveColor.randomCurveColor ( operation = 'disable' ) ;

    ##################
    ''' layout '''
    ##################
    colorUtilties_layout = pm.frameLayout ( label = 'color utilities' , collapsable = True , collapse = True , cc = frameLayout_cc , bgc = ( 0.4 , 0 , 0 ) ) ;
    with colorUtilties_layout :

        with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width/2 ) ] ) :
            pm.button ( label = 'assign shade' , c = assignDNshader_cmd ) ;
            pm.button ( label = 'outliner color assigner' , c = outlinerColorAssigner_cmd ) ;

            separator ( ) ;

            pm.text ( label = 'import ...' )
            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/4 ) , ( 2 , width/4 ) ] ) :
                pm.button ( label = 'ramp shaders' , c = importRampShader_cmd ) ;
                pm.button ( label = 'checker shader' , c = importCheckerShader_cmd ) ;

            separator ( ) ;

            pm.text ( label = "help: select curves' shape then run the script" ) ;
            with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/4 ) , ( 2 , width/4 ) ] ) :
                pm.button ( label = 'randomize curve color' , c = randomCurveColor_cmd )  ;
                pm.button ( label = 'disable curve color' , c = disableRandomCurveColor_cmd )  ;

def setupLayout_grp ( *args ) :
    with pm.frameLayout ( label = 'setup' , collapsable = True , collapse = True , cc = frameLayout_cc , bgc = ( 0 , 0 , 0 ) ) :
        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2/6*2 ) , ( 2 , width/2/6*4 ) ] ) :
            filler ( ) ;
            with pm.rowColumnLayout ( nc = 1 , w = width/2/6*4 ) :
                clothSetupLayout_grp ( ) ;
                separator ( ) ;    
                hairSetupLayout_grp ( ) ;
                separator ( ) ;
                pMuscleLayout_grp ( ) ;

def createWindRig ( *args ) :
    import sim.createWindRig as createWindRig ;
    reload ( createWindRig ) ;
    createWindRig.run ( ) ;

def windPresetManager ( *args ) :
    import sim.presetManager.windPresetManager as windPresetManager ;
    reload ( windPresetManager ) ;
    windPresetManager.run ( ) ;

def simETC ( *args ) :
    import sim.simETC as simETC ;
    reload ( simETC ) ;
    simETC.run ( ) ;

def deleteShapeOrig ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.deleteShapeOrig_run ( ) ;

def freezeTransform_deleteHistory ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.freezeTransform_deleteHistory_run ( ) ;

def centerPivot ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.centerPivot_run ( ) ;

def deleteRebuiltShape ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.deleteRebuiltShape_run ( ) ;
        
##### MAYA HELP #####

def openFB ( *arg ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://www.facebook.com/messages/t' ) ;

def openRiffPrivate ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://www.facebook.com/groups/383908845064813/' ) ;

def openHelp ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'http://help.autodesk.com/view/MAYAUL/2017/ENU/' ) ;

def openScriptNotes ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/14bk94x12RL3u0SB97ugkfn398KOYfi42fOmckQR0Skk' ) ;

def openScriptEditor ( *args ) :
    command = '''
ScriptEditor;
if (`scriptedPanel -q -exists scriptEditorPanel1`) { scriptedPanel -e -tor scriptEditorPanel1; showWindow scriptEditorPanel1Window; selectCurrentExecuterControl; }else { CommandWindow; };
'''
    pm.mel.eval ( command ) ;

##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################
''' RESOURCES '''
##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################

def resourceTabLayout_grp ( *args ) :

    import general.utilities as utl ;
    reload ( utl ) ;

    def completePythonBootcamp_cmd ( *args ) :
        utl.openLink ( link = r'http://nbviewer.jupyter.org/github/jmportilla/Complete-Python-Bootcamp/tree/master/' ) ;

    def mayaGUISnippets_cmd ( *args ) :
        wp = os.path.realpath (__file__) ;
        fileName = os.path.basename (__file__) ;
        wp = wp.partition ( 'library' ) [0] ;
        wp = wp + r'general/website/mayaGUISnippets.html' ;
        utl.openLink ( link = wp ) ;

    def mayaColorNames_cmd ( *args ) :
        wp = os.path.realpath (__file__) ;
        fileName = os.path.basename (__file__) ;
        wp = wp.partition ( 'library' ) [0] ;
        wp = wp + r'general\website\MolScript v2.1_ Colour names.html' ;
        utl.openLink ( link = wp ) ;

    def googleSpreadsheetsPython_cmd ( *args ) :
        wp = os.path.realpath (__file__) ;
        fileName = os.path.basename (__file__) ;
        wp = wp.partition ( 'library' ) [0] ;
        wp = wp + r'general\website\Google Spreadsheets and Python.html' ;
        utl.openLink ( link = wp ) ;

    def cgcircuitBrowse_cmd ( *args ) :
        import general.utilities as utl ;
        reload ( utl ) ;
        utl.openLink ( link = 'cgcircuit.com' ) ;

    localWidth = width/2 ;

    with pm.rowColumnLayout ( 'resources' ,  nc = 1 , cw = ( 1 , localWidth ) ) :

        pm.button ( label = 'complete python bootcamp (online)' , w = localWidth , bgc = ( 0.95 , 0.85 , 0.5) , c = completePythonBootcamp_cmd ) ;
        
        separator ( ) ;

        pm.button ( label = 'maya GUI snippets' , w = localWidth , c = mayaGUISnippets_cmd ) ;    
        pm.button ( label = 'colour names (RGB 0 - 1)' , w = localWidth , c = mayaColorNames_cmd ) ;
        pm.button ( label = 'google spreadsheets & python' , w = localWidth , c = googleSpreadsheetsPython_cmd ) ;

        separator ( ) ;

        pm.button ( label = 'cg circuit' , w = localWidth , c = cgcircuitBrowse_cmd ) ;

        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , localWidth/5 ) , ( 2 , localWidth/5*4 ) ] ) :
            pm.text ( label = 'ID' ) ;
            pm.textField ( tx = 'contact@riffcg.com' ) ;
            pm.text ( label = 'PASS' ) ;
            pm.textField ( tx = 'RiffRiff' ) ;

##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################
''' SCRIPT TEMPLATE '''
##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################

def printClass ( *args ) :
    import general.text.textReader as trd ;
    reload ( trd ) ;
    trd.printClass ( ) ;

def printWindow ( *args ) :
    import general.text.textReader as trd ;
    reload ( trd ) ;
    trd.printWindow ( ) ;

def printPyNode_Attribute ( *args ) :
    import general.text.textReader as trd ;
    reload ( trd ) ;
    trd.printPyNode_Attribute ( ) ;

def printExec ( *args ) :
    import general.text.textReader as trd ;
    reload ( trd ) ;
    trd.printExec ( ) ;

def printNestingDictionaries ( *args ) :
    import general.text.textReader as trd ;
    reload ( trd ) ;
    trd.printNestingDictionaries ( ) ;

def printSlicingString ( *args ) :
    import general.text.textReader as trd ;
    reload ( trd ) ;
    trd.printSlicingString ( ) ;

def printSelfPath ( *args ) :
    import general.text.textReader as trd ;
    reload ( trd ) ;
    trd.printSelfPath ( ) ;

def printVersionCheck ( *args ) :
    import general.text.textReader as trd ;
    reload ( trd ) ;
    trd.printVersionCheck ( ) ;

def printMayaSelfPath ( *args ):
    import general.text.textReader as trd ;
    reload ( trd ) ;
    trd.printMayaSelfPath ( ) ;

def printUsingSet ( *args ) :
    import general.text.textReader as trd ;
    reload ( trd ) ;
    trd.printUsingSet ( ) ;

def printOs ( *args ) :
    import general.text.textReader as trd ;
    reload ( trd ) ;
    trd.printOs ( ) ;

##### EXTERNAL #####

def boutCondo ( *args ) :
    
    bp = mp + '/external/BOUTPython' ;
    
    if not bp in sys.path :
        sys.path.append ( bp ) ;
    
    import ClothSimScript.ClothSimCondo as ClothSimCondo ;
    reload ( ClothSimCondo ) ;

def ffWind ( *args ) :

    ffwp = mp + '/external/FFWind' ;

    if not ffwp in sys.path :
        sys.path.append ( ffwp ) ;

    import connectWind ;
    reload ( connectWind ) ;

    connectWind.main ( ) ;

def UnlockSoftenEdge ( *arg ) :

    bp = mp + '/external/BOUTPython' ;    
    if not bp in sys.path :
        sys.path.append ( bp ) ;

    import ClothSimScript.UnlockSoftenEdge as buse ;
    reload ( buse ) ;
    buse.main ( ) ;

def DeletePolyAV ( *arg ) :

    bp = mp + '/external/BOUTPython' ;    
    if not bp in sys.path :
        sys.path.append ( bp ) ;

    import ClothSimScript.DeletePolyAV as bdav ;
    reload ( bdav ) ;
    bdav.main ( ) ;


def ReOrder ( *arg ) :

    bp = mp + '/external/BOUTPython' ;    
    if not bp in sys.path :
        sys.path.append ( bp ) ;

    import ClothSimScript.ReOrder as bro ;
    reload ( bro ) ;
    bro.main ( ) ;

def softCluster_copyToMayaScript_cmd ( *args ) :
    import sim.softCluster as softCluster ;
    reload ( softCluster ) ;
    softCluster.copyToMayaScript ( ) ;

##### WIP #####

def nClothPresetManager ( *args ) :
    import sim.nClothPresetManager as nClothPresetManager ;
    reload ( nClothPresetManager ) ;
    nClothPresetManager.run ( ) ;

def COLsetup_cmd ( *args ) :
    import wip.setUpCol as setUpCol ;
    reload ( setUpCol ) ;
    setUpCol.createCharacterLoc ( ) ;

##### ETC #####

def frameLayout_cc ( *args ) :
 
    pm.window ( 'birdMainUI', e = True , h = 10 ) ;

##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################
''' MAIN UI '''
##########################################################################################################################################################################
##########################################################################################################################################################################
##########################################################################################################################################################################

def run ( ) :
    
    # check if window exists

    if pm.window ( 'birdMainUI' , exists = True ) :
        pm.deleteUI ( 'birdMainUI' ) ;
        
    # create window 

    window = pm.window ( 'birdMainUI', title =  title , mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    window = pm.window ( 'birdMainUI', e = True , width = width , h = 10 ) ;
    with window :

        # title
        titleLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] ) ;
        with titleLayout :

            #pm.picture ( )
            pm.symbolButton ( image = selfPath + '/' + 'mainUI_elem/mainUI_title.jpg' , width = 500 , height = 50 , c = birdScript ) ;

        mainLayout = pm.rowColumnLayout ( nc = 2 , w = width/2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) ;
        with mainLayout :

            projects_tabLayout = pm.tabLayout ( 'projects' , innerMarginWidth = 5 , innerMarginHeight = 5 ) ;    
            with projects_tabLayout :

                twoHeroes_tab = pm.rowColumnLayout ( 'TwoHeroes' ,  nc = 1 , cw = ( 1 , width/2 ) ) ;
                with twoHeroes_tab :

                    twoHeroes_tabLayout_grp ( ) ;
        
                local_tab = pm.rowColumnLayout ( 'Local' ,  nc = 1 , cw = ( 1 , width/2 ) ) ;
                with local_tab :

                    pm.button ( label = "client's Royal Blood doc" , c = FMP_royalBloodDoc , bgc = ( 1 , 0.08 , 0.55 )) ;
                    pm.button ( label = 'Royal Blood Doc' , c = royalBloodDoc , bgc = ( 1 , 0.7 , 0.75 ) ) ;
                    pm.button ( label = 'project manager' , c = projectManager , bgc = ( 1 , 0.65 , 0 ) ) ;
        
            utilities_tabLayout = pm.tabLayout ( innerMarginWidth = 5 , innerMarginHeight = 5 ) ;
            with utilities_tabLayout :

                # ---------- #
                ### sim tab ###
                # ---------- #
                sim_tab = pm.rowColumnLayout ( 'sim' , nc = 1 , cw = ( 1 , width/2 ) ) ;
                with sim_tab :

                    simulationDocumentLayout_grp ( ) ;
                    separator ( ) ;
                    simUtilitiesLayout_grp ( ) ;
                    separator ( ) ;
                    hairUtilitiesLayout_grp ( ) ;
                    separator ( ) ;
                    otherUtilitiesLayout_grp ( ) ;

                    separator ( ) ;

                    setupLayout_grp ( ) ;
                    
                    separator ( ) ;
                    curveUtilitiesLayout_grp ( ) ;
                    separator ( ) ;
                    colorUtilitiesLayout_grp ( ) ;


                    separator ( ) ;
     
                    wind_layout = pm.frameLayout ( label = 'wind utilities' , collapsable = True , collapse = True , cc = frameLayout_cc , bgc = ( 0.2 , 0.3 , 0.3 ) ) ;
                    with wind_layout :

                        with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width / 2.0 ) ] ) :

                            with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width / 4.0 ) , ( 2 , width / 4.0 ) ] ) :

                                pm.button ( label = 'connect wind' , c = ffWind , bgc = ( 0 , 0.55 , 0.6 ) ) ;
                                pm.button ( label = 'create wind rig' , c = createWindRig , bgc = ( 0.12 , 0.7 , 0.7 ) ) ;

                            pm.button ( label = 'wind preset manager (Two Heroes)' , c = windPresetManager , bgc = ( 0.24 , 0.85 , 0.8 ) ) ;

                    separator ( ) ;

                    cleaningUtilities_layout = pm.frameLayout ( label = 'cleaning utilities' , collapsable = True , collapse = True , cc = frameLayout_cc , bgc = ( 0 , 0 , 0 ) ) ;
                    with cleaningUtilities_layout :

                        with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :

                            pm.button ( label = 'unlock and soften edge' , c = UnlockSoftenEdge , w = width/2 ) ;
                            pm.button ( label = 'delete average vertex' , c = DeletePolyAV , w = width/2 ) ;
                            pm.button ( label = 'reorder' , c = ReOrder , w = width/2 ) ;

                            separator ( ) ;

                            pm.button ( label = 'delete curve rebuilt shape' , c = deleteRebuiltShape , w = width/2 ) ;
                            pm.button ( label = 'delete orig shape' , c = deleteShapeOrig , w = width/2 ) ;
                            pm.button ( label = 'freeze transform / delete history' , c = freezeTransform_deleteHistory , w = width/2 ) ;
                            pm.button ( label = 'center pivot' , c = centerPivot , w = width/2 ) ;

                    separator ( ) ;

                    test_layout = pm.frameLayout ( label = 'test / WIP' , collapsable = True , collapse = True , cc = frameLayout_cc , bgc = ( 0.5 , 0 , 0.5 ) ) ;
                    with test_layout :


                        with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :

                            with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/4 ) , ( 2 , width/4 ) ] ) :

                                pm.button ( label = 'nCloth preset (WIP)' , c = nClothPresetManager , bgc = ( 0 , 0 , 0 ) ) ;
                                pm.button ( label = 'nHair preset (WIP)' , c = '' , bgc = ( 0 , 0 , 0 ) ) ;

                            pm.button ( label = 'set up col' , c = COLsetup_cmd ) ;
                            
                            def preRollCluster_cmd ( *args ) :
                                import wip.prerollCluster as prerollCluster ;
                                reload ( prerollCluster ) ;
                                prerollCluster.run ( ) ;
                            pm.button ( label = 'preroll cluster (WIP)' , c = preRollCluster_cmd , bgc = ( 0 , 0 , 0 ) ) ;

                            def absoluteTrackRevamp_cmd ( *args ) :
                                import wip.absoluteTrackRevamp as absoluteTrackRevampTest ;
                                reload ( absoluteTrackRevampTest ) ;
                                absoluteTrackRevampTest.run ( ) ;
                            pm.button ( label = 'absoluteTrackRevamp (WIP)' , c = absoluteTrackRevamp_cmd , bgc = ( 0 , 0 , 0 ) ) ;

                    separator ( ) ;

                    pm.button ( label = 'bird miscellaneous condo' , c = simETC , bgc = ( 1 , 1 , 0 ) ) ;
        
                # ---------- #
                ### rig tab ###
                # ---------- #
                rig_tab = pm.rowColumnLayout ( 'rig' , nc = 1 , cw = ( 1 , width/2 ) ) ;
                with rig_tab :

                    pm.floatSliderGrp ( 'jointSizeSlider' , label = 'joint size' , field = True , minValue = 0.01 , precision = 2 ,
                        columnWidth3 = [ width/10 , width/10 , width/10*8 ] , columnAlign3 = [ "center" , "both" , "left" ] ,
                        value = pm.jointDisplayScale ( q = True ) , dc = jointSizeSlider_cmd ) ;

                    pm.button ( label = 'follicle utilities' , c = follicles , bgc = ( 0.85 , 0.08 , 0.25 ) ) ;
                    pm.button ( label = 'creat hair joint (wip)' , c = createHairJoint ) ;
                    
                    with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/4 ) , ( 2 , width/4 ) ] ) :
                        pm.button ( label = 'attribute up' , c = sk_attrShiftUp , bgc = ( 0.6 , 0.8 , 0.2 ) ) ;
                        pm.button ( label = 'attribute down' , c = sk_attrShiftDown , bgc = ( 0.2 , 0.55 , 0.35 ) ) ;
                    
                    pm.button ( label = 'cometRename' , c = cometRename , bgc = ( 1 , 0.9 , 0.7 ) ) ;
                    pm.button ( label = 'show hidden (general) attribute' , c = showHiddenAttribute ) ;
                    pm.button ( label = 'print self path' , c = printScenePath ) ;

                    separator ( ) ;

                    softCluster_layout = pm.frameLayout ( label = 'soft cluster' , collapsable = True , collapse = True , cc = frameLayout_cc , bgc = ( 1 , 0.25 , 0 ) ) ;
                    with softCluster_layout :

                        pm.button ( label = 'copy file' , c = softCluster_copyToMayaScript_cmd ) ;
                        helpText = 'after you click "copy file" button, drag install.mel into the maya scene' ;
                        pm.scrollField ( editable = False , ww = True , w = width/2/25*23 , text = helpText , h = 45 ) ;

                # ---------- #
                ### resources tab ###
                # ---------- #

                resourceTabLayout_grp ( ) ;                

                # ---------- #
                ### scriptTemplate tab ###
                # ---------- #

                template_tab = pm.rowColumnLayout (  'template' , nc = 1 , cw = [ ( 1 , width/2 ) ] ) ;
                with template_tab :

                    mayaTemplate_layout = pm.frameLayout ( label = 'MAYA' , collapsable = True , collapse = False , cc = frameLayout_cc , bgc = ( 0.1 , 0.1 , 0.5 )  ) ;
                    with mayaTemplate_layout :

                        with pm.rowColumnLayout (  nc = 2 , cw = [ ( 1 , width/4 ) , ( 2 , width/4 ) ] ) :

                            pm.button ( label = 'maya UI' , c = printWindow , bgc = ( 0.95 , 0.85 , 0.5 ) ) ;
                            pm.button ( label = 'PyNode : Attibutes' , c = printPyNode_Attribute ) ;
                            pm.button ( label = 'self path' , c = printMayaSelfPath  ) ;
                    
                    separator ( ) ;

                    pythonTemplate_layout = pm.frameLayout ( label = 'PYTHON' , collapsable = True , collapse = False , cc = frameLayout_cc , bgc = ( 0.1 , 0.1 , 0.5 )  ) ;
                    with pythonTemplate_layout :
                    
                        with pm.rowColumnLayout (  nc = 2 , cw = [ ( 1 , width/4 ) , ( 2 , width/4 ) ] ) :

                            pm.button ( label = 'get self path' , c = printSelfPath ) ;
                            pm.button ( label = 'version check' , c = printVersionCheck ) ;
                            pm.button ( label = 'class' , c = printClass ) ;
                            pm.button ( label = 'exec txt as python' , c = printExec ) ;
                            pm.button ( label = 'nesting dictionaries' , c = printNestingDictionaries ) ;
                            pm.button ( label = 'using set' , c = printUsingSet ) ;
                            pm.button ( label = 'os' , c = printOs ) ;
                            pm.button ( label = 'slicing string' , c = printSlicingString ) ;

        with mainLayout :

            with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/4 ) , ( 2 , width/4 ) ] ) :
                pm.button ( 'facebook chat' , w = width/4 , c = openFB ) ;
                pm.button ( 'RiFF Private' , w = width/4 , c = openRiffPrivate ) ;

            with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/4 ) , ( 2 , width/4 ) ] ) :
                pm.button ( 'maya help (2017)' , w = width/4 , c = openHelp ) ;
                pm.symbolButton ( image = selfPath + '/' + 'mainUI_elem/scriptEditor.png' , width = width/4 , c = openScriptEditor ) ;

    with pm.rowColumnLayout ( 'scriptNoteLayout' , nc = 1 , w = width ) :
        pm.button ( label = 'requests' , c = openScriptNotes , w = width , bgc = ( 0.65 , 1 , 0.2 ) ) ;

    pm.showWindow ( window ) ;
    
#birdMainUI () ;