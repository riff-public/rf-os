##############################################################################################################################
##############################################################################################################################
''' TABLE OF CONTENT '''
##############################################################################################################################
##############################################################################################################################

'''

navigate with "ctrl + f"

[ ISL_IDX ] INFO SECTION
[ DOC_IDX ] DOC SECTION
[ UTL_IDX ] UTILITIES SECTION
[ NAV_IDX ] NAVIGATION SECTION
[ NMC_IDX ] NAMING CONVENSTION SECTION

[ UIS_IDX ] UI
    [ UIF_IDX ] UI FUNCTIONS
    [ UIC_IDX ] UI CONTENT

'''

import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

##################
''' PATH '''
##################
# return : C:/Users/Administrator/Desktop/script/birdScript/library/
selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

mp = selfPath.split('/library')[0] ;
# mp = D:/Dropbox/script/birdScript
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
else : pass ;

##################
'''CONFIG'''
##################

def readText ( path , *args ) :
    # return text
    file = open ( path , 'r' ) ;
    text = file.read ( ) ;
    file.close ( ) ;
    return text ;

configText = readText ( path = selfPath + '/config.txt' ) ;
exec ( configText ) ;

globalWidth = config_dict [ 'width' ] ;
layoutWidth = globalWidth/2 ;
width = globalWidth/2*0.95 ;

##################
##################
''' [ ISL_IDX ] INFO SECTION '''
##################
##################

def darkHorse_setResolution_cmd ( *args ) :

    defaultResolution = pm.general.PyNode ( 'defaultResolution' ) ;

    defaultResolution.aspectLock.set ( 0 ) ;
    defaultResolution.width.set ( 3840 ) ;
    defaultResolution.height.set ( 2160 ) ;
    defaultResolution.dotsPerInch.set ( 72 ) ;
    defaultResolution.deviceAspectRatio.set( 1.778 );

#updateMayaSoftwareDeviceAspectRatio;
#updateMayaSoftwarePixelAspectRatio;

    pm.currentUnit ( time = 'ntsc' ) ;

    pm.select ( defaultResolution ) ;

    #pm.mel.eval ( 'ToggleAttributeEditor' ) ;
    #pm.mel.eval ( 'checkAspectLockHeight2 "defaultResolution"' ) ;
    #pm.mel.eval ( 'checkAspectLockWidth2 "defaultResolution"' ) ;

def resolutionGate_cmd ( *args ) :

    import maya.OpenMaya as OpenMaya
    import maya.OpenMayaUI as OpenMayaUI

    view = OpenMayaUI.M3dView.active3dView ( ) ;
    cam = OpenMaya.MDagPath()
    view.getCamera(cam)
    camPath = cam.fullPathName()

    camNode = pm.general.PyNode ( camPath ) ;

    status = pm.camera ( camPath , q = True , displayResolution = True )

    if camNode.overscan.get() != 1 :
        camNode.overscan.unlock() ;
        pm.disconnectAttr ( camNode.overscan ) ;

    if not status :
        pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = True , overscan = 1.0 , lockTransform = True ) ;
    else :
        pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = False , overscan = 1.0 , lockTransform = False ) ;

    camera = pm.general.PyNode ( camPath ) ;
    #cameraShape = pm.listRelatives ( camera , shapes = True ) [0] ;
    camera.displayGateMaskColor.set ( 0 , 0 , 0 ) ;
    camera.displayGateMaskOpacity.set ( 1 ) ;

    #setAttr "CAM:cam0030Shape.displayGateMaskColor" -type double3 0 0 0 ;
    #setAttr CAM:cam0030Shape.displayGateMaskOpacity 1;

def toggleAntiAliasing_cmd ( *args ) :
    
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    
    if hardwareRenderingGlobals.multiSampleEnable.get() == 0 :
        hardwareRenderingGlobals.multiSampleEnable.set(1) ;
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = True ) ;

    elif hardwareRenderingGlobals.multiSampleEnable.get() == 1 :
        hardwareRenderingGlobals.multiSampleEnable.set(0) ;
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = False ) ;

    sampleCountChange_cmd ( ) ;

def sampleCountChange_cmd ( *args ) :
    
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    
    multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , q = True , value = True ) ;
    multiSampleCount = int ( multiSampleCount ) ;
    hardwareRenderingGlobals.multiSampleCount.set ( multiSampleCount ) ;

def setDefaultMultiSampling_cmd ( *args ) :
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    pm.optionMenu ( 'sampleCount_ptm' , e = True , enable = hardwareRenderingGlobals.multiSampleEnable.get() ) ;

##################
##################
''' [ DOC_IDX ] DOC SECTION '''
##################
##################

def darkHorse_WIPSequences_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1218t3_T5wtQ678vSW5FHuy0bEPSdETS3HO-L5lmozeA/edit?ts=5c346e1c#gid=1709744959' ) ;

def darkHorse_Story_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1218t3_T5wtQ678vSW5FHuy0bEPSdETS3HO-L5lmozeA/edit?ts=5c346e1c#gid=2008602622' ) ;

def shotgun_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://riffanimation.shotgunstudio.com/page/7429' ) ;

def workplace_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://riffstudio.facebook.com' ) ;

def darkHorse_completedSequences_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1H6tig3gw843nu0IRl7b7LnGcpoJ4l1K9O9yeSImumwY/edit?usp=sharing' ) ;

def darkHorse_simulationBreakdown_cmd ( *args ) :
	import general.utilities as utl ;
	reload ( utl ) ;
	utl.openLink ( link = r'https://docs.google.com/spreadsheets/d/1DQe23etWowANUwMmBvlNiUtY5PHR8uapLzvWxRAxO2A/edit?ts=5b222473#gid=2123138132' )  ;

def darkHorse_simulationBreakdownBackup_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = r'https://docs.google.com/spreadsheets/d/1y7yx7s3YZCgV35NOH6Sc187OQsm6bpkOw_fPxSY-p4w/edit#gid=1866481003' )  ;

def darkHorse_openLayoutDoc_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1Aa8q0E6L8LZ-uFeN58gPKKH7ybQhGI_ee5twAubgdwI/edit#gid=0' ) ;

def darkHorse_openFacebook_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://www.facebook.com/groups/665802640258497/' ) ;

def darkHorse_openProjectTimeline_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1ywLsNq_yuS63o6uosc8Tzs66EoO1XeJ-9DljH4uJmIg/' ) ;

def darkHorse_openTeaserBreakdownDoc_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/1j-spZIgdJoUcOys0PbWttzd5wtoXMmjxdhaZb73h-gI/edit?ts=59d70bfc#gid=1097980972' ) ;

def darkHorse_openShotList_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/19_B9R0fvifJro6eycBZcYjrvRva2NZZhqQSTxu4slWM/' ) ;

def darkHorse_open_SLR_timeline_cmd ( *args ) :
    import os ;
    os.startfile ( r'P:\Doublemonkeys\all\doc\timeline\SLR_timeline' ) ;

def darkHorse_open_SLR_April2018 ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'https://docs.google.com/spreadsheets/d/17propT0lLUamureakZ1pCb5G4IiJWZNdt71vB_xdhBo/' ) ;\

def twoHeroes_cacheOutUtilGen2_cmd ( *args ) :
    import projects.twoHeroes.cacheOutUtilities_generation2.core as cacheOutUtilities_generation2 ;
    reload ( cacheOutUtilities_generation2 ) ;
    cacheOutUtilities_generation2.run ( ) ;
    

##################
##################
''' [ UTL_IDX ] UTILITIES SECTION '''
##################
##################

def darkHorse_autoNaming_cmd ( *args )  :
    import project.darkHorse.autonaming as thAN ;
    reload ( thAN ) ;
    thAN.TLo2H_ANUI ( ) ;

def darkHorse_autoNamingGeneration1_cmd ( *args )  :
    import project.darkHorse.autonaming_generation1 as thAN ;
    reload ( thAN ) ;
    thAN.TLo2H_ANUI ( ) ;

def darkHorse_autonamingGeneration2_cmd ( *args ) :
    import projects.darkHorse.autonaming_generation2 as autonaming_generation2 ;
    reload ( autonaming_generation2 ) ;
    project = 'darkHorse'
    autonaming_generation2.run ( project ) ;

def darkHorse_autonamingGeneration2_toShelf_cmd ( *args ) :
    import projects.darkHorse.autonaming_generation2 as autonaming_generation2 ;
    reload ( autonaming_generation2 ) ;
    autonaming_generation2.toShelf ( ) ;

def darkHorse_playblastViewer_cmd ( *args ) :
    import projects.darkHorse.playblastViewer as darkHorse_playblastViewer ;
    reload ( darkHorse_playblastViewer ) ;
    darkHorse_playblastViewer.run (  ) ;

def darkHorse_playblastViewer_toShelf_cmd ( *args ) :
    import projects.darkHorse.playblastViewer as darkHorse_playblastViewer ;
    reload ( darkHorse_playblastViewer ) ;
    darkHorse_playblastViewer.toShelf ( ) ;

def darkHorse_fileManager_cmd ( *args ) :

    rp = ('O:/Pipeline/core/maya') ;

    if not rp in sys.path :
        sys.path.insert ( 0 , rp ) ;

    from rftool.file_manager import fm_express_app as fm_app ;

    reload(fm_app) ;
    reload(fm_app.file_utils) ;
    reload(fm_app.path_info) ;

    reload(fm_app.config) ;

    reload(fm_app.path_info) ;
    fm_app.show() ;

def darkHorse_cacheOutUtil_cmd ( *args ) :
    import project.darkHorse.cacheOutUtilities as cou ;
    reload ( cou ) ;
    cou.animCacheOutUI ( ) ;

def darkHorse_cacheOutUtilGen2_cmd ( *args ) :
    import projects.darkHorse.cacheOutUtilities_generation2.core as cacheOutUtilities ;
    reload ( cacheOutUtilities ) ;
    cacheOutUtilities.run ( ) ;

def darkHorse_simPreRoll_cmd (*args):
    from rf_maya.rftool.simulation.sim.sim_preRoll import sprUI
    reload(sprUI)
    sprUI.preRollUI().sprUI()

def darkHorse_layoutOrganizer_cmd ( *args ) :
    import project.darkHorse.layoutOrganizer as darkHorse_layoutOrganizer ;
    reload ( darkHorse_layoutOrganizer ) ;
    darkHorse_layoutOrganizer.run ( ) ;

def darkHorse_layoutOrganizer_toShelf_cmd ( *args ) :
    import project.darkHorse.layoutOrganizer as darkHorse_layoutOrganizer ;
    reload ( darkHorse_layoutOrganizer ) ;
    darkHorse_layoutOrganizer.toShelf ( ) ;

def darkHorse_fee_colorAssigner_cmd ( *args ) :
    import external.RiFF.fee_colorAssigner.core as fee_colorAssigner ;
    reload ( fee_colorAssigner ) ;
    fee_colorAssigner.run ( ) ;

def darkHorse_groomShaderImporter_caller ( *args ) :
	import project.darkHorse.hair_groomShaderUtil.core as darkHorse_hair_groomShaderUtil ;
	reload ( darkHorse_hair_groomShaderUtil ) ;
	darkHorse_hair_groomShaderUtil.run() ;

# def darkHorse_xGenShaderImporter_cmd ( *args ) :
#     import project.darkHorse.xgenShaderImporter as darkHorse_xgenShaderImporter ;
#     reload ( darkHorse_xgenShaderImporter ) ;
#     darkHorse_xgenShaderImporter.run ( ) ;

def lillyCollider_cmd ( *args ) :
    import project.darkHorse.lillyCollider as lillyCollider ;
    reload ( lillyCollider ) ;
    lillyCollider.run ( )  ;

def godEmperorSlideOnMesh_cmd ( *args ) :
    import project.darkHorse.specific_godEmperorSlideOnMesh.core as specific_godEmperorSlideOnMesh ;
    reload ( specific_godEmperorSlideOnMesh ) ;
    specific_godEmperorSlideOnMesh.run ( )  ;

def darkHorse_detachRig_run ( *args ) :
    import project.darkHorse.rig_detachRig.core as darkHorse_rig_detachRig ;
    reload ( darkHorse_rig_detachRig ) ;
    darkHorse_rig_detachRig.run ( ) ;

def darkHorse_positionPuller_run ( *args ) :
	import project.darkHorse.setdress_positionPuller.core as setdress_positionPuller ;
	reload ( setdress_positionPuller ) ;
	setdress_positionPuller.run() ;

##################
##################
''' [ NAV_IDX ] NAVIGATION SECTION '''
##################
##################

def browseTechAnim_cmd ( *args ) :
    import os ;
    # os.startfile ( r'P:\Temp\Shared\_techAnim' ) ;
    os.startfile ( r'N:\Staff\_techAnim' ) ;

def darkHorse_browseDaily_cmd ( *args ) :
    import os ;
    os.startfile ( r'P:\darkHorse\daily\sim' ) ;



def darkHorse_browseScene_cmd ( *args ) :
    import os ;
    os.startfile ( r'P:\darkHorse\scene\work' ) ;



##############################################################################################################################
##############################################################################################################################
''' [ UIS_IDX ] UI '''
##############################################################################################################################
##############################################################################################################################

def initialized_cmd ( *args ) :
    setDefaultMultiSampling_cmd ( ) ;

##################
''' [ UIF_IDX ] UI FUNCTIONS '''
##################

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

##################
''' [ UIC_IDX ] UI CONTENT '''
##################

def setFrameRate ( *args ) :
    ###June Edit###
    # set fps to 24
    pm.currentUnit ( time = 'ntsc' ) ;
    
def insert ( project='', *args ) :

    ##################
    ##################
    ''' [ ISL_IDX ] INFO SECTION '''
    ##################
    ##################

    with pm.scrollLayout ( w = layoutWidth , h = 500 ) :

        with pm.rowColumnLayout (  nc = 5 , cw = [ ( 1 , width*0.05 ) , ( 2 , width*0.55 ) , ( 3 , width*0.05 ) , ( 4 , width*0.30 ) , ( 5 , width*0.05 ) ] ) :

            filler ( ) ;
            pm.text ( label = 'resolution' ) ;
            filler ( ) ;
            pm.text ( label = 'fps' ) ;
            filler ( ) ;

            filler ( ) ;      
            with pm.rowColumnLayout (  nc = 2 , cw = [ ( 1 , width*0.55/2 ) , ( 2 , width*0.55/2 ) ] ) :       
                pm.intField ( v = 3840 ) ;
                pm.intField ( v = 2160 ) ;
            filler ( ) ;
            pm.intField ( v = 30 ) ;

        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 )  , ( 2 , width/2 ) ] ) :
            pm.button ( label = 'set scene' , c = darkHorse_setResolution_cmd ) ;
            pm.button ( label = 'resolution gate' , c = resolutionGate_cmd  ) ;

     
            with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :
                filler ( ) ;
                pm.button ( label = 'toggle anti-aliasing' , w = width/2 , c = toggleAntiAliasing_cmd ) ;

            with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :
                
                pm.text ( label = 'sample count' , w = width/2 ) ;

                with pm.optionMenu( 'sampleCount_ptm' , w = width/2 , changeCommand = sampleCountChange_cmd ) :
                    pm.menuItem ( label= '16' ) ;
                    pm.menuItem ( label= '8' ) ;
                    pm.menuItem ( label= '4' ) ;

        separator ( ) ;


        
        ##################
        ##################
        ''' [ UTL_IDX ] UTILITIES SECTION '''
        ##################
        ##################

        pm.separator ( vis = True , h = 30 ) ;
        with pm.rowColumnLayout ( nc = 1 , w = width ) :
            pm.button ( label = 'Sim PreRoll' , c = darkHorse_simPreRoll_cmd , bgc = ( 0 , 1 , 1 ) , w = width) ;
            
        pm.separator ( vis = True , h = 10 ) ;

        
