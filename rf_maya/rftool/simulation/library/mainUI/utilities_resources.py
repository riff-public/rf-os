import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

##################
''' PATH '''
##################
# return : C:/Users/Administrator/Desktop/script/birdScript/library/
selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

mp = selfPath.split('/library')[0] ;
# mp = D:/Dropbox/script/birdScript
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
else : pass ;

##################
'''CONFIG'''
##################

def readText ( path , *args ) :
    # return text
    file = open ( path , 'r' ) ;
    text = file.read ( ) ;
    file.close ( ) ;
    return text ;

configText = readText ( path = selfPath + '/config.txt' ) ;
exec ( configText ) ;

globalWidth = config_dict [ 'width' ] ;
layoutWidth = globalWidth/2 ;
width = globalWidth/2*0.935 ;

def completePythonBootcamp_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = r'http://nbviewer.jupyter.org/github/jmportilla/Complete-Python-Bootcamp/tree/master/' ) ;

def mayaGUISnippets_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    wp = os.path.realpath (__file__) ;
    fileName = os.path.basename (__file__) ;
    wp = wp.partition ( 'library' ) [0] ;
    wp = wp + r'general/website/mayaGUISnippets.html' ;
    utl.openLink ( link = wp ) ;

def mayaColorNames_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    wp = os.path.realpath (__file__) ;
    fileName = os.path.basename (__file__) ;
    wp = wp.partition ( 'library' ) [0] ;
    wp = wp + r'general\website\MolScript v2.1_ Colour names.html' ;
    utl.openLink ( link = wp ) ;

def mayaColorNames_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    wp = os.path.realpath (__file__) ;
    fileName = os.path.basename (__file__) ;
    wp = wp.partition ( 'library' ) [0] ;
    wp = wp + r'general\website\MolScript v2.1_ Colour names.html' ;
    utl.openLink ( link = wp ) ;

def googleSpreadsheetsPython_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    wp = os.path.realpath (__file__) ;
    fileName = os.path.basename (__file__) ;
    wp = wp.partition ( 'library' ) [0] ;
    wp = wp + r'general\website\Google Spreadsheets and Python.html' ;
    utl.openLink ( link = wp ) ;

def regularExpression_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    wp = os.path.realpath (__file__) ;
    fileName = os.path.basename (__file__) ;
    wp = wp.partition ( 'library' ) [0] ;
    wp = wp + r'general\website\Regular Expressions in Python.html' ;
    utl.openLink ( link = wp ) ;

def python_writingCleanCode_link ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    wp = os.path.realpath (__file__) ;
    fileName = os.path.basename (__file__) ;
    wp = wp.partition ( 'library' ) [0] ;
    wp = wp + r'general\website\Python_WritingCleanCode\WritingCleanCode_ChadVernon.html' ;
    utl.openLink ( link = wp ) ;

def cgcircuitBrowse_cmd ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = 'cgcircuit.com' ) ;

##################
##################
''' simulation tutorials '''
##################
##################

##################
''' [ SMD_IDX ] SIM DOC '''
##################

def open_RiffInfo ( *args ) :
    utl.openLink ( link = r'https://docs.google.com/document/d/1OmPp07ZRXyoI7Ky0TWwTNY_B9iugj3bzCUfZhN123eg/' ) ;

def open_simulationFAQ ( *args ) :
    utl.openLink ( link = r'https://docs.google.com/document/d/1wBDtlqjuhFR1ygm1Hj-C71ezWId8k2jaAlAz-DVq0MI/' ) ;

def open_RiffSimulationWorkflow ( *args ) :
    utl.openLink ( link = r'https://docs.google.com/document/d/18fHgP6jI7YU7mL0VrnPAXsR9yYLcsD0ITXbMFszr_mY/' ) ;

def open_JuneClothFAQ ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = r'https://drive.google.com/file/d/1yqgrRxoHxE8FM_TOy_h93JLwTsAMcufU/view?usp=sharing' ) ;
 
def open_JuneHairFAQ ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    utl.openLink ( link = r'https://docs.google.com/document/d/1uetQuBYHE7z1UePGfG0EkeGn6Xy0POnqFZ5RPfsdQZk/edit#heading=h.6jynaot9cbnq' ) ;
    
def openFile_cmd ( file = '' , *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    
    wp = os.path.realpath (__file__) ;
    fileName = os.path.basename (__file__) ;
    wp = wp.partition ( 'library' ) [0] ;
    wp = wp + r'resources/sim/%s' % file  ;
    
    utl.openLink ( link = wp ) ;

def open_mayaSettings ( *args ) :
    openFile_cmd ( 'Maya Settings.pdf' ) ;

def open_st1_preparingCollider ( *args ) :
    openFile_cmd ( 'Sim Tutorial 1 - Preparing Collider.pdf' ) ;

def open_st2_preparingCurvesFromXGen ( *args ) :
    openFile_cmd ( 'Sim Tutorial 2-2 - Preparing Curves From XGen.pdf' ) ;

def open_st3_preparingHairDynamicSetup ( *args ) :
    openFile_cmd ( 'Sim Tutorial 3-1 - Preparing Hair Dynamic Setup.pdf' ) ;

def open_st4_preparingHairXGenSetup ( *args ) :
    openFile_cmd ( 'Sim Tutorial 3-2 - Preparing Hair XGen Setup.pdf' ) ;

def open_st5_usingNHairOutputCurveToDriveYeti ( *args ) :
    openFile_cmd ( 'Sim Tutorial 3-2 - Using nHair Output Curve to drive Yeti.pdf' ) ;

def openNClothReference ( *args ) :
    import general.utilities as utl ;
    reload ( utl ) ;
    wp = os.path.realpath (__file__) ;
    fileName = os.path.basename (__file__) ;
    wp = wp.partition ( 'library' ) [0] ;
    wp = wp + r'general\website\nClothReference.html' ;
    utl.openLink ( link = wp ) ;


##################
##################
''' UI '''
##################
##################

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

def insert ( *args ) :


    with pm.rowColumnLayout ( 'resources' ,  nc = 1 , cw = ( 1 , width ) ) :
        with pm.frameLayout ( label = 'Simulation Documents' , collapsable = True , collapse = True , bgc = ( 0 , 0 , 0.5 ) ) :
            with pm.rowColumnLayout ( nc = 1 , w = width ) :

                pm.button ( label = 'Maya Settings'             , width = width , c = open_mayaSettings ) ;

                pm.button ( label = 'Simulation FAQ (WIP)'      , width = width , c = open_simulationFAQ , bgc = ( 1 , 1 , 0 ) ) ;
                pm.button ( label = 'Riff Simulation Workflow'  , width = width , c = open_RiffSimulationWorkflow ) ;

                pm.button ( label = 'nCloth References' , width = width , c = openNClothReference ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/10*1 ) , ( 2 , width/10*9 ) ] ) :

                    pm.text   ( label = '1' ) ;
                    pm.button ( label = 'Preparing Collider'            , w = width/10*9    , c = open_st1_preparingCollider ) ;

                    pm.text   ( label = '2' ) ;
                    pm.button ( label = 'Preparing Curves From XGen'    , w = width/10*9    , c = open_st2_preparingCurvesFromXGen ) ;

                    pm.text   ( label = '3' ) ;
                    pm.button ( label = 'Preparing Hair Dynamic Setup'  , w = width/10*9    , c = open_st3_preparingHairDynamicSetup ) ;

                    pm.text   ( label = '4' ) ;
                    pm.button ( label = 'Preparing Hair XGen Setup'     , w = width/10*9    , c = open_st4_preparingHairXGenSetup ) ;

                    pm.text   ( label = '5' ) ;
                    pm.button ( label = 'Using nHair Output Curve to drive Yeti'            , w = width/10*9    , c = open_st5_usingNHairOutputCurveToDriveYeti ) ;
                
            with pm.frameLayout ( label = "June's Simulation Documents" , collapsable = True , collapse = True , bgc = ( 0 , 0 , 0.5 ) ) :
                with pm.rowColumnLayout ( nc = 1 , w = width ) :

                    pm.button ( label = 'Cloth Simulation FAQ'      , width = width , c = open_JuneClothFAQ ) ;
                    pm.button ( label = 'Hair Simulation FAQ'       , width = width , c = open_JuneHairFAQ ) ;

            separator ( ) ;        
        
        pm.button ( label = 'complete python bootcamp (online)' , w = width , bgc = ( 0.95 , 0.85 , 0.5) , c = completePythonBootcamp_cmd ) ;
        
        separator ( ) ;

        pm.button ( label = 'Mel : maya GUI snippets' , w = width , c = mayaGUISnippets_cmd ) ;

        separator ( ) ;

        pm.button ( label = 'General : Colour Names (RGB 0 - 1)' , w = width , c = mayaColorNames_cmd ) ;

        separator ( ) ;

        pm.button ( label = 'Python : Regular Expression' , w = width , c = regularExpression_cmd ) ;
        pm.button ( label = 'Python : Writing Clean Code' , w = width , c = python_writingCleanCode_link ) ;
        pm.button ( label = 'Python : Google Spreadsheets & Python' , w = width , c = googleSpreadsheetsPython_cmd ) ;


        '''
        if pm.textField ( 'username_textField' , q = True , text = True ) == 'weerapot c. (admin2367)' :

            separator ( ) ;

            pm.button ( label = 'cg circuit' , w = width , c = cgcircuitBrowse_cmd ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/5 ) , ( 2 , width/5*4 ) ] ) :
                pm.text ( label = 'ID' ) ;
                pm.textField ( tx = 'contact@riffcg.com' ) ;
                pm.text ( label = 'PASS' ) ;
                pm.textField ( tx = 'RiffRiff' ) ;
        '''