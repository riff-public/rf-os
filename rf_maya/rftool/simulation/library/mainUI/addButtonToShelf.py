import pymel.core as pm ;
import os ;

self_path = os.path.realpath (__file__) ;
self_name = os.path.basename (__file__) ;
self_path = self_path.replace ( self_name , '' ) ;
self_path = self_path.replace ( '\\' , '/' ) ;

# just in case #
if self_path[-1] != '/' :
	self_path += '/' ;
else : pass ;

def makeCommand_cmd ( *args ) :

	cmd = '''
import maya.cmds as mc ;
import sys ;

mp = "%s" ;
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;

import library.mainUI.UI1 as mui ;
reload ( mui ) ;

mui.run () ;
''' % self_path.split ( '/library/' ) [0] ;

	return cmd ;

#D:/Dropbox/script/birdScript

def run ( *args ) :
	# print self_path ;
	# D:/Dropbox/script/birdScript/library/mainUI/

	image_path = self_path + 'media/icon.png' ;

	cmd = makeCommand_cmd ( ) ;


	mel_cmd = '''
global string $gShelfTopLevel;
string $shelves = `tabLayout -q -selectTab $gShelfTopLevel`;
'''

	currentShelf = pm.mel.eval ( mel_cmd );

	pm.shelfButton ( style = 'iconOnly' , image = image_path , command = cmd , parent = currentShelf ) ;
