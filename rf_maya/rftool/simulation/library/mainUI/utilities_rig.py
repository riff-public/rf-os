##############################################################################################################################
##############################################################################################################################
''' TABLE OF CONTENT '''
##############################################################################################################################
##############################################################################################################################

'''

navigate with "ctrl + f"

[ GNU_IDX ] GENERAL UTILITIES 
[ ATU_IDX ] ATTRIBUTE UTILITIES
[ HRU_IDX ] HAIR RIG RELATED UTILITIES
[ CRU_IDX ] CURVE UTILITIES
[ SCI_IDX ] SOFT CLUSTER INSTALLATION

[ UIS_IDX ] UI
    [ UIF_IDX ] UI FUNCTIONS
    [ UIC_IDX ] UI CONTENT

'''

import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

import general.utilities as utl ;
reload ( utl ) ;

##################
''' PATH '''
##################
# return : C:/Users/Administrator/Desktop/script/birdScript/library/
selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

mp = selfPath.split('/library')[0] ;
# mp = D:/Dropbox/script/birdScript
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
else : pass ;

##################
'''CONFIG'''
##################

def readText ( path , *args ) :
    # return text
    file = open ( path , 'r' ) ;
    text = file.read ( ) ;
    file.close ( ) ;
    return text ;

configText = readText ( path = selfPath + '/config.txt' ) ;
exec ( configText ) ;

globalWidth = config_dict [ 'width' ] ;
layoutWidth = globalWidth/2 ;
width = globalWidth/2*0.935 ;


##################
''' [ GNU_IDX ] GENERAL UTILITIES '''
##################

def jointSizeSlider_cmd ( *args ) :
    value = pm.floatSliderGrp ( 'jointSizeSlider' , q = True , v = True ) ;
    pm.jointDisplayScale ( value ) ;

def printScenePath_cmd ( *args ) :
    print pm.mel.eval ( "file -q -exn" ) ;

def cometRename_cmd ( *args ) :
    import external.cometRename as cometRename ;
    reload ( cometRename ) ;
    cometRename.cometRename ( ) ;

def geo_moveShapeToCopiedTransform_cmd ( *args ) :
    import rig.geo_moveShapeToCopiedTransform as geo_moveShapeToCopiedTransform ;
    reload ( geo_moveShapeToCopiedTransform ) ;
    geo_moveShapeToCopiedTransform.run ( ) ;

##################
''' [ ATU_IDX ] ATTRIBUTE UTILITIES '''
##################

def skAttrShiftUp_cmd ( *args ) :
    import external.sk_attrShift as ats ;
    reload ( ats ) ;
    ats.shiftUp ( ) ;

def skAttrShiftDown_cmd ( *args ) :
    import external.sk_attrShift as ats ;
    reload ( ats ) ;
    ats.shiftDown ( ) ;

def showHiddenAttribute_cmd ( *args ) :
    import general.showHiddenAttribute as sha ;
    reload ( sha ) ;
    sha.showHiddenAttribute ( ) ;

##################
''' [ HRU_IDX ] HAIR RIG RELATED UTILITIES '''
##################

def createHairJoint_cmd ( *args ) :
    import rig.createHairJoint as createHairJoint ;
    reload ( createHairJoint ) ;
    createHairJoint.createHairJointUI ( ) ;

def folliclesUtil_cmd ( *args ) :
    import rig.follicles.prototype.folliclesUI as fol ;
    reload ( fol ) ;
    fol.folUI ( ) ;

def makeCurveFromJointChain_cmd ( *args ) :
    import rig.makeCurveFromJointChain as makeCurveFromJointChain ;
    reload ( makeCurveFromJointChain ) ;
    makeCurveFromJointChain.run ( ) ;

##################
''' [ CRU_IDX ] CURVE UTILITIES '''
##################

def controlMaker_cmd ( *args ) :
    import external.pk.controlMaker as controlMaker ;
    reload ( controlMaker ) ;
    controlMaker.run ( ) ;

def curveEPMelToPython_cmd ( *args ) :
    import general.curveEPMelToPython as curveEPMelToPython ;
    reload ( curveEPMelToPython ) ;
    curveEPMelToPython.run ( ) ;

def curveCreator_cmd ( *args ) :
    import general.curveCreator as curveCreator ;
    reload ( curveCreator ) ;
    curveCreator.run ( ) ;

def copyCrvShape_cmd ( *args ) :
    import external.copyCrvShape as copyCrvShape ;
    reload ( copyCrvShape ) ;
    copyCrvShape.run ( ) ;

def curve_separateCurveShape_cmd ( *args ) :
    import rig.curve_separateCurveShape as curve_separateCurveShape ;
    reload ( curve_separateCurveShape ) ;
    curve_separateCurveShape.run ( ) ;

##############################################################################################################################
##############################################################################################################################
''' [ UIS_IDX ] UI '''
##############################################################################################################################
##############################################################################################################################

##################
''' [ UIF_IDX ] UI FUNCTIONS '''
##################

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

##################
''' [ UIC_IDX ] UI CONTENT '''
##################

def insert ( *args ) :

    with pm.scrollLayout ( w = layoutWidth , h = 450 ) :

        with pm.rowColumnLayout ( nc = 1 , w = width ) :

            ##################
            ''' [ GNU_IDX ] GENERAL UTILITIES '''
            ##################

            pm.floatSliderGrp ( 'jointSizeSlider' , label = 'joint size' , field = True , minValue = 0.01 , maxValue = 10 , precision = 2 ,
                columnWidth3 = [ width/5 , width/5 , width/5*3 ] , columnAlign3 = [ "center" , "both" , "left" ] ,
                value = pm.jointDisplayScale ( q = True ) , dc = jointSizeSlider_cmd , cc = jointSizeSlider_cmd ) ;

            pm.button ( label = 'print self path' , c = printScenePath_cmd ) ;

            pm.button ( label = 'cometRename' , c = cometRename_cmd , bgc = ( 1 , 0.9 , 0.7 ) ) ;

            separator ( ) ;

            pm.button ( label = 'move shape to copied transform' , c = geo_moveShapeToCopiedTransform_cmd ) ;
            pm.text ( label = '#select transform then geometry' , bgc = ( 0.85 , 0.85 , 0.85 ) ) ;
           
            separator ( ) ;

            ##################
            ''' [ ATU_IDX ] ATTRIBUTE UTILITIES '''
            ##################

            with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                pm.button ( label = 'attribute up' , c = skAttrShiftUp_cmd ) ;
                pm.button ( label = 'attribute down' , c = skAttrShiftDown_cmd ) ;

            pm.button ( label = 'show hidden (general) attribute' , c = showHiddenAttribute_cmd ) ;

            separator ( ) ;

            ##################
            ''' [ HRU_IDX ] HAIR RIG RELATED UTILITIES '''
            ##################

            pm.button ( label = 'creat hair joint' , c = createHairJoint_cmd ) ;
            pm.button ( label = 'make curve from joint chain' , c = makeCurveFromJointChain_cmd ) ;

            separator ( ) ;

            ##################
            ''' [ CRU_IDX ] CURVE UTILITIES '''
            ##################

            with pm.frameLayout ( label = 'curve utilities' , collapsable = True , collapse = True , bgc = ( 0 , 0 , 0.2 ) ) :

                with pm.rowColumnLayout ( nc = 1 ) :

                    pm.button ( label = 'pk controller maker' , c = controlMaker_cmd ) ;

                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = 'curveEP mel 2 python' , c = curveEPMelToPython_cmd ) ;
                        pm.button ( label = 'curve creator' , c = curveCreator_cmd ) ;

                    pm.button ( label = 'copy curve shape (monk)' , c = copyCrvShape_cmd ) ;
                    pm.button ( label = 'separate curve shapes' , c = curve_separateCurveShape_cmd ) ;
