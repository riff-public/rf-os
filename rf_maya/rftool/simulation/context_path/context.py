import os
import sys
import time

core_path = '%s/core' % os.environ.get('RFSCRIPT')
if core_path not in sys.path:
    sys.path.append(core_path)

from rf_utils.context import context_info


class SimulationContext:
    def __init__(self, project="projectName"):
        self.project = project
        context = context_info.Context()
        context.update(
            project=project
        )
        self.sim_context = context_info.ContextPathInfo(context=context)
        print "self.sim_context", self.sim_context
        print "xxxxxxxxxxxxxxx"

    def get_context(self):
        if self.project == "Two_Heroes":
            return 'P:/Two_Heroes/scene/'
        elif self.project == "SevenChickMovie":
        return 'P:/SevenChickMovie/scene/work/'

    def update_context(self):
        pass
