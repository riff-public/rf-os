class Test(object):
    def __init__(self, driver, driven):
        self.driver = driver ;
        self.driven = driven ;

    def printSelf ( self ) :
    	print ( 'driver : {a} , driven : {b}'.format ( a = self.driver , b = self.driven ) ) ;

test1 = Test ( driver = 'driver_polygon' , driven = 'driven_polygon' ) ;

test1.printSelf () ;

