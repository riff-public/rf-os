import sys ;
import os ;
import maya.cmds as mc ;

mp = r'C:\Users\AuntieHome\Desktop\script\birdScript20170707_riff\test\uiTest' ;

if not mp in sys.path :
	sys.path.append ( mp ) ;


def ctPrint ( ) :
	import customPrint as cp ;
	reload ( cp ) ;
	cp.customPrint ( test = 'yo' ) ;


def UI2 ( ) :
    
    # check if window exists
    
    if mc.window ( 'UI2' , exists = True ) :
        mc.deleteUI ( 'UI2' ) ;
        
    # create window 
    
    window = mc.window ( 'UI2', title = "UI2 v0.0", width = 200,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    tabs = mc.tabLayout ( innerMarginWidth = 5 , innerMarginHeight = 5 ) ;
    
    mc.button ( 'print yo' , c = 'ctPrint ( )' ) ;

    mc.setParent( '..' ) ;    
   
    # show window 
    
    mc.showWindow ( window ) ;
    
#UI2 () ;
