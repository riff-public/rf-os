import sys ;
import os ;
import maya.cmds as mc ;

	# get path from this script location
mp = r'C:\Users\AuntieHome\Desktop\script\birdScript20170707_riff\test\uiTest'

if not mp in sys.path :
	sys.path.append ( mp ) ;

def UI2 ( ) :
	import UI2 as u2 ;
	reload ( u2 ) ;
	u2.UI2 ( ) ;


def UI1 ( ) :
    
    # check if window exists
    
    if mc.window ( 'UI1' , exists = True ) :
        mc.deleteUI ( 'UI1' ) ;
        
    # create window 
    
    window = mc.window ( 'UI1', title = "UI1 v0.0", width = 200,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    tabs = mc.tabLayout ( innerMarginWidth = 5 , innerMarginHeight = 5 ) ;
    
    mc.button ( 'UI2' , c = 'UI2 ()' ) ;

    mc.setParent( '..' ) ;    
   
    # show window 
    
    mc.showWindow ( window ) ;
    
UI1 ( ) ;