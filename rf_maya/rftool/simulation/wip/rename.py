import os ;
import re ;

class Path ( object ) :

    def __init__ ( self , path ) :
        self.path = path ;

    def __str__ ( self ) :
        return self.path ;

    def __repr__ ( self ) :
        return self.path ;

    def listdir ( self ) :
        itemList = os.listdir ( self.path ) ;
        itemList.sort ( ) ;
        return itemList ;

    def udim_to_u1v1 ( self ) :

        itemList = self.listdir ( ) ;
        
        for item in itemList :
            
            currentNumber = re.findall ( '[0-9]{4}' , item ) [0] ;

            v = currentNumber[1:-1]
            v = int ( v ) + 1 ;
            
            u = currentNumber[-1] ;
            u = int ( u ) ;
            
            if u == 0 :
                u = 10 ;
                v -= 1 ;

            currentUV = '_u' + str(u) + '_v' + str(v) ;

            print currentNumber , currentUV ;

            targetPath = os.path.join ( self.path , item ) ;
            resultPath = targetPath.replace ( currentNumber , currentUV );
    
            os.rename ( targetPath , resultPath ) ;

    def u1v1_to_udim ( self ) :

        itemList = self.listdir ( ) ;
        
        for item in itemList :

            currentUV = re.findall ( 'u' + '[0-9]{1,2}' + '_' + 'v' + '[0-9]{1,2}' , item ) [0] ;

            u = currentUV.split('_') [0] ;
            u = re.findall ( '[0-9]{1,2}' , u ) [0] ;
            
            v = currentUV.split('_') [1] ;
            v = re.findall ( '[0-9]{1,2}' , v ) [0] ;

            print currentUV , u , v ;

path = r'C:\Users\weerapot.c\Desktop\rename\AO' ;

path_path = Path (path) ;
path_path.u1v1_to_udim ();

# path = r'C:\Users\weerapot.c\Desktop\rename\AO' ;

# files = os.listdir ( path ) ;
# files.sort ( ) ;

# for each in files :
    
#     currentNumber = re.findall ( '[0-9]{4}' , each ) [0] ;
    
#     print currentNumber ;
    
#     v = currentNumber[1:-1]
#     v = int ( v ) + 1 ;
    
#     u = currentNumber[-1] ;
#     u = int ( u ) ;
    
#     if u == 0 :
#         u = 10 ;
#         v -= 1 ;
    
#     currentUV = '_u' + str(u) + '_v' + str(v) 
    
#     targetPath = os.path.join ( path , each ) ;
#     resultPath = targetPath.replace ( currentNumber , currentUV );
    
#     os.rename ( targetPath , resultPath ) ;