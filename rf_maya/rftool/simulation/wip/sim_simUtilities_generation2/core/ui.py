import pymel.core as pm ;

class Ui ( object ) :

    def __init__ ( self ) :
        self.title  = 'simulation utitlities' ;
        self.ui     = 'simUtilities_UI' ;
        self.width  = 300.00 ;

    def deleteUi ( self ) :
        if pm.window ( self.ui , ex = True ) :
            pm.deleteUI ( self.ui ) ;
            # requestioning
            deleteUi ( ) ;

    def run ( self ) :
        self.deleteUi() ;

        window = pm.window ( self.ui ,
            title   = self.title ,
            width   = self.width ,
            mnb     = True ,
            mxb     = True ,
            sizable = True ,
            rtf     = True ,
            ) ;

# def run ( *args ) :


#     window = pm.window ( 'simUtilities', title = "Simulation Utilities" , w = width , 
#         mnb = True , mxb = False , sizeable = True , rtf = True ) ;

#     pm.window ( 'simUtilities', e = True , w = width , h = 10 ) ;
    
#     with window :
    
#         mainLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] ) ;
#         with mainLayout :

#             ######

#             nucleusTxtScrll = pm.textScrollList ( 'nucleusTxtScrll' , w = width , h = 60 , ams = True , sc = updateNucleusUI ) ;
            
#             nucleusLayout = pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width / 2.0 ) , ( 2 , width / 2.0 ) ] ) ;
#             with nucleusLayout :
#                 pm.button ( 'neBtn' , label = 'enable nucleus' ,  c  = enableNucleus , w = width / 2.0 ) ;
#                 pm.button ( 'ndBtn' , label = 'disable nucleus' , c  = disableNucleus , w = width / 2.0 ) ;
#                 updateNucleusUI ( ) ;
            
#             pm.separator ( vis = 0 ) ;

#             cycleCheckLayout = pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width / 2.0 ) , ( 2 , width / 2.0 ) ] ) ;
#             with cycleCheckLayout :
#                 pm.button ( 'eccBtn' , label = 'enable cycle check' ,  c  = enableCycle , w = width / 2.0 ) ;
#                 pm.button ( 'dccBtn' , label = 'disable cycle check' , c  = disableCycle , w = width / 2.0 ) ;
#                 updateCycleButton ( ) ;

#             pm.separator ( vis = 0 ) ;

#             enableLayout = pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width / 2.0 ) , ( 2 , width / 2.0 ) ] ) ;
#             with enableLayout :
#                 pm.button ( 'nceBtn' , label = 'enable nCloth' ,  c  = enableNCloth , w = width / 2.0 , bgc = ( 0 , 1 , 1 ) ) ;
#                 pm.button ( 'ncdBtn' , label = 'disable nCloth' , c  = disableNCloth , w = width / 2.0 , bgc = ( 0 , 0.74902 , 1 ) ) ;

#                 pm.button ( 'nheBtn' , label = 'enable nHair' ,  c  = enableNHair , w = width / 2.0 , bgc = ( 0.498039 , 1 , 0 ) ) ;
#                 pm.button ( 'nhdBtn' , label = 'disable nHair' , c  = disableNHair , w = width / 2.0 , bgc = ( 0.196078 , 0.803922 , 0.196078 ) ) ;

#             ######

#             pm.separator ( vis = True , h = 10 ) ;

#             timeRangeLayout = pm.rowColumnLayout ( w = width , nc = 5 , columnWidth = [
#                 ( 1 , width / 5.0 ), ( 2 , width / 5.0 ) , ( 3 , width / 5.0 ) , ( 4 , width / 5.0 ) , ( 5 , width / 5.0 ) ] ) ;
#             with timeRangeLayout :    

#                 pm.intField ( 'preRollIF' ) ;
#                 pm.intField ( 'startIF' ) ;
#                 pm.button ( label = 'save' , c = save , bgc = ( 0.564706 , 0.933333 , 0.564706 ) ) ;
#                 pm.intField ( 'endIF' ) ;
#                 pm.intField ( 'postRollIF' ) ;
#                 pm.text ( label = 'pre roll' ) ;
#                 pm.text ( label = 'start' ) ;
#                 pm.text ( label = '' ) ;
#                 pm.text ( label = 'end' ) ;
#                 pm.text ( label = 'post roll' ) ;
    
#             setTimeLayout = pm.rowColumnLayout ( w = width , nc = 3 , columnWidth = [ ( 1 , width * 0.05 ) , ( 2 , width * 0.9 ) , ( 3 , width * 0.05 ) ] ) ;
#             with setTimeLayout :

#                 pm.text ( label = '' ) ;

#                 setTimeBtnLayout = pm.rowColumnLayout ( w = width * 0.9 , nc = 1 , columnWidth = [ ( 1 , width * 0.9 ) ] ) ;
#                 with setTimeBtnLayout :
#                     pm.button ( label = 'get time range' , c = getTimeRange , bgc = ( 1 , 0.843137 , 0 ) ) ;
#                     pm.button ( 'setTimeRangeBtn' , label = 'set time range' , c = setTimeRange ) ;
#                     # set time range to sim --> color change 

#                 pm.text ( label = '' ) ;

#             pm.separator ( h = 5 ) ;
            

#             tab = pm.tabLayout ( 'tabLayout' ) ;
#             with tab :

#                 simLayout = pm.rowColumnLayout ( 'simulation' , w = width  , nc = 1 , cw = [ ( 1 , width ) ] ) ;
#                 with simLayout :

#                     simMainLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] ) ;
#                     with simMainLayout :

#                         pm.separator ( h = 5 , vis = False ) ;

#                         with pm.rowColumnLayout ( w = width , nc = 5 , columnWidth = [ ( 1 , width*0.05 ), ( 2 , width*0.5 ) , ( 3 , width*0.05 ) , ( 4 , width*0.35 ) , ( 5 , width*0.05 ) ] ) :
#                         # simulate button layout
#                             filler ( ) ;
#                             with pm.optionMenu( 'simTypeOpt' , label='sim type' ) :
#                                 pm.menuItem ( label='replace' ) ;
#                                 pm.menuItem ( label='version' ) ;
#                                 pm.menuItem ( label='subversion' ) ;
#                             filler ( ) ;
#                             pm.button ( label = 'simulate' , c = simulateBtn , bgc = ( 0 , 1 , 1 ) ) ;
#                             filler ( ) ;

#                         with pm.rowColumnLayout ( w = width , nc = 5 , cw = [ ( 1 , width*0.35 ) , ( 2 , width*0.25 ) , ( 3 , width*0.25 ) , ( 4 , width*0.1 ) , ( 5 , width*0.05 ) ] ) :

#                             filler ( ) ;
#                             pm.text ( label = 'evaluate every' ) ;
#                             pm.floatField ( 'evaluateEvery_input' , pre = 2 , v = 1 ) ;
#                             pm.text ( label = 'frame' ) ;
#                             filler ( ) ;

#                         with pm.rowColumnLayout ( w = width , nc = 5 , columnWidth = [ ( 1 , width*0.05 ), ( 2 , width*0.5 ) , ( 3 , width*0.05 ) , ( 4 , width*0.35 ) , ( 5 , width*0.05 ) ] ) :
#                         # playblast check box
#                             filler ( ) ;
#                             filler ( ) ;
#                             filler ( ) ;
#                             with pm.rowColumnLayout ( nc = 4 , columnWidth = [ ( 1 , width*0.075 ) , ( 2 , width*0.05 ) , ( 3 , width*0.2 ) , ( 4 , width*0.075 ) ] ) :        
#                                 filler ( ) ;
#                                 pm.checkBox ( 'playBlastCB' , label = ' ' , value = True ) ;
#                                 pm.text ( label = 'playblast' , align = 'left' ) ;
#                                 filler ( ) ;
#                             filler ( ) ;

#                     pm.separator ( h = 10 ) ;

#                     playblastLayout = pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) ;
#                     with playblastLayout :
                        
#                         playblastTimeRangeLayout = pm.rowColumnLayout ( nc = 5 , columnWidth = [
#                             ( 1 , width / 5.0 ), ( 2 , width / 5.0  ) , ( 3 , width / 5.0  ) , ( 4 , width / 5.0  ) , ( 5 , width / 5.0  ) ] ) ;
#                         with playblastTimeRangeLayout :   
                        
#                             pm.text ( label = '' ) ;
#                             pm.intField ( 'playblastStart' ) ;
#                             pm.text ( label = '' ) ;
#                             pm.intField ( 'playblastEnd' ) ;
#                             pm.text ( label = '' ) ;
                        
#                             pm.text ( label = '' ) ;
#                             pm.text ( label = 'start' ) ;
#                             pm.text ( label = '' ) ;
#                             pm.text ( label = 'end' ) ;
#                             pm.text ( label = '' ) ;

#                         playblastBtnLayout = pm.rowColumnLayout ( nc = 3 , columnWidth = [ ( 1 , width * 0.05 ) , ( 2 , width * 0.9 ) , ( 3 , width * 0.05 ) ] ) ;
#                         with playblastBtnLayout :

#                             pm.text ( label = '' ) ;

#                             with pm.rowColumnLayout ( nc = 1 , w = width*0.9 ) :
                            
#                                 pm.button ( label = 'playblast (for view)' , c = viewPlayblast , w = width*0.9 , bgc = ( 1 , 0.388235 , 0.278431 ) ) ;
                                
#                                 filler ( ) ;

#                                 #with pm.frameLayout ( label = 'PLAYBLAST (VIDEO)' , collapsable = True , collapse = False , cc = frameLayout_cc , bgc = ( 0.1 , 0.1 , 0.5 )  ) :
#                                 #    with pm.rowColumnLayout ( nc = 1 ) :

#                                 pm.text ( label = 'playblast directory :' ) ;
#                                 pm.textField ( 'playblastPath_textField' , w = width*0.5 ) ;
#                                 with pm.rowColumnLayout ( nc = 3 , w = width*0.9 , cw = [ ( 1 , width*0.3 ) , ( 2 , width*0.3 ) , ( 3 , width*0.3 ) ] ) :
#                                     filler ( ) ;
#                                     pm.button ( label = 'open' , c = openPlayblastPath_cmd ) ;
#                                     pm.button ( label = 'browse' , c = playblastPathBrowse_cmd ) ;

#                                 pm.text ( label = 'playblast name :' ) ;
#                                 pm.textField ( 'playblastName_textField' ) ;

#                                 with pm.rowColumnLayout ( nc = 2 , w = width*0.9 , cw = [ ( 1 , width*0.6 ) , ( 2 , width*0.3 ) ] ) :
#                                     filler ( ) ;
#                                     pm.button ( label = 'refresh' , c = updatePlayblastName_cmd ) ;

#                                 filler ( ) ;

#                                 pm.text ( label = 'playblast ( video )' )

#                                 with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width*0.45 ) , ( 2 , width*0.45 ) ] ) :

#                                     pm.text ( 'playblast_version_txt' , label = '' ) ;
#                                     pm.text ( 'playblast_subVersion_text' , label = '' ) ;

#                                     pm.button ( label = 'increment version' , c = playblastVideo_version_cmd ) ;
#                                     pm.button ( label = 'increment sub version' , c = playblastVideo_subVersion_cmd ) ;

#                                     pm.text ( label = 'playblast scale (%)' )
#                                     pm.intField ( 'playblastScale_intField' , value = 50 ) ;

#                             filler ( ) ;

#                     pm.separator ( h = 5 , vis = False ) ;

#                 with pm.rowColumnLayout ( 'cache in' , w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] ) :
                    
#                     filler ( ) ;

#                     with pm.rowColumnLayout ( nc = 3 , columnWidth = [ ( 1 , width * 0.05 ) , ( 2 , width * 0.9 ) , ( 3 , width * 0.05 ) ] ) :
                        
#                         filler ( ) ;
                        
#                         with pm.rowColumnLayout ( nc = 1 , w = width*0.9 ) :

#                             shotCache_path = currentProj + 'data/shotCache/' ;

#                             try :
#                                 shotCache_list = os.listdir ( shotCache_path ) ;
#                             except :
#                                 shotCache_list = [] ;
#                             ######################################
#                             '''camera'''
#                             ######################################
#                             pm.text ( label = 'camera :' ) ;
                            
#                             ### get camera default path ###
#                             defaultCameraPath = shotCache_path ;

#                             for shotCache in shotCache_list :
#                                 if '.cam.' in shotCache :
#                                     defaultCameraPath = shotCache_path + shotCache ;
#                                 else : pass ;

#                             with pm.rowColumnLayout ( nc = 2 , w = width*0.9 , cw = [ ( 1 , width*0.7 ) , ( 2 , width*0.2 ) ] ) :
#                                 pm.textField ( 'cameraPath_textField' , w = width*0.5 , tx = defaultCameraPath ) ;
#                                 pm.button ( label = 'browse' , c = cameraPathBrowse_cmd ) ;                            

#                             with pm.rowColumnLayout ( nc = 2 , w = width*0.9 , cw = [ ( 1 , width*0.45 ) , ( 2 , width*0.45 ) ] ) :
#                                 filler ( ) ;
#                                 pm.button ( label = 'import camera' , c = importCam ) ;

#                             ######################################
#                             '''character alembic'''
#                             ######################################
                            
#                             filler ( ) ;

#                             pm.text ( label = 'character cache :' ) ;
                            
#                             ### get camera default path ###

#                             currentChar = currentProj.split ( '/' ) [-2] ;
#                             print currentChar;
#                             defaultAlembicPath = currentProj + 'data/shotCache/' ;

#                             for shotCache in shotCache_list :
#                                 if '.'+currentChar+'.' in shotCache :
#                                     defaultAlembicPath += shotCache ;
#                                 else : pass ;
                            
#                             with pm.rowColumnLayout ( nc = 2 , w = width*0.9 , cw = [ ( 1 , width*0.7 ) , ( 2 , width*0.2 ) ] ) :
#                                 pm.textField ( 'alembicPath_textField' , w = width*0.5 , tx = defaultAlembicPath ) ;
#                                 pm.button ( label = 'browse' , c = alembicPathBrowse_cmd ) ;

#                             with pm.rowColumnLayout ( nc = 2 , w = width*0.9 , cw = [ ( 1 , width*0.45 ) , ( 2 , width*0.45 ) ] ) :
#                                 pm.checkBox ( 'alembicImportType_cbx' , label = 'blendShape' , v = True , enable = False  ) ;
#                                 pm.button ( label = 'import character cache' , c = importAlembic ) ;

#                             ######################################
#                             '''hair dyn'''
#                             ######################################
#                             filler ( ) ;

#                             pm.text ( label = 'hair dyn cache (for xgen) :' ) ;
                            
#                             defaultHairDynPath = currentProj + 'cache/alembic' ;

#                             if os.path.exists ( defaultHairDynPath + '/DYN_ABC' ) == True :
#                                 defaultHairDynPath += '/DYN_ABC' ;
#                             else : pass ;

#                             with pm.rowColumnLayout ( nc = 2 , w = width*0.9 , cw = [ ( 1 , width*0.7 ) , ( 2 , width*0.2 ) ] ) :
#                                 pm.textField ( 'hairDynPath_textField' , w = width*0.5 , tx = defaultHairDynPath ) ;
#                                 pm.button ( label = 'browse' , c = hairDynPathBrowse_cmd ) ;

#                             with pm.rowColumnLayout ( nc = 2 , w = width*0.9 , cw = [ ( 1 , width*0.45 ) , ( 2 , width*0.45 ) ] ) :
#                                 filler ( ) ;
#                                 pm.button ( label = 'import hair dyn' , c = importHairDyn ) ;

#                             filler ( ) ;

#                             pm.button ( label = 'import' , c = importCache_cmd ) ;

#                         filler ( ) ;

#                 with pm.rowColumnLayout ( 'cache out' , w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] ) :
#                     pass ;

#     initializeTimeRange ( ) ;
#     updateSetTimeRangeBtnClr ( ) ;
#     updatePlayblastDefaultPath_cmd ( ) ;
#     updatePlayblastName_cmd ( ) ;
#     updatePlayblastVersionDisplay ( ) ;
#     window.show ( ) ;