import pymel.core as pm ;

class NClothNode ( object ) :

    def __init__ ( self , node ) :
        self.node = node ;

    def __str__( self) : 
        return str ( self.node ) ;
    
    def __repr__( self) : 
        return str ( self.node ) ;

    def getNCloth ( self ) :
        # [ GNCIDX ]
        # return nCloth, nClothShape , nucleus
        
        #########
        ''' get shapes '''
        #########
        shape_list = pm.listRelatives ( self.node , shapes = True ) ;
        
        #########
        ''' find nCloth '''
        #########        
        for shape in shape_list :
            connection = pm.listConnections ( shape , type = 'nCloth' ) ;
            
            if connection == [] :
                pass ;
            else : 
                nCloth = connection [0] ;
        
            #########
            ''' find nCloth shape '''
            #########
            shape = pm.listRelatives ( nCloth , shapes = True ) [0] ;
            
            if str ( pm.nodeType ( shape ) ) == 'nCloth' :
                nClothShape = shape ;
                nucleus = pm.listConnections ( nClothShape , type = 'nucleus' ) [0] ;
            else : pass ;
            
            #########
            ''' return '''
            #########  

            return nCloth , nClothShape , nucleus ; 

