import pymel.core as pm ;
import maya.cmds as mc ;

###########################
###########################
###########################
''' layout organizer '''
###########################
###########################
###########################

def createGrp_cmd ( name = '' , *args ) :
    if pm.objExists ( name ) == True :
        grp = pm.general.PyNode ( name ) ;
    else :
        grp = pm.group ( em = True , w = True , n = name ) ;
    return grp ;

def findRefTopNode_cmd ( node = '' , *args ) : 
    parent = pm.listRelatives ( node , p = True ) ;
    if parent != [] :
        if pm.referenceQuery ( parent , isNodeReferenced = True ) == True :  
            return ( findRefTopNode_cmd ( node = parent ) ) ;      
        else: return node ;
    else: return node ;

def organizeReferences_cmd ( *args ) :

    ### get reference path ###
    referencePath_list = mc.file ( q = True , r = True ) ;

    ### organize reference path ###
    camRef_list     = [] ;
    charRef_list    = [] ;
    setRef_list     = [] ;
    miscRef_list    = [] ;

    for path in referencePath_list :
        
        if '/camera/' in path :
            camRef_list.append ( path ) ;
        
        elif ( '/char/' in path ) or ( '/character/' in path ) :
            charRef_list.append ( path ) ;
            
        elif '/set/' in path :
            setRef_list.append ( path ) ;

        else :
            miscRef_list.append ( path ) ;

    def groupReference_cmd ( group = '' , *args ) :

    ### parent all camera to cam_grp  ###

    cam_grp = createGrp_cmd ( name = 'cam_grp' ) ;

    cam_list = [] ;

    for each in camRef_list :
        ns = mc.referenceQuery ( each , ns = True ) ;
        childs = pm.ls ( '%s:*' % ns , type = 'transform' , l = True ) ;
        cam_list.append ( findRefTopNode_cmd ( node = childs[0] ) ) ;

    cam_list.sort ( ) ;
      
    for each in cam_list :

        parent = pm.listRelatives ( each , p = True ) ;

        if parent : parent = parent [0] ;

        if parent == cam_grp : pass ;
        else : pm.parent ( each , cam_grp ) ;

    ### parent all character to char_grp  ###

    char_grp = createGrp_cmd ( name = 'char_grp' ) ;

    character_list = [] ;

    for each in charRef_list :
        ns = mc.referenceQuery ( each , ns = True ) ;
        childs = pm.ls ( '%s:*' % ns , type = 'transform' , l = True ) ;
        character_list.append ( findRefTopNode_cmd ( node = childs[0] ) ) ;

    character_list.sort ( ) ;
      
    for each in character_list :

        parent = pm.listRelatives ( each , p = True ) ;

        if parent : parent = parent [0] ;

        if parent == char_grp : pass ;
        else : pm.parent ( each , char_grp ) ;

    ### parent all set to set_grp  ###

    set_grp = createGrp_cmd ( name = 'set_grp' ) ;

    set_list = [] ;

    for each in setRef_list :
        ns = mc.referenceQuery ( each , ns = True ) ;
        childs = pm.ls ( '%s:*' % ns , type = 'transform' , l = True ) ;
        set_list.append ( findRefTopNode_cmd ( node = childs[0] ) ) ;

    set_list.sort ( ) ;
      
    for each in set_list :

        parent = pm.listRelatives ( each , p = True ) ;

        if parent : parent = parent [0] ;

        if parent == set_grp : pass ;
        else : pm.parent ( each , set_grp ) ;

##################
##################
''' camera '''
##################
##################

def getCamList_cmd ( *args ) :

    allCam_list = pm.ls ( cameras = True ) ;
    cam_list = [] ;
    
    for cam in allCam_list :
        if cam == 'frontShape' or cam == 'perspShape' or cam == 'sideShape' or cam == 'topShape' :
            pass ;
        else :
            cam_list.append ( cam ) ;

    return cam_list ;

def cameraCheck_cmd ( *args ) :
    
    cam_list = getCamList_cmd ( ) ;
   
    # cameraCheck1_log

    if len ( cam_list ) == 0 :
        pm.text ( 'cameraCheck1_log' , e = True , label = 'no camera detected' , bgc = ( 1 , 0 , 0 ) ) ;
        pm.text ( 'cameraCheck2_log' , e = True , label = 'please check above condition first' , bgc = ( 1 , 0 , 0 ) ) ;
    elif len ( cam_list ) == 1 :
        pm.text ( 'cameraCheck1_log' , e = True , label = 'OK!' , bgc = ( 0 , 1 , 0 ) ) ;
    else :
        pm.text ( 'cameraCheck1_log' , e = True , label = 'multiple cameras detected' , bgc = ( 1 , 0 , 0 ) ) ;
        pm.text ( 'cameraCheck2_log' , e = True , label = 'please check above condition first' , bgc = ( 1 , 0 , 0 ) ) ;

    # cameraCheck2_log

    if len ( cam_list ) == 1 :

        cam_tfm = pm.listRelatives ( cam_list[0] , parent = True ) [0] ;
        
        topNode = mc.ls ( str ( cam_tfm ) , long = True ) [0] ;
        topNode = topNode.split ( '|' ) [1] ;

        if topNode != 'cam_grp' : 
            pm.text ( 'cameraCheck2_log' , e = True , label = 'camera is not in cam_grp' , bgc = ( 1 , 0 , 0 ) ) ;
            pm.button ( 'cameraCheck2Fix_btn' , e = True , label = 'FIX' , enable = True ) ;

        else :
            pm.text ( 'cameraCheck2_log' , e = True , label = 'OK!' , bgc = ( 0 , 1 , 0 ) ) ;

def cameraCheck2Fix_cmd ( *args ) :

    if pm.objExists ( 'cam_grp' ) == True :
        cam_grp = pm.general.PyNode ( 'cam_grp' ) ;
    else :
        cam_grp = pm.group ( em = True , w = True , n = 'cam_grp' ) ;

    cam_list = getCamList_cmd ( ) ;
    cam_tfm = pm.listRelatives ( cam_list[0] , parent = True ) [0] ;
    
    topNode = mc.ls ( str ( cam_tfm ) , long = True ) [0] ;
    topNode = topNode.split ( '|' ) [1] ;

    pm.parent ( topNode , cam_grp ) ;

    pm.button ( 'cameraCheck2Fix_btn' , e = True , label = 'FIX' , enable = False ) ;

    cameraCheck_cmd ( ) ;

##################
##################
''' namespace '''
##################
##################

def namespaceCheck_cmd ( *args ) :

    references_list = [] ;

    char_list = pm.listRelatives ( 'char_grp' , children = True ) ;
    set_list = pm.listRelatives ( 'set_grp' , children = True ) ;

    references_list.extend ( char_list ) ;
    references_list.extend ( set_list ) ;

    count = 0 ;

    for each in references_list :
        if each.count(':') > 1 :
            count += 1 ;

    if count > 0 :
        pm.text ( 'namespaceCheck1_log' , e = True , label = 'double namespace detected' , bgc = ( 1 , 0 , 0 ) ) ;
        pm.button ( 'namespaceCheck1Fix_btn' , e = True , label = 'FIX' , enable = True ) ; 
    else :
        pm.text ( 'namespaceCheck1_log' , e = True , label = 'OK!' , bgc = ( 0 , 1 , 0 ) ) ;
             
def namespaceCheck1Fix_cmd ( *args ) :

    references_list = [] ;

    char_list = pm.listRelatives ( 'char_grp' , children = True ) ;
    set_list = pm.listRelatives ( 'set_grp' , children = True ) ;

    references_list.extend ( char_list ) ;
    references_list.extend ( set_list ) ;

    print references_list ;

    doubleNamespace_list = [] ;

    for each in references_list :
        if each.count(':') > 1 :
            namespace = each.split(':')[0] ;
            if namespace[0] == '|' :
                namespace = namespace[1:] ;
            doubleNamespace_list.append ( namespace ) ;
        else :
            pass ;

    doubleNamespace_list = set ( doubleNamespace_list ) ;
    doubleNamespace_list = list ( doubleNamespace_list ) ;

    print doubleNamespace_list ;

    
    for each in doubleNamespace_list :
        pm.namespace( mv = [ each , ":" ] , force = True ) ;

    pm.button ( 'namespaceCheck1Fix_btn' , e = True , label = 'FIX' , enable = False ) ;

    namespaceCheck_cmd ( ) ;


def globalCheck_cmd ( *args ) :

    cameraCheck_cmd ( ) ;
    namespaceCheck_cmd ( ) ;

##################
##################
##################
''' UI '''
##################
##################
##################

width = 500.00 ;

def run ( ) :
    # check if window exists
    if pm.window ( 'layoutOrganizer_UI' , exists = True ) :
        pm.deleteUI ( 'layoutOrganizer_UI' ) ;
    else : pass ;

    window = pm.window ( 'layoutOrganizer_UI', title = "layout organizer v0.0" ,
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    pm.window ( 'layoutOrganizer_UI' , e = True , w = width , h = 10 ) ;
    
    with window :

        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            pm.button ( label = 'organize references' , c = organizeReferences_cmd ) ;
 
            ### camera ###
            with pm.frameLayout ( label = 'camera' , collapsable = False , collapse = False , width = width , bgc = ( 0 , 0 , 0.5 ) ) :              
                with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/4 ) , ( 2 , width/2 ) , ( 3 , width/4 ) ] ) :
                    
                    pm.text ( label = 'single camera' ) ;
                    pm.text ( 'cameraCheck1_log' , label = '...' ) ;
                    pm.button ( 'cameraCheck1Fix_btn' , label = 'FIX' , enable = False ) ;

                    pm.text ( label = 'camera in "cam_grp"' ) ;
                    pm.text ( 'cameraCheck2_log' , label = '...' ) ;
                    pm.button ( 'cameraCheck2Fix_btn' , label = 'FIX' , enable = False , c = cameraCheck2Fix_cmd ) ;

            ### namespace ###
            with pm.frameLayout ( label = 'namespace' , collapsable = False , collapse = False , width = width , bgc = ( 0 , 0 , 0.5 ) ) :              
                with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/4 ) , ( 2 , width/2 ) , ( 3 , width/4 ) ] ) :

                    pm.text ( label = 'double namespace' ) ;
                    pm.text ( 'namespaceCheck1_log' , label = '...' ) ;
                    pm.button ( 'namespaceCheck1Fix_btn' , label = 'FIX' , enable = False , c = namespaceCheck1Fix_cmd ) ;

            pm.button ( label = 'check' , width = width , c = globalCheck_cmd ) ;

    window.show () ;