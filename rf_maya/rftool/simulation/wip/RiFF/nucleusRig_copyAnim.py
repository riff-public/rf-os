import pymel.core as pm ;

class NucleusRigNode ( object ) :
    
    def __init__ ( self , node ) :
        self.node = pm.general.PyNode ( node ) ;
        self.gimbal = self.getGimbal () ;
        
    def __str__ ( self ) :
        return str( self.node ) ;
    
    def __repr__ ( self ) :
        return str( self.node ) ;
    
    def dir ( self ) :
        for attr in dir( self ) :
            print attr ;
            
    def pmDir ( self ) :
        for attr in dir( self.node ) :
            print attr ;
            
    def getGimbal ( self ) :
        gimbal = self.node.nodeName() ;
        gimbal = gimbal.split('_')[0] ;
        gimbal += 'Gmbl_Ctrl' ;
        try :
            gimbal = pm.general.PyNode( gimbal ) ;
            return gimbal ;
        except :
            return None ;
            
    def test ( self ) :
        for each in self.node.listAttr( keyable  = True ) :
            print each ;
            
nucleusRig = NucleusRigNode( 'nucleus1_Ctrl' ) ;
print nucleusRig.pmDir() ;
nucleusRig.test() ;