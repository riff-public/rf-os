import pymel.core as pm ;

pm.createNode ( 'reverse' , n = 'headFix_rev' ) ;

bsh = pm.general.PyNode ( 'CIN_COL_BSH' ) ;
rev = pm.general.PyNode ( 'headFix_rev' ) ;
cluster = pm.general.PyNode ( 'headFix_clusterCluster' ) ;

bsh.LilySRU_Grp_CIN >> rev.ix ;
rev.ox >> cluster.envelope ;