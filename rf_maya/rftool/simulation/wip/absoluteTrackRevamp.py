import pymel.core as pm ;
import os ;

####################
####################
''' self path '''
####################
####################

selfPath = os.path.realpath (__file__) ;
fileName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( fileName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

####################
####################
''' load necessary plugin '''
####################
####################

if pm.pluginInfo ( 'matrixNodes.mll' , q = True , loaded = True ) == True :
    pass ;
else :
    pm.loadPlugin ( 'matrixNodes.mll' ) ;

####################
####################
''' general utilities '''
####################
####################

def makeName_cmd ( target = '' , *args ) :
    targetSplit_list = target.split ( '_' ) ;
    name = targetSplit_list[0] ;
    for each in targetSplit_list[1:] :
        name += each.capitalize ( ) ;
    return name ;

####################
####################
''' general maya utilities '''
####################
####################

def getMayaVersion_cmd ( *args ) :
    mayaVersion = pm.mel.eval ( 'getApplicationVersionAsFloat' ) ;    
    return mayaVersion ;

def timeRangeQuery_cmd ( *args ) :
    startFrame = pm.playbackOptions ( q = True , minTime = True ) ; 
    endFrame = pm.playbackOptions ( q = True , maxTime = True ) ;
    return ( startFrame , endFrame ) ;

####################
####################
''' panel utilities '''
####################
####################

def getCurrentModelPanel_cmd ( *args ) :
    modelPanel =  pm.getPanel ( type = 'modelPanel' ) ;
    activePanel = pm.getPanel ( vis = True ) ;
    currentModelPanel = [] ; 
    for each in activePanel :
        if each in modelPanel :
            currentModelPanel.append ( each ) ;
        else :
            pass ;
    return currentModelPanel ;

def getShowList_cmd ( *args ) :

    mayaVersion = getMayaVersion_cmd ( ) ;
    mayaShow_list = None ;

    if mayaVersion == 2017.0 :
        mayaShow_list = [
            'nurbsCurves' ,
            'cv' ,
            'hulls' ,
            'polymeshes' ,
            'hos' , 
            'subdivSurfaces' ,
            'planes' , 
            'lights' ,
            'cameras' ,
            'imagePlane' ,
            'joints' ,
            'ikHandles' ,
            'deformers' ,
            'dynamics' ,
            'particleInstancers' ,
            'fluids' ,
            'hairSystems' ,
            'follicles' ,
            'nCloths' ,
            'nParticles' ,
            'nRigids' ,
            'dynamicConstraints' ,
            'locators' ,
            'dimensions' ,
            'pivots' ,
            'handles' ,
            'textures' ,
            'strokes' ,
            'motionTrails' ,
            'pluginShapes' ,
            'clipGhosts' ,
            'greasePencils' ,
            #'pluginObjects'
            ] ;

    else :
        print ( "This script doesn't fully support this maya version" ) ;    

    return mayaShow_list ;

def getShowPanelStatus_cmd ( panel = '' , *args ) :

    mayaShow_list = getShowList_cmd ( ) ;

    if mayaShow_list == None :
        pass ;

    else :
        show_dict = {} ;
        for atrribute in mayaShow_list :
            command = '''
{attribute}Stat = pm.modelEditor ( '{panel}' , q = True , {attribute} = True ) ;
show_dict['{attribute}'] =  {attribute}Stat
                    '''.format ( attribute = atrribute , panel = panel ) ;  
            #print command ;
            exec ( command ) ;

        return show_dict ;

def setPanelShow_cmd ( setDict = '' , panel = '' , *args ) :

    mayaShow_list = getShowList_cmd ( ) ;

    if mayaShow_list == None :
        pass ;

    else :
        for attribute in mayaShow_list :
            command = '''
pm.modelEditor ( '{panel}' , e = True , {attribute} = {attributeStatus} )
                '''.format ( panel = panel , attribute = attribute , attributeStatus = setDict [ attribute ] ) ;

            exec ( command ) ;


####################
####################
''' curve utilities '''
####################
####################

textDirectory = selfPath.split ( 'sim' ) [0] + 'rig/curveData/' ;

fourDirectionArrow_txt = open ( textDirectory + 'absTrack_fourDirectionArrow.txt' , 'a+' ) ;
fourDirectionArrow_data = fourDirectionArrow_txt.read ( ) ;
fourDirectionArrow_txt.close ( ) ;

star_text = open ( textDirectory + 'absTrack_star.txt' , 'a+' ) ;
star_data = star_text.read ( ) ;
star_text.close ( ) ;

def createCurve_cmd ( curve = '' , name = '' , *args ) :
	curve = pm.curve ( d = 1 , p = curve ) ;
	curve.rename ( name ) ;
	return curve ;

####################
####################
''' maya utilities '''
####################
####################

def nodeConstraint_cmd ( master , slave , *args ) :
    # return mulMatrix_node , decomposeMatrix_node

    master_tfm = pm.general.PyNode ( master ) ;
    slave_tfm = pm.general.PyNode ( slave ) ;

    master_nm = makeName_cmd ( target = master_tfm ) ;
    slave_nm = makeName_cmd ( target = slave_tfm ) ;

    mulMatrix_node = pm.createNode ( 'multMatrix' , n = master_nm + 'NCon_mmt' ) ;
    decomposeMatrix_node = pm.createNode ( 'decomposeMatrix' , n = master_nm + 'NCon_dcm' ) ;

    master_tfm.worldMatrix >> mulMatrix_node.matrixIn[0] ;
    slave.parentInverseMatrix >> mulMatrix_node.matrixIn[1] ;

    mulMatrix_node.matrixSum >> decomposeMatrix_node.inputMatrix ;

    decomposeMatrix_node.outputTranslate >> slave.translate ;
    decomposeMatrix_node.outputRotate >> slave.rotate ;
    decomposeMatrix_node.outputScale >> slave.scale ;

    return ( mulMatrix_node , decomposeMatrix_node ) ;

def reverseNodeConstraint_cmd ( master , slave , *args ) :
    # return decomposeMatrix_node

    master_tfm = pm.general.PyNode ( master ) ;
    slave_tfm = pm.general.PyNode ( slave ) ;

    master_nm = makeName_cmd ( target = master_tfm ) ;
    slave_nm = makeName_cmd ( target = slave_tfm ) ;

    decomposeMatrix_node = pm.createNode ( 'decomposeMatrix' , n = master_nm + 'RNCon_dcm' ) ;

    master_tfm.worldInverseMatrix >> decomposeMatrix_node.inputMatrix ;

    decomposeMatrix_node.outputTranslate >> slave.translate ;
    decomposeMatrix_node.outputRotate >> slave.rotate ;
    decomposeMatrix_node.outputScale >> slave.scale ;

    return ( decomposeMatrix_node ) ;

def bakeDummyAnim_cmd ( target , startFrame , endFrame , sampleBy , *args ) :

    currentModelPanels_list = getCurrentModelPanel_cmd ( ) ;
    currentModelPanels_list.sort ( ) ;

    currentPanelShowDict_list = [] ;

    for each in currentModelPanels_list :

        currentPanelShowDict_list.append ( getShowPanelStatus_cmd ( panel = each ) ) ;

        pm.modelEditor ( each , e = True , allObjects = 0 ) ;

    pm.bakeResults ( target ,
        at = [ "tx" , "ty" , "tz" , "rx" , "ry" , "rz" ] , t = ( startFrame , endFrame ) , sb = sampleBy , simulation = True , minimizeRotation = True ) ;     

    # def setPanelShow_cmd ( setDict = '' , panel = '' , *args ) :

    for i in range ( 0 , len ( currentModelPanels_list ) ) :
        setPanelShow_cmd ( setDict = currentPanelShowDict_list [i] , panel =  currentModelPanels_list [i] ) ;

####################
####################
''' track function '''
####################
####################

def track_cmd ( *args ) :

    # reset report
    # pm.scrollField ( 'report_scrollField' , e = True , text = '#report' , bgc = ( 0 , 0 , 0 ) ) ;

    selection = pm.ls ( sl = True ) ;

    if len ( selection ) == 0 :
        # pm.scrollField ( 'report_scrollField' , e = True , text = '# error : no object selected' , bgc = ( 1 , 0.5 , 0.5 ) ) ;
        pass ;

    elif len ( selection ) != 1 :
        #pm.scrollField ( 'report_scrollField' , e = True , text = '# error : more than 1 object is selected' , bgc = ( 1 , 0.5 , 0.5 ) ) ;
        pass ;

    else :

        target_tfm = selection[0] ;
        target_tfm = pm.general.PyNode ( target_tfm ) ;
        target_nm = makeName_cmd ( target = target_tfm ) ;

        	# make dummy_tfm rotation = world
        
        dummy_tfm = pm.group ( em = True , w = True , n = target_nm + '_dummy_tfm' ) ;
        pointCon = pm.pointConstraint ( target_tfm , dummy_tfm , mo = False , skip = 'none' ) ;
        pm.delete ( pointCon ) ;

        	###

        parCon = pm.parentConstraint ( target_tfm , dummy_tfm , mo = True , skipRotate = 'none' , skipTranslate = 'none' ) ;

        bakeDummyAnim_cmd (
            target      = dummy_tfm ,
            startFrame  = pm.intField ( 'startFrame_intField' , q = True , v = True ) ,
            endFrame    = pm.intField ( 'endFrame_intField' , q = True , v = True ) ,
            sampleBy    = pm.floatField ( 'sample_floatField' , q = True , v = True )
            ) ;

        pm.delete ( parCon ) ;

        	# make origin_tfm
        makeOrigin_tfm = pm.group ( em = True , w = True , n = target_nm + '_makeOrigin_tfm' ) ;
        reverseNodeConstraint_cmd ( master = dummy_tfm , slave = makeOrigin_tfm ) ;   

        	# make controller

        main_ctrl = 

        '''
        
  
             

        ### test ###

        makeOrigin_tfm.addAttr ( '__AMP__' , keyable = True , attributeType = 'float' , dv = 0 ) ;
        makeOrigin_tfm.__AMP__.lock ( ) ;

        for attribute in [ 't' , 'r' ] :
            for axis in [ 'x' , 'y' , 'z' ] :        
                makeOrigin_tfm.addAttr (  attribute + axis + 'Amp' , keyable = True , attributeType = 'float' , dv = 1 ) ;

        bwdTranslateAmp_mdv = pm.createNode ( 'multiplyDivide' , n = target_nm + 'BwdTranslateAmp_pma' ) ;
        bwdRotateAmp_mdv = pm.createNode ( 'multiplyDivide' , n = target_nm + 'BwdTranslateAmp_pma' ) ;

        dummy_tfm.t >> bwdTranslateAmp_mdv.i1 ;
        makeOrigin_tfm.txAmp >> bwdTranslateAmp_mdv.i2x ;
        makeOrigin_tfm.tyAmp >> bwdTranslateAmp_mdv.i2y ;
        makeOrigin_tfm.tzAmp >> bwdTranslateAmp_mdv.i2z ;

        dummy_tfm.r >> bwdRotateAmp_mdv.i1 ;
        makeOrigin_tfm.rxAmp >> bwdRotateAmp_mdv.i2x ;
        makeOrigin_tfm.ryAmp >> bwdRotateAmp_mdv.i2y ;
        makeOrigin_tfm.rzAmp >> bwdRotateAmp_mdv.i2z ;
		'''

        #bwdTranslate_pma = pm.createNode ( 'plusMinusAverage' , n = target_nm + 'BwdTranslate_pma' ) ;
        #bwdRotate_pma = pm.createNode ( 'plusMinusAverage' , n = target_nm + 'BwdRotate_pma' ) ;
        
        #dummy_tfm.t >> bwdTranslate_pma.input3D[0] ;
        #bwdTranslateAmp_mdv.o >> bwdTranslate_pma.input3D[1] ;

        #dummy_tfm.r >> bwdRotate_pma.input3D[0] ;
        #bwdRotateAmp_mdv.o >> bwdRotate_pma.input3D[1] ;

        BWD_GRP = pm.group ( em = True , w = True , n = target_nm + 'BWD_GRP' ) ;

        bwdTranslateAmp_mdv.o >> BWD_GRP.t ;
        bwdRotateAmp_mdv.o >> BWD_GRP.r ;

        '''
        BWDGrpTfm_pma = 
        
        BWD_Tfm = pm.group ( em = True , w = True , n = target_nm + 'BWD_GRP' ) ;
        
        Amp_Tfm
        '''
        
##########
##########
''' UI '''
##########
##########
width = 300.00 ;

def refresh_cmd ( *args ) :

    # reset report
    #pm.scrollField ( 'report_scrollField' , e = True , text = '#report' , bgc = ( 0 , 0 , 0 ) ) ;

    startEndFrame = timeRangeQuery_cmd ( ) ;
    pm.intField ( 'startFrame_intField' , e = True , v = startEndFrame[0] ) ;
    pm.intField ( 'endFrame_intField' , e = True , v = startEndFrame[1]  ) ;
    pm.intField ( 'posFrame_intField' , e = True , v = startEndFrame[0]  ) ;

def filler ( *args ) :
    pm.text ( label = '' ) ;

def separator ( *args ) :
    pm.separator ( h = 5 , vis = 0 ) ;

def run ( *args ) :

    if pm.window ( 'absoluteTrack_UI' , exists = True ) :
        pm.deleteUI ( 'absoluteTrack_UI' ) ;
    else : pass ;
   
    window = pm.window ( 'absoluteTrack_UI', title = "absolute track revamp v1" ,
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'absoluteTrack_UI' , e = True , w = width , h = 10 ) ;
    
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            with pm.frameLayout ( label = 'RiFF studio 2017 / Weerapot C.' , collapsable = False , collapse = False , bgc = ( 0 , 0 , 0.5 ) ) :

                with pm.rowColumnLayout ( nc = 1 , w = width ) :
        
                    with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/3 ) , ( 2 , width/3 ) , ( 3 , width/3 ) ] ) :
                    
                        with pm.rowColumnLayout ( nc = 1 , w = width/3 ) :    
                            pm.text ( label = 'start frame' , w = width/3 ) ;
                            pm.intField ( 'startFrame_intField' , w = width/3  ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/3 ) :
                            pm.text ( label = 'end frame' , w = width/3 ) ;
                            pm.intField ( 'endFrame_intField' , w = width/3  ) ;
                        
                        with pm.rowColumnLayout ( nc = 1 , w = width/3 ) :
                            pm.text ( label = 'default pos frame' , w = width/3 ) ;        
                            pm.intField ( 'posFrame_intField' , w = width/3  ) ;

                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/3*2 ) , ( 2 , width/3 ) ] ) :

                        filler ( ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/3 ) :
                            pm.text ( label = 'sample by' ) ;
                            pm.floatField ( 'sample_floatField' , pre = 2 , w = width/3 , v = 1.00 ) ;

                    separator ( ) ;

                    '''
                    statusProgressBar_layout = pm.rowColumnLayout ( 'statusProgressBar_layout' , nc = 1 , w = width ) ;
                    with statusProgressBar_layout :
                        status_progressBar = ProgressBar ( name = 'status_progressBar' ) ;
                        status_progressBar.insert ( ) ;

                    reportScrollField_layout = pm.rowColumnLayout ( 'report_scrollField_layout' , nc = 1 , w = width ) ;
                    with reportScrollField_layout :
                        report_scrollField = StatusReport ( name = 'report_scrollField' ) ;
                        report_scrollField.insert ( parent = reportScrollField_layout ) ;
                    '''

                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = 'refresh' , w = width/2 , bgc = (  0.5 , 1 , 0.85 ) , c = refresh_cmd ) ;
                        pm.button ( label = 'track!' , w = width/2 , bgc = ( 1 , 0.85 , 0 ) , c = track_cmd ) ;
                                    
    refresh_cmd ( ) ;
    
    window.show () ;

#run ( ) ;

"""
modelPanels_list = getCurrentModelPanel_cmd () ;

modelPanelList_dict = {} ;

for panel in modelPanels_list :

    print getShowPanelStatus_cmd ( panel = panel ) ;

    command = '''
{panel}_dict = getShowPanelStatus_cmd ( panel = '{panel}' ) ;
modelPanelList_dict['{panel}_dict'] = {panel}_dict '''.format ( panel = str ( panel ) ) ;
    
    exec command ;

print modelPanelList_dict ;
"""
