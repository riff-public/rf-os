import pymel.core as pm ;

##################
''' UI '''
##################

def separator ( times = 1 , *args ) :
    for i in range ( 0 , times ) :
        pm.separator ( h = 5 , vis = False ) ;

def filler ( times = 1 , *args ) :
    for i in range ( 0 , times ) :
        pm.text ( label = ' ' ) ;

def run ( *args ) :

    title   = 'preset manager' ;
    title   += ' ' + 'v0.0' ;
    title   += ' ' + '2017/12/21' ;

    window_UI = 'presetManager_UI' ;
    
    width = 500.00 ;

    # CHECK IF WINDOW EXISTS
    if pm.window ( window_UI , exists = True ) :
        pm.deleteUI ( window_UI ) ;
    else : pass ;

    window = pm.window ( window_UI , title = title , mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    pm.window ( window_UI , e = True , w = width , h = 10 ) ;
    with window :

        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :

    window.show ( ) ;