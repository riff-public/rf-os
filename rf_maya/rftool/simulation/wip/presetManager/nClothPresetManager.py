import pymel.core as pm ;
import maya.cmds as mc ;
import os ;

presetDirectory = r'P:\_TechAnim\preset' ;

def browseDirectory ( *args ) :
    import os ;
    os.startfile ( presetDirectory ) ;

def updateProjectList ( *args ) :

    projectList = os.listdir ( presetDirectory ) ;
    for each in projectList :
        pm.menuItem ( label = each ) ;

##################################################################

def getShot ( *args ) :
    
    selfPath = mc.file ( q = True , exn = True ) ;

    f = selfPath.split('film')[1].split('/')[0] ;
     
    q = selfPath.split('film')[1].split('/')[1].split('q')[1] ;
    
    s = selfPath.split('film')[1].split('/')[2].split('s')[1] ;
    
    c = selfPath.split('film')[1].split('/')[3] ;

    return [ f , q , s , c ] ;
 
def updateShotInfo ( *args ) :

    shotInfo = getShot ( ) ;

    pm.textField ( 'filmField' , e = True , text = shotInfo [0] ) ;
    pm.textField ( 'sequenceField' , e = True , text = shotInfo [1] ) ;
    pm.textField ( 'shotField' , e = True , text = shotInfo [2] ) ;

    pm.textField ( 'charField' , e = True , text = shotInfo [3] ) ;

def refresh ( *args ) :

    updateShotInfo ( ) ;

##################################################################

def nClothAttr ( *args ) :

    selection = pm.ls ( sl = True ) ;

    nCloth = [] ;

    nClothDict = { } ;

    for each in selection :
        
        eachShapes = pm.listRelatives ( each , shapes = True ) ;
        
        for shape in eachShapes :

            nCloth = pm.listConnections ( shape , type = 'nCloth' ) ;
            
            if nCloth == [] :
                pass ;
            else :
                nCloth = pm.general.PyNode ( nCloth [0] ) ;
                
                nClothAttrDic = { } ;

                nClothAttrDic [ 'isDynamic' ] = nCloth.isDynamic.get ( ) ;
                
                # collisions #
                
                nClothAttrDic [ 'collide' ] = nCloth.collide.get ( ) ;
                nClothAttrDic [ 'selfCollide' ] = nCloth.selfCollide.get ( ) ;
                nClothAttrDic [ 'collisionFlag' ] = nCloth.collisionFlag.get ( ) ;
                
                nClothAttrDic [ 'selfCollisionFlag' ] = nCloth.selfCollisionFlag.get ( ) ;
                nClothAttrDic [ 'collideStrength' ] = nCloth.collideStrength.get ( ) ;
                nClothAttrDic [ 'collisionLayer' ] = nCloth.collisionLayer.get ( ) ;
                nClothAttrDic [ 'thickness' ] = nCloth.thickness.get ( ) ;
                nClothAttrDic [ 'selfCollideWidthScale' ] = nCloth.selfCollideWidthScale.get ( ) ;
                nClothAttrDic [ 'solverDisplay' ] = nCloth.solverDisplay.get ( ) ;
                nClothAttrDic [ 'displayColor' ] = nCloth.displayColor.get ( ) ;
                nClothAttrDic [ 'bounce' ] = nCloth.bounce.get ( ) ;
                nClothAttrDic [ 'friction' ] = nCloth.friction.get ( ) ;
                nClothAttrDic [ 'stickiness' ] = nCloth.stickiness.get ( ) ;
               
                # dynamic properties #

                nClothAttrDic [ 'stretchResistance' ] = nCloth.stretchResistance.get ( ) ;
                nClothAttrDic [ 'compressionResistance' ] = nCloth.compressionResistance.get ( ) ;
                nClothAttrDic [ 'bendResistance' ] = nCloth.bendResistance.get ( ) ;
                nClothAttrDic [ 'bendAngleDropoff' ] = nCloth.bendAngleDropoff.get ( ) ;
                nClothAttrDic [ 'shearResistance' ] = nCloth.shearResistance.get ( ) ;

                nClothAttrDic [ 'restitutionAngle' ] = nCloth.restitutionAngle.get ( ) ;
                nClothAttrDic [ 'restitutionTension' ] = nCloth.restitutionTension.get ( ) ;

                nClothAttrDic [ 'rigidity' ] = nCloth.rigidity.get ( ) ;
                nClothAttrDic [ 'deformResistance' ] = nCloth.deformResistance.get ( ) ;
                nClothAttrDic [ 'usePolygonShells' ] = nCloth.usePolygonShells.get ( ) ;

                nClothAttrDic [ 'inputMeshAttract' ] = nCloth.inputMeshAttract.get ( ) ;
                nClothAttrDic [ 'inputAttractMethod' ] = nCloth.inputAttractMethod.get ( ) ;
                nClothAttrDic [ 'inputAttractDamp' ] = nCloth.inputAttractDamp.get ( ) ;
                nClothAttrDic [ 'inputMotionDrag' ] = nCloth.inputMotionDrag.get ( ) ;
                nClothAttrDic [ 'restLengthScale' ] = nCloth.restLengthScale.get ( ) ;
                nClothAttrDic [ 'bendAngleScale' ] = nCloth.bendAngleScale.get ( ) ;
                nClothAttrDic [ 'pointMass' ] = nCloth.pointMass.get ( ) ;
                nClothAttrDic [ 'lift' ] = nCloth.lift.get ( ) ;
                nClothAttrDic [ 'drag' ] = nCloth.drag.get ( ) ;
                nClothAttrDic [ 'damp' ] = nCloth.damp.get ( ) ;
                nClothAttrDic [ 'stretchDamp' ] = nCloth.stretchDamp.get ( ) ;
                nClothAttrDic [ 'scalingRelation' ] = nCloth.scalingRelation.get ( ) ;
                nClothAttrDic [ 'ignoreSolverGravity' ] = nCloth.ignoreSolverGravity.get ( ) ;
                nClothAttrDic [ 'ignoreSolverWind' ] = nCloth.ignoreSolverWind.get ( ) ;
                nClothAttrDic [ 'localForceX' ] = nCloth.localForceX.get ( ) ;
                nClothAttrDic [ 'localForceY' ] = nCloth.localForceY.get ( ) ;
                nClothAttrDic [ 'localForceZ' ] = nCloth.localForceZ.get ( ) ;
                nClothAttrDic [ 'localWindX' ] = nCloth.localWindX.get ( ) ;
                nClothAttrDic [ 'localWindY' ] = nCloth.localWindY.get ( ) ;
                nClothAttrDic [ 'localWindZ' ] = nCloth.localWindZ.get ( ) ;

                nClothDict [ str ( each ) ] = nClothAttrDic ;
            
                return nClothAttrDic ;

def saveNCloth ( *args ) :
    nCloth = nClothAttr ( ) ;


    shot = getShot ( ) ;
    #return [ f , q , s , c ] ;
    f = shot [0] ;
    q = shot [1] ;
    s = shot [2] ;
    c = shot [3] ;

    targetDirectory = presetDirectory + '/' + c + '/' + 'f' + f + 'q' + q + 's' + s ;

    print targetDirectory ;

    #if os.path.exists ( presetDirectory + )

def run ( *args ) :
    
    # check if window exists
    if pm.window ( 'nClothPresetManager_UI' , exists = True ) :
        pm.deleteUI ( 'nClothPresetManager_UI' ) ;
    else : pass ;
   
    window = pm.window ( 'nClothPresetManager_UI', title = "nCloth preset manager" ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'nClothPresetManager_UI' , e = True , w = 450 , h = 100 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 , w = 450 ) ;
        with mainLayout :
            
            pm.button ( label = 'current directory = %s' % presetDirectory , c = browseDirectory , bgc = ( 1 , 0.411765 , 0.705882 ) ) ;

            simTypeOpt = pm.optionMenu ( 'simTypeOpt' , label='project' , w = 450 ) ;
            with simTypeOpt :

                updateProjectList ( ) ;

            pm.separator ( vis = False , h = 10 ) ;

            textScrollLayout = pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , 150 ) , ( 2 , 150 ) , ( 3 , 150 ) ] ) ;
            with textScrollLayout :

                pm.text ( label = 'CHARACTER' , font = 'boldLabelFont' , bgc = ( 0 , .5 , 0.6 ) ) ;
                pm.text ( label = 'SHOT' , font = 'boldLabelFont' , bgc = ( 0 , 0.75 , 0.8 ) ) ;
                pm.text ( label = 'NCLOTH' , font = 'boldLabelFont' , bgc = ( 0 , 1 , 1 ) ) ;

                pm.textScrollList ( 'cList' , h = 200 , ams = False ) ;
                pm.textScrollList ( 'sList' , h = 200 , ams = False ) ;
                pm.textScrollList ( 'nList' , h = 200 , ams = True ) ;

            pm.button ( label = 'refresh' , bgc = ( 0.498039 , 1 , 0.831373 ) ) ;

            pm.separator ( vis = False , h = 10 ) ;

            infoLayout = pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , 150 ) , ( 2 , 150 ) , ( 3 , 150 ) ] ) ;
            with infoLayout :

                pm.text ( label = 'film' ) ;
                pm.text ( label = 'sequence' ) ;
                pm.text ( label = 'shot' ) ;
                
                pm.textField ( 'filmField' ) ;
                pm.textField ( 'sequenceField' ) ;
                pm.textField ( 'shotField' ) ;


            charInfoLayout = pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , 150 ) , ( 2 , 300 ) ] ) ;
            with charInfoLayout :

                pm.text ( label = 'character' ) ;
                pm.text ( label = 'comment' ) ;
                
                pm.textField ( 'charField' ) ;
                pm.textField ( 'commentField' ) ;

            pm.separator ( vis = False , h = 10 ) ;

            pm.button ( label = 'save nCloth preset' , c = saveNCloth , bgc = ( 1 , 0.843137 , 0 ) ) ;
            
            pm.separator ( vis = False , h = 10 ) ;

            pm.button ( label = 'set nCloth' , bgc = ( 0.498039 , 1 , 0 ) ) ;
            pm.button ( label = 'set nucleus' , bgc = ( 0.196078 , 0.803922 , 0.196078 ) ) ;

            refresh ( ) ;

    window.show ( ) ;