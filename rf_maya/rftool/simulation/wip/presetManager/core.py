presetPath = r'C:\Users\weerapot.c\Desktop\preset' ;


class NNode ( object ) :

    def __init__ ( self , node ) :
        self.node = node ;

    def __str__ ( self ) :
        return str ( self.node ) ;

    def __repr__ ( self ) :
        return str ( self.node ) ;
    
    ##################
    ''' nCloth '''
    ##################

    def getNCloth ( self ) :

        if self.node.getShape().nodeType() == 'nCloth' :
            return self.node ; 

        else :
            shapes_list = self.node.listRelatives ( shapes = True ) ;

            connection_list = [] ;

            for shape in shapes_list :
                connection = shape.listConnections ( type = 'nCloth' ) [0] ;
                if connection :
                    if connection not in connection_list :
                        connection_list.append ( connection ) ;

            if ( connection_list ) == [] :
                return None ;
            else :
                if len ( connection_list ) > 1 :
                    pm.error ( 'more than 1 nCloth shape detected in a single node, please check' ) ;
                elif len ( connection_list ) == 1 :
                    return connection_list [0] ;

    def getNClothShape ( self ) :
        nCloth      = self.getNCloth ( ) ;
        nClothShape = nCloth.getShape ( ) ;
        return nClothShape ;

    ##################
    ''' nHair '''
    ##################

    def getNHair ( self ) :
        pass ;

    def getNHairShape ( self ) :
        pass ;

    ##################
    ''' nucleus '''
    ##################
    
    def getNucleus ( self ) :

        if self.getNCloth ( ) :
            target = self.getNClothShape ( ) ;
        elif self.getNHair ( ) :
            target = self.getNHairShape ( ) ;

        ### if node == nCloth ###
        try :

            nucleus = nClothShape.listConnections ( type = 'nucleus' ) [0]  ;
            return ( nucleus ) ;
        except :
            pass ;

# def getNCloth ( ) :
#         # [ GNCIDX ]
#         # return nCloth, nClothShape , nucleus
        
#         #########
#         ''' get shapes '''
#         #########
#         shape_list = pm.listRelatives ( self.target , shapes = True ) ;
        
#         #########
#         ''' find nCloth '''
#         #########        
#         for shape in shape_list :
#             connection = pm.listConnections ( shape , type = 'nCloth' ) ;
            
#             if connection == [] :
#                 pass ;
#             else : 
#                 nCloth = connection [0] ;
        
#             #########
#             ''' find nCloth shape '''
#             #########
#             shape = pm.listRelatives ( nCloth , shapes = True ) [0] ;
            
#             if str ( pm.nodeType ( shape ) ) == 'nCloth' :
#                 nClothShape = shape ;
#                 nucleus = pm.listConnections ( nClothShape , type = 'nucleus' ) [0] ;
#             else : pass ;
            
#             #########
#             ''' return '''
#             #########  

#             return nCloth , nClothShape , nucleus ;
