char_dict = {} ;

##################
##################
''' SRU '''
##################
##################

char_dict['extra/captainSRU'] 		= [ 'SRU/captainSRU' ] ;
char_dict['extra/ericBandageSRU'] 	= [ 'SRU/ericBandageSRU' ] ;
char_dict['extra/ericSRU'] 			= [ 'SRU/ericSRU' ] ;
char_dict['main/hanumanSRU'] 		= [ 'SRU/hanumanSRU' ] ;
char_dict['extra/hippoSRU'] 		= [ 'SRU/hippoSRU' ] ;
char_dict['extra/hippoScarSRU'] 	= [ 'SRU/hippoScarSRU' ] ;
char_dict['extra/kevinGauzeSRU'] 	= [ 'SRU/kevinGauzeSRU' ] ;
char_dict['extra/kevinSRU'] 		= [ 'SRU/kevinSRU' ] ;
char_dict['extra/lilySRU'] 			= [ 'SRU/lilySRU' ] ;
#char_dict['/robotSRU'] 			= [ 'SRU/robotSRU' ] ;
char_dict['main/sunnySRU'] 			= [ 'SRU/sunnySRU' ] ;

##################
##################
''' main '''
##################
##################

#char_dict['/hanuman'] 					= [ 'main/hanuman' ] ;
char_dict['main/hanumanAnSuit'] 		= [ 'main/hanumanAncientArmor' ] ;
#char_dict['/hanumanApron'] 			= [ 'main/hanumanApron' ] ;
char_dict['main/hanumanWarSuit'] 		= [ 'main/hanumanArmor' ] ;
#char_dict['/hanumanBronze'] 			= [ 'main/hanumanBronze' ] ;
char_dict['main/hanumanCasualSuit'] 	= [ 'main/hanumanCasual' ] ;
#char_dict['/hanumanNaked'] 			= [ 'main/hanumanNaked' ] ;
#char_dict['/hanumanNoRedCloth'] 		= [ 'main/hanumanNoRedCloth' ] ;
#char_dict['/mayorLokCity'] 			= [ 'main/mayorLokCity' ] ;
#char_dict['/sonGoku'] 					= [ 'main/sonGoku' ] ;
char_dict['main/sonGokuAnSuit'] 		= [ 'main/sonGokuAncientArmor' ] ;
char_dict['main/sonGokuWarSuit'] 		= [ 'main/sonGokuArmor' ] ;
char_dict['main/sunnyCasualSuit'] 		= [ 'main/sunnyCasual' ] ;
char_dict['main/sunnyDad'] 				= [ 'main/sunnyDad' ] ;
char_dict['main/sunnyDadPajama'] 		= [ 'main/sunnyDadPajama' ] ;

##################
##################
''' demon '''
##################
##################

char_dict['main/blueDemon'] 		= [ 'demon/blueDemon' ] ;
#char_dict['/blueDemonUnborn'] 		= [ 'demon/blueDemonUnborn' ] ;

char_dict['extra/bodyDemon'] 		= [ 'demon/bodyDemon' ] ;
char_dict['extra/bodyDemonBone'] 	= [ 'demon/bodyDemonBone' ] ;
#char_dict['/bodyHuman'] 			= [ 'demon/bodyHuman' ] ;
#char_dict['/bodyHumanTux'] 		= [ 'demon/bodyHumanTux' ] ;

char_dict['extra/brainDemon'] 		= [ 'demon/brainDemon' ] ;
#char_dict['/brainHuman'] 			= [ 'demon/brainHuman' ] ;
char_dict['extra/brainDemonDoctor'] = [ 'demon/brainHumanDoc' ] ;
#char_dict['/brainHumanTux'] 		= [ 'demon/brainHumanTux' ] ;

#char_dict['/bullCalf'] 			= [ 'demon/bullCalf' ] ;
char_dict['extra/bullDemon'] 		= [ 'demon/bullDemon' ] ;
char_dict['extra/bullDemonBaby'] 	= [ 'demon/bullDemonBaby' ] ;
char_dict['extra/bullDemonKing']	= [ 'demon/bullDemonKing' ] ;
#char_dict['/bullHuman'] 			= [ 'demon/bullHuman' ] ;

char_dict['extra/earsDemon'] 		= [ 'demon/earsDemon' ] ;
char_dict['extra/earsDemonBone'] 	= [ 'demon/earsDemonBone' ] ;
#char_dict['/earsHuman'] = [ 'demon/earsHuman' ] ;
#char_dict['/earsHumanTux'] = [ 'demon/earsHumanTux' ] ;

char_dict['extra/eyesDemon'] 		= [ 'demon/eyesDemon' ] ;
#char_dict['/eyesHuman'] 			= [ 'demon/eyesHuman' ] ;
char_dict['extra/eyesDemonDoctor'] 	= [ 'demon/eyesHumanDoc' ] ;
#char_dict['/eyesHumanTux'] 		= [ 'demon/eyesHumanTux' ] ;

#char_dict['/hornGold'] 			= [ 'demon/hornGold' ] ;
#char_dict['/hornSilver'] 			= [ 'demon/hornSilver' ] ;

char_dict['extra/noseDemon'] 		= [ 'demon/noseDemon' ] ;
char_dict['extra/noseDemonBone'] 	= [ 'demon/noseDemonBone' ] ;
#char_dict['/noseHuman'] = [ 'demon/noseHuman' ] ;
#char_dict['/noseHumanTux'] = [ 'demon/noseHumanTux' ] ;

#char_dict['/phantomDemon01'] 		= [ 'demon/phantomDemon01' ] ;
#char_dict['/phantomDemon02'] 		= [ 'demon/phantomDemon02' ] ;
#char_dict['/phantomDemon03'] 		= [ 'demon/phantomDemon03' ] ;
#char_dict['/phantomDemon04'] 		= [ 'demon/phantomDemon04' ] ;
#char_dict['/phantomDemon05'] 		= [ 'demon/phantomDemon05' ] ;
#char_dict['/phantomDemon06'] 		= [ 'demon/phantomDemon06' ] ;

char_dict['extra/skeletonDemon'] 	= [ 'demon/skeletonDemon' ] ;
#char_dict['/skeletonHuman'] 		= [ 'demon/skeletonHuman' ] ;

char_dict['extra/tongueDemon'] = [ 'demon/tongueDemon' ] ;
char_dict['extra/tongueDemonBone'] = [ 'demon/tongueDemonBone' ] ;
#char_dict['/tongueHuman'] = [ 'demon/tongueHuman' ] ;
#char_dict['/tongueHumanTux'] = [ 'demon/tongueHumanTux' ] ;

char_dict['extra/westernDemonA'] = [ 'demon/westernDemon01' ] ;
char_dict['extra/westernDemonB'] = [ 'demon/westernDemon02' ] ;
char_dict['extra/westernDemonC'] = [ 'demon/westernDemon03' ] ;

##################
##################
''' god '''
##################
##################

#char_dict['/chineseGod01'] 	= [ 'god/chineseGod01' ] ;
#char_dict['/chineseGod02'] 	= [ 'god/chineseGod02' ] ;
#char_dict['/chineseGod03'] 	= [ 'god/chineseGod03' ] ;
#char_dict['/chineseGod04'] 	= [ 'god/chineseGod04' ] ;
#char_dict['/chineseGod05'] 	= [ 'god/chineseGod05' ] ;
#char_dict['/chineseGod06'] 	= [ 'god/chineseGod06' ] ;
#char_dict['/chineseGod07'] 	= [ 'god/chineseGod07' ] ;
#char_dict['/chineseGod08'] 	= [ 'god/chineseGod08' ] ;

#char_dict['/dragonSerpent'] 	= [ 'god/dragonSerpent' ] ;

#char_dict['/eagleGaruda'] 		= [ 'god/eagleGaruda' ] ;

#char_dict['/emperorGod'] 		= [ 'god/emperorGod' ] ;

#char_dict['/indianGod01'] 		= [ 'god/indianGod01' ] ;
#char_dict['/indianGod02'] 		= [ 'god/indianGod02' ] ;
#char_dict['/indianGod03'] 		= [ 'god/indianGod03' ] ;
#char_dict['/indianGod04'] 		= [ 'god/indianGod04' ] ;
#char_dict['/indianGod05'] 		= [ 'god/indianGod05' ] ;

#char_dict['/luteGod'] 			= [ 'god/luteGod' ] ;

#char_dict['/monkShaGod'] 		= [ 'god/monkShaGod' ] ;

#char_dict['/nezhaGod'] 		= [ 'god/nezhaGod' ] ;

#char_dict['/pagodaGod'] 		= [ 'god/pagodaGod' ] ;

#char_dict['/pigsyGod'] 		= [ 'god/pigsyGod' ] ;

#char_dict['/princessIronFanOldGod'] = [ 'god/princessIronFanOldGod' ] ;

#char_dict['/rainGod'] 			= [ 'god/rainGod' ] ;

#char_dict['/skyDogGod'] 		= [ 'god/skyDogGod' ] ;

char_dict['extra/soma'] 		= [ 'god/somaGod' ] ;

char_dict['main/spiderGirl'] 	= [ 'god/spiderGirlGod' ] ;
char_dict['main/spider'] 		= [ 'god/spiderSpiderGirlGod' ] ;

#char_dict['/swordGod'] 		= [ 'god/swordGod' ] ;

#char_dict['/threeEyesGod'] 	= [ 'god/threeEyesGod' ] ;

