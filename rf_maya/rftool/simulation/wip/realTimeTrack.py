import pymel.core as pm ;

class Curve ( object ) :

    def __init__ ( self ) :
        super ( Curve , self ).__init__() ;

        ### curve dict ###
        self.curve_dict = {} ;

        self.curve_dict['fourDirectionalArrow'] = [ ( -19.999955 , 0 , 9.999955 ) , ( -9.999955 , 0 , 19.999955 ) , ( -9.999955 , 0 , 39.999954 ) , ( -19.999955 , 0 , 39.999954 ) , ( 4.47035e-005 , 0 , 59.99995 ) , ( 20.000044 , 0 , 39.999954 ) , ( 10.000044 , 0 , 39.999954 ) , ( 10.000044 , 0 , 19.999955 ) , ( 20.000044 , 0 , 9.999955 ) , ( 40.000044 , 0 , 9.999955 ) , ( 40.000044 , 0 , 19.999955 ) , ( 60.00004 , 0 , -4.47035e-005 ) , ( 40.000044 , 0 , -20.000044 ) , ( 40.000044 , 0 , -10.000044 ) , ( 20.000044 , 0 , -10.000044 ) , ( 10.000044 , 0 , -20.000044 ) , ( 10.000044 , 0 , -40.000044 ) , ( 20.000044 , 0 , -40.000044 ) , ( 4.47035e-005 , 0 , -60.00004 ) , ( -19.999955 , 0 , -40.000044 ) , ( -9.999955 , 0 , -40.000044 ) , ( -9.999955 , 0 , -20.000044 ) , ( -19.999955 , 0 , -10.000044 ) , ( -39.999954 , 0 , -10.000044 ) , ( -39.999954 , 0 , -20.000044 ) , ( -59.99995 , 0 , -4.47035e-005 ) , ( -39.999954 , 0 , 19.999955 ) , ( -39.999954 , 0 , 9.999955 ) , ( -19.999955 , 0 , 9.999955 ) ] ;

        self.curve_dict['fly'] = [ ( 5.98151 , 0 , 7.68556 ) , ( 14.158347 , 19.425926 , -2.335602 ) , ( 14.158347 , 19.425926 , -29.0998 ) , ( 5.98151 , 0 , -7.685555 ) , ( -5.98151 , 0 , -7.685555 ) , ( -14.158347 , 19.425926 , -29.0998 ) , ( -14.158347 , 19.425926 , -2.335602 ) , ( -5.98151 , 0 , 7.68556 ) , ( 5.98151 , 0 , 7.68556 ) ] ;   

        ### color dict ###

        self.color_dict = {} ;

        self.color_dict['none']     = [ False   , ( 0 , 0 , 0 ) ] ;
        self.color_dict['white']    = [ True    , ( 1 , 1 , 1 ) ] ;
        self.color_dict['red']      = [ True    , ( 1 , 0 , 0 ) ] ;
        self.color_dict['blue']     = [ True    , ( 0 , 0 , 1 ) ] ;
        self.color_dict['green']    = [ True    , ( 0 , 1 , 0 ) ] ;
        self.color_dict['yellow']   = [ True    , ( 1 , 1 , 0 ) ] ;
        self.color_dict['lightBlue'] = [ True   , ( 0 , 1 , 1 ) ] ;
        self.color_dict['brown']    = [ True , ( 0.3 , 0.1 , 0.1 ) ] ;

    def createCurve ( self , type , name ) :
        curve = pm.curve ( d = 1 , p = self.curve_dict [ type ] ) ;
        curve.rename ( name ) ;
        print curve ;
        return curve ;

    def setCurveColor ( self , target , color ) :

        info    = self.color_dict [ color ] ;
        en      = info[0] ;
        color   = info[1] ;
        
        target = pm.general.PyNode ( target ) ;
        targetShape = target.getShape() ;
        targetShape.overrideEnabled.set ( )

        targetShape.overrideEnabled.set ( en ) ;
        targetShape.overrideRGBColors.set ( en ) ;
        targetShape.overrideColorRGB.set ( color ) ;

class InteractiveTrack ( Curve ) :

    def __init__ ( self ) :
        super ( InteractiveTrack, self ).__init__() ;

    def track ( self , target = 'Test' , *args ) :
        
        targetName = target ;

        ### controller ###

        mainCtrl = self.createCurve ( 'fourDirectionalArrow' , 'InteractiveTrack_Ctrl' ) ;
        mainCtrlShape = mainCtrl.getShape() ;

        mainCtrlShape.addAttr ( 'trackTime' , keyable = True , attributeType = 'time' , dv = 0 ) ;
        
        ### calculate the time difference , and abs it ###

        timeNode = pm.ls ( type = 'time' ) [0] ;

        timeDifPma = pm.createNode ( 'plusMinusAverage' , n = targetName + 'TrackTimeDif_Pma' ) ;
        timeDifPma.operation.set ( 2 ) ;

        mainCtrlShape.trackTime >> timeDifPma.input2D[0].input2Dx ;
        timeNode.outTime        >> timeDifPma.input2D[1].input2Dx

        powerMdv = pm.createNode ( 'multiplyDivide' , n = targetName + 'TrackPwr_Mdv' ) ;
        powerMdv.operation.set(3) ;
        for axis in [ 'x' , 'y' , 'z' ] :
            exec ( "powerMdv.i2%s.set(2) ;" % axis ) ;
        
        squareRootMdv = pm.createNode ( 'multiplyDivide' , n = targetName + 'TrackSqrRoot_Mdv' ) ;
        squareRootMdv.operation.set(3) ;
        for axis in [ 'x' , 'y' , 'z' ] :
            exec ( "powerMdv.o{axis} >> squareRootMdv.i1{axis} ;".format ( axis = axis ) ) ;
            exec ( "squareRootMdv.i2%s.set(0.5) ;" % axis ) ;

        timeDifPma.o2x >> powerMdv.i1x ;

        ### caculate the condition ###

        condition = pm.createNode ( 'condition' , n = targetName + 'Track_Con' ) ;
        condition.operation.set ( 4 ) ;
        # less than

        mainCtrlShape.trackTime >> condition.firstTerm ;
        timeNode.outTime        >> condition.secondTerm ;

        for color in [ 'R' , 'G' , 'B' ] :
          exec ( 'condition.colorIfTrue%s.set(-1)' % color ) ;
          exec ( 'condition.colorIfFalse%s.set(1)' % color ) ;

        

        condition.outColor.get() >> 

        #pm.createNode ( 'frameCache' ) ;

def run ( *args ) :

    it = InteractiveTrack() ;
    it.track() ;

run() ;