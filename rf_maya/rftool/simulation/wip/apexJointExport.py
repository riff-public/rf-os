import pymel.core as pm ;


def makeName_cmd ( target = '' , *args ) :
    targetSplit_list = target.split ( '_' ) ;
    name = targetSplit_list[0] ;
    for each in targetSplit_list[1:] :
        name += each.capitalize ( ) ;
    return name ;
    
def nodeConstraint_cmd ( master , slave , *args ) :
    # return mulMatrix_node , decomposeMatrix_node

    master_tfm = pm.general.PyNode ( master ) ;
    slave_tfm = pm.general.PyNode ( slave ) ;

    master_nm = makeName_cmd ( target = master_tfm ) ;
    slave_nm = makeName_cmd ( target = slave_tfm ) ;

    mulMatrix_node = pm.createNode ( 'multMatrix' , n = master_nm + 'NCon_mmt' ) ;
    decomposeMatrix_node = pm.createNode ( 'decomposeMatrix' , n = master_nm + 'NCon_dcm' ) ;

    master_tfm.worldMatrix >> mulMatrix_node.matrixIn[0] ;
    slave.parentInverseMatrix >> mulMatrix_node.matrixIn[1] ;

    mulMatrix_node.matrixSum >> decomposeMatrix_node.inputMatrix ;

    decomposeMatrix_node.outputTranslate >> slave.translate ;
    decomposeMatrix_node.outputRotate >> slave.rotate ;
    decomposeMatrix_node.outputScale >> slave.scale ;

    return ( mulMatrix_node , decomposeMatrix_node ) ;

def run_cmd ( target = 'Skin_Grp' ) :
    
    target = pm.general.PyNode ( target ) ;
    target_COUT = pm.group ( em = True , n = target + '_COUT' ) ;
      
    nodeConstraint_cmd ( master = 'Offset_Ctrl' , slave = target_COUT ) ;
    target.rotateOrder >> target_COUT.rotateOrder ;
    
    item_list = pm.listRelatives ( target , ad = True ) ;
    #item_list.sort( ) ;
    
    target_list = [] ;
    
    for item in item_list :
        if ( '_Grp' or '_Jnt' in str(item) ) and ( 'Constraint' not in str(item) ) :
            print target_list.append ( item ) ;
        else : pass ;
            
    #target_list.sort( );
            
    for target in target_list :
        if '_Grp' in str(target) :
            pm.group ( em = True , w = True , n = target + '_COUT' ) ;
        elif '_Jnt' in str(target) :
            pm.createNode ( 'joint' , n = target + '_COUT' ) ;
        else : pass ;
            
    for target in target_list :
        
       parent = pm.listRelatives ( target , parent = True ) ;
       if parent != [] :
           parent = parent[0] ;
           pm.parent ( target + '_COUT' , parent + '_COUT' ) ;
       else : pass ;
       
       target_COUT = pm.general.PyNode ( target + '_COUT' ) ;
       
       if '_Grp' in str(target) :
           
           target.t >> target_COUT.t ;
           target.r >> target_COUT.r ;
           target.s >> target_COUT.s ;
           target.rotateOrder >> target_COUT.rotateOrder ;
           
       elif '_Jnt' in str(target) :
           
           target.t >> target_COUT.t ;
           target.r >> target_COUT.r ;
           target.s >> target_COUT.s ;
           target.rotateOrder >> target_COUT.rotateOrder ;
           target.jointOrient >> target_COUT.jointOrient ;
           
run_cmd ( ) ;