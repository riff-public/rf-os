import pymel.core as pm ;

class simSetNode ( object ) :
    # getNCloth [ GNCIDX ]
    # getNClothSetting [ GNSIDX ]
    # getNHair [ GNHIDX ]
    # getNHairSetting [ GHSIDX ]

    def __init__ ( self , target ) :
        self.target = target ;
        
    def getNCloth ( self ) :
        # [ GNCIDX ]
        # return nCloth, nClothShape , nucleus
        
        #########
        ''' get shapes '''
        #########
        shape_list = pm.listRelatives ( self.target , shapes = True ) ;
        
        #########
        ''' find nCloth '''
        #########        
        for shape in shape_list :
            connection = pm.listConnections ( shape , type = 'nCloth' ) ;
            
            if connection == [] :
                pass ;
            else : 
                nCloth = connection [0] ;
        
            #########
            ''' find nCloth shape '''
            #########
            shape = pm.listRelatives ( nCloth , shapes = True ) [0] ;
            
            if str ( pm.nodeType ( shape ) ) == 'nCloth' :
                nClothShape = shape ;
                nucleus = pm.listConnections ( nClothShape , type = 'nucleus' ) [0] ;
            else : pass ;
            
            #########
            ''' return '''
            #########  

            return nCloth , nClothShape , nucleus ; 
        
    def getNClothSetting ( self ) :
        # [ GNSIDX ]
        # return nClothSetting_dict, nucleusSetting_dict
        #   nClothSetting_dict contains : collisions , dynamic properties

        nCloth_elem = self.getNCloth();
        
        nCloth = pm.general.PyNode ( nCloth_elem[0] ) ;
        nClothShape = pm.general.PyNode ( nCloth_elem[1] ) ;
        nucleus = pm.general.PyNode ( nCloth_elem[2] ) ;

        ##################
        ''' nCloth '''
        ##################

        nClothSetting_dict = {} ;
        
        ### collisions ###
        nClothSetting_dict['collide']               = nClothShape.collide.get() ;
        nClothSetting_dict['selfCollide']           = nClothShape.selfCollide.get() ;
        nClothSetting_dict['collisionFlag']         = nClothShape.collisionFlag.get() ;
        nClothSetting_dict['selfCollisionFlag']     = nClothShape.selfCollisionFlag.get() ;      
        nClothSetting_dict['collideStrength']       = nClothShape.collideStrength.get() ;      
        nClothSetting_dict['collisionLayer']        = nClothShape.collisionLayer.get() ;  

        nClothSetting_dict['thickness']             = nClothShape.thickness.get() ;  
        nClothSetting_dict['selfCollideWidthScale'] = nClothShape.selfCollideWidthScale.get() ;  
        
        nClothSetting_dict['solverDisplay']         = nClothShape.solverDisplay.get() ;  
        nClothSetting_dict['displayColor']          = nClothShape.displayColor.get() ;  

        nClothSetting_dict['bounce']                = nClothShape.bounce.get() ;  
        nClothSetting_dict['friction']              = nClothShape.friction.get() ;  
        nClothSetting_dict['stickiness']            = nClothShape.stickiness.get() ;  

        ### dynamic properties ###
        nClothSetting_dict['stretchResistance']     = nClothShape.stretchResistance.get() ;  
        nClothSetting_dict['compressionResistance'] = nClothShape.compressionResistance.get() ;  
        nClothSetting_dict['bendResistance']        = nClothShape.bendResistance.get() ;  
        nClothSetting_dict['bendAngleDropoff']      = nClothShape.bendAngleDropoff.get() ;  
        nClothSetting_dict['shearResistance']       = nClothShape.shearResistance.get() ;  

        nClothSetting_dict['restitutionAngle']      = nClothShape.restitutionAngle.get() ; 
        nClothSetting_dict['restitutionTension']    = nClothShape.restitutionTension.get() ; 

        nClothSetting_dict['rigidity']              = nClothShape.rigidity.get() ; 
        nClothSetting_dict['deformResistance']      = nClothShape.deformResistance.get() ; 
        nClothSetting_dict['usePolygonShells']      = nClothShape.usePolygonShells.get() ; 
        nClothSetting_dict['inputMeshAttract']      = nClothShape.inputMeshAttract.get() ; 
        nClothSetting_dict['inputAttractMethod']    = nClothShape.inputAttractMethod.get() ; 
        nClothSetting_dict['inputAttractDamp']      = nClothShape.inputAttractDamp.get() ; 
        nClothSetting_dict['inputMotionDrag']       = nClothShape.inputMotionDrag.get() ; 

        nClothSetting_dict['restLengthScale']       = nClothShape.restLengthScale.get() ; 
        nClothSetting_dict['bendAngleScale']        = nClothShape.bendAngleScale.get() ; 

        nClothSetting_dict['pointMass']             = nClothShape.pointMass.get() ; 
        nClothSetting_dict['lift']                  = nClothShape.lift.get() ; 
        nClothSetting_dict['drag']                  = nClothShape.drag.get() ; 
        nClothSetting_dict['tangentialDrag']        = nClothShape.tangentialDrag.get() ; 
        nClothSetting_dict['damp']                  = nClothShape.damp.get() ; 
        nClothSetting_dict['stretchDamp']           = nClothShape.stretchDamp.get() ; 

        nClothSetting_dict['scalingRelation']       = nClothShape.scalingRelation.get() ; 
        nClothSetting_dict['ignoreSolverGravity']   = nClothShape.ignoreSolverGravity.get() ; 
        nClothSetting_dict['ignoreSolverWind']      = nClothShape.ignoreSolverWind.get() ; 
        nClothSetting_dict['localForceX']           = nClothShape.localForceX.get() ; 
        nClothSetting_dict['localForceY']           = nClothShape.localForceY.get() ; 
        nClothSetting_dict['localForceZ']           = nClothShape.localForceZ.get() ; 
        nClothSetting_dict['localWindX']            = nClothShape.localWindX.get() ; 
        nClothSetting_dict['localWindY']            = nClothShape.localWindY.get() ; 
        nClothSetting_dict['localWindZ']            = nClothShape.localWindZ.get() ; 

        ##################
        ''' nucleus '''
        ##################

        nucleusSetting_dict = {} ;

        nucleusSetting_dict['enable']               = nucleus.enable.get() ;
        nucleusSetting_dict['visibility']           = nucleus.visibility.get() ;
        
        ### transform attributes ###
        nucleusSetting_dict['translateX']           = nucleus.translateX.get() ;
        nucleusSetting_dict['translateY']           = nucleus.translateY.get() ;
        nucleusSetting_dict['translateZ']           = nucleus.translateZ.get() ;
        
        nucleusSetting_dict['rotateX']              = nucleus.rotateX.get() ;
        nucleusSetting_dict['rotateY']              = nucleus.rotateY.get() ;
        nucleusSetting_dict['rotateZ']              = nucleus.rotateZ.get() ;
        
        nucleusSetting_dict['scaleX']               = nucleus.scaleX.get() ;
        nucleusSetting_dict['scaleY']               = nucleus.scaleY.get() ;
        nucleusSetting_dict['scaleZ']               = nucleus.scaleZ.get() ;
        
        nucleusSetting_dict['shearXY']              = nucleus.shearXY.get() ;
        nucleusSetting_dict['shearXZ']              = nucleus.shearXZ.get() ;
        nucleusSetting_dict['shearYZ']              = nucleus.shearYZ.get() ;
        
        nucleusSetting_dict['rotateOrder']          = nucleus.rotateOrder.get() ;
        nucleusSetting_dict['rotateAxisX']          = nucleus.rotateAxisX.get() ;
        nucleusSetting_dict['rotateAxisY']          = nucleus.rotateAxisY.get() ;
        nucleusSetting_dict['rotateAxisZ']          = nucleus.rotateAxisZ.get() ;
        
        nucleusSetting_dict['inheritsTransform']    = nucleus.inheritsTransform.get() ;

        ### gravity and wind ###
        nucleusSetting_dict['gravity']              = nucleus.gravity.get() ;

        nucleusSetting_dict['gravityDirectionX']    = nucleus.gravityDirectionX.get() ;
        nucleusSetting_dict['gravityDirectionY']    = nucleus.gravityDirectionY.get() ;
        nucleusSetting_dict['gravityDirectionZ']    = nucleus.gravityDirectionZ.get() ;
        
        nucleusSetting_dict['airDensity']           = nucleus.airDensity.get() ;
        
        nucleusSetting_dict['windSpeed']            = nucleus.windSpeed.get() ;
        
        nucleusSetting_dict['windDirectionX']       = nucleus.windDirectionX.get() ;
        nucleusSetting_dict['windDirectionY']       = nucleus.windDirectionY.get() ;
        nucleusSetting_dict['windDirectionZ']       = nucleus.windDirectionZ.get() ;
        
        nucleusSetting_dict['windNoise']            = nucleus.windNoise.get() ;

        ### solver attribute ###
        nucleusSetting_dict['subSteps']                 = nucleus.subSteps.get() ;
        nucleusSetting_dict['maxCollisionIterations']   = nucleus.maxCollisionIterations.get() ;
        nucleusSetting_dict['collisionLayerRange']      = nucleus.collisionLayerRange.get() ;
        nucleusSetting_dict['timingOutput']             = nucleus.timingOutput.get() ;
        nucleusSetting_dict['useTransform']             = nucleus.useTransform.get() ;

        ### scale attributes ###
        nucleusSetting_dict['timeScale']                = nucleus.timeScale.get() ;
        nucleusSetting_dict['spaceScale']               = nucleus.spaceScale.get() ;

        return [ nClothSetting_dict , nucleusSetting_dict ];

    def getNHair ( self ) :
        # [ GNHIDX ]
        # return nHair, nHairShape , nucleus
        
        shape_list = pm.listRelatives ( self.target , shapes = True ) ;
        
        for each in shape_list :
            print each ;
            if pm.nodeType ( each ) != 'hairSystem' :
                pass ;
            else :
                nHair = self.target ;
                nHairShape = each ;
                nucleus = pm.listConnections ( nHairShape , type = 'nucleus' ) [0] ;
            
                return nHair , nHairShape , nucleus ; 
    
    def _getHairSetting_getAttractionScaleGraph ( self ) :

        nHair_elem = self.getNHair();
        
        nHair = pm.general.PyNode ( nHair_elem[0] ) ;
        nHairShape = pm.general.PyNode ( nHair_elem[1] ) ;
        nucleus = pm.general.PyNode ( nHair_elem[2] ) ;


        attributeSize = nHairShape.attractionScale.get ( size = True ) ;
        attributeDict = {} ;
        
        counter = 0 ;

        for i in range ( 0 , attributeSize ) :
            point = 'point%d' % (i+1) ; 

            floatValue = 
        '''
        def queryAttractionScaleGraph ( hairSystem = '' , *args ) :
                         
                point = 'point' + str ( i + 1 ) ;
                
                floatValue = pm.getAttr ( hairSystem + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_FloatValue' ) ;
                position = pm.getAttr ( hairSystem + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_Position' ) ;
                attractionScale = pm.getAttr ( hairSystem + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_Interp' ) ;
                
                while ( floatValue == 0.0 ) and ( position == 0.0 ) and ( attractionScale == 0 ) :
                    
                    pm.removeMultiInstance ( '%s.attractionScale[%s]' % ( hairSystem , ( i + counter ) ) , b = True ) ;
                    
                    counter += 1 ;
                    
                    floatValue = pm.getAttr ( hairSystem + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_FloatValue' ) ;
                    position = pm.getAttr ( hairSystem + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_Position' ) ;
                    attractionScale = pm.getAttr ( hairSystem + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_Interp' ) ;
                    
                    # kill inifinite loop        
                    if counter == 100 :
                        break ;
                    
                attrDict [ point ] = [ floatValue , position , attractionScale ] ;
                
            return [ attrDict , attrSize ] ;
        '''

    def getNHairSetting ( self ) :
        # [ GHSIDX ]

        nHair_elem = self.getNHair();
        
        nHair = pm.general.PyNode ( nHair_elem[0] ) ;
        nHairShape = pm.general.PyNode ( nHair_elem[1] ) ;
        nucleus = pm.general.PyNode ( nHair_elem[2] ) ;

        nHairSetting_dict = {} ;

        # check if use nucleus 
        if nHairShape.active.get() == 1 :
            
            nHairSetting_dict['simulationMethod']           = nHairShape.simulationMethod.get();
            nHairSetting_dict['displayQuality']             = nHairShape.displayQuality.get();
            nHairSetting_dict['active']                     = nHairShape.active.get();

            # colision #
            nHairSetting_dict['collide']                    = nHairShape.collide.get();
            nHairSetting_dict['selfCollide']                = nHairShape.selfCollide.get();
            nHairSetting_dict['collisionFlag']              = nHairShape.collisionFlag.get();
            nHairSetting_dict['selfCollisionFlag']          = nHairShape.selfCollisionFlag.get();
            nHairSetting_dict['collideStrength']            = nHairShape.collideStrength.get();
            nHairSetting_dict['collisionLayer']             = nHairShape.collisionLayer.get();
            nHairSetting_dict['maxSelfCollisionIterations'] = nHairShape.maxSelfCollisionIterations.get();
            
            nHairSetting_dict['collideWidthOffset']         = nHairShape.collideWidthOffset.get();
            nHairSetting_dict['selfCollideWidthScale']      = nHairShape.selfCollideWidthScale.get();
            
            nHairSetting_dict['solverDisplay']              = nHairShape.solverDisplay.get();
            nHairSetting_dict['displayColor']               = nHairShape.displayColor.get();
            
            nHairSetting_dict['bounce']                     = nHairShape.bounce.get();
            nHairSetting_dict['friction']                   = nHairShape.friction.get();
            nHairSetting_dict['stickiness']                 = nHairShape.stickiness.get();
            nHairSetting_dict['staticCling']                = nHairShape.staticCling.get();

            # dynamic properties
            nHairSetting_dict['stretchResistance']          = nHairShape.stretchResistance.get();
            nHairSetting_dict['compressionResistance']      = nHairShape.compressionResistance.get();
            nHairSetting_dict['bendResistance']             = nHairShape.bendResistance.get();
            nHairSetting_dict['twistResistance']            = nHairShape.twistResistance.get();
            nHairSetting_dict['extraBendLinks']             = nHairShape.extraBendLinks.get();
            nHairSetting_dict['restLengthScale']            = nHairShape.restLengthScale.get();
            nHairSetting_dict['noStretch']                  = nHairShape.noStretch.get();

            # start curve attract
            nHairSetting_dict['startCurveAttract']          = nHairShape.startCurveAttract.get();
            nHairSetting_dict['attractionDamp']             = nHairShape.attractionDamp.get();



        elif nHairShape.active.get() == 0 :
            pass ;

        return nHairSetting_dict ;

selection = pm.ls ( sl = True ) [0] ;
nHair = simSetNode ( target = selection ) ;
nHair._getHairSetting_getAttractionScaleGraph ( ) ;