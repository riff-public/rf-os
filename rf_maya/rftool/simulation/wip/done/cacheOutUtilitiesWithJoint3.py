import pymel.core as pm ;
import maya.cmds as mc ;
import os , re , math ;

################################################################################################################################################
################################################################################################################################################
''' Global Utils '''
################################################################################################################################################
################################################################################################################################################

##################
''' path '''
##################

def getCachePath_cmd ( *args ) :
    # P:/Two_Heroes/scene/film001/q0210/s0080/finalCam/cache/
    selfPath = mc.file ( q = True , exn = True ) ;
    cachePath = selfPath.split ( 'maya' ) [0] + 'cache/' ;
    return cachePath ;

def createDirectory_cmd ( path = '' , *args ) :
    if os.path.exists ( path ) == False :
        os.makedirs ( path ) ;
    else : pass ;

def checkVersion_cmd (
    path                    = '' ,
    name                    = '' ,
    incrementVersion        = True ,
    incrementSubVersion     = False ,
    origin                  = False ,
    *args ) :
    
    if os.path.exists ( path ) == False :
        os.makedirs ( path ) ;
    else : pass ;

    allFile = os.listdir ( path ) ;
    versionList = [] ;
    
    for each in allFile :

        if name in str(each) :
            versionList.append ( each ) ;
        else : pass ;
    
    if len ( versionList ) == 0 :
        version = '001'
        subVersion = '001'

    else :
        versionList.sort ( ) ;

        latestVersion = versionList [-1] ;

        latestVersionSplit = latestVersion.split ( '_' ) ;
        latestVersion = [] ;
        latestVersion.extend ( [ latestVersionSplit[0] , latestVersionSplit[1] ] ) ;

        version = int ( re.findall ( '[0-9]{3}' ,  latestVersion [ -2 ] ) [0] ) ;
        
        subVersion = int ( re.findall ( '[0-9]{3}' , latestVersion [ -1 ] ) [0] ) ;

        if incrementSubVersion == True :
            subVersion += 1 ;
            subVersion = str(subVersion).zfill(3) ;
        else :
            subVersion = str(subVersion).zfill(3) ;
            
        if incrementVersion == True :
            version += 1 ;
            version = str(version).zfill(3) ;
            subVersion = '001' ;
                # overwrite increment subversion
        else :
            version = str(version).zfill(3) ;

        # if this is recache of origin cache
        if latestVersion[1] == '000' :
            version = int(version) ;
            version -= 1 ;
            version = str(version).zfill(3) ;
        else : pass ;

    # if this is origin cache
    if origin == True :
        subVersion = '000' ;
    else : pass ;

    return 'v' + str(version) + '_' + str(subVersion) ;

##################
''' info '''
##################

def queryUITime_cmd ( *args ) :
    # return [ preRoll , start , end , postRoll ] ;
    preRoll     = pm.intField ( 'preRoll_intField'  , q = True , v = True ) ;
    start       = pm.intField ( 'start_intField'    , q = True , v = True ) ;
    end         = pm.intField ( 'end_intField'      , q = True , v = True ) ;
    postRoll    = pm.intField ( 'postRoll_intField' , q = True , v = True ) ;
    return [ preRoll , start , end , postRoll ] ;

def getSceneType_cmd ( *args ) :

    selfPath = mc.file ( q = True , exn = True ) ;

    if '/finalCam/' in str(selfPath) :
        sceneType = 'finalCam' ;
    elif '/finalLayout/' in str(selfPath) :
        sceneType = 'finalLayout' ;
    elif '/anim/' in str(selfPath) :
        sceneType = 'anim' ;

    return sceneType ;

##################
''' general '''
##################

def createGrp_cmd ( name = '' , *args ) :
    if pm.objExists ( name ) == True :
        grp = pm.general.PyNode ( name ) ;
    else :
        grp = pm.group ( em = True , w = True , n = name ) ;
    return grp ;

def createDisplayLayer_cmd ( n = '' , *args ) :
    if pm.objExists ( n ) == True :
        displayLayer = pm.general.PyNode ( n ) ; 
    else :
        displayLayer = pm.createDisplayLayer ( name = n , number = 1 , empty = True ) ;
    return displayLayer ;

def findTopNode_cmd ( node = '' , *args ) : 
    parent = pm.listRelatives ( node , p = True ) ;
    if parent != [] :
        return ( findTopNode_cmd ( node = parent ) ) ;      
    else:
        if isinstance ( node , list ) :
            node = node[0] ;
        return node ;

def bakeKeys_cmd ( target = '' , preRoll = '' , postRoll = '' , hierarchy = 'none' , *args ) :
    # below

    step =  pm.floatField ( 'step_input' , q = True , v = True ) ;

    pm.bakeResults ( target ,
        simulation  = True ,
        hierarchy = hierarchy ,
        time = ( preRoll , postRoll ) ,
        sampleBy = step ,
        disableImplicitControl = True ,
        preserveOutsideKeys = True ,
        sparseAnimCurveBake = False ,
        removeBakedAttributeFromLayer = False ,
        removeBakedAnimFromLayer = False ,
        bakeOnOverrideLayer = False ,
        minimizeRotation = True ,
        controlPoints = False ,
        shape = True ) ;

##################
''' cacheOut '''
##################

def exportABC_cmd ( alembicPath , root , *args ) :

    ### query necessary data ### 
    time = queryUITime_cmd ( ) ;
    preRoll     = time[0] ;
    postRoll    = time[3] ;

    step        =  pm.floatField ( 'step_input' , q = True , v = True ) ;

    ### export abc command ###
    command = 'AbcExport -j ' ; # start command
    command += '"-frameRange %s %s '    % ( str(preRoll) , str(postRoll) ) ;
    command += '-step %s '              % str(step) ;
    #command += '-attr __forSim__ -attr preRoll -attr start -attr end -attr postRoll -attr placementSclX -attr placementSclY -attr placementSclZ -attr headSclX -attr headSclY -attr headSclZ -attr gobalScale' + ' ' ;
    command += '-worldSpace -writeVisibility -eulerFilter -dataFormat ogawa' + ' ' ;
    command += '-root %s '              % root ;
    command += '-file %s " ;'             % alembicPath ;
    
    # print command ;
    pm.mel.eval ( command ) ;
    return ( alembicPath ) ;

def exportCustomABC_cmd ( root , cacheName , directory , origin = False , *args ) :

    facialStat = pm.checkBox ( 'charFacial_checkBox' , q = True , v = True ) ;

    cache_path = getCachePath_cmd ( ) ;
    # P:/Two_Heroes/scene/film001/q0210/s0080/finalCam/cache/
    cache_path += directory + '/' ;
    createDirectory_cmd ( path = cache_path ) ;

    cache_name = cacheName ;

    version = checkVersion_cmd ( 
        path                    = cache_path ,
        name                    = cache_name ,
        incrementVersion        = True ,
        incrementSubVersion     = False ,
        origin                  = origin ,
        ) ;

    cache_name += '.' ;
    cache_name += version ;

    if facialStat == 0 :
        cache_name += '_noFacial' ;
    else : pass ;

    cache_name += '.' ;
    cache_name += 'abc' ;

    alembic_path = cache_path + cache_name ;
    root = mc.ls ( str(root) , l = True ) [0] ;
    alembicPath = exportABC_cmd ( alembicPath = alembic_path , root = root ) ;
    return alembicPath ;

def exportTimerangeTxt_cmd ( *args ) :
    # included in cacheOutBtn_cmd

    cache_path = getCachePath_cmd ( ) ;

    time = queryUITime_cmd ( ) ;
    preRoll     = str(time[0]) ;
    start       = str(time[1]) ;
    end         = str(time[2]) ;
    postRoll    = str(time[3]) ;

    text = "{ 'pre roll': %s , 'start': %s , 'end' : %s , 'post roll' : %s }" % ( preRoll , start , end , postRoll ) ;
    text_path = cache_path + 'timerange.txt' ;
    text_file = open ( text_path , 'w+' ) ;
    text_file.write ( text ) ;
    text_file.close ( ) ;

    progressBar = ProgressBar ( 'progressBar' ) ;
    progressBar.update ( 10 ) ;
    
################################################################################################################################################
################################################################################################################################################
''' camera '''
################################################################################################################################################
################################################################################################################################################

def listAllCam_cmd ( *args ) :
        
    allCam_list = pm.ls ( type = 'camera' ) ;
    
    frontShape  = pm.general.PyNode ( 'frontShape' ) ;
    perspShape  = pm.general.PyNode ( 'perspShape' ) ;
    sideShape   = pm.general.PyNode ( 'sideShape' ) ;
    topShape    = pm.general.PyNode ( 'topShape' ) ;

    defaultCam_list = [ frontShape , perspShape , sideShape , topShape ] ;

    for camShape in defaultCam_list :
        if camShape in allCam_list :
            allCam_list.remove ( camShape ) ;

    return allCam_list ;

def camCacheOut_cmd ( *args ) :
    
    sceneType = getSceneType_cmd ( ) ;

    cam_grp = createGrp_cmd ( name = 'cam_grp' ) ;
    print cam_grp ;

    allCam_list = listAllCam_cmd ( ) ;

    for cam in allCam_list :
        
        cam_parent = findTopNode_cmd ( cam ) ;
        if cam_parent != cam_grp :
            pm.parent ( cam_parent , cam_grp ) ;
        else : pass ;

    exportCustomABC_cmd ( root = cam_grp , cacheName = 'camera' , directory = 'share.cam.' + sceneType ) ;

    progressBar = ProgressBar ( 'progressBar' ) ;
    progressBar.update ( 20 ) ;

################################################################################################################################################
################################################################################################################################################
''' CHARs '''
################################################################################################################################################
################################################################################################################################################

import pymel.core as pm ;
import maya.cmds as mc ;

def getCharacterList_cmd ( *args ) :
    # charType.char.namespace

    referencePath_list = mc.file ( q = True , r = True ) ;
    
    ### get character path ###
    charRefPath_list = [] ;
    
    for path in referencePath_list :
       if ( '/char/' in str(path) ) or ( '/character/' in str(path) ) :
           charRefPath_list.append ( path ) ;
    
    ### make character information ###
    char_list = [] ;
     
    for path in charRefPath_list :

        if '/char/' in str(path) :
            char = '/char/' ;
        elif '/character/' in str(path) :
            char = '/character/' ;
        
        elem = path.split(char)[1];
        elem = elem.split('/');
        
        charType     = elem[0];
        char         = elem[1] ;  
        namespace    = mc.file ( path , q = True , namespace = True ) ;

        char_list.append ( '%s_%s.%s' % ( charType , char , namespace ) ) ;
    
    char_list.sort ( ) ;   
    
    return char_list ;
    
def charListTxtScollUpdate_cmd ( *args ) :
    # included in refreshed_cmd

    pm.textScrollList ( 'characterList_textScroll' , e = True , ra = True ) ;

    char_list = getCharacterList_cmd ( ) ;    

    for char in char_list :
        pm.textScrollList ( 'characterList_textScroll' , e = True , append = char ) ;

def setFacial_cmd ( namespace = '' , enable = True , *args ) :

    facial_grp = pm.general.PyNode ( namespace + ':FclRigStill_Grp' ) ;
    #babyJ002:FclRigStill_Grp

    facialGrpChildren_list = pm.listRelatives ( facial_grp , ad = True , type = 'transform' ) ;

    for child in facialGrpChildren_list :
        
        if str(child)[-4:] == '_Geo' :
        
            bsn = child.split('_')[0] + '_Bsn' ;
            
            try :       
                bsn = pm.general.PyNode ( bsn ) ;
                bsn.envelope.set(enable) ;
            except :
                pass ;

def charCacheOut_cmd ( *args ) :

    sceneType = getSceneType_cmd ( ) ;

    char_list = pm.textScrollList ( 'characterList_textScroll' , q = True , selectItem = True ) ;

    ### progress bar value for each loop ###
    progressBar = ProgressBar ( 'progressBar' ) ;
    value = 100.00 ;
    individualValue = value/len(char_list) ;
    individualValue = math.floor ( individualValue ) ;
    individualValue = int ( individualValue ) ;

    for each in char_list :
        
        element = each.split ( '.' ) ;

        charType    = element[0].split('_')[0] ;
        char        = element[0].split('_')[1] ;
        namespace   = element[1] ;

        facialStat = pm.checkBox ( 'charFacial_checkBox' , q = True , v = True ) ;

        setFacial_cmd ( namespace = namespace , enable = facialStat ) ;

        ### get geoGrp ###

        if pm.objExists ( namespace + ':' + 'Geo_Grp' ) == True :
            geo_grp = pm.general.PyNode ( namespace + ':' + 'Geo_Grp' ) ;
        elif pm.objExists ( namespace + ':' + 'geo_grp' ) == True :
            geo_grp = pm.general.PyNode ( namespace + ':' + 'geo_grp' ) ;
        else :
            pm.error ( "geo_grp , Geo_Grp doesn't exist. Please contact Bird for script update" ) ;

        ### cacheout ###

        cacheName = char + '.' + 'geoGrp'
        
        originStat = pm.checkBox ( 'originCache_checkBox' , q = True , v = True ) ;

        if originStat == False :

            exportCustomABC_cmd ( root = geo_grp , cacheName = cacheName , directory = charType + '_' + char + '.' + namespace + '.' + sceneType ) ;
        
        else : 

            ### get timerange ###
            time        = queryUITime_cmd ( ) ;
            preRoll     = time[0] ;
            postRoll    = time[3] ;
            pm.currentTime ( preRoll , edit = True ) ;

            ### get fly_ctrl ###        
            if pm.objExists ( namespace + ':Place_Ctrl' ) == True :
                fly_ctrl = pm.general.PyNode ( namespace + ':Offset_Ctrl' ) ;
            elif pm.objExists ( namespace + ':master_ctrl' ) == True :
                fly_ctrl = pm.general.PyNode ( namespace + ':master_ctrl' ) ;
            else :
                pm.error ( "master_ctrl , Offset_Ctrl doesn't exist. Please contact Bird for script update" ) ;

            ### bakeKey ###
            bakeKeys_cmd ( target = fly_ctrl , preRoll = preRoll , postRoll = postRoll , hierarchy = 'below' ) ;

            ### snap character to origin and cache out ###
            nullGrp = pm.group ( em = True , w = True , n = namespace + ':null_grp' ) ;
            parCon = pm.parentConstraint ( nullGrp , fly_ctrl , mo = False , weight = 1 ,  skipRotate = 'none' , skipTranslate = 'none' , ) ;

            originCache_path = exportCustomABC_cmd ( root = geo_grp , cacheName = cacheName , directory = charType + '_' + char + '.' + namespace + '.' + sceneType , origin = True ) ;

            ### import origin cache ###
            mc.file ( originCache_path , i = True , type = 'Alembic' , ignoreVersion = True , renameAll = False , mergeNamespacesOnClash = False , renamingPrefix = namespace , loadReferenceDepth = 'none' ) ;

            ### check geo_grp ###
            if pm.objExists ( 'Geo_Grp' ) == True :
                geo_grp = 'Geo_Grp' ;
            elif pm.objExists ( 'geo_grp' ) == True :
                geo_grp = 'geo_grp' ;
            else :
                pm.error ( "geo_grp , Geo_Grp doesn't exist. Please contact Bird for script update" ) ;

            ### snap character back to its rightful location and cache out ###
            pm.parent ( geo_grp , fly_ctrl ) ;

            pm.delete ( parCon ) ;
            pm.delete ( nullGrp ) ;

            exportCustomABC_cmd ( root = geo_grp , cacheName = cacheName , directory = charType + '_' + char + '.' + namespace + '.' + sceneType ) ;
            pm.delete ( geo_grp ) ;

        setFacial_cmd ( namespace = namespace , enable = True ) ;

        ### progress bar update ###
        progressBar.update ( individualValue ) ;

    ### just in case there's leftover for progressbar ###
    if individualValue*len(char_list) == 100 :
        pass ;
    else :
        leftOverValue = value - (individualValue*len(char_list)) ;
        leftOverValue = int ( leftOverValue ) ; 
        progressBar.update ( leftOverValue ) ;

################################################################################################################################################
################################################################################################################################################
''' PROPs '''
################################################################################################################################################
################################################################################################################################################

##################
''' create '''
##################

def createPropShader_cmd ( *args ) :
    if pm.objExists ( 'propABC_shader' ) != True :    
        propShader = pm.shadingNode ( 'lambert' , asShader = True , n = 'propABC_shader' ) ;
    else :
        propShader = pm.general.PyNode ( 'propABC_shader' ) ;
    propShader.color.set( 0  , 1 , 1 );

    return propShader ;

##################
''' util '''
##################

def duplicateBSH_cmd ( target , groupName , *args ) :
    prop_grp = createGrp_cmd ( name = groupName + '_pABC' ) ;   
    duplicatedObj_nm = target.split(':')[-2] + '_' + target.split(':')[-1] ;
    duplicatedObj = pm.duplicate ( target , n = duplicatedObj_nm ) ;
    pm.blendShape ( target , duplicatedObj , origin = 'world' , weight = [ 0 , 1.0 ] , n = 'prop_BSH' ) ;
    pm.parent ( duplicatedObj , prop_grp ) ;
    return duplicatedObj ;

##################
''' main '''
##################

def createPropGrp_cmd ( *args ) :
    # included in createAddPropBtn_cmd

    groupName = pm.textField ( 'propGrp_txtField' , q = True , text = True ) ;
    
    selection_list = pm.ls ( sl = True ) ;

    if len ( selection_list ) < 1 :
        pass ;

    else : 
        ### shader ###
        shader = createPropShader_cmd ( ) ;
        ### layer ###
        selectedProp_layer = createDisplayLayer_cmd ( n = 'selectedProp_layer' ) ;
        
        if selectedProp_layer.visibility.get ( ) == True :
            pm.mel.eval ( 'layerEditorLayerButtonVisibilityChange %s' % selectedProp_layer ) ;
        else :
            pass ;

        duplicatedObj_list = [] ;

        for each in selection_list : 
            
            duplicatedObj = duplicateBSH_cmd ( target = each , groupName = groupName ) ;
            duplicatedObj_list.append ( duplicatedObj ) ;

            pm.select ( duplicatedObj_list ) ;
            pm.hyperShade( assign = shader ) ;

            pm.editDisplayLayerMembers ( selectedProp_layer , each , noRecurse = True ) ;   

def propGrpExists_cmd ( *args ) :
    # included in propSectionUpdate_cmd
    
    ABC_GRP = pm.ls ( '*_pABC' , type = 'transform' ) ;

    if ABC_GRP != [] :
        stat = True ;   
    else :
        stat = False ;

    pm.checkBox ( 'propExport_checkBox' , e = True , label = 'prop export' , v = stat , enable = stat ) ;
    #groupName = pm.textField ( 'propGrp_txtField' , q = True , text = True ) ;
    #status = pm.objExists ( groupName + '_pABC' ) ;

def propListTxtScollUpdate_cmd ( *args ) :
    # included in propSectionUpdate_cmd
    pm.textScrollList ( 'propList_textScroll' , e = True , ra = True ) ;

    ABC_GRP = pm.ls ( '*_pABC' , type = 'transform' ) ;
    ABC_GRP.sort ( ) ;
    
    for group in ABC_GRP :
        pm.textScrollList ( 'propList_textScroll' , e = True , append = group ) ;

def createAddPropBtn_cmd ( *args ) :
    createPropGrp_cmd ( ) ;
    refresh_cmd ( ) ;

def propSectionUpdate_cmd ( *args ) :
    # included in refreshed_cmd
    propGrpExists_cmd ( ) ;
    propListTxtScollUpdate_cmd ( ) ;

def propCacheOut_cmd ( *args ) :
    # included in cacheOutBtn_cmd

    sceneType = getSceneType_cmd ( ) ;

    pGrp_list = pm.textScrollList ( 'propList_textScroll' , q = True , selectItem = True ) ;

    ### progress bar value for each loop ###
    progressBar = ProgressBar ( 'progressBar' ) ;
    value = 100.00 ;
    individualValue = value/len(pGrp_list) ;
    individualValue = math.floor ( individualValue ) ;
    individualValue = int ( individualValue ) ;
   
    for pGrp in pGrp_list :
        cache_name = pGrp.split('_')[0] ;
        exportCustomABC_cmd ( root = pGrp , cacheName = cache_name , directory = 'share.prop.' + sceneType ) ;
        ### progress bar update ###
        progressBar.update ( individualValue ) ;

    ### just in case there's leftover for progressbar ###
    if individualValue*len(pGrp_list) == 100 :
        pass ;
    else :
        leftOverValue = value - (individualValue*len(pGrp_list)) ;
        leftOverValue = int ( leftOverValue ) ; 
        progressBar.update ( leftOverValue ) ;

################################################################################################################################################
################################################################################################################################################
''' global '''
################################################################################################################################################
################################################################################################################################################

def refresh_cmd ( *args ) :

    charListTxtScollUpdate_cmd ( ) ;
    propSectionUpdate_cmd ( ) ;
    updateTimeRange_cmd ( ) ;

    progressBar = ProgressBar ( 'progressBar' ) ;
    progressBar.delete() ;
    progressBar.insert ( parent = 'progressBar_layout' ) ;

def cacheOutBtn_cmd ( *args ) :

    cachePath = getCachePath_cmd ( ) ;
    createDirectory_cmd ( cachePath ) ;

    progressBar = ProgressBar ( 'progressBar' ) ;
    progressBar.delete() ;
    progressBar.insert ( parent = 'progressBar_layout' ) ;

    #################################
    ''' set progressBar max value '''
    #################################

    progressBarMax_value = 10 ;

    if pm.checkBox ( 'cameraCache_checkBox' , q = True , v = True ) == True :
        progressBarMax_value += 20 ;

    if pm.checkBox ( 'charExport_checkBox' , q = True , v = True ) == True :
        if len ( pm.textScrollList ( 'characterList_textScroll' , q = True , selectItem = True ) ) != 0 :
            progressBarMax_value += 100 ;
        else : pass ;
    else : pass ;

    if ( pm.checkBox ( 'propExport_checkBox' , q = True , v = True ) == True ) :
        if len ( pm.textScrollList ( 'propList_textScroll' , q = True , selectItem = True ) ) != 0 :
            progressBarMax_value += 100 ;
        else : pass ;
    else : pass ;

    progressBar.minMax ( min = 1 , max = progressBarMax_value ) ;

    #################################
    ''' check and export elements '''
    #################################
    
    exportTimerangeTxt_cmd ( ) ;

    if pm.checkBox ( 'cameraCache_checkBox' , q = True , v = True ) == True :
        camCacheOut_cmd ( ) ;
    else : pass ;

    if pm.checkBox ( 'charExport_checkBox' , q = True , v = True ) == True :
        if len ( pm.textScrollList ( 'characterList_textScroll' , q = True , selectItem = True ) ) != 0 :
            charCacheOut_cmd ( ) ;
        else : pass ;
    else : pass ;

    if ( pm.checkBox ( 'propExport_checkBox' , q = True , v = True ) == True ) :
        if len ( pm.textScrollList ( 'propList_textScroll' , q = True , selectItem = True ) ) != 0 :
            propCacheOut_cmd ( ) ;
        else : pass ;
    else : pass ;
    
################################################################################################################################################
################################################################################################################################################
''' UI '''
################################################################################################################################################
################################################################################################################################################

class ProgressBar ( object ) :
    # progressBar_layout
    
    def __init__ ( self , progressBar = '' , ) :
        self.progressBar = progressBar ;
        self.progressBar = progressBar ;

    def delete ( self ) :
        mc.deleteUI ( self.progressBar ) ;

    def insert ( self , parent ) :
        self.layout = parent ;
        mc.progressBar ( self.progressBar , p = self.layout , width = width ) ;

    def minMax ( self , min , max ) :
        self.min = min ;
        self.max = max ;
        mc.progressBar ( self.progressBar , e = True , min = self.min , max = self.max ) ;
    
    def update ( self , value ) :
        beforeValue = mc.progressBar ( self.progressBar , q = True , pr = True ) ;
        finalValue = value + beforeValue ;
        mc.progressBar ( self.progressBar , e = True , pr = finalValue ) ;
        pm.refresh();

def cacheDirectoryBtn_cmd ( *args ) :
    import os ;
    path = getCachePath_cmd ( ) ;
    os.startfile ( path ) ;

def findStartEnd_cmd ( *args ) :
    # return [ preRoll , start , end , postRoll ] ;
    start       = pm.playbackOptions ( q = True , min = True ) ;
    end         = pm.playbackOptions ( q = True , max = True ) ;
    preRoll     = start - 30 ;
    postRoll    = end + 10 ;
    return [ preRoll , start , end , postRoll ] ;

def updateTimeRange_cmd ( *args ) :
    # included in refresh_cmd
    
    time = findStartEnd_cmd ( ) ;
    pm.intField ( 'preRoll_intField'    , e = True , v = time [0] ) ;
    pm.intField ( 'start_intField'      , e = True , v = time [1] ) ;
    pm.intField ( 'end_intField'        , e = True , v = time [2] ) ;
    pm.intField ( 'postRoll_intField'   , e = True , v = time [3] ) ;

width = 450.00 ;
title = 'cache Out Util v4.3' ;

help_txt = '''<ORIGIN option>
Turn this option on when the character is too far from the origin.

This script will :

1. cache the object out from the origin (0 , 0 , 0)

2. import the cache back into the scene

3. position the cache back to it's original position

4. cache the object out again

<JOINT INFO option>
Turn this option on when you want to simulate with pMuscle setup

<NOTE TO USER>
Since this project's asset is vaguely organized, this script is heavily hard coded and inflexible. Should the problem arised please contact Bird of simulation department

in case of emergency:
( facebook )    Bird Weerapot
( line )        birdiesama'''

def separator ( *args ) :
    pm.separator ( h = 5 , vis = False ) ;

def filler ( times = 1 , *args ) :
    for i in range ( 0 , int ( times ) ) :
        pm.text ( label = '' ) ;

def run ( *args ) :

    if pm.window ( 'animCacheOut_UI' , exists = True ) :
        pm.deleteUI ( 'animCacheOut_UI' ) ;
    else : pass ;

    window = pm.window ( 'animCacheOut_UI', title = title , mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    pm.window ( 'animCacheOut_UI' , e = True , w = width , h = 10 ) ;

    main_layout = pm.rowColumnLayout ( p = window , nc = 1 ) ;
    with main_layout :

        #########
        ''' TAB '''
        #########
        with pm.tabLayout ( innerMarginWidth = 5 , innerMarginHeight = 5 , w = width ) :

            #########
            ''' MAIN TAB '''
            #########
            with pm.rowColumnLayout ( 'main' , nc = 1 , w = width ) :

                pm.button ( label = 'cache directory' , c = cacheDirectoryBtn_cmd ) ;
                    
                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                    #########
                    ''' CHARACTER '''
                    #########
                    with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :

                        with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/2/4 ) , ( 2 , width/2/2 ) , ( 3 , width/2/4 )] ) :
                            filler ( ) ;
                            pm.checkBox ( 'charExport_checkBox' , label = 'char export' , v = True , enable = True , h = 20 ) ;
                            filler ( ) ;
                        
                        pm.textScrollList ( 'characterList_textScroll' , w = width/2 , h = 150 , ams = True ) ;

                        with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/2/4 ) , ( 2 , width/2/2 ) , ( 3 , width/2/4 )] ) :
                            filler ( ) ;
                            pm.checkBox ( 'charFacial_checkBox' , label = 'facial' , v = True , enable = True , h = 20 ) ;
                            filler ( ) ;

                    #########
                    ''' PROP '''
                    #########
                    with pm.rowColumnLayout ( nc = 1, w = width/2 ) :
 
                        with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/2/4 ) , ( 2 , width/2/2 ) , ( 3 , width/2/4 )] ) :
                            filler ( ) ;
                            pm.checkBox ( 'propExport_checkBox' , label = 'prop export' , v = False , enable = False , h = 20 ) ;
                            filler ( ) ;

                        pm.textScrollList ( 'propList_textScroll' , w = width/2 , h = 150 , ams = True ) ;

                        pm.text ( label = "props' name" ) ;
                        pm.textField ( 'propGrp_txtField' , text = 'prop' ) ;

                        pm.button ( label = 'create/add prop' , c = createAddPropBtn_cmd ) ;

                #########
                ''' TIME RANGE LAYOUT '''
                #########
                with pm.rowColumnLayout ( nc = 4 ,  columnWidth = [ ( 1 , width/4 ) , ( 2 , width/4 ) , ( 3 , width/4 ) , ( 4 , width/4 ) ] ) :

                    pm.intField ( 'preRoll_intField' ) ;
                    pm.intField ( 'start_intField' ) ;
                    pm.intField ( 'end_intField' ) ;
                    pm.intField ( 'postRoll_intField' ) ;

                    pm.text ( label = 'pre roll -30' , bgc = [ 0.45 , 0.75 , 0.45 ] ) ;
                    pm.text ( label = 'start' , bgc = [ 1 , 1 , 0 ] ) ;
                    pm.text ( label = 'end' , bgc = [ 1 , 1 , 0 ] ) ;
                    pm.text ( label = 'post roll +10' , bgc = [ 0.45 , 0.75 , 0.45 ] ) ;

                separator ( ) ;

                #########
                ''' REFRESH BUTTON '''
                #########
                with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/4 ) , ( 2 , width/2 ) , ( 3 , width/4 ) ] ) :

                    filler ( ) ;
                    pm.button ( label = 'refresh' , c = refresh_cmd , bgc = (  0.65 , 0.85 , 0.9 ) ) ;

                separator ( ) ;

                #########
                ''' OPTIONS '''
                #########
                with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , width/4 ) , ( 2 , width/4 ) , ( 3 , width/4 ) , ( 4 , width/4 ) ] ) :

                    pm.text ( label = 'step' ) ;
                    filler (3) ;

                    with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/4/5 ) , ( 2 , width/4/5*3 ) , ( 3 , width/4/5 )] ) :
                        filler ( ) ;
                        pm.floatField ( 'step_input' , ann = 'step' , precision = 2 , v = 1 , w = width/8 ) ;
                        filler ( ) ;

                    pm.checkBox ( 'cameraCache_checkBox' , label = 'camera' , v = True ) ;
                    pm.checkBox ( 'jointInfo_checkBox' , label = 'joint info' , v = False , enable = False ) ;
                    pm.checkBox ( 'originCache_checkBox' , label = 'origin' , v = False ) ;

                separator ( ) ;

                #########
                ''' CACHE OUT BUTTON '''
                #########

                pm.text ( label = "In viewport, please don't forget to Show > None" , bgc = ( 1 , 0.4 , 0.7 ) ) ;
                
                pm.button ( label = 'cache out' , c = cacheOutBtn_cmd ) ;

                with pm.rowColumnLayout ( 'progressBar_layout' , nc = 1 , w = width ) :

                    progressBar = ProgressBar ( 'progressBar' ) ;
                    progressBar.insert ( parent = 'progressBar_layout' ) ;

            #########
            ''' HELP TAB '''
            #########
            with pm.rowColumnLayout ( 'help' ,  nc = 1 , w = width ) :

                print pm.window ( 'animCacheOut_UI' , q = True , h = True ) ;
                        
                help_scrollField = pm.scrollField ( wordWrap = True , editable = True , width = width , height = 425 , text = help_txt ) ;

    refresh_cmd ( ) ;
    #updateTSL_cmd ( ) ;
    #updateTimeRange_cmd ( ) ;
    
    window.show () ;