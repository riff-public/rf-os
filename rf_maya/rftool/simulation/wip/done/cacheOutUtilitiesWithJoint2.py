import pymel.core as pm ;
import maya.cmds as mc ;
import os , re ;
       
##################
##################
''' path '''
##################
##################   

def getCachePath_cmd ( *args ) :
    selfPath = mc.file ( q = True , exn = True ) ;
    cachePath = selfPath.split ( 'maya' ) [0] + 'cache/' ;
    return cachePath ;

def checkVersion_cmd ( directory = '' , name = '' , incrementVersion = True , incrementSubVersion = False , origin = False , *args ) :
    
    if os.path.exists ( directory ) == False :
        os.makedirs ( directory ) ;
    else : pass ;

    allFile = os.listdir ( directory ) ;
    versionList = [] ;
    
    for each in allFile :
        if each.split('.')[0] == name :
            versionList.append ( each ) ;
        else : pass ;
    
    if len ( versionList ) == 0 :
        version = '001'
        subVersion = '001'

    else :
        versionList.sort ( ) ;
        latestVersion = versionList [-1] ;
        latestVersion = latestVersion.split ( '_' ) ;
        version = int ( re.findall ( '[0-9]{3}' ,  latestVersion [ -2 ] ) [0] ) ;
        subVersion = int ( re.findall ( '[0-9]{3}' , latestVersion [ -1 ] ) [0] ) ;

        if incrementSubVersion == True :
            subVersion += 1 ;
            subVersion = str(subVersion).zfill(3) ;
        else :
            subVersion = str(subVersion).zfill(3) ;
            
        if incrementVersion == True :
            version += 1 ;
            version = str(version).zfill ( 3 ) ;
            subVersion = '001' ;
                # overwrite increment subversion
        else :
            version = str(version).zfill ( 3 ) ;

    if origin == True :
        subVersion = '000' ;
    else : pass ;

    return str(version) + '_' + str(subVersion) ;

##################
##################
''' general utilities '''
##################
##################

class Info ( object ) :

    def __init__ ( self , object ) :
        self.object = object ;

    def root ( self ) :
        # find the root of target

        root = mc.ls ( self.object , l = True ) [0]
        root = root.split ( "|" ) [1] ;

        return root ;

##################
##################
''' export geometry commands '''
##################
##################

def queryUITime_cmd ( ) :
    # return [ preRoll , start , end , postRoll ] ;
    preRoll = pm.intField ( 'preRoll_intField' , q = True , v = True ) ;
    start = pm.intField ( 'start_intField' , q = True , v = True ) ;
    end = pm.intField ( 'end_intField' , q = True , v = True ) ;
    postRoll = pm.intField ( 'postRoll_intField' , q = True , v = True ) ;
    return [ preRoll , start , end , postRoll ] ;

def imprintData_cmd ( namespace = '' , *args ) :

    try : target = pm.general.PyNode( namespace + ':' + 'Geo_Grp' ) ;
    except : target = pm.general.PyNode( namespace + ':' + 'geo_grp' ) ;
    else : pass ;
    
    time = queryUITime_cmd ( ) ;
    preRoll = time [0] ;
    start = time [1] ;
    end = time [2] ;
    postRoll = time [3] ;
    
    target.addAttr ( '__forSim__' , keyable = True , attributeType = 'float' ) ; target.__forSim__.lock ( ) ;
    target.addAttr ( 'preRoll' , keyable = True , attributeType = 'float' , dv = preRoll ) ; target.preRoll.lock ( ) ;
    target.addAttr ( 'start' , keyable = True , attributeType = 'float' , dv = start ) ; target.start.lock ( ) ;
    target.addAttr ( 'end' , keyable = True , attributeType = 'float' , dv = end ) ; target.end.lock ( ) ;    
    target.addAttr ( 'postRoll' , keyable = True , attributeType = 'float' , dv = postRoll ) ; target.postRoll.lock ( ) ;
    
    try :
        placement_ctrl = pm.general.PyNode ( namespace + ':' + 'Place_Ctrl' ) ;
        placementScl = placement_ctrl.s.get ( ) ;
        target.addAttr ( 'placementSclX' , keyable = True , attributeType = 'float' , dv = placementScl [0] ) ; target.placementSclX.lock ( ) ;
        target.addAttr ( 'placementSclY' , keyable = True , attributeType = 'float' , dv = placementScl [1] ) ; target.placementSclY.lock ( ) ;
        target.addAttr ( 'placementSclZ' , keyable = True , attributeType = 'float' , dv = placementScl [2] ) ; target.placementSclZ.lock ( ) ;
    except : pass ;

    try :
        placement_ctrl = pm.general.PyNode ( namespace + ':' + 'master_ctrl' ) ;
        placementScl = placement_ctrl.globalScale.get ( ) ;
        target.addAttr ( 'globalScale' , keyable = True , attributeType = 'float' , dv = placementScl ) ; target.globalScale.lock ( ) ;
    except : pass ;

    try :
        head_ctrl = pm.general.PyNode ( namespace + ':' + 'Head_Ctrl' ) ;
        headScl = head_ctrl.s.get ( ) ;
        target.addAttr ( 'headSclX' , keyable = True , attributeType = 'float' , dv = headScl [0] ) ; target.headSclX.lock ( ) ;
        target.addAttr ( 'headSclY' , keyable = True , attributeType = 'float' , dv = headScl [1] ) ; target.headSclY.lock ( ) ;
        target.addAttr ( 'headSclZ' , keyable = True , attributeType = 'float' , dv = headScl [2] ) ; target.headSclZ.lock ( ) ;
    except : pass ;

def removeData_cmd ( namespace = '' , *args ) :

    try : target = pm.general.PyNode( namespace + ':' + 'Geo_Grp' ) ;
    except : target = pm.general.PyNode( namespace + ':' + 'geo_grp' ) ;
    else : pass ;
    
    target.__forSim__.unlock ( ) ; target.deleteAttr ( '__forSim__' ) ;
    target.preRoll.unlock ( ) ; target.deleteAttr ( 'preRoll' ) ;
    target.start.unlock ( ) ; target.deleteAttr ( 'start' ) ;
    target.end.unlock ( ) ; target.deleteAttr ( 'end' ) ;
    target.postRoll.unlock ( ) ; target.deleteAttr ( 'postRoll' ) ;
    
    try :
        target.placementSclX.unlock ( ) ; target.deleteAttr ( 'placementSclX' ) ; 
        target.placementSclY.unlock ( ) ; target.deleteAttr ( 'placementSclY' ) ;
        target.placementSclZ.unlock ( ) ; target.deleteAttr ( 'placementSclZ' ) ; 
    except :    
        target.globalScale.unlock ( ) ; target.deleteAttr ( 'globalScale' ) ; 
    else : pass ;
    
    try :
        target.headSclX.unlock ( ) ; target.deleteAttr ( 'headSclX' ) ;
        target.headSclY.unlock ( ) ; target.deleteAttr ( 'headSclY' ) ;
        target.headSclZ.unlock ( ) ; target.deleteAttr ( 'headSclZ' ) ;
    except : pass ;
    else : pass ;

def bakeKeys_cmd ( target = '' , preRoll = '' , postRoll = '' , hierarchy = 'none' , *args ) :
    # below

    pm.bakeResults ( target ,
        simulation  = True ,
        hierarchy = hierarchy ,
        time = ( preRoll , postRoll ) ,
        sampleBy = 1 ,
        #oversamplingRate = 1 ,
        disableImplicitControl = True ,
        preserveOutsideKeys = True ,
        sparseAnimCurveBake = False ,
        removeBakedAttributeFromLayer = False ,
        removeBakedAnimFromLayer = False ,
        bakeOnOverrideLayer = False ,
        minimizeRotation = True ,
        controlPoints = False ,
        shape = True ) ;

def exportCache_cmd ( namespace = '' , useNamespace = True , origin = False , incrementVersion = True , incrementSubVersion = False , *args ) :
    
    time = queryUITime_cmd ( ) ;
    preRoll = time [0] ;
    start = time [1] ;
    end = time [2] ;
    postRoll = time [3] ;
    
    if useNamespace == True :
        if pm.objExists ( namespace + ':' + 'Geo_Grp' ) == True :
            geoGrpPath = mc.ls ( namespace + ':' + 'Geo_Grp' , l = True ) ;
            asset = 'Geo_Grp' ;
        elif pm.objExists ( namespace + ':' + 'geo_grp' ) == True :
            geoGrpPath = mc.ls ( namespace + ':' + 'geo_grp' , l = True ) ;        
            asset = 'geo_grp' ;
        else :
            pm.error ( "geo_grp , Geo_Grp doesn't exist. Please contact Bird for script update" ) ;

    else :
        if pm.objExists ( 'Geo_Grp' ) == True :
            geoGrpPath = mc.ls ( 'Geo_Grp' , l = True ) ;
            asset = 'Geo_Grp' ;
        elif pm.objExists ( 'geo_grp' ) == True :
            geoGrpPath = mc.ls ( 'geo_grp' , l = True ) ;        
            asset = 'geo_grp' ;
        else :
            pm.error ( "geo_grp , Geo_Grp doesn't exist. Please contact Bird for script update" ) ;

    cachePath = getCachePath_cmd ( ) ;
    cachePath += namespace ;
    
    #print cachePath ;
    
    version = checkVersion_cmd ( directory = cachePath , name = namespace , incrementVersion = incrementVersion , incrementSubVersion = incrementSubVersion , origin = origin , *args ) ;
    
    cacheName = namespace + '.' + asset + '.' + 'v' + version + '.abc' ;
    
    step = pm.floatField ( 'step_input' , q = True , v = True ) ;

# AbcExport -j "-frameRange 71 120 -uvWrite -worldSpace -dataFormat ogawa -root |head_LOC|SunnyCasualSuit_COL|Geo_Grp_COL|SunnyCasualSuitSimGeo_Grp_COL|ShirtSim_Geo_COL -file D:/Dropbox/doubleMonkey/TwoHeroes/film001/q0300/s0390/sunnyCasual/cache/alembic/toDelete/test.abc";

    command = 'AbcExport -j' + ' ' ; # start command
    command += '"-frameRange' + ' ' + str(preRoll) + ' '  + str(postRoll) + ' ' ;
    command += '-step %s' % ( step ) + ' ' ;
    #command = '-frame' + ' ' + str(preRoll) + ' ' + str(postRoll) + ' ' ; 
    command += '-attr __forSim__ -attr preRoll -attr start -attr end -attr postRoll -attr placementSclX -attr placementSclY -attr placementSclZ -attr headSclX -attr headSclY -attr headSclZ -attr gobalScale' + ' ' ;
    command += '-worldSpace -writeVisibility -eulerFilter -dataFormat ogawa' + ' ' ;
    command += '-root' + ' ' + geoGrpPath[0] + ' ' ;
    command += '-file' + ' ' +  cachePath + '/' + cacheName + '"' ;
    
    #print command ;
    pm.mel.eval ( str ( command ) ) ;

    cacheFullPath = cachePath + '/' + cacheName ;
    return ( cacheFullPath ) ;

##################
##################
''' export camera commands '''
##################
##################

def listCamera_cmd ( *args ) :

    defaultCams = [ 'frontShape' , 'perspShape' , 'sideShape' , 'topShape' ] ;

    allCam = mc.ls ( type = 'camera' ) ;
    cam = [] ;

    for each in allCam :
        if each not in defaultCams :
            cam.append ( each ) ;

    return cam ;

def exportCamera_cmd ( *args ) :

    allCam = listCamera_cmd ( ) ;
    cam = [] ;


    for each in allCam :

        camInfo = Info ( each ) ;
        cam.append ( camInfo.root ( ) ) ;

    cam = sorted ( set ( cam ) ) ;

    cacheFullPathList = [] ;

    for each in cam :

        namespace = each ;
        asset = 'camera'
        camPath = mc.ls ( each , l = True ) ;

        time = queryUITime_cmd ( ) ;
        preRoll = time [0] ;
        start = time [1] ;
        end = time [2] ;
        postRoll = time [3] ;
        
        cachePath = getCachePath_cmd ( ) ;
        cachePath += 'camera' ;
        
        version = checkVersion_cmd ( directory = cachePath , name = namespace , incrementVersion = True , incrementSubVersion = False ) ;
        
        cacheName = namespace + '.' + asset + '.' + 'v' + version + '.abc' ;
        print cacheName ;
        
        command = 'AbcExport -j' + ' ' ; 
        command += '"-frameRange' + ' ' + str(preRoll) + ' '  + str(postRoll) + ' ' ;
        command += '-worldSpace -writeVisibility -eulerFilter -dataFormat ogawa' + ' ' ;
        command += '-root' + ' ' + camPath[0] + ' ' ;
        command += '-file' + ' ' +  cachePath + '/' + cacheName + '"' ;
        
        #bakeKeys_cmd ( target = each , preRoll = preRoll , postRoll = postRoll , hierarchy = 'below' ) ;        

        pm.mel.eval ( str ( command ) ) ;

        cacheFullPath = cachePath + '/' + cacheName ;
        cacheFullPathList.append ( cacheFullPath ) ;
    
    return cacheFullPathList ;

##################
##################
''' export joint info '''
##################
##################

def makeName_cmd ( target = '' , *args ) :
    targetSplit_list = target.split ( '_' ) ;
    name = targetSplit_list[0] ;
    for each in targetSplit_list[1:] :
        name += each.capitalize ( ) ;
    return name ;
    
def nodeConstraint_cmd ( master , slave , *args ) :
    # return mulMatrix_node , decomposeMatrix_node

    master_tfm = pm.general.PyNode ( master ) ;
    slave_tfm = pm.general.PyNode ( slave ) ;

    master_nm = makeName_cmd ( target = master_tfm ) ;
    slave_nm = makeName_cmd ( target = slave_tfm ) ;

    mulMatrix_node = pm.createNode ( 'multMatrix' , n = master_nm + 'NCon_mmt' ) ;
    decomposeMatrix_node = pm.createNode ( 'decomposeMatrix' , n = master_nm + 'NCon_dcm' ) ;

    master_tfm.worldMatrix >> mulMatrix_node.matrixIn[0] ;
    slave.parentInverseMatrix >> mulMatrix_node.matrixIn[1] ;

    mulMatrix_node.matrixSum >> decomposeMatrix_node.inputMatrix ;

    decomposeMatrix_node.outputTranslate >> slave.translate ;
    decomposeMatrix_node.outputRotate >> slave.rotate ;
    decomposeMatrix_node.outputScale >> slave.scale ;

    return ( mulMatrix_node , decomposeMatrix_node ) ;


def imprintData_jointInfo_cmd ( target = 'Skin_Grp' , namespace = '' , *args ) :

    pass ;

'''
def imprintData_cmd ( namespace = '' , *args ) :

    try : target = pm.general.PyNode( namespace + ':' + 'Geo_Grp' ) ;
    except : target = pm.general.PyNode( namespace + ':' + 'geo_grp' ) ;
    else : pass ;
    
    time = queryUITime_cmd ( ) ;
    preRoll = time [0] ;
    start = time [1] ;
    end = time [2] ;
    postRoll = time [3] ;
    
    target.addAttr ( '__forSim__' , keyable = True , attributeType = 'float' ) ; target.__forSim__.lock ( ) ;
    target.addAttr ( 'preRoll' , keyable = True , attributeType = 'float' , dv = preRoll ) ; target.preRoll.lock ( ) ;
    target.addAttr ( 'start' , keyable = True , attributeType = 'float' , dv = start ) ; target.start.lock ( ) ;
    target.addAttr ( 'end' , keyable = True , attributeType = 'float' , dv = end ) ; target.end.lock ( ) ;    
    target.addAttr ( 'postRoll' , keyable = True , attributeType = 'float' , dv = postRoll ) ; target.postRoll.lock ( ) ;
    
    try :
        placement_ctrl = pm.general.PyNode ( namespace + ':' + 'Place_Ctrl' ) ;
        placementScl = placement_ctrl.s.get ( ) ;
        target.addAttr ( 'placementSclX' , keyable = True , attributeType = 'float' , dv = placementScl [0] ) ; target.placementSclX.lock ( ) ;
        target.addAttr ( 'placementSclY' , keyable = True , attributeType = 'float' , dv = placementScl [1] ) ; target.placementSclY.lock ( ) ;
        target.addAttr ( 'placementSclZ' , keyable = True , attributeType = 'float' , dv = placementScl [2] ) ; target.placementSclZ.lock ( ) ;
    except : pass ;

    try :
        placement_ctrl = pm.general.PyNode ( namespace + ':' + 'master_ctrl' ) ;
        placementScl = placement_ctrl.globalScale.get ( ) ;
        target.addAttr ( 'globalScale' , keyable = True , attributeType = 'float' , dv = placementScl ) ; target.globalScale.lock ( ) ;
    except : pass ;

    try :
        head_ctrl = pm.general.PyNode ( namespace + ':' + 'Head_Ctrl' ) ;
        headScl = head_ctrl.s.get ( ) ;
        target.addAttr ( 'headSclX' , keyable = True , attributeType = 'float' , dv = headScl [0] ) ; target.headSclX.lock ( ) ;
        target.addAttr ( 'headSclY' , keyable = True , attributeType = 'float' , dv = headScl [1] ) ; target.headSclY.lock ( ) ;
        target.addAttr ( 'headSclZ' , keyable = True , attributeType = 'float' , dv = headScl [2] ) ; target.headSclZ.lock ( ) ;
    except : pass ;
'''

def makeJointCOUT_cmd ( target , namespace ) :
    
    print target ;
    print namespace ;

    '''
    target = pm.general.PyNode ( namespace + target ) ;
    target_COUT = pm.group ( em = True , n = target + '_COUT' ) ;
      
    nodeConstraint_cmd ( master =  namespace + 'Offset_Ctrl' , slave = target_COUT ) ;
    target.rotateOrder >> target_COUT.rotateOrder ;
    
    item_list = pm.listRelatives ( target , ad = True ) ;
    #item_list.sort( ) ;
    
    target_list = [] ;
    
    for item in item_list :
        if ( '_Grp' or '_Jnt' in str(item) ) and ( 'Constraint' not in str(item) ) :
            print target_list.append ( item ) ;
        else : pass ;
            
    #target_list.sort( );
            
    for target in target_list :
        if '_Grp' in str(target) :
            pm.group ( em = True , w = True , n = target + '_COUT' ) ;
        elif '_Jnt' in str(target) :
            pm.createNode ( 'joint' , n = target + '_COUT' ) ;
        else : pass ;
            
    for target in target_list :
        
       parent = pm.listRelatives ( target , parent = True ) ;
       if parent != [] :
           parent = parent[0] ;
           pm.parent ( target + '_COUT' , parent + '_COUT' ) ;
       else : pass ;
       
       target_COUT = pm.general.PyNode ( target + '_COUT' ) ;
       
       if '_Grp' in str(target) :
           
           target.t >> target_COUT.t ;
           target.r >> target_COUT.r ;
           target.s >> target_COUT.s ;
           target.rotateOrder >> target_COUT.rotateOrder ;
           
       elif '_Jnt' in str(target) :
           
           target.t >> target_COUT.t ;
           target.r >> target_COUT.r ;
           target.s >> target_COUT.s ;
           target.rotateOrder >> target_COUT.rotateOrder ;
           target.jointOrient >> target_COUT.jointOrient ;

    return namespace + target ;
    '''
           
def exportJointInfo_cmd ( *args ) :

    target = 'Skin_Grp' ;
    
    '''
    namespace = 
    
    print target ;

    if namespace == '' :
        pass ;
    else :
        namespace += ':' ;

    print namespace ;
    '''

##################
##################
''' button command '''
##################
##################

def exportABC_cmd ( *args ) :

    cacheList = pm.textScrollList ( 'characterList_textScroll' , q = True , si = True ) ;    
    cacheList.extend ( pm.textScrollList ( 'propList_textScroll' , q = True , si = True ) ) ;

    ###

    if pm.checkBox ( 'cameraCache_checkBox' , q = True , v = True ) == True :
        exportCamera_cmd ( ) ;
    else : pass ;

    ###

    if cacheList == [] :
        pass ;
    else :

        if pm.checkBox ( 'originCache_checkBox' , q = True , v = True ) == False :
            # no origin
            
            for each in cacheList :

                imprintData_cmd ( namespace = each ) ;
                exportCache_cmd ( namespace = each ) ;
                removeData_cmd ( namespace = each ) ;

        else :
            # origin
            time = queryUITime_cmd ( ) ;
            preRoll = time [0] ;
            postRoll = time [3] ;
            pm.currentTime ( preRoll , edit = True ) ;

            for each in cacheList :

                imprintData_cmd ( namespace = each ) ;
                
                if pm.objExists ( each + ':' + 'Place_Ctrl' ) == True :
                    flyCtrl = pm.general.PyNode ( each + ':' + 'Offset_Ctrl' ) ;
                elif pm.objExists ( each + ':' + 'master_ctrl' ) == True :
                    flyCtrl = pm.general.PyNode ( each + ':' + 'master_ctrl' ) ;
                else :
                    pm.error ( "master_ctrl , Offset_Ctrl doesn't exist. Please contact Bird for script update" ) ;

                bakeKeys_cmd ( target = flyCtrl , preRoll = preRoll , postRoll = postRoll , hierarchy = 'below' ) ;

                nullGrp = pm.general.PyNode ( pm.group ( em = True , w = True , n = each + ':' + 'null_grp' ) ) ;

                # ( don't forget to delete animation key before parent constraint )
                parCon = pm.parentConstraint ( nullGrp , flyCtrl , mo = False , weight = 1 , 
                    skipRotate = 'none' ,
                    skipTranslate = 'none' , ) ;

                originCachePath = exportCache_cmd ( namespace = each , origin = True ) ;

                mc.file ( originCachePath , i = True , type = 'Alembic' , ignoreVersion = True , renameAll = False , mergeNamespacesOnClash = False ,
                    renamingPrefix = each , loadReferenceDepth = 'none' ) ;

                if pm.objExists ( 'Geo_Grp' ) == True :
                    geoGrp = 'Geo_Grp' ;
                elif pm.objExists ( 'geo_grp' ) == True :
                    geoGrp = 'geo_grp' ;
                else :
                    pm.error ( "geo_grp , Geo_Grp doesn't exist. Please contact Bird for script update" ) ;

                pm.parent ( geoGrp , flyCtrl ) ;

                pm.delete ( parCon ) ;

                pm.delete ( nullGrp ) ;

                exportCache_cmd ( namespace = each , useNamespace = False , incrementVersion = False , incrementSubVersion = True ) ;

                pm.delete ( geoGrp ) ;

                removeData_cmd ( namespace = each ) ;    

##################
##################
''' update '''
##################
##################

def findStartEnd_cmd ( *args ) :
    # return [ preRoll , start , end , postRoll ] ;
    start = pm.playbackOptions ( q = True , min = True ) ;
    end = pm.playbackOptions ( q = True , max = True ) ;
    preRoll = start - 30 ;
    postRoll = end + 30 ;
    return [ preRoll , start , end , postRoll ] ;

def getRefNS_cmd ( *args ) :
        # return character and prop ( prop/vehicle/weapon ) namespace
    
    references = mc.file ( q = True , r = True ) ;
    charRefList = [] ;
    propRefList = [] ;
    
    for each in references :

        namespace = ( mc.file ( each , q = True , namespace = True ) ) ;

        if ( '/char/' in str ( each ) ) or ( '/character/' in str ( each ) ) :
            
            if ( 'background' in each ) :
                pass ;

            else :
                if ( pm.objExists ( namespace + ':' + 'Geo_Grp' ) == True  ) or ( pm.objExists ( namespace + ':' + 'geo_grp' ) == True  ) :
                    charRefList.append ( each ) ;
                else : pass ;

        elif ( 'prop' in each ) or ( 'vehicle' in each ) or ( 'weapon' in each ) :
            ### PROP ###            
            if ( 'alley' in each ) :
                pass ;

            elif ( 'prop' in each )  :
                if ( pm.objExists ( namespace + ':' + 'Geo_Grp' ) == True  ) or ( pm.objExists ( namespace + ':' + 'geo_grp' ) == True  ) :
                    propRefList.append ( each ) ;
                else : pass ;
                
            else : pass ;
            
            ### VEHICLE ###
            if ( 'vehicle' in each ) :              
                if ( 'bus' in each ) or ( 'truck' in each ) or ( 'car' in each ) or ( 'van' in each ) :
                    pass ;
                else :

                    if ( pm.objExists ( namespace + ':' + 'Geo_Grp' ) == True  ) or ( pm.objExists ( namespace + ':' + 'geo_grp' ) == True  ) :
                        propRefList.append ( each ) ;
                    else : pass ;
                    
            else : pass ;
            
            ### WEAPON ###
            if 'weapon' in each :
                if ( pm.objExists ( namespace + ':' + 'Geo_Grp' ) == True  ) or ( pm.objExists ( namespace + ':' + 'geo_grp' ) == True  ) :
                    propRefList.append ( each ) ;
                else : pass ;

            else : pass ;
                
        else : pass ;
                
    charNSList = [] ;
    propNSList = [] ;
    
    for each in charRefList :
        charNSList.append ( mc.file ( each , q = True , namespace = True ) ) ;
    
    for each in propRefList :
        propNSList.append ( mc.file ( each , q = True , namespace = True ) ) ;
        
    if pm.objExists ( 'set' ) == True :
        setItems = pm.listRelatives ( c = True ) ;
        
        for prop in propNSList :
            for item in setItems :
                if prop in item :
                    propNSList.remove ( prop ) ;
    
    charNSList.sort();
    propNSList.sort();

    return ( charNSList , propNSList ) ;

def updateTSL_cmd ( *args ) :

    NSList = getRefNS_cmd ( ) ;
    charNSList = NSList [0] ;

    pm.textScrollList ( 'characterList_textScroll' , e = True , ra = True ) ;
    for each in charNSList :    
        pm.textScrollList ( 'characterList_textScroll' , e = True , append = each ) ;
    
    propNSList = NSList [1] ;
    pm.textScrollList ( 'propList_textScroll' , e = True , ra = True ) ;
    for each in propNSList :    
        pm.textScrollList ( 'propList_textScroll' , e = True , append = each ) ;

def updateTimeRange_cmd ( *args ) :
    time = findStartEnd_cmd ( ) ;
    # print time ;
    pm.intField ( 'preRoll_intField' , e = True , v = time [0] ) ;
    pm.intField ( 'start_intField' , e = True , v = time [1] ) ;
    pm.intField ( 'end_intField' , e = True , v = time [2] ) ;
    pm.intField ( 'postRoll_intField' , e = True , v = time [3] ) ;

def refresh_cmd ( *args ) :
    updateTSL_cmd ( ) ;
    updateTimeRange_cmd ( ) ;

##################
##################
''' UI '''
##################
##################

width = 400.00 ;
title = 'bCache Out Util v4.2' ;

help_txt = '''
<ORIGIN option>
Turn this option on when the character is too far from the origin.

This script will :

1. cache the object out from the origin (0 , 0 , 0)

2. import the cache back into the scene

3. position the cache back to it's original position

4. cache the object out again

<JOINT INFO option>
Turn this option on when you want to simulate with pMuscle setup


<NOTE TO USER>
Since this project's asset is vaguely organized, this script is heavily hard coded and inflexible. Should the problem arised please contact Bird of simulation department

in case of emergency:
( facebook )    Bird Weerapot
( line )        birdiesama
'''

def separator ( *args ) :
    pm.separator ( h = 5 , vis = False ) ;

def filler ( times = 1 , *args ) :
    for i in range ( 0 , int ( times ) ) :
        pm.text ( label = '' ) ;

def run ( *args ) :

    if pm.window ( 'animCacheOut_UI' , exists = True ) :
        pm.deleteUI ( 'animCacheOut_UI' ) ;
    else : pass ;

    window = pm.window ( 'animCacheOut_UI', title = title , mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    pm.window ( 'animCacheOut_UI' , e = True , w = width , h = 10 ) ;

    main_layout = pm.rowColumnLayout ( p = window , nc = 1 ) ;
    with main_layout :

        #########
        ''' TAB '''
        #########
        with pm.tabLayout ( innerMarginWidth = 5 , innerMarginHeight = 5 , w = width ) :

            #########
            ''' MAIN TAB '''
            #########
            with pm.rowColumnLayout ( 'main' , nc = 1 , w = width ) :

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                    #########
                    ''' CHARACTER '''
                    #########
                    with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :

                        pm.text ( label = 'CHARACTER(S)' , align = 'center' , font = 'boldLabelFont' ) ;
                        pm.textScrollList ( 'characterList_textScroll' , w = width/2 , h = 150 , ams = True ) ;

                    #########
                    ''' PROP '''
                    #########
                    with pm.rowColumnLayout ( nc = 1, w = width/2 ) :

                        pm.text ( label = 'PROP(S)' , align = 'center' , font = 'boldLabelFont' ) ;
                        pm.textScrollList ( 'propList_textScroll' , w = width/2 , h = 150 , ams = True ) ;
    
                #########
                ''' TIME RANGE LAYOUT '''
                #########
                with pm.rowColumnLayout ( nc = 4 ,  columnWidth = [ ( 1 , width/4 ) , ( 2 , width/4 ) , ( 3 , width/4 ) , ( 4 , width/4 ) ] ) :

                    pm.intField ( 'preRoll_intField' ) ;
                    pm.intField ( 'start_intField' ) ;
                    pm.intField ( 'end_intField' ) ;
                    pm.intField ( 'postRoll_intField' ) ;

                    pm.text ( label = 'pre roll -30' , bgc = [ 0.45 , 0.75 , 0.45 ] ) ;
                    pm.text ( label = 'start' , bgc = [ 1 , 1 , 0 ] ) ;
                    pm.text ( label = 'end' , bgc = [ 1 , 1 , 0 ] ) ;
                    pm.text ( label = 'post roll +30' , bgc = [ 0.45 , 0.75 , 0.45 ] ) ;

                separator ( ) ;

                #########
                ''' REFRESH BUTTON '''
                #########
                with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/4 ) , ( 2 , width/2 ) , ( 3 , width/4 ) ] ) :

                    filler ( ) ;
                    pm.button ( label = 'refresh' , c = refresh_cmd , bgc = (  0.65 , 0.85 , 0.9 ) ) ;

                separator ( ) ;

                #########
                ''' OPTIONS '''
                #########
                with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , width/4 ) , ( 2 , width/4 ) , ( 3 , width/4 ) , ( 4 , width/4 ) ] ) :

                    pm.text ( label = 'step' ) ;
                    filler (3) ;

                    with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/4/5 ) , ( 2 , width/4/5*3 ) , ( 3 , width/4/5 )] ) :
                        filler ( ) ;
                        pm.floatField ( 'step_input' , ann = 'step' , precision = 2 , v = 1 , w = width/8 ) ;
                        filler ( ) ;

                    pm.checkBox ( 'cameraCache_checkBox' , label = 'camera' , v = True ) ;
                    pm.checkBox ( 'jointInfo_checkBox' , label = 'joint info' , v = False , enable = False ) ;
                    pm.checkBox ( 'originCache_checkBox' , label = 'origin' , v = False ) ;

                separator ( ) ;

                #########
                ''' CACHE OUT BUTTON '''
                #########

                pm.text ( label = "In viewport, please don't forget to Show > None" , bgc = ( 1 , 0.4 , 0.7 ) ) ;

                pm.button ( label = 'cache out' , c = exportABC_cmd ) ;

                separator ( ) ;

                pm.button ( label = 'joint info test' , bgc = ( 0 , 0 , 0 ) , c = exportJointInfo_cmd ) ;

            #########
            ''' HELP TAB '''
            #########
            with pm.rowColumnLayout ( 'help' ,  nc = 1 , w = width ) :
                        
                help_scrollField = pm.scrollField ( wordWrap = True , editable = True , width = width , height = 315 , text = help_txt ) ;

    updateTSL_cmd ( ) ;
    updateTimeRange_cmd ( ) ;
    
    window.show () ;