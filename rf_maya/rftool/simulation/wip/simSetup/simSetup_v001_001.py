import pymel.core   as pm ;
import maya.cmds    as mc ;
import re ;

class Info ( object ) :

    def __init__ ( self ) :
        self.workSpace_path = pm.system.Workspace.getPath() ;
        self.ui = Ui() ;

    #def save ( self ) :

    #def load ( self ) :

class Setup ( object ) :

    def __init__ ( self ) :
        self.ui = Ui() ;

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def getTopNode ( self , node , *args ) :
        if node.getParent() :
            return self.getTopNode ( node.getParent() ) ;
        else :
            return node ;

    def getEdges ( self , *args ) :
        edgeSelectionError_txt = '[selection error] please select 2 edges to continue' ;
        selection = pm.ls ( sl = True ) ;

        ### object ###

        if len ( selection ) > 0 :
            object = selection[0].node() ;
            object = pm.general.PyNode ( object ) ;

        else :
            pm.error ( edgeSelectionError_txt ) ;

        ### edge ###
        
        if len ( selection ) == 1 :
            
            selection   = selection [0] ;
            edgeID      = re.findall ( 'e' + '[[]' + '\d+' + '[:]' + '\d+' + '[]]' , str(selection) ) ;
            # ['e[656:657]']

            if edgeID :
                edge_list   = re.findall ( '\d+' , str(edgeID) ) ;
                
                edges       = [] ;
                for edge in edge_list :
                    edges.append ( edge ) ;

            else :
                pm.error ( edgeSelectionError_txt ) ;

        elif len ( selection ) == 2 :
            
            edges = [] ;

            for each in selection :
                edgeID = ( re.findall ( 'e' + '[[]' + '\d+' + '[]]' , str(each) ) ) ;
                edges.append ( re.findall ( '\d+' , str(edgeID) ) [0] ) ;
            
        else :
            pm.error ( edgeSelectionError_txt ) ;

        pm.textField ( self.ui.edge1_txtField , e = True , text = edges[0] ) ;
        pm.textField ( self.ui.edge2_txtField , e = True , text = edges[1] ) ;

        ### transform + shape ###

        objShape    = selection[0].node() ;
        obj         = objShape.getTransform() ;

        # check for tfm duplicate #
        try :
            obj     = pm.general.PyNode ( obj.nodeName() ) ;
        except :
            pm.error ( '[duplicate error] %s is not unique' % obj.nodeName() ) ;
        
        pm.textField ( self.ui.object_txtField , e = True , text = obj ) ;
        pm.textField ( self.ui.objectTopNode_txtField , e = True , text = self.getTopNode ( node = obj ) ) ;

    def showUi ( self ) :
        self.ui.show() ;
        pm.button ( self.ui.getEdge_btn , e = True , c = self.getEdges ) ;

class Ui ( object ) :

    def __init__ ( self ) :
        self.ui         = 'simSetupTool_Ui' 
        self.width      = 400.00 ;
        self.title      = 'Sim Setup Tool' ;
        self.version    = 'v0.0' ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    def deleteUI ( self , ui ) :
        if pm.window ( ui , ex = True ) :
            pm.deleteUI ( ui ) ;
            self.deleteUI ( ui ) ;

    def createWindow ( self ) :
        
        self.deleteUI ( self.ui ) ; 
        
        self.window = pm.window ( self.ui ,
            title       = self.title + '_' + self.version ,
            mnb         = True ,
            mxb         = False ,
            sizeable    = True ,
            rtf         = True ,
            ) ;

        pm.window ( self.window , e = True , w = self.width , h = 10 ) ;
    
    def filler ( self , rep = 1 ) :
        for i in range ( 1 , rep + 1 ) :
            pm.text ( label = '' ) ;

    def separator ( self , h = 5 ) :
        pm.separator ( vis = False , h = h ) ;

    def show ( self ) :
        
        self.createWindow () ;
        with self.window :

            # main_layout
            with pm.rowColumnLayout ( nc = 1 , w = self.width ) :

                with pm.rowColumnLayout ( nc = 5 , cw = [
                        ( 1 , self.width / 10 * 0.4 ) ,
                        ( 2 , self.width / 10 * 4.5 ) ,
                        ( 3 , self.width / 10 * 0.2 ) ,
                        ( 4 , self.width / 10 * 4.5 ) ,
                        ( 5 , self.width / 10 * 0.4 ) ] ) :

                    self.filler ( ) ;
                    
                    localWidth = self.width / 10 * 4.5 ;
                    with pm.rowColumnLayout ( nc = 1 , w = localWidth ) :

                        pm.text ( label = 'object' , w = localWidth ) ;
                        self.object_txtField = pm.textField ( 'object_txtField' , w = localWidth ) ;

                        pm.text ( label = 'edge 1' , w = localWidth   ) ;
                        self.edge1_txtField = pm.textField ( 'edge1_txtField' , w = localWidth ) ;
                    
                    self.filler ( ) ;

                    localWidth = self.width / 10 * 4.5 ;
                    with pm.rowColumnLayout ( nc = 1 , w = localWidth ) :

                        pm.text ( label = 'object top node' , w = localWidth ) ;
                        self.objectTopNode_txtField = pm.textField ( 'objectTopNode_txtField' , w = localWidth ) ;

                        pm.text ( label = 'edge 2' , w = localWidth ) ;
                        self.edge2_txtField = pm.textField ( 'edge2_txtField' , w = localWidth ) ;
    
                    self.filler ( ) ;

                self.separator() ;

                with pm.rowColumnLayout ( nc = 3 , cw = [
                    ( 1 , self.width / 10 * 0.4 ) ,
                    ( 2 , self.width / 10 * 9.2 ) ,
                    ( 3 , self.width / 10 * 0.4 ) ] ) :
                    
                    localWidth = 9.2 ;
                    self.filler ( ) ;
                    self.getEdge_btn = pm.button ( 'getEdge_btn' , label = 'get edge' , bgc = ( 1 , 0.85 , 0 ) , w = localWidth ) ;
                    self.filler ( ) ;

                self.separator() ;

                with pm.rowColumnLayout ( nc = 5 , cw = [
                        ( 1 , self.width / 10 * 0.4 ) ,
                        ( 2 , self.width / 10 * 4.5 ) ,
                        ( 3 , self.width / 10 * 0.2 ) ,
                        ( 4 , self.width / 10 * 4.5 ) ,
                        ( 5 , self.width / 10 * 0.4 ) ] ) :

                    localWidth = self.width / 10 * 4.5 ;
                    self.filler ( ) ;
                    self.save_btn = pm.button ( 'save_btn' , label = 'save' , w = localWidth ) ;
                    self.filler() ;
                    self.load_btn = pm.button ( 'load_btn' , label = 'load' , w = localWidth ) ;
                    self.filler() ;

                self.separator() ;

        self.window.show() ;

def run ( *args ) :
    setup = Setup() ;
    setup.showUi() ;

'''
def caller ( *args ) :
    import wip.simSetup.simSetup_v001_001 as test ;
    reload ( test ) ;
    test.run() ;
'''
