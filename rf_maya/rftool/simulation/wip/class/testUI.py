import pymel.core as pm ;

class TestUI ( object ) :

    def __init__ ( self ) :
        self.width  = 500.00 ;
        self.title  = 'test window' ;
        self.ui     = 'test_ui' ;

    def __str__ ( self ) :
        return self.title ;

    def __repr__ ( self ) :
        return self.title ;

    def checkDuplicatedUI ( self ) :
        if pm.window ( self.ui , exists = True ) :
            pm.deleteUI ( self.ui ) ;
            self.checkDuplicatedUI ( ) ;
        else :
            pass ;

    def run ( self ) :
        self.checkDuplicatedUI ( ) ;

        window = pm.window ( self.ui ,
            title       = self.title ,
            mnb         = True , 
            mxb         = False ,
            sizeable    = True , 
            rtf         = True ) ;

        pm.window ( self.ui , e = True , w = self.width , h = 10 ) ;
        
        with window :

            # mainLayout
            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , self.width/2 ) , ( 2 , self.width/2 ) ] ) :

                self.floatField     = pm.floatField () ;
                self.floatField_btn = pm.button ( c = self.printFloatField ) ; 

    def printFloatField ( self , *args ) :
        print pm.floatField ( self.floatField , q = True , v = True ) ;

def run ( *args ) :
    ui = TestUI() ;
    ui.run () ;

run () ;