import pymel.core as pm ;

class BaseClass ( object ) :
	
	def __init__ ( self ) :
		pass ;

	def printJune ( self , *args ) :
		print ( 'June is Awesome' ) ;

class GUI ( BaseClass ) :

    def __init__ ( self ) :

    	super ( BaseClass , self ).__init__() ;

        self.ui         = 'GUI_ui' ;
        self.width      = 500.00 ;
        self.title      = 'title' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def show ( self ) :

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = self.width , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = self.width ) ;
            with main_layout :

                pm.button ( label = 'print june' , w = self.width , c = self.printJune ) ;

        window.show () ;

def run ( ) :
	gui = GUI() ;
	gui.show() ;

run() ;