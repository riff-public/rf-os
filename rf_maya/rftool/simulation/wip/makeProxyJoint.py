import pymel.core as pm ;

#references = mc.file ( q = True , r = True ) ;

#print references ;

def jointProxy_cmd ( ref , *args ) :

    def showUnlockDefaultAttr_cmd ( target , *args ) :

        target = pm.general.PyNode ( target ) ;

        for axis in [ 'x' , 'y' , 'z' ] :
            
            pm.setAttr ( target + '.t' + axis , channelBox = True ) ; 
            pm.setAttr ( target + '.t' + axis , keyable = True , lock = False ) ;

            pm.setAttr ( target + '.r' + axis , channelBox = True ) ;
            pm.setAttr ( target + '.r' + axis , keyable = True , lock = False ) ;
            
            pm.setAttr ( target + '.s' + axis , channelBox = True ) ;
            pm.setAttr ( target + '.s' + axis , keyable = True , lock = False ) ;
    
    ref = 'P:/Two_Heroes/asset/character/god/spiderGirlGod/lib/spiderGirlGod_rig_md.ma' ;
    
    namespace = mc.file ( ref , q = True , namespace = True ) ;
    
    if pm.objExists ( namespace + ':Skin_Grp' ) != True :
        pm.error ( namespace + "'s Skin_Grp does not exist" ) ;
    else :
        
        skin_grp = pm.duplicate ( namespace + ':Skin_Grp' ) [0] ;
        
        pm.parent ( skin_grp , w = True ) ;
        
        ### clear constraint ###
        allItem_list = pm.listRelatives ( skin_grp , allDescendents = True ) ;
        for item in allItem_list :
            if ( 'parentCon' in str(item) ) or ( 'scaleCon' in str(item) ) :
                pm.delete ( item ) ;
            else :
                pass ;
        
        ### connect to original ###

        allItem_list = pm.listRelatives ( skin_grp , allDescendents = True ) ;
        
        for item in allItem_list :
        
            print item ;
           
            refItem = pm.general.PyNode ( namespace + ':' + item ) ;
            item = pm.general.PyNode ( item ) ;
            
            showUnlockDefaultAttr_cmd ( item ) ;

            refItem.t >> item.t ;
            refItem.r >> item.r ;
            refItem.s >> item.s ;

        showUnlockDefaultAttr_cmd ( skin_grp ) ;
        parCon = pm.parentConstraint ( namespace + ':Skin_Grp' , skin_grp , mo = False , skipTranslate = 'none' , skipRotate = 'none' ) ;

        ### *** don't forget to delete constraint after bake
                
jointProxy_cmd ( ref = '' ) ;