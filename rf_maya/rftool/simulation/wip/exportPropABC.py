import pymel.core as pm ;

##################
''' create '''
##################

def createPropShader_cmd ( *args ) :
    if pm.objExists ( 'propABC_shader' ) != True :    
        propShader = pm.shadingNode ( 'lambert' , asShader = True , n = 'propABC_shader' ) ;
    else :
        propShader = pm.general.PyNode ( 'propABC_shader' ) ;
    propShader.color.set( 0  , 1 , 1 );

    return propShader ;

def createDisplayLayer ( n = '' , *args ) :
    if pm.objExists ( n ) == True :
        displayLayer = pm.general.PyNode ( n ) ; 
    else :
        displayLayer = pm.createDisplayLayer ( name = n , number = 1 , empty = True ) ;
    return displayLayer ;


def createGrp_cmd ( name = '' , *args ) :
    if pm.objExists ( name ) == True :
        grp = pm.general.PyNode ( name ) ;
    else :
        grp = pm.group ( em = True , w = True , n = name ) ;
    return grp ;

##################
''' util '''
##################

def duplicateBSH_cmd ( target , *args ) :
    
    prop_grp = createGrp_cmd ( name = 'prop_ABC' ) ;
    
    duplicatedObj_nm = target.split(':')[-2] + '_' + target.split(':')[-1] ;
    duplicatedObj = pm.duplicate ( target , n = duplicatedObj_nm ) ;
    pm.blendShape ( target , duplicatedObj , origin = 'world' , weight = [ 0 , 1.0 ] , n = 'prop_BSH' ) ;
    pm.parent ( duplicatedObj , prop_grp ) ;
    return duplicatedObj ;

##################
''' main '''
##################

def createPropGrp_cmd ( *args ) :
    
    selection_list = pm.ls ( sl = True ) ;

    ### shader ###
    shader = createPropShader_cmd ( ) ;

    ### layer ###
    selectedProp_layer = createDisplayLayer ( n = 'selectedProp_layer' ) ;
    
    if selectedProp_layer.visibility.get ( ) == True :
        pm.mel.eval ( 'layerEditorLayerButtonVisibilityChange %s' % selectedProp_layer ) ;
    else :
        pass ;

    duplicatedObj_list = [] ;

    for each in selection_list : 
        
        duplicatedObj = duplicateBSH_cmd ( target = each ) ;
        duplicatedObj_list.append ( duplicatedObj ) ;

        pm.select ( duplicatedObj_list ) ;
        pm.hyperShade( assign = shader ) ;

        pm.editDisplayLayerMembers ( selectedProp_layer , each , noRecurse = True ) ;    
    
createPropGrp_cmd ( ) ;