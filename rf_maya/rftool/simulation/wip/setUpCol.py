import pymel.core as pm ;
import maya.cmds as mc ;

def findRoot ( *args ) :
    # return root
    selection = mc.ls ( sl = True ) [0] ;

    fullPath = mc.listRelatives ( selection , p = True , f = True ) ;
    
    fullPath = fullPath[0].split ( '|' ) ;
    root = fullPath [1] ;
    
    return root ;
    
def clean ( target = '' , *args ) :
    # freeze transform , delete history , center pivot
    pm.makeIdentity ( target , apply = True ) ;    
    pm.delete ( target , ch = True ) ;
    pm.xform ( target , cp = True ) ;
    
def makeName ( target ) :
    # return name 

    nameSplit = target.split ( '_' ) ;
    name = '' ;

    for each in nameSplit :

        if each[0].isupper != True :
            name += ( each[0].upper( ) + each[1:] ) ;
        else :
            name += each ;
            
    return name ;
    
def duplicate ( target = '' , suffix = '' , *args ) :
    # return duplicatedRoot ;

    duplicatedRoot = pm.duplicate ( target ) [0] ;
    duplicatedRoot.rename ( duplicatedRoot [0:-1] + suffix ) ;
    
    hierarchy = pm.listRelatives ( duplicatedRoot , ad = True , type = 'transform' ) ;
    for each in hierarchy :
        
        each.rename ( each + suffix ) ;
        
    return ( duplicatedRoot ) ;

def addSuffix ( target = '' , suffix = '' , *args ) :
    # return renamed target
    hierarchy = pm.listRelatives ( target , ad = True , type = 'transform' ) ;
        
    for each in hierarchy :
        
        each.rename ( each + suffix ) ;

    target = pm.general.PyNode ( target ) ;
    target.rename ( target + suffix ) ;
        
    return target ;

def createCharacterLoc ( *args ) :
    
    CIN_topNode = findRoot ( ) ;
    characterName = makeName ( target = CIN_topNode ) ;
    
    object = pm.ls ( sl = True , o = True ) ;
    object = pm.listRelatives ( object , parent = True ) [ 0 ] ;
    print object ;
    # object = PyNode ;

    edges = pm.ls ( sl = True ) ;
    curves = [] ;
    
    for edge in edges :
        curve = pm.polyToCurve ( edge , form = 0 , degree = 1 ) ;
        curves.append ( curve [0] ) ;
    
    nurb = pm.loft ( curves [0] , curves [1] , ch = 0 , ar = True , d = 1 , ss = 1 , rn = 0 , po = 0 , rsn = True ) ;
    
    nurb = pm.general.PyNode ( nurb [0] ) ;
    nurb.rename ( characterName + '_nrb' ) ;    
    clean ( target = nurb ) ;
    pm.delete ( curves ) ;
    # create follicle

    folShape = pm.createNode ( 'follicle' ) ;
    fol = pm.listRelatives ( folShape , type = 'transform' , parent = True ) [0] ;

    pm.rename ( fol , characterName + '_fol' ) ;

    folShape.simulationMethod.set ( 0 ) ;

    folShape.outRotate >> fol.r ;
    folShape.outTranslate >> fol.t ;

    # objectShape =  pm.listRelatives ( nurb , s = True ) ;

    nurb.worldMatrix[0] >> folShape.inputWorldMatrix ;
    nurb.local >> folShape.inputSurface ;

    folShape.parameterU.set ( 0.5 ) ;
    folShape.parameterV.set ( 0.5 ) ;
    
    # create _CIN then _COL
    
    COL_topNode = duplicate ( target = CIN_topNode , suffix = '_COL' ) ; 
    CIN_topNode = addSuffix ( target = CIN_topNode , suffix = '_CIN' ) ; 

    # CIN_COL_BSH

    print nurb ;
    print object ;
    
    wrap = pm.deformer ( nurb , object , type = 'wrap' ) ;
    print wrap ;

    # noTouch = pm.group ( em = True , w = True , name = 'noTouch_grp' ) ;
