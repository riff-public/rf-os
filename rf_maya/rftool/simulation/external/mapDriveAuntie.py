import os
import time
import subprocess
import shutil
import glob


def mapDrive():
    
    # for d in map(chr, range(97, 123)):
    for d in [ "a", "b" , "g" , "h", "o" , "p" , "q" , "r" , "s" , "u" , "x" , "z" , "l" , "m" , "w" , "y"]:
        if os.path.isdir(d + ":/"):
            subprocess.call(["net","use","%s:" % d, "/delete"])
    
    time.sleep(12)

    subprocess.call(["net","use","g:", r"\\192.168.1.26\game"])
    #subprocess.call(["net","use","h:", r"\\RIFFHOT\Volume_1"])
    #subprocess.call(["net","use","o:", r"\\riffxel\Project\toei"])
    subprocess.call(["net","use","p:", r"\\192.168.1.26\Project"])
    #subprocess.call(["net","use","q:", r"\\192.168.1.26\Q"])
    subprocess.call(["net","use","r:", r"\\riffraam\render"])
    #subprocess.call(["net","use","z:", r"\\192.168.1.26\Project\Ghibli"]) 
    subprocess.call(["net","use","x:", r"\\192.168.1.26\Project\Korea"])
    subprocess.call(["net","use","a:", r"\\christcache\VFX-Cache"])
    subprocess.call(["net","use","b:", r"\\jackcache\VFX-Cache\HQshared"])
    subprocess.call(["net","use","y:", r"\\192.168.1.26\Production"])
    subprocess.call(["net","use","z:", r"\\192.168.1.26\Post-Production"])
    #subprocess.call(["net","use","w:", r"\\RIFFARCHIVE\wares"])
    #subprocess.call(["net","use","u:", r"\\192.168.1.26\Project\Unicorn_Project\driveU"])

    #if not os.path.isdir("L:/"): subprocess.call(["net","use","l:", r"\\NAS-01\libraries"])
    #if not os.path.isdir("M:/"): subprocess.call(["net","use","m:", r"\\NAS-01\onone"])


def copyStuff():
    
    baseDir = r"\\riffxel\script\install\copyToMaya"

    for mayaVer in ["2011", "2012" , "2013" , "2014" , "2015" , "2017" ]:
        
        if mayaVer == "2017" :
            dst = "C:\\Users\\AuntieHome\\Documents\\maya\\%s" % mayaVer
        else :
            dst = "C:\\Users\\AuntieHome\\Documents\\maya\\%s-x64" % mayaVer
        
        if os.path.isdir(dst):
            
            # Maya Env is mostly separate for each version.
            src = "%s\\%s\\Maya.env" % (baseDir, mayaVer)
            if os.path.isfile(src):
                shutil.copy2(src, dst)

            # userSetup both mel and py are mostly the same for all version
            for ext in [ "mel", "py"]:
                src = "%s\\global\\userSetup.%s" % (baseDir, ext)
                if os.path.isfile(src):
                    shutil.copy2(src, "%s\\scripts" % dst)

            # .bat plugin separate for each version.
            src = "%s\\%s\\fuckingplugin.bat" % (baseDir, mayaVer)
            if os.path.isfile(src):
                shutil.copy2(src, dst)

    # clear C:\\3delight_cache
    cacheDir = "C:\\3delight_cache"
    if os.path.isdir(cacheDir): shutil.rmtree(cacheDir)
    os.makedirs(cacheDir)

    #empty recycle bin
    try:
        from ctypes import windll
        windll.shell32.SHEmptyRecycleBinA(None, None, 1)
    except:
        pass

    #run .bat per version
    os.system("C:\\Users\\AuntieHome\\Documents\\maya\\2015-x64\\fuckingplugin.bat")
    os.system("C:\\Users\\AuntieHome\\Documents\\maya\\2017\\fuckingplugin.bat")
    
if __name__ == '__main__':

    os.environ['NAIAD_PATH'] = r"\\render\shader\naiad"
    os.environ['MAYA_MODULE_PATH'] = r"\\render\shader\naiad\Buddies\Maya"
    mapDrive()
    copyStuff()
