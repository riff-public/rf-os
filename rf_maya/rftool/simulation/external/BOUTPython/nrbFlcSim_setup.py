import maya.cmds as mc
import maya.mel as mm
import sys
import create_nurbBase as cnb

def getData():
	charName = mc.textField('charName_TF',q = True , text = True )
	vtrLFTUP = mc.intField('vtrLFTUP_IF', q = True , v = True)
	vtrRGTUP = mc.intField('vtrRGTUP_IF', q = True , v = True)
	vtrLFTLOW = mc.intField('vtrLFTLOW_IF', q = True , v = True)
	vtrRGTLOW = mc.intField('vtrRGTLOW_IF', q = True , v = True)
	geoNrb = mc.textField('bodyName_TF', text = True , q  = True )
	vtxList = [vtrLFTUP,vtrRGTUP,vtrLFTLOW,vtrRGTLOW]
	snapNrbFlc_FUNC(charName,geoNrb,vtxList)
	
def snapNrbFlc_FUNC(charName,geoNrb,vtxList):

	NURBname = charName +'_nrb'
	FOLname = charName +'_flc'
	cnb.createNURBbase(NURBname, FOLname)
	cnb.snapNURBbase(geoNrb, vtxID = vtxList, nurbBase=NURBname)
	notouchGrp = mc.group(em = True, n = 'NOTOUCH_GRP')
	flcGrp = mc.group(em = True, n = charName + 'Flc_GRP')
	mc.parent(NURBname,notouchGrp)
	mc.parent(FOLname,flcGrp)
	mc.parent(flcGrp,notouchGrp)
	mc.select([NURBname, geoNrb])
	mc.CreateWrap()
	mc.parent(geoNrb+'Base',notouchGrp)
	sel = mc.listRelatives(notouchGrp,ad = True)
	for i in sel:
		lockList = mc.listAttr(i , k = True)
		if lockList != None:
			for j in lockList :
				if 'visibility'not in j:
					try:
						mc.setAttr(i + '.' + j ,lock = True)
					except:
						pass


def main():
	if mc.window('setupNrbFlc', q=True ,exists=True):
	    mc.deleteUI('setupNrbFlc')
	mc.window('setupNrbFlc',title="Nurb Flc",s=False,w = 250,h = 351)
	mc.columnLayout(cat = ('both',1), cw = 230)
	mc.frameLayout( label='create nurb', borderStyle='out' , w = 230)
	mc.columnLayout( rowSpacing = 1 , cat = ('both',1), cw = 230)
	mc.rowColumnLayout(nc=2,columnWidth=[(1, 80), (2, 140)])
	mc.text( label='Character:' )
	mc.textField('charName_TF', text = 'char' )
	mc.setParent( '..' )
	mc.columnLayout( rowSpacing = 1 , cat = ('both',1), cw = 230)
	mc.rowColumnLayout(nc=2,columnWidth=[(1, 80), (2, 140)])
	mc.text( label='body:' )
	mc.textField('bodyName_TF', text = 'char_sdm' )
	mc.setParent('..')
	mc.rowColumnLayout(nc=3,columnWidth=[(1, 80), (2, 65), (2, 65)])
	mc.text( label='vtx:' )
	mc.intField('vtrLFTUP_IF', v = 0 , w = 65)
	mc.intField('vtrRGTUP_IF', v = 0 , w = 65)
	mc.text('')
	mc.intField('vtrLFTLOW_IF', v = 0 , w = 65)
	mc.intField('vtrRGTLOW_IF', v = 0 , w = 65)
	mc.setParent('..')
	mc.setParent('..')
	mc.setParent('..')
	mc.button('createNrbFlcBT', l = 'create', c = ml.Callback(getData))
	mc.showWindow('setupNrbFlc')
	
#main()



