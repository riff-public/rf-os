import maya.cmds as mc

def createNURBbase(NURBname, FOLname):
            
    mc.nurbsPlane(n=NURBname, p=[0.0, 0.0, 0.0], ax=[0.0, 1.0, 0.0], w=1, lr=1, d=1, u=1, v=1, ch=0)
    mc.createNode('follicle')
    mc.pickWalk(d='up')
    mc.rename(FOLname)
     
    mc.connectAttr( NURBname + 'Shape.worldMatrix[0]', FOLname + 'Shape.inputWorldMatrix', f=1)
    mc.connectAttr( NURBname + 'Shape.local', FOLname + 'Shape.inputSurface', f=1)
    mc.connectAttr( FOLname + 'Shape.outTranslate', FOLname + '.translate', f=1)
    mc.connectAttr( FOLname + 'Shape.outRotate', FOLname + '.rotate', f=1)
    mc.setAttr( FOLname + 'Shape.parameterU', 0.5)
    mc.setAttr( FOLname + 'Shape.parameterV', 0.5)

def snapNURBbase(geoNrb, vtxID, nurbBase):
    
    vtxBase = [
    
    [ geoNrb + '.vtx['+ str(vtxID[0]) +']', geoNrb + '.vtx['+ str(vtxID[1]) +']'],
    [ geoNrb + '.vtx['+ str(vtxID[2]) +']', geoNrb + '.vtx['+ str(vtxID[3]) +']']
    
    ]
    
    vtxBaseWS = []
        
    # Define vertex position
    for i in xrange(2):
        vtxBaseWS.append([])
        for j in xrange(2):
            vtxBaseWS[i].append(mc.xform(vtxBase[i][j], ws=1, t=1, q=1))
    
    # Snap CV to vertex
    for i in xrange(2):
        for j in xrange(2):
            
            nurbCV = '%s.cv[%d][%d]' %(nurbBase, i, j)
            tx = vtxBaseWS[i][j][0]
            ty = vtxBaseWS[i][j][1]
            tz = vtxBaseWS[i][j][2]
            
            mc.move(tx, ty, tz, nurbCV, a=1)
            