###############################
# Fixed shapeDeform script
###############################

import maya.cmds as mc

def abcInfo():
    allAbc = mc.ls(et = 'AlembicNode')
    
    for eachNode in allAbc:
        print '\n', eachNode
        print 'Start/End Frame'
        print mc.getAttr(eachNode + '.startFrame'), mc.getAttr(eachNode + '.endFrame')
        
        startF = mc.getAttr(eachNode + '.startFrame')
        endF   = mc.getAttr(eachNode + '.endFrame')
        
        mc.playbackOptions( minTime = startF, ast = startF, aet = endF, maxTime = endF )
        mc.currentTime( startF, e = True )

def fixShapeDeformed():
    allShape = mc.ls(et = 'mesh')
    
    incShpNode = []
    
    for eachShape in allShape:
        if 'ShapeDeformed' in eachShape:
            incShpNode.append(eachShape)
    
    if len(incShpNode) > 0:
        
        print '\n============================='
        print 'Rename invalid shape node'
        print '=============================\n'
        
        for eachShape in incShpNode:
            
            validShape = eachShape.split('|')[-1][:-8]
            mc.select(eachShape)
            mc.rename(validShape)
            
            print eachShape, ' -> ', validShape
    else:
        print '\n============================='
        print 'All shape node name are valid'
        print '=============================\n'
        
    mc.select(cl = 1)

def fixShapeName():
    allShape = mc.ls(et = 'mesh')
#    print allShape
    
#    transformNode = []
    
    for eachShape in allShape:
        mc.select(eachShape)
        newName = mc.pickWalk(direction='up')[0] + 'Shape'
        mc.select(eachShape)
        mc.rename(newName)
    
#fixShapeName()

abcInfo()
fixShapeDeformed()