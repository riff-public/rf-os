import maya.cmds as mc

def main(*arg):
    selectedGroup = mc.ls(sl = True)
    
    objInGroup = mc.listRelatives(selectedGroup)
    objInGroup.sort()
    
    for eachObj in objInGroup:
        mc.reorder(eachObj, back=True)