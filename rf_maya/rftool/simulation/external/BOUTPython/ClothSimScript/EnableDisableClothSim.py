import maya.cmds as mc


def EnableNCloth(*arg):
    ###   Enable nCloth Dynamic   ###
    sel = mc.ls(sl=True)
    for i in range(len(sel)):
        ObjShape = mc.listRelatives(sel[i], shapes = True)[0]
        nClothShape = mc.listConnections(ObjShape, type = 'nCloth')[0]
        mc.setAttr(nClothShape + '.isDynamic' , 1)

def DisableNCloth(*arg):
    ###   Disable nCloth Dynamic   ###
    sel = mc.ls(sl=True)
    for i in range(len(sel)):
        ObjShape = mc.listRelatives(sel[i], shapes = True)[0]
        nClothShape = mc.listConnections(ObjShape, type = 'nCloth')[0]
        mc.setAttr(nClothShape + '.isDynamic' , 0)