import maya.cmds as mc
from random import randrange, uniform

def main(*args):
    sel = mc.ls(sl=True)
    
    for i in range(len(sel)):
        SelShape = mc.listRelatives(sel[i] , c=True)
        SelABC = mc.listConnections(SelShape , s=True , type = 'AlembicNode')
        ABCsf = mc.getAttr(SelABC[0] + '.startFrame')
        ABCef = mc.getAttr(SelABC[0] + '.endFrame')
        
        # randrange gives you an integral value
        frand = uniform(ABCsf,ABCef)
        SetOffset = round(frand, 0)
        SelOffset = mc.setAttr(SelABC[0] + '.offset' , SetOffset)
        SelCycleType = mc.setAttr(SelABC[0] + '.cycleType' , 1)
        
        # Print Value
        print ''
        print sel[i]
        print 'First Frame = ' , ABCsf
        print 'End Frame   = ' , ABCef
        print 'Speed       = ' , mc.getAttr(SelABC[0] + '.speed')
        print 'Offset      = ' , mc.getAttr(SelABC[0] + '.offset')
        
if mc.window ('BOUT_RandomOffsetABC', q = True, ex = True):
    mc.deleteUI ('BOUT_RandomOffsetABC')
    
window = mc.window ('BOUT_RandomOffsetABC' , title = "BOUT Random Offset ABC", widthHeight=(100, 50))

mc.columnLayout(adj = True)

mc.button( label='Random Offset  ABC', c =  main)

mc.setParent( '..' )

mc.showWindow('BOUT_RandomOffsetABC')
mc.window ('BOUT_RandomOffsetABC', e = True , s = 0 , w = 300 , h = 27 , tlc = [500,800])