import maya.cmds as mc

def main(*arg):
	sel = mc.ls(sl = True)
	for each in sel:
		shp = mc.listRelatives(each)
		objType = mc.objectType(shp)
		rigid = mc.listConnections(shp, t = 'mesh')[0]
		mc.rename(each,rigid + '_' + objType)