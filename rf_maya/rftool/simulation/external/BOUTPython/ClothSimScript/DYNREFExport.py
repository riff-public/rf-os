import maya.cmds as mc
import maya.mel as mm
import os

import databaseQuery as dbq
import production as pdt
import fileOperator as fop
import UITools as pg
import MnkHrchyOdinUI as mhou

import ClothSimScript.YumInfo as byif

import sys

mp = '/proj/asset/rig/scriptRigger/win/pythonScript'
if not mp in sys.path :
    sys.path.append(mp)

import testNewSimTools.createDYN_REF_for_hero as createDYN_REFXML

import testNewSimTools.createnDYN_preset as createnDYNPreset
#reload(createnDYNPreset)

def DYNREFExportFUN(*arg):
    
    yumList = byif.YumInfoFUN(*arg)
    
    createnDYNPreset.doCreateDYNAttrPreset(type = "nCloth")
    createnDYNPreset.doCreateDYNAttrPreset(type = "nRigid")
    
    createDYN_REFXML.doCreateAttrPreset()
    
    Versioning = mc.intField( Version , q = True, v = True)
    CheckCur = mc.checkBox( MakeCur , q = True , v = True ) 
    
    # Export allDYN
    DYNREF = mc.duplicate( 'FIX_GRP' , rc = True , n='DYN_REF')[0]
    mc.parent( DYNREF , w=True )
    DYNREFGC = mc.listRelatives( DYNREF , c = True)
    for i in range(len(DYNREFGC)):
        mc.DeleteHistory(DYNREFGC[i])
        DYNREFSplit = DYNREFGC[i].split('FIX1')
        mc.rename(DYNREFGC[i], (DYNREFSplit[0] + 'REF'))
    mc.setAttr( DYNREF + '.v', 1 )
    
    #Export DYNbyPiece
    DYNREFList = mc.listRelatives( DYNREF , c = True)
    print DYNREFList
    
    # ExportSelection
    pathREF = "/proj/" + yumList[0] + "/" + yumList[1] + "/" + yumList[2] + "/" + yumList[3] + "/" + yumList[4] + "/data/DYN_REF/"
    #print pathREF 

    if not os.path.exists(pathREF):
    	os.makedirs(pathREF)
    
    pathVER = pathREF + yumList[3] + "_allDYNVersions/"
    if not os.path.exists(pathVER):
    	os.makedirs(pathVER)
    
    if os.path.exists(pathVER + yumList[3] + "_allDYN.v" + str(Versioning) + ".ma"):
        print 'Already Exists'

        def YesReplace(*arg):
            print 'Yes'
            DYNREF = mc.duplicate( 'FIX_GRP' , rc = True , n='DYN_REF')[0]
            mc.parent( DYNREF , w=True )
            DYNREFGC = mc.listRelatives( DYNREF , c = True)
            for i in range(len(DYNREFGC)):
                mc.DeleteHistory(DYNREFGC[i])
                DYNREFSplit = DYNREFGC[i].split('FIX1')
                mc.rename(DYNREFGC[i], (DYNREFSplit[0] + 'REF'))
            
            mc.setAttr( DYNREF + '.v', 1 )
            mc.file(pathVER + yumList[3] + "_allDYN.v" + str(Versioning) + ".ma" , f=True , options='v=0' , type='mayaAscii' , pr=True , es=True )
            mc.delete(DYNREF)
            mc.deleteUI (windowRO , window = True )
        def NoReplace(*arg):
            print 'No'
            mc.deleteUI (windowRO , window = True )

        if mc.window ('ReplaceObject', q = True, ex = True):
            mc.deleteUI ('ReplaceObject')
        
        windowRO = mc.window ('ReplaceObject' , title = "Object Already Exists" )
        
        mc.columnLayout( adjustableColumn=True )
        mc.text( label='' )
        mc.text( label=yumList[3] + "_allDYN.v" + str(Versioning) + ".ma" , align='center')
        mc.text( label=' Do you want to Relace it ? ' , align='center')
        mc.text( label='' )
        
        mc.rowColumnLayout( numberOfRows=1 , cat = [(1,'left',50),(2,'left',15),(3,'left' ,19),(4,'left',5),(5,'left' ,5),(6,'left' ,5)])
        
        mc.button(label = 'Yes', c = YesReplace , w=60)
        mc.button(label = 'No', c = NoReplace , w=60)
        
        mc.showWindow('ReplaceObject')
        mc.window ('ReplaceObject', e = True , s = 0 , w = 250, h = 105 , tlc = [600,1100])
                
    else:	
        mc.file(pathVER + yumList[3] + "_allDYN.v" + str(Versioning) + ".ma" , f=True , options='v=0' , type='mayaAscii' , pr=True , es=True )
    
    # Make .cur
    if CheckCur == 1:
        mc.file(pathREF + yumList[3] + "_allDYN." + "cur" + ".ma" , f=True , options='v=0' , type='mayaAscii' , pr=True , es=True )
    else:
        print 'Dose not make .cur'
    
    mc.delete(DYNREF)

def nClothnRigidExport(*arg):
    
    createnDYNPreset.doCreateDYNAttrPreset(type = "nCloth")
    createnDYNPreset.doCreateDYNAttrPreset(type = "nRigid")

### Create DYNREFExport Window ###
if mc.window ('DYNREFExport', q = True, ex = True):
    mc.deleteUI ('DYNREFExport')
        
window = mc.window ('DYNREFExport' , title = "Bout DYN_REF Export" )

mc.frameLayout( label='Requirement in Outliner' )
mc.columnLayout()
mc.text( label=" 1. FIX_GRP ")
mc.setParent( '..' )

mc.frameLayout( label='Export Selection' )
mc.columnLayout(adj = True)

mc.rowColumnLayout( numberOfRows = 1 , cat = [(1,'left',2),(2,'left',5),(3,'left' ,19),(4,'left',5),(5,'left' ,16),(6,'left' ,5)] )

mc.text( label=' Version ' )
Version = mc.intField(v = 1)
MakeCur = mc.checkBox(label = 'Make .cur' , v = True)

mc.setParent('..')

mc.button(label = 'DYN_REF Export', c = DYNREFExportFUN)
mc.button(label = 'nCloth nRigid Export', c = nClothnRigidExport)

mc.setParent('..')

mc.showWindow('DYNREFExport')
mc.window ('DYNREFExport', e = True , s = 0 , w = 250, h = 134 , tlc = [458,1100])