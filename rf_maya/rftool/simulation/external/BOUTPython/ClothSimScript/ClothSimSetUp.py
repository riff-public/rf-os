import maya.cmds as mc
import maya.mel as mm
import sys

import winUtility as util
reload ( util )

import nrbFlcSim_setup

def ClothSimSetting(*arg):

    # Naming #
    Naming = mc.textField( Name , q = True, tx = True)
    FirstFraming = mc.intField( FirstFrame , q = True, v = True)
    SecondFraming = mc.intField( SecondFrame , q = True, v = True)
    Wrapped = mc.textField( WrapObj , q = True, tx = True)
    CheckFix = mc.checkBox( FixGRP , q = True , v = True )
    CheckLayer = mc.checkBox( LayerBox , q = True , v = True )
    Vtx1st = mc.intField( Vtx1 , q = True, v = True)
    Vtx2nd = mc.intField( Vtx2 , q = True, v = True)
    Vtx3th = mc.intField( Vtx3 , q = True, v = True)
    Vtx4th = mc.intField( Vtx4 , q = True, v = True)    
        
    CIN = '_CIN'
    COL = '_COL'
    LOC = '_LOC'
    DYN = '_DYN'
    FIX = '_FIX'
    BSH = '_BSH'
        
    CharCINObj = mc.listRelatives( Naming , c = True , ad = True , type = 'transform')
    for i in range(len(CharCINObj)):
        mc.rename(CharCINObj[i], (CharCINObj[i] + CIN))
    CharCIN = mc.rename(Naming , (Naming + CIN))
    mc.setAttr( CharCIN + '.t', lock=True )
    mc.setAttr( CharCIN + '.r', lock=True )
    mc.setAttr( CharCIN + '.s', lock=True )
    
    CharCOLObj = mc.duplicate( CharCIN , rc = True)
    for each in CharCOLObj:
        FirstCharCol = each.split(CIN)
        mc.rename(each, (FirstCharCol[0] + COL))
    
    mc.setAttr( CharCIN + '.v', 0 )
    
    CharCOL = Naming + COL
    
    mc.blendShape( CharCIN , Naming + COL , o = 'world' , n = 'CIN_COL_BSH')
    mc.setAttr('CIN_COL_BSH.' + Naming + CIN , 1)
    mc.setKeyframe( 'CIN_COL_BSH.' + Naming + CIN, t=[FirstFraming,SecondFraming] )
    mc.currentTime( FirstFraming )
    mc.setAttr('CIN_COL_BSH.' + Naming + CIN , 0)
    
    nrbFlcSim_setup.snapNrbFlc_FUNC(charName = Naming , geoNrb = Wrapped + CIN , vtxList= [Vtx1st,Vtx2nd,Vtx3th,Vtx4th])
    
    mc.setAttr('NOTOUCH_GRP.visibility' , 0)
    
    # Create Locator #
    PosLoc = mc.spaceLocator( n = (Naming + LOC))
    PosCon = mc.parentConstraint( Naming + '_flc' , PosLoc , mo = 0)
    mc.delete(PosCon)
    mc.makeIdentity(PosLoc , apply=True)
    mc.setAttr( PosLoc[0] + 'Shape' + '.v', 0)
    mc.parent(CharCOL , PosLoc)
    
    # Create Sim Group #
    SimG = mc.createNode('transform' , n = 'SIM_GRP')
    NClothG = mc.createNode('transform' , n = 'nCloth_GRP')
    NRigidG = mc.createNode('transform' , n = 'nRigid_GRP')
    DycG = mc.createNode('transform' , n = 'DYC_GRP')

    if mc.objExists('DYN_GRP'):
        # Create Group #
        DynG = 'DYN_GRP'
        DynGList = mc.listRelatives( DynG , c = True )
        print DynGList
        mc.setAttr( DynG + '.t', lock=True )
        mc.setAttr( DynG + '.r', lock=True )
        mc.setAttr( DynG + '.s', lock=True )
        for i in range(len(DynGList)):
            mc.setAttr( DynGList[i] + '.t', lock=True )
            mc.setAttr( DynGList[i] + '.r', lock=True )
            mc.setAttr( DynGList[i] + '.s', lock=True )

        # FIX Group #
        if CheckFix == 1:
            FixGObj = mc.duplicate( DynG , rc = True)
            for each in FixGObj:
                FirstFixG = each.split(DYN)
                mc.rename(each, (FirstFixG[0] + FIX))
            FixG = mc.rename(DynG + '1' + FIX, 'FIX_GRP')
            FixGList = mc.listRelatives( FixG , c = True )
        
            for i in range(len(DynGList)):
                FixBSH = mc.blendShape( FixGList[i] , DynGList[i] , o = 'world' , n = FixGList[i] + BSH )
                mc.setAttr( FixBSH[0] + '.' + FixGList[i] , 1)
        
            mc.setAttr( FixG + '.t', lock=True )
            mc.setAttr( FixG + '.r', lock=True )
            mc.setAttr( FixG + '.s', lock=True )
            mc.setAttr( FixG + '.v', 0 )
            
        else:
            print 'No FIX_GRP'
        
        # Parent #
        mc.parent(DynG , PosLoc)
        if CheckFix == 1:
            mc.parent(FixG , PosLoc)
        else:
            print 'No FIX_GRP'
        
        # Create NCloth #
        for i in range(len(DynGList)):
            mc.select(DynGList[i])
            mm.eval('createNCloth 0;')
    
        for i in range(len(DynGList)):
            ObjShape = mc.listRelatives(DynGList[i])
            nClothList = mc.listConnections(ObjShape[0], type = 'nCloth')
            nClothNode = mc.rename(nClothList[0] , DynGList[i] + '_nCloth')
            nClothShape = mc.listRelatives(nClothNode)[0]
            conNode = mc.listConnections(nClothShape , t = 'mesh', sh = True)
            for eachNode in conNode:
                if 'outputCloth' in eachNode :
                    mc.rename(eachNode,nClothNode.split('_DYN')[0] + '_outputCloth')

        nClothNodeShape =  mc.ls( type = 'nCloth' )
        nClothNode = mc.listRelatives(nClothNodeShape ,p = True)
        mc.setAttr('nucleus1.startFrame', FirstFraming)
        mc.parent('nucleus1' , SimG)
        mc.parent(nClothNode , NClothG)
    
        # Create NRigid #
        mc.select(Wrapped + COL)
        mm.eval('makeCollideNCloth;')
        ObjWrappedCOLShape = mc.listRelatives(Wrapped + COL)[0]
        nRigidShape = mc.listConnections(ObjWrappedCOLShape, type = 'nRigid')[0]
        mc.rename(nRigidShape , Wrapped + COL + '_nRigid')
    
        nRigidNodeShape =  mc.ls( type = 'nRigid' )
        nRigidNode = mc.listRelatives(nRigidNodeShape ,p = True)
        mc.parent(nRigidNode , NRigidG)
        
        # Add Layer #
        if CheckLayer == 1:
            for i in range(len(DynGList)-1,-1,-1):
                mc.select(DynGList[i])
                mc.createDisplayLayer( noRecurse=True, name = DynGList[i] + '_layer')
        else:
            print 'Not assign Layer'
        
    else:
        print 'pass'

    # Parent #
    LocCon = mc.parentConstraint( Naming + '_flc' , PosLoc , mo = 1 , n = (PosLoc[0] + '_ParCon'))
    mc.parent(NClothG , SimG)
    mc.parent(NRigidG , SimG)
    mc.parent(DycG , SimG)
    
    mc.select( clear=True )
    

def getSelection(*arg):
    sel = mc.ls(os = True)
    vtxDict = {}
    num = 1

    for each in sel :
        vtx = each.split('.vtx[')[1]
        vtx = vtx.split(']')[0]
        
        if ':' in vtx :
            vertices  = vtx.split ( ':' ) ;
            for vertex in vertices :
                vtxDict[num] = vertex
                num += 1
        else :
            vtxDict[num] = vtx
            num += 1

    print vtxDict
    return vtxDict

def get(*arg):
    vertex = getSelection()
    mc.intField('vtrLFTUP_IF', e = True , v = int(vertex[1]))
    mc.intField('vtrRGTUP_IF', e = True , v = int(vertex[2]))
    mc.intField('vtrLFTLOW_IF', e = True , v = int(vertex[3]))
    mc.intField('vtrRGTLOW_IF', e = True , v = int(vertex[4]))

### CreateClothSimSetUp Window ###
if mc.window ('ClothSimSetUp', q = True, ex = True):
    mc.deleteUI ('ClothSimSetUp')
        
window = mc.window ('ClothSimSetUp' , title = "Bout ClothSim Set Up" )

mc.frameLayout( label='Requirement in Outliner' )
mc.columnLayout()
mc.text( label=" 1. Character's name")
mc.text( label=' 2. DYN_GRP' )
mc.text( label='     2.1 Cloth_DYN' )
mc.setParent( '..' )

mc.frameLayout( label='Naming Character & Set Key BlendShape' )
mc.columnLayout(adj = True)

mc.rowColumnLayout( numberOfRows = 1 , cat = [(1,'left',3),(2,'left',5),(3,'left' ,10),(4,'left',5),(5,'left' ,18),(6,'left' ,5)] )

mc.text( label="Char's Name:" )
Name = mc.textField(text = 'Character')

mc.text( label=' 1stFrame ' )
FirstFrame = mc.intField(v = 1)

mc.text( label=' 2ndFrame ' )
SecondFrame = mc.intField(v = 10)

mc.setParent('..')
mc.setParent('..')

mc.frameLayout( label='Naming Wrap Object & Points of Vertex' )

mc.rowLayout( numberOfColumns = 9, adjustableColumn = 9 , cat = [(1,'left',5),(2,'left',2),(3,'left' ,10),(4,'left',10),(5,'left' ,5),(6,'left' ,5),(7,'left' ,5)])

mc.text( label=' Wrap Object :' )
WrapObj = mc.textField(text = 'allBody_retopo' )
FixGRP = mc.checkBox(label = 'FIX_GRP' , v = True)
LayerBox = mc.checkBox(label = 'Layer' , v = True)
mc.separator( height = 50 , style='in' , hr = False )

mc.rowColumnLayout(nc=3,columnWidth=[(1, 40), (2, 65), (3, 65)])
mc.text( label='Vertex :' )
Vtx1 = mc.intField('vtrLFTUP_IF', v = 1 , w = 65)
Vtx2 = mc.intField('vtrRGTUP_IF', v = 2 , w = 65)
mc.text('')
Vtx3 = mc.intField('vtrLFTLOW_IF', v = 3 , w = 65)
Vtx4 = mc.intField('vtrRGTLOW_IF', v = 4 , w = 65)

mc.setParent('..')
mc.button(label = 'getVertexID' , c = util.Callback(get))
mc.text( label='  ' )

mc.setParent('..')
mc.setParent('..')

mc.button(label = 'Create ClothSim Set Up', c = ClothSimSetting)

mc.setParent('..')

mc.showWindow('ClothSimSetUp')
mc.window ('ClothSimSetUp', e = True , s = 0 , w = 528, h = 240 , tlc = [500,800])