import maya.cmds as mc

def DYNCleaner(*arg):
    DYNG = 'DYN_GRP'
    FIXG = 'FIX_GRP'
    DYCG = 'DYC_GRP'
    #DYNCING = 'DYN_CIN_GRP'
    
    DYNGH = mc.listRelatives(DYNG , c = True)
    #FIXGH = mc.listRelatives(FIXG , c = True)
    DYCGH = mc.listRelatives(DYCG , c = True)
    
    mc.delete(DYCGH)
    mc.delete(DYNGH)
    mc.delete(FIXG)
    
    # Delete Layer
    for each in DYNGH:
        mc.delete(each + '_layer')
    
    #if mc.objExists(DYNCING):
        #DYNCINGH = mc.listRelatives(DYNCING , c = True)
        #mc.delete(DYNCINGH)
