import maya.cmds as mc
import maya.mel as mm

def main(*arg):
    RIV = mm.eval('rivet')
    TrackRIV = mc.rename( RIV , 'track_RIV' )

    RivT = mc.xform(TrackRIV , q = True , t = True , ws = True)
    RivR = mc.xform(TrackRIV , q = True , ro = True , ws = True)

    FWDloc = mc.spaceLocator(n = 'FWD_LOC')
    BWDloc = mc.spaceLocator(n = 'BWD_LOC')

    mc.setAttr( FWDloc[0] + '.tx'  , RivT[0])
    mc.setAttr( FWDloc[0] + '.ty'  , RivT[1])
    mc.setAttr( FWDloc[0] + '.tz'  , RivT[2])
    mc.setAttr( FWDloc[0] + '.rx'  , RivR[0])
    mc.setAttr( FWDloc[0] + '.ry'  , RivR[1])
    mc.setAttr( FWDloc[0] + '.rz'  , RivR[2])
    mc.setAttr( BWDloc[0] + '.tx'  , RivT[0])
    mc.setAttr( BWDloc[0] + '.ty'  , RivT[1])
    mc.setAttr( BWDloc[0] + '.tz'  , RivT[2])
    mc.setAttr( BWDloc[0] + '.rx'  , RivR[0])
    mc.setAttr( BWDloc[0] + '.ry'  , RivR[1])
    mc.setAttr( BWDloc[0] + '.rz'  , RivR[2])

    mc.makeIdentity(FWDloc, t=1 , r=1 , s=1 , apply=True)
    mc.makeIdentity(BWDloc, t=1 , r=1 , s=1 , apply=True)

    FWDParent = mc.parentConstraint( TrackRIV , FWDloc , mo = 1)
    BWDParent = mc.parentConstraint( TrackRIV , BWDloc , mo = 1)

    Min = mc.playbackOptions( q = True , min = True)
    Max = mc.playbackOptions( q = True , max = True)

    mc.bakeResults( FWDloc, t=(Min,Max))
    mc.bakeResults( BWDloc, t=(Min,Max))

    mc.cutKey( FWDloc , time=(Min,Max) , at = 'sx' )
    mc.cutKey( FWDloc , time=(Min,Max) , at = 'sy' )
    mc.cutKey( FWDloc , time=(Min,Max) , at = 'sz' )
    mc.cutKey( FWDloc , time=(Min,Max) , at = 'v' )
    mc.cutKey( BWDloc , time=(Min,Max) , at = 'sx' )
    mc.cutKey( BWDloc , time=(Min,Max) , at = 'sy' )
    mc.cutKey( BWDloc , time=(Min,Max) , at = 'sz' )
    mc.cutKey( BWDloc , time=(Min,Max) , at = 'v' )

    mc.selectKey( BWDloc, time=(Min,Max) )
    mc.scaleKey( timeScale = 1.0 , timePivot = 0 , fs = 1 , fp = 0 , vs = -1.0 , vp = 0)

    mc.delete(FWDParent)
    mc.delete(BWDParent)