import maya.cmds as mc

def main(*arg):
    sel = mc.ls(sl = True)
    BSHnode = mc.blendShape( sel[0] , sel[1] , o = 'world' , n = sel[1] + '_BSN')
    objBSH = mc.listAttr(BSHnode , k=True, m=True)
    mc.setAttr( BSHnode[0] + '.' + objBSH[1] , 1)