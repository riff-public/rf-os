import maya.cmds as mc

'''
def ClothSimSetUp(*arg):
    import ClothSimScript.ClothSimSetUp as bpcs
    reload(bpcs)

def AssignDNshader(*arg):
    import ClothSimScript.AssignDNshader as badns
    reload(badns)
'''

def FixShapeDeformed(*arg):
    import ClothSimScript.FixShapeDeformed as bfsdf
    reload(bfsdf)
        
def DYNCleanUpShape(*arg):
    import ClothSimScript.DYNCleanUpShape as bdcus
    bdcus.DYNCleanUpShapeFUN(*arg)
    
def GetAndRandomOffsetABC(*arg):
    import ClothSimScript.GetAndRandomOffsetABC as bgaro
    reload(bgaro)

'''
def EnableClothSim(*arg):
    import ClothSimScript.EnableDisableClothSim as bedcs
    reload(bedcs)
    bedcs.EnableNCloth(*arg)    

def DisableClothSim(*arg):
    import ClothSimScript.EnableDisableClothSim as bedcs
    reload(bedcs)
    bedcs.DisableNCloth(*arg)

def TrackTranslate(*arg):
    import ClothSimScript.TrackTranslate as btt
    reload(btt)


def RenameNCloth(*arg):
    import ClothSimScript.RenameNClothNRigid as brncr
    reload(brncr)
    brncr.main(*arg)

def RenameNConstraint(*arg):
    import ClothSimScript.RenameNConstraint as brnct
    reload(brnct)
    brnct.main(*arg)
'''

def ReOrder(*arg):
    import ClothSimScript.ReOrder as bro
    reload(bro)
    bro.main(*arg)

'''
def EditStartBSH(*arg):
    import ClothSimScript.EditStartBSH as besb
    reload(besb)
'''

def AssignLayer(*arg):
    import ClothSimScript.AssignObjLayer as baol
    reload(baol)
    baol.main(*arg)

'''
def BSHObject(*arg):
    import ClothSimScript.BlendShapeObject as bbsh
    reload(bbsh)
    bbsh.main(*arg)
'''

def UnlockSoftenEdge(*arg):
    import ClothSimScript.UnlockSoftenEdge as buse
    reload(buse)
    buse.main(*arg)
    
def DeletePolyAV(*arg):
    import ClothSimScript.DeletePolyAV as bdav
    reload(bdav)
    bdav.main(*arg)
    
def MakeDYNSetUp(*arg):
    import ClothSimScript.MakeDYNSetUp as bmdyn
    reload(bmdyn)

def MakeNRigidSetUp(*arg):
    import ClothSimScript.MakeNRigidSetUp as bmnrg
    reload(bmnrg)
    bmnrg.main(*arg)
    
if mc.window ('BOUT_ClothSimCondo', q = True, ex = True):
    mc.deleteUI ('BOUT_ClothSimCondo')
    
window = mc.window ('BOUT_ClothSimCondo' , title = "BOUT ClothSimulation" , sizeable = True )
mc.window ( 'BOUT_ClothSimCondo' , e = True , w = 210 )
mc.columnLayout( adj = True )

#mc.button( label='ClothSimSetUp', c =  ClothSimSetUp)
#mc.button( label='AssignDNshader', c =  AssignDNshader)
#mc.button( label='FixShapeDeformed', c =  FixShapeDeformed)
#mc.button( label='DYN CleanUp Shape', c =  DYNCleanUpShape)
#mc.button( label='GetAndRandomOffsetABC', c =  GetAndRandomOffsetABC)
#mc.button( label='EnableClothSim', c =  EnableClothSim)
#mc.button( label='DisableClothSim', c =  DisableClothSim)
#mc.button( label='Track Translate', c =  TrackTranslate)
#mc.button( label='Rename nCloth nRigid', c =  RenameNCloth)
#mc.button( label='Rename nConstraint', c =  RenameNConstraint)
mc.button( label='ReOrder', c =  ReOrder)
#mc.button( label='EditStartBSH', c =  EditStartBSH)
mc.button( label='Assign Object layer', c =  AssignLayer)
#mc.button( label='BlendShape Object', c =  BSHObject)
mc.button( label='UnlockNormals and SoftenEdge', c =  UnlockSoftenEdge)
mc.button( label='Delete Average Vertex', c =  DeletePolyAV)
#mc.button( label='Make DYN SetUp', c =  MakeDYNSetUp)
#mc.button( label='Make nRigid SetUp', c =  MakeNRigidSetUp)

mc.showWindow('BOUT_ClothSimCondo')
#mc.window ('BOUT_ClothSimCondo', e = True, w = 260, h = 417 , tlc = [400,838])