import maya.cmds as mc


def main(*arg):

    PosLoc = mc.spaceLocator( n = 'CCC_LOC')
    ColG = mc.createNode('transform' , n = 'COL_GRP')
    DynG = mc.createNode('transform' , n = 'DYN_GRP')

    mc.parent(ColG , PosLoc)
    mc.parent(DynG , PosLoc)

    SimG = mc.createNode('transform' , n = 'SIM_GRP')
    NClothG = mc.createNode('transform' , n = 'nCloth_GRP')
    NRigidG = mc.createNode('transform' , n = 'nRigid_GRP')
    DmcG = mc.createNode('transform' , n = 'DYC_GRP')

    mc.parent(NClothG , SimG)
    mc.parent(NRigidG , SimG)
    mc.parent(DmcG , SimG)

    COutG = mc.createNode('transform' , n = 'CCC_DYN_COUT')