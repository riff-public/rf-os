//Maya ASCII 2014 scene
//Name: hairWindBox.v1.ma
//Last modified: ��,  2�� 17, 2015 10:23:27 ����
//Codeset: EUC-JP
requires maya "2014";
requires -nodeType "mentalrayFramebuffer" -nodeType "mentalrayOutputPass" -nodeType "mentalrayRenderPass"
		 -nodeType "mentalrayUserBuffer" -nodeType "mentalraySubdivApprox" -nodeType "mentalrayCurveApprox"
		 -nodeType "mentalraySurfaceApprox" -nodeType "mentalrayDisplaceApprox" -nodeType "mentalrayOptions"
		 -nodeType "mentalrayGlobals" -nodeType "mentalrayItemsList" -nodeType "mentalrayShader"
		 -nodeType "mentalrayUserData" -nodeType "mentalrayText" -nodeType "mentalrayTessellation"
		 -nodeType "mentalrayPhenomenon" -nodeType "mentalrayLightProfile" -nodeType "mentalrayVertexColors"
		 -nodeType "mentalrayIblShape" -nodeType "mapVizShape" -nodeType "mentalrayCCMeshProxy"
		 -nodeType "cylindricalLightLocator" -nodeType "discLightLocator" -nodeType "rectangularLightLocator"
		 -nodeType "sphericalLightLocator" -nodeType "abcimport" -nodeType "mia_physicalsun"
		 -nodeType "mia_physicalsky" -nodeType "mia_material" -nodeType "mia_material_x" -nodeType "mia_roundcorners"
		 -nodeType "mia_exposure_simple" -nodeType "mia_portal_light" -nodeType "mia_light_surface"
		 -nodeType "mia_exposure_photographic" -nodeType "mia_exposure_photographic_rev" -nodeType "mia_lens_bokeh"
		 -nodeType "mia_envblur" -nodeType "mia_ciesky" -nodeType "mia_photometric_light"
		 -nodeType "mib_texture_vector" -nodeType "mib_texture_remap" -nodeType "mib_texture_rotate"
		 -nodeType "mib_bump_basis" -nodeType "mib_bump_map" -nodeType "mib_passthrough_bump_map"
		 -nodeType "mib_bump_map2" -nodeType "mib_lookup_spherical" -nodeType "mib_lookup_cube1"
		 -nodeType "mib_lookup_cube6" -nodeType "mib_lookup_background" -nodeType "mib_lookup_cylindrical"
		 -nodeType "mib_texture_lookup" -nodeType "mib_texture_lookup2" -nodeType "mib_texture_filter_lookup"
		 -nodeType "mib_texture_checkerboard" -nodeType "mib_texture_polkadot" -nodeType "mib_texture_polkasphere"
		 -nodeType "mib_texture_turbulence" -nodeType "mib_texture_wave" -nodeType "mib_reflect"
		 -nodeType "mib_refract" -nodeType "mib_transparency" -nodeType "mib_continue" -nodeType "mib_opacity"
		 -nodeType "mib_twosided" -nodeType "mib_refraction_index" -nodeType "mib_dielectric"
		 -nodeType "mib_ray_marcher" -nodeType "mib_illum_lambert" -nodeType "mib_illum_phong"
		 -nodeType "mib_illum_ward" -nodeType "mib_illum_ward_deriv" -nodeType "mib_illum_blinn"
		 -nodeType "mib_illum_cooktorr" -nodeType "mib_illum_hair" -nodeType "mib_volume"
		 -nodeType "mib_color_alpha" -nodeType "mib_color_average" -nodeType "mib_color_intensity"
		 -nodeType "mib_color_interpolate" -nodeType "mib_color_mix" -nodeType "mib_color_spread"
		 -nodeType "mib_geo_cube" -nodeType "mib_geo_torus" -nodeType "mib_geo_sphere" -nodeType "mib_geo_cone"
		 -nodeType "mib_geo_cylinder" -nodeType "mib_geo_square" -nodeType "mib_geo_instance"
		 -nodeType "mib_geo_instance_mlist" -nodeType "mib_geo_add_uv_texsurf" -nodeType "mib_photon_basic"
		 -nodeType "mib_light_infinite" -nodeType "mib_light_point" -nodeType "mib_light_spot"
		 -nodeType "mib_light_photometric" -nodeType "mib_cie_d" -nodeType "mib_blackbody"
		 -nodeType "mib_shadow_transparency" -nodeType "mib_lens_stencil" -nodeType "mib_lens_clamp"
		 -nodeType "mib_lightmap_write" -nodeType "mib_lightmap_sample" -nodeType "mib_amb_occlusion"
		 -nodeType "mib_fast_occlusion" -nodeType "mib_map_get_scalar" -nodeType "mib_map_get_integer"
		 -nodeType "mib_map_get_vector" -nodeType "mib_map_get_color" -nodeType "mib_map_get_transform"
		 -nodeType "mib_map_get_scalar_array" -nodeType "mib_map_get_integer_array" -nodeType "mib_fg_occlusion"
		 -nodeType "mib_bent_normal_env" -nodeType "mib_glossy_reflection" -nodeType "mib_glossy_refraction"
		 -nodeType "mib_illum_hair_x" -nodeType "builtin_bsdf_architectural" -nodeType "builtin_bsdf_architectural_comp"
		 -nodeType "builtin_bsdf_carpaint" -nodeType "builtin_bsdf_ashikhmin" -nodeType "builtin_bsdf_lambert"
		 -nodeType "builtin_bsdf_mirror" -nodeType "builtin_bsdf_phong" -nodeType "contour_store_function"
		 -nodeType "contour_store_function_simple" -nodeType "contour_contrast_function_levels"
		 -nodeType "contour_contrast_function_simple" -nodeType "contour_shader_simple" -nodeType "contour_shader_silhouette"
		 -nodeType "contour_shader_maxcolor" -nodeType "contour_shader_curvature" -nodeType "contour_shader_factorcolor"
		 -nodeType "contour_shader_depthfade" -nodeType "contour_shader_framefade" -nodeType "contour_shader_layerthinner"
		 -nodeType "contour_shader_widthfromcolor" -nodeType "contour_shader_widthfromlightdir"
		 -nodeType "contour_shader_widthfromlight" -nodeType "contour_shader_combi" -nodeType "contour_only"
		 -nodeType "contour_composite" -nodeType "contour_ps" -nodeType "mi_metallic_paint"
		 -nodeType "mi_metallic_paint_x" -nodeType "mi_bump_flakes" -nodeType "mi_car_paint_phen"
		 -nodeType "mi_metallic_paint_output_mixer" -nodeType "mi_car_paint_phen_x" -nodeType "physical_lens_dof"
		 -nodeType "physical_light" -nodeType "dgs_material" -nodeType "dgs_material_photon"
		 -nodeType "dielectric_material" -nodeType "dielectric_material_photon" -nodeType "oversampling_lens"
		 -nodeType "path_material" -nodeType "parti_volume" -nodeType "parti_volume_photon"
		 -nodeType "transmat" -nodeType "transmat_photon" -nodeType "mip_rayswitch" -nodeType "mip_rayswitch_advanced"
		 -nodeType "mip_rayswitch_environment" -nodeType "mip_card_opacity" -nodeType "mip_motionblur"
		 -nodeType "mip_motion_vector" -nodeType "mip_matteshadow" -nodeType "mip_cameramap"
		 -nodeType "mip_mirrorball" -nodeType "mip_grayball" -nodeType "mip_gamma_gain" -nodeType "mip_render_subset"
		 -nodeType "mip_matteshadow_mtl" -nodeType "mip_binaryproxy" -nodeType "mip_rayswitch_stage"
		 -nodeType "mip_fgshooter" -nodeType "mib_ptex_lookup" -nodeType "misss_physical"
		 -nodeType "misss_physical_phen" -nodeType "misss_fast_shader" -nodeType "misss_fast_shader_x"
		 -nodeType "misss_fast_shader2" -nodeType "misss_fast_shader2_x" -nodeType "misss_skin_specular"
		 -nodeType "misss_lightmap_write" -nodeType "misss_lambert_gamma" -nodeType "misss_call_shader"
		 -nodeType "misss_set_normal" -nodeType "misss_fast_lmap_maya" -nodeType "misss_fast_simple_maya"
		 -nodeType "misss_fast_skin_maya" -nodeType "misss_fast_skin_phen" -nodeType "misss_fast_skin_phen_d"
		 -nodeType "misss_mia_skin2_phen" -nodeType "misss_mia_skin2_phen_d" -nodeType "misss_lightmap_phen"
		 -nodeType "misss_mia_skin2_surface_phen" -nodeType "surfaceSampler" -nodeType "mib_data_bool"
		 -nodeType "mib_data_int" -nodeType "mib_data_scalar" -nodeType "mib_data_vector"
		 -nodeType "mib_data_color" -nodeType "mib_data_string" -nodeType "mib_data_texture"
		 -nodeType "mib_data_shader" -nodeType "mib_data_bool_array" -nodeType "mib_data_int_array"
		 -nodeType "mib_data_scalar_array" -nodeType "mib_data_vector_array" -nodeType "mib_data_color_array"
		 -nodeType "mib_data_string_array" -nodeType "mib_data_texture_array" -nodeType "mib_data_shader_array"
		 -nodeType "mib_data_get_bool" -nodeType "mib_data_get_int" -nodeType "mib_data_get_scalar"
		 -nodeType "mib_data_get_vector" -nodeType "mib_data_get_color" -nodeType "mib_data_get_string"
		 -nodeType "mib_data_get_texture" -nodeType "mib_data_get_shader" -nodeType "mib_data_get_shader_bool"
		 -nodeType "mib_data_get_shader_int" -nodeType "mib_data_get_shader_scalar" -nodeType "mib_data_get_shader_vector"
		 -nodeType "mib_data_get_shader_color" -nodeType "user_ibl_env" -nodeType "user_ibl_rect"
		 -nodeType "xgen_geo" -nodeType "xgen_seexpr" -nodeType "xgen_scalar_to_integer" -nodeType "xgen_integer_to_vector"
		 -nodeType "xgen_scalar_to_vector" -nodeType "xgen_boolean_to_vector" -nodeType "xgen_boolean_switch"
		 -nodeType "xgen_tube_normals" -nodeType "xgen_hair_phen" -nodeType "sqtMr3DMotionVectorShader"
		 -nodeType "sqtMrGeoDepthShader" -nodeType "sqtMrGeoMassiveExtShader" -nodeType "sqtMrGeoParticlesExtShader"
		 -nodeType "sqtMrGlossyReflectionShader" -nodeType "sqtMrHairMatteShader" -nodeType "sqtMrHairShader"
		 -nodeType "sqtMrParticlesExtShader" -nodeType "sqtMrVoxelCacheBakeShader" -nodeType "sqtMrVoxelCacheSampleShader"
		 -nodeType "sqtMrVoxelCacheReadShader" -nodeType "sqtMrCameraData" -nodeType "sqtMrMotionVectorShader"
		 -nodeType "sqtMrDepthShader" -nodeType "sqtMrHairMotionVectorShader" -nodeType "sqtMrHairDepthShader"
		 -nodeType "sqtMrGeoHairShader" -nodeType "sqtMrHairExtShader" -nodeType "sqtMrHairMotionVectorExtShader"
		 -nodeType "sqtMrHairDepthExtShader" -nodeType "sqtMrHairMatteExtShader" -nodeType "sqtMrHairTextureShader"
		 -nodeType "sqtMrGeoParticlesShader" -nodeType "sqtMrParticlesShader" -nodeType "sqtMrParticlesTextureShader"
		 -nodeType "sqtMrGeoPathShader" -nodeType "sqtMrBumpShader" -nodeType "sqtMrBumpExtShader"
		 -nodeType "sqtMrTextureRemapShader" -nodeType "sqtMrTextureShader" -nodeType "sqtMrBumpTextureExtShader"
		 -nodeType "sqtMrBumpTextureShader" -nodeType "sqtMrDisplacementTextureShader" -nodeType "sqtMrBumpGradientShader"
		 -nodeType "sqtMrMultiTextureShader" -nodeType "sqtMrMultiTextureExtShader" -nodeType "sqtMrMultiBumpTextureShader"
		 -nodeType "sqtMrMultiBumpTextureExtShader" -nodeType "sqtMrMultiDisplacementTextureShader"
		 -nodeType "sqtMrMultiDisplacementTextureExtShader" -nodeType "sqtMrProjectionTextureShader"
		 -nodeType "sqtMrProjectionBumpTextureShader" -nodeType "sqtMrWaveColorShader" -nodeType "sqtMrColorCompoShader"
		 -nodeType "sqtMrColorLayerShader" -nodeType "sqtMrFresnelShader" -nodeType "sqtMrRaySwitchShader"
		 -nodeType "sqtMrGammaShader" -nodeType "sqtMrVisibilityShader" -nodeType "sqtMrRaySwitchExtShader"
		 -nodeType "sqtMrFrameBufferShader" -nodeType "sqtMrFrameBufferExtShader" -nodeType "sqtMrToneMapSimpleShader"
		 -nodeType "sqtMrSphericalLensShader" -nodeType "sqtMrMaterialShader" -nodeType "sqtMrMaterialExtShader"
		 -nodeType "sqtMrMaterialExtXShader" -nodeType "sqtMrMaterialExtXCompoShader" -nodeType "sqtMrMatteShader"
		 -nodeType "sqtMrMatteExtShader" -nodeType "sqtMrTranslucenceShader" -nodeType "sqtMrAllInOneShader"
		 -nodeType "sqtMrCameraTemperatureShader" -nodeType "sqtMrCurvatureShader" -nodeType "sqtMrShadowShader"
		 -nodeType "sqtMrPhotonShader" -nodeType "sqtMrLightShader" -nodeType "sqtMrLightExtShader"
		 -nodeType "sqtMrAreaLightShader" -nodeType "sqtMrAreaLightExtShader" -nodeType "sqtMrEnvironmentShader"
		 -nodeType "sqtMrEnvironmentExtShader" -nodeType "sqtMrEnvironmentSwitchShader" -nodeType "sqtMrMultiEnvironmentShader"
		 -nodeType "sqtMrEnvironmentSwitchExtShader" -nodeType "sqtMrShadowMaskShader" -nodeType "sqtMrDepthLightmapShader"
		 -nodeType "sqtMrLightFogShader" -nodeType "sqtMrVolumeSimpleShader" -nodeType "sqtMrVolumeShader"
		 -nodeType "sqtMrVolumeBufferedShader" -nodeType "sqtMrSSSLightmapShader" -nodeType "sqtMrSSSLightmapExtShader"
		 -nodeType "sqtMrSSSShader" -nodeType "sqtMrSSSExtShader" -nodeType "sqtMrSSSSimpleLightmapShader"
		 -nodeType "sqtMrSSSSimpleLightmapExtShader" -nodeType "sqtMrLeatherShader" -nodeType "sqtMrTextileShader"
		 -nodeType "sqtMrFlakeShader" -nodeType "sqtMrDOFDataShader" -nodeType "sqtMrHairDOFDataShader"
		 -nodeType "sqtMrObjectData" -nodeType "sqtMrColorVariationShader" -nodeType "sqtMrColorCorrectionShader"
		 -nodeType "sqtMrParallaxShader" -nodeType "sqtMrParallaxSimpleShader" -nodeType "sqtMrDirtShader"
		 -nodeType "sqtMrColorCorrectionCompoShader" -nodeType "sqtMrColorCorrectionExtShader"
		 -nodeType "sqtMrScatterShader" -nodeType "sqtMrTranslucenceExtShader" -nodeType "sqtMrLightBlockerShader"
		 -nodeType "sqtMrSimplePassesShader" -nodeType "sqtMrDebugPassesShader" -nodeType "sqtMrFluidBumpTextureShader"
		 -nodeType "sqtMrColorConversionShader" -nodeType "sqtMrFlowShader" -nodeType "sqtMrSurfaceFractalShader"
		 -nodeType "sqtMrRippleShader" -nodeType "sqtMrMiaCompatibilityShader" -nodeType "sqtMrColorCompoCompatibilityShader"
		 -nodeType "realflowMeltShaderMr" -nodeType "realflowVelocityShader" -nodeType "mia_material_x_passes"
		 -nodeType "mi_metallic_paint_x_passes" -nodeType "mi_car_paint_phen_x_passes" -nodeType "misss_fast_shader_x_passes"
		 -dataType "byteArray" "Mayatomr" "2014.0 - 3.11.1.13 ";
requires "TechCgfxShader" "1.2.20110111";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014";
fileInfo "cutIdentifier" "201310082153-890429";
fileInfo "osv" "Linux 2.6.32-220.el6.x86_64 #1 SMP Tue Dec 6 19:48:22 GMT 2011 x86_64";
createNode transform -n "dynamicsGrp";
createNode transform -n "allWindGrp" -p "dynamicsGrp";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on ".s";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr -l on ".sh";
	setAttr -l on ".ra";
	setAttr -l on -k on ".it";
	setAttr ".dsp" yes;
createNode transform -n "allWindTranslate" -p "allWindGrp";
	setAttr -l on ".r";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on ".s";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr -l on ".sh";
	setAttr -l on ".ra";
	setAttr -l on ".it";
createNode transform -n "allWindRotate" -p "allWindTranslate";
	setAttr -l on ".t";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -l on -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on ".s";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr -l on ".sh";
	setAttr -l on ".ra";
	setAttr -k on ".it";
createNode transform -n "allWindScale" -p "allWindRotate";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -l on ".r" -type "double3" 0 90 0 ;
	setAttr -l on ".r";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -l on ".ro";
	setAttr ".s" -type "double3" 8 4 4 ;
	setAttr -l on ".sh";
	setAttr -l on ".ra";
	setAttr -l on ".it";
createNode mesh -n "allWindScaleShape" -p "allWindScale";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 17 ".uvst[0].uvsp[0:16]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1 0 0 1 1.4894599e-16 0.40000001 0.80000001;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".bnr" 0;
	setAttr -s 11 ".vt[0:10]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 0.41999999 0.5 0.47999999
		 0.47999999 0.5 0.44999999 0.41999999 0.5 0.41999999;
	setAttr -s 15 ".ed[0:14]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 9 0 9 10 0 10 8 0;
	setAttr -s 7 -ch 27 ".fc[0:6]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 3 12 13 14
		mu 0 3 14 15 16;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".ndt" 0;
	setAttr ".tgsp" 1;
	setAttr ".vnm" 0;
createNode transform -n "allWindParticle" -p "allWindScale";
	setAttr ".tmp" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode particle -n "allWindParticleShape" -p "allWindParticle";
	addAttr -ci true -sn "lifespanPP" -ln "lifespanPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "lifespanPP0" -ln "lifespanPP0" -dt "doubleArray";
	addAttr -ci true -sn "lifespan" -ln "lifespan" -at "double";
	addAttr -ci true -sn "nsp" -ln "minSpeed" -dv 0.5 -at "double";
	addAttr -ci true -sn "xsp" -ln "maxSpeed" -dv 1 -at "double";
	addAttr -ci true -sn "wav" -ln "waver" -dv 0.5 -at "double";
	addAttr -ci true -sn "dsp" -ln "dispersal" -dv 5 -min 0 -at "double";
	addAttr -ci true -sn "tof" -ln "timeOffset" -at "double";
	addAttr -ci true -sn "fps" -ln "fps" -dv 30 -at "double";
	addAttr -is true -ci true -sn "tailSize" -ln "tailSize" -dv 1 -at "float";
	setAttr -k off ".v";
	setAttr ".gf" -type "Int32Array" 0 ;
	setAttr ".pos0" -type "vectorArray" 50 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 ;
	setAttr ".vel0" -type "vectorArray" 50 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 ;
	setAttr ".acc0" -type "vectorArray" 50 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 ;
	setAttr ".usc" yes;
	setAttr ".scp" -type "string" "CHgladiolusShirtsMainWork_startup";
	setAttr ".mas0" -type "doubleArray" 50 1 1 1 1 1 1
		 1 1 1 1 1 1 1 1 1 1 1 1
		 1 1 1 1 1 1 1 1 1 1 1 1
		 1 1 1 1 1 1 1 1 1 1 1 1
		 1 1 1 1 1 1 1 1 ;
	setAttr ".id0" -type "doubleArray" 50 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 16 17
		 18 19 20 21 22 23 24 25 26 27 28 29
		 30 31 32 33 34 35 36 37 38 39 40 41
		 42 43 44 45 46 47 48 49 ;
	setAttr ".nid" 50;
	setAttr ".nid0" 50;
	setAttr ".bt0" -type "doubleArray" 50 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004
		 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004
		 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004
		 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004
		 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 -0.80000000000000004 ;
	setAttr ".ag0" -type "doubleArray" 50 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 ;
	setAttr -k off ".dve";
	setAttr -k off ".lfm";
	setAttr -k off ".lfr";
	setAttr -k off ".ead";
	setAttr -s 7 ".xi";
	setAttr -s 2 ".xo";
	setAttr ".irbx" -type "string" (
		"int   $ID         = .I[0];\nfloat $minSp      = .I[1];\nfloat $maxSp      = .I[2];\nfloat $fps        = .I[3];\nfloat $waver      = .I[4];\nfloat $dispersal  = .I[5];\nfloat $timeOffset = .I[6];\nfloat $magicID = ( noise( $ID + 1) / 2.0 + 0.5) * $dispersal;\nfloat $spRatio = noise( $magicID) / 2.0 + 0.5;\nfloat $curSp   = $spRatio * ( $maxSp - $minSp) + $minSp;\nfloat $absSp   = abs( $curSp);\nfloat $tmpPos[], $num[];\nvector $pos[], $vel;\n// Get particle position.\nif( $fps == 0){ $fps = 1; }\n$num[0] = $magicID + time - ( 1.0 / $fps) + $timeOffset;\n$num[1] = $magicID + time + $timeOffset;\nfor( $i = 0; $i < size( $num); $i++){\n    $tmpPos[0] = ( $num[$i] * $absSp + $magicID) % 1.0 - 0.5;\n    $tmpPos[1] = noise( $num[$i] * $waver + $magicID * 23.4567) / 2.0;\n    $tmpPos[2] = noise( $num[$i] * $waver + $magicID * 34.5678) / 2.0;\n    $pos[$i] = << $tmpPos[0], $tmpPos[1], $tmpPos[2]>>;\n}\n// Get velocity.\n$vel = $pos[1] - $pos[0];\nif( $vel.x < 0){\n    vector $tmpVec = $pos[1];\n    $vel = << $tmpVec.x + 0.5, $vel.y, $vel.z>>;\n}\n"
		+ "// Reverse.\n$pos[1] = $pos[1] * (( $curSp < 0) ? -1.0 : 1.0);\n$vel *= ( $curSp < 0) ? -1.0 : 1.0;\n.O[0] = $pos[1] - $vel;\n.O[1] = $vel * $fps;");
	setAttr ".irax" -type "string" "";
	setAttr ".icx" -type "string" (
		"int   $ID         = .I[0];\nfloat $minSp      = .I[1];\nfloat $maxSp      = .I[2];\nfloat $fps        = .I[3];\nfloat $waver      = .I[4];\nfloat $dispersal  = .I[5];\nfloat $timeOffset = .I[6];\nfloat $magicID = ( noise( $ID + 1) / 2.0 + 0.5) * $dispersal;\nfloat $spRatio = noise( $magicID) / 2.0 + 0.5;\nfloat $curSp   = $spRatio * ( $maxSp - $minSp) + $minSp;\nfloat $absSp   = abs( $curSp);\nfloat $tmpPos[], $num[];\nvector $pos[], $vel;\n// Get particle position.\nif( $fps == 0){ $fps = 1; }\n$num[0] = $magicID + time - ( 1.0 / $fps) + $timeOffset;\n$num[1] = $magicID + time + $timeOffset;\nfor( $i = 0; $i < size( $num); $i++){\n    $tmpPos[0] = ( $num[$i] * $absSp + $magicID) % 1.0 - 0.5;\n    $tmpPos[1] = noise( $num[$i] * $waver + $magicID * 23.4567) / 2.0;\n    $tmpPos[2] = noise( $num[$i] * $waver + $magicID * 34.5678) / 2.0;\n    $pos[$i] = << $tmpPos[0], $tmpPos[1], $tmpPos[2]>>;\n}\n// Get velocity.\n$vel = $pos[1] - $pos[0];\nif( $vel.x < 0){\n    vector $tmpVec = $pos[1];\n    $vel = << $tmpVec.x + 0.5, $vel.y, $vel.z>>;\n}\n"
		+ "// Reverse.\n$pos[1] = $pos[1] * (( $curSp < 0) ? -1.0 : 1.0);\n$vel *= ( $curSp < 0) ? -1.0 : 1.0;\n.O[0] = $pos[1] - $vel;\n.O[1] = $vel * $fps;");
	setAttr -k off ".dw";
	setAttr -k off ".fiw";
	setAttr -k off ".con";
	setAttr -k off ".eiw";
	setAttr -k off ".mxc";
	setAttr -k off ".lod";
	setAttr -k off ".inh";
	setAttr ".cts" 1;
	setAttr -k off ".igs";
	setAttr -k off ".ecfh";
	setAttr -k off ".tgs";
	setAttr -k off ".gsm";
	setAttr -k off ".chd";
	setAttr ".chw" 100;
	setAttr -k off ".trd";
	setAttr ".prt" 6;
	setAttr ".lifespanPP0" -type "doubleArray" 50 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38
		 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38
		 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38
		 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38
		 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 3.4028234663852886e+38 ;
	setAttr ".lifespan" 1;
	setAttr -k on ".nsp";
	setAttr -k on ".xsp";
	setAttr -k on ".wav";
	setAttr -k on ".dsp";
	setAttr -k on ".tof";
	setAttr -k on ".fps";
createNode airField -n "allWindAirField" -p "allWindScale";
	setAttr ".v" no;
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr -k off ".mag" 0;
	setAttr -k off ".apv" yes;
	setAttr ".umd" yes;
	setAttr -k off ".vol";
	setAttr -k off ".vex";
	setAttr -k off ".vox";
	setAttr -k off ".voy";
	setAttr -k off ".voz";
	setAttr -k off ".tsr";
	setAttr -k off ".vsw";
	setAttr ".fc[0]"  0 1 1;
	setAttr ".amag[0]"  0 1 1;
	setAttr ".crad[0]"  0 1 1;
	setAttr -k off ".dx";
	setAttr -k off ".dy";
	setAttr -k off ".dz";
	setAttr ".spd" 0.5;
	setAttr -k off ".co" yes;
	setAttr -k off ".spr" 1;
	setAttr -k off ".es";
	setAttr -k off ".ir" yes;
createNode pointConstraint -n "allWindPointConst" -p "allWindScale";
	addAttr -ci true -sn "w0" -ln "allRopeWindCube1TranslateW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -k on ".w0";
createNode transform -n "allWindProperty" -p "dynamicsGrp";
	addAttr -ci true -sn "inLocalSpace" -ln "inLocalSpace" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "windAngleV" -ln "windAngleV" -at "doubleAngle";
	addAttr -ci true -sn "windAngleH" -ln "windAngleH" -at "doubleAngle";
	addAttr -ci true -sn "windMinSpeed" -ln "windMinSpeed" -at "double";
	addAttr -ci true -sn "windMaxSpeed" -ln "windMaxSpeed" -at "double";
	addAttr -ci true -sn "windWaver" -ln "windWaver" -at "double";
	addAttr -ci true -sn "windDispersal" -ln "windDispersal" -at "double";
	addAttr -ci true -sn "windTimeOffset" -ln "windTimeOffset" -at "double";
	addAttr -ci true -sn "fieldPower" -ln "fieldPower" -at "double";
	addAttr -ci true -sn "fieldAttenuation" -ln "fieldAttenuation" -at "double";
	addAttr -ci true -sn "fieldMaxDistance" -ln "fieldMaxDistance" -at "doubleLinear";
	setAttr -l on -k off ".v";
	setAttr -l on ".t";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -l on ".r";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on ".s";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr -l on ".sh";
	setAttr -l on ".ra";
	setAttr -l on ".it";
	setAttr -k on ".inLocalSpace";
	setAttr -k on ".windAngleV";
	setAttr -k on ".windAngleH";
	setAttr -k on ".windMinSpeed" 0.5;
	setAttr -k on ".windMaxSpeed" 1;
	setAttr -k on ".windWaver" 2;
	setAttr -k on ".windDispersal" 1;
	setAttr -k on ".windTimeOffset";
	setAttr -k on ".fieldPower";
	setAttr -k on ".fieldAttenuation" 1.5;
	setAttr -k on ".fieldMaxDistance" 1.5;
createNode transform -n "GlobalProperty" -p "dynamicsGrp";
	addAttr -ci true -sn "simulationStartFrame" -ln "simulationStartFrame" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on ".t";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -l on ".r";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -l on ".ro";
	setAttr -l on ".s";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr -l on ".sh";
	setAttr -l on ".ra";
	setAttr -l on ".it";
	setAttr -k on ".simulationStartFrame" -24;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
	setAttr -k on ".etw";
	setAttr -k on ".tps";
	setAttr -k on ".tms";
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
lockNode -l 1 ;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
lockNode -l 1 ;
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -k on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1;
	setAttr -k on ".ef" 10;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -k on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".rv";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mot";
	setAttr -av -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -cb on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -cb on ".isu";
	setAttr -cb on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".rp";
	setAttr -k on ".cai";
	setAttr -k on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -k on ".ei";
	setAttr -av -k on ".ex";
	setAttr -av -k on ".es";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bf";
	setAttr -k on ".fii";
	setAttr -av -k on ".sf";
	setAttr -k on ".gr";
	setAttr -k on ".li";
	setAttr -k on ".ls";
	setAttr -av -k on ".mb";
	setAttr -k on ".ti";
	setAttr -k on ".txt";
	setAttr -k on ".mpr";
	setAttr -k on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -k on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -k on ".as";
	setAttr -k on ".ds";
	setAttr -k on ".lm";
	setAttr -av -k on ".fir";
	setAttr -k on ".aap";
	setAttr -av -k on ".gh";
	setAttr -cb on ".sd";
connectAttr "allWindProperty.windAngleV" "allWindRotate.rx";
connectAttr "allWindProperty.windAngleH" "allWindRotate.ry";
connectAttr "allWindProperty.inLocalSpace" "allWindRotate.it";
connectAttr "allWindPointConst.ctx" "allWindScale.tx";
connectAttr "allWindPointConst.cty" "allWindScale.ty";
connectAttr "allWindPointConst.ctz" "allWindScale.tz";
connectAttr "allWindProperty.windMinSpeed" "allWindParticleShape.nsp";
connectAttr "allWindProperty.windMaxSpeed" "allWindParticleShape.xsp";
connectAttr "allWindProperty.windWaver" "allWindParticleShape.wav";
connectAttr "allWindProperty.windDispersal" "allWindParticleShape.dsp";
connectAttr "allWindProperty.windTimeOffset" "allWindParticleShape.tof";
connectAttr ":time1.o" "allWindParticleShape.cti";
connectAttr "GlobalProperty.simulationStartFrame" "allWindParticleShape.stf";
connectAttr "allWindParticleShape.xo[0]" "allWindParticleShape.pos";
connectAttr "allWindParticleShape.xo[1]" "allWindParticleShape.vel";
connectAttr "allWindParticleShape.id" "allWindParticleShape.xi[0]";
connectAttr "allWindParticleShape.nsp" "allWindParticleShape.xi[1]";
connectAttr "allWindParticleShape.xsp" "allWindParticleShape.xi[2]";
connectAttr "allWindParticleShape.fps" "allWindParticleShape.xi[3]";
connectAttr "allWindParticleShape.wav" "allWindParticleShape.xi[4]";
connectAttr "allWindParticleShape.dsp" "allWindParticleShape.xi[5]";
connectAttr "allWindParticleShape.tof" "allWindParticleShape.xi[6]";
connectAttr ":time1.o" "allWindParticleShape.tim";
connectAttr "allWindAirField.iv" "allWindParticleShape.tailSize";
connectAttr "allWindParticleShape.ctd" "allWindAirField.t";
connectAttr "allWindProperty.fieldPower" "allWindAirField.iv";
connectAttr "allWindParticleShape.cwcn" "allWindAirField.ocd";
connectAttr "allWindParticleShape.cwps" "allWindAirField.opd";
connectAttr "allWindParticleShape.cwvl" "allWindAirField.ovd";
connectAttr "allWindProperty.fieldAttenuation" "allWindAirField.att";
connectAttr "allWindProperty.fieldMaxDistance" "allWindAirField.max";
connectAttr "allWindScale.pim" "allWindPointConst.cpim";
connectAttr "allWindScale.rp" "allWindPointConst.crp";
connectAttr "allWindScale.rpt" "allWindPointConst.crt";
connectAttr "allWindTranslate.t" "allWindPointConst.tg[0].tt";
connectAttr "allWindTranslate.rp" "allWindPointConst.tg[0].trp";
connectAttr "allWindTranslate.rpt" "allWindPointConst.tg[0].trt";
connectAttr "allWindTranslate.pm" "allWindPointConst.tg[0].tpm";
connectAttr "allWindPointConst.w0" "allWindPointConst.tg[0].tw";
connectAttr "allWindParticleShape.iog" ":initialParticleSE.dsm" -na;
// End of hairWindBox.v1.ma
