import maya.cmds as mc

def main(*args):
    sel = mc.ls(sl=True)

    FirstFraming = mc.intField( FirstFrame , q = True, v = True)
    EndFraming = mc.intField( EndFrame , q = True, v = True)

    TRNloc = mc.spaceLocator(n = 'TRN_' + sel[0] + '_Track')
    FWDloc = mc.spaceLocator(n = 'FWD_' + sel[0] + '_Track')
    BWDloc = mc.spaceLocator(n = 'BWD_' + sel[0] + '_Track')

    mc.setAttr( TRNloc[0] + '.v'  , 0 )
    mc.setAttr( TRNloc[0] + '.r'  , l = 1 )
    mc.setAttr( TRNloc[0] + '.s'  , l = 1 )
    mc.setAttr( TRNloc[0] + '.rx'  , k = 0)
    mc.setAttr( TRNloc[0] + '.ry'  , k = 0)
    mc.setAttr( TRNloc[0] + '.rz'  , k = 0)
    mc.setAttr( TRNloc[0] + '.sx'  , k = 0)
    mc.setAttr( TRNloc[0] + '.sy'  , k = 0)
    mc.setAttr( TRNloc[0] + '.sz'  , k = 0)

    mc.setAttr( FWDloc[0] + '.r'  , l = 1 )
    mc.setAttr( FWDloc[0] + '.s'  , l = 1 )
    mc.setAttr( FWDloc[0] + '.rx'  , k = 0)
    mc.setAttr( FWDloc[0] + '.ry'  , k = 0)
    mc.setAttr( FWDloc[0] + '.rz'  , k = 0)
    mc.setAttr( FWDloc[0] + '.sx'  , k = 0)
    mc.setAttr( FWDloc[0] + '.sy'  , k = 0)
    mc.setAttr( FWDloc[0] + '.sz'  , k = 0)

    mc.setAttr( BWDloc[0] + '.r'  , l = 1 )
    mc.setAttr( BWDloc[0] + '.s'  , l = 1 )
    mc.setAttr( BWDloc[0] + '.rx'  , k = 0)
    mc.setAttr( BWDloc[0] + '.ry'  , k = 0)
    mc.setAttr( BWDloc[0] + '.rz'  , k = 0)
    mc.setAttr( BWDloc[0] + '.sx'  , k = 0)
    mc.setAttr( BWDloc[0] + '.sy'  , k = 0)
    mc.setAttr( BWDloc[0] + '.sz'  , k = 0)

    TRNParent = mc.pointConstraint( sel[0] , TRNloc , mo = 1)

    mc.bakeResults( TRNloc, t=(FirstFraming,EndFraming) , simulation=True)

    mc.delete(TRNParent)

    mc.addAttr( BWDloc , shortName='EX', longName='EnvelopeX', defaultValue = 1.0 ,attributeType='double' , k = 1)
    mc.addAttr( BWDloc , shortName='EY', longName='EnvelopeY', defaultValue = 1.0 ,attributeType='double' , k = 1)
    mc.addAttr( BWDloc , shortName='EZ', longName='EnvelopeZ', defaultValue = 1.0 ,attributeType='double' , k = 1)

    TRNrv = mc.createNode( 'multiplyDivide' , n = TRNloc[0] + '_rv_mdv')
    mc.setAttr( TRNrv + '.input2X' , -1)
    mc.setAttr( TRNrv + '.input2Y' , -1)
    mc.setAttr( TRNrv + '.input2Z' , -1)

    TRNev = mc.createNode( 'multiplyDivide' , n = TRNloc[0] + '_ev_mdv')

    mc.connectAttr( TRNloc[0] + '.translate', TRNrv + '.input1', force = True)
    mc.connectAttr( TRNrv + '.output', TRNev + '.input1', force = True)
    mc.connectAttr( TRNev + '.output', BWDloc[0] + '.translate', force = True , lock = True)

    mc.connectAttr( BWDloc[0] + '.EnvelopeX', TRNev + '.input2.input2X', force = True)
    mc.connectAttr( BWDloc[0] + '.EnvelopeY', TRNev + '.input2.input2Y', force = True)
    mc.connectAttr( BWDloc[0] + '.EnvelopeZ', TRNev + '.input2.input2Z', force = True)
    
    TRNFwd = mc.createNode( 'multiplyDivide' , n = TRNloc[0] + '_FWD_mdv')
    mc.connectAttr( TRNloc[0] + '.translate', TRNFwd + '.input1', force = True)
    mc.connectAttr( BWDloc[0] + '.EnvelopeX', TRNFwd + '.input2.input2X', force = True)
    mc.connectAttr( BWDloc[0] + '.EnvelopeY', TRNFwd + '.input2.input2Y', force = True)
    mc.connectAttr( BWDloc[0] + '.EnvelopeZ', TRNFwd + '.input2.input2Z', force = True)
    mc.connectAttr( TRNFwd + '.output', FWDloc[0] + '.translate', force = True , lock = True)
    
    if mc.objExists('BWD_GRP'):
        mc.parent('BWD_GRP' , BWDloc)
    
    else:
        BWDgrp = mc.createNode( 'transform' , n = 'BWD' + '_GRP')
        mc.setAttr( BWDgrp + '.t'  , l = 1 )
        mc.setAttr( BWDgrp + '.r'  , l = 1 )
        mc.setAttr( BWDgrp + '.s'  , l = 1 )
     
        for i in range(1,len(sel)):
            mc.setAttr( sel[i] + '.t'  , l = 1 )
            mc.setAttr( sel[i] + '.r'  , l = 1 )
            #mc.setAttr( sel[i] + '.s'  , l = 1 )
            mc.parent(sel[i] , BWDgrp)
        
        mc.parent(BWDgrp , BWDloc)

if mc.window ('BOUT_TrackTranslate', q = True, ex = True):
    mc.deleteUI ('BOUT_TrackTranslate')
    
window = mc.window ('BOUT_TrackTranslate' , title = "BOUT Track Translate", widthHeight=(100, 50))

mc.frameLayout( label='How to use a Script' )
mc.columnLayout()
mc.text( label=" 1. Select First Object to Track")
mc.text( label=' 2. Select Others Object to Backward')
mc.setParent( '..' )

mc.frameLayout( label='Set Frame' )
mc.columnLayout(adj = True)

mc.rowColumnLayout( numberOfRows = 1 , cat = [(1,'left',5),(2,'left',5),(3,'left' ,19),(4,'left',5),(5,'left' ,16),(6,'left' ,5)] )

Min = mc.playbackOptions( q = True , min = True)
Max = mc.playbackOptions( q = True , max = True)

mc.text( label=' FirstFrame ' )
FirstFrame = mc.intField(v = Min)

mc.text( label=' EndFrame ' )
EndFrame = mc.intField(v = Max)
mc.setParent( '..' )
mc.setParent( '..' )

mc.button( label='Track Translate', c =  main)

mc.setParent( '..' )

mc.showWindow('BOUT_TrackTranslate')
mc.window ('BOUT_TrackTranslate', e = True , s = 0 , w = 400 , h = 138 , tlc = [500,700])