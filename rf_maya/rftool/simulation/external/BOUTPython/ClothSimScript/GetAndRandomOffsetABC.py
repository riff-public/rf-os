import maya.cmds as mc
from random import randrange, uniform

def getAt(*args):
    sel = mc.ls(sl=True)
    SelShape = mc.listRelatives(sel[0] , c=True)
    SelABC = mc.listConnections(SelShape , s=True , type = 'AlembicNode')
    ABCsf = mc.getAttr(SelABC[0] + '.startFrame')
    ABCef = mc.getAttr(SelABC[0] + '.endFrame')
    Speed = mc.getAttr(SelABC[0] + '.speed')
    Offset = mc.getAttr(SelABC[0] + '.offset')
    mc.floatField( ABCsfV , e = True, v = ABCsf)
    mc.floatField( ABCefV , e = True, v = ABCef)
    mc.floatField( SpeedV , e = True, v = Speed)
    mc.floatField( OffsetV , e = True, v = Offset)



def main(*args):
    sel = mc.ls(sl=True)
    
    for i in range(len(sel)):
        SelShape = mc.listRelatives(sel[i] , c=True)
        SelABC = mc.listConnections(SelShape , s=True , type = 'AlembicNode')
        ABCsf = mc.getAttr(SelABC[0] + '.startFrame')
        ABCef = mc.getAttr(SelABC[0] + '.endFrame')
        
        # randrange gives you an integral value
        frand = uniform(ABCsf,ABCef)
        SetOffset = round(frand, 0)
        SelOffset = mc.setAttr(SelABC[0] + '.offset' , SetOffset)
        SelCycleType = mc.setAttr(SelABC[0] + '.cycleType' , 1)

        # Print Value
        print ''
        print sel[i]
        print 'First Frame = ' , ABCsf
        print 'End Frame   = ' , ABCef
        print 'Speed       = ' , mc.getAttr(SelABC[0] + '.speed')
        print 'Offset      = ' , mc.getAttr(SelABC[0] + '.offset')
        getAt()
    

if mc.window ('BOUT_RandomOffsetABC', q = True, ex = True):
    mc.deleteUI ('BOUT_RandomOffsetABC')
    
window = mc.window ('BOUT_RandomOffsetABC' , title = "BOUT Random Offset ABC", widthHeight=(100, 50))

mc.frameLayout( label='AlembicNode')
mc.columnLayout(adj = True)

mc.rowColumnLayout( numberOfRows = 1 , cat = [(1,'left',20),(2,'left',5),(3,'left' ,19),(4,'left',5),(5,'left' ,16),(6,'left' ,5)] )
mc.text( label='1st Frame' )
ABCsfV = mc.floatField(value= 1)
mc.text( label='End Frame' )
ABCefV = mc.floatField(value= 100)
mc.setParent( '..' )

mc.rowColumnLayout( numberOfRows = 1 , cat = [(1,'left',30),(2,'left',14),(3,'left' ,30),(4,'left',21),(5,'left' ,16),(6,'left' ,5)] )
mc.text( label='Speed' )
SpeedV = mc.floatField(value= 1)
mc.text( label='Offset' )
OffsetV = mc.floatField(value= 0)
mc.setParent( '..' )

mc.columnLayout(adj = True)
mc.button( label='Get Attribute', c =  getAt)
mc.setParent( '..' )
mc.setParent( '..' )


mc.button( label='Random Offset  ABC', c =  main)

mc.setParent( '..' )

mc.showWindow('BOUT_RandomOffsetABC')
mc.window ('BOUT_RandomOffsetABC', e = True , s = 0 , w = 300 , h = 150 , tlc = [500,700])