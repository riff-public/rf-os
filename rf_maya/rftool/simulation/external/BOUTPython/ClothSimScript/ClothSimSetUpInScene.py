import maya.cmds as mc
import databaseQuery as dbq
import production as pdt
import fileOperator as fop
import UITools as pg
import MnkHrchyOdinUI as mhou
import ClothSimScript.YumInfo as byif
import ClothSimScript.DYNCleaner as bdync
import ClothSimScript.MakeCINDYNSetUp as bmcd

def ClothSimSetUpInScene(*arg):
    
    yumList = byif.YumInfoFUN()
    
    customElem = yumList[4]
    customElem = customElem[0].lower()+customElem[1:]

    pathREF = "/proj/" + yumList[0] + "/share/char/" + customElem + "/Sim/data/DYN_REF/"
    mc.file( pathREF + customElem + "_allDYN." + "cur.ma" , f=True , options='v=0' , type='mayaAscii' , pr=True , i=True )

    bdync.DYNCleaner()

    bmcd.MakeCINDYNSetUp() 