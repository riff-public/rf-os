import maya.cmds as mc

def main(*arg):
    sel = mc.ls(sl = True)
    
    DYC = '_DYC'
    DYCForm = ['_PTS','_CMP','_BND','_STR']
    
    for i in range(len(sel)):
        DYCShape = mc.listRelatives(sel[i] , c=True)
        print DYCShape
        Comp = mc.listConnections(DYCShape , s=True , type = 'nComponent')
        print Comp
        nCloth = mc.listConnections(Comp , s=True , type = 'nCloth')
        print nCloth
        nclothLst = len(nCloth)
        nCloth = nCloth[nclothLst -1]
        print nCloth
        
        if (mc.getAttr(DYCShape[0] + '.connectionMethod')) == 0  :
            print '1'
            if (mc.getAttr(DYCShape[0] + '.bend')) == 0  :
                for j in range(1,30):
                    if mc.objExists(nCloth.split('_')[0] + DYCForm[0] + str(j) + DYC):
                        print 'Exists'
                    else:
                        print 'Rename PTS'
                        mc.rename(sel[i],nCloth.split('_')[0] + DYCForm[0] + str(j) + DYC)
                        break
            else:
                for j in range(1,30):
                    if mc.objExists(nCloth.split('_')[0] + DYCForm[2] + str(j) + DYC):
                        print 'Exists'
                    else:
                        print 'Rename BND'
                        mc.rename(sel[i],nCloth.split('_')[0] + DYCForm[2] + str(j) + DYC)
                        break
        
        elif (mc.getAttr(DYCShape[0] + '.connectionMethod')) == 1  :
            print '2'
            for j in range(1,30):
                if mc.objExists(nCloth.split('_')[0] + DYCForm[1] + str(j) + DYC):
                    print 'Exists'
                else:
                    print 'Rename CMP'
                    mc.rename(sel[i],nCloth.split('_')[0] + DYCForm[1] + str(j) + DYC)
                    break