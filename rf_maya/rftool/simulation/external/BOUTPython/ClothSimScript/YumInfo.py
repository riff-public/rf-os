import maya.cmds as mc
import databaseQuery as dbq
import production as pdt
import fileOperator as fop
import UITools as pg
import MnkHrchyOdinUI as mhou

def YumInfoFUN(*arg):
    # initialize yumbrella module
    yumDBQ = dbq.databaseQuery()
    yumPDT = pdt.production(yumDBQ)
    yumFOP = fop.fileOperator()
    
    Pull = yumPDT.expandPath(mc.file(query=True, sceneName=True))
    
    job = Pull['job']
    clas = Pull['class']
    scene = Pull['scene']
    shoted = Pull['shot']
    elemHN = Pull['elemHN']
    
    return job,clas,scene,shoted,elemHN