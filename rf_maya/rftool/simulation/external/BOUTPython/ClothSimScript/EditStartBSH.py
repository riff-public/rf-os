import maya.cmds as mc

def main(*args):
    SetFraming = mc.intField( SetFrame , q = True, v = True)
    
    BSN = 'CIN_COL_BSH'
    keySel = mc.listAttr ((BSN + '.w'), m = True)
    mc.cutKey( BSN ,at = keySel , option="keys" )
    mc.setKeyframe( BSN ,at = keySel , t = SetFraming , v = 0)
    mc.setKeyframe( BSN ,at = keySel , t = SetFraming + 10 , v = 1)
    mc.setAttr('nucleus1.startFrame' , SetFraming)
    mc.playbackOptions( minTime = SetFraming )
    mc.currentTime( SetFraming )
    
if mc.window ('BOUT_Edit_Start_Frame', q = True, ex = True):
    mc.deleteUI ('BOUT_Edit_Start_Frame')
    
window = mc.window ('BOUT_Edit_Start_Frame' , title = "BOUT Change Start Frame", widthHeight=(100, 50))

mc.frameLayout( label='Set Start Frame' )
mc.columnLayout(adj = True)

mc.rowColumnLayout( numberOfRows = 1 , cat = [(1,'left',20),(2,'left',5),(3,'left' ,10),(4,'left',5),(5,'left' ,16),(6,'left' ,5)] )

Min = mc.playbackOptions( q = True , min = True)

mc.text( label=' Frame : ' )
SetFrame = mc.intField(v = Min)

mc.setParent( '..' )

mc.button( label='Change Start Frame', c =  main)

mc.setParent( '..' )

mc.showWindow('BOUT_Edit_Start_Frame')
mc.window ('BOUT_Edit_Start_Frame', e = True , s = 0 , w = 300 , h = 70 , tlc = [500,900])