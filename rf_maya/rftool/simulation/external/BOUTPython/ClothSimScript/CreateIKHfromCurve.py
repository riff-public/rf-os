import maya.cmds as mc

def CreateIKHfromCurveFUN(*arg):
    sel = mc.ls(sl = True)
    for i in range(len(sel)):
        
        selRel = mc.listRelatives(sel[i] + '1_jnt' , ad = True)
        mc.ikHandle( n = sel[i] + '_ikh' , sj = sel[i] + '1_jnt' , ee= selRel[0] , c=sel[i] , sol ='ikSplineSolver' , ccv=False)
        selRelBind = mc.listRelatives(sel[i] + '1_jnt' , ad = True , type = 'joint')
        print selRelBind
        selSplit = sel[i].split('_')
        print selSplit
        mc.skinCluster( selSplit[0] + '_sdm', sel[i] + '1_jnt' , n = selSplit[0] + '_skc' ,dr=10 )  
        
        # Create CurveInFo #
        crvInfo = mc.createNode('curveInfo' , n = sel[i] + '_crvInfo')
        crvGlb = mc.duplicate( sel[i] , rc = True , n= sel[i] + '_global')
        crvGlbInfo = mc.createNode('curveInfo' , n = crvGlb[0] + '_crvInfo')
        
        mc.connectAttr(sel[i] + 'Shape.worldSpace' , crvInfo + '.inputCurve')
        mc.connectAttr(crvGlb[0] + 'Shape.worldSpace' , crvGlbInfo + '.inputCurve')
    
        
        crvMdv = mc.createNode('multiplyDivide' , n = sel[i] + '_mdv')
        mc.setAttr(crvMdv + '.operation' ,2)
        mc.connectAttr(crvInfo + '.arcLength' , crvMdv + '.input1.input1X')
        mc.connectAttr(crvGlbInfo + '.arcLength' , crvMdv + '.input2.input2X')
    
        # Connect to Joint #
        for j in range(len(selRelBind)): 
            mc.connectAttr(crvMdv + '.output.outputX' , sel[i] + str(j+1) + '_jnt' + '.scale.scaleY')


if mc.window ('CreateIKHfromCurve', q = True, ex = True):
    mc.deleteUI ('CreateIKHfromCurve')
    
window = mc.window ('CreateIKHfromCurve' , title = "Create IKH from Curve", widthHeight=(100, 250))
mc.columnLayout( adj = True )

mc.button( label='Create IKH from Curve' , c = CreateIKHfromCurveFUN )

mc.showWindow('CreateIKHfromCurve')
mc.window ('CreateIKHfromCurve', e = True, w = 250, h = 26, tlc = [400,1100])            