import maya.cmds as mc
import maya.mel as mm

def MakeCINDYNSetUp(*arg):
    # Rename REF to DYN
    DYNREF = 'DYN_REF'
    DYNREFGC = mc.listRelatives( DYNREF , c = True)
    for i in range(len(DYNREFGC)):
        DYNREFSplit = DYNREFGC[i].split('REF')
        mc.rename(DYNREFGC[i], (DYNREFSplit[0] + 'DYN'))
    
    # Delete DYN no use
    REFL = mc.textScrollList('elemQuery_TS',q = True , ai = True)
    mc.delete(REFL)

    # Make DYN_CIN SetUp
    if mc.objExists('DYN_CIN_GRP'):
        print 'DYN_CIN_GRP is exists'
    else:
        mc.createNode('transform' , n = 'DYN_CIN_GRP')
        mc.select( clear=True )
    
    if mc.objExists('nRigid_GRP'):
        print 'nRigid_GRP is exists'
    else:
        mc.createNode('transform' , n = 'nRigid_GRP')
        mc.select( clear=True )
        
    NRigidG = 'nRigid_GRP'

    DYNCINL = mc.textScrollList('CINQuery_TS',q = True , ai = True)
    for i in range(len(DYNCINL)):
        print DYNCINL[i]
        mc.select( DYNCINL[i] )
        mc.rename( DYNCINL[i] , DYNCINL[i] + '_CIN')
        mm.eval('makeCollideNCloth;')
        ObjWrappedCOLShape = mc.listRelatives(DYNCINL[i] + '_CIN')[0]
        nRigidShape = mc.listConnections(ObjWrappedCOLShape, type = 'nRigid')[0]
        mc.rename(nRigidShape , DYNCINL[i] + '_CIN_nRigid')
        mc.setAttr( DYNCINL[i] + '_CIN_nRigidShape' + '.thickness' , 0.01)
        mc.parent(DYNCINL[i] + '_CIN_nRigid' , NRigidG)
        mc.parent(DYNCINL[i] + '_CIN' , 'DYN_CIN_GRP')
    
    # Make DYN SetUp
    DYNL = mc.textScrollList('DYNQuery_TS',q = True , ai = True)

    if mc.objExists('DYN_GRP'):
        print 'DYN_GRP is exists'
    else:
        mc.createNode('transform' , n = 'DYN_GRP')
    
    if mc.objExists('nCloth_GRP'):
        print 'nCloth_GRP is exists'
    else:
        mc.createNode('transform' , n = 'nCloth_GRP')
        mc.parent( 'nCloth_GRP' , 'SIM_GRP' )
        
    for i in range(len(DYNL)):
        mc.setAttr(DYNL[i] + '.t' , l=1)
        mc.setAttr(DYNL[i] + '.r' , l=1)
        mc.setAttr(DYNL[i] + '.s' , l=1)

        # Create NCloth #
        mc.select(DYNL[i])
        mm.eval('createNCloth 0;')
        ObjShape = mc.listRelatives(DYNL[i])
        nClothList = mc.listConnections(ObjShape[0], type = 'nCloth')
        print nClothList
        nClothNode = mc.rename(nClothList[0] , DYNL[i] + '_nCloth')
        nClothShape = mc.listRelatives(nClothNode)[0]
        conNode = mc.listConnections(nClothShape , t = 'mesh', sh = True)
        for eachNode in conNode:
            if 'outputCloth' in eachNode :
                mc.rename(eachNode,nClothNode.split('_DYN')[0] + '_outputCloth')
  
        mc.parent( DYNL[i] , 'DYN_GRP' )
        mc.parent( DYNL[i] + '_nCloth' , 'nCloth_GRP' )
        
    # Parent #
    mc.parent( NRigidG , w = True )
    mc.parent( NRigidG , 'SIM_GRP' )

    # Add Layer #
    for i in range(len(DYNL)-1,-1,-1):
        mc.select(DYNL[i])
        mc.createDisplayLayer( noRecurse=True, name = DYNL[i] + '_layer')
        
    mc.delete('DYN_REF')    