import maya.cmds as mc

def DNshader1(*args):
    sel = mc.ls(sl = True)
    
    if mc.objExists('DNshader1'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='DNshader1SG')
        else:
            print ''
    
    else:
        print 'DNshader1 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            DNshader1 = mc.shadingNode('blinn', asShader=True, n = 'DNshader1')
            DNshader1SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader1SG')
            mc.connectAttr( DNshader1 + '.outColor' , 'DNshader1SG' + '.surfaceShader')
            mc.setAttr(DNshader1 + '.color' , 1 , 0.4 , 0.4 )
            mc.setAttr(DNshader1 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader1 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader1 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.sets(sel, e=True, fe=DNshader1SG)
        else:
            print 'Make DNshader1'
            DNshader1 = mc.shadingNode('blinn', asShader=True, n = 'DNshader1')
            DNshader1SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader1SG')
            mc.connectAttr( DNshader1 + '.outColor' , 'DNshader1SG' + '.surfaceShader' )
            mc.setAttr(DNshader1 + '.color' , 1 , 0.4 , 0.4 )
            mc.setAttr(DNshader1 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader1 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader1 + '.specularColor' , 0.15 , 0.15 , 0.15 )
        
def DNshader2(*args):
    sel = mc.ls(sl = True)
    
    if mc.objExists('DNshader2'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='DNshader2SG')
        else:
            print ''
    else:
        print 'DNshader2 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            DNshader2 = mc.shadingNode('blinn', asShader=True, n = 'DNshader2')
            DNshader2SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader2SG')
            mc.connectAttr( DNshader2 + '.outColor' , 'DNshader2SG' + '.surfaceShader' )
            mc.setAttr(DNshader2 + '.color' , 1 , 0.4 , 0.8 )
            mc.setAttr(DNshader2 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader2 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader2 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.sets(sel, e=True, fe=DNshader2SG)
        else:
            print 'Make DNshader2'
            DNshader2 = mc.shadingNode('blinn', asShader=True, n = 'DNshader2')
            DNshader2SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader2SG')
            mc.connectAttr( DNshader2 + '.outColor' , 'DNshader2SG' + '.surfaceShader' )
            mc.setAttr(DNshader2 + '.color' , 1 , 0.4 , 0.8 )
            mc.setAttr(DNshader2 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader2 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader2 + '.specularColor' , 0.15 , 0.15 , 0.15 )

def DNshader3(*args):
    sel = mc.ls(sl = True)
    if mc.objExists('DNshader3'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='DNshader3SG')
        else:
            print ''
    else:
        print 'DNshader3 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            DNshader3 = mc.shadingNode('blinn', asShader=True, n = 'DNshader3')
            DNshader3SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader3SG')
            mc.connectAttr( DNshader3 + '.outColor' , 'DNshader3SG' + '.surfaceShader' )
            mc.setAttr(DNshader3 + '.color' , 0.8 , 0.4 , 1 )
            mc.setAttr(DNshader3 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader3 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader3 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.sets(sel, e=True, fe=DNshader3SG)
        else:
            print 'Make DNshader3'
            DNshader3 = mc.shadingNode('blinn', asShader=True, n = 'DNshader3')
            DNshader3SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader3SG')
            mc.connectAttr( DNshader3 + '.outColor' , 'DNshader3SG' + '.surfaceShader' )
            mc.setAttr(DNshader3 + '.color' , 0.8 , 0.4 , 1 )
            mc.setAttr(DNshader3 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader3 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader3 + '.specularColor' , 0.15 , 0.15 , 0.15 )

        
def DNshader4(*args):
    sel = mc.ls(sl = True)
    if mc.objExists('DNshader4'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='DNshader4SG')
        else:
            print ''
    else:
        print 'DNshader4 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            DNshader4 = mc.shadingNode('blinn', asShader=True, n = 'DNshader4')
            DNshader4SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader4SG')
            mc.connectAttr( DNshader4 + '.outColor' , 'DNshader4SG' + '.surfaceShader' )
            mc.setAttr(DNshader4 + '.color' , 0.5 , 0.4 , 1 )
            mc.setAttr(DNshader4 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader4 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader4 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.sets(sel, e=True, fe=DNshader4SG)
        else:
            print 'Make DNshader4'
            DNshader4 = mc.shadingNode('blinn', asShader=True, n = 'DNshader4')
            DNshader4SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader4SG')
            mc.connectAttr( DNshader4 + '.outColor' , 'DNshader4SG' + '.surfaceShader' )
            mc.setAttr(DNshader4 + '.color' , 0.5 , 0.4 , 1 )
            mc.setAttr(DNshader4 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader4 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader4 + '.specularColor' , 0.15 , 0.15 , 0.15 )

    
def DNshader5(*args):
    sel = mc.ls(sl = True)
    
    if mc.objExists('DNshader5'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='DNshader5SG')
        else:
            print ''
    else:
        print 'DNshader5 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            DNshader5 = mc.shadingNode('blinn', asShader=True, n = 'DNshader5')
            DNshader5SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader5SG')
            mc.connectAttr( DNshader5 + '.outColor' , 'DNshader5SG' + '.surfaceShader' )
            mc.setAttr(DNshader5 + '.color' , 0.4 , 0.6 , 1 )
            mc.setAttr(DNshader5 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader5 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader5 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.sets(sel, e=True, fe=DNshader5SG)
        else:
            print 'Make DNshader5'
            DNshader5 = mc.shadingNode('blinn', asShader=True, n = 'DNshader5')
            DNshader5SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader5SG')
            mc.connectAttr( DNshader5 + '.outColor' , 'DNshader5SG' + '.surfaceShader' )
            mc.setAttr(DNshader5 + '.color' , 0.4 , 0.6 , 1 )
            mc.setAttr(DNshader5 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader5 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader5 + '.specularColor' , 0.15 , 0.15 , 0.15 )
    

def DNshader6(*args):
    sel = mc.ls(sl = True)
    
    if mc.objExists('DNshader6'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='DNshader6SG')
        else:
            print ''
    else:
        print 'DNshader6 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            DNshader6 = mc.shadingNode('blinn', asShader=True, n = 'DNshader6')
            DNshader6SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader6SG')
            mc.connectAttr( DNshader6 + '.outColor' , 'DNshader6SG' + '.surfaceShader' )
            mc.setAttr(DNshader6 + '.color' , 0.4 , 0.9 , 1 )
            mc.setAttr(DNshader6 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader6 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader6 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.sets(sel, e=True, fe=DNshader6SG)
        else:
            print 'Make DNshader6'
            DNshader6 = mc.shadingNode('blinn', asShader=True, n = 'DNshader6')
            DNshader6SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader6SG')
            mc.connectAttr( DNshader6 + '.outColor' , 'DNshader6SG' + '.surfaceShader' )
            mc.setAttr(DNshader6 + '.color' , 0.4 , 0.9 , 1 )
            mc.setAttr(DNshader6 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader6 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader6 + '.specularColor' , 0.15 , 0.15 , 0.15 )


def DNshader7(*args):
    sel = mc.ls(sl = True)
    if mc.objExists('DNshader7'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='DNshader7SG')
        else:
            print ''
    else:
        print 'DNshader7 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            DNshader7 = mc.shadingNode('blinn', asShader=True, n = 'DNshader7')
            DNshader7SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader7SG')
            mc.connectAttr( DNshader7 + '.outColor' , 'DNshader7SG' + '.surfaceShader' )
            mc.setAttr(DNshader7 + '.color' , 0.4 , 1 , 0.7 )
            mc.setAttr(DNshader7 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader7 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader7 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.sets(sel, e=True, fe=DNshader7SG)
        else:
            print 'Make DNshader7'
            DNshader7 = mc.shadingNode('blinn', asShader=True, n = 'DNshader7')
            DNshader7SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader7SG')
            mc.connectAttr( DNshader7 + '.outColor' , 'DNshader7SG' + '.surfaceShader' )
            mc.setAttr(DNshader7 + '.color' , 0.4 , 1 , 0.7 )
            mc.setAttr(DNshader7 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader7 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader7 + '.specularColor' , 0.15 , 0.15 , 0.15 )


def DNshader8(*args):
    sel = mc.ls(sl = True)
    if mc.objExists('DNshader8'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='DNshader8SG')
        else:
            print ''
    else:
        print 'DNshader8 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            DNshader8 = mc.shadingNode('blinn', asShader=True, n = 'DNshader8')
            DNshader8SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader8SG')
            mc.connectAttr( DNshader8 + '.outColor' , 'DNshader8SG' + '.surfaceShader' )
            mc.setAttr(DNshader8 + '.color' , 0.6 , 1 , 0.4 )
            mc.setAttr(DNshader8 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader8 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader8 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.sets(sel, e=True, fe=DNshader8SG)
        else:
            print 'Make DNshader8'
            DNshader8 = mc.shadingNode('blinn', asShader=True, n = 'DNshader8')
            DNshader8SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader8SG')
            mc.connectAttr( DNshader8 + '.outColor' , 'DNshader8SG' + '.surfaceShader' )
            mc.setAttr(DNshader8 + '.color' , 0.6 , 1 , 0.4 )
            mc.setAttr(DNshader8 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader8 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader8 + '.specularColor' , 0.15 , 0.15 , 0.15 )

def DNshader9(*args):
    sel = mc.ls(sl = True)
    if mc.objExists('DNshader9'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='DNshader9SG')
        else:
            print ''
    else:
        print 'DNshader9 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            DNshader9 = mc.shadingNode('blinn', asShader=True, n = 'DNshader9')
            DNshader9SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader9SG')
            mc.connectAttr( DNshader9 + '.outColor' , 'DNshader9SG' + '.surfaceShader' )
            mc.setAttr(DNshader9 + '.color' , 1 , 0.9 , 0.4 )
            mc.setAttr(DNshader9 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader9 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader9 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.sets(sel, e=True, fe=DNshader9SG)
        else:
            print 'Make DNshader9'
            DNshader9 = mc.shadingNode('blinn', asShader=True, n = 'DNshader9')
            DNshader9SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader9SG')
            mc.connectAttr( DNshader9 + '.outColor' , 'DNshader9SG' + '.surfaceShader' )
            mc.setAttr(DNshader9 + '.color' , 1 , 0.9 , 0.4 )
            mc.setAttr(DNshader9 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader9 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader9 + '.specularColor' , 0.15 , 0.15 , 0.15 )

def DNshader10(*args):
    sel = mc.ls(sl = True)
    if mc.objExists('DNshader10'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='DNshader10SG')
        else:
            print ''
    else:
        print 'DNshader10 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            DNshader10 = mc.shadingNode('blinn', asShader=True, n = 'DNshader10')
            DNshader10SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader10SG')
            mc.connectAttr( DNshader10 + '.outColor' , 'DNshader10SG' + '.surfaceShader' )
            mc.setAttr(DNshader10 + '.color' , 1 , 0.6 , 0.4 )
            mc.setAttr(DNshader10 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader10 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader10 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.sets(sel, e=True, fe=DNshader10SG)
        else:
            print 'Make DNshader10'
            DNshader10 = mc.shadingNode('blinn', asShader=True, n = 'DNshader10')
            DNshader10SG = mc.sets( r=True, nss=True, em=True, n =  'DNshader10SG')
            mc.connectAttr( DNshader10 + '.outColor' , 'DNshader10SG' + '.surfaceShader' )
            mc.setAttr(DNshader10 + '.color' , 1 , 0.6 , 0.4 )
            mc.setAttr(DNshader10 + '.eccentricity' , 0.6)
            mc.setAttr(DNshader10 + '.specularRollOff' , 0.3)
            mc.setAttr(DNshader10 + '.specularColor' , 0.15 , 0.15 , 0.15 )

def DNshaderSkin(*args):
    sel = mc.ls(sl = True)
    if mc.objExists('DNshaderSkin'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='DNshaderSkinSG')
        else:
            print ''
    else:
        print 'DNshaderSkin dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            DNshaderSkin = mc.shadingNode('blinn', asShader=True, n = 'DNshaderSkin')
            DNshaderSkinSG = mc.sets( r=True, nss=True, em=True, n =  'DNshaderSkinSG')
            mc.connectAttr( DNshaderSkin + '.outColor' , 'DNshaderSkinSG' + '.surfaceShader' )
            mc.setAttr(DNshaderSkin + '.color' , 1 , 0.833333 , 0.75 )
            mc.setAttr(DNshaderSkin + '.eccentricity' , 0.6)
            mc.setAttr(DNshaderSkin + '.specularRollOff' , 0.3)
            mc.setAttr(DNshaderSkin + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.sets(sel, e=True, fe=DNshaderSkinSG)
        else:
            print 'Make DNshaderSkin'
            DNshaderSkin = mc.shadingNode('blinn', asShader=True, n = 'DNshaderSkin')
            DNshaderSkinSG = mc.sets( r=True, nss=True, em=True, n =  'DNshaderSkinSG')
            mc.connectAttr( DNshaderSkin + '.outColor' , 'DNshaderSkinSG' + '.surfaceShader' )
            mc.setAttr(DNshaderSkin + '.color' , 1 , 0.833333 , 0.75 )
            mc.setAttr(DNshaderSkin + '.eccentricity' , 0.6)
            mc.setAttr(DNshaderSkin + '.specularRollOff' , 0.3)
            mc.setAttr(DNshaderSkin + '.specularColor' , 0.15 , 0.15 , 0.15 )

def GrayShader1(*args):
    sel = mc.ls(sl = True)
    if mc.objExists('GrayShader1'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='GrayShader1SG')
        else:
            print ''
    else:
        print 'GrayShader1 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            GrayShader1 = mc.shadingNode('blinn', asShader=True, n = 'GrayShader1')
            GrayShader1SG = mc.sets( r=True, nss=True, em=True, n =  'GrayShader1SG')
            mc.connectAttr( GrayShader1 + '.outColor' , 'GrayShader1SG' + '.surfaceShader' )
            mc.setAttr(GrayShader1 + '.color' , 0 , 0 , 0 )
            mc.setAttr(GrayShader1 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.setAttr(GrayShader1 + '.eccentricity' , 1.0)
            mc.setAttr(GrayShader1 + '.specularRollOff' , 0.3)
            mc.sets(sel, e=True, fe=GrayShader1SG)
        else:
            print 'Make GrayShader1'
            GrayShader1 = mc.shadingNode('blinn', asShader=True, n = 'GrayShader1')
            GrayShader1SG = mc.sets( r=True, nss=True, em=True, n =  'GrayShader1SG')
            mc.connectAttr( GrayShader1 + '.outColor' , 'GrayShader1SG' + '.surfaceShader' )
            mc.setAttr(GrayShader1 + '.color' , 0 , 0 , 0 )
            mc.setAttr(GrayShader1 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.setAttr(GrayShader1 + '.eccentricity' , 1.0)
            mc.setAttr(GrayShader1 + '.specularRollOff' , 0.3)

    
def GrayShader2(*args):
    sel = mc.ls(sl = True)
    if mc.objExists('GrayShader2'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='GrayShader2SG')
        else:
            print ''
    else:
        print 'GrayShader2 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            GrayShader2 = mc.shadingNode('blinn', asShader=True, n = 'GrayShader2')
            GrayShader2SG = mc.sets( r=True, nss=True, em=True, n =  'GrayShader2SG')
            mc.connectAttr( GrayShader2 + '.outColor' , 'GrayShader2SG' + '.surfaceShader' )
            mc.setAttr(GrayShader2 + '.color' , 0.25 , 0.25 , 0.25 )
            mc.setAttr(GrayShader2 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.setAttr(GrayShader2 + '.eccentricity' , 1.0)
            mc.setAttr(GrayShader2 + '.specularRollOff' , 0.3)
            mc.sets(sel, e=True, fe=GrayShader2SG)
        else:
            print 'Make GrayShader2'
            GrayShader2 = mc.shadingNode('blinn', asShader=True, n = 'GrayShader2')
            GrayShader2SG = mc.sets( r=True, nss=True, em=True, n =  'GrayShader2SG')
            mc.connectAttr( GrayShader2 + '.outColor' , 'GrayShader2SG' + '.surfaceShader' )
            mc.setAttr(GrayShader2 + '.color' , 0.25 , 0.25 , 0.25 )
            mc.setAttr(GrayShader2 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.setAttr(GrayShader2 + '.eccentricity' , 1.0)
            mc.setAttr(GrayShader2 + '.specularRollOff' , 0.3)

    
def GrayShader3(*args):
    sel = mc.ls(sl = True)
    if mc.objExists('GrayShader3'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='GrayShader3SG')
        else:
            print ''
    else:
        print 'GrayShader3 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            GrayShader3 = mc.shadingNode('blinn', asShader=True, n = 'GrayShader3')
            GrayShader3SG = mc.sets( r=True, nss=True, em=True, n =  'GrayShader3SG')
            mc.connectAttr( GrayShader3 + '.outColor' , 'GrayShader3SG' + '.surfaceShader' )
            mc.setAttr(GrayShader3 + '.color' , 0.5 , 0.5 , 0.5 )
            mc.setAttr(GrayShader3 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.setAttr(GrayShader3 + '.eccentricity' , 1.0)
            mc.setAttr(GrayShader3 + '.specularRollOff' , 0.3)
            mc.sets(sel, e=True, fe=GrayShader3SG)
        else:
            print 'Make GrayShader3'
            GrayShader3 = mc.shadingNode('blinn', asShader=True, n = 'GrayShader3')
            GrayShader3SG = mc.sets( r=True, nss=True, em=True, n =  'GrayShader3SG')
            mc.connectAttr( GrayShader3 + '.outColor' , 'GrayShader3SG' + '.surfaceShader' )
            mc.setAttr(GrayShader3 + '.color' , 0.5 , 0.5 , 0.5 )
            mc.setAttr(GrayShader3 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.setAttr(GrayShader3 + '.eccentricity' , 1.0)
            mc.setAttr(GrayShader3 + '.specularRollOff' , 0.3)

    
def GrayShader4(*args):
    sel = mc.ls(sl = True)
    if mc.objExists('GrayShader4'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='GrayShader4SG')
        else:
            print ''
    else:
        print 'GrayShader4 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            GrayShader4 = mc.shadingNode('blinn', asShader=True, n = 'GrayShader4')
            GrayShader4SG = mc.sets( r=True, nss=True, em=True, n =  'GrayShader4SG')
            mc.connectAttr( GrayShader4 + '.outColor' , 'GrayShader4SG' + '.surfaceShader' )
            mc.setAttr(GrayShader4 + '.color' , 0.75 , 0.75 , 0.75 )
            mc.setAttr(GrayShader4 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.setAttr(GrayShader4 + '.eccentricity' , 1.0)
            mc.setAttr(GrayShader4 + '.specularRollOff' , 0.3)
            mc.sets(sel, e=True, fe=GrayShader4SG)
        else:
            print 'Make GrayShader4'
            GrayShader4 = mc.shadingNode('blinn', asShader=True, n = 'GrayShader4')
            GrayShader4SG = mc.sets( r=True, nss=True, em=True, n =  'GrayShader4SG')
            mc.connectAttr( GrayShader4 + '.outColor' , 'GrayShader4SG' + '.surfaceShader' )
            mc.setAttr(GrayShader4 + '.color' , 0.75 , 0.75 , 0.75 )
            mc.setAttr(GrayShader4 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.setAttr(GrayShader4 + '.eccentricity' , 1.0)
            mc.setAttr(GrayShader4 + '.specularRollOff' , 0.3)

    
def GrayShader5(*args):
    sel = mc.ls(sl = True)
    if mc.objExists('GrayShader5'):
        print 'Already Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            print sel
            mc.sets(sel, e=True, fe='GrayShader5SG')
        else:
            print ''
    else:
        print 'GrayShader5 dose not Exists'
        if mc.ls(sl = True):
            print 'Assign Texture'
            GrayShader5 = mc.shadingNode('blinn', asShader=True, n = 'GrayShader5')
            GrayShader5SG = mc.sets( r=True, nss=True, em=True, n =  'GrayShader5SG')
            mc.connectAttr( GrayShader5 + '.outColor' , 'GrayShader5SG' + '.surfaceShader' )
            mc.setAttr(GrayShader5 + '.color' , 1 , 1 , 1 )
            mc.setAttr(GrayShader5 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.setAttr(GrayShader5 + '.eccentricity' , 1.0)
            mc.setAttr(GrayShader5 + '.specularRollOff' , 0.3)
            mc.sets(sel, e=True, fe=GrayShader5SG)
        else:
            print 'Make GrayShader5'
            GrayShader5 = mc.shadingNode('blinn', asShader=True, n = 'GrayShader5')
            GrayShader5SG = mc.sets( r=True, nss=True, em=True, n =  'GrayShader5SG')
            mc.connectAttr( GrayShader5 + '.outColor' , 'GrayShader5SG' + '.surfaceShader' )
            mc.setAttr(GrayShader5 + '.color' , 1 , 1 , 1 )
            mc.setAttr(GrayShader5 + '.specularColor' , 0.15 , 0.15 , 0.15 )
            mc.setAttr(GrayShader5 + '.eccentricity' , 1.0)
            mc.setAttr(GrayShader5 + '.specularRollOff' , 0.3)

    
if mc.window ('CreateDNshader', q = True, ex = True):
    mc.deleteUI ('CreateDNshader')
    
window = mc.window ('CreateDNshader' , title = "Create DNshader (Blinn)", widthHeight=(100, 150))
mc.columnLayout( adj = True )

mc.button( label='Red', bgc = (1.0 ,0.0 ,0.0) , c = DNshader1 )
mc.button( label='Pink', bgc = (1.0 ,0.2 ,1.0) , c = DNshader2 )
mc.button( label='Purple', bgc = (0.5 ,0.0 ,0.9) , c = DNshader3 )
mc.button( label='DarkPurple', bgc = (0.25 ,0.0 ,0.5) , c = DNshader4 )
mc.button( label='Blue', bgc = (0.0 ,0.5 ,1.0) , c = DNshader5 )
mc.button( label='LightBlue', bgc = (0.5 ,1.0 ,1.0) , c = DNshader6 )
mc.button( label='LightGreen', bgc = (0.1 ,1.0 ,0.8) , c = DNshader7 )
mc.button( label='Green', bgc = (0.0 ,1.0 ,0.0) , c = DNshader8 )
mc.button( label='Yellow', bgc = (1.0 ,1.0 ,0.0) , c = DNshader9 )
mc.button( label='Orange', bgc = (1.0 ,0.5 ,0.0) , c = DNshader10 )
mc.button( label='Skin', bgc = (1.0 ,0.5 ,0.5) , c = DNshaderSkin )
mc.button( label='Black', bgc = (0.1 ,0.1 ,0.1) , c = GrayShader1 )
mc.button( label='Dark', bgc = (0.25 ,0.25 ,0.25) , c = GrayShader2 )
mc.button( label='Gray', bgc = (0.5 ,0.5 ,0.5) , c = GrayShader3 )
mc.button( label='LightGray', bgc = (0.75 ,0.75 ,0.75) , c = GrayShader4 )
mc.button( label='White', bgc = (1.0 ,1.0 ,1.0) , c = GrayShader5 )

mc.showWindow('CreateDNshader')
mc.window ('CreateDNshader', e = True, w = 260, h = 370, tlc = [400,1100])