import maya.cmds as mc

unknowPluginList = mc.unknownPlugin(q=1, l=True)
for unknowPlug in unknowPluginList:
    try:
        mc.unknownPlugin(unknowPlug, r=True)
    except:
        pass