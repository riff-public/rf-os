import maya.cmds as mc

def main(*args):

    sel = mc.ls(sl=True)
    
    for i in range(len(sel)-1,-1,-1):
        mc.select(sel[i])
        mc.createDisplayLayer( noRecurse=True, name = sel[i] + '_layer')
