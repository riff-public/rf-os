import maya.cmds as mc
import maya.mel as mm
import databaseQuery as dbq
import production as pdt
import fileOperator as fop
yumDBQ = dbq.databaseQuery()
yumPDT = pdt.production(yumDBQ)
yumFOP = fop.fileOperator()

yumPath = yumPDT.expandPath(mc.file( q = True , sn = True))

def main(*arg):
    sel = mc.ls(sl = True)
    
    # FIX Geometry #
    for i in range(len(sel)):
        #FIXDYNObj = mc.duplicate( sel[i] , rc = True)[0]
        #FIXObjSplit = FIXDYNObj.split('DYN')
        #FIXObj = mc.rename(FIXDYNObj, (FIXObjSplit[0] + 'FIX'))
    
        #FIXBSH = mc.blendShape( FIXObj , sel[i] , o = 'world' , n = FIXObj + '_BSN' )
        #mc.setAttr( FIXBSH[0] + '.' + FIXObj , 1)
    
        # DYN COUT Geometry #
        DYNCOUTObj = mc.duplicate( sel[i] , rc = True)[0]
        COUTObjSplit = DYNCOUTObj.split('1')
        COUTObj = mc.rename(DYNCOUTObj, (COUTObjSplit[0] + '_COUT'))
    
        # Create NCloth #
        mc.select(sel[i])
        mm.eval('createNCloth 0;')
    
        ObjShape = mc.listRelatives(sel[i])
        nClothList = mc.listConnections(ObjShape[0], type = 'nCloth')
        nClothNode = mc.rename(nClothList[0] , sel[i] + '_nCloth')
        nClothShape = mc.listRelatives(nClothNode)[0]
        conNode = mc.listConnections(nClothShape , t = 'mesh', sh = True)
        for eachNode in conNode:
            if 'outputCloth' in eachNode :
                mc.rename(eachNode,nClothNode.split('_DYN')[0] + '_outputCloth')

        COUTBSH = mc.blendShape( sel[i] , COUTObj , o = 'world' , n = COUTObj + '_BSN' )
        mc.setAttr( COUTBSH[0] + '.' + sel[i] , 1)
    
        # Parent #
        mc.parent( sel[i] , 'DYN_GRP' )
        #mc.parent( FIXObj , 'FIX_GRP' )
        #mc.parent( sel[i] + '_COUT' , yumPath['shot'] + '_DYN_COUT' )
        mc.parent( sel[i] + '_nCloth' , 'nCloth_GRP' )

    # Add Layer #
    for i in range(len(sel)-1,-1,-1):
        mc.select(sel[i])
        mc.createDisplayLayer( noRecurse=True, name = sel[i] + '_layer')