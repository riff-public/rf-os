import maya.cmds as mc

# Wire Curve and Polygon #

# 1.Sel Curve Group and Polygon Group
# 2.Run

def WireFUN(*args):
    sel = mc.ls(sl=True)
    
    selListCRV = mc.listRelatives(sel[0] , c = True)
    print selListCRV
    
    selListSDM = mc.listRelatives(sel[1] , c = True)
    print selListSDM
    
    
    for i in range(len(selListSDM)) :
    	mc.wire(selListSDM[i], w = selListCRV[i], gw = False , en = 1.0, ce = 0.0, li = 0.0 , n = selListSDM[i] + '_wire')
    	mc.setAttr(selListSDM[i] + '_wire' + ".dropoffDistance[0]", 10000)

if mc.window ('WireCurvePolygon', q = True, ex = True):
    mc.deleteUI ('WireCurvePolygon')
    
window = mc.window ('WireCurvePolygon' , title = "Wire Curve Polygon", widthHeight=(100, 200))
mc.columnLayout( adj = True )

mc.button( label='Wire Curve and Polygon', c = WireFUN )


mc.showWindow('WireCurvePolygon')
mc.window ('WireCurvePolygon', e = True, w = 250, h = 27, tlc = [400,1100])