import maya.cmds as mc;
import pymel.core as pm;
import os, re, ast;
import subprocess;
import maya.mel as mel;
import sys;

quickTime = "C:\\Program Files (x86)\\QuickTime\\QuickTimePlayer.exe";
mpc_hc = "C:\\Program Files (x86)\\K-Lite Codec Pack\\MPC-HC64\\mpc-hc64_nvo.exe";
imagePath = "N:\\Staff\\_techAnim\\June\\juneScript\\playblast\\imagePlayblastUI";
currentProject = pm.workspace( q = True, rd = True );

mp = os.path.realpath(__file__);
fileName = os.path.basename(__file__);
mp = mp.replace( fileName , '' );
mp = mp.replace( '\\' , '/' );

def createFolderTxt( *args ):
    myDocuments = os.path.expanduser('~');          
    myDocuments = myDocuments.replace('\\', '/');
    # C:/Users/Legion/Documents
    prefPath = myDocuments + '/juneScriptData';
    if os.path.exists( prefPath ) == False:
        os.makedirs( prefPath );
    else : pass;

    return prefPath;

def directory( path = '' , *args ):
    if os.path.exists ( path ) == False:
        os.makedirs ( path );
    else : pass ;
    
    return ( path );

def browsePath( *args ):
    try : 
        startPath = pm.textField( 'inputPath' , q = True , text = True );
    except :
        startPath = '/C:';

    browsePath = mc.fileDialog2( ds = 2 , fm = 3 , startingDirectory = startPath , okc = 'select directory' )[0];
 
    if browsePath[-1] != '/':
        browsePath += '/';

    pm.textField( 'inputPath' , e = True , text = browsePath );
    saveDefaultPath();
    playblastPath();

def saveDefaultPath( *args ):
    prefPath = createFolderTxt( );
    inputPathTxt = open( prefPath + '/inputPath.txt' , 'w+' );
    pathName = pm.textField( 'inputPath' , q = True , text = True );
    inputPathTxt.write( pathName );
    inputPathTxt.close( );

def getDefaultPath( *args ):
    prefPath = createFolderTxt();
    pathname = ""; 
    if os.path.isfile( prefPath + '/inputPath.txt' ):
        pathTxt = open( prefPath + '/inputPath.txt', 'r' );
        pathname = pathTxt.read();
        pathTxt.close();
    else :
        pathTxt = open( prefPath + '/inputPath.txt', 'w' );
        pathTxt.write("");
        pathTxt.close();
    return pathname;

def playblastPath( *args ):
    pathFile = os.path.normpath( mc.file( q = True, sn = True )).split('\\');
    try:
        newPath = pathFile[1] + '/' + pathFile[2] + '/' + pathFile[3] + '/' + pathFile[4] + '/' + pathFile[6];
        pathName = getDefaultPath();
        dirName = pathName + 'Playblast' + '/' + newPath;

    except:
        newPath = pathFile[-1].split('.');
        pathName = getDefaultPath();
        dirName = pathName + 'Playblast' + '/' + newPath[0];

    mc.sysFile( dirName, makeDir=True );
    pm.textField( 'currentPath', e = True, tx = dirName );
    listPlayblast( dirName );

def openFolder( *args ):
    currentPath = pm.textField( 'currentPath', q = True , tx = True );
    os.startfile( currentPath );

def openFolderIM( *args ):
    currentPath = pm.textField( 'currentPath', q = True , tx = True );
    imageFolder = currentPath + '/Image/';
    os.startfile( imageFolder );

def listPlayblast( playblastPath = '', *args ):
    fileListsVDO = [ f for f in os.listdir(playblastPath) if os.path.isfile(os.path.join(playblastPath,f)) ];
    if os.path.isdir(playblastPath + '/Image/'):
        fileListsIM = os.listdir( playblastPath + '/Image/' );
    else :
        fileListsIM = [];

    if fileListsIM == []:
        pm.textScrollList( 'previewListsIM', e = True , ra = True );
    else :
        pm.textScrollList( 'previewListsIM', e = True , ra = True );
        pm.textScrollList( 'previewListsIM', e = True , a = fileListsIM );

    if fileListsVDO == []:
        pm.textScrollList( 'previewListsVDO', e = True , ra = True );
    else :
        pm.textScrollList( 'previewListsVDO', e = True , ra = True );
        pm.textScrollList( 'previewListsVDO', e = True , a = fileListsVDO );
        
def getFilesName( *args ):
    getFile = os.path.normpath( mc.file( q = True, sn = True )).split('\\');
    try:
        getSceneFile = getFile[-1];
        sceneName = getSceneFile.split('.ma')[0];
    except:
        getSceneFile = 'untitled';
        sceneName = getSceneFile;

    pm.textField( 'fileNameVD', e = True, tx = sceneName );
    
    return sceneName;

def getFilesNameImage( *args ):
    getFile = os.path.normpath( mc.file( q = True, sn = True )).split('\\');
    try:
        getSceneFile = getFile[-1];
        sceneName = getSceneFile.split('.ma')[0];
    except:
        getSceneFile = 'untitled';
        sceneName = getSceneFile;

    username = getUsername();
    if username != '':
        newFileNameIM = username + '.' + sceneName;
    else :
        newFileNameIM = sceneName;
    pm.textField( 'fileNameIM', e = True, tx = newFileNameIM );

    return newFileNameIM;

def increaseImageSubversion( *args ):
    playblastPath = pm.textField( 'currentPath', q = True , text = True );
    getNameIM = pm.textField( 'fileNameIM', q = True, tx = True );
    if os.path.isdir(playblastPath + '/Image/'):
        folderListsIM = os.listdir( playblastPath + '/Image/' );
    else :
        folderListsIM = [];
    newSubversion = str(len(folderListsIM)+1).zfill(3);
    newNameIM = getNameIM + '.v001_' + newSubversion  + '.image';
    pm.textField( 'fileNameIM', e = True, text = newNameIM );

def imageFolderType( *args ):
    playblastPath = pm.textField( 'currentPath', q = True , text = True );
    folderListsIM = os.listdir( playblastPath + '/Image/' );
    if pm.optionMenu ( 'imageFolderType' , q = True , v = True ) == 'Subversion':
        newSubversion = str(len(folderListsIM)+1).zfill(3);
        newNameIM = getFilesNameImage() 
        renameIM = newNameIM + '.v001_' + newSubversion  + '.image';
        pm.textField( 'fileNameIM', e = True, text = renameIM );
    else :
        folderListsIM = os.listdir( playblastPath + '/Image/' );
        pm.textField( 'fileNameIM', e = True, text = folderListsIM[-1] );

def setUsername( *args ):
    username = pm.textField( 'userNameTF', q = True, text = True );
    prefPath = createFolderTxt();
    setUsernameTxt = open( prefPath + '/setUsername.txt', 'w' );
    setUsernameTxt.write( username );
    setUsernameTxt.close();
    pm.textField( 'userNameTF', e = True, en = False );
    pm.checkBox( 'userNameTxtCB', e = True, v = False );

    version = pm.textField( 'versionTF', q = True, text = True );
    subVersion = pm.textField( 'subVersionTF', q = True, text = True );
    newFileNamePB = username + '.' + getFilesName() + '.v' + version + '_' + subVersion;
    pm.textField( 'fileNameVD', text = newFileNamePB, e = True );

    getFilesNameImage();

def getUsername( *args ):
    prefPath = createFolderTxt();
    username = ""; 
    if os.path.isfile( prefPath + '/setUsername.txt' ):
        userTxt = open( prefPath + '/setUsername.txt', 'r' );
        username = userTxt.read();
        userTxt.close();
    else :
        userTxt = open( prefPath + '/setUsername.txt', 'w' );
        userTxt.write("");
        userTxt.close();
    return username;

def increaseVersion( *args ):
    playblastPath = pm.textField( 'currentPath', q = True , text = True );
    getVersionMov = checkVersion(playblastPath, '.mov', True, True );
    arrVersionMov = getVersionMov.split('_');
    setVersion( arrVersionMov );

def currentVersion( *args ):
    playblastPath = pm.textField( 'currentPath', q = True , text = True );
    getVersionMov = checkVersion(playblastPath, '.mov', False, True );
    arrVersionMov = getVersionMov.split ('_');
    setVersion( arrVersionMov );

def setVersion( arrVersion = [], *args ):
    pm.textField( 'versionTF', text = arrVersion[0], e = True, en = False );
    pm.textField( 'subVersionTF', text = arrVersion[1], e = True, en = False );
    fileNameVD = pm.textField( 'fileNameVD', q = True, text = True );
    username = getUsername();
    if username != '':
        newFileNamePB = username + '.' + getFilesName() + '.v' + arrVersion[0] + '_' + arrVersion[1];
    else :
        newFileNamePB = getFilesName() + '.v' + arrVersion[0] + '_' + arrVersion[1];
    pm.textField( 'fileNameVD', text = newFileNamePB, e = True  );

def checkVersion(
    path                    = '' ,
    name                    = '' ,
    incrementVersion        = False,
    incrementSubVersion     = False,
    origin                  = False,
    *args ):
    
    if os.path.exists( path ) == False:
        os.makedirs( path );
    else : pass;

    allFile = os.listdir( path );
    versionList = [];
    
    for each in allFile:
        if name in str(each):
            versionList.append( each );
        else : pass;
    
    if len ( versionList ) == 0:
        version = '001';
        subVersion = '001';
    else :
        versionList.sort( );
        latestVersion = versionList [-1];
        latestVersionSplit = latestVersion.split( '_' );
        latestVersion = [];
        latestVersion.extend( [ latestVersionSplit[(len(latestVersionSplit)-2)], latestVersionSplit[(len(latestVersionSplit)-1)]]);
        # print latestVersionSplit;
        # print latestVersion;
        finalVertion = latestVersion[-2].split('.');

        version = int( re.findall( '[0-9]{3}',  finalVertion[ -1 ])[0] );
        subVersion = int( re.findall( '[0-9]{3}', latestVersion[ -1 ])[0] );

        if incrementSubVersion == True:
            subVersion += 1 ;
            subVersion = str(subVersion).zfill(3);
        else :
            subVersion = str(subVersion).zfill(3);
            
        if incrementVersion == True:
            version += 1;
            version = str(version).zfill(3);
            subVersion = '001';
        # overwrite increment subversion
        else :
            version = str(version).zfill(3);

        # if this is recache of origin cache
        if latestVersion[1] == '000':
            version = int(version);
            version -= 1 ;
            version = str(version).zfill(3);
        else : pass;

    # if this is origin cache
    if origin == True:
        subVersion = '000';
    else : pass;

    return str(version) + '_' + str(subVersion);
        
def playButton( *args ):
    itemVideo = pm.textScrollList( 'previewListsVDO',  q = True, selectItem = True )[0];
    playblastPath = pm.textField( 'currentPath', q = True, text = True );

    filePath = playblastPath + "/" + itemVideo;
    # print filePath;
    realPath = filePath.replace("/", "\\");
    # print realPath;
    if itemVideo :
        subprocess.Popen( [mpc_hc, realPath] );#quickTime
    else :
        mc.warning( 'No Select List' );

def deleteButton( *args ):
    itemVideos = pm.textScrollList( 'previewListsVDO',  q = True, selectItem = True );
    playblastPath = pm.textField( 'currentPath', q = True, text = True );

    if itemVideos :
        for itemVideo in itemVideos :
            os.remove( playblastPath + "\\" + itemVideo ); 
            listPlayblast( playblastPath );
    else :
        mc.warning( 'No Select List' );
    currentVersion();

def getTimeRange( *args ):
    try:
        from rftool.preload import duration_check
        from rf_utils.sg import sg_utils
        from rf_utils.context import context_info
    except:
        pass

    try:
        file_name = mc.file(q=True, sn=True)
        project = file_name.split('/')[1]
        entity  = file_name.split('/')[3]

        context = context_info.Context()
        context.use_sg(sg_utils.sg, project, 'scene', entityName= entity)
        scene = context_info.ContextPathInfo(context=context)
        sgDuration = duration_check.get_duration(scene)
        startFrame = scene.projectInfo.scene.start_frame
        endFrame = startFrame + (sgDuration - 1);
    except:
        startFrame = pm.playbackOptions( q=True, min=True );
        endFrame = pm.playbackOptions( q=True, max=True );

    startFrameTF = pm.intField( 'startTF', e = True, value = startFrame );
    endFrameTF = pm.intField( 'endTF' , e = True , value = endFrame );

def camResolution( *args ):
    widthTxt = int(pm.textField( 'widthTF', q = True, text = True ));  
    heightTxt = int(pm.textField( 'heightTF', q = True, text = True ));
    prefPath = createFolderTxt();
    setWidthTxt = open( prefPath + '/setCamResolution.txt', 'w' );
    setWidthTxt.write( str(widthTxt) + '#' + str(heightTxt) );
    setWidthTxt.close();
    pm.textField( 'widthTF', e = True, en = False );
    pm.textField( 'heightTF', e = True, en = False );
    pm.checkBox( 'resolutionTxtCB', e = True, v = False );

    aspectRatio = float(widthTxt) / float(heightTxt);

    defaultResolution = pm.general.PyNode( 'defaultResolution' );
    #print defaultResolution
    defaultResolution.aspectLock.set( 0 );
    defaultResolution.width.set( widthTxt );
    defaultResolution.height.set( heightTxt );
    defaultResolution.dotsPerInch.set( 72 );
    defaultResolution.deviceAspectRatio.set( aspectRatio );

    pm.currentUnit( time = 'film' );
    pm.select( defaultResolution );
    resolutionGate();

def getCamResolution( *args ):
    prefPath = createFolderTxt();
    resolution = "3840#2160" ; 
    if os.path.isfile( prefPath + '/setCamResolution.txt' ):
        resolutionTxt = open( prefPath + '/setCamResolution.txt', 'r' );
        resolution = resolutionTxt.read();
        resolutionTxt.close();
    else :
        resolutionTxt = open( prefPath + '/setCamResolution.txt', 'w' );
        resolutionTxt.write( resolution );
        resolutionTxt.close();

    width , height = resolution.split('#');
    return int(width) , int(height);

def resolutionGate( *args ):
    currCamera = activeCamera();
    mc.camera( currCamera, e=True, ff='fill' );
    status = pm.camera( currCamera , q = True , displayResolution = True );

    camera = pm.general.PyNode( currCamera );
    # print camera
    if camera.overscan.get() != 1 :
        camera.overscan.unlock() ;
        pm.disconnectAttr ( camera.overscan ) ;

    if status != True :
        pm.camera( currCamera , e = True, displayFilmGate = False, displayResolution = True, overscan = 1.3 )
    else :
        pm.camera( currCamera , e = True, displayFilmGate = False, displayResolution = False, overscan = 1.0 );

    camera = pm.general.PyNode( currCamera );
    camera.displayGateMaskColor.set( 0, 0, 0 );
    camera.displayGateMaskOpacity.set( 1 );

def activeCamera( *args ):
    import maya.OpenMaya as OpenMaya;
    import maya.OpenMayaUI as OpenMayaUI;

    view = OpenMayaUI.M3dView.active3dView();
    cam = OpenMaya.MDagPath();
    view.getCamera(cam);
    camPath = cam.fullPathName();

    return camPath;

def activePanelShow( switch = True, *args ):
    mayaVersion = mel.eval( 'getApplicationVersionAsFloat' );
    if switch == True :
        activePanel = mc.getPanel( wf = True );
        if 'modelPanel' not in activePanel:
            activePanel = 'modelPanel4';
        mc.modelEditor( activePanel, e = True, imagePlane = True );
    elif switch == False :
        activePanel = mc.getPanel( wf = True );
        if 'modelPanel' not in activePanel:
            activePanel = 'modelPanel4';
        mc.modelEditor( activePanel, e = True, imagePlane = False );

def pmcurrTime( *args ):
    curr = pm.currentTime( q = True );
    return int(curr);

def anti_aliasing( *args ):
    hardwareRenderingGlobals = pm.general.PyNode('hardwareRenderingGlobals') ;
    hardwareRenderingGlobals.multiSampleEnable.set(1);
    hardwareRenderingGlobals.multiSampleCount.set(int(16));

def playBlastButton( *args ):
    extention = "mov";
    path = pm.textField( 'currentPath', q = True, text = True );
    fileName = pm.textField( 'fileNameVD', q = True, text = True );
    userName = pm.textField( 'userNameTF', q = True , text = True );
    width , height = getCamResolution();
    activePanelShow(True);
    visibilityHUD(False);
    anti_aliasing(); 
    #createWaterMark
    if pm.radioCollection( 'wmSwitch', q = True, sl = True ) == 'wmSwitchOn':
        setWatermark();
    
    filePath = os.path.normpath( mc.file( q = True, sn = True ));
    sceneName = filePath.split( '\\' )[-1];
    
    startTime = pm.intField( 'startTF', q = True , value = True );
    endTime = pm.intField( 'endTF', q = True , value = True );
    range = (endTime - startTime) + 1;
    
    activeModelView = pm.playblast( ae = True );
    activeModelView = activeModelView.split('|')[-1];
    camera = pm.modelPanel( activeModelView, q = True, cam = True );
    cameraShape = camera;
    
    disResolution = pm.getAttr( cameraShape + '.displayResolution' );
    disGateMask = pm.getAttr( cameraShape + '.displayGateMask' );
    disGateMaskOpacity = pm.getAttr( cameraShape + '.displayGateMaskOpacity' );
    disGateMaskColor = pm.getAttr( cameraShape + '.displayGateMaskColor' );
    disOverScan = pm.getAttr( cameraShape + '.overscan' );

    realPath = path.replace("/", "\\");

    if width > 1920 or height > 1080 :
        percent = 50;
    else :
        percent = 100;
    
    clearHUD();
    if pm.radioCollection( 'hudSwitch', q = True, sl = True ) == 'hudSwitchOn':
        pm.headsUpDisplay( 'HudSCN', s = 5, b = 0, bs = 'small', l = 'Scene Name  :  ' + sceneName );
        pm.headsUpDisplay( 'HudUSN', s = 5, b = 1, bs = 'small', l = 'User  :  ' + userName );
        pm.headsUpDisplay( 'HudFRN', s = 9, b = 0, bs = 'small', l = 'Frame Range  :   %s  -  %s  ( %s )' %(startTime, endTime, range));
        pm.headsUpDisplay( 'HudFNB', s = 9, b = 1, bs = 'small', l = 'Frame : ',c = pmcurrTime, atr = True );
    
    cmdPlayblast = {
                          "filename"       : realPath + "\\" + fileName,
                          "startTime"      : startTime,
                          "endTime"        : endTime,
                          "format"         : "qt", 
                          "widthHeight"    : ( width, height ),
                          "quality"        : 100, 
                          "percent"        : percent, 
                          "framePadding"   : 4, 
                          "forceOverwrite" : True,
                          "clearCache"     : True, 
                          "viewer"         : True, 
                          "showOrnaments"  : True,
                          "compression"    : "MPEG-4 Video",
                          "offScreen"      : True,
                    };

    pm.playblast(**cmdPlayblast);
    clearHUD();
    
    currentVersion();
    playblastPath = pm.textField( 'currentPath', q = True, text = True );
    listPlayblast( playblastPath );

    if pm.radioCollection( 'wmSwitch', q = True, sl = True ) == 'wmSwitchOn':
        mc.delete('imagePlane2'); # watermarkDefault

    visibilityHUD(True);
    activePanelShow(False);

def playblastImage( *args ):
    extention = "tif";
    fromPath = pm.textField( 'currentPath', q = True, text = True );
    imName = fromPath.split( '/' )
    imFolderName = pm.textField( 'fileNameIM', q = True, text = True );
    path = fromPath + '/Image/' + imFolderName + '/' + imName[4];
    userName = pm.textField( 'userNameTF', q = True , text = True );
    width , height = getCamResolution();
    activePanelShow(True);
    visibilityHUD(False);
    anti_aliasing(); 
    # createWaterMark
    if pm.radioCollection( 'wmSwitch', q = True, sl = True ) == 'wmSwitchOn':
        setWatermark();
    
    filePath = os.path.normpath( mc.file( q = True, sn = True ));
    sceneName = filePath.split( '\\' )[-1];
    
    startTime = pm.intField( 'startTF', q = True , value = True );
    endTime = pm.intField( 'endTF', q = True , value = True );
    range = (endTime - startTime) + 1;
    
    activeModelView = pm.playblast( ae = True );
    activeModelView = activeModelView.split('|')[-1];
    camera = pm.modelPanel( activeModelView, q = True, cam = True );
    cameraShape = camera;
    
    disResolution = pm.getAttr( cameraShape + '.displayResolution' );
    disGateMask = pm.getAttr( cameraShape + '.displayGateMask' );
    disGateMaskOpacity = pm.getAttr( cameraShape + '.displayGateMaskOpacity' );
    disGateMaskColor = pm.getAttr( cameraShape + '.displayGateMaskColor' );
    disOverScan = pm.getAttr( cameraShape + '.overscan' );

    realPath = path.replace("/", "\\");

    if width > 1920 or height > 1080 :
        percent = 50;
    else :
        percent = 100;
    
    clearHUD();
    if pm.radioCollection( 'hudSwitch', q = True, sl = True ) == 'hudSwitchOn':
        pm.headsUpDisplay( 'HudSCN', s = 5, b = 0, bs = 'small', l = 'Scene Name  :  ' + sceneName );
        pm.headsUpDisplay( 'HudUSN', s = 5, b = 1, bs = 'small', l = 'User  :  ' + userName );
        pm.headsUpDisplay( 'HudFRN', s = 9, b = 0, bs = 'small', l = 'Frame Range  :   %s  -  %s  ( %s )' %(startTime, endTime, range));
        pm.headsUpDisplay( 'HudFNB', s = 9, b = 1, bs = 'small', l = 'Frame : ',c = pmcurrTime, atr = True );
    
    cmdPlayblast = {
                          "filename"       : realPath,
                          "startTime"      : startTime,
                          "endTime"        : endTime,
                          "format"         : "image", 
                          "widthHeight"    : ( width, height ),
                          "quality"        : 100, 
                          "percent"        : percent, 
                          "framePadding"   : 4, 
                          "clearCache"     : True, 
                          "viewer"         : False, 
                          "showOrnaments"  : False,
                          "offScreen"      : True,
                          "compression"    : extention,
                    };

    pm.playblast(**cmdPlayblast);
    clearHUD();
    playblastPath = pm.textField( 'currentPath', q = True, text = True );
    listPlayblast( playblastPath );

    if pm.radioCollection( 'wmSwitch', q = True, sl = True ) == 'wmSwitchOn':
        mc.delete('imagePlane2'); # watermarkDefault

    visibilityHUD(True);
    activePanelShow(False);
    getFilesNameImage();
    imageFolderType();

def clearHUD( *args ):
    pm.headsUpDisplay(rp = ( 5, 0 ));
    pm.headsUpDisplay(rp = ( 5, 1 ));
    pm.headsUpDisplay(rp = ( 9, 0 ));
    pm.headsUpDisplay(rp = ( 9, 1 ));

def visibilityHUD( visHUD = False, *args ):
    if visHUD == False:
        mel.eval( 'setAnimationDetailsVisibility(0)' );
        mel.eval( 'setCameraNamesVisibility(0)' );
        mel.eval( 'setCapsLockVisibility(0)' );
        mel.eval( 'setCurrentContainerVisibility(0)' );
        mel.eval( 'setCurrentFrameVisibility(0)' );
        mel.eval( 'setFocalLengthVisibility(0)' );
        mel.eval( 'setFrameRateVisibility(0)' );
        mel.eval( 'setHikDetailsVisibility(0)' );
        mel.eval( 'setObjectDetailsVisibility(0)' );
        mel.eval( 'setParticleCountVisibility(0)' );
        mel.eval( 'setPolyCountVisibility(0)' );
        mel.eval( 'setSceneTimecodeVisibility(0)' );
        mel.eval( 'setSelectDetailsVisibility(0)' );
        mel.eval( 'setSymmetryVisibility(0)' );
        mel.eval( 'setViewAxisVisibility(0)' );
        mel.eval( 'setViewportRendererVisibility(0)' );
        # mel.eval( 'setXGenHUDVisibility(0)' );
    elif visHUD == True: 
        mel.eval( 'setCameraNamesVisibility(1)' );
        mel.eval( 'setCurrentFrameVisibility(1)' );

def setWatermark( switch = True, *args ):
    waterMarkFile = imagePath + "/watermark/RiFFStudio.png";
    currCamera = activeCamera();
    activeView = currCamera.split('|');
    cameraName = activeView[-1];
    imgPlane = pm.imagePlane( camera = cameraName, n = "imagePlane1" );
    mc.camera( cameraName, e=True, ff='fill' );
    mc.setAttr( imgPlane[1]+".fit", 1);
    mc.setAttr( imgPlane[1]+".depth", 1);
    mc.setAttr( imgPlane[1]+".alphaGain", 0.2);
    mc.setAttr( imgPlane[1]+".imageName", waterMarkFile, type="string");
    mc.select('imagePlane2', d = True);

def playblastTools( version = '1.0' ):
    width = 501;
    
    if pm.window( 'PlayblastUI', exists = True ):
        pm.deleteUI( 'PlayblastUI' );
        
    window = pm.window( 'PlayblastUI', title = "Playblast Tools  %s" %version, width = width,
        mnb = True, mxb = False, sizeable = False, rtf = True );

    window = pm.window( 'PlayblastUI', e = True, width = width, height = 440 );

    videoLayout = pm.rowColumnLayout( h = 15,w = width, nc = 1, columnWidth = ( 1, width ) ) ;
    with videoLayout :
        pm.text( label = ' PLAYBLAST VIDEO ', bgc = (0.411765, 0.411765, 0.411765) );
    
    separator1Layout = pm.rowColumnLayout( h = 1,w = width, nc = 1, columnWidth = ( 1, 497 ) );
    with separator1Layout :
        pm.separator( h = 1, st = 'in' );

    listLayout = pm.rowColumnLayout( w = width, nc = 1, columnWidth = ( 1, width ) );
    with listLayout :
        pm.textScrollList( 'previewListsVDO', h = 100, numberOfRows = 20, allowMultiSelection = 1, dcc = playButton );
        pm.separator( vis = False, h = 3 );

    imLayout = pm.rowColumnLayout( h = 15,w = width, nc = 1, columnWidth = ( 1, width ) ) ;
    with imLayout :
        pm.text( label = ' PLAYBLAST IMAGE ', bgc = (0.411765, 0.411765, 0.411765) );
    
    separator2Layout = pm.rowColumnLayout( h = 1,w = width, nc = 1, columnWidth = ( 1, 497 ) );
    with separator2Layout :
        pm.separator( h = 1, st = 'in' );

    imlistLayout = pm.rowColumnLayout( w = width, nc = 1, columnWidth = ( 1, width ) );
    with imlistLayout :
        pm.textScrollList( 'previewListsIM', h = 100, numberOfRows = 20, allowMultiSelection = 1, dcc = openFolderIM );
        pm.separator( vis = False, h = 3 );
    
    buttonLayout = pm.rowColumnLayout( h = 30, w = width, nc = 3, columnWidth = [ ( 1, 246 ), ( 2, 5 ), ( 3, 246 ) ] );
    with buttonLayout :
        pm.shelfButton( style = 'iconOnly', image = imagePath + "/buttonIcon/play.png", bgc = ( 0.44, 0.78, 0.44 ), h = 30, c = playButton );
        pm.separator( vis = False )
        pm.shelfButton( style = 'iconOnly', image = imagePath + "/buttonIcon/delete.png", bgc = ( 0.9, 0.25, 0.25 ), h = 30, c = deleteButton );
    
    separator3Layout = pm.rowColumnLayout( h = 10,w = width, nc = 1, columnWidth = ( 1, 497 ));
    with separator3Layout :
        pm.separator( h = 5, st = 'in' );

    with pm.scrollLayout ( childResizable = True, h = 170) :
    
        localLayout = pm.rowColumnLayout( h = 25,w = width, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 65 ), ( 2, 355 ), ( 3, 5 ), ( 4, 65 )]) ;
        with localLayout :
            pm.text( label = ' Local Disk :' );
            pm.textField( 'inputPath', en = False, text = '' );
            pm.separator( vis = False );
            pm.button( label = 'Browse', h = 5, c = browsePath );

        separator4Layout = pm.rowColumnLayout( h = 10,w = width, nc = 1, columnWidth = ( 1, 497 ));
        with separator4Layout :
            pm.separator( h = 5, st = 'in' );

        pathLayout = pm.rowColumnLayout( h = 25,w = width, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 60 ), ( 2, 400 ), ( 3, 5 ), ( 4, 25 )]) ;
        with pathLayout :
            pm.text( label = ' Path File :' );
            pm.textField( 'currentPath', en = False, text = '' );
            pm.separator( vis = False );
            pm.shelfButton( style = 'iconOnly', image = imagePath + "/buttonIcon/folder.png", bgc = ( 0.9, 0.9, 0.1 ), h = 1, c = openFolder );
            separatorPathLayout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
            with separatorPathLayout :
                pm.separator( h = 5, st = 'in' );

        pm.columnLayout("columnLayoutName01", adjustableColumn = True)
        frameLayout = pm.frameLayout ("layoutFrame01", label = "Playblast Options", collapsable = True, collapse = True, w = width, parent = "columnLayoutName01")
        with frameLayout : 
            separator5Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
            with separator5Layout :
                pm.separator( h = 5, st = 'in' );
                
            fileNameLayout = pm.rowColumnLayout( h = 25, w = width, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 75 ), ( 2, 325 ), ( 3, 5 ), ( 4, 90 ) ]) ;
            with fileNameLayout :
                pm.text( label = ' Video Name :' );
                pm.textField( 'fileNameVD', en = False );
                pm.separator( vis = False );
                pm.checkBox( 'overrideFileNameCB', l = 'Edit file name', onc = "mc.textField( 'fileNameVD', e = True, en = True )" 
                                                                        , ofc = "mc.textField( 'fileNameVD', e = True, en = False )" );

            separator6Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
            with separator6Layout :
                pm.separator( h = 5, st = 'in' );

            ImageNameLayout = pm.rowColumnLayout( h = 25, w = width, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 110 ), ( 2, 290 ), ( 3, 5 ), ( 4, 90 ) ]) ;
            with ImageNameLayout :
                pm.text( label = ' Image Folder Name :' );
                pm.textField( 'fileNameIM', en = False );
                pm.separator( vis = False );
                pm.checkBox( 'overrideFileNameCB', l = 'Edit file name', onc = "mc.textField( 'fileNameIM', e = True, en = True )" 
                                                                        , ofc = "mc.textField( 'fileNameIM', e = True, en = False )" );

            separator7Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
            with separator7Layout :
                pm.separator( h = 5, st = 'in' );

            addUserNameLayout = pm.rowColumnLayout( h = 25, w = width, nc = 8, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 125 ), ( 3, 5 ), ( 4, 20 ), ( 5, 65 ), ( 6, 5 ), ( 7, 80 ), ( 8, 100 )]);
            with addUserNameLayout :
                pm.text( label = ' Add Username :' );
                pm.textField( 'userNameTF', text = '', en = False );
                pm.separator( vis = False );
                pm.checkBox( 'userNameTxtCB', l = '', v = False, onc = "mc.textField( 'userNameTF', e = True, en = True )" 
                                                                , ofc = "mc.textField( 'userNameTF', e = True, en = False )" );
                pm.button( label = 'SET', h = 5, c = setUsername );
                pm.separator( vis = False );
                pm.text( label = ' Image Folder :' );
                with pm.optionMenu( 'imageFolderType', label = '' , cc = imageFolderType) :
                    pm.menuItem ( label='Subversion') ;
                    pm.menuItem ( label='Replace') ;

            separator8Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
            with separator8Layout :
                pm.separator( h = 5, st = 'in' );
                
            versionLayout = pm.rowColumnLayout( h = 25, w = width, nc = 7, columnAlign = (1, 'left'), columnWidth = [ ( 1, 85 ), ( 2, 130 ), ( 3, 5 ), ( 4, 105 ), ( 5, 5 ), ( 6, 80 ), ( 7, 80 ) ]);
            with versionLayout :
                pm.text( label = ' Video Version :' );
                pm.textField( 'versionTF', en = False );
                pm.separator( vis = False );
                pm.checkBox( 'increaseVersionCB', l = 'Increase Version',  v = False, onc = increaseVersion, ofc = currentVersion );
                pm.separator( vis = False );
                pm.text( label = 'Subversion :' );
                pm.textField( 'subVersionTF', en = False );

            separator9Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
            with separator9Layout :
                pm.separator( h = 5, st = 'in' );

            resolutionLayout = pm.rowColumnLayout( h = 25, w = width, columnAlign = (1, 'left'), nc = 14, columnWidth = [ ( 1, 95 ), ( 2, 50 ), ( 3, 20 ), ( 4, 50 ), ( 5, 5 ), ( 6, 20 ), ( 7, 65 ), ( 8, 10 ), ( 9, 65 ), ( 10, 10 ), ( 11, 40 ), ( 12, 10 ), ( 13, 10 ), ( 14, 40 )]);
            with resolutionLayout :
                pm.text( label = ' Resolution Gate :' );
                pm.textField( 'widthTF', en = False );
                pm.text( label = 'X' );
                pm.textField( 'heightTF', en = False );
                pm.separator( vis = False );
                pm.checkBox( 'resolutionTxtCB', l = '', v = False, onc = "mc.textField( 'widthTF', e = True, en = True ), mc.textField( 'heightTF', e = True, en = True )" 
                                                                , ofc = "mc.textField( 'widthTF', e = True, en = False ), mc.textField( 'heightTF', e = True, en = False )" );
                pm.button( label = 'SET', h = 5, c = camResolution );
                pm.separator( vis = False );
                pm.text( label = 'Watermark :' );
                pm.separator( vis = False );
                pm.radioCollection( 'wmSwitch' );
                pm.radioButton( 'wmSwitchOff', l = 'Off');
                pm.separator( vis = False );
                pm.radioCollection( 'wmSwitch', e = True, select = 'wmSwitchOff' );
                pm.separator( vis = False );
                pm.radioButton( 'wmSwitchOn', l = 'On');

            separator10Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
            with separator10Layout :
                pm.separator( h = 5, st = 'in' );

            resolutionLayout = pm.rowColumnLayout( h = 25, w = width, columnAlign = (1, 'left'), nc = 11, columnWidth = [ ( 1, 100 ), ( 2, 5 ), ( 3, 55 ), ( 4, 10 ), ( 5, 5 ), ( 6, 60 ), ( 7, 75 ), ( 8, 40 ), ( 9, 50 ), ( 10, 40 ), ( 11, 50 )]);
            with resolutionLayout :

                pm.text( label = ' Heads Up Display :' );
                pm.separator( vis = False );
                pm.radioCollection( 'hudSwitch' );
                pm.radioButton( 'hudSwitchOn', l = 'Show');
                pm.separator( vis = False );
                pm.radioCollection( 'hudSwitch', e = True, select = 'hudSwitchOn' );
                pm.separator( vis = False );
                pm.radioButton( 'hudSwitchOff', l = 'Hide');
                pm.text( label = ' Time Range :' );
                pm.text( label = ' Start' );
                pm.intField( 'startTF' );
                pm.text( label = ' End' );
                pm.intField( 'endTF' );

        separator11Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
        with separator11Layout :
            pm.separator( h = 5, st = 'in' );

        subLayout = pm.rowColumnLayout( w = width, nc = 2, columnWidth = [ ( 1, width / 2.0 ), ( 2, width / 2.0 )]);
        with subLayout :    
            subLeftLayout = pm.rowColumnLayout( w = width / 2.0, nc = 1, columnWidth = ( 1, width / 2.0 ));
            with subLeftLayout :
                buttonPlayblastIMLayout = pm.rowColumnLayout( w = width, nc = 1, columnWidth = ( 1, 244 ));
                with buttonPlayblastIMLayout :
                    pm.shelfButton( label = 'PLAYBLAST IMAGE', style = 'iconAndTextVertical', image = imagePath + "/buttonIcon/photo-camera.png", h = 50, bgc = (0.98, 0.98, 0.90), c = playblastImage ); 
                    
            subRightLayout = pm.rowColumnLayout( w = width / 2.0, nc = 1, columnWidth = ( 1, width / 2.0 ));
            with subRightLayout :
                buttonPlayblastLayout = pm.rowColumnLayout( w = width, nc = 1, columnWidth = ( 1, 244 ));
                with buttonPlayblastLayout :
                    pm.shelfButton( label = 'PLAYBLAST VIDEO', style = 'iconAndTextVertical', image = imagePath + "/buttonIcon/video-camera.png", h = 50, bgc = (0.98, 0.90, 0.98), c = playBlastButton );   
            
        pm.showWindow( window );

def setDefaultUI( *args ):
    getFilesName();
    username = getUsername();
    if username != '':
        pm.textField ( 'userNameTF', e = True, text = username );
        setUsername();

    getFilesNameImage();

    width , height = getCamResolution();
    pm.textField( 'widthTF', e = True, text = str(width) );  
    pm.textField( 'heightTF', e = True, text = str(height) );
    
    localPath = getDefaultPath();
    if localPath != '':
        pm.textField( 'inputPath', e = True, tx = localPath );

    playblastPath();
    currentVersion();
    getTimeRange();
    increaseImageSubversion();