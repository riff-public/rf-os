import maya.cmds as mc;
import pymel.core as pm;
import os, re, ast;
import subprocess;
import maya.mel as mel;

currentProject = pm.workspace( q = True , rd = True );
shotCachePath = currentProject + 'data/shotCache/';
pm.cycleCheck( e = 0 ) ;

def getPathGeo( *args ):
    try :
        shotCacheList = os.listdir ( shotCachePath );
    except :
        shotCacheList = [] ;

    currentChar = currentProject.split ( '/' ) [-2];
    # print currentChar;
    selfPathGeo = currentProject + 'data/shotCache/';

    for shotCache in shotCacheList :
        if '.'+currentChar+'.' in shotCache :
            selfPathGeo += shotCache ;
        else : pass ;
    pm.textField( 'geoPath', e = True, tx = selfPathGeo );
    return selfPathGeo;
    
def importGeo( *args ):
    selfPathGeo = getPathGeo();
    fileNameGeo = mc.fileDialog2 ( dir = selfPathGeo, fileMode = 4, ff="*abc", okc="Import" );
    pathGeo = mc.file(fileNameGeo, i=True, uns=False);

def getPathCam( *args ):
    try :
        shotCacheList = os.listdir ( shotCachePath );
    except :
        shotCacheList = [];

    selfPathCam = shotCachePath;
    for shotCache in shotCacheList :
        if '.cam.' in shotCache :
            selfPathCam = shotCachePath + shotCache;
        else : pass;
    pm.textField( 'camPath', e = True, tx = selfPathCam );
    return selfPathCam;

def importCam( *args ):
    selfPathCam = getPathCam();
    fileNameCam = mc.fileDialog2 ( dir = selfPathCam, fileMode = 4, ff="*abc", okc="Import" );
    pathCam = mc.file(fileNameCam, i=True, uns=False);

def getPathProp( *args ):
    try :
        shotCacheList = os.listdir ( shotCachePath );
    except :
        shotCacheList = [] ;

    selfPathProp = shotCachePath;
    for shotCache in shotCacheList :
        if '.prop.' in shotCache :
            selfPathProp = shotCachePath + shotCache;
        else : pass;
    pm.textField( 'propPath', e = True, tx = selfPathProp );
    return selfPathProp;

def importProp( *args ):
    selfPathProp = getPathProp();
    fileNameProp = mc.fileDialog2 ( dir = selfPathProp, fileMode = 4, ff="*abc", okc="Import" );
    pathProp = mc.file(fileNameProp, i=True, uns=False);

def getPathAbc( *args ):
    selfPathAbc = currentProject + 'cache' ;
    pm.textField( 'abcPath', e = True, tx = selfPathAbc );
    return selfPathAbc;

def importAbc( *args ):
    selfPathAbc = getPathAbc();
    fileNameAbc = mc.fileDialog2 ( dir = selfPathAbc, fileMode = 4, ff="*abc", okc="Import" );
    pathAbc = mc.file(fileNameAbc, i=True, uns=False);

def currentChar( *args ):
    try :
        shotCacheList = os.listdir ( shotCachePath );
    except :
        shotCacheList = [] ;

    currentChar = currentProject.split ( '/' ) [-2];
    # print currentChar;
    return currentChar;

def exportShirtPants( *args ):
    currentCharEx = currentChar();
    selfPath = currentProject + 'cache/alembic';
    check = Path( selfPath );
    # print check.checkVersion('1_ShirtPantsDyn');
    ShirtPantsVersion = check.checkVersion('1_ShirtPantsDyn');
    fullnameSP = currentCharEx + '.' '1_ShirtPantsDyn' + '.' + ShirtPantsVersion + '.abc' ;
    pm.textField( 'exportSP', e = True, tx = fullnameSP );

def exportHairDYN( *args ):
    currentCharEx = currentChar();
    selfPath = currentProject + 'cache/alembic';
    check = Path( selfPath );
    # print check.checkVersion('3_HairDyn');
    hairDYNVersion = check.checkVersion('3_HairDyn');
    fullnameHD = currentCharEx + '.' '3_HairDyn' + '.' + hairDYNVersion + '.abc' ;
    pm.textField( 'exportHD', e = True, tx = fullnameHD );

def exportGeoGrp( *args ):
    currentCharEx = currentChar();
    selfPath = currentProject + 'cache/alembic';
    check = Path( selfPath );
    # print check.checkVersion('grpGeo');
    grpGeoVersion = check.checkVersion('grpGeo');
    fullnameCha = grpGeoVersion + '.' + currentCharEx + '.grpGeo' + '.abc';
    pm.textField( 'exportGG', e = True, tx = fullnameCha );


def exportXGen( *args ):
    currentCharEx = currentChar();
    selfPath = currentProject + 'cache/alembic';
    check = Path( selfPath );
    # print check.checkVersion('xGen');
    xGenVersion = check.checkVersion('xGen'); 
    fullnameCha = xGenVersion + '.' + currentCharEx + '.xGen' + '.abc'
    pm.textField( 'exportXG', e = True, tx = fullnameCha );

def abcShirtPants( *args ):
    pathTimeRange = "data";
    selfPathTimeRange = currentProject + pathTimeRange;
    # print selfPathTimeRange
    timeRangeTxt = open( selfPathTimeRange + '/timerange.txt', 'r' );
    timeRange = timeRangeTxt.read();
    # print timeRange ;
    timeRangeTxt.close();

    dictTimeRange = ast.literal_eval( timeRange );
    preRollFrame = dictTimeRange['pre roll'];
    postRollFrame = dictTimeRange['post roll'];

    currentPath = pm.textField( 'exportSP', q = True , tx = True );
    selfPathAbc = currentProject + 'cache/alembic/' + currentPath;

    getrootDYN = pm.general.PyNode('DYN_GRP');
    rootDYN = getrootDYN.longName ( );
    # print selfPathAbc
    abcSP = 'AbcExport -j "-frameRange '+str(preRollFrame)+' '+str(postRollFrame)+' -uvWrite -worldSpace -writeVisibility -dataFormat ogawa -root '+rootDYN+' -file '+selfPathAbc+'"'
    # print abcSP
    mel.eval(abcSP)
    exportShirtPants();

def abcHair( *args ):
    pathTimeRange = "data";
    selfPathTimeRange = currentProject + pathTimeRange;
    # print selfPathTimeRange
    timeRangeTxt = open( selfPathTimeRange + '/timerange.txt', 'r' );
    timeRange = timeRangeTxt.read();
    # print timeRange ;
    timeRangeTxt.close();

    dictTimeRange = ast.literal_eval( timeRange );
    preRollFrame = dictTimeRange['pre roll'];
    postRollFrame = dictTimeRange['post roll'];

    currentPath = pm.textField( 'exportHD', q = True , tx = True );
    selfPathAbc = currentProject + 'cache/alembic/' + currentPath;

    getrootHair = pm.general.PyNode('hair');
    rootHair = getrootHair.longName ( );
    # print rootHair ;
    abcHD = 'AbcExport -j "-frameRange '+str(preRollFrame)+' '+str(postRollFrame)+' -uvWrite -worldSpace -writeVisibility -dataFormat ogawa -root '+rootHair+' -file '+selfPathAbc+'"'
    # print abcHD
    mel.eval(abcHD)
    exportHairDYN();

def abcGeoGrp( *args ):
    pathTimeRange = "data";
    selfPathTimeRange = currentProject + pathTimeRange;
    # print selfPathTimeRange
    timeRangeTxt = open( selfPathTimeRange + '/timerange.txt', 'r' );
    timeRange = timeRangeTxt.read();
    # print timeRange ;
    timeRangeTxt.close();

    dictTimeRange = ast.literal_eval( timeRange );
    preRollFrame = dictTimeRange['pre roll'];
    postRollFrame = dictTimeRange['post roll'];

    currentPath = pm.textField( 'exportGG', q = True , tx = True );
    selfPathAbc = currentProject + 'cache/alembic/' + currentPath;
    
    getrootGeo = pm.general.PyNode('Geo_Grp');
    rootGeo = getrootGeo.longName ( );
    # print selfPathAbc
    abcGG = 'AbcExport -j "-frameRange '+str(preRollFrame)+' '+str(postRollFrame)+' -uvWrite -worldSpace -writeVisibility -dataFormat ogawa -root '+rootGeo+' -file '+selfPathAbc+'"'
    # print abcGG
    mel.eval(abcGG);
    exportGeoGrp();

class Path ( object ) :

    def __init__ ( self , path ) :
        
        self.path = path ;

        if os.path.exists ( self.path ) == False :
            os.makedirs ( self.path ) ;
        else : pass ;

    def __str__ ( self ) :
        return self.path ;

    def __repr__ ( self ) :
        return self.path ;

    # return 'vXXX_XXX'
    def checkVersion ( self , name , incrementVersion = False , incrementSubVersion = True ) :

        allFile = os.listdir ( self.path ) ;
        version_list = [ ] ;

        for each in allFile :
            if name in str ( each ) :
                version = re.findall ( 'v' + '[0-9]{3}' + '_' + '[0-9]{3}' , each ) ;
                try : 
                    version = version[0] ;
                    version_list.append ( version ) ;
                except :
                    pass ;

        version_list.sort ( ) ;

        if len ( version_list ) == 0 :
            currentVersion = 'v001_001' ;
        else :
            currentVersion = version_list[-1] ;

            version = currentVersion.split('_')[0] ;
            version = version.split('v')[-1] ;
            version = int ( version ) ;

            subVersion = currentVersion.split('_')[-1] ;
            subVersion = int ( subVersion ) ;

            if incrementVersion == True :
                version += 1 ;                

            if incrementSubVersion == True :
                subVersion += 1 ;

            version     = str(version) ;
            subVersion  = str(subVersion) ;

            currentVersion = 'v' + version.zfill(3) + '_' + subVersion.zfill(3) ;

        return currentVersion ;

def cacheTools( version = '1.0' ):
    width = 500;
    
    if pm.window( 'CacheUI', exists = True ):
        pm.deleteUI( 'CacheUI' );
        
    window = pm.window( 'CacheUI', title = "Cache Tools  %s" %version, width = width,
        mnb = False, mxb = True, sizeable = False, rtf = True );

    window = pm.window( 'CacheUI', e = True, width = width );
    
    separator1Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
    with separator1Layout :
        pm.separator( h = 5, st = 'in' );

    importCacheLayout = pm.rowColumnLayout( h = 25,w = width, bgc = (0.4, 0.4, 0.4), nc = 1, columnWidth = [ ( 1, 500 )]) ;
    with importCacheLayout :
        pm.text( label = 'Import Cache' );
    
    separator2Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
    with separator2Layout :
        pm.separator( h = 5, st = 'in' );

    characterLayout = pm.rowColumnLayout( h = 25,w = width, columnAlign = (1, 'left'), nc = 4, columnWidth = [ ( 1, 90 ), ( 2, 360 ), ( 3, 10 ), ( 4, 25 )]) ;
    with characterLayout :
        pm.text( label = ' Character :' );
        pm.textField( 'geoPath', en = False, text = '' );
        pm.separator( vis = False );
        pm.shelfButton( style = 'iconOnly', image1 = 'importCache.png', bgc = (0.847059, 0.74902, 0.847059), h = 1, c = importGeo);

    separator3Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
    with separator3Layout :
        pm.separator( h = 5, st = 'in' );

    cameraLayout = pm.rowColumnLayout( h = 25,w = width, columnAlign = (1, 'left'), nc = 4, columnWidth = [ ( 1, 90 ), ( 2, 360 ), ( 3, 10 ), ( 4, 25 )]) ;
    with cameraLayout :
        pm.text( label = ' Camera :' );
        pm.textField( 'camPath', en = False, text = '' );
        pm.separator( vis = False );
        pm.shelfButton( style = 'iconOnly', image1 = 'importCache.png', bgc = (0.847059, 0.74902, 0.847059), h = 1, c = importCam);

    separator4Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
    with separator4Layout :
        pm.separator( h = 5, st = 'in' );

    cameraLayout = pm.rowColumnLayout( h = 25,w = width, columnAlign = (1, 'left'), nc = 4, columnWidth = [ ( 1, 90 ), ( 2, 360 ), ( 3, 10 ), ( 4, 25 )]) ;
    with cameraLayout :
        pm.text( label = ' Prop :' );
        pm.textField( 'propPath', en = False, text = '' );
        pm.separator( vis = False );
        pm.shelfButton( style = 'iconOnly', image1 = 'importCache.png', bgc = (0.847059, 0.74902, 0.847059), h = 1, c = importProp);

    separator5Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
    with separator5Layout :
        pm.separator( h = 5, st = 'in' );

    alembicCacheLayout = pm.rowColumnLayout( h = 25,w = width, columnAlign = (1, 'left'), nc = 4, columnWidth = [ ( 1, 90 ), ( 2, 360 ), ( 3, 10 ), ( 4, 25 )]) ;
    with alembicCacheLayout :
        pm.text( label = ' Alembic Cache :' );
        pm.textField( 'abcPath', en = False, text = '' );
        pm.separator( vis = False );
        pm.shelfButton( style = 'iconOnly', image1 = 'importCache.png', bgc = (0.847059, 0.74902, 0.847059), h = 1, c = importAbc);

    separator6Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
    with separator6Layout :
        pm.separator( h = 5, st = 'in' );

    exportDYNCacheLayout = pm.rowColumnLayout( h = 25,w = width, bgc = (0.4, 0.4, 0.4), nc = 1, columnWidth = [ ( 1, 500 )]) ;
    with exportDYNCacheLayout :
        pm.text( label = 'Export Cache DYN' );

    separator7Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
    with separator7Layout :
        pm.separator( h = 5, st = 'in' );

    shirtPantsDYNLayout = pm.rowColumnLayout( h = 25,w = width, columnAlign = (1, 'left'), nc = 4, columnWidth = [ ( 1, 90 ), ( 2, 360 ), ( 3, 10 ), ( 4, 25 )] );
    with shirtPantsDYNLayout :
        pm.text( label = ' ShirtPants DYN :' );
        pm.textField( 'exportSP', en = True, text = '' );
        pm.separator( vis = False );
        pm.shelfButton( style = 'iconOnly', image1 = 'exportCache.png', bgc = (0.690196, 0.878431, 0.901961), h = 1, c = abcShirtPants);

    separator8Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
    with separator8Layout :
        pm.separator( h = 5, st = 'in' );

    hairDYNLayout = pm.rowColumnLayout( h = 25,w = width, columnAlign = (1, 'left'), nc = 4, columnWidth = [ ( 1, 90 ), ( 2, 360 ), ( 3, 10 ), ( 4, 25 )] );
    with hairDYNLayout :
        pm.text( label = ' Hair DYN :' );
        pm.textField( 'exportHD', en = True, text = '' );
        pm.separator( vis = False );
        pm.shelfButton( style = 'iconOnly', image1 = 'exportCache.png', bgc = (0.690196, 0.878431, 0.901961), h = 1, c = abcHair );

    separator9Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
    with separator9Layout :
        pm.separator( h = 5, st = 'in' );

    exportCacheLayout = pm.rowColumnLayout( h = 25,w = width, bgc = (0.4, 0.4, 0.4), nc = 1, columnWidth = [ ( 1, 500 )]) ;
    with exportCacheLayout :
        pm.text( label = 'Export Cache To Client' );  

    separator10Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
    with separator10Layout :
        pm.separator( h = 5, st = 'in' );

    grpGeoLayout = pm.rowColumnLayout( h = 25,w = width, columnAlign = (1, 'left'), nc = 4, columnWidth = [ ( 1, 90 ), ( 2, 360 ), ( 3, 10 ), ( 4, 25 )] );
    with grpGeoLayout :
        pm.text( label = ' grpGeo :' );
        pm.textField( 'exportGG', en = True, text = '' );
        pm.separator( vis = False );
        pm.shelfButton( style = 'iconOnly', image1 = 'exportCache.png', bgc = ( 0.6, 0.8, 0.6 ), h = 1, c = abcGeoGrp );  

    separator11Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
    with separator11Layout :
        pm.separator( h = 5, st = 'in' );

    xGenLayout = pm.rowColumnLayout( h = 25,w = width, columnAlign = (1, 'left'), nc = 4, columnWidth = [ ( 1, 90 ), ( 2, 360 ), ( 3, 10 ), ( 4, 25 )] );
    with xGenLayout :
        pm.text( label = ' xGen :' );
        pm.textField( 'exportXG', en = True, text = '' );
        pm.separator( vis = False );
        # pm.shelfButton( style = 'iconOnly', image1 = 'exportCache.png', bgc = ( 0.6, 0.8, 0.6 ), h = 1);  

    separator12Layout = pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 497 ));
    with separator12Layout :
        pm.separator( h = 5, st = 'in' );

    pm.showWindow( window );

def setDefaultIC( *args ):
    getPathGeo();
    getPathCam();
    getPathProp();
    getPathAbc();
    exportShirtPants();
    exportHairDYN();
    exportGeoGrp();
    exportXGen();