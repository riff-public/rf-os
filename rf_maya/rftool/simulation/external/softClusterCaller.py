import maya.cmds as mc ;
import pymel.core as pm ;
import sys ;
import os ;

self_path = os.path.realpath (__file__) ;
fileName = os.path.basename (__file__) ;
self_path = self_path.replace ( fileName , '' ) ;
self_path = self_path.replace ( '\\' , '/' ) ;
# D:/Dropbox/script/birdScript/external/

'''

mp = "D:\Dropbox\script\birdScript\external\SoftClusterEX-3.0.1\SoftClusterEX\scripts\SoftClusterEX" ;
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
'''

def run ( *args ) :

	softCluster_path = self_path + 'SoftClusterEX-3.0.1/SoftClusterEX/scripts/' ;
	
	if not softCluster_path in sys.path :
	    sys.path.insert ( 0 , softCluster_path ) ;

	import SoftClusterEX ;

	### load plugin ###

	if pm.pluginInfo ( 'softSelectionQuery.mll' , q = True , loaded = True ) == True : pass ;
	
	else :

		mayaVersion = pm.mel.eval ( 'getApplicationVersionAsFloat' ) ;
		mayaVersion = int ( mayaVersion ) ;

		softClusterPlugin_path = self_path + 'SoftClusterEX-3.0.1/SoftClusterEX/bin/softSelectionQuery/'
		softClusterPlugin_path += '%d/' % mayaVersion ;
		softClusterPlugin_path += 'plug-ins/softSelectionQuery.mll'

		pm.loadPlugin ( softClusterPlugin_path ) ;


	SoftClusterEX.launch() ;

def createCommand_cmd ( *args ) :

	mayaVersion = pm.mel.eval ( 'getApplicationVersionAsFloat' ) ;
	mayaVersion = int ( mayaVersion ) ;

	cmd = '''
###### script start ######

import maya.cmds as mc ;
import pymel.core as pm ;

self_path = '%s' ;

softCluster_path = self_path + 'SoftClusterEX-3.0.1/SoftClusterEX/scripts/' ;
	
if not softCluster_path in sys.path :
    sys.path.insert ( 0 , softCluster_path ) ;

import SoftClusterEX ;

### load plugin ###

if pm.pluginInfo ( 'softSelectionQuery.mll' , q = True , loaded = True ) == True : pass ;

else :

	

	softClusterPlugin_path = self_path + 'SoftClusterEX-3.0.1/SoftClusterEX/bin/softSelectionQuery/'
	softClusterPlugin_path += '%s/' ;
	softClusterPlugin_path += 'plug-ins/softSelectionQuery.mll'

	pm.loadPlugin ( softClusterPlugin_path ) ;


SoftClusterEX.launch() ;

###### script end ######
''' % ( self_path , str(mayaVersion) ) ;
	
	return cmd ;

def addButtonToShelf_cmd ( *args ) :

	cmd = createCommand_cmd ( ) ;

	image_path = self_path + 'SoftClusterEX-3.0.1/SoftClusterEX/icons/softClusterEXIcon.png' ;

	mel_cmd = '''
global string $gShelfTopLevel;
string $shelves = `tabLayout -q -selectTab $gShelfTopLevel`;
'''

	currentShelf = pm.mel.eval ( mel_cmd );

	pm.shelfButton ( style = 'iconOnly' , image = image_path , command = cmd , parent = currentShelf ) ;

	