import maya.cmds as mc

def mirrorShape( *args ):
	searchQuery = mc.textField('searchUI' ,q = True ,tx = True)
	raplaceQuery = mc.textField('replaceUI' ,q = True ,tx = True)
	
	list = mc.ls('*'+searchQuery+'*_ctrl')
	list2 = mc.ls('*'+searchQuery+'*_Ctrl')
	list.extend ( list2 ) ;

	for i in list:
		newCrv = i.replace(searchQuery,raplaceQuery)
		crvShape = mc.listRelatives(i , s = True)
		crvDegree = mc.getAttr(crvShape[0]+'.degree')
		crvSpans = mc.getAttr(crvShape[0]+'.spans')
		crvCvs = crvDegree + crvSpans
		for j in range(0,crvCvs+1):
			pos = mc.xform(i + '.cv[ '+str(j)+ ']' , q = True , ws = True , t = True )
			mc.xform(newCrv + '.cv[ '+str(j)+ ']' , ws = True , t = (-pos[0],pos[1],pos[2]) )

def copyCrvShape( *args ):
	sel = mc.ls(sl = True)
	if len(sel)>1:
		crvShape = mc.listRelatives(sel[0] , s = True)
		crvDegree = mc.getAttr(crvShape[0]+'.degree')
		crvSpans = mc.getAttr(crvShape[0]+'.spans')
		crvCvs = crvDegree + crvSpans
		for j in range(0,crvCvs+1):
			pos = mc.xform(sel[0] + '.cv[ '+str(j)+ ']' , q = True , os = True , t = True )
			for i in range(0,len(sel)):
				if i == 0:
					continue
				else :
					mc.xform(sel[i] + '.cv[ '+str(j)+ ']' , os = True , t = pos )
	else :
		searchQuery = mc.textField('searchUI' ,q = True ,tx = True)
		raplaceQuery = mc.textField('replaceUI' ,q = True ,tx = True)
		newCrv = sel[0].replace(searchQuery,raplaceQuery)
		crvShape = mc.listRelatives(sel[0] , s = True)
		crvDegree = mc.getAttr(crvShape[0]+'.degree')
		crvSpans = mc.getAttr(crvShape[0]+'.spans')
		crvCvs = crvDegree + crvSpans
		for j in range(0,crvCvs+1):
			pos = mc.xform(sel[0] + '.cv[ '+str(j)+ ']' , q = True , ws = True , t = True )
			mc.xform(newCrv + '.cv[ '+str(j)+ ']' , ws = True , t = (-pos[0],pos[1],pos[2]) )
def run ( *args ) :
	
	if mc.window('copyShape', q=True ,exists=True):
		mc.deleteUI('copyShape')
	
	copyShape = mc.window('copyShape',title="Copy Curve Shape",s=False)
	
	mc.columnLayout( rowSpacing = 1 ,cat = ('both',1) ,cw = 200 )
	mc.text(l = 'Search for')
	mc.textField('searchUI' , tx = 'LFT')
	mc.text(l = 'Replace with')
	mc.textField('replaceUI' , tx = 'RGT')
	mc.button(label = 'copy', h = 35 , c = copyCrvShape )
	mc.button(label = 'mirror all', h = 35 , c = mirrorShape )
	mc.setParent('..')
	mc.window('copyShape',e = True,w = 200,h = 150)
	mc.showWindow(copyShape)

#main()
