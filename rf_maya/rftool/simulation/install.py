import sys ;
import os ;
import fileinput ;

	# get path from this script location
mp = os.path.realpath (__file__) ;
fileName = os.path.basename (__file__) ;
mp = mp.replace ( fileName , '' ) ;
mp = mp.replace ( '\\' , '/' ) ;
#mp = mp.replace ( mp[-1] , '/' ) ;
mp = mp[:-1] ;

	# append path
if not mp in sys.path :
	sys.path.append ( mp ) ;

	# update myPath.txt as well as querry the old path

fileRead = open ( mp + '/myPath.txt' , 'r+' ) ;
oldPath = fileRead.read () ;
fileRead.close ( ) ;

	# update myPath in main UI 
'''
mainUI = mp + '/library/mainUI.py' ;
replacements = { oldPath : mp } ;

lines = []

with open ( mainUI ) as infile:
    for line in infile :
        for src , target in replacements.items ( ) :
            line = line.replace ( src , target )
        lines.append(line)

with open( mainUI, 'w' ) as outfile :
    for line in lines :
        outfile.write(line) ;
'''

caller = mp + '/caller.py' ;
replacements = { oldPath : mp } ;

lines = []

with open ( caller ) as infile:
    for line in infile :
        for src , target in replacements.items ( ) :
            line = line.replace ( src , target )
        lines.append(line)

with open( caller, 'w' ) as outfile :
    for line in lines :
        outfile.write(line) ;

file = open ( mp + '/myPath.txt' , 'w+' ) ;
file.write ( mp ) ;
file.close ( ) ;

	# install softCluster
'''
mayaScriptPath =  ( os.path.expanduser('~/Documents/maya/scripts/SoftClusterEX') ) ;

mayaScriptPath = mayaScriptPath.replace ( '\\' , '/' ) ;

softClusterPath = ( mp + '/external/SoftClusterEX-3.0.1/SoftClusterEX' ) ;


if os.path.exists ( mayaScriptPath )  == False :

	import shutil, errno ;
	
	shutil.copytree ( softClusterPath , mayaScriptPath ) ;
'''