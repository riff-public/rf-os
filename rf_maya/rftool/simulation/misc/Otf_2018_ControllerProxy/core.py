import pymel.core as pm ;

def makeProxy ( *args ) :

	selection_list = pm.ls ( sl = True ) ;

	for selection in selection_list :

		### anim group ###
	    group = pm.group ( em = True , n = selection.nodeName() + '_AnimGrp' ) ;
	    pm.parentConstraint ( selection , group , mo = False , skipRotate = 'none' , skipTranslate = 'none' ) ;
	    
	    ### space group ###
	    spaceGroup = pm.group ( em = True , n = selection.nodeName() + '_SpaceGrp' ) ;
	    parent = selection.getParent() ;
	    pm.parentConstraint ( parent , spaceGroup ) ;
	 
	    connectGroup = pm.group ( em = True , n = 'Connect_Grp' ) ;
	    
	    for attr in [ 't' , 'r' , 's' ] :
	        exec ( 'connectGroup.' + attr + '.lock()' ) ;
	        
	    pm.parent ( group , spaceGroup ) ;
	    pm.parent ( connectGroup , group ) ;