import pymel.core as pm ;

def createABCGrp ( *args ) :
    
    def parentShape ( master , slave , *args ) :
        pm.parent ( slave.getShape() , master, shape = True , r = True ) ;

    suffix = '_Abc' ;

    target = pm.ls ( sl = True ) [0] ;
    group = pm.group ( em = True , w = True , n = target.nodeName() + suffix ) ;

    allItem = target.listRelatives ( ad = True , type = 'transform' ) ;
    
    dup_list = [] ;
    
    for each in allItem :
        tfm = pm.group ( em = True , name = each + suffix ) ;
        # for attr in [ 't' , 'r' , 's' , 'v' , 'shear' , 'rotatePivot' , 'rotateAxis' , 'rotatePivotTranslate' , 'scalePivot' , 'scalePivotTranslate' ] :
        #     exec ( 'each.{attr} >> tfm.{attr}'.format( attr = attr ) ) ;
        each.v >> tfm.v ;
        dup_list.append ( [ each , tfm ] ) ;
    
    for each in allItem :

        if each.getShape() :
        
            if each.getShape().nodeType() == 'mesh' :

                eachTfm = pm.general.PyNode ( each.nodeName() + suffix ) ;

                temptTfm = pm.duplicate ( each ) [0] ;
                #pm.delete ( temptTfm.getChildren() ) ;
                pm.parent ( temptTfm , w = True ) ;

                parentShape ( eachTfm , temptTfm ) ;

                pm.delete ( temptTfm ) ;

                eachTfm.getShape().rename( eachTfm.nodeName()+'Shape' ) ;

                #if parent   : pm.parent ( each , parent ) ;
                #if children : pm.parent ( children , each ) ;

    for each in dup_list :
        if each[0].getShape().nodeType() == 'mesh' :

            parent      = each[0].getParent() ;
            children    = each[0].getChildren( type = 'transform' ) ;

            if parent :
                pm.parent ( each[0] , w = True ) ;
            if children :
                pm.parent ( children , w = True ) ; 

            pm.blendShape ( each[0] , each[1] , origin = 'world' , weight = [ 0 , 1.0 ] , n = each[0].nodeName() + suffix + '_Bsn' ) ;

            if parent :
                pm.parent ( each[0] , parent ) ;
            if children :
                pm.parent ( children , each[0] ) ; 

    for each in dup_list :
        if each[0].getParent() :
            # if each[0].getParent() and each[0].getParent() != target :
            parent = each[0].getParent().nodeName() + suffix ;
            pm.parent ( each[1] , parent ) ;
        else : pass ;

def bakeKey ( *args ) :

    start   = pm.playbackOptions ( q = True , min = True ) ;
    end     = pm.playbackOptions ( q = True , max = True ) ;

    selection_list = pm.ls ( sl = True ) ;

    for selection in selection_list :

        pm.bakeResults ( selection ,
            simulation  = True ,
            hierarchy = 'below' ,
            time = ( start , end ) ,
            sampleBy = 1 ,
            disableImplicitControl = True ,
            preserveOutsideKeys = True ,
            sparseAnimCurveBake = False ,
            removeBakedAttributeFromLayer = False ,
            removeBakedAnimFromLayer = False ,
            bakeOnOverrideLayer = False ,
            minimizeRotation = True ,
            controlPoints = False ,
            shape = True ) ;

def rename ( *args ) :

    selection_list = pm.ls ( sl = True ) ;

    for selection in selection_list :

        mainName = selection.nodeName() ;

        counter = 0 ;
        
        node_list = selection.listRelatives ( allDescendents = True ) ;
        node_list.reverse() ;

        for node in node_list :
            pm.rename ( node , mainName + '_' + str(counter).zfill(4) ) ;
            counter += 1 ;

def run ( *args ) :
    
    width = 300.00 ;

    # check if window exists
    if pm.window ( 'mooThesisCacheOut_Ui' , exists = True ) :
        pm.deleteUI ( 'mooThesisCacheOut_Ui' ) ;
    else : pass ;
   
    window = pm.window ( 'mooThesisCacheOut_Ui', title = "moo's thesis COUT util" ,
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'mooThesisCacheOut_Ui' , e = True , w = width , h = 20 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            pm.button ( label = 'bake key' , c = bakeKey ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                pm.button ( label = 'rename' , c = rename ) ;
                pm.button ( label = 'create ABC group' , c = createABCGrp ) ;
                
    
    window.show () ;

run ( ) ;