import pymel.core as pm ;

#cycleCheck -e off

if pm.pluginInfo ( 'matrixNodes.mll' , q = True , loaded = True ) == True :
    pass ;
else :
    pm.loadPlugin ( 'matrixNodes.mll' ) ;

def nodeConstraint ( master , slave , *args ) :
    # return mulMatrix_node , decomposeMatrix_node

    def makeName_cmd ( target = '' , *args ) :
        targetSplit_list = target.split ( '_' ) ;
        name = targetSplit_list[0] ;
        for each in targetSplit_list[1:] :
            name += each.capitalize ( ) ;
        return name ;

    master_tfm = pm.general.PyNode ( master ) ;
    slave_tfm = pm.general.PyNode ( slave ) ;

    master_nm = makeName_cmd ( target = master_tfm ) ;
    slave_nm = makeName_cmd ( target = slave_tfm ) ;

    mulMatrix_node = pm.createNode ( 'multMatrix' , n = master_nm + 'NCon_mmt' ) ;
    decomposeMatrix_node = pm.createNode ( 'decomposeMatrix' , n = master_nm + 'NCon_dcm' ) ;

    master_tfm.worldMatrix >> mulMatrix_node.matrixIn[0] ;
    #slave.parentInverseMatrix >> mulMatrix_node.matrixIn[1] ;

    mulMatrix_node.matrixSum >> decomposeMatrix_node.inputMatrix ;

    decomposeMatrix_node.outputTranslate >> slave.translate ;
    decomposeMatrix_node.outputRotate >> slave.rotate ;
    decomposeMatrix_node.outputScale >> slave.scale ;

    return ( mulMatrix_node , decomposeMatrix_node ) ;


def bakeKey ( *args ) :

    start   = pm.playbackOptions ( q = True , min = True ) ;
    end     = pm.playbackOptions ( q = True , max = True ) ;

    selection_list = pm.ls ( sl = True ) ;

    for selection in selection_list :

        pm.bakeResults ( selection ,
            simulation  = True ,
            hierarchy = 'below' ,
            time = ( start , end ) ,
            sampleBy = 1 ,
            disableImplicitControl = True ,
            preserveOutsideKeys = True ,
            sparseAnimCurveBake = False ,
            removeBakedAttributeFromLayer = False ,
            removeBakedAnimFromLayer = False ,
            bakeOnOverrideLayer = False ,
            minimizeRotation = True ,
            controlPoints = False ,
            shape = True ) ;

def rename ( *args ) :

    selection_list = pm.ls ( sl = True ) ;

    for selection in selection_list :

        mainName = selection.nodeName() ;

        counter = 0 ;
        
        node_list = selection.listRelatives ( allDescendents = True ) ;
        node_list.reverse() ;

        for node in node_list :
            pm.rename ( node , mainName + '_' + str(counter).zfill(4) ) ;
            counter += 1 ;

def createABCGrp ( *args ) :

    selection_list = pm.ls ( sl = True ) ;

    for selection in selection_list :
        suffix = '_Abc' ;
        group = pm.group ( em = True , w = True , n = selection + suffix ) ;

        # selection.t >> group.t ;
        # selection.r >> group.r ;
        # selection.s >> group.s ;
        # selection.v >> group.v ;

        dupParent_list = [] ;

        for transform in selection.listRelatives ( allDescendents = True , type = 'transform' ) :

            shape = transform.getShape() ;

            parent = transform.getParent ( ) ;
            children = transform.getChildren ( type = 'transform' ) ; 
            children.reverse()  ;

            if parent :
                dupParent = parent.nodeName() + suffix ;
            else :
                dupParent = group ;

            if parent :
                pm.parent ( transform , w = True ) ;
            if children :
                pm.parent ( children , w = True ) ;
            
            if shape.nodeType () == 'locator' :
                
                pass ;

            else :
                dup = pm.duplicate ( transform ) [0]  ;

                ### clean mesh ###
                pm.delete ( dup , ch = True ) ;

                dup.t.set ( 0 , 0 , 0 ) ;
                dup.r.set ( 0 , 0 , 0 ) ;
                dup.s.set ( 1 , 1 , 1 ) ;
 
                dupShape = dup.getShape() ;
                dupShape.rename ( transform.nodeName() + suffix + 'Shape' ) ;

                for shape in dup.getShapes() :
                    if shape != dupShape :
                        pm.delete ( shape ) ;

                dup.rename ( transform.nodeName() + suffix ) ;

                ### blendshape ### ;
                pm.blendShape ( transform , dup , origin = 'world' , weight = [ 0 , 1.0 ] , n = transform.nodeName() + suffix + '_BSH' ) ;
                #nodeConstraint ( transform , dup ) ;
                #pm.parentConstraint ( transform , dup , mo = 0 , skipRotate = 'none' , skipTranslate = 'none' ) ;
                #pm.scaleConstraint ( transform , dup , mo = 0 , skip = 'none' ) ;
                ### connect vis ###
                # transform.t >> dup.t ;
                # transform.r >> dup.r ;
                # transform.s >> dup.s ;
                transform.v >> dup.v ;
                
                dupParent_list.append ( [ dup , dupParent ] ) ;
                pm.parent ( dup , group ) ;

            if parent :
                pm.parent ( transform , parent ) ;
            if children :
                pm.parent ( children , transform ) ;

        # for each in dupParent_list :
        #     pm.parent ( each[0] , each[1] ) ;

def run ( *args ) :
    
    width = 300.00 ;

    # check if window exists
    if pm.window ( 'mooThesisCacheOut_Ui' , exists = True ) :
        pm.deleteUI ( 'mooThesisCacheOut_Ui' ) ;
    else : pass ;
   
    window = pm.window ( 'mooThesisCacheOut_Ui', title = "moo's thesis COUT util" ,
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'mooThesisCacheOut_Ui' , e = True , w = width , h = 20 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                pm.button ( label = 'bake key' , c = bakeKey ) ;
                pm.button ( label = 'create ABC group' , c = createABCGrp )
                pm.button ( label = 'rename' , c = rename )
    
    window.show () ;

run ( ) ;