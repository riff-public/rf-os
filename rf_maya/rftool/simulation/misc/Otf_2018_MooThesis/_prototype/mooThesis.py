def nodeConstraint ( master , slave , *args ) :
    # return mulMatrix_node , decomposeMatrix_node

    def makeName_cmd ( target = '' , *args ) :
        targetSplit_list = target.split ( '_' ) ;
        name = targetSplit_list[0] ;
        for each in targetSplit_list[1:] :
            name += each.capitalize ( ) ;
        return name ;

    master_tfm = pm.general.PyNode ( master ) ;
    slave_tfm = pm.general.PyNode ( slave ) ;

    master_nm = makeName_cmd ( target = master_tfm ) ;
    slave_nm = makeName_cmd ( target = slave_tfm ) ;

    mulMatrix_node = pm.createNode ( 'multMatrix' , n = master_nm + 'NCon_mmt' ) ;
    decomposeMatrix_node = pm.createNode ( 'decomposeMatrix' , n = master_nm + 'NCon_dcm' ) ;

    master_tfm.worldMatrix >> mulMatrix_node.matrixIn[0] ;
    slave.parentInverseMatrix >> mulMatrix_node.matrixIn[1] ;

    mulMatrix_node.matrixSum >> decomposeMatrix_node.inputMatrix ;

    decomposeMatrix_node.outputTranslate >> slave.translate ;
    decomposeMatrix_node.outputRotate >> slave.rotate ;
    decomposeMatrix_node.outputScale >> slave.scale ;

    return ( mulMatrix_node , decomposeMatrix_node ) ;

def renameGrp ( target , *args ) :

    mainName = target.nodeName() ;

    item_list = target.listRelatives ( ad = True ) ;
    item_list.reverse() ;

    counter = 0 ;

    for item in item_list :
        item.getShape().rename( mainName + '_' + str(counter).zfill(4) + 'Shape' ) ;
        item.rename ( mainName + '_' + str(counter).zfill(4) ) ;

        counter += 1 ;

def customBlendShape ( master , slave , *args ) :
    pm.blendShape ( master , slave , origin = 'world' , weight = [ 0 , 1.0 ] , n = '%s_srcBSH' % master ) ;

def duplicateMeshHierarchy ( target , *args ) :

    mainGrp = target ;

    item_list = target.listRelatives ( ad = True , type = 'transform' ) ;

    for item in item_list :

        if item.getShape() :

            if item.getShape().nodeType() != 'locator' :

                parent      = item.getParent() ;
                children    = item.getChildren() ;

                if parent   : pm.parent ( item , w = True ) ;
                if children : pm.parent ( children , w = True ) ;

                dupItem = pm.duplicate ( item ) [0] ;
                dupItemShapes = dupItem.getShapes() ;
                for shape in dupItemShapes :
                    if shape != dupItem.getShape() :
                        pm.delete ( shape ) ;
                dupItem.getShape().rename( item.nodeName() + '_AbcShape') ;
                dupItem.rename ( item.nodeName() + '_Abc' ) [0] ;
                nodeConstraint ( item , dupItem ) ;
                customBlendShape ( item , dupItem ) ;

                item.v >> dupItem.v ;

                if parent   : pm.parent ( item , parent ) ;
                if children : pm.parent ( children , item ) ;

def run ( *args ) :

    selection_list = pm.ls ( sl = True ) ;

    for selection in selection_list :
        renameGrp ( selection ) ;
        duplicateMeshHierarchy ( selection ) ;

run () ;