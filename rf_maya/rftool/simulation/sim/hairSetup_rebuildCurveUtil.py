import pymel.core as pm ;
import math ;

##################
##################
''' core '''
##################
##################

def freezeTransform_cmd ( target , *args ) :
    pm.makeIdentity ( target , apply = True ) ;

def deleteHistory_cmd ( target , *args  ) :
    pm.delete ( target , ch = True ) ;

def centerPivot_cmd ( target , *args ) :
    pm.xform ( target , cp = True ) ;

def deleteRebuiltShape_cmd ( target , *args ) :

    shape_list = pm.listRelatives ( target , shapes = True ) ;
        
    for shape in shape_list :            
        if 'rebuilt' in str ( shape ) :
            pm.delete ( shape ) ;
        else : pass ;

def cleanCurve_cmd ( target_list , *args ) :

    freezeTransform_cmd ( target_list ) ;

    deleteHistory_cmd ( target_list ) ;

    centerPivot_cmd ( target_list ) ;

    for target in ( target_list ) :
        deleteRebuiltShape_cmd ( target ) ;

def freezeTransformBtn_cmd ( *args ) :
    selection = pm.ls ( sl = True ) ;
    pm.makeIdentity ( selection , apply = True ) ;

def deleteHistoryBtn_cmd ( *args ) :
    selection = pm.ls ( sl = True ) ;
    pm.delete ( selection , ch = True ) ;

def centerPivotBtn_cmd ( *args ) :
    selection = pm.ls ( sl = True ) ;
    pm.xform ( selection , cp = True ) ;

def deleteRebuiltShapeBtn_cmd ( *args ) :

    selection = pm.ls ( sl = True ) ;

    for each in selection :

        shape_list = pm.listRelatives ( each , shapes = True ) ;
        
        for shape in shape_list :            
            if 'rebuilt' in str ( shape ) :
                pm.delete ( shape ) ;
            else : pass ;

##################
##################
''' core '''
##################
##################

def reset_UI(*args):
    pm.floatField ( 'currentShortestLength_floatField' , e = True , value = 0.0 ) ;
    pm.floatField ( 'currentLongestLength_floatField' , e = True , value = 0.0 ) ;

    pm.intField ( 'currentMinSpan_intField' , e = True , value = 0 ) ;
    pm.intField ( 'currentMaxSpan_intField' , e = True , value = 0 ) ;
    pm.intField('currentDegree_intField', e=True, value=0)

def getCurrentInfo_cmd ( *args ) :
    
    allCurves_list = get_curves()
    if not allCurves_list:
        reset_UI()
        return

    ### get longest length

    curveLength_list = [] ;

    for curve in allCurves_list :

        # check if curves
        curveShape = pm.listRelatives ( curve , shapes = True ) ; 

        if curveShape != [] :
            curveLength_list.append ( pm.arclen ( curve ) ) ;
        else :
            pass ;

    curveLength_list.sort ( ) ;

    shortestLength_info     = curveLength_list[0] ;
    longestLength_info      = curveLength_list[-1] ;

    pm.floatField ( 'currentShortestLength_floatField' , e = True , v = shortestLength_info ) ;
    pm.floatField ( 'currentLongestLength_floatField' , e = True , v = longestLength_info ) ;

    ### get span and degree ###

    curveSpan_list = [] ;
    curveDegree_list = [] ;

    for curve in allCurves_list :

        curveShape = pm.listRelatives ( curve , shapes = True ) ;

        if curveShape != [] :
            curveShape = curveShape [0] ;

            curveSpan_list.append ( pm.getAttr ( curveShape + '.spans' ) ) ;
            curveDegree_list.append ( pm.getAttr ( curveShape + '.degree' ) )

        else :
            pass ;

    curveSpan_list.sort() ;
    curveDegree_list.sort() ;

    pm.intField ( 'currentMinSpan_intField' , e = True , v = curveSpan_list[0] ) ;
    pm.intField ( 'currentMaxSpan_intField' , e = True , v = curveSpan_list[-1] ) ;
    pm.intField ( 'currentDegree_intField' , e = True , v = curveDegree_list[-1] ) ;

def rebuid_cmd ( *args ) :

    # curve_suffix = pm.textField ( 'curveSuffix_textField' , q = True , text = True ) ;

    # allCurves_list = pm.ls ( '*%s' % curve_suffix ) ;
    allCurves_list = get_curves()
    if not allCurves_list:
        return

    # print allCurves_list ;

    currentShortestLength_info  = pm.floatField ( 'currentShortestLength_floatField' , q = True , value = True ) ;
    currentLongestLength_info   = pm.floatField ( 'currentLongestLength_floatField' , q = True , value = True ) ;
    length_info                 = currentLongestLength_info - currentShortestLength_info ;

    currentMinSpan_info = pm.intField ( 'currentMinSpan_intField' , q = True , value = True ) ;
    currentMaxSpan_info = pm.intField ( 'currentMaxSpan_intField' , q = True , value = True ) ;
    

    minSpan = pm.intField ( 'minSpan_intField' , q = True , v = True ) ;
    maxSpan = pm.intField ( 'maxSpan_intField' , q = True , v = True ) ;
    span_info = maxSpan - minSpan ;

    degree = int ( pm.optionMenu ( 'degree_ptm' , q = True , value = True ) ) ;

    cleanCurve_cmd ( allCurves_list ) ;

    for curve in  allCurves_list :
        
        percentage = pm.arclen(curve) ;
        percentage -= currentShortestLength_info ;
        try:
            percentage /= length_info ;
        except ZeroDivisionError:
            percentage = 1

        span = span_info * percentage ;

        span = round ( span ) ;
        span += minSpan ;
        spam = int ( span ) ;

        pm.rebuildCurve ( curve ,
            constructionHistory = 0 ,
            replaceOriginal     = 1 ,
            rebuildType         = 0 , # uniform
            endKnots            = 1 , # multiple end knots
            keepRange           = 0 ,
            keepControlPoints   = 0 ,
            keepEndPoints       = 1 ,
            keepTangents        = 0 ,
            spans               = span ,
            degree              = degree ,
            tolerance           = 0.01 ,
            ) ;

    cleanCurve_cmd ( allCurves_list ) ;

    # rebuildCurve -ch 1 -rpo 1 -rt 0 -end 1 -kr 0 -kcp 0 -kep 1 -kt 0 -s 4 -d 3 -tol 0.01 "hairHead0416_ORI";
    if pm.checkBoxGrp('fromSel_chkBoxGrp', q=True, v1=True):
        pm.select(allCurves_list)
    refresh_cmd ( ) ;
    

def fromSel_checked(*args):
    enable = pm.checkBoxGrp('fromSel_chkBoxGrp', q=True, v1=True)
    pm.textField('curveSuffix_textField', e=True, en=not enable)

def refresh_cmd ( *args ) :
    getCurrentInfo_cmd () ;    

def get_curves( * args):
    sel_checked = pm.checkBoxGrp('fromSel_chkBoxGrp', q=True, v1=True)
    allCurves_list = []
    if sel_checked:
        allCurves_list = [s for s in pm.selected(type='transform') if s.getShape(ni=True) and s.getShape(ni=True).nodeType() == 'nurbsCurve']
    else:
        curve_suffix = pm.textField ( 'curveSuffix_textField' , q = True , text = True ) ;
        allCurves_list = [s for s in pm.ls ( '*%s' % curve_suffix, type='transform' ) if s.getShape(ni=True) and s.getShape(ni=True).nodeType() == 'nurbsCurve']

    return allCurves_list
        

##################
##################
##################
''' UI '''
##################
##################
##################

title = 'rebuild curve util v1.0' ;
width = 400.00 ; 

def filler ( repeat = 1 , *args ) :
    for i in range ( 1 , repeat + 1 ) :
        pm.text ( label = '' ) ;

def separator ( repeat = 1 , *args ) :
    for i in range ( 1 , repeat + 1 ) :
        pm.text ( label = '' , h = 2.5 ) ;

def run ( *args ) :
    
    # check if window exists
    if pm.window ( 'rebuildCurveUtil_UI' , exists = True ) :
        pm.deleteUI ( 'rebuildCurveUtil_UI' ) ;
    else : pass ;
   
    window = pm.window ( 'rebuildCurveUtil_UI', title = title ,
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;

    pm.window ( 'rebuildCurveUtil_UI' , e = True , w = width , h = 100 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :

            with pm.frameLayout ( label = 'CLEANING' , collapsable = False , collapse = False , bgc = ( 0.1 , 0.1 , 0.5 )  ) :

                with pm.rowColumnLayout ( nc = 1 , w = width ) :
    
                    with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/3 ) , ( 2 , width/3 ) , ( 3 , width/3 ) ] ) :
                        pm.button ( label = 'FT' , c = freezeTransformBtn_cmd ) ;
                        pm.button ( label = 'Hist' , c = deleteHistoryBtn_cmd ) ;
                        pm.button ( label = 'CP' , c = centerPivotBtn_cmd ) ;

                    pm.button ( label = 'delete curve rebuilt shape' , c = deleteRebuiltShapeBtn_cmd ) ;
            
            with pm.frameLayout ( label = 'GET CURVES' , collapsable = False , collapse = False , bgc = ( 0.1 , 0.1 , 0.5 )  ) :
                
                # with pm.rowColumnLayout ( nc = 1 , w = width ) :
                    
                    # separator ( ) ;
                with pm.rowColumnLayout ( nc = 2 , co=[(1, 'left', 90), (2, 'left', 5)]) :
                    
                    with pm.columnLayout():
                        with pm.rowColumnLayout ( nc = 4 ) :

                            filler ( ) ;
                            pm.text ( label = 'from suffix' ) ;
                            pm.textField ( 'curveSuffix_textField' , text = '_ORI' ) ;
                            # filler ( 3 ) ;
                            
                            filler ( ) ;

                            filler ( 2) ;
                            pm.checkBoxGrp('fromSel_chkBoxGrp', label1='from selection', v1=False, cc=fromSel_checked)
                            filler()

                        # filler ( ) ;
                    # with pm.columnLayout():
                    pm.button ( label = ' refresh ' , c = refresh_cmd ) ;
                    # separator ( ) ;

            with pm.frameLayout ( label = 'CURRENT INFO' , collapsable = False , collapse = False , bgc = ( 0.1 , 0.1 , 0.5 )  ) :

                with pm.rowColumnLayout ( nc = 1 , w = width ) :

                    separator ( ) ;

                    with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , width/4 ) , ( 2 , width/4 ) , ( 3 , width/4 ) , ( 4 , width/4 ) ] ) :

                        filler ( ) ;
                        pm.text ( label = 'shortest length' ) ;
                        pm.text ( label = 'longest length' ) ;
                        filler ( ) ;

                        filler ( ) ;
                        pm.floatField ( 'currentShortestLength_floatField' ) ;
                        pm.floatField ( 'currentLongestLength_floatField' ) ;
                        filler ( ) ;

                    with pm.rowColumnLayout ( nc = 5 , cw = [ ( 1 , width/4/2 ) , ( 2 , width/4 ) , ( 3 , width/4 ) , ( 4 , width/4 ) , ( 5 , width/4/2 ) ] ) :\

                        filler ( ) ;
                        pm.text ( label = 'min span' ) ;
                        pm.text ( label = 'max span' ) ;
                        pm.text ( label = 'degree' ) ;
                        filler ( ) ;

                        filler ( ) ;
                        pm.intField ( 'currentMinSpan_intField' ) ;
                        pm.intField ( 'currentMaxSpan_intField' ) ;
                        pm.intField ( 'currentDegree_intField' ) ;
                        filler ( ) ;

                        

                    separator ( ) ;

            with pm.frameLayout ( label = 'REBUILD' , collapsable = False , collapse = False , bgc = ( 0.1 , 0.1 , 0.5 )  ) :

                with pm.rowColumnLayout ( nc = 1 , w = width ) :

                    with pm.rowColumnLayout ( nc = 5 , cw = [ ( 1 , width/4/2 ) , ( 2 , width/4 ) , ( 3 , width/4 ) , ( 4 , width/4 ) , ( 5 , width/4/2 ) ] ) :

                        filler ( ) ;
                        pm.text ( label = 'min span' , w = width/4 ) ;
                        pm.text ( label = 'max span' , w = width/4 ) ;
                        pm.text ( label = 'degree' , w = width/4 ) ;
                        filler ( ) ;

                        filler ( ) ;
                        pm.intField ( 'minSpan_intField' , w = width/4 , v = 1 ) ;
                        pm.intField ( 'maxSpan_intField' , w = width/4 , v = 6 ) ;
                        with pm.optionMenu( 'degree_ptm' , w = width/4  ) :
                            pm.menuItem ( label= '3' ) ;
                            pm.menuItem ( label= '1' ) ;
                        filler ( ) ;

                    with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/4/2 ) , ( 2 , width/4*3 ) , ( 3 , width/4/2 ) ] ) :
                        filler ( ) ;
                        pm.button ( label = 'rebuild' , c = rebuid_cmd ) ;
                        filler ( ) ;
                        
                    separator ( ) ;

    try:
        getCurrentInfo_cmd ( ) ;
    except IndexError:
        pass

    window.show () ;