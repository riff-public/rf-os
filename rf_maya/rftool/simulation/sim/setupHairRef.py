import maya.cmds as mc
import maya.mel as mel
import random
   
listNameChar = []
listNurb = []

listGrpCIN = []
listGrpCOL = []


listBodyCOLN = []
listBodyCOLNu = []

listNameORI = []


listNameGeoUV = []

        
def getEdges(*args):
    
          
    selsE = mc.ls(sl = True)
    print selsE
    numSelsE = len(selsE)
    
    if numSelsE == 2:
        print 'edgesTwo'

        for i in range(len(selsE)):
            num = i + 1
    
            if '.e' in selsE[i]:
                print 'yess'
                
                nameSpl = selsE[i].split('.')
                print nameSpl
                
                #nameSplB = nameSpl[0].split('_md')
                #print 'nameSplB:', nameSplB
                
                nameSplB = nameSpl[0].replace("_md","")
                print nameSplB
                
                listNameChar.append(nameSplB)
                print listNameChar
                
                 
                mc.textField('edge%s'%num, e = True , text = nameSpl[1])
                mc.textField('edge%s'%num, q = True , text = True)
                
              
                
            else:
                print 'no E'
                
    else:
        print 'please select two edges'
        
        
    
def saveEdges(*args):
    
    print listNameChar
    
    splName = listNameChar[0].split(':')
    print 'splName', splName
    
    nameT = splName[0] + '_' + splName[1]
    
    
    listEdges = []
    
    edge1 = mc.textField('edge1', q = True , text = True)
    edge2 = mc.textField('edge2', q = True , text = True)
    
    print edge1
    print edge2
    
    listEdges.append(edge1)
    listEdges.append(edge2)
    
    print listEdges
        
           
    fQ = mc.file(q= True,location = True)
        
    dirFile = fQ.split("/")
    print dirFile
    
    dirFile.remove(dirFile[-1]);
    
    newDirF = '/'.join(map(str, dirFile))
    print newDirF 
    
    
    
    f = open("%s/%s_Edges.txt"%(newDirF, nameT),"w")
    f.write(str(listEdges))
    
    f.close() 
    
    
    
def loadEdges(*args):
    
    sels = mc.ls(sl = True)
    print sels
    #[u'badger_md:BodyFur_Geo']
    
    
    numSels = len(sels)
    print numSels
    
    if 1 == numSels:
        print 'yesss'
        
        splName = sels[0].split(':')
        
        nameT = splName[0] + '_' + splName[1]
        
        
        fQ = mc.file(q= True,location = True)
            
        dirFile = fQ.split("/")
        print dirFile
        
        dirFile.remove(dirFile[-1]);
        
        newDirF = '/'.join(map(str, dirFile))
        print newDirF 
        
        
        f = open ("%s/%s_Edges.txt"%(newDirF,nameT),"r" ) 
        namesLoc = f.read ( ) 
        whip = eval(namesLoc)
        print 'whip:', whip 
        
        f.close ( )
        
        
     
        for i in range(len(whip)):
            
            num = i + 1
        
            mc.textField('edge%s'%num, e = True , text = whip[i])
            
            mc.textField('edge%s'%num, q = True , text = True)
            
            listNameChar.append(sels[0])
            print listNameChar
            
        
               
        
    else:
        
        print 'you have to select Geo that you want to make hair dynamis'
        
        

##Press Run HairDynamic    
#Create Ref
def createRef(*args):
    
    q = mc.textField('edge1', q = True , text = True)
    print 'q', q
    
    if 'q' == 00 :
        print 'Go to get edges first'
        
    else:    
    
        listRef = mc.ls(type = 'reference')
        print 'listRef', listRef
        
        for selRef in listRef:
            
            qR = mc.referenceQuery(selRef, nodes = True)
            print 'qR:', qR
            
            if ':Geo_Grp' in qR[0]:
                print 'Yess!! this is node of character model'
                     
                rfQ = mc.referenceQuery( selRef, filename = True )
                print "rfQ:", rfQ
                
                rfQNsp = mc.referenceQuery( selRef, namespace = True )
                print "rfQNsp:", rfQNsp
                
                newNSp = rfQNsp.replace("_md","")
                print "newNSp:", newNSp
                
                listNameSp = ['COL', 'CIN']
                
                for crf in listNameSp:
                    
                    mc.file(rfQ, reference = True, namespace = '%s_%s'%(newNSp, crf))
                    
                
                mc.file(rfQ, i = True)
                
                mc.file(rfQ, removeReference = True)
                
                ###RunNextDef
                makeNurbNew()
       
                
            else:
                print 'NO'
            
            
        
       
                 
    
def makeNurbNew(*args):    
    
    edge1 = mc.textField('edge1', q = True , text = True)
    edge2 = mc.textField('edge2', q = True , text = True)
    
    print edge1
    print edge2
    
    print listNameChar[0]
    
    nameGeo = listNameChar[0].split(':')
    print "nameGeo", nameGeo
    
    nameWithCIN = nameGeo[0] + '_CIN'
    print 'nameWithCIN', nameWithCIN
    
    nameCINSP = nameWithCIN + ':' + nameGeo[1]
    print "nameCINSP:", nameCINSP
    
           
    GeoAndE1 = nameCINSP +'.'+edge1
    GeoAndE2 = nameCINSP +'.'+edge2
        
            
    nurb = mc.loft( GeoAndE1, GeoAndE2, ch = True, u = True, c = False, ar = True, d = 1, ss = 1, rn = False, po = 0, rsn = True, n ='%s_CIN_nrb'%nameGeo[1])
    print nurb
    listNurb.append(nurb[0])
    mc.CenterPivot()
    mc.DeleteHistory()
    mc.makeIdentity(apply = True, t = True, r = True, s = True, n = 0, pn = True) 
    
    
    ###RunNextDef
    makeFol()
    
        
def makeFol(*args):
    
    mc.select(listNurb[0], r = True)
    
    mel.eval( 'evalNoSelectNotify "createHair 1 1 10 0 0 0 0 5 0 1 1 1";')
    
    selFol = mc.ls(type = 'follicle')
    print "selFol", selFol[0]
    
    mc.setAttr ("%s.simulationMethod"%selFol[0], 0)
    
    mc.select(selFol)
    pickFol = mc.pickWalk(d = 'up')
    
        
    noTuGrp = mc.createNode('transform', n = 'noTouch_grp')
    mc.setAttr("%s.visibility"%noTuGrp, 0)
    
    mc.parent(pickFol,noTuGrp )
    
    print "listNameChar:", listNameChar
    nameChar = listNameChar[0].split(':')
    mc.parent('%s_CIN_nrb'%nameChar[1],noTuGrp )
    
    mc.select('hairSystem1Follicles', r = True)
    mc.delete()
    mc.select('hairSystem1', r = True)
    mc.delete()
    mc.select('curve1', r = True)
    mc.delete()
    mc.select('pfxHair1', r = True)
    mc.delete()
    mc.select('nucleus1', r = True)
    mc.delete()
    
    
    ###RunNextDef
    makeWrap()
    
        
def makeWrap(*args):
       
    mc.select(listNurb[0], r = True)
    
    print 'listNameChar[0]:', listNameChar[0]
    
    #line = 'KongPanda'
    index = listNameChar[0].find(':')
    addCIN = listNameChar[0][:index] + '_CIN' + listNameChar[0][index:]    
    print 'addCIN', addCIN
    
    
    mc.select(addCIN, add = True)
    
    mc.CreateWrap()
    
    mc.select('%sBase'%addCIN)
    selBase = mc.ls(sl = True)
    
    
    mc.parent(selBase, 'noTouch_grp') 
      
    
    ###RunNextDef
    bsh()



def bsh(*args):
    
    listNameCIN = []
    listNameCOL = []
    
    listTopGrp = mc.ls(assemblies = True)
    print listTopGrp
    
    for topGrp in listTopGrp:
        
        print topGrp
        
        if 'CIN' in topGrp :
            print 'Yes CIN'
                                    
            listNameCIN.append(topGrp)
            
            
        elif 'COL' in topGrp:
            print 'Yes COL'
                                   
            listNameCOL.append(topGrp)
                        
            
        else:
            print "No"
            
    
    #grpCIN = mc.group( listNameCIN[0], n='Geo_Grp_CIN' )
    #listGrpCIN.append(grpCIN)
    
    grpCOL = mc.group( listNameCOL[0], n='Geo_Grp_COL' )
    listGrpCOL.append(grpCOL)
    
    
    #mc.select('Geo_Grp_CIN', r = True)    
    #mc.select('Geo_Grp_COL', add = True)
    
    mc.select(listNameCIN[0], r = True)    
    mc.select( listNameCOL[0], add = True)
    
   
      
    mc.blendShape(automatic = True, origin = 'world', n = 'CIN_COL_BSH' , w = ( 0 , 1 ) )
    
    
    
    ###RunNextDef
    makeLoc()




def makeLoc(*args):
    
    print "listGrpCIN", listGrpCIN
    print "listGrpCOL", listGrpCOL
    
    
    lsFol = mc.ls(type = 'follicle')
    mc.select(lsFol, r = True)
    pickFol = mc.pickWalk(d = 'up')
    print pickFol
    
    getTX = mc.getAttr('%s.translateX'%pickFol[0])
    
    getTY = mc.getAttr('%s.translateY'%pickFol[0])
    
    getTZ = mc.getAttr('%s.translateZ'%pickFol[0])
    
    
    getRX = mc.getAttr('%s.rotateX'%pickFol[0])
    
    getRY = mc.getAttr('%s.rotateY'%pickFol[0])
    
    getRZ = mc.getAttr('%s.rotateZ'%pickFol[0])
    
    mc.createNode('locator')
    
    locGrp = mc.rename('locator1', 'Geo_Grp_LOC')
    print locGrp
    
    mc.setAttr("%s.translateX"%locGrp, getTX)
    mc.setAttr("%s.translateY"%locGrp, getTY)
    mc.setAttr("%s.translateZ"%locGrp, getTZ)
    
    
    mc.setAttr ("%s.rotateX"%locGrp, getRX)
    mc.setAttr ("%s.rotateY"%locGrp, getRY)
    mc.setAttr ("%s.rotateZ"%locGrp, getRZ) 
    
    
    mc.select(locGrp, r = True)
    mc.makeIdentity (apply = True, t = True, r = True, s = True, n = 0 , pn = True)
    #makeIdentity -apply true -t 1 -r 1 -s 1 -n 0 -pn 1;
    
    
    mc.parent(listGrpCOL[0], locGrp )


    mc.setAttr( "%s.tx"%listGrpCOL[0], lock = True)
    mc.setAttr( "%s.ty"%listGrpCOL[0], lock = True)
    mc.setAttr( "%s.tz"%listGrpCOL[0], lock = True)
    mc.setAttr( "%s.rx"%listGrpCOL[0], lock = True)
    mc.setAttr( "%s.ry"%listGrpCOL[0], lock = True)
    mc.setAttr( "%s.rz"%listGrpCOL[0], lock = True)
    mc.setAttr( "%s.sx"%listGrpCOL[0], lock = True)
    mc.setAttr( "%s.sy"%listGrpCOL[0], lock = True)
    mc.setAttr( "%s.sz"%listGrpCOL[0], lock = True)
    
    
    mc.select(pickFol, r = True)
    mc.select(locGrp , add = True)
    mc.parentConstraint(weight = 1, mo = True, dr = True) 
    #parentConstraint -mo -dr -weight 1;
    
    ###RunNextDef
    dupBodyCOL()
    


def dupBodyCOL(*args):
        
       
    print 'listNameChar', listNameChar
    
    nameBody = listNameChar[0].split(':')
    print "nameBody[1]:", nameBody[1]
    
    if 1 == len(listNameGeoUV):
        print 'this is Geo Edit UV'
        
        mc.delete( listNameGeoUV[0], ch = True )
        
        for i in range(4):
            print "i", i + 1
            
            reNum = i + 1
            
            print "reNum" ,reNum
            
            dupObjUV = mc.duplicate(listNameGeoUV[0],rr = True)
            print "dupObjUV", dupObjUV
            print "dupObjUV", dupObjUV[0]
            
            mc.delete( dupObjUV[0], constructionHistory = True )
                                                                                             
                                                                                   
            reName = mc.rename(dupObjUV[0], '%sN%s_COL'%(dupObjUV[0], reNum))
            print "reName:", reName 
            
                                                                                      
            listBodyCOLN.append(reName)
            listBodyCOLNu.append(reName)
            
            if 'N1_COL' in reName :
                print 'No Hide'
                
            else: 
                print 'Hide'
                
                mc.setAttr("%s.visibility"%reName, 0)
        
        
    else:
        
        print 'this is Geo Original UV'
        
        for i in range(4):
            print "i", i + 1
            
            reNum = i + 1
            
            print "reNum" ,reNum
            
            
            dupObj = mc.duplicate(nameBody[1],rr = True)
           
            print "dupObj", dupObj
            print "dupObj", dupObj[0]
            
            mc.delete( dupObj[0], constructionHistory = True )
                                                                                                                                                  
            reName = mc.rename(dupObj[0], '%sN%s_COL'%(dupObj[0], reNum))
            print "reName:", reName                                   
                                                       
            listBodyCOLN.append(reName)
            listBodyCOLNu.append(reName)
            
            if 'N1_COL' in reName :
                print 'No Hide'
                
            else:
                print 'Hide'
                
                mc.setAttr("%s.visibility"%reName, 0)
            
    
    
                        
    print "listBodyCOLN", listBodyCOLN 
    ##-----------
   
            
    ##-----------------        
    
    tf = mc.createNode('transform', n = 'Geo_Grp_COL_MNCol')
    print "tf:", tf
    
    mc.parent(tf, listGrpCOL[0]) 
    
    for n in listBodyCOLN:
        print "n:", n          
           
        mc.parent(n, tf)
        
        
              
       
    print 'listNameChar[0]', listNameChar[0]
    
    index = listNameChar[0].find(':')
    addCOL = listNameChar[0][:index] + '_COL' + listNameChar[0][index:]
    print "addCOL:", addCOL
    
    
    listBodyCOLN.insert(0, addCOL)
    print "listBodyCOLN:", listBodyCOLN
    
    
    ##----------
                   

    
            
            
    ###RunNextDef
    makeCOLDYN()
            
            
            
            
            
def makeCOLDYN(*args):
        
    print 'listBodyCOLNu:', listBodyCOLNu
    
    #listBodyCOLNu = ['BodyFur_GeoN1_COL', 'BodyFur_GeoN2_COL', 'BodyFur_GeoN3_COL', 'BodyFur_GeoN4_COL']
          
                                 
    for i in range(len(listBodyCOLNu)):
        num = i+1
        layDis = mc.editDisplayLayerMembers( 'hairOri%s_layer'%num, query=True )
        print layDis
        
        mc.select( layDis,r = True)
        mc.select( listBodyCOLNu[i], tgl = True , add = True)
        
        makeCDyn = mel.eval( 'makeCurvesDynamic 2 { "1", "0", "1", "1", "0"};') 
        print "makeCDyn", makeCDyn
        
        mc.select('hairSystem%s'%num, r = True)
                        
        selsHair = mc.ls(sl = True)
        print 'selsHair:', selsHair 
        
        mc.rename(selsHair, 'HairN%s_hairSystem'%num) 
        
        
    ###RunNextDef
    makeNucleus()

 
def makeNucleus(*args):
    
    listHSys = mc.ls(type = 'hairSystem')
    print listHSys
    
    for sys in listHSys:
        print sys
        if '1' in sys:
            print 'yesss, this is hairSys number1'
            
        else:
            print 'No 1'
            mc.select(sys, r = True)
            
            pickFol = mc.pickWalk(d = 'up')
            
            sels = mc.ls(sl = True)
            print sels
            
            mel.eval('assignNSolver "";')
            
            
    ###RunNextDef
    grpHairOutCv()
            
    


def grpHairOutCv(*args):  

    #nameHrORI = 'Hair_ORI_Grp'    
    #nameHair = nameHrORI.split('_')
    
    print "listNameORI", listNameORI
    nameHair = listNameORI[0].split('_')
    
        
    mc.select('hairSystem*OutputCurves', r = True)
    selsOutCv = mc.ls(sl = True)
    print "selsOutCv:", selsOutCv
    
    
    if nameHair[0] == 'Hair':        
        print 'name ORI Grp is Hair'
        
        nameHrCRV= mc.group(selsOutCv, n = 'hair')
        print "nameHrCRV:",nameHrCRV
        
        
        mc.setAttr ( nameHrCRV + ".useOutlinerColor" , True)
    
        mc.setAttr ( nameHrCRV + ".outlinerColor" , 1.0, 1.0, 0.0)
    
        mel.eval('AEdagNodeCommonRefreshOutliners();')
        
        
        
        
                
        
    else:
        print 'no hair name'
   
        nameHrCRV= mc.group(selsOutCv, n = '%s'%nameHair[0])
        print "nameHrCRV:",nameHrCRV
        
        mc.setAttr ( nameHrCRV + ".useOutlinerColor" , True)
    
        mc.setAttr ( nameHrCRV + ".outlinerColor" , 1.0, 1.0, 0.0)
    
        mel.eval('AEdagNodeCommonRefreshOutliners();')
        
        
        
    
    
    for outGrp in range(len(selsOutCv)):   
        num = outGrp + 1
        print num
        
        mc.rename(selsOutCv[outGrp], '%sN%s_CRV'%(nameHair[0], num))
        
    ###RunNextDef
    chgNameOutCV()
        
        
def chgNameOutCV(*args):
        
    #listNameORI = ['Hair_ORI_Grp']
        
    print "listNameORI", listNameORI
    #listNameORI [u'Hair_ORI_Grp']
    nameORI = listNameORI[0].split('_')
    
    print 'nameORI', nameORI
    
        
    listFol = mc.ls(type = 'follicle')
    print 'listFol', listFol
    
    for fol in listFol:
        print "fol", fol
        
        if '_' in fol :
            
            print 'no!!! this is follicle from noTouchGrp'
                    
        else:
            print 'OK'
        
            mc.select(fol, r = True)
            pickFol = mc.pickWalk(d = 'up')
            print 'pickFol', pickFol
            
            mc.select(pickFol,r = True, hi = True)
            selsHi = mc.ls(sl = True)
            print "selsHi:", selsHi
            #selsHi: [u'follicle124', u'follicleShape124', u'Hair1555_ORI', u'Hair1555_ORIShape']
            
            selORI= selsHi[2].split('_')
            print "selORI", selORI
                                                        
            
            splName = pickFol[0].split('follicle')
            print "splName:", splName
            
            mc.select('curve%s'%splName[1], r = True)
            selsOutCv = mc.ls(sl = True)
            
            if 3 == len(selORI):
                print 'it is 3'
                mc.rename(selsOutCv[0], '%s_%s_CRV'%(selORI[0], selORI[1]))
                
            elif 2 == len(selORI):
                print 'it is 2'
                mc.rename(selsOutCv[0], '%s_CRV'%selORI[0])
                
            else:
                print 'false'
                
            
            
            
    ###RunNextDef
    makeGrpSIM()
    
def makeGrpSIM(*args):
    
    #nameORIGrp = 'Hair_ORI_Grp'
    #nameORI = nameORIGrp.split('_')
    
    print "listNameORI", listNameORI
    nameORI = listNameORI[0].split('_')
        
    
    listHSys = mc.ls(type = 'hairSystem')
    print "listHSys", listHSys
    
    listNu = mc.ls(type = 'nucleus')
    print "listNu", listNu
    
    simGrp = mc.createNode('transform', n = 'SIM_GRP')
    print "simGrp", simGrp
    
    
    for hSys in range(len(listHSys)):
        num = hSys + 1    
        mc.select(listHSys[hSys],r = True)
        pickHsys = mc.pickWalk(d = 'up')
        print 'pickHsys', pickHsys
                
        nameHSys = mc.rename(pickHsys, '%sN%s_hairSystem'%(nameORI[0],num))
        
        mc.parent(nameHSys, simGrp)
        
    for nu in listNu:
        
        mc.select(nu,r = True)
        pickNu = mc.pickWalk(d = 'up')
        print 'pickNu', pickNu
        
        mc.parent(pickNu, simGrp)
        
        
    ###RunNextDef
    makeTubeNrb()



def makeTubeNrb(*args):
    
    print 'listNameORI', listNameORI
    nameORI= listNameORI[0].split('_')
    
    mc.select('*N*_CRV', r = True)
    selsNCRV = mc.ls(sl = True)
    print selsNCRV
    
    topGrpTube = mc.createNode('transform', n = '%sTube_grp'%nameORI[0])
    
    listBase = []
    listTube = []
    
    listGrpBase = []
    listGrpTube = []
    
    for nGrp in selsNCRV:
        
        print 'nGrp', nGrp
        
        nameCrv = nGrp.split('_')
        grpBase = mc.createNode('transform', n = '%s_baseGRP'%nameCrv[0])
        mc.setAttr("%s.visibility"%grpBase, 0)
                
        grpTube = mc.createNode('transform', n = '%s_tubeGRP'%nameCrv[0])
                
        mc.parent(grpBase, topGrpTube)
        mc.parent(grpTube, topGrpTube)
        
        #selectOutputCurve
        mc.select(nGrp, hi = True, r = True)
        selsCRV = mc.ls(sl = True)
        print 'sselsCRV', selsCRV
        
        
        for crv in selsCRV: 
                
            if 'N' in crv:
                print 'Noo!!!, this is Group'
                
            elif 'Shape' in crv:
                print 'Noo!!!, this is Shape'
                
                                                   
            else:
                print 'Yesss!!!'
            
                mc.select('%s.cv[0]'%crv)
                selscv = mc.ls(sl = True)
                print selscv
                
                pcv = mc.pointPosition(selscv)
                print pcv
                
                ccBase = mc.circle(c = [pcv[0], pcv[1], pcv[2]], nr = [0, 1, 0], sw = 360, r = 0.05, d = 3, ut = False, tol = 0.01, s = 8, ch = True, n ='%s_base'%crv)
                mc.CenterPivot()
                print 'ccBase', ccBase
                
                mc.parent(ccBase[0], grpBase)
                
                            
                extdNurb = mc.extrude("%s_base"%crv ,"%s"%crv , ch = True, rn = False, po = 0, et = 2, ucp = 0, fpt = True, upn = 1, rotation = 0, scale = 0.4, rsp = True)
                print 'extdNurb', extdNurb
                
                mc.setAttr("%s.fixedPath"%extdNurb[1], 0)
                mc.setAttr("%s.useProfileNormal"%extdNurb[1], 0) 
                
                mc.parent(extdNurb[0], grpTube)
                

    mc.parent(topGrpTube, 'SIM_GRP')
    
    nameORI = mc.textField('nameORI', q = True , text = True)
    mc.parent(nameORI, 'SIM_GRP')


    ###RunNextDef
    assCol()


        

    
 
    
def assCol(*args):
    
    print 'listNameChar[0]:', listNameChar[0]
    
    index = listNameChar[0].find(':')
    addCIN = listNameChar[0][:index] + '_CIN' + listNameChar[0][index:]    
    print 'addCIN', addCIN
    
    spl = addCIN.split(':')
    print 'spl', spl
    
    GeoCIN = spl[0] + ':Geo_Grp'
    
    
    mc.select('noTouch_grp', r = True)
    mc.select(GeoCIN, add = True)
    mc.select('Geo_Grp_LOC', add = True)
    mc.select('SIM_GRP', add = True)
    
    listColor = [(1,0,0), (1.0,0.0,1.0), (0.49803900718688965, 1.0, 0.0), (0.0, 1.0, 1.0)]
    
    print (listColor[0])[0]
    
    
    grpColor = mc.ls(sl=True)
    print "grpColor ", grpColor 
    
    for i in range(len(grpColor)) :
        
        print 'grpColor[i]', grpColor[i]
        print 'listColor[i]', listColor[i]
                      
        mc.setAttr ( grpColor[i] + ".useOutlinerColor" , True)
    
        mc.setAttr ( grpColor[i] + ".outlinerColor" , listColor[i][0],listColor[i][1],listColor[i][2])
    
        mel.eval('AEdagNodeCommonRefreshOutliners();')
        
        mc.setAttr("noTouch_grp.visibility", 0)
        
        
        
    grpORI = mc.textField('nameORI', q = True , text = True) 
    print grpORI
    #mc.parent(grpORI, 'SIM_GRP')
    
    
    ###RunNextDef
    createLyDYN()
    
     
    
def createLyDYN(*args):
    
    mc.select(cl = True)
    
    for ly in range(3):
        
        num = ly + 1
        
        hairTubeDen = mc.createDisplayLayer (name = "hairTubeDensity%s_layer"%num , number = 1)
        
        mc.setAttr ( '%s.displayType'%hairTubeDen, 0 ) 
        mc.setAttr ( '%s.color'%hairTubeDen, 28 ) 
        mc.setAttr ( '%s.overrideColorRGB'%hairTubeDen, 0, 0, 0 ) 
        mc.setAttr ( '%s.overrideRGBColors'%hairTubeDen, 0 )


    #Create Layer HairTube
    hairTube = mc.createDisplayLayer (name = "hairTube_layer" , number = 1)
    
    mc.setAttr ( '%s.displayType'%hairTube, 0 ) 
    mc.setAttr ( '%s.color'%hairTube, 29 ) 
    mc.setAttr ( '%s.overrideColorRGB'%hairTube, 0, 0, 0 ) 
    mc.setAttr ( '%s.overrideRGBColors'%hairTube, 0 )



    mc.select('*N*_CRV', r = True )
    selsCRV = mc.ls(sl = True)
    print 'selsCRV', selsCRV           
    #Create Layer HairDYN
    hairDYNLy = mc.createDisplayLayer (name = "hairDYN_layer" , number = 1)
    print "hairDYNLy:", hairDYNLy
    
    mc.setAttr ( '%s.displayType'%hairDYNLy, 0 ) 
    mc.setAttr ( '%s.color'%hairDYNLy, 22 ) 
    mc.setAttr ( '%s.overrideColorRGB'%hairDYNLy, 0, 0, 0 ) 
    mc.setAttr ( '%s.overrideRGBColors'%hairDYNLy, 0 )
           
    ###RunNextDef        
    randomNurb()




def randomNurb(*args):
    
    print 'listNameORI', listNameORI
    nameORI= listNameORI[0].split('_')
    
    
    mc.select('%s*_tubeGRP'%nameORI[0],r = True)
    selsGrp = mc.ls(sl = True)
    print selsGrp
    
    mc.editDisplayLayerMembers ( 'hairTube_layer',  selsGrp  )
    
    
    mc.select('%s*_tubeGRP'%nameORI[0],r = True, hi = True)
    sels = mc.ls(sl = True)
    
    num = len(sels)
    print num
    
    
    for s in range(3):
        numB = 4 - s 
        print 'numB', numB
        numA = s + 1
        print numA
    
        #mc.select(sels, cl = True)
        
        aa = [random.randrange(num) for x in range(num/numB)]
        
        print 'aa', aa
        listSelsN = []
        
        for i in aa:
            print i
            selsN = sels[i]
            print 'selsN:', selsN
            
            if 'tubeGRP' in selsN :
                print 'pass'
                
            else:
                print 'ok'
            
                listSelsN.append(selsN)
            
                                     
                mc.editDisplayLayerMembers ( 'hairTubeDensity%s_layer'%numA,  listSelsN  ) 
    
    
    ###RunNextDef    
    makeGreenShade() 
 

def makeGreenShade(*args):
    
    nameShd = 'hairGreen_shdTest'
    
    mc.shadingNode ('lambert' , asShader = True , n = '%s'%nameShd)
    mc.sets (renderable = True, noSurfaceShader = True, empty = True ,name = '%sSG'%nameShd)
    mc.connectAttr('%s.outColor'%nameShd, '%sSG.surfaceShader'%nameShd)
    
    myRamp = mc.shadingNode('ramp', asTexture = True )
    myPlace2d = mc.shadingNode('place2dTexture', asUtility = True )
    mc.connectAttr( '%s.outUV'%myPlace2d, '%s.uv'%myRamp)
    mc.connectAttr( '%s.outUvFilterSize'%myPlace2d, '%s.uvFilterSize'%myRamp)
    mc.defaultNavigation(source = '%s'%myRamp ,destination = '%s.color'%nameShd, connectToExisting = True)
    mc.connectAttr('%s.outColor'%myRamp, '%s.color'%nameShd, f = True)
    
    
    mc.setAttr("%s.colorEntryList[1].color"%myRamp, 0.356404, 1.000024, 0 , type = 'double3')
    mc.setAttr ("%s.colorEntryList[1].position"%myRamp, 1)
    
    mc.setAttr( "%s.colorEntryList[2].color"%myRamp, 0, 0.610501, 0,  type = 'double3' )
    mc.setAttr ("%s.colorEntryList[2].position"%myRamp, 0.5)
    
    mc.setAttr ("%s.colorEntryList[0].color"%myRamp, 0 , 0.0843749 ,0,  type = 'double3' )
    mc.setAttr ("%s.colorEntryList[0].position"%myRamp, 0)
    
    
    mc.select('*N1_tubeGRP', r = True)
    
    mc.sets( e = True, forceElement = '%sSG'%nameShd)
    
    
    ###RunNextDef
    makeBlueShade()
    makeRedShade()
    makeYellowShade()
    
    
    delGeoGrp()
    
    
    

def makeBlueShade(*args):
    
    nameShd = 'hairBlue_shdTest'
    
    mc.shadingNode ('lambert' , asShader = True , n = '%s'%nameShd)
    mc.sets (renderable = True, noSurfaceShader = True, empty = True ,name = '%sSG'%nameShd)
    mc.connectAttr('%s.outColor'%nameShd, '%sSG.surfaceShader'%nameShd)
    
    myRamp = mc.shadingNode('ramp', asTexture = True )
    myPlace2d = mc.shadingNode('place2dTexture', asUtility = True )
    mc.connectAttr( '%s.outUV'%myPlace2d, '%s.uv'%myRamp)
    mc.connectAttr( '%s.outUvFilterSize'%myPlace2d, '%s.uvFilterSize'%myRamp)
    mc.defaultNavigation(source = '%s'%myRamp ,destination = '%s.color'%nameShd, connectToExisting = True)
    mc.connectAttr('%s.outColor'%myRamp, '%s.color'%nameShd, f = True)
    
    
    mc.setAttr("%s.colorEntryList[1].color"%myRamp, 0, 0.991112, 0.991112, type = 'double3')
    mc.setAttr ("%s.colorEntryList[1].position"%myRamp, 1)
    
    mc.setAttr( "%s.colorEntryList[2].color"%myRamp, 0, 0.36131, 0.564708 ,  type = 'double3' )
    mc.setAttr ("%s.colorEntryList[2].position"%myRamp, 0.5)
    
    mc.setAttr ("%s.colorEntryList[0].color"%myRamp, 0, 0.00802302 ,0.0451856 ,  type = 'double3' )
    mc.setAttr ("%s.colorEntryList[0].position"%myRamp, 0)
    
    
    mc.select('*N2_tubeGRP', r = True)
    
    mc.sets( e = True, forceElement = '%sSG'%nameShd)
    
    
    
def makeRedShade(*args):
    
    nameShd = 'hairRed_shdTest'
    
    mc.shadingNode ('lambert' , asShader = True , n = '%s'%nameShd)
    mc.sets (renderable = True, noSurfaceShader = True, empty = True ,name = '%sSG'%nameShd)
    mc.connectAttr('%s.outColor'%nameShd, '%sSG.surfaceShader'%nameShd)
    
    myRamp = mc.shadingNode('ramp', asTexture = True )
    myPlace2d = mc.shadingNode('place2dTexture', asUtility = True )
    mc.connectAttr( '%s.outUV'%myPlace2d, '%s.uv'%myRamp)
    mc.connectAttr( '%s.outUvFilterSize'%myPlace2d, '%s.uvFilterSize'%myRamp)
    mc.defaultNavigation(source = '%s'%myRamp ,destination = '%s.color'%nameShd, connectToExisting = True)
    mc.connectAttr('%s.outColor'%myRamp, '%s.color'%nameShd, f = True)
    
    
    mc.setAttr("%s.colorEntryList[1].color"%myRamp, 0.991112, 0.0886537, 0 , type = 'double3')
    mc.setAttr ("%s.colorEntryList[1].position"%myRamp, 1)
    
    mc.setAttr( "%s.colorEntryList[2].color"%myRamp,  0.371239, 0, 0.000606903 ,  type = 'double3' )
    mc.setAttr ("%s.colorEntryList[2].position"%myRamp, 0.5)
    
    mc.setAttr ("%s.colorEntryList[0].color"%myRamp, 0.104615, 0, 0.00242761,  type = 'double3' )
    mc.setAttr ("%s.colorEntryList[0].position"%myRamp, 0)
    
    
    mc.select('*N3_tubeGRP', r = True)
    
    mc.sets( e = True, forceElement = '%sSG'%nameShd)
    
    
    
def makeYellowShade(*args):
    
    nameShd = 'hairYellow_shdTest'
    
    mc.shadingNode ('lambert' , asShader = True , n = '%s'%nameShd)
    mc.sets (renderable = True, noSurfaceShader = True, empty = True ,name = '%sSG'%nameShd)
    mc.connectAttr('%s.outColor'%nameShd, '%sSG.surfaceShader'%nameShd)
    
    myRamp = mc.shadingNode('ramp', asTexture = True )
    myPlace2d = mc.shadingNode('place2dTexture', asUtility = True )
    mc.connectAttr( '%s.outUV'%myPlace2d, '%s.uv'%myRamp)
    mc.connectAttr( '%s.outUvFilterSize'%myPlace2d, '%s.uvFilterSize'%myRamp)
    mc.defaultNavigation(source = '%s'%myRamp ,destination = '%s.color'%nameShd, connectToExisting = True)
    mc.connectAttr('%s.outColor'%myRamp, '%s.color'%nameShd, f = True)
    
    
    mc.setAttr("%s.colorEntryList[1].color"%myRamp, 1.000024, 1.000024, 0 , type = 'double3')
    mc.setAttr ("%s.colorEntryList[1].position"%myRamp, 1)
    
    mc.setAttr( "%s.colorEntryList[2].color"%myRamp, 1.000024, 0.428685, 0,  type = 'double3' )
    mc.setAttr ("%s.colorEntryList[2].position"%myRamp, 0.5)
    
    mc.setAttr ("%s.colorEntryList[0].color"%myRamp, 0.104615, 0.0395457, 0,  type = 'double3' )
    mc.setAttr ("%s.colorEntryList[0].position"%myRamp, 0)
    
    
    mc.select('*N4_tubeGRP', r = True)
    
    mc.sets( e = True, forceElement = '%sSG'%nameShd)




     
def delGeoGrp(*args):
    
    geoGrp = 'Geo_Grp' 
    
    mc.select(geoGrp, r = True)
    
    mc.delete()
    
    ##NextFunction
    grpNRig()
    
    
def grpNRig(*args):
    
    lsNRg = mc.ls(type = 'nRigid')
    
    
    mc.group(lsNRg, n = 'nRigid_Grp')
    
    mc.parent('nRigid_Grp', 'SIM_GRP')
    
    
    
    ##NextFunction
    hideGeoCol()
    
def hideGeoCol(*args):
           
    print listNameChar
    
    splName = listNameChar[0].split(':')
    print 'splName', splName
        
    index = listNameChar[0].find(':')
    addCOL = listNameChar[0][:index] + '_COL' + listNameChar[0][index:]
    print "addCOL:", addCOL
                
    mc.setAttr("%s.visibility"%addCOL, 0)   
    
    
    
    
    ##NextFunction
    delGeoUV() 
    
    
def delGeoUV(*args):
    
    print 'listNameGeoUV', listNameGeoUV
    
    if 1 == len(listNameGeoUV):
        print 'delete it'
        mc.select(listNameGeoUV[0], r = True)
        mc.delete()
        
    else:
        print 'no edit uv geo' 
        
    
    
    ##NextFunction
    myMotionPath()  
        
 
 
 
        
def myMotionPath(*args):
        
    #myBase = ['HairFront1_L_CRV_base']
    mc.select('*_CRV_base')
    myBase = mc.ls(sl = True)
    print 'myBase', myBase
        
    #myCurveShp = ['HairFront1_L_CRVShape']
    mc.select('*_CRVShape', r = True)
    myCurveShp = mc.ls(sl = True)
    print 'myCurveShp', myCurveShp
    
    print 'myBase', len(myBase)
    print 'myCurveShp', len(myCurveShp)
        
    for i in range(len(myBase)):
        print 'myBase', myBase[i]
        print 'myCurveShp', myCurveShp[i]
        
        myMtp = mc.createNode('motionPath', n = 'myMotionPath')
                
        mc.connectAttr('%s.worldSpace[0]'%myCurveShp[i], '%s.geometryPath'%myMtp)
        
        #motionPath to circle
        mc.connectAttr('%s.message'%myMtp, '%s.specifiedManipLocation'%myBase[i])
        
        mc.connectAttr('%s.rotateX'%myMtp, '%s.rotateX'%myBase[i])
        mc.connectAttr('%s.rotateY'%myMtp, '%s.rotateY'%myBase[i])
        mc.connectAttr('%s.rotateZ'%myMtp, '%s.rotateZ'%myBase[i])
        
        mc.connectAttr('%s.rotateOrder'%myMtp, '%s.rotateOrder'%myBase[i])
        
        
        myAdlX = mc.createNode('addDoubleLinear', n = 'myAdlX')
        myAdlY = mc.createNode('addDoubleLinear', n = 'myAdlY')
        myAdlZ = mc.createNode('addDoubleLinear', n = 'myAdlZ')
        
        ###X
        #disconnectAttr HairFront4_CRV_mtp.xCoordinate HairFront4_CRV_adlX.input2;
        mc.connectAttr('%s.xCoordinate'%myMtp, '%s.input2'%myAdlX)
        
        #disconnectAttr HairFront4_base.transMinusRotatePivotX HairFront4_CRV_adlX.input1;
        mc.connectAttr('%s.transMinusRotatePivotX'%myBase[i], '%s.input1'%myAdlX)
        
        #disconnectAttr HairFront4_CRV_adlX.output HairFront4_base.translateX;
        mc.connectAttr('%s.output'%myAdlX, '%s.translateX'%myBase[i])
        
        ###Y
        mc.connectAttr('%s.yCoordinate'%myMtp, '%s.input2'%myAdlY)
        
        mc.connectAttr('%s.transMinusRotatePivotY'%myBase[i], '%s.input1'%myAdlY)
        
        mc.connectAttr('%s.output'%myAdlY, '%s.translateY'%myBase[i])
        
        ###Z
        mc.connectAttr('%s.zCoordinate'%myMtp, '%s.input2'%myAdlZ)
        
        mc.connectAttr('%s.transMinusRotatePivotZ'%myBase[i], '%s.input1'%myAdlZ)
        
        mc.connectAttr('%s.output'%myAdlZ, '%s.translateZ'%myBase[i])
        
        
        
    ##NextFunction
    assNRig()


def assNRig(*args):
    
    mc.select('nRigid*', r = True)
    selsNR = mc.ls(sl = True)
    print selsNR
        
    for i in range(4):
        print selsNR[i]
        num = i + 1
    
        mc.select(selsNR[i], r = True)
        
        if 1 == num:
            print '1'
            
        else:
            mel.eval('assignNSolver nucleus%s;'%num)
            
    
    
    
    
    ##nextDefine        
    mnCOLbsh()
            
            
            
            
            
          
def mnCOLbsh(*args):
    
    
    print 'listBodyCOLN', listBodyCOLN

    for i in range(len(listBodyCOLN)):
        print i                               
                             
        numList = len(listBodyCOLN)
        print numList
        
        numI = i + 1
        
                        
        if numI == numList:
            print 'end'
            
        elif numI < numList:
            print 'blendShape to MNCol'
            
            mc.select(listBodyCOLN[i], r = True)  
            mc.select(listBodyCOLN[i+1], add = True)
                            
            mc.blendShape(automatic = True, origin = 'world', n = '%s_BSH'%listBodyCOLN[i] , w = ( 0 , 1 ) )    
            
    #RunNextFuntion        
    conntNrgdBB()
            
            
            


def conntNrgdBB(*args):
        
              
    listNrgd = mc.ls(type = 'nRigid' )
    print 'listNrgd', listNrgd
    
    
    listShpNg = []
    
    
    for ln in range(len(listNrgd)):
        
        if listNrgd[ln] == listNrgd[3]:
            print  'listNrgd[3]', listNrgd[3]
            
        else:
            print 'ok'
            
            
            mc.connectAttr('%s.isDynamic'%listNrgd[ln], '%s.isDynamic'%listNrgd[ln+1])
            mc.connectAttr('%s.cofl'%listNrgd[ln], '%s.cofl'%listNrgd[ln+1])
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.cold";
            mc.connectAttr('%s.cold'%listNrgd[ln], '%s.cold'%listNrgd[ln+1])
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.pou";
            mc.connectAttr('%s.pou'%listNrgd[ln], '%s.pou'%listNrgd[ln+1])
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.por";
            mc.connectAttr('%s.por'%listNrgd[ln], '%s.por'%listNrgd[ln+1])
            
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.cop";
            mc.connectAttr('%s.cop'%listNrgd[ln], '%s.cop'%listNrgd[ln+1])
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.tpc";
            mc.connectAttr('%s.tpc'%listNrgd[ln], '%s.tpc'%listNrgd[ln+1])
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.dcr";
            mc.connectAttr('%s.dcr'%listNrgd[ln], '%s.dcr'%listNrgd[ln+1])
            
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.dcg";
            mc.connectAttr('%s.dcg'%listNrgd[ln], '%s.dcg'%listNrgd[ln+1])
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.dcb";
            mc.connectAttr('%s.dcb'%listNrgd[ln], '%s.dcb'%listNrgd[ln+1])
            
            #CBdeleteConnection "BodyFur_GeoN4COL_nRigidShape.boce";
            mc.connectAttr('%s.boce'%listNrgd[ln], '%s.boce'%listNrgd[ln+1])
            
            
            mc.connectAttr('%s.collideStrength'%listNrgd[ln], '%s.collideStrength'%listNrgd[ln+1])
            mc.connectAttr('%s.collisionLayer'%listNrgd[ln], '%s.collisionLayer'%listNrgd[ln+1])
            mc.connectAttr('%s.thickness'%listNrgd[ln], '%s.thickness'%listNrgd[ln+1])
            
            mc.connectAttr('%s.friction'%listNrgd[ln], '%s.friction'%listNrgd[ln+1])
            mc.connectAttr('%s.stickiness'%listNrgd[ln], '%s.stickiness'%listNrgd[ln+1])
            
            
            
            
            
                 
    
def getORIGrp(*args):
    
    selsORI = mc.ls(sl = True)
    print selsORI
    mc.textField('nameORI', e = True , text = selsORI[0])
    mc.textField('nameORI', q = True , text = True)   
    
    listNameORI.append(selsORI[0]) 
    
    
    
        
def dupForORI(*args):
    
    selsCVGrp = mc.ls(sl = True)
    print selsCVGrp
    
    dupCVGrp = mc.duplicate( selsCVGrp, rr = True)
    print 'dupCVGrp:', dupCVGrp
    
    mc.parent( w = True)
    
    mc.select(dupCVGrp, hi = True, r = True)
    selsCV = mc.ls(sl = True)
    print 'selsCV:', selsCV
    
    for i in selsCV:
        
        print 'i', i
        
        if 'Shape' in i:
            print 'this is Shape'
            
        else:
            print 'Change to ORI'
        
            
            nameORI = i.replace('_CRV', '_ORI')
            print 'nameORI', nameORI
            reName = mc.rename(i, nameORI)
            
            mc.delete( reName, constructionHistory = True )
            
            

def getGeoUV(*args): 

    selGeoUV = mc.ls(sl = True)
    print selGeoUV
    mc.textField('nameGeoUV', e = True , text = selGeoUV[0])
    mc.textField('nameGeoUV', q = True , text = True)   
    
    listNameGeoUV.append(selGeoUV[0]) 
    
    
    




           
def myWindow(*args):
    
    windowSetHair = "SetUp_Hair"

    if (mc.window(windowSetHair, exists = True )):
        print "True"
        print "myWindow open already"
        mc.deleteUI(windowSetHair)
        
    else:
        print "No myWindow open "
    
    
    # Make a new window
    
    windowSetHair = mc.window(windowSetHair, title= "SetUp_Hair", widthHeight=(200, 800), sizeable = True )
    
    mc.frameLayout( label='Duplicate CurveGrp From Groom' )
    mc.separator( height=10, style='out' )
    
    mc.setParent ('..')
    
    mc.rowColumnLayout( numberOfColumns=1 , width = 200, h =40)
    mc.button(label = 'Duplicate Curve Grp', width = 200, command = dupForORI)
    mc.separator( height=10, style='out' )
    mc.setParent ('..')
    
    
    mc.frameLayout( label='Run HairDynamic' )
    mc.setParent ('..')
    mc.setParent ('..')
        
    mc.rowColumnLayout( numberOfColumns=2 , columnAttach=(2, 'both', 20), columnWidth =[(1, 100), (2, 100)])
    mc.text( label='edge1' )
    mc.text( label='edge2' )
    
    mc.setParent ('..')
    mc.rowColumnLayout( numberOfColumns=2 , columnAttach=(1, 'both', 4), columnWidth =[(1, 100), (2, 100)])
    edge1 = mc.textField('edge1', text = '00')    
    edge2 = mc.textField('edge2', text = '00')
    
    mc.setParent ('..')
    
    
    mc.rowColumnLayout( numberOfColumns=1 , width = 200, h =400)
    mc.button(label = 'get edges', width = 200, command = getEdges)
    mc.button(label = 'save edges', width = 200, command = saveEdges)
    mc.button(label = 'load edges', width = 200, command = loadEdges)
    
    mc.separator( height=40, style='out' )
    
    mc.rowColumnLayout( numberOfColumns= 1 , columnWidth =[1, 200])
    mc.text( label='hair ORI Grp' )
    
    
    mc.setParent ('..')
    mc.rowColumnLayout( numberOfColumns= 1 , columnWidth =[1, 200])
    nameORI = mc.textField('nameORI', text = '')
    mc.button(label = 'get ORI Grp', width = 200, command = getORIGrp)    
        
    mc.separator( height=40, style='out' )
    
    mc.rowColumnLayout( numberOfColumns=1 , width = 200, h =40)
    mc.button(label = 'Run HairDynamic', width = 200, command = createRef, backgroundColor = [ 1, 0.843137, 0])
    
    mc.separator( height=10, style='out' )
        
    mc.setParent ('..')
    
    
    
    mc.frameLayout( 'frameA',label = 'Geo Edit UV', collapsable = True, collapse = True )
    nameGeoUV = mc.textField('nameGeoUV', text = '', p = 'frameA')
    mc.button(label = 'get Geo EditUV', width = 200, command = getGeoUV)    
        
    mc.separator(  style='out' )
    
    
    
    
    
    mc.showWindow( windowSetHair )
    
    
def run():
    myWindow()
    
    
    
    









