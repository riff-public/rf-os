# bird sim ETC

import pymel.core as pm ;

def F2CCopy ( *args ) :

    selection = pm.ls ( sl = True ) ;
    
    def getHairName ( *args ) :
    
        hairNameList = [] ;
    
        for each in selection :
            
            children = pm.listRelatives ( each , c = True ) ;
            
            for child in children :
                if 'ORI' in str ( child ) :
                    name = child.split ( '_' ) [0] ; 
                    hairNameList.append ( name ) ;
                    
        return hairNameList ;
        
    nameList = getHairName ( ) ;
 
    pm.scrollField ( 'F2CTxtScroll' , e = True , text = str ( nameList ) ) ;

def F2CPaste ( *args ) :
 
    listText = pm.scrollField ( 'F2CTxtScroll' , q = True , text = True ) ;

    listText = 'list' + ' ' + '=' + ' ' + listText ;

    exec ( listText ) ;

    selection = pm.ls ( sl = True ) ;
    
    for i in range ( 0 , len ( selection ) ) :
        pm.rename ( selection [i] , list [i] + '_CRV' ) ;

#################
#################

def selectStep ( step = 2 , *args ) : 

    selection = pm.ls ( sl = True ) ;

    selectionList = [] ;

    for i in range ( 0 , len ( selection ) , step ) :

        selectionList.append ( selection [ i ] ) ;
        
    pm.select ( selectionList ) ;

def selectStep2 ( *args ) :
    selectStep ( step = 2 ) ;

def selectStep3 ( *args ) :
    selectStep ( step = 3 ) ;

#################
#################

def selectCurveCV ( *args ) :

	cv = pm.textField ( 'hairCV' , q = True , text = True ) ;

	selection = pm.ls ( sl = True ) ;

	toSelectList = [] ;

	for each in selection :

		toSelectList.append ( str ( each ) + '.cv[%s]' % cv ) ;

	pm.select ( toSelectList ) ;


def vertexToNrb ( *args ) :
    # select 4 vertex then convert it to nurb plane

    selection = pm.ls ( os = True ) ;

    transformShape = pm.ls ( sl = True , o = True )[0] ;

    name = transformShape.split ( '_' ) [0] ;

    nrb = pm.polyPlane ( w = 1 , h = 1 , sx = 1 , sy = 1 , ax = ( 0 , 1 , 0 ) , cuv = 2 ,ch = 1 , n = name + '_nrb' ) [0] ;

    #polyPlane -w 1 -h 1 -sx 1 -sy 1 -ax 0 1 0 -cuv 2 -ch 1;

    for i in range ( 0 , 4 ) :
        
        pos = pm.xform ( selection [i] , q = True , t = True , ws = True ) ;
        pm.move ( '%s.vtx[%d]' % ( nrb , i ) , pos ) ;

############################################################################################################################################################################################
############################################################################################################################################################################################
############################################################################################################################################################################################
############################################################################################################################################################################################
############################################################################################################################################################################################

width = 250.00 ;
UI_title = 'Bird ETC simulation utilities' ;

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = '' ) ;

def frameLayout_cc ( *args ) :
    pm.window ( 'simETCUI' , e = True , h = 10 ) ;

def run ( *args ) :
    
    if pm.window ( 'simETCUI' , exists = True ) :
        pm.deleteUI ( 'simETCUI' ) ;
    else : pass ;
   
    window = pm.window ( 'simETCUI', title = UI_title , mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    pm.window ( 'simETCUI' , e = True , w = width , h = 10 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            with pm.frameLayout ( label = 'curve name ( follcles > curves )' ,
                collapsable = True , collapse = False , cc = frameLayout_cc , w = width ) :

                with pm.rowColumnLayout ( nc = 1 ) :

                    separator ( 5 ) ;

                    with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/50 ) , ( 2 , width/50*48 ) , ( 3 , width/50 ) ] ) :

                        filler ( ) ;

                        with pm.rowColumnLayout ( nc = 1 ) :
                            pm.text ( label = r'copy curve name' , bgc = ( 1 , 0.4 , 0.7 ) ) ;
                            pm.text ( label = r'under selected follicles  > selected curves' , bgc = ( 1 , 0.4 , 0.7 ) ) ;

                            pm.textScroll = pm.scrollField ( 'F2CTxtScroll' , wordWrap = True , h = 100 , editable = False ) ;
                            
                            with pm.rowColumnLayout ( nc = 2  , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                                pm.button ( label = 'copy' , bgc = ( 1 , 0.5 , 0.75 ) , c = F2CCopy ) ;
                                pm.button ( label = 'paste' , bgc = ( 1 , 0.6 , 0.8 ) , c = F2CPaste ) ;

                        filler ( ) ;

                    separator ( 5 ) ;

            pm.separator ( vis = True , h = 10 , bgc = ( 0 , 0 , 0 ) ) ;
            
            selectStepLayout =  pm.rowColumnLayout ( nc = 2  , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) ;
            with selectStepLayout :

                pm.button ( label = 'select step 2' , c = selectStep2 , bgc = ( 0 , 1 , 0 ) ) ;
                pm.button ( label = 'select step 3' , c = selectStep3 , bgc = ( 0.678431 , 1 , 0.184314 ) ) ;

            pm.separator ( vis = False , h = 5 ) ;

            selectHairTipLayout =  pm.rowColumnLayout ( nc = 2  , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) ;
            with selectHairTipLayout :

            	pm.textField ( 'hairCV' , text = '23' ) ;
            	pm.button ( label = 'select curve cv(s)' , c = selectCurveCV ) ;

            pm.separator ( vis = False , h = 5 ) ;

            pm.text ( label = 'convert 4 vertex to nurb plane') ;
            pm.text ( label = '(select in "Z" manner)') ;

            pm.button ( label = 'convert', c = vertexToNrb ) ;
            
    window.show () ;

run ( ) ;

