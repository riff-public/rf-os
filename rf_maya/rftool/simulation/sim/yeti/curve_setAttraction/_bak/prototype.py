import pymel.core as pm ;

class YetiSet ( object ) :
    
    def __init__ ( self ) :
        set                 = pm.ls ( sl = True ) [0] ;
        self.setItem_list   = pm.sets ( set , q = True ) ;

    def setAttraction ( self , base , tip ) :
        for curve in self.setItem_list :
            curveShape = curve.getShape() ;
            curveShape.tipAttraction.set(base) ;
            curveShape.baseAttraction.set(tip) ;