import pymel.core as pm ;

class SetCurveAttraction ( object ) :

    def __init__ ( self ) :
        pass ;

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def insertUI ( self , width ) :

        pm.text ( label = 'help: select "curve(s)" or "guide set"' )

        with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/3 ) , ( 2 , width/3 ) , ( 3 , width/3 ) ] ) : 

            with pm.rowColumnLayout ( nc = 1 , cw = [ ( 1 , width/3 ) ] ) :
                pm.text ( label = 'base attract' ) ;
                self.base_field = pm.floatField ( 'yeti_baseAttraction_floatField' , precision = 2 , max = 1 , min = 0 , v = 1 ) ;

            with pm.rowColumnLayout ( nc = 1 , cw = [ ( 1 , width/3 ) ] ) :
                pm.text ( label = 'tip attract' ) ;
                self.tip_field  = pm.floatField ( 'yeti_tipAttraction_floatField' , precision = 2 , max = 1 , min = 0 , v = 1 ) ;

            with pm.rowColumnLayout ( nc = 1 , cw = [ ( 1 , width/3 ) ] ) :
                pm.text ( label = '' ) ;
                self.button     = pm.button ( 'yeti_setAttraction_btn' , label = 'set' , bgc = ( 1 , 1 , 1 ) , c = self.setAttraction ) ;

    def setAttraction ( self , *args ) :

        self.base_val   = pm.floatField ( self.base_field , q = True , v = True ) ;
        self.tip_val    = pm.floatField ( self.tip_field , q = True , v = True ) ;

        ### select curves or a set ###

        selection = pm.ls ( sl = True ) ;

        ### check if set or curves ###

        if selection :

            ### check selection node type ###

            try :
                nodeType = selection[0].nodeType() ;
                
                if nodeType == 'objectSet' :
                    pass ;
                
                else :
                    nodeType = selection[0].getShape().nodeType() ;
                    
                    if nodeType == 'nurbsCurve' :
                        pass ;
                    else :
                        nodeType = None ;

            except :

                nodeType = None ;

            if nodeType :

                ### set target ###

                if nodeType == 'objectSet' :
                    target = pm.sets ( selection[0] , q = True ) ;

                elif nodeType == 'nurbsCurve' :
                    target = selection ;

                ### operation ###

                for curve in target :
                    curveShape = curve.getShape() ;
                    curveShape.tipAttraction.set ( self.base_val ) ;
                    curveShape.baseAttraction.set ( self.tip_val ) ;

def insert ( width , *args ) :
    ui = SetCurveAttraction() ;
    ui.insertUI ( width = width ) ;