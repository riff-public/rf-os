import pymel.core as pm ;

class SetCurveWeight ( object ) :

    def __init__ ( self ) :
        pass ;

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def insertUI ( self , width ) :

        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) : 

            with pm.rowColumnLayout ( nc = 1 , cw = [ ( 1 , width/2 ) ] ) :
                pm.text ( label = 'weight' ) ;
                self.weight_field = pm.floatField ( 'yeti_weight_floatField' , precision = 2 , max = 1 , min = 0 , v = 1 ) ;

            with pm.rowColumnLayout ( nc = 1 , cw = [ ( 1 , width/2 ) ] ) :
                pm.text ( label = '' ) ;
                self.button     = pm.button ( 'yeti_setWeight_btn' , label = 'set' , bgc = ( 1 , 1 , 1 ) , c = self.setAttraction ) ;

    def setAttraction ( self , *args ) :

        self.weight_val   = pm.floatField ( self.weight_field , q = True , v = True ) ;

        ### select curves or a set ###

        selection = pm.ls ( sl = True ) ;

        ### check if set or curves ###

        if selection :

            ### check selection node type ###

            try :
                nodeType = selection[0].nodeType() ;
                
                if nodeType == 'objectSet' :
                    pass ;
                
                else :
                    nodeType = selection[0].getShape().nodeType() ;
                    
                    if nodeType == 'nurbsCurve' :
                        pass ;
                    else :
                        nodeType = None ;

            except :

                nodeType = None ;

            if nodeType :

                ### set target ###

                if nodeType == 'objectSet' :
                    target = pm.sets ( selection[0] , q = True ) ;

                elif nodeType == 'nurbsCurve' :
                    target = selection ;

                ### operation ###

                for curve in target :
                    curveShape = curve.getShape() ;
                    curveShape.weight.set ( self.weight_val ) ;

def insert ( width , *args ) :
    ui = SetCurveWeight() ;
    ui.insertUI ( width = width ) ;