def export_cache(node, path, startframe, endframe): 
	# set path 
	mc.setAttr('%s.outputCacheFileName' % node, path, type='string')
	mc.setAttr('%s.outputCacheFrameRangeStart' % node, startframe)
	mc.setAttr('%s.outputCacheFrameRangeEnd' % node, endframe)
	mc.setAttr('%s.outputCacheNumberOfSamples' % node, 1)
	mm.eval('AEpgYetiWriteCacheCMD %s' % node)

def import_cache(nodeName, path): 
	node = mm.eval('pgYetiCreate();')
	mc.setAttr('%s.fileMode' % node, 1)
	mc.setAttr('%s.cacheFileName' % node, path, type='string')
	mc.setAttr('%s.ignoreCachePreview' % node, 1)
	transform = mc.listRelatives(node, p=True)
	if transform: 
		newName = mc.rename(transform, nodeName)
	return newName 