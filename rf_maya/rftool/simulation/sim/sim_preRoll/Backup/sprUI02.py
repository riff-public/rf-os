import maya.cmds as mc
import sys
import pymel.core as pmc
#sys.path.append(r'N:\Staff\Hong\Hong\Scripts\sim\preRoll')
import simPreRoll as spr
reload(spr)

class preRollUI():
	def __init__(self):
		self.NS = ''
		self.TPoseFrame = 971
		self.simPreRollFrame = 981
		self.AnimpreRollFrame = 991
		self.startFrame = 0
		self.endFrame = spr.assumePreRoll() - 1
		self.windows = 'sprWin'

	###### update variable for function #############
	def updateNS(self,*args):
		self.NS = mc.textField('nsTextField',q=1,text=1)
	
	def updateNSButton(self,*args):
		self.NS = pmc.selected()[0].namespace()[:-1]
		mc.textField('nsTextField',e=1,text=self.NS)

	def updateTPoseFrame(self,*args):
		self.TPoseFrame = float(mc.textField('tpText',q=1,text=1))
	def updatesimPreRollFrame(self,*args):
		self.simPreRollFrame = float(mc.textField('sprText',q=1,text=1))
	def updateAnimpreRollFrame(self,*args):
		self.AnimpreRollFrame = float(mc.textField('anmText',q=1,text=1))
	def updateStartFrame(self,*args):
		self.startFrame = float(mc.textField('startFrame',q=1,text=1))
	def updateEndFrame(self,*args):
		self.endFrame = float(mc.textField('endFrame',q=1,text=1))

	####### command button ############################

	def createPreRoll(self,*args):
		self.NS = mc.textField('nsTextField',q=1,text=1)
		self.TPoseFrame = float(mc.textField('tpText',q=1,text=1))
		self.simPreRollFrame = float(mc.textField('sprText',q=1,text=1))
		self.AnimpreRollFrame = float(mc.textField('anmText',q=1,text=1))
		spr.setPreRoll(self.NS,self.TPoseFrame,self.simPreRollFrame,self.AnimpreRollFrame)
	def deleteKey(self,*args):
		self.startFrame = float(mc.textField('startFrame',q=1,text=1))
		self.endFrame = float(mc.textField('endFrame',q=1,text=1))
		spr.delCtrlKey(self.NS,self.startFrame,self.endFrame)


	def sprUI(self):
		if mc.window(self.windows,exists=1):
			mc.deleteUI(self.windows)
		win = mc.window(self.windows,title = 'Sim PreRoll Tool', sizeable=False)
		mc.showWindow(self.windows)
		
		main = mc.columnLayout('main',rs=5,co=['both',12])
		layout1 = mc.columnLayout('preRoll Tool',rs=5,co=['both',8])
		mc.separator(h=2,w=300,st='none')
		
		mc.rowLayout('row1',numberOfColumns = 5)
		mc.text('text1',label = 'Character NameSpace')
		mc.separator(h=1,w=5,st='none')
		nsTextField = mc.textField('nsTextField',w=150,text='',cc=self.updateNS)
		mc.separator(h=1,w=5,st='none')
		geNS= mc.button('getNS',label = 'Get NameSpace',c=self.updateNSButton)
		mc.setParent(layout1)
		mc.separator(h=2,w=5,st='none')
		#mc.setParent(layout1)
		
		mc.rowLayout('row2',numberOfColumns = 3)
		mc.text('text2',label = 'TPose Frame: ')
		mc.separator(w=35,st='none')
		tpText = mc.textField('tpText',w=50,text=950,cc = self.updateTPoseFrame)
		mc.setParent(layout1)
		
		mc.rowLayout('row3',numberOfColumns = 3)
		mc.text('text3',label = 'SimPreRoll Frame: ')
		mc.separator(w=10,st='none')
		sprfText = mc.textField('sprText',w=50,text=970,cc = self.updatesimPreRollFrame)
		mc.setParent(layout1)

		mc.rowLayout('row4',numberOfColumns = 3)
		mc.text('text4',label = 'AnimPreRoll Frame: ')
		mc.separator(w=1,st='none')
		anmText = mc.textField('anmText',w=50,text=995,cc = self.updateAnimpreRollFrame)
		mc.setParent(layout1)
		mc.separator(h=1,st='none')
		createPreRoll= mc.button('cpr',label = 'Create PreRoll',w=150,c=self.createPreRoll)

		mc.separator(h=10,w=400,st='singleDash')
		mc.text('text5',label = 'DeleteFrame From: ')
		mc.separator(h=2,st='none')
		mc.rowLayout('row5',numberOfColumns = 9)
		#mc.separator(w=5,st='none')
		startFrame = mc.textField('startFrame',w=50,text=0,cc = self.updateStartFrame)
		mc.separator(w=5,st='none')
		mc.text('text6',label = 'to ')
		mc.separator(w=5,st='none')
		endFrame = mc.textField('endFrame',w=50,text=self.endFrame,cc = self.updateEndFrame)
		
		#mc.rowLayout('row6',numberOfColumns = 3)
		#mc.text('text6',label = 'End Frame: ')
		#mc.separator(w=6,st='none')
		
		mc.setParent(layout1)

		mc.setParent(layout1)
		mc.separator(h=1,st='none')
		delKey= mc.button('delKey',label = 'Delete Key',w=150,c=self.deleteKey)

		mc.separator(h=10,st='none')



#preRollUI().sprUI()