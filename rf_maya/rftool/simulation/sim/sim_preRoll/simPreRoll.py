import maya.cmds as mc
import maya.mel as mel
import sys
from rftool.rig.ncmel import match
reload(match)
import LWSnap as lw
reload(lw)

def assumePreRoll():
	sceneFrame = mc.playbackOptions(q=1,min=1) - 5
	return sceneFrame


def createTPose(ctrlList,TPoseFrame):
	if ctrlList != []:
		mc.currentTime(TPoseFrame)
		for ctrl in ctrlList:
			if ('Root_Ctrl' not in ctrl ) and (mc.keyframe(ctrl,q=1,timeChange=1)):
				'''
				if not (mc.getAttr('{}.rx'.format(ctrl),l=1)):
					mc.setAttr('{}.rx'.format(ctrl),0)
					mc.setKeyframe(ctrl+'.rx')
				if not (mc.getAttr('{}.ry'.format(ctrl),l=1)):
					mc.setAttr('{}.ry'.format(ctrl),0)
					mc.setKeyframe(ctrl+'.ry')

				if not (mc.getAttr('{}.rz'.format(ctrl),l=1)):
					mc.setAttr('{}.rz'.format(ctrl),0)
					mc.setKeyframe(ctrl+'.rz')	
				'''
				attrList = mc.listAttr(ctrl,u=1,k=1)
				for attr in attrList:
					if ('scale' not in attr) and ('Scale' not in attr) and not (mc.getAttr('{}.{}'.format(ctrl,attr),l=1)) :
						mc.setAttr('{}.{}'.format(ctrl,attr),0)
						mc.setKeyframe('{}.{}'.format(ctrl,attr),s=0)
			elif ':Root_Ctrl' in ctrl:
				mc.setAttr('{}.rx'.format(ctrl),0)
				mc.setAttr('{}.rz'.format(ctrl),0)
				mc.setKeyframe(ctrl+'.r',s=0,itt='flat',ott='flat')

def copyCtrlToFrame(oriFrame,targetFrame,ctrlList = []):
	if ctrlList != []:
		mc.currentTime(oriFrame)
		mc.select(ctrlList)
		mel.eval('timeSliderCopyKey;')
		mc.currentTime(targetFrame)
		mel.eval('timeSliderPasteKey false;')
		flatTangent(targetFrame,ctrlList)
		#for i in ctrlList:
			#mc.currentTime(oriFrame)
			#mc.setKeyframe(i,s=0,t=oriFrame,itt='flat',ott='flat')
		#mc.copyKey(ctrlList,time=(oriFrame,oriFrame))
		#mc.pasteKey(ctrlList,t=(targetFrame,targetFrame),option = 'insert',)
		#flatTangent(targetFrame,ctrlList)
		#inFlatTangent(oriFrame,ctrlList)

def flatTangent(targetFrame,ctrlList = []):
	mc.keyTangent(ctrlList,t=(targetFrame,targetFrame),inTangentType = 'flat',outTangentType = 'flat',e=1)

def inFlatTangent(targetFrame,ctrlList = []):
	mc.keyTangent(ctrlList,t=(targetFrame,targetFrame),inTangentType = 'flat')
			
def convertIK(swCtrlList=[]):
	if swCtrlList !=[]:
		t = mc.currentTime(q=1)
		for i in swCtrlList:
			if mc.getAttr(i+'.fkIk') == 1 :      
				mc.setKeyframe(i+'.fkIk',s=0,itt='flat')
				#mc.currentTime(targetFrame)
				mc.select(i)
				match.run()
				#mc.setAttr(i+'.fkIk',0)
				mc.setKeyframe(i+'.fkIk',s=0,itt='flat')
				fkContrlList = findFkCtrlFromSw(i)
				mc.setKeyframe(fkContrlList)


def findFkCtrlFromSw(ctrl):
	jntConList = mc.listConnections('{}.fkIk'.format(ctrl),s=1,type='parentConstraint')
	ctrlList = []
	for i in jntConList:
		tl = mc.parentConstraint(i,q=1,tl=1)
		#wal = mc.parentConstraint(i,q=1,wal=1)
		for j in tl:
			#print j
			if 'Fk' in j:
				print j
				fkCon = mc.listConnections(j,s=1,type='parentConstraint')
				print set(fkCon)
				fkJntCon = [k for k in list(set(fkCon)) if 'Fk' in k]
				for l in fkJntCon:
					fkCtrl = mc.parentConstraint(l,q=1,tl=1)
					print fkCtrl
					for m in fkCtrl:
						if 'Gmbl' in m:
							fkCtrl = m.replace('Gmbl','')
						if fkCtrl not in ctrlList:
							ctrlList.append(fkCtrl)
				#ctrl = mc.parentConstraint(fkCon)
	return ctrlList


def convertToLocal(lwCtrl):
	#print ctrlList
	#for i in lwCtrlList:
		#print i
		#print mc.getAttr(i+'.localWorld')
	t= mc.currentTime(q=1)
	if mc.getAttr(lwCtrl+'.localWorld'):
		mc.setKeyframe(lwCtrl,s=0)
		lw.swapToLocal(lwCtrl)
		#mc.setAttr(lwCtrl+'.localWorld',0)
		mc.setKeyframe(lwCtrl,s=0)
		partCtrlList = findFkCtrlFromLW(lwCtrl)
		mc.setKeyframe(partCtrlList, s=0 )

def findFkCtrlFromLW(lwctrl):
	print lwctrl
	lwList = mc.listRelatives(lwctrl,c=1,ad=1,type='transform')
	
	if lwList != None:
		ctrlList = [ctrl for ctrl in lwList if ('Gmbl' not in ctrl) and ('Grp' not in ctrl)]
		ctrlList.append(lwctrl)
	else:
		ctrlList = [lwctrl]

	return ctrlList

def delCtrlKey(NS,startFrame,endFrame):
	ctrlList = mc.ls('{}:*_Ctrl'.format(NS))
	keyCtrlList = []
	for i in ctrlList:
		if mc.keyframe(i,q=1,timeChange=1):
			keyCtrlList.append(i)
	print keyCtrlList
	mc.cutKey(keyCtrlList,clear=1,time = (startFrame,endFrame))

def setPreRoll(NS,TPoseFrame,simPreRollFrame,AnimpreRollFrame):
	mc.select(d=1)
	#wholeList = mc.ls('{}:*_Ctrl'.format(NS))
	#gmblList = mc.ls('{}:*Gmbl*_Ctrl*'.format(NS))
	#ctrlList = list(set(wholeList)-set(gmblList))
	ctrlList = mc.ls('{}:*_Ctrl'.format(NS))
	conCtrl = mc.ls('{}:*Cons_Ctrl*'.format(NS))
	ctrlList = list(set(ctrlList)-set(conCtrl))
	lwCtrlList = []
	switchCtrlList = []
	keyCtrlList = []
	userAttrCtrlList = []

	for i in ctrlList:
		if mc.keyframe(i,q=1,timeChange=1):
			keyCtrlList.append(i)

	for i in ctrlList:
		if (mc.objExists(i+'.localWorld')):
			lwCtrlList.append(i)
			
	for i in ctrlList:
		if (mc.objExists(i+'.fkIk')):
			switchCtrlList.append(i)

	#for i in ctrlList:
		#if mc.listAttr(i,ud=1,k=1):
			#userAttrCtrlList.append(i)

	#allCtrl = list(set(keyCtrlList)+set(lwCtrlList)+set(switchCtrlList))
	mc.select(ctrlList)
	mc.currentTime(AnimpreRollFrame)
	print ctrlList
	mc.setKeyframe(ctrlList,s=0,itt='flat')
	#mc.setKeyframe(switchCtrlList,s=0,t= AnimpreRollFrame,itt='flat')
	#mc.setKeyframe(userAttrCtrlList,s=0,t= AnimpreRollFrame,itt='flat')
	#mc.setKeyframe(lwCtrlList,s=0,t=AnimpreRollFrame,itt='flat')

	#keyCtrlList2 = []
	#for i in ctrlList:
		#if mc.keyframe(i,q=1,timeChange=1):
			#keyCtrlList2.append(i)
	copyCtrlToFrame(AnimpreRollFrame , (AnimpreRollFrame-1), ctrlList)
	mc.currentTime(AnimpreRollFrame - 1)
	mc.setKeyframe(switchCtrlList,s=0,t=(AnimpreRollFrame - 1),itt='flat',ott='flat',i=1)
	mc.setKeyframe(lwCtrlList,s=0,t= (AnimpreRollFrame - 1),itt='flat',ott='flat',i=1)
	convertIK(switchCtrlList)

	for lwCtrl in lwCtrlList:
		convertToLocal(lwCtrl)

	#for userCtrl in userAttrCtrlList:
		#attrList = mc.listAttr(userCtrl,u=1,k=1)
		#for attr in attrList:
			#if ('scale' not in attr) and ('Scale' not in attr) and not (mc.getAttr('{}.{}'.format(userCtrl,attr),l=1)):
				#mc.setAttr('{}.{}'.format(userCtrl,attr),0)
				#mc.setKeyframe('{}.{}'.format(userCtrl,attr),t=(AnimpreRollFrame-1),itt='flat',ott='flat')
	

	#keyCtrlList3 = []
	#for i in ctrlList:
		#if mc.keyframe(i,q=1,timeChange=1):
			#keyCtrlList3.append(i)	

	copyCtrlToFrame((AnimpreRollFrame-1),simPreRollFrame,ctrlList)
	
	#keyCtrlList4 = []
	#for i in ctrlList:
		#if mc.keyframe(i,q=1,timeChange=1):
			#keyCtrlList4.append(i)

	allMove = '{}:AllMover_Ctrl'.format(NS)
	offSet = '{}:Offset_Ctrl'.format(NS)
	#root = '{}:Root_Ctrl'
	try:
		ctrlList.remove(allMove)
		ctrlList.remove(offSet)
	except ValueError:
		pass
	#keyCtrlList2.append()
	createTPose(ctrlList,TPoseFrame)


