import maya.cmds as mc


def swapLW(ctrl):
	checkCase = mc.getAttr('{}.localWorld'.format(ctrl))
	grp = mc.createNode('transform',n='{}_saveTransform'.format(ctrl))
	mc.delete(mc.parentConstraint(ctrl,grp,mo=0))
	mat = mc.xform(grp,q=1,ws=1,m=1)
	if checkCase ==1:
		mc.setAttr('{}.localWorld'.format(ctrl),0)
		mc.xform(ctrl,ws=1,m=mat)
	elif checkCase ==0:
		mc.setAttr('{}.localWorld'.format(ctrl),1)
		mc.xform(ctrl,ws=1,m=mat)	

def swapToLocal(ctrl):
	checkCase = mc.getAttr('{}.localWorld'.format(ctrl))
	grp = mc.createNode('transform',n='{}_saveTransform'.format(ctrl))
	mc.delete(mc.parentConstraint(ctrl,grp,mo=0))
	mat = mc.xform(grp,q=1,ws=1,m=1)
	if checkCase ==1:
		mc.setAttr('{}.localWorld'.format(ctrl),0)
		mc.xform(ctrl,ws=1,m=mat)
	mc.delete(grp)

def swapToWorld(ctrl):
	checkCase = mc.getAttr('{}.localWorld'.format(ctrl))
	grp = mc.createNode('transform',n='{}_saveTransform'.format(ctrl))
	mc.delete(mc.parentConstraint(ctrl,grp,mo=0))
	mat = mc.xform(grp,q=1,ws=1,m=1)
	if checkCase ==0:
		mc.setAttr('{}.localWorld'.format(ctrl),1)
		mc.xform(ctrl,ws=1,m=mat)
	mc.delete(grp)
