import pymel.core as pm ;

####################
####################
''' general utilities '''
####################
####################

colorDict = { } ;
colorDict [ 'green' ]       = ( 0.498039 , 1 , 0 ) ;
colorDict [ 'yellow' ]      = ( 1 , 1 , 0 ) ;
colorDict [ 'red' ]         = ( 1 , 0 , 0 ) ;
colorDict [ 'pink' ]        = ( 1 , 0.411765 , 0.705882 ) ;
colorDict [ 'purple' ]      = ( 1 , 0 , 1 ) ;
colorDict [ 'lightBlue' ]   = ( 0 , 1 , 1 ) ;
colorDict [ 'paleGreen' ]   = ( 0.564706 , 0.933333 , 0.564706 ) ;
colorDict [ 'paleBlue' ]    = ( 0.529412 , 0.807843 , 0.980392 ) ;
colorDict [ 'white' ]       = ( 1 , 1 , 1 ) ;
colorDict [ 'paleRed' ]     = ( 0.803922 , 0.360784 , 0.360784 ) ;
colorDict [ 'palePink' ]    = ( 0.941176 , 0.501961 , 0.501961 ) ;
colorDict [ 'paleYellow' ]  = ( 1 , 0.870588 , 0.678431 ) ;
colorDict [ 'palePurple' ]  = ( 0.866667 , 0.627451 , 0.866667 ) ;

def changeOutlinerColor ( target , color , *args ) :

    target = pm.general.PyNode ( target ) ;
    target.useOutlinerColor.set ( 1 ) ;        
    target.outlinerColor.set ( color ) ;    

def makeName_cmd ( target = '' , stripLastElm = True , *args ) :

    targetSplit_list = target.split ( '_' ) ;
    
    if stripLastElm == True :
        targetSplit_list.remove ( targetSplit_list[-1] ) ;
    else : pass ;

    name = targetSplit_list[0] ;
    
    for each in targetSplit_list[1:] :
        name += each.capitalize ( ) ;
    return name ;

class Clean ( object ) :

    def __init__ ( self , node ) :
        self.node = node ;

    def freezeTransform ( self ) :
        pm.makeIdentity ( self.node , apply = True ) ;

    def deleteHistory ( self ) :
        pm.delete ( self.node , ch = True ) ;

    def centerPivot ( self ) :
        pm.xform ( self.node , cp = True ) ;

    def deleteRebuiltShape ( self ) :

        shapeList = pm.listRelatives ( self.node , shapes = True ) ;
            
        for shape in shapeList :            
            if 'rebuilt' in str ( shape ) :
                pm.delete ( shape ) ;
            else : pass ;

def cleanCurve_cmd ( target = '' , *args ) :
    target = Clean ( target ) ;
    target.freezeTransform ( ) ;
    target.deleteHistory ( ) ;
    target.centerPivot ( ) ;
    target.deleteRebuiltShape ( ) ;

####################
####################
''' script commands '''
####################
####################

def softCluster_cmd ( *args ) :
    import SoftClusterEX ;
    reload(SoftClusterEX) ;
    SoftClusterEX.launch() ;

def findCurvePoint_cmd ( master = '' , slave = '' , *args ) :

    master_pos = pm.xform ( master , q = True , rp = True , ws = True ) ;
    slave_pos = pm.xform ( slave , q = True , rp = True , ws = True ) ;

    inbetween1_pos = [] ;
    inbetween2_pos = [] ;

    for i in range ( 0 , 3 ) :
        inbetween1_pos.append ( master_pos[i] + (slave_pos[i]-master_pos[i])*0.33 ) ; 
        inbetween2_pos.append ( master_pos[i] + (slave_pos[i]-master_pos[i])*0.66 ) ;
        
    return master_pos , inbetween1_pos , inbetween2_pos , slave_pos ; 
    
def operate_cmd ( *args ) :

    selection = pm.ls ( sl = True ) ;

    master = selection = pm.ls ( sl = True ) [-1] ;
    slave_list = selection = pm.ls ( sl = True ) [0:-1] ;

    print master ;
    print slave_list ;

    ##################
    ''' SIM_GRP '''
    ##################
    if pm.objExists ( 'SIM_GRP' ) != True :
        SIM_GRP = pm.group ( em = True , w = True , n = 'SIM_GRP' ) ;
        changeOutlinerColor ( target = SIM_GRP , color = colorDict [ 'lightBlue' ] ) ;
    else :
        SIM_GRP = pm.general.PyNode ( 'SIM_GRP' ) ;

    ##################
    ''' pMuscle_GRP '''
    ##################
    if pm.objExists ( 'pMuscle_GRP' ) != True :
        pMuscle_GRP = pm.group ( em = True , w = True , n = 'pMuscle_GRP' ) ;
    else :
        pMuscle_GRP = pm.general.PyNode ( 'pMuscle_GRP' ) ;

    pMuscleGRPParent = pm.listRelatives ( pMuscle_GRP ) ;

    if pMuscleGRPParent != [] :
        pMuscleGRPParent = pMuscleGRPParent[0] ;
    else : pass ;

    if pMuscleGRPParent != SIM_GRP :
        pm.parent ( pMuscle_GRP , SIM_GRP ) ;
    else : pass ;

    ##################
    ''' CRV_GRP '''
    ##################
    if pm.objExists ( 'CRV_GRP' ) != True :
        CRV_GRP = pm.group ( em = True , w = True , n = 'CRV_GRP' ) ;
    else :
        CRV_GRP = pm.general.PyNode ( 'CRV_GRP' ) ;

    CRVGRPParent = pm.listRelatives ( CRV_GRP ) ;

    if CRVGRPParent != [] :
        CRVGRPParent = CRVGRPParent[0] ;
    else : pass ;

    if CRVGRPParent != pMuscle_GRP :
        pm.parent ( CRV_GRP , pMuscle_GRP ) ;
    else : pass ;

    ##################
    ''' IKH_GRP '''
    ##################
    if pm.objExists ( 'IKH_GRP' ) != True :
        IKH_GRP = pm.group ( em = True , w = True , n = 'IKH_GRP' ) ;
    else :
        IKH_GRP = pm.general.PyNode ( 'IKH_GRP' ) ;

    ##################
    ''' IKH_GRP organization '''
    ##################
    IKHGRPParent = pm.listRelatives ( IKH_GRP ) ;

    if IKHGRPParent != [] :
        IKHGRPParent = IKHGRPParent[0] ;
    else : pass ;

    if IKHGRPParent != pMuscle_GRP :
        pm.parent ( IKH_GRP , pMuscle_GRP ) ;
    else : pass ;

    ##################
    ''' MAIN OPERATION '''
    ##################
    if master == [] or slave_list == [] :
        pass ;
    
    else :

        curve_list = [] ;
        baseJnt_list = [] ;
        tipJnt_list = [] ;
        ORIGRP_list = [] ;
        slaveNm_list = [] ;

        for slave in slave_list :
            
            master_nm = makeName_cmd ( target = master ) ;
            slave_nm = makeName_cmd ( target = slave ) ;
            slaveNm_list.append ( slave_nm ) ;

            ORI_GRP = pm.group ( em = True , w = True , n = slave_nm + 'ORI_GRP' ) ;
            pm.parentConstraint ( master , ORI_GRP , mo = 0 , skipRotate = 'none' , skipTranslate = 'none' ) ;
            pm.scaleConstraint ( master , ORI_GRP , mo = 0 , skip = 'none' ) ;
            ORIGRP_list.append ( ORI_GRP ) ;

            ##################
            ''' positions '''
            ##################
            pos_list = findCurvePoint_cmd ( master = master , slave = slave ) ;
            # return master_pos , inbetween1_pos , inbetween2_pos , slave_pos ; 
            master_pos = pos_list [0] ;
            inbetween1_pos = pos_list [1] ;
            inbetween2_pos = pos_list [2] ;
            slave_pos = pos_list [3] ;

            ##################
            ''' base/tip joint '''
            ##################
            base_jnt = pm.createNode ( 'joint' , n = slave_nm + '_baseJnt' ) ;
            baseJnt_list.append ( base_jnt) ;

            tip_jnt = pm.createNode ( 'joint' , n = slave_nm + '_tipJnt' ) ;
            tipJnt_list.append ( tip_jnt ) ;

            pm.xform ( base_jnt , t = master_pos ) ;
            pm.xform ( tip_jnt , t = slave_pos ) ;
            pm.parent ( tip_jnt , base_jnt ) ;
            pm.parent ( base_jnt , ORI_GRP ) ;

            ''' connect '''
            pm.parentConstraint ( tip_jnt , slave , mo = 0 , skipRotate = 'none' , skipTranslate = 'none' ) ;
            pm.scaleConstraint ( tip_jnt , slave , mo = 0 , skip = 'none' ) ;
            pm.parent ( slave , ORI_GRP ) ;

            ##################
            ''' curves '''
            ##################
            curve = pm.curve ( d = 3 , p = [ ( master_pos ) , ( inbetween1_pos ) , ( inbetween2_pos ) , ( slave_pos ) ] , n = slave_nm + '_ORI' ) ;
            cleanCurve_cmd ( target = curve ) ;

            curve_list.append ( curve ) ;

        pm.select ( curve_list ) ;
        pm.mel.eval ( '''makeCurvesDynamic 2 { "1", "0", "1", "1", "0"};''' ) ;

        ##################
        ''' nucleus '''
        ##################
        nucleusParent = pm.listRelatives ( 'nucleus1' , parent = True ) ;

        if nucleusParent != [] :
            nucleusParent = nucleusParent[0] ;
        else : pass ;

        if nucleusParent != SIM_GRP :
                pm.parent ( 'nucleus1' , SIM_GRP ) ;
        else : pass ;

        ##################
        ''' hairSystem '''
        ##################

        if 'LFT' in str(slaveNm_list[0]) :
            hairSystemNm = slaveNm_list[0].split('LFT')[0]+'_hairSystem' ;
        elif 'RGT' in str(slaveNm_list[0]) :
            hairSystemNm = slaveNm_list[0].split('RGT')[0]+'_hairSystem' ;
        else :
            hairSystemNm = slaveNm_list[0] + '_hairSystem' ;

        hairSystem = pm.rename ( 'hairSystem1' , hairSystemNm ) ;
        hairSystemShape = pm.listRelatives ( hairSystem , shapes = True ) [0] ;

        hairSystemShape.active.set(0) ;
        hairSystemShape.startCurveAttract.set ( 0.65 ) ;
        hairSystemShape.attractionDamp.set ( 0.25 ) ;
        hairSystemShape.drag.set ( 0.1 ) ;
        hairSystemShape.damp.set ( 0.25 ) ;

        pm.parent ( hairSystem , SIM_GRP ) ;

        ##################
        ''' FOL '''
        ##################

        for i in range ( 0 , len ( slaveNm_list ) ) :
            FOL = pm.rename ( 'follicle%s'%(i+1) , slaveNm_list[i] + '_FOL' ) ;
            pm.parent ( FOL , ORIGRP_list[i] ) ;
        
            FOLShape = pm.listRelatives ( FOL , shapes = True ) ;
            FOLShape = pm.general.PyNode ( FOLShape [0] ) ;
            FOLShape.pointLock.set ( 1 ) ;
        
        pm.delete ( 'hairSystem1Follicles' ) ;

        ##################
        ''' CRV '''
        ##################

        CRV_list = [] ;

        for i in range ( 0 , len ( slaveNm_list ) ) :
            CRV = pm.rename ( 'curve%s'%(i+1) , slaveNm_list[i] + '_CRV' ) ;
            CRV_list.append ( CRV ) ;
            pm.parent ( CRV , CRV_GRP ) ;        
        pm.delete ( 'hairSystem1OutputCurves' ) ;

        for i in range ( 0 , len ( slaveNm_list ) ) :

            pm.ikHandle (
                startJoint  = baseJnt_list[i] ,
                endEffector = tipJnt_list[i] ,
                curve       = CRV_list[i] ,
                createCurve = False ,
                parentCurve = False ,
                name        = slaveNm_list[i] + '_IKH' ,
                solver      = 'ikSplineSolver'
                ) ;

            pm.rename ( 'effector1' , slaveNm_list[i] + '_EFF' ) ;

            IKH = pm.general.PyNode ( slaveNm_list[i] + '_IKH' ) ;
            pm.parent ( IKH , IKH_GRP ) ;

        ##################
        ''' ELSE organization '''
        ##################

        for i in range ( 0 , len ( slaveNm_list ) ) :

            pm.parent ( ORIGRP_list[i] , pMuscle_GRP ) ;

####################
####################
''' UI '''
####################
####################

##################
''' help '''
##################

help_text = '''softCluster joint suffix = _scJnt

1. select soft cluster joint(s)
2. select parent joint
3. click 'operate'

author note: this script rely heavily on default name when maya create things thus please make sure that items in the scene/outliner is properly named before running this script'''

##################
''' width '''
##################

width = 250.00 ;
title = 'pMuscle setup'

##################
''' UI commands '''
##################

def frameLayout_cc ( *args ) :
    pm.window ( 'pMuscle_UI', e = True , h = 10 ) ;

##################
''' main UI '''
##################

def run ( *args ) :
    
    # check if window exists
    if pm.window ( 'pMuscle_UI' , exists = True ) :
        pm.deleteUI ( 'pMuscle_UI' ) ;
    else : pass ;
   
    window = pm.window ( 'pMuscle_UI', title = title , mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    pm.window ( 'pMuscle_UI' , e = True , w = width ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            with pm.frameLayout ( label = 'help' , collapsable = True , collapse = False , cc = frameLayout_cc , bgc = ( 0 , 0 , 0.2 ) ) :

                textScroll = pm.scrollField ( wordWrap = True , editable = True , text = help_text , h = 200 ) ;

            pm.text ( label = '' ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                pm.text ( label = '' ) ;
                pm.button ( label = 'softCluster' , w = width/2 , c = softCluster_cmd , bgc = ( 1 , 0.8 , 0 ) ) ;
            
            pm.text ( label = '' ) ;

            pm.button ( label = 'operate' , w = width , c = operate_cmd , bgc = ( 0 , 1 , 1 ) ) ;

    window.show () ;

run ( ) ;