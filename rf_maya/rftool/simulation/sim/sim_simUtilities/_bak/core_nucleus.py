import pymel.core as pm ;

class NucleusNode ( object ) :
    
    def __init__ ( self ) :
        self.nucleus = pm.ls ( type = 'nucleus' ) ;
        self.nucleus.sort() ;
    
    def __str__ ( self ) :
        return str(self.nucleus) ;
   
    def __repr__ ( self ) :
        return str(self.nucleus) ;

    def getEnableState ( self ) :
        self.enable = [] ;
        for each in self.nucleus :
            self.enable.append ( each.enable.get() ) ;
        #print self.enable ;
    
    def setEnable ( self , state ) :
        for each in self.nucleus :
            each.enable.set( state ) ;
               
    def reset ( self ) :
        for i in range ( 0 , len(self.nucleus) ) :
            self.nucleus[i].enable.set( self.enable[i] ) ;
    
    def pmDir ( self ) :
        for each in dir ( pm.general.PyNode ( self.nucleus[0] ) ) :
            print each ;
    
    def dir ( self ) :
        for each in dir ( self ) :
            print each ;