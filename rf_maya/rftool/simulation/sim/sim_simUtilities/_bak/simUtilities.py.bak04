import pymel.core as pm ;
import os , ast , re ;

######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################
''' simulate '''
######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################

currentProj = pm.workspace ( q = True , rootDirectory = True ) ;

def save ( *args ) :
    timeRangeTxt = open ( currentProj + 'data/timerange.txt' , 'w+' ) ;

    preRoll = pm.intField ( 'preRollIF' ,  q = True , value = True ) ;
    start = pm.intField ( 'startIF' ,  q = True , value = True ) ;
    end = pm.intField ( 'endIF' ,  q = True , value = True ) ;
    postRoll = pm.intField ( 'postRollIF' ,  q = True , value = True ) ;
    
    timeRangeTxt.write ( "{ 'pre roll': %d , 'start': %d , 'end' : %d , 'post roll' : %d }" % ( preRoll, start , end , postRoll ) ) ;
    timeRangeTxt.close ( ) ;

def initializeTimeRange ( *args ) :
    if os.path.exists ( currentProj + 'data/timerange.txt' ) == True :
        timeRangeTxt = open ( currentProj + 'data/timerange.txt' , 'r+' ) ;
        timeRange = timeRangeTxt.read ( ) ;
        timeRange = ast.literal_eval ( timeRange ) ;
        timeRangeTxt.close ( ) ;
        
        pm.intField ( 'preRollIF' ,  e = True , value = timeRange['pre roll'] ) ;
        pm.intField ( 'startIF' ,  e = True , value = timeRange['start'] ) ;
        pm.intField ( 'endIF' ,  e = True , value = timeRange['end'] ) ;
        pm.intField ( 'postRollIF' ,  e = True , value = timeRange['post roll'] ) ;

        pm.intField ( 'playblastStart' , e = True , value = timeRange['start'] ) ;
        pm.intField ( 'playblastEnd' , e = True , value = timeRange['end'] ) ;

    else : pass ;

def customPlayblast ( start , end , resW , resH , *args ) :

    if resW > 1920 or resH > 1080 :
        percent = 50 ;
    else :
        percent = 100 ;

    pm.playblast ( startTime = start , endTime = end , format = 'image' ,
        sequenceTime = 0 , clearCache = 1 , viewer = True , showOrnaments = True ,
        offScreen = True , fp = 4 , percent = percent , compression =  "maya" , quality = 100 ,
        widthHeight = [ resW , resH ] ) ;

def viewPlayblast ( *args ) :

    resW = pm.getAttr ( "defaultResolution.width" ) ;
    resH = pm.getAttr ( "defaultResolution.height" ) ;

    start = pm.intField ( 'playblastStart' ,  q = True , value = True ) ;
    end = pm.intField ( 'playblastEnd' ,  q = True , value = True ) ;

    customPlayblast ( start , end , resW , resH ) ;

def setTimeRange ( *args ) :
    currentStart = pm.playbackOptions ( q = True , min = True ) ;
    currentEnd = pm.playbackOptions ( q = True , max = True ) ;

    preRoll = pm.intField ( 'preRollIF' ,  q = True , value = True ) ;
    start = pm.intField ( 'startIF' ,  q = True , value = True ) ;
    end = pm.intField ( 'endIF' ,  q = True , value = True ) ;
    postRoll = pm.intField ( 'postRollIF' ,  q = True , value = True ) ;

    if currentStart == preRoll and currentEnd == postRoll :
        pm.playbackOptions ( min = start , max = end ) ;

    elif currentStart == start and currentEnd == end :
        pm.playbackOptions ( min = preRoll , max = postRoll ) ;

    else :
        pm.playbackOptions ( min = preRoll , max = postRoll ) ;

    updateSetTimeRangeBtnClr ( ) ;
    setStartBsh ( start = preRoll ) ;

def getTimeRange ( *args ) :

    selection = pm.ls ( sl = True ) ;
    if len ( selection ) != 1 :
        # print ( 'more than 1 object selected' ) ;
        pass ;
    else :
        selection = selection [0] ;
        selection = pm.general.PyNode ( selection ) ;
        
        start = selection.start.get ( ) ;
        end = selection.end.get ( ) ;

        preroll = start - 30 ;
        postRoll = end + 10 ;

        pm.intField ( 'preRollIF' ,  e = True , value = preroll ) ;
        pm.intField ( 'startIF' ,  e = True , value = start ) ;
        pm.intField ( 'endIF' ,  e = True , value = end ) ;
        pm.intField ( 'postRollIF' ,  e = True , value = postRoll ) ;

        pm.intField ( 'playblastStart' , e = True , value = start ) ;
        pm.intField ( 'playblastEnd' , e = True , value = end ) ;

        updateSetTimeRangeBtnClr ( ) ;

def checkVersion ( directory = '' , name = '' , incrementVersion = True , incrementSubVersion = False , *args ) :
    
    if os.path.exists ( directory ) == False :
        os.makedirs ( directory ) ;
    else : pass ;

    allFile = os.listdir ( directory ) ;
    versionList = [] ;
    
    for each in allFile :
        if each.split('_')[0] == name :
            versionList.append ( each ) ;
        else : pass ;
    
    if len ( versionList ) == 0 :
        version = '001'
        subVersion = '001'

    else :
        versionList.sort ( ) ;
        latestVersion = versionList [-1] ;
        latestVersion = latestVersion.split ( '_' ) ;
        version = int ( re.findall ( '[0-9]{3}' ,  latestVersion [ -2 ] ) [0] ) ;
        subVersion = int ( re.findall ( '[0-9]{3}' , latestVersion [ -1 ] ) [0] ) ;

        if incrementSubVersion == True :
            subVersion += 1 ;
            subVersion = str(subVersion).zfill(3) ;
        else :
            subVersion = str(subVersion).zfill(3) ;
            
        if incrementVersion == True :
            version += 1 ;
            version = str(version).zfill ( 3 ) ;
            subVersion = '001' ;
                # overwrite increment subversion
        else :
            version = str(version).zfill ( 3 ) ;

    return str(version) + '_' + str(subVersion) ;

def createName ( *args ) :
    selection = pm.ls ( sl = True ) ;
    globalName = '' ;
    for each in selection :
        name = each.split ( '_' ) [0] ;
        globalName += str(name) ;
    return globalName ;

def simulate ( type = '' ) :
    
    name = createName ( )  ;

    currentProject = pm.workspace ( q = True , fullName = True ) ;

    dynCacheDirectory = currentProject + '/data/DYN/%s' % name ;

    if os.path.exists ( dynCacheDirectory ) == False :
        os.makedirs ( dynCacheDirectory ) ;
    else : pass ;

    if type == 'replace' :
        incrementVersion = False ;
        incrementSubVersion = False ;
        
        try : pm.mel.eval ( 'deleteCacheFile 2 { "delete", "" } ;' ) ;
        except : pass ;
        else : pass ;

    else :
        if type == 'version' :
            incrementVersion = True ;
            incrementSubVersion = False ;
            
        elif type == 'subversion' :
            incrementVersion = False ;
            incrementSubVersion = True ;
                
        try : pm.mel.eval ( 'deleteCacheFile 2 { "keep", "" } ;' ) ;
        except : pass ;
        else : pass ;
    
    version = checkVersion ( directory = dynCacheDirectory , name = name ,
        incrementVersion = incrementVersion , incrementSubVersion = incrementSubVersion ) ;
    cacheName = name +  '_DYN_v' + version ;

    pm.mel.eval ( 'doCreateNclothCache 5 { "2", "1", "10", "OneFile", "1", "%s","0","%s","0", "add", "0", "1", "1","0","1","mcx" } ;' % ( dynCacheDirectory , cacheName ) ) ;

def simulateBtn ( *args ) :
    type = pm.optionMenu( 'simTypeOpt' , q = True , v = True ) ;
    simulate ( type = type ) ;

    if pm.checkBox ( 'playBlastCB' , q = True , value = True ) == True :
        viewPlayblast ( ) ;

def updateSetTimeRangeBtnClr ( *args ) :

    preRoll = pm.intField ( 'preRollIF' ,  q = True , value = True ) ;
    postRoll = pm.intField ( 'postRollIF' ,  q = True , value = True ) ;

    start = pm.intField ( 'startIF' ,  q = True , value = True ) ;
    end = pm.intField ( 'endIF' ,  q = True , value = True ) ;

    currentStart = pm.playbackOptions ( q = True , min = True ) ;
    currentEnd = pm.playbackOptions ( q = True , max = True ) ;

    # print ( preRoll , postRoll , start , end ) ;

    if ( currentStart == start ) and ( currentEnd == end ) :
        pm.button ( 'setTimeRangeBtn' , e = True , bgc = ( 1 , 0.388235 , 0.278431 ) ) ;

    elif ( currentStart == preRoll ) and ( currentEnd == postRoll ) :
        pm.button ( 'setTimeRangeBtn' , e = True , bgc = ( 0 , 1 , 1 ) ) ;

    else : pass ;


def setStartBsh ( start , *args ) :
    
    bsh = 'CIN_COL_BSH' ;
    
    if pm.objExists ( bsh ) == True :
        key = pm.listAttr ( bsh + '.w' , m = True ) ;
        pm.cutKey ( bsh , at = key , option = 'keys' ) ;
        pm.setKeyframe ( bsh , at = key , t = start , v = 0 ) ;
        pm.setKeyframe ( bsh , at = key , t = start + 10 , v = 1 ) ;
    else : pass ;

    nucleus = pm.ls ( type = 'nucleus' ) ;

    if len ( nucleus ) == 0 :
        pass ;
    else :
        for each in nucleus :
            pm.setAttr ( each + '.' + 'startFrame' , start ) ;

    #pm.currentTime ( start ) ;

######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################
''' cache out '''
######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################

def createBwdFwdGrp ( *args ) :
    
    selection = pm.ls ( sl = True ) ;
    
    if len ( selection ) != 1 :
        pass ;
    
    else :    

        selection = selection [0] ;
        selection = pm.general.PyNode ( selection ) ;
        
        bwdGrp = pm.group ( em = True , n = '%s_BWD' % selection ) ;
        fwdGrp = pm.group ( em = True , n = '%s_FWD' % selection ) ;
        
        bwdGrp.tx.set ( selection.tx.get ( ) * -1 ) ;
        bwdGrp.ty.set ( selection.ty.get ( ) * -1 ) ;
        bwdGrp.tz.set ( selection.tz.get ( ) * -1 ) ;
        bwdGrp.t.lock ( ) ;
        bwdGrp.r.lock ( ) ;
        bwdGrp.s.lock ( ) ;
        
        fwdGrp.t.set ( selection.t.get ( ) ) ;
        fwdGrp.t.lock ( ) ;
        fwdGrp.r.lock ( ) ;
        fwdGrp.s.lock ( ) ;

## cache out ##
#doCreateGeometryCache 6 { "2", "1", "10", "OneFile", "1", "D:/TwoHeroes/film001/q0310/s0060/hanumanCasual/cache/DYN","0","Pants_DYN_v001_001","0", "export", "0", "1", "1","0","1","mcx","1" } ;
#// Result: D:/TwoHeroes/film001/q0310/s0060/hanumanCasual/cache/DYN//Pants_DYN_v001_001.xml // 


######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################
''' disable '''
######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################

def updateNucleusUI ( *args ) :
    nucleus = pm.ls ( type = 'nucleus' ) ;
    allNucleus = pm.textScrollList ( 'nucleusTxtScrll' , q = True , allItems = True ) ;

    for each in allNucleus :
        if pm.objExists ( each ) == False :
            pm.textScrollList ( 'nucleusTxtScrll' , e = True , removeItem = each ) ;
        else :  pass ;

    for each in nucleus :
        if each not in allNucleus :
            pm.textScrollList ( 'nucleusTxtScrll' , e = True , append = each ) ;
        else : pass ;

    selectedNucleus = pm.textScrollList ( 'nucleusTxtScrll' , q = True , selectItem = True ) ;

    if selectedNucleus != [] :
        if len(selectedNucleus) == 1 :
                if pm.getAttr ( '%s.enable' % selectedNucleus[0] ) == 0 :
                    pm.button ( 'neBtn' , e = True , nbg = False ) ;
                    pm.button ( 'ndBtn' , e = True , nbg = True ) ; 
                elif pm.getAttr ( '%s.enable' % selectedNucleus[0] ) == 1 :
                    pm.button ( 'neBtn' , e = True , nbg = True ) ;
                    pm.button ( 'ndBtn' , e = True , nbg = False ) ;    
        else :
            pm.button ( 'neBtn' , e = True , nbg = False ) ;
            pm.button ( 'ndBtn' , e = True , nbg = False ) ;
    else : pass ;

def enableNucleus ( *args ) :
    selectedNucleus = pm.textScrollList ( 'nucleusTxtScrll' , q = True , selectItem = True ) ;

    if selectedNucleus != [] :  
        for each in selectedNucleus :
            pm.setAttr ( each + '.enable' , 1 ) ;
        updateNucleusUI ( ) ;
    else : pass ;

def disableNucleus ( *args ) :
    selectedNucleus = pm.textScrollList ( 'nucleusTxtScrll' , q = True , selectItem = True ) ;

    if selectedNucleus != [] :  
        for each in selectedNucleus :
            pm.setAttr ( each + '.enable' , 0 ) ;
        updateNucleusUI ( ) ;
    else : pass ;

def enableDisableNCloth ( mode , *arg ) :
    selection = pm.ls ( sl = True ) ;

    for each in selection :
        eachShape = pm.listRelatives ( each , shapes = True ) [0] ;
        if pm.objectType ( eachShape ) == 'mesh' :
            nClothShape = pm.general.PyNode ( pm.listConnections ( eachShape , type = 'nCloth' )[0] ) ;
            nClothShape.isDynamic.set(mode) ;

        elif pm.objectType ( eachShape ) == 'nCloth' :
            nClothShape = pm.general.PyNode ( eachShape ) ;
            nClothShape.isDynamic.set(mode) ;

def enableNCloth ( *arg) :
    enableDisableNCloth ( 1 ) ;

def disableNCloth ( *arg) :
    enableDisableNCloth ( 0 ) ;

def enableDisableNHair ( mode , *args ) :
    # 0 = disable
    # 3 = enable

    selection = pm.ls ( sl = True ) ;

    for each in selection :
        eachShape = pm.general.PyNode ( pm.listRelatives ( each , shapes = True ) [0] ) ;
        eachShape.simulationMethod.set ( mode ) ;

def enableNHair ( *args ) :
    enableDisableNHair ( mode = 3 ) ;

def disableNHair ( *args ) :
    enableDisableNHair ( mode = 0 ) ;

def enableCycle ( *args ) :
    pm.cycleCheck ( e = 1 ) ;
    updateCycleButton ( ) ;

def disableCycle ( *args ) :
    pm.cycleCheck ( e = 0 ) ;
    updateCycleButton ( ) ;

def updateCycleButton ( *args ) :
    cc = pm.cycleCheck ( q = True , e = True ) ;
    
    if cc == True :
        pm.button ( 'eccBtn' , e = True , nbg = True ) ;
        pm.button ( 'dccBtn' , e = True , nbg = False ) ;   
    else :
        pm.button ( 'eccBtn' , e = True , nbg = False ) ;
        pm.button ( 'dccBtn' , e = True , nbg = True ) ;

######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################
''' UI '''
######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################

def simUtilities ( *args ) :

    width = 300 ;

    if pm.window ( 'simUtilities' , exists = True ) :
        pm.deleteUI ( 'simUtilities' ) ;
    else : pass ;

    window = pm.window ( 'simUtilities', title = "Simulation Utilities" , w = width , 
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;

    pm.window ( 'simUtilities', e = True , w = width , h = 10 ) ;
    
    with window :
    
        mainLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] ) ;
        with mainLayout :

            ######

            nucleusTxtScrll = pm.textScrollList ( 'nucleusTxtScrll' , w = width , h = 60 , ams = True , sc = updateNucleusUI ) ;
            
            nucleusLayout = pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width / 2.0 ) , ( 2 , width / 2.0 ) ] ) ;
            with nucleusLayout :
                pm.button ( 'neBtn' , label = 'enable nucleus' ,  c  = enableNucleus , w = width / 2.0 ) ;
                pm.button ( 'ndBtn' , label = 'disable nucleus' , c  = disableNucleus , w = width / 2.0 ) ;
                updateNucleusUI ( ) ;
            
            pm.separator ( vis = 0 ) ;

            cycleCheckLayout = pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width / 2.0 ) , ( 2 , width / 2.0 ) ] ) ;
            with cycleCheckLayout :
                pm.button ( 'eccBtn' , label = 'enable cycle check' ,  c  = enableCycle , w = width / 2.0 ) ;
                pm.button ( 'dccBtn' , label = 'disable cycle check' , c  = disableCycle , w = width / 2.0 ) ;
                updateCycleButton ( ) ;

            pm.separator ( vis = 0 ) ;

            enableLayout = pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width / 2.0 ) , ( 2 , width / 2.0 ) ] ) ;
            with enableLayout :
                pm.button ( 'nceBtn' , label = 'enable nCloth' ,  c  = enableNCloth , w = width / 2.0 , bgc = ( 0 , 1 , 1 ) ) ;
                pm.button ( 'ncdBtn' , label = 'disable nCloth' , c  = disableNCloth , w = width / 2.0 , bgc = ( 0 , 0.74902 , 1 ) ) ;

                pm.button ( 'nheBtn' , label = 'enable nHair' ,  c  = enableNHair , w = width / 2.0 , bgc = ( 0.498039 , 1 , 0 ) ) ;
                pm.button ( 'nhdBtn' , label = 'disable nHair' , c  = disableNHair , w = width / 2.0 , bgc = ( 0.196078 , 0.803922 , 0.196078 ) ) ;

            ######

            pm.separator ( vis = True , h = 10 ) ;

            timeRangeLayout = pm.rowColumnLayout ( w = width , nc = 5 , columnWidth = [
                ( 1 , width / 5.0 ), ( 2 , width / 5.0 ) , ( 3 , width / 5.0 ) , ( 4 , width / 5.0 ) , ( 5 , width / 5.0 ) ] ) ;
            with timeRangeLayout :    

                pm.intField ( 'preRollIF' ) ;
                pm.intField ( 'startIF' ) ;
                pm.button ( label = 'save' , c = save , bgc = ( 0.564706 , 0.933333 , 0.564706 ) ) ;
                pm.intField ( 'endIF' ) ;
                pm.intField ( 'postRollIF' ) ;
                pm.text ( label = 'pre roll' ) ;
                pm.text ( label = 'start' ) ;
                pm.text ( label = '' ) ;
                pm.text ( label = 'end' ) ;
                pm.text ( label = 'post roll' ) ;
    
            setTimeLayout = pm.rowColumnLayout ( w = width , nc = 3 , columnWidth = [ ( 1 , width * 0.05 ) , ( 2 , width * 0.9 ) , ( 3 , width * 0.05 ) ] ) ;
            with setTimeLayout :

                pm.text ( label = '' ) ;

                setTimeBtnLayout = pm.rowColumnLayout ( w = width * 0.9 , nc = 1 , columnWidth = [ ( 1 , width * 0.9 ) ] ) ;
                with setTimeBtnLayout :
                    pm.button ( label = 'get time range' , c = getTimeRange , bgc = ( 1 , 0.843137 , 0 ) ) ;
                    pm.button ( 'setTimeRangeBtn' , label = 'set time range' , c = setTimeRange ) ;
                    # set time range to sim --> color change 

                pm.text ( label = '' ) ;

            pm.separator ( h = 5 ) ;
            

            tab = pm.tabLayout ( 'tabLayout' ) ;
            with tab :

                simLayout = pm.rowColumnLayout ( w = width  , nc = 1 , cw = [ ( 1 , width ) ] ) ;
                with simLayout :

                    simMainLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] ) ;
                    with simMainLayout :

                        pm.separator ( h = 5 , vis = False ) ;

                        simUtilLayout = pm.rowColumnLayout ( w = width , nc = 5 , columnWidth =
                            [ ( 1 , width * 0.05 ), ( 2 , width * 0.5 ) , ( 3 , width * 0.05 ) , ( 4 , width * 0.35 ) , ( 5 , width * 0.05 ) ] ) ;
                        with simUtilLayout :

                            pm.text ( label = '' ) ;

                            simTypeOpt = pm.optionMenu( 'simTypeOpt' , label='sim type' ) ;
                            with simTypeOpt :
                                
                                pm.menuItem ( label='replace' ) ;
                                pm.menuItem ( label='version' ) ;
                                pm.menuItem ( label='subversion' ) ;

                            pm.text ( label = '' ) ;
                            pm.button ( label = 'simulate' , c = simulateBtn , bgc = ( 0 , 1 , 1 ) ) ;
                            pm.text ( label = '' ) ;

                            pm.text ( label = '' ) ; pm.text ( label = '' ) ; pm.text ( label = '' ) ;

                            playblastCBLayout = pm.rowColumnLayout ( w = width , nc = 4 , columnWidth = [
                                ( 1 , width * 0.075 ) , ( 2 , width * 0.05 ) , ( 3 , width * 0.2 ) , ( 4 , width * 0.075 ) ] ) ;
                                
                            with playblastCBLayout :
                
                                pm.text ( label = '' ) ;
                                pm.checkBox ( 'playBlastCB' , label = ' ' , value = True ) ;
                                pm.text ( label = 'playblast' , align = 'left' ) ;
                                pm.text ( label = '' ) ;
                            
                            pm.text ( label = '' ) ;

                    pm.separator ( h = 10 ) ;

                    playblastLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] ) ;
                    with playblastLayout :
                        
                        playblastTimeRangeLayout = pm.rowColumnLayout ( w = 300 , nc = 5 , columnWidth = [
                            ( 1 , width / 5.0 ), ( 2 , width / 5.0  ) , ( 3 , width / 5.0  ) , ( 4 , width / 5.0  ) , ( 5 , width / 5.0  ) ] ) ;
                        with playblastTimeRangeLayout :   
                        
                            pm.text ( label = '' ) ;
                            pm.intField ( 'playblastStart' ) ;
                            pm.text ( label = '' ) ;
                            pm.intField ( 'playblastEnd' ) ;
                            pm.text ( label = '' ) ;
                        
                            pm.text ( label = '' ) ;
                            pm.text ( label = 'start' ) ;
                            pm.text ( label = '' ) ;
                            pm.text ( label = 'end' ) ;
                            pm.text ( label = '' ) ;

                        playblastBtnLayout = pm.rowColumnLayout ( w = 300 , nc = 3 , columnWidth = [ ( 1 , width * 0.05 ) , ( 2 , width * 0.9 ) , ( 3 , width * 0.05 ) ] ) ;
                        with playblastBtnLayout :

                            pm.text ( label = '' ) ;
                            pm.button ( label = 'playblast' , c = viewPlayblast , bgc = ( 1 , 0.388235 , 0.278431 ) ) ;
                            pm.text ( label = '' ) ;

                    pm.separator ( h = 5 , vis = False ) ;

                cacheOutUtilLayout = pm.rowColumnLayout ( w = 300 , nc = 1 , columnWidth = [ ( 1 , width ) ] ) ;
                with cacheOutUtilLayout :

                    pm.text ( '' ) ;

    pm.tabLayout ( 'tabLayout' , edit = True , tabLabel = ( ( simLayout , 'simulation' ) , ( cacheOutUtilLayout , 'cache out' ) ) ) ;
    initializeTimeRange ( ) ;
    updateSetTimeRangeBtnClr ( ) ;
    window.show ( ) ;
    
#simUtilities ( ) ;
#initializeTimeRange ( ) ;