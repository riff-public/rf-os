import pymel.core as pm ;

class DuplicateNClothMesh ( object ) :
    
    def __init__ ( self ) :
        pass ;
    
    def __str__ ( self ) :
        pass ;
    
    def __repr__ ( self ) :
        pass ;
    
    def duplicateNClothMesh ( self , target ) :
        
        target = pm.general.PyNode ( target ) ;
        
        targetDup = pm.duplicate ( target ) [0] ;
        targetDup.rename ( target.nodeName() + '_Dup' ) ;
        
        targetDupShapes = targetDup.getShapes() ;
        toDelete_list = [] ;
        
        for shape in targetDupShapes :
            if 'outputCloth' in str(shape) :
                outputCloth = shape ;
            else :
                toDelete_list.append ( shape ) ;
         
        pm.delete ( toDelete_list ) ;
        outputCloth.rename ( targetDup.nodeName() + 'Shape' ) ;
        
    def duplicateSelection ( self ) :
    
        selection = pm.ls ( sl = True ) ;
    
        for each in selection :
            self.duplicateNClothMesh ( target = each ) ;

def run ( *args ) :
    dnm = DuplicateNClothMesh() ;
    dnm.duplicateSelection() ;