import pymel.core as pm ;

def blendShape ( driver , driven , *args ) :
    driverNm = driver.split ( '_' ) [0] ;
    drivenNm = driven.split ( '_' ) [0] ;
    driverSfx = driver.split ( '_' ) [1] ;
    drivenSfx = driven.split ( '_' ) [1] ;
    bsh = pm.blendShape ( driver , driven , automatic = True , origin = 'world' , n = driverNm + driverSfx + '2' + drivenSfx + '_BSH' ) [0] ; 
    pm.setAttr ( bsh + '.' + driver , 1 ) ;
    return bsh ;

def setBlendShape ( bsh , crv , blendStart , blendEnd , *args ) :
    
    numberOfCv = pm.getAttr ( crv + '.' + 'spans' ) + pm.getAttr ( crv + '.' + 'degree' ) ;
    
    cvStart = int ( numberOfCv / 100.0 * blendStart ) ;
    cvEnd = int ( numberOfCv / 100.0 * blendEnd ) ;
    
    blendAmount = 1 / float ( cvEnd - cvStart ) ;
     
    for i in range ( 0 , numberOfCv ) :
        pm.setAttr ( bsh + '.inputTarget[0].inputTargetGroup[0].targetWeights[' + str ( i ) + ']' , 0 ) ;
     
    for i in range ( 0 , cvStart ) :    
        pm.setAttr ( bsh + '.inputTarget[0].inputTargetGroup[0].targetWeights[' + str ( i ) + ']' , 1 ) ;
    
    value = 1 ;
    for i in range ( cvStart , cvEnd ) :
        pm.setAttr ( bsh + '.inputTarget[0].inputTargetGroup[0].targetWeights[' + str ( i ) + ']' , value ) ;
        value -= blendAmount ;

def blendShapeBtnCmd ( *args ) :

    blendStart = pm.intField ( 'blendStartInput' , q = True , v = True ) ;
    blendEnd = pm.intField ( 'blendEndInput' , q = True , v = True ) ;

    selection = pm.ls ( sl = True ) ;
    
    for each in selection :
        nm = each.split ( '_' ) [0] ;
        driver = nm + '_ORI' ;
        driven = each ;
        bsh = blendShape ( driver = driver , driven = driven ) ;
        
        setBlendShape ( bsh = bsh , crv = each , blendStart = blendStart , blendEnd = blendEnd ) ; 

def setBtnCmd ( *args ) :
    
    blendStart = pm.intField ( 'blendStartInput' , q = True , v = True ) ;
    blendEnd = pm.intField ( 'blendEndInput' , q = True , v = True ) ;
    
    selection = pm.ls ( sl = True ) ;
    
    for each in selection :
    
        nm = each.split ( '_' ) [0];
        
        bsh = nm + 'ORI2CRV_BSH' ;
                
        setBlendShape ( bsh = bsh , crv = each , blendStart = blendStart , blendEnd = blendEnd ) ;


def run ( *args ) :
    
    text = '''    
- this script will blendshape curve(s)
  under follicle to selected curve(s)
  
- curve under follicle should be named
  'curve_ORI'
  
- selected curve(s) (output curve(s))
  should be named 'curve_CRV'
'''

    width = 280 ;
    
    # check if window exists
    if pm.window ( 'crvBlendshapeUI' , exists = True ) :
        pm.deleteUI ( 'crvBlendshapeUI' ) ;
    else : pass ;
   
    window = pm.window ( 'crvBlendshapeUI', title = "curve blendShape utility" ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'crvBlendshapeUI' , e = True , w = width ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
        
            textScroll = pm.scrollField ( wordWrap = True , editable = False , text = text , w = width , h = 160 ) ;
            
            pm.separator ( vis = False , h = 10 ) ;
            
            userInputLayout = pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width / 2 ) , ( 2 , width / 2 ) ] ) ;
            with userInputLayout :
                
                pm.intField ( 'blendStartInput' , v = 20 ) ;
                pm.intField ( 'blendEndInput' , v = 75 ) ;
                pm.text ( label = 'blend start ( % )' ) ;
                pm.text ( label = 'blend end ( % )' ) ;
            
            pm.separator ( vis = False , h = 10 ) ;
            
            pm.button ( label = 'blendshape' , c = blendShapeBtnCmd , bgc = ( 0.8 , 0.6 , 0.1 ) ) ;
            
            pm.separator ( vis = False , h = 10 ) ;
            
            pm.button ( label = 'set' , c = setBtnCmd , bgc = (  0 , 0.75 , 1 ) ) ;
            
    window.show () ;

run ( ) ;