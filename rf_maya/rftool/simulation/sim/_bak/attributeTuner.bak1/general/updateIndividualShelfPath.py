import os ;

import library.general as gl ;
reload ( gl ) ;

def updateShelfPath () :

    # get path

    modulePath = gl.module_path () ;

    modulePath = modulePath + '/path.txt' ;

    pathTxt = open ( str ( modulePath ) , 'r' ) ;

    mp = str ( pathTxt.read () ) ;        
    pathShelf = '%sshelf' % mp ;
    
    pathTxt.close() ;

    for file in os.listdir ( pathShelf ) :
   
        if file.endswith ( '.py' ) :
        
            filePath = '%s/%s' % ( pathShelf , file ) ;
            # path to .py 
                
            pyRead = open ( filePath , 'r' ) ;
            pyReadContent = pyRead.read() ;
            pyRead.close () ;
        
            pyRead = open ( filePath , 'r' ) ;
            for line in pyRead :
            
                if line.startswith ( 'mp = ' ) :
                
                    oldPath = line ;   
            pyRead.close () ;
        
            newPathContent = pyReadContent.replace ( oldPath , "mp = '%s' ; \n" % mp ) ;
                        
            pyWrite = open ( ( filePath ) , 'w' ) ;
        
            pyWrite.write ( newPathContent ) ;

            pyWrite.close () ;