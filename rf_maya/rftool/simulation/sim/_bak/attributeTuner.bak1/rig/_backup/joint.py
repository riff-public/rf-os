import maya.cmds as mc ;

import rig.dic as dic ;
reload ( dic ) ;

import rig.controller as ctrl ;
reload ( ctrl ) ;

import rig.general as gen ;
reload ( gen ) ;

def copyJnt ( oriJnt = '' , newJntNm = '' , parent = '' ) :

    jntPos = mc.xform ( oriJnt , q = True , ws = True , rp = True ) ;         
    jntRot = mc.xform ( oriJnt , q = True , ws = True , ro = True ) ;
    jntRotOrder = mc.xform ( oriJnt , q = True , ws = True , roo = True ) ;

    if parent == '' :
        
        jnt = mc.createNode ( 'joint' , n = newJntNm ) ;
    
    else :
    
        jnt = mc.createNode ( 'joint' , n = newJntNm ) ;
        mc.parent ( jnt , parent ) ;

    mc.xform ( jnt , ws = True , t = jntPos , ro = jntRot , roo = jntRotOrder ) ;

    mc.makeIdentity ( jnt , apply = True , r = True ) ;
                
    return jnt ;

def insertPosJnt ( jnt = '' ) :
    
    # This function insert pos jnt between the jnt chain you selected. 
    # You only required to select the top of the chain joint. 
    
    if mc.listRelatives ( jnt , ad = True ) == None :
        mc.error ( "Your selected joint does not have a child" ) ;
    
    else :
        
        jntParent = mc.listRelatives ( jnt , p = True ) ; 
        
        oriJntList = mc.listRelatives ( jnt , ad = True ) ;
        oriJntList.append ( jnt ) ;
        oriJntList.reverse () ;
        
        for each in oriJntList :
            if '_jnt' not in each [ -4 : ] :
                mc.error ( "Check if you named your joints properly. This script requires '_jnt' as joint's name suffix. Problem at : %s " % each ) ;
            
            else :
                pass ;
                
        
            parent = mc.listRelatives ( each , p = True ) ;
            
            if parent != None :
                
                mc.parent ( each , w = True ) ;
                
            else :
                pass ;
                
        posJntList = [] ;
       
        for i in range ( 0 , ( len ( oriJntList ) -1 ) ) :
            
            jntNm = oriJntList [ i ].split ( '_jnt' ) ;
            
            posJntNm = '%sPos_jnt'% jntNm [ 0 ] ;
            
            posJnt = mc.duplicate ( oriJntList [i] , n = posJntNm ) ;
            
            posJntList.append ( posJnt [0] ) ; 
                        
        for i in range ( 0 , len ( posJntList ) ) :
        
             mc.parent ( oriJntList [i] , posJntList [i] ) ;
             
             if i != 0 :
                 
                 mc.parent ( posJntList [i] , oriJntList [i-1] ) ;
                 
        mc.parent ( oriJntList [ -1 ] , oriJntList [ -2 ] ) ;
        
            
        if jntParent != None :
            
            mc.parent ( posJntList [0] , jntParent ) ;    

def fkIkParentBlend ( jnt = '' ) :

    # This function create FK IK blend using parent constrain method. Scale, however, will use colour blender instead.
    # You only required to select the top of the chain joint.
    # return ( fkJntList , ikJntList , switchZroGrp , switch , rev ) ;

    if jnt [ -4 : ] != '_jnt' :
        
        mc.error ( "This script requires '_jnt' as joint's name suffix" ) ;
        
    else :    
        
        skinJntList = mc.listRelatives ( jnt , ad = True ) ; 
        skinJntList.append ( jnt ) ;
        skinJntList.reverse () ;
    
        # duplicate original joint chain for fk ik
        
        fkJntList = [] ;
        ikJntList = [] ;
        
        for i in range ( 0 , len ( skinJntList ) ) :
            
            jntNm = skinJntList [ i ] .split ( '_jnt' ) ;
                            
            if i == 0 :

                fkJnt = copyJnt ( oriJnt = skinJntList [ i ] , newJntNm = '%sFk_jnt' % jntNm [0] ) ;
                fkJntList.append ( fkJnt ) ;

                ikJnt = copyJnt ( oriJnt = skinJntList [ i ] , newJntNm = '%sIk_jnt' % jntNm [0] ) ;
                ikJntList.append ( ikJnt ) ;
                
            else :
                
                fkJnt = copyJnt ( oriJnt = skinJntList [ i ] , newJntNm = '%sFk_jnt' % jntNm [0] ) ;
                mc.parent ( fkJnt , fkJntList [ i - 1 ] ) ;
                fkJntList.append ( fkJnt ) ;
        
                ikJnt = copyJnt ( oriJnt = skinJntList [ i ] , newJntNm = '%sIk_jnt' % jntNm [0] ) ;
                mc.parent ( ikJnt , ikJntList [ i - 1 ] ) ;
                ikJntList.append ( ikJnt ) ;

        insertPosJnt ( jnt = fkJntList [0] ) ;
        insertPosJnt ( jnt = ikJntList [0] ) ;
        
        # fkIk switch ctrl
        
        nm = skinJntList [0] .split ( '1_jnt' ) ;
           
        switch = ( ctrl.createCurve ( jnt = '' , shape = 'stick' , name = '%sFkIk_ctrl' % nm [0] ,
            color = 'brightGreen' , scale = 1 ) [0] ) ;
        
        gen.lockAttr ( obj = switch , hide = True ) ;
        mc.addAttr ( switch , ln = 'fkIk' , at = 'double' , min = 0 , max = 1 , keyable = True ) ;
        
        switchZroGrp = mc.group ( em = True , n = '%sFkIkCtrlZro_grp' % nm [0] ) ;
        
        mc.parent ( switch , switchZroGrp ) ;
        
        # connectAttr 
        
        rev = mc.createNode ( 'reverse' , n = '%sFkIk_rev' % nm [0] ) ;
        
        mc.connectAttr ( '%s.fkIk' % switch , '%s.ix' % rev ) ;
        
        for i in range ( 0 , len ( skinJntList ) ) :
            
            jntNm = skinJntList [ i ] .split ( '_jnt' ) ;
            
            # translate and rotate

            parentConstraint = mc.parentConstraint ( fkJntList [ i ] , ikJntList [ i ] , skinJntList [ i ] , mo = True ) ;
            
            mc.connectAttr ( '%s.ox' % rev , '%s.%sW0' % ( parentConstraint [0] , fkJntList [ i ] ) ) ;
        
            mc.connectAttr ( '%s.fkIk' % switch , '%s.%sW1' % ( parentConstraint [0] , ikJntList [ i ] ) ) ; 
        
            # scl
            
            blndColors = mc.createNode ( 'blendColors' , n = '%sFkIkScl_bc' % jntNm [0] ) ;
            
            mc.connectAttr ( '%s.scale' % fkJntList [ i ] , '%s.color2' % blndColors ) ;  
            mc.connectAttr ( '%s.scale' % ikJntList [ i ] , '%s.color1' % blndColors ) ;
            
            mc.connectAttr ( '%s.op' % blndColors , '%s.scale' % skinJntList [ i ] ) ;
                        
            mc.connectAttr ( '%s.fkIk' % switch , '%s.blender' % blndColors ) ;

        return fkJntList , ikJntList , switchZroGrp , switch , rev ;

def fkIkParentBlendCustom ( jnt = '' , ctrl = '' , rev = '' ) :
    # return [ fkJntChain , ikJntChain , blendColorsList , parentConList ]

    skinJntChain = mc.listRelatives ( jnt , ad = True ) ;
    skinJntChain.append ( jnt ) ;
    skinJntChain.reverse () ;
        
    fkJntChain = [] ;
    ikJntChain = [] ;
    blendColorsList = [] ;
    parentConList = [] ;
       
    for each in skinJntChain :
            
        fkJnt = copyJnt ( oriJnt = each , newJntNm = '%sFk_jnt' % ( each.split ( '_jnt' ) [0] ) , parent = '' ) ;
        fkJntChain.append ( fkJnt ) ;   
    
        ikJnt = copyJnt ( oriJnt = each , newJntNm = '%sIk_jnt' % ( each.split ( '_jnt' ) [0] ) , parent = '' ) ;
        ikJntChain.append ( ikJnt ) ;
        
    for i in range ( 0 , len ( skinJntChain ) ) :
        if i != 0 :
            mc.parent ( fkJntChain [ i ] , fkJntChain [ i - 1 ] ) ;   
            mc.parent ( ikJntChain [ i ] , ikJntChain [ i - 1 ] ) ;
                
    for i in range ( 0 , len ( skinJntChain ) ) :
        
        name = skinJntChain[i].split ( '_jnt' ) [0] ;
        
        parentCon = mc.parentConstraint ( fkJntChain [i] , ikJntChain [i] , skinJntChain [i] , mo = True ) [0] ;
        
        blendColors = mc.createNode ( 'blendColors' , n = '%sFkIkScl_bc' % name ) ;
        
        mc.connectAttr ( '%s.s' % ikJntChain [i] , '%s.color1' % blendColors ) ;     
        mc.connectAttr ( '%s.s' % fkJntChain [i] , '%s.color2' % blendColors ) ;
        mc.connectAttr ( '%s.op' % blendColors , '%s.s' % skinJntChain [i] ) ;
        
        mc.connectAttr ( '%s.ox' % rev , '%s.%sW0' % ( parentCon , fkJntChain [ i ] ) ) ;
        
        mc.connectAttr ( '%s.fkIk' % ctrl , '%s.%sW1' % ( parentCon , ikJntChain [ i ] ) ) ; 
        mc.connectAttr ( '%s.fkIk' % ctrl , '%s.blender' % blendColors ) ; 
        
        blendColorsList.append ( blendColors ) ;
        parentConList.append ( parentCon ) ;
    
    return [ fkJntChain , ikJntChain , blendColorsList , parentConList ] ;

def fkStretchSquash ( ctrlA = '' , ctrlB = '' ) :
    
    nameA = ctrlA.split ( '_ctrl' ) [0] ;
    nameB = ctrlB.split ( '_ctrl' ) [0] ;
    
    # add attr
     
    mc.addAttr ( ctrlA , ln = '_stretchSquash_' , at = 'double' , keyable = True ) ;
    mc.setAttr ( '%s._stretchSquash_' % ctrlA , lock = True ) ; 
    
    mc.addAttr ( ctrlA , ln = 'stretch' , at = 'double' , keyable = True ) ;
    mc.addAttr ( ctrlA , ln = 'squash' , at = 'double' , keyable = True ) ;
    
    ctrlAShape = mc.listRelatives ( ctrlA , s = True ) ;
    
    mc.addAttr ( ctrlAShape , ln = 'stretchSquashAmp' , at = 'double' , dv = 0.1 , keyable = True ) ;

    # create amp

    amp = mc.createNode ( 'multiplyDivide' , n = '%sStSqAmp_mdv' % nameA ) ;
    mc.setAttr ( '%s.op' % amp , 1 ) ;
    
    mc.connectAttr ( '%s.stretchSquashAmp' % ctrlAShape [0] , '%s.i2x' % amp ) ;
    mc.connectAttr ( '%s.stretchSquashAmp' % ctrlAShape [0] , '%s.i2y' % amp ) ;
    
    # stSq to amp
    
    mc.connectAttr ( '%s.stretch' % ctrlA , '%s.i1x' % amp ) ;
    mc.connectAttr ( '%s.squash' % ctrlA , '%s.i1y' % amp ) ;
    
    # stretch 
    
    mc.connectAttr ( '%s.ox' % amp , '%sCtrlTfm_grp.ty' % nameB ) ;
    
    # squash 
    
    pma = mc.createNode ( 'plusMinusAverage' , n = '%sStSq_pma' % nameA ) ;
    mc.setAttr ( '%s.op' % pma , 1 ) ;
    mc.setAttr ( '%s.i2[1].i2y' % pma , 1 ) ;    
    
    mc.connectAttr ( '%s.oy' % amp , '%s.i2[0].i2y' % pma ) ;
    
    mc.connectAttr ( '%s.o2y' % pma , '%s_jnt.sx' % nameA ) ;
    mc.connectAttr ( '%s.o2y' % pma , '%s_jnt.sz' % nameA ) ;

def fkCurl ( ctrlA = '' , ctrlB = '' ) :
    
    nameA = ctrlA.split ( '_ctrl' ) [0] ;
    nameB = ctrlB.split ( '_ctrl' ) [0] ;
   
    # add attr
     
    mc.addAttr ( ctrlA , ln = '_curl_' , at = 'double' , keyable = True ) ;
    mc.setAttr ( '%s._curl_' % ctrlA , lock = True ) ;

    mc.addAttr ( ctrlA , ln = 'curl' , at = 'double' , keyable = True , min = 0 , max = 1 , dv = 1 ) ;
    
    ctrlAShape = mc.listRelatives ( ctrlA , s = True ) ;
    mc.addAttr ( ctrlAShape , ln = 'detailControl' , at = 'bool' , dv = False , keyable = True ) ;

    ctrlBShape = mc.listRelatives ( ctrlB , s = True ) ;

    # connect detail control to controller B visibility 
    
    mc.connectAttr ( '%s.detailControl' % ctrlAShape [0] , '%s.v' % ctrlBShape [0] ) ;
    
    # blendColors
    
    bc = mc.createNode ( 'blendColors' , n = '%sCurl_bc' % nameA ) ;
    mc.connectAttr ( '%s.curl' % ctrlA , '%s.blender' % bc ) ;
    
    mc.connectAttr ( '%s_jnt.r' % nameA , '%s.color1' % bc ) ;
    
    mc.connectAttr ( '%s.op' % bc , '%sCtrlTfm_grp.r' % nameB ) ;

def localWorld ( ctrl = '' , orientParent = 0 ) :
    # return [ localGrp , worldGrp ] ;

    mc.addAttr ( ctrl , ln = '_localWorld_' , at = 'double' , keyable = True ) ;
    mc.setAttr ( '%s._localWorld_' % ctrl , lock = True ) ;

    mc.addAttr ( ctrl , ln = 'localWorld' , at = 'double' , min = 0 , max = 1 , keyable = True ) ;
    
    rev = mc.createNode ( 'reverse' , n = 'chestFkLocWor_rev' ) ;
    
    mc.connectAttr ( '%s.localWorld' % ctrl , '%s.ix' % rev ) ;
 
    animGrp = '' ;
    
    if mc.objExists ( 'anim_grp' ) == True :
        animGrp = 'anim_grp' ; 
   
    counter = 1 ;   
    parent = mc.listRelatives ( ctrl , parent = True ) [0] ;
    
    if 'Zro_grp' not in parent and counter < 6 :
    
        parent = mc.listRelatives ( parent , parent = True ) [0] ;
    
        print 'searching zroGrp attempt [%s] %s ' % ( counter , parent ) ;
                
        if counter == 5 :
           
            mc.error ( "check your zero grp name and hierarchy, this function requires 'Zro_grp' as grp name suffix" ) ;
            
        counter = counter + 1 ;
    
    zroGrp = parent ;
    zroGrpNm = zroGrp.split ( 'Zro_grp' ) [0] ;
    zroGrpParent = mc.listRelatives ( parent , parent = True ) ;

    localGrp = gen.copyGrp ( oriGrp = zroGrp , newGrpNm = '%sLoc_grp' % zroGrpNm ) ;
    worldGrp = gen.copyGrp ( oriGrp = zroGrp , newGrpNm = '%sWor_grp' % zroGrpNm ) ;

    if zroGrpParent != None :
        mc.parent ( localGrp , worldGrp , zroGrpParent ) ;
                    
    # organization 
    
    if animGrp != '' :
        
        if orientParent == 1 :
            
            mc.parentConstraint ( animGrp , worldGrp , mo = True ) ;
    
        if orientParent == 0 :
    
            mc.orientConstraint ( animGrp , worldGrp , mo = True ) ;
        
    # contraint type 
    
    if orientParent == 1 :
        
        parCon = mc.parentConstraint ( localGrp , worldGrp , zroGrp , mo = True ) [0] ;
        
        mc.connectAttr ( '%s.ox' % rev , '%s.%sW0' % ( parCon , localGrp ) ) ; 
        mc.connectAttr ( '%s.localWorld' % ctrl , '%s.%sW1' % ( parCon , worldGrp ) ) ; 
            
    elif orientParent == 0 :

        orientCon = mc.orientConstraint ( localGrp , worldGrp , zroGrp , mo = True ) [0] ;
        
        mc.connectAttr ( '%s.ox' % rev , '%s.%sW0' % ( orientCon , localGrp ) ) ; 
        mc.connectAttr ( '%s.localWorld' % ctrl , '%s.%sW1' % ( orientCon , worldGrp ) ) ; 
    
    else :
        
        mc.error ( 'kwarg orientParent only accept int value from 0 - 1' ) ;

    return [ localGrp , worldGrp ] ;
