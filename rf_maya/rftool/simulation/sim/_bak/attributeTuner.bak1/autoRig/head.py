import maya.cmds as mc ;

#### autoRig module

import autoRig.jointDummy as dmJnt ;
reload ( dmJnt ) ;

#### rig module

import rig.controller as ctrl ;
reload ( ctrl ) ;

import rig.general as gen ;
reload ( gen ) ;

import rig.dic as dic ;
reload ( dic ) ;

import rig.joint as jnt ;
reload ( jnt ) ;

####

def head () :
    
    ### check elements
        
    skinGrp = '' ;
    animGrp = '' ;    
    rootJnt = '' ;
    neck2Jnt = '' ;
                    
    if mc.objExists ( 'skin_grp' ) == True :
        skinGrp = 'skin_grp' ;
                 
    if mc.objExists ( 'anim_grp' ) == True :
        animGrp = 'anim_grp' ;

    if mc.objExists ( 'root_jnt' ) == True :
        rootJnt = 'root_jnt' ;    
    
    if mc.objExists ( 'neck2_jnt' ) == True :
        neck2Jnt = 'neck2_jnt' ;
                
    # rig group 
    
    headRigGrp = mc.group ( n = 'headRig_grp' , em = True ) ;
    
    if neck2Jnt != '' :
        
        neck2JntROO = mc.xform ( neck2Jnt , q = True , ws = True , roo = True ) ;
        
        mc.xform ( headRigGrp , ws = True , roo = neck2JntROO ) ;
        
        mc.parentConstraint ( neck2Jnt , headRigGrp , mo = False ) ;
        
    if animGrp != '' :
        
        mc.parent ( headRigGrp , animGrp ) ;        
        
    # joint
    
    if mc.objExists ( 'head1_dmJnt' ) == False or mc.objExists ( 'head2_dmJnt' ) == False :
        
        mc.error ( 'check your head dummy joint chain' ) ;
            
    head1Jnt = jnt.copyJnt ( oriJnt = 'head1_dmJnt' , newJntNm = 'head1_jnt' ) ;     
    
    head2Jnt = jnt.copyJnt ( oriJnt = 'head2_dmJnt' , newJntNm = 'head2_jnt' , parent = head1Jnt ) ;     
    mc.setAttr ( '%s.segmentScaleCompensate' % head2Jnt , 0 ) ;
    
    # ctrl 
    
    headCtrl = ctrl.createController ( jnt = head1Jnt , shape = 'head' , gimble = True , 
        color = 'blue' , joint = True , scale = 1 , parent = '' , tfmGrp = False ) ;
        # return [ ctrl , zroGrp , tfmGrp , gimbleCtrl ]
    
    headCtrlZroGrp = mc.rename ( headCtrl [1] , 'headCtrlZro_grp' ) ;
    headGmblCtrl = mc.rename ( headCtrl [3] , 'headGmbl_ctrl' ) ; 
    headCtrl = mc.rename ( headCtrl [0] , 'head_ctrl' ) ;
    
    mc.move ( 0 , 0.81 , 0 , '%s.cv[0:4]' % headGmblCtrl , '%s.cv[7]' % headGmblCtrl , '%s.cv[10]' % headGmblCtrl , '%s.cv[13]' % headGmblCtrl , 
        r = True , os = True , wd = True ) ;
    
    mc.parentConstraint ( headGmblCtrl , head1Jnt ) ;
    mc.scaleConstraint ( headGmblCtrl , head1Jnt ) ;
    
    gen.lockAttr ( headCtrl , t = False , r = False , s = False , hide = True ) ;
    gen.lockAttr ( headGmblCtrl , t = False , r = False , hide = True ) ;
    
    mc.parent ( headCtrlZroGrp , headRigGrp ) ;
        
    headLocWor = jnt.localWorld ( ctrl = headCtrl , orientParent = 0 ) ;
    
    mc.setAttr ( '%s.localWorld' % headCtrl , 1 ) ;
        
    # organization

    gen.lockAttr ( headRigGrp ) ;
    
    if neck2Jnt != '' :
        
        mc.parent ( head1Jnt , neck2Jnt ) ;
        
    gen.lockAttr ( headCtrlZroGrp ) ;
    gen.lockAttr ( headLocWor [0] ) ;
    gen.lockAttr ( headLocWor [1] ) ;
