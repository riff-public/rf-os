import maya.cmds as mc ;

import rig.joint as jnt ;
reload ( jnt ) ;

import rig.general as gen ;
reload ( gen ) ;

import rig.controller as ctrl ;
reload ( ctrl ) ;

def root () :
        
    skinGrp = '' ;
    animGrp = '' ;
    rootDm = '' ;    

    ### condition 
    
    if mc.objExists ( 'root_dmJnt' ) == True :        
        rootDm = 'root_dmJnt' ;
                    
    else :
        mc.error ( "root_dmJnt doesn't exist" ) ;
    
    ### check groups 
        
    if mc.objExists ( 'skin_grp' ) == True :
        skinGrp = 'skin_grp' ;
    
    if mc.objExists ( 'anim_grp' ) == True :
        animGrp = 'anim_grp' ;
    
    #
                
    rootJnt = jnt.copyJnt ( oriJnt = rootDm , newJntNm = 'root_jnt' ) ; 
    
    if skinGrp != '' :
        mc.parent ( rootJnt , skinGrp ) ;  
    
    #
    
    rootRigGrp = mc.group ( n = 'rootRig_grp' , em = True ) ;
    
    if animGrp != '' :
        mc.parent ( rootRigGrp , animGrp ) ;
        
        
    #
             
    rootCtrl = ctrl.createController ( 
        jnt = rootJnt , shape = 'root' , gimble = True , color = 'brown' , joint = True , 
        scale = 0.75 , parent = rootRigGrp ) ;
    
    rootGmblCtrl = rootCtrl [3] ;
    rootZroGrp = rootCtrl [1] ;
    rootCtrl = rootCtrl [0] ;
              
    gen.lockAttr ( rootZroGrp ) ;
    gen.lockAttr ( rootCtrl , t = False , r = False , hide = True ) ;
    gen.lockAttr ( rootGmblCtrl , t = False , r = False , hide = True ) ; 
    
    mc.parentConstraint ( rootGmblCtrl , rootJnt , mo = False ) ;
    
    # organization
    
    gen.lockAttr ( rootRigGrp ) ; 
    mc.setAttr ( '%s.v' % rootDm , 0 ) ;
