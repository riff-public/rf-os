import sys ;

mp = '/Users/BirdieSaMa/Desktop/birdScript/' ;

if not mp in sys.path :
    
    sys.path.append ( mp ) ;

import maya.cmds as mc ;

mc.file ( f = True , new = True ) ;

####

import maya.cmds as mc ;

#### autoRig module

import autoRig.rigPrep as rp ;
reload ( rp ) ;

import autoRig.root as root ;
reload ( root ) ;

import autoRig.pelvis as pelvis ;
reload ( pelvis ) ;

import autoRig.spine as spine ;
reload ( spine ) ;

import autoRig.neck as neck ;
reload ( neck ) ;

import autoRig.head as head ;

import autoRig.jointDummy as dmJnt ;
reload ( dmJnt ) ;

#### rig module

import rig.controller as ctrl ;
reload ( ctrl ) ;

import rig.general as gen ;
reload ( gen ) ;

import rig.dic as dic ;
reload ( dic ) ;

import rig.joint as jnt ;
reload ( jnt ) ;

####
 
def leg ( side = 'LFT' ) : 
    
    # check elements
    
    skinGrp = '' ;
    animGrp = '' ;    
    rootJnt = '' ;
    pelvisJnt = '' ;
                    
    if mc.objExists ( 'skin_grp' ) == True :
        skinGrp = 'skin_grp' ;
                 
    if mc.objExists ( 'anim_grp' ) == True :
        animGrp = 'anim_grp' ;

    if mc.objExists ( 'root_jnt' ) == True :
        rootJnt = 'root_jnt' ;    
    
    if mc.objExists ( 'pelvis_jnt' ) == True :
        pelvisJnt = 'pelvis_jnt' ;
    
    # rig group
    
    legRigGrp = mc.group ( n = 'legRig%s_grp' % side , em = True ) ;
    
    if pelvisJnt != '' :
        pelvisROO = mc.xform ( pelvisJnt , q = True , ws = True , roo = True ) ;
        
        mc.xform ( legRigGrp , ws = True , roo = pelvisROO ) ; 
        
        mc.parentConstraint ( pelvisJnt , legRigGrp , mo = False ) ;
        
    legFkRigGrp = gen.copyGrp ( oriGrp = legRigGrp , newGrpNm = 'legFkRig%s_grp' % side , parent = legRigGrp ) ;
    
    legIkRigGrp = gen.copyGrp ( oriGrp = legRigGrp , newGrpNm = 'legIkRig%s_grp' % side , parent = legRigGrp ) ;
            
    # skin jnt 

    if mc.objExists ( 'upLeg%s_dmJnt' % side ) == False or \
        mc.objExists ( 'lowLeg%s_dmJnt' % side ) == False or \
        mc.objExists ( 'knee%s_dmJnt' % side ) == False or \
        mc.objExists ( 'ankle%s_dmJnt' % side ) == False or \
        mc.objExists ( 'ball%s_dmJnt' % side ) == False or \
        mc.objExists ( 'toe%s_dmJnt' % side ) == False or \
        mc.objExists ( 'heel%s_dmJnt' % side ) == False or \
        mc.objExists ( 'footIn%s_dmJnt' % side ) == False or \
        mc.objExists ( 'footOut%s_dmJnt' % side ) == False :
            
        mc.error ( 'check your %s joint chain' %s ) ;
            
    upLeg = jnt.copyJnt ( oriJnt = 'upLeg%s_dmJnt' % side , newJntNm = 'upLeg%s_jnt' % side ) ; 
        
    lowLeg = jnt.copyJnt ( oriJnt = 'lowLeg%s_dmJnt' % side , newJntNm = 'lowLeg%s_jnt' % side , parent = upLeg ) ;
    
    ankle = jnt.copyJnt ( oriJnt = 'ankle%s_dmJnt' % side , newJntNm = 'ankle%s_jnt' % side , parent = lowLeg ) ;
    
    ball = jnt.copyJnt ( oriJnt = 'ball%s_dmJnt' % side , newJntNm = 'ball%s_jnt' % side , parent = ankle ) ;
    
    toe = jnt.copyJnt ( oriJnt = 'toe%s_dmJnt' % side , newJntNm = 'toe%s_jnt' % side , parent = ball ) ;
    
    legSkinJntChain = [ upLeg , lowLeg , ankle , ball , toe ] ;

    # fkIk blend
    
    fkIkBlend = jnt.fkIkBlend ( jnt = upLeg , switch = '' , method = 'blendColors' ) ;
    
    legSwitch = fkIkBlend [0] ;
    legSwitchZroGrp = fkIkBlend [1] ;
    legFkJntChain = fkIkBlend [2] ;
    legIkJntChain = fkIkBlend [3] ;
    legRev = mc.listConnections ( legSwitch , type = 'reverse' ) [0] ;    
    
    # print switch , switchZroGrp , fkJntChain  , ikJntChain ;
    
    mc.parent ( legFkJntChain [0] , legFkRigGrp ) ; 
    mc.parent ( legIkJntChain [0] , legIkRigGrp ) ;
    
    mc.setAttr ( '%s.v' % legFkJntChain [0] , 0 ) ; 
    mc.setAttr ( '%s.v' % legIkJntChain [0] , 0 ) ;
        
    legSwitchZroGrpPos = mc.xform ( legSkinJntChain [2] , q = True , ws = True , rp = True ) ;
    mc.xform ( legSwitchZroGrp , ws = True , t = legSwitchZroGrpPos ) ;
    
    mc.parentConstraint ( legSkinJntChain [2] , legSwitchZroGrp , mo = True ) ;
    
    mc.parent ( legSwitchZroGrp , legRigGrp ) ;
    
    gen.lockAttr ( legSwitchZroGrp ) ; 
    
    mc.connectAttr ( '%s.fkIk' % legSwitch  , '%s.v' % legIkRigGrp ) ; 
    mc.connectAttr ( '%s.ox' % legRev  , '%s.v' % legFkRigGrp ) ;
                    
    ##############
    ''' FK '''
    ##############
    
    # ctrl 
    
        # upLegCtrl
    
    upLegFkCtrl = ctrl.controller ( jnt = legFkJntChain [0] , shape = 'sphere' , color = 'red' , 
        joint = True , local = False , tfmGrp = True , gimble = True , scale = 0.7 ) ;
    
    upLegFkGmblCtrl = upLegFkCtrl [1] ;
    upLegFkCtrlZroGrp = upLegFkCtrl [2] ;        
    upLegFkCtrlTfmGrp = upLegFkCtrl [3] ;
    upLegFkCtrl = upLegFkCtrl [0] ;
                
    gen.parCon ( upLegFkGmblCtrl , legFkJntChain [0] , mo = False , lock = True ) ;
    gen.lockAttr ( upLegFkCtrl , t = False , r = False , hide = True ) ;
    gen.lockAttr ( upLegFkGmblCtrl , t = False , r = False , hide = True ) ;
    
    mc.parent ( upLegFkCtrlZroGrp , legFkRigGrp ) ;
        
        # lowLegCtrl
    
    lowLegFkCtrl = ctrl.controller ( jnt = legFkJntChain [1] , shape = 'sphere' , color = 'red' , 
        joint = True , local = False , tfmGrp = True , gimble = True , scale = 0.7 , parent = upLegFkGmblCtrl ) ;
    
    lowLegFkGmblCtrl = lowLegFkCtrl [1] ;
    lowLegFkCtrlZroGrp = lowLegFkCtrl [2] ;        
    lowLegFkCtrlTfmGrp = lowLegFkCtrl [3] ;
    lowLegFkCtrl = lowLegFkCtrl [0] ;
                
    gen.parCon ( lowLegFkGmblCtrl , legFkJntChain [1] , mo = False , lock = True ) ;
    gen.lockAttr ( lowLegFkCtrl , t = False , r = False , hide = True ) ;
    gen.lockAttr ( lowLegFkGmblCtrl , t = False , r = False , hide = True ) ;
    
    jnt.fkStretchSquash ( ctrlA = upLegFkCtrl , ctrlB = lowLegFkCtrl , stretch = True , squash = False , stretchAxis = 'y' , stretchRev = True ) ;
    jnt.localWorld ( ctrl = upLegFkCtrl , orientParent = 0 ) ;
     
        # ankleCtrl
    
    ankleFkCtrl = ctrl.controller ( jnt = legFkJntChain [2] , shape = 'sphere' , color = 'red' , 
        joint = True , local = False , tfmGrp = True , gimble = True , scale = 0.7 , parent = lowLegFkGmblCtrl ) ;
    
    ankleFkGmblCtrl = ankleFkCtrl [1] ;
    ankleFkCtrlZroGrp = ankleFkCtrl [2] ;        
    ankleFkCtrlTfmGrp = ankleFkCtrl [3] ;
    ankleFkCtrl = ankleFkCtrl [0] ;
                
    gen.parCon ( ankleFkGmblCtrl , legFkJntChain [2] , mo = False , lock = True ) ;
    gen.lockAttr ( ankleFkCtrl , t = False , r = False , hide = True ) ;
    gen.lockAttr ( ankleFkGmblCtrl , t = False , r = False , hide = True ) ;
    
    jnt.fkStretchSquash ( ctrlA = lowLegFkCtrl , ctrlB = ankleFkCtrl , stretch = True , squash = False , stretchAxis = 'y' , stretchRev = True ) ;
    
        # ballCtrl
    
    ballFkCtrl = ctrl.controller ( jnt = legFkJntChain [3] , shape = 'hemisphere' , color = 'red' , 
        joint = True , local = False , tfmGrp = True , gimble = True , scale = 0.7 , parent = ankleFkGmblCtrl ) ;
    
    ballFkGmblCtrl = ballFkCtrl [1] ;
    ballFkCtrlZroGrp = ballFkCtrl [2] ;        
    ballFkCtrlTfmGrp = ballFkCtrl [3] ;
    ballFkCtrl = ballFkCtrl [0] ;
                
    gen.parCon ( ballFkGmblCtrl , legFkJntChain [3] , mo = False , lock = True ) ;
    gen.lockAttr ( ballFkCtrl , t = False , r = False , hide = True ) ;
    gen.lockAttr ( ballFkGmblCtrl , t = False , r = False , hide = True ) ;
    
    jnt.fkStretchSquash ( ctrlA = ankleFkCtrl , ctrlB = ballFkCtrl , stretch = True , squash = False , stretchAxis = 'z' , stretchRev = False ) ;
    
    ctrlAdjust = mc.xform ( legFkJntChain [3] , q = True , ws = True , ro = True ) [0] ;
    ctrlAdjustPivot = mc.xform ( legFkJntChain [3] , q = True , ws = True , rp = True ) ;
    ballFkCtrlShape = mc.listRelatives ( ballFkCtrl , s = True ) [0] ;
    ballFkGmblCtrlShape = mc.listRelatives ( ballFkGmblCtrl , s = True ) [0] ;
    
    mc.rotate ( ctrlAdjust , 0 , 0 , '%s.cv[0:97]' % ballFkCtrlShape ,  p = ctrlAdjustPivot ) ;
    mc.rotate ( ctrlAdjust , 0 , 0 , '%s.cv[0:97]' % ballFkGmblCtrlShape ,  p = ctrlAdjustPivot ) ;
    
    ##############
    ''' IK '''
    ##############
    
    mc.setAttr ( '%s.fkIk' % legSwitch , 1 ) ;
    
        # upLeg Ctrl 
        
    upLegIkCtrl = ctrl.controller ( jnt = legIkJntChain [0] , shape = 'cube' , color = 'blue' , 
        joint = True , local = False , tfmGrp = False , gimble = False , scale = 0.7 , parent = legIkRigGrp ) ;
    
    upLegIkCtrlZroGrp = upLegIkCtrl [2] ;
    upLegIkCtrl = upLegIkCtrl [0] ;
        
    gen.pointCon ( upLegIkCtrl , legIkJntChain [0] ) ;
    
    gen.lockAttr ( upLegIkCtrl , t = False , hide = True ) ;
        
        # foot Ctrl 
    
    legIkCtrl = ctrl.controller ( jnt = legIkJntChain [2] , shape = 'basicFoot' , color = 'blue' , 
        joint = True , local = False , tfmGrp = False , gimble = True , scale = 1 , parent = upLegIkCtrl , nm = 'leg%s' % side ) ;        
    
    legIkGmblCtrl = legIkCtrl [1] ;
    legIkCtrlZro = legIkCtrl [2] ;
    legIkCtrl = legIkCtrl [0] ;
    
    legIkCtrlShape = mc.listRelatives ( legIkCtrl , s = True ) [0] ;
    legIkGmblCtrlShape = mc.listRelatives ( legIkGmblCtrl , s = True ) [0] ;
    
    ctrlAdjust = mc.xform ( legIkJntChain [2] , q = True , ws = True , ro = True ) [0] ;
    ctrlAdjustPivot = mc.xform ( legIkJntChain [2] , q = True , ws = True , rp = True ) ;
        
    mc.rotate ( ctrlAdjust , 0 , 0 , '%s.cv[0:15]' % legIkCtrlShape ,  p = ctrlAdjustPivot ) ;
    mc.rotate ( ctrlAdjust , 0 , 0 , '%s.cv[0:15]' % legIkGmblCtrlShape ,  p = ctrlAdjustPivot ) ;
    
    mc.scale ( 1 , -1 , 1 , '%s.cv[0:15]' % legIkCtrlShape ,  p = ctrlAdjustPivot ) ;
    mc.scale ( 1 , -1 , 1 , '%s.cv[0:15]' % legIkGmblCtrlShape ,  p = ctrlAdjustPivot ) ;
    
    mc.move ( 0 , 0.2 , -0.01 , '%s.cv[0:15]' % legIkGmblCtrlShape , relative = True , ls = True , worldSpaceDistance = True ) ;
    
    gen.lockAttr ( legIkCtrl , t = False , r = False , hide = True ) ;
    gen.lockAttr ( legIkGmblCtrl , t = False , r = False , hide = True ) ;
        
        # knee Ctrl 
        
    kneeIkCtrl = ctrl.controller ( jnt = 'knee%s_dmJnt' % side , shape = 'sphere' , color = 'blue' , 
        joint = True , local = False , tfmGrp = False , gimble = False , scale = 0.25 , parent = legIkGmblCtrl , nm = 'knee%s' % side ) ;
        
    kneeIkCtrlZroGrp = kneeIkCtrl [2] ;
    kneeIkCtrl = kneeIkCtrl [0] ;
    
    gen.lockAttr ( kneeIkCtrl , t = False , hide = True ) ;
    
        # ik
    
            # lowLegIkIkh
        
    lowLegIkIkh = mc.ikHandle ( sj = legIkJntChain [0] , ee = legIkJntChain [2] , sol = 'ikRPsolver' ) ;

    lowLegIkEff = mc.rename ( lowLegIkIkh [1] , 'lowLegIk%s_eff' % side ) ;
    lowLegIkIkh = mc.rename ( lowLegIkIkh [0] , 'lowLegIk%s_ikh' % side ) ;
    
            # ankleIkIkh
            
    ankleIkIkh = mc.ikHandle ( sj = legIkJntChain [2] , ee = legIkJntChain [3] ) ;
    
    ankleIkEff = mc.rename ( ankleIkIkh [1] , 'ankleIk%s_eff' % side ) ;
    ankleIkIkh = mc.rename ( ankleIkIkh [0] , 'ankleIk%s_ikh' % side ) ;    
    
            # ballIkIkh

    ballIkIkh =  mc.ikHandle ( sj = legIkJntChain [3] , ee = legIkJntChain [4] ) ;
    
    ballIkEff = mc.rename ( ballIkIkh [1] , 'ballIk%s_eff' % side ) ;
    ballIkIkh = mc.rename ( ballIkIkh [0] , 'ballIk%s_ikh' % side ) ;
        
    # organization 
 
    if mc.objExists ( pelvisJnt ) == True :
        mc.parent ( upLeg , pelvisJnt ) ;   
                            
####
    
rp.createDummy () ;
root.root () ;
pelvis.pelvis () ;
spine.spine () ;
neck.neck () ;        
head.head () ;
leg ( side = 'LFT' ) ;
leg ( side = 'RGT' ) ;
