import maya.cmds as mc ;

#### autoRig module

import autoRig.jointDummy as dmJnt ;
reload ( dmJnt ) ;

#### rig module

import rig.controller as ctrl ;
reload ( ctrl ) ;

import rig.general as gen ;
reload ( gen ) ;

import rig.dic as dic ;
reload ( dic ) ;

import rig.joint as jnt ;
reload ( jnt ) ;

####
      
def spine () :

    ### check elements
    
    placement = '' ;
    placementSpcDm = '' ;

    stillGrp = '' ;
    
    skinGrp = '' ;
    animGrp = '' ;
    templateGrp = '' ;
    
    rootJnt = '' ;
            
    if mc.objExists ( 'placement_ctrl' ) == True :
        placement = 'placement_ctrl' ;
    
    if mc.objExists ( 'still_grp' ) == True :
        stillGrp = 'still_grp' ;
    
    if mc.objExists ( 'skin_grp' ) == True :
        skinGrp = 'skin_grp' ;
                 
    if mc.objExists ( 'anim_grp' ) == True :
        animGrp = 'anim_grp' ;

    if mc.objExists ( 'template_grp' ) == True :
        templateGrp = 'template_grp' ;

    if mc.objExists ( 'root_jnt' ) == True :
        rootJnt = 'root_jnt' ;

    # groups 
        
    spineRigGrp = mc.group ( n = 'spineRig_grp' , em = True ) ;
    
    if rootJnt != '' :
        
        rootJntRotOrder = mc.xform ( rootJnt , q = True , ws = True , roo = True ) ;
        rootJntPos = mc.xform ( rootJnt ,q = True , ws = True , rp = True ) ;
        rootJntRo = mc.xform ( rootJnt , q = True , ws = True , ro = True ) ;
    
        mc.parentConstraint ( rootJnt , spineRigGrp , mo = False ) ;
        mc.xform ( spineRigGrp , ws = True , roo = rootJntRotOrder ) ;

    if animGrp != '' :
        
        mc.parent ( spineRigGrp , animGrp ) ;
    
    spineRigGrpPos = mc.xform ( spineRigGrp , q = True , ws = True , rp = True ) ;
    spineRigGrpRO = mc.xform ( spineRigGrp , q = True , ws = True , ro = True ) ;
    spineRigGrpROO = mc.xform ( spineRigGrp , q = True , ws = True , roo = True ) ;
            
    spineFkRigGrp = mc.group ( n = 'spineFkRig_grp' , em = True ) ;
    mc.xform ( spineFkRigGrp , ws = True , t = spineRigGrpPos , ro = spineRigGrpRO , roo = spineRigGrpROO ) ;
    mc.parent ( spineFkRigGrp , spineRigGrp ) ; 
    
    spineIkRigGrp = mc.group ( n = 'spineIkRig_grp' , em = True ) ;
    mc.xform ( spineIkRigGrp , ws = True , t = spineRigGrpPos , ro = spineRigGrpRO , roo = spineRigGrpROO ) ;
    mc.parent ( spineIkRigGrp , spineRigGrp ) ; 
    
    spineIkStillGrp = mc.group ( n = 'spineIkStill_grp' , em = True ) ;
    
    if stillGrp != '' :
        mc.parent ( spineIkStillGrp , stillGrp ) ;
               
    # create joint chain
        
    if mc.objExists ( 'spine1_dmJnt' ) != True or \
        mc.objExists ( 'spine2_dmJnt' ) != True or \
        mc.objExists ( 'spine3_dmJnt' ) != True or \
        mc.objExists ( 'spine4_dmJnt' ) != True or \
        mc.objExists ( 'chest_dmJnt' ) != True :
            
        mc.error ( "check your spine dummy jnt chain" ) ;
        
    spine1Jnt = jnt.copyJnt ( oriJnt = 'spine1_dmJnt' , newJntNm = 'spine1_jnt' ) ;
    
    if skinGrp != '' :
        mc.parent ( spine1Jnt , skinGrp ) ; 

    if rootJnt != '' :
        mc.parent ( spine1Jnt , rootJnt ) ; 
        
    spine2Jnt = jnt.copyJnt ( oriJnt = 'spine2_dmJnt' , newJntNm = 'spine2_jnt' , parent = spine1Jnt ) ;

    spine3Jnt = jnt.copyJnt ( oriJnt = 'spine3_dmJnt' , newJntNm = 'spine3_jnt' , parent = spine2Jnt ) ;

    spine4Jnt = jnt.copyJnt ( oriJnt = 'spine4_dmJnt' , newJntNm = 'spine4_jnt' , parent = spine3Jnt ) ;
    
    spine5Jnt = jnt.copyJnt ( oriJnt = 'chest_dmJnt' , newJntNm = 'spine5_jnt' , parent = spine4Jnt ) ;
    
    skinJntChain = [ spine1Jnt , spine2Jnt , spine3Jnt , spine4Jnt , spine5Jnt ] ;
    
    # FK IK blend
    
    #fkIkBlend = jnt.fkIkParentBlend ( jnt = 'spine1_jnt' ) ;
    fkIkBlend = jnt.fkIkBlend ( jnt = spine1Jnt , switch = '' , method = 'parentConstraint' , posJnt = True ) ;

    switch = fkIkBlend [0] ;        
    switchZroGrp = fkIkBlend [1] ;
    fkJntChain = fkIkBlend [2] ;
    ikJntChain = fkIkBlend [3] ;
    rev = mc.listConnections ( switch , type = 'reverse' ) [0] ;
        
    fkTopParent = mc.listRelatives ( fkJntChain [0] , type = 'joint' , p = True ) [0] ;
    ikTopParent = mc.listRelatives ( ikJntChain [0] , type = 'joint' , p = True ) [0] ;

    
    ikPosJntChain = [] ;
    for i in range ( 0 , len ( ikJntChain ) -1 ) :
        posJnt = mc.listRelatives ( ikJntChain [ i ] , p = True , type = 'joint' ) ;
        ikPosJntChain.append ( posJnt ) ;

        
    mc.parentConstraint ( spine1Jnt , switchZroGrp , mo = False ) ;
    mc.parent ( switchZroGrp , spineRigGrp ) ;
    
        # FK IK group visibility
    
    mc.connectAttr ( '%s.ox' % rev , '%s.v' % spineFkRigGrp ) ;
    mc.connectAttr ( '%s.fkIk' % switch , '%s.v' % spineIkRigGrp ) ;
    
    
    ####################
    ''' FK  '''
    ####################    

    # ctrl for each joint
    
            # Fk spine 1
    
    fkSpine1 = ctrl.createController ( jnt = fkJntChain [0] , 
        shape = 'circle' , gimble = True , color = 'red' , joint = False , scale = 1.25 , parent = '' , tfmGrp = True) ;
                # [u'spine1Fk_ctrl', u'spine1FkCtrlZro_grp', u'spine1FkTfmZro_grp']
        
    fkSpine1Gmbl = mc.listRelatives ( fkSpine1 [0] , type = 'transform' , c = True ) ;
    mc.parentConstraint ( fkSpine1Gmbl , fkJntChain [0] ) ;
    
    fkSpine1PosJnt = mc.listRelatives ( fkJntChain [0] , type = 'joint' , p = True ) ;
    mc.pointConstraint ( fkSpine1Gmbl , fkSpine1PosJnt , mo = False ) ; 

    gen.lockAttr ( fkSpine1 [0] , t = False , r = False , hide = True ) ;
    gen.lockAttr ( fkSpine1Gmbl [0] , t = False , r = False , hide = True ) ;

            # Fk spine 2 
    
    fkSpine2 = ctrl.createController ( jnt = fkJntChain [1] , 
        shape = 'circle' , gimble = True , color = 'red' , joint = False , scale = 1.25 , parent = fkSpine1Gmbl , tfmGrp = True) ;

    fkSpine2Gmbl = mc.listRelatives ( fkSpine2 [0] , type = 'transform' , c = True ) ;
    mc.parentConstraint ( fkSpine2Gmbl , fkJntChain [1] , mo = False ) ;
 
    fkSpine2PosJnt = mc.listRelatives ( fkJntChain [1] , type = 'joint' , p = True ) ;
    mc.pointConstraint ( fkSpine2Gmbl , fkSpine2PosJnt , mo = False ) ; 

    gen.lockAttr ( fkSpine2 [0] , t = False , r = False , hide = True ) ;
    gen.lockAttr ( fkSpine2Gmbl [0] , t = False , r = False , hide = True ) ;

            # Fk spine 3
        
    fkSpine3 = ctrl.createController ( jnt = fkJntChain [2] , 
        shape = 'circle' , gimble = True , color = 'red' , joint = False , scale = 1.25 , parent = fkSpine2Gmbl , tfmGrp = True) ;
    
    fkSpine3Gmbl = mc.listRelatives ( fkSpine3 [0] , type = 'transform' , c = True ) ;
    mc.parentConstraint ( fkSpine3Gmbl , fkJntChain [2] , mo = False ) ;
 
    fkSpine3PosJnt = mc.listRelatives ( fkJntChain [2] , type = 'joint' , p = True ) ;
    mc.pointConstraint ( fkSpine3Gmbl , fkSpine3PosJnt , mo = False ) ; 
    
    gen.lockAttr ( fkSpine3 [0] , t = False , r = False , hide = True ) ;
    gen.lockAttr ( fkSpine3Gmbl [0] , t = False , r = False , hide = True ) ;
        
            # Fk spine 4
            
    fkSpine4 = ctrl.createController ( jnt = fkJntChain [3] , 
        shape = 'circle' , gimble = True , color = 'red' , joint = False , scale = 1.25 , parent = fkSpine3Gmbl , tfmGrp = True) ;
    
    fkSpine4Gmbl = mc.listRelatives ( fkSpine4 [0] , type = 'transform' , c = True ) ;
    mc.parentConstraint ( fkSpine4Gmbl , fkJntChain [3] , mo = False ) ;
 
    fkSpine4PosJnt = mc.listRelatives ( fkJntChain [3] , type = 'joint' , p = True ) ;
    mc.pointConstraint ( fkSpine4Gmbl , fkSpine4PosJnt , mo = False ) ; 
    
    gen.lockAttr ( fkSpine4 [0] , t = False , r = False , hide = True ) ;
    gen.lockAttr ( fkSpine4Gmbl [0] , t = False , r = False , hide = True ) ;
        
            # Fk spine 5
        
    fkSpine5 = ctrl.createController ( jnt = fkJntChain [4] , 
        shape = 'circle' , gimble = True , color = 'red' , joint = False , scale = 1.25 , parent = fkSpine4Gmbl , tfmGrp = True ) ;
    
    mc.delete ( fkSpine5 [0] ) ;
    
    mc.parentConstraint ( fkSpine5[2] , fkJntChain [4] , mo = False ) ;

        # stretch squash
    
    jnt.fkStretchSquash ( ctrlA = fkSpine1 [0] , ctrlB = fkSpine2 [0] ) ;
    jnt.fkStretchSquash ( ctrlA = fkSpine2 [0] , ctrlB = fkSpine3 [0] ) ;
    jnt.fkStretchSquash ( ctrlA = fkSpine3 [0] , ctrlB = fkSpine4 [0] ) ;
    jnt.fkStretchSquash ( ctrlA = fkSpine4 [0] , ctrlB = fkSpine5 [0] ) ;
    
        # curl
    jnt.fkCurl ( ctrlA = fkSpine1 [0] , ctrlB = fkSpine2 [0] ) ; 
    jnt.fkCurl ( ctrlA = fkSpine3 [0] , ctrlB = fkSpine4 [0] ) ;
    
    ###########
    ''' IK  '''
    ########### 
    mc.setAttr ( '%s.fkIk' % switch , 1 ) ;
    
        # nurb surface
    
    nurbEp = [] ;
    
    for each in ikJntChain :
        ep = mc.xform ( each , q = True , ws = True , rp = True ) ;
        nurbEp.append ( ep ) ;
    
    ikCrvLFT = mc.curve ( n = 'spineIkLFT_crv' , d = 3 , ep = nurbEp ) ;
    ikCrvRGT = mc.curve ( n = 'spineIkRGT_crv' , d = 3 , ep = nurbEp ) ;
        
    if placement != '' :
        
        placementPos = mc.xform ( placement , q = True , ws = True , rp = True ) ;
        placementRO = mc.xform ( placement , q = True , ws = True , ro = True ) ;
        placementROO = mc.xform ( placement , q = True , ws = True , roo = True ) ;
   
        placementSpcDm = mc.group ( n = 'placementSpcDm_grp' , em = True ) ;
        mc.xform ( placementSpcDm , t = placementPos , ro = placementRO , roo = placementROO ) ;
         
        mc.parent ( ikCrvLFT , ikCrvRGT , placementSpcDm ) ;
        mc.makeIdentity ( ikCrvLFT , apply = True , t = True , r = True , s = True ) ;
        mc.makeIdentity ( ikCrvRGT , apply = True , t = True , r = True , s = True ) ;   
    
    mc.setAttr ( '%s.tx' % ikCrvLFT , 0.25 ) ; 
    mc.setAttr ( '%s.tx' % ikCrvRGT , -0.25 ) ; 

    ikNrb = mc.loft ( ikCrvLFT , ikCrvRGT , n = 'spineIk_nrb' , ch = 1 , u = 1 , c = 0 , ar = 1 , d = 3 , ss = 1 , rn = 0 , po = 0 , rsn = True ) [0] ;
    
    if placementSpcDm != '' :
    
        mc.delete ( placementSpcDm ) ;
    
    else:
        
        mc.delete ( ikCrvLFT , ikCrvRGT ) ;
    
        # follicles 
        
            # follicle positions
        
    folPos = [ 0 ] ;
    
    fullLenCrv = mc.curve ( n = 'fullLen_crv' , d = 3 , ep = nurbEp ) ;
    fullLen = mc.arclen ( fullLenCrv ) ;
    mc.delete ( fullLenCrv ) ;
    
    for i in range ( 2 , 5 ) :
        
        posCrv = mc.curve ( n = 'fol%sPos_crv' % i , d = 3 , ep = nurbEp [ 0 : i ] ) ;
        pos = ( mc.arclen ( posCrv ) / fullLen ) ;
        mc.delete ( posCrv ) ;
        folPos.append ( pos ) ;
    
    folPos.append ( 1 ) ;

    folGrp = mc.group ( name = 'spineIkPosFlc_grp' , em = True ) ;
        
    folList = [] ;
            
    for i in range ( 0 , len ( folPos ) ) :
        fol = gen.createNrbFol ( nurb = ikNrb , name = 'spine%sIkPos_flc' % ( i + 1 ) , u = folPos [ i ] ) ;
        folList.append ( fol ) ;
        mc.parent ( fol , folGrp ) ;    
        
        # tip base and main control
        
    baseJnt = jnt.copyJnt ( oriJnt = ikJntChain [0] , newJntNm = 'spineIkBase_jnt' ) ;
    tipJnt = jnt.copyJnt ( oriJnt = ikJntChain [4] , newJntNm = 'spineIkTip_jnt' ) ;
            
    midConPos = mc.xform ( ikJntChain [2] , q = True , ws = True , rp = True ) ;
    midConROO = mc.xform ( ikJntChain [2] , q = True , ws = True , roo = True ) ;
        
    midConZroGrp = mc.group ( n = 'spineIkCtrlZro_grp' , em = True ) ; 
    midConAimGrp = mc.group ( n = 'spineIkCtrlAim_grp' , em = True ) ;
    midCon = ctrl.createCurve ( shape = 'square' , name = 'spineIk_ctrl' , gimble = False , color = 'yellow' , joint = True , scale = 1.30 ) [0] ;
    
    mc.xform ( midCon , ws = True , roo = midConROO ) ;
    mc.xform ( midConAimGrp , ws = True , roo = midConROO ) ;
    
    mc.parent ( midCon , midConAimGrp ) ;
    mc.parent ( midConAimGrp , midConZroGrp ) ;

    mc.xform ( midConZroGrp , ws = True , t = midConPos , roo = midConROO ) ;
    
        # ik detail controls
 
    detailCtrlList = [] ;
    detailCtrlZroGrpList = [] ;
        
    for i in range ( 0 , len ( ikJntChain ) - 1 ) :
        jntPos = mc.xform ( ikJntChain [i] , q = True , ws = True , rp = True ) ;
        jntROO = mc.xform ( ikJntChain [i] , q = True , ws = True , roo = True ) ;
        nm = ( ikJntChain [i] . split ( '_jnt' ) [0] ) ;
        
        zroGrp = mc.group ( n = '%sCtrlZro_grp' % nm , em = True ) ; 
        control = ctrl.createCurve ( shape = 'circle' , name = '%s_ctrl' % nm , gimble = False , color = 'lightBlue' , joint = True , scale = 1.25 ) [0] ;
        
        gen.lockAttr ( control , t = False , r = False , hide = True ) ;
        
        mc.xform ( zroGrp , ws = True , roo = jntROO ) ;
        mc.xform ( control , ws = True , roo = jntROO ) ;
        
        mc.parent ( control , zroGrp ) ;
        
        mc.xform ( zroGrp , ws = True , t = jntPos ) ;
        
        mc.parentConstraint ( control , ikJntChain [i] , mo = True ) ;
        mc.pointConstraint ( control , ikPosJntChain [i] , mo = True ) ;
        
        detailCtrlList.append ( control ) ;
        detailCtrlZroGrpList.append ( mc.listRelatives ( control , p = True ) [0] ) ;
        
        if i == 0 :
            
            mc.parentConstraint ( baseJnt , zroGrp ) ;
                            
        else :
        
            mc.parentConstraint ( folList [i] , zroGrp , mo = True ) ;

        mc.parentConstraint ( tipJnt , ikJntChain [4] ) ; 
                
        # skin
    
    skinCluster = mc.skinCluster ( baseJnt , midCon , tipJnt , ikNrb , nw = 1 , name = 'spineIk_skinCluster' ) [0] ;
    
    mc.skinPercent ( skinCluster , '%s.cv[0][0:3]' % ikNrb , transformValue = [ ( baseJnt , 1 ) ] ) ;
    mc.skinPercent ( skinCluster , '%s.cv[1][0:3]' % ikNrb , transformValue = [ ( baseJnt , 0.8 ) , ( midCon , 0.2 ) ] ) ;
    mc.skinPercent ( skinCluster , '%s.cv[2][0:3]' % ikNrb , transformValue = [ ( baseJnt , 0.4 ) , ( midCon , 0.6 ) ] ) ;
    mc.skinPercent ( skinCluster , '%s.cv[3][0:3]' % ikNrb , transformValue = [ ( midCon , 1 ) ] ) ;
    mc.skinPercent ( skinCluster , '%s.cv[4][0:3]' % ikNrb , transformValue = [ ( midCon , 0.6 ) , ( tipJnt , 0.4 ) ] ) ;
    mc.skinPercent ( skinCluster , '%s.cv[5][0:3]' % ikNrb , transformValue = [ ( midCon , 0.2 ) , ( tipJnt , 0.8 ) ] ) ;
    mc.skinPercent ( skinCluster , '%s.cv[6][0:3]' % ikNrb , transformValue = [ ( tipJnt , 1 ) ] ) ;
    
        # point and aim constraint 
        
    mc.pointConstraint ( baseJnt , tipJnt , midConZroGrp , mo = True ) ; 
    
    mc.aimConstraint ( tipJnt , midConAimGrp , mo = True , weight = 1 , aimVector = ( 0 , 1 , 0 ) , upVector = ( 0 , 0 , 1 ) ,
        worldUpType = 'objectrotation' , worldUpVector = ( 0 , 0 , 1 ) , worldUpObject = midConZroGrp ) ;
     
    # squash
    
        # add squash attributes to controller 
    
    detailSquashAttr = [] ;
        
    for each in detailCtrlList :
    
        mc.addAttr ( each , ln = '_squash_' , at = 'double' , keyable = True ) ;
        mc.setAttr ( '%s._squash_' % each , lock = True ) ; 
        
        mc.addAttr ( each , ln = 'squash' , at = 'double' , keyable = True ) ;
        
        detailSquashAttr.append ( '%s.squash' % each ) ;    
    
    mc.addAttr ( midCon , ln = '_squash_' , at = 'double' , keyable = True ) ;
    mc.setAttr ( '%s._squash_' % midCon , lock = True ) ; 
    
    mc.addAttr ( midCon , ln = 'autoSquash' , at = 'double' , min = 0 , max = 1 , dv = 1 , keyable = True ) ;    
    mc.addAttr ( midCon , ln = 'squash' , at = 'double' , keyable = True ) ;
        
        # create curve 
        
    ikSqDynCrv = mc.curve ( n = 'spineIkSqDyn_crv' , d = 3 , ep = nurbEp ) ;
    mc.rename ( mc.listRelatives ( ikSqDynCrv , s = True ) [0] , 'spineIkSqDyn_crvShape' ) ;
    
    ikSqDynCif = mc.createNode ( 'curveInfo' , n = 'spineIkSqDyn_cif' ) ;
    
    mc.connectAttr ( '%s.worldSpace[0]' % ikSqDynCrv , '%s.inputCurve' % ikSqDynCif ) ;
    
        #
    
    ikSqStatCrv = mc.curve ( n = 'spineIkSqStat_crv' , d = 3 , ep = nurbEp ) ;    
    mc.rename ( mc.listRelatives ( ikSqStatCrv , s = True ) [0] , 'spineIkSqStat_crvShape' ) ;
    
    ikSqStatCif = mc.createNode ( 'curveInfo' , n = 'spineIkSqStat_cif' ) ; 

    mc.connectAttr ( '%s.worldSpace[0]' % ikSqStatCrv , '%s.inputCurve' % ikSqStatCif ) ;
    
        #
    
    ikSqSfi = mc.createNode ( 'curveFromSurfaceIso' , n = 'spineIkSqDynCrv_sfi' ) ;
    
    mc.connectAttr ( '%s.worldSpace[0]' % ikNrb , '%s.inputSurface' % ikSqSfi ) ; 
    mc.connectAttr ( '%s.outputCurve' % ikSqSfi ,  '%s.create' % ikSqDynCrv ) ;
    
    mc.setAttr ( '%s.isoparmValue' % ikSqSfi , 0.5 ) ;
    mc.setAttr ( '%s.isoparmDirection' % ikSqSfi , 0 ) ;
       
        ## ( auto squash , main squash , detail squash ) ;
    
        # z detail squash
   
           # detailSquashAttr ( str list )
           # squashAmp ( mdv , list ) 
           # squashCombine ( pma , list ) 
           # ikJntChain = ( jnt ,list )
    
    counter = 1 ;
    
    squashAmp = [] ;
    squashCombine = [] ;
    
    for each in detailSquashAttr :
        
        amp = mc.createNode ( 'multiplyDivide' , n = 'spine%sIkSqAmp_mdv' % counter ) ;        
        pma = mc.createNode ( 'plusMinusAverage' , n = 'spine%sIkSqCombine_pma' % counter ) ;

        axes = [ 'x' , 'y' , 'z' ] ;

        for axis in axes :
        
            mc.setAttr ( 'spine%sIkSqAmp_mdv.i2%s' % ( counter , axis ) , 0.1 ) ;
    
            mc.setAttr ( 'spine%sIkSqCombine_pma.i3[0].i3%s' % ( counter , axis ) , 1 ) ; 
        
        mc.connectAttr ( 'spine%sIkSqAmp_mdv.ox' % counter , 'spine%sIkSqCombine_pma.i3[1].i3y' % counter ) ;
        mc.connectAttr ( 'spine%sIkSqAmp_mdv.oy' % counter , 'spine%sIkSqCombine_pma.i3[2].i3y' % counter ) ;
        mc.connectAttr ( 'spine%sIkSqAmp_mdv.oz' % counter , 'spine%sIkSqCombine_pma.i3[3].i3y' % counter ) ;
                                         
        mc.connectAttr ( 'spine%sIkSqCombine_pma.o3y' % counter , '%s.sx' % ikJntChain [ counter - 1 ] ) ; 
        mc.connectAttr ( 'spine%sIkSqCombine_pma.o3y' % counter , '%s.sz' % ikJntChain [ counter - 1 ] ) ;
        
        mc.connectAttr ( each , 'spine%sIkSqAmp_mdv.i1z' % counter ) ;

        squashAmp.append ( amp ) ;
        squashCombine.append ( pma ) ;
        counter = counter + 1 ;
        
        # y main squash
    
    for each in squashAmp :
   
        mc.connectAttr ( '%s.squash' % midCon , '%s.i1y' % each ) ;
    
    mc.setAttr ( '%s.i2y' % squashAmp [0] , 0.1 ) ;   
    mc.setAttr ( '%s.i2y' % squashAmp [1] , 0.2 ) ;
    mc.setAttr ( '%s.i2y' % squashAmp [2] , 0.3 ) ;
    mc.setAttr ( '%s.i2y' % squashAmp [3] , 0.2 ) ;
    
        # x auto squash        
    
            # len ( dyn ) / len ( stat ) = squash value
    sqVal = mc.createNode ( 'multiplyDivide' , n = 'spineIkAutoSqVal_mdv' ) ;
    mc.setAttr ( '%s.op' % sqVal , 2 ) ; 
    
    mc.connectAttr ( '%s.arcLength' % ikSqDynCif , '%s.i1x' % sqVal ) ;
    mc.connectAttr ( '%s.arcLength' % ikSqStatCif , '%s.i2x' % sqVal ) ;
    
            # invert linear graph direction
    sqGraphAdjust = mc.createNode ( 'multiplyDivide' , n = 'spineIkAutoSqGraphAdjust_mdv' ) ;
    mc.setAttr ( '%s.op' % sqGraphAdjust , 2 ) ;
    mc.setAttr ( '%s.i1x' % sqGraphAdjust , 1 ) ;
    mc.connectAttr ( '%s.ox' % sqVal , '%s.i2x' % sqGraphAdjust ) ;
    
            # convert linear graph to exponential
    sqGraphExpo = mc.createNode ( 'multiplyDivide' , n = 'spineIkAutoSqGraphExpo_mdv' ) ;
    mc.setAttr ( '%s.op' % sqGraphExpo , 3 ) ;
    mc.setAttr ( '%s.i2x' % sqGraphExpo , 0.5 ) ;
    mc.connectAttr ( '%s.ox' % sqGraphAdjust , '%s.i1x' % sqGraphExpo ) ;
    
            # make default squash value = 0
    sqZeroOutValue = mc.createNode ( 'plusMinusAverage' , n = 'spineIkAutoSqZroOutVal_pma' ) ;
    mc.setAttr ( '%s.i2[1].i2x' % sqZeroOutValue , -1 ) ;
    mc.connectAttr ( '%s.ox' % sqGraphExpo , '%s.i2[0].i2x' % sqZeroOutValue ) ;
    
            # auto squash switch
    autoSqSwitch = mc.createNode ( 'multiplyDivide' , n = 'spineIkAutoSqSwitch_mdv' ) ;
    mc.connectAttr ( '%s.o2x' % sqZeroOutValue , '%s.i1x' % autoSqSwitch ) ;
    mc.connectAttr ( '%s.autoSquash' % midCon , '%s.i2x' % autoSqSwitch ) ;
    
            # connect squash value to each spine amp
    
    for i in range ( 0 , 4 ) :
    
        mc.connectAttr ( '%s.ox' % autoSqSwitch , '%s.i1x' % squashAmp [i] ) ; 
        mc.setAttr ( '%s.i2x' % squashAmp [i] , 0.5 ) ;    
        
            # template cube 
    
    if templateGrp != '' :

        tmpGrp = gen.copyGrp ( oriGrp = stillGrp , newGrpNm = 'spineIk_tmpGrp' , parent = templateGrp ) ;
        
    else :
        
        tmpGrp = mc.group ( n = 'spineIk_tmpGrp' , em = True ) ;
        mc.setAttr ( '%s.template' % tmpGrp , 1 ) ;  
    
    mc.connectAttr ( '%s.fkIk' % switch , '%s.v' % tmpGrp ) ;
    
    spineTmp = [] ;

    for i in range ( 1 , 5 ) :
        
        tmpCube = mc.polyCube ( w = 1 , h = 0.35 , d = 1 , n = 'spine%sIk_tmpCube' % i ) [0] ;  
        
        mc.delete ( tmpCube , constructionHistory = True ) ;
        # delete history 
        
        mc.parentConstraint ( skinJntChain [ i - 1 ] , tmpCube , mo = 0 ) ;
        mc.scaleConstraint ( skinJntChain [ i - 1 ] , tmpCube ) ;
        #mc.connectAttr ( '%s.s' % skinJntChain [ i -1 ] , '%s.s' % tmpCube ) ;
        
        spineTmp.append ( tmpCube ) ;
        
        mc.parent ( tmpCube , tmpGrp ) ;
        
        gen.lockAttr ( tmpCube ) ;
    
        # detailControl visibility attribute 
    
    midConShape = mc.listRelatives ( midCon , s = True ) [0] ;
    
    mc.addAttr ( midConShape , ln = 'detailControl' , at = 'bool' , keyable = True ) ;
    
    detailCtrlGrp = gen.copyGrp ( oriGrp = spineIkRigGrp , newGrpNm = 'spineIkDetailCtrl_grp' , parent = spineIkRigGrp ) ;
    
    mc.connectAttr ( '%s.detailControl' % midConShape , '%s.v' % detailCtrlGrp ) ; 
    
    gen.lockAttr ( detailCtrlGrp ) ;
    
    mc.parent ( detailCtrlZroGrpList , detailCtrlGrp ) ;
    
    for each in detailCtrlZroGrpList :
        gen.lockAttr ( each ) ;
    
    # organization

        # main groups 
        
    gen.lockAttr ( spineRigGrp ) ;
    gen.lockAttr ( spineFkRigGrp ) ;
    gen.lockAttr ( spineIkRigGrp ) ;
    gen.lockAttr ( switchZroGrp ) ;

        # main joints
        
    mc.parent ( fkTopParent , spineFkRigGrp ) ;
    mc.setAttr ( '%s.v' % fkTopParent , 0 ) ; 

        # fk 
        
    fkTopZroGrp = fkSpine1 [1] ;
    mc.parent ( fkTopZroGrp , spineFkRigGrp ) ;
    gen.lockAttr ( fkTopZroGrp ) ;

    mc.parent ( ikTopParent , spineIkRigGrp ) ;
    mc.setAttr ( '%s.v' % ikTopParent , 0 ) ; 

    fkTfmGrpList = [] ;
    fkTfmGrpList.append ( fkSpine1 [2] ) ;
    fkTfmGrpList.append ( fkSpine2 [2] ) ;
    fkTfmGrpList.append ( fkSpine3 [2] ) ;
    fkTfmGrpList.append ( fkSpine4 [2] ) ;
    fkTfmGrpList.append ( fkSpine5 [2] ) ;
    for each in fkTfmGrpList :
        gen.lockAttr ( each ) ;

    fkZroGrpList = [] ;
    fkZroGrpList.append ( fkSpine1 [1] ) ;    
    fkZroGrpList.append ( fkSpine2 [1] ) ;
    fkZroGrpList.append ( fkSpine3 [1] ) ;
    fkZroGrpList.append ( fkSpine4 [1] ) ;
    fkZroGrpList.append ( fkSpine5 [1] ) ;   
    for each in fkZroGrpList :
        gen.lockAttr ( each ) ;
    
        # ik
                    
    gen.lockAttr ( spineIkStillGrp ) ;
    mc.parent ( ikNrb , spineIkStillGrp ) ;
    mc.parent ( folGrp , spineIkStillGrp ) ;
    gen.lockAttr ( folGrp ) ;
    mc.parent ( ikSqDynCrv , spineIkStillGrp ) ;
                
    mc.parent ( midConZroGrp , spineIkRigGrp ) ;
    gen.lockAttr ( midConZroGrp ) ;
    gen.lockAttr ( midConAimGrp ) ;

    mc.parent ( baseJnt , tipJnt , spineIkRigGrp ) ;
    mc.setAttr ( '%s.v' % baseJnt , 0 ) ;
    mc.setAttr ( '%s.v' % tipJnt , 0 ) ;

    mc.parent ( ikSqStatCrv , spineIkRigGrp ) ;

    mc.setAttr ( '%s.v' % ikSqDynCrv , 0 ) ;
    mc.setAttr ( '%s.v' % ikSqStatCrv , 0 ) ;
    gen.lockAttr ( ikSqDynCrv ) ;
    gen.lockAttr ( ikSqStatCrv ) ;
    gen.lockAttr ( tmpGrp ) ;

    gen.lockAttr ( midCon , t = False , r = False , hide = True ) ;

            # tip and base jnt

    if mc.objExists ( 'pelvis_jnt' ) == True :
        pelvisJnt = 'pelvis_jnt' ;
        mc.parentConstraint ( pelvisJnt , baseJnt , mo = True ) ;

    ####################
    ''' CHEST  '''
    #################### 
    
        # group 
    
    chestRigGrp = mc.group ( n = 'chestRig_grp' , em = True ) ;
    
    spine5ROO =  mc.xform ( spine5Jnt , q = True , ws = True , roo = True ) ;
    mc.xform ( chestRigGrp , roo = spine5ROO ) ;
     
    if animGrp != '' :
        mc.parent ( chestRigGrp , animGrp ) ;
    
    chestFkRigGrp = gen.copyGrp ( oriGrp = chestRigGrp , newGrpNm = 'chestFkRig_grp' , parent = chestRigGrp ) ;
    mc.parentConstraint ( spine5Jnt , chestFkRigGrp ) ; 
    
    chestIkRigGrp = gen.copyGrp ( oriGrp = chestRigGrp , newGrpNm = 'chestIkRig_grp' , parent = chestRigGrp ) ;
    
    if rootJnt != '' :
        mc.parentConstraint ( rootJnt , chestIkRigGrp ) ;
    
    mc.connectAttr ( '%s.fkIk' % switch , '%s.v' % chestIkRigGrp ) ; 
    mc.connectAttr ( '%s.ox' % rev , '%s.v' % chestFkRigGrp ) ; 
            
        # create chest skin jnt 
    
    if mc.objExists ( 'chest_dmJnt' ) == False or mc.objExists ( 'neck_dmJnt' ) == False :
        mc.error ( 'check your chest dummy jnt chain' ) ;
        
    chest1Jnt = jnt.copyJnt ( oriJnt = 'chest_dmJnt' , newJntNm = 'chest1_jnt' ) ;
        
    chest2Jnt = jnt.copyJnt ( oriJnt = 'neck_dmJnt' , newJntNm = 'chest2_jnt' , parent = chest1Jnt ) ;
    
    chestSkinJntChain = [ chest1Jnt , chest2Jnt ] ;
        
        # chest fkIk blend
    
    # chestFkIkBlend = jnt.fkIkParentBlendCustom ( jnt = chest1Jnt , ctrl = switch , rev = rev ) ;  
    chestFkIkBlend = jnt.fkIkBlend ( jnt = chest1Jnt , switch = switch , method = 'parentConstraint' ) ;
    chestFkJntChain = chestFkIkBlend [2] ;
    chestIkJntChain = chestFkIkBlend [3] ;
    
#    mc.parent 
    
    #################
    ''' chest FK '''
    ################# 
    
        # chest1
        
    chest1FkCtrl = ctrl.createController ( jnt = chestFkJntChain [0] , shape = 'circle' , 
        gimble = True , color = 'red' , joint = True , scale = 1.30 , parent = '' , tfmGrp = True ) ;
    # return [ ctrl , zroGrp , tfmGrp , gimbleCtrl ]
    
    chest1FkGmblCtrl = chest1FkCtrl [3] ; 
    chest1FkCtrlZro = chest1FkCtrl [1] ; 
    chest1FkTfmGrp = chest1FkCtrl [2] ;
    chest1FkCtrl = chest1FkCtrl [0] ;
    
    mc.parentConstraint ( chest1FkGmblCtrl , chestFkJntChain [0] ) ;  
    
    gen.lockAttr ( chest1FkCtrl , t = False , r = False , hide = True ) ;
    gen.lockAttr ( chest1FkGmblCtrl , t = False , r = False , hide = True ) ;
    
    mc.parent ( chest1FkCtrlZro , chestFkRigGrp ) ;    
    
        # chest2 
    
    chest2FkCtrl = ctrl.createController ( jnt = chestFkJntChain [1] , shape = 'circle' , 
        gimble = True , color = 'red' , joint = True , scale = 1.30 , parent = chest1FkGmblCtrl , tfmGrp = True ) ;
    
    chest2FkCtrlZro = chest2FkCtrl [1] ;    
    chest2FkTfmGrp = chest2FkCtrl [2] ;
    chest2FkCtrl = chest2FkCtrl [0] ;
    
    mc.parentConstraint ( chest2FkTfmGrp , chestFkJntChain [1] ) ;
            
        # stretch squash 
    
    jnt.fkStretchSquash ( ctrlA = chest1FkCtrl , ctrlB = chest2FkCtrl ) ;
    mc.delete ( chest2FkCtrl ) ;
    
        # local world
   
    chestFkLocalWorld = jnt.localWorld ( ctrl = chest1FkCtrl , orientParent = 0 ) ; 
    chestFkLocGrp = chestFkLocalWorld [0] ;
    chestFkWorGrp = chestFkLocalWorld [1] ;
    
    #################
    ''' chest IK '''
    ################# 
    
        # chest1
        
    chest1IkCtrl = ctrl.createController ( jnt = chestIkJntChain [0] , shape = 'chest' , 
        gimble = True , color = 'blue' , joint = True , scale = 1 , parent = '' , tfmGrp = True ) ;
    # return [ ctrl , zroGrp , tfmGrp , gimbleCtrl ]
    
    chest1IkCtrlZroGrp = chest1IkCtrl [1] ;
    chest1IkCtrlTfmGrp = chest1IkCtrl [2] ;
    chest1IkGmblCtrl = chest1IkCtrl [3] ;
    chest1IkCtrl = chest1IkCtrl [0] ;
    
    mc.parentConstraint ( chest1IkCtrl , chestIkJntChain [0] ) ;
    
    mc.parent ( chest1IkCtrlZroGrp , chestIkRigGrp ) ; 

    gen.lockAttr ( chest1IkCtrl , t = False , r = False , hide = True ) ;        
    gen.lockAttr ( chest1IkGmblCtrl , t = False , r = False , hide = True ) ;
    
        # chest 2

    chest2IkCtrl = ctrl.createController ( jnt = chestIkJntChain [1] , shape = 'chest' , 
        gimble = True , color = 'blue' , joint = True , scale = 1 , parent = chest1IkGmblCtrl , tfmGrp = True ) ;
    
    chest2IkCtrlZroGrp = chest2IkCtrl [1] ;
    chest2IkCtrlTfmGrp = chest2IkCtrl [2] ;     
    chest2IkCtrl = chest2IkCtrl [0] ;

    mc.parentConstraint ( chest2IkCtrlTfmGrp , chestIkJntChain [1] ) ;
    
        # stretch squash 
    
    jnt.fkStretchSquash ( ctrlA = chest1IkCtrl , ctrlB = chest2IkCtrl ) ;
    mc.delete ( chest2IkCtrl ) ;
    
        # local world
   
    chestIkLocalWorld = jnt.localWorld ( ctrl = chest1IkCtrl , orientParent = 1 ) ; 
    chestIkLocGrp = chestIkLocalWorld [0] ;
    chestIkWorGrp = chestIkLocalWorld [1] ;
            
    # organization
    
    gen.lockAttr ( chestRigGrp ) ;
    gen.lockAttr ( chestFkRigGrp ) ;
    gen.lockAttr ( chestIkRigGrp ) ;
    
    gen.lockAttr ( chest1FkCtrlZro ) ; 
    gen.lockAttr ( chest1FkTfmGrp ) ;
    
    gen.lockAttr ( chest2FkCtrlZro ) ; 
    gen.lockAttr ( chest2FkTfmGrp ) ;
    
    gen.lockAttr ( chestFkLocGrp ) ;
    gen.lockAttr ( chestFkWorGrp ) ;
    
    gen.lockAttr ( chest1IkCtrlZroGrp ) ; 
    gen.lockAttr ( chest1IkCtrlTfmGrp ) ;
        
    gen.lockAttr ( chest2IkCtrlZroGrp ) ; 
    gen.lockAttr ( chest2IkCtrlTfmGrp ) ;
    
    gen.lockAttr ( chestIkLocGrp ) ;
    gen.lockAttr ( chestIkWorGrp ) ;

    mc.parent ( chestFkJntChain [0] , chestFkRigGrp ) ; 
    mc.setAttr ( '%s.v' % chestFkJntChain [0] , 0 ) ;
    mc.parent ( chestIkJntChain [0] , chestIkRigGrp ) ;
    mc.setAttr ( '%s.v' % chestIkJntChain [0] , 0 ) ;
    
    mc.parent ( chest1Jnt , spine5Jnt ) ;
    
    mc.parentConstraint ( chestIkJntChain [0] , tipJnt ) ;
