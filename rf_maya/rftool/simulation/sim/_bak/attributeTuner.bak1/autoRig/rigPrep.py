import maya.cmds as mc ;

import autoRig.jointDummy as jntDmy ;
reload ( jntDmy ) ;

import rig.controller as ctrl ;
reload ( ctrl ) ;

import rig.general as gen ;
reload ( gen ) ;

def createDummy () :

        # group

    rigGrp = mc.group ( n = 'rig_grp' , em = True ) ;
    
    stillGrp = mc.group ( n = 'still_grp' , em = True , p = rigGrp ) ;
    mc.setAttr ( '%s.v' % stillGrp , 0 ) ;
    
            # parent later 
    
    animGrp = mc.group ( n = 'anim_grp' , em = True ) ;
    
    skinGrp = mc.group ( n = 'skin_grp' , em = True ) ;

    templateGrp = mc.group ( n = 'template_grp' , em = True ) ;
    mc.setAttr ( '%s.template' % templateGrp , 1 ) ; 

        # ctrl
        
    placementCtrl = ctrl.createCurve ( shape = 'fourDirectionalArrow' , color = 'yellow' , name = 'placement_ctrl') ;
    placementCtrl = placementCtrl [0] ;
    
    mc.parent ( placementCtrl , rigGrp ) ;
         
    flyCtrl = ctrl.createCurve ( shape = 'wings' , color = 'white' , name = 'fly_ctrl' , scale = 3 ) ;
    flyCtrl = flyCtrl [0] ;
       
    mc.setAttr ( '%s.tz' % flyCtrl , -1.5 ) ;
    mc.makeIdentity ( flyCtrl , apply = True ) ;
    mc.move ( 0 , 0 , 0 , '%s.rotatePivot' % flyCtrl , '%s.scalePivot' % flyCtrl , absolute = True ) ;     
    mc.parent ( flyCtrl , placementCtrl ) ;

        # create dummy joint

    dmyJnt = jntDmy.createDmJnt () ;        
    mc.parent ( dmyJnt , skinGrp ) ;
    
    # organization
    
    mc.parent ( animGrp , skinGrp , flyCtrl ) ;
    mc.parent ( templateGrp , rigGrp ) ;
                
    gen.lockAttr ( obj = stillGrp ) ;    
    gen.lockAttr ( obj = animGrp ) ;
    gen.lockAttr ( obj = skinGrp ) ;
    gen.lockAttr ( obj = templateGrp , v = False ) ;
