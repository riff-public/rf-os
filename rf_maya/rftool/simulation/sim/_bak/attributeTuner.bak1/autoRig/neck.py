import maya.cmds as mc ;

#### autoRig module

import autoRig.jointDummy as dmJnt ;
reload ( dmJnt ) ;

#### rig module

import rig.controller as ctrl ;
reload ( ctrl ) ;

import rig.general as gen ;
reload ( gen ) ;

import rig.dic as dic ;
reload ( dic ) ;

import rig.joint as jnt ;
reload ( jnt ) ;

####

def neck () :
    
    ### check elements
        
    skinGrp = '' ;
    animGrp = '' ;    
    rootJnt = '' ;
    chest2Jnt = '' ;
                    
    if mc.objExists ( 'skin_grp' ) == True :
        skinGrp = 'skin_grp' ;
                 
    if mc.objExists ( 'anim_grp' ) == True :
        animGrp = 'anim_grp' ;

    if mc.objExists ( 'root_jnt' ) == True :
        rootJnt = 'root_jnt' ;    
    
    if mc.objExists ( 'chest2_jnt' ) == True :
        chest2Jnt = 'chest2_jnt' ;

        # rigGrp
    
    neckRigGrp = mc.group ( n = 'neckRig_grp' , em = True ) ; 
     
    if chest2Jnt != '' :
        
        chest2ROO = mc.xform ( chest2Jnt , q = True , ws = True , roo = True ) ;
        
        mc.xform ( neckRigGrp , ws = True , roo = chest2ROO ) ;
        
        mc.parentConstraint ( chest2Jnt , neckRigGrp , mo = False ) ; 
           
        # neck skin joint        

    if mc.objExists ( 'neck_dmJnt' ) == False or mc.objExists ( 'head1_dmJnt' ) == False :
        mc.error ( 'check your neck dummy joint chain' ) ;

    neck1Jnt = jnt.copyJnt ( oriJnt = 'neck_dmJnt' , newJntNm = 'neck1_jnt' ) ;
    
    neck2Jnt = jnt.copyJnt ( oriJnt = 'head1_dmJnt' , newJntNm = 'neck2_jnt' , parent = neck1Jnt ) ;
    
    neckJntChain = [ neck1Jnt , neck2Jnt ] ;
   
        # neck controls
    
            # neck 1
        
    neck1Ctrl = ctrl.createController ( jnt = neckJntChain [0] , 
        shape = 'circle' , gimble = True , color = 'red' , joint = False , scale = 0.65 , parent = '' , tfmGrp = True ) ;
                # [u'spine1Fk_ctrl', u'spine1FkCtrlZro_grp', u'spine1FkTfmZro_grp']
                
    neck1CtrlZroGrp = neck1Ctrl [1] ;
    neck1CtrlTfmGrp = neck1Ctrl [2] ;
    neck1GmblCtrl = neck1Ctrl [3] ;
    neck1Ctrl = neck1Ctrl [0] ;
    
    mc.parentConstraint ( neck1GmblCtrl , neckJntChain [0] ) ;
    
    mc.parent ( neck1CtrlZroGrp , neckRigGrp ) ;
    
    gen.lockAttr ( neck1Ctrl , t = False , r = False , hide = True ) ;
    gen.lockAttr ( neck1GmblCtrl , t = False , r = False , hide = True ) ;
        
            # neck 2

    neck2Ctrl = ctrl.createController ( jnt = neckJntChain [1] , 
        shape = 'circle' , gimble = True , color = 'red' , joint = False , scale = 0.65 , parent = neck1GmblCtrl , tfmGrp = True , ) ;

    neck2CtrlZroGrp = neck2Ctrl [1] ;
    neck2CtrlTfmGrp = neck2Ctrl [2] ;
    neck2Ctrl = neck2Ctrl [0] ;
    
    mc.parentConstraint ( neck2CtrlTfmGrp , neckJntChain [1] ) ;      
    
            # stretch 
    
    jnt.fkStretchSquash ( ctrlA = neck1Ctrl , ctrlB = neck2Ctrl , stretch = True , squash = False ) ;      
    mc.delete ( neck2Ctrl ) ;
    
            # local world
            
    neck1CtrlLocWor = jnt.localWorld ( ctrl = neck1Ctrl , orientParent = 0 ) ;
    
    # organization 
    
    if chest2Jnt != '' :
        
        mc.parent ( neckJntChain [0] , chest2Jnt ) ;
    
    gen.lockAttr ( neck1CtrlZroGrp ) ;
    gen.lockAttr ( neck1CtrlTfmGrp ) ;

    gen.lockAttr ( neck2CtrlZroGrp ) ;
    gen.lockAttr ( neck2CtrlTfmGrp ) ;
    
    gen.lockAttr ( neck1CtrlLocWor [0] ) ;
    gen.lockAttr ( neck1CtrlLocWor [1] ) ;
    
    if animGrp != '' :
        
        mc.parent ( neckRigGrp , animGrp ) ;
        
    gen.lockAttr ( neckRigGrp ) ;  
