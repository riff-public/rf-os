import maya.cmds as mc ;

#### autoRig module

import autoRig.jointDummy as dmJnt ;
reload ( dmJnt ) ;

#### rig module

import rig.controller as ctrl ;
reload ( ctrl ) ;

import rig.general as gen ;
reload ( gen ) ;

import rig.dic as dic ;
reload ( dic ) ;

import rig.joint as jnt ;
reload ( jnt ) ;

####

def pelvis () :
    
    ### check elements
    
    placement = '' ;
    
    stillGrp = '' ;
    
    skinGrp = '' ;
    animGrp = '' ;
    templateGrp = '' ;
    
    rootJnt = '' ;
            
    if mc.objExists ( 'placement_ctrl' ) == True :
        placement = 'placement_ctrl' ;
    
    if mc.objExists ( 'still_grp' ) == True :
        stillGrp = 'still_grp' ;
    
    if mc.objExists ( 'skin_grp' ) == True :
        skinGrp = 'skin_grp' ;
                 
    if mc.objExists ( 'anim_grp' ) == True :
        animGrp = 'anim_grp' ;

    if mc.objExists ( 'template_grp' ) == True :
        templateGrp = 'template_grp' ;

    if mc.objExists ( 'root_jnt' ) == True :
        rootJnt = 'root_jnt' ;
        
    ### check dm jnt 
    
    if mc.objExists ( 'pelvis_dmJnt' ) == True :        
        pelvisDm = 'pelvis_dmJnt' ;
                    
    else :
        mc.error ( "pelvis_dmJnt doesn't exist" ) ;
        
        # create skin joint 
    
    pelvisJnt = jnt.copyJnt ( oriJnt = pelvisDm , newJntNm = 'pelvis_jnt' ) ;
       
        # rig group

    pelvisRigGrp = mc.group ( n = 'pelvisRig_grp' , em = True ) ;
        
    if rootJnt != '' :
        
        rootPos = mc.xform ( rootJnt , q = True , ws = True , rp = True ) ;
        rootRO = mc.xform ( rootJnt , q = True , ws = True , ro = True ) ;
        rootROO = mc.xform ( rootJnt , q = True , ws = True , roo = True ) ;
    
        mc.xform ( pelvisRigGrp , ws = True , roo = rootROO ) ;

        mc.parentConstraint ( rootJnt , pelvisRigGrp ) ;
        
    if animGrp != '' :
    
        mc.parent ( pelvisRigGrp , animGrp ) ;        
        
        # create controller 
        
    pelvisCtrl = ctrl.createController ( jnt = pelvisJnt , shape = 'square' , gimble = True , 
        color = 'red' , joint = True , scale = 1.3 , parent = pelvisRigGrp , tfmGrp = False ) ;
    
    pelvisCtrlZro = pelvisCtrl [1] ;
    pelvisCtrlGmbl = pelvisCtrl [3] ;
    pelvisCtrl = pelvisCtrl [0] ;
            
    mc.move ( 0 , -0.5 , 0 , '%s.cv[0:4]' % pelvisCtrl , r = True , os = True , wd = True ) ;
    mc.move ( 0 , -0.5 , 0 , '%s.cv[0:4]' % pelvisCtrlGmbl , r = True , os = True , wd = True , ) ;
        
    mc.parentConstraint ( pelvisCtrlGmbl , pelvisJnt ) ;
    
    gen.lockAttr ( pelvisCtrl , t = False , r = False , hide = True ) ;
    gen.lockAttr ( pelvisCtrlGmbl , t = False , r = False , hide = True ) ;
    
        # organization 
    
    if rootJnt != '' :
        mc.parent ( pelvisJnt , rootJnt ) ;
    
    gen.lockAttr ( pelvisRigGrp ) ;    
    gen.lockAttr ( pelvisCtrlZro ) ;
