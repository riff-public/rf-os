import maya.cmds as mc ;

import rig.dic as dic ;
reload ( dic ) ;

def createDmJnt () :

    root = createDmRoot () ;    
    pelvis = createDmPelvis () ;
    spineChain = createDmSpineChain () ;
    neckChain = createDmNeckChain () ;
    headChain = createDmHeadChain () ;

    legChain = createDmLegChain () ; 

    return root ;

#####

def createDmRoot () :
    
    root = mc.createNode ( 'joint' ,  n = "root_dmJnt" ) ;
    mc.setAttr ( "%s.t" % root , 0 , 9.0474036391524066 , 0.47496525881882345 , type = "double3" ) ;
    mc.setAttr ( "%s.ro" % root , 2 ) ;

    return root ;

def createDmPelvis () :
    
    pelvis = mc.createNode ( 'joint' , n = "pelvis_dmJnt" ) ;
    mc.setAttr ( "%s.t" % pelvis , 0 , 8.9103947630720484 , 0.44446554336463906 , type = "double3" ) ;
    mc.setAttr ( "%s.ro" % pelvis ,  2 ) ;

    if mc.objExists ( 'root_dmJnt' ) == True :
        mc.parent ( pelvis , 'root_dmJnt' ) ;

    return pelvis ;

def createDmSpineChain () :
    
    spine1 = mc.createNode ( 'joint' ,  n = "spine1_dmJnt" ) ;
    mc.setAttr ( "%s.t" % spine1 , 4.3790577010150533e-47 , 9.1944331096283651 , 0.48903753711989478 , type = 'double3' ) ;
    mc.setAttr ( "%s.ro" % spine1 ,  2 ) ;

    if mc.objExists ( 'root_dmJnt' ) == True :
        mc.parent ( spine1 , 'root_dmJnt' ) ;

    spine2 = mc.createNode ( 'joint' , n = "spine2_dmJnt" ) ;
    mc.setAttr ( "%s.t" % spine2 , 4.8148248609680896e-35 , 9.8118195103279824 , 0.49734415937876764 , type = 'double3' ) ;
    mc.setAttr ( "%s.ro" % spine2 , 2 ) ;
    mc.parent ( spine2 , spine1 ) ;

    spine3 = mc.createNode ( 'joint' , n = "spine3_dmJnt" ) ;
    mc.setAttr ( "%s.t" % spine3 , -1.9721522630525317e-31 , 10.388103263246226 , 0.48783876549735417 , type = 'double3' ) ;
    mc.setAttr ( "%s.ro" % spine3 , 2 ) ;
    mc.parent ( spine3 , spine2 ) ;

    spine4 = mc.createNode ( 'joint' , n = "spine4_dmJnt" ) ;
    mc.setAttr ( "%s.t" % spine4 , -1.9721522630525313e-31 , 11.013774709569617 , 0.41486876929647842 , type = 'double3' ) ;
    mc.setAttr ( "%s.ro" % spine4 , 2 ) ;
    mc.parent ( spine4 , spine3 ) ;

    chest = mc.createNode ( 'joint' , n = "chest_dmJnt" ) ;
    mc.setAttr ( "%s.t" % chest , 0 , 11.650614580989298 , 0.3164618075204384 , type = 'double3' ) ;
    mc.setAttr ( "%s.ro" % chest ,  2 ) ;
    mc.parent ( chest , spine4 ) ;

    return spine1 ;    

def createDmNeckChain () :

    neck = mc.createNode ( 'joint' ,  n =  "neck_dmJnt" ) ;
    mc.setAttr ( '%s.t' % neck , 0 , 12.703025903596108 , 0.11917751060196097 , type = 'double3' ) ;
    mc.setAttr ( '%s.ro' % neck , 2 ) ;

    if mc.objExists ( 'chest_dmJnt' ) == True :
        mc.parent ( neck , 'chest_dmJnt' ) ;

    if mc.objExists ( 'head1_dmjnt' ) == False :
        head1 = mc.createNode ( 'joint' , n = "head1_dmJnt" ) ;
        mc.setAttr ( "%s.t" % head1 , 0 , 13.29968568314475 , 0.17524534052751312 , type = 'double3' ) ;
        mc.setAttr ( "%s.ro" % head1 , 2 ) ;
        mc.parent ( head1 , neck ) ;

    return neck ;

def createDmHeadChain () :

    if mc.objExists ( 'head1_dmJnt' ) == False :
        head1 = mc.createNode ( 'joint' , n = "head1_dmJnt" ) ;
        mc.setAttr ( "%s.t" % head1 , 0 , 13.29968568314475 , 0.17524534052751312 , type = 'double3' ) ;
        mc.setAttr ( "%s.ro" % head1 , 2 ) ;
        
        if mc.objExists ( 'neck_dmJnt' ) == True :
            mc.parent ( head1 , 'neck_dmJnt' ) ;

    else :

        head1 = 'head1_dmJnt' ;
    
    head2 = mc.createNode ( 'joint' , n = "head2_dmJnt" ) ;
    mc.setAttr ( '%s.t' % head2 , 0 , 16.904855035375849 , 0.17524534052751312 , type = 'double3' ) ;
    mc.setAttr ( '%s.ro' % head2 , 2 ) ;
    mc.parent ( head2 , head1 ) ;

    return head1 ;
        
def createDmLegChain () :
    
    upLegLFT = mc.createNode ( 'joint' , n =  "upLegLFT_dmJnt" ) ;
    mc.setAttr ( "%s.t" % upLegLFT , 0.53507476617022964 , 8.022740287693134 , 0.36895938581521598 , type = 'double3' ) ;
    mc.setAttr ( "%s.ro" % upLegLFT ,  1 ) ;
    mc.setAttr ( "%s.jo" % upLegLFT , 0 , 0 , 180 , type = 'double3' ) ;

    if mc.objExists ( 'pelvis_dmJnt' ) == True :
        mc.parent ( upLegLFT , 'pelvis_dmJnt' ) ;

    lowLegLFT = mc.createNode ( 'joint' , n =  "lowLegLFT_dmJnt" ) ;
    mc.setAttr ( '%s.t' % lowLegLFT , 0.53507476617022964 , 4.5168507041858366 , 0.44589956248880974 , type = 'double3' ) ;
    mc.setAttr ( "%s.ro" % lowLegLFT , 1 ) ;
    mc.setAttr ( "%s.jo" % lowLegLFT , 0 , 0 , 180, type = "double3" ) ;
    mc.parent ( lowLegLFT , upLegLFT ) ;

    kneeLFT = mc.createNode ( 'joint' , n =  "kneeLFT_dmJnt" ) ;
    mc.setAttr ( '%s.t' % kneeLFT , 0.53507476617022964 , 4.5168507041858366 , 3.44589956248880974 , type = 'double3' ) ;
    mc.setAttr ( "%s.ro" % kneeLFT , 1 ) ;
    mc.setAttr ( "%s.jo" % kneeLFT , 0 , 0 , 180, type = "double3" ) ;
    mc.parent ( kneeLFT , lowLegLFT ) ;
        # color
    mc.setAttr ( '%s.overrideEnabled' % kneeLFT , 1 ) ;
    mc.setAttr ( '%s.overrideColor' % kneeLFT , dic.overrideColor [ 'lightBlue' ] ) ; 

    ankleLFT = mc.createNode ( 'joint' , n = "ankleLFT_dmJnt" ) ;
    mc.setAttr ( '%s.t' % ankleLFT , 0.53507476617022953 , 0.57018589249404394 , 0.013084978526534563 , type = 'double3' ) ;
    mc.setAttr ( '%s.ro' % ankleLFT , 4 ) ;
    mc.setAttr ( '%s.jo' % ankleLFT , 90 , 0 , 180 , type = 'double3' ) ;
    mc.parent ( ankleLFT , lowLegLFT ) ;

    ballLFT = mc.createNode ( 'joint' , n =  "ballLFT_dmJnt" ) ;
    mc.setAttr ( "%s.t" % ballLFT , 0.5350747661702292 , 6.6613381477509392e-16 , 1.0596381952578953 , type = 'double3' ) ;
    mc.setAttr ( '%s.ro' % ballLFT , 1 ) ;
    mc.setAttr ( '%s.jo' % ballLFT , 90 , 0 , 180 , type = 'double3' ) ;
    mc.parent ( ballLFT , ankleLFT ) ;

    toeLFT = mc.createNode ( 'joint' , n = "toeLFT_dmJnt" ) ;
    mc.setAttr ( "%s.t" % toeLFT , 0.5350747661702292 , 9.1947936735069015e-16 , 1.7006051314333228 , type = 'double3' ) ;
    mc.setAttr ( '%s.ro' % toeLFT , 1 ) ;
    mc.setAttr ( '%s.jo' % toeLFT , 90 , 0 , 180 , type = 'double3' ) ;
    mc.parent ( toeLFT , ballLFT ) ;

    heelLFT = mc.createNode ( 'joint' , n = "heelLFT_dmJnt" ) ;
    mc.setAttr ( "%s.t" % heelLFT , 0.5350747661702292 , 1.5543122344752192e-15 , -0.45405672923586371 , type = 'double3' ) ;
    mc.setAttr ( "%s.ro" % heelLFT , 1 ) ;
    mc.setAttr ( "%s.jo" % heelLFT , 90 , 0 , 180 , type = 'double3' ) ;    
        # color
    mc.setAttr ( '%s.overrideEnabled' % heelLFT , 1 ) ;
    mc.setAttr ( '%s.overrideColor' % heelLFT , dic.overrideColor [ 'lightBlue' ] ) ;
    mc.parent ( heelLFT , ankleLFT ) ;

    footOutLFT = mc.createNode ( 'joint' , n = "footOutLFT_dmJnt" ) ;
    mc.setAttr ( "%s.t" % footOutLFT , 0.83507476617022913 , 1.0250632134893504e-15 , 1.0596381952578953 , type = 'double3' ) ;
    mc.setAttr ( "%s.ro" % footOutLFT , 1 ) ;
    mc.setAttr ( "%s.jo" % footOutLFT , 90 , 0 , 180 , type = 'double3' ) ;
        # color
    mc.setAttr ( '%s.overrideEnabled' % footOutLFT , 1 ) ;
    mc.setAttr ( '%s.overrideColor' % footOutLFT , dic.overrideColor [ 'lightBlue' ] ) ;
    mc.parent ( footOutLFT , ballLFT ) ;

    footInLFT = mc.createNode ( 'joint' , n = "footInLFT_dmJnt" ) ;
    mc.setAttr ( "%s.t" % footInLFT , 0.23507476617022921 , 1.0985420214381916e-15 , 1.0596381952578953 , type = 'double3' ) ;
    mc.setAttr ( "%s.ro" % footInLFT , 1 ) ;
    mc.setAttr ( "%s.jo" % footInLFT , 90 , 0 , 180 , type = 'double3' ) ;
        # color
    mc.setAttr ( '%s.overrideEnabled' % footInLFT , 1 ) ;
    mc.setAttr ( '%s.overrideColor' % footInLFT , dic.overrideColor [ 'lightBlue' ] ) ;
    mc.parent ( footInLFT , ballLFT ) ;
    
    legChainRGT = mc.mirrorJoint ( upLegLFT , mirrorYZ = True , mirrorBehavior = True , searchReplace = [ 'LFT' , 'RGT' ] ) ;

    #print legChainRGT 
    #[u'upLegRGT_dmJnt', u'lowLegRGT_dmJnt', u'kneeRGT_dmJnt', u'ankleRGT_dmJnt', u'ballRGT_dmJnt', u'toeRGT_dmJnt', u'footOutRGT_dmJnt', u'footInRGT_dmJnt', u'heelRGT_dmJnt']

    legChainLFT = [ upLegLFT , lowLegLFT , kneeLFT , ankleLFT , ballLFT , toeLFT , footOutLFT , footInLFT , heelLFT ] ;

    # -1 1 1 for upleg
    # -1 -1 -1 for the rest
    for i in range ( 0 , len ( legChainRGT ) ) :

        nm = ( legChainRGT[i].split ( '_dmJnt' ) ) [0] ;
        
        rev = mc.createNode ( 'multiplyDivide' , n = '%s_dmMdv' % nm ) ;

        if i == 0 :
            mc.setAttr ( '%s.input2' % rev , -1 , 1 , 1 , type = 'double3' ) ;

        else :
            mc.setAttr ( '%s.input2' % rev , -1 , -1 , -1 , type = 'double3' ) ;

        mc.connectAttr ( '%s.t' % legChainLFT [ i ] , '%s.input1' %  rev ) ;
        mc.connectAttr ( '%s.output' % rev , '%s.t' % legChainRGT [ i ] ) ;
        
        mc.connectAttr ( '%s.r' % legChainLFT [ i ] , '%s.r' % legChainRGT [ i ] ) ;
        mc.connectAttr ( '%s.s' % legChainLFT [ i ] , '%s.s' % legChainRGT [ i ] ) ;
        
    return upLegLFT ;
