import maya.cmds as mc ;
import maya.mel as mm ;

import library.mayaUI as mul ;
reload ( mul ) ;

import library.mayaLibrary as ml ;
reload ( ml ) ;

import library.general as gl ;
reload ( gl ) ;

import rig.general as gen ;
reload ( gen ) ;

import general.panel as pnl ;
reload ( pnl ) ;

import rig.controller as ctrl ;
reload ( ctrl ) ;

# procs 

def timeRangeQuery () :
    
    startFrame = mc.playbackOptions ( q = True , minTime = True ) ; 
    endFrame = mc.playbackOptions ( q = True , maxTime = True ) ;
    
    return ( startFrame , endFrame ) ;
        
# main procs

def rotTrack ( obj = '' , startFrame = 1 , endFrame = 100 , defaultFrame = 970 ) :
    
    progressbar = mul.progressBar ( progressBar = 'absTrack_progressBar' ) ;
    
    obj = mc.ls ( sl = True ) [0] ;
    
    name = str ( obj ) ;
    
    axes = [ 'x' , 'y' , 'z' ] ;
    
    if ( '_riv' in name ) :
        
        name = obj.split ( '_riv' ) [0] ;
            
    roo = mc.xform ( obj , q = True , ws = True , roo = True ) ;
        
    # dummy rivet
    
    dmmyRiv = mc.listRelatives ( mc.createNode ( 'locator' ) , p = True ) ;
    dmmyRiv = mc.rename ( dmmyRiv , '%s_dmmyRiv' % name ) ;
    
    mc.xform ( dmmyRiv , roo = roo ) ;
    
    mc.connectAttr ( '%s.translate' % obj , '%s.translate' % dmmyRiv ) ;
    mc.connectAttr ( '%s.rotate' % obj , '%s.rotate' % dmmyRiv ) ;
    
    # >>>
    progressbar.update ( value = 20 ) ;
    
        # bake key
        
    startFrame = startFrame - 50 ;
    endFrame = endFrame + 50 ;
    
    try : 
    
        showList = pnl.activePanelShowQuery () ;
    
    except :
        
        try : 
            pnl.activePanelAllDisplay ( switch = False ) ;
            mc.bakeResults ( dmmyRiv , at = [ "tx" , "ty" , "tz" , "rx" , "ry" , "rz" ] , t = ( startFrame , endFrame ) , sb = 1 , simulation = True ) ;        
            # pnl.activePanelAllDisplay ( switch = True ) ;
        
        except : 
            mc.modelEditor ( 'modelPanel4' , e = True , allObjects = False ) ;
            mc.bakeResults ( dmmyRiv , at = [ "tx" , "ty" , "tz" , "rx" , "ry" , "rz" ] , t = ( startFrame , endFrame ) , sb = 1 , simulation = True ) ;
            #mc.modelEditor ( modelPanel4 , e = True , allObjects = True ) ;
            
    else :
    
        pnl.activePanelAllDisplay ( switch = False ) ;
        mc.bakeResults ( dmmyRiv , at = [ "tx" , "ty" , "tz" , "rx" , "ry" , "rz" ] , t = ( startFrame , endFrame ) , sb = 1 , simulation = True ) ;
        pnl.activePanelShowSet ( showList ) ;
        
    gen.lockAttr ( dmmyRiv , t = False , r = False , hide = True ) ;
    
    mc.currentTime ( defaultFrame ) ;
    
    # >>>
    progressbar.update ( value = 70 ) ;
    
    #################
    'global no touch'
    #################
    globalNoTouch = mc.group ( em = True , n = '%s_track_noTouch_grp' % name ) ;
    mc.xform ( globalNoTouch , roo = roo ) ;
    mc.setAttr ( '%s.v' % globalNoTouch , 0 ) ;
    gen.lockAttr ( globalNoTouch ) ;
    mc.parent ( dmmyRiv , globalNoTouch ) ;
    
    ###########
    ''' BWD ''' 
    ###########
        
        # ROT: transpose and decompose
    
    rotTpm = mc.createNode ( 'transposeMatrix' , n = '%s_BWD_rotRev_tpm' % name ) ; 
    mc.connectAttr ( '%s.matrix' % dmmyRiv , '%s.inputMatrix' % rotTpm ) ;    
    
    rotDcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_rotRev_dcm' % name ) ;
    mc.connectAttr ( '%s.outputMatrix' % rotTpm , '%s.inputMatrix' % rotDcm ) ;

        # ROT: BWD

    rotBWD = mc.group ( em = True , n = '%s_BWD_rotBWD_grp' % name ) ;
    mc.xform ( rotBWD , roo = roo ) ;
    gen.lockAttr ( rotBWD , r = False , hide = True ) ;
    gen.lockAttr ( rotBWD , t = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputRotate' % rotDcm , '%s.r' % rotBWD ) ;


    ''''''
    
    rotBWD_dmmy = mc.group ( em = True , n = '%s_BWD_rotBWD_dmmyGrp' % name ) ;
    mc.xform ( rotBWD_dmmy , roo = roo ) ; 
    gen.lockAttr ( rotBWD_dmmy , r = False , hide = True ) ;
    gen.lockAttr ( rotBWD_dmmy , t = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputRotate' % rotDcm , '%s.r' % rotBWD_dmmy ) ;
    
    
    transBWD_ref = mc.group ( em = True , n = '%s_BWD_transBWD_dmmyGrp' % name ) ;
    mc.xform ( transBWD_ref , roo = roo ) ;
    mc.setAttr ( '%s.v' % transBWD_ref , 0 ) ;
    gen.lockAttr ( transBWD_ref ) ;
    mc.parent ( transBWD_ref , rotBWD_dmmy ) ;
    mc.connectAttr ( '%s.t' % dmmyRiv , '%s.t' % transBWD_ref ) ;
    mc.connectAttr ( '%s.r' % dmmyRiv , '%s.r' % transBWD_ref ) ;
    
    transBWD_ref_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_transBWD_ref_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % transBWD_ref , '%s.inputMatrix' % transBWD_ref_dcm ) ;
    
    transBWD_ref_rev = mc.createNode ( 'multiplyDivide' , n = '%s_BWD_transBWD_ref_rev_mdv' % name ) ;
    mc.connectAttr ( '%s.outputTranslate' % transBWD_ref_dcm , '%s.i1' % transBWD_ref_rev ) ;
    for each in axes :
        mc.setAttr ( '%s.i2%s' % ( transBWD_ref_rev , each ) , -1 ) ; 
        
        # TRANS : BWD
        
    transBWD = mc.group ( em = True , n = '%s_BWD_transBWD_grp' % name ) ;
    mc.xform ( transBWD , roo = roo ) ;
    gen.lockAttr ( transBWD , t = False , hide = True ) ;
    gen.lockAttr ( transBWD , r = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.o' % transBWD_ref_rev , '%s.t' % transBWD ) ;
    mc.parent ( rotBWD , transBWD ) ;
                    
        # controller
        
    ctrlZro = mc.group ( em = True , n = '%s_BWD_ctrlZro_grp' % name ) ;
    mc.xform ( ctrlZro , roo = roo ) ;

    mainCtrl = ctrl.createCurve ( jnt = '' , shape = 'fourDirectionalArrow' , name = '%s_BWD_ctrl' % name , 
        gimble = False , color = 'lightBlue' , joint = True , scale = 1 ) [0] ;
    mc.xform ( mainCtrl , roo = roo ) ;
    gen.lockAttr ( mainCtrl , t = False , r = False , hide = True ) ;

    gmblCtrl = ctrl.createCurve ( jnt = '' , shape = 'star' , name = '%s_BWD_gmblCtrl' % name , 
        gimble = False , color = 'white' , joint = False , scale = 3.3 ) [0] ;
    mc.xform ( gmblCtrl , roo = roo ) ;
    gen.lockAttr ( gmblCtrl , t = False , r = False , hide = True ) ;

    mainCtrlShape = mc.listRelatives ( mainCtrl , shapes = True ) [0] ;
    mc.addAttr ( mainCtrlShape , ln = 'gimbalControl' , at = 'bool' , keyable = True ) ;
    mc.connectAttr ( '%s.gimbalControl' % mainCtrlShape , '%sShape.v' % gmblCtrl ) ;

        # dmmyRiv dmmy
        
    dmmyRivRefGrp = mc.group ( em = True , n = '%s_dmmyRiv_ref_grp' % name ) ; 
    mc.xform ( dmmyRivRefGrp , roo = roo ) ;
    mc.setAttr ( '%s.v' % dmmyRivRefGrp , 0 ) ;
    gen.lockAttr ( dmmyRivRefGrp ) ;
    mc.parent ( dmmyRivRefGrp , dmmyRiv ) ;
    
    dmmyRiv_ref_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_dmmyRiv_ref_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % dmmyRivRefGrp , '%s.inputMatrix' % dmmyRiv_ref_dcm ) ;
    
            # default tfm
        
    defaultTfm = mc.group ( em = True , n = '%s_BWD_defaultTfm_grp' % name ) ;
    mc.xform ( defaultTfm , roo = roo ) ;
    gen.lockAttr ( defaultTfm , t = False , r = False , hide = True ) ;
    
            # AMP

    mc.addAttr ( mainCtrl , ln = '__amplifier__' , at = 'double' , keyable = True ) ;
    mc.setAttr ( '%s.__amplifier__' % mainCtrl , lock = True ) ;
    
    for each in axes :
        mc.addAttr ( mainCtrl , ln = 'trans%s' % each.upper() , at = 'double' , keyable = True , dv = 1 ) ;
        
        temptBc = mc.createNode ( 'blendColors' , n = '%s_BWD_t%sSwitch_bc' % ( name , each ) ) ;
        
        temptTrans = mc.getAttr ( '%s.t%s' % ( dmmyRiv , each ) ) ; 
         
        mc.connectAttr ( '%s.trans%s' % ( mainCtrl , each.upper() ) , '%s.blender' % temptBc ) ;
                
        mc.connectAttr ( '%s.outputTranslate%s' % ( dmmyRiv_ref_dcm , each.upper() ) , '%s.c1r' % temptBc ) ;
        mc.setAttr ( '%s.c2r' % temptBc , temptTrans ) ;

        mc.connectAttr ( '%s.outputR' % temptBc , '%s.t%s' % ( defaultTfm , each ) ) ;
                
    ####

    for each in axes :
        mc.addAttr ( mainCtrl , ln = 'rot%s' % each.upper() , at = 'double' , keyable = True , dv = 1 ) ;
        
        temptBc = mc.createNode ( 'blendColors' , n = '%s_BWD_r%sSwitch_bc' % ( name , each ) ) ;
        
        temptRot = mc.getAttr ( '%s.r%s' % ( dmmyRiv , each ) ) ; 
         
        mc.connectAttr ( '%s.rot%s' % ( mainCtrl , each.upper() ) , '%s.blender' % temptBc ) ;
                
        mc.connectAttr ( '%s.outputRotate%s' % ( dmmyRiv_ref_dcm , each.upper() ) , '%s.c1r' % temptBc ) ;
        mc.setAttr ( '%s.c2r' % temptBc , temptRot ) ;

        mc.connectAttr ( '%s.outputR' % temptBc , '%s.r%s' % ( defaultTfm , each ) ) ;
            
    mc.parent ( transBWD , gmblCtrl ) ;    
    mc.parent ( gmblCtrl , mainCtrl ) ;
    mc.parent ( mainCtrl , ctrlZro ) ;
    #################################################    
    #mc.parent ( ctrlZro , defaultTfm ) ;
    ctrlZro_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_ctrlZro_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % defaultTfm , '%s.inputMatrix' % ctrlZro_dcm ) ; 
    mc.connectAttr ( '%s.outputTranslate' % ctrlZro_dcm , '%s.t' % ctrlZro ) ;
    mc.connectAttr ( '%s.outputRotate' % ctrlZro_dcm , '%s.r' % ctrlZro ) ;
    gen.lockAttr ( ctrlZro ) ;
    
        # organization 
        
    BWD_noTouch = mc.group ( em = True , n = '%s_BWD_noTouch_grp' % name ) ;
    mc.xform ( BWD_noTouch , roo = roo ) ;
    # mc.setAttr ( '%s.v' % BWD_noTouch , 0 ) ;
    gen.lockAttr ( BWD_noTouch ) ;
    
    mc.parent ( rotBWD_dmmy , BWD_noTouch );
    mc.parent ( defaultTfm , BWD_noTouch ) ;
    
    gen.lockAttr ( defaultTfm , s = False , v = False ) ;
    
        # BWD group prep

    BWD_dmmy = mc.group ( em = True , n = '%s_BWD_BWDgrp_dmmyGrp' % name ) ;
    mc.xform ( BWD_dmmy , roo = roo ) ;
    mc.setAttr ( '%s.v' % BWD_dmmy , 0 ) ;
    gen.lockAttr ( BWD_dmmy ) ;
    mc.parent ( BWD_dmmy , rotBWD ) ;
    
    BWD_dmmy_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_BWDgrp_dmmy_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % BWD_dmmy , '%s.inputMatrix' % BWD_dmmy_dcm ) ;
    
    ########### 
    ''' FWD ''' 
    ###########
    # >>>
    progressbar.update ( value = 90 ) ;
    
        # ref
        
            # under gimbal control ref
        
    gmblCtrl_ref = mc.group ( em = True , n = '%s_BWD_gmblCtrl_dmmyGrp' % name ) ;
    mc.xform ( gmblCtrl_ref , roo = roo ) ;
    mc.setAttr ( '%s.v' % gmblCtrl_ref , 0 ) ;
    gen.lockAttr ( gmblCtrl_ref ) ;
    mc.parent ( gmblCtrl_ref , gmblCtrl ) ;

    gmblCtrl_dmmy_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_gmblCtrl_dmmy_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % gmblCtrl_ref , '%s.inputMatrix' % gmblCtrl_dmmy_dcm ) ;
      
    gmblCtrl_dmmy_dcm_ref = mc.group ( em = True , n = '%s_BWD_gmblCtrl_dmmy_dcm_ref_grp' % name ) ;
    mc.xform ( gmblCtrl_dmmy_dcm_ref , roo = roo ) ;
    mc.connectAttr ( '%s.outputTranslate' % gmblCtrl_dmmy_dcm , '%s.t' % gmblCtrl_dmmy_dcm_ref ) ;
    mc.connectAttr ( '%s.outputRotate' % gmblCtrl_dmmy_dcm , '%s.r' % gmblCtrl_dmmy_dcm_ref ) ;
        
    FWD_rotTpm = mc.createNode ( 'transposeMatrix' , n = '%s_FWD_rotRev_tpm' % name ) ; 
    mc.connectAttr ( '%s.matrix' % gmblCtrl_dmmy_dcm_ref , '%s.inputMatrix' % FWD_rotTpm ) ;    
    
    FWD_rotDcm = mc.createNode ( 'decomposeMatrix' , n = '%s_FWD_rotRev_dcm' % name ) ;
    mc.connectAttr ( '%s.outputMatrix' % FWD_rotTpm , '%s.inputMatrix' % FWD_rotDcm ) ;
    
    FWD_rotBWD_dmmy = mc.group ( em = True , n = '%s_FWD_rotBWD_dmmyGrp' % name ) ;
    mc.xform ( FWD_rotBWD_dmmy , roo = roo ) ; 
    gen.lockAttr ( FWD_rotBWD_dmmy , r = False , hide = True ) ;
    gen.lockAttr ( FWD_rotBWD_dmmy , t = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputRotate' % FWD_rotDcm , '%s.r' % FWD_rotBWD_dmmy ) ;
    
    FWD_transBWD_ref = mc.group ( em = True , n = '%s_FWD_transBWD_dmmyGrp' % name ) ;
    mc.xform ( FWD_transBWD_ref , roo = roo ) ;
    mc.setAttr ( '%s.v' % FWD_transBWD_ref , 0 ) ;
    gen.lockAttr ( FWD_transBWD_ref ) ;
    mc.parent ( FWD_transBWD_ref , FWD_rotBWD_dmmy ) ;
    mc.connectAttr ( '%s.t' % gmblCtrl_dmmy_dcm_ref , '%s.t' % FWD_transBWD_ref ) ;
    mc.connectAttr ( '%s.r' % gmblCtrl_dmmy_dcm_ref , '%s.r' % FWD_transBWD_ref ) ;
    
    FWD_transBWD_ref_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_FWD_transBWD_ref_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % FWD_transBWD_ref , '%s.inputMatrix' % FWD_transBWD_ref_dcm ) ;
    
    FWD_transBWD_ref_rev = mc.createNode ( 'multiplyDivide' , n = '%s_FWD_transBWD_ref_rev_mdv' % name ) ;
    mc.connectAttr ( '%s.outputTranslate' % FWD_transBWD_ref_dcm , '%s.i1' % FWD_transBWD_ref_rev ) ;
    for each in axes :
        mc.setAttr ( '%s.i2%s' % ( FWD_transBWD_ref_rev , each ) , -1 ) ; 

    FWD_transBWD = mc.group ( em = True , n = '%s_FWD_transBWD_grp' % name ) ;
    mc.xform ( FWD_transBWD , roo = roo ) ;
    gen.lockAttr ( FWD_transBWD , t = False , hide = True ) ;
    gen.lockAttr ( FWD_transBWD , r = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.o' % FWD_transBWD_ref_rev , '%s.t' % FWD_transBWD ) ;
    
    FWD_rotBWD = mc.group ( em = True , n = '%s_FWD_rotBWD_grp' % name ) ;
    mc.xform ( FWD_rotBWD , roo = roo ) ; 
    gen.lockAttr ( FWD_rotBWD , r = False , hide = True ) ;
    gen.lockAttr ( FWD_rotBWD , t = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputRotate' % FWD_rotDcm , '%s.r' % FWD_rotBWD ) ;
              
    mc.parent ( FWD_rotBWD , FWD_transBWD ) ;
    
    # >>>
    progressbar.update ( value = 90 ) ;
    
        # default tfm
    
    fwdDefaultTfm = mc.group ( em = True , n = '%s_FWD_defaultTfm_grp' % name ) ;
    mc.xform ( fwdDefaultTfm , roo = roo ) ;
    mc.connectAttr ( '%s.t' % dmmyRiv , '%s.t' % fwdDefaultTfm ) ; 
    mc.connectAttr ( '%s.r' % dmmyRiv , '%s.r' % fwdDefaultTfm ) ;
    
    gen.lockAttr ( fwdDefaultTfm ) ;
    
    mc.parent ( FWD_transBWD , fwdDefaultTfm ) ;
    
        # organization 
        
    FWD_noTouch = mc.group ( em = True , n = '%s_FWD_noTouch_grp' % name ) ;
    mc.xform ( FWD_noTouch , roo = roo ) ;
    gen.lockAttr ( FWD_noTouch ) ;
    
    mc.parent ( FWD_rotBWD_dmmy , FWD_noTouch );
    mc.parent ( fwdDefaultTfm , FWD_noTouch ) ;
    mc.parent ( gmblCtrl_dmmy_dcm_ref , FWD_noTouch ) ;
    
    gen.lockAttr ( gmblCtrl_dmmy_dcm_ref ) ;
    gen.lockAttr ( defaultTfm , s = False , v = False ) ;
    
        # FWD group prep

    FWD_dmmy = mc.group ( em = True , n = '%s_FWD_FWDgrp_dmmyGrp' % name ) ;
    mc.xform ( FWD_dmmy , roo = roo ) ;
    mc.setAttr ( '%s.v' % FWD_dmmy , 0 ) ;
    gen.lockAttr ( FWD_dmmy ) ;
    mc.parent ( FWD_dmmy , FWD_rotBWD ) ;
    
    FWD_dmmy_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_FWD_FWDgrp_dmmy_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % FWD_dmmy , '%s.inputMatrix' % FWD_dmmy_dcm ) ;
        
    ######################## 
    ''' user interaction ''' 
    ########################
    # >>>
    progressbar.update ( value = 95 ) ;
    
    mc.parent ( BWD_noTouch , globalNoTouch ) ;    
    mc.parent ( FWD_noTouch , globalNoTouch ) ;
         
    # BWD grp
                    
    BWD_grp = mc.group ( em = True , n = '%s_BWD' % name ) ;
    mc.xform ( BWD_grp , roo = roo ) ;
    gen.lockAttr ( BWD_grp , t = False , r = False , v = False , hide = True ) ;
    gen.lockAttr ( BWD_grp , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputTranslate' % BWD_dmmy_dcm , '%s.t' % BWD_grp ) ;  
    mc.connectAttr ( '%s.outputRotate' % BWD_dmmy_dcm , '%s.r' % BWD_grp ) ;
    
    # FWD grp
                    
    FWD_grp = mc.group ( em = True , n = '%s_FWD' % name ) ;
    mc.xform ( FWD_grp , roo = roo ) ;
    gen.lockAttr ( FWD_grp , t = False , r = False , v = False , hide = True ) ;
    gen.lockAttr ( FWD_grp , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputTranslate' % FWD_dmmy_dcm , '%s.t' % FWD_grp ) ;  
    mc.connectAttr ( '%s.outputRotate' % FWD_dmmy_dcm , '%s.r' % FWD_grp ) ;
    
    ### >>>
    
    progressbar.delete();

    progressbar.insert ( parent = 'progressBarLayout' ) ;
    
def absoluteTrackUI () :
	
    # check if window exists
	
    if mc.window ( 'absoluteTrackUI' , exists = True ) :
		mc.deleteUI ( 'absoluteTrackUI' ) ;
		
    # create window 
	
    window = mc.window ( 'absoluteTrackUI' , title = "absTrack beta v2.0" ,
        mnb = False , mxb = False , sizeable = False ) ;

    mainLayout = mc.rowColumnLayout ( nc = 1 , cw = [ ( 1 , 280 ) ] ) ;

    userInputLayout = mc.rowColumnLayout ( nc = 7 , cw = [ ( 1 , 10 ) , ( 2 , 80 ) , ( 3 , 10 ) , ( 4 , 80 ) , ( 5 , 10 ) , ( 6 , 80 ) , ( 7 , 10 ) ] ) ;

    mc.text ( l = '' ) ;
    mc.text ( l = 'start frame' ) ;
    mc.text ( l = '' ) ;
    mc.text ( l = 'end frame' ) ;
    mc.text ( l = '' ) ;
    mc.text ( l = 'default pos' ) ;
    mc.text ( l = '' ) ;
    
    mc.text ( l = '' ) ;
    mc.intField ( 'absTrack_startFrame' , v = timeRangeQuery () [0] ) ;
    mc.text ( l = '' ) ;
    mc.intField ( 'absTrack_endFrame' , v = timeRangeQuery () [1] ) ;
    mc.text ( l = '' ) ;
    mc.intField ( 'absTrack_defaultPos' , v = 970 ) ;
    mc.text ( l = '' ) ;
    
    mc.setParent( '..' ) ;
    
    buttonLayout = mc.rowColumnLayout ( nc = 3 , cw = [ ( 1 , 10 ) , ( 2 , 260 ) , ( 3 , 10 ) ] ) ;
    mc.text ( l = '' ) ;
    mc.button ( l = 'track' , c = ml.Callback ( trackButtonCmd ) ) ;
    
    mc.text ( l = '' ) ;
    
    mc.text ( l = '' ) ;
    progressBarLayout = mc.rowColumnLayout ( 'progressBarLayout' , nc = 1 , cw = [ ( 1 , 280 ) ] ) ;
    mc.progressBar ( 'absTrack_progressBar' ) ;
    mc.setParent( '..' ) ;

    mc.text ( l = '' ) ;
    
    mc.setParent( '..' ) ;
     
    # show window 
	
    mc.showWindow ( window ) ;  

def trackButtonCmd () :
    rotTrack ( startFrame = mc.intField ( 'absTrack_startFrame' , q = True , v = True ) ,
        endFrame = mc.intField ( 'absTrack_endFrame' , q = True , v = True ) ,
        defaultFrame = mc.intField ( 'absTrack_defaultPos' , q = True , v = True ) ) ;

#absoluteTrackUI () ;