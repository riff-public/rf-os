import maya.cmds as mc ;

import library.maya as ml ;
reload ( ml ) ;

import library.general as gl ;
reload ( gl ) ;

import rig.general as gen ;
reload ( gen ) ;

##### proc #####

def selectionCheck () :

    selection = mc.filterExpand ( sm = 32 ) ;
    
    if selection == None or len ( selection ) != 2 :
        
        time = gl.systemTime () ;
        
        mc.error ( '( %s ) no 2 edges selected' % time ) ;

    else :

        return selection ;
        
##### main proc #####

def createRivet ( name = 'untitled' , autoNaming = False ) :

    # create nurb surface from selected edges :

    edges = selectionCheck () ;

        # check autoNaming

    if autoNaming == True :
        
        name = mc.listRelatives ( mc.listRelatives ( edges , p = True ) , p = True ) [0] ;
    
        # check duplication
    
    if mc.objExists ( '%s_rivet' % name ) == True :
        
        time = gl.systemTime () ;
        
        mc.error ( '( %s ) %s_rivet already exist' % ( time , name ) ) ;
    
    curves = [] ;
    curveInputs = [] ;

    for each in edges :
          
          polyEdgeToCurve = mc.polyToCurve ( each , form = 2 , degree = 1 ) ;
                 
          mc.select ( polyEdgeToCurve [0] ) ;
          
          curve = mc.filterExpand ( sm = 9 ) ;
          
          curves.append ( curve ) ; 
          curveInputs.append ( polyEdgeToCurve [1] ) ;
          
                   
    curve1 = mc.rename ( curves[0] , "%s_crv1_crv" % name ) ;
    curve2 = mc.rename ( curves[1] , "%s_crv2_crv" % name ) ;
    
    mc.rename ( curveInputs [0] , '%s_crv1_polyEdgeToCurve' % name ) ;
    mc.rename ( curveInputs [1] , '%s_crv2_polyEdgeToCurve' % name ) ;
    
    loft = mc.loft ( curve1 , curve2 , ch = 1 , u = 1 , po = 0 , rsn = True , name = "%s_loftSfc_nrb" % name ) ;
    mc.rename ( loft [1] , '%s_loft' % name ) ;
   
    mc.select ( loft [0] ) ;
    
    loftSurface =  mc.filterExpand ( sm = 10 ) [0] ;
        
    # follicle
    
    fol = gen.createNrbFol ( nurb = loftSurface , u = 0.5 , v = 0.5 , name = '%s_flc' % name ) ;
    
    # rivet
    
    riv = mc.createNode ( 'locator' ) ;
    
    riv = mc.listRelatives ( riv , parent = True ) [0] ;
    
    riv = mc.rename ( riv , '%s_rivet' % name ) ;
    
    mc.connectAttr ( '%s.translate' % fol , '%s.translate' % riv ) ;

    mc.connectAttr ( '%s.rotate' % fol , '%s.rotate' % riv ) ;
    
    # organize
         
    noTouch = mc.group ( n = '%s_rivet_noTouch_grp' % name , em = True ) ; 
   
    mc.parent ( curve1 , curve2 , loftSurface , fol , noTouch ) ;

    mc.setAttr ( '%s.v' % noTouch , 0 ) ;
    gen.lockAttr ( noTouch , v = False , hide = True ) ;
    
    gen.lockAttr ( curve1 ) ;
    gen.lockAttr ( curve2 ) ;
    
    gen.lockAttr ( loft [0] ) ;
    
    gen.lockAttr ( fol ) ;
    
# ------------

def createRivetButton ( ) :
    
    name = mc.textField ( 'txtField_name' , q = True , tx = True ) ;
    
    if name == '' :
        
        createRivet ( autoNaming = True ) ;
        
    else :
        
        createRivet ( name = name ) ;

##### UI #####

def noFlipRivetUI () :
	
    # check if window exists
	
    if mc.window ( 'noFlipRivetUI' , exists = True ) :
		mc.deleteUI ( 'noFlipRivetUI' ) ;
		
    # create window 
	
    window = mc.window ( 'noFlipRivetUI' , title = "noFlipRiv v a.0.1" ,
        mnb = False , mxb = False , sizeable = False ) ;

    mainLayout = mc.rowColumnLayout ( nc = 1 , cw = [ ( 1 , 250 ) ] ) ;

    nameLayout = mc.rowColumnLayout ( nc = 4 , cw = [ ( 1 , 10 ) , ( 2 , 40 ) , ( 3 , 190 ) , ( 4 , 10 ) ] ) ;
    
    mc.text ( l = '' ) ;
    mc.text ( l = 'name :' ) ;
    mc.textField ( 'txtField_name' ) ;
    mc.text ( l = '' ) ;
    
    mc.setParent( '..' ) ;
    
    nameLayout = mc.rowColumnLayout ( nc = 3 , cw = [ ( 1 , 10 ) , ( 2 , 230 ) , ( 3 , 10 ) ] ) ;
    
    mc.text ( l = '' ) ;
    mc.button ( 'create' , c = ml.Callback ( createRivetButton ) ) ;
    mc.text ( l = '' ) ;
    
    mc.setParent( '..' ) ;

    # show window 
	
    mc.showWindow ( window ) ;          
