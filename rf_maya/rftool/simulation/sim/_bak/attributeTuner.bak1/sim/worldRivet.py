##############################################################################################################################
# world rivet beta v1.1
##############################################################################################################################

'''

wrote for maya 2015

project start     : ~september 2014
alpha v0.1        : ~september 2014
alpha v0.2 start  : 5th september 2014
beta v1.0         : 25th september 2014
    script done   : 25th september 2014
    tested        : -

beta v1.1 notes:
- UI compatiable with maya 2015
- UI is now scalable 

beta v1.0 notes:
- changed the project from no flip rivet to world rivet
- uses the same method as rivet script ( mel )
- UI update
- *** requires matrixNodes.bundle plug-in

alpha v0.2 notes :
- added offset option
- UI done

alpha v0.1 notes :
- basic functionality/UI done

'''

import maya.cmds as mc ;

import library.mayaLibrary as ml ;
reload ( ml ) ;

import library.general as gl ;
reload ( gl ) ;

import rig.general as gen ;
reload ( gen ) ;

##### proc #####

def selectionCheck () :

    selection = mc.filterExpand ( sm = 32 ) ;
    
    if selection == None or len ( selection ) != 2 :
        
        time = gl.systemTime () ;
        
        mc.error ( '( %s ) no 2 edges selected' % time ) ;

    else :

        return selection ;
        
##### main proc #####

def createRivet ( name = 'untitled' , autoNaming = False ) :

    axes = [ 'x' , 'y' , 'z' ] ;

    # create nurb surface from selected edges :

    edges = selectionCheck () ;

        # check autoNaming

    if autoNaming == True :
        
        name = mc.listRelatives ( mc.listRelatives ( edges , p = True ) , p = True ) [0] ;
    
        # check duplication
    
    if mc.objExists ( '%s_riv' % name ) == True :
        
        time = gl.systemTime () ;
        
        mc.error ( '( %s ) %s_riv already exist' % ( time , name ) ) ;
    
    ######
        
    obj = edges[0].split ( '.' ) [0] ;
    edge1 = edges[0].split ( '[' ) [1] ;
    edge1 = edge1.split ( ']' ) [0] ;
    edge2 = edges[1].split ( '[' ) [1] ;
    edge2 = edge2.split ( ']' ) [0] ;

    # curve from mesh edge
                 
    cme1 = mc.createNode ( 'curveFromMeshEdge' , n = '%s_edge1_cme' % name ) ;
    mc.setAttr ( '%s.ihi' % cme1 , 1 ) ;
    mc.setAttr ( '%s.ei[0]' % cme1 , int ( edge1 ) ) ;    

    cme2 = mc.createNode ( 'curveFromMeshEdge' , n = '%s_edge2_cme' % name ) ;
    mc.setAttr ( '%s.ihi' % cme2 , 1 ) ;
    mc.setAttr ( '%s.ei[0]' % cme2 , int ( edge2 ) ) ;

    mc.connectAttr ( '%s.w' % obj , '%s.im' % cme1 ) ;
    mc.connectAttr ( '%s.w' % obj , '%s.im' % cme2 ) ;
    
    # loft
        
    loft = mc.createNode ( 'loft' , n = '%s_loft' % name ) ;
    mc.setAttr ( '%s.ic' % loft , s = 2 ) ;
    mc.setAttr ( '%s.u' % loft , True ) ;
    mc.setAttr ( '%s.rsn' % loft , True ) ;

    mc.connectAttr ( '%s.oc' % cme1 , '%s.ic[0]' % loft ) ;
    mc.connectAttr ( '%s.oc' % cme2 , '%s.ic[1]' % loft ) ;
    
    # point on surface
    
    poi = mc.createNode ( 'pointOnSurfaceInfo' , n = '%s_poi' % name ) ;
    mc.setAttr ( '%s.turnOnPercentage' % poi , 1 ) ;
    mc.setAttr ( '%s.parameterU' % poi , 0.5 ) ;
    mc.setAttr ( '%s.parameterV' % poi , 0.5 ) ;

    mc.connectAttr ( '%s.os' % loft , '%s.is' % poi ) ; 

    # dmmyTfm
    
    dmmyTfm = mc.createNode ( 'transform' , n = '%s_dmmyTfm' % name ) ;
    mc.setAttr ( '%s.v' % dmmyTfm , 0 );
        
    # aim contraint
    
    aimCon = mc.createNode ( 'aimConstraint' , p = dmmyTfm , n = '%s_aimCon' % dmmyTfm ) ;
    mc.setAttr ( '%s.tg[0].tw' % aimCon , 1 ) ; 
    mc.setAttr ( '%s.a' % aimCon , 0 , 1 , 0 , type = 'double3' ) ;
    mc.setAttr ( '%s.u' % aimCon , 0 , 0 , 1 , type = 'double3' ) ;
    gen.lockAttr ( aimCon ) ;

    mc.connectAttr ( '%s.position' % poi , '%s.translate' % dmmyTfm ) ;
    mc.connectAttr ( '%s.n' % poi , '%s.tg[0].tt' % aimCon ) ;  
    mc.connectAttr ( '%s.tv' % poi , '%s.wu' % aimCon ) ;  

    for each in axes :

        mc.connectAttr ( '%s.cr%s' % ( aimCon , each ) , '%s.r%s' % ( dmmyTfm , each ) ) ; 

    # organization

    noTouch = mc.group ( em = True , n = '%s_riv_noTouch_grp' % name ) ;
    mc.setAttr ( '%s.v' % noTouch , 0 ) ;
    gen.lockAttr ( noTouch ) ;
    mc.parent ( dmmyTfm  , noTouch ) ;
    
    # rivet
    
    rotZro_dmmyTfm = mc.createNode ( 'transform' , n = '%s_rotZro_dmmyTfm' % name ) ;
    gen.lockAttr ( rotZro_dmmyTfm , r = False , hide = True ) ;
    mc.parent ( rotZro_dmmyTfm , dmmyTfm ) ;
    gen.lockAttr ( rotZro_dmmyTfm , t = False , s = False , v = False ) ;
    gen.lockAttr ( dmmyTfm ) ;
    
    rotZro_dmmyTfm_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_rotZro_dmmyTfm_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % rotZro_dmmyTfm , '%s.inputMatrix' % rotZro_dmmyTfm_dcm ) ;
    
    riv = mc.createNode ( 'transform' , n = '%s_riv' % name ) ;
    mc.createNode ( 'locator' , n = '%sShape' % riv , p = riv ) ;
    
    mc.connectAttr ( '%s.outputTranslate' % rotZro_dmmyTfm_dcm , '%s.t' % riv ) ;
    mc.connectAttr ( '%s.outputRotate' % rotZro_dmmyTfm_dcm , '%s.r' % riv ) ;
    
# ------------

def createRivetButton ( ) :
    
    name = mc.textField ( 'nameTxtField' , q = True , tx = True ) ;
    
    if name == '' :
        
        createRivet ( autoNaming = True ) ;
        
    else :
        
        createRivet ( name = name ) ;

##### UI #####

def worldRivetUI () :
	
    # check if window exists
	
    if mc.window ( 'noFlipRivetUI' , exists = True ) :
		mc.deleteUI ( 'noFlipRivetUI' ) ;

    window = mc.window ( 'noFlipRivetUI' , title = "beta v1.1" ) ;
	
###

    mainLayout = mc.formLayout ( 'mainLayout' , w = 200 , h = 75 ) ;
    
    nameTxt = mc.text ( 'nameTxt' , l = 'name : ' , h = 25 ) ;
    nameTxtField = mc.textField ( 'nameTxtField' , h = 25 ) ;
    createBtn = mc.button ( 'createBtn' , l = 'create' , h = 25 , c = ml.Callback ( createRivetButton ) ) ;
    
    mc.formLayout ( 'mainLayout' , e = True , 
        
        af = (
        ( nameTxt , 'left' , 10 ) , ( nameTxt , 'top' , 10 ) ,
        ( nameTxtField , 'right' , 10 ) , ( nameTxtField , 'top' , 10 ) ,
        ( createBtn , 'left' , 10 ) , ( createBtn , 'right' , 10 ) ,
        ) ,
        
        ac = (
        ( nameTxtField , 'left' , 5 , nameTxt ) ,
        ( createBtn , 'top' , 5 , nameTxtField ) ,
        ) ,       
        
        ap = (
        ( createBtn , 'bottom' , 10 , 100 )
        )
        ) ;

###	
		
    # show window 

    mc.showWindow ( window ) ;  
