class Callback ( object ) :
    
    def __init__ ( self , func , *args , **kwargs ) :
        
        self.func = func ;
        self.args = args ;
        self.kwargs = kwargs ;
        
    def __call__ ( self , * args ) :
        
        return self.func ( *self.args , **self.kwargs ) ;
