import sys ;
import os ;

def we_are_frozen():
    # All of the modules are built-in to the interpreter, e.g., by py2exe
    return hasattr(sys, "frozen")

def module_path():
    encoding = sys.getfilesystemencoding()
    if we_are_frozen():
        return os.path.dirname(unicode(sys.executable, encoding))
    return os.path.dirname(unicode(__file__, encoding))

from datetime import datetime ;

def systemTime () :
	rawTime = str ( datetime.time ( datetime.now () ) ) ;
	rawTime2 = rawTime.split ( ':' ) ;
	rawTime3 = rawTime2[2].split( '.' ) ;
	
	hour = rawTime2 [0] ;
	minute = rawTime2 [1] ;
	second = rawTime3 [0] ;
	
	return '%s:%s:%s' % ( hour , minute , second ) ;
