import maya.cmds as mc ;

class progressBar ( object ) :
    
    def __init__ ( self , progressBar = '' ) :
        self.progressBar = progressBar ;
        
    def delete ( self ) :
        mc.deleteUI ( self.progressBar ) ;

    def insert ( self , parent ) :
        self.layout = parent ;
        mc.progressBar ( self.progressBar , p = self.layout ) ;
    
    def update ( self , value ) :
        self.value = value ;
        mc.progressBar ( self.progressBar , e = True , pr = self.value ) ;
        
    def printMyself ( self ) :
        print self.progressBar ;


