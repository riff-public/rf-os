import pymel.core as pm
import maya.cmds as mc
import json
import os

class SamColorYeti ( object ) :
	def __init__ ( self ) :
		self.presetPath = self.getPreferencePath ()

	def getPreferencePath ( self , * args ) :
		myDoc = os.path.expanduser ( '~' )
		myDoc = myDoc.replace ( '\\' , '/' )
		prefPath = myDoc + '/samColorYeti'
		if not os.path.exists ( prefPath ) :
			os.makedirs ( prefPath )
		return prefPath

	def getColor ( self , target ) :
		#version = 0
		#path = self.presetPath + '/' + str ( target ) + '.' + str ( version ) + '.json'
		folder = pm.textField ( self.file_txt , q = True , tx = True )
		path = self.presetPath + '/' + folder
		if not os.path.exists ( path ) :
			os.makedirs ( path )
		file = path + '/' + str ( target ) + '.json'
		colorDict = {}
		node = target.getShape ()
		color = node.getAttr ( 'color' )
		colorDict [ str ( node ) ] = color
		color_json = json.dumps ( colorDict )
		#while os.path.exists ( path ) :
		#	path = self.presetPath + '/' + str ( target ) + '.' + str ( version ) + '.json'
		#	version += 1
		with open ( file , 'w' ) as outfile :
			json.dump ( colorDict , outfile )

	def getAll ( self , *args ) :
		pick = pm.ls ( sl = True )
		for each in pick :
			self.getColor ( target = each )

	def closeFolderUI ( self , *args ) :
		if pm.window ( 'samAddColorYeti' , exists = True ) :
			pm.deleteUI ( 'samAddColorYeti' )
		else : pass

	def createFolder_cmd ( self , *args ) :
		self.getAll ()
		self.closeFolderUI ()
		self.listDict ()

	def folderUI ( self , *args ) :
		w = 200.00
		rgb = 255.00
		if pm.window ( 'samAddColorYeti' , exists = True ) :
			pm.deleteUI ( 'samAddColorYeti' )
		else : pass
		window = pm.window ( 'samAddColorYeti' , t = 'Add' , tb = True , mnb = True , mxb = True , s = False , rtf = True )
		pm.window ( 'samAddColorYeti' , e = True , w = w , h = 10 )
		with window :
			with pm.rowColumnLayout ( nc = 1 ) :
				self.file_txt = pm.textField ( fn = 'obliqueLabelFont' , w = w )
				pm.button ( l = 'ADD' , c = self.createFolder_cmd , bgc = ( 188 / rgb , 36 / rgb , 60 / rgb ) )

	def listDict ( self , *args , **kwargs ) :
		fileLists = os.listdir ( self.presetPath )
		for file in fileLists :
			if pm.menuItem ( file , ex = True ) :
				pass
			else :
				pm.menuItem ( file , l = file , p = self.optionMenu )
	
	def loadAll ( self , *args ) :
		optionMenu = pm.optionMenu ( 'optMenu' , q = True , v = True )
		path = self.presetPath + '/' + optionMenu
		fileLists = os.listdir ( path )
		node = []
		for file in fileLists :
			data = open ( path + '/' + file )
			nodeData = json.load ( data )
			for node in nodeData :
				if pm.objExists ( node ) :
					color = nodeData [ node ]
					pm.setAttr ( node + '.color' , color )
				else :
					mc.warning ( str ( node ) + ' >> Status : Not Found' )

	def mainUI ( self , *args ) :
		w = 300.00
		rgb = 255.00
		if pm.window ( 'samColorYetiAsset' , exists = True ) :
			pm.deleteUI ( 'samColorYetiAsset' )
		else : pass
		window = pm.window ( 'samColorYetiAsset' ,
			t = 'Color Asset Tool By Sam' , tb = True , mnb = True , mxb = True , s = False , rtf = True )
		pm.window ( 'samColorYetiAsset' , e = True , w = w , h = 10 )
		with window :
			with pm.rowColumnLayout ( nc = 1 ) :
				pm.text ( l = '' , h = 10 )
				with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , w / 1.2 ) , ( 2 , w / 60 ) , ( 3 , w / 6 ) ] ) :
					self.optionMenu = pm.optionMenu ( 'optMenu' )
					pm.text ( l = '' )
					pm.button ( l = '+' , c = self.folderUI , bgc = ( 188 / rgb , 36 / rgb , 60 / rgb ) , al = 'center' )
				pm.text ( h = 5 , l = '')
				pm.button ( l = 'ASSIGN COLOR' , c = self.loadAll , bgc = ( 239 / rgb , 192 / rgb , 80 / rgb ) )
		self.listDict ()

SamColorYeti().mainUI()







