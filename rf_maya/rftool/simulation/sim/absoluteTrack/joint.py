import maya.cmds as mc ;

import rig.dic as abtDic ;
reload ( abtDic ) ;

import rig.controller as abtCtrl ;
reload ( abtCtrl ) ;

import rig.general as abtGen ;
reload ( abtGen ) ;

def copyJnt ( oriJnt = '' , newJntNm = '' , parent = '' ) :

    jntPos = mc.xform ( oriJnt , q = True , ws = True , rp = True ) ;         
    jntRot = mc.xform ( oriJnt , q = True , ws = True , ro = True ) ;
    jntRotOrder = mc.xform ( oriJnt , q = True , ws = True , roo = True ) ;

    if parent == '' :
        
        jnt = mc.createNode ( 'joint' , n = newJntNm ) ;
    
    else :
    
        jnt = mc.createNode ( 'joint' , n = newJntNm ) ;
        mc.parent ( jnt , parent ) ;

    mc.xform ( jnt , ws = True , t = jntPos , ro = jntRot , roo = jntRotOrder ) ;

    mc.makeIdentity ( jnt , apply = True , r = True ) ;
                
    return jnt ;

def insertPosJnt ( jnt = '' ) :
    
    # This function insert pos jnt between the jnt chain you selected. 
    # You only required to select the top of the chain joint. 
    
    if mc.listRelatives ( jnt , ad = True ) == None :
        mc.error ( "Your selected joint does not have a child" ) ;
    
    else :
        
        jntParent = mc.listRelatives ( jnt , p = True ) ; 
        
        oriJntList = mc.listRelatives ( jnt , ad = True ) ;
        oriJntList.append ( jnt ) ;
        oriJntList.reverse () ;
        
        for each in oriJntList :
            if '_jnt' not in each [ -4 : ] :
                mc.error ( "Check if you named your joints properly. This script requires '_jnt' as joint's name suffix. Problem at : %s " % each ) ;
            
            else :
                pass ;
                
        
            parent = mc.listRelatives ( each , p = True ) ;
            
            if parent != None :
                
                mc.parent ( each , w = True ) ;
                
            else :
                pass ;
                
        posJntList = [] ;
       
        for i in range ( 0 , ( len ( oriJntList ) -1 ) ) :
            
            jntNm = oriJntList [ i ].split ( '_jnt' ) ;
            
            posJntNm = '%sPos_jnt'% jntNm [ 0 ] ;
            
            posJnt = mc.duplicate ( oriJntList [i] , n = posJntNm ) ;
            
            posJntList.append ( posJnt [0] ) ; 
                        
        for i in range ( 0 , len ( posJntList ) ) :
        
             mc.parent ( oriJntList [i] , posJntList [i] ) ;
             
             if i != 0 :
                 
                 mc.parent ( posJntList [i] , oriJntList [i-1] ) ;
                 
        mc.parent ( oriJntList [ -1 ] , oriJntList [ -2 ] ) ;
                    
        if jntParent != None :
            
            mc.parent ( posJntList [0] , jntParent ) ;

        return posJntList ;

def fkIkBlend ( jnt = '' , switch = '' , method = 'blendColors' , side = '' , posJnt = False ) :
    # method = blendColors , parentConstraint
    # you only need to select/input the top of the chain joint
    # this function returns [ , , ] ;

    if 'LFT' in jnt :
        side = 'LFT' ;

    elif 'RGT' in jnt :
        side = 'RGT' ;

    else :
        pass ;

    skinJntChain = mc.listRelatives ( jnt , ad = True ) ;
    skinJntChain.append ( jnt ) ;
    skinJntChain.reverse () ;
        
    fkJntChain = [] ;
    ikJntChain = [] ;
    blendColorsList = [] ;

    switchZroGrp = '' ;
        
    if switch == '' :
        
        if '1_jnt' in skinJntChain [0] : 
        
            nm = skinJntChain [0] .split ( '1_jnt' ) [0] ;

            nm = nm.replace ( side , '' ) ;
    
        else :
            
            nm = skinJntChain [0] .split ( '_jnt' ) [0] ;

            nm = nm.replace ( side , '' ) ;
            
        switch = ( abtCtrl.createCurve ( jnt = '' , shape = 'stick' , name = '%sFkIk%s_ctrl' % ( nm , side ) ,
            color = 'brightGreen' , scale = 1 ) [0] ) ;
        
        abtGen.lockAttr ( obj = switch , hide = True ) ;
        mc.addAttr ( switch , ln = 'fkIk' , at = 'double' , min = 0 , max = 1 , keyable = True ) ;
        
        switchZroGrp = mc.group ( em = True , n = '%sFkIkCtrlZro%s_grp' % ( nm , side ) ) ;
        
        mc.parent ( switch , switchZroGrp ) ;

        # reverse
        
        rev = mc.createNode ( 'reverse' , n = '%sFkIk%s_rev' % ( nm , side ) ) ;
        
        mc.connectAttr ( '%s.fkIk' % switch , '%s.ix' % rev ) ; 

    for i in range ( 0 , len ( skinJntChain ) ) :

        newJntNm = skinJntChain[i].split ( '_jnt' ) [0] ;
        newJntNm = newJntNm.replace ( side , '' ) ;
        
        if i == 0 :
            
            fkJnt = copyJnt ( oriJnt = skinJntChain [i] , newJntNm = '%sFk%s_jnt' % ( newJntNm , side ) , parent = '' ) ;
            fkJntChain.append ( fkJnt ) ;   
    
            ikJnt = copyJnt ( oriJnt = skinJntChain [i] , newJntNm = '%sIk%s_jnt' % ( newJntNm , side ) , parent = '' ) ;
            ikJntChain.append ( ikJnt ) ;
        
        else :

            fkJnt = copyJnt ( oriJnt = skinJntChain [i] , newJntNm = '%sFk%s_jnt' % ( newJntNm , side ) , parent = fkJntChain [ i - 1 ] ) ;
            fkJntChain.append ( fkJnt ) ;   
    
            ikJnt = copyJnt ( oriJnt = skinJntChain [i] , newJntNm = '%sIk%s_jnt' % ( newJntNm , side ) , parent = ikJntChain [ i - 1 ] ) ;
            ikJntChain.append ( ikJnt ) ;
    
    if method == 'blendColors' :
        
        for i in range ( 0 , len ( skinJntChain ) ) :
        
            bcNm = skinJntChain [i] .split ( '_jnt' ) [0] ;
            bcNm = bcNm.replace ( side , '' ) ;
            
            transBc = mc.createNode ( 'blendColors' , n = '%sFkIkTrans%s_bc' % ( bcNm , side ) ) ;
            rotBc = mc.createNode ( 'blendColors' , n = '%sFkIkRot%s_bc' % ( bcNm , side ) ) ;
            scaleBc = mc.createNode ( 'blendColors' , n = '%sFkIkScl%s_bc' % ( bcNm , side ) ) ;
            
            att = [ 't' , 'r' , 's' ] ;
            
            for each in att :

                if each == 't' :
                    workingBc = transBc ;

                elif each == 'r' :
                    workingBc = rotBc ;
                    
                elif each == 's' :
                    workingBc = scaleBc ;
                    
                mc.connectAttr ( '%s.%s' % ( fkJntChain [i] , each ) , '%s.color2' % workingBc ) ;
                mc.connectAttr ( '%s.%s' % ( ikJntChain [i] , each ) , '%s.color1' % workingBc ) ;
                mc.connectAttr ( '%s.op' % workingBc , '%s.%s' % ( skinJntChain[i] , each ) ) ;
                mc.connectAttr ( '%s.fkIk' % switch , '%s.blender' % workingBc ) ;
                                        
    elif method == 'parentConstraint' :
    
        rev = mc.listConnections ( switch , type = 'reverse' ) ;

        if len ( rev ) >= 2 :
            
            mc.error ( 'your %s has connection to multiple reverse nodes' % switch ) ;
        
        else :
            
            rev = rev [0] ;
        
        for i in range ( 0 , len ( skinJntChain ) ) :
      
            parCon = mc.parentConstraint ( fkJntChain [i] , ikJntChain [i] , skinJntChain [i] ) ; 
            
            mc.connectAttr ( '%s.ox' % rev , '%s.%sW0' % ( parCon [0] , fkJntChain [ i ] ) ) ;
            mc.connectAttr ( '%s.fkIk' % switch , '%s.%sW1' % ( parCon [0] , ikJntChain [ i ] ) ) ; 
            
            bcNm = skinJntChain [i] .split ( '_jnt' ) [0] ;
            bcNm = bcNm.replace ( side , '' ) ;
            
            scaleBc = mc.createNode ( 'blendColors' , n = '%sFkIkScl%s_bc' % ( bcNm , side ) ) ;
            
            mc.connectAttr ( '%s.fkIk' % switch , '%s.blender' % scaleBc ) ;
            mc.connectAttr ( '%s.s' % fkJntChain [i] , '%s.color2' % scaleBc ) ;  
            mc.connectAttr ( '%s.s' % ikJntChain [i] , '%s.color1' % scaleBc ) ;
            mc.connectAttr ( '%s.op' % scaleBc , '%s.s' % skinJntChain [i] ) ;

    else :

        mc.error ( "Please check your method input. Only 'blendColots' and 'parentConstraint' are valid" ) ;

    fkPosJntChain = [] ;
    ikPosJntChain = [] ;
    
    if posJnt == True :

        fkPosJntChain = insertPosJnt ( jnt = fkJntChain [0] ) ;
        ikPosJntChain = insertPosJnt ( jnt = ikJntChain [0] ) ;
    
    # print switch , switchZroGrp , fkJntChain , ikJntChain ;    
    return [ switch , switchZroGrp , fkJntChain , ikJntChain , fkPosJntChain , ikPosJntChain ] ;

def fkStretchSquash ( ctrlA = '' , ctrlB = '' , stretch = True , squash = True , stretchAxis = 'y' , stretchRev = False ) :
    
    nameA = ctrlA.split ( '_ctrl' ) [0] ;
    nameB = ctrlB.split ( '_ctrl' ) [0] ;
    
    # add attr

    if stretch == True and squash == True :
     
        mc.addAttr ( ctrlA , ln = '_stretchSquash_' , at = 'double' , keyable = True ) ;    
        mc.setAttr ( '%s._stretchSquash_' % ctrlA , lock = True ) ; 

    elif stretch == True and squash == False :

        mc.addAttr ( ctrlA , ln = '_stretch_' , at = 'double' , keyable = True ) ;    
        mc.setAttr ( '%s._stretch_' % ctrlA , lock = True ) ; 

    elif squash == True and stretch == False :

        mc.addAttr ( ctrlA , ln = '_squash_' , at = 'double' , keyable = True ) ;    
        mc.setAttr ( '%s._squash_' % ctrlA , lock = True ) ; 
    
    ctrlAShape = mc.listRelatives ( ctrlA , s = True ) ;
    
    mc.addAttr ( ctrlAShape , ln = 'stretchSquashAmp' , at = 'double' , dv = 0.1 , keyable = True ) ;

    # create amp

    amp = mc.createNode ( 'multiplyDivide' , n = '%sStSqAmp_mdv' % nameA ) ;
    mc.setAttr ( '%s.op' % amp , 1 ) ;
    
    mc.connectAttr ( '%s.stretchSquashAmp' % ctrlAShape [0] , '%s.i2x' % amp ) ;
    mc.connectAttr ( '%s.stretchSquashAmp' % ctrlAShape [0] , '%s.i2y' % amp ) ;
    
    # stretch 

    if stretch == True :

        mc.addAttr ( ctrlA , ln = 'stretch' , at = 'double' , keyable = True ) ;
                
        mc.connectAttr ( '%s.stretch' % ctrlA , '%s.i1x' % amp ) ;

        if stretchRev == True :

            stretchRev = mc.createNode ( 'multiplyDivide' , n = '%sStRev_mdv' % nameA ) ;
            mc.setAttr ( '%s.op' % stretchRev , 1 ) ;
            mc.setAttr ( '%s.i2x' % stretchRev , -1 ) ;
            mc.connectAttr ( '%s.ox' % amp , '%s.i1x' % stretchRev ) ;
            
            mc.connectAttr ( '%s.ox' % stretchRev , '%sCtrlTfm_grp.t%s' % ( nameB , stretchAxis ) ) ;

        else :

            mc.connectAttr ( '%s.ox' % amp , '%sCtrlTfm_grp.t%s' % ( nameB , stretchAxis ) ) ;

    if squash == True :

        mc.addAttr ( ctrlA , ln = 'squash' , at = 'double' , keyable = True ) ;
        
        mc.connectAttr ( '%s.squash' % ctrlA , '%s.i1y' % amp ) ;
    
        pma = mc.createNode ( 'plusMinusAverage' , n = '%sStSq_pma' % nameA ) ;
        mc.setAttr ( '%s.op' % pma , 1 ) ;
        mc.setAttr ( '%s.i2[1].i2y' % pma , 1 ) ;    
    
        mc.connectAttr ( '%s.oy' % amp , '%s.i2[0].i2y' % pma ) ;
    
        mc.connectAttr ( '%s.o2y' % pma , '%s_jnt.sx' % nameA ) ;
        mc.connectAttr ( '%s.o2y' % pma , '%s_jnt.sz' % nameA ) ;

def fkCurl ( ctrlA = '' , ctrlB = '' ) :
    
    nameA = ctrlA.split ( '_ctrl' ) [0] ;
    nameB = ctrlB.split ( '_ctrl' ) [0] ;
   
    # add attr
     
    mc.addAttr ( ctrlA , ln = '_curl_' , at = 'double' , keyable = True ) ;
    mc.setAttr ( '%s._curl_' % ctrlA , lock = True ) ;

    mc.addAttr ( ctrlA , ln = 'curl' , at = 'double' , keyable = True , min = 0 , max = 1 , dv = 1 ) ;
    
    ctrlAShape = mc.listRelatives ( ctrlA , s = True ) ;
    mc.addAttr ( ctrlAShape , ln = 'detailControl' , at = 'bool' , dv = False , keyable = True ) ;

    ctrlBShape = mc.listRelatives ( ctrlB , s = True ) ;

    # connect detail control to controller B visibility 
    
    mc.connectAttr ( '%s.detailControl' % ctrlAShape [0] , '%s.v' % ctrlBShape [0] ) ;
    
    # blendColors
    
    bc = mc.createNode ( 'blendColors' , n = '%sCurl_bc' % nameA ) ;
    mc.connectAttr ( '%s.curl' % ctrlA , '%s.blender' % bc ) ;
    
    mc.connectAttr ( '%s_jnt.r' % nameA , '%s.color1' % bc ) ;
    
    mc.connectAttr ( '%s.op' % bc , '%sCtrlTfm_grp.r' % nameB ) ;

def localWorld ( ctrl = '' , orientParent = 0 ) :
    # return [ localGrp , worldGrp ] ;

    mc.addAttr ( ctrl , ln = '_localWorld_' , at = 'double' , keyable = True ) ;
    mc.setAttr ( '%s._localWorld_' % ctrl , lock = True ) ;

    mc.addAttr ( ctrl , ln = 'localWorld' , at = 'double' , min = 0 , max = 1 , keyable = True ) ;
    
    rev = mc.createNode ( 'reverse' , n = 'chestFkLocWor_rev' ) ;
    
    mc.connectAttr ( '%s.localWorld' % ctrl , '%s.ix' % rev ) ;
 
    animGrp = '' ;
    
    if mc.objExists ( 'anim_grp' ) == True :
        animGrp = 'anim_grp' ; 
   
    counter = 1 ;   
    parent = mc.listRelatives ( ctrl , parent = True ) [0] ;
    
    if 'Zro_grp' not in parent and counter < 6 :
    
        parent = mc.listRelatives ( parent , parent = True ) [0] ;
    
        print 'searching zroGrp attempt [%s] %s ' % ( counter , parent ) ;
                
        if counter == 5 :
           
            mc.error ( "check your zero grp name and hierarchy, this function requires 'Zro_grp' as grp name suffix" ) ;
            
        counter = counter + 1 ;
    
    zroGrp = parent ;
    zroGrpNm = zroGrp.split ( 'Zro_grp' ) [0] ;
    zroGrpParent = mc.listRelatives ( parent , parent = True ) ;

    localGrp = abtGen.copyGrp ( oriGrp = zroGrp , newGrpNm = '%sLoc_grp' % zroGrpNm ) ;
    worldGrp = abtGen.copyGrp ( oriGrp = zroGrp , newGrpNm = '%sWor_grp' % zroGrpNm ) ;

    if zroGrpParent != None :
        mc.parent ( localGrp , worldGrp , zroGrpParent ) ;
                    
    # organization 
    
    if animGrp != '' :
        
        if orientParent == 1 :
            
            mc.parentConstraint ( animGrp , worldGrp , mo = True ) ;
    
        if orientParent == 0 :
    
            mc.orientConstraint ( animGrp , worldGrp , mo = True ) ;
        
    # contraint type 
    
    if orientParent == 1 :
        
        parCon = mc.parentConstraint ( localGrp , worldGrp , zroGrp , mo = True ) [0] ;
        
        mc.connectAttr ( '%s.ox' % rev , '%s.%sW0' % ( parCon , localGrp ) ) ; 
        mc.connectAttr ( '%s.localWorld' % ctrl , '%s.%sW1' % ( parCon , worldGrp ) ) ; 
            
    elif orientParent == 0 :

        orientCon = mc.orientConstraint ( localGrp , worldGrp , zroGrp , mo = True ) [0] ;
        
        mc.connectAttr ( '%s.ox' % rev , '%s.%sW0' % ( orientCon , localGrp ) ) ; 
        mc.connectAttr ( '%s.localWorld' % ctrl , '%s.%sW1' % ( orientCon , worldGrp ) ) ; 
    
    else :
        
        mc.error ( 'kwarg orientParent only accept int value from 0 - 1' ) ;

    return [ localGrp , worldGrp ] ;
