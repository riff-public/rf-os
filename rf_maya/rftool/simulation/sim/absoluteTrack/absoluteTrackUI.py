##############################################################################################################################
# absolute track beta 4.0
##############################################################################################################################

'''

wrote for maya 2015

project start     : October 30th, 2013
alpha v0.1        : November 3rd, 2013
alpha v0.2        : November 4th, 2013
beta v1.0         : -
beta v2.0         : September 30th, 2014
beta v3.0         : July 5th, 2017

beta v3.0 notes:
- make the script standalone for new shelf (riff)

beta v2.1 notes:
- scalable UI
- fixed cycle bug
- known issues: prsp cam on off

beta v2.0 notes:
- script overhauled 
- UI with progress bar implemented
- panel on off features 

alpha v0.2 notes :

- rewrite the whole script for optimization sake
- only bake result for main_loc
- separate function for rotate groups
- check object rotation order
- animController function
- locked attr ( nodes ) and turn non-interactive object visibility off
- changed CIN/COUT group name to BWD/FWD to prevent user confusion

'''

import maya.cmds as mc ;
import maya.mel as mm ;
import sys ;
import pymel.core as pm ;

import sim.absoluteTrack.mayaUI as abtMul ;
reload ( abtMul ) ;

import sim.absoluteTrack.mayaLibrary as abtMl ;
reload ( abtMl ) ;

import sim.absoluteTrack.general as abtGl ;
reload ( abtGl ) ;

import sim.absoluteTrack.panel as abtPnl ;
reload ( abtPnl ) ;

import sim.absoluteTrack.controller as abtCtrl ;
reload ( abtCtrl ) ;

# procs 

def timeRangeQuery () :
    
    startFrame = mc.playbackOptions ( q = True , minTime = True ) ; 
    endFrame = mc.playbackOptions ( q = True , maxTime = True ) ;
    
    return ( startFrame , endFrame ) ;
        
# main procs

def rotTrack ( obj = '' , startFrame = 1 , endFrame = 100 , defaultFrame = 970 ) :
    
    progressbar = abtMul.progressBar ( progressBar = 'absTrack_progressBar' ) ;
    
    obj = mc.ls ( sl = True ) [0] ;
    
    name = str ( obj ) ;
    
    axes = [ 'x' , 'y' , 'z' ] ;
    
    if ( '_riv' in name ) :
        
        name = obj.split ( '_riv' ) [0] ;
            
    roo = mc.xform ( obj , q = True , ws = True , roo = True ) ;

    roo = 'xyz' ;
        
    # dummy rivet
    
    dmmyRiv = mc.listRelatives ( mc.createNode ( 'locator' ) , p = True ) ;
    dmmyRiv = mc.rename ( dmmyRiv , '%s_dmmyRiv' % name ) ;
    
    mc.xform ( dmmyRiv , roo = roo ) ;
    
    mc.connectAttr ( '%s.translate' % obj , '%s.translate' % dmmyRiv ) ;
    mc.connectAttr ( '%s.rotate' % obj , '%s.rotate' % dmmyRiv ) ;
    
    # >>>
    progressbar.update ( value = 20 ) ;
    
        # bake key
        
    startFrame = startFrame - 50 ;
    endFrame = endFrame + 50 ;

    sb = pm.floatField ( 'sampleBy_intput' , q = True , v = True ) ;
    
    try : 
    
        showList = abtPnl.activePanelShowQuery () ;
    
    except :
        
        try : 
            abtPnl.activePanelAllDisplay ( switch = False ) ;
            mc.bakeResults ( dmmyRiv , at = [ "tx" , "ty" , "tz" , "rx" , "ry" , "rz" ] , t = ( startFrame , endFrame ) , sb = sb , simulation = True , minimizeRotation = True ) ;        
            # abtPnl.activePanelAllDisplay ( switch = True ) ;
        
        except : 
            mc.modelEditor ( 'modelPanel4' , e = True , allObjects = False ) ;
            mc.bakeResults ( dmmyRiv , at = [ "tx" , "ty" , "tz" , "rx" , "ry" , "rz" ] , t = ( startFrame , endFrame ) , sb = sb , simulation = True , minimizeRotation = True ) ;
            #mc.modelEditor ( modelPanel4 , e = True , allObjects = True ) ;
            
    else :
    
        abtPnl.activePanelAllDisplay ( switch = False ) ;
        mc.bakeResults ( dmmyRiv , at = [ "tx" , "ty" , "tz" , "rx" , "ry" , "rz" ] , t = ( startFrame , endFrame ) , sb = sb , simulation = True , minimizeRotation = True ) ;
        abtPnl.activePanelShowSet ( showList ) ;
        
    abtGl.lockAttr ( dmmyRiv , t = False , r = False , hide = True ) ;
    
    mc.currentTime ( defaultFrame ) ;
    
    # >>>
    progressbar.update ( value = 70 ) ;

    mc.disconnectAttr ( '%s.translate' % obj , '%s.translate' % dmmyRiv ) ;
    mc.disconnectAttr ( '%s.rotate' % obj , '%s.rotate' % dmmyRiv ) ;
    
    #################
    'global no touch'
    #################
    globalNoTouch = mc.group ( em = True , n = '%s_track_noTouch_grp' % name ) ;
    mc.xform ( globalNoTouch , roo = roo ) ;
    mc.setAttr ( '%s.v' % globalNoTouch , 0 ) ;
    abtGl.lockAttr ( globalNoTouch ) ;
    mc.parent ( dmmyRiv , globalNoTouch ) ;
    
    ###########
    ''' BWD ''' 
    ###########
        
        # ROT: transpose and decompose
    
    rotTpm = mc.createNode ( 'transposeMatrix' , n = '%s_BWD_rotRev_tpm' % name ) ; 
    mc.connectAttr ( '%s.matrix' % dmmyRiv , '%s.inputMatrix' % rotTpm ) ;    
    
    rotDcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_rotRev_dcm' % name ) ;
    mc.connectAttr ( '%s.outputMatrix' % rotTpm , '%s.inputMatrix' % rotDcm ) ;
    
        # ROT: BWD

    rotBWD = mc.group ( em = True , n = '%s_BWD_rotBWD_grp' % name ) ;
    mc.xform ( rotBWD , roo = roo ) ;
    abtGl.lockAttr ( rotBWD , r = False , hide = True ) ;
    abtGl.lockAttr ( rotBWD , t = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputRotate' % rotDcm , '%s.r' % rotBWD ) ;


    ''''''
    
    rotBWD_dmmy = mc.group ( em = True , n = '%s_BWD_rotBWD_dmmyGrp' % name ) ;
    mc.xform ( rotBWD_dmmy , roo = roo ) ; 
    abtGl.lockAttr ( rotBWD_dmmy , r = False , hide = True ) ;
    abtGl.lockAttr ( rotBWD_dmmy , t = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputRotate' % rotDcm , '%s.r' % rotBWD_dmmy ) ;
    
    
    transBWD_ref = mc.group ( em = True , n = '%s_BWD_transBWD_dmmyGrp' % name ) ;
    mc.xform ( transBWD_ref , roo = roo ) ;
    mc.setAttr ( '%s.v' % transBWD_ref , 0 ) ;
    abtGl.lockAttr ( transBWD_ref ) ;
    mc.parent ( transBWD_ref , rotBWD_dmmy ) ;
    mc.connectAttr ( '%s.t' % dmmyRiv , '%s.t' % transBWD_ref ) ;
    mc.connectAttr ( '%s.r' % dmmyRiv , '%s.r' % transBWD_ref ) ;
    
    transBWD_ref_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_transBWD_ref_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % transBWD_ref , '%s.inputMatrix' % transBWD_ref_dcm ) ;
    mc.connectAttr ( '%s.rotateOrder' % transBWD_ref , '%s.inputRotateOrder' % transBWD_ref_dcm ) ;
    
    transBWD_ref_rev = mc.createNode ( 'multiplyDivide' , n = '%s_BWD_transBWD_ref_rev_mdv' % name ) ;
    mc.connectAttr ( '%s.outputTranslate' % transBWD_ref_dcm , '%s.i1' % transBWD_ref_rev ) ;
    for each in axes :
        mc.setAttr ( '%s.i2%s' % ( transBWD_ref_rev , each ) , -1 ) ; 
        
        # TRANS : BWD
        
    transBWD = mc.group ( em = True , n = '%s_BWD_transBWD_grp' % name ) ;
    mc.xform ( transBWD , roo = roo ) ;
    abtGl.lockAttr ( transBWD , t = False , hide = True ) ;
    abtGl.lockAttr ( transBWD , r = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.o' % transBWD_ref_rev , '%s.t' % transBWD ) ;
    mc.parent ( rotBWD , transBWD ) ;
                    
        # controller
        
    ctrlZro = mc.group ( em = True , n = '%s_BWD_ctrlZro_grp' % name ) ;
    mc.xform ( ctrlZro , roo = roo ) ;

    mainCtrl = abtCtrl.createCurve ( jnt = '' , shape = 'fourDirectionalArrow' , name = '%s_BWD_ctrl' % name , 
        gimble = False , color = 'lightBlue' , joint = True , scale = 1*2 ) [0] ;
    mc.xform ( mainCtrl , roo = roo ) ;
    abtGl.lockAttr ( mainCtrl , t = False , r = False , hide = True ) ;

    gmblCtrl = abtCtrl.createCurve ( jnt = '' , shape = 'star' , name = '%s_BWD_gmblCtrl' % name , 
        gimble = False , color = 'white' , joint = False , scale = 3.3*2 ) [0] ;
    mc.xform ( gmblCtrl , roo = roo ) ;
    abtGl.lockAttr ( gmblCtrl , t = False , r = False , hide = True ) ;

    mainCtrlShape = mc.listRelatives ( mainCtrl , shapes = True ) [0] ;
    mc.addAttr ( mainCtrlShape , ln = 'gimbalControl' , at = 'bool' , keyable = True ) ;
    mc.connectAttr ( '%s.gimbalControl' % mainCtrlShape , '%sShape.v' % gmblCtrl ) ;

        # dmmyRiv dmmy
        
    dmmyRivRefGrp = mc.group ( em = True , n = '%s_dmmyRiv_ref_grp' % name ) ; 
    mc.xform ( dmmyRivRefGrp , roo = roo ) ;
    mc.setAttr ( '%s.v' % dmmyRivRefGrp , 0 ) ;
    abtGl.lockAttr ( dmmyRivRefGrp ) ;
    mc.parent ( dmmyRivRefGrp , dmmyRiv ) ;
    
    dmmyRiv_ref_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_dmmyRiv_ref_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % dmmyRivRefGrp , '%s.inputMatrix' % dmmyRiv_ref_dcm ) ;
    mc.connectAttr ( '%s.rotateOrder' % dmmyRivRefGrp , '%s.inputRotateOrder' % dmmyRiv_ref_dcm ) ;
    
            # default tfm
        
    defaultTfm = mc.group ( em = True , n = '%s_BWD_defaultTfm_grp' % name ) ;
    mc.xform ( defaultTfm , roo = roo ) ;
    abtGl.lockAttr ( defaultTfm , t = False , r = False , hide = True ) ;
    
            # AMP

    mc.addAttr ( mainCtrl , ln = '__amplifier__' , at = 'double' , keyable = True ) ;
    mc.setAttr ( '%s.__amplifier__' % mainCtrl , lock = True ) ;
    
    for each in axes :
        mc.addAttr ( mainCtrl , ln = 'trans%s' % each.upper() , at = 'double' , keyable = True , dv = 1 ) ;
        
        temptBc = mc.createNode ( 'blendColors' , n = '%s_BWD_t%sSwitch_bc' % ( name , each ) ) ;
        
        temptTrans = mc.getAttr ( '%s.t%s' % ( dmmyRiv , each ) ) ; 
         
        mc.connectAttr ( '%s.trans%s' % ( mainCtrl , each.upper() ) , '%s.blender' % temptBc ) ;
                
        mc.connectAttr ( '%s.outputTranslate%s' % ( dmmyRiv_ref_dcm , each.upper() ) , '%s.c1r' % temptBc ) ;
        mc.setAttr ( '%s.c2r' % temptBc , temptTrans ) ;

        mc.connectAttr ( '%s.outputR' % temptBc , '%s.t%s' % ( defaultTfm , each ) ) ;
                
    ####

    for each in axes :
        mc.addAttr ( mainCtrl , ln = 'rot%s' % each.upper() , at = 'double' , keyable = True , dv = 1 ) ;
        
        temptBc = mc.createNode ( 'blendColors' , n = '%s_BWD_r%sSwitch_bc' % ( name , each ) ) ;
        
        temptRot = mc.getAttr ( '%s.r%s' % ( dmmyRiv , each ) ) ; 
         
        mc.connectAttr ( '%s.rot%s' % ( mainCtrl , each.upper() ) , '%s.blender' % temptBc ) ;
                
        mc.connectAttr ( '%s.outputRotate%s' % ( dmmyRiv_ref_dcm , each.upper() ) , '%s.c1r' % temptBc ) ;
        mc.setAttr ( '%s.c2r' % temptBc , temptRot ) ;

        mc.connectAttr ( '%s.outputR' % temptBc , '%s.r%s' % ( defaultTfm , each ) ) ;
            
    mc.parent ( transBWD , gmblCtrl ) ;    
    mc.parent ( gmblCtrl , mainCtrl ) ;
    mc.parent ( mainCtrl , ctrlZro ) ;

    #################################################    

    #mc.parent ( ctrlZro , defaultTfm ) ;
    ctrlZro_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_ctrlZro_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % defaultTfm , '%s.inputMatrix' % ctrlZro_dcm ) ; 
    mc.connectAttr ( '%s.rotateOrder' % defaultTfm , '%s.inputRotateOrder' % ctrlZro_dcm ) ; 
    
    mc.connectAttr ( '%s.outputTranslate' % ctrlZro_dcm , '%s.t' % ctrlZro ) ;
    mc.connectAttr ( '%s.outputRotate' % ctrlZro_dcm , '%s.r' % ctrlZro ) ;
    abtGl.lockAttr ( ctrlZro ) ;
    
        # organization 
        
    BWD_noTouch = mc.group ( em = True , n = '%s_BWD_noTouch_grp' % name ) ;
    mc.xform ( BWD_noTouch , roo = roo ) ;
    # mc.setAttr ( '%s.v' % BWD_noTouch , 0 ) ;
    abtGl.lockAttr ( BWD_noTouch ) ;
    
    mc.parent ( rotBWD_dmmy , BWD_noTouch );
    mc.parent ( defaultTfm , BWD_noTouch ) ;
    
    abtGl.lockAttr ( defaultTfm , s = False , v = False ) ;
    
        # BWD group prep

    BWD_dmmy = mc.group ( em = True , n = '%s_BWD_BWDgrp_dmmyGrp' % name ) ;
    mc.xform ( BWD_dmmy , roo = roo ) ;
    mc.setAttr ( '%s.v' % BWD_dmmy , 0 ) ;
    abtGl.lockAttr ( BWD_dmmy ) ;
    mc.parent ( BWD_dmmy , rotBWD ) ;
    
    BWD_dmmy_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_BWDgrp_dmmy_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % BWD_dmmy , '%s.inputMatrix' % BWD_dmmy_dcm ) ;
    mc.connectAttr ( '%s.rotateOrder' % BWD_dmmy , '%s.inputRotateOrder' % BWD_dmmy_dcm ) ;
    
    ########### 
    ''' FWD ''' 
    ###########
    # >>>
    progressbar.update ( value = 90 ) ;
    
        # ref
        
            # under gimbal control ref
        
    gmblCtrl_ref = mc.group ( em = True , n = '%s_BWD_gmblCtrl_dmmyGrp' % name ) ;
    mc.xform ( gmblCtrl_ref , roo = roo ) ;
    mc.setAttr ( '%s.v' % gmblCtrl_ref , 0 ) ;
    abtGl.lockAttr ( gmblCtrl_ref ) ;
    mc.parent ( gmblCtrl_ref , gmblCtrl ) ;

    gmblCtrl_dmmy_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_gmblCtrl_dmmy_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % gmblCtrl_ref , '%s.inputMatrix' % gmblCtrl_dmmy_dcm ) ;
    mc.connectAttr ( '%s.rotateOrder' % gmblCtrl_ref , '%s.inputRotateOrder' % gmblCtrl_dmmy_dcm ) ;
      
    gmblCtrl_dmmy_dcm_ref = mc.group ( em = True , n = '%s_BWD_gmblCtrl_dmmy_dcm_ref_grp' % name ) ;
    mc.xform ( gmblCtrl_dmmy_dcm_ref , roo = roo ) ;
    mc.connectAttr ( '%s.outputTranslate' % gmblCtrl_dmmy_dcm , '%s.t' % gmblCtrl_dmmy_dcm_ref ) ;
    mc.connectAttr ( '%s.outputRotate' % gmblCtrl_dmmy_dcm , '%s.r' % gmblCtrl_dmmy_dcm_ref ) ;
        
    FWD_rotTpm = mc.createNode ( 'transposeMatrix' , n = '%s_FWD_rotRev_tpm' % name ) ; 
    mc.connectAttr ( '%s.matrix' % gmblCtrl_dmmy_dcm_ref , '%s.inputMatrix' % FWD_rotTpm ) ;    
    
    FWD_rotDcm = mc.createNode ( 'decomposeMatrix' , n = '%s_FWD_rotRev_dcm' % name ) ;
    mc.connectAttr ( '%s.outputMatrix' % FWD_rotTpm , '%s.inputMatrix' % FWD_rotDcm ) ;
    
    FWD_rotBWD_dmmy = mc.group ( em = True , n = '%s_FWD_rotBWD_dmmyGrp' % name ) ;
    mc.xform ( FWD_rotBWD_dmmy , roo = roo ) ; 
    abtGl.lockAttr ( FWD_rotBWD_dmmy , r = False , hide = True ) ;
    abtGl.lockAttr ( FWD_rotBWD_dmmy , t = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputRotate' % FWD_rotDcm , '%s.r' % FWD_rotBWD_dmmy ) ;
    
    FWD_transBWD_ref = mc.group ( em = True , n = '%s_FWD_transBWD_dmmyGrp' % name ) ;
    mc.xform ( FWD_transBWD_ref , roo = roo ) ;
    mc.setAttr ( '%s.v' % FWD_transBWD_ref , 0 ) ;
    abtGl.lockAttr ( FWD_transBWD_ref ) ;
    mc.parent ( FWD_transBWD_ref , FWD_rotBWD_dmmy ) ;
    mc.connectAttr ( '%s.t' % gmblCtrl_dmmy_dcm_ref , '%s.t' % FWD_transBWD_ref ) ;
    mc.connectAttr ( '%s.r' % gmblCtrl_dmmy_dcm_ref , '%s.r' % FWD_transBWD_ref ) ;
    
    FWD_transBWD_ref_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_FWD_transBWD_ref_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % FWD_transBWD_ref , '%s.inputMatrix' % FWD_transBWD_ref_dcm ) ;
    mc.connectAttr ( '%s.rotateOrder' % FWD_transBWD_ref , '%s.inputRotateOrder' % FWD_transBWD_ref_dcm ) ;
    
    FWD_transBWD_ref_rev = mc.createNode ( 'multiplyDivide' , n = '%s_FWD_transBWD_ref_rev_mdv' % name ) ;
    mc.connectAttr ( '%s.outputTranslate' % FWD_transBWD_ref_dcm , '%s.i1' % FWD_transBWD_ref_rev ) ;
    for each in axes :
        mc.setAttr ( '%s.i2%s' % ( FWD_transBWD_ref_rev , each ) , -1 ) ; 

    FWD_transBWD = mc.group ( em = True , n = '%s_FWD_transBWD_grp' % name ) ;
    mc.xform ( FWD_transBWD , roo = roo ) ;
    abtGl.lockAttr ( FWD_transBWD , t = False , hide = True ) ;
    abtGl.lockAttr ( FWD_transBWD , r = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.o' % FWD_transBWD_ref_rev , '%s.t' % FWD_transBWD ) ;
    
    FWD_rotBWD = mc.group ( em = True , n = '%s_FWD_rotBWD_grp' % name ) ;
    mc.xform ( FWD_rotBWD , roo = roo ) ; 
    abtGl.lockAttr ( FWD_rotBWD , r = False , hide = True ) ;
    abtGl.lockAttr ( FWD_rotBWD , t = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputRotate' % FWD_rotDcm , '%s.r' % FWD_rotBWD ) ;
              
    mc.parent ( FWD_rotBWD , FWD_transBWD ) ;
    
    # >>>
    progressbar.update ( value = 90 ) ;
    
        # default tfm
    
    fwdDefaultTfm = mc.group ( em = True , n = '%s_FWD_defaultTfm_grp' % name ) ;
    mc.xform ( fwdDefaultTfm , roo = roo ) ;
    mc.connectAttr ( '%s.t' % dmmyRiv , '%s.t' % fwdDefaultTfm ) ; 
    mc.connectAttr ( '%s.r' % dmmyRiv , '%s.r' % fwdDefaultTfm ) ;
    
    abtGl.lockAttr ( fwdDefaultTfm ) ;
    
    mc.parent ( FWD_transBWD , fwdDefaultTfm ) ;
    
        # organization 
        
    FWD_noTouch = mc.group ( em = True , n = '%s_FWD_noTouch_grp' % name ) ;
    mc.xform ( FWD_noTouch , roo = roo ) ;
    abtGl.lockAttr ( FWD_noTouch ) ;
    
    mc.parent ( FWD_rotBWD_dmmy , FWD_noTouch );
    mc.parent ( fwdDefaultTfm , FWD_noTouch ) ;
    mc.parent ( gmblCtrl_dmmy_dcm_ref , FWD_noTouch ) ;
    
    abtGl.lockAttr ( gmblCtrl_dmmy_dcm_ref ) ;
    abtGl.lockAttr ( defaultTfm , s = False , v = False ) ;
    
        # FWD group prep

    FWD_dmmy = mc.group ( em = True , n = '%s_FWD_FWDgrp_dmmyGrp' % name ) ;
    mc.xform ( FWD_dmmy , roo = roo ) ;
    mc.setAttr ( '%s.v' % FWD_dmmy , 0 ) ;
    abtGl.lockAttr ( FWD_dmmy ) ;
    mc.parent ( FWD_dmmy , FWD_rotBWD ) ;
    
    FWD_dmmy_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_FWD_FWDgrp_dmmy_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % FWD_dmmy , '%s.inputMatrix' % FWD_dmmy_dcm ) ;
    mc.connectAttr ( '%s.rotateOrder' % FWD_dmmy , '%s.inputRotateOrder' % FWD_dmmy_dcm ) ;
       
    ######################## 
    ''' user interaction ''' 
    ########################
    # >>>
    progressbar.update ( value = 95 ) ;
    
    mc.parent ( BWD_noTouch , globalNoTouch ) ;    
    mc.parent ( FWD_noTouch , globalNoTouch ) ;
         
    # BWD grp
                    
    BWD_grp = mc.group ( em = True , n = '%s_BWD' % name ) ;
    mc.xform ( BWD_grp , roo = roo ) ;
    abtGl.lockAttr ( BWD_grp , t = False , r = False , v = False , hide = True ) ;
    abtGl.lockAttr ( BWD_grp , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputTranslate' % BWD_dmmy_dcm , '%s.t' % BWD_grp ) ;  
    mc.connectAttr ( '%s.outputRotate' % BWD_dmmy_dcm , '%s.r' % BWD_grp ) ;
    
    # FWD grp
                    
    FWD_grp = mc.group ( em = True , n = '%s_FWD' % name ) ;
    mc.xform ( FWD_grp , roo = roo ) ;
    abtGl.lockAttr ( FWD_grp , t = False , r = False , v = False , hide = True ) ;
    abtGl.lockAttr ( FWD_grp , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputTranslate' % FWD_dmmy_dcm , '%s.t' % FWD_grp ) ;  
    mc.connectAttr ( '%s.outputRotate' % FWD_dmmy_dcm , '%s.r' % FWD_grp ) ;
    
    ### >>>
    
    progressbar.delete ( ) ;

    progressbar.insert ( parent = 'absTrack_progressBarLayout' ) ;

    mc.formLayout ( 'absTrack_progressBarLayout' , e = True ,
        af = (
        ( 'absTrack_progressBar' , 'left' , 0 ) , ( 'absTrack_progressBar' , 'right' , 0 ) )
        ) ;
            
    
def absoluteTrackUI ( *args ) :

    def filler ( *args ) :
        pm.text ( label = '' ) ;

    def separator ( *args ) :
        pm.separator ( vis = False , h = 5 ) ;

    width = 300.00 ;
   
    if pm.window ( 'abTrack_UI' , exists = True ) :
        pm.deleteUI ( 'abTrack_UI' ) ;
    else : pass ;
   
    window = pm.window ( 'abTrack_UI', title = "absolute track v3.0" ,
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    
    pm.window ( 'abTrack_UI' , e = True , w = width ) ;
    with window :
    
        with pm.rowColumnLayout ( nc = 1 ) :

            with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/3 ) , ( 2 , width/3 ) , ( 3 , width/3 ) ] ) :
                startFrameLbl = pm.text ( 'absTrack_startFrameLbl' , l = 'start frame' , h = 15 ) ;
                endFrameLbl = pm.text ( 'absTrack_endFrameLbl' , l = 'end frame' , h = 15 ) ;
                defaultPosLbl = pm.text ( 'absTrack_defaultPosLbl' , l = 'default pos' , h = 15 ) ;

                startFrame = pm.intField ( 'absTrack_startFrame' , v = timeRangeQuery () [0] , h = 25 ) ;
                endFrame = pm.intField ( 'absTrack_endFrame' , v = timeRangeQuery () [1] , h = 25 ) ;
                defaultPos = pm.intField ( 'absTrack_defaultPos' , v = timeRangeQuery () [0] , h = 25 ) ;

            separator ( ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/3*2 ) , ( 2 , width/3 ) ] ) :
                filler ( ) ;

                with pm.rowColumnLayout ( nc = 1 , w = width/3 ) :
                    pm.text ( label = 'sampleBy' , w = width/3 ) ;
                    pm.floatField ( 'sampleBy_intput' , precision = 2 , v = 1 , w = width/3 ) ;

            separator ( ) ;

            button = pm.button ( 'absTrack_button' , l = 'track' , c = abtMl.Callback ( trackButtonCmd ) , h = 25 ) ;

            
            progressBarLayout = pm.formLayout ( 'absTrack_progressBarLayout' , h = 25) ;
            with progressBarLayout :

                progressBar = pm.progressBar ( 'absTrack_progressBar' , h = 25 , w = width ) ;
                

    window.show () ;

def trackButtonCmd () :
    rotTrack ( startFrame = mc.intField ( 'absTrack_startFrame' , q = True , v = True ) ,
        endFrame = mc.intField ( 'absTrack_endFrame' , q = True , v = True ) ,
        defaultFrame = mc.intField ( 'absTrack_defaultPos' , q = True , v = True ) ) ;

#absoluteTrackUI () ;
