selection = pm.ls ( sl = True ) ;

group = pm.group ( selection ) ;
currentFrame = pm.currentTime ( q = True ) ;
currentFrame = str ( int ( currentFrame ) ) ;
group.rename ( 'f' + currentFrame + '_' + '1'.zfill(3) ) ;


for axis in [ 'x' , 'y' , 'z' ] :
    for attr in [ 't' , 'r' , 's' ] : 
        exec ( 'group.{attr}{axis}.set( lock = True , keyable = False )'.format ( attr = attr , axis = axis ) ) ;
group.v.set ( lock = True , keyable = False ) ;
    
group.addAttr ( 'envelope' , keyable = True , attributeType = 'float' , max = 1 , min = 0 , dv = 1 ) ;

for each in selection :
    connections = each.connections() ;
    for connection in connections :
        if connection.nodeType() == 'cluster' :
            connection = pm.general.PyNode ( connection ) ;
            group.envelope >> connection.envelope ; 