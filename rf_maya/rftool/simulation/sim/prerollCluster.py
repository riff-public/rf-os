import pymel.core as pm ;

def makeName_cmd ( target = '' , stripLastElm = True , *args ) :

    targetSplit_list = target.split ( '_' ) ;
    
    if stripLastElm == True :
        targetSplit_list.remove ( targetSplit_list[-1] ) ;
    else : pass ;

    name = targetSplit_list[0] ;
    
    for each in targetSplit_list[1:] :
        name += ( each[0].upper() + each[1:] ) ;
    
    return name ;

def run ( *args ) :

	selection = pm.ls ( sl = True ) ;

	if len ( selection ) == 0 :
		pass ;
	else :

		##################
		''' prerollCluster_GRP '''
		##################
		if pm.objExists ( 'prerollCluster_GRP' ) != True :
			prerollCluster_GRP = pm.group ( n = 'prerollCluster_GRP' , em = True , w = True ) ;
			pm.parent ( prerollCluster_GRP , 'LilySRU_Grp_LOC' ) ;
		else :
			prerollCluster_GRP = pm.general.PyNode ( 'prerollCluster_GRP' ) ;

		##################
		''' cluster '''
		##################
		nm = '' ;

		for i in range ( 0 , len ( selection ) ) :
			if i == 0 :
				nm += makeName_cmd(target=selection[i]) ;
			else :
				nm += ( makeName_cmd(target=selection[i])[0].upper() + makeName_cmd(target=selection[i])[1:] ) ;

		pm.select ( selection ) ;

		clusterElem = pm.mel.eval ( 'newCluster "-envelope 1"' ) ;
		# [u'cluster1', u'cluster1Handle']
		
		cluster = clusterElem [0] ;
		clusterHandle = clusterElem [1] ;

		cluster = pm.rename ( cluster , nm + '_cluster' ) ;
		clusterHandle = pm.rename ( clusterHandle , nm + '_clusterHandle' ) ;

		cluster.relative.set ( 1 ) ;

		group = pm.group ( em = True , n = nm + 'Cluster_grp' ) ;

		pm.parent ( clusterHandle , group ) ;

		pm.parent ( group , prerollCluster_GRP ) ;

		##################
		''' reverse '''
		##################

		reverse = pm.createNode ( 'reverse' , n = nm + '_rev' ) ;

		BSH = pm.general.PyNode ( 'CIN_COL_BSH' ) ;
		BSHAttr = pm.aliasAttr ( BSH , q = True ) [0] ;

		pm.connectAttr ( BSH + '.' + BSHAttr , reverse +'.ix' ) ;
		reverse.ox >> cluster.envelope ;

		##################
		''' select cluster '''
		##################

		pm.select ( clusterHandle ) ;