import pymel.core as pm ;

class NucleusRig ( object ) :

    def __init__ ( self , nucleus ) :

        self.nucleus = pm.general.PyNode ( nucleus ) ;

        ### curve points ###
        self.mainCtrlPoint = [ ( -5.96046e-007 , 0 , 20.000002 ) , ( 0 , 0 , 0 ) , ( -5.96046e-007 , 0 , 20.000002 ) , ( 6.180339 , 0 , 19.021132 ) , ( 11.755705 , 0 , 16.180342 ) , ( 16.18034 , 0 , 11.755706 ) , ( 19.021132 , 0 , 6.18034 ) , ( 20 , 0 , 0 ) , ( 0 , 0 , 0 ) , ( 20 , 0 , 0 ) , ( 19.021143 , 0 , -6.180344 ) , ( 16.180351 , 0 , -11.755713 ) , ( 11.755713 , 0 , -16.180349 ) , ( 6.180343 , 0 , -19.021141 ) , ( 0 , 0 , -20.00001 ) , ( 0 , 0 , 0 ) , ( 0 , 0 , -20.00001 ) , ( -6.180343 , 0 , -19.021139 ) , ( -11.75571 , 0 , -16.180346 ) , ( -16.180346 , 0 , -11.755709 ) , ( -19.021135 , 0 , -6.180341 ) , ( -20.000004 , 0 , 0 ) , ( 0 , 0 , 0 ) , ( -20.000004 , 0 , 0 ) , ( -19.021135 , 0 , 6.180341 ) , ( -16.180344 , 0 , 11.755707 ) , ( -11.755707 , 0 , 16.180342 ) , ( -6.180341 , 0 , 19.021133 ) , ( -5.96046e-007 , 0 , 20.000002 ) , ( 0 , 0 , 0 ) , ( 0 , -40 , 0 ) , ( -1.24252e-007 , -40 , 4.16921 ) , ( 0 , -44.169209 , 0 ) , ( -4.169211 , -40 , 0 ) , ( 0 , -40 , 0 ) , ( 0 , -40 , -4.169212 ) , ( 0 , -44.169209 , 0 ) , ( 4.16921 , -40 , 0 ) , ( 0 , -40 , 0 ) , ( 4.16921 , -40 , 0 ) , ( 3.965157 , -40 , -1.288357 ) , ( 3.372964 , -40 , -2.450602 ) , ( 2.450602 , -40 , -3.372964 ) , ( 1.288357 , -40 , -3.965156 ) , ( 0 , -40 , -4.169212 ) , ( -1.288357 , -40 , -3.965156 ) , ( -2.450601 , -40 , -3.372963 ) , ( -3.372963 , -40 , -2.450601 ) , ( -3.965155 , -40 , -1.288357 ) , ( -4.169211 , -40 , 0 ) , ( -3.965155 , -40 , 1.288357 ) , ( -3.372962 , -40 , 2.450601 ) , ( -2.450601 , -40 , 3.372962 ) , ( -1.288357 , -40 , 3.965155 ) , ( -1.24252e-007 , -40 , 4.16921 ) , ( 1.288357 , -40 , 3.965155 ) , ( 2.4506 , -40 , 3.372962 ) , ( 3.372962 , -40 , 2.4506 ) , ( 3.965154 , -40 , 1.288357 ) , ( 4.16921 , -40 , 0 ) ]

        self.gimbalCtrlPoint = [ ( 9.747901 , 0 , -9.747901 ) , ( 0 , -44.169209 , 0 ) , ( -9.747901 , 0 , -9.747901 ) , ( 0 , -44.169209 , 0 ) , ( -9.747901 , 0 , 9.747901 ) , ( 0 , -44.169209 , 0 ) , ( 9.747901 , 0 , 9.747901 ) , ( 9.747901 , 0 , -9.747901 ) , ( -9.747901 , 0 , -9.747901 ) , ( -9.747901 , 0 , 9.747901 ) , ( 9.747901 , 0 , 9.747901 ) ]

        ### creating elements ###
        self.mainCtrl = pm.curve ( d = 1 , p = self.mainCtrlPoint ) ;
        self.gimbalCtrl = pm.curve ( d = 1 , p = self.gimbalCtrlPoint ) ;
        self.gimbalCtrlZroGrp = pm.group ( em = True , w = True ) ;
        self.vectorProduct  = pm.createNode ( 'vectorProduct' ) ; 

        ### managing attr / parenting  ###
        for each in [ self.mainCtrl , self.gimbalCtrl ] :
            self.clean ( each ) ; 
            self.lockHideAttr ( each ) ;

        for attr in ( 't' , 'r' , 's' , 'v' ) :
            command = 'self.gimbalCtrlZroGrp.%s.lock()' % attr ;
            exec ( command ) ;

        pm.parent ( self.gimbalCtrl , self.gimbalCtrlZroGrp ) ;
        pm.parent ( self.gimbalCtrlZroGrp , self.mainCtrl ) ;

        ### gimbal controller ###
        self.mainCtrlShape = self.mainCtrl.getShape ( ) ;
        self.gimbalCtrlShape = self.gimbalCtrl.getShape ( ) ;

        self.mainCtrlShape.addAttr ( 'gimbalCtrl' , keyable = True , attributeType = 'bool' ) ;
        self.mainCtrlShape.gimbalCtrl >> self.gimbalCtrlShape.v ;
        self.setColor ( self.gimbalCtrl , ( 1 , 1 , 1 ) ) ;

    def __str__ ( self ) :
        return str ( self.mainCtrl ) ;

    def __repr__ ( self ) :
        return str ( self.mainCtrl ) ;

    def clean ( self , target ) :
        ### freeze transformation ###
        pm.makeIdentity ( target , apply = True ) ;
        ### delete history ###
        pm.delete ( target , ch = True ) ;

    def lockHideAttr ( self , target ) :
        for axis in ( 'x' , 'y' , 'z' ) :
            pm.setAttr ( target + '.s' + axis , lock = True , keyable = False , channelBox = False ) ;
        #pm.setAttr ( target + '.v' , lock = True , keyable = False , channelBox = False ) ;

    def setColor ( self , target , color ) :
        target  = pm.general.PyNode ( target ) ;
        shape   = target.getShape ( ) ;
        shape.overrideEnabled.set ( True ) ;
        shape.overrideRGBColors.set ( True ) ;
        shape.overrideColorRGB.set ( color ) ;

    def rename ( self ) :
        self.mainCtrl.rename ( self.nucleus + '_Ctrl' ) ;
        self.gimbalCtrl.rename ( self.nucleus + 'Gmbl_Ctrl' ) ;
        self.gimbalCtrlZroGrp.rename ( self.nucleus + 'GmblCtrlZro_Grp' ) ; 
        self.vectorProduct.rename ( self.nucleus + '_vectorProduct' ) ;

    def connect ( self ) :

        self.rename ( ) ;

        self.gimbalCtrl.worldMatrix >> self.vectorProduct.matrix ;
        self.vectorProduct.operation.set ( 3 ) ;
        self.vectorProduct.input1Y.set ( -1 ) ;
        self.vectorProduct.output >> self.nucleus.gravityDirection ;

        pm.pointConstraint ( self.gimbalCtrl , self.nucleus , mo = False , skip = 'none' ) ;

        self.mainCtrl.addAttr ( '__nucleus__' , keyable = True , attributeType = 'float' ) ;
        self.mainCtrl.__nucleus__.lock ( ) ;

#         for attr in [ 'enable' , 'gravity' , 'timeScale' , 'spaceScale' , 'subSteps' , 'maxCollisionIterations' ] :

#             command = '''
# attrType    = self.nucleus.{attr}.type ( ) ;
# attrVal     = self.nucleus.{attr}.get ( ) ;

# self.mainCtrl.addAttr ( '{attr}' , keyable = True , attributeType = attrType ) ;
# self.mainCtrl.{attr} >> self.nucleus.{attr} ;
# self.mainCtrl.{attr}.set ( attrVal ) ;
# '''.format ( attr = attr ) ;

#             exec ( command ) ;

def run ( *args ) :

    green           = ( 0 , 1 , 0 ) ;
    lightBlue       = ( 0 , 1 , 1 ) ;
    red             = ( 1 , 0 , 0 ) ;
    yellow          = ( 1 , 1 , 0 ) ;
    colorIndex      = [ green , lightBlue , red , yellow ] ;
    attrSetIndex    = [ [ 'tx' , 10 ] , [ 'tx' , -10 ] , [ 'tz' , 10 ] , [ 'tz' , -10 ] ] ;

    selection = pm.ls ( sl = True ) ;

    counter = 0 ;

    for each in selection :

        nucleus     = each ;
        nucleusCtrl = NucleusRig ( nucleus ) ;
        nucleusCtrl.connect ( ) ;
        
        if len(selection) != 1 :

            nucleusCtrl.setColor ( nucleusCtrl , colorIndex[counter] ) ;
            
            ### necleus global controller ###
            if pm.objExists ( 'nucleusGlobal_Ctrl' ) == False :
                globalCtrlPoint = [ ( -46.293479 , 0 , 11.57337 ) , ( -46.293479 , 0 , 23.14674 ) , ( -69.440219 , 0 , 0 ) , ( -46.293479 , 0 , -23.14674 ) , ( -46.293479 , 0 , -11.57337 ) , ( -23.14674 , 0 , -11.57337 ) , ( -11.57337 , 0 , -23.14674 ) , ( -11.57337 , 0 , -46.293479 ) , ( -23.14674 , 0 , -46.293479 ) , ( 0 , 0 , -69.440219 ) , ( 23.14674 , 0 , -46.293479 ) , ( 11.57337 , 0 , -46.293479 ) , ( 11.57337 , 0 , -23.14674 ) , ( 23.14674 , 0 , -11.57337 ) , ( 46.293479 , 0 , -11.57337 ) , ( 46.293479 , 0 , -23.14674 ) , ( 69.440219 , 0 , 0 ) , ( 46.293479 , 0 , 23.14674 ) , ( 46.293479 , 0 , 11.57337 ) , ( 23.14674 , 0 , 11.57337 ) , ( 11.57337 , 0 , 23.14674 ) , ( 11.57337 , 0 , 46.293479 ) , ( 23.14674 , 0 , 46.293479 ) , ( 0 , 0 , 69.440219 ) , ( -23.14674 , 0 , 46.293479 ) , ( -11.57337 , 0 , 46.293479 ) , ( -11.57337 , 0 , 23.14674 ) , ( -23.14674 , 0 , 11.57337 ) , ( -46.293479 , 0 , 11.57337 ) ]
                globalCtrl = pm.curve ( d = 1 , p = globalCtrlPoint ) ;
                globalCtrl.rename ( 'nucleusGlobal_Ctrl' ) ;

                for axis in ( 'x' , 'y' , 'z' ) :
                    pm.setAttr ( globalCtrl + '.s' + axis , lock = True , keyable = False , channelBox = False ) ;

                globalCtrlShape = globalCtrl.getShape ( ) ;
                globalCtrlShape.overrideEnabled.set ( True ) ;
                globalCtrlShape.overrideRGBColors.set ( True ) ;
                globalCtrlShape.overrideColorRGB.set ( ( 1 , 0 , 0.6 ) ) ;

            else :
                globalCtrl = pm.general.PyNode ( 'nucleusGlobal_Ctrl' ) ;

            pm.parent ( nucleusCtrl , globalCtrl ) ;

            pm.setAttr ( str(nucleusCtrl) + '.' + attrSetIndex[counter][0] , attrSetIndex[counter][1] ) ;

            counter += 1 ;

# run ( ) ;