import pymel.core as pm ;
import maya.cmds as mc ;
import os , ast , re ;

pm.cycleCheck ( e = 0 ) ;

currentProj = pm.workspace ( q = True , rootDirectory = True ) ;

######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################
''' cache out '''
######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################

## cache out ##
#doCreateGeometryCache 6 { "2", "1", "10", "OneFile", "1", "D:/TwoHeroes/film001/q0310/s0060/hanumanCasual/cache/DYN","0","Pants_DYN_v001_001","0", "export", "0", "1", "1","0","1","mcx","1" } ;
#// Result: D:/TwoHeroes/film001/q0310/s0060/hanumanCasual/cache/DYN//Pants_DYN_v001_001.xml // 
width = 300.00 ;
def export_cache_sim (*args):

    ### hard code pattern from autonaming_generation2

    path =  mc.file(sn=True,q=True)
    split_path = path.split('/')
    project = split_path[1]
    episode = split_path[2]
    shot_name = split_path[3]
    namespace = split_path[4]

    from rf_utils.context import context_info
    context = context_info.Context()



def run ( *args ) :

    if pm.window ( 'export_cache' , exists = True ) :
        pm.deleteUI ( 'export_cache' ) ;
    else : pass ;

    window = pm.window ( 'export_cache', title = "Export Sim Cache" , w = width , 
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;

    pm.window ( 'export_cache', e = True , w = width , h = 10 ) ;
    
    with window :
    
        mainLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] ) ;
        with mainLayout :
            import_layout = pm.rowColumnLayout ( 'Import' , w = width , nc = 2 ) ;
            with import_layout :
                pm.separator ( h = 5 , vis = False ) ;
                pm.button ( label = 'Export Cache' , c = export_cache_sim , bgc = ( 1 , 0.9 , 0.7 ) ) ;
    window.show ( ) ;

# def toShelf ( *args ) :

#     import os ;

#     self_path = os.path.realpath (__file__) ;
#     file_name = os.path.basename (__file__) ;
#     self_path = self_path.replace ( file_name , '' ) ;
#     self_path = self_path.replace ( '\\' , '/' ) ;

#     if self_path[-1] != '/' :
#         self_path += '/' ;

#     # self_path = D:/Dropbox/script/birdScript/projects/twoHeroes/

#     image_path = self_path + 'media/simUtil_icon.png'

#     commandHeader = '''
# import sys ;

# mp = "%s" ;
# if not mp in sys.path :
#     sys.path.insert ( 0 , mp ) ;
# ''' % self_path.split ('/sim/')[0] ;
# # D:/Dropbox/script/birdScript

#     cmd = commandHeader ;
#     cmd += '''
# import sim.sim_simUtilities.core as sim_simUtilities ;
# reload ( sim_simUtilities ) ;
# sim_simUtilities.run ( ) ;

# '''
#     mel_cmd = '''
# global string $gShelfTopLevel;
# string $shelves = `tabLayout -q -selectTab $gShelfTopLevel`;
# '''
#     currentShelf = pm.mel.eval ( mel_cmd );
#     pm.shelfButton ( style = 'iconOnly' , image = image_path , command = cmd , parent = currentShelf ) ;