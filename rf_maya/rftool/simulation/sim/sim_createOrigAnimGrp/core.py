import pymel.core as pm ;

class Origin ( object ) :

    def __init__ ( self , node ) :
        self.node = node ;
    
    def makeName ( self ) :
        # return name 

        nameSplit = self.node.split ( '_' ) ;
        name = '' ;

        for each in nameSplit :

            if each[0].isupper != True :
                name += ( each[0].upper( ) + each[1:] ) ;
            else :
                name += each ;
                
        return name ;

    def createBwdFwdGrp ( self ) :
        
        name = self.makeName ( ) ;

        selection = self.node ;
        selection = pm.general.PyNode ( selection ) ;

        bwdGrp = pm.group ( em = True , n = '%s_ORIGIN' % name ) ;
        fwdGrp = pm.group ( em = True , n = '%s_ANIM' % name ) ;
        
        bwdGrp.tx.set ( selection.tx.get ( ) * -1 ) ;
        bwdGrp.ty.set ( selection.ty.get ( ) * -1 ) ;
        bwdGrp.tz.set ( selection.tz.get ( ) * -1 ) ;
        bwdGrp.t.lock ( ) ;
        bwdGrp.r.lock ( ) ;
        bwdGrp.s.lock ( ) ;
        bwdGrp.useOutlinerColor.set ( 1 ) ;        
        bwdGrp.outlinerColor.set ( 0 , 0.98 , 0.6 ) ;
        
        fwdGrp.t.set ( selection.t.get ( ) ) ;
        fwdGrp.t.lock ( ) ;
        fwdGrp.r.lock ( ) ;
        fwdGrp.s.lock ( ) ;
        fwdGrp.useOutlinerColor.set ( 1 ) ;        
        fwdGrp.outlinerColor.set ( 0 , 0.98 , 0.6 ) ;

        return ( bwdGrp , fwdGrp ) ;

def run ( *args ) :

    selection = pm.ls ( sl = True ) ;

    if len ( selection ) != 1 :
        pass ;

    else :
        selection = selection [0] ;

    print selection ;

    selection = Origin ( node = selection ) ;
    selection.createBwdFwdGrp ( ) ;