import pymel.core as pm ;

title = 'select hair by length v1.0' ;
width = 450.00 ;

############
############
''' CORE '''
############
############

import sim.hair_selectionByLength.core as hsbl ;
reload ( hsbl ) ;

def refresh_cmd ( *args ) :
    hsbl.refresh_cmd ( ) ;

def selectCurves_cmd ( *args ) :
    hsbl.selectCurves_cmd ( ) ;

############
############
''' UI '''
############
############

def separator ( *args ) :
    pm.separator ( vis = False , h = 5 ) ;

def filler ( *args ) :
    pm.text ( label = '' ) ;

def run ( *args ) :
    
    # check if window exists
    if pm.window ( 'hair_selectHairByLength_UI' , exists = True ) :
        pm.deleteUI ( 'hair_selectHairByLength_UI' ) ;
    else : pass ;

    window = pm.window ( 'hair_selectHairByLength_UI', title = title , mnb = True , mxb = False , sizeable = True , rtf = True ) ;

    with pm.window ( 'hair_selectHairByLength_UI' , e = True , w = width , h = 10 ) :
        ### main layout ###
        with pm.rowColumnLayout ( nc = 1 , w = width ) :
            
            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2/2 ) , ( 2 , width/2/2 ) ] ) :
                    pm.text ( label = 'hair suffix' , width = width/2/2 ) ;
                    pm.textField ( 'hsbl_hairSuffix_textField' ,  width = width/2/2 , text = '_CRV' ) ;

            with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/4 ) , ( 2 , width/4 ) , ( 3 , width/2 ) ] ) :
                pm.text ( label = 'longest length' , w = width/4 ) ;
                pm.floatField ( 'hsbl_longestLength_floatField' , v = 0.0 , w = width/4 ) ;
                pm.button ( label = 'refresh' , w = width/2 , c = refresh_cmd ) ;

            pm.separator ( h = 20 ) ;

            pm.floatSliderGrp ( 'hsbl_hairLength_slider' , label = 'hair length (%)' , field = True ,
                    minValue = 0.01 , maxValue = 100.00 , precision = 2 ,
                    columnWidth3 = [ width/4 , width/4 , width/2 ] ,
                    columnAlign3 = [ "center" , "both" , "left" ] ,
                    value = 100 ,
                    width = width ) ;

            pm.button ( label = 'select curve' , width = width ,
                        c = selectCurves_cmd ) ;
            
    refresh_cmd ( ) ;

    window.show () ;
    