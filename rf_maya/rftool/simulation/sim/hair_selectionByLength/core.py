import pymel.core as pm ;

def getLongestLength_cmd ( *args ) :
    # return longestLength

    hairSuffix = pm.textField ( 'hsbl_hairSuffix_textField' ,  q = True , text = True ) ;

    allCurves_list = pm.ls ( '*%s' % hairSuffix ) ;

    curveLength_list = [] ;

    for curve in allCurves_list :
        length = pm.arclen ( curve ) ;
        curveLength_list.append ( length ) ;

    curveLength_list.sort ( ) ;

    longestLength = curveLength_list[-1] ;

    return longestLength

def selectCurves_cmd ( *args ) :

    hairSuffix = pm.textField ( 'hsbl_hairSuffix_textField' ,  q = True , text = True ) ;

    allCurves_list = pm.ls ( '*%s' % hairSuffix ) ;
    toSelect_list = [ ] ;
    longestLength = pm.floatField ( 'hsbl_longestLength_floatField' , q = True , v = True ) ;
    percentage = pm.floatSliderGrp ( 'hsbl_hairLength_slider' , q = True , v = True ) ;

    for curve in allCurves_list :
        
        try :
            type = curve.getShape() ;
            type = str ( pm.objectType ( type ) ) ;
            if type == 'nurbsCurve' :
                if pm.arclen ( curve ) < ( longestLength / 100 * percentage ) :
                    toSelect_list.append ( curve ) ;
        except : pass ;

    pm.select ( toSelect_list ) ;

def refresh_cmd ( *args ) :
    pm.floatField ( 'hsbl_longestLength_floatField' , e = True , v = getLongestLength_cmd() ) ;


