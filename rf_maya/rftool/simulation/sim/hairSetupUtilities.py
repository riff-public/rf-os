import os ;
import pymel.core as pm ;
import maya.cmds as mc ;
import ast ;

colorDict = { } ;
colorDict [ 'green' ]       = ( 0.498039 , 1 , 0 ) ;
colorDict [ 'yellow' ]      = ( 1 , 1 , 0 ) ;
colorDict [ 'red' ]         = ( 1 , 0 , 0 ) ;
colorDict [ 'pink' ]        = ( 1 , 0.411765 , 0.705882 ) ;
colorDict [ 'purple' ]      = ( 1 , 0 , 1 ) ;
colorDict [ 'lightBlue' ]   = ( 0 , 1 , 1 ) ;
colorDict [ 'paleGreen' ]   = ( 0.564706 , 0.933333 , 0.564706 ) ;
colorDict [ 'paleBlue' ]    = ( 0.529412 , 0.807843 , 0.980392 ) ;
colorDict [ 'white' ]       = ( 1 , 1 , 1 ) ;
colorDict [ 'paleRed' ]     = ( 0.803922 , 0.360784 , 0.360784 ) ;
colorDict [ 'palePink' ]    = ( 0.941176 , 0.501961 , 0.501961 ) ;
colorDict [ 'paleYellow' ]  = ( 1 , 0.870588 , 0.678431 ) ;
colorDict [ 'palePurple' ]  = ( 0.866667 , 0.627451 , 0.866667 ) ;


selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;
mp = selfPath.split('/sim/')[0] ;


def changeOutlinerColor ( target , color , *args ) :

    target = pm.general.PyNode ( target ) ;
    target.useOutlinerColor.set ( 1 ) ;        
    target.outlinerColor.set ( color ) ;    

###

class HairSetup ( object ) :

    def __init__( self ) :
        self.layer1 = 'hairOri1_layer' ;
        self.layer2 = 'hairOri2_layer' ;
        self.layer3 = 'hairOri3_layer' ;
        self.layer4 = 'hairOri4_layer' ;
    
    def createLayer ( self , name , color ) :
        self.name = name ;
        self.color = color ;
        
        layer = pm.createDisplayLayer ( name = self.name , empty = True ) ;
        layer = pm.general.PyNode ( layer ) ;
        layer.color.set ( self.color ) ;
        return name ;

    def sceneInitialize ( self ) :
        
        if pm.objExists ( self.layer4 ) == False :
            layer4 = HairSetup ( ) ;
            layer4 = layer4.createLayer ( name = self.layer4 , color = 17 ) ;
        else : pass ;
        
        if pm.objExists ( self.layer3 ) == False :
            layer3 = HairSetup ( ) ;
            layer3 = layer3.createLayer ( name = self.layer3 , color = 13 ) ;
        else : pass ;
            
        if pm.objExists ( self.layer2 ) == False :
            layer2 = HairSetup ( ) ;
            layer2 = layer2.createLayer ( name = self.layer2 , color = 18 ) ;
        else : pass ;
        
        if pm.objExists ( self.layer1 ) == False :    
            layer1 = HairSetup ( ) ;
            layer1 = layer1.createLayer ( name = self.layer1 , color = 14 ) ;
        else : pass ;
        
    def layerVisToggle ( self ) :
        pm.mel.eval ( 'layerEditorLayerButtonVisibilityChange %s' % self.layer1 ) ;
        pm.mel.eval ( 'layerEditorLayerButtonVisibilityChange %s' % self.layer2 ) ;
        pm.mel.eval ( 'layerEditorLayerButtonVisibilityChange %s' % self.layer3 ) ;
        pm.mel.eval ( 'layerEditorLayerButtonVisibilityChange %s' % self.layer4 ) ;
        
    def assignLayer1 ( self ) :
        pm.mel.eval ( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer1 ) ;
    
    def assignLayer2 ( self ) :
        pm.mel.eval ( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer2 ) ;
        
    def assignLayer3 ( self ) :
        pm.mel.eval ( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer3 ) ;
        
    def assignLayer4 ( self ) :
        pm.mel.eval ( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer4 ) ;

########################################################################
########################################################################
########################################################################

def initializeScene ( *args ) :
    initializeScene = HairSetup ( ) ;
    initializeScene.sceneInitialize ( ) ;
    
def layerVisToggle ( *args ) :
    layerVisToggle = HairSetup ( ) ;
    layerVisToggle.layerVisToggle ( ) ;

def assignLayer1 ( *args ) :
    assignLayer1 = HairSetup ( ) ;
    assignLayer1.assignLayer1 ( ) ;
    
def assignLayer2 ( *args ) :
    assignLayer2 = HairSetup ( ) ;
    assignLayer2.assignLayer2 ( ) ;

def assignLayer3 ( *args ) :
    assignLayer3 = HairSetup ( ) ;
    assignLayer3.assignLayer3 ( ) ;

def assignLayer4 ( *args ) :
    assignLayer4 = HairSetup ( ) ;
    assignLayer4.assignLayer4 ( ) ;

########################################################################
########################################################################
########################################################################

def F2CCopy ( *args ) :

    selection = pm.ls ( sl = True ) ;
    
    def getHairName ( *args ) :
    
        hairNameList = [] ;
    
        for each in selection :
            
            children = pm.listRelatives ( each , c = True ) ;
            
            for child in children :
                if 'ORI' in str ( child ) :
                    name = child.split ( '_' ) [0] ; 
                    hairNameList.append ( name ) ;
                    
        return hairNameList ;
        
    nameList = getHairName ( ) ;
 
    pm.scrollField ( 'F2CTxtScroll' , e = True , text = str ( nameList ) ) ;

def F2CPaste ( *args ) :
 
    listText = pm.scrollField ( 'F2CTxtScroll' , q = True , text = True ) ;

    listText = 'list' + ' ' + '=' + ' ' + listText ;

    exec ( listText ) ;

    selection = pm.ls ( sl = True ) ;
    
    for i in range ( 0 , len ( selection ) ) :
        pm.rename ( selection [i] , list [i] + '_CRV' ) ;

########################################################################
########################################################################
########################################################################

def selectStep ( step = 2 , *args ) : 

    selection = pm.ls ( sl = True ) ;

    selectionList = [] ;

    for i in range ( 0 , len ( selection ) , step ) :

        selectionList.append ( selection [ i ] ) ;
        
    pm.select ( selectionList ) ;

def selectStep2 ( *args ) :
    selectStep ( step = 2 ) ;

def selectStep3 ( *args ) :
    selectStep ( step = 3 ) ;

########################################################################
########################################################################
########################################################################

def selectCurveCV ( *args ) :

    cv = pm.textField ( 'hairCV' , q = True , text = True ) ;

    selection = pm.ls ( sl = True ) ;

    toSelectList = [] ;

    for each in selection :

        toSelectList.append ( str ( each ) + '.cv[%s]' % cv ) ;

    pm.select ( toSelectList ) ;

def frameLayout_cc ( *args ) :
    pm.window ( 'hairSetupUtilUI' , e = True , h = 10 ) ;

########################################################################
''' step 020 '''
########################################################################

currentProject = pm.workspace ( q = True , rd = True ) ;
    # rd = rootDirectory
    # D:/Dropbox/doubleMonkey/setup/hanumanCasual/

textFilePathDefault = currentProject + 'data/dynSetupData' ;

if os.path.exists ( textFilePathDefault ) == True :
    pass ;
else :
    os.makedirs ( textFilePathDefault ) ;

def getOri_cmd ( *args ) :
    
    outlinerItem = pm.ls ( assemblies = True ) ;
    
    ori = [] ;

    for each in outlinerItem :
        if '_ORI' in str ( each ) :
            ori.append ( each ) ;
        else : pass ;

    if len ( ori ) != 1 :
        pm.error ( 'more that one hair_ORI group is in the scene, please check' ) ;

    else :
        ori = ori [0] ;
        pm.textField ( 'hairElem_textField' , e = True , tx = ori ) ;

def getEdges_cmd ( *args ) :

    edges = pm.ls ( sl = True ) ;

    if len ( edges ) != 2 :
        pm.error ( '2 edges must be selected' ) ;

    else :
        pm.textField ( 'edge1_txtField' , e = True , tx = edges[0].split('.')[1] ) ;
        pm.textField ( 'edge2_txtField' , e = True , tx = edges[1].split('.')[1] ) ;
        pm.textField ( 'objectShape_textField' , e = True , tx = edges[0].split('.')[0] ) ;

        object = pm.ls ( sl = True , o = True ) ;
        object = pm.listRelatives ( object , parent = True ) [ 0 ] ;

        selection = mc.ls ( sl = True ) [0] ;
        fullPath = mc.listRelatives ( selection , p = True , f = True ) ;
        fullPath = fullPath[0].split ( '|' ) ;
        objectRoot = fullPath [1] ;

        pm.textField ( 'object_textField' , e = True , tx = object ) ;
        pm.textField ( 'objectRoot_textField' , e = True , tx = objectRoot ) ;

    getOri_cmd ( ) ;

def objectInfoSave_cmd ( *args ) :

    textFilePath = currentProject + 'data/dynSetupData' ;

    textFileName = mc.file ( q = True , exn = True ) ;
    # D:/Dropbox/doubleMonkey/setup/hanumanCasual/scenes/hanumanCasual.001.hair1_hairTop.v001_001.ma

    textFileName = textFileName.split ( '/' ) [-1] + '.dynSetupData.txt' ;
    # hanumanCasual.001.hair1_hairTop.v001_001.ma.dynSetupData.txt

    textFilePath += '/' + textFileName ;

    dataDict = { } ;

    object      = pm.textField ( 'object_textField' , q = True , tx = True ) ;
    objectShape = pm.textField ( 'objectShape_textField' , q = True , tx = True ) ;
    objectRoot  = pm.textField ( 'objectRoot_textField' , q = True , tx = True ) ;
    edge1       = pm.textField ( 'edge1_txtField' , q = True , tx = True ) ;
    edge2       = pm.textField ( 'edge2_txtField' , q = True , tx = True ) ;
    hairElem    = pm.textField ( 'hairElem_textField' , q = True , tx = True ) ;
        
    dataDict [ 'object' ]       = object ;
    dataDict [ 'objectShape' ]  = objectShape ;
    dataDict [ 'objectRoot' ]   = objectRoot ;
    dataDict [ 'edge1' ]        = edge1 ; 
    dataDict [ 'edge2' ]        = edge2 ;
    dataDict [ 'hairElem' ]     = hairElem ;

    textFile = open ( textFilePath , 'w+' ) ;
    textFile.write ( str ( dataDict) ) ;
    textFile.close ( ) ;

def objectInfoLoad_cmd ( *args ) :

    textFilePath = currentProject + 'data/dynSetupData' ;

    path = pm.fileDialog2 ( dir = textFilePath , fileMode = 4 ) ;

    textFile = open ( path [0] , 'r' ) ;
    text = textFile.read ( ) ;
    textFile.close ( ) ;

    print text ;

    dataDict = ast.literal_eval ( text ) ;

    print dataDict ;

    pm.textField ( 'object_textField' , e = True , tx = dataDict [ 'object' ]  ) ;
    pm.textField ( 'objectShape_textField' , e = True , tx = dataDict [ 'objectShape' ]  ) ;
    pm.textField ( 'objectRoot_textField' , e = True , tx = dataDict [ 'objectRoot' ]  ) ;
    pm.textField ( 'edge1_txtField' , e = True , tx = dataDict [ 'edge1' ]   ) ;
    pm.textField ( 'edge2_txtField' , e = True , tx = dataDict [ 'edge2' ]  ) ;
    pm.textField ( 'hairElem_textField' , e = True , tx =  dataDict [ 'hairElem' ] ) ;

def clean ( target = '' , *args ) :
    # freeze transform , delete history , center pivot
    pm.makeIdentity ( target , apply = True ) ;    
    pm.delete ( target , ch = True ) ;
    pm.xform ( target , cp = True ) ;

def deleteOrig_cmd ( target = '' , *args ) :
    
    shapes = pm.listRelatives ( target , shapes = True ) ;

    for shape in shapes :
        if 'Orig' in str ( shape ) :
            pm.delete ( shape ) ;
        else : pass ;

def addSuffix ( target = '' , suffix = '' , *args ) :
    # return renamed target
    hierarchy = pm.listRelatives ( target , ad = True , type = 'transform' ) ;
        
    for each in hierarchy :
        
        each.rename ( each + suffix ) ;

    target = pm.general.PyNode ( target ) ;
    target.rename ( target + suffix ) ;
        
    return target ;

def duplicate ( target = '' , suffix = '' , *args ) :
    # return duplicatedRoot ;
    duplicatedRoot = pm.duplicate ( target ) [0] ;
    duplicatedRoot.rename ( duplicatedRoot [0:-1] + suffix ) ;
    
    hierarchy = pm.listRelatives ( duplicatedRoot , ad = True , type = 'transform' ) ;
    for each in hierarchy :
        each.rename ( each + suffix ) ;
        
    return ( duplicatedRoot ) ;

def extrudeHairTube ( group ) :
    # return baseGrp tubeGrp 

    axes = ( 'x' , 'y' , 'z' ) ;
                   
    if '_' in group :
        groupNm = group.split ( '_' ) [0] ;
    else :
        groupNm = group ;

    curveList =  pm.listRelatives ( group , children = True ) ;
          
    baseList = [ ] ;
    
    extrudeList = [ ] ;
    
    for each in curveList:
        
        circle = pm.circle ( n = each.split('_')[0] + '_base' ) [0] ;
        
        baseList.append ( circle ) ;
        
        for axis in axes :
            pm.setAttr ( circle + '.s' + axis , 0.3 ) ;
        
        extrude = pm.extrude ( circle , each , ch = True , rn = False , po = 0 , et = 2 , ucp = 0 , fpt = 0 , upn = 0 , rsp = 1 , sc = 0.5 ) [0] ;

        extrudeList.append ( extrude ) ;
    
    baseGrp = pm.group ( baseList , n = groupNm + '_baseGRP' ) ;
    
    tubeGrp = pm.group ( extrudeList , n = groupNm + '_tubeGRP' ) ;

    ### connect nodes so that the base follows hair curves
 
    for i in range ( 0 , len ( curveList ) ) :
            
        curve = curveList [i] ;
        curveShape = pm.listRelatives ( curve , shapes = True ) [0] ;
        base = baseList [i] ;
        
        mtp = pm.createNode ( 'motionPath' , n = curve + '_mtp' ) ;
        pm.setAttr ( mtp + '.sideTwist' , 90 ) ;        
        pm.connectAttr ( curveShape + '.worldSpace[0]' , mtp + '.geometryPath' ) ;        
        pm.connectAttr ( mtp + '.rotateX' , base + '.rotateX' ) ;
        pm.connectAttr ( mtp + '.rotateY' , base + '.rotateY' ) ;
        pm.connectAttr ( mtp + '.rotateZ' , base + '.rotateZ' ) ;
        pm.connectAttr ( mtp + '.rotateOrder' , base + '.rotateOrder' ) ;
        pm.connectAttr ( mtp + '.message' , base + '.specifiedManipLocation' ) ;
        
        adlX = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlX' ) ;
        adlY = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlY' ) ;
        adlZ = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlZ' ) ;
        
        pm.connectAttr ( base + '.transMinusRotatePivotX' , adlX + '.input1' ) ;
        pm.connectAttr ( mtp + '.xCoordinate' , adlX + '.input2' ) ;
        pm.connectAttr ( adlX + '.output' , base + '.translateX' ) ;
        
        pm.connectAttr ( base + '.transMinusRotatePivotY' , adlY + '.input1' ) ;
        pm.connectAttr ( mtp + '.yCoordinate' , adlY + '.input2' ) ;
        pm.connectAttr ( adlY + '.output' , base + '.translateY' ) ;
        
        pm.connectAttr ( base + '.transMinusRotatePivotZ' , adlZ + '.input1' ) ;
        pm.connectAttr ( mtp + '.zCoordinate' , adlZ + '.input2' ) ;
        pm.connectAttr ( adlZ + '.output' , base + '.translateZ' ) ;

    return [ baseGrp , tubeGrp ] ;

def dynSetup_cmd ( *args ) :

    CIN_object = pm.textField ( 'object_textField' , q = True , tx = True ) ;
    CIN_object = pm.general.PyNode ( CIN_object ) ;

    CIN_objectShape = pm.textField ( 'objectShape_textField' , q = True , tx = True ) ;
    CIN_objectShape = pm.general.PyNode ( CIN_objectShape ) ;

    CIN_objectRoot = pm.textField ( 'objectRoot_textField' , q = True , tx = True ) ;
    CIN_objectRoot = pm.general.PyNode ( CIN_objectRoot ) ;

    CIN_edge1 = pm.textField ( 'edge1_txtField' , q = True , tx = True ) ;
    CIN_edge1 = CIN_objectShape + '.' + CIN_edge1 ;
    CIN_edge1 = pm.general.PyNode ( CIN_edge1 ) ;

    CIN_edge2 = pm.textField ( 'edge2_txtField' , q = True , tx = True ) ;
    CIN_edge2 = CIN_objectShape + '.' + CIN_edge2 ;
    CIN_edge2 = pm.general.PyNode ( CIN_edge2 ) ;

    hair_ori = pm.textField ( 'hairElem_textField' , q = True , tx = True ) ;

    hairElem = pm.textField ( 'hairElem_textField' , q = True , tx = True ) ;
    hairElem = hairElem.split ( '_ORI' ) [ 0 ] ;

    ### duplicate and rename ###

    COL_objectRoot = duplicate ( target = CIN_objectRoot , suffix = '_COL' ) ;

    addSuffix ( target = CIN_objectRoot , suffix = '_CIN' ) ;

        # print CIN_object ;
        # print CIN_objectShape ;
        # print CIN_objectRoot ;
        # print CIN_edge1 ;
        # print CIN_edge2 ;
            # okay, it did update !

    bsh = pm.blendShape ( CIN_objectRoot , COL_objectRoot , automatic = True , origin = 'world' , n = 'CIN_COL_BSH' ) [0] ; 

    startFrame = pm.playbackOptions ( q = True , min = True ) ;

    pm.setKeyframe (  bsh , attribute =  CIN_objectRoot , value = 0 , t = startFrame ) ;
    pm.setKeyframe (  bsh , attribute =  CIN_objectRoot , value = 1 , t = startFrame + 10 ) ;

    ### create noTouch_grp ###

    noTouch_grp = pm.group ( em = True , w = True , name = 'noTouch_grp' ) ;

    noTouch_grp.t.lock ( ) ;
    noTouch_grp.r.lock ( ) ;
    noTouch_grp.s.lock ( ) ;

    ### create nurbsurface ###

    CIN_curve1 = pm.polyToCurve ( CIN_edge1 , form = 0 , degree = 1 , n = CIN_object + '_crv1' ) ;
    CIN_curve2 = pm.polyToCurve ( CIN_edge2 , form = 0 , degree = 1 , n = CIN_object + '_crv1' ) ;

    CIN_nurb = pm.loft ( CIN_curve1 , CIN_curve2 , ch = 0 , ar = True , d = 1 , ss = 1 , rn = 0 , po = 0 , rsn = True ) ;
    CIN_nurb = pm.general.PyNode ( CIN_nurb [0] ) ;
    CIN_nurb.rename ( CIN_object + '_nrb' ) ;

    clean ( target = CIN_nurb ) ;

    pm.delete ( CIN_curve1 ) ;
    pm.delete ( CIN_curve2 ) ;

    ### create follicle ###

    CIN_folShape = pm.createNode ( 'follicle' ) ;
    CIN_fol = pm.listRelatives ( CIN_folShape , type = 'transform' , parent = True ) [0] ;

    pm.rename ( CIN_fol , CIN_object + '_fol' ) ;

    CIN_folShape.simulationMethod.set ( 0 ) ;

    CIN_folShape.outRotate >> CIN_fol.r ;
    CIN_folShape.outTranslate >> CIN_fol.t ;

    CIN_nurb.worldMatrix[0] >> CIN_folShape.inputWorldMatrix ;
    CIN_nurb.local >> CIN_folShape.inputSurface ;

    CIN_folShape.parameterU.set ( 0.5 ) ;
    CIN_folShape.parameterV.set ( 0.5 ) ;

    ### wrap ###

    pm.select ( CIN_nurb , CIN_object ) ;
    mc.CreateWrap ( ) ;

    wrap = set ( pm.listConnections ( CIN_nurb + 'Shape' , type = 'wrap' ) ) ;
    wrap = list ( wrap ) ;
    wrap = pm.general.PyNode ( wrap [0] ) ;
    pm.rename ( wrap , CIN_object + '_wrap' ) ;

    wrap.exclusiveBind.set ( 1 ) ;
    wrap.falloffMode.set ( 0 ) ;

    wrapBaseList = pm.listConnections ( wrap ) ;

    for each in wrapBaseList :
        if 'Base' in str ( each ) :
            wrapBase = each ;

    pm.parent ( wrapBase , w = True ) ;

    ### noTouch_grp organization ###

    pm.parent ( CIN_nurb , noTouch_grp ) ;
    pm.parent ( CIN_fol , noTouch_grp ) ;
    pm.parent ( wrapBase , noTouch_grp ) ; 

    ### locator ###

    loc = pm.spaceLocator ( n = CIN_objectRoot.split ( '_CIN' ) [0] + '_LOC' ) ;
    changeOutlinerColor ( target = loc , color = colorDict [ 'green' ] ) ;

    pointCon = pm.pointConstraint ( CIN_fol , loc , mo = False ) ;
    pm.delete ( pointCon ) ;

    clean ( loc ) ;

    pm.parentConstraint ( CIN_fol , loc , mo = True ) ;

    pm.parent ( COL_objectRoot , loc ) ;
    pm.reorder ( COL_objectRoot , f = True ) ;

    COL_objectRoot.t.lock ( ) ;
    COL_objectRoot.r.lock ( ) ;
    COL_objectRoot.s.lock ( ) ;

    ### outliner color  and visibility ###

    changeOutlinerColor ( target = CIN_objectRoot , color = colorDict [ 'purple' ] ) ;
    CIN_objectRoot.v.set ( 0 ) ;

    changeOutlinerColor ( target = noTouch_grp , color = colorDict [ 'red' ] ) ;
    noTouch_grp.v.set ( 0 ) ;

    ### groups ###

    sim_grp = pm.group ( em = True , w = True , n = 'SIM_GRP' ) ;
    changeOutlinerColor ( target = sim_grp , color = colorDict [ 'lightBlue' ] ) ;

    nCloth_grp = pm.group ( em = True , w = True , n = 'nCloth_GRP' ) ;
    nRigid_grp = pm.group ( em = True , w = True , n = 'nRigid_GRP' ) ;
    DYC_grp = pm.group ( em = True , w = True , n = 'DYC_GRP' ) ;

    pm.parent ( nCloth_grp , nRigid_grp , DYC_grp , sim_grp ) ;

    ### outliner organization ###

    reordering_tempt = pm.group ( em = True , w = True , n = 'reordering_tempt' ) ;

    orderList = [ noTouch_grp , CIN_objectRoot , loc , sim_grp ] ;

    pm.parent ( orderList , reordering_tempt ) ;
    pm.parent ( orderList , w = True ) ;
    pm.delete ( reordering_tempt ) ;

    ### set up multiple hair system ###

    hair_grp = pm.group ( em = True , w = True , n = 'hair' ) ;
    changeOutlinerColor ( target = hair_grp , color = colorDict [ 'yellow' ] ) ;
    hair_grp.t.lock ( ) ;
    hair_grp.r.lock ( ) ;
    hair_grp.s.lock ( ) ;

    multiNucleusCol_grp = pm.group ( em = True , w = True , n = CIN_objectRoot + '_MNCol' ) ;
    # multiNucleusCol_grp.v.set ( 0 ) ;
    pm.parent ( multiNucleusCol_grp , loc ) ;
    pm.reorder ( multiNucleusCol_grp , r = 2 ) ;

    for i in range ( 1 , 5 ) :

        duplicatedObject = pm.duplicate ( CIN_object , n = CIN_object.split ( '_CIN' ) [0] + 'N' + str ( i ) + '_COL' ) ;
        deleteOrig_cmd ( target = duplicatedObject ) ;
        
        pm.parent ( duplicatedObject , multiNucleusCol_grp ) ;

        pm.mel.eval ( 'layerEditorSelectObjects hairOri%s_layer;' % str ( i ) ) ;
        
        curves = pm.ls ( sl = True ) ;

        pm.select ( curves , duplicatedObject ) ;
        
        #pm.mel.eval ( 'makeCurvesDynamic 2 { "1", "0", "1", "1", "0"};' ) ;
        pm.mel.eval ( 'makeCurvesDynamic 2 { "1", "1", "1", "1", "0"};' ) ;

        hairSystem = pm.general.PyNode ( 'hairSystem%s' % str ( i ) ) ;        

        try :
            print ( "try:nRigid%s" % str ( i ) ) ;
            nRigid = pm.general.PyNode ( 'nRigid%s' % str ( i ) ) ;
        except :
            print ( "except:nRigid%s" % str ( i ) ) ;
            pm.select ( duplicatedObject ) ;
            pm.mel.eval ( 'makeCollideNCloth' ) ;
            nRigid = pm.general.PyNode ( 'nRigid%s' % str ( i ) ) ;
        '''
        else :
            print ( "else:nRigid%s" % str ( i ) ) ;
            pm.error ( 'please check line 623' ) ;
        '''
        
        outputCurve_grp = pm.general.PyNode ( 'hairSystem%sOutputCurves' % str ( i ) ) ;
        pm.parent ( outputCurve_grp , hair_grp ) ;

        if i == 1 :
            
            bsh = pm.blendShape (  CIN_object.split('_CIN')[0]+'_COL' , duplicatedObject , automatic = True , origin = 'world' , n = CIN_object.split ( '_CIN' ) [0] + 'N' + str ( i ) + '_BSH' ) [0] ; 
            pm.setAttr ( bsh + '.' + CIN_object.split('_CIN')[0]+'_COL' , 1 ) ;

        else : 

            duplicatedObject[0].v.set ( 0 ) ;

            bsh = pm.blendShape (  CIN_object.split ( '_CIN' ) [0] + 'N' + str ( i - 1 ) + '_COL' , duplicatedObject , automatic = True , origin = 'world' , n = CIN_object.split ( '_CIN' ) [0] + 'N' + str ( i ) + '_BSH' ) [0] ; 
            pm.setAttr ( bsh + '.' + CIN_object.split ( '_CIN' ) [0] + 'N' + str ( i - 1 ) + '_COL' , 1 ) ;

    for i in range ( 2 , 5 ) :

        pm.select ( 'hairSystem%s' % str ( i ) , 'nRigid%s' % str ( i ) ) ;

        pm.mel.eval ( 'assignNSolver "";' ) ;

    hairSystemList = [] ;
    nRigidList = [] ;

    for i in range ( 1 , 5 ) :

        hairSystem = pm.general.PyNode ( 'hairSystem%d' % i ) ;
        hairSystem.rename ( hairElem + 'N%d' % i + '_hairSystem' ) ;
        hairSystemList.append ( hairSystem ) ;

        nRigid = pm.general.PyNode ( 'nRigid%d' % i ) ;
        nRigid.rename ( CIN_object.split ( '_CIN' ) [0] + 'N%dCOL_nRigid'% i ) ;
        nRigidList.append ( nRigid ) ;

    # connect nucleus1's nRigid Attribute to else nucleus's nRigid
    pm.setAttr ( CIN_object.split('_CIN')[0]+'_COL.v' , 0 ) ;

    driver_nRigid = pm.general.PyNode ( nRigidList [0] ) ;
    driven_nRigidList = nRigidList [1:] ;

    for each in driven_nRigidList :
        driven_nRigid = pm.general.PyNode ( each ) ;

        driver_nRigid.isDynamic >> driven_nRigid.isDynamic ;
        driver_nRigid.collide >> driven_nRigid.collide ;
        driver_nRigid.collisionFlag >> driven_nRigid.collisionFlag ;
        driver_nRigid.collideStrength >> driven_nRigid.collideStrength ;
        driver_nRigid.collisionLayer >> driven_nRigid.collisionLayer ;
        driver_nRigid.thickness >> driven_nRigid.thickness ;
        driver_nRigid.solverDisplay >> driven_nRigid.solverDisplay ;
        driver_nRigid.displayColor >> driven_nRigid.displayColor ;
        driver_nRigid.bounce >> driven_nRigid.bounce ;
        driver_nRigid.friction >> driven_nRigid.friction ;
        driver_nRigid.stickiness >> driven_nRigid.stickiness ;
        driver_nRigid.trappedCheck >> driven_nRigid.trappedCheck ;
        driver_nRigid.pushOut >> driven_nRigid.pushOut ;
        driver_nRigid.pushOutRadius >> driven_nRigid.pushOutRadius ;
        driver_nRigid.crossoverPush >> driven_nRigid.crossoverPush ;

    ###

    pm.parent ( 'nucleus1' , 'nucleus2' , 'nucleus3' , 'nucleus4' , sim_grp ) ;
    pm.parent ( hairSystemList , sim_grp ) ;

    pm.parent ( nCloth_grp , w = True ) ;
    pm.parent ( nCloth_grp , sim_grp ) ;

    pm.parent ( nRigid_grp , w = True ) ;
    pm.parent ( nRigid_grp , sim_grp ) ;
    pm.parent ( nRigidList , nRigid_grp ) ;

    pm.parent ( DYC_grp , w = True ) ;
    pm.parent ( DYC_grp , sim_grp ) ;

    ### rename dyn curve ###

    follicleList = pm.listRelatives ( hair_ori , c = True ) ;
    print follicleList ;

    hairNameList = [ ] ;

    for follicle in follicleList :

        children = pm.listRelatives ( follicle , c = True ) ;
    
        for child in children :
            if 'ORI' in str ( child ) :
                name = child.split ( '_' ) [0] ; 
                hairNameList.append ( name ) ;

    hairDynList = [ ] ;

    hairDynGrpList = pm.listRelatives ( hair_grp , c = True ) ;

    for each in hairDynGrpList :
        hairDynList.extend ( pm.listRelatives ( each , c = True ) ) ;

    for i in range ( 0 , len ( hairNameList ) ) :
        pm.rename ( hairDynList [i] , hairNameList [i] + '_CRV' ) ;

    ##################

    rampShaderPath = mp + '/' + 'asset/shader_lambertRamp.ma' ;
    pm.mel.eval ( 'file -import -type "mayaAscii"  -ignoreVersion -mergeNamespacesOnClash false -rpr "shader_lambertRamp" -options "v=0;"  -importFrameRate true  -importTimeRange "override" "%s";' % rampShaderPath ) ;

    rampShader = [] ;
    rampShader.append ( 'hairGreen_shdSG' ) ;
    rampShader.append ( 'hairBlue_shdSG' ) ;
    rampShader.append ( 'hairRed_shdSG' ) ;
    rampShader.append ( 'hairYellow_shdSG' ) ;

    ##################

    baseGrpList = [] ;
    tubeGrpList = [] ;

    for i in range ( 0 , len ( hairDynGrpList ) ) :
        CRV_grp  = pm.rename ( hairDynGrpList [i] , hairElem + 'N%d_CRV' % ( i + 1 ) ) ;

        print CRV_grp ;

        extrude = extrudeHairTube ( CRV_grp ) ;

        baseGrpList.append ( extrude [0] ) ;
        tubeGrpList.append ( extrude [1] ) ;

        pm.select ( extrude [1] ) ;
        pm.mel.eval ( 'sets -e -forceElement %s;' % rampShader [i] ) ;

    hairTube_grp = pm.group ( em = True , w = True , n = 'hairTube_grp' ) ;
    hairTube_grp.t.lock ( ) ;
    hairTube_grp.r.lock ( ) ;
    hairTube_grp.s.lock ( ) ;

    hairTube_grp.addAttr ( 'hairTube_tubeScale' , keyable = True , attributeType = 'float' , dv = 0.3 ) ;
    hairTube_grp.addAttr ( 'hairTube_tipScale' , keyable = True , attributeType = 'float' , dv = 0.5 ) ;

    baseList = [ ] ; 

    for each in baseGrpList :
        each.v.set ( 0 ) ;
        bases = pm.listRelatives ( each , c = True ) ;
        baseList.extend ( bases ) ;

    for each in baseList :
        hairTube_grp.hairTube_tubeScale >> each.sx ;
        hairTube_grp.hairTube_tubeScale >> each.sy ;
        hairTube_grp.hairTube_tubeScale >> each.sz ;

    extrudeList = pm.ls ( type = 'extrude' ) ;

    for each in extrudeList :
        hairTube_grp.hairTube_tipScale >> each.scale ; 

    pm.parent ( baseGrpList , tubeGrpList , hairTube_grp ) ;

    pm.displaySmoothness ( hairTube_grp , divisionsU = 0 , divisionsV = 0 , pointsWire = 4 , pointsShaded = 1 , polygonObject = 1 ) ;

    pm.parent ( hair_ori , hairTube_grp , sim_grp ) ;

    # layer organization 

    def step ( target = [] , step = 2 , *args ) : 

        toSelectList = [ ] ;

        for i in range ( 0 , len ( target ) , step ) :

            toSelectList.append ( target [ i ] ) ;
            
        return toSelectList ;

    hairTubeList = [ ] ;

    for each in tubeGrpList :
        hairTubeList.extend ( pm.listRelatives ( each , c = True , type = 'transform' ) ) ;

    hairTubeDensity1List = step ( target = hairTubeList , step = 3 ) ;
    for each in hairTubeDensity1List :
        hairTubeList.remove ( each ) ;

    hairTubeDensity2List = step ( target = hairTubeList , step = 3 ) ;
    for each in hairTubeDensity2List :
        hairTubeList.remove ( each ) ;

    hairTubeDensity3List = step ( target = hairTubeList , step = 2 ) ;
    for each in hairTubeDensity3List :
        hairTubeList.remove ( each ) ;

    hairTubeDensity1_layer = pm.createDisplayLayer ( name = 'hairTubeDensity1_layer' , number = 1 , empty = True ) ;
    hairTubeDensity1_layer.color.set ( 28 ) ;
    
    hairTubeDensity2_layer = pm.createDisplayLayer ( name = 'hairTubeDensity2_layer' , number = 1 , empty = True ) ;
    hairTubeDensity2_layer.color.set ( 28 ) ;
    
    hairTubeDensity3_layer = pm.createDisplayLayer ( name = 'hairTubeDensity3_layer' , number = 1 , empty = True ) ;
    hairTubeDensity3_layer.color.set ( 28 ) ;
    
    hairTube_layer = pm.createDisplayLayer ( name = 'hairTube_layer' , number = 1 , empty = True ) ;
    hairTube_layer.color.set ( 29 ) ;
    
    hairDYN_layer = pm.createDisplayLayer ( name = 'hairDYN_layer' , number = 1 , empty = True ) ;
    hairDYN_layer.color.set ( 22 ) ;
    
    pm.editDisplayLayerMembers ( hairTube_layer , hairTube_grp ) ;
    pm.editDisplayLayerMembers ( hairTubeDensity3_layer , hairTubeDensity3List ) ;
    pm.editDisplayLayerMembers ( hairTubeDensity2_layer , hairTubeDensity2List ) ;
    pm.editDisplayLayerMembers ( hairTubeDensity1_layer , hairTubeDensity1List ) ;
    pm.editDisplayLayerMembers ( hairDYN_layer , hair_grp ) ;

def hairTipConstraint_cmd ( *args ) :

    hairElem = pm.textField ( 'hairElem_textField' , q = True , tx = True ) ;
    hairElem = hairElem.split ( '_ORI' ) [ 0 ] ;
    print hairElem ;

    for i in range ( 1 , 5 ) :

        pm.mel.eval ( 'layerEditorSelectObjects hairOri%s_layer;' % str ( i ) ) ;

        selection_list = pm.ls ( sl = True ) ;

        toSelect_list = [] ;

        for selection in selection_list :

            selectionShape = pm.listRelatives ( selection , shapes = True ) [0] ;

            nos = pm.getAttr ( selectionShape + '.spans' ) ;
            deg = pm.getAttr ( selectionShape + '.degree' ) ;

            toSelect_list.append ( '%s.cv[%s]' % ( selection , str ( nos + deg - 1 ) ) ) ;

        pm.select ( toSelect_list ) ; 

        nConShape = pm.mel.eval ( 'createNConstraint pointToPoint 0;' ) [0] ;
        nConShape = pm.general.PyNode ( nConShape ) ;

        nCon = pm.listRelatives ( nConShape , p = True ) [0] ;
        nCon.rename ( hairElem + str ( i ) + 'Tip_C2C_DYC' ) ;
        
        nConShape.connectionMethod.set ( 1 ) ;
        nConShape.maxDistance.set ( 1 ) ;

        pm.parent ( nCon , 'DYC_GRP' ) ;


########################################################################
''' clean up utilities '''
########################################################################

def deleteShapeOrig ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.deleteShapeOrig_run ( ) ;

'''
def freezeTransform_deleteHistory ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.freezeTransform_deleteHistory_run ( ) ;
'''

def freezeTransform_cmd ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.freezeTransform_run ( ) ;

def deleteHistory_cmd ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.deleteHistory_run ( ) ;

def centerPivot ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.centerPivot_run ( ) ;

def deleteRebuiltShape ( *args ) :
    import general.utilities as utilities ;
    reload ( utilities ) ;
    utilities.deleteRebuiltShape_run ( ) ;

########################################################################
''' UI '''
########################################################################

def filler ( n = 1 , *args ) :
    
    for i in range ( 0 , n ) :
    
        pm.text ( label = '' ) ;

def fillerLine ( n = 5 ) :

    pm.text ( label = '' , h = n ) ;

def run ( *args ) :
    
    width = 300.0 ;
    
    # check if window exists
    if pm.window ( 'hairSetupUtilUI' , exists = True ) :
        pm.deleteUI ( 'hairSetupUtilUI' ) ;
    else : pass ;
   
    window = pm.window ( 'hairSetupUtilUI', title = "hair setup utility v0.0" ,
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'hairSetupUtilUI' , e = True , w = width , h = 100 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 , w = width ) ;
        with mainLayout :
            

            step000_frameLayout = pm.frameLayout ( label = 'STEP 0 : CLEAN UP' , cc = frameLayout_cc , collapse = False , collapsable = True , bgc = ( 0 , 0 , 0.5 ) ) ;
            with step000_frameLayout :

                step000_mainLayout = pm.rowColumnLayout ( nc = 1 , w = width ) ;
                with step000_mainLayout :

                    fillerLine ( ) ;
                    
                    with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width / 20 ) , ( 2 , width / 10 * 9 ) , ( 3 , width / 20 ) ] ) :

                        filler ( ) ;
                        
                        with pm.rowColumnLayout ( nc = 1 , w = width / 10 * 9 ) :
                            
                            '''
                            pm.text ( label = 'general cleaning utilities' , w  = width / 10 * 9 ) ;
                            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/10*9/2 ) , ( 2 , width/10*9/2 ) ] ) :
                                pm.button ( label = 'freeze transform' , c = freezeTransform_cmd , w = width/10*9/2 ) ;
                                pm.button ( label = 'delete history' , c = deleteHistory_cmd , w = width/10*9/2 ) ;
                            #pm.button ( label = 'freeze transform / delete history' , w = width / 10 * 9 , c = freezeTransform_deleteHistory ) ;
                            pm.button ( label = 'center pivot' , w = width / 10 * 9 , c = centerPivot ) ;
                        
                            fillerLine ( ) ;
                            '''
                            pm.text ( label = 'curve cleaning utilities' , w  = width / 10 * 9 ) ;
                            pm.button ( label = 'delete rebuilt shape' , w  = width / 10 * 9 , c = deleteRebuiltShape ) ;

                            fillerLine ( ) ;
        
                            pm.text ( label = 'polygon cleaning utilities' , w  = width / 10 * 9 ) ;
                            pm.button ( label = 'delete orig shape' , w  = width / 10 * 9 , c = deleteShapeOrig ) ;        

                    fillerLine ( ) ;

            step010_frameLayout = pm.frameLayout ( label = 'STEP 1 : ASSIGN HAIR LAYER' , cc = frameLayout_cc , collapse = False , collapsable = True , bgc = ( 0 , 0 , 0.5 ) ) ;
            with step010_frameLayout :

                step010_mainLayout = pm.rowColumnLayout ( nc = 1 , w = width ) ;
                with step010_mainLayout :

                    fillerLine ( ) ;

                    step010_uiLayout = pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width / 20 ) , ( 2 , width / 10 * 9 ) , ( 3 , width / 20 ) ] ) ;
                    with step010_uiLayout :

                        filler ( ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width / 10 * 8 ) :

                            pm.button ( label = 'create layer' , c = initializeScene , w = width / 10 * 9 ) ;
                        
                            fillerLine ( ) ;

                            with pm.rowColumnLayout ( nc  = 4 , cw =
                                [ ( 1 , width / 10 * 2.25 ) , ( 2 , width / 10 * 2.25 ) , ( 3 , width / 10 * 2.25 ) , ( 4 , width / 10 * 2.25 ) ] ) :

                                pm.button ( label = 'green' , c = assignLayer1 , bgc = ( 0.5 , 1 , 0 ) ) ;
                                pm.button ( label = 'blue' , c = assignLayer2 , bgc = ( 0 , 1 , 1 ) ) ;
                                pm.button ( label = 'red' , c = assignLayer3 , bgc = ( 1 , 0 , 0 ) ) ;
                                pm.button ( label = 'yellow' , c = assignLayer4 , bgc = ( 1 , 1 , 0 ) ) ;
                                
                            pm.button ( label = 'visibility' , c = layerVisToggle , w = width / 10 * 9 ) ;

                            fillerLine ( ) ;

                            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/10*4.5 ) , ( 2 , width/10*4.5 ) ] ) :
                                pm.button ( label = 'select step 2' , c = selectStep2 , bgc = ( 0 , 1 , 0 ) ) ;
                                pm.button ( label = 'select step 3' , c = selectStep3 , bgc = ( 0.678431 , 1 , 0.184314 ) ) ;
                            
                        filler ( ) ;

                    fillerLine ( ) ;
                
            pm.separator ( vis = True , h = 15 , bgc = ( 0 , 0 , 0 ) ) ;

            step020_frameLayout = pm.frameLayout ( label = 'STEP 2 : DYN RIG SETUP' , cc = frameLayout_cc , collapse = False , collapsable = True , bgc = ( 0 , 0 , 0.5 ) ) ;
            with step020_frameLayout :

                step020_mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
                with step020_mainLayout :
                
                    with pm.rowColumnLayout ( nc = 5 , cw = [ ( 1 , width / 5  ) , ( 2 , width / 5 ) , ( 3 , width / 5 ) , ( 4 , width / 5 ) , ( 5 , width / 5 ) ] ) :
                       
                        filler ( ) ;
                        pm.text ( label = 'edge 1' ) ;
                        filler ( ) ;
                        pm.text ( label = 'edge 2' ) ;
                        filler ( ) ;

                        filler ( ) ;
                        pm.textField ( 'edge1_txtField' ) ;
                        filler ( ) ;
                        pm.textField ( 'edge2_txtField' ) ;
                        filler ( ) ;

                    fillerLine ( ) ;

                    with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width / 5 ) , ( 2 , width / 5 * 3 ) , ( 3 , width / 5 ) ] ) :

                        filler ( ) ;
                        pm.button ( label = 'get edges' , c = getEdges_cmd , bgc = ( 1 , 0.8 , 0 ) ) ;
                        filler ( ) ;

                    pm.separator ( vis = False , h = 5 ) ;

                    with pm.rowColumnLayout ( nc = 5 , cw = [
                        ( 1 , width / 10 * 2 / 3 ) , ( 2 , width / 10 * 4 ) ,
                        ( 3 , width / 10 * 2 / 3 ) , ( 4 , width / 10 * 4 ) , ( 5 , width / 10 * 2 / 3 ) ] ) :

                        filler ( ) ;
                        pm.text ( label = 'operating object' ) ;
                        filler ( ) ;
                        pm.text ( label = 'operating object shape' ) ;
                        filler ( ) ;

                        filler ( ) ;
                        pm.textField ( 'object_textField' ) ;
                        filler ( ) ;
                        pm.textField ( 'objectShape_textField' ) ;
                        filler ( ) ;

                    with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width / 5 ) , ( 2 , width / 5 * 3 ) , ( 3 , width / 5 ) ] ) :

                        filler ( ) ;                  
                        pm.text ( label = 'operating object top node' ) ;
                        filler ( ) ;

                        filler ( ) ;
                        pm.textField ( 'objectRoot_textField' ) ;
                        filler ( ) ;

                    with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width / 5 ) , ( 2 , width/5*3 ) , ( 3 , width / 5 ) ] ) :
                        
                        filler ( ) ;
                        with pm.rowColumnLayout ( nc = 1 , w = width/5*3 ) :         
                            pm.text ( label = 'hair ORI group' , w = width/5*3 ) ;
                            pm.textField ( 'hairElem_textField' , w = width/5*3 ) ;
                            pm.button ( label = 'refresh' , w = width/5*3 , c = getOri_cmd , bgc = ( 0 , 0.98 , 0.6 ) ) ;
                        filler ( ) ;

                    pm.separator ( vis = False , h = 10 ) ;

                    with pm.rowColumnLayout ( nc = 3  , cw = [ ( 1 , width / 20 ) , ( 2 , width / 10 * 9 ) , ( 3 , width / 20 ) ] ) :

                        filler ( ) ;
                        
                        with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width / 10 * 9 / 10 * 4.9 ) , ( 2 , width / 10 * 9 / 10 * 0.2 ) , ( 3 , width / 10 * 9 / 10 * 4.9 ) ] ) :

                            pm.button ( label = 'save' , c = objectInfoSave_cmd ) ;
                            filler ( ) ;
                            pm.button ( label = 'load' , c = objectInfoLoad_cmd ) ;
                       
                        filler ( ) ;

                    pm.separator ( vis = False , h = 2 ) ;

                    with pm.rowColumnLayout ( nc = 3  , cw = [ ( 1 , width / 20 ) , ( 2 , width / 10 * 9 ) , ( 3 , width / 20 ) ] ) :

                        filler ( ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width / 10 * 9 ) :
                            
                            pm.button ( label = 'run' , c = dynSetup_cmd , w = ( width/10*9 ) , bgc = ( 1 , 0.8 , 0 ) ) ;

                            fillerLine ( ) ;

                            pm.button ( label = 'hair tip constraint' , c  = hairTipConstraint_cmd , w = width/10*9 , bgc = ( 0.5 , 1 , 0 ) ) ;

                        filler ( ) ;

                    fillerLine ( ) ;
        
    window.show () ;