from __future__ import print_function ;
import random , os ;
import maya.cmds as mc;
import pymel.core as pm ;
from datetime import datetime ;

import sim.attributeTuner.library.mayaLibrary as attMl ;
reload ( attMl ) ;

import sim.attributeTuner.library.general as attGl ;
reload ( attGl ) ;

def systemTime () :
	rawTime = str ( datetime.time ( datetime.now () ) ) ;
	rawTime2 = rawTime.split ( ':' ) ;
	rawTime3 = rawTime2[2].split( '.' ) ;
	
	hour = rawTime2 [0] ;
	minute = rawTime2 [1] ;
	second = rawTime3 [0] ;
	
	return '%s:%s:%s' % ( hour , minute , second ) ;

# UI reset

objName = '' ;
dummyAttrName = '' ;

### create dummyAttr grp

def createDummyAttrFunc ( name = 'untitled' ) :

	if name == '' :
    		name = 'untitled' ;

	dummyAttr = mc.group ( n = '%s_dummyAttr' % name , em = True ) ;
	dummyAddAttr = ( [ 'random' , 'randomAmplifier' , 'noise' , 'noiseAmplifier' ] ) ;

	for each in dummyAddAttr :
    		mc.addAttr ( dummyAttr , ln = each , at = 'double' , keyable = True ) ;

	mc.setAttr ( '%s.randomAmplifier' % dummyAttr , 1 ) ;
	mc.setAttr ( '%s.noiseAmplifier' % dummyAttr , 1 ) ;

	mc.addAttr ( dummyAttr , ln = '_____' , at = 'double' , keyable = True ) ;
	mc.setAttr ( '%s._____' % dummyAttr , lock = True ) ;

	dummyLockHideAttr = ( [ 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' ] ) ;

	for each in dummyLockHideAttr :
    		mc.setAttr ( '%s.%s' % ( dummyAttr , each ) , lock = True , keyable = False , channelBox = False ) ;

	createAmplifier ( dummyAttr = str ( dummyAttr ) , type = 'random' ) ;
	createAmplifier ( dummyAttr = str ( dummyAttr ) , type = 'noise' ) ;

def createDummyAttr () :
	createDummyAttrFunc ( name = mc.textField ( 'dummyAttrName' , q = True , tx = True ) ) ;
	dummyAttrOptionMenuRefresh () ;

### dummyAttr selector functions

def dummyAttrListRefresher () :

	transformList = mc.ls ( type = 'transform' ) ;

	dummyAttrList = [] ;
	
	for each in transformList :
    		if '_dummyAttr' in each :
        			dummyAttrList.append ( each ) ;

	return dummyAttrList ;

def dummyAttrOptionMenuRefresh () :

	dummyAttrList = dummyAttrListRefresher () ;
	mc.textScrollList ( 'dummyAttrListTxtScroll' , e = True , ra = 1 ) ;

	for i in range ( 0 , len ( dummyAttrList ) ) :
    		mc.textScrollList ( 'dummyAttrListTxtScroll' , e = True , append = dummyAttrList [i] ) ;

	try :
    		mc.text ( 'connectionEditorDummyAttrNme' , e = True , label = '...' ) ;

	except :
    		pass ;

	else :
    		mc.text ( 'connectionEditorDummyAttrNme' , e = True , label = '...' ) ;

    	dummyAttrSelectionStatUpdate () ;

def dummyAttrSelected () :
        
        global dummyAttrName ;

	CEdummyAttrNmUpdate () ;
	mc.select ( mc.textScrollList ( 'dummyAttrListTxtScroll' , q = True , si = True ) ) ;
	dummyAttrName = ( mc.textScrollList ( 'dummyAttrListTxtScroll' , q = True , si = True ) ) ;
	buttonColourRefresh () ;
	dummyAttrSelectionStatUpdate () ;

def deleteDummyAttrFunc () :

	dummyAttr = ( mc.textScrollList ( 'dummyAttrListTxtScroll' , q = True , si = True ) ) ;
	mc.delete ( dummyAttr ) ;
	dummyAttrOptionMenuRefresh () ;

	multiplyDivide = mc.ls ( type = 'multiplyDivide' ) ;

	for each in multiplyDivide :
    		if ( each == '%s_randomAmp' % dummyAttr [0] ) or each == ( '%s_noiseAmp' % dummyAttr [0] ) :
        			mc.delete ( each ) ;

def deleteDummyAttr () :

	try :
    		terminateConnection ( type = 'deleteDummyAttr' ) ;

	except :
    		deleteDummyAttrFunc () ;

	else :
    		deleteDummyAttrFunc () ;

    	dummyAttrSelectionStatUpdate () ;

### randomizer functions

def createAmplifier ( dummyAttr , type ) :
	maNode = mc.createNode ( 'multiplyDivide' , n = '%s_%sAmp' % ( dummyAttr , type ) ) ;
	mc.setAttr ( '%s.op' % maNode , 1 ) ;
	mc.connectAttr ( '%s.%s' % ( dummyAttr , type ) ,  '%s.i1x' % maNode ) ;
	mc.connectAttr ( '%s.%sAmplifier' % ( dummyAttr , type ) , '%s.i2x' % maNode ) ;

def generateRandomValueList ( startFrame = 1 , endFrame = 10 , min = 0 , max = 10  , scale = 0.1 , digits = 2 , evaluate = 1 ) :

	randomValue = [] ;

	for i in range ( startFrame , ( endFrame + 1 ) , evaluate ) :
		generatedRandomValue = (( random.uniform ( min , max ) ) * scale ) ;
		generatedRandomValue = round ( generatedRandomValue , digits ) ;
		randomValue.append ( generatedRandomValue ) ;

	return randomValue ;

def generateRandomEvalFrameList ( startFrame = 1 , endFrame = 100 , minFrame = 1 , maxFrame = 10 ) :

	randomValue = [] ;

	limit = ( endFrame + 1 ) - startFrame ;
	counter = 0 ;

	while counter < limit :

		generatedRandomValue = int ( random.uniform ( ( minFrame - 0.5 ) , ( maxFrame + 0.5 ) ) ) ;
		randomValue.append ( generatedRandomValue ) ;
		counter = counter + generatedRandomValue ;

	generatedRandomValue = int ( random.uniform (( minFrame - 0.5 ) , ( maxFrame + 0.5 ) ) ) ;
	randomValue.append ( generatedRandomValue ) ;

	return randomValue ;

def generateRandomValue () :

	if ( mc.text ( 'connectionEditorDummyAttrNme' , q = True , label = True ) ) == '...' :

		t = systemTime () ;

		mc.scrollField ( 'errorReport' , e = True , text = 'error: ( %s ) no dummyAttr is selected' % t ) ;
		mc.error ( '( %s ) no dummyAttr is selected' % t ) ;

	time = ( ( mc.intField ( 'randStartFrame' , q = True , v = True ) ) - 50 ) ;

	dummyAttr = ( mc.textScrollList ( 'dummyAttrListTxtScroll' , q = True , selectItem = True ) ) ;

	mc.cutKey ( '%s.random' % dummyAttr [0] , cl = 1 ) ;

	randomValueList = generateRandomValueList ( 
		startFrame = ( ( mc.intField ( 'randStartFrame' , q = True , v = True ) ) - 50 ) ,
		endFrame = ( ( mc.intField ( 'randEndFrame' , q = True , v = True ) ) + 50 ) , 
		min = ( mc.floatField ( 'randMinValue' , q = True , v = True ) ) ,
		max = ( mc.floatField ( 'randMaxValue' , q = True , v = True ) ) ,
		scale = ( mc.floatField ( 'randScale' , q = True , v = True ) ) ,
		digits = ( mc.intField ( 'randomDecimal' , q = True , v = True ) ) ,
		evaluate = ( mc.intField ( 'randomFrame' , q = True , v = True ) ) ) ;

	if ( mc.checkBox ( 'randomRandEvalFrame' , q = True , v = True ) ) == True :

		startFrame = ( ( mc.intField ( 'randStartFrame' , q = True , v = True ) ) - 50 ) ;
		endFrame = ( ( mc.intField ( 'randEndFrame' , q  = True , v = True ) ) + 50 ) ;

		randomEvaluationList = generateRandomEvalFrameList ( 
			startFrame = ( ( mc.intField ( 'randStartFrame' , q = True , v = True ) ) - 50 ) ,
			endFrame = ( ( mc.intField ( 'randEndFrame' , q = True , v = True ) ) + 50 ) ,
			minFrame = ( mc.intField ( 'randomRandEvalMinFrame' , q = True , v = True ) ) ,
			maxFrame = ( mc.intField ( 'randomRandEvalMaxFrame' , q = True , v = True ) ) ) ;

		time = ( ( mc.intField ( 'randStartFrame' , q = True , v = True ) ) - 50 ) ;
		counter = 0 ;
		limit = ( ( mc.intField ( 'randEndFrame' , q = True , v = True ) ) + 50 ) ;

		while  time < limit :

			mc.setKeyframe ( dummyAttr [0] , at = 'random' , v = randomValueList [ counter ] , t = time , 
				itt = 'spline' , ott = 'spline' ) ;

			time = time + randomEvaluationList [ counter ] ;
			counter = counter + 1 ; 

		mc.setKeyframe ( dummyAttr [0] , at = 'random' , v = randomValueList [ counter ] , t = time , 
			itt = 'spline' , ott = 'spline' ) ;

	elif ( mc.checkBox ( 'randomRandEvalFrame' , q = True , v = True ) ) != True :

		evaluate = ( mc.intField ( 'randomFrame' , q = True , v = True ) ) ;

		for each in randomValueList :
			mc.setKeyframe ( dummyAttr [0] , at = 'random' , v = each , t = time , itt = 'spline' , ott = 'spline' ) ;
			time = time + evaluate ;

def randStartFrameEnter () :

	randStartFrame = mc.intField ( 'randStartFrame' , q = True , v = True ) ;
	randEndFrame = mc.intField ( 'randEndFrame' , q = True , v = True ) ;

	if randStartFrame >= randEndFrame :
		mc.intField ( 'randEndFrame' , e = True , v = ( randStartFrame + 1 ) ) ;
		mc.intField ( 'noiseEndFrame' , e = True , v = ( randStartFrame + 1 ) ) ; 

	if randStartFrame != ( mc.intField ( 'noiseStartFrame' , q = True , v = True ) ) :
		mc.intField ( 'noiseStartFrame' , e = True , v = randStartFrame ) ;

def randEndFrameEnter () :

	randStartFrame = mc.intField ( 'randStartFrame' , q = True , v = True ) ;
	randEndFrame = mc.intField ( 'randEndFrame' , q = True , v = True ) ;

	if randEndFrame <= randStartFrame :
		mc.intField ( 'randStartFrame' , e = True , v = ( randEndFrame -1 ) ) ;
		mc.intField ( 'noiseStartFrame' , e = True , v = ( randEndFrame -1 ) ) ;

	if randEndFrame != ( mc.intField ( 'noiseEndFrame' , q = True , v = True ) ) :
		mc.intField ( 'noiseEndFrame' , e = True , v = randEndFrame ) ;

def randMinValueEnter () :

	randMaxValue = mc.floatField ( 'randMaxValue' , q = True , v = True ) ;
	randMinValue = mc.floatField ( 'randMinValue' , q = True , v = True ) ;

	if randMinValue >= randMaxValue :
		mc.floatField ( 'randMaxValue' , e = True , v = ( randMinValue + 1 ) ) ;

def randMaxValueEnter () :

	randMaxValue = mc.floatField ( 'randMaxValue' , q = True , v = True ) ;
	randMinValue = mc.floatField ( 'randMinValue' , q = True , v = True ) ;

	if randMaxValue <= randMinValue :
		mc.floatField ( 'randMinValue' , e = True , v = ( randMaxValue - 1 ) ) ;

def randEvaluateEnter () :
	randEval = mc.intField ( 'randomFrame' , q = True , v = True ) ;
	noiseEval = mc.intField ( 'noiseFrame' , q = True , v = True ) ;

	if randEval != noiseEval :
		mc.intField ( 'noiseFrame' , e = True , v = randEval ) ;

def randomRanEvalMinEnter () :
	min = ( mc.intField ( 'randomRandEvalMinFrame' , q = True , v = True ) ) ;
	max = ( mc.intField ( 'randomRandEvalMaxFrame' , q = True , v = True ) ) ;

	if min >= max :
		mc.intField ( 'randomRandEvalMaxFrame' , e = True , v = ( min + 1 ) ) ;

def randomRanEvalMaxEnter () :
	min = ( mc.intField ( 'randomRandEvalMinFrame' , q = True , v = True ) ) ;
	max = ( mc.intField ( 'randomRandEvalMaxFrame' , q = True , v = True ) ) ;

	if max <= min :
		mc.intField ( 'randomRandEvalMinFrame' , e = True , v = ( max - 1 ) ) ;

def randomRanEvalCheckBoxOn () :
	
	mc.intField ( 'randomRandEvalMinFrame' , e = True , enable = 1 ) ;
	mc.intField ( 'randomRandEvalMaxFrame' , e = True , enable = 1 ) ;
	
	mc.intField ( 'randomFrame' , e = True , enable = 0 ) ;

def randomRanEvalCheckBoxOff () :
	
	mc.intField ( 'randomRandEvalMinFrame' , e = True , enable = 0 ) ;
	mc.intField ( 'randomRandEvalMaxFrame' , e = True , enable = 0 ) ;	
	
	mc.intField ( 'randomFrame' , e = True , enable = 1 ) ;

### noise functions 

def generateNoiseValueList ( startFrame = 1  , endFrame = 100 , scale = 0.1 , evaluate = 1 ) :
	noiseValue = [] ;
	
	for i in range ( startFrame , ( endFrame + 1 ) , evaluate ) :
		noiseValue.append ( ( random.choice ( [ -1 , -0.5 , 0 , 0.5 , 1 ] ) ) * scale ) ;
		
	return noiseValue ;

def generateNoiseValue () :
	
	if ( mc.text ( 'connectionEditorDummyAttrNme' , q = True , label = True ) ) == '...' :
		
		t = systemTime () ;
		
		mc.scrollField ( 'errorReport' , e = True , text = 'error: ( %s ) no dummyAttr is selected' % t ) ;
		
		mc.error ( 'error: ( %s ) no dummyAttr is selected' % t ) ;
		
	dummyAttr = ( mc.textScrollList ( 'dummyAttrListTxtScroll' , q = True , selectItem = True ) ) ;
	
	mc.cutKey ( '%s.noise' % dummyAttr [0] , cl = 1 ) ;
	
	time = ( ( mc.intField ( 'noiseStartFrame' , q = True , v = True ) ) - 50 ) ;
	
	noiseValueList = generateNoiseValueList ( 
		startFrame = ( ( mc.intField ( 'noiseStartFrame' , q = True , v = True ) ) - 50 ) ,
		endFrame = ( ( mc.intField ( 'noiseEndFrame' , q = True , v = True ) ) + 50 ) ,
		scale = ( mc.floatField ( 'noiseScale' , q = True , v = True ) ) ,
		evaluate = ( mc.intField ( 'noiseFrame' , q = True , v = True ) ) ) ;
	
	if ( mc.checkBox ( 'randomNoiseEvalFrame' , q = True , v = True ) ) == True :
		
		startFrame = ( ( mc.intField ( 'noiseStartFrame' , q = True , v = True ) ) - 50 ) ;
		endFrame = ( ( mc.intField ( 'noiseEndFrame' , q = True , v = True ) ) + 50 ) ;
	
		randomEvaluationList = generateRandomEvalFrameList ( 
			startFrame = ( ( mc.intField ( 'noiseStartFrame' , q = True , v = True ) ) - 50 ) ,
			endFrame = ( ( mc.intField ( 'noiseEndFrame' , q = True , v = True ) ) + 50 ) ,
			minFrame = ( mc.intField ( 'randomNoiseEvalMinFrame' , q = True , v = True ) ) ,
			maxFrame = ( mc.intField ( 'randomNoiseEvalMaxFrame' , q = True , v = True ) ) ) ;
	
		time = ( ( mc.intField ( 'noiseStartFrame' , q = True , v = True ) ) - 50 ) ;
		counter = 0 ;
		limit = ( ( mc.intField ( 'noiseEndFrame' , q = True , v = True ) ) + 50 ) ;
		
		while time < limit :
			
			mc.setKeyframe ( dummyAttr [0] , at = 'noise' , v = noiseValueList [ counter ] , 
				t = time , itt = 'spline' , ott = 'spline' ) ;
				
			time = time + randomEvaluationList [counter] ;
			counter = counter + 1 ;
			
		mc.setKeyframe ( dummyAttr [0] , at = 'noise' , v = noiseValueList [ counter ] , 
			t = time , itt = 'spline' , ott = 'spline' ) ;
	
	elif ( mc.checkBox ( 'randomNoiseEvalFrame' , q = True , v = True ) ) != True :
		
		evaluate = ( mc.intField ( 'noiseFrame' , q = True , v = True ) ) ;
		
		for each in noiseValueList :
			
			mc.setKeyframe ( dummyAttr [0] , at = 'noise' , v = each , t = time , 
				itt = 'spline' , ott = 'spline' ) ;
			
			time = time + evaluate ;

def noiseStartFrameEnter () :
	
	noiseStartFrame = mc.intField ( 'noiseStartFrame' , q = True , v = True ) ;
	noiseEndFrame = mc.intField ( 'noiseEndFrame' , q = True , v = True ) ;
	
	if noiseStartFrame >= noiseEndFrame :
		mc.intField ( 'noiseEndFrame' , e = True , v = ( noiseStartFrame + 1 ) ) ;
		mc.intField ( 'randEndFrame' , e = True , v = ( noiseStartFrame + 1 ) ) ;	
		
	if noiseStartFrame != ( mc.intField ( 'randStartFrame' , q = True , v = True ) ) :
		mc.intField ( 'randStartFrame' , e = True , v = noiseStartFrame ) ;

def noiseEndFrameEnter () :
	
	noiseStartFrame = mc.intField ( 'noiseStartFrame' , q = True , v = True ) ;
	noiseEndFrame = mc.intField ( 'noiseEndFrame' , q = True , v = True ) ;
	
	if noiseEndFrame <= noiseStartFrame :
		mc.intField ( 'noiseStartFrame' , e = True , v = ( noiseEndFrame - 1 ) ) ;
		mc.intField ( 'randStartFrame' , e = True , v = ( noiseEndFrame - 1 ) ) ;	
		
	if noiseEndFrame != ( mc.intField ( 'randEndFrame' , q = True , v = True ) ) :
		mc.intField ( 'randEndFrame' , e = True , v = noiseEndFrame ) ;

def noiseEvaluateEnter () :
	
	randEval = mc.intField ( 'randomFrame' , q = True , v = True ) ;
	noiseEval = mc.intField ( 'noiseFrame' , q = True , v = True ) ;
	
	if noiseEval != randEval :
		mc.intField ( 'randomFrame' , e = True , v = noiseEval ) ;

def randomNoiseEvalMinEnter () :
	min = ( mc.intField ( 'randomNoiseEvalMinFrame' , q = True , v = True ) ) ;
	max = ( mc.intField ( 'randomNoiseEvalMaxFrame' , q = True , v = True ) ) ;
	
	if min >= max :
		mc.intField ( 'randomNoiseEvalMaxFrame' , e = True , v = ( min + 1 ) ) ;

def randomNoiseEvalMaxEnter () :
	min = ( mc.intField ( 'randomNoiseEvalMinFrame' , q = True , v = True ) ) ;
	max = ( mc.intField ( 'randomNoiseEvalMaxFrame' , q = True , v = True ) ) ;
	
	if max <= min :
		mc.intField ( 'randomNoiseEvalMinFrame' , e = True , v = ( max - 1 ) ) ;

def randomNoiseEvalCheckBoxOn () :
	
	mc.intField ( 'randomNoiseEvalMinFrame' , e = True , enable = 1 ) ;
	mc.intField ( 'randomNoiseEvalMaxFrame' , e = True , enable = 1 ) ;
	
	mc.intField ( 'noiseFrame' , e = True , enable = 0 ) ;
	
def randomNoiseEvalCheckBoxOff () :
	
	mc.intField ( 'randomNoiseEvalMinFrame' , e = True , enable = 0 ) ;
	mc.intField ( 'randomNoiseEvalMaxFrame' , e = True , enable = 0 ) ;
	
	mc.intField ( 'noiseFrame' , e = True , enable = 1 ) ;

### connectionEditor function

def attributeFilter () :
	
	object = ( mc.text ( 'connectionEditorObjNme' , q = True , label = True ) ) ;
	textFilter = ( mc.textField ( 'connectionEditorObjTxtFilter' , q = True , text = True ) ) ;
	
	if object == '...' or '' or None :		
		pass ;
		
	else :
		
		rawList = mc.listAttr ( object , keyable = True , unlocked = True , visible = True ) ;
		
		attrList = [] ;
		
		for each in rawList :
			
			if textFilter in each :
				attrList.append ( each ) ;
				
		mc.textScrollList ( 'connectionEditorObjTxtScroll' , e = True , ra = 1 , append = attrList ) ;

def buttonColourRefresh () :
	
	obj = ( mc.text ( 'connectionEditorObjNme' , q = True , label = True ) ) ;
	attr = ( mc.textScrollList ( 'connectionEditorObjTxtScroll' , q= True , si = True ) ) ;
	
	dummyAttr = ( mc.textScrollList ( 'dummyAttrListTxtScroll' , q = True , si = True ) ) ;
	
	if dummyAttr == None or attr == None or obj == None :
		
		mc.button ( 'connectRandom' , e = True , bgc = ( 0.4 , 0.4 , 0.4 ) ) ;
		mc.button ( 'connectNoise' , e = True , bgc = ( 0.4 , 0.4 , 0.4 ) ) ;
		
	else :
		
		connections = mc.listAttr ( dummyAttr , unlocked = True , visible = True ) ;
		
		try :
			
			for each in connections :
				continue ;
				
		except :
			
			pass ;
			
		else :
			
			for each in connections :
				
				if ( str ( dummyAttr [0] ) ) and ( str ( obj ) ) and ( str ( attr [0] ) ) in each :
					
					connection = 'yes' ;
					break ;
					
				else :
					
					connection = 'no' ;
			
			if connection == 'yes' :
				
				try :
					mc.getAttr ( '%s_%s_opAttr.switchRandom' % ( obj , attr [0] ) ) ;
					mc.getAttr ( '%s_%s_opAttr.switchNoise' % ( obj , attr [0] ) ) ;
					
				except :
					mc.button ( 'connectRandom' , e = True , bgc = ( 0.4 , 0.4 , 0.4 ) ) ;
					mc.button ( 'connectNoise' , e = True , bgc = ( 0.4 , 0.4 , 0.4 ) ) ;
					
				else :
					switchRandom = mc.getAttr ( '%s_%s_opAttr.switchRandom' % ( obj , attr [0] ) ) ;
					switchNoise = mc.getAttr ( '%s_%s_opAttr.switchNoise' % ( obj , attr [0] ) ) ;
					
					if switchRandom == 0 :
						mc.button ( 'connectRandom' , e = True , ebg = 1 , bgc = ( 1 , 0 , 0 ) ) ;
						
					elif switchRandom != 0 :
						mc.button ( 'connectRandom' , e = True , ebg = 1 , bgc = ( 0 , 1 , 0 ) ) ;
						
					if switchNoise == 0 :
						mc.button ( 'connectNoise' , e = True , ebg = 1 , bgc = ( 1 , 0 , 0 ) ) ;
						
					elif switchNoise != 0 :
						mc.button ( 'connectNoise' , e = True , ebg = 1 , bgc = ( 0 , 1 , 0 ) ) ;
						
			else :
				mc.button ( 'connectRandom' , e = True , bgc = ( 0.4 , 0.4 , 0.4 ) ) ;
				mc.button ( 'connectNoise' , e = True , bgc = ( 0.4 , 0.4 , 0.4 ) ) ;

def CEobjectNmUpdate () :
	
	name = mc.ls ( selection = True ) ;
	
	if name == [] :
		
		mc.textScrollList ( 'connectionEditorObjTxtScroll' , e = True , ra = 1 ) ;
		
		mc.text ( 'connectionEditorObjNme' , e = True , label = '...' ) ;
	
	else :
		
		mc.text ( 'connectionEditorObjNme' , e = True , label = str ( name [0] ) ) ;
		
		mc.textScrollList ( 'connectionEditorObjTxtScroll' , e = True , ra = 1 ) ;
		
		attrList = mc.listAttr ( keyable = True , unlocked = True , visible = True ) ;
		
		mc.textScrollList ( 'connectionEditorObjTxtScroll' , e = True , append = attrList ) ;
	
	global objName ;

	objName = name ;
	
	attributeFilter () ;

	buttonColourRefresh () ;

def CEdummyAttrNmUpdate () :
	name = mc.textScrollList ( 'dummyAttrListTxtScroll' , q = True , selectItem = True ) ;
	mc.text ( 'connectionEditorDummyAttrNme' , e = True , label = str ( name [0] ) ) ;

def connect ( type , operator = '' , obj = '' , dummyAttr = '' , objAttr = '' , randomDisable = '' , noiseDisable = '' ) :
	
### copy attribute + key frames 

	obj = objName ;
	dummyAttr = dummyAttrName ;
	objAttr = ( mc.textScrollList ( 'connectionEditorObjTxtScroll' , q = True , si = True ) ) ;

	if randomDisable == 'True' :
		try :
			mc.setAttr ( '%s_%s_opAttr.switchRandom' % ( obj [0] , objAttr [0] ) , 0 ) ;
			buttonColourRefresh () ;
		except :			
			t = systemTime () ;
			
			mc.scrollField ( 'errorReport' , e = True , 
				text = 'error: ( %s ) selected attribute has no connection to dummyAttr' % t ) ;
		
			mc.error ( 'error: ( %s ) selected attribute has no connection to dummyAttr' % t ) ;
			
	elif noiseDisable == 'True' :
		try:
			mc.setAttr ( '%s_%s_opAttr.switchNoise' % ( obj [0] , objAttr[0] ) , 0 ) ;
			buttonColourRefresh () ;
		except :
			t = systemTime () ;
			
			mc.scrollField ( 'errorReport' , e = True , 
				text = 'error: ( %s ) selected attribute has no connection to dummyAttr' % t ) ;
		
			mc.error ( 'error: ( %s ) selected attribute has no connection to dummyAttr' % t ) ;
				
	else :
		
		for i in range ( 0 , len ( objAttr ) ) :
			
			attrType = mc.getAttr ( '%s.%s' % ( obj [0] , objAttr[i] ) , type = 1 ) ;
			
			###
			
			if attrType == 'bool' :
				attrType = 'long' ;
			
			elif attrType == 'doubleLinear' or 'doubleAngle' or 'double' :
				attrType = 'double' ;
				
			else :
				
				t = systemTime () ;
				
				mc.scrollField ( 'errorReport' , e = True , 
					text = 'error: ( %s ) This attribute is not applicable' % t ) ;
		
				mc.error ( 'error: ( %s ) This attribute is not applicable' % t ) ;
				
			###
			
			try :
				
				mc.getAttr ( '%s.%s_%s' % ( dummyAttr [0] , obj [0] , objAttr [i] ) ) ;
				
			except : # the attribute doesn't exist
			
				mc.addAttr ( str ( dummyAttr [0] ) , longName = '%s_%s' % ( obj [0] , objAttr [i] ) , 
					at = attrType , keyable = 1 ) ;
			
			else : # the attribute exists
				pass ;
				
			try :
				print ( 'okay, problem here 1' ) ;

				timeStart = mc.playbackOptions ( q = True , min = True ) ;
				timeEnd = mc.playbackOptions ( q = True , max = True ) ;
				
				mc.copyKey ( obj [0] , attribute = objAttr [i] ) ;
				mc.pasteKey ( dummyAttr [0] , attribute = '%s_%s' % ( obj [0] , objAttr [i] ) ,
                                        option = 'insert' , copies = 1 , connect = 1 ,
                                        timeOffset = 0 , floatOffset = 0 , valueOffset = 0 ) ;
				
			except :
				
				if mc.objExists ( '%s_%s_opAttr_finalValue_ex' % ( obj [0] , objAttr [i] ) ) == True :
					
					if type == 'random' :
						mc.setAttr ( '%s_%s_opAttr.switchRandom' % ( obj [0] , objAttr [i] ) , 1 ) ;
						buttonColourRefresh () ;
						
					elif type == 'noise' :
						mc.setAttr ( '%s_%s_opAttr.switchNoise' % ( obj [0] , objAttr [i] ) , 1 ) ;
						buttonColourRefresh () ;
						
				else :
					
					value = mc.getAttr ( '%s.%s' % ( obj [0] , objAttr [i] ) ) ;
					mc.setAttr ( '%s.%s_%s' % ( dummyAttr [0] , obj [0] , objAttr [0] ) , value ) ;
	
			else :
                                print ( 'okay, problem here 2' ) ;

				#mc.copyKey ( obj [0] , attribute = objAttr [i] ) ;
				#mc.pasteKey ( dummyAttr [0] , attribute = '%s_%s' % ( obj [0] , objAttr [i] ) ) ;
				#mc.cutKey ( '%s.%s_%s' % ( dummyAttr [0] , obj [0] , objAttr [i] ) , cl = 1 ) ;
				
				
			# create operated attributes 
			
			if ( mc.objExists ( '%s_%s_opAttr' % ( obj [0] , objAttr [i] ) ) == True ) :
				
				operatedAttr = ( '%s_%s_opAttr' % ( obj [0] , objAttr [i] ) ) ;
				
			else :
				
				operatedAttr = mc.group ( n = '%s_%s_opAttr' % ( obj [0] , objAttr [i] ) , em = True ) ;
				
				operatedAddAttr = ( 
					[ 'plusRandom' , 'multiplyRandom' , 'nullRandom' , 'switchRandom' , 'outRandom' ,
					'plusNoise' , 'multiplyNoise' , 'nullNoise' , 'switchNoise' , 'outNoise' , 
					'finalValue' ] ) ;
					
				for j in operatedAddAttr :
					mc.addAttr ( operatedAttr , ln = j , at = attrType , keyable = True ) ;
					
				operatedLockHideAttr = ( 
					[ 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' ] ) ;
					
				for k in operatedLockHideAttr :
					mc.setAttr ( '%s.%s' % ( operatedAttr , k ) , lock = True , keyable = False , channelBox = False ) ;
					
				mc.expression ( n = '%s_plusRandom_ex' % operatedAttr , o = operatedAttr , 
					s = '%s.plusRandom = ( %s_randomAmp.ox ) + ( %s.%s_%s )' % (
						operatedAttr , dummyAttr [0] , dummyAttr [0] , obj [0] , objAttr [i] ) ) ;
						
				mc.expression ( n = '%s_multiplyRandom_ex' % operatedAttr , o = operatedAttr , 
					s = '%s.multiplyRandom = ( %s_randomAmp.ox ) * ( %s.%s_%s )' % (
						operatedAttr , dummyAttr [0] , dummyAttr [0] , obj [0] , objAttr [i] ) ) ;
						
				mc.expression ( n = '%s_nullRandom_ex' % operatedAttr , o = operatedAttr , 
					s = '%s.nullRandom = ( %s.%s_%s )' % ( 
						operatedAttr , dummyAttr [0] , obj [0] , objAttr [i] ) ) ;
	
				mc.expression ( n = '%s_switchRandom_ex' % operatedAttr , o = operatedAttr , 
					s = 'if ( %s.switchRandom == 0 ) { %s.outRandom = %s.nullRandom ; } \
					else if ( %s.switchRandom == 1 ) { %s.outRandom = %s.plusRandom ; } \
					else if ( %s.switchRandom == 2 ) { %s.outRandom = %s.multiplyRandom ; }' % ( 
						operatedAttr , operatedAttr , operatedAttr , operatedAttr , 
						operatedAttr , operatedAttr , operatedAttr , operatedAttr , 
						operatedAttr ) ) ;
						
				mc.expression ( n = '%s_plusNoise_ex' % operatedAttr , o = operatedAttr , 
					s = '%s.plusNoise = ( %s_noiseAmp.ox ) + ( %s.outRandom )' % ( 
						operatedAttr , dummyAttr [0] , operatedAttr ) ) ;
						
				mc.expression ( n = '%s_multiplyNoise_ex' % operatedAttr , o = operatedAttr , 
					s = '%s.multiplyNoise = ( %s_noiseAmp.ox ) * ( %s.outRandom )' % ( 
						operatedAttr , dummyAttr [0] , operatedAttr ) ) ;
						
				mc.expression ( n = '%s_nullNoise_ex' % operatedAttr , o = operatedAttr , 
					s = '%s.nullNoise = %s.outRandom' % ( 
						operatedAttr, operatedAttr ) ) ;
						
				mc.expression ( n = '%s_switchNoise_ex' % operatedAttr , o = operatedAttr , 
					s = 'if ( %s.switchNoise == 0 ) { %s.outNoise = %s.nullNoise ; } \
					else if ( %s.switchNoise == 1 ) { %s.outNoise = %s.plusNoise ; } \
					else if ( %s.switchNoise == 2 ) { %s.outNoise = %s.multiplyNoise ; }' % (
						operatedAttr , operatedAttr , operatedAttr , operatedAttr , 
						operatedAttr , operatedAttr , operatedAttr , operatedAttr , 
						operatedAttr ) ) ;
						
				mc.expression ( n = '%s_finalValue_ex' % operatedAttr , o = operatedAttr , 
					s = '%s.finalValue = %s.outNoise' % ( operatedAttr, operatedAttr ) ) ;
					
				# connect operated attribute to the real attribute 
				
				mc.cutKey ( '%s.%s' % ( obj [0] , objAttr [i] ) , cl = 1 ) ;
				
				mc.expression ( n = '%s_realValue_ex' % operatedAttr, o = obj [0] , 
					s = '%s.%s = %s.finalValue' % ( obj [0] , objAttr [i] , operatedAttr ) ) ; 
	
			# check if the button pressed was [ random ] or [ noise ]
			
			randomOp = mc.optionMenu ( 'randOp' , q = True , v = True ) ;
			noiseOp = mc.optionMenu ( 'noiseOp' , q = True , v = True ) ;
			
				# random OP
				
			if randomOp == '+' :
				randomOp = 1 ;
				
			elif randomOp == 'x' :
				randomOp = 2 ;
				
				# noiseOp
				
			if noiseOp == '+' :
				noiseOp = 1 ;
				
			elif noiseOp == 'x' :
				noiseOp = 2 ;
				
			if type == 'random' :
				
				if randomDisable == 'True' :	
					mc.setAttr ( '%s.switchRandom' % operatedAttr, 0 ) ;
				
				else :
					mc.setAttr ( '%s.switchRandom' % operatedAttr , randomOp ) ;
					
			if type == 'noise' :
				
				if noiseDisable == 'True' :
					mc.setAttr ( '%s.switchNoise' % operatedAttr , 0 ) ;
					
				else :
					mc.setAttr ( '%s.switchNoise' % operatedAttr , noiseOp ) ;
					
			buttonColourRefresh () ;

def terminateConnection ( type = '' ) :
	
	obj = mc.text ( 'connectionEditorObjNme' , q = True , label = True ) ;
	objAttr = mc.textScrollList ( 'connectionEditorObjTxtScroll' , q = True , si = True ) ;
	
	rawExpressions = mc.ls ( type = 'expression' ) ;
	expressions = [] ;
	
	for each in rawExpressions :
		if obj and objAttr [0] in each :
			expressions.append ( each ) ;
			
	for each in expressions :
		mc.delete ( each ) ;
		
	mc.delete ( '%s_%s_opAttr' % ( obj , objAttr [0] ) ) ;
	
	dummyAttr = mc.textScrollList ( 'dummyAttrListTxtScroll' , q = True , si = True ) ;
	
	try :
		
		mc.getAttr ( '%s.%s_%s' % ( dummyAttr [0] , obj , objAttr [0] ) ) ;
		
	except :
			
		pass ;
			
	else :
			
		mc.deleteAttr ( '%s.%s_%s' % ( dummyAttr [0] , obj , objAttr [0] ) ) ;
			
	if type != 'deleteDummyAttr' :
			
		createAmplifier ( dummyAttr = str ( dummyAttr [0] ) , type = 'random' ) ;
		createAmplifier ( dummyAttr = str ( dummyAttr [0] ) , type = 'noise' ) ;
			
	buttonColourRefresh () ;

# ------
		
def connectTypeElementChecker () :
	
	if mc.text ( 'connectionEditorObjNme' , q = True , label = True ) == '...' or \
	mc.text ( 'connectionEditorDummyAttrNme' , q = True , label = True ) == '...' :
		
		t = systemTime () ;
		
		mc.scrollField ( 'errorReport' , e = True , 
			text = 'error: ( %s ) object or/and dummyAttr is not selected' % t ) ; 
			
		mc.error ( '( %s ) object or/and dummyAttr is not selected' % t ) ;
		
	else :
		
		pass ;
		
def connectTypeRandom () :
	connectTypeElementChecker () ;
	connect ( type = 'random' ) ;
	
def connectTypeRandomDisable () :
	connectTypeElementChecker () ;
	connect ( type = 'random' , randomDisable = 'True' ) ;

def connectTypeNoise () :
	connectTypeElementChecker () ;
	connect ( type = 'noise' ) ;
	
def connectTypeNoiseDisable () :
	connectTypeElementChecker () ;
	connect ( type = 'noise' , noiseDisable = 'True' ) ;

def dummyAttrSelectionStatUpdate ( *arg ) :

        stat = 	mc.textScrollList ( 'dummyAttrListTxtScroll' , q = True , si = True ) ;

        if stat == '' or stat == None :
                stat = [ 'none' ] ;

        if mc.frameLayout ( 'dummyAttrSelectionFrameLayout' , q = True , cl = True ) == True :

                mc.text ( 'dummyAttrSelectionStat' , e = True , h = 15 , align = 'left' ,
                        l = '         current selection : %s' % stat [0] ) ;

        elif mc.frameLayout ( 'dummyAttrSelectionFrameLayout' , q = True , cl = True ) == False :
                mc.text ( 'dummyAttrSelectionStat' , e = True , h = 1 , align = 'left' ,    
                        l = '' ) ;
                
def dummyAttrSelectionStatDisable () :
        mc.text ( 'dummyAttrSelectionStat' , e = True , h = 1 , align = 'left' ,    
                l = '' ) ;
        
# menu

def versionHistory () :

	selfPath = os.path.realpath (__file__) ;
	fileName = os.path.basename (__file__) ;
	selfPath = selfPath.replace ( fileName , '' ) ;
	selfPath = selfPath.replace ( '\\' , '/' ) ;

	txtFile = open ( '%s/attributeTuner_versionHistory.txt' % selfPath ) ;

	for line in txtFile :
		print ( line , end = '' ) ;

	txtFile.close () ;

# --------------------------------------------------------------------------------------------------

## main UI function 

def attrTunerUI () :
	
	# check if window exists
	
	if mc.window ( 'attrTunerUI' , exists = True ) :
		mc.deleteUI ( 'attrTunerUI' ) ;
		
	# create window 
	
	window = mc.window ( 'attrTunerUI' , title = 'attribute tuner beta v1.0' ,
		mnb = False , mxb = False , sizeable = False , rtf = True ) ;
	
	window = mc.window ( 'attrTunerUI' , e = True , h = 720 ) ;

	# mainLayout 
	
	mainLayout = mc.rowColumnLayout ( nc = 1 ) ;

	# menu

	mc.separator ( height = 5 , style = 'none' ) ;
	
	menuBarLayout = mc.menuBarLayout()
        mc.menu( label='help' ) ;
        mc.menuItem ( l = 'how to' , c = attMl.Callback ( attributeTunerHelpUI ) ) ;

        mc.menu( label='data' ) ;
        mc.menuItem( label = 'version history' , c = attMl.Callback ( versionHistory ) ) ;
                
	mc.setParent ( '..' ) ;

	mc.separator ( height = 10 , style = 'in' ) ;
	
	# generate dummyAttr button

        generateDummyAttrFrameLayout = mc.frameLayout ( l = 'generate dummy Attr' , cll = True , cl = False , w = 400 ) ;
	
	mc.text ( label = '' ) ;
	
	generateDummyAttrLayout = mc.rowColumnLayout ( nc = 7 , 
		cw = [ ( 1 , 20 ) , ( 2 , 40 ) , ( 3 , 100 ) , ( 4 , 80 ) , ( 5 , 10 ) , ( 6 , 130 ) , ( 7 , 20 ) ] ) ;
		
	mc.text ( label = '' ) ;
	mc.text ( label = 'name : ' ) ;
	mc.textField ( 'dummyAttrName' ) ;
	mc.text ( label = '_dummyAttr' ) ;
	mc.text ( label = '' ) ;
	mc.button ( label = 'create dummy Attr' , c = attMl.Callback ( createDummyAttr ) ) ;
	mc.text ( label = '' ) ;
	
	mc.setParent ( '..' ) ;

	mc.text ( label = '' ) ;
	
	mc.setParent ( '..' ) ;

	# dummyAttr selector 
	
	mc.separator ( height = 10 , style = 'in' ) ;

        dummyAttrSelectionFrameLayout = mc.frameLayout ( 'dummyAttrSelectionFrameLayout' ,
                l = 'select dummy Attr' , cll = True , cl = False , w = 400 ,
                cc = attMl.Callback ( dummyAttrSelectionStatUpdate ) ,
                ec = attMl.Callback ( dummyAttrSelectionStatDisable ) ) ;

# -----------------------------------------------------------------------------------------------

	mc.text ( label = '' ) ;
	
	dummyAttrSelectionLayout = mc.rowColumnLayout ( nc = 3 , 
		cw = [ ( 1 , 50 ) , ( 2 , 300 ) , ( 3 , 50 ) ] , rs = ( 2 , 3 ) ) ;
		
	mc.text ( label = '' ) ;
	mc.textScrollList ( 'dummyAttrListTxtScroll' , w = 300 , h = 70 , allowMultiSelection = False , 
		sc = attMl.Callback ( dummyAttrSelected ) ) ;
		
	mc.text ( label = '' ) ;
	
	mc.text ( label = '' ) ;
	mc.button ( label = 'refresh' , c = attMl.Callback ( dummyAttrOptionMenuRefresh ) ) ;
	mc.text ( label = '' ) ;
	
	mc.text ( label = '' ) ;
	mc.button ( label = 'delete' , c = attMl.Callback ( deleteDummyAttr ) ) ;
	mc.text ( label = '' ) ;
	
	mc.setParent ( '..' ) ;

        mc.text ( label = '' ) ;

	mc.setParent ( '..' ) ;
	
	mc.text ( 'dummyAttrSelectionStat' , h = 1 , l = '' , align = 'left' ) ;
	
	dummyAttrOptionMenuRefresh () ;
	
	# random value function
	
	mc.separator ( height = 10 , style = 'in' ) ;
	
	randomValueLayout = mc.frameLayout ( l = 'random value' , cll = True , cl = True , w = 400 ) ;

        mc.text ( label = '' ) ;
	
	randomValueElemsLayout = mc.rowColumnLayout ( nc = 7 , 
		cw = [ ( 1 , 10 ) , ( 2 , 76 ) , ( 3 , 76 ) , ( 4 , 76 ) , ( 5 , 76 ) , ( 6 , 76 ) , ( 7 , 10 ) ] ) ; 
		
	mc.text ( label = '' ) ;
	mc.text ( label = 'start frame' ) ; 
	mc.text ( label = 'end frame' ) ;
	mc.text ( label = 'min value' ) ;
	mc.text ( label = 'max value' ) ;
	mc.text ( label = 'scale' ) ;
	mc.text ( label = '' ) ;
	
	mc.text ( label = '' ) ;
	mc.intField ( 'randStartFrame' , v = mc.playbackOptions ( q = True , minTime = True ) , cc = attMl.Callback ( randStartFrameEnter ) ) ;
	mc.intField ( 'randEndFrame' , v = mc.playbackOptions ( q = True , maxTime = True ) , cc = attMl.Callback ( randEndFrameEnter ) ) ;
	mc.floatField ( 'randMinValue' , v = 1 , cc = attMl.Callback ( randMinValueEnter ) ) ;
	mc.floatField ( 'randMaxValue' , v = 10 , cc = attMl.Callback ( randMaxValueEnter ) ) ;
	mc.floatField ( 'randScale' , v = 1 ) ;
	mc.text ( label = '' ) ;
	
	mc.setParent ( '..' ) ;	
	
	randomGenButtonLayout = mc.rowColumnLayout ( nc = 3 , 
		cw = [ ( 1 , 10 ) , ( 2 , 380 ) , ( 3 , 10 ) ] ) ; 
		
	mc.text ( label = '' ) ;
	mc.button ( label = 'generate random value' , c = attMl.Callback ( generateRandomValue ) ) ;
	mc.text ( label = '' ) ;
	
	mc.setParent ( '..' ) ;	
	
	randomValueDecimal = mc.rowColumnLayout ( nc = 8 , 
		cw = [ ( 1 , 10 ) , ( 2 , 83 ) , ( 3 , 76 ) , ( 4 , 30 ) , 
			( 5 , 80 ) , ( 6 , 76 ) , ( 7 , 35 ) , ( 8 , 10 ) ] ) ;
			
	mc.text ( label = '' ) ;
	mc.text ( label = 'decimal digits' ) ;
	mc.intField ( 'randomDecimal' , v = 2 ) ;
	mc.text ( label = '' ) ;
	mc.text ( label = 'evaluate every' ) ;
	mc.intField ( 'randomFrame' , v = 1 , cc = attMl.Callback ( randEvaluateEnter ) ) ;
	mc.text ( label = 'frame' ) ;
	mc.text ( label = '' ) ;
	
	mc.setParent ( '..' ) ;	
	
	mc.text ( label = '' ) ;
	
	mc.rowColumnLayout ( nc = 8 , 
		cw = [ ( 1 , 10 ) , ( 2 , 15 ) , ( 3 , 129 ) , ( 4 , 76 ) ,  
			( 5 , 48 ) , ( 6 , 76 ) , ( 7 , 35 ) , ( 8 , 10 ) ] ) ;
			
	mc.text ( label = '' ) ;
	
	mc.checkBox ( 'randomRandEvalFrame' , 
		onCommand = attMl.Callback ( randomRanEvalCheckBoxOn ) ,
		offCommand = attMl.Callback ( randomRanEvalCheckBoxOff ) ) ;
		
	mc.text ( label = 'random evaluation from' ) ;
	
	mc.intField ( 'randomRandEvalMinFrame' , v = 1 , enable = 0 , 
		cc = attMl.Callback ( randomRanEvalMinEnter ) ) ;
	
	mc.text ( label = 'frame to' ) ;
	
	mc.intField ( 'randomRandEvalMaxFrame' , v = 10 , enable = 0 , 
		cc = attMl.Callback ( randomRanEvalMaxEnter ) ) ;
	
	mc.text ( label = 'frame' ) ;
	mc.text ( label = '' ) ;
	
	mc.setParent ( '..' ) ;

	mc.text ( label = '' ) ;

	mc.setParent ( '..' ) ;
	
	# noise value function 
	
	mc.separator ( height = 10 , style = 'in' ) ;
	
	noiseValueLayout = mc.frameLayout ( l = 'noise value' , cll = True , cl = True , w = 400 ) ;
	mc.text ( label = '' ) ;
	
	mc.rowColumnLayout ( nc = 6 , 
		cw = [ ( 1 , 81 ) , ( 2 , 76 ) , ( 3 , 76 ) , ( 4 , 10 ) , ( 5 , 76 ) , ( 6 , 81 ) ] ) ;
		
	mc.text ( label = '' ) ;
	mc.text ( label = 'start frame' ) ;
	mc.text ( label = 'end frame' ) ;
	mc.text ( label = '' ) ;
	mc.text ( label = 'scale' ) ;
	mc.text ( label = '' ) ;
	
	mc.text ( label = '' ) ;
	
	mc.intField ( 'noiseStartFrame' , v = mc.playbackOptions ( q = True , minTime = True ) , 
		cc = attMl.Callback ( noiseStartFrameEnter ) ) ;
	
	mc.intField ( 'noiseEndFrame' , v = mc.playbackOptions ( q = True , maxTime = True ) ,
		cc = attMl.Callback ( noiseEndFrameEnter ) ) ;
		
	mc.text ( label = '' ) ;
	mc.floatField ( 'noiseScale' , v = 1 ) ;
	mc.text ( label = '' ) ;
		
	mc.setParent ( '..' ) ;	
	
	noiseGenButtonLayout = mc.rowColumnLayout ( nc = 3 , cw = [ ( 1 , 10)  , ( 2 , 380 ) , ( 3 , 10 ) ] ) ;
	
	mc.text ( label = '' ) ;
	mc.button ( label = 'generate noise value' , c = attMl.Callback ( generateNoiseValue ) ) ;
	mc.text ( label = '' ) ;
	
	mc.setParent ( '..' ) ;	
	
	noiseValueEvaluate = mc.rowColumnLayout ( nc = 5 , 
		cw = [ ( 1 , 199 ) , ( 2 , 80 ) , ( 3 , 76 ) , ( 4 , 35 ) , ( 5 , 10 ) ] ) ;
		
	mc.text ( label = '' ) ;
	mc.text ( label = 'evaluate every' ) ;
	mc.intField ( 'noiseFrame' , v = 1 , cc = attMl.Callback ( noiseEvaluateEnter ) ) ;
	mc.text ( label = 'frame' ) ;
	mc.text ( label = '' ) ;
	
	mc.setParent ( '..' ) ;	

	mc.text ( label = '' ) ;
	
	mc.rowColumnLayout ( nc = 8 , 
		cw = [ ( 1 , 10 ) , ( 2 , 15 ) , ( 3 , 129 ) , ( 4 , 76 ) , 
			( 5 , 48 ) , ( 6 , 76 ) , ( 7 , 35 ) , ( 8 , 10 ) ] ) ;
			
	mc.text ( label = '' ) ;
	
	mc.checkBox ( 'randomNoiseEvalFrame' , 
		onCommand = attMl.Callback ( randomNoiseEvalCheckBoxOn ) ,
		offCommand = attMl.Callback ( randomNoiseEvalCheckBoxOff ) ) ;	
		
	mc.text ( label = 'random evaluation from' ) ;
	
	mc.intField ( 'randomNoiseEvalMinFrame' , v = 1 , enable = 0 , 
		cc = attMl.Callback ( randomNoiseEvalMinEnter ) ) ;
		
	mc.text ( label = 'frame to' ) ;
	
	mc.intField ( 'randomNoiseEvalMaxFrame' , v = 10 , enable = 0 , 
		cc = attMl.Callback ( randomNoiseEvalMaxEnter ) ) ; 
		
	mc.text ( label = 'frame' ) ;
	mc.text ( label = '' ) ;
	
	mc.setParent ( '..' ) ;

	mc.text ( label = '' ) ;
	
	mc.setParent ( '..' ) ;
	
	mc.separator ( height = 10 , style = 'in' ) ;
	
	# connection editor 

        connectionEditorFrameLayout = mc.frameLayout ( l = 'connection editor' , cll = True , w = 400 ) ;
	
	mc.text ( label = '' ) ;	

#####

	connectionEditorLayout = mc.rowColumnLayout ( nc = 2 , cw = [ ( 1 , 200 ) , ( 2 , 200 ) ] ) ;

#####
	
	connectionEditorObjectLayout = mc.rowColumnLayout ( nc = 3 , 
		cw = [ ( 1 , 10 ) , ( 2 ,185 ) , ( 3 , 5 ) ] ) ;
		
	mc.text ( label = '' ) ;
	mc.text ( 'connectionEditorObjNme' , label = '...' ) ;
	mc.text ( label = '' ) ;
	
	mc.text ( label = '' ) ;
	mc.textScrollList ( 'connectionEditorObjTxtScroll' , w = 185 , h = 140 , allowMultiSelection = False , 
		sc = attMl.Callback ( buttonColourRefresh ) ) ;
	mc.text ( label = '' ) ;
	
	mc.text ( label = '' ) ;
	mc.textField ( 'connectionEditorObjTxtFilter' , cc = attMl.Callback ( attributeFilter ) ) ;
	mc.text ( label = '' ) ;
	
	mc.text ( label = '' ) ;
	mc.button ( label = 'select object' , c = attMl.Callback ( CEobjectNmUpdate ) ) ;
	mc.text ( label = '' ) ;
	
	mc.setParent ( '..' ) ;
	
#####

	connectionEditorDummyAttrLayout = mc.rowColumnLayout ( nc = 1 ) ;
	
	mc.rowColumnLayout ( nc = 3 , 
		cw = [ ( 1 , 5 ) , ( 2 , 185 ) , ( 3 , 10 ) ] ) ;
	
	mc.text ( label = '' ) ;
	mc.text ( 'connectionEditorDummyAttrNme' , label = '...' ) ;
	mc.text ( label = '' ) ;
	
	mc.setParent ( '..' ) ;
	
	connectionEditorDummyAttrButtonLayout = mc.rowColumnLayout ( nc = 5 , 
		cw = [ ( 1, 5 ) , ( 2 , 40 ) , ( 3 , 115 ) , ( 4 , 30 ) , ( 5 , 10 ) ] ) ; 
	
	#
	
	mc.text ( label = '' ) ;
	
	mc.optionMenu ( 'randOp' , cc = attMl.Callback ( connectTypeRandom ) ) ;
	mc.menuItem ( label = '+' ) ;
	mc.menuItem ( label = 'x' ) ;
	
	mc.button ( 'connectRandom' , label = 'random' , 
		c = attMl.Callback ( connectTypeRandom ) , ebg = 1 , bgc = ( 0.4 , 0.4 , 0.4 ) ) ;
		
	mc.button ( label = 'x' , c = attMl.Callback ( connectTypeRandomDisable ) ) ;
	
	mc.text ( label = '' ) ;
	
	#
	
	mc.text ( label = '' ) ;
	
	mc.optionMenu ( 'noiseOp' , cc = attMl.Callback ( connectTypeNoise ) ) ;
	mc.menuItem ( label = '+' ) ;
	mc.menuItem ( label = 'x' ) ;
	
	mc.button ( 'connectNoise' , label = 'noise' , 
		c = attMl.Callback ( connectTypeNoise ) , ebg = 1 , bgc = ( 0.4 , 0.4 , 0.4 ) ) ;

	mc.button ( label = 'x' , c = attMl.Callback ( connectTypeNoiseDisable ) ) ;
	
	mc.text ( label = '' ) ;
	
	#
	
	mc.setParent ( '..' ) ;
	
	mc.rowColumnLayout ( nc = 3 , cw = [ ( 1, 5 ) , ( 2 , 185 ) , ( 3 , 10 ) ] ) ;

	mc.text ( label = '' ) ;
	mc.button ( label = 'terminate connection' , c = attMl.Callback ( terminateConnection ) ) ;
	mc.text ( label = '' ) ;

	mc.setParent ( '..' ) ;
	
#####

	mc.setParent ( '..' ) ;
	mc.setParent ( '..' ) ;

        mc.text ( label = '' ) ;

        mc.setParent ( '..' ) ;
        
	# error report
	
	####
	
	mc.separator ( height = 10 , style = 'in' ) ;

        errorReportFrameLayout = mc.frameLayout ( l = 'error report' , cll = True , cl = False , w = 400 ) ;
	
	errorReportLayout = mc.rowColumnLayout ( nc = 1 , cw = [ ( 1, 400 ) ] ) ;

	mc.scrollField ( 'errorReport' , editable = False , wordWrap = True , 
		text = 'error report' , w = 380 , h = 40 ) ;
	
	mc.setParent ( '..' ) ;

	mc.setParent ( '..' ) ;	

	####
	
	mc.separator ( height = 5 , style = 'none' ) ;
	
	# show window 
	
	mc.showWindow ( window ) ;

# attrTunerUI () ;

#### help ####

def attributeTunerHelpUI () :

	selfPath = os.path.realpath (__file__) ;
	fileName = os.path.basename (__file__) ;
	selfPath = selfPath.replace ( fileName , '' ) ;
	selfPath = selfPath.replace ( '\\' , '/' ) ;

	image = '%s/attributeTuner_help/eng_page1.jpg' % selfPath ;

	if mc.window ( 'attributeTunerHelpUI' , exists = True ) :
	    mc.deleteUI ( 'attributeTunerHelpUI' ) ;
	
	window = mc.window ( 'attributeTunerHelpUI' , title = "attribute tuner beta v1.0 help" ,
	    mnb = False , mxb = False , sizeable = False ) ;    
	    
	mainLayout = mc.rowColumnLayout ( nc = 1 , cw = [ ( 1 , 720 ) ] ) ;

	#

	imageLayout = mc.rowColumnLayout ( nc = 1 , cw = [ ( 1 , 720 ) ] ) ;

	mc.image ( 'attributeTuner_helpImage' , image = image ) ;

	mc.setParent( '..' ) ;    

	#

	imageChangerLayout = mc.rowColumnLayout ( nc = 5 , cw = [ ( 1 , 280 ) , ( 2 , 30 ) , ( 3 , 100 ) , ( 4 , 30 ) , ( 5 , 280 ) ] ) ;

	mc.text ( l = '' ) ;
	mc.button ( l = '<' , c = attMl.Callback ( pageChange , operation = 'minus' ) ) ;
	mc.text ( 'attributeTuner_helpPageNumber' , l = '1/7' ) ;
	mc.button ( l = '>' , c = attMl.Callback ( pageChange , operation = 'plus' ) ) ;
	mc.text ( l = '' ) ;

	mc.setParent( '..' ) ;    

	#
	
	mc.showWindow ( window ) ;  

def pageChange ( operation = 'plus' ) :
    
    # check operation
    
    if operation == 'plus' :
        
        op = 1 ;
        
    elif operation == 'minus' :
      
        op = -1 ;
        
    else :
        
        mc.error ( 'invalid operation variable input' ) ;
        
    # image
    
    image = mc.image ( 'attributeTuner_helpImage' , q = True , image = True ) [ : -5 ] ;
    number = int ( mc.image ( 'attributeTuner_helpImage' , q = True , image = True ) [ -5 ] ) ;
    format = '.jpg'
    
    # check
    
    numberTest = number + op ;
    
    if numberTest < 1 or numberTest > 7 :
        pass;
        
    else :
        number = number + op ;
        
        mc.image ( 'attributeTuner_helpImage' , e = True , image = '%s%s%s' % ( image , number , format ) ) ;
        
        mc.text ( 'attributeTuner_helpPageNumber' , e = True , l = '%s/7' % number ) ;
