import pymel.core as pm ;

'''
select curves that doesn't have correct shape name ( in this case, curves that was exported from xGen inGuide)
run the script
'''

def run ( *args ) :

	selection = pm.ls ( sl = True ) ;

	for each in selection :
	    shape = each.getShape();
	    shape.rename ( str(each) + 'Shape' ) ;