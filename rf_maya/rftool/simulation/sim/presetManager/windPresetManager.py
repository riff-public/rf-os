import pymel.core as pm ;
import maya.cmds as mc ;
import os ;

# selfPath = mc.file ( q = True , exn = True ) ;
# print selfPath ;

selfPath = os.path.realpath (__file__) ;
fileName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( fileName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

windPresetDirectory = selfPath + '/data/twoHeroes/wind' ;
#windPresetDirectory = r'P:\_TechAnim\twoHeroes\preset\wind' ;

def getShot ( *args ) :
    
    scenePath = mc.file ( q = True , exn = True ) ;

    f = scenePath.split('film')[1].split('/')[0] ;
     
    q = scenePath.split('film')[1].split('/')[1].split('q')[1] ;
    
    s = scenePath.split('film')[1].split('/')[2].split('s')[1] ;

    c = scenePath.split('film')[1].split('/')[3] ;
    
    return [ f , q , s , c ] ;

def browseTechAnim ( *args ) :
    import os ;
    os.startfile ( windPresetDirectory ) ;

def refresh ( *args ) :
    preset = os.listdir ( windPresetDirectory ) ;
    preset.sort ( ) ;
    
    pm.textScrollList ( 'presetTxtScroll' , e = True , ra = True ) ;

    for each in preset :
        pm.textScrollList ( 'presetTxtScroll' , e = True , append = each ) ;

    shot = getShot ( ) ;

    pm.textField ( 'filmField' , e = True , text = shot [0] ) ;
    pm.textField ( 'sequenceField' , e = True , text = shot [1] ) ;
    pm.textField ( 'shotField' , e = True , text = shot [2] ) ;

def getWind ( *args ) :

    if pm.objExists ( 'windPlacement_ctrl' ) == True :
        
        target = 'windPlacement_ctrl' ;

    else :
        target = 'particle_particleController' ;
        
    target = pm.general.PyNode ( target ) ;

    globalValue = {  } ;

    globalValue [ 'windValue' ] = { } ;

    globalValue [ 'windValue' ] [ 'EmitRate' ] = target.EmitRate.get ( ) ;
    globalValue [ 'windValue' ] [ 'PartLifeSpan' ] = target.PartLifeSpan.get ( ) ;
    globalValue [ 'windValue' ] [ 'PartSpdMax' ] = target.PartSpdMax.get ( ) ;
    globalValue [ 'windValue' ] [ 'PartSpdMin' ] = target.PartSpdMin.get ( ) ;
    globalValue [ 'windValue' ] [ 'PartStartFrame' ] = target.PartStartFrame.get ( ) ;
    globalValue [ 'windValue' ] [ 'RadiPower' ] = target.RadiPower.get ( ) ;
    
    ###
    
    globalValue [ 'windValue' ] [ 'RadiDistance' ] = target.RadiDistance.get ( ) ;
    globalValue [ 'windValue' ] [ 'TurbPower' ] = target.TurbPower.get ( ) ;
    globalValue [ 'windValue' ] [ 'TurbFreqMax' ] = target.TurbFreqMax.get ( ) ;
    globalValue [ 'windValue' ] [ 'TurbFreqMin' ] = target.TurbFreqMin.get ( ) ;

    ### ###

    nucleus = pm.general.PyNode ( 'nucleus1' ) ;

    globalValue [ 'nucleus' ] = { } ;

    globalValue [ 'nucleus' ] [ 'windNoise' ] =  nucleus.windNoise.get ( ) ;
    globalValue [ 'nucleus' ] [ 'substeps' ] = nucleus.subSteps.get ( ) ;
    globalValue [ 'nucleus' ] [ 'maxCollisionIterations' ] = nucleus.maxCollisionIterations.get ( ) ;
    globalValue [ 'nucleus' ] [ 'collisionLayerRange' ] = nucleus.collisionLayerRange.get ( ) ;
    globalValue [ 'nucleus' ] [ 'timingOutput' ] = nucleus.timingOutput.get ( ) ;
    globalValue [ 'nucleus' ] [ 'useTransform' ] = nucleus.useTransform.get ( ) ;
    globalValue [ 'nucleus' ] [ 'startFrame' ] = nucleus.startFrame.get ( ) ;
    globalValue [ 'nucleus' ] [ 'frameJumpLimit' ] = nucleus.frameJumpLimit.get ( ) ;
    globalValue [ 'nucleus' ] [ 'timeScale' ] = nucleus.timeScale.get ( ) ;
    globalValue [ 'nucleus' ] [ 'spaceScale' ] = nucleus.spaceScale.get ( ) ; 
    globalValue [ 'nucleus' ] [ 'caching' ] = nucleus.caching.get ( ) ;
    globalValue [ 'nucleus' ] [ 'frozen' ] = nucleus.frozen.get ( ) ;
    globalValue [ 'nucleus' ] [ 'nodeState' ] = nucleus.nodeState.get ( ) ;
    globalValue [ 'nucleus' ] [ 'hiddenInOutliner' ] = nucleus.hiddenInOutliner.get ( ) ;
    globalValue [ 'nucleus' ] [ 'useOutlinerColor' ] = nucleus.useOutlinerColor.get ( ) ;
    globalValue [ 'nucleus' ] [ 'outlinerColor' ] = nucleus.outlinerColor.get ( ) ;
    globalValue [ 'nucleus' ] [ 'wireColorRGB' ] = nucleus.wireColorRGB.get ( ) ;
    globalValue [ 'nucleus' ] [ 'selectionChildHighlighting' ] = nucleus.selectionChildHighlighting.get ( ) ;

    return globalValue ;

def setWind ( *args ) :

    if pm.objExists ( 'windPlacement_ctrl' ) == True :
        target = 'windPlacement_ctrl' ;
    else :
        target = 'particle_particleController' ;
        
    target = pm.general.PyNode ( target ) ;

    #print target ;

    file = pm.textScrollList ( 'presetTxtScroll' , q = True , si = True ) ;

    if len ( file ) == 0 :

        pass ;

    else :

        file = file [0] ;
        globalValueTxt = open ( windPresetDirectory + '/' + '%s' % file , 'r+' ) ;
        globalValueTxtContent = globalValueTxt.read ( ) ;
        globalValueTxt.close ( ) ;

        cmdTxt = 'globalValue = %s' % globalValueTxtContent ;

        exec cmdTxt ;

        target.EmitRate.set ( globalValue [ 'windValue' ] [ 'EmitRate' ] ) ;
        target.PartLifeSpan.set ( globalValue [ 'windValue' ] [ 'PartLifeSpan' ] ) ;
        target.PartSpdMax.set ( globalValue [ 'windValue' ] [ 'PartSpdMax' ] ) ;
        target.PartSpdMin.set ( globalValue [ 'windValue' ] [ 'PartSpdMin' ] ) ;
        target.PartStartFrame.set ( globalValue [ 'windValue' ] [ 'PartStartFrame' ] ) ;
        target.RadiPower.set (  globalValue [ 'windValue' ] [ 'RadiPower' ] ) ;
        
        ###
        
        target.RadiDistance.set ( globalValue [ 'windValue' ] [ 'RadiDistance' ] ) ;
        target.TurbPower.set ( globalValue [ 'windValue' ] [ 'TurbPower' ] ) ;
        target.TurbFreqMax.set ( globalValue [ 'windValue' ] [ 'TurbFreqMax' ] ) ;
        target.TurbFreqMin.set ( globalValue [ 'windValue' ] [ 'TurbFreqMin' ] ) ;

        print ( 'wind attribute has been set' ) ;


def setNucleus ( *args ) :
        
    target = pm.general.PyNode ( 'nucleus1' ) ;

    #print target ;

    file = pm.textScrollList ( 'presetTxtScroll' , q = True , si = True ) ;

    if len ( file ) == 0 :

        pass ;

    else :

        file = file [0] ;
        globalValueTxt = open ( windPresetDirectory + '/' + '%s' % file , 'r+' ) ;
        globalValueTxtContent = globalValueTxt.read ( ) ;
        globalValueTxt.close ( ) ;

        cmdTxt = 'globalValue = %s' % globalValueTxtContent ;

        exec cmdTxt ;

        target.windNoise.set ( globalValue [ 'nucleus' ] [ 'windNoise' ] ) ;
        target.subSteps.set ( globalValue [ 'nucleus' ] [ 'substeps' ] ) ;
        target.maxCollisionIterations.set ( globalValue [ 'nucleus' ] [ 'maxCollisionIterations' ] ) ;
        target.collisionLayerRange.set ( globalValue [ 'nucleus' ] [ 'collisionLayerRange' ] ) ; 
        target.timingOutput.set ( globalValue [ 'nucleus' ] [ 'timingOutput' ] ) ; 
        target.useTransform.set ( globalValue [ 'nucleus' ] [ 'useTransform' ] ) ;       
        target.startFrame.set ( globalValue [ 'nucleus' ] [ 'startFrame' ] ) ;
        target.frameJumpLimit.set ( globalValue [ 'nucleus' ] [ 'frameJumpLimit' ] ) ;
        target.timeScale.set ( globalValue [ 'nucleus' ] [ 'timeScale' ]  ) ;        
        target.spaceScale.set ( globalValue [ 'nucleus' ] [ 'spaceScale' ] ) ;
        target.caching.set ( globalValue [ 'nucleus' ] [ 'caching' ] ) ;
        target.frozen.set ( globalValue [ 'nucleus' ] [ 'frozen' ] ) ;
        target.nodeState.set ( globalValue [ 'nucleus' ] [ 'nodeState' ] ) ;
        target.hiddenInOutliner.set ( globalValue [ 'nucleus' ] [ 'hiddenInOutliner' ] ) ;
        target.useOutlinerColor.set ( globalValue [ 'nucleus' ] [ 'useOutlinerColor' ] ) ;
        target.outlinerColor.set ( globalValue [ 'nucleus' ] [ 'outlinerColor' ] ) ;
        target.wireColorRGB.set ( globalValue [ 'nucleus' ] [ 'wireColorRGB' ] ) ;
        target.selectionChildHighlighting.set ( globalValue [ 'nucleus' ] [ 'selectionChildHighlighting' ] ) ;

        print ( 'nucleus has been set' ) ;

def saveWindPreset ( *args ) :
    
    f = pm.textField ( 'filmField' , q = True , text = True ) ;
    q = pm.textField ( 'sequenceField' , q = True , text = True ) ;
    s = pm.textField ( 'shotField' , q = True , text = True ) ;
    
    if f == 0 :
        f = '000' ;
    else :
        f = str ( f ) ;
        f = f.zfill ( 3 ) ;
    
    if q == 0 :
        q = '0000' ;
    else :
        q = str ( q ) ;
        q = q.zfill ( 4 ) ;

    if s == 0 :
        s = '0000' ;
    else : 
        s = str ( s ) ;
        s = s.zfill ( 4 ) ;

    char = pm.textField ( 'charField' , q = True , text = True ) ;
    comment = pm.textField ( 'commentField' , q = True , text = True ) ;
    
    fileName = 'f' + f + '.' + 'q' + q + '.' + 's' + s + '.' + char ;

    if comment == '' :
        pass ;
    else :
        fileName += ( '_' + comment ) ; 
    
    presetTxt = open ( windPresetDirectory + '/' + '%s.txt' % fileName , 'w+' ) ;
    globalValue = getWind ( ) ;

    presetTxt.write ( str ( globalValue ) ) ;
    presetTxt.close ( ) ;
    
    pass ;

def updateCharField ( *args ) :

    char = getShot ( ) [ 3 ] ;

    pm.textField ( 'charField' , e = True , text = char ) ;
    
    
def run ( *args ) :
    
    # check if window exists
    if pm.window ( 'TH_windPresetUI' , exists = True ) :
        pm.deleteUI ( 'TH_windPresetUI' ) ;
    else : pass ;
   
    window = pm.window ( 'TH_windPresetUI', title = "Two Heroes Wind Preset" ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'TH_windPresetUI' , e = True , w = 300 , h = 100 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 , w = 300 ) ;
        with mainLayout :
            
            pm.textScrollList ( 'presetTxtScroll' , w = 300 , h = 300 , ams = False ) ;  
            
            pm.button ( label = 'refresh' , c = refresh , bgc = ( 0.498039 , 1 , 0.831373 ) ) ;
            
            pm.separator ( vis = False , h = 10 ) ;
            
            shotLayout = pm.rowColumnLayout ( nc = 3 , w = 300 , cw = [ ( 1 , 100 ) , ( 2 , 100 ) , ( 3 , 100 ) ] ) ;
            with shotLayout :
                
                pm.text ( label = 'film' ) ;
                pm.text ( label = 'sequence' ) ;
                pm.text ( label = 'shot' ) ;
                
                pm.textField ( 'filmField' ) ;
                pm.textField ( 'sequenceField' ) ;
                pm.textField ( 'shotField' ) ;
                
            charLayout = pm.rowColumnLayout ( nc = 2 , w = 300 , cw = [ ( 1 , 150 ) , ( 2 , 150 ) ] ) ;
            with charLayout :
                
                pm.text ( label = 'character' ) ;
                pm.text ( label = 'comment' ) ;
                
                pm.textField ( 'charField' ) ;
                pm.textField ( 'commentField' ) ;
                                        
            pm.button ( label = 'save preset' , c = saveWindPreset , bgc = ( 1 , 0.843137 , 0 ) ) ;
            
            pm.separator ( vis = False , h = 10 ) ;
            
            pm.button ( label = 'set wind' , c = setWind , bgc = ( 0.498039 , 1 , 0 ) ) ;
            pm.button ( label = 'set nucleus' , c = setNucleus , bgc = ( 0.196078 , 0.803922 , 0.196078 ) ) ;
            
            pm.separator ( vis = False , h = 10 ) ;
            
            pm.button ( label = 'wind preset directory' , c = browseTechAnim, bgc = ( 1 , 0.411765 , 0.705882 ) ) ;
            
        updateCharField ( ) ;
        refresh ( ) ;
   
    window.show ( ) ;