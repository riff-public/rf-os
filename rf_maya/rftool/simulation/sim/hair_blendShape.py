import pymel.core as pm ;

def blendShape_cmd ( driver , driven , bshName ,  *args ) :
    bsh = pm.blendShape ( driver , driven , automatic = True , origin = 'world' , n = bshName ) [0] ; 
    pm.setAttr ( bsh + '.' + driver , 1 ) ;
    return bsh ;

def setBlendShape_cmd ( bsh , crv , blendStart , blendEnd , *args ) :
    
    numberOfCv = pm.getAttr ( crv + '.' + 'spans' ) + pm.getAttr ( crv + '.' + 'degree' ) ;
    
    cvStart = int ( numberOfCv / 100.0 * blendStart ) ;
    cvEnd = int ( numberOfCv / 100.0 * blendEnd ) ;
    
    blendAmount = 1 / float ( cvEnd - cvStart ) ;
     
    for i in range ( 0 , numberOfCv ) :
        pm.setAttr ( bsh + '.inputTarget[0].inputTargetGroup[0].targetWeights[' + str ( i ) + ']' , 0 ) ;
     
    for i in range ( 0 , cvStart ) :    
        pm.setAttr ( bsh + '.inputTarget[0].inputTargetGroup[0].targetWeights[' + str ( i ) + ']' , 1 ) ;
    
    value = 1 ;
    for i in range ( cvStart , cvEnd ) :
        pm.setAttr ( bsh + '.inputTarget[0].inputTargetGroup[0].targetWeights[' + str ( i ) + ']' , value ) ;
        value -= blendAmount ;

def set_cmd ( *args ) :

    blendStart = pm.intField ( 'blendStart_input' , q = True , v = True ) ;
    blendEnd = pm.intField ( 'blendEnd_input' , q = True , v = True ) ;

    selection = pm.ls ( sl = True ) ;
    
    for each in selection :
        nm = each.split ( '_' ) [0] ;
        driver = nm + '_ORI' ;
        driven = each ;

        # find if blendshape exists
        driverNm    = driver.split ( '_' ) [0] ;
        drivenNm    = driven.split ( '_' ) [0] ;
        driverSfx   = driver.split ( '_' ) [1] ;
        drivenSfx   = driven.split ( '_' ) [1] ;
        bshName     = driverNm + driverSfx + '2' + drivenSfx + '_BSH'

        if pm.objExists ( bshName ) == False :

            bsh = blendShape_cmd ( driver = driver , driven = driven , bshName = bshName ) ;
        
        setBlendShape_cmd ( bsh = bshName , crv = each , blendStart = blendStart , blendEnd = blendEnd ) ; 

def enableDisable_cmd ( op = 1 , *args ) :
    # op == 0 == disable
    # op == 1 == enable
    
    selection = pm.ls ( sl = True ) ;
    
    for each in selection :
        nm = each.split ( '_' ) [0] ;
        driver = nm + '_ORI' ;
        driven = each ;

        # find if blendshape exists
        driverNm    = driver.split ( '_' ) [0] ;
        drivenNm    = driven.split ( '_' ) [0] ;
        driverSfx   = driver.split ( '_' ) [1] ;
        drivenSfx   = driven.split ( '_' ) [1] ;
        bshName     = driverNm + driverSfx + '2' + drivenSfx + '_BSH'

        if pm.objExists ( bshName ) == False :
            pass ;

        else :
            bsh = pm.general.PyNode ( bshName ) ;
            pm.setAttr ( bsh + '.' + driver , op ) ;

def enable_cmd ( *args ) :
    enableDisable_cmd ( op = 1 ) ;

def disable_cmd ( *args ) :
    enableDisable_cmd ( op = 0 ) ;

def delete_cmd ( *args ) :
    
    selection = pm.ls ( sl = True ) ;
    
    for each in selection :
        nm = each.split ( '_' ) [0] ;
        driver = nm + '_ORI' ;
        driven = each ;

        # find if blendshape exists
        driverNm    = driver.split ( '_' ) [0] ;
        drivenNm    = driven.split ( '_' ) [0] ;
        driverSfx   = driver.split ( '_' ) [1] ;
        drivenSfx   = driven.split ( '_' ) [1] ;
        bshName     = driverNm + driverSfx + '2' + drivenSfx + '_BSH'

        if pm.objExists ( bshName ) == False :
            pass ;
        else :
            pm.delete ( bshName ) ;

def run ( *args ) :

    def separator ( *args ) :
        pm.separator ( h = 5 , vis = False ) ;

    def filler ( rep = 1 , *args ) :
        if rep == 1 :
            pm.text ( label = ' ' ) ;
        elif rep > 1 :
            for i in range ( 0 , filler ) :
                pm.text ( label = ' ' ) ;
        else : pass ;
   
    width = 400.00 ;

    title           = 'hair blendshape' ;
    version         = 'v2.0' ;
    lastModified    = '2017/12/21' ; 
    # check if window exists

    help_txt = '''- this script will blendshape curve(s) under follicle to selected curve(s)
  
- curve under follicle should be named 'curve_ORI'
  
- selected curve(s) (output curve(s)) should be named "curve_CRV"'''

    if pm.window ( 'hairBSH_UI' , exists = True ) :
        pm.deleteUI ( 'hairBSH_UI' ) ;
    else : pass ;
   
    window = pm.window ( 'hairBSH_UI', title = title + ' '  + version + ' ' + lastModified , mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    pm.window ( 'hairBSH_UI' , e = True , w = 250 , h = 100 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
             
            pm.scrollField ( wordWrap = True , editable = False , text = help_txt , w = width , h = 115 ) ;
            
            separator ( ) ;

            with pm.rowColumnLayout ( nc = 5 , w = width , cw = [ ( 1 , width/3/3 ) , ( 2 , width/3 ) , ( 3 , width/3/3 ) , ( 4 , width/3 ) , ( 5 , width/3/3 ) ] ) :
                
                filler ( ) ;

                with pm.rowColumnLayout ( nc = 1 ) :
                    pm.text ( label = 'blend start ( % )' , w = width/3 ) ;
                    pm.intField ( 'blendStart_input' , v = 0 , w = width/3 ) ;

                filler ( ) ;

                with pm.rowColumnLayout ( nc = 1 ) :
                    pm.text ( label = 'blend end ( % )' , w = width/3 ) ;
                    pm.intField ( 'blendEnd_input' , v = 100 , w = width/3 ) ;

                filler ( ) ;

            separator ( ) ;

            pm.button ( label = 'set' , c = set_cmd , bgc = ( 1 , 0.85 , 0 ) ) ;

            separator ( ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                pm.button ( label = 'enable' , c = enable_cmd ) ;
                pm.button ( label = 'disable' , c = disable_cmd ) ;

            separator ( ) ;

            pm.button ( label = 'delete' , c = delete_cmd , bgc = ( 0.8 , 0.35 , 0.35 ) ) ;

    window.show () ;

run ( ) ;