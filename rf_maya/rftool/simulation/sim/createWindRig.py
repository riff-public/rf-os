import pymel.core as pm ;

def createWindRig ( *args ) :

    windGrp = pm.group ( em = True , w = True , n = 'wind_grp' ) ;
    windGrp = pm.general.PyNode ( windGrp ) ;
    
    placement = pm.curve ( d = 1 , p = [ ( -33.529067 , 0 , 29.433365 ) , ( 1.19744e-007 , 0 , -44.542969 ) , ( 33.529067 , 0 , 29.433365 ) , ( -33.529067 , 0 , 29.433365 ) ] ) ;
    placement = pm.rename ( placement , 'windPlacement_ctrl' ) ;
    placement = pm.general.PyNode ( placement) ;
    
    placementShape = pm.listRelatives ( placement , s = True ) [0] ;
    
    placementShape.overrideEnabled.set ( 1 ) ;
    placementShape.overrideColor.set ( 17 ) ;
    
    particleCtrl = pm.general.PyNode ( 'particle_particleController' ) ;
    
    placement.addAttr ( '__wind__' , keyable = True , attributeType = 'float' ) ;
    placement.__wind__.lock ( ) ;
    
    placement.addAttr ( 'EmitRate' , keyable = True , attributeType = 'float' ) ;
    placement.EmitRate.set ( particleCtrl.EmitRate.get ( ) ) ;
    placement.EmitRate >> particleCtrl.EmitRate ;
    
    placement.addAttr ( 'PartLifeSpan' , keyable = True , attributeType = 'float' ) ;
    placement.PartLifeSpan.set ( particleCtrl.PartLifeSpan.get ( ) ) ;
    placement.PartLifeSpan >> particleCtrl.PartLifeSpan ;
    
    placement.addAttr ( 'PartSpdMax' , keyable = True , attributeType = 'float' ) ;
    placement.PartSpdMax.set ( particleCtrl.PartSpdMax.get ( ) ) ;
    placement.PartSpdMax >> particleCtrl.PartSpdMax ;
    
    placement.addAttr ( 'PartSpdMin' , keyable = True , attributeType = 'float' ) ;
    placement.PartSpdMin.set ( particleCtrl.PartSpdMin.get ( ) ) ;
    placement.PartSpdMin >> particleCtrl.PartSpdMin ;
    
    placement.addAttr ( 'PartStartFrame' , keyable = True , attributeType = 'float' ) ;
    placement.PartStartFrame.set ( particleCtrl.PartStartFrame.get ( ) ) ;
    placement.PartStartFrame >> particleCtrl.PartStartFrame ;
    
    placement.addAttr ( 'RadiPower' , keyable = True , attributeType = 'float' ) ;
    placement.RadiPower.set ( particleCtrl.RadiPower.get ( ) ) ;
    placement.RadiPower >> particleCtrl.RadiPower ;
    
    ######################################################################################
    
    placement.addAttr ( 'RadiDistance' , keyable = True , attributeType = 'float' ) ;
    placement.RadiDistance.set ( particleCtrl.RadiDistance.get ( ) ) ;
    placement.RadiDistance >> particleCtrl.RadiDistance ;
    
    placement.addAttr ( 'TurbPower' , keyable = True , attributeType = 'float' ) ;
    placement.TurbPower.set ( particleCtrl.TurbPower.get ( ) ) ;
    placement.TurbPower >> particleCtrl.TurbPower ;
    
    placement.addAttr ( 'TurbFreqMax' , keyable = True , attributeType = 'float' ) ;
    placement.TurbFreqMax.set ( particleCtrl.TurbFreqMax.get ( ) ) ;
    placement.TurbFreqMax >> particleCtrl.TurbFreqMax ;
    
    placement.addAttr ( 'TurbFreqMin' , keyable = True , attributeType = 'float' ) ;
    placement.TurbFreqMin.set ( particleCtrl.TurbFreqMin.get ( ) ) ;
    placement.TurbFreqMin >> particleCtrl.TurbFreqMin ;

    ########################################################################################
    
    pm.parent ( 'STnClothGrp' , placement ) ;
    pm.parent ( placement , windGrp ) ;

    return windGrp ;

def run ( *args ) :

    selection = pm.ls ( sl = True ) ;
    pos = pm.xform ( selection , q = True , ws = True , rp = True ) ;

    windGrp = createWindRig ( ) ;
    windGrp.t.set ( pos ) ;