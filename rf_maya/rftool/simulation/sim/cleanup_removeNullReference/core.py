import pymel.core as pm ;
import maya.cmds as mc ;
import os ;

class NullRef ( object ) :

	def __init__ ( self ) :
		
		selfPath = os.path.realpath (__file__) ;
		fileName = os.path.basename (__file__) ;
		selfPath = selfPath.replace ( fileName , '' ) ;
		self.selfPath = selfPath.replace ( '\\' , '/' ) ;

		self.nullScenePath = self.selfPath.split ( 'sim/cleanup_removeNullReference' )  [0] ;
		self.nullScenePath += 'asset/null.ma' ;

	def removeNullRef ( self ) :		

		refNode_list = pm.ls ( type = 'reference' ) ;
		noneRefNode_list = [] ;

		for refNode in refNode_list :
		    refPath = pm.referenceQuery ( refNode , filename = True , unresolvedName = True ) ;
		    if not os.path.exists ( refPath ) :
		        noneRefNode_list.append ( refNode ) ;

		for refNode in noneRefNode_list :
			mc.file ( self.nullScenePath , loadReference = str(refNode) ) ;
			mc.file ( removeReference = True , referenceNode = str(refNode) ) ;

def run ( *args ) :
	nr = NullRef() ;
	nr.removeNullRef() ;