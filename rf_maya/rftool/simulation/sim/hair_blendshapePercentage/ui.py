import pymel.core as pm ;

##################
##################
''' CORE '''
##################
##################

import sim.hair_blendshapePercentage.core as hbsp ;
reload ( hbsp ) ;

def percentageBlendshape_cmd ( *args ) :
    hbsp.percentageBlendshape_cmd ( ) ;

def deletePercentageBlendshape_cmd ( *args ) :
    hbsp.deletePercentageBlendshape_cmd ( ) ;

def selectPctBSHCurve_cmd ( *args ) :
    hbsp.selectPctBSHCurve_cmd ( ) ;

def updateEnvelopeOpt_cmd ( *args ) :
    hbsp.updateEnvelopeOpt_cmd ( ) ;

def updateEnvelopeOpt_cmd ( *args ) :
    hbsp.updateEnvelopeOpt_cmd ( ) ;

def refresh_cmd ( *args ) :
    hbsp.refresh_cmd ( ) ;

##################
##################
''' UI '''
##################
##################

title = 'hair percentage blendshape util v2.0' ;
width = 450.00/1.5 ;

def separator ( *args ) :
    pm.separator ( vis = False , h = 5 ) ;

def filler ( *args ) :
    pm.text ( label = '' ) ;

def run ( *args ) :
    
    # check if window exists
    if pm.window ( 'hair_percentageBlendshape_UI' , exists = True ) :
        pm.deleteUI ( 'hair_percentageBlendshape_UI' ) ;
    else : pass ;
   
    window = pm.window ( 'hair_percentageBlendshape_UI', title = title ,
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;
        
    with pm.window ( 'hair_percentageBlendshape_UI' , e = True , w = width , h = 10 ) :
        ### main layout ###
        with pm.rowColumnLayout ( nc = 1 ) :
            
            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                pm.text ( label = 'driver suffix' , width = width/2 ) ;
                pm.textField ( 'hbsp_driver_textField' ,  width = width/2 , text = '_ORI' ) ;

                pm.text ( label = 'driven suffix' , width = width/2 ) ;
                pm.textField ( 'hbsp_driven_textField' ,  width = width/2 , text = '_CRV' ) ;

            separator ( ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                pm.text ( label = 'longest length (driven crvs)' , w = width/4 ) ;
                pm.floatField ( 'hbsp_longestLength_floatField' , v = 0.0 , w = width/4 ) ;
                
                filler ( ) ;
                pm.button ( label = 'refresh' , w = width/2 , c = refresh_cmd ) ;

            pm.separator ( h = 20 , vis = True ) ;
            
            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                pm.text ( label = 'envelope' , width = width/2 ) ;
                pm.floatField ( 'hbsp_envelope_floatField' , v = 1.0 , width = width/2 , precision = 2 ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                pm.button ( label = 'blendshape/set' , width = width/2 , c = percentageBlendshape_cmd , bgc = ( 1 , 1 , 1 ) ) ;
                pm.button ( label = 'delete blendshape' , width = width/2 , c = deletePercentageBlendshape_cmd ) ;

            pm.separator ( h = 20 , vis = True ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                pm.checkBox ( 'hbsp_selectByEnvelope_checkBox' , label = 'select by envelope' , v = False , enable = True , h = 20 , cc = updateEnvelopeOpt_cmd ) ;
                pm.optionMenu( 'hbsp_envelope_opt' , label='envelope' , enable = False ) ;

            pm.button ( label = 'select curve with percentage blendshape' , width = width , c = selectPctBSHCurve_cmd ) ;

    refresh_cmd ( ) ;

    window.show ( ) ;