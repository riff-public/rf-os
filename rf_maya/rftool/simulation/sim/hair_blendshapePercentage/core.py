import pymel.core as pm ;

def selectPctBSHCurve_cmd ( *args ) :

    driver_suffix = pm.textField ( 'hbsp_driver_textField' ,  q = True , text = True ) ;
    driven_suffix = pm.textField ( 'hbsp_driven_textField' ,  q = True , text = True ) ;

    pctBSH_set = set_cmd ( ) ;
    pm.select ( pctBSH_set ) ;

    if pm.checkBox ( 'hbsp_selectByEnvelope_checkBox' , q = True , value = True ) == True :

        selection = pm.ls ( sl = True ) ;

        toSelect_list = [] ;

        envelope = float ( pm.optionMenu( 'hbsp_envelope_opt' , q = True , value = True ) ) ;

        #envelope = pm.floatField ( 'hbsp_envelope_floatField' , q = True , v = True ) ;

        for each in selection :

            driver = each.split(driven_suffix)[0] + driver_suffix ;
            driven = each ;

            if pm.objExists ( '%s_pctBSH' % driver ) == True :
                if envelope == round ( pm.blendShape ( '%s_pctBSH' % driver , q = True , envelope = True ) , 2 ) :
                    toSelect_list.append ( each ) ;
                else : pass ;
            else : pass ;

        pm.select ( toSelect_list ) ;

def getCurrentEnvelope_cmd ( *args ) :

    previousSelection = pm.ls ( sl = True ) ;

    driver_suffix = pm.textField ( 'hbsp_driver_textField' ,  q = True , text = True ) ;
    driven_suffix = pm.textField ( 'hbsp_driven_textField' ,  q = True , text = True ) ;

    pctBSH_set = set_cmd ( ) ;
    pm.select ( pctBSH_set ) ;

    selection = pm.ls ( sl = True ) ;

    envelope_list = [] ;

    for each in selection :

        driver = each.split(driven_suffix)[0] + driver_suffix ;
        driven = each ;

        if pm.objExists ( '%s_pctBSH' % driver ) == True :
            envelope = pm.blendShape ( '%s_pctBSH' % driver , q = True , envelope = True ) ;
            envelope = round ( envelope , 2 ) ;
            envelope_list.append ( envelope ) ;
        else : pass ;

    envelope_list = set ( envelope_list ) ;
    envelope_list = list ( envelope_list ) ;

    envelope_list.sort( ) ;

    pm.select ( previousSelection ) ;

    if len ( envelope_list ) == [] :
        return None ;
    else :
        return envelope_list ;

def set_cmd ( *args ) :

    if pm.objExists ( 'pctBSH_set' ) == True :
        pass ;
    else :
        pm.sets ( name = 'pctBSH_set' ) ;

    return pm.general.PyNode ( 'pctBSH_set' ) ;

def percentageBlendshape_cmd ( *args ) :

    longestLength = pm.floatField ( 'hbsp_longestLength_floatField' , q = True , v = True ) ;
    envelope = pm.floatField ( 'hbsp_envelope_floatField' , q = True , v = True ) ;

    driver_suffix = pm.textField ( 'hbsp_driver_textField' ,  q = True , text = True ) ;
    driven_suffix = pm.textField ( 'hbsp_driven_textField' ,  q = True , text = True ) ;

    curves_list = pm.ls ( sl = True ) ;
    curves_list.sort ( ) ;

    pctBSH_set = set_cmd ( ) ;

    pm.sets ( pctBSH_set , e = True , add = curves_list ) ;

    for curve in curves_list :

        type = curve.getShape() ;
        type = str ( pm.objectType ( type ) ) ;
        if type == 'nurbsCurve' :

            driver = curve.split(driven_suffix)[0] + driver_suffix ;
            driven = curve ;

            #ORI = curve.split ( '_' ) [0] + '_ORI' ;
            #CRV = curve ;
            curvePercentage = 1 / longestLength * pm.arclen ( curve ) ;
            curvePercentage = 1 - curvePercentage ;

            if pm.objExists ( '%s_pctBSH' % driver ) == True :
                pm.blendShape ( '%s_pctBSH' % driver , e = True , weight = [ 0 , curvePercentage ] , envelope = envelope ) ;
            else :
                pm.blendShape ( driver , driven  , origin = 'world' , weight = [ 0 , curvePercentage ] , n = '%s_pctBSH' % driver , envelope = envelope ) ;

def deletePercentageBlendshape_cmd ( *args ) :

    curves_list = pm.ls ( sl = True ) ;
    curves_list.sort ( ) ;

    driver_suffix = pm.textField ( 'hbsp_driver_textField' ,  q = True , text = True ) ;
    driven_suffix = pm.textField ( 'hbsp_driven_textField' ,  q = True , text = True ) ;

    pctBSH_set = set_cmd ( ) ;

    for curve in curves_list :
        driver = curve.split(driven_suffix)[0] + driver_suffix ;
        driven = curve ;

        if pm.objExists ( '%s_pctBSH' % driver ) == True :
            pm.delete ( '%s_pctBSH' % driver )
        else :
            pass ;

    pm.sets ( pctBSH_set , e = True , remove = curves_list ) ;

def getLongestLength_cmd ( *args ) :
    # return longestLength

    hairSuffix = pm.textField ( 'hbsp_driven_textField' ,  q = True , text = True ) ;

    allCurves_list = pm.ls ( '*%s' % hairSuffix ) ;

    curveLength_list = [] ;

    for curve in allCurves_list :
        length = pm.arclen ( curve ) ;
        curveLength_list.append ( length ) ;

    curveLength_list.sort ( ) ;

    longestLength = curveLength_list[-1] ;

    return longestLength ;

def updateEnvelopeOpt_cmd ( *args ) :

    envelopeOpt_stat = pm.checkBox ( 'hbsp_selectByEnvelope_checkBox' , q = True , v = True ) ;
    if envelopeOpt_stat == False :

        ### remove all optionMenu items ###
        options_list = pm.optionMenu( 'hbsp_envelope_opt' , q = True , itemListLong = True ) ;
        if options_list != [] :
            for each in options_list :
                pm.deleteUI ( each ) ;

        ### diable optionMenu ###
        pm.optionMenu( 'hbsp_envelope_opt' , e = True , enable = False ) ;

    elif envelopeOpt_stat == True :
        ### remove all optionMenu items ###
        options_list = pm.optionMenu( 'hbsp_envelope_opt' , q = True , itemListLong = True ) ;
        if options_list != [] :
            for each in options_list :
                pm.deleteUI ( each ) ;

        ### enable optionMenu ###
        pm.optionMenu( 'hbsp_envelope_opt' , e = True , enable = True ) ;

        envelope_list = getCurrentEnvelope_cmd ( ) ;

        for each in envelope_list :
            if envelope_list != None :
                pm.menuItem ( label = str ( each ) , p = 'hbsp_envelope_opt' ) ;

def refresh_cmd ( *args ) :
    pm.floatField ( 'hbsp_longestLength_floatField' , e = True , v = getLongestLength_cmd() ) ;