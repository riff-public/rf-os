import pymel.core as pm ;

def queryNucleus ( *args ) :
    # return a list of nucleus

    nucleusList = pm.ls ( type = 'nucleus' ) ;
    return nucleusList ;

def blendShape ( driver , driven , *args ) :

        driverNm = driver.split ( '_' ) [0] ;
        drivenNm = driven.split ( '_' ) [0] ;

        bsh = pm.blendShape ( driver , driven , automatic = True , origin = 'world' , n = driverNm + drivenNm + '_BSH' ) [0] ; 
        pm.setAttr ( bsh + '.' + driver , 1 ) ;

class General ( object ) :

    def __init__( self , node ):
        self.node = node ;

    def makeName ( self ) :
        # return name 

        nameSplit = self.node.split ( '_' ) ;
        name = '' ;

        for i in range ( 0 , len ( nameSplit ) - 1 ) :
           if i == 0 : name += nameSplit[i] ;
           else : name += nameSplit[i].title( ) ;

        return name ;

    def world ( self ) :
        # parent self to the world
        # return None
        pm.parent ( self.node , w = True ) ;

    def lockAttr ( self , operation = 'lock' , t = True , r = True , s = True , v = True ) :
        
        self.operation = operation ;

        self.t = t ;
        self.r = r ;
        self.s = s ;
        self.v = v ;

        # operation : 'lock' , 'unlock'
        # return None
        
        target = pm.general.PyNode ( self.node ) ;
        
        if self.operation == 'lock' :
            
            if self.t == True : target.t.lock ( ) ;
            else : pass ;
            
            if self.r == True : target.r.lock ( ) ;
            else : pass ;
            
            if self.s == True : target.s.lock ( ) ;
            else : pass ;
            
            if self.v == True : target.v.lock ( ) ;
            else : pass ;
            
        elif self.operation == 'unlock' :
            
            if self.t == True : target.t.unlock ( ) ;
            else : pass ;
            
            if self.r == True : target.r.unlock ( ) ;
            else : pass ;
            
            if self.s == True : target.s.unlock ( ) ;
            else : pass ;
            
            if self.v == True : target.v.unlock ( ) ;
            else : pass ;
        
        else : pm.error ( 'please check operation argument' ) ;

    def duplicateSelf ( self , name = '' ) :
        # return duplicated object

        self.name = name ;

        duplicatedObject = pm.duplicate ( self.node , name = name ) [0] ;

        return duplicatedObject ;

    def makeCollide ( self , name = '' , nucleus = '' ) :
        # return nRigid

        self.name = name ;
        self.nucleus = nucleus ;

        # make nRigid
        pm.select ( self.node ) ;
        nRigid = pm.mel.eval ( 'makeCollideNCloth' ) [0] ;
        nRigid = pm.listRelatives ( nRigid , allParents = True ) [0] ;
        nRigid = pm.general.PyNode ( nRigid ) ;
        pm.rename ( nRigid , self.name ) ;

        # set nRigid
        nRigid.thickness.set ( 0.1 ) ;
        nRigid.friction.set ( 0 ) ;

        # assign solver
        pm.select ( nRigid ) ;
        pm.mel.eval ( 'assignNSolver %s' % self.nucleus ) ;

        return nRigid ;

def run ( ) :

    nucleusList = queryNucleus ( ) ;

    selectionList = pm.ls ( sl = True ) ;

    for selection in selectionList :

        pm.setAttr ( selection + '.v' , 0 ) ;

        target = General ( selection ) ;
        name = target.makeName ( ) ;
        # print name ;
        # ShirtsGeo

        group = pm.group ( em = True , name = name + 'MulN_Grp' ) ;

        duplicatedObjectList = [] ;
        nRigidList = [] ;

        for i in range ( 0 , len ( nucleusList ) ) :

            duplicatedObject = target.duplicateSelf ( name = name + 'N' + str ( i + 1 ) + '_COL' ) ;
            duplicatedObjectList.append ( duplicatedObject ) ;
            # print duplicatedObject ;
            # ShirtsGeoN1_COL

            duplicatedObject = General ( duplicatedObject ) ;
            duplicatedObject.lockAttr ( operation = 'unlock' ) ;
            duplicatedObject.world ( ) ;

            nRigid = duplicatedObject.makeCollide ( name = name + 'N' + str ( i + 1 ) + '_nRigid' , nucleus = 'nucleus' + str ( i + 1 ) ) ;
            nRigidList.append ( nRigid ) ;
            # print nRigid ;
            # ShirtsGeoN1_nRigid

        for i in range ( 0 , len ( duplicatedObjectList ) ) :
        	if i == 0 :
        		blendShape ( driver = selection , driven = duplicatedObjectList[0] ) ;
        	else :
        		blendShape ( driver = duplicatedObjectList[0] , driven = duplicatedObjectList[i] ) ;
        '''
        for each in duplicatedObjectList :
            blendShape ( driver = selection , driven = each ) ;
		'''

        pm.parent ( duplicatedObjectList , group ) ;
        pm.parent ( nRigidList , group ) ;

        pm.setAttr ( duplicatedObjectList[0] + '.v' , 1 ) ;

        for each in nRigidList :
            target = pm.general.PyNode ( each ) ;
            target.v.set ( 0 ) ;

        # connect nucleus1's nRigid Attribute to else nucleus's nRigid 
        driver_nRigid = pm.general.PyNode ( nRigidList [0] ) ;
        driven_nRigidList = nRigidList [1:] ;

        for each in driven_nRigidList :
            driven_nRigid = pm.general.PyNode ( each ) ;

            driver_nRigid.isDynamic >> driven_nRigid.isDynamic ;
            driver_nRigid.collide >> driven_nRigid.collide ;
            driver_nRigid.collisionFlag >> driven_nRigid.collisionFlag ;
            driver_nRigid.collideStrength >> driven_nRigid.collideStrength ;
            driver_nRigid.collisionLayer >> driven_nRigid.collisionLayer ;
            driver_nRigid.thickness >> driven_nRigid.thickness ;
            driver_nRigid.solverDisplay >> driven_nRigid.solverDisplay ;
            driver_nRigid.displayColor >> driven_nRigid.displayColor ;
            driver_nRigid.bounce >> driven_nRigid.bounce ;
            driver_nRigid.friction >> driven_nRigid.friction ;
            driver_nRigid.stickiness >> driven_nRigid.stickiness ;
            driver_nRigid.trappedCheck >> driven_nRigid.trappedCheck ;
            driver_nRigid.pushOut >> driven_nRigid.pushOut ;
            driver_nRigid.pushOutRadius >> driven_nRigid.pushOutRadius ;
            driver_nRigid.crossoverPush >> driven_nRigid.crossoverPush ;