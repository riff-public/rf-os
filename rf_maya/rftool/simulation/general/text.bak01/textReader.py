from __future__ import print_function ;
import os ;
import maya.cmds as mc ;
import pymel.core as pm ;

mp = os.path.realpath (__file__) ;
fileName = os.path.basename (__file__) ;
mp = mp.replace ( fileName , '' ) ;
mp = mp.replace ( '\\' , '/' ) ;

def readTxt ( txt , type ) :

    file = open ( mp + type + '/' +  txt , 'r' ) ;

    text = file.read ( ) ;
    file.close ( ) ;
    return text ;

'''
def textPopUp ( txt = '' , type = '' , title = 'text' ) :

    text = readTxt ( txt = txt , type = type ) ;

    # check if window exists
    if pm.window ( 'textPopUpUI' , exists = True ) :
        pm.deleteUI ( 'textPopUpUI' ) ;
    else : pass ;
   
    window = pm.window ( 'textPopUpUI', title = title ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'textPopUpUI' , e = True , w = 500 , h = 500 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 , p = window ) ;
        with mainLayout :
            
            pm.separator ( h = 5 , vis = False ) ;

            mainLayout2 = pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , 5 ) , ( 2 , 490 ) , ( 3 , 5) ] ) ;
            with mainLayout2 :

                pm.text ( label = '' ) ;

                pm.scrollField ( wordWrap = True , editable = True , h = 490 , text = text ) ;

                pm.text ( label = '' ) ;

            pm.separator ( h = 5 , vis = False ) ;

    window.show () ;

'''
def textPopUp ( txt = '' , type = '' , title = 'text' ) :

    text = readTxt ( txt = txt , type = type ) ;

    # check if window exists
    if pm.window ( 'textPopUpUI' , exists = True ) :
        pm.deleteUI ( 'textPopUpUI' ) ;
    else : pass ;
   
    window = pm.window ( 'textPopUpUI', title = title ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'textPopUpUI' , e = True , w = 500 , h = 500 ) ;

    with window :
    
        mainLayout = pm.formLayout ( ) ;
        
        textScroll = pm.scrollField ( wordWrap = True , editable = True , text = text ) ;

        pm.formLayout ( mainLayout , e = True , attachForm = [ ( textScroll , 'left' , 5 ) , ( textScroll , 'right' , 5 ) , ( textScroll , 'top' , 5 ) , ( textScroll , 'bottom' , 5 )  ] )

        # cmds.formLayout( form, edit=True, attachForm=[(b1, 'top', 5), (b1, 'left', 5), (b2, 'left', 5), (b2, 'bottom', 5), (b2, 'right', 5), (column, 'top', 5), (column, 'right', 5) ], attachControl=[(b1, 'bottom', 5, b2), (column, 'bottom', 5, b2)], attachPosition=[(b1, 'right', 5, 75), (column, 'left', 0, 75)], attachNone=(b2, 'top') )

    window.show () ;


def printWindow ( *args ) :
    textPopUp ( txt = 'window.txt' , type = 'mayaTemplate' , title = 'window (pyMel)' ) ;

def printPyNode_Attribute ( *args ) :
    textPopUp ( txt = 'PyNode_Attribute.txt' , type = 'mayaTemplate' , title = 'PyNode : Attibutes' ) ;

def printMayaSelfPath ( *args ) :
    textPopUp ( txt = 'selfPath.txt' , type = 'mayaTemplate' , title = 'self path' ) ;

####

def printClass ( *args ) :
    textPopUp ( txt = 'class.txt' , type = 'pythonTemplate' , title = 'class' ) ;

def printExec ( *args ) :
    textPopUp ( txt = 'exec.txt' , type = 'pythonTemplate' , title = 'executing text as python script' ) ;

def printNestingDictionaries ( *args ) :
    textPopUp ( txt = 'nestingDictionaries.txt' , type = 'pythonTemplate' , title = 'nesting dictionaries' ) ;

def printSlicingString ( *args ) :
    textPopUp ( txt = 'slicingString.txt' , type = 'pythonTemplate' , title = 'slicing string' ) ;

def printSelfPath ( *args ) :
    textPopUp ( txt = 'selfPath.txt' , type = 'pythonTemplate' , title = 'find self path' ) ;

def printVersionCheck ( *args ) :
    textPopUp ( txt = 'versionCheck.txt' , type = 'pythonTemplate' , title = 'version check' ) ;

def printUsingSet ( *args ) :
    textPopUp ( txt = 'usingSet.txt' , type = 'pythonTemplate' , title = 'using set' ) ;

def printOs ( *args ) :
    textPopUp ( txt = 'os.txt' , type = 'pythonTemplate' , title = 'os' ) ;