import pymel.core as pm ;
import maya.mel as ml ;


fpsDic = {
    15 : 'game' ,
    24 : 'film' , 
    25 : 'pal' ,
    30 : 'ntsc' ,
    48 : 'show' ,
    50 : 'palf' ,
    60 : 'ntscf' ,
    } ;

def setRes ( w = 1280 , h = 720 , dpi = 72 , fps = 24 , *args ) :

	pm.setAttr ( 'defaultResolution.aspectLock' , 0 ) ;
	pm.setAttr ( 'defaultResolution.dotsPerInch' , dpi ) ;
	pm.setAttr ( 'defaultResolution.width' ,  w ) ;
	pm.setAttr ( 'defaultResolution.height' , h ) ;
	pm.currentUnit ( time = fpsDic [ fps ] ) ;

	pm.select ( 'defaultResolution' ) ;
	#eval ('ToggleAttributeEditor')
	#eval('checkAspectLockHeight2 "defaultResolution"')
	#eval('checkAspectLockWidth2 "defaultResolution"')

	
#setRes ( w = 3840 , h = 1632 , dpi = 72 , fps = 24 ) ;