import os ;
import sys ;

import maya.cmds as mc ;
import maya.mel as ml ;

axes = ( 'x' , 'y' , 'z' )

def showHiddenAttribute () :

	selection = mc.ls ( sl = True ) ;

	for each in selection :

		for axis in axes :

			mc.setAttr ( each + '.t' + axis , channelBox = True ) ;
			mc.setAttr ( each + '.r' + axis , channelBox = True ) ;
			mc.setAttr ( each + '.s' + axis , channelBox = True ) ;

		mc.setAttr ( each + '.v' , channelBox = True ) ;