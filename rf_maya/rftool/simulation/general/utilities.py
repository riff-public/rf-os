import webbrowser ;
import pymel.core as pm ;
import maya.cmds as mc ;

def openLink ( link = '' ) :

	webbrowser.open( link ) ;

#link = 'https://docs.google.com/spreadsheets/d/14Lawz9PybwGsx1Y9oZauonv44UzKEZFSzvH9tDKeb84/edit?usp=sharing' ;
#openLink ( link ) ;

def customBlendShape ( *args ) :
    
    selection = pm.ls ( sl = True ) ;
    
    driver = selection [0] ;
    driven = selection [1] ;
    
    bsh = pm.blendShape ( driver , driven , o = 'world', automatic = True , n = '%s_srcBSH' % driver ) ;

    pm.setAttr ( '%s.%s' % ( bsh[0] , driver ) , 1 ) ; 
    pm.setAttr ( '%s.envelope' % bsh[0] , 1 ) ;

def customBlendShape2 ( *args ) :

    selection = pm.ls ( sl = True ) ;
    
    driver = selection [0] ;
    driven = selection [1] ;
    
    pm.blendShape ( driver , driven , origin = 'world' , weight = [ 0 , 1.0 ] , n = '%s_srcBSH' % driver ) ;

class Info ( object ) :

	def __init__ ( self , object ) :
		self.object = object ;

	def root ( self ) :
		# find the root of target

		root = mc.ls ( self.object , l = True ) [0]
		root = root.split ( "|" ) [1] ;

		return root ;

class Clean ( object ) :

    def __init__ ( self , node ) :
        self.node = node ;

    def deleteShapeOrig ( self ) :
    
        shapes = pm.listRelatives ( self.node , shapes = True ) ;

        for shape in shapes :
            if 'Orig' in str ( shape ) :
                pm.delete ( shape ) ;
            else : pass ;

    def freezeTransform ( self ) :
        pm.makeIdentity ( self.node , apply = True ) ;

    def deleteHistory ( self ) :
        pm.delete ( self.node , ch = True ) ;

    def centerPivot ( self ) :
        pm.xform ( self.node , cp = True ) ;

    def deleteRebuiltShape ( self ) :

        shapeList = pm.listRelatives ( self.node , shapes = True ) ;
            
        for shape in shapeList :            
            if 'rebuilt' in str ( shape ) :
                pm.delete ( shape ) ;
            else : pass ;

def deleteShapeOrig_run ( *args ) :
    
    selection = pm.ls ( sl = True ) ;

    for each in selection :
        target = Clean ( each ) ;
        target.deleteShapeOrig ( ) ;

def freezeTransform_deleteHistory_run ( *args ) :

    selection = pm.ls ( sl = True ) ;

    for each in selection :
        target = Clean ( each ) ;
        target.freezeTransform ( ) ;
        target.deleteHistory ( ) ;

def freezeTransform_run ( *args ) :

    selection = pm.ls ( sl = True ) ;

    for each in selection :
        target = Clean ( each ) ;
        target.freezeTransform ( ) ;

def deleteHistory_run ( *args ) :

    selection = pm.ls ( sl = True ) ;

    for each in selection :
        target = Clean ( each ) ;
        target.deleteHistory ( ) ;


def centerPivot_run ( *args ) :

    selection = pm.ls ( sl = True ) ;

    for each in selection :
        target = Clean ( each ) ;
        target.centerPivot ( ) ;

def deleteRebuiltShape_run ( *args ) :

    selection = pm.ls ( sl = True ) ;

    for each in selection :
        target = Clean ( each ) ;
        target.deleteRebuiltShape ( ) ;

def freezeTransform_deleteHistory_centerPivot_toShelf ( *args ) :

    import os ;

    self_path = os.path.realpath (__file__) ;
    self_name = os.path.basename (__file__) ;
    self_path = self_path.replace ( self_name , '' ) ;
    self_path = self_path.replace ( '\\' , '/' ) ;

    # just in case #
    if self_path[-1] != '/' :
        self_path += '/' ;
    else : pass ;

    header_cmd = '''
import sys ;

mp = "%s" ;
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
'''
    
    freezeTransform_icon = self_path + 'media/freezeTransform_icon.png' ;
    freezeTransform_cmd = header_cmd + '''
import general.utilities as utilities ;
reload ( utilities ) ;
utilities.freezeTransform_run ( ) ;
'''
    
    deleteHistory_icon = self_path + 'media/deleteHistory_icon.png' ;
    deleteHistory_cmd = header_cmd + '''
import general.utilities as utilities ;
reload ( utilities ) ;
utilities.deleteHistory_run ( ) ;
'''

    centerPivot_icon = self_path + 'media/centerPivot_icon.png' ; 
    centerPivot_cmd = header_cmd + '''
import general.utilities as utilities ;
reload ( utilities ) ;
utilities.centerPivot_run ( ) ;
'''

    ### find current shelf ###
    mel_cmd = '''
global string $gShelfTopLevel;
string $shelves = `tabLayout -q -selectTab $gShelfTopLevel`;
'''
    currentShelf = pm.mel.eval ( mel_cmd );

    pm.shelfButton ( style = 'iconOnly' , image = freezeTransform_icon , command = freezeTransform_cmd , parent = currentShelf ) ;
    pm.shelfButton ( style = 'iconOnly' , image = deleteHistory_icon , command = deleteHistory_cmd , parent = currentShelf ) ;
    pm.shelfButton ( style = 'iconOnly' , image = centerPivot_icon , command = centerPivot_cmd , parent = currentShelf ) ;