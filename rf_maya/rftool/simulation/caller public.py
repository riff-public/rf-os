import maya.cmds as mc ;
import sys ;

mp = "//riff-data/Data/Data/Pipeline/core/rf_maya/rftool/simulation" ;

#O:\Pipeline\core\rf_maya\rftool\simulation

if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;

import library.mainUI.UI as mui ;
reload ( mui ) ;

mui.run () ;