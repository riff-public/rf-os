import sys ;

mp = "//riff-data/Data/Data/Pipeline/core/rf_maya/rftool/simulation" ;

#O:\Pipeline\core\rf_maya\rftool\simulation

if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;

import simulationTools.others.anim_motionPath.core as anim_motionPath ;
reload ( anim_motionPath ) ;

anim_motionPath.run () ;