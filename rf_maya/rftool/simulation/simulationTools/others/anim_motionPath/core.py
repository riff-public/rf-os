import pymel.core as pm ;

### YAY ###
# store original attribute values
# if controller doesn't have motion path attributes...

if not pm.pluginInfo ( 'matrixNodes.mll' , q = True , loaded = True ) :
    pm.loadPlugin ( 'matrixNodes.mll' ) ;

class General ( object ) :

    def __init__ ( self ) :
        super ( General , self ).__init__() ;

    def parCon ( self , driver , driven , mo = False ) :

        name = driver.nodeName() + '_' + driven.nodeName() + '_ParCon' ;

        if not pm.objExists ( name ) :
        
            parCon = pm.parentConstraint ( driver , driven ,
                mo = mo , skipTranslate = 'none' , skipRotate = 'none' , name = name ) ;
        
        else :
            parCon = pm.PyNode ( name ) ;

        return parCon ;

class Cleanup ( object ) :

    def __init__ ( self ) :
        super ( Cleanup , self ).__init__() ;

    def freezeTransform ( self , target ) :
        pm.makeIdentity ( target , apply = True ) ;

    def deleteHistory ( self , target ) :
        pm.delete ( target , ch = True ) ;

    def centerPivot ( self , target ) :
        pm.xform ( target , cp = True ) ;

class MotionPathRig ( object ) :

    def __init__ ( self ) :

        super ( MotionPathRig , self ).__init__ () ;

        self.ctrlPoint = [ ( 18.197911 , -5.253284 , 0 ) , ( 0 , 10.506569 , 0 ) , ( -18.197911 , -5.253284 , 0 ) , ( 18.197911 , -5.253284 , 0 ) ] ;

    def setCurveColor ( self , target , color = ( 0 , 0 , 0 ) ) :

        target      = pm.PyNode ( target ) ;
        targetShape = target.getShape() ;

        targetShape.overrideEnabled.set ( True ) ;
        targetShape.overrideRGBColors.set ( True ) ;
        targetShape.overrideColorRGB.set ( color ) ;

    def posToParameter ( self , curve , pos ) :
        # return parameter
        
        curve       = pm.PyNode ( curve ) ;
        curveShape  = curve.getShape() ;

        npoc    = pm.shadingNode ( 'nearestPointOnCurve' , asUtility = True ) ;
        
        curveShape.worldSpace >> npoc.inputCurve ;
        npoc.inPosition.set ( pos ) ;

        parameter = npoc.result.parameter.get() ;
        pm.delete ( npoc ) ;

        return parameter ;

    def parameterToPos ( self , curve , parameter ) :
        # return pos

        curve       = pm.PyNode ( curve ) ;
        curveShape  = curve.getShape() ;

        poci = pm.shadingNode ( 'pointOnCurveInfo' , asUtility = True ) ; 

        curveShape.worldSpace >> poci.inputCurve ;
        poci.parameter.set ( parameter ) ;

        pos = poci.result.position.get() ;
        
        pm.delete ( poci ) ;

        return pos ;

    def createMotionPathRig ( self ) :
        ''' return newCurve , newCurve1 , newCurve2 '''

        self.getGuiInfo() ;
        self.curve.v.set ( 0 ) ;

        rigGrpName      = self.curve.nodeName() + '_MotionPathRig_Grp' ;
        ctrlGrpName     = self.curve.nodeName() + '_MotionPathCtrl_Grp' ;

        newCurveName    = self.curve.nodeName() + '_MotionPath_Crv' ;
        newCurve1Name   = self.curve.nodeName() + '_MotionPath1_Crv' ;
        newCurve2Name   = self.curve.nodeName() + '_MotionPath2_Crv' ;

        ### Check if rigGrp exists , else get newCurve , newCurve1 , newCurve2
        if not pm.objExists ( rigGrpName ) :

            ##################
            ''' Rig Group '''
            ##################
            rigGrp = pm.group ( em = True , w = True , n = rigGrpName ) ;

            for attr in [ 't' , 'r' , 's' ] :
                exec ( 'rigGrp.' + attr + '.set ( lock = True )' ) ;

            ##################
            ''' Curve Rebuilding '''
            ##################

            ### Get 'Curve' Cv Position
            curveCvPos_list = [] ;

            for i in range ( 0 , self.curve_cv ) :
                targetCv = self.curve.nodeName() + '.cv[%d]' % i ;
                pos = pm.xform ( targetCv , q = True , ws = True , t = True ) ;
                curveCvPos_list.append ( pos ) ;

            ### Adjust Cv Position from sectional proxy curve for more accuracy ###
            curveCvPosAdjusted_list = [] ;

            for i in range ( 0 , self.curve_cv ) :

                ### proxy curve ###
                proxyCurvePoints = [] ;

                for operation in [ -3 , -2 , -1 , 0 , 1 , 2 , 3 ] :
                    value = i + operation ;
                    if ( value >= 0 ) and ( value <= ( self.curve_cv -1 ) ) :
                        proxyCurvePoints.append ( curveCvPos_list[value] ) ;

                proxyCurve = pm.curve ( d = self.curve_degree , p = proxyCurvePoints ) ;

                ### get parameter from proxy curve ###
                proxyPara   = self.posToParameter ( proxyCurve , curveCvPos_list[i] ) ;
                proxyPos    = self.parameterToPos ( proxyCurve , proxyPara ) ;
                curveCvPosAdjusted_list.append ( proxyPos ) ;

                pm.delete ( proxyCurve ) ;

            ### Get 'Curve' Cv Para
            curveCvPara_list = [] ;

            for pos in curveCvPosAdjusted_list [:-1] :
                curveCvPara_list.append ( self.posToParameter ( curve = self.curve , pos = pos ) ) ;
            
            curveCvPara_list.append ( self.curveShape.maxValue.get() ) ;

            # Make parameter pair list, to calculate smoothness
            para1_list = curveCvPara_list[:-1] ;
            para2_list = curveCvPara_list[1:] ;

            # Make parameter_list for smoothness
            smoothPara_list = [] ;

            for para1 , para2 in zip ( para1_list , para2_list ) :
                
                smoothPara_list.append ( para1 )
                fraction = ( para2 - para1 ) / float ( self.rebuildSmoothness + 1 ) ;

                for i in range ( 1 , self.rebuildSmoothness + 1 ) :
                    smoothPara_list.append ( para1 + ( fraction * i ) ) ;

            smoothPara_list.append ( para2_list[-1] ) ;

            # Convert parameter back to pos
            smoothPos_list = [] ;

            for para in smoothPara_list :
                smoothPos_list.append ( self.parameterToPos ( curve = self.curve , parameter = para ) ) ;

            # Recreate curve with smooth pos
            newCurve = pm.curve ( d = self.curve_degree , p = smoothPos_list , n = newCurveName ) ;
            newCurveShape = newCurve.getShape() ;

            newCurve_degree = pm.getAttr ( newCurveShape.nodeName() + '.degree' ) ;
            newCurve_spans  = pm.getAttr ( newCurveShape.nodeName() + '.spans' ) ;
            newCurve_cv     = newCurve_degree + newCurve_spans ;

            pm.parent ( newCurve , rigGrp ) ;

            ##################
            ''' Creating Transforms '''
            ##################

            ### Aim Axes Dict ###
            aimAxesDict = {} ;
            aimAxesDict['x'] = [ 1.0 , 0.0 , 0.0 ] ;
            aimAxesDict['y'] = [ 0.0 , 1.0 , 0.0 ] ;
            aimAxesDict['z'] = [ 0.0 , 0.0 , 1.0 ] ;

            invertAimAxesDict = {} ;
            invertAimAxesDict['x'] = [ -1.0 ,  0.0 ,  0.0 ] ;
            invertAimAxesDict['y'] = [  0.0 , -1.0 ,  0.0 ] ;
            invertAimAxesDict['z'] = [  0.0 ,  0.0 , -1.0 ] ;
            
            ### Create Main Tfm(s) at Cv(s) ###
            tfm_list        = [] ;
            ctrl_list       = [] ;
            joint_list      = [] ;

            for i in range ( 0 , ( newCurve_cv + 1 ) , ( self.rebuildSmoothness ) )  :

                pos = pm.xform ( newCurve.nodeName() + '.cv[%d]' % i , q = True , ws = True , t = True ) ;

                ### ZroGrp ###
                tfm = pm.group ( em = True , w = True , n = newCurve.nodeName() + '_' + str(i) + '_Zro_Grp' ) ;

                ### Ctrl ###

                # ctrl
                ctrl = pm.curve ( d = 1 , p = self.ctrlPoint ) ;
                ctrl.rename ( newCurve.nodeName() + '_' + str(i+1) + '_Ctrl' ) ;
                self.freezeTransform ( target = ctrl ) ;
                self.deleteHistory ( target = ctrl ) ;
                self.setCurveColor ( target = ctrl , color = ( 0 , 1 , 1 ) ) ;

                ctrlShape = ctrl.getShape() ;
                ctrlShape.addAttr ( 'gimbalControl' , keyable = True , attributeType = 'bool' ) ;
                
                # gimbal ctrl
                gimbalCtrl = pm.curve ( d = 1 , p = self.ctrlPoint ) ;
                ctrl.rename ( newCurve.nodeName() + '_' + str(i+1) + 'Gimbal_Ctrl' ) ;
                gimbalCtrl.s.set ( [ 0.8 , 0.8 , 0.8 ] ) ;
                self.freezeTransform ( target = gimbalCtrl ) ;
                self.deleteHistory ( target = gimbalCtrl ) ;
                self.setCurveColor ( target = gimbalCtrl , color = ( 1 , 1 , 1 ) ) ;

                gimbalCtrlShape = gimbalCtrl.getShape() ;
                ctrlShape.gimbalControl >> gimbalCtrlShape.v ;

                pm.parent ( gimbalCtrl , ctrl ) ;

                ctrl_list.append ( ctrl ) ;
                pm.parent ( ctrl , tfm ) ;

                ### Joint ###
                joint = pm.createNode ( 'joint' , n = newCurve.nodeName() + '_' + str(i) + '_Jnt' ) ;
                joint.v.set(0) ;
                joint_list.append ( joint ) ;
                pm.parent ( joint , gimbalCtrl ) ;

                ### Snap Tfm to Cv ###
                pm.xform ( tfm , ws = True , t = pos ) ;
                tfm_list.append ( tfm ) ;

                ### Reference Tfm ###
                upVector = aimAxesDict [ self.upAxis ] ;

                if i != self.curve_cv :
                    refCv = i + 1 ;
                    aimVector = aimAxesDict [ self.frontAxis ] ;                 
                else :
                    refCv = i - 1 ;
                    aimVector = invertAimAxesDict [ self.frontAxis ] ;

                refPos = pm.xform ( newCurve.nodeName() + '.cv[%d]' % refCv , q = True , ws = True , t = True ) ;
                refGrp = pm.group ( em = True ) ;
                pm.xform ( refGrp , ws = True , t = refPos ) ;

                aimCon = pm.aimConstraint ( refGrp , tfm ,
                    offset      = [ 0.0 , 0.0 , 0.0 ] ,
                    weight      = 1 ,
                    aimVector   = aimVector ,
                    upVector    = upVector ,
                    worldUpType = 'scene' ,
                    skip        = 'none'
                    ) ;
                
                pm.delete ( aimCon ) ;
                pm.delete ( refGrp ) ;

            ### Create Crv1+2 Tfm(s) at Cv(s) ###
            newCurve1Grp_list    = [] ;
            newCurve2Grp_list    = [] ;

            tmpTfm_list = []

            for i in range ( 0 , ( newCurve_cv + 1 ) )  :
                pos = pm.xform ( newCurve.nodeName() + '.cv[%d]' % i , q = True , ws = True , t = True ) ;

                ### ZroGrp ###
                tmpTfm = pm.group ( em = True , w = True , n = newCurve.nodeName() + '_' + str(i) + '_Zro_Grp' ) ;

                ### Crv1 Grp ###
                newCurve1Grp = pm.group ( em = True , n = newCurve.nodeName() + '_' + str(i) + '_Crv1_Grp' , p = tmpTfm ) ;
                newCurve1Grp.tx.set ( -5 ) ;
                newCurve1Grp_list.append ( newCurve1Grp ) ;

                ### Crv2 Grp ###
                newCurve2Grp = pm.group ( em = True , n = newCurve.nodeName() + '_' + str(i) + '_Crv2_Grp' , p = tmpTfm ) ;
                newCurve2Grp.tx.set ( 5 ) ;
                newCurve2Grp_list.append ( newCurve2Grp ) ;

                ### Snap Tfm to Cv ###
                pm.xform ( tmpTfm , ws = True , t = pos ) ;
                tmpTfm_list.append ( tmpTfm ) ;

                ### Reference Tfm ###
                upVector = aimAxesDict [ self.upAxis ] ;

                if i != self.curve_cv :
                    refCv = i + 1 ;
                    aimVector = aimAxesDict [ self.frontAxis ] ;                 
                else :
                    refCv = i - 1 ;
                    aimVector = invertAimAxesDict [ self.frontAxis ] ;

                refPos = pm.xform ( newCurve.nodeName() + '.cv[%d]' % refCv , q = True , ws = True , t = True ) ;
                refGrp = pm.group ( em = True ) ;
                pm.xform ( refGrp , ws = True , t = refPos ) ;

                aimCon = pm.aimConstraint ( refGrp , tmpTfm ,
                    offset      = [ 0.0 , 0.0 , 0.0 ] ,
                    weight      = 1 ,
                    aimVector   = aimVector ,
                    upVector    = upVector ,
                    worldUpType = 'scene' ,
                    skip        = 'none'
                    ) ;
                
                pm.delete ( aimCon ) ;
                pm.delete ( refGrp ) ;

            newCurve1GrpPos_list = [] ;
            newCurve2GrpPos_list = [] ;

            for newCurve1Grp , newCurve2Grp in zip ( newCurve1Grp_list , newCurve2Grp_list ) :
                newCurve1GrpPos_list.append ( pm.xform ( newCurve1Grp , q = True , ws = True , t = True ) ) ;
                newCurve2GrpPos_list.append ( pm.xform ( newCurve2Grp , q = True , ws = True , t = True ) ) ;

            pm.delete ( newCurve1Grp_list ) ;
            pm.delete ( newCurve2Grp_list ) ;
            pm.delete ( tmpTfm_list ) ;

            ##################
            ''' Create New Curve1+2 '''
            ##################

            # Create Reference Curves
            newCurve1 = pm.curve ( d = newCurve_degree , p = newCurve1GrpPos_list ) ;
            newCurve1.rename ( newCurve1Name ) ;
            newCurve1Shape = newCurve1.getShape() ;
            newCurve1Shape.template.set(1) ;
            #self.setCurveColor ( target = newCurve1 , color = (  1 , 0.7 , 0.75 ) ) ;
            pm.parent ( newCurve1 , rigGrp ) ;

            newCurve2 = pm.curve ( d = newCurve_degree , p = newCurve2GrpPos_list ) ;
            newCurve2.rename ( newCurve2Name ) ;
            newCurve2Shape = newCurve2.getShape() ;
            newCurve2Shape.template.set(1) ;
            #self.setCurveColor ( target = newCurve2 , color = (  0 , 1 , 1 ) ) ;
            pm.parent ( newCurve2 , rigGrp ) ;

            ##################
            ''' Paint Skin Weight '''
            ##################

            skinCluster  = pm.skinCluster ( joint_list , newCurve ) ;
            skinCluster1 = pm.skinCluster ( joint_list , newCurve1 ) ;
            skinCluster2 = pm.skinCluster ( joint_list , newCurve2 ) ;

            for joint1 , joint2 in zip ( joint_list[:-1] , joint_list[1:] ) :
                
                cv1 = int ( joint1.split('_')[-2] ) ;
                cv2 = int ( joint2.split('_')[-2] ) ;
                
                weightVal = 1.0 / ( self.rebuildSmoothness + 1 ) ;
                
                counter = 0 ;

                for i in range ( cv1 , cv2 + 1 ) :

                        pm.skinPercent ( skinCluster , newCurve.nodeName() + '.cv[%d]' % i ,
                            transformValue = [
                                ( joint1 , 1 - ( weightVal * counter ) ) ,
                                ( joint2 , 0 + ( weightVal * counter ) ) ,
                                ] ) ;
                        pm.skinPercent ( skinCluster1 , newCurve1.nodeName() + '.cv[%d]' % i ,
                            transformValue = [
                                ( joint1 , 1 - ( weightVal * counter ) ) ,
                                ( joint2 , 0 + ( weightVal * counter ) ) ,
                                ] ) ;

                        pm.skinPercent ( skinCluster2 , newCurve2.nodeName() + '.cv[%d]' % i ,
                            transformValue = [
                                ( joint1 , 1 - ( weightVal * counter ) ) ,
                                ( joint2 , 0 + ( weightVal * counter ) ) ,
                                ] ) ;

                        counter += 1 ;

            ctrlGrp = pm.group ( em = True , w = True , n = ctrlGrpName ) ;
            pm.parent ( ctrlGrp , rigGrp ) ;
            pm.parent ( tfm_list , ctrlGrp ) ;

        else :
            newCurve    = pm.PyNode ( newCurveName ) ;
            newCurve1   = pm.PyNode ( newCurve1Name ) ;
            newCurve2   = pm.PyNode ( newCurve2Name ) ;

            print newCurve , newCurve1 , newCurve2

        return newCurve , newCurve1 , newCurve2 ;

class VectorNetwork ( object ) :

    def __init__ ( self ) :
        super ( VectorNetwork , self ).__init__ () ;

    def motionPathTfmToCurve ( self , ctrl , curve , tfm , name ) :

        ### Aim Axes Dict ###
        aimAxesDict = {} ;
        aimAxesDict['x'] = [ 1.0 , 0.0 , 0.0 ] ;
        aimAxesDict['y'] = [ 0.0 , 1.0 , 0.0 ] ;
        aimAxesDict['z'] = [ 0.0 , 0.0 , 1.0 ] ;

        invertAimAxesDict = {} ;
        invertAimAxesDict['x'] = [ -1.0 ,  0.0 ,  0.0 ] ;
        invertAimAxesDict['y'] = [  0.0 , -1.0 ,  0.0 ] ;
        invertAimAxesDict['z'] = [  0.0 ,  0.0 , -1.0 ] ;

        ctrl    = pm.PyNode ( ctrl ) ;
        curve   = pm.PyNode ( curve ) ;
        tfm     = pm.PyNode ( tfm ) ;

        # name = ctrl.nodeName() + curve.nodeName() ;

        worldUpVector = aimAxesDict [ self.upAxis ] ;

        motionPath = pm.pathAnimation ( tfm , curve ,
            fractionMode    = True , 
            follow          = True ,
            followAxis      = self.frontAxis , 
            upAxis          = self.upAxis ,
            worldUpType     = "vector" ,
            worldUpVector   = worldUpVector ,
            inverseUp       = False ,
            inverseFront    = False ,
            bank            = True , 
            bankScale       = 1 ,
            bankThreshold   = 90 ,
            startTimeU      = pm.playbackOptions ( q = True , minTime = True ) ,
            endTimeU        = pm.playbackOptions ( q = True , maxTime = True ) ,
            n               = name ,
            ) ;
        
        motionPath = pm.PyNode ( motionPath ) ;

        motionPath.allCoordinates.xCoordinate >> tfm.tx ;
        motionPath.allCoordinates.yCoordinate >> tfm.ty ;
        motionPath.allCoordinates.zCoordinate >> tfm.tz ;

        ### delete addDoubleLinear
        nodes = pm.listConnections ( motionPath , connections = True ) ;

        for node in nodes :
            for item in node :
                if item.nodeType() == 'addDoubleLinear' :
                    pm.delete ( item ) ;

        return motionPath ;

    def createVectorNetwork ( self , ctrl , newCurve , newCurve1 , newCurve2 ) :
        # return motionPathRigGrp , newCurveMotionPathGrp , newCurve_motionPath , newCurve1_motionPath , newCurve2_motionPath ;

        ctrl        = pm.PyNode ( ctrl ) ;
        newCurve    = pm.PyNode ( newCurve ) ;
        newCurve1   = pm.PyNode ( newCurve1 ) ;
        newCurve2   = pm.PyNode ( newCurve2 ) ;

        name = ctrl.nodeName() + '_' + newCurve.nodeName() ;

        motionPathRigGrpName        = ctrl.nodeName() + '_' + newCurve.nodeName() + '_MotionPathRig_Grp' ;
        
        newCurveMotionPathGrpName   = ctrl.nodeName() + '_' + newCurve.nodeName() + '_MotionPath_Grp' ;
        newCurve1MotionPathGrpName  = ctrl.nodeName() + '_' + newCurve1.nodeName() + '_MotionPath_Grp' ;
        newCurve2MotionPathGrpName  = ctrl.nodeName() + '_' + newCurve2.nodeName() + '_MotionPath_Grp' ;

        newCurveMotionPathName   = ctrl.nodeName() + '_' + newCurve.nodeName() + '_MotionPath' ;
        newCurve1MotionPathName  = ctrl.nodeName() + '_' + newCurve1.nodeName() + '_MotionPath' ;
        newCurve2MotionPathName  = ctrl.nodeName() + '_' + newCurve2.nodeName() + '_MotionPath' ;

        if not pm.objExists ( motionPathRigGrpName ) :

            motionPathRigGrp = pm.group ( em = True , w = True , n = motionPathRigGrpName ) ;

            newCurveMotionPathGrp   = pm.group ( em = True , w = True , n = newCurveMotionPathGrpName ) ;
            newCurve1MotionPathGrp  = pm.group ( em = True , w = True , n = newCurve1MotionPathGrpName ) ;
            newCurve2MotionPathGrp  = pm.group ( em = True , w = True , n = newCurve2MotionPathGrpName ) ;

            newCurve_motionPath = self.motionPathTfmToCurve (
                ctrl    = ctrl ,
                curve   = newCurve ,
                tfm     = newCurveMotionPathGrp ,
                name    = newCurveMotionPathName ) ;

            newCurve1_motionPath = self.motionPathTfmToCurve (
                ctrl    = ctrl ,
                curve   = newCurve1 ,
                tfm     = newCurve1MotionPathGrp ,
                name    = newCurve1MotionPathName ) ;

            newCurve2_motionPath = self.motionPathTfmToCurve (
                ctrl    = ctrl ,
                curve   = newCurve2 ,
                tfm     = newCurve2MotionPathGrp ,
                name    = newCurve2MotionPathName ) ;

            ### 'Front Vector' from newCurve1MotionPathGrp ;
            vectorProduct_front = pm.createNode ( 'vectorProduct' , n = name + '_VectorFront_Vp' ) ;
            vectorProduct_front.operation.set ( 3 ) ;
            vectorProduct_front.input1Z.set ( 1 ) ;

            newCurve1MotionPathGrp.worldMatrix >> vectorProduct_front.matrix ;

            ### 'Side Vector' from newCurve1MotionPathGrp , newCurve2MotionPathGrp
            vectorProduct_side = pm.createNode ( 'plusMinusAverage' , n = name + '_VectorSide_Pma' ) ;
            vectorProduct_side.operation.set(2) ;

            newCurve1MotionPathGrpWorldMatrix = pm.createNode ( 'decomposeMatrix' , n = name + '_VectorSide_Dmt' ) ;
            newCurve1MotionPathGrp.worldMatrix >> newCurve1MotionPathGrpWorldMatrix.inputMatrix ;
            newCurve1MotionPathGrpWorldMatrix.outputTranslate >> vectorProduct_side.i3.i3[0] ;

            newCurve2MotionPathGrpWorldMatrix = pm.createNode ( 'decomposeMatrix' , n = name + '_VectorSide_Dmt' ) ;
            newCurve2MotionPathGrp.worldMatrix >> newCurve2MotionPathGrpWorldMatrix.inputMatrix ;
            newCurve2MotionPathGrpWorldMatrix.outputTranslate >> vectorProduct_side.i3.i3[1] ;

            ### get upVector
            vectorProduct_up = pm.createNode ( 'vectorProduct' , n = name + '_VectorUp_Vp' ) ;
            vectorProduct_up.operation.set ( 2 ) ;
            vectorProduct_front.output >> vectorProduct_up.input2 ;
            vectorProduct_side.output3D >> vectorProduct_up.input1 ;

            ### Organization ###
            pm.parent ( [ newCurveMotionPathGrp , newCurve1MotionPathGrp , newCurve2MotionPathGrp ] , motionPathRigGrp )

            ### Finalized ###
            vectorProduct_up.output >> newCurve_motionPath.worldUpVector ;
            newCurve_motionPath.bank.set ( 1 ) ;
            newCurve_motionPath.bankLimit.set ( 360 ) ;

        else :

            motionPathRigGrp = pm.PyNode ( motionPathRigGrpName ) ;
            
            newCurveMotionPathGrp   = pm.PyNode ( newCurveMotionPathGrpName ) ;
            newCurve1MotionPathGrp  = pm.PyNode ( newCurve1MotionPathGrpName ) ;
            newCurve2MotionPathGrp  = pm.PyNode ( newCurve2MotionPathGrpName ) ;

            newCurve_motionPath     = pm.PyNode ( newCurveMotionPathName ) ;
            newCurve1_motionPath    = pm.PyNode ( newCurve1MotionPathName ) ;
            newCurve2_motionPath    = pm.PyNode ( newCurve2MotionPathName ) ;

        return motionPathRigGrp , newCurveMotionPathGrp , newCurve_motionPath , newCurve1_motionPath , newCurve2_motionPath ;

class GuiFunc ( object ) :

    def __init__ ( self ) :
        super ( GuiFunc , self ).__init__ () ;

    def ctrlGet_btn_cmd ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;

        text = '' ;

        for selection in selection_list :
            selection = pm.PyNode ( selection ) ;
            text += selection.nodeName() + ' , ' ;

        pm.textField ( self.ctrl_textField , e = True , tx = text ) ;

    def curveGet_btn_cmd ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;
        selection = selection_list[0] ;
        selection = pm.PyNode ( selection ) ;

        pm.textField ( self.curve_textField , e = True , tx = selection.nodeName() ) ;

    def getGuiInfo ( self , *args ) :

        ##################
        ### Public Variable ###
        ##################
        # self.ctrl_list
        # self.curve
        # self.curveShape
        # self.curve_degree
        # self.curve_spans
        # self.curve_cv
        # self.frontAxis
        # self.upAxis
        # self.rebuildSmoothness

        ##################
        ### Query Ctrls ###
        ##################

        ctrl_textField_info = pm.textField ( self.ctrl_textField , q = True , tx = True ) ;
        ctrl_textField_info = ctrl_textField_info.split ( ' , ' ) ;
        self.ctrl_list = [] ;

        if ctrl_textField_info :
            for ctrl in ctrl_textField_info :
                if ctrl :
                    self.ctrl_list.append ( pm.PyNode ( ctrl ) ) ;

        ##################
        ### Query Motion Path ###
        ##################

        curve_textField_info = pm.textField ( self.curve_textField , q = True , tx = True ) ;
        self.curve = None ;
        if curve_textField_info :
            self.curve      = pm.PyNode ( curve_textField_info ) ;
            self.curveShape = self.curve.getShape() ;

        self.curve_degree = pm.getAttr ( self.curveShape.nodeName() + '.degree' ) ;
        self.curve_spans  = pm.getAttr ( self.curveShape.nodeName() + '.spans' ) ;
        self.curve_cv     = self.curve_degree + self.curve_spans ;

        ##################
        ### Query Axis ###
        ##################
        axesDict    = {} ;
        axesDict[1] = 'x' ;
        axesDict[2] = 'y' ;
        axesDict[3] = 'z' ;

        ### Query Front Axis ###
        frontAxis_radioButton_info = pm.radioButtonGrp ( self.frontAxis_radioButton , q = True , select = True ) ;
        self.frontAxis = axesDict[frontAxis_radioButton_info] ;

        ### Query Up Axis ###
        upAxis_radioButton_info = pm.radioButtonGrp ( self.upAxis_radioButton , q = True , select = True ) ;
        self.upAxis = axesDict[upAxis_radioButton_info] ;

        ##################
        ### Query Rebuild Smoothness ###
        ##################

        self.rebuildSmoothness = pm.intField ( self.rebuildSmoothness_intField , q = True , v = True ) ;

    def attach_btn_cmd ( self , *args ) :

        product_list = self.createMotionPathRig () ;
        newCurve    = product_list[0] ;
        newCurve1   = product_list[1] ;
        newCurve2   = product_list[2] ;

        for ctrl in self.ctrl_list :

            product_list = self.createVectorNetwork ( ctrl = ctrl , newCurve = newCurve , newCurve1 = newCurve1 , newCurve2 = newCurve2 ) ;
            motionPathRigGrp        = product_list[0] ;
            newCurveMotionPathGrp   = product_list[1] ;
            newCurve_motionPath     = product_list[2] ;
            newCurve1_motionPath    = product_list[3] ;
            newCurve2_motionPath    = product_list[4] ;

            ### oldAttributes ###

            motionPathRigGrp.addAttr ( 'OriginalAttributeValues' , keyable = True , attributeType = 'float' ) ;
            motionPathRigGrp.OriginalAttributeValues.lock() ;

            for attr in [ 't' , 'r' ] :
                for axis in [ 'x' , 'y' , 'z' ] :
                    motionPathRigGrp.addAttr ( 'ori_' + attr + axis , keyable = True , attributeType = 'float' ) ;
                    exec ( 'val = ctrl.' + attr + axis + '.get()' ) ;
                    exec ( 'motionPathRigGrp.ori_' + attr + axis + '.set ( val )' ) ;

            if  pm.attributeQuery ( 'MotionPath' , n = ctrl , exists = True ) :

                # get rid of motion path original animation
                for motionPath in [ newCurve_motionPath , newCurve1_motionPath , newCurve2_motionPath ] :
                    targetAttr = pm.listAttr ( motionPath.uValue , m = True ) ;
                    pm.cutKey ( motionPath , at = targetAttr , option = 'keys' ) ;

                ctrl.uValue     >> newCurve_motionPath.uValue ;
                ctrl.uValue     >> newCurve1_motionPath.uValue ;
                ctrl.uValue     >> newCurve2_motionPath.uValue ;

                ctrl.frontTwist >> newCurve_motionPath.frontTwist ;
                ctrl.upTwist    >> newCurve_motionPath.upTwist ;
                ctrl.sideTwist  >> newCurve_motionPath.sideTwist ;
                ctrl.inverseUp  >> newCurve_motionPath.inverseUp ;
                ctrl.inverseFront >> newCurve_motionPath.inverseFront ;

                targetAttr = pm.listAttr ( ctrl.uValue , m = True ) ;

                startTimeU = pm.playbackOptions ( q = True , minTime = True ) ,
                endTimeU   = pm.playbackOptions ( q = True , maxTime = True ) ,

                pm.setKeyframe ( ctrl , at = targetAttr , t = startTimeU , v = 0 ) ;
                pm.setKeyframe ( ctrl , at = targetAttr , t = endTimeU , v = 1 ) ;

            else :

                # create attributes #
                motionPathRigGrp.addAttr ( 'MotionPath' , keyable = True , attributeType = 'float' ) ;
                motionPathRigGrp.MotionPath.lock() ;

                motionPathRigGrp.addAttr ( 'uValue' , keyable = True , attributeType = 'float' ) ;
                motionPathRigGrp.addAttr ( 'frontTwist' , keyable = True , attributeType = 'float' ) ;
                motionPathRigGrp.addAttr ( 'upTwist' , keyable = True , attributeType = 'float' ) ;
                motionPathRigGrp.addAttr ( 'sideTwist' , keyable = True , attributeType = 'float' ) ;

                motionPathRigGrp.addAttr ( 'inverseUp' , keyable = True , attributeType = 'bool' ) ;
                motionPathRigGrp.addAttr ( 'inverseFront' , keyable = True , attributeType = 'bool' ) ;        

                # connect attributes #
                motionPathRigGrp.uValue     >> newCurve_motionPath.uValue ;
                motionPathRigGrp.uValue     >> newCurve1_motionPath.uValue ;
                motionPathRigGrp.uValue     >> newCurve2_motionPath.uValue ;

                motionPathRigGrp.frontTwist >> newCurve_motionPath.frontTwist ;
                motionPathRigGrp.upTwist    >> newCurve_motionPath.upTwist ;
                motionPathRigGrp.sideTwist  >> newCurve_motionPath.sideTwist ;
                motionPathRigGrp.inverseUp  >> newCurve_motionPath.inverseUp ;
                motionPathRigGrp.inverseFront >> newCurve_motionPath.inverseFront ;

                targetAttr = pm.listAttr ( motionPathRigGrp.uValue , m = True ) ;

                startTimeU = pm.playbackOptions ( q = True , minTime = True ) ,
                endTimeU   = pm.playbackOptions ( q = True , maxTime = True ) ,

                pm.setKeyframe ( motionPathRigGrp , at = targetAttr , t = startTimeU , v = 0 ) ;
                pm.setKeyframe ( motionPathRigGrp , at = targetAttr , t = endTimeU , v = 1 ) ;

            parCon = self.parCon ( driver = newCurveMotionPathGrp , driven = ctrl ) ;
            # newCurveMotionPathGrp.t >> ctrl.t ;
            # newCurveMotionPathGrp.r >> ctrl.r ;

    def detach_btn_cmd ( self , *args ) :

        product_list = self.createMotionPathRig () ;
        newCurve    = product_list[0] ;
        newCurve1   = product_list[1] ;
        newCurve2   = product_list[2] ;

        for ctrl in self.ctrl_list :

            product_list = self.createVectorNetwork ( ctrl = ctrl , newCurve = newCurve , newCurve1 = newCurve1 , newCurve2 = newCurve2 ) ;
            motionPathRigGrp        = product_list[0] ;
            newCurveMotionPathGrp   = product_list[1] ;
            newCurve_motionPath     = product_list[2] ;
            newCurve1_motionPath    = product_list[3] ;
            newCurve2_motionPath    = product_list[4] ;

            parCon = self.parCon ( driver = newCurveMotionPathGrp , driven = ctrl ) ;

            pm.delete ( parCon ) ;

            for attr in [ 't' , 'r' ] :
                for axis in [ 'x' , 'y' , 'z' ] :
                    exec ( 'val = motionPathRigGrp.' + attr + axis + '.get()' ) ;
                    exec ( 'ctrl.' + attr + axis + '.set( val )' ) ;

            # Disconnect Attr

            if  pm.attributeQuery ( 'MotionPath' , n = ctrl , exists = True ) :

                ctrl.uValue     // newCurve_motionPath.uValue ;
                ctrl.uValue     // newCurve1_motionPath.uValue ;
                ctrl.uValue     // newCurve2_motionPath.uValue ;
                
                ctrl.frontTwist // newCurve_motionPath.frontTwist ;
                ctrl.upTwist    // newCurve_motionPath.upTwist ;
                ctrl.sideTwist  // newCurve_motionPath.sideTwist ;
                ctrl.inverseUp  // newCurve_motionPath.inverseUp ;
                ctrl.inverseFront // newCurve_motionPath.inverseFront ;

            else :

                motionPathRigGrp.uValue     // newCurve_motionPath.uValue ;
                motionPathRigGrp.uValue     // newCurve1_motionPath.uValue ;
                motionPathRigGrp.uValue     // newCurve2_motionPath.uValue ;

                motionPathRigGrp.frontTwist // newCurve_motionPath.frontTwist ;
                motionPathRigGrp.upTwist    // newCurve_motionPath.upTwist ;
                motionPathRigGrp.sideTwist  // newCurve_motionPath.sideTwist ;
                motionPathRigGrp.inverseUp  // newCurve_motionPath.inverseUp ;
                motionPathRigGrp.inverseFront // newCurve_motionPath.inverseFront ;

                # delete uValue anim
                targetAttr = pm.listAttr ( motionPathRigGrp.uValue , m = True ) ;
                pm.cutKey ( motionPathRigGrp , at = targetAttr , option = 'keys' ) ;

            pm.delete ( motionPathRigGrp ) ;

class Gui ( object ) :

    def __init__ ( self ) :
        super ( Gui , self ).__init__ () ;

        self.ui         = 'motionPath_ui' ;
        self.width      = 350.00 ;
        self.title      = 'Motion Path Tool' ;
        self.version    = 2.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def null ( self ) :
        pm.text ( label = '' ) ;

    def showGui ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                pm.text ( label = '(c) RiFF Animation Studio 2018, Weerapot C.' ) ;
                
                pm.separator ( h = 10 ) ;

                pm.text ( label = 'Controller' ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/3*2 ) , ( 2 , w/3 ) ] ) :
                    self.ctrl_textField = pm.textField () ;
                    self.ctrlGet_btn    = pm.button ( label = 'Get' , c = self.ctrlGet_btn_cmd ) ;

                pm.separator ( h = 10 ) ;

                pm.text ( label = 'Motion Curve' ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/3*2 ) , ( 2 , w/3 ) ] ) :
                    self.curve_textField = pm.textField () ;
                    self.curveGet_btn    = pm.button ( label = 'Get' , c = self.curveGet_btn_cmd ) ;

                pm.separator ( h = 10 ) ;

                self.frontAxis_radioButton = pm.radioButtonGrp (
                    label = 'Front Axis : ' ,
                    labelArray3 = [ 'x' , 'y' , 'z' ] ,
                    numberOfRadioButtons = 3 ,
                    cw4 = [ w/4 , w/4 , w/4 , w/4 ] ,
                    select = 3 ,
                    ) ;

                self.upAxis_radioButton = pm.radioButtonGrp (
                    label = 'Up Axis : ' ,
                    labelArray3 = [ 'x' , 'y' , 'z' ] ,
                    numberOfRadioButtons = 3 ,
                    cw4 = [ w/4 , w/4 , w/4 , w/4 ] ,
                    select = 2 ,
                    ) ;

                with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , w/3/2 ) , ( 2 , w/3 ) , ( 3 , w/3 ) , ( 4 , w/3/2 ) ] ) :
                    
                    self.null() ;
                    
                    pm.text ( label = 'Rebuild Smoothness : ' , w = w/3 ) ;
                    self.rebuildSmoothness_intField = pm.intField ( w = w/3 , v = 2 ) ;
                    
                    self.null() ;

                pm.separator ( h = 10 ) ;

                self.attach_btn = pm.button ( label = 'Attach' , w = w , c = self.attach_btn_cmd ) ;
                self.detach_btn = pm.button ( label = 'Detach' , w = w , c = self.detach_btn_cmd ) ;

                # pm.separator ( h = 10 ) ;

                # self.detach_btn = pm.button ( label = 'Detach' , w = w ) ;

                ### Ctrl Scale ###

        window.show () ;

class Main ( Gui , GuiFunc , MotionPathRig , VectorNetwork , Cleanup , General ) :

    def __init__ ( self ) :
        super ( Main , self ).__init__ () ;

def run ( *args ) :
    gui = Main() ;
    gui.showGui() ;