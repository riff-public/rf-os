import sys ;

mp = "//riff-data/Data/Data/Pipeline/core/rf_maya/rftool/simulation" ;

#O:\Pipeline\core\rf_maya\rftool\simulation

if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;

import simulationTools.others.anim_speedCalculator.core as anim_speedCalculator ;
reload ( anim_speedCalculator ) ;

anim_speedCalculator.run () ;