import pymel.core as pm ;

if not pm.pluginInfo ( 'matrixNodes.mll' , q = True , loaded = True ) :
    pm.loadPlugin ( 'matrixNodes.mll' ) ;

class SceneInfo ( object ) :

    def __init__( self ) :
        super ( SceneInfo , self ).__init__() ;

    def getCurrentPanel ( self , *args ) :
        # return current active panel list
        
        modelPanel_list     = pm.getPanel ( type = 'modelPanel' ) ;
        activePanel_list    = pm.getPanel ( vis = True ) ;

        currentModelPanel_list = [] ; 

        for panel in activePanel_list :
            if panel in modelPanel_list :
                currentModelPanel_list.append ( panel ) ;
        
        return currentModelPanel_list ;

    def getCurrentFps ( self , *args ) :
        # return int

        fpsDict = {} ;
        fpsDict['game'  ] = 15 ;
        fpsDict['film'  ] = 24 ;
        fpsDict['pal'   ] = 25 ;
        fpsDict['ntsc'  ] = 30 ;
        fpsDict['show'  ] = 48 ;
        fpsDict['palf'  ] = 50 ;
        fpsDict['ntscf' ] = 60 ;

        fpsType = pm.currentUnit ( q = True , time = True ) ;
        return ( fpsDict [ fpsType ] ) ;

    def getStartEndFrame ( self , *args ) :
        # return start , end

        start   = int ( pm.playbackOptions ( q = True , minTime = True ) ) ;
        end     = int ( pm.playbackOptions ( q = True , maxTime = True ) ) ;

        return start , end ;

class SpeedCalculator ( object ) :

    def __init__ ( self ) :
        super ( SpeedCalculator , self ).__init__() ;

    def calculateDistanceSpeed ( self , target , start , end , fps ) :
        # return distance , speed

        target = pm.PyNode ( target ) ;

        start   = int ( start ) ;
        end     = int ( end ) ;
        time    = int ( ( ( start-end ) ** 2 ) ** 0.5 ) ;

        finalDistance = 0.0 ;

        decMatrix = pm.createNode ( 'decomposeMatrix' , n = target.nodeName() + '_DecMatrix' ) ;
        target.worldMatrix >> decMatrix.inputMatrix ;

        for f1, f2 in zip ( range ( start , end ) , range ( start + 1 , end + 1 ) ) :
            f1Val = pm.getAttr ( decMatrix.outputTranslate , t = f1 ) ;
            f2Val = pm.getAttr ( decMatrix.outputTranslate , t = f2 ) ;
            distance = ( (f1Val[0]-f2Val[0])**2 + (f1Val[1]-f2Val[1])**2 + (f1Val[2]-f2Val[2])**2 ) ** 0.5 ;
            finalDistance += distance ;

        pm.delete ( decMatrix ) ;

        # fps = self.getCurrentFps() ;

        distance    = finalDistance ;
        speed       = (finalDistance/time)*fps ;
        
        return distance , speed ;

class GuiFunc ( object ) :

    def __init__ ( self ) :
        super ( GuiFunc , self ).__init__() ;

    def updateGui ( self , *args ) :

        product_list = self.getStartEndFrame() ;
        start   = product_list[0] ;
        end     = product_list[1] ;

        fps = self.getCurrentFps() ;

        pm.intField ( self.startFrame_intField , e = True , v = start ) ;
        pm.intField ( self.endFrame_intField , e = True , v = end ) ;
        pm.intField ( self.fps_intField , e = True , v = fps ) ;

    def calculate_btn_cmd ( self , *args ) :
        
        start = pm.intField ( self.startFrame_intField , q = True , v = True ) ;
        end   = pm.intField ( self.endFrame_intField , q = True , v = True ) ;
        fps   = pm.intField ( self.fps_intField , q = True , v = True ) ;

        titleTxt = '[Object Name] / [Time Range] / [Total Distance Travelled (Units)] / [Speed (Units)]' ;
        
        infoTxt_list = [] ;
        infoTxt_list.append ( titleTxt ) ;

        selection_list = pm.ls ( sl = True ) ;

        for selection in selection_list :

            product_list = self.calculateDistanceSpeed ( target = selection , start = start , end = end , fps = fps ) ;
            distance    = product_list[0] ;
            speed       = product_list[1] ;

            text  = selection.nodeName() + ' / ' ;
            text += str(start) + '-' + str(end) + ' / ' ;
            text += str(round(distance,2)) + ' / ' ;
            text += str(round(speed,2)) + ' per sec' ;
            infoTxt_list.append ( text ) ; 

            self.appendToHud ( infoTxt_list = infoTxt_list ) ;

    def appendToHud ( self , infoTxt_list ) :

        # limit to 10 objects

        radioButton = pm.radioCollection ( self.hudPos_radioCollection , q = True , sl = True ) ;
        s = pm.radioButton ( radioButton , q = True , da = True ) ;

        if len ( infoTxt_list ) > 11 :
            infoTxt_list = infoTxt_list[:11] ;

        for i in range ( 0 , len ( infoTxt_list ) ) :
            hudNm = 'SpeedInfo' + str(i) + '_HeadUpDisplay' ;
            pm.headsUpDisplay( removePosition = ( s , i ) ) ;
            pm.headsUpDisplay( hudNm , s = s , b = i , bs = 'small', l = infoTxt_list[i] ) ;

    def clearHud ( self , infoTxt_list ) :

        radioButton = pm.radioCollection ( self.hudPos_radioCollection , q = True , sl = True ) ;
        s = pm.radioButton ( radioButton , q = True , da = True ) ;

        for i in range ( 0 , 11 ) :
            pm.headsUpDisplay( removePosition = ( s , i ) ) ;
                
class Gui ( object ) :

    def __init__ ( self ) :

        super ( Gui , self ).__init__() ;

        self.ui         = 'GUI_ui' ;
        self.width      = 300.00 ;
        self.title      = 'Speed Calculator' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    def filler ( self ) :
        pm.text ( label = '' ) ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def showGui ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                pm.text ( label = '(c) RiFF Animation Studio 2018, Weerapot C.' ) ;

                pm.separator ( h = 10 ) ;

                # initialized info
                with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , w/3 ) , ( 2 , w/3 ) , ( 3 , w/3 ) ] ) :
                    
                    pm.text ( label = 'Start Frame' ) ;
                    pm.text ( label = 'End Frame') ;
                    pm.text ( label = 'Fps' ) ;

                    self.startFrame_intField    = pm.intField() ;
                    self.endFrame_intField      = pm.intField() ;
                    self.fps_intField           = pm.intField() ;

                with pm.rowColumnLayout ( nc = 1 , w = w ) :
                    pm.button ( label = 'refresh' , c = self.updateGui , w = w ) ;

                # hud position

                with pm.rowColumnLayout ( nc = 1 , w = w ) :

                    pm.text ( label = 'Hud Position' ) ;

                    self.hudPos_radioCollection = pm.radioCollection(  ) ;
                    with pm.rowColumnLayout ( nc = 5 , cw = [ ( 1 , w/5 ) , ( 2 , w/5 ) , ( 3 , w/5 )  , ( 4 , w/5 ) , ( 5 , w/5 ) ] ) : 
                        pm.radioButton ( label='0' , da = 0 ) ;
                        pm.radioButton ( label='1' , da = 1 ) ;
                        pm.radioButton ( label='2' , da = 2 ) ;
                        pm.radioButton ( label='3' , da = 3 ) ;
                        pm.radioButton ( label='4' , da = 4 ) ;
                        pm.radioButton ( label='5' , da = 5 ) ;
                        pm.radioButton ( label='6' , da = 6 ) ;
                        pm.radioButton ( label='7' , da = 7 ) ;
                        pm.radioButton ( label='8' , da = 8 ) ;
                        pm.radioButton ( label='9' , da = 9 ) ;

                    radioButton_list = pm.radioCollection ( self.hudPos_radioCollection , q = True , cia = True ) ;
                    pm.radioCollection ( self.hudPos_radioCollection , e = True , sl = radioButton_list[0] ) ;
                                    
                # calculate button
                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :
                    self.calculate_btn = pm.button ( label = 'Calculate & Insert to Hud' , w = w/2 , c = self.calculate_btn_cmd ) ;
                    self.clear_btn = pm.button ( label = 'Clear Hud' , w = w/2 , c = self.clearHud ) ;
        
        self.updateGui() ;
        window.show () ;

class Main ( SceneInfo , SpeedCalculator , Gui , GuiFunc ) :

    def __init__ ( self ) :
        super ( Main , self ).__init__() ;

def run ( *args ) :
    main = Main() ;
    main.showGui() ;

run () ;