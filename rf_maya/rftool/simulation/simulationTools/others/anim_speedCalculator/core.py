import pymel.core as pm ;

if not pm.pluginInfo ( 'matrixNodes.mll' , q = True , loaded = True ) :
    pm.loadPlugin ( 'matrixNodes.mll' ) ;

class ProjectInfo ( object ) :

    def __init__ ( self ) :

        super ( ProjectInfo , self ).__init__() ;

        self.spaceScale = 0.1 ;
        # sevenChicks

        self.spaceScaleReciprocal = 1.0/self.spaceScale ;

class SceneInfo ( object ) :

    def __init__( self ) :
        super ( SceneInfo , self ).__init__() ;

    def getCurrentPanel ( self , *args ) :
        # return current active panel list
        
        modelPanel_list     = pm.getPanel ( type = 'modelPanel' ) ;
        activePanel_list    = pm.getPanel ( vis = True ) ;

        currentModelPanel_list = [] ; 

        for panel in activePanel_list :
            if panel in modelPanel_list :
                currentModelPanel_list.append ( panel ) ;
        
        return currentModelPanel_list ;

    def getCurrentFps ( self , *args ) :
        # return int

        fpsDict = {} ;
        fpsDict['game'  ] = 15 ;
        fpsDict['film'  ] = 24 ;
        fpsDict['pal'   ] = 25 ;
        fpsDict['ntsc'  ] = 30 ;
        fpsDict['show'  ] = 48 ;
        fpsDict['palf'  ] = 50 ;
        fpsDict['ntscf' ] = 60 ;

        fpsType = pm.currentUnit ( q = True , time = True ) ;
        return ( fpsDict [ fpsType ] ) ;

    def getStartEndFrame ( self , *args ) :
        # return start , end

        start   = int ( pm.playbackOptions ( q = True , minTime = True ) ) ;
        end     = int ( pm.playbackOptions ( q = True , maxTime = True ) ) ;

        return start , end ;

class SpeedCalculator ( object ) :

    def __init__ ( self ) :
        super ( SpeedCalculator , self ).__init__() ;

    def cmToElse ( self , data ) :
    
        unit = 'cm' ;
        
        if ( data >= 100 ) and ( data < 100000 ) :
            data = data*(1.0/100) ;
            unit = 'm' ;
        
        elif data >= 100000 :
            data = data*(1.0/100000) ;
            unit = 'km' ;
        
        return data, unit ;

    def calculateDistanceSpeed ( self , target , start , end , fps ) :
        # return distance , speed

        target = pm.PyNode ( target ) ;

        start   = int ( start ) ;
        end     = int ( end ) ;
        time    = int ( ( ( start-end ) ** 2 ) ** 0.5 ) ;

        finalDistance = 0.0 ;

        decMatrix = pm.createNode ( 'decomposeMatrix' , n = target.nodeName() + '_DecMatrix' ) ;
        target.worldMatrix >> decMatrix.inputMatrix ;

        for f1, f2 in zip ( range ( start , end ) , range ( start + 1 , end + 1 ) ) :
            f1Val = pm.getAttr ( decMatrix.outputTranslate , t = f1 ) ;
            f2Val = pm.getAttr ( decMatrix.outputTranslate , t = f2 ) ;
            distance = ( (f1Val[0]-f2Val[0])**2 + (f1Val[1]-f2Val[1])**2 + (f1Val[2]-f2Val[2])**2 ) ** 0.5 ;
            finalDistance += distance ;

        pm.delete ( decMatrix ) ;

        # fps = self.getCurrentFps() ;

        distance    = finalDistance ;
        speed       = (finalDistance/time)*fps ;
        
        return distance , speed ;

class GuiFunc ( object ) :

    def __init__ ( self ) :
        super ( GuiFunc , self ).__init__() ;

    def updateGui ( self , *args ) :

        product_list = self.getStartEndFrame() ;
        start   = product_list[0] ;
        end     = product_list[1] ;

        fps = self.getCurrentFps() ;

        pm.intField ( self.startFrame_intField , e = True , v = start ) ;
        pm.intField ( self.endFrame_intField , e = True , v = end ) ;
        pm.intField ( self.fps_intField , e = True , v = fps ) ;

    def calculate_btn_cmd ( self , *args ) :
        
        start = pm.intField ( self.startFrame_intField , q = True , v = True ) ;
        end   = pm.intField ( self.endFrame_intField , q = True , v = True ) ;
        fps   = pm.intField ( self.fps_intField , q = True , v = True ) ;

        selection = pm.ls ( sl = True ) [0] ;

        product_list = self.calculateDistanceSpeed ( target = selection , start = start , end = end , fps = fps ) ;
        
        distance_product_list = self.cmToElse ( data = product_list[0]*self.spaceScaleReciprocal ) ;
        distance        = distance_product_list[0] ;
        distanceUnit    = distance_product_list[1] ;
        
        speedPerHour = ( product_list[1]*self.spaceScaleReciprocal )*60*60 ;
        speedPerHour_product_list = self.cmToElse ( data = speedPerHour ) ;
        speed       = speedPerHour_product_list[0] ;
        speedUnit   = speedPerHour_product_list[1] ;

        object_data             = 'OBJECT : ' + selection.nodeName() ;        
        frameRange_data         = 'FRAME RANGE : ' + str(start) + '-' + str(end) ;
        distance_data           = 'DISTANCE TRAVELED : ' +  str ( round ( distance,2 ) ) + ' ' + distanceUnit.title() ;
        # ( 'll' = UK , 'l' = US ) ;
        speed_data              = 'SPEED : ' + str ( round ( speed , 2 ) ) + ' ' + speedUnit.title() + ' Per Hour' ;
        
        self.clearHud() ;

        radioButton = pm.radioCollection ( self.hudPos_radioCollection , q = True , sl = True ) ;
        s = pm.radioButton ( radioButton , q = True , da = True ) ;

        # check if exists, if so delete ;

        objHudNm        = selection.nodeName() + '_obj_hud' ;
        frameRangeHudNm = selection.nodeName() + '_frameRange_hud' ;
        distanceHudNm   = selection.nodeName() + '_distance_hud' ;
        speedHudNm      = selection.nodeName() + '_speed_hud' ;

        for hud in [ objHudNm , frameRangeHudNm , distanceHudNm , speedHudNm ] :
            if pm.headsUpDisplay ( hud , q = True , exists = True ) :
                pm.headsUpDisplay ( hud , e = True , remove = True ) ;

        pm.headsUpDisplay( objHudNm , s = s , b = 1 , bs = 'small', labelFontSize = 'small' , l =  object_data ) ;
        pm.headsUpDisplay( frameRangeHudNm , s = s , b = 2 , bs = 'small', labelFontSize = 'small' , l =  frameRange_data ) ;
        pm.headsUpDisplay( distanceHudNm , s = s , b = 3 , bs = 'small', labelFontSize = 'small' , l =  distance_data ) ;
        pm.headsUpDisplay( speedHudNm , s = s , b = 4 , bs = 'small', labelFontSize = 'large' , l =  speed_data ) ;

    def appendToHud ( self , infoTxt_list ) :

        # limit to 10 objects

        radioButton = pm.radioCollection ( self.hudPos_radioCollection , q = True , sl = True ) ;
        s = pm.radioButton ( radioButton , q = True , da = True ) ;

        if len ( infoTxt_list ) > 11 :
            infoTxt_list = infoTxt_list[:11] ;

        for i in range ( 0 , len ( infoTxt_list ) - 1 ) :
            hudNm = 'SpeedInfo' + str(i) + '_HeadUpDisplay' ;
            pm.headsUpDisplay( removePosition = ( s , i ) ) ;
            pm.headsUpDisplay( hudNm , s = s , b = i , bs = 'small', labelFontSize = 'large' , l = infoTxt_list[i] ) ;

    def clearHud ( self , *args ) :

        radioButton = pm.radioCollection ( self.hudPos_radioCollection , q = True , sl = True ) ;
        s = pm.radioButton ( radioButton , q = True , da = True ) ;

        for i in range ( 0 , 11 ) :
            pm.headsUpDisplay( removePosition = ( s , i ) ) ;
                
class Gui ( object ) :

    def __init__ ( self ) :

        super ( Gui , self ).__init__() ;

        self.ui         = 'GUI_ui' ;
        self.width      = 300.00 ;
        self.title      = 'Speed Calculator' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    def filler ( self ) :
        pm.text ( label = '' ) ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def showGui ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                pm.text ( label = '(c) RiFF Animation Studio 2018, Weerapot C.' ) ;

                pm.separator ( h = 10 ) ;

                # initialized info
                with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , w/3 ) , ( 2 , w/3 ) , ( 3 , w/3 ) ] ) :
                    
                    pm.text ( label = 'Start Frame' ) ;
                    pm.text ( label = 'End Frame') ;
                    pm.text ( label = 'Fps' ) ;

                    self.startFrame_intField    = pm.intField() ;
                    self.endFrame_intField      = pm.intField() ;
                    self.fps_intField           = pm.intField() ;

                with pm.rowColumnLayout ( nc = 1 , w = w ) :
                    pm.button ( label = 'refresh' , c = self.updateGui , w = w ) ;

                # hud position

                with pm.rowColumnLayout ( nc = 1 , w = w ) :

                    pm.text ( label = 'Hud Position' ) ;

                    self.hudPos_radioCollection = pm.radioCollection(  ) ;
                    with pm.rowColumnLayout ( nc = 5 , cw = [ ( 1 , w/5 ) , ( 2 , w/5 ) , ( 3 , w/5 )  , ( 4 , w/5 ) , ( 5 , w/5 ) ] ) : 
                        pm.radioButton ( label='0' , da = 0 ) ;
                        pm.radioButton ( label='1' , da = 1 ) ;
                        pm.radioButton ( label='2' , da = 2 ) ;
                        pm.radioButton ( label='3' , da = 3 ) ;
                        pm.radioButton ( label='4' , da = 4 ) ;
                        pm.radioButton ( label='5' , da = 5 ) ;
                        pm.radioButton ( label='6' , da = 6 ) ;
                        pm.radioButton ( label='7' , da = 7 ) ;
                        pm.radioButton ( label='8' , da = 8 ) ;
                        pm.radioButton ( label='9' , da = 9 ) ;

                    radioButton_list = pm.radioCollection ( self.hudPos_radioCollection , q = True , cia = True ) ;
                    pm.radioCollection ( self.hudPos_radioCollection , e = True , sl = radioButton_list[0] ) ;
                                    
                # calculate button
                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :
                    self.calculate_btn = pm.button ( label = 'Calculate & Insert to Hud' , w = w/2 , c = self.calculate_btn_cmd ) ;
                    self.clear_btn = pm.button ( label = 'Clear Hud' , w = w/2 , c = self.clearHud ) ;
        
        self.updateGui() ;
        window.show () ;

class Main ( SceneInfo , SpeedCalculator , Gui , GuiFunc , ProjectInfo ) :

    def __init__ ( self ) :
        super ( Main , self ).__init__() ;

def run ( *args ) :
    main = Main() ;
    main.showGui() ;