import pymel.core as pm ;

class SceneInfo ( object ) :

    def __init__( self ) :
        super ( SceneInfo , self ).__init__() ;

    def getCurrentPanel ( self , *args ) :
        # return current active panel list
        
        modelPanel_list     = pm.getPanel ( type = 'modelPanel' ) ;
        activePanel_list    = pm.getPanel ( vis = True ) ;

        currentModelPanel_list = [] ; 

        for panel in activePanel_list :
            if panel in modelPanel_list :
                currentModelPanel_list.append ( panel ) ;
        
        return currentModelPanel_list ;

    def getCurrentFps ( self , *args ) :
        # return int

        fpsDict = {} ;
        fpsDict['game'  ] = 15 ;
        fpsDict['film'  ] = 24 ;
        fpsDict['pal'   ] = 25 ;
        fpsDict['ntsc'  ] = 30 ;
        fpsDict['show'  ] = 48 ;
        fpsDict['palf'  ] = 50 ;
        fpsDict['ntscf' ] = 60 ;

        fpsType = pm.currentUnit ( q = True , time = True ) ;
        return ( fpsDict [ fpsType ] ) ;

    def getStartEndFrame ( self , *args ) :
        # return start , end

        start   = int ( pm.playbackOptions ( q = True , minTime = True ) ) ;
        end     = int ( pm.playbackOptions ( q = True , maxTime = True ) ) ;

        return start , end ;

def test() ;
    info = SceneInfo() ;
    print info.getCurrentFps() ;