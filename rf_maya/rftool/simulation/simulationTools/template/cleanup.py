import pymel.core as pm ;
import re ;

class Cleanup ( object ) :

    def __init__ ( self ) :
        super ( Cleanup , self ).__init__() ;

    def freezeTransform ( self , target ) :
        pm.makeIdentity ( target , apply = True ) ;

    def deleteHistory ( self , target ) :
        pm.delete ( target , ch = True ) ;

    def centerPivot ( self , target ) :
        pm.xform ( target , cp = True ) ;