import maya.cmds as mc

def run (*args ) :
    
    selN1 = mc.ls(selection = True)
    print selN1
    numN1 = len(selN1)
    print numN1
        
    if  0 >= numN1:
        print 'nothing seleted'
        
    else:
        print 'yes'

        c1 = mc.curve(p = [ ( 4 , 0 , 1 ) , ( 4 , 0 , 3 ) , ( 7 , 0 , 0 ) , ( 4 , 0 , -3 ) , ( 4 , 0 , -1 ) , ( 1 , 0 , -1 ) , ( 1 , 0 , -4 ) , 
                    ( 3 , 0 , -4 ) , ( 0 , 0 , -7 ) , ( -3 , 0 , -4 ) , ( -1 , 0 , -4 ) , ( -1 , 0 , -1 ) , ( -4 , 0 , -1 ) , ( -4 , 0 , -3 ) , 
                    ( -7 , 0 , 0 ) , ( -4 , 0 , 3 ) , ( -4 , 0 , 1 ) , ( -1 , 0 , 1 ) , ( -1 , 0 , 4 ) , ( -3 , 0 , 4 ) , ( 0 , 0 , 7 ) , ( 3 , 0 , 4 ) , 
                    ( 1 , 0 , 4 ) , ( 1 , 0 , 1 ) , ( 4 , 0 , 1 ) ],d=1)
                    
        c2 = mc.curve (p = [ ( 0 , -15 , 0 ) , ( -1 , 0 , -1 ) , ( -1 , 0 , 1 ) , ( -0 , -15 , -0 ) ],d=1)
    
        c3 = mc.curve (p =[ ( 0 , -15 , 0 ) , ( 1 , 0 , -1 ) , ( 1 , 0 , 1 ) , ( 0 , -15 , 0 ) ], d=1)
    
        c1Sh = mc.listRelatives(c1)
        c2Sh = mc.listRelatives(c2)
        c3Sh = mc.listRelatives(c3)
    
        mc.parent( c2Sh ,c3Sh ,c1, relative =True, shape = True)
    
        mc.delete(c2, c3)
    
        selC = mc.select(c1)
        sels = mc.ls(sl = True)
        mainCtrl = mc.rename('%s'%sels[0],'WindDirection_Ctrl')
        print mainCtrl
        
        mc.setAttr ("%s.overrideEnabled"%mainCtrl, 1)
        mc.setAttr ("%s.overrideColor"%mainCtrl,16)
    
        locWndA = mc.spaceLocator(n = 'locWndA')
        print locWndA[0]
            
        locWndB = mc.spaceLocator(n = 'locWndB')
        mc.move(0,-1,0)
        
        selWndA = mc.select(locWndA[0])
        selWndB = mc.select(locWndB[0], add = True)
        mc.hide()
        
    
        #parent
        mc.parent('%s'%locWndA[0],'%s'%mainCtrl)
        mc.parent('%s'%locWndB[0],'%s'%mainCtrl)
    
        #constrain
        pConW = mc.pointConstraint('%s'%locWndA[0],'%s'%locWndB[0], mo = True)
        print pConW[0]
    
        #CreateNode
        vp = mc.createNode('vectorProduct')
        vp = mc.rename('%s'%vp, 'Wnd_vp')
        print vp
        
    
        #ConnectNode
        mc.connectAttr('%s.worldMatrix[0]'%locWndA[0], '%s.matrix'%vp)
    
        mc.connectAttr('%s.constraintTranslate'%pConW[0], '%s.input1'%vp)
    
        mc.connectAttr('%s.output'%vp, '%s.windDirection'%selN1[0])
    
        vp = mc.setAttr('%s.operation'%vp, 3)