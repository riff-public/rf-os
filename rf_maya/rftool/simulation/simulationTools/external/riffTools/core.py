class Riff ( ) :

	def __init__ ( self ) :
		pass ;

	def kheng_colorAssigner ( self , *args ) :
		import simulationTools.external.riffTools.kheng_colorAssigner.core as kheng_colorAssigner ;
		reload ( kheng_colorAssigner ) ;
		kheng_colorAssigner.run() ;

	def june_updateShowMenu ( self , *args ) :
		import simulationTools.external.riffTools.june_updateShowMenu.updateShowMenuUIJune as updateShowMenuUIJune ;
		reload ( updateShowMenuUIJune ) ;
		updateShowMenuUIJune.updateShowMenuUI() ;

	def fee_windRig ( self , *args ) :
		import simulationTools.external.riffTools.fee_windRig.core as fee_windRig ;
		reload ( fee_windRig ) ;
		fee_windRig.run() ;