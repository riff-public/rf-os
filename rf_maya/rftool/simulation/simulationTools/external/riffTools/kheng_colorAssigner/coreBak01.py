import maya.cmds as mc ;
import pymel.core as pm ;

#Color library
colorList = [
    ['Rose Quart'   , ( 0.969, 0.792, 0.792 ) ] , #roseQuart
    ['Salmon'       , ( 0.980, 0.502, 0.447 ) ] ,
    ['Red'          , ( 1    , 0    , 0     ) ] ,
    ['Burnt Orange' , ( 0.953, 0.443, 0.2   ) ] ,
    ['Orange'       , ( 1    , 0.644, 0     ) ] ,
    ['yellow'       , ( 1    , 1    , 0     ) ] ,
    ['Electric Lime', ( 0.823, 1    , 0     ) ] ,
    ['Yellow Green' , ( 0.519, 0.812, 0.207 ) ] ,
    ['Kelly Green'  , ( 0.275, 0.62 , 0     ) ] ,
    ['Green'        , ( 0    , 0.5  , 0     ) ] ,
    ['Surfie Green' , ( 0.02 , 0.439, 0.455 ) ] ,
    ['Boston Blue'  , ( 0.239, 0.569, 0.643 ) ] ,
    ['Robin Egg Blue',( 0    , 0.8  , 0.8   ) ] ,
    ['Navy Blue'    , ( 0    , 0.4  , 0.8   ) ] ,
    ['Medium Blue'  , ( 0    , 0    , 0.8   ) ] ,
    ['Persian Indigo',( 0.2  , 0    , 0.4   ) ] ,
    ['Purple'       , ( 0.5  , 0    , 0.5   ) ] ,
    ['Magenta'      , ( 1    , 0    , 1     ) ] ,
    ['Skin1'        , ( 1    , 0.67 , 0.469 ) ] ,
    ['Skin2'        , ( 1    , 0.685, 0.55  ) ] ,
    ['skin3'        , ( 0.6  , 0.298, 0.161 ) ] ,
    ['Black'        , ( 0    , 0    , 0     ) ] ,
    ['Dim Gray'     , ( 0.25 , 0.25 , 0.25  ) ] ,
    ['Gray'         , ( 0.5  , 0.5  , 0.5   ) ] ,
    ['Sliver'       , ( 0.75 , 0.75 , 0.75  ) ] ,
    ['White'        , ( 1    , 1    , 1     ) ] ,
    ]

hueColorList = [
    ['Rose Quart'   , ( 0.969, 0.792, 0.792 ) ] ,
    ['Rose Bud'     , ( 0.988, 0.729, 0.701 ) ] ,
    ['Radical Red'  , ( 1.000, 0.458, 0.458 ) ] ,
    ['Hit Pink'     , ( 1.000, 0.698, 0.458 ) ] ,
    ['Golden Tainai', ( 1.000, 0.803, 0.458 ) ] ,
    ['Milan'        , ( 0.980, 0.956, 0.513 ) ] ,
    ['Honeysuckle'  , ( 0.870, 1.000, 0.298 ) ] ,
    ['Feijoa'       , ( 0.670, 0.827, 0.450 ) ] ,
    ['Mantis'       , ( 0.478, 0.733, 0.298 ) ] ,
    ['Fruit Salad'  , ( 0.298, 0.650, 0.298 ) ] ,
    ['Cadet Blue'   , ( 0.313, 0.607, 0.615 ) ] ,
    ['Glacier'      , ( 0.466, 0.698, 0.749 ) ] ,
    ['Medium Turquoise',(0.388,0.784, 0.811 ) ] ,
    ['Picton Blue'  , ( 0.317, 0.572, 0.807 ) ] ,
    ['Neon Blue '   , ( 0.360, 0.360, 0.941 ) ] ,
    ['Ce Soir'      , ( 0.521, 0.360, 0.678 ) ] ,
    ['Violet Blue'  , ( 0.643, 0.305, 0.619 ) ] ,
    ['Fuchsia Pink' , ( 1.000, 0.556, 0.996 ) ] ,
    ['Skin1'        , ( 1    , 0.67 , 0.469 ) ] ,
    ['Skin2'        , ( 1    , 0.685, 0.55  ) ] ,
    ['skin3'        , ( 0.6  , 0.298, 0.161 ) ] ,
    ['Black'        , ( 0    , 0    , 0     ) ] ,
    ['Dim Gray'     , ( 0.25 , 0.25 , 0.25  ) ] ,
    ['Gray'         , ( 0.5  , 0.5  , 0.5   ) ] ,
    ['Sliver'       , ( 0.75 , 0.75 , 0.75  ) ] ,
    ['White'        , ( 1    , 1    , 1     ) ] , 
    ]

basicColorList = [
    ['BasicRed'     , ( 1.00 , 0.40 , 0.40  ) ] ,
    ['BasicPink'    , ( 1.00 , 0.40 , 0.80  ) ] ,
    ['BasicPurple'  , ( 0.80 , 0.40 , 1.00  ) ] ,
    ['BasicDarkPurple'   , ( 0.50 , 0.40 , 1.00  ) ] ,
    ['BasicBlue'         , ( 0.40 , 0.60 , 1.00  ) ] ,
    ['BasicLightBlur'    , ( 0.40 , 0.90 , 1.00  ) ] ,
    ['BasicLightGreen'   , ( 0.40 , 1.00 , 0.70  ) ] ,
    ['BasicGreen'        , ( 0.60 , 1.00 , 0.40  ) ] ,
    ['BasicYellow'       , ( 1.00 , 0.90 , 0.40  ) ] ,
    ['BasicOrange'       , ( 1.00 , 0.60 , 0.40  ) ] ,
    ['BasinSkin'         , ( 1.00 , 0.685, 0.55  ) ] ,
    ['BasicBlack'        , ( 0.00 , 0.00 , 0.00  ) ] ,
    ['BasicDark'         , ( 0.25 , 0.25 , 0.25  ) ] ,
    ['BasicGray'         , ( 0.50 , 0.50 , 0.50  ) ] ,
    ['BasicLightGray'    , ( 0.75 , 0.75 , 0.75  ) ] ,
    ['BasicWhite'        , ( 1.00 , 1.00 , 1.00  ) ] ,
    ]
'''Color Assign Shader Scirpt'''

def composeName ( node ) :
    
    listOfName = node.split(" ")    
    name = listOfName[0].lower() ;

    for each in listOfName[1:] :
        name = name + each.capitalize() ;

    return name ; 

######################################
#BasicColor
for color in basicColorList :
        # ['Rose Quart' , ( 0.969, 0.792, 0.792 ) ]
        name = composeName ( node = color[0] ) ;
        code = color[1] ;
        
        cmd = '''
def assignShade{name}_cmd ( *args ) :
    listObj = pm.ls ( sl = True )
    if pm.objExists('{name}'):
        pass
    else :
        create (colorName = '{name}', colorCode = {code} );

    print ('{name}') 
    for selectAssign in listObj :
        pm.select ( str(selectAssign) ) ;
        pm.hyperShade ( assign = '{name}')
'''.format ( name = name.capitalize(), code = code ) ;
        exec ( cmd ) ;

def makeBasicButtons ( w , *args ) :

    for color in basicColorList :
        # ['Rose Quart' , ( 0.969, 0.792, 0.792 ) ]
        name = composeName ( node = color[0] ) ;
        code = color[1] ;
        
        buttonCmd = 'pm.button ( name , label = color[0] , bgc = color[1] , w = w, c = assignShade{name}_cmd ) ;'.format ( name = name.capitalize() ) ;
        exec ( buttonCmd ) ;

####################################################        

#PrimeTone
for color in colorList :
        # ['Rose Quart' , ( 0.969, 0.792, 0.792 ) ]
        name = composeName ( node = color[0] ) ;
        code = color[1] ;
        
        cmd = '''
def assignShade{name}_cmd ( *args ) :
    listObj = pm.ls ( sl = True )
    if pm.objExists('{name}'):
        pass
    else :
        create (colorName = '{name}', colorCode = {code} );

    print ('{name}') 
    for selectAssign in listObj :
        pm.select ( str(selectAssign) ) ;
        pm.hyperShade ( assign = '{name}')
'''.format ( name = name.capitalize(), code = code ) ;
        exec ( cmd ) ;

def makeButtons ( w , *args ) :

    for color in colorList :
        # ['Rose Quart' , ( 0.969, 0.792, 0.792 ) ]
        name = composeName ( node = color[0] ) ;
        code = color[1] ;
        
        buttonCmd = 'pm.button ( name , label = color[0] , bgc = color[1] , w = w, c = assignShade{name}_cmd ) ;'.format ( name = name.capitalize() ) ;
        exec ( buttonCmd ) ;

####################################################################
#HueTone
for color in hueColorList :
        # ['Rose Quart' , ( 0.969, 0.792, 0.792 ) ]
        name = composeName ( node = color[0] ) ;
        code = color[1] ;
        
        cmd = '''
def assignShade{name}_cmd ( *args ) :
    listObj = pm.ls ( sl = True )
    if pm.objExists('{name}'):
        pass
    else :
        create (colorName = '{name}', colorCode = {code} );

    print ('{name}') 
    for selectAssign in listObj :
        pm.select ( str(selectAssign) ) ;
        pm.hyperShade ( assign = '{name}')
'''.format ( name = name.capitalize(), code = code ) ;
        exec ( cmd ) ;

def makeHueButtons ( w , *args ) :

    for color in hueColorList :
        # ['Rose Quart' , ( 0.969, 0.792, 0.792 ) ]
        name = composeName ( node = color[0] ) ;
        code = color[1] ;
        
        buttonCmd = 'pm.button ( name , label = color[0] , bgc = color[1] , w = w, c = assignShade{name}_cmd ) ;'.format ( name = name.capitalize() ) ;
        exec ( buttonCmd ) ;

#####################################################################
#ExtraColor
def greenScreenColor (*args) :
    listObj = pm.ls ( sl = True )

    if pm.objExists('greenScreen'):
        pass
    else :
        pm.shadingNode ("surfaceShader" ,asShader = True , n = 'greenScreen')
        pm.setAttr ("greenScreen.outColor", (0 , 1 , 0), typ = "double3" )

    for greenAssign in listObj :
        pm.select ( str(greenAssign) ) ;
        pm.hyperShade ( assign = 'greenScreen')

def transparencyShader (*args) :
    listObj = pm.ls ( sl = True )

    if pm.objExists('transparencyShade'):
        pass
    else :
        pm.shadingNode ("blinn" ,asShader = True , n = 'transparencyShade')
        pm.setAttr ("transparencyShade.color", (0.75 , 0.75 , 0.75), typ = "double3" )
        pm.setAttr ("transparencyShade.transparency", (0.75 , 0.75 , 0.75), typ ="double3" ) ;
        pm.setAttr ("transparencyShade.eccentricity", 0.6 ) ; 
        pm.setAttr ("transparencyShade.specularRollOff", 0.3 ) ;

    for tranAssign in listObj :
        pm.select ( str(tranAssign) ) ;
        pm.hyperShade ( assign = 'transparencyShade')


def create( colorName , colorCode ):
    if pm.objExists('colorName'):
        pass

    else :
        exec ('pm.shadingNode ( "blinn" , asShader = True , n = colorName ) ;')
        exec ('pm.setAttr ( "%s.color" % colorName , colorCode , typ="double3" ) ;' );
        exec ('pm.setAttr ( "%s.eccentricity" % colorName, 0.6 );' )
        exec ('pm.setAttr ("%s.specularRollOff" % colorName, 0.3 );')
        exec ('pm.setAttr ("%s.specularColor" % colorName, (0.15, 0.15, 0.15),typ="double3");')

def assignLoopColor ( *args ):
    listObj = pm.ls ( sl = True )
    array = 0
    colorCount = len(colorList) - 8

    # for i in range ( 0 , len(listObj) ) :
    #   print listObj[i] ;

    for targetObj in listObj :
        color = colorList[array] ;
        name = composeName ( node = color[0] )
        cmdAssign = 'assignShade{clName}_cmd()'.format ( clName = name.capitalize() ) ;
        
        pm.select ( targetObj ) ;
        
        exec ( cmdAssign ) ;

        if array > colorCount :
            array = 0 ;
        else :
            array += 1 ;

def deleteUnusedNodes(*args):
    pm.mel.eval('MLdeleteUnused;')

def windowUI ( *args ) :
    w = 250.00 
    # check if window exists
    if pm.window ( 'windowUI' , exists = True ) :
        pm.deleteUI ( 'windowUI' ) ;
    else : pass ;
   
    window = pm.window ( 'windowUI', title = "Window" ,
        mnb = True , mxb = False , sizeable = False , rtf = True ) ;
        
    pm.window ( 'windowUI' , e = True , w = w , h = 100 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            with pm.tabLayout (w = w) :
                with pm.rowColumnLayout ('BasicTone' , w = w) :
                    makeBasicButtons (w = w) ;

                with pm.rowColumnLayout ('PrimeTone' , w = w) :
                    makeButtons (w = w) ;
            
                with pm.rowColumnLayout ( 'HueTone' , w = w ) :
                    makeHueButtons (w = w ) ;

            mc.separator( width = 50, height=10 , style='out' )
            mc.button ( label ='GreenScreen', c = greenScreenColor , bgc = (0,1,0))
            mc.button ( label ='transparency', c = transparencyShader , bgc = (0.25 ,0.25 ,0.25))
            mc.button ( label ='AssignShadeLoop', c = assignLoopColor )
            mc.button ( label ='Delete Unuse Notes', c = deleteUnusedNodes )
            
    window.show () ;

def run ( *args ) :
    windowUI ( ) ;