import pymel.core as pm ;

class Util ( object ) :

    def __init__ ( self ) :
        super ( Util , self ).__init__( ) ;

    def copyCurveShape ( self ,  driverSuffix , drivenSuffix ) :

        if not driverSuffix :
            print ( 'invalid driverSuffix' ) ;
        else :

            driver_list = pm.ls ( '*%s' % driverSuffix , type = 'transform' ) ;
            if not driver_list :
                print ( "Crv%s doesn't exist?" % driverSuffix ) ;
            else :

                if not drivenSuffix :
                    print ( 'invalid drivenSuffix' ) ;
                else :
                    
                    driven_list = pm.ls ( '*%s' % drivenSuffix , type = 'transform' ) ;
                    if not driven_list :
                        print ( "Crv%s doesn't exist?" % drivenSuffix ) ;
                    else :
                        
                        for driver in driver_list :
                            try :
                                driverName = driver.nodeName() ;
                                name = driverName.split ( driverSuffix ) [0] ;
                                driven = pm.general.PyNode ( name + drivenSuffix ) ;

                                driverShape = driver.getShape() ;
                                drivenShape = driven.getShape() ;

                                degree  = pm.getAttr ( driverShape + '.degree' ) ;
                                spans   = pm.getAttr ( driverShape + '.spans' ) ;
                                cv      = degree + spans ;

                                for i in range ( 0 , cv + 1 ) :
                                    pos = pm.xform ( '%s.cv[%s]' % ( driver , str(i) ) , q = True , ws = True , t = True ) ;
                                    pm.xform ( '%s.cv[%s]' % ( driven , str(i) ) , ws = True , t = pos ) ; 
                            except :
                                print ( 'No operation from [%s]' % driver ) ;

    def copyShape_cmd ( self , *args ) :

        driverSuffix = pm.textField ( self.driverSuffix_textField , q = True , tx = True ) ;
        if not driverSuffix :
            driverSuffix = None ;

        drivenSuffix = pm.textField ( self.drivenSuffix_textField , q = True , tx = True ) ;
        if not drivenSuffix :
            drivenSuffix = None ;

        self.copyCurveShape ( driverSuffix , drivenSuffix ) ;

    def callWindow ( self ) :
        self.showWindow() ;