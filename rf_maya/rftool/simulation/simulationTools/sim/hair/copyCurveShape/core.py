modulePath = 'simulationTools.sim.hair.copyCurveShape.' ;

for module in [ 'gui' , 'util' ] :
    exec ( 'import {modulePath}{module} as {module} ;'.format ( modulePath = modulePath , module = module ) ) ;
    exec ( 'reload ( {module} ) ;'.format ( module = module ) ) ;

class CopyCurveShape ( gui.Gui , util.Util ) :
    def __init__ ( self ) :
        super ( CopyCurveShape , self ).__init__ ( ) ;

def run ( ) :
    copyCurveShape = CopyCurveShape() ;
    copyCurveShape.callWindow ( ) ;