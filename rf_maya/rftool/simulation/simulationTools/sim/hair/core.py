class Hair ( object ) :

    def __init__ ( self ) :
        super ( Hair , self ).__init__ ( ) ;

    def copyCurveShape ( self , *args ) :
        import simulationTools.sim.hair.copyCurveShape.core as copyCurveShape ;
        reload ( copyCurveShape ) ;
        copyCurveShape.run() ;