import pymel.core as pm ;
import re ;

class NConstraint ( object ) :
    
    def __init__ ( self ) :
        pass ;

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def checkIncrement ( self , name , incrementIncrement = True , *args ) :

        allItemShape_list = pm.ls ( '{name}*'.format ( name = name ) , type = 'dynamicConstraint' ) ;
        allItem_list    = [] ;
        validItem_list  = [] ;

        for itemShape in allItemShape_list :
            allItem_list.append ( itemShape.getTransform() ) ;

        for item in allItem_list :
            if re.findall ( name + '[0-9]{2}' , str(item) ) :
                validItem_list.append ( item ) ;

        validItem_list.sort() ;
            
        if not validItem_list :
            
            finalIncrement = '01' ;

        else :
            
            currentIncrement = validItem_list[-1] ;

            for elem in [ name , '_Dyc' ] :

                currentIncrement = currentIncrement.split ( elem ) ;

                for split in currentIncrement :
                    if not split :
                        currentIncrement.remove ( split ) ;

                currentIncrement = ( currentIncrement[0] ) ;

            if incrementIncrement :
                currentIncrement = int ( currentIncrement ) ;
                finalIncrement = str ( currentIncrement + 1 ) ;
                finalIncrement = finalIncrement.zfill(2) ;

        return ( finalIncrement ) ;

    def composeName ( self , names_list ) :

        composedName = '' ;
        nameSplits_list = [] ;

        for name in names_list :
            name = pm.general.PyNode ( name ) ;
            name = name.nodeName() ;
            nameSplits_list.append ( name.split ( '_' ) [0] ) ;

        composedName += nameSplits_list[0].capitalize() ;

        if len ( nameSplits_list ) > 1 :
            for name in nameSplits_list [1:] :
                composedName += '_' + name.capitalize() ;

        index = self.checkIncrement ( name = composedName ) ;

        composedName += index ;
        composedName += '_Dyc' ;

        return composedName ;

    def getNObjects ( self , target ) :
        # for Point Constraint, return [ driver , driven ]
        # [nt.Transform(u'Body_Geo_COL_nRigid'), nt.Transform(u'Shirts_DYN_nCloth')]

        target = pm.general.PyNode ( target ) ;
        targetShape = target.getShape() ;
        return_list = [] ;
        
        nComponents_list = targetShape.listConnections( type = 'nComponent' ) ;
        
        if len ( nComponents_list ) == 1 :
            
            nComponent = nComponents_list[0] ;
            
            connections_list = nComponent.listConnections() ;
            
            if target in connections_list :
                connections_list.remove ( target ) ;
            return_list.append ( connections_list[0] ) ;

        elif len > 1 :

            driver = '' ;
            driven = '' ;

            for nComponent in nComponents_list :
                connections_list = nComponent.listConnections() ;
                
                if target in connections_list :
                    connections_list.remove ( target ) ;

                connection = connections_list[0] ;

                if nComponent.elements.get() == 0 :
                    driver = connection ;
                elif nComponent.elements.get() == 2 :
                    driven = connection ;

                '''
                elements
                0 = from indice list
                1 = borders
                2 = all

                from 0 to 2
                '''

            return_list.append ( driver ) ;
            return_list.append ( driven ) ;

        return return_list ;

    def rename ( self ) :

        selection_list = pm.ls ( sl = True ) ;

        for selection in selection_list :
            names_list = self.getNObjects ( target = selection ) ;
            constraintName = self.composeName ( names_list = names_list ) ;

            constraintName = self.composeName ( names_list = self.getNObjects ( target = selection ) ) ;
            print constraintName ;            
            selection.rename ( constraintName ) ;

def run ( *args ) :

    nc = NConstraint() ;
    nc.rename() ;

run() ;