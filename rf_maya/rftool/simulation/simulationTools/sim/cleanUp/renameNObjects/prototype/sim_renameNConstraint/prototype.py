import pymel.core as pm ;


class NConstraint ( object ) :
    
	def __init__ ( self ) :
		pass ;

	def __str__ ( self ) :
		pass ;

	def __repr__ ( self ) :
		pass ;

	def composeName ( self , target ) :
		# return composedName ;
		target = pm.general.PyNode ( target ) ;
		target = target.nodeName() ;

		nameSplit = target.split ( '_' ) ;

		composedName = nameSplit[0] ;

		if len ( nameSplit ) > 1 :

			for name in nameSplit[1:] :
				composedName += name[0].upper() ;
				composedName += name[1:] ;
		
		return composedName ;

	def rename ( self ) :

		selection_list = pm.ls ( sl = True ) ;

		for selection in selection_list :

			constraintName 		= '' ;
			constraintName_list = [] ;

			connectionConnection = None ;

			selectionShape =  selection.getShapes() [0];
			
			print selectionShape.listConnections() ;

			for shapeConnection in selectionShape.listConnections() :
				
				if shapeConnection.nodeType() == 'nComponent' :
					connectionConnection_list = shapeConnection.listConnections() ;
					print shapeConnection, shapeConnection.listConnections() ;
				
					if selection in connectionConnection_list :
						connectionConnection_list.remove ( selection ) ;
						connectionConnection = connectionConnection_list[0] ;

					if connectionConnection :
						constraintName_list.append ( self.composeName ( target = connectionConnection ) ) ;

			constraintName = constraintName_list[0] ;
			
			if len ( constraintName_list ) > 1 :
				for name in constraintName_list[1:] :
					constraintName += '_' + name ;

			constraintName += '_dyc' ;

			print constraintName ;

def run ( *args ) :
	nc = NConstraint() ;
	nc.rename() ;