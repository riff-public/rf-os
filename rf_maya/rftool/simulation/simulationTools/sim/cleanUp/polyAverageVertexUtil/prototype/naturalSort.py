import re ;
arrowEpisode_list = [
    'ArrowSeason1_ep1' ,
    'ArrowSeason1_ep10' ,
    'ArrowSeason1_ep2' ,
    'ArrowSeason1_ep23' ,
    'ArrowSeason1_ep87' ,
    'ArrowSeason1_ep5' ,
    'ArrowSeason1_ep6'] ;

# class NaturalSort ( ) :

#     def __init__ ( self ) :
#         pass ;

#     def 

def convert ( input ) :

    if input.isdigit() :
        return int ( input ) ;
    else :
        return input.lower() ;
        
def alphanum_key ( key ) :
    return_list = [] ;
    for c in re.split ( '([0-9]+)', key )  :
        return_list.append ( convert ( c ) ) ;
    return return_list ;

for each in arrowEpisode_list :
    print alphanum_key ( each ) ;

####

# def natural_sort(l): 
#     convert = lambda text: int(text) if text.isdigit() else text.lower() 
#     alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
#     return sorted(l, key = alphanum_key)