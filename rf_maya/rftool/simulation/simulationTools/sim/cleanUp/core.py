class CleanUp ( object ) :

	def __init__ ( self ) :
		super ( CleanUp , self ).__init__ ( ) ;

	def renameNObects ( self , *args ) :
	    import simulationTools.sim.cleanUp.renameNObjects.core as renameNObjects ;
	    reload ( renameNObjects ) ;
	    renameNObjects.run() ;

	def polyAverageVertexUtil ( self , *args ) :
	    import simulationTools.sim.cleanUp.polyAverageVertexUtil.core as polyAverageVertexUtil ;
	    reload ( polyAverageVertexUtil ) ;
	    polyAverageVertexUtil.run() ;

	def deleteHierarchyConstraints ( self , *args ) :
		import simulationTools.sim.cleanUp.deleteHierarchyConstraints.core as deleteHierarchyConstraints ;
		reload ( deleteHierarchyConstraints ) ;
		deleteHierarchyConstraints.run() ;