import pymel.core as pm ;

def run ( *args ) :

	selection = pm.ls ( sl = True ) ;
	toDelete_list = [] ;

	for each in selection :
	    
	    children_list = each.listRelatives ( ad = True , type = 'transform' ) ;
	    
	    for child in children_list :
	        if child.nodeType() in [ 'parentConstraint' , 'scaleConstraint' , 'pointConstraint' ] :
	            toDelete_list.append ( child ) ;

	pm.delete ( toDelete_list ) ;