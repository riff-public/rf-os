modulePath = 'simulationTools.sim.wip.simUtil.' ;

for module in [ 'gui' , 'nucleus' ] :
	exec ( 'import {modulePath}{module} as {module} ;'.format ( modulePath = modulePath , module = module ) ) ;
	exec ( 'reload ( {module} ) ;'.format ( module = module ) ) ; 

class SimUtil ( gui.Gui , nucleus.Nucleus ) :

	def __init__ ( self ) :
		super ( SimUtil , self ).__init__() ;

def run ( *args ) :
	simUtil = SimUtil() ;
	simUtil.showGui() ;