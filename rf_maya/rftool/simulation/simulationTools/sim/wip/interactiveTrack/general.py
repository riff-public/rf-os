import pymel.core as pm ;

class CleanUp ( object ) :

    def __init__ ( self ) :
        super ( CleanUp, self ).__init__() ;

    def freezeTransform ( self , target ) :
        
        target = pm.general.PyNode ( target ) ;

        oldSettings_dict = {} ;
        attr_list = [] ;

        for attr in [ 't'  , 'r' , 's' ] :
            for axis in [ 'x' , 'y' , 'z' ] :
                attr_list.append ( attr + axis ) ;
        attr_list.append ( 'v' ) ;

        for attr in attr_list :
            for attrAttr in [ 'channelBox' , 'keyable' , 'lock' ] :
                exec ( "oldSettings_dict [ '%s_{attrAttr}' % attr ] = target.getAttr ( attr , {attrAttr} = True )".format ( attrAttr = attrAttr ) ) ;
            
            exec ( "target.{attr}.set( channelBox = True , keyable = True , lock = False )".format ( attr = attr ) ) ;

        pm.makeIdentity ( target , apply = True ) ;

        for attr in attr_list :
            cmd = "target.%s.set (" % attr ;
            cmd += "channelBox = oldSettings_dict ['%s_channelBox'] ," % attr ;
            cmd += "keyable = oldSettings_dict ['%s_keyable'] ," % attr ;
            cmd += "lock = oldSettings_dict ['%s_lock'] ) ;" % attr ;
            exec (  cmd ) ;

    def deleteHistory ( self , target ) :
        pm.delete ( target , ch = True ) ;

    def centerPivot ( self , target ) :
        pm.xform ( target , cp = True ) ;

    def clean ( self , target , cp = False ) :
        self.freezeTransform ( target ) ;
        self.deleteHistory ( target ) ;
        if cp :
            self.centerPivot ( target ) ;

class General ( CleanUp ) :

    def __init__ ( self ) :
        super ( General , self ).__init__() ;

    def composeName ( self , target ) :
        # return composed name
        target = pm.general.PyNode ( target ) ;
        targetName = target.nodeName() ;

        targetNameSplit = targetName.split ( '_' ) ;

        composedName =  targetNameSplit[0] ;

        for split in targetNameSplit[1:] :
            composedName += split[0].upper() + split[1:] ;

        return composedName ;

    def attachFolPoly ( self , target , name ) :

        targetShape = target.getShape() ;

        folShape = pm.createNode ( 'follicle' ) ;
        fol = folShape.getParent() ;
        fol.rename ( name ) ;

        folShape.simulationMethod.set ( 0 ) ;
        folShape.parameterU.set ( 0.5 ) ;
        folShape.parameterV.set ( 0.5 ) ;

        folShape.outTranslate >> fol.translate ;
        folShape.outRotate >> fol.rotate ;
        
        targetShape.worldMatrix >> folShape.inputWorldMatrix ;
        targetShape.outMesh >> folShape.inputMesh ;

        return ( fol ) ;

    def bakeKeys ( self , target , start , end , sample ) :

        pm.bakeResults ( target ,
            simulation  = False ,
            hierarchy = 'none' ,
            time = ( start , end ) ,
            sampleBy = sample ,
            disableImplicitControl = True ,
            preserveOutsideKeys = True ,
            sparseAnimCurveBake = False ,
            removeBakedAttributeFromLayer = False ,
            removeBakedAnimFromLayer = False ,
            bakeOnOverrideLayer = False ,
            minimizeRotation = True ,
            controlPoints = False ,
            shape = True ) ;

    def lockTransform ( self , target ) :
        target = pm.general.PyNode ( target ) ;
        for attr in [ 't' , 'r' , 's' ] :
            for axis in [ 'x' , 'y' , 'z' ] :
                exec ( 'target.%s%s.lock() ;' % ( attr , axis ) ) ;

    def lockHideAttr ( self , target , attr_list ) :
        for attr in attr_list :
            pm.setAttr ( target + '.' + attr , k = False , l = True ) ;