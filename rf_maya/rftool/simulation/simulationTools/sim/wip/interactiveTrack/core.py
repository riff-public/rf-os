import pymel.core as pm ;

import simulationTools.sim.interactiveTrack.curve as crv ;
reload ( crv ) ;

import simulationTools.sim.interactiveTrack.gui as gui ;
reload ( gui ) ;

class SetUp ( crv.Curve ) :

    def __init__ ( self ) :
        super ( SetUp , self ).__init__() ;

    def track ( self , *args ) :

        target = pm.ls ( sl = True ) [0] ;

        worldRotation = pm.checkBox ( self.worldRotation_checkBox , q = True , v = True ) ;

        sample  = pm.floatField ( self.sample_floatField , q = True , v = True ) ;
        
        start   = pm.playbackOptions ( q = True , min = True ) ;
        end     = pm.playbackOptions ( q = True , max = True ) ;



        if target :
 
            targetName = self.composeName ( target ) ;

            dummyTfm_Grp = pm.group ( em = True , n = targetName + 'DummyTfm_Grp' ) ;

            if worldRotation :
                parCon = pm.parentConstraint ( target , dummyTfm_Grp , mo = False , skipTranslate = 'none' ,
                    skipRotate = [ 'x' , 'y' , 'z' ] ) ;
            else :
                parCon = pm.parentConstraint ( target , dummyTfm_Grp , mo = False , skipTranslate = 'none' , skipRotate = 'none' ) ;

            #self.bakeKeys ( dummyTfm_Grp , start , end , sample ) ;
            pm.delete ( parCon ) ;

            animCurves = dummyTfm_Grp.listConnections ( type = 'animCurve' ) ;
            pm.filterCurve ( animCurves , f = 'euler' ) ;

            # # placement controller 
            placementCtrlZroGrp = pm.group ( em = True , w = True , n = targetName + 'TckPlacementCtrlZro_Grp' ) ;
            self.lockTransform ( placementCtrlZroGrp ) ;
            placementCtrl     = self.createCurve ( 'fourDirectionalArrow' , name = targetName + 'TckPlacement_Ctrl' , color = 'lightBlue' ) ;
            self.lockHideAttr ( placementCtrl , [ 'sx' , 'sy' , 'sz' , 'v' ] ) ;
            pm.parent ( placementCtrl , placementCtrlZroGrp ) ;

            backwardGrp = pm.group ( em = True , w = True , n = targetName + '_BwdGrp' ) ;
            forwardGrp  = pm.group ( em = True , w = True , n = targetName + '_FwdGrp' ) ;

            # # detail controller 
            # detailCtrl        = self.createCurve ( 'star' , name = targetName + 'TckDetail_Ctrl' , color = 'white' ) ;



    # pm.select ( CIN_nurb , CIN_object ) ;
    # mc.CreateWrap ( ) ;

    # wrap = set ( pm.listConnections ( CIN_nurb + 'Shape' , type = 'wrap' ) ) ;
    # wrap = list ( wrap ) ;
    # wrap = pm.general.PyNode ( wrap [0] ) ;
    # pm.rename ( wrap , CIN_object + '_wrap' ) ;

    # wrap.exclusiveBind.set ( 1 ) ;
    # wrap.falloffMode.set ( 0 ) ;

    # wrapBaseList = pm.listConnections ( wrap ) ;

    # for each in wrapBaseList :
    #     if 'Base' in str ( each ) :
    #         wrapBase = each ;

    # pm.parent ( wrapBase , w = True ) ;

             

            

class InteractiveTrack ( gui.Gui , SetUp ) :
    
    def __init__ ( self ) :
        super ( InteractiveTrack , self  ).__init__() ;

    def guiCall ( self , *args ) :
        self.showGui() ;

def run ( *args ) :
    it = InteractiveTrack() ;
    it.guiCall() ;

# import simulationTools.sim.interactiveTrack.core as interactiveTrack ;
# reload ( interactiveTrack ) ;
# interactiveTrack.run() ;