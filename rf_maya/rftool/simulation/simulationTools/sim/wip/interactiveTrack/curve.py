import pymel.core as pm ;

import simulationTools.sim.interactiveTrack.general as gen ;
reload ( gen ) ;

class Curve ( gen.General ) :

    def __init__ ( self ) :

        super ( Curve , self ).__init__() ;

        ### curve dict ###
        self.curve_dict = {} ;

        self.curve_dict['fourDirectionalArrow'] = [ ( -19.999955 , 0 , 9.999955 ) , ( -9.999955 , 0 , 19.999955 ) , ( -9.999955 , 0 , 39.999954 ) , ( -19.999955 , 0 , 39.999954 ) , ( 4.47035e-005 , 0 , 59.99995 ) , ( 20.000044 , 0 , 39.999954 ) , ( 10.000044 , 0 , 39.999954 ) , ( 10.000044 , 0 , 19.999955 ) , ( 20.000044 , 0 , 9.999955 ) , ( 40.000044 , 0 , 9.999955 ) , ( 40.000044 , 0 , 19.999955 ) , ( 60.00004 , 0 , -4.47035e-005 ) , ( 40.000044 , 0 , -20.000044 ) , ( 40.000044 , 0 , -10.000044 ) , ( 20.000044 , 0 , -10.000044 ) , ( 10.000044 , 0 , -20.000044 ) , ( 10.000044 , 0 , -40.000044 ) , ( 20.000044 , 0 , -40.000044 ) , ( 4.47035e-005 , 0 , -60.00004 ) , ( -19.999955 , 0 , -40.000044 ) , ( -9.999955 , 0 , -40.000044 ) , ( -9.999955 , 0 , -20.000044 ) , ( -19.999955 , 0 , -10.000044 ) , ( -39.999954 , 0 , -10.000044 ) , ( -39.999954 , 0 , -20.000044 ) , ( -59.99995 , 0 , -4.47035e-005 ) , ( -39.999954 , 0 , 19.999955 ) , ( -39.999954 , 0 , 9.999955 ) , ( -19.999955 , 0 , 9.999955 ) ] ;
        self.curve_dict['fly'] = [ ( 5.98151 , 0 , 7.68556 ) , ( 14.158347 , 19.425926 , -2.335602 ) , ( 14.158347 , 19.425926 , -29.0998 ) , ( 5.98151 , 0 , -7.685555 ) , ( -5.98151 , 0 , -7.685555 ) , ( -14.158347 , 19.425926 , -29.0998 ) , ( -14.158347 , 19.425926 , -2.335602 ) , ( -5.98151 , 0 , 7.68556 ) , ( 5.98151 , 0 , 7.68556 ) ] ;   
        self.curve_dict['star'] = [ ( -17.500004 , 0 , 0 ) , ( -12.593696 , 0 , 4.091938 ) , ( -14.157801 , 0 , 10.286244 ) , ( -7.783331 , 0 , 10.712839 ) , ( -5.407799 , 0 , 16.643492 ) , ( 3.66791e-007 , 0 , 13.241789 ) , ( 5.407797 , 0 , 16.64349 ) , ( 7.783331 , 0 , 10.712839 ) , ( 14.157798 , 0 , 10.286243 ) , ( 12.593694 , 0 , 4.091937 ) , ( 17.5 , 0 , 0 ) , ( 12.593702 , 0 , -4.091941 ) , ( 14.157807 , 0 , -10.286248 ) , ( 7.783335 , 0 , -10.712845 ) , ( 5.4078 , 0 , -16.643498 ) , ( 7.61427e-007 , 0 , -13.241795 ) , ( -5.4078 , 0 , -16.643497 ) , ( -7.783333 , 0 , -10.712843 ) , ( -14.157802 , 0 , -10.286245 ) , ( -12.593696 , 0 , -4.091941 ) , ( -17.500004 , 0 , 0 ) ] ;

        ### color dict ###
        self.color_dict = {} ;

        self.color_dict['none']     = [ False   , ( 0 , 0 , 0 ) ] ;
        self.color_dict['white']    = [ True    , ( 1 , 1 , 1 ) ] ;
        self.color_dict['red']      = [ True    , ( 1 , 0 , 0 ) ] ;
        self.color_dict['blue']     = [ True    , ( 0 , 0 , 1 ) ] ;
        self.color_dict['green']    = [ True    , ( 0 , 1 , 0 ) ] ;
        self.color_dict['yellow']   = [ True    , ( 1 , 1 , 0 ) ] ;
        self.color_dict['lightBlue'] = [ True   , ( 0 , 1 , 1 ) ] ;
        self.color_dict['brown']    = [ True , ( 0.3 , 0.1 , 0.1 ) ] ;

    def createCurve ( self , type , name , color ) :

        scale   = pm.floatField ( self.controllerScale_floatField , q = True , v = True ) ;

        curve = pm.curve ( d = 1 , p = self.curve_dict [ type ] ) ;
        curve.rename ( name ) ;
        
        self.setCurveColor ( curve , color ) ;
        curve.s.set ( scale , scale , scale ) ;
        self.clean ( curve ) ;
        return curve ;

    def setCurveColor ( self , target , color ) :

        info    = self.color_dict [ color ] ;
        en      = info[0] ;
        color   = info[1] ;
        
        target = pm.general.PyNode ( target ) ;
        targetShape = target.getShape() ;
        targetShape.overrideEnabled.set ( )

        targetShape.overrideEnabled.set ( en ) ;
        targetShape.overrideRGBColors.set ( en ) ;
        targetShape.overrideColorRGB.set ( color ) ;