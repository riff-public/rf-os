import pymel.core as pm ;
import os ;

import simulationTools.sim.setupUtil.curve as crv ;
reload ( crv ) ;

class CleanUp ( object ) :

    def __init__ ( self ) :
        super ( CleanUp, self ).__init__() ;

    def freezeTransform ( self , target ) :
        
        target = pm.general.PyNode ( target ) ;

        oldSettings_dict = {} ;
        attr_list = [] ;

        for attr in [ 't'  , 'r' , 's' ] :
            for axis in [ 'x' , 'y' , 'z' ] :
                attr_list.append ( attr + axis ) ;
        attr_list.append ( 'v' ) ;

        for attr in attr_list :
            for attrAttr in [ 'channelBox' , 'keyable' , 'lock' ] :
                exec ( "oldSettings_dict [ '%s_{attrAttr}' % attr ] = target.getAttr ( attr , {attrAttr} = True )".format ( attrAttr = attrAttr ) ) ;
            
            exec ( "target.{attr}.set( channelBox = True , keyable = True , lock = False )".format ( attr = attr ) ) ;

        pm.makeIdentity ( target , apply = True ) ;

        for attr in attr_list :
            cmd = "target.%s.set (" % attr ;
            cmd += "channelBox = oldSettings_dict ['%s_channelBox'] ," % attr ;
            cmd += "keyable = oldSettings_dict ['%s_keyable'] ," % attr ;
            cmd += "lock = oldSettings_dict ['%s_lock'] ) ;" % attr ;
            exec (  cmd ) ;

    def deleteHistory ( self , target ) :
        pm.delete ( target , ch = True ) ;

    def centerPivot ( self , target ) :
        pm.xform ( target , cp = True ) ;

    def clean ( self , target ) :
        self.freezeTransform ( target ) ;
        self.deleteHistory ( target ) ;
        self.centerPivot ( target ) ;

    def createLockedGroup ( self , name ) :
        # return group 
        group = pm.group ( em = True , w = True , name = name ) ;
        for attr in [ 't' , 'r' , 's' ] :
            exec ( 'group.%s.lock()' % attr ) ;
        return group ;

    def attachFollicleNurb ( self , name , surface ) :
        # return fol

        surface = pm.general.PyNode ( surface ) ;

        folShape    = pm.createNode ( 'follicle' ) ;
        fol         = folShape.getParent() ;

        fol.rename ( name ) ;

        folShape.simulationMethod.set(0) ;
        folShape.outTranslate   >> fol.t ;
        folShape.outRotate      >> fol.r ;

        surface.worldMatrix[0]  >> folShape.inputWorldMatrix ;
        surface.local           >> folShape.inputSurface ;

        folShape.parameterU.set ( 0.5 ) ;
        folShape.parameterV.set ( 0.5 ) ;

        return fol ;

class General ( CleanUp , crv.Curve ) :
    
    def __init__ ( self ) :
        super ( General, self ).__init__() ;
        #crv.Curve.__init__( self ) ;

    def setOutlinerColor ( self , target , color ) :

        colorDict = { } ;
        colorDict [ 'green' ]       = ( 0.5 , 1 , 0 ) ;
        colorDict [ 'yellow' ]      = ( 1 , 1 , 0 ) ;
        colorDict [ 'red' ]         = ( 1 , 0 , 0 ) ;
        colorDict [ 'pink' ]        = ( 1 , 0.4 , 0.7 ) ;
        colorDict [ 'purple' ]      = ( 1 , 0 , 1 ) ;
        colorDict [ 'lightBlue' ]   = ( 0 , 1 , 1 ) ;
        colorDict [ 'paleGreen' ]   = ( 0.55 , 0.95 , 0.55 ) ;
        colorDict [ 'paleBlue' ]    = ( 0.50 , 0.8 , 1 ) ;
        colorDict [ 'white' ]       = ( 1 , 1 , 1 ) ;
        colorDict [ 'paleRed' ]     = ( 0.8 , 0.35 , 0.35 ) ;
        colorDict [ 'palePink' ]    = ( 0.95 , 0.5 , 0.5 ) ;
        colorDict [ 'paleYellow' ]  = ( 1 , 0.85 , 0.65 ) ;
        colorDict [ 'palePurple' ]  = ( 0.85 , 0.60 , 0.85 ) ;

        target = pm.general.PyNode ( target ) ;
        target.useOutlinerColor.set ( 1 ) ;        
        target.outlinerColor.set ( colorDict [ color ] ) ;    
    
    def duplicateHierarchy ( self , target , suffix ) :
        
        target = pm.general.PyNode ( target ) ;
        dupTarget = pm.duplicate ( target ) [0] ;
        
        ori_list = [ target] ;
        dup_list = [ dupTarget ] ;
        
        ori_list.extend ( target.listRelatives ( ad = True , type = 'transform' ) ) ;
        dup_list.extend ( dupTarget.listRelatives ( ad = True , type = 'transform' ) ) ;
    
        for ori , dup in zip ( ori_list , dup_list ) :
            dup.rename ( ori.nodeName() + suffix ) ;
        
        return dupTarget ;

    def getTopNode ( self , target ) :
   
        target = pm.general.PyNode ( target ) ;
        parent = target.getParent() ;
        
        if parent :
            return self.getTopNode ( parent ) ;
        else :
            return target ;

    def checkIncrement ( self , name , path , incrementIncrement = False ) :
        # check for '.xxxx.' e.g. '.0001.'

        file_list = os.listdir ( path ) ;
        increment_list = [] ;

        if not file_list :
            finalIncrement = '0001' ;

        else :
            for file in file_list :
                if name in str ( file ) :
                    increment = re.findall ( '.' + '[0-9]{4}' + '.' , file ) ;
                    if increment :
                        increment = increment[0] ;
                        increment_list.append ( increment ) ;

            if not increment_list :
                finalIncrement = '0001' ;
            else :
                
                increment_list.sort() ;
                currentIncrement = increment_list[-1] ;
                increment = currentIncrement.split('.')
                for split in increment :
                    if split == '' :
                        increment.remove ( split ) ;
                increment = increment[0] ;
                increment = int ( increment ) ;

                if incrementIncrement :
                    increment += increment ;

                finalIncrement = str(increment).zfill(4) ;

        return finalIncrement ;