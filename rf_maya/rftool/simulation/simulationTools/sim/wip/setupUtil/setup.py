import pymel.core as pm ;
import maya.cmds as mc ;
import re , ast ;

import simulationTools.sim.setupUtil.general as general ;
reload ( general ) ;

class Setup ( general.General ) :
    
    def __init__ ( self ) :
        super ( Setup , self ).__init__() ;
        self.currentWorkSpacePath = pm.workspace ( q = True , rootDirectory = True ) ;
        self.dataPath   = self.currentWorkSpacePath + 'data/dynSetupData/' ;
        self.sceneName  = pm.system.sceneName().split('/')[-1] ;

    def getEdgesBtn_cmd ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;
        edges_list = [] ;

        for selection in selection_list :
            edges =  selection.split ( '.' ) [-1] ;
            edgesSplit = re.split ( '([0-9]+)' , edges ) ;
            for split in edgesSplit :
                if split.isdigit() :
                    edges_list.append ( split ) ;

        # set edges textField
        for textField , edge in zip ( [ self.edge1_textField , self.edge2_textField ] , edges_list ) :
            pm.textField ( textField , e = True , text = edge ) ;

        objectShape = pm.general.PyNode ( selection_list[0] ) ;
        object = objectShape.getParent() ;
        objectTopNode = self.getTopNode ( object ) ;

        # set object textField
        pm.textField ( self.object_textField , e = True , text = object ) ;
        pm.textField ( self.objectTopNode_textField , e = True , text = objectTopNode ) ;

    def saveBtn_cmd ( self , *args ) :
        
        edge1       = pm.textField ( self.edge1_textField , q = True , text = True ) ;
        edge2       = pm.textField ( self.edge2_textField , q = True , text = True ) ;
        object      = pm.textField ( self.object_textField , q = True , text = True ) ;
        objectTopNode = pm.textField ( self.objectTopNode_textField , q = True , text = True ) ;

        info_dict = {} ;

        info_dict [ 'edge1' ]       = edge1 ;
        info_dict [ 'edge2' ]       = edge2 ;
        info_dict [ 'object' ]      = object ;
        info_dict [ 'objectTopNode' ] = objectTopNode ;

        textFileName = self.sceneName ;
        textFileName += '.SetupData.' ;
        increment = self.checkIncrement ( name = textFileName , path = self.dataPath ) ;
        textFileName += increment ;
        textFileName += '.txt' ;

        saveTxtFile = open ( self.dataPath + textFileName , 'w+' ) ;
        saveTxtFile.write ( str ( info_dict ) ) ;
        saveTxtFile.close ( ) ;

    def loadBtn_cmd ( self , *args ) :

        loadTxtPath = pm.fileDialog2 ( dir = self.dataPath , fileMode = 4 ) [0] ;
        loadTxtFile = open ( loadTxtPath , 'r'  ) ;
        loadTxt = loadTxtFile.read ( ) ;
        loadTxtFile.close() ;

        info_dict = ast.literal_eval ( loadTxt ) ;

        pm.textField ( self.edge1_textField , e = True , text = info_dict [ 'edge1' ] ) ;
        pm.textField ( self.edge2_textField , e = True , text = info_dict [ 'edge2' ] ) ;
        pm.textField ( self.object_textField , e = True , text = info_dict [ 'object' ] ) ;
        pm.textField ( self.objectTopNode_textField , e = True , text = info_dict [ 'objectTopNode' ] ) ;
    
    def baseSetUpBtn_cmd ( self , *args ) :

        object = pm.textField ( self.object_textField , q = True , text = True ) ;
        object = pm.general.PyNode ( object ) ;

        objectShape = object.getShape() ;

        edge1 = pm.textField ( self.edge1_textField , q = True , text = True ) ;
        edge1 = objectShape + '.e[' + edge1 + ']' ;
        edge2 = pm.textField ( self.edge2_textField , q = True , text = True ) ;
        edge2 = objectShape + '.e[' + edge2 + ']' ;

        objectTopNode = pm.textField ( self.objectTopNode_textField , q = True , text = True ) ;
        objectTopNode = pm.general.PyNode ( objectTopNode ) ;

        cinTopNode  = self.duplicateHierarchy ( objectTopNode , '_Cin' ) ;
        cinObject   = pm.general.PyNode ( object.nodeName() + '_Cin' ) ;
        cinObjectShape = cinObject.getShape() ;

        colTopNode = self.duplicateHierarchy ( objectTopNode , '_Col' ) ;
        colObject  = pm.general.PyNode ( object.nodeName() + '_Col' ) ;
        colObjectShape = colObject.getShape() ;

        ### create Grp ###
        noTouchGrp = self.createLockedGroup ( name = 'NoTouch_Grp' ) ;
        noTouchGrp.v.set(0) ;
        self.setOutlinerColor ( target = noTouchGrp , color = 'red' ) ;

        wrapGrp = self.createLockedGroup ( name = 'Wrap_Grp' ) ;
        pm.parent ( wrapGrp , noTouchGrp ) ;

        ### create nurb surface ###
        curve1 = pm.polyToCurve ( edge1 , form = 0 , degree = 1 , n = object + '_Crv1' ) ;
        curve2 = pm.polyToCurve ( edge2 , form = 0 , degree = 1 , n = object + '_Crv2' ) ;

        nurb = pm.loft ( curve1 , curve2 , ch = 0 , ar = True , d = 1 , ss = 1 , rn = 0 , po = 0 , rsn = True ) [0] ;
        nurbShape = nurb.getShape() ;
        nurb.rename ( object + '_Nrb' ) ;
        self.clean ( nurb ) ;
        
        pm.delete ( curve1 , curve2 ) ;

        ### create follicle , attach to nurb ###
        fol = self.attachFollicleNurb ( name = object + '_Fol' , surface = nurb ) ;

        ### wrap ###
        pm.select ( nurb , cinObject ) ;
        mc.CreateWrap ( ) ;

        wrap = nurbShape.listConnections ( type = 'wrap' ) ;
        wrap = list ( set ( wrap ) ) [0] ;
        wrap.rename ( object + '_WrapNode' ) ;
        
        wrap.exclusiveBind.set ( 1 ) ;
        wrap.falloffMode.set ( 0 ) ;

        wrapBase_list = wrap.listConnections ( type = 'mesh' ) ;
        for base in wrapBase_list :
            if 'Base' in str ( base ) :
                wrapBase = base ;

        folPos  = pm.group ( em = True , n = object + 'Fol_PosGrp' ) ;
        pos     = pm.xform ( fol , q = True , ws = True , rp = True ) ;
        pm.xform ( folPos , t = pos ) ;
        pm.parent ( folPos , fol ) ;

        wrapElem = [ nurb , fol , wrapBase ] ;
        pm.parent ( wrapElem , wrapGrp ) ;

        for item in [ noTouchGrp , wrapGrp , fol , folPos , nurb  ] : 
            self.transformLock ( item ) ;

        ### create controller ###
        placementCtrl       = self.createCurve ( 'fourDirectionalArrow' , 'Placement_Ctrl' ) ;
        placementCtrlShape  = placementCtrl.getShape() ;
        self.clean ( placementCtrl ) ;
        self.setCurveColor ( placementCtrl , 'yellow' ) ;

        placementCtrlZroGrp = pm.group ( em = True , w = True , n = 'PlacementCtrlZro_Grp' ) ;
        pm.parent ( placementCtrl , placementCtrlZroGrp ) ;

        flyCtrl         = self.createCurve ( 'fly' , 'Fly_Ctrl' ) ;
        flyCtrlShape    = flyCtrl.getShape() ;
        self.clean ( flyCtrl ) ;
        self.setCurveColor ( flyCtrl , 'white' ) ;

        flyCtrlZroGrp = pm.group ( em = True , w = True , n = 'FlyCtrlZro_Grp' ) ;
        pm.parent ( flyCtrl , flyCtrlZroGrp ) ;
        pm.parent ( flyCtrlZroGrp , placementCtrl ) ;

        pos = pm.xform ( fol , q = True , rp = True , ws = True ) ;
        pm.xform ( flyCtrlZroGrp , t = pos ) ;
        
        placementCtrlShape.addAttr ( 'flyControl' , keyable = True , attributeType = 'bool' , dv = True ) ;
        placementCtrlShape.flyControl >> flyCtrlShape.visibility ;

        placementCtrlShape.addAttr ( 'positionAmp' , keyable = True , attributeType = 'float' , dv = 1 , max = 1 , min = 0 ) ;

        ### connect folPos >> amp >> placementCtrlZroGrp

        positionTransAmp    = pm.createNode ( 'multiplyDivide' , n = 'PositionTransAmp_Mdv' ) ;
        positionRotAmp      = pm.createNode ( 'multiplyDivide' , n = 'PositionRotAmp_Mdv' ) ;

        for axis in [ 'x' , 'y' , 'z' ] :
            exec ( "placementCtrlShape.positionAmp >> positionTransAmp.i2%s" % axis ) ;
            exec ( "placementCtrlShape.positionAmp >> positionRotAmp.i2%s" % axis ) ;

        # parent node constraint to amp
        master = folPos ;
        slave = placementCtrlZroGrp
        
        mulMatrix       = pm.createNode ( 'multMatrix'      , n = 'PositionAmp_MulMatrix' ) ;
        decomposeMatrix = pm.createNode ( 'decomposeMatrix' , n = 'PositionAmp_DecMatrix' )
        
        # calculate offset
        offsetMulMatrix = pm.createNode ( 'multMatrix' , n = 'PositionAmpOffset_MulMatrix' ) ;
        placementCtrlZroGrp.worldMatrix >> offsetMulMatrix.matrixIn[0] ;
        folPos.worldInverseMatrix       >> offsetMulMatrix.matrixIn[1] ;
        mulMatrix.matrixIn[0].set ( offsetMulMatrix.matrixSum.get() ) ;
        pm.delete ( offsetMulMatrix ) ;
             
        # connect to Amp
        folPos.worldMatrix                      >> mulMatrix.matrixIn[1] ;
        placementCtrlZroGrp.parentInverseMatrix >> mulMatrix.matrixIn[2] ;

        mulMatrix.matrixSum >> decomposeMatrix.inputMatrix ;

        decomposeMatrix.outputTranslate >> positionTransAmp.i1 ;
        decomposeMatrix.outputRotate    >> positionRotAmp.i1 ;

        positionTransAmp.o  >> placementCtrlZroGrp.t ;
        positionRotAmp.o    >> placementCtrlZroGrp.r ;

        for item in [ placementCtrlZroGrp , flyCtrlZroGrp ] : 
            self.transformLock ( item ) ;

        ### create tfm grp ###
        tfmGrp = pm.group ( em = True , w = True , n = object + '_Tfm' ) ;
        pm.parentConstraint ( flyCtrl , tfmGrp , mo = True , skipTranslate = 'none' , skipRotate = 'none' ) ;
        pm.parent ( colTopNode , tfmGrp ) ;

        self.worldBlendShape ( cinTopNode , colTopNode , 'CIN_COL_BSH' ) ;