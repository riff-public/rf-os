import pymel.core as pm ;
import os , re ;

class Workspace ( object ) :
    
    def __init__ ( self ) :
        self.workspace = pm.workspace ( q = True , rootDirectory = True ) ;
        if self.workspace[-1] == '/' :
            self.workspace = self.workspace[0:-1] ;

    def __str__ ( self ) :
        return self.workspace ;

    def __repr__ ( self ) :
        return self.workspace ;

    def getProject ( self ) :
        project = self.workspace.split('/')[-1] ;
        return project ;
    
    def getProductCachePath ( self ) :
        project     = self.getProject ( ) ;
        cachePath   = self.workspace + '/product/' + project + '/' ;
        return cachePath ;

    def getWorkspaceCachePath ( self ) :
        dynCachePath = self.workspace + '/cache/dyn/'
        return dynCachePath ;

class Path ( object ) :

    def __init__ ( self , path ) :
        
        self.path = path ;

        if os.path.exists ( self.path ) == False :
            os.makedirs ( self.path ) ;
        else : pass ;

    def __str__ ( self ) :
        return self.path ;

    def __repr__ ( self ) :
        return self.path ;

    # return 'vXXX_XXX'
    def checkVersion ( self , name , incrementVersion = False , incrementSubVersion = False ) :

        allFile = os.listdir ( self.path ) ;
        version_list = [ ] ;

        for each in allFile :
            if name in str ( each ) :
                version = re.findall ( 'v' + '[0-9]{3}' + '_' + '[0-9]{3}' , each ) ;
                try : 
                    version = version[0] ;
                    version_list.append ( version ) ;
                except :
                    pass ;

        version_list.sort ( ) ;

        if len ( version_list ) == 0 :
            currentVersion = 'v001_001' ;
        else :
            currentVersion = version_list[-1] ;

            version = currentVersion.split('_')[0] ;
            version = version.split('v')[-1] ;
            version = int ( version ) ;

            subVersion = currentVersion.split('_')[-1] ;
            subVersion = int ( subVersion ) ;

            if incrementVersion == True :
                version += 1 ;                

            if incrementSubVersion == True :
                subVersion += 1 ;

            version     = str(version) ;
            subVersion  = str(subVersion) ;

            currentVersion = 'v' + version.zfill(3) + '_' + subVersion.zfill(3) ;

        return currentVersion ;

class GenNode ( object ) :

    def __init__ ( self , node ) :
        self.node = pm.general.PyNode ( node ) ;

    def __str__ ( self ) :

        if self.node.isUniquelyNamed() == False :
            return self.node.split('|')[-1] ;
        else :
            return str ( self.node ) ;

    def __repr__ ( self ) :

        if self.node.isUniquelyNamed() == False :
            return self.node.split('|')[-1] ;
        else :
            return str ( self.node ) ;

    def makeName ( self ) :
        
        if self.node.isUniquelyNamed() == False :
            inputName = self.node.split('|')[-1] ;
        else :
            inputName = self.node ;

        nameElem = inputName.split ( '_' ) ;

        name = '' ;

        for each in nameElem :
            name += each[0].upper() + each[1:] ;
        
        return name ;

    def getLongName ( self ) :

        return self.node.longName ( ) ;

def exportAlembicCache ( cacheType = 'product' , *args ) :
    # workspace , product

    selection = pm.ls ( sl = True ) [0]
    selection = GenNode ( selection  ) ;

    current_workspace   = Workspace ( ) ;
    projectName         = current_workspace.getProject ( ) ;

    if cacheType == 'workspace' :
        cachePath = Path ( current_workspace.getWorkspaceCachePath ( ) ) ;

    if cacheType == 'product' :        
        cachePath = Path ( current_workspace.getProductCachePath ( ) ) ;

    # v001_001.projectName.makeName.abc
    cacheName = projectName + '.' + selection.makeName ( ) + '.' ;
    cacheName += cachePath.checkVersion ( cacheName , incrementSubVersion = True )  + '.abc' ;

    preRoll     = 71 ;
    postRoll    = 275 ;
    step        = 1 ;

    ### export abc command ###
    command = 'AbcExport -j ' ; # start command
    command += '"-frameRange %s %s '    % ( str(preRoll) , str(postRoll) ) ;
    command += '-step %s '              % str(step) ;
    #command += '-attr __forSim__ -attr preRoll -attr start -attr end -attr postRoll -attr placementSclX -attr placementSclY -attr placementSclZ -attr headSclX -attr headSclY -attr headSclZ -attr gobalScale' + ' ' ;
    command += '-worldSpace -writeVisibility -eulerFilter -dataFormat ogawa' + ' ' ;
    command += '-root %s '              % selection.getLongName ( ) ;
    command += '-file %s " ;'           % ( str(cachePath) + str(cacheName) ) ;

    pm.mel.eval ( command ) ;

    '''
    productCache_path = Path ( current_workspace.getProductCachePath ( ) ) ;
    print productCache_path.checkVersion ( name = 'lol' ) ;
    '''

#project                = current_workspace.getProject ;
#productCachePath       = current_workspace.getProductCachePath ;

exportAlembicCache ( ) ;