import pymel.core as pm ;

class Gui ( object ) :

    def __init__ ( self ) :
        self.ui         = 'simUtil_ui' ;
        self.width      = 400.00 ;
        self.title      = 'Simulation Utilities' ;
        self.version    = 3.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def showGui ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                ### Nucleus Section ###
                with pm.rowColumnLayout ( nc = 1 , w = w ) :                
                    self.nucleus_txtScrll = pm.textScrollList ( 'nucleus_txtScrll' , w = width , h = 60 , ams = True , sc = self.updateNucleusGUI ) ;
                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        self.enableNucleus_btn  = pm.button ( 'enableNucleus_btn' , label = 'Enable Nucleus' , c = self.enableNucleus_cmd , w = width/2.0 ) ;
                        self.disableNucleus_btn = pm.button ( 'disableNucleus_btn' , label = 'Disable Nucleus' , c = self.disableNucleus_cmd , w = width/2.0 ) ;
        
        self.updateNucleusGUI() ;
        
        window.show () ;

# gui = Gui() ;
# gui.showGui() ;