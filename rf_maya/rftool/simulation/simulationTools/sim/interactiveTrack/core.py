import pymel.core as pm ;

import simulationTools.sim.interactiveTrack.curve as crv ;
reload ( crv ) ;

import simulationTools.sim.interactiveTrack.gui as gui ;
reload ( gui ) ;

import simulationTools.sim.interactiveTrack.general as gen ;
reload ( gen ) ;

class SetUp ( crv.Curve , gen.General ) :

	def __init__ ( self ) :
		super ( SetUp , self ).__init__() ;

	def track ( self , *args ) :

		target = pm.ls ( sl = True ) [0] ;

		if target :
 
			targetName = self.composeName ( target ) ;

			placementCtrl = crv.Curve() ;
			placementCtrl.createCurve ( 'fourDirectionalArrow' , name = targetName + 'TckPlacement_Ctrl' ) ;
			placementCtrl.setCurveColor ( 'lightBlue' ) ;

class InteractiveTrack ( gui.Gui , SetUp ) :
	
	def __init__ ( self ) :
		super ( InteractiveTrack , self  ).__init__() ;

	def guiCall ( self , *args ) :
		self.showGui() ;

def run ( *args ) :
	it = InteractiveTrack() ;
	it.guiCall() ;

# import simulationTools.sim.interactiveTrack.core as interactiveTrack ;
# reload ( interactiveTrack ) ;
# interactiveTrack.run() ;