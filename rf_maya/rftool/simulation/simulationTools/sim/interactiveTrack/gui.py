import pymel.core as pm ;

class Gui ( object ) :

    def __init__ ( self ) :
        self.ui         = 'interactiveTrack_ui' ;
        self.width      = 250.00 ;
        self.title      = 'Int Track' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def showGui ( self ) :
        
        w = self.width ;
        
        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :
                
                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :                
                    pm.text ( label = 'Sample By' ) ;
                    pm.floatField ( v = 1 , pre = 2 ) ;
                
                pm.button ( label = 'Track' , c = self.track ) ;

        window.show () ;