import pymel.core as pm ;

class CleanUp ( object ) :

    def __init__ ( self ) :
        super ( CleanUp, self ).__init__() ;

    def freezeTransform ( self , target ) :
        
        target = pm.general.PyNode ( target ) ;

        oldSettings_dict = {} ;
        attr_list = [] ;

        for attr in [ 't'  , 'r' , 's' ] :
            for axis in [ 'x' , 'y' , 'z' ] :
                attr_list.append ( attr + axis ) ;
        attr_list.append ( 'v' ) ;

        for attr in attr_list :
            for attrAttr in [ 'channelBox' , 'keyable' , 'lock' ] :
                exec ( "oldSettings_dict [ '%s_{attrAttr}' % attr ] = target.getAttr ( attr , {attrAttr} = True )".format ( attrAttr = attrAttr ) ) ;
            
            exec ( "target.{attr}.set( channelBox = True , keyable = True , lock = False )".format ( attr = attr ) ) ;

        pm.makeIdentity ( target , apply = True ) ;

        for attr in attr_list :
            cmd = "target.%s.set (" % attr ;
            cmd += "channelBox = oldSettings_dict ['%s_channelBox'] ," % attr ;
            cmd += "keyable = oldSettings_dict ['%s_keyable'] ," % attr ;
            cmd += "lock = oldSettings_dict ['%s_lock'] ) ;" % attr ;
            exec (  cmd ) ;

    def deleteHistory ( self , target ) :
        pm.delete ( target , ch = True ) ;

    def centerPivot ( self , target ) :
        pm.xform ( target , cp = True ) ;

    def clean ( self , target , cp = False ) :
        self.freezeTransform ( target ) ;
        self.deleteHistory ( target ) ;
        if cp :
            self.centerPivot ( target ) ;

class General ( CleanUp ) :

    def __init__ ( self ) :
        super ( General , self ).__init__() ;

    def composeName ( self , target ) :
        # return composed name
        target = pm.general.PyNode ( target ) ;
        targetName = target.nodeName() ;

        targetNameSplit = targetName.split ( '_' ) ;

        composedName =  targetNameSplit[0] ;

        for split in targetNameSplit[1:] :
            composedName += split[0].upper() + split[1:] ;

        return composedName ;