import pymel.core as pm ;
import os ;

import simulationTools.sim.setupUtil.general as general ;
reload ( general ) ;

import simulationTools.sim.setupUtil.setup as setup ;
reload ( setup ) ;
    
class Gui ( setup.Setup ) :

    def __init__ ( self ) :
        super ( Gui , self ).__init__() ;
        self.ui         = 'simSetupUtil_ui' ;
        self.width      = 300.00 ;
        self.title      = 'Sim Setup Util' ;
        self.version    = '2.0.0' ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def show ( self ) :
            
        title = self.title ;
        version = self.version ;
        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = title , version = version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :                    
                    
                    with pm.rowColumnLayout ( nc = 1 , w = w/2 ) :
                        pm.text ( label = 'Edge1' , w = w/2 ) ;
                        self.edge1_textField = pm.textField ( w = w/2 ) ;
                    
                    with pm.rowColumnLayout ( nc = 1 , w = w/2 ) :
                        pm.text ( label = 'Edge2' , w = w/2 ) ;
                        self.edge2_textField = pm.textField ( w = w/2 ) ;
                    
                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :   

                    with pm.rowColumnLayout ( nc = 1 , w = w/2 ) :
                        pm.text ( label = 'Object' , w = w/2 ) ;
                        self.object_textField = pm.textField ( w = w/2 ) ;

                    with pm.rowColumnLayout ( nc = 1 , w = w/2 ) :
                        pm.text ( label = 'Object Top Node' ) ;
                        self.objectTopNode_textField = pm.textField ( w = w/2 ) ;

                pm.separator ( h = 10 , vis = False ) ;

                self.getEdges_btn = pm.button ( label = 'Get Edges' , w = w , c = self.getEdgesBtn_cmd) ;
                pm.button ( self.getEdges_btn , e = True , bgc = ( 1 , 0.85 , 0 ) ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :
                    pm.button ( label = 'Save' , c = self.saveBtn_cmd ) ;
                    pm.button ( label = 'Load' , c = self.loadBtn_cmd ) ;
                
                pm.separator ( h = 10 , vis = False ) ;

                pm.button ( label = 'Base Setup' , w = w , c = self.baseSetUpBtn_cmd ) ;
                    
        window.show () ;

def run ( *args ) :
    window = Gui() ;
    window.show() ;