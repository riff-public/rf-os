import simulationTools.sim.setupUtil.gui as gui ;
reload ( gui ) ;

class SimulationUtil ( gui.GUI ) :

    def __init__ ( self ) :
        super ( gui.GUI , self ).__init__() ;
        # gui.GUI.__init__ ( self ) ;
        pm.button ( self.getEdges_btn , e = True , c = self.getEdgesBtn_cmd ) ;

def run ( *args ) :
    window = SimulationUtil() ;
    window.show() ;
