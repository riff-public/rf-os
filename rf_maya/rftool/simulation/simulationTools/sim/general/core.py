class General ( object ) :

    def __init__ ( self ) :
        super ( General , self ).__init__ ( ) ;

    def attachFollicles ( self , *args ) :
        import simulationTools.sim.general.attachFollicles.core as gen_attachFollicles  ;
        reload ( gen_attachFollicles ) ;
        gen_attachFollicles.run() ;

    def attachTfmToCurve ( self , *args ) :
    	import simulationTools.sim.general.attachTfmToCurve.core as gen_attachTfmToCurve  ;
    	reload ( gen_attachTfmToCurve ) ;
    	gen_attachTfmToCurve.run() ;

    # def attachToCurve ( self , *args ) :
    # 	import simulationTools.sim.general.attachToCurve.core as gen_attachToCurve ;
    # 	reload ( gen_attachToCurve ) ;
    # 	gen_attachToCurve.run() ;

    def customSelect ( self , *args ) :
        import simulationTools.sim.general.customSelect.core as gen_customSelect ;
        reload ( gen_customSelect ) ;
        gen_customSelect.run() ;

    def nucleusRig_attach ( *args ) :
        import simulationTools.sim.general.nucleusRig.core as gen_nucleusRig ;
        reload ( gen_nucleusRig ) ;
        gen_nucleusRig.attach ( ) ;

    def nucleusRig_detach ( *args ) :
        import simulationTools.sim.general.nucleusRig.core as gen_nucleusRig ;
        reload ( gen_nucleusRig ) ;
        gen_nucleusRig.detach ( ) ;