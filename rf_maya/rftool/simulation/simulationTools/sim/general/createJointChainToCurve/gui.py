import pymel.core as pm ;

class CurveFunc ( object ) :

    def __init__ ( self ) :
        super ( CurveFunc , self ).__init__() ;

    def curveGet_cmd ( self , *args ) :
        
        selection_list = pm.ls ( sl = True ) ;

        if not selection_list :
            pass ;

        else :
            if len ( selection_list ) != 1 :
                print ( '<<!!>> More than 1 curves are being selected, only the first selected curve will be used' ) ;

            selection = selection_list[0] ;
            selectionShape = selection.getShape() ;

            # Just in case it is a joint or something
            try :
                if selectionShape.nodeType() != 'nurbsCurve' :
                    print ( '<<!!>> Your selected object is not a curve, thus it will not be appended to the field' ) ;

                else :
                    pm.textScrollList ( self.curve_textScrollList , e = True , ra = True ) ;
                    pm.textScrollList ( self.curve_textScrollList , e = True , append = selection ) ;

            except :
                print ( '<<!!>> Your selected object is not a curve, thus it will not be appended to the field' ) ;

    def curveClear_cmd ( self , *args ) :
        pm.textScrollList ( self.curve_textScrollList , e = True , ra = True ) ;

class TfmFunc ( object ) :

    def __init__ ( self ) :
        super ( TfmFunc , self ).__init__() ;

    def tfmAppend_cmd ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;
        selection_list = self.naturalSort ( selection_list ) ;

        if not selection_list :
            pass ;

        else :
            for selection in selection_list :

                allItem_list = pm.textScrollList ( self.tfm_textScrollList , q = True , allItems = True ) ;

                if selection in allItem_list :
                    print ( '<!!> %s already in the textScrolList, thus it will not be appended as duplicate' % selection ) ;

                else :
                    pm.textScrollList ( self.tfm_textScrollList , e = True , append = selection ) ;

    def tfmRemove_cmd ( self , *args ) :

        removeItem_list = pm.textScrollList ( self.tfm_textScrollList , q = True , selectItem = True ) ;

        for item in removeItem_list :
            pm.textScrollList ( self.tfm_textScrollList , e = True , removeItem = item ) ;

    def tfmClear_cmd ( self , *args ) :
        pm.textScrollList ( self.tfm_textScrollList , e = True , ra = True ) ;

    def tfmSort_cmd ( self , *args ) :

        allItem_list = pm.textScrollList ( self.tfm_textScrollList , q = True , allItems = True ) ;
        allItem_list = self.naturalSort ( allItem_list ) ;

        pm.textScrollList ( self.tfm_textScrollList , e = True , ra = True ) ;

        for item in allItem_list :
            pm.textScrollList ( self.tfm_textScrollList , e = True , append = item ) ;

class AimZFunc ( object ) :

    def __init__ ( self ) :
        super ( AimZFunc , self ).__init__() ;

    def aimZCreate_cmd ( self , *args ) :
        loc = pm.spaceLocator ( n = 'AimZ_Loc' ) ;
        pm.textScrollList ( self.aim_textScrollList , e = True , ra = True ) ;
        pm.textScrollList ( self.aim_textScrollList , e = True , append = loc ) ;
        pm.optionMenu ( self.secondaryAimDir_otm , e = True , select = 3 ) ;

    def aimZGet_cmd ( self , *args ) :
        selection_list = pm.ls ( sl = True ) ;
        
        if not selection_list :
            pass ; 
        else :
            if len ( selection_list ) != 1 :
                print ( '<<!!>> More than 1 transform are being selected, only the first selected transform will be used' ) ;
            
            selection = selection_list[0] ;

            pm.textScrollList ( self.aim_textScrollList , e = True , ra = True ) ;
            pm.textScrollList ( self.aim_textScrollList , e = True , append = selection ) ;

        pm.optionMenu ( self.secondaryAimDir_otm , e = True , select = 3 ) ;

    def aimZClear_cmd ( self , *args ) :
        pm.textScrollList ( self.aim_textScrollList , e = True , ra = True ) ;
        pm.optionMenu ( self.secondaryAimDir_otm , e = True , select = 1 ) ;

class CreateJointChain ( object ) :

    def __init__ ( self ) :

        super ( CreateJointChain , self ).__init__() ;

    def jointChainCreate_cmd ( self , *args ) :

        ###########################
        ''' Gather All Gui Info '''
        ###########################

        ### Curve ###
        curve = pm.textScrollList ( self.curve_textScrollList , q = True , allItems = True ) ;
        if not curve :
            pm.error ( '<!!> No curve - please select a curve and append it to the field to continue' ) ;
        curve = pm.PyNode ( curve[0] ) ;

        ### Transform List ###
        transform_list = pm.textScrollList ( self.tfm_textScrollList , q = True , allItems = True ) ;
        if not transform_list :
            pm.error ( '<!!> No transform(s) - please select transform(s) and append them to the field to continue' ) ;

        ### AimZ ###

        aimZ = pm.textScrollList ( self.aim_textScrollList , q = True , allItems = True ) ;
        if not aimZ :
            aimZ = None ;
        else :
            aimZ = pm.PyNode ( aimZ[0] ) ;

        ### AimDir ###
        aimDir = pm.optionMenu ( self.aimDir_otm , q = True , v = True ) ;

        ### AimZ type ###

        aimZType = pm.optionMenu ( self.secondaryAimDir_otm , q = True , v = True ) ;

        ### Segment Division ###
        segmentDivision = pm.intField ( self.segmentDivision_intField , q = True , v = True ) ;

        if not segmentDivision :
            segmentDivision = 0 ;

        segmentType = pm.optionMenu ( self.segmentType_otm , q = True , v = True ) ;
        # segmentAverage = pm.checkBox ( self.segmentAverage_cbx , q = True , v = True ) ;

        self.createJointChain (
            curve           = curve ,
            transform_list  = transform_list ,
            segmentDivision = segmentDivision ,
            aimDir          = aimDir ,
            aimZ            = aimZ ,
            aimZType        = aimZType ,
            segmentType     = segmentType ,
            ) ;

class CreatePolyPlane ( object ) :

    def __init__ ( self ) :

        super ( CreatePolyPlane , self ).__init__() ;

    def getSelectedRootJoint ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;

        if not selection_list :
            pm.error ( '<<!!>> No object is being selected, please select the root joint of the joint chain to continue.' ) ;

        if len ( selection_list ) != 1 :
            pm.error ( '<<!!>> More than 1 object are being selected, please select only the root joint of the joint chain to continue.' ) ;

        selection = selection_list[0] ;

        if selection.nodeType() != 'joint' :
            pm.error ( '<<!!>> The object selected is not a joint' ) ;

        return selection ;

    def createPolyPlaneRef_cmd ( self , *args ) :
        
        selection = self.getSelectedRootJoint() ;

        joint_list = [] ;
        joint_list.append ( selection ) ;
        joint_list.extend ( selection.listRelatives ( ad = True , type = 'transform' ) ) ;

        # check if refCrv exists

        for item in joint_list :
            if '_RefCrv' in item.nodeName() :
                pm.error ( '<!!> Ref Crv detected in joint chain, no operation will be commenced' )

        self.createRefCrv ( jnt_list = joint_list ) ;

        pm.select ( selection ) ;

    def refSizeFsg_cc ( self , *args  ) :
        
        selection = self.getSelectedRootJoint() ;

        value = pm.floatSliderGrp ( self.refSize_fsg , q = True , v = True ) ;

        for item in selection.listRelatives ( ad = True , type = 'transform' ) :

            if '_RefCrv' in item.nodeName() :
                item.s.set ( value , value , value ) ;

    def createPolyPlane_cmd ( self , *args ) :
        
        selection = self.getSelectedRootJoint() ;

        refCrv_list = [] ;

        for item in selection.listRelatives ( ad = True , type = 'transform' ) :
            if '_RefCrv' in item.nodeName() :
                refCrv_list.append ( item ) ;

        self.createPolyPlane ( refCrv_list = refCrv_list ) ;

class Gui ( CurveFunc , TfmFunc , AimZFunc , CreateJointChain , CreatePolyPlane ) :

    def __init__ ( self ) :

        super ( Gui , self ).__init__() ;

        self.ui         = 'createJointChainToCurve_ui' ;
        self.width      = 425.00 ;
        self.title      = 'Create Joint Chain to Curve Tool' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def showGui ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = self.width , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = self.width ) ;
            with main_layout :

                # curve layout
                with pm.frameLayout (
                    label       = 'Curve' ,
                    width       = w ,
                    collapsable = False ,
                    collapse    = False ,
                    bgc         = ( 0.078 , 0.153 , 0.263 ) ,
                    ) :

                    with pm.rowColumnLayout ( nc = 1 , w = w ) :

                        self.curve_textScrollList = pm.textScrollList ( w = w , h = 25 , enable = False ) ;

                        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :

                            self.curveGet_btn   = pm.button ( label = 'Get Curve' , bgc = ( 1 , 1 , 1 ) , w = w/2 , c = self.curveGet_cmd ) ;
                            self.curveClear_btn = pm.button ( label = 'Clear' , w = w/2 , c = self.curveClear_cmd ) ;

                # transform layout
                with pm.frameLayout (
                    label       = 'Transform' ,
                    width       = w ,
                    collapsable = False ,
                    collapse    = False ,
                    bgc         = ( 0.078 , 0.153 , 0.263 ) ,
                    ) :

                    with pm.rowColumnLayout ( nc = 1 , w = w ) :

                        self.tfm_textScrollList = pm.textScrollList ( w = w , h = 125 , allowMultiSelection = True ) ;

                        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :

                            self.tfmAppend_btn  = pm.button ( label = 'Append Tfm(s)' , bgc = ( 1 , 1 , 1 ) , w = w/2 , c = self.tfmAppend_cmd ) ;
                            self.tfmRemove_btn  = pm.button ( label = 'Remove Tfm(s)' , w = w/2 , c = self.tfmRemove_cmd ) ;
                       
                            self.tfmSort_btn    = pm.button ( label = 'Sort Tfm(s)' , w = w/2 , c = self.tfmSort_cmd ) ;
                            self.tfmClear_btn   = pm.button ( label = 'Clear' , w = w/2 , c = self.tfmClear_cmd ) ;

                # aim layout
                with pm.frameLayout (
                    label       = 'Aim Z (Optional)' ,
                    width       = w ,
                    collapsable = False ,
                    collapse    = False ,
                    bgc         = ( 0.078 , 0.153 , 0.263 ) ,
                    ) :

                    with pm.rowColumnLayout ( nc = 1 , w = w ) :

                        self.aim_textScrollList = pm.textScrollList ( w = w , h = 25 , enable = False ) ;

                        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :
                            self.aimZCreate_btn = pm.button ( label = 'Create AimZ Object' , w = w/2 , bgc = ( 1 , 1 , 1 ) , c = self.aimZCreate_cmd ) ;
                            self.aimZGet_btn    = pm.button ( label = 'Get AimZ Object' , w = w/2 , c = self.aimZGet_cmd ) ;

                            pm.text ( label = '' ) ;
                            self.aimZClear_btn  = pm.button ( label = 'Clear AimZ Object' , c = self.aimZClear_cmd ) ;

                # create joint chain
                with pm.frameLayout (
                    label       = 'Joint Chain Options' ,
                    width       = w ,
                    collapsable = False ,
                    collapse    = False ,
                    bgc         = ( 0.078 , 0.153 , 0.263 ) ,
                    ) :

                    with pm.rowColumnLayout ( nc = 1 , w = w ) :

                        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :

                            with pm.rowColumnLayout ( nc = 1 , w = w/2 ) : 

                                pm.text ( label = 'Orient Options' ) ;
                                pm.separator ( h = 10 ) ;

                                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2/2 ) , ( 2 , w/2/2 ) ] ) :

                                    with pm.rowColumnLayout ( nc = 1 , w = w/2/2 ) :

                                        pm.text ( label = 'Primary Axis (Y)' ) ;

                                        self.aimDir_otm = pm.optionMenu ( w = w/2/2 ) ;
                                        with self.aimDir_otm :
                                            pm.menuItem ( label = 'Y Down' ) ;
                                            pm.menuItem ( label = 'Y Up' ) ;

                                    with pm.rowColumnLayout ( nc = 1 , w = w/2/2 ) :

                                        pm.text ( label = 'Secondary Axis (Z)' ) ;
                                        
                                        self.secondaryAimDir_otm = pm.optionMenu ( w = w/2/2 ) ;
                                        with self.secondaryAimDir_otm :
                                            pm.menuItem ( label = 'None' ) ;
                                            pm.menuItem ( label = 'Aim Obj Z Up' ) ;
                                            pm.menuItem ( label = 'To Aim Obj' ) ;

                            with pm.rowColumnLayout ( nc = 1 , w = w/2 ) : 

                                pm.text ( label = 'Segment Division Options' ) ;
                                pm.separator ( h = 10 ) ;

                                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2/2 ) , ( 2 , w/2/2 ) ] ) :

                                    with pm.rowColumnLayout ( nc = 1 , w = w/2/2 ) :

                                        pm.text ( label = 'Segment Type' ) ;

                                        self.segmentType_otm = pm.optionMenu ( w = w/2/2 ) ;
                                        with self.segmentType_otm :
                                            pm.menuItem ( label = 'Uniform' ) ;
                                            pm.menuItem ( label = 'Average' ) ;

                                    with pm.rowColumnLayout ( nc = 1 , w = w/2/2 ) :

                                        pm.text ( label = 'Segment Division' , w = w/2/2 ) ;

                                        self.segmentDivision_intField = pm.intField ( w = w/2/2 , v = 3 ) ;

                with pm.frameLayout (
                    label       = 'Create Joint Chain' ,
                    width       = w ,
                    collapsable = False ,
                    collapse    = False ,
                    bgc         = ( 0.078 , 0.153 , 0.263 ) ,
                    ) :

                    with pm.rowColumnLayout ( nc = 1 , w = w ) :
                        
                        self.jointChainCreate_btn = pm.button ( label = 'Create Joint Chain' , w = w , bgc = ( 1 , 1 , 1 ) , c = self.jointChainCreate_cmd ) ;

                with pm.frameLayout (
                    label       = 'Create Poly Plane' ,
                    width       = w ,
                    collapsable = False ,
                    collapse    = False ,
                    bgc         = ( 0.078 , 0.153 , 0.263 ) ,
                    ) :

                    with pm.rowColumnLayout ( nc = 1 , w = w ) :
                        self.createPolyPlaneRef_btn = pm.button ( label = 'Create PolyPlane Ref' , w = w ,
                            c = self.createPolyPlaneRef_cmd ) ;
                        
                        self.refSize_fsg = pm.floatSliderGrp ( label = "Ref Size",
                            field = True ,
                            #fieldMinValue = 0 , fieldMaxValue = 10,
                            minValue = 0, maxValue = 100,
                            columnWidth3 = [ w/8 , w/8 , w/8*6 ] , columnAlign3 = ["left", "both", "left"],
                            precision = 2, value = 1 ,
                            cc = self.refSizeFsg_cc , dc = self.refSizeFsg_cc ) ;

                        self.createPolyPlane_btn = pm.button ( label = 'Create PolyPlane' , w = w , bgc = ( 1 , 1 , 1 ) ,
                            c = self.createPolyPlane_cmd ) ;

        window.show () ;