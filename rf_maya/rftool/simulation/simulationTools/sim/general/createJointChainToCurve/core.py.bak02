import pymel.core as pm ;
import math ;

import simulationTools.sim.general.createJointChainToCurve.gui as gui ;
reload ( gui ) ;

import simulationTools.sim.general.createJointChainToCurve.general as gen ;
reload ( gen ) ;

class CreatePolyPlane ( object ) :
    
    def __init__ ( self ) :        
        super ( CreatePolyPlane , self ).__init__() ;
        self.refCrv_point = [ ( -1 , 0 , 0 ) , ( 1 , 0 , 0 )  , ( 0 , 0 , 0.33 ) , ( -1 , 0 , 0 ) ] ;

    def createRefCrv ( self , jnt_list ) :
        # refCrv_list

        refCrv_list = [] ;

        for jnt in jnt_list :
            jntNm   = jnt.nodeName().split('_')[0] ;

            refCrv  = pm.curve ( d = 1 , p = self.refCrv_point , n = jntNm + '_RefCrv' ) ;

            refCrv_list.append ( refCrv ) ;
            
            ### Move RefCrv under selected Jnts ###
            refCrv.t.lock() ;
            refCrv.r.lock() ;
            pm.parent ( refCrv , jnt ) ;
            refCrv.r.unlock() ;
            refCrv.rx.lock() ;

        # assign color so that it is easier for the eyes
        # auto twist for easier adjustment ;

        return refCrv_list ;

    def createPolyPlane ( self , refCrv_list ) :

        crv1_point = [] ;
        crv2_point = [] ;

        for refCrv in refCrv_list :

            refCrv = pm.PyNode ( refCrv ) ;
            print refCrv ;

            pos = pm.xform ( refCrv.ep[0] , q = True , t = True , ws = True ) ;
            crv1_point.append ( pos ) ;

            pos = pm.xform ( refCrv.ep[1] , q = True , t = True , ws = True ) ;
            crv2_point.append ( pos ) ;

        plane = pm.polyPlane ( sx = 1 , sy = len ( crv1_point ) - 1 ) ;

        c1 = 1 ;
        c2 = 0 ;

        for point1 , point2 in zip ( crv1_point , crv2_point ) :
            pm.xform (  plane[0].nodeName() + '.vtx[%s]' % c1 , t = point1 , ws = True ) ;
            pm.xform (  plane[0].nodeName() + '.vtx[%s]' % c2 , t = point2 , ws = True ) ;
            c1 += 2 ;
            c2 += 2 ;

        # crv1 = pm.curve ( d = 1 , p = crv1_point ) ;
        # print crv1 ;
        # crv2 = pm.curve ( d = 1 , p = crv2_point ) ;
        # print crv2 ;

        # self.deleteHistory ( loftedPoly ) ;
        # pm.delete ( crv1 , crv2 ) ;
        # pm.delete ( refCrv_list ) ;

        # nm1 = refCrv_list[0].nodeName().split('_')[0] ;
        # nm2 = refCrv_list[-1].nodeName().split('_')[0] ;

        # loftedPoly[0].rename ( nm1 + '_' + nm2 + '_Poly' ) ;

class CoreFunc ( CreatePolyPlane , gui.Gui , gen.General ) :

    def __init__ ( self ) :

        super ( CoreFunc , self ).__init__() ;

    def rebuildCurve ( self , curve ) :
        # Rebuild the curve so that it have min, max value of 0 to 1 - cleaner and more managable

        curve = pm.PyNode ( curve ) ;
        curveShape = curve.getShape() ;

        minValue    = curveShape.minMaxValue.minValue.get() ;
        maxValue    = curveShape.minMaxValue.maxValue.get() ;
        degree      = pm.getAttr ( curveShape.nodeName()+ '.degree' ) ;
        spans       = curveShape.spans.get() ; 

        if (minValue != 0) or (maxValue != 1) :
            
            pm.rebuildCurve ( curve ,
                ch                  = 1 ,
                rpo                 = 1 ,
                rebuildType         = 0 ,
                end                 = 1 ,
                keepRange           = 0 ,
                keepControlPoints   = 0 ,
                keepEndPoints       = 1 ,
                keepTangents        = 0 ,
                spans               = spans ,
                degree              = degree ,
                tolerance           = 0.01 ,
                ) ;

    def nearestPointOnCurve ( self , curve , transform ) :
        # return parameter

        curve       = pm.PyNode ( curve ) ;
        curveShape  = curve.getShape() ;
        
        transform   = pm.PyNode ( transform ) ;

        npoc    = pm.shadingNode ( 'nearestPointOnCurve' , asUtility = True ) ;
        pos     = pm.xform ( transform , q = True , rp = True , ws = True ) ;
        
        curveShape.worldSpace >> npoc.inputCurve ;
        npoc.inPosition.set ( pos ) ;

        parameter = npoc.result.parameter.get() ;
        pm.delete ( npoc ) ;

        return parameter ;

    def snapTransformToCurve ( self , curve , parameter ) :
        # return pos

        curve       = pm.PyNode ( curve ) ;
        curveShape  = curve.getShape() ;

        poci = pm.shadingNode ( 'pointOnCurveInfo' , asUtility = True ) ; 

        curveShape.worldSpace >> poci.inputCurve ;
        #poci.turnOnPercentage.set(1) ;
        poci.parameter.set ( parameter ) ;

        pos = poci.result.position.get() ;
        
        pm.delete ( poci ) ;

        return pos ;

    def createJointChain ( self ,
        curve ,
        transform_list ,
        segmentDivision ,
        aimDir ,
        aimZ ,
        aimZType ,
        segmentType ,
        *args ) :

        curve       = pm.PyNode ( curve ) ;
        curveShape  = curve.getShape() ;

        minValue    = curveShape.minMaxValue.minValue.get() ;
        maxValue    = curveShape.minMaxValue.maxValue.get() ; 
        
        ###########################
        ''' Rebuild the Curve '''
        ###########################
        #self.rebuildCurve ( curve ) ;

        ###########################
        ''' Create Parameter List '''
        ###########################
        ### Finding nearest point on curve for each transform ###

        parameter_list = [] ;

        for transform in transform_list :
            parameter = self.nearestPointOnCurve ( curve = curve , transform = transform ) ;
            parameter_list.append ( [ transform , parameter ] ) ;

        parameter_list.sort ( key = lambda p : p[1] ) ;

        ###########################
        ''' Calculate Distance '''
        ###########################
        
        ### Calculate average segment distance ###        
        averageDistance = 0 ;

        for i in range ( 0 , len ( parameter_list ) -1 ) :
            averageDistance += ( parameter_list[i+1][1] - parameter_list[i][1] ) ;
        
        averageDistance /= ( len ( parameter_list ) - 1 ) ;
        #dParameter_list = [ [ 'dummyStart' , 0 ] ] +  parameter_list + [ [ 'dummyEnd' , 1 ] ] ;

        joint_list = [] ;

        ####################################
        ''' Start Segment '''
        transform       = parameter_list[0][0] ;
        transform       = pm.PyNode ( transform ) ;
        transformName   = transform.split('_')[0] ;

        startSegmentLength = parameter_list[0][1] ;
        startSegmentDivision = round ( startSegmentLength / ( averageDistance / (segmentDivision + 1) ) ) ;
        startSegmentDivision = int ( startSegmentDivision ) ;

        if startSegmentDivision != 0 :

            segmentDivisionLength = startSegmentLength / startSegmentDivision ;

            for i in range ( 0 , startSegmentDivision ) :
                jnt = pm.createNode ( 'joint' , name = transformName + '_S' + str ( i + 1 ) + '_PJnt' ) ;
                parameter =  0 + ( (i) * segmentDivisionLength ) ;
                pos = self.snapTransformToCurve ( curve = curve , parameter = parameter ) ;
                jnt.t.set ( pos ) ;
                
                joint_list.append ( jnt ) ;

        ####################################
        ''' Mid Segment '''
        
        for i in range ( 0 , len ( parameter_list ) ) :

            transform = parameter_list[i][0] ;
            transform = pm.PyNode ( transform ) ;
            transformName = transform.nodeName().split('_')[0] ;

            parameter = parameter_list[i][1] ;

            mainJnt = pm.createNode ( 'joint' , n = transformName + '_0_PJnt' ) ;
  
            pos = self.snapTransformToCurve ( curve = curve , parameter = parameter ) ;
            mainJnt.t.set ( pos ) ;
            mainJnt.radius.set ( 2 ) ;

            joint_list.append ( mainJnt ) ;

            # if segmentDivision == 0 : no operation :

            if segmentDivision != 0 :

                if segmentDivision < 0 :
                    segmentDivision = int ( (segmentDivision**2)**0.5 ) ;

                # If this is the last of the transform, stop

                if i != len ( parameter_list ) - 1 :
                    
                    nextTransform = parameter_list[i+1][0] ;
                    nextTransform = pm.PyNode ( nextTransform ) ;
                    nextTransformName = nextTransform.nodeName().split('_')[0] ;

                    nextParameter = parameter_list[i+1][1] ;

                    curSegmentLength    = nextParameter - parameter ;
                    
                    ### Segment Type ###
                    if segmentType == 'Uniform' :
                        curSegmentDivision = segmentDivision ;
                    elif segmentType == 'Average' :
                        curSegmentDivision  = round ( curSegmentLength / ( averageDistance / segmentDivision ) ) ;
                        curSegmentDivision  = int ( curSegmentDivision ) ;

                    curSegmentDivisionLength = curSegmentLength / ( curSegmentDivision + 1 ) ;

                    for i in range ( 0 , curSegmentDivision ) :
                        jnt = pm.createNode ( 'joint' , name = transformName + '_' + str ( i + 1 ) + '_PJnt' ) ;
                        curParameter =  parameter + ( ( i + 1 ) * curSegmentDivisionLength ) ;
                        pos = self.snapTransformToCurve ( curve = curve , parameter = curParameter ) ;
                        jnt.t.set ( pos ) ;

                        joint_list.append ( jnt ) ;

        ####################################
        ''' End Segment '''
        transform       = parameter_list[-1][0] ;
        transform       = pm.PyNode ( transform ) ;
        transformName   = transform.split('_')[0] ;

        endSegmentLength    = maxValue - parameter_list[-1][1] ;
        endSegmentDivision  = round ( endSegmentLength / ( averageDistance / (segmentDivision + 1) ) ) ;
        endSegmentDivision  = int ( endSegmentDivision ) ;

        if endSegmentDivision != 0 :

            segmentDivisionLength = endSegmentLength / endSegmentDivision ;

            for i in range ( 0 , endSegmentDivision ) :
                jnt = pm.createNode ( 'joint' , name = transformName + '_E' + str ( i + 1 ) + '_PJnt' ) ;
                parameter =  parameter_list[-1][1] + ( (i+1) * segmentDivisionLength ) ;
                pos = self.snapTransformToCurve ( curve = curve , parameter = parameter ) ;
                jnt.t.set ( pos ) ;

                joint_list.append ( jnt ) ;

        ### Primary Axis Aim (Y) ###
        driver_list = joint_list ;
        driven_list = joint_list ;

        if aimDir == 'Y Up' :
            ### For some reason, the vanilla .reverse() doesn't work
            driver_list = joint_list[-1::-1] ;
            driven_list = joint_list[-1::-1] ;

        driver_list = driver_list[1:] + driver_list[0:1] ;
        driven_list = driven_list ;

        ### Secondary Axis Aim (Z) ###

        if ( aimZType == 'None' ) or ( not aimZ ) :

            for i in range ( 0 , len ( driver_list ) -1 ) :
                aimCon = pm.aimConstraint ( driver_list[i] , driven_list[i] ,
                    offset      = [ 0.0 , 0.0 , 0.0 ] ,
                    weight      = 1 ,
                    aimVector   = [ 0.0 , 1.0 , 0.0 ] ,
                    upVector    = [ 0.0 , 0.0 , 1.0 ] ,
                    worldUpType = 'scene' ,
                    skip        = 'none'
                    ) ;

                pm.delete ( aimCon ) ;

            aimCon = pm.aimConstraint ( driver_list[-3] , driven_list[-1] ,
                offset      = [ 0.0 , 0.0 , 0.0 ] ,
                weight      = 1 ,
                aimVector   = [ 0.0 , -1.0 , 0.0 ] ,
                upVector    = [ 0.0 , 0.0 , 1.0 ] ,
                worldUpType = 'scene' ,
                skip        = 'none'
                ) ;

            pm.delete ( aimCon ) ;

        elif ( aimZType == 'Aim Obj Z Up' ) and ( aimZ ) :
            
            for i in range ( 0 , len ( driver_list ) -1 ) :
                aimCon = pm.aimConstraint ( driver_list[i] , driven_list[i] ,
                    offset      = [ 0.0 , 0.0 , 0.0 ] ,
                    weight      = 1 ,
                    aimVector   = [ 0.0 , 1.0 , 0.0 ] ,
                    upVector    = [ 0.0 , 0.0 , 1.0 ] ,
                    worldUpType = 'objectrotation' , 
                    worldUpVector   = [ 0.0 , 0.0 , 1 ] ,
                    worldUpObject   = aimZ ,
                    skip        = 'none'
                    ) ;

                pm.delete ( aimCon ) ;

            aimCon = pm.aimConstraint ( driver_list[-3] , driven_list[-1] ,
                offset      = [ 0.0 , 0.0 , 0.0 ] ,
                weight      = 1 ,
                aimVector   = [ 0.0 , -1.0 , 0.0 ] ,
                upVector    = [ 0.0 , 0.0 , 1.0 ] ,
                worldUpType = 'objectrotation' , 
                worldUpVector   = [ 0.0 , 0.0 , 1 ] ,
                worldUpObject   = aimZ ,
                skip        = 'none'
                ) ;

            pm.delete ( aimCon ) ;

        elif ( aimZType == 'To Aim Obj' ) and ( aimZ ) :

            for i in range ( 0 , len ( driver_list ) -1 ) :
                aimCon = pm.aimConstraint ( driver_list[i] , driven_list[i] ,
                    offset      = [ 0.0 , 0.0 , 0.0 ] ,
                    weight      = 1 ,
                    aimVector   = [ 0.0 , 1.0 , 0.0 ] ,
                    upVector    = [ 0.0 , 0.0 , 1.0 ] ,
                    worldUpType = 'object' ,
                    worldUpObject   = aimZ ,
                    skip        = 'none'
                    ) ;

                pm.delete ( aimCon ) ;

            aimCon = pm.aimConstraint ( driver_list[-3] , driven_list[-1] ,
                offset      = [ 0.0 , 0.0 , 0.0 ] ,
                weight      = 1 ,
                aimVector   = [ 0.0 , -1.0 , 0.0 ] ,
                upVector    = [ 0.0 , 0.0 , 1.0 ] ,
                worldUpType = 'object' ,
                worldUpObject   = aimZ ,
                skip        = 'none'
                ) ;

            pm.delete ( aimCon ) ;

        ### make chain ###

        for child , parent in zip ( driven_list[-1:0:-1] , driven_list[-2::-1] ) :
            pm.parent ( child , parent ) ;

        ### make ik spline ###

        # ikHandleName = transform_list[0].split('_')[0] + '_' + transform_list[-1].split('_')[0] + '_Ikh' ; 

        # pm.ikHandle (
        #     startJoint  = driven_list[0] ,
        #     endEffector = driven_list[-1] ,
        #     curve       = curve ,
        #     createCurve = False ,
        #     parentCurve = False ,
        #     name        = ikHandleName ,
        #     solver      = 'ikSplineSolver' ,
        #     ) ;

def run ( *args ) :

    main = CoreFunc () ;
    main.showGui() ;

# test () ;