import pymel.core as pm ;
import re ;

class Cleanup ( object ) :

    def __init__ ( self ) :
        super ( Cleanup , self ).__init__() ;

    def freezeTransform ( self , target ) :
        pm.makeIdentity ( target , apply = True ) ;

    def deleteHistory ( self , target ) :
        pm.delete ( target , ch = True ) ;

    def centerPivot ( self , target ) :
        pm.xform ( target , cp = True ) ;

class NaturalSort ( object ) :

    def __init__ ( self ) :
        super ( NaturalSort , self ).__init__() ;

    def convertKeyElem ( self , input ) :

        if input.isdigit() :
            return int ( input ) ;
        else :
            return input.lower() ;

    def splitKey ( self , key ) :

        return_list = [] ;

        key = str ( key ) ;

        for elem in re.split ( '([0-9]+)' , key ) :
            return_list.append ( self.convertKeyElem ( elem ) ) ;

        return return_list ;

    def naturalSort ( self , list ) :

        return sorted ( list , key = self.splitKey ) ;

class General ( Cleanup , NaturalSort ) :

    def __init__ ( self ) :
        super ( General , self ).__init__() ;