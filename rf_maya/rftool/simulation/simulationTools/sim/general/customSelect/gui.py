import pymel.core as pm ;

class Gui ( object ) :

    def __init__ ( self ) :
        self.ui         = 'customSelect_ui' ;
        self.width      = 300.00 ;
        self.title      = 'Custom Select' ;
        self.version    = 1.0 ;

    # requestioning
    def checkUiUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUiUniqueness ( ui ) ;

    def checkBox_cmd ( self , *args ) :
        if pm.checkBox ( self.group_checkBox , q = True , v = True ) :
            pm.textField ( self.type_textField , e = True , en = False ) ;
        else :
            pm.textField ( self.type_textField , e = True , en = True ) ;

    def showWindow ( self ) :
      
        w = self.width ;

        # check ui duplication
        self.checkUiUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = self.width , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = self.width ) ;
            with main_layout :

                with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , w/3 ) , ( 2 , w/3 ) , ( 3 , w/3 ) ] ) :
                    
                    with pm.rowColumnLayout ( nc = 1 , w = w/3 ) :
                        pm.text ( label = 'Prefix' , w = w/3 ) ;
                        self.prefix_textField   = pm.textField ( w = w/3 ) ;
                    
                    with pm.rowColumnLayout ( nc = 1 , w = w/3 ) :
                        pm.text ( label = 'Suffix' , w = w/3 ) ;
                        self.suffix_textField   = pm.textField ( w = w/3 ) ;
                    
                    with pm.rowColumnLayout ( nc = 1 , w = w/3 ) :
                        pm.text ( label = 'Type' , w = w/3 ) ;
                        self.type_textField     = pm.textField ( w = w/3 ) ;
                        self.group_checkBox     = pm.checkBox ( label = 'Group' , cc = self.checkBox_cmd ) ;
                    
                pm.button ( label = 'Select' , w = w , c = self.select ) ;

        window.show () ;