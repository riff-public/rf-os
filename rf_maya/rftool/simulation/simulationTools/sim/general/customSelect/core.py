modulePath = 'simulationTools.sim.general.customSelect.' ;

for module in [ 'gui' , 'util' ] :
	exec ( 'import {modulePath}{module} as {module} ;'.format ( modulePath = modulePath , module = module ) ) ;
	exec ( 'reload ( {module} ) ;'.format ( module = module ) ) ; 

class CustomSelect ( gui.Gui , util.Util ) :

	def __init__ ( self ) :
		super ( CustomSelect , self ).__init__() ;

def run ( *args ) :
	customSelect = CustomSelect() ;
	customSelect.showWindow() ;