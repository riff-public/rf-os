import pymel.core as pm ;

class Util ( object ) :

    def __init__ ( self ) :
        super ( Util , self ).__init__ ( ) ;


    def checkType ( self , list , type , group = False ) :

        returnList = [] ;

        if group :
            for item in list :
                if not item.getShape() :
                    returnList.append ( item ) ;

        else :
            for item in list :
                if item.getShape() :
                    if str ( item.getShape().nodeType() )  == type :
                        returnList.append ( item ) ;

        return returnList ;

    def select ( self , *args ) :

        prefix  = pm.textField ( self.prefix_textField , q = True , tx = True ) ;
        suffix  = pm.textField ( self.suffix_textField , q = True , tx = True ) ;
        type    = pm.textField ( self.type_textField , q = True , tx = True ) ;
        group   = pm.checkBox ( self.group_checkBox , q = True , v = True ) ;

        toSelect = pm.ls ( '%s*%s' % ( prefix , suffix ) , type = 'transform' ) ;
        toSelect = self.checkType ( list = toSelect , type = type , group = group ) ;

        pm.select ( toSelect ) ;