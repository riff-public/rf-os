import pymel.core as pm ;

import simulationTools.sim.general.attachToCurve.function as func ;
reload ( func ) ;

import simulationTools.sim.general.attachToCurve.gui as gui ;
reload ( gui ) ;

import simulationTools.sim.general.attachToCurve.guiFunction as guiFunc ;
reload ( guiFunc ) ;

import simulationTools.sim.general.attachToCurve.general as gen ;
reload ( gen ) ;

class Main ( func.AttachToCurve , gui.Gui , guiFunc.GuiFunc , gen.NaturalSort ) :

    def __init__ ( self ) :
        super ( Main , self ).__init__() ;

def run ( *args ) :
    gui = Main() ;
    gui.show() ;

run() ;