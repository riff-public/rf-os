import pymel.core as pm ;

class AttachToCurve ( object ) :

    def __init__ ( self ) :
        
        super ( AttachToCurve , self ).__init__() ;

    def rebuildCurve ( self , curve ) :

        curve = pm.PyNode ( curve ) ;
        curveShape = curve.getShape() ;

        minValue    = curveShape.minMaxValue.minValue.get() ;
        maxValue    = curveShape.minMaxValue.maxValue.get() ;
        degree      = pm.getAttr ( curveShape.nodeName()+ '.degree' ) ;
        spans       = curveShape.spans.get() ; 

        if (minValue != 0) or (maxValue != 1) :
            
            pm.rebuildCurve ( curve ,
                ch  = 1 ,
                rpo = 1 ,
                rebuildType = 0 ,
                end         = 1 ,
                keepRange           = 0 ,
                keepControlPoints   = 0 ,
                keepEndPoints       = 1 ,
                keepTangents        = 0 ,
                spans       = spans ,
                degree      = degree ,
                tolerance   = 0.01 ,
                ) ;
        
        else :
            pass ;

    def attach ( self , *args ) :

        ###########################
        '''Start of UI Query'''
        ###########################
        curve = pm.textScrollList ( self.curve_textScrollList , q = True , allItems = True ) ;
        if not curve :
            pm.error ( '<!!> No curve, thus no operation' ) ;
        else :
            curve = curve[0] ;

        transform_list = pm.textScrollList ( self.tfm_textScrollList , q = True , allItems = True ) ;
        if not transform_list :
            pm.error ( '<!!> No transform(s), thus no operation' ) ;

        aimCon  = pm.checkBox ( self.aimCon_cbx , q = True , v = True ) ;
        loop    = pm.checkBox ( self.loop_cbx   , q = True , v = True ) ;
        orient = pm.optionMenu ( self.orient_otm , q = True , value = True ) ;

        ###########################
        '''Start of operation'''
        ###########################

        curve = pm.PyNode ( curve ) ;
        curveShape = curve.getShape() ;

        ### rebuild curve ###
        self.rebuildCurve ( curve ) ;

        aimGrp_list = [] ;
        rotGrp_list = [] ;

        for tfm in transform_list :

            tfm = pm.PyNode ( tfm ) ;
            tfmName = tfm.split('_')[0] ;

            npoc    = pm.shadingNode ( 'nearestPointOnCurve' , asUtility = True ) ;
            pos     = pm.xform ( tfm , q = True , rp = True , ws = True ) ;
            
            curveShape.worldSpace >> npoc.inputCurve ;
            npoc.inPosition.set ( pos ) ;

            parameter = npoc.result.parameter.get() ;
            pm.delete ( npoc ) ;

            poci = pm.shadingNode ( 'pointOnCurveInfo' , asUtility = True , n = tfmName + '_Poci' ) ; 

            curveShape.worldSpace >> poci.inputCurve ;
            poci.turnOnPercentage.set(1) ;
            poci.parameter.set ( parameter ) ;

            pociGrp     = pm.group ( em = True , n = tfmName + 'Poci_Grp' ) ;
            
            #rotGrp      = pm.group ( em = True , n = tfmName + 'Rot_Grp' ) ;
            rotGrp_list.append ( pociGrp ) ; 
            
            aimGrp      = pm.group ( em = True , n = tfmName + 'Aim_Grp' ) ;
            aimGrp_list.append ( aimGrp ) ;
            
            # pm.parent ( rotGrp , pociGrp ) ;
            # pm.parent ( aimGrp , rotGrp ) ;
            pm.parent ( aimGrp , pociGrp ) ;
            poci.result.position >> pociGrp.t ;
            
        ### Aim Constraint ###

        if aimCon :

            driverTemp_list = aimGrp_list ;
            drivenTemp_list = rotGrp_list ;

            if orient == 'Y Up' :
                driverTemp_list.reverse() ;
                drivenTemp_list.reverse() ;

            driver_list = driverTemp_list[1:] + driverTemp_list[0:1] ;
            driven_list = drivenTemp_list ;

            if loop :
            
                for driver, driven in zip ( driver_list , driven_list) :
                    pm.aimConstraint ( driver , driven ,
                        offset = [ 0.0 , 0.0 , 0.0 ] ,
                        weight = 1 ,
                        aimVector = [ 0.0 , 1.0 , 0.0 ] ,
                        upVector = [ 0.0 , 0.0 , 1.0 ] ,
                        worldUpType = 'scene' ,
                        skip = 'none'
                        ) ;

            else :

                for i in range ( 0 , len ( driver_list ) -1 ) :

                    pm.aimConstraint ( driver_list[i] , driven_list[i] ,
                        offset = [ 0.0 , 0.0 , 0.0 ] ,
                        weight = 1 ,
                        aimVector = [ 0.0 , 1.0 , 0.0 ] ,
                        upVector = [ 0.0 , 0.0 , 1.0 ] ,
                        worldUpType = 'scene' ,
                        skip = 'none'
                        ) ;

                tempCon = pm.aimConstraint ( driver_list[-3] , driven_list[-1] ,
                    offset = [ 0.0 , 0.0 , 0.0 ] ,
                    weight = 1 ,
                    aimVector = [ 0.0 , -1.0 , 0.0 ] ,
                    upVector = [ 0.0 , 0.0 , 1.0 ] ,
                    worldUpType = 'scene' ,
                    skip = 'none'
                    ) ;

                #pm.delete ( tempCon ) ;
                
        ### parent transform into the group ###
        for tfm , rotGrp in zip ( transform_list , rotGrp_list ) :
            
            pm.parentConstraint ( rotGrp , tfm , mo = True , skipTranslate = 'none' , skipRotate = 'none' ) ;