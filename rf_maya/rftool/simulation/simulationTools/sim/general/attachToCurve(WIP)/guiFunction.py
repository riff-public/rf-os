import pymel.core as pm ;

class CurveFunc ( object ) :

    def __init__ ( self ) :
        super ( CurveFunc , self ).__init__() ;

    def curveGet_cmd ( self , *args ) :
        
        selection_list = pm.ls ( sl = True ) ;

        if not selection_list :
            pass ;

        else :
            if len ( selection_list ) != 1 :
                print ( '<<!!>> More than 1 curves are being selected, only the first selected curve will be used' ) ;

            selection = selection_list[0] ;
            selectionShape = selection.getShape() ;

            if selectionShape.nodeType() != 'nurbsCurve' :
                print ( '<<!!>> Your selected object is not a curve, thus it will not be appended to the field' ) ;

            else :
                pm.textScrollList ( self.curve_textScrollList , e = True , ra = True ) ;
                pm.textScrollList ( self.curve_textScrollList , e = True , append = selection ) ;

    def curveClear_cmd ( self , *args ) :
        pm.textScrollList ( self.curve_textScrollList , e = True , ra = True ) ;

class TfmFunc ( object ) :

    def __init__ ( self ) :
        super ( TfmFunc , self ).__init__() ;

    def tfmAppend_cmd ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;
        selection_list = self.naturalSort ( selection_list ) ;

        if not selection_list :
            pass ;

        else :
            for selection in selection_list :

                allItem_list = pm.textScrollList ( self.tfm_textScrollList , q = True , allItems = True ) ;

                if selection in allItem_list :
                    print ( '<!!> %s already in the textScrolList, thus it will not be appended as duplicate' % selection ) ;

                else :
                    pm.textScrollList ( self.tfm_textScrollList , e = True , append = selection ) ;

    def tfmRemove_cmd ( self , *args ) :

        removeItem_list = pm.textScrollList ( self.tfm_textScrollList , q = True , selectItem = True ) ;

        for item in removeItem_list :
            pm.textScrollList ( self.tfm_textScrollList , e = True , removeItem = item ) ;

    def tfmClear_cmd ( self , *args ) :
        pm.textScrollList ( self.tfm_textScrollList , e = True , ra = True ) ;

    def tfmSort_cmd ( self , *args ) :

        allItem_list = pm.textScrollList ( self.tfm_textScrollList , q = True , allItems = True ) ;
        allItem_list = self.naturalSort ( allItem_list ) ;

        pm.textScrollList ( self.tfm_textScrollList , e = True , ra = True ) ;

        for item in allItem_list :
            pm.textScrollList ( self.tfm_textScrollList , e = True , append = item ) ;

class AttachFunc ( object ) :

    def __init__ ( self ) :
        super ( AttachFunc , self ).__init__() ;

    def attach_cmd ( self , *args ) :
        self.attach ( ) ;

class GuiFunc ( CurveFunc , TfmFunc , AttachFunc ) :

    def __init__ ( self ) :
        super ( GuiFunc , self ).__init__() ;