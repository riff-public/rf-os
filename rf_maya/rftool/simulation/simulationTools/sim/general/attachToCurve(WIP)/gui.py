import pymel.core as pm ;

class Gui ( object ) :

    def __init__ ( self ) :
        super ( Gui , self ).__init__() ;

        self.ui         = 'attachToCurve_ui' ;
        self.width      = 325.00 ;
        self.title      = 'Attach to Curve Tool' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def show ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
            
            # main layout
            with pm.rowColumnLayout ( nc = 1 , w = w ) :

                pm.button ( label = 'Help' , w = w ) ;
                pm.text ( label = '(c) RiFF Animation Studio, 2018, by Weerapot C.' ) ;

                # curve layout
                with pm.frameLayout (
                    label       = 'Curve' ,
                    width       = w ,
                    collapsable = False ,
                    collapse    = False ,
                    bgc         = ( 0.078 , 0.153 , 0.263 ) ,
                    ) :

                    with pm.rowColumnLayout ( nc = 1 , w = w ) :

                        self.curve_textScrollList = pm.textScrollList ( w = w , h = 25 , enable = False ) ;

                        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :

                            self.curveGet_btn   = pm.button ( label = 'Get Curve' , bgc = ( 1 , 1 , 1 ) , w = w/2 , c = self.curveGet_cmd ) ;
                            self.curveClear_btn = pm.button ( label = 'Clear' , w = w/2 , c = self.curveClear_cmd ) ;

                # transform layout
                with pm.frameLayout (
                    label       = 'Transform' ,
                    width       = w ,
                    collapsable = False ,
                    collapse    = False ,
                    bgc         = ( 0.078 , 0.153 , 0.263 ) ,
                    ) :

                    with pm.rowColumnLayout ( nc = 1 , w = w ) :

                        self.tfm_textScrollList = pm.textScrollList ( w = w , h = 125 , allowMultiSelection = True ) ;

                        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :

                            self.tfmAppend_btn  = pm.button ( label = 'Append Tfm(s)' , bgc = ( 1 , 1 , 1 ) , w = w/2 , c = self.tfmAppend_cmd ) ;
                            self.tfmRemove_btn  = pm.button ( label = 'Remove Tfm(s)' , w = w/2 , c = self.tfmRemove_cmd ) ;
                       
                            self.tfmSort_btn    = pm.button ( label = 'Sort Tfm(s)' , w = w/2 , c = self.tfmSort_cmd ) ;
                            self.tfmClear_btn   = pm.button ( label = 'Clear' , w = w/2 , c = self.tfmClear_cmd ) ;

                # aim layout
                with pm.frameLayout (
                    label       = 'Aim Constraint (Optional)' ,
                    width       = w ,
                    collapsable = False ,
                    collapse    = False ,
                    bgc         = ( 0.078 , 0.153 , 0.263 ) ,
                    ) :

                    with pm.rowColumnLayout ( nc = 1 , w = w ) :

                        with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , w/6 ) , ( 2 , w/3 ) , ( 3 , w/3 ) , ( 4 , w/6 ) ] ) :
                            pm.text( label = '' ) ;

                            self.aimCon_cbx = pm.checkBox ( label = 'Aim Constraint' , value = True ) ;
                            self.loop_cbx   = pm.checkBox ( label = 'Loop' ) ;
                            
                            pm.text ( label = '' ) ;

                        pm.separator ( h = 10 ) ;

                        pm.text ( label = 'Orient Option' ) ;

                        self.orient_otm = pm.optionMenu ( w = w ) ;
                        with self.orient_otm :
                            pm.menuItem ( label = 'Y Down' ) ;
                            pm.menuItem ( label = 'Y Up' ) ;

                # attach layout
                with pm.frameLayout (
                    label       = 'Attach' ,
                    width       = w ,
                    collapsable = False ,
                    collapse    = False ,
                    bgc         = ( 0.078 , 0.153 , 0.263 ) ,
                    ) :

                    with pm.rowColumnLayout ( nc = 1 , w = w ) :
                        self.attach_btn = pm.button ( label = 'Attach' , bgc = ( 0.5 , 1 , 0 ) , w = w , c = self.attach_cmd ) ;

        window.show () ;