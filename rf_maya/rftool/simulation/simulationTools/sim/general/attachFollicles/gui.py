import pymel.core as pm ;

class Gui ( object ) :

    def __init__ ( self ) :
        super ( Gui , self ).__init__() ;

        self.ui         = 'attachFollicle_gui' ;
        self.width      = 300.00 ;
        self.title      = 'Attach Follicle' ;
        self.version    = 2.0 ;

        self.help = '''
### Attach Fol ###

*** If you only select the surface, when click 'Attach Fol' button the follicle will be created on the surface and parameter U, V will be setted as (0.5).

1. Select transform(s) , then select surface you want the follicles to attach to.

2. The follicle(s) will be placed on the closest point on surface to the selected transform(s).

2. If you want the selected transform to be parented into the follicles, check the 'Parent' checkBox.
''' ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def cc_cmd ( self , *args ) :
    	pm.window ( self.window , e = True , h = 10.00 ) ;

    def showGui ( self ) :
        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        self.window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( self.window , e = True , w = w , h = 10.00 ) ;
        with self.window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                self.attachFol_btn = pm.button ( label = 'Attach Fol' , w = w , c = self.attachFol_cmd ) ;

                with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , w/4 ) , ( 2 , w/4) , ( 3 , w/4 ) , ( 4 , w/4 ) ] ) :
                    pm.text ( label = '' ) ;
                    pm.text ( label = '' ) ;
                    pm.text ( label = '' ) ;
                    self.parent_cbx = pm.checkBox( label = 'Parent' , w = w , v = True ) ;

                pm.separator ( vis = False , h = 15 ) ;

                self.moveFol_btn = pm.button ( label = 'Move Fol' , w = w  ) ;

                pm.separator ( vis = False , h = 15 ) ;

                with pm.frameLayout ( label = 'Help' , collapsable = True , collapse = True , w = w , cc = self.cc_cmd ) :
                	with pm.rowColumnLayout ( nc = 1 , w = w ) :
	                	pm.scrollField ( w = w , h = 325 , wordWrap = True , tx = self.help ) ;

        self.window.show () ;