import pymel.core as pm ;

selection_list = pm.ls ( sl = True ) ;

curve_list = selection_list ;

for curve in curve_list :
    curve 		= pm.PyNode ( curve ) ;
    curveShape 	= curve.getShape() ;

    loc     = pm.spaceLocator ( n = curve.nodeName().split('_')[0] + '_Loc' ) ;
    pci = pm.shadingNode ( 'pointOnCurveInfo' , asUtility = True , n = curve.nodeName().split('_')[0] + '_Pci' ) ; 
    
    curveShape.worldSpace >> pci.inputCurve ;
    pci.turnOnPercentage.set(1) ;
    pci.parameter.set ( 1 ) ;
    pci.result.position >> loc.t ;

    aimLoc  = pm.spaceLocator ( n = curve.nodeName().split('_')[0] + '_AimLoc' ) ;
    aimpci = pm.shadingNode ( 'pointOnCurveInfo' , asUtility = True , n = curve.nodeName().split('_')[0] + '_AimPci' ) ; 
    
    curveShape.worldSpace >> aimpci.inputCurve ;
    aimpci.turnOnPercentage.set(1) ;
    aimpci.parameter.set ( 0.95 ) ;
    aimpci.result.position >> aimLoc.t ;

    aimCon = pm.aimConstraint ( aimLoc , loc ,
                offset      = [ 0.0 , 0.0 , 0.0 ] ,
                weight      = 1 ,
                aimVector   = [ 0.0 , -1.0 , 0.0 ] ,
                upVector    = [ 0.0 , 0.0 , 1.0 ] ,
                worldUpType = 'scene' ,
                skip        = 'none'
                ) ;

    startFrame = mc.playbackOptions ( q = True , minTime = True ) ; 
    endFrame = mc.playbackOptions ( q = True , maxTime = True ) ;

    tfm     = pm.group ( em = True , w = True , n = curve.nodeName().split('_')[0] + '_Tfm' ) ;

    parCon  = pm.parentConstraint ( loc , tfm , mo = False , skipTranslate = 'none' , skipRotate = 'none' ) ;
    pm.bakeResults ( tfm ,
        at  = [ "tx" , "ty" , "tz" , "rx" , "ry" , "rz" ] ,
        t   = ( startFrame , endFrame ) ,
        sb  = 1 ,
        simulation = True ,
        minimizeRotation = True ) ;

    pm.delete ( parCon , aimLoc , aimpci , loc , pci ) ;
