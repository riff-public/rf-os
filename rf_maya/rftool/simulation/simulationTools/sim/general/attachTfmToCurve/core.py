import pymel.core as pm ;

class CoreFunc ( object ) :

    def __init__ ( self ) :

        super ( CoreFunc , self ).__init__() ;

    def guiQuery ( self , *args ) :

        self.curvePercentage_val = pm.floatSliderGrp ( self.curvePercentage_fsg , q = True , v = True ) ;

        self.translateMain_val  = pm.checkBoxGrp ( self.translateMain_cbg , q = True , v1 = True ) ;
        self.translateSub_val   = pm.checkBoxGrp ( self.translateSub_cbg , q = True , va3 = True ) ;

        self.rotateMain_val     = pm.checkBoxGrp ( self.rotateMain_cbg , q = True , v1 = True ) ;
        self.rotateSub_val      = pm.checkBoxGrp ( self.rotateSub_cbg , q = True , va3 = True ) ;

        # print ( 'curve percentage    : ' + str ( self.curvePercentage_val ) ) ;
        # print ( 'curve translateMain : ' + str ( self.translateMain_val ) ) ;
        # print ( 'curve translateSub  : ' + str ( self.translateSub_val ) ) ;
        # print ( 'curve rotateMain    : ' + str ( self.rotateMain_val ) ) ;
        # print ( 'curve rotateSub     : ' + str ( self.rotateSub_val ) ) ;

    def getSelection ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;
        
        curve_list = [] ;

        group_list      = [] ;
        groupCurve_list = [] ;

        ### check if group , or actual curve ###
        for selection in selection_list :
            
            # if group #
            if not selection.getShape() :
                if selection.listRelatives ( ad = True , type = 'nurbsCurve' ) :
                    group_list.append ( selection ) ;
            
            # if curve #
            elif selection.getShape() :
                if selection.getShape().nodeType() == 'nurbsCurve' :
                    curve_list.append ( selection ) ; 

        if group_list :
            for group in group_list :
                children_list = group.listRelatives ( ad = True , type = 'transform' ) ;

                for child in children_list :
                    if child.getShape().nodeType() == 'nurbsCurve' :
                        groupCurve_list.append ( [ group , child ] ) ;

        self.groupCurve_list    = groupCurve_list ;
        self.curve_list         = curve_list ;

    def attachTfmToCurve ( self , curve ) :
        # return [ tfm , toDelete_list ] ;
        
        curve       = pm.PyNode ( curve ) ;
        curveShape  = curve.getShape() ;

        loc = pm.spaceLocator ( n = curve.nodeName().split('_')[0] + '_Loc' ) ;
        pci = pm.shadingNode ( 'pointOnCurveInfo' , asUtility = True , n = curve.nodeName().split('_')[0] + '_Pci' ) ; 
    
        curveShape.worldSpace >> pci.inputCurve ;
        pci.turnOnPercentage.set(1) ;
        pci.parameter.set ( self.curvePercentage_val ) ;
        
        pci.result.position >> loc.t ;

        aimLoc  = pm.spaceLocator ( n = curve.nodeName().split('_')[0] + '_AimLoc' ) ;
        aimpci = pm.shadingNode ( 'pointOnCurveInfo' , asUtility = True , n = curve.nodeName().split('_')[0] + '_AimPci' ) ; 
    
        curveShape.worldSpace >> aimpci.inputCurve ;
        aimpci.turnOnPercentage.set(1) ;
        aimpci.parameter.set ( self.aimPercentage_val ) ;
        aimpci.result.position >> aimLoc.t ;

        aimCon = pm.aimConstraint ( aimLoc , loc ,
                    offset      = [ 0.0 , 0.0 , 0.0 ] ,
                    weight      = 1 ,
                    aimVector   = [ 0.0 , -1.0 , 0.0 ] ,
                    upVector    = [ 0.0 , 0.0 , 1.0 ] ,
                    worldUpType = 'scene' ,
                    skip        = 'none'
                    ) ;

        tfm     = pm.group ( em = True , w = True , n = curve.nodeName().split('_')[0] + '_Tfm' ) ;


        skipTranslate   = [] ;
        skipRotate      = [] ;

        if self.translateMain_val :
            skipTranslate = 'none' ;
        else :
            if self.translateMain_val == [ 1 , 1 , 1 ] :
                skipTranslate = 'none' ;
            else :
                if not self.translateSub_val[0] :
                    skipTranslate.append ( 'x' ) ;
                if not self.translateSub_val[1] :
                    skipTranslate.append ( 'y' ) ;
                if not self.translateSub_val[2] :
                    skipTranslate.append ( 'z' ) ;

        if self.rotateMain_val :
            skipRotate = 'none' ;
        else :
            if self.rotateMain_val == [ 1 , 1 , 1 ] :
                skipRotate = 'none' ;
            else :
                if not self.rotateSub_val[0] :
                    skipRotate.append ( 'x' ) ;
                if not self.rotateSub_val[1] :
                    skipRotate.append ( 'y' ) ;
                if not self.rotateSub_val[2] :
                    skipRotate.append ( 'z' ) ;

        # snap transform to location before constraint operation ;
        parCon = pm.parentConstraint ( loc , tfm , mo = False , skipTranslate = 'none' , skipRotate = 'none' ) ;
        pm.delete ( parCon ) ;

        parCon = pm.parentConstraint ( loc , tfm , mo = False , skipTranslate = skipTranslate , skipRotate = skipRotate ) ;

        toDelete_list   = [ parCon , aimLoc , aimpci , loc , pci ] ;
        
        return [ tfm , toDelete_list ] ;

    def bakeKey ( self , target ) :

        startFrame = pm.playbackOptions ( q = True , minTime = True ) ;
        startFrame -= 5 ;

        endFrame = pm.playbackOptions ( q = True , maxTime = True ) ;
        endFrame += 5 ;

        at_list = [] ;

        if self.translateMain_val :
            at_list.extend ( [ 'tx' , 'ty' , 'tz' ] ) ;
        else :
            if self.translateMain_val == [ 1 , 1 , 1 ] :
                at_list.extend ( [ 'tx' , 'ty' , 'tz' ] ) ;
            else :
                if self.translateSub_val[0] :
                    at_list.append ( 'tx' ) ;
                if self.translateSub_val[1] :
                    at_list.append ( 'ty' ) ;
                if self.translateSub_val[2] :
                    at_list.append ( 'tz' ) ;

        if self.rotateMain_val :
            at_list.extend ( [ 'rx' , 'ry' , 'rz' ] ) ;
        else :
            if self.rotateMain_val == [ 1 , 1 , 1 ] :
                at_list.extend ( [ 'rx' , 'ry' , 'rz' ] ) ;
            else :
                if self.rotateSub_val[0] :
                    at_list.append ( 'rx' ) ;
                if self.rotateSub_val[1] :
                    at_list.append ( 'ry' ) ;
                if self.rotateSub_val[2] :
                    at_list.append ( 'rz' ) ;

        pm.bakeResults ( target ,
            at  = at_list ,
            t   = ( startFrame , endFrame ) ,
            sb  = 1 ,
            simulation = True ,
            minimizeRotation = True ) ;

        # Euler filter
        for each in target :
            each = pm.PyNode ( each ) ;
            animCurve_list = each.listConnections ( type = 'animCurve' ) ;
            pm.filterCurve ( animCurve_list ) ;

#     pm.delete ( parCon , aimLoc , aimpci , loc , pci ) ;    

    def mainFunc ( self , *args ) :

        self.guiQuery() ;

        if self.curvePercentage_val >= 50 :
            self.aimPercentage_val = self.curvePercentage_val - 0.5 ;
        else :
            self.aimPercentage_val = self.curvePercentage_val + 0.5 ;

        self.curvePercentage_val    /= 100 ;
        self.aimPercentage_val      /= 100 ;

        self.getSelection() ;

        toBake_list     = [] ;
        toDelete_list   = [] ;

        if self.groupCurve_list :
            # [ group , child ]

            for groupCurve in self.groupCurve_list :

                groupName = groupCurve[0].nodeName().split('_')[0] + '_TfmGrp' ;

                if not pm.objExists ( groupName ) :
                    group = pm.group ( em = True , w = True , n = groupName ) ;
                else :
                    group = pm.PyNode ( groupName ) ;

                tfm = self.attachTfmToCurve ( groupCurve[1] ) ;

                toBake_list.append ( tfm[0] ) ;
                toDelete_list.append ( tfm[1] ) ;
                
                tfm = tfm[0] ;

                pm.parent ( tfm , group ) ;

        if self.curve_list :

            for curve in self.curve_list :

                tfm = self.attachTfmToCurve ( curve ) ;

                toBake_list.append ( tfm[0] ) ;
                toDelete_list.append ( tfm[1] ) ;

        self.bakeKey ( toBake_list ) ;
        pm.delete ( toDelete_list ) ;

class GuiFunc ( object ) :

    def __init__ ( self ) :

        super ( GuiFunc , self ).__init__() ;

    def translateMain_cc ( self , *args ) :

        if pm.checkBoxGrp ( self.translateMain_cbg , q = True , v1 = True ) :
            pm.checkBoxGrp  ( self.translateSub_cbg , e = True , en = False ) ;

        elif not pm.checkBoxGrp ( self.translateMain_cbg , q = True , v1 = True ) :
            pm.checkBoxGrp  ( self.translateSub_cbg , e = True , en = True ) ;

    def rotateMain_cc ( self , *args ) :

        if pm.checkBoxGrp ( self.rotateMain_cbg , q = True , v1 = True ) :
            pm.checkBoxGrp  ( self.rotateSub_cbg , e = True , en = False ) ;

        elif not pm.checkBoxGrp ( self.rotateMain_cbg , q = True , v1 = True ) :
            pm.checkBoxGrp  ( self.rotateSub_cbg , e = True , en = True ) ;

    def guiInitiate ( self , *args ) :
        self.translateMain_cc() ;
        self.rotateMain_cc() ;
        self.window.show() ;

class Gui ( CoreFunc , GuiFunc ) :

    def __init__ ( self ) :

        super ( Gui , self ).__init__() ;

        self.ui         = 'transformOnCurve_ui' ;
        self.width      = 350.00 ;
        self.title      = 'Create Tfm On Curve' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def show ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        self.window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( self.window , e = True , w = w , h = 10.00 ) ;
        with self.window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , w/10*2 ) , ( 2 , w/10*8/3 )  , ( 3 , w/10*8/3 ) , ( 4 , w/10*8/3 ) ] ) :

                    pm.text ( label = '' ) ;
                    pm.text ( label = '0 (Base)' ) ;
                    pm.text ( label = '' ) ;
                    pm.text ( label = '100 (Tip)' ) ;

                self.curvePercentage_fsg = pm.floatSliderGrp ( 
                    field       = True ,
                    minValue    = 0 ,
                    maxValue    = 100 ,
                    columnWidth3 = [ 0 , 20 , 80 ] ,
                    columnAlign3 = [ "left" , "both" , "left" ] ,
                    precision   = 2 ,
                    value       = 100 ,
                    ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :
                    
                    with pm.rowColumnLayout ( nc = 1 , w = w/2 ) :
                        
                        # pm.text ( label = 'Translate' ) ;
                        
                        self.translateMain_cbg = pm.checkBoxGrp (
                            ncb     = 1 ,
                            label1  = 'Translate' ,
                            cc1     = self.translateMain_cc , 
                            cw1     = w/2 ,
                            v1      = True ,
                            ) ;

                        self.translateSub_cbg = pm.checkBoxGrp (
                            ncb         = 3 ,
                            labelArray3 = [ 'tx' , 'ty' , 'tz' ] ,
                            cw3         = [ w/2/3 , w/2/3 , w/2/3 ] ,
                            va3         = [ True , True , True ] ,
                            ) ;
                    
                    with pm.rowColumnLayout ( nc = 1 , w = w/2 ) :
                        
                        # pm.text ( label = 'Rotate' ) ;

                        self.rotateMain_cbg = pm.checkBoxGrp (
                            ncb     = 1 ,
                            label1  = 'Rotate' ,
                            cc1     = self.rotateMain_cc , 
                            cw1     = w/2 ,
                            v1      = False ,
                            ) ;
                        
                        self.rotateSub_cbg = pm.checkBoxGrp (
                            ncb         = 3 ,
                            labelArray3 = [ 'rx' , 'ry' , 'rz' ] ,
                            cw3         = [ w/2/3 , w/2/3 , w/2/3 ] ,
                            va3         = [ True , False , True ] ,
                            ) ;

                pm.button ( label = 'Create Transform' , w = w , c = self.mainFunc ) ;

        self.guiInitiate() ;       

def run ( *args ) :
    gui = Gui() ;
    gui.show() ;