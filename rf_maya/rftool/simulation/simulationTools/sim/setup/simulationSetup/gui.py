import pymel.core as pm ;

class Gui ( object ) :

    def __init__ ( self ) :
        self.ui         = 'simulationSet_ui' ;
        self.width      = 350.00 ;
        self.title      = 'Simulation Setup' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def show ( self ) :

    	w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = self.width , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = self.width ) ;
            with main_layout :

                pass ;

        window.show () ;

def run ( *args ) :
	gui = Gui() ;
	gui.show() ;

run () ;