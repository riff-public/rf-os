import pymel.core as pm ;
import maya.cmds as mc ;
import os , ast , re ;

from sim.sim_simUtilities import core_nucleus ;
reload ( core_nucleus ) ;

pm.cycleCheck ( e = 0 ) ;

######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################
''' simulate '''
######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################

currentProj = pm.workspace ( q = True , rootDirectory = True ) ;

def save ( *args ) :
    timeRangeTxt = open ( currentProj + 'data/timerange.txt' , 'w+' ) ;

    preRoll = pm.intField ( 'preRollIF' ,  q = True , value = True ) ;
    start = pm.intField ( 'startIF' ,  q = True , value = True ) ;
    end = pm.intField ( 'endIF' ,  q = True , value = True ) ;
    postRoll = pm.intField ( 'postRollIF' ,  q = True , value = True ) ;
    
    timeRangeTxt.write ( "{ 'pre roll': %d , 'start': %d , 'end' : %d , 'post roll' : %d }" % ( preRoll, start , end , postRoll ) ) ;
    timeRangeTxt.close ( ) ;

def initializeTimeRange ( *args ) :
    if os.path.exists ( currentProj + 'data/timerange.txt' ) == True :
        timeRangeTxt = open ( currentProj + 'data/timerange.txt' , 'r+' ) ;
        timeRange = timeRangeTxt.read ( ) ;
        timeRange = ast.literal_eval ( timeRange ) ;
        timeRangeTxt.close ( ) ;
        
        pm.intField ( 'preRollIF' ,  e = True , value = timeRange['pre roll'] ) ;
        pm.intField ( 'startIF' ,  e = True , value = timeRange['start'] ) ;
        pm.intField ( 'endIF' ,  e = True , value = timeRange['end'] ) ;
        pm.intField ( 'postRollIF' ,  e = True , value = timeRange['post roll'] ) ;

        pm.intField ( 'playblastStart' , e = True , value = timeRange['start'] ) ;
        pm.intField ( 'playblastEnd' , e = True , value = timeRange['end'] ) ;

    else : pass ;

def customPlayblast ( start , end , resW , resH , *args ) :

    if resW > 1920 or resH > 1080 :
        percent = 50 ;
    else :
        percent = 100 ;

    pm.playblast ( startTime = start , endTime = end , format = 'image' ,
        sequenceTime = 0 , clearCache = 1 , viewer = True , showOrnaments = True ,
        offScreen = True , fp = 4 , percent = percent , compression =  "maya" , quality = 100 ,
        widthHeight = [ resW , resH ] ) ;

def viewPlayblast ( *args ) :

    resW = pm.getAttr ( "defaultResolution.width" ) ;
    resH = pm.getAttr ( "defaultResolution.height" ) ;

    start = pm.intField ( 'playblastStart' ,  q = True , value = True ) ;
    end = pm.intField ( 'playblastEnd' ,  q = True , value = True ) ;

    customPlayblast ( start , end , resW , resH ) ;

def setTimeRange ( *args ) :
    currentStart = pm.playbackOptions ( q = True , min = True ) ;
    currentEnd = pm.playbackOptions ( q = True , max = True ) ;

    preRoll = pm.intField ( 'preRollIF' ,  q = True , value = True ) ;
    start = pm.intField ( 'startIF' ,  q = True , value = True ) ;
    end = pm.intField ( 'endIF' ,  q = True , value = True ) ;
    postRoll = pm.intField ( 'postRollIF' ,  q = True , value = True ) ;

    if currentStart == preRoll and currentEnd == postRoll :
        pm.playbackOptions ( min = start , max = end ) ;

    elif currentStart == start and currentEnd == end :
        pm.playbackOptions ( min = preRoll , max = postRoll ) ;

    else :
        pm.playbackOptions ( min = preRoll , max = postRoll ) ;

    updateSetTimeRangeBtnClr ( ) ;
    setStartBsh ( start = preRoll ) ;

def getTimeRange ( *args ) :

    selection = pm.ls ( sl = True ) ;
    if len ( selection ) != 1 :
        # print ( 'more than 1 object selected' ) ;
        pass ;
    else :
        selection = selection [0] ;
        selection = pm.general.PyNode ( selection ) ;
        
        start = selection.start.get ( ) ;
        end = selection.end.get ( ) ;

        preroll = start - 30 ;
        postRoll = end + 10 ;

        pm.intField ( 'preRollIF' ,  e = True , value = preroll ) ;
        pm.intField ( 'startIF' ,  e = True , value = start ) ;
        pm.intField ( 'endIF' ,  e = True , value = end ) ;
        pm.intField ( 'postRollIF' ,  e = True , value = postRoll ) ;

        pm.intField ( 'playblastStart' , e = True , value = start ) ;
        pm.intField ( 'playblastEnd' , e = True , value = end ) ;

        updateSetTimeRangeBtnClr ( ) ;

def checkVersion ( directory = '' , name = '' , incrementVersion = True , incrementSubVersion = False , *args ) :
    
    if os.path.exists ( directory ) == False :
        os.makedirs ( directory ) ;
    else : pass ;

    allFile = os.listdir ( directory ) ;
    versionList = [] ;
    
    for each in allFile :
        if each.split('_')[0] == name :
            versionList.append ( each ) ;
        else : pass ;
    
    if len ( versionList ) == 0 :
        version = '001'
        subVersion = '001'

    else :
        versionList.sort ( ) ;
        latestVersion = versionList [-1] ;
        latestVersion = latestVersion.split ( '_' ) ;
        version = int ( re.findall ( '[0-9]{3}' ,  latestVersion [ -2 ] ) [0] ) ;
        subVersion = int ( re.findall ( '[0-9]{3}' , latestVersion [ -1 ] ) [0] ) ;

        if incrementSubVersion == True :
            subVersion += 1 ;
            subVersion = str(subVersion).zfill(3) ;
        else :
            subVersion = str(subVersion).zfill(3) ;
            
        if incrementVersion == True :
            version += 1 ;
            version = str(version).zfill ( 3 ) ;
            subVersion = '001' ;
                # overwrite increment subversion
        else :
            version = str(version).zfill ( 3 ) ;

    return str(version) + '_' + str(subVersion) ;

def createName ( *args ) :
    selection = pm.ls ( sl = True ) ;
    globalName = '' ;
    for each in selection :
        name = each.split ( '_' ) [0] ;
        globalName += str(name) ;
    return globalName ;

def simulate ( type = '' ) :
    
    name = createName ( )  ;

    currentProject = pm.workspace ( q = True , fullName = True ) ;

    dynCacheDirectory = currentProject + '/data/DYN/%s' % name ;

    if os.path.exists ( dynCacheDirectory ) == False :
        os.makedirs ( dynCacheDirectory ) ;
    else : pass ;

    if type == 'replace' :
        incrementVersion = False ;
        incrementSubVersion = False ;
        
        try : pm.mel.eval ( 'deleteCacheFile 2 { "delete", "" } ;' ) ;
        except : pass ;
        else : pass ;

    else :
        if type == 'version' :
            incrementVersion = True ;
            incrementSubVersion = False ;
            
        elif type == 'subversion' :
            incrementVersion = False ;
            incrementSubVersion = True ;
                
        try : pm.mel.eval ( 'deleteCacheFile 2 { "keep", "" } ;' ) ;
        except : pass ;
        else : pass ;
    
    version = checkVersion ( directory = dynCacheDirectory , name = name ,
        incrementVersion = incrementVersion , incrementSubVersion = incrementSubVersion ) ;
    cacheName = name +  '_DYN_v' + version ;

    ev = pm.floatField ( 'evaluateEvery_input' , q = True , v = True ) ;

    pm.mel.eval ( 'doCreateNclothCache 5 { "2", "1", "10", "OneFile", "1", "%s","0","%s","0", "add", "0", "%s", "1","0","1","mcx" } ;' % ( dynCacheDirectory , cacheName , ev ) ) ;

def simulateBtn ( *args ) :
    type = pm.optionMenu( 'simTypeOpt' , q = True , v = True ) ;
    simulate ( type = type ) ;

    nucleus = core_nucleus.NucleusNode() ;
    nucleus.getEnableState() ;
    nucleus.enable(False) ;

    if pm.checkBox ( 'playBlastCB' , q = True , value = True ) == True :
        viewPlayblast ( ) ;

    nucleus.reset() ;

def updateSetTimeRangeBtnClr ( *args ) :

    preRoll = pm.intField ( 'preRollIF' ,  q = True , value = True ) ;
    postRoll = pm.intField ( 'postRollIF' ,  q = True , value = True ) ;

    start = pm.intField ( 'startIF' ,  q = True , value = True ) ;
    end = pm.intField ( 'endIF' ,  q = True , value = True ) ;

    currentStart = pm.playbackOptions ( q = True , min = True ) ;
    currentEnd = pm.playbackOptions ( q = True , max = True ) ;

    # print ( preRoll , postRoll , start , end ) ;

    if ( currentStart == start ) and ( currentEnd == end ) :
        pm.button ( 'setTimeRangeBtn' , e = True , bgc = ( 1 , 0.388235 , 0.278431 ) ) ;

    elif ( currentStart == preRoll ) and ( currentEnd == postRoll ) :
        pm.button ( 'setTimeRangeBtn' , e = True , bgc = ( 0 , 1 , 1 ) ) ;

    else : pass ;


def setStartBsh ( start , *args ) :
    
    bsh = 'CIN_COL_BSH' ;
    
    if pm.objExists ( bsh ) == True :
        key = pm.listAttr ( bsh + '.w' , m = True ) ;
        pm.cutKey ( bsh , at = key , option = 'keys' ) ;
        pm.setKeyframe ( bsh , at = key , t = start , v = 0 ) ;
        pm.setKeyframe ( bsh , at = key , t = start + 10 , v = 1 ) ;
    else : pass ;

    nucleus = pm.ls ( type = 'nucleus' ) ;

    if len ( nucleus ) == 0 :
        pass ;
    else :
        for each in nucleus :
            pm.setAttr ( each + '.' + 'startFrame' , start ) ;

    #pm.currentTime ( start ) ;

######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################
''' cache out '''
######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################

def createBwdFwdGrp ( *args ) :
    
    selection = pm.ls ( sl = True ) ;
    
    if len ( selection ) != 1 :
        pass ;
    
    else :    

        selection = selection [0] ;
        selection = pm.general.PyNode ( selection ) ;
        
        bwdGrp = pm.group ( em = True , n = '%s_BWD' % selection ) ;
        fwdGrp = pm.group ( em = True , n = '%s_FWD' % selection ) ;
        
        bwdGrp.tx.set ( selection.tx.get ( ) * -1 ) ;
        bwdGrp.ty.set ( selection.ty.get ( ) * -1 ) ;
        bwdGrp.tz.set ( selection.tz.get ( ) * -1 ) ;
        bwdGrp.t.lock ( ) ;
        bwdGrp.r.lock ( ) ;
        bwdGrp.s.lock ( ) ;
        
        fwdGrp.t.set ( selection.t.get ( ) ) ;
        fwdGrp.t.lock ( ) ;
        fwdGrp.r.lock ( ) ;
        fwdGrp.s.lock ( ) ;

## cache out ##
#doCreateGeometryCache 6 { "2", "1", "10", "OneFile", "1", "D:/TwoHeroes/film001/q0310/s0060/hanumanCasual/cache/DYN","0","Pants_DYN_v001_001","0", "export", "0", "1", "1","0","1","mcx","1" } ;
#// Result: D:/TwoHeroes/film001/q0310/s0060/hanumanCasual/cache/DYN//Pants_DYN_v001_001.xml // 


######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################
''' disable '''
######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################

def updateNucleusUI ( *args ) :
    nucleus = pm.ls ( type = 'nucleus' ) ;
    allNucleus = pm.textScrollList ( 'nucleusTxtScrll' , q = True , allItems = True ) ;

    for each in allNucleus :
        if pm.objExists ( each ) == False :
            pm.textScrollList ( 'nucleusTxtScrll' , e = True , removeItem = each ) ;
        else :  pass ;

    for each in nucleus :
        if each not in allNucleus :
            pm.textScrollList ( 'nucleusTxtScrll' , e = True , append = each ) ;
        else : pass ;

    selectedNucleus = pm.textScrollList ( 'nucleusTxtScrll' , q = True , selectItem = True ) ;

    if selectedNucleus != [] :
        if len(selectedNucleus) == 1 :
                if pm.getAttr ( '%s.enable' % selectedNucleus[0] ) == 0 :
                    pm.button ( 'neBtn' , e = True , nbg = False ) ;
                    pm.button ( 'ndBtn' , e = True , nbg = True ) ; 
                elif pm.getAttr ( '%s.enable' % selectedNucleus[0] ) == 1 :
                    pm.button ( 'neBtn' , e = True , nbg = True ) ;
                    pm.button ( 'ndBtn' , e = True , nbg = False ) ;    
        else :
            pm.button ( 'neBtn' , e = True , nbg = False ) ;
            pm.button ( 'ndBtn' , e = True , nbg = False ) ;
    else : pass ;

def enableNucleus ( *args ) :
    selectedNucleus = pm.textScrollList ( 'nucleusTxtScrll' , q = True , selectItem = True ) ;

    if selectedNucleus != [] :  
        for each in selectedNucleus :
            pm.setAttr ( each + '.enable' , 1 ) ;
        updateNucleusUI ( ) ;
    else : pass ;

def disableNucleus ( *args ) :
    selectedNucleus = pm.textScrollList ( 'nucleusTxtScrll' , q = True , selectItem = True ) ;

    if selectedNucleus != [] :  
        for each in selectedNucleus :
            pm.setAttr ( each + '.enable' , 0 ) ;
        updateNucleusUI ( ) ;
    else : pass ;

def enableDisableNCloth ( mode , *arg ) :
    selection = pm.ls ( sl = True ) ;

    for each in selection :
        eachShape = pm.listRelatives ( each , shapes = True ) [0] ;
        if pm.objectType ( eachShape ) == 'mesh' :
            nClothShape = pm.general.PyNode ( pm.listConnections ( eachShape , type = 'nCloth' )[0] ) ;
            nClothShape.isDynamic.set(mode) ;

        elif pm.objectType ( eachShape ) == 'nCloth' :
            nClothShape = pm.general.PyNode ( eachShape ) ;
            nClothShape.isDynamic.set(mode) ;

def enableNCloth ( *arg) :
    enableDisableNCloth ( 1 ) ;

def disableNCloth ( *arg) :
    enableDisableNCloth ( 0 ) ;

def enableDisableNHair ( mode , *args ) :
    # 0 = disable
    # 3 = enable

    selection = pm.ls ( sl = True ) ;

    for each in selection :
        eachShape = pm.general.PyNode ( pm.listRelatives ( each , shapes = True ) [0] ) ;
        eachShape.simulationMethod.set ( mode ) ;

def enableNHair ( *args ) :
    enableDisableNHair ( mode = 3 ) ;

def disableNHair ( *args ) :
    enableDisableNHair ( mode = 0 ) ;

def enableCycle ( *args ) :
    pm.cycleCheck ( e = 1 ) ;
    updateCycleButton ( ) ;

def disableCycle ( *args ) :
    pm.cycleCheck ( e = 0 ) ;
    updateCycleButton ( ) ;

def updateCycleButton ( *args ) :
    cc = pm.cycleCheck ( q = True , e = True ) ;
    
    if cc == True :
        pm.button ( 'eccBtn' , e = True , nbg = True ) ;
        pm.button ( 'dccBtn' , e = True , nbg = False ) ;   
    else :
        pm.button ( 'eccBtn' , e = True , nbg = False ) ;
        pm.button ( 'dccBtn' , e = True , nbg = True ) ;

######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################
''' import cache '''
######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################

def alembicPathBrowse_cmd ( *args ) :
    
    defaultAlembicPath = pm.textField ( 'alembicPath_textField' , q = True , tx = True ) ;
    alembicPath = pm.fileDialog2 ( dir = defaultAlembicPath , fileMode = 4 ) [0];
    #print alembicPath ;
    pm.textField ( 'alembicPath_textField' , e = True , tx = alembicPath ) ;

def cameraPathBrowse_cmd ( *args ) :
    
    defaultCameraPath = pm.textField ( 'cameraPath_textField' , q = True , tx = True ) ;
    cameraPath = pm.fileDialog2 ( dir = defaultCameraPath , fileMode = 4 ) [0];
    #print alembicPath ;
    pm.textField ( 'cameraPath_textField' , e = True , tx = cameraPath ) ;

def hairDynPathBrowse_cmd ( *args ) :
    defaultHairDynPath = pm.textField ( 'hairDynPath_textField' , q = True , tx = True ) ;
    hairDynPath = pm.fileDialog2 ( dir = defaultHairDynPath , fileMode = 4 ) [0];
    #print alembicPath ;
    pm.textField ( 'hairDynPath_textField' , e = True , tx = hairDynPath ) ;

def copyAttr ( driver , driven , *args  ) :
    
    driver = pm.general.PyNode ( driver ) ;
    driven = pm.general.PyNode ( driven ) ;

    # unlock attribute
    driver.tx.unlock ( ) ; driver.ty.unlock ( ) ; driver.tz.unlock ( ) ;
    driver.rx.unlock ( ) ; driver.ry.unlock ( ) ; driver.rz.unlock ( ) ;
    driver.sx.unlock ( ) ; driver.sy.unlock ( ) ; driver.sz.unlock ( ) ;
    driver.v.unlock ( ) ;
    
    driven.tx.unlock ( ) ; driven.ty.unlock ( ) ; driven.tz.unlock ( ) ;
    driven.rx.unlock ( ) ; driven.ry.unlock ( ) ; driven.rz.unlock ( ) ;
    driven.sx.unlock ( ) ; driven.sy.unlock ( ) ; driven.sz.unlock ( ) ;
    driven.v.unlock ( ) ;
    
    # copy attribute 
    driven.t.set ( driver.t.get ( ) ) ;
    driven.r.set ( driver.r.get ( ) ) ;
    driven.s.set ( driver.s.get ( ) ) ;
    driven.v.set ( driver.v.get ( ) ) ;
    
    # lock attribute
    driver.tx.lock ( ) ; driver.ty.lock ( ) ; driver.tz.lock ( ) ;
    driver.rx.lock ( ) ; driver.ry.lock ( ) ; driver.rz.lock ( ) ;
    driver.sx.lock ( ) ; driver.sy.lock ( ) ; driver.sz.lock ( ) ;
    #driver.v.lock ( ) ;
    
    driven.tx.lock ( ) ; driven.ty.lock ( ) ; driven.tz.lock ( ) ;
    driven.rx.lock ( ) ; driven.ry.lock ( ) ; driven.rz.lock ( ) ;
    driven.sx.lock ( ) ; driven.sy.lock ( ) ; driven.sz.lock ( ) ;
    #driven.v.lock ( ) ;

def connectABC ( topNode = '' , *args ) :
    
    # copy transform
    if pm.objExists ( topNode.split(':')[1] + '_CIN' ) == True :
    
        copyAttr ( driver = topNode , driven = ( topNode.split(':')[1] + '_CIN' ) ) ;
        
        print ( '(succeeded/transform) %s >>> %s' % ( topNode , topNode.split(':')[1] + '_CIN' ) ) ; 
    
    else :
        print ( '(failed/transform) %s >>> %s' % ( topNode , topNode.split(':')[1] + '_CIN' ) ) ; 

    allNode = pm.listRelatives ( topNode , ad = True ) ;
    
    for each in allNode :
        
        if pm.nodeType ( each ) == 'transform' :
            if pm.objExists ( each.split (':')[1] + '_CIN' ) == True :
                copyAttr ( driver = each , driven = each.split (':')[1] + '_CIN' ) ;
                print ( '(succeeded/transform) %s >>> %s' % ( each , each.split (':')[1] + '_CIN' ) ) ; 
            else :
                print ( '(failed/transform) %s >>> %s' % ( each , each.split (':')[1] + '_CIN' ) ) ;
        else : pass ;
    
    for each in allNode :
        
        if 'Shape' in str ( each ) :
            shape = each ;
            transform = each.split ( 'Shape' ) [0] ;
            
            outPolyMesh = pm.listConnections ( shape + '.inMesh' , p = True ) ;
            
            if outPolyMesh == [] :
                print ( '(no connection/abc) %s >>> None' % ( each ) ) ; 
            else :
                outPolyMesh = outPolyMesh [0] ;
                try :
                    pm.connectAttr ( outPolyMesh , transform.split(':')[1] + '_CINShape.inMesh' , f = True ) ;
                    print ( '(succeeded/abc) %s >>> %s' % ( each , each.split (':')[1] + '_CIN' ) ) ; 
                except :
                    print ( '(no connection/abc) %s >>> None' % ( each ) ) ; 
                    
    cinTopNode = pm.listRelatives ( topNode.split(':')[1] + '_CIN' , p = True ) [0] ;
    pm.parent ( cinTopNode , 'BWD_GRP' ) ;

def importAlembic ( *args ) :

    if pm.objExists ( 'BWD_GRP' ) == True :
        bwd_grp = 'BWD_GRP' ;
    else :
        bwd_grp = pm.group ( em = True , w = True , n = 'BWD_GRP' ) ;
        bwd_grp.t.lock ( ) ;
        bwd_grp.r.lock ( ) ;
        bwd_grp.s.lock ( ) ;

    alembicPath = pm.textField ( 'alembicPath_textField' , q = True , tx = True ) ;

    alembicPathValidity = False ;

    try :
        pm.system.importFile ( alembicPath , namespace = 'ABC' , gr = True , groupName = 'ABC_GRP' ) ;
        alembicPathValidity = True ;   
    except :
        print ( '<alembic> path invalid, please check' ) ;
        alembicPathValidity = False ;

    if alembicPathValidity == True :
        alembicTopNode = pm.listRelatives ( 'ABC_GRP' , children = True ) [0] ;
        
        # get timerange

        pm.select ( alembicTopNode ) ;
        try : getTimeRange ( ) ;
        except : pass ;
        
        # connect alembic\

        if pm.checkBox ( 'alembicImportType_cbx' , q = True , v = True ) == True :
            children = pm.listRelatives ( alembicTopNode , c = True ) ;
            
            for each in children :

                try :
                
                    bsh = pm.blendShape ( each , each.split(':')[1] + '_CIN' , automatic = True , origin = 'world' , n = each.split(':')[1] + 'ABC_BSH' ) [0] ; 
                    pm.setAttr ( bsh + '.' + each.split(':')[1] , 1 ) ;
                
                except :

                    try :
    
                        childrenChildren = pm.listRelatives ( each , c = True ) ;

                        for cc in childrenChildren :

                            bsh = pm.blendShape ( cc , cc.split(':')[1] + '_CIN' , automatic = True , origin = 'world' , n = cc.split(':')[1] + 'ABC_BSH' ) [0] ; 
                            pm.setAttr ( bsh + '.' + cc.split(':')[1] , 1 ) ;

                    except :
                        
                        print ( '<blendshape> %s is not blendshape to %s, shape or hierarchy not identical' % ( each , each.split(':')[1] + '_CIN' ) ) ;

            alembicTopNode.t.lock ( ) ;
            alembicTopNode.r.lock ( ) ;
            alembicTopNode.s.lock ( ) ;
            alembicTopNode.v.set ( 0 ) ;

            pm.parent ( alembicTopNode , bwd_grp ) ;
            pm.delete ( 'ABC_GRP' ) ;

            cinTopNode = pm.listRelatives ( alembicTopNode.split(':')[1] + '_CIN' , p = True ) [0] ;
            pm.parent ( cinTopNode , 'BWD_GRP' ) ;

        elif pm.checkBox ( 'alembicImportType_cbx' , q = True , v = True ) == False :
            connectABC ( topNode = alembicTopNode ) ;
            pm.delete ( 'ABC_GRP' ) ;

def importCam ( *args ) :
    
    if pm.objExists ( 'BWD_GRP' ) == True :
        bwd_grp = 'BWD_GRP' ;
    else :
        bwd_grp = pm.group ( em = True , w = True , n = 'BWD_GRP' ) ;
        bwd_grp.t.lock ( ) ;
        bwd_grp.r.lock ( ) ;
        bwd_grp.s.lock ( ) ;

    cameraPath = pm.textField ( 'cameraPath_textField' , q = True , tx = True ) ;
    cameraPathValidity = False ;

    try :
        pm.system.importFile ( cameraPath , namespace = 'CAM' , gr = True , groupName = 'CAM_GRP' ) ;
        cameraPathValidity = True ;

    except :
        print ( '<camera> path invalid, please check' ) ;

    if cameraPathValidity == True :        
        cameraTopNode = pm.listRelatives ( 'CAM_GRP' , children = True ) ;

        for each in cameraTopNode :

            each.t.lock ( ) ;
            each.r.lock ( ) ;
            each.s.lock ( ) ;

        pm.parent ( cameraTopNode , bwd_grp ) ;
        pm.delete ( 'CAM_GRP' ) ;

def importHairDyn ( *args ) :

    hairDynPath = pm.textField ( 'hairDynPath_textField' , q = True , tx = True ) ;
    #hairDynPathValidity = False ;

    melCmd = 'AbcImport -mode import -connect "/" "%s";' % hairDynPath ;

    try :

        pm.mel.eval ( melCmd ) ;

    except :

        print ( '<hair dyn> path invalid, please check' ) ;


def importCache_cmd ( *args ) :

    importCam ( ) ;
    importAlembic ( ) ;
    importHairDyn ( ) ;

######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################
''' playblast video '''
######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################

def getPreferencePath ( *args ) :
    myDocuments = os.path.expanduser('~') ;
    myDocuments = myDocuments.replace ( '\\' , '/' ) ;
    # C:/Users/Legion/Documents
    
    prefPath = myDocuments + '/birdScriptPreferences/simulationUtilities'

    if os.path.exists ( prefPath ) == False :
        os.makedirs ( prefPath ) ;
    else : pass ;

    return prefPath ;

def playblastPathBrowse_cmd ( *args ) :

    playblastPath = pm.fileDialog2 ( fileMode = 3 , ff = '' ) [0];
    
    pm.textField ( 'playblastPath_textField' , e = True , tx = playblastPath ) ;

    prefPath = getPreferencePath ( ) ;

    playblastDefaultPath_txt = open ( prefPath + '/playblastDefaultPath.txt' , 'w+') ;
    playblastDefaultPath_txt.write ( playblastPath ) ;
    playblastDefaultPath_txt.close ( ) ;

    updatePlayblastVersionDisplay ( ) ;

def updatePlayblastDefaultPath_cmd ( *args ) :

    prefPath = getPreferencePath ( ) ;

    if os.path.exists ( prefPath + '/playblastDefaultPath.txt' ) == False :
        pm.textField ( 'playblastPath_textField' , e = True , tx = 'plz browse & select default directory' ) ;
        
    else :
        playblastDefaultPath_txt = open ( prefPath + '/playblastDefaultPath.txt' , 'r+') ;
        playblastDefaultPath = playblastDefaultPath_txt.read ( ) ;
        playblastDefaultPath_txt.close ( ) ;
        pm.textField ( 'playblastPath_textField' , e = True , tx = playblastDefaultPath ) ;

def updatePlayblastName_cmd ( *args ) :

    sceneName = mc.file ( q = True , exn = True ).split('/')[-1] ;
    sceneVersion = sceneName.split('.') [-2] ;

    sceneName = sceneName.split ( '.' + sceneVersion ) [0] ;

    pm.textField ( 'playblastName_textField' , e = True , tx = sceneName ) ;

    updatePlayblastVersionDisplay ( ) ;

def checkPlayblastVersion ( directory = '' , name = '' , incrementVersion = True , incrementSubVersion = False , *args ) :
    
    if os.path.exists ( directory ) == False :
        os.makedirs ( directory ) ;
    else : pass ;

    allFile = os.listdir ( directory ) ;
    versionList = [] ;
    
    for each in allFile :
        
        if name in str ( each ) :
            versionList.append ( each ) ;
        else : pass ;
    
    if len ( versionList ) == 0 :
        version = '001'
        subVersion = '001'

    else :
        versionList.sort ( ) ;

        latestVersion = versionList [-1] ;
        latestVersion = latestVersion.split ( '.' ) [-2] ;
        latestVersion = latestVersion.split ( '_' ) ;

        version = re.findall ( '[0-9]{3}' ,  latestVersion [ 0 ] ) [0] ;
        version = int ( version ) ;

        subVersion = re.findall ( '[0-9]{3}' , latestVersion [ 1 ] ) [0] ;
        subVersion = int ( subVersion ) ;

        if incrementSubVersion == True :
            subVersion += 1 ;
            subVersion = str(subVersion).zfill(3) ;
        else :
            subVersion = str(subVersion).zfill(3) ;
            
        if incrementVersion == True :
            version += 1 ;
            version = str(version).zfill ( 3 ) ;
            subVersion = '001' ;
                # overwrite increment subversion
        else :
            version = str(version).zfill ( 3 ) ;

    return str(version) + '_' + str(subVersion) ;

def playblastVideo_cmd ( incrementVersion = False , incrementSubVersion = False , *args ) :

    playblastDirectory = pm.textField ( 'playblastPath_textField' , q = True , tx = True ) ;
    playblastName = pm.textField ( 'playblastName_textField' , q = True , tx = True ) ;

    start = pm.intField ( 'playblastStart' , q = True , value = True ) ;
    end = pm.intField ( 'playblastEnd' , q = True , value = True ) ;

    version = checkPlayblastVersion ( directory = playblastDirectory , name = playblastName ,
        incrementVersion = incrementVersion , incrementSubVersion = incrementSubVersion ) ;

    scale = pm.intField ( 'playblastScale_intField' , q = True , value = True ) ;

    w = pm.getAttr ( 'defaultResolution.width' ) ;
    h = pm.getAttr ( 'defaultResolution.height' ) ;

    pm.playblast ( format = 'qt' , filename = playblastDirectory + '/' + playblastName + '.v' + version + '.mov' , st = start , et = end , clearCache = True , viewer = True ,
        showOrnaments = True , offScreen = True , fp = 4 , percent = scale , compression = "MPEG-4 Video" , quality = 100 , widthHeight = [ w , h ] , forceOverwrite = True ) ;

    updatePlayblastVersionDisplay ( ) ;

def playblastVideo_version_cmd ( *args ) :
    
    nucleus = core_nucleus.NucleusNode() ;
    nucleus.getEnableState() ;
    nucleus.enable(False) ;

    playblastVideo_cmd ( incrementVersion = True ) ;

    nucleus.reset() ;

def playblastVideo_subVersion_cmd ( *args ) :

    nucleus = core_nucleus.NucleusNode() ;
    nucleus.getEnableState() ;
    nucleus.enable(False) ;

    playblastVideo_cmd ( incrementSubVersion = True ) ;

    nucleus.reset() ;
    
def updatePlayblastVersionDisplay ( *args ) :

    playblastDirectory = pm.textField ( 'playblastPath_textField' , q = True , tx = True ) ;
    playblastName = pm.textField ( 'playblastName_textField' , q = True , tx = True ) ;

    version = checkPlayblastVersion ( directory = playblastDirectory , name = playblastName , incrementVersion = False , incrementSubVersion = False ) ;

    pm.text ( 'playblast_version_txt' , e = True ,  label = version.split('_')[0] ) ;
    pm.text ( 'playblast_subVersion_text' , e = True , label = version.split('_')[1] ) ;

def openPlayblastPath_cmd ( *args ) :

    path = pm.textField ( 'playblastPath_textField' , q = True , tx = True ) ;
    os.startfile ( path ) ;

######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################
''' UI '''
######################################################################################################################################################################################################
######################################################################################################################################################################################################
######################################################################################################################################################################################################

def frameLayout_cc ( *args ) : 
    pm.window ( 'simUtilities', e = True , h = 10 ) ;

def filler ( *args ) :
    pm.text ( label = '' ) ;

def run ( *args ) :

    width = 300.00 ;

    if pm.window ( 'simUtilities' , exists = True ) :
        pm.deleteUI ( 'simUtilities' ) ;
    else : pass ;

    window = pm.window ( 'simUtilities', title = "Simulation Utilities" , w = width , 
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;

    pm.window ( 'simUtilities', e = True , w = width , h = 10 ) ;
    
    with window :
    
        mainLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] ) ;
        with mainLayout :

            ######

            nucleusTxtScrll = pm.textScrollList ( 'nucleusTxtScrll' , w = width , h = 60 , ams = True , sc = updateNucleusUI ) ;
            
            nucleusLayout = pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width / 2.0 ) , ( 2 , width / 2.0 ) ] ) ;
            with nucleusLayout :
                pm.button ( 'neBtn' , label = 'enable nucleus' ,  c  = enableNucleus , w = width / 2.0 ) ;
                pm.button ( 'ndBtn' , label = 'disable nucleus' , c  = disableNucleus , w = width / 2.0 ) ;
                updateNucleusUI ( ) ;
            
            pm.separator ( vis = 0 ) ;

            cycleCheckLayout = pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width / 2.0 ) , ( 2 , width / 2.0 ) ] ) ;
            with cycleCheckLayout :
                pm.button ( 'eccBtn' , label = 'enable cycle check' ,  c  = enableCycle , w = width / 2.0 ) ;
                pm.button ( 'dccBtn' , label = 'disable cycle check' , c  = disableCycle , w = width / 2.0 ) ;
                updateCycleButton ( ) ;

            pm.separator ( vis = 0 ) ;

            enableLayout = pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width / 2.0 ) , ( 2 , width / 2.0 ) ] ) ;
            with enableLayout :
                pm.button ( 'nceBtn' , label = 'enable nCloth' ,  c  = enableNCloth , w = width / 2.0 , bgc = ( 0 , 1 , 1 ) ) ;
                pm.button ( 'ncdBtn' , label = 'disable nCloth' , c  = disableNCloth , w = width / 2.0 , bgc = ( 0 , 0.74902 , 1 ) ) ;

                pm.button ( 'nheBtn' , label = 'enable nHair' ,  c  = enableNHair , w = width / 2.0 , bgc = ( 0.498039 , 1 , 0 ) ) ;
                pm.button ( 'nhdBtn' , label = 'disable nHair' , c  = disableNHair , w = width / 2.0 , bgc = ( 0.196078 , 0.803922 , 0.196078 ) ) ;

            ######

            pm.separator ( vis = True , h = 10 ) ;

            timeRangeLayout = pm.rowColumnLayout ( w = width , nc = 5 , columnWidth = [
                ( 1 , width / 5.0 ), ( 2 , width / 5.0 ) , ( 3 , width / 5.0 ) , ( 4 , width / 5.0 ) , ( 5 , width / 5.0 ) ] ) ;
            with timeRangeLayout :    

                pm.intField ( 'preRollIF' ) ;
                pm.intField ( 'startIF' ) ;
                pm.button ( label = 'save' , c = save , bgc = ( 0.564706 , 0.933333 , 0.564706 ) ) ;
                pm.intField ( 'endIF' ) ;
                pm.intField ( 'postRollIF' ) ;
                pm.text ( label = 'pre roll' ) ;
                pm.text ( label = 'start' ) ;
                pm.text ( label = '' ) ;
                pm.text ( label = 'end' ) ;
                pm.text ( label = 'post roll' ) ;
    
            setTimeLayout = pm.rowColumnLayout ( w = width , nc = 3 , columnWidth = [ ( 1 , width * 0.05 ) , ( 2 , width * 0.9 ) , ( 3 , width * 0.05 ) ] ) ;
            with setTimeLayout :

                pm.text ( label = '' ) ;

                setTimeBtnLayout = pm.rowColumnLayout ( w = width * 0.9 , nc = 1 , columnWidth = [ ( 1 , width * 0.9 ) ] ) ;
                with setTimeBtnLayout :
                    pm.button ( label = 'get time range' , c = getTimeRange , bgc = ( 1 , 0.843137 , 0 ) ) ;
                    pm.button ( 'setTimeRangeBtn' , label = 'set time range' , c = setTimeRange ) ;
                    # set time range to sim --> color change 

                pm.text ( label = '' ) ;

            pm.separator ( h = 5 ) ;
            

            tab = pm.tabLayout ( 'tabLayout' ) ;
            with tab :

                simLayout = pm.rowColumnLayout ( 'simulation' , w = width  , nc = 1 , cw = [ ( 1 , width ) ] ) ;
                with simLayout :

                    simMainLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] ) ;
                    with simMainLayout :

                        pm.separator ( h = 5 , vis = False ) ;

                        with pm.rowColumnLayout ( w = width , nc = 5 , columnWidth = [ ( 1 , width*0.05 ), ( 2 , width*0.5 ) , ( 3 , width*0.05 ) , ( 4 , width*0.35 ) , ( 5 , width*0.05 ) ] ) :
                        # simulate button layout
                            filler ( ) ;
                            with pm.optionMenu( 'simTypeOpt' , label='sim type' ) :
                                pm.menuItem ( label='replace' ) ;
                                pm.menuItem ( label='version' ) ;
                                pm.menuItem ( label='subversion' ) ;
                            filler ( ) ;
                            pm.button ( label = 'simulate' , c = simulateBtn , bgc = ( 0 , 1 , 1 ) ) ;
                            filler ( ) ;

                        with pm.rowColumnLayout ( w = width , nc = 5 , cw = [ ( 1 , width*0.35 ) , ( 2 , width*0.25 ) , ( 3 , width*0.25 ) , ( 4 , width*0.1 ) , ( 5 , width*0.05 ) ] ) :

                            filler ( ) ;
                            pm.text ( label = 'evaluate every' ) ;
                            pm.floatField ( 'evaluateEvery_input' , pre = 2 , v = 1 ) ;
                            pm.text ( label = 'frame' ) ;
                            filler ( ) ;

                        with pm.rowColumnLayout ( w = width , nc = 5 , columnWidth = [ ( 1 , width*0.05 ), ( 2 , width*0.5 ) , ( 3 , width*0.05 ) , ( 4 , width*0.35 ) , ( 5 , width*0.05 ) ] ) :
                        # playblast check box
                            filler ( ) ;
                            filler ( ) ;
                            filler ( ) ;
                            with pm.rowColumnLayout ( nc = 4 , columnWidth = [ ( 1 , width*0.075 ) , ( 2 , width*0.05 ) , ( 3 , width*0.2 ) , ( 4 , width*0.075 ) ] ) :        
                                filler ( ) ;
                                pm.checkBox ( 'playBlastCB' , label = ' ' , value = True ) ;
                                pm.text ( label = 'playblast' , align = 'left' ) ;
                                filler ( ) ;
                            filler ( ) ;

                    pm.separator ( h = 10 ) ;

                    playblastLayout = pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) ;
                    with playblastLayout :
                        
                        playblastTimeRangeLayout = pm.rowColumnLayout ( nc = 5 , columnWidth = [
                            ( 1 , width / 5.0 ), ( 2 , width / 5.0  ) , ( 3 , width / 5.0  ) , ( 4 , width / 5.0  ) , ( 5 , width / 5.0  ) ] ) ;
                        with playblastTimeRangeLayout :   
                        
                            pm.text ( label = '' ) ;
                            pm.intField ( 'playblastStart' ) ;
                            pm.text ( label = '' ) ;
                            pm.intField ( 'playblastEnd' ) ;
                            pm.text ( label = '' ) ;
                        
                            pm.text ( label = '' ) ;
                            pm.text ( label = 'start' ) ;
                            pm.text ( label = '' ) ;
                            pm.text ( label = 'end' ) ;
                            pm.text ( label = '' ) ;

                        playblastBtnLayout = pm.rowColumnLayout ( nc = 3 , columnWidth = [ ( 1 , width * 0.05 ) , ( 2 , width * 0.9 ) , ( 3 , width * 0.05 ) ] ) ;
                        with playblastBtnLayout :

                            pm.text ( label = '' ) ;

                            with pm.rowColumnLayout ( nc = 1 , w = width*0.9 ) :
                            
                                pm.button ( label = 'playblast (for view)' , c = viewPlayblast , w = width*0.9 , bgc = ( 1 , 0.388235 , 0.278431 ) ) ;
                                
                                filler ( ) ;

                                #with pm.frameLayout ( label = 'PLAYBLAST (VIDEO)' , collapsable = True , collapse = False , cc = frameLayout_cc , bgc = ( 0.1 , 0.1 , 0.5 )  ) :
                                #    with pm.rowColumnLayout ( nc = 1 ) :

                                pm.text ( label = 'playblast directory :' ) ;
                                pm.textField ( 'playblastPath_textField' , w = width*0.5 ) ;
                                with pm.rowColumnLayout ( nc = 3 , w = width*0.9 , cw = [ ( 1 , width*0.3 ) , ( 2 , width*0.3 ) , ( 3 , width*0.3 ) ] ) :
                                    filler ( ) ;
                                    pm.button ( label = 'open' , c = openPlayblastPath_cmd ) ;
                                    pm.button ( label = 'browse' , c = playblastPathBrowse_cmd ) ;

                                pm.text ( label = 'playblast name :' ) ;
                                pm.textField ( 'playblastName_textField' ) ;

                                with pm.rowColumnLayout ( nc = 2 , w = width*0.9 , cw = [ ( 1 , width*0.6 ) , ( 2 , width*0.3 ) ] ) :
                                    filler ( ) ;
                                    pm.button ( label = 'refresh' , c = updatePlayblastName_cmd ) ;

                                filler ( ) ;

                                pm.text ( label = 'playblast ( video )' )

                                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width*0.45 ) , ( 2 , width*0.45 ) ] ) :

                                    pm.text ( 'playblast_version_txt' , label = '' ) ;
                                    pm.text ( 'playblast_subVersion_text' , label = '' ) ;

                                    pm.button ( label = 'increment version' , c = playblastVideo_version_cmd ) ;
                                    pm.button ( label = 'increment sub version' , c = playblastVideo_subVersion_cmd ) ;

                                    pm.text ( label = 'playblast scale (%)' )
                                    pm.intField ( 'playblastScale_intField' , value = 50 ) ;

                            filler ( ) ;

                    pm.separator ( h = 5 , vis = False ) ;

                with pm.rowColumnLayout ( 'cache in' , w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] ) :
                    
                    filler ( ) ;

                    with pm.rowColumnLayout ( nc = 3 , columnWidth = [ ( 1 , width * 0.05 ) , ( 2 , width * 0.9 ) , ( 3 , width * 0.05 ) ] ) :
                        
                        filler ( ) ;
                        
                        with pm.rowColumnLayout ( nc = 1 , w = width*0.9 ) :

                            shotCache_path = currentProj + 'data/shotCache/' ;

                            try :
                                shotCache_list = os.listdir ( shotCache_path ) ;
                            except :
                                shotCache_list = [] ;
                            ######################################
                            '''camera'''
                            ######################################
                            pm.text ( label = 'camera :' ) ;
                            
                            ### get camera default path ###
                            defaultCameraPath = shotCache_path ;

                            for shotCache in shotCache_list :
                                if '.cam.' in shotCache :
                                    defaultCameraPath = shotCache_path + shotCache ;
                                else : pass ;

                            with pm.rowColumnLayout ( nc = 2 , w = width*0.9 , cw = [ ( 1 , width*0.7 ) , ( 2 , width*0.2 ) ] ) :
                                pm.textField ( 'cameraPath_textField' , w = width*0.5 , tx = defaultCameraPath ) ;
                                pm.button ( label = 'browse' , c = cameraPathBrowse_cmd ) ;                            

                            with pm.rowColumnLayout ( nc = 2 , w = width*0.9 , cw = [ ( 1 , width*0.45 ) , ( 2 , width*0.45 ) ] ) :
                                filler ( ) ;
                                pm.button ( label = 'import camera' , c = importCam ) ;

                            ######################################
                            '''character alembic'''
                            ######################################
                            
                            filler ( ) ;

                            pm.text ( label = 'character cache :' ) ;
                            
                            ### get camera default path ###

                            currentChar = currentProj.split ( '/' ) [-2] ;
                            print currentChar;
                            defaultAlembicPath = currentProj + 'data/shotCache/' ;

                            for shotCache in shotCache_list :
                                if '.'+currentChar+'.' in shotCache :
                                    defaultAlembicPath += shotCache ;
                                else : pass ;
                            
                            with pm.rowColumnLayout ( nc = 2 , w = width*0.9 , cw = [ ( 1 , width*0.7 ) , ( 2 , width*0.2 ) ] ) :
                                pm.textField ( 'alembicPath_textField' , w = width*0.5 , tx = defaultAlembicPath ) ;
                                pm.button ( label = 'browse' , c = alembicPathBrowse_cmd ) ;

                            with pm.rowColumnLayout ( nc = 2 , w = width*0.9 , cw = [ ( 1 , width*0.45 ) , ( 2 , width*0.45 ) ] ) :
                                pm.checkBox ( 'alembicImportType_cbx' , label = 'blendShape' , v = True , enable = False  ) ;
                                pm.button ( label = 'import character cache' , c = importAlembic ) ;

                            ######################################
                            '''hair dyn'''
                            ######################################
                            filler ( ) ;

                            pm.text ( label = 'hair dyn cache (for xgen) :' ) ;
                            
                            defaultHairDynPath = currentProj + 'cache/alembic' ;

                            if os.path.exists ( defaultHairDynPath + '/DYN_ABC' ) == True :
                                defaultHairDynPath += '/DYN_ABC' ;
                            else : pass ;

                            with pm.rowColumnLayout ( nc = 2 , w = width*0.9 , cw = [ ( 1 , width*0.7 ) , ( 2 , width*0.2 ) ] ) :
                                pm.textField ( 'hairDynPath_textField' , w = width*0.5 , tx = defaultHairDynPath ) ;
                                pm.button ( label = 'browse' , c = hairDynPathBrowse_cmd ) ;

                            with pm.rowColumnLayout ( nc = 2 , w = width*0.9 , cw = [ ( 1 , width*0.45 ) , ( 2 , width*0.45 ) ] ) :
                                filler ( ) ;
                                pm.button ( label = 'import hair dyn' , c = importHairDyn ) ;

                            filler ( ) ;

                            pm.button ( label = 'import' , c = importCache_cmd ) ;

                        filler ( ) ;

                with pm.rowColumnLayout ( 'cache out' , w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] ) :
                    pass ;

    initializeTimeRange ( ) ;
    updateSetTimeRangeBtnClr ( ) ;
    updatePlayblastDefaultPath_cmd ( ) ;
    updatePlayblastName_cmd ( ) ;
    updatePlayblastVersionDisplay ( ) ;
    window.show ( ) ;

#simUtilities ( ) ;
#initializeTimeRange ( ) ;

def toShelf ( *args ) :

    import os ;

    self_path = os.path.realpath (__file__) ;
    file_name = os.path.basename (__file__) ;
    self_path = self_path.replace ( file_name , '' ) ;
    self_path = self_path.replace ( '\\' , '/' ) ;

    if self_path[-1] != '/' :
        self_path += '/' ;

    # self_path = D:/Dropbox/script/birdScript/projects/twoHeroes/

    image_path = self_path + 'media/simUtil_icon.png'

    commandHeader = '''
import sys ;

mp = "%s" ;
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
''' % self_path.split ('/sim/')[0] ;
# D:/Dropbox/script/birdScript

    cmd = commandHeader ;
    cmd += '''
import sim.simUtilities as sut ;
reload ( sut ) ;
sut.simUtilities ( ) ;
'''
    mel_cmd = '''
global string $gShelfTopLevel;
string $shelves = `tabLayout -q -selectTab $gShelfTopLevel`;
'''
    currentShelf = pm.mel.eval ( mel_cmd );
    pm.shelfButton ( style = 'iconOnly' , image = image_path , command = cmd , parent = currentShelf ) ;