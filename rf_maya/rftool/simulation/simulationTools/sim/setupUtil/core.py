import pymel.core as pm ;

import simulationTools.sim.setupUtil.gui as gui ;
reload ( gui ) ;

import simulationTools.sim.setupUtil.setup as setup ;
reload ( setup ) ;

class SimulationUtil ( gui.Gui , setup.Setup ) :

    def __init__ ( self ) :
        super ( SimulationUtil , self ).__init__() ;
        # gui.GUI.__init__ ( self ) ;
        #pm.button ( self.getEdges_btn , e = True , c = self.getEdgesBtn_cmd ) ;

def run ( *args ) :
    window = SimulationUtil() ;
    window.show() ;
