import pymel.core as pm ;

class PrerollConstraint ( object ) :

    def __init__ ( self ) :

        super ( PrerollConstraint , self ).__init__() ;

    def createConstraint ( self , dyn , col ) :

        dyn         = pm.PyNode ( dyn ) ;
        dynShape    = dyn.getShape() ;
        
        col = pm.PyNode ( col ) ;

        start   = pm.playbackOptions ( q = True , minTime = True ) ;
        end     = pm.playbackOptions ( q = True , maxTime = True ) ;

        dynName = dyn.nodeName().split('_')[0] ;
        colName = col.nodeName().split('_')[0] ;

        # get all vertex on dyn #
        dynVertex = pm.polyEvaluate ( dyn , vertex = True ) ;
        dynVertex = dyn + '.vtx[0:' + str ( dynVertex ) + ']' ; 

        pm.select ( dynVertex , col ) ;
        nConShape   = pm.mel.eval ( 'createNConstraint pointToSurface 0' ) [0] ;
        nConShape   = pm.PyNode ( nConShape ) ;
        nCon        = nConShape.getParent() ;
        nCon.rename ( dynName + '_' + colName + '_PrerollDyc' ) ; 

        nConShape_strength          = pm.listAttr ( nConShape.strength , m = True ) ;
        nConShape_tangentStrength   = pm.listAttr ( nConShape.tangentStrength , m = True ) ;

        # set nConShape attr keyframe #
        pm.setKeyframe ( nConShape , at = nConShape_strength , t = start + 10 , v = 20 ) ;
        pm.setKeyframe ( nConShape , at = nConShape_strength , t = start + 15 , v = 0 ) ;
        pm.setKeyframe ( nConShape , at = nConShape_tangentStrength , t = start + 10 , v = 10 ) ;
        pm.setKeyframe ( nConShape , at = nConShape_tangentStrength , t = start + 15 , v = 0 ) ;

        # getNCloth and NClothShape
        connection_list = pm.listConnections ( dynShape , type = 'nCloth' ) ;
        connection_list = list ( set ( connection_list ) ) ;

        nCloth      = connection_list[0] ;
        nClothShape = nCloth.getShape() ;

        nClothShape_collide = pm.listAttr ( nClothShape.collide , m = True ) ;
        nClothShape_selfCollide = pm.listAttr ( nClothShape.selfCollide , m = True ) ;

        pm.setKeyframe ( nClothShape , at = nClothShape_collide , t = start + 10 , v = 0 ) ;
        pm.setKeyframe ( nClothShape , at = nClothShape_collide , t = start + 11 , v = 1 ) ;

        pm.setKeyframe ( nClothShape , at = nClothShape_selfCollide , t = start + 15 , v = 0 ) ;
        pm.setKeyframe ( nClothShape , at = nClothShape_selfCollide , t = start + 16 , v = 1 ) ;

class Main ( PrerollConstraint ) :
    
    def __init__ ( self ) :
        super ( Main , self ).__init__() ;

def run ( *args ) :

    main = Main() ;

    selection_list = pm.ls ( sl = True ) ;

    dyn_list = selection_list [ : -1 ] ;
    col = selection_list [-1] ;

    for dyn in dyn_list :
        main.createConstraint ( dyn = dyn , col = col ) ;