class Cloth ( object ) :

	def __init__ ( self ) :
		super ( Cloth , self ).__init__() ;

	def createPrerollConstraint ( self , *args ) :
		import simulationTools.sim.cloth.createPrerollConstraint.core as cloth_createPrerollConstraint ;
		reload ( cloth_createPrerollConstraint ) ;
		cloth_createPrerollConstraint.run() ;