import pymel.core as pm ;

selection = pm.ls ( sl = True ) [0];
wrap_list = selection.getChildren() ;

for wrap in wrap_list :
    ori = wrap.replace ( '_Wrap' , '' ) ;
    
    try :
        pm.blendShape ( wrap , ori , origin = 'world' , weight = [ 0 , 1.0 ] , n = wrap + '_Bsn' ) ;
        print ( wrap + ' >> ' + ori ) ;
    
    except :
        print ( 'no operation for ' + wrap ) ;