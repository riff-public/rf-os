import pymel.core as pm ;

def printValue ( *args ) :
    value = pm.checkBoxGrp ( 'example_cbg' , q = True , va3 = True ) ;
    print value ;
    
w = 300.00 ;
exampleWindow = pm.window ( title = 'CheckBoxGroup Example' , w = w , h = 10 ) ;
pm.rowColumnLayout ( nc = 1 , w = w ) ;
pm.checkBoxGrp ( 'example_cbg' , ncb = 3 , labelArray3 = [ 'tx' , 'ty' , 'tz' ] , w = w ) ;
# ncb = numberOfCheckBoxes
pm.button ( label = 'Print Value' , c = printValue , w = w ) ;
exampleWindow.show() ;