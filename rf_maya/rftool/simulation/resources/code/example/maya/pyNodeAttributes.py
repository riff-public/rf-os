import pymel.core as pm

# make object a pyNode :
target = pm.general.PyNode ( 'pSphere1' ) ;

### attributes ###

target.attr ( '' ).lock() ;
target.attr ( '' ).unlock() ;
target.attr ( '' ).set() ;
target.attr ( '' ).get() ;

for axis in [ 'x' , 'y' , 'z' ] :
    target.attr( 's' + axis ).lock() ;
    target.attr( 's' + axis ).unlock() ;
    target.attr( 's' + axis ).set(10) ;
    print target.attr( 's' + axis ).get() ;

### connect / disconnect attributes ###

# connect 
master.tx >> slave.tx ;
# disconnect
master.tx // slave.tx

### add attribute ###
target.addAttr ( 'reverseAmpX' , keyable = True , attributeType = 'float' ) ;