import pymel.core as pm ;
import os ;
import re ;

def checkVersion ( name , path , incrementVersion = False , incrementSubVersion = False , *args  ) :
    # look for 'vxxx_xxx' e.g. 'v001_001'

    file_list = os.listdir ( path ) ;
    version_list = [] ;

    if not file_list :
        finalVersion = 'v001_001' ;

    else : 
        for file in file_list :
            if name in str ( file ) :
                version = re.findall ( 'v' + '[0-9]{3}' + '_' + '[0-9]{3}' , file ) ;
                if version :
                    version = version[0] ;
                    version_list.append ( version ) ;
    
        if not version_list :
            finalVersion = 'v001_001' ;
        else :
            version_list.sort() ;

            currentVersion = version_list[-1] ;
            version = currentVersion.split('_')[0] ;
            version = version.split('v')[-1] ;
            version = int ( version ) ;

            subVersion = currentVersion.split('_')[-1] ;
            subVersion = int ( subVersion ) ;

            if incrementVersion :
                version     += 1 ;
                subVersion   = 1 ;           

            elif incrementSubVersion :
                subVersion += 1 ;

            version     = str(version) ;
            subVersion  = str(subVersion) ;

            finalVersion = 'v' + version.zfill(3) + '_' + subVersion.zfill(3) ;
        
    return finalVersion ;