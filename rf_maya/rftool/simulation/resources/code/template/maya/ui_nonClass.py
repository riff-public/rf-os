import pymel.core as pm ;

# requestioning
def checkUniqueness ( ui ) :
    if pm.window ( ui , exists = True ) :
        pm.deleteUI ( ui ) ;
        checkUniqueness ( ui ) ;

def gui ( *args ) :

    ui      = 'gui_ui' ;
    width   = 500.00 ;
    title   = 'title' ;
    version = 1.0 ;

    # check ui duplication
    checkUniqueness ( ui ) ;

    window = pm.window ( ui ,
        title       = '{title} v{version}'.format ( title = title , version = version ) ,
        mnb         = True  , # minimize button
        mxb         = False , # maximize button 
        sizeable    = True  ,
        rtf         = True  , # resizeToFitChildren
        )

    # edit the window, so that the window size is refreshed every time it is called
    pm.window ( ui , e = True , w = width , h = 10 ) ;
    with window :

        with pm.rowColumnLayout ( nc = 1 , w = width ) :
            pass ;

    window.show() ;

gui ( ) ;