import pymel.core as pm ;

def getTopNode ( node , *args ) :
    if node.getParent() :
        return getTopNode ( node.getParent() ) ;
    else :
        return node ;