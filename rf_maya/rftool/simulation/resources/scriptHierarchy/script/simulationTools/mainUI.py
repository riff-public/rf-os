import pymel.core as pm ;

def printFeeIsAwesome ( *args ) :
    import simulationTools.sim.printFeeIsAwesome as printFeeIsAwesome ;
    reload ( printFeeIsAwesome ) ;
    printFeeIsAwesome.run() ;

class GUI ( object ) :

    def __init__ ( self ) :
        self.ui         = 'GUI_ui' ;
        self.width      = 250.00 ;
        self.title      = 'title' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def show ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                pm.button ( label = 'Print Fee Is Awesome' , w = w , c = printFeeIsAwesome ) ;

        window.show () ;

def run ( *args ) :
    gui = GUI() ;
    gui.show() ;