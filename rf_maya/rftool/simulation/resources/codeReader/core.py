import pymel.core as pm ;
import os ;

class CodeReader_GUI ( object ) :

    def __init__ ( self , textPath , title ) :

        self.textPath   = textPath ;
        self.text       = self.readTxt ( textPath ) ;
        self.ui         = 'textReader_ui' ;
        self.width      = 500.00 ;
        self.height     = 500.00 ;
        self.title      = title ;

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def readTxt ( self , path ) :
        file = open ( path , 'r' ) ;
        text = file.read ( ) ;
        file.close ( ) ;
        return text ;

    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def show ( self ) :

        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = self.title ,
            mnb         = True  ,
            mxb         = False ,
            sizeable    = True  ,
            rtf         = True  ,
            ) ;

        pm.window ( window , e = True , w = self.width , h = self.height ) ;
        with window :
    
            main_layout =  pm.formLayout ( ) ;    
            with main_layout :

                textScroll = pm.scrollField ( wordWrap = True , editable = True , text = self.text  ) ;

        pm.formLayout ( main_layout , e = True ,
            attachForm = [
                ( textScroll , 'left'   , 5 ) ,
                ( textScroll , 'right'  , 5 ) ,
                ( textScroll , 'top'    , 5 ) ,
                ( textScroll , 'bottom' , 5 ) ,
                ] ) ;
        
        window.show () ;

class CodeReader_Core ( object ) :

    def __init__ ( self ) :
        pass ;

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def getScriptPath ( self ) :
        
        selfPath    = os.path.realpath (__file__ ) ;
        fileName    = os.path.basename (__file__ ) ;
        selfPath    = selfPath.replace ( fileName , '' ) ;
        selfPath    = selfPath.replace ( '\\' , '/' ) ;
        scriptPath  = selfPath.split ( 'resources' ) [0] ;

        return scriptPath ;

    def getCodeList ( self ) :

        scriptPath  = self.getScriptPath() ;
        codePath    = scriptPath + 'resources/code/' ;

        type_list = os.listdir ( codePath ) ;
        layout_list = [] ;
        code_list   = [] ;
        
        for type in type_list :

            subType_list = os.listdir ( codePath + type + '/' ) ;
            layout_list.append ( [ type , subType_list ] ) ;

            for subType in subType_list :
                item_list = os.listdir ( codePath + type + '/' + subType + '/' ) ;

                for item in item_list :
                    code_list.append ( [ type , subType , item ] ) ;

        return [ layout_list , code_list ] ;
        #[['example', ['python']], ['template', []]]
        #[['example', 'python', 'requestioning.py']]

    def codeSnippetBrowse ( self , *args ) :
        os.startfile ( self.getScriptPath() + 'resources/code/' ) ;

    def insert ( self , width ) :

        height = 500.00 ;

        code_list = self.getCodeList () ;

        layout_list = code_list[0] ;
        text_list   = code_list[1] ;

        with pm.scrollLayout ( w = width , h = height ) :

            mainLayout = pm.rowColumnLayout ( nc = 1 , w = width ) ;
    
            pm.button ( label = 'Code Snippet' , width = width , height = 25 , bgc = ( 0.5 , 0 , 0 ) , c = self.codeSnippetBrowse ) ;
            # pm.text ( label = 'CODE SNIPPET' , font = 'boldLabelFont' , width = width , height = 25 , bgc = ( 0.5 , 0 , 0 ) ) ;

            for layout in layout_list :
                frameLayout = layout[0] ;
                cmd  = "{frameLayout}_frameLayout = ".format ( frameLayout = frameLayout ) ;
                cmd += "pm.frameLayout ( '{frameLayout}_frameLayout' , label = '{frameLayout}'.capitalize() , ".format ( frameLayout = frameLayout ) ;
                cmd += "parent = mainLayout , width = {width} , collapsable = True , collapse = False , bgc = ( 0.078 , 0.153 , 0.263 ) ) ;".format ( width = width ) ;
                exec ( cmd ) ;

                cmd  = "{frameLayout}_layout =".format ( frameLayout = frameLayout ) ;
                cmd += "pm.rowColumnLayout ( nc = 1 , w = width , parent = {frameLayout}_frameLayout ) ;".format ( frameLayout = frameLayout ) ;
                exec ( cmd ) ;

                subLayout_list = layout[1] ;
                for subLayout in subLayout_list :

                    cmd  = "{frameLayout}_{subLayout}_mainLayout = ".format ( frameLayout = frameLayout , subLayout = subLayout ) ;
                    cmd += "pm.rowColumnLayout ( '{frameLayout}_{subLayout}_mainLayout' ,".format ( frameLayout = frameLayout , subLayout = subLayout ) ;
                    cmd += "parent = {frameLayout}_layout , ".format ( frameLayout = frameLayout ) ;
                    cmd += "nc = 1 , w = {width} )".format ( width = width ) ;
                    exec ( cmd ) ;

                    pm.text ( label = subLayout.capitalize() , width = width , height = 25 , bgc = ( 0.144 , 0.243 , 0.459 ) ) ;

                    cmd  = ( "{frameLayout}_{subLayout}_layout = ".format ( frameLayout = frameLayout , subLayout = subLayout ) ) ;
                    cmd += ( "pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] , " ) ;
                    cmd += ( "parent = {frameLayout}_{subLayout}_mainLayout ) ;".format ( frameLayout = frameLayout , subLayout = subLayout ) ) ;
                    exec ( cmd ) ;

            for text in text_list :

                frameLayout = text[0] ;
                subLayout   = text[1] ;
                button      = text[2].split('.')[0] ;

                textPath = self.getScriptPath() ;
                textPath += 'resources/code/' ;
                textPath += text[0] + '/' ;
                textPath += text[1] + '/' ;
                textPath += text[2] ;

                cmd = '''
def {button}_call ( self , *args ) :

    codeReader_GUI = CodeReader_GUI ( textPath = '{textPath}' , title = '{title}' ) ;
    codeReader_GUI.show() ;
    
'''.format ( scriptPath = self.getScriptPath() ,
        button      = button ,
        textPath    = textPath ,
        title       = text[2] ) ;
                exec ( cmd ) ;

                cmd  = "{button}_btn = ".format ( button = button ) ;
                cmd += "pm.button ( '{button}_btn' , label = '{button}' , w = width/2 , c = {button}_call , ".format ( button = button ) ;
                cmd += "parent = {frameLayout}_{subLayout}_layout ) ;".format ( frameLayout = frameLayout , subLayout = subLayout ) ;
                exec ( cmd ) ; 
                
def run ( width , *args ) :
    codeReader = CodeReader_Core () ;
    codeReader.insert( width ) ;