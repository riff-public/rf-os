import pymel.core as pm ;
selection = pm.ls ( sl = True ) ;

pointList = [] ;

for each in selection :
    list = pm.xform ( each , q = True , rp = True , ws = True ) ;
    set = ( list[0] , list[1] , list[2] ) ;        
    pointList.append ( set ) ;

for each in selection[:1] :
    list = pm.xform ( each , q = True , rp = True , ws = True ) ;
    set = ( list[0] , list[1] , list[2] ) ;        
    pointList.append ( set ) ;

pm.curve ( d = 1 , p = pointList ) ;