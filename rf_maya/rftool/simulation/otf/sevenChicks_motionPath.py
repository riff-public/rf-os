import pymel.core as pm ;

class MotionPath ( object ) :
    
    def __init__ ( self ) :
        
        super ( MotionPath , self ).__init__() ;
    
        self.ctrlPoint = [ ( 18.197911 , -5.253284 , 0 ) , ( 0 , 10.506569 , 0 ) , ( -18.197911 , -5.253284 , 0 ) , ( 18.197911 , -5.253284 , 0 ) ] ;

    def createTfmAlongCV ( self , curve ) :

        curve       = pm.PyNode ( curve ) ;
        curveShape  = curve.getShape() ;
        name = curve.nodeName() ;

        degree  = pm.getAttr ( curveShape.nodeName() + '.degree' ) ;
        spans   = pm.getAttr ( curveShape.nodeName() + '.spans' ) ;
        cv      = degree + spans ;

        # Create Tfm(s) at CV(s)
        tfm_list        = [] ;
        crv1Grp_list    = [] ;
        crv2Grp_list    = [] ;
        ctrl_list       = [] ;
        joint_list      = [] ;

        for i in range ( 0 , cv ) :
            
            pos = pm.xform ( curve.nodeName() + '.cv[%d]' % i , q = True , ws = True , t = True ) ;
            tfm = pm.group ( em = True , w = True , n = name + '_' + str(i+1) + '_Zro_Grp' ) ;

            ctrl = pm.curve ( d = 1 , p = self.ctrlPoint ) ;
            ctrl.rename ( name + '_' + str(i+1) + '_Ctrl' ) ;
            ctrl_list.append ( ctrl ) ;
            pm.parent ( ctrl , tfm ) ;

            joint = pm.createNode ( 'joint' , n = name + '_' + str(i+1) + '_Jnt' ) ;
            joint_list.append ( joint ) ;
            pm.parent ( joint , ctrl ) ;

            crv1Grp = pm.group ( em = True , n = name + '_' + str(i+1) + '_Crv1_Grp' , p = tfm ) ;
            crv1Grp.tx.set ( -5 ) ;
            crv1Grp_list.append ( crv1Grp ) ;

            crv2Grp = pm.group ( em = True , n = name + '_' + str(i+1) + '_Crv2_Grp' , p = tfm ) ;
            crv2Grp.tx.set ( 5 ) ;
            crv2Grp_list.append ( crv2Grp ) ;

            pm.xform ( tfm , ws = True , t = pos ) ;
            tfm_list.append ( tfm ) ;

        driver_list = tfm_list[1:] + tfm_list[0:1] ;
        driven_list = tfm_list ;

        for i in range ( 0 , len ( driver_list ) -1 ) :
            aimCon = pm.aimConstraint ( driver_list[i] , driven_list[i] ,
                offset      = [ 0.0 , 0.0 , 0.0 ] ,
                weight      = 1 ,
                aimVector   = [ 0.0 , 0.0 , 1.0 ] ,
                upVector    = [ 0.0 , 1.0 , 0.0 ] ,
                worldUpType = 'scene' ,
                skip        = 'none'
                ) ;
            pm.delete ( aimCon ) ;

        aimCon = pm.aimConstraint ( driver_list[-3] , driven_list[-1] ,
            offset      = [ 0.0 , 0.0 , 0.0 ] ,
            weight      = 1 ,
            aimVector   = [ 0.0 , 0.0 , -1.0 ] ,
            upVector    = [ 0.0 , 1.0 , 0.0 ] ,
            worldUpType = 'scene' ,
            skip        = 'none'
            ) ;
        pm.delete ( aimCon ) ;

        return tfm_list , crv1Grp_list , crv2Grp_list , ctrl_list , joint_list ;

    def createRig ( self , curve ) :

        curve       = pm.PyNode ( curve ) ;
        curveShape  = curve.getShape() ;
        name = curve.nodeName() ;

        degree  = pm.getAttr ( curveShape.nodeName() + '.degree' ) ;
        spans   = pm.getAttr ( curveShape.nodeName() + '.spans' ) ;
        cv      = degree + spans ;

        if not pm.objExists ( name + '_MotionPath_Grp' ) :

            curveMotionPathGrp = pm.group ( em = True , n = name + '_MotionPath_Grp' ) ;

            product_list = self.createTfmAlongCV ( curve = curve ) ;

            tfm_list        = product_list[0] ;
            pm.parent ( tfm_list , curveMotionPathGrp ) ;

            crv1Grp_list    = product_list[1] ;
            crv2Grp_list    = product_list[2] ;

            ctrl_list   = product_list[3]
            joint_list  = product_list[4]

            crv1GrpPos_list = [] ;
            crv2GrpPos_list = [] ;

            for crv1Grp , crv2Grp in zip ( crv1Grp_list , crv2Grp_list ) :
                crv1GrpPos_list.append ( pm.xform ( crv1Grp , q = True , ws = True , t = True ) ) ;
                crv2GrpPos_list.append ( pm.xform ( crv2Grp , q = True , ws = True , t = True ) ) ;

            curve1 = pm.curve ( d = degree , p = crv1GrpPos_list ) ;
            curve1.rename ( name + '_Crv1' ) ;
            pm.parent ( curve1 , curveMotionPathGrp ) ;

            curve2 = pm.curve ( d = degree , p = crv2GrpPos_list ) ;
            curve2.rename ( name + '_Crv2' ) ;
            pm.parent ( curve2 , curveMotionPathGrp ) ;

            pm.delete ( crv1Grp_list ) ;
            pm.delete ( crv2Grp_list ) ;

            skinCluster = pm.skinCluster ( joint_list , curve ) ;
            skinCluster1 = pm.skinCluster ( joint_list , curve1 ) ;
            skinCluster2 = pm.skinCluster ( joint_list , curve2 ) ;

            for i in range ( 0 , cv ) :
                pm.skinPercent ( skinCluster , curve.nodeName() + '.cv[%d]' % i , transformValue = [ ( joint_list[i] , 1 ) ] ) ;
                pm.skinPercent ( skinCluster1 , curve1.nodeName() + '.cv[%d]' % i , transformValue = [ ( joint_list[i] , 1 ) ] ) ;
                pm.skinPercent ( skinCluster2 , curve2.nodeName() + '.cv[%d]' % i , transformValue = [ ( joint_list[i] , 1 ) ] ) ;

            return curve1 , curve2 , curveMotionPathGrp ;

    def attachAnimPathTfm ( self , curve ) :

        curve   = pm.PyNode ( curve ) ;
        name    = curve.nodeName() ;

        pathAnimTfm = pm.group ( em = True , w = True , n = name + '_PathAnim_Tfm' ) ;

        motionPath = pm.pathAnimation ( pathAnimTfm , curve ,
            fractionMode    = True , 
            follow          = True ,
            followAxis      = 'z' , 
            upAxis          = 'y' ,
            worldUpType     = "vector" ,
            worldUpVector   = ( 0.0 , 1.0 ,  0.0 ) ,
            inverseUp       = False ,
            inverseFront    = False ,
            bank            = True , 
            bankScale       = 1 ,
            bankThreshold   = 90 ,
            startTimeU      = pm.playbackOptions ( q = True , minTime = True ) ,
            endTimeU        = pm.playbackOptions ( q = True , maxTime = True ) ,
            ) ;

        return pathAnimTfm , motionPath ;

class GuiFunc ( object ) :

    def __init__ ( self ) :
        super ( GuiFunc , self ).__init__ () ;

    def ctrlGet_btn_cmd ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;

        text = '' ;

        for selection in selection_list :
            selection = pm.PyNode ( selection ) ;
            text += selection.nodeName() + ' , ' ;

        pm.textField ( self.ctrl_textField , e = True , tx = text ) ;

    def curveGet_btn_cmd ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;
        selection = selection_list[0] ;
        selection = pm.PyNode ( selection ) ;

        pm.textField ( self.curve_textField , e = True , tx = selection.nodeName() ) ;

    def run_btn_cmd ( self , *args ) :

        ctrl_list = pm.textField ( self.ctrl_textField , q = True , tx = True ).split ( ' , ' ) ;
        for ctrl in ctrl_list :
            if not ctrl :
                ctrl_list.remove ( ctrl ) ;

        curve = pm.textField ( self.curve_textField , q = True , tx = True ) ;
        curve = pm.PyNode ( curve ) ;
        name = curve.nodeName() ;

        product_list = self.createRig ( curve = curve ) ;

        curve1  = pm.PyNode ( product_list[0] ) ;
        name1   = curve1.nodeName() ;

        curve2  = pm.PyNode ( product_list[1] ) ;
        name2   = curve2.nodeName() ;
        
        curveMotionPathGrp = product_list[2] ;

        product_list = self.attachAnimPathTfm ( curve = curve ) ;
        pathAnimTfm = pm.PyNode ( product_list[0] ) ;
        motionPath  = pm.PyNode ( product_list[1] ) ;

        product_list = self.attachAnimPathTfm ( curve = curve1 ) ;
        pathAnimTfm1 = pm.PyNode ( product_list[0] ) ;
        motionPath1  = pm.PyNode ( product_list[1] ) ;

        product_list = self.attachAnimPathTfm ( curve = curve2 ) ;
        pathAnimTfm2 = pm.PyNode ( product_list[0] ) ;
        motionPath2  = pm.PyNode ( product_list[1] ) ;

        motionPath.uValue >> motionPath1.uValue ;
        motionPath.uValue >> motionPath2.uValue ;

        ### get zVector from pathAnimTfm
        vectorProductZ = pm.createNode ( 'vectorProduct' , n = name + '_VectorZ_Vp' ) ;
        vectorProductZ.operation.set ( 3 ) ;
        vectorProductZ.input1Z.set ( 1 ) ;

        pathAnimTfm1.worldMatrix >> vectorProductZ.matrix

        ### get xVector from pathAnimTfm1 , pathAnimTfm2
        vectorProductX = pm.createNode ( 'plusMinusAverage' , n = name + '_VectorX_Pma' ) ;
        vectorProductX.operation.set(2) ;

        pathAnimTfm1WorldMatrix = pm.createNode ( 'decomposeMatrix' , n = name1 + '_VectorX_Dmt' ) ;
        pathAnimTfm1.worldMatrix >> pathAnimTfm1WorldMatrix.inputMatrix ;
        pathAnimTfm1WorldMatrix.outputTranslate >> vectorProductX.i3.i3[0] ;

        pathAnimTfm2WorldMatrix = pm.createNode ( 'decomposeMatrix' , n = name2 + '_VectorX_Dmt' ) ;
        pathAnimTfm2.worldMatrix >> pathAnimTfm2WorldMatrix.inputMatrix ;
        pathAnimTfm2WorldMatrix.outputTranslate >> vectorProductX.i3.i3[1] ;
        # output3D

        ### get yVector
        vectorProductY = pm.createNode ( 'vectorProduct' , n = name + '_VectorY_Vp' ) ;
        vectorProductY.operation.set ( 2 ) ;
        vectorProductZ.output >> vectorProductY.input2 ;
        vectorProductX.output3D >> vectorProductY.input1 ;

        ### getAngleBetween 
        vectorProductY.output >> motionPath.worldUpVector ;
        motionPath.bank.set ( 1 ) ;
        motionPath.bankLimit.set ( 360 ) ;

class Gui ( object ) :

    def __init__ ( self ) :
        super ( Gui , self ).__init__ () ;

        self.ui         = 'motionPath_ui' ;
        self.width      = 350.00 ;
        self.title      = 'Motion Path Tool' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def showGui ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                pm.text ( label = 'Controller' ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/3*2 ) , ( 2 , w/3 ) ] ) :
                    self.ctrl_textField = pm.textField () ;
                    self.ctrlGet_btn    = pm.button ( label = 'Get' , c = self.ctrlGet_btn_cmd ) ;

                pm.separator ( h = 10 ) ;

                pm.text ( label = 'Motion Curve' ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/3*2 ) , ( 2 , w/3 ) ] ) :
                    self.curve_textField = pm.textField () ;
                    self.curveGet_btn    = pm.button ( label = 'Get' , c = self.curveGet_btn_cmd ) ;

                pm.separator ( h = 10 ) ;

                self.run_btn = pm.button ( label = 'Run' , w = w , c = self.run_btn_cmd ) ;

        window.show () ;

class Main ( Gui , GuiFunc , MotionPath ) :

    def __init__ ( self ) :
        super ( Main , self ).__init__ () ;

def run ( *args ) :
    gui = Main() ;
    gui.showGui() ;

run () ;