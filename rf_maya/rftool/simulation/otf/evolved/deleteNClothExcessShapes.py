import pymel.core as pm ;

selection = pm.ls ( sl = True ) [0] ;

shapes = selection.getShapes() ;
toDelete_list = [] ;

for each in shapes :
    if 'outputCloth' in str(each) :
        outputCloth = each ;
    else :
        toDelete_list.append ( each ) ;

pm.delete ( toDelete_list ) ;
outputCloth.rename ( selection.nodeName() + 'Shape')