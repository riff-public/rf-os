import pymel.core as pm ;

ori_list = pm.ls ( '*_ORI' , type = 'transform' ) ;
preSetup_list = pm.ls ( '*_PreSetup' , type = 'transform' ) ;

for ori , presetup in zip ( ori_list , preSetup_list ) :
  
    try :
    	oriShape 		= ori.getShape() ;
    	presetupShape 	= presetup.getShape() ;
       
    	degree 	= pm.getAttr ( oriShape + '.degree' ) ;
    	spans 	= pm.getAttr ( oriShape + '.spans' ) ;
    	cvNo 	= degree + spans ;
    
    	for i in range ( 0 , cvNo + 1 ) :
    		pos = pm.xform ( presetup + '.cv[%s]' % str(i) , q = True , ws = True , t = True ) ;
    		pm.xform ( ori + '.cv[%s]' % str(i) , ws = True , t = pos ) ;
    except :
        print ori , presetup ;