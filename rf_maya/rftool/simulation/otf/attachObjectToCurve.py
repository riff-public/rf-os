import pymel.core as pm ;

class Main ( AttachToCurve , Gui ) :

    def __init__ ( self ) :
        super ( Main , self ).__init__() ;

class AttachToCurve ( object ) :

    def __init__ ( self ) :
        pass ;

    def rebuildCurve ( self , curve ) :

        if (minValue != 0) or (maxValue != 1) :
            
            pm.rebuildCurve ( curve ,
                ch  = 1 ,
                rpo = 1 ,
                rebuildType = 0 ,
                end         = 1 ,
                keepRange           = 0 ,
                keepControlPoints   = 0 ,
                keepEndPoints       = 1 ,
                keepTangents        = 0 ,
                spans       = spans ,
                degree      = degree ,
                tolerance   = 0.01 ,
                ) ;
        
        else :
            pass ;

    def attach ( self ) :

        selection_list = pm.ls ( sl = True ) ;
        # [0:-1] = transform , #[-1] = curve

        transform_list = selection_list [0:-1] ;
        curve = selection_list [-1] ;
        curveShape = curve.getShape() ;

        minValue    = curveShape.minMaxValue.minValue.get() ;
        maxValue    = curveShape.minMaxValue.maxValue.get() ;
        degree      = pm.getAttr ( curveShape.nodeName()+ '.degree' ) ;
        spans       = curveShape.spans.get() ; 

        # rebuild curve 
        self.rebuildCurve ( curve ) ;

        aimGrp_list = [] ;
        rotGrp_list = [] ;

        for tfm in transform_list :

            tfmName = tfm.split('_')[0] ;

            npoc    = pm.shadingNode ( 'nearestPointOnCurve' , asUtility = True ) ;
            pos     = pm.xform ( tfm , q = True , rp = True , ws = True ) ;
            
            curveShape.worldSpace >> npoc.inputCurve ;
            npoc.inPosition.set ( pos ) ;

            parameter = npoc.result.parameter.get() ;
            pm.delete ( npoc ) ;

            poci = pm.shadingNode ( 'pointOnCurveInfo' , asUtility = True , n = tfmName + '_Poci' ) ; 

            curveShape.worldSpace >> poci.inputCurve ;
            poci.turnOnPercentage.set(1) ;
            poci.parameter.set ( parameter ) ;

            pociGrp     = pm.group ( em = True , n = tfmName + 'Poci_Grp' ) ;
            
            rotGrp      = pm.group ( em = True , n = tfmName + 'Rot_Grp' ) ;
            rotGrp_list.append ( rotGrp ) ; 
            
            aimGrp      = pm.group ( em = True , n = tfmName + 'Aim_Grp' ) ;
            aimGrp_list.append ( aimGrp ) ;
            
            pm.parent ( rotGrp , pociGrp ) ;
            pm.parent ( aimGrp , rotGrp ) ;
            poci.result.position >> pociGrp.t ;
            pm.parent ( tfm , rotGrp ) ;

        for driver, driven in zip ( aimGrp_list[1:] + aimGrp_list[0:1] , rotGrp_list ) :
            pm.aimConstraint ( driver , driven ) ;

class Gui ( object ) :

    def __init__ ( self ) :
        self.ui         = 'attachToCurve_ui' ;
        self.width      = 300.00 ;
        self.title      = 'Attach to Curve Tool' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def show ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                pass ;

        window.show () ;