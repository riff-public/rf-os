import pymel.core as pm ;

class MotionPath ( object ) :
    
    def __init__ ( self ) :
        
        super ( MotionPath , self ).__init__() ;
    
        self.ctrlPoint = [ ( 18.197911 , -5.253284 , 0 ) , ( 0 , 10.506569 , 0 ) , ( -18.197911 , -5.253284 , 0 ) , ( 18.197911 , -5.253284 , 0 ) ] ;

    def createCurveRigGroup ( self ) :
        # self.curveRigGroup

        groupName = self.curve.nodeName() + '_MotionPathRig_Grp' ;

        if not pm.objExists ( groupName ) :
            self.curveRigGroup = pm.group ( em = True , w = True , n = groupName ) ;
        else :
            self.curveRigGroup = pm.PyNode ( groupName ) ;

    def createNoTouchGroup ( self ) :
        # self.noTouchGroup
        
        groupName = self.curve.nodeName() + '_NoTouch_Grp' ;

        if not pm.objExists ( groupName ) :
            self.noTouchGroup = pm.group ( em = True , w = True , n = groupName ) ;
            self.noTouchGroup.v.set ( 0 ) ;
        else :
            self.noTouchGroup = pm.PyNode ( groupName ) ;

    def createTfmAlongCv ( self ) :

        ### Aim Axes Dict ###
        aimAxesDict = {} ;
        aimAxesDict['x'] = [ 1.0 , 0.0 , 0.0 ] ;
        aimAxesDict['y'] = [ 0.0 , 1.0 , 0.0 ] ;
        aimAxesDict['z'] = [ 0.0 , 0.0 , 1.0 ] ;

        invertAimAxesDict = {} ;
        invertAimAxesDict['x'] = [ -1.0 ,  0.0 ,  0.0 ] ;
        invertAimAxesDict['y'] = [  0.0 , -1.0 ,  0.0 ] ;
        invertAimAxesDict['z'] = [  0.0 ,  0.0 , -1.0 ] ;

        ### Create Tfm(s) at Cv(s) ###
        tfm_list        = [] ;
        crv1Grp_list    = [] ;
        crv2Grp_list    = [] ;
        ctrl_list       = [] ;
        joint_list      = [] ;

        for i in range ( 0 , ( self.cv + 1 ) , ( self.rebuildSmoothness ) )  :
            
            pos = pm.xform ( self.dupCurve.nodeName() + '.cv[%d]' % i , q = True , ws = True , t = True ) ;
            
            ### ZroGrp ###
            tfm = pm.group ( em = True , w = True , n = self.dupCurve.nodeName() + '_' + str(i+1) + '_Zro_Grp' ) ;

            ### Ctrl ###
            ctrl = pm.curve ( d = 1 , p = self.ctrlPoint ) ;
            ctrl.rename ( self.dupCurve.nodeName() + '_' + str(i+1) + '_Ctrl' ) ;
            ctrl_list.append ( ctrl ) ;
            pm.parent ( ctrl , tfm ) ;

            ### Joint ###
            joint = pm.createNode ( 'joint' , n = self.dupCurve.nodeName() + '_' + str(i+1) + '_Jnt' ) ;
            joint_list.append ( joint ) ;
            pm.parent ( joint , ctrl ) ;

            ### Crv1 Grp ###
            crv1Grp = pm.group ( em = True , n = self.dupCurve.nodeName() + '_' + str(i+1) + '_Crv1_Grp' , p = tfm ) ;
            crv1Grp.tx.set ( -5 ) ;
            crv1Grp_list.append ( crv1Grp ) ;

            ### Crv2 Grp ###
            crv2Grp = pm.group ( em = True , n = self.dupCurve.nodeName() + '_' + str(i+1) + '_Crv2_Grp' , p = tfm ) ;
            crv2Grp.tx.set ( 5 ) ;
            crv2Grp_list.append ( crv2Grp ) ;

            ### Snap Tfm to Cv ###
            pm.xform ( tfm , ws = True , t = pos ) ;
            tfm_list.append ( tfm ) ;

            ### Reference Tfm ###
            
            upVector = aimAxesDict [ self.upAxis ] ;

            if i != self.cv :
                refCv = i + 1 ;
                aimVector = aimAxesDict [ self.frontAxis ] ;                 
            else :
                refCv = i - 1 ;
                aimVector = invertAimAxesDict [ self.frontAxis ] ;

            refPos = pm.xform ( self.dupCurve.nodeName() + '.cv[%d]' % refCv , q = True , ws = True , t = True ) ;
            refGrp = pm.group ( em = True ) ;
            pm.xform ( refGrp , ws = True , t = refPos ) ;

            aimCon = pm.aimConstraint ( refGrp , tfm ,
                offset      = [ 0.0 , 0.0 , 0.0 ] ,
                weight      = 1 ,
                aimVector   = aimVector ,
                upVector    = upVector ,
                worldUpType = 'scene' ,
                skip        = 'none'
                ) ;
            
            pm.delete ( aimCon ) ;
            pm.delete ( refGrp ) ;

    def createCurveRig ( self ) :
        # *** goal : return MotionPathRig_Curve

        if not pm.objExists ( self.curve.nodeName() + '_MotionPathRig_Grp' ) :
            
            ### Preparing Groups ###            
            self.createCurveRigGroup() ;
            self.createNoTouchGroup() ;

            ### Duplicate Curve for Rebuild ###
            dupCurve = pm.duplicate ( self.curve ) [0] ;
            dupCurve = dupCurve.rename ( self.curve.nodeName() + '_MotionPathRig_Curve' ) ;
            self.dupCurve = pm.PyNode ( dupCurve ) ;

            self.dupCurveShape = self.dupCurve.getShape() ;
            self.name = self.dupCurve.nodeName() ;

            self.degree = pm.getAttr ( self.dupCurveShape.nodeName() + '.degree' ) ;
            self.spans  = pm.getAttr ( self.dupCurveShape.nodeName() + '.spans' ) ;
            self.cv     = self.degree + self.spans ;
        
            pm.parent ( self.curve , self.noTouchGroup ) ;
            pm.parent ( self.dupCurve , self.curveRigGroup ) ;

            # Getting the position of the original CV
            # Getting the position on the curve
            # Calculating smoothness
            # recreateCurve

            # ### Rebuild Duplicated Curve ###
            # pm.rebuildCurve ( self.dupCurve ,
            #     ch      = 0 ,
            #     rpo     = 1 , # replaceOriginal
            #     rt      = 0 , # rebuildType , 0 = uniform
            #     end     = 1 , # endKnots    , 1 = multiple end knots
            #     kr      = 0 , # keepRange
            #     kcp     = 0 , # keepControlPoints
            #     kep     = 1 , # keepEndPoints
            #     kt      = 0 , # keepTangents
            #     s       = self.spans * ( self.rebuildSmoothness + 1 ) , # spans
            #     d       = 3 ,
            #     tol     = 0.01 ,
            #     ) ;

            # ### Recalculate Duplicated Curve's CV ###
            # self.degree = pm.getAttr ( self.dupCurveShape.nodeName() + '.degree' ) ;
            # self.spans  = pm.getAttr ( self.dupCurveShape.nodeName() + '.spans' ) ;
            # self.cv     = self.degree + self.spans ;

            # self.createTfmAlongCv () ;

class GuiFunc ( object ) :

    def __init__ ( self ) :
        super ( GuiFunc , self ).__init__ () ;

    def ctrlGet_btn_cmd ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;

        text = '' ;

        for selection in selection_list :
            selection = pm.PyNode ( selection ) ;
            text += selection.nodeName() + ' , ' ;

        pm.textField ( self.ctrl_textField , e = True , tx = text ) ;

    def curveGet_btn_cmd ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;
        selection = selection_list[0] ;
        selection = pm.PyNode ( selection ) ;

        pm.textField ( self.curve_textField , e = True , tx = selection.nodeName() ) ;

    def getGuiInfo ( self , *args ) :

        ##################
        ### Query Ctrls ###
        ##################

        ctrl_textField_info = pm.textField ( self.ctrl_textField , q = True , tx = True ) ;
        ctrl_textField_info = ctrl_textField_info.split ( ' , ' ) ;
        self.ctrl_list = [] ;

        if ctrl_textField_info :
            for ctrl in ctrl_textField_info :
                if ctrl :
                    self.ctrl_list.append ( pm.PyNode ( ctrl ) ) ;

        ##################
        ### Query Motion Path ###
        ##################

        curve_textField_info = pm.textField ( self.curve_textField , q = True , tx = True ) ;
        self.curve = None ;
        if curve_textField_info :
            self.curve      = pm.PyNode ( curve_textField_info ) ;
            self.curveShape = self.curve.getShape() ;

        ##################
        ### Query Axis ###
        ##################
        axesDict    = {} ;
        axesDict[1] = 'x' ;
        axesDict[2] = 'y' ;
        axesDict[3] = 'z' ;

        ### Query Front Axis ###
        frontAxis_radioButton_info = pm.radioButtonGrp ( self.frontAxis_radioButton , q = True , select = True ) ;
        self.frontAxis = axesDict[frontAxis_radioButton_info] ;

        ### Query Up Axis ###
        upAxis_radioButton_info = pm.radioButtonGrp ( self.upAxis_radioButton , q = True , select = True ) ;
        self.upAxis = axesDict[upAxis_radioButton_info] ;

        ##################
        ### Query Rebuild Smoothness ###
        ##################

        self.rebuildSmoothness = pm.intField ( self.rebuildSmoothness_intField , q = True , v = True ) ;

    def attach_btn_cmd ( self , *args ) :

        self.getGuiInfo() ;

        if self.ctrl_list :

            for ctrl in self.ctrl_list :
            
                self.ctrl = pm.PyNode ( ctrl ) ;
                # attach ctrl to motionPath
                # goal : get original attribute value from control 

                self.createCurveRig () ;

class Gui ( object ) :

    def __init__ ( self ) :
        super ( Gui , self ).__init__ () ;

        self.ui         = 'motionPath_ui' ;
        self.width      = 350.00 ;
        self.title      = 'Motion Path Tool' ;
        self.version    = 2.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def null ( self ) :
        pm.text ( label = '' ) ;

    def showGui ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                pm.text ( label = '(c) RiFF Animation Studio 2018, Weerapot C.' ) ;
                
                pm.separator ( h = 10 ) ;

                pm.text ( label = 'Controller' ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/3*2 ) , ( 2 , w/3 ) ] ) :
                    self.ctrl_textField = pm.textField () ;
                    self.ctrlGet_btn    = pm.button ( label = 'Get' , c = self.ctrlGet_btn_cmd ) ;

                pm.separator ( h = 10 ) ;

                pm.text ( label = 'Motion Curve' ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/3*2 ) , ( 2 , w/3 ) ] ) :
                    self.curve_textField = pm.textField () ;
                    self.curveGet_btn    = pm.button ( label = 'Get' , c = self.curveGet_btn_cmd ) ;

                pm.separator ( h = 10 ) ;

                self.frontAxis_radioButton = pm.radioButtonGrp (
                    label = 'Front Axis : ' ,
                    labelArray3 = [ 'x' , 'y' , 'z' ] ,
                    numberOfRadioButtons = 3 ,
                    cw4 = [ w/4 , w/4 , w/4 , w/4 ] ,
                    select = 3 ,
                    ) ;

                self.upAxis_radioButton = pm.radioButtonGrp (
                    label = 'Up Axis : ' ,
                    labelArray3 = [ 'x' , 'y' , 'z' ] ,
                    numberOfRadioButtons = 3 ,
                    cw4 = [ w/4 , w/4 , w/4 , w/4 ] ,
                    select = 2 ,
                    ) ;

                with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , w/3/2 ) , ( 2 , w/3 ) , ( 3 , w/3 ) , ( 4 , w/3/2 ) ] ) :
                    
                    self.null() ;
                    
                    pm.text ( label = 'Rebuild Smoothness : ' , w = w/3 ) ;
                    self.rebuildSmoothness_intField = pm.intField ( w = w/3 , v = 3 ) ;
                    
                    self.null() ;

                pm.separator ( h = 10 ) ;

                self.attach_btn = pm.button ( label = 'Attach' , w = w , c = self.attach_btn_cmd ) ;
                self.attach_btn = pm.button ( label = 'Detach' , w = w ) ;

                ### Ctrl Scale ###

        window.show () ;

class Main ( Gui , GuiFunc , MotionPath ) :

    def __init__ ( self ) :
        super ( Main , self ).__init__ () ;

def run ( *args ) :
    gui = Main() ;
    gui.showGui() ;

run () ;