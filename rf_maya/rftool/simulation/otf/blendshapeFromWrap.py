'''
### how to ###
select wrap mesh ( with the suffix '_Wrap' ) ;
run the script ;
'''

import pymel.core as pm ;

suffix = '_Wrap' ;

selection_list = pm.ls ( sl = True ) ;

for item in selection_list :
    driver = item ;
    driven = pm.general.PyNode ( item.nodeName().split(suffix)[0] ) ;
    pm.blendShape ( driver , driven , origin = 'world' , weight = [ 0 , 1.0 ] , n = '%s_WrapBSn' % driver ) ;