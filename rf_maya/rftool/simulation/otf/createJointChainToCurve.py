import pymel.core as pm ;
import math ;

class CoreFunc ( object ) :

    def __init__ ( self ) :
        super ( CoreFunc , self ).__init__() ;

    def rebuildCurve ( self , curve ) :
        # Rebuild the curve so that it have min, max value of 0 to 1 - cleaner and more managable

        curve = pm.PyNode ( curve ) ;
        curveShape = curve.getShape() ;

        minValue    = curveShape.minMaxValue.minValue.get() ;
        maxValue    = curveShape.minMaxValue.maxValue.get() ;
        degree      = pm.getAttr ( curveShape.nodeName()+ '.degree' ) ;
        spans       = curveShape.spans.get() ; 

        if (minValue != 0) or (maxValue != 1) :
            
            pm.rebuildCurve ( curve ,
                ch                  = 1 ,
                rpo                 = 1 ,
                rebuildType         = 0 ,
                end                 = 1 ,
                keepRange           = 0 ,
                keepControlPoints   = 0 ,
                keepEndPoints       = 1 ,
                keepTangents        = 0 ,
                spans               = spans ,
                degree              = degree ,
                tolerance           = 0.01 ,
                ) ;

    def nearestPointOnCurve ( self , curve , transform ) :
        # return parameter

        curve       = pm.PyNode ( curve ) ;
        curveShape  = curve.getShape() ;
        
        transform   = pm.PyNode ( transform ) ;

        npoc    = pm.shadingNode ( 'nearestPointOnCurve' , asUtility = True ) ;
        pos     = pm.xform ( transform , q = True , rp = True , ws = True ) ;
        
        curveShape.worldSpace >> npoc.inputCurve ;
        npoc.inPosition.set ( pos ) ;

        parameter = npoc.result.parameter.get() ;
        pm.delete ( npoc ) ;

        return parameter ;

    def snapTransformToCurve ( self , curve , parameter ) :
        # return pos

        curve       = pm.PyNode ( curve ) ;
        curveShape  = curve.getShape() ;

        poci = pm.shadingNode ( 'pointOnCurveInfo' , asUtility = True ) ; 

        curveShape.worldSpace >> poci.inputCurve ;
        poci.turnOnPercentage.set(1) ;
        poci.parameter.set ( parameter ) ;

        pos = poci.result.position.get() ;
        
        pm.delete ( poci ) ;

        return pos ;

    def createJointChain ( self , curve , transform_list , segmentDivision , *args ) :

        curve = pm.PyNode ( curve ) ;
        
        ###########################
        ''' Rebuild the Curve '''
        ###########################
        self.rebuildCurve ( curve ) ;

        ###########################
        ''' Create Parameter List '''
        ###########################
        ### Finding nearest point on curve for each transform ###

        parameter_list = [] ;

        for transform in transform_list :
            parameter = self.nearestPointOnCurve ( curve = curve , transform = transform ) ;
            parameter_list.append ( [ transform , parameter ] ) ;

        parameter_list.sort ( key = lambda p : p[1] ) ;

        ###########################
        ''' Calculate Distance '''
        ###########################
        
        ### Calculate average segment distance ###        
        averageDistance = 0 ;

        for i in range ( 0 , len ( parameter_list ) -1 ) :
            averageDistance += ( parameter_list[i+1][1] - parameter_list[i][1] ) ;
        
        averageDistance /= ( len ( parameter_list ) - 1 ) ;
        #dParameter_list = [ [ 'dummyStart' , 0 ] ] +  parameter_list + [ [ 'dummyEnd' , 1 ] ] ;

        joint_list = [] ;

        ####################################
        ''' Start Segment '''
        transform       = parameter_list[0][0] ;
        transform       = pm.PyNode ( transform ) ;
        transformName   = transform.split('_')[0] ;

        startSegmentLength = parameter_list[0][1] ;
        startSegmentDivision = round ( startSegmentLength / ( averageDistance / (segmentDivision + 1) ) ) ;
        startSegmentDivision = int ( startSegmentDivision ) ;

        segmentDivisionLength = startSegmentLength / startSegmentDivision ;

        for i in range ( 0 , startSegmentDivision ) :
            jnt = pm.createNode ( 'joint' , name = transformName + '_S' + str ( i + 1 ) + '_PJnt' ) ;
            parameter =  0 + ( (i) * segmentDivisionLength ) ;
            pos = self.snapTransformToCurve ( curve = curve , parameter = parameter ) ;
            jnt.t.set ( pos ) ;
            
            joint_list.append ( jnt ) ;

        ####################################
        ''' Mid Segment '''
        
        for i in range ( 0 , len ( parameter_list ) ) :

            transform = parameter_list[i][0] ;
            transform = pm.PyNode ( transform ) ;
            transformName = transform.nodeName().split('_')[0] ;

            parameter = parameter_list[i][1] ;

            mainJnt = pm.createNode ( 'joint' , n = transformName + '_0_PJnt' ) ;
  
            pos = self.snapTransformToCurve ( curve = curve , parameter = parameter ) ;
            mainJnt.t.set ( pos ) ;
            mainJnt.radius.set ( 3 ) ;

            joint_list.append ( mainJnt ) ;

            # if this is the last of the transform, stop
            if i != len ( parameter_list ) - 1 :
                
                nextTransform = parameter_list[i+1][0] ;
                nextTransform = pm.PyNode ( nextTransform ) ;
                nextTransformName = nextTransform.nodeName().split('_')[0] ;

                nextParameter = parameter_list[i+1][1] ;

                curSegmentLength    = nextParameter - parameter ;
                curSegmentDivision  = round ( curSegmentLength / ( averageDistance / segmentDivision ) ) ;
                curSegmentDivision  = int ( curSegmentDivision ) ;

                curSegmentDivisionLength = curSegmentLength / ( curSegmentDivision + 1 ) ;

                for i in range ( 0 , curSegmentDivision ) :
                    jnt = pm.createNode ( 'joint' , name = transformName + '_' + str ( i + 1 ) + '_PJnt' ) ;
                    curParameter =  parameter + ( ( i + 1 ) * curSegmentDivisionLength ) ;
                    pos = self.snapTransformToCurve ( curve = curve , parameter = curParameter ) ;
                    jnt.t.set ( pos ) ;

                    joint_list.append ( jnt ) ;

        ####################################
        ''' End Segment '''
        transform       = parameter_list[-1][0] ;
        transform       = pm.PyNode ( transform ) ;
        transformName   = transform.split('_')[0] ;

        endSegmentLength = 1.0 - parameter_list[-1][1] ;
        endSegmentDivision = round ( endSegmentLength / ( averageDistance / (segmentDivision + 1) ) ) ;
        endSegmentDivision = int ( endSegmentDivision ) ;

        segmentDivisionLength = endSegmentLength / endSegmentDivision ;
        print segmentDivisionLength ;

        for i in range ( 0 , endSegmentDivision ) :
            jnt = pm.createNode ( 'joint' , name = transformName + '_E' + str ( i + 1 ) + '_PJnt' ) ;
            parameter =  parameter_list[-1][1] + ( (i+1) * segmentDivisionLength ) ;
            pos = self.snapTransformToCurve ( curve = curve , parameter = parameter ) ;
            jnt.t.set ( pos ) ;

            joint_list.append ( jnt ) ;

def test ( ) :
    selection_list = pm.ls ( sl = True ) ;
    curve = selection_list[-1] ;
    segmentDivision = 3 ;
    print curve ;
    print selection_list[:-1]
    ts = CoreFunc() ;
    ts.createJointChain ( curve = curve , transform_list = selection_list[:-1] , segmentDivision = 3 ) ;

test () ;