import pymel.core as pm ;

selection = pm.ls ( sl = True ) ;

driver = selection[-1] ;
driven_list = selection[:-1] ;

driverShape = driver.getShape() ;

for driven in driven_list :
    
    folName = driven.nodeName() + '_Fol' ;
    folShape = pm.createNode ( 'follicle' ) ;
    folTfm = folShape.getParent() ;
    folTfm.rename ( folName ) ;
    
    folShape.simulationMethod.set(0) ;

    folShape.outRotate >> folTfm.rotate ;
    folShape.outTranslate >> folTfm.translate ;
    
    driverShape.worldMatrix >> folShape.inputWorldMatrix ;
    driverShape.outMesh >> folShape.inputMesh ;
    
    cpm = pm.createNode ( 'closestPointOnMesh' ) ;
    driverShape.outMesh >> cpm.inMesh ;
    
    pos = pm.xform ( driven , q = True , rp = True , ws = True ) ;
    
    for val , axis in zip ( pos , [ 'X' , 'Y' , 'Z' ] ) :
        exec ( 'cpm.inPosition' + axis + '.set (' + str(val) + ')' ) ;
    
    uVal = cpm.result.parameterU.get() ;
    vVal = cpm.result.parameterV.get() ;
    
    folShape.parameterU.set(uVal) ;
    folShape.parameterV.set(vVal) ;
    
    pm.delete ( cpm ) ;
    
    pm.parentConstraint ( folTfm , driven , mo = True , skipRotate = 'none' , skipTranslate = 'none' ) ;
    