import pymel.core as pm ;

selection = pm.ls ( sl = True ) ;

driver = selection[-1] ;
driven_list = selection[:-1] ;

driverShape = driver.getShape() ;

for driven in driven_list :
    
    folName = driven.nodeName() + '_Fol' ;
    folShape = pm.createNode ( 'follicle' ) ;
    folTfm = folShape.getParent() ;
    folTfm.rename ( folName ) ;
    
    folShape.simulationMethod.set(0) ;

    folShape.outRotate >> folTfm.rotate ;
    folShape.outTranslate >> folTfm.translate ;
    
    driverShape.worldMatrix[0] >> folShape.inputWorldMatrix ;
    driverShape.local >> folShape.inputSurface ;
    
    cos = pm.createNode ( 'closestPointOnSurface' , n = driven + '_cos' ) ;
    driver.worldSpace[0] >> cos.inputSurface ;
    pos = pm.xform ( driven , q = True , rp = True , ws = True ) ;
    
    
    for val , axis in zip ( pos , [ 'X' , 'Y' , 'Z' ] ) :
        exec ( 'cos.inPosition' + axis + '.set (' + str(val) + ')' ) ;
    
    uVal = cos.result.parameterU.get() ;
    vVal = cos.result.parameterV.get() ;
    
    folShape.parameterU.set(uVal) ;
    folShape.parameterV.set(vVal) ;
    
    pm.delete ( cos ) ;
    
    pm.parent ( driven , folTfm ) ;