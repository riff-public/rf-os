import pymel.core as pm ;

class MotionPath ( object ) :
    
    def __init__ ( self ) :
        
        super ( MotionPath , self ).__init__() ;
    
        self.ctrlPoint = [ ( 18.197911 , -5.253284 , 0 ) , ( 0 , 10.506569 , 0 ) , ( -18.197911 , -5.253284 , 0 ) , ( 18.197911 , -5.253284 , 0 ) ] ;

    def addAttr ( self , node , attrName , attrType = 'float' ) :

        if not pm.attributeQuery ( attrName , node = node , exists = True ) :
            ctrlShape.addAttr ( attrName , keyable = True , attributeType = attributeType ) ;

    def addMotionPathAttr ( self , ctrl ) :

        ctrl = pm.PyNode ( ctrl ) ;
        ctrlShape = ctrl.getShape() ;

        if not pm.attributeQuery ( '__MotionPath__' , n = ctrlShape , exists = True ) :
            ctrlShape.addAttr ( '__MotionPath__' , keyable = True , attributeType = 'float' , lock = True  ) ;

            for attrName in [ 'uValue' , 'frontTwist' , 'upTwist' , 'sideTwist' ] :
                self.addAttr ( node = ctrlShape , attrName = attr ) ;

target.addAttr ( 'reverseAmpX' , keyable = True , attributeType = 'float' ) ;

    def createCurveRigGroup ( self ) :
        # self.curveRigGroup

        groupName = self.curve.nodeName() + '_MotionPathRig_Grp' ;

        if not pm.objExists ( groupName ) :
            self.curveRigGroup = pm.group ( em = True , w = True , n = groupName ) ;
        else :
            self.curveRigGroup = pm.PyNode ( groupName ) ;

    def createNoTouchGroup ( self ) :
        # self.noTouchGroup
        
        groupName = self.curve.nodeName() + '_NoTouch_Grp' ;

        if not pm.objExists ( groupName ) :
            self.noTouchGroup = pm.group ( em = True , w = True , n = groupName ) ;
            self.noTouchGroup.v.set ( 0 ) ;
        else :
            self.noTouchGroup = pm.PyNode ( groupName ) ;

    def createTfmAlongCv ( self ) :

        ### Aim Axes Dict ###
        aimAxesDict = {} ;
        aimAxesDict['x'] = [ 1.0 , 0.0 , 0.0 ] ;
        aimAxesDict['y'] = [ 0.0 , 1.0 , 0.0 ] ;
        aimAxesDict['z'] = [ 0.0 , 0.0 , 1.0 ] ;

        invertAimAxesDict = {} ;
        invertAimAxesDict['x'] = [ -1.0 ,  0.0 ,  0.0 ] ;
        invertAimAxesDict['y'] = [  0.0 , -1.0 ,  0.0 ] ;
        invertAimAxesDict['z'] = [  0.0 ,  0.0 , -1.0 ] ;

        ### Create Main Tfm(s) at Cv(s) ###
        tfm_list        = [] ;
        ctrl_list       = [] ;
        joint_list      = [] ;

        for i in range ( 0 , ( self.cv + 1 ) , ( self.rebuildSmoothness ) )  :
            
            pos = pm.xform ( self.curve.nodeName() + '.cv[%d]' % i , q = True , ws = True , t = True ) ;
            
            ### ZroGrp ###
            tfm = pm.group ( em = True , w = True , n = self.curve.nodeName() + '_' + str(i) + '_Zro_Grp' ) ;

            ### Ctrl ###
            ctrl = pm.curve ( d = 1 , p = self.ctrlPoint ) ;
            ctrl.rename ( self.curve.nodeName() + '_' + str(i+1) + '_Ctrl' ) ;
            ctrl_list.append ( ctrl ) ;
            pm.parent ( ctrl , tfm ) ;

            ### Joint ###
            joint = pm.createNode ( 'joint' , n = self.curve.nodeName() + '_' + str(i) + '_Jnt' ) ;
            joint_list.append ( joint ) ;
            pm.parent ( joint , ctrl ) ;

            ### Snap Tfm to Cv ###
            pm.xform ( tfm , ws = True , t = pos ) ;
            tfm_list.append ( tfm ) ;

            ### Reference Tfm ###

            upVector = aimAxesDict [ self.upAxis ] ;

            if i != self.cv :
                refCv = i + 1 ;
                aimVector = aimAxesDict [ self.frontAxis ] ;                 
            else :
                refCv = i - 1 ;
                aimVector = invertAimAxesDict [ self.frontAxis ] ;

            refPos = pm.xform ( self.curve.nodeName() + '.cv[%d]' % refCv , q = True , ws = True , t = True ) ;
            refGrp = pm.group ( em = True ) ;
            pm.xform ( refGrp , ws = True , t = refPos ) ;

            aimCon = pm.aimConstraint ( refGrp , tfm ,
                offset      = [ 0.0 , 0.0 , 0.0 ] ,
                weight      = 1 ,
                aimVector   = aimVector ,
                upVector    = upVector ,
                worldUpType = 'scene' ,
                skip        = 'none'
                ) ;
            
            pm.delete ( aimCon ) ;
            pm.delete ( refGrp ) ;

        self.tfm_list       = tfm_list ;
        self.ctrl_list      = ctrl_list ;
        self.joint_list     = joint_list ;

        ### Create Crv1/2 Tfm(s) at Cv(s) ###

        crv1Grp_list    = [] ;
        crv2Grp_list    = [] ;

        tmpTfm_list = []

        for i in range ( 0 , ( self.cv + 1 ) )  :

            pos = pm.xform ( self.curve.nodeName() + '.cv[%d]' % i , q = True , ws = True , t = True ) ;
            
            ### ZroGrp ###
            tmpTfm = pm.group ( em = True , w = True , n = self.curve.nodeName() + '_' + str(i) + '_Zro_Grp' ) ;

            ### Crv1 Grp ###
            crv1Grp = pm.group ( em = True , n = self.curve.nodeName() + '_' + str(i) + '_Crv1_Grp' , p = tmpTfm ) ;
            crv1Grp.tx.set ( -5 ) ;
            crv1Grp_list.append ( crv1Grp ) ;

            ### Crv2 Grp ###
            crv2Grp = pm.group ( em = True , n = self.curve.nodeName() + '_' + str(i) + '_Crv2_Grp' , p = tmpTfm ) ;
            crv2Grp.tx.set ( 5 ) ;
            crv2Grp_list.append ( crv2Grp ) ;

            ### Snap Tfm to Cv ###
            pm.xform ( tmpTfm , ws = True , t = pos ) ;
            tmpTfm_list.append ( tmpTfm ) ;

            ### Reference Tfm ###
            upVector = aimAxesDict [ self.upAxis ] ;

            if i != self.cv :
                refCv = i + 1 ;
                aimVector = aimAxesDict [ self.frontAxis ] ;                 
            else :
                refCv = i - 1 ;
                aimVector = invertAimAxesDict [ self.frontAxis ] ;

            refPos = pm.xform ( self.curve.nodeName() + '.cv[%d]' % refCv , q = True , ws = True , t = True ) ;
            refGrp = pm.group ( em = True ) ;
            pm.xform ( refGrp , ws = True , t = refPos ) ;

            aimCon = pm.aimConstraint ( refGrp , tmpTfm ,
                offset      = [ 0.0 , 0.0 , 0.0 ] ,
                weight      = 1 ,
                aimVector   = aimVector ,
                upVector    = upVector ,
                worldUpType = 'scene' ,
                skip        = 'none'
                ) ;
            
            pm.delete ( aimCon ) ;
            pm.delete ( refGrp ) ;

        crv1GrpPos_list = [] ;
        crv2GrpPos_list = [] ;

        for crv1Grp , crv2Grp in zip ( crv1Grp_list , crv2Grp_list ) :
            crv1GrpPos_list.append ( pm.xform ( crv1Grp , q = True , ws = True , t = True ) ) ;
            crv2GrpPos_list.append ( pm.xform ( crv2Grp , q = True , ws = True , t = True ) ) ;

        pm.delete ( crv1Grp_list ) ;
        pm.delete ( crv2Grp_list ) ;
        pm.delete ( tmpTfm_list ) ;

        self.crv1GrpPos_list = crv1GrpPos_list ;
        self.crv2GrpPos_list = crv2GrpPos_list ;

    def posToParameter ( self , curve , pos ) :
        # return parameter
        
        curve       = pm.PyNode ( curve ) ;
        curveShape  = curve.getShape() ;

        npoc    = pm.shadingNode ( 'nearestPointOnCurve' , asUtility = True ) ;
        
        curveShape.worldSpace >> npoc.inputCurve ;
        npoc.inPosition.set ( pos ) ;

        parameter = npoc.result.parameter.get() ;
        pm.delete ( npoc ) ;

        return parameter ;

    def parameterToPos ( self , curve , parameter ) :
        # return pos

        curve       = pm.PyNode ( curve ) ;
        curveShape  = curve.getShape() ;

        poci = pm.shadingNode ( 'pointOnCurveInfo' , asUtility = True ) ; 

        curveShape.worldSpace >> poci.inputCurve ;
        poci.parameter.set ( parameter ) ;

        pos = poci.result.position.get() ;
        
        pm.delete ( poci ) ;

        return pos ;

    def createCurveRig ( self ) :
        # reglobalize curve
        # globalise curve1 , curve2

        if not pm.objExists ( self.curve.nodeName() + '_MotionPathRig_Grp' ) :
            
            ### Preparing Groups ###            
            self.createCurveRigGroup() ;
            # self.createNoTouchGroup() ;

            ### Get ori Cv position ###

            oriCvPos_list = [] ;

            for i in range ( 0 , self.cv ) :
                targetCv = self.curve.nodeName() + '.cv[%d]' % i ;
                pos = pm.xform ( targetCv , q = True , ws = True , t = True ) ;
                oriCvPos_list.append ( pos ) ;

            ### Get new position from sectional proxy curve for more accuracy ###
            
            proxyPos_list = [] ;

            for i in range ( 0 , self.cv ) :

                ### proxy curve ###
                proxyCurvePoints = [] ;

                for operation in [ -3 , -2 , -1 , 0 , 1 , 2 , 3 ] :
                    value = i + operation ;
                    if ( value >= 0 ) and ( value <= ( self.cv -1 ) ) :
                        proxyCurvePoints.append ( oriCvPos_list[value] ) ;

                proxyCurve = pm.curve ( d = self.degree , p = proxyCurvePoints ) ;

                ### get parameter from proxy curve ###
                proxyPara   = self.posToParameter ( proxyCurve , oriCvPos_list[i] ) ;
                proxyPos    = self.parameterToPos ( proxyCurve , proxyPara ) ;
                proxyPos_list.append ( proxyPos ) ;

                pm.delete ( proxyCurve ) ;

            ### Getting the parameter on the original curve ###

            oriCvPara_list = [] ;

            for pos in proxyPos_list [:-1] :
                oriCvPara_list.append ( self.posToParameter ( curve = self.curve , pos = pos ) ) ;
            
            oriCvPara_list.append ( self.curveShape.maxValue.get() ) ;

            # Make parameter pair list, to calculate smoothness
            parameter1_list = oriCvPara_list[:-1] ;
            parameter2_list = oriCvPara_list[1:] ;

            # Make parameter_list for smoothness
            smoothParameter_list = [] ;

            for parameter1 , parameter2 in zip ( parameter1_list , parameter2_list ) :
                
                smoothParameter_list.append ( parameter1 )
                fraction = ( parameter2 - parameter1 ) / float ( self.rebuildSmoothness + 1 ) ;

                for i in range ( 1 , self.rebuildSmoothness + 1 ) :
                    smoothParameter_list.append ( parameter1 + ( fraction * i ) ) ;

            smoothParameter_list.append ( parameter2_list[-1] ) ;

            # Convert parameter back to pos
            smoothPos_list = [] ;

            for parameter in smoothParameter_list :
                smoothPos_list.append ( self.parameterToPos ( curve = self.curve , parameter = parameter ) ) ;

            # Recreate curve with smooth pos
            self.curve = pm.curve ( d = self.degree , p = smoothPos_list , n = self.curve.nodeName() + '_Rebuilt_Path' ) ;
            self.curveShape = self.curve.getShape() ;
            pm.parent ( self.curve , self.curveRigGroup ) ;

            self.degree = pm.getAttr ( self.curveShape.nodeName() + '.degree' ) ;
            self.spans  = pm.getAttr ( self.curveShape.nodeName() + '.spans' ) ;
            self.cv     = self.degree + self.spans ;

            # Create Tfm Along CV
            self.createTfmAlongCv () ;
            pm.parent ( self.tfm_list , self.curveRigGroup ) ;

            # Create Reference Curves
            self.curve1 = pm.curve ( d = self.degree , p = self.crv1GrpPos_list ) ;
            self.curve1.rename ( self.curve.nodeName() + '_Crv1' ) ;
            pm.parent ( self.curve1 , self.curveRigGroup ) ;

            self.curve2 = pm.curve ( d = self.degree , p = self.crv2GrpPos_list ) ;
            self.curve2.rename ( self.curve.nodeName() + '_Crv2' ) ;
            pm.parent ( self.curve2 , self.curveRigGroup ) ;

            skinCluster  = pm.skinCluster ( self.joint_list , self.curve ) ;
            skinCluster1 = pm.skinCluster ( self.joint_list , self.curve1 ) ;
            skinCluster2 = pm.skinCluster ( self.joint_list , self.curve2 ) ;

            # Paint Skin Weight
            for joint1 , joint2 in zip ( self.joint_list[:-1] , self.joint_list[1:] ) :
                cv1 = int ( joint1.split('_')[-2] ) ;
                cv2 = int ( joint2.split('_')[-2] ) ;
                
                weightVal = 1.0 / ( self.rebuildSmoothness + 1 ) ;
                
                counter = 0 ;

                for i in range ( cv1 , cv2 + 1 ) :

                        pm.skinPercent ( skinCluster , self.curve.nodeName() + '.cv[%d]' % i ,
                            transformValue = [
                                ( joint1 , 1 - ( weightVal * counter ) ) ,
                                ( joint2 , 0 + ( weightVal * counter ) ) ,
                                ] ) ;
                        pm.skinPercent ( skinCluster1 , self.curve1.nodeName() + '.cv[%d]' % i ,
                            transformValue = [
                                ( joint1 , 1 - ( weightVal * counter ) ) ,
                                ( joint2 , 0 + ( weightVal * counter ) ) ,
                                ] ) ;

                        pm.skinPercent ( skinCluster2 , self.curve2.nodeName() + '.cv[%d]' % i ,
                            transformValue = [
                                ( joint1 , 1 - ( weightVal * counter ) ) ,
                                ( joint2 , 0 + ( weightVal * counter ) ) ,
                                ] ) ;

                        counter += 1 ;

        else :

            self.curve = pm.PyNode ( self.curve.nodeName() + '_Rebuilt_Path' ) ;
            self.curveShape = self.curve.getShape() ;

            self.degree = pm.getAttr ( self.curveShape.nodeName() + '.degree' ) ;
            self.spans  = pm.getAttr ( self.curveShape.nodeName() + '.spans' ) ;
            self.cv     = self.degree + self.spans ;

            self.curve1 = pm.PyNode ( self.curve.nodeName() + '_Crv1' ) ;
            self.curve2 = pm.PyNode ( self.curve.nodeName() + '_Crv2' ) ;

    def attachAnimPathTfm ( self , curve , ctrl ) :

        ### Aim Axes Dict ###
        aimAxesDict = {} ;
        aimAxesDict['x'] = [ 1.0 , 0.0 , 0.0 ] ;
        aimAxesDict['y'] = [ 0.0 , 1.0 , 0.0 ] ;
        aimAxesDict['z'] = [ 0.0 , 0.0 , 1.0 ] ;

        invertAimAxesDict = {} ;
        invertAimAxesDict['x'] = [ -1.0 ,  0.0 ,  0.0 ] ;
        invertAimAxesDict['y'] = [  0.0 , -1.0 ,  0.0 ] ;
        invertAimAxesDict['z'] = [  0.0 ,  0.0 , -1.0 ] ;

        curve   = pm.PyNode ( curve ) ;
        ctrl    = pm.PyNode ( ctrl ) ;
        
        name    = ctrl.nodeName() + '_' + curve.nodeName() ;

        pathAnimTfm = pm.group ( em = True , w = True , n = name + '_PathAnim_Tfm' ) ;

        worldUpVector = aimAxesDict [ self.upAxis ] ;

        motionPath = pm.pathAnimation ( pathAnimTfm , curve ,
            fractionMode    = True , 
            follow          = True ,
            followAxis      = self.frontAxis , 
            upAxis          = self.upAxis ,
            worldUpType     = "vector" ,
            worldUpVector   = worldUpVector ,
            inverseUp       = False ,
            inverseFront    = False ,
            bank            = True , 
            bankScale       = 1 ,
            bankThreshold   = 90 ,
            startTimeU      = pm.playbackOptions ( q = True , minTime = True ) ,
            endTimeU        = pm.playbackOptions ( q = True , maxTime = True ) ,
            n               = name + '_PathAnim_MotionPath' ,
            ) ;
        
        return pathAnimTfm , motionPath ;

    def createVectorNetwork ( self ) :

        name = self.ctrl.nodeName() + '_' + self.curve.nodeName() ;

        if not pm.objExists ( name + '_PathAnim_Tfm' ) :

            product_list = self.attachAnimPathTfm ( curve = self.curve , ctrl = self.ctrl ) ;
            pathAnimTfm = pm.PyNode ( product_list[0] ) ;
            motionPath  = pm.PyNode ( product_list[1] ) ;

            product_list = self.attachAnimPathTfm ( curve = self.curve1 , ctrl = self.ctrl ) ;
            pathAnimTfm1 = pm.PyNode ( product_list[0] ) ;
            motionPath1  = pm.PyNode ( product_list[1] ) ;

            product_list = self.attachAnimPathTfm ( curve = self.curve2 , ctrl = self.ctrl ) ;
            pathAnimTfm2 = pm.PyNode ( product_list[0] ) ;
            motionPath2  = pm.PyNode ( product_list[1] ) ;

            motionPath.uValue >> motionPath1.uValue ;
            motionPath.uValue >> motionPath2.uValue ;

            name = self.ctrl.nodeName() + '_' + self.curve.nodeName() ;

            ### get frontVector from pathAnimTfm
            vectorProduct_front = pm.createNode ( 'vectorProduct' , n = name + '_VectorFront_Vp' ) ;
            vectorProduct_front.operation.set ( 3 ) ;
            vectorProduct_front.input1Z.set ( 1 ) ;

            pathAnimTfm1.worldMatrix >> vectorProduct_front.matrix

            ### get sideVector from pathAnimTfm1 , pathAnimTfm2
            vectorProduct_side = pm.createNode ( 'plusMinusAverage' , n = name + '_VectorSide_Pma' ) ;
            vectorProduct_side.operation.set(2) ;

            pathAnimTfm1WorldMatrix = pm.createNode ( 'decomposeMatrix' , n = name + '_VectorSide_Dmt' ) ;
            pathAnimTfm1.worldMatrix >> pathAnimTfm1WorldMatrix.inputMatrix ;
            pathAnimTfm1WorldMatrix.outputTranslate >> vectorProduct_side.i3.i3[0] ;

            pathAnimTfm2WorldMatrix = pm.createNode ( 'decomposeMatrix' , n = name + '_VectorSide_Dmt' ) ;
            pathAnimTfm2.worldMatrix >> pathAnimTfm2WorldMatrix.inputMatrix ;
            pathAnimTfm2WorldMatrix.outputTranslate >> vectorProduct_side.i3.i3[1] ;

            ### get upVector
            vectorProduct_up = pm.createNode ( 'vectorProduct' , n = name + '_VectorUp_Vp' ) ;
            vectorProduct_up.operation.set ( 2 ) ;
            vectorProduct_front.output >> vectorProduct_up.input2 ;
            vectorProduct_side.output3D >> vectorProduct_up.input1 ;

            ### Finalized ###
            vectorProduct_up.output >> motionPath.worldUpVector ;
            motionPath.bank.set ( 1 ) ;
            motionPath.bankLimit.set ( 360 ) ;

            self.vectorProduct_up = vectorProduct_up ;

class GuiFunc ( object ) :

    def __init__ ( self ) :
        super ( GuiFunc , self ).__init__ () ;

    def ctrlGet_btn_cmd ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;

        text = '' ;

        for selection in selection_list :
            selection = pm.PyNode ( selection ) ;
            text += selection.nodeName() + ' , ' ;

        pm.textField ( self.ctrl_textField , e = True , tx = text ) ;

    def curveGet_btn_cmd ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;
        selection = selection_list[0] ;
        selection = pm.PyNode ( selection ) ;

        pm.textField ( self.curve_textField , e = True , tx = selection.nodeName() ) ;

    def getGuiInfo ( self , *args ) :

        ##################
        ### Query Ctrls ###
        ##################

        ctrl_textField_info = pm.textField ( self.ctrl_textField , q = True , tx = True ) ;
        ctrl_textField_info = ctrl_textField_info.split ( ' , ' ) ;
        self.ctrl_list = [] ;

        if ctrl_textField_info :
            for ctrl in ctrl_textField_info :
                if ctrl :
                    self.ctrl_list.append ( pm.PyNode ( ctrl ) ) ;

        ##################
        ### Query Motion Path ###
        ##################

        curve_textField_info = pm.textField ( self.curve_textField , q = True , tx = True ) ;
        self.curve = None ;
        if curve_textField_info :
            self.curve      = pm.PyNode ( curve_textField_info ) ;
            self.curveShape = self.curve.getShape() ;

        self.degree = pm.getAttr ( self.curveShape.nodeName() + '.degree' ) ;
        self.spans  = pm.getAttr ( self.curveShape.nodeName() + '.spans' ) ;
        self.cv     = self.degree + self.spans ;

        ##################
        ### Query Axis ###
        ##################
        axesDict    = {} ;
        axesDict[1] = 'x' ;
        axesDict[2] = 'y' ;
        axesDict[3] = 'z' ;

        ### Query Front Axis ###
        frontAxis_radioButton_info = pm.radioButtonGrp ( self.frontAxis_radioButton , q = True , select = True ) ;
        self.frontAxis = axesDict[frontAxis_radioButton_info] ;

        ### Query Up Axis ###
        upAxis_radioButton_info = pm.radioButtonGrp ( self.upAxis_radioButton , q = True , select = True ) ;
        self.upAxis = axesDict[upAxis_radioButton_info] ;

        ##################
        ### Query Rebuild Smoothness ###
        ##################

        self.rebuildSmoothness = pm.intField ( self.rebuildSmoothness_intField , q = True , v = True ) ;

    def attach_btn_cmd ( self , *args ) :

        self.getGuiInfo() ;

        if self.ctrl_list :

            for ctrl in self.ctrl_list :
            
                self.ctrl = pm.PyNode ( ctrl ) ;
                


                self.createCurveRig () ;

                self.createVectorNetwork() ;

                # goal : get original attribute value from control 

class Gui ( object ) :

    def __init__ ( self ) :
        super ( Gui , self ).__init__ () ;

        self.ui         = 'motionPath_ui' ;
        self.width      = 350.00 ;
        self.title      = 'Motion Path Tool' ;
        self.version    = 2.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def null ( self ) :
        pm.text ( label = '' ) ;

    def showGui ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                pm.text ( label = '(c) RiFF Animation Studio 2018, Weerapot C.' ) ;
                
                pm.separator ( h = 10 ) ;

                pm.text ( label = 'Controller' ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/3*2 ) , ( 2 , w/3 ) ] ) :
                    self.ctrl_textField = pm.textField () ;
                    self.ctrlGet_btn    = pm.button ( label = 'Get' , c = self.ctrlGet_btn_cmd ) ;

                pm.separator ( h = 10 ) ;

                pm.text ( label = 'Motion Curve' ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/3*2 ) , ( 2 , w/3 ) ] ) :
                    self.curve_textField = pm.textField () ;
                    self.curveGet_btn    = pm.button ( label = 'Get' , c = self.curveGet_btn_cmd ) ;

                pm.separator ( h = 10 ) ;

                self.frontAxis_radioButton = pm.radioButtonGrp (
                    label = 'Front Axis : ' ,
                    labelArray3 = [ 'x' , 'y' , 'z' ] ,
                    numberOfRadioButtons = 3 ,
                    cw4 = [ w/4 , w/4 , w/4 , w/4 ] ,
                    select = 3 ,
                    ) ;

                self.upAxis_radioButton = pm.radioButtonGrp (
                    label = 'Up Axis : ' ,
                    labelArray3 = [ 'x' , 'y' , 'z' ] ,
                    numberOfRadioButtons = 3 ,
                    cw4 = [ w/4 , w/4 , w/4 , w/4 ] ,
                    select = 2 ,
                    ) ;

                with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , w/3/2 ) , ( 2 , w/3 ) , ( 3 , w/3 ) , ( 4 , w/3/2 ) ] ) :
                    
                    self.null() ;
                    
                    pm.text ( label = 'Rebuild Smoothness : ' , w = w/3 ) ;
                    self.rebuildSmoothness_intField = pm.intField ( w = w/3 , v = 3 ) ;
                    
                    self.null() ;

                pm.separator ( h = 10 ) ;

                self.attach_btn = pm.button ( label = 'Attach' , w = w , c = self.attach_btn_cmd ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :

                    pm.button ( label = 'Inverse Front' , w = w/2 ) ;
                    pm.button ( label = 'Inverse Up' , w = w/2 ) ;

                pm.separator ( h = 10 ) ;

                self.detach_btn = pm.button ( label = 'Detach' , w = w ) ;

                ### Ctrl Scale ###

        window.show () ;

class Main ( Gui , GuiFunc , MotionPath ) :

    def __init__ ( self ) :
        super ( Main , self ).__init__ () ;

def run ( *args ) :
    gui = Main() ;
    gui.showGui() ;

run () ;