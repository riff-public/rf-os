class Constraint ( object ) :
    
    def __init__ ( self ) :
        self.message = 'Dook is very Awesome' ;
    
    def printAwesome ( self ) :
        print ( self.message ) ;
    
    def printSuperAwesome ( self ) :
        print ( 'Super' ) ;
        self.printAwesome () ;
        
class AutoRig ( Constraint ) :
    
    def __init__ ( self ) :
        super ( AutoRig , self ).__init__ ( ) ;
        
ar = AutoRig() ;
ar.printSuperAwesome() ;
