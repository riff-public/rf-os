import MASH.api as mapi ;

mashNetwork = mapi.Network() ;
mashNetwork.createNetwork("cacheFreeze") ;
mashTime = mashNetwork.addNode("MASH_Time") ;

mash = pm.general.PyNode ( mashNetwork.waiter ) ;
mashDis = pm.general.PyNode ( mashNetwork.distribute ) ;
mashRepro = pm.general.PyNode ( mashNetwork.instancer ) ;
mashTime = pm.general.PyNode ( mashTime.name ) ;

mashDis.pointCount.set(1) ;
mashDis.arrangement.set(7) ;
# distribution type = initial state

