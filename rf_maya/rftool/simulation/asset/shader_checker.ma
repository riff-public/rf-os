//Maya ASCII 2017ff05 scene
//Name: checkerShader.ma
//Last modified: Wed, Oct 25, 2017 04:29:44 AM
//Codeset: 1252
requires maya "2017ff05";
requires "stereoCamera" "10.0";
requires "TurtleForMaya2009" "5.0.0.5";
requires "arkMayaExporter_2013_5" "1.21.ma3.s2.me5.a7";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
createNode lambert -n "checkerBlue_shd";
	rename -uid "55380F4A-4D3F-4A66-3834-0EB41A254D62";
createNode lambert -n "checkerGreen_shd";
	rename -uid "DAFFAFC9-4DA5-EC62-1360-A783D3CC97AB";
createNode lambert -n "checkerGrey_shd";
	rename -uid "CB45A16A-44D0-70B2-7064-EE81CDFB02B9";
createNode lambert -n "checkerPurple_shd";
	rename -uid "FD7C2F90-48E7-A1BC-1424-9297E62ECFA6";
createNode lambert -n "checkerRed_shd";
	rename -uid "D49DE35E-4397-39F6-5E5C-69A6F771FB10";
createNode lambert -n "checkerYellow_shd";
	rename -uid "7549EC5A-4CC6-AAFF-AC11-A4A936A24930";
createNode checker -n "checker2";
	rename -uid "4D870208-47E5-CF26-2137-5796AECED9BC";
	setAttr ".c1" -type "float3" 0 1 1 ;
	setAttr ".c2" -type "float3" 0 0.70020002 1 ;
createNode place2dTexture -n "place2dTexture2";
	rename -uid "B6A9DCF2-416D-8295-5FC4-08A11CA2A478";
	setAttr ".re" -type "float2" 4 4 ;
createNode checker -n "checker3";
	rename -uid "F3F7914F-429F-FD45-0F61-D5A7565B6E45";
	setAttr ".c1" -type "float3" 0.4156 1 0 ;
	setAttr ".c2" -type "float3" 0 0.64929998 0.066799998 ;
createNode place2dTexture -n "place2dTexture3";
	rename -uid "A9F2D40C-4B5D-7D9E-CC1C-DE8780E72964";
	setAttr ".re" -type "float2" 4 4 ;
createNode checker -n "checker6";
	rename -uid "7311AAE5-433A-727D-EE88-6D9C0E15B01F";
	setAttr ".c1" -type "float3" 0.88999999 0.88999999 0.88999999 ;
	setAttr ".c2" -type "float3" 0.45100001 0.45100001 0.45100001 ;
createNode place2dTexture -n "place2dTexture6";
	rename -uid "AE5E487D-4F78-6278-4726-4A91E178F02C";
	setAttr ".re" -type "float2" 4 4 ;
createNode checker -n "checker1";
	rename -uid "90E65E97-49F5-195F-CEFE-34928EB8CCAD";
	setAttr ".c1" -type "float3" 1 0 1 ;
	setAttr ".c2" -type "float3" 0.76599997 0 0.76599997 ;
createNode place2dTexture -n "place2dTexture1";
	rename -uid "091502C2-476E-7C7B-0A30-7E9D28E5DE46";
	setAttr ".re" -type "float2" 4 4 ;
createNode checker -n "checker5";
	rename -uid "C3293DA3-45F8-D834-D9C5-1CB11A49F55A";
	setAttr ".c1" -type "float3" 1 0.29180348 0.14499998 ;
	setAttr ".c2" -type "float3" 0.79220003 0.1234 0.2383 ;
createNode place2dTexture -n "place2dTexture5";
	rename -uid "5EE1C79F-42D0-1E27-D2ED-6BBA6D03AF17";
	setAttr ".re" -type "float2" 4 4 ;
createNode checker -n "checker4";
	rename -uid "4B52D10A-458D-F3C4-A518-229F8A07AD4D";
	setAttr ".c1" -type "float3" 1 1 0 ;
	setAttr ".c2" -type "float3" 0.81809998 0.65469998 0.1168 ;
createNode place2dTexture -n "place2dTexture4";
	rename -uid "3A56DE66-4013-2A22-2989-249752E92B17";
	setAttr ".re" -type "float2" 4 4 ;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 0;
	setAttr -av -k on ".unw";
	setAttr -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -k on ".ihi";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".tmr" 2048;
	setAttr -av ".aoam";
	setAttr -av ".aora";
	setAttr -av -k on ".mbsof";
	setAttr ".msaa" yes;
	setAttr ".aasc" 16;
	setAttr ".laa" yes;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 15 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 17 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 6 ".u";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
	setAttr -s 3 ".r";
select -ne :defaultTextureList1;
	setAttr -s 6 ".tx";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 13 ".dsm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -sn "shave_old_preRenderMel" -ln "shave_old_preRenderMel" -dt "string";
	addAttr -ci true -sn "shave_old_postRenderMel" -ln "shave_old_postRenderMel" -dt "string";
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey" -type "string" "exr";
	setAttr -k on ".gama";
	setAttr -k on ".an";
	setAttr -k on ".ar";
	setAttr -k on ".fs" 0.96;
	setAttr -k on ".ef" 9.6;
	setAttr -av -k on ".bfs";
	setAttr -k on ".me";
	setAttr -k on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -k on ".oft";
	setAttr -k on ".umfn";
	setAttr -k on ".ufe";
	setAttr -cb on ".pff";
	setAttr -k on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".rv";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mot";
	setAttr -av -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".mbso";
	setAttr -k on ".mbsc";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm" -type "string" "";
	setAttr -cb on ".pom" -type "string" "";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -k on ".rcp";
	setAttr -k on ".icp";
	setAttr -k on ".ocp";
	setAttr -cb on ".hbl";
	setAttr ".shave_old_preRenderMel" -type "string" "";
	setAttr ".shave_old_postRenderMel" -type "string" "";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :defaultColorMgtGlobals;
	setAttr ".cme" no;
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
connectAttr "checker2.oc" "checkerBlue_shd.c";
connectAttr "checker3.oc" "checkerGreen_shd.c";
connectAttr "checker6.oc" "checkerGrey_shd.c";
connectAttr "checker1.oc" "checkerPurple_shd.c";
connectAttr "checker5.oc" "checkerRed_shd.c";
connectAttr "checker4.oc" "checkerYellow_shd.c";
connectAttr "place2dTexture2.o" "checker2.uv";
connectAttr "place2dTexture2.ofs" "checker2.fs";
connectAttr "place2dTexture3.o" "checker3.uv";
connectAttr "place2dTexture3.ofs" "checker3.fs";
connectAttr "place2dTexture6.o" "checker6.uv";
connectAttr "place2dTexture6.ofs" "checker6.fs";
connectAttr "place2dTexture1.o" "checker1.uv";
connectAttr "place2dTexture1.ofs" "checker1.fs";
connectAttr "place2dTexture5.o" "checker5.uv";
connectAttr "place2dTexture5.ofs" "checker5.fs";
connectAttr "place2dTexture4.o" "checker4.uv";
connectAttr "place2dTexture4.ofs" "checker4.fs";
connectAttr "checkerPurple_shd.msg" ":defaultShaderList1.s" -na;
connectAttr "checkerBlue_shd.msg" ":defaultShaderList1.s" -na;
connectAttr "checkerGreen_shd.msg" ":defaultShaderList1.s" -na;
connectAttr "checkerYellow_shd.msg" ":defaultShaderList1.s" -na;
connectAttr "checkerRed_shd.msg" ":defaultShaderList1.s" -na;
connectAttr "checkerGrey_shd.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture3.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture4.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture5.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture6.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "checker1.msg" ":defaultTextureList1.tx" -na;
connectAttr "checker2.msg" ":defaultTextureList1.tx" -na;
connectAttr "checker3.msg" ":defaultTextureList1.tx" -na;
connectAttr "checker4.msg" ":defaultTextureList1.tx" -na;
connectAttr "checker5.msg" ":defaultTextureList1.tx" -na;
connectAttr "checker6.msg" ":defaultTextureList1.tx" -na;
// End of checkerShader.ma
