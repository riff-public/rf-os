import maya.cmds as mc ;
import sys ;

mp = r"O:\Pipeline\core\rf_maya\rftool\simulation" ;
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;

import model.udim_u1v1_converter.ui as uuc ;
reload ( uuc ) ;

uuc_ui = uuc.UI();
uuc_ui.run();