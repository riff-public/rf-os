import pymel.core as pm ;
import maya.cmds as mc ;
import os ;

import model.udim_u1v1_converter.core as uuc ;
reload ( uuc ) ;

class UI ( object ) :

    def __init__ ( self ) :
        self.title      = 'udim u1v1 convertor' ;
        self.version    = 1.0
        self.width      = 400.00 ;

    class inputPathField ( object ) :

        def __init__ ( self ) :
            self.inputPathField_pnt = 'inputPath_textField' ;

        def insert ( self ) :
            pm.textField ( self.inputPathField_pnt , text = '...' ) ;

        def getPath ( self ) :
            return pm.textField ( self.inputPathField_pnt , q = True , text = True ) ;

        def setPath ( self , path ) :
            pm.textField ( self.inputPathField_pnt , e = True , text = path ) ;

    def run ( self ) :

        # check if window exists
        if pm.window ( 'uuc_ui' , exists = True ) :
            pm.deleteUI ( 'uuc_ui' ) ;
        else : pass ;

        window = pm.window ( 'uuc_ui',
            title       = '%s %s' % ( self.title , self.version ) ,
            mnb         = True ,
            mxb         = False ,
            sizeable    = True ,
            rtf         = True ) ;
        pm.window ( window , e = True , w = self.width )
        with window :

            ### mainLayout ###
            with pm.rowColumnLayout ( nc = 1 ) :

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , self.width/4*3 ) , ( 2 , self.width/4 ) ] ) :
                    
                    inputPathField = self.inputPathField ( ) ;
                    inputPathField.insert ( ) ;
                    
                    pm.button ( label = 'browse' , c = self.browsePath_cmd ) ;

                pm.separator ( h = 10 ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , self.width/2 ) , ( 2 , self.width/2 ) ] ) :

                    pm.button ( label = 'udim to u1v1' , c = self.udim_to_u1v1 ) ;
                    pm.button ( label = 'u1v1 to udim' , c = self.u1v1_to_udim ) ;

        window.show () ;

    def browsePath_cmd ( self , *args ) :
        inputPathField  = self.inputPathField ( ) ;
        startPath       = inputPathField.getPath (  ) ;
        
        if startPath == '...' : startPath = '/D:' ;

        browsePath = mc.fileDialog2 ( ds = 2 , fm = 3 , startingDirectory = startPath , okc = 'select directory' ) [0] ;
        if browsePath[-1] != '/' : browsePath += '/' ;

        inputPathField.setPath ( browsePath ) ;

    def udim_to_u1v1 ( self , *args ) :
        inputPathField  = self.inputPathField ( ) ;
        targetPath  = inputPathField.getPath (  ) ;
        path        = uuc.Path ( targetPath ) ;
        path.udim_to_u1v1 ( ) ;

    def u1v1_to_udim ( self , *args ) :
        inputPathField  = self.inputPathField ( ) ;
        targetPath  = inputPathField.getPath (  ) ;
        path        = uuc.Path ( targetPath ) ;
        path.u1v1_to_udim ( ) ;