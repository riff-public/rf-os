import os ;
import re ;

class Path ( object ) :

    def __init__ ( self , path ) :
        self.path = path ;

    def __str__ ( self ) :
        return self.path ;

    def __repr__ ( self ) :
        return self.path ;

    def listdir ( self ) :
        itemList = os.listdir ( self.path ) ;
        itemList.sort ( ) ;
        return itemList ;

    def udim_to_u1v1 ( self ) :

        itemList = self.listdir ( ) ;

        for item in itemList :

            try :
        
                currentNumber = re.findall ( '[0-9]{4}' , item ) [0] ;

                v = currentNumber[1:-1]
                v = int ( v ) + 1 ;
                
                u = currentNumber[-1] ;
                u = int ( u ) ;
                
                if u == 0 :
                    u = 10 ;
                    v -= 1 ;

                currentUV = '_u' + str(u) + '_v' + str(v) ;

                targetPath = os.path.join ( self.path , item ) ;
                resultPath = targetPath.replace ( currentNumber , currentUV );
        
                os.rename ( targetPath , resultPath ) ;

            except : pass ;

    def u1v1_to_udim ( self ) :

        itemList = self.listdir ( ) ;
       
        for item in itemList :

            try :
                
                currentUV = re.findall ( 'u' + '[0-9]{1,2}' + '_' + 'v' + '[0-9]{1,2}' , item ) [0] ;

                u = currentUV.split('_') [0] ;
                u = re.findall ( '[0-9]{1,2}' , u ) [0] ;
                u = int(u) ;

                v = currentUV.split('_') [1] ;
                v = re.findall ( '[0-9]{1,2}' , v ) [0] ;
                v = int(v) ;

                if u == 10 :
                    u = 0 ;
                    v += 1 ;

                v = v - 1 ;

                currentNumberFnt = '1' ;
                currentNumberMid = str(v).zfill(2) ;
                currentNumberBck = str(u) ;

                currentNumber = currentNumberFnt + currentNumberMid + currentNumberBck ;

                targetPath = os.path.join ( self.path , item ) ;
                resultPath = targetPath.replace ( '_' + currentUV , currentNumber );
        
                os.rename ( targetPath , resultPath ) ;
            
            except :

                pass ;

# path = r'C:\Users\weerapot.c\Desktop\rename\AO' ;

# path_path = Path (path) ;
# path_path.u1v1_to_udim ();