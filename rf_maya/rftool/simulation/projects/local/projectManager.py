from pymel.core import * ;
import os , re , shutil ;

#################################################################################################################################

def createFolder ( directory = '' , element = '' , *args ) :

    if os.path.exists ( directory ) == True :
        error ( element + ' ' + 'already exist, please check' )
    else :
        os.makedirs ( directory , *args ) ;

def selectionError ( selection = '' ) :
    error ( 'please select' + ' ' + selection + ' ' + 'to continue' ) ;

def getPreferencePath ( *args ) :
    myDocuments = os.path.expanduser('~') ;
    myDocuments = myDocuments.replace ( '\\' , '/' ) ;
    # C:/Users/Legion/Documents
    
    prefPath = myDocuments + '/birdScriptPreferences/projectManager'

    return prefPath ;

def getScriptPath ( *args ) :

    selfPath = os.path.realpath (__file__) ;
    selfName = os.path.basename (__file__) ;
    selfPath = selfPath.replace ( selfName , '' ) ;
    selfPath = selfPath.replace ( '\\' , '/' ) ;
    # C:\Users\Legion\Desktop\birdScript\projects\local

    scriptPath = selfPath.split ( 'birdScript' ) [ 1 ] ;
    scriptPath = selfPath.replace ( scriptPath , '') ;

    return scriptPath ;

#################################################################################################################################

def updatePath ( *args ) :

    path = textField ( 'userDirectory' , q = True , text = True ) ;

    if os.path.exists ( path ) == True :
        updateProjectList ( directory = path ) ;
    else : pass ;

def defaultPath ( *args ) :
    
    prefPath = getPreferencePath ( ) ;
    
    if os.path.exists ( prefPath ) == False :
        os.makedirs ( prefPath ) ;
    else: pass ;
    
    defaultPathTxt = open ( prefPath + '/defaultPath.txt' , 'a+' ) ;
    defaultPath = defaultPathTxt.read ( ) ;
    defaultPathTxt.close ( ) ;
    
    textField ( 'userDirectory' , e = True , text = defaultPath ) ;
    
    if os.path.exists ( defaultPath ) == True :
        updateProjectList ( directory = defaultPath ) ;
        
    else : pass ;

def defaultProject ( *args ) :

    prefPath = getPreferencePath ( ) ;
    
    if os.path.exists ( prefPath ) == False :
        os.makedirs ( prefPath ) ;
    else: pass ;
    
    defaultProjectTxt = open ( prefPath + '/defaultProject.txt' , 'a+' ) ;
    defaultProject = defaultProjectTxt.read ( ) ;
    defaultProjectTxt.close ( ) ;
    
    projectList = textScrollList ( 'projectList' , q = True , allItems = True ) ;

    if defaultProject in projectList :
        textScrollList ( 'projectList' , e = True , selectItem = defaultProject ) ;
    else : pass ;

def saveWorkingDirectory ( *args ) :
    
    prefPath = getPreferencePath ( ) ;
    
    userDirectory = textField ( 'userDirectory' , q = True , text = True ) ;
    
    defaultPathTxt = open ( prefPath + '/defaultPath.txt' , 'w+' ) ;
    defaultPathTxt.write ( userDirectory ) ;
    defaultPathTxt.close ( ) ;

def saveProject ( *args ) :
    
    prefPath = getPreferencePath ( ) ;
    
    try : project = textScrollList ( 'projectList' , q = True , selectItem = True ) [0] ;
    except : project = None ;
    else : pass ;

    if project != None :
        defaultProjectTxt = open ( prefPath + '/defaultProject.txt' , 'w+' ) ;
        defaultProjectTxt.write ( project ) ;
        defaultProjectTxt.close ( ) ;
        saveWorkingDirectory ( ) ;
    else : pass ;

def updateProjectList ( directory , *args ) :

    allItems = os.listdir ( directory ) ;
    projects = [ p for p in os.listdir( directory ) if os.path.isdir ( os.path.join ( directory , p ) ) ] ;
    projects.sort ( ) ;

    textScrollList ( 'projectList' , e = True , ra = True ) ;
    
    for project in projects :
        textScrollList ( 'projectList' , e = True , append = project ) ;

    directory = directory.replace ( '\\' , '/' ) ;

    directoryTempt = directory.split ( '/' ) [ -1 ] ;
    if directoryTempt == '' :
        directoryTempt = directory.split ( '/' ) [ -2 ] ;
    else : pass ;
    directory = directoryTempt ;

    frameLayout ( 'selectProjectLayout' , e = True , label = 'PROJECTS in [ %s ]' % directory ) ;
    
def browsePath ( *args ) :

    startingDirectory = textField ( 'userDirectory' , q = True , text = True ) ;

    userDirectory = fileDialog2 ( bbo = 1 , fm = 3 , ff = '' , startingDirectory = startingDirectory ,
        okc = 'select directory' ) [0] ;
    
    textField ( 'userDirectory' , e = True , text = userDirectory ) ;
    
    allItems = os.listdir ( userDirectory ) ;
   
    updateProjectList ( directory = userDirectory ) ;

def createProject ( *args ) :
    userDirectory = textField ( 'userDirectory' , q = True , text = True ) ;
    newProjectname = textField ( 'projectInput' , q = True , text = True ) ;
    newProjectPath = userDirectory + '/' + newProjectname ;
    
    if os.path.exists ( newProjectPath ) == True :
        error ( 'project directory already exist, please check' ) ;
    else :
        os.makedirs ( newProjectPath ) ;

        os.makedirs ( newProjectPath + '/' + '_fromClient' ) ;
        os.makedirs ( newProjectPath + '/' + '_toClient' ) ;
        os.makedirs ( newProjectPath + '/' + 'scene' ) ;
        os.makedirs ( newProjectPath + '/' + 'asset/char' ) ;

        updateProjectList ( userDirectory ) ;

#################################################################################################################################

def tabChangeCommand ( *args ) :
    projectDirectory = textField ( 'userDirectory' , q = True , text = True ) ;
    selectedProject = textScrollList ( 'projectList' , q = True , selectItem = True ) [0] ;
    currentProject =  projectDirectory + '/' + selectedProject ;
    
    prefPath = getPreferencePath ( ) ;
    
    if os.path.exists ( currentProject ) == True :

        currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'w+' ) ;
        currentProjectTxt.write ( currentProject ) ;
        currentProjectTxt.close ( ) ;

        saveWorkingDirectory ( ) ;
        saveProject ( ) ;
        
        saveDefaultSelectedShot ( ) ;
        saveDefaultSelectedChar ( ) ;

        updateInfo ( ) ;

    else : pass ;

#################################################################################################################################

def saveDefaultSelectedShot ( *args ) :
    
    prefPath = getPreferencePath ( ) ;

    defaultSelection = '' ;

    film = textScrollList ( 'filmScrollList' , q = True , selectItem = True ) ;
    if film == [] :
        pass ;
    else :
        defaultSelection += film[0] ;

        sequence = textScrollList ( 'sequenceScrollList' , q = True , selectItem = True  ) ;
        if sequence == [] :
            pass ;
        else :
            defaultSelection += '.' + sequence[0] ;

            shot = textScrollList ( 'shotScrollList' , q = True , selectItem = True ) ;
            if shot == [] :
                pass ;
            else :
                defaultSelection += '.' + shot[0] ;

                shotChar = textScrollList ( 'shotCharScrollList' , q = True , selectItem = True ) ;
                if shotChar == [] :
                    pass ;
                else :
                    defaultSelection += '.' + shotChar[0] ;

    defaultSelectedShotTxt = open ( prefPath + '/defaultSelectedShot.txt' , 'w+' ) ;
    defaultSelectedShotTxt.write ( defaultSelection ) ;
    defaultSelectedShotTxt.close ( ) ;

def selectDefaultSelectedShot ( *args ) :

    prefPath = getPreferencePath ( ) ;

    defaultSelectedShotTxt = open ( prefPath + '/defaultSelectedShot.txt' , 'a+' ) ;
    defaultSelectedShot = defaultSelectedShotTxt.read ( ) ;
    defaultSelectedShotTxt.close ( ) ;
    defaultSelectedShot = defaultSelectedShot.split ( '.' ) ;

    if len ( defaultSelectedShot ) == 0 :
        updateFilmList ( ) ; updateSequenceList ( ) ; updateShotList ( ) ; updateShotCharList ( ) ;
    else :
        updateFilmList ( ) ;
        textScrollList ( 'filmScrollList' , e = True , selectItem = defaultSelectedShot [0] ) ;

        if len ( defaultSelectedShot ) == 1 :
            updateSequenceList ( ) ; updateShotList ( ) ; updateShotCharList ( ) ;
        else :
            updateSequenceList ( ) ;
            textScrollList ( 'sequenceScrollList' , e = True , selectItem = defaultSelectedShot [1] ) ;

            if len ( defaultSelectedShot ) == 2 :
                updateShotList ( ) ; updateShotCharList ( ) ;
            else :
                updateShotList ( ) ;
                textScrollList ( 'shotScrollList' , e = True , selectItem = defaultSelectedShot [2] ) ;

                if len ( defaultSelectedShot ) == 3 :
                    updateShotCharList ( ) ;
                else :
                    updateShotCharList ( ) ;
                    textScrollList ( 'shotCharScrollList' , e = True , selectItem = defaultSelectedShot [3] ) ;

def saveDefaultSelectedChar ( *args  ) :

    prefPath = getPreferencePath ( ) ;

    char = textScrollList ( 'characterScrollList' , q = True , selectItem = True ) ;

    if char == [] :
        char = '' ;
    else :
        char = char [0] ;

    defaultSelectedCharTxt = open ( prefPath + '/defaultSelectedChar.txt' , 'w+' ) ;
    defaultSelectedCharTxt.write ( char ) ;
    defaultSelectedCharTxt.close ( ) ;

def selectDefaultSelectedChar ( *args ) :

    prefPath = getPreferencePath ( ) ;

    defaultSelectedCharTxt = open ( prefPath + '/defaultSelectedChar.txt' , 'a+' ) ;
    defaultSelectedChar = defaultSelectedCharTxt.read ( ) ;
    defaultSelectedCharTxt.close ( ) ;

    if defaultSelectedChar == '' :
        updateCharList ( ) ;
    else :
        updateCharList ( ) ;

        textScrollList ( 'characterScrollList' , e = True , selectItem = defaultSelectedChar ) ;
        
        updateCharElementList ( ) ;

#################################################################################################################################

def updateFilmList ( *args ) :

    prefPath = getPreferencePath ( ) ;

    currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'a+' ) ;
    currentProject = currentProjectTxt.read ( ) ;
    currentProjectTxt.close ( ) ;
    # C:/Users/Legion/Desktop/royalBlood/projectTest2

    filmDirectory = currentProject + '/scene' ;

    if os.path.exists ( filmDirectory ) == True :

        filmList = os.listdir ( filmDirectory ) ;
        filmList.sort ( ) ;

        textScrollList ( 'filmScrollList' , e = True , ra = True ) ;

        for each in filmList :

            textScrollList ( 'filmScrollList' , e = True , append = each ) ;
    else :

        textScrollList ( 'filmScrollList' , e = True , ra = True ) ;

    updateSequenceList ( ) ;
    updateShotList ( ) ;
    updateShotCharList ( ) ;
    updateInfo ( ) ;

def createFilm ( *args ) :

    film = textField ( 'newFilmTextField' , q = True , text = True ) ;

    if film == '' :
        pass ;
    else :
        prefPath = getPreferencePath ( ) ;
        currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'a+' ) ;
        currentProject = currentProjectTxt.read ( ) ;
        currentProjectTxt.close ( ) ;

        newFilmDirectory = currentProject + '/' + 'scene' + '/' + film ;
        createFolder ( directory = newFilmDirectory , element = 'film' ) ;
        updateFilmList ( ) ;
        updateInfo ( ) ;

def updateSequenceList ( *args ) :

    film = textScrollList ( 'filmScrollList' , q = True , selectItem = True ) ;
    if film == [] : textScrollList ( 'sequenceScrollList' , e = True , ra = True ) ;
    else :
        film = film [0] ;
    
        prefPath = getPreferencePath ( ) ;

        currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'a+' ) ;
        currentProject = currentProjectTxt.read ( ) ;
        currentProjectTxt.close ( ) ;
        # C:/Users/Legion/Desktop/royalBlood/projectTest2

        sequenceDirectory = currentProject + '/scene' + '/' + film ;

        if os.path.exists ( sequenceDirectory ) == True :

            sequenceList = os.listdir ( sequenceDirectory ) ;
            sequenceList.sort ( ) ;

            textScrollList ( 'sequenceScrollList' , e = True , ra = True ) ;

            for each in sequenceList :

                textScrollList ( 'sequenceScrollList' , e = True , append = each ) ;
        else :

            textScrollList ( 'sequenceScrollList' , e = True , ra = True ) ;

    updateShotList ( ) ;
    updateShotCharList ( ) ;
    updateInfo ( ) ;

def createSequence ( *args ) :

    film = textScrollList ( 'filmScrollList' , q = True , selectItem = True ) ;

    if film == [] :
        error ( 'please select a film before to create a sequence' ) ;
    else :        
        film = film [0] ;

        sequence = textField ( 'newSequenceTextField' , q = True , text = True ) ;

        if sequence == '' :
            pass ;
        else :
            prefPath = getPreferencePath ( ) ;
            currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'a+' ) ;
            currentProject = currentProjectTxt.read ( ) ;
            currentProjectTxt.close ( ) ;

            newSequenceDirectory = currentProject + '/' + 'scene' + '/' + film + '/' + sequence ;
            createFolder ( directory = newSequenceDirectory , element = 'sequence' ) ;
            updateSequenceList ( ) ;
            updateInfo ( ) ;

def createShot ( *args ) :

    film = textScrollList ( 'filmScrollList' , q = True , selectItem = True ) ;
    if film == [] :
        error ( 'please select a film before to create a sequence' ) ;
    else :        
        film = film [0] ;

        sequence = textScrollList ( 'sequenceScrollList' , q = True , selectItem = True ) ;
        if sequence == [] :
            error ( 'please select a sequence before to create a shot' ) ;
        else :
            sequence = sequence [0] ;

            shot = textField ( 'newShotTextField' , q = True , text = True ) ; 
            if shot == '' :
                pass ;
            else :

                prefPath = getPreferencePath ( ) ;
                currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'a+' ) ;
                currentProject = currentProjectTxt.read ( ) ;
                currentProjectTxt.close ( ) ;

                newShotDirectory = currentProject + '/' + 'scene' + '/' + film + '/' + sequence + '/' + shot ;
                createFolder ( directory = newShotDirectory , element = 'shot' ) ;
                updateShotList ( ) ;
                updateInfo ( ) ;

def updateShotList ( *args ) :

    film = textScrollList ( 'filmScrollList' , q = True , selectItem = True ) ;
    if film == [] : pass ;
    else :
        film = film [0] ;

        sequence = textScrollList ( 'sequenceScrollList' , q = True , selectItem = True ) ;
        if sequence == [] : textScrollList ( 'shotScrollList' , e = True , ra = True ) ;
        else :
            sequence = sequence [0] ;

            prefPath = getPreferencePath ( ) ;

            currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'a+' ) ;
            currentProject = currentProjectTxt.read ( ) ;
            currentProjectTxt.close ( ) ;
            # C:/Users/Legion/Desktop/royalBlood/projectTest2

            shotDirectory = currentProject + '/scene' + '/' + film + '/' + sequence ;

            if os.path.exists ( shotDirectory ) == True :

                shotList = os.listdir ( shotDirectory ) ;
                shotList.sort ( ) ;

                textScrollList ( 'shotScrollList' , e = True , ra = True ) ;

                for each in shotList :

                    textScrollList ( 'shotScrollList' , e = True , append = each ) ;
            else :

                textScrollList ( 'shotScrollList' , e = True , ra = True ) ;

    updateShotCharList ( ) ;
    updateInfo ( ) ;

def updateShotCharList ( *args ) :

    film = textScrollList ( 'filmScrollList' , q = True , selectItem = True ) ;
    if film == [] : pass ;
    else :
        film = film [0] ;

        sequence = textScrollList ( 'sequenceScrollList' , q = True , selectItem = True ) ;
        if sequence == [] : pass ;
        else :
            sequence = sequence [0] ;

            shot = textScrollList ( 'shotScrollList' , q = True , selectItem = True ) ;
            if shot == [] : textScrollList ( 'shotCharScrollList' , e = True , ra = True ) ;
            else :
                shot = shot [0] ;

                prefPath = getPreferencePath ( ) ;

                currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'a+' ) ;
                currentProject = currentProjectTxt.read ( ) ;
                currentProjectTxt.close ( ) ;

                shotCharDirectory = currentProject + '/scene' + '/' + film + '/' + sequence + '/' + shot ;

                if os.path.exists ( shotCharDirectory ) == True :

                    shotCharList = os.listdir ( shotCharDirectory ) ;
                    shotCharList.sort ( ) ;

                    textScrollList ( 'shotCharScrollList' , e = True , ra = True ) ;

                    for each in shotCharList :

                        textScrollList ( 'shotCharScrollList' , e = True , append = each ) ;
                else :

                    textScrollList ( 'shotCharScrollList' , e = True , ra = True ) ;
    updateInfo ( ) ;

#################################################################################################################################

def autonaming ( *args ) :

    char = textScrollList ( 'characterScrollList' , q = True , selectItem = True ) ;
    if char == [] :
        selectionError ( selection = 'a character' ) ;
    else :
        char = char [0] ;

        elementList = textScrollList ( 'characterElementScrollList' , q = True , selectItem = True ) ;
        if elementList == [] :
            selectionError ( selection = 'element(s)' ) ;
        else :
            
            film = textScrollList ( 'filmScrollList' , q = True , selectItem = True ) ;
            if film == [] :
                selectionError ( selection = 'a film' ) ;
            else :
                film = film [0] ;

                sequence = textScrollList ( 'sequenceScrollList' , q = True , selectItem = True ) ;
                if sequence == [] :
                    selectionError ( selection = 'a sequence' ) ;
                else :
                    sequence = sequence [0] ;

                    shot = textScrollList ( 'shotScrollList' , q = True , selectItem = True ) ;
                    if shot == [] :
                        selectionError ( selection = 'a shot' ) ;
                    else :
                        shot = shot [0] ;

                        shotCharList = textScrollList ( 'shotCharScrollList' , q = True ,  allItems = True ) ;

                        #print ( char , elementList , film , sequence , shot , shotCharList ) ;
                        # (u'archer', [u'test'], u'film1', u'sequence1', u'shot', [u'archerrrrr'])

                        prefPath = getPreferencePath ( ) ;

                        currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'a+' ) ;
                        currentProject = currentProjectTxt.read ( ) ;
                        currentProjectTxt.close ( ) ;

                        sourceAutonamingDirectory = currentProject + '/asset/char' + '/' + char ;

                        destinationAutonamingDirectory = currentProject + '/scene' + '/' + film + '/' + sequence + '/' + shot ;

                        destinationAutonamingCharDirectory = destinationAutonamingDirectory + '/' + char ;

                        autonamingDirectory ( directory = destinationAutonamingCharDirectory ) ;

                        destinationAutonamingCharVersionDirectory = destinationAutonamingCharDirectory + '/' + 'version' ;

                        version = checkVersion ( directory = destinationAutonamingCharVersionDirectory , incrementVersion = True ) ;

                        for each in elementList :

                            heroDirectory = sourceAutonamingDirectory + '/' + each ;

                            heroList = os.listdir ( heroDirectory ) ;

                            for each in heroList :

                                if 'HERO' not in each :
                                    pass ;

                                else :

                                    heroName = each.split ( '.' ) ;
                                    charName = heroName [0] ;
                                    typeName = heroName [1] ;
                                    name = film + '.' + sequence + '.' + shot + '.' + charName + '.' + typeName + '.' + 'v' + version + '.ma'

                                    shutil.copy ( heroDirectory + '/' + each , destinationAutonamingCharVersionDirectory ) ;

                                    shutil.move ( destinationAutonamingCharVersionDirectory + '/' + each ,
                                        destinationAutonamingCharVersionDirectory + '/' + name ) ;

                        updateShotCharList ( ) ;

                        confirmDialog ( message = 'autonaming completed' ) ;

def setProject ( *args ) :

    film = textScrollList ( 'filmScrollList' , q = True , selectItem = True ) ;
    if film == [] :
        selectionError ( selection = 'a film' ) ;
    else :
        film = film [0] ;

        sequence = textScrollList ( 'sequenceScrollList' , q = True , selectItem = True ) ;
        if sequence == [] :
            selectionError ( selection = 'a sequence' ) ;
        else :
            sequence = sequence [0] ;

            shot = textScrollList ( 'shotScrollList' , q = True , selectItem = True ) ;
            if shot == [] :
                selectionError ( selection = 'a shot' ) ;
            else :
                shot = shot [0] ;

                shotChar = textScrollList ( 'shotCharScrollList' , q = True , selectItem = True ) ;
                if shotChar == [] :
                    selectionError ( selection = 'a character' ) ;
                else :
                    shotChar = shotChar [0] ;

                    prefPath = getPreferencePath ( ) ;

                    currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'a+' ) ;
                    currentProject = currentProjectTxt.read ( ) ;
                    currentProjectTxt.close ( ) ;
                    # C:/Users/Legion/Desktop/royalBlood/Royal_Blood
                    setProjectDirectory = currentProject + '/' + 'scene' + '/' + film + '/' + sequence + '/' + shot + '/' + shotChar ;

                    mel.eval( 'setProject "%s"' % setProjectDirectory ) ;

def checkVersion ( directory = '' , name = '' , incrementVersion = True , incrementSubVersion = False , *args ) :
    
    allFile = os.listdir ( directory ) ;
    versionList = [] ;

    for each in allFile :
        version = each.split ( '.' ) [-2] ;
        versionList.append ( version ) ;
    
    if len ( versionList ) == 0 :
        version = '001'
        subVersion = '001'

    else :
        versionList.sort ( ) ;
        latestVersion = versionList [-1] ;
        latestVersion = latestVersion.split ( '_' ) ;
        version = int ( re.findall ( '[0-9]{3}' ,  latestVersion [ -2 ] ) [0] ) ;
        subVersion = int ( re.findall ( '[0-9]{3}' , latestVersion [ -1 ] ) [0] ) ;

        if incrementSubVersion == True :
            subVersion += 1 ;
            subVersion = str(subVersion).zfill(3) ;
        else :
            subVersion = str(subVersion).zfill(3) ;
            
        if incrementVersion == True :
            version += 1 ;
            version = str(version).zfill ( 3 ) ;
            subVersion = '001' ;
                # overwrite increment subversion
        else :
            version = str(version).zfill ( 3 ) ;

    return str(version) + '_' + str(subVersion) ;


def autonamingDirectory ( directory = '' )  :

    scriptPath = getScriptPath ( ) ;

    if os.path.exists ( directory ) == False :
        os.makedirs ( directory ) ;
        os.makedirs ( directory + '/_DDR' ) ;
        os.makedirs ( directory + '/_maya' ) ;
        os.makedirs ( directory + '/data' ) ;
        os.makedirs ( directory + '/hero' ) ;
        os.makedirs ( directory + '/version' ) ;
        workspace = scriptPath + '/asset/workspace.mel' ;
        shutil.copy ( workspace , directory ) ;
    else : pass ;

    mel.eval( 'setProject "%s"' % directory ) ;

    return directory ;

def updateCharList ( *args ) :
    
    prefPath = getPreferencePath ( ) ;

    currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'a+' ) ;
    currentProject = currentProjectTxt.read ( ) ;
    currentProjectTxt.close ( ) ;

    characterDirectory = currentProject + '/asset/char' ;

    if os.path.exists ( characterDirectory ) == True :
        characterList = os.listdir ( characterDirectory ) ;
        characterList.sort ( ) ;

        textScrollList ( 'characterScrollList' , e = True , ra = True ) ;

        for each in characterList :
            textScrollList ( 'characterScrollList' , e = True , append = each ) ;

    else :

        textScrollList ( 'characterScrollList' , e = True , ra = True ) ;
        textScrollList ( 'characterScrollList' , e = True , append = "no char(s)" ) ;

    #textScrollList ( 'characterScrollList' , h = 75 , ams = False ) ;

def updateCharElementList ( *args ) :

    try :

        char = textScrollList ( 'characterScrollList' , q = True , selectItem = True ) [0] ;

        prefPath = getPreferencePath ( ) ;

        currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'a+' ) ;
        currentProject = currentProjectTxt.read ( ) ;
        currentProjectTxt.close ( ) ;

        characterElementDirectory = currentProject + '/asset/char/' + char ;

        if char != '' :

            if os.path.exists ( characterElementDirectory ) == True :

                charElementList = os.listdir ( characterElementDirectory ) ;
                charElementList.sort ( ) ;

                textScrollList ( 'characterElementScrollList' , e = True , ra = True ) ;

                for each in charElementList :    
                    textScrollList ( 'characterElementScrollList' , e = True , append = each ) ;       

            else : pass ;

    except :

        textScrollList ( 'characterElementScrollList' , e = True , ra = True  ) ;    

    else :

        pass ;

    updateInfo ( ) ;

def createCharacter ( *args ) :

    prefPath = getPreferencePath ( ) ;

    currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'a+' ) ;
    currentProject = currentProjectTxt.read ( ) ;
    currentProjectTxt.close ( ) ;

    newChar = textField ( 'newCharTextField' , q = True , text = True ) ;

    if newChar == '' :
        pass ;

    else :

        newCharDirectory = currentProject + '/asset/char/' + newChar ;

        if os.path.exists ( newCharDirectory ) == True :
            error ( 'character already exist, please check again' ) ;
        else :
            os.makedirs ( newCharDirectory ) ;
            os.makedirs ( newCharDirectory + '/cloth' ) ;
            os.makedirs ( newCharDirectory + '/hair' ) ;

        updateCharList ( ) ;
        updateCharElementList ( ) ;

def createElement ( *args ) :

    try :
        char = textScrollList ( 'characterScrollList' , q = True , selectItem = True ) [0] ;
    except :
        error ( 'please select a character to create its element' ) ;
    else : pass ;

    prefPath = getPreferencePath ( ) ;

    currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'a+' ) ;
    currentProject = currentProjectTxt.read ( ) ;
    currentProjectTxt.close ( ) ;

    newElem = textField ( 'newElemTextField' , q = True , text = True ) ;

    if newElem == '' :

        pass ;

    else :

        newElemDirectory = currentProject + '/asset/char/' + char + '/' + newElem ;

        if os.path.exists ( newElemDirectory ) == True :
            error ( 'element already exist, please check again' ) ;
        else :
            os.makedirs ( newElemDirectory ) ;

        updateCharElementList ( ) ;

def updateInfo ( ) :

    film = textScrollList ( 'filmScrollList' , q = True , selectItem = True ) ;
    if film == [] :
        textField ( 'shotInfoTxtField' , e = True , text = '...' ) ;
    else :
        film = film [0] ;
        
        sequence = textScrollList ( 'sequenceScrollList' , q = True , selectItem = True ) ;
        if sequence == [] :
            textField ( 'shotInfoTxtField' , e = True , text = film + '/...' ) ;
        else :
            sequence = sequence [0] ;

            shot = textScrollList ( 'shotScrollList' , q = True , selectItem = True ) ;
            if shot == [] :
                textField ( 'shotInfoTxtField' , e = True , text = film + '/' + sequence + '/...' ) ;
            else :
                shot = shot [0] ;

                shotChar = textScrollList ( 'shotCharScrollList' , q = True , selectItem = True ) ;
                if shotChar == [] :
                    textField ( 'shotInfoTxtField' , e = True , text = film + '/' + sequence + '/' + shot + '/...' ) ;
                else :
                    shotChar = shotChar [0] ;
                    textField ( 'shotInfoTxtField' , e = True , text = film + '/' + sequence + '/' + shot + '/' + shotChar ) ;

    char = textScrollList ( 'characterScrollList' , q = True , selectItem = True ) ;
    if char == [] :
        textField ( 'elementInfoTxtField' , e = True , text = '...' ) ;
    else :
        char = char [0] ;

        element = textScrollList ( 'characterElementScrollList' , q = True , selectItem = True ) ;
        if element == [] :
            textField ( 'elementInfoTxtField' , e = True , text = char + ' : ...' ) ;
        else :
            elementStr = '' ;
            for each in element :
                if each != element [-1] :
                    elementStr += each + ' / '
                else :
                    elementStr += each
            textField ( 'elementInfoTxtField' , e = True , text = char + ' : ' + elementStr ) ;

#################################################################################################################################

def updateAll ( *args ) :
    
    updateFilmList ( ) ;
    updateCharList ( ) ;
    updateCharElementList ( ) ;

def initializeProject ( *args ) :

    try :
        projectDirectory = textField ( 'userDirectory' , q = True , text = True ) ;
        selectedProject = textScrollList ( 'projectList' , q = True , selectItem = True ) [0] ;
        currentProject =  projectDirectory + '/' + selectedProject ;
        
        prefPath = getPreferencePath ( ) ;
        
        if os.path.exists ( currentProject ) == True :

            currentProjectTxt = open ( prefPath + '/currentProject.txt' , 'w+' ) ;
            currentProjectTxt.write ( currentProject ) ;
            currentProjectTxt.close ( ) ;

            saveWorkingDirectory ( ) ;
            saveProject ( ) ;
        else : pass ;

    except : pass ;

    # check if window exists
    if window ( 'projectManagerUI' , q = True , exists = True ) == True :
        deleteUI ( 'projectManagerUI' , window = True ) ;

    projectManagerUI ( ) ;

#################################################################################################################################

def projectManagerUI ( *args ) :
    
    # check if window exists
    if window ( 'projectManagerUI' , q = True , exists = True ) == True :
        deleteUI ( 'projectManagerUI' , window = True ) ;
    
    win = window ( 'projectManagerUI' , title = 'Bird project manager v0.0' ) ;
    window ( win , e = True , w = 400 , h = 393 , sizeable = False ) ;
    with win :
    
        mainLayout = rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            mainTabLayout = tabLayout ( changeCommand = tabChangeCommand ) ;
            with mainTabLayout :
                    
                setProjectTab = rowColumnLayout ( nc = 1 , w = 400 ) ;
                with setProjectTab :
                    
                    separator ( vis = True ) ;
                    
                    browseProjectFrameLayout = frameLayout ( 'browseProjectFrameLayout' , label = 'WORKING DIRECTORY' , w = 400 , bgc = ( 0.0980392 , 0.0980392 , 0.439216 ) ) ;
                    with browseProjectFrameLayout :

                        browseProjectLayout = rowColumnLayout ( nc = 2 ,
                            columnWidth = ( [ 1 , 320 ] , [ 2 , 80 ] ) ,
                            rs = [ 1 , 2 ] ) ;
                        with browseProjectLayout :
                        
                            textField ( 'userDirectory' , ec = updatePath , cc = updatePath ) ;
                            button ( label = 'browse' , c = browsePath ) ;
                            
                            #text ( label = '' ) ;
                            #button ( label = 'save directory' , c = saveWorkingDirectory , bgc = ( 0.498039 , 1 , 0 ) ) ;
                    
                    separator ( vis = False , h = 5 ) ;
                    
                    selectProjectLayout = frameLayout ( 'selectProjectLayout' , label = 'PROJECTS' , w = 400 , bgc = ( 0.0980392 , 0.0980392 , 0.439216 ) ) ;
                    with selectProjectLayout :
                        
                        projectList = textScrollList ( 'projectList' , ams = False , sc = updateAll ) ;
                        defaultPath ( ) ;
                        defaultProject ( ) ;
                    
                    createProjectLayout = rowColumnLayout ( nc = 2 , cw = ( [ 1 , 320 ] , [ 2 , 80 ] ) , rs = [ 1 , 2 ] ) ;
                    with createProjectLayout :    
                        projectInput = textField ( 'projectInput' ) ;
                        button ( label = 'create project' , bgc = ( 1 , 0.647059 , 0 ) , c = createProject ) ;
                        text ( label = '' ) ;
                        button ( label = 'initialize' , c = initializeProject , bgc = ( 0.498039 , 1 , 0 ) ) ;

                projectManagerTab = rowColumnLayout ( nc = 1 , w = 400 ) ;
                with projectManagerTab :
                    
                    separator ( vis = True ) ;
                    
                    shotFrameLayout = frameLayout ( 'shotFrameLayout' , label = 'SHOT' , w = 400 , bgc = ( 0.0980392 , 0.0980392 , 0.439216 ) ) ;
                    with shotFrameLayout :

                        shotLayout = rowColumnLayout ( nc = 4 , columnWidth = ( [ 1 , 100 ] , [ 2 , 100 ] , [ 3 , 100 ] , [ 4 , 100 ] ) ) ;
                        with shotLayout :

                            text ( label = 'FILM' , w = 100 , font = 'boldLabelFont' , bgc =  ( 1 , 0.0784314 , 0.576471 ) ) ;
                            text ( label = 'SEQUENCE' , w = 100 , font = 'boldLabelFont' , bgc =  ( 1 , 0.411765 , 0.705882 ) ) ;
                            text ( label = 'SHOT' , w = 100 , font = 'boldLabelFont' , bgc =  ( 1 , 0.713725 , 0.756863 ) ) ;
                            text ( label = 'CHARACTER' , w = 100 , font = 'boldLabelFont' , bgc =  ( 0.133333 , 0.545098 , 0.133333 ) ) ;

                            textScrollList ( 'filmScrollList' , h = 125 , ams = False , sc = updateSequenceList ) ;
                            textScrollList ( 'sequenceScrollList' , h = 125 , ams = False , sc = updateShotList ) ;
                            textScrollList ( 'shotScrollList' , h = 125 , ams = False , sc = updateShotCharList ) ;
                            textScrollList ( 'shotCharScrollList' , h = 125 , ams = False , sc = updateInfo ) ;

                            textField ( 'newFilmTextField' ) ;
                            textField ( 'newSequenceTextField' ) ;
                            textField ( 'newShotTextField' ) ;
                            text ( label = '' ) ;

                            button ( label = 'create film' , bgc =  ( 1 , 0.647059 , 0 ) , c = createFilm ) ;
                            button ( label = 'create sequence' , bgc =  ( 1 , 0.843137 , 0 ) , c = createSequence ) ;
                            button ( label = 'create shot' , bgc =  ( 1 , 1 , 0 ) , c = createShot ) ;
                            text ( label = '' ) ;

                    characterLayout = rowColumnLayout ( nc = 2 , columnWidth = ( [ 1 , 200 ] , [ 2 , 200 ] ) ) ;
                    with characterLayout :

                        characterFrameLayout = frameLayout ( 'characterFrameLayout' , label = 'CHARACTER' , w = 200 , bgc = ( 0.0980392 , 0.0980392 , 0.439216 ) ) ;
                        with characterFrameLayout :
                        
                            characterSelectionLayout = rowColumnLayout ( nc = 2 , columnWidth = ( [ 1 , 100 ] , [ 2 , 100 ] ) ) ;
                            with characterSelectionLayout :

                                text ( label = 'CHARACTER' , w = 100 , font = 'boldLabelFont' , bgc =  ( 0.133333 , 0.545098 , 0.133333 ) ) ;
                                text ( label = 'ELEMENT' , w = 100 , font = 'boldLabelFont' , bgc =  ( 0.196078 , 0.803922 , 0.196078 ) ) ;

                                textScrollList ( 'characterScrollList' , h = 75 , ams = False , sc = updateCharElementList ) ;
                                textScrollList ( 'characterElementScrollList' , h = 75 , ams = True , sc = updateInfo ) ;

                                textField ( 'newCharTextField' ) ;
                                textField ( 'newElemTextField' ) ;

                                button ( 'create character' , bgc =  ( 1 , 0.647059 , 0 ) , c = createCharacter ) ;
                                button ( 'create element' , bgc =  ( 1 , 0.843137 , 0 ) , c = createElement ) ;

                        autoNamingInfoFrameLayout = frameLayout ( 'autoNamingInfoFrameLayout' , label = 'AUTONAMING INFO' , w = 200 , bgc = ( 0.545098 , 0 , 0 ) ) ;
                        with autoNamingInfoFrameLayout :

                            autonamingInfoMaginLayout = rowColumnLayout ( nc = 3 , columnWidth = ( [ 1 , 10 ] , [ 2 , 180 ] , [ 3 , 10 ] ) ) ;
                            with autonamingInfoMaginLayout :

                                text ( label = '' ) ;

                                autonamingInfoLayout = rowColumnLayout ( nc = 1 , columnWidth = ( [ 1 , 180 ] ) ) ;
                                with autonamingInfoLayout :

                                    text ( label = 'SHOT' , font = 'boldLabelFont' ) ;
                                    textField ( 'shotInfoTxtField' , text = '...' ) ;

                                    text ( label = 'ELEMENT' , font = 'boldLabelFont' ) ;
                                    textField ( 'elementInfoTxtField' , text = '...' ) ;

                                    separator ( vis = False ) ;

                                    button ( label = 'autonaming' , c = autonaming , bgc = ( 1 , 0.647059 , 0 ) ) ;
                                    button ( label = 'set project' , c = setProject , bgc = ( 0.498039 , 1 , 0 ) ) ;

                                text ( label = '' ) ;

                helpTab = rowColumnLayout ( nc = 1 , w = 400 ) ;
                with helpTab :

                    with frameLayout ( label = 'SAVING DEFAULT' , w = 400 , bgc = ( 0.0980392 , 0.0980392 , 0.439216 ) ) :

                        with rowColumnLayout ( nc = 3 , cw = ( [ 1 , 10 ] , [ 2 , 380 ] , [ 3 , 10 ] ) ) :

                            text ( label = '' ) ;
                            
                            scrollField ( wordWrap = True , editable = True , h = 60 ,
                                text = 'Everytime you change a tab, everything you selected with be saved as default.' ) ;

                            text ( label = '' ) ;

                    namingConventionFrameLayout = frameLayout ( 'namingConventionFrameLayout' , label = 'NAMING CONVENTION' , w = 400 , bgc = ( 0.0980392 , 0.0980392 , 0.439216 ) ) ;
                    with namingConventionFrameLayout :

                        with rowColumnLayout ( nc = 3 , cw = ( [ 1 , 10 ] , [ 2 , 380 ] , [ 3 , 10 ] ) ) :

                            text ( label = '' ) ;
                            with rowColumnLayout ( nc = 1 , cw = [ 1 , 380 ] ) :

                                with frameLayout ( label = 'ASSET' , w = 380 , bgc = ( 0.545098 , 0 , 0 ) ) :

                                    scrollField ( wordWrap = True , editable = True , h = 80 ,
                                        text = 'name convention example : support.cloth01_pants.HERO\n\nEverything with "HERO" in its name with be coppied to the shot.' ) ;

                            text ( label = '' ) ;

                    with frameLayout ( label = 'KNOWN BUGS' , w = 400 , bgc = ( 0.0980392 , 0.0980392 , 0.439216 ) ) :

                        with rowColumnLayout ( nc = 3 , cw = ( [ 1 , 10 ] , [ 2 , 380 ] , [ 3 , 10 ] ) ) :

                            text ( label = '' ) ;
                            
                            bug = "version 0.0" ;
                            bug += "\n- 'Item not Found' error usually come up when you first use the script or when you first initialize the project. However, it doesn't affect the functionality of the tool." ;
                            bug += "\n- If you don't select a project before you click the initialize button, the UI will not reload correctly and result in window simply disappear. Simply run the script once more."
                            
                            scrollField ( wordWrap = True , editable = True , h = 100 ,
                                text = bug ) ;
                                

                            text ( label = '' ) ;

            tabLayout ( mainTabLayout , edit = True , tabLabel = (
                ( setProjectTab , 'project setup' ) , ( projectManagerTab , 'project manager' ) , ( helpTab , 'help' ) ) ) ;

    selectDefaultSelectedShot ( ) ;
    selectDefaultSelectedChar ( ) ;
                        
    win.show ( ) ;