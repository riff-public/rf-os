import pymel.core as pm ;

# this script was optimized for two heroes rigs, for maya 2017 , in year 2018
# (C) RiFF Studio, Weerapot Chaoman

class RestShapeRig ( object ) :

    def __init__ ( self ) :
        pass ;

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    ########################
    ########################
    ''' general utilities '''
    ########################
    ########################
    def makeName ( self , target ) :
        target                  = pm.general.PyNode ( target ) ;
        targetName              = target.nodeName() ;
        targetNameSplit_list    = targetName.split ( '_' ) ;
        name = targetNameSplit_list[0] ;
        for each in targetNameSplit_list[1:] :
            name += each.capitalize() ;
        return name ;

    ########################
    ########################
    ''' run '''
    ########################
    ########################

    def scaleAmp ( self ) :

        if pm.objExists ( 'scaleAmp_mdv' ) :
            
            scaleAmp = pm.general.PyNode ( 'scaleAmp_mdv' ) ;
        
        else :

            scaleAverage_pma = pm.createNode ( 'plusMinusAverage' , n = 'scaleAverage_pma' ) ;
            scaleAverage_pma.operation.set(1) ;

            scaleAmp = pm.createNode ( 'multiplyDivide' , n = 'scaleAmp_mdv' ) ;
            scaleAmp.operation.set(2) ;
            
            skin_grp = pm.general.PyNode ( 'Skin_Grp' ) ;

            skin_grp.sx >> scaleAverage_pma.i1[0] ;
            skin_grp.sy >> scaleAverage_pma.i1[1] ;
            skin_grp.sz >> scaleAverage_pma.i1[2] ;

            scaleAverage_pma.o1 >> scaleAmp.i1x ;
            scaleAmp.i2x.set(3) ;

        return scaleAmp ;

    def initializeRig ( self ) :
        
        ### step 1 : delete unnecessary geo groups

        placement_ctrl = pm.general.PyNode ( 'Place_Ctrl' ) ;
        self.releaseAttr ( placement_ctrl ) ;
        for axis in [ 'x' , 'y' , 'z' ] :
            exec ( 'pm.disconnectAttr ( placement_ctrl.s{axis} ) ;'.format ( axis = axis ) ) ;
            exec ( 'placement_ctrl.s{axis}.set(1) ;'.format ( axis = axis ) ) ;

        geo_grp         = pm.general.PyNode ( 'Geo_Grp' ) ;
        geoGrp_children = geo_grp.getChildren() ;
        
        DYN_GRP     = pm.general.PyNode ( 'DYN_GRP' ) ;
        DYN_GRP.v.set ( 1 ) ;

        for child in geoGrp_children :
            if child != DYN_GRP :
                pm.delete ( child ) ;

        ### step 2 : hide unnecessary ctrls
        # make leg and arm fk 
        for side in [ 'L' , 'R' ] :
            for ctrl in [
                'Leg_%s_Ctrl' % side ,
                'Arm_%s_Ctrl' % side ,
                ] :

                ctrl = pm.general.PyNode ( ctrl ) ;
                ctrl.fkIk.set(0) ;

        # hide unnecessary ctrls
        ctrl_grp            = pm.general.PyNode ( 'Ctrl_Grp' ) ;
        ctrlGrp_children    = ctrl_grp.getChildren() ;

        NeckCtrl_Grp    = pm.general.PyNode ( 'NeckCtrl_Grp' ) ;
        HeadCtrl_Grp    = pm.general.PyNode ( 'HeadCtrl_Grp' ) ;
        SpineCtrl_Grp   = pm.general.PyNode ( 'SpineCtrl_Grp' ) ;
        ArmCtrl_L_Grp   = pm.general.PyNode ( 'ArmCtrl_L_Grp' ) ;
        ArmCtrl_R_Grp   = pm.general.PyNode ( 'ArmCtrl_R_Grp' ) ;
        LegCtrl_L_Grp   = pm.general.PyNode ( 'LegCtrl_L_Grp' ) ;
        LegCtrl_R_Grp   = pm.general.PyNode ( 'LegCtrl_R_Grp' ) ;
        stretchCtrl_List = [ HeadCtrl_Grp , SpineCtrl_Grp , ArmCtrl_L_Grp , ArmCtrl_R_Grp , LegCtrl_L_Grp , LegCtrl_R_Grp ] ;
        
        for child in ctrlGrp_children :
            if child not in stretchCtrl_List :
                child.v.set(0) ;

        ### step 4 : duplicate and rename skin_grp
        skinRsr_grp = pm.general.PyNode ( 'Skin_Grp' ) ;
        skinRsr_grp.v.set(1) ;

        tfm_dic = {} ;

        for type in [ 'transform' , 'joint' ] : 
            for each in skinRsr_grp.listRelatives ( ad = True , type = type ) :
                for attr in [ 't' , 'r' , 's' ] :
                    exec ( "tfm_dic[ '{each}.{attr}' ] = each.{attr}.get() ;".format ( each = each , attr = attr ) ) ;

        # duplicate
        skinCIN_grp = pm.duplicate ( skinRsr_grp ) [0] ;

        # rename (for Rsr) 
        skinRsr_grp.rename ( skinRsr_grp.nodeName() + '_Rsr' ) ;

        skinRsrGrp_children = skinRsr_grp.listRelatives ( ad = True ) ;
        for child in skinRsrGrp_children :
            child.rename ( child.nodeName() + '_Rsr' ) ;

        # remove constraints
        skinCINGrp_parCon = skinCIN_grp.listRelatives ( ad = True , type = 'parentConstraint' ) ;
        for parCon in skinCINGrp_parCon :
            pm.parentConstraint ( parCon , e = True , rm = True ) ;

        skinCINGrp_sclCon = skinCIN_grp.listRelatives ( ad = True , type = 'scaleConstraint' ) ;
        for sclCon in skinCINGrp_sclCon :
            pm.scaleConstraint ( sclCon , e = True , rm = True ) ;

        for type in [ 'transform' , 'joint' ] : 
            for each in skinCIN_grp.listRelatives ( ad = True , type = type ) :
                for attr in [ 't' , 'r' , 's' ] :
                    exec ( "each.{attr}.set( tfm_dic['{each}.{attr}'] ) ;".format ( each = each , attr = attr ) ) ;
                    #exec ( "tfm_dic[ '{each}.{attr}' ] = each.{attr}.get() ;".format ( each = each , attr = attr ) ) ;

        self.releaseAttr ( skinCIN_grp ) ;
        pm.parent ( skinCIN_grp , w = True ) ;
        skinCIN_grp.rename ( 'Skin_Grp' ) ;
        
        # connect CIN's joint scale to Rsr's

        skinCINGrp_allItem  = skinCIN_grp.listRelatives ( ad = True ) ;

        for item in skinCINGrp_allItem :
            self.releaseAttr ( item ) ;
            
            if item.nodeType() == 'joint' :
                CIN_jnt = item ;
                rsr_jnt = pm.general.PyNode ( CIN_jnt.nodeName() + '_Rsr' ) ;

                # break scale connections
                for axis in [ 'x' , 'y' , 'z' ] :
                    exec ( 'pm.disconnectAttr ( rsr_jnt.s{axis} ) ;'.format ( axis = axis ) ) ;
                    exec ( 'CIN_jnt.s{axis} >> rsr_jnt.s{axis} ;'.format ( axis = axis ) ) ;

        ### step 3 : clean up scene
        # delete unused node

        placement_ctrl  = pm.general.PyNode ( 'Place_Ctrl' ) ;

        for axis in [ 'x' , 'y' , 'z' ] :
            exec ( 'skinCIN_grp.s{axis} >> placement_ctrl.s{axis} ;'.format ( axis = axis ) ) ;

        pm.mel.eval('MLdeleteUnused;') ;

        scaleAmp = self.scaleAmp() ;

    ########################
    ########################
    ''' step 2 : modify rig '''
    ########################
    ########################

    def decomposeMatrix ( self , target ) :
        # dmn = 'D'ecompose 'M'atrix 'N'ode
        
        target = pm.general.PyNode ( target ) ;
        dmnName = self.makeName ( target.nodeName() ) + '_dmn' ;

        if not pm.objExists ( dmnName ) :
            dmn = pm.createNode ( 'decomposeMatrix' , n = dmnName ) ;
            target.worldMatrix >> dmn.inputMatrix ;
        else :
            dmn = pm.general.PyNode ( dmnName ) ;

        return dmn ;

    def distanceBetween ( self , targetA , targetB ) :
        # dbn = 'D'istance 'B'etween 'N'ode

        targetA = pm.general.PyNode ( targetA ) ;
        targetB = pm.general.PyNode ( targetB ) ;
        
        dbnName = self.makeName(targetA.nodeName()) + '_' + self.makeName(targetB.nodeName()) + '_dbn' ;

        if not pm.objExists ( dbnName ) :

            targetA_dmn = self.decomposeMatrix ( targetA ) ;
            targetB_dmn = self.decomposeMatrix ( targetB ) ;
            
            dbn = pm.createNode ( 'distanceBetween' , n = dbnName ) ;

            targetA_dmn.outputTranslate >> dbn.point1 ;
            targetB_dmn.outputTranslate >> dbn.point2 ;

        else :
            dbn = pm.general.PyNode ( dbn ) ;

        return dbn ;

    def releaseAttr ( self , target ) :
        
        target = pm.general.PyNode ( target ) ;

        for attr in [ 't' , 'r' , 's' ] :
            for axis in [ 'x' , 'y' , 'z' ] :
                exec ( 'target.{attr}{axis}.set( channelBox = True , lock = False ) ;'.format ( attr = attr , axis = axis ) ) ;

    def installStretch ( self , baseJnt , tipJnt , tipCtrl , ribbonJnt = None , side = None ) :

        scaleAmp        = self.scaleAmp() ;
        
        baseJnt     = pm.general.PyNode ( baseJnt ) ;
        tipJnt      = pm.general.PyNode ( tipJnt ) ;
        ribbonJnt   = [
            pm.general.PyNode ( ribbonJnt[0] ) , 
            pm.general.PyNode ( ribbonJnt[1] ) ,
            pm.general.PyNode ( ribbonJnt[2] ) ,
            pm.general.PyNode ( ribbonJnt[3] ) ,
            pm.general.PyNode ( ribbonJnt[4] ) ] ;
        tipCtrl     = pm.general.PyNode ( tipCtrl ) ;

        baseTipName = self.makeName(baseJnt.nodeName()) + '_' + self.makeName(tipJnt.nodeName()) ;

        allJnt_list = [] ;
        allJnt_list.append ( baseJnt ) ;
        allJnt_list.extend ( ribbonJnt ) ;
        allJnt_list.append ( tipJnt ) ;

        jntPair_list = [] ;
        for i in range ( 0 , len(allJnt_list)-1 ) :
            jntPair_list.append ( [ allJnt_list[i] , allJnt_list[i+1] ] ) ;

        dbn_list = [] ;
        for jntPair in jntPair_list :
            dbn = self.distanceBetween ( jntPair[0] , jntPair[1] ) ;
            dbn_list.append ( dbn ) ;

        # static length
        staticLength = 0.0 ;
        for dbn in dbn_list :
            staticLength += dbn.distance.get() ;

        outputStaticLength_mdv = pm.createNode ( 'multiplyDivide' , n = baseTipName + '_staticLength_mdv' ) ;
        outputStaticLength_mdv.operation.set(0) ;
        outputStaticLength_mdv.i1x.set ( staticLength ) ;
        scaleAmp.ox >> outputStaticLength_mdv.i2x ;

        # find dynamic length
        dynamicLength_pma = pm.createNode ( 'plusMinusAverage' , name = self.makeName(baseJnt.nodeName()) + '_' + self.makeName(tipJnt.nodeName()) + '_pma' ) ;
        dynamicLength_pma.operation.set(1) ;
        for i in range ( 0 , len (dbn_list) ) :
            dbn_list[i].distance >> dynamicLength_pma.i1[i] ;

        outputDynamicLength_mdv = pm.createNode ( 'multiplyDivide' , n = baseTipName + '_dynamicLength_mdv' ) ;
        outputDynamicLength_mdv.operation.set(2) ;
        dynamicLength_pma.o1 >> outputDynamicLength_mdv.i1x ;
        scaleAmp.ox >> outputDynamicLength_mdv.i2x ;

        # use pma to find translate value for tip control
        translateValue_pma = pm.createNode ( 'plusMinusAverage' , name = self.makeName(baseJnt.nodeName()) + '_' + self.makeName(tipJnt.nodeName()) + '_tVal_pma' ) ;
        translateValue_pma.operation.set(2) ;
        outputDynamicLength_mdv.ox  >> translateValue_pma.i1[0] ;
        outputStaticLength_mdv.ox   >> translateValue_pma.i1[1] ;

        outputTranslateValue_mdv = pm.createNode ( 'multiplyDivide' , n = baseTipName + '_tVal_mdv' ) ;
        outputTranslateValue_mdv.operation.set(2) ;
        translateValue_pma.o1   >> outputTranslateValue_mdv.i1x ;
        scaleAmp.ox             >> outputTranslateValue_mdv.i2x ;


        if not side :
            outputTranslateValue_mdv.ox >> tipCtrl.ty ;
        else :
            if side == 'L' :
                outputTranslateValue_mdv.ox >> tipCtrl.ty ;
            elif side == 'R' :
                mdv = pm.createNode ( 'multiplyDivide' , n = self.makeName(baseJnt.nodeName()) + '_' + self.makeName(tipJnt.nodeName()) + '_TranslateVal_mdv' ) ;
                mdv.operation.set ( 1 ) ;
                translateValue_pma.o1 >> mdv.i1x ;
                mdv.i2x.set ( -1 ) ;
                mdv.ox >> tipCtrl.ty ;

    def modifyRig ( self ) :

        ### spine >> chest ###
        self.installStretch (
            baseJnt     = 'Pelvis_Jnt' ,
            tipJnt      = 'Chest_Jnt' ,
            ribbonJnt   = [
              'SpineSkin_1_Jnt' ,
              'SpineSkin_2_Jnt' ,
              'SpineSkin_3_Jnt' ,
              'SpineSkin_4_Jnt' ,
              'SpineSkin_5_Jnt' ] ,
            tipCtrl     = 'Chest_Ctrl' ) ;

        ### neck >> head ###
        self.installStretch (
            baseJnt     = 'Neck_Jnt' ,
            tipJnt      = 'Head_Jnt' ,
            ribbonJnt   = [
              'RbnNeckDtl_1_Jnt' ,
              'RbnNeckDtl_2_Jnt' ,
              'RbnNeckDtl_3_Jnt' ,
              'RbnNeckDtl_4_Jnt' ,
              'RbnNeckDtl_5_Jnt' ] ,
            tipCtrl     = 'Head_Ctrl' ) ;

        for side in [ 'L' , 'R' ] :

            ### upArm ###
            self.installStretch ( 
                baseJnt     = pm.general.PyNode ( 'UpArm_%s_Jnt' % side ) ,
                tipJnt      = pm.general.PyNode ( 'Forearm_%s_Jnt' % side ) ,
                ribbonJnt   = [
                    pm.general.PyNode ( 'RbnUpArmDtl_1_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpArmDtl_2_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpArmDtl_3_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpArmDtl_4_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpArmDtl_5_%s_Jnt' % side ) ] ,
                tipCtrl     = pm.general.PyNode ( 'ForearmFk_%s_Ctrl' % side ) ,
                side = side ) ;
            
            ### loArm ###
            self.installStretch ( 
                baseJnt     = pm.general.PyNode ( 'Forearm_%s_Jnt' % side ) ,
                tipJnt      = pm.general.PyNode ( 'Wrist_%s_Jnt' % side ) ,
                ribbonJnt   = [
                    pm.general.PyNode ( 'RbnForearmDtl_1_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnForearmDtl_2_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnForearmDtl_3_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnForearmDtl_4_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnForearmDtl_5_%s_Jnt' % side ) ] ,
                tipCtrl     = pm.general.PyNode ( 'WristFk_%s_Ctrl' % side ) ,
                side = side ) ;

            ### upLeg ###
            self.installStretch ( 
                baseJnt     = pm.general.PyNode ( 'UpLeg_%s_Jnt' % side ) ,
                tipJnt      = pm.general.PyNode ( 'LowLeg_%s_Jnt' % side ) ,
                ribbonJnt   = [
                    pm.general.PyNode ( 'RbnUpLegDtl_1_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpLegDtl_2_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpLegDtl_3_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpLegDtl_4_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpLegDtl_5_%s_Jnt' % side ) ] ,
                tipCtrl     = pm.general.PyNode ( 'LowLegFk_%s_Ctrl' % side ) ,
                side = side ) ;

            ### loLeg ###
            self.installStretch ( 
                baseJnt     = pm.general.PyNode ( 'LowLeg_%s_Jnt' % side ) ,
                tipJnt      = pm.general.PyNode ( 'Ankle_%s_Jnt' % side ) ,
                ribbonJnt   = [
                    pm.general.PyNode ( 'RbnLowLegDtl_1_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnLowLegDtl_2_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnLowLegDtl_3_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnLowLegDtl_4_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnLowLegDtl_5_%s_Jnt' % side ) ] ,
                tipCtrl     = pm.general.PyNode ( 'AnkleFk_%s_Ctrl' % side ) ,
                side = side ) ;

def run ( *args ) :
    target = RestShapeRig() ;
    target.initializeRig() ;
    target.modifyRig() ;

#run() ;