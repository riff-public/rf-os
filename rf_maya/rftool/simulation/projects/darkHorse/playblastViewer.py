import pymel.core as pm ;
import maya.cmds as mc ;
import os
#, shutil , re ;

############
############
''' config '''
############
############

project_path = 'P:/darkHorse/scene/publ/';

############
############
''' core '''
############
############

def directory_cmd ( path = '' , *args ) :

    if os.path.exists ( path ) == False :
        os.makedirs ( path ) ;
    else : pass ;
    
    return ( path ) ;

def getPreferencePath_cmd ( *args ) :
    myDocuments = os.path.expanduser('~') ;
    myDocuments = myDocuments.replace ( '\\' , '/' ) ;
    pref_path = directory_cmd ( myDocuments + '/birdScriptPreferences/twoHeroes/playblastViewer/' ) ;
    return pref_path ;


def saveDefaultSelection_cmd ( *args ) :

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;

    film            = film_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;

    playblastClass  = pm.optionMenu ( 'playblastClass_optionMenu' , q = True , value = True ) ;

    defaultSelection_dict = {} ;

    defaultSelection_dict['film']           = film ;
    defaultSelection_dict['sequence']       = sequence ;
    defaultSelection_dict['shot']           = shot ;

    defaultSelection_dict['playblastClass'] = playblastClass ;

    pref_path = getPreferencePath_cmd ( ) ;

    defaultSelectionText_path = pref_path + 'defaultSelection.txt' ;
    defaultSelection_file = open ( defaultSelectionText_path , 'w+' ) ;
    defaultSelection_file.write ( 'defaultSelection_dict = %s' % str(defaultSelection_dict) ) ;
    defaultSelection_file.close ( ) ;

def setDefaultSelection_cmd ( *args ) :

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;

    pref_path = getPreferencePath_cmd ( ) ;

    defaultSelectionText_path = pref_path + 'defaultSelection.txt' ;

    if os.path.isfile ( defaultSelectionText_path ) == True :
    
        defaultSelection_file = open ( defaultSelectionText_path , 'r' ) ;
        defaultSelection_txt = defaultSelection_file.read ( ) ;
        defaultSelection_file.close ( ) ;

        exec ( defaultSelection_txt ) ;

        pm.optionMenu ( 'playblastClass_optionMenu' , e = True , value = defaultSelection_dict['playblastClass'] ) ;

        updateFilmTextScrollList_cmd ( ) ;

        if defaultSelection_dict['film'] == None : pass ;
        else :
            film_textScrollList.selectItem ( defaultSelection_dict['film'] ) ;
            updateSequenceTextScrollList_cmd ( ) ;

            if ( defaultSelection_dict['sequence'] ) == None : pass ;
            else :
                sequence_textScrollList.selectItem ( defaultSelection_dict['sequence'] ) ;
                updateShotTextScrollList_cmd ( ) ;

                if defaultSelection_dict['shot'] == None : pass ;
                else :
                    shot_textScrollList.selectItem ( defaultSelection_dict['shot'] ) ;
                    updatePlayblastTextScrollList_cmd ( ) ;

    else : pass ;

############
############
''' UI CORE '''
############
############

class TextScrollList ( object ) :

    def __init__ ( self, textScrollList ) :
        self.textScrollList = textScrollList ;

    def append ( self , item ) :
        self.item = item ;
        pm.textScrollList ( self.textScrollList , e = True , append = self.item ) ;

    def clear ( self ) :
        pm.textScrollList ( self.textScrollList , e = True , ra = True ) ; 

    def currentItem ( self ) :

        currentItem = pm.textScrollList ( self.textScrollList , q = True , selectItem = True ) ;
        
        if currentItem == [] :
            return None ;
        else :
            if len ( currentItem ) == 1 :
                return currentItem [0] ;
            else :
                return ( currentItem ) ;

    def selectItem ( self , selectItem ) :
        self.selectItem = selectItem ;
        pm.textScrollList ( self.textScrollList , e = True , selectItem = self.selectItem ) ;

def listdir ( elem = '' , *args ) :

    target_path = project_path ;

    film_textScrollList         = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList     = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList         = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    

    if elem == 'film' :
        return os.listdir ( target_path ) ;
    
    elif elem == 'sequence' :
        target_path += film_textScrollList.currentItem() + '/' ;

    elif elem == 'shot' :
        target_path += film_textScrollList.currentItem() + '/' ;
        target_path += sequence_textScrollList.currentItem() + '/' ;

    elif elem == 'playblast' :

        playblastClass = pm.optionMenu ( 'playblastClass_optionMenu' , q = True , value = True ) ;

        if film_textScrollList.currentItem() != None :
            target_path += film_textScrollList.currentItem() + '/' ;

            if sequence_textScrollList.currentItem() != None :
                target_path += sequence_textScrollList.currentItem() + '/' ;
        
                if shot_textScrollList.currentItem() != None :
                    target_path += shot_textScrollList.currentItem() + '/' ;
                    target_path += playblastClass + '/' ;
                    target_path += 'maya/playblast/'

        else : return None ;

    #print target_path ;

    if os.path.exists ( target_path ) == False :
        return None ;
    else :
        return os.listdir ( target_path ) ;

# def listdir ( elem = '' , *args ) :

#     browserClass = pm.optionMenu ( 'browserClass_optionMenu' , q = True , value = True ) ;

#     if browserClass == 'server' :
#         target_path = project_path ;
#     elif browserClass == 'local' :
#         target_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;

#     act_textScrollList         = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
#     sequence_textScrollList     = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
#     shot_textScrollList         = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
#     character_textScrollList    = TextScrollList ( textScrollList = 'character_textScrollList' ) ;

#     if elem == 'act' :
#         return os.listdir ( target_path ) ;
    
#     elif elem == 'sequence' :
#         target_path += act_textScrollList.currentItem() + '/' ;

#     elif elem == 'shot' :
#         target_path += act_textScrollList.currentItem() + '/' ;
#         target_path += sequence_textScrollList.currentItem() + '/' ;

#     elif elem == 'character' :

#         target_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;

#         if act_textScrollList.currentItem() != None :
#             target_path += act_textScrollList.currentItem() + '/' ;

#             if sequence_textScrollList.currentItem() != None :
#                 target_path += sequence_textScrollList.currentItem() + '/' ;

#     elif elem == 'cam' :

#         target_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;

#         if act_textScrollList.currentItem() != None :
#             target_path += act_textScrollList.currentItem() + '/' ;

#             if sequence_textScrollList.currentItem() != None :
#                 target_path += sequence_textScrollList.currentItem() + '/' ;
        
#                 if shot_textScrollList.currentItem() != None :
#                     target_path += shot_textScrollList.currentItem() ;
#                     target_path += '/output' ;


#         else : return None ;

#     print "target_path:::: ", target_path
#     if os.path.exists ( target_path ) == False :
#         return None ;
#     else :
#         return os.listdir ( target_path ) ;



def updateProjectBrowser_cmd ( elem = '' , *args ) :

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    playblast_textScrollList        = TextScrollList ( textScrollList = 'playblast_textScrollList' ) ;

    if elem == 'film' :

        film_textScrollList.clear ( ) ;
        sequence_textScrollList.clear ( ) ;
        shot_textScrollList.clear ( ) ;

        filmItem_list = listdir ( 'film' ) ;
        filmItem_list.sort ( ) ;
        for item in filmItem_list :
            film_textScrollList.append ( item ) ;

    elif elem == 'sequence' :

        sequence_textScrollList.clear ( ) ;
        shot_textScrollList.clear ( ) ;

        sequenceItem_list = listdir ( 'sequence' ) ;
        sequenceItem_list.sort ( ) ;
        for item in sequenceItem_list :
            sequence_textScrollList.append ( item ) ;

    elif elem == 'shot' :

        shot_textScrollList.clear ( ) ;
        
        shotItem_list = listdir ( 'shot' ) ;
        shotItem_list.sort ( ) ;
        for item in shotItem_list :
            shot_textScrollList.append ( item ) ;

    elif elem == 'playblast' :

        playblast_textScrollList.clear ( ) ;
        
        playblastItem_list = listdir ( 'playblast' ) ;

        if playblastItem_list != None :

            playblastItem_list.sort ( reverse = True ) ;

            for item in playblastItem_list :
                playblast_textScrollList.append ( item ) ;

def updateFilmTextScrollList_cmd ( *args ) :
    updateProjectBrowser_cmd ( elem = 'film' ) ;

def updateSequenceTextScrollList_cmd ( *args ) :
    updateProjectBrowser_cmd ( elem = 'sequence' ) ;

def updateShotTextScrollList_cmd ( *args ) :
    updateProjectBrowser_cmd ( elem = 'shot' ) ;

def updatePlayblastTextScrollList_cmd ( *args ) :
    updateProjectBrowser_cmd ( elem = 'playblast' ) ;
    saveDefaultSelection_cmd ( ) ;

def filmSelection_cmd ( *args ) :
    updateSequenceTextScrollList_cmd ( ) ;
    saveDefaultSelection_cmd ( ) ;

def sequenceSelection_cmd ( *args ) :
    updateShotTextScrollList_cmd ( ) ;
    saveDefaultSelection_cmd ( ) ;

def shotSelection_cmd ( *args ) :
    updatePlayblastTextScrollList_cmd ( ) ;
    saveDefaultSelection_cmd ( ) ;

def browserDoubleClick_cmd ( *args ) :

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    playblast_textScrollList        = TextScrollList ( textScrollList = 'playblast_textScrollList' ) ;

    playblastClass      = pm.optionMenu ( 'playblastClass_optionMenu' , q = True , value = True ) ;

    film            = film_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    playblast       = playblast_textScrollList.currentItem ( ) ;

    browse_path = project_path ;

    if film == None : pass ;
    else :
        browse_path += film ;
        
        if sequence == None : pass ;
        else :
            browse_path += '/' + sequence ;

            if shot == None : pass ;
            else :
                browse_path += '/' + shot ;
                browse_path += '/' + playblastClass + '/' + 'maya/playblast/' ;

    if playblastClass == 'cloth' :
        directory_cmd ( browse_path ) ;

    try :
        os.startfile ( browse_path ) ;
    except :
        pass ;

def playblastDoubleClick_cmd ( *args ) :

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    playblast_textScrollList        = TextScrollList ( textScrollList = 'playblast_textScrollList' ) ;

    film            = film_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    playblast       = playblast_textScrollList.currentItem ( ) ;
    
    playblastClass  = pm.optionMenu ( 'playblastClass_optionMenu' , q = True , value = True ) ;

    browse_path = project_path
    browse_path += film ;
    browse_path += '/' + sequence ;
    browse_path += '/' + shot ;

    browse_path += '/' + playblastClass + '/' + 'maya/playblast/' + playblast ;

    os.startfile ( browse_path ) ;

############
############
''' UI '''
############
############

def run ( *args ) :

    title = 'two heroes playblast viewer v001_001' ;

    width = 700.00 ;
    textScrollHeight = 200.00 ;

    def separator ( times = 1 , *args ) :
        for i in range ( 0 , times ):
            pm.separator ( h = 5 , vis = False ) ;

    def filler ( *args ) :
        pm.text ( label = '' ) ;

    # check if window exists
    if pm.window ( 'twoHeroes_playblastViewer_UI' , exists = True ) :
        pm.deleteUI ( 'twoHeroes_playblastViewer_UI' ) ;
    else : pass ;
   
    window = pm.window ( 'twoHeroes_playblastViewer_UI', title = title ,
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'twoHeroes_playblastViewer_UI' , e = True , w = width , h = 100 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            ###########################
            ''' project browser layout '''
            ###########################
            
            with pm.frameLayout ( label = 'PROJECT BROWSER' , w = width , bgc = ( 0.1 , 0.1 , 0.45 ) ) :

                with pm.rowColumnLayout ( nc = 1 , w = width ) :

                    with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/3 ) , ( 2 , width/3 ) , ( 3 , width/3 ) ] ) :

                        with pm.rowColumnLayout ( nc = 1 , w = width/3 ) :
                            pm.text ( label = 'FILM' , font = 'boldLabelFont' , bgc =  ( 1 , 0 , 0.55 ) , width = width/3 ) ;
                            pm.textScrollList ( 'film_textScrollList' , ams = False , h = textScrollHeight , width = width/3 ,
                                sc = filmSelection_cmd ,
                                dcc = browserDoubleClick_cmd ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/3 ) :
                            pm.text ( label = 'SEQUENCE' , font = 'boldLabelFont' , bgc =  ( 1 , 0.4 , 0.7 ) , width = width/3 ) ;
                            pm.textScrollList ( 'sequence_textScrollList' , ams = False , h = textScrollHeight , width = width/3 ,
                                sc = sequenceSelection_cmd ,
                                dcc = browserDoubleClick_cmd ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/3 ) :
                            pm.text ( label = 'SHOT' , font = 'boldLabelFont' , bgc =  ( 1 , 0.7 , 0.75 ) , width = width/3 ) ;
                            pm.textScrollList ( 'shot_textScrollList' , ams = False , h = textScrollHeight , width = width/3 ,
                                sc = shotSelection_cmd ,
                                dcc = browserDoubleClick_cmd ) ;
                                
            ###########################
            ''' autonaming layout '''
            ###########################

            with pm.frameLayout ( label = 'PLAYBLAST LAYOUT' , w = width , bgc = ( 0.1 , 0.1 , 0.45 ) ) :

                    with pm.rowColumnLayout ( nc = 1 , w = width) :

                        ###########################
                        ''' browse cache layout '''
                        ###########################
                        with pm.rowColumnLayout ( nc = 1 , w = width ) :
            
                            with pm.optionMenu ( 'playblastClass_optionMenu' , label = 'playlast class' , cc = updatePlayblastTextScrollList_cmd , w = width ) :
                                pm.menuItem ( label='version' ) ;
                                pm.menuItem ( label='hero' ) ;
  

                            pm.textScrollList ( 'playblast_textScrollList' , ams = True , h = textScrollHeight , width = width ,
                                #sc = 'characterSelection_cmd' ,
                                dcc = playblastDoubleClick_cmd ,
                                ) ;

    updateFilmTextScrollList_cmd ( ) ;
    setDefaultSelection_cmd ( ) ;

    window.show () ;

def toShelf ( *args ) :

    self_path = os.path.realpath (__file__) ;
    self_name = os.path.basename (__file__) ;
    self_path = self_path.replace ( self_name , '' ) ;
    self_path = self_path.replace ( '\\' , '/' ) ;

	# self_path = D:/Dropbox/script/birdScript/projects/twoHeroes/

    image_path = self_path + 'media\playblastViewer_icon.png'

    commandHeader = '''
import sys ;

mp = "%s" ;
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
''' % self_path.split ('/projects/')[0] ;
# D:/Dropbox/script/birdScript

    cmd = commandHeader ;
    cmd += '''
import projects.twoHeroes.playblastViewer as playblastViewer ;
reload ( playblastViewer ) ;
playblastViewer.run ( ) ;
'''
    mel_cmd = '''
global string $gShelfTopLevel;
string $shelves = `tabLayout -q -selectTab $gShelfTopLevel`;
'''
    currentShelf = pm.mel.eval ( mel_cmd );
    pm.shelfButton ( style = 'iconOnly' , image = image_path , command = cmd , parent = currentShelf ) ;