import pymel.core as pm ;
import os ;

'''
help :
xGen shader name = filename + '_XgS'
Yeti shader name = filename + '_YtS'
'''

class XGenShader_GUI ( object ) :

    def __init__ ( self ) :
        self.ui         = 'groomShader_UI' ;
        self.title      = 'Groom Shader' ;
        self.version    = 'v2.0' ; 
        self.width      = 250.00 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;
    
    def checkUniqueness ( self , ui ) :

        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def getSelfPath ( self ) :

        self_path   = os.path.realpath (__file__) ;
        fileName    = os.path.basename (__file__) ;
        self_path   = self_path.replace ( fileName , '' ) ;
        #self_path   = self_path.replace ( '\\' , '/' ) ;

        # D:/Dropbox/script/birdScript/wip/hair_groomShaderUtil/
        return self_path ;

    def openShaderLocation ( self , *args ) :
        os.startfile ( r'%s' % self.getSelfPath() + 'shader' ) ;

    def assign ( self , *args ) :

        selection = pm.ls ( sl = True ) ;

        type = pm.optionMenu ( self.groomType_opm , q = True , v = True ) ;
        # 'Interactive xGen'
        # 'Yeti'

        if type == 'Interactive xGen' :
            nodeType = 'xgmSplineDescription' ;
        elif type == 'Yeti' :
            nodeType = 'pgYetiMaya' ;

        validItem_list = [] ;

        # check selected item validity
        for each in selection :
            
            if each.getShape() :

                selectionType = each.getShape().nodeType() ;
                
                if selectionType == nodeType :
                    validItem_list.append ( each ) ;

        if validItem_list :
            
            shader = self.importShader() ;

            if type == 'Interactive xGen' :

                pm.select ( validItem_list ) ;

                try :
                    exec ( 'sets -e -forceElement ' + shader.nodeName() + 'SE;' ) ;
                    #pm.hyperShade ( a = shader ) ;

                except :

                    attr_list = [] ;

                    attr_list.extend ( [ 'rootColorDR' , 'rootColorDG' , 'rootColorDB' ] ) ;
                    attr_list.extend ( [ 'tipColorDR' , 'tipColorDG' , 'tipColorDB' ] ) ;
                    attr_list.extend ( [ 'intensityD' ] ) ;
                    attr_list.extend ( [ 'transparencyR' , 'transparencyG' , 'transparencyB' ] ) ;
                    attr_list.extend ( [ 'ambientColorR' , 'ambientColorG' , 'ambientColorB' ] ) ;
                    attr_list.extend ( [ 'incandescenceR' , 'incandescenceG' , 'incandescenceB' ] ) ;
                    attr_list.extend ( [ 'colorRR' , 'colorRG' , 'colorRB' ] ) ;
                    attr_list.extend ( [ 'intensityR' , 'longitudinalShiftR' , 'longitudinalWidthR' ] ) ;
                    attr_list.extend ( [ 'colorTTB' , 'colorTTR' , 'colorTTG' ] ) ;
                    attr_list.extend ( [ 'intensityTT' , 'longitudinalShiftTT' , 'longitudinalWidthTT' ] ) ;
                    attr_list.extend ( [ 'azimuthalWidthTT' ] ) ;
                    attr_list.extend ( [ 'colorTRTR' , 'colorTRTG' , 'colorTRTB' ] ) ;
                    attr_list.extend ( [ 'intensityTRT' , 'longitudinalShiftTRT' , 'longitudinalWidthTRT' ] ) ;
                    attr_list.extend ( [ 'colorGR' , 'colorGG' , 'colorGB' ] ) ;
                    attr_list.extend ( [ 'intensityG' , 'azimuthalShiftG' , 'azimuthalWidthG' ] ) ;

                    selection = pm.ls ( sl = True ) ;

                    for item in selection :

                        itemShape = item.getShape() ;
                        shadingGrp = pm.listConnections ( itemShape , type = 'shadingEngine' ) [0] ;

                        if str(shadingGrp)[-2:] == 'SG' :
                            shadingGrp = pm.general.PyNode ( str(shadingGrp)[:-2] ) ;

                        # shadingGrp = str(shadingGrp).replace ( '_XgSSG' , '_XgS' ) ;
                        # shadingGrp = pm.general.PyNode ( shadingGrp ) ;

                        for attr in attr_list :
                            
                            try :

                                exec ( 'shaderLockStat = shader.{attr}.get( lock = True )'.format ( attr = attr ) ) ;
                                exec ( 'shadingGrpLockStat = shadingGrp.{attr}.get( lock = True )'.format ( attr = attr ) ) ;

                                exec ( 'pm.setAttr ( shader.{attr} , lock = False )'.format ( attr = attr ) ) ;
                                exec ( 'pm.setAttr ( shadingGrp.{attr} , lock = False )'.format ( attr = attr ) ) ;
                                # exec ( 'shader.{attr}.unlock()'.format ( attr = attr ) ) ;
                                # exec ( 'shadingGrp.{attr}.unlock()'.format ( attr = attr ) ) ;
                            
                                exec ( 'pm.disconnectAttr ( shadingGrp.{attr} )'.format ( attr = attr ) ) ;

                                exec ( 'shadingGrp.{attr}.set ( shader.{attr}.get() ) ;'.format ( attr = attr ) ) ;

                                exec ( 'shader.{attr}.set( lock = shaderLockStat )'.format ( attr = attr , shaderLockStat = shaderLockStat ) ) ;
                                exec ( 'shadingGrp.{attr}.set( lock = shadingGrpLockStat )'.format ( attr = attr , shadingGrpLockStat = shadingGrpLockStat) ) ;

                            except :

                                print ( '{shadingGrp}.{attr} is not editable, please check'.format ( shadingGrp = shadingGrp , attr = attr ) ) ;

                pm.select ( clear = True ) ;

            if type == 'Yeti' :
                for each in validItem_list :
                    pm.connectAttr ( shader.outColor , each.color ) ; 
            # connectAttr -force Sunny_YtS.outColor MainHair_YetiNodeShape.color;

    def importShader ( self ) :

        type = pm.optionMenu ( self.groomType_opm , q = True , v = True ) ;

        if type == 'Interactive xGen' :
            suffix = '_XgS' ;
        elif type == 'Yeti' :
            suffix = '_YtS' ;

        char = pm.textScrollList ( self.groomShaderList_tsl , q  = True , selectItem = True ) [0] ;

        if not pm.objExists ( char + suffix ) :

            file_path = self.getSelfPath() + 'shader/' + type  + '/' + char + '.ma' ;
            pm.system.importFile ( file_path ) ;

        return pm.general.PyNode ( char + suffix ) ;

    def updateList ( self , *args ) :

        pm.textScrollList ( self.groomShaderList_tsl , e = True , ra = True ) ;

        type = pm.optionMenu ( self.groomType_opm , q = True , v = True ) ;

        shader_path = self.getSelfPath() + 'shader/' + type ;
        shader_list = os.listdir ( shader_path ) ;
        shader_list.sort() ;

        for each in shader_list :
            if '.ma' in str ( each ) :
                char = each.replace ( '.ma' , '' ) ;
                pm.textScrollList ( self.groomShaderList_tsl , e = True , append = char ) ;

    def show ( self ) :

        self.checkUniqueness( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = self.title + ' ' + self.version ,
            mnb         = True ,
            mxb         = False ,
            sizeable    = True ,
            rtf         = True ) ;

        pm.window ( window , e = True , w = self.width , h = 10 ) ;

        with window :

            # mainLayout
            with pm.rowColumnLayout ( nc = 1 , w = self.width ) :

                pm.button ( label = 'open shader location' , w = self.width , c = self.openShaderLocation ) ;

                pm.button ( label = 'refresh' , w = self.width , c = self.updateList , bgc = ( 0.55 , 0.9 , 0.55 ) ) ;

                self.groomType_opm = pm.optionMenu ( 'groomType_opm' , label = 'Groom Type' , cc = self.updateList ) ;
                with self.groomType_opm :
                    pm.menuItem ( label = 'Interactive xGen' ) ;
                    pm.menuItem ( label = 'Yeti' ) ;

                self.groomShaderList_tsl = pm.textScrollList ( 'groomShaderList_tsl' , w = self.width , ams = False , h = 200 ) ;

                pm.button ( label = 'assign shade' , w = self.width , c = self.assign ) ;

        self.updateList() ;

        window.show() ;

def run ( *args ) :

    GUI = XGenShader_GUI() ;
    GUI.show() ;

'''
import wip.hair_groomShaderUtil.core as hair_groomShaderUtil ;
reload ( hair_groomShaderUtil ) ;
hair_groomShaderUtil.run() ;
'''