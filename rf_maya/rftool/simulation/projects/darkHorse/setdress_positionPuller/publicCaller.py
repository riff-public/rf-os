import sys ;

mp = "//riff-data/Data/Data/Pipeline/core/rf_maya/rftool/simulation" ;

if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;

import projects.twoHeroes.setdress_positionPuller.core as setdress_positionPuller ;
reload ( setdress_positionPuller ) ;

setdress_positionPuller.run() ;