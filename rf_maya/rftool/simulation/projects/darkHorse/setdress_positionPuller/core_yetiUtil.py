import maya.cmds as mc ;
import pymel.core as pm ;

class YetiUtil ( object ) :

    def __init__ ( self ) :

        # check if yeti plugin is loaded
        plugin = 'pgYetiMaya.mll'
        if not pm.pluginInfo ( plugin , q = True , loaded = True ): 
            pm.loadPlugin ( plugin , quiet = True ) ;

        self.selfPath = mc.file ( q = True , exn = True ) ;
        self.cachePath = self.getCachePath() ;

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def yetiNodeGroup ( self , *args ) :

        groupName = 'YetiNode_Grp' ;

        if not pm.objExists ( groupName ) :
            group = pm.group ( em = True , n = groupName ) ;
            group.t.lock() ;
            group.r.lock() ;
            group.s.lock() ;
            
        else :
            group = pm.general.PyNode ( groupName ) ;

        return ( group ) ;

    def getCachePath ( self ) :

        selfPath = self.selfPath ;

        selfPathSplit = selfPath.split ( 'maya' ) ;
        for split in selfPathSplit :
            if split == '' :
                selfPathSplit.remove ( split ) ;

        cachePath = selfPathSplit[0] ;
        if cachePath[-1] == '/' :
            cachePath = cachePath[0:-1] ;

        cachePathSplit = cachePath.split ( '/' ) ;
        for split in cachePathSplit :
            if split == '' :
                cachePathSplit.remove ( split ) ;

        elem = cachePathSplit[-1] ;

        cachePath = cachePath.replace ( elem , '' ) ;
        cachePath += 'cache' ;

        return cachePath ;

    def browseYeti ( self , *args ) :
        # return yetiNodeName , filePath ;
        
        fileList =  pm.fileDialog2 ( fileMode = 1 , dir = self.cachePath ) ;
        
        ### file path ###
        filePath = fileList[0] ;

        filePathSplit = filePath.split ( '.' ) ;
        for split in filePathSplit :
            if split == '' :
                filePathSplit.remove ( split ) ;

        filePath = filePath.replace ( filePathSplit[-2] , r'%04d' ) ;

        ### file name ###
        fileSplit = filePath.split ( '/' ) ;
        for split in fileSplit :
            if split == '' :
                fileSplit.remove ( split ) ;

        fileName = fileSplit[-1] ;

        fileNameSplit = fileName.split ( '.' ) ;
        for split in fileNameSplit :
            if split == '' :
                fileNameSplit.remove ( split ) ;

        yetiNodeName = fileNameSplit[-3] ;

        return yetiNodeName , filePath ;

    def createYetiNode ( self , name , path , *args ) :

        cmd = 'pgYetiCreate()' ;

        group = self.yetiNodeGroup() ;
        
        yetiNodeShape = pm.mel.eval ( cmd ) ;
        yetiNodeShape = pm.general.PyNode ( yetiNodeShape ) ;

        yetiNode = yetiNodeShape.getTransform() ;
        yetiNode.rename ( name ) ;

        pm.parent ( yetiNode , group ) ;

        yetiNodeShape.viewportWidth.set ( 3 ) ;
        yetiNodeShape.cacheFileName.set ( path ) ;
        yetiNodeShape.fileMode.set ( 1 ) ;
        yetiNodeShape.renderDensity.set ( 1 ) ;
  
    def importYeti ( self , *args ) :
        
        yetiInfo        = self.browseYeti() ;
        yetiNodeName    = yetiInfo[0] ;
        yetiPath        = yetiInfo[1] ;
        
        self.createYetiNode ( name = yetiNodeName , path = yetiPath ) ;