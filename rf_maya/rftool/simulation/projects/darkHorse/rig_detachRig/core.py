import pymel.core as pm ;

class Sim_RigUtil ( object ) :

    def __init__ ( self ) :
        pass ;

    def getGeoGrp ( self ) :
        for each in [ 'Geo_Grp' , 'geo_grp' ] :
            if pm.objExists ( each ) :
                return each ;

    def getTopNode ( self , target ) :
        target = pm.general.PyNode ( target ) ;
        if target.getParent() :
            return self.getTopNode ( target.getParent() ) ;
        else :
            return target ;

    def releaseAttr ( self , target ) :
        target = pm.general.PyNode ( target ) ;
        for attr in [ 't' , 'r' , 's' ] :
            for axis in [ 'x' , 'y' , 'z' ] :
                exec ( 'target.{attr}{axis}.set( channelBox = True , lock = False ) ;'.format ( attr = attr , axis = axis ) ) ;

    def detachCtrl ( self ) :
        
        try :
        	skin_grp = pm.general.PyNode( 'Skin_Grp' ) ;
        except :
        	pm.error ( 'Skin_Grp does not exist or is not unique' ) ;

        for conType in [ 'parentConstraint' , 'scaleConstraint' ] :
        	for con in skin_grp.listRelatives ( ad = True , type = conType ) :
        		pm.delete ( con ) ;

       	pm.parent ( skin_grp , self.getTopNode ( skin_grp ) ) ;

       	for item in self.getTopNode(skin_grp).getChildren() :
       		if item == pm.general.PyNode ( 'Geo_Grp' ) :
       			pass ;
       		elif item == pm.general.PyNode ( 'Skin_Grp' ) :
       			pass ;
       		else :
       			pm.delete ( item ) ;

        self.releaseAttr ( skin_grp ) ;

        for item in skin_grp.listRelatives ( ad = True , type = 'transform' ) :
          self.releaseAttr ( item ) ;

        geoGrp = self.getGeoGrp() ;
        pm.select ( geoGrp ) ;
        pm.mel.eval ( 'sets -e -forceElement initialShadingGroup;' ) ;
        pm.select ( clear = True ) ;
        

def run ( *args ) :
	rig = Sim_RigUtil() ;
	rig.detachCtrl() ;