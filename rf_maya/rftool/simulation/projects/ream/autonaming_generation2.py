
_title = 'autonaming_generation2'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'autonaming_generation2'

#Import python modules
import sys
import os, shutil, re
import getpass
from collections import OrderedDict
from functools import partial

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework modules 
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import maya.cmds as mc 
import pymel.core as pm
from rf_utils.context import context_info
from rf_utils import register_shot

reload(context_info)


project_path = 'P:/Ream/scene/publ/'
project_work_path = 'P:/Ream/scene/work/'

self_path = os.path.realpath (__file__) ;
self_name = os.path.basename (__file__) ;
self_path = self_path.replace ( self_name , '' ) ;
self_path = self_path.replace ( '\\' , '/' ) ;
print 'self_path', self_path
print 'self_name', self_name
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
''' general info '''
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################

def directory_cmd ( path = '' , *args ) :

    if os.path.exists ( path ) == False :
        os.makedirs ( path ) ;
    else : pass ;
    
    return ( path ) ;

def checkVersion_cmd ( directory = '' , name = '' , incrementVersion = True , incrementSubVersion = False , *args ) :
    # return format : 'v001_001'

    allFile = os.listdir ( directory ) ;
    versionList = [] ;

    for each in allFile :
        try :
            version = each.split ( '.' ) [-2] ;
            if len ( version.split ( '_' ) ) != 2 :
                pass ;
            else :
                versionList.append ( version ) ;
        except : pass ;
        else : pass ;
    
    if len ( versionList ) == 0 :
        version = '001'
        subVersion = '001'

    else :
        versionList.sort ( ) ;
        latestVersion = versionList [-1] ;
        latestVersion = latestVersion.split ( '_' ) ;
        version = int ( re.findall ( '[0-9]{3}' ,  latestVersion [ -2 ] ) [0] ) ;
        subVersion = int ( re.findall ( '[0-9]{3}' , latestVersion [ -1 ] ) [0] ) ;

        if incrementSubVersion == True :
            subVersion += 1 ;
            subVersion = str(subVersion).zfill(3) ;
        else :
            subVersion = str(subVersion).zfill(3) ;
            
        if incrementVersion == True :
            version += 1 ;
            version = str(version).zfill ( 3 ) ;
            subVersion = '001' ;
                # overwrite increment subversion
        else :
            version = str(version).zfill ( 3 ) ;

    return ( 'v' + str(version) + '_' + str(subVersion) ) ;

####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
''' user info '''
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################

def getPreferencePath_cmd ( *args ) :
    myDocuments = os.path.expanduser('~') ;
    print myDocuments, 'myDocuments'
    myDocuments = myDocuments.replace ( '\\' , '/' ) ;
    pref_path = directory_cmd ( myDocuments + '/birdScriptPreferences/ReamAutonaming_generation2/' ) ;
    return pref_path ;

def saveDefaultPath_cmd ( *args ) :
    pref_path = getPreferencePath_cmd ( ) ;
    inputPath_file = open ( pref_path + 'inputPath.txt' , 'w+' ) ;
    input_path = pm.textField ( 'inputPath_textField' , q = True , text = True ) ;
    print input_path, 'input_path'
    inputPath_file.write ( input_path ) ;
    inputPath_file.close ( ) ;

def setDefaultPath_cmd ( *args ) :
    
    pref_path = getPreferencePath_cmd ( ) ;

    inputPathText_path = pref_path + 'inputPath.txt' ;

    if os.path.isfile ( inputPathText_path ) == True :
        inputPath_file = open ( inputPathText_path , 'r' ) ;
        inputPath_text = inputPath_file.read ( ) ;
        inputPath_file.close ( ) ;

        pm.textField ( 'inputPath_textField' , e = True , text = inputPath_text ) ;
        pm.text ( 'inputPath_text' , e = True , label = inputPath_text + 'Ream/' ) ;

    else : pass ;

def saveDefaultSelection_cmd ( *args ) :

    act_textScrollList             = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheFile_textScrollList   = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;

    act            = act_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    character       = character_textScrollList.currentItem ( ) ;
    cacheFile  = cacheFile_textScrollList.currentItem ( ) ;

    browserClass    = pm.optionMenu ( 'browserClass_optionMenu' , q = True , value = True ) ;
    cacheClass      = pm.optionMenu ( 'cacheClass_optionMenu' , q = True , value = True ) ;

    defaultSelection_dict = {} ;

    defaultSelection_dict['act']           = act ;
    defaultSelection_dict['sequence']       = sequence ;
    defaultSelection_dict['shot']           = shot ;
    defaultSelection_dict['character']      = character ;
    defaultSelection_dict['cacheFile'] = cacheFile ;
    defaultSelection_dict['browserClass']   = browserClass ;
    defaultSelection_dict['cacheClass']     = cacheClass ;

    pref_path = getPreferencePath_cmd ( ) ;

    defaultSelectionText_path = pref_path + 'defaultSelection.txt' ;
    defaultSelection_file = open ( defaultSelectionText_path , 'w+' ) ;
    defaultSelection_file.write ( 'defaultSelection_dict = %s' % str(defaultSelection_dict) ) ;
    defaultSelection_file.close ( ) ;

def setDefaultSelection_cmd ( *args ) :

    act_textScrollList             = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheFile_textScrollList   = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;

    pref_path = getPreferencePath_cmd ( ) ;

    defaultSelectionText_path = pref_path + 'defaultSelection.txt' ;

    if os.path.isfile ( defaultSelectionText_path ) == True :
    
        defaultSelection_file = open ( defaultSelectionText_path , 'r' ) ;
        defaultSelection_txt = defaultSelection_file.read ( ) ;
        defaultSelection_file.close ( ) ;

        exec ( defaultSelection_txt ) ;

        pm.optionMenu ( 'browserClass_optionMenu' , e = True , value = defaultSelection_dict['browserClass'] ) ;
        pm.optionMenu ( 'cacheClass_optionMenu' , e = True , value = defaultSelection_dict['cacheClass'] ) ;

        updateactTextScrollList_cmd ( ) ;

        if defaultSelection_dict['act'] == None : pass ;
        else :
            act_textScrollList.selectItem ( defaultSelection_dict['act'] ) ;
            updateSequenceTextScrollList_cmd ( ) ;

            if ( defaultSelection_dict['sequence'] ) == None : pass ;
            else :
                sequence_textScrollList.selectItem ( defaultSelection_dict['sequence'] ) ;
                updateShotTextScrollList_cmd ( ) ;

                if defaultSelection_dict['shot'] == None : pass ;
                else :
                    shot_textScrollList.selectItem ( defaultSelection_dict['shot'] ) ;
                    updateCharacterTextScrollList_cmd ( ) ;

                    if defaultSelection_dict['character'] == None : pass ;
                    else :
                        character_textScrollList.selectItem ( defaultSelection_dict['character'] ) ;

                    if defaultSelection_dict['cacheFile'] == None : pass ;
                    else :
                        cacheFile_textScrollList.selectItem ( defaultSelection_dict['cacheFile'] ) ;

    else : pass ;

def browsePath_cmd ( project, *args ) :

    try : 
        start_path = pm.textField ( 'inputPath_textField' , q = True , text = True ) ;
    except :
        start_path = '/C:' ;

    browse_path = mc.fileDialog2 ( ds = 2 , fm = 3 , startingDirectory = start_path , okc = 'select directory' ) [0] ;
 
    if browse_path[-1] != '/' :
        browse_path += '/' ;

    pm.textField ( 'inputPath_textField' , e = True , text = browse_path ) ;
    pm.text ( 'inputPath_text' , e = True , label = browse_path + '%s/'%project ) ;
    saveDefaultPath_cmd ( ) ;

####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
''' set project '''
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################

def setProject_cmd ( *args ) :

    act_textScrollList             = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheFile_textScrollList   = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;

    act            = act_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    character       = character_textScrollList.currentItem ( ) ;
    cacheFile  = cacheFile_textScrollList.currentItem ( ) ;

    setProject_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;
    
    if act == None : pass ;
    else : 
        setProject_path += act + '/' ;

        if sequence == None : pass ;
        else :
            setProject_path += sequence + '/' ;

            # if shot == None : pass ;
            # else :
            #     setProject_path += shot + '/' ;

            if character == None : pass ;
            else :
                setProject_path += character ;

    print setProject_path ;

    try :
        pm.mel.eval( 'setProject "%s"' % setProject_path ) ;
    except :
        pm.error ( 'your input directory is not a maya project' ) ;

####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
''' autonaming '''
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################

def copy ( source , destination , name = None ,*args ) :
    # example : newFile = copy ( source = 'C:/Users/weerapot.c/Desktop/test1/content1.test.txt' , destination = 'C:/Users/weerapot.c/Desktop/test2' , name = 'test3.txt' ) ;
    
    ### copy ###
    shutil.copy2 ( source , destination ) ;
    
    ### get copied file's path ###
    copiedFile_name = os.path.basename(source) ;
    
    if destination[-1] != '/' :
        destination += '/' ;
    else : pass ;
 
    copiedFile_path = destination + copiedFile_name ;
    
    ### make path for new name, rename copied file ###
    if name != None :
        newName = destination + name ;

        shutil.move ( copiedFile_path , newName ) ;
        
        ### return copied path ###    
        return newName ;
    
    else :
        ### return copied path ###
        return copiedFile_path ;

def copytree ( source , destination , *args ) :

    sourceFolder_name = source.split ( '/' ) [-1] ;

    if destination[-1] != '/' :
        destination += '/' ;
    else : pass ;

    destination_path = destination + sourceFolder_name ;

    if os.path.exists ( destination_path ) == True :
        shutil.rmtree ( destination_path ) ;
    else : pass ;

    shutil.copytree ( source , destination_path ) ;

    return destination_path ;


def copy_merge ( source , destination , *args ) :

    sourceFolder_name = source.split ( '/' ) [-1] ;

    if destination[-1] != '/' :
        destination += '/' ;
    else : pass ;

    destination_path = destination + sourceFolder_name ;

    if os.path.exists ( destination_path ) == True :
         list_file = os.listdir(source)
         for file in list_file:
            source_file = os.path.join(source, file)
            dest_file = os.path.join(destination, file)
            copy(source_file, dest_file)
    else :
        shutil.rmtree(source)
        shutil.copytree ( source , destination_path ) ;

    return destination_path ;

def get_asset_sim_path(namespace, path, cache =False):###################### for scm################
    scene = context_info.ContextPathInfo(path= path)
    print scene
    scene.context.update(app='data', step ='sim', process ='main')
    print scene,'2'
    # cache_name = scene.output_name(outputKey='cache')
    # techName = scene2.output_name(outputKey='cache')
    path = scene.path.workspace().abs_path()
    # print 'path', path
    cache_path = os.path.join(path, namespace).replace('\\','/')
    print 'cache_path', cache_path
    scene.context.update( process =namespace)
    # print cache_path
    # cache_name = os.listdir(cache_path)[0]
    # cache_no_facial = os.listdir(cache_path)[-1]
    # print 'cache_name',cache_name
    # cache_name = scene.publish_name(hero=True,ext='.abc')
    # if not cache:
    # return cache_path
    # else:
    
    return cache_path, cache_path

    ################# return list_asset

    ### 1. must return cache_hero_path
    #### 2. list all namespace IN data folder and ORiGIn Folder 
    #### [cache_hero_path, list_copy_path]

def autonamingBtn_cmd ( project, *args ) :
    print 'testse'
    saveDefaultSelection_cmd ( ) ;
    
    localProject_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;

    act_textScrollList             = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheFile_textScrollList   = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;

    browserClass    = pm.optionMenu ( 'browserClass_optionMenu' , q = True , value = True ) ;
    cacheClass      = pm.optionMenu ( 'cacheClass_optionMenu' , q = True , value = True ) ;

    act            = act_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    character       = character_textScrollList.currentItem ( ) ;
    cacheFile  = cacheFile_textScrollList.currentItem ( ) ; 

    if ( act != None ) and ( sequence != None ) and ( shot != None ) and ( cacheFile != None ) :

        if type(cacheFile) == type(u'string') :
            cacheFile = [cacheFile] ;


        if cacheClass == 'char':
            for character in cacheFile :

                if browserClass == 'server_data':
                    scene_path = '%s%s/%s/output'%(project_work_path, act, sequence)
                    cache_hero_path, folder_asset_path = get_asset_sim_path(character, scene_path)
                    # cache_tech_hero_path = get_asset_sim_path('%s_Tech'%character, scene_path)
                    scene = context_info.ContextPathInfo(path = scene_path)
                    reg = register_shot.Register(scene) 

                elif browserClass == 'server':
                    data_path = '%s%s/%s/output/%s'%(project_path, act, sequence, character)
                    scene = context_info.ContextPathInfo(path = data_path)
                    reg = register_shot.Register(scene)  
                    cache_hero_path = reg.get.asset(character)['cache']['heroFile']
                    folder_asset_path = os.path.dirname(cache_hero_path)
                    # cache_tech_char = reg.get.asset('%s_Tech'%character)
                    # if cache_tech_char:
                    #   cache_tech_hero_path = cache_tech_char['cache']['heroFile']
                    cache_tech_hero_path = None
                    # pm.confirmDialog ( message = '%s tech_Geo dose not exist'% character, b='OK' )
                    

                description_path = reg.get.asset(character)['description']
                asset_context = context_info.ContextPathInfo(path= description_path)



                ########################
                # create local Folder##
                #######################
                print 'testsets'
                autonaming_path = directory_cmd ( localProject_path + act + '/' + sequence + '/' + character + '/' ) ;
                print 'pass' ,self_path
                copy ( self_path + 'asset/workspace.mel' , autonaming_path ) ; 
                directory_cmd ( autonaming_path + '_DDR' ) ;
                directory_cmd ( autonaming_path + '_maya' ) ;
                directory_cmd ( autonaming_path + 'data' ) ;
                directory_cmd ( autonaming_path + 'hero' ) ;
                directory_cmd ( autonaming_path + 'version' ) ;
                directory_cmd ( autonaming_path + 'cache' ) ;


                # print cache_hero_path 
                # cache_tech_char = reg.get.asset('%s_Tech'%character)

                # if cache_tech_char:
                #     cache_tech_hero_path = cache_tech_char['cache']['heroFile']
                # else: 
                #     cache_tech_hero_path = None
                #     pm.confirmDialog ( message = '%s tech_Geo dose not exist'% character, b='OK' )
                #     break




                character_path = 'P:/Ream/asset/publ/char/%s'%(asset_context.name)
                # asset_path = reg.get.asset('%s_Tech'%character)['description']

                sence = context_info.ContextPathInfo(path = cache_hero_path)
                sence.context.update(step ='sim')

                all_cam = reg.get.asset_list(type = 'camera')


                # list_asset = [cache_hero_path, cache_tech_hero_path] 
                # if cache_hero_path_noFac:
                #     list_asset.append(cache_hero_path_noFac)

                list_cam_path = []
                for cam in all_cam:
                    cam_path = reg.get.asset(cam)['cache']['heroFile']
                    list_cam_path.append(cam_path) 
                # print cache_hero_path, cache_tech_hero_path , list_cam_path
                
                # for asset in list_asset:
                shotCache_path = directory_cmd ( autonaming_path + 'data/shotCache/hero' ) ;
                if shotCache_path:
                    copytree(folder_asset_path, shotCache_path)

                for cam in list_cam_path:
                    camCache_path = directory_cmd ( autonaming_path + 'data/cam' )
                    copy(cam, camCache_path)

                ################
                ## Setup File ##
                ################

                asset = context_info.ContextPathInfo(path = character_path)
                asset.context.update(step = 'sim', process='main', publish=True)
                path = asset.path.hero().abs_path()
                print path
                sim_path = path.replace('hero','sim')
                processes = os.listdir(sim_path)

                for process in processes:
                    process_path = '%s/hero/output'%process
                    hero_path = os.path.join(sim_path, process_path)
                    print hero_path
                    file_hero = os.listdir(hero_path)[0]
                    file_output_hero = os.path.join(hero_path, file_hero)
                    sim_rig_path = directory_cmd ( autonaming_path + 'version/' + process ) ;
                    print sim_rig_path
                    copy(file_output_hero, sim_rig_path)

                character_name = character.split('.')[0] ;



            pm.confirmDialog ( message = '%s autonaming completed'%character_name, b='OK' )
            updateCharacterTextScrollList_cmd ( )
            # copytree ( serverCache_Path , shotCache_path )

        else:
            if not character:
                pm.confirmDialog ( message = 'Must select Character before', b='OK' )
            else:
                for cache in cacheFile :
                    if cacheClass == 'yeti_cache':
                        autonaming_path = directory_cmd ( localProject_path + act + '/' + sequence + '/' + character + '/'+'data/shotCache/yeti_cache' )
                    elif cacheClass == 'prop':
                        autonaming_path = directory_cmd ( localProject_path + act + '/' + sequence + '/' + character + '/'+'data/shotCache' )

                    scene_path = '%s%s/%s/output'%(project_path, act, sequence)
                    scene = context_info.ContextPathInfo(path = scene_path)
                    reg = register_shot.Register(scene)  
                    cache_hero_path = reg.get.asset(cache)['cache']['heroFile']
                    cache_hero_path_dir = os.path.dirname(cache_hero_path)
                    if cacheClass == 'yeti_cache':
                        copytree(cache_hero_path_dir, autonaming_path)
                    elif cacheClass == 'prop':
                        copy(cache_hero_path, autonaming_path)
                pm.confirmDialog ( message = '%s:%s autonaming completed'%(cacheClass, character), b='OK' )
        
        shot_entity = sg_process.get_shot_entity(scene.project, scene.name)
        task_entity = sg_process.get_one_task(shot_entity, 'sim')
        sg_process.set_task_status(task_entity['id'], 'ip')         

                        


    else : 
        
        pm.confirmDialog ( message = 'incomplete selection' ) ;
        print ( 'incomplete selection' ) ;

    # # directory_cmd ( )




####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
''' UI update / navigate '''
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################

class TextScrollList ( object ) :

    def __init__ ( self, textScrollList ) :
        self.textScrollList = textScrollList ;

    def append ( self , item ) :
        self.item = item ;
        pm.textScrollList ( self.textScrollList , e = True , append = self.item ) ;

    def clear ( self ) :
        pm.textScrollList ( self.textScrollList , e = True , ra = True ) ; 

    def currentItem ( self ) :

        currentItem = pm.textScrollList ( self.textScrollList , q = True , selectItem = True ) ;
        
        if currentItem == [] :
            return None ;
        else :
            if len ( currentItem ) == 1 :
                return currentItem [0] ;
            else :
                return ( currentItem ) ;

    def selectItem ( self , selectItem ) :
        self.selectItem = selectItem ;
        pm.textScrollList ( self.textScrollList , e = True , selectItem = self.selectItem ) ;

    
def listdir ( elem = '' , *args ) :

    browserClass = pm.optionMenu ( 'browserClass_optionMenu' , q = True , value = True ) ;

    if browserClass == 'server' or browserClass == 'server_data':
        target_path = project_work_path ;
    elif browserClass == 'local' :
        target_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;

    act_textScrollList         = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList     = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList         = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList    = TextScrollList ( textScrollList = 'character_textScrollList' ) ;

    if elem == 'act' :
        return os.listdir ( target_path ) ;
    
    elif elem == 'sequence' :
        target_path += act_textScrollList.currentItem() + '/' ;

    elif elem == 'shot' :
        target_path += act_textScrollList.currentItem() + '/' ;
        target_path += sequence_textScrollList.currentItem() + '/' ;

    elif elem == 'character' :

        target_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;

        if act_textScrollList.currentItem() != None :
            target_path += act_textScrollList.currentItem() + '/' ;

            if sequence_textScrollList.currentItem() != None :
                target_path += sequence_textScrollList.currentItem() + '/' ;

        print 'char', target_path
    elif elem == 'cam' :

        target_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;

        if act_textScrollList.currentItem() != None :
            target_path += act_textScrollList.currentItem() + '/' ;

            if sequence_textScrollList.currentItem() != None :
                target_path += sequence_textScrollList.currentItem() + '/' ;
        
                if shot_textScrollList.currentItem() != None :
                    target_path += shot_textScrollList.currentItem() ;
                    target_path += '/output' ;


        else : return None ;

    print "target_path:::: ", target_path
    if os.path.exists ( target_path ) == False :
        return None ;
    else :
        return os.listdir ( target_path ) ;

def updateProjectBrowser_cmd ( elem = '' , *args ) :

    act_textScrollList             = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheFile_textScrollList   = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;

    print "elem:2323245 ", elem

    if elem == 'act' :  

        act_textScrollList.clear ( ) ;
        sequence_textScrollList.clear ( ) ;
        shot_textScrollList.clear ( ) ;

        actItem_list = listdir ( 'act' ) ;
        actItem_list.sort ( ) ;
        for item in actItem_list :
            act_textScrollList.append ( item ) ;

    elif elem == 'sequence' :

        sequence_textScrollList.clear ( ) ;
        shot_textScrollList.clear ( ) ;

        sequenceItem_list = listdir ( 'sequence' ) ;
        print "sequenceItem_list: ", sequenceItem_list
        sequenceItem_list.sort ( ) ;
        for item in sequenceItem_list :
            sequence_textScrollList.append ( item ) ;

    elif elem == 'shot' :

        shot_textScrollList.clear ( ) ;
        
        shotItem_list = listdir ( 'shot' ) ;
        shotItem_list.sort ( ) ;
        for item in shotItem_list :
            shot_textScrollList.append ( item ) ;

    character_textScrollList.clear ( ) ;
    cacheFile_textScrollList.clear ( ) ;

def updateactTextScrollList_cmd ( *args ) :
    updateProjectBrowser_cmd ( elem = 'act' ) ;

def updateSequenceTextScrollList_cmd ( *args ) :
    updateProjectBrowser_cmd ( elem = 'sequence' ) ;

def updateShotTextScrollList_cmd ( *args ) :
    updateProjectBrowser_cmd ( elem = 'shot' ) ;

def updateCharacterTextScrollList_cmd ( *args ) : ######change
    
    act_textScrollList             = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheFile_textScrollList   = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;

    cacheFile_textScrollList.clear ( )
    character_textScrollList.clear ()

    act        = act_textScrollList.currentItem() ;
    sequence    = sequence_textScrollList.currentItem() ;
    shot        = shot_textScrollList.currentItem() ;
    cacheClass      = pm.optionMenu ( 'cacheClass_optionMenu' , q = True , value = True )
    if ( act != None ) and ( sequence != None ) and ( shot != None ):
        charItem_list = listdir ( 'character' )
        scene_path = '%s%s/%s/output'%(project_path, act, sequence)
        scene = context_info.ContextPathInfo(path = scene_path)
        reg = register_shot.Register(scene)
        asset_list = reg.get.asset_list()
        dict_type = dict ()
        for asset in asset_list: 
            path_aseet_des = reg.get.asset(asset)['description']
            context = context_info.ContextPathInfo(path = path_aseet_des)
            file_type = reg.get.asset(asset)['type']
            asset_type = context.type

            if file_type == 'camera':
                dict_type[asset] = 'camera'
            elif  file_type == 'yeti_cache' or file_type == 'abc_tech_cache':
                dict_type[asset] = file_type
            else:
                dict_type[asset] = asset_type
        ############{'canis_001_LongHair': 'yeti_cache', 'canis_001_BodyFlyalway': 'yeti_cache', 'canis_001_Tech': 'abc_tech_cache', 'sackBig_001': 'prop', 'canis_001': 'char', 'canis_001_ShortHair': 'yeti_cache', 'canisWeapon_002': 'prop', 'canisWeapon_001': 'prop', 'canis_001_Braids': 'yeti_cache', 'bambooB_001': 'prop', 'canis_001_Eyebrow': 'yeti_cache', 'canis_001_BodyFur': 'yeti_cache', 'canis_cam': 'camera'}
        ######## 'abc_tech_cache' must be copy with 'char'
        for key,value in dict_type.iteritems():
            if value == cacheClass:
                cacheFile_textScrollList.append ( item = key )
        if charItem_list:
            print charItem_list
            for item in charItem_list :
                character_textScrollList.append ( item )
    else:
        character_textScrollList.clear ( ) ;
        cacheFile_textScrollList.clear ( ) ;


    # if ( act != None ) and ( sequence != None ) and ( shot != None ) :

    #     ### update character list ###
    #     charItem_list = listdir ( 'character' ) ;
    #     camItem_list = listdir ( 'cam' )

    #     if charItem_list != None :

    #         charItem_list.sort ( ) ;

    #         for charItem in charItem_list:
    #             if len(charItem.split('_')) < 2:
    #                 charItem_list.remove(charItem)

    #         character_textScrollList.clear ( ) ;

    #         for item in charItem_list :
    #             character_textScrollList.append ( item ) ;

    #     elif camItem_list != None:
    #         camItem_list.sort()


    #         character_textScrollList.clear ( )

    #         for item in camItem_list :
    #             character_textScrollList.append ( item )
    #     else :

    #         character_textScrollList.clear ( ) ;

    #     ### update charCache list ###
    #     cacheClass = pm.optionMenu ( 'cacheClass_optionMenu' , q = True , value = True ) ;
    #     print 111111
    #     if cacheClass == 'cam':
    #         cacheFile_textScrollList.clear() ;
    #         cachePath = '%s%s/%s/output'%(project_path, act, sequence)

    #         cam = os.listdir(cachePath)
    #         if cam:
    #             cacheFile_textScrollList.append ( item = cam[0] ) ;
    #     else:
    #         # character_textScrollList.clear ( )
    #         cacheFile_textScrollList.clear() ;
    #         cachePath =  project_path ;
    #         cachePath += act + '/' ;   
    #         cachePath += sequence + '/' ;
    #         cachePath += shot + '/' ;
    #         cachePath += cacheClass + '/' ;
            
    #         try :
    #             allCache_list = os.listdir ( cachePath ) ;

    #             cacheChar_list = [] ;

    #             for cache in allCache_list :
    #                 if '_' in cache.split('.')[0] :
    #                     cacheChar_list.append ( cache ) ;

    #             cacheChar_list.sort ( ) ;
                
    #             for char in cacheChar_list :
    #                 cacheFile_textScrollList.append ( item = char ) ;
            
    #         except : pass ;

    # else :            
        
    #     character_textScrollList.clear ( ) ;
    #     cacheFile_textScrollList.clear ( ) ;

def actSelection_cmd ( *args ) :
    updateSequenceTextScrollList_cmd ( ) ;
    saveDefaultSelection_cmd ( ) ;

def sequenceSelection_cmd ( *args ) :
    updateShotTextScrollList_cmd ( ) ;
    saveDefaultSelection_cmd ( ) ;

def shotSelection_cmd ( *args ) :
    updateCharacterTextScrollList_cmd ( ) ;
    saveDefaultSelection_cmd ( ) ;

def characterSelection_cmd ( *args ) :
    saveDefaultSelection_cmd ( ) ;

def browserClassChange_cmd ( *args ) :####chage

    act_textScrollList             = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheFile_textScrollList   = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;

    act            = act_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    character       = character_textScrollList.currentItem ( ) ;
    cacheFile  = cacheFile_textScrollList.currentItem ( ) ;

    updateactTextScrollList_cmd ( ) ;

    if act == None : pass ;
    else : 
        
        try :
            act_textScrollList.selectItem ( act ) ;
            updateSequenceTextScrollList_cmd ( ) ;
        except : pass ;

        if sequence == None : pass ;
        else :
            
            try :
                sequence_textScrollList.selectItem ( sequence ) ;
                updateShotTextScrollList_cmd ( ) ;
            except : pass ;

            if shot == None : pass ;
            else :
            
                try :
                    shot_textScrollList.selectItem ( shot ) ;
                    updateCharacterTextScrollList_cmd ( ) ;
                except : pass ;

                if character == None : pass ;
                else :
                    try : character_textScrollList.selectItem ( character ) ;
                    except : pass ;
                
                if cacheFile == None : pass ;
                else :
                    try : cacheFile_textScrollList.selectItem ( cacheFile ) ;
                    except : pass ;

def browserDoubleClick_cmd ( *args ) :

    act_textScrollList             = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheFile_textScrollList   = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;

    act            = act_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    character       = character_textScrollList.currentItem ( ) ;
    cacheFile  = cacheFile_textScrollList.currentItem ( ) ;

    browserClass    = pm.optionMenu ( 'browserClass_optionMenu' , q = True , value = True ) ;
    #cacheClass      = pm.optionMenu ( 'cacheClass_optionMenu' , q = True , value = True ) ;

    if browserClass == 'server' :
        browse_path = project_path ;
    elif browserClass == 'local' :
        browse_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;

    if act == None : pass ;
    else :
        browse_path += act ;
        
        if sequence == None : pass ;
        else :
            browse_path += '/' + sequence ;

            if shot == None : pass ;
            else :
                browse_path += '/' + shot ;

                if character == None : pass ;
                else : browse_path += '/' + character ;

    os.startfile ( browse_path ) ;

def charDoubleClick_cmd ( *args ) :

    act_textScrollList             = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheFile_textScrollList   = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;

    act            = act_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    character       = character_textScrollList.currentItem ( ) ;
    cacheFile  = cacheFile_textScrollList.currentItem ( ) ;

    browse_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;
    browse_path += act ;
    browse_path += '/' + sequence ;
    browse_path += '/' + shot ;
    browse_path += '/' + character ;

    os.startfile ( browse_path ) ;

def cacheCharDoubleClick_cmd ( *args ) :

    act_textScrollList             = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheFile_textScrollList   = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;

    act            = act_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    character       = character_textScrollList.currentItem ( ) ;
    cacheFile  = cacheFile_textScrollList.currentItem ( ) ;

    cacheClass      = pm.optionMenu ( 'cacheClass_optionMenu' , q = True , value = True ) ;

    browse_path = project_path
    browse_path += act ;
    browse_path += '/' + sequence ;
    browse_path += '/' + shot ;

    browse_path += '/' + cacheClass ;

    os.startfile ( browse_path ) ;

def shortcut_cmd ( elem = '' , *args ) :

    cacheFile_textScrollList   = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;
    cacheFile  = cacheFile_textScrollList.currentItem ( ) ;

    if type ( cacheFile ) == type ( [] ) :
        cacheFile = cacheFile [0] ;

    setup_path = 'P:/Ream/asset/publ/char/' ;

    character = cacheFile.split('.')[0] ;
    character = character.replace( '_' , '/' ) ;
    setup_path += character + '/' + elem + '/maya/hero/' ;

    if os.path.exists ( setup_path ) != True :
        print ( "this character doesn't have " + elem + " setup yet" ) ;
    else :
        os.startfile ( setup_path ) ;

def characterClothSetupShortcut_cmd ( *args ) :
    shortcut_cmd ( elem = 'cloth' ) ;

def characterHairSetupShortcut_cmd ( *args ) :
    shortcut_cmd ( elem = 'hair' ) ;

####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
''' UI '''
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################

def run ( project, *args ) :

    title = 'autonaming g2 v001_001' ;

    width = 700.00 ;
    textScrollHeight = 200.00 ;

    def separator ( times = 1 , *args ) :
        for i in range ( 0 , times ):
            pm.separator ( h = 5 , vis = False ) ;

    def filler ( *args ) :
        pm.text ( label = '' ) ;

    # check if window exists
    if pm.window ( 'autonamingGen2_UI' , exists = True ) :
        pm.deleteUI ( 'autonamingGen2_UI' ) ;
    else : pass ;
   
    window = pm.window ( 'autonamingGen2_UI', title = title ,
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'autonamingGen2_UI' , e = True , w = width , h = 100 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            ###########################
            ''' local path '''
            ###########################

            with pm.frameLayout ( label = 'INITIALIZE PROJECT' , w = width , bgc = ( 0.1 , 0.1 , 0.45 ) ) :

                pm.text ( 'inputPath_text' , label = '...' , bgc = ( 1 , 0.85 , 0 ) ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/6*5 ) , ( 2 , width/6*1 ) ] ) :
                pm.textField ( 'inputPath_textField' , text = '...' ) ;
                pm.button ( label = 'browse' , c = partial(browsePath_cmd, project) ) ;

            ###########################
            ''' project browser layout '''
            ###########################
            
            with pm.frameLayout ( label = 'PROJECT BROWSER' , w = width , bgc = ( 0.1 , 0.1 , 0.45 ) ) :

                with pm.rowColumnLayout ( nc = 1 , w = width ) :

                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                        with pm.optionMenu ( 'browserClass_optionMenu' , label='browser class' , cc = browserClassChange_cmd , w = width/2 ) :
                                pm.menuItem ( label='server' ) ;
                                pm.menuItem ( label='server_data' ) ;
                                pm.menuItem ( label='local' ) ;

                        filler ( ) ;

                    with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , width/4 ) , ( 2 , width/4 ) , ( 3 , width/4 ) , ( 4 , width/4 ) ] ) :

                        with pm.rowColumnLayout ( nc = 1 , w = width/4 ) :
                            pm.text ( label = 'act' , font = 'boldLabelFont' , bgc =  ( 1 , 0 , 0.55 ) , width = width/4 ) ;
                            pm.textScrollList ( 'act_textScrollList' , ams = False , h = textScrollHeight , width = width/4 ,
                                sc = actSelection_cmd ,
                                dcc = browserDoubleClick_cmd ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/4 ) :
                            pm.text ( label = 'SEQUENCE' , font = 'boldLabelFont' , bgc =  ( 1 , 0.4 , 0.7 ) , width = width/4 ) ;
                            pm.textScrollList ( 'sequence_textScrollList' , ams = False , h = textScrollHeight , width = width/4 ,
                                sc = sequenceSelection_cmd ,
                                dcc = browserDoubleClick_cmd ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/4 ) :
                            pm.text ( label = 'Department' , font = 'boldLabelFont' , bgc =  ( 1 , 0.7 , 0.75 ) , width = width/4 ) ;
                            pm.textScrollList ( 'shot_textScrollList' , ams = False , h = textScrollHeight , width = width/4 ,
                                sc = shotSelection_cmd ,
                                dcc = browserDoubleClick_cmd ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/4 ) :
                            pm.text ( label = 'CHARACTER' , font = 'boldLabelFont' , bgc =  ( 0.1 , 0.55 , 0.15 ) , width = width/4 ) ;
                            pm.textScrollList ( 'character_textScrollList' , ams = False , h = textScrollHeight , width = width/4 ,
                                sc = characterSelection_cmd ,
                                dcc = charDoubleClick_cmd ) ;
                    
                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        filler ( ) ;
                        pm.button ( label = 'set project' , w = width/2 , c = setProject_cmd , bgc = ( 0.65 , 1 , 0.2 ) ) ;
            
            ###########################
            ''' autonaming layout '''
            ###########################

            with pm.frameLayout ( label = 'AUTONAMING LAYOUT' , w = width , bgc = ( 0.1 , 0.1 , 0.45 ) ) :

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                    ### LEFT ###
                    with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :

                        ###########################
                        ''' browse cache layout '''
                        ###########################
                        with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :
            
                            with pm.optionMenu ( 'cacheClass_optionMenu' , label = 'cache class' , cc = updateCharacterTextScrollList_cmd , w = width/2 ) :
                                pm.menuItem ( label='char' ) ;
                                pm.menuItem ( label='yeti_cache' ) ;
                                pm.menuItem ( label='prop' ) ;
                                
                            pm.textScrollList ( 'cacheFile_textScrollList' , ams = True , h = textScrollHeight , width = width/2 ,
                                sc = characterSelection_cmd ,
                                dcc = cacheCharDoubleClick_cmd ) ;

                    ### RIGHT ###
                    with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :

                        ###########################
                        ''' shotcuts layout '''
                        ###########################

                        separator ( 4 ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :

                            pm.text ( label = 'shortcuts' ) ;

                            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/4 ) , ( 2 , width/4 ) ] ) :
                                pm.button ( label = 'cloth setup' , c = characterClothSetupShortcut_cmd ) ;
                                pm.button ( label = 'hair setup' , c = characterHairSetupShortcut_cmd ) ;

                        separator ( 20 ) ;

                        ###########################
                        ''' autonaming layout '''
                        ###########################
                        with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :

                            with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/2/4 ) , ( 2 , width/2/2 ) , ( 3 , width/2/4 ) ] ) :
                                filler ( ) ;
                                with pm.rowColumnLayout ( nc = 1 , w = width/2/3 ) :
                                    pm.checkBox ( 'characterSetup_cbx' , label = 'character setup' , value = True ) ;
                                    pm.checkBox ( 'cache_cbx' , label = 'cache (cam/char/prop)' , value = True ) ;
                                filler ( ) ;

                            pm.button ( label = 'autonaming' , c = partial(autonamingBtn_cmd, project) , bgc = ( 1 , 0.85 , 0 ) ) ;

    updateactTextScrollList_cmd ( ) ;
    setDefaultPath_cmd ( ) ;
    setDefaultSelection_cmd ( ) ;

    window.show () ;

def toShelf ( *args ) :

    # self_path = D:/Dropbox/script/birdScript/projects/twoHeroes/

    image_path = self_path + 'media/autonaming_icon.png'

    commandHeader = '''
import sys ;

mp = "%s" ;
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
''' % self_path.split ('/projects/')[0] ;
# D:/Dropbox/script/birdScript

    cmd = commandHeader ;
    cmd += '''
import projects.Ream.autonaming_generation2 as autonaming_generation2 ;
reload ( autonaming_generation2 ) ;
project = 'Ream'
autonaming_generation2.run ( project ) ;
'''
    mel_cmd = '''
global string $gShelfTopLevel;
string $shelves = `tabLayout -q -selectTab $gShelfTopLevel`;
'''
    currentShelf = pm.mel.eval ( mel_cmd );
    pm.shelfButton ( style = 'iconOnly' , image = image_path , command = cmd , parent = currentShelf ) ;