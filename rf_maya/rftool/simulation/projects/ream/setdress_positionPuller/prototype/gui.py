import pymel.core as pm ;

class PositionPullerGUI ( object ) :

    def __init__ ( self ) :
        self.ui         = 'positionPullerGUI_ui' ;
        self.width      = 300.00 ;
        self.title      = 'Position Puller' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def show ( self ) :

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = self.width , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = self.width ) ;
            with main_layout :

                pm.text ( label = 'Ori_Grp :' , w = self.width ) ;
                self.oriGrpTextField = pm.textField ( w = self.width ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , self.width/2 ) , ( 2 , self.width/2 ) ] ) :
                    pm.button ( label = 'Refresh' ) ;
                    pm.button ( label = 'Import Ori_Grp' ) ;

                pm.separator ( vis = False , h = 5 ) ;

                pm.button ( label = 'Pull Position' ) ;

                pm.separator ( vis = False , h = 5 ) ;
                
                pm.button ( label = 'Import Yeti' ) ;

        window.show () ;

def run ( *args ) :
    gui = PositionPullerGUI() ;
    gui.show() ;