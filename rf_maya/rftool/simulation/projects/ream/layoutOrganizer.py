import pymel.core as pm ;
import maya.cmds as mc ;
import os ;

self_path = os.path.realpath (__file__) ;
self_name = os.path.basename (__file__) ;
self_path = self_path.replace ( self_name , '' ) ;
self_path = self_path.replace ( '\\' , '/' ) ;
#D:/Dropbox/script/birdScript/projects/twoHeroes/

###########################
###########################
###########################
''' layout organizer '''
###########################
###########################
###########################

def createGrp_cmd ( name = '' , *args ) :
    if pm.objExists ( name ) == True :
        grp = pm.general.PyNode ( name ) ;
    else :
        grp = pm.group ( em = True , w = True , n = name ) ;
    return grp ;

def findRefTopNode_cmd ( node = '' , *args ) : 
    parent = pm.listRelatives ( node , p = True ) ;
    if parent != [] :
        if pm.referenceQuery ( parent , isNodeReferenced = True ) == True :  
            return ( findRefTopNode_cmd ( node = parent ) ) ;      
        else: return node ;
    else: return node ;

def organizeReferences_cmd ( *args ) :

    ### get reference path ###
    referencePath_list = mc.file ( q = True , r = True ) ;

    ### organize reference path ###
    camRef_list     = [] ;
    charRef_list    = [] ;
    setRef_list     = [] ;
    propRef_list     = [] ;
    miscRef_list    = [] ;

    for path in referencePath_list :

        if pm.referenceQuery ( path , isLoaded = True ) != True :
            pass ;
        
        elif ( '/camera/' in path ) or ( '/_cam/' in path ) :
            camRef_list.append ( path ) ;
        
        elif ( '/char/' in path ) or ( '/character/' in path ) :
            charRef_list.append ( path ) ;
            
        elif '/set/' in path :
            setRef_list.append ( path ) ;

        elif '/prop/' in path :
            propRef_list.append ( path ) ;

        else :
            miscRef_list.append ( path ) ;

    def groupReference_cmd ( group = '' , refList = [] , *args ) :

        target_list = [] ;

        for each in refList :
            if pm.referenceQuery ( each , isLoaded = True ) == True :
                ns = mc.referenceQuery ( each , ns = True ) ;
                childs = pm.ls ( '%s:*' % ns , type = 'transform' , l = True ) ;
                target_list.append ( findRefTopNode_cmd ( node = childs[0] ) ) ;
            else :
                pass ;

        target_list.sort ( ) ;
          
        for each in target_list :

            if each not in [ 'cam_grp' , 'char_grp' , 'set_grp' , 'prop_grp' , 'misc_grp' , 'uncategorized_grp' ] :

                parent = pm.listRelatives ( each , p = True ) ;

                if parent : parent = parent [0] ;

                if parent == group : pass ;
                else : pm.parent ( each , group ) ;

            else : pass ;

    ### cam  ###
    cam_grp = createGrp_cmd ( name = 'cam_grp' ) ;
    groupReference_cmd ( group = cam_grp , refList = camRef_list ) ;

    ### char  ###
    char_grp = createGrp_cmd ( name = 'char_grp' ) ;
    groupReference_cmd ( group = char_grp , refList = charRef_list ) ;

    ### set  ###
    set_grp = createGrp_cmd ( name = 'set_grp' ) ;
    groupReference_cmd ( group = set_grp , refList = setRef_list ) ;

    ### prop  ###
    prop_grp = createGrp_cmd ( name = 'prop_grp' ) ;
    groupReference_cmd ( group = prop_grp , refList = propRef_list ) ;

    ### prop  ###
    misc_grp = createGrp_cmd ( name = 'misc_grp' ) ;
    groupReference_cmd ( group = misc_grp , refList = miscRef_list ) ;

    ### else ###
    uncategorized_grp = createGrp_cmd ( name = 'uncategorized_grp' ) ;
    
    outliner_list = pm.ls ( assemblies = True ) ;
    
    outliner_list.remove ( cam_grp ) ;
    outliner_list.remove ( char_grp ) ;
    outliner_list.remove ( set_grp ) ;
    outliner_list.remove ( prop_grp ) ;
    outliner_list.remove ( misc_grp ) ;
    outliner_list.remove ( uncategorized_grp ) ;

    camFront    = pm.general.PyNode ( 'persp' ) ;
    camPersp    = pm.general.PyNode ( 'top' ) ;
    camSide     = pm.general.PyNode ( 'front' ) ;
    camTop      = pm.general.PyNode ( 'side' ) ;

    outliner_list.remove ( camFront ) ;
    outliner_list.remove ( camPersp ) ;
    outliner_list.remove ( camSide ) ;
    outliner_list.remove ( camTop ) ;
    
    for each in outliner_list :
        try : 
            pm.parent ( each , uncategorized_grp ) ;
        except :
            print ( each + ' is not valid?' ) ;

    globalCheck_cmd ( ) ;

##################
##################
''' camera '''
##################
##################

def getCamList_cmd ( *args ) :

    allCam_list = pm.ls ( cameras = True ) ;
    cam_list = [] ;
    
    for cam in allCam_list :
        if cam == 'frontShape' or cam == 'perspShape' or cam == 'sideShape' or cam == 'topShape' :
            pass ;
        else :
            cam_list.append ( cam ) ;

    return cam_list ;

def cameraCheck_cmd ( *args ) :
    
    cam_list = getCamList_cmd ( ) ;
   
    # cameraCheck1_log

    if len ( cam_list ) == 0 :
        pm.text ( 'cameraCheck1_log' , e = True , label = 'no camera detected' , bgc = ( 1 , 0 , 0 ) ) ;
        pm.text ( 'cameraCheck2_log' , e = True , label = 'please check above condition first' , bgc = ( 1 , 0 , 0 ) ) ;
    elif len ( cam_list ) == 1 :
        pm.text ( 'cameraCheck1_log' , e = True , label = 'OK!' , bgc = ( 0 , 1 , 0 ) ) ;
    else :
        pm.text ( 'cameraCheck1_log' , e = True , label = 'multiple cameras detected' , bgc = ( 1 , 0 , 0 ) ) ;
        pm.text ( 'cameraCheck2_log' , e = True , label = 'please check above condition first' , bgc = ( 1 , 0 , 0 ) ) ;

    # cameraCheck2_log

    if len ( cam_list ) == 1 :

        cam_tfm = pm.listRelatives ( cam_list[0] , parent = True ) [0] ;
        
        topNode = mc.ls ( str ( cam_tfm ) , long = True ) [0] ;
        topNode = topNode.split ( '|' ) [1] ;

        if topNode != 'cam_grp' : 
            pm.text ( 'cameraCheck2_log' , e = True , label = 'camera is not in cam_grp' , bgc = ( 1 , 0 , 0 ) ) ;
            pm.button ( 'cameraCheck2Fix_btn' , e = True , label = 'FIX' , enable = True ) ;

        else :
            pm.text ( 'cameraCheck2_log' , e = True , label = 'OK!' , bgc = ( 0 , 1 , 0 ) ) ;

def cameraCheck2Fix_cmd ( *args ) :

    if pm.objExists ( 'cam_grp' ) == True :
        cam_grp = pm.general.PyNode ( 'cam_grp' ) ;
    else :
        cam_grp = pm.group ( em = True , w = True , n = 'cam_grp' ) ;

    cam_list = getCamList_cmd ( ) ;
    cam_tfm = pm.listRelatives ( cam_list[0] , parent = True ) [0] ;
    
    topNode = mc.ls ( str ( cam_tfm ) , long = True ) [0] ;
    topNode = topNode.split ( '|' ) [1] ;

    pm.parent ( topNode , cam_grp ) ;

    pm.button ( 'cameraCheck2Fix_btn' , e = True , label = 'FIX' , enable = False ) ;

    cameraCheck_cmd ( ) ;

##################
##################
''' reference from doublemonkey '''
##################
##################

def replaceRef_cmd ( srcPath , dstPath , *args ) :
    rnNode = mc.referenceQuery ( srcPath , referenceNode = True ) ; 
    mc.file ( dstPath , loadReference = rnNode ) ;

'''    
example :

replace_reference (
    srcPath = 'P:/Doublemonkeys/all/asset/char/main/sunnySRU/hero/sunnySRU_med.ma' ,
    dstPath = 'P:/Two_Heroes/asset/character/SRU/sunnySRU/lib/sunnySRU_rig_md.ma' ) ;
'''

def doublemonkeysRefCheck_cmd ( *args ) :

    doublemonkeyRef_stat = False ;

    ref_list = mc.file ( q = True , r = True ) ;

    charRef_list = [] ;

    for path in ref_list :
        
        if pm.referenceQuery ( path , isLoaded = True ) != True :
            pass ;
        elif ( '/char/' in str ( path ) ) or ( '/character/' in str ( path ) ) :
            charRef_list.append ( path ) ;

    for path in charRef_list :
        print path ;
        
        if '/Doublemonkeys/' in str(path) :
            doublemonkeyRef_stat = True ;
        
        else : pass ;

        if doublemonkeyRef_stat == True :
            pm.text ( 'doublemonkeysRef_log' , e = True , label = "char ref from 'doublemonkey' detected" , bgc = ( 1 , 0 , 0 ) ) ;
            pm.button ( 'doublemonkeysRef_btn' , e = True , label = 'FIX' , enable = True ) ; 
        else :
            pm.text ( 'doublemonkeysRef_log' , e = True , label = 'OK!' , bgc = ( 0 , 1 , 0 ) ) ;

def doublemonkeysRefFix_cmd ( *args ) :
    
    charDictTxt_file = open ( self_path + 'asset/characterTrackDict.txt' , 'r' ) ;
    charDict_txt = charDictTxt_file.read ( ) ;
    charDictTxt_file.close ( ) ;

    exec ( charDict_txt ) ;

    ###

    ref_list = mc.file ( q = True , r = True ) ;

    charRef_list            = [] ;
    doubleMonkeyRef_list    = [] ;

    for path in ref_list :
        if pm.referenceQuery ( path , isLoaded = True ) != True :
            pass ;
        elif ( '/char/' in str ( path ) ) or ( '/character/' in str ( path ) ) :
            charRef_list.append ( path ) ;

    for path in charRef_list :
        if '/Doublemonkeys/' in str(path) :
            doubleMonkeyRef_list.append ( path ) ;

    dmChar_list = [] ;

    ### check if character in bird's database ###
    for path in doubleMonkeyRef_list :        
        dmChar = path.split('/char/')[1] ;
        dmChar = dmChar.split('/hero/')[0] ;
        dmChar_list.append ( dmChar ) ;

    for dmChar in dmChar_list :
        try : print char_dict[dmChar] ;
        except :
            pm.text ( 'doublemonkeysRef_log' , e = True , label = dmChar + ' not in database, please contact bird' , bgc = ( 1 , 0 , 0 ) ) ;
            pm.error ( dmChar + ' not in database, please contact bird' ) ;

    ### begin replace operation ###
    for path in doubleMonkeyRef_list :        
        dmChar = path.split('/char/')[1] ;
        dmChar = dmChar.split('/hero/')[0] ;

        # rig type = med , proxy
        if ( '/' + dmChar.split('/')[1] + '_med' ) in path :
            rigType = 'med' ;
        elif ( '/' + dmChar.split('/')[1] + '_proxy' ) in path :
            rigType = 'proxy' ;
        else :
            pm.text ( 'doublemonkeysRef_log' , e = True , label = 'rig type not recognized, please contact bird' , bgc = ( 1 , 0 , 0 ) ) ;
            pm.error ( 'rig type not recognized, please contact bird' ) ;

        thChar = char_dict[dmChar] ;

        if rigType == 'med' :
            rigName = thChar.split('/')[1] + '_rig_md.ma' ;
        elif rigType == 'proxy' :
            rigName = thChar.split('/')[1] + '_rig_pr.ma' ;

        ### check if rig exists ###
        rigPath =  'P:/Two_Heroes/asset/character/' + thChar + '/lib/' + rigName ;

        if os.path.isfile ( rigPath ) != True :
            # print rigPath ;
            pm.text ( 'doublemonkeysRef_log' , e = True , label ='please check ' + rigName + 'location, please contact bird' , bgc = ( 1 , 0 , 0 ) ) ;
            pm.error ( 'please check ' + rigName + 'location, please contact bird' ) ;
        else :
            replaceRef_cmd ( srcPath = path , dstPath = rigPath ) ;

    doublemonkeysRefCheck_cmd ( ) ;

    # D:/Dropbox/script/birdScript/projects/twoHeroes/

##################
##################
''' namespace '''
##################
##################

def reorder_cmd ( target = '' , *args ) :

    children_list = pm.listRelatives ( target ) ;
    children_list.sort ( ) ;

    for each in children_list :
        pm.reorder ( each , back = True ) ;

def fixCharNS_cmd ( *args ) :

    ref_list = mc.file ( q = True , r = True ) ;
    
    charRef_list = [] ;
 
    for path in ref_list :

        if pm.referenceQuery ( path , isLoaded = True ) != True :
            
            pass ;
        
        elif ( '/char/' in str ( path ) ) or ( '/character/' in str ( path ) ) :
        
            if '/char/' in str ( path ) :
                char = '/char/' ;
            elif '/character/' in str ( path ) :
                char = '/character/' ;
            
            charInfo = [] ;
                        
            if '{' and '}' in str ( path ) :
                index = path.split('{')[1].split('}')[0] ;
                index = int ( index ) ;
                index += 1 ;
            else :
                index = 1 ;
                
            character = path.split(char)[1] ;
            character = character.split('/')[1] ;
            
            index = str(index) ;
            index = index.zfill(3) ;

            #namespace = character+index+'RN' ;
            namespace = character+'_'+index ;

            oldNamespace = mc.file ( path , q = True , namespace = True ) ;

            #print ( oldNamespace , namespace ) ;

            if oldNamespace == namespace :
                pass ;
            else :
                mc.file ( path , e = True , namespace = namespace ) ;

            reorder_cmd ( target = 'char_grp' ) ;

def namespaceCheck_cmd ( *args ) :

    references_list = [] ;

    char_list = pm.listRelatives ( 'char_grp' , children = True ) ;
    set_list = pm.listRelatives ( 'set_grp' , children = True ) ;

    references_list.extend ( char_list ) ;
    references_list.extend ( set_list ) ;

    count = 0 ;

    for each in references_list :
        if each.count(':') > 1 :
            count += 1 ;

    if count > 0 :
        pm.text ( 'namespaceCheck1_log' , e = True , label = 'double namespace detected' , bgc = ( 1 , 0 , 0 ) ) ;
        pm.button ( 'namespaceCheck1Fix_btn' , e = True , label = 'FIX' , enable = True ) ; 
    else :
        pm.text ( 'namespaceCheck1_log' , e = True , label = 'OK!' , bgc = ( 0 , 1 , 0 ) ) ;
             
def namespaceCheck1Fix_cmd ( *args ) :

    references_list = [] ;

    char_list = pm.listRelatives ( 'char_grp' , children = True ) ;
    set_list = pm.listRelatives ( 'set_grp' , children = True ) ;

    references_list.extend ( char_list ) ;
    references_list.extend ( set_list ) ;

    #print references_list ;

    doubleNamespace_list = [] ;

    for each in references_list :
        if each.count(':') > 1 :
            namespace = each.split(':')[0] ;
            if namespace[0] == '|' :
                namespace = namespace[1:] ;
            doubleNamespace_list.append ( namespace ) ;
        else :
            pass ;

    doubleNamespace_list = set ( doubleNamespace_list ) ;
    doubleNamespace_list = list ( doubleNamespace_list ) ;

    #print doubleNamespace_list ;

    for each in doubleNamespace_list :
        pm.namespace( mv = [ each , ":" ] , force = True ) ;

    pm.button ( 'namespaceCheck1Fix_btn' , e = True , label = 'FIX' , enable = False ) ;

    namespaceCheck_cmd ( ) ;

##################
##################
##################
''' UI '''
##################
##################
##################

def globalCheck_cmd ( *args ) :

    cameraCheck_cmd ( ) ;
    namespaceCheck_cmd ( ) ;
    doublemonkeysRefCheck_cmd ( ) ;

width = 700.00 ;

def separator ( *args ) :
    pm.separator ( h = 5 , vis = False ) ;

def run ( ) :
    # check if window exists
    if pm.window ( 'layoutOrganizer_UI' , exists = True ) :
        pm.deleteUI ( 'layoutOrganizer_UI' ) ;
    else : pass ;

    window = pm.window ( 'layoutOrganizer_UI', title = "two heroes layout organizer v1.0" ,
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    pm.window ( 'layoutOrganizer_UI' , e = True , w = width , h = 10 ) ;
    
    with window :

        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            pm.button ( label = 'organize references' , c = organizeReferences_cmd ) ;
            pm.button ( label = 'fix character namespace' , c = fixCharNS_cmd ) ;
            
            separator ( ) ;

            ### reference ###
            with pm.frameLayout ( label = 'reference' , collapsable = False , collapse = False , width = width , bgc = ( 0 , 0 , 0.5 ) ) :              
                with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/4 ) , ( 2 , width/2 ) , ( 3 , width/4 ) ] ) :
                    pm.text ( label = 'char ref from Doublemonkeys' ) ;
                    pm.text ( 'doublemonkeysRef_log' , label = '...' ) ;
                    pm.button ( 'doublemonkeysRef_btn' , label = 'FIX' , enable = False , c = doublemonkeysRefFix_cmd ) ;

            ### camera ###
            with pm.frameLayout ( label = 'camera' , collapsable = False , collapse = False , width = width , bgc = ( 0 , 0 , 0.5 ) ) :              
                with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/4 ) , ( 2 , width/2 ) , ( 3 , width/4 ) ] ) :
                    
                    pm.text ( label = 'single camera' ) ;
                    pm.text ( 'cameraCheck1_log' , label = '...' ) ;
                    pm.button ( 'cameraCheck1Fix_btn' , label = 'FIX' , enable = False ) ;

                    pm.text ( label = 'camera in "cam_grp"' ) ;
                    pm.text ( 'cameraCheck2_log' , label = '...' ) ;
                    pm.button ( 'cameraCheck2Fix_btn' , label = 'FIX' , enable = False , c = cameraCheck2Fix_cmd ) ;

            ### namespace ###
            with pm.frameLayout ( label = 'namespace' , collapsable = False , collapse = False , width = width , bgc = ( 0 , 0 , 0.5 ) ) :              
                with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/4 ) , ( 2 , width/2 ) , ( 3 , width/4 ) ] ) :

                    pm.text ( label = 'double namespace' ) ;
                    pm.text ( 'namespaceCheck1_log' , label = '...' ) ;
                    pm.button ( 'namespaceCheck1Fix_btn' , label = 'FIX' , enable = False , c = namespaceCheck1Fix_cmd ) ;

            pm.button ( label = 'check' , width = width , c = globalCheck_cmd ) ;

    window.show () ;

def toShelf ( *args ) :

    # self_path = D:/Dropbox/script/birdScript/projects/twoHeroes/

    image_path = self_path + 'media\layoutOrganizer_icon.png'

    commandHeader = '''
import sys ;

mp = "%s" ;
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
''' % self_path.split ('/projects/')[0] ;
# D:/Dropbox/script/birdScript

    cmd = commandHeader ;
    cmd += '''
import projects.twoHeroes.layoutOrganizer as twoHeroes_layoutOrganizer ;
reload ( twoHeroes_layoutOrganizer ) ;
twoHeroes_layoutOrganizer.run ( ) ;
'''
    mel_cmd = '''
global string $gShelfTopLevel;
string $shelves = `tabLayout -q -selectTab $gShelfTopLevel`;
'''
    currentShelf = pm.mel.eval ( mel_cmd );
    pm.shelfButton ( style = 'iconOnly' , image = image_path , command = cmd , parent = currentShelf ) ;