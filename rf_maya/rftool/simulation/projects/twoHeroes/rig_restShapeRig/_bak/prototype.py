import pymel.core as pm ;

# jnt_list = pm.ls ( sl = True ) ;
# jntA = jnt_list[0] ;
# jntB = jnt_list[1] ;

jntA = pm.createNode ( 'joint' , name = 'jointA' ) ;
jntB = pm.createNode ( 'joint' , name = 'jointB' ) ;
# jntB = jnt_list[1] ;

jntA_dmn = pm.createNode ( 'decomposeMatrix' , n = jntA + '_dmn' ) ;
jntA.worldMatrix >> jntA_dmn.inputMatrix  ;

jntB_dmn = pm.createNode ( 'decomposeMatrix' , n = jntB + '_dmn' ) ;
jntB.worldMatrix >> jntB_dmn.inputMatrix  ;

dbn = pm.createNode ( 'distanceBetween' ) ;

jntA_dmn.outputTranslate >> dbn.point1 ;
jntB_dmn.outputTranslate >> dbn.point2 ;

print dbn.distance.get() ;