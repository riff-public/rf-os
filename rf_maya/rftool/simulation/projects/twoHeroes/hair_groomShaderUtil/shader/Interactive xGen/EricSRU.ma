//Maya ASCII 2017ff05 scene
//Name: EricSRU.ma
//Last modified: Sun, May 06, 2018 03:30:33 AM
//Codeset: 1252
requires maya "2017ff05";
requires -nodeType "hairPhysicalShader" "hairPhysicalShader" "1.0";
requires "stereoCamera" "10.0";
requires "PO_Reader_v2012_64" "1.0";
requires "TurtleForMaya2009" "5.0.0.5";
requires "pfOptions.py" "1.0";
requires "arkMayaExporter_2013_5" "1.21.ma3.s2.me5.a7";
currentUnit -l centimeter -a degree -t pal;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
createNode hairPhysicalShader -n "EricSRU_XgS";
	rename -uid "AB311F0C-4A93-32F2-43C0-21BED8D336C6";
	setAttr ".rcD" -type "float3" 0.015996011 0.005605381 0 ;
	setAttr ".tcD" -type "float3" 0.063011274 0.022174019 0 ;
	setAttr ".iD" 0.6577380895614624;
	setAttr ".ac" -type "float3" 0.23511904 0.23511904 0.23511904 ;
	setAttr ".iR" 0.2589285671710968;
	setAttr ".lsR" -4.7619047164916992;
	setAttr ".lwR" 3.2738094329833984;
	setAttr ".cTRT" -type "float3" 0.70238096 0.30807847 0.11044308 ;
	setAttr ".lsTRT" -2.4404761791229248;
	setAttr ".iG" 0.1428571492433548;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 145;
	setAttr -av -k on ".unw" 145;
	setAttr -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -k on ".ihi";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".hwi" yes;
	setAttr ".tmr" 2048;
	setAttr -av ".aoam";
	setAttr -av ".aora";
	setAttr -av ".mbe";
	setAttr -av -k on ".mbsof";
	setAttr ".msaa" yes;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	addAttr -ci true -h true -sn "aal" -ln "attributeAliasList" -dt "attributeAlias";
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr ".aal" -type "attributeAlias" {"ai_aov_N","aiCustomAOVs[0]","ai_aov_direct_specular_2"
		,"aiCustomAOVs[10]","ai_aov_indirect_backlight","aiCustomAOVs[11]","ai_aov_indirect_diffuse"
		,"aiCustomAOVs[12]","ai_aov_indirect_diffuse_raw","aiCustomAOVs[13]","ai_aov_indirect_specular"
		,"aiCustomAOVs[14]","ai_aov_indirect_specular_2","aiCustomAOVs[15]","ai_aov_refraction"
		,"aiCustomAOVs[16]","ai_aov_single_scatter","aiCustomAOVs[17]","ai_aov_sss","aiCustomAOVs[18]"
		,"ai_aov_uv","aiCustomAOVs[19]","ai_aov_P","aiCustomAOVs[1]","ai_aov_depth","aiCustomAOVs[20]"
		,"ai_aov_id_1","aiCustomAOVs[21]","ai_aov_id_8","aiCustomAOVs[22]","ai_aov_id_2","aiCustomAOVs[23]"
		,"ai_aov_direct_glint","aiCustomAOVs[24]","ai_aov_direct_global","aiCustomAOVs[25]"
		,"ai_aov_direct_local","aiCustomAOVs[26]","ai_aov_direct_transmission","aiCustomAOVs[27]"
		,"ai_aov_indirect_glint","aiCustomAOVs[28]","ai_aov_indirect_global","aiCustomAOVs[29]"
		,"ai_aov_Z","aiCustomAOVs[2]","ai_aov_indirect_local","aiCustomAOVs[30]","ai_aov_indirect_transmission"
		,"aiCustomAOVs[31]","ai_aov_id_3","aiCustomAOVs[32]","ai_aov_crypto_asset","aiCustomAOVs[3]"
		,"ai_aov_crypto_material","aiCustomAOVs[4]","ai_aov_diffuse_color","aiCustomAOVs[5]"
		,"ai_aov_direct_backlight","aiCustomAOVs[6]","ai_aov_direct_diffuse","aiCustomAOVs[7]"
		,"ai_aov_direct_diffuse_raw","aiCustomAOVs[8]","ai_aov_direct_specular","aiCustomAOVs[9]"
		} ;
select -ne :initialParticleSE;
	addAttr -ci true -h true -sn "aal" -ln "attributeAliasList" -dt "attributeAlias";
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr ".aal" -type "attributeAlias" {"ai_aov_N","aiCustomAOVs[0]","ai_aov_direct_specular_2"
		,"aiCustomAOVs[10]","ai_aov_indirect_backlight","aiCustomAOVs[11]","ai_aov_indirect_diffuse"
		,"aiCustomAOVs[12]","ai_aov_indirect_diffuse_raw","aiCustomAOVs[13]","ai_aov_indirect_specular"
		,"aiCustomAOVs[14]","ai_aov_indirect_specular_2","aiCustomAOVs[15]","ai_aov_refraction"
		,"aiCustomAOVs[16]","ai_aov_single_scatter","aiCustomAOVs[17]","ai_aov_sss","aiCustomAOVs[18]"
		,"ai_aov_uv","aiCustomAOVs[19]","ai_aov_P","aiCustomAOVs[1]","ai_aov_depth","aiCustomAOVs[20]"
		,"ai_aov_id_1","aiCustomAOVs[21]","ai_aov_id_8","aiCustomAOVs[22]","ai_aov_id_2","aiCustomAOVs[23]"
		,"ai_aov_direct_glint","aiCustomAOVs[24]","ai_aov_direct_global","aiCustomAOVs[25]"
		,"ai_aov_direct_local","aiCustomAOVs[26]","ai_aov_direct_transmission","aiCustomAOVs[27]"
		,"ai_aov_indirect_glint","aiCustomAOVs[28]","ai_aov_indirect_global","aiCustomAOVs[29]"
		,"ai_aov_Z","aiCustomAOVs[2]","ai_aov_indirect_local","aiCustomAOVs[30]","ai_aov_indirect_transmission"
		,"aiCustomAOVs[31]","ai_aov_id_3","aiCustomAOVs[32]","ai_aov_crypto_asset","aiCustomAOVs[3]"
		,"ai_aov_crypto_material","aiCustomAOVs[4]","ai_aov_diffuse_color","aiCustomAOVs[5]"
		,"ai_aov_direct_backlight","aiCustomAOVs[6]","ai_aov_direct_diffuse","aiCustomAOVs[7]"
		,"ai_aov_direct_diffuse_raw","aiCustomAOVs[8]","ai_aov_direct_specular","aiCustomAOVs[9]"
		} ;
select -ne :defaultRenderGlobals;
	addAttr -ci true -sn "shave_old_preRenderMel" -ln "shave_old_preRenderMel" -dt "string";
	addAttr -ci true -sn "shave_old_postRenderMel" -ln "shave_old_postRenderMel" -dt "string";
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr" 25;
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -av -k on ".edl" no;
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf" 51;
	setAttr -cb on ".imfkey" -type "string" "exr";
	setAttr -av -k on ".gama";
	setAttr -k on ".an" yes;
	setAttr -k on ".ar";
	setAttr -k on ".fs" 11;
	setAttr -k on ".ef" 11;
	setAttr -av -k on ".bfs";
	setAttr -k on ".me";
	setAttr -k on ".se";
	setAttr -av -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -k on ".oft";
	setAttr -k on ".umfn";
	setAttr -k on ".ufe";
	setAttr -cb on ".pff" yes;
	setAttr -k on ".peie" 2;
	setAttr -cb on ".ifp" -type "string" "lookdev/sunnySRU/<RenderLayer>/<RenderLayer>";
	setAttr -k on ".rv";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mot";
	setAttr -av -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".mbso";
	setAttr -k on ".mbsc";
	setAttr -av -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm" -type "string" "";
	setAttr -cb on ".pom" -type "string" "";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -av -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -k on ".rcp";
	setAttr -k on ".icp";
	setAttr -k on ".ocp";
	setAttr -cb on ".hbl";
	setAttr ".shave_old_preRenderMel" -type "string" "";
	setAttr ".shave_old_postRenderMel" -type "string" "";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h" 1100;
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar" 0.87272727489471436;
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr" 25;
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
connectAttr "EricSRU_XgS.msg" ":defaultShaderList1.s" -na;
connectAttr "EricSRU_XgS.oc" ":internal_standInSE.ss";
// End of EricSRU.ma
