//Maya ASCII 2017ff05 scene
//Name: Sunny.ma
//Last modified: Sun, May 06, 2018 01:47:51 PM
//Codeset: 1252
requires maya "2017ff05";
requires -nodeType "RedshiftHair" -nodeType "RedshiftHairRandomColor" -nodeType "RedshiftHairPosition"
		 "redshift4maya" "2.5.40";
requires -dataType "pgYetiMayaData" -dataType "pgYetiMayaStrandData" -dataType "pgYetiMayaFeatherData"
		 "pgYetiMaya" "2.2.1";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode RedshiftHair -n "Sunny_YtS";
	rename -uid "D09A2B6B-4F45-FBC8-55EE-FCAF6E700A2C";
	setAttr ".irefl_color" -type "float3" 0.0099999998 0.0049999999 0.0019999999 ;
	setAttr ".irefl_weight" 1;
	setAttr ".irefl_gloss" 0.80000001192092896;
	setAttr ".irefl_samples" 32;
	setAttr ".diffuse_weight" 0.60000002384185791;
	setAttr ".trans_color" -type "float3" 0.06666667 0.033333335 0.013333337 ;
	setAttr ".trans_weight" 0.75;
	setAttr ".refl_color" -type "float3" 0.50370371 0.50370371 0.50370371 ;
	setAttr ".refl_weight" 0.05000000074505806;
	setAttr ".refl_gloss" 0.80000001192092896;
	setAttr ".refl_samples" 32;
	setAttr ".angularShift" -2.5;
	setAttr ".depth_override" yes;
	setAttr ".refl_enablecutoff" yes;
	setAttr ".trans_enablecutoff" yes;
createNode RedshiftHairRandomColor -n "rsHairRandomColor1";
	rename -uid "C8A88F70-4CD9-B109-7142-B6B77208DB9E";
	setAttr ".color" -type "float3" 0.0099999998 0.0049999999 0.0019999999 ;
	setAttr ".hueAmount" 0.20000000298023224;
createNode ramp -n "ramp1";
	rename -uid "CA9039A8-404C-30C2-71E0-A4BAF37E9CCC";
	setAttr -s 2 ".cel";
	setAttr ".cel[1].ep" 0.89999997615814209;
	setAttr ".cel[1].ec" -type "float3" 0 0 0 ;
	setAttr ".cel[2].ep" 1;
	setAttr ".cel[2].ec" -type "float3" 1 1 1 ;
createNode RedshiftHairPosition -n "rsHairPosition1";
	rename -uid "3F067DA7-4C05-8647-9DA8-DA9A5995E20C";
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av -k on ".unw" 1;
	setAttr -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -k on ".ihi";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".hwi" yes;
	setAttr ".etmr" yes;
	setAttr ".tmr" 2048;
	setAttr -av ".aoam";
	setAttr -av ".aora";
	setAttr -av ".mbe";
	setAttr -av -k on ".mbsof";
	setAttr ".msaa" yes;
	setAttr ".aasc" 16;
	setAttr ".laa" yes;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 3 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
	setAttr -s 4 ".r";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :initialShadingGroup;
	addAttr -ci true -h true -sn "aal" -ln "attributeAliasList" -dt "attributeAlias";
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr ".ai_surface_shader" -type "float3" 0 0 -1.#INF ;
	setAttr ".ai_volume_shader" -type "float3" 0 0 -1.#INF ;
	setAttr ".aal" -type "attributeAlias" {"ai_aov_N","aiCustomAOVs[0]","ai_aov_direct_specular_2"
		,"aiCustomAOVs[10]","ai_aov_indirect_backlight","aiCustomAOVs[11]","ai_aov_indirect_diffuse"
		,"aiCustomAOVs[12]","ai_aov_indirect_diffuse_raw","aiCustomAOVs[13]","ai_aov_indirect_specular"
		,"aiCustomAOVs[14]","ai_aov_indirect_specular_2","aiCustomAOVs[15]","ai_aov_refraction"
		,"aiCustomAOVs[16]","ai_aov_single_scatter","aiCustomAOVs[17]","ai_aov_sss","aiCustomAOVs[18]"
		,"ai_aov_uv","aiCustomAOVs[19]","ai_aov_P","aiCustomAOVs[1]","ai_aov_depth","aiCustomAOVs[20]"
		,"ai_aov_id_1","aiCustomAOVs[21]","ai_aov_id_8","aiCustomAOVs[22]","ai_aov_id_2","aiCustomAOVs[23]"
		,"ai_aov_direct_glint","aiCustomAOVs[24]","ai_aov_direct_global","aiCustomAOVs[25]"
		,"ai_aov_direct_local","aiCustomAOVs[26]","ai_aov_direct_transmission","aiCustomAOVs[27]"
		,"ai_aov_indirect_glint","aiCustomAOVs[28]","ai_aov_indirect_global","aiCustomAOVs[29]"
		,"ai_aov_Z","aiCustomAOVs[2]","ai_aov_indirect_local","aiCustomAOVs[30]","ai_aov_indirect_transmission"
		,"aiCustomAOVs[31]","ai_aov_id_3","aiCustomAOVs[32]","ai_aov_crypto_asset","aiCustomAOVs[3]"
		,"ai_aov_crypto_material","aiCustomAOVs[4]","ai_aov_diffuse_color","aiCustomAOVs[5]"
		,"ai_aov_direct_backlight","aiCustomAOVs[6]","ai_aov_direct_diffuse","aiCustomAOVs[7]"
		,"ai_aov_direct_diffuse_raw","aiCustomAOVs[8]","ai_aov_direct_specular","aiCustomAOVs[9]"
		} ;
select -ne :initialParticleSE;
	addAttr -ci true -h true -sn "aal" -ln "attributeAliasList" -dt "attributeAlias";
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr ".ai_surface_shader" -type "float3" 0 0 -1.#INF ;
	setAttr ".ai_volume_shader" -type "float3" 0 0 -1.#INF ;
	setAttr ".aal" -type "attributeAlias" {"ai_aov_N","aiCustomAOVs[0]","ai_aov_direct_specular_2"
		,"aiCustomAOVs[10]","ai_aov_indirect_backlight","aiCustomAOVs[11]","ai_aov_indirect_diffuse"
		,"aiCustomAOVs[12]","ai_aov_indirect_diffuse_raw","aiCustomAOVs[13]","ai_aov_indirect_specular"
		,"aiCustomAOVs[14]","ai_aov_indirect_specular_2","aiCustomAOVs[15]","ai_aov_refraction"
		,"aiCustomAOVs[16]","ai_aov_single_scatter","aiCustomAOVs[17]","ai_aov_sss","aiCustomAOVs[18]"
		,"ai_aov_uv","aiCustomAOVs[19]","ai_aov_P","aiCustomAOVs[1]","ai_aov_depth","aiCustomAOVs[20]"
		,"ai_aov_id_1","aiCustomAOVs[21]","ai_aov_id_8","aiCustomAOVs[22]","ai_aov_id_2","aiCustomAOVs[23]"
		,"ai_aov_direct_glint","aiCustomAOVs[24]","ai_aov_direct_global","aiCustomAOVs[25]"
		,"ai_aov_direct_local","aiCustomAOVs[26]","ai_aov_direct_transmission","aiCustomAOVs[27]"
		,"ai_aov_indirect_glint","aiCustomAOVs[28]","ai_aov_indirect_global","aiCustomAOVs[29]"
		,"ai_aov_Z","aiCustomAOVs[2]","ai_aov_indirect_local","aiCustomAOVs[30]","ai_aov_indirect_transmission"
		,"aiCustomAOVs[31]","ai_aov_id_3","aiCustomAOVs[32]","ai_aov_crypto_asset","aiCustomAOVs[3]"
		,"ai_aov_crypto_material","aiCustomAOVs[4]","ai_aov_diffuse_color","aiCustomAOVs[5]"
		,"ai_aov_direct_backlight","aiCustomAOVs[6]","ai_aov_direct_diffuse","aiCustomAOVs[7]"
		,"ai_aov_direct_diffuse_raw","aiCustomAOVs[8]","ai_aov_direct_specular","aiCustomAOVs[9]"
		} ;
select -ne :defaultRenderGlobals;
	addAttr -ci true -sn "shave_old_preRenderMel" -ln "shave_old_preRenderMel" -dt "string";
	addAttr -ci true -sn "shave_old_postRenderMel" -ln "shave_old_postRenderMel" -dt "string";
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -av -k on ".edl";
	setAttr -cb on ".ren" -type "string" "redshift";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf" 51;
	setAttr -cb on ".imfkey" -type "string" "iff";
	setAttr -av -k on ".gama";
	setAttr -k on ".an";
	setAttr -k on ".ar";
	setAttr -k on ".fs" 0.96;
	setAttr -k on ".ef" 9.6;
	setAttr -av -k on ".bfs";
	setAttr -k on ".me";
	setAttr -k on ".se";
	setAttr -av -k on ".be";
	setAttr -cb on ".ep" 1;
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -k on ".oft" -type "string" "";
	setAttr -k on ".umfn";
	setAttr -k on ".ufe";
	setAttr -cb on ".pff" yes;
	setAttr -k on ".peie";
	setAttr -cb on ".ifp" -type "string" "";
	setAttr -k on ".rv" -type "string" "";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mot";
	setAttr -av -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".mbso";
	setAttr -k on ".mbsc";
	setAttr -av -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram" -type "string" " pgYetiPreRender; pgYetiVRayPreRender";
	setAttr -k on ".poam" -type "string" "pgYetiVRayPostRender";
	setAttr -k on ".prlm" -type "string" "";
	setAttr -k on ".polm" -type "string" "";
	setAttr -cb on ".prm" -type "string" "";
	setAttr -cb on ".pom" -type "string" "";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -av -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -k on ".rcp";
	setAttr -k on ".icp";
	setAttr -k on ".ocp";
	setAttr -cb on ".hbl";
	setAttr ".shave_old_preRenderMel" -type "string" "";
	setAttr ".shave_old_postRenderMel" -type "string" "";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w" 2048;
	setAttr -av -k on ".h" 2048;
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar" 1;
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi" 300;
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
connectAttr "rsHairRandomColor1.oc" "Sunny_YtS.diffuse_color";
connectAttr "ramp1.oa" "Sunny_YtS.transp_weight";
connectAttr "rsHairPosition1.uv" "ramp1.uv";
connectAttr "Sunny_YtS.msg" ":defaultShaderList1.s" -na;
connectAttr "rsHairPosition1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "rsHairRandomColor1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "ramp1.msg" ":defaultTextureList1.tx" -na;
// End of Sunny.ma
