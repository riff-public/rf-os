import pymel.core as pm ;
import maya.cmds as mc ;
import os , shutil , re ;

####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
''' project info '''
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
from rf_utils.context import context_info
reload(context_info)

project_path = 'P:/Two_Heroes/scene/' ;
# project_path = 'P:/SevenChickMovie/scene/work/' ;


self_path = os.path.realpath (__file__) ;
self_name = os.path.basename (__file__) ;
self_path = self_path.replace ( self_name , '' ) ;
self_path = self_path.replace ( '\\' , '/' ) ;
#D:/Dropbox/script/birdScript/projects/twoHeroes/

####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
''' general info '''
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################

def directory_cmd ( path = '' , *args ) :

    if os.path.exists ( path ) == False :
        os.makedirs ( path ) ;
    else : pass ;
    
    return ( path ) ;

def checkVersion_cmd ( directory = '' , name = '' , incrementVersion = True , incrementSubVersion = False , *args ) :
    # return format : 'v001_001'

    allFile = os.listdir ( directory ) ;
    versionList = [] ;

    for each in allFile :
        try :
            version = each.split ( '.' ) [-2] ;
            if len ( version.split ( '_' ) ) != 2 :
                pass ;
            else :
                versionList.append ( version ) ;
        except : pass ;
        else : pass ;
    
    if len ( versionList ) == 0 :
        version = '001'
        subVersion = '001'

    else :
        versionList.sort ( ) ;
        latestVersion = versionList [-1] ;
        latestVersion = latestVersion.split ( '_' ) ;
        version = int ( re.findall ( '[0-9]{3}' ,  latestVersion [ -2 ] ) [0] ) ;
        subVersion = int ( re.findall ( '[0-9]{3}' , latestVersion [ -1 ] ) [0] ) ;

        if incrementSubVersion == True :
            subVersion += 1 ;
            subVersion = str(subVersion).zfill(3) ;
        else :
            subVersion = str(subVersion).zfill(3) ;
            
        if incrementVersion == True :
            version += 1 ;
            version = str(version).zfill ( 3 ) ;
            subVersion = '001' ;
                # overwrite increment subversion
        else :
            version = str(version).zfill ( 3 ) ;

    return ( 'v' + str(version) + '_' + str(subVersion) ) ;

####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
''' user info '''
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################

def getPreferencePath_cmd ( *args ) :
    myDocuments = os.path.expanduser('~') ;
    myDocuments = myDocuments.replace ( '\\' , '/' ) ;
    pref_path = directory_cmd ( myDocuments + '/birdScriptPreferences/twoHeroesAutonaming_generation2/' ) ;
    return pref_path ;

def saveDefaultPath_cmd ( *args ) :
    pref_path = getPreferencePath_cmd ( ) ;
    inputPath_file = open ( pref_path + 'inputPath.txt' , 'w+' ) ;
    input_path = pm.textField ( 'inputPath_textField' , q = True , text = True ) ;
    inputPath_file.write ( input_path ) ;
    inputPath_file.close ( ) ;

def setDefaultPath_cmd ( *args ) :
    
    pref_path = getPreferencePath_cmd ( ) ;

    inputPathText_path = pref_path + 'inputPath.txt' ;

    if os.path.isfile ( inputPathText_path ) == True :
        inputPath_file = open ( inputPathText_path , 'r' ) ;
        inputPath_text = inputPath_file.read ( ) ;
        inputPath_file.close ( ) ;

        pm.textField ( 'inputPath_textField' , e = True , text = inputPath_text ) ;
        pm.text ( 'inputPath_text' , e = True , label = inputPath_text + 'TwoHeroes/' ) ;

    else : pass ;

def saveDefaultSelection_cmd ( *args ) :

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheCharacter_textScrollList   = TextScrollList ( textScrollList = 'cacheCharacter_textScrollList' ) ;

    film            = film_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    character       = character_textScrollList.currentItem ( ) ;
    cacheCharacter  = cacheCharacter_textScrollList.currentItem ( ) ;

    browserClass    = pm.optionMenu ( 'browserClass_optionMenu' , q = True , value = True ) ;
    cacheClass      = pm.optionMenu ( 'cacheClass_optionMenu' , q = True , value = True ) ;

    defaultSelection_dict = {} ;

    defaultSelection_dict['film']           = film ;
    defaultSelection_dict['sequence']       = sequence ;
    defaultSelection_dict['shot']           = shot ;
    defaultSelection_dict['character']      = character ;
    defaultSelection_dict['cacheCharacter'] = cacheCharacter ;
    defaultSelection_dict['browserClass']   = browserClass ;
    defaultSelection_dict['cacheClass']     = cacheClass ;

    pref_path = getPreferencePath_cmd ( ) ;

    defaultSelectionText_path = pref_path + 'defaultSelection.txt' ;
    defaultSelection_file = open ( defaultSelectionText_path , 'w+' ) ;
    defaultSelection_file.write ( 'defaultSelection_dict = %s' % str(defaultSelection_dict) ) ;
    defaultSelection_file.close ( ) ;

def setDefaultSelection_cmd ( *args ) :

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheCharacter_textScrollList   = TextScrollList ( textScrollList = 'cacheCharacter_textScrollList' ) ;

    pref_path = getPreferencePath_cmd ( ) ;

    defaultSelectionText_path = pref_path + 'defaultSelection.txt' ;

    if os.path.isfile ( defaultSelectionText_path ) == True :
    
        defaultSelection_file = open ( defaultSelectionText_path , 'r' ) ;
        defaultSelection_txt = defaultSelection_file.read ( ) ;
        defaultSelection_file.close ( ) ;

        exec ( defaultSelection_txt ) ;

        pm.optionMenu ( 'browserClass_optionMenu' , e = True , value = defaultSelection_dict['browserClass'] ) ;
        pm.optionMenu ( 'cacheClass_optionMenu' , e = True , value = defaultSelection_dict['cacheClass'] ) ;

        updateFilmTextScrollList_cmd ( ) ;

        if defaultSelection_dict['film'] == None : pass ;
        else :
            film_textScrollList.selectItem ( defaultSelection_dict['film'] ) ;
            updateSequenceTextScrollList_cmd ( ) ;

            if ( defaultSelection_dict['sequence'] ) == None : pass ;
            else :
                sequence_textScrollList.selectItem ( defaultSelection_dict['sequence'] ) ;
                updateShotTextScrollList_cmd ( ) ;

                if defaultSelection_dict['shot'] == None : pass ;
                else :
                    shot_textScrollList.selectItem ( defaultSelection_dict['shot'] ) ;
                    updateCharacterTextScrollList_cmd ( ) ;

                    if defaultSelection_dict['character'] == None : pass ;
                    else :
                        character_textScrollList.selectItem ( defaultSelection_dict['character'] ) ;

                    if defaultSelection_dict['cacheCharacter'] == None : pass ;
                    else :
                        cacheCharacter_textScrollList.selectItem ( defaultSelection_dict['cacheCharacter'] ) ;

    else : pass ;

def browsePath_cmd ( *args ) :

    try : 
        start_path = pm.textField ( 'inputPath_textField' , q = True , text = True ) ;
    except :
        start_path = '/C:' ;

    browse_path = mc.fileDialog2 ( ds = 2 , fm = 3 , startingDirectory = start_path , okc = 'select directory' ) [0] ;
 
    if browse_path[-1] != '/' :
        browse_path += '/' ;

    pm.textField ( 'inputPath_textField' , e = True , text = browse_path ) ;
    pm.text ( 'inputPath_text' , e = True , label = browse_path + 'TwoHeroes/' ) ;
    saveDefaultPath_cmd ( ) ;

####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
''' set project '''
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################

def setProject_cmd ( *args ) :

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheCharacter_textScrollList   = TextScrollList ( textScrollList = 'cacheCharacter_textScrollList' ) ;

    film            = film_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    character       = character_textScrollList.currentItem ( ) ;
    cacheCharacter  = cacheCharacter_textScrollList.currentItem ( ) ;

    setProject_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;
    
    if film == None : pass ;
    else : 
        setProject_path += film + '/' ;

        if sequence == None : pass ;
        else :
            setProject_path += sequence + '/' ;

            if shot == None : pass ;
            else :
                setProject_path += shot + '/' ;

                if character == None : pass ;
                else :
                    setProject_path += character ;

    print setProject_path ;

    try :
        pm.mel.eval( 'setProject "%s"' % setProject_path ) ;
    except :
        pm.error ( 'your input directory is not a maya project' ) ;

####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
''' autonaming '''
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################

def copy ( source , destination , name = None ,*args ) :
    # example : newFile = copy ( source = 'C:/Users/weerapot.c/Desktop/test1/content1.test.txt' , destination = 'C:/Users/weerapot.c/Desktop/test2' , name = 'test3.txt' ) ;
    
    ### copy ###
    shutil.copy2 ( source , destination ) ;
    
    ### get copied file's path ###
    copiedFile_name = source.split ( '/' ) [-1] ;
    
    if destination[-1] != '/' :
        destination += '/' ;
    else : pass ;
 
    copiedFile_path = destination + copiedFile_name ;
    
    ### make path for new name, rename copied file ###
    if name != None :
        newName = destination + name ;
        shutil.move ( copiedFile_path , newName ) ;
        
        ### return copied path ###    
        return newName ;
    
    else :
        ### return copied path ###
        return copiedFile_path ;

def copytree ( source , destination , *args ) :

    sourceFolder_name = source.split ( '/' ) [-1] ;

    if destination[-1] != '/' :
        destination += '/' ;
    else : pass ;

    destination_path = destination + sourceFolder_name ;

    if os.path.exists ( destination_path ) == True :
        shutil.rmtree ( destination_path ) ;
    else : pass ;

    shutil.copytree ( source , destination_path ) ;

    return destination_path ;

def autonamingBtn_cmd ( *args ) :

    saveDefaultSelection_cmd ( ) ;
    
    localProject_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheCharacter_textScrollList   = TextScrollList ( textScrollList = 'cacheCharacter_textScrollList' ) ;

    browserClass    = pm.optionMenu ( 'browserClass_optionMenu' , q = True , value = True ) ;
    cacheClass      = pm.optionMenu ( 'cacheClass_optionMenu' , q = True , value = True ) ;

    film            = film_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    character       = character_textScrollList.currentItem ( ) ;
    cacheCharacter  = cacheCharacter_textScrollList.currentItem ( ) ; 

    if ( film != None ) and ( sequence != None ) and ( shot != None ) and ( cacheCharacter != None ) :

        if type(cacheCharacter) == type(u'string') :
            cacheCharacter = [cacheCharacter] ; 

        for each in cacheCharacter :

            charIndex = each.split ( '.' ) [1] ;

            autonaming_path = directory_cmd ( localProject_path + film + '/' + sequence + '/' + shot + '/' + charIndex + '/' ) ;

            ### copy workspace.mel ###
            copy ( self_path + 'asset/workspace.mel' , autonaming_path ) ; 

            directory_cmd ( autonaming_path + '_DDR' ) ;
            directory_cmd ( autonaming_path + '_maya' ) ;
            directory_cmd ( autonaming_path + 'data' ) ;
            directory_cmd ( autonaming_path + 'hero' ) ;
            directory_cmd ( autonaming_path + 'version' ) ;
            directory_cmd ( autonaming_path + 'cache' ) ;

            ##################
            ##################
            ''' cache '''
            ##################
            ##################

            serverCache_Path = 'P:/Two_Heroes/scene/' ;
            serverCache_Path += film + '/' ;   
            serverCache_Path += sequence + '/' ;
            serverCache_Path += shot + '/' ;
            serverCache_Path += cacheClass + '/' ;
            serverCache_Path += 'cache/' ;

            ### copy time range ###
            copy ( serverCache_Path + 'timerange.txt' , autonaming_path + 'data' ) ;

            ### copy cache ###
            if pm.checkBox ( 'cache_cbx' , q = True , value = True ) != True : pass ;
            
            else :
                shotCache_path = directory_cmd ( autonaming_path + 'data/shotCache/' ) ;

                serverCache_list = os.listdir ( serverCache_Path ) ;

                for cache in serverCache_list :
                    if  ( ( '.' + charIndex + '.' ) in cache ) or ( 'share.' in cache ) :
                        copytree ( serverCache_Path + cache , shotCache_path ) ;
                    else : pass ;

            ##################
            ##################
            ''' setup '''
            ##################
            ##################

            ### autonaming setup heroes ###
            if pm.checkBox ( 'characterSetup_cbx' , q = True , value = True ) != True : pass ;
            else :
                character_path = 'P:/Two_Heroes/asset/character/' ;

                character = each.split('.')[0] ;
                character = character.replace( '_' , '/' ) ;
                character_path += character + '/' ;

                for elem in [ 'cloth' , 'hair' ] :
                    
                    setup_path = character_path + elem + '/maya/hero/' ;

                    if os.path.exists ( setup_path ) != True : pass ;
                    else :
                        setupAutonaming_path = cloth_path = directory_cmd ( autonaming_path + 'version/' + elem + '/'  ) ;

                        setup_list = os.listdir ( setup_path ) ;
                        version = checkVersion_cmd ( directory = setupAutonaming_path , name = setup_list[0].split('.')[0] ) ;

                        for setup in setup_list :
                            copy ( setup_path + setup , setupAutonaming_path ,
                                name = film + '.' + sequence + '.' + shot + '.' + setup.split('HERO')[0] + version + setup.split('HERO')[1] ) ;

        pm.mel.eval( 'setProject "%s"' % autonaming_path ) ;

        updateCharacterTextScrollList_cmd ( ) ;

        pm.confirmDialog ( message = 'autonaming completed' ) ;

    else : 

        print ( 'incomplete selection' ) ;

    #directory_cmd ( )

####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
''' UI update / navigate '''
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################

class TextScrollList ( object ) :

    def __init__ ( self, textScrollList ) :
        self.textScrollList = textScrollList ;

    def append ( self , item ) :
        self.item = item ;
        pm.textScrollList ( self.textScrollList , e = True , append = self.item ) ;

    def clear ( self ) :
        pm.textScrollList ( self.textScrollList , e = True , ra = True ) ; 

    def currentItem ( self ) :

        currentItem = pm.textScrollList ( self.textScrollList , q = True , selectItem = True ) ;
        
        if currentItem == [] :
            return None ;
        else :
            if len ( currentItem ) == 1 :
                return currentItem [0] ;
            else :
                return ( currentItem ) ;

    def selectItem ( self , selectItem ) :
        self.selectItem = selectItem ;
        pm.textScrollList ( self.textScrollList , e = True , selectItem = self.selectItem ) ;

    
def listdir ( elem = '' , *args ) :

    browserClass = pm.optionMenu ( 'browserClass_optionMenu' , q = True , value = True ) ;

    if browserClass == 'server' :
        target_path = project_path ;
    elif browserClass == 'local' :
        target_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;

    film_textScrollList         = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList     = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList         = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList    = TextScrollList ( textScrollList = 'character_textScrollList' ) ;

    if elem == 'film' :
        return os.listdir ( target_path ) ;
    
    elif elem == 'sequence' :
        target_path += film_textScrollList.currentItem() + '/' ;

    elif elem == 'shot' :
        target_path += film_textScrollList.currentItem() + '/' ;
        target_path += sequence_textScrollList.currentItem() + '/' ;

    elif elem == 'character' :

        target_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;

        if film_textScrollList.currentItem() != None :
            target_path += film_textScrollList.currentItem() + '/' ;

            if sequence_textScrollList.currentItem() != None :
                target_path += sequence_textScrollList.currentItem() + '/' ;
        
                if shot_textScrollList.currentItem() != None :
                    target_path += shot_textScrollList.currentItem() + '/' ;

        else : return None ;

    print "target_path: ", target_path
    if os.path.exists ( target_path ) == False :
        return None ;
    else :
        return os.listdir ( target_path ) ;

def updateProjectBrowser_cmd ( elem = '' , *args ) :

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheCharacter_textScrollList   = TextScrollList ( textScrollList = 'cacheCharacter_textScrollList' ) ;

    print "elem: ", elem

    if elem == 'film' :

        film_textScrollList.clear ( ) ;
        sequence_textScrollList.clear ( ) ;
        shot_textScrollList.clear ( ) ;

        filmItem_list = listdir ( 'film' ) ;
        filmItem_list.sort ( ) ;
        for item in filmItem_list :
            film_textScrollList.append ( item ) ;

    elif elem == 'sequence' :

        sequence_textScrollList.clear ( ) ;
        shot_textScrollList.clear ( ) ;

        sequenceItem_list = listdir ( 'sequence' ) ;
        print "sequenceItem_list: ", sequenceItem_list
        sequenceItem_list.sort ( ) ;
        for item in sequenceItem_list :
            sequence_textScrollList.append ( item ) ;

    elif elem == 'shot' :

        shot_textScrollList.clear ( ) ;
        
        shotItem_list = listdir ( 'shot' ) ;
        shotItem_list.sort ( ) ;
        for item in shotItem_list :
            shot_textScrollList.append ( item ) ;

    character_textScrollList.clear ( ) ;
    cacheCharacter_textScrollList.clear ( ) ;

def updateFilmTextScrollList_cmd ( *args ) :
    updateProjectBrowser_cmd ( elem = 'film' ) ;

def updateSequenceTextScrollList_cmd ( *args ) :
    updateProjectBrowser_cmd ( elem = 'sequence' ) ;

def updateShotTextScrollList_cmd ( *args ) :
    updateProjectBrowser_cmd ( elem = 'shot' ) ;

def updateCharacterTextScrollList_cmd ( *args ) :

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheCharacter_textScrollList   = TextScrollList ( textScrollList = 'cacheCharacter_textScrollList' ) ;

    film        = film_textScrollList.currentItem() ;
    sequence    = sequence_textScrollList.currentItem() ;
    shot        = shot_textScrollList.currentItem() ;

    if ( film != None ) and ( sequence != None ) and ( shot != None ) :

        ### update character list ###
        charItem_list = listdir ( 'character' ) ;

        if charItem_list != None :

            charItem_list.sort ( ) ;

            character_textScrollList.clear ( ) ;

            for item in charItem_list :
                character_textScrollList.append ( item ) ;

        else :

            character_textScrollList.clear ( ) ;

        ### update charCache list ###
        cacheClass = pm.optionMenu ( 'cacheClass_optionMenu' , q = True , value = True ) ;
        
        cacheCharacter_textScrollList.clear() ;

        cachePath = 'P:/Two_Heroes/scene/' ;
        cachePath += film + '/' ;   
        cachePath += sequence + '/' ;
        cachePath += shot + '/' ;
        cachePath += cacheClass + '/' ;
        cachePath += 'cache/' ;
        
        try :
            allCache_list = os.listdir ( cachePath ) ;

            cacheChar_list = [] ;

            for cache in allCache_list :
                if '_' in cache.split('.')[0] :
                    cacheChar_list.append ( cache ) ;

            cacheChar_list.sort ( ) ;
            
            for char in cacheChar_list :
                cacheCharacter_textScrollList.append ( item = char ) ;
        
        except : pass ;

    else :            
        
        character_textScrollList.clear ( ) ;
        cacheCharacter_textScrollList.clear ( ) ;

def filmSelection_cmd ( *args ) :
    updateSequenceTextScrollList_cmd ( ) ;
    saveDefaultSelection_cmd ( ) ;

def sequenceSelection_cmd ( *args ) :
    updateShotTextScrollList_cmd ( ) ;
    saveDefaultSelection_cmd ( ) ;

def shotSelection_cmd ( *args ) :
    updateCharacterTextScrollList_cmd ( ) ;
    saveDefaultSelection_cmd ( ) ;

def characterSelection_cmd ( *args ) :
    saveDefaultSelection_cmd ( ) ;

def browserClassChange_cmd ( *args ) :

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheCharacter_textScrollList   = TextScrollList ( textScrollList = 'cacheCharacter_textScrollList' ) ;

    film            = film_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    character       = character_textScrollList.currentItem ( ) ;
    cacheCharacter  = cacheCharacter_textScrollList.currentItem ( ) ;

    updateFilmTextScrollList_cmd ( ) ;

    if film == None : pass ;
    else : 
        
        try :
            film_textScrollList.selectItem ( film ) ;
            updateSequenceTextScrollList_cmd ( ) ;
        except : pass ;

        if sequence == None : pass ;
        else :
            
            try :
                sequence_textScrollList.selectItem ( sequence ) ;
                updateShotTextScrollList_cmd ( ) ;
            except : pass ;

            if shot == None : pass ;
            else :
            
                try :
                    shot_textScrollList.selectItem ( shot ) ;
                    updateCharacterTextScrollList_cmd ( ) ;
                except : pass ;

                if character == None : pass ;
                else :
                    try : character_textScrollList.selectItem ( character ) ;
                    except : pass ;
                
                if cacheCharacter == None : pass ;
                else :
                    try : cacheCharacter_textScrollList.selectItem ( cacheCharacter ) ;
                    except : pass ;

def browserDoubleClick_cmd ( *args ) :

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheCharacter_textScrollList   = TextScrollList ( textScrollList = 'cacheCharacter_textScrollList' ) ;

    film            = film_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    character       = character_textScrollList.currentItem ( ) ;
    cacheCharacter  = cacheCharacter_textScrollList.currentItem ( ) ;

    browserClass    = pm.optionMenu ( 'browserClass_optionMenu' , q = True , value = True ) ;
    #cacheClass      = pm.optionMenu ( 'cacheClass_optionMenu' , q = True , value = True ) ;

    if browserClass == 'server' :
        browse_path = project_path ;
    elif browserClass == 'local' :
        browse_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;

    if film == None : pass ;
    else :
        browse_path += film ;
        
        if sequence == None : pass ;
        else :
            browse_path += '/' + sequence ;

            if shot == None : pass ;
            else :
                browse_path += '/' + shot ;

                if character == None : pass ;
                else : browse_path += '/' + character ;

    os.startfile ( browse_path ) ;

def charDoubleClick_cmd ( *args ) :

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheCharacter_textScrollList   = TextScrollList ( textScrollList = 'cacheCharacter_textScrollList' ) ;

    film            = film_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    character       = character_textScrollList.currentItem ( ) ;
    cacheCharacter  = cacheCharacter_textScrollList.currentItem ( ) ;

    browse_path = pm.text ( 'inputPath_text' , q = True , label = True ) ;
    browse_path += film ;
    browse_path += '/' + sequence ;
    browse_path += '/' + shot ;
    browse_path += '/' + character ;

    os.startfile ( browse_path ) ;

def cacheCharDoubleClick_cmd ( *args ) :

    film_textScrollList             = TextScrollList ( textScrollList = 'film_textScrollList' ) ;
    sequence_textScrollList         = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    shot_textScrollList             = TextScrollList ( textScrollList = 'shot_textScrollList' ) ;
    character_textScrollList        = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    cacheCharacter_textScrollList   = TextScrollList ( textScrollList = 'cacheCharacter_textScrollList' ) ;

    film            = film_textScrollList.currentItem ( ) ;
    sequence        = sequence_textScrollList.currentItem ( ) ;
    shot            = shot_textScrollList.currentItem ( ) ;
    character       = character_textScrollList.currentItem ( ) ;
    cacheCharacter  = cacheCharacter_textScrollList.currentItem ( ) ;

    cacheClass      = pm.optionMenu ( 'cacheClass_optionMenu' , q = True , value = True ) ;

    browse_path = project_path
    browse_path += film ;
    browse_path += '/' + sequence ;
    browse_path += '/' + shot ;

    browse_path += '/' + cacheClass + '/cache' ;

    os.startfile ( browse_path ) ;

def shortcut_cmd ( elem = '' , *args ) :

    cacheCharacter_textScrollList   = TextScrollList ( textScrollList = 'cacheCharacter_textScrollList' ) ;
    cacheCharacter  = cacheCharacter_textScrollList.currentItem ( ) ;

    if type ( cacheCharacter ) == type ( [] ) :
        cacheCharacter = cacheCharacter [0] ;

    setup_path = 'P:/Two_Heroes/asset/character/' ;

    character = cacheCharacter.split('.')[0] ;
    character = character.replace( '_' , '/' ) ;
    setup_path += character + '/' + elem + '/maya/hero/' ;

    if os.path.exists ( setup_path ) != True :
        print ( "this character doesn't have " + elem + " setup yet" ) ;
    else :
        os.startfile ( setup_path ) ;

def characterClothSetupShortcut_cmd ( *args ) :
    shortcut_cmd ( elem = 'cloth' ) ;

def characterHairSetupShortcut_cmd ( *args ) :
    shortcut_cmd ( elem = 'hair' ) ;

####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
''' UI '''
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################

def run ( *args ) :

    title = 'two heroes autonaming g2 v001_001' ;

    width = 700.00 ;
    textScrollHeight = 200.00 ;

    def separator ( times = 1 , *args ) :
        for i in range ( 0 , times ):
            pm.separator ( h = 5 , vis = False ) ;

    def filler ( *args ) :
        pm.text ( label = '' ) ;

    # check if window exists
    if pm.window ( 'twoHeroes_autonamingGen2_UI' , exists = True ) :
        pm.deleteUI ( 'twoHeroes_autonamingGen2_UI' ) ;
    else : pass ;
   
    window = pm.window ( 'twoHeroes_autonamingGen2_UI', title = title ,
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'twoHeroes_autonamingGen2_UI' , e = True , w = width , h = 100 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            ###########################
            ''' local path '''
            ###########################

            with pm.frameLayout ( label = 'INITIALIZE PROJECT' , w = width , bgc = ( 0.1 , 0.1 , 0.45 ) ) :

                pm.text ( 'inputPath_text' , label = '...' , bgc = ( 1 , 0.85 , 0 ) ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/6*5 ) , ( 2 , width/6*1 ) ] ) :
                pm.textField ( 'inputPath_textField' , text = '...' ) ;
                pm.button ( label = 'browse' , c = browsePath_cmd ) ;

            ###########################
            ''' project browser layout '''
            ###########################
            
            with pm.frameLayout ( label = 'PROJECT BROWSER' , w = width , bgc = ( 0.1 , 0.1 , 0.45 ) ) :

                with pm.rowColumnLayout ( nc = 1 , w = width ) :

                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                        with pm.optionMenu ( 'browserClass_optionMenu' , label='browser class' , cc = browserClassChange_cmd , w = width/2 ) :
                                pm.menuItem ( label='server' ) ;
                                pm.menuItem ( label='local' ) ;

                        filler ( ) ;

                    with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , width/4 ) , ( 2 , width/4 ) , ( 3 , width/4 ) , ( 4 , width/4 ) ] ) :

                        with pm.rowColumnLayout ( nc = 1 , w = width/4 ) :
                            pm.text ( label = 'FILM' , font = 'boldLabelFont' , bgc =  ( 1 , 0 , 0.55 ) , width = width/4 ) ;
                            pm.textScrollList ( 'film_textScrollList' , ams = False , h = textScrollHeight , width = width/4 ,
                                sc = filmSelection_cmd ,
                                dcc = browserDoubleClick_cmd ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/4 ) :
                            pm.text ( label = 'SEQUENCE' , font = 'boldLabelFont' , bgc =  ( 1 , 0.4 , 0.7 ) , width = width/4 ) ;
                            pm.textScrollList ( 'sequence_textScrollList' , ams = False , h = textScrollHeight , width = width/4 ,
                                sc = sequenceSelection_cmd ,
                                dcc = browserDoubleClick_cmd ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/4 ) :
                            pm.text ( label = 'SHOT' , font = 'boldLabelFont' , bgc =  ( 1 , 0.7 , 0.75 ) , width = width/4 ) ;
                            pm.textScrollList ( 'shot_textScrollList' , ams = False , h = textScrollHeight , width = width/4 ,
                                sc = shotSelection_cmd ,
                                dcc = browserDoubleClick_cmd ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/4 ) :
                            pm.text ( label = 'CHARACTER' , font = 'boldLabelFont' , bgc =  ( 0.1 , 0.55 , 0.15 ) , width = width/4 ) ;
                            pm.textScrollList ( 'character_textScrollList' , ams = False , h = textScrollHeight , width = width/4 ,
                                sc = characterSelection_cmd ,
                                dcc = charDoubleClick_cmd ) ;
                    
                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        filler ( ) ;
                        pm.button ( label = 'set project' , w = width/2 , c = setProject_cmd , bgc = ( 0.65 , 1 , 0.2 ) ) ;
            
            ###########################
            ''' autonaming layout '''
            ###########################

            with pm.frameLayout ( label = 'AUTONAMING LAYOUT' , w = width , bgc = ( 0.1 , 0.1 , 0.45 ) ) :

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                    ### LEFT ###
                    with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :

                        ###########################
                        ''' browse cache layout '''
                        ###########################
                        with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :
            
                            with pm.optionMenu ( 'cacheClass_optionMenu' , label = 'cache class' , cc = updateCharacterTextScrollList_cmd , w = width/2 ) :
                                pm.menuItem ( label='finalCam' ) ;
                                pm.menuItem ( label='anim' ) ;
                                pm.menuItem ( label='setDress' ) ;
                                pm.menuItem ( label='layout' ) ;
                                pm.menuItem ( label='finalLayout' ) ;
                                
                            pm.textScrollList ( 'cacheCharacter_textScrollList' , ams = True , h = textScrollHeight , width = width/2 ,
                                sc = characterSelection_cmd ,
                                dcc = cacheCharDoubleClick_cmd ) ;

                    ### RIGHT ###
                    with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :

                        ###########################
                        ''' shotcuts layout '''
                        ###########################

                        separator ( 4 ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :

                            pm.text ( label = 'shortcuts' ) ;

                            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/4 ) , ( 2 , width/4 ) ] ) :
                                pm.button ( label = 'cloth setup' , c = characterClothSetupShortcut_cmd ) ;
                                pm.button ( label = 'hair setup' , c = characterHairSetupShortcut_cmd ) ;

                        separator ( 20 ) ;

                        ###########################
                        ''' autonaming layout '''
                        ###########################
                        with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :

                            with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/2/4 ) , ( 2 , width/2/2 ) , ( 3 , width/2/4 ) ] ) :
                                filler ( ) ;
                                with pm.rowColumnLayout ( nc = 1 , w = width/2/3 ) :
                                    pm.checkBox ( 'characterSetup_cbx' , label = 'character setup' , value = True ) ;
                                    pm.checkBox ( 'cache_cbx' , label = 'cache (cam/char/prop)' , value = True ) ;
                                filler ( ) ;

                            pm.button ( label = 'autonaming' , c = autonamingBtn_cmd , bgc = ( 1 , 0.85 , 0 ) ) ;

    updateFilmTextScrollList_cmd ( ) ;
    setDefaultPath_cmd ( ) ;
    setDefaultSelection_cmd ( ) ;

    window.show () ;

def toShelf ( *args ) :

    # self_path = D:/Dropbox/script/birdScript/projects/twoHeroes/

    image_path = self_path + 'media/autonaming_icon.png'

    commandHeader = '''
import sys ;

mp = "%s" ;
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;
''' % self_path.split ('/projects/')[0] ;
# D:/Dropbox/script/birdScript

    cmd = commandHeader ;
    cmd += '''
import projects.twoHeroes.autonaming_generation2 as autonaming_generation2 ;
reload ( autonaming_generation2 ) ;
autonaming_generation2.run ( ) ;
'''
    mel_cmd = '''
global string $gShelfTopLevel;
string $shelves = `tabLayout -q -selectTab $gShelfTopLevel`;
'''
    currentShelf = pm.mel.eval ( mel_cmd );
    pm.shelfButton ( style = 'iconOnly' , image = image_path , command = cmd , parent = currentShelf ) ;