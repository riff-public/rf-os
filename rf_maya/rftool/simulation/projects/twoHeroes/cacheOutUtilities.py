import pymel.core as pm ;
import maya.cmds as mc ;
import os , re ;
       
def getRefNS ( *args ) :
        # return character and prop ( prop/vehicle/weapon ) namespace
    
    references = mc.file ( q = True , r = True ) ;
    charRefList = [] ;
    propRefList = [] ;
    
    for each in references :

        namespace = ( mc.file ( each , q = True , namespace = True ) ) ;

        if ( 'char' in str ( each ) ) or ( 'character' in str ( each ) ) :
            
            if ( 'background' in each ) :
                pass ;

            else :
                if ( pm.objExists ( namespace + ':' + 'Geo_Grp' ) == True  ) or ( pm.objExists ( namespace + ':' + 'geo_grp' ) == True  ) :
                    charRefList.append ( each ) ;
                else : pass ;

        elif ( 'prop' in each ) or ( 'vehicle' in each ) or ( 'weapon' in each ) :
            ### PROP ###            
            if ( 'alley' in each ) :
                pass ;

            elif ( 'prop' in each )  :
                if ( pm.objExists ( namespace + ':' + 'Geo_Grp' ) == True  ) or ( pm.objExists ( namespace + ':' + 'geo_grp' ) == True  ) :
                    propRefList.append ( each ) ;
                else : pass ;
                
            else : pass ;
            
            ### VEHICLE ###
            if ( 'vehicle' in each ) :              
                if ( 'bus' in each ) or ( 'truck' in each ) or ( 'car' in each ) or ( 'van' in each ) :
                    pass ;
                else :

                    if ( pm.objExists ( namespace + ':' + 'Geo_Grp' ) == True  ) or ( pm.objExists ( namespace + ':' + 'geo_grp' ) == True  ) :
                        propRefList.append ( each ) ;
                    else : pass ;
                    
            else : pass ;
            
            ### WEAPON ###
            if 'weapon' in each :
                if ( pm.objExists ( namespace + ':' + 'Geo_Grp' ) == True  ) or ( pm.objExists ( namespace + ':' + 'geo_grp' ) == True  ) :
                    propRefList.append ( each ) ;
                else : pass ;

            else : pass ;
                
        else : pass ;
                
    charNSList = [] ;
    propNSList = [] ;
    
    for each in charRefList :
        charNSList.append ( mc.file ( each , q = True , namespace = True ) ) ;
    
    for each in propRefList :
        propNSList.append ( mc.file ( each , q = True , namespace = True ) ) ;
        
    if pm.objExists ( 'set' ) == True :
        setItems = pm.listRelatives ( c = True ) ;
        
        for prop in propNSList :
            for item in setItems :
                if prop in item :
                    propNSList.remove ( prop ) ;
        
    return ( charNSList , propNSList ) ;

def updateTSL ( *args ) :

    NSList = getRefNS ( ) ;
    
    charNSList = NSList [0] ;
    pm.textScrollList ( 'characterListTxtScroll' , e = True , ra = True ) ;
    for each in charNSList :    
        pm.textScrollList ( 'characterListTxtScroll' , e = True , append = each ) ;
    
    propNSList = NSList [1] ;
    pm.textScrollList ( 'propListTxtScroll' , e = True , ra = True ) ;
    for each in propNSList :    
        pm.textScrollList ( 'propListTxtScroll' , e = True , append = each ) ;

def updateTimeRange ( *args ) :
    time = findStartEnd ( ) ;
    # print time ;
    pm.intField ( 'preRollIntField' , e = True , v = time [0] ) ;
    pm.intField ( 'startIntField' , e = True , v = time [1] ) ;
    pm.intField ( 'endIntField' , e = True , v = time [2] ) ;
    pm.intField ( 'postRollIntField' , e = True , v = time [3] ) ;

def refresh ( *args ) :
    updateTSL ( ) ;
    updateTimeRange ( ) ;

#########################################################################

def findStartEnd ( *args ) :
    # return [ preRoll , start , end , postRoll ] ;
    start = pm.playbackOptions ( q = True , min = True ) ;
    end = pm.playbackOptions ( q = True , max = True ) ;
    preRoll = start - 30 ;
    postRoll = end + 30 ;
    return [ preRoll , start , end , postRoll ] ;

def queryUITime ( ) :
    # return [ preRoll , start , end , postRoll ] ;
    preRoll = pm.intField ( 'preRollIntField' , q = True , v = True ) ;
    start = pm.intField ( 'startIntField' , q = True , v = True ) ;
    end = pm.intField ( 'endIntField' , q = True , v = True ) ;
    postRoll = pm.intField ( 'postRollIntField' , q = True , v = True ) ;
    return [ preRoll , start , end , postRoll ] ;

def imprintData ( namespace = '' , *args ) :

    try : target = pm.general.PyNode( namespace + ':' + 'Geo_Grp' ) ;
    except : target = pm.general.PyNode( namespace + ':' + 'geo_grp' ) ;
    else : pass ;
    
    time = queryUITime ( ) ;
    preRoll = time [0] ;
    start = time [1] ;
    end = time [2] ;
    postRoll = time [3] ;
    
    target.addAttr ( '__forSim__' , keyable = True , attributeType = 'float' ) ; target.__forSim__.lock ( ) ;
    target.addAttr ( 'preRoll' , keyable = True , attributeType = 'float' , dv = preRoll ) ; target.preRoll.lock ( ) ;
    target.addAttr ( 'start' , keyable = True , attributeType = 'float' , dv = start ) ; target.start.lock ( ) ;
    target.addAttr ( 'end' , keyable = True , attributeType = 'float' , dv = end ) ; target.end.lock ( ) ;    
    target.addAttr ( 'postRoll' , keyable = True , attributeType = 'float' , dv = postRoll ) ; target.postRoll.lock ( ) ;
    
    try :
        placement_ctrl = pm.general.PyNode ( namespace + ':' + 'Place_Ctrl' ) ;
        placementScl = placement_ctrl.s.get ( ) ;
        target.addAttr ( 'placementSclX' , keyable = True , attributeType = 'float' , dv = placementScl [0] ) ; target.placementSclX.lock ( ) ;
        target.addAttr ( 'placementSclY' , keyable = True , attributeType = 'float' , dv = placementScl [1] ) ; target.placementSclY.lock ( ) ;
        target.addAttr ( 'placementSclZ' , keyable = True , attributeType = 'float' , dv = placementScl [2] ) ; target.placementSclZ.lock ( ) ;

        #pm.copyKey ( placement_ctrl , time = ( preRoll , postRoll ) , attribute = 'sx', option = "curve" ) ;
        #pm.pasteKey ( target , attribute = 'placementSclX' , placementSclX = 'replaceCompletely' , time = ( preRoll , postRoll ) ) ;

        #pm.copyKey ( placement_ctrl , time = ( preRoll , postRoll ) , attribute = 'sy', option = "curve" ) ;
        #pm.pasteKey ( target , attribute = 'placementSclY' , placementSclX = 'replaceCompletely' , time = ( preRoll , postRoll ) ) ;

        #pm.copyKey ( placement_ctrl , time = ( preRoll , postRoll ) , attribute = 'sz', option = "curve" ) ;
        #pm.pasteKey ( target , attribute = 'placementSclZ' , placementSclX = 'replaceCompletely' , time = ( preRoll , postRoll ) ) ;
    except : pass ;

    try :
        placement_ctrl = pm.general.PyNode ( namespace + ':' + 'master_ctrl' ) ;
        placementScl = placement_ctrl.globalScale.get ( ) ;
        target.addAttr ( 'globalScale' , keyable = True , attributeType = 'float' , dv = placementScl ) ; target.globalScale.lock ( ) ;

        #pm.copyKey ( placement_ctrl , time = ( preRoll , postRoll ) , attribute = 'globalScale', option = "curve" ) ;
        #pm.pasteKey ( target , attribute = 'globalScale' , placementSclX = 'replaceCompletely' , time = ( preRoll , postRoll ) ) ;
    except : pass ;

    try :
        head_ctrl = pm.general.PyNode ( namespace + ':' + 'Head_Ctrl' ) ;
        headScl = head_ctrl.s.get ( ) ;
        target.addAttr ( 'headSclX' , keyable = True , attributeType = 'float' , dv = headScl [0] ) ; target.headSclX.lock ( ) ;
        target.addAttr ( 'headSclY' , keyable = True , attributeType = 'float' , dv = headScl [1] ) ; target.headSclY.lock ( ) ;
        target.addAttr ( 'headSclZ' , keyable = True , attributeType = 'float' , dv = headScl [2] ) ; target.headSclZ.lock ( ) ;

        #pm.copyKey ( head_ctrl , time = ( preRoll , postRoll ) , attribute = 'sx', option = "curve" ) ;
        #pm.pasteKey ( target , attribute = 'headSclX' , placementSclX = 'replaceCompletely' , time = ( preRoll , postRoll ) ) ;

        #pm.copyKey ( head_ctrl , time = ( preRoll , postRoll ) , attribute = 'sy', option = "curve" ) ;
        #pm.pasteKey ( target , attribute = 'headSclY' , placementSclX = 'replaceCompletely' , time = ( preRoll , postRoll ) ) ;

        #pm.copyKey ( head_ctrl , time = ( preRoll , postRoll ) , attribute = 'sz', option = "curve" ) ;
        #pm.pasteKey ( target , attribute = 'headSclZ' , placementSclX = 'replaceCompletely' , time = ( preRoll , postRoll ) ) ;
    except : pass ;


def removeData ( namespace = '' , *args ) :

    try : target = pm.general.PyNode( namespace + ':' + 'Geo_Grp' ) ;
    except : target = pm.general.PyNode( namespace + ':' + 'geo_grp' ) ;
    else : pass ;
    
    target.__forSim__.unlock ( ) ; target.deleteAttr ( '__forSim__' ) ;
    target.preRoll.unlock ( ) ; target.deleteAttr ( 'preRoll' ) ;
    target.start.unlock ( ) ; target.deleteAttr ( 'start' ) ;
    target.end.unlock ( ) ; target.deleteAttr ( 'end' ) ;
    target.postRoll.unlock ( ) ; target.deleteAttr ( 'postRoll' ) ;
    
    try :
        target.placementSclX.unlock ( ) ; target.deleteAttr ( 'placementSclX' ) ; 
        target.placementSclY.unlock ( ) ; target.deleteAttr ( 'placementSclY' ) ;
        target.placementSclZ.unlock ( ) ; target.deleteAttr ( 'placementSclZ' ) ; 
    except :    
        target.globalScale.unlock ( ) ; target.deleteAttr ( 'globalScale' ) ; 
    else : pass ;
    
    try :
        target.headSclX.unlock ( ) ; target.deleteAttr ( 'headSclX' ) ;
        target.headSclY.unlock ( ) ; target.deleteAttr ( 'headSclY' ) ;
        target.headSclZ.unlock ( ) ; target.deleteAttr ( 'headSclZ' ) ;
    except : pass ;
    else : pass ;

def bakeKeys ( target = '' , preRoll = '' , postRoll = '' , hierarchy = 'none' , *args ) :
    # below

    pm.bakeResults ( target ,
        simulation  = True ,
        hierarchy = hierarchy ,
        time = ( preRoll , postRoll ) ,
        sampleBy = 1 ,
        #oversamplingRate = 1 ,
        disableImplicitControl = True ,
        preserveOutsideKeys = True ,
        sparseAnimCurveBake = False ,
        removeBakedAttributeFromLayer = False ,
        removeBakedAnimFromLayer = False ,
        bakeOnOverrideLayer = False ,
        minimizeRotation = True ,
        controlPoints = False ,
        shape = True ) ;

def exportABCBtn ( *args ) :

    cacheList = pm.textScrollList ( 'characterListTxtScroll' , q = True , si = True ) ;    
    cacheList.extend ( pm.textScrollList ( 'propListTxtScroll' , q = True , si = True ) ) ;

    ###

    if pm.checkBox ( 'cameraCacheCBX' , q = True , v = True ) == True :
        exportCamera ( ) ;
    else : pass ;

    ###

    if cacheList == [] :
        pass ;
    else :

        if pm.checkBox ( 'originCacheCBX' , q = True , v = True ) == False :
            # no origin
            
            for each in cacheList :

                imprintData ( namespace = each ) ;
                exportCache ( namespace = each ) ;
                removeData ( namespace = each ) ;

        else :
            # origin
            time = queryUITime ( ) ;
            preRoll = time [0] ;
            postRoll = time [3] ;
            pm.currentTime ( preRoll , edit = True ) ;

            for each in cacheList :

                imprintData ( namespace = each ) ;
                
                if pm.objExists ( each + ':' + 'Place_Ctrl' ) == True :
                    flyCtrl = pm.general.PyNode ( each + ':' + 'Offset_Ctrl' ) ;
                elif pm.objExists ( each + ':' + 'master_ctrl' ) == True :
                    flyCtrl = pm.general.PyNode ( each + ':' + 'master_ctrl' ) ;
                else :
                    pm.error ( "master_ctrl , Offset_Ctrl doesn't exist. Please contact Bird for script update" ) ;

                bakeKeys ( target = flyCtrl , preRoll = preRoll , postRoll = postRoll , hierarchy = 'below' ) ;

                nullGrp = pm.general.PyNode ( pm.group ( em = True , w = True , n = each + ':' + 'null_grp' ) ) ;

                # ( don't forget to delete animation key before parent constraint )
                parCon = pm.parentConstraint ( nullGrp , flyCtrl , mo = False , weight = 1 , 
                    skipRotate = 'none' ,
                    skipTranslate = 'none' , ) ;

                originCachePath = exportCache ( namespace = each , origin = True ) ;

                mc.file ( originCachePath , i = True , type = 'Alembic' , ignoreVersion = True , renameAll = False , mergeNamespacesOnClash = False ,
                    renamingPrefix = each , loadReferenceDepth = 'none' ) ;

                if pm.objExists ( 'Geo_Grp' ) == True :
                    geoGrp = 'Geo_Grp' ;
                elif pm.objExists ( 'geo_grp' ) == True :
                    geoGrp = 'geo_grp' ;
                else :
                    pm.error ( "geo_grp , Geo_Grp doesn't exist. Please contact Bird for script update" ) ;

                pm.parent ( geoGrp , flyCtrl ) ;

                pm.delete ( parCon ) ;

                pm.delete ( nullGrp ) ;

                exportCache ( namespace = each , useNamespace = False , incrementVersion = False , incrementSubVersion = True ) ;

                pm.delete ( geoGrp ) ;

                removeData ( namespace = each ) ;        


def getCachePath ( *args ) :
    selfPath = mc.file ( q = True , exn = True ) ;
    cachePath = selfPath.split ( 'maya' ) [0] + 'cache/' ;
    return cachePath ;

def checkVersion ( directory = '' , name = '' , incrementVersion = True , incrementSubVersion = False , origin = False , *args ) :
    
    if os.path.exists ( directory ) == False :
        os.makedirs ( directory ) ;
    else : pass ;

    allFile = os.listdir ( directory ) ;
    versionList = [] ;
    
    for each in allFile :
        if each.split('.')[0] == name :
            versionList.append ( each ) ;
        else : pass ;
    
    if len ( versionList ) == 0 :
        version = '001'
        subVersion = '001'

    else :
        versionList.sort ( ) ;
        latestVersion = versionList [-1] ;
        latestVersion = latestVersion.split ( '_' ) ;
        version = int ( re.findall ( '[0-9]{3}' ,  latestVersion [ -2 ] ) [0] ) ;
        subVersion = int ( re.findall ( '[0-9]{3}' , latestVersion [ -1 ] ) [0] ) ;

        if incrementSubVersion == True :
            subVersion += 1 ;
            subVersion = str(subVersion).zfill(3) ;
        else :
            subVersion = str(subVersion).zfill(3) ;
            
        if incrementVersion == True :
            version += 1 ;
            version = str(version).zfill ( 3 ) ;
            subVersion = '001' ;
                # overwrite increment subversion
        else :
            version = str(version).zfill ( 3 ) ;

    if origin == True :
        subVersion = '000' ;
    else : pass ;

    return str(version) + '_' + str(subVersion) ;
    
##########################################################################################################################################################################################
'''export camera'''
class Info ( object ) :

    def __init__ ( self , object ) :
        self.object = object ;

    def root ( self ) :
        # find the root of target

        root = mc.ls ( self.object , l = True ) [0]
        root = root.split ( "|" ) [1] ;

        return root ;

def listCamera ( *args ) :

    defaultCams = [ 'frontShape' , 'perspShape' , 'sideShape' , 'topShape' ] ;

    allCam = mc.ls ( type = 'camera' ) ;
    cam = [] ;

    for each in allCam :
        if each not in defaultCams :
            cam.append ( each ) ;

    return cam ;
        
def exportCamera ( *args ) :

    allCam = listCamera ( ) ;
    cam = [] ;


    for each in allCam :

        camInfo = Info ( each ) ;
        cam.append ( camInfo.root ( ) ) ;

    cam = sorted ( set ( cam ) ) ;

    cacheFullPathList = [] ;

    for each in cam :

        namespace = each ;
        asset = 'camera'
        camPath = mc.ls ( each , l = True ) ;

        time = queryUITime ( ) ;
        preRoll = time [0] ;
        start = time [1] ;
        end = time [2] ;
        postRoll = time [3] ;
        
        cachePath = getCachePath ( ) ;
        cachePath += 'camera' ;
        
        version = checkVersion ( directory = cachePath , name = namespace , incrementVersion = True , incrementSubVersion = False ) ;
        
        cacheName = namespace + '.' + asset + '.' + 'v' + version + '.abc' ;
        print cacheName ;
        
        command = 'AbcExport -j' + ' ' ; 
        command += '"-frameRange' + ' ' + str(preRoll) + ' '  + str(postRoll) + ' ' ;
        command += '-worldSpace -writeVisibility -eulerFilter -dataFormat ogawa' + ' ' ;
        command += '-root' + ' ' + camPath[0] + ' ' ;
        command += '-file' + ' ' +  cachePath + '/' + cacheName + '"' ;
        
        #bakeKeys ( target = each , preRoll = preRoll , postRoll = postRoll , hierarchy = 'below' ) ;        

        pm.mel.eval ( str ( command ) ) ;

        cacheFullPath = cachePath + '/' + cacheName ;
        cacheFullPathList.append ( cacheFullPath ) ;
    
    return cacheFullPathList ;
##########################################################################################################################################################################################

def exportCache ( namespace = '' , useNamespace = True , origin = False , incrementVersion = True , incrementSubVersion = False , *args ) :
    
    time = queryUITime ( ) ;
    preRoll = time [0] ;
    start = time [1] ;
    end = time [2] ;
    postRoll = time [3] ;
    
    if useNamespace == True :
        if pm.objExists ( namespace + ':' + 'Geo_Grp' ) == True :
            geoGrpPath = mc.ls ( namespace + ':' + 'Geo_Grp' , l = True ) ;
            asset = 'Geo_Grp' ;
        elif pm.objExists ( namespace + ':' + 'geo_grp' ) == True :
            geoGrpPath = mc.ls ( namespace + ':' + 'geo_grp' , l = True ) ;        
            asset = 'geo_grp' ;
        else :
            pm.error ( "geo_grp , Geo_Grp doesn't exist. Please contact Bird for script update" ) ;

    else :
        if pm.objExists ( 'Geo_Grp' ) == True :
            geoGrpPath = mc.ls ( 'Geo_Grp' , l = True ) ;
            asset = 'Geo_Grp' ;
        elif pm.objExists ( 'geo_grp' ) == True :
            geoGrpPath = mc.ls ( 'geo_grp' , l = True ) ;        
            asset = 'geo_grp' ;
        else :
            pm.error ( "geo_grp , Geo_Grp doesn't exist. Please contact Bird for script update" ) ;

    cachePath = getCachePath ( ) ;
    cachePath += namespace ;
    
    #print cachePath ;
    
    version = checkVersion ( directory = cachePath , name = namespace , incrementVersion = incrementVersion , incrementSubVersion = incrementSubVersion , origin = origin , *args ) ;
    
    cacheName = namespace + '.' + asset + '.' + 'v' + version + '.abc' ;
    
    step = pm.floatField ( 'step_input' , q = True , v = True ) ;

# AbcExport -j "-frameRange 71 120 -uvWrite -worldSpace -dataFormat ogawa -root |head_LOC|SunnyCasualSuit_COL|Geo_Grp_COL|SunnyCasualSuitSimGeo_Grp_COL|ShirtSim_Geo_COL -file D:/Dropbox/doubleMonkey/TwoHeroes/film001/q0300/s0390/sunnyCasual/cache/alembic/toDelete/test.abc";


    command = 'AbcExport -j' + ' ' ; # start command
    command += '"-frameRange' + ' ' + str(preRoll) + ' '  + str(postRoll) + ' ' ;
    command += '-step %s' % ( step ) + ' ' ;
    #command = '-frame' + ' ' + str(preRoll) + ' ' + str(postRoll) + ' ' ; 
    command += '-attr __forSim__ -attr preRoll -attr start -attr end -attr postRoll -attr placementSclX -attr placementSclY -attr placementSclZ -attr headSclX -attr headSclY -attr headSclZ -attr gobalScale' + ' ' ;
    command += '-worldSpace -writeVisibility -eulerFilter -dataFormat ogawa' + ' ' ;
    command += '-root' + ' ' + geoGrpPath[0] + ' ' ;
    command += '-file' + ' ' +  cachePath + '/' + cacheName + '"' ;
    
    #print command ;
    pm.mel.eval ( str ( command ) ) ;

    cacheFullPath = cachePath + '/' + cacheName ;
    return ( cacheFullPath ) ;

def animCacheOutUI ( *args ) :

    if pm.window ( 'animCacheOutUI' , exists = True ) :
        pm.deleteUI ( 'animCacheOutUI' ) ;
    else : pass ;

    window = pm.window ( 'animCacheOutUI', title = "Two Heroes COUT utilities v3" ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
    pm.window ( 'animCacheOutUI' , e = True , w = 310 ) ;
    
    mainLayout = pm.rowColumnLayout ( p = window , nc = 1 ) ;
            
    marginColumn = pm.rowColumnLayout ( p = mainLayout , nc = 3 ) ;
    
    pm.text ( label = '' , p = marginColumn , w = 2.5 ) ;
    marginRow = pm.rowColumnLayout ( p = marginColumn , nr = 3 ) ;
    pm.text ( label = '' , p = marginColumn , w = 2.5 ) ;
    
    pm.text ( label = '' , p = marginRow , h = 2.5 ) ;
    centerLayout = pm.rowColumnLayout ( p = marginRow , nc = 1 , w = 310 ) ;
    pm.text ( label = '' , p = marginRow , h = 2.5 ) ;

    tabs = pm.tabLayout ( innerMarginWidth = 5 , innerMarginHeight = 5 , p = centerLayout , w = 310 ) ;

    #################### MAIN ####################

    mainTabLayout = pm.rowColumnLayout ( 'mainTabLayout' ,  nc = 1 , cw = ( 1 , 310 ) , p = tabs ) ;

    pm.separator ( vis = False , p = mainTabLayout ) ;
    pm.separator ( vis = True , p = mainTabLayout ) ;

    selectionLayout = pm.rowColumnLayout ( p = mainTabLayout , nc = 2 ,
        columnWidth = [ ( 1 , 150 ) , ( 2 , 150 ) ] ) ;

    pm.text ( label = 'character(s) :' , align = 'left' , h = 25 , p = selectionLayout ) ;
    pm.text ( label = 'prop(s) :' , align = 'left' , h = 25 , p = selectionLayout ) ;
    
    characterTSL = pm.textScrollList ( 'characterListTxtScroll' , w = 150 , h = 100 , p = selectionLayout , ams = True ) ;
    propTSL = pm.textScrollList ( 'propListTxtScroll' , w = 150 , h = 100 , p = selectionLayout , ams = True ) ;
    updateTSL ( ) ;

    pm.separator ( p = selectionLayout , vis = False ) ;
    pm.separator ( p = selectionLayout , vis = False ) ;
    
    timeRangeLayout = pm.rowColumnLayout ( p = mainTabLayout , nc = 4 ,
        columnWidth = [ ( 1 , 75 ) , ( 2 , 75 ) , ( 3 , 75 ) , ( 4 , 75 ) ] ) ;
    
    pm.intField ( 'preRollIntField' , p = timeRangeLayout ) ;
    pm.intField ( 'startIntField' , p = timeRangeLayout ) ;
    pm.intField ( 'endIntField' , p = timeRangeLayout ) ;
    pm.intField ( 'postRollIntField' , p = timeRangeLayout ) ;
    updateTimeRange ( ) ;
    
    pm.text ( label = 'pre roll -30' , p = timeRangeLayout , bgc = [ 0.443137 , 0.776471 , 0.443137 ] ) ;
    pm.text ( label = 'start' , p = timeRangeLayout , bgc = [ 1 , 1 , 0 ] ) ;
    pm.text ( label = 'end' , p = timeRangeLayout , bgc = [ 1 , 1 , 0 ] ) ;
    pm.text ( label = 'post roll +30' , p = timeRangeLayout , bgc = [ 0.443137 , 0.776471 , 0.443137 ]) ;

    refreshLayout = pm.rowColumnLayout ( p = mainTabLayout , nc = 2 ,
        columnWidth = [ ( 1 , 150 ) , ( 2 , 150 ) ] ) ;
    pm.text ( label = '' , p = refreshLayout ) ;
    pm.button ( label = 'refresh' , p = refreshLayout , c = refresh , bgc = (  0.690196 , 0.878431 , 0.901961 ) ) ;

    option_layout = pm.rowColumnLayout ( p = mainTabLayout , nc = 4 , cw = [ ( 1 , 300/4.0 ) , ( 2 , 300/4.0 ) , ( 3 , 300/4.0 ) , ( 4 , 300/4.0 ) ] ) ;
    with option_layout :

        with pm.rowColumnLayout ( nc = 2 , w = 300/4.0 ) :
            pm.text ( label = 'step' , w = 300/4.0/2 ) ;
            pm.floatField ( 'step_input' , precision = 2 , v = 1 , w = 300/4.0/2 ) ;

        pm.text ( label = ' ' ) ;

        originCacheCBX = pm.checkBox ( 'originCacheCBX' , label = 'origin' , v = False ) ;

        cameraCacheCBX = pm.checkBox ( 'cameraCacheCBX' , label = 'camera' , v = True ) ;

    
    pm.separator ( vis = True , p = mainTabLayout ) ;
    
    pm.text ( label = "In viewport, please don't forget to Show > None" , p = mainTabLayout , bgc = ( 1 , 0.411765 , 0.705882 ) ) ;

    pm.button ( label = 'cache out' , p = mainTabLayout , c = exportABCBtn ) ;

    #################### HELP TAB  ####################
    helpTabLayout = pm.rowColumnLayout ( 'helpTabLayout' ,  nc = 1 , cw = ( 1 , 310 ) , p = tabs ) ;

    pm.separator ( vis = False , p = helpTabLayout ) ;
    pm.separator ( vis = True , p = helpTabLayout ) ;
    pm.separator ( vis = False , p = helpTabLayout ) ;
    
    helpLayout = pm.frameLayout ( label = 'help' , p = helpTabLayout ) ;
    
    optionDescriptionLayout = pm.rowColumnLayout ( p = helpLayout , nc = 2 , columnWidth = [ ( 1 , 20 ) , ( 2 , 280 ) ] ) ;

        ### origin ###  
    pm.text ( label = '' , p = optionDescriptionLayout) ;    
    originLayout = pm.frameLayout ( label = 'origin' , p = optionDescriptionLayout ) ;  
    pm.text ( p = originLayout , align = 'left' ,
        label = 'use this when the object(s) is too far from the origin' , bgc = [ 1 , 1 , 0 ] ) ;
    pm.text ( p = originLayout , align = 'left' ,
        label = '%s\n%s\n%s\n%s' % (
        '    - cache the object(s) OUT from the origin ( 0 , 0 , 0 )' , 
        '    - import the cache back into the scene' ,
        '    - position the cache back to its original position' ,
        '    - cache the object(s) OUT again' ) ) ;

        ### developer note ###
    pm.text ( label = '' , p = optionDescriptionLayout) ;    
    originLayout = pm.frameLayout ( label = 'note to users' , p = optionDescriptionLayout ) ;   
    pm.text ( p = originLayout , align = 'left' , bgc = [ 1 , 0.270588 , 0 ] ,
        label = '%s\n%s\n%s' % (
        'since props in this project has vague organization,' ,
        'should the problem arised or unwanted props' ,
        '(e.g. set) were listed please inform Bird of aTech team.' ) ) ;
    pm.text ( p = originLayout , align = 'left' , bgc = [ 1 , 1 , 0 ] ,
        label = 'incase of emergency, line: birdiesama' ) ;
        
    pm.tabLayout ( tabs , edit = True , tabLabel = (
        ( helpTabLayout , 'help' ) , ( mainTabLayout , 'main' ) ) , width = 310 ) ;
    
    window.show () ;

#animCacheOutUI ( ) ;