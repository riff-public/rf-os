import os , stat , shutil , re , ast ;
import pymel.core as pm ;
import maya.mel as ml ;

home = os.getenv ( 'HOME' ) ;
user = os.getenv ( 'USER' ) ;
desktop = os.path.join ( home [:home.find(user)] , user , 'Desktop' ) ;
desktop = desktop.replace ( '\\' , '/' ) ;

selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

twoHeroesDirectory = 'P:/Two_Heroes/scene' ;
characterDirectory = 'P:/Two_Heroes/asset/character' ;

class TextScrollList ( object ) :

    def __init__ ( self , name ) :
        self.name = name ;

    def get ( self ) :
        return pm.textScrollList ( self.name , q = True , selectItem = True ) ;

    def select ( self , value ) :
        self.value = value ;

        pm.textScrollList ( self.name , e = True , selectItem = self.value ) ;

    def clear ( self ) :
        pm.textScrollList ( self.name , e = True , ra = True ) ;

    def add ( self , value )  :
        self.value = value ;

        pm.textScrollList ( self.name , e = True , append = self.value ) ;

fTSL = TextScrollList ( 'fLayout' ) ;
qTSL = TextScrollList ( 'qLayout' ) ;
sTSL = TextScrollList ( 'sLayout' ) ;
cTSL = TextScrollList ( 'cLayout' ) ;

charTypeTSL = TextScrollList ( 'charTypeListLayout' ) ;
charTSL = TextScrollList ( 'charListLayout' ) ;
charElememtTSL = TextScrollList ( 'charElementListLayout' ) ;

def checkVersion ( directory = '' , name = '' , incrementVersion = True , incrementSubVersion = False , *args ) :
    # return format : '001_001'

    allFile = os.listdir ( directory ) ;
    versionList = [] ;

    for each in allFile :
        try :
            version = each.split ( '.' ) [-2] ;
            if len ( version.split ( '_' ) ) != 2 :
                pass ;
            else :
                versionList.append ( version ) ;
        except : pass ;
        else : pass ;
    
    if len ( versionList ) == 0 :
        version = '001'
        subVersion = '001'

    else :
        versionList.sort ( ) ;
        latestVersion = versionList [-1] ;
        latestVersion = latestVersion.split ( '_' ) ;
        version = int ( re.findall ( '[0-9]{3}' ,  latestVersion [ -2 ] ) [0] ) ;
        subVersion = int ( re.findall ( '[0-9]{3}' , latestVersion [ -1 ] ) [0] ) ;

        if incrementSubVersion == True :
            subVersion += 1 ;
            subVersion = str(subVersion).zfill(3) ;
        else :
            subVersion = str(subVersion).zfill(3) ;
            
        if incrementVersion == True :
            version += 1 ;
            version = str(version).zfill ( 3 ) ;
            subVersion = '001' ;
                # overwrite increment subversion
        else :
            version = str(version).zfill ( 3 ) ;

    return str(version) + '_' + str(subVersion) ;

def getPreferencePath ( *args ) :
    myDocuments = os.path.expanduser('~') ;
    myDocuments = myDocuments.replace ( '\\' , '/' ) ;
    # C:/Users/Legion/Documents
    
    prefPath = myDocuments + '/birdScriptPreferences/twoHeroesAutonaming'

    if os.path.exists ( prefPath ) == False :
        os.makedirs ( prefPath ) ;
    else : pass ;

    return prefPath ;

def saveProjectDirectory ( projectDirectory ) :

    prefPath = getPreferencePath ( ) ;

    projectDirectoryTxt = open ( prefPath + '/' + 'projectDirectory.txt' , 'w+' ) ;
    projectDirectoryTxt.write ( projectDirectory ) ;
    projectDirectoryTxt.close ( ) ;

def checkShotDirectory ( directory = '' , *args ) :

    if os.path.exists ( directory ) == True :
        pass ;
    else :
        os.makedirs ( directory ) ;
        os.makedirs ( directory + '/_DDR' ) ;
        os.makedirs ( directory + '/_maya' ) ;
        os.makedirs ( directory + '/data' ) ;
        os.makedirs ( directory + '/hero' ) ;
        os.makedirs ( directory + '/version' ) ;
        os.makedirs ( directory + '/cache' ) ;
        workspace = selfPath + '/workspace.mel' ;
        shutil.copy ( workspace , directory ) ;

def querySelection ( *args ) :
    # return a dictionary ;

    f = fTSL.get ( ) ;
    q = qTSL.get ( ) ;
    s = sTSL.get ( ) ;
    c = cTSL.get ( ) ;
    charType = charTypeTSL.get ( ) ;
    char = charTSL.get ( ) ;
    elementList = charElememtTSL.get ( ) ;

    if f == [] :
        pass ;
    else :
        f = f[0] ;     
        
    if q == [] :
        pass ;
    else :
        q = q[0] ;

    if s == [] :
        pass ;
    else :
        s = s[0] ;

    if c == [] :
        pass ;
    else :
        c = c[0] ;

    if charType == [] :
        pass ;
    else :
        charType = charType [0] ;

    if char == [] :
        pass ;
    else :
        char = char [0] ;

    selection = {} ;

    selection [ 'f' ] = f ;
    selection [ 'q' ] = q ;
    selection [ 's' ] = s ;
    selection [ 'c' ] = c ;

    selection [ 'charType' ] = charType ;
    selection [ 'char' ] = char ;

    selection [ 'charElementList' ] = elementList ;

    return selection ;

def saveDefaultSelection ( *args ) :

    selection = querySelection ( ) ;

    prefPath = getPreferencePath ( ) ;
    defaultSelectionTxt = open ( prefPath + '/' + 'defaultSelection.txt' , 'w+' ) ;
    defaultSelectionTxt.write ( str ( selection ) ) ;
    defaultSelectionTxt.close ( ) ;

def setDefault ( *args ) :

    projectDirectory = getProjectDirectory ( ) ;

    pm.textField ( 'userDirectory' , e = True , text = projectDirectory ) ;
    pm.text ( 'projectDirectory' , e = True , label = projectDirectory + 'TwoHeroes' ) ;    

    prefPath = getPreferencePath ( ) ;
    defaultSelectionTxt = open ( prefPath + '/' + 'defaultSelection.txt' , 'a+' ) ;
    defaultSelection = defaultSelectionTxt.read ( ) ;
    defaultSelectionTxt.close ( ) ;

    if defaultSelection == '' :
        
        pass ;

    else :

        defaultSelection = ast.literal_eval ( defaultSelection ) ;

        defaultF = defaultSelection [ 'f' ] ;
        defaultQ = defaultSelection [ 'q' ] ;
        defaultS = defaultSelection [ 's' ] ;
        defaultC = defaultSelection [ 'c' ] ;

        defaultCharType = defaultSelection [ 'charType' ] ;
        defaultChar = defaultSelection [ 'char' ] ;
        defaultCharElementList  = defaultSelection [ 'charElementList' ] ;

        if defaultF == [] : 
            pass ;
        else :
            fTSL.select ( value = defaultF ) ;
            updateQLayout ( ) ;

            if defaultQ == [] :
                pass ;
            else :
                qTSL.select ( value = defaultQ ) ;
                updateSLayout ( ) ;

                if defaultS == [] :
                    pass ;
                else :
                    sTSL.select ( value = defaultS ) ;
                    updateCLayout ( ) ;

                    if defaultC == [] :
                        pass ;
                    else :
                        cTSL.select ( value = defaultC ) ;

        if defaultCharType == [] :
            pass ;
        else :
            charTypeTSL.select ( value = defaultCharType ) ;
            updateCharacterLayout ( ) ;

            if defaultChar == [] :
                pass ;
            else :
                charTSL.select ( value = defaultChar ) ;
                updateCharacterElementListLayout ( ) ;

                if defaultCharElementList == [] :
                    pass ;
                else :
                    charElememtTSL.select ( value = defaultCharElementList ) ;

def autonaming ( *args ) : # WIP

    f = fTSL.get ( ) ;
    q = qTSL.get ( ) ;
    s = sTSL.get ( ) ;
    c = cTSL.get ( ) ;
    charType = charTypeTSL.get ( ) ;
    char = charTSL.get ( ) ;
    elementList = charElememtTSL.get ( ) ;

    if f == [] :
        pass ;
    else :
        f = f[0] ;     
        
        if q == [] :
            pass ;
        else :
            q = q[0] ;

            if s == [] :
                pass ;
            else :
                s = s[0] ;

                ###
                               
                if charType == [] :
                    pass ;
                else :
                    charType = charType [0] ;

                    if char == [] :
                        pass ;
                    else :
                        char = char [0] ;
                
                        shotDirectory = ( getProjectDirectory ( ) ) + 'TwoHeroes' + '/' + f + '/' + q + '/' + s + '/' + char ;

                        checkShotDirectory ( shotDirectory ) ;
                        pm.mel.eval( 'setProject "%s"' % shotDirectory ) ;

                        ###################################################################################################################################
                        
                        elementList = charElememtTSL.get ( ) ;

                        previousVersion = checkVersion ( directory = ( shotDirectory + '/' + 'version' ) , name = char , incrementVersion = False , incrementSubVersion = False ) ;

                        version = checkVersion ( directory = ( shotDirectory + '/' + 'version' ) , name = char , incrementVersion = True , incrementSubVersion = False ) ;

                        ###

                        if os.path.exists ( shotDirectory + '/' + 'version' + '/' + 'oldVersion' ) == True :
                            pass ;
                        else :
                            os.makedirs ( shotDirectory + '/' + 'version' + '/' + 'oldVersion' ) ;

                        previousVersionFileList = [] ;
                        allFileList = os.listdir ( shotDirectory + '/' + 'version' ) ;
                        for each in allFileList :
                            if previousVersion in each :
                                previousVersionFileList.append ( each ) ;
                        
                        for each in previousVersionFileList :
                            pass ;
                            #print ( shotDirectory + '/' + 'version' + '/' + each )
                            #print ( shotDirectory + '/' + 'version' + '/' + 'oldVersion' + '/' + each ) 
                            #shutil.move ( ( shotDirectory + '/' + 'version' + '/' + each ) , ( shotDirectory + '/' + 'version' + '/' + 'oldVersion' + '/' + each ) ) ;     

                        ###

                        if 'hair' in elementList :
                            #print ( 'hair will be autonaming') ;

                            hairDirectory = 'P:/Two_Heroes/asset/character/' + charType + '/' + char + '/hair/maya/hero' ;
                            hairFileList = os.listdir ( hairDirectory ) ;

                            hairHeroList = [] ;
                            hairDataList = [] ;
                            
                            for each in hairFileList :
                                if '.HERO.' in each :
                                    hairHeroList.append ( each ) ;
                                else :
                                    hairDataList.append ( each ) ;

                            for each in hairHeroList :
                                #sunnyCasual.HAIR0_hairStatic.HERO.ma

                                heroName = each.split ( '.' ) ;
                                fileType = heroName [-1] ;
                                heroType = heroName [1] ;
                                heroName = f + '.' + q + '.' + s + '.' + char + '.' + heroType + '.' + 'v' + version + '.' + fileType ; 
                            
                                shutil.copy ( ( hairDirectory + '/' + each ) , ( shotDirectory + '/' + 'version' ) ) ;

                                shutil.move ( ( shotDirectory + '/' + 'version' + '/' + each ) , ( shotDirectory + '/' + 'version' + '/' + heroName ) ) ;

                            for each in hairDataList :
                                
                                exists = os.path.exists ( shotDirectory + '/' + 'data' + '/' + each ) ;
                                replace = pm.checkBox ( 'replaceCB' , q = True , value = True ) ;

                                if ( exists == True ) and ( replace == True ) :
                                    shutil.rmtree ( shotDirectory + '/' + 'data' + '/' + each ) ;
                                    shutil.copytree ( ( hairDirectory + '/' + each ) , ( shotDirectory + '/' + 'data' + '/' + each ) ) ;
                                elif ( exists == True ) and ( replace == False ) :
                                    pass ;
                                else :
                                    shutil.copytree ( ( hairDirectory + '/' + each ) , ( shotDirectory + '/' + 'data' + '/' + each ) ) ;

                        else : pass ;

                        if 'cloth' in elementList :
                            print ( 'cloth will be autonaming') ;

                            clothDirectory = 'P:/Two_Heroes/asset/character/' + charType + '/' + char + '/cloth/maya/hero' ;
                            clothFileList = os.listdir ( clothDirectory ) ;

                            clothHeroList = [] ;
                            clothDataList = [] ;
                            
                            for each in clothFileList :
                                if '.HERO.' in each :
                                    clothHeroList.append ( each ) ;
                                else :
                                    clothDataList.append ( each ) ;

                            for each in clothHeroList :
                                #sunnyCasual.HAIR0_hairStatic.HERO.ma

                                heroName = each.split ( '.' ) ;
                                fileType = heroName [-1] ;
                                heroType = heroName [1] ;
                                heroName = f + '.' + q + '.' + s + '.' + char + '.' + heroType + '.' + 'v' + version + '.' + fileType ; 
                            
                                shutil.copy ( ( clothDirectory + '/' + each ) , ( shotDirectory + '/' + 'version' ) ) ;

                                shutil.move ( ( shotDirectory + '/' + 'version' + '/' + each ) , ( shotDirectory + '/' + 'version' + '/' + heroName ) ) ;

                        else : pass ;

                        ###################################################################################################################################

                        replace = pm.checkBox ( 'replaceCB' , q = True , value = True ) ;

                        shotAnimCacheDirectory = shotDirectory + '/' + 'data' + '/' + 'shotCache' ;
                        shotCamCacheDirectory = shotAnimCacheDirectory + '/' + 'camera' ;

                        if os.path.exists ( shotCamCacheDirectory ) == True :
                            pass ;
                        else :
                            os.makedirs ( shotCamCacheDirectory ) ;


                        animCacheDirectory = 'P:/Two_Heroes/scene/' + f + '/' + q + '/' + s + '/anim/cache' ;
                        layoutCacheDirectory = 'P:/Two_Heroes/scene/' + f + '/' + q + '/' + s + '/finalLayout/cache' ;


                        ###

                        if os.path.exists ( animCacheDirectory ) == True :
                            animCache = len ( os.listdir ( animCacheDirectory ) ) >= 2 ;
                            animCam = os.path.exists ( 'P:/Two_Heroes/scene/' + f + '/' + q + '/' + s + '/anim/cache/camera' ) ;
                        else :
                            animCache = False ;
                            animCam = False ;
                        
                        ###

                        if os.path.exists ( layoutCacheDirectory ) == True :

                            layoutCache = len ( os.listdir ( layoutCacheDirectory ) ) >= 2 ;
                            layoutCam = os.path.exists ( 'P:/Two_Heroes/scene/' + f + '/' + q + '/' + s + '/finalLayout/cache/camera' ) ;

                        else :
                            layoutCache = False ;
                            layoutCam = False ;

                        ###

                        if layoutCam == True :
                            camType = 'finalLayout' ;
                        
                        elif animCam == True :
                            camType = 'anim' ;
                        else :
                            camType = None ;

                        if camType == None :
                            pass ;
                        else :
                            camDirectory = ( 'P:/Two_Heroes/scene/' + f + '/' + q + '/' + s + '/' + camType + '/cache/camera' ) ;
                            camVersion = checkVersion ( directory = camDirectory , incrementVersion = False , incrementSubVersion = False ) ;
                            print camVersion ;
                            camList = os.listdir ( camDirectory ) ;
                            for each in camList :
                                if camVersion in each :
                                    if replace == True :
                                        if os.path.isfile ( shotCamCacheDirectory + '/' + each ) == True :
                                            os.remove ( shotCamCacheDirectory + '/' + each ) ;
                                            shutil.copy ( ( camDirectory + '/' + each ) , ( shotCamCacheDirectory ) ) ;
                                        else :
                                            shutil.copy ( ( camDirectory + '/' + each ) , ( shotCamCacheDirectory ) ) ;
                                    else :
                                        if os.path.isfile ( shotCamCacheDirectory + '/' + each ) == True :
                                            pass ;
                                        else :
                                            shutil.copy ( ( camDirectory + '/' + each ) , ( shotCamCacheDirectory ) ) ;
                                else : pass ;
                                
                        ###

                        if layoutCache == True :
                            cacheType = 'finalLayout' ;
                        
                        elif animCache == True :
                            cacheType = 'anim' ;
                        else :
                            cacheType = None ;

                        if cacheType == None :
                            pass ;
                        else :
                            
                            cacheDirectory = ( 'P:/Two_Heroes/scene/' + f + '/' + q + '/' + s + '/' + cacheType + '/' + 'cache' ) ;
                            characters = os.listdir ( cacheDirectory ) ;

                            if 'camera' in characters :
                                characters.remove ( 'camera' ) ;
                            else : pass ;

                            print characters ;

                            for character in characters :

                                if os.path.exists ( shotAnimCacheDirectory + '/' + character ) == True :
                                    pass ;
                                else :
                                    os.makedirs ( shotAnimCacheDirectory + '/' + character ) ;

                                allCache = os.listdir ( cacheDirectory + '/' + character ) ;
                                
                                version = checkVersion ( directory = cacheDirectory + '/' + character , incrementVersion = False , incrementSubVersion = False ) ;
                                print version ;

                                for cache in allCache :
                                    if version in cache :                                    
                                
                                        if replace == True :
                                        
                                            if os.path.isfile ( shotAnimCacheDirectory + '/' + character + '/' + cache ) == True :
                                                os.remove ( shotAnimCacheDirectory + '/' + character + '/' + cache ) ;
                                                shutil.copy ( ( cacheDirectory + '/' + character + '/' + cache ) , ( shotAnimCacheDirectory + '/' + character ) ) ;
                                            else :
                                                shutil.copy ( ( cacheDirectory + '/' + character + '/' + cache ) , ( shotAnimCacheDirectory + '/' + character ) ) ;
                                        
                                        else :
                                        
                                            if os.path.isfile ( shotAnimCacheDirectory + '/' + character + '/' + cache ) == True :
                                                pass ;
                                            else :
                                                shutil.copy ( ( cacheDirectory + '/' + character + '/' + cache ) , ( shotAnimCacheDirectory + '/' + character ) ) ;
                                    
                                    else : pass ;

                    updateCLayout ( ) ;
                    saveDefaultSelection ( ) ;
                    pm.confirmDialog ( message = 'autonaming completed' ) ;

def browsePath ( *args ) :

    startingDirectory = pm.textField ( 'userDirectory' , q = True , text = True ) ;

    userDirectory = pm.fileDialog2 ( bbo = 1 , fm = 3 , ff = '' , startingDirectory = startingDirectory ,
        okc = 'select directory' ) [0] ;
    
    if userDirectory[-1] != '/' :
        userDirectory += '/' ;

    pm.textField ( 'userDirectory' , e = True , text = userDirectory ) ;
    pm.text ( 'projectDirectory' , e = True , label = userDirectory + 'TwoHeroes' ) ;

    #allItems = os.listdir ( userDirectory ) ;

    saveProjectDirectory ( projectDirectory = userDirectory ) ;
   
    #updateProjectList ( directory = userDirectory ) ;

def getProjectDirectory ( *args ) :

    prefPath = getPreferencePath ( ) ;

    projectDirectoryTxt = open ( prefPath + '/' + 'projectDirectory.txt' , 'r+' ) ;
    projectDirectory = projectDirectoryTxt.read ( ) ;
    projectDirectoryTxt.close ( ) ;

    return projectDirectory ;

# P:\Two_Heroes\asset\character\main\sunnyCasual\hair\maya\hero

###########################################################################################################################################################

def updateFLayout ( *args ) :

    fTSL.clear ( ) ;
    qTSL.clear ( ) ;
    sTSL.clear ( ) ;
    cTSL.clear ( ) ;

    fList = os.listdir ( twoHeroesDirectory ) ;
    fList.sort ( ) ;

    for f in fList :
        fTSL.add ( value = f ) ;

def updateQLayout ( *args ) :

    f = fTSL.get ( ) ;
    
    if f == [] :
        qTSL.clear ( ) ;
        sTSL.clear ( ) ;
        cTSL.clear ( ) ;
    else : 
        f = f[0] ;

        qTSL.clear ( ) ;
        sTSL.clear ( ) ;
        cTSL.clear ( ) ;

        fPath = twoHeroesDirectory + '/' + f ;
        
        qList = os.listdir ( fPath ) ;
        qList.sort ( ) ;

        for q in qList :
            qTSL.add ( value = q ) ;

def updateSLayout ( *args ) :

    f = fTSL.get ( ) ;
    if f == [] :
        qTSL.clear ( ) ;
        sTSL.clear ( ) ;
        cTSL.clear ( ) ;
    else :
        f = f[0] ;

        q = qTSL.get ( ) ;
        if q == [] :
            sTSL.clear ( ) ;
            cTSL.clear ( ) ;
        else :
            q = q[0] ;

            sTSL.clear ( ) ;
            cTSL.clear ( ) ;

            qPath = twoHeroesDirectory + '/' + f + '/' + q ;
            sList = os.listdir ( qPath ) ;
            sList.sort ( ) ;

            for s in sList :
                sTSL.add ( value = s ) ;

def updateCLayout ( *args ) :

    f = fTSL.get ( ) ;
    if f == [] :
        qTSL.clear ( ) ;
        sTSL.clear ( ) ;
        cTSL.clear ( ) ;
    else :
        f = f[0] ;

        q = qTSL.get ( ) ;
        if q == [] :
            sTSL.clear ( ) ;
            cTSL.clear ( ) ;
        else :
            q = q[0] ;

            s = sTSL.get ( ) ;
            if s == [] :
                cTSL.clear ( ) ;
            else :
                s = s[0] ;

                cTSL.clear ( ) ;

                cPath = ( getProjectDirectory ( ) ) + 'TwoHeroes' + '/' + f + '/' + q + '/' + s ;

                if os.path.exists ( cPath ) == True :
                    cList = os.listdir ( cPath ) ;
                    cList.sort ( ) ;

                    for c in cList :
                        cTSL.add ( value = c ) ;

                else : pass ;

def setProject ( *args ) :

    f = fTSL.get ( ) ;
    if f == [] :
        pass ;
    else :
        f = f[0] ;

        q = qTSL.get ( ) ;
        if q == [] :
            pass ;
        else :
            q = q[0] ;

            s = sTSL.get ( ) ;
            if s == [] :
                pass ;
            else :
                s = s[0] ;
                
                c = cTSL.get ( ) ;
                if c == [] :
                    pass ;
                else :
                    c = c[0] ;

                    projectDirectory = ( getProjectDirectory ( ) ) + 'TwoHeroes' + '/' + f + '/' + q + '/' + s + '/' + c ;
                    pm.mel.eval( 'setProject "%s"' % projectDirectory ) ;

                    saveDefaultSelection ( ) ;

###########################################################################################################################################################

def updateCharacterTypeLayout ( *args ) :

    charTypeTSL.clear ( ) ;
    charTSL.clear ( ) ;
    charElememtTSL.clear ( ) ;

    characterTypeList = os.listdir ( characterDirectory ) ;
    characterTypeList.sort ( ) ;

    for characterType in characterTypeList :
        charTypeTSL.add ( value = characterType ) ;

def updateCharacterLayout ( *args ) :

    characterType = charTypeTSL.get ( ) ;
    
    if characterType == [] :
        charTSL.clear ( ) ;
        charElememtTSL.clear ( ) ;
    else :
        characterType = characterType [0] ;

        charTSL.clear ( ) ;
        charElememtTSL.clear ( ) ;

        characterList = os.listdir ( characterDirectory + '/' + characterType ) ;
        characterList.sort ( ) ;
        for character in characterList :
            charTSL.add ( value = character ) ;

def updateCharacterElementListLayout ( *args ) :

    characterType = charTypeTSL.get ( ) ;
    character = charTSL.get ( ) ;

    if characterType == [] :
        charTSL.clear ( ) ;
        charElememtTSL.clear ( ) ;
    else :
        characterType = characterType [0] ;
        
        if character == [] :   
            charElememtTSL.clear ( ) ;
        else :
            character = character [0] ;

            charElememtTSL.clear ( ) ;

            characterHair = characterDirectory + '/' + characterType + '/' + character + '/' + 'hair' + '/' + 'maya' + '/' + 'hero' ;

            if os.path.exists ( characterHair ) == True :
                charElememtTSL.add ( value = 'hair' ) ;
            else : pass ;

            characterCloth = characterDirectory + '/' + characterType + '/' + character + '/' + 'cloth' + '/' + 'maya' + '/' + 'hero' ;

            if os.path.exists ( characterCloth ) == True :
                charElememtTSL.add ( value = 'cloth' ) ;
            else : pass ;

def refresh ( *args ) :

    updateFLayout ( ) ;
    updateCharacterTypeLayout ( ) ;

###########################################################################################################################################################

def openProjectDirectory ( *args ) :
    import os ;
    projectDirectory = getProjectDirectory ( ) ;
    projectDirectory += 'TwoHeroes'  ;    
    os.startfile ( projectDirectory ) ;

def openShotDirectory ( *args ) :

    f = fTSL.get ( ) ;
    q = qTSL.get ( ) ;
    s = sTSL.get ( ) ;
    c = cTSL.get ( ) ;

    if f == [] :
        pass ;
    else :
        f = f[0] ;     
        
        if q == [] :
            projectDirectory = ( getProjectDirectory ( ) ) + 'TwoHeroes' + '/' + f ;
            if os.path.exists ( projectDirectory ) == True :
                os.startfile ( projectDirectory ) ;
            else :
                pm.error ( "local directory doesn't exist" ) ;
        else :
            q = q[0] ;

            if s == [] :
                projectDirectory = ( getProjectDirectory ( ) ) + 'TwoHeroes' + '/' + f + '/' + q ;
                if os.path.exists ( projectDirectory ) == True :
                    os.startfile ( projectDirectory ) ;
                else :
                    pm.error ( "local directory doesn't exist" ) ;
            else :
                s = s[0] ;

                if c == [] :
                    projectDirectory = ( getProjectDirectory ( ) ) + 'TwoHeroes' + '/' + f + '/' + q + '/' + s  ;
                    if os.path.exists ( projectDirectory ) == True :
                        os.startfile ( projectDirectory ) ;
                    else :
                        pm.error ( "local directory doesn't exist" ) ;
                else :
                    c = c[0] ;

                    projectDirectory = ( getProjectDirectory ( ) ) + 'TwoHeroes' + '/' + f + '/' + q + '/' + s + '/' + c ;
                    if os.path.exists ( projectDirectory ) == True :
                        os.startfile ( projectDirectory ) ;
                    else :
                        pm.error ( "local directory doesn't exist" ) ;                        

def openCharDirectory ( *args ) :
    # characterDirectory = 'P:/Two_Heroes/asset/character' ;

    charType = charTypeTSL.get ( ) ;
    char = charTSL.get ( ) ;
    element = charElememtTSL.get ( ) ;

    if charType == [] :
        pass ;
    else :
        charType = charType [0]

        if char == [] :

            openCharDirectory = ( characterDirectory + '/' + charType ) ;
        
            if os.path.exists ( openCharDirectory ) == True :
                os.startfile ( openCharDirectory ) ;
            else :
                pm.error ( "input directory doesn't exist" ) ;
        else :
            char = char [0] ;

            if element == [] :

                openCharDirectory = ( characterDirectory + '/' + charType + '/' + char ) ;
        
                if os.path.exists ( openCharDirectory ) == True :
                    os.startfile ( openCharDirectory ) ;
                else :
                    pm.error ( "input directory doesn't exist" ) ;
            else :
                element = element [0] ;

                openCharDirectory = ( characterDirectory + '/' + charType + '/' + char + '/' + element + '/' + 'maya/hero' ) ;
        
                if os.path.exists ( openCharDirectory ) == True :
                    os.startfile ( openCharDirectory ) ;
                else :
                    pm.error ( "input directory doesn't exist" ) ;

def TLo2H_ANUI ( ) :

    width = 600.00 ;

    if pm.window ( 'TLo2H_ANUI' , exists = True ) :
        pm.deleteUI ( 'TLo2H_ANUI' ) ;
    else : pass ;

    window = pm.window ( 'TLo2H_ANUI', title = "The Legend of 2 Heroes Autonaming v0.0", width = width ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
    window = pm.window ( 'TLo2H_ANUI', e = True , width = width ) ;
    with window :

        mainLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] ) ;
        with mainLayout :

            initializeProjectFrameLayout = pm.frameLayout ( label = 'INITIALIZE PROJECT' , w = width , bgc = ( 0.0980392 , 0.0980392 , 0.439216 ) ) ;
            with initializeProjectFrameLayout :

                pm.text ( 'projectDirectory' , label = '...' ) ;

                initializeProjectLayout = pm.rowColumnLayout ( w = width , nc = 2 , columnWidth = [ ( 1 , width/4*3 ) , ( 2 , width/4 ) ] ) ;
                with initializeProjectLayout :

                    pm.textField ( 'userDirectory' ) ;
                    pm.button ( label = 'browse' , c = browsePath ) ;

            projectFrameLayout = pm.frameLayout ( label = 'PROJECT' , w = width , bgc = ( 0.0980392 , 0.0980392 , 0.439216 ) ) ;
            with projectFrameLayout :

                projectLayout = pm.rowColumnLayout ( w = width , nc = 4 , columnWidth = [ ( 1 , width/4 ) , ( 2 , width/4 ) , ( 3 , width/4 ) , ( 4 , width/4 ) ] ) ; 
                with projectLayout :

                    pm.text ( label = 'FILM' , font = 'boldLabelFont' , bgc =  ( 1 , 0.0784314 , 0.576471 ) ) ;
                    pm.text ( label = 'SEQUENCE' , font = 'boldLabelFont' , bgc =  ( 1 , 0.411765 , 0.705882 ) ) ;
                    pm.text ( label = 'SHOT' , font = 'boldLabelFont' , bgc =  ( 1 , 0.713725 , 0.756863 ) ) ;
                    pm.text ( label = 'CHARACTER' , font = 'boldLabelFont' , bgc =  ( 0.133333 , 0.545098 , 0.133333 ) ) ;

                    fLayout = pm.textScrollList ( 'fLayout' , w = width/4 , h = 150 , ams = False , sc = updateQLayout , dcc = openShotDirectory ) ;
                    qLayout = pm.textScrollList ( 'qLayout' , w = width/4 , h = 150 , ams = False , sc = updateSLayout , dcc = openShotDirectory ) ;
                    sLayout = pm.textScrollList ( 'sLayout' , w = width/4 , h = 150 , ams = False , sc = updateCLayout , dcc = openShotDirectory ) ;
                    cLayout = pm.textScrollList ( 'cLayout' , w = width/4 , h = 150 , ams = False , dcc = openShotDirectory ) ;
                    
            autonamingLayout = pm.rowColumnLayout ( w = width , nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) ; 
            with autonamingLayout :
            
                characterFrameLayout = pm.frameLayout ( label = 'CHARACTER' , w = width/4*3 , bgc = ( 0.0980392 , 0.0980392 , 0.439216 ) ) ;
                with characterFrameLayout :

                    characterLayout = pm.rowColumnLayout ( w = width/4*3 , nc = 2 , columnWidth = [ ( 1 , width/4 ) , ( 2 , width/4 ) ] ) ; 
                    with characterLayout :

                        pm.text ( label = 'CHARACTER TYPE' , w = width/4 , font = 'boldLabelFont' , bgc =  ( 0.133333 , 0.545098 , 0.133333 ) ) ;
                        pm.text ( label = 'CHARACTER' , w = width/4 , font = 'boldLabelFont' , bgc =  ( 0.196078 , 0.803922 , 0.196078 ) ) ;
                        
                        charTypeListLayout = pm.textScrollList ( 'charTypeListLayout' , w = width/4 , h = 150 , ams = True , sc = updateCharacterLayout , dcc = openCharDirectory ) ;
                        charListLayout = pm.textScrollList ( 'charListLayout' , w = width/4 , h = 150 , ams = True , sc = updateCharacterElementListLayout , dcc = openCharDirectory ) ;

                buttonFrameLayout = pm.frameLayout ( 'buttonFrameLayout' , label = 'AUTONAMING' , w = width/2 , bgc = ( 0.545098 , 0 , 0 ) ) ;
                with buttonFrameLayout :

                    buttonMainLayout = pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width/2 ) ] ) ; 
                    with buttonMainLayout :

                        buttonMarginLayout = pm.rowColumnLayout ( nc = 3 , columnWidth = [ ( 1 , width/2/25 ) , ( 2 , width/2/25*23 ) , ( 3 , width/2/25 ) ] ) ; 
                        with buttonMarginLayout :

                                pm.text ( label = '' ) ;

                                buttonLayout = pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width/2/25*23 ) ] ) ; 
                                with buttonLayout :

                                    pm.text ( label = 'ELEMENT' , w = width/4 , font = 'boldLabelFont' , bgc =  ( 0.564706 , 0.933333 , 0.564706 ) ) ;
                                    charElementListLayout = pm.textScrollList ( 'charElementListLayout' , w = width/4 , h = 50 , ams = True , dcc = openCharDirectory ) ;

                                    hairCB =  pm.checkBox ( 'replaceCB' , label = 'replace cache and camera' , value = True ) ;

                                    pm.separator ( vis = False ) ;

                                    pm.button ( label = 'autonaming' , bgc = ( 1 , 0.647059 , 0 ) , c = autonaming ) ;
                                    pm.button ( label = 'set project' , bgc = ( 0.603922 , 0.803922 , 0.196078 ) , c = setProject ) ;

                                    pm.separator ( vis = False ) ;

                                    pm.button ( label = 'refresh' , bgc = ( 0.686275 , 0.933333 , 0.933333 ) , c = refresh ) ;
                                
                                pm.text ( label = '' ) ;

            openLayout = pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) ;
            with openLayout :

                pm.button ( label = 'open project directory' , bgc = ( 1 , 1 , 0.878431 ) , c = openProjectDirectory ) ;
                pm.button ( label = 'open shot directory' , bgc = (  0.690196 , 0.768627 , 0.870588 ) , c = openShotDirectory ) ;

    updateFLayout ( ) ;
    updateCharacterTypeLayout ( ) ;
    setDefault ( ) ;

    window.show ( )  ;