



class RestShapeRig ( object ) :

    def __init__ ( self ) :
        pass ;

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def makeName ( self , target ) :
        targetSplit_list = target.split ( '_' ) ;
        name = targetSplit_list[0] ;
        for each in targetSplit_list[1:] :
            name += each.capitalize ( ) ;
        return name ;

    def distanceBetween ( self , baseJnt , tipJnt , ribbonJnt , tipCtrl , side = None ) :
        # dmn = 'D'ecompose 'M'atrix 'N'ode
        # dbn = 'D'istance 'B'etween 'N'ode

        allJnt_list = [] ;
        allJnt_list.append ( baseJnt ) ;
        allJnt_list.extend ( ribbonJnt ) ;
        allJnt_list.append ( tipJnt ) ;

        dbn_list = [] ;

        for i in range ( 0 , len(allJnt_list) - 1 ) :

            targetA     = allJnt_list[i] ;
            targetB     = allJnt_list[i+1] ;

            for id in [ 'A' , 'B' ] :
                exec ( "target{id}Name = self.makeName ( target{id} )".format ( id = id ) ) ;
                exec ( "target{id}_dmn = pm.createNode ( 'decomposeMatrix' , n = target{id}Name + '_dmn' )".format ( id = id ) ) ;
                
                exec ( "target{id}.worldMatrix >> target{id}_dmn.inputMatrix".format ( id = id ) ) ;

            target_dbn = pm.createNode ( 'distanceBetween' , n = targetAName + '_' + targetBName + '_dbn' ) ;
            
            targetA_dmn.outputTranslate >> target_dbn.point1 ;
            targetB_dmn.outputTranslate >> target_dbn.point2 ;

            dbn_list.append ( target_dbn ) ;

        # find original length (static) ;
        staticLength = 0.0 ;
        for dbn in dbn_list :
            staticLength += dbn.distance.get() ;

        # find dynamic length
        dynamicLength_pma = pm.createNode ( 'plusMinusAverage' , name = baseJnt + '_' + tipJnt + '_pma' ) ;
        dynamicLength_pma.operation.set(1) ;
        for i in range ( 0 , len (dbn_list) ) :
            dbn_list[i].distance >> dynamicLength_pma.i1[i] ;

        # use pma to find translate value for tip control
        translateValue_pma = pm.createNode ( 'plusMinusAverage' , name = baseJnt + '_' + tipJnt + '_TranslateVal_pma' ) ;
        translateValue_pma.operation.set(2) ;
        dynamicLength_pma.o1 >> translateValue_pma.i1[0] ;
        translateValue_pma.i1[1].set( staticLength ) ;

        if not side :
            pass ;
        else :
            if side == 'L' :
                translateValue_pma.o1 >> tipCtrl.ty ;
            elif side == 'R' :
                mdv = pm.createNode ( 'multiplyDivide' , n = baseJnt + '_' + tipJnt + '_TranslateVal_mdv' ) ;
                mdv.operation.set ( 1 ) ;
                translateValue_pma.o1 >> mdv.i1x ;
                mdv.i2x.set ( -1 ) ;
                mdv.ox >> tipCtrl.ty ;

    def modifyRig ( self ) :

        ### chest ###
        self.distanceBetween ( 
            baseJnt     = pm.general.PyNode ( 'Spine_Jnt' ) ,
            tipJnt      = pm.general.PyNode ( 'Chest_Jnt' ) ,
            ribbonJnt   = [
                pm.general.PyNode ( 'SpineSkin_1_Jnt' ) ,
                pm.general.PyNode ( 'SpineSkin_2_Jnt' ) ,
                pm.general.PyNode ( 'SpineSkin_3_Jnt' ) ,
                pm.general.PyNode ( 'SpineSkin_4_Jnt' ) ,
                pm.general.PyNode ( 'SpineSkin_5_Jnt' ) ] ,
            tipCtrl     = pm.general.PyNode ( 'Chest_Ctrl' ) ) ;

        for side in [ 'L' , 'R' ] :

            ### upArm ###
            self.distanceBetween ( 
                baseJnt     = pm.general.PyNode ( 'UpArm_%s_Jnt' % side ) ,
                tipJnt      = pm.general.PyNode ( 'Forearm_%s_Jnt' % side ) ,
                ribbonJnt   = [
                    pm.general.PyNode ( 'RbnUpArmDtl_1_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpArmDtl_2_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpArmDtl_3_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpArmDtl_4_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpArmDtl_5_%s_Jnt' % side ) ] ,
                tipCtrl     = pm.general.PyNode ( 'ForearmFk_%s_Ctrl' % side ) ,
                side = side ) ;
            
            ### loArm ###
            self.distanceBetween ( 
                baseJnt     = pm.general.PyNode ( 'Forearm_%s_Jnt' % side ) ,
                tipJnt      = pm.general.PyNode ( 'Wrist_%s_Jnt' % side ) ,
                ribbonJnt   = [
                    pm.general.PyNode ( 'RbnForearmDtl_1_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnForearmDtl_2_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnForearmDtl_3_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnForearmDtl_4_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnForearmDtl_5_%s_Jnt' % side ) ] ,
                tipCtrl     = pm.general.PyNode ( 'WristFk_%s_Ctrl' % side ) ,
                side = side ) ;

            ### upLeg ###
            self.distanceBetween ( 
                baseJnt     = pm.general.PyNode ( 'UpLeg_%s_Jnt' % side ) ,
                tipJnt      = pm.general.PyNode ( 'LowLeg_%s_Jnt' % side ) ,
                ribbonJnt   = [
                    pm.general.PyNode ( 'RbnUpLegDtl_1_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpLegDtl_2_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpLegDtl_3_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpLegDtl_4_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnUpLegDtl_5_%s_Jnt' % side ) ] ,
                tipCtrl     = pm.general.PyNode ( 'LowLegFk_%s_Ctrl' % side ) ,
                side = side ) ;

            ### loLeg ###
            self.distanceBetween ( 
                baseJnt     = pm.general.PyNode ( 'LowLeg_%s_Jnt' % side ) ,
                tipJnt      = pm.general.PyNode ( 'Ankle_%s_Jnt' % side ) ,
                ribbonJnt   = [
                    pm.general.PyNode ( 'RbnLowLegDtl_1_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnLowLegDtl_2_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnLowLegDtl_3_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnLowLegDtl_4_%s_Jnt' % side ) ,
                    pm.general.PyNode ( 'RbnLowLegDtl_5_%s_Jnt' % side ) ] ,
                tipCtrl     = pm.general.PyNode ( 'AnkleFk_%s_Ctrl' % side ) ,
                side = side ) ;

test = RestShapeRig () ;
test.modifyRig() ;