char_dict = {} ;

##################
##################
''' SRU '''
##################
##################

char_dict['extra/captainSRU'] 		= 'SRU/captainSRU' ;
char_dict['extra/ericBandageSRU'] 	= 'SRU/ericBandageSRU' ;
char_dict['extra/ericSRU'] 			= 'SRU/ericSRU' ;
char_dict['main/hanumanSRU'] 		= 'SRU/hanumanSRU' ;
char_dict['extra/hippoSRU'] 		= 'SRU/hippoSRU' ;
char_dict['extra/hippoScarSRU'] 	= 'SRU/hippoScarSRU' ;
char_dict['extra/kevinGauzeSRU'] 	= 'SRU/kevinGauzeSRU' ;
char_dict['extra/kevinSRU'] 		= 'SRU/kevinSRU' ;
char_dict['extra/lilySRU'] 			= 'SRU/lilySRU' ;
#char_dict['/robotSRU'] 			= 'SRU/robotSRU' ;
char_dict['main/sunnySRU'] 			= 'SRU/sunnySRU' ;

##################
##################
''' main '''
##################
##################

#char_dict['/hanuman'] 					= 'main/hanuman' ;
char_dict['main/hanumanAnSuit'] 		= 'main/hanumanAncientArmor' ;
#char_dict['/hanumanApron'] 			= 'main/hanumanApron' ;
char_dict['main/hanumanWarSuit'] 		= 'main/hanumanArmor' ;
#char_dict['/hanumanBronze'] 			= 'main/hanumanBronze' ;
char_dict['main/hanumanCasualSuit'] 	= 'main/hanumanCasual' ;
#char_dict['/hanumanNaked'] 			= 'main/hanumanNaked' ;
#char_dict['/hanumanNoRedCloth'] 		= 'main/hanumanNoRedCloth' ;
#char_dict['/mayorLokCity'] 			= 'main/mayorLokCity' ;
#char_dict['/sonGoku'] 					= 'main/sonGoku' ;
char_dict['main/sonGokuAnSuit'] 		= 'main/sonGokuAncientArmor' ;
char_dict['main/sonGokuWarSuit'] 		= 'main/sonGokuArmor' ;
char_dict['main/sunnyCasualSuit'] 		= 'main/sunnyCasual' ;
char_dict['main/sunnyDad'] 				= 'main/sunnyDad' ;
char_dict['main/sunnyDadPajama'] 		= 'main/sunnyDadPajama' ;

###############=###
##################
''' demon '''
##################
##################

char_dict['main/blueDemon'] 		= 'demon/blueDemon' ;
#char_dict['/blueDemonUnborn'] 		= 'demon/blueDemonUnborn' ;

#char_dict['extra/bodyDemon'] 		= 'demon/bodyDemon' ;
char_dict['extra/bodyDemonBone'] 	= 'demon/bodyDemonBone' ;
char_dict['extra/bodyDemon'] 			= 'demon/bodyHuman' ;
#char_dict['/bodyHumanTux'] 		= 'demon/bodyHumanTux' ;

#char_dict['extra/brainDemon'] 		= 'demon/brainDemon' ;
char_dict['extra/brainDemon'] 			= 'demon/brainHuman' ;
char_dict['extra/brainDemonDoctor'] = 'demon/brainHumanDoc' ;
#char_dict['/brainHumanTux'] 		= 'demon/brainHumanTux' ;

#char_dict['/bullCalf'] 			= 'demon/bullCalf' ;
char_dict['extra/bullDemon'] 		= 'demon/bullDemon' ;
char_dict['extra/bullDemonBaby'] 	= 'demon/bullDemonBaby' ;
char_dict['extra/bullDemonKing']	= 'demon/bullDemonKing' ;
#char_dict['/bullHuman'] 			= 'demon/bullHuman' ;

#char_dict['extra/earsDemon'] 		= 'demon/earsDemon' ;
char_dict['extra/earsDemonBone'] 	= 'demon/earsDemonBone' ;
char_dict['extra/earsDemon'] 		= 'demon/earsHuman' ;
#char_dict['/earsHumanTux'] = 'demon/earsHumanTux' ;

#char_dict['extra/eyesDemon'] 		= 'demon/eyesDemon' ;
char_dict['extra/eyesDemon'] 		= 'demon/eyesHuman' ;
char_dict['extra/eyesDemonDoctor']	= 'demon/eyesHumanDoc' ;
#char_dict['/eyesHumanTux'] 		= 'demon/eyesHumanTux' ;

#char_dict['/hornGold'] 			= 'demon/hornGold' ;
#char_dict['/hornSilver'] 			= 'demon/hornSilver' ;

#char_dict['extra/noseDemon'] 		= 'demon/noseDemon' ;
char_dict['extra/noseDemonBone'] 	= 'demon/noseDemonBone' ;
char_dict['extra/noseDemon'] 		= 'demon/noseHuman' ;
#char_dict['/noseHumanTux'] = 'demon/noseHumanTux' ;

#char_dict['/phantomDemon01'] 		= 'demon/phantomDemon01' ;
#char_dict['/phantomDemon02'] 		= 'demon/phantomDemon02' ;
#char_dict['/phantomDemon03'] 		= 'demon/phantomDemon03' ;
#char_dict['/phantomDemon04'] 		= 'demon/phantomDemon04' ;
#char_dict['/phantomDemon05'] 		= 'demon/phantomDemon05' ;
#char_dict['/phantomDemon06'] 		= 'demon/phantomDemon06' ;

char_dict['extra/skeletonDemon'] 	= 'demon/skeletonDemon' ;
char_dict['extra/skeletonLady'] 			= 'demon/skeletonHuman' ;

#char_dict['extra/tongueDemon'] = 'demon/tongueDemon' ;
char_dict['extra/tongueDemonBone'] 	= 'demon/tongueDemonBone' ;
char_dict['extra/tongueDemon'] 		= 'demon/tongueHuman' ;
#char_dict['/tongueHumanTux'] = 'demon/tongueHumanTux' ;

char_dict['extra/westernDemonA'] = 'demon/westernDemon01' ;
char_dict['extra/westernDemonB'] = 'demon/westernDemon02' ;
char_dict['extra/westernDemonC'] = 'demon/westernDemon03' ;

##################
##################
''' god '''
##################
##################

#char_dict['/chineseGod01'] 	= 'god/chineseGod01' ;
#char_dict['/chineseGod02'] 	= 'god/chineseGod02' ;
#char_dict['/chineseGod03'] 	= 'god/chineseGod03' ;
#char_dict['/chineseGod04'] 	= 'god/chineseGod04' ;
#char_dict['/chineseGod05'] 	= 'god/chineseGod05' ;
#char_dict['/chineseGod06'] 	= 'god/chineseGod06' ;
#char_dict['/chineseGod07'] 	= 'god/chineseGod07' ;
#char_dict['/chineseGod08'] 	= 'god/chineseGod08' ;

char_dict['extra\DragonSerpent'] 	= 'god/dragonSerpent' ;

char_dict['extra\eagleGaruda'] 		= 'god/eagleGaruda' ;

char_dict['main/godEmperor'] 		= 'god/emperorGod' ;

char_dict['extra/indianGodA'] 		= 'god/indianGod01' ;
char_dict['extra/indianGodB'] 		= 'god/indianGod02' ;
char_dict['extra/indianGodC'] 		= 'god/indianGod03' ;
#char_dict['/indianGod04'] 		= 'god/indianGod04' ;
#char_dict['/indianGod05'] 		= 'god/indianGod05' ;

#char_dict['/luteGod'] 			= 'god/luteGod' ;

#char_dict['/monkShaGod'] 		= 'god/monkShaGod' ;

char_dict['extra/nezha'] 			= 'god/nezhaGod' ;

char_dict['extra/pagodaGod'] 		= 'god/pagodaGod' ;

char_dict['extra/pigsy'] 			= 'god/pigsyGod' ;

char_dict['extra/princessOfIronFanOld'] = 'god/princessIronFanOldGod' ;

#char_dict['/rainGod'] 			= 'god/rainGod' ;

char_dict['extra/skyDog'] 		= 'god/skyDogGod' ;

char_dict['extra/soma'] 		= 'god/somaGod' ;

char_dict['main/spiderGirl'] 	= 'god/spiderGirlGod' ;
char_dict['main/spider'] 		= 'god/spiderSpiderGirlGod' ;

#char_dict['/swordGod'] 		= 'god/swordGod' ;

char_dict['extra/threeEyeGod'] 	= 'god/threeEyesGod' ;

##################
##################
''' secondary '''
##################
##################

#char_dict['/assistantMayor01'] 	= 'secondary/assistantMayor01' ;
#char_dict['/assistantMayor02'] 	= 'secondary/assistantMayor02' ;
char_dict['extra/babyA'] 			= 'secondary/baby01' ;
char_dict['extra/babyB'] 			= 'secondary/baby02' ;
char_dict['extra/babyC'] 			= 'secondary/baby03' ;
char_dict['extra/babyD'] 			= 'secondary/baby04' ;
char_dict['extra/babyE'] 			= 'secondary/baby05' ;
char_dict['extra/babyF'] 			= 'secondary/baby06' ;
char_dict['extra/babyG'] 			= 'secondary/baby07' ;
char_dict['extra/babyH'] 			= 'secondary/baby08' ;
char_dict['extra/babyI'] 			= 'secondary/baby09' ;
char_dict['extra/babyJ'] 			= 'secondary/baby10' ;
#char_dict['/boyObese01'] 			= 'secondary/boyObese01' ;
#char_dict['/boyObese02'] 			= 'secondary/boyObese02' ;
#char_dict['/boyObese03'] 			= 'secondary/boyObese03' ;
#char_dict['/cameraMan01'] 			= 'secondary/cameraMan01' ;
#char_dict['/climber'] 				= 'secondary/climber' ;
#char_dict['/fairySnailLittle'] 	= 'secondary/fairySnailLittle' ;
#char_dict['/girlObese01'] 			= 'secondary/girlObese01' ;
#char_dict['/girlObese02'] 			= 'secondary/girlObese02' ;
#char_dict['/girlObese03'] 			= 'secondary/girlObese03' ;
#char_dict['/infantGeneric01'] 		= 'secondary/infantGeneric01' ;
#char_dict['/infantGeneric02'] 		= 'secondary/infantGeneric02' ;
#char_dict['/infantGeneric03'] 		= 'secondary/infantGeneric03' ;
#char_dict['/infantGeneric04'] 		= 'secondary/infantGeneric04' ;
#char_dict['/infantGeneric05'] 		= 'secondary/infantGeneric05' ;
#char_dict['/manClimber01'] 		= 'secondary/manClimber01' ;
#char_dict['/manFatherSunny'] 		= 'secondary/manFatherSunny' ;
#char_dict['/manHomeless'] 			= 'secondary/manHomeless' ;
char_dict['extra/obeseMan'] 		= 'secondary/manObese' ;
#char_dict['/manOld01'] 			= 'secondary/manOld01' ;
#char_dict['/manOld02'] 			= 'secondary/manOld02' ;
#char_dict['/manReporter'] 			= 'secondary/manReporter' ;
#char_dict['/manWorker01'] 			= 'secondary/manWorker01' ;
#char_dict['/manWorker02'] 			= 'secondary/manWorker02' ;
#char_dict['/merchant01'] 			= 'secondary/merchant01' ;
#char_dict['/merchant02'] 			= 'secondary/merchant02' ;
#char_dict['/merchant03'] 			= 'secondary/merchant03' ;
#char_dict['/merchant04'] 			= 'secondary/merchant04' ;
#char_dict['/merchant05'] 			= 'secondary/merchant05' ;
#char_dict['/photographer01'] 		= 'secondary/photographer01' ;
#char_dict['/powerplantManagerProximitySuit'] = 'secondary/powerplantManagerProximitySuit' ;
#char_dict['/prisoner01'] 			= 'secondary/prisoner01' ;
#char_dict['/prisoner02'] 			= 'secondary/prisoner02' ;
#char_dict['/prisoner03'] 			= 'secondary/prisoner03' ;
#char_dict['/prisoner04'] 			= 'secondary/prisoner04' ;
#char_dict['/prisoner05'] 			= 'secondary/prisoner05' ;

char_dict['extra/restaurantCustomerA2']	= 'secondary/restaurantCustomer01' ;
char_dict['extra/restaurantCustomerB'] 	= 'secondary/restaurantCustomer02' ;

#char_dict['/restaurantManager'] 	= 'secondary/restaurantManager' ;
#char_dict['/staffTV01'] 			= 'secondary/staffTV01' ;
#char_dict['/staffTV02'] 			= 'secondary/staffTV02' ;
#char_dict['/sunnyReal'] 			= 'secondary/sunnyReal' ;
#char_dict['/waiter01'] 			= 'secondary/waiter01' ;
#char_dict['/womanOld01'] 			= 'secondary/womanOld01' ;
#char_dict['/womanOld02'] 			= 'secondary/womanOld02' ;
#char_dict['/womanOld03'] 			= 'secondary/womanOld03' ;
#char_dict['/womanReporter'] 		= 'secondary/womanReporter' ;

##################
##################
''' service '''
##################
##################

# char_dict['/bodyguard01'] = 'service/bodyguard01' ;
# char_dict['/bodyguard02'] = 'service/bodyguard02' ;
# char_dict['/bodyguard03'] = 'service/bodyguard03' ;
# char_dict['/bodyguard04'] = 'service/bodyguard04' ;
char_dict['extra/Diners03'] = 'service/diners03' ;
char_dict['extra/Diners05'] = 'service/diners05' ;
char_dict['extra/Diners06'] = 'service/diners06' ;
char_dict['extra/Diners07'] = 'service/diners07' ;
# char_dict['/doctor01'] = 'service/doctor01' ;
# char_dict['/doctor02'] = 'service/doctor02' ;
# char_dict['/doctor03'] = 'service/doctor03' ;
# char_dict['/doctor04'] = 'service/doctor04' ;
# char_dict['/doctor05'] = 'service/doctor05' ;
# char_dict['/doctor06'] = 'service/doctor06' ;
# char_dict['/driverLimo01'] = 'service/driverLimo01' ;
# char_dict['/driverLimo02'] = 'service/driverLimo02' ;
# char_dict['/driverLimo03'] = 'service/driverLimo03' ;
# char_dict['/driverLimo04'] = 'service/driverLimo04' ;
# char_dict['/driverLimo05'] = 'service/driverLimo05' ;
# char_dict['/fire01'] = 'service/fire01' ;
# char_dict['/fire02'] = 'service/fire02' ;
# char_dict['/fire03'] = 'service/fire03' ;
# char_dict['/fire04'] = 'service/fire04' ;
# char_dict['/fire05'] = 'service/fire05' ;
# char_dict['/fireCaptain'] = 'service/fireCaptain' ;
# char_dict['/fireProximitySuit'] = 'service/fireProximitySuit' ;
# char_dict['/governmentClerk01'] = 'service/governmentClerk01' ;
# char_dict['/governmentClerk02'] = 'service/governmentClerk02' ;
# char_dict['/hospitalPrincipal'] = 'service/hospitalPrincipal' ;
# char_dict['/medicalStaff01'] = 'service/medicalStaff01' ;
# char_dict['/medicalStaff02'] = 'service/medicalStaff02' ;
# char_dict['/medicalStaff03'] = 'service/medicalStaff03' ;
# char_dict['/medicalStaff04'] = 'service/medicalStaff04' ;
# char_dict['/medicalStaff05'] = 'service/medicalStaff05' ;
# char_dict['/nurse01'] = 'service/nurse01' ;
# char_dict['/nurse02'] = 'service/nurse02' ;
# char_dict['/nurse03'] = 'service/nurse03' ;
# char_dict['/nurse04'] = 'service/nurse04' ;
# char_dict['/nurse05'] = 'service/nurse05' ;
# char_dict['/nurse06'] = 'service/nurse06' ;
# char_dict['/nurse07'] = 'service/nurse07' ;
# char_dict['/police01'] = 'service/police01' ;
# char_dict['/police02'] = 'service/police02' ;
# char_dict['/police03'] = 'service/police03' ;
# char_dict['/police04'] = 'service/police04' ;
# char_dict['/police05'] = 'service/police05' ;
# char_dict['/security01'] = 'service/security01' ;
# char_dict['/security02'] = 'service/security02' ;
# char_dict['/security03'] = 'service/security03' ;
# char_dict['/security04'] = 'service/security04' ;

'''

### search twoHeroes ###

import os ;

allChar =  os.listdir ( r'P:\Two_Heroes\asset\character\secondary' );

allChar.sort ( ) ;

for char in allChar :
    print ( "char_dict['/"+ char + "'] = 'secondary/" + char + "' ;" ) ;

### search doubleMonkeys ###

allChar = os.listdir ( r'P:\Doublemonkeys\all\asset\char\extra' ) ;

allChar.sort ( ) ;

for char in allChar :
    if 'baby' in char :
        print char ;

'''