import pymel.core as pm ;

class PositionPuller ( object ) :
    
    def __init__ ( self ) :
        pass ;
    
    def composeName ( self , target ) :
        
        targetSplit = target.split('_') ;
        
        if len ( targetSplit ) > 1 :
            name = targetSplit[0] ;
            
            for each in targetSplit[1:] :
                name += each[0].upper() ;
                name += each[1:] ;
                
        else :        
            name = targetSplit[0] ;
        
        return name ;

    def makeAnimProxy ( self , target , *args ) :
        
        target = pm.general.PyNode ( target ) ;
        
        targetNamespace = target.namespace() ;
        if targetNamespace :
            nameSplit = target.nodeName().split( ':' ) ;
            for name in nameSplit :
                if name == '' :
                    nameSplit.remove ( name ) ;
            namespace = self.composeName ( nameSplit[0] ) ;
            name = self.composeName ( nameSplit[1] ) ;
            
            print name , namespace ;
    
        loc = pm.spaceLocator ( n = namespace + '_' + name + '_AnimLoc' ) ;
        locGrp = pm.group ( em = True , n = namespace + '_' + name + 'AnimLocGrp' ) ;
        pm.parent ( loc , locGrp ) ;
        
        pm.copyKey ( target ) ;
        pm.pasteKey ( loc ) ;
        pm.parentConstraint ( loc , target , mo = True , skipRotate = 'none' , skipTranslate = 'none' ) ;
        
        locGrp.t.lock() ;
        locGrp.r.lock() ;
        locGrp.s.lock() ; 
        
        return locGrp ;

