import pymel.core as pm ;

import projects.twoHeroes.setdress_positionPuller.core_oriGrp as core_oriGrp ;
reload ( core_oriGrp ) ;

import projects.twoHeroes.setdress_positionPuller.core_positionPuller as core_positionPuller ;
reload ( core_positionPuller ) ;

import projects.twoHeroes.setdress_positionPuller.core_yetiUtil as core_yetiUtil ;
reload ( core_yetiUtil ) ;

class PositionPullerGUI ( core_oriGrp.OriGrp , core_positionPuller.PositionPuller , core_yetiUtil.YetiUtil , object ) :

    def __init__ ( self ) :
        core_oriGrp.OriGrp.__init__ ( self ) ;
        core_positionPuller.PositionPuller.__init__ ( self ) ;
        core_yetiUtil.YetiUtil.__init__ ( self ) ;

        self.ui         = 'positionPullerGUI_ui' ;
        self.width      = 300.00 ;
        self.title      = 'Position Puller' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    def importOriGrp_cmd ( self , *args ) :
        oriGrp = self.importOriGrp() ;
        self.updateOriGrpTextScrollList ( mostRecentItem = oriGrp ) ;

    def updateOriGrpTextScrollList ( self , mostRecentItem = None , *args ) :
        oriGrp_list = pm.ls ( 'ORI:*' ) ;
        oriGrp_list.sort() ;

        pm.textScrollList ( self.oriGrpTextScrollList , e = True , ra = True ) ;

        for oriGrp in oriGrp_list :
            pm.textScrollList ( self.oriGrpTextScrollList , e = True , append = oriGrp ) ;

        if mostRecentItem :
            pm.textScrollList ( self.oriGrpTextScrollList , e = True , selectItem = mostRecentItem ) ;

    def pull_cmd ( self , *args ) :

        selection = pm.ls ( sl = True ) ;

        oriGrp = pm.textScrollList ( self.oriGrpTextScrollList , q = True ,  selectItem = True ) [0] ;

        for each in selection :
            locGrp = self.makeAnimProxy ( each ) ;
            pm.parent ( locGrp , oriGrp ) ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def show ( self ) :
        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = self.width , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = self.width ) ;
            with main_layout :

                pm.text ( label = 'Ori_Grp :' , w = self.width ) ;
                self.oriGrpTextScrollList = pm.textScrollList ( w = self.width , h = 100.00 , allowMultiSelection = False ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , self.width/2 ) , ( 2 , self.width/2 ) ] ) :
                    pm.button ( label = 'Refresh' , c = self.updateOriGrpTextScrollList , bgc = ( 0.55 , 0.95 , 0.55 ) ) ;
                    pm.button ( label = 'Import Ori_Grp' , c = self.importOriGrp_cmd ) ;

                pm.separator ( vis = False , h = 5 ) ;

                pm.button ( label = 'Pull Position' , c = self.pull_cmd , bgc = ( 1 , 1 , 1 ) ) ;

                pm.separator ( vis = False , h = 5 ) ;
                
                pm.button ( label = 'Import Yeti' , c = self.importYeti ) ;

        self.updateOriGrpTextScrollList() ;

        window.show () ;

def run ( *args ) :
    gui = PositionPullerGUI() ;
    gui.show() ;