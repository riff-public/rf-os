import pymel.core as pm ;
import maya.cmds as mc ;

class OriGrp ( object ) :

	def __init__ ( self ) :
		self.selfPath = mc.file ( q = True , exn = True ) ;
		self.cachePath = self.getCachePath() ;

	def __str__ ( self ) :
		pass ;

	def __repr__ ( self ) :
		pass ;

	def createNameSpace ( self ) :

		if not pm.namespace ( exists = 'ORI' ) :
			pm.namespace ( addNamespace = 'ORI' ) ;

	def getCachePath ( self ) :

		selfPath = self.selfPath ;

		selfPathSplit = selfPath.split ( 'maya' ) ;
		for split in selfPathSplit :
		    if split == '' :
		        selfPathSplit.remove ( split ) ;

		cachePath = selfPathSplit[0] ;
		if cachePath[-1] == '/' :
		    cachePath = cachePath[0:-1] ;

		cachePathSplit = cachePath.split ( '/' ) ;
		for split in cachePathSplit :
		    if split == '' :
		        cachePathSplit.remove ( split ) ;

		elem = cachePathSplit[-1] ;

		cachePath = cachePath.replace ( elem , '' ) ;
		cachePath += 'cache' ;

		return cachePath ;

	def importOriGrp ( self ) :

		self.createNameSpace() ;

		oriGrpPath = pm.fileDialog2 ( fileMode = 1 , dir = self.cachePath ) ;

		if pm.objExists ( 'oriImportTemptGrp' ) == True :
		    pm.delete ( 'oriImportTemptGrp' ) ;

		pm.system.importFile ( oriGrpPath , gr = True , groupName = 'oriImportTemptGrp' ) ;

		temptGrp = pm.general.PyNode ( 'oriImportTemptGrp' ) ;

		oriGrp = temptGrp.getChildren() [0] ;

		oriGrp.rename ( 'ORI:' + oriGrp.nodeName() ) ;

		pm.parent ( oriGrp , w = True ) ;

		oriGrp.t.lock() ;
		oriGrp.r.lock() ;
		oriGrp.s.lock() ;

		pm.delete ( temptGrp ) ;

		return oriGrp ;