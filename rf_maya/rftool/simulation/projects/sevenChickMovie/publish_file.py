
_title = 'autonaming_generation2'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'autonaming_generation2'

#Import python modules
import sys
import os, shutil, re
import getpass
from collections import OrderedDict
from functools import partial

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework modules 
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import maya.cmds as mc 
import pymel.core as pm
from rf_utils.context import context_info
from rf_utils import register_shot

reload(context_info)


project_path = 'P:/SevenChickMovie/scene/publ/'

self_path = os.path.realpath (__file__) ;
self_name = os.path.basename (__file__) ;
self_path = self_path.replace ( self_name , '' ) ;
self_path = self_path.replace ( '\\' , '/' ) ;
print 'self_path', self_path
print 'self_name', self_name

####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
''' UI '''
####################################################################################################################################################################################
####################################################################################################################################################################################
####################################################################################################################################################################################
class TextScrollList ( object ) :

    def __init__ ( self, textScrollList ) :
        self.textScrollList = textScrollList ;

    def append ( self , item ) :
        self.item = item ;
        pm.textScrollList ( self.textScrollList , e = True , append = self.item ) ;

    def clear ( self ) :
        pm.textScrollList ( self.textScrollList , e = True , ra = True ) ; 

    def currentItem ( self ) :

        currentItem = pm.textScrollList ( self.textScrollList , q = True , selectItem = True ) ;
        
        if currentItem == [] :
            return None ;
        else :
            if len ( currentItem ) == 1 :
                return currentItem [0] ;
            else :
                return ( currentItem ) ;

    def selectItem ( self , selectItem ) :
        self.selectItem = selectItem ;
        pm.textScrollList ( self.textScrollList , e = True , selectItem = self.selectItem ) ;


def list_dir(*args):
    target_path = pm.text ( 'inputPath_text' , q = True , label = True ) 
    print target_path


def publish_file ( *args ) :
    target_path = pm.text ( 'inputPath_text' , q = True , label = True ) 
    act_textScrollList = TextScrollList(textScrollList='act_textScrollList')
    shot_textScrollList = TextScrollList(textScrollList='shot_textScrollList')
    character_textScrollList = TextScrollList(textScrollList='character_textScrollList')
    
    act = act_textScrollList.currentItem()
    shot = shot_textScrollList.currentItem()
    char = character_textScrollList.currentItem()
    if act != None, shot != None, char != None:
        context = context_info.Context()
        context.update(project='SevenChickMovie', entityType='scene', episode= act, entity= shot, step='sim', process='output')
        entity_shot = context_info.ContextPathInfo(context)
        output_path = entity_shot.path.hero().abs_path()
        local_path = target_path + act + shot + char + 'cache'
        print local_path
        print output_path




def directory_cmd ( path = '' , *args ) :

    if os.path.exists ( path ) == False :
        os.makedirs ( path ) ;
    else : pass ;
    
    return ( path ) ;

def getPreferencePath_cmd ( *args ) :
    myDocuments = os.path.expanduser('~') ;
    print myDocuments, 'myDocuments'
    myDocuments = myDocuments.replace ( '\\' , '/' ) ;
    pref_path = directory_cmd ( myDocuments + '/birdScriptPreferences/sevenChickpublish/' ) ;
    return pref_path ;

def saveDefaultPath_cmd ( *args ) :
    pref_path = getPreferencePath_cmd ( ) ;
    inputPath_file = open ( pref_path + 'inputPath.txt' , 'w+' ) ;
    input_path = pm.textField ( 'inputPath_textField' , q = True , text = True ) ;
    print input_path, 'input_path'
    inputPath_file.write ( input_path ) ;
    inputPath_file.close ( ) ;

def setDefaultPath_cmd ( *args ) :
    
    pref_path = getPreferencePath_cmd ( ) ;

    inputPathText_path = pref_path + 'inputPath.txt' ;

    if os.path.isfile ( inputPathText_path ) == True :
        inputPath_file = open ( inputPathText_path , 'r' ) ;
        inputPath_text = inputPath_file.read ( ) ;
        inputPath_file.close ( ) ;

        pm.textField ( 'inputPath_textField' , e = True , text = inputPath_text ) ;
        pm.text ( 'inputPath_text' , e = True , label = inputPath_text + 'SevenChickMovie/' ) ;

    else : pass ;

def list_act ():
    input_path = pm.text ( 'inputPath_text' , q = True , label = True ) 
    act_textScrollList = TextScrollList(textScrollList='act_textScrollList')
    act_textScrollList.clear()
    if os.path.exists(input_path):
        acts= os.listdir(input_path)
        for act in acts:
            act_textScrollList.append(act)

def list_shot():
    
    input_path = pm.text ( 'inputPath_text' , q = True , label = True ) 
    act_textScrollList = TextScrollList(textScrollList='act_textScrollList')
    shot_textScrollList = TextScrollList(textScrollList='shot_textScrollList')
    shot_textScrollList.clear()

    act = act_textScrollList.currentItem()
    path_ = os.path.join(input_path, act)

    if os.path.exists(path_):
        shots= os.listdir(path_)
        for shot in shots:
            shot_textScrollList.append(shot)

def list_character():
    
    input_path = pm.text ( 'inputPath_text' , q = True , label = True ) 
    act_textScrollList = TextScrollList(textScrollList='act_textScrollList')
    shot_textScrollList = TextScrollList(textScrollList='shot_textScrollList')
    character_textScrollList = TextScrollList(textScrollList='character_textScrollList')
    character_textScrollList.clear()

    act = act_textScrollList.currentItem()
    shot = shot_textScrollList.currentItem()
    path_ = os.path.join(input_path, act)
    char_path = os.path.join(path_, shot)

    if os.path.exists(char_path):
        char_list = os.listdir(char_path)
        for char in char_list:
            if os.path.isdir(os.path.join(char_path, char)):
                character_textScrollList.append(char)

def list_cache():
    
    input_path = pm.text ( 'inputPath_text' , q = True , label = True ) 
    act_textScrollList = TextScrollList(textScrollList='act_textScrollList')
    shot_textScrollList = TextScrollList(textScrollList='shot_textScrollList')
    character_textScrollList = TextScrollList(textScrollList='character_textScrollList')
    cache_textScrollList = TextScrollList(textScrollList='cache_textScrollList')
    cache_textScrollList.clear()

    act = act_textScrollList.currentItem()
    shot = shot_textScrollList.currentItem()
    char = character_textScrollList.currentItem()

    path_ = os.path.join(input_path, act)
    char_path = os.path.join(path_, shot)
    cache_path = os.path.join(char_path, '%s/cache'%char)

    if os.path.exists(cache_path):
        cache_list = os.listdir(cache_path)
        for cache in cache_list:
            cache_textScrollList.append(cache)


def run ( project, *args ) :

    title = 'autonaming g2 v001_001' ;

    width = 600.00 ;
    textScrollHeight = 200.00 ;

    def separator ( times = 1 , *args ) :
        for i in range ( 0 , times ):
            pm.separator ( h = 5 , vis = False ) ;

    def filler ( *args ) :
        pm.text ( label = '' ) ;

    # check if window exists
    if pm.window ( 'publish_File' , exists = True ) :
        pm.deleteUI ( 'publish_File' ) ;
    else : pass ;
   
    window = pm.window ( 'publish_File', title = title ,
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'publish_File' , e = True , w = width , h = 100 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            ###########################
            ''' local path '''
            ###########################

            with pm.frameLayout ( label = 'INITIALIZE PROJECT' , w = width , bgc = ( 0.1 , 0.1 , 0.45 ) ) :

                pm.text ( 'inputPath_text' , label = '...' , bgc = ( 1 , 0.85 , 0 ) ) ;

            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/6*5 ) , ( 2 , width/6*1 ) ] ,  ) :
                pm.textField ( 'inputPath_textField' , text = '...' ) ;
                pm.button ( label = 'browse'   , c = partial(browsePath_cmd, project)) ;

            ###########################
            ''' project browser layout '''
            ###########################
            
            with pm.frameLayout ( label = 'PROJECT BROWSER' , w = width , bgc = ( 0.1 , 0.1 , 0.45 ) ) :

                with pm.rowColumnLayout ( nc = 1 , w = width ) :

                    with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , width/4 ) , ( 2 , width/4 ) , ( 3 , width/4 ) , ( 4 , width/4 ) ] ) :
                        with pm.rowColumnLayout ( nc = 1 , w = width/4 ) :
                            pm.text ( label = 'Act' , font = 'boldLabelFont' , bgc =  ( 1 , 5 , 0.55 ) , width = width/4 ) ;
                            pm.textScrollList ( 'act_textScrollList' , ams = False , h = textScrollHeight , width = width/4, sc=list_shot ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/4 ) :
                            pm.text ( label = 'Shot' , font = 'boldLabelFont' , bgc =  ( 1 , 0 , 0.55 ) , width = width/4 ) ;
                            pm.textScrollList ( 'shot_textScrollList' , ams = False , h = textScrollHeight , width = width/4, sc=list_character
                            ) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/4 ) :
                            pm.text ( label = 'Character' , font = 'boldLabelFont' , bgc =  ( 1 , 0.4 , 0.7 ) , width = width/4 ) ;
                            pm.textScrollList ( 'character_textScrollList' , ams = False , h = textScrollHeight , width = width/4 , sc=list_cache) ;

                        with pm.rowColumnLayout ( nc = 1 , w = width/4 ) :
                            pm.text ( label = 'Cache' , font = 'boldLabelFont' , bgc =  ( 1 , 0.7 , 0.75 ) , width = width/4 ) ;
                            pm.textScrollList ( 'cache_textScrollList' , ams = False , h = textScrollHeight , width = width/4  ) ;

                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        filler ( ) ;
                        pm.button ( label = 'Publish File' , w = width/2  , bgc = ( 0.65 , 1 , 0.2 ), c = publish_file ) ;
           
    setDefaultPath_cmd ( )
    list_act()
    window.show () ;


def toShelf ( *args ) :

    # self_path = D:/Dropbox/script/birdScript/projects/twoHeroes/

    image_path = self_path + 'media/autonaming_icon.png'

    commandHeader = '''
import projects.sevenChickMovie.publish_file as publish_file_to ;
reload ( publish_file_to ) ;
project = 'SevenChickMovie'
publish_file_to.run ( project ) 
'''
    currentShelf = pm.mel.eval ( mel_cmd );
    pm.shelfButton ( style = 'iconOnly' , image = image_path , command = cmd , parent = currentShelf ) ;