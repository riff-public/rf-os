import pymel.core as pm ;

if not pm.pluginInfo ( 'matrixNodes.mll' , q = True , loaded = True ) :
    pm.loadPlugin ( 'matrixNodes.mll' ) ;

class CoreFunc ( object ) :

    def __init__ ( self ) :
        super ( CoreFunc , self ).__init__() ;

    def parCon ( self , driver , driven , mo = False , skipRotate = 'none' , skipTranslate = 'none' , clear = False ) :
        # return parCon
        parCon = pm.parentConstraint ( driver , driven , mo = mo , skipRotate = skipRotate , skipTranslate = skipTranslate ) ;

        if clear :
            pm.delete ( parCon ) ;
        else :
            return parCon ;

    def copyRO ( self , driver , driven ) :
        driver = pm.PyNode ( driver ) ;
        driven = pm.PyNode ( driven ) ;

        driven.ro.set ( driver.ro.get() ) ;


    def getTopNode ( self , node , *args ) :

        if node.getParent() :
            return self.getTopNode ( node.getParent() ) ;
        
        else :
            return node ;

    def core_run ( self , plane , ctrl_list , *args ) :

        plane = pm.PyNode ( plane ) ;
        planeShape  = plane.getShape() ;

        for ctrl in ctrl_list :

            name = ctrl.nodeName().split('_')[0] ;

            parTfm = ctrl.getParent() ;
            refTfm = pm.group ( em = True , w = True , n = name + 'Ref_Tfm' ) ;
            moveTfm = pm.group ( em = True , w = True , n = name + 'mov_Tfm' ) ;

            self.parCon ( parTfm , refTfm , clear = True ) ;
            self.copyRO ( ctrl , refTfm ) ;

            self.parCon ( parTfm , moveTfm , clear = True ) ;
            self.copyRO ( ctrl , moveTfm ) ;

            pm.parent ( refTfm , moveTfm , parTfm ) ;
            pm.parent ( ctrl , moveTfm ) ;
    
            # make the ref orientation as world 

            topNode = self.getTopNode ( ctrl ) ;
            pm.orientConstraint ( topNode , refTfm , mo = False , skip = 'none' , n = name + 'Ref_OriCon' ) ; 

            # attach follicle to plane, let it follows the parTfm
            folShape = pm.createNode ( 'follicle' ) ;
            fol = folShape.getParent() ;
            fol.rename ( name + '_Fol' ) ;

            folFixTfm = pm.group ( em = True , w = True , n = name + 'FolFix_Tfm' ) ;
            
            rot = pm.xform ( ctrl , q = True , ws = True , rotation = True ) ;

            # folFixTfm.r.set ( rot ) ;
            # folFixTfm.rx.set ( folFixTfm.rx.get() + 90 ) ;
            # folFixTfm.rz.set ( folFixTfm.rx.get() -180 ) ;
            # folFixTfm.t.lock() ;
            # folFixTfm.r.lock() ;
            # folFixTfm.s.lock() ;
            # pm.parent ( folFixTfm , fol ) ;

            folShape.simulationMethod.set (0) ;
        
            folShape.outRotate      >> fol.rotate ;
            folShape.outTranslate   >> fol.translate ;

            planeShape.worldMatrix  >> folShape.inputWorldMatrix ;
            planeShape.outMesh      >> folShape.inputMesh ;

            cpm = pm.shadingNode ( 'closestPointOnMesh' , asUtility = True , n = name + '_Cpm'  ) ;

            planeShape.outMesh >> cpm.inMesh ;

            dcm = pm.shadingNode ( 'decomposeMatrix' , asUtility = True , n = name + '_Dcm' ) ;

            parTfm.worldMatrix >> dcm.inputMatrix ;
            dcm.outputTranslateX >> cpm.inPositionX ;
            dcm.outputTranslateY >> cpm.inPositionY ;
            dcm.outputTranslateZ >> cpm.inPositionZ ;

            cpm.result.parameterU >> folShape.parameterU ;
            cpm.result.parameterV >> folShape.parameterV ;

            # getting ty

            dcm = pm.shadingNode ( 'decomposeMatrix' , asUtility = True , n = name + 'TY_Dcm' ) ;
            mmt = pm.shadingNode ( 'matrixMult' , asUtility = True , n = name + 'TY_mmt' ) ;

            fol.worldMatrix >> mmt.matrixIn[0] ;
            moveTfm.parentInverseMatrix[0] >> mmt.matrixIn[1] ;

            mmt.matrixSum >> dcm.inputMatrix ;

            dcm.outputTranslateY >> moveTfm.ty ;






def run ( *args ) :

    main = CoreFunc() ;

    selection_list = pm.ls ( sl = True ) ;

    plane       = selection_list[-1] ;
    ctrl_list   = selection_list[:-1] ;

    main.core_run ( plane = plane , ctrl_list = ctrl_list ) ;

run () ;