import pymel.core as pm ;

if not pm.pluginInfo ( 'matrixNodes.mll' , q = True , loaded = True ) :
    pm.loadPlugin ( 'matrixNodes.mll' ) ;

class General ( object ) :

    def __init__ ( self ) :
        super ( General , self ).__init__ () ;

    def freezeTransform ( self , target ) :
        target = pm.PyNode ( target ) ;
        pm.makeIdentity ( target , apply = True ) ;

    def deleteHistory ( self , target ) :
        target = pm.PyNode ( target ) ;
        pm.delete ( target , ch = True ) ;

    def centerPivot ( self , target ) :
        target = pm.PyNode ( target ) ;
        pm.xform ( target , cp = True ) ;

    def showHiddenAttr ( self , target ) :
        
        target = pm.PyNode ( target ) ;

        for attr in [ 't' , 'r' , 's' ] :
            for axis in [ 'x' , 'y' , 'z' ] :
                exec ( "pm.setAttr ( target.%s%s , channelBox = True ) ;" % ( attr , axis ) ) ;

        pm.setAttr ( target.v , channelBox = True ) ;

    def unlockAllAttr ( self , target ) :

        target = pm.PyNode ( target ) ;

        for attr in [ 't' , 'r' , 's' ] :
            for axis in [ 'x' , 'y' , 'z' ] :
                exec ( "pm.setAttr ( target.%s%s , lock = False )" % ( attr , axis ) ) ;
                exec ( "pm.setAttr ( target.%s%s , keyable = True )" % ( attr , axis ) ) ;

        pm.setAttr ( target.v , lock = False ) ;
        pm.setAttr ( target.v , keyable = True ) ;

    def copyRO ( self , driver , driven ) :

        driver = pm.PyNode ( driver ) ;
        driven = pm.PyNode ( driven ) ;

        driven.ro.set ( driver.ro.get() ) ;

    def parCon ( self , driver , driven , mo = False , skipRotate = 'none' , skipTranslate = 'none' , clear = False ) :
        # return parCon
        parCon = pm.parentConstraint ( driver , driven , mo = mo , skipRotate = skipRotate , skipTranslate = skipTranslate ) ;

        if clear :
            pm.delete ( parCon ) ;
        else :
            return parCon ;

class GuiFunc ( object ) :

    def __init__ ( self ) :
        super ( GuiFunc , self ).__init__ () ;

        self.godEmperor_ctrl_list = [
            'TrailFnt_Ctrl' ,
            'Trail1_L_Ctrl' ,
            'Trail2_L_Ctrl' ,
            'Trail3_L_Ctrl' ,
            'Trail4_L_Ctrl' ,
            'Trail5_L_Ctrl' ,
            'Trail6_L_Ctrl' ,
            'Trail7_L_Ctrl' ,
            'TrailBck_Ctrl' ,
            'Trail7_R_Ctrl' ,
            'Trail6_R_Ctrl' ,
            'Trail5_R_Ctrl' ,
            'Trail4_R_Ctrl' ,
            'Trail3_R_Ctrl' ,
            'Trail2_R_Ctrl' ,
            'Trail1_R_Ctrl' ,
            ] ;

    def charGet_btn_cmd ( self , *args ) :

        selection = pm.ls ( sl = True ) [0] ;
        pm.textField ( self.char_textField , e = True , text = selection ) ;

    def planeGet_btn_cmd ( self , *args ) :

        selection = pm.ls ( sl = True ) [0] ;
        pm.textField ( self.plane_textField , e = True , text = selection ) ;

    def makePlaneProxy_func ( self , target , *args ) :

        target = pm.PyNode ( target ) ;

        if not pm.objExists ( target.nodeName() + '_PlaneProxy' ) :
            planeProxy = pm.duplicate ( target ) [0] ;
            planeProxy.rename ( target.nodeName() + '_PlaneProxy' ) ;

            pm.parent ( planeProxy , w = True ) ;

            self.showHiddenAttr ( target = planeProxy ) ;
            self.unlockAllAttr ( target = planeProxy ) ;
            self.freezeTransform ( target = planeProxy ) ;
            self.deleteHistory ( target = planeProxy ) ;

        else :
            planeProxy = pm.PyNode ( target.nodeName() + '_PlaneProxy' ) ;

        if pm.checkBox ( self.planeRecreateUV_cbx , q = True , v = True ) :

            planeProxyShape = planeProxy.getShape() ;

            pm.polyAutoProjection ( planeProxyShape.nodeName() + '.f[*]' ,
                layoutMethod = 0 ,
                projectBothDirections = 0 ,
                insertBeforeDeformers = 1 ,
                createNewMap = 0 ,
                layout = 2 ,
                scaleMode = 1 ,
                optimize = 1 ,
                planes = 6 ,
                percentageSpace = 0.2 ,
                worldSpace = 0
                ) ;

        return planeProxy ;

    def attach_btn_cmd ( self , *args ) :

        godEmperor_ctrl_list = self.godEmperor_ctrl_list ;

        if pm.textField ( self.char_textField , q = True , text = True ) :

            char    = pm.textField ( self.char_textField , q = True , text = True ) ;
            char    = pm.PyNode ( char ) ;

            if pm.textField ( self.plane_textField , q = True , text = True ) :

                plane   = pm.textField ( self.plane_textField , q = True , text = True ) ;
                plane   = pm.PyNode ( plane ) ;

                namespace = char.namespace() ;
                
                if not pm.objExists ( namespace + 'SlideOnSurface_RigGrp' ) :
                    sosGroup = pm.group ( em = True , w = True , n = namespace + 'SlideOnSurface_RigGrp' ) ;
                    sosGroup.v.set ( 0 ) ;
                else :
                    sosGroup = pm.PyNode ( namespace + 'SlideOnSurface_RigGrp' ) ;

                planeProxy = self.makePlaneProxy_func ( target = plane ) ;
                planeProxyShape = planeProxy.getShape() ;

                pm.parent ( planeProxy , sosGroup ) ;
                planeProxy.v.set ( 0 ) ;

                for ctrl in godEmperor_ctrl_list :

                    ctrl = pm.PyNode ( namespace + ctrl ) ;
                    name = ctrl.split ( '_Ctrl' ) [0] ;

                    #####################
                    ''' Create Groups '''
                    #####################

                    ctrlOffset = ctrl.getParent() ;
                    ctrlZro = ctrlOffset.getParent() ;

                    ctrlZroProxy = pm.group ( em = True , w = True , n = name + '_CtrlZroProxy' ) ;
                    pm.parent ( ctrlZroProxy , sosGroup ) ;
                    self.copyRO ( driver = ctrlZro , driven = ctrlZroProxy ) ;
                    self.parCon ( driver = ctrlZro , driven = ctrlZroProxy , mo = False , clear = False ) ;

                    refGroup = pm.group ( em = True , w = True , n = name + '_RefGrp' ) ;
                    movGroup = pm.group ( em = True , w = True , n = name + '_MovGrp' ) ;

                    pm.parent ( refGroup , movGroup , ctrlZroProxy ) ;
                    
                    self.copyRO ( driver = ctrlOffset , driven = refGroup ) ;
                    self.copyRO ( driver = ctrlOffset , driven = movGroup ) ;

                    self.parCon ( driver = ctrlOffset , driven = refGroup , mo = False , clear = True ) ;
                    self.parCon ( driver = ctrlOffset , driven = movGroup , mo = False , clear = True ) ;

                    pm.orientConstraint ( sosGroup , refGroup , mo = False , skip = 'none' ) ; 

                    #####################
                    ''' Create Fol '''
                    #####################

                    folShape    = pm.createNode ( 'follicle' ) ;
                    fol         = folShape.getParent() ;
                    fol.rename ( name + 'Fol' ) ;

                    pm.parent ( fol , sosGroup ) ;

                    folFixGroup = pm.group ( em = True , w = True , n = name + '_FolFixGrp' ) ;
                    folFixGroup.rx.set ( 90 ) ;
                    folFixGroup.rz.set ( 180 ) ;
                    folFixGroup.t.lock() ;
                    folFixGroup.r.lock() ;
                    folFixGroup.s.lock() ;
                    pm.parent ( folFixGroup , fol ) ;

                    folShape.simulationMethod.set ( 0 ) ;

                    #folShape.outRotate      >> fol.rotate ;
                    ctrlZroProxy.rotate      >> fol.rotate ;
                    folShape.outTranslate   >> fol.translate ;

                    planeProxyShape.worldMatrix  >> folShape.inputWorldMatrix ;
                    planeProxyShape.outMesh      >> folShape.inputMesh ;

                    cpm = pm.shadingNode ( 'closestPointOnMesh' , asUtility = True , n = name + '_Cpm'  ) ;

                    planeProxyShape.outMesh >> cpm.inMesh ;

                    dcm = pm.shadingNode ( 'decomposeMatrix' , asUtility = True , n = name + '_Dcm' ) ;

                    ctrlZroProxy.worldMatrix >> dcm.inputMatrix ;
                    dcm.outputTranslateX >> cpm.inPositionX ;
                    dcm.outputTranslateY >> cpm.inPositionY ;
                    dcm.outputTranslateZ >> cpm.inPositionZ ;

                    cpm.result.parameterU >> folShape.parameterU ;
                    cpm.result.parameterV >> folShape.parameterV ;

                    #####################
                    ''' Getting Ty '''
                    #####################

                    dcm = pm.shadingNode ( 'decomposeMatrix' , asUtility = True , n = name + 'TY_Dcm' ) ;
                    mmt = pm.shadingNode ( 'multMatrix' , asUtility = True , n = name + 'TY_mmt' ) ;

                    fol.worldMatrix >> mmt.matrixIn[0] ;
                    movGroup.parentInverseMatrix[0] >> mmt.matrixIn[1] ;

                    mmt.matrixSum >> dcm.inputMatrix ;

                    dcm.outputTranslate >> movGroup.t ;

                    self.parCon ( driver = movGroup , driven = ctrlOffset , mo = False ) ;

    def detach_btn_cmd ( self , *args ) :

        godEmperor_ctrl_list = self.godEmperor_ctrl_list ;

        if pm.textField ( self.char_textField , q = True , text = True ) :

            char    = pm.textField ( self.char_textField , q = True , text = True ) ;
            char    = pm.PyNode ( char ) ;

            namespace = char.namespace() ;
            
            if not pm.objExists ( namespace + 'SlideOnSurface_RigGrp' ) :
                
                pass ;
            else :
                
                pm.delete ( namespace + 'SlideOnSurface_RigGrp' ) ;

                for ctrl in godEmperor_ctrl_list :
                    ctrl = pm.PyNode ( namespace + ctrl ) ;
                    ctrlOffset = ctrl.getParent() ;

                    for attr in [ 't' , 'r' ] :
                        for axis in [ 'x' , 'y' , 'z' ] :
                            pm.setAttr ( ctrlOffset + '.' + attr + axis , 0 ) ;

class Gui ( object ) :

    def __init__ ( self ) :

        super ( Gui , self ).__init__ () ;

        self.ui         = 'slideOnMesh_ui' ;
        self.width      = 350.00 ;
        self.title      = 'Slide On Mesh' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    # requestioning
    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def showGui ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                pm.text ( label = 'Character' ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/3*2 ) , ( 2 , w/3 ) ] ) :
                    self.char_textField = pm.textField () ;
                    self.charGet_btn    = pm.button ( label = 'Get' , c = self.charGet_btn_cmd ) ;

                pm.separator ( h = 10 ) ;

                pm.text ( label = 'Plane' ) ;

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/3*2 ) , ( 2 , w/3 ) ] ) :
                    self.plane_textField = pm.textField () ;
                    self.planeGet_btn    = pm.button ( label = 'Get' , c = self.planeGet_btn_cmd ) ;

                self.planeRecreateUV_cbx = pm.checkBox ( label = 'Recreate Plane UV (Auto)' ) ;

                pm.separator ( h = 10 ) ;

                self.attach_btn = pm.button ( label = 'Attach' , w = w , c = self.attach_btn_cmd ) ;
                self.detach_btn = pm.button ( label = 'Detach' , w = w , c = self.detach_btn_cmd ) ;

        window.show () ;

class Main ( General , GuiFunc , Gui ) :

    def __init__ ( self ) :
        super ( Main , self ).__init__ () ;

def run ( *args ) :
	gui = Main() ;
	gui.showGui() ;