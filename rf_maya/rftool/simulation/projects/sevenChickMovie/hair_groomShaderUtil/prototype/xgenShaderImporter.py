import pymel.core as pm ;
import os ;

width = 250.00 ;
uiTitle = 'xGen Shader Importer' ;

selfPath = os.path.realpath (__file__) ;
fileName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( fileName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

xGenPath = selfPath + 'asset/xgenShader' ;

def separator ( h = 5 , *args ) :
    
    pm.separator ( vis = False , h = h ) ;

def updatexGenList_cmd ( *args ) :

    pm.textScrollList ( 'xGenList_tsl' , e = True , ra = True ) ;

    xGenList = os.listdir ( xGenPath ) ;
    xGenList.sort ( ) ;

    for each in xGenList :

        pm.textScrollList ( 'xGenList_tsl' , e = True , append = each ) ;

def import_cmd ( *args ) :

    selectedXGen = pm.textScrollList ( 'xGenList_tsl' , q = True , si = True ) [0] ;

    file = xGenPath + '/' + selectedXGen ;

    pm.system.importFile ( file ) ;

#file = r'D:\Dropbox\doubleMonkey\TwoHeroes\film001\q0330\s0050\hanumanCasual\data\shotCache\camera\cam_grp.camera.v002_001.abc' ;
#pm.system.importFile ( file , gr = True , groupName = 'test_grp' ) ;
#file -import -type "mayaAscii"  -ignoreVersion -mergeNamespacesOnClash false -rpr "hanumanCasualSuit_xgnShd" -options "v=0;"  -importFrameRate true  -importTimeRange "override" "";


def run ( *args ) :
    
    # check if window exists
    if pm.window ( 'xGenShaderImporter_UI' , exists = True ) :
        pm.deleteUI ( 'xGenShaderImporter_UI' ) ;
    else : pass ;
   
    window = pm.window ( 'xGenShaderImporter_UI', title = uiTitle , mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    pm.window ( 'xGenShaderImporter_UI' , e = True , w = width , h = 10 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :

            pm.textScrollList ( 'xGenList_tsl' , w = width , h = 200 ) ;
            pm.button ( label = 'import' , w = width , c = import_cmd ) ;

    updatexGenList_cmd ( ) ;
  
    window.show () ;
