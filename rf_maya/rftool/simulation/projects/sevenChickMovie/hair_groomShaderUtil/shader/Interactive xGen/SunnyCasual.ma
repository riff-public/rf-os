//Maya ASCII 2017ff05 scene
//Name: SunnyCasual_XgS.ma
//Last modified: Sat, May 05, 2018 10:50:17 PM
//Codeset: 1252
requires maya "2017ff05";
requires -nodeType "hairPhysicalShader" "hairPhysicalShader" "1.0";
requires "stereoCamera" "10.0";
requires "3delight_for_maya2010" "2";
requires "FurryBall_2012" "4.0.2917";
requires "stereoCamera" "10.0";
requires "PVstFlexSliderNode.py" "Unknown";
requires "PO_Reader_v2012_64" "1.0";
requires "pfOptions.py" "1.0";
requires "TurtleForMaya2009" "5.0.0.5";
requires "arkMayaExporter_2013_5" "1.21.ma3.s2.me5.a7";
requires "vsMaster" "1.0";
requires "vsVmtParamConversion.py" "1.0";
requires "vstUtils" "1.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
createNode hairPhysicalShader -n "SunnyCasual_XgS";
	rename -uid "3115E466-44BA-A11C-E05B-F5B0C5ED32D5";
	setAttr ".rcD" -type "float3" 0 0 0 ;
	setAttr ".tcD" -type "float3" 0.0070000002 0.0020222305 0.00053900015 ;
	setAttr -l on ".ac";
	setAttr -l on ".incand";
	setAttr ".cR" -type "float3" 0.69230771 0.69230771 0.69230771 ;
	setAttr ".iR" 0.20000000298023224;
	setAttr ".cTT" -type "float3" 0.32167831 0.11523451 0.053720295 ;
	setAttr ".iTT" 0.5;
	setAttr -l on ".lsTT" -4;
	setAttr ".awTT" 25;
	setAttr ".cTRT" -type "float3" 0.30769232 0.13496007 0.048381899 ;
	setAttr ".iTRT" 0.25;
	setAttr -l on ".cG" -type "float3" 0 0 0 ;
	setAttr -l on ".cG";
	setAttr -l on ".iG" 0;
	setAttr -l on ".asG" 0;
	setAttr -l on ".awG" 0;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -s 2 ".r";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "SunnyCasual_XgS.msg" ":defaultShaderList1.s" -na;
connectAttr "SunnyCasual_XgS.oc" ":internal_standInSE.ss";
// End of SunnyCasual_XgS.ma
