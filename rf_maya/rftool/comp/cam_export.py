# export camera for comp
uiName = 'exportCamUI'
import os
import sys
import subprocess
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
import pymel.core as pm
from rftool.utils import maya_utils
from rftool.rig.utils import rivet
from rf_utils import file_utils
import re

reload(maya_utils)
reload(rivet)

def export(camera, nulls=[], outputPath=''):
	exportCam = duplicate_cam(camera)
	mc.setAttr('{}.t'.format(exportCam), lock=0)
	mc.setAttr('{}.r'.format(exportCam), lock=0)
	mc.setAttr('{}.s'.format(exportCam), lock=0)
	transfer_anim(camera, exportCam)

	if not outputPath:
		outputPath = output_path(camera)
		prepare_output_dir(outputPath)
	locator_export = mc.ls(sl=True, type='locator')
	exportObjs = nulls + [exportCam] if nulls else exportCam
	exportObjs = exportObjs+locator_export if locator_export else exportObjs

	result = maya_utils.export_selection(outputPath, exportObjs)
	logger.info('Export camera %s -> %s' % (camera, outputPath))
	mc.delete(exportCam) if result else logger.error('Export camera failed')
	return result


def export_null(targetObjects, outputPath=''):
	nullLoc = []
	for targetObject in targetObjects:
		objName = targetObject.split(':')[-1]
		name = 'Null_%s' % (objName)
		loc = mc.spaceLocator(n=name)[0]
		snap(loc, targetObject)
		nullLoc.append(loc)

	if not outputPath:
		outputPath = output_path('null')
		prepare_output_dir(outputPath)

		result = maya_utils.export_selection(outputPath, nullLoc)
	mc.delete(nullLoc)
	return result


def create_null(targetObject):
	
	name = generate_name(targetObject)
	loc = mc.spaceLocator(n='%s_loc'%name)[0]
	snap_point(loc, targetObject)
	return loc

def create_null_rivet(targetObject):
	loc_name = generate_name(targetObject[0])
	loc = rivet.run(prefix = loc_name)
	return loc

def generate_name(obj_name): 
	obj = pm.PyNode(obj_name)
	namespace = obj.namespace()
	obj_typ = obj.nodeType()

	if namespace and obj_typ == 'mesh':
		name = obj.name().split('.')[0].split(namespace)[-1]
	elif namespace and obj_typ == 'transform':
		name = obj.name().split(namespace)[-1]
	elif not namespace and obj_typ == 'mesh':
		name = obj.name().split('.')[0]
	elif not namespace and obj_typ == 'transform':
		name = obj.name()
	
	if namespace:
		findObjs = mc.ls('null_%s_%s_*' % (namespace, name))
		name_space_check = True
	else:
		findObjs = mc.ls('null_%s_*' % (name))
		name_space_check = False

	output_version = 1
	if findObjs:
		list_num = []
		for ix in findObjs:
			match = re.findall('_[0-9]_', ix)
			if match:
				count_num = int(match[0].replace('_', ''))
				list_num.append(count_num)
			
		output_version = int(max(list_num)) + 1

	if name_space_check:
		loc_name = 'null_%s_%s_%s'%(namespace, name, output_version)
	else:
		loc_name = 'null_%s_%s'%(name, output_version)
	return loc_name


def output_path(name):
	name = name.replace(':', '_') if ':' in name else name
	dirname = os.path.dirname(mc.file(q=True, sn=True) or mc.file(q=True, loc=True))
	outputPath = '%s/%s/%s.ma' % (dirname, 'export', name)
	return outputPath

def snap(obj, target):
	mc.delete(mc.parentConstraint(target, obj))

def snap_point(obj, target):
	pos = mc.xform(target, q=True, t=True, ws=True)
	mc.xform(obj, ws=True, t=pos)


def duplicate_cam(camera):
	dups = mc.duplicate(camera)
	exportCam = dups[0]
	# clear other nodes
	childs = mc.listRelatives(exportCam, c=True, f=True)
	removeObjs = [a for a in childs if not mc.objectType(a, isType='camera')]
	mc.delete(removeObjs)
	# unlock node
	unlock_cam(exportCam)
	if mc.listRelatives(exportCam, p=True):
		exportCam = mc.parent(exportCam, w=True)[0]
	return exportCam

def prepare_output_dir(filename):
	if not os.path.exists(os.path.dirname(filename)):
		os.makedirs(os.path.dirname(filename))
	return os.path.dirname(filename)

def unlock_cam(camera):
	camShape = mc.listRelatives(camera, s=True, f=True)
	maya_utils.lock_transform([camera, camShape[0]], False)


def transfer_anim(srcCam, dstCam):
	# make connections
	pcNode = mc.parentConstraint(srcCam, dstCam)
	scNode = mc.scaleConstraint(srcCam, dstCam)
	mc.connectAttr('%s.fl' % srcCam, '%s.fl' % dstCam, f=True)
	bake_key(dstCam)
	mc.delete(pcNode)
	mc.delete(scNode)


def bake_key(targetObject, start=int(), end=int()):
	startTime = int(mc.playbackOptions(q=True, min=True))
	endTime = int(mc.playbackOptions(q=True, max=True))
	start = startTime if not start else start
	end = endTime if not end else end
	mc.bakeResults(targetObject, t=(start, end), sb=1)


class UI(object):
	"""docstring for UI"""
	def __init__(self):
		super(UI, self).__init__()
		self.maya_ui()

	def maya_ui(self):
		self.remove_ui(uiName)
		self.window = mc.window(uiName, title='Export cam and null object')
		layout = mc.columnLayout(adj=1, rs=4)
		mc.text(l='Select object or vertices')
		self.exportNullButton = mc.button(l='1. Create Null', h=30, c=self.create_null)
		mc.text(l='Please select camera')
		self.exportCamButton = mc.button(l='2. Export Cam', h=30, c=self.export_cam)
		mc.text('')
		self.openButton = mc.button(l='Show output files', h=30, c=self.show_output)
		self.copyToComp = mc.button(l='Copy to comp', h=30, c=self.copy_to_comp)
		mc.showWindow()
		mc.window(uiName, e=True, wh=[240, 200])

	def export_cam(self, *args):
		sels = mc.ls(sl=True)
		nulls = self.get_nulls()
		for camera in sels:
			shape = mc.listRelatives(camera, s=True, f=True)

			if mc.objectType(shape[0], isType='camera') if shape else None:
				shortName = camera.split('|')[-1]
				outputPath = self.output_path(shortName)
				export(camera, nulls, outputPath)

	def get_nulls(self, *args):
		""" get locator start with null """
		null = 'null'
		locatorShapes = mc.ls(type='locator')
		# nulls = [mc.listRelatives(a, p=True)[0] for a in locatorShapes if 'null' in mc.listRelatives(a, p=True)[0]]
		nulls = [mc.listRelatives(a, p=True)[0] for a in locatorShapes if 'null' == mc.listRelatives(a, p=True)[0][0:4]]
		return nulls

	def create_null(self, *args):
		sels = mc.ls(sl=True,fl=True)
		print sels[0]
		if '.e' in sels[0]:
			targetObject = sels
			create_null_rivet(targetObject)
		else:
			for targetObject in sels:
				create_null(targetObject)

	def export_null(self, *args):
		sels = mc.ls(sl=True)
		name = 'null'
		result = mc.promptDialog(title='Filename', message='Enter Filename', button=['Export', 'Cancel'])
		if result == 'Export':
			filename = mc.promptDialog(q=True, text=True) or name
			outputPath = self.output_path(filename)
			export_null(sels, outputPath)

	def show_output(self, *args):
		outputDir = os.path.dirname(self.output_path('', create=True).replace('/', '\\'))
		subprocess.Popen(r'explorer /select,"%s"' % outputDir)

	def copy_to_comp(self, *args):
		# try to back to comp dir
		# current P:/CloudMaker/scene/film001/q0010/S0070/light/maya/work/cm_film001_q0010_S0070_light_v001_AN.ma
		# copy to P:\CloudMaker\scene\film001\q0010\S0070\comp\camera
		outputDir = os.path.dirname(self.output_path('', create=True))
		shotDir = '/'.join(outputDir.split('/')[0:-4])
		srcFiles = file_utils.list_file(outputDir)

		dstDir = '%s/comp/camera' % shotDir

		for srcFile in srcFiles:
			src = '%s/%s' % (outputDir, srcFile)
			dst = '%s/%s' % (dstDir, srcFile)
			file_utils.copy(src, dst)
			logger.info('Copy %s' % dst)


	def output_path(self, name, create=True):
		currentFile = mc.file(q=True, sn=True) or mc.file(q=True, loc=True)
		dirname = '%s/%s' % (os.path.dirname(currentFile), 'export')

		if not os.path.exists(dirname) and create:
			os.makedirs(dirname)

		name = name.replace(':', '_') if ':' in name else name

		return '%s/%s.ma' % (dirname, name)

	def remove_ui(self, uiName):
		if mc.window(uiName, exists=True):
			mc.deleteUI(uiName)
			self.remove_ui(uiName)


def show():
	app = UI()
	return app

