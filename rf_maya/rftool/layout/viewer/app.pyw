# v.0.0.1 polytag switcher
_title = 'Tool'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'CheckupUI'

#Import python modules
import sys
import os 
import json 
import getpass
import subprocess

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# config file 
config_file = '%s/local_config.json' % moduleDir
with open(config_file) as configData: 
    localConfig = json.load(configData)

SCRIPT = localConfig['SCRIPT']
ROOT = os.environ.get(SCRIPT)
core = localConfig['CORE']

# import config
sys.path.append('%s/%s' % (ROOT, core))
import rf_config as config

# framework modules 
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

configfile = 'O:/Pipeline/core/rf_maya/rftool/layout/projDataBase/projDataBase.py'
sys.path.append(os.path.dirname(os.path.expanduser(configfile)))
import projDataBase
from rftool.layout.review import db_hook
 
qtCreatorFile = "O:/Pipeline/core/rf_maya/rftool/layout/app/Qt/UI/checkUp.ui" # Enter file here.
rv            = "C:\\Program Files\\Shotgun\\RV-7.2.1\\bin\\rv.exe"
quickTime     = "C:\\Program Files (x86)\\QuickTime\\QuickTimePlayer.exe"
mpc           = "C:\\Program Files (x86)\\K-Lite Codec Pack\\MPC-HC64\\mpc-hc64.exe"
projList      = projDataBase.projList() 


class StandAlone(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(StandAlone, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        if config.isMaya: 
            self.ui = load.setup_ui_maya(uiFile, parent)
        else: 
            self.ui = load.setup_ui(uiFile, self)
        
        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.set_signals()


    def set_signals(self): 

        self.ui.projList.addItems(projList)
        self.ui.depCombo.activated.connect(self.listSeq)
        self.ui.projList.itemClicked.connect(self.listEp)
        self.ui.epList.itemClicked.connect(self.listSeq)
        self.ui.seqList.itemClicked.connect(self.listPubShot)
        self.ui.shotPubList.itemClicked.connect(self.listVerShot)
        self.ui.playerCombo.activated.connect(self.playerSelector)
        self.ui.pubPlayBtn.clicked.connect(self.playPubButton)
        self.ui.verPlayBtn.clicked.connect(self.playVerButton)
        self.ui.pubBrowseBtn.clicked.connect(self.runPubExplorer)
        self.ui.verBrowseBtn.clicked.connect(self.runVerExplorer)
        self.ui.submitSgBtn.clicked.connect(self.submitSG)

#====================== Listing UI functions =========================
    
    def listEp(self):
        selProj  = self.ui.projList.currentItem().text()
        pipeline = projDataBase.projDataBase('P:/'+selProj) [1]        
        if pipeline == 0:
            epList = os.listdir('P:/%s/scene' %selProj)
        elif pipeline == 1:
            epList = os.listdir('P:/%s' %selProj)        
        self.ui.epList.clear()
        self.ui.seqList.clear()
        self.ui.shotPubList.clear()
        self.ui.shotVerList.clear()        
        self.ui.epList.addItems(epList)
        self.ui.epList.sortItems(0)

    def listSeq(self):
        #try:
        selProj  = self.ui.projList.currentItem().text()
        selEp    = self.ui.epList.currentItem().text()
        selDep   = self.ui.depCombo.currentText()
        pipeline = projDataBase.projDataBase('P:/'+selProj) [1]           
        if pipeline == 0:
            seqList = os.listdir('P:/%s/scene/%s' %(selProj,selEp))
        elif pipeline == 1:
            seqList = os.listdir('P:/%s/%s/scene/%s' %(selProj,selEp,selDep))
        self.ui.seqList.clear()
        self.ui.shotPubList.clear()
        self.ui.shotVerList.clear()
        self.ui.seqList.addItems(seqList)
        self.ui.seqList.sortItems(0)
        #except:
        #    self.ui.seqList.clear()
        #    self.ui.shotPubList.clear()
        #    self.ui.shotVerList.clear()
    
    def listPubShot(self):
        try:
            selProj  = self.ui.projList.currentItem().text()
            selEp    = self.ui.epList.currentItem().text()
            selDep   = self.ui.depCombo.currentText()
            seqItem  = self.ui.seqList.currentItem().text()
            pipeline = projDataBase.projDataBase('P:/'+selProj) [1]           
            if pipeline == 0:
                pubList = os.listdir('P:/%s/scene/%s/%s/playblast/%s' %(selProj,selEp,seqItem,selDep))
            elif pipeline == 1:
                pubList = os.listdir('P:/%s/%s/scene/%s/preview'      %(selProj,selEp,selDep))
            self.ui.shotPubList.clear()
            self.ui.shotVerList.clear()            
            self.ui.shotPubList.addItems(pubList)
            self.ui.shotPubList.sortItems(0) 
        except:
            self.ui.shotPubList.clear()
            self.ui.shotVerList.clear()  
            
    def listVerShot(self):          
        try:
            selProj  = self.ui.projList.currentItem().text()
            selEp    = self.ui.epList.currentItem().text()
            selDep   = self.ui.depCombo.currentText()
            seqItem  = self.ui.seqList.currentItem().text()
            pubItem  = self.ui.shotPubList.currentItem().text()
            pipeline = projDataBase.projDataBase('P:/'+selProj) [1]           
            if pipeline == 0:
                shotName = pubItem.split('_')[3]
                verList  = os.listdir('P:/%s/scene/%s/%s/%s/%s/maya/playblast' %(selProj,selEp,seqItem,shotName,selDep))
            elif pipeline == 1:
                shotName = pubItem.split('_')[0]
                verList  = os.listdir('P:/%s/%s/scene/%s/%s/output/preview'    %(selProj,selEp,selDep,shotName))
            self.ui.shotVerList.clear()            
            self.ui.shotVerList.addItems(verList)
            self.ui.shotVerList.sortItems(1) 
        except:
            self.ui.shotVerList.clear()  

#====================== System functions =========================

    def playerSelector(self):
        playerLabel = self.playerCombo.currentText()
        if playerLabel == 'RV':
            player = eval('rv')
        elif playerLabel == 'QuickTime':
            player = eval('quickTime')
        elif playerLabel == 'Media Player Classic':
            player = eval('mpc')
        return player
        
    def playPubButton(self):
        selProj  = self.ui.projList.currentItem().text()
        selEp    = self.ui.epList.currentItem().text()
        selDep   = self.ui.depCombo.currentText()
        seqItem  = self.ui.seqList.currentItem().text()
        selItem  = [item.text() for item in self.ui.shotPubList.selectedItems()]
        pipeline = projDataBase.projDataBase('P:/'+selProj) [1]
        player   = self.playerSelector()
        files = [player]
        if pipeline == 0:
            filePathPubTxt = 'P:\\%s\\scene\\%s\\%s\\playblast\\%s' %(selProj,selEp,seqItem,selDep)
        elif pipeline == 1:
            filePathPubTxt = 'P:\\%s\\%s\\scene\\%s\\preview'      %(selProj,selEp,selDep)
        if selItem:
            for i in selItem:
                files.append(filePathPubTxt + "\\" + str(i))
            subprocess.Popen(files)
        else:
            pass
            
    def playVerButton(self):
        selProj  = self.ui.projList.currentItem().text()
        selEp    = self.ui.epList.currentItem().text()
        seqItem  = self.ui.seqList.currentItem().text()
        verItem  = self.ui.shotVerList.currentItem().text()
        selDep   = self.ui.depCombo.currentText()
        pipeline = projDataBase.projDataBase('P:/'+selProj) [1]
        player   = self.playerSelector()
        if pipeline == 0:
            shotName = verItem.split('_')[3]
            filePathTxt = 'P:\\%s\\scene\\%s\\%s\\%s\\%s\\maya\\playblast' %(selProj, selEp, seqItem, shotName, selDep) 
        elif pipeline == 1:
            shotName = verItem.split('_')[0]
            filePathTxt = 'P:\\%s\\%s\\scene\\%s\\%s\\output\\preview'    %(selProj, selEp, selDep, shotName)    
        if verItem:
            print [player, filePathTxt + "\\" + str(verItem)]
            subprocess.Popen([player, filePathTxt + "\\" + str(verItem)])
        else:
            pass
            
    def runPubExplorer(self):
        selProj  = self.ui.projList.currentItem().text()
        selEp    = self.ui.epList.currentItem().text()
        seqItem  = self.ui.seqList.currentItem().text()
        selDep   = self.ui.depCombo.currentText()
        pipeline = projDataBase.projDataBase('P:/'+selProj) [1]
        player   = self.playerSelector()
        if pipeline == 0:
            filePathPubTxt = 'P:/%s/scene/%s/%s/playblast/%s' %(selProj,selEp,seqItem,selDep)
        elif pipeline == 1:
            filePathPubTxt = 'P:/%s/%s/scene/%s/preview'      %(selProj,selEp,selDep)    
        os.startfile(str(filePathPubTxt))
        
    def submitSG(self):
        reload(db_hook)
        pubItem  = self.ui.shotPubList.currentItem().text()
        selItem  = [item.text() for item in self.ui.shotPubList.selectedItems()]
        selCount = len(selItem)
        num      = 1
        for i in selItem:
            selProj          = self.ui.projList.currentItem().text()
            shotNameSplit    = str(i).split('_')
            workPath         = 'P:/%s/scene/%s/%s/%s/%s/maya/work'      %(selProj, shotNameSplit[1], shotNameSplit[2], shotNameSplit[3], shotNameSplit[4][:-4])
            outputVerPath    = 'P:/%s/scene/%s/%s/%s/%s/maya/playblast' %(selProj, shotNameSplit[1], shotNameSplit[2], shotNameSplit[3], shotNameSplit[4][:-4])            
            workList         = os.listdir(workPath)
            outputVerList    = os.listdir(outputVerPath)
            latestWorkFile   = max(workList)
            latestOutputFile = max(outputVerList)
            userName         = mc.optionVar(q = 'RFUser')
            db_hook.send_to_review(workPath + '/' + latestWorkFile, outputVerPath + '/' + latestOutputFile, userName)
            print 'Submit %s (%s): done.' % (latestOutputFile, str(num) + '/' + str(selCount))
            num += 1
        mc.warning('Submitting done.')
        
    def runVerExplorer(self):
        selProj  = self.ui.projList.currentItem().text()
        selEp    = self.ui.epList.currentItem().text()
        seqItem  = self.ui.seqList.currentItem().text()
        selDep   = self.ui.depCombo.currentText()
        verItem  = self.ui.shotVerList.currentItem().text()
        pipeline = projDataBase.projDataBase('P:/'+selProj) [1]
        player   = self.playerSelector()   
        if pipeline == 0:
            shotName = verItem.split('_')[3]
            filePathTxt = 'P:/%s/scene/%s/%s/%s/%s/maya/playblast' %(selProj, selEp, seqItem, shotName, selDep) 
        elif pipeline == 1:
            shotName = verItem.split('_')[0]
            filePathTxt = 'P:/%s/%s/scene/%s/%s/output/preview'    %(selProj, selEp, selDep, shotName)
        os.startfile(str(filePathTxt))  


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = StandAlone(maya_win.getMayaWindow())
        return myApp

    else: 
        logger.info('Run in standalone\n')
        app = QtWidgets.QApplication.instance()
        if not app: 
            app = QtWidgets.QApplication(sys.argv)
        myApp = StandAlone()
        stylesheet.set_default(app)
        sys.exit(app.exec_())

if __name__ == '__main__': 
    show()



              


