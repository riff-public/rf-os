import maya.cmds as mc
def selectShape():
    sel = mc.ls(sl = True)
    selShape = []
    for i in sel:
        selShape.append(mc.listRelatives(i)[0])
    mc.select(d = True)    
    for i in selShape:    
        mc.select(i, add = True)
