import maya.cmds as cmd
from collections import OrderedDict 
def bakeCam(*args):
    camShape = cmd.ls(sl = True)[0]
    camTransform = camShape[0:-5]
    minFrame = cmd.playbackOptions(q = True, min = True)
    maxFrame = cmd.playbackOptions(q = True, max = True)
    newCam = cmd.camera()
    cmd.setAttr('%s.rotateOrder' %newCam[0], 2)
    masterCtrl = cmd.ls('*master_ctrl', r = True)[0]
    cmd.parentConstraint(camTransform, newCam[0])
    cmd.bakeResults(newCam[0], t = (minFrame,maxFrame))
    flKeyCount = cmd.keyframe(masterCtrl, at = 'focal_length', q = True, kc = True)
    if flKeyCount >= 1:
        cmd.copyKey(masterCtrl, at = 'focal_length')
        cmd.pasteKey(newCam[1], at = 'fl')
    else:
        camFL = cmd.getAttr('%s.focal_length' %masterCtrl)
        cmd.setAttr('%s.fl' %newCam[1], camFL)
    cmd.delete('%s_parentConstraint1' %newCam[0])
    cmd.rename(newCam[0], '%s_camshot' %camTransform) 

