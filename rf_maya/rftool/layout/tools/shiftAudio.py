import maya.cmds as mc

def shiftAudio():
    sel = mc.ls(type = 'audio')
    for i in sel:
        mc.setAttr("%s.offset" %i, 101)
