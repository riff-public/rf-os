import maya.cmds as cmd
def bakeShake(*args):
    shakeCtrl = cmd.ls(sl = True)
    minFrame = cmd.playbackOptions(q=True, min=True)
    maxFrame = cmd.playbackOptions(q=True, max=True)
    shakeCtrlStr = str(shakeCtrl[0])
    subShake = cmd.listRelatives(cmd.ls(sl=True))
    if shakeCtrlStr.endswith("shake_ctrl") == True:
        cmd.spaceLocator(n = 'shakeLoc1')
        cmd.setAttr('shakeLoc1.rotateOrder', 2)           
        cmd.select(subShake[0], r = True)
        cmd.select('shakeLoc1', add = True)
        sel = cmd.ls(sl = True)
        cmd.parent(sel[1], shakeCtrl[0])
        cmd.parentConstraint(sel[0], sel[1])
        cmd.bakeResults(sel[1], t = (minFrame,maxFrame))
        cmd.copyKey(sel[1], at = ['tx','ty','tz','rx','ry','rz'])
        cmd.pasteKey(shakeCtrl[0], at = ['tx','ty','tz','rx','ry','rz'])
        cmd.delete(sel[1])
        cmd.setAttr('%s.seed' %shakeCtrl[0], 0)
        cmd.setAttr('%s.offset' %shakeCtrl[0], 0)
        cmd.setAttr('%s.frequencyTransX' %shakeCtrl[0], 0)
        cmd.setAttr('%s.frequencyTransY' %shakeCtrl[0], 0)
        cmd.setAttr('%s.amplitudeTransX' %shakeCtrl[0], 0)
        cmd.setAttr('%s.amplitudeTransY' %shakeCtrl[0], 0)
        cmd.setAttr('%s.frequencyRotX' %shakeCtrl[0], 0)
        cmd.setAttr('%s.frequencyRotY' %shakeCtrl[0], 0)
        cmd.setAttr('%s.frequencyRotZ' %shakeCtrl[0], 0)
        cmd.setAttr('%s.amplitudeRotX' %shakeCtrl[0], 0)
        cmd.setAttr('%s.amplitudeRotY' %shakeCtrl[0], 0)
        cmd.setAttr('%s.amplitudeRotZ' %shakeCtrl[0], 0)
        cmd.setAttr('%s.subFreqRot' %shakeCtrl[0], 0)
        cmd.setAttr('%s.subAmpRot' %shakeCtrl[0], 0) 
     
        cmd.cutKey('%s' %shakeCtrl[0], cl = True, at = ["seed", 
                                             "offset", 
                                             "frequencyTransX", 
                                             "frequencyTransY", 
                                             "amplitudeTransX", 
                                             "amplitudeTransY", 
                                             "frequencyRotX", 
                                             "frequencyRotY", 
                                             "frequencyRotZ", 
                                             "amplitudeRotX", 
                                             "amplitudeRotY", 
                                             "amplitudeRotZ", 
                                             "subFreqRot", 
                                             "subAmpRot"])
        cmd.select(shakeCtrl[0])
    else:
        cmd.warning('Please select shake_ctrl')
