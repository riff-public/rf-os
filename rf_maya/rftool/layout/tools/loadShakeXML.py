from xml.dom import minidom
import pymel.core as pm

def loadShakeXML (*args):
    mydoc    = minidom.parse('D:/RIFF_WORK/Temp/shake_value.xml')
    items    = mydoc.getElementsByTagName('value')
    selPaste = pm.ls(sl = True)
    shakeVal = []

    for elem in items:
        shakeVal.append(float(elem.firstChild.data))

    selPaste = pm.ls(sl = True) [0]
    pm.setAttr(selPaste.seed, shakeVal[0])
    pm.setAttr(selPaste.offset, shakeVal[1])
    pm.setAttr(selPaste.frequencyTransX, shakeVal[2])
    pm.setAttr(selPaste.frequencyTransY, shakeVal[3])
    pm.setAttr(selPaste.amplitudeTransX, shakeVal[4])
    pm.setAttr(selPaste.amplitudeTransY, shakeVal[5])
    pm.setAttr(selPaste.frequencyRotX, shakeVal[6])
    pm.setAttr(selPaste.frequencyRotY, shakeVal[7])
    pm.setAttr(selPaste.frequencyRotZ, shakeVal[8])
    pm.setAttr(selPaste.amplitudeRotX, shakeVal[9])
    pm.setAttr(selPaste.amplitudeRotY, shakeVal[10])
    pm.setAttr(selPaste.amplitudeRotZ, shakeVal[11])
    pm.setAttr(selPaste.subFreqRot, shakeVal[12])
    pm.setAttr(selPaste.subAmpRot, shakeVal[13])
