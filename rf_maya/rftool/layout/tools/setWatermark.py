import maya.cmds as mc

def activeCamera(*args):
    pass
    '''
    import maya.OpenMaya as OpenMaya
    import maya.OpenMayaUI as OpenMayaUI
    view    = OpenMayaUI.M3dView.active3dView()
    cam     = OpenMaya.MDagPath()
    view.getCamera(cam)
    camPath = cam.fullPathName()
    return camPath
    '''
def setWatermark(switch = True, *args):
    pass
    '''
    waterMarkFile = "P:/Temp/Shared/_techAnim/June/juneScript/playblast/imagePlayblastUI/RiFFStudio.png"
    currCamera    = activeCamera()
    activeView    = currCamera.split('|')
    cameraName    = activeView[-1]
    camNearPlane  = mc.getAttr('%s.nearClipPlane' %cameraName)
    imgPlane      = mc.imagePlane(camera = cameraName, n = "waterMark_imagePlane1" )
    mc.camera(cameraName, e = True, ff = 'fill')
    mc.setAttr(imgPlane[1] + ".fit", 1)
    mc.setAttr(imgPlane[1] + ".depth", (camNearPlane + 0.5))
    mc.setAttr(imgPlane[1] + ".alphaGain", 0.2)
    mc.setAttr(imgPlane[1] + ".imageName", waterMarkFile, type = "string")
    mc.select('waterMark_imagePlane1', d = True)
    
    modPan = mc.getPanel(wf = True)
    mc.modelEditor('%s' %modPan, e = True, imp = True)
    '''
    
def deleteWatermark(*args):
    pass
    '''
    try:
        mc.delete('waterMark_imagePlane1')
    except:
        pass
    '''

