import pymel.core as pm

def createMeshRef(*args):
    sel = pm.ls(sl = True)
    shape = pm.listRelatives(sel)
    pm.duplicate ()
    pm.parent (w = True)
    for i in range(len(shape)):
        pm.setAttr ("%s.overrideEnabled" %shape[i], 1)
        pm.setAttr ("%s.overrideDisplayType" %shape[i], 2)
        pm.setAttr ("%s.overrideShading" %shape[i], 0)
        pm.setAttr ("%s.overrideTexturing" %shape[i], 0)
        pm.setAttr ("%s.overridePlayback" %shape[i], 0)
        pm.setAttr ("%s.overrideColor" %shape[i], 1)
