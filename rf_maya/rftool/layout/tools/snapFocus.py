def snapFocus(*args):
    import pymel.core as pm
    obj = pm.ls(sl = True) [0]
    loc = pm.ls('*measureObj_loc') [0]
    pos = pm.xform(obj, q = 1, ws = 1, t = 1)
    pm.xform(loc, t = [pos[0], pos[1], pos[2]])
    distance_node = pm.ls('*distanceDimension')[0]
    distance_value = pm.getAttr("%s.distance" % distance_node)
    pm.setAttr("viewportCam_ctrl.focus_distance", distance_value)
