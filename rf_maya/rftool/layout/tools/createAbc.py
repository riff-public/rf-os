def doCreateAbc(num, *args):
    import maya.cmds as cmd
    import maya.mel as mm
    cmd.sysFile('D:/RIFF_WORK/Temp/', makeDir = True)
    
    start = cmd.playbackOptions(q = True, min = True)
    end = cmd.playbackOptions(q = True, max = True)
    currentFileLoc = cmd.file(q = True, loc = True)
    fileName = currentFileLoc.split('/')[-1]
    root = ''
    save_name = "D:/RIFF_WORK/Temp/alembicTemp%s.abc" %num
    sel = cmd.ls(sl = True)
    for i in sel:
        root = root + '-root ' + i + ' '    
    command = "-frameRange " + str(start) + " " + str(end) +" -uvWrite -worldSpace " + root + " -file " + save_name
    cmd.AbcExport (j = command)
    
    ######### hide rig group
    def findRefTopNode_cmd ( node = '' , *args ) : 
        parent = cmd.listRelatives ( node , p = True ) 
        if parent != [] :
            if cmd.referenceQuery ( parent , isNodeReferenced = True ) == True :  
                return ( findRefTopNode_cmd ( node = parent ) )       
            else: return node 
        else: return node 
            
    selection = cmd.ls ( sl = True ) 
    for i in selection:
        a = findRefTopNode_cmd ( i )
        cmd.select(a, add = True) 

    cmd.createDisplayLayer(n = 'Abc_rigGrp_lyr', nr = True)
    cmd.setAttr('Abc_rigGrp_lyr.visibility', 0)
    
    ######### import abc
    cmd.group(em = True, name = 'alembic_grp')
    mm.eval('AbcImport -mode import -reparent "alembic_grp" "%s";' %save_name)

def clearAbc(*args):
    import maya.cmds as cmd
    ################## restore
    cmd.delete('alembic_grp')
    try:
        cmd.delete('alembicTemp*')
    except:
        pass
    cmd.delete('Abc_rigGrp_lyr')
