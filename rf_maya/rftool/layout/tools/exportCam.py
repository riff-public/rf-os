import maya.cmds as cmd
def exportCam(*args):  
    currentFileLoc = cmd.file(q = True, loc = True)
    aList = currentFileLoc.split('/')
    bList = aList[-1].split('_')
    aList.pop(-1)
    bList = bList[0:4]
    filePath = ''
    fileName = ''    
    for i in aList:
        filePath = filePath + i + '/'
    for i in bList:
        fileName = fileName + i + '_'    
    fileName = fileName + 'cam.ma'    
    output = filePath + fileName
    sel = cmd.ls(sl = True)[0]
    cmd.select (sel, r = True)
    cmd.file ("%s" %output, force = True, options = "v=0;", typ = "mayaAscii", pr = True, es = True)  
