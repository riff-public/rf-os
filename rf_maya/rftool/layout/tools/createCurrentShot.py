import pymel.core as pm

def createCurrShot(*args):
    cam = pm.ls(sl = True)
    start = pm.playbackOptions(q = True, min = True)
    end = pm.playbackOptions(q = True, max = True)
    if not cam:
        pm.shot(st = start, sst = start, et = end, set = end)
    else:
        pm.shot(st = start, sst = start, et = end, set = end, cc = str(cam[0]))
