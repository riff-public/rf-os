import maya.cmds as mc

def getSGfileList(currentFileLoc, *args):
    try:    
        aList          = currentFileLoc.split('/') # split directory heirachy into list for set new playblast directory
        nameOfCurrent  = aList[-1]#[:-8]
        bList          = nameOfCurrent.split('_')#[:-2]  # split file name each "_" for remove username
        filePathVerTxt = aList[0]+'/'+aList[1]+'/'+aList[2]+'/'+aList[3]+'/'+aList[4]+'/'+aList[5]+'/'+aList[6]+'/'+aList[7]+'/playblast'+'/'  
        # publish
        filePathPubTxt = aList[0]+'/'+aList[1]+'/'+aList[2]+'/'+aList[3]+'/'+aList[4]+'/'+'playblast'+'/'+aList[6]+'/'
        pbFilePubTxt   = bList[0]+'_'+bList[1]+'_'+bList[2]+'_'+bList[3]+'_'+bList[4]+'.mov'                
    except: # if file directory is incorrect
        pass    
    verFileList    = mc.getFileList(folder = filePathVerTxt, fs = '*.mov')
    if not verFileList:
        latestVerFile = 'none'
    else:
        latestVerFile = max(verFileList)
    return filePathVerTxt, latestVerFile, filePathPubTxt, pbFilePubTxt
    
def getRIFFfileList(currentFileLoc, *args):
    try:    
        aList = currentFileLoc.split('/') # split directory heirachy into list for set new playblast directory
        nameOfCurrent = aList[-1]#[:-8]
        bList = nameOfCurrent.split('_')#[:-2]  # split file name each "_" for remove username
        filePathVerTxt = aList[0]+'/'+aList[1]+'/'+aList[2]+'/'+aList[3]+'/'+aList[4]+'/'+aList[5]+'/'+'output'+'/'+'preview'+'/'
        # publish
        filePathPubTxt = aList[0]+'/'+aList[1]+'/'+aList[2]+'/'+aList[3]+'/'+aList[4]+'/'+'preview'+'/'
        pbFilePubTxt = bList[0]+'_'+bList[1]+'.mov'
    except: # if file directory is incorrect
        pass    
    verFileList    = mc.getFileList(folder = filePathVerTxt, fs = '*.mov')
    if not verFileList:
        latestVerFile = 'none'
    else:
        latestVerFile  = max(verFileList)
    return filePathVerTxt, latestVerFile, filePathPubTxt, pbFilePubTxt
