def selCtrl(*args):
    import maya.cmds as cmd
    selCtrlIndex = cmd.textScrollList('ctrlListTS', q = True, sii = True)
    selCtrlDict = {1:'*constrain_ctrl', 2:'*master_ctrl', 3:'*root_ctrl', 4:'*shake_ctrl', 5:'*viewportCam_ctrl', 6:'*aim_ctrl', 7:'*aim_gimbal_ctrl', 8:'*up_ctrl'}
    selCtrlList = []
    for i in selCtrlIndex:
        selCtrlList.append(selCtrlDict[i])
    cmd.select(cmd.ls(selCtrlList, r = True)[0])
