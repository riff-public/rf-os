import xml.etree.ElementTree as ET
import pymel.core as pm
import os

def saveShakeXML (*args):
    selCopy = pm.ls(sl = True) [0]
    seed    = pm.getAttr(selCopy.seed)
    offset  = pm.getAttr(selCopy.offset)
    freqTX  = pm.getAttr(selCopy.frequencyTransX)
    freqTY  = pm.getAttr(selCopy.frequencyTransY)
    ampTX   = pm.getAttr(selCopy.amplitudeTransX)
    ampTY   = pm.getAttr(selCopy.amplitudeTransY)
    freqRX  = pm.getAttr(selCopy.frequencyRotX)
    freqRY  = pm.getAttr(selCopy.frequencyRotY)
    freqRZ  = pm.getAttr(selCopy.frequencyRotZ)
    ampRX   = pm.getAttr(selCopy.amplitudeRotX)
    ampRY   = pm.getAttr(selCopy.amplitudeRotY)
    ampRZ   = pm.getAttr(selCopy.amplitudeRotZ)
    subFreq = pm.getAttr(selCopy.subFreqRot)
    subAmp  = pm.getAttr(selCopy.subAmpRot) 
    shakeValue = [ seed,    offset, 
                   freqTX,  freqTY, 
                   ampTX,   ampTY, 
                   freqRX,  freqRY, freqRZ,
                   ampRX,   ampRY,  ampRZ ,
                   subFreq, subAmp ]

    data = ET.Element('shake_data') 
    seedAttr    = ET.SubElement(data, 'value')
    offsetAttr  = ET.SubElement(data, 'value')
    freqTXAttr  = ET.SubElement(data, 'value')
    freqTYAttr  = ET.SubElement(data, 'value')
    ampTXAttr   = ET.SubElement(data, 'value')
    ampTYAttr   = ET.SubElement(data, 'value')
    freqRXAttr  = ET.SubElement(data, 'value')
    freqRYAttr  = ET.SubElement(data, 'value')
    freqRZAttr  = ET.SubElement(data, 'value')
    ampRXAttr   = ET.SubElement(data, 'value')
    ampRYAttr   = ET.SubElement(data, 'value')
    ampRZAttr   = ET.SubElement(data, 'value')
    subFreqAttr = ET.SubElement(data, 'value')
    subAmpAttr  = ET.SubElement(data, 'value')

    seedAttr.set   ('name', 'seed'   )  
    offsetAttr.set ('name', 'offset' )
    freqTXAttr.set ('name', 'freqTX' )
    freqTYAttr.set ('name', 'freqTY' )
    ampTXAttr.set  ('name', 'ampTX'  )
    ampTYAttr.set  ('name', 'ampTY'  )
    freqRXAttr.set ('name', 'freqRX' )
    freqRYAttr.set ('name', 'freqRY' )
    freqRZAttr.set ('name', 'freqRZ' )
    ampRXAttr.set  ('name', 'ampRX'  )
    ampRYAttr.set  ('name', 'ampRY'  )
    ampRZAttr.set  ('name', 'ampRZ'  )
    subFreqAttr.set('name', 'subFreq')
    subAmpAttr.set ('name', 'subAmp' )
      
    seedAttr.text    = str(seed)
    offsetAttr.text  = str(offset)
    freqTXAttr.text  = str(freqTX)
    freqTYAttr.text  = str(freqTY)
    ampTXAttr.text   = str(ampTX)
    ampTYAttr.text   = str(ampTY)
    freqRXAttr.text  = str(freqRX)
    freqRYAttr.text  = str(freqRY)
    freqRZAttr.text  = str(freqRZ)
    ampRXAttr.text   = str(ampRX)
    ampRYAttr.text   = str(ampRY)
    ampRZAttr.text   = str(ampRZ)
    subFreqAttr.text = str(subFreq)
    subAmpAttr.text  = str(subAmp)

    try:
        os.makedirs("D:\\RIFF_WORK\\Temp")
    except:
        pass
    for i in range(2):
        mydata = ET.tostring(data)
        myfile = open("D:/RIFF_WORK/Temp/shake_value.xml", 'w')
        myfile.write(mydata)

    pm.warning('Save shake done')
