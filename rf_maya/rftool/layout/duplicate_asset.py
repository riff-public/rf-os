import sys, os
import re
from collections import OrderedDict

from PySide2 import QtCore, QtWidgets

import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel

from nuTools.pipeline import pipeTools
from nuTools import misc
from rf_utils.context import context_info
from rf_utils.sg import sg_process
from rftool.utils import pipeline_utils
from rftool.utils.ui import maya_win
from rf_app.asm import asm_lib
from rf_maya.rftool.utils import maya_utils
from rf_maya.rftool.shade import shade_utils

def get_available_namespace(name, n=1):
    i = 1
    all_dags = mc.ls(dag=True)
    new_names = []
    while True:
        new_name = '{}_{}'.format(name, str(i).zfill(3))
        if [dag for dag in all_dags if dag.startswith(new_name)]:
            i += 1
        else:
            new_names.append(new_name)
            if len(new_names) == n:
                return new_names
            else:
                i += 1

def duplicate_ref_with_setAttr(sels, numCopy=1):
    new_nss = {}
    refs = pipeTools.getFileRefFromObjects(sels)
    for ref in refs:
        edits = ref.getReferenceEdits()
        if not edits:
            continue

        set_attr_edits = [e for e in edits if e.split(' ')[0] == 'setAttr']
        if not set_attr_edits:
            continue

        # duplicate the reference
        old_ns = ref.namespace
        if not old_ns:
            om.MGlobal.displayError('Duplicate Ref only works with reference with namespace, skipping: %s' %ref.refNode.nodeName())
            continue

        # get ref namespaces
        path = ref.withCopyNumber()
        asset = context_info.ContextPathInfo(path=path)
        asset_name = asset.name
        new_namespaces = get_available_namespace(name=asset_name, n=numCopy)

        # get parent
        topNode = pm.PyNode(pipeTools.getTopNodeInRef(ref=path))
        topNodeParent = topNode.getParent()

        # get shade
        old_geos = pipeTools.getAllVisibleGeoInRef(path)
        shadeFilePath, shadeNamespace, geoNameSpace = shade_utils.get_assigned_ref_shade(old_geos[0])
        shadeFileName = os.path.basename(shadeFilePath)
        shadeKey = shadeFileName.split('.')[0].split('_mtr_')[-1] # get rid of .hero

        success_edits = []
        for i, new_ns in enumerate(new_namespaces):
            # create ref
            new_ref = pm.createReference(ref.path, namespace=new_ns)
            new_ns = new_ref.namespace
            new_path = new_ref.withCopyNumber()
            new_nss[old_ns] = new_ns

            # parent to the same parent as the source ref
            refTopNode = pm.PyNode(pipeTools.getTopNodeInRef(ref=new_path))
            if topNodeParent:
                pm.parent(refTopNode, topNodeParent)

            # apply edits
            new_edits = []
            if success_edits:  # reuse edit from last time so we only evaluate on edits that succeeded
                set_attr_edits = success_edits
            for ed in set_attr_edits:
                splits = ed.split(' ')
                obj = splits[1]
                obj = misc.replaceDagPathNamespace(obj, new_ns)
                splits[1] = obj
                new_edits.append(' '.join(splits))
            if new_edits:
                for ed in new_edits:
                    try:
                        mel.eval(ed)
                        success_edits.append(ed)
                    except:
                        pass

            # apply shader
            # connect shade Connect for stroke
            geoGrp = '%s:%s' % (new_ns, asset.projectInfo.asset.geo_grp())
            connectionInfo = []
            if mc.objExists('%s.shdConnect' %geoGrp):
                connectionStr = mc.getAttr('%s.shdConnect' %geoGrp)
                connectionData = eval(connectionStr)
                if connectionData and shadeKey in connectionData:
                    connectionInfo = connectionData[shadeKey]
            shade_utils.apply_ref_shade(shadeFilePath, shadeNamespace, new_ns, connectionInfo)   

            # reevaluate network
            pm.dgdirty(refTopNode)    

    if new_nss:
        to_sel = []
        for old, new in new_nss.iteritems():
            for s in sels:
                old_ns = s.namespace()[:-1]
                if old_ns == old:
                    new_obj = misc.replaceDagPathNamespace(s.shortName(), new)
                    if pm.objExists(new_obj):
                        to_sel.append(pm.PyNode(new_obj))
        if to_sel:
            pm.select(to_sel)

    return new_nss

def duplicate_asset_with_pose():
    sels = pm.selected()
    if not sels:
        return
    # pre detect number of refs
    refs = pipeTools.getFileRefFromObjects(sels)

    # popup for user input
    dialog = QtWidgets.QInputDialog(maya_win.getMayaWindow())
    dialog.setInputMode(QtWidgets.QInputDialog.IntInput) 
    dialog.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
    dialog.setWindowTitle('Duplicate with pose')
    dialog.setLabelText('Found {} asset(s) selected:\n  -{}\n\nHow many to duplicate?'.format(len(refs), '\n  -'.join([r.namespace for r in refs])))
    dialog.setIntValue(1)
    dialog.setIntMinimum(1)
    dialog.setIntMaximum(999)
    dialog.setIntStep(1)
    dialog.resize(235, 225)  
    ok = dialog.exec_()                                
    numCopy = dialog.intValue()
    if numCopy and ok:
        # make duplication
        new_nss = []
        with maya_utils.FreezeViewport():
            new_nss = duplicate_ref_with_setAttr(sels=sels, numCopy=numCopy)
        # register to SG
        if new_nss:
            scene = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
            set_asset_list(scene=scene, shotName=scene.name_code)

def set_asset_list(scene, shotName, **kwargs):
    scene = scene.copy()
    scene.context.update(entityCode=shotName, entity='none')
    scene.update_scene_context()
    shotEntity = sg_process.get_shot_entity(scene.project, scene.name)
    assetDict = pipeline_utils.collect_assets(scene)
    setDict = asm_lib.collect_set_description(scene)
    setdressDict = asm_lib.collect_asset_description(scene)
    shotdressDict = asm_lib.collect_shotdress_description(scene)
    entities = combine_dict(assetDict, setDict)
    setDressEntities = combine_dict(shotdressDict, setdressDict)
    sg_process.update_asset_list(shotEntity.get('id'), entities, sg_field='assets')
    sg_process.update_asset_list(shotEntity.get('id'), setDressEntities, sg_field='sg_setdresses')

def combine_dict(dict1, dict2):
    sgEntities = []
    combDict = OrderedDict()
    for k, v in dict1.iteritems():
        if not k in combDict.keys():
            combDict[k] = v

    for k, v in dict2.iteritems():
        if not k in combDict.keys():
            combDict[k] = v

    for k, v in combDict.iteritems():
        if v.get('id'):
            entity = {'type': 'Asset', 'id': v.get('id'), 'code': v.get('name'), 'project': v.get('project')}
            if not entity in sgEntities:
                sgEntities.append(entity)
    return sgEntities
'''
from rf_maya.rftool.layout import duplicate_asset
reload(duplicate_asset)
duplicate_asset.duplicate_asset_with_pose()
'''