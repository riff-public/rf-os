import sys 
import os 
from rftool.fix import sg_tmp
from rftool.utils import path_info 
from rf_utils import file_utils
reload(sg_tmp)

def send_to_review(workspaceFile, mediaFile, user=None): 
    shot = path_info.PathInfo(path=workspaceFile)
    status = 'rev'
    version = shot.version
    task = shot.step
    sg_tmp.send_shot_review(shot.project, shot.episode, shot.sequence, shot.name, version, task, status, mediaFile, thumbnailFile='', user=user, step=task)



def publish_mov(workspaceFile): 
    ext = '.mov'
    shot = path_info.PathInfo(path=workspaceFile)
    publishDir = shot.publishPath(publish='mov')
    publishName = shot.publishName(version=shot.version)
    path = '{0}/{1}{2}'.format(publishDir, publishName, ext)
    return path    

# example 
# from rftool.layout.review import db_hook
# reload(db_hook)
# workspaceFile = mc.file(q=True, sn=True)
# playblastDstFile = publish_mov(workspaceFile) 
# playblast and save to playblastDstFile
# db_hook.send_to_review(workspaceFile, playblastDstFile)

# mediaFile = 'P:/Two_Heroes/scene/film001/q0370/s0010/layout/publishMov/th_film001_q0370_s0010_v003.mov'