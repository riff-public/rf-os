#Ratio:
# 1 = 16:9 FHD (1920x1080)
# 2 = 2.35:1 Anamorphic (1920x800)
# 3 = 16:9 HD (1280x720)
#Pipeline:
# 0 = Shotgun
# 1 = Riff's

def projList(*args):
    projList = ['Two_Heroes', 'pucca', 'ThePool', 'SCG', 'NaKee', 'CloudMaker']
    return projList

def projDataBase(filePath, *args):
    currentProj = filePath.split('/')[1]
    if currentProj == 'Two_Heroes':
        ratio    = 2
        pipeline = 0
        daily    = 'P:/Doublemonkeys/all/daily'
        sgUrl    = 'https://riffanimation.shotgunstudio.com/page/project_overview?project_id=167'
        overScan = 1
    elif currentProj == 'pucca':
        ratio    = 3
        pipeline = 1
        daily    = 'P:/pucca/all/daily'
        sgUrl    = 'none'
        overScan = 1.3
    elif currentProj == 'ThePool':
        ratio    = 1
        pipeline = 1
        daily    = 'P:/ThePool/all/daily'
        sgUrl    = 'none'
        overScan = 1
    elif currentProj == 'SCG':
        ratio    = 1
        pipeline = 1
        daily    = 'P:/SCG/daily'
        sgUrl    = 'none'
        overScan = 1
    elif currentProj == 'NaKee':
        ratio    = 1
        pipeline = 1
        daily    = 'P:/NaKee/Daily'
        sgUrl    = 'none'
        overScan = 1
    elif currentProj == 'CloudMaker':
        ratio    = 2
        pipeline = 0
        daily    = 'P:/CloudMaker/Daily'
        sgUrl    = 'https://riffanimation.shotgunstudio.com/page/project_overview?project_id=203'
        overScan = 1
    return ratio, pipeline, daily, sgUrl, overScan

def userDataBase(*args):
    userTeam = {
        '[Team] Mick':['mick', 'mark', 'khao', 'obba'],
        '[Team] Ray' :['ray', 'pai', 'joe'],
        '[Team] Pong':['pong', 'kit', 'guide', 'Q'],
        '[Team] Wor' :['wor', 'mac', 'chris'],
        '[Team] Nair':['nair', 'prim', 'mhee', 'aoei'],
        '[Team] Tot' :['tot', 'kid', 'ton'],
        '[Team] Ping':['ping', 'drive', 'mha'],
    }   
    return userTeam
