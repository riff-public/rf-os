import pymel.core as pm
import maya.cmds as cmds
import maya.mel as mm
from collections import OrderedDict
import os
from rf_maya.lib import camera_lib
import xml.dom.minidom


class layoutStarterMain():
    
    # CONSTRUCTOR
    def __init__(self):
        self._numOfCam = 1
        self._searchObj = 'camshot'
        self._startNum = 1
        self._prefix = 's'
        self._suffix = '_cam'
        self._startShot = 1
        self._offsetFrame = 100
        self._shotWidth = 1920
        self._shotHeight = 1080
        
    # INTERFACE
    def UI(self):
        if pm.window('layoutStarter', exists = True): 
            pm.deleteUI('layoutStarter')
            
        layoutStarterWin = pm.window('layoutStarter', title = 'Layout Starter v0.2', rtf = True, sizeable = True, mxb = False)
        main = pm.columnLayout(adjustableColumn = True, p = layoutStarterWin)
        
        initScene = pm.frameLayout(borderStyle = 'out',
                                   label = 'Preparing Scene', 
                                   marginHeight = 5, 
                                   marginWidth = 5,
                                   width = 250, 
                                   parent = main)
        pm.button(label = 'Import Shots from XML', parent = initScene, command = self.importXMLProc)
        grid0 = pm.rowColumnLayout(numberOfColumns = 3, 
                                   rowSpacing = (1,5), 
                                   columnWidth = [(1,70),(2,90),(3,80)], 
                                   columnAttach = [(1,'both',5),(2,'both',5),(3,'both',5)],
                                   parent = initScene)
        pm.text(label = 'Cameras: ', align = 'right', parent = grid0)
        self._numOfCam = pm.intField(value = 1, parent = grid0)
        pm.button(label = 'Create', parent = grid0, command = self.importCamProc)
        pm.button(label = 'Remove Namespace', parent = initScene, command = self.removeNamespaceProc)
        
        ObjNaming = pm.frameLayout(borderStyle = 'out',
                                   label = 'Camera Manage', 
                                   marginHeight = 5, 
                                   marginWidth = 5,
                                   width = 250, 
                                   parent = main)
        grid1 = pm.rowColumnLayout(numberOfColumns = 3, 
                                   rowSpacing = (1,5), 
                                   columnWidth = [(1,70),(2,90),(3,80)], 
                                   columnAttach = [(1,'both',5),(2,'both',5),(3,'both',5)],
                                   parent = ObjNaming)
        pm.text(label = 'Search:', al = 'right', parent = grid1)
        self._searchObj = pm.textField(text = 'camshot', parent = grid1)
        pm.button(label = 'Select', parent = grid1, command = self.searchObjProc)
        pm.text(l='Start Num:', al = 'right', parent = grid1)
        self._startNum = pm.intField(v = 1, parent = grid1)
        self._stepNum = pm.optionMenu(parent = grid1)
        pm.menuItem(l = 'x10')
        pm.menuItem(l = 'x1')       
        pm.text(l='Prefix:', al = 'right', parent = grid1)
        self._prefix = pm.textField(text = 's', parent = grid1)
        pm.text(l=' ', parent = grid1)
        pm.text(l='Suffix:', al = 'right', parent = grid1)
        self._suffix = pm.textField(text = '_cam', parent = grid1)
        pm.button(l='Rename', parent = grid1, c = self.objNamProc)
        
        pm.text(label = ' Camera clip plane', align = 'left', font = 'boldLabelFont', parent = ObjNaming)
        
        grid11 = pm.rowColumnLayout(numberOfColumns = 3, 
                                   rowSpacing = (1,5), 
                                   columnWidth = [(1,70),(2,90),(3,80)], 
                                   columnAttach = [(1,'both',5),(2,'both',5),(3,'both',5)],
                                   parent = ObjNaming)
        
        pm.text(l='Near clip:', al = 'right', parent = grid11)
        self._nearClip = pm.intField(value = 1, parent = grid11)
        pm.text(l=' ', parent = grid11)
        pm.text(l='Far clip:', al = 'right', parent = grid11)
        self._farClip = pm.intField(value = 1000000, parent = grid11)
        pm.button(l='Set', parent = grid11, c = self.setCamClipPlane)
                
        ShotNaming = pm.frameLayout(borderStyle = 'out',
                                    label = 'Shot Manage', 
                                    marginHeight = 5, 
                                    marginWidth = 5,
                                    width = 250, 
                                    parent = main)
        grid2 = pm.rowColumnLayout(numberOfColumns = 3, 
                                   rowSpacing = (1,5), 
                                   columnWidth = [(1,70),(2,90),(3,80)], 
                                   columnAttach = [(1,'both',5),(2,'both',5),(3,'both',5)], 
                                   parent = ShotNaming)
        pm.text(label = 'Start Shot:', align = 'right', parent = grid2)
        self._startShot = pm.intField(value = 1, parent = grid2)
        pm.text(l = '', parent = grid2)
        pm.text(l = 'Step:', align = 'right', parent = grid2)
        self._stepShot = pm.optionMenu(parent = grid2)
        pm.menuItem(l = 'x10')
        pm.menuItem(l = 'x1')  
        pm.button(label = 'Rename', parent = grid2, command = self.shotNamingProc)
        pm.text(label = 'Offset:', align = 'right', parent = grid2)
        self._offsetFrame = pm.intField(value = 100, parent = grid2)
        pm.button(label = 'Offset', parent = grid2, command = self.shotOffsetProc)
        
        pm.text(label = ' Shot resolution', align = 'left', font = 'boldLabelFont', parent = ShotNaming)
        
        grid3 = pm.rowColumnLayout(numberOfColumns = 3, 
                                   rowSpacing = (1,5), 
                                   columnWidth = [(1,70),(2,90),(3,80)], 
                                   columnAttach = [(1,'both',5),(2,'both',5),(3,'both',5)], 
                                   parent = ShotNaming)
        pm.text(label = 'Width:', align = 'right', parent = grid3)
        self._shotWidth = pm.intField(value = 1920, parent = grid3)
        pm.text(label = ' ', parent = grid3)
        pm.text(label = 'Height:', align = 'right', parent = grid3)
        self._shotHeight = pm.intField(value = 1080, parent = grid3)
        pm.button(label = 'Set', parent = grid3, command = self.shotResProc)
        
        pm.button(label = 'Assign All Cams to Shots', parent = ShotNaming, command = self.assignCamProc)
        pm.text(label = 'Cameras will be selected by Camera Naming settings', enable = False, ww = True, parent = ShotNaming)
        pm.separator(style = 'none', height = 5, parent = ShotNaming)
                 
        pm.showWindow(layoutStarterWin)

    def load_xml(self, file_path):
        data_dict = OrderedDict()
        DOMTree = xml.dom.minidom.parse(file_path)
        collection = DOMTree.documentElement
        clipitems = collection.getElementsByTagName("clipitem")
        for clip in clipitems:
            clip_name = clip.getElementsByTagName('name')[0]
            duration = clip.getElementsByTagName('duration')[0]
            cut_in = clip.getElementsByTagName('in')[0]
            cut_out = clip.getElementsByTagName('out')[0]
            start = clip.getElementsByTagName('start')[0]
            end = clip.getElementsByTagName('end')[0]

            duration = duration.childNodes[0].data
            working_duration = int(cut_out.childNodes[0].data) - int(cut_in.childNodes[0].data)
            cut_in_shot = str(int(cut_in.childNodes[0].data) + 1)
            cut_out_shot = cut_out.childNodes[0].data

            FIX_START_SHOT = 101
            
            print clip_name.childNodes[0].data
            data_dict[clip_name.childNodes[0].data] = working_duration
        return data_dict
        
    # IMPORT XML
    def importXMLProc(self, *args, **kwargs):
        filename = cmds.fileDialog2(fileMode=1, caption="Import Seq")[0]
        data_xml_dict = self.load_xml(filename)
        self.create_shot(data_xml_dict)
        # mm.eval('performImportEDL 0;')
        # pm.select (cl = True)
        
    # IMPORT CAMERAS
    def importCamProc(self, *args, **kwargs):
        _numOfCam = pm.intField(self._numOfCam, query = True, value = True)
        
        for i in range(_numOfCam):
            cmds.file('P:/layoutLibrary/all/asset/tool/camera/layoutCamRig/hero/layoutCamRig_1.01_med.ma', i = True, dns = True)
    
    # REMOVE NAMESPACE
    def removeNamespaceProc(self, *args, **kwargs):
        obj_ls = pm.ls ( sl = True )
        num = 0
        
        for i in obj_ls:
            refprefix = obj_ls[num].split(':')[0]
            pm.namespace( mv = [ refprefix ,":" ] ,force = True )
            num += 1
            
    # SEARCH AND SELECT OBJECT
    def searchObjProc(self, *args, **kwargs):
        _searchObj = pm.textField(self._searchObj, query = True, text = True)
        listed = pm.ls('%s' %_searchObj)
        pm.select(listed)
    
    # SERIES OBJECT NAMING
    def objNamProc(self, *args, **kwargs):
        _prefix = pm.textField(self._prefix, query = True, text = True)
        _startNum = pm.intField(self._startNum, query = True, value = True)
        _suffix = pm.textField(self._suffix, query = True, text = True)
        _stepNum = pm.optionMenu(self._stepNum, query = True, v = True)
        obj_ls = pm.ls(sl = True)
        if _stepNum == 'x10':
            # prevent same name
            for i in obj_ls:
                pm.rename(i, "null%03d0" %_startNum)
                _startNum += 1
            
            # start rename
            _startNum = pm.intField(self._startNum, query = True, value = True)            
            for i in obj_ls:
                pm.rename(i, "%s%03d0%s" %(_prefix, _startNum, _suffix))
                _startNum += 1
        elif _stepNum == 'x1':
            for i in obj_ls:
                pm.rename(i, "null%04d" %_startNum)
                _startNum += 1
                
            _startNum = pm.intField(self._startNum, query = True, value = True) 
            for i in obj_ls:
                pm.rename(i, "%s%04d%s" %(_prefix, _startNum, _suffix))
                _startNum += 1
            
    # SET CAMERA CLIP PLANE
    def setCamClipPlane(self, *args, **kwargs):
        _nearClip = pm.intField(self._nearClip, query = True, value = True)
        _farClip = pm.intField(self._farClip, query = True, value = True)
        obj_ls = pm.ls(sl = True)
        for i in obj_ls:
            pm.setAttr(i.nearClipPlane, _nearClip)
            pm.setAttr(i.farClipPlane, _farClip)
    
    # SHOT MANAGER -> SERIES SHOT NAMING
    def shotNamingProc(self, *args, **kwargs):
        _startShot = pm.intField(self._startShot, query = True, value = True)
        _stepShot = pm.optionMenu(self._stepShot, query = True, v = True)
        shotList = pm.ls(sl = True)
        num = 0
        
        if _stepShot == 'x10':
            # prevent same name
            for i in shotList:
                shotList[num].setShotName("null%03d0" %_startShot)
                shotList[num].setName("null%03d0" %_startShot)
                num += 1
                _startShot += 1
            
            # start rename
            _startShot = pm.intField(self._startShot, query = True, value = True)
            num = 0
            for i in shotList:
                shotList[num].setShotName("s%03d0" %_startShot)
                shotList[num].setName("s%03d0" %_startShot)
                num += 1
                _startShot += 1
                
        elif _stepShot == 'x1':
            for i in shotList:
                shotList[num].setShotName("null%04d" %_startShot)
                shotList[num].setName("null%04d" %_startShot)
                num += 1
                _startShot += 1
            
            _startShot = pm.intField(self._startShot, query = True, value = True)
            num = 0
            for i in shotList:
                shotList[num].setShotName("s%04d" %_startShot)
                shotList[num].setName("s%04d" %_startShot)
                num += 1
                _startShot += 1
        
    # SHOT MANAGER -> SHOT OFFSET
    def shotOffsetProc(self, *args, **kwargs):
        _offsetFrame = pm.intField(self._offsetFrame, query = True, value = True)
        shotList = pm.ls(sl = True)
        num = 0

        for i in shotList:
            startFrame = pm.getAttr(i.startFrame)
            endFrame = pm.getAttr(i.endFrame)
            shotList[num].setStartTime(startFrame + _offsetFrame)
            shotList[num].setEndTime(endFrame + _offsetFrame)
            num += 1
            _offsetFrame += 100
            
    # SHOT MANAGER -> SHOT RESOLUTION
    def shotResProc(self, *args, **kwargs):
        _shotWidth = pm.intField(self._shotWidth, query = True, value = True)
        _shotHeight = pm.intField(self._shotHeight, query = True, value = True)
        shotList = pm.ls(sl = True)
        num = 0
        
        for i in shotList:
            print i
            print shotList[num]
            pm.setAttr(i.wResolution, _shotWidth)
            pm.setAttr(i.hResolution, _shotHeight)
            num += 1
    
    # ASSIGN CAMERA        
    def assignCamProc(self, *args, **kwargs):
        _startNum = pm.intField(self._startNum, query = True, value = True)
        _prefix = pm.textField(self._prefix, query = True, text = True)
        _suffix = pm.textField(self._suffix, query = True, text = True)
        shotList = pm.ls(sl = True)
        num = 0
        
        for i in shotList:
            shotList[num].setCurrentCamera("%s%03d0%s" %(_prefix, _startNum, _suffix))
            num += 1
            _startNum += 1


    def import_cam(self):
        import_item = cmds.file('P:/layoutLibrary/all/asset/tool/camera/layoutCamRig/hero/layoutCamRig_1.01_med.ma', i = True, dns = True, rnn=True)
        cam = cmds.ls(import_item, type='camera')[0]
        return cam

    def create_shot(self, shot_data):
        DEFAULT_RANGE= 0
        DEFAULT_DURATION_SHOT = 101
        chk = cmds.confirmDialog( title='create_sequencer_shots', message='create_sequencer_shots', button=['Yes','No'], defaultButton='Yes', cancelButton='No', dismissString='No',messageAlign= 'center' )
        if chk == 'Yes':
            start = True
            start_seq_time = ''
            cam_grp = cmds.group(em=True,n='cam_grp')
            for shotCode, duartion in shot_data.iteritems():
                print shotCode, 'test'
                if start:
                    start_time = 1 + DEFAULT_RANGE 
                    start = False
                cameraShape = self.import_cam()
                cam_shot = cmds.listRelatives(cameraShape,p=True)[0]
                cam_shot_grp = cmds.listRelatives(cam_shot,p=True)[0]
                cam_ = cmds.rename(cam_shot, '%s_cam'%shotCode)
                camshot_grp = cmds.rename(cam_shot_grp, '%s_grp'%shotCode)
                end_time = start_time + duartion - 1
                ####create shot 
                cam_seq_shot = camera_lib.create_shot(shotCode, cam_, start_time, end_time)
                cmds.parent (camshot_grp, cam_grp)
                print start_time,end_time
                if start_seq_time:
                    cmds.setAttr('%s.sequenceStartFrame'% shotCode, start_seq_time)
                    cmds.setAttr('%s.scale'% shotCode, 1)
                end_seq_time = cmds.getAttr('%s.sequenceEndFrame'% shotCode)

                #create sound 
                # if os.path.exists(soundPath):
                #     sound_name ='%s_sound' %shotCode
                #     sound_tranform = cmds.sound(file = soundPath, n = sound_name, offset = start_time)
                #     cmds.sequenceManager(ata=sound_tranform)
                #     cmds.setAttr('%s.sourceStart'%sound_tranform, 0)
                #     cmds.setAttr('%s.sourceEnd'%sound_tranform, shot['sg_working_duration'])
                #     if start_seq_time:
                #         cmds.setAttr('%s.offset'%sound_tranform, start_seq_time)
                #     else:
                #         cmds.setAttr('%s.offset'%sound_tranform, start_time)
                
                    # cmds.shot(shotCode, e=True, mute=True)
                start_time = end_time + DEFAULT_DURATION_SHOT
                start_seq_time = end_seq_time + 1