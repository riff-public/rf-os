import pymel.core as pm
import maya.cmds as cmd
import maya.mel as mm
import os
import subprocess
from rftool.layout.projDataBase import projDataBase
#from rftool.layout.getOutput import getOutput

rv        = "C:\\Program Files\\Shotgun\\RV-7.2.1\\bin\\rv.exe"
quickTime = "C:\\Program Files (x86)\\QuickTime\\QuickTimePlayer.exe"
mpc       = "C:\\Program Files (x86)\\K-Lite Codec Pack\\MPC-HC64\\mpc-hc64.exe"

projList  = projDataBase.projList()

def UI():
    if pm.window('checkupWin', exists = True): 
        pm.deleteUI('checkupWin') 
            
    pm.window('checkupWin', title = 'Checkup v0.2', ret = True, rtf = True, sizeable = True, mxb = False)
    main = pm.columnLayout(adjustableColumn = True, rs = 5, cat = ('both',5))
    pm.separator(style = 'none', h = 2, p = main)
    
    # player
    playerRow = pm.rowLayout(cw = (1,80), cal = (1, 'left'),nc = 2, adj = 2, cat = [(1,'both',5),(2,'both',5),(3,'both',5)], p = main)
    pm.text(l = 'Select player: ', p = playerRow)
    pm.optionMenu('playerSel', p = playerRow)
    pm.menuItem('rv', l = 'RV')
    pm.menuItem('quickTime', l = 'QuickTime')
    pm.menuItem('mpc', l = 'Media Player Classic')
    
    fileListRow = pm.rowLayout(nc = 5, cat = [(1,'both',5),(2,'both',5),(3,'both',5),(5,'both',5)], p = main)
    
    # project - episode
    projCol = pm.columnLayout(rs = 5, p = fileListRow)
    pm.optionMenuGrp('department', l = 'Department: ', cw = (1,65), cal = (1,'left'), p = projCol, cc = listSeq)
    pm.menuItem(l = 'layout')
    pm.menuItem(l = 'anim')
    pm.menuItem(l = 'finalCam')
    pm.text(l = 'Project')
    pm.textScrollList('projListsTS', h = 150, w = 140, p = projCol, append = projList, sc = listEp)
    pm.separator(style = 'none', h = 5, p = projCol)
    pm.text(l = 'Episode')
    pm.textScrollList('epListsTS', h = 150, w = 140, p = projCol, sc = listSeq)
    pm.separator(style = 'none', h = 35, p = projCol)
    
    # sequence
    seqListCol = pm.columnLayout(rs = 5, p = fileListRow)
    pm.optionMenu('folderSel', p = seqListCol)
    pm.menuItem(l = 'Project Sequence')
    pm.menuItem(l = 'Daily')
        
    pm.textScrollList('seqListsTS', w = 113, h = 350, p = seqListCol, sc = listPubShot)
    pm.separator(style = 'none', h = 35, p = seqListCol)
    
    # publish
    pubFileListCol = pm.columnLayout(rs = 5, p = fileListRow)
    pm.text(l = 'Publish shots file: ', al = 'left', w = 165, p = pubFileListCol)
    pm.textScrollList('pubListsTS', ams = True, w = 268, h = 350, p = pubFileListCol, sc = listVerShot)
    pubBtnRow = pm.rowLayout(nc = 2, p = pubFileListCol)
    pm.button(l = 'Play', w = 132, p = pubBtnRow, c = playPubButton)
    pm.button(l = 'Browse', w = 132, p = pubBtnRow, c = runPubExplorer)
    
    pm.text(l = '>', p = fileListRow)
    
    # version
    verFileListCol = pm.columnLayout(rs = 5, p = fileListRow)
    pm.text(l = 'Version files: ', al = 'left', w = 165, p = verFileListCol)
    pm.textScrollList('verListsTS', w = 268, h = 350, p = verFileListCol)
    verBtnRow = pm.rowLayout(nc = 2, p = verFileListCol)
    pm.button(l = 'Play', w = 132, p = verBtnRow, c = playVerButton)
    pm.button(l = 'Browse', w = 132, p = verBtnRow, c = runVerExplorer)
    
    pm.showWindow('checkupWin')
    
    #listProj()

#====================== Listing UI functions =========================
    
def listEp(*args):
    selProj  = pm.textScrollList('projListsTS', q = True, selectItem = True)[0]
    pipeline = projDataBase.projDataBase('P:/'+selProj) [1]

    if pipeline == 0:
        epList = pm.getFileList(folder = 'P:/%s/scene' %selProj)
    elif pipeline == 1:
        epList = pm.getFileList(folder = 'P:/%s' %selProj)
    
    pm.textScrollList('epListsTS',  e = True, ra = True)
    pm.textScrollList('seqListsTS', e = True, ra = True)
    pm.textScrollList('pubListsTS', e = True, ra = True)
    pm.textScrollList('verListsTS', e = True, ra = True)        
    epList.sort(key = lambda k : k.lower())            
    pm.textScrollList('epListsTS',  e = True, append = epList)

def listSeq(*args):
    try:
        selProj  = pm.textScrollList('projListsTS', q = True, selectItem = True)[0]
        selEp    = pm.textScrollList('epListsTS',   q = True, selectItem = True)[0]
        selDep   = pm.optionMenuGrp ('department',  q = True, v = True)
        pipeline = projDataBase.projDataBase('P:/'+selProj) [1]
                
        if pipeline == 0:
            seqList = pm.getFileList(folder = 'P:/%s/scene/%s' %(selProj,selEp))
        elif pipeline == 1:
            seqList = pm.getFileList(folder = 'P:/%s/%s/scene/%s' %(selProj,selEp,selDep))
    
        pm.textScrollList('seqListsTS', e = True, ra = True)
        pm.textScrollList('pubListsTS', e = True, ra = True)
        pm.textScrollList('verListsTS', e = True, ra = True)  
        seqList.sort(key = lambda k : k.lower())
        pm.textScrollList('seqListsTS', e = True, append = seqList)
    except:
        pass
    
def listPubShot(*args):
    selProj  = pm.textScrollList('projListsTS', q = True, selectItem = True)[0]
    selEp    = pm.textScrollList('epListsTS',   q = True, selectItem = True)[0]
    selDep   = pm.optionMenuGrp ('department',  q = True, v = True)
    seqItem  = pm.textScrollList('seqListsTS',  q = True, selectItem = True)[0]
    pipeline = projDataBase.projDataBase('P:/'+selProj) [1]

    if pipeline == 0:
        pubList = pm.getFileList(folder = 'P:/%s/scene/%s/%s/playblast/%s' %(selProj,selEp,seqItem,selDep))
    elif pipeline == 1:
        pubList = pm.getFileList(folder = 'P:/%s/%s/scene/%s/preview'      %(selProj,selEp,selDep))

    pm.textScrollList('pubListsTS', e = True, ra = True)
    pm.textScrollList('verListsTS', e = True, ra = True)
    if not pubList:
        pm.textScrollList('pubListsTS', e = True, ra = True)
    else:
        pubList.sort(key = lambda k : k.lower())
        pm.textScrollList('pubListsTS', e = True, append = pubList)
        
def listVerShot(*args):
    selProj  = pm.textScrollList('projListsTS', q = True, selectItem = True)[0]
    selEp    = pm.textScrollList('epListsTS',   q = True, selectItem = True)[0]
    selDep   = pm.optionMenuGrp ('department',  q = True, v = True)
    seqItem  = pm.textScrollList('seqListsTS',  q = True, selectItem = True)[0]        
    pubItem  = pm.textScrollList('pubListsTS',  q = True, selectItem = True)[0]
    pipeline = projDataBase.projDataBase('P:/'+selProj) [1]

    if pipeline == 0:
        shotName = pubItem.split('_')[3]       #proj   #ep #q #s #dep
        verList  = pm.getFileList(folder = 'P:/%s/scene/%s/%s/%s/%s/maya/playblast' %(selProj,selEp,seqItem,shotName,selDep))
    elif pipeline == 1:
        shotName = pubItem.split('_')[0]      #pr #ep      #dep #s
        verList  = pm.getFileList(folder = 'P:/%s/%s/scene/%s/%s/output/preview'    %(selProj,selEp,selDep,shotName))
    
    pm.textScrollList('verListsTS', e = True, ra = True)
    verList.sort(key = lambda k : k.lower())
    verList.reverse()
    pm.textScrollList('verListsTS', e = True, append = verList) 

#====================== System functions =========================
   
def playerSelector(*args):
    playerLabel = pm.optionMenu('playerSel', q = True, sl = True)    
    if playerLabel == 1:
        player = eval('rv')
    elif playerLabel == 2:
        player = eval('quickTime')
    elif playerLabel == 3:
        player = eval('mpc')       
    return player

def getPath(*args):
    selProj  = pm.textScrollList('projListsTS', q = True, selectItem = True)[0]
    selEp    = pm.textScrollList('epListsTS',   q = True, selectItem = True)[0]
    selDep   = pm.optionMenuGrp ('department',  q = True, v = True)
    seqItem  = pm.textScrollList('seqListsTS',  q = True, selectItem = True)[0]
    selItem  = pm.textScrollList('pubListsTS',  q = True, selectItem = True)
    pipeline = projDataBase.projDataBase('P:/'+selProj) [1]
    player = playerSelector()
    files = [player]        
     
def playPubButton(*args):
    selProj  = pm.textScrollList('projListsTS', q = True, selectItem = True)[0]
    selEp    = pm.textScrollList('epListsTS',   q = True, selectItem = True)[0]
    selDep   = pm.optionMenuGrp ('department',  q = True, v = True)
    seqItem  = pm.textScrollList('seqListsTS',  q = True, selectItem = True)[0]
    selItem  = pm.textScrollList('pubListsTS',  q = True, selectItem = True)
    pipeline = projDataBase.projDataBase('P:/'+selProj) [1]
    player = playerSelector()
    files = [player]        
    if pipeline == 0:
        filePathPubTxt = 'P:/%s/scene/%s/%s/playblast/%s' %(selProj,selEp,seqItem,selDep)
    elif pipeline == 1:
        filePathPubTxt = 'P:/%s/%s/scene/%s/preview'      %(selProj,selEp,selDep)
    if selItem:
        for i in selItem:
            files.append(filePathPubTxt + "\\" + i)
        subprocess.Popen(files)
    else:
        mc.warning('No Select List')
        
def playVerButton(*args):
    selProj     = pm.textScrollList('projListsTS', q = True, selectItem = True)[0]
    selEp       = pm.textScrollList('epListsTS',   q = True, selectItem = True)[0]
    seqItem     = pm.textScrollList('seqListsTS',  q = True, selectItem = True)[0]
    verItem     = pm.textScrollList('verListsTS',  q = True, selectItem = True)[0]
    selDep      = pm.optionMenuGrp ('department',  q = True, v = True)
    pipeline    = projDataBase.projDataBase('P:/'+selProj) [1]
    player      = playerSelector()
    if pipeline == 0:
        shotName = verItem.split('_')[3]
        filePathTxt = 'P:/%s/scene/%s/%s/%s/%s/maya/playblast' %(selProj, selEp, seqItem, shotName, selDep) 
    elif pipeline == 1:
        shotName = verItem.split('_')[0]
        filePathTxt = 'P:/%s/%s/scene/%s/%s/output/preview'    %(selProj, selEp, selDep, shotName)    
    if verItem:
        subprocess.Popen([player, filePathTxt + "\\" + verItem])
    else:
        mc.warning('No Select List')
        
def runPubExplorer(*args):
    selProj     = pm.textScrollList('projListsTS', q = True, selectItem = True)[0]
    selEp       = pm.textScrollList('epListsTS',   q = True, selectItem = True)[0]
    seqItem     = pm.textScrollList('seqListsTS',  q = True, selectItem = True)[0]
    selDep      = pm.optionMenuGrp ('department',  q = True, v = True)
    pipeline    = projDataBase.projDataBase('P:/'+selProj) [1]
    player      = playerSelector()
    if pipeline == 0:
        filePathPubTxt = 'P:/%s/scene/%s/%s/playblast/%s' %(selProj,selEp,seqItem,selDep)
    elif pipeline == 1:
        filePathPubTxt = 'P:/%s/%s/scene/%s/preview'      %(selProj,selEp,selDep)    
    os.startfile(filePathPubTxt)    
    
def runVerExplorer(*args):
    selProj     = pm.textScrollList('projListsTS', q = True, selectItem = True)[0]
    selEp       = pm.textScrollList('epListsTS',   q = True, selectItem = True)[0]
    seqItem     = pm.textScrollList('seqListsTS',  q = True, selectItem = True)[0]
    selDep      = pm.optionMenuGrp ('department',  q = True, v = True)
    verItem     = pm.textScrollList('verListsTS',  q = True, selectItem = True)[0]
    pipeline    = projDataBase.projDataBase('P:/'+selProj) [1]
    player      = playerSelector()    
    if pipeline == 0:
        shotName = verItem.split('_')[3]
        filePathTxt = 'P:/%s/scene/%s/%s/%s/%s/maya/playblast' %(selProj, selEp, seqItem, shotName, selDep) 
    elif pipeline == 1:
        shotName = verItem.split('_')[0]
        filePathTxt = 'P:/%s/%s/scene/%s/%s/output/preview'    %(selProj, selEp, selDep, shotName)
    os.startfile(filePathTxt)
