import maya.cmds as mc
import maya.mel as mm
from os import startfile
from os import remove
from webbrowser import open
from datetime import datetime
from subprocess import Popen
from subprocess import call
from shutil import copy2
from rftool.layout.review import db_hook
from rftool.layout.projDataBase import projDataBase
from rftool.layout.tools import getOutput

ffmpeg    = r"O:\\Pipeline\\legacy\\lib\\ffmpeg\\bin\\ffmpeg.exe"
TMP       = r"C:\\pbTemp\\pbTemp"

rv        = r"C:\\Program Files\\Shotgun\\RV-7.2.1\\bin\\rv.exe"
quickTime = r"C:\\Program Files (x86)\\QuickTime\\QuickTimePlayer.exe"
mpc       = r"C:\\Program Files (x86)\\K-Lite Codec Pack\\MPC-HC64\\mpc-hc64.exe"

currentFilePath = mc.file(q = True, loc = True)

def UI():
    if mc.window('SGplayblastWin', exists = True):
        mc.deleteUI('SGplayblastWin')

    def useTimerangeChk(*args):
        if mc.radioButton('D', q = True, sl = True):
            mc.intField('startIntF', e = True, en = True)
            mc.intField('endIntF', e = True, en = True)
        else:
            mc.intField('startIntF', e = True, en = False)
            mc.intField('endIntF', e = True, en = False)
            
    def selSepChk(*args):
        if mc.radioButton('E', q = True, sl = True):
            mc.checkBox('viewChk', e = True, en = False)
            mc.checkBox('ovrwChk', e = True, en = False)
        else:
            mc.checkBox('viewChk', e = True, en = True)
            mc.checkBox('ovrwChk', e = True, en = True)
        
    mc.window('SGplayblastWin', title = 'Friday Playblast for Shotgun v2.0', rtf = True, mxb = False)
    main = mc.formLayout('mainLayout', nd = 100)
    scroll = mc.scrollLayout('scrollLayout', cr = True)
    
    row1 = mc.rowLayout(nc = 3, cat = [(1,'both',5),(2,'both',5),(3,'both',5)], rat =(1,'both',8), adj = 2, p = scroll)
    mc.text(l = 'Select Player:', p = row1)
    mc.optionMenu('playerSel', p = row1)
    mc.menuItem('rv', l = 'RV')
    mc.menuItem('quickTime', l = 'QuickTime')
    mc.menuItem('mpc', l = 'Media Player Classic')
    mc.button(l = 'Refresh', w = 80, c = refreshList)
        
    frame1 = mc.frameLayout('frame1', l = 'Publish Files', bs = 'out', cll = True, cl = True, mh = 5, mw = 5, p = scroll) #################### collapse = True
    mc.textScrollList('pubListsTS', w = 268, h = 200, ams = True, p = frame1)
    row11 = mc.rowLayout('row11', nc = 3, adj = 1, p = frame1)
    mc.button(l = 'Play', w = 110, p = row11, c = playPubButton)
    mc.button(l = 'Insert to Shot', w = 110, p = row11, c = insPubToShot)
    mc.button(l = 'Browse', w = 110, p = row11, c = runPubExplorer)
            
    frame2 = mc.frameLayout('frame2', l = 'Version Files', bs = 'out', cll = True, mh = 5, mw = 5, p = scroll) #################### collapse = False
    mc.textScrollList('verListsTS', w = 268, h = 200, p = frame2)
    row21 = mc.rowLayout('row21', nc = 3, adj = 1, p = frame2)
    mc.button(l = 'Play', w = 110, p = row21, c = playVerButton)
    mc.button(l = 'Delete', w = 110, p = row21, c = deleteVerButton)
    mc.button(l = 'Browse', w = 110, p = row21, c = runVerExplorer)
            
    frame3 = mc.frameLayout('frame3', l = 'Settings', bs = 'out', mh = 5, mw = 5, p = scroll)  #################### collapse = False
    col31 = mc.columnLayout(adj = True, rs = 5, cat =('both',10), p = frame3)
    grid31 = mc.rowColumnLayout(nc = 3, cs = [(3,5),(3,5)], cw = [(1,60),(2,80)], p = col31)
    mc.text(l = 'Play mode: ', p = grid31)
    mc.radioCollection('pbModeRdoI', p = grid31)
    mc.radioButton('A', l = 'Timeline', sl = True, p = grid31)
    mc.radioButton('C', l = 'Selected Shots', p = grid31)
    mc.text(l = ' ', p = grid31)
    mc.radioButton('B', l = 'Selection', p = grid31)
    mc.radioButton('E', l = 'Selected Shots(separately)', p = grid31, cc = selSepChk)
    mc.text(l = ' ', p = grid31)
    mc.text(l = ' ', p = grid31)
    mc.radioButton('D', l = 'Sequence Start/End', p = grid31, cc = useTimerangeChk)
    mc.separator(style = 'none', h = 5, p = grid31)
    mc.separator(style = 'none', p = grid31)
    mc.separator(style = 'none', p = grid31)
    mc.text(l = 'Time range: ', p = grid31)
    mc.intField('startIntF', w = 80, v = 1, en = False, p = grid31)
    mc.intField('endIntF', w = 80, v = 100, en = False, p = grid31)
    row33 = mc.rowLayout(nc = 2, p = col31)
    mc.text(l = 'Resolution:  ', p = row33)
    mc.optionMenu('resCombo', p = row33)
    mc.menuItem(l = '1920 x 1080 (Full HD)')
    mc.menuItem(l = '1920 x 800 (Anamorphic)')
    mc.menuItem(l = '1280 x 720 (HD)')
    mc.checkBox('viewChk', l = 'Preview', v = True, p = col31)
    mc.checkBox('ovrwChk', l = 'Overwrite latest version', p = col31)
    mc.checkBox('pubChk', l = 'Publish', p = col31)
        
    frame4 = mc.frameLayout('frame4', l = 'Utilities', bs = 'out', cll = True, cl = False, mh = 5, mw = 5, p = scroll)  #################### collapse = True
    row41 = mc.rowLayout('row41', adj = 1, nc = 2, p = frame4)
    mc.button('submitSG', l = 'Submit to Shotgun', p = row41, c = submitSG)
    mc.button('openSG', l = '...', p = row41, c = openSG)
    row42 = mc.rowLayout('row42', adj = 1, nc = 2, p = frame4)
    mc.button(l = 'Submit to Daily', p = row42, c = submitDaily)
    mc.button(l = '...', p = row42, c = browseDaily)
            
    btn = mc.button('pbBtn', l = 'Playblast', h = 40, p = main, c = doPlayblast)
    mc.formLayout( main, 
                   edit = True, 
                   attachForm = [(scroll, 'top', 5), (scroll, 'left', 5), (scroll, 'right', 5), (btn, 'left', 5), (btn, 'bottom', 5), (btn, 'right', 5)], 
                   attachControl = [(scroll, 'bottom', 5, btn)], 
                   attachNone = (btn, 'top') )
    
    mc.showWindow('SGplayblastWin')
    
    refreshList()     
    
def runVerExplorer(*args):
    pipeline = projDataBase.projDataBase(currentFilePath) [2]
    filePathVerTxt = refreshList() [0]
    filePathVerTxt = filePathVerTxt.replace('/','\\')    
    startfile(filePathVerTxt)
    
def runPubExplorer(*args):
    pipeline = projDataBase.projDataBase(currentFilePath) [2]
    filePathPubTxt = refreshList() [2]
    filePathPubTxt = filePathPubTxt.replace('/','\\')
    mc.sysFile('%s' %filePathPubTxt, makeDir = True)
    startfile(filePathPubTxt)
         
def refreshList(*args):
    try:
        mc.textScrollList('pubListsTS', e = True, en = True)
        mc.rowLayout('row11', e = True, en = True)
        mc.textScrollList('verListsTS', e = True, en = True)
        mc.rowLayout('row21', e = True, en = True)
        mc.frameLayout('frame3', e = True, en = True)
        mc.rowLayout('row41', e = True, en = True)        
        mc.rowLayout('row42', e = True, en = True)  
        mc.button('pbBtn', e = True, en = True)        
        ratio    = projDataBase.projDataBase(currentFilePath) [0]
        pipeline = projDataBase.projDataBase(currentFilePath) [1]
        mc.optionMenu('resCombo', e = True, sl = ratio)
        if pipeline == 0:
            mc.button('submitSG', e = True, en = True)
            mc.button('openSG', e = True, en = True)             
            filePathPubTxt = getOutput.getSGfileList(currentFilePath) [2]
            pbFilePubTxt   = getOutput.getSGfileList(currentFilePath) [3]
            filePathVerTxt = getOutput.getSGfileList(currentFilePath) [0]
            latestVerFile  = getOutput.getSGfileList(currentFilePath) [1]
            pubFiles = mc.getFileList(folder = filePathPubTxt, fs = '*.mov')
            verFiles = mc.getFileList(folder = filePathVerTxt, fs = '*.mov')
            mc.textScrollList('pubListsTS', e = True, ra = True)
            mc.textScrollList('verListsTS', e = True, ra = True) 
            if not pubFiles:
                pass
            else:        
                pubFiles.sort(key = lambda k : k.lower())
                mc.textScrollList('pubListsTS', e = True, append = pubFiles)
            if not verFiles:
                pass
            else:  
                verFiles.sort(key = lambda k : k.lower())
                verFiles.reverse()
                mc.textScrollList('verListsTS', e = True, append = verFiles)
            currentFileLoc = currentFilePath.split('_')
            usrFileName    = currentFileLoc[-1][:-3] # file name remove version number
            origNaming     = pbFilePubTxt.replace('.mov','') # remove .mov to add user name
            return filePathVerTxt, latestVerFile, filePathPubTxt, pbFilePubTxt, usrFileName
            
        elif pipeline == 1:
            mc.button('submitSG', e = True, en = False)
            mc.button('openSG',   e = True, en = False) 
            filePathPubTxt = getOutput.getRIFFfileList(currentFilePath) [2]
            pbFilePubTxt   = getOutput.getRIFFfileList(currentFilePath) [3]
            filePathVerTxt = getOutput.getRIFFfileList(currentFilePath) [0]
            latestVerFile  = getOutput.getRIFFfileList(currentFilePath) [1]
            pubFiles = mc.getFileList(folder = filePathPubTxt, fs = '*.mov')
            verFiles = mc.getFileList(folder = filePathVerTxt, fs = '*.mov')
            mc.textScrollList('pubListsTS', e = True, ra = True)
            mc.textScrollList('verListsTS', e = True, ra = True) 
            if not pubFiles:
                pass
            else:        
                pubFiles.sort(key = lambda k : k.lower())
                mc.textScrollList('pubListsTS', e = True, append = pubFiles)
            if not verFiles:
                pass
            else:  
                verFiles.sort(key = lambda k : k.lower())
                verFiles.reverse()
                mc.textScrollList('verListsTS', e = True, append = verFiles)  
            usrFileName = 'none'
            return filePathVerTxt, latestVerFile, filePathPubTxt, pbFilePubTxt, usrFileName
    except:
        mc.textScrollList('pubListsTS', e = True, en = False, ra = True)
        mc.rowLayout('row11', e = True, en = False)
        mc.textScrollList('verListsTS', e = True, en = False, ra = True)
        mc.rowLayout('row21', e = True, en = False)
        mc.frameLayout('frame3', e = True, en = False)
        mc.rowLayout('row41', e = True, en = False)        
        mc.rowLayout('row42', e = True, en = False)        
        mc.button('pbBtn', e = True, en = False)
    
def playerSelector(*args):
    playerLabel = mc.optionMenu('playerSel', q = True, sl = True)    
    if playerLabel   == 1:
        player = eval('rv')
    elif playerLabel == 2:
        player = eval('quickTime')
    elif playerLabel == 3:
        player = eval('mpc')       
    return player

def playPubButton(*args):    
    item = mc.textScrollList('pubListsTS', q = True, selectItem = True)
    player = playerSelector()
    files = [player]
    filePathPubTxt = refreshList() [2]
    filePathPubTxt = filePathPubTxt.replace('/','\\')
    if item:
        for j in item:
            files.append(filePathPubTxt + j)
        Popen(files)
    else:
        mc.warning('No Select List')
        
def insPubToShot(*args):
    item = mc.textScrollList('pubListsTS', q = True, selectItem = True)[0]
    shotList = mc.ls(sl = True, typ = 'shot')
    filePathPubTxt = refreshList() [2]
    filePathPubTxt = filePathPubTxt.replace('/','\\')
    if not shotList:
        cam = mc.camera()[0]
        selShot = mc.shot(cc = cam)
    else:
        selShot = shotList[0]
    st           = mc.shot(selShot, q = True, st = True)
    clipFrameLen = mc.movieInfo('%s%s' %(filePathPubTxt, item), f = True)[0]
    endTime      = (st + int(clipFrameLen)) - 1    
    mc.shot(selShot, e = True, et = endTime)
    mc.shot(selShot, e = True, cl = '%s%s' %(filePathPubTxt, item)) # add publish clip   
    camCurrShot  = mc.shot(selShot, q = True, cc = True)    
    mc.setAttr("%s.displayResolution" %camCurrShot, 1)
    mc.setAttr("%s.displayGateMaskOpacity" %camCurrShot, 1)
    mm.eval('setAttr "%s.displayGateMaskColor" -type double3 0 0 0 ;' %camCurrShot)
       
def playVerButton(*args):
    item           = mc.textScrollList('verListsTS', q = True, selectItem = True)[0]
    player         = playerSelector()
    filePathVerTxt = refreshList() [0]
    filePathVerTxt = filePathVerTxt.replace('/','\\')      
    if item:
        Popen([player, filePathVerTxt + item])
    else:
        mc.warning('No Select List')          

def deleteVerButton(*args) :
    confirm = mc.confirmDialog(t = 'Delete file', m = 'Do you want to delete?', b = ['Yes','No'], db = 'Yes', cb = 'No', ds = 'No')     
    if confirm == 'Yes':
        items          = mc.textScrollList('verListsTS', q = True, selectItem = True)
        filePathVerTxt = refreshList() [0]
        filePathVerTxt = filePathVerTxt.replace('/','\\')
        if items:
            for i in items:
                remove(filePathVerTxt + i) 
        else:
            mc.warning('No Select List')    
        refreshList()
    elif confirm == 'No':
        pass
   
def doPlayblast(*args):
    filePathVerTxt = refreshList() [0]
    latestVerFile  = refreshList() [1]
    filePathPubTxt = refreshList() [2]
    pbFilePubTxt   = refreshList() [3]
    usrFileName    = refreshList() [4]
    pipeline       = projDataBase.projDataBase(currentFilePath) [1]
    overScan       = projDataBase.projDataBase(currentFilePath) [4]
    player         = playerSelector()
    origNaming     = pbFilePubTxt.replace('.mov','') # remove .mov to add user name
    
    pbModeRdoIV    = mc.radioCollection('pbModeRdoI', q = True, sl = True)
    startIntF      = mc.intField       ('startIntF', q = True, v = True)
    endIntF        = mc.intField       ('endIntF', q = True, v = True)
    resCombo       = mc.optionMenu     ('resCombo', q = True, v = True)
    viewChk        = mc.checkBox       ('viewChk', q = True, v = True)    
    ovrwChk        = mc.checkBox       ('ovrwChk', q = True, v = True)
    pubChk         = mc.checkBox       ('pubChk', q = True, v = True)
        
    selShots       = mc.ls(sl = True, typ = 'shot')    
    timeCtrl       = mm.eval('$tmpVar=$gPlayBackSlider')
    currSound      = mc.timeControl(timeCtrl, q = True, s = True)
    
    if pipeline == 0:
        if latestVerFile == 'none':
            outputName = origNaming+'_v001'+'_'+usrFileName+'.mov'
        else:
            a = latestVerFile.split('_')[-2][1:]
            lastVerNum = int(a)
            if ovrwChk == True:           
                outputName = origNaming+'_v%03d'%(lastVerNum)+'_'+usrFileName+'.mov'
            elif ovrwChk == False:
                outputName = origNaming+'_v%03d'%(lastVerNum+1)+'_'+usrFileName+'.mov'   
    elif pipeline == 1:
        if latestVerFile == 'none':
            outputName = origNaming+'_v001'+'.mov'
        else:
            a          = latestVerFile.split('_')[-1][1:-4]
            lastVerNum = int(a)
            if ovrwChk   == True:           
                outputName = origNaming+'_v%03d'%(lastVerNum)+'.mov'
            elif ovrwChk == False:
                outputName = origNaming+'_v%03d'%(lastVerNum+1)+'.mov'         
            
    if pbModeRdoIV   == 'A':
        start   = mc.playbackOptions(q = True, min = True)
        end     = mc.playbackOptions(q = True, max = True)
        sqTime  = False
    elif pbModeRdoIV == 'B':
        start, end = mc.timeControl(timeCtrl, q = True, rangeArray = True)
        sqTime  = False
    elif pbModeRdoIV == 'C':
        selShot = mc.ls(sl=True, typ='shot')
        n       = len(selShot)        
        start   = mc.shot(selShot[0], q = True, sst = True)
        end     = mc.shot(selShot[n-1], q = True, set = True)
        sqTime  = True
    elif pbModeRdoIV == 'D':
        start   = startIntF
        end     = endIntF
        sqTime  = True
        
    if resCombo   == '1920 x 1080 (Full HD)':
        width   = 1920
        height  = 1080
    elif resCombo == '1920 x 800 (Anamorphic)':
        width   = 1920
        height  = 800
    elif resCombo == '1280 x 720 (HD)':
        width   = 1280
        height  = 720

    camMCtrlLs = mc.ls('*master_ctrl', r = True)
    ovrscnCtrl = []
    for i in camMCtrlLs:
        try:
            ovrscnCtrl.append(mc.getAttr('%s.overscan' %i))
            mc.setAttr('%s.overscan' %i, overScan)
        except:
            pass
   
    cam = mc.listCameras(p = True)
    ovrscnCam = []
    for i in cam:
        ovrscnValue = mc.getAttr("%s.overscan" %i)
        if mc.connectionInfo("%s.overscan" %i, isDestination = True):
            pass
        else:
            ovrscnCam.append(mc.getAttr('%s.overscan' %i))
            mc.setAttr("%s.overscan" %i, overScan)
    
    clearHUD()
    
    if pbModeRdoIV == 'E': ##############################################  
            
        for i in selShots:           
            sStart         = mc.shot(i, q = True, sst = True)
            sEnd           = mc.shot(i, q = True, set = True)
            departName     = pbFilePubTxt.split('_')[-1][:-4]
            outputShotName = i + '_' + departName
                
            range =(sEnd - sStart)+1
            mc.headsUpDisplay('HudSCN', s = 5, b = 0, bs = 'small', lfs = 'large', dfs = 'large', l = 'Scene Name  :  '+origNaming)
            mc.headsUpDisplay('HudUSN', s = 5, b = 1, bs = 'small', lfs = 'large', dfs = 'large', l = 'User  :  '+usrFileName)
            mc.headsUpDisplay('HudFRN', s = 9, b = 0, bs = 'small', lfs = 'large', dfs = 'large', l = 'Frame Range  :   %s  -  %s(%s)' %(int(sStart),int(sEnd),int(range)))
            mc.headsUpDisplay('HudFNB', s = 9, b = 1, bs = 'small', lfs = 'large', dfs = 'large', l = 'Frame Number  :', c = "mc.currentTime(q = True)", atr = True)               
            
            mc.playblast( startTime      = sStart,
            			  endTime        = sEnd,
            			  sequenceTime   = True,
            			  filename       = "%sshots/%s" %(filePathVerTxt, outputShotName),
            			  showOrnaments  = True,
            			  widthHeight    = (width, height),
            			  sound          = currSound,
            			  viewer         = False,
            			  offScreen      = True,
            			  percent        = 100,
            			  quality        = 100,
            			  forceOverwrite = True,
            			  clearCache     = True,
            			  compression    = "H.264",
            			  format         = "qt")
           			  
            if pubChk == True:
                mc.sysFile('%sshots' %filePathPubTxt, makeDir = True)
                copy2("%sshots/%s.mov" %(filePathVerTxt, outputShotName), "%sshots/%s.mov" %(filePathPubTxt, outputShotName))
            
            clearHUD()
            
    else: ############################################## 
        range =(end - start)+1
        mc.headsUpDisplay('HudSCN', s = 5, b = 0, bs = 'small', lfs = 'large', dfs = 'large', l = 'Scene Name  :  ' + origNaming)
        mc.headsUpDisplay('HudUSN', s = 5, b = 1, bs = 'small', lfs = 'large', dfs = 'large', l = 'User  :  ' + usrFileName)
        mc.headsUpDisplay('HudFRN', s = 9, b = 0, bs = 'small', lfs = 'large', dfs = 'large', l = 'Frame Range  :   %s  -  %s(%s)' %(int(start),int(end),int(range)))
        mc.headsUpDisplay('HudFNB', s = 9, b = 1, bs = 'small', lfs = 'large', dfs = 'large', l = 'Frame Number  :', c = "mc.currentTime(q = True)", atr = True)

        try:
            remove(TMP)        
        except:
            pass
            
        mc.playblast( startTime      = start,
        			  endTime        = end,
        			  sequenceTime   = sqTime,
        			  filename       = TMP,
        			  showOrnaments  = True,
        			  widthHeight    = (width, height),
        			  sound          = currSound,
        			  viewer         = False,
        			  offScreen      = True,
        			  percent        = 100,
        			  quality        = 100,
        			  forceOverwrite = True,
        			  clearCache     = True,
        			  compression    = "H.264",
        			  format         = "qt")
                      
        fpsMaya = mc.currentUnit(q=True, time=True)
        if 'fps' in fpsMaya:
            fps = fpsMaya.replace("fps", "")
        else:
            fpsList = {
                "game" : 15,
                "film" : 24,
                "pal"  : 25,
                "ntsc" : 30,
                "show" : 48,
                "palf" : 50,
                "ntscf" : 60,
            }
            try:
                fps = fpsList[fpsMaya]
            except:
                fps = 30
        
        ffmpegCommand  = "%s -y -i \"%s.mov\" -r %s " %(ffmpeg, TMP, fps)
        ffmpegCommand += "-vcodec libx264 -vprofile baseline -crf 22 -bf 0 "
        ffmpegCommand += "-pix_fmt yuv420p -f mov "
        ffmpegCommand += "%s%s" %(filePathVerTxt, outputName)
        call(ffmpegCommand)
        
        if pubChk == True:
            mc.sysFile('%s' %filePathPubTxt, makeDir = True)
            copy2("%s%s" %(filePathVerTxt, outputName), "%s%s" %(filePathPubTxt, pbFilePubTxt))
            
        if viewChk == True:
            filePathVerExp = filePathVerTxt.replace('/','\\')
            Popen([player, str(filePathVerExp + outputName)])
    
    n = 0           		  
    for i in camMCtrlLs:
        try:
            mc.setAttr('%s.overscan' %i, ovrscnCtrl[n])
            n += 1        
        except:
            pass
    n = 0        
    for i in cam:
        if mc.connectionInfo("%s.overscan" %i, isDestination = True):
            pass
        else:
            mc.setAttr("%s.overscan" %i, ovrscnCam[n])
            n += 1

    clearHUD()
    refreshList()

def clearHUD(*args):
    mc.headsUpDisplay(rp = (5,0))
    mc.headsUpDisplay(rp = (5,1))
    mc.headsUpDisplay(rp = (9,0))
    mc.headsUpDisplay(rp = (9,1))
    
def submitSG(*args):
    reload(db_hook)
    workspaceFile  = mc.file(q = True, sn = True)
    filePathVerTxt = refreshList() [0]
    latestVerFile  = refreshList() [1]
    userName       = mc.optionVar(q = 'RFUser')
    db_hook.send_to_review(workspaceFile, filePathVerTxt + latestVerFile, userName)
    mc.warning('Shotgun submitting done')
    
    filePathPubTxt = refreshList() [2]
    pbFilePubTxt   = refreshList() [3]
    mc.sysFile('%s' %filePathPubTxt, makeDir = True)
    copy2("%s%s" %(filePathVerTxt, latestVerFile), "%s%s" %(filePathPubTxt, pbFilePubTxt))

def openSG(*args):
    url = projDataBase.projDataBase(currentFilePath) [3]
    open(url)

def getDailyPath(*args):
    pipeline       = projDataBase.projDataBase(currentFilePath) [1]
    now            = datetime.now()
    dateYear       = '%d'   %now.year
    dateMonth      = '%02d' %now.month
    dateDay        = '%02d' %now.day
    dailyPath      = projDataBase.projDataBase(currentFilePath) [2]
    filePathVerTxt = refreshList() [0]
    latestVerFile  = refreshList() [1]
    currentFileLoc = currentFilePath
    aList          = currentFileLoc.split('/') # split directory heirachy into list for set new playblast directory
    nameOfCurrent  = aList[-1]#[:-8]
    if pipeline   == 0:
        depart         = nameOfCurrent.split('_')[-3]    
    elif pipeline == 1:
        depart         = nameOfCurrent.split('_')[-2]    
    dailyNowPath   = dailyPath + '/' + depart + '/' + dateYear + '_' + dateMonth + '_' + dateDay + '/'
    return dailyNowPath, filePathVerTxt, latestVerFile
    
def submitDaily(*args):
    dailyNowPath   = getDailyPath() [0]
    filePathVerTxt = refreshList()  [0]
    latestVerFile  = refreshList()  [1]
    mc.sysFile('%s' %dailyNowPath, makeDir = True)
    aList          = latestVerFile.split('_')
    dailyFile      = aList[0] + '_' + aList[1] + '_' + aList[2] + '_' + aList[3] + '_' + aList[4] + '_' + aList[6]
    copy2("%s%s" %(filePathVerTxt, latestVerFile), "%s%s" %(dailyNowPath, dailyFile))
    mc.warning('Daily submitting done')
    
    filePathPubTxt = refreshList() [2]
    pbFilePubTxt   = refreshList() [3]
    mc.sysFile('%s' %filePathPubTxt, makeDir = True)
    copy2("%s%s" %(filePathVerTxt, latestVerFile), "%s%s" %(filePathPubTxt, pbFilePubTxt))    
    
def browseDaily(*args):
    dailyNowPath = getDailyPath() [0]
    dailyNowPath = dailyNowPath.replace('/','\\')  
    mc.sysFile('%s' %dailyNowPath, makeDir = True)  
    startfile(dailyNowPath)
