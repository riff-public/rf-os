import maya.cmds as cmd

def exportCamUI():
    if cmd.window('exportCamWin', exists = True): 
        cmd.deleteUI('exportCamWin')    
    output = getFileName()        
    cmd.window('exportCamWin', title = 'Export camera', rtf = True, sizeable = True, mxb = False)
    main = cmd.columnLayout(adj = True, rs = 5)  
    cmd.separator(style = 'in')
    cmd.text(l = 'Select camera group first')
    cmd.separator(style = 'in')
    cmd.text(l = 'File will save to: ' + output, ww = True)
    cmd.button(l = 'Export', c = saveFile)    
    cmd.showWindow('exportCamWin')  

def getFileName():
    currentFileLoc = cmd.file(q = True, loc = True)
    aList = currentFileLoc.split('/')
    bList = aList[-1].split('_')
    aList.pop(-1)
    bList = bList[0:4]
    filePath = ''
    fileName = ''    
    for i in aList:
        filePath = filePath + i + '/'
    for i in bList:
        fileName = fileName + i + '_'    
    fileName = fileName + 'cam.ma'    
    return filePath + fileName
    
def saveFile(*args):   
    output = getFileName()
    sel = cmd.ls(sl = True)[0]
    cmd.select (sel, r = True)
    cmd.file ("%s" %output, force = True, options = "v=0;", typ = "mayaAscii", pr = True, es = True)

def exportCam_UI():
    exportCamUI()