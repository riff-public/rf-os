import pymel.core as pm
import maya.cmds as mc

def UI():
    if pm.window('exportCamWin', exists = True): 
        pm.deleteUI('exportCamWin')
    pm.window('exportCamWin', title = 'Export Camera', rtf = True, sizeable = True, mxb = False)
    
    pm.columnLayout(adj = True, rs = 5)
    pm.separator(style = 'none', h = 2)
    pm.separator(style = 'in')
    pm.text('text01', l = '')
    pm.separator(style = 'in')
    pm.button('yepBtn', l = 'Yep, Export Now', c = doExportNow)
    pm.button('nopeBtn', l = "Nope, I'll select another camera grp, then press this button", c = doExportSel)
    
    pm.showWindow('exportCamWin')
    getCurrCamName()

def getCurrCamName():
    try:
        topParent = pm.ls(assemblies = True)
        camMainGrp = pm.ls('*cam_grp*', r = True)[0]    
        currFile = pm.system.sceneName().split('/')[-1]
        currShot = currFile.split('_')[-4]    
        camGrpLs = pm.listRelatives(str(camMainGrp), c = True)
        for i in camGrpLs:
            camGrpName = i.split('_')[-2]
            if camGrpName.lower() == currShot.lower():
                currCam = i
                pm.select(i)
        pm.text('text01', e = True, l = 'Do you want to export "%s"?' %currCam)
        return currCam
    except:
        pm.text('text01', e = True, l = '"%s" camera was not found. Please select a camera group manually, then press button.' %currShot)
        pm.button('yepBtn', e = True, en = False)
        pm.button('nopeBtn', e = True, l = "Export selected camera group")

def getFileName():
    currentFileLoc = mc.file(q = True, loc = True)
    pList = currentFileLoc.split('/')
    fList = pathSplitList[-1].split('_')    
    verPath = pList[0]+'/'+pList[1]+'/'+pList[2]+'/'+pList[3]+'/'+pList[4]+'/'+pList[5]+'/'+pList[6]+'/'+pList[7]+'/camera'
    pubPath = pList[0]+'/'+pList[1]+'/'+pList[2]+'/'+pList[3]+'/'+pList[4]+'/cameras'    
    mc.sysFile(verPath, makeDir = True)
    mc.sysFile(pubPath, makeDir = True)
    verFileList = mc.getFileList(folder = verPath, fs = '*.ma')
    if verFileList:
        latestVerFile = max(verFileList)
        latestVerNum = int(latestVerFile.split('_')[-1][1:-3]) + 1
    else:
        latestVerNum = 1    
    verFile = fList[0]+'_'+fList[1]+'_'+fList[2]+'_'+fList[3]+'_cam_v%003d.ma' %latestVerNum
    pubFile = fList[0]+'_'+fList[1]+'_'+fList[2]+'_'+fList[3]+'_cam.ma'
    return verPath, verFile, pubPath, pubFile

def doExportNow(*args):
    currCam = getCurrCamName()
    verPath = getFileName()[0]
    verFile = getFileName()[1]
    pubPath = getFileName()[2]
    pubFile = getFileName()[3]
    pm.select(currCam)
    mc.file ("%s/%s" %(verPath,verFile), force = True, options = "v=0;", typ = "mayaAscii", pr = True, es = True)
    mc.file ("%s/%s" %(pubPath,pubFile), force = True, options = "v=0;", typ = "mayaAscii", pr = True, es = True)

def doExportSel(*args):
    verPath = getFileName()[0]
    verFile = getFileName()[1]
    pubPath = getFileName()[2]
    pubFile = getFileName()[3]
    mc.file ("%s/%s" %(verPath,verFile), force = True, options = "v=0;", typ = "mayaAscii", pr = True, es = True)
    mc.file ("%s/%s" %(pubPath,pubFile), force = True, options = "v=0;", typ = "mayaAscii", pr = True, es = True)
