import maya.cmds as cmd
import maya.mel as mm
from rftool.layout.tools import createAbc as abc
from rftool.layout.tools import selCamCtrl
from rftool.layout.tools import exportCam
from rftool.layout.tools import bakeShake
from rftool.layout.tools import bakeCam
from rftool.layout.tools import createCurrentShot
from rftool.layout.tools import snapFocus
from rftool.layout.tools import saveShakeXML
from rftool.layout.tools import loadShakeXML
reload(bakeShake)

def UI():
    if cmd.window('finalCamWin', exists = True): 
        cmd.deleteUI('finalCamWin')
    finalCamWin = cmd.window('finalCamWin', title = 'Final Layout Tool', ret = True, rtf = True, sizeable = True, tlb = True)
    scrollLyt = cmd.scrollLayout(cr = True)
    main = cmd.columnLayout(adj = True, p = scrollLyt)
    cmd.separator(style = 'none', h = 5, p = main)    
    cmd.button(l = 'Organize Scene', p = main, c = organizeCam)
    cmd.separator(style = 'none', h = 5, p = main) 
    ctrlLayout = cmd.frameLayout(borderStyle = 'out',
                          label = 'Camera Controllers', 
                          marginHeight = 3, 
                          marginWidth = 5,
                          cll = True,
                          parent = main) 
    cmd.textScrollList('ctrlListTS', w = 50, append = ['Placement', 'Main', 'Gimbal', 'Shake','Viewport', 'Aim', 'AimGimbal', 'Up'], h = 120, ams = True, p = ctrlLayout, sc = selCamCtrl.selCtrl)
    chBoxLayout = cmd.frameLayout(borderStyle = 'out',
                          label = 'Channel Box', 
                          marginHeight = 3, 
                          marginWidth = 5,
                          cll = True,
                          parent = main)
    cmd.channelBox('dave', h = 330, w = 50, els = True, p = chBoxLayout)
    cmd.popupMenu('menu', p = 'dave', pmc = lambda *args: mm.eval('generateChannelMenu menu 1'))
    toolsLayout = cmd.frameLayout(borderStyle = 'out',
                          label = 'Utilities', 
                          marginHeight = 3, 
                          marginWidth = 5,
                          cll = True,
                          parent = main)
    col1 = cmd.columnLayout(adj = True, rs = 5, p = toolsLayout)                      
    row1 = cmd.rowLayout(nc = 4, adj = 1, p = col1)    
    cmd.text(l = 'Create Abc: ', p = row1)
    cmd.button(l = '1', w = 45, p = row1, c = lambda x: abc.doCreateAbc(1))
    cmd.button(l = '2', w = 45, p = row1, c = lambda x: abc.doCreateAbc(2))
    cmd.button(l = '3', w = 45, p = row1, c = lambda x: abc.doCreateAbc(3))
    cmd.button(l = 'Clear Alembic', c = lambda x: abc.clearAbc, p = col1)
    cmd.separator(style = 'in', p = col1)
    cmd.button(l = 'Create Current Shot', p = col1, c = createCurrentShot.createCurrShot)  
    cmd.separator(style = 'in', p = col1)
    cmd.button(l = 'Snap Focus', p = col1, c = snapFocus.snapFocus)    
    row1 = cmd.rowLayout(nc = 3, adj = 1, p = col1)
    cmd.button(l = 'Save Shake', w = 75, p = row1, c = saveShakeXML.saveShakeXML)
    cmd.button(l = 'Load Shake', w = 75, p = row1, c = loadShakeXML.loadShakeXML)   
    cmd.button(l = 'Bake Shake to Anim', p = col1, c = bakeShake.bakeShake)
    cmd.button(l = 'Bake to Basic Cam', p = col1, c = bakeCam.bakeCam)
    cmd.separator(style = 'in', p = col1)      
    cmd.button(l = 'Export Camera', p = col1, c = exportCam.exportCam)    
    cmd.separator(style = 'none', p = col1)
    
    if cmd.dockControl('finalCamWin', exists = True): 
        cmd.deleteUI('finalCamWin', control = True)    
    camManagerDock = cmd.dockControl(area = 'right', 
                                content = finalCamWin, 
                                w = 230, 
                                l = 'Final Camera Tools')

def organizeCam(*args):
    import sys ;

    mp = "//riff-data/Data/Data/Pipeline/core/rf_maya/rftool/simulation" ;
    if not mp in sys.path :
        sys.path.insert ( 0 , mp ) ;

    import projects.twoHeroes.layoutOrganizer as twoHeroes_layoutOrganizer ;
    reload ( twoHeroes_layoutOrganizer ) ;
    twoHeroes_layoutOrganizer.run ( ) ;
