# RiFF Camera Manager (Dock Version) for Layout Artist
# Designed by: Pathompong Thitithan
# Develop by:  Kathawut Poldet

import pymel.core as pm
import maya.mel as mm
from collections import OrderedDict 
import re

class camManagerMain():
    
    # GLOBAL VARIABLE
    def __init__(self):
        self._shakeValue = []
 
    # INTERFACE    
    def UI(self):
        if pm.window('camManagerWin', exists = True): 
            pm.deleteUI('camManagerWin')           
        camManagerWin = pm.window('camManagerWin')
        scrollLyt = pm.scrollLayout(w = 230, cr = True)
        mainLayout = pm.columnLayout(cw = 220, adj = True, p = scrollLyt)
        initCam = pm.frameLayout(borderStyle = 'out',
                                 label = 'Preparing Cameras', 
                                 marginHeight = 5, 
                                 marginWidth = 5,
                                 width = 220,
                                 cll = True, 
                                 parent = mainLayout)
        row1 = pm.rowLayout(nc = 3,
                            adj = 2, 
                            columnAttach = [(1,'both',5),(2,'both',5),(3,'both',5)],
                            p = initCam)
        pm.text(l='Camera Group:', al = 'right', w = 80, p = row1)
        self.camGrpName = pm.text(l='[none]', al = 'left', p = row1)
        pm.button(l='Load', w = 50, p = row1,  c = self.loadCamGrpProc)
        row2 = pm.rowLayout(nc = 3, 
                            adj = 2,
                            columnAttach = [(1,'both',5),(2,'both',5),(3,'both',5)],
                            p = initCam)
        pm.text(l='Ref. camera:', al = 'right', w = 80, p = row2)
        self.refCamName = pm.text(l='persp', al = 'left', fn='boldLabelFont', p = row2)
        pm.button(l='Load', w = 50, p = row2, c = self.loadRefCamProc)
        ctrlCam = pm.frameLayout(borderStyle = 'out',
                                 label = 'Camera Controllers', 
                                 marginHeight = 5, 
                                 marginWidth = 5,
                                 width = 220, 
                                 cll = True, 
                                 parent = mainLayout)
        row3 = pm.rowLayout(nc = 2,
                            adj = 1, 
                            cw = (2,85),
                            columnAttach = [(1,'both',2),(2,'both',2)], 
                            p = ctrlCam)
        pm.text(l='Camera list:', al = 'left', p = row3)
        pm.text(l='Controller:', al = 'left', p = row3)
        # TextScrollList #
        row4 = pm.rowLayout(nc = 2,
                            adj = 1,
                            cw = (2,85), 
                            columnAttach = [(1,'both',2),(2,'both',2)], 
                            p = ctrlCam)        
        self.cam_ls_view = pm.textScrollList(allowMultiSelection = True, h = 160, p = row4, sc = self.selectCamProc)
        self.ctrl_ls_view = pm.textScrollList(allowMultiSelection = True, h = 160, 
                          append=['Placement', 'Main', 'Gimbal', 'Shake','Viewport', 'Aim', 'AimGimbal', 'Up'], 
                          sc = self.selectConProc, p = row4)
        # TextScrollList #
        row5 = pm.rowLayout(nc = 2,
                            adj = 1, 
                            cw = (2,85),
                            columnAttach = [(1,'both',2),(2,'both',2)], 
                            p = ctrlCam)        
        pm.button(l='Current shot', p = row5, c = self.selectCurrentShotProc)
        pm.text(l='', p = row5)
        chBox = pm.frameLayout(borderStyle = 'out',
                              label = 'Channel Box', 
                              marginHeight = 5, 
                              marginWidth = 5,
                              width = 220, 
                              cll = True, 
                              parent = mainLayout)
        pm.channelBox('dave', h = 250, els = True, p = chBox)
        pm.popupMenu('menu', p = 'dave', pmc = lambda *args: mm.eval('generateChannelMenu menu 1'))
        util = pm.frameLayout(borderStyle = 'out',
                              label = 'Utilities', 
                              marginHeight = 5, 
                              marginWidth = 5,
                              width = 220, 
                              cll = True,
                              parent = mainLayout) 
        pm.text(l = '  Ref. Camera Lens Select:', al = 'left', p = util)                  
        lensForm = pm.formLayout(numberOfDivisions=100, p = util)
        b1 = pm.button(l='24mm', p = lensForm, c = lambda x: self.ref_cam_lens(24))
        b2 = pm.button(l='28mm', p = lensForm, c = lambda x: self.ref_cam_lens(28))
        b3 = pm.button(l='35mm', p = lensForm, c = lambda x: self.ref_cam_lens(35))
        b4 = pm.button(l='50mm', p = lensForm, c = lambda x: self.ref_cam_lens(50))
        b5 = pm.button(l='85mm', p = lensForm, c = lambda x: self.ref_cam_lens(85))
        b6 = pm.button(l='135mm', p = lensForm, c = lambda x: self.ref_cam_lens(135))
        pm.formLayout(lensForm, 
                      edit = True, 
                      attachForm = [ (b1,'left',5), (b1,'top',5),    (b2,'top',5),    (b3,'right',5),(b3,'top',5), 
                                     (b4,'left',5), (b4,'bottom',0), (b5,'bottom',0), (b6,'right',5),(b6,'bottom',0) ], 
                      attachControl = [ (b2,'left',5,b1),(b2,'right',5,b3), 
                                        (b5,'left',5,b4),(b5,'right',5,b6) ],
                      attachPosition = [(b1,'right',5,35), (b3,'left',5,65), (b4,'right',5,35), (b6,'left',5,65) ] )
        pm.separator(style = 'in', p = util)
        grid3 = pm.columnLayout(rs = 4, 
                                adj = True,
                                columnAttach = ('both',5), 
                                p = util)
        pm.button(l='Snap to Ref. Cam', w=130, p = grid3, c = self.snapRefCamProc)
        pm.button(l='Snap Focus', w=130, p = grid3, c = self.snapFocusProc)
        pm.button(l='Copy Shake Value', p = grid3, c = self.copyShakeProc)
        pm.button(l='Paste Shake Value', p = grid3, c = self.pasteShakeProc)
        pm.button(l='Bake Shake to Anim', p = grid3, c = self.bakeShakeAnimProc)
        pm.button(l='Bake to Basic Cam', p = grid3, c = self.bakeBasicCam)
        if pm.dockControl('camManagerDock', exists = True): 
            pm.deleteUI('camManagerDock', control = True)
        camManagerDock = pm.dockControl(area = 'left', 
                                        content = camManagerWin, 
                                        w = 230, 
                                        l = 'Camera Manager')
        
    def loadCamGrpProc(self, *args, **kwargs):
        self.load_cam_ls()
               
    def loadRefCamProc(self, *args, **kwargs):
        self.load_ref_cam()
        
    def selectCamProc(self, *args, **kwargs):
        pm.textScrollList(self.ctrl_ls_view, e = True, da = True)
        self.get_cam_selected()
        
    def selectConProc(self, *args, **kwargs):
        self.get_con_selected()
        
    def selectCurrentShotProc(self, *args, **kwargs):
        self.get_current_cam()
        
    def snapRefCamProc(self, *args, **kwargs):
        self.snap_ref_cam()
        
    def snapFocusProc(self, *args, **kwargs):
        self.snap_focus()
        
    def copyShakeProc(self, *args, **kwargs):
        self.copy_shake()
        
    def pasteShakeProc(self, *args, **kwargs):
        self.paste_shake()
        
    def bakeShakeAnimProc(self, *args, **kwargs):
        self.bake_shake_anim()
        
    def bakeBasicCam(self, *args, **kwargs):
        self.bake_basic_cam()
        

############################ Function #################################

    # List camera from selected in Outliner
    def load_cam_ls(self, *args, **kwargs):
        # Check if it's a group or not
        selection = pm.ls(selection=True, transforms=True)
        def is_group(node):
            children = node.getChildren()
            for child in children:
                if type(child) is not pm.nodetypes.Transform:
                    return False
            return True
        selection = filter(is_group, selection)
        if len(selection) != 0:
            check = True
        else:
            check = False
        # Execute
        if check == True:
            pm.textScrollList(self.cam_ls_view,e = True, ra = True)
            cam_ls = pm.listRelatives(pm.ls(sl = True, dag = True, ca = True), parent = True)
            pm.textScrollList(self.cam_ls_view, e = True, append = cam_ls)
            camGrpName = pm.ls(sl = True)
            pm.text(self.camGrpName, e = True, l = '%s' %camGrpName[0], fn='boldLabelFont')
        else:
            pm.warning('Please select cameras group')
            
    # Get selected from listView and return value    
    def get_cam_selected(self, *args, **kwargs):
        cam_selected = pm.textScrollList(self.cam_ls_view, q = True, si = True)
        cam_shot_grp = pm.listRelatives(cam_selected, p = True)
        return cam_shot_grp
        
    # Use value from function above to select object in scene
    def get_con_selected(self, *args, **kwargs):
        # Get current index from listView
        ctrl_index = str(pm.textScrollList(self.ctrl_ls_view, q = True, sii = True))
        # Keep selected camera into list [cam1,cam2,cam3,....n]
        cam_selected_ls = []
        for i in self.get_cam_selected():
            ctrl_ls = pm.listRelatives(pm.listRelatives(pm.ls(i, dag = True), type = "nurbsCurve"), p = True)
            ctrl_ls = list(OrderedDict.fromkeys(ctrl_ls))
            cam_selected_ls.append(ctrl_ls)
        # Convert current index into list (remove character keep only number)
        ctrl_index = list(re.sub("\D", "", ctrl_index))
        # Matching index list with camera list and keep it in array (cam1:con1, cam2:con1, cam1:con2, cam2,con2...n)
        ctrl_selected_ls = []
        for i in list(ctrl_index):
            for c in cam_selected_ls:
                ctrl_selected_ls.append(c[int(i) - 1])
        pm.select(ctrl_selected_ls)
    
    # Load ref. camera    
    def load_ref_cam(self, *args, **kwargs):
        try:
            sel = pm.ls(sl = True)
            camCheck = pm.listRelatives(pm.ls(sl = True), typ = 'camera')
            check = pm.objectType(camCheck[0], isType = 'camera' )
            if check == True:
                refCamName = pm.ls(sl = True)
                pm.text(self.refCamName, e = True, l = '%s' %refCamName[0], fn='boldLabelFont')
            else:
                pm.warning('Please select a camera')
        except:
            pm.warning('Please select a camera')
        
    # Select cam that on current time
    def get_current_cam(self, *args, **kwargs):
        try:
            currentShot = pm.ls(pm.sequenceManager(q=True, currentShot=True))
            currentCam = pm.getAttr("%s.currentCamera" % currentShot[0])
            pm.textScrollList(self.cam_ls_view, e = True, da = True, si = currentCam)
            pm.textScrollList(self.ctrl_ls_view, e = True, da = True)

            start = pm.shot(currentShot, q = True, startTime = True)
            end = pm.shot(currentShot, q = True, endTime = True)
            pm.playbackOptions(ast = start, min = start, aet = end, max = end)
        except:
            pm.warning('Please check on the Camera Sequencer')
        
    # Set ref.cam focal length
    def ref_cam_lens(self, refFL, *args, **kwargs):
        try:
            refCam = pm.text(self.refCamName, q = True, l = True)
            refCamShape = pm.ls(refCam)
            pm.setAttr(refCamShape[0].focalLength, refFL)
        except:
            pm.warning('Please load Ref. camera')
    
   # snap to ref. camera    
    def snap_ref_cam(self, *args, **kwargs):
        sel = pm.ls(sl = True)
        selName = str(sel[0])
        if not sel:
            pm.warning('Please select one camera controller')
        else:
            try:
                refCam = pm.text(self.refCamName, q = True, l = True)
                pos = pm.xform(refCam, q = 1, ws = 1, t = 1)
                rot = pm.xform(refCam, q = 1, ws = 1, ro = 1)
                if 'constrain_ctrl' in selName:
                    selReltv = pm.listRelatives(pm.listRelatives(pm.ls(sel[0]), p = True), ad=True, typ='transform')
                    pm.xform(sel[0], ws = 1, t = [pos[0],pos[1],pos[2]])
                    pm.xform(selReltv[1], ws = 0, t = [0,0,0])
                    pm.xform(sel[0], ws = 1, ro = [0,rot[1],0])
                    pm.xform(selReltv[1], ws = 0, ro = [rot[0],0,rot[2]])
                else:
                    pm.xform(sel[0], ws = 1, t = [pos[0],pos[1],pos[2]])
                    pm.xform(sel[0], ws = 1, ro = [rot[0],rot[1],rot[2]])
                refCamShape = pm.ls(refCam)
                fl = pm.getAttr(refCamShape[0].focalLength)
                ctrl_ls = pm.listRelatives(pm.listRelatives(pm.ls(self.get_cam_selected(), dag = True), type = "nurbsCurve"), p = True)
                ctrl_ls = list(OrderedDict.fromkeys(ctrl_ls))
                pm.setAttr("%s.focal_length" % ctrl_ls[1], fl)
            except:
                pm.warning('Please check on Ref. camera')
    
    # Snap focus to selected object    
    def snap_focus(self, *args, **kwargs):
        try:
            obj = pm.ls(sl = True)
            loc = pm.listRelatives(self.get_cam_selected())[3]
            pos = pm.xform(obj[0], q = 1, ws = 1, t = 1)
            pm.xform(loc, t = [pos[0], pos[1], pos[2]])
            distance_node = pm.listRelatives(self.get_cam_selected())[4]
            distance_value = pm.getAttr("%s.distance" % distance_node)
            ctrl_ls = pm.listRelatives(pm.listRelatives(pm.ls(self.get_cam_selected(), dag = True), type = "nurbsCurve"), p = True)
            ctrl_ls = list(OrderedDict.fromkeys(ctrl_ls))
            pm.setAttr("%s.focus_distance" % ctrl_ls[4], distance_value)
        except:
            pm.warning('Please select current camera and an object you want to focus')
    
    # Copy value from selected Shake ctrl    
    def copy_shake(self, *args, **kwargs):
        try:
            selCopy = pm.ls(sl=True)
            seed = pm.getAttr(selCopy[0].seed)
            offset = pm.getAttr(selCopy[0].offset)
            freqTX = pm.getAttr(selCopy[0].frequencyTransX)
            freqTY = pm.getAttr(selCopy[0].frequencyTransY)
            ampTX = pm.getAttr(selCopy[0].amplitudeTransX)
            ampTY = pm.getAttr(selCopy[0].amplitudeTransY)
            freqRX = pm.getAttr(selCopy[0].frequencyRotX)
            freqRY = pm.getAttr(selCopy[0].frequencyRotY)
            freqRZ = pm.getAttr(selCopy[0].frequencyRotZ)
            ampRX = pm.getAttr(selCopy[0].amplitudeRotX)
            ampRY = pm.getAttr(selCopy[0].amplitudeRotY)
            ampRZ = pm.getAttr(selCopy[0].amplitudeRotZ)
            subFreq = pm.getAttr(selCopy[0].subFreqRot)
            subAmp = pm.getAttr(selCopy[0].subAmpRot) 
            self._shakeValue = [seed, offset, 
                                freqTX, freqTY, 
                                ampTX , ampTY, 
                                freqRX, freqRY, freqRZ,
                                ampRX , ampRY , ampRZ ,
                                subFreq, subAmp]
        except:
            pm.warning('Please select shake_ctrl')
            
    # Paste value to selected Shake ctrl     
    def paste_shake(self, *args, **kwargs):
        try:
            setShakeVal = self._shakeValue
            selPaste = pm.ls(sl=True)
            for i in selPaste:
                pm.setAttr(i.seed, setShakeVal[0])
                pm.setAttr(i.offset, setShakeVal[1])
                pm.setAttr(i.frequencyTransX, setShakeVal[2])
                pm.setAttr(i.frequencyTransY, setShakeVal[3])
                pm.setAttr(i.amplitudeTransX, setShakeVal[4])
                pm.setAttr(i.amplitudeTransY, setShakeVal[5])
                pm.setAttr(i.frequencyRotX, setShakeVal[6])
                pm.setAttr(i.frequencyRotY, setShakeVal[7])
                pm.setAttr(i.frequencyRotZ, setShakeVal[8])
                pm.setAttr(i.amplitudeRotX, setShakeVal[9])
                pm.setAttr(i.amplitudeRotY, setShakeVal[10])
                pm.setAttr(i.amplitudeRotZ, setShakeVal[11])
                pm.setAttr(i.subFreqRot, setShakeVal[12])
                pm.setAttr(i.subAmpRot, setShakeVal[13])
        except:
            pm.warning('Please select shake_ctrl')
            
    # Bake shake exp. to anim data
    def bake_shake_anim(self, *args, **kwargs):
        try:
            shakeCtrl = (pm.ls(sl = True))
            minFrame = pm.playbackOptions(q=True, min=True)
            maxFrame = pm.playbackOptions(q=True, max=True)
            shakeCtrlStr = str(shakeCtrl[0])
            subShake = pm.listRelatives(pm.listRelatives(pm.ls(sl=True)))
            shakeCtrl = pm.ls(sl = True)          
            if shakeCtrlStr.endswith("shake_ctrl") == True:
                pm.spaceLocator()
                pm.setAttr('locator1.rotateOrder', 2)           
                pm.select(subShake[0], r = True)
                pm.select('locator1', add = True)
                sel = pm.ls(sl = True)
                pm.parent(sel[1], shakeCtrl[0])
                pm.parentConstraint(sel[0], sel[1])
                pm.bakeResults(sel[1], t = (minFrame,maxFrame))
                pm.copyKey(sel[1], at = ['tx','ty','tz','rx','ry','rz'])
                pm.pasteKey(shakeCtrl[0], at = ['tx','ty','tz','rx','ry','rz'])
                pm.delete(sel[1])
                pm.setAttr(shakeCtrl[0].seed, 0)
                pm.setAttr(shakeCtrl[0].offset, 0)
                pm.setAttr(shakeCtrl[0].frequencyTransX, 0)
                pm.setAttr(shakeCtrl[0].frequencyTransY, 0)
                pm.setAttr(shakeCtrl[0].amplitudeTransX, 0)
                pm.setAttr(shakeCtrl[0].amplitudeTransY, 0)
                pm.setAttr(shakeCtrl[0].frequencyRotX, 0)
                pm.setAttr(shakeCtrl[0].frequencyRotY, 0)
                pm.setAttr(shakeCtrl[0].frequencyRotZ, 0)
                pm.setAttr(shakeCtrl[0].amplitudeRotX, 0)
                pm.setAttr(shakeCtrl[0].amplitudeRotY, 0)
                pm.setAttr(shakeCtrl[0].amplitudeRotZ, 0)
                pm.setAttr(shakeCtrl[0].subFreqRot, 0)
                pm.setAttr(shakeCtrl[0].subAmpRot, 0) 
                pm.select(shakeCtrl[0]) 
            else:
                pm.warning('Please select shake_ctrl')
        except:
            pm.warning('Please select shake_ctrl')
            
    # Bake rigged cam to basic cam
    def bake_basic_cam(self, *args, **kwargs):
        try:
            sel = pm.ls(sl = True)
            camCheck = pm.listRelatives(pm.ls(sl = True), typ = 'camera')
            check = pm.objectType(camCheck[0], isType = 'camera' )
            if check == True:
                minFrame = pm.playbackOptions(q = True, min = True)
                maxFrame = pm.playbackOptions(q = True, max = True)
                newCam = pm.camera()
                pm.setAttr('%s.rotateOrder' %newCam[0], 2)
                masterCtrl = pm.listRelatives(pm.listRelatives(pm.ls(sel[0]), p = True),ad=True, typ='transform')
                masterCtrl = list(OrderedDict.fromkeys(masterCtrl))
                sel.append(masterCtrl[1]) #sel0 = originalCam, sel1 = mainCtrl
                pm.parentConstraint(sel[0], newCam[0])
                pm.bakeResults(newCam[0], t = (minFrame,maxFrame))
                flKeyCount = pm.keyframe(sel[1], at = 'focal_length', q = True, kc = True)
                if flKeyCount >= 1:
                    pm.copyKey(sel[1], at = 'focal_length')
                    pm.pasteKey(newCam[1], at = 'fl')
                else:
                    camFL = pm.getAttr('%s.focal_length' %sel[1])
                    pm.setAttr(newCam[1].fl, camFL)
                pm.delete('%s_parentConstraint1' %newCam[0])
                pm.rename(newCam[0], '%s_camshot' %sel[0]) 
            else:
                pm.warning('Please select the camera')
        except:
            pm.warning('Please select a camera shot')
