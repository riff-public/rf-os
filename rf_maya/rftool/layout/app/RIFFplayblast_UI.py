import pymel.core as pm
import maya.cmds as cmd
import maya.mel as mm
import os
from shutil import copy2
import subprocess

ffmpeg = r"O:\\Pipeline\\legacy\\lib\\ffmpeg\\bin\\ffmpeg.exe"
TMP = "C:\\pbTemp\\pbTemp"

rv = "C:\\Program Files\\Shotgun\\RV-7.2.1\\bin\\rv.exe"
quickTime = "C:\\Program Files (x86)\\QuickTime\\QuickTimePlayer.exe"
mpc = "C:\\Program Files (x86)\\K-Lite Codec Pack\\MPC-HC64\\mpc-hc64.exe"

def UI():
    if pm.window('riffPlayblastWin', exists = True): 
        pm.deleteUI('riffPlayblastWin') 
        
    currentFileLoc = cmd.file(q = True, loc = True)
    try:    
        aList = currentFileLoc.split('/') # split directory heirachy into list for set new playblast directory
        nameOfCurrent = aList[-1]#[:-8]
        bList = nameOfCurrent.split('_')#[:-2]  # split file name each "_" for remove username
        filePathTxt = aList[0]+'/'+aList[1]+'/'+aList[2]+'/'+aList[3]+'/'+aList[4]+'/'+aList[5]+'/'+'output'+'/'+'preview'+'/'
        filePathExp = aList[0]+'\\'+aList[1]+'\\'+aList[2]+'\\'+aList[3]+'\\'+aList[4]+'\\'+aList[5]+'\\'+'output'+'\\'+'preview'
        # publish
        filePathPubTxt = aList[0]+'/'+aList[1]+'/'+aList[2]+'/'+aList[3]+'/'+aList[4]+'/'+'preview'+'/'
        filePathPubExp = aList[0]+'\\'+aList[1]+'\\'+aList[2]+'\\'+aList[3]+'\\'+aList[4]+'\\'+'preview'
        pbFilePubTxt = bList[0]+'_'+bList[1]+'.mov'
    except: # if file directory is incorrect
        filePathTxt = '[none]'
        filePathPubTxt = '[none]'
        filePathExp = 'explorer'
        filePathPubExp = 'explorer'
        pbFilePubTxt = ''
    
    pubFiles = pm.getFileList(folder = filePathPubTxt, fs = '*.mp4')
    verFiles = pm.getFileList(folder = filePathTxt, fs = '*.mp4')
        
    def overrideFileNameChkOn(*args):
        fileNameTxtFV.setEnable(True)
        
    def overrideFileNameChkOff(*args):
        pm.textField('fileNameTxtF', e = True, tx = pbFilePubTxt)
        fileNameTxtFV.setEnable(False)

    def useTimerangeChk(*args):
        if sqStartEndChk.getSelect() == False:
            startIntF.setEnable(False)
            endIntF.setEnable(False)
            pm.optionMenu('codecCombo', e = True, sl = 1)
        else:
            startIntF.setEnable(True)
            endIntF.setEnable(True)
            pm.optionMenu('codecCombo', e = True, sl = 2)
            
    def useSelShotChk(*args):
        if selShotChk.getSelect() == False:
            pm.optionMenu('codecCombo', e = True, sl = 1)
        else:
            pm.optionMenu('codecCombo', e = True, sl = 2)
                 
    def runVerExplorer(*args):
        os.startfile(filePathExp)
        
    def runPubExplorer(*args):
        pm.sysFile('%s' %filePathPubExp, makeDir = True)
        os.startfile(filePathPubExp)
        
    pm.window('riffPlayblastWin', title = 'Monday Playblast for Shotgun v1.0', ret = True, rtf = True, sizeable = True, mxb = False)
    main = pm.columnLayout(adjustableColumn = True, rs = 5, cat = ('both',5))
    pm.separator(style = 'none', h = 2, p = main)

    playerRow = pm.rowLayout(nc = 2, adj = 2, cat = [(1,'both',5),(2,'both',5),(3,'both',5)], p = main)
    pm.text(l = 'Select player: ', p = playerRow)
    pm.optionMenu('playerSel', p = playerRow)
    pm.menuItem('quickTime', l = 'QuickTime')
    pm.menuItem('rv', l = 'RV')
    pm.menuItem('mpc', l = 'Media Player Classic')
    #pm.button(l = 'Refresh List', w = 105, p = playerRow, c = lambda x: refreshList(filePathTxt, filePathPubTxt))

    fileListRow = pm.rowLayout(nc = 2, adj = 2, cat = [(1,'both',5),(2,'both',5)], p = main)
    pubFileListCol = pm.columnLayout(rs = 5, p = fileListRow)
    pubTab = pm.rowLayout(nc = 2, adj = 1, p = pubFileListCol)
    pm.text(l = 'Publish files: ', al = 'left', w = 165, p = pubTab)
    pm.button(l = 'Open Explorer', w = 100, p = pubTab, c = runPubExplorer)   
    pubFileList = pm.textScrollList('pubListsTS', ams = True, w = 268, p = pubFileListCol)
    pubBtnRow = pm.rowLayout(nc = 2, p = pubFileListCol)
    pm.button(l = 'Play', w = 132, p = pubBtnRow, c = lambda x: playPubButton(filePathPubTxt))
    pm.button(l = 'Insert to Shot', w = 132, p = pubBtnRow, c = lambda x: insPubToShot(filePathPubTxt))
            
    verFileListCol = pm.columnLayout(rs = 5, p = fileListRow)
    verTab = pm.rowLayout(nc = 2, adj = 1, p = verFileListCol)
    pm.text(l = 'Version files: ', al = 'left', w = 165, p = verTab)
    pm.button(l = 'Open Explorer', w = 100, p = verTab, c = runVerExplorer)  
    verFileList = pm.textScrollList('verListsTS', ams = True, w = 268, p = verFileListCol)
    verBtnRow = pm.rowLayout(nc = 2, p = verFileListCol)
    pm.button(l = 'Play', w = 132, p = verBtnRow, c = lambda x: playVerButton(filePathTxt))
    pm.button(l = 'Delete', w = 132, p = verBtnRow, c = lambda x: deleteVerButton(filePathTxt))

    grid1 = pm.rowColumnLayout(nc = 3, cat = [(1,'both',5),(2,'both',5),(3,'both',5)], rs = (1,5), cw = [(1,80),(2,360)], cal = [(1,'right'),(2,'left')], p = main)

    pm.text(l = 'File name:', p = grid1)
    fileNameTxtFV = pm.textField('fileNameTxtF', tx = pbFilePubTxt, en = False, p = grid1)
    pm.checkBox(l = 'Override file name', p = grid1, onc = overrideFileNameChkOn, ofc = overrideFileNameChkOff)
    
    pm.text(l = ' ', p = grid1)
    viewChk = pm.checkBox('viewChk', l = 'View', v = True, p = grid1)
    pm.text(l = ' ', p = grid1) 

    pm.text(l = ' ', p = grid1)
    ornamentsChk = pm.checkBox('ornamentsChk', l = 'Show ornaments', v = True, p = grid1)
    pm.text(l = ' ', p = grid1) 
    
    pm.text(l = ' ', p = grid1) 
    overScanChk = pm.checkBox('overScanChk', l = 'Remove overscan', p = grid1)
    pm.text(l = ' ', p = grid1)  

    pm.text(l = 'Play mode:', p = grid1)
    row2 = pm.rowLayout(nc = 4, p = grid1)
    rPbModeRdo = pm.radioCollection('rPbModeRdo', p = row2)
    pm.radioButton('A', l = 'Timeline', sl = True, p = row2)
    pm.radioButton('B', l = 'Selection', p = row2)
    selShotChk = pm.radioButton('C', l = 'Selected shots', p = row2, cc = useSelShotChk)
    sqStartEndChk = pm.radioButton('D', l = 'Sequence Start/End', p = row2, cc = useTimerangeChk)
    pm.text(l = ' ', p = grid1)
    
    pm.text(l = ' ', p = grid1)
    sepShotChk = pm.radioButton('E', l = 'Selected shots (separation)', p = grid1, cc = useSelShotChk)
    pm.text(l = ' ', p = grid1)
    
    pm.text(l = 'Start time:', p = grid1)
    row3 = pm.rowLayout(nc = 3, p = grid1)
    startIntF = pm.intField('startIntF', v = 1, en = False, p = row3)
    pm.text(l = ' ', p = grid1)
    
    pm.text(l = 'End time:', p = grid1)
    row4 = pm.rowLayout(nc = 3, p = grid1)
    endIntF = pm.intField('endIntF', v = 100, en = False, p = row4)
    pm.text(l = ' ', p = grid1)
    
    pm.text( l = 'Resolution:', p = grid1)
    resCombo = pm.optionMenu('resCombo', p = grid1)
    pm.menuItem(l = '1920 x 1080 (Full HD 100%)')
    pm.menuItem(l = '1280 x 720 (HD 100%)')
    pm.menuItem(l = '1920 x 800 (Anamorphic 100%)')
    pm.menuItem(l = '960 x 540 (Full HD 50%)')
    pm.menuItem(l = '960 x 400 (Anamorphic 50%)')
    pm.text(l = ' ', p = grid1)
    
    pm.text( l = 'Codec:', p = grid1)
    codecCombo = pm.optionMenu('codecCombo', p = grid1)
    pm.menuItem(l = 'FFmpeg (Smaller File Size)')
    pm.menuItem(l = 'H.264 (Original Codec)')
    pm.text(l = ' ', p = grid1)
    
    pm.text(l = ' ', p = grid1)
    ovrwChk = pm.checkBox('ovrwChk', l = 'Overwrite latest version', p = grid1)
    pm.text(l = ' ', p = grid1)
    
    pm.text(l = ' ', p = grid1)
    pubChk = pm.checkBox('pubChk', l = 'Publish', p = grid1)
    pm.text(l = ' ', p = grid1)
        
    pm.button(l = 'Playblast', p = main, c = lambda x: doPlayblast(filePathTxt, filePathPubTxt))                                                                
    pm.separator(style = 'none', p = main)
    
    refreshList()
    
    pm.showWindow('riffPlayblastWin')
   
def doPlayblast(_filePathTxt, _filePathPubTxt):
    filePathTxt = _filePathTxt
    filePathPubTxt = _filePathPubTxt
    player = playerSelector()
    
    fileNameTxtFV = pm.textField('fileNameTxtF', q = True, tx = True)
    viewChk = pm.checkBox('viewChk', q = True, v = True)
    overScanChk = pm.checkBox('overScanChk', q = True, v = True)
    ornamentsChk = pm.checkBox('ornamentsChk', q = True, v = True)
    rPbModeRdo = pm.radioCollection('rPbModeRdo', q = True, sl = True)
    startIntF = pm.intField('startIntF', q = True, v = True)
    endIntF = pm.intField('endIntF', q = True, v = True)
    resCombo = pm.optionMenu('resCombo', q = True, v = True)
    codecCombo = pm.optionMenu('codecCombo', q = True, v = True)
    ovrwChk = pm.checkBox('ovrwChk', q = True, v = True)
    pubChk = pm.checkBox('pubChk', q = True, v = True)
    
    currentFileLoc = cmd.file(q = True, loc = True)
    aList = currentFileLoc.split('/') # split directory heirachy into list for set new playblast directory
    nameOfCurrent = aList[-1][:-8] # file name remove version number
    #cList = nameOfCurrent.split('_') # split file name to get user
    #usrFileName = cList[-1] # get user
    origNaming = fileNameTxtFV.replace('.mov','') # remove .mov to add user name
    selShots = pm.ls(sl = True, typ = 'shot')    
    timeCtrl = mm.eval('$tmpVar=$gPlayBackSlider')
    currSound = pm.timeControl(timeCtrl, q = True, s = True)
    
    try:
        b = []
        fileList = pm.getFileList(folder = filePathTxt)
        for i in fileList: # find v.number in files list
            a = i.split('_')
            b.append(a[-1]) # keep v.number in "b"
        lastVerNum = int(max(b)[1:-4]) # get latest version number
        if ovrwChk == True:           
            outputName = origNaming+'_v%03d'%(lastVerNum)+'.mov'
        elif ovrwChk == False:
            outputName = origNaming+'_v%03d'%(lastVerNum+1)+'.mov'
    except:
        outputName = origNaming+'_v001'+'.mov'
    
    if rPbModeRdo == 'A':
        start = pm.playbackOptions(q = True, min = True)
        end = pm.playbackOptions(q = True, max = True)
        sqTime = False
    elif rPbModeRdo == 'B':
        start, end = pm.timeControl(timeCtrl, q = True, rangeArray = True)
        sqTime = False
    elif rPbModeRdo == 'C':
        selShot = pm.ls(sl=True, typ='shot')
        n = len(selShot)        
        start = pm.shot(selShot[0], q = True, sst = True)
        end = pm.shot(selShot[n-1], q = True, set = True)
        sqTime = True
    elif rPbModeRdo == 'D':
        start = startIntF
        end = endIntF
        sqTime = True
        
    if resCombo == '1920 x 1080 (Full HD 100%)':
        width = 1920
        height = 1080
    elif resCombo == '1920 x 800 (Anamorphic 100%)':
        width = 1920
        height = 800
    elif resCombo == '1280 x 720 (HD 100%)':
        width = 1280
        height = 720
    elif resCombo == '960 x 540 (Full HD 50%)':
        width = 960
        height = 540
    elif resCombo == '960 x 400 (Anamorphic 50%)':
        width = 960
        height = 400

    if overScanChk == 1: # get each overscan value and set to 1
        camMCtrlLs = pm.ls('master_ctrl')
        ovrscn = []
        for i in camMCtrlLs:
            try:
                ovrscn.append(pm.getAttr(i.overscan))
                if overScanChk == True:
                    pm.setAttr(i.overscan, 1)
            except:
                pass       
    
    clearHUD()   
    
    if rPbModeRdo == 'E': ##############################################        
        for i in selShots:
           
            sStart = pm.shot(i, q = True, sst = True)
            sEnd = pm.shot(i, q = True, set = True)
            departName = nameOfCurrent.split('_')[-1]
            outputShotName = i + '_' + departName
            
            pm.playblast (startTime      = sStart,
            			  endTime        = sEnd,
            			  sequenceTime   = True,
            			  filename       = "%sshots/%s" %(filePathTxt, outputShotName),
            			  showOrnaments  = ornamentsChk,
            			  widthHeight    = (width, height),
            			  sound          = currSound,
            			  viewer         = False,
            			  offScreen      = True,
            			  percent        = 100,
            			  quality        = 100,
            			  forceOverwrite = True,
            			  clearCache     = True,
            			  compression    = "H.264",
            			  format         = "qt")
            			  
        if pubChk == True:
            pm.sysFile('%sshots' %filePathPubTxt, makeDir = True)
            copy2("%sshots/%s" %(filePathTxt, outputShotName), "%sshots/%s" %(filePathPubTxt, outputShotName))
        			  
    else:     ########################################################### 
        range = (end - start)+1
        pm.headsUpDisplay('HudSCN', s = 5, b = 0, bs = 'small', l = 'Scene Name  :  ')
        pm.headsUpDisplay('HudFRN', s = 9, b = 0, bs = 'small', l = 'Frame Range  :   %s  -  %s  ( %s )' %(int(start),int(end),int(range)))
        pm.headsUpDisplay('HudFNB', s = 9, b = 1, bs = 'small', l = 'Frame Number  :', c = "pm.currentTime(q = True)", atr = True)
        
        if codecCombo == 'H.264 (Original Codec)':
            pm.playblast (startTime      = start,
            			  endTime        = end,
            			  sequenceTime   = sqTime,
            			  filename       = "%s%s" %(filePathTxt, outputName),
            			  showOrnaments  = ornamentsChk,
            			  widthHeight    = (width, height),
            			  sound          = currSound,
            			  viewer         = False,
            			  offScreen      = True,
            			  percent        = 100,
            			  quality        = 100,
            			  forceOverwrite = True,
            			  clearCache     = True,
            			  compression    = "H.264",
            			  format         = "qt")
            			  
        elif codecCombo == 'FFmpeg (Smaller File Size)':
            try:
                os.remove(TMP)        
            except:
                pass
            pm.playblast (startTime      = start,
            			  endTime        = end,
            			  sequenceTime   = sqTime,
            			  filename       = TMP,
            			  showOrnaments  = ornamentsChk,
            			  widthHeight    = (width, height),
            			  sound          = currSound,
            			  viewer         = False,
            			  offScreen      = True,
            			  percent        = 100,
            			  quality        = 100,
            			  forceOverwrite = True,
            			  clearCache     = True,
            			  compression    = "H.264",
            			  format         = "qt")
                          
            fpsMaya = cmd.currentUnit(q=True, time=True)
            if 'fps' in fpsMaya:
                fps = fpsMaya.replace("fps", "")
            else:
                fpsList = {
                    "game" : 15,
                    "film" : 24,
                    "pal"  : 25,
                    "ntsc" : 30,
                    "show" : 48,
                    "palf" : 50,
                    "ntscf" : 60,
                }
                try:
                    fps = fpsList[fpsMaya]
                except:
                    fps = 30
            
            ffmpegCommand = "%s -y -i \"%s.mov\" -r %s " % (ffmpeg, TMP, fps)
            ffmpegCommand += "-vcodec libx264 -vprofile baseline -crf 22 -bf 0 "
            ffmpegCommand += "-pix_fmt yuv420p -f mov "
            ffmpegCommand += "%s%s" %(filePathTxt, outputName)        
            subprocess.call(ffmpegCommand)
    
        if pubChk == True:
            pm.sysFile('%s' %filePathPubTxt, makeDir = True)
            copy2("%s%s" %(filePathTxt, outputName), "%s%s" %(filePathPubTxt, fileNameTxtFV))
            
        if viewChk == True:
            subprocess.Popen([player, filePathTxt + "\\" + outputName])
        
    if overScanChk == 1: # set overscan to old value  
        try:			  
            for i in camMCtrlLs:
                pm.setAttr(i.overscan, ovrscn[n])
                n += 1
        except:
            pass

    clearHUD()    
    # refresh file list
    refreshList()
    
def refreshList(*args):
    currentFileLoc = cmd.file(q = True, loc = True)
    try:    
        aList = currentFileLoc.split('/') # split directory heirachy into list for set new playblast directory
        nameOfCurrent = aList[-1]#[:-8]
        bList = nameOfCurrent.split('_')#[:-2]  # split file name each "_" for remove username
        filePathTxt = aList[0]+'/'+aList[1]+'/'+aList[2]+'/'+aList[3]+'/'+aList[4]+'/'+aList[5]+'/'+'output'+'/'+'preview'+'/'
        filePathExp = aList[0]+'\\'+aList[1]+'\\'+aList[2]+'\\'+aList[3]+'\\'+aList[4]+'\\'+aList[5]+'\\'+'output'+'\\'+'preview'
        # publish
        filePathPubTxt = aList[0]+'/'+aList[1]+'/'+aList[2]+'/'+aList[3]+'/'+aList[4]+'/'+'preview'+'/'
        filePathPubExp = aList[0]+'\\'+aList[1]+'\\'+aList[2]+'\\'+aList[3]+'\\'+aList[4]+'\\'+'preview'
        pbFilePubTxt = bList[0]+'_'+bList[1]+'.mov'
    except: # if file directory is incorrect
        filePathTxt = '[none]'
        filePathPubTxt = '[none]'
        filePathExp = 'explorer'
        filePathPubExp = 'explorer'
        pbFilePubTxt = ''
   
    pubFiles = pm.getFileList(folder = filePathPubTxt)
    verFiles = pm.getFileList(folder = filePathTxt)    
   
    pm.textScrollList('pubListsTS', e = True, ra = True)
    pm.textScrollList('verListsTS', e = True, ra = True)
    
    if not pubFiles:
        pass
    else:        
        pubFiles.sort(key = lambda k : k.lower())
        pm.textScrollList('pubListsTS', e = True, append = pubFiles)
    if not verFiles:
        pass
    else:  
        verFiles.sort(key = lambda k : k.lower())
        pm.textScrollList('verListsTS', e = True, append = verFiles)   
        
    pm.textField('fileNameTxtF', e = True, tx = pbFilePubTxt)
    
def playerSelector(*args):
    playerLabel = pm.optionMenu('playerSel', q = True, sl = True)
    
    if playerLabel == 1:
        player = eval('quickTime')
    elif playerLabel == 2:
        player = eval('rv')
    elif playerLabel == 3:
        player = eval('mpc')   
    
    return player
     
def playPubButton(filePathPubTxt, *args):
    item = pm.textScrollList('pubListsTS', q = True, selectItem = True)
    player = playerSelector()
    files = [player]        

    if item:
        for i in item:
            files.append(filePathPubTxt + "\\" + i)
        subprocess.Popen(files)
    else:
        mc.warning('No Select List')
        
def insPubToShot(filePathPubTxt, *args):
    item = pm.textScrollList('pubListsTS', q = True, selectItem = True)[0]
    shotList = pm.ls(sl = True, typ = 'shot')
    if not shotList:
        cam = pm.camera()[0]
        selShot = pm.shot(cc = cam)
    else:
        selShot = shotList[0]
    st = pm.shot(selShot, q = True, st = True)
    clipFrameLen = pm.movieInfo('%s%s' %(filePathPubTxt, item), f = True)[0]
    endTime = (st + int(clipFrameLen)) - 1
    
    pm.shot(selShot, e = True, et = endTime)
    pm.shot(selShot, e = True, cl = '%s%s' %(filePathPubTxt, item)) # add publish clip
    
    camCurrShot = pm.shot(selShot, q = True, cc = True)
    
    pm.setAttr("%s.displayResolution" %camCurrShot, 1)
    pm.setAttr("%s.displayGateMaskOpacity" %camCurrShot, 1)
    mm.eval('setAttr "%s.displayGateMaskColor" -type double3 0 0 0 ;' %camCurrShot)
        
def playVerButton(filePathTxt, *args):
    item = pm.textScrollList('verListsTS', q = True, selectItem = True)[0]
    player = playerSelector()
    
    if item:
        subprocess.Popen([player, filePathTxt + "\\" + item])
    else:
        mc.warning('No Select List')          
            
def deleteVerButton(filePathTxt, *args) :
    confirm = pm.confirmDialog(t = 'Delete file', m = 'Do you want to delete?', b = ['Yes','No'], db = 'Yes', cb = 'No', ds = 'No')
    if confirm == 'Yes':
        items = pm.textScrollList('verListsTS', q = True, selectItem = True)
        if items:
            for item in items:
                os.remove(filePathTxt + "\\" + item) 
        else:
            mc.warning('No Select List')        
        refreshList()
    elif confirm == 'No':
        pass
        
def clearHUD(*args):
    pm.headsUpDisplay(rp = (5,0))
    pm.headsUpDisplay(rp = (5,1))
    pm.headsUpDisplay(rp = (9,0))
    pm.headsUpDisplay(rp = (9,1))
