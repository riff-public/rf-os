import pymel.core as pm
from maya.cmds import file
import maya.cmds as mc


import maya.OpenMaya as om
import maya.OpenMayaUI as omu

from rf_maya.rftool.layout.split_shot import split_shot_cmd
reload(split_shot_cmd)

#UI:
#-set output file directory
# -save to anim
#-playblast settings
# -on/off
# -resolution
#-set audio path
# -on/off
def splitUI():
    if pm.window('splitShot', exists = True): 
        pm.deleteUI('splitShot')
        
    def filePathDisplay(*args):
        _pathInput = pm.textField(pathInput, q = True, tx = True)
        _pathInputCorrect = _pathInput.replace("\\", "/")
        pm.text(pathPreview, e = True, l = '%s/anim/[shot]/version/[shot]_anim_v001.ma' %_pathInputCorrect)
        pm.text(pbPathPreview, e = True, l = '%s/anim/preview/[shot]_anim.mov' %_pathInputCorrect)
        
    def getAnimPath(*args):
        animPath = pm.fileDialog2(fm = 3, ds = 2, ff = "directory", okc = "Select")
        try:
            pm.textField(pathInput, e = True, text = '%s' %animPath[0])
            filePathDisplay()
        except:
            pass
            
    def intAnimPath(*args):
        try:
            loc = file(q = True, loc = True).split('/')
            animPath = loc[0] + '/' + loc[1] + '/' + loc[2] + '/' + loc[3]
            pm.text(pathPreview, e = True, l = '%s/anim/[shot]/version/[shot]_anim_v001.ma' %animPath)
            pm.text(pbPathPreview, e = True, l = '%s/anim/preview/[shot]_anim.mov' %animPath)
            pm.textField(pathInput, e = True, text = '%s' %animPath)
        except:
            pass
        
    def getAudioPath(*args):
        audioPath = pm.fileDialog2(fm = 3, ds = 2, ff = "directory", okc = "Select")
        try:
            pm.textField(audioPathInput, e = True, text = '%s' %audioPath[0])
        except:
            pass
            
    def audioChkOn(*args):
        audioPathTxt.setEnable(True)
        audioPathInput.setEnable(True)
        audioPathBtn.setEnable(True)
        
    def audioChkOff(*args):
        audioPathTxt.setEnable(False)
        audioPathInput.setEnable(False)
        audioPathBtn.setEnable(False)
        
    def playblastChkOn(*args):
        pbResTxt.setEnable(True)
        pbResCombo.setEnable(True)
        pbPathPreviewTxt.setEnable(True)
        
    def playblastChkOff(*args):
        pbResTxt.setEnable(False)
        pbResCombo.setEnable(False)
        pbPathPreviewTxt.setEnable(False)


    item = mc.ls('cam_grp')
    if not item == []:
        mc.hide(item)
        splitShot = pm.window('splitShot', title = 'Split Shot v0.1', rtf = True, sizeable = True, mxb = False)
        main = pm.columnLayout(adjustableColumn = True, rs = 5, cat = ('both',5))
        pm.separator(style = 'none', h = 3, p = main)
        row1 = pm.rowLayout(nc = 3, cw3 = (70,100,70), adj = 2, cal = (1,'right'), cat = [(1,'both',3),(2,'both',3),(3,'both',3)], p = main)
        pm.text(l = 'Scene path:', p = row1)
        pathInput = pm.textField(aie = True, ec = filePathDisplay, p = row1)
        pm.button(l = 'Browse', w = 70, p = row1, c = getAnimPath)

        row2 = pm.rowLayout(nc = 3, cw3 = (70,100,70), adj = 2, cal = (1,'right'), cat = [(1,'both',3),(2,'both',3),(3,'both',3)], p = main)
        pm.text(l = 'File save:', p = row2)
        pathPreview = pm.text(l = '[none]', al = 'left', en = False, p = row2)


        pm.separator(style = 'none', h = 2, p = main)
        pm.separator(style = 'in', p = main)
         
        row3 = pm.rowLayout(nc = 3, cw3 = (70,100,70), adj = 2, cal = (1,'right'), cat = [(1,'both',3),(2,'both',3),(3,'both',3)], p = main)
        pm.text(l = ' ', p = row2)
        audioChk = pm.checkBox(l = 'Import shots audio', p = row3, onc = audioChkOn, ofc = audioChkOff)
            
        row4 = pm.rowLayout(nc = 3, cw3 = (70,100,70), adj = 2, cal = (1,'right'), cat = [(1,'both',3),(2,'both',3),(3,'both',3)], p = main)
        audioPathTxt = pm.text(l = 'Audio path:', p = row3, en = False)
        audioPathInput = pm.textField(p = row3, en = False)
        audioPathBtn = pm.button(l = 'Browse', w = 70, p = row4, c = getAudioPath)

        pm.separator(style = 'none', h = 2, p = main)
        pm.separator(style = 'in', p = main)
            
        row5 = pm.rowLayout(nc = 3, cw3 = (70,100,70), adj = 2, cal = (1,'right'), cat = [(1,'both',3),(2,'both',3),(3,'both',3)], p = main)
        pm.text(l = ' ', p = row4)
        playblastChk = pm.checkBox(l = 'Playblast', p = row5, onc = playblastChkOn, ofc = playblastChkOff)
            
        row6 = pm.rowLayout(nc = 3, cw3 = (70,100,70), adj = 2, cal = (1,'right'), cat = [(1,'both',3),(2,'both',3),(3,'both',3)], p = main)
        pbResTxt = pm.text(l = 'Resolution:', p = row6, en = False)
        pbResCombo = pm.optionMenu(p = row5, en = False)   
        pm.menuItem(l = '960 x 400 (Anamorphic 50%)')
        pm.menuItem(l = '960 x 540 (Full HD 50%)')
        pm.menuItem(l = '1920 x 800 (Anamorphic 100%)')
        pm.menuItem(l = '1920 x 1080 (Full HD 100%)')

        row7 = pm.rowLayout(nc = 3, cw3 = (70,100,70), adj = 2, cal = (1,'right'), cat = [(1,'both',3),(2,'both',3),(3,'both',3)], p = main)
        pbPathPreviewTxt = pm.text(l = 'Output path:', p = row7, en = False)
        pbPathPreview = pm.text(l = '[none]', al = 'left', en = False, p = row6)
            
        pm.separator(style = 'none', h = 2, p = main)
        pm.separator(style = 'in', h = 5, p = main)

        pm.button(l = 'Split', p = main, c = lambda x: saveFile(pathInput, audioPathInput, audioChk, playblastChk, pbResCombo))
        pm.separator(style = 'none', h = 3, p = main)

        pm.showWindow(splitShot)

        intAnimPath()
    else:
        mc.confirmDialog(title='InsertCam', button = 'OK', message = 'Insert cam_grp')

                
    

            
#"save file"
#-save to directory by settings
# -create directory --> save file
# -[project]\scene\anim\[shot]\ hero,version,output

def shift_frame(*args):
        split_shot_cmd.shiftShot2()

def select_viewport_objects(): 
    import maya.OpenMaya as om
    import maya.OpenMayaUI as omUI
    view = omUI.M3dView.active3dView()
    om.MGlobal.selectFromScreen(0,0,view.portWidth(),view.portHeight(),om.MGlobal.kAddToList)
    sel = mc.ls(sl=True)
    return sel

def saveFile(_pathInput, _audioPath, _importAudioChk, _playblastChk, _pbResCombo, *args, **kwargs):
    pathInput = pm.textField(_pathInput, q = True, tx = True)
    audioPath = pm.textField(_audioPath, q = True, tx = True)
    audioChk = pm.checkBox(_importAudioChk, q = True, v = True)
    playblastChk = pm.checkBox(_playblastChk, q = True, v = True)
    pbResCombo = pm.optionMenu(_pbResCombo, q = True, v = True)
    
    sequence = mc.sequenceManager(q=True, ws=True)
    pathInputCorrect = pathInput.replace("\\", "/")
    shots = mc.sequenceManager(lsh=True)

    try:
        for iShotName in shots:
            # go to current shot, set start & end frame each shot
            split_shot_cmd.shiftShot2(iShotName)
            currentCameraShape = mc.shot(iShotName, q=True, currentCamera=True)
            currentCamera = mc.listRelatives(currentCameraShape, parent = True)
            print currentCameraShape
            print currentCamera
            print iShotName
            sqStFrame = 100
            mc.sequenceManager(currentTime = 101)
            # item = mc.ls('cam_grp')
            # if not item == []:
            #     mc.hide(item)
            # else:
            #     mc.confirmDialog(button = ['OK'], massage =('Insert cam_grp'))

            start = mc.shot(iShotName, q = True, startTime = True)
            end = mc.shot(iShotName, q = True, endTime = True)
            mc.playbackOptions(ast = start, min = start, aet = end, max = end)
            mc.lookThru(currentCamera[0])
            mc.select(clear = True)

            for frame in range(int(start),int(end)):
                mc.currentTime(frame)
                if frame % 5 == 0:
                    select_viewport_objects() 
                    list = mc.ls(sl=True)
                    mc.select(list, add = True)
                elif frame % 5 == 0:
                    select_viewport_objects() 
                    list = mc.ls(sl=True)
                    mc.select(list, add = True)


            # GET A LIST OF REFERENCES
            mc.select(iShotName, add = True)
            mc.select(mc.listRelatives(currentCamera[0], p = True), add = True )  

            # import audio
            if audioChk == True:
                sounds = importAudio(audioPath, iShotName, start)
                print sounds
                mc.select(sounds, add = True)

            else:
                pass

           #  ExportSelection
            print mc.ls(sl=True)
            pm.sysFile('%s/anim/%s/version' %(pathInputCorrect, iShotName), makeDir = True)
            pathFile = '%s/anim/%s/version/%s_anim_v001.ma' %(pathInputCorrect, iShotName, iShotName)
            pm.exportSelected(pathFile, preserveReferences = True)
            print start,end
            writeFileMaSenceConfig(pathFile, start, end)
            writeFileMaShot(pathFile, iShotName)
            #mc.file('%s/anim/%s/version/%s_anim_v001.ma' %(pathInputCorrect, iShotName, iShotName), force = True, options ="v=0;", exportSelected=True, typ='mayaAscii', pr=True);

             # create directory
            
            # save file
            # cmd.file(rename = )
            # cmd.file(save = True, type = "mayaAscii")


            # playblast
            if playblastChk == True:
                pm.sysFile('%s/anim/preview' %(pathInputCorrect), makeDir = True)
                doPlayblast(pbResCombo, pathInputCorrect, iShotName)
            else:
                pass     
    except KeyboardInterrupt:
        pass   



#"import audio"
#-read files from directory
#-check if it have file match to the shot
# -Yes: import
# -No: skip
def importAudio(_audioPath, _shotName, _startTime, *args, **kwargs):

    _audioPathCorrect = _audioPath.replace("\\", "/")
    fileList = pm.getFileList(folder = "%s" %_audioPathCorrect)
    pm.delete(pm.ls(type = 'audio'))
    for iAudioFile in fileList:
        if '%s' %_shotName in iAudioFile:
            sound =  pm.sound(file = "%s/%s" %(_audioPathCorrect, iAudioFile), n = '%s_sound' %_shotName, offset = _startTime)
        else:
            pass    
    return sound

#"playblast"
#-playblast by settings & save to default directory
# -[project]\scene\anim\[shot]\output

def writeFileMaSenceConfig(pathFile, start, end):
    file = open(pathFile,"a") 
    senceConfig = 'createNode script -name "sceneConfigurationScriptNode";setAttr ".before" -type "string" "playbackOptions -min %s -max %s -ast %s -aet %s ";setAttr ".scriptType" 6;' % (start, end, start, end)
    file.write(senceConfig) 
    file.close()
def writeFileMaShot(pathFile, shotName):
    file = open(pathFile,"a") 
    createNode = '\ncreateNode sequencer -n "sequencer1";'
    senceConfig = '\nconnectAttr "%s.se" "sequencer1.shts" -na;'%(shotName)
    mayaNode = '\nconnectAttr "%s.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[2].dn";'%(shotName)
    file.write(createNode)
    file.write(mayaNode)
    file.write(senceConfig) 
    file.close()


def doPlayblast(_pbRes, _outputPath, _shotName, *args, **kwargs):
    if _pbRes == '960 x 400 (Anamorphic 50%)':
        width = 960
        height = 400
    elif _pbRes == '960 x 540 (Full HD 50%)':
        width = 960
        height = 540
    elif _pbRes == '1920 x 800 (Anamorphic 100%)':
        width = 1920
        height = 800
    elif _pbRes == '1920 x 1080 (Full HD 100%)':
        width = 1920
        height = 1080
    pm.playblast( widthHeight = (width,height), 
              filename = "%s/anim/preview/%s_anim.mov" %(_outputPath, _shotName), 
              sound = '%s_sound' %_shotName, 
              format = 'qt', 
              compression = "H.264",
              quality = 70,
              percent = 100,
              forceOverwrite = True,
              sequenceTime = False,
              clearCache = True,
              viewer = False,
              showOrnaments = False,
              offScreen = True )
    
def splitShot_UI():
    splitUI()

