import maya.cmds as mayac
import pymel.core as pm
import rftool.layout.temp.DJB_Character as DJB_Character
from rftool.layout.temp import xbotAnimTransfer as at

xbotScene = "O:/Pipeline/core/rf_maya/rftool/layout/resources/mocap/xbot.ma"

def UI():
    if pm.window('transferMixamoAnimWin', exists = True):
        pm.deleteUI('transferMixamoAnimWin')
        
    pm.window('transferMixamoAnimWin', title = 'Transfer MIXAMO Mocap b0.01', rtf = True, mxb = False)
    pm.columnLayout(adj = True, rs = 5)
    
    pm.text(l = '1. New scene, then import mocap file')
    pm.button(l = 'Import Mocap File', c = importAnim)
    pm.text(l = '2. Reference character(s) that you desire')
    pm.text(l = '3. Click "Tranfer Anim"')
    pm.button(l = 'Tranfer Anim', c = tranferAnim)
    pm.text(l = '4. Do you wanna remove xbot? Click "Remove xbot"')
    pm.button(l = 'Remove xbot', c = deleteXbot)
    
    pm.showWindow('transferMixamoAnimWin')
    
def importAnim(arg = None, importFile = None):  
    pm.importFile(xbotScene) 
    DJB_CharacterInstance = DJB_Character.Character.createCharacterClassInstance()
    stuffInScene = mayac.ls()
    if not DJB_CharacterInstance:
        OpenMaya.MGlobal.displayError("You must rig a character first")
    if not importFile:
        importFile = mayac.Import()
    if importFile:
        mayac.file(importFile, i=True, force=True)
    stuffInSceneAfterImport = mayac.ls()
    if len(stuffInSceneAfterImport)-len(stuffInScene):
        allJoints = mayac.ls(type="joint")
        HipsJoint = None
        for joint in allJoints:
            if ("Hips" in joint or "Root" in joint) and "Bind" not in joint and "AnimData" not in joint and "IK_Dummy" not in joint:
                HipsJoint = joint
                break
        if not HipsJoint:
            OpenMaya.MGlobal.displayError("No compatible skeleton found in imported file.")
    
        if len(DJB_CharacterInstance) == 1:
            if DJB_CharacterInstance[0].Hips.Bind_Joint:
                isCorrectRig = DJB_CharacterInstance[0].checkSkeletonProportions(HipsJoint)
                if isCorrectRig:
                    DJB_CharacterInstance[0].transferMotionToAnimDataJoints(HipsJoint, newStartTime = 0, mixMethod = "insert")
                    DJB_CharacterInstance = DJB_Character.Character.createCharacterClassInstance()
                    if not DJB_CharacterInstance:
                        OpenMaya.MGlobal.displayError("No Character Found!")
                    else:
                        if DJB_CharacterInstance[0].origAnim:
                            DJB_CharacterInstance[0].deleteOriginalAnimation()
                        else:
                            OpenMaya.MGlobal.displayError("No Original Animation Found!")
                else:
                    OpenMaya.MGlobal.displayError("Imported Skeleton does not match character!")
        else:
            mayac.select(HipsJoint, r=True)
            ACR_copyAnimationToRigWindow()
            self.deleteOrigAnimation_function()
            
    DJB_CharacterInstance = DJB_Character.Character.createCharacterClassInstance()
    if DJB_CharacterInstance:
        DJB_CharacterInstance[0].bakeAnimationToControls()
    else:
        OpenMaya.MGlobal.displayError("No Character Found!")
    
def tranferAnim(*args):
    xbotCtrlList = at.xbotCtrl()
    charCtrlList = at.charCtrl()
    localCtrlList = [4,5,11,15]
    for i in localCtrlList:
        sel = pm.ls(charCtrlList[i], r = True)
        for j in sel:
            pm.setAttr('%s.localWorld' %j, 0)
    worldCtrlList = [7,9]
    for i in worldCtrlList:
        sel = pm.ls(charCtrlList[i], r = True)
        for j in sel:
            pm.setAttr('%s.localWorld' %j, 1)
    for i in range(47):
        selFrom = pm.ls(xbotCtrlList[i], r = True)
        selTo = pm.ls(charCtrlList[i], r = True)
        pm.setAttr('%s.rotateOrder' %selTo[0], 0)
        if i >= 0 and i < 6:
            pm.copyKey(selFrom)
            pm.pasteKey(selTo)
        elif 6 <= i < 8:
            pm.copyKey(selFrom)
            pm.pasteKey(selTo)
            pm.scaleKey(selTo, at = 'rx', vs = -1)
            pm.scaleKey(selTo, at = 'ry', vs = -1)
        elif 8 <= i < 10:
            pm.copyKey(selFrom)
            pm.pasteKey(selTo)
            pm.scaleKey(selTo, at = 'rx', vs = -1)
        elif 10 <= i < 14:
            pm.setAttr('%s.rotateOrder' %selTo[0], 4)
            pm.copyKey(selFrom, at = 'rx')
            pm.pasteKey(selTo, at = 'ry')
            pm.copyKey(selFrom, at = 'ry')
            pm.pasteKey(selTo, at = 'rx')
            pm.scaleKey(selTo, at = 'rx', vs = -1)
            pm.copyKey(selFrom, at = 'rz')
            pm.pasteKey(selTo, at = 'rz')
        elif 14 <= i < 18:
            pm.setAttr('%s.rotateOrder' %selTo[0], 4)
            pm.copyKey(selFrom, at = 'rx')
            pm.pasteKey(selTo, at = 'ry')
            pm.copyKey(selFrom, at = 'ry')
            pm.pasteKey(selTo, at = 'rx')
            pm.copyKey(selFrom, at = 'rz')
            pm.pasteKey(selTo, at = 'rz')
            pm.scaleKey(selTo, at = 'rz', vs = -1)
        elif 18 <= i < 33:
            pm.copyKey(selFrom, at = 'rx')
            pm.pasteKey(selTo, at = 'ry')
            pm.copyKey(selFrom, at = 'ry')
            pm.pasteKey(selTo, at = 'rz')
            pm.scaleKey(selTo, at = 'rz', vs = -1)
            pm.copyKey(selFrom, at = 'rz')
            pm.pasteKey(selTo, at = 'rx')
        elif 33 <= i <= 47:
            pm.copyKey(selFrom, at = 'rx')
            pm.pasteKey(selTo, at = 'ry')
            pm.copyKey(selFrom, at = 'ry')
            pm.pasteKey(selTo, at = 'rz')
            pm.copyKey(selFrom, at = 'rz')
            pm.pasteKey(selTo, at = 'rx')
            pm.scaleKey(selTo, at = 'rx', vs = -1)
    
    selLFootFrom = pm.ls(xbotCtrlList[6], r = True)
    selLFootTo = pm.ls(charCtrlList[6], r = True)
    selRFootFrom = pm.ls(xbotCtrlList[8], r = True)
    selRFootTo = pm.ls(charCtrlList[8], r = True)
    pm.copyKey(selLFootFrom, at = 'ToeTap')
    pm.pasteKey(selLFootTo, at = 'toeBend')
    pm.copyKey(selRFootFrom, at = 'ToeTap')
    pm.pasteKey(selRFootTo, at = 'toeBend')       

def deleteXbot(*args):
    intNodelList = at.intNode()
    pm.delete(intNodelList)
