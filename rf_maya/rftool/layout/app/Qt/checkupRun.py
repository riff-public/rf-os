import sys
from PyQt4 import QtCore, QtGui, uic
import os
import subprocess
from datetime import datetime
from shutil import copy2

projDataBaseFile = 'O:/Pipeline/core/rf_maya/rftool/layout/projDataBase/projDataBase.py'
db_hookFile      = 'O:/Pipeline/core/rf_maya/rftool/layout/review/db_hook.py'
sys.path.append(os.path.dirname(os.path.expanduser(projDataBaseFile)))
#sys.path.append(os.path.dirname(os.path.expanduser(db_hookFile)))
import projDataBase
#import db_hook
 
qtCreatorFile = "O:/Pipeline/core/rf_maya/rftool/layout/app/Qt/UI/checkUp.ui" # Enter file here.
rv            = "C:\\Program Files\\Shotgun\\RV-7.2.1\\bin\\rv.exe"
quickTime     = "C:\\Program Files (x86)\\QuickTime\\QuickTimePlayer.exe"
mpc           = "C:\\Program Files (x86)\\K-Lite Codec Pack\\MPC-HC64\\mpc-hc64.exe"
projList      = projDataBase.projList() 

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

class MyApp(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.set_signals()
        self.submitSgBtn.setDisabled(True)
        
    def set_signals(self):
        self.projList.addItems(projList)
        self.depCombo.activated.connect(self.listSeq)
        self.projList.itemClicked.connect(self.listEp)
        self.epList.itemClicked.connect(self.listSeq)
        self.seqList.itemClicked.connect(self.listPubShot)
        self.shotPubList.itemClicked.connect(self.listVerShot)
        self.playerCombo.activated.connect(self.playerSelector)
        self.pubPlayBtn.clicked.connect(self.playPubButton)
        self.verPlayBtn.clicked.connect(self.playVerButton)
        self.pubBrowseBtn.clicked.connect(self.runPubExplorer)
        self.verBrowseBtn.clicked.connect(self.runVerExplorer)
        #self.submitSgBtn.clicked.connect(self.submitSG)
        self.submitDailyBtn.clicked.connect(self.submitDaily)
        self.locCombo.activated.connect(self.listSeq)
        self.locCombo.activated.connect(self.setEnbButtons)

#====================== Listing UI functions =========================  
    
    def listEp(self):
        selProj  = self.projList.currentItem().text()
        pipeline = projDataBase.projDataBase('P:/'+selProj) [1]        
        if pipeline == 0:
            epList = os.listdir('P:/%s/scene' %selProj)
        elif pipeline == 1:
            epList = os.listdir('P:/%s' %selProj)        
        self.epList.clear()
        self.seqList.clear()
        self.shotPubList.clear()
        self.shotVerList.clear()        
        self.epList.addItems(epList)
        self.epList.sortItems(0)

    def listSeq(self):
        try:
            selLoc   = self.locCombo.currentText()
            if selLoc == 'Project':
                selProj  = self.projList.currentItem().text()
                selEp    = self.epList.currentItem().text()
                selDep   = self.depCombo.currentText()
                pipeline = projDataBase.projDataBase('P:/'+selProj) [1]           
                if pipeline == 0:
                    seqList = os.listdir('P:/%s/scene/%s' %(selProj,selEp))
                elif pipeline == 1:
                    seqList = os.listdir('P:/%s/%s/scene/%s' %(selProj,selEp,selDep))            
                self.seqList.clear()
                self.shotPubList.clear()
                self.shotVerList.clear()
                self.seqList.addItems(seqList)
                self.seqList.sortItems(0)
            elif selLoc == 'Daily':
                selProj       = self.projList.currentItem().text()
                selEp         = self.epList.currentItem().text()
                selDep        = self.depCombo.currentText()
                dailyPath     = projDataBase.projDataBase('P:/'+selProj) [2]                
                dailyDateList = os.listdir('%s/%s/' %(dailyPath, selDep))                               
                self.seqList.clear()
                self.shotPubList.clear()
                self.shotVerList.clear()
                self.seqList.addItems(dailyDateList)
                self.seqList.sortItems(1)
        except:
            self.seqList.clear()
            self.shotPubList.clear()
            self.shotVerList.clear()
    
    def listPubShot(self):
        try:
            selLoc = self.locCombo.currentText()
            if selLoc == 'Project':
                selProj  = self.projList.currentItem().text()
                selEp    = self.epList.currentItem().text()
                selDep   = self.depCombo.currentText()
                seqItem  = self.seqList.currentItem().text()
                pipeline = projDataBase.projDataBase('P:/'+selProj) [1]           
                if pipeline == 0:
                    pubList = os.listdir('P:/%s/scene/%s/%s/playblast/%s' %(selProj,selEp,seqItem,selDep))
                elif pipeline == 1:
                    pubList = os.listdir('P:/%s/%s/scene/%s/preview'      %(selProj,selEp,selDep))            
                self.shotPubList.clear()
                self.shotVerList.clear()            
                self.shotPubList.addItems(pubList)
                self.shotPubList.sortItems(0) 
            elif selLoc == 'Daily':
                userTeam      = projDataBase.userDataBase()
                selProj       = self.projList.currentItem().text()
                selEp         = self.epList.currentItem().text()
                selDep        = self.depCombo.currentText()
                dailyItem     = self.seqList.currentItem().text()
                dailyPath     = projDataBase.projDataBase('P:/'+selProj) [2]                
                dailyFileList = os.listdir('%s/%s/%s' %(dailyPath, selDep, dailyItem))
                leadList      = []
                leadList      = userTeam.keys()
                leadList.sort()
                self.shotPubList.clear()
                self.shotVerList.clear()
                if selDep == 'anim':
                    self.shotPubList.addItems(leadList)
                self.shotPubList.addItems(dailyFileList)
                self.shotPubList.sortItems(0)
        except:
            self.shotPubList.clear()
            self.shotVerList.clear() 
            
    def listVerShot(self):          
        try:
            selLoc   = self.locCombo.currentText()
            if selLoc == 'Project':
                selProj  = self.projList.currentItem().text()
                selEp    = self.epList.currentItem().text()
                selDep   = self.depCombo.currentText()
                seqItem  = self.seqList.currentItem().text()
                pubItem  = self.shotPubList.currentItem().text()
                pipeline = projDataBase.projDataBase('P:/'+selProj) [1]           
                if pipeline == 0:
                    shotName = pubItem.split('_')[3]
                    verList  = os.listdir('P:/%s/scene/%s/%s/%s/%s/maya/playblast' %(selProj,selEp,seqItem,shotName,selDep))
                elif pipeline == 1:
                    shotName = pubItem.split('_')[0]
                    verList  = os.listdir('P:/%s/%s/scene/%s/%s/output/preview'    %(selProj,selEp,selDep,shotName)) 
                self.shotVerList.clear()            
                self.shotVerList.addItems(verList)
                self.shotVerList.sortItems(1) 
            elif selLoc == 'Daily':
                userTeam         = projDataBase.userDataBase()
                selProj          = self.projList.currentItem().text()
                selEp            = self.epList.currentItem().text()
                selDep           = self.depCombo.currentText()
                dailyItem        = self.seqList.currentItem().text()
                dailyFiles       = self.shotPubList.currentItem().text()
                dailyPath        = projDataBase.projDataBase('P:/'+selProj) [2]
                self.shotVerList.clear()
                dailyFileList = os.listdir('%s/%s/%s' %(dailyPath, selDep, dailyItem))
                a = []
                if dailyFiles in userTeam.keys():
                    for i in userTeam['%s' %dailyFiles]: #user under lead
                        for j in dailyFileList:
                            try:
                                shotUser = j.split('_')[-1][:-4]
                                if shotUser.lower() == i.lower():
                                    a.append(j)
                            except:
                                pass
                    self.shotVerList.addItems(a)
                    self.shotVerList.sortItems(0)
                else:
                    dailyFileVerList = os.listdir('%s/%s/%s/%s' %(dailyPath, selDep, dailyItem, dailyFiles))
                    self.shotVerList.addItems(dailyFileVerList)
                    self.shotVerList.sortItems(0)
        except:
            self.shotVerList.clear()

    def setEnbButtons(self):
        selLoc   = self.locCombo.currentText()
        if selLoc == 'Project':
            #self.submitSgBtn.setEnabled(True)
            self.submitDailyBtn.setEnabled(True)
            self.shotPubText.setText('Shots')
            self.shotVerText.setText('Version files')
            #self.label_18.setEnabled(True)
            #self.verPlayBtn.setEnabled(True)
            self.verBrowseBtn.setEnabled(True)
            self.seqText.setText('Sequences:')
        elif selLoc == 'Daily':
            #self.submitSgBtn.setDisabled(True)
            self.submitDailyBtn.setDisabled(True)
            self.shotPubText.setText('Team/Files')
            self.shotVerText.setText('Files')
            #self.label_18.setDisabled(True)
            #self.verPlayBtn.setDisabled(True)
            self.verBrowseBtn.setDisabled(True)
            self.seqText.setText('Daily dates:')

#====================== System functions =========================

    def playerSelector(self):
        playerLabel = self.playerCombo.currentText()
        if playerLabel == 'RV':
            player = eval('rv')
        elif playerLabel == 'QuickTime':
            player = eval('quickTime')
        elif playerLabel == 'Media Player Classic':
            player = eval('mpc')
        return player
        
    def playPubButton(self):
        selLoc   = self.locCombo.currentText()
        selProj  = self.projList.currentItem().text()
        selEp    = self.epList.currentItem().text()
        selDep   = self.depCombo.currentText()
        seqItem  = self.seqList.currentItem().text()
        selItem  = [item.text() for item in self.shotPubList.selectedItems()]
        pipeline = projDataBase.projDataBase('P:/'+selProj) [1]
        player   = self.playerSelector()
        files = [player]
        if selLoc == 'Project':
            if pipeline == 0:
                pipeline = projDataBase.projDataBase('P:/'+selProj) [1]
                filePathPubTxt = 'P:\\%s\\scene\\%s\\%s\\playblast\\%s' %(selProj,selEp,seqItem,selDep)
            elif pipeline == 1:
                filePathPubTxt = 'P:\\%s\\%s\\scene\\%s\\preview'      %(selProj,selEp,selDep)
        elif selLoc == 'Daily': 
            dailyPath      = projDataBase.projDataBase('P:/'+selProj) [2]
            dailyPath      = dailyPath.replace('/','\\')
            filePathPubTxt = '%s\\%s\\%s' %(dailyPath, selDep, seqItem)
        if selItem:
            for i in selItem:
                files.append(filePathPubTxt + "\\" + str(i))
            subprocess.Popen(files)
        else:
            pass
            
    def playVerButton(self):
        selLoc   = self.locCombo.currentText()
        selProj  = self.projList.currentItem().text()
        selEp    = self.epList.currentItem().text()
        seqItem  = self.seqList.currentItem().text()
        verItem  = [item.text() for item in self.shotVerList.selectedItems()]
        selDep   = self.depCombo.currentText()
        pipeline = projDataBase.projDataBase('P:/'+selProj) [1]
        player   = self.playerSelector()
        verFiles = [player]
        if selLoc == 'Project':
            if pipeline == 0:
                shotName = verItem[0].split('_')[3]
                filePathTxt = 'P:\\%s\\scene\\%s\\%s\\%s\\%s\\maya\\playblast' %(selProj, selEp, seqItem, shotName, selDep) 
            elif pipeline == 1:
                shotName = verItem.split('_')[0]
                filePathTxt = 'P:\\%s\\%s\\scene\\%s\\%s\\output\\preview'    %(selProj, selEp, selDep, shotName)  
        elif selLoc == 'Daily':
            dailyPath      = projDataBase.projDataBase('P:/'+selProj) [2]
            dailyPath      = dailyPath.replace('/','\\')
            filePathTxt = '%s\\%s\\%s' %(dailyPath, selDep, seqItem)
        if verItem:
            for i in verItem:
                verFiles.append(filePathTxt + "\\" + str(i))
            subprocess.Popen(verFiles)
        else:
            pass
            
    def runPubExplorer(self):
        selLoc   = self.locCombo.currentText()
        selProj  = self.projList.currentItem().text()
        selEp    = self.epList.currentItem().text()
        seqItem  = self.seqList.currentItem().text()
        selDep   = self.depCombo.currentText()
        pipeline = projDataBase.projDataBase('P:/'+selProj) [1]
        player   = self.playerSelector()
        if selLoc == 'Project':
            if pipeline == 0:
                filePathPubTxt = 'P:/%s/scene/%s/%s/playblast/%s' %(selProj,selEp,seqItem,selDep)
            elif pipeline == 1:
                filePathPubTxt = 'P:/%s/%s/scene/%s/preview'      %(selProj,selEp,selDep)    
        elif selLoc == 'Daily': 
            dailyPath      = projDataBase.projDataBase('P:/'+selProj) [2]
            dailyPath      = dailyPath.replace('/','\\')
            filePathPubTxt = '%s\\%s\\%s' %(dailyPath, selDep, seqItem)
        os.startfile(str(filePathPubTxt))
    '''    
    def submitSG(self):
        reload(db_hook)
        pubItem  = self.shotPubList.currentItem().text()
        selItem  = [item.text() for item in self.shotPubList.selectedItems()]
        selCount = len(selItem)
        num      = 1
        for i in selItem:
            selProj          = self.projList.currentItem().text()
            shotNameSplit    = str(i).split('_')            
            workPath         = 'P:/%s/scene/%s/%s/%s/%s/maya/work'      %(selProj, shotNameSplit[1], shotNameSplit[2], shotNameSplit[3], shotNameSplit[4][:-4])
            outputVerPath    = 'P:/%s/scene/%s/%s/%s/%s/maya/playblast' %(selProj, shotNameSplit[1], shotNameSplit[2], shotNameSplit[3], shotNameSplit[4][:-4])            
            workList         = os.listdir(workPath)
            outputVerList    = os.listdir(outputVerPath)
            latestWorkFile   = max(workList)
            latestOutputFile = max(outputVerList)
            userName         = latestOutputFile.split('_')[-1][:-4]
            
            db_hook.send_to_review(workPath + '/' + latestWorkFile, outputVerPath + '/' + latestOutputFile, userName)
            print 'Submit %s (%s): done.' % (latestOutputFile, str(num) + '/' + str(selCount))
            num += 1
    '''
    def submitDaily(self):
        selItem        = [item.text() for item in self.shotPubList.selectedItems()]
        selProj        = self.projList.currentItem().text()
        selEp          = self.epList.currentItem().text()
        selDep         = self.depCombo.currentText()
        seqItem        = self.seqList.currentItem().text()        
        pipeline       = projDataBase.projDataBase('P:/'+selProj) [1]
        now            = datetime.now()
        dateYear       = '%d'   %now.year
        dateMonth      = '%02d' %now.month
        dateDay        = '%02d' %now.day
        dailyPath      = projDataBase.projDataBase('P:/'+selProj) [2]
        dailyNowPath   = dailyPath + '/' + selDep + '/' + dateYear + '_' + dateMonth + '_' + dateDay + '/'
        selCount       = len(selItem)
        num            = 1
        try:
        	os.makedirs('%s' %dailyNowPath)
    	except:
    		pass
        for i in selItem:
            shotName = i.split('_')[3]
            if pipeline == 0:
                filePathVerTxt   = 'P:/%s/scene/%s/%s/%s/%s/maya/playblast/' %(selProj, selEp, seqItem, shotName, selDep)
                outputVerList    = os.listdir(filePathVerTxt)
                latestVerFile    = max(outputVerList)
                aList            = latestVerFile.split('_')
                dailyFile        = aList[0] + '_' + aList[1] + '_' + aList[2] + '_' + aList[3] + '_' + aList[4] + '_' + aList[6]
            elif pipeline == 1:
                pass
            print 'Submitting: %s (%s)...' % (latestVerFile, str(num) + '/' + str(selCount))
            copy2("%s%s" %(filePathVerTxt, latestVerFile), "%s%s" %(dailyNowPath, dailyFile))
            num += 1
        print 'Submitting done.'

    def runVerExplorer(self):
        selProj  = self.projList.currentItem().text()
        selEp    = self.epList.currentItem().text()
        seqItem  = self.seqList.currentItem().text()
        selDep   = self.depCombo.currentText()
        verItem  = self.shotVerList.currentItem().text()
        pipeline = projDataBase.projDataBase('P:/'+selProj) [1]
        player   = self.playerSelector()   
        if pipeline == 0:
            shotName = verItem.split('_')[3]
            filePathTxt = 'P:/%s/scene/%s/%s/%s/%s/maya/playblast' %(selProj, selEp, seqItem, shotName, selDep) 
        elif pipeline == 1:
            shotName = verItem.split('_')[0]
            filePathTxt = 'P:/%s/%s/scene/%s/%s/output/preview'    %(selProj, selEp, selDep, shotName)
        os.startfile(str(filePathTxt))    

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv) #disable in Maya
    window = MyApp()
    window.show()
    sys.exit(app.exec_()) #disable in Maya
