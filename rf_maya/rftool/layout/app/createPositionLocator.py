import pymel.core as pm

def UI():
    if pm.window('locCreator', exists = True):
        pm.deleteUI('locCreator')        
    pm.window('locCreator', title = 'locCreator', rtf = True, mxb = False)    
    main = pm.columnLayout('mainLayout', adj = True, rs = 5, cat = ('both',5))
    pm.separator(style = 'none')
    pm.text(l = 'Keyword:', al = 'left')
    pm.textField('kWordFind')
    pm.radioCollection('srModeRdo')
    pm.radioButton('A', l = 'Under selected', sl = True)
    pm.radioButton('B', l = 'All in scene')
    pm.button(l = 'Search and Select', c = findObj)
    pm.separator(style = 'in')
    pm.button(l = 'Create Locators', c = snapLoc)
    pm.button(l = 'Snap to Locators', c = snapToLoc)
    pm.separator(style = 'none')    
    pm.showWindow('locCreator')

def findObj(*args):
    findOp = pm.radioCollection('srModeRdo', q = True, sl = True)
    kWord = pm.textField('kWordFind', q = True, tx = True)
    if findOp == 'A':
        sel = pm.ls(sl = True)
        rawLs = pm.listRelatives(sel, ad = True, type = 'transform')
        pm.select(cl = True)
        for i in rawLs:
            if i.find(kWord) != -1:
                pm.select(i, add = True)
    elif findOp == 'B':
        allLs = pm.ls('*%s*' %kWord, type = 'transform')
        pm.select(allLs)
        
def snapLoc(*args):
    sel = pm.ls(sl = True)
    locRawLs = []
    for i in sel:
        loc = pm.spaceLocator(n = '%s_loc' %i)
        locRawLs.append('%s_loc' %i)
        pos = pm.xform(i, q = 1, ws = 1, t = 1)
        rot = pm.xform(i, q = 1, ws = 1, ro = 1) 
        pm.xform(loc, ws = 1, t = [pos[0],pos[1],pos[2]])
        pm.xform(loc, ws = 1, ro = [rot[0],rot[1],rot[2]])
    pm.group(locRawLs, n = 'positionLoc_grp')
    
def snapToLoc(*args):
    locLs = pm.listRelatives('positionLoc_grp')
    for i in locLs:
        try:
            objName = i.replace('_loc','')
            obj = pm.ls(objName)[0]
            pos = pm.xform(i, q = 1, ws = 1, t = 1)
            rot = pm.xform(i, q = 1, ws = 1, ro = 1) 
            pm.xform(obj, ws = 1, t = [pos[0],pos[1],pos[2]])
            pm.xform(obj, ws = 1, ro = [rot[0],rot[1],rot[2]])
        except:
            pass
