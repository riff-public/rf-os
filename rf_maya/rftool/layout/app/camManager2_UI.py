# RiFF Camera Manager 2 for Layout Artist
# Designed and developed by: Pathompong Thitithan

import pymel.core as pm
from collections import OrderedDict 
import re

class camManagerMain():
    
    # GLOBAL VARIABLE
    def __init__(self):
        self._shakeValue = []
        
    # INTERFACE     
    def UI(self):
        if pm.window('camManagerWin', exists = True): 
            pm.deleteUI('camManagerWin')
                    
        camManagerWin = pm.window('camManagerWin', title = 'Camera Manager v2.0',ip=True, rtf = True, mxb = False)
        mainLayout = pm.formLayout(numberOfDivisions=100, p = camManagerWin)
        #-----------------------
        initCam = pm.frameLayout(borderStyle = 'out',
                                 label = 'Preparing Cameras', 
                                 marginHeight = 5, 
                                 marginWidth = 5,
                                 width = 295,
                                 cll = True,
                                 p = mainLayout)
        ###
        row1 = pm.rowLayout(nc = 3,
                            adj = 2, 
                            columnAttach = [(1,'both',5),(2,'both',5),(3,'both',5)],
                            p = initCam)
        pm.text(l='Camera Group:', al = 'right', w = 80, p = row1)
        self.camGrpName = pm.text(l='[none]', al = 'left', p = row1)
        pm.button(l='Load', w = 85, p = row1,  c = self.loadCamGrpProc)
        ###
        row2 = pm.rowLayout(nc = 3, 
                            adj = 2,
                            columnAttach = [(1,'both',5),(2,'both',5),(3,'both',5)],
                            p = initCam)
        pm.text(l='Ref. camera:', al = 'right', w = 80, p = row2)
        self.refCamName = pm.text(l='persp', al = 'left', fn='boldLabelFont', p = row2)
        pm.button(l='Load', w = 85, p = row2, c = self.loadRefCamProc)
        #-----------------------
        ctrlCam = pm.frameLayout(borderStyle = 'out',
                                 label = 'Camera Controllers', 
                                 marginHeight = 5, 
                                 marginWidth = 5,
                                 width = 295, 
                                 cll = True,
                                 p = mainLayout)
        ctrlForm = pm.formLayout(numberOfDivisions=100, p = ctrlCam)
        ###
        tx1 = pm.text(l='Camera list:', al = 'left', p = ctrlForm)
        tx2 = pm.text(l='Controller:', al = 'left', p = ctrlForm)
        
        self.cam_ls_view = pm.textScrollList(allowMultiSelection = True, p = ctrlForm, sc = self.selectCamProc)
        self.ctrl_ls_view = pm.textScrollList(allowMultiSelection = True, 
                                              sc = self.selectConProc,
                                              p = ctrlForm)
        
        tx3 = pm.button(l='Current shot', p = ctrlForm, c = self.selectCurrentShotProc)
        tx4 = pm.button(l='Select All Selected', p = ctrlForm, c = self.selectAllCtrlProc)
        
        pm.formLayout(ctrlForm, 
                      edit = True, 
                      attachForm = [                      (tx1,'left',5),(tx1,'top',5)                  ,                  (tx2,'top',5),(tx2,'right',5), 
                                                          (self.cam_ls_view,'left',5)                   ,                  (self.ctrl_ls_view,'right',5), 
                                                        (tx3,'left',5),(tx3,'bottom',5)                 ,                (tx4,'bottom',5),(tx4,'right',5)],
                      attachControl = [(self.cam_ls_view,'top',5,tx1),(self.cam_ls_view,'bottom',5,tx3) , (self.ctrl_ls_view,'top',5,tx2),(self.ctrl_ls_view,'bottom',5,tx4)],
                      attachPosition = [                       (tx1,'right',5,50)                       ,                        (tx2,'left',5,50),
                                                        (self.cam_ls_view,'right',5,50)                 ,                 (self.ctrl_ls_view,'left',5,50),
                                                               (tx3,'right',5,50)                       ,                       (tx4,'left',5,50)])
        #-----------------------
        util = pm.frameLayout(borderStyle = 'out',
                              label = 'Utilities', 
                              marginHeight = 5, 
                              marginWidth = 5,
                              width = 220, 
                              cll = True,
                              parent = mainLayout) 
        pm.text(l = '  Ref. Camera Lens Select:', al = 'left', p = util)  
        ###              
        lensForm = pm.formLayout(numberOfDivisions=100, p = util)
        b1 = pm.button(l='24mm', p = lensForm, c = lambda x: self.ref_cam_lens(24))
        b2 = pm.button(l='28mm', p = lensForm, c = lambda x: self.ref_cam_lens(28))
        b3 = pm.button(l='35mm', p = lensForm, c = lambda x: self.ref_cam_lens(35))
        b4 = pm.button(l='50mm', p = lensForm, c = lambda x: self.ref_cam_lens(50))
        b5 = pm.button(l='85mm', p = lensForm, c = lambda x: self.ref_cam_lens(85))
        b6 = pm.button(l='135mm', p = lensForm, c = lambda x: self.ref_cam_lens(135))
        pm.formLayout(lensForm, 
                      edit = True, 
                      attachForm = [ (b1,'left',5), (b1,'top',5),    (b2,'top',5),    (b3,'right',5),(b3,'top',5), 
                                     (b4,'left',5), (b4,'bottom',0), (b5,'bottom',0), (b6,'right',5),(b6,'bottom',0) ], 
                      attachControl = [ (b2,'left',5,b1),(b2,'right',5,b3), 
                                        (b5,'left',5,b4),(b5,'right',5,b6) ],
                      attachPosition = [(b1,'right',5,34), (b3,'left',5,66), (b4,'right',5,34), (b6,'left',5,66) ] )
        pm.separator(style = 'in', p = util)
        ###
        gridForm = lensForm = pm.formLayout(numberOfDivisions=100, p = util)
        c1 = pm.button(l='Snap to Ref. Cam', p = gridForm, c = self.snapRefCamProc)
        c2 = pm.button(l='Snap Focus', p = gridForm, c = self.snapFocusProc)
        c3 = pm.button(l='Copy Shake Value', p = gridForm, c = self.copyShakeProc)
        c4 = pm.button(l='Paste Shake Value', p = gridForm, c = self.pasteShakeProc)
        c5 = pm.button(l='Bake Shake to Anim', p = gridForm, c = self.bakeShakeAnimProc)
        c6 = pm.button(l='Bake to Basic Cam', p = gridForm, c = self.bakeBasicCam)
        pm.formLayout(gridForm, 
                      edit = True, 
                      attachForm = [      (c1,'left',5),(c1,'top',0)     ,   (c2,'top',0),(c2,'right',5), 
                                                 (c3,'left',5)           ,       (c4,'right',5), 
                                         (c5,'left',5),(c5,'bottom',5)   , (c6,'bottom',5),(c6,'right',5)],
                      attachControl = [(c3,'top',5,c1),(c3,'bottom',5,c5),(c4,'top',5,c2),(c4,'bottom',5,c6)],
                      attachPosition = [       (c1,'right',5,50)         ,      (c2,'left',5,50),
                                               (c3,'right',5,50)         ,      (c4,'left',5,50),
                                               (c5,'right',5,50)         ,      (c6,'left',5,50)])
        pm.formLayout(
                      mainLayout, 
                      edit = True, 
                      attachForm = [(initCam,'left',0), (initCam,'right',0), (initCam,'top',0), 
                                    (ctrlCam,'left',0), (ctrlCam,'right',0), 
                                    (util,'left',0), (util,'right',0), (util,'bottom',0)], 
                      attachControl = [(ctrlCam,'bottom',0,util), (ctrlCam,'top',0,initCam)],
                      )
                      
        pm.showWindow(camManagerWin)
        self.int_load_cam_ls()
        
    def selectCamProc(self, *args, **kwargs):
        self.get_cam_selected()

    def loadCamGrpProc(self, *args, **kwargs):
        self.load_cam_ls()
               
    def loadRefCamProc(self, *args, **kwargs):
        self.load_ref_cam()
        
    def selectCamProc(self, *args, **kwargs):
        self.get_cam_selected()
        
    def selectConProc(self, *args, **kwargs):
        self.get_con_selected()
        
    def selectCurrentShotProc(self, *args, **kwargs):
        self.get_current_cam()
        
    def selectAllCtrlProc(self, *args, **kwargs):
        self.sel_all_ctrl()
        
    def snapRefCamProc(self, *args, **kwargs):
        self.snap_ref_cam()
        
    def snapFocusProc(self, *args, **kwargs):
        self.snap_focus()

    def copyShakeProc(self, *args, **kwargs):
        self.copy_shake()
        
    def pasteShakeProc(self, *args, **kwargs):
        self.paste_shake()        

    def bakeShakeAnimProc(self, *args, **kwargs):
        self.bake_shake_anim()
        
    def bakeBasicCam(self, *args, **kwargs):
        self.bake_basic_cam()
        

############################ Function #################################
    
    def int_load_cam_ls(self, *args, **kwargs):
        try:
            camLs = []
            pm.text(self.camGrpName, e = True, l = 'cam_grp', fn = 'boldLabelFont')
            camGrpLs = pm.listRelatives('|cam_grp', c = True)
            pm.textScrollList(self.cam_ls_view, e = True, ra = True)
            for i in camGrpLs:
                camName = i.replace('_grp','_cam')
                camLs.append(camName)
            camLs.sort()
            pm.textScrollList(self.cam_ls_view, e = True, append = camLs)
        except:
            pass
        
    def load_cam_ls(self, *args, **kwargs):
        camLs = []
        sel = pm.ls(sl = True)
        camGrpLs = pm.listRelatives(sel[0], c = True)
        pm.text(self.camGrpName, e = True, l = sel[0].replace('|',''), fn = 'boldLabelFont')
        pm.textScrollList(self.cam_ls_view, e = True, ra = True)
        for i in camGrpLs:
            camName = i.replace('_grp','_cam')
            camLs.append(camName)
        camLs.sort()
        pm.textScrollList(self.cam_ls_view, e = True, append = camLs)
            
    def get_cam_selected(self, *args, **kwargs):
        pm.textScrollList(self.ctrl_ls_view, e = True, ra = True)
        cam_selected = pm.textScrollList(self.cam_ls_view, q = True, si = True)
        camSel = []
        ctrlLs = []
        for i in cam_selected:
            camSel = i.replace('cam','grp')
            lsIncamSel = pm.listRelatives(camSel, ad = True)
            lsIncamSel.reverse()
            for i in lsIncamSel:
                if str(i).split('|')[-1].find('_camCtrl') != -1:            # find '_camCtrl' from every last child node (if result = -1, word isn't match)
                    if str(i).split('|')[-1].find('_camCtrlShape') == -1:   # find item that isn't '_camCtrlShape' (if result = -1, word isn't match)
                        visStatus = pm.getAttr('%s.v' %i)
                        if visStatus == True:
                            camName = i.split('|')[0].replace('_grp','')
                            ctrlName = i.split('|')[-1].replace('_camCtrl','')
                            ctrlLs.append('%s_%s' % (camName,ctrlName))
            ctrlLs.append('-------')
        #if pm.ls(sl = True)
        pm.select(cl = True)
        pm.textScrollList(self.ctrl_ls_view, e = True, append = ctrlLs)
    
    def get_con_selected(self, *args, **kwargs):
        pm.select(cl = True)
        ctrl_selected = pm.textScrollList(self.ctrl_ls_view, q = True, si = True)
        for i in ctrl_selected:
            try:
                camGrpName = i.split('_')[0] + '_grp'
                camCtrlName = i.split('_')[-1] + '_camCtrl'
                lsIncamSel = pm.listRelatives(camGrpName, ad = True)
                for j in lsIncamSel:
                        _camCtrlName = str(j).split('|')[-1]
                        if _camCtrlName.find('_camCtrl') != -1 and _camCtrlName.find('_camCtrlShape') == -1:  # find '_camCtrl' from every last child node (if result = -1, word isn't match)
                            if j.split('|')[-1] == camCtrlName:
                                pm.select(j, add = True)
            except:
                pass

    def load_ref_cam(self, *args, **kwargs):
        sel = pm.ls(sl = True)
        pm.text(self.refCamName, e = True, l = '%s' %sel[0], fn = 'boldLabelFont')
          
    def get_current_cam(self, *args, **kwargs):
        currentShot = pm.ls(pm.sequenceManager(q = True, currentShot = True))
        currentCam = pm.getAttr("%s.currentCamera" % currentShot[0]).split('|')[-1]
        pm.textScrollList(self.cam_ls_view, e = True, da = True, si = currentCam)
        self.selectCamProc()
        start = pm.shot(currentShot, q = True, startTime = True)
        end = pm.shot(currentShot, q = True, endTime = True)
        pm.playbackOptions(ast = start, min = start, aet = end, max = end)
        
    def sel_all_ctrl(self, *args, **kwargs):
        camLs = []
        ctrlLs = []
        allItems = []
        selCamLs = pm.textScrollList(self.cam_ls_view, q = True, si = True)
        selCtrlLs = pm.textScrollList(self.ctrl_ls_view, q = True, si = True)
        for i in selCamLs:
            camLs.append(i.split('_')[0])
        for i in selCtrlLs:
            try:
                ctrlLs.append(i.split('_')[1])
            except:
                pass
        for i in camLs:
            for j in ctrlLs:
                allItems.append(i + '_' + j)
        pm.textScrollList(self.ctrl_ls_view, e = True, si = allItems)
        self.get_con_selected()
            
    def ref_cam_lens(self, refFL, *args, **kwargs):
        refCam = pm.text(self.refCamName, q = True, l = True)
        refCamShape = pm.ls(refCam)
        pm.setAttr(refCamShape[0].focalLength, refFL)
    
    def snap_ref_cam(self, *args, **kwargs):
        sel = pm.ls(sl = True)[0]    # cam ctrl
        refCam = pm.text(self.refCamName, q = True, l = True)
        pos = pm.xform(refCam, q = 1, ws = 1, t = 1)
        rot = pm.xform(refCam, q = 1, ws = 1, ro = 1)
        selReltv = pm.listRelatives(pm.listRelatives(pm.ls(sel), p = True), ad=True, typ='transform') #<<<<< check list num. for new ver. cam
        if  str(sel).split('|')[-1].find('camAllMove_camCtrl') != -1:
            pm.xform(sel, ws = 1, t = [pos[0],pos[1],pos[2]])
            pm.xform(selReltv[12], ws = 0, t = [0,0,0])
            pm.xform(sel, ws = 1, ro = [0,0,0])
            pm.xform(selReltv[12], ws = 1, ro = [rot[0],rot[1],rot[2]])
        else:
            pm.xform(sel, ws = 1, t = [pos[0],pos[1],pos[2]])
            pm.xform(sel, ws = 1, ro = [rot[0],rot[1],rot[2]])
        refCamShape = pm.ls(refCam)
        fl = pm.getAttr(refCamShape[0].focalLength)
        pm.setAttr("%s.focalLength" % selReltv[2], fl)
    
    def snap_focus(self, *args, **kwargs):
        obj = pm.ls(sl = True)[0]
        currShot = pm.sequenceManager(q = True, currentShot = True)
        loc = pm.ls('%s_COI_Location' %currShot)
        pos = pm.xform(obj, q = 1, ws = 1, t = 1)
        pm.xform(loc, t = [pos[0], pos[1], pos[2]])
        distance_node = pm.ls('%s_distance_node' %currShot)[0]
        distance_value = pm.getAttr("%s.distance" % distance_node)
        pm.setAttr("%s_camShape.focusDistance" %currShot, distance_value)

    def copy_shake(self, *args, **kwargs):
        try:
            selCopy = pm.ls(sl=True)[0]
            seed = pm.getAttr(selCopy.seed)
            offset = pm.getAttr(selCopy.offset)
            freqTX = pm.getAttr(selCopy.freqTX)
            freqTY = pm.getAttr(selCopy.freqTY)
            ampTX = pm.getAttr(selCopy.ampTX)
            ampTY = pm.getAttr(selCopy.ampTY)
            freqRX = pm.getAttr(selCopy.freqRX)
            freqRY = pm.getAttr(selCopy.freqRY)
            freqRZ = pm.getAttr(selCopy.freqRZ)
            ampRX = pm.getAttr(selCopy.ampRX)
            ampRY = pm.getAttr(selCopy.ampRY)
            ampRZ = pm.getAttr(selCopy.ampRZ)
            self._shakeValue = [seed, offset, 
                                freqTX, freqTY, 
                                ampTX , ampTY, 
                                freqRX, freqRY, freqRZ,
                                ampRX , ampRY , ampRZ]
        except:
            pm.warning('Please select "autoShake_camCtrl".')
              
    def paste_shake(self, *args, **kwargs):
        try:
            setShakeVal = self._shakeValue
            selPaste = pm.ls(sl=True)
            for i in selPaste:
                pm.setAttr(i.seed, setShakeVal[0])
                pm.setAttr(i.offset, setShakeVal[1])
                pm.setAttr(i.freqTX, setShakeVal[2])
                pm.setAttr(i.freqTY, setShakeVal[3])
                pm.setAttr(i.ampTX, setShakeVal[4])
                pm.setAttr(i.ampTY, setShakeVal[5])
                pm.setAttr(i.freqRX, setShakeVal[6])
                pm.setAttr(i.freqRY, setShakeVal[7])
                pm.setAttr(i.freqRZ, setShakeVal[8])
                pm.setAttr(i.ampRX, setShakeVal[9])
                pm.setAttr(i.ampRY, setShakeVal[10])
                pm.setAttr(i.ampRZ, setShakeVal[11])  
        except:
            pm.warning('Please select "autoShake_camCtrl".')      

    def bake_shake_anim(self, *args, **kwargs):
        try:
            camSel = str(pm.ls(sl = True)[0])
            camGrp = camSel.replace('_cam','_grp')
            minFrame = pm.playbackOptions(q = True, min = True)
            maxFrame = pm.playbackOptions(q = True, max = True)
            handHeldCtrl = '%s|camAllMove_camCtrl|camMove_camCtrl|orbit_grp|crane_grp|camAim_grp|cam_grp|camCtrl_camCtrl|anim_ctrl|camGimbal|camHandHeld_camCtrl' %camGrp
            autoShakeCtrl = '%s|camAllMove_camCtrl|camMove_camCtrl|orbit_grp|crane_grp|camAim_grp|cam_grp|camCtrl_camCtrl|autoShake_grp|autoShake_camCtrl' %camGrp
            
            pm.spaceLocator(n = 'camShake_loc')
            pm.parent('camShake_loc', '%s|handHeld' %camGrp)
            pm.parentConstraint('%s|handHeld|camShake_node' %camGrp, 'camShake_loc', n = 'camShake_constrain', mo = False, w = 1)
            pm.bakeResults('camShake_loc', t = (minFrame,maxFrame), sb = 1)
            if pm.keyframe(handHeldCtrl, q = True, kc = True) > 0:
                pm.cutKey(handHeldCtrl)
            pm.copyKey('camShake_loc')
            pm.pasteKey(handHeldCtrl)
            if pm.keyframe(autoShakeCtrl, q = True, kc = True) > 0:
                pm.cutKey(autoShakeCtrl)
            pm.setAttr('%s.seed' %autoShakeCtrl, 0)
            pm.setAttr('%s.offset' %autoShakeCtrl, 0)
            pm.setAttr('%s.freqTX' %autoShakeCtrl, 0)
            pm.setAttr('%s.freqTY' %autoShakeCtrl, 0)
            pm.setAttr('%s.ampTX' %autoShakeCtrl, 0)
            pm.setAttr('%s.ampTY' %autoShakeCtrl, 0)
            pm.setAttr('%s.freqRX' %autoShakeCtrl, 0)
            pm.setAttr('%s.freqRY' %autoShakeCtrl, 0)
            pm.setAttr('%s.freqRZ' %autoShakeCtrl, 0)
            pm.setAttr('%s.ampRX' %autoShakeCtrl, 0)
            pm.setAttr('%s.ampRY' %autoShakeCtrl, 0)
            pm.setAttr('%s.ampRZ' %autoShakeCtrl, 0)
            pm.delete('camShake_loc')
            pm.select(handHeldCtrl)
        except:
            pm.warning('Please select a camera.')
            pm.delete('camShake_loc')
            
    def bake_basic_cam(self, *args, **kwargs):
        try:
            camSel = pm.ls(sl = True)[0]
            shotName = camSel.split('_')[0]
            minFrame = pm.playbackOptions(q = True, min = True)
            maxFrame = pm.playbackOptions(q = True, max = True)
            bakedCam,_ = pm.camera(n = '%s_camshot' %shotName)
            pm.parentConstraint(camSel, bakedCam, n = 'camBaked_constrain', mo = False, w = 1)
            pm.bakeResults(bakedCam, t = (minFrame,maxFrame), sb = 1)
            pm.delete('camBaked_constrain')
        except:
            pm.warning('Please select a camera shot')
            pm.delete(bakedCam)