import pymel.core as pm
import maya.mel as mm
import maya.cmds as cmds

def UI():
    if pm.window('layoutStarterWin02', exists = True):
        pm.deleteUI('layoutStarterWin02')
        
    pm.window('layoutStarterWin02', title = 'Layout Starter 2.0', rtf = True, mxb = False)
    mainLayout = pm.columnLayout(adj = True)
    with mainLayout:
        pm.separator(style = 'none', h = 5)
        pm.button(l = 'Import XML', h = 40, c = doImportXML)
        pm.separator(style = 'none', h = 5)
        frame2 = pm.frameLayout(l = 'Camera Manage', bs = 'out', mh = 5, mw = 5)
        with frame2:
            row03 = pm.rowLayout(nc = 3, adj = 2, cw = (1,70))
            with row03:
                pm.text(l = 'Create cams: ')
                pm.intField('camAmount', v = 1)
                pm.button(l = 'Create', w = 70, c = createCam)
        frame1 = pm.frameLayout(l = 'Shot Manage', bs = 'out', mh = 5, mw = 5)
        with frame1:
            row01 = pm.rowLayout(nc = 3, adj = 2, cw = (1,70))
            with row01:
                pm.text(l = 'Shot start: ')
                pm.intField(v = 1)
                pm.optionMenu('stepShotName', w = 70)
                pm.menuItem(l = 'x1')
                pm.menuItem(l = 'x10')
                pm.optionMenu('stepShotName', e = True, sl = 2)
            pm.button(l = 'Rename Shots')
        pm.separator(style = 'none')
            
    pm.showWindow('layoutStarterWin02')

def createCam(*args):
    camAmount = pm.intField('camAmount', q = True, v = True)
    for i in range(camAmount):
        cmds.file('G:/3D Asset/layoutLibrary/all/asset/tool/camera/layoutCamRig/rig/hero/camera_rig_hero.ma', i = True, dns = True)

def doImportXML(*args):
    # import xml
    mm.eval('performImportEDL 0;')
    pm.select (cl = True)
    # delete camera created from xml
    # count amount of shots
    shots = pm.ls(typ = 'shot')
    for i in shots:
        try:
            intShotCam = pm.shot(i, q = True, cc = True)
            pm.delete(intShotCam)
        except:
            pass
    # import cameras by amount of shots
    for i in range(len(shots)):
        cmds.file('G:/3D Asset/layoutLibrary/all/asset/tool/camera/layoutCamRig/rig/hero/camera_rig_hero.ma', i = True, dns = True)
    # group cameras to 'cam_grp'
    pm.group('camshot_grp*', n = 'cam_grp')
    # assign cameras to shots
    cams = pm.ls('camshot')
    for i in range(len(shots)):
        pm.shot(shots[i], e = True, cc = cams[i])
    # get shots name
    # set cameras name from shots
    for i in shots:
        shotCam = pm.shot(i, q = True, cc = True)
        pm.rename(shotCam, '%s_camShape' %i)
        shotCam = pm.shot(i, q = True, cc = True)
        pm.rename(pm.listRelatives(shotCam, ap = True), '%s_cam' %i)
        shotCam = pm.shot(i, q = True, cc = True)
        camMainGrp = pm.listRelatives(pm.listRelatives(shotCam, ap = True), ap = True)
        pm.rename(camMainGrp, '%s_grp' %i)
        for j in pm.listRelatives(camMainGrp, c = True):
            if j.split('|')[-1] == 'distance_grp':
                distanceChildLs = pm.listRelatives(j, c = True)
                for k in pm.listRelatives(j, c = True):
                    ctrlN = k.split('|')[-1]
                    pm.rename(k, '%s_%s' %(i,ctrlN))
        
    # remove audio node   
    for i in shots:
        try:
            pm.delete(pm.shot(i, q = True, aud = True))
        except:
            pass
    # offset each shots
    offsetFrame = 0
    for i in range(len(shots)):
        startFrame = pm.getAttr(shots[i].startFrame)
        endFrame = pm.getAttr(shots[i].endFrame)
        shots[i].setStartTime(startFrame + offsetFrame)
        shots[i].setEndTime(endFrame + offsetFrame)
        offsetFrame += 100
    
#UI()
