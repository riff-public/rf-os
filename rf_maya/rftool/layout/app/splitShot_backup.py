import pymel.core as pm
from maya.cmds import file
import maya.cmds as cmd

#UI:
#-set output file directory
# -save to anim
#-playblast settings
# -on/off
# -resolution
#-set audio path
# -on/off
def splitUI():
    if pm.window('splitShot', exists = True): 
        pm.deleteUI('splitShot')
        
    def filePathDisplay(*args):
        _pathInput = pm.textField(pathInput, q = True, tx = True)
        _pathInputCorrect = _pathInput.replace("\\", "/")
        pm.text(pathPreview, e = True, l = '%s/anim/[shot]/version/[shot]_anim_v001.ma' %_pathInputCorrect)
        pm.text(pbPathPreview, e = True, l = '%s/anim/preview/[shot]_anim.mov' %_pathInputCorrect)
        
    def getAnimPath(*args):
        animPath = pm.fileDialog2(fm = 3, ds = 2, ff = "directory", okc = "Select")
        try:
            pm.textField(pathInput, e = True, text = '%s' %animPath[0])
            filePathDisplay()
        except:
            pass
            
    def intAnimPath(*args):
        try:
            loc = file(q = True, loc = True).split('/')
            animPath = loc[0] + '/' + loc[1] + '/' + loc[2] + '/' + loc[3]
            pm.text(pathPreview, e = True, l = '%s/anim/[shot]/version/[shot]_anim_v001.ma' %animPath)
            pm.text(pbPathPreview, e = True, l = '%s/anim/preview/[shot]_anim.mov' %animPath)
            pm.textField(pathInput, e = True, text = '%s' %animPath)
        except:
            pass
        
    def getAudioPath(*args):
        audioPath = pm.fileDialog2(fm = 3, ds = 2, ff = "directory", okc = "Select")
        try:
            pm.textField(audioPathInput, e = True, text = '%s' %audioPath[0])
        except:
            pass
            
    def audioChkOn(*args):
        audioPathTxt.setEnable(True)
        audioPathInput.setEnable(True)
        audioPathBtn.setEnable(True)
        
    def audioChkOff(*args):
        audioPathTxt.setEnable(False)
        audioPathInput.setEnable(False)
        audioPathBtn.setEnable(False)
        
    def playblastChkOn(*args):
        pbResTxt.setEnable(True)
        pbResCombo.setEnable(True)
        pbPathPreviewTxt.setEnable(True)
        
    def playblastChkOff(*args):
        pbResTxt.setEnable(False)
        pbResCombo.setEnable(False)
        pbPathPreviewTxt.setEnable(False)
                
    splitShot = pm.window('splitShot', title = 'Split Shot v0.1', rtf = True, sizeable = True, mxb = False)
    main = pm.columnLayout(adjustableColumn = True, rs = 5, cat = ('both',5))
    pm.separator(style = 'none', h = 3, p = main)
    row1 = pm.rowLayout(nc = 3, cw3 = (70,100,70), adj = 2, cal = (1,'right'), cat = [(1,'both',3),(2,'both',3),(3,'both',3)], p = main)
    pm.text(l = 'Scene path:', p = row1)
    pathInput = pm.textField(aie = True, ec = filePathDisplay, p = row1)
    pm.button(l = 'Browse', w = 70, p = row1, c = getAnimPath)
    
    row2 = pm.rowLayout(nc = 3, cw3 = (70,100,70), adj = 2, cal = (1,'right'), cat = [(1,'both',3),(2,'both',3),(3,'both',3)], p = main)
    pm.text(l = 'File save:', p = row2)
    pathPreview = pm.text(l = '[none]', al = 'left', en = False, p = row2)
    
    pm.separator(style = 'none', h = 2, p = main)
    pm.separator(style = 'in', p = main)
        
    row3 = pm.rowLayout(nc = 3, cw3 = (70,100,70), adj = 2, cal = (1,'right'), cat = [(1,'both',3),(2,'both',3),(3,'both',3)], p = main)
    pm.text(l = ' ', p = row3)
    audioChk = pm.checkBox(l = 'Import shots audio', p = row3, onc = audioChkOn, ofc = audioChkOff)
        
    row4 = pm.rowLayout(nc = 3, cw3 = (70,100,70), adj = 2, cal = (1,'right'), cat = [(1,'both',3),(2,'both',3),(3,'both',3)], p = main)
    audioPathTxt = pm.text(l = 'Audio path:', p = row4, en = False)
    audioPathInput = pm.textField(p = row4, en = False)
    audioPathBtn = pm.button(l = 'Browse', w = 70, p = row4, c = getAudioPath)

    pm.separator(style = 'none', h = 2, p = main)
    pm.separator(style = 'in', p = main)
        
    row5 = pm.rowLayout(nc = 3, cw3 = (70,100,70), adj = 2, cal = (1,'right'), cat = [(1,'both',3),(2,'both',3),(3,'both',3)], p = main)
    pm.text(l = ' ', p = row5)
    playblastChk = pm.checkBox(l = 'Playblast', p = row5, onc = playblastChkOn, ofc = playblastChkOff)
        
    row6 = pm.rowLayout(nc = 3, cw3 = (70,100,70), adj = 2, cal = (1,'right'), cat = [(1,'both',3),(2,'both',3),(3,'both',3)], p = main)
    pbResTxt = pm.text(l = 'Resolution:', p = row6, en = False)
    pbResCombo = pm.optionMenu(p = row6, en = False)   
    pm.menuItem(l = '960 x 400 (Anamorphic 50%)')
    pm.menuItem(l = '960 x 540 (Full HD 50%)')
    pm.menuItem(l = '1920 x 800 (Anamorphic 100%)')
    pm.menuItem(l = '1920 x 1080 (Full HD 100%)')
    
    row7 = pm.rowLayout(nc = 3, cw3 = (70,100,70), adj = 2, cal = (1,'right'), cat = [(1,'both',3),(2,'both',3),(3,'both',3)], p = main)
    pbPathPreviewTxt = pm.text(l = 'Output path:', p = row7, en = False)
    pbPathPreview = pm.text(l = '[none]', al = 'left', en = False, p = row7)
        
    pm.separator(style = 'none', h = 2, p = main)
    pm.separator(style = 'in', h = 5, p = main)
    
    pm.button(l = 'Split', p = main, c = lambda x: saveFile(pathInput, audioPathInput, audioChk, playblastChk, pbResCombo))
    pm.separator(style = 'none', h = 3, p = main)
    
    pm.showWindow(splitShot)
    
    intAnimPath()

            
#"save file"
#-save to directory by settings
# -create directory --> save file
# -[project]\scene\anim\[shot]\ hero,version,output
def saveFile(_pathInput, _audioPath, _importAudioChk, _playblastChk, _pbResCombo, *args, **kwargs):
    pathInput = pm.textField(_pathInput, q = True, tx = True)
    audioPath = pm.textField(_audioPath, q = True, tx = True)
    audioChk = pm.checkBox(_importAudioChk, q = True, v = True)
    playblastChk = pm.checkBox(_playblastChk, q = True, v = True)
    pbResCombo = pm.optionMenu(_pbResCombo, q = True, v = True)
    
    pathInputCorrect = pathInput.replace("\\", "/")
    shots = pm.sequenceManager(listShots = True)
    try:
        for iShotName in shots:
            # go to current shot, set start & end frame each shot
            sqStFrame = iShotName.getSequenceStartTime()
            pm.sequenceManager(currentTime = sqStFrame)
            
            start = pm.shot(iShotName, q = True, startTime = True)
            end = pm.shot(iShotName, q = True, endTime = True)
            pm.playbackOptions(ast = start, min = start, aet = end, max = end)
            # import audio
            if audioChk == True:
                importAudio(audioPath, iShotName, start)
            else:
                pass
            # create directory
            pm.sysFile('%s/anim/%s/version' %(pathInputCorrect, iShotName), makeDir = True)
            # save file
            cmd.file(rename = '%s/anim/%s/version/%s_anim_v001' %(pathInputCorrect, iShotName, iShotName))
            cmd.file(save = True, type = "mayaAscii")
            # playblast
            if playblastChk == True:
                pm.sysFile('%s/anim/preview' %(pathInputCorrect), makeDir = True)
                doPlayblast(pbResCombo, pathInputCorrect, iShotName)
            else:
                pass     
    except KeyboardInterrupt:
        pass   

#"import audio"
#-read files from directory
#-check if it have file match to the shot
# -Yes: import
# -No: skip
def importAudio(_audioPath, _shotName, _startTime, *args, **kwargs):
    _audioPathCorrect = _audioPath.replace("\\", "/")
    fileList = pm.getFileList(folder = "%s" %_audioPathCorrect)
    pm.delete(pm.ls(type = 'audio'))
    for iAudioFile in fileList:
        if '%s' %_shotName in iAudioFile:
            pm.sound(file = "%s/%s" %(_audioPathCorrect, iAudioFile), n = '%s_sound' %_shotName, offset = _startTime)
        else:
            pass    

#"playblast"
#-playblast by settings & save to default directory
# -[project]\scene\anim\[shot]\output
def doPlayblast(_pbRes, _outputPath, _shotName, *args, **kwargs):
    if _pbRes == '960 x 400 (Anamorphic 50%)':
        width = 960
        height = 400
    elif _pbRes == '960 x 540 (Full HD 50%)':
        width = 960
        height = 540
    elif _pbRes == '1920 x 800 (Anamorphic 100%)':
        width = 1920
        height = 800
    elif _pbRes == '1920 x 1080 (Full HD 100%)':
        width = 1920
        height = 1080
    pm.playblast( widthHeight = (width,height), 
              filename = "%s/anim/preview/%s_anim.mov" %(_outputPath, _shotName), 
              sound = '%s_sound' %_shotName, 
              format = 'qt', 
              compression = "H.264",
              quality = 70,
              percent = 100,
              forceOverwrite = True,
              sequenceTime = False,
              clearCache = True,
              viewer = False,
              showOrnaments = False,
              offScreen = True )
    
def splitShot_UI():
    splitUI()
