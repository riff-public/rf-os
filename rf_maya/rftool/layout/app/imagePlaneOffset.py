import pymel.core as pm

def imgPlnSetUI():
    if pm.window('imgPlnSetWin', exists = True): 
        pm.deleteUI('imgPlnSetWin')
        
    def imgPlnScaleChkOn(*args):
        ratioTxt.setEnable(True)
        ratioMenu.setEnable(True)
        
    def imgPlnScaleChkOff(*args):
        ratioTxt.setEnable(False)
        ratioMenu.setEnable(False)  
                         
    imgPlnSetWin = pm.window('imgPlnSetWin',
                              title = 'Image Plane Sequence Offset', 
                              mxb = False)
    main = pm.columnLayout(adj = True, rs = 5, cat = ("both",5))
    pm.separator(style = 'none', h = 3, p = main)
    pm.separator(style = 'in', h = 2, p = main)
    pm.text(l = 'Create image plane first, then select image plane node', p = main)
    pm.separator(style = 'in', h = 4, p = main)
    
    grid1 = pm.rowColumnLayout(nc = 2, cs = [(1,15),(2,5)], rs = (1,5), p = main)
    pm.text(l = 'Start frame: ', al = 'right', p = grid1)
    intFrame = pm.intField(value = 1, p = grid1)
    pm.text(l = 'Shots offset: ', al = 'right', p = grid1)
    shotOffset = pm.intField(value = 100, p = grid1)
    pm.text(l = '', p = grid1)
    imgPlnScaleChk = pm.checkBox(l = 'Appy image plane scale', p = grid1, onc = imgPlnScaleChkOn, ofc = imgPlnScaleChkOff)
    ratioTxt = pm.text(l = 'Screen Ratio: ', al = 'right', en = False, p = grid1)
    
    ratioMenu = pm.optionMenu(en = False, p = grid1)
    pm.menuItem(l = '16:9 (Full HD)')
    pm.menuItem(l = '2.35:1 (Anamorphic)')
    
    pm.separator(style = 'none', h = 1, p = main)
    
    pm.button(l = 'Apply', p = main, c = lambda x: operate(intFrame, shotOffset, imgPlnScaleChk, ratioMenu))
    pm.separator(style = 'none', h = 3, p = main)
    pm.showWindow(imgPlnSetWin)
    
def operate(_intFrame, _shotOffset, _imgPlnScaleChk, _ratioMenu, *args, **kwargs):
    intFrame = pm.intField(_intFrame, q = True, v = True)
    shotOffset = pm.intField(_shotOffset, q = True, v = True)
    imgPlnScaleChk = pm.checkBox(_imgPlnScaleChk, q = True, v = True)
    ratioMenu = pm.optionMenu(_ratioMenu, q = True, v = True)
    shots = pm.sequenceManager(listShots = True)
    imgPln = pm.ls(sl = True)
    num = 0
    for i in shots:
        start = pm.shot(i, q = True, startTime = True)
        pm.currentTime(start)
        pm.setKeyframe('%s.fo' %imgPln[0])
        pm.setAttr('%s.frameOffset' %imgPln[0], -((num*shotOffset)+intFrame))
        end = pm.shot(i, q = True, endTime = True)
        pm.currentTime(end)
        pm.setKeyframe('%s.fo' %imgPln[0])
        pm.setAttr('%s.frameOffset' %imgPln[0], -((num*shotOffset)+intFrame))
        num+=1
    pm.keyTangent( 'imagePlaneShape1', itt='linear', ott='linear')
    pm.setAttr("%s.depth" %imgPln[0], 2)
    if imgPlnScaleChk == True:
        if ratioMenu == '16:9 (Full HD)':
            pm.setAttr("%s.sizeX" %imgPln[0], 0.5)
            pm.setAttr("%s.offsetX" %imgPln[0], -0.45)
            pm.setAttr("%s.offsetY" %imgPln[0], -0.25)
        elif ratioMenu == '2.35:1 (Anamorphic)':
            pm.setAttr("%s.sizeX" %imgPln[0], 0.35)
            pm.setAttr("%s.offsetX" %imgPln[0], -0.53)
            pm.setAttr("%s.offsetY" %imgPln[0], -0.2)            
    else:
        pass
