# Python modules
import sys
import os
import tempfile
from collections import OrderedDict

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(logging.NullHandler())

try: 
    # Maya modules
    import maya.cmds as mc
    import pymel.core as pm

    # custom modules
    from rf_utils.context import context_info
    from rf_utils import file_utils
    from rf_utils.pipeline import asset_tag
    from rf_app.anim.animIO import core as animIOCore
    from rf_maya.rftool.utils import maya_utils
    from rf_maya.rftool.shade import shade_utils
    from rf_utils import admin
    from nuTools.misc import removeDagPathNamespace, addDagPathNamespace, addMsgAttr

except ImportError as e:
    print("except", e)

PARENT_ASSET_ATTR = 'parentAsset'
SUBASSET_NAME_ATTR = 'subAssetName'

def get_connected_assets(entity):
    ''' 
    Search the workspace scene for subasset connection data 
    returns:
        [OrderedDict]: subasset data
    '''
    subasset_grp_name = entity.projectInfo.asset.subasset_grp()
    export_grp_name = entity.projectInfo.asset.export_grp()
    if not mc.objExists(subasset_grp_name) or not mc.objExists(export_grp_name):
        return
    geo_grp_name = entity.projectInfo.asset.geo_grp()
    subasset_grp = pm.PyNode(subasset_grp_name)
    export_grp = pm.PyNode(export_grp_name)
    subasset_data = OrderedDict()
    for cons in subasset_grp.getChildren(type='parentConstraint', ad=True):
        # only consider non refered constraint
        if cons.isReferenced():
            continue

        # get constraint child, must be a referenced object
        cons_child = cons.getParent()
        if not cons_child.isReferenced():
            continue

        # init subasset context path info
        subasset_ref = cons_child.referenceFile()
        subasset = context_info.ContextPathInfo(path=subasset_ref.path)

        # sub asset must be a valid Asset
        if not subasset.valid:
            continue

        # get subasset geo_grp
        subasset_ns = subasset_ref.namespace
        geo_grp_name_ns = '{}:{}'.format(subasset_ns, geo_grp_name)
        if len(mc.ls(geo_grp_name_ns)) != 1:
            continue

        # get constraint targets, only allow 1 parent target
        cons_targets = cons.getTargetList()
        if len(cons_targets) != 1:
            continue
        target = cons_targets[0]

        # get shader info
        subasset_geo_grp = pm.PyNode(geo_grp_name_ns)
        look = subasset_geo_grp.attr('look').get() if subasset_geo_grp.hasAttr('look') and subasset_geo_grp.attr('look').get() else ''
        mtrPath = subasset_geo_grp.attr('mtrPath').get() if subasset_geo_grp.hasAttr('mtrPath') and subasset_geo_grp.attr('mtrPath').get() else ''

        # get asset pose
        poses = dict(animIOCore.get_pose(project=entity.project, namespace=subasset_ns, ignore_exceptions=True))

        # target must live under export_grp
        if target.isChildOf(export_grp):
            if subasset_ns not in subasset_data:
                data = {'subAsset': subasset.name,
                        'parentAsset': entity.name,
                        'ref_path': str(subasset_ref.unresolvedPath()),         # ref path to subasset
                        'is_default': cons_child.isVisible(),                   # if subasset is visible, it's default
                        'look': look,
                        'mtrPath': mtrPath,
                        'constraint': [],
                        'poses': poses
                        }
                subasset_data[subasset_ns] = data

            # constraint data
            constraint_data = {'parent': removeDagPathNamespace(target.shortName()),
                            'child': removeDagPathNamespace(cons_child.shortName()),
                            'offset_translate': list(cons.target[0].targetOffsetTranslate.get()),
                            'offset_rotate': list(cons.target[0].targetOffsetRotate.get()),
                            'interpType': cons.interpType.get()}
            subasset_data[subasset_ns]['constraint'].append(constraint_data)

    return subasset_data

def write_subasset_data():
    ''' 
    Write out sub-asset connection data as yml from rig work scene 
    returns:
        [str]: path to subasset data YML
    '''
    entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
    subasset_data = get_connected_assets(entity=entity)

    # write to yml
    if subasset_data:
        subasset_data_path = get_data_path(entity)
        if file_utils.is_writable(subasset_data_path):
            file_utils.ymlDumper(subasset_data_path, subasset_data)
        else:
            fh, dest_path = tempfile.mkstemp(suffix='.yml')
            dest_path = dest_path.replace('\\', '/')
            os.close(fh)
            file_utils.ymlDumper(dest_path, subasset_data)
            # copy using admin module
            admin.copyfile(dest_path, subasset_data_path)
        return subasset_data_path

def get_data_path(entity):
    ''' 
    Get subasset data YML path from given entity
    args:
        entity[ContextPathInfo]: the parent asset entity
    returns:
        [str]: the path to subasset data YML
    '''
    hero_dir = entity.path.hero().abs_path()
    yml_fn = entity.output_name(outputKey='subAssetData', hero=True)
    subasset_data_path = '{}/{}'.format(hero_dir, yml_fn)
    return subasset_data_path

def read_data(entity):
    ''' 
    Read subasset data of the given entity from YML 
    args:
        entity[ContextPathInfo]: the parent asset entity
    returns:
        [OrderedDict]: the subasset data
    '''
    subasset_data_path = get_data_path(entity)
    if os.path.exists(subasset_data_path):
        subasset_data = file_utils.ymlLoader(subasset_data_path)
    else:
        subasset_data = None
    return subasset_data

def load_subasset(namespace, subasset_name, data):
    ''' 
    Load and connect a specific sub-asset of asset entity 
    args:
        namespace[str]: parent asset namespace
        subasset_name[str]: the name of subasset to load
        data[dict]: subasset data
    returns:
        [str]: namespace of loaded subasset
    '''
    # reference subasset asset
    ref_path = os.path.expandvars(data['ref_path'])
    if not os.path.exists(ref_path):
        logger.error('Sub-asset path does not exists: {}'.format(ref_path))
        return
    scene = context_info.ContextPathInfo()
    subasset_ns = maya_utils.get_namespace2('{}_001'.format(data['subAsset']), scene)
    subasset_ref_path = maya_utils.create_reference_asset(subasset_ns, data['ref_path'])

    # get subasset geo grp
    asset = context_info.ContextPathInfo(path=ref_path)
    subasset_geo_grp = '{}:{}'.format(subasset_ns, asset.projectInfo.asset.geo_grp())
    if len(mc.ls(subasset_geo_grp)) != 1:
        logger.error('Cannot find subasset Geo_Grp: {}'.format(subasset_geo_grp))
        return
    subasset_geo_grp = pm.PyNode(subasset_geo_grp)

    # get parent asset geo grp
    parent_asset_geo_grp = '{}:{}'.format(namespace, asset.projectInfo.asset.geo_grp())
    if len(mc.ls(parent_asset_geo_grp)) != 1:
        logger.error('Cannot find parent asset Geo_Grp: {}'.format(parent_asset_geo_grp))
        return
    parent_asset_geo_grp = pm.PyNode(parent_asset_geo_grp)

    # setup constraint
    for cons_data in data['constraint']:
        parent = addDagPathNamespace(cons_data['parent'], namespace+':')
        child = addDagPathNamespace(cons_data['child'], subasset_ns+':')
        if len(mc.ls(parent)) == 1 and len(mc.ls(child)) == 1:
            cons = pm.parentConstraint(parent, child)
            cons.target[0].targetOffsetTranslate.set(cons_data['offset_translate'])
            cons.target[0].targetOffsetRotate.set(cons_data['offset_rotate'])
            cons.interpType.set(cons_data['interpType'])

            # setup subasset name
            asset_tag.add_str_attr(obj=cons.shortName(), attr=SUBASSET_NAME_ATTR, value=subasset_name)

            # setup message connections from parent Geo_Grp.message --> subasset ParentConstraint.parentAsset
            parent_asset_attr = addMsgAttr(obj=cons, attr=PARENT_ASSET_ATTR, multi=False)
            pm.connectAttr(parent_asset_geo_grp.message, parent_asset_attr, f=True)
        else:
            logger.error('Sub-asset connection failed: parent={}, child={}'.format(parent, child))

    # apply shader
    shader_path = os.path.expandvars(data['mtrPath'])
    if shader_path and os.path.exists(shader_path):
        # get shader namespace
        shade_entity = context_info.ContextPathInfo(path=shader_path)
        fn = os.path.basename(shader_path)
        outputList = shade_entity.guess_outputKey(fn)
        outputKey, step = outputList[0]
        shade_entity.context.update(step=step)
        shade_entity.extract_name(fn, outputKey=outputKey)
        shadeNamespace = shade_entity.mtr_namespace
        # get connection info
        connection_info = shade_utils.get_connection_info(namespace=subasset_ns, shadePath=shader_path)
        shade_utils.apply_ref_shade(data['mtrPath'], shadeNamespace, subasset_ns, connection_info)
        # add tag
        asset_tag.add_tag(subasset_geo_grp.shortName(), look=asset.look, mtrPath=data['mtrPath'])

    # apply pose
    poses = data['poses']
    if poses:
        animIOCore.set_pose(poses=poses, namespace=subasset_ns)

    return subasset_ns

def get_asset_entity(obj):
    '''
    Get entity and namespace from given object
    args:
        obj[str]: name of an object that's part of a referenced asset
    returns:
        [tuple]: ContextPathInfo and reference namespace of obj
    '''
    entity, namespace = None, None
    if not mc.referenceQuery(obj, inr=True):
        return entity, namespace

    ref_path = mc.referenceQuery(obj, f=True, withoutCopyNumber=True)
    entity = context_info.ContextPathInfo(path=ref_path)
    if not entity.valid:
        return entity, namespace
    # get namespace
    ref_node = mc.referenceQuery(obj, rfn=True)
    try:
        namespace = mc.referenceQuery(ref_node, namespace=True)[1:]
    except:
        pass

    return entity, namespace

def load_default_subasset(obj):
    '''
    Load and connect only default asset for this asset entity
    args:
        obj[str]: any object that's part of the parent asset
    returns:
        [list]: list of loaded subasset namespaces
    '''
    # get parent entity and namespace
    results = []
    entity, namespace = get_asset_entity(obj)
    if not entity or not namespace:
        return results

    subasset_data = read_data(entity)
    if not subasset_data:
        return results

    
    for subasset_name, data in subasset_data.items():
        if data['is_default']:
            subasset_ns = load_subasset(namespace, subasset_name, data)
            results.append(subasset_ns)
    return results

def load_named_subasset(obj, subasset_name):
    '''
    Load and connect only default asset for this asset entity
    args:
        obj[str]: any object that's part of the parent asset
        subasset_name[str]: name of subasset to load
    returns:
        [str]: loaded subasset namespace
    '''
    # get parent entity and namespace
    entity, namespace = get_asset_entity(obj)
    if not entity or not namespace:
        return

    subasset_data = read_data(entity)
    if not subasset_data:
        return

    result = None
    if subasset_name in subasset_data:
        result = load_subasset(namespace, subasset_name, subasset_data[subasset_name])
    return result

def disconnect_subasset_setups():
    ''' 
    Disconnect all sub-assets from the parent asset 
    returns:
        [list]: names of deleted constraints
    '''
    entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
    subasset_grp_name = entity.projectInfo.asset.subasset_grp()
    export_grp_name = entity.projectInfo.asset.export_grp()
    if not mc.objExists(subasset_grp_name) or not mc.objExists(export_grp_name):
        return
    subasset_grp = pm.PyNode(subasset_grp_name)
    export_grp = pm.PyNode(export_grp_name)

    to_delete = []
    for cons in subasset_grp.getChildren(type='parentConstraint', ad=True):
        # get constraint child
        cons_child = cons.getParent()

        # only consider non refered constraint constraint to a referenced asset
        if not cons.isReferenced() and cons_child.isReferenced():
            # get constraint targets, only allow 1 parent target
            cons_targets = cons.getTargetList()

            # if constraint target is under Export_Grp, collect constraint for deletion
            if [t for t in cons_targets if t.isChildOf(export_grp)]:
                to_delete.append(cons)

    results = []
    if to_delete:
        results = [n.shortName() for n in to_delete]
        pm.delete(to_delete)
    return results

def list_subassets(obj):
    ''' 
    Get a list of sub-assets connected to a parent asset
    args:
        obj[str]: any object that's part of the parent asset
    returns:
        [OrderedDict]: subasset found connected to parent asset {subasset_name: [constraint1, constraint2]}
    '''
    entity, namespace = get_asset_entity(obj)
    results = OrderedDict()
    if not entity or not namespace:
        return results
    geo_grp = '{}:{}'.format(namespace, entity.projectInfo.asset.geo_grp())
    if len(mc.ls(geo_grp)) != 1:
        return
    geo_grp = pm.PyNode(geo_grp)
    geo_grp_connections = geo_grp.message.outputs(type='parentConstraint')
    for node in geo_grp_connections:
        if node.hasAttr(SUBASSET_NAME_ATTR):
            subasset_name = node.attr(SUBASSET_NAME_ATTR).get()
            if subasset_name not in results:
                results[subasset_name] = []
            if node not in results[subasset_name]:
                results[subasset_name].append(node)
    return results

def unload_subasset(obj, subasset_names):
    '''
    Unload and cleanup a subasset from scene
    args:
        obj[str]: any object that's part of the parent asset
        subasset_name[list]: names of subasset to unload
    returns:
        [list]: namespaces of removed subassets
    '''
    current_subassets = list_subassets(obj)
    results = set()
    
    for name, cons in current_subassets.items():
        if name in subasset_names:
            subasset_ref = None
            to_delete = set()
            for con in cons:
                cons_child = con.getParent()
                if cons_child.isReferenced():
                    subasset_ref = cons_child.referenceFile()
                    namespace = subasset_ref.namespace
                    if namespace:
                        results.add(namespace)
                        to_delete.add(con)
            if to_delete:
                pm.delete(list(to_delete))
            if subasset_ref:
                subasset_ref.remove()
    return list(results)

'''
from rf_maya.rftool.subasset import subasset_utils
reload(subasset_utils)

# ----- RIG WORKSPACE
# write data
res = subasset_utils.write_subasset_data()

# disconnect constraint before rig publish
subasset_utils.disconnect_subasset_setups()

# ----- SHOT WORKSPACE
# load all default subassets
sels = mc.ls(sl=True)
# load all default subasset
subasset_utils.load_default_subasset(sels[0])

# load specific subasset
subasset_utils.load_named_subasset(sels[0], 'jasperSword_R')
subasset_utils.load_named_subasset(sels[0], 'jasperSword_L')

# list subasset
subasset_utils.list_subassets(sels[0])

# remove specific subasset
subasset_utils.unload_subasset(sels[0], 'jasperSword_R')
'''