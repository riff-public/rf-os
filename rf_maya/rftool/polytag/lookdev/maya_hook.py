# hook func for polytag lookdev export 
import os
import sys
from collections import OrderedDict
import maya.cmds as mc 
from rftool.polytag import polytag_core
from rftool.utils import path_info
from rftool.utils import maya_utils
from rftool.shade import shade_utils


class Setting: 
	rigRen = 'rigRen'
	shade = 'shade'
	rigGrp = 'Rig_Grp'
	geoGrp = 'Geo_Grp'
	dataExt = 'yml'

def export(rig, shade, res): 
	""" main function for export """
	sels = mc.ls(sl=True, l=True)
	roots = list(set([polytag_core.find_root(a) for a in sels]))
	allExportFiles = OrderedDict()

	for root in roots: 
		id, project, assetName, path = polytag_core.get_attr(root)
		asset = path_info.PathInfo(path=path)

		# re position 
		currentPosData = current_attr(root)
		reset_attr(root)
		
		# create rig 
		mc.select(root)
		maya_utils.create_rig_grp(objs=root, res=res, ctrl=True)
		exportFiles = export_lib(asset, rig, shade, res)

		# bring back
		root = remove_rig(root) 
		restore_pos(root, currentPosData)

		if exportFiles: 
			allExportFiles.update({root: exportFiles})

	return allExportFiles


def export_lib(asset, rig, shade, res): 
	""" export to lib """ 
	rigLibFile = ('/').join([asset.libPath(), asset.libName(Setting.rigRen, res)])
	shadeLibFile = ('/').join([asset.libPath(), asset.libName(Setting.shade, res)])
	shadeLibData = ('/').join([asset.libPath(), asset.libName(Setting.shade, res, ext=Setting.dataExt)])
	result = []

	if rig: 
		mc.select(Setting.rigGrp)
		rigExport = maya_utils.export_selection(rigLibFile, Setting.rigGrp)
		
		if rigExport: 
			result.append(rigExport)

	if shade: 
		shadeExport = shade_utils.export_shade(shadeLibFile, targetGrp=Setting.geoGrp)
		shadeDataExport = shade_utils.export_shade_data(shadeLibData, targetGrp=Setting.geoGrp)
		
		if shadeExport: 
			result.append(shadeExport)
		if shadeDataExport: 
			result.append(shadeDataExport)

	return result


def remove_rig(root): 
	root = root.split('|')[-1]
	root = mc.parent(root, w=True)[0]
	if mc.objExists(Setting.rigGrp): 
		mc.delete(Setting.rigGrp)

	return root
		

def restore_pos(root, data): 
	translate, rotate, scale, order = data
	mc.setAttr('%s.translate' % root, translate[0][0], translate[0][1], translate[0][2])
	mc.setAttr('%s.rotate' % root, rotate[0][0], rotate[0][1], rotate[0][2])
	mc.setAttr('%s.scale' % root, scale[0][0], scale[0][1], scale[0][2])

def reset_attr(obj): 
	mc.setAttr('%s.translate' % obj, 0, 0, 0)
	mc.setAttr('%s.rotate' % obj, 0, 0, 0)
	mc.setAttr('%s.scale' % obj, 1, 1, 1)

def current_attr(obj): 
	translate = mc.getAttr('%s.translate' % obj)
	rotate = mc.getAttr('%s.rotate' % obj)
	scale = mc.getAttr('%s.scale' % obj)
	order = maya_utils.outliner_order(obj)
	return (translate, rotate, scale, order)