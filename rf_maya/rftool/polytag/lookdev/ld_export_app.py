# v.0.0.1 polytag switcher
_title = 'Lookdev Export'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'RFLookdevExport'

#Import python modules
import sys
import os 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework config 
import rf_config as config
from rf_utils import log_utils
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# import framework modules 
from rf_utils.ui import load
import maya_hook as hook 
reload(hook)

class Setting: 
    res = ['md', 'pr']
    rigGrp = 'Rig_Grp'


class PTagLookDevExport(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(PTagLookDevExport, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.init_functions()
        self.init_signals()


    def init_functions(self): 
        self.list_res()

    
    def init_signals(self): 
        self.ui.export_pushButton.clicked.connect(self.export)


    def list_res(self): 
        """ list level from config file """
        allRes = Setting.res
        self.ui.res_comboBox.clear()

        for res in allRes:  
            self.ui.res_comboBox.addItem(res)

    def export(self): 
        """ switch selection """ 
        rig, shade, res = self.get_options()
        if not hook.mc.objExists(Setting.rigGrp): 
            result = hook.export(rig, shade, res)
            self.show_result(result)

        else: 
            QtWidgets.QMessageBox.warning(self, 'Warning', '%s exists in the scene. Cannot export' % Setting.rigGrp)


    def get_options(self): 
        """ get ui options """
        rig = self.ui.rig_checkBox.isChecked()
        shade = self.ui.shade_checkBox.isChecked()
        res = str(self.ui.res_comboBox.currentText())
        return rig, shade, res


    def show_result(self, result): 
        for k, files in result.iteritems(): 
            logger.debug(k)
            for file in files: 
                logger.debug('- %s' % file)


def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = PTagLookDevExport(maya_win.getMayaWindow())
    return myApp
