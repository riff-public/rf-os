# work around for non-reference asset 
# give path and asset infomation tag on ply geometry
import os
import sys 
import maya.cmds as mc 
import maya.mel as mm 
from rftool.utils import maya_utils
from rftool.utils import path_info
from functools import partial 
from collections import OrderedDict
import logging
logger = logging.getLogger(__name__)

class Tag: 
    attrs = OrderedDict()
    id = 'id'
    project = 'project'
    assetName = 'assetName'
    path = 'path'
    assetData = 'assetData'
    root = 'root'
    isRoot = 'isRoot'

    attrs[id] = {'data': 'long'}
    attrs[project] = {'data': 'string'}
    attrs[assetName] = {'data': 'string'}
    attrs[path] = {'data': 'string'}
    attrs[assetData] = {'data': 'string'}
    attrs[root] = {'data': 'string'}
    attrs[isRoot] = {'data': 'bool'}

class Asset: 
    rootCtrl = 'Place_Ctrl'


def add_tag(objs): 
    """ add tag to input objects """ 
    for obj in objs: 
        for attr, dataType in Tag.attrs.iteritems(): 
            if not mc.objExists('%s.%s' % (obj, attr)): 
                if dataType.get('data') == 'string': 
                    add_string(obj, attr)

                if dataType.get('data') == 'long': 
                    add_int(obj, attr)

                if dataType.get('data') == 'bool': 
                    add_bool(obj, attr)


def set_root_tag(obj, project='', assetName='', path='', scene=False, autoId=False, id=0, reTag=False): 
    """ use this function to set root tag """ 
    root = obj.split('|')[-1]
    isRoot = True
    objln = mc.ls(obj, l=True)[0]
    if scene: 
        asset = path_info.PathInfo()
        project = asset.project 
        assetName = asset.name
        path = asset.path 

    if project and assetName and path: 
        if autoId: 
            id = find_asset_id(project, assetName)
        add_data(obj, id, project, assetName, path, root, isRoot, reTag=reTag)

        meshes = mc.listRelatives(obj, ad=True, type='mesh', f=True)
        if meshes: 
            transformMeshes = list(set([mc.listRelatives(a, p=True, f=True)[0] for a in meshes]))

            if objln in transformMeshes: 
                transformMeshes.remove(objln)

            if transformMeshes: 
                for ply in transformMeshes: 
                    add_data(ply, id, project, assetName, path, root, isRoot=False, reTag=reTag)

    else: 
        logger.debug('Missing data')
        logger.debug('project : %s' % project)
        logger.debug('assetName : %s' % assetName)
        logger.debug('path : %s' % path)


def find_asset_id(project, assetName): 
    """ find asset id from shotgun """
    from rftool.utils import sg_process
    entity = sg_process.sg.find_one('Asset', [['project.Project.name', 'is', project], ['code', 'is', assetName]], ['code', 'sg_asset_type', 'sg_subtype', 'id'])
    id = 0 
    if entity: 
        id = entity.get('id')
    return id


def add_data(obj, id, project, assetName, path, root, isRoot, reTag=False): 
    add_int(obj, Tag.id, id, reTag=reTag)
    add_string(obj, Tag.project, project, reTag=reTag)
    add_string(obj, Tag.assetName, assetName, reTag=reTag)
    add_string(obj, Tag.path, path, reTag=reTag)
    add_string(obj, Tag.root, root, reTag=reTag)
    add_bool(obj, Tag.isRoot, isRoot, reTag=reTag)


def add_string(obj, attr, value=None, reTag=False): 
    objAttr = '%s.%s' % (obj, attr)
    if reTag: 
        if mc.objExists(objAttr): 
            mc.deleteAttr(obj, at=attr)
    if not mc.objExists(objAttr): 
        mc.addAttr(obj, ln=attr, dt='string')
        mc.setAttr('%s.%s' % (obj, attr), e=True, keyable=True)
    if value: 
        mc.setAttr(objAttr, value, type='string')

    return objAttr

def add_int(obj, attr, value=None, reTag=False, keyable=False): 
    objAttr = '%s.%s' % (obj, attr)
    if reTag: 
        if mc.objExists(objAttr): 
            mc.deleteAttr(obj, at=attr)
    if not mc.objExists(objAttr): 
        mc.addAttr(obj, ln=attr, at='long', min=0, dv=0)
        mc.setAttr('%s.%s' % (obj, attr), e=True, keyable=keyable)
    if value: 
        mc.setAttr(objAttr, value)

    return objAttr

def add_bool(obj, attr, value=None, reTag=False, keyable=False): 
    objAttr = '%s.%s' % (obj, attr)
    if reTag: 
        if mc.objExists(objAttr): 
            mc.deleteAttr(obj, at=attr)
    if not mc.objExists(objAttr): 
        mc.addAttr(obj, ln=attr, at='bool')
        mc.setAttr('%s.%s' % (obj, attr), e=True, keyable=keyable)
    if value: 
        mc.setAttr(objAttr, value)

    return objAttr


def list_polytag(transforms=None): 
    meshTransforms = []
    info = dict()
    
    if transforms: 
        meshTransforms = transforms
    if not transforms: 
        meshes = mc.ls(type='mesh')

    if not meshTransforms: 
        meshTransforms = [mc.listRelatives(a, p=True, f=True)[0] for a in meshes if mc.listRelatives(a, p=True)]
        
    if meshTransforms: 
        for mesh in meshTransforms: 
            if check_attr(mesh): 
                root = find_root(mesh)
                
                if root: 
                    id, project, assetName, path = get_attr(root)
                    asset = path_info.PathInfo(path=path)

                    if not assetName in info.keys(): 
                        info[assetName] = [asset, [root]]

                    else: 
                        if not root in info[assetName][1]: 
                            info[assetName][1].append(root)

    return info


def switch_selection(level): 
    sels = mc.ls(sl=True, l=True)
    info = list_polytag(transforms=sels)
    success = []
    missingRefs = []

    if sels and info: 
        for assetName, values in info.iteritems(): 
            asset, plys = values
            result, missingRef, failRef = switch(plys, level, mode='normal')
            success+=result
            missingRefs+=missingRef

            if len(plys) == len(result): 
                logger.info('switch %s success %s' % (assetName, plys))

            if failRef: 
                logger.warning('Failed assets %s' % failRef)

    mm.eval('hyperShadePanelMenuCommand("hyperShadePanel1", "deleteUnusedNodes");')

    return {'skip': [a for a in sels if not a in success], 'missingRef': missingRefs}


def switch(roots, level, mode='normal'): 
    """ switch given plys to different level """ 
    inputPly = None 
    success = []
    missingRef = []
    failRef = []

    for i, root in enumerate(roots): 
        id, project, assetName, path = get_attr(root)
        asset = path_info.PathInfo(path=path)
        refPath = '%s/%s_%s' % (asset.libPath(), asset.name, level)

        if os.path.exists(refPath): 
            plcAsset = place_asset(refPath, inputPly=inputPly)
            if plcAsset: 
                transfer_attr(root, plcAsset, refPath)
                plcAsset = snap_target(plcAsset, root)
                remove_obj(root)
                rootSn = root.split('|')[-1]
                plcAsset = rename_node(plcAsset, rootSn)

                if i == 0 and mode == 'duplicate': 
                    inputPly = plcAsset

                success.append(root)
            else: 
                failRef.append(refPath)

        else: 
            missingRef.append(refPath)


    return success, missingRef, failRef


def remove_obj(obj): 
    if not mc.referenceQuery(obj, inr=True): 
        mc.delete(obj)
    else: 
        path = mc.referenceQuery(obj, f=True)
        maya_utils.remove_reference(path)


def place_asset(refPath, inputPly=None): 
    """ if inputPly, use inputPly to duplicate """ 
    if not inputPly: 
        placeAssets = import_asset(refPath)

    if inputPly: 
        placeAssets = mc.duplicate(inputPly)[0]
        placeAssets = mc.parent(placeAssets, w=True)[0]

    return placeAssets


def import_asset(refPath): 
    """ import asset and findRoot """ 
    current = mc.ls(assemblies=True, l=True)
    ext = os.path.splitext(refPath)[-1]

    if ext == '.ma': 
        mc.file(refPath, i=True)
        placeAssets = [a for a in mc.ls(assemblies=True, l=True) if not a in current]

        # in the pipeline, only Rig_Grp 
        if len(placeAssets) == 1: 
            root = None 
            childs = mc.listRelatives(placeAssets[0], ad=True, f=True)
            for child in childs: 
                if mc.objExists('%s.%s' % (child, Tag.isRoot)): 
                    isRoot = mc.getAttr('%s.%s' % (child, Tag.isRoot))
                    if isRoot: 
                        root = mc.parent(child, w=True)[0]
                        break

            # polytag format
            if root: 
                mc.delete(placeAssets)
                return root

            # outside polytag
            else: 
                placeAsset = None
                plys = maya_utils.find_ply(placeAssets[0], f=True)

                if len(plys) == 1: 
                    placeAsset = mc.parent(plys[0], w=True)[0]

                if len(plys) > 1: 
                    placeAsset = mc.polyUnite(plys, mergeUVSets=True, ch=False, n='tmpAsset')

                print '%s No polygon found, skip' % refPath
                logger.error('%s No polygon found, skip' % refPath)

                mc.delete(placeAssets)
                return placeAsset


    if ext == '.abc': 
        gpuNode = mc.createNode('gpuCache')
        mc.setAttr('%s.cacheFileName' % gpuNode, refPath, type='string')
        transform = mc.listRelatives(gpuNode, p=True)[0]
        add_tag_gpu(transform)
        return transform


def reference_asset(namespace, refPath): 
    path = mc.file(refPath, r=True, ns=namespace)
    ns = mc.file(path, q=True, namespace=True)
    ctrl = '%s:%s' % (ns, Asset.rootCtrl)
    print path, ctrl 

    if mc.objExists(ctrl): 
        return ctrl 
    else: 
        print 'removing ...'
        mc.file(path, rr=True)



def snap_target(srcPly, target): 
    mc.delete(mc.parentConstraint(target, srcPly))
    mc.delete(mc.scaleConstraint(target, srcPly))
    targetOrder = maya_utils.outliner_order(target)

    targetParent = find_parent(target)
    srcParent = mc.listRelatives(srcPly, p=True)

    if not srcParent == targetParent: 
        # result = mc.parent(srcPly, targetParent)[0]
        result = maya_utils.parent_order(srcPly, targetParent, targetOrder)[0]
        # print result
        return result 
    return srcPly


def find_parent(obj): 
    """ find top parent for the object """ 
    if not mc.referenceQuery(obj, inr=True): 
        targetParent = mc.listRelatives(obj, p=True)
    else: 
        topRef = maya_utils.find_top_reference(obj)
        targetParent = mc.listRelatives(topRef, p=True)
    return targetParent


def rename_node(src, dst): 
    result = mc.rename(src, dst)
    # check if gpu in dst 
    shape = mc.listRelatives(result, s=True, f=True)

    if shape: 
        if mc.objectType(shape[0], isType='gpuCache'): 
            mc.rename(shape[0], '%s_gpu' % dst)

    return result


def is_root(ply): 
    """ check if this is a root """ 
    if ply: 
        if mc.objExists('%s.%s' % (ply, Tag.isRoot)): 
            isRoot = mc.getAttr('%s.%s' % (ply, Tag.isRoot))
            if isRoot: 
                if mc.objExists('%s.%s' % (ply, Tag.assetName)) and mc.objExists('%s.%s' % (ply, Tag.path)): 
                    return True



def find_root(ply): 
    plyln = mc.ls(ply, l=True)[0]
    plyList = plyln.split('|')

    for i, ply in enumerate(plyList): 
        if i == 0: 
            cmbList = plyList
        else: 
            cmbList = plyList[:-i]
        ln = ('|').join(cmbList) # long name
        sn = cmbList[-1] # short name 
        # root = mc.getAttr('%s.%s' % (ln, Tag.root))

        if is_root(ln): 
            return ln


def is_polytag(ply): 
    # not ref 
    if not mc.referenceQuery(ply, inr=True): 
        return True if find_root(ply) else False
    return False

def is_rig(ply): 
    if mc.referenceQuery(ply, inr=True): 
        return True if find_root_ctrl(ply) else False
    return False 

def find_root_ctrl(ply): 
    # find Place_Ctrl
    ctrl = 'Place_Ctrl'
    if ':' in ply: 
        sn = ply.split('|')[-1]
        targetCtrl = sn.replace(':%s' % ply.split(':')[-1], ':%s' % ctrl)
        if mc.objExists(targetCtrl): 
            return targetCtrl


def get_attr(ply): 
    id = mc.getAttr('%s.%s' % (ply, Tag.id))
    assetName = mc.getAttr('%s.%s' % (ply, Tag.assetName))
    path = mc.getAttr('%s.%s' % (ply, Tag.path))
    project = mc.getAttr('%s.%s' % (ply, Tag.project))

    return id, project, assetName, path

def transfer_attr(srcObj, dstObj, path): 
    isRoot = True
    id = mc.getAttr('%s.%s' % (srcObj, Tag.id))
    project = mc.getAttr('%s.%s' % (srcObj, Tag.project))
    assetName = mc.getAttr('%s.%s' % (srcObj, Tag.assetName)) 
    root = dstObj
    add_data(dstObj, id, project, assetName, path, root, isRoot, reTag=False)


def check_attr(ply): 
    projectAttr = '%s.%s' % (ply, Tag.project)
    assetAttr = '%s.%s' % (ply, Tag.assetName)
    pathAttr = '%s.%s' % (ply, Tag.path)
    isRootAttr = '%s.%s' % (ply, Tag.isRoot)

    if mc.objExists(projectAttr) and mc.objExists(assetAttr) and mc.objExists(pathAttr) and mc.objExists(isRootAttr): 
        return True


def select_tag(tag=False): 
    plys = mc.ls(type='mesh')

    if plys: 
        transforms = [mc.listRelatives(a, p=True)[0] for a in plys]
    if not tag: 
        noTagPlys = [a for a in transforms if not check_attr(a)]
        if noTagPlys: 
            mc.select(noTagPlys)

    if tag: 
        tagPlys = [a for a in transforms if check_attr(a)]
        if tagPlys: 
            mc.select(tagPlys)


def assign_tag(): 
    geoGrp = 'Geo_Grp'

    if mc.objExists(geoGrp): 
        childs = mc.listRelatives(geoGrp, c=True)
        childs = [a for a in childs if not 'Constraint' in mc.objectType(a)]

        if len(childs) == 1: 
            set_root_tag(childs[0], scene=True, reTag=True, autoId=True)

        else: 
            logger.warning('childs more than 1')



def polytag_to_sel(assetName=None): 
    sels = mc.ls(sl=True)
    delGrp = 'deleteGrp'

    if len(sels) > 1: 
        src = sels[0]
        targets = sels[1:]
        if not assetName: 
            id, project, assetName, path = get_attr(src)
        name = '%s_001' % assetName
        assetGrp = '%s_Grp' % assetName

        for i, target in enumerate(targets): 
            newName = maya_utils.get_asset_name(name)
            dup = mc.duplicate(src)[0]
            dup = mc.rename(dup, newName)
            mc.delete(mc.parentConstraint(target, dup))
            # mc.delete(mc.scaleConstraint(target, dup))
            
            if not mc.objExists(assetGrp): 
                assetGrp = mc.group(n=assetGrp, em=True)

            if not mc.objExists(delGrp): 
                delGrp = mc.group(n=delGrp, em=True)

            mc.parent(target, delGrp)
            mc.parent(dup, assetGrp)


def add_tag_gpu(node): 
    """ add tag to gpu """ 
    cachePathAttr = '%s.cacheFileName' % node
    
    if mc.objExists(cachePathAttr): 
        path = mc.getAttr(cachePathAttr)
        asset = path_info.PathInfo(path=path)
        set_root_tag(node, project=asset.project, assetName=asset.name, path=path, scene=False, autoId=True, reTag=True)


def add_tag_all_gpus(): 
    gpus = mc.ls(type='gpuCache')
    transforms = [mc.listRelatives(a, p=True)[0] for a in gpus]

    for node in transforms: 
        add_tag_gpu(node)


def polytag_to_reference(level='rig_md.ma'): 
    """ switching polytag form to reference node """ 
    sels = mc.ls(sl=True, l=True)
    roots = [find_root(a) for a in sels if is_polytag(a)]
    
    for root in roots: 
        id, project, assetName, path = get_attr(root)
        asset = path_info.PathInfo(path=path)
        refPath = '%s/%s_%s' % (asset.libPath(), asset.name, level)
        ctrl = reference_asset(root.split('|')[-1], refPath)
        topNode = maya_utils.find_top_reference(ctrl)

        if ctrl: 
            snap_reference(root, ctrl)
            mc.delete(root)


def snap_reference(srcObj, target): 
    # snap 
    snap(srcObj, target)
    srcOrder = maya_utils.outliner_order(srcObj)
    topNode = maya_utils.find_top_reference(target)

    srcParent = find_parent(srcObj)
    targetParent = mc.listRelatives(topNode, p=True)

    if not srcParent == targetParent: 
        result = maya_utils.parent_order(topNode, srcParent, srcOrder)[0]
        return result

def reference_to_polytag(level='rig_md.ma'): 
    """ switching polytag form to reference node """ 
    sels = mc.ls(sl=True, l=True)
    namespaces = list(set([maya_utils.get_node_namespace(a) for a in sels if is_rig(a)]))
    ctrls = ['%s:%s' % (a, Asset.rootCtrl) for a in namespaces]
    
    for ctrl in ctrls: 
        path = mc.referenceQuery(ctrl, f = True)
        asset = path_info.PathInfo(path=path)
        namespace = mc.referenceQuery(path, ns=True)
        refPath = '%s/%s_%s' % (asset.libPath(), asset.name, level)
        topNode = maya_utils.find_top_reference(ctrl)
        
        plcAsset = place_asset(refPath, inputPly=None)
        snap(ctrl, plcAsset)
        plcAsset = replace_outliner_position(topNode, plcAsset)
        plcAsset = rename_node(plcAsset, namespace)
        mc.file(path, rr=True)



def snap(src, dst): 
    # snap 
    mc.delete(mc.parentConstraint(src, dst))
    mc.delete(mc.scaleConstraint(src, dst))


def replace_outliner_position(oldObject, newObject): 
    oldOrder = maya_utils.outliner_order(oldObject)
    oldParent = mc.listRelatives(oldObject, p=True)
    newParent = mc.listRelatives(newObject, p=True)

    if not oldParent == newParent: 
        result = maya_utils.parent_order(newObject, oldParent, oldOrder)[0]
        return result


def polytag_to_selection(polytagObj, targetObjs, rename=False): 
    """ duplicate polytag to targetObjects """ 
    src = polytagObj
    dst = targetObjs

    for obj in dst: 
        order = maya_utils.outliner_order(obj)    
        oldParent = mc.listRelatives(obj, p=True)[0]
        dup = mc.duplicate(src)[0]
        snap(obj, dup)
        maya_utils.parent_order(dup, oldParent, order)
        mc.delete(obj)

        if rename: 
            mc.rename(dup, obj.split('|')[-1])
