# v.0.0.1 polytag switcher
_title = 'Polytag Switcher'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'RFPolytagSwitcher'

# config 
appConfig = 'app_config.yml'

#Import python modules
import sys
import os 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework config 
import rf_config as config
from rf_utils import log_utils
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

# import framework modules 
from rf_utils.ui import load
from rf_utils import file_utils
from rftool.polytag import polytag_core


class PolytagSwithcer(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(PolytagSwithcer, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        # config
        self.config = file_utils.ymlLoader('%s/%s' % (moduleDir, appConfig))

        self.init_functions()
        self.init_signals()


    def init_functions(self): 
        self.list_level()

    
    def init_signals(self): 
        self.ui.switch_pushButton.clicked.connect(self.switch)


    def list_level(self): 
        """ list level from config file """
        levels = self.config.get('level')
        self.ui.level_listWidget.clear()

        for level, value in levels.iteritems(): 
            item = QtWidgets.QListWidgetItem(self.ui.level_listWidget)
            item.setText(level)
            item.setData(QtCore.Qt.UserRole, value)   

    def switch(self): 
        """ switch selection """ 
        level = self.ui.level_listWidget.currentItem().data(QtCore.Qt.UserRole)
        result = polytag_core.switch_selection(level)
        skip = result['skip']
        missingRef = result['missingRef']

        if skip or missingRef: 
            message = 'Not all selected assets can be switched. Press OK to show'
            messageBoxResult = QtWidgets.QMessageBox.warning(self, 'Warning', message, QtWidgets.QMessageBox.Ok, QtWidgets.QMessageBox.Cancel)

            if messageBoxResult == QtWidgets.QMessageBox.Ok: 
                polytag_core.mc.select(skip)
                logger.info('Missing refs \n%s' % ('\n').join(missingRef))



def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = PolytagSwithcer(maya_win.getMayaWindow())
    return myApp
