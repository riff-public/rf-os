# v.0.0.1 polytag switcher
_title = 'Polytag Utilities'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'PolytagUtilitiesUI'

#Import python modules
import sys
import os 
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework modules 
import rf_config as config
from rf_utils import log_utils
from rf_utils.ui import load
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from rftool.polytag import polytag_core
reload(polytag_core)

class Level: 
    rig = 'rig_md.ma'
    gpu = 'gpu_md.abc'


class PolytagUtilities(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(PolytagUtilities, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        
        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))

        self.init_signals()


    def init_signals(self): 
        # swtich 
        self.ui.polytagToRig_pushButton.clicked.connect(self.polytagToRig)
        self.ui.rigToPolytag_pushButton.clicked.connect(self.rigToPolytag)

        # utils 
        self.ui.tagGpu_pushButton.clicked.connect(self.tag_gpu)
        self.ui.tagAllGpus_pushButton.clicked.connect(self.tag_all_gpus)


    def polytagToRig(self): 
        polytag_core.polytag_to_reference(Level.rig)


    def rigToPolytag(self): 
        mode = 'model' if self.ui.model_radioButton.isChecked() else 'gpu' if self.ui.gpu_radioButton.isChecked() else None
        if mode: 
            level = Level.gpu if mode == 'gpu' else Level.rig if mode == 'model' else Level.gpu
            polytag_core.reference_to_polytag(level)

    
    def tag_gpu(self): 
        sels = polytag_core.mc.ls(sl=True)

        for node in sels: 
            polytag_core.add_tag_gpu(node)

    def tag_all_gpus(self): 
        polytag_core.add_tag_all_gpus()


def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = PolytagUtilities(maya_win.getMayaWindow())
    return myApp
