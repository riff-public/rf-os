import maya.cmds as mc

def run ():
    nameCtrl = ('HairA1Fk_Ctrl','HairB1Fk_Ctrl','HairC1Fk_Ctrl','HairD1Fk_Ctrl',
                'HairE1Fk_Ctrl','HairF1Fk_Ctrl','HairG1Fk_Ctrl','HairH1Fk_Ctrl',
                'HairJ1Fk_Ctrl','HairK1Fk_Ctrl','HairL1Fk_Ctrl','HairM1Fk_Ctrl',
                'HairN1Fk_Ctrl','HairO1Fk_Ctrl','HairP1Fk_Ctrl','HairQ1Fk_Ctrl',
                'HairR1Fk_Ctrl','HairS1Fk_Ctrl','HairT1Fk_Ctrl','HairU1Fk_Ctrl',
                'HairV1Fk_Ctrl','HairW1Fk_Ctrl','HairX1Fk_Ctrl','HairY1Fk_Ctrl',
                'HairZ1Fk_Ctrl','HairAA1Fk_Ctrl','HairBB1Fk_Ctrl','HairCC1Fk_Ctrl',
                'HairDD1Fk_Ctrl','HairEE1Fk_Ctrl','HairFF1Fk_Ctrl','HairGG1Fk_Ctrl',
                'HairHH1Fk_Ctrl','HairII1Fk_Ctrl')
                
    sel = mc.ls(sl=True)

    if ':' in sel[0] :
        nameSpace = sel[0].split(':')[0]

    mc.select( cl = True )

    for ctrl in nameCtrl :
        mc.select( '%s:%s' %(nameSpace,ctrl) , tgl = True )