import maya.cmds as mc
import pymel.core as pm
from rftool.rig.ncmel import match
reload(match)

def matchAllAllFrameRange(NS = '', st=0, end=1 , fkIk=0):
	sel_switch = mc.ls(sl=True) 

	if sel_switch:
		rLegCtrlKeyList = []
		lLegCtrlKeyList= []
		rArmCtrlKeyList = []
		lArmCtrlKeyList= []
		ctrlKeyList =[]
	####### leg Left ###################   
		legSwitchL = 'Leg_L_Ctrl'
		legL = 'UpLegFk_L_Ctrl'
		kneeL = 'LowLegFk_L_Ctrl'
		ankleL = 'AnkleFk_L_Ctrl'
		
		legIKL = 'UpLegIk_L_Ctrl'
		kneeIKL = 'KneeIk_L_Ctrl'
		ankleIKL = 'AnkleIk_L_Ctrl'
		
	####### leg Right ###################        
		legSwitchR = 'Leg_R_Ctrl'
		legR = 'UpLegFk_R_Ctrl'
		kneeR = 'LowLegFk_R_Ctrl'
		ankleR = 'AnkleFk_R_Ctrl'    
		
		legIKR = 'UpLegIk_R_Ctrl'
		kneeIKR = 'KneeIk_R_Ctrl'
		ankleIKR = 'AnkleIk_R_Ctrl'
		

	########case Leg #################################################################
		lleg = legL
		lknee = kneeL
		lankle = ankleL
		llegSwitch = legSwitchL
		
		llegIK = legIKL
		lkneeIK = kneeIKL
		lankleIK = ankleIKL
				
		rleg = legR
		rknee = kneeR
		rankle = ankleR
		rlegSwitch = legSwitchR
		rlegIK = legIKR
		rkneeIK = kneeIKR
		rankleIK = ankleIKR
							
		lLegCtrlList = (lleg,lknee,lankle)
		lLegSwitchName = '%s:%s'%(NS,llegSwitch)
		lLegIKCtrlList = (llegIK,lkneeIK,lankleIK)

		rLegCtrlList = (rleg,rknee,rankle)
		rLegSwitchName = '%s:%s'%(NS,rlegSwitch)
		rLegIKCtrlList = (rlegIK,rkneeIK,rankleIK)


	####### Arm Left #######################   
		armSwitchL = 'Arm_L_Ctrl'
		armL = 'UpArmFk_L_Ctrl'
		elbowL = 'ForearmFk_L_Ctrl'
		wristL = 'WristFk_L_Ctrl'

		armIKL = 'UpArmIk_L_Ctrl'
		elbowIKL = 'ElbowIk_L_Ctrl'
		wristIKL = 'WristIk_L_Ctrl'
		
	####### Arm Right #######################
		armSwitchR = 'Arm_R_Ctrl'        
		armR = 'UpArmFk_R_Ctrl'
		elbowR = 'ForearmFk_R_Ctrl'
		wristR = 'WristFk_R_Ctrl'    

		armIKR = 'UpArmIk_R_Ctrl'
		elbowIKR = 'ElbowIk_R_Ctrl'
		wristIKR = 'WristIk_R_Ctrl'      

	########case Arm #################################################################

		larm= armL
		lelbow = elbowL
		lwrist = wristL
		larmSwitch = armSwitchL
		
		larmIK = armIKL
		lelbowIK = elbowIKL
		lwristIK = wristIKL
		
		rarm= armR
		relbow = elbowR
		rwrist = wristR
		rarmSwitch = armSwitchR
		
		rarmIK = armIKR
		relbowIK = elbowIKR
		rwristIK = wristIKR

		lArmCtrlList = (larm,lelbow,lwrist)
		lArmSwitchName = '%s:%s'%(NS,larmSwitch)
		lArmIKCtrlList = (larmIK,lelbowIK,lwristIK)

		rArmCtrlList = (rarm,relbow,rwrist)
		rArmSwitchName = '%s:%s'%(NS,rarmSwitch)
		rArmIKCtrlList = (rarmIK,relbowIK,rwristIK)

		if lLegSwitchName in sel_switch:
			for ctrl in lLegCtrlList:
				ctrlName = '%s:%s'%(NS,ctrl)
				keys = mc.keyframe(ctrlName,q=1,timeChange=1)
				if keys != None:
					lLegCtrlKeyList = set(list(lLegCtrlKeyList) + list(keys))

		if rLegSwitchName in sel_switch:			
			for ctrl in rLegCtrlList:
				ctrlName = '%s:%s'%(NS,ctrl)
				keys = mc.keyframe(ctrlName,q=1,timeChange=1)
				if keys != None:
					rLegCtrlKeyList = set(list(rLegCtrlKeyList) + list(keys))
		 
		if lArmSwitchName in sel_switch:
			for ctrl in lArmCtrlList:
				ctrlName = '%s:%s'%(NS,ctrl)
				keys = mc.keyframe(ctrlName,q=1,timeChange=1)
				if keys != None:
					lArmCtrlKeyList = set(list(lArmCtrlKeyList) + list(keys))

		if rArmSwitchName in sel_switch:
			for ctrl in rArmCtrlList:
				ctrlName = '%s:%s'%(NS,ctrl)
				keys = mc.keyframe(ctrlName,q=1,timeChange=1)
				if keys != None:
					rArmCtrlKeyList = set(list(rArmCtrlKeyList) + list(keys)) 

		ctrlKeyList = set(list(rLegCtrlKeyList)+list(lLegCtrlKeyList)+list(rArmCtrlKeyList)+list(lArmCtrlKeyList))

		if fkIk:
			def_val = 0
			rev_val = 1
		else:			
			def_val = 1
			rev_val = 0

		for key in ctrlKeyList:
			if key >= st and key <=end:
				mc.currentTime(key,e=1)

				if key in lLegCtrlKeyList:
					if mc.getAttr(lLegSwitchName+'.fkIk')== def_val:
						mc.setAttr(lLegSwitchName+'.fkIk',rev_val)
					mc.select(lLegSwitchName)
					match.run()
					for IKCon in lLegIKCtrlList:
						IKCtrl = '%s:%s'%(NS,IKCon) 
						mc.setKeyframe(IKCtrl,s=0)

				if key in rLegCtrlKeyList:
					if mc.getAttr(rLegSwitchName+'.fkIk')== def_val:
						mc.setAttr(rLegSwitchName+'.fkIk',rev_val)             
					mc.select(rLegSwitchName)
					match.run()
					for IKCon in rLegIKCtrlList:
						IKCtrl = '%s:%s'%(NS,IKCon) 
						mc.setKeyframe(IKCtrl,s=0)

				if key in lArmCtrlKeyList:
					if mc.getAttr(lArmSwitchName+'.fkIk')== def_val:
						mc.setAttr(lArmSwitchName+'.fkIk',rev_val)
					mc.select(lArmSwitchName)
					match.run()
					for IKCon in lArmIKCtrlList:
						IKCtrl = '%s:%s'%(NS,IKCon) 
						mc.setKeyframe(IKCtrl,s=0)

				if key in rArmCtrlKeyList:
					if mc.getAttr(rArmSwitchName+'.fkIk')== def_val:
						mc.setAttr(rArmSwitchName+'.fkIk',rev_val)             
					mc.select(rArmSwitchName)
					match.run()
					for IKCon in rArmIKCtrlList:
						IKCtrl = '%s:%s'%(NS,IKCon) 
						mc.setKeyframe(IKCtrl,s=0)


def mocap_ik_to_fk():
	sel = pm.selected()
	ns_list = []
	char_sel_list = []
	for i in sel:
		ns_list.append(i.namespace().__str__())
	ns_list = list(set(ns_list))
	
	for ns in ns_list:
		obj_list = []
		for obj in sel:
			if ns in obj.name():
				obj_list.append(obj.name())
		char_sel_list.append(obj_list)
	
	start = mc.playbackOptions(min=1,q=1)
	end = mc.playbackOptions(max=1,q=1)
	
	for num,ns in enumerate (ns_list):
		mc.select(char_sel_list[num])
		matchAllAllFrameRange(ns[:-1],start,end,1)


def mocap_fk_to_ik():
	sel = pm.selected()
	ns_list = []
	char_sel_list = []
	for i in sel:
		ns_list.append(i.namespace().__str__())
	ns_list = list(set(ns_list))
	
	for ns in ns_list:
		obj_list = []
		for obj in sel:
			if ns in obj.name():
				obj_list.append(obj.name())
		char_sel_list.append(obj_list)
	
	start = mc.playbackOptions(min=1,q=1)
	end = mc.playbackOptions(max=1,q=1)
	
	for num,ns in enumerate (ns_list):
		mc.select(char_sel_list[num])
		matchAllAllFrameRange(ns[:-1],start,end,0)