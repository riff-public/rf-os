# v.0.0.1 beta
_title = 'Key Shifter'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'KeyShifterUi'

#Import python modules
import sys
import os
import logging
import maya.cmds as mc 
from rf_maya.lib import sequencer_lib
reload(sequencer_lib)

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


def deleteUI(ui): 
    if mc.window(ui, exists=True): 
        mc.deleteUI(ui)
        deleteUI(ui)


class KeyShifter(object):
    def __init__(self):
        #Setup Window
        super(KeyShifter, self).__init__()
        self.w = 200
        self.h = 100

    def show(self): 
        """ ui function """ 
        deleteUI(uiName)
        mc.window(uiName, t='%s %s %s' % (_title, _version, _des))
        mc.columnLayout(adj=1, rs=4)

        mc.text(l='Shift Key')
        mc.separator()
        mc.text(l='Enter Number of Frame to Shift')
        self.inputKeyTX = mc.textField()
        self.button = mc.button(l='Shift', c=self.shift, h=30)
        mc.showWindow(uiName)
        mc.window(uiName, e=True, wh=[self.w, self.h])

    def shift(self, *args): 
        offsetKey = mc.textField(self.inputKeyTX, q=True, tx=True)
        sequencer_lib.shift_key(type = 'default', frameRange = [1, 10000], frame = int(offsetKey))
        logger.info('Shifted %s frame' % offsetKey)

        # else: 
        #     mc.confirmDialog(title='Error', message='"%s" is invalid Value. Enter only Number' % offsetKey, button=['Yes'])


def show(): 
    app = KeyShifter()
    app.show()
    return app
