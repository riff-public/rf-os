import os
import maya.mel as mm
import maya.cmds as mc

def call():
    
    version = mc.about(version=True)
    script_path = os.environ['RFSCRIPT'].replace('\\', '/')
    cmds = 'loadPlugin -qt "%s/core/rf_maya/plugins/%s/AnimSchoolPicker.mll";' %(script_path, version)
    cmdsCall = 'AnimSchoolPicker();'
    mm.eval(cmds)
    mm.eval(cmdsCall)