import os, sys
import itertools
from functools import partial
from collections import OrderedDict

import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel
import maya.OpenMaya as om
import maya.OpenMayaAnim as oma

from rf_utils.context import context_info
from rf_utils.widget import dialog
# reload(dialog)

class UnlockAnimLayer():
    def __init__(self): 
        self.animLayers = mc.ls(type='animLayer')
        self.lockLayers = []
        for layer in self.animLayers:
            if mc.animLayer(layer, q=True, lock=True):
                self.lockLayers.append(layer)

    def __enter__(self):
        for layer in self.lockLayers:
            mc.animLayer(layer, e=True, lock=False)

    def __exit__(self, *args):
        for layer in self.lockLayers:
            mc.animLayer(layer, e=True, lock=True)

def create_asset_layers(separate=True):
    from random import random
    from rf_utils.ui import load
    from rftool.utils.ui import maya_win

    try:
        scene = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
        geoGrp = scene.projectInfo.asset.geo_grp()
    except:
        geoGrp = 'Geo_Grp'

    sels = pm.selected()
    refs = []
    if sels:
        for sel in sels:
            if sel.isReferenced():
                path = pm.referenceQuery(sel, f=True)
                refs.append(pm.FileReference(path))

    if not refs:
        refs = pm.listReferences()
    if not refs:
        return

    all_layers = pm.ls(type='displayLayer')
    all_layer_names = [l.nodeName() for l in all_layers]
    data = {}
    objects = []
    for ref in refs:
        ns = ref.namespace
        if ns:
            geoGrps = pm.ls('%s:%s' %(ns, geoGrp))
            color = (random(), random(), random())

            if separate:
                geoLyrName = '%sGeo_Layer' %ns
                ctrlLyrName = '%sCtrl_Layer' %ns
                if geoGrps:
                    data[geoLyrName] = (geoGrps, color)
                    objects += geoGrps

                ctrls = pm.ls('%s:AllMover_Ctrl' %(ns)) + pm.ls('%s:Rig_Grp|%s:*Crv_Grp' %(ns, ns))

                if ctrls:
                    data[ctrlLyrName] = (ctrls, color)
                    objects += ctrls
            else:
                lyrName = '%s_Layer' %ns
                objs = []
                if geoGrps:
                    objs += geoGrps
                    objects += geoGrps
                ctrls = pm.ls('%s:AllMover_Ctrl' %(ns)) + pm.ls('%s:Rig_Grp|%s:*Crv_Grp' %(ns, ns))
                if ctrls:
                    objs += ctrls
                    objects += ctrls
                if objs:
                    data[lyrName] = (objs, color)
    if objects:
        pm.select(objects, r=True)
    else:
        return

    ui_path = '%s\\create_asset_layers_dialog.ui' %os.path.dirname(__file__)
    ui_parent = maya_win.getMayaWindow()
    dialog = load.setup_ui_maya(ui_path, ui_parent)
    dialog.nss_listWidget.addItems(sorted(data.keys()))
    for r in xrange(dialog.nss_listWidget.count()):
        item = dialog.nss_listWidget.item(r)
        item.setSelected(True)

    if dialog.exec_():
        filtered_names = [str(n.text()) for n in dialog.nss_listWidget.selectedItems()]
        for name, data in data.iteritems():
            if name in filtered_names:
                if name not in all_layer_names:
                    lyr = pm.createDisplayLayer(n=name, empty=True)
                    lyr.overrideRGBColors.set(True)
                    lyr.overrideColorRGB.set(data[1])
                else:
                    lyr = pm.PyNode(name)
                lyr.addMembers(data[0])

def modify_weighted_tangent(inc=0.25):
    from nuTools.misc import checkMod

    selKeys = mc.keyframe(q=True, sl=True, n=True)
    if not selKeys:
        return

    mod = checkMod()
    breakTangent = False
    weightSide = 'ow'
    if mod['ctrl']:
        breakTangent = True
        weightSide = 'iw'
    elif mod['alt']:
        breakTangent = True
        
    queryKwArgs = {'q':True}
    editKwArgs = {'e':True}
    for key in selKeys:
        if not pm.keyTangent(key, q=True, wt=True)[0]:
            pm.keyTangent(key, e=True, wt=True)

        selTimes = pm.keyframe(key, q=True, sl=True, tc=True)
        for time in selTimes:
            keyArg = [key]
            queryKwArgs['time'] = time
            editKwArgs['time'] = time

            # query current weight
            queryKwArgs[weightSide] = True
            curr_oweight = pm.keyTangent(*keyArg, **queryKwArgs)[0]

            # calculate new weight
            new_oweight = curr_oweight + inc
            if new_oweight < 0.0:
                new_oweight = 0.0

            # add lock kwargs
            if breakTangent:
                editKwArgs['lock'] = False
            else:
                editKwArgs['lock'] = True
            editKwArgs[weightSide] = new_oweight
            pm.keyTangent(*keyArg, **editKwArgs)

def reset_weighted_tangent():
    selKeys = mc.keyframe(q=True, sl=True, n=True)
    if not selKeys:
        return
    for key in selKeys:
        selTimes = pm.keyframe(key, q=True, sl=True, tc=True)
        if not selTimes:
            selTimes = pm.keyframe(key, q=True, sl=True)

        for time in selTimes:
            if pm.keyTangent(key, q=True, wt=True)[0]:
                pm.keyTangent(key, e=True, wt=True)

            pm.keyTangent(key, time=time, itt='auto', ott='auto')

def flip_animCurve_value(keys=[], pivot=None):  # pivots are ('center', 'start', 'end')
    if not keys:
        keys = pm.keyframe(q=True, sl=True, n=True)
    if not keys:
        return

    if not pivot:
        dial = dialog.ScrollMessageBox.show(title='Caution!', 
                        message='Flip anim curve from?', 
                        ls=('center', 'start', 'end'))

        if dial.clickedButton() == dial.yes_button:
            pivot = str(dial.list_widget.currentItem().text())
        else:
            return

    for key in keys:
        values = pm.keyframe(key, q=True, vc=True)
        if pivot == 'center':
            maxv = max(values)
            minv = min(values)
            pivotValue = (abs(maxv - minv) * 0.5) + minv
        elif pivot == 'start':
            pivotValue = values[0]
        elif pivot == 'end':
            pivotValue = values[-1]
        # else:
        #     pivotValue = values[pivot]
        pm.scaleKey(key, scaleSpecifiedKeys=True, iub=False,
            timeScale=1, timePivot=1,
            floatScale=1, floatPivot=pivotValue,
            valueScale=-1, valuePivot=pivotValue)

def flip_animCurve_time(keys=[], pivot=None):  # pivots are ('center', 'start', 'end')
    if not keys:
        keys = pm.keyframe(q=True, sl=True, n=True)
    if not keys:
        return

    if not pivot:
        dial = dialog.ScrollMessageBox.show(title='Caution!', 
                        message='Flip anim curve from?', 
                        ls=('center', 'start', 'end'))
        
        if dial.clickedButton() == dial.yes_button:
            pivot = str(dial.list_widget.currentItem().text())
        else:
            return

    for key in keys:
        times = pm.keyframe(key, q=True, tc=True)
        if pivot == 'center':
            pivotValue = (abs(times[-1] - times[0]) * 0.5) + times[0]
        elif pivot == 'start':
            pivotValue = values[0]
        elif pivot == 'end':
            pivotValue = values[-1]
        # else:
        #     pivotValue = values[pivot]
        pm.scaleKey(key, scaleSpecifiedKeys=True, iub=False,
            timeScale=-1, timePivot=pivotValue,
            floatScale=1, floatPivot=pivotValue,
            valueScale=1, valuePivot=1)

def copy_incremental_animation(left='_L_', right='_R_'):
    sels = pm.selected(type='transform')
    if not sels:
        return

    # get offset from user
    offset = 0
    result = pm.promptDialog(title='Copy world animation', message='Time Offset:', text='0', 
        button=('OK', 'Cancle'))
    if result == 'Cancle':
        return
    else:
        offset = float(pm.promptDialog(q=True, text=True))

    # get incAttrs
    incAttrs = []
    result = pm.promptDialog(title='Copy world animation', message='Which attribute keeps adding up over time?\nUse "," to separate.', text='tz', 
        button=('OK', 'Cancle'))
    if result == 'Cancle':
        return
    else:
        incStr = str(pm.promptDialog(q=True, text=True))
        incStr = incStr.replace(' ', '')
        incAttrs = incStr.split(',')

    # copy animation with offset
    copy_res = copy_animations_across(sels=sels, mirror=False, left=left, right=right)

    # move curve
    for ctrl, data in copy_res.iteritems():
        for attr, keys in data.iteritems():
            src_key = keys[0]
            op_key = keys[1]
            # generate offset
            times = pm.keyframe(src_key, q=True, tc=True)
            last_half = (times[0]+offset, times[-1])
            first_half = (times[0], times[0]+offset)
            
            values = pm.keyframe(op_key, q=True, vc=True)
            last_val = values[-1]
            first_val = values[0]
            m = 1
            if first_val > last_val:
                m = -1

            if offset:
                op_attr = ctrl.attr(attr)
                # copy last half to first half
                pm.copyKey(src_key, time=last_half, option='keys')
                pm.pasteKey(op_attr, option='replace', connect=False, timeOffset=offset*-1, floatOffset=offset*-1, valueOffset=0)

                # copy first half
                pm.copyKey(src_key, time=first_half, option='keys')
                pm.pasteKey(op_attr, option='replace', connect=False, timeOffset=offset, floatOffset=offset, valueOffset=0)

            if attr in incAttrs:
                # pm.keyframe(op_key, e=True, iub=True, r=True, o='over', tc=offset)
               
                value_offset = abs(last_val - first_val) * 0.5 * m
                for time in pm.keyframe(op_key, q=True, tc=True):
                    if time < last_half[0]:  # it's the first half
                        move_value = value_offset * -1
                    else:
                        move_value = value_offset
                    pm.keyframe(op_key, e=True, iub=True, r=True, o='over', t=time, vc=move_value)
            
def copy_animations_across(sels=[], mirror=True, left='_L_', right='_R_'):
    if not sels:
        sels = pm.selected(type='transform')
        if not sels:
            return

    result = {}
    for ctrl in sels:
        # find opposite side
        ctrlName = ctrl.nodeName()
        ctrlNameNoNs = ctrlName.split(':')[-1]
        if left in ctrlNameNoNs:
            exist_side = left
            opposite_side = right
        elif right in ctrlNameNoNs:
            exist_side = right
            opposite_side = left
        else:
            om.MGlobal.displayWarning('Cannot find side for: %s' %ctrlName)
            continue


        op_side_name = '%s%s' %(ctrl.namespace(), ctrlNameNoNs.replace(exist_side, opposite_side))
        op_node = None
        try:
            op_node = pm.PyNode(op_side_name)
        except:
            om.MGlobal.displayWarning('Cannot find opposite side for: %s' %ctrlName)
            continue

        # find keys on the source side
        keyable_atrs = ctrl.listAttr(k=True, v=True)
        src_keys = []
        keyed_attrs = []
        for attr in keyable_atrs:
            keys = attr.inputs(type='animCurve')
            if keys and not keys[0].isReferenced():  # if the ctrl has key and the key is not referenced
                src_keys.append(keys[0])
                keyed_attrs.append(attr)

        if not src_keys:
            om.MGlobal.displayWarning('No key on: %s' %ctrlName)
            continue

        attr_res = {}
        for attr, key in zip(keyed_attrs, src_keys):
            sn = attr.shortName()
            if not op_node.hasAttr(sn):
                om.MGlobal.displayWarning('Opposite ctrl, %s has no attribute %s' %(op_node.nodeName(), sn))
                continue

            op_attr = op_node.attr(sn)
            op_keys = op_attr.inputs(type='animCurve')
            times = pm.keyframe(key, q=True, tc=True)
            if not op_keys:  # the opposite side is not keyed
                firstTime = times[0]
                op_attr.setKey(t=firstTime)
                op_key = op_attr.inputs(type='animCurve')[0]
            else:
                op_key = op_keys[0]

            pm.copyKey(key, controlPoints=0, shape=0)
            pm.pasteKey(op_attr, option='replace', copies=1, connect=0, timeOffset=0, floatOffset=0, valueOffset=0)

            pre_inf = key.preInfinity.get()
            post_inf = key.postInfinity.get()

            op_key.preInfinity.set(pre_inf)
            op_key.postInfinity.set(post_inf)

            if mirror:
                flip_animCurve_value(keys=[op_key], pivot='center')

            attr_res[sn] = (key, op_key)
        result[op_node] = attr_res

    return result

def generate_position_anim_curve(positions, start, end):
    acx = pm.createNode('animCurveTL')
    acXMObj = acx.__apimobject__()
    curveFnX = oma.MFnAnimCurve(acXMObj)

    acy = pm.createNode('animCurveTL')
    acYMObj = acy.__apimobject__()
    curveFnY = oma.MFnAnimCurve(acYMObj)

    acz = pm.createNode('animCurveTL')
    acZMObj = acz.__apimobject__()
    curveFnZ = oma.MFnAnimCurve(acZMObj)
    i = 0
    for frame in xrange(start, end+1):
        mtime = om.MTime(frame)
        curveFnX.addKeyframe(mtime, positions[i][0])
        curveFnY.addKeyframe(mtime, positions[i][1])
        curveFnZ.addKeyframe(mtime, positions[i][2])
        i += 1
    return acx, acy, acz

def clean_key_outside_range(startFrame, endFrame):
    dgIt = om.MItDependencyNodes(om.MFn.kAnimCurve)
    result = set()
    while not dgIt.isDone():
        curveFn = oma.MFnAnimCurve(dgIt.thisNode())
        if not curveFn.isFromReferencedFile() and curveFn.animCurveType() in (0, 1, 2, 3):
            numKey = curveFn.numKeyframes()
            if not numKey:
                continue
            keyStart = curveFn.time(0).value()
            if numKey < 2:
                endIndex = 0
            else:
                endIndex = numKey-1
            keyEnd = curveFn.time(endIndex).value()

            curve_name = str(curveFn.name())

            # insert a key then delete key outside
            if keyStart < startFrame:
                pm.setKeyframe(curve_name, time=startFrame, insert=True)
                pm.cutKey(curve_name, t=':%s' %(startFrame-1), cl=True)
                result.add(curve_name)
            if keyEnd > endFrame:
                pm.setKeyframe(curve_name, time=endFrame, insert=True)
                pm.cutKey(curve_name, t='%s:' %(endFrame+1), cl=True)
                result.add(curve_name)

        dgIt.next()

    return result

def get_sel_ref_geoGrp():
    sels = pm.selected()
    if not sels:
        return
    sceneName = pm.sceneName()
    geoGrps = []
    for sel in sels:
        namespace = sel.namespace()
        geoGrpName = 'Geo_Grp'
        try:
            scene = context_info.ContextPathInfo(path=sceneName)
            geoGrpName = scene.projectInfo.asset.geo_grp()
        except:
            pass

        geoGrpNs = '%s%s' %(namespace, geoGrpName)
        grpLs = pm.ls(geoGrpNs, type='transform')
        if len(grpLs) != 1:
            om.MGlobal.displayError('Cannot find: %s' %geoGrpNs)
            return
        geoGrps.append(grpLs[0])

    return list(set(geoGrps))

def delete_instance_anim_mesh():
    geoGrps = get_sel_ref_geoGrp()
    if not geoGrps:
        om.MGlobal.displayError('Please select any part of the asset and try again.')
        return
    for geoGrp in geoGrps:
        outMsgs = geoGrp.message.outputs(p=True)
        nodes = set()
        for plug in outMsgs:
            if plug.longName() == 'parentInstance':
                nodes.add(plug.node())
        if nodes:
            pm.delete(list(nodes))

def duplicate_instance_anim_mesh(offset=None, angles=OrderedDict([('threeQuaterView',[0, 45, 0]), ('sideView', [0, 90, 0])])):
    geoGrps = get_sel_ref_geoGrp()
    if not geoGrps:
        om.MGlobal.displayError('Please select any part of the asset and try again.')
        return

    # figure out offset
    bb = geoGrps[0].getBoundingBox()
    center = bb.center()
    rootPos = [center[0], bb[0][1], center[2]]
    if not offset:
        delta_x = (bb[1][0] - bb[0][0])
        offset = (delta_x*1.75, 0, 0)

    i = 0
    m = 1
    ctrlScale = offset[0]*0.334
    ns = geoGrps[0].namespace()

    for angleName, value in angles.iteritems():
        # create controller
        ctrlName = '%s%s_Ctrl' %(ns, angleName)
        ctrl = pm.circle(n=ctrlName, nr=(0, 1, 0), ch=False)[0]
        ctrl.overrideEnabled.set(True)
        ctrl.overrideColor.set(18)
        ctrl.v.lock()
        ctrl.v.showInChannelBox(False)
        pm.scale(ctrl.getShape().cv[0:7], (ctrlScale, ctrlScale, ctrlScale), r=True)
        ctrl.translate.set(rootPos)

        newAttr = pm.addAttr(ctrl, ln='parentInstance', at='message', m=False)
        
        for geoGrp in geoGrps:
            dupGeo = pm.instance(geoGrp, n='%s_%s' %(geoGrp, angleName))[0]
            # pm.delete(pm.parentConstraint(dupGeo, ctrl))
            pm.xform(dupGeo, ws=True, piv=rootPos)
            pm.parent(dupGeo, ctrl)
            
        pm.connectAttr(geoGrps[0].message, ctrl.parentInstance, f=True)
        # move and rotate the controller
        pm.xform(ctrl, ws=True, r=True, t=((offset[0]*(i+1)*m), offset[1], offset[2]))
        pm.xform(ctrl, ws=True, r=True, ro=value)

        if (i+1)%2:
            m = -1

        if (i+1)%3 == 0:
            i += 1

    pm.select(geoGrps, r=True)



