# v.0.0.1 polytag switcher
_title = 'RF Save shelf'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'RFsaveshelfUI'

#Import python modules
import sys
import os
import getpass
import logging
from functools import partial
from collections import OrderedDict
import shutil

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

##rf_temp
LocalTemp = os.environ['TEMP']
rf_tmp = os.path.join(LocalTemp,'rf_tmp').replace('\\','/')
# import config
import rf_config as config

if config.isMaya:
    import maya.cmds as mc
    import maya.mel as mm

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rf_utils import icon

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from pymel.all import *
import maya.mel as mel
from rf_utils import file_utils




class SaveshelfUI(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(SaveshelfUI, self).__init__(parent)

        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        if config.isMaya:
            self.ui = load.setup_ui_maya(uiFile, parent)
        else:
            self.ui = load.setup_ui(uiFile, self)


        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.init_signals()
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.list_shelf()
        self.ui.widget_list_Layout.addWidget(self.listWidget)



    def init_signals(self):
        self.ui.refresh_pushButton.clicked.connect(self.list_shelf)
        self.ui.save_pushButton.clicked.connect(self.sourceCopy)

    def list_shelf(self):
        current = []
        icon_path = icon.maya
        self.listWidget.clear()
        script_dir = os.environ['RFSCRIPT']
        version = mc.about(v=True)
        if version == '2016 Extension 2 SP2':
            version = '2016.5'
        shelf_path = "core/rf_maya/shelfs/%s"%version
        shelf_db_path = os.path.join(script_dir, shelf_path).replace("\\", "/")
        custom_shelf = os.listdir(shelf_db_path)
        currentShelf = self.currentShelf()
        for shelf in custom_shelf:
            name_shelf = shelf.split('_')[1].split('.')[0]
            item = QtWidgets.QListWidgetItem(self.listWidget)
            item.setText(shelf)
            icons = QtGui.QIcon(icon_path)
            item.setIcon(icons)
            if name_shelf == currentShelf:
                self.listWidget.setCurrentItem(item)


    def currentShelf(self):
        shelftoplevel = mel.eval("$gShelfTopLevel = $gShelfTopLevel;")
        shelf = mc.shelfTabLayout(shelftoplevel,selectTab=True, q=True)
        return shelf


    def sourceCopy(self):
        mel.eval('saveAllShelves $gShelfTopLevel;')
        script_dir = os.environ['RFSCRIPT']
        app_dir = os.environ['MAYA_APP_DIR']
        version = mc.about(v=True)

        shelf_path_local = "%s/prefs/shelves"%version
        shelf_path_db = "core/rf_maya/shelfs/%s"%version
        if version == '2015':
            version_2015 = '2015-x64'
            shelf_path_local = "%s/prefs/shelves"%version_2015
        if version == '2016 Extension 2 SP2':
            version_2016 = '2016.5'
            shelf_path_local = "%s/prefs/shelves"%version_2016
            shelf_path_db = "core/rf_maya/shelfs/%s"%version_2016
            
        shelves_name = self.listWidget.selectedItems()
        print shelves_name
        for shelf_name in shelves_name:
            shelf_basename = shelf_name.text()

            shelf_maya_path = os.path.join(app_dir, shelf_path_local).replace('\\', '/')
            shelf_base_path = os.path.join(shelf_maya_path, shelf_basename).replace('\\', '/')

            database_path = os.path.join(script_dir, shelf_path_db).replace('\\', '/')
            database_base_path = os.path.join(database_path, shelf_basename).replace('\\', '/')

            file_utils.copy(shelf_base_path, database_base_path)
            print "Copy"
            print "src: ",shelf_base_path
            print "dst: ",database_base_path

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = SaveshelfUI(maya_win.getMayaWindow())
        return myApp

if __name__ == '__main__':
    show()
