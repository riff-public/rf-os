from Qt import QtWidgets 
from Qt import QtGui 
from Qt import QtCore
from functools import partial
import logging
import maya.cmds as mc
import pymel.core as pm
from . import on2_utils as on2
reload(on2)


logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
uiName = 'On2Ui'


class App(QtWidgets.QMainWindow):
    """docstring for Ui"""
    def __init__(self, parent=None):
        super(App, self).__init__(parent=parent)
        self.ui = Ui(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle('RF On2 Control')
        self.resize(250, 400)
        self.init_signals()
        self.init_functions()

    def init_signals(self): 
        self.ui.add_button.clicked.connect(self.add_node)
        self.ui.del_button.clicked.connect(self.del_node)
        self.ui.add_ctrl.clicked.connect(self.add_ctrl)
        self.ui.remove_ctrl.clicked.connect(self.remove_ctrl)
        self.ui.namespace_list.itemClicked.connect(self.select)
        self.ui.addkey_button.clicked.connect(self.addkey)
        self.ui.namespace_list.customContextMenuRequested.connect(self.show_menu)

    def init_functions(self): 
        self.refresh_tsn_nodes()

    def refresh_tsn_nodes(self): 
        self.item_dict = dict()
        nodes = on2.list_time_step_nodes()
        self.ui.namespace_list.clear()

        for node in nodes: 
            widget = NSItem()
            item = QtWidgets.QListWidgetItem()
            self.ui.namespace_list.addItem(item)

            item.setData(QtCore.Qt.UserRole, node)
            self.update_node_item(node, widget)
            widget.active_button.clicked.connect(partial(self.set_action, node, widget))

            item.setSizeHint(widget.sizeHint())
            self.ui.namespace_list.setItemWidget(item, widget)
            self.item_dict[node.name] = widget

    def update_node_item(self, node, widget): 
        widget.set_namespace(node.namespace)
        widget.set_active(node.is_active())
        widget.set_all_ctrl(not node.is_exception())

    def add_node(self): 
        ns = on2.get_selection_namespace()
        if ns: 
            tsn = on2.TimeStepNode(ns)
            tsn.connect()
            self.refresh_tsn_nodes()

    def del_node(self): 
        items = self.ui.namespace_list.selectedItems()
        for item in items: 
            node = item.data(QtCore.Qt.UserRole)
            on2.delete_node(node.name)
        self.refresh_tsn_nodes()

    def set_action(self, node, widget): 
        if node.is_active(): 
            node.disconnect()
            widget.set_active(False)
        else: 
            node.connect()
            widget.set_active(True)

    def deactive(self): 
        nss = on2.get_selected_namespaces()
        for ns in nss: 
            on2.remove_retime(ns)

    def add_ctrl(self): 
        node = on2.get_node_from_selection()
        ctrls = on2.get_selection()
        node.include(ctrls)

        # update ui 
        if node.name in self.item_dict.keys(): 
            widget = self.item_dict[node.name]
            self.update_node_item(node, widget)

    def remove_ctrl(self): 
        node = on2.get_node_from_selection()
        ctrls = on2.get_selection()
        node.exclude(ctrls)

        # update ui 
        if node.name in self.item_dict.keys(): 
            widget = self.item_dict[node.name]
            self.update_node_item(node, widget)

    def clear_all_exceptions(self, node): 
        # update ui 
        node.clear_exceptions()
        if node.name in self.item_dict.keys(): 
            widget = self.item_dict[node.name]
            self.update_node_item(node, widget)

    def select_ctrls(self, node, mode): 
        ctrls = on2.get_all_ctrls(node.namespace) 
        on1_ctrls = node.get_exceptions()
        on2_ctrls = list()

        if mode == 'on1': 
            on2.select_node(on1_ctrls)
        if mode == 'on2': 
            for ctrl in ctrls: 
                if not ctrl.name() in on1_ctrls: 
                    on2_ctrls.append(ctrl.name())
            on2.select_node(on2_ctrls)

    def select(self, item): 
        node = item.data(QtCore.Qt.UserRole)
        on2.select_node(node)

    def addkey(self): 
        item = self.ui.namespace_list.currentItem()
        if item: 
            node = item.data(QtCore.Qt.UserRole)
            on2.add_key(node)

    def show_menu(self, pos): 
        item = self.ui.namespace_list.currentItem()
        if item: 
            menu = QtWidgets.QMenu(self)
            node = item.data(QtCore.Qt.UserRole)
            if node.is_exception(): 
                active = menu.addAction('Active All Ctrls')
                active.triggered.connect(partial(self.clear_all_exceptions, node))
                menu.addSeparator()

                selecton2 = menu.addAction('Select on2 ctrls')
                selecton2.triggered.connect(partial(self.select_ctrls, node, 'on2'))
                selecton1 = menu.addAction('Select on1 ctrls')
                selecton1.triggered.connect(partial(self.select_ctrls, node, 'on1'))
                menu.addSeparator()


            refresh = menu.addAction('Refresh')
            refresh.triggered.connect(node.connect)
            menu.popup(self.ui.namespace_list.mapToGlobal(pos))



class Ui(QtWidgets.QWidget):
    """docstring for Ui"""
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent=parent)
        # main layout
        self.layout = QtWidgets.QVBoxLayout()

        # horizontal layout 
        self.hlayout = QtWidgets.QHBoxLayout()
        self.add_button = QtWidgets.QPushButton('+ Asset')
        self.del_button = QtWidgets.QPushButton('- Asset')
        self.hlayout.addWidget(self.add_button)
        self.hlayout.addWidget(self.del_button)

        self.hlayout2 = QtWidgets.QHBoxLayout()
        self.add_ctrl = QtWidgets.QPushButton('+ Ctrl')
        self.remove_ctrl = QtWidgets.QPushButton('- Ctrl')
        self.hlayout2.addWidget(self.add_ctrl)
        self.hlayout2.addWidget(self.remove_ctrl)

        # add to main layout 
        self.namespace_list = QtWidgets.QListWidget()
        self.addkey_button = QtWidgets.QPushButton('Add Key')

        self.addkey_button.setMinimumSize(QtCore.QSize(20, 30))
        self.layout.addWidget(self.namespace_list)
        self.layout.addLayout(self.hlayout)
        self.layout.addLayout(self.hlayout2)
        self.layout.addWidget(self.addkey_button)
        self.setLayout(self.layout)

        # override 
        self.namespace_list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)


class NSItem(QtWidgets.QWidget):
    """docstring for NSItem"""
    def __init__(self, parent=None):
        super(NSItem, self).__init__(parent=parent)
        self.layout = QtWidgets.QHBoxLayout()
        spacer = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.namespace = QtWidgets.QLabel()
        self.ctrl = QtWidgets.QLabel()
        self.active_button = QtWidgets.QPushButton()
        self.layout.addWidget(self.namespace)
        self.layout.addItem(spacer)
        self.layout.addWidget(self.ctrl)
        self.layout.addWidget(self.active_button)
        self.setLayout(self.layout)

        # override
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.active_button.setMaximumSize(QtCore.QSize(15, 15))

    def set_namespace(self, text): 
        self.namespace.setText(text)

    def set_active(self, state): 
        if state: 
            colors = [255, 255, 255]
            button = [100, 255, 100]
        else: 
            colors = [100, 100, 100]
            button = [100, 100, 100]
        self.namespace.setStyleSheet('color: rgb({}, {}, {})'.format(colors[0], colors[1], colors[2]))
        self.active_button.setStyleSheet('background-color: rgb({}, {}, {})'.format(button[0], button[1], button[2]))
        
    def set_all_ctrl(self, state): 
        text = ''
        if state: 
            text = 'All Ctrls'
        self.ctrl.setText(text)


def show(): 
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = App(parent=maya_win.getMayaWindow())
    myApp.show()
