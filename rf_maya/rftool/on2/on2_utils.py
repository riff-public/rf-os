import sys
import os 
import logging 
import pymel.core as pm
import maya.cmds as mc 
from rf_utils import file_utils
from collections import OrderedDict

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# fix pair blend not working
# add animLayer support
# fix bug None_TSN


class Attr: 
    active = 'active'
    ns = 'namespaces'
    ref = 'ref'
    excpt = 'exception'
    suffix = 'TSN' # Time Step


def create_retime_curve(name='', step=2, keylist=[]):
    tt = pm.createNode('animCurveTT')
    add_extra_attr(tt)
    tt.rename(name) if name else None

    sf = int(pm.playbackOptions(q=True, min=True))
    ef = int(pm.playbackOptions(q=True, max=True))

    create_curve(tt, sf, ef, step, keylist)

    # i = 0
    # for f in range(sf, ef-1, 2):
    #     allow = True
    #     if keylist: 
    #         allow = True if f in keylist else False
    #     if allow: 
    #         tt.addKey(f, 0.0, tangentInType='fixed', tangentOutType='step')
    #         pm.keyframe(tt, index=i, absolute=True, valueChange=f)
    #     i += 1
    return tt


def create_curve(node, sf, ef, step=2, keylist=[]): 
    print('create_curve', node, sf, ef, step, keylist)
    tt = pm.PyNode(node)
    i = 0
    
    for f in range(sf, ef+2, step):
        allow = True
        if keylist: 
            allow = True if f in keylist else False
        if allow: 
            tt.addKey(f, 0.0, tangentInType='fixed', tangentOutType='step')
            pm.keyframe(tt, index=i, absolute=True, valueChange=f)
        if f == sf: 
            pm.cutKey(tt, time=(sf+1, ef))
        i += 1


class TimeStepNode(object):
    """docstring for TimeStepNode"""
    def __init__(self, namespace, create=True):
        super(TimeStepNode, self).__init__()
        self.namespace = namespace
        self.create = create
        self.step = 2
        self.node = self.create_node()

    def __str__(self): 
        return self.node.name()

    def __repr__(self): 
        return self.node.name()

    @property 
    def name(self): 
        return self.node.name()

    def create_node(self): 
        if not is_node_exists(self.namespace): 
            if self.create: 
                node = create_local_retime(self.namespace)
                add_extra_attr(node)
                set_namespace(node, self.namespace)
                set_ref_path(node, get_path_namespace(self.namespace))
            else: 
                logger.error('Node {} not exists'.format(local_retime(self.namespace)))
        else: 
            node = create_local_retime(self.namespace)
        return node

    def connect(self): 
        exceptions = self.get_exceptions()
        connect_retime(self.namespace, exceptions=exceptions)
        connect_retime_cache(self.namespace, self.node)
        set_active(self.node, True)

    def disconnect(self): 
        disconnect_retime(self.namespace)
        disconnect_retime_cache(self.namespace)
        set_active(self.node, False)

    def connect_to_namespace(self, namespace): 
        exceptions = self.get_exceptions()
        connect_retime(self.namespace, exceptions=exceptions, target_namespace=namespace)
        connect_retime_cache(namespace, self.node)
        set_active(self.node, True)

    def exclude(self, ctrls): 
        disconnect_selected(ctrls)
        add_exceptions(self.node, ctrls)

    def include(self, ctrls): 
        connect_selected(self.node, ctrls)
        remove_exceptions(self.node, ctrls)

    def clear_exceptions(self): 
        exceptions = self.get_exceptions()
        set_exceptions(self.node, list())
        self.include(exceptions)

    def select(self): 
        pm.select(self.node)

    def is_active(self): 
        return is_active(self.node)

    def set_active(self, state): 
        set_active(self.node, state)

    def get_exceptions(self): 
        return get_exceptions(self.node)

    def set_exceptions(self, ctrls): 
        set_exceptions(self.node, ctrls)

    def is_exception(self): 
        return True if self.get_exceptions() else False

    def get_connected_namespaces(self): 
        return get_connected_namespaces(self.node)

    def get_connected_namespace(self): 
        return self.get_connected_namespaces()[0]

    def get_connected_ctrls(self): 
        return get_connected_ctrls(self.node)

    def get_namespace(self): 
        return get_namespace(self.node)

    def get_ref(self): 
        attr = '{}.{}'.format(self.node, Attr.ref)
        if pm.objExists(attr): 
            return pm.getAttr('{}.{}'.format(self.node, Attr.ref))
        else: 
            logger.warning('{} does not exists'.format(attr))

    def renamespace(self, new_namespace): 
        new_node_name = local_retime(new_namespace)
        new_ref = get_path_namespace(new_namespace)

        if new_ref: 
            self.set_namespace(new_namespace)
            self.set_ref(new_ref)
            self.node.rename(new_node_name)
            self.namespace = new_namespace
        else: 
            logger.error('namespace "{}" not exists'.format(new_namespace))

    def is_correct_asset(self): 
        namespaces = self.get_connected_namespaces()
        if len(namespaces) == 0: 
                return True

        elif len(namespaces) == 1: 
            current_connected_namespace = namespaces[0]
            node_namespace = self.get_namespace()
            if not current_connected_namespace == node_namespace: 
                return False 

        else: 
            logger.warning('More than 1 namepsace connected')
            logger.warning(', '.join(namespaces))
            return False 

        return True

    def set_namespace(self, new_namespace): 
        set_namespace(self.node, new_namespace)

    def set_ref(self, value): 
        return pm.setAttr('{}.{}'.format(self.node, Attr.ref), value)

    def reset_curve(self): 
        sf = int(pm.playbackOptions(q=True, min=True))
        ef = int(pm.playbackOptions(q=True, max=True))
        create_curve(self.node, sf, ef, step=self.step)

    def retime_curve(self, keylist=[]): 
        sf = int(pm.playbackOptions(q=True, min=True))
        ef = int(pm.playbackOptions(q=True, max=True))
        create_curve(self.node, sf, ef, step=self.step, keylist=keylist)

    def get_keyframes(self): 
        values = pm.keyframe(self.node, query=True, timeChange=True)
        return values


def add_extra_attr(node): 
    if not pm.objExists('{}.{}'.format(node, Attr.active)): 
        pm.addAttr(node, ln=Attr.active, at='bool')
    if not pm.objExists('{}.{}'.format(node, Attr.ns)): 
        pm.addAttr(node, ln=Attr.ns, dt='string')
    if not pm.objExists('{}.{}'.format(node, Attr.excpt)): 
        pm.addAttr(node, ln=Attr.excpt, dt='string')
    if not pm.objExists('{}.{}'.format(node, Attr.ref)): 
        pm.addAttr(node, ln=Attr.ref, dt='string')

    pm.setAttr('{}.{}'.format(node, Attr.active), e=True, keyable=False, channelBox=True)
    pm.setAttr('{}.{}'.format(node, Attr.ns), e=True, keyable=False, channelBox=True)
    pm.setAttr('{}.{}'.format(node, Attr.excpt), e=True, keyable=False, channelBox=True)
    pm.setAttr('{}.{}'.format(node, Attr.ref), e=True, keyable=False, channelBox=True)


def create_global_retime(): 
    if not pm.objExists(global_retime()): 
        return create_retime_curve(global_retime())
    else: 
        return pm.PyNode(global_retime())


def create_local_retime(namespace): 
    if not pm.objExists(local_retime(namespace)): 
        return create_retime_curve(local_retime(namespace))
    else: 
        return get_local_retime(namespace)


def local_retime(namespace): 
    name = '{}_{}'.format(namespace, Attr.suffix)
    return name


def global_retime(): 
    return 'global_{}'.format(Attr.suffix)


def get_local_retime(namespace): 
    node = local_retime(namespace)
    if pm.objExists(node): 
        return pm.PyNode(node)


def get_selection_namespace(): 
    # get namespace from selection 
    ns = str()
    sels = mc.ls(sl=True)
    if sels: 
        ns = mc.referenceQuery(sels[0], ns=True)[1:]
    return ns 


def get_path_namespace(namespace): 
    sel = mc.ls('{}:*'.format(namespace))
    if sel: 
        path = mc.referenceQuery(sel[0], f=True)
        return path


def get_retime_node(namespace): 
    if pm.objExists(local_retime(namespace)): 
        return pm.PyNode(local_retime(namespace))
    return create_global_retime()


def get_connected_ctrls(node): 
    """ return connected reference asset ctrl 
        node = "childA_0012_TSN" """
    ctrls = list()
    curves = mc.listConnections('{}.output'.format(node))

    if curves: 
        for crv in curves: 
            attr = '{}.output'.format(crv)
            if mc.objExists(attr): 
                ctrl = mc.listConnections(attr)
                if ctrl:
                    if mc.referenceQuery(ctrl, inr=True): 
                        ctrls.append(ctrl)

    return ctrls


def get_connected_namespaces(node): 
    namespaces = list()
    ctrls = get_connected_ctrls(node)

    for ctrl in ctrls: 
        ns = mc.referenceQuery(ctrl, ns=True)[1:]
        if not ns in namespaces: 
            namespaces.append(ns)

    return namespaces


def add_remove_namespace(node, namespace, add=True): 
    raw = pm.getAttr('{}.{}'.format(node, Attr.ns))
    if raw: 
        namespaces = eval(raw)
        if add: 
            if not namespace in namespaces: 
                namespaces.append(namespace)
        else: 
            if namespace in namespaces: 
                namespaces.remove(namespace)

        pm.setAttr('{}.{}'.format(node, Attr.ns), str(namespaces), type='string')


def set_ref_path(node, value): 
    pm.setAttr('{}.{}'.format(node, Attr.ref), str(value), type='string')


def connect_retime(namespace, exceptions=[], target_namespace=None):
    tt = create_local_retime(namespace)
    ctrls = get_all_ctrls(namespace) if not target_namespace else get_all_ctrls(target_namespace)
    for ctrl in ctrls:
        if ctrl.nodeName() in exceptions:
            continue
        for ac in find_anim_curves(ctrl):
            if ac.isReferenced():
                continue
            if not pm.isConnected(tt.output, ac.input): 
                pm.connectAttr(tt.output, ac.input, f=True)

    return tt


def get_all_ctrls(namespace): 
    ctrls = pm.ls('{}:*_Ctrl'.format(namespace)) + pm.ls('{}:*_ctrl'.format(namespace))
    return ctrls 


def get_all_alembic(namespace): 
    abcs = pm.ls('{}:*'.format(ns), type='AlembicNode')
    return abcs


def connect_selected(node, ctrls):
    node = pm.PyNode(node)
    for ctrl in ctrls: 
        ctrl = pm.PyNode(ctrl)
        for ac in find_anim_curves(ctrl):
            if ac.isReferenced():
                continue
            if not pm.isConnected(node.output, ac.input): 
                pm.connectAttr(node.output, ac.input, f=True)


def list_time_step_nodes(): 
    nodes = mc.ls(type='animCurveTT')
    # get by matching suffix 
    t_nodes = list()
    for node in nodes: 
        if '_{}'.format(Attr.suffix) == node[-(len(Attr.suffix) + 1):]: 
            ns = get_namespace(node)
            if not ns: 
                ns = node.replace('_{}'.format(Attr.suffix), '')
            t_nodes.append(TimeStepNode(ns))

    return t_nodes
    # nodes = [TimeStepNode(get_namespace(a)) for a in nodes if '_{}'.format(Attr.suffix) == a[-(len(Attr.suffix) + 1):]]
    # return nodes


def list_active_time_nodes(): 
    nodes = list_time_step_nodes()
    return [a for a in nodes if a.is_active()]


def is_node_exists(namespace): 
    return pm.objExists(local_retime(namespace))


def select_node(node): 
    mc.select(node)


def delete_node(node): 
    mc.delete(node)


def set_namespace(node, namespace): 
    attr = '{}.{}'.format(node, Attr.ns)
    pm.setAttr(attr, namespace, type='string')


def get_namespace(node): 
    attr = '{}.{}'.format(node, Attr.ns)
    return pm.getAttr(attr) if pm.objExists(attr) else ''


def get_node_from_selection(): 
    ns = get_selection_namespace()
    if ns: 
        node = local_retime(ns)
        if pm.objExists(node): 
            return TimeStepNode(ns)


def get_selection(): 
    return mc.ls(sl=True)


def set_active(node, state): 
    attr = '{}.{}'.format(node, Attr.active)
    pm.setAttr(attr, state)


def is_active(node): 
    attr = '{}.{}'.format(node, Attr.active)
    return pm.getAttr(attr) if pm.objExists(attr) else ''


def set_exceptions(node, ctrls): 
    attr = '{}.{}'.format(node, Attr.excpt)
    value = str(ctrls)
    pm.setAttr(attr, value, type='string')


def add_exceptions(node, ctrls): 
    values = get_exceptions(node)
    for ctrl in ctrls: 
        if not ctrl in values: 
            values.append(ctrl)
    set_exceptions(node, values)


def remove_exceptions(node, ctrls): 
    values = get_exceptions(node)
    for ctrl in ctrls: 
        if ctrl in values: 
            values.remove(ctrl)
    set_exceptions(node, values)


def get_exceptions(node): 
    attr = '{}.{}'.format(node, Attr.excpt)
    vlist = list()
    if pm.objExists(attr): 
        v = pm.getAttr(attr)
        vlist = eval(pm.getAttr(attr)) if v else list()
    return vlist


def add_retime(namespace, exceptions=[]): 
    tt = connect_retime(namespace, exceptions)
    add_remove_namespace(tt, namespace, add=True)


def remove_retime(namespace, exceptions=[]): 
    disconnect_retime(namespace, exceptions)
    add_remove_namespace(tt, namespace, add=False)


def add_key(node=None): 
    if node: 
        mc.select(node)
    mc.setKeyframe(v=mc.currentTime(q=True), ott='step')


def disconnect_retime2(namespace): 
    node_name = '{}_TC'.format(namespace)
    if pm.objExists(node_name): 
        tt = pm.PyNode(node_name)
        for attr in pm.listConnections(tt.output): 
            pm.disconnectAttr(tt.output, attr.input)


def disconnect_retime(namespace, exceptions=[]):
    ctrls = get_all_ctrls(namespace)
    for ctrl in ctrls:
        if ctrl.nodeName() in exceptions:
            continue
        for ac in find_anim_curves(ctrl):
            if ac.isReferenced():
                continue
            attrs = pm.listConnections(ac.input, p=True)
            
            for attr in attrs: 
                pm.disconnectAttr(attr, ac.input)


def disconnect_selected(ctrls): 
    for ctrl in ctrls:
        ctrl = pm.PyNode(ctrl)

        for ac in find_anim_curves(ctrl):
            if ac.isReferenced():
                continue
            attrs = pm.listConnections(ac.input, p=True)
            
            for attr in attrs: 
                pm.disconnectAttr(attr, ac.input)


def reconnect_all_nodes(): 
    nodes = list_active_time_nodes()
    for node in nodes: 
        node.connect() 


def disconnect_all_nodes(): 
    nodes = list_active_time_nodes()
    for node in nodes: 
        node.disconnect() 


def find_anim_curves(ctrl): 
    anim_nodes = list()
    ctrl = pm.PyNode(ctrl)
    anim_nodes = ctrl.inputs(type='animCurve')

    for node in ctrl.inputs(): 
        all_anims = node.inputs(type='animCurve')
        if all_anims: 
            anims = [a for a in all_anims if not a.type() == 'animCurveTT']
            anim_nodes.extend(anims)

    return anim_nodes




def track_geo(parent, child, vtx_id=0):
    sf = int(pm.playbackOptions(q=True, min=True))
    ef = int(pm.playbackOptions(q=True, max=True))
    pm.currentTime(sf, e=True)

    piv1 = pm.dt.Vector(pm.xform(child.vtx[vtx_id], q=True, ws=True, t=True))
    piv2 = pm.dt.Vector(pm.xform(parent.vtx[vtx_id], q=True, ws=True, t=True))
    offset = piv2 - piv1

    for f in range(sf, ef+1):
        pm.currentTime(f, e=True)
        piv1 = pm.dt.Vector(pm.xform(child.vtx[vtx_id], q=True, ws=True, t=True))
        piv2 = pm.dt.Vector(pm.xform(parent.vtx[vtx_id], q=True, ws=True, t=True))
        inv_piv = (piv2 - piv1) - offset
        pm.xform(child, ws=True, t=inv_piv, r=True)
        pm.setKeyframe(child.t)


def connect_retime_cache(ns, tt):
    abcs = pm.ls('{}:*'.format(ns), type='AlembicNode')
    for abc in abcs:
        pm.connectAttr(tt.output, abc.time, f=True)


def disconnect_retime_cache(ns):
    abcs = pm.ls('{}:*'.format(ns), type='AlembicNode')
    time_node = pm.PyNode('time1')
    for abc in abcs:
        pm.connectAttr(time_node.outTime, abc.time, f=True)


def get_selected_namespaces(): 
    sels = mc.ls(sl=True)
    namespaces = list()
    for sel in sels: 
        if mc.referenceQuery(sel, inr=True): 
            ns = mc.referenceQuery(sel, ns=True)
            if not ns in namespaces: 
                namespaces.append(ns)
    return namespaces


class DisableTimeStep(object):
    """docstring for DisableTimeStep"""
    def __init__(self): 
        self.nodes = list_active_time_nodes()

    def __enter__(self): 
        if self.nodes: 
            for node in self.nodes: 
                node.disconnect()
         
    def __exit__(self, *args): 
        if self.nodes: 
            for node in self.nodes: 
                node.connect() 

        
#############################################################
''' QC area 
''' 

def rename_node_to_namespace(node): 
    """ rename TSN node to match connected namespace """ 
    if mc.objExists(node): 
        tsn = TimeStepNode(get_namespace(node), create=False)
        if not tsn.is_correct_asset(): 
            connected_namespace = tsn.get_connected_namespace()
            node_namespace = tsn.get_namespace()
            if not node_namespace == connected_namespace: 
                correct_node_name = local_retime(connected_namespace)
                tsn.renamespace(connected_namespace)
                print('invalid asset namespace. changing to {}'.format(correct_node_name))



#################################################################
''' exporting area 
'''
def export_all_data(dst): 
    nodes = list_active_time_nodes()
    data = OrderedDict()

    for node in nodes: 
        node_data = OrderedDict()
        node_data['name'] = node.name
        node_data['namespace'] = node.namespace 
        node_data['ref'] = node.get_ref()
        node_data['execptions'] = node.get_exceptions()
        node_data['values'] = node.get_keyframes()
        data[node.namespace] = node_data

    return file_utils.ymlDumper(dst, data)


def export_data(ns, dst): 
    pm_node = get_local_retime(ns)
    if pm_node and pm.objExists(pm_node): 
        node = TimeStepNode(ns)
        if node: 
            if not os.path.exists(os.path.dirname(dst)): 
                os.makedirs(os.path.dirname(dst))
            data = file_utils.ymlLoader(dst) if os.path.exists(dst) else OrderedDict()
            node_data = OrderedDict()
            node_data['name'] = node.name
            node_data['namespace'] = node.namespace 
            node_data['ref'] = node.get_ref()
            node_data['execptions'] = node.get_exceptions()
            node_data['values'] = node.get_keyframes()
            data[node.namespace] = node_data
            return file_utils.ymlDumper(dst, data)


def import_data(src, namespaces=[], connect=True): 
    data = file_utils.ymlLoader(src)
    for k, v in data.items(): 
        allow = True 
        ns = v['namespace']
        if namespaces: 
            allow = True if ns in namespaces else False 
        if allow: 
            print('-----------', allow)
            node = TimeStepNode(ns)
            print('retime', v['values'])
            node.retime_curve(v['values'])
            node.set_exceptions(v['execptions'])
            return node
            # if connect: 
            #     node.connect()


def get_data_file(scene): 
    return '{}/{}'.format(scene.path.scheme(key='on2DataPath').abs_path(), scene.output_name(outputKey='on2_data', hero=True))


def connect_cache_data(scene, namespace, target_namespace=None): 
    path = get_data_file(scene)
    node = import_data(path, namespaces=[namespace], connect=False)
    if node: 
        if target_namespace: 
            node.connect_to_namespace(target_namespace)
        else: 
            node.connect()
    return node
        

def test(): 
    # to pia 
    # this is example scene 
    # P:/Bikey/scene/work/ep0/bk_ep0_q0010_s0000/anim/main/maya/bk_ep0_q0010_s0000_anim_main.v004.TA.ma
    # 1. export on2 data to this path from this command 
    path = scene.path.scheme(key='on2DataPath')
    filename = scene.output_name(outputKey='on2_data', hero=True)
    # $RFPUBL/Bikey/scene/publ/ep0/bk_ep0_q0010_s0000/_data/on2Data/bk_ep0_q0010_s0000_on2data.hero.yml
    # 




'''
# EXAMPLE:
# create TT curve
tt = create_retime_curve()

# make on1 animation become on 2
namespace = 'raam_001'
exceptions = ['raam_001:Root_Ctrl']
connect_retime(namespace=namespace, exceptions=exceptions)


# After connecting retime curve to sim cache, this should make sim cache becomes on 2s.
# But sim cache will lack behind anim cache who's translate is on 1s.
# track geo
sim_geo = pm.PyNode('sim_on1:ClothCover_Geo')
anim_geo = pm.PyNode('anim_on2:ClothCover_Geo')
vtx_id = 0  # point of track. Should be pretty still and hidden

track_geo(parent=anim_geo, child=sim_geo, vtx_id=0)

----
ts = on2_utils.TimeStep('stormRebelArmy_001')
ts.connect()

ts.exclude(['stormRebelArmy_001:ChestFk_Ctrl'])
ts.include(['stormRebelArmy_001:ChestFk_Ctrl'])
ts.disconnect()
ts.select()

'''