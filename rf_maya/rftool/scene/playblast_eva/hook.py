import os
import sys
import maya.cmds as mc
from rf_maya.lib import sequencer_lib

def list_sequencer():
	return mc.ls(type='shot')

def shot_info(shotName):
	if mc.objExists(shotName):
		return sequencer_lib.shot_info(shotName)
	return 0, 0, 0, 0

def order_shot(shotNames, mode='time'):
	if mode == 'name':
		return sorted(shotNames)
	if mode == 'time':
		shotDict = dict()
		timeOrder = []
		missingShots = []
		for shotName in shotNames:
			if mc.objExists(shotName) and mc.objectType(shotName, isType='shot'):
				startFrame, endFrame, seqStartFrame, seqEndFrame = sequencer_lib.shot_info(shotName)
				shotDict[seqStartFrame] = shotName
			else:
				missingShots.append(shotName)

		timeOrder = [shotDict[time] for time in sorted(shotDict.keys())] + missingShots
		return timeOrder


def obj_exists(obj):
	return mc.objExists(obj)

def camera_info(shotName):
	return mc.shot(shotName, q=True, cc=True) if mc.objExists(shotName) else None

def set_camera(shotName):
	cameraShape = camera_info(shotName)
	try: 
		mc.setAttr('%s.filmFit' % cameraShape, 1)
		mc.setAttr('%s.overscan' % cameraShape, 1)
	except Exception as e: 
		pass 

	overscan = mc.getAttr('%s.overscan' % cameraShape)
	return True if overscan == 1 else False 

def list_shots(scene):
	# list by structure
	scene = scene.copy()
	sequenceKey = scene.sequence
	shotDir = os.path.split(scene.path.name().abs_path())[0]
	allShots = list_folder(shotDir)
	entityNames = [a for a in allShots if sequenceKey in a]
	shots = []

	for shot in entityNames:
		scene.extract_name(shot, workfileKey='sgName', force=True)
		if not scene.name_code == 'all':
			shots.append(scene.name_code)

	return shots

def list_assets(scene): 
	# list by structure
	scene = scene.copy()
	return [scene.name]

def list_folder(path):
    return [d for d in os.listdir(path) if os.path.isdir(os.path.join(path,d))]
