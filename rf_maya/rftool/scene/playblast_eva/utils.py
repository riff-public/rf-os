import os
import sys
import maya.cmds as mc
from rf_maya.lib import playblast_lib
from rf_utils.context import context_info
from rf_utils import publish_info
# from rf_utils import user_info
# from rf_utils.sg import sg_process
# from rf_app.publish.asset import sg_hook
from rf_utils import file_utils
# from rf_utils import daily
from rftool.utils import maya_utils

reload(playblast_lib)
reload(playblast_lib.playblast_converter)
reload(context_info)
# reload(sg_hook)

def playblast_wip(scene, shot, showHud=True, light=False, subversion=False, format ='qt', _part ='', stepKey=[]):
    user = os.environ.get('RFUSER')
    workspaceOutput = scene.path.work_output().abs_path()
    scene.context.update(publishVersion=scene.version)
    filename = scene.publish_name(ext='.mov')
    if _part:
        scene2 = scene.copy()
        scene2.context.update(name_code=_part, publishVersion = scene2.version)
        filename =  scene2.publish_name(ext='.png')

    dst = '%s/%s' % (workspaceOutput, filename)

    if format == 'qt':
        print 'test', filename
        playblast_lib.playblast_sequencer_hud(dst, shot, showHud=showHud, light=False, stepKey=stepKey)
    elif format == 'image':
        print '{}/{}/{}'.format(workspaceOutput, scene.version, filename)
        # with maya_utils.FreezeViewport():
        switch_viewport_vp2()
        old_pref = mc.colorManagementPrefs(cmEnabled=True, q=True)
        mc.colorManagementPrefs(cmEnabled=False,e=True)
        playblast_lib.playblast_sequencer(os.path.join(workspaceOutput, _part), shotNames=[shot], size=(2474, 1282), format= format, codec='png', combine=False, file_name= '{name}_{shot}{ext}'.format(name=scene.name, shot=_part, ext='.png'), stepKey=stepKey)
        mc.colorManagementPrefs(cmEnabled=old_pref,e=True)
    # else:
    #     playblast_lib.playblast_sequencer(dst, shotNames=[shot], size=(2474, 1282), format='qt', codec='H.264', combine=False)

    return dst

def playblast_to_queue(scene, startFrame, endFrame, camera, shotName, task_name='unknown', sg_upload=True):
    # path = context_info.RootPath(str(scene.path.path())).abs_path()
    from rf_utils.deadline import playblast_queue
    reload(playblast_queue)
    active_cam = get_active_cam()
    if not active_cam:
        no_active_cam_err = 'No active camera found, please click on a viewport.'
        logger.error(no_active_cam_err)
        raise ValueError(no_active_cam_err)
    preivew_light = playblast_lib.add_preview_light_playblast(camera)
    path = mc.file(s=True)
    cur_cam = mc.shot(shotName, cc=True, q=True)
    fcl = mc.getAttr('%s.focalLength' % cur_cam)
    # dst
    workspaceOutput = scene.path.work_output().abs_path()
    scene.context.update(publishVersion=scene.version)
    audioMov = scene.publish_name(ext='.wav')
    filename = scene.publish_name(ext='.png').replace('.png', ".####.png")
    dst = '%s' % workspaceOutput
    # dst = workspaceOutput
    # call queue
    set_resolution(scene.project)
    shot_entity = context_info.ContextPathInfo(path=filename)
    sended_job = playblast_queue.playblast(
        path,
        int(startFrame),
        int(endFrame),
        camera,
        dst,
        shotName,
        filename,
        audioMov,
        scene,
        focalLength=fcl,
        task_name=task_name,
        sg_upload=sg_upload
    )
    playblast_lib.remove_preview_light(active_cam)
    mc.file(s=True)

    return sended_job

def switch_viewport_vp2():
    VP2_NAME = 'vp2Renderer'
    renderers = {}
    for panel in mc.lsUI(editors=True): 
        if panel.find('modelPanel') != -1:
            renderer = mc.modelEditor(panel, q=True, rnm=True)
            renderers[panel] = renderer
            if renderer != VP2_NAME:  # switch to VP2
                try:
                    mc.modelEditor(panel, e=True, rendererName=VP2_NAME)
                except:
                    pass

# def playblast_daily(scene, shot, taskName, inputFile=None, showHud=True, playlists=[], light=False):
#     dailyDst = get_daily_version(scene)
#     user = user_info.User()
#     description = 'daily'
#     status = 'rev'
#     taskEntity = get_task(scene, taskName)

#     if taskEntity:
#         if inputFile:
#             dailyDst = file_utils.copy(inputFile, dailyDst)
#         if not inputFile:
#             dailyDst = playblast_lib.playblast_sequencer_hud(dailyDst, shot, hudProject=scene.project, hudUser=user.name(), hudStep=scene.step, hudFilename=scene.filename, hudShot=shot, showHud=showHud, light=light)

#         versionResult = sg_hook.publish_daily(scene, taskEntity, status, [dailyDst], user.sg_user(), description, playlistEntities=playlists)
#         dailyResult = daily.submit(scene, [dailyDst])
#     return dailyDst

# def submit_playblast_publish(scene, taskName, inputFile, status='apr'):
#     publishDst, heroDst = get_publish_outputs(scene, inputFile)
#     user = user_info.User()
#     description = '%s publish' % scene.step
#     taskEntity = get_task(scene, taskName)

#     # copy to hero
#     file_utils.copy(inputFile, heroDst) if not inputFile == heroDst else None
#     file_utils.copy(inputFile, publishDst) if not inputFile == publishDst else None

#     versionResult = sg_hook.publish_version(scene, taskEntity, status, [publishDst], user.sg_user(), description, uploadOption='default')
#     return publishDst


# def get_daily_version(scene, ext='.mov'):
#     scene.context.update(res='shot')
#     regInfo = publish_info.RegisterShotInfo(scene)
#     regVersion = regInfo.get_latest_version('workspace') or 'v001'
#     scene.context.update(publishVersion=regVersion)
#     filename = scene.daily_name(version='subversion', ext=ext)
#     scene.context.update(publishVersion=scene.subversion)
#     outputDir = scene.path.outputImg().abs_path()
#     scene.context.update(puplishVersion=regVersion)
#     dst = '%s/%s' % (outputDir, filename)
#     return dst


def get_publish_outputs(scene, inputFile):
    # setup context
    entity = context_info.ContextPathInfo(path=inputFile)
    scene.context.update(res='shot')
    regInfo = publish_info.RegisterShotInfo(scene)
    # regVersion = regInfo.get_latest_version('workspace')
    regVersion = entity.version
    scene.context.update(publishVersion=regVersion)

    # publish name
    publishDir = scene.path.scheme(key='outputImgPath').abs_path()
    publishFile = scene.publish_name(ext='.mov')
    publishFilePath = '%s/%s' % (publishDir, publishFile)
    # hero name
    heroDir = scene.path.scheme(key='outputHeroImgPath').abs_path()
    heroFile = scene.publish_name(hero=True, ext='.mov')
    heroFilePath = '%s/%s' % (heroDir, heroFile)

    return publishFilePath, heroFilePath


# def get_task(scene, taskName):
#     scene.context.use_sg(sg_process.sg, project=scene.project, entityType='scene', entityName=scene.name)
#     taskEntity = sg_process.get_one_task_by_step(scene.context.sgEntity, scene.step, taskName, create=True)
#     return taskEntity


def set_resolution(project):
    from rf_utils import project_info
    proj = project_info.ProjectInfo(project)
    proj.render.set_size(mode='preview')
    mc.setAttr('defaultResolution.deviceAspectRatio', 2.353)

def list_render_layer():
    list_layer = []
    render_layer = mc.ls(type='renderLayer')
    for layer in render_layer:
        if not 'defaultRenderLayer' in layer:
            list_layer.append(layer)
    return list_layer

def create(file_path):
    file_path = check_exists(file_path)
    all_file = os.listdir(file_path)
    increment_count=[]
    for file in all_file:
        increment = re.findall ( 'v' + '[0-9]{3}', file ) ;
        if increment:
            increment_count.append(increment[0])
    if increment_count:
        version_lastest = int(max(increment_count).split('v')[1])
        new_version = 'v%03d'%(version_lastest+1)
    else:
        new_version = 'v001'
    return new_version

def get_stepkey(): 
    from rftool.utils import maya_utils
    reload(maya_utils)
    allFrames = []

    objs = mc.ls(sl=True)
    if not objs: 
        return 

    for obj in objs: 
        frames = maya_utils.find_keyframe(obj)
        if frames: 
            allFrames += frames

    return sorted(list(set(allFrames)))
