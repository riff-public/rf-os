#!/usr/bin/env python
# -- coding: utf-8 --

# v.0.0.1 polytag switcher
_title = 'PlayblastEVA'
_version = 'v.0.0.1'
_des = 'alpha for Eva'
uiName = 'PlayblastUI'

#Import python modules
import sys
import os
import getpass
import subprocess
from collections import OrderedDict
from functools import partial

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework modules
import rf_config as config
reload(config)
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.context import context_info
from rf_utils import file_utils
from rf_utils.widget import shot_list_widget
from rf_utils.widget import dialog
from rf_utils import publish_info
# import thread
# reload(thread)
reload(file_utils)
reload(shot_list_widget)
from rf_utils import icon
import utils
reload(utils)
import pb_ui
reload(pb_ui)
import hook
reload(hook)
import maya.cmds as mc
# from rf_qc.maya_lib.scene import check_shot_lockdown
# reload(check_shot_lockdown)

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

class Config: 
    autoPublishStep = ['anim']
    autoTaskMap = {'anim': 'anim'}
    approvedStatus = 'apr'
    wip = 'wip'


class Color:
    red = [255, 100, 100]
    grey = [100, 100, 100]
    green = [100, 255, 100]
    lightGrey = [200, 200, 200]

class PlayblastCore(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(PlayblastCore, self).__init__(parent)

        # ui read
        self.ui = pb_ui.Ui()
        self.setCentralWidget(self.ui)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setObjectName(uiName)
        self.resize(700, 500)

        # var
        self.pipeline = False

        self.init_ui()
        self.init_functions()
        self.init_signals()

    def init_ui(self):
        self.set_default()
        self.set_context()
        self.list_step()
        self.list_shot()

    def init_functions(self):
        self.ui.file_listWidget.open_explorer = self.open_explorer

    def init_signals(self):
        # item selected
        self.ui.shot_listWidget.itemSelectionChanged.connect(self.shot_selected)

        # buttons
        self.ui.playblast_pushButton.clicked.connect(self.playblast)
        self.ui.play_pushButton.clicked.connect(self.play_in_rv)
        # self.ui.daily_pushButton.clicked.connect(self.send_to_sg)
        self.ui.playblastQueqe_pushButton.clicked.connect(partial(self.playblast, True))

        # comboBox
        self.ui.task_comboBox.currentIndexChanged.connect(self.check_daily_input)
        self.ui.file_listWidget.itemSelected.connect(self.check_daily_input)

        # checkBox
        self.ui.showLatest_checkBox.stateChanged.connect(self.list_mov_files)
        self.ui.step_comboBox.currentIndexChanged.connect(self.list_mov_files)
        self.ui.selectAll_checkBox.stateChanged.connect(self.select_all)
        self.ui.allProcess_checkBox.stateChanged.connect(self.shot_selected)
        self.ui.sortedOrder_checkBox.stateChanged.connect(self.list_shot)
        self.ui.showAll_checkBox.stateChanged.connect(self.show_all)

        # ui
        self.ui.showOption_checkBox.stateChanged.connect(self.option_selected)
        self.ui.playblast_part_checkBox.stateChanged.connect(self.list_render_part)
        self.ui.playblast_part_pushButton.clicked.connect(self.playblast_part)
        # self.ui.daily_checkBox.stateChanged.connect(self.daily_selected)
        # self.ui.publish_checkBox.stateChanged.connect(self.publish_selected)

    def set_default(self):
        # self.ui.autoPlay_checkBox.setChecked(True)
        self.ui.playblast_frame.setVisible(False)
        self.ui.daily_frame.setVisible(False)
        self.ui.daily_pushButton.setVisible(False)
        # self.ui.hud_checkBox.setChecked(True)
        # self.ui.publish_checkBox.setEnabled(True)
        # self.ui.autoPublish_checkBox.setChecked(False)

        # set custom message for lock check 
        # check_shot_lockdown.Message.animMesg = 'Shot นี้ถูกส่งไปเรนเดอร์แล้ว ติอต่อ พี่น้อง พี่มิ้ม เพื่อแจ้ง Alt+Chaya เพื่อขอแก้ก่อนจะส่ง Playblast ใหม่ได้'

    def list_render_part(self):
        self.ui.playblast_part_list_wid.clear()
        if self.ui.playblast_part_checkBox.isChecked():
            render_parts = utils.list_render_layer()
            print render_parts
            for itm in render_parts:
                wid_item = QtWidgets.QListWidgetItem(itm)
                newFont = QtGui.QFont("Cambria", 13, QtGui.QFont.Bold) 
                wid_item.setFont(newFont)
                # wid_item.setFlags(QtCore.Qt.ItemIsSelectable)#->setFlags(item->flags() | Qt::ItemIsUserCheckable)
                wid_item.setFlags(wid_item.flags() | QtCore.Qt.ItemIsUserCheckable)
                wid_item.setCheckState(QtCore.Qt.Unchecked)
                self.ui.playblast_part_list_wid.addItem(wid_item)

    def playblast_part(self):
        list_play_part = []
        stepKey = []
        stepKeyOption = self.ui.playStepKey_checkBox.isChecked()
        if stepKeyOption: 
            stepKey = utils.get_stepkey()
            if not stepKey: 
                dialog.MessageBox.error('Error', 'Stepped key option is selected. Please select atlease one object with keyframe')
                return 
        for row_count in range(0, self.ui.playblast_part_list_wid.count()):
            if self.ui.playblast_part_list_wid.item(row_count).checkState() != QtCore.Qt.CheckState.Unchecked:
                layer_name = self.ui.playblast_part_list_wid.item(row_count).text()
                mc.editRenderLayerGlobals( currentRenderLayer=layer_name )
                version = self.scene.version
                output_path = self.scene.path.work_output().abs_path()
                layer_path = os.path.join(output_path, layer_name)
                if not os.path.exists(layer_path):
                    os.makedirs(layer_path)
                utils.playblast_wip(self.scene, self.shotName, showHud=False, format='image', _part= layer_name, stepKey=stepKey)
                
                # list_play_part.append(self.ui.playblast_part_list_wid.item(row_count).text())


        # mc.editRenderLayerGlobals( currentRenderLayer= )

        #  create folder
        # utils.increment_version(output_path)


    def set_context(self):
        self.scene = context_info.ContextPathInfo()

        if self.scene.project:
            self.shotName = self.scene.name_code
            self.shotMode = True if not self.shotName == 'all' else False
            self.pipeline = True
            self.ui.project_label.setText(self.scene.project)
            self.ui.name_label.setText(self.scene.name)

        else:
            state = False if self.scene.project else True
            self.set_warning(state)

    def set_warning(self, state):
        """ warning out of the pipeline """
        self.ui.project_label.setFont(QtGui.QFont("Arial", 9))
        color = [240, 60, 60]
        if state:
            self.ui.project_label.setText('-- Not in the Pipeline --')
            self.ui.project_label.setStyleSheet('color: rgb(%s, %s, %s)' % (color[0], color[1], color[2]))
        else:
            self.ui.project_label.setStyleSheet('')



    def list_step(self):
        self.ui.step_comboBox.clear()

        if self.pipeline:
            path = self.scene.path.step()
            browsePath = os.path.split(path.abs_path())[0]
            steps = sorted(file_utils.list_folder(browsePath))

            for i, step in enumerate(steps):
                display = '%s - Current' % step if step == self.scene.step else step
                self.ui.step_comboBox.addItem(display)
                self.ui.step_comboBox.setItemData(i, step, QtCore.Qt.UserRole)

            # set default to step
            index = steps.index(self.scene.step) or 0
            self.ui.step_comboBox.setCurrentIndex(index)

        else:
            self.ui.step_comboBox.addItem('-- Not in the Pipeline --')

    def list_shot(self):
        if self.scene.project:
            self.add_shots(False)

    def add_shots(self, showAll=True):
        shots = hook.list_sequencer()
        
        if self.scene.entity_type == 'scene': 
            regShots = hook.list_shots(self.scene)
        if self.scene.entity_type == 'asset': 
            self.scene.context.update(res='md') # for asset mode
            regShots = hook.list_assets(self.scene)

        combineShots = sorted(list(set(shots + regShots)))
        mode = 'time' if self.ui.sortedOrder_checkBox.isChecked() else 'name'
        combineShots = hook.order_shot(combineShots, mode=mode)
        self.ui.shot_listWidget.clear()

        for i, shotName in enumerate(combineShots):
            startFrame, endFrame, seqStartFrame, seqEndFrame = hook.shot_info(shotName)
            camera = hook.camera_info(shotName)
            status = self.shot_status(shotName)
            isCreate = hook.obj_exists(shotName)
            data = {'shotName': shotName, 'startFrame': startFrame, 'endFrame': endFrame, 'seqStartFrame': seqStartFrame, 'seqEndFrame': seqEndFrame, 'status': status, 'playable': isCreate, 'camera': camera}
            frameRange = '%s - %s' % (startFrame, endFrame)

            if showAll or status:
                widgetItem = shot_list_widget.ShotItemBasic()
                self.set_shot_item(widgetItem, shotName, frameRange, status, isCreate)
                item = self.ui.shot_listWidget.add_widget_item(widgetItem, data)

    def shot_status(self, shotName):
        if self.scene.entity_type == 'scene': 
            if self.shotName == 'all':
                return True
            status = True if shotName.lower() == self.shotName.lower() else False
            return status
        return True

    def set_shot_item(self, widgetItem, shotName, frameRange, state, isCreate):
        color = [200, 200, 200] if state else [100, 100, 100]
        rangeColor = [200, 200, 100] if isCreate else [200, 100, 100]
        iconPath = icon.camera if state else icon.cameraNa
        color = color if isCreate else [200, 100, 100]
        widgetItem.set_shot_color(color)
        widgetItem.set_range_color(rangeColor)
        widgetItem.set_text_shot(shotName)
        widgetItem.set_text_frame(frameRange)
        widgetItem.set_icon(iconPath)


    def shot_selected(self, data):
        self.list_mov_files()
        self.button_check()

    def option_selected(self, value):
        state = True if value else False
        self.ui.playblast_frame.setVisible(state)

    # def daily_selected(self, value):
    #     state = True if value else False
    #     self.ui.daily_frame.setVisible(state)
    #     self.ui.daily_pushButton.setVisible(state)
    #     self.ui.file_listWidget.clear()
    #     self.add_shots(showAll=not state)
    #     self.fetch_sg_data()

    def publish_selected(self, value):
        state = True if value else False
        self.ui.daily_checkBox.setChecked(True) if not self.ui.daily_checkBox.isChecked() else None
        buttonLabel = 'Daily' if not self.ui.publish_checkBox.isChecked() else 'Publish'
        self.ui.daily_pushButton.setText(buttonLabel)


    def list_mov_files(self):
        items = self.ui.shot_listWidget.selected_items()
        fileDict = OrderedDict()
        step = self.ui.step_comboBox.itemData(self.ui.step_comboBox.currentIndex(), QtCore.Qt.UserRole) # allow mov list from different department context
        allProcess = self.ui.allProcess_checkBox.isChecked()

        if items:
            for item in items:
                shotData = item.data(QtCore.Qt.UserRole)
                shotName = shotData['shotName']
                status = shotData['status']
                # if status:
                self.scene = self.set_shot_context(self.scene, step, shotName)

                files = self.list_process_mov(self.scene, allProcess=allProcess)
                fileDict[shotName] = {'files': files}

            self.show_mov(fileDict)

    def list_process_mov(self, scene, allProcess=False):
        processes = self.get_all_process(scene, allProcess=allProcess)
        allFiles = []
        originalProcess = scene.process
        for process in processes:
            scene.context.update(process=process)
            workspaceOutput = scene.path.work_output().abs_path()
            files = file_utils.list_file(workspaceOutput) if os.path.exists(workspaceOutput) else []
            files = ['%s/%s' % (workspaceOutput, a) for a in files]
            allFiles += files
        # restore
        scene.context.update(process=originalProcess)
        return allFiles

    def get_all_process(self, scene, allProcess=False):
        if not allProcess:
            return [scene.process]
        processPath = scene.path.process().abs_path()
        processes = file_utils.listFolder(os.path.split(processPath)[0])
        return processes

    def button_check(self):
        datas = self.ui.shot_listWidget.selected_datas()
        state = any([a['status'] for a in datas]) if datas else False
        self.ui.playblast_pushButton.setEnabled(state)

    def select_all(self, value):
        state = True if value else False
        items = self.ui.shot_listWidget.items()
        self.ui.shot_listWidget.blockSignals(True)
        self.ui.shot_listWidget.select_items(items, state)
        self.list_mov_files() if state else self.ui.file_listWidget.clear()
        self.ui.shot_listWidget.blockSignals(False)

    def show_all(self, value):
        state = True if value else False
        self.add_shots(state)

    def show_mov(self, fileDict):
        showLatest = self.ui.showLatest_checkBox.isChecked()
        self.ui.file_listWidget.clear()
        bgColor1 = [60, 60, 60]
        bgColor2 = [20, 20, 20]
        colors = [bgColor1, bgColor2]
        count = 0

        for shotName, data in fileDict.iteritems():
            files = data['files']
            files = file_utils.sort_by_date(files)
            color = colors[count % 2]
            if showLatest:
                data = {'shotName': shotName, 'filePath': files[-1] if files else []}
                item = self.ui.file_listWidget.add_data(files[-1], data) if files else None
                item.setBackground(QtGui.QColor(color[0], color[1], color[2])) if item else None

            else:
                for filePath in files:
                    data = {'shotName': shotName, 'filePath': filePath}
                    item = self.ui.file_listWidget.add_data(filePath, data)
                    item.setBackground(QtGui.QColor(color[0], color[1], color[2]))

            if files:
                count += 1

    def playblast(self, queue=False):
        # allow = check_shot_lockdown.run(self.scene) if self.scene.step == 'anim' else True
        stepKeyOption = self.ui.playStepKey_checkBox.isChecked()
        stepKey = []
        if stepKeyOption: 
            stepKey = utils.get_stepkey()
            if not stepKey: 
                dialog.MessageBox.error('Error', 'Stepped key option is selected. Please select atlease one object with keyframe')
                return 

        items = self.ui.shot_listWidget.selected_items()
        showHud = self.ui.hud_checkBox.isChecked()
        autoPublish = self.ui.autoPublish_checkBox.isChecked()

        borderCrop = self.ui.borderCrop_checkBox.isChecked()
        step = self.scene.step # always play from scene context
        movFiles = []
        autoPublishData = []
        if items:
            for item in items:
                print 1
                shotData = item.data(QtCore.Qt.UserRole)
                print shotData
                shotName = shotData['shotName']
                playable = shotData['playable']
                startFrame = shotData['startFrame']
                endFrame = shotData['endFrame']
                camera = shotData['camera']

                camera_namespace = camera.split(':')[0]
                camGrid_Ctlr = '%s:camCtrl_camCtrl'%camera_namespace
                framGrid_attr = mc.listAttr(camGrid_Ctlr)

                if 'frameGrid' in  framGrid_attr:
                    print camGrid_Ctlr
                    if not borderCrop:
                        mc.setAttr('%s.frameGrid'%camGrid_Ctlr, 0)
                taskName = ''
                print 2
                # if self.ui.daily_checkBox.isChecked():
                #     taskEntity = self.ui.task_comboBox.itemData(self.ui.task_comboBox.currentIndex(), QtCore.Qt.UserRole)
                #     taskName = taskEntity.get('content', '')
                # self.scene = self.set_shot_context(self.scene, step, shotName)

                if queue:
                    upload = True if taskName else False
                    sended_job, destination = utils.playblast_to_queue(self.scene, startFrame, endFrame, camera, shotName, task_name=taskName, sg_upload=upload)
                    logger.debug('Submitted to queue')
                    if sended_job:
                        self.update_status(item, 'Sent to queue', 'complete')
                    else:
                        self.update_status(item, 'Cannot sent to queue. please resent', 'failed')
                    continue

                if playable:
                    print 3
                    self.update_status(item, 'Playblasting ...', 'normal')
                    settingResult = hook.set_camera(shotName) 
                    light = self.ui.light_checkBox.isChecked()

                    if not settingResult: 
                        dialog.MessageBox.error('Error', 'Wrong Overscan!\nPlease Edit cam to fix this issue and re-Export')
                        continue

                    try:
                        print 4
                        result = utils.playblast_wip(self.scene, shotName, showHud, light, stepKey=stepKey)
                        movFiles.append(result)
                        print 5
                        autoPublishData.append({'shotName': shotName, 'filePath': result})
                        self.update_status(item, 'Finished', 'complete')

                    except Exception as e:
                        logger.error(e)
                        self.update_status(item, 'Failed', 'failed')
                else:
                    logger.warning('No shot name %s' % shotName)
                    self.update_status(item, 'No shot', 'failed')

                if 'frameGrid' in  framGrid_attr:
                    mc.setAttr('%s.frameGrid'%camGrid_Ctlr, 1)

            if movFiles:
                self.list_mov_files()
                self.open_rv(movFiles) if self.ui.autoPlay_checkBox.isChecked() else None

            # if autoPublish: 
            #     if autoPublishData: 
            #         logger.info('Auto publish ...')
            #         self.auto_publish(autoPublishData)


    def set_shot_context(self, scene, step, shotName):
        scene.context.update(entityCode=shotName, step=step)
        scene.context.update(entity=scene.sg_name)
        return scene

    def play_in_rv(self):
        files = self.ui.file_listWidget.selected_files()
        files = [a['filePath'] for a in files]
        self.open_rv(files)

    def open_rv(self, filenames):
        rvPath = config.Software.rv
        if not os.path.exists(rvPath):
            root, end = rvPath.split('RV-')
            version = [a for a in file_utils.list_folder(root) if 'RV-' in a]
            if version:
                rvPath = '%s%s/%s' % (root, version[0], '/'.join(end.split('/')[1:]))
            else:
                dialog.MessageBox.error('Error', 'No RV installed')

        commands = [rvPath]
        commands += filenames
        subprocess.Popen(commands)


    def update_status(self, item, message, status):
        if status == 'normal':
            color = Color.lightGrey
        if status == 'complete':
            color = Color.green
        if status == 'failed':
            color = Color.red
        widgetItem = self.ui.shot_listWidget.widget_item(item)
        widgetItem.set_text_status(message)
        widgetItem.set_status_color(color)
        QtWidgets.QApplication.processEvents()

    # def fetch_sg_data(self):
    #     if not self.shotMode:
    #         datas = self.ui.shot_listWidget.datas()
    #         # get task from first shot
    #         shotName = datas[0]['shotName'] if datas else ''
    #         self.scene.context.update(entityCode=shotName)

    #     self.thread = thread.GetTaskPlaylist(self.scene)
    #     self.thread.value.connect(self.set_task_playlist)
    #     self.thread.start()

    def set_task_playlist(self, data):
        tasks = data['task']
        playlists = data['playlist']
        self.set_task(tasks)
        self.set_playlist(playlists)
        self.check_daily_input()
        self.auto_task_selection()


    def check_daily_input(self):
        taskEntity = self.ui.task_comboBox.itemData(self.ui.task_comboBox.currentIndex(), QtCore.Qt.UserRole)
        inputFile = self.ui.file_listWidget.selected_file()
        taskName = ''
        if taskEntity:
            taskName = taskEntity.get('content', '')

        state = True if taskName and inputFile else False
        if self.shotMode:
            state = state if len(self.ui.file_listWidget.selected_files()) == 1 else False
        self.ui.daily_pushButton.setEnabled(state)

        if taskName:
            self.check_apr_task(taskEntity)


    def check_apr_task(self, taskEntity):
        # confirm status checking
        status = taskEntity.get('sg_status_list') if taskEntity else ''
        taskName = taskEntity.get('content', '') if taskEntity else ''
        if status == 'apr':
            result = self.confirm_dialog('Confirm',  '"%s" task already approved. Do you still want to daily this task?' % taskName)
            if not result:
                self.ui.task_comboBox.setCurrentIndex(0)


    def set_task(self, tasks):
        self.ui.task_comboBox.clear()
        self.ui.task_comboBox.addItem('- Select Task -')
        self.ui.task_comboBox.setItemData(0, dict(), QtCore.Qt.UserRole)

        for i, task in enumerate(tasks):
            self.ui.task_comboBox.addItem(task['content'])
            self.ui.task_comboBox.setItemData(i+1, task, QtCore.Qt.UserRole)

    def set_playlist(self, playlists):
        self.ui.playlist_comboBox.clear()
        self.ui.playlist_comboBox.addItem('- Select Playlist -')
        self.ui.playlist_comboBox.setItemData(0, dict(), QtCore.Qt.UserRole)

        for i, playlist in enumerate(playlists):
            self.ui.playlist_comboBox.addItem(playlist['code'])
            self.ui.playlist_comboBox.setItemData(i+1, playlist, QtCore.Qt.UserRole)

    def auto_task_selection(self):
        """ set auto task work only open scene from task manager """
        entity = self.scene.copy()
        entity.use_env_context()
        currentTask = entity.task
        self.scene.context.update(task=entity.task)
        allTasks = [self.ui.task_comboBox.itemData(a, QtCore.Qt.UserRole) for a in range(self.ui.task_comboBox.count())]

        for i, task in enumerate(allTasks):
            if task:
                content = task.get('content')
                if content == currentTask:
                    self.ui.task_comboBox.setCurrentIndex(i)
                    self.ui.task_comboBox.setEnabled(False)


    # def send_to_sg(self):
    #     """ send to daily or publish """
    #     publish = self.ui.publish_checkBox.isChecked()

    #     playlists = []
    #     taskEntity = self.ui.task_comboBox.itemData(self.ui.task_comboBox.currentIndex(), QtCore.Qt.UserRole)
    #     playlist = self.ui.playlist_comboBox.itemData(self.ui.playlist_comboBox.currentIndex(), QtCore.Qt.UserRole)
    #     taskName = taskEntity.get('content', '')
    #     status = taskEntity.get('sg_task_status')

    #     inputDatas = self.ui.file_listWidget.selected_files()
    #     self.submitCount = len(inputDatas)
    #     self.completeCount = 0

    #     if not publish:
    #         self.ui.status.setText('Submiting daily ...')
    #         self.ui.daily_pushButton.setEnabled(False)
    #         self.submitThread = thread.SubmitDialies(self.scene, taskName, inputDatas, playlist)
    #         self.submitThread.status.connect(self.daily_status)
    #         self.submitThread.finished.connect(self.daily_complete)
    #         self.submitThread.progressStatus.connect(self.daily_progress_status)
    #         self.submitThread.start()

    #     if publish:
    #         self.ui.status.setText('Publishing video ...')
    #         self.ui.daily_pushButton.setEnabled(False)
    #         self.submitThread = thread.SubmitPublish(self.scene, taskName, inputDatas)
    #         self.submitThread.status.connect(self.daily_status)
    #         self.submitThread.finished.connect(self.publish_complete)
    #         self.submitThread.progressStatus.connect(self.daily_progress_status)
    #         self.submitThread.start()


    # def auto_publish(self, inputDatas): 
    #     """ auto publish """ 
    #     # inputDatas [{u'shotName': u's0010', u'filePath': u'P:/projectName/scene/work/ep01/pr_ep01_q0070_s0010/anim/main/output/pr_ep01_q0070_s0010_anim_main.v007.TA.mov'}]
    #     self.submitCount = len(inputDatas)
    #     self.completeCount = 0
    #     taskName = Config.autoTaskMap.get(self.scene.step, None)
        
    #     if self.scene.step in Config.autoPublishStep and taskName: 
    #         self.submitThread = thread.SubmitPublish(self.scene, taskName, inputDatas, pubStatus=Config.wip)
    #         self.submitThread.status.connect(self.daily_status)
    #         self.submitThread.finished.connect(partial(self.publish_complete, False))
    #         self.submitThread.progressStatus.connect(self.daily_progress_status)
    #         self.submitThread.start()


    def daily_complete(self):
        if self.completeCount == self.submitCount:
            message = 'Submit %s shots complete' % self.submitCount
            dialogMessage = 'Send to daily complete!!'
            dialog.MessageBox.success('Daily status', dialogMessage)

        else:
            errorCount = self.submitCount - self.completeCount
            message = 'Submit %s/%s shots complete' % (self.completeCount, self.submitCount)
            dialogMessage = 'Not all shots complete\nThere were %s shots error' % errorCount
            dialog.MessageBox.error('Daily status', dialogMessage)

        self.ui.status.setText(message)
        self.ui.daily_pushButton.setEnabled(True)

    
    def publish_complete(self, showDialog=True):
        if self.completeCount == self.submitCount:
            message = 'Published %s shots complete' % self.submitCount
            dialogMessage = 'Publish complete!!'
            if showDialog: 
                dialog.MessageBox.success('Publish status', dialogMessage)

        else:
            errorCount = self.submitCount - self.completeCount
            message = 'Submit %s/%s shots complete' % (self.completeCount, self.submitCount)
            dialogMessage = 'Not all shots complete\nThere were %s shots error' % errorCount
            if showDialog: 
                dialog.MessageBox.error('Publish status', dialogMessage)

        self.ui.status.setText(message)
        self.ui.daily_pushButton.setEnabled(True)


    def daily_progress_status(self, status):
        self.completeCount += 1 if status else 0

    def daily_status(self, dictValue):
        text = dictValue['text']
        text2 = dictValue['text2']
        status = dictValue['status']
        shotName = dictValue['shotName']
        item = self.find_shot_item(shotName)
        self.ui.status.setText(text)
        mode = 'complete' if status else 'failed'

        if item:
            self.update_status(item, text2, mode)


    # def send_to_publish(self):
    #     taskEntity = self.ui.task_comboBox.itemData(self.ui.task_comboBox.currentIndex(), QtCore.Qt.UserRole)
    #     playlist = self.ui.playlist_comboBox.itemData(self.ui.playlist_comboBox.currentIndex(), QtCore.Qt.UserRole)
    #     taskName = taskEntity.get('content', '')

    #     inputDatas = self.ui.file_listWidget.selected_files()
    #     self.submitCount = len(inputDatas)
    #     self.completeCount = 0
    #     self.ui.status.setText('Submiting daily ...')

    #     self.ui.daily_pushButton.setEnabled(False)
    #     self.submitThread = thread.SubmitPublish(self.scene, taskEntity, inputDatas)
    #     self.submitThread.status.connect(self.daily_status)
    #     self.submitThread.finished.connect(self.daily_complete)
    #     self.submitThread.progressStatus.connect(self.daily_progress_status)
    #     self.submitThread.start()

    def find_shot_item(self, shotName):
        items = self.ui.shot_listWidget.items()

        for item in items:
            itemWidget = self.ui.shot_listWidget.listWidget.itemWidget(item)
            shotItem = itemWidget.get_text_shot()

            if shotName == shotItem:
                return item

    def open_explorer(self, item):
        data = item.data(QtCore.Qt.UserRole)
        path = data['filePath']
        subprocess.Popen(r'explorer /select,"%s"' % path.replace('/', '\\'))

    def confirm_dialog(self, title, message):
        msgBox = QtWidgets.QMessageBox( self )
        msgBox.setIcon( QtWidgets.QMessageBox.Information )
        msgBox.setText(message)
        msgBox.setWindowTitle(title)

        # msgBox.setInformativeText(information)
        msgBox.addButton( QtWidgets.QMessageBox.Yes )
        msgBox.addButton( QtWidgets.QMessageBox.No )

        msgBox.setDefaultButton( QtWidgets.QMessageBox.No )
        result = msgBox.exec_()

        if result == QtWidgets.QMessageBox.Yes:
            return True
        return False


def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = PlayblastCore(maya_win.getMayaWindow())
    myApp.show()
    return myApp

# from rftool.scene.playblast import utils
# from rf_utils.context import context_info
# reload(utils)
# scene = context_info.ContextPathInfo()
# utils.playblast_daily(scene, 'S0010', 'layout', inputFile=None)
# from rf_utils import publish_info
# reload(publish_info)
# reg = publish_info.RegisterShotInfo(scene)
# reg.get_latest_version('workspace')
# reload(app.ui.info_widget)
