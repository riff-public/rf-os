import sys
import os
from collections import OrderedDict
import time

import logging
logger = logging.getLogger(__name__)

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

from rf_utils.context import context_info
from rf_utils import file_utils
from rf_utils import publish_info

import utils
reload(utils)
# from rf_utils.sg import sg_utils

# sg = sg_utils.sg

class Config:
    typeMap = {'Asset': 'asset', 'Shot': 'scene'}


class ConnectShotgun(QtCore.QThread):
    status = QtCore.Signal(bool)

    def __init__(self):
        QtCore.QThread.__init__(self)


    def run(self):
        from rf_utils.sg import sg_utils
        global sg
        sg = sg_utils.sg
        self.status.emit(True)


class GetTaskPlaylist(QtCore.QThread):
    value = QtCore.Signal(dict)

    def __init__(self, scene):
        QtCore.QThread.__init__(self)
        self.scene = scene

    def run(self):
        from rf_utils.sg import sg_utils
        tasks = find_task(sg_utils.sg, self.scene.project, self.scene.sg_name, self.scene.step)
        playlists = find_playlist(sg_utils.sg, self.scene.project, self.scene.step)
        self.value.emit({'task': tasks, 'playlist': playlists})


class SubmitDialy(QtCore.QThread):
    """docstring for SubmitDialy"""
    value = QtCore.Signal(dict)
    def __init__(self, scene, taskName, inputFile, playlists):
        super(SubmitDialy, self).__init__()
        self.scene = scene
        self.taskName = taskName
        self.inputFile = inputFile
        self.playlists = playlists

    def run(self):
        result = utils.playblast_daily(self.scene, '', self.taskName, inputFile=self.inputFile, playlists=self.playlists)
        self.value.emit(result)


class SubmitDialies(QtCore.QThread):
    """docstring for SubmitDialy"""
    value = QtCore.Signal(dict)
    status = QtCore.Signal(dict)
    progressStatus = QtCore.Signal(bool)

    def __init__(self, scene, taskName, inputDatas, playlist):
        super(SubmitDialies, self).__init__()
        self.scene = scene
        self.taskName = taskName
        self.inputDatas = inputDatas
        self.playlists = [playlist] if playlist else []

    def run(self):
        count = 1
        for i, inputData in enumerate(self.inputDatas):
            inputFile = inputData['filePath']
            shotName = inputData['shotName']
            self.scene.context.update(entityCode=shotName)
            self.scene.context.update(entity=self.scene.sg_name)

            try:
                self.set_status(shotName, count, True)
                result = utils.playblast_daily(self.scene, '', self.taskName, inputFile=inputFile, playlists=self.playlists)
                self.set_status_text(shotName, 'Complete', 'Complete', True)
                count += 1
                self.progressStatus.emit(True)

            except Exception as e:
                self.set_status_text(shotName, 'Error submitting %s %s' % (shotName, e), 'Error', False)
                logger.error(e)
                self.progressStatus.emit(False)


    def set_status(self, shotName, index, status):
        data = dict()
        text = 'Submiting "%s" to Shotgun ...  (%s/%s)' % (shotName, index, len(self.inputDatas))
        text2 = 'Updating Shotgun ...'
        data = {'shotName': shotName, 'text': text, 'text2': text2, 'status': status}
        self.status.emit(data)

    def set_status_text(self, shotName, text, text2, status):
        data = dict()
        data = {'shotName': shotName, 'text': text, 'text2': text2, 'status': status}
        self.status.emit(data)


class SubmitPublish(QtCore.QThread):
    """docstring for SubmitDialy"""
    value = QtCore.Signal(dict)
    status = QtCore.Signal(dict)
    progressStatus = QtCore.Signal(bool)

    def __init__(self, scene, taskName, inputDatas, pubStatus='apr'):
        super(SubmitPublish, self).__init__()
        self.scene = scene
        self.taskName = taskName
        self.inputDatas = inputDatas
        self.pubStatus = pubStatus

    def run(self):
        count = 1
        for i, inputData in enumerate(self.inputDatas):
            inputFile = inputData['filePath']
            shotName = inputData['shotName']
            self.scene.context.update(entityCode=shotName)
            self.scene.context.update(entity=self.scene.sg_name)

            # try:
            self.set_status(shotName, count, True)
            result = utils.submit_playblast_publish(self.scene, self.taskName, inputFile, status=self.pubStatus)
            self.set_status_text(shotName, 'Complete', 'Complete', True)
            count += 1
            self.progressStatus.emit(True)

            # except Exception as e:
            #     self.set_status_text(shotName, 'Error submitting %s %s' % (shotName, e), 'Error', False)
            #     logger.error(e)
            #     self.progressStatus.emit(False)

    def set_status(self, shotName, index, status):
        data = dict()
        text = 'Submiting "%s" to Shotgun ...  (%s/%s)' % (shotName, index, len(self.inputDatas))
        text2 = 'Updating Shotgun ...'
        data = {'shotName': shotName, 'text': text, 'text2': text2, 'status': status}
        self.status.emit(data)

    def set_status_text(self, shotName, text, text2, status):
        data = dict()
        data = {'shotName': shotName, 'text': text, 'text2': text2, 'status': status}
        self.status.emit(data)


class FindShots(QtCore.QThread):
    """docstring for FindShots"""
    value = QtCore.Signal(list)
    def __init__(self, project, episode, sequence):
        super(FindShots, self).__init__()
        self.project = project
        self.episode = episode
        self.sequence = sequence

    def run(self):
        from rf_utils.sg import sg_utils
        result = find_shots(sg_utils.sg, self.project, self.episode, self.sequence)
        self.value.emit(result)


def find_task(sg, project, shotname, step):
    filters = [['project.Project.name', 'is', project],
                    ['entity.Shot.code', 'is', shotname],
                    ['step.Step.short_name', 'is', step]]
    fields = ['content', 'id', 'entity', 'project', 'sg_status_list']
    result = sg.find('Task', filters, fields)
    return result


def find_playlist(sg, project, step):
    filters = [['project.Project.name', 'is', project],
                    ['sg_department', 'is', step],
                    ['locked', 'is', False]]
    fields = ['code', 'id', 'sg_department', 'project']
    result = sg.find('Playlist', filters, fields)
    return result


def find_shots(sg, project, episode, sequence):
    filters = [['project.Project.name', 'is', project],
                    ['sg_episode.Scene.code', 'is', episode],
                    ['sg_sequence_code', 'is', sequence]]
    fields = ['code', 'sg_shortcode', 'sg_working_duration', 'sg_cut_order']
    result = sg.find('Shot', filters, fields)
    return result
