from Qt import QtCore, QtWidgets
from rf_utils.widget import shot_list_widget
from rf_utils.widget import file_widget
reload(shot_list_widget)
reload(file_widget)


class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)
        self.all_layout = QtWidgets.QVBoxLayout()
        self.title_layout = QtWidgets.QGridLayout()

        self.name_label = QtWidgets.QLabel('-')
        self.title_layout.addWidget(self.name_label, 1, 0, 1, 1)

        self.step_label = QtWidgets.QLabel()
        self.step_label.setText('Department')
        self.title_layout.addWidget(self.step_label, 0, 1, 1, 1)

        self.project_label = QtWidgets.QLabel('-')
        self.title_layout.addWidget(self.project_label, 0, 0, 1, 1)

        self.step_comboBox = QtWidgets.QComboBox()
        self.title_layout.addWidget(self.step_comboBox, 0, 2, 1, 1)
        self.title_layout.setColumnStretch(0, 2)
        self.title_layout.setColumnStretch(1, 1)
        self.title_layout.setColumnStretch(2, 1)

        self.all_layout.addLayout(self.title_layout)

        self.line = QtWidgets.QFrame()
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)

        self.all_layout.addWidget(self.line)
        self.main_layout = QtWidgets.QGridLayout()
        self.showLatest_checkBox = QtWidgets.QCheckBox('Show latest version')


        self.main_layout.addWidget(self.showLatest_checkBox, 2, 1, 1, 1)
        self.play_pushButton = QtWidgets.QPushButton('Play Movie')
        self.play_pushButton.setMinimumSize(QtCore.QSize(0, 60))
        self.main_layout.addWidget(self.play_pushButton, 3, 1, 1, 1)

        self.file_listWidget = file_widget.FileListWidget()
        self.file_listWidget.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.main_layout.addWidget(self.file_listWidget, 1, 1, 1, 1)

        self.fileListLayout = QtWidgets.QHBoxLayout()

        self.fileLabel = QtWidgets.QLabel('File List')
        self.allProcess_checkBox = QtWidgets.QCheckBox('All process')

        self.fileListLayout.addWidget(self.fileLabel)
        self.fileListLayout.addWidget(self.allProcess_checkBox)

        self.fileListLayout.setStretch(0, 1)
        self.fileListLayout.setStretch(1, 1)
        self.main_layout.addLayout(self.fileListLayout, 0, 1, 1, 1)

        self.sequenceOptionLayout = QtWidgets.QHBoxLayout()
        self.selectAll_checkBox = QtWidgets.QCheckBox('Select All')
        self.sequenceOptionLayout.addWidget(self.selectAll_checkBox)

        self.showAll_checkBox = QtWidgets.QCheckBox('Show All')
        self.sequenceOptionLayout.addWidget(self.showAll_checkBox)

        self.main_layout.addLayout(self.sequenceOptionLayout, 2, 0, 1, 1)

        self.shot_listWidget = shot_list_widget.ShotListWidget()
        self.shot_listWidget.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        self.playblast_part_checkBox = QtWidgets.QCheckBox('Playblast Part')
        self.main_layout.addWidget(self.shot_listWidget, 1, 0, 1, 1)
        self.play_part_layout = QtWidgets.QVBoxLayout()
        self.playblast_part_list_wid = QtWidgets.QListWidget()
        self.play_pushButton.setMinimumSize(QtCore.QSize(0, 60))
        # self.playblast_part_list_wid.setMaximumSize(QtCore.QSize(240, 80))
        self.play_part_layout.addWidget(self.playblast_part_checkBox)
        self.play_part_layout.addWidget(self.playblast_part_list_wid)
        self.play_part_select_all_checkBox = QtWidgets.QCheckBox ('Select All')
        # self.play_part_layout.addWidget(self.playblast_part_pushButton)
        self.playblast_part_pushButton = QtWidgets.QPushButton('Playblast Part')
        self.playblast_part_pushButton.setMinimumSize(QtCore.QSize(0, 60))
        
        self.main_layout.addLayout(self.play_part_layout, 1, 3, 1, 1)
        self.main_layout.addWidget(self.play_part_select_all_checkBox, 2, 3, 1, 1)
        self.main_layout.addWidget(self.playblast_part_pushButton, 3, 3, 1, 1)


        self.seqLayout = QtWidgets.QHBoxLayout()
        self.seqLabel = QtWidgets.QLabel('Sequencer')
        self.sortedOrder_checkBox = QtWidgets.QCheckBox('Chronological Order')
        self.seqLayout.addWidget(self.seqLabel)
        self.seqLayout.addWidget(self.sortedOrder_checkBox)
        self.main_layout.addLayout(self.seqLayout, 0, 0, 1, 1)

        self.playblast_layout1 = QtWidgets.QVBoxLayout()
        self.playblast_pushButton = QtWidgets.QPushButton('Playblast')
        self.playblast_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.playblastQueqe_pushButton = QtWidgets.QPushButton('Playblast Queue')
        self.playblastQueqe_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.playblast_layout1.addWidget(self.playblast_pushButton)
        self.playblast_layout1.addWidget(self.playblastQueqe_pushButton)
        self.main_layout.addLayout(self.playblast_layout1, 3, 0, 1, 1)
        self.all_layout.addLayout(self.main_layout)

        self.playblastOption_layout = QtWidgets.QHBoxLayout()

        self.option_layout = QtWidgets.QGridLayout()
        self.optionCheckBox_layout = QtWidgets.QHBoxLayout()
        self.daily_checkBox = QtWidgets.QCheckBox('daily')
        self.borderCrop_checkBox = QtWidgets.QCheckBox('Border Crop')
        self.publish_checkBox = QtWidgets.QCheckBox('publish')
        self.optionCheckBox_layout.addWidget(self.daily_checkBox)
        self.optionCheckBox_layout.addWidget(self.borderCrop_checkBox)
        self.optionCheckBox_layout.addWidget(self.publish_checkBox)
        self.option_layout.addLayout(self.optionCheckBox_layout, 0, 1, 1, 1, 1)

        self.showOption_checkBox = QtWidgets.QCheckBox('Options')
        self.playblastOption_layout.addWidget(self.showOption_checkBox)
        self.option_layout.addLayout(self.playblastOption_layout, 0, 0, 1, 1)

        self.hud_checkBox = QtWidgets.QCheckBox('Hud')
        self.playblastOption_layout.addWidget(self.hud_checkBox)

        self.autoPlay_checkBox = QtWidgets.QCheckBox('Play')
        self.playblastOption_layout.addWidget(self.autoPlay_checkBox)

        self.playStepKey_checkBox = QtWidgets.QCheckBox('Stepped Key')
        self.playblastOption_layout.addWidget(self.playStepKey_checkBox)

        self.autoPublish_checkBox = QtWidgets.QCheckBox('Auto Pub')
        self.playblastOption_layout.addWidget(self.autoPublish_checkBox)

        self.daily_pushButton = QtWidgets.QPushButton('Daily')
        self.daily_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.option_layout.addWidget(self.daily_pushButton, 2, 1, 1, 1)

        self.daily_frame = QtWidgets.QFrame()
        self.daily_frame.setFrameShape(QtWidgets.QFrame.Box)
        self.daily_frame.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.gridLayout_12 = QtWidgets.QGridLayout(self.daily_frame)

        self.task_label = QtWidgets.QLabel('Task')
        self.gridLayout_12.addWidget(self.task_label, 0, 0, 1, 1)

        self.task_comboBox = QtWidgets.QComboBox(self.daily_frame)
        self.gridLayout_12.addWidget(self.task_comboBox, 0, 1, 1, 1)
        self.playlist_label = QtWidgets.QLabel('Playlist')
        self.gridLayout_12.addWidget(self.playlist_label, 1, 0, 1, 1)

        self.playlist_comboBox = QtWidgets.QComboBox(self.daily_frame)
        self.gridLayout_12.addWidget(self.playlist_comboBox, 1, 1, 1, 1)
        self.option_layout.addWidget(self.daily_frame, 1, 1, 1, 1)
        self.playblast_frame = QtWidgets.QFrame()
        self.playblast_frame.setFrameShape(QtWidgets.QFrame.Box)
        self.playblast_frame.setFrameShadow(QtWidgets.QFrame.Sunken)

        self.gridLayout_10 = QtWidgets.QGridLayout(self.playblast_frame)
        self.defaultView_checkBox = QtWidgets.QCheckBox('Play as it is')
        self.gridLayout_10.addWidget(self.defaultView_checkBox, 0, 0, 1, 1)

        self.light_checkBox = QtWidgets.QCheckBox('Show Light')
        self.gridLayout_10.addWidget(self.light_checkBox, 2, 0, 1, 1)

        self.texture_checkBox = QtWidgets.QCheckBox('Show Texture')
        self.gridLayout_10.addWidget(self.texture_checkBox, 2, 1, 1, 1)

        self.hideCtrl_checkBox = QtWidgets.QCheckBox('Hide Ctrl')
        self.gridLayout_10.addWidget(self.hideCtrl_checkBox, 0, 1, 1, 1)
        self.option_layout.addWidget(self.playblast_frame, 1, 0, 1, 1)

        self.playblast_layout2 = QtWidgets.QVBoxLayout()
        
        # self.playblastQueqe_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        # self.playblast_layout2.addWidget(self.playblastQueqe_pushButton)
        self.option_layout.addLayout(self.playblast_layout2, 0, 0, 1, 1)


       
        # self.option_layout.addLayout(self.gridLayout_13, 3, 0, 1, 1)

        self.all_layout.addLayout(self.option_layout)



        self.line2 = QtWidgets.QFrame()
        self.line2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.all_layout.addWidget(self.line2)

        self.status = QtWidgets.QLabel()
        self.all_layout.addWidget(self.status)

        self.setLayout(self.all_layout)

