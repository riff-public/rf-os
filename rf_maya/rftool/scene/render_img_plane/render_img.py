import os
import sys
from rf_utils.context import context_info
reload(context_info.project_info)
reload(context_info)
from rf_utils import  file_utils
from rf_maya.rftool.scene.render_img_plane import setUp_file_imgSeq
reload(setUp_file_imgSeq)
moduleDir = os.path.dirname(sys.modules[__name__].__file__)


def render_imageseq(scene_entity):
                    #sceneFile, dst, cam, size=[1980, 816], start=1001, end=1020, targetGrp=['Char_Grp', 'Prop_Grp']
    ## sceneFile = '"P:/SevenChickMovie/scene/work/dev/scm_dev_q9990_s0990/anim/main/maya/scm_dev_q9990_s0990_anim_main.v003.pipe.ma"'
    publish_app_path = scene_entity.path.scheme(key='publishHeroWorkspacePath').abs_path()
    hero_file = file_utils.get_lastest_file(publish_app_path, 'ma')
    sceneFile = os.path.join(publish_app_path, hero_file)
    output_img_path = scene_entity.path.scheme(key='outputHeroImgPath').abs_path()

    scene_entity.context.update(publishVersion='hero', res='md')
    img_name = scene_entity.output_name(outputKey='imgSeq')
    dst = os.path.join(output_img_path, img_name)
    shotName = scene_entity.name_code
    data_path = os.path.join(moduleDir, "config_grp_output.json")
    target_grp_dict = file_utils.ymlLoader(data_path)
    targetGrp = target_grp_dict[scene_entity.step]
    
    print dst
    send_to_farm_path, shot_data = setUp_file_imgSeq.main(source_File =sceneFile, dest_File= dst, shotName = shotName ,target_grp= targetGrp)
    start, end = [shot_data['start'], shot_data['end']]
    cam = shot_data['camera']
    imgPlane_to_queue(send_to_farm_path, dst, scene_entity, img_name, start, end, cam, shotName, task_name='unknown', sg_upload=False)
    print send_to_farm_path
    # file_utils.remove_file(send_to_farm_path)

def imgPlane_to_queue(scene_file, dst, scene, filename, startFrame, endFrame, camera, shotName, task_name='unknown', sg_upload=False):
    # path = context_info.RootPath(str(scene.path.path())).abs_path()
    from rf_utils.deadline import imgPlane_queue
    reload(imgPlane_queue)
    # playblast_lib.add_preview_light_playblast()
    # fcl = mc.getAttr('%s.focalLength' % cur_cam)
    # dst
    # workspaceOutput = scene.path.work_output().abs_path()
    # scene.context.update(publishVersion=scene.version)
    # audioMov = scene.publish_name(ext='.wav')
    # filename = scene.publish_name(ext='.png').replace('.png', ".####.png")
    # dst = '%s' % workspaceOutput
    # dst = workspaceOutput
    # call queue
    shot_entity = context_info.ContextPathInfo(path=filename)
    sended_job = imgPlane_queue.render_img(
        scene_file,
        int(startFrame),
        int(endFrame),
        camera,
        dst,
        shotName,
        filename,
        #scene_file, start, end, camera, dst, shot_name, output_filename
    )
    # playblast_lib.add_preview_light_playblast()
    # mc.file(s=True)

    return sended_job

def render_img_seq():
    scene = context_info.ContextPathInfo()
    output_img_path = scene.path.output_hero_img().abs_path()
    ####### set name in render out put

    ######
    file_name = scene.publish_name(ext='.png', hero =True)
    new_publ_name = file_name.replace('.png', '')
    ######## set image project_path
    mc.setAttr("defaultRenderGlobals.imageFilePrefix", new_publ_name ,type='string')
    ##########
    mc.workspace(fr=['images','imgSeq'])
    mc.workspace(output_img_path, openWorkspace=True)




def render_setting_img_seq(cam_shape):
    mc.setAttr("defaultRenderGlobals.imageFormat", 10)
    mc.setAttr("%s.depth"%cam_shape, 1)
    mc.setAttr("defaultRenderGlobals.currentRenderer", "mayaSoftware", type="string")

