import sys, os
import json

import maya.cmds as mc
import maya.mel as mel
import pymel.core as pm

import argparse
from collections import OrderedDict
# import logging
# logger = logging.getLogger(__name__)
# logger.addHandler(logging.NullHandler())

# from rf_utils.context import context_info
from rf_maya.lib import sequencer_lib
from rf_utils import cask
from rf_utils.context import context_info
from rf_utils import register_shot
from rf_utils import register_entity
from rf_utils import file_utils

import getpass
import logging
import rf_config as config
from rf_utils import log_utils

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name('test_img_seq', user)
logger = log_utils.init_logger(logFile)
logger.setLevel(logging.DEBUG)

def setup_parser(title):
    # create the parser
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-s', dest='source_File', type=str, help='The path to the source scene to be cached', required=True)
    parser.add_argument('-s', dest='dest_File', type=str, help='The path to the source scene to be cached', required=True)
    parser.add_argument('-s', dest='target_grp', type=str, help='target_grp to be Render', default=[], required=True)
    parser.add_argument('-s', dest='shotName', type=str, help='target_grp to be Render', default=[], required=True)
    parser.add_argument('-s', dest='size', type=str, help='size that resolution scene', default=[])
    parser.add_argument('-l', dest='log', type=str, help='The path to temp log file for result', default='')
    # parser.add_argument('-n', dest='entity_step', nargs='+', help='entity_step tha', default='')
    return parser

    #size=[1980, 816], start=1001, end=1020, targetGrp

def set_padding_pattern_maya(): ## [name.#.ext]
    pm.setAttr("defaultRenderGlobals.outFormatControl", 0)
    pm.setAttr("defaultRenderGlobals.animation", 1)
    pm.setAttr("defaultRenderGlobals.putFrameBeforeExt", 1)
    pm.setAttr("defaultRenderGlobals.extensionPadding", 4)

def main(source_File, dest_File, shotName, target_grp=['Char_Grp', 'Prop_Grp'], size=[1980, 816], log=''):
    # needs to load the ABC plugin
    logger.info(':::: Start Setting File :::')
    result = {}

    # open File
    if os.path.exists(source_File) and os.path.splitext(source_File)[1] == '.ma':
        pm.openFile(source_File, force=True)
    else: 
        return 
    logger.info('test')
    ########## if already render_layer delete that before.######
    if pm.objExists('imgSeq'):
        pm.delete('imgSeq')
    logger.info(':::: Start :::')
    ############################################################
    ############ select Object to be in Output##################
    pm.select(clear=True)
    for item in target_grp:
        if pm.objExists(item):
            pm.select(item, add=True)
    ############################################################
    item_select = pm.ls(sl=True)
    if item_select == []:
        logger.info(':::: Error Non-Grp Assign :::')
        return
    logger.info(':::: step1 :::')
    ############find camera###############
    cam = mc.shot(shotName, cc=True, q=True)
    startFrame = mc.getAttr('%s.startFrame' % shotName)
    endFrame = mc.getAttr('%s.endFrame' % shotName)
    #######################################
    ####### setting renderlayer #########
    render_layer = pm.createRenderLayer(n= 'imgSeq')
    render_layer.addAdjustments(['defaultRenderGlobals.imageFormat', 'defaultRenderGlobals.extensionPadding', 
                                 'defaultRenderGlobals.startFrame', 'defaultRenderGlobals.endFrame', 
                                 'defaultResolution.width', 'defaultResolution.height',
                                 'defaultRenderGlobals.currentRenderer'])
    pm.setAttr("defaultRenderGlobals.currentRenderer", "mayaSoftware", type="string")
    set_padding_pattern_maya()
    pm.setAttr("defaultRenderGlobals.imageFormat", 7)
    pm.setAttr("defaultRenderGlobals.startFrame", startFrame)
    pm.setAttr("defaultRenderGlobals.endFrame", startFrame)
    pm.setAttr("defaultResolution.width", size[0])
    pm.setAttr("defaultResolution.height", size[1])
    #######################################
    logger.info(':::: setting renderlayer :::')
    ########### Check Camera #############
    if pm.objExists(cam): ####(cam = "s0990_cam:camshotShape")
        ######### Check depth
        render_layer.addAdjustments(['{cam}.mask'.format(cam=cam), '{cam}.depth'.format(cam=cam)])
        pm.setAttr('{cam}.mask'.format(cam=cam), 1) 
        pm.setAttr('{cam}.depth'.format(cam=cam), 1)
    else:
        logger.info(':::: Error Camera Assign :::')
        return
    ######################################
    ########### set workspace and name ########
    dir_path = os.path.dirname(dest_File)
    image_dir_path = os.path.dirname(dir_path)
    image_dir_name = os.path.basename(dir_path)
    mc.workspace(image_dir_path, openWorkspace=True)
    mc.workspace(fr=['images', image_dir_name])
    dest_ext = os.path.splitext(dest_File)[1] 
    if dest_ext:
        new_publ_name = os.path.basename(dest_File).replace(dest_ext, '')
    else:
        new_publ_name = os.path.basename(dest_File)
    mc.setAttr("defaultRenderGlobals.imageFilePrefix", new_publ_name ,type='string')
    #######################################
    logger.info(':::: Setting File Complete :::')
    src_ext = dest_ext = os.path.splitext(source_File)[1] 
    output_path = source_File.replace(src_ext, '_temp{ext}'.format(ext=src_ext))
    logger.info('::::{path}:::::::'.format(path=output_path))
    pm.saveAs(output_path)
    logger.info('::::{path}:::::::'.format(path=output_path))
    shot_data = dict()
    shot_data['camera'] = cam
    shot_data['start'] = startFrame
    shot_data['end'] = endFrame
    # logs
    return [output_path, shot_data]
        
def writeLog(log, data):
    if log:
        with open(log, 'w') as f:
            json.dump(data, f, sort_keys=True, indent=4, separators=(',', ':'))


if __name__ == "__main__":
    print ':::::: Setup file ImgSeq ::::::'
    logger.info(':::::: Setup file ImgSeq ::::::')

    parser = setup_parser('Setup file ImgSeq')
    params = parser.parse_args()
    main(source_File=params.source_File, 
        dest_File=params.dest_File,
        shotName= params.shotName,
        target_grp= params.target_grp,
        size= params.size, 
        log = params.log)

'''
"C:/Program Files/Autodesk/Maya2017/bin/mayapy.exe" "O:/pipeline/core/rf_maya/rftool/utils/export_yeti_cache.py" -s "P:/projectName/scene/work/ep01/pr_ep01_q0050_s0110/anim/main/maya/pr_ep01_q0050_s0110_anim_main.v001.Nu.ma" -n "canis_001_Tech"
'''
'''
"setUp_file_imgSeq.main(source_File ="D:/test/imgSeq/source/scm_dev_q9990_s0990_anim_main.v003.pipe.ma", dest_File= "D:/test/imgSeq/dest/scm_dev_q9990_s0990_anim_main",cam ='s0990_cam:camshotShape',target_grp=['Char_Grp'])
'''