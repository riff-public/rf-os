import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc
from rf_maya.lib import playblast_lib
from rf_utils.context import context_info
from rf_utils import publish_info
from rf_utils import project_info
from rf_utils import user_info
from rf_utils.sg import sg_process
from rf_app.publish.asset import sg_hook
from rf_utils import file_utils
from rf_utils import daily
from rf_utils import admin
from rf_utils.pipeline import multiple_clip_view as mcv
# reload(mcv)
# reload(playblast_lib)
# reload(playblast_lib.playblast_converter)
# reload(context_info)
# reload(sg_hook)

def playblast_wip(scene, shot, showHud=True, light=False, subversion=False, stepKey=[], quality=70, remove_images=True, subclips=[], subclip_size=0.15):
    user = user_info.User()
    workspaceOutput = scene.path.work_output().abs_path()
    # workspaceOutput = 'D:/__playground/test_pnp/'
    scene.context.update(publishVersion=scene.version)
    filename = scene.publish_name(ext='.mov')
    dst = '{}/{}'.format(workspaceOutput, filename)

    if showHud == True:
        playblast_lib.playblast_sequencer_hud(dst, shot, 
            hudProject=scene.project, 
            hudUser=user.name(), 
            hudStep=scene.step, 
            hudFilename=scene.filename, 
            hudShot=shot,
            showHud=showHud, 
            light=light, 
            stepKey=stepKey, 
            quality=quality, 
            remove_images=remove_images)
    else:
        finalW, finalH = scene.projectInfo.render.final_outputH()
        playblast_lib.playblast_sequencer(dst, shotNames=[shot], size=(finalW, finalH), format='qt', codec='H.264', combine=True, stepKey=stepKey, quality=quality)

    if subclips:
        projectInfo = project_info.ProjectInfo(project=scene.project)
        fps = projectInfo.render.fps()
        input_paths = [dst] + [p[1] for p in subclips]
        texts = [t[0] for t in subclips]
        main_clip_ratio = 1.0 - subclip_size
        dst_fn, dst_ext = os.path.splitext(dst)
        subclip_dst = '{}.subclip{}'.format(dst_fn, dst_ext)  # save output with subclip in a different name
        dst = mcv.main(input_paths, subclip_dst, fps=fps, main_clip_ratio=main_clip_ratio, texts=texts, background_color=(0.0, 0.0, 0.0), sub_clip_spacing='top')
    return dst

def viewport_active():
    panel_cur =  mc.getPanel(wf=True)
    logger.info('Active UI: {active_ui}'.format(active_ui=panel_cur))
    if mc.objectTypeUI (mc.getPanel(wf=True)) == 'modelEditor':
        return True
    else:
        return False
    
def playblast_to_queue(scene, startFrame, endFrame, camera, shotName, task_name='unknown', sg_upload=True, framerate='24', remove_images=True):
    # path = context_info.RootPath(str(scene.path.path())).abs_path()
    from rf_utils.deadline import playblast_queue
    # reload(playblast_queue)

    cam_tr = mc.listRelatives(camera, parent=True, pa=True)
    preview_light = playblast_lib.add_preview_light_playblast(cam_tr)
    path = mc.file(s=True)
    fn, ext = os.path.splitext(os.path.basename(path))
    cur_cam = mc.shot(shotName, cc=True, q=True)
    fcl = mc.getAttr('{}.focalLength'.format(cur_cam))

    # asset output folder
    workspaceOutput = scene.path.work_output().abs_path()

    # temp path for audio
    workspaceTemp = scene.path.scheme('tempProcessPath').abs_path()
    if not os.path.exists(workspaceTemp):
        os.makedirs(workspaceTemp)

    scene.context.update(publishVersion=scene.version)
    audio_path = '{}/{}'.format(workspaceTemp, scene.publish_name(ext='.wav'))
    filename = scene.publish_name(ext='.png').replace('.png', ".####.png")

    # make image folder
    dst = '{}/{}'.format(workspaceOutput, '_'.join(fn.split('.')))
    if not os.path.exists(dst):
        os.makedirs(dst)

    # call queue
    set_resolution(scene.project)
    sended_job = playblast_queue.playblast(
        path,
        int(startFrame),
        int(endFrame),
        camera,
        dst,
        shotName,
        filename,
        audio_path,
        scene,
        focalLength=fcl,
        task_name=task_name,
        sg_upload=sg_upload,
        framerate=framerate,
        remove_images=remove_images
    )
    playblast_lib.remove_preview_light(preview_light)
    mc.file(s=True)

    return sended_job, dst

def playblast_daily(scene, shot, taskName, inputFile=None, showHud=True, playlists=[], light=False, description=''):
    dailyDst = get_daily_version(scene)
    user = user_info.User()
    if not description:
        description = 'daily'
    status = 'rev'
    if scene.context.entityType == 'asset':
        taskEntity = get_task(scene, taskName, entityType = 'asset')

    elif scene.context.entityType == 'scene':
        taskEntity = get_task(scene, taskName, entityType = 'scene')

    if taskEntity:
        if inputFile:
            dailyDst = file_utils.copy(inputFile, dailyDst)
        if not inputFile:
            dailyDst = playblast_lib.playblast_sequencer_hud(dailyDst, shot, hudProject=scene.project, hudUser=user.name(), hudStep=scene.step, hudFilename=scene.filename, hudShot=shot, showHud=showHud, light=light)

        versionResult = sg_hook.publish_daily(scene, taskEntity, status, [dailyDst], user.sg_user(), description, playlistEntities=playlists)
        dailyResult = daily.submit(scene, [dailyDst])
    return dailyDst

def submit_playblast_publish(scene, taskName, inputFile, status='apr', description=''):
    publishDst, heroDst = get_publish_outputs(scene, inputFile)
    user = user_info.User()
    if not description:
        description = '{} publish'.format(scene.step)
    if scene.context.entityType == 'asset':
        taskEntity = get_task(scene, taskName, entityType = 'asset')

    elif scene.context.entityType == 'scene':
        taskEntity = get_task(scene, taskName, entityType = 'scene')

    # copy to hero
    # file_utils.copy(inputFile, heroDst) if not inputFile == heroDst else None
    # file_utils.copy(inputFile, publishDst) if not inputFile == publishDst else None
    heroDir = os.path.dirname(heroDst)
    if not os.path.exists(heroDir):
        admin.makedirs(heroDir)
    admin.copyfile(inputFile, heroDst) if not inputFile == heroDst else None
    # file_utils.copy(inputFile, heroDst) if not inputFile == heroDst else None

    publDir = os.path.dirname(publishDst)
    if not os.path.exists(publDir):
        admin.makedirs(publDir)
    admin.copyfile(inputFile, publishDst) if not inputFile == publishDst else None
    # file_utils.copy(inputFile, publishDst) if not inputFile == publishDst else None

    versionResult = sg_hook.publish_version(scene, taskEntity, status, [publishDst], user.sg_user(), description, uploadOption='default')
    return publishDst

def get_daily_version(scene, ext='.mov'):
    scene.context.update(res='shot')
    regInfo = publish_info.RegisterShotInfo(scene)
    regVersion = regInfo.get_latest_version('workspace') or 'v001'
    scene.context.update(publishVersion=regVersion)
    filename = scene.daily_name(version='subversion', ext=ext)
    scene.context.update(publishVersion=scene.subversion)
    outputDir = scene.path.outputImg().abs_path()
    scene.context.update(puplishVersion=regVersion)
    dst = '{}/{}'.format(outputDir, filename)
    return dst

def get_publish_outputs(scene, inputFile):
    # setup context
    entity = context_info.ContextPathInfo(path=inputFile)
    scene.context.update(res='shot')
    regInfo = publish_info.RegisterShotInfo(scene)
    # regVersion = regInfo.get_latest_version('workspace')
    regVersion = entity.version
    scene.context.update(publishVersion=regVersion)

    # publish name
    publishDir = scene.path.scheme(key='outputImgPath').abs_path()
    publishFile = scene.publish_name(ext='.mov')
    publishFilePath = '{}/{}'.format(publishDir, publishFile)
    # hero name
    heroDir = scene.path.scheme(key='outputHeroImgPath').abs_path()
    heroFile = scene.publish_name(hero=True, ext='.mov')
    heroFilePath = '{}/{}'.format(heroDir, heroFile)

    return publishFilePath, heroFilePath

def get_task(scene, taskName, entityType = 'scene'):
    scene.context.use_sg(sg_process.sg, project=scene.project, entityType=entityType, entityName=scene.name)
    taskEntity = sg_process.get_one_task_by_step(scene.context.sgEntity, scene.step, taskName, create=True)
    return taskEntity

def set_resolution(project):
    from rf_utils import project_info
    proj = project_info.ProjectInfo(project)
    proj.render.set_size(mode='preview')
    #mc.setAttr('defaultResolution.deviceAspectRatio', 2.353)

def get_stepkey(): 
    from rftool.utils import maya_utils
    # reload(maya_utils)
    allFrames = []

    objs = mc.ls(sl=True)
    if not objs: 
        return 

    for obj in objs: 
        frames = maya_utils.find_keyframe(obj)
        if frames: 
            allFrames += frames

    return sorted(list(set(allFrames)))

