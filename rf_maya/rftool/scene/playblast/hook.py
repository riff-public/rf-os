import os
import sys
import maya.cmds as mc
from rf_maya.lib import sequencer_lib

def list_sequencer():
    return mc.ls(type='shot')

def shot_info(shotName):
    if mc.objExists(shotName):
        return sequencer_lib.shot_info(shotName)
    return 0, 0, 0, 0

def waitCursor(state):
    mc.waitCursor(st=state)

def order_shot(shotNames, mode='time'):
    if mode == 'name':
        return sorted(shotNames)
    if mode == 'time':
        shotDict = dict()
        timeOrder = []
        missingShots = []
        for shotName in shotNames:
            if mc.objExists(shotName) and mc.objectType(shotName, isType='shot'):
                startFrame, endFrame, seqStartFrame, seqEndFrame = sequencer_lib.shot_info(shotName)
                shotDict[seqStartFrame] = shotName
            else:
                missingShots.append(shotName)

        timeOrder = [shotDict[time] for time in sorted(shotDict.keys())] + missingShots
        return timeOrder


def obj_exists(obj):
    return mc.objExists(obj)

def camera_info(shotName):
    return mc.shot(shotName, q=True, cc=True) if mc.objExists(shotName) else None

def set_camera(shotName):
    setValue = {'overscan': 1, 'filmFit': 1}
    cameraShape = camera_info(shotName)

    for attr, value in setValue.items(): 
        ctrls = mc.listConnections('%s.%s' % (cameraShape, attr), s=True, p=True)
        if ctrls: 
            mc.setAttr(ctrls[0], value)
        else: 
            mc.setAttr('%s.%s' % (cameraShape, attr), value)

    ctrls = mc.listConnections('%s.overscan' % cameraShape, s=True, p=True)
    overscan = mc.getAttr('%s.overscan' % cameraShape)
    return True if overscan == 1 else False 

def set_black_frame(camera, blackFrame):
    objName = camera.replace(camera.split(':')[-1],'camCtrl_camCtrl')
    attrName = '{}.BlackFrame'.format(objName)
    if mc.objExists(attrName):
        mc.setAttr(attrName, blackFrame)


def set_filmgate_abc(camera, state):
    # only set for alembic camera 
    path = mc.referenceQuery(camera, f=True) 
    ext = path.split('.')[-1]
    if ext == 'abc': 
        mc.setAttr('{}.displayFilmGate'.format(camera), True)
        mc.setAttr('{}.displayResolution'.format(camera), True)
        mc.setAttr('{}.displayGateMask'.format(camera), True)
        mc.setAttr('{}.displayGateMaskColor'.format(camera), 0, 0, 0, type='double3')
        mc.setAttr('{}.displayGateMaskOpacity'.format(camera), state)


def hide_others_camera(scene, shotName):
    # camgrp =  scene.projectInfo.asset.cam_grp()
    listHide = []
    cameras = mc.ls(type='camera', l=True)
    for cam in cameras:
        camlist = cam.split('|')
        for name in camlist:
            if ':' in name:
                listHide.append(name)
                break
    for cam in listHide:
        if shotName in cam:
            listHide.remove(cam)
    mc.hide(listHide)
    return listHide

def show_hidden_camera(camList):
    mc.showHidden(camList)
    return camList

def list_shots(scene):
    # list by structure
    scene = scene.copy()
    sequenceKey = scene.sequence
    shotDir = os.path.split(scene.path.name().abs_path())[0]
    allShots = list_folder(shotDir)
    entityNames = [a for a in allShots if sequenceKey in a]
    shots = []

    for shot in entityNames:
        scene.extract_name(shot, workfileKey='sgName', force=True)
        if not scene.name_code == 'all':
            shots.append(scene.name_code)

    return shots

def list_assets(scene): 
    # list by structure
    scene = scene.copy()
    return [scene.name]

def list_folder(path):
    return [d for d in os.listdir(path) if os.path.isdir(os.path.join(path,d))]
