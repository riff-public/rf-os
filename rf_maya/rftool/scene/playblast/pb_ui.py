from Qt import QtCore, QtWidgets, QtGui
from rf_utils.widget import shot_list_widget
from rf_utils.widget import file_widget
# reload(shot_list_widget)
# reload(file_widget)
from rf_utils.widget import collapse_widget
# reload(collapse_widget)
from rf_utils.widget import hashtag_widget
# reload(hashtag_widget)

class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
    	super(Ui, self).__init__(parent)

        # main layouts
        self.all_layout = QtWidgets.QVBoxLayout()
        self.main_layout = QtWidgets.QHBoxLayout()

        # --- Title layout
        self.title_layout = QtWidgets.QGridLayout()

        self.name_label = QtWidgets.QLabel('-')
        self.title_layout.addWidget(self.name_label, 1, 0, 1, 1)

        self.step_label = QtWidgets.QLabel()
        self.step_label.setText('Department')
        self.step_label.setMaximumWidth(65)
        self.title_layout.addWidget(self.step_label, 0, 1, 1, 1)

        self.project_label = QtWidgets.QLabel('-')
        self.title_layout.addWidget(self.project_label, 0, 0, 1, 1)

        self.step_comboBox = QtWidgets.QComboBox()
        self.title_layout.addWidget(self.step_comboBox, 0, 2, 1, 1)
        self.title_layout.setColumnStretch(0, 2)
        self.title_layout.setColumnStretch(1, 1)
        self.title_layout.setColumnStretch(2, 1)
        self.all_layout.addLayout(self.title_layout)

        self.line = QtWidgets.QFrame()
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.all_layout.addWidget(self.line)

        self.all_layout.addLayout(self.main_layout)

        # ==========================================================================================================================
        # ----- camera list layout
        self.seqLayout = QtWidgets.QVBoxLayout()
        self.main_layout.addLayout(self.seqLayout)

        # shot splitter
        self.shotSplitter = QtWidgets.QSplitter()
        self.shotSplitter.setOrientation(QtCore.Qt.Vertical)
        self.shotViewSplitWidget = QtWidgets.QWidget(self.shotSplitter)
        self.shotViewSplitWidget.setMinimumWidth(120)
        self.shotSetupSplitWidget = QtWidgets.QWidget(self.shotSplitter)
        self.shotSplitter.setSizes([500, 0])
        self.seqLayout.addWidget(self.shotSplitter)

        self.seqViewLayout = QtWidgets.QVBoxLayout(self.shotViewSplitWidget)
        self.seqViewLayout.setContentsMargins(0, 0, 0, 0)
        self.seqSetupLayout = QtWidgets.QVBoxLayout(self.shotSetupSplitWidget)
        self.seqSetupLayout.setContentsMargins(0, 0, 0, 0)


        # ----- file list layout
        self.fileListLayout = QtWidgets.QVBoxLayout()
        self.main_layout.addLayout(self.fileListLayout)

        # file splitter
        self.fileSplitter = QtWidgets.QSplitter()
        self.fileSplitter.setOrientation(QtCore.Qt.Vertical)
        self.fileViewSplitWidget = QtWidgets.QWidget(self.fileSplitter)
        self.fileViewSplitWidget.setMinimumWidth(120)
        self.fileSetupSplitWidget = QtWidgets.QWidget(self.fileSplitter)
        self.fileSplitter.setSizes([500, 0])
        self.fileListLayout.addWidget(self.fileSplitter)

        self.fileViewLayout = QtWidgets.QVBoxLayout(self.fileViewSplitWidget)
        self.fileViewLayout.setContentsMargins(0, 0, 0, 0)
        self.fileSetupLayout = QtWidgets.QVBoxLayout(self.fileSetupSplitWidget)
        self.fileSetupLayout.setContentsMargins(0, 0, 0, 0)
        
        # camera list
        self.shotTitle_layout = QtWidgets.QHBoxLayout()
        self.seqViewLayout.addLayout(self.shotTitle_layout)

        self.spacer5 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.shotTitle_layout.addItem(self.spacer5)

        self.seqLabel = QtWidgets.QLabel('Shots')
        font = self.seqLabel.font()
        font.setPixelSize(12)
        self.seqLabel.setFont(font)
        self.shotTitle_layout.addWidget(self.seqLabel)

        # refresh shot button
        self.refresh_shot_pushButton = QtWidgets.QPushButton()
        self.refresh_shot_pushButton.setMaximumSize(QtCore.QSize(20, 20))
        self.shotTitle_layout.addWidget(self.refresh_shot_pushButton)

        self.spacer3 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.shotTitle_layout.addItem(self.spacer3)

        self.shot_listWidget = shot_list_widget.ShotListWidget()
        self.shot_listWidget.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.seqViewLayout.addWidget(self.shot_listWidget)
        
        # sequence show options
        self.sequenceOptionLayout = QtWidgets.QHBoxLayout()
        self.sequenceOptionLayout.setContentsMargins(5, 0, 5, 0)

        self.selectAll_checkBox = QtWidgets.QCheckBox('Select All')
        self.sequenceOptionLayout.addWidget(self.selectAll_checkBox, 1, QtCore.Qt.AlignHCenter)
        self.showAll_checkBox = QtWidgets.QCheckBox('Show All')
        self.sequenceOptionLayout.addWidget(self.showAll_checkBox, 1, QtCore.Qt.AlignHCenter)
        self.sortedOrder_checkBox = QtWidgets.QCheckBox('Sort Order')
        self.sequenceOptionLayout.addWidget(self.sortedOrder_checkBox, 1, QtCore.Qt.AlignHCenter)
        self.seqViewLayout.addLayout(self.sequenceOptionLayout)

        self.playblastButtonLayout = QtWidgets.QHBoxLayout()
        self.playblastButtonLayout.setSpacing(5)
        self.playblastButtonLayout.setContentsMargins(0, 0, 0, 0)
        self.seqViewLayout.addLayout(self.playblastButtonLayout)

        # playblast button
        self.playblast_pushButton = QtWidgets.QPushButton('Playblast')
        self.playblast_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.playblastButtonLayout.addWidget(self.playblast_pushButton)

        # Farm playblast
        self.playblastQueqe_pushButton = QtWidgets.QPushButton('')
        self.playblastQueqe_pushButton.setMaximumSize(QtCore.QSize(30, 30))
        self.playblastButtonLayout.addWidget(self.playblastQueqe_pushButton)
        self.playblastButtonLayout.setStretch(0, 3)
        self.playblastButtonLayout.setStretch(1, 1)

        # render settings
        self.playblast_option_groupBox = collapse_widget.CollapsibleBox('Playblast Settings')
        self.seqSetupLayout.addWidget(self.playblast_option_groupBox)

        self.playblastOption_layout = QtWidgets.QVBoxLayout()
        self.playblastOption_layout.setContentsMargins(9, 9, 9, 9)

        self.playblastOptionGrid_layout = QtWidgets.QGridLayout()
        # self.playblastOptionGrid_layout.setContentsMargins(9, 9, 9, 9)
        self.playblastOption_layout.addLayout(self.playblastOptionGrid_layout)
        
        self.hud_checkBox = QtWidgets.QCheckBox('HUD')
        self.playblastOptionGrid_layout.addWidget(self.hud_checkBox, 0, 0)

        self.light_checkBox = QtWidgets.QCheckBox('Light')
        self.playblastOptionGrid_layout.addWidget(self.light_checkBox, 1, 0)

        self.playStepKey_checkBox = QtWidgets.QCheckBox('Stepped key')
        self.playblastOptionGrid_layout.addWidget(self.playStepKey_checkBox, 0, 2)

        self.removeImgs_checkBox = QtWidgets.QCheckBox('Clear tmp')
        self.playblastOptionGrid_layout.addWidget(self.removeImgs_checkBox, 0, 1)

        self.autoPlay_checkBox = QtWidgets.QCheckBox('Auto play')
        self.playblastOptionGrid_layout.addWidget(self.autoPlay_checkBox, 1, 1)

        self.autoPublish_checkBox = QtWidgets.QCheckBox('Auto publish')
        self.playblastOptionGrid_layout.addWidget(self.autoPublish_checkBox, 1, 2)

        # quality settings
        self.quality_layout = QtWidgets.QHBoxLayout()
        self.quality_layout.setContentsMargins(0, 0, 0, 0)
        self.quality_slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.quality_slider.setMinimum(1)
        self.quality_slider.setMaximum(100)
        self.quality_slider.setMinimumSize(QtCore.QSize(50, 18))
        self.quality_field = QtWidgets.QLineEdit()
        self.quality_field.setMinimumSize(QtCore.QSize(36, 18))
        self.quality_field.setMaximumSize(QtCore.QSize(36, 18))
        self.quality_field.setValidator(QtGui.QIntValidator())
        self.quality_field.setInputMask('999%')
        self.quality_label = QtWidgets.QLabel('Quality:')
        self.quality_layout.addWidget(self.quality_label)
        self.quality_layout.addWidget(self.quality_field)
        self.quality_layout.addWidget(self.quality_slider)
        self.playblastOption_layout.addLayout(self.quality_layout)

        # attach subclip
        self.subclip_groupbox = QtWidgets.QGroupBox('Subclips')
        self.playblastOption_layout.addWidget(self.subclip_groupbox)

        self.subclip_layout = QtWidgets.QVBoxLayout(self.subclip_groupbox)
        self.subclip_layout.setContentsMargins(5, 5, 5, 5)
        self.subclip_layout.setSpacing(7)

        # subclip widget
        self.subclip_widget = hashtag_widget.TagWidget(inLine=False)
        self.subclip_widget.label.setVisible(False)
        self.subclip_widget.allow_creation = True
        self.subclip_widget.allow_duplicate = False
        self.subclip_widget.allow_rename = False
        self.subclip_widget.use_hash = False
        self.subclip_widget.line_edit.setHidden(True)
        self.subclip_widget.allow_delete = True
        self.subclip_widget.display.setMaximumHeight(55)
        self.subclip_layout.addWidget(self.subclip_widget)
        self.subclip_widget.line_layout.setSpacing(5)
        

        # self.spacer11 = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        # self.subclip_widget.line_layout.addItem(self.spacer11)
        # subclip size settings
        self.subclip_size_layout = QtWidgets.QHBoxLayout()
        self.subclip_size_layout.setContentsMargins(0, 0, 0, 0)
        self.subclipsize_slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.subclipsize_slider.setMinimum(1)
        self.subclipsize_slider.setMaximum(100)
        self.subclipsize_slider.setMinimumSize(QtCore.QSize(50, 18))
        self.subclipsize_field = QtWidgets.QLineEdit()
        self.subclipsize_field.setMinimumSize(QtCore.QSize(36, 18))
        self.subclipsize_field.setMaximumSize(QtCore.QSize(36, 18))
        self.subclipsize_field.setValidator(QtGui.QIntValidator())
        self.subclipsize_field.setInputMask('999%')
        self.subclipsize_label = QtWidgets.QLabel('Size:')
        self.subclip_size_layout.addWidget(self.subclipsize_label)
        self.subclip_size_layout.addWidget(self.subclipsize_field)
        self.subclip_size_layout.addWidget(self.subclipsize_slider)
        self.subclip_widget.line_layout.addLayout(self.subclip_size_layout)

        self.attach_subclip_button = QtWidgets.QPushButton()
        self.attach_subclip_button.setMaximumSize(QtCore.QSize(35, 20))
        self.subclip_widget.line_layout.addWidget(self.attach_subclip_button)

        self.browse_subclip_button = QtWidgets.QPushButton()
        self.browse_subclip_button.setMaximumSize(QtCore.QSize(20, 20))
        self.subclip_widget.line_layout.addWidget(self.browse_subclip_button)

        self.clear_subclip_button = QtWidgets.QPushButton()
        self.clear_subclip_button.setMaximumSize(QtCore.QSize(20, 20))
        self.subclip_widget.line_layout.addWidget(self.clear_subclip_button)
        self.playblast_option_groupBox.setContentLayout(self.playblastOption_layout)

        self.spacer1 = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.seqSetupLayout.addItem(self.spacer1)

        # ==========================================================================================================================
        self.fileTitleLayout = QtWidgets.QHBoxLayout()
        self.fileViewLayout.addLayout(self.fileTitleLayout)

        self.spacer6 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.fileTitleLayout.addItem(self.spacer6)

        # file list label
        self.fileLabel = QtWidgets.QLabel('Media')
        font = self.fileLabel.font()
        font.setPixelSize(12)
        self.fileLabel.setFont(font)
        self.fileTitleLayout.addWidget(self.fileLabel)

        # refresh file button
        self.refresh_file_pushButton = QtWidgets.QPushButton()
        self.refresh_file_pushButton.setMaximumSize(QtCore.QSize(20, 20))
        self.fileTitleLayout.addWidget(self.refresh_file_pushButton)

        self.spacer4 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.fileTitleLayout.addItem(self.spacer4)

        # file list widget
        self.file_listWidget = file_widget.FileListWidget()
        self.file_listWidget.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.fileViewLayout.addWidget(self.file_listWidget)

        # file display options
        self.playOption_layout = QtWidgets.QHBoxLayout()
        self.playOption_layout.setContentsMargins(5, 0, 5, 0)
        self.fileViewLayout.addLayout(self.playOption_layout)

        self.allProcess_checkBox = QtWidgets.QCheckBox('All Process')
        self.playOption_layout.addWidget(self.allProcess_checkBox, 1, QtCore.Qt.AlignHCenter)

        self.showLatest_checkBox = QtWidgets.QCheckBox('Latest Only')
        self.playOption_layout.addWidget(self.showLatest_checkBox, 1, QtCore.Qt.AlignHCenter)
        
        # play app options
        self.playApp_layout = QtWidgets.QHBoxLayout()
        self.fileViewLayout.addLayout(self.playApp_layout)

        self.playApp_label = QtWidgets.QLabel('Application: ')
        self.playApp_layout.addWidget(self.playApp_label, 1, QtCore.Qt.AlignHCenter)

        self.rv_radiobutton = QtWidgets.QRadioButton("RV")
        self.playApp_layout.addWidget(self.rv_radiobutton, 3, QtCore.Qt.AlignLeft)

        self.keyframepro_radiobutton = QtWidgets.QRadioButton("Keyframe Pro")
        self.playApp_layout.addWidget(self.keyframepro_radiobutton, 3, QtCore.Qt.AlignLeft)
        
        # play button
        self.play_pushButton = QtWidgets.QPushButton('Play Movie')
        self.play_pushButton.setMinimumSize(QtCore.QSize(0, 30))
        self.fileViewLayout.addWidget(self.play_pushButton)

        self.publish_option_groupBox = collapse_widget.CollapsibleBox('Publish Settings')
        self.fileSetupLayout.addWidget(self.publish_option_groupBox)
        
        self.publishOption_layout = QtWidgets.QVBoxLayout()
        self.publishOption_layout.setContentsMargins(9, 9, 9, 9)
        self.publishOption_layout.setSpacing(5)

        # checkbox to show option
        self.checkbox_option_layout = QtWidgets.QHBoxLayout()
        self.publishOption_layout.addLayout(self.checkbox_option_layout)

        # self.daily_checkBox = QtWidgets.QCheckBox('Daily')
        # self.checkbox_option_layout.addWidget(self.daily_checkBox, 1, QtCore.Qt.AlignHCenter)

        # self.publish_checkBox = QtWidgets.QCheckBox('Publish')
        # self.checkbox_option_layout.addWidget(self.publish_checkBox, 1, QtCore.Qt.AlignHCenter)

        self.daily_radioButton = QtWidgets.QRadioButton("Daily")
        self.checkbox_option_layout.addWidget(self.daily_radioButton, 1, QtCore.Qt.AlignHCenter)
        self.publish_radioButton = QtWidgets.QRadioButton("Publish")
        self.checkbox_option_layout.addWidget(self.publish_radioButton, 1, QtCore.Qt.AlignHCenter)
        # # dummy line
        # self.dummyLine = QtWidgets.QLabel()
        # self.dummyLine.setFrameShape(QtWidgets.QFrame.HLine)
        # self.dummyLine.setFrameShadow(QtWidgets.QFrame.Sunken)
        # self.publishOption_layout.addWidget(self.dummyLine)

        self.dummyLine = QtWidgets.QLabel()
        self.dummyLine.setFixedHeight(5)
        self.dummyLine.setFrameShape(QtWidgets.QFrame.HLine)
        self.dummyLine.setFrameShadow(QtWidgets.QFrame.Plain)
        self.publishOption_layout.addWidget(self.dummyLine)

        # publish option
        self.publish_option_layout = QtWidgets.QGridLayout()
        self.publishOption_layout.addLayout(self.publish_option_layout)

        self.daily_pushButton = QtWidgets.QPushButton('Send')
        self.daily_pushButton.setMinimumSize(QtCore.QSize(0, 25))
        self.publish_option_layout.addWidget(self.daily_pushButton, 2, 1, 1, 1)

        # self.daily_frame = QtWidgets.QFrame()
        # self.daily_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        # self.daily_frame.setFrameShadow(QtWidgets.QFrame.Plain)
        # self.publish_option_layout.addWidget(self.daily_frame, 1, 1, 1, 1)

        self.gridLayout_12 = QtWidgets.QGridLayout()
        self.publish_option_layout.addLayout(self.gridLayout_12, 1, 1, 1, 1)

        self.task_label = QtWidgets.QLabel('Task')
        self.gridLayout_12.addWidget(self.task_label, 0, 0, 1, 1, QtCore.Qt.AlignRight)

        self.task_comboBox = QtWidgets.QComboBox()
        self.task_comboBox.setMinimumWidth(120)
        self.gridLayout_12.addWidget(self.task_comboBox, 0, 1, 1, 2, QtCore.Qt.AlignLeft)

        self.playlist_label = QtWidgets.QLabel('Playlist')
        self.gridLayout_12.addWidget(self.playlist_label, 1, 0, 1, 1, QtCore.Qt.AlignRight)

        self.playlist_comboBox = QtWidgets.QComboBox()
        self.playlist_comboBox.setMinimumWidth(120)
        self.gridLayout_12.addWidget(self.playlist_comboBox, 1, 1, 1, 2, QtCore.Qt.AlignLeft)

        self.desc_label = QtWidgets.QLabel('Description')
        self.gridLayout_12.addWidget(self.desc_label, 2, 0, 1, 1, QtCore.Qt.AlignRight)

        self.desc_lineEdit = QtWidgets.QLineEdit()
        self.desc_lineEdit.setMinimumWidth(150)
        self.gridLayout_12.addWidget(self.desc_lineEdit, 2, 1, 1, 2, QtCore.Qt.AlignLeft)

        self.spacer2 = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.fileSetupLayout.addItem(self.spacer2)

        self.publish_option_groupBox.setContentLayout(self.publishOption_layout)

        # bottom line
        self.line2 = QtWidgets.QFrame()
        self.line2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.all_layout.addWidget(self.line2)

        # status line
        self.status = QtWidgets.QLabel()
        self.all_layout.addWidget(self.status)
        self.setLayout(self.all_layout)