#!/usr/bin/env python
# -- coding: utf-8 --

# v.0.0.1 - polytag switcher
# v.0.0.2 - cleanup, change burnin to CV
# v.1.0.0 - UI upgraded, add sub-clip options
# v.1.0.1 - Fix bug summit daily not working - self.check_apr_task()
# v.1.0.2 - Fix bug cannot attach custom still image, add clear subclip button
# v.1.0.3 - Fix bug in auto publish checkboxes
# v.1.0.4 - Fix bug Maya crash if both daily&publish checkboxes are checked
# v.1.1.0 - Add option to adjust subclip size
# v.1.2.0 - Save output with subclips in a different name
# v.1.2.1 - Remove test exception

_title = 'Playblast'
_version = 'v.1.2.1'
_des = ''
uiName = 'PlayblastUI'

#Import python modules
import sys
import os
import getpass
import subprocess
from collections import OrderedDict
from functools import partial
moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# framework modules
import rf_config as config
# reload(config)
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.context import context_info
from rf_utils import file_utils
from rf_utils.widget import shot_list_widget
from rf_utils.widget import dialog
from rf_utils import publish_info
import thread
#eload(thread)
from rf_utils.sg import task_check
# reload(thread)
# reload(file_utils)
# reload(shot_list_widget)
from rf_utils import icon
import utils
# reload(utils)
import pb_ui
# reload(pb_ui)
import hook
reload(hook)
from rf_qc.maya_lib.scene import check_shot_lockdown
# reload(check_shot_lockdown)

user = '{}-{}'.format(config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

icons_dir = '{script_root}/core/icons'.format(script_root=os.environ['RFSCRIPT'])
os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

class Config: 
    autoPublishStep = ['anim']
    autoTaskMap = {'anim': 'anim'}
    approvedStatus = 'apr'
    wip = 'wip'
    ui_size = (580, 630)

class Color:
    red = [255, 100, 100]
    grey = [100, 100, 100]
    green = [100, 255, 100]
    lightGrey = [200, 200, 200]

class PlayblastCore(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(PlayblastCore, self).__init__(parent)
        self._task_set = False

        # ui read
        self.ui = pb_ui.Ui()
        self.setCentralWidget(self.ui)
        self.setWindowTitle('{} {} {}'.format(_title, _version, _des))
        self.setObjectName(uiName)
        self.setMinimumSize(QtCore.QSize(Config.ui_size[0], Config.ui_size[1]))
        self.resize(Config.ui_size[0], Config.ui_size[1])

        self.subclip_menu = QtWidgets.QMenu(self)

        # --- add icons
        # play movie button
        play_iconWidget = QtGui.QIcon()
        play_iconWidget.addPixmap(QtGui.QPixmap('{0}/play_icon_white.png'.format(icons_dir)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.play_pushButton.setIcon(play_iconWidget)

        # playblast button
        pb_iconWidget = QtGui.QIcon()
        pb_iconWidget.addPixmap(QtGui.QPixmap('{0}/film_slate_icon1.png'.format(icons_dir)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.playblast_pushButton.setIcon(pb_iconWidget)

        # farm playblast button
        farmPb_iconWidget = QtGui.QIcon()
        farmPb_iconWidget.addPixmap(QtGui.QPixmap('{0}/submit_icon2.png'.format(icons_dir)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.playblastQueqe_pushButton.setIcon(farmPb_iconWidget)

        # refresh buttons
        refresh_iconWidget = QtGui.QIcon()
        refresh_iconWidget.addPixmap(QtGui.QPixmap('{0}/refresh_bw.png'.format(icons_dir)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.refresh_shot_pushButton.setIcon(refresh_iconWidget)
        self.ui.refresh_file_pushButton.setIcon(refresh_iconWidget)

        # attach subclip buttons
        attach_subclip_iconWidget = QtGui.QIcon()
        attach_subclip_iconWidget.addPixmap(QtGui.QPixmap('{0}/paperclip_icon.png'.format(icons_dir)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.attach_subclip_button.setIcon(attach_subclip_iconWidget)

        # browse subclip buttons
        browse_subclip_iconWidget = QtGui.QIcon()
        browse_subclip_iconWidget.addPixmap(QtGui.QPixmap('{0}/dot_icon.png'.format(icons_dir)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.browse_subclip_button.setIcon(browse_subclip_iconWidget)

        # clear subclip buttons
        clear_subclip_iconWidget = QtGui.QIcon()
        clear_subclip_iconWidget.addPixmap(QtGui.QPixmap('{0}/bin_icon.png'.format(icons_dir)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.clear_subclip_button.setIcon(clear_subclip_iconWidget)

        app_iconWidget = QtGui.QIcon()
        app_iconWidget.addPixmap(QtGui.QPixmap('{0}/playblast_icon.png'.format(icons_dir)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(app_iconWidget)

        # var
        self.pipeline = False

        self.init_ui()
        self.init_functions()
        self.init_signals()

    def init_ui(self):
        self.set_default()
        self.set_context()
        self.set_project()
        self.list_step()
        self.list_shot()
        self.setup_subclip_menu()

    def init_functions(self):
        self.ui.file_listWidget.open_explorer = self.open_explorer

    def init_signals(self):
        # item selected
        self.ui.shot_listWidget.itemSelectionChanged.connect(self.shot_selected)
        self.ui.file_listWidget.itemDoubleClicked.connect(self.double_clicked_play_media)

        # buttons
        self.ui.playblast_pushButton.clicked.connect(self.playblast)
        self.ui.play_pushButton.clicked.connect(self.play_media)
        self.ui.daily_pushButton.clicked.connect(self.send_to_sg)
        self.ui.refresh_shot_pushButton.clicked.connect(self.refresh_shot)
        self.ui.refresh_file_pushButton.clicked.connect(self.refresh_file)
        self.ui.playblastQueqe_pushButton.clicked.connect(partial(self.playblast, True))
        self.ui.browse_subclip_button.clicked.connect(self.browse_subclip)
        self.ui.clear_subclip_button.clicked.connect(self.clear_subclip)

        # comboBox
        self.ui.task_comboBox.currentIndexChanged.connect(self.check_daily_input)
        self.ui.file_listWidget.itemSelected.connect(self.check_daily_input)

        # checkBox
        self.ui.showLatest_checkBox.stateChanged.connect(self.list_mov_files)
        self.ui.step_comboBox.currentIndexChanged.connect(self.list_mov_files)
        self.ui.selectAll_checkBox.stateChanged.connect(self.select_all)
        self.ui.allProcess_checkBox.stateChanged.connect(self.shot_selected)
        self.ui.sortedOrder_checkBox.stateChanged.connect(self.list_shot)
        self.ui.showAll_checkBox.stateChanged.connect(self.show_all)

        # ui
        # self.ui.showOption_checkBox.stateChanged.connect(self.option_selected)
        self.ui.daily_radioButton.toggled.connect(self.daily_selected)
        self.ui.publish_radioButton.toggled.connect(self.publish_selected)
        self.ui.autoPublish_checkBox.stateChanged.connect(self.auto_publish_selected)
        self.ui.publish_option_groupBox.stateChanged.connect(self.publish_stage_changed)

        # slider
        self.ui.quality_slider.valueChanged.connect(self.quality_slider_changed)
        self.ui.quality_field.textChanged.connect(self.quality_field_changed)
        self.ui.subclipsize_slider.valueChanged.connect(self.subclipsize_slider_changed)
        self.ui.subclipsize_field.textChanged.connect(self.subclipsize_field_changed)
        
    def set_default(self):
        self.ui.autoPlay_checkBox.setChecked(True)
        # self.ui.playblast_frame.setVisible(False)
        # self.ui.daily_frame.setVisible(False)
        # self.ui.daily_pushButton.setVisible(False)
        self.ui.daily_pushButton.setEnabled(False)
        self.ui.hud_checkBox.setChecked(True)
        self.ui.daily_radioButton.setChecked(True)
        self.ui.autoPublish_checkBox.setChecked(False)

        self.ui.quality_field.setText('70')
        self.ui.quality_slider.setValue(70)
        self.ui.subclipsize_field.setText('25')
        self.ui.subclipsize_slider.setValue(25)
        self.ui.removeImgs_checkBox.setChecked(True)
        self.ui.keyframepro_radiobutton.setChecked(True)

        # HIDE PLAY APP, for now
        self.ui.playApp_label.setHidden(True)
        self.ui.keyframepro_radiobutton.setHidden(True)
        self.ui.rv_radiobutton.setHidden(True)
        self.ui.rv_radiobutton.setEnabled(False)

        # tooltips
        self.ui.refresh_shot_pushButton.setToolTip('Find all shots in the scene')
        self.ui.refresh_file_pushButton.setToolTip('Find all media available for the selected shots')
        self.ui.playblast_pushButton.setToolTip('Start a playblast')
        self.ui.playblastQueqe_pushButton.setToolTip('Send playblast job to farm')
        self.ui.play_pushButton.setToolTip('Play selected media in a player')
        self.ui.attach_subclip_button.setToolTip('Add latest published media from a department')
        self.ui.browse_subclip_button.setToolTip('Browse for custom media to attach as subclip')
        self.ui.clear_subclip_button.setToolTip('Clear all subclips')

        self.ui.selectAll_checkBox.setToolTip('Select all shots in shot list')
        self.ui.showAll_checkBox.setToolTip('Show all shot within this scene')
        self.ui.sortedOrder_checkBox.setToolTip('If checked, sort shot by time, else by name')
        self.ui.allProcess_checkBox.setToolTip('Display media in all process not just the main process')
        self.ui.showLatest_checkBox.setToolTip('Show only the latest media from selected shot')

        # set custom message for lock check 
        check_shot_lockdown.Message.animMesg = 'Shot นี้ถูกส่งไปเรนเดอร์แล้ว ติดต่อ พี่มิ้ม และ project producer เพื่อแจ้งแผนก light เพื่อขอแก้ก่อนจะส่ง Playblast ใหม่ได้ \n Shot already sent to render please contact animation producer and project producer to fix.'

    def set_context(self):
        self.scene = context_info.ContextPathInfo()

        if self.scene.project:
            self.shotName = self.scene.name_code
            sequenceKey = self.scene.projectInfo.scene.sequence_key
            self.shotMode = True if not self.shotName == sequenceKey else False
            self.pipeline = True
            self.ui.project_label.setText(self.scene.project)
            self.ui.name_label.setText(self.scene.name)
            if self.scene.process in ['qc'] or self.scene.step in ['techanim']:
                self.ui.removeImgs_checkBox.setChecked(False)
        else:
            state = False if self.scene.project else True
            self.set_warning(state)

    def set_project(self): 
        # set project config on start tool 
        self.scene.projectInfo.render.set_size(mode='preview')

    def set_warning(self, state):
        """ warning out of the pipeline """
        self.ui.project_label.setFont(QtGui.QFont("Arial", 9))
        color = [240, 60, 60]
        if state:
            self.ui.project_label.setText('-- Not in the Pipeline --')
            self.ui.project_label.setStyleSheet('color: rgb({}, {}, {})'.format(color[0], color[1], color[2]))
        else:
            self.ui.project_label.setStyleSheet('')

    def setup_subclip_menu(self):
        # setup menu
        self.subclip_menu.clear()
        self.ui.attach_subclip_button.setMenu(self.subclip_menu)

        # animatic
        animatic_action = QtWidgets.QAction('animatic', self)
        animatic_action.triggered.connect(partial(self.add_subclip, 'edit', 'animatic'))
        self.subclip_menu.addAction(animatic_action)

        layout_action = QtWidgets.QAction('layout', self)
        layout_action.triggered.connect(partial(self.add_subclip, 'layout', 'main'))
        self.subclip_menu.addAction(layout_action)
        
        anim_action = QtWidgets.QAction('anim', self)
        anim_action.triggered.connect(partial(self.add_subclip, 'anim', 'main'))
        self.subclip_menu.addAction(anim_action)
        
        finalCam_action = QtWidgets.QAction('finalcam', self)
        finalCam_action.triggered.connect(partial(self.add_subclip, 'finalcam', 'main'))
        self.subclip_menu.addAction(finalCam_action)
        
        techAnim_action = QtWidgets.QAction('techanim', self)
        techAnim_action.triggered.connect(partial(self.add_subclip, 'techanim', 'main'))
        self.subclip_menu.addAction(techAnim_action)

    def setup_subclip_shot_options(self):
        selected_datas = self.ui.shot_listWidget.selected_datas()
        if len(selected_datas) != 1:
            self.setup_subclip_menu()
            return

        self.subclip_menu.clear()
        shotData = selected_datas[0]
        
        for step in ('edit', 'layout', 'anim', 'finalcam', 'techanim'):
            scene = self.scene.copy()
            scene = self.set_shot_context(scene, step, shotData['shotName'])
            workspace = context_info.ContextKey.publish if step=='edit' else  context_info.ContextKey.work
            processes = scene.list_process(workspace=workspace)
            
            if processes == ['main']:
                action = QtWidgets.QAction(step, self)
                action.triggered.connect(partial(self.add_subclip, step, 'main'))
                self.subclip_menu.addAction(action)
            else:
                if 'main' in processes:
                    processes.pop(processes.index('main'))
                    processes = ['main'] + processes
                # add new menu
                step_menu = QtWidgets.QMenu(step, self)
                has_file = False
                for p in processes:
                    scene.context.update(process=p)
                    scene.update_scene_context()
                    if self.get_subclip_path(scene=scene):
                        process_action = QtWidgets.QAction(p, self)
                        process_action.triggered.connect(partial(self.add_subclip, step, p))
                        step_menu.addAction(process_action)
                        has_file = True
                if has_file:
                    self.subclip_menu.addMenu(step_menu)

    def get_selected_subclips(self, shotData):
        subclips = []
        for data in self.ui.subclip_widget.get_all_item():
            if 'path' in data:
                subclips.append((os.path.basename(data['path']), data['path']))
            elif 'step' in data and 'process' in data:
                scene = self.scene.copy()
                scene.context.update(entityCode=shotData['shotName'])
                scene.context.update(step=data['step'])
                scene.context.update(process=data['process'])
                scene.context.update(entity=scene.sg_name)
                media_path = self.get_subclip_path(scene=scene)
                subclips.append(('{} - {}'.format(scene.step, scene.process), media_path))
        return subclips

    def get_subclip_path(self, scene):
        # get mov path
        if scene.step == 'edit':  # * Edit uses publ hero
            output_path = scene.path.output_hero_img().abs_path()
            filename = scene.publish_name(hero=True, ext='.mov')
            filepath = '{}/{}'.format(output_path, filename)
            if os.path.exists(filepath):
                return filepath
        else:  # Other dept do not want to publish, so use latest work
            output_path = scene.path.work_output().abs_path()
            if os.path.exists(output_path):
                latest_file = file_utils.get_latest_file(srcDir=output_path, ext='.mov').replace('\\', '/')
                if latest_file:
                    return latest_file
    
    def add_subclip(self, step, process):
        self.ui.subclip_widget.add_item('{} - {}'.format(step, process), data={'step':step, 'process':process})
    
    def browse_subclip(self):
        dialog = QtWidgets.QFileDialog()
        dialog.setWindowIcon(QtGui.QIcon('{0}/playblast_icon.png'.format(icons_dir)))
        dialog.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        dialog.setWindowTitle('Browse subclip')
        dialog.setNameFilters(["Supported Media (*.mov *.mp4 *.mpeg *.jpeg *.png)"])
        dialog.setFileMode(QtWidgets.QFileDialog.ExistingFiles)
        dialog.setViewMode(QtWidgets.QFileDialog.Detail)
        dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptOpen) 
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            chosen_paths = dialog.selectedFiles()
            for path in chosen_paths:
                self.ui.subclip_widget.add_item(os.path.basename(path), data={'path':path})

    def clear_subclip(self):
        self.ui.subclip_widget.clear()

    def quality_slider_changed(self):
        value = self.ui.quality_slider.value()
        self.ui.quality_field.setText(str(value))

    def quality_field_changed(self):
        value = self.ui.quality_field.text()[:-1]
        self.ui.quality_slider.setValue(int(value))

    def subclipsize_slider_changed(self):
        value = self.ui.subclipsize_slider.value()
        self.ui.subclipsize_field.setText(str(value))

    def subclipsize_field_changed(self):
        value = self.ui.subclipsize_field.text()[:-1]
        self.ui.subclipsize_slider.setValue(int(value))

    def list_step(self):
        self.ui.step_comboBox.clear()

        if self.pipeline:
            path = self.scene.path.step()
            browsePath = os.path.split(path.abs_path())[0]
            steps = sorted(file_utils.list_folder(browsePath))

            for i, step in enumerate(steps):
                display = '{} - Current'.format(step) if step == self.scene.step else step
                self.ui.step_comboBox.addItem(display)
                self.ui.step_comboBox.setItemData(i, step, QtCore.Qt.UserRole)

            # set default to step
            index = steps.index(self.scene.step) or 0
            self.ui.step_comboBox.setCurrentIndex(index)

        else:
            self.ui.step_comboBox.addItem('-- Not in the Pipeline --')

    def list_shot(self):
        if self.scene.project:
            self.add_shots(False)

    def add_shots(self, showAll=True):
        shots = hook.list_sequencer()
        
        if self.scene.entity_type == 'scene': 
            regShots = hook.list_shots(self.scene)
        if self.scene.entity_type == 'asset': 
            self.scene.context.update(res='md') # for asset mode
            regShots = hook.list_assets(self.scene)

        combineShots = sorted(list(set(shots + regShots)))
        mode = 'time' if self.ui.sortedOrder_checkBox.isChecked() else 'name'
        combineShots = hook.order_shot(combineShots, mode=mode)
        self.ui.shot_listWidget.clear()

        for i, shotName in enumerate(combineShots):
            startFrame, endFrame, seqStartFrame, seqEndFrame = hook.shot_info(shotName)
            camera = hook.camera_info(shotName)
            status = self.shot_status(shotName)
            isCreate = hook.obj_exists(shotName)
            data = {'shotName': shotName, 'startFrame': startFrame, 'endFrame': endFrame, 'seqStartFrame': seqStartFrame, 'seqEndFrame': seqEndFrame, 'status': status, 'playable': isCreate, 'camera': camera}
            frameRange = '{} - {}'.format(startFrame, endFrame)

            if showAll or status:
                widgetItem = shot_list_widget.ShotItemBasic()
                self.set_shot_item(widgetItem, shotName, frameRange, status, isCreate)
                item = self.ui.shot_listWidget.add_widget_item(widgetItem, data)

    def shot_status(self, shotName):
        if self.scene.entity_type == 'scene': 
            sequenceKey = self.scene.projectInfo.scene.sequence_key
            if self.shotName == sequenceKey:
                return True
            status = True if shotName.lower() == self.shotName.lower() else False
            return status
        return True

    def set_shot_item(self, widgetItem, shotName, frameRange, state, isCreate):
        color = [200, 200, 200] if state else [100, 100, 100]
        rangeColor = [200, 200, 100] if isCreate else [200, 100, 100]
        iconPath = icon.camera if state else icon.cameraNa
        color = color if isCreate else [200, 100, 100]
        widgetItem.set_shot_color(color)
        widgetItem.set_range_color(rangeColor)
        widgetItem.set_text_shot(shotName)
        widgetItem.set_text_frame(frameRange)
        widgetItem.set_icon(iconPath)

    def shot_selected(self, data):
        self.list_mov_files()
        self.button_check()
        self.setup_subclip_shot_options()

    def option_selected(self, value):
        state = True if value else False
        self.ui.playblast_frame.setVisible(state)

    def daily_selected(self, value):
        # state = self.ui.showAll_checkBox.isChecked()
        # self.ui.daily_frame.setVisible(state)
        # self.ui.daily_pushButton.setVisible(state)
        # self.ui.file_listWidget.clear()
        # self.add_shots(showAll=state)
        # self.fetch_sg_data()
        if value:
            self.check_daily_input()

    def publish_stage_changed(self, value):
        if not value and not self._task_set:
            self.fetch_sg_data()

    def refresh_shot(self):
        self.ui.file_listWidget.clear()
        self.add_shots(showAll=self.ui.showAll_checkBox.isChecked())

    def refresh_file(self):
        self.list_mov_files()

    def publish_selected(self, value):
        # self.ui.daily_checkBox.setChecked(True) if not self.ui.daily_checkBox.isChecked() else None
        # buttonLabel = 'Daily' if not self.ui.publish_checkBox.isChecked() else 'Publish'
        # self.ui.daily_pushButton.setText(buttonLabel)
        if value:
            self.check_daily_input()

    def auto_publish_selected(self, value):
        if value:
            self.ui.publish_option_groupBox.toggle_button.setChecked(False)

    def list_mov_files(self):
        items = self.ui.shot_listWidget.selected_items()
        fileDict = OrderedDict()
        step = self.ui.step_comboBox.itemData(self.ui.step_comboBox.currentIndex(), QtCore.Qt.UserRole) # allow mov list from different department context
        allProcess = self.ui.allProcess_checkBox.isChecked()

        if items:
            for item in items:
                shotData = item.data(QtCore.Qt.UserRole)
                shotName = shotData['shotName']
                status = shotData['status']
                # if status:
                self.scene = self.set_shot_context(self.scene, step, shotName)

                files = self.list_process_mov(self.scene, allProcess=allProcess)
                fileDict[shotName] = {'files': files}

            self.show_mov(fileDict)

    def list_process_mov(self, scene, allProcess=False):
        processes = self.get_all_process(scene, allProcess=allProcess)
        allFiles = []
        originalProcess = scene.process
        for process in processes:
            scene.context.update(process=process)
            workspaceOutput = scene.path.work_output().abs_path()
            files = file_utils.list_file(workspaceOutput) if os.path.exists(workspaceOutput) else []
            files = ['{}/{}'.format(workspaceOutput, a) for a in files]
            allFiles += files
        # restore
        scene.context.update(process=originalProcess)
        return allFiles

    def get_all_process(self, scene, allProcess=False):
        if not allProcess:
            return [scene.process]
        processPath = scene.path.process().abs_path()
        processes = file_utils.listFolder(os.path.split(processPath)[0])
        return processes

    def button_check(self):
        datas = self.ui.shot_listWidget.selected_datas()
        state = any([a['status'] for a in datas]) if datas else False
        self.ui.playblast_pushButton.setEnabled(state)

    def select_all(self, value):
        state = True if value else False
        items = self.ui.shot_listWidget.items()
        self.ui.shot_listWidget.blockSignals(True)
        self.ui.shot_listWidget.select_items(items, state)
        self.list_mov_files() if state else self.ui.file_listWidget.clear()
        self.ui.shot_listWidget.blockSignals(False)

    def show_all(self, value):
        state = True if value else False
        self.add_shots(state)

    def show_mov(self, fileDict):
        showLatest = self.ui.showLatest_checkBox.isChecked()
        self.ui.file_listWidget.clear()
        bgColor1 = [60, 60, 60]
        bgColor2 = [20, 20, 20]
        colors = [bgColor1, bgColor2]
        count = 0

        for shotName, data in fileDict.items():
            files = data['files']
            files = file_utils.sort_by_date(files)
            color = colors[count % 2]
            if showLatest:
                data = {'shotName': shotName, 'filePath': files[-1] if files else []}
                item = self.ui.file_listWidget.add_data(files[-1], data) if files else None
                item.setBackground(QtGui.QColor(color[0], color[1], color[2])) if item else None

            else:
                for filePath in files:
                    data = {'shotName': shotName, 'filePath': filePath}
                    item = self.ui.file_listWidget.add_data(filePath, data)
                    item.setBackground(QtGui.QColor(color[0], color[1], color[2]))

            if files:
                count += 1

    def playblast(self, queue=False):
        stepKeyOption = self.ui.playStepKey_checkBox.isChecked()
        stepKey = []
        if stepKeyOption: 
            stepKey = utils.get_stepkey()
            if not stepKey: 
                dialog.MessageBox.error('Error', 'Stepped key option is selected. Please select at least one object with keyframe')
                return 

        allow = check_shot_lockdown.run(self.scene) if self.scene.step == 'anim' else True
        # allow = True
        if allow: 
            items = self.ui.shot_listWidget.selected_items()
            if not items:
                no_item_err = 'Please select at least 1 shot camera'
                logger.error(no_item_err)
                dialog.MessageBox.error('Error', no_item_err)
                return

            # get/set task env
            task_check.check()
            showHud = self.ui.hud_checkBox.isChecked()
            autoPublish = self.ui.autoPublish_checkBox.isChecked()
            quality = self.ui.quality_slider.value()
            removeImgs = self.ui.removeImgs_checkBox.isChecked()
            step = self.scene.step # always play from scene context
            movFiles = []
            autoPublishData = []

            if not utils.viewport_active():
                no_active_cam_err = 'No active viewport found, please click on a viewport.'
                logger.error(no_active_cam_err)
                dialog.MessageBox.error('Error', no_active_cam_err)
                return

            if items:
                for item in items:
                    shotData = item.data(QtCore.Qt.UserRole)
                    shotName = shotData['shotName']
                    playable = shotData['playable']
                    startFrame = shotData['startFrame']
                    endFrame = shotData['endFrame']
                    camera = shotData['camera']
                    # hide all others camera
                    hiddenCamList = hook.hide_others_camera(self.scene, shotName)
                    # taskName = ''
                    # if self.ui.daily_checkBox.isChecked():
                    taskEntity = self.ui.task_comboBox.itemData(self.ui.task_comboBox.currentIndex(), QtCore.Qt.UserRole)
                    taskName = taskEntity.get('content', '') if taskEntity else ''
                    self.scene = self.set_shot_context(self.scene, step, shotName)

                    # set black frame on *:camCtrl_camCtrl
                    blackFrame = self.scene.projectInfo.render.black_frame()
                    if blackFrame:
                        hook.set_black_frame(camera, blackFrame)
                        hook.set_filmgate_abc(camera, blackFrame)

                    if queue:
                        upload = True if taskName else False
                        framerate = self.scene.projectInfo.render.fps()

                        # confirm sending to farm
                        confirm_sent_to_farm = self.confirm_dialog('Confirm',  'Send "{}" to Render Farm \nfor playblast, confirm?'.format(self.scene.name))
                        if not confirm_sent_to_farm:
                            return

                        hook.waitCursor(True)
                        sended_job, destination = utils.playblast_to_queue(self.scene, 
                                                    startFrame, 
                                                    endFrame, 
                                                    camera, 
                                                    shotName, 
                                                    task_name=taskName, 
                                                    sg_upload=upload, 
                                                    framerate=framerate,
                                                    remove_images=removeImgs)

                        logger.debug('Submitted to queue')
                        hook.waitCursor(False)
                        if sended_job:
                            dialog.MessageBox.success('Submit to farm', 'Playblast job sent to farm.\nPlease wait for output.')
                            try:
                                subprocess.Popen(r'explorer "{}"'.format(os.path.dirname(destination.replace('/', '\\'))))
                            except:
                                pass
                            self.update_status(item, 'Sent to queue', 'complete')
                        else:
                            dialog.MessageBox.error('Submit to farm', 'Error sending farm playblast, please try again.')
                            self.update_status(item, 'Cannot sent to queue. please resent', 'failed')
                        
                        continue

                    if playable:
                        self.update_status(item, 'Playblasting ...', 'normal')
                        settingResult = hook.set_camera(shotName) 
                        light = self.ui.light_checkBox.isChecked()

                        if not settingResult: 
                            dialog.MessageBox.error('Error', 'Wrong Overscan!\nPlease Edit cam to fix this issue and re-Export')
                            continue

                        # get subclips
                        subclips = self.get_selected_subclips(shotData)
                        logger.info('Subclips: {}'.format(subclips))
                        # get subclip size
                        subclipsize_value = self.ui.subclipsize_slider.value()
                        
                        subclip_size = (((45.0*subclipsize_value)/100) + 4 ) * 0.01 # map to range between 0.05 to 0.49
                        logger.info('Subclip size: {}'.format(subclip_size))

                        # start playblast
                        hook.waitCursor(True)
                        try:
                            result = utils.playblast_wip(self.scene, 
                                                        shotName, 
                                                        showHud, 
                                                        light, 
                                                        stepKey=stepKey, 
                                                        quality=quality, 
                                                        remove_images=removeImgs, 
                                                        subclips=subclips, 
                                                        subclip_size=subclip_size)
                            movFiles.append(result)
                            autoPublishData.append({'shotName': shotName, 'filePath': result})
                            self.update_status(item, 'Finished', 'complete')

                        except Exception as e:
                            logger.error(e)
                            self.update_status(item, 'Failed', 'failed')
                        hook.waitCursor(False)
                    else:
                        logger.warning('No shot name {}'.format(shotName))
                        self.update_status(item, 'No shot', 'failed')

                    hook.show_hidden_camera(hiddenCamList)

                if movFiles and self.ui.autoPlay_checkBox.isChecked():
                    self.list_mov_files()
                    self.play_media(files=movFiles)

                if autoPublish: 
                    if autoPublishData: 
                        logger.info('Auto publish ...')
                        self.auto_publish(autoPublishData)


    def set_shot_context(self, scene, step, shotName):
        scene.context.update(entityCode=shotName, step=step)
        scene.context.update(entity=scene.sg_name)
        return scene

    def play_media(self, files=[]):
        if not files:
            files = self.ui.file_listWidget.selected_files()
            files = [a['filePath'] for a in files]

        result = False
        if self.ui.rv_radiobutton.isChecked():
            result = self.open_rv(files)
        elif self.ui.keyframepro_radiobutton.isChecked():
            result = self.open_keyframe_pro(files)

        if not result:
            for f in files:
                os.startfile(f)

    def double_clicked_play_media(self, data):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        self.play_media(files=[data['filePath']])
        QtWidgets.QApplication.restoreOverrideCursor()

    def open_rv(self, filenames):
        if not filenames:
            return

        rvPath = config.Software.rv
        if not os.path.exists(rvPath):
            root, end = rvPath.split('RV-')
            version = [a for a in file_utils.list_folder(root) if 'RV-' in a]
            if version:
                rvPath = '{}{}/{}'.format(root, version[0], '/'.join(end.split('/')[1:]))
            else:
                dialog.MessageBox.error('Error', 'No RV installed')
                return

        commands = [rvPath]
        commands += filenames
        subprocess.Popen(commands, shell=True)
        return True

    def open_keyframe_pro(self, filenames): 
        if not filenames:
            return
        kfpPath = config.Software.keyframePro
        if os.path.exists(kfpPath):
            if len(filenames) > 1:
                commands = [config.Software.pythonwPath, 
                    '{script_root}/core/rf_utils/keyframepro_utils.py'.format(script_root=os.environ['RFSCRIPT']), 
                    '-i']
                commands += filenames
            else:
                commands = [kfpPath, filenames[-1]]
            subprocess.Popen(commands, shell=True)
            return True

    def update_status(self, item, message, status):
        if status == 'normal':
            color = Color.lightGrey
        elif status == 'complete':
            color = Color.green
        elif status == 'failed':
            color = Color.red
        widgetItem = self.ui.shot_listWidget.widget_item(item)
        widgetItem.set_text_status(message)
        widgetItem.set_status_color(color)
        QtWidgets.QApplication.processEvents()

    def fetch_sg_data(self):
        if not self.shotMode:
            datas = self.ui.shot_listWidget.datas()
            # get task from first shot
            shotName = datas[0]['shotName'] if datas else ''
            self.scene.context.update(entityCode=shotName)

        self.thread = thread.GetTaskPlaylist(self.scene)
        self.thread.value.connect(self.set_task_playlist)
        self.thread.start()

    def set_task_playlist(self, data):
        tasks = data['task']
        playlists = data['playlist']
        self.set_task(tasks)
        self.set_playlist(playlists)
        self.check_daily_input()
        self.auto_task_selection()
        self._task_set = True

    def check_daily_input(self):
        taskEntity = self.ui.task_comboBox.itemData(self.ui.task_comboBox.currentIndex(), QtCore.Qt.UserRole)
        inputFile = self.ui.file_listWidget.selected_file()
        publish_checked = True if self.ui.publish_radioButton.isChecked() else False
        taskName = ''
        if taskEntity:
            taskName = taskEntity.get('content', '')

        state = True if taskName and inputFile else False
        if self.shotMode:
            state = state if len(self.ui.file_listWidget.selected_files()) == 1 else False
        self.ui.daily_pushButton.setEnabled(state)

    def check_apr_task(self, taskEntity):
        # confirm status checking
        status = taskEntity.get('sg_status_list') if taskEntity else ''
        taskName = taskEntity.get('content', '') if taskEntity else ''
        if status == 'apr':
            result = self.confirm_dialog('Confirm',  '"{}" task already approved. Do you still want to daily this task?'.format(taskName))
            if not result:
                self.ui.task_comboBox.setCurrentIndex(0)
                return False
        return True

    def set_task(self, tasks):
        self.ui.task_comboBox.clear()
        self.ui.task_comboBox.addItem('- Select Task -')
        self.ui.task_comboBox.setItemData(0, dict(), QtCore.Qt.UserRole)

        for i, task in enumerate(tasks):
            self.ui.task_comboBox.addItem(task['content'])
            self.ui.task_comboBox.setItemData(i+1, task, QtCore.Qt.UserRole)

    def set_playlist(self, playlists):
        self.ui.playlist_comboBox.clear()
        self.ui.playlist_comboBox.addItem('- Select Playlist -')
        self.ui.playlist_comboBox.setItemData(0, dict(), QtCore.Qt.UserRole)

        for i, playlist in enumerate(playlists):
            self.ui.playlist_comboBox.addItem(playlist['code'])
            self.ui.playlist_comboBox.setItemData(i+1, playlist, QtCore.Qt.UserRole)

    def auto_task_selection(self):
        """ set auto task work only open scene from task manager """
        entity = self.scene.copy()
        entity.use_env_context()
        currentTask = entity.task
        self.scene.context.update(task=entity.task)
        allTasks = [self.ui.task_comboBox.itemData(a, QtCore.Qt.UserRole) for a in range(self.ui.task_comboBox.count())]

        for i, task in enumerate(allTasks):
            if task:
                content = task.get('content')
                if content == currentTask:
                    self.ui.task_comboBox.setCurrentIndex(i)
                    self.ui.task_comboBox.setEnabled(False)

    def send_to_sg(self):
        """ send to daily or publish """
        publish = self.ui.publish_radioButton.isChecked()
        # daily = self.ui.daily_checkBox.isChecked()

        playlists = []
        taskEntity = self.ui.task_comboBox.itemData(self.ui.task_comboBox.currentIndex(), QtCore.Qt.UserRole)
        playlist = self.ui.playlist_comboBox.itemData(self.ui.playlist_comboBox.currentIndex(), QtCore.Qt.UserRole)
        taskName = taskEntity.get('content', '')
        if taskName:
            confirm = self.check_apr_task(taskEntity)
            if not confirm:
                return

        status = taskEntity.get('sg_task_status')
        inputDatas = self.ui.file_listWidget.selected_files()
        self.submitCount = len(inputDatas)
        self.completeCount = 0
        description = self.ui.desc_lineEdit.text()

        if not publish:
            self.ui.status.setText('Submiting daily ...')
            self.ui.daily_pushButton.setEnabled(False)
            self.submitThread = thread.SubmitDialies(self.scene, taskName, inputDatas, playlist, description=description)
            self.submitThread.status.connect(self.daily_status)
            self.submitThread.finished.connect(self.daily_complete)
            self.submitThread.progressStatus.connect(self.daily_progress_status)
            self.submitThread.start()

        else:
            self.ui.status.setText('Publishing video ...')
            self.ui.daily_pushButton.setEnabled(False)
            self.submitThread = thread.SubmitPublish(self.scene, taskName, inputDatas, description=description)
            self.submitThread.status.connect(self.daily_status)
            self.submitThread.finished.connect(self.publish_complete)
            self.submitThread.progressStatus.connect(self.daily_progress_status)
            self.submitThread.start()

    def auto_publish(self, inputDatas): 
        """ auto publish """ 
        # inputDatas [{u'shotName': u's0010', u'filePath': u'P:/projectName/scene/work/ep01/pr_ep01_q0070_s0010/anim/main/output/pr_ep01_q0070_s0010_anim_main.v007.TA.mov'}]
        self.submitCount = len(inputDatas)
        self.completeCount = 0
        taskName = Config.autoTaskMap.get(self.scene.step, None)
        
        if self.scene.step in Config.autoPublishStep and taskName: 
            self.submitThread = thread.SubmitPublish(self.scene, taskName, inputDatas, pubStatus=Config.wip)
            self.submitThread.status.connect(self.daily_status)
            self.submitThread.finished.connect(partial(self.publish_complete, False))
            self.submitThread.progressStatus.connect(self.daily_progress_status)
            self.submitThread.start()

    def daily_complete(self):
        if self.completeCount == self.submitCount:
            message = 'Submit {} shots complete'.format(self.submitCount)
            dialogMessage = 'Send to daily complete!!'
            dialog.MessageBox.success('Daily status', dialogMessage)

        else:
            errorCount = self.submitCount - self.completeCount
            message = 'Submit {}/{} shots complete'.format(self.completeCount, self.submitCount)
            dialogMessage = 'Not all shots complete\nThere were {} shots error'.format(errorCount)
            dialog.MessageBox.error('Daily status', dialogMessage)

        self.ui.status.setText(message)
        self.ui.daily_pushButton.setEnabled(True)

    def publish_complete(self, showDialog=True):
        if self.completeCount == self.submitCount:
            message = 'Published {} shots complete'.format(self.submitCount)
            dialogMessage = 'Publish complete!!'
            if showDialog: 
                dialog.MessageBox.success('Publish status', dialogMessage)

        else:
            errorCount = self.submitCount - self.completeCount
            message = 'Submit {}/{} shots complete'.format(self.completeCount, self.submitCount)
            dialogMessage = 'Not all shots complete\nThere were {} shots error'.format(errorCount)
            if showDialog: 
                dialog.MessageBox.error('Publish status', dialogMessage)

        self.ui.status.setText(message)
        self.ui.daily_pushButton.setEnabled(True)

    def daily_progress_status(self, status):
        self.completeCount += 1 if status else 0

    def daily_status(self, dictValue):
        text = dictValue['text']
        text2 = dictValue['text2']
        status = dictValue['status']
        shotName = dictValue['shotName']
        item = self.find_shot_item(shotName)
        self.ui.status.setText(text)
        mode = 'complete' if status else 'failed'

        if item:
            self.update_status(item, text2, mode)

    def send_to_publish(self):
        taskEntity = self.ui.task_comboBox.itemData(self.ui.task_comboBox.currentIndex(), QtCore.Qt.UserRole)
        playlist = self.ui.playlist_comboBox.itemData(self.ui.playlist_comboBox.currentIndex(), QtCore.Qt.UserRole)
        taskName = taskEntity.get('content', '')

        inputDatas = self.ui.file_listWidget.selected_files()
        self.submitCount = len(inputDatas)
        self.completeCount = 0
        self.ui.status.setText('Submiting daily ...')

        self.ui.daily_pushButton.setEnabled(False)
        self.submitThread = thread.SubmitPublish(self.scene, taskEntity, inputDatas)
        self.submitThread.status.connect(self.daily_status)
        self.submitThread.finished.connect(self.daily_complete)
        self.submitThread.progressStatus.connect(self.daily_progress_status)
        self.submitThread.start()

    def find_shot_item(self, shotName):
        items = self.ui.shot_listWidget.items()

        for item in items:
            itemWidget = self.ui.shot_listWidget.listWidget.itemWidget(item)
            shotItem = itemWidget.get_text_shot()

            if shotName == shotItem:
                return item

    def open_explorer(self, item):
        data = item.data(QtCore.Qt.UserRole)
        path = data['filePath']
        subprocess.Popen(r'explorer /select,"{}"'.format(path.replace('/', '\\')))

    def confirm_dialog(self, title, message):
        msgBox = QtWidgets.QMessageBox( self )
        msgBox.setIcon( QtWidgets.QMessageBox.Information )
        msgBox.setText(message)
        msgBox.setWindowTitle(title)

        # msgBox.setInformativeText(information)
        msgBox.addButton( QtWidgets.QMessageBox.Yes )
        msgBox.addButton( QtWidgets.QMessageBox.No )

        msgBox.setDefaultButton( QtWidgets.QMessageBox.No )
        result = msgBox.exec_()

        if result == QtWidgets.QMessageBox.Yes:
            return True
        return False

def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = PlayblastCore(maya_win.getMayaWindow())
    myApp.show()
    return myApp

# from rftool.scene.playblast import utils
# from rf_utils.context import context_info
# reload(utils)
# scene = context_info.ContextPathInfo()
# utils.playblast_daily(scene, 'S0010', 'layout', inputFile=None)
# from rf_utils import publish_info
# reload(publish_info)
# reg = publish_info.RegisterShotInfo(scene)
# reg.get_latest_version('workspace')
# reload(app.ui.info_widget)
