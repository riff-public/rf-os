# v.0.0.1 polytag switcher
_title = 'PlayblastExpress'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'PlayblastExpressUI'

#Import python modules
import sys
import os
import getpass
import subprocess

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]


# framework modules
import rf_config as config
reload(config)
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.context import context_info
from rf_utils import file_utils
import utils
reload(utils)
import thread
reload(thread)
import maya.cmds as mc
import maya.mel as mm
user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'

logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

sg = None


class PlayblastExpress(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        #Setup Window
        super(PlayblastExpress, self).__init__(parent)

        # ui read
        uiFile = '%s/express_ui.ui' % moduleDir
        self.ui = load.setup_ui_maya(uiFile, parent)
        self.setCentralWidget(self.ui)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setObjectName(uiName)

        self.w = 300
        self.h = 430
        self.resize(self.w, self.h)

        self.scene = context_info.ContextPathInfo()
        self.shotName = self.scene.name_code
        self.init_functions()
        self.init_signals()

    def init_signals(self):
        self.ui.view_pushButton.clicked.connect(self.view)
        self.ui.create_pushButton.clicked.connect(self.create_seq)
        self.ui.openDir_pushButton.clicked.connect(self.open_explorer)
        self.ui.sequencer_pushButton.clicked.connect(self.show_sequencer)
        self.ui.playblast_pushButton.clicked.connect(self.playblast)
        self.ui.daily_checkBox.stateChanged.connect(self.show_daily)
        self.ui.daily_pushButton.clicked.connect(self.send_to_daily)


    def init_functions(self):
        self.ui.sequencer_pushButton.setVisible(False)
        self.ui.daily_pushButton.setEnabled(False)
        self.show_daily_ui(False)
        self.list_mov()
        self.list_sequencer()
        self.list_camera()

    def show_daily_ui(self, state):
        self.ui.task.setVisible(state)
        self.ui.playlist.setVisible(state)
        self.ui.task_comboBox.setVisible(state)
        self.ui.playlist_comboBox.setVisible(state)
        self.ui.daily_pushButton.setVisible(state)

    def show_daily(self, state):
        extend = 27 if state else -27
        self.show_daily_ui(state)
        self.resize(self.w, self.h + extend)
        if state:
            self.fetch_sg_data()

    def fetch_sg_data(self):
        self.thread = thread.GetTaskPlaylist(self.scene)
        self.thread.value.connect(self.set_task_playlist)
        self.thread.start()

    def set_task_playlist(self, data):
        tasks = data['task']
        playlists = data['playlist']
        self.set_task(tasks)
        self.set_playlist(playlists)
        self.ui.daily_pushButton.setEnabled(True)

    def set_task(self, tasks):
        self.ui.task_comboBox.clear()
        self.ui.task_comboBox.addItem('- Select Task -')
        self.ui.task_comboBox.setItemData(0, dict(), QtCore.Qt.UserRole)

        for i, task in enumerate(tasks):
            self.ui.task_comboBox.addItem(task['content'])
            self.ui.task_comboBox.setItemData(i+1, task, QtCore.Qt.UserRole)

    def set_playlist(self, playlists):
        self.ui.playlist_comboBox.clear()
        self.ui.playlist_comboBox.addItem('- Select Playlist -')
        self.ui.playlist_comboBox.setItemData(0, dict(), QtCore.Qt.UserRole)

        for i, playlist in enumerate(playlists):
            self.ui.playlist_comboBox.addItem(playlist['code'])
            self.ui.playlist_comboBox.setItemData(i+1, playlist, QtCore.Qt.UserRole)

    def list_camera(self):
        exception = ['front', 'side', 'top']
        cams = [a for a in mc.listCameras() if not a in exception]
        self.ui.cam_comboBox.clear()
        self.ui.cam_comboBox.addItems(cams)

    def list_sequencer(self):
        sequencers = mc.ls(type='shot')
        self.ui.seq_comboBox.clear()
        if sequencers:
            self.ui.seq_comboBox.addItems(sequencers)
            self.ui.playblast_pushButton.setEnabled(True)
            self.create_seq_ui(False)
        else:
            self.ui.seq_comboBox.addItem('- No Sequencer -')
            self.ui.playblast_pushButton.setEnabled(False)
            self.create_seq_ui(True)

    def create_seq_ui(self, state):
        self.ui.create_pushButton.setVisible(state)
        self.ui.cam_comboBox.setVisible(state)
        self.ui.sequencer_pushButton.setVisible(not state)

    def show_sequencer(self):
        mm.eval('SequenceEditor;')

    def create_seq(self):
        cam = str(self.ui.cam_comboBox.currentText())
        startTime = mc.playbackOptions(q=True, min=True)
        endTime = mc.playbackOptions(q=True, max=True)
        result = mc.shot(startTime=startTime, endTime=endTime, sequenceStartTime=startTime, sequenceEndTime=endTime, currentCamera=cam)
        mc.setAttr('%s.shotName' % result, self.shotName, type='string')
        mc.rename(result, self.shotName)
        self.list_sequencer()

    def open_explorer(self):
        workspaceOutput = self.scene.path.work_output().abs_path()
        if workspaceOutput:
            subprocess.Popen(r'explorer /select,"%s"' % workspaceOutput.replace('/', '\\'))


    def playblast(self):
        self.scene = context_info.ContextPathInfo()
        shot = str(self.ui.seq_comboBox.currentText())
        showHud = self.ui.hud_checkBox.isChecked()
        result = utils.playblast_wip(self.scene, shot, showHud)
        self.list_mov()
        self.open_rv(result)

    def send_to_daily(self):
        playlists = []
        taskEntity = self.ui.task_comboBox.itemData(self.ui.task_comboBox.currentIndex(), QtCore.Qt.UserRole)
        taskName = taskEntity.get('content', '')
        inputFile = self.ui.file_listWidget.currentItem().data(QtCore.Qt.UserRole) if self.ui.file_listWidget.currentItem() else ''
        playlist = self.ui.playlist_comboBox.itemData(self.ui.playlist_comboBox.currentIndex(), QtCore.Qt.UserRole)

        if playlist:
            playlists.append(playlist)

        if taskName and inputFile:
            self.ui.daily_pushButton.setEnabled(False)
            # utils.playblast_daily(self.scene, '', taskName, inputFile=inputFile, showHud=True, playlists=playlists)
            # self.ui.daily_pushButton.setEnabled(True)
            self.submitThread = thread.SubmitDialy(self.scene, taskName, inputFile, playlists)
            self.submitThread.value.connect(self.daily_complete)
            self.submitThread.start()

        else:
            QtWidgets.QMessageBox.warning(self, 'Information', 'Please select a file and task to submit daily')

    def daily_complete(self, value):
        self.ui.daily_pushButton.setEnabled(True)
        QtWidgets.QMessageBox.information(self, 'Complete', 'Submit daily complete')

    def list_mov(self):
        workspaceOutput = self.scene.path.work_output().abs_path()
        self.ui.file_listWidget.clear()
        if os.path.exists(workspaceOutput):
            files = file_utils.list_file(workspaceOutput)
            for file in files:
                item = QtWidgets.QListWidgetItem(self.ui.file_listWidget)
                item.setText(file)
                item.setData(QtCore.Qt.UserRole, '%s/%s' % (workspaceOutput, file))

        else:
            self.ui.file_listWidget.addItem('-- No Files --')

    def get_sel_file(self):
        selItem = self.ui.file_listWidget.currentItem()
        return selItem.data(QtCore.Qt.UserRole) if selItem else None

    def view(self, inputFile=''):
        filename = self.get_sel_file()

        if inputFile:
            self.open_rv(inputFile)

        elif filename:
            self.open_rv(filename)

    def open_rv(self, filename):
        subprocess.Popen([config.Software.rv, filename])



def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = PlayblastExpress(maya_win.getMayaWindow())
    myApp.show()
    return myApp

# from rftool.scene.playblast import utils
# from rf_utils.context import context_info
# reload(utils)
# scene = context_info.ContextPathInfo()
# utils.playblast_daily(scene, 'S0010', 'layout', inputFile=None)
# from rf_utils import publish_info
# reload(publish_info)
# reg = publish_info.RegisterShotInfo(scene)
# reg.get_latest_version('workspace')
# reload(app.ui.info_widget)
