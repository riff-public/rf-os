import maya.cmds as mc
import maya.mel as mm
from functools import partial
from rf_utils.context import context_info
from rf_utils import file_utils

import maya.app.renderSetup.model.renderSetup as renderSetup

rs = renderSetup.instance()
currentLayer = rs.getVisibleRenderLayer().name()


class Config: 
	tracker = 'tracker'
	assetPath = 'assetPath'
	info = 'info'


def create_rivet(prefix='prefix', force=True):
	# prefix = 'aaaa'
	edges = mc.filterExpand(sm = 32)
	assetPath = str()

	if len(edges) >= 2 :
		assetName = ''
		locname = '{}_loc'.format(prefix)
		create = False if mc.objExists(locname) else True 
		create = True if force else create 
		if create: 
			if mc.referenceQuery(edges[0], inr=True): 
				assetPath = mc.referenceQuery(edges[0], f=True)
				asset = context_info.ContextPathInfo(path=assetPath)
				assetName = asset.name
			transform = edges[0].split('.')[0]
			meshShape = mc.listRelatives(transform, s = True)[0]
			e1 = edges[0].split('[')[-1].split(']')[0]
			e2 = edges[1].split('[')[-1].split(']')[0]

			# create curveFromMeshEdge
			cfme1 = mc.createNode('curveFromMeshEdge', n = '%s1_cfme' % prefix)
			cfme2 = mc.createNode('curveFromMeshEdge', n = '%s2_cfme' % prefix)

			# create loft
			loft = mc.createNode('loft', n = '%s_loft' % prefix)

			# set edge index
			mc.setAttr('%s.ei[0]' % cfme1, int(e1))
			mc.setAttr('%s.ei[0]' % cfme2, int(e2))

			# connect shape to curveFromMeshEdge
			mc.connectAttr('%s.worldMesh[0]' % meshShape, '%s.inputMesh' % cfme1, f = True)
			mc.connectAttr('%s.worldMesh[0]' % meshShape, '%s.inputMesh' % cfme2, f = True)

			# connect curveFromMeshEdge to loft
			mc.connectAttr('%s.outputCurve' % cfme1, '%s.inputCurve[0]' % loft, f = True)
			mc.connectAttr('%s.outputCurve' % cfme2, '%s.inputCurve[1]' % loft, f = True)

			# createNode pointOnSurfaceInfo
			poi = mc.createNode('pointOnSurfaceInfo', n = '%s_poi' % prefix)
			mc.setAttr('%s.turnOnPercentage' % poi, 1)
			mc.setAttr('%s.parameterU' % poi, 0.5)
			mc.setAttr('%s.parameterV' % poi, 0.5)

			# connect loft to poi
			mc.connectAttr('%s.outputSurface' % loft, '%s.inputSurface' % poi, f = True)

			# createNode aimConstraint
			aim = mc.createNode('aimConstraint', n = '%s_aim' % prefix)

			# connect poi to aimConstraint
			mc.connectAttr('%s.tangentV' % poi, '%s.target[0].targetTranslate' % aim, f = True)
			mc.connectAttr('%s.normal' % poi, '%s.worldUpVector' % aim, f = True)

			# create locator
			name = '%s_%s_loc' % (assetName, prefix) if assetName else '%s_loc' % prefix
			loc = mc.spaceLocator(n = name)[0]

			attr = Config.tracker
			mc.addAttr(loc, ln=attr, at='bool')
			mc.setAttr('{}.{}'.format(loc, attr), e=True, keyable=True)
			mc.setAttr('{}.{}'.format(loc, attr), True)

			attr1 = Config.assetPath
			mc.addAttr(loc, ln=attr1, dt='string')
			mc.setAttr('{}.{}'.format(loc, attr1), e=True, keyable=True)
			mc.setAttr('{}.{}'.format(loc, attr1), assetPath, type='string')

			attr2 = Config.info
			mc.addAttr(loc, ln=attr2, dt='string')
			mc.setAttr('{}.{}'.format(loc, attr2), e=True, keyable=True)
			mc.setAttr('{}.{}'.format(loc, attr2), str(edges), type='string')

			# connect poi to loc
			mc.connectAttr('%s.position' % poi, '%s.translate' % loc, f = True)
			mc.connectAttr('%s.constraintRotate' % aim, '%s.rotate' % loc, f = True)

			return loc, aim


class Ui(): 
	def __init__(self): 
		self.uiName = 'rivetUi'
		self.trackerGrp = 'tracker_Grp'

	def show(self): 
		if mc.window(self.uiName, exists=True): 
			mc.deleteUI(self.uiName)
		win = mc.window(self.uiName, t='Rivet Ui')
		mc.window(win, e=True, wh=[200, 320])
		mc.columnLayout(adj=True, rs=4)

		mc.frameLayout(l='Create Locator', collapsable=True)
		mc.text(l='Enter Prefix')
		self.prefixTx = mc.textField(tx='Ui')
		mc.button(l='Create', c=partial(self.createRivet), h=30, bgc=(0.6, 1, 0.6))
		mc.setParent('..')

		mc.frameLayout(l='Export tracker data', collapsable=True)
		mc.text(l='Select asset and export')
		mc.button(l='Export', c=partial(self.exportData), h=30, bgc=(1, 0.6, 0.4))
		mc.setParent('..')

		mc.frameLayout(l='Create tracker', collapsable=True)
		mc.text(l='Create tracker in cache scene')
		mc.button(l='Create All tracker', c=partial(self.buildRivet), h=30, bgc=(0.6, 0.8, 1))
		mc.button(l='Create Selected', c=partial(self.buildSelectedRivet), h=30, bgc=(0.6, 0.8, 1))
		mc.setParent('..')

		self.status = mc.text(l='')

		mc.showWindow()

	def createRivet(self, *args): 
		prefix = mc.textField(self.prefixTx, q=True, tx=True)
		result = create_rivet(prefix, force=False)
		if result: 
			print result, 
			loc, constraint = result
			entity = find_asset(loc)
			group_locator(entity, (loc, constraint))

	def exportData(self, *args): 
		export()
		mc.confirmDialog(title="Done", m="Export Done")

	def buildRivet(self, *args):
		if currentLayer != "defaultRenderLayer":
			return mc.warning("Please Switch to MasterLayer")

		build()
		mc.confirmDialog(title="Done", m="Create all done")

	def buildSelectedRivet(self, *args):
		if currentLayer != "defaultRenderLayer":
			return mc.warning("Please Switch to MasterLayer")
			
		build_selected()
		mc.confirmDialog(title="Done", m="Create selected done")


def group_locator(entity, objs): 
	trackerGrp = 'tracker_Grp'
	assetGrp = entity.name
	if not mc.objExists(trackerGrp): 
		group = mc.group(em=True, n=trackerGrp)
	if not mc.objExists(entity.name): 
		assetGrp = mc.group(em=True, n=entity.name)
	mc.parent(objs, assetGrp)
	parents = mc.listRelatives(assetGrp, p=True)
	if not parents: 
		mc.parent(assetGrp, trackerGrp)
	else: 
		if trackerGrp not in parents: 
			mc.parent(assetGrp, trackerGrp)


def run(): 
	ui = Ui()
	ui.show()


def list_trackers(): 
	locs = mc.ls(type='locator')
	trackers = list()
	for locShape in locs: 
		loc = mc.listRelatives(locShape, p=True)[0]
		attr = '{}.{}'.format(loc, Config.tracker)
		if mc.objExists(attr): 
			trackers.append(loc)
	return trackers


def find_edges(locator): 
	from rftool.utils import maya_utils
	reload(maya_utils)
	nodes = maya_utils.list_node_downstream(locator)
	cfmes = [a for a in nodes if mc.objectType(a, isType='curveFromMeshEdge')]
	meshes = [a for a in nodes if mc.objectType(a, isType='mesh')]
	edgeIndexs = list()

	if meshes: 
		for cfme in cfmes: 
			index = mc.getAttr('{}.edgeIndex[0]'.format(cfme))
			attr = '{}.e[{}]'.format(meshes[0], index)
			edgeIndexs.append(attr)

	return edgeIndexs


def find_object(locator): 
	from rftool.utils import maya_utils
	reload(maya_utils)
	nodes = maya_utils.list_node_downstream(locator)
	meshes = [a for a in nodes if mc.objectType(a, isType='mesh')]

	if meshes: 
		return mc.listRelatives(meshes[0], p=True)


def find_asset(locator): 
	mesh = find_object(locator)
	if mc.referenceQuery(mesh, inr=True): 
		path = mc.referenceQuery(mesh, f=True)
		entity = context_info.ContextPathInfo(path=path)
		return entity


def export():
	""" export locator """ 
	import maya.cmds as mc
	sels = mc.ls(sl=True)
	assets = list()
	loc_dict = dict()

	# list all assets 
	for sel in sels: 
		if mc.referenceQuery(sel, inr=True): 
			path = mc.referenceQuery(sel, f=True)
			assets.append(path)

	# list all locators 
	locs = list_trackers()
	for loc in locs: 
		mesh = find_object(loc)
		if mc.referenceQuery(mesh, inr=True): 
			asset_path = mc.referenceQuery(mesh, f=True)
			entity = context_info.ContextPathInfo(path=asset_path)
			if not entity.name in loc_dict.keys(): 
				loc_dict[entity.name] = [loc]
			else: 
				loc_dict[entity.name].append(loc)

	for path in list(set(assets)): 
		data = dict()
		entity = context_info.ContextPathInfo(path=path)
		entity.context.update(process='main', res='md', step='rig')
		data_file = '{}/{}'.format(entity.path.hero().abs_path(), entity.output_name(outputKey='data', hero=True))
		if entity.name in loc_dict.keys(): 
			locs = loc_dict[entity.name]
			for loc in locs: 
				edges = find_edges(loc)
				data[loc] = edges
			raw_data = file_utils.ymlLoader(data_file)
			raw_data['trackers'] = data
			file_utils.ymlDumper(data_file, raw_data)
			print('write {} - {}'.format(data_file, data))
		else: 
			print('no locator found on {}'.format(entity.name))


def build(): 
	assets = get_tracker_asset()
	for asset in assets: 
		build_asset(asset)


def build_selected(): 
	scene = context_info.ContextPathInfo()
	sels = mc.ls(sl=True)
	validNaming = list()
	assets = get_tracker_asset()
	invalid = list()

	for sel in sels: 
		if mc.referenceQuery(sel, inr=True): 
			path = mc.referenceQuery(sel, f=True)
			namespace = mc.referenceQuery(path, ns=True)[1:]
			validName = '{}:{}'.format(namespace, scene.projectInfo.asset.geo_grp())
			if validName in assets: 
				build_asset(validName)
			else: 
				print '{} has no tracking data'.format(validName)
				invalid.append(validName)


def get_tracker_asset():
	import os
	# list all namespace 
	scene = context_info.ContextPathInfo()
	allAssets = mc.ls('{}:{}'.format('*', scene.projectInfo.asset.geo_grp()))
	validAssets = list()

	for asset in allAssets: 
		assetData = get_asset_data(asset)
		if not assetData:
			mc.warning("{} is invalid type. skipped".format(asset))
			continue
		data_file = assetData['data_file']
		if not os.path.isfile(data_file):
			mc.warning("{} data file not found. skipped".format(asset))
			continue

		raw_data = file_utils.ymlLoader(data_file)
		if 'trackers' in raw_data.keys(): 
			validAssets.append(asset)

	return validAssets

def build_asset(asset): 
	assetData = get_asset_data(asset)
	data_file = assetData['data_file']
	namespace = assetData['namespace']
	entity = assetData['entity']
	raw_data = file_utils.ymlLoader(data_file)
	data = raw_data['trackers']

	for loc, edges in data.items(): 
		prefix = loc.replace('_loc', '')
		selections = list()
		for edge in edges: 
			edge_name = '{}:{}'.format(namespace, edge.split(':')[-1])
			selections.append(edge_name)

		mc.select(selections)
		result = create_rivet(prefix, force=False)
		if result: 
			newloc, constraint = result
			group_locator(entity, (newloc, constraint))

def get_asset_data(cacheAsset): 
	from rf_utils import register_shot
	scene = context_info.ContextPathInfo()
	reg = register_shot.Register(scene)

	asset = cacheAsset.split(":")[0]
	if asset not in reg.get.assetData:
		return
	if reg.get.assetData[asset]["type"] != "abc_cache":
		return

	data = dict()

	asset_path = mc.getAttr('{}.refPath'.format(cacheAsset))
	entity = context_info.ContextPathInfo(path=asset_path)
	if mc.referenceQuery(cacheAsset, inr=True): 
		path = mc.referenceQuery(cacheAsset, f=True)
		namespace = mc.referenceQuery(path, ns=True)[1:]
		data_file = '{}/{}'.format(entity.path.hero().abs_path(), entity.output_name(outputKey='data', hero=True))
	data = {'path': path, 'namespace': namespace, 'data_file': data_file, 'entity': entity}
	return data


""" 
Ui for export rivet 
from rftool.loc_tracker import rivet 
rivet.run()

Export data 
# select any part of reference asset 
rivet.export()

Build locator in cache file 
rivet.build()
or 
rivet.build_asset('kai_001:Geo_Grp')
"""
