_title = 'Riff Movie Image'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFMovieImage'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
# sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import maya.cmds as mc
import maya.mel as mm
import ui
reload(ui)

IMAGEPLANE_SIZE_MULTIPLIER = 0.24
GAP = 0.01

class RFMovieImage(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #SetUp Window
        super(RFMovieImage, self).__init__(parent)
        #ui read
        self.ui = ui.MovieImageUi(parent)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setCentralWidget(self.ui)
        self.lst_shot = mc.sequenceManager(lsh=True)
        self.list_seq()
        self.init_state()
        self.adjustSize()
        self.setFixedSize(270,195)
        # self.set_logo()

    def init_state(self):
        self.ui.Create_pushButton.clicked.connect(self.create_all_seq)
        self.ui.left_up_pushButton.clicked.connect(self.set_offset_left_up)
        self.ui.right_up_pushButton.clicked.connect(self.set_offset_right_up)
        self.ui.left_down_pushButton.clicked.connect(self.set_offset_left_down)
        self.ui.right_down_pushButton.clicked.connect(self.set_offset_right_down)
        self.ui.create_by_shot_button.clicked.connect(self.create_one_shot)
        self.ui.close_pushButtom.clicked.connect(self.close)
        self.ui.close_all_button.clicked.connect(self.close_all)
        self.ui.size_slider.valueChanged.connect(self.change_size)
        self.ui.reset_button.clicked.connect(self.reset_size)
        self.ui.size_field.setText('100')
        self.ui.size_field.returnPressed.connect(self.size_text_changed)

    def close(self):
        shot_name = self.ui.comboBox.currentText()
        cam_shot = mc.shot(shot_name,currentCamera=True,q=True)
        img_plane = mc.listConnections(cam_shot,type='imagePlane')
        if img_plane:
            mc.delete(img_plane)

    def close_all(self):
        close_all_movie_img_plane(self.lst_shot)

    def set_offset_left_up(self):
        shot_name = self.ui.comboBox.currentText()
        cam_shot = mc.shot(shot_name,currentCamera=True,q=True)
        tr = mc.listConnections(cam_shot,type='imagePlane')[0]
        img_plane = mc.listRelatives(tr, shapes=True, type='imagePlane')[0]
        self.set_offset(img_plane, 'LeftUp')

    def set_offset_right_up(self):
        shot_name = self.ui.comboBox.currentText()
        cam_shot = mc.shot(shot_name,currentCamera=True,q=True)
        tr = mc.listConnections(cam_shot,type='imagePlane')[0]
        img_plane = mc.listRelatives(tr, shapes=True, type='imagePlane')[0]
        self.set_offset(img_plane, 'RightUp')

    def set_offset_left_down(self):
        shot_name = self.ui.comboBox.currentText()
        cam_shot = mc.shot(shot_name,currentCamera=True,q=True)
        tr = mc.listConnections(cam_shot,type='imagePlane')[0]
        img_plane = mc.listRelatives(tr, shapes=True, type='imagePlane')[0]
        self.set_offset(img_plane, 'LeftDown')

    def set_offset_right_down(self):
        shot_name = self.ui.comboBox.currentText()
        cam_shot = mc.shot(shot_name,currentCamera=True,q=True)
        tr = mc.listConnections(cam_shot,type='imagePlane')[0]
        img_plane = mc.listRelatives(tr, shapes=True, type='imagePlane')[0]
        self.set_offset(img_plane, 'RightDown')

    def size_text_changed(self):
        value = int(self.ui.size_field.text())
        self.ui.size_slider.setValue(value)

    def change_size(self):
        self.ui.size_field.setText(str(self.ui.size_slider.value()))
        shot_name = self.ui.comboBox.currentText()
        cam_shot = mc.shot(shot_name,currentCamera=True,q=True)
        try:
            tr = mc.listConnections(cam_shot,type='imagePlane')[0]
            img_plane = mc.listRelatives(tr, shapes=True, type='imagePlane')[0]
        except:
            return

        direction = 'LeftDown'
        try:
            dir_value = mc.getAttr('%s.imgPlaneDirection' %img_plane)
            if dir_value in ('RightUp', 'LeftDown', 'RightDown', 'LeftUp'):
                direction = dir_value
        except:
            pass

        self.set_offset(img_plane, direction)

    def reset_size(self):
        self.ui.size_slider.setValue(100)

    def list_seq(self):
        if self.lst_shot:
            for shot in self.lst_shot:
                self.ui.comboBox.addItem(shot)

    def create_seq_movie(self, shot_name):
        cam_shot = mc.shot(shot_name,currentCamera=True,q=True)
        scene = self.scene_context()
        name = scene.context.entity
        step = scene.context.step
        if 'all' in name:
            name = name.replace('all', shot_name)
        scene.context.update(entityCode=shot_name)
        scene.context.update(entity=name)
        scene.context.update(step='edit')
        scene.context.update(process = 'animatic')
        output_path = scene.path.output_hero_img().abs_path()
        files = os.listdir(output_path) if os.path.exists(output_path) else None


        if files:
            file = files[0]
            # print file
        else:
            return
        path_files = os.path.join(output_path, file).replace('\\', '/')
        img_plane_tr, img_plane = self.get_img_plane_from_cam(cam=cam_shot)

        if step =='layout':
            if mc.objectType(cam_shot) == 'camera':
                mc.shot(shot_name, cl= path_files, co =.9 , e=True)
                img_plane_tr, img_plane = self.get_img_plane_from_cam(cam=cam_shot)
            else:
                return
        elif step == 'anim':
            if mc.objectType(cam_shot) == 'camera':
                if not img_plane_tr:
                    img_planes = mc.imagePlane(camera=cam_shot, sia=False)
                    img_plane_tr = img_planes[0]
                    img_plane = img_planes[1]

                frame_start = mc.getAttr("%s.startFrame"%shot_name)
                mc.setAttr("%s.useFrameExtension"%img_plane, 1)
                mc.setAttr("%s.type"%img_plane,2)
                mc.setAttr('%s.imageName'%img_plane, path_files ,type='string')
                express_node = mc.listConnections(img_plane,t='expression')
                mc.delete(express_node)
                mc.setAttr("%s.depth"%img_plane , 1)
                command_frameExt = "%s.frameExtension=frame - %d"%(img_plane, frame_start-1)
                mc.expression(img_plane, s = command_frameExt,n ='s0010_img_plane_exp')
            else:
                return

        # frame_start = mc.getAttr("%s.startFrame"%shot_name)
        # print cam_shot
        
        # print img_plane
        self.set_offset(img_plane, direction= 'LeftDown')
    
        return shot_name

    def get_img_plane_from_cam(self, cam):
        img_plane_tr, img_plane = None, None
        try:
            img_plane_tr = mc.listConnections('%s.imagePlane' %cam, s=True, d=False, type='imagePlane')
            img_plane = mc.listRelatives(img_plane_tr, shapes=True, type='imagePlane')[0]
        except:
            return None, None
        return img_plane_tr, img_plane

    def scene_context(self):
        from rf_utils.context import context_info
        scene = context_info.ContextPathInfo()
        return scene
    
    def get_path_scene_context(self, scene):
        scene.context.update(entityCode='s0010')
        scene.context.update(entity=names)
        scene.context.update(step='edit')
        scene.context.update(process = 'animatic')
        output_path = scene.path.output_hero_img().abs_path()
        files = os.listdir(output_path)[0]

    def create_all_seq(self):
        for shot in self.lst_shot:
            self.create_seq_movie(shot)

    def create_one_shot(self):
        shot =self.ui.comboBox.currentText()
        data = self.create_seq_movie(shot)
        # print data

    def fit_image(self, img_plane_shp):
        rg = mc.ls(renderGlobals=True)
        rez = mc.listConnections(rg[0] + ".resolution")
        rezAspect = mc.getAttr(rez[0] + ".deviceAspectRatio")
        cams = mc.listConnections(img_plane_shp + '.message', s=False, d=True, type='camera')
        if not cams:
            return

        camX = mc.getAttr(cams[0] + ".horizontalFilmAperture")
        camY = mc.getAttr(cams[0] + ".verticalFilmAperture")
        fitType = mc.getAttr(cams[0] + ".filmFit")

        sizeX = img_plane_shp+'.sizeX'
        sizeY = img_plane_shp+'.sizeY'
        if fitType == 0:  # fill
            camAspect = camX/camY
            if rezAspect < camAspect:
                mc.setAttr(sizeY, camY)
                mc.setAttr(sizeX, (camY*rezAspect))
            else:
                mc.setAttr(sizeX, camX)
                mc.setAttr(sizeY, (camX*rezAspect))
        elif fitType == 1:  # horizontal
            mc.setAttr(sizeX, camX)
            mc.setAttr(sizeY, (camX/rezAspect))
        elif fitType == 2:  # vertical
            mc.setAttr(sizeY, camY)
            mc.setAttr(sizeX, (camY/rezAspect))
        elif fitType == 3:  # overscan
            if rezAspect < camAspect:
                mc.setAttr(sizeX, camX)
                mc.setAttr(sizeY, (camX/rezAspect))
            else:
                mc.setAttr(sizeX, (camY*rezAspect))
                mc.setAttr(sizeY, camY)
                

    def set_offset(self, img_plane_shp, direction):
        mult = float(self.ui.size_slider.value()) / 100.0
        size = mult * IMAGEPLANE_SIZE_MULTIPLIER
        self.fit_image(img_plane_shp)
        sx = mc.getAttr('%s.sizeX' %img_plane_shp)
        sy = mc.getAttr('%s.sizeY' %img_plane_shp)

        mc.setAttr("%s.sizeX"%img_plane_shp , sx*size)
        mc.setAttr("%s.sizeY"%img_plane_shp , sy*size)

        avg = (sx + sy)*0.5

        offsetX = ((sx/2) - (sx*size*0.5)) - (avg*GAP)
        offsetY = ((sy/2) - (sy*size*0.5)) - (avg*GAP)
        if direction == 'LeftDown':
            mc.setAttr("%s.offsetX"%img_plane_shp , offsetX * -1)
            mc.setAttr("%s.offsetY"%img_plane_shp , offsetY * -1)
            
        elif direction == 'LeftUp':
            mc.setAttr("%s.offsetX"%img_plane_shp , offsetX * -1)
            mc.setAttr("%s.offsetY"%img_plane_shp , offsetY)

        elif direction == 'RightDown':
            mc.setAttr("%s.offsetX"%img_plane_shp , offsetX)
            mc.setAttr("%s.offsetY"%img_plane_shp , offsetY * -1)

        elif direction == 'RightUp':
            mc.setAttr("%s.offsetX"%img_plane_shp , offsetX)
            mc.setAttr("%s.offsetY"%img_plane_shp , offsetY)

        if not mc.objExists('%s.imgPlaneDirection' %img_plane_shp):
            mc.addAttr(img_plane_shp, ln='imgPlaneDirection', dt='string')
        mc.setAttr('%s.imgPlaneDirection' %img_plane_shp, direction, type='string')


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFMovieImage(maya_win.getMayaWindow())
        myApp.show()
        return myApp
if __name__ == '__main__':
    show()

def close_all_movie_img_plane(list_shot):
    for shot in list_shot:
        cam_shot = mc.shot(shot,currentCamera=True,q=True)
        img_plane = mc.listConnections(cam_shot,type='imagePlane')
        if img_plane:
            mc.delete(img_plane)