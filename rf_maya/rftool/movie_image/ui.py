# -*- coding: utf-8 -*-

#  implementation generated from reading ui file 'C:\Users\pawaris.n\Desktop\untitled.ui'
#
# Created: Thu Oct 25 14:40:29 2018
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class MovieImageUi(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(MovieImageUi, self).__init__(parent)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.comboBox = QtWidgets.QComboBox()
        self.horizontalLayout_4.addWidget(self.comboBox)
        self.pushButton_6 = QtWidgets.QPushButton("Change")
        self.pushButton_6.setMinimumSize(QtCore.QSize(50, 18))
        self.horizontalLayout_4.addWidget(self.pushButton_6)
        self.horizontalLayout_4.setStretch(0, 2)
        self.Create_pushButton = QtWidgets.QPushButton('Create All')
        self.Create_pushButton.setMinimumSize(QtCore.QSize(200, 18))
        self.verticalLayout.addWidget(self.Create_pushButton)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.label = QtWidgets.QLabel('Shot')
        self.horizontalLayout_5.addWidget(self.label)
        self.lineEdit = QtWidgets.QLineEdit()
        self.horizontalLayout_5.addWidget(self.lineEdit)

        self.create_by_shot_button = QtWidgets.QPushButton('Create by shot')
        self.create_by_shot_button.setMinimumSize(QtCore.QSize(200, 18))
        self.verticalLayout.addWidget(self.create_by_shot_button)

        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.sizeText = QtWidgets.QLabel('Size: ')
        self.size_slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.size_slider.setMinimum(0)
        self.size_slider.setMaximum(200)
        self.size_slider.setValue(100)
        self.size_field = QtWidgets.QLineEdit()
        self.size_field.setMinimumSize(QtCore.QSize(30, 18))
        self.size_field.setMaximumSize(QtCore.QSize(30, 18))
        self.size_field.setValidator(QtGui.QIntValidator())

        self.reset_button = QtWidgets.QPushButton('Reset')
        
        self.horizontalLayout_6.addWidget(self.sizeText)
        self.horizontalLayout_6.addWidget(self.size_field)
        self.horizontalLayout_6.addWidget(self.size_slider)
        self.horizontalLayout_6.addWidget(self.reset_button)
        # self.horizontalLayout_6.setStretch(1, 1, 4, 2)

        self.verticalLayout.addLayout(self.horizontalLayout_6)

        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.left_up_pushButton = QtWidgets.QPushButton("LeftUP")
        self.left_up_pushButton.setMinimumSize(QtCore.QSize(50, 18))
        self.horizontalLayout.addWidget(self.left_up_pushButton)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.right_up_pushButton = QtWidgets.QPushButton("RightUp")
        self.right_up_pushButton.setMinimumSize(QtCore.QSize(50, 18))
        self.horizontalLayout.addWidget(self.right_up_pushButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem1)
        self.close_pushButtom = QtWidgets.QPushButton('Close')
        self.close_pushButtom.setMinimumSize(QtCore.QSize(50, 18))
        self.horizontalLayout_3.addWidget(self.close_pushButtom)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem2)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.left_down_pushButton = QtWidgets.QPushButton('LeftDown')
        self.left_down_pushButton.setMinimumSize(QtCore.QSize(50, 18))
        self.horizontalLayout_2.addWidget(self.left_down_pushButton)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem3)
        self.right_down_pushButton = QtWidgets.QPushButton("RightDown")
        self.right_down_pushButton.setMinimumSize(QtCore.QSize(50, 18))

        self.close_all_button = QtWidgets.QPushButton("closeAll")
        self.close_all_button.setMinimumSize(QtCore.QSize(200, 18))
        self.horizontalLayout_2.addWidget(self.right_down_pushButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.verticalLayout.addWidget(self.close_all_button)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.verticalLayout.setStretch(1, 1)
        self.verticalLayout_2.setStretch(1, 1)
        self.setLayout(self.verticalLayout_2)

