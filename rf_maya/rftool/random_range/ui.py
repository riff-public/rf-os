import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import icon

class RandomOffsetUI(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(RandomOffsetUI, self).__init__(parent)
        self.resize(385, 111)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.setWindowTitle(" Riff RandomOffset")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.start_line_edit = QtWidgets.QLineEdit()
        self.horizontalLayout.addWidget(self.start_line_edit)
        self.label = QtWidgets.QLabel('-')
        self.horizontalLayout.addWidget(self.label)
        self.end_line_edit = QtWidgets.QLineEdit()
        self.horizontalLayout.addWidget(self.end_line_edit)
        self.random_push_button = QtWidgets.QPushButton('RandomOffset')
        self.horizontalLayout.addWidget(self.random_push_button)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.random_all_chk = QtWidgets.QCheckBox('Random All')
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.random_all_chk.sizePolicy().hasHeightForWidth())
        self.random_all_chk.setSizePolicy(sizePolicy)
        self.horizontalLayout_2.addWidget(self.random_all_chk)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.setLayout(self.verticalLayout_2)

