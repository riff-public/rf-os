_title = 'Riff RandomOffset'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFRandomOffset'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
# sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
import maya.cmds as mc
import ui
reload(ui)

from random import randint

class RandomOffset(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #SetUp Window
        super(RandomOffset, self).__init__(parent)
        #ui read
        self.ui = ui.RandomOffsetUI(parent)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setCentralWidget(self.ui)
        self.init_state()
        # self.set_logo()

    def init_state(self):
        self.ui.random_push_button.clicked.connect(self.set_attr)

    def random(self):
        range1 = self.ui.start_line_edit.text()
        range2 = self.ui.end_line_edit.text()
        return randint(int(range1), int(range2))

    def set_attr(self):
        if self.ui.random_all_chk.checkState() == QtCore.Qt.CheckState.Unchecked:
            list_sel = mc.ls(sl=True)
            print list_sel
            for transform in list_sel:
                print transform
                namespace = transform.split(':')
                if len(namespace) > 1 :
                    place_ctrl = '%s:Place_CtrlShape'%namespace[0]
                    chk = mc.listAttr( place_ctrl ,string='offset')
                    if chk:
                        print place_ctrl
                        random_value = self.random()
                        print random_value
                        mc.setAttr('%s.offset'%place_ctrl, random_value)
        elif self.ui.random_all_chk.checkState() == QtCore.Qt.CheckState.Checked:
            all_ctrl = mc.ls('*:Place_CtrlShape')
            for ctrl in all_ctrl:
                chk = mc.listAttr( ctrl ,string='offset')
                if chk:
                    print '%s.offset'%ctrl
                    random_value = self.random()
                    mc.setAttr('%s.offset'%ctrl, random_value)
             
def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RandomOffset(maya_win.getMayaWindow())
        myApp.show()
        return myApp
if __name__ == '__main__':
    show()
