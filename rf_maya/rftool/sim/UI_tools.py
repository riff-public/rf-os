import sys ;
import os ;
import pymel.core as pm ;
import maya.cmds as mc ;
import rf_config as config

path = os.environ['RFSCRIPT']

main_project = ''
selfPath = path + "/core/rf_maya/rftool/sim"

#########################################################################################################################################################################
####** When open UI, reload PLUGIN
##################
##### PLUGIN ##### 
##################

### ABC ###
if pm.pluginInfo ( 'AbcExport.mll' , q = True , loaded = True ) == True : pass ;
else : pm.loadPlugin ( 'AbcExport.mll' ) ;

if pm.pluginInfo ( 'AbcImport.mll' , q = True , loaded = True ) == True : pass ;
else : pm.loadPlugin ( 'AbcImport.mll' ) ;

### matrix ###
if pm.pluginInfo ( 'matrixNodes.mll' , q = True , loaded = True ) == True : pass ;
else : pm.loadPlugin ( 'matrixNodes.mll' ) ;

# ### '''yeti''' ###
# if pm.pluginInfo ( 'pgYetiMaya.mll' , q = True , loaded = True ) == True : pass ;
# else : pm.loadPlugin ( 'pgYetiMaya.mll' ) ;

### fbx ###
if pm.pluginInfo ( 'fbxmaya.mll' , q = True , loaded = True ) == True : pass ;
else : pm.loadPlugin ( 'fbxmaya.mll' ) ;
#########################################################################################################################################################################

def getUsername_cmd ( *args ) :

	if os.environ['RFuser']:
		return os.environ['RFuser']
	else:
		return ''
####################### get Username #########################

def filler ( *args ) :
	pm.text ( label = ' ' ) ;

####################### for space #############################
def addButtonToShelf_cmd ( *args ) : 
	import addButtonToShelf ;
	reload ( addButtonToShelf ) ;
	addButtonToShelf.run ( ) ;
####################### for UI_toots#########################


#########################################################################################################################################################################
############################ Main UI #######################################
def run ( ) :
	
	# check if window exists

	if pm.window ( 'MainUI' , exists = True ) :
		pm.deleteUI ( 'MainUI' ) ;
		
	# create window 

	window = pm.window ( 'MainUI', title =  'Character Effect Tools' , mnb = True , mxb = False , sizeable = False , rtf = True ) ;
	window = pm.window ( 'MainUI', e = True , width = 500 , h = 10 , menuBar=True) ;
	with window :

		mainLayout = pm.rowColumnLayout ( nc = 1 , w = 500) ;
		pm.setParent()
		with mainLayout :

			### title ###
			pm.symbolButton ( image = selfPath + '/' + 'image_fortools/title.v02.jpg' , width = 500 , height = 50  ) ;

			### username ###
			# with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/2/6 ) , ( 2 , width/2/6*4 ) , ( 3 , width/2/6  ) ] ):
			#     filler ( ) ;
			#     with pm.optionMenu(label='Projects'):
			#         pm.menuItem(label='Two Heros')
			#         pm.menuItem(label='SevenChicks')
			#         pm.menuItem(label='Pucca')
			#         pm.menuItem(label='CloudMaker')
			#     filler ( ) ;

			### username ###
			with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , 250 ) , ( 2 , 250 ) ]) :

				with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , 41.66 ) , ( 2 , 166.66 ) , ( 3 , 41.66  ) ] ) :
					filler ( ) ;
					pm.button ( label = 'add button to shelf' , c = addButtonToShelf_cmd ) ;
					filler ( ) ;

				with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , 62.5 )  , ( 2 , 62.5 ) , ( 3 , 62.5 ) ] ) :
					pm.text ( label = 'username' ) ;
					user = getUsername_cmd()
					pm.textField ( 'username_textField' , text = user, editable=False ) ;
					
					# pm.button ( label = 'save' , c =  ) ;

			with pm.rowColumnLayout ( nc = 2 , w = 250  , columnWidth = [ ( 1 , 250 ) , ( 2 , 250 ) ] ) :

				##################
				##################
				''' PROJECT '''
				##################
				##################
				with pm.tabLayout ( 'projects' , innerMarginWidth = 5 , innerMarginHeight = 5 ) : 
					##################
					''' SEVENCHICK '''
					##################
					with pm.rowColumnLayout ( 'ProjectData' , nc = 1 , cw = ( 1 , 250 ) ) :
						import projectData.project_data as project_data ;
						reload ( project_data ) ;
						main_project = 'project_data' 
						project_data.insert () ;

					##################
					''' Dark Horse '''
					##################
					with pm.rowColumnLayout ( 'Dark Horse' , nc = 1 , cw = ( 1 , 250 ) ) :
						import projectData.project_darkHorse as project_darkHorse ;
						reload ( project_darkHorse ) ;
						main_project = 'Dark Horse'
						project_darkHorse.insert ( ) ;
				# ##################
				##################
				''' UTILITIES '''
				##################
				##################
				with pm.tabLayout ( 'utilities' , innerMarginWidth = 5 , innerMarginHeight = 5 ) : 

					##################
					''' SIM '''
					##################
					with pm.rowColumnLayout ( 'Sim' , nc = 1 , cw = ( 1 , 5 ) ) :
						import tools.simtools.callSim as callSim ;
						reload ( callSim ) ;
						callSim.run ( ) ;

					##################
					''' ARTIST TOOL '''
					##################
					with pm.rowColumnLayout ( 'Artist Tool' , nc = 1 , cw = ( 1 , 250 ) ) :
						import tools.artist_tools.callArtist_tool as callArtist ;
						reload ( callArtist ) ;
						callArtist.run ( ) ;
					# with pm.rowColumnLayout ( 'Artist Tool' , nc = 1 , cw = ( 1 , 5 ) ) :
					# 	import tools.Artist_tool.callArtist_tool as callArtist_tool ;
					# 	reload ( callArtist_tool ) ;
					# 	callArtist_tool.run ( ) ;

					##################
					''' RIG '''
					##################
					with pm.rowColumnLayout ( 'Rig' , nc = 1 , cw = ( 1 , 250 ) ) :
						import tools.rig.callRig as callRig ;
						reload ( callRig ) ;
						callRig.insert ( ) ;

					##################
					''' RESOURCE '''
					##################
					with pm.rowColumnLayout ( 'Documents' , nc = 1 , cw = ( 1 , 5 ) ) :

						with pm.scrollLayout ( w = 250 , h = 500 ) :                    
							import tools.documents.callResources as callResources ;
							reload ( callResources ) ;
							callResources.insert ( ) ;              



	pm.showWindow ( window ) ;
##########################################################################################################################################################################