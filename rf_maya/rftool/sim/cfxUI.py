import os ;
import pymel.core as pm ;
import maya.cmds as mc ;

path = os.environ['RFSCRIPT']
mainProject = ''
selfPath = path + "/core/rf_maya/rftool/sim"

# When open UI will reload plugin below.
# ABC
if pm.pluginInfo ( 'AbcExport.mll' , q = True , loaded = True ) == True : pass ;
else : pm.loadPlugin ( 'AbcExport.mll' ) ;

if pm.pluginInfo ( 'AbcImport.mll' , q = True , loaded = True ) == True : pass ;
else : pm.loadPlugin ( 'AbcImport.mll' ) ;

# matrix
if pm.pluginInfo ( 'matrixNodes.mll' , q = True , loaded = True ) == True : pass ;
else : pm.loadPlugin ( 'matrixNodes.mll' ) ;

# yeti
# if pm.pluginInfo ( 'pgYetiMaya.mll' , q = True , loaded = True ) == True : pass ;
# else : pm.loadPlugin ( 'pgYetiMaya.mll' ) ;

# fbx
if pm.pluginInfo ( 'fbxmaya.mll' , q = True , loaded = True ) == True : pass ;
else : pm.loadPlugin ( 'fbxmaya.mll' ) ;

def getUsername ( *args ) :
    if os.environ['RFUSER']:
        return os.environ['RFUSER']
    else:
        return ''

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

def runCfxUI ( ) :
    if pm.window ( 'cfxUI' , exists = True ) :
        pm.deleteUI ( 'cfxUI' ) ;

    window = pm.window( 'cfxUI' , 
        title     = '{title} {version}'.format( title = 'Character Effect Tools', version = 1.0 ),
        mnb       = True ,
        mxb       = False,
        sizeable  = False,
        rtf       = True,
        bgc       = [ 0.15, 0.15, 0.15 ]
        );
    window = pm.window ( 'cfxUI', e = True , width = 502 , h = 10 , menuBar=True) ;

    with window :

        mainLayout = pm.rowColumnLayout ( nc = 1, w = 500, h = 640) ;
        pm.setParent()
        with mainLayout :

            with pm.rowColumnLayout( h = 80,w = 500, nc = 1, columnAlign = (1, 'left'), columnWidth = ( 1, 500 )):
                pm.symbolButton( image = selfPath + '/' + 'image_fortools/cfxUI.jpg' , width = 500 , height = 80 );
                pm.separator( vis = False );

            with pm.rowColumnLayout( h = 5, w = 500, nc = 1, columnWidth = ( 1, 500 )):
                pm.separator( h = 5, st = 'in' );

            with pm.rowColumnLayout ( nc = 4, w = 500 , cw = [ ( 1 , 250 )  , ( 2 , 65 ) , ( 3 , 65 ), ( 3 , 65 ) ] ) :
                pm.separator( vis = False );
                pm.text ( label = 'Username' ) ;
                user = getUsername()
                pm.textField ( 'username_textField' , text = user, editable=False ) ;

            with pm.rowColumnLayout( h = 5, w = 500, nc = 1, columnWidth = ( 1, 500 )):
                pm.separator( h = 5, st = 'in' );

            with pm.rowColumnLayout ( nc = 2 , w = 500 , columnWidth = [ ( 1 , 250 ) , ( 2 , 250 ) ] ) :

                with pm.tabLayout ( 'projects' , innerMarginWidth = 5 , innerMarginHeight = 5 ) : 

                    with pm.rowColumnLayout ( 'Project Data' , nc = 1 , cw = ( 1 , 250 ) ) :
                        import projectData.project_data_v2 as project_data_v2 ;
                        reload ( project_data_v2 ) ;
                        mainProject = 'project_data_v2' 
                        project_data_v2.insert () ;

                    with pm.rowColumnLayout ( 'Dark Horse' , nc = 1 , cw = ( 1 , 250 ) ) :
                        import projectData.project_darkHorse as project_darkHorse ;
                        reload ( project_darkHorse ) ;
                        mainProject = 'Dark Horse'
                        project_darkHorse.insert ( ) ;

                with pm.tabLayout ( 'utilities' , innerMarginWidth = 5 , innerMarginHeight = 5 ) : 

                    with pm.rowColumnLayout ( 'Sim' , nc = 1 , cw = ( 1 , 250 ) ) :
                        import tools.simtools.callSim_v2 as callSim_v2 ;
                        reload ( callSim_v2 ) ;
                        callSim_v2.run ( ) ;

                    with pm.rowColumnLayout ( 'Artist Tool' , nc = 1 , cw = ( 1 , 250 ) ) :
                        import tools.artist_tools.callArtist_tool as callArtist ;
                        reload ( callArtist ) ;
                        callArtist.run ( ) ;

                    with pm.rowColumnLayout ( 'Rig' , nc = 1 , cw = ( 1 , 250 ) ) :
                        import tools.rig.callRig as callRig ;
                        reload ( callRig ) ;
                        callRig.insert ( ) ;

                    with pm.rowColumnLayout ( 'Documents' , nc = 1 , cw = ( 1 , 250 ) ) :

                        with pm.scrollLayout ( w = 250 , h = 500 ) :                    
                            import tools.documents.callResources as callResources ;
                            reload ( callResources ) ;
                            callResources.insert ( ) ;              

    pm.showWindow ( window ) ;
##########################################################################################################################################################################