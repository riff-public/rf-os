import pymel.core as pm ;
import os ;

selfPath = "%s/core/rf_maya/rftool/sim" %os.environ['RFSCRIPT']

#########################################################################################################################################################################

def makeCommand_cmd ( *args ) :

	cmd = '''
import maya.cmds as mc ;
import sys ;

mp = "%s/core/rf_maya/rftool/sim" %os.environ['RFSCRIPT']  ;
if not mp in sys.path :
    sys.path.insert ( 0 , mp ) ;

import library.mainUI.UI1 as mui ;
reload ( mui ) ;

mui.run () ;
''' 

	return cmd ;


def run ( *args ) :


	image_path = self_path + 'image_fortools/icon.png' ;
	print image_path

	cmd = makeCommand_cmd ( ) ;


	mel_cmd = '''
global string $gShelfTopLevel;
string $shelves = `tabLayout -q -selectTab $gShelfTopLevel`;
'''

	currentShelf = pm.mel.eval ( mel_cmd );

	pm.shelfButton ( style = 'iconOnly' , image = image_path , command = cmd , parent = currentShelf ) ;
