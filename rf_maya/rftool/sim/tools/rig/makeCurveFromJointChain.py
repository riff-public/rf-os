import pymel.core as pm ;

##################
''' GENERAL UTILITIES '''
##################

def makeName_cmd ( target = '' , stripLastElm = True , *args ) :
    
    targetSplit_list = target.split ( '_' ) ;
    
    if stripLastElm == True :
        targetSplit_list.remove ( targetSplit_list[-1] ) ;
    else : pass ;

    name = targetSplit_list[0] ;
    
    for each in targetSplit_list[1:] :
        name += ( each[0].upper() + each[1:] ) ;
    
    return name ;


##################
''' RUN '''
##################

def run ( *args ) :

    selection = pm.ls ( sl = True ) ;

    for each in selection :

        nm = makeName_cmd ( target = each ) ;

        joint_list = [] ;
        joint_list.append ( each ) ;
        joint_list.extend ( pm.listRelatives ( each , ad = True ) ) ;
        joint_list.sort ( ) ;

        pos_list = [] ;

        for joint in joint_list :

            pos = pm.xform ( joint , q = True , rp = True , ws = True ) ;  
            pos = tuple ( pos ) ;  
            pos_list.append ( pos ) ;

        pm.curve ( d = 3 , p = pos_list , n = nm + '_CRV' ) ;
