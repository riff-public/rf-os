import pymel.core as pm ;

def run ( *args ) :

    selection = pm.ls ( sl = True ) [0] ;

    selection = pm.rename ( selection , 'curveTransform' ) ;

    shapes = pm.listRelatives ( selection ) ;

    for i in range ( 0 , len ( shapes ) ) :
        group = pm.group ( em = True , w = True , n = 'curve' + str(i) ) ;
        pm.parent ( shapes[i] , group , s = True , r = True ) ;
        pm.rename ( shapes[i] , 'curve' + str(i) + 'Shape' ) ;