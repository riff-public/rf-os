import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

layoutWidth = 250
width = 237.5

############################################### def #####################################################

def jointSizeSlider_cmd ( *args ) :
    value = pm.floatSliderGrp ( 'jointSizeSlider' , q = True , v = True ) ;
    pm.jointDisplayScale ( value ) ;

def printScenePath_cmd ( *args ) :
    print pm.mel.eval ( "file -q -exn" ) ;
    
def geo_moveShapeToCopiedTransform_cmd ( *args ) :
    import tools.rig.geo_moveShapeToCopiedTransform as geo_moveShapeToCopiedTransform ;
    reload ( geo_moveShapeToCopiedTransform ) ;
    geo_moveShapeToCopiedTransform.run ( ) ;

def skAttrShiftUp_cmd ( *args ) :
    import tools.rig.sk_attrShift as ats ;
    reload ( ats ) ;
    ats.shiftUp ( ) ;

def skAttrShiftDown_cmd ( *args ) :
    import tools.rig.sk_attrShift as ats ;
    reload ( ats ) ;
    ats.shiftDown ( ) ;

def createHairJoint_cmd ( *args ) :
    import tools.rig.createHairJoint as createHairJoint ;
    reload ( createHairJoint ) ;
    createHairJoint.createHairJointUI ( ) ;

def makeCurveFromJointChain_cmd ( *args ) :
    import tools.rig.makeCurveFromJointChain as makeCurveFromJointChain ;
    reload ( makeCurveFromJointChain ) ;
    makeCurveFromJointChain.run ( ) ;

def controlMaker_cmd ( *args ) :
    import tools.rig.pk.controlMaker as controlMaker ;
    reload ( controlMaker ) ;
    controlMaker.run ( ) ;

def curveEPMelToPython_cmd ( *args ) :
    import tools.rig.curveEPMelToPython as curveEPMelToPython ;
    reload ( curveEPMelToPython ) ;
    curveEPMelToPython.run ( ) ;

def curveCreator_cmd ( *args ) :
    import tools.rig.curveCreator as curveCreator ;
    reload ( curveCreator ) ;
    curveCreator.run ( ) ;

def curve_separateCurveShape_cmd ( *args ) :
    import tools.rig.curve_separateCurveShape as curve_separateCurveShape ;
    reload ( curve_separateCurveShape ) ;
    curve_separateCurveShape.run ( ) ;

########################################## def space #########################################################

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

##########################################################################################################################################################################################
############################################### main UI #################################################
def insert ( *args ) :

    with pm.scrollLayout ( w = layoutWidth , h = 450 ) :

        with pm.rowColumnLayout ( nc = 1 , w = width ) :

            ##################
            ''' [ GNU_IDX ] GENERAL UTILITIES '''
            ##################

            pm.floatSliderGrp ( 'jointSizeSlider' , label = 'joint size' , field = True , minValue = 0.01 , maxValue = 10 , precision = 2 ,
                columnWidth3 = [ width/5 , width/5 , width/5*3 ] , columnAlign3 = [ "center" , "both" , "left" ] ,
                value = pm.jointDisplayScale ( q = True ) , dc = jointSizeSlider_cmd , cc = jointSizeSlider_cmd ) ;

            pm.button ( label = 'print self path' , c = printScenePath_cmd ) ;
            separator ( ) ;

            pm.button ( label = 'move shape to copied transform' , c = geo_moveShapeToCopiedTransform_cmd ) ;
            pm.text ( label = '#select transform then geometry' , bgc = ( 0.85 , 0.85 , 0.85 ) ) ;
           
            separator ( ) ;

            ##################
            ''' [ ATU_IDX ] ATTRIBUTE UTILITIES '''
            ##################

            with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                pm.button ( label = 'attribute up' , c = skAttrShiftUp_cmd ) ;
                pm.button ( label = 'attribute down' , c = skAttrShiftDown_cmd ) ;

            separator ( ) ;

            ##################
            ''' [ HRU_IDX ] HAIR RIG RELATED UTILITIES '''
            ##################

            pm.button ( label = 'creat hair joint' , c = createHairJoint_cmd ) ;
            pm.button ( label = 'make curve from joint chain' , c = makeCurveFromJointChain_cmd ) ;

            separator ( ) ;

            ##################
            ''' [ CRU_IDX ] CURVE UTILITIES '''
            ##################

            with pm.frameLayout ( label = 'curve utilities' , collapsable = True , collapse = True , bgc = ( 0 , 0 , 0.2 ) ) :

                with pm.rowColumnLayout ( nc = 1 ) :

                    pm.button ( label = 'pk controller maker' , c = controlMaker_cmd ) ;

                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = 'curveEP mel 2 python' , c = curveEPMelToPython_cmd ) ;
                        pm.button ( label = 'curve creator' , c = curveCreator_cmd ) ;

                    pm.button ( label = 'separate curve shapes' , c = curve_separateCurveShape_cmd ) ;
