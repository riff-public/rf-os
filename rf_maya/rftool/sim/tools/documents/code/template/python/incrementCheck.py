import pymel.core as pm ;
import os ;
import re ;

def checkIncrement ( name , path , incrementIncrement = False , *args ) :
    # check for '.xxxx.' e.g. '.0001.'

    file_list = os.listdir ( path ) ;
    increment_list = [] ;

    if not file_list :
        finalIncrement = '0001' ;

    else :
        for file in file_list :
            if name in str ( file ) :
                increment = re.findall ( '.' + '[0-9]{4}' + '.' , file ) ;
                if increment :
                    increment = increment[0] ;
                    increment_list.append ( increment ) ;

        if not increment_list :
            finalIncrement = '0001' ;
        else :
            
            increment_list.sort() ;
            currentIncrement = increment_list[-1] ;
            increment = currentIncrement.split('.')
            for split in increment :
                if split == '' :
                    increment.remove ( split ) ;
            increment = increment[0] ;
            increment = int ( increment ) ;

            if incrementIncrement :
                increment += increment ;

            finalIncrement = str(increment).zfill(4) ;

    return finalIncrement ;