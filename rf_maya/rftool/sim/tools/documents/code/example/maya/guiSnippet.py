### frameLayout ###
pm.frameLayout ( 'example_frameLayout' ,
	label 		= 'Example' ,
	parent 		= mainLayout ,
	width 		= width ,
	collapsable = True ,
	collapse 	= False ,
	bgc 		= ( 0.078 , 0.153 , 0.263 ) ,
	) ;

### rowColumnLayout ###
pm.rowColumnLayout ( nc = 1 , w = width ) ;
pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) ;

### optionMenu ###
with pm.optionMenu( 'simTypeOpt' , label = 'sim type' ) :
	pm.menuItem ( label='replace' ) ;
	pm.menuItem ( label='version' ) ;
	pm.menuItem ( label='subversion' ) ;