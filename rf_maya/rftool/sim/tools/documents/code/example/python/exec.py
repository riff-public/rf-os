### exec ###

text = """
dict = { "key1" : 1 } ;
dict [ "key2" ] = 4 ;
dict [ "key3" ] = 1.5 ;
"""
exec text ;

print ( dict ) ;

### literal_eval ###
# ast.literal_eval raises an exception if the input isn't a valid Python datatype, so the code won't be executed if it's not.

import ast ;

text = '{ "key1" : 3 , "key2" : 7.5 , "key3" : 9 }' ;
dict = ast.literal_eval ( text ) ;

print ( dict ) ;
