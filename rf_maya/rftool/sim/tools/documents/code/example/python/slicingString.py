name 	= 'head' ;
suffix 	= '_ctrl' ;

### "+" method ###
	attribute = name + suffix + '.attribute' ;
	print attribute ; 
    # >>> head_ctrl.attribute

### "%s" method ###
	attribute = '%s%s.attribute' % ( name , suffix ) ;
	print attribute ; 
    # >>> head_ctrl.attribute
	
	repeat = '%s%s%s%s' % ( name , name , name , name ) ;
	print repeat ;
    # >>> headheadheadhead

### "format" method ###
	attribute = '{a}{b}.attribute'.format ( a = name , b = suffix ) ;
	print attribute ;
   	# >>> head_ctrl.attribute

	repeat = '{a}{a}{a}{a}'.format ( a = name ) ;
	print repeat ;
    # >>> headheadheadhead