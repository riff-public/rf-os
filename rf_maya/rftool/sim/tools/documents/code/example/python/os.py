# Checking if a File Exists
os.path.isfile('./file.txt')    # True  
os.path.isfile('./dir')    		# False  

# Checking if a Directory Exists
os.path.isdir('./file.txt')    	# False  
os.path.isdir('./dir')    		# True  

# Checking if Either Exist
os.path.exists('./file.txt')    # True  
os.path.exists('./dir')    		# True  

##########

# make path
os.makedirs ( path ) ;