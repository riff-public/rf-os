import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;
import webbrowser ;

path = os.environ['RFSCRIPT']
layoutWidth = 250
width = 237.5
height     = 500.00

############################################### def #####################################################

def openLink ( link = '' ) :

	webbrowser.open( link ) ;

def openFile_pdf ( file = '' , *args ) :
   
	wp = path + "/core/rf_maya/rftool/sim/tools/Documents/pdf/%s" %file
	
	openLink ( link = wp ) ;

def openFile_web ( file = '' , *args ) :
   
	wp = path + "/core/rf_maya/rftool/sim/tools/Documents/website/%s" %file
	
	openLink ( link = wp ) ;

def open_mayaSettings ( *args ) :
	openFile_pdf ( 'Maya Settings.pdf' ) ;

def openNClothReference ( *args ) :
	openFile_web ( 'nClothReference.html' ) ;

def openNClothATTRIBUTES( *args ) :
	openFile_web ( 'nCloth Dynamic Attributes.html' ) ;

def open_st1_preparingCollider ( *args ) :
	openFile_pdf ( 'Sim Tutorial 1 - Preparing Collider.pdf' ) ;

def open_st2_preparingCurvesFromXGen ( *args ) :
	openFile_pdf ( 'Sim Tutorial 2-2 - Preparing Curves From XGen.pdf' ) ;

def open_st3_preparingHairDynamicSetup ( *args ) :
	openFile_pdf ( 'Sim Tutorial 3-1 - Preparing Hair Dynamic Setup.pdf' ) ;

def open_st4_preparingHairXGenSetup ( *args ) :
	openFile_pdf ( 'Sim Tutorial 3-2 - Preparing Hair XGen Setup.pdf' ) ;

def open_st5_usingNHairOutputCurveToDriveYeti ( *args ) :
	openFile_pdf ( 'Sim Tutorial 3-2 - Using nHair Output Curve to drive Yeti.pdf' ) ;

def completePythonBootcamp_cmd ( *args ) :
	openLink ( link = r'http://nbviewer.jupyter.org/github/jmportilla/Complete-Python-Bootcamp/tree/master/' ) ;

def mayaGUISnippets_cmd ( *args ) :
	openFile_web ( 'mayaGUISnippets.html' ) ;

def mayaColorNames_cmd ( *args ) :
	openFile_web ( 'MolScript v2.1_ Colour names.html' ) ;

def regularExpression_cmd ( *args ) :
	openFile_web ( 'Regular Expressions in Python.html' ) ;

def python_writingCleanCode_link ( *args ) :
	openFile_web ( 'WritingCleanCode_ChadVernon.html' ) ;

def googleSpreadsheetsPython_cmd ( *args ) :
	openFile_web ( 'Google Spreadsheets and Python.html' ) ;

######################################## def for template ####################################################

scriptPath  = path + '/core/rf_maya/rftool/sim/tools/documents/code/'
layout_list = [] 
code_list   = [] 

def getCodeList ( *args  ) :

	type_list = os.listdir ( scriptPath ) ;

	for type in type_list :

		subType_list = os.listdir ( scriptPath + type + '/' ) ;

		layout_list.append ( [ type , subType_list ] ) ;

		for subType in subType_list :
			item_list = os.listdir ( scriptPath + type + '/' + subType + '/' ) ;

			for item in item_list :
				code_list.append ( [ type , subType , item ] ) ;

	return [ layout_list , code_list ] ;

def checkUniqueness ( ui ,*args  ) :
    if pm.window ( ui , exists = True ) :
        pm.deleteUI ( ui ) ;
        checkUniqueness ( ui ) ;

def readTxt ( path ,*args  ) :
    file = open ( path , 'r' ) ;
    text = file.read ( ) ;
    file.close ( ) ;
    return text ;

def show (textPath, title, *args ) :

    textPath   = textPath ;
    text       = readTxt ( textPath ) ;
    ui         = 'textReader_ui' ;
    width      = 500.00 ;
    height     = 500.00 ;
    title      = title ;
    
    checkUniqueness ( ui ) ;

    window = pm.window ( ui , 
    	title       = title ,
        mnb         = True  ,
        mxb         = False ,
        sizeable    = True  ,
        rtf         = True  ,
        ) ;

    pm.window ( window , e = True , w = width , h = height ) ;
    with window :

        main_layout =  pm.formLayout ( ) ;    
        with main_layout :

            textScroll = pm.scrollField ( wordWrap = True , editable = True , text = text ) ;

    pm.formLayout ( main_layout , e = True ,
        attachForm = [
            ( textScroll , 'left'   , 5 ) ,
            ( textScroll , 'right'  , 5 ) ,
            ( textScroll , 'top'    , 5 ) ,
            ( textScroll , 'bottom' , 5 ) ,
            ] ) ;
    
    window.show () ;

########################################## def space #########################################################

def separator ( h = 5 , *args ) :
	pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
	pm.text ( label = ' ' ) ;

##########################################################################################################################################################################################
############################################### DOC #################################################
def insert ( *args ) :

	with pm.rowColumnLayout ( 'Documents' ,  nc = 1 , cw = ( 1 , 250 ) ) :
		with pm.frameLayout ( label = 'Simulation Documents' , collapsable = True , collapse = True , bgc = ( 0 , 0 , 0.5 ) ) :
			with pm.rowColumnLayout ( nc = 1 , w = 250 ) :

				pm.button ( label = 'Maya Settings'             , width = 250 , c = open_mayaSettings, bgc=[0.25, 0.25, 0.25] ) ;

				pm.button ( label = 'nCloth ATTRIBUTES' , width = width , c = openNClothReference, bgc=[0.25, 0.25, 0.25] ) ;

				pm.button ( label = 'nCloth Dynamic ATTRIBUTES 2' , width = width , c = openNClothATTRIBUTES, bgc=[0.25, 0.25, 0.25] ) ;

				with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/10*1 ) , ( 2 , width/10*9 ) ] ) :

					pm.text   ( label = '1' ) ;
					pm.button ( label = 'Preparing Collider'            , w = width/10*9    , c = open_st1_preparingCollider, bgc=[0.25, 0.25, 0.25] ) ;

					pm.text   ( label = '2' ) ;
					pm.button ( label = 'Preparing Curves From XGen'    , w = width/10*9    , c = open_st2_preparingCurvesFromXGen, bgc=[0.25, 0.25, 0.25] ) ;

					pm.text   ( label = '3' ) ;
					pm.button ( label = 'Preparing Hair Dynamic Setup'  , w = width/10*9    , c = open_st3_preparingHairDynamicSetup, bgc=[0.25, 0.25, 0.25] ) ;

					pm.text   ( label = '4' ) ;
					pm.button ( label = 'Preparing Hair XGen Setup'     , w = width/10*9    , c = open_st4_preparingHairXGenSetup, bgc=[0.25, 0.25, 0.25] ) ;

					pm.text   ( label = '5' ) ;
					pm.button ( label = 'Using nHair Output Curve to drive Yeti'            , w = width/10*9    , c = open_st5_usingNHairOutputCurveToDriveYeti, bgc=[0.25, 0.25, 0.25] ) ;
				

			separator ( ) ;   

################################################# Code ######################################################################
		separator ( ) ;
		pm.button ( label = 'Code Color' , w = width , c = mayaColorNames_cmd, bgc=[0.25, 0.25, 0.25] ) ;
		separator ( ) ;
		with pm.frameLayout ( label = 'how to write code (Python & pymel)' , collapsable = True , collapse = True , bgc = ( 0.117647, 0.564706 ,1) ) :
			with pm.rowColumnLayout ( nc = 1 , w = 250 ) :

				pm.button ( label = 'complete python bootcamp (online)' , w = width , bgc = ( 0.95 , 0.85 , 0.5) , c = completePythonBootcamp_cmd ) ;
				pm.button ( label = 'UI Basic' , w = width , c = mayaGUISnippets_cmd, bgc=[0.25, 0.25, 0.25] ) ;
				pm.button ( label = 'Python : Regular Expression' , w = width , c = regularExpression_cmd, bgc=[0.25, 0.25, 0.25] ) ;
				pm.button ( label = 'Python : Writing Clean Code' , w = width , c = python_writingCleanCode_link, bgc=[0.25, 0.25, 0.25] ) ;
				pm.button ( label = 'Python : Google Spreadsheets & Python' , w = width , c = googleSpreadsheetsPython_cmd, bgc=[0.25, 0.25, 0.25] ) ;

################################# template ######################################################################################
				
				code_list = getCodeList()
				layout_list = code_list[0] ;
				text_list   = code_list[1] ;
				mainLayout = pm.rowColumnLayout ( nc = 1 , w = width ) ;

				for layout in layout_list :
					frameLayout = layout[0] ;
					cmd  = "{frameLayout}_frameLayout = ".format ( frameLayout = frameLayout ) ;
					cmd += "pm.frameLayout ( '{frameLayout}_frameLayout' , label = '{frameLayout}'.capitalize() , ".format ( frameLayout = frameLayout ) ;
					cmd += "parent = mainLayout , width = {width} , collapsable = True , collapse = False , bgc = ( 0.078 , 0.153 , 0.263 ) ) ;".format ( width = width ) ;
					exec ( cmd ) ;

					cmd  = "{frameLayout}_layout =".format ( frameLayout = frameLayout ) ;
					cmd += "pm.rowColumnLayout ( nc = 1 , w = width , parent = {frameLayout}_frameLayout ) ;".format ( frameLayout = frameLayout ) ;
					exec ( cmd ) ;

					subLayout_list = layout[1] ;
					for subLayout in subLayout_list :

						cmd  = "{frameLayout}_{subLayout}_mainLayout = ".format ( frameLayout = frameLayout , subLayout = subLayout ) ;
						cmd += "pm.rowColumnLayout ( '{frameLayout}_{subLayout}_mainLayout' ,".format ( frameLayout = frameLayout , subLayout = subLayout ) ;
						cmd += "parent = {frameLayout}_layout , ".format ( frameLayout = frameLayout ) ;
						cmd += "nc = 1 , w = {width} )".format ( width = width ) ;
						exec ( cmd ) ;

						pm.text ( label = subLayout.capitalize() , width = width , height = 25 , bgc = ( 0.144 , 0.243 , 0.459 ) ) ;

						cmd  = ( "{frameLayout}_{subLayout}_layout = ".format ( frameLayout = frameLayout , subLayout = subLayout ) ) ;
						cmd += ( "pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] , " ) ;
						cmd += ( "parent = {frameLayout}_{subLayout}_mainLayout ) ;".format ( frameLayout = frameLayout , subLayout = subLayout ) ) ;
						exec ( cmd ) ;

				for text in text_list :

					frameLayout = text[0] ;
					subLayout   = text[1] ;
					button      = text[2].split('.')[0] ;

					textPath = scriptPath ;
					textPath += text[0] + '/' ;
					textPath += text[1] + '/' ;
					textPath += text[2] ;
					
					cmd = '''
def {button}_call (  *args ) :

	show ( textPath = '{textPath}' , title = '{title}' ) ;

	
'''.format ( scriptPath = scriptPath ,
		button      = button ,
		textPath    = textPath ,
		title       = text[2] ) 

					exec ( cmd ) 

					cmd  = "{button}_btn = ".format ( button = button ) ;
					cmd += "pm.button ( '{button}_btn' , label = '{button}' , w = width/2 , c = {button}_call , ".format ( button = button ) ;
					cmd += "parent = {frameLayout}_{subLayout}_layout ) ;".format ( frameLayout = frameLayout , subLayout = subLayout ) ;
					exec ( cmd ) ; 