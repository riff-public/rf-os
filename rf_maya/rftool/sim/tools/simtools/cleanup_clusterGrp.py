import pymel.core as pm ;

class ClusterGroup ( object ) :
    
    def __init__ ( self ) :
        self.selection = pm.ls ( sl = True ) ;
        self.currentFrame = str ( int ( pm.currentTime ( q = True ) ) ) ;
        
    def lockHideAttr ( self , target ) :
        target = pm.general.PyNode ( target ) ;
        
        for attr in [ 't' , 'r' , 's' ] :
            for axis in [ 'x' , 'y' , 'z' ] :        
                exec ( 'target.{attr}{axis}.set( lock = True , keyable = False) ;'.format ( attr = attr , axis = axis ) ) ;
        target.v.set ( lock = True , keyable = False ) ;
        
    def makeGroup ( self ) :
        group = pm.group ( self.selection ) ;
        group.rename ( 'f{currentFrame}_{index}'.format ( currentFrame = self.currentFrame , index = '1'.zfill(3) ) ) ;
        self.lockHideAttr ( group ) ;
        
        group.addAttr ( 'envelope' , keyable = True , attributeType = 'float' , max = 1 , min = 0 , dv = 1 ) ;
        
        for each in self.selection :
            connections = each.connections() ;
            for connection in connections :
                if connection.nodeType() == 'cluster' :
                    connection = pm.general.PyNode ( connection ) ;
                    group.envelope >> connection.envelope ;
                    
        return ( group ) ;

def run ( *args ) :
    clusterGroup = ClusterGroup() ;
    clusterGroup.makeGroup() ;