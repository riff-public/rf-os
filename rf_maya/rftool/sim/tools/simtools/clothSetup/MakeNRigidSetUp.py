import maya.cmds as mc
import maya.mel as mm

def main(*arg):
    
    if mc.objExists('nRigid_GRP'):
        print 'nRigid_GRP is exists'
    else:
        mc.createNode('transform' , n = 'nRigid_GRP')
    
    NRigidG = 'nRigid_GRP'
    
    sel = mc.ls(sl = True)
    for i in range(len(sel)):
        mm.eval('makeCollideNCloth;')
        ObjWrappedCOLShape = mc.listRelatives(sel[i])[0]
        print ObjWrappedCOLShape
        nRigidShape = mc.listConnections(ObjWrappedCOLShape, type = 'nRigid')[0]
        mc.rename(nRigidShape , sel[i] + '_nRigid')
        mc.setAttr( sel[i] + '_nRigidShape' + '.thickness' , 0.01)
        mc.parent(sel[i] + '_nRigid' , NRigidG)
    
    #nRigidNodeShape =  mc.ls( type = 'nRigid' )
    #nRigidNode = mc.listRelatives(nRigidNodeShape ,p = True)
    #mc.parent(nRigidNode , NRigidG)