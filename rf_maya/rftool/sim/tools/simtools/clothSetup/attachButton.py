import pymel.core as pm
import maya.cmds as cm
import maya.mel as mm

def createLocator (*args) :

    sel = pm.ls(sl = True)

    lenSel = len(sel)
    selObj_shape = sel[0].split('.')
    selObj_pyNode = pm.general.PyNode(selObj_shape[0])
    selObj = selObj_pyNode.getParent()
    dupObj = pm.duplicate(selObj , n = selObj + '_forWrp' )
    print dupObj[0]
    #print selObj
    loc = pm.spaceLocator(n =selObj+ '_loc')


    #print sel;print lenSel
    curveList = []
    for each in range(lenSel) :
        print each
        print sel[1]
        ceate = pm.polyToCurve(sel[each] , form = 2 , degree = 3)
        curveList.append(ceate[0])
        
    loftSel = (curveList[0],curveList[1])

    loftName = pm.loft(loftSel , n = selObj+'_loft')
    pm.delete(curveList)
    print loftName[0]
    print loc ; print loftSel[0]
    print selObj
    pm.select ( loftName[0] , dupObj[0] ) ;
    mc.CreateWrap ( ) ;

    wrap = set ( pm.listConnections ( loftName[0] + 'Shape' , type = 'wrap' ) ) ;
    wrap = list ( wrap ) ;
    wrap = pm.general.PyNode ( wrap [0] ) ;
    pm.rename ( wrap , selObj + '_wrap' ) ;

    wrap.exclusiveBind.set ( 1 ) ;
    wrap.falloffMode.set ( 0 ) ;

    wrapBaseList = pm.listConnections ( wrap ) ;
    print loftName[0]
    folShape = pm.createNode ( 'follicle' ) ;
    fol = pm.listRelatives ( folShape , type = 'transform' , parent = True ) [0] ;

    pm.rename ( fol , selObj + '_fol' ) ;

    folShape.simulationMethod.set ( 0 ) ;

    folShape.outRotate >> fol.r ;
    folShape.outTranslate >> fol.t ;

    loftName[0].worldMatrix[0] >> folShape.inputWorldMatrix ;
    loftName[0].local >> folShape.inputSurface ;

    folShape.parameterU.set ( 0.5 ) ;
    folShape.parameterV.set ( 0.5 ) ;
    dpc = pm.pointConstraint ( fol , loc , mo = False )
    pm.delete(dpc)
    pm.parentConstraint( fol , loc ,mo = True)
    pm.group(fol,loftName[0],dupObj[0] + 'Base', dupObj[0] ,n = 'noTrouch')
    pm.hide('noTrouch')

class Follicle ( object ) :
    
    def __init__ ( self ) :
        pass ;
    
    def __str__ ( self ) :
        pass ;
    
    def __repr__ ( self ) :
        pass ;
     
    def attachfol ( self ,mesh,meshMainShape,dSel) :

        #print meshShape
        
        self.folShape = pm.createNode ( 'follicle' ) 
        self.fol = self.folShape.getParent()

        pm.rename ( self.fol , mesh+'_Fol' )
        pm.parent( self.fol, self.nameGrp )
        
        self.folShape.outRotate >> self.fol.rotate 
        self.folShape.outTranslate >> self.fol.translate
        meshMainShape.worldMatrix  >> self.folShape.inputWorldMatrix  
        meshMainShape.outMesh  >> self.folShape.inputMesh 
    
    def setPosition( self , mesh , meshMain) :
        cpm = pm.createNode('closestPointOnMesh', n='button_'+ mesh +'_cpm')
        #cpm = pm.general.PyNode ('button'+ selMesh +'_cpm') ;
        pm.select(mesh,visible = True)
        cluster = pm.cluster(rel=True , n= 'cluster_'+mesh+'_cpm')
        pos = pm.xform ( cluster , q = True , rp = True , ws = True ) ;
    
        cpm.inPositionX.set (pos[0]) ;
        cpm.inPositionY.set (pos[1]) ;
        cpm.inPositionZ.set (pos[2]) ;
        
        meshMain.outMesh  >> cpm.inMesh 

        # print self.folShape ;
        cpm.result.parameterU >> self.folShape.parameterU
        cpm.result.parameterV >> self.folShape.parameterV
        cpm.result.parameterU // self.folShape.parameterU
        cpm.result.parameterV // self.folShape.parameterV

        pm.delete ( cpm ) ;
        pm.delete ( cluster ) ;

    def selectMeshShape( self ) :
        sel =  pm.ls(selection = True) 
        lenSel = len(sel)
        mesh_num = len(sel)-1
        self.nameGrp = 'Follicle'+sel[mesh_num]+'_Grp'
        if not pm.objExists(self.nameGrp):
            pm.group(em=True, name = self.nameGrp)
        if not pm.objExists('FollicleStill_Grp'):
            pm.group(em=True, name='FollicleStill_Grp')
        #meshList = []

        for each in range(lenSel-1) :
            dupSel = pm.duplicate (sel[each] , n= sel[each]+'_Still')
            print dupSel
            
            ###

            for axis in [ 'x' , 'y' , 'z' ] :
            	for attr in [ 't' , 'r' , 's' ] :
            		exec ( 'dupSel[0].{attr}{axis}.unlock();'.format ( attr = attr , axis = axis ) ) ;

            dupSel[0].t.unlock();
            dupSel[0].r.unlock();
            dupSel[0].s.unlock();
            ### 

            pm.parent( dupSel[0], world=True ) 
            meshMain = pm.general.PyNode ( sel[mesh_num] )
            meshMainShape = pm.general.PyNode ( meshMain )        
            mesh = pm.general.PyNode ( dupSel[0] )
            meshShape = mesh.getShape()
            self.attachfol (mesh,meshMainShape,dupSel) 
            self.setPosition(mesh,meshMain)
            pm.parent( mesh, self.fol )
            print dupSel[0]
            print sel[each]
            pm.select( dupSel[0] , sel[each] )
            pm.blendShape(n=dupSel[0] + '_Bsh' , o ='world' , weight = [ 0 , 1.0 ] )
        pm.parent(self.nameGrp ,'FollicleStill_Grp')
        pm.hide('FollicleStill_Grp')
        
def checkUniqueness ( ui ) :
    if pm.window ( ui , exists = True ) :
        pm.deleteUI ( ui ) ;
        checkUniqueness ( ui ) ;
        
def attachFollicle ( *args ) :
    
    follcleClass = Follicle() ;
    follcleClass.selectMeshShape() ;
    
def freezeTranform() :
    sel = pm.ls(sl = True)
    pm.makeIdentity(sel , t = True , r = True , s = True , n = False , pn = True , apply = True)
    

def gui ( *args ) :
    
    uiName    = 'FiwUI' ;
    width     = 200.00 ;

    checkUniqueness( ui = uiName ) ;        
    window = pm.window ( uiName , title = 'My Script' , mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    pm.window ( uiName , e = True , w = width , h = 1 ) ;
    
    with window :
        
        mainLayout = pm.rowColumnLayout ( nc = 1 , w = width ) ;
        # nc = number of column
        # w = width
        with mainLayout :
            buttonA = pm.button ( label = 'Attach Follicle' , c = attachFollicle , w = width) 
            buttonB = pm.button ( label = 'Freeze Tranform' , c = freezeTranform , w = width )
            buttonB = pm.button ( label = 'Create Locator' , c = createLocator , w = width ) 
            # cw = column width
           
    # rtf = resize to fit children
    window.show() ;
    
# gui()