# move fol position
# help

import pymel.core as pm ;

import tools.simtools.clothSetup.attachFollicles.gui as gui ;
reload ( gui ) ;

class Follicles ( gui.Gui ) :

    def __init__ ( self ) :
        super ( Follicles , self ).__init__() ;

    def composeName ( self , target ) :

        targetSplit = target.split('_') ;

        composedName = targetSplit[0][0].upper() ;
        composedName += targetSplit[0][1:] ;

        for split in targetSplit[1:] :
            composedName += split[0].upper() ;
            composedName += split[1:] ;

        return composedName ;

    def closestPointPos ( self , driver , aim ) :
        driver = pm.PyNode ( driver ) ;
        driverShape = driver.getShape() ;

        # mesh
        if driverShape.nodeType() == 'mesh' :
            cpm = pm.createNode ( 'closestPointOnMesh' ) ;     
            driver.outMesh >> cpm.inMesh ;

            pos = pm.xform ( aim , q = True , rp = True , ws = True ) ;
            for axis , index in zip ( [ 'x' , 'y' , 'z' ] , [ 0 , 1 , 2 ] ) :
                pm.setAttr ( cpm + '.inPosition' + axis.upper() , pos[index] ) ;

            u = cpm.result.parameterU.get() ;
            v = cpm.result.parameterV.get() ;

            pm.delete ( cpm ) ;

        # nurb
        else :
            cos = pm.createNode ( 'closestPointOnSurface' ) ;
            driver.worldSpace[0] >> cos.inputSurface ;

            pos = pm.xform ( aim , q = True , rp = True , ws = True ) ;
            for axis , index in zip ( [ 'x' , 'y' , 'z' ] , [ 0 , 1 , 2 ] ) :
                pm.setAttr ( cpm + '.inPosition' + axis.upper() , pos[index] ) ;
            
            u = cpm.result.parameterU.get() ;
            v = cpm.result.parameterV.get() ;
            
            pm.delete ( cos ) ;
            
        return ( u , v ) ;

    def attachFol ( self , driver , aim = None , pao = False ) :
        # pao = parent aim object

        driver = pm.PyNode ( driver ) ;

        if aim :

            for each in aim :

                each = pm.PyNode ( each ) ;

                driverName  = self.composeName ( driver ) ;
                aimName     = self.composeName ( each ) ; 

                folShape    = pm.createNode ( 'follicle' ) ;
                fol         = folShape.getParent() ;
                fol.rename ( driverName + '_' + aimName + '_Fol' ) ;                
                folShape.simulationMethod.set(0) ;

                folShape.outRotate      >> fol.r ;
                folShape.outTranslate   >> fol.t ;

                driverShape = driver.getShape() ;

                # polygon
                if driverShape.nodeType() == 'mesh' :
                    driver.worldMatrix  >> folShape.inputWorldMatrix ;
                    driver.outMesh      >> folShape.inputMesh ;
                # nurb
                else :
                    driver.worldMatrix[0]   >> folShape.inputWorldMatrix ;
                    driver.local            >> folShape.inputSurface ;

                uv = self.closestPointPos ( driver = driver , aim = each ) ;
                folShape.parameterU.set ( uv[0] ) ;
                folShape.parameterV.set ( uv[1] ) ;

                if pao :
                    pm.parent ( each , fol ) ;

        else :

            driverName  = self.composeName ( driver ) ;

            folShape    = pm.createNode ( 'follicle' ) ;
            fol         = folShape.getParent() ;
            fol.rename ( driverName + '_Fol' ) ;
            folShape.simulationMethod.set(0) ;

            folShape.outRotate      >> fol.r ;
            folShape.outTranslate   >> fol.t ;

            driverShape = driver.getShape() ;

            # polygon
            if driverShape.nodeType() == 'mesh' :
                driver.worldMatrix  >> folShape.inputWorldMatrix ;
                driver.outMesh      >> folShape.inputMesh ;
            # nurb
            else :
                driver.worldMatrix[0]   >> folShape.inputWorldMatrix ;
                driver.local            >> folShape.inputSurface ;

            folShape.parameterU.set ( 0.5 ) ;
            folShape.parameterV.set ( 0.5 ) ;

    def attachFol_cmd ( self , *args ) :

        pao = pm.checkBox ( self.parent_cbx , q = True , v = True ) ;
        selection = pm.ls ( sl = True ) ;

        if selection :
            if len ( selection ) == 1 :
                driver = selection[0] ;
                self.attachFol ( driver ) ;
            else :
                driver = selection[-1] ;
                aim = selection[0:-1] ;
                self.attachFol ( driver , aim , pao = pao ) ;

def run ( *args ) :
    fol = Follicles() ;
    fol.showGui() ;