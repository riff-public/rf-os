######################################################

from __future__ import print_function ;
import os ;
import maya.cmds as mc ;

mp = os.path.realpath (__file__) ;
fileName = os.path.basename (__file__) ;
mp = mp.replace ( fileName , '' ) ;
mp = mp.replace ( '\\' , '/' ) ;

def readTxt ( txt , *args ) :

    file = open ( mp + txt , 'r' ) ;

    for line in file :
        print ( line , end = '' ) ;

def printHelp ( *args ) :

    readTxt ( txt = 'help.txt' ) ;

######################################################

import pymel.core as pm ;

import rig.follicles.prototype.core as foco ;
reload ( foco ) ;


def folAttachNoTgt ( *args ) :
    foco.folAttachNoTgt ( ) ;
    
def folAttach ( *args ) :
    foco.folAttach ( ) ;

def folSnap2Pos ( *arg ) :
    foco.folSnap2Pos ( ) ;

def folCenterSelf ( *arg ) :
    foco.folCenterSelf ( ) ;
    
def folCon ( *arg ) :
    conCBX = pm.checkBox ( 'folConConCBX' , q = True , v = True ) ;
    moCBX = pm.checkBox ( 'polFolConMOCBX' , q = True , v = True ) ;
    foco.folCon ( con = conCBX , mo = moCBX ) ;
        
def folUI ( ) :
    
    # delete window
    if pm.window ( 'folUI' , exists = True ) :
        pm.deleteUI ( 'folUI' ) ;
     
    win = pm.window ( 'folUI' , title = 'follicle utilities' , w = 240 , h = 185 , s = False , mxb = False , mnb = False  ) ;
        ###
    
    formLayout = pm.formLayout( 'formLayout' , w = 240 , h = 185 , nd = 100 ) ;    
        
    mainlayout = pm.rowColumnLayout ( w = 230 , parent = formLayout ) ;
            
    pm.formLayout ( formLayout , e = True , af = (
        ( mainlayout , 'left' , 5 ) , ( mainlayout , 'right' , 5 ) ,
        ( mainlayout , 'top' , 5 ) , ( mainlayout , 'bottom' , 5 ) ,
        ) ) ;
            ###
    
    attachLayout = pm.columnLayout ( 'attachLayout' , p = mainlayout , width = 230 ) ;
                    ###
    attachFolBtn = pm.button ( 'attachFolBtn' , p = attachLayout , label = 'attach follicle(s)' , w = 230 , c = folAttachNoTgt ) ;
    attachFolBtnTgt = pm.button ( 'attachFolBtnTgt' , p = attachLayout , label = 'attach follicle(s) with aim' , w = 230 , c = folAttach ) ;
    
    pm.separator ( p = mainlayout , vis = False ) ; ###
    
    snapLayout = pm.columnLayout ( 'snapLayout' , p = mainlayout , width = 230 ) ;
    
    snapFolBtn = pm.button ( 'snapFolBtn' , p = snapLayout , label = 'snap follicle(s) to aim' , w = 230 , c = folSnap2Pos ) ;
    centerFolBtn = pm.button ( 'centerFolBtn' , p = snapLayout , label = 'center follicle(s)' , w = 230 , c = folCenterSelf ) ;
            ###
    pm.separator ( p = mainlayout , vis = False ) ; ###
    
    folConLayout = pm.columnLayout ( 'folConLayout' , p = mainlayout , width = 230 ) ;
                ###
    folConBtn = pm.button ( 'folConBtn' , p = folConLayout , label = 'follicle constraint' , width = 230 , c = folCon ) ;
    
    folConOptLayout = pm.rowLayout ( 'folConOptLayout' , p = folConLayout , nc = 2 , cw2 = [ 115 , 115 ] ) ;
                    ###
    folConConCBX = pm.checkBox ( 'folConConCBX' , p = folConOptLayout ,  label = 'constraint' , value = True ) ;
    folConMOCBX = pm.checkBox ( 'polFolConMOCBX' , p = folConOptLayout , label = 'maintain offset' , value = True ) ;
    
    pm.separator ( p = mainlayout , vis = False ) ; ###
    
    helpLayout = pm.rowLayout ( 'helpLayout' , p = mainlayout , nc = 2 , cw2 = [ 115 , 115 ] ) ;
    pm.text ( p = helpLayout , l = '' ) ;
    helpBtn = pm.button ( 'helpBtn' , p = helpLayout , label = 'print help' , w = 115 , c = printHelp ) ;

    win.show ( ) ;