import maya.cmds as mc
import maya.mel as mm

def MakeDYNSetUp(*arg):
    sel = mc.ls(sl = True)
    CheckFix = mc.checkBox( FixGRP , q = True , v = True )       
    
    if mc.objExists('DYN_GRP'):
        print 'DYN_GRP is exists'
    else:
        mc.createNode('transform' , n = 'DYN_GRP')
    
    if mc.objExists('FIX_GRP'):
        print 'FIX_GRP is exists'
    else:
        mc.createNode('transform' , n = 'FIX_GRP')
        
    if mc.objExists('nCloth_GRP'):
        print 'nCloth_GRP is exists'
    else:
        mc.createNode('transform' , n = 'nCloth_GRP')
        
   
    for i in range(len(sel)):
        mc.setAttr(sel[i] + '.t' , l=1)
        mc.setAttr(sel[i] + '.r' , l=1)
        mc.setAttr(sel[i] + '.s' , l=1)
        # FIX Geometry #
        if CheckFix == 1:
            FIXDYNObj = mc.duplicate( sel[i] , rc = True)[0]
            FIXObjSplit = FIXDYNObj.split('DYN')
            FIXObj = mc.rename(FIXDYNObj, (FIXObjSplit[0] + 'FIX'))
    
            FIXBSH = mc.blendShape( FIXObj , sel[i] , o = 'world' , n = FIXObj + '_BSN' )
            mc.setAttr( FIXBSH[0] + '.' + FIXObj , 1)
            mc.parent( FIXObj , 'FIX_GRP' )
        else:
            print 'No FIX_GRP'
        """
        # DYN COUT Geometry #
        DYNCOUTObj = mc.duplicate( sel[i] , rc = True)[0]
        COUTObjSplit = DYNCOUTObj.split('1')
        COUTObj = mc.rename(DYNCOUTObj, (COUTObjSplit[0] + '_COUT'))
        """
        # Create NCloth #
        mc.select(sel[i])
        mm.eval('createNCloth 0;')
    
        ObjShape = mc.listRelatives(sel[i])
        nClothList = mc.listConnections(ObjShape[0], type = 'nCloth')
        nClothNode = mc.rename(nClothList[0] , sel[i] + '_nCloth')
        nClothShape = mc.listRelatives(nClothNode)[0]
        conNode = mc.listConnections(nClothShape , t = 'mesh', sh = True)
        for eachNode in conNode:
            if 'outputCloth' in eachNode :
                mc.rename(eachNode,nClothNode.split('_DYN')[0] + '_outputCloth')

        #COUTBSH = mc.blendShape( sel[i] , COUTObj , o = 'world' , n = COUTObj + '_BSN' )
        #mc.setAttr( COUTBSH[0] + '.' + sel[i] , 1)
    
        # Parent #
        mc.parent( sel[i] , 'DYN_GRP' )
        mc.parent( sel[i] + '_nCloth' , 'nCloth_GRP' )

    # Add Layer #
    for i in range(len(sel)-1,-1,-1):
        mc.select(sel[i])
        mc.createDisplayLayer( noRecurse=True, name = sel[i] + '_layer')

if mc.window ('MakeDYNSetUp', q = True, ex = True):
    mc.deleteUI ('MakeDYNSetUp')
    
window = mc.window ('MakeDYNSetUp' , title = "Make DYN SetUp", widthHeight=(100, 150))
mc.columnLayout( adj = True )

FixGRP = mc.checkBox(label = 'Make DYN_FIX' , v = True)
mc.button( label='MakeDYNSetUp' , c = MakeDYNSetUp )

mc.showWindow('MakeDYNSetUp')
mc.window ('MakeDYNSetUp', e = True, w = 250, h = 44, tlc = [400,1100])        