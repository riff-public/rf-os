import sys , os 
import pymel.core as pm 
import maya.cmds as mc 

path = os.environ['RFSCRIPT']
selfPath = path + "/core/rf_maya/rftool/sim"
imagePath = selfPath + '/image_fortools';

############################ def for call in sim #############################

def cometRename_cmd ( *args ) :####### change name
    import tools.simtools.cometRename as cometRename 
    reload ( cometRename ) 
    cometRename.cometRename ( ) 

def customBlendShape_cmd ( *args ) : ########### blendShape auto envelope
    selection = pm.ls ( sl = True ) 
    
    driver = selection [0] 
    driven = selection [1] 
    
    bsh = pm.blendShape ( driver , driven , o = 'world', n = '%s_BSH' % driver ,bf = 1) 

    if ":" in driver:
        dni  = driver.split(":")
        pm.setAttr ( '%s.%s' % ( bsh[0] , dni[1] ) , 1 ) 
        pm.setAttr ( '%s.envelope' % bsh[0] , 1 ) 
    else:

        pm.setAttr ( '%s.%s' % ( bsh[0] , driver ) , 1 ) 
        pm.setAttr ( '%s.envelope' % bsh[0] , 1 ) 

def softCluster_cmd ( *args ) :
    from tools.artist_tools.tool_sam.clusterSl import cluster3
    reload ( cluster3 )

def Cluster_cmd ( *args ) :
    sels = mc.ls(sl=True)

    if '.' in sels[0]:
    
        b = sels[0].split(".")
    
    
        mc.cluster(n = b[0]+'Cluster')
    else:
        mc.cluster(n = sels[0]+'Cluster')

def stickyCluster_cmd(*args):
    from nuTools.rigTools import bpmRig
    # reload(bpmRig)

    stickyCluster = bpmRig.BpmClusterRig()
    stickyCluster.rig()

def makeClusterGrp_call ( *args ) :
    import tools.simtools.cleanup_clusterGrp as cleanup_clusterGrp ;
    reload ( cleanup_clusterGrp ) ;
    cleanup_clusterGrp.run() ;

################################ def setup cloth ################################################
def attachFollicles ( *args ) :
    import tools.simtools.clothSetup.attachFollicles.core as gen_attachFollicles  
    reload ( gen_attachFollicles ) 
    gen_attachFollicles.run() 

def connectVisibility (*args):
    geoGrps = pm.namespaceInfo( lon=True )
    cinGrp = geoGrps[0] + ':Geo_Grp'
    geoGrp = geoGrps[1] + ':Geo_Grp'
    childrenCIN = pm.listRelatives ( cinGrp, ad=True, typ='transform' )
    childrenCOL = pm.listRelatives ( geoGrp, ad=True, typ='transform' )

    for childrenChildCIN, childrenChildCOL in zip( childrenCIN, childrenCOL ):
        driver = childrenChildCIN + '.visibility'
        driven = childrenChildCOL + '.visibility'
        mc.connectAttr(driver, driven)

def duplicateNClothMesh_cmd ( *args ) :
    import tools.simtools.duplicateNClothMesh as duplicateNClothMesh 
    reload ( duplicateNClothMesh ) 
    duplicateNClothMesh.run() 

def makeDYNSetUp_cmd ( *args ) :
    import tools.simtools.clothSetup.MakeDYNSetUp as MakeDYNSetUp 
    reload ( MakeDYNSetUp ) ;

def makeNRigidSetUp_cmd ( *args ) :
    import tools.simtools.clothSetup.MakeNRigidSetUp 
    reload ( MakeNRigidSetUp ) 
    MakeNRigidSetUp.main()

def attachButton_cmd ( *args ) :
    import tools.simtools.clothSetup.attachButton as attachButton
    reload ( attachButton ) ;
    attachButton.attachFollicle() ;

################################ def setup hair ################################################

def hair_qchaircurves_cmd(*args):    
    from rf_utils.pipeline import check
    reload(check)

    sels = pm.selected()
    curr_grp = sels[0].nodeName()
    abc_grp = sels[1].nodeName().split(':')[-1]
    abc_path = str(pm.referenceQuery(sels[1], f=True))
    if not abc_path.endswith('.abc'):
        print 'Only work with .abc reference'
    else:
        checker = check.CustomCheck(curr_grp, abc_grp, abc_path)
        res = checker.check()

def hair_fixCurveShapeName_cmd ( *args ) :
    selection = pm.ls ( sl = True ) 

    for each in selection :
        shape = each.getShape();
        shape.rename ( str(each) + 'Shape' ) 

def hairSetup_rebuildCurveUtil_cmd ( *args ) :
    import tools.simtools.hairSetup.hairSetup_rebuildCurveUtil as hairSetup_rebuildCurveUtil 
    reload ( hairSetup_rebuildCurveUtil ) 
    hairSetup_rebuildCurveUtil.run ( )

def extrudeHairTube_cmd ( *args ) :
    import tools.artist_tools.tool_eye.extrudeHairTube_Edit as eht 
    reload ( eht ) 
    eht.extrudeTube() 

def copyCurveShape ( self , *args ) :
    import tools.simtools.hairSetup.copyCurveShape.core as copyCurveShape 
    reload ( copyCurveShape ) 
    copyCurveShape.run()

def renameCrvOri (*args ):
    import tools.simtools.hairSetup.renameOri as renameOri 
    reload ( renameOri ) 
    renameOri.run() 

############################################ def Utilities #########################################
def simUtilities_cmd ( *args ) :
    import tools.simtools.utilities.sim_simUtilities.core as sim_simUtilities ;
    reload ( sim_simUtilities ) ;
    sim_simUtilities.run ( ) ;

def hairSettingUtilities_cmd ( *args ) :
    import tools.simtools.utilities.hairSettingUtilities as hairSettingUtilities ;
    reload ( hairSettingUtilities ) ;
    hairSettingUtilities.run ( ) ;

########################################### def track ##################################################
def rivet_cmd ( *args ) :
    import tools.rig.rivet as riv ;
    reload ( riv ) ;
    riv.rivet ( ) ;

def Track_cmd ( *args ) :
    from rf_maya.rftool.sim.tools.simtools.track.absoluteTrack import Track
    reload( Track )
    Track.rivetTransform()

################################################ def hair ####################################################
def hair_blendshapeCV_cmd ( *args ) :
    import tools.simtools.hair.hair_blendshapeCV as hair_blendshapeCV ;
    reload ( hair_blendshapeCV ) ;
    hair_blendshapeCV.run ( ) ;

def hair_blendshapePercentage_run ( *args ) :
    import tools.simtools.hair.hair_blendshapePercentage.ui as hair_blendshapePercentage ;
    reload ( hair_blendshapePercentage ) ;
    hair_blendshapePercentage.run ( ) ;

def multipleNucleus_colliderSetUp_cmd ( *args ) :
    import tools.simtools.hair.multipleNucleus_colliderSetUp as multipleNucleus_colliderSetUp ;
    reload ( multipleNucleus_colliderSetUp ) ;
    multipleNucleus_colliderSetUp.run ( ) ;

def tube2ORISelection_cmd ( *args ) :
    import tools.simtools.hair.hair_general as hair_general ;
    reload ( hair_general ) ;
    hair_general.tube2OriSelection_cmd ( ) ;

def attachTfmToCurve ( *args ) :
    import tools.simtools.hair.attachTfmToCurve as attachTfmToCurve ;
    reload ( attachTfmToCurve ) ;
    attachTfmToCurve.run ( ) ;

def nu_BlendShapeCurveNode_cmd( ) :
    gcrvs = pm.ls('Hair*_CRV', type='transform') 
    for gc in gcrvs: 
        name = gc.nodeName() 
        pair = name.replace('_CRV', '_BSH') 
        pairObjs = pm.ls(pair) 
    if pairObjs: 
        pm.connectAttr(gc.worldSpace[0], pariObjs[0].create, f=True) 

def nu_FixCurveUV_cmd( *args ) :
    from nuTools.util import hairCurves as uhc 
    reload(uhc)
    uhc.fix_curves_on_border(fix=True,tol=0.00001, moveDist=0.001)

####################################### def wind #########################################
def ffWind_cmd ( *args ) :
    import tools.simtools.wind.FFWind.connectWind as connectWind ;
    reload ( connectWind ) ;
    connectWind.main ( ) ;

def createWindRig_cmd ( *args ) :
    import tools.simtools.wind.createWindRig as createWindRig ;
    reload ( createWindRig ) ;
    createWindRig.run ( ) ;

def nucleusRig_attach ( *args ) :
    import tools.simtools.wind.nucleusRig.core as gen_nucleusRig ;
    reload ( gen_nucleusRig ) ;
    gen_nucleusRig.attach ( ) ;

def nucleusRig_detach ( *args ) :
    import tools.simtools.wind.nucleusRig.core as gen_nucleusRig ;
    reload ( gen_nucleusRig ) ;
    gen_nucleusRig.detach ( ) ;

def attribute_randomizer_run ( *args ) :
    import tools.simtools.wind.attribute_randomizer as attribute_randomizer ;
    reload ( attribute_randomizer ) ;
    attribute_randomizer.run ( ) ; 

############################# def Cleaning Utilities #################################

def freezeTransform_cmd ( *args ) :
    import tools.simtools.cleaningUtilities.utilities as utilities 
    reload ( utilities ) 
    utilities.freezeTransform_run ( ) 

def deleteHistory_cmd ( *args ) :
    import tools.simtools.cleaningUtilities.utilities as utilities 
    reload ( utilities ) 
    utilities.deleteHistory_run ( ) 

def centerPivot_cmd ( *args ) :
    import tools.simtools.cleaningUtilities.utilities as utilities 
    reload ( utilities ) 
    utilities.centerPivot_run ( ) 

def freezeTransform_deleteHistory_centerPivot_toShelf_cmd ( *args ) :
    import tools.simtools.cleaningUtilities.utilities as utilities 
    reload ( utilities ) 
    utilities.freezeTransform_deleteHistory_centerPivot_toShelf ( ) 

def deleteAbcBlendShape_cmd ( *args ) :
    import tools.simtools.cleaningUtilities.cleanup_deleteAbcBlendShape as cleanup_deleteAbcBlendShape 
    reload ( cleanup_deleteAbcBlendShape ) 
    cleanup_deleteAbcBlendShape.run() 

def renameNObects ( *args ) :
    import tools.simtools.cleaningUtilities.renameNObjects.core as renameNObjects 
    reload ( renameNObjects ) 
    renameNObjects.run()  

def removeNullReferebce_cmd ( *args ) :
    import tools.simtools.cleaningUtilities.cleanup_removeNullReference as cleanup_removeNullReference 
    reload ( cleanup_removeNullReference ) 
    cleanup_removeNullReference.run() 

def findDuplicated_cmd ( *args ) :
    import tools.simtools.cleaningUtilities.cleanup_findDuplicated as cleanup_findDuplicated 
    reload ( cleanup_findDuplicated ) 
    cleanup_findDuplicated.run() 

def unlockSoftenEdge_cmd ( *arg ) :
    import tools.simtools.cleaningUtilities.UnlockSoftenEdge as buse 
    reload ( buse ) 
    buse.main ( ) 

def polyAverageVertexUtil ( *args ) :
    import tools.simtools.cleaningUtilities.polyAverageVertexUtil.core as polyAverageVertexUtil 
    reload ( polyAverageVertexUtil ) 
    polyAverageVertexUtil.run() 

def deleteHierarchyConstraints ( *args ) :
    import tools.simtools.cleaningUtilities.deleteHierarchyConstraints as deleteHierarchyConstraints 
    reload ( deleteHierarchyConstraints ) 
    deleteHierarchyConstraints.run() 

def reOrder_cmd ( *arg ) :
    import tools.simtools.cleaningUtilities.ReOrder as bro 
    reload ( bro ) 
    bro.main ( )    

def deleteRebuiltShape_cmd ( *args ) :
    import tools.simtools.cleaningUtilities.utilities as utilities 
    reload ( utilities ) 
    utilities.deleteRebuiltShape_run ( ) 

def deleteShapeOrig_cmd ( *args ) :
    import tools.simtools.cleaningUtilities.utilities as utilities 
    reload ( utilities ) 
    utilities.deleteShapeOrig_run ( ) 

def deletePolyAV_cmd ( *arg ) :
    import tools.simtools.cleaningUtilities.DeletePolyAV as bdav 
    reload ( bdav ) 
    bdav.main ( )    

#################### def color utilities ########################

def colorAssigner ( *args ) :
    import tools.simtools.colorUtilities.fabulousColor as fabulousColor 
    reload ( fabulousColor ) 
    fabulousColor.run ( ) 

def assignDNshader_cmd ( *args ) :        
    import tools.simtools.colorUtilities.AssignDNshader as badns 
    reload ( badns ) 

def outlinerColorAssigner_cmd ( *args ) :
    import tools.simtools.colorUtilities.outlinerColorAssigner as outlinerColorAssigner 
    reload ( outlinerColorAssigner ) 
    outlinerColorAssigner.run ( ) 

def importRampShader_cmd ( *args ) :
    rampShaderPath = selfPath + '/tools/simtools/colorUtilities/asset/shader_lambertRamp.ma' 
    pm.system.importFile ( rampShaderPath ) 

def importCheckerShader_cmd ( *args ) :
    rampShaderPath = selfPath + '/tools/simtools/colorUtilities/asset/shader_checker.ma' 
    pm.system.importFile ( rampShaderPath ) 

def randomCurveColor_cmd ( *args ) :
    import tools.simtools.colorUtilities.randomCurveColor as randomCurveColor 
    reload ( randomCurveColor ) 
    randomCurveColor.randomCurveColor ( operation = 'enable' ) 

def disableRandomCurveColor_cmd ( *args ) :
    import tools.simtools.colorUtilities.randomCurveColor as randomCurveColor 
    reload ( randomCurveColor ) 
    randomCurveColor.randomCurveColor ( operation = 'disable' ) 
####################################### space ##############################################
def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) 

def filler ( *args ) :
    pm.text ( label = ' ' ) 
###########################################################################################################################################################
layoutWidth = 250
width = 237.5
def run ( *args ) :
###################### main ########################################
    with pm.scrollLayout ( w = layoutWidth , h = 500 ) :

        with pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, width )):
            pm.separator( h = 10, st = 'in' );
                
        pm.separator ( vis = True , h = 5 ); 
        with pm.rowColumnLayout( h = 15, w = width, nc = 4, columnWidth = [( 1, 60 ), ( 2, 60 ), ( 3, 60 ), ( 4, 60 )]):
            pm.text( label = 'Rename', fn = 'smallPlainLabelFont');
            pm.text( label = 'BlendShape', fn = 'smallPlainLabelFont');
            pm.text( label = 'Cluster', fn = 'smallPlainLabelFont');
            pm.text( label = 'Soft Cluster', fn = 'smallPlainLabelFont');

        with pm.rowColumnLayout ( nc = 4, w = width , columnWidth = [ ( 1 , 60 ) , ( 2 , 60 ), ( 3 , 60 ), ( 4, 60 ) ] ) :
            pm.symbolButton( c = cometRename_cmd, image = imagePath + '/rename.png' );
            pm.symbolButton( c = customBlendShape_cmd, image = imagePath + '/blend-tool.png' );
            pm.symbolButton( c = Cluster_cmd, image = imagePath + '/cluster.png' );
            pm.symbolButton( c = softCluster_cmd, image = imagePath + '/softCluster.png' );
        pm.separator ( vis = True , h = 5 ) ;

        with pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, width )):
            pm.separator( h = 10, st = 'in' );

        pm.separator ( vis = True , h = 5 ); 
        with pm.rowColumnLayout( h = 15, w = width, nc = 4, columnWidth = [( 1, 60 ), ( 2, 60 ), ( 3, 60 ), ( 4, 60 )]):
            pm.text( label = 'Sticky Cluster', fn = 'smallPlainLabelFont');
            pm.text( label = 'Cluster GRP', fn = 'smallPlainLabelFont');
            pm.text( label = 'Sim Utils', fn = 'smallPlainLabelFont');
            pm.text( label = 'Hair Utils', fn = 'smallPlainLabelFont');

        with pm.rowColumnLayout ( nc = 4, w = width , columnWidth = [ ( 1 , 60 ) , ( 2 , 60 ), ( 3 , 60 ), ( 4, 60 ) ] ) :
            pm.symbolButton( c = stickyCluster_cmd, image = imagePath + '/stickyCluster.png' );
            pm.symbolButton( c = makeClusterGrp_call, image = imagePath + '/clusterGrp.png' );
            pm.symbolButton( c = simUtilities_cmd, image = imagePath + '/robotics.png' );
            pm.symbolButton( c = hairSettingUtilities_cmd, image = imagePath + '/girl.png' );
        pm.separator ( vis = True , h = 5 ) ;

        with pm.rowColumnLayout( h = 5, w = width, nc = 1, columnWidth = ( 1, width )):
            pm.separator( h = 5, st = 'in' );

        with pm.rowColumnLayout ( nc = 1 , w = width ) :
        ############ cloth setup ################
            # pm.text ( label = 'Setup' )
            with pm.frameLayout ( label = 'Setup Cloth' , collapsable = True , collapse = True , bgc = ( 0.4 , 0.5 , 0.5 ) , width = width ) :
                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :
                    pm.button ( label = 'Attach Follicles' , c = attachFollicles, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'Connect Visibility' , c = connectVisibility, bgc=[0.25, 0.25, 0.25] ) ;

                with pm.frameLayout ( label = 'DYN' , collapsable = True , collapse = False , bgc = ( 0.8 , 0.6 , 0.1 ) ) :
                    with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :
                        pm.button ( label = 'Duplicate NCloth Mesh' , c = duplicateNClothMesh_cmd, bgc=[0.25, 0.25, 0.25] ) ;
                        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                            pm.button ( label = 'Make DYN Setup' , c = makeDYNSetUp_cmd, bgc=[0.25, 0.25, 0.25] ) ;
                            pm.button ( label = 'Make nRigid Setup' , c = makeNRigidSetUp_cmd, bgc=[0.25, 0.25, 0.25] ) ;

                with pm.frameLayout ( label = 'Button' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0.5 ) ) :
                    pm.button ( label = "Fiw's Attach Button" , c = attachButton_cmd, bgc=[0.25, 0.25, 0.25] ) ;
            separator ( ) ;
        ################## hair setup #####################
            with pm.frameLayout ( label = 'Setup Hair' , collapsable = True , collapse = True , bgc = ( 0.4 , 0.5 , 0.5 ) ) :
        
                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :

                    pm.button ( label = "qc hair curves" , c = hair_qchaircurves_cmd , w = width, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = "fix curves' shape name" , c = hair_fixCurveShapeName_cmd , w = width, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'rebuild curve util' , c = hairSetup_rebuildCurveUtil_cmd , w = width, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'extrude hair tube along curve' , w = width , c = extrudeHairTube_cmd, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'Copy Curve Shape' , w = width , c = copyCurveShape, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'rename Crv Ori' , w = 237.5 , c = renameCrvOri ,bgc=[0.25, 0.25, 0.25]) ;

                    with pm.frameLayout ( label = 'YETI' , collapsable = True , collapse = False , bgc = ( 0.7 , 0 , 0.5 ) ) :

                        with pm.rowColumnLayout ( nc = 1 , cw = [ ( 1 , width ) ] ) :

                            import tools.simtools.hairSetup.yeti_setAttraction as yeti_curve_setAttraction ;
                            reload ( yeti_curve_setAttraction ) ;
                            yeti_curve_setAttraction.insert ( width = width ) ;
            separator ( ) ;
        ##################### track #################################
            with pm.frameLayout ( label = 'Track' , collapsable = True , collapse = True , bgc = ( 0.7 , 0.4 , 0.4 ) ) :
        
                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :
                    # pm.button ( label = "rivet transform" , c = june_rivetTransform_cmd  ) ;
                    with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = 'rivet' , c = rivet_cmd, bgc=[0.25, 0.25, 0.25] ) ;
                        pm.button ( label = 'track' , c = Track_cmd, bgc=[0.25, 0.25, 0.25] ) ;
                    
            separator ( ) ;
        #################### hair ######################
            
            with pm.frameLayout ( label = 'Hair' , collapsable = True , collapse = True , bgc = ( 0.5 ,0 ,0.501961) ) :
                
                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :
                    pm.button ( label = 'hair CV blendshape' , c = hair_blendshapeCV_cmd , width = width, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'select ORI from hair tube', c = tube2ORISelection_cmd, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'hair percentage blendshape' , c = hair_blendshapePercentage_run , width = width, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'create multiple nucleus collider' , c = multipleNucleus_colliderSetUp_cmd, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'Attach Tfm to Curve' , c = attachTfmToCurve, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'BlendShape (CurveNode)' , c = nu_BlendShapeCurveNode_cmd, bgc=[0.25, 0.25, 0.25] )
                    pm.button ( label = 'FixCurveUV', c = nu_FixCurveUV_cmd, bgc=[0.25, 0.25, 0.25] )
            separator ( ) ;

        #################### wind utilities #######################
            # pm.text ( label = 'Wind' )
            with pm.frameLayout ( label = 'Wind Utilities' , collapsable = True , collapse = True , bgc = ( 0.2 , 0.3 , 0.3 ) ) :

                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :

                    with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = 'connect wind' , c = ffWind_cmd, bgc=[0.25, 0.25, 0.25] ) ;
                        pm.button ( label = 'create wind rig' , c = createWindRig_cmd, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.text ( label = 'Nucleus Rig' ) ;
                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = 'Create' , c = nucleusRig_attach, bgc=[0.25, 0.25, 0.25] ) ;
                        pm.button ( label = 'Delete' , c = nucleusRig_detach, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'attribute randomizer' , c = attribute_randomizer_run, bgc=[0.25, 0.25, 0.25] ) ;
                    
            separator ( ) ;

        ####################### Cleaning Utilities #####################
            with pm.frameLayout ( label = 'Cleaning Utilities' , collapsable = True , collapse = True , bgc = ( 0.7, 0.3, 0.1 ) ) :

                with pm.rowColumnLayout ( nc = 1 , w = width ) :

                    with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/3 ) , ( 2 , width/3 ) , ( 3 , width/3 ) ] ) :
                        pm.button ( label = 'FT' , c = freezeTransform_cmd , w = width/6*5/3, bgc=[0.25, 0.25, 0.25] ) ;
                        pm.button ( label = 'Hist' , c = deleteHistory_cmd , w = width/6*5/3, bgc=[0.25, 0.25, 0.25] ) ;
                        pm.button ( label = 'CP' , c = centerPivot_cmd , w = width/6*5/3, bgc=[0.25, 0.25, 0.25] ) ;

                    separator ( ) ;

                    pm.button ( label = 'Delete Abc BlendShape' , c = deleteAbcBlendShape_cmd , w = width, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.text ( label = 'select ABC:Geo_Grp then press the button' ) ;

                    separator ( ) ;

                    pm.button ( label = 'Rename nObjects' , c = renameNObects , w = width, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.text ( label = 'supported nObjs: nCloth, nHair, nConstraint' ) ;

                    separator ( ) ;                    

                    pm.button ( label = 'Remove Null Reference' , c = removeNullReferebce_cmd , w = width, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'Find Duplicated' , c = findDuplicated_cmd , w = width, bgc=[0.25, 0.25, 0.25] ) ;

                    separator ( ) ;    

                    pm.button ( label = 'Unlock and Soften Edge' , c = unlockSoftenEdge_cmd , w = width, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'Poly Average Vertex Util' , c = polyAverageVertexUtil , w = width, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'Delete Hierarchy Constraint' , c = deleteHierarchyConstraints , w = width, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'Reorder' , c = reOrder_cmd , w = width, bgc=[0.25, 0.25, 0.25] ) ;

                    separator ( ) ;

                    pm.button ( label = "Delete Curves' Rebuilt Shape" , c = deleteRebuiltShape_cmd , w = width, bgc=[0.25, 0.25, 0.25] ) ;
                    pm.button ( label = 'Delete Orig Shape' , c = deleteShapeOrig_cmd , w = width, bgc=[0.25, 0.25, 0.25] ) ;

                    separator ( ) ;

                    pm.button ( label = 'Delete Average Vertex' , c = deletePolyAV_cmd , w = width, bgc=[0.25, 0.25, 0.25] ) ;

        #################### color utilities ########################
            with pm.frameLayout ( label = 'Color Utilities' , collapsable = True , collapse = True , bgc = ( 0.5, 0.1, 0.1 ) ) :

                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width ) ] ) :

                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = "Kheng's Color" , c = colorAssigner, bgc=[0.25, 0.25, 0.25] ) ;
                        pm.button ( label = "Old Color" , c = assignDNshader_cmd, bgc=[0.25, 0.25, 0.25] ) ;

                    pm.button ( label = 'outliner color assigner' , c = outlinerColorAssigner_cmd, bgc=[0.25, 0.25, 0.25] ) ;
                    
                    separator ( ) ;

                    pm.text ( label = 'import ...' )
                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = 'ramp shaders' , c = importRampShader_cmd, bgc=[0.25, 0.25, 0.25] ) ;
                        pm.button ( label = 'checker shader' , c = importCheckerShader_cmd, bgc=[0.25, 0.25, 0.25] ) ;

                    separator ( ) ;

                    pm.text ( label = "help: select curves' shape then run the script" ) ;
                    with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                        pm.button ( label = 'randomize curve color' , c = randomCurveColor_cmd, bgc=[0.25, 0.25, 0.25] )  ;
                        pm.button ( label = 'disable curve color' , c = disableRandomCurveColor_cmd, bgc=[0.25, 0.25, 0.25] )  ;
            separator ( ) ;