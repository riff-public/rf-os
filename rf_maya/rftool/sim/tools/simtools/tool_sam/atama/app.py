import os
import yaml
import shutil
import maya.cmds as mc
import pymel.core as pm
from PySide2.QtGui import*
from PySide2.QtCore import*
from PySide2.QtWidgets import*

import core
reload(core)
import ui
reload(ui)

supported_video = ['mov','qt','mp4']
class DragDropWidget(QListWidget):
	dropped = Signal(list)
	multipleDropped = Signal(list)
	def __init__(self,*args,**kwargs):
		super(DragDropWidget, self).__init__(*args,**kwargs)
		self.setWordWrap(True)
		self.setAcceptDrops(True)

	def dragEnterEvent(self,event):
		if event.mimeData().hasUrls:
			event.accept()
		else: event.ignore()

	def dragMoveEvent(self,event):
		if event.mimeData().hasUrls:
			event.setDropAction(Qt.CopyAction)
			event.accept()
		else: event.ignore()

	def dropEvent(self,event):
		self.clear()
		if event.mimeData().hasUrls:
			event.setDropAction(Qt.CopyAction)
			event.accept()
			links=[]
			for url in event.mimeData().urls():
				links.append(str(url.toLocalFile()))
			self.multipleDropped.emit(links)
			for link in links:
				if link.split('.')[-1] not in supported_video:
					links.remove(link)
			self.addWidgetItems(links)

	def addWidgetItems(self,items):
		for item in items:
			icon = QIcon(core.icon_path+'/video.png')
			item = QListWidgetItem(item)
			item.setIcon(icon)
			self.addItem(item)

class AtamaProject(QMainWindow,ui.Ui_AtamaProject_MainWindow):
	def __init__(self,parent):
		super(AtamaProject,self).__init__(parent)
		'''info'''
		self.setupUi(self)
		self.setWindowTitle(QApplication.translate("AtamaProject_MainWindow","AtamaProject Tool - %s"%core.version,None,-1))
		'''dragDrop'''
		self.tmpLayout = QHBoxLayout(self)
		self.tmpLayout.setEnabled(False)
		self.dragDrop_widget = DragDropWidget()
		self.dragDrop_widget.setGeometry(QRect(0,0,280,80))
		sizePolicy = QSizePolicy(QSizePolicy.Expanding,QSizePolicy.Minimum)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.dragDrop_widget.sizePolicy().hasHeightForWidth())
		self.dragDrop_widget.setSizePolicy(sizePolicy)
		self.dragDrop_widget.setMinimumSize(QSize(60,60))
		self.dragDrop_widget.setObjectName('dragDrop_widget')
		brush = QBrush()
		brush.setStyle(Qt.BDiagPattern)
		palette = self.dragDrop_widget.palette()
		palette.setBrush(QPalette.Background,brush)
		self.dragDrop_widget.setPalette(palette)
		self.button_dragDropArea = QScrollArea(self.dragDrop_groupBox)
		self.button_dragDropArea.setWidgetResizable(True)
		self.button_dragDropArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
		self.button_dragDropArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
		self.button_dragDropArea.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
		self.button_dragDropArea.setObjectName('button_dragDropArea')
		self.button_dragDropArea.setWidget(self.dragDrop_widget)
		self.dragDrop_layout.addWidget(self.button_dragDropArea)
		'''function'''
		self.createDataPath()
	    # self.createDefaultText(core.local_path)
		self.setDefaultLocal(core.local_path)
		self.refreshWorkspace()
		self.loadData()
		self.refreshYeti()
		self.defaultYeti()
		self.simTimeRange()
		self.yetiTimeRange()
		self.updateSentPath()
		'''localCMD'''
		self.local_pushButton.clicked.connect(self.selectDefault)
		self.local_radioButton.clicked.connect(self.switchRadioButton)
		self.server_radioButton.clicked.connect(self.switchRadioButton)
		self.act_listWidget.itemSelectionChanged.connect(self.loadShot)
		self.autoNaming_pushButton.clicked.connect(self.autoNaming)
		self.autoNaming_pushButton.clicked.connect(self.setProject)
		'''timeRange'''
		self.trWorkspace_pushButton.clicked.connect(self.refreshWorkspace)
		self.trWorkspace_pushButton.clicked.connect(self.simTimeRange)
		self.setTimeRange_pushButton.clicked.connect(self.setTimeRange)
		'''yetiRefresh'''
		self.yetiWorkspace_pushButton.clicked.connect(self.refreshWorkspace)
		self.yetiWorkspace_pushButton.clicked.connect(self.yetiTimeRange)
		self.yetiWorkspace_pushButton.clicked.connect(self.defaultYeti)
		'''yetiCMD'''
		self.yeti_pushButton.clicked.connect(self.refreshYeti)
		self.yetiPath_pushButton.clicked.connect(self.yetiPath)
		self.yetiOpen_pushButton.clicked.connect(self.openYetiPath)
		self.cacheAll_pushButton.clicked.connect(self.cacheYetiAll)
		self.cache_pushButton.clicked.connect(self.cacheYetiSelected)
		'''copyRefresh'''
		self.copyWorkspace_pushButton.clicked.connect(self.updateSentPath)
		'''update'''
		self.dragDrop_widget.itemSelectionChanged.connect(self.updateNamePb)
		'''copyCMD'''
		self.sentPath_pushButton.clicked.connect(self.openSentPath)
		self.copyYeti_pushButton.clicked.connect(self.copyYeti)
		self.copyTech_pushButton.clicked.connect(self.copyTech)
		self.copyPb_pushButton.clicked.connect(self.copyPb)

	def test(self):
		print True

	def createDataPath(self):
		self.createFolder(core.scriptData_path)

	def createDefaultText(self,path):
		if not os.path.exists(path):
			self.writeText(path,'')

	def setDefaultLocal(self,path):
		if os.path.exists(path):
			local = self.readText(path)
			self.local_lineEdit.setText(local)

	def selectDefault(self):
		folder = pm.fileDialog2(cap='Select Folder',fm=3,okc='Select',dir=core.default_path)[0]
		self.local_lineEdit.setText(folder)
		local = self.local_lineEdit.text()
		self.writeText(core.local_path,local)

	def writeText(self,path,object):
		with open(r'{path}'.format(path=path),'w+') as file:
			file.write(object)
			file.close()

	def readText(self,path):
		with open(r'{path}'.format(path=path),'r') as file:
			local = file.read()
			file.close()
		return local

	def createFolder(self,name):
		if not os.path.exists(name):
			os.makedirs(name)
		return(name)

	def openFolder(self,path):
		if os.path.exists(path):
			os.startfile(path)

	def copyfile(self,file,path):
		shutil.copy2(file,path)

	def copyFolder(self,folder,sent_folder,symlink,ignore):
		for item in os.listdir(folder):
			wanted = os.path.join(folder,item)
			sent = os.path.join(sent_folder,item)
			if os.path.exists(sent):
				try:
					shutil.rmtree(sent)
				except: os.unlink(sent)
			if os.path.isdir(wanted):
				shutil.copytree(wanted,sent,symlink,ignore)
			else: shutil.copy2(wanted,sent)

	def updateTimeRange(self):
		durations = []
		workspace = pm.workspace(q=True,rd=True)
		currentAct = workspace.split('/')[2]
		currentShot = workspace.split('/')[3]
		with open(r'{path}'.format(path=core.yaml_file)) as file:
			data = yaml.load(file)
			shots = data['allShots']
			for shot in range(len(shots)):
				if shots[shot][0] == currentAct:
					for each in range(len(shots[shot])):
						if shots[shot][each][0] == currentShot:
							durations.append(shots[shot][each][1])
				else: pass
		time = self.timeRange(durations[0])
		preRoll = core.preRollTime
		start = core.startTime
		end = time[0]
		postRoll = time[1]
		return (preRoll,start,end,postRoll)

	def refreshWorkspace(self):
		workspace = pm.workspace(q=True,rd=True)

	def simTimeRange(self):
		workspace = pm.workspace(q=True,rd=True)
		project = workspace.split('/')[1]
		if project == core.project:
			time = self.updateTimeRange()
			self.preRoll_lineEdit.setText(str(time[0]))
			self.start_lineEdit.setText(str(time[1]))
			self.end_lineEdit.setText(str(time[2]))
			self.postRoll_lineEdit.setText(str(time[3]))
			self.checkTimeRange()
		else:pass

	def timeRange(self,time):
		end = core.startTime + time
		postRoll = end + 10
		return (end,postRoll)

	def checkTimeRange(self,*args):
		currentStart = pm.playbackOptions(q=True,min=True)
		currentEnd = pm.playbackOptions(q=True,max=True)
		timeRange = self.getTimeRange()
		preRoll = timeRange[0]
		start = timeRange[1]
		end = timeRange[2]
		postRoll = timeRange[3]
		if (currentStart==preRoll) and (currentEnd==postRoll):
			self.setTimeRange_pushButton.setStyleSheet('background-color:#99ccff')
		elif (currentStart==start) and (currentEnd==end):
			self.setTimeRange_pushButton.setStyleSheet('background-color:#ff9966')
		else: pass

	def getTimeRange(self,*args):
		preRoll = int(self.preRoll_lineEdit.text())
		start = int(self.start_lineEdit.text())
		end = int(self.end_lineEdit.text())
		postRoll = int(self.postRoll_lineEdit.text())
		return (preRoll,start,end,postRoll)

	def setTimeRange(self):
		currentStart = pm.playbackOptions(q=True,min=True)
		currentEnd = pm.playbackOptions(q=True,max=True)
		timeRange = self.getTimeRange()
		preRoll = timeRange[0]
		start = timeRange[1]
		end = timeRange[2]
		postRoll = timeRange[3]
		if (currentStart==preRoll) and (currentEnd==postRoll):
			pm.playbackOptions(min=start,max=end)
			self.setTimeRange_pushButton.setStyleSheet('background-color:#ff9966')
		elif (currentStart==start) and (currentEnd==end):
			pm.playbackOptions(min=preRoll,max=postRoll)
			self.setTimeRange_pushButton.setStyleSheet('background-color:#99ccff')
		else : 
			pm.playbackOptions(min=preRoll,max=postRoll)
			self.setTimeRange_pushButton.setStyleSheet('background-color:#99ccff')
		self.setStartBsh(preRoll)

	def setStartBsh(self,preRoll):
		if pm.objExists('CIN_COL_BSH'):
			bsn = pm.general.PyNode('CIN_COL_BSH')
			key = pm.listAttr(bsn.w,m=True)
			pm.cutKey(bsn,at=key,o='keys')
			pm.setKeyframe(bsn,at=key,t=preRoll,v=0)
			pm.setKeyframe(bsn,at=key,t=preRoll+10,v=1)
		elif pm.objExists('CIN_GRP_BSH'):
			bsn = pm.general.PyNode('CIN_GRP_BSH')
			key = pm.listAttr(bsn.w,m=True)
			pm.cutKey(bsn,at=key,o='keys')
			pm.setKeyframe(bsn,at=key,t=preRoll,v=0)
			pm.setKeyframe(bsn,at=key,t=preRoll+10,v=1)
		
	def switchRadioButton(self):
		if self.local_radioButton.isChecked():
			self.autoNaming_pushButton.setText('Set Project')
		elif self.server_radioButton.isChecked():
			self.autoNaming_pushButton.setText('Auto Naming')
		self.loadData()

	def loadData(self):
		self.loadAct()
		self.loadShot()

	def loadAct(self):
		default = self.local_lineEdit.text()
		if self.server_radioButton.isChecked():
			self.act_listWidget.clear()
			with open(r'{path}'.format(path=core.yaml_file)) as file:
				data = yaml.load(file)
				shots = data['allShots']
				for shot in range(len(shots)):
					self.act_listWidget.addItem(shots[shot][0])
		elif self.local_radioButton.isChecked():
			self.act_listWidget.clear()
			if os.path.exists(r'{default}{project}'.format(default=default,project=core.project)):
				acts = os.listdir(r'{default}{project}'.format(default=default,project=core.project))
				for act in acts:
					self.act_listWidget.addItem(act)

	def loadShot(self):
		default = self.local_lineEdit.text()
		if not self.act_listWidget.currentItem():
			pass
		else:
			act = self.act_listWidget.currentItem().text()
			if self.server_radioButton.isChecked():
				self.shot_listWidget.clear()
				with open(r'{path}'.format(path=core.yaml_file)) as file:
					data = yaml.load(file)
					shots = data['allShots']
					for shot in range(len(shots)):
						if shots[shot][0] == act:
							for each in range(len(shots[shot])):
								if each == 0: pass
								else:
									if not shots[shot][each][0] == '':
										self.shot_listWidget.addItem(shots[shot][each][0])
			elif self.local_radioButton.isChecked():
				self.shot_listWidget.clear()
				if os.path.exists(r'{default}{project}'.format(default=default,project=core.project)):
					acts = os.listdir(r'{default}{project}/{act}'.format(default=default,project=core.project,act=act))
					for act in acts:
						self.shot_listWidget.addItem(act)

	'''autoNaming'''
	def autoNaming(self):
		if self.autoNaming_pushButton.text() == 'Auto Naming':
			local_path = self.local_lineEdit.text()
			selected_act = self.act_listWidget.currentItem().text()
			selected_shot = self.shot_listWidget.currentItem().text()
			name = '{local}/{project}/{act}/{shot}/{name}/'.format(local=local_path,project=core.project,act=selected_act,shot=selected_shot,name=core.character)
			autoNaming_path = self.createFolder(name)
			file = open(autoNaming_path+'workspace.mel','w+')
			file.close()
			dataFolder = self.createFolder(autoNaming_path+'data')
			versionFolder = self.createFolder(autoNaming_path+'version')
			cacheFolder = self.createFolder(autoNaming_path+'cache')
			self.copySetup(versionFolder)
		else : pass

	def copySetup(self,path):
		files = []
		folders = os.listdir(core.sim_path)
		for folder in folders:
			files.append(core.sim_path+'/{folder}/hero/output'.format(folder=folder))
		for file in files:
			hero = file+'/{file}'.format(file=os.listdir(file)[0])
			self.copyfile(hero,path)

	def setProject(self):
		if self.autoNaming_pushButton.text() == 'Set Project':
			location = self.local_lineEdit.text()
			act = self.act_listWidget.currentItem().text()
			shot = self.shot_listWidget.currentItem().text()
			selectionPath = '{location}/{project}/{act}/{shot}/{char}'.format(location=location,project=core.project,
				act=act,shot=shot,char=core.character)
			try:
				pm.mel.eval('setProject "{path}"'.format(path=selectionPath))
			except:
				pm.error('First,You must autoNaming before use this function')
		else : pass

	def buildSender(self):
		workspace = pm.workspace(q=True,rd=True)
		act = workspace.split('/')[2]
		shot = workspace.split('/')[3]
		'''S:/2022/05_Atama/Maya/cache/01_CFX/ATM_CG_S09toS11_V2'''
		sentPath = core.sent_path+'/{act}/{shot}'.format(act=act,shot=shot)
		'''S:/2022/05_Atama/Maya/cache/01_CFX/ATM_CG_S09toS11_V2/krasue_Hair'''
		self.createFolder(sentPath+'/{char}_Hair'.format(char=core.character))
		'''S:/2022/05_Atama/Maya/cache/01_CFX/ATM_CG_S09toS11_V2/krasue_Tech'''
		self.createFolder(sentPath+'/{char}_Tech'.format(char=core.character))
		'''S:/2022/05_Atama/Maya/cache/01_CFX/ATM_CG_S09toS11_V2/outputMedia'''
		self.createFolder(sentPath+'/outputMedia')

	def cacheYeti(self,yeti):
		pm.mel.eval('pgYetiEnsurePluginLoaded()')
		pm.select(yeti,r=True)
		yeti = yeti.split('_YetiNode')[0]
		'''D:/Atama/ATM_CG_S09toS11_V2/krasue'''
		workspace = pm.workspace(q=True,rd=True)
		act = workspace.split('/')[2]
		shot = workspace.split('/')[3]
		'''D:/Atama/ATM_CG_S09toS11_V2/krasue/cache'''
		location = workspace+'cache'
		'''D:/Atama/ATM_CG_S09toS11_V2/krasue/cache/krasue/krasue_hair'''
		cacheFolder = self.createFolder(location+'/{char}/{char}_{yeti}'.format(char=core.character,yeti=yeti))
		'''D:/Atama/ATM_CG_S09toS11_V2/krasue/cache/krasue/krasue_hair/ATM_CG_S09toS11_V2_krasue_krasue_hair.hero.%04d.fur'''
		outputName = '{cacheFolder}/{act}_{shot}_{char}_{yeti}.hero.%04d.fur'.format(cacheFolder=cacheFolder,act=act,shot=shot,char=core.character,yeti=yeti)
		start = int(self.yetiStart_lineEdit.text())
		end = int(self.yetiEnd_lineEdit.text())
		nos = int(self.yetiNos_lineEdit.text())
		pm.mel.eval('pgYetiCommand -writeCache "%s"'%outputName+' -range %s %s -samples %s'%(start,end,nos))

	def yetiTimeRange(self):
		workspace = pm.workspace(q=True,rd=True)
		project = workspace.split('/')[1]
		if project == core.project:
			time = self.updateTimeRange()
			self.yetiStart_lineEdit.setText(str(int(core.startTime)-5))
			self.yetiEnd_lineEdit.setText(str(time[2]+5))
			self.yetiNos_lineEdit.setText(str(10))
		else:pass

	def cacheYetiSelected(self,*args):
		selected = self.yeti_listWidget.selectedItems()
		for each in selected:
			each = each.text()
			self.cacheYeti(each)

	def cacheYetiAll(self,*args):
		self.yeti_listWidget.selectAll()
		selected = self.yeti_listWidget.selectedItems()
		for each in selected:
			each = each.text()
			self.cacheYeti(each)

	# def copyYetiPath(self):
	# 	workspace = pm.workspace(q=True,rd=True)
	# 	shot = workspace.split('/')[2]
	# 	sentPath = core.sent_path+'/{shot}/{char}_Hair'.format(shot=shot,char=core.character)
	# 	self.yetiPath_label.setText(sentPath)

	def copyYeti(self):
		workspace = pm.workspace(q=True,rd=True)
		sentPath = self.sentPath_label.text()+'/{char}_Hair'.format(char=core.character)
		cacheFolder = workspace+'cache/{char}'.format(char=core.character)
		self.copyFolder(cacheFolder,sentPath,None,None)

	def copyTech(self):
		workspace = pm.workspace(q=True,rd=True)
		sentPath = self.sentPath_label.text()+'/{char}_Tech'.format(char=core.character)
		cacheFolder = workspace+'output/Yeti/{char}_tech/hero'.format(char=core.character)
		self.createFolder(sentPath)
		for item in os.listdir(cacheFolder):
		    cache = os.path.join(cacheFolder,item).replace('\\','/')
		    sent = os.path.join(sentPath,item).replace('\\','/')
		    shutil.copy2(cache,sent)

	def copyPb(self):
		item = QListWidgetItem(self.dragDrop_widget.currentItem())
		path = item.text()
		want_path = self.sentPath_label.text()+'/outputMedia'
		result = self.pb_label.text()
		self.copyfile(path,result)
		self.updateNamePb()

	def checkVersion(self,path):
		if os.path.exists(path):
			if not os.listdir(path)==[]:
				item = os.listdir(path)[-1]
				version = '%03d'%(int(item.split('.')[-2].split('v')[1])+1)
			else:
				version = '%03d'%(int(1))
		return version

	def openSentPath(self):
		path = self.sentPath_label.text()
		self.openFolder(path)

	def updateSentPath(self):
		workspace = pm.workspace(q=True,rd=True)
		project = workspace.split('/')[1]
		act = workspace.split('/')[2]
		shot = workspace.split('/')[3]
		if project == core.project:
			sentPath = core.sent_path+'/{act}/{shot}'.format(act=act,shot=shot)
			self.sentPath_label.setText(sentPath)
		else:pass

	def updateNamePb(self):
		workspace = pm.workspace(q=True,rd=True)
		act = workspace.split('/')[2]
		shot = workspace.split('/')[3]
		name = core.getUserName()
		path = self.sentPath_label.text()+'/outputMedia'
		self.createFolder(path)
		version = self.checkVersion(path)
		name = '{path}/{act}_{shot}_sim_{name}.v{version}.mov'.format(path=path,act=act,shot=shot,name=name,version=version)
		self.pb_label.setText(name)

	def refreshYeti(self):
		self.yeti_listWidget.clear()
		yetis = pm.listRelatives(pm.ls(typ='pgYetiMaya'),p=True)
		for yeti in yetis:
			self.yeti_listWidget.addItem(str(yeti))

	def defaultYeti(self):
		workspace = pm.workspace(q=True,rd=True)
		path = workspace+'cache'
		self.yetiPath_lineEdit.setText(path)

	def yetiPath(self):
		default = self.yetiPath_lineEdit.text()
		folder = pm.fileDialog2(cap='Select Folder',fm=3,okc='Select',dir=default)[0]
		self.yetiPath_lineEdit.setText(folder)

	def openYetiPath(self):
		path = self.yetiPath_lineEdit.text()
		self.openFolder(path)





