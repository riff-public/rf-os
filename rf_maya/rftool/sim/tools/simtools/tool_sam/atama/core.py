import os
import yaml
'''version'''
version = 'beta'
'''info'''
project = 'Atama'
default_path = '/C:'
character = 'krasue'
sim_path = 'P:/Atama/asset/publ/char/krasue/sim'
sent_path = 'S:/2022/05_Atama/Maya/cache/01_CFX'
document_path = os.path.expanduser('~')
script_path = os.path.dirname(__file__).replace('\\','/')
icon_path = script_path + '/icon'
yaml_file = script_path + '/allShots.yaml'
scriptData_path = document_path + '/samScriptData/Atama'
local_path = scriptData_path + '/localPath.txt'

preRollTime = 971
startTime = 1001

'''getData'''
def getUserName(*args):
	if os.environ['RFuser']:
		return os.environ['RFuser'].capitalize()
	else:
		return ''

def getListShots(index):
	with open ( r'path') as file:
		data = yaml.load(file)
	shot = data['allShots'][index][0]
	timeRange = data['allShots'][index][1]
	return shot,timeRange


