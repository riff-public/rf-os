import os ;
import pymel.core as pm ;


selfPath = os.path.realpath (__file__) ;
fileName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( fileName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

cometRenameTxt = open ( selfPath + 'cometRename.txt' , 'r' ) ;
cometRenameCmd = cometRenameTxt.read ( ) ;
cometRenameTxt.close ( ) ;

pm.mel.eval ( cometRenameCmd ) ;

def cometRename ( *args ) :
	pm.mel.eval ( 'cometRename ( ) ;' ) ;