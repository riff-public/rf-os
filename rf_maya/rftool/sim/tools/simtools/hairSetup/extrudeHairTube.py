import pymel.core as pm ;
axes = ( 'x' , 'y' , 'z' ) ;

def checkIfGroup ( selection ) :

    for each in selection :
    
        if pm.nodeType ( each ) != 'transform' : return False ;
        
        else : pass ;
            
        children = pm.listRelatives ( each , c = True ) ;
        
        if children == None : pass ;
        
        else :
            
            for each in children :
                
                if each == None : pass ;
                
                elif pm.nodeType ( each ) != 'transform' : return False ;
                
                else : pass ;
                    
    return True ;                    
            
def extrudeHairTube ( selection ) :
    
    if ( checkIfGroup ( selection = pm.ls ( sl = True ) ) ) != True :
        pm.error ( 'please select curve group(s)' ) ;
    print selection    
    for group in selection :
        
        if '_' in group :
            groupNm = group.split ( '_' ) [0] ;
        else :
            groupNm = group ;

        curveList =  pm.listRelatives ( group , children = True ) ;
              
        baseList = [ ] ;
        
        extrudeList = [ ] ;
        
        for each in curveList:
            
            circle = pm.circle ( n = each.split('_')[0] + '_base' ) [0] ;
            
            baseList.append ( circle ) ;
            
            for axis in axes :
                pm.setAttr ( circle + '.s' + axis , 0.3 ) ;
            
            extrude = pm.extrude ( circle , each , ch = True , rn = False , po = 0 , et = 2 , ucp = 0 , fpt = 0 , upn = 0 , rsp = 1 ) [0] ;
            
            extrudeList.append ( extrude ) ;
        
        pm.group ( baseList , n = groupNm + '_baseGRP' ) ;
        
        pm.group ( extrudeList , n = groupNm + '_tubeGRP' ) ;

        ### connect nodes so that the base follows hair curves
     
        for i in range ( 0 , len ( curveList ) ) :
                
            curve = curveList [i] ;
            curveShape = pm.listRelatives ( curve , shapes = True ) [0] ;
            base = baseList [i] ;
            
            mtp = pm.createNode ( 'motionPath' , n = curve + '_mtp' ) ;
            pm.setAttr ( mtp + '.sideTwist' , 90 ) ;        
            pm.connectAttr ( curveShape + '.worldSpace[0]' , mtp + '.geometryPath' ) ;        
            pm.connectAttr ( mtp + '.rotateX' , base + '.rotateX' ) ;
            pm.connectAttr ( mtp + '.rotateY' , base + '.rotateY' ) ;
            pm.connectAttr ( mtp + '.rotateZ' , base + '.rotateZ' ) ;
            pm.connectAttr ( mtp + '.rotateOrder' , base + '.rotateOrder' ) ;
            pm.connectAttr ( mtp + '.message' , base + '.specifiedManipLocation' ) ;
            
            adlX = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlX' ) ;
            adlY = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlY' ) ;
            adlZ = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlZ' ) ;
            
            pm.connectAttr ( base + '.transMinusRotatePivotX' , adlX + '.input1' ) ;
            pm.connectAttr ( mtp + '.xCoordinate' , adlX + '.input2' ) ;
            pm.connectAttr ( adlX + '.output' , base + '.translateX' ) ;
            
            pm.connectAttr ( base + '.transMinusRotatePivotY' , adlY + '.input1' ) ;
            pm.connectAttr ( mtp + '.yCoordinate' , adlY + '.input2' ) ;
            pm.connectAttr ( adlY + '.output' , base + '.translateY' ) ;
            
            pm.connectAttr ( base + '.transMinusRotatePivotZ' , adlZ + '.input1' ) ;
            pm.connectAttr ( mtp + '.zCoordinate' , adlZ + '.input2' ) ;
            pm.connectAttr ( adlZ + '.output' , base + '.translateZ' ) ;