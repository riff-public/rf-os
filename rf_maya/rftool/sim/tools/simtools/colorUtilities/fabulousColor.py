import maya.cmds as mc ;
import pymel.core as pm 

#Color library
colorList = [
    ['Salmon'       , ( 0.980, 0.502, 0.447 ) ] ,
    ['Rose Bud'     , ( 0.988, 0.729, 0.701 ) ] ,
    ['Red'          , ( 1    , 0    , 0     ) ] ,
    ['Radical Red'  , ( 1.000, 0.458, 0.458 ) ] ,
    ['Burnt Orange' , ( 0.953, 0.443, 0.2   ) ] ,
    ['Hit Pink'     , ( 1.000, 0.698, 0.458 ) ] ,
    ['Orange'       , ( 1    , 0.644, 0     ) ] ,
    ['Golden Tainai', ( 1.000, 0.803, 0.458 ) ] ,
    ['Yellow'       , ( 1    , 1    , 0     ) ] ,
    ['Milan'        , ( 0.980, 0.956, 0.513 ) ] ,
    ['Electric Lime', ( 0.823, 1    , 0     ) ] ,
    ['Honeysuckle'  , ( 0.870, 1.000, 0.298 ) ] ,
    ['Yellow Green' , ( 0.519, 0.812, 0.207 ) ] ,
    ['Feijoa'       , ( 0.670, 0.827, 0.450 ) ] ,
    ['Kelly Green'  , ( 0.275, 0.62 , 0     ) ] ,
    ['Mantis'       , ( 0.478, 0.733, 0.298 ) ] ,
    ['Green' , ( 0    , 0.5  , 0     ) ] ,
    ['Fruit Salad'  , ( 0.298, 0.650, 0.298 ) ] ,
    ['Surfie Green' , ( 0.02 , 0.439, 0.455 ) ] ,
    ['Cadet Blue'   , ( 0.313, 0.607, 0.615 ) ] ,
    ['Boston Blue'  , ( 0.239, 0.569, 0.643 ) ] ,
    ['Glacier'      , ( 0.466, 0.698, 0.749 ) ] ,
    ['Robin Egg Blue',( 0    , 0.8  , 0.8   ) ] ,
    ['Medium Turquoise',(0.388,0.784, 0.811 ) ] ,
    ['Navy Blue'    , ( 0    , 0.4  , 0.8   ) ] ,
    ['Picton Blue'  , ( 0.317, 0.572, 0.807 ) ] ,
    ['Medium Blue'  , ( 0    , 0    , 0.8   ) ] ,
    ['Neon Blue'    , ( 0.360, 0.360, 0.941 ) ] ,
    ['Persian Indigo',( 0.2  , 0    , 0.4   ) ] ,
    ['Ce Soir'      , ( 0.521, 0.360, 0.678 ) ] ,
    ['Purple'       , ( 0.5  , 0    , 0.5   ) ] ,
    ['Violet Blue'  , ( 0.643, 0.305, 0.619 ) ] ,
    ['Magenta'      , ( 1    , 0    , 1     ) ] ,
    ['Fuchsia Pink' , ( 1.000, 0.556, 0.996 ) ] ,
    ['Skin1'        , ( 1    , 0.67 , 0.469 ) ] ,
    ['Skin2'        , ( 1    , 0.685, 0.55  ) ] ,
    ['Skin3'        , ( 0.6  , 0.298, 0.161 ) ] ,
    ['Skin4'        , ( 0.941 , 0.847, 0.828 ) ] ,
    ['Black'        , ( 0    , 0    , 0     ) ] ,
    ['Dim Gray'     , ( 0.25 , 0.25 , 0.25  ) ] ,
    ['Gray'         , ( 0.5  , 0.5  , 0.5   ) ] ,
    ['Sliver'       , ( 0.75 , 0.75 , 0.75  ) ] ,
    ['White'        , ( 1    , 1    , 1     ) ] , 
    ]

basicColorList = [
    ['Basic Red'     , ( 1.00 , 0.40 , 0.40  ) ] ,
    ['Basic Pink'    , ( 1.00 , 0.40 , 0.80  ) ] ,
    ['Basic Purple'  , ( 0.80 , 0.40 , 1.00  ) ] ,
    ['Basic DarkPurple'   , ( 0.50 , 0.40 , 1.00  ) ] ,
    ['Basic Blue'         , ( 0.40 , 0.60 , 1.00  ) ] ,
    ['Basic LightBlue'    , ( 0.40 , 0.90 , 1.00  ) ] ,
    ['Basic LightGreen'   , ( 0.40 , 1.00 , 0.70  ) ] ,
    ['Basic Green'        , ( 0.60 , 1.00 , 0.40  ) ] ,
    ['Basic Yellow'       , ( 1.00 , 0.90 , 0.40  ) ] ,
    ['Basic Orange'       , ( 1.00 , 0.60 , 0.40  ) ] ,
    ['Basin Skin'         , ( 1.00 , 0.685, 0.55  ) ] ,
    ['Black'        , ( 0.00 , 0.00 , 0.00  ) ] ,
    ['Dim Gray'         , ( 0.25 , 0.25 , 0.25  ) ] ,
    ['Gray'         , ( 0.50 , 0.50 , 0.50  ) ] ,
    ['Sliver'    , ( 0.75 , 0.75 , 0.75  ) ] ,
    ['White'        , ( 1.00 , 1.00 , 1.00  ) ] ,
    ]

extraColorList = [
            ['IndianRed'       , ( 0.804    , 0.360    , 0.360    ) ],
            ['LightCoral'      , ( 0.941    , 0.5      , 0.5      ) ],
            ['Salmon'          , ( 0.980    , 0.502    , 0.447    ) ],
            ['DarkSalmon'      , ( 0.913    , 0.588    , 0.478    ) ],
            ['LightSalmon'     , ( 1.000    , 0.627    , 0.478    ) ],
            ['Crimson'         , ( 0.862    , 0.078    , 0.235    ) ],
            ['Red'             , ( 1.000    , 0        , 0        ) ],
            ['FireBrick'       , ( 0.698    , 0.133    , 0.133    ) ],
            ['DarkRed'         , ( 0.545    , 0        , 0        ) ],
            ['Pink'            , ( 1.0      , 0.752    , 0.796    ) ],
            ['LightPink'       , ( 1.0      , 0.713    , 0.756    ) ],
            ['HotPink'         , ( 1.0      , 0.411    , 0.705    ) ],
            ['DeepPink'        , ( 1.0      , 0.0784   , 0.576    ) ],
            ['MediumVioletRed' , ( 0.78039  , 0.08235  , 0.52157  ) ],
            ['PaleVioletRed'   , ( 0.858    , 0.439    , 0.576    ) ],
            ['Coral'           , ( 1.00     , 0.498    , 0.313    ) ],
            ['Tomato'          , ( 1.00     , 0.388    , 0.278    ) ],
            ['Orange Red'      , ( 1.00     , 0.270    , 0        ) ],
            ['DarkOrange'      , ( 1.00     , 0.549    , 0        ) ],
            ['Orange'          , ( 1.000    , 0.644    , 0        ) ],
            ['Gold'            , ( 1.000    , 0.843    , 0        ) ],
            ['Yellow'          , ( 1.000    , 1.000    , 0        ) ],
            ['Light Yellow'    , ( 1.000    , 1.000    , 0.878    ) ],
            ['LemonChiffon'    , ( 1.000    , 0.980    , 0.803    ) ],
            ['LightGolden Rody', ( 0.980    , 0.980    , 0.823    ) ],
            ['Papaya Whip'     , ( 1.000    , 0.937    , 0.835    ) ],
            ['Moccasin'        , ( 1.000    , 0.894    , 0.709    ) ],
            ['PeachPuff'       , ( 1.000    , 0.85490  , 0.725    ) ],
            ['PaleGolden Rod'  , ( 0.933    , 0.909    , 0.666    ) ],
            ['Khaki'           , ( 0.941    , 0.901    , 0.549    ) ],
            ['Dark Khaki'      , ( 0.741    , 0.717    , 0.419    ) ],
            ['Lavender'        , ( 0.901    , 0.901    , 0.980    ) ],
            ['Thistle'         , ( 0.847    , 0.749    , 0.847    ) ],
            ['Plum'            , ( 0.866    , 0.627    , 0.866    ) ],
            ['Violet'          , ( 0.8333   , 0.509    , 0.8333   ) ],
            ['Orcid'           , ( 0.854    , 0.439    , 0.839    ) ],
            ['Magenta'         , ( 1        , 0        , 1        ) ],
            ['Medium Orchid'   , ( 0.729    , 0.333    , 0.827    ) ],
            ['Medium Purple'   , ( 0.576    , 0.439    , 0.858    ) ],
            ['Rebecca Purple'  , ( 0.400    , 0.200    , 0.400    ) ],
            ['BlueViolet'      , ( 0.541    , 0.168    , 0.886    ) ],
            ['Dark Violet'     , ( 0.580    , 0        , 0.827    ) ],
            ['Dark Orchid'     , ( 0.6      , 0.196    , 0.8      ) ],
            ['Dark Magenta'    , ( 0.545    , 0        , 0.545    ) ],
            ['Purple'          , ( 0.5      , 0        , 0.5      ) ],
            ['Indigo'          , ( 0.294    , 0        , 0.519    ) ],
            ['SlateBlue'       , ( 0.415    , 0        , 0.803    ) ],
            ['Dark SlateBlue'  , ( 0.282    , 0.239    , 0.545    ) ],
            ['Medium SlateBlue', ( 0.482    , 0.407    , 0.933    ) ],
            ['Midnight Blue'   , ( 0.098    , 0.098    , 0.439    ) ],
            ['Navy'            , ( 0        , 0        , 0.501    ) ],
            ['Dark Blue'       , ( 0        , 0        , 0.545    ) ],
            ['Medium Blue'     , ( 0        , 0        , 0.8      ) ],
            ['Blue'            , ( 0        , 0        , 1        ) ],
            ['Royal Blue'      , ( 0.254    , 0.411    , 0.882    ) ],
            ['CornFlowerBlue'  , ( 0.392    , 0.584    , 0.929    ) ],
            ['Dodger Blue'     , ( 0.117    , 0.564    , 1        ) ],
            ['Deepsky Blue'    , ( 0        , 0.749    , 1        ) ],
            ['Lightsky Blue'   , ( 0.529    , 0.807    , 0.980    ) ],
            ['SkyBlue'         , ( 0.529    , 0.807    , 0.921    ) ],
            ['Powder Blue'     , ( 0.690    , 0.878    , 0.901    ) ],
            ['LightSteel Blue' , ( 0.690    , 0.768    , 0.870    ) ],
            ['SteelBlue'       , ( 0.274    , 0.509    , 0.705    ) ],
            ['CadetBlue'       , ( 0.372    , 0.619    , 0.627    ) ],
            ['Dark Turquoise'  , ( 0        , 0.807    , 0.819    ) ],
            ['Medium Turquoise', ( 0.388    , 0.784    , 0.811    ) ],
            ['Turquoise'       , ( 0.250    , 0.878    , 0.815    ) ],
            ['Aquamarine'      , ( 0.498    , 1        , 0.831    ) ],
            ['Pale Turquoise'  , ( 0.686    , 0.9333   , 0.9333   ) ],
            ['LightCyan'       , ( 0.878    , 1        , 1        ) ],
            ['Cyan'            , ( 0        , 1        , 1        ) ],
            ['Teal'            , ( 0        , 0.501    , 0.501    ) ],
            ['DarkCyan'        , ( 0        , 0.545    , 0.545    ) ],
            ['LightSeaGreen'   , ( 0.125    , 0.698    , 0.666    ) ],
            ['DarkSeaGreen'    , ( 0.560    , 0.737    , 0.545    ) ],
            ['MediumAquamarine', ( 0.400    , 0.803    , 0.666    ) ],
            ['Dark OliveGreen' , ( 0.334    , 0.419    , 0.184    ) ],
            ['Olive'           , ( 0.501    , 0.501    , 0        ) ],
            ['Olivedrab'       , ( 0.419    , 0.556    , 0.137    ) ],
            ['YellowGreen'     , ( 0.603    , 0.803    , 0.196    ) ],
            ['DarkGreen'       , ( 0.603    , 0.803    , 0.196    ) ],
            ['Green'           , ( 0        , 0.5        , 0      ) ],
            ['Forest Green'    , ( 0.134    , 0.545    , 0.134    ) ],
            ['SeaGreen'        , ( 0.180    , 0.545    , 0.341    ) ],
            ['Medium SeaGreen' , ( 0.235    , 0.701    , 0.443    ) ], 
            ['SpringGreen'     , ( 0        , 1        , 0.498    ) ],
            ['Medium SpringGreen',(0        , 0.980    , 0.603    ) ],
            ['LightGreen'      , ( 0.564    , 0.934    , 0.564    ) ],
            ['Pale Green'      , ( 0.596    , 0.984    , 0.596    ) ],
            ['Lime'            , ( 0        , 1        , 0        ) ],
            ['LawnGreen'       , ( 0.486    , 0.988    , 0        ) ],
            ['Chartreuse'      , ( 0.498    , 1        , 0        ) ],
            ['Green Yellow'    , ( 0.67     , 1        , 0.18     ) ],
            ['CornSilk'        , ( 1        , 0.972    ,0.862     ) ],
            ['Blanche Dalmond' , ( 1        , 0.921    , 0.803    ) ],
            ['Bisque'          , ( 1        , 0.89     , 0.768    ) ],
            ['Nanajo White'    , ( 1        , 0.870    , 0.678    ) ],
            ['Wheat'           , ( 0.960    , 0.870    , 0.701    ) ],
            ['BurlyWood'       , ( 0.870    , 0.721    , 0.529    ) ],
            ['Tan'             , ( 0.823    , 0.705    , 0.549    ) ],
            ['RosyBrown'       , ( 0.737    , 0.560    , 0.560    ) ],
            ['SandyBrown'      , ( 0.956    , 0.643    , 0.376    ) ],
            ['GoldenRod'       , ( 0.854    , 0.647    , 0.125    ) ],
            ['Dark GoldenRod'  , ( 0.721    , 0.525    , 0.043    ) ],
            ['Peru'            , ( 0.803    , 0.521    , 0.247    ) ],
            ['Chocolate'       , ( 0.823    , 0.411    , 0.117    ) ],
            ['SaddleBrown'     , ( 0.545    , 0.270    , 0.0745   ) ],
            ['Sienna'          , ( 0.627    , 0.321    , 0.176    ) ],
            ['Brown'           , ( 0.647    , 0.164    , 0.164    ) ],
            ['Maroon'          , ( 0.501    , 0        , 0        ) ]
    ]
'''Color Assign Shader Scirpt'''



        
def composeName ( node ) :
    
    listOfName = node.split(" ")    
    name = listOfName[0];

    for each in listOfName[1:] :
        name = name.capitalize() + each.capitalize() ;

    return name  ; 

######################################
#BasicColor
for color in basicColorList :
        # ['Rose Quart' , ( 0.969, 0.792, 0.792 ) ]

        name = composeName ( node = color[0] ) ;
        shadeName = '%s'%name +'_Shd'
        code = color[1] ;
        
        cmd = '''
def assignShade{name}_cmd ( *args ) :
    listObj = pm.ls ( sl = True )
    if pm.objExists('{shadeName}'):
        pass
    else :
        create (colorName = '{shadeName}', colorCode = {code} );

    print ('{name}') 
    for selectAssign in listObj :
        pm.select ( str(selectAssign) ) ;
        pm.hyperShade ( assign = '{shadeName}')

    pm.select (listObj)
'''.format ( name = name.capitalize(), shadeName = shadeName, code = code ) ;
        exec ( cmd ) ;

def makeBasicButtons ( w , *args ) :

    for color in basicColorList :
        # ['Rose Quart' , ( 0.969, 0.792, 0.792 ) ]
        name = composeName ( node = color[0] ) ;
        code = color[1] ;
        
        buttonCmd = 'pm.button ( name , label = color[0] , bgc = color[1] , w = w, c = assignShade{name}_cmd ) ;'.format ( name = name.capitalize() ) ;
        exec ( buttonCmd ) ;

####################################################

#PrimeTone
for color in colorList :
        # ['Rose Quart' , ( 0.969, 0.792, 0.792 ) ]

        name = composeName ( node = color[0] ) ;
        shadeName = '%s'%name +'_Shd'
        code = color[1] ;
        
        cmd = '''
def assignShade{name}_cmd ( *args ) :
    listObj = pm.ls ( sl = True )
    if pm.objExists('{shadeName}'):
        pass
    else :
        create (colorName = '{shadeName}', colorCode = {code} );

    print ('{name}') 
    for selectAssign in listObj :
        pm.select ( str(selectAssign) ) ;
        pm.hyperShade ( assign = '{shadeName}')

    pm.select (listObj)
'''.format ( name = name.capitalize(), shadeName = shadeName, code = code ) ;
        exec ( cmd ) ;

def makeButtons ( w , *args ) :

    for color in colorList :
        # ['Rose Quart' , ( 0.969, 0.792, 0.792 ) ]
        name = composeName ( node = color[0] ) ;
        code = color[1] ;
        
        buttonCmd = 'pm.button ( name , label = color[0] , bgc = color[1] , w = w, c = assignShade{name}_cmd ) ;'.format ( name = name.capitalize() ) ;
        exec ( buttonCmd ) ;


#####################################################################
#extraTone
for color in extraColorList :
        # ['Rose Quart' , ( 0.969, 0.792, 0.792 ) ]

        name = composeName ( node = color[0] ) ;
        shadeName = '%s'%name +'_Shd'
        code = color[1] ;
        
        cmd = '''
def assignShade{name}_cmd ( *args ) :
    listObj = pm.ls ( sl = True )
    if pm.objExists('{shadeName}'):
        pass
    else :
        create (colorName = '{shadeName}', colorCode = {code} );

    print ('{name}') 
    for selectAssign in listObj :
        pm.select ( str(selectAssign) ) ;
        pm.hyperShade ( assign = '{shadeName}')

    pm.select (listObj)
'''.format ( name = name.capitalize(), shadeName = shadeName, code = code ) ;
        exec ( cmd ) ;
def makeExtraButtons ( w , *args ) :

    for color in extraColorList :
        # ['Rose Quart' , ( 0.969, 0.792, 0.792 ) ]
        name = composeName ( node = color[0] ) ;
        code = color[1] ;
        
        buttonCmd = 'pm.button ( name , label = " " , bgc = color[1] , w = w, h = 28, c = assignShade{name}_cmd ) ;'.format ( name = name.capitalize() ) ;
        exec ( buttonCmd ) ;


######################################################################
#ExtraColor
def roseQuartColor (*args) :
    listObj = pm.ls ( sl = True )

    if pm.objExists('roseQuart_shd'):
        pass
    else :
        pm.shadingNode ("blinn" ,asShader = True , n = 'roseQuart_shd')
        pm.setAttr ("roseQuart_shd.color", ( 0.969, 0.792, 0.792 ), typ = "double3" )
        pm.setAttr ("roseQuart_shd.eccentricity", 0.6 );
        pm.setAttr ("roseQuart_shd.specularRollOff", 0.3 );
        pm.setAttr ("roseQuart_shd.translucence", 0.1);
        pm.setAttr ("roseQuart_shd.specularColor", (0.15, 0.15, 0.15),typ="double3");

    for greenAssign in listObj :
        pm.select ( str(greenAssign) ) ;
        pm.hyperShade ( assign = 'roseQuart_shd')


def greenScreenColor (*args) :
    listObj = pm.ls ( sl = True )

    if pm.objExists('greenScreen_shd'):
        pass
    else :
        pm.shadingNode ("surfaceShader" ,asShader = True , n = 'greenScreen_shd')
        pm.setAttr ("greenScreen_shd.outColor", (0 , 1 , 0), typ = "double3" )

    for greenAssign in listObj :
        pm.select ( str(greenAssign) ) ;
        pm.hyperShade ( assign = 'greenScreen_shd')

    pm.select (listObj)

def transparencyShader (*args) :
    listObj = pm.ls ( sl = True )

    if pm.objExists('transparencyShade_shd'):
        pass
    else :
        pm.shadingNode ("blinn" ,asShader = True , n = 'transparencyShade_shd')
        pm.setAttr ("transparencyShade_shd.color", (0.75 , 0.75 , 0.75), typ = "double3" )
        pm.setAttr ("transparencyShade_shd.transparency", (0.75 , 0.75 , 0.75), typ ="double3" ) ;
        pm.setAttr ("transparencyShade_shd.eccentricity", 0.6 ) ; 
        pm.setAttr ("transparencyShade_shd.specularRollOff", 0.3 ) ;

    for tranAssign in listObj :
        pm.select ( str(tranAssign) ) ;
        pm.hyperShade ( assign = 'transparencyShade_shd')

    pm.select (listObj)


def create( colorName , colorCode ):
    if pm.objExists('colorName'):
        pass

    else :
        exec ('pm.shadingNode ( "blinn" , asShader = True , n = colorName ) ;')
        exec ('pm.setAttr ( "%s.color" % colorName , colorCode , typ="double3" ) ;' );
        exec ('pm.setAttr ( "%s.eccentricity" % colorName, 0.6 );' )
        exec ('pm.setAttr ("%s.specularRollOff" % colorName, 0.3 );')
        exec ('pm.setAttr ("%s.translucence" % colorName,0.1);')
        exec ('pm.setAttr ("%s.specularColor" % colorName, (0.15, 0.15, 0.15),typ="double3");')

def assignLoopColor ( *args ):
    listObj = pm.ls ( sl = True )
    array = 0
    colorCount = len(colorList) - 8

    # for i in range ( 0 , len(listObj) ) :
    #   print listObj[i] ;

    for targetObj in listObj :
        color = colorList[array] ;
        name = composeName ( node = color[0] )
        cmdAssign = 'assignShade{clName}_cmd()'.format ( clName = name.capitalize() ) ;
        
        pm.select ( targetObj ) ;
        
        exec ( cmdAssign ) ;

        if array > colorCount :
            array = 0 ;
        else :
            array += 1 ;

def deleteUnusedNodes(*args):
    pm.mel.eval('MLdeleteUnused;')

    
def khengColor ( *args ) :
    w = 250.00 
    UIName = "khengColor_UI"
    # check if window exists
    if pm.window ( UIName , exists = True ) :
        pm.deleteUI ( UIName ) ;
    else : pass ;
   
    window = pm.window ( UIName, title = "Window" ,
        mnb = True , mxb = False , sizeable = False , rtf = True ) ;
        
    pm.window ( UIName , e = True , w = w , h = 100 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            with pm.tabLayout (w = w) :
                with pm.rowColumnLayout ('BasicTone' , w = w) :
                    makeBasicButtons (w = w) ;

                with pm.rowColumnLayout ('PrimeTone' ,  w = w ) :
                    pm.button ( name , label = "Rose Quart" , bgc = ( 0.969, 0.792, 0.792 ) , w = w, c = roseQuartColor )
                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/2 ) , ( 2 , w/2 ) ] ) :
                        makeButtons (w = w/2) ;
            

                with pm.rowColumnLayout ( 'ExtraTone' ,nc =6 , cw =[ (1, w/6), (2,w/6 ),(3 , w/6),( 4,w/6),(5,w/6),(6,w/6)]) :
                    makeExtraButtons (w = w/7 ) ;

            mc.separator( width = 50, height=10 , style='out' )
            mc.button ( label ='GreenScreen', c = greenScreenColor , bgc = (0,1,0))
            mc.button ( label ='transparency', c = transparencyShader , bgc = (0.25 ,0.25 ,0.25))
            mc.button ( label ='AssignShadeLoop', c = assignLoopColor )
            mc.button ( label ='Delete Unuse Notes', c = deleteUnusedNodes )
            
    
    window.show () ;

def run (*args):
    khengColor ( ) ;