import pymel.core as pm ;

def disableColor ( *args ) :

    selection = pm.ls ( sl = True ) ;
    
    for each in selection :
        
        target = pm.general.PyNode ( each ) ;
    
        target.useOutlinerColor.set ( 0 ) ;    

def changeColor ( color , *args ) :

    selection = pm.ls ( sl = True ) ;
    
    for each in selection :
        
        target = pm.general.PyNode ( each ) ;
    
        target.useOutlinerColor.set ( 1 ) ;        
        target.outlinerColor.set ( color ) ;    

def green ( *args ) :
    changeColor ( color = ( 0.498039 , 1 , 0 ) ) ;

def yellow ( *args ) :
    changeColor ( color = ( 1 , 1 , 0 ) ) ;
    
def red ( *args ) :
    changeColor ( color = ( 1 , 0 , 0 ) ) ;
    
def pink ( *args ) :
    changeColor ( color = ( 1 , 0.411765 , 0.705882 ) ) ;
    
def purple ( *args ) :
    changeColor ( color = ( 1 , 0 , 1 ) ) ;
    
def lightBlue ( *args ) :
    changeColor ( color = ( 0 , 1 , 1 ) ) ;
    

    
def paleGreen ( *args ) :
    changeColor ( color = ( 0.564706 , 0.933333 , 0.564706 ) ) ;

def paleBlue ( *args ) :
    changeColor ( color = ( 0.529412 , 0.807843 , 0.980392 ) ) ;

def white ( *args ) :
    changeColor ( color = ( 1 , 1 , 1 ) ) ;

def paleRed ( *args ) :
    changeColor ( color = ( 0.803922 , 0.360784 , 0.360784 ) ) ;
    
def palePink ( *args ) :
    changeColor ( color = ( 0.941176 , 0.501961 , 0.501961 ) ) ;
    
def paleYellow ( *args ) :
    changeColor ( color = ( 1 , 0.870588 , 0.678431 ) ) ;
    
def palePurple ( *args ) :
    changeColor ( color = ( 0.866667 , 0.627451 , 0.866667 ) ) ;
    
def run ( *args ) :
    
    # check if window exists
    if pm.window ( 'outlinerColorUI' , exists = True ) :
        pm.deleteUI ( 'outlinerColorUI' ) ;
    else : pass ;
   
    window = pm.window ( 'outlinerColorUI', title = "outliner color assigner" ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'outlinerColorUI' , e = True , w = 200 , h = 100 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , 100 ) , ( 2 , 100 ) ] ) ;
        with mainLayout :
    
            normLayout = pm.rowColumnLayout ( nc = 1 , cw = ( 1 , 100 ) ) ;
            with normLayout :
    
                pm.button ( label = 'disable' , w = 100 , c = disableColor ) ;
        
                pm.button ( label = 'green' , w = 100 , c = green , bgc = ( 0.498039 , 1 , 0 ) ) ;
                pm.button ( label = 'lightBlue' , w = 100 , c = lightBlue , bgc = ( 0 , 1 , 1 ) ) ;
                pm.button ( label = 'yellow' , w = 100 , c = yellow , bgc = ( 1 , 1 , 0 ) ) ;
                pm.button ( label = 'purple' , w = 100 , c = purple , bgc = ( 1 , 0 , 1 ) ) ;
                pm.button ( label = 'red' , w = 100 , c = red , bgc = ( 1 , 0 , 0 ) ) ;
                pm.button ( label = 'pink' , w = 100 , c = pink , bgc = ( 1 , 0.411765 , 0.705882 ) ) ;
            
            paleLayout = pm.rowColumnLayout ( nc = 1 , cw = [ ( 1 , 100 ) ] ) ;
            with paleLayout :
                
                pm.button ( label = 'white' , w = 100 , c = white , bgc = ( 1 , 1 , 1 ) ) ;
                pm.button ( label = 'paleGreen' , w = 100 , c = paleGreen , bgc = ( 0.564706 , 0.933333 , 0.564706 ) ) ;
                pm.button ( label = 'paleBlue' , w = 100 , c = paleBlue , bgc = ( 0.529412 , 0.807843 , 0.980392 ) ) ;
                pm.button ( label = 'paleYellow' , w = 100 , c = paleYellow , bgc = ( 1 , 0.870588 , 0.678431 ) ) ;
                pm.button ( label = 'palePurple' , w = 100 , c = palePurple , bgc = ( 0.866667 , 0.627451 , 0.866667 ) ) ; 
                pm.button ( label = 'paleRed' , w = 100 , c = paleRed , bgc = ( 0.803922 , 0.360784 , 0.360784 ) ) ;
                pm.button ( label = 'palePink' , w = 100 , c = palePink , bgc = ( 0.941176 , 0.501961 , 0.501961 ) ) ;
                
    window.show ( ) ;

run ( ) ;