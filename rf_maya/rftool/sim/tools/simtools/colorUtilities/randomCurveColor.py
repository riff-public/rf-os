import random ;
import pymel.core as pm ;


def randomCurveColor ( operation = 'enable' , *args ) :

    if operation == 'enable' :
        white = 16 ;
        lightYellow = 21 ;
        lightBlue = 18 ;
        lightPink = 19 ;
        skin = 20 ;
        lightGreen = 22 ;
        purple = 30 ;

        color = [ white , lightYellow , lightBlue , lightPink , skin , lightGreen , purple ] ;

        colorRandom = random.SystemRandom ( ) ;

        selection = pm.ls ( sl = True ) ;

        for each in selection : 
            curve = pm.general.PyNode ( each ) ;
            curve.overrideEnabled.set ( 1 ) ;
            curve.overrideColor.set ( colorRandom.choice ( color ) ) ;

    elif operation == 'disable' :

        selection = pm.ls ( sl = True ) ;

        for each in selection : 
            curve = pm.general.PyNode ( each ) ;
            curve.overrideEnabled.set ( 0 ) ;

    else : pm.error ( 'please check operation' )