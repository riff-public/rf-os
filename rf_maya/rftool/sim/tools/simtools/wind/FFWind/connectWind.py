import maya.cmds as mc
import maya.mel as mm
import external.FFWind.winUtility as util
import os ;

selfPath = os.path.realpath (__file__) ;
fileName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( fileName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;
#mp = mp.replace ( mp[-1] , '/' ) ;
selfPath = selfPath[:-1] ;

#mm.eval('source "/proj/ff/sqex/maya2014-x64/scripts/sqvSimulationOutsourcingTool/sub_scripts/sqvOsConnectWind_nCloth2.mel";') #import script from FF
#mm.eval('source "C:/Users/naiwi/Documents/pythonScript/AssetTools/wind/sqvOsConnectWind_nCloth2.mel";')
mm.eval('source "%s/sqvOsConnectWind_nCloth2.mel";' % selfPath ) ;

def updateWindnClothUI():
	nClothNode = mc.ls(typ = 'nCloth')
	mc.textScrollList('shownClothNodeName',e = True )
	mc.textScrollList('shownClothNodeName',e = True ,ra = True ,a = nClothNode)
	
def checkConnections(nClothNode):
	for eachClothShape in nClothNode:
		listCon = mc.listConnections(eachClothShape, p = True , d = True)
		connecttionList = []
		for each in listCon :
			if 'particle_radialField' in each:
				#print 'Has Connection'
				connecttionList.append(each)
			else:
				pass
	#return connecttionList
	if connecttionList == '' or connecttionList == []:
		return False
	else:
		return True
	
def importWind():
	#windPath = 'C:/Users/naiwi/Documents/pythonScript/AssetTools/wind/clothWindSet.v1.ma'
	windPath = '%s/clothWindSet.v1.ma' % selfPath
	rpr = windPath[len(windPath)-1]
	cmd = 'file -import -type "mayaAscii" -mergeNamespacesOnClash false -rpr "'+rpr+'" -options "v=0;" -loadReferenceDepth "none" "'+windPath+'";'
	mm.eval(cmd)
	
def connectNuecleus():
	nue = mc.ls(typ = 'nucleus')
	mm.eval('sqvOsConnectWind_nCloth("nucleus",{"STnClothGrp"},{"'+nue[0]+'"})')

def connectnCloth():
	cmd = 'sqvOsConnectWind_nCloth("particle",{"particle_radialField"},{'
	selectNodeShape = mc.textScrollList('shownClothNodeName',q = True ,si = True)
	sel = []
	for each in selectNodeShape:
		sel.append( mc.listRelatives(each , p = True)[0])
	i = 0
	for eachSel in sel:
		if i == 0 :
			cmd = cmd + '"%s"' %(eachSel)
		else :
			cmd = cmd + ',"%s"' %(eachSel)
		i += 1 
	cmd = cmd + "})"
	print cmd
	mm.eval(cmd)
	
def disconnectnCloth():
	nClothNode = mc.textScrollList('shownClothNodeName',q = True ,si = True)
	for eachClothShape in nClothNode:
		listCon = mc.listConnections(eachClothShape, p = True , d = True)
		connecttionList = []
		for each in listCon :
			if 'particle_radialField' in each:
				#print 'Has Connection'
				connecttionList.append(each)
			else:
				pass
		
		print connecttionList
		for i in connecttionList:
			linkConnection = mc.listConnections(i, c = True,p = True)
			#print linkConnection
			try:
				mc.disconnectAttr(linkConnection[1],linkConnection[0])
				print '##Disconnect Attr : ' + linkConnection[1]+' to '+linkConnection[0]
			except:
				mc.disconnectAttr(linkConnection[0],linkConnection[1])
				print '##Disconnect Attr : ' + linkConnection[0]+' to '+linkConnection[1]
		
		
def selectnCloth():
	selectNodeShape = mc.textScrollList('shownClothNodeName',q = True ,si = True)
	conStat = checkConnections(selectNodeShape)
	if conStat == True:
		mc.text('statusNCloth', e = True ,l = 'Connected',bgc = (0,0.5,0))
		mc.button('ConnectnCloth_BT', e = True,en = 0)
		mc.button('DisconnectnCloth_BT', e = True,en = 1)
	elif conStat == False:
		mc.text('statusNCloth', e = True ,l = 'No Connection',bgc = (0.5,0,0))
		mc.button('ConnectnCloth_BT', e = True,en = 1)
		mc.button('DisconnectnCloth_BT', e = True,en = 0)
		
	selectNode = []
	for each in selectNodeShape:
		selectNode.append( mc.listRelatives(each , p = True)[0].split('_nCloth')[0])
	mc.select(selectNode)
	
def main():
	UIWIDTH = 500
	UIHEIGHT = 300
	startf = mc.playbackOptions( q = True , min = True )
	endf = mc.playbackOptions( q = True , max = True )
	# init UI
	if mc.window('WindTools', q=True ,exists=True):
		mc.deleteUI('WindTools')
	mc.window('WindTools',title="Wind Tool",w = UIWIDTH,h = UIHEIGHT)
	
	masterUI_WindTools = mc.formLayout(numberOfDivisions = 100)
	frame_ImportWinds = mc.frameLayout( label='Import Wind paticle', borderStyle='out' )
	mc.button('importWind_BT', h = 50,l = 'Import Wind' , c = util.Callback(importWind))
	mc.setParent( '..' )
	frame_ConnectWinds = mc.frameLayout( label='Connect Wind paticle', borderStyle='out' )
	mc.setParent( '..' )
	mc.button('ConnectnNucleus_BT', h = 50,l = 'Connect Nucleus', c = util.Callback(connectNuecleus))
	mc.textScrollList('shownClothNodeName',ams = True ,sc = util.Callback(selectnCloth))
	mc.text('statusNClothBar', l = 'Status : ')
	mc.text('statusNCloth', l = 'no select')
	mc.button('ConnectnCloth_BT', h = 50,l = 'Connect nCloth', c = util.Callback(connectnCloth))
	mc.button('DisconnectnCloth_BT', h = 50,l = 'Disconnect nCloth',en = 0,c = util.Callback(disconnectnCloth))
	mc.button('refreshListnCloth_BT', h = 30,l = 'refresh', c = util.Callback(updateWindnClothUI))
	
	# edit UI
	mc.formLayout( masterUI_WindTools, edit=True, af=[(frame_ImportWinds, 'top', 1),(frame_ImportWinds, 'left', 1),(frame_ImportWinds, 'right', 1)
													,(frame_ConnectWinds, 'right', 1),(frame_ConnectWinds, 'left', 1),(frame_ConnectWinds,'bottom',1)
													,('ConnectnNucleus_BT', 'left', 1),('ConnectnNucleus_BT', 'left', 5,),('ConnectnNucleus_BT', 'right', 5,)
													,('shownClothNodeName', 'left', 5),('shownClothNodeName', 'right', 200),('shownClothNodeName', 'bottom', 5)
													,('ConnectnCloth_BT', 'right', 5),('DisconnectnCloth_BT', 'right', 5)
													])
	mc.formLayout( masterUI_WindTools, edit=True, ac=[(frame_ConnectWinds, 'top', 1,frame_ImportWinds),('shownClothNodeName', 'top', 80,frame_ImportWinds)
													,('ConnectnNucleus_BT', 'top', 25,frame_ImportWinds),('ConnectnCloth_BT', 'left', 5,'shownClothNodeName')
													,('statusNClothBar', 'top', 80,frame_ImportWinds),('statusNClothBar', 'left', 5,'shownClothNodeName')
													,('statusNCloth', 'top', 80,frame_ImportWinds),('statusNCloth', 'left', 5,'statusNClothBar')
													,('ConnectnCloth_BT', 'top', 5,'statusNClothBar'),('DisconnectnCloth_BT', 'left', 5,'shownClothNodeName')
													,('DisconnectnCloth_BT', 'top', 5,'ConnectnCloth_BT'),('refreshListnCloth_BT', 'left', 5,'shownClothNodeName')
													,('refreshListnCloth_BT', 'top', 5,'DisconnectnCloth_BT')
													])
													
	#mc.formLayout( masterUI_WindTools, edit=True, ac=[('frame_ImportWinds', 'left', 1,mainColumn)])
	# draw UI
	mc.showWindow('WindTools')
	updateWindnClothUI()
	
#main()
