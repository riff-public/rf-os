//Maya ASCII 2014 scene
//Name: clothWindSet.v1.ma
//Last modified: ��,  2�� 17, 2015 11:10:29 ����
//Codeset: EUC-JP
requires maya "2014";
requires -nodeType "mentalrayFramebuffer" -nodeType "mentalrayOutputPass" -nodeType "mentalrayRenderPass"
		 -nodeType "mentalrayUserBuffer" -nodeType "mentalraySubdivApprox" -nodeType "mentalrayCurveApprox"
		 -nodeType "mentalraySurfaceApprox" -nodeType "mentalrayDisplaceApprox" -nodeType "mentalrayOptions"
		 -nodeType "mentalrayGlobals" -nodeType "mentalrayItemsList" -nodeType "mentalrayShader"
		 -nodeType "mentalrayUserData" -nodeType "mentalrayText" -nodeType "mentalrayTessellation"
		 -nodeType "mentalrayPhenomenon" -nodeType "mentalrayLightProfile" -nodeType "mentalrayVertexColors"
		 -nodeType "mentalrayIblShape" -nodeType "mapVizShape" -nodeType "mentalrayCCMeshProxy"
		 -nodeType "cylindricalLightLocator" -nodeType "discLightLocator" -nodeType "rectangularLightLocator"
		 -nodeType "sphericalLightLocator" -nodeType "abcimport" -nodeType "mia_physicalsun"
		 -nodeType "mia_physicalsky" -nodeType "mia_material" -nodeType "mia_material_x" -nodeType "mia_roundcorners"
		 -nodeType "mia_exposure_simple" -nodeType "mia_portal_light" -nodeType "mia_light_surface"
		 -nodeType "mia_exposure_photographic" -nodeType "mia_exposure_photographic_rev" -nodeType "mia_lens_bokeh"
		 -nodeType "mia_envblur" -nodeType "mia_ciesky" -nodeType "mia_photometric_light"
		 -nodeType "mib_texture_vector" -nodeType "mib_texture_remap" -nodeType "mib_texture_rotate"
		 -nodeType "mib_bump_basis" -nodeType "mib_bump_map" -nodeType "mib_passthrough_bump_map"
		 -nodeType "mib_bump_map2" -nodeType "mib_lookup_spherical" -nodeType "mib_lookup_cube1"
		 -nodeType "mib_lookup_cube6" -nodeType "mib_lookup_background" -nodeType "mib_lookup_cylindrical"
		 -nodeType "mib_texture_lookup" -nodeType "mib_texture_lookup2" -nodeType "mib_texture_filter_lookup"
		 -nodeType "mib_texture_checkerboard" -nodeType "mib_texture_polkadot" -nodeType "mib_texture_polkasphere"
		 -nodeType "mib_texture_turbulence" -nodeType "mib_texture_wave" -nodeType "mib_reflect"
		 -nodeType "mib_refract" -nodeType "mib_transparency" -nodeType "mib_continue" -nodeType "mib_opacity"
		 -nodeType "mib_twosided" -nodeType "mib_refraction_index" -nodeType "mib_dielectric"
		 -nodeType "mib_ray_marcher" -nodeType "mib_illum_lambert" -nodeType "mib_illum_phong"
		 -nodeType "mib_illum_ward" -nodeType "mib_illum_ward_deriv" -nodeType "mib_illum_blinn"
		 -nodeType "mib_illum_cooktorr" -nodeType "mib_illum_hair" -nodeType "mib_volume"
		 -nodeType "mib_color_alpha" -nodeType "mib_color_average" -nodeType "mib_color_intensity"
		 -nodeType "mib_color_interpolate" -nodeType "mib_color_mix" -nodeType "mib_color_spread"
		 -nodeType "mib_geo_cube" -nodeType "mib_geo_torus" -nodeType "mib_geo_sphere" -nodeType "mib_geo_cone"
		 -nodeType "mib_geo_cylinder" -nodeType "mib_geo_square" -nodeType "mib_geo_instance"
		 -nodeType "mib_geo_instance_mlist" -nodeType "mib_geo_add_uv_texsurf" -nodeType "mib_photon_basic"
		 -nodeType "mib_light_infinite" -nodeType "mib_light_point" -nodeType "mib_light_spot"
		 -nodeType "mib_light_photometric" -nodeType "mib_cie_d" -nodeType "mib_blackbody"
		 -nodeType "mib_shadow_transparency" -nodeType "mib_lens_stencil" -nodeType "mib_lens_clamp"
		 -nodeType "mib_lightmap_write" -nodeType "mib_lightmap_sample" -nodeType "mib_amb_occlusion"
		 -nodeType "mib_fast_occlusion" -nodeType "mib_map_get_scalar" -nodeType "mib_map_get_integer"
		 -nodeType "mib_map_get_vector" -nodeType "mib_map_get_color" -nodeType "mib_map_get_transform"
		 -nodeType "mib_map_get_scalar_array" -nodeType "mib_map_get_integer_array" -nodeType "mib_fg_occlusion"
		 -nodeType "mib_bent_normal_env" -nodeType "mib_glossy_reflection" -nodeType "mib_glossy_refraction"
		 -nodeType "mib_illum_hair_x" -nodeType "builtin_bsdf_architectural" -nodeType "builtin_bsdf_architectural_comp"
		 -nodeType "builtin_bsdf_carpaint" -nodeType "builtin_bsdf_ashikhmin" -nodeType "builtin_bsdf_lambert"
		 -nodeType "builtin_bsdf_mirror" -nodeType "builtin_bsdf_phong" -nodeType "contour_store_function"
		 -nodeType "contour_store_function_simple" -nodeType "contour_contrast_function_levels"
		 -nodeType "contour_contrast_function_simple" -nodeType "contour_shader_simple" -nodeType "contour_shader_silhouette"
		 -nodeType "contour_shader_maxcolor" -nodeType "contour_shader_curvature" -nodeType "contour_shader_factorcolor"
		 -nodeType "contour_shader_depthfade" -nodeType "contour_shader_framefade" -nodeType "contour_shader_layerthinner"
		 -nodeType "contour_shader_widthfromcolor" -nodeType "contour_shader_widthfromlightdir"
		 -nodeType "contour_shader_widthfromlight" -nodeType "contour_shader_combi" -nodeType "contour_only"
		 -nodeType "contour_composite" -nodeType "contour_ps" -nodeType "mi_metallic_paint"
		 -nodeType "mi_metallic_paint_x" -nodeType "mi_bump_flakes" -nodeType "mi_car_paint_phen"
		 -nodeType "mi_metallic_paint_output_mixer" -nodeType "mi_car_paint_phen_x" -nodeType "physical_lens_dof"
		 -nodeType "physical_light" -nodeType "dgs_material" -nodeType "dgs_material_photon"
		 -nodeType "dielectric_material" -nodeType "dielectric_material_photon" -nodeType "oversampling_lens"
		 -nodeType "path_material" -nodeType "parti_volume" -nodeType "parti_volume_photon"
		 -nodeType "transmat" -nodeType "transmat_photon" -nodeType "mip_rayswitch" -nodeType "mip_rayswitch_advanced"
		 -nodeType "mip_rayswitch_environment" -nodeType "mip_card_opacity" -nodeType "mip_motionblur"
		 -nodeType "mip_motion_vector" -nodeType "mip_matteshadow" -nodeType "mip_cameramap"
		 -nodeType "mip_mirrorball" -nodeType "mip_grayball" -nodeType "mip_gamma_gain" -nodeType "mip_render_subset"
		 -nodeType "mip_matteshadow_mtl" -nodeType "mip_binaryproxy" -nodeType "mip_rayswitch_stage"
		 -nodeType "mip_fgshooter" -nodeType "mib_ptex_lookup" -nodeType "misss_physical"
		 -nodeType "misss_physical_phen" -nodeType "misss_fast_shader" -nodeType "misss_fast_shader_x"
		 -nodeType "misss_fast_shader2" -nodeType "misss_fast_shader2_x" -nodeType "misss_skin_specular"
		 -nodeType "misss_lightmap_write" -nodeType "misss_lambert_gamma" -nodeType "misss_call_shader"
		 -nodeType "misss_set_normal" -nodeType "misss_fast_lmap_maya" -nodeType "misss_fast_simple_maya"
		 -nodeType "misss_fast_skin_maya" -nodeType "misss_fast_skin_phen" -nodeType "misss_fast_skin_phen_d"
		 -nodeType "misss_mia_skin2_phen" -nodeType "misss_mia_skin2_phen_d" -nodeType "misss_lightmap_phen"
		 -nodeType "misss_mia_skin2_surface_phen" -nodeType "surfaceSampler" -nodeType "mib_data_bool"
		 -nodeType "mib_data_int" -nodeType "mib_data_scalar" -nodeType "mib_data_vector"
		 -nodeType "mib_data_color" -nodeType "mib_data_string" -nodeType "mib_data_texture"
		 -nodeType "mib_data_shader" -nodeType "mib_data_bool_array" -nodeType "mib_data_int_array"
		 -nodeType "mib_data_scalar_array" -nodeType "mib_data_vector_array" -nodeType "mib_data_color_array"
		 -nodeType "mib_data_string_array" -nodeType "mib_data_texture_array" -nodeType "mib_data_shader_array"
		 -nodeType "mib_data_get_bool" -nodeType "mib_data_get_int" -nodeType "mib_data_get_scalar"
		 -nodeType "mib_data_get_vector" -nodeType "mib_data_get_color" -nodeType "mib_data_get_string"
		 -nodeType "mib_data_get_texture" -nodeType "mib_data_get_shader" -nodeType "mib_data_get_shader_bool"
		 -nodeType "mib_data_get_shader_int" -nodeType "mib_data_get_shader_scalar" -nodeType "mib_data_get_shader_vector"
		 -nodeType "mib_data_get_shader_color" -nodeType "user_ibl_env" -nodeType "user_ibl_rect"
		 -nodeType "xgen_geo" -nodeType "xgen_seexpr" -nodeType "xgen_scalar_to_integer" -nodeType "xgen_integer_to_vector"
		 -nodeType "xgen_scalar_to_vector" -nodeType "xgen_boolean_to_vector" -nodeType "xgen_boolean_switch"
		 -nodeType "xgen_tube_normals" -nodeType "xgen_hair_phen" -nodeType "sqtMr3DMotionVectorShader"
		 -nodeType "sqtMrGeoDepthShader" -nodeType "sqtMrGeoMassiveExtShader" -nodeType "sqtMrGeoParticlesExtShader"
		 -nodeType "sqtMrGlossyReflectionShader" -nodeType "sqtMrHairMatteShader" -nodeType "sqtMrHairShader"
		 -nodeType "sqtMrParticlesExtShader" -nodeType "sqtMrVoxelCacheBakeShader" -nodeType "sqtMrVoxelCacheSampleShader"
		 -nodeType "sqtMrVoxelCacheReadShader" -nodeType "sqtMrCameraData" -nodeType "sqtMrMotionVectorShader"
		 -nodeType "sqtMrDepthShader" -nodeType "sqtMrHairMotionVectorShader" -nodeType "sqtMrHairDepthShader"
		 -nodeType "sqtMrGeoHairShader" -nodeType "sqtMrHairExtShader" -nodeType "sqtMrHairMotionVectorExtShader"
		 -nodeType "sqtMrHairDepthExtShader" -nodeType "sqtMrHairMatteExtShader" -nodeType "sqtMrHairTextureShader"
		 -nodeType "sqtMrGeoParticlesShader" -nodeType "sqtMrParticlesShader" -nodeType "sqtMrParticlesTextureShader"
		 -nodeType "sqtMrGeoPathShader" -nodeType "sqtMrBumpShader" -nodeType "sqtMrBumpExtShader"
		 -nodeType "sqtMrTextureRemapShader" -nodeType "sqtMrTextureShader" -nodeType "sqtMrBumpTextureExtShader"
		 -nodeType "sqtMrBumpTextureShader" -nodeType "sqtMrDisplacementTextureShader" -nodeType "sqtMrBumpGradientShader"
		 -nodeType "sqtMrMultiTextureShader" -nodeType "sqtMrMultiTextureExtShader" -nodeType "sqtMrMultiBumpTextureShader"
		 -nodeType "sqtMrMultiBumpTextureExtShader" -nodeType "sqtMrMultiDisplacementTextureShader"
		 -nodeType "sqtMrMultiDisplacementTextureExtShader" -nodeType "sqtMrProjectionTextureShader"
		 -nodeType "sqtMrProjectionBumpTextureShader" -nodeType "sqtMrWaveColorShader" -nodeType "sqtMrColorCompoShader"
		 -nodeType "sqtMrColorLayerShader" -nodeType "sqtMrFresnelShader" -nodeType "sqtMrRaySwitchShader"
		 -nodeType "sqtMrGammaShader" -nodeType "sqtMrVisibilityShader" -nodeType "sqtMrRaySwitchExtShader"
		 -nodeType "sqtMrFrameBufferShader" -nodeType "sqtMrFrameBufferExtShader" -nodeType "sqtMrToneMapSimpleShader"
		 -nodeType "sqtMrSphericalLensShader" -nodeType "sqtMrMaterialShader" -nodeType "sqtMrMaterialExtShader"
		 -nodeType "sqtMrMaterialExtXShader" -nodeType "sqtMrMaterialExtXCompoShader" -nodeType "sqtMrMatteShader"
		 -nodeType "sqtMrMatteExtShader" -nodeType "sqtMrTranslucenceShader" -nodeType "sqtMrAllInOneShader"
		 -nodeType "sqtMrCameraTemperatureShader" -nodeType "sqtMrCurvatureShader" -nodeType "sqtMrShadowShader"
		 -nodeType "sqtMrPhotonShader" -nodeType "sqtMrLightShader" -nodeType "sqtMrLightExtShader"
		 -nodeType "sqtMrAreaLightShader" -nodeType "sqtMrAreaLightExtShader" -nodeType "sqtMrEnvironmentShader"
		 -nodeType "sqtMrEnvironmentExtShader" -nodeType "sqtMrEnvironmentSwitchShader" -nodeType "sqtMrMultiEnvironmentShader"
		 -nodeType "sqtMrEnvironmentSwitchExtShader" -nodeType "sqtMrShadowMaskShader" -nodeType "sqtMrDepthLightmapShader"
		 -nodeType "sqtMrLightFogShader" -nodeType "sqtMrVolumeSimpleShader" -nodeType "sqtMrVolumeShader"
		 -nodeType "sqtMrVolumeBufferedShader" -nodeType "sqtMrSSSLightmapShader" -nodeType "sqtMrSSSLightmapExtShader"
		 -nodeType "sqtMrSSSShader" -nodeType "sqtMrSSSExtShader" -nodeType "sqtMrSSSSimpleLightmapShader"
		 -nodeType "sqtMrSSSSimpleLightmapExtShader" -nodeType "sqtMrLeatherShader" -nodeType "sqtMrTextileShader"
		 -nodeType "sqtMrFlakeShader" -nodeType "sqtMrDOFDataShader" -nodeType "sqtMrHairDOFDataShader"
		 -nodeType "sqtMrObjectData" -nodeType "sqtMrColorVariationShader" -nodeType "sqtMrColorCorrectionShader"
		 -nodeType "sqtMrParallaxShader" -nodeType "sqtMrParallaxSimpleShader" -nodeType "sqtMrDirtShader"
		 -nodeType "sqtMrColorCorrectionCompoShader" -nodeType "sqtMrColorCorrectionExtShader"
		 -nodeType "sqtMrScatterShader" -nodeType "sqtMrTranslucenceExtShader" -nodeType "sqtMrLightBlockerShader"
		 -nodeType "sqtMrSimplePassesShader" -nodeType "sqtMrDebugPassesShader" -nodeType "sqtMrFluidBumpTextureShader"
		 -nodeType "sqtMrColorConversionShader" -nodeType "sqtMrFlowShader" -nodeType "sqtMrSurfaceFractalShader"
		 -nodeType "sqtMrRippleShader" -nodeType "sqtMrMiaCompatibilityShader" -nodeType "sqtMrColorCompoCompatibilityShader"
		 -nodeType "realflowMeltShaderMr" -nodeType "realflowVelocityShader" -nodeType "mia_material_x_passes"
		 -nodeType "mi_metallic_paint_x_passes" -nodeType "mi_car_paint_phen_x_passes" -nodeType "misss_fast_shader_x_passes"
		 -dataType "byteArray" "Mayatomr" "2014.0 - 3.11.1.13 ";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014";
fileInfo "cutIdentifier" "201310082153-890429";
fileInfo "osv" "Linux 2.6.32-220.el6.x86_64 #1 SMP Tue Dec 6 19:48:22 GMT 2011 x86_64";
createNode transform -n "STnClothGrp";
	addAttr -ci true -m -sn "distinction" -ln "distinction" -at "message";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -s 8 ".distinction";
createNode transform -n "const_STnucleus1" -p "STnClothGrp";
	addAttr -ci true -sn "distinction" -ln "distinction" -at "message";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode transform -n "STnucleus1" -p "const_STnucleus1";
	addAttr -ci true -k true -sn "useWind" -ln "useWind" -dv 1 -min 0 -max 1 -en "off:on" 
		-at "enum";
	addAttr -ci true -k true -sn "airDensity" -ln "airDensity" -dv 1 -at "double";
	addAttr -ci true -k true -sn "windSpeed" -ln "windSpeed" -at "double";
	addAttr -ci true -k true -sn "windDirectionX" -ln "windDirectionX" -at "double";
	addAttr -ci true -k true -sn "windDirectionY" -ln "windDirectionY" -at "double";
	addAttr -ci true -k true -sn "windDirectionZ" -ln "windDirectionZ" -at "double";
	addAttr -ci true -k true -sn "useGravityAnim" -ln "useGravityAnim" -dv 1 -min 0 
		-max 1 -en "off:on" -at "enum";
	addAttr -ci true -k true -sn "gravity" -ln "gravity" -dv 9.8 -at "double";
	addAttr -ci true -k true -sn "gravityDirectionX" -ln "gravityDirectionX" -at "double";
	addAttr -ci true -k true -sn "gravityDirectionY" -ln "gravityDirectionY" -dv -1 
		-at "double";
	addAttr -ci true -k true -sn "gravityDirectionZ" -ln "gravityDirectionZ" -at "double";
	addAttr -ci true -k true -sn "usePlane" -ln "usePlane" -min 0 -max 1 -en "off:on" 
		-at "enum";
	addAttr -ci true -k true -sn "planeBounce" -ln "planeBounce" -min 0 -at "double";
	addAttr -ci true -k true -sn "planeFriction" -ln "planeFriction" -min 0 -at "double";
	addAttr -ci true -k true -sn "planeOriginX" -ln "planeOriginX" -at "double";
	addAttr -ci true -k true -sn "planeOriginY" -ln "planeOriginY" -at "double";
	addAttr -ci true -k true -sn "planeOriginZ" -ln "planeOriginZ" -at "double";
	addAttr -ci true -k true -sn "planeNormalX" -ln "planeNormalX" -at "double";
	addAttr -ci true -k true -sn "planeNormalY" -ln "planeNormalY" -dv 1 -at "double";
	addAttr -ci true -k true -sn "planeNormalZ" -ln "planeNormalZ" -at "double";
	addAttr -ci true -sn "distinction" -ln "distinction" -at "message";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".useWind";
	setAttr -cb on ".windSpeed";
	setAttr -cb on ".windDirectionX";
	setAttr -cb on ".windDirectionY";
	setAttr -cb on ".windDirectionZ";
	setAttr -k on ".useGravityAnim";
	setAttr -cb on ".gravity";
	setAttr -cb on ".gravityDirectionX";
	setAttr -cb on ".gravityDirectionY";
	setAttr -cb on ".gravityDirectionZ";
	setAttr -k on ".usePlane";
	setAttr -cb on ".planeOriginX";
	setAttr -cb on ".planeOriginY";
	setAttr -cb on ".planeOriginZ";
	setAttr -cb on ".planeNormalX";
	setAttr -cb on ".planeNormalY";
	setAttr -cb on ".planeNormalZ";
createNode annotationShape -n "STnucleusShape1" -p "STnucleus1";
	setAttr -k off ".v";
	setAttr ".tmp" yes;
	setAttr ".txt" -type "string" "STnucleus1";
createNode locator -n "STnucleus1_LoSp" -p "STnucleus1";
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode transform -n "STnucleus1_windConst" -p "STnucleus1";
	addAttr -ci true -sn "distinction" -ln "distinction" -at "message";
	setAttr -l on ".t";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -l on ".s";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
createNode nurbsCurve -n "windLength_nucleus1Shape" -p "STnucleus1_windConst";
	setAttr -k off ".v";
	setAttr ".tmp" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 0 -30
		;
createNode transform -n "windLength_nucleus1Dimension" -p "windLength_nucleus1Shape";
createNode arcLengthDimension -n "windLength_nucleus1DimensionShape" -p "windLength_nucleus1Dimension";
	setAttr -k off ".v";
	setAttr ".upv" 1;
createNode transform -n "noiseT1" -p "STnucleus1_windConst";
	addAttr -ci true -sn "mxtx" -ln "maxDistanceTX" -at "double";
	addAttr -ci true -sn "mxty" -ln "maxDistanceTY" -at "double";
	addAttr -ci true -sn "mxtz" -ln "maxDistanceTZ" -at "double";
	addAttr -ci true -sn "mxrx" -ln "maxDistanceRX" -at "double";
	addAttr -ci true -sn "mxry" -ln "maxDistanceRY" -at "double";
	addAttr -ci true -sn "mxrz" -ln "maxDistanceRZ" -at "double";
	addAttr -ci true -sn "spdt" -ln "speedT" -dv 1 -at "double";
	addAttr -ci true -sn "spdr" -ln "speedR" -dv 1 -at "double";
	addAttr -ci true -sn "tot" -ln "timeOffsetT" -at "double";
	addAttr -ci true -sn "tor" -ln "timeOffsetR" -at "double";
	setAttr -k off ".v";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr -k on ".mxtx" 2;
	setAttr -k on ".mxty" 2;
	setAttr -k on ".mxtz" 10;
	setAttr -k on ".mxrx";
	setAttr -k on ".mxry";
	setAttr -k on ".mxrz";
	setAttr -k on ".spdt" 30;
	setAttr -k on ".spdr";
	setAttr -k on ".tot";
	setAttr -k on ".tor";
createNode transform -n "STnucleus1_wind" -p "noiseT1";
	addAttr -ci true -sn "distinction" -ln "distinction" -at "message";
	setAttr -k off ".v";
	setAttr ".t" -type "double3" 0 0 -30 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "STnucleus1_wind_LoSp" -p "STnucleus1_wind";
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode annotationShape -n "STnucleus1_W" -p "STnucleus1_wind";
	setAttr -k off ".v";
	setAttr ".ovs" no;
	setAttr ".ovt" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".txt" -type "string" "W";
	setAttr -k off ".daro" no;
createNode transform -n "STnucleus1_gravityConst" -p "STnucleus1";
	addAttr -ci true -sn "distinction" -ln "distinction" -at "message";
	setAttr -l on ".t";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr ".r" -type "double3" -180 0 0 ;
	setAttr -l on ".s";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
createNode annotationShape -n "STnucleus1_gravityArrowShape" -p "STnucleus1_gravityConst";
	setAttr -k off ".v";
	setAttr ".tmp" yes;
	setAttr ".txt" -type "string" "";
createNode transform -n "STnucleus1_gravity" -p "STnucleus1_gravityConst";
	addAttr -ci true -sn "distinction" -ln "distinction" -at "message";
	setAttr -k off ".v";
	setAttr ".t" -type "double3" 0 9.8 0 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "STnucleus1_gravity_LoSp" -p "STnucleus1_gravity";
	setAttr -k off ".v";
	setAttr ".io" yes;
createNode annotationShape -n "STnucleus1_G" -p "STnucleus1_gravity";
	setAttr -k off ".v";
	setAttr ".ovs" no;
	setAttr ".ovt" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".txt" -type "string" "G";
	setAttr -k off ".daro" no;
createNode nurbsCurve -n "gravityLength_nucleus1Shape" -p "STnucleus1_gravityConst";
	setAttr -k off ".v";
	setAttr ".tmp" yes;
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 9.8000000000000007 0
		;
createNode transform -n "gravityLength_nucleus1Dimension" -p "gravityLength_nucleus1Shape";
createNode arcLengthDimension -n "gravityLength_nucleus1DimensionShape" -p "gravityLength_nucleus1Dimension";
	setAttr -k off ".v";
	setAttr ".upv" 1;
createNode transform -n "STnucleus1_planeConst" -p "STnucleus1";
	addAttr -ci true -sn "distinction" -ln "distinction" -at "message";
	setAttr -l on ".t";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -l on ".s";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
createNode transform -n "STnucleus1_plane" -p "STnucleus1_planeConst";
	addAttr -ci true -sn "distinction" -ln "distinction" -at "message";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 24;
	setAttr ".it" no;
createNode nurbsCurve -n "STnucleus1_planeShape" -p "STnucleus1_plane";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 2 0 no 3
		3 0 1 2
		3
		-0.5 0 0.5
		-0.5 0 -0.5
		0.5 0 -0.5
		;
createNode nurbsCurve -n "STnucleus1_planeShape1" -p "STnucleus1_plane";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 2 0 no 3
		3 0 1 2
		3
		-0.5 0 0.5
		0.5 0 0.5
		0.5 0 -0.5
		;
createNode locator -n "STnucleus1_planeBase_LoSp" -p "STnucleus1_plane";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".los" -type "double3" 0 1 1 ;
createNode locator -n "STnucleus1_planeUp_LoSp" -p "STnucleus1_plane";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".lp" -type "double3" 0 1 0 ;
createNode annotationShape -n "STnucleus1_P" -p "STnucleus1_plane";
	setAttr -k off ".v";
	setAttr ".ovs" no;
	setAttr ".ovt" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 23;
	setAttr ".txt" -type "string" "P";
	setAttr -k off ".daro" no;
createNode transform -n "particleWindGP" -p "const_STnucleus1";
createNode transform -n "particle_particleController" -p "particleWindGP";
	addAttr -ci true -sn "EmitRate" -ln "EmitRate" -dv 100 -min 0 -at "double";
	addAttr -ci true -sn "PartLifeSpan" -ln "PartLifeSpan" -dv 3 -min 0 -at "double";
	addAttr -ci true -sn "PartSpdMax" -ln "PartSpdMax" -dv 2 -min 0 -at "double";
	addAttr -ci true -sn "PartSpdMin" -ln "PartSpdMin" -dv 1 -min 0 -at "double";
	addAttr -ci true -sn "PartStartFrame" -ln "PartStartFrame" -dv -100 -at "double";
	addAttr -ci true -sn "RadiPower" -ln "RadiPower" -dv 100 -min 0 -at "double";
	addAttr -ci true -sn "RadiAttenMax" -ln "RadiAttenMax" -dv 1 -min 0 -at "double";
	addAttr -ci true -sn "RadiAttenMin" -ln "RadiAttenMin" -dv 0.5 -min 0 -at "double";
	addAttr -ci true -sn "RadiDistance" -ln "RadiDistance" -dv 1 -min 0 -at "double";
	addAttr -ci true -sn "TurbPower" -ln "TurbPower" -dv 1 -min 0 -at "double";
	addAttr -ci true -sn "TurbFreqMax" -ln "TurbFreqMax" -dv 1 -min 0 -at "double";
	addAttr -ci true -sn "TurbFreqMin" -ln "TurbFreqMin" -dv 0.5 -min 0 -at "double";
	setAttr ".t" -type "double3" 0 0 10 ;
	setAttr -k on ".EmitRate" 3000;
	setAttr -k on ".PartLifeSpan" 8;
	setAttr -k on ".PartSpdMax" 30;
	setAttr -k on ".PartSpdMin" 25;
	setAttr -k on ".PartStartFrame" 1;
	setAttr -k on ".RadiPower" 200;
	setAttr -l on -k on ".RadiAttenMax";
	setAttr -l on -k on ".RadiAttenMin" 1;
	setAttr -k on ".RadiDistance" 3;
	setAttr -k on ".TurbPower" 1.2;
	setAttr -k on ".TurbFreqMax";
	setAttr -k on ".TurbFreqMin";
createNode transform -n "particle_particleSurface" -p "particle_particleController";
	setAttr ".tmp" yes;
	setAttr ".s" -type "double3" 2.5 2.5 2.5 ;
createNode nurbsSurface -n "particle_particleSurfaceShape" -p "particle_particleSurface";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 0 no 
		6 0 0 0 1 1 1
		6 0 0 0 1 1 1
		
		16
		-7.5 -7.5 0
		-7.5 -2.5 0
		-7.5 2.5 0
		-7.5 7.5 0
		-2.5 -7.5 0
		-2.5 -2.5 0
		-2.5 2.5 0
		-2.5 7.5 0
		2.5 -7.5 0
		2.5 -2.5 0
		2.5 2.5 0
		2.5 7.5 0
		7.5 -7.5 0
		7.5 -2.5 0
		7.5 2.5 0
		7.5 7.5 0
		
		;
	setAttr ".nufa" 4.5;
	setAttr ".nvfa" 4.5;
createNode pointEmitter -n "particle_emitter" -p "particle_particleSurface";
	setAttr ".emt" 2;
	setAttr ".sro" no;
	setAttr -l on ".urpp";
createNode transform -n "particle_particle" -p "particle_particleController";
createNode particle -n "particle_particleShape" -p "particle_particle";
	addAttr -ci true -sn "lifespanPP" -ln "lifespanPP" -dt "doubleArray";
	addAttr -ci true -h true -sn "lifespanPP0" -ln "lifespanPP0" -dt "doubleArray";
	addAttr -ci true -sn "lifespan" -ln "lifespan" -at "double";
	setAttr -k off ".v";
	setAttr ".gf" -type "Int32Array" 0 ;
	setAttr ".pos0" -type "vectorArray" 0 ;
	setAttr ".vel0" -type "vectorArray" 0 ;
	setAttr ".acc0" -type "vectorArray" 0 ;
	setAttr ".usc" yes;
	setAttr ".scp" -type "string" "clothWindSet_test.v1_startup";
	setAttr ".mas0" -type "doubleArray" 0 ;
	setAttr ".id0" -type "doubleArray" 0 ;
	setAttr ".bt0" -type "doubleArray" 0 ;
	setAttr ".ag0" -type "doubleArray" 0 ;
	setAttr ".lfm" 2;
	setAttr -s 3 ".xi";
	setAttr ".irbx" -type "string" "";
	setAttr ".irax" -type "string" "";
	setAttr ".icx" -type "string" "float $sf = .I[0];\nfloat $min = .I[1];\nfloat $max = .I[2];\nif(frame == $sf)\nseed(10);\nfloat $z = rand($min, $max);\n.O[0] = <<0,0,-$z>>;";
	setAttr ".chw" 501;
	setAttr ".lifespanPP0" -type "doubleArray" 0 ;
	setAttr -k on ".lifespan";
createNode radialField -n "particle_radialField" -p "particle_particle";
	setAttr ".apv" yes;
	setAttr ".umd" yes;
	setAttr ".fc[0]"  0 1 1;
	setAttr ".amag[0]"  0 1 1;
	setAttr ".crad[0]"  0 1 1;
createNode transform -n "particle_distanceCurve" -p "particle_particleController";
createNode locator -n "particle_distanceCurveShape" -p "particle_distanceCurve";
	setAttr -k off ".v";
createNode transform -n "particle_curveTopA" -p "particle_distanceCurve";
createNode transform -n "particle_curveTopB" -p "particle_distanceCurve";
createNode transform -n "particle_curveBar" -p "particle_distanceCurve";
createNode nurbsCurve -n "curveShape1" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 -10
		0 1 -7
		0 -1 -7
		0 0 -10
		;
createNode nurbsCurve -n "curveShape2" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 3 0 no 3
		4 0 1 2 3
		4
		0 0 -10
		-1 0 -7
		1 0 -7
		0 0 -10
		;
createNode nurbsCurve -n "curveShape3" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 1 0 no 3
		2 0 1
		2
		0 0 0
		0 0 -10
		;
createNode nurbsCurve -n "curveShape4" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 43 0 no 3
		48 0 0 0 0.082000000000000017 0.082000000000000017 0.082000000000000017 0.16100000000000003 0.16100000000000003 0.16100000000000003 0.60200000000000009 0.60200000000000009
		 0.60200000000000009 0.68100000000000005 0.68100000000000005 0.68100000000000005 0.76300000000000001 0.76300000000000001 0.76300000000000001 1.073 1.073 1.073 1.155 1.155
		 1.155 1.2989999999999999 1.2989999999999999 1.2989999999999999 1.417 1.417 1.417 2.4169999999999998 2.4169999999999998 2.4169999999999998 3.4169999999999998 3.4169999999999998
		 3.4169999999999998 4.4169999999999998 4.4169999999999998 4.4169999999999998 5.4169999999999998 5.4169999999999998 5.4169999999999998 5.4550000000000001 5.4550000000000001 5.4550000000000001 5.6210000000000004 5.6210000000000004
		 5.6210000000000004
		46
		7.9500000000000002 11.265000000000001 -6.1232339957367663e-18
		7.9500000000000002 11.128333333333334 -6.1232339957367663e-18
		7.9500000000000002 10.991666666666667 -6.1232339957367663e-18
		7.9500000000000002 10.855 -6.1232339957367663e-18
		7.8183333333333334 10.855 -2.2247750184510251e-17
		7.6866666666666665 10.855 -3.8372266373283731e-17
		7.5549999999999997 10.855 -5.4496782562057226e-17
		7.5549999999999997 10.119999999999999 -5.4496782562057226e-17
		7.5549999999999997 9.3849999999999998 -5.4496782562057226e-17
		7.5549999999999997 8.6500000000000004 -5.4496782562057226e-17
		7.6866666666666665 8.6500000000000004 -3.8372266373283731e-17
		7.8183333333333334 8.6500000000000004 -2.2247750184510245e-17
		7.9500000000000002 8.6500000000000004 -6.1232339957367663e-18
		7.9500000000000002 8.5133333333333336 -6.1232339957367663e-18
		7.9500000000000002 8.3766666666666669 -6.1232339957367663e-18
		7.9500000000000002 8.2400000000000002 -6.1232339957367663e-18
		7.4333333333333336 8.2400000000000002 -6.9396651951683359e-17
		6.9166666666666661 8.2400000000000002 -1.3267006990762994e-16
		6.4000000000000004 8.2400000000000002 -1.9594348786357652e-16
		6.4000000000000004 8.3766666666666669 -1.9594348786357652e-16
		6.4000000000000004 8.5133333333333336 -1.9594348786357652e-16
		6.4000000000000004 8.6500000000000004 -1.9594348786357652e-16
		6.6400000000000006 8.6500000000000004 -1.6655196468404001e-16
		6.8799999999999999 8.6500000000000004 -1.3716044150450355e-16
		7.1200000000000001 8.6500000000000004 -1.0776891832496709e-16
		7.1200000000000001 8.8466666666666658 -1.0776891832496709e-16
		7.1200000000000001 9.043333333333333 -1.0776891832496709e-16
		7.1200000000000001 9.2400000000000002 -1.0776891832496709e-16
		6.8499999999999996 9.0099999999999998 -1.4083438190194563e-16
		6.5250000000000004 8.9649999999999999 -1.806354028742346e-16
		6.3700000000000001 8.9649999999999999 -1.996174282610186e-16
		5.7999999999999998 8.9649999999999999 -2.694222958124177e-16
		5.2050000000000001 9.4450000000000003 -3.4228878036168529e-16
		5.2050000000000001 10.16 -3.4228878036168529e-16
		5.2050000000000001 10.755000000000001 -3.4228878036168529e-16
		5.665 11.335000000000001 -2.8595502760090702e-16
		6.375 11.335000000000001 -1.9900510486144491e-16
		6.4699999999999998 11.335000000000001 -1.8737096026954504e-16
		6.8149999999999995 11.335000000000001 -1.4512064569896137e-16
		7.1200000000000001 11.074999999999999 -1.0776891832496708e-16
		7.1200000000000001 11.138333333333334 -1.0776891832496709e-16
		7.1200000000000001 11.201666666666668 -1.0776891832496709e-16
		7.1200000000000001 11.265000000000001 -1.0776891832496709e-16
		7.3966666666666665 11.265000000000001 -7.3887023548556969e-17
		7.6733333333333329 11.265000000000001 -4.0005128772146879e-17
		7.9500000000000002 11.265000000000001 -6.1232339957367663e-18
		;
createNode nurbsCurve -n "curveShape5" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 10 0 no 3
		15 0 0 0 1 1 1 2 2 2 3 3
		 3 4 4 4
		13
		7.1399999999999997 10.17 -1.0531962472667239e-16
		7.1399999999999997 10.565 -1.0531962472667239e-16
		6.8200000000000003 10.904999999999999 -1.445083222993877e-16
		6.375 10.904999999999999 -1.9900510486144491e-16
		5.8900000000000006 10.904999999999999 -2.5840047462009152e-16
		5.665 10.51 -2.8595502760090702e-16
		5.665 10.16 -2.8595502760090702e-16
		5.665 9.7200000000000006 -2.8595502760090702e-16
		5.9950000000000001 9.3949999999999996 -2.4554168322904433e-16
		6.3799999999999999 9.3949999999999996 -1.9839278146187122e-16
		6.71 9.3949999999999996 -1.5797943709000858e-16
		7.1399999999999997 9.6600000000000001 -1.0531962472667239e-16
		7.1399999999999997 10.17 -1.0531962472667239e-16
		;
createNode nurbsCurve -n "curveShape6" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 40 0 no 3
		45 0 0 0 1 1 1 2 2 2 2.0869252552484028 2.0869252552484028
		 2.0869252552484028 3.0869252552484028 3.0869252552484028 3.0869252552484028 4.0869252552484028 4.0869252552484028 4.0869252552484028 5.0869252552484028 5.0869252552484028 5.0869252552484028 6.0869252552484028 6.0869252552484028
		 6.0869252552484028 7.0869252552484028 7.0869252552484028 7.0869252552484028 8.0869252552484028 8.0869252552484028 8.0869252552484028 8.1349252552484028 8.1349252552484028 8.1349252552484028 8.3079252552484029 8.3079252552484029
		 8.3079252552484029 8.3899252552484036 8.3899252552484036 8.3899252552484036 8.4759252552484039 8.4759252552484039 8.4759252552484039 8.7009252552484035 8.7009252552484035 8.7009252552484035
		43
		2.625 10.535 -6.5824765454170241e-16
		2.625 11.050000000000001 -6.5824765454170241e-16
		2.9050000000000002 11.370000000000001 -6.2395754416557649e-16
		3.5449999999999999 11.370000000000001 -5.4558014902014587e-16
		3.9500000000000002 11.370000000000001 -4.9598195365467806e-16
		4.29 11.234999999999999 -4.5434396248366809e-16
		4.5949999999999998 11.109999999999999 -4.1699223510967379e-16
		4.5383333333333331 10.976666666666667 -4.239319003048421e-16
		4.4816666666666665 10.843333333333334 -4.3087156550001046e-16
		4.4249999999999998 10.710000000000001 -4.3781123069517883e-16
		4.2400000000000002 10.789999999999999 -4.6046719647940487e-16
		3.8999999999999999 10.94 -5.0210518765041484e-16
		3.5499999999999998 10.94 -5.4496782562057218e-16
		3.0549999999999997 10.94 -6.0558784217836626e-16
		3.0600000000000001 10.67 -6.0497551877879248e-16
		3.0600000000000001 10.494999999999999 -6.0497551877879248e-16
		3.2400000000000002 10.515000000000001 -5.8293187639414012e-16
		3.4050000000000002 10.535 -5.6272520420820882e-16
		3.6299999999999999 10.535 -5.3517065122739337e-16
		4.5099999999999998 10.535 -4.2740173290242633e-16
		4.7599999999999998 10.015000000000001 -3.9678556292374245e-16
		4.7599999999999998 9.7100000000000009 -3.9678556292374245e-16
		4.7599999999999998 9.3200000000000003 -3.9678556292374245e-16
		4.415 8.9299999999999997 -4.3903587749432615e-16
		3.8849999999999998 8.9299999999999997 -5.039421578491359e-16
		3.4850000000000003 8.9299999999999997 -5.5292802981503002e-16
		3.2250000000000001 9.1199999999999992 -5.8476884659286118e-16
		3.0600000000000001 9.2400000000000002 -6.0497551877879248e-16
		3.0599999999999996 9.1600000000000001 -6.0497551877879257e-16
		3.0599999999999996 9.0800000000000001 -6.0497551877879257e-16
		3.0599999999999996 9 -6.0497551877879257e-16
		2.7716666666666665 9 -6.4028616815420797e-16
		2.4833333333333329 9 -6.7559681752962326e-16
		2.1949999999999998 9 -7.1090746690503855e-16
		2.1949999999999998 9.1366666666666667 -7.1090746690503855e-16
		2.1949999999999998 9.2733333333333334 -7.1090746690503855e-16
		2.1949999999999998 9.4100000000000001 -7.1090746690503855e-16
		2.3383333333333329 9.4100000000000001 -6.933541961172599e-16
		2.4816666666666665 9.4100000000000001 -6.7580092532948116e-16
		2.625 9.4100000000000001 -6.5824765454170241e-16
		2.625 9.7850000000000001 -6.5824765454170241e-16
		2.625 10.16 -6.5824765454170241e-16
		2.625 10.535 -6.5824765454170241e-16
		;
createNode nurbsCurve -n "curveShape7" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 13 0 no 3
		18 0 0 0 1 1 1 2 2 2 3 3
		 3 4 4 4 4.0519999999999996 4.0519999999999996 4.0519999999999996
		16
		3.0600000000000001 9.8100000000000005 -6.0497551877879248e-16
		3.4450000000000003 9.3800000000000008 -5.5782661701161942e-16
		3.79 9.3599999999999994 -5.1557630244103567e-16
		3.8899999999999997 9.3599999999999994 -5.0332983444956221e-16
		4.1050000000000004 9.3599999999999994 -4.7699992826789414e-16
		4.3099999999999996 9.5199999999999996 -4.5189466888537334e-16
		4.3099999999999996 9.7050000000000001 -4.5189466888537334e-16
		4.3099999999999996 9.7650000000000006 -4.5189466888537334e-16
		4.2850000000000001 10.125 -4.5495628588324178e-16
		3.5800000000000001 10.125 -5.4129388522313015e-16
		3.3849999999999998 10.125 -5.6517449780650357e-16
		3.2350000000000003 10.095000000000001 -5.8354419979371381e-16
		3.0600000000000001 10.07 -6.0497551877879248e-16
		3.0599999999999996 9.9833333333333343 -6.0497551877879257e-16
		3.0599999999999996 9.8966666666666665 -6.0497551877879257e-16
		3.0599999999999996 9.8100000000000005 -6.0497551877879257e-16
		;
createNode nurbsCurve -n "curveShape8" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 46 0 no 3
		51 0 0 0 0.082000000000000017 0.082000000000000017 0.082000000000000017 0.193 0.193 0.193 0.48199999999999998 0.48199999999999998
		 0.48199999999999998 0.60199999999999998 0.60199999999999998 0.60199999999999998 0.68399999999999994 0.68399999999999994 0.68399999999999994 1.0760000000000001 1.0760000000000001 1.0760000000000001 1.1580000000000001 1.1580000000000001
		 1.1580000000000001 1.3430000000000002 1.3430000000000002 1.3430000000000002 1.5300000000000002 1.5300000000000002 1.5300000000000002 2.5300000000000002 2.5300000000000002 2.5300000000000002 3.5300000000000002 3.5300000000000002
		 3.5300000000000002 3.6420044641967455 3.6420044641967455 3.6420044641967455 4.6420044641967451 4.6420044641967451 4.6420044641967451 5.6420044641967451 5.6420044641967451 5.6420044641967451 5.7160044641967449 5.7160044641967449
		 5.7160044641967449 5.9140044641967453 5.9140044641967453 5.9140044641967453
		49
		1.645 11.265000000000001 -7.78263040858143e-16
		1.645 11.128333333333334 -7.78263040858143e-16
		1.645 10.991666666666667 -7.78263040858143e-16
		1.645 10.855 -7.78263040858143e-16
		1.46 10.855 -8.0091900664236905e-16
		1.2749999999999999 10.855 -8.2357497242659509e-16
		1.0899999999999999 10.855 -8.4623093821082114e-16
		1.0899999999999999 10.373333333333333 -8.4623093821082114e-16
		1.0899999999999999 9.8916666666666675 -8.4623093821082114e-16
		1.0899999999999999 9.4100000000000001 -8.4623093821082114e-16
		1.29 9.4100000000000001 -8.2173800222787403e-16
		1.49 9.4100000000000001 -7.9724506624492702e-16
		1.6899999999999999 9.4100000000000001 -7.7275213026197991e-16
		1.6899999999999999 9.2733333333333334 -7.7275213026197991e-16
		1.6899999999999999 9.1366666666666667 -7.7275213026197991e-16
		1.6899999999999999 9 -7.7275213026197991e-16
		1.0366666666666666 9 -8.5276238780627371e-16
		0.38333333333333353 9 -9.3277264535056731e-16
		-0.27000000000000002 9 -1.0127829028948611e-15
		-0.26999999999999957 9.1366666666666667 -1.0127829028948611e-15
		-0.26999999999999957 9.2733333333333334 -1.0127829028948611e-15
		-0.26999999999999957 9.4100000000000001 -1.0127829028948611e-15
		0.038333333333332886 9.4100000000000001 -9.7502295992115106e-16
		0.34666666666666646 9.4100000000000001 -9.3726301694744102e-16
		0.6549999999999998 9.4100000000000001 -8.9950307397373097e-16
		0.6549999999999998 9.7216666666666676 -8.9950307397373097e-16
		0.6549999999999998 10.033333333333333 -8.9950307397373097e-16
		0.6549999999999998 10.345000000000001 -8.9950307397373097e-16
		0.61499999999999977 10.385 -9.0440166117032047e-16
		0.16000000000000014 10.904999999999999 -9.6012309053152496e-16
		-0.16500000000000004 10.904999999999999 -9.9992411150381396e-16
		-0.33499999999999996 10.904999999999999 -1.0207431070893189e-15
		-0.42999999999999972 10.785 -1.0323772516812189e-15
		-0.48499999999999988 10.69 -1.0391128090765294e-15
		-0.63333333333333375 10.803333333333333 -1.0572784032638816e-15
		-0.78166666666666673 10.916666666666666 -1.075443997451234e-15
		-0.92999999999999972 11.029999999999999 -1.0936095916385865e-15
		-0.82000000000000028 11.140000000000001 -1.0801384768479656e-15
		-0.62999999999999989 11.335000000000001 -1.056870187664166e-15
		-0.25 11.335000000000001 -1.0103336092965665e-15
		0.18000000000000016 11.335000000000001 -9.5767379693323021e-16
		0.44500000000000006 11.094999999999999 -9.2522065675582546e-16
		0.6549999999999998 10.895 -8.9950307397373097e-16
		0.6549999999999998 11.018333333333333 -8.9950307397373097e-16
		0.6549999999999998 11.141666666666666 -8.9950307397373097e-16
		0.6549999999999998 11.265000000000001 -8.9950307397373097e-16
		0.98499999999999988 11.265000000000001 -8.5908972960186838e-16
		1.3149999999999999 11.265000000000001 -8.1867638523000569e-16
		1.645 11.265000000000001 -7.78263040858143e-16
		;
createNode nurbsCurve -n "curveShape9" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 52 0 no 3
		57 0 0 0 0.13999999999999996 0.13999999999999996 0.13999999999999996 0.20599999999999996 0.20599999999999996 0.20599999999999996 0.28799999999999998 0.28799999999999998
		 0.28799999999999998 0.35399999999999998 0.35399999999999998 0.35399999999999998 0.52200000000000002 0.52200000000000002 0.52200000000000002 1.522 1.522 1.522 2.5220000000000002 2.5220000000000002
		 2.5220000000000002 3.5220000000000002 3.5220000000000002 3.5220000000000002 3.6076154191720162 3.6076154191720162 3.6076154191720162 3.6421552512503572 3.6421552512503572 3.6421552512503572 4.6421552512503572 4.6421552512503572
		 4.6421552512503572 5.6421552512503572 5.6421552512503572 5.6421552512503572 5.8101552512503574 5.8101552512503574 5.8101552512503574 6.0381552512503571 6.0381552512503571 6.0381552512503571 6.120155251250357 6.120155251250357
		 6.120155251250357 6.3481552512503567 6.3481552512503567 6.3481552512503567 6.4881552512503564 6.4881552512503564 6.4881552512503564 6.5751552512503562 6.5751552512503562 6.5751552512503562
		55
		-1.7949999999999999 11.914999999999999 -1.1995415397648326e-15
		-1.7949999999999999 11.681666666666667 -1.1995415397648326e-15
		-1.7949999999999999 11.448333333333334 -1.1995415397648326e-15
		-1.7949999999999999 11.215 -1.1995415397648326e-15
		-1.6850000000000001 11.215 -1.1860704249742116e-15
		-1.575 11.215 -1.1725993101835907e-15
		-1.4650000000000001 11.215 -1.1591281953929698e-15
		-1.4650000000000001 11.078333333333333 -1.1591281953929698e-15
		-1.4650000000000001 10.941666666666666 -1.1591281953929698e-15
		-1.4650000000000001 10.805 -1.1591281953929698e-15
		-1.575 10.805 -1.1725993101835907e-15
		-1.6850000000000001 10.805 -1.1860704249742116e-15
		-1.7949999999999999 10.805 -1.1995415397648326e-15
		-1.7949999999999999 10.525 -1.1995415397648326e-15
		-1.7949999999999999 10.245000000000001 -1.1995415397648326e-15
		-1.7949999999999999 9.9649999999999999 -1.1995415397648326e-15
		-1.7949999999999999 9.6649999999999991 -1.1995415397648326e-15
		-1.8 9.3699999999999992 -1.2001538631644061e-15
		-2.0049999999999999 9.1549999999999994 -1.2252591225469268e-15
		-2.1550000000000002 9 -1.2436288245341373e-15
		-2.4400000000000004 8.9299999999999997 -1.2785312583098368e-15
		-2.6600000000000001 8.9299999999999997 -1.3054734878910786e-15
		-3.0650000000000004 8.9299999999999997 -1.3550716832565464e-15
		-3.5049999999999999 9.1300000000000008 -1.40895614241903e-15
		-3.7650000000000001 9.25 -1.4407969591968611e-15
		-3.71 9.3816666666666659 -1.4340614018015507e-15
		-3.6550000000000002 9.5133333333333336 -1.4273258444062402e-15
		-3.6000000000000001 9.6449999999999996 -1.4205902870109297e-15
		-3.5466666666666664 9.6233333333333331 -1.4140588374154773e-15
		-3.4933333333333332 9.6016666666666666 -1.4075273878200246e-15
		-3.4399999999999995 9.5800000000000001 -1.4009959382245721e-15
		-3.1499999999999999 9.4600000000000009 -1.3654811810492988e-15
		-2.915 9.3599999999999994 -1.336701981269336e-15
		-2.6200000000000001 9.3599999999999994 -1.3005749006944891e-15
		-2.2250000000000001 9.3599999999999994 -1.2522013521281687e-15
		-2.23 9.6199999999999992 -1.2528136755277423e-15
		-2.23 9.9649999999999999 -1.2528136755277423e-15
		-2.23 10.244999999999999 -1.2528136755277423e-15
		-2.23 10.525 -1.2528136755277423e-15
		-2.23 10.805 -1.2528136755277423e-15
		-2.6100000000000003 10.805 -1.2993502538953418e-15
		-2.9899999999999998 10.805 -1.3458868322629412e-15
		-3.3700000000000001 10.805 -1.3924234106305407e-15
		-3.3700000000000001 10.941666666666666 -1.3924234106305407e-15
		-3.3700000000000001 11.078333333333333 -1.3924234106305407e-15
		-3.3700000000000001 11.215 -1.3924234106305407e-15
		-2.9899999999999998 11.215 -1.3458868322629412e-15
		-2.6100000000000003 11.215 -1.2993502538953418e-15
		-2.23 11.215 -1.2528136755277423e-15
		-2.2300000000000004 11.448333333333334 -1.2528136755277423e-15
		-2.2300000000000004 11.681666666666667 -1.2528136755277423e-15
		-2.2300000000000004 11.914999999999999 -1.2528136755277423e-15
		-2.085 11.914999999999999 -1.2350562969401058e-15
		-1.9400000000000002 11.914999999999999 -1.2172989183524692e-15
		-1.7949999999999999 11.914999999999999 -1.1995415397648326e-15
		;
createNode nurbsCurve -n "curveShape10" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 10 2 no 3
		11 0 0.082000000000000017 0.22500000000000003 0.51400000000000001 0.68400000000000005 0.76600000000000001 1.1899999999999999 1.272 1.4390000000000001 1.8100000000000001 2.04
		
		11
		-4.5750000000000002 11.265000000000001 -1.5399933499277967e-15
		-4.5750000000000002 10.855 -1.5399933499277967e-15
		-5.29 10.855 -1.6275555960668326e-15
		-5.29 9.4100000000000001 -1.6275555960668326e-15
		-4.4399999999999995 9.4100000000000001 -1.5234606181393075e-15
		-4.4399999999999995 9 -1.5234606181393075e-15
		-6.5600000000000005 9 -1.7830857395585463e-15
		-6.5600000000000005 9.4100000000000001 -1.7830857395585463e-15
		-5.7249999999999996 9.4100000000000001 -1.6808277318297425e-15
		-5.7249999999999996 11.265000000000001 -1.6808277318297425e-15
		-4.5750000000000002 11.265000000000001 -1.5399933499277967e-15
		;
createNode nurbsCurve -n "curveShape11" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 4 2 no 3
		5 0 0.10600000000000001 0.23800000000000002 0.34400000000000003 0.47600000000000003
		5
		-5.1950000000000003 11.699999999999999 -1.6159214514749326e-15
		-5.7249999999999996 11.699999999999999 -1.6808277318297425e-15
		-5.7249999999999996 12.359999999999999 -1.6808277318297425e-15
		-5.1950000000000003 12.359999999999999 -1.6159214514749326e-15
		-5.1950000000000003 11.699999999999999 -1.6159214514749326e-15
		;
createNode nurbsCurve -n "curveShape12" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 40 0 no 3
		45 0 0 0 0.055731499172371093 0.055731499172371093 0.055731499172371093 1.0557314991723712 1.0557314991723712 1.0557314991723712 2.0557314991723712 2.0557314991723712
		 2.0557314991723712 3.0557314991723712 3.0557314991723712 3.0557314991723712 4.0557314991723707 4.0557314991723707 4.0557314991723707 4.13896610735911 4.13896610735911 4.13896610735911 5.13896610735911 5.13896610735911
		 5.13896610735911 6.13896610735911 6.13896610735911 6.13896610735911 7.13896610735911 7.13896610735911 7.13896610735911 8.13896610735911 8.13896610735911 8.13896610735911 8.2236832838937417 8.2236832838937417
		 8.2236832838937417 9.2236832838937417 9.2236832838937417 9.2236832838937417 10.223683283893742 10.223683283893742 10.223683283893742 10.302092467029393 10.302092467029393 10.302092467029393
		43
		-9.2699999999999996 11.334999999999999 -2.1149650221274792e-15
		-9.254999999999999 11.243333333333334 -2.1131280519287582e-15
		-9.2400000000000002 11.151666666666667 -2.1112910817300373e-15
		-9.2249999999999996 11.06 -2.1094541115313159e-15
		-8.9700000000000006 11.275 -2.0782256181530587e-15
		-8.7650000000000006 11.335000000000001 -2.0531203587705379e-15
		-8.5150000000000006 11.335000000000001 -2.0225041887918541e-15
		-7.7249999999999996 11.335000000000001 -1.9257570916592132e-15
		-7.2949999999999999 10.715 -1.8730972792958771e-15
		-7.2949999999999999 10.130000000000001 -1.8730972792958771e-15
		-7.2949999999999999 9.4550000000000001 -1.8730972792958771e-15
		-7.8049999999999997 8.9299999999999997 -1.9355542660523918e-15
		-8.5299999999999994 8.9299999999999997 -2.024341158990575e-15
		-9.120000000000001 8.9299999999999997 -2.0965953201402688e-15
		-9.5999999999999996 9.2949999999999999 -2.1553783664993416e-15
		-9.8350000000000009 9.4749999999999996 -2.1841575662793048e-15
		-9.7550000000000008 9.5883333333333329 -2.1743603918861258e-15
		-9.6750000000000007 9.7016666666666662 -2.1645632174929472e-15
		-9.5950000000000006 9.8149999999999995 -2.1547660430997682e-15
		-9.3049999999999997 9.6099999999999994 -2.1192512859244949e-15
		-8.9350000000000005 9.3599999999999994 -2.0739393543560426e-15
		-8.5500000000000007 9.3599999999999994 -2.0267904525888698e-15
		-7.9699999999999998 9.3599999999999994 -1.9557609382383232e-15
		-7.7549999999999999 9.7949999999999999 -1.9294310320566551e-15
		-7.7549999999999999 10.135 -1.9294310320566551e-15
		-7.7549999999999999 10.35 -1.9294310320566551e-15
		-7.875 10.904999999999999 -1.9441267936464232e-15
		-8.5850000000000009 10.904999999999999 -2.0310767163858855e-15
		-9.1549999999999994 10.904999999999999 -2.1008815839372845e-15
		-9.1950000000000003 10.609999999999999 -2.105780171133874e-15
		-9.2400000000000002 10.300000000000001 -2.1112910817300373e-15
		-9.379999999999999 10.318333333333333 -2.1284361369181001e-15
		-9.5199999999999996 10.336666666666666 -2.145581192106163e-15
		-9.6600000000000001 10.355 -2.1627262472942259e-15
		-9.6449999999999996 10.470000000000001 -2.1608892770955049e-15
		-9.629999999999999 10.6 -2.1590523068967839e-15
		-9.629999999999999 10.795 -2.1590523068967839e-15
		-9.629999999999999 10.975 -2.1590523068967839e-15
		-9.6449999999999996 11.16 -2.1608892770955049e-15
		-9.6600000000000001 11.295 -2.1627262472942259e-15
		-9.5299999999999994 11.308333333333334 -2.1468058389053102e-15
		-9.4000000000000004 11.321666666666667 -2.1308854305163949e-15
		-9.2699999999999996 11.335000000000001 -2.1149650221274792e-15
		;
createNode nurbsCurve -n "curveShape13" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 10 2 no 3
		11 0 0.083384650865731838 0.23970483510308488 0.71570483510308491 0.88370483510308495 0.96570483510308491 1.3887048351030851 1.4707048351030851 1.6387048351030851 2.2047048351030849 2.4562798736104852
		
		11
		-10.460000000000001 12.155000000000001 -2.2606979912260139e-15
		-10.5 11.74 -2.2655965784226034e-15
		-11.279999999999999 11.789999999999999 -2.3611190287560971e-15
		-11.279999999999999 9.4100000000000001 -2.3611190287560971e-15
		-10.44 9.4100000000000001 -2.2582486976277195e-15
		-10.44 9 -2.2582486976277195e-15
		-12.555 9 -2.5172614956473846e-15
		-12.555 9.4100000000000001 -2.5172614956473846e-15
		-11.715 9.4100000000000001 -2.414391164519007e-15
		-11.715 12.24 -2.414391164519007e-15
		-10.460000000000001 12.155000000000001 -2.2606979912260139e-15
		;
createNode nurbsCurve -n "curveShape14" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 25 0 no 3
		30 0 0 0 1 1 1 2 2 2 2.3919999999999999 2.3919999999999999
		 2.3919999999999999 3.3919999999999999 3.3919999999999999 3.3919999999999999 4.3919999999999995 4.3919999999999995 4.3919999999999995 5.3919999999999995 5.3919999999999995 5.3919999999999995 6.3919999999999995 6.3919999999999995
		 6.3919999999999995 7.3919999999999995 7.3919999999999995 7.3919999999999995 7.4787698104181395 7.4787698104181395 7.4787698104181395
		28
		-15.68 9.7200000000000006 -2.8999636203809327e-15
		-15.41 9.5600000000000005 -2.8668981568039541e-15
		-14.99 9.3599999999999994 -2.8154629912397656e-15
		-14.574999999999999 9.3599999999999994 -2.7646401490751504e-15
		-13.925000000000001 9.3599999999999994 -2.6850381071305724e-15
		-13.81 9.7899999999999991 -2.6709546689403777e-15
		-13.765000000000001 9.9700000000000006 -2.6654437583442144e-15
		-14.418333333333333 9.9700000000000006 -2.7454540158885084e-15
		-15.071666666666667 9.9700000000000006 -2.825464273432802e-15
		-15.725 9.9700000000000006 -2.905474530977096e-15
		-15.73 10.015000000000001 -2.9060868543766694e-15
		-15.74 10.16 -2.9073115011758169e-15
		-15.74 10.275 -2.9073115011758169e-15
		-15.74 10.765000000000001 -2.9073115011758169e-15
		-15.34 11.335000000000001 -2.8583256292099227e-15
		-14.550000000000001 11.335000000000001 -2.7615785320772819e-15
		-13.779999999999999 11.335000000000001 -2.6672807285429357e-15
		-13.305 10.765000000000001 -2.6091100055834363e-15
		-13.305 10.120000000000001 -2.6091100055834363e-15
		-13.305 9.4900000000000002 -2.6091100055834363e-15
		-13.745000000000001 8.9299999999999997 -2.66299446474592e-15
		-14.585000000000001 8.9299999999999997 -2.7658647958742976e-15
		-14.975 8.9299999999999997 -2.8136260210410442e-15
		-15.43 9.0749999999999993 -2.8693474504022489e-15
		-15.880000000000001 9.3350000000000009 -2.9244565563638798e-15
		-15.813333333333334 9.4633333333333329 -2.916292244369564e-15
		-15.746666666666666 9.5916666666666668 -2.9081279323752485e-15
		-15.68 9.7200000000000006 -2.8999636203809327e-15
		;
createNode nurbsCurve -n "curveShape15" -p "particle_distanceCurve";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 7 0 no 3
		12 0 0 0 1 1 1 2 2 2 2.2970000000000002 2.2970000000000002
		 2.2970000000000002
		10
		-13.795 10.380000000000001 -2.6691176987416567e-15
		-13.845000000000001 10.515000000000001 -2.6752409327373934e-15
		-14 10.904999999999999 -2.6942229581241772e-15
		-14.555 10.904999999999999 -2.7621908554768556e-15
		-14.695 10.904999999999999 -2.7793359106649185e-15
		-15.145 10.895 -2.8344450166265494e-15
		-15.280000000000001 10.380000000000001 -2.8509777484150385e-15
		-14.785 10.380000000000001 -2.7903577318572447e-15
		-14.289999999999999 10.380000000000001 -2.7297377152994505e-15
		-13.795 10.380000000000001 -2.6691176987416567e-15
		;
createNode turbulenceField -n "particle_turbulenceField" -p "particle_particleController";
	setAttr ".att" 1;
	setAttr ".fc[0]"  0 1 1;
	setAttr ".amag[0]"  0 1 1;
	setAttr ".crad[0]"  0 1 1;
createNode expression -n "STnucleus1_EXP_WindGravityPlane";
	setAttr -k on ".nds";
	setAttr -s 21 ".in";
	setAttr -s 21 ".in";
	setAttr -s 20 ".out";
	setAttr ".ixp" -type "string" (
		"{\n\tif(.I[0]){\n\t\t.O[0] = 1;\n\t\t.O[1] = 1;\n\t\t.O[2] = 1;\n\t\tfloat $wloX = .I[1];\n\t\tfloat $wloY = .I[2];\n\t\tfloat $wloZ = .I[3];\n\t\tfloat $aloX = .I[4];\n\t\tfloat $aloY = .I[5];\n\t\tfloat $aloZ = .I[6];\n\t\tvector $wind = <<$wloX-$aloX,$wloY-$aloY,$wloZ-$aloZ>>;\n\t\t.O[3] = mag($wind);\n\t\tvector $windDir = unit($wind);\n\t\t.O[4] = $windDir.x;\n\t\t.O[5] = $windDir.y;\n\t\t.O[6] = $windDir.z;\n\t}else{\n\t\t.O[0] = 0;\n\t.O[1] = 0;\n\t\t.O[2] = 0;\n\t\t.O[3] = 0;\n\t}\n}\n\n{\n\tif(.I[7]){\n\t\t.O[7] = 1;\n\t\t.O[8] = 1;\n\t\tfloat $wloX = .I[8];\n\t\tfloat $wloY = .I[9];\n\t\tfloat $wloZ = .I[10];\n\t\tfloat $aloX = .I[4];\n\t\tfloat $aloY = .I[5];\n\t\tfloat $aloZ = .I[6];\n\t\tvector $g = <<$wloX-$aloX,$wloY-$aloY,$wloZ-$aloZ>>;\n\t\t.O[9] = mag($g);\n\t\tvector $gDir = unit($g);\n\t\t.O[10] = $gDir.x;\n\t\t.O[11] = $gDir.y;\n\t\t.O[12] = $gDir.z;\n\t}else{\n\t\t.O[7] = 0;\n\t.O[8] = 0;\n\t\t.O[9] = 9.8;\n\t\t.O[10] = 0;\n\t\t.O[11] = -1;\n\t\t.O[12] = 0;\n\t}\n}\n\n{\n\tif(.I[11]){\n\t\t.O[13] = 1;\n\t\t.O[14] = .I[12];\n\t\t.O[15] = .I[13];\n\t\t.O[16] = .I[14];\n\t\tfloat $wloX = .I[15];\n\t\tfloat $wloY = .I[16];\n\t\tfloat $wloZ = .I[17];\n"
		+ "\t\tfloat $aloX = .I[18];\n\t\tfloat $aloY = .I[19];\n\t\tfloat $aloZ = .I[20];\n\t\tvector $up = unit(<<$wloX-$aloX,$wloY-$aloY,$wloZ-$aloZ>>);\n\t\t.O[17] = $up.x;\n\t\t.O[18] = $up.y;\n\t\t.O[19] = $up.z;\n\t}else{\n\t\t.O[13] = 0;\n\t}\n}");
createNode expression -n "noiseExp1";
	setAttr -k on ".nds";
	setAttr -s 10 ".in";
	setAttr -s 10 ".in";
	setAttr -s 6 ".out";
	setAttr ".ixp" -type "string" "float $mxtx  = .I[0];\r\nfloat $mxty  = .I[1];\r\nfloat $mxtz  = .I[2];\r\nfloat $mxrx  = .I[3];\r\nfloat $mxry  = .I[4];\r\nfloat $mxrz  = .I[5];\r\nfloat $spdt  = .I[6];\r\nfloat $spdr  = .I[7];\r\nfloat $tot   = .I[8];\r\nfloat $tor   = .I[9];\r\nfloat $time = time;\r\n//\r\nfloat $toy = 12.3456789;\r\nfloat $toz = 23.4567891;\r\n//\r\nfloat $tx = noise( $time * $spdt + $tot)        * $mxtx;\r\nfloat $ty = noise( $time * $spdt + $tot + $toy) * $mxty;\r\nfloat $tz = noise( $time * $spdt + $tot + $toz) * $mxtz;\r\nfloat $rx = noise( $time * $spdr + $tor)        * $mxrx;\r\nfloat $ry = noise( $time * $spdr + $tor + $toy) * $mxry;\r\nfloat $rz = noise( $time * $spdr + $tor + $toz) * $mxrz;\r\n//\r\n.O[0] = $tx;\r\n.O[1] = $ty;\r\n.O[2] = $tz;.O[3] = $rx;\r\n.O[4] = $ry;\r\n.O[5] = $rz;";
	setAttr ".ani" 0;
createNode unitConversion -n "unitConversion1";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion2";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion3";
	setAttr ".cf" 0.017453292519943295;
createNode sqtMrTextureShader -n "CHagni_sqtMrTextureShader22";
createNode sqtMrTextureRemapShader -n "CHagni_sqtMrTextureRemapShader26";
createNode mentalrayTexture -n "CHagni_mentalrayTexture73";
	setAttr ".ftn" -type "string" "/cg/proj/black/element/CH/MasterMainCast/CHnoct/all/DH/textures/crowdman_disTest01.tif";
	setAttr ".mift" yes;
createNode geoConnector -n "geoConnector1";
createNode expression -n "particle_fieldExp";
	setAttr -k on ".nds";
	setAttr -s 10 ".in[9]"  1;
	setAttr -s 9 ".in";
	setAttr -s 7 ".out";
	setAttr ".ixp" -type "string" "float $noise = noise(time);\n//emitter setting\n\t.O[0] = .I[0];\n//particle setting\n\t.O[1] = .I[1];\n//radial setting\n\tfloat $raMin = .I[2];\n\tfloat $raMax = .I[3];\n\tfloat $rAt = ($noise + 1.0) / 2.0 * ($raMax - $raMin) + $raMin;\n\t.O[2] = $rAt;\n\t.O[3] = .I[4];\n\t.O[4] = .I[5];\n//turbulence setting\n\t.O[5] = .I[6];\n\tfloat $tfMin = .I[7];\n\tfloat $tfMax = .I[8];\n\tfloat $tF = ($noise + 1.0) / 2.0 * ($tfMax - $tfMin) + $tfMin;\n\t.O[6] = $tF;";
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 0;
	setAttr -av ".unw";
	setAttr -k on ".etw";
	setAttr -k on ".tps";
	setAttr -k on ".tms";
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
lockNode -l 1 ;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
lockNode -l 1 ;
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
select -ne :defaultTextureList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".tx";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr" 30;
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf" 5;
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -k on ".an" yes;
	setAttr -cb on ".ar";
	setAttr -k on ".fs" 1001;
	setAttr -k on ".ef" 1001;
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -k on ".pff" yes;
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".rv";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu" 1;
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mot";
	setAttr -av -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
select -ne :defaultRenderQuality;
	setAttr -k on ".cch";
	setAttr -k on ".nds";
	setAttr ".eaa" 0;
	setAttr -av ".mss";
	setAttr -k on ".mvs";
	setAttr -k on ".mvm";
	setAttr -k on ".vs";
	setAttr -k on ".pss";
	setAttr -k on ".rct";
	setAttr -k on ".gct";
	setAttr -k on ".bct";
	setAttr -k on ".cct";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w" 1344;
	setAttr -av -k on ".h" 756;
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar" 1.3333332538604736;
	setAttr -av -k on ".ldar";
	setAttr -cb on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -cb on ".isu";
	setAttr -cb on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr" 30;
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
select -ne :hardwareRenderingGlobals;
	setAttr ".vac" 2;
select -ne :defaultHardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".rp";
	setAttr -k on ".cai";
	setAttr -k on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -k on ".ei";
	setAttr -av -k on ".ex";
	setAttr -av -k on ".es";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bf";
	setAttr -k on ".fii";
	setAttr -av -k on ".sf";
	setAttr -k on ".gr";
	setAttr -k on ".li";
	setAttr -k on ".ls";
	setAttr -av -k on ".mb";
	setAttr -k on ".ti";
	setAttr -k on ".txt";
	setAttr -k on ".mpr";
	setAttr -k on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -k on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -k on ".as";
	setAttr -k on ".ds";
	setAttr -k on ".lm";
	setAttr -av -k on ".fir";
	setAttr -k on ".aap";
	setAttr -av -k on ".gh";
	setAttr -cb on ".sd";
connectAttr "STnClothGrp.distinction[1]" "const_STnucleus1.distinction";
connectAttr "STnucleus1_EXP_WindGravityPlane.out[3]" "STnucleus1.windSpeed";
connectAttr "STnucleus1_EXP_WindGravityPlane.out[4]" "STnucleus1.windDirectionX"
		;
connectAttr "STnucleus1_EXP_WindGravityPlane.out[5]" "STnucleus1.windDirectionY"
		;
connectAttr "STnucleus1_EXP_WindGravityPlane.out[6]" "STnucleus1.windDirectionZ"
		;
connectAttr "STnucleus1_EXP_WindGravityPlane.out[9]" "STnucleus1.gravity";
connectAttr "STnucleus1_EXP_WindGravityPlane.out[10]" "STnucleus1.gravityDirectionX"
		;
connectAttr "STnucleus1_EXP_WindGravityPlane.out[11]" "STnucleus1.gravityDirectionY"
		;
connectAttr "STnucleus1_EXP_WindGravityPlane.out[12]" "STnucleus1.gravityDirectionZ"
		;
connectAttr "STnucleus1_EXP_WindGravityPlane.out[14]" "STnucleus1.planeOriginX";
connectAttr "STnucleus1_EXP_WindGravityPlane.out[15]" "STnucleus1.planeOriginY";
connectAttr "STnucleus1_EXP_WindGravityPlane.out[16]" "STnucleus1.planeOriginZ";
connectAttr "STnucleus1_EXP_WindGravityPlane.out[17]" "STnucleus1.planeNormalX";
connectAttr "STnucleus1_EXP_WindGravityPlane.out[18]" "STnucleus1.planeNormalY";
connectAttr "STnucleus1_EXP_WindGravityPlane.out[19]" "STnucleus1.planeNormalZ";
connectAttr "STnClothGrp.distinction[0]" "STnucleus1.distinction";
connectAttr "STnucleus1_wind_LoSp.wm" "STnucleusShape1.dom" -na;
connectAttr "STnucleus1_EXP_WindGravityPlane.out[2]" "STnucleusShape1.daro";
connectAttr "STnClothGrp.distinction[2]" "STnucleus1_windConst.distinction";
connectAttr "STnucleus1_EXP_WindGravityPlane.out[1]" "windLength_nucleus1Shape.v"
		;
connectAttr "STnucleus1_wind.t" "windLength_nucleus1Shape.cp[1]";
connectAttr "windLength_nucleus1Shape.ws" "windLength_nucleus1DimensionShape.ng"
		;
connectAttr "noiseExp1.out[0]" "noiseT1.tx";
connectAttr "noiseExp1.out[1]" "noiseT1.ty";
connectAttr "noiseExp1.out[2]" "noiseT1.tz";
connectAttr "unitConversion1.o" "noiseT1.rx";
connectAttr "unitConversion2.o" "noiseT1.ry";
connectAttr "unitConversion3.o" "noiseT1.rz";
connectAttr "STnClothGrp.distinction[3]" "STnucleus1_wind.distinction";
connectAttr "STnucleus1_EXP_WindGravityPlane.out[0]" "STnucleus1_W.v";
connectAttr "STnClothGrp.distinction[4]" "STnucleus1_gravityConst.distinction";
connectAttr "STnucleus1_gravity_LoSp.wm" "STnucleus1_gravityArrowShape.dom" -na;
connectAttr "STnucleus1_EXP_WindGravityPlane.out[7]" "STnucleus1_gravityArrowShape.daro"
		;
connectAttr "STnClothGrp.distinction[5]" "STnucleus1_gravity.distinction";
connectAttr "STnucleus1_EXP_WindGravityPlane.out[8]" "gravityLength_nucleus1Shape.v"
		;
connectAttr "STnucleus1_gravity.tx" "gravityLength_nucleus1Shape.cp[1].xv";
connectAttr "STnucleus1_gravity.ty" "gravityLength_nucleus1Shape.cp[1].yv";
connectAttr "STnucleus1_gravity.tz" "gravityLength_nucleus1Shape.cp[1].zv";
connectAttr "gravityLength_nucleus1Shape.ws" "gravityLength_nucleus1DimensionShape.ng"
		;
connectAttr "STnClothGrp.distinction[6]" "STnucleus1_planeConst.distinction";
connectAttr "STnucleus1_EXP_WindGravityPlane.out[13]" "STnucleus1_plane.v";
connectAttr "STnClothGrp.distinction[7]" "STnucleus1_plane.distinction";
connectAttr ":time1.o" "particle_emitter.ct";
connectAttr "geoConnector1.ocd" "particle_emitter.ocd";
connectAttr "geoConnector1.ocl" "particle_emitter.t";
connectAttr "geoConnector1.pos" "particle_emitter.opd";
connectAttr "geoConnector1.vel" "particle_emitter.ovd";
connectAttr "geoConnector1.swg" "particle_emitter.swge";
connectAttr "particle_particleShape.ifl" "particle_emitter.full[0]";
connectAttr "particle_particleShape.tss" "particle_emitter.dt[0]";
connectAttr "particle_particleShape.inh" "particle_emitter.inh[0]";
connectAttr "particle_particleShape.stt" "particle_emitter.stt[0]";
connectAttr "particle_particleShape.sd[0]" "particle_emitter.sd[0]";
connectAttr "particle_fieldExp.out[0]" "particle_emitter.rat";
connectAttr "particle_fieldExp.out[1]" "particle_particleShape.lifespan";
connectAttr ":time1.o" "particle_particleShape.cti";
connectAttr "particle_emitter.ot[0]" "particle_particleShape.npt[0]";
connectAttr "particle_particleShape.xo[0]" "particle_particleShape.vel";
connectAttr "particle_particleController.PartStartFrame" "particle_particleShape.xi[0]"
		;
connectAttr "particle_particleController.PartSpdMin" "particle_particleShape.xi[1]"
		;
connectAttr "particle_particleController.PartSpdMax" "particle_particleShape.xi[2]"
		;
connectAttr ":time1.o" "particle_particleShape.tim";
connectAttr "particle_particleController.PartStartFrame" "particle_particleShape.stf"
		;
connectAttr "particle_particleShape.cwcn" "particle_radialField.ocd";
connectAttr "particle_particleShape.ctd" "particle_radialField.t";
connectAttr "particle_particleShape.cwps" "particle_radialField.opd";
connectAttr "particle_particleShape.cwvl" "particle_radialField.ovd";
connectAttr "particle_particleShape.opfd[0]" "particle_radialField.oppd";
connectAttr "particle_fieldExp.out[2]" "particle_radialField.att";
connectAttr "particle_fieldExp.out[3]" "particle_radialField.mag";
connectAttr "particle_fieldExp.out[4]" "particle_radialField.max";
connectAttr "particle_fieldExp.out[5]" "particle_turbulenceField.mag";
connectAttr "particle_fieldExp.out[6]" "particle_turbulenceField.frq";
connectAttr "STnucleus1.useWind" "STnucleus1_EXP_WindGravityPlane.in[0]";
connectAttr "STnucleus1_wind_LoSp.wp.wpx" "STnucleus1_EXP_WindGravityPlane.in[1]"
		;
connectAttr "STnucleus1_wind_LoSp.wp.wpy" "STnucleus1_EXP_WindGravityPlane.in[2]"
		;
connectAttr "STnucleus1_wind_LoSp.wp.wpz" "STnucleus1_EXP_WindGravityPlane.in[3]"
		;
connectAttr "STnucleus1_LoSp.wp.wpx" "STnucleus1_EXP_WindGravityPlane.in[4]";
connectAttr "STnucleus1_LoSp.wp.wpy" "STnucleus1_EXP_WindGravityPlane.in[5]";
connectAttr "STnucleus1_LoSp.wp.wpz" "STnucleus1_EXP_WindGravityPlane.in[6]";
connectAttr "STnucleus1.useGravityAnim" "STnucleus1_EXP_WindGravityPlane.in[7]";
connectAttr "STnucleus1_gravity_LoSp.wp.wpx" "STnucleus1_EXP_WindGravityPlane.in[8]"
		;
connectAttr "STnucleus1_gravity_LoSp.wp.wpy" "STnucleus1_EXP_WindGravityPlane.in[9]"
		;
connectAttr "STnucleus1_gravity_LoSp.wp.wpz" "STnucleus1_EXP_WindGravityPlane.in[10]"
		;
connectAttr "STnucleus1.usePlane" "STnucleus1_EXP_WindGravityPlane.in[11]";
connectAttr "STnucleus1_plane.tx" "STnucleus1_EXP_WindGravityPlane.in[12]";
connectAttr "STnucleus1_plane.ty" "STnucleus1_EXP_WindGravityPlane.in[13]";
connectAttr "STnucleus1_plane.tz" "STnucleus1_EXP_WindGravityPlane.in[14]";
connectAttr "STnucleus1_planeUp_LoSp.wp.wpx" "STnucleus1_EXP_WindGravityPlane.in[15]"
		;
connectAttr "STnucleus1_planeUp_LoSp.wp.wpy" "STnucleus1_EXP_WindGravityPlane.in[16]"
		;
connectAttr "STnucleus1_planeUp_LoSp.wp.wpz" "STnucleus1_EXP_WindGravityPlane.in[17]"
		;
connectAttr "STnucleus1_planeBase_LoSp.wp.wpx" "STnucleus1_EXP_WindGravityPlane.in[18]"
		;
connectAttr "STnucleus1_planeBase_LoSp.wp.wpy" "STnucleus1_EXP_WindGravityPlane.in[19]"
		;
connectAttr "STnucleus1_planeBase_LoSp.wp.wpz" "STnucleus1_EXP_WindGravityPlane.in[20]"
		;
connectAttr ":time1.o" "STnucleus1_EXP_WindGravityPlane.tim";
connectAttr "STnucleus1.msg" "STnucleus1_EXP_WindGravityPlane.obm";
connectAttr "noiseT1.mxtx" "noiseExp1.in[0]";
connectAttr "noiseT1.mxty" "noiseExp1.in[1]";
connectAttr "noiseT1.mxtz" "noiseExp1.in[2]";
connectAttr "noiseT1.mxrx" "noiseExp1.in[3]";
connectAttr "noiseT1.mxry" "noiseExp1.in[4]";
connectAttr "noiseT1.mxrz" "noiseExp1.in[5]";
connectAttr "noiseT1.spdt" "noiseExp1.in[6]";
connectAttr "noiseT1.spdr" "noiseExp1.in[7]";
connectAttr "noiseT1.tot" "noiseExp1.in[8]";
connectAttr "noiseT1.tor" "noiseExp1.in[9]";
connectAttr ":time1.o" "noiseExp1.tim";
connectAttr "noiseExp1.out[3]" "unitConversion1.i";
connectAttr "noiseExp1.out[4]" "unitConversion2.i";
connectAttr "noiseExp1.out[5]" "unitConversion3.i";
connectAttr "CHagni_sqtMrTextureRemapShader26.S00" "CHagni_sqtMrTextureShader22.S08"
		;
connectAttr "CHagni_sqtMrTextureRemapShader26.S01" "CHagni_sqtMrTextureShader22.S09"
		;
connectAttr "CHagni_sqtMrTextureRemapShader26.S02" "CHagni_sqtMrTextureShader22.S10"
		;
connectAttr "CHagni_sqtMrTextureRemapShader26.S03" "CHagni_sqtMrTextureShader22.S11"
		;
connectAttr "CHagni_sqtMrTextureRemapShader26.S04" "CHagni_sqtMrTextureShader22.S12"
		;
connectAttr "CHagni_sqtMrTextureRemapShader26.S05" "CHagni_sqtMrTextureShader22.S13"
		;
connectAttr "CHagni_sqtMrTextureRemapShader26.S06" "CHagni_sqtMrTextureShader22.S14"
		;
connectAttr "CHagni_sqtMrTextureRemapShader26.S07" "CHagni_sqtMrTextureShader22.S15"
		;
connectAttr "CHagni_sqtMrTextureRemapShader26.S08" "CHagni_sqtMrTextureShader22.S16"
		;
connectAttr "CHagni_sqtMrTextureRemapShader26.S09" "CHagni_sqtMrTextureShader22.S17"
		;
connectAttr "CHagni_mentalrayTexture73.msg" "CHagni_sqtMrTextureShader22.s02";
connectAttr ":time1.o" "geoConnector1.ct";
connectAttr "particle_particleSurfaceShape.l" "geoConnector1.lge";
connectAttr "particle_particleSurfaceShape.wm" "geoConnector1.wm";
connectAttr "particle_particleSurfaceShape.msg" "geoConnector1.own";
connectAttr ":time1.o" "particle_fieldExp.tim";
connectAttr "particle_particleController.EmitRate" "particle_fieldExp.in[0]";
connectAttr "particle_particleController.PartLifeSpan" "particle_fieldExp.in[1]"
		;
connectAttr "particle_particleController.RadiAttenMin" "particle_fieldExp.in[2]"
		;
connectAttr "particle_particleController.RadiAttenMax" "particle_fieldExp.in[3]"
		;
connectAttr "particle_particleController.RadiPower" "particle_fieldExp.in[4]";
connectAttr "particle_particleController.RadiDistance" "particle_fieldExp.in[5]"
		;
connectAttr "particle_particleController.TurbPower" "particle_fieldExp.in[6]";
connectAttr "particle_particleController.TurbFreqMin" "particle_fieldExp.in[7]";
connectAttr "particle_particleController.TurbFreqMax" "particle_fieldExp.in[8]";
connectAttr "particle_particleSurfaceShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "CHagni_sqtMrTextureShader22.S19" ":initialShadingGroup.mids";
connectAttr "particle_particleShape.iog" ":initialParticleSE.dsm" -na;
connectAttr "CHagni_sqtMrTextureShader22.msg" ":defaultTextureList1.tx" -na;
connectAttr "CHagni_mentalrayTexture73.msg" ":defaultTextureList1.tx" -na;
connectAttr "CHagni_sqtMrTextureRemapShader26.msg" ":defaultRenderUtilityList1.u"
		 -na;
// End of clothWindSet.v1.ma
