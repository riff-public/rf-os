import sys
import os
import commands
import maya.cmds as mc
import maya.mel as mm
import re
import fnmatch
import ast

class Callback(object):
	def __init__(self,func,*args,**kwargs):
		self.func = func
		self.args = args
		self.kwargs = kwargs
	def __call__(self,*args):
		return self.func(*self.args,**self.kwargs)

class getObjName(object):
	def __init__(self):
		self.fullpathName = ''
	def getRealName(self,fullpathName):
		self.objName = fullpathName.split('|')
		return self.objName[len(self.objName)-1]
    		
class checkVersion(object):
	
	def __init__ (self):
		self.currentVersion = ''
		
	def check(self,currentVersion):	
		self.currentVersion = currentVersion
		match = re.search('(v\d+)_(\d+)',currentVersion)
		self.mainVersion = int(re.split('v',match.group(1))[1])
		self.subVersion = int(match.group(2))
		return self.mainVersion , self.subVersion

class folderInfo(object):
	
	def __init__ (self):
		self.path = ''
		
	def check(self,path):
		self.path = path
		if not os.path.exists(self.path):
			return False
		elif os.path.exists(self.path):
			return True
			
	def make(self,path):
		self.path = path
		if not os.path.exists(self.path):
			os.makedirs(self.path)
			return self.path
		
	def listItemInFolder(self,path):
		self.path = path
		if os.path.exists(self.path) == True :
			self.element = os.listdir(self.path)
			return self.element
		else:
			return None
			
	def delFile(self,path):
		self.path = path
		if os.path.isfile(self.path) == True :
			print self.path +' : Is regular file'
			os.remove(self.path)
			print '## Remove File Complete !!'
			return True
		else:
			print 'This path : "'+self.path+'" is not regular file'
			print '## cannot Remove !!'
			return False
			
	def browsFileDir(self,path = '',typeSel = 1 , fillter = ''):
		self.path = path
		self.typeSel = typeSel
		self.fillter = fillter
		self.targetDir = mc.fileDialog2(  ff = self.fillter , fm=self.typeSel, okCaption="Select" , startingDirectory = self.path )
		if self.targetDir == None:
			return None
		else:
			return self.targetDir
			
	def findObj(self,Dir,keyword):
		self.rootDir = Dir
		self.keyword = keyword
		matches=[]
		for dirName, subDirList, fileList in os.walk(self.rootDir,topdown=False):
			for fn in fnmatch.filter(fileList, self.keyword):
				matches.append(os.path.join(dirName, fn))
		#print len(matches)
	
		if len(matches) > 0:
			return matches
		
		else :
			return None
		
class getDBScene(object):
	
	def __init__ (self):
		self.simPathDict = {}
		self.frameRange = []
		self.pathDict = {}
		#self.yumDBQ = dbq.databaseQuery()
		#self.yumPDT = pdt.production(self.yumDBQ)
		
	def get(self):
		self.currentScene = mc.file(query=True, sceneName=True)
		try :
			#self.pathDict = self.yumPDT.expandPath(self.currentScene)
			self.simPathDict['job'] = self.pathDict['job']
			self.simPathDict['class'] = self.pathDict['class']
			self.simPathDict['scene'] = self.pathDict['scene']
			self.simPathDict['shot'] = self.pathDict['shot']
			self.simPathDict['elemHN'] = self.pathDict['elemHN']
			self.simPathDict['elemGN'] = self.pathDict['elemGN']
			self.shotInfo = self.yumDBQ.getShotInfo(self.pathDict['job'], self.pathDict['class'], self.pathDict['scene'], self.pathDict['shot'])
			if self.shotInfo != None:
				self.simPathDict['shotfs'] = self.shotInfo['shotfs']
				self.simPathDict['shotfe'] = self.shotInfo['shotfe']
			else :
				self.simPathDict['shotfs'] = mc.playbackOptions(q = True , min = True)
				self.simPathDict['shotfe'] = mc.playbackOptions(q = True , max = True)
			return self.simPathDict
			
		except :
			self.simPathDict['job'] = ''
			self.simPathDict['class'] = ''
			self.simPathDict['scene'] = ''
			self.simPathDict['shot'] = ''
			self.simPathDict['elemHN'] = ''
			self.simPathDict['elemGN'] = ''
			self.simPathDict['shotfs'] = 0
			self.simPathDict['shotfe'] = 1
			return self.simPathDict

class progressWindows(object):
	NAME = ''
	PROGRESS_BAR_UI = ''
	TEXT_UI = ''
	TITLE = 'Progress Window'
	DISPLAY = 'Progress : '
	MAX_VALUE = 100
	VALUE = 0
	WIDTH = 260
	HEIGHT = 65
	
	def __init__ (self ,title = 'Progress Window' , displayText = 'Progress : ',maxValue = 100):
		self.TITLE = title
		self.DISPLAY = displayText
		self.MAX_VALUE = maxValue
		self.NAME = mc.window(title = self.TITLE,s = 0)
		self.formLayout_UI = mc.formLayout()
		self.TEXT_UI = mc.text( l = self.DISPLAY,p = self.formLayout_UI)
		self.PROGRESS_BAR_UI = mc.progressBar(maxValue = self.MAX_VALUE,width = (self.WIDTH-30), p = self.formLayout_UI)
		mc.formLayout(self.formLayout_UI, e = True , af = [(self.TEXT_UI,'top',5),(self.TEXT_UI,'left',1),(self.TEXT_UI,'right',1),(self.PROGRESS_BAR_UI,'right',10),(self.PROGRESS_BAR_UI,'left',10)],ac = [(self.PROGRESS_BAR_UI,'top',5,self.TEXT_UI)])
		mc.showWindow(self.NAME)
		mc.window(self.NAME , e = True , width = self.WIDTH , height = self.HEIGHT ,s = 0)
	
	def update(self,amount = 1):
		if mc.window(self.NAME , q = True , ex = True):
			if mc.progressBar(self.PROGRESS_BAR_UI , q = True , ex = True):
				self.VALUE += float(amount)
				mc.text(self.TEXT_UI, e = True , l = (self.DISPLAY))
				mc.progressBar(self.PROGRESS_BAR_UI , e = True , progress = self.VALUE)
				if self.VALUE >= self.MAX_VALUE:
					self.kill()
	def reset(self,displayText = 'Progress : '):
		if mc.window(self.NAME , q = True , ex = True):
			if mc.progressBar(self.PROGRESS_BAR_UI , q = True , ex = True):
				self.DISPLAY = displayText
				self.VALUE = 0
				mc.progressBar(self.PROGRESS_BAR_UI , e = True , progress = self.VALUE)
				mc.text(self.TEXT_UI, e = True , l = (self.DISPLAY + `self.VALUE` + ' %'))
	def kill(self):
		if mc.window(self.NAME , q = True , ex = True):
			mc.deleteUI(self.NAME)
			
class progressBars(object):
	NAME = ''
	PROGRESS_BAR_UI = ''
	TITLE = 'Progress Window'
	DISPLAY = 'Progress : '
	MAX_VALUE = 100
	VALUE = 0
	
	def __init__ (self ,title = 'Progress Window', displayText = 'Progress : ',maxValue = 100):
		self.DISPLAY = displayText
		self.MAX_VALUE = maxValue
		self.gMainProgressBar = mm.eval('$tmp = $gMainProgressBar')
		self.PROGRESS_BAR_UI = mc.progressBar( self.gMainProgressBar,edit=True,beginProgress=True, isInterruptable=True, status = self.DISPLAY, maxValue = self.MAX_VALUE )
	
	def update(self,amount = 1):
		
		self.VALUE += float(amount)
		mc.progressBar(self.gMainProgressBar, edit=True, pr=self.VALUE , status = self.DISPLAY)
		if self.VALUE > self.MAX_VALUE:
			self.kill()
			
	def kill(self):
		mc.progressBar(self.gMainProgressBar, edit=True, endProgress=True)
	
	
class paintLoop(object):
	def __init__(self):
		self.operation = 'smooth'
		self.time = 1
	def doPaint(self,operation = 'smooth',time = 1):
		self.current = mc.currentCtx()
		self.operation = operation
		self.time = time
		mc.setToolTo(self.current)
		for eachTime in xrange(self.time):
			mc.artAttrCtx(self.current, e = True ,clear = True , sao = self.operation)
			
class renameSimNode(object):
	def __init__ (self):
		self.type = ''
		
	def doRenameSimNode(self):
		self.sel = mc.ls(sl = True)
		for each in self.sel:
			shp = mc.listRelatives(each)
			objType = mc.objectType(shp)
			rigid = mc.listConnections(shp, t = 'mesh')[0]
			mc.rename(each,rigid + '_' + objType)

class DataFld(object):
	def __init__(self):
		self.path = ''
		
	def getDataFld(self):
		files = mc.file(q = True,sn = True)
		tmpAry = files.split('/')
		tmpAry[-2] = 'data'
		dataFld = ('/').join(tmpAry[0:-1])
		if not os.path.exists(dataFld):
			os.makedirs(dataFld)
		self.path = dataFld
		return self.path
		
		
class ConvertVariable(object):
	def __init__(self):
		self.var = ''
		
	def get(self,string):
		try:
			return ast.literal_eval(string)
		except:
			return string
			
class Indent(object):
	def __init__(self):
		self.elem = ''
		
	def doIndent(self,elem, level = 0):
		i = "\n" + level*" "
		if len(elem):
			if not elem.text or not elem.text.strip():
				elem.text = i + " "
			if not elem.tail or not elem.tail.strip():
				elem.tail = i
			for elem in elem:
				self.doIndent(elem,level + 1)
			if not elem.tail or not elem.tail.strip():
				elem.tail = i
		else :
			if level and (not elem.tail or not elem.tail.strip()):
				elem.tail = i

class CheckboxPrompt(object):
	def __init__(self):
		self.text = ''
		self.button = {}
		import maya.cmds as mc

	def mainUI(self): 
		# Get the dialog's formLayout.
		#
		form = mc.setParent(q=True)

		# layoutDialog's are not resizable, so hard code a size here,
		# to make sure all UI elements are visible.
		#
		mc.formLayout(form, e=True, width=300)

		t = mc.text(l=self.text)

		b1 = mc.button(l=self.button['b1'], c='maya.cmds.layoutDialog( dismiss="'+self.button['b1']+'" )' )
		b2 = mc.button(l=self.button['b2'], c='maya.cmds.layoutDialog( dismiss="'+self.button['b2']+'" )' )
		b3 = mc.button(l=self.button['b3'], c='maya.cmds.layoutDialog( dismiss="'+self.button['b3']+'" )' )

		spacer = 5
		top = 5
		edge = 5

		mc.formLayout(form, edit=True,
		                                attachForm=[(t, 'top', top+20), (t, 'left', edge), (t, 'right', edge), (b1, 'left', edge), (b3, 'right', edge)],
		                                attachNone=[(t, 'bottom'), (b1, 'bottom'), (b2, 'bottom'), (b3, 'bottom')],
		                                attachControl=[(b1, 'top', spacer+20, t), (b2, 'top', spacer+20, t), (b3, 'top', spacer+20, t)],
		                                attachPosition=[(b1, 'right', spacer, 33), (b2, 'left', spacer, 33), (b2, 'right', spacer, 66), (b3, 'left', spacer, 66)])
	
	def doCreateUI(self,text = 'This file is Exits , What do you want to do?',button = {'b1':'Append','b2':'Increment','b3':'No'}):
		self.text = text
		self.button = button
		result = mc.layoutDialog(ui=self.mainUI)
		return result