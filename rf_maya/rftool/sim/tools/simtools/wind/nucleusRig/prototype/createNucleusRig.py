import os ;
import pymel.core as pm ;

selfPath = os.path.realpath (__file__) ;
fileName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( fileName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;


def createCurve ( *args ) :

    selectedCurve = pm.textScrollList ( 'curveList_textScrollList' , q = True , selectItem = True ) [0] + '.txt' ;    
    curveTxtPath = curvePath + '/' + selectedCurve ;

    curveTxt = open ( curveTxtPath ) ;
    text = curveTxt.read ( ) ;
    point = ast.literal_eval ( text ) ;
    curveTxt.close ( ) ;

    pm.curve ( d = 1 , p = point ) ;

# curveShape [worldMatrix] >>>  vectorProduct [matrix]
# op == vector matrix product
# input1 == ( 0 , -1 , 0 )
# output >> gravity direction