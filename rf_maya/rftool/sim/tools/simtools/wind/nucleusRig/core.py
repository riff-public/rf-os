### revamped version , 2018/10/10 , (c) RiFF Animation Studio, Weerapot C. ### 

import pymel.core as pm ;

class General ( object ) :

    def __init__ ( self ) :
        super ( General , self ).__init__() ;

        self.colorDict = {} ;
        self.colorDict [ 'green' ]      = ( 0 , 1 , 0 ) ;       
        self.colorDict [ 'lightBlue' ]  = ( 0 , 1 , 1 ) ;
        self.colorDict [ 'red' ]        = ( 1 , 0 , 0 ) ;
        self.colorDict [ 'yellow' ]     = ( 1 , 1 , 0 ) ;
        self.colorDict [ 'white' ]      = ( 1 , 1 , 1 ) ;

    def clean ( self , target ) :
        # freeze transform, delete history
        target = pm.PyNode ( target ) ;
        pm.makeIdentity ( target , apply = True ) ;
        pm.delete ( target , ch = True ) ;

    def lockHideAttr ( self , target , attr ) :
        target = pm.PyNode ( target ) ;
        pm.setAttr ( target + '.' + attr , lock = True , keyable = False , channelBox = False ) ;

    def lockAllAttr ( self , target ) :
        target = pm.PyNode ( target ) ;
        for attr in [ 't' , 'r' , 's' ] :
            for axis in [ 'x' , 'y' , 'z' ] :
                pm.setAttr ( target + '.' + attr + axis , lock = True ) ;

    def setColor ( self , target , color ) :

        target      = pm.PyNode ( target ) ;
        targetShape = target.getShape() ; 

        targetShape.overrideEnabled.set ( True ) ;
        targetShape.overrideRGBColors.set ( True ) ;

        targetShape.overrideColorRGB.set ( color ) ;

class NucleusRig ( object ) :

    def __init__ ( self ) :

        super ( NucleusRig , self ).__init__() ;

        self.placementCtrl_point = [(-7.9960873737160485, 0.0, 1.9990218866105116), (-7.9960873737160485, 0.0, 3.9980437732210232), (-11.994131146937072, 0.0, 0.0), (-7.9960873737160485, 0.0, -3.9980437732210232), (-7.9960873737160485, 0.0, -1.9990218866105116), (-3.9980437732210232, 0.0, -1.9990218866105116), (-1.9990218866105116, 0.0, -3.9980437732210232), (-1.9990218866105116, 0.0, -7.9960873737160485), (-3.9980437732210232, 0.0, -7.9960873737160485), (0.0, 0.0, -11.994131146937072), (3.9980437732210232, 0.0, -7.9960873737160485), (1.9990218866105116, 0.0, -7.9960873737160485), (1.9990218866105116, 0.0, -3.9980437732210232), (3.9980437732210232, 0.0, -1.9990218866105116), (7.9960873737160485, 0.0, -1.9990218866105116), (7.9960873737160485, 0.0, -3.9980437732210232), (11.994131146937072, 0.0, 0.0), (7.9960873737160485, 0.0, 3.9980437732210232), (7.9960873737160485, 0.0, 1.9990218866105116), (3.9980437732210232, 0.0, 1.9990218866105116), (1.9990218866105116, 0.0, 3.9980437732210232), (1.9990218866105116, 0.0, 7.9960873737160485), (3.9980437732210232, 0.0, 7.9960873737160485), (0.0, 0.0, 11.994131146937072), (-3.9980437732210232, 0.0, 7.9960873737160485), (-1.9990218866105116, 0.0, 7.9960873737160485), (-1.9990218866105116, 0.0, 3.9980437732210232), (-3.9980437732210232, 0.0, 1.9990218866105116), (-7.9960873737160485, 0.0, 1.9990218866105116)] ;

        self.nucleusCtrl_point = [(-1.0295264036548118e-07, 0.0, 3.4545203108734968), (0.0, 0.0, 0.0), (-1.0295264036548118e-07, 0.0, 3.4545203108734968), (1.0675052234286573, 0.0, 3.2854440129458897), (2.030515881505268, 0.0, 2.7947657243174024), (2.794765378865406, 0.0, 2.030516054231266), (3.2854440129458897, 0.0, 1.067505396154656), (3.4545199654215, 0.0, 0.0), (0.0, 0.0, 0.0), (3.4545199654215, 0.0, 0.0), (3.2854459129318703, 0.0, -1.0675060870586488), (2.794767278851387, 0.0, -2.0305172633132544), (2.0305172633132544, 0.0, -2.79476693339939), (1.0675059143326504, 0.0, -3.285445567479874), (0.0, 0.0, -3.454521692681483), (0.0, 0.0, 0.0), (0.0, 0.0, -3.454521692681483), (-1.0675059143326504, 0.0, -3.285445222027878), (-2.0305167451352597, 0.0, -2.794766415221396), (-2.794766415221396, 0.0, -2.0305165724092613), (-3.285444531123885, 0.0, -1.067505568880654), (-3.4545206563254935, 0.0, 0.0), (0.0, 0.0, 0.0), (-3.4545206563254935, 0.0, 0.0), (-3.285444531123885, 0.0, 1.067505568880654), (-2.7947660697693992, 0.0, 2.030516226957264), (-2.030516226957264, 0.0, 2.7947657243174024), (-1.067505568880654, 0.0, 3.2854441856718877), (-1.0295264036548118e-07, 0.0, 3.4545203108734968), (0.0, 0.0, 0.0), (0.0, -6.909039930843, 0.0), (-2.146155073717761e-08, -6.909039930843, 0.7201309592517486), (0.0, -7.629170717368751, 0.0), (-0.7201311319777468, -6.909039930843, 0.0), (0.0, -6.909039930843, 0.0), (0.0, -6.909039930843, -0.7201313047037451), (0.0, -7.629170717368751, 0.0), (0.7201309592517486, -6.909039930843, 0.0), (0.0, -6.909039930843, 0.0), (0.7201309592517486, -6.909039930843, 0.0), (0.684885701126541, -6.909039930843, -0.2225327489545274), (0.5825985740323983, -6.909039930843, -0.42328267681509296), (0.42328267681509296, -6.909039930843, -0.5825985740323983), (0.2225327489545274, -6.909039930843, -0.6848855284005427), (0.0, -6.909039930843, -0.7201313047037451), (-0.2225327489545274, -6.909039930843, -0.6848855284005427), (-0.4232825040890947, -6.909039930843, -0.5825984013064), (-0.5825984013064, -6.909039930843, -0.4232825040890947), (-0.6848853556745446, -6.909039930843, -0.2225327489545274), (-0.7201311319777468, -6.909039930843, 0.0), (-0.6848853556745446, -6.909039930843, 0.2225327489545274), (-0.5825982285804017, -6.909039930843, 0.4232825040890947), (-0.4232825040890947, -6.909039930843, 0.5825982285804017), (-0.2225327489545274, -6.909039930843, 0.6848853556745446), (-2.146155073717761e-08, -6.909039930843, 0.7201309592517486), (0.2225327489545274, -6.909039930843, 0.6848853556745446), (0.42328233136309645, -6.909039930843, 0.5825982285804017), (0.5825982285804017, -6.909039930843, 0.42328233136309645), (0.6848851829485462, -6.909039930843, 0.2225327489545274), (0.7201309592517486, -6.909039930843, 0.0)] ;

        self.nucleusGimbalCtrl_point = [(1.6837159312726104, 0.0, -1.6837159312726104), (0.0, -7.629170717368751, 0.0), (-1.6837159312726104, 0.0, -1.6837159312726104), (0.0, -7.629170717368751, 0.0), (-1.6837159312726104, 0.0, 1.6837159312726104), (0.0, -7.629170717368751, 0.0), (1.6837159312726104, 0.0, 1.6837159312726104), (1.6837159312726104, 0.0, -1.6837159312726104), (-1.6837159312726104, 0.0, -1.6837159312726104), (-1.6837159312726104, 0.0, 1.6837159312726104), (1.6837159312726104, 0.0, 1.6837159312726104)] ;

        self.windCtrl_point = [(0.0, 6.588973933070183e-07, -7.228105769354802e-08), (-1.5128522073751853e-08, -0.1649381605308827, 0.2890061545607577), (-2.0822624464376195e-08, -0.3560010673522567, 0.5226969391353501), (-2.447846479163058e-08, -0.5967536503816153, 0.6766029766369752), (-2.57381794260554e-08, -0.863629991355375, 0.7339259170190722), (-2.447846479163058e-08, -1.1305063323291347, 0.6766029766369752), (-2.0822624464376195e-08, -1.3712589565395807, 0.5226969391353501), (-1.5128522073751853e-08, -1.5623213280068224, 0.2890061545607577), (0.0, -1.7272598179864018, -7.228105769354802e-08), (0.0, -1.8921984726903296, -0.2890063524364208), (0.0, -2.0832609265197455, -0.5226972177255514), (0.0, -2.3240136330923655, -0.6766033762989837), (0.0, -2.590889974066125, -0.7339262359665425), (0.0, -2.8577661503155367, -0.6766033762989837), (0.0, -3.098519021612505, -0.5226972177255514), (0.0, -3.289581475441921, -0.2890063524364208), (0.0, -3.4545199654215, -7.228105769354802e-08), (-1.5128522073751853e-08, -3.6194584554010794, 0.2890061545607577), (-2.0822624464376195e-08, -3.810520909230496, 0.5226969391353501), (-2.447846479163058e-08, -4.051273780527464, 0.6766029766369752), (-2.57381794260554e-08, -4.3181499567768755, 0.7339259170190722), (-2.447846479163058e-08, -4.585026133026287, 0.6766029766369752), (-2.0822624464376195e-08, -4.825779004323256, 0.5226969391353501), (-1.5128522073751853e-08, -5.016841458152671, 0.2890061545607577), (0.0, -5.18177994813225, -7.228105769354802e-08), (0.0, -5.346718108663133, -0.2890063524364208), (0.0, -5.537780891941246, -0.5226972177255514), (0.0, -5.778533433789517, -0.6766033762989837), (0.0, -6.045409939487626, -0.7339262359665425), (0.0, -6.312286445185733, -0.6766033762989837), (0.0, -6.553038987034006, -0.5226972177255514), (0.0, -6.744102099760815, -0.2890063524364208), (0.0, -6.909041248637787, -7.228105769354802e-08)] ;

        self.posDict = {} ;
        self.posDict [ 'up' ]       = [ 'tx' , 10 ] ;
        self.posDict [ 'down' ]     = [ 'tx' , -10 ] ;
        self.posDict [ 'left' ]     = [ 'tz' , 10 ] ;
        self.posDict [ 'right' ]    = [ 'tz' , -10 ] ;

    def createNucleusRigGroup ( self ) :

        nucleusRigGroupName = 'NucleusRig_Grp' ;

        if not pm.objExists ( nucleusRigGroupName ) :
            nucleusRigGrp = pm.group ( em = True , w = True , n = nucleusRigGroupName ) ;
        else :
            nucleusRigGrp = pm.PyNode ( nucleusRigGroupName ) ;

        return nucleusRigGrp ;

    def createCtrl ( self , name , point ) :
        # return ctrl , zroGrp

        ctrl = pm.curve ( d = 1 , point = point ) ;
        ctrl.rename ( name + '_Ctrl' ) ;

        for attr in [ 'sx' , 'sy' , 'sz' ] :
            self.clean ( target = ctrl ) ;
            # self.lockHideAttr ( target = ctrl , attr = attr ) ;

        ctrlZroGrp = pm.group ( em = True , w = True , n = name + 'CtrlZro_Grp' ) ;
        # self.lockAllAttr ( target = ctrlZroGrp ) ;

        pm.parent ( ctrl , ctrlZroGrp ) ;

        return ctrl , ctrlZroGrp ;

    def createNucleusIndividualRig ( self , nucleus ) :
        # return currentNucleusRigGrp , nucleusCtrl , windCtrl ;

        nucleusRigGrp = self.createNucleusRigGroup() ;

        currentNucleusRigGrp = pm.group ( em = True , n = nucleus.nodeName() + 'Rig_Grp' ) ;
        pm.parent ( currentNucleusRigGrp , nucleusRigGrp ) ;

        nucleus = pm.PyNode ( nucleus ) ;

        ### Nucleus Ctrl ###

        product_list    = self.createCtrl ( name = nucleus.nodeName() , point = self.nucleusCtrl_point ) ;
        nucleusCtrl     = product_list[0] ;
        nucleusCtrlShape = nucleusCtrl.getShape() ;
        nucleusCtrlZro  = product_list[1] ;

        product_list = self.createCtrl ( name = nucleus.nodeName() + 'Gmbl' , point = self.nucleusGimbalCtrl_point ) ;
        nucleusGmblCtrl     = product_list[0] ;
        nucleusGmblCtrlShape = nucleusGmblCtrl.getShape() ;
        self.setColor ( target = nucleusGmblCtrl , color = self.colorDict['white'] ) ;
        nucleusGmblCtrlZro  = product_list[1] ;

        pm.parent ( nucleusGmblCtrlZro , nucleusCtrl ) ;

        nucleusCtrlShape.addAttr ( 'gimbalCtrl' , keyable = True , attributeType = 'bool' ) ;
        nucleusCtrlShape.gimbalCtrl >> nucleusGmblCtrlShape.v ;

        gravityVtp = pm.createNode ( 'vectorProduct' ) ;
        gravityVtp.rename ( nucleus.nodeName() + 'Gravity_Vtp' ) ;

        nucleusGmblCtrl.worldMatrix >> gravityVtp.matrix ;

        gravityVtp.operation.set ( 3 ) ;
        gravityVtp.input1Y.set ( -1 ) ;
        gravityVtp.output >> nucleus.gravityDirection ;

        pm.parent ( nucleusCtrlZro , currentNucleusRigGrp ) ;

        ### Wind Ctrl ###

        product_list = self.createCtrl ( name = nucleus.nodeName() + 'Wind' , point = self.windCtrl_point ) ;
        windCtrl = product_list[0] ;
        self.setColor ( target = windCtrl , color = self.colorDict['white'] ) ;
        windCtrlZro = product_list[1] ;

        pm.parentConstraint ( nucleusCtrl , windCtrlZro , mo = False , skipTranslate = 'none' , skipRotate = [ 'x' , 'y' , 'z'] ) ;

        windVtp = pm.createNode ( 'vectorProduct' ) ;
        windVtp.rename ( nucleus.nodeName() + 'Wind_Vtp' ) ;

        windCtrl.worldMatrix >> windVtp.matrix ;

        windVtp.operation.set ( 3 ) ;
        windVtp.input1Y.set ( -1 ) ;
        windVtp.output >> nucleus.windDirection ;

        pm.parent ( windCtrlZro , currentNucleusRigGrp ) ;

        ### Parent Constraint Ctrl to the Nucleus ###

        pm.parentConstraint ( nucleusCtrl , nucleus , mo = False , skipTranslate = 'none' , skipRotate = [ 'x' , 'y' , 'z'] ) ;

        return currentNucleusRigGrp , nucleusCtrl , windCtrl ;

    def createNucleusRig ( self , *args ) :

        selection = pm.ls ( sl = True ) ;

        nucleusRigGrp = self.createNucleusRigGroup() ;

        if len ( selection ) == 1 :
            
            nucleus = selection[0] ;

            product_list = self.createNucleusIndividualRig ( nucleus = nucleus ) ;
            rigGroup    = product_list[0] ;
            nucluesCtrl = product_list[1] ;
            windCtrl    = product_list[2] ;

            pm.parent ( rigGroup , nucleusRigGrp ) ;
        
        else :

            ### create placement controller ###

            placementCtrl = pm.curve ( d = 1 , p = self.placementCtrl_point ) ;
            placementCtrl.rename ( 'NucleusPlacement_Ctrl' ) ;

            self.clean ( target = placementCtrl ) ;

            for attr in [ 's' ] :
                for axis in [ 'x' , 'y' , 'z' ] :
                    self.lockHideAttr ( target = placementCtrl , attr = attr + axis ) ;

            pm.parent ( placementCtrl , nucleusRigGrp ) ;

            ### now dealing with selected nucleus(s) ###

            # create position list
            pos_list = [] ;

            refPivotGrp = pm.group ( em = True ) ;
            refPosGrp   = pm.group ( em = True ) ;
            pm.parent ( refPosGrp , refPivotGrp ) ;
            
            pm.xform ( refPivotGrp , ws = True , t = ( 0 , 0 , 3 ) ) ;
            pm.xform ( refPivotGrp , ws = True , rp = ( 0 , 0 , 0 ) ) ;

            rotateValue = 360.00 / len(selection) ;

            for i in range ( 0 , len(selection) ) :
                refPivotGrp.ry.set ( rotateValue * i ) ;
                pos = pm.xform ( refPosGrp , q = True , ws = True , t = True ) ;
                pos_list.append ( pos ) ;

            pm.delete ( refPivotGrp , refPosGrp ) ;

            # create individual nucleus rig
            
            nucluesCtrl_list = [] ;

            for nucleus in selection :

                product_list = self.createNucleusIndividualRig ( nucleus = nucleus ) ;
                rigGroup    = product_list[0] ;
                nucluesCtrl = product_list[1] ;
                nucluesCtrl_list.append ( nucluesCtrl ) ;
                windCtrl    = product_list[2] ;

                pm.parent ( rigGroup , placementCtrl ) ;

            # set nucleusRigPosition and color

            colorList = [ 'green' , 'lightBlue' , 'red' , 'yellow' ] ;

            for i in range ( 0 , len(nucluesCtrl_list) ) :
                pm.xform ( nucluesCtrl_list[i] , ws = True , t = pos_list[i] ) ;
                color = colorList [ i % len(colorList) ] ;
                self.setColor ( target = nucluesCtrl_list[i] , color = self.colorDict[color] ) ;

    def deleteNucleusRig ( self , *args ) :

        selection = pm.ls ( sl = True ) ;

        # check if selected nucleus rig group exists
        for nucleus in selection :
            
            if pm.objExists ( nucleus.nodeName() + 'Rig_Grp' ) :

                rigGroup = pm.PyNode ( nucleus.nodeName() + 'Rig_Grp' ) ;

                connectionList = pm.listConnections ( selection , type = 'parentConstraint' ) ;
                connectionList = set ( connectionList ) ;
                parConList = list ( connectionList ) ;
                
                pm.delete ( parConList ) ;
                pm.delete ( rigGroup ) ;

        if pm.objExists ( 'NucleusPlacement_Ctrl' ) :
            
            placementCtrl = pm.PyNode ( 'NucleusPlacement_Ctrl' ) ;

            if not placementCtrl.listRelatives ( ad = True , type = 'transform' ) :
                pm.delete ( placementCtrl ) ;

        if pm.objExists ( 'NucleusRig_Grp' ) :

            rigGrp = pm.PyNode ( 'NucleusRig_Grp' ) ;

            if not rigGrp.listRelatives ( ad = True , type = 'transform' ) :
                pm.delete ( rigGrp ) ;

class Main ( General , NucleusRig ) :

    def __init__ ( self ) :
        super ( Main , self ).__init__() ;

def attach ( *args ) :
    main = Main() ;
    main.createNucleusRig() ;

def detach ( *args ) :
    main = Main() ;
    main.deleteNucleusRig() ;

# def testRun ( *args ) :
#     detach() ;

# testRun () ;