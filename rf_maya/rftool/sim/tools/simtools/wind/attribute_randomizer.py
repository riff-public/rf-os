import pymel.core as pm ;
import random ;
# wrote for maya 2017

class AttrRandomizer ( object ) :

    def __init__ ( self ) :

        self.Gui = self.Gui ( ) ;
        
    class Gui ( ) :

        def __init__ ( self ) :
            self.width      = 350.00 ;
            self.version    = 'v0' ;
            self.title      = 'Attribute Randomizer ' + self.version ;
            self.ui         = 'attrRandomizer_ui' ;

        def __str__ ( self ) :
            return str ( self.ui ) ;

        def __repr__ ( self ) :
            return str ( self.ui ) ;

        def checkDuplicateWindow ( self ) :

            if pm.window ( self.ui , exists = True ) :
                pm.deleteUI ( self.ui ) ;
            else : pass ;

        def show ( self ) :
            
            self.checkDuplicateWindow ( ) ;

            window = pm.window (
                self.ui ,
                title                   = self.title ,
                minimizeButton          = True ,
                maximizeButton          = False ,
                sizeable                = True ,
                resizeToFitChildren     = True ,
                ) ;
            pm.window ( self.ui , e = True , w = self.width , h = 10 ) ;
            with window :

                with pm.rowColumnLayout ( nc = 1 ) :

                    with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , self.width/4 ) , ( 2 , self.width/4 ) , ( 3 , self.width/4 ) , ( 4 , self.width/4 ) ] ) :

                        pm.text ( label = 'preRoll (-30)'   , bgc = ( 0.45 , 0.75 , 0.45 ) ) ;
                        pm.text ( label = 'start'           , bgc = ( 1 , 1 , 0 ) ) ;
                        pm.text ( label = 'end'             , bgc = ( 1 , 1 , 0 ) ) ;
                        pm.text ( label = 'postRoll (+10)'  , bgc = ( 0.45 , 0.75 , 0.45 ) ) ;

                        self.preRoll_intField   = pm.intField ( 'preRoll_intField'  , width = self.width/4 , enable = False ) ;
                        self.start_intField     = pm.intField ( 'start_intField'    , width = self.width/4 , cc = self.start_cc ) ;
                        self.end_intField       = pm.intField ( 'end_intField'      , width = self.width/4 , cc = self.end_cc ) ;
                        self.postRoll_intField  = pm.intField ( 'postRoll_intField' , width = self.width/4 , enable = False ) ;

                    pm.separator ( h = 10 ) ;
                    with pm.rowColumnLayout ( nc = 4 , cw = [ ( 1 , self.width/4 ) , ( 2 , self.width/4 ) , ( 3 , self.width/4 ) , ( 4 , self.width/4 ) ] ) :

                        pm.text ( label = '' ) ;
                        pm.text ( label = 'min value' ) ;
                        pm.text ( label = 'max value' ) ;
                        pm.text ( label = 'precision' ) ;

                        pm.text ( label = '' ) ;
                        self.minValue_floatField    = pm.floatField ( 'minValue_floatField' , width = self.width/4 , value = 1  , cc = self.minValue_cc ) ;
                        self.maxValue_floatField    = pm.floatField ( 'maxValue_floatField' , width = self.width/4 , value = 10 , cc = self.maxValue_cc ) ;
                        self.precision_intField     = pm.intField   ( 'precision_intField'  , width = self.width/4 , value = 2  , cc = self.precision_cc , minValue = 0 ) ;
                    
                    pm.separator ( h = 10 ) ;

                    self.evaluateLayout = pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , self.width/4 ) , ( 2 , self.width/2 ) , ( 3 , self.width/4 ) ] ) ;
                    with self.evaluateLayout :

                        pm.text ( label = 'evaluate every ' , align = 'right' ) ;
                    
                        with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , self.width/2*0.45 ) , ( 2 , self.width/2*0.1 ) , ( 3 , self.width/2*0.45 ) ] ) :
                            self.minEval_intField   = pm.intField ( 'minEval_intField' , value = 1 ) ;
                            pm.text ( label = '~' ) ;
                            self.maxEval_intField   = pm.intField ( 'maxEval_intField' , value = 3 ) ;
                            # write minRandValue_cc

                        pm.text ( label = ' frame' , align = 'left' ) ;

                    pm.separator ( h = 10 ) ;
                     
                    pm.button ( label = 'randomize attribute value'         , c = self.randomize_cmd ) ;
                    pm.button ( label = 'delete radomized attribute value'  , c = self.deleteRandom_cmd , bgc = ( 1 , 0 , 0 ) ) ;

            self.refresh ( ) ;
            window.show ( ) ;

        def start_cc ( self , *args ) :
            
            self.preRoll_intField.setValue ( self.start_intField.getValue ( ) - 30 ) ;

            if self.start_intField.getValue ( ) >= self.end_intField.getValue ( ) :
                self.end_intField.setValue ( self.start_intField.getValue ( ) + 1 ) ;
                self.end_cc ( ) ;

        def end_cc ( self , *args ) :

            self.postRoll_intField.setValue ( self.end_intField.getValue ( ) + 10 ) ;

            if self.start_intField.getValue ( ) >= self.end_intField.getValue ( ) :
                self.start_intField.setValue ( self.end_intField.getValue ( ) - 1 ) ;
                self.start_cc ( ) ;

        def minValue_cc ( self , *args ) :
            if self.minValue_floatField.getValue ( ) >= self.maxValue_floatField.getValue ( ) :
                self.maxValue_floatField.setValue ( self.minValue_floatField.getValue ( ) + 1 ) ;

        def maxValue_cc ( self , *args ) :
            if self.minValue_floatField.getValue ( ) >= self.maxValue_floatField.getValue ( ) :
                self.minValue_floatField.setValue ( self.maxValue_floatField.getValue ( ) - 1 ) ;

        def precision_cc ( self , *args ) :

            minValue = format ( self.minValue_floatField.getValue ( ) , '.{precision}f'.format ( precision = self.precision_intField.getValue ( ) ) ) ;
            minValue = float ( minValue ) ;

            maxValue = format ( self.maxValue_floatField.getValue ( ) , '.{precision}f'.format ( precision = self.precision_intField.getValue ( ) ) ) ;
            maxValue = float ( maxValue ) ;

            self.minValue_floatField.setPrecision ( self.precision_intField.getValue ( ) ) ;
            self.minValue_floatField.setValue ( minValue ) ;
            self.maxValue_floatField.setPrecision ( self.precision_intField.getValue ( ) ) ;
            self.maxValue_floatField.setValue ( maxValue ) ;

        def timeRange ( self , *args ) :
            self.start      = pm.playbackOptions ( q = True , min = True ) ;
            self.end        = pm.playbackOptions ( q = True , max = True ) ;
            self.preRoll    = self.start - 30 ;
            self.postRoll   = self.end + 10 ;

        def refresh ( self , *args ) :
            self.timeRange ( ) ;
            self.precision_cc ( ) ;
            self.preRoll_intField.setValue ( self.preRoll ) ;
            self.start_intField.setValue ( self.start ) ;  
            self.end_intField.setValue ( self.end ) ;    
            self.postRoll_intField.setValue ( self.postRoll ) ;

        def getSelectedObj ( self , *args ) :
            selectedObj = pm.ls ( sl = True ) ;
            if not selectedObj :
                self.selectedObj = None ;
            else :
                self.selectedObj = selectedObj ;

            #print self.selectedObj ;

        def getSelectedAttr ( self , *args ) :
            # get selected attr ONLY from [main channel box]

            selectedAttr = [] ;
            
            selectedMainAttr_cmd    = 'channelBox -q -sma mainChannelBox ;' ;
            # selectedShapeAttr_cmd   = 'channelBox -q -ssa mainChannelBox ;' ;
            
            selectedMainAttr = pm.mel.eval ( selectedMainAttr_cmd ) ;
            if not selectedMainAttr : selectedMainAttr = [] ;

            # selectedShapeAttr = pm.mel.eval ( selectedShapeAttr_cmd ) ;
            # if not selectedShapeAttr : selectedShapeAttr = [] ;

            selectedAttr.extend ( selectedMainAttr ) ;
            # selectedAttr.extend ( selectedShapeAttr ) ;

            if selectedAttr == [] :
                self.selectedAttr = None ;
            else :
                self.selectedAttr = selectedAttr ;

            #print self.selectedAttr ;

        def attr ( self , node , attr , attrType = 'float' , *args ) :
            # return outputAttr  
            if not ( pm.attributeQuery ( attr , node = node , exists = True ) ) :
                outputAttr = node.addAttr ( attr , keyable = True , attributeType = attrType ) ;
            else :
                exec ( 'outputAttr = node.%s' %  attr  ) ;

            return outputAttr ;

        def randomizeTimeRange ( self , *args ) :

            randomizedTimeRange = [] ;
            randomizedTimeRange.append ( self.preRoll_intField.getValue ( ) ) ;

            currentTimeRange = self.preRoll_intField.getValue ( ) ;

            while currentTimeRange < self.postRoll_intField.getValue ( ) :
                currentTimeRange += random.randint ( self.minEval_intField.getValue ( ) , self.maxEval_intField.getValue ( ) ) ;
                randomizedTimeRange.append ( currentTimeRange ) ;

            self.randomizedTimeRange = randomizedTimeRange ;

        def randomize_cmd ( self , *args ) :
            
            self.getSelectedObj ( ) ;
            self.getSelectedAttr ( ) ;

            if ( self.selectedObj != None ) and ( self.selectedAttr != None ) :

                ### objects ###
                for obj in self.selectedObj :
                    
                    objShape = obj.getShape ( ) ;
                    
                    self.attr ( objShape , '__random__' , 'float' ) ;
                    objShape.__random__.lock ( ) ;

                    for attr in self.selectedAttr :

                        ### first time stat ###
                        firstTimeStat = False ;
                        if not ( pm.attributeQuery ( attr + '_ori' , node = objShape , exists = True ) ) :
                            firstTimeStat = True ;

                        attrType = pm.attributeQuery ( attr , node = obj , attributeType = True ) ;

                        for each in [ '_ori' , '_rand' , '_amp' ] :
                            self.attr ( objShape , attr + each , attrType ) ;

                        ### first time initialization ###
                        if firstTimeStat == True :

                            keyExists = pm.copyKey ( obj , attribute = attr ) ;

                            if not keyExists :
                                exec ( 'oriValue = obj.%s.get ( ) ;' % attr ) ;
                                exec ( 'objShape.%s_ori.set ( oriValue ) ;' % attr ) ;
                            else :
                                pm.pasteKey ( objShape , attribute = attr + '_ori' , option = 'insert' , copies = True , connect = True , timeOffset = False , floatOffset = False , valueOffset = False ) ;

                            result  = pm.createNode ( 'plusMinusAverage'    , n = obj + attr[0].upper() + attr[1:] + 'Result_pma' ) ;
                            amp     = pm.createNode ( 'multiplyDivide'      , n = obj + attr[0].upper() + attr[1:] + 'Amp_mdv' ) ;

                            amp.operation.set ( 1 ) ;       # set operation to multiply
                            result.operation.set ( 1 ) ;    # set operation to sum
                            
                            exec ( 'objShape.%s_rand >> amp.i1x' % attr ) ;
                            
                            exec ( 'objShape.%s_amp.set(1)'     % attr ) ;
                            exec ( 'objShape.%s_amp >> amp.i2x' % attr ) ;

                            if attrType != 'bool' :
                                exec ( 'objShape.%s_ori >> result.input1D[0]' % attr ) ;   
                            amp.ox >> result.input1D[1] ;
                            
                            exec ( 'result.output1D >> obj.%s ;' % attr ) ;

                    ### set ###
                        for each in [ '_ori' , '_rand' , '_amp' ] :
                            exec ( 'objShape.%s%s.unlock ( ) ;' % ( attr , each ) ) ;

                        self.randomizeTimeRange ( ) ;
                        
                        pm.cutKey ( objShape + '.' + attr + '_rand' , cl = 1 ) ;

                        for frame in self.randomizedTimeRange :

                            if attrType == 'bool' :
                                pm.setKeyframe (
                                    objShape ,
                                    at  = attr + '_rand' ,
                                    v   = random.randint ( 0 , 1 ) ,
                                    t   = frame ,
                                    itt = 'auto' ,
                                    ott = 'auto' ,
                                    ) ;

                            else :
                                pm.setKeyframe (
                                    objShape ,
                                    at  = attr + '_rand' ,
                                    v   = float ( format ( random.uniform ( self.minValue_floatField.getValue ( ) , self.maxValue_floatField.getValue ( ) ) , '.{precision}f'.format ( precision = self.precision_intField.getValue ( ) ) ) ) ,
                                    t   = frame ,
                                    itt = 'spline' ,
                                    ott = 'spline' ,
                                    ) ;
                            
                            pm.setInfinity ( objShape + '.' + attr + '_rand' , preInfinite = 'cycle' , postInfinite = 'cycle' ) ;
                            
                    ### lock ###
                        for each in [ '_ori' , '_rand' ] :
                            exec ( 'objShape.%s%s.lock ( ) ;' % ( attr , each ) ) ;
                        if attrType == 'bool' :
                            exec ( 'objShape.%s_amp.lock ( ) ;' % ( attr ) ) ;

                pm.select ( self.selectedObj ) ;

        def deleteRandom_cmd ( self , *args ) :

            self.getSelectedObj ( ) ;
            self.getSelectedAttr ( ) ;

            for obj in self.selectedObj :

                    objShape = obj.getShape ( ) ;

                    for attr in self.selectedAttr :

                        if not ( pm.attributeQuery ( attr + '_ori' , node = objShape , exists = True ) ) :
                            pass ;
                        elif not ( pm.attributeQuery ( attr + '_rand' , node = objShape , exists = True ) ) :
                            pass ;
                        elif not ( pm.attributeQuery ( attr + '_amp' , node = objShape , exists = True ) ) :
                            pass ;

                        else :

                            ### unlock ###
                            for each in [ '_ori' , '_rand' , '_amp' ] :
                                exec ( 'objShape.%s%s.unlock ( ) ;' % ( attr , each ) ) ;

                            ### copy ori attribute to attribute ###
                            keyExists = pm.copyKey ( objShape , attribute = attr + '_ori' ) ;

                            if not keyExists :
                                exec ( 'oriValue = objShape.%s_ori.get ( ) ;' % attr ) ;
                                pm.mel.eval ( 'source channelBoxCommand; CBdeleteConnection \"%s\"' % ( obj + '.' + attr ) ) ;
                                exec ( 'obj.%s.set ( oriValue ) ;' % attr ) ;
                            else :
                                pm.mel.eval ( 'source channelBoxCommand; CBdeleteConnection \"%s\"' % ( obj + '.' + attr ) ) ;
                                pm.pasteKey ( obj , attribute = attr , option = 'insert' , copies = True , connect = True , timeOffset = False , floatOffset = False , valueOffset = False ) ;

                            ### delete elems ###
                            for each in [ '_ori' , '_rand' , '_amp' ] :
                                pm.deleteAttr ( objShape , attribute = attr + each ) ;

                            if not any ( '_ori' in each for each in pm.listAttr ( objShape , userDefined = True ) ) :
                                objShape.__random__.unlock ( ) ;
                                pm.deleteAttr ( objShape , attribute = '__random__' ) ;

                            result  = obj + attr[0].upper() + attr[1:] + 'Result_pma' ;
                            amp     = obj + attr[0].upper() + attr[1:] + 'Amp_mdv' 

                            for each in [ result , amp ] :
                                if pm.objExists ( each ) == True :
                                    pm.delete ( each ) ;

def run ( *args ) :
    attrRandomizer = AttrRandomizer ( ) ;
    attrRandomizer.Gui.show ( ) ;

# run ( ) ;
# anim curve = inifinity