import pymel.core as pm ;

def updateHairSystemList ( *args ) :

    hairSystem = pm.ls ( type = 'hairSystem' ) ;
    hairSystem.sort ( ) ;

    pm.textScrollList ( 'hairSystemList' , e = True , ra = True ) ;

    for each in hairSystem :
        pm.textScrollList ( 'hairSystemList' , e = True , append = each ) ;

    pm.textScrollList ( 'hairSystemList' , e = True , selectItem = hairSystem ) ;

    return hairSystem ; 

def queryHairSystemAttribute ( hairSystem = '' , *args ) :

    attributeValues = [] ;

    #dynamic properties
    stretchResistance = pm.getAttr ( hairSystem + '.' + 'stretchResistance' ) ;
    compressionResistance = pm.getAttr ( hairSystem + '.' + 'compressionResistance' ) ;
    bendResistance = pm.getAttr ( hairSystem + '.' + 'bendResistance' ) ;
    twistResistance = pm.getAttr ( hairSystem + '.' + 'twistResistance' ) ;
    extraBendLinks = pm.getAttr ( hairSystem + '.' + 'extraBendLinks' ) ;
    restLengthScale = pm.getAttr ( hairSystem + '.' + 'restLengthScale' ) ;
    noStretch = pm.getAttr ( hairSystem + '.' + 'noStretch' ) ;
    attributeValues.extend ( [ stretchResistance , compressionResistance , bendResistance , twistResistance , extraBendLinks , restLengthScale , noStretch ] ) ;
    
    # start curve attract
    startCurveAttract = pm.getAttr ( hairSystem + '.' + 'startCurveAttract' ) ;
    attractionDamp = pm.getAttr ( hairSystem + '.' + 'attractionDamp' ) ;
    attributeValues.extend ( [ startCurveAttract , attractionDamp ] ) ;

    # attraction scale

    # force
    mass = pm.getAttr ( hairSystem + '.' + 'mass' ) ;
    drag = pm.getAttr ( hairSystem + '.' + 'drag' ) ;
    tangentialDrag = pm.getAttr ( hairSystem + '.' + 'tangentialDrag' ) ;
    motionDrag = pm.getAttr ( hairSystem + '.' + 'motionDrag' ) ;
    damp = pm.getAttr ( hairSystem + '.' + 'damp' ) ;
    stretchDamp = pm.getAttr ( hairSystem + '.' + 'stretchDamp' ) ;
    dynamicsWeight = pm.getAttr ( hairSystem + '.' + 'dynamicsWeight' ) ;
    ignoreSolverGravity = pm.getAttr ( hairSystem + '.' + 'ignoreSolverGravity' ) ;
    ignoreSolverWind = pm.getAttr ( hairSystem + '.' + 'ignoreSolverWind' ) ;
    disableFollicleAnim = pm.getAttr ( hairSystem + '.' + 'disableFollicleAnim' ) ;
    attributeValues.extend ( [ mass , drag , tangentialDrag , motionDrag , damp , stretchDamp , dynamicsWeight , ignoreSolverGravity , ignoreSolverWind , disableFollicleAnim ] ) ;

    # turbulence
    turbulenceStrength = pm.getAttr ( hairSystem + '.' + 'turbulenceStrength' ) ;
    turbulenceFrequency = pm.getAttr ( hairSystem + '.' + 'turbulenceFrequency' ) ;
    turbulenceSpeed = pm.getAttr ( hairSystem + '.' + 'turbulenceSpeed' ) ;
    attributeValues.extend ( [ turbulenceStrength , turbulenceFrequency , turbulenceSpeed ] ) ;

    return ( attributeValues ) ;

def setHairSystemUIAttribute ( hairSystem = '' , *args ) :

    attributeValues = queryHairSystemAttribute ( hairSystem = hairSystem ) ;

    #dynamic properties
    pm.floatField ( 'stretchResistance_val' , e = True , v = attributeValues [0] ) ;
    pm.floatField ( 'compressionResistance_val' , e = True , v = attributeValues [1] ) ;
    pm.floatField ( 'bendResistance_val' , e = True , v = attributeValues [2] ) ;
    pm.floatField ( 'twistResistance_val' , e = True , v = attributeValues [3] ) ;
    pm.floatField ( 'extraBendLinks_val' , e = True , v = attributeValues [4] ) ;
    pm.floatField ( 'restLengthScale_val' , e = True , v = attributeValues [5] ) ;
    pm.checkBox ( 'noStretch_cbx' , e = True , v = attributeValues [6] ) ;
   
    # start curve attract
    pm.floatField ( 'startCurveAttract_val' , e = True , v = attributeValues [7] ) ;
    pm.floatField ( 'attractionDamp_val' , e = True , v = attributeValues [8] ) ;

    # force
    pm.floatField ( 'mass_val' , e = True , v = attributeValues [9] ) ;
    pm.floatField ( 'drag_val' , e = True , v = attributeValues [10] ) ;
    pm.floatField ( 'tangentialDrag_val' , e = True , v = attributeValues [11] ) ;
    pm.floatField ( 'motionDrag_val' , e = True , v = attributeValues [12] ) ;
    pm.floatField ( 'damp_val' , e = True , v = attributeValues [13] ) ;
    pm.floatField ( 'stretchDamp_val' , e = True , v = attributeValues [14] ) ;
    pm.floatField ( 'dynamicsWeight_val' , e = True , v = attributeValues [15] ) ;
    pm.checkBox ( 'ignoreSolverGravity_cbx' , e = True , v = attributeValues [16] ) ;
    pm.checkBox ( 'ignoreSolverWind_cbx' , e = True , v = attributeValues [17] ) ;
    pm.checkBox ( 'disableFollicleAnim_cbx' , e = True , v = attributeValues [18] ) ;

    # turbulence
    pm.floatField ( 'turbulenceStrength_val' , e = True , v = attributeValues [19] ) ;
    pm.floatField ( 'turbulenceFrequency_val' , e = True , v = attributeValues [20] ) ;
    pm.floatField ( 'turbulenceSpeed_val' , e = True , v = attributeValues [21] ) ;

def querySelectedHairSystem ( *args ) :

    hairSystems = pm.textScrollList ( 'hairSystemList' , q = True , selectItem = True  ) ;
    #hairSystems.sort ( ) ;
    return ( hairSystems ) ;

def setHairSystemAttribute ( attribute = '' , *args ) :

    val = pm.floatField ( '%s_val' % attribute , q = True , v = True ) ;
    valOffset = pm.floatField ( '%s_offsetVal' % attribute , q = True , value = True ) ;
    operator = pm.optionMenu ( '%s_opm' % attribute , q = True , v = True ) ;

    hairSystems = querySelectedHairSystem ( ) ;

    for i in range ( 0 , len ( hairSystems ) ) :

        offset = valOffset * i ;

        if str ( operator ) == '+' :
            value = val + offset ;
        elif str ( operator ) == '-' :
            value = val - offset ;
        else :
            pm.error ( 'operator error, please check script' ) ; 

        pm.setAttr ( hairSystems [i] + '.' + attribute , value ) ;

def setHairSystemCbx ( attribute = '' , *args ) :

    val = pm.checkBox ( '%s_cbx' % attribute , q = True ,  value = True ) ;

    hairSystems = querySelectedHairSystem ( ) ;

    for each in hairSystems :

        pm.setAttr ( each + '.' + attribute ,  val ) ;

############################################################
############################################################
############################################################

def queryAttractionScaleGraph ( hairSystem = '' , *args ) :
        
    attrSize = pm.getAttr ( '%s.attractionScale' % hairSystem , size = True ) ;
    
    attrDict =  {} ;
    
    counter = 0 ;
    
    for i in range ( 0 , attrSize ) :
        
        point = 'point' + str ( i + 1 ) ;
        
        floatValue = pm.getAttr ( hairSystem + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_FloatValue' ) ;
        position = pm.getAttr ( hairSystem + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_Position' ) ;
        attractionScale = pm.getAttr ( hairSystem + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_Interp' ) ;
        
        while ( floatValue == 0.0 ) and ( position == 0.0 ) and ( attractionScale == 0 ) :
            
            pm.removeMultiInstance ( '%s.attractionScale[%s]' % ( hairSystem , ( i + counter ) ) , b = True ) ;
            
            counter += 1 ;
            
            floatValue = pm.getAttr ( hairSystem + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_FloatValue' ) ;
            position = pm.getAttr ( hairSystem + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_Position' ) ;
            attractionScale = pm.getAttr ( hairSystem + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_Interp' ) ;
            
            # kill inifinite loop        
            if counter == 100 :
                break ;
            
        attrDict [ point ] = [ floatValue , position , attractionScale ] ;
        
    return [ attrDict , attrSize ] ;

def setAttractionScaleGraph ( hairSystemDriver = '' , hairSystemDrivenList = [] , *args ) :

    hairSystemDriverList = queryAttractionScaleGraph ( hairSystem = hairSystemDriver ) ; 
    hairSystemDriverAttrDict = hairSystemDriverList [0] ;
    hairSystemDriverAttrSize = hairSystemDriverList [1] ;
    
    for hairSystemDriven in hairSystemDrivenList :
        
        counter = 0 ;

        attrSize = pm.getAttr ( '%s.attractionScale' % hairSystemDriven , size = True ) ;
        
        floatValue = pm.getAttr ( hairSystemDriven + '.' + 'attractionScale[0].attractionScale_FloatValue' ) ;
        position = pm.getAttr ( hairSystemDriven + '.' + 'attractionScale[0].attractionScale_Position' ) ;
        attractionScale = pm.getAttr ( hairSystemDriven + '.' + 'attractionScale[0].attractionScale_Interp' ) ;

        if ( floatValue == 0.0 ) and ( position == 0.0 ) and ( attractionScale == 0.0 ) :
            attrSize += 1 ;

        else :
            pass ;

        for i in range ( 1 , attrSize ) : 

            floatValue = pm.getAttr ( hairSystemDriven + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_FloatValue' ) ;
            position = pm.getAttr ( hairSystemDriven + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_Position' ) ;
            attractionScale = pm.getAttr ( hairSystemDriven + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_Interp' ) ;
            
            while ( floatValue == 0.0 ) and ( position == 0.0 ) and ( attractionScale == 0 ) :
                
                pm.removeMultiInstance ( '%s.attractionScale[%s]' % ( hairSystemDriven , ( i + counter ) ) , b = True ) ;
                
                counter += 1 ;
                
                floatValue = pm.getAttr ( hairSystemDriven + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_FloatValue' ) ;
                position = pm.getAttr ( hairSystemDriven + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_Position' ) ;
                attractionScale = pm.getAttr ( hairSystemDriven + '.' + 'attractionScale[%s]' % ( i + counter ) + '.' + 'attractionScale_Interp' ) ;
                
                # kill inifinite loop        
                if counter == 100 :
                    break ;

            pm.removeMultiInstance ( '%s.attractionScale[%s]' % ( hairSystemDriven , ( i + counter ) ) , b = True ) ;
    
        for i in range ( 0 , hairSystemDriverAttrSize ) :
            pm.setAttr ( hairSystemDriven + '.' + 'attractionScale[%s]' % i + '.' + 'attractionScale_FloatValue' , hairSystemDriverAttrDict [ 'point%s' % str ( i+1 ) ] [0] ) ;
            pm.setAttr ( hairSystemDriven + '.' + 'attractionScale[%s]' % i + '.' + 'attractionScale_Position' , hairSystemDriverAttrDict [ 'point%s' % str ( i+1 ) ] [1] ) ;
            pm.setAttr ( hairSystemDriven + '.' + 'attractionScale[%s]' % i + '.' + 'attractionScale_Interp' , hairSystemDriverAttrDict [ 'point%s' % str ( i+1 ) ] [2] ) ;

def copyGraphBtn ( *args ) :

    hairSystems = querySelectedHairSystem ( ) ;
    hairSystems.sort ( ) ;

    hairSystemDriver = hairSystems [0] ;
    hairSystemDrivenList = hairSystems [1:]

    setAttractionScaleGraph ( hairSystemDriver = hairSystemDriver , hairSystemDrivenList = hairSystemDrivenList ) ;

############################################################
############################################################
############################################################

### dynamic properties ###

def stretchResistance_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'stretchResistance' ) ;

def compressionResistance_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'compressionResistance' ) ;

def bendResistance_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'bendResistance' ) ;
                        
def twistResistance_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'twistResistance' ) ;
                        
def extraBendLinks_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'extraBendLinks' ) ;
                        
def restLengthScale_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'restLengthScale' ) ;
                        
def noStretch_cmd ( *args ) :
    setHairSystemCbx ( attribute = 'noStretch' ) ;   

### start curve attract ###

def startCurveAttract_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'startCurveAttract' ) ;

def attractionDamp_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'attractionDamp' ) ;

### force ###

def mass_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'mass' ) ;

def drag_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'drag' ) ;

def tangentialDrag_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'tangentialDrag' ) ;

def motionDrag_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'motionDrag' ) ;

def damp_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'damp' ) ;

def stretchDamp_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'stretchDamp' ) ;

def dynamicsWeight_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'dynamicsWeight' ) ;

def ignoreSolverGravity_cmd ( *args ) :
    setHairSystemCbx ( attribute = 'ignoreSolverGravity' ) ;   

def ignoreSolverWind_cmd ( *args ) :
    setHairSystemCbx ( attribute = 'ignoreSolverWind' ) ;

def disableFollicleAnim_cmd ( *args ) :
    setHairSystemCbx ( attribute = 'disableFollicleAnim' ) ;

### turbulence ###

def turbulenceStrength_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'turbulenceStrength' ) ;

def turbulenceFrequency_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'turbulenceFrequency' ) ;

def turbulenceSpeed_cmd ( *args ) :
    setHairSystemAttribute ( attribute = 'turbulenceSpeed' ) ;

##########
##########
##########

def collapseCommand ( *args ) :

    pm.window ( 'hairSettingUtilitiesUI' , e = True , h = 10 ) ;

##########
##########
##########

def hairSystemList_sc ( *args ) :

    hairSystems = querySelectedHairSystem ( ) ;

    setHairSystemUIAttribute ( hairSystem = hairSystems [0] ) ;

##########
##########
##########

def enableDisableNHair ( mode , *args ) :
    # 0 = disable
    # 3 = enable


    selection = querySelectedHairSystem ( ) ;

    for each in selection :
        hairsystem = pm.general.PyNode ( each ) ;
        hairsystem.simulationMethod.set ( mode ) ;

def enableNHair ( *args ) :
    enableDisableNHair ( mode = 3 ) ;

def disableNHair ( *args ) :
    enableDisableNHair ( mode = 0 ) ;

##########
##########
##########

def refresh ( * args ) :
    hairSystem = updateHairSystemList ( ) ;
    setHairSystemUIAttribute ( hairSystem = hairSystem [0] ) ;

def run ( *args ) :
    
    width = 300 ;
    
    # check if window exists
    if pm.window ( 'hairSettingUtilitiesUI' , exists = True ) :
        pm.deleteUI ( 'hairSettingUtilitiesUI' ) ;
    else : pass ;
   
    window = pm.window ( 'hairSettingUtilitiesUI', title = "hair setting utilities" ,
        mnb = False , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'hairSettingUtilitiesUI' , e = True , w = width , h = 10 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
            
            hairSystemLayout = pm.rowColumnLayout ( nc = 1 ) ;
            with hairSystemLayout :
                
                pm.textScrollList ( 'hairSystemList' , w = width , h = 75 , ams = True , sc = hairSystemList_sc ) ;
                pm.button ( label = 'refresh' , c = refresh , bgc = ( 0.282353 , 0.819608 , 0.8 ) ) ;
                
                hairSystemEnableLayout = pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width / 2.0 ) , ( 2 , width / 2.0 ) ] ) ;
                with hairSystemEnableLayout :
                    
                    pm.button ( label = 'enable nHair' , c = enableNHair , bgc = ( 0.498039 , 1 , 0 ) ) ;
                    pm.button ( label = 'disable nHair' , c = disableNHair , bgc = ( 0.196078 , 0.803922 , 0.196078 ) ) ;
                
            pm.separator ( vis = False , h = 5 ) ;
            
            dynamicPropertiesFrameLayout = pm.frameLayout ( 'dynamicPropertiesFrameLayout' ,
                label = 'DYNAMIC PROPERTIES' , bgc = ( 0 , 0 , 0.5 ) ,
                collapsable = True , cc = collapseCommand ) ;

            with dynamicPropertiesFrameLayout :
            
                dynamicPropertiesMainLayout = pm.rowColumnLayout ( nc = 1 , w = width ) ;
                with dynamicPropertiesMainLayout :
            
                    dynamicPropertiesAttLayout = pm.rowColumnLayout ( nc = 2 , cw = [
                        ( 1 , ( width / 10.0 ) * 6.25 ) ,
                        ( 2 , ( width / 10.0 ) * 3.75 ) ] ) ;
                    with dynamicPropertiesAttLayout :    
                                        
                        pm.text ( 'ATTRIBUTE' , bgc = (  0.1 , 0.5 , 0.1 ) , fn = 'boldLabelFont' ) ;
                        pm.text ( 'OFFSET' , bgc = ( 0.5 , 1 , 0 ) , fn = 'boldLabelFont' ) ;
            
                    dynamicPropertiesLayout = pm.rowColumnLayout ( nc = 4 , cw = [
                        ( 1 , ( width / 10.0 ) * 4 ) ,
                        ( 2 , ( width / 10.0 ) * 2.25 ) ,
                        ( 3 , ( width / 10.0 ) * 1.5 ) ,
                        ( 4 , ( width / 10.0 ) * 2.25 ) ] ) ;
                    with dynamicPropertiesLayout :
                        
                        pm.text ( label = 'stretchResistance' ) ;
                        pm.floatField ( 'stretchResistance_val' , pre = 3 , cc = stretchResistance_cmd ) ;
                        with pm.optionMenu ( 'stretchResistance_opm' , cc = stretchResistance_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'stretchResistance_offsetVal' , value = 0.1 , pre = 3 , cc = stretchResistance_cmd ) ;
                        
                        pm.text ( label = 'compressionResistance' ) ;
                        pm.floatField ( 'compressionResistance_val' , pre = 3 , cc = compressionResistance_cmd ) ;
                        with pm.optionMenu ( 'compressionResistance_opm' , cc = compressionResistance_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'compressionResistance_offsetVal' , value = 0.1 , pre = 3 , cc = compressionResistance_cmd ) ;
                        
                        pm.text ( label = 'bendResistance' ) ;
                        pm.floatField ( 'bendResistance_val' , pre = 3 , cc = bendResistance_cmd ) ;
                        with pm.optionMenu ( 'bendResistance_opm' , cc = bendResistance_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'bendResistance_offsetVal' , value = 0.1 , pre = 3 , cc = bendResistance_cmd ) ;
                        
                        pm.text ( label = 'twistResistance' ) ;
                        pm.floatField ( 'twistResistance_val' , pre = 3 , cc = twistResistance_cmd ) ;
                        with pm.optionMenu ( 'twistResistance_opm' , cc = twistResistance_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'twistResistance_offsetVal' , value = 0.1 , pre = 3 , cc = twistResistance_cmd ) ;
                        
                        pm.text ( label = 'extraBendLinks' ) ;
                        pm.floatField ( 'extraBendLinks_val' , pre = 3 , cc = extraBendLinks_cmd ) ;
                        with pm.optionMenu ( 'extraBendLinks_opm' , cc = extraBendLinks_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'extraBendLinks_offsetVal' , value = 0.1 , pre = 3 , cc = extraBendLinks_cmd ) ;
                        
                        pm.text ( label = 'restLengthScale' ) ;
                        pm.floatField ( 'restLengthScale_val' , pre = 3 , cc = restLengthScale_cmd ) ;
                        with pm.optionMenu ( 'restLengthScale_opm' , cc = restLengthScale_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'restLengthScale_offsetVal' , value = 0.1 , pre = 3 , cc = restLengthScale_cmd ) ;
                    
                    dynamicPropertiesCbxLayout = pm.rowColumnLayout ( nc = 2 , cw = [
                        ( 1 , ( width / 10.0 ) * 4 ) ,
                        ( 2 , ( width / 10.0 ) * 6 ) ] ) ;
                    with dynamicPropertiesCbxLayout :    
                        pm.text ( label = ' ' ) ;
                        pm.checkBox ( 'noStretch_cbx' , label = 'noStretch' , cc = noStretch_cmd ) ;

            startCurveAttractFrameLayout = pm.frameLayout ( label = 'START CURVE ATTRACT' , bgc = ( 0 , 0 , 0.5 ) ,
                collapsable = True , cc = collapseCommand ) ;
            with startCurveAttractFrameLayout :
            
                startCurveAttractMainLayout = pm.rowColumnLayout ( nc = 1 , w = width ) ;
                with startCurveAttractMainLayout :
            
                    startCurveAttractAttLayout = pm.rowColumnLayout ( nc = 2 , cw = [
                        ( 1 , ( width / 10.0 ) * 6.25 ) ,
                        ( 2 , ( width / 10.0 ) * 3.75 ) ] ) ;
                    with startCurveAttractAttLayout :    
                                        
                        pm.text ( 'ATTRIBUTE' , bgc = (  0.1 , 0.5 , 0.1 ) , fn = 'boldLabelFont' ) ;
                        pm.text ( 'OFFSET' , bgc = ( 0.5 , 1 , 0 ) , fn = 'boldLabelFont' ) ;
            
                    startCurveAttractLayout = pm.rowColumnLayout ( nc = 4 , cw = [
                        ( 1 , ( width / 10.0 ) * 4 ) ,
                        ( 2 , ( width / 10.0 ) * 2.25 ) ,
                        ( 3 , ( width / 10.0 ) * 1.5 ) ,
                        ( 4 , ( width / 10.0 ) * 2.25 ) ] ) ;
                    with startCurveAttractLayout :
                        
                        pm.text ( label = 'startCurveAttract' ) ;
                        pm.floatField ( 'startCurveAttract_val' , pre = 3 , cc = startCurveAttract_cmd ) ;
                        with pm.optionMenu ( 'startCurveAttract_opm' , cc = startCurveAttract_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'startCurveAttract_offsetVal' , value = 0.025 , pre = 3 , cc = startCurveAttract_cmd ) ;
                        
                        pm.text ( label = 'attractionDamp' ) ;
                        pm.floatField ( 'attractionDamp_val' , pre = 3 , cc = attractionDamp_cmd ) ;
                        with pm.optionMenu ( 'attractionDamp_opm' , cc = attractionDamp_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'attractionDamp_offsetVal' , value = 0.025 , pre = 3 , cc = attractionDamp_cmd ) ;
            
                attractionScaleFrameLayout = pm.frameLayout ( label = 'ATTRACTION SCALE' , bgc = (  0.2 , 0.3 , 0.3 ) ) ;
                with attractionScaleFrameLayout :

                    pm.button ( label = 'copy attraction scale graph' , c = copyGraphBtn , bgc = ( 1 , 0.85 , 0 ) ) ;

            forceFrameLayout = pm.frameLayout ( label = 'FORCE' , bgc = ( 0 , 0 , 0.5 ) ,
                collapsable = True , cc = collapseCommand ) ;
            with forceFrameLayout :

                forceMainLayout = pm.rowColumnLayout ( nc = 1 , w = width ) ;
                with forceMainLayout :
            
                    forceAttLayout = pm.rowColumnLayout ( nc = 2 , cw = [
                        ( 1 , ( width / 10.0 ) * 6.25 ) ,
                        ( 2 , ( width / 10.0 ) * 3.75 ) ] ) ;
                    with forceAttLayout :    
                                        
                        pm.text ( 'ATTRIBUTE' , bgc = (  0.1 , 0.5 , 0.1 ) , fn = 'boldLabelFont' ) ;
                        pm.text ( 'OFFSET' , bgc = ( 0.5 , 1 , 0 ) , fn = 'boldLabelFont' ) ;
            
                    forceLayout = pm.rowColumnLayout ( nc = 4 , cw = [
                        ( 1 , ( width / 10.0 ) * 4 ) ,
                        ( 2 , ( width / 10.0 ) * 2.25 ) ,
                        ( 3 , ( width / 10.0 ) * 1.5 ) ,
                        ( 4 , ( width / 10.0 ) * 2.25 ) ] ) ;
                    with forceLayout :

                        pm.text ( label = 'mass' ) ;
                        pm.floatField ( 'mass_val' , pre = 3 , cc = mass_cmd ) ;
                        with pm.optionMenu ( 'mass_opm' , cc = mass_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'mass_offsetVal' , value = 0.1 , pre = 3 , cc = mass_cmd ) ;

                        pm.text ( label = 'drag' ) ;
                        pm.floatField ( 'drag_val' , pre = 3 , cc = drag_cmd ) ;
                        with pm.optionMenu ( 'drag_opm' , cc = drag_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'drag_offsetVal' , value = 0.1 , pre = 3 , cc = drag_cmd ) ;

                        pm.text ( label = 'tangentialDrag' ) ;
                        pm.floatField ( 'tangentialDrag_val' , pre = 3 , cc = tangentialDrag_cmd ) ;
                        with pm.optionMenu ( 'tangentialDrag_opm' , cc = tangentialDrag_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'tangentialDrag_offsetVal' , value = 0.1 , pre = 3 , cc = tangentialDrag_cmd ) ;

                        pm.text ( label = 'motionDrag' ) ;
                        pm.floatField ( 'motionDrag_val' , pre = 3 , cc = motionDrag_cmd ) ;
                        with pm.optionMenu ( 'motionDrag_opm' , cc = motionDrag_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'motionDrag_offsetVal' , value = 0.1 , pre = 3 , cc = motionDrag_cmd ) ;

                        pm.text ( label = 'damp' ) ;
                        pm.floatField ( 'damp_val' , pre = 3 , cc = damp_cmd ) ;
                        with pm.optionMenu ( 'damp_opm' , cc = damp_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'damp_offsetVal' , value = 0.1 , pre = 3 , cc = damp_cmd ) ;

                        pm.text ( label = 'stretchDamp' ) ;
                        pm.floatField ( 'stretchDamp_val' , pre = 3 , cc = stretchDamp_cmd ) ;
                        with pm.optionMenu ( 'stretchDamp_opm' , cc = stretchDamp_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'stretchDamp_offsetVal' , value = 0.1 , pre = 3 , cc = stretchDamp_cmd ) ;

                        pm.text ( label = 'dynamicsWeight' ) ;
                        pm.floatField ( 'dynamicsWeight_val' , pre = 3 , cc = dynamicsWeight_cmd ) ;
                        with pm.optionMenu ( 'dynamicsWeight_opm' , cc = dynamicsWeight_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'dynamicsWeight_offsetVal' , value = 0.1 , pre = 3 , cc = dynamicsWeight_cmd ) ;

                    forceCbxLayout = pm.rowColumnLayout ( nc = 2 , cw = [
                        ( 1 , ( width / 10.0 ) * 4 ) ,
                        ( 2 , ( width / 10.0 ) * 6 ) ] ) ;
                    with forceCbxLayout :    

                        pm.text ( label = ' ' ) ;
                        pm.checkBox ( 'ignoreSolverGravity_cbx' , label = 'ignoreSolverGravity' , cc = ignoreSolverGravity_cmd ) ;

                        pm.text ( label = ' ' ) ;
                        pm.checkBox ( 'ignoreSolverWind_cbx' , label = 'ignoreSolverWind' , cc = ignoreSolverWind_cmd ) ;

                        pm.text ( label = ' ' ) ;
                        pm.checkBox ( 'disableFollicleAnim_cbx' , label = 'disableFollicleAnim' , cc = disableFollicleAnim_cmd ) ;

            turbulenceFrameLayout = pm.frameLayout ( label = 'TURBULENCE' , bgc = ( 0 , 0 , 0.5 ) ,
                collapsable = True , cc = collapseCommand ) ;
            with turbulenceFrameLayout :

                turbulenceMainLayout = pm.rowColumnLayout ( nc = 1 , w = width ) ;
                with turbulenceMainLayout :
            
                    turbulenceAttLayout = pm.rowColumnLayout ( nc = 2 , cw = [
                        ( 1 , ( width / 10.0 ) * 6.25 ) ,
                        ( 2 , ( width / 10.0 ) * 3.75 ) ] ) ;
                    with turbulenceAttLayout :    
                                        
                        pm.text ( 'ATTRIBUTE' , bgc = (  0.1 , 0.5 , 0.1 ) , fn = 'boldLabelFont' ) ;
                        pm.text ( 'OFFSET' , bgc = ( 0.5 , 1 , 0 ) , fn = 'boldLabelFont' ) ;
            
                    turbulenceLayout = pm.rowColumnLayout ( nc = 4 , cw = [
                        ( 1 , ( width / 10.0 ) * 4 ) ,
                        ( 2 , ( width / 10.0 ) * 2.25 ) ,
                        ( 3 , ( width / 10.0 ) * 1.5 ) ,
                        ( 4 , ( width / 10.0 ) * 2.25 ) ] ) ;
                    with turbulenceLayout :

                        pm.text ( label = 'turbulenceStrength' ) ;
                        pm.floatField ( 'turbulenceStrength_val' , pre = 3 , cc = turbulenceStrength_cmd ) ;
                        with pm.optionMenu ( 'turbulenceStrength_opm' , cc = turbulenceStrength_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'turbulenceStrength_offsetVal' , value = 0.1 , pre = 3 , cc = turbulenceStrength_cmd ) ;

                        pm.text ( label = 'turbulenceFrequency' ) ;
                        pm.floatField ( 'turbulenceFrequency_val' , pre = 3 , cc = turbulenceFrequency_cmd ) ;
                        with pm.optionMenu ( 'turbulenceFrequency_opm' , cc = turbulenceFrequency_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'turbulenceFrequency_offsetVal' , value = 0.1 , pre = 3 , cc = turbulenceFrequency_cmd ) ;

                        pm.text ( label = 'turbulenceSpeed' ) ;
                        pm.floatField ( 'turbulenceSpeed_val' , pre = 3 , cc = turbulenceSpeed_cmd ) ;
                        with pm.optionMenu ( 'turbulenceSpeed_opm' , cc = turbulenceSpeed_cmd ) :
                            pm.menuItem ( label='+' ) ;
                            pm.menuItem ( label='-' ) ;
                        pm.floatField ( 'turbulenceSpeed_offsetVal' , value = 0.1 , pre = 3 , cc = turbulenceSpeed_cmd ) ;

    hairSystem = updateHairSystemList ( ) ;
    setHairSystemUIAttribute ( hairSystem = hairSystem [0] ) ;

    window.show () ;