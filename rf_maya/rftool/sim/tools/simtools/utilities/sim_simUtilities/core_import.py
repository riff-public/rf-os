import pymel.core as pm ;
import maya.cmds as mc;
import os ;
from tools.artist_tools.tool_sam.origin import auto_origin
reload ( auto_origin )

class Import_GUI ( object ) :
    
    def __init__ ( self ) :
        self.currentProjectPath = pm.workspace ( q = True , rootDirectory = True ) ;
        self.dataProjectPath = self.currentProjectPath + 'data' ;
        self.shotCachePath      = self.currentProjectPath + 'data/shotCache/hero' ;
        self.camCachePath       = self.currentProjectPath + 'data/cam' ;


    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def bwdGrp ( self , *args ) :

        if pm.objExists ( 'BWD_GRP' ) :
            bwd_grp = pm.general.PyNode ( 'BWD_GRP' ) ;
        else :
            bwd_grp = pm.group ( em = True , w = True , n = 'BWD_GRP' ) ;
            bwd_grp.t.lock() ;
            bwd_grp.r.lock() ;
            bwd_grp.s.lock() ;

        return bwd_grp ;
    def lock_Grp ( self , *args ) :

        if pm.objExists ( 'LOCK_GRP' ) :
            lock_grp = pm.general.PyNode ( 'LOCK_GRP' ) ;
        else :
            lock_grp = pm.group ( em = True , w = True , n = 'LOCK_GRP' ) ;
            lock_grp.t.lock() ;
            lock_grp.r.lock() ;
            lock_grp.s.lock() ;

        return lock_grp ;

    def coutnamespace_ABC(self,*args):
        listnameSp = pm.namespaceInfo( listOnlyNamespaces=True );
        i=0
        for each in listnameSp:
            if 'ABC' in each:
                i+=1
                newName = 'ABC'+ str(i)
        return newName

    def coutnamespace_Tech(self,*args):
        listnameSp = pm.namespaceInfo( listOnlyNamespaces=True );
        i=0
        for each in listnameSp:
            if 'Tech' in each:
                i+=1
                newName = 'Tech'+ str(i)
        return newName

    def coutnamespace_CAM(self,*args):
        listnameSp = pm.namespaceInfo( listOnlyNamespaces=True );
        i=0
        for each in listnameSp:
            if 'CAM' in each:
                i+=1
                newName = 'CAM'+ str(i)
        return newName
            
    def importBtn_cmd (self,*args):
        item = pm.textScrollList( self.textlist, q=True, si=True )
#        for items in item:
#            itempath = self.dataProjectPath + '/' + items
#            pm.system.importFile( itempath )
        for items in item:
            itempath = self.dataProjectPath + '/' + items
            pm.system.importFile( itempath , namespace = 'Namesp' )

            geoGrp = mc.ls(typ='transform' )
            for each in geoGrp:
                if 'Namesp' in each and ':Geo_Grp' in each:
                    if mc.namespace(ex='ABC') == False :
                        mc.namespace(add='ABC')
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp,'ABC'))
                        mc.namespace(rm=namesp)

                    elif mc.namespace(ex='ABC'):
                        name = self.coutnamespace_ABC()
                        mc.namespace(add=name)
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp,name))
                        mc.namespace(rm=namesp)

                elif 'Namesp' in each and ':TechGeo_Grp' in each:
                    if mc.namespace(ex='Tech') == False :
                        mc.namespace(add='Tech')
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp , 'Tech'))
                        mc.namespace(rm=namesp)

                    elif mc.namespace(ex='Tech'):
                        name = self.coutnamespace_Tech()
                        mc.namespace(add=name)
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp,name))
                        mc.namespace(rm=namesp)

                elif 'Namesp' in each and ':camshot' in each:
                    if mc.namespace(ex='CAM') == False :
                        mc.namespace(add='CAM')
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp , 'CAM'))
                        mc.namespace(rm=namesp)

                    elif mc.namespace(ex='CAM'):
                        name = self.coutnamespace_CAM()
                        mc.namespace(add=name)
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp,name))
                        mc.namespace(rm=namesp)

                # else :
        if mc.namespace(ex='Namesp'):
            mc.namespace(rm='Namesp',mnr=True)

        if pm.checkBox ( self.charImportType_cbx , q = True , v = True ):
            sel = mc.ls(typ='transform')
            abcgrp = ''
            for each in sel:
                if ':Geo_Grp' in each and 'ABC' in each:
                    abcgrp = each
            for each in sel:
                if 'CIN:Geo_Grp' in each :
                    if 'ABC' in abcgrp :
                        mc.select ( abcgrp ) ;
                        mc.select ( '*CIN:Geo_Grp' ,add = True) ;
                        mc.blendShape (  origin = 'world' , automatic = True, weight = [ 0 , 1.0 ] , n = 'ABC_BSH' )
                        if mc.getAttr('%s.visibility' % (abcgrp)):
                            mc.setAttr('%s.visibility' %(abcgrp) ,0 )

        lockGrp = ['ABC*:Geo_Grp','CAM*:camshot']
        for lock in lockGrp:
            locks = pm.ls(lock, r=True)[0]
            locks.t.lock()
            locks.r.lock()
            locks.s.lock()

    def importBWD_cmd (self,*args):
        item = pm.textScrollList( self.textlist, q=True, si=True )
        bwd_grp = self.bwdGrp()

        for items in item:
            itempath = self.dataProjectPath + '/' + items
            pm.system.importFile( itempath , namespace='Namesp' )

            geoGrp = mc.ls(typ='transform')
            for each in geoGrp:
                if 'Namesp' in each and ':Geo_Grp' in each:
                    if mc.namespace(ex='ABC') == False :
                        mc.namespace(add='ABC')
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp,'ABC'))
                        mc.namespace(rm=namesp)

                    elif mc.namespace(ex='ABC'):
                        name = self.coutnamespace_ABC()
                        mc.namespace(add=name)
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp,name))
                        mc.namespace(rm=namesp)

                elif 'Namesp' in each and ':TechGeo_Grp' in each:
                    if mc.namespace(ex='Tech') == False :
                        mc.namespace(add='Tech')
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp , 'Tech'))
                        mc.namespace(rm=namesp)

                    elif mc.namespace(ex='Tech'):
                        name = self.coutnamespace_Tech()
                        mc.namespace(add=name)
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp,name))
                        mc.namespace(rm=namesp)

                elif 'Namesp' in each and ':camshot' in each:
                    if mc.namespace(ex='CAM') == False :
                        mc.namespace(add='CAM')
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp , 'CAM'))
                        mc.namespace(rm=namesp)

                    elif mc.namespace(ex='CAM'):
                        name = self.coutnamespace_CAM()
                        mc.namespace(add=name)
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp,name))
                        mc.namespace(rm=namesp)

        if mc.namespace(ex='Namesp'):
            mc.namespace(rm='Namesp',mnr=True)

        if pm.checkBox ( self.charImportType_cbx , q = True , v = True ):
            sel = mc.ls(typ='transform')
            abcgrp = ''
            for each in sel:
                if ':Geo_Grp' in each and 'ABC' in each:
                    abcgrp = each
            for each in sel:
                if 'CIN:Geo_Grp' in each :
                    if 'ABC' in abcgrp :
                        mc.select ( abcgrp ) ;
                        mc.select ( '*CIN:Geo_Grp' ,add = True) ;
                        mc.blendShape (  origin = 'world' , automatic = True, weight = [ 0 , 1.0 ] , n = 'ABC_BSH' )
                        if mc.getAttr('%s.visibility' % (abcgrp)):
                            mc.setAttr('%s.visibility' %(abcgrp) ,0 )
        sel = mc.ls(typ='transform')
        for n in sel:
            
            if 'ABC:Geo_Grp' in n:
                pm.parent('ABC*:Geo_Grp',bwd_grp)
            if 'CAM:camshot' in n:
                pm.parent('CAM*:*',bwd_grp)
            if 'Tech:TechGeo_Grp' in n:
                pm.parent('Tech:TechGeo_Grp',bwd_grp)
        
        listBWD = pm.listRelatives('BWD_GRP',children=True)
        for item in listBWD :
            if item == 'CAM*:*parentConstraint1*':
                pm.delete(item)

        lockGrp = ['ABC*:Geo_Grp','CAM*:camshot']
        for lock in lockGrp:
            locks = pm.ls(lock, r=True)[0]
            locks.t.lock()
            locks.r.lock()
            locks.s.lock()

    def importBWDORI_cmd (self,*args):
        item = pm.textScrollList( self.textlist, q=True, si=True )
        bwd_grp = self.bwdGrp()
        lock_grp = self.lock_Grp()

        for items in item:
            itempath = self.dataProjectPath + '/' + items
            pm.system.importFile( itempath , namespace='Namesp' )

            geoGrp = mc.ls(typ='transform')
            for each in geoGrp:
                if 'Namesp' in each and ':Geo_Grp' in each:
                    if mc.namespace(ex='ABC') == False :
                        mc.namespace(add='ABC')
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp,'ABC'))
                        mc.namespace(rm=namesp)

                    elif mc.namespace(ex='ABC'):
                        name = self.coutnamespace_ABC()
                        mc.namespace(add=name)
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp,name))
                        mc.namespace(rm=namesp)

                elif 'Namesp' in each and ':TechGeo_Grp' in each:
                    if mc.namespace(ex='Tech') == False :
                        mc.namespace(add='Tech')
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp , 'Tech'))
                        mc.namespace(rm=namesp)

                    elif mc.namespace(ex='Tech'):
                        name = self.coutnamespace_Tech()
                        mc.namespace(add=name)
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp,name))
                        mc.namespace(rm=namesp)

                elif 'Namesp' in each and ':camshot' in each:
                    if mc.namespace(ex='CAM') == False :
                        mc.namespace(add='CAM')
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp , 'CAM'))
                        mc.namespace(rm=namesp)

                    elif mc.namespace(ex='CAM'):
                        name = self.coutnamespace_CAM()
                        mc.namespace(add=name)
                        namesp = each.split(':')[0]
                        mc.namespace(mv=(namesp,name))
                        mc.namespace(rm=namesp)

        if mc.namespace(ex='Namesp'):
            mc.namespace(rm='Namesp',mnr=True)

        if pm.checkBox ( self.charImportType_cbx , q = True , v = True ):
            sel = mc.ls(typ='transform')
            abcgrp = ''
            for each in sel:
                if ':Geo_Grp' in each and 'ABC' in each:
                    abcgrp = each
            for each in sel:
                if 'CIN:Geo_Grp' in each :
                    if 'ABC' in abcgrp :
                        mc.select ( abcgrp ) ;
                        mc.select ( '*CIN:Geo_Grp' ,add = True) ;
                        mc.blendShape (  origin = 'world' , automatic = True, weight = [ 0 , 1.0 ] , n = 'ABC_BSH' )
                        if mc.getAttr('%s.visibility' % (abcgrp)):
                            mc.setAttr('%s.visibility' %(abcgrp) ,0 )

        sel = mc.ls(typ='transform')
        for n in sel:
            
            if 'ABC:Geo_Grp' in n:
                pm.parent('ABC*:Geo_Grp',bwd_grp)
            if 'CAM:camshot' in n:
                pm.parent('CAM*:*',lock_grp)
                auto_origin.auto_origin().load_origin()
                pm.parent(lock_grp,'ORI_GRP')
                pm.parent('ORI_GRP',bwd_grp)
            if 'Tech:TechGeo_Grp' in n:
                pm.parent('Tech:TechGeo_Grp',bwd_grp)
        
        
        pm.parent('ORI_GRP',bwd_grp)

        listBWD = pm.listRelatives('LOCK_GRP',children=True)
        for item in listBWD :
            if item == 'CAM*:*parentConstraint1*':
                pm.delete(item)

        lockGrp = ['ABC*:Geo_Grp','CAM*:camshot']
        for lock in lockGrp:
            locks = pm.ls(lock, r=True)[0]
            locks.t.lock()
            locks.r.lock()
            locks.s.lock()

    def errorchack(self,*args):
        errortext = ''
        if pm.checkBox ( self.charImportType_cbx , q = True , v = True ):
            sel = mc.ls('*CIN:Geo_Grp',typ='transform')
            if sel == []:
                errortext =  'Not CIN:ABC'
        return errortext

    # def refresh_cmd(self,*args):
    #     self.errorchack()

    def insert ( self , width ) :

        listItem = os.listdir(self.dataProjectPath);
        listItemm = []
        for files in listItem:
            if files.endswith('.abc'):
                listItemm.append(files)

        with pm.rowColumnLayout ( nc = 1 , w = width) :
            pm.text (label = 'import',w=width);
            self.textlist = pm.textScrollList ( 'import_textScrollList' , ams = True , h = 200 , append = listItemm )

            self.charImportType_cbx = pm.checkBox ( 'charImportType_cbx' , label = 'BlendShape ABC' , v = True , enable = True  ) ;
            texterror = self.errorchack()
            pm.text(label=texterror,align='left')

#            pm.text(label=self.errorchack,align='left')
                   

        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2) , ( 2 , width/2 ) ] ) :
            pm.button( 'import_btn' , label = 'Import' , w = width/2 ,c=self.importBtn_cmd ) ;
            pm.button( 'BWDimport_btn' , label = 'BWD Import' , w = width/2,c=self.importBWD_cmd ) ;

        pm.button(label = 'BWD Import(ORI)' , w = width ,c=self.importBWDORI_cmd )

        # with pm.rowColumnLayout(nc = 2 ,cw = [ ( 1 , width/2) , ( 2 , width/2 ) ]):
        #     pm.button( 'refresh_Bth' , label = 'refresh' , w = width/2 ,c=self.refresh_cmd )
