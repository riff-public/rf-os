import pymel.core as pm ;
import os , re ;
import maya.cmds as mc ;
import maya.OpenMaya as om ;
import maya.OpenMayaUI as omui ;
import maya.mel as mel  ;

path = os.environ['RFSCRIPT']

from tools.simtools.utilities.sim_simUtilities import core_nucleus ;
reload ( core_nucleus ) ;

nucleus = core_nucleus.NucleusNode ( ) ;

class Playblast_GUI ( object ) :

    def __init__ ( self,ListCamera=[]) :
        self.currentProject = pm.workspace ( q = True , rootDirectory = True ) ;
        self.preferencePath = self.getPreferencePath ( ) ;
        self.ListCamera = ListCamera ;
        self.returnCamera = self.returnActiveCamera();

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def getPreferencePath ( self ) :
        
        myDocuments = os.path.expanduser('~') ;
        myDocuments = myDocuments.replace ( '\\' , '/' ) ;
        # C:/Users/Legion/Documents

        prefPath = myDocuments + '/birdScriptPreferences/simulationUtilities/'

        if not os.path.exists ( prefPath ) :
            os.makedirs ( prefPath ) ;

        return prefPath ;

    def openPlayblastPathBtn_cmd ( self , *args ) :

        path = pm.textField ( self.playblastPath_textField , q = True , tx = True ) ;

        if os.path.exists ( path ) :
           os.startfile ( path ) ;

    def browseBtn_cmd ( self , *args ) :

        startingDirectory = pm.textField ( self.playblastPath_textField , q = True , tx = True ) ;

        playblastLocationPath = pm.fileDialog2 ( fileMode = 3 , fileFilter = '0' , dir = startingDirectory ) [0] ;
        if playblastLocationPath[-1] != '/' :
            playblastLocationPath += '/'

        #print playblastLocationPath ;

        pm.textField ( self.playblastPath_textField , e = True , tx = playblastLocationPath ) ;

        playblastDefaultPathTxt_file = open ( self.preferencePath + '/playblastDefaultPath.txt' , 'w+' ) ;
        playblastDefaultPathTxt_file.write ( playblastLocationPath ) ;
        playblastDefaultPathTxt_file.close ( ) ;

    def updatePlayblastDefaultPathTextField ( self ) :

        if not os.path.exists ( self.preferencePath + '/playblastDefaultPath.txt' ) :
            pm.textField ( self.playblastPath_textField , e = True , tx = 'Please browse and select default directory' ) ;

        else :
            
            playblastDefaultPathTxt_file = open ( self.preferencePath + '/playblastDefaultPath.txt' , 'r+' ) ;
            playblastDefaultPath = playblastDefaultPathTxt_file.read() ;
            playblastDefaultPathTxt_file.close() ;

            pm.textField ( self.playblastPath_textField , e = True , tx = playblastDefaultPath ) ;

    def composePlayblastName ( self , *args ) :
        
        try :

            # self.currentProject ;
            # D:/TwoHeroes/film001/q0420/s0160/spiderGirlGod_001/

            nameElem = self.currentProject.split('/') ;

            # for elem in nameElem :
            #     if elem == '' :
            #         nameElem.remove ( elem ) ;

            # sequence    = nameElem[-3] ;
            # shot        = nameElem[-2] ;
            # charName    = nameElem[-1] ;        
            
            sequence    = nameElem[3].split('_');
            newSequence = sequence[2] + '_' + sequence[3]
            shot        = nameElem[-2] ;
            charName    = nameElem[-1] ; 
            scenePath       = pm.system.sceneName() ;
            
            if scenePath :
                scenePathElem   = scenePath.split('/') ;
                for elem in scenePathElem :
                    if elem == '' :
                        scenePathElem.remove ( elem ) ;

                sceneName = scenePathElem[-1] ;
            else :
                sceneName = 'untitled' ;

            version     = re.findall ( 'v' + '[0-9]{3}' + '_' + '[0-9]{3}' , sceneName ) ;
            if version :
                version = version[0] ;

            if '.' in sceneName :

                sceneNameElem = sceneName.split('.') ;

                #print sceneNameElem ;

                for elem in sceneNameElem :

                    if elem == '' :
                        sceneNameElem.remove ( elem ) ;
     
                #print sceneNameElem ;

                sceneName = sceneNameElem[-3] ;


            # name = sequence + '.' + shot + '.' + charName + '.' + sceneName ;
            name = newSequence + '.' + sceneName ;
            
            if version :
                name += '.' + version ;

        except :
            name = 'none' ;

        return name ;
        
    def updatePlayblastName ( self , *args ) :
         playblastName = self.composePlayblastName ( ) ;
         pm.textField ( self.playblastName_textField , e = True , tx = playblastName )

    def getSceneResolution ( self ) :
        width   = pm.getAttr ( "defaultResolution.width" ) ;
        height  = pm.getAttr ( "defaultResolution.height" ) ;
        return [ width , height ] ;

    def returnActiveCamera(self, *args):
        view = omui.M3dView.active3dView()
        camera = om.MDagPath()
        view.getCamera(camera)
        return om.MFnDagNode(camera.transform()).name()
#    print returnActiveCamera()

    def getUsername_cmd ( *args ) :
        if os.environ['RFuser']:
            return os.environ['RFuser']
        else:
            return ''

    def clearHUD( *args ):
        pm.headsUpDisplay(rp = ( 5, 0 ));
        pm.headsUpDisplay(rp = ( 5, 1 ));
        pm.headsUpDisplay(rp = ( 9, 0 ));
        pm.headsUpDisplay(rp = ( 9, 1 ));

    def visibilityHUD( self, visHUD = False, *args ):
        if visHUD == False:
            print '1'
            mel.eval( 'setAnimationDetailsVisibility(0)' );
            mel.eval( 'setCameraNamesVisibility(0)' );
            mel.eval( 'setCapsLockVisibility(0)' );
            mel.eval( 'setCurrentContainerVisibility(0)' );
            mel.eval( 'setCurrentFrameVisibility(0)' );
            mel.eval( 'setFocalLengthVisibility(0)' );
            mel.eval( 'setFrameRateVisibility(0)' );
            mel.eval( 'setHikDetailsVisibility(0)' );
            mel.eval( 'setObjectDetailsVisibility(0)' );
            mel.eval( 'setParticleCountVisibility(0)' );
            mel.eval( 'setPolyCountVisibility(0)' );
            mel.eval( 'setSceneTimecodeVisibility(0)' );
            mel.eval( 'setSelectDetailsVisibility(0)' );
            mel.eval( 'setSymmetryVisibility(0)' );
            mel.eval( 'setViewAxisVisibility(0)' );
            mel.eval( 'setViewportRendererVisibility(0)' );
            # mel.eval( 'setXGenHUDVisibility(0)' );
        elif visHUD == True:
            print '2' 
            mel.eval( 'setCameraNamesVisibility(1)' );
            mel.eval( 'setCurrentFrameVisibility(1)' );

    def textHeadUp(self,*args):
        self.user = getUsername_cmd()


    def CameraList_cmd(self,*args):
#        self.CameraList = CameraList        
        CameraList = mc.listCameras(p=True)
        i=0
        for CameraLists in CameraList:
            if CameraList[i][-5:] == 'Shape':
                CameraList[i] = CameraList[i][0:-5]
                
#            mc.menuItem(l=CameraList[i])
            i=i+1
        #print CameraList
        self.CameraList = CameraList
        return self.CameraList

    def menuGUI(self,*arge):
        FT_CameraList = self.CameraList_cmd()
        OP_Menu = mc.optionMenu(self.playblast_cmr,q=True,ill=True)
        for i in OP_Menu:
            pm.deleteUI(i)
        for i in range(len(FT_CameraList)):            
           self.Menuitem = mc.menuItem(l=FT_CameraList[i],p=self.playblast_cmr)

    def pmcurrTime( *args ):
        curr = pm.currentTime( q = True );
        return int(curr);
    
    def anti_aliasing( self , *args ):
        hardwareRenderingGlobals = pm.general.PyNode('hardwareRenderingGlobals') ;
        hardwareRenderingGlobals.multiSampleEnable.set(1);
        hardwareRenderingGlobals.multiSampleCount.set(int(16));

    def toggleAntiAliasing_cmd ( self ,*args ) :
        
        hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
        
        if hardwareRenderingGlobals.multiSampleEnable.get() == 0 :
            hardwareRenderingGlobals.multiSampleEnable.set(1) ;
            multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = True ) ;

        elif hardwareRenderingGlobals.multiSampleEnable.get() == 1 :
            hardwareRenderingGlobals.multiSampleEnable.set(0) ;
            multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = False ) ;

        self.sampleCountChange_cmd( );

    def sampleCountChange_cmd( self ,*args ) :
        
        hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
        
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , q = True , value = True ) ;
        multiSampleCount = int ( multiSampleCount ) ;
        hardwareRenderingGlobals.multiSampleCount.set ( multiSampleCount ) ;

    def setPlayblast_cmd( self,*args ):
        self.toggleAntiAliasing_cmd();
        # show output cloth and output hair yeti
        mel.eval('modelEditor -e -polymeshes true modelPanel4;modelEditor -e -hos true modelPanel4;');
        mel.eval('modelEditor -e -pluginShapes true modelPanel4;');
        try:
            pm.select( '*_YetiNode', r = True );
            mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );
        except:
            pass;
        mel.eval( 'ActivateViewport20;' );
        listNamespace = pm.namespaceInfo( lon=True );
        for camName in listNamespace:
            if 'CAM' in camName:
                mel.eval('lookThroughModelPanel CAM:camshotShape modelPanel4;');
            else:
                pass;
        pm.select( cl = True );

    def playblastBtn_cmd ( self , *args ) :
        # disable nucleus before playblast

        projectPathElem = self.currentProject.split('/') ;
        for elem in projectPathElem :
            if elem == '' :
                projectPathElem.remove ( elem ) ;
        charName    = projectPathElem [-1] ;

        sceneResolution = self.getSceneResolution () ;
        resW = sceneResolution[0] ;
        resH = sceneResolution[1] ;

        playblastStart  = pm.intField ( self.playblastStart_intField    , q = True , v = True ) ;
        playblastEnd    = pm.intField ( self.playblastEnd_intField      , q = True , v = True ) ;        
        incrementMethod = pm.optionMenu ( self.increment_opt , q = True , v = True ) ;
        scale           = pm.intField ( self.playblastScale_intField , q = True , v = True ) ;

        name            = pm.textField ( self.playblastName_textField , q = True , tx = True ) ;
        playblastPath   = pm.textField ( self.playblastPath_textField , q = True , tx = True ) ;
        increment       = self.checkIncrement ( name , playblastPath ) ;

        playblastType   = pm.optionMenu ( self.playblast_opt , q = True , v = True ) ;
        range = playblastStart - playblastEnd
        #print playblastPath + name + '.' + increment ;
        #print playblastPath + name + '.' + increment + '/' + charName ;

        nucleus.getEnableState ( ) ;
        nucleus.setEnable ( False ) ;
        self.visibilityHUD(False);

        if playblastType == 'Tiff' :

            if not os.path.exists ( playblastPath + name + '.' + increment ) :
                os.makedirs ( playblastPath + name + '.' + increment ) ;

            self.anti_aliasing();
            self.clearHUD();
            if pm.checkBox( self.headupDisplay_cbx ,q=True,v=True):
                mc.headsUpDisplay( 'HudSCN', s = 5, b = 0, bs = 'small', l = 'Playblast Name  :  ' + name );
                mc.headsUpDisplay( 'HudUSN', s = 5, b = 1, bs = 'small', l = 'User  :  ' + self.getUsername_cmd() );
                mc.headsUpDisplay( 'HudFRN', s = 9, b = 0, bs = 'small', l = 'Frame Range  :   %s  -  %s ' % ( playblastStart , playblastEnd),c = self.pmcurrTime, atr = True );
                mc.headsUpDisplay( 'HudFNB', s = 9, b = 1, bs = 'small', l = 'Shots : ' + self.currentProject.split('/')[3] );

            cameraType = pm.optionMenu(self.playblast_cmr,q=True , v=True)
            self.returnCamera = self.returnActiveCamera()
            mc.lookThru(cameraType)

            pm.playblast (
                format          = 'image' ,
                startTime       = playblastStart ,
                endTime         = playblastEnd ,
                filename        = playblastPath + name + '.' + increment + '/' + charName ,
                sequenceTime    = 0 ,
                clearCache      = 0 ,
                viewer          = 0 ,
                showOrnaments   = 0 ,
                offScreen       = True ,
                fp              = 4 , # framePadding
                percent         = scale ,
                compression     = 'tif' ,
                quality         = 100 ,
                widthHeight     = [ resW , resH ] ,
                ) ;
            mc.lookThru(self.returnCamera)
            self.clearHUD();

        if playblastType == 'Video' :

            self.anti_aliasing();
            self.clearHUD();
            if pm.checkBox( self.headupDisplay_cbx ,q=True,v=True):
                mc.headsUpDisplay( 'HudSCN', s = 5, b = 0, bs = 'small', l = 'Playblast Name  :  ' + name );
                mc.headsUpDisplay( 'HudUSN', s = 5, b = 1, bs = 'small', l = 'User  :  ' + self.getUsername_cmd() );
                mc.headsUpDisplay( 'HudFRN', s = 9, b = 0, bs = 'small', l = 'Frame Range  :   %s  -  %s ' % ( playblastStart , playblastEnd),c = self.pmcurrTime, atr = True );
                mc.headsUpDisplay( 'HudFNB', s = 9, b = 1, bs = 'small', l = 'Shots : ' + self.currentProject.split('/')[3] );

            cameraType = pm.optionMenu(self.playblast_cmr,q=True , v=True)
            self.returnCamera = self.returnActiveCamera()
            mc.lookThru(cameraType)

            pm.playblast (
                format          = 'qt' ,
                startTime       = playblastStart ,
                endTime         = playblastEnd ,
                filename        = playblastPath + name + '.' + increment + '.mov' ,
                clearCache      = True ,
                viewer          = True ,
                showOrnaments   = True ,
                offScreen       = True ,
                fp              = 4 , # framePadding
                percent         = scale ,
                compression     = "MPEG-4 Video" ,
                quality         = 100 ,
                widthHeight     = [ resW , resH ] ,
                forceOverwrite  = True ,
                ) ;
            mc.lookThru(self.returnCamera)
            self.clearHUD();

        if playblastType == 'View' :

            self.clearHUD();
            if pm.checkBox( self.headupDisplay_cbx ,q=True,v=True):
                mc.headsUpDisplay( 'HudSCN', s = 5, b = 0, bs = 'small', l = 'Playblast Name  :  ' + name );
                mc.headsUpDisplay( 'HudUSN', s = 5, b = 1, bs = 'small', l = 'User  :  ' + self.getUsername_cmd() );
                mc.headsUpDisplay( 'HudFRN', s = 9, b = 0, bs = 'small', l = 'Frame Range  :   %s  -  %s ' % ( playblastStart , playblastEnd),c = self.pmcurrTime, atr = True );
                mc.headsUpDisplay( 'HudFNB', s = 9, b = 1, bs = 'small', l = 'Shots : ' + self.currentProject.split('/')[3] );

            cameraType = pm.optionMenu(self.playblast_cmr,q=True , v=True)
            self.returnCamera = self.returnActiveCamera()
            mc.lookThru(cameraType)

            pm.playblast (
                format          = 'image' ,
                startTime       = playblastStart ,
                endTime         = playblastEnd ,
                sequenceTime    = 0 ,
                clearCache      = 1 ,
                viewer          = True ,
                showOrnaments   = True ,
                offScreen       = True ,
                fp              = 4 ,
                percent         = scale ,
                compression     = "maya" ,
                quality         = 100 ,
                widthHeight     = [ resW , resH ] ,
                ) ;

            mc.lookThru(self.returnCamera)
            self.clearHUD();

        self.visibilityHUD(True);
        nucleus.reset() ;

    def updateIncrementMethodGUI ( self , *args ) :

        if pm.optionMenu ( self.playblast_opt , q = True , v = True ) == 'View' :
            pm.optionMenu ( self.increment_opt , e = True , enable = False ) ;
        else :
            pm.optionMenu ( self.increment_opt , e = True , enable = True ) ;

    def checkIncrement ( self , name , path , incrementIncrement = False , *args ) :
        # check for '.xxxx.' e.g. '.0001.'

        ### add on start ###
        incrementMethod = pm.optionMenu ( self.increment_opt , q = True , v = True ) ;

        if incrementMethod == 'Version' :
            incrementIncrement = True ;

        playblastType   = pm.optionMenu ( self.playblast_opt , q = True , v = True ) ;
        extension       = '.mov'
        ### add on end ###

        file_list = os.listdir ( path ) ;
        increment_list = [] ;

        if not file_list :
            finalIncrement = '0001' ;

        else :
            for file in file_list :

                condition1 = False  ;
                condition2 = True   ;

                if name in str ( file ) :
                    condition1 = True ;

                if playblastType == 'Video' :
                    if not extension in str ( file ) :
                        condition2 = False ;
                else :
                    if extension in str ( file ) :
                        condition2 = False ;

                if condition1 and condition2 :
                    ### add on ###
                    increment = re.findall ( '\.' + '[0-9]{4}' , file ) ;
                    if increment :
                        increment = increment[0] ;
                        increment_list.append ( increment ) ;

            if not increment_list :
                finalIncrement = '0001' ;
            else :
                increment_list.sort() ;
                currentIncrement = increment_list[-1] ;
                increment = currentIncrement.split('.')
                for split in increment :
                    if split == '' :
                        increment.remove ( split ) ;
                increment = increment[0] ;
                increment = int ( increment ) ;

                if incrementIncrement :
                    increment += 1 ;

                finalIncrement = str(increment).zfill(4) ;

        return finalIncrement ;

    def insert ( self , width ) :
        width = width*0.98 ;

        with pm.rowColumnLayout ( nc = 5 , cw = [ ( 1 , width/5 ) , ( 2 , width/5 ) , ( 3 , width/5 ) , ( 4 , width/5 ) , ( 5 , width/5 ) ] ) :

            pm.text ( label = '' ) ;
            
            with pm.rowColumnLayout ( nc = 1 , w = width/5 ) :
                self.playblastStart_intField = pm.intField ( 'playblastStart_intField' , w = width/5 ) ;
                pm.text ( label = 'Start' , w = width/5 ) ;
            
            pm.text ( label = '' ) ;

            with pm.rowColumnLayout ( nc = 1 , w = width/5 ) :
                self.playblastEnd_intField = pm.intField ( 'playblastEnd_intField' , w = width/5 ) ;
                pm.text ( label = 'End' , w = width/5 ) ;

            pm.text ( label = '' ) ;

        with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/3 ) , ( 2 , width/3 ) , ( 3 , width/3 ) ] ) :

            with pm.rowColumnLayout ( nc = 1 , w = width/3 ) :
                self.playblast_opt = pm.optionMenu ( 'playblast_opt' , w = width/3 , cc = self.updateIncrementMethodGUI ) ;
                with self.playblast_opt :
                    pm.menuItem ( label = 'View'    ) ;
                    pm.menuItem ( label = 'Video'   ) ;
                    pm.menuItem ( label = 'Tiff'    ) ; 
                pm.text ( label = 'Type' , w = width/3 ) ;

            with pm.rowColumnLayout ( nc = 1 , w = width/3 ) :
                self.increment_opt = pm.optionMenu ( 'increment_opt' , w = width/3 ) ;
                with self.increment_opt :
                    pm.menuItem ( label = 'Version' ) ;
                    pm.menuItem ( label = 'Replace' ) ;                    
                pm.text ( label = 'Increment' , w = width/3 ) ;

            with pm.rowColumnLayout ( nc = 1 , w = width/3 ) :
                self.playblastScale_intField = pm.intField ( 'playblastScale_intField' , value = 50 ) ;
                pm.text ( label = 'Scale (%)' ) ;

        pm.separator ( h = 5 , vis = False ) ;
        with pm.rowColumnLayout ( nc = 5,cw = [ ( 1 , width/4 ) , ( 2 , width/14 ), ( 3 , width/3 ) , ( 4 , width/70 ), ( 5 , width/3 ) ] ) :
            pm.text( label = ' Anti-aliasing ' );
            pm.separator( vis = False );
            with pm.optionMenu( 'sampleCount_ptm' , w = width/4 , changeCommand = self.sampleCountChange_cmd ) :
                pm.menuItem ( label= '16' ) ;
                pm.menuItem ( label= '8' ) ;
                pm.menuItem ( label= '4' ) ;
            pm.separator( vis = False );
            pm.button( l='Set Playblast', w = width/3, c= self.setPlayblast_cmd );
        pm.separator( h = 5 , vis = False );

        self.CameraList_cmd()
        with pm.rowColumnLayout ( nc = 5,cw = [ ( 1 , width/4 ) , ( 2 , width/14 ), ( 3 , width/3 ) , ( 4 , width/70 ), ( 5 , width/3 ) ] ) :
            pm.text ( label = 'Camera',w = width/3 ) ;
            pm.separator( vis = False );
            with pm.rowColumnLayout (nc = 1 , cw = [(1,95)]) :
                self.playblast_cmr = mc.optionMenu(self.ListCamera);
                for i in range(len(self.CameraList)):            
                    self.Menuitem = mc.menuItem(l=self.CameraList[i]);

            pm.separator( vis = False );
            pm.button ('listplayblast_btn',l='Refresh',w = width/3,c=self.menuGUI);
        pm.separator ( h = 5 , vis = False ) ;

        self.openPlayblast_btn = pm.button ( 'openPlayblast_btn' , label = 'Playblast Directory' , w = width , c = self.openPlayblastPathBtn_cmd ) ;
        self.playblastPath_textField = pm.textField ( 'playblastPath_textField' , w = width ) ;

        with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/3*2 ) , ( 2 , width/3 ) ,( 1 , width/3*2 )] ) :
            # pm.text ( label = '' ) ;
            pm.separator ( h = 5 , vis = False ) ;
            self.browse_btn = pm.button ( 'browse_btn' , label = 'Browse' , c = self.browseBtn_cmd ) ;
            pm.separator ( h = 5 , vis = False ) ;

        pm.text ( label = 'Playblast Name' , w = width ) ;
        self.playblastName_textField = pm.textField ( 'playblastName_textField' , w = width ) ;

        with pm.rowColumnLayout(nc=2):
            self.headupDisplay_cbx = pm.checkBox('headupDisplay_cbx',l='HeadsUp Display',v=True)
            pm.text(l='')

        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/3*2 ) , ( 2 , width/3 ) ] ) :
            pm.text ( label = '' ) ;
            self.refresh_btn = pm.button ( 'refresh_btn' , label = 'Refresh' , c = self.updatePlayblastName ) ;

        pm.separator ( h = 5 , vis = False ) ;

        self.playblast_btn = pm.button ( 'playblast_btn' , label = 'Playblast' , c = self.playblastBtn_cmd , w = width ) ;

        self.updateIncrementMethodGUI ( ) ;
        self.updatePlayblastDefaultPathTextField ( ) ;
        self.updatePlayblastName ( ) ;