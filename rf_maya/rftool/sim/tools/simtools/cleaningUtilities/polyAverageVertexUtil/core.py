import pymel.core as pm ;

import re ;

import simulationTools.sim.cleanUp.polyAverageVertexUtil.general as gen ;
reload ( gen ) ;

# selection = pm.ls ( sl = True ) ;

class PolyAverageVertex ( gen.NaturalSort ) :

    def __init__ ( self ) :
        pass ;

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def getPavNode ( self , target ) :
        # Pav = PolyAverageVertex
        
        target      = pm.general.PyNode ( target ) ;
        targetShape = target.getShape() ;
        pav_list    = targetShape.listConnections ( type = 'polyAverageVertex' ) ;

        pav_list = set ( pav_list ) ;
        pav_list = list ( pav_list ) ;
        pav_list = self.naturalSort ( pav_list ) ;

        return pav_list ;

class GUI ( PolyAverageVertex ) :

    def __init__ ( self ) :
        self.ui         = 'polyAverageVertexUtil_ui' ;
        self.width      = 500.00 ;
        self.title      = 'Poly Average Vertex Util' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def getTextScrollList ( self ) :
        # return [ object_list , textScrollList_list ] ;

        tab_list = pm.tabLayout ( self.pav_tabLayout , q = True , childArray = True ) ;
        
        object_list = [] ;
        textScrollList_list = [] ;

        if tab_list :
            
            for tab in tab_list :

                obj = tab.split ( '_rowColumnLayout' ) [0] ;
                object_list.append ( pm.general.PyNode ( obj ) ) ;

                textScrollList = obj + '_textScrollList' ;

                cmd = "textScrollList = self.{textScrollList} ;".format ( textScrollList = textScrollList ) ;
                exec ( cmd ) ;

                textScrollList_list.append ( textScrollList ) ;

        if ( object_list ) and ( textScrollList_list ) :
            return [ object_list , textScrollList_list ] ;
        else :
            return None ;

    def pavTabLayout_refresh ( self , *args ) :

        info = self.getTextScrollList() ;

        if info :

            object_list         = info[0] ;
            textScrollList_list = info[1] ;

            for object , textScrollList in zip ( object_list , textScrollList_list ) :

                    # remove all pav from current textScrollList
                    pm.textScrollList ( textScrollList , e = True , ra = True ) ;

                    pav_list = self.getPavNode ( target = object ) ;

                    for pav in pav_list :
                        pm.textScrollList ( textScrollList , e = True , append = pav ) ;
                    
    def pavTabLayout_removeAll ( self , *args ) :

        remove_list = pm.tabLayout ( self.pav_tabLayout , q = True , childArray = True ) ;

        if remove_list :
            for each in remove_list :
                pm.deleteUI ( each ) ;

    def pavTabLayout_loadPolygon ( self , *args ) :

        self.pavTabLayout_removeAll() ;

        w = self.width ;
        
        selection_list = pm.ls ( sl = True ) ;
        selection_list = self.naturalSort ( selection_list ) ;
        
        for selection in selection_list :

            selectionName = selection.nodeName() ;
            
            cmd = "test = self.{selectionName}_rowColumnLayout = ".format ( selectionName = selectionName ) ;
            cmd += "pm.rowColumnLayout ( selectionName + '_rowColumnLayout' , nc = 1 , p = self.pav_tabLayout , w = w/3*2 ) ;" ;
            exec ( cmd ) ;
            cmd = "child = self.{selectionName}_rowColumnLayout".format ( selectionName = selectionName ) ;
            exec ( cmd ) ;
            with child :
                
                cmd = "self.{selectionName}_textScrollList = ".format ( selectionName = selectionName ) ;
                cmd += "pm.textScrollList (  '{selectionName}_textScrollList' , ams = True , w = w/3*2 , h = 300 )".format ( selectionName = selectionName ) ;
                exec ( cmd ) ;

                pav_list = self.getPavNode ( target = selection ) ;

                cmd = "textScrollList = self.{selectionName}_textScrollList".format ( selectionName = selectionName ) ;
                exec ( cmd ) ;

                for pav in pav_list :
                    pm.textScrollList ( textScrollList , e = True , append = pav ) ;

            pm.tabLayout ( self.pav_tabLayout , e = True , tabLabel = ( ( child ,  selectionName ) ) ) ;

    def pavTabLayout_changeCommand ( self , *args ) :
        self.pavTabLayout_deselectAll() ;
        self.pavTabLayout_refresh() ;

    def pavTabLayout_deselectAll ( self , *args ) :

        info = self.getTextScrollList() ;
        if info :
            textScrollList_list = info[1] ;

            for textScrollList in textScrollList_list :
                pm.textScrollList ( textScrollList , e = True , deselectAll = True ) ;

    def pavTabLayout_querySelection ( self , *args ) :

        info = self.getTextScrollList() ;
        if info :
            textScrollList_list = info[1] ;
            
            for textScrollList in textScrollList_list :
                selection_list = pm.textScrollList ( textScrollList , q = True , selectItem = True ) ;
                
                if selection_list :
                    return selection_list ;

    def deleteSelectedPav ( self , *args ) :

        toDelete_list = self.pavTabLayout_querySelection() ;
        pm.delete ( toDelete_list ) ;
        self.pavTabLayout_refresh() ;

    def createPavAttr ( self , *args ) :

        # get current average vertex selection from text scroll list
        selection_list = self.pavTabLayout_querySelection() ;
        
        # get general info
        samplePav   = pm.general.PyNode ( selection_list[0] ) ;
        mesh        = samplePav.listConnections ( type = 'mesh' ) [0] ;
        meshShape   = mesh.getShape() ;
        
        # create globalAmp
        globalAmpAttrName = mesh.split ( '_' ) [0] ;
        globalAmpAttrName += globalAmpAttrName[0].upper() + globalAmpAttrName[1:] ;
        globalAmpAttrName += '_PavAmp' ;
        if not pm.attributeQuery ( globalAmpAttrName , node = meshShape , exists = True  ) :
            meshShape.addAttr ( globalAmpAttrName , keyable = True , attributeType = 'float' , max = 1 , min = 0 , dv = 1 ) ;
        exec ( 'globalAmpAttr = meshShape.%s' % globalAmpAttrName ) ;

        currentFrame = str ( int ( pm.currentTime ( query=True ) ) ) ;

        # create various names
        attrName    = 'F' + currentFrame + '_' ;
        index       = self.checkAttrIncrement ( shape = meshShape , inputStr = attrName ) ;
        attrName    += index ;
        ''''''
        pavName     = attrName ;
        ''''''
        ampName     = mesh.split ( '_' ) [0] + '_' + attrName + '_Mdv' ;
        attrName    += '_PavAttr' ;

        # create amp
        amp = self.createAmp ( name = ampName ) ;
        for axis in [ 'x' , 'y' , 'z' ] :
            exec ( 'pm.connectAttr ( globalAmpAttr , amp.i2%s )' % axis ) ;

        # create pav attribute on meshShape
        meshShape.addAttr ( attrName , keyable = True , attributeType = 'float' , max = 10 , min = 0 , dv = 10 ) ;
        exec ( "attr = meshShape.%s" % attrName ) ;

        # connect attribute to amp
        pm.connectAttr ( attr , amp.i1x ) ;

        # connect everything to pav iterations
        for selection in selection_list :
            selection = pm.general.PyNode ( selection ) ;
            selection.iterations.disconnect() ; 
            pm.connectAttr ( amp.ox , selection.iterations ) ;
            currentPavName = pavName + '_' + self.pavNameIncrement ( inputStr = pavName ) + '_Pav' ;
            pm.rename ( selection , currentPavName ) ;

        self.pavTabLayout_refresh () ;
        pm.select ( mesh ) ;

    def createAmp ( self , name ) :
        if pm.objExists ( name ) :
            return pm.general.PyNode ( name ) ;
        else :
            amp = pm.createNode ( 'multiplyDivide' ) ;
            amp.rename ( name ) ;
            return amp ; 

    def checkAttrIncrement ( self , shape , inputStr ) :
        
        shape = pm.general.PyNode ( shape ) ;
        
        allAttr_list = shape.listAttr ( userDefined = True ) ;
        attr_list    = [] ;

        for attr in allAttr_list :
            if re.findall ( inputStr + '[0-9]+' , str(attr) ) :
                attr_list.append ( attr ) ;

        attr_list = self.naturalSort ( attr_list ) ;

        if attr_list :
            latestIncrement = attr_list[-1] ;
            latestIncrement = re.findall ( '_' + '[0-9]+' + '_' , str ( latestIncrement ) ) [0] ;
            latestIncrement = re.findall ( '[0-9]+' , str ( latestIncrement ) ) [0] ;
            newIncrement    = int ( latestIncrement ) + 1 ;
            return str ( newIncrement ) ;

        else :
            return '1' ;  

    def pavNameIncrement ( self , inputStr ) :

        pav_list = pm.ls ( '%s*' % inputStr , type = 'polyAverageVertex' ) ;

        if pav_list :

            pav_list = self.naturalSort ( pav_list ) ;

            latestIncrement = pav_list[-1] ;
            latestIncrement = re.findall ( '[0-9]+' + '_Pav' , str ( latestIncrement ) ) [0] ;
            latestIncrement = re.findall ( '[0-9]+' , str ( latestIncrement ) ) [0] ;
            newIncrement    = int ( latestIncrement ) + 1 ;
            return str ( newIncrement ) ;

        else :
            return '1' ;

    def deletePavAttr ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;
        selectedAttr_list = pm.channelBox ( "mainChannelBox" , q = 1 , ssa = 1 ) ;

        for selection in selection_list :
            selectionShape = selection.getShape() ;

            # prevent script from deleting global amp
            mesh = selection.nodeName() ;
            lockedAttr = mesh[0].upper() + mesh[1:] + '_PavAmp' ;
            if lockedAttr in selectedAttr_list :
                selectedAttr_list.remove ( lockedAttr ) ;

            for selectedAttr in selectedAttr_list :
            
                if pm.attributeQuery ( selectedAttr , node = selectionShape , exists = True  ) :

                    exec ( 'target = selectionShape.{selectedAttr}'.format ( selectedAttr = selectedAttr ) ) ;
                    
                    mdv_list = target.listConnections ( ) ;
                    
                    # rename pav before deletion
                    for mdv in mdv_list :
                        pav_list = mdv.listConnections ( type = 'polyAverageVertex' ) ;
                        for pav in pav_list :
                            pav.rename ( 'polyAverageVertex1' ) ;
                    
                    pm.deleteAttr( '{selectionShape}.{selectedAttr}'.format ( selectionShape = selectionShape , selectedAttr = selectedAttr ) ) ;
                    pm.delete ( mdv_list ) ;

        self.pavTabLayout_refresh() ;

    def undo ( self , *args ) :
        pm.undo() ;
        self.pavTabLayout_refresh() ;

    def redo ( self , *args ) :
        pm.redo() ;
        self.pavTabLayout_refresh() ;

    def show ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/5*3 ) , ( 2 , w/5*2 ) ] ) :

                    self.pav_tabLayout =  pm.tabLayout ( w = w/5*3 , cc = self.pavTabLayout_changeCommand ) ;
                    with self.pav_tabLayout :
                        pass ;

                    with pm.rowColumnLayout ( nc = 1 , w = w/3 ) :

                        pm.button ( label = 'Load Target Polygons' , c = self.pavTabLayout_loadPolygon ) ;
                        pm.button ( label = 'Refresh' , c = self.pavTabLayout_refresh ) ;

                        pm.separator ( h = 10 , vis = False ) ;

                        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/5*2/2 ) , ( 2 , w/5*2/2 ) ] ) :
                            pm.button ( label = 'Undo' , c = self.undo ) ;
                            pm.button ( label = 'Redo' , c = self.redo ) ;

                        pm.button ( label = 'Delete Selected PAV' , c = self.deleteSelectedPav ) ;

                        pm.separator ( h = 20 , vis = True ) ;

                        pm.button ( label = 'Create PAV Attribute' , c = self.createPavAttr ) ;
                        pm.button ( label = 'Delete PAV Attribute' , c = self.deletePavAttr ) ;

                pass ;

        self.pavTabLayout_loadPolygon() ;
        window.show () ;

def run ( *args ) :
    gui = GUI() ;
    gui.show() ;