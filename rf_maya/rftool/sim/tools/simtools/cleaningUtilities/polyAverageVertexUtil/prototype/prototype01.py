import pymel.core as pm ;

import wip.cleanup_polyAverageVertexUtil.general as gen ;
reload ( gen ) ;

# selection = pm.ls ( sl = True ) ;

class PolyAverageVertex ( gen.NaturalSort ) :

    def __init__ ( self ) :
        pass ;

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def getPavNode ( self , target ) :
        # Pav = PolyAverageVertex
        
        target      = pm.general.PyNode ( target ) ;
        targetShape = target.getShape() ;
        pav_list    = targetShape.listConnections ( type = 'polyAverageVertex' ) ;

        pav_list = set ( pav_list ) ;
        pav_list = list ( pav_list ) ;
        pav_list = self.naturalSort ( pav_list ) ;

        return pav_list ;

class GUI ( PolyAverageVertex ) :

    def __init__ ( self ) :
        self.ui         = 'polyAverageVertexUtil_ui' ;
        self.width      = 500.00 ;
        self.title      = 'Poly Average Vertex Util' ;
        self.version    = 1.0 ;

    def __str__ ( self ) :
        return self.ui ;

    def __repr__ ( self ) :
        return self.ui ;

    def checkUniqueness ( self , ui ) :
        if pm.window ( ui , exists = True ) :
            pm.deleteUI ( ui ) ;
            self.checkUniqueness ( ui ) ;

    def pavTabLayout_refresh ( self , *args ) :

        update_list = pm.tabLayout ( self.pav_tabLayout , q = True , childArray = True ) ;

        if update_list :

            for update in update_list :
                target = update.split ( '_' ) [0] ;
                pav_list = self.getPavNode ( target = target ) ;

                # remove all pav from current textScrollList
                cmd = 'pm.textScrollList ( self.{target}_textScrollList , e = True , ra = True ) ;'.format ( target = target ) ;
                exec ( cmd ) ;

                for pav in pav_list :
                    cmd = 'pm.textScrollList ( self.{target}_textScrollList , e = True , append = pav ) ;'.format ( target = target ) ;
                    exec ( cmd ) ;

    def pavTabLayout_removeAll ( self , *args ) :

        remove_list = pm.tabLayout ( self.pav_tabLayout , q = True , childArray = True ) ;

        if remove_list :
            for each in remove_list :
                pm.deleteUI ( each ) ;

    def pavTabLayout_loadPolygon ( self , *args ) :

        self.pavTabLayout_removeAll() ;

        w = self.width ;
        
        selection_list = pm.ls ( sl = True ) ;
        selection_list = self.naturalSort ( selection_list ) ;
        
        for selection in selection_list :

            selectionName = selection.nodeName() ;
            
            cmd = "test = self.{selectionName}_rowColumnLayout = ".format ( selectionName = selectionName ) ;
            cmd += "pm.rowColumnLayout ( selectionName + '_rowColumnLayout' , nc = 1 , p = self.pav_tabLayout , w = w/3*2 ) ;" ;
            exec ( cmd ) ;
            print test ;

            cmd = "child = self.{selectionName}_rowColumnLayout".format ( selectionName = selectionName ) ;
            exec ( cmd ) ;
            print child ;
            with child :
                
                cmd = "self.{selectionName}_textScrollList = ".format ( selectionName = selectionName ) ;
                cmd += "pm.textScrollList (  '{selectionName}_textScrollList' , ams = True , w = w/3*2 , h = 300 )".format ( selectionName = selectionName ) ;
                exec ( cmd ) ;

                pav_list = self.getPavNode ( target = selection ) ;

                cmd = "textScrollList = self.{selectionName}_textScrollList".format ( selectionName = selectionName ) ;
                exec ( cmd ) ;

                for pav in pav_list :
                    pm.textScrollList ( textScrollList , e = True , append = pav ) ;

            pm.tabLayout ( self.pav_tabLayout , e = True , tabLabel = ( ( child ,  selectionName ) ) ) ;

    def pavTabLayout_changeCommand ( self , *args ) :
        self.pavTabLayout_deselectAll() ;
        self.pavTabLayout_refresh() ;

    def pavTabLayout_deselectAll ( self , *args ) :

        rowColumnLayout_list = pm.tabLayout ( self.pav_tabLayout , q = True , childArray = True ) ;
        
        if rowColumnLayout_list :
            for rowColumnLayout in rowColumnLayout_list :
                textScrollList = rowColumnLayout.split('_')[0] + '_textScrollList' ;

                cmd = 'textScrollList = self.{textScrollList} ;'.format ( textScrollList = textScrollList )  ;
                exec ( cmd ) ;

                pm.textScrollList ( textScrollList , e = True , deselectAll = True ) ;

    def pavTabLayout_querySelection ( self , *args ) :

        rowColumnLayout_list = pm.tabLayout ( self.pav_tabLayout , q = True , childArray = True ) ;
        
        if rowColumnLayout_list :
            for rowColumnLayout in rowColumnLayout_list :
                textScrollList = rowColumnLayout.split('_')[0] + '_textScrollList' ;

                cmd = 'textScrollList = self.{textScrollList} ;'.format ( textScrollList = textScrollList )  ;
                exec ( cmd ) ;

                selection_list = pm.textScrollList ( textScrollList , q = True , selectItem = True ) ;
                if selection_list :
                    return selection_list ;

                # except :
                #     pass ;

    def deleteSelectedPav ( self , *args ) :

        toDelete_list = self.pavTabLayout_querySelection() ;
        pm.delete ( toDelete_list ) ;
        self.pavTabLayout_refresh() ;

    def createPavAttr ( self , *args ) :
        # amp
        # create attr under shape
        pass ;

    def undo ( self , *args ) :
        pm.undo() ;
        self.pavTabLayout_refresh() ;

    def redo ( self , *args ) :
        pm.redo() ;
        self.pavTabLayout_refresh() ;

    def show ( self ) :

        w = self.width ;

        # check ui duplication
        self.checkUniqueness ( self.ui ) ;

        window = pm.window ( self.ui , 
            title       = '{title} v{version}'.format ( title = self.title , version = self.version ) ,
            mnb         = True  , # minimize button
            mxb         = False , # maximize button 
            sizeable    = True  ,
            rtf         = True  , # resizeToFitChildren
            ) ;

        # edit the window, so that the window size is refreshed every time it is called
        pm.window ( window , e = True , w = w , h = 10.00 ) ;
        with window :
    
            main_layout = pm.rowColumnLayout ( nc = 1 , w = w ) ;
            with main_layout :

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/3*2 ) , ( 2 , w/3 ) ] ) :

                    self.pav_tabLayout =  pm.tabLayout ( w = w/3*2  , cc = self.pavTabLayout_changeCommand ) ;
                    with self.pav_tabLayout :
                        pass ;

                    with pm.rowColumnLayout ( nc = 1 , w = w/3 ) :

                        pm.button ( label = 'Load Target Polygons' , c = self.pavTabLayout_loadPolygon ) ;
                        pm.button ( label = 'Refresh' , c = self.pavTabLayout_refresh ) ;

                        pm.separator ( h = 10 , vis = False ) ;

                        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w/3/2 ) , ( 2 , w/3/2 ) ] ) :
                            pm.button ( label = 'undo' , c = self.undo ) ;
                            pm.button ( label = 'redo' , c = self.redo ) ;

                        pm.button ( label = 'Delete Selected PAV' , c = self.deleteSelectedPav ) ;

                        pm.separator ( h = 10 , vis = False ) ;

                        pm.button ( label = 'Create PAV Attribute' ) ;

                pass ;

        self.pavTabLayout_loadPolygon() ;
        window.show () ;

def run ( *args ) :
    gui = GUI() ;
    gui.show() ;

'''
import wip.cleanup_polyAverageVertexUtil.prototype01 as prototype ;
reload ( prototype ) ;
prototype.run() ;
'''