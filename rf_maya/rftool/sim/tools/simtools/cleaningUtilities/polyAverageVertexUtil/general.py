import re ;

class NaturalSort ( ) :

    def __init__ ( self ) :
        pass ;

    def convertKeyElem ( self , input ) :

        if input.isdigit() :
            return int ( input ) ;
        else :
            return input.lower() ;

    def splitKey ( self , key ) :

        return_list = [] ;

        key = str ( key ) ;

        for elem in re.split ( '([0-9]+)' , key ) :
            return_list.append ( self.convertKeyElem ( elem ) ) ;

        return return_list ;

    def naturalSort ( self , list ) :

        return sorted ( list , key = self.splitKey ) ;