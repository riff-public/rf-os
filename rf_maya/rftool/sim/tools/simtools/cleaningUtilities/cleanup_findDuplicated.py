import pymel.core as pm ;

def findDuplicated ( *args ) :
    # select top node then run the script

    selection = pm.ls ( sl = True ) [0] ;
    
    node_list = selection.listRelatives ( type = 'transform' , ad = True ) ;
    
    duplicate_list = [] ;
    
    for node in node_list :
    	
    	try :
    		pm.general.PyNode ( node.nodeName() ) ;
    
    	except :
    		duplicate_list.append ( node.nodeName() ) ;
    
    duplicate_list = set ( duplicate_list ) ;
    duplicate_list = list ( duplicate_list ) ;
    
    return duplicate_list ;

def run ( *args ) :
    print findDuplicated() ;