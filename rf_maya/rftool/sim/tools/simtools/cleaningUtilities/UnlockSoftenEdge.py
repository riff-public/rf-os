import maya.cmds as mc

def main(*arg):
    sel = mc.ls(sl = True)
    mc.polyNormalPerVertex(ufn=True)
    for i in range(len(sel)):
        print sel[i]
        mc.select( sel[i] )
        mc.polySoftEdge(a=180 , ch=1 )
    mc.select( cl=True )
#main()