import pymel.core as pm ;

def deleteBlendshape ( topNode ) :
    # select ABC:Geo_Grp then run this script
    
    blendShape_list = [] ;

    topNode = pm.general.PyNode ( topNode ) ;

    children_list = topNode.listRelatives ( ad = True , type = 'mesh' ) ;

    for child in children_list :
        
        outBlendShape_list = child.listConnections ( type = 'blendShape' ) ;

        for outBlendShape in outBlendShape_list :

            if outBlendShape not in blendShape_list :
                blendShape_list.append ( outBlendShape ) ;

    pm.delete ( blendShape_list ) ;

def run ( *args ) :
    selection = pm.ls ( sl = True ) [0] ;
    deleteBlendshape ( topNode = selection ) ;