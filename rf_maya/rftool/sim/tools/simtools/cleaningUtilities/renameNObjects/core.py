import pymel.core as pm ;
import re ;

class General ( object ) :

    def __init__ ( self ) :
        pass ;

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def removeNullFromList ( self , list ) :
        for item in list :
            if not item :
                list.remove ( item ) ;
        return list ;

    def capitalise ( self , input ) :
        input = str ( input ) ;
        input = input[0].upper() + input[1:] ;
        return input ;

class RenameNConstraint ( object ) :
    
    def __init__ ( self ) :
        pass ;

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def checkIncrement ( self , name , incrementIncrement = True , *args ) :

        allItemShape_list = pm.ls ( '{name}*'.format ( name = name ) , type = 'dynamicConstraint' ) ;
        allItem_list    = [] ;
        validItem_list  = [] ;

        for itemShape in allItemShape_list :
            allItem_list.append ( itemShape.getTransform() ) ;

        for item in allItem_list :
            if re.findall ( name + '[0-9]{2}' , str(item) ) :
                if item.count ( name ) == 1 :
                    validItem_list.append ( item ) ;

        validItem_list.sort() ;
            
        if not validItem_list :
            
            finalIncrement = '01' ;

        else :
            
            currentIncrement = validItem_list[-1] ;

            for elem in [ name , '_Dyc' ] :

                currentIncrement = currentIncrement.split ( elem ) ;

                for split in currentIncrement :
                    if not split :
                        currentIncrement.remove ( split ) ;

                currentIncrement = ( currentIncrement[0] ) ;

            if not currentIncrement.isdigit() :
                finalIncrement = '01' ;

            else :
                if incrementIncrement :
                    currentIncrement = int ( currentIncrement ) ;
                    finalIncrement = str ( currentIncrement + 1 ) ;
                    finalIncrement = finalIncrement.zfill(2) ;

        return ( finalIncrement ) ;

    def composeName ( self , names_list ) :

        composedName = '' ;
        nameSplits_list = [] ;

        for name in names_list :

            name = pm.general.PyNode ( name ) ;
            name = name.nodeName() ;
            nameSplits_list.append ( name.split ( '_' ) [0] ) ;

        # composedName += nameSplits_list[0][:1].capitalize() + nameSplits_list[0][1:]; 
        composedName += nameSplits_list[0];

        if len ( nameSplits_list ) > 1 :
            for name in nameSplits_list [1:] :
                # composedName += '_' + name[0][:1].capitalize() + name[1:];
                composedName += '_' + name;

        index = self.checkIncrement ( name = composedName ) ;

        composedName += index ;
        composedName += '_Dyc' ;

        return composedName ;

    def getNObjects ( self , target ) :
        # for Point Constraint, return [ driver , driven ]
        # [nt.Transform(u'Body_Geo_COL_nRigid'), nt.Transform(u'Shirts_DYN_nCloth')]

        target = pm.general.PyNode ( target ) ;
        targetShape = target.getShape() ;
        return_list = [] ;
        
        nComponents_list = targetShape.listConnections( type = 'nComponent' ) ;
        nComponents_list = set ( nComponents_list ) ;
        nComponents_list = list ( nComponents_list ) ;

        nComponents_list = set ( nComponents_list ) ;
        nComponents_list = list ( nComponents_list ) ;

        if len ( nComponents_list ) == 1 :
            
            nComponent = nComponents_list[0] ;
            
            connections_list = nComponent.listConnections() ;

            if target in connections_list :
                connections_list.remove ( target ) ;
            
            for connection in connections_list :
                if not connection == target :
                    return_list.append ( connection ) ;

        elif len > 1 :

            driver = None ;
            driven = None ;

            for nComponent in nComponents_list :
                connections_list = nComponent.listConnections() ;
                
                if target in connections_list :
                    connections_list.remove ( target ) ;

                connection = connections_list[0] ;

                if nComponent.elements.get() == 0 :
                    driver = connection ;
                
                elif nComponent.elements.get() == 2 :
                    driven = connection ;

            if ( not driver ) or ( not driven ) :

                for nComponent in nComponents_list :
                    connections_list = nComponent.listConnections() ;
                    
                    if target in connections_list :
                        connections_list.remove ( target ) ;

                    connection = connections_list[0] ;

                    if nComponent.componentType.get() == 2 :
                        driver = connection ;
                    elif nComponent.componentType.get() == 6 :
                        driven = connection ;

            return_list.append ( driver ) ;
            return_list.append ( driven ) ;

        return return_list ;

    def rename ( self , target ) :

        names_list = self.getNObjects ( target = target ) ;
        constraintName = self.composeName ( names_list = names_list ) ;

        constraintName = self.composeName ( names_list = self.getNObjects ( target = target ) ) ;         
        target.rename ( constraintName ) ;

class RenameNObjects ( General ) :

    def __init__ ( self ) :
        pass ;

    def __str__ ( self ) :
        pass ;

    def __repr__ ( self ) :
        pass ;

    def renameNGeneral ( self , target ) :
        # rename nCloth, nRigid
            
        targetShape = target.getShape() ;
        
        nodeType = targetShape.nodeType() ;
        nodeType = self.capitalise ( nodeType ) ;
        
        mesh = targetShape.listConnections ( type = 'mesh' ) ;
        mesh = set ( mesh ) ;
        mesh = list ( mesh ) ;
        
        if len ( mesh ) != 1 :
            pm.error ( 'please check script, more than 1 mesh detected in nObject' ) ;
        
        else :
            mesh = mesh[0] ;
            if ':' in mesh :
                geoName = mesh.split(':')[1] ;
            else:
                geoName = mesh ;
            nameSplit = geoName.split ( '_' ) ;
            nameSplit = self.removeNullFromList ( nameSplit ) ;
            name = nameSplit[0] ;       
            name = self.capitalise ( name ) ;    
            name += '_' + nodeType ;
            
            target.rename ( name ) ;

    def renameNConstraint ( self , target ) :
        rnc = RenameNConstraint() ;
        rnc.rename ( target = target ) ;

    def renameAll ( self , *args ) :

        selection_list = pm.ls ( sl = True ) ;
        
        nCloth_list     = [] ;
        nRigid_list     = [] ;
        dynamicConstraint_list = [] ;

        for selection in selection_list :
            selectionShape = selection.getShape() ;
            selectionType = selectionShape.nodeType() ;

            for type in [ 'nCloth' , 'nRigid' , 'dynamicConstraint' ] :

                cmd = '''
if selectionType == '{type}' :
    {type}_list.append ( selection ) ;
                '''.format ( type = type ) ; 
        
                exec ( cmd ) ;

        nGeneral_list = [] ;
        nGeneral_list.extend ( nCloth_list ) ;
        nGeneral_list.extend ( nRigid_list ) ;

        for each in nGeneral_list :
            self.renameNGeneral ( target = each ) ;

        for each in dynamicConstraint_list :
            self.renameNConstraint ( target = each ) ;

def run ( *args ) :
    rno = RenameNObjects() ;
    rno.renameAll() ;