import maya.cmds as mc

def main(*arg):
	sel = mc.ls(sl = True)
	for each in sel:
		shp = mc.listRelatives(each)
		PAV = mc.listConnections(shp, t = 'polyAverageVertex')
		mc.delete(PAV)