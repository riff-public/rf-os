import maya.cmds as mc ;

import maya.mel as mm ;

from rf_maya.rftool.sim.tools.simtools.track.absoluteTrack import general as abtGl 
reload ( abtGl ) 

def activePanelShowQuery () :
    
    mayaVersion = mm.eval ( 'getApplicationVersionAsFloat' ) ;

    activePanel = mc.getPanel ( wf = True ) ;

    if 'modelPanel' not in activePanel :
        activePanel = 'modelPanel4' ;

    showList = [] ;

    if mayaVersion < 2013.0 :
        
        time = abtGl.systemTime () ;
        
        mc.error ( '( %s ) activePanelShowQuery does not support Maya version prior to 2013.0 at this time, please update script' % time ) ;
    
    elif mayaVersion > 2015.0 :
        
        time = abtGl.systemTime () ;
        
        mc.error ( '( %s ) activePanelShowQuery does not support Maya version beyond 2014.0 at this time, please update script' % time ) ;
    
    if mayaVersion >= 2013.0 and mayaVersion < 2014.0 :
        
        nurbCurves = mc.modelEditor ( activePanel , q = True , nurbsCurves = True ) ;
        nurbsSurfaces = mc.modelEditor ( activePanel , q = True , nurbsSurfaces = True ) ;
        polymeshes = mc.modelEditor ( activePanel , q = True , polymeshes = True ) ;
        subdivSurfaces = mc.modelEditor ( activePanel , q = True , subdivSurfaces = True ) ;
        planes = mc.modelEditor ( activePanel , q = True , planes = True ) ;
        lights = mc.modelEditor ( activePanel , q = True , lights = True ) ;
        cameras = mc.modelEditor ( activePanel , q = True , cameras = True ) ;
        joints = mc.modelEditor ( activePanel , q = True , joints = True ) ;
        ikHandles = mc.modelEditor ( activePanel , q = True , ikHandles = True ) ;
        deformers = mc.modelEditor ( activePanel , q = True , deformers = True ) ;
        dynamics = mc.modelEditor ( activePanel , q = True , dynamics = True ) ;
        fluids = mc.modelEditor ( activePanel , q = True , fluids = True ) ;
        hairSystems = mc.modelEditor ( activePanel , q = True , hairSystems = True ) ;
        follicles = mc.modelEditor ( activePanel , q = True , follicles = True ) ;
        nCloths = mc.modelEditor ( activePanel , q = True , nCloths = True ) ;
        nParticles = mc.modelEditor ( activePanel , q = True , nParticles = True ) ;
        nRigids = mc.modelEditor ( activePanel , q = True , nRigids = True ) ;
        dynamicConstraints = mc.modelEditor ( activePanel , q = True , dynamicConstraints = True ) ;
        locators = mc.modelEditor ( activePanel , q = True , locators = True ) ;
        dimensions = mc.modelEditor ( activePanel , q = True , dimensions = True ) ;
        pivots = mc.modelEditor ( activePanel , q = True , pivots = True ) ;
        handles = mc.modelEditor ( activePanel , q = True , handles = True ) ;
        textures = mc.modelEditor ( activePanel , q = True , textures = True ) ;
        strokes = mc.modelEditor ( activePanel , q = True , strokes = True ) ;
        motionTrails = mc.modelEditor ( activePanel , q = True , motionTrails = True ) ;
        # manipulators = mc.modelEditor ( activePanel , q = True , manipulators = True ) ;
        clipGhosts = mc.modelEditor ( activePanel , q = True , clipGhosts = True ) ;
        
        showList = ( nurbCurves , nurbsSurfaces , polymeshes , subdivSurfaces , planes , 
            lights , cameras , joints , ikHandles , deformers , dynamics , fluids ,
            hairSystems , follicles , nCloths , nParticles , nRigids , dynamicConstraints ,
            locators , dimensions , pivots , handles , textures , strokes , motionTrails , clipGhosts ) ;

    if mayaVersion >= 2014.0 and mayaVersion < 2015.0 :
        
        nurbsCurves = mc.modelEditor ( activePanel , q = True , nurbsCurves = True ) ;
        nurbsSurfaces = mc.modelEditor ( activePanel , q = True , nurbsSurfaces = True ) ;
        cv = mc.modelEditor ( activePanel , q = True , cv = True ) ;
        hulls = mc.modelEditor ( activePanel , q = True , hulls = True ) ;
        polymeshes = mc.modelEditor ( activePanel , q = True , polymeshes = True ) ;
        subdivSurfaces = mc.modelEditor ( activePanel , q = True , subdivSurfaces = True ) ;
        planes = mc.modelEditor ( activePanel , q = True , planes = True ) ;
        lights = mc.modelEditor ( activePanel , q = True , lights = True ) ;
        cameras = mc.modelEditor ( activePanel , q = True , cameras = True ) ;
        imagePlane = mc.modelEditor ( activePanel , q = True , imagePlane = True ) ;
        joints = mc.modelEditor ( activePanel , q = True , joints = True ) ;
        ikHandles = mc.modelEditor ( activePanel , q = True , ikHandles = True ) ;
        deformers = mc.modelEditor ( activePanel , q = True , deformers = True ) ;
        dynamics = mc.modelEditor ( activePanel , q = True , dynamics = True ) ;
        fluids = mc.modelEditor ( activePanel , q = True , fluids = True ) ;
        hairSystems = mc.modelEditor ( activePanel , q = True , hairSystems = True ) ;
        follicles = mc.modelEditor ( activePanel , q = True , follicles = True ) ;
        nCloths = mc.modelEditor ( activePanel , q = True , nCloths = True ) ;
        nParticles = mc.modelEditor ( activePanel , q = True , nParticles = True ) ;
        nRigids = mc.modelEditor ( activePanel , q = True , nRigids = True ) ;
        dynamicConstraints = mc.modelEditor ( activePanel , q = True , dynamicConstraints = True ) ;
        locators = mc.modelEditor ( activePanel , q = True , locators = True ) ;
        dimensions = mc.modelEditor ( activePanel , q = True , dimensions = True ) ;
        pivots = mc.modelEditor ( activePanel , q = True , pivots = True ) ;
        handles = mc.modelEditor ( activePanel , q = True , handles = True ) ;
        textures = mc.modelEditor ( activePanel , q = True , textures = True ) ; 
        strokes = mc.modelEditor ( activePanel , q = True , strokes = True ) ; 
        motionTrails = mc.modelEditor ( activePanel , q = True , motionTrails = True ) ; 
        pluginShapes = mc.modelEditor ( activePanel , q = True , pluginShapes = True ) ; 
        clipGhosts = mc.modelEditor ( activePanel , q = True , clipGhosts = True ) ; 
        greasePencils = mc.modelEditor ( activePanel , q = True , greasePencils = True ) ; 
        gpuCacheDisplayFilter = mc.modelEditor ( activePanel , q = True , queryPluginObjects = 'gpuCacheDisplayFilter' ) ; 
        
        showList = ( nurbsCurves , nurbsSurfaces , cv , hulls , polymeshes ,
            subdivSurfaces , planes , lights , cameras , imagePlane , 
            joints , ikHandles , deformers , dynamics , fluids , 
            hairSystems , follicles , nCloths , nParticles , nRigids ,
            dynamicConstraints , locators , dimensions , pivots , handles ,
            textures , strokes , motionTrails , pluginShapes , clipGhosts ,
            greasePencils , gpuCacheDisplayFilter ) ;
            
        return showList ;
    
    if mayaVersion >= 2015.0 and mayaVersion < 2016.0 :
        
        nurbsCurves = mc.modelEditor ( activePanel , q = True , nurbsCurves = True ) ;
        nurbsSurfaces = mc.modelEditor ( activePanel , q = True , nurbsSurfaces = True ) ;
        cv = mc.modelEditor ( activePanel , q = True , cv = True ) ;
        hulls = mc.modelEditor ( activePanel , q = True , hulls = True ) ;
        polymeshes = mc.modelEditor ( activePanel , q = True , polymeshes = True ) ;
        subdivSurfaces = mc.modelEditor ( activePanel , q = True , subdivSurfaces = True ) ;
        planes = mc.modelEditor ( activePanel , q = True , planes = True ) ;
        lights = mc.modelEditor ( activePanel , q = True , lights = True ) ;
        cameras = mc.modelEditor ( activePanel , q = True , cameras = True ) ;
        imagePlane = mc.modelEditor ( activePanel , q = True , imagePlane = True ) ;
        
        joints = mc.modelEditor ( activePanel , q = True , joints = True ) ;
        ikHandles = mc.modelEditor ( activePanel , q = True , ikHandles = True ) ;
        deformers = mc.modelEditor ( activePanel , q = True , deformers = True ) ;
        dynamics = mc.modelEditor ( activePanel , q = True , dynamics = True ) ;
        particleInstancers = mc.modelEditor ( activePanel , q = True , particleInstancers = True ) ;
        fluids = mc.modelEditor ( activePanel , q = True , fluids = True ) ;
        hairSystems = mc.modelEditor ( activePanel , q = True , hairSystems = True ) ;
        follicles = mc.modelEditor ( activePanel , q = True , follicles = True ) ;
        nCloths = mc.modelEditor ( activePanel , q = True , nCloths = True ) ;
        nParticles = mc.modelEditor ( activePanel , q = True , nParticles = True ) ;
        
        nRigids = mc.modelEditor ( activePanel , q = True , nRigids = True ) ;
        dynamicConstraints = mc.modelEditor ( activePanel , q = True , dynamicConstraints = True ) ;
        locators = mc.modelEditor ( activePanel , q = True , locators = True ) ;
        dimensions = mc.modelEditor ( activePanel , q = True , dimensions = True ) ;
        pivots = mc.modelEditor ( activePanel , q = True , pivots = True ) ;
        handles = mc.modelEditor ( activePanel , q = True , handles = True ) ;
        textures = mc.modelEditor ( activePanel , q = True , textures = True ) ;
        strokes = mc.modelEditor ( activePanel , q = True , strokes = True ) ;
        motionTrails = mc.modelEditor ( activePanel , q = True , motionTrails = True ) ;
        pluginShapes = mc.modelEditor ( activePanel , q = True , pluginShapes = True ) ;
        
        clipGhosts = mc.modelEditor ( activePanel , q = True , clipGhosts = True ) ;
        greasePencils = mc.modelEditor ( activePanel , q = True , greasePencils = True ) ;
        gpuCacheDisplayFilter = mc.modelEditor ( activePanel , q = True , queryPluginObjects = 'gpuCacheDisplayFilter' ) ; 
        
        showList = ( nurbsCurves , nurbsSurfaces , cv , hulls , polymeshes ,
            subdivSurfaces , planes , lights , cameras , imagePlane ,
            joints , ikHandles , deformers , dynamics , particleInstancers , 
            fluids , hairSystems , follicles , nCloths , nParticles ,
            nRigids , dynamicConstraints , locators , dimensions , pivots ,
            handles , textures , strokes , motionTrails , pluginShapes ,
            clipGhosts , greasePencils , gpuCacheDisplayFilter ) ;
            
        return showList ;
    
def activePanelShowSet ( showList = [] ) :
    
    mayaVersion = mm.eval ( 'getApplicationVersionAsFloat' ) ;
        
    activePanel = mc.getPanel ( wf = True ) ;

    if 'modelPanel' not in activePanel :
        activePanel = 'modelPanel4' ;

    if mayaVersion < 2013.0 :
        
        time = abtGl.systemTime () ;
        
        mc.error ( '( %s ) activePanelShowQuery does not support Maya version prior to 2013.0 at this time, please update script' % time ) ;
    
    elif mayaVersion > 2015.0 :
        
        time = abtGl.systemTime () ;
        
        mc.error ( '( %s ) activePanelShowQuery does not support Maya version beyond 2014.0 at this time, please update script' % time ) ;
    
    if mayaVersion >= 2013.0 and mayaVersion < 2014.0 :
        
        nurbCurves = mc.modelEditor ( activePanel , e = True , nurbsCurves = showList [0] ) ;
        nurbsSurfaces = mc.modelEditor ( activePanel , e = True , nurbsSurfaces = showList [1] ) ;
        polymeshes = mc.modelEditor ( activePanel , e = True , polymeshes = showList [2] ) ;
        subdivSurfaces = mc.modelEditor ( activePanel , e = True , subdivSurfaces = showList [3] ) ;
        planes = mc.modelEditor ( activePanel , e = True , planes = showList [4] ) ;
        lights = mc.modelEditor ( activePanel , e = True , lights = showList [5] ) ;
        cameras = mc.modelEditor ( activePanel , e = True , cameras = showList [6] ) ;
        joints = mc.modelEditor ( activePanel , e = True , joints = showList [7] ) ;
        ikHandles = mc.modelEditor ( activePanel , e = True , ikHandles = showList [8] ) ;
        deformers = mc.modelEditor ( activePanel , e = True , deformers = showList [9] ) ;
        dynamics = mc.modelEditor ( activePanel , e = True , dynamics = showList [10] ) ;
        fluids = mc.modelEditor ( activePanel , e = True , fluids = showList [11] ) ;
        hairSystems = mc.modelEditor ( activePanel , e = True , hairSystems = showList [12] ) ;
        follicles = mc.modelEditor ( activePanel , e = True , follicles = showList [13] ) ;
        nCloths = mc.modelEditor ( activePanel , e = True , nCloths = showList [14] ) ;
        nParticles = mc.modelEditor ( activePanel , e = True , nParticles = showList [15] ) ;
        nRigids = mc.modelEditor ( activePanel , e = True , nRigids = showList [16] ) ;
        dynamicConstraints = mc.modelEditor ( activePanel , e = True , dynamicConstraints = showList [17] ) ;
        locators = mc.modelEditor ( activePanel , e = True , locators = showList [18] ) ;
        dimensions = mc.modelEditor ( activePanel , e = True , dimensions = showList [19] ) ;
        pivots = mc.modelEditor ( activePanel , e = True , pivots = showList [20] ) ;
        handles = mc.modelEditor ( activePanel , e = True , handles = showList [21] ) ;
        textures = mc.modelEditor ( activePanel , e = True , textures = showList [22] ) ;
        strokes = mc.modelEditor ( activePanel , e = True , strokes = showList [23] ) ;
        motionTrails = mc.modelEditor ( activePanel , e = True , motionTrails = showList [24] ) ;
        clipGhosts = mc.modelEditor ( activePanel , e = True , clipGhosts = showList [25] ) ;

    if mayaVersion >= 2014.0 and mayaVersion < 2015.0 :
        
        nurbsCurves = mc.modelEditor ( activePanel , e = True , nurbsCurves = showList [0] ) ;
        nurbsSurfaces = mc.modelEditor ( activePanel , e = True , nurbsSurfaces = showList [1] ) ;
        cv = mc.modelEditor ( activePanel , e = True , cv = showList [2] ) ;
        hulls = mc.modelEditor ( activePanel , e = True , hulls = showList [3] ) ;
        polymeshes = mc.modelEditor ( activePanel , e = True , polymeshes = showList [4] ) ;
        subdivSurfaces = mc.modelEditor ( activePanel , e = True , subdivSurfaces = showList [5] ) ;
        planes = mc.modelEditor ( activePanel , e = True , planes = showList [6] ) ;
        lights = mc.modelEditor ( activePanel , e = True , lights = showList [7] ) ;
        cameras = mc.modelEditor ( activePanel , e = True , cameras = showList [8] ) ;
        imagePlane = mc.modelEditor ( activePanel , e = True , imagePlane = showList [9] ) ;
        joints = mc.modelEditor ( activePanel , e = True , joints = showList [10] ) ;
        ikHandles = mc.modelEditor ( activePanel , e = True , ikHandles = showList [11] ) ;
        deformers = mc.modelEditor ( activePanel , e = True , deformers = showList [12] ) ;
        dynamics = mc.modelEditor ( activePanel , e = True , dynamics = showList [13] ) ;
        fluids = mc.modelEditor ( activePanel , e = True , fluids = showList [14] ) ;
        hairSystems = mc.modelEditor ( activePanel , e = True , hairSystems = showList [15] ) ;
        follicles = mc.modelEditor ( activePanel , e = True , follicles = showList [16] ) ;
        nCloths = mc.modelEditor ( activePanel , e = True , nCloths = showList [17] ) ;
        nParticles = mc.modelEditor ( activePanel , e = True , nParticles = showList [18] ) ;
        nRigids = mc.modelEditor ( activePanel , e = True , nRigids = showList [19] ) ;
        dynamicConstraints = mc.modelEditor ( activePanel , e = True , dynamicConstraints = showList [20] ) ;
        locators = mc.modelEditor ( activePanel , e = True , locators = showList [21] ) ;
        dimensions = mc.modelEditor ( activePanel , e = True , dimensions = showList [22] ) ;
        pivots = mc.modelEditor ( activePanel , e = True , pivots = showList [23] ) ;
        handles = mc.modelEditor ( activePanel , e = True , handles = showList [24] ) ;
        textures = mc.modelEditor ( activePanel , e = True , textures = showList [25] ) ;
        strokes = mc.modelEditor ( activePanel , e = True , strokes = showList [26] ) ;
        motionTrails = mc.modelEditor ( activePanel , e = True , motionTrails = showList [27] ) ;
        pluginShapes = mc.modelEditor ( activePanel , e = True , pluginShapes = showList [28] ) ;
        clipGhosts = mc.modelEditor ( activePanel , e = True , clipGhosts = showList [29] ) ;
        greasePencils = mc.modelEditor ( activePanel , e = True , greasePencils = showList [30] ) ;
        gpuCacheDisplayFilter = mc.modelEditor ( activePanel , e = True , pluginObjects = [ 'gpuCacheDisplayFilter' , showList [31] ] ) ;

    if mayaVersion >= 2015.0 and mayaVersion < 2016.0 :
        
        nurbsCurves = mc.modelEditor ( activePanel , e = True , nurbsCurves = showList [0] ) ;  
        nurbsSurfaces = mc.modelEditor ( activePanel , e = True , nurbsSurfaces = showList [1] ) ;  
        cv = mc.modelEditor ( activePanel , e = True , cv = showList [2] ) ;  
        hulls = mc.modelEditor ( activePanel , e = True , hulls = showList [3] ) ;  
        polymeshes = mc.modelEditor ( activePanel , e = True , polymeshes = showList [4] ) ;  
        subdivSurfaces = mc.modelEditor ( activePanel , e = True , subdivSurfaces = showList [5] ) ;  
        planes = mc.modelEditor ( activePanel , e = True , planes = showList [6] ) ;  
        lights = mc.modelEditor ( activePanel , e = True , lights = showList [7] ) ;  
        cameras = mc.modelEditor ( activePanel , e = True , cameras = showList [8] ) ; 
        imagePlane = mc.modelEditor ( activePanel , e = True , imagePlane = showList [9] ) ; 
        
        joints = mc.modelEditor ( activePanel , e = True , joints = showList [10] ) ; 
        ikHandles = mc.modelEditor ( activePanel , e = True , ikHandles = showList [11] ) ; 
        deformers = mc.modelEditor ( activePanel , e = True , deformers = showList [12] ) ; 
        dynamics = mc.modelEditor ( activePanel , e = True , dynamics = showList [13] ) ; 
        particleInstancers = mc.modelEditor ( activePanel , e = True , particleInstancers = showList [14] ) ; 
        fluids = mc.modelEditor ( activePanel , e = True , fluids = showList [15] ) ; 
        hairSystems = mc.modelEditor ( activePanel , e = True , hairSystems = showList [16] ) ; 
        follicles = mc.modelEditor ( activePanel , e = True , follicles = showList [17] ) ; 
        nCloths = mc.modelEditor ( activePanel , e = True , nCloths = showList [18] ) ; 
        nParticles = mc.modelEditor ( activePanel , e = True , nParticles = showList [19] ) ; 
        
        nRigids = mc.modelEditor ( activePanel , e = True , nRigids = showList [20] ) ; 
        dynamicConstraints = mc.modelEditor ( activePanel , e = True , dynamicConstraints = showList [21] ) ; 
        locators = mc.modelEditor ( activePanel , e = True , locators = showList [22] ) ; 
        dimensions = mc.modelEditor ( activePanel , e = True , dimensions = showList [23] ) ; 
        pivots = mc.modelEditor ( activePanel , e = True , pivots = showList [24] ) ; 
        handles = mc.modelEditor ( activePanel , e = True , handles = showList [25] ) ; 
        textures = mc.modelEditor ( activePanel , e = True , textures = showList [26] ) ; 
        strokes = mc.modelEditor ( activePanel , e = True , strokes = showList [27] ) ; 
        motionTrails = mc.modelEditor ( activePanel , e = True , motionTrails = showList [28] ) ; 
        pluginShapes = mc.modelEditor ( activePanel , e = True , pluginShapes = showList [29] ) ; 
        
        clipGhosts = mc.modelEditor ( activePanel , e = True , clipGhosts = showList [30] ) ; 
        greasePencils = mc.modelEditor ( activePanel , e = True , greasePencils = showList [31] ) ; 
        pluginObjects = mc.modelEditor ( activePanel , e = True , pluginObjects = [ 'gpuCacheDisplayFilter' , showList [32] ] ) ;
        
def activePanelAllDisplay ( switch = True ) :

    if switch == True :

        activePanel = mc.getPanel ( wf = True ) ;

        if 'modelPanel' not in activePanel :
            activePanel = 'modelPanel4' ;
        
        mc.modelEditor ( activePanel , e = True , allObjects = True ) ;
    
    elif switch == False :

        activePanel = mc.getPanel ( wf = True ) ;

        if activePanel != 'modelPanel1' or activePanel != 'modelPanel2' or activePanel != 'modelPanel3' or activePanel != 'modelPanel4' :

            activePanel = 'modelPanel4' ;

        mc.modelEditor ( activePanel , e = True , allObjects = False ) ;
        
    else :
        
        time = abtGl.systemTime () ;
        
        mc.error ( '( %s ) invalid input' % time ) ; 
