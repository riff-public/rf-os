import maya.cmds as mc ;
import sys ;
import os ;

def we_are_frozen():
    # All of the modules are built-in to the interpreter, e.g., by py2exe
    return hasattr(sys, "frozen")

def module_path():
    encoding = sys.getfilesystemencoding()
    if we_are_frozen():
        return os.path.dirname(unicode(sys.executable, encoding))
    return os.path.dirname(unicode(__file__, encoding))

from datetime import datetime ;

def systemTime () :
    rawTime = str ( datetime.time ( datetime.now () ) ) ;
    rawTime2 = rawTime.split ( ':' ) ;
    rawTime3 = rawTime2[2].split( '.' ) ;
    
    hour = rawTime2 [0] ;
    minute = rawTime2 [1] ;
    second = rawTime3 [0] ;
    
    return '%s:%s:%s' % ( hour , minute , second ) ;


def lockAttr ( obj = '' , t = True , r = True , s = True , v = True , hide = False , attr = None ) :

    if hide == True :
        
        hide = False ;
        
    elif hide == False :
        
        hide = True ;

    if attr == None :

        ###
    
        axes = [ 'x' , 'y' , 'z' ] ;

        if t == True :

            for i in range ( 0 , 3 ) :

                mc.setAttr ( '%s.t%s' % ( obj , axes [i] ) , lock = True , keyable = hide , channelBox = hide ) ; 

        if r == True :
        
            for i in range ( 0 , 3 ) :

                mc.setAttr ( '%s.r%s' % ( obj , axes [i] ) , lock = True , keyable = hide , channelBox = hide ) ; 

        if s == True :

            for i in range ( 0 , 3 ) :

                mc.setAttr ( '%s.s%s' % ( obj , axes [i] ) , lock = True , keyable = hide , channelBox = hide ) ; 

        if v == True :

            mc.setAttr ( '%s.v' % obj , lock = True , keyable = hide , channelBox = hide ) ;

    else :

        if len ( attr ) == 1 :

            mc.setAttr ( '%s.%s' % ( obj , attr ) , lock = True , keyable = hide , channelBox = hide ) ;

        else :

            for each in attr :

                mc.setAttr ( '%s.%s' % ( obj , each ) , lock = True , keyable = hide , channelBox = hide ) ;

def createNrbFol ( nurb = '' , u = 0.5 , v = 0.5 , name = '' ) :
    # obsolete
    
    if nurb [ -4 : ] != '_nrb' :
        mc.error ( "This function requires '_nrb' as nurbsSurface suffix." ) ;
    
    if mc.nodeType ( nurb ) == 'transform' :
        nurb = mc.listRelatives ( nurb , s = True ) [0] ;
        
    
    elif mc.nodeType ( nurb ) == 'nurbsSurface' :
        pass
        
    else :
        mc.error ( 'Selected object needs to be nurbsSurface or transform with nurbsSurface' ) ;
    
    nm = nurb.split ( '_nrb' ) [0] ;

    fol = mc.createNode ( 'follicle' ) ;
    
    if name == '' :
        
        folParent = mc.rename ( mc.listRelatives ( fol , p = True ) [0] , '%sPos_flc' % nm ) ; 
        fol = mc.listRelatives ( folParent , c = True ) [0] ;
    
    else :

        folParent = mc.rename ( mc.listRelatives ( fol , p = True ) [0] , name ) ; 
        fol = mc.listRelatives ( folParent , c = True ) [0] ;
    
    mc.connectAttr ( '%s.local' % nurb , '%s.inputSurface' % fol ) ;
    mc.connectAttr ( '%s.worldMatrix[0]' % nurb , '%s.inputWorldMatrix' % fol ) ;
    mc.connectAttr ( '%s.outRotate' % fol , '%s.rotate' % folParent ) ;
    mc.connectAttr ( '%s.outTranslate' % fol , '%s.translate' % folParent ) ;
    
    mc.setAttr ( '%s.parameterU' % fol , u ) ;
    mc.setAttr ( '%s.parameterV' % fol , v ) ;
    lockAttr ( folParent , s = False , v = False ) ;

    return folParent ;

def createRivetTfm ( nurb = '' , u = 0.5 , v = 0.5 , name = '' , aim = 'y' , up = 'z' ) :

    if aim == 'x' :
        aim = ( 1 , 0 , 0 ) ;
    elif aim == 'y' :
        aim = ( 0 , 1 , 0 ) ;
    elif aim == 'z' :
        aim = ( 0 , 0 , 1 ) ;
        
    if up == 'x' :
        up = ( 1 , 0 , 0 ) ;
    elif up == 'y' :
        up = ( 0 , 1 , 0 ) ;
    elif up == 'x' :
        up = ( 0 , 0 , 1 ) ;

    if name == '' :
        name = nurb ;

    if '_nrb' in name :
        
        name = name.split ( '_nrb' ) [0] ; 

    aimCon = mc.createNode ( 'aimConstraint' , n = '%s_aimCon' % name ) ;
    psi = mc.createNode ( 'pointOnSurfaceInfo' , n = '%s_psi' % name ) ;
    grp = mc.group ( n = '%s_grp' % name , em = True ) ;

    mc.parent ( aimCon , grp ) ;

    mc.setAttr ( '%s.parameterU' % psi , u ) ;
    mc.setAttr ( '%s.parameterV' % psi , v ) ;
    mc.setAttr ( '%s.turnOnPercentage' % psi , 1 ) ;
    mc.setAttr ( '%s.aimVector' % aimCon , aim[0] , aim[1] , aim[2] ) ;
    mc.setAttr ( '%s.upVector' % aimCon , up[0] , up[1] , up[2] ) ;

    mc.connectAttr ( '%s.worldSpace[0]' % nurb , '%s.inputSurface' % psi ) ;
    mc.connectAttr ( '%s.position' % psi , '%s.t' % grp ) ;
    mc.connectAttr ( '%s.tangentU' % psi , '%s.target[0].targetTranslate' % aimCon ) ;
    mc.connectAttr ( '%s.normal' % psi , '%s.worldUpVector' % aimCon ) ;
    mc.connectAttr ( '%s.constraintRotate' % aimCon , '%s.r' % grp ) ;
    
    return grp ;

def copyGrp ( oriGrp = '' , newGrpNm = '' , parent = '' ) :

    pos = mc.xform ( oriGrp , q = True , ws = True , rp = True ) ;
    ro = mc.xform ( oriGrp , q = True , ws = True , ro = True ) ;
    roo = mc.xform ( oriGrp , q = True , ws = True , roo = True ) ;
    
    newGrp = mc.group ( name = newGrpNm , em = True ) ;
    mc.xform ( newGrp , ws = True , t = pos , ro = ro , roo = roo ) ;

    if parent != '' :

        mc.parent ( newGrp , parent ) ;

    return newGrp ;

# contraints

def parCon ( parent = [] , target = '' , lock = False , mo = False ) :
    
    parCon =  mc.parentConstraint ( parent , target , mo = mo ) [0] ;
      
    if lock == True : 
          
        mc.setAttr ( '%s.nodeState' % parCon , lock = True ) ; 
        mc.setAttr ( '%s.interpType' % parCon , lock = True ) ; 
        
        #
        
        list = type ( [] ) ;

        if type ( parent ) == list :
   
            for i in range ( 0 , len ( parent ) ) :
   
                mc.setAttr ( '%s.%sW%s' % ( parCon , parent [ i ] , i ) , lock = True ) ;
      
        else :
          
            mc.setAttr ( '%s.%sW0' % ( parCon , parent ) , lock = True ) ;
       
    return parCon ;
   
def pointCon ( parent = [] , target = '' , lock = False , mo = False ) :
    
    pointCon =  mc.pointConstraint ( parent , target , mo = mo ) [0] ;
      
    if lock == True :
       
        axes = [ 'X' , 'Y' , 'Z' ] ;
       
        mc.setAttr ( '%s.nodeState' % pointCon , lock = True ) ;
       
        for axis in axes :
           
            mc.setAttr ( '%s.offset%s' % ( pointCon , axis ) , lock = True ) ;
       
        #
       
        list = type ( [] ) ;
        
        if type ( parent ) == list :
               
            for i in range ( 0 , len ( parent ) ) :
           
                mc.setAttr ( '%s.%sW%s' % ( pointCon , parent [i] , i ) , lock = True ) ; 
        
        else :
            
            mc.setAttr ( '%s.%sW0' % ( pointCon , parent ) , lock = True ) ;
                 
    return pointCon ;
   
def oriCon ( parent = [] , target = '' , lock = False , mo = False ) :
    
    oriCon =  mc.orientConstraint ( parent , target , mo = mo ) [0] ;
    
    if lock == True :
       
        axes = [ 'X' , 'Y' , 'Z' ] ;
       
        mc.setAttr ( '%s.nodeState' % oriCon , lock = True ) ;
        mc.setAttr ( '%s.interpType' % oriCon , lock = True ) ;
              
        for axis in axes :
           
            mc.setAttr ( '%s.offset%s' % ( oriCon , axis ) , lock = True ) ;
       
        list = type ( [] ) ;
       
        if type ( parent ) == list :
           
            for i in range ( 0 , len ( parent ) ) :
           
                mc.setAttr ( '%s.%sW%s' % ( oriCon , parent [i] , i ) , lock = True ) ; 
    
        else :
            
            mc.setAttr ( '%s.%sW0' % ( oriCon , parent ) , lock = True ) ;
    
    return oriCon ;
