import maya.cmds as mc ;
import maya.mel as mm ;
import sys ;
import pymel.core as pm ;

from rf_maya.rftool.sim.tools.simtools.track.absoluteTrack import mayaLibrary as abtMl 
reload ( abtMl ) ;

from rf_maya.rftool.sim.tools.simtools.track.absoluteTrack import general as abtGl 
reload ( abtGl ) 

from rf_maya.rftool.sim.tools.simtools.track.absoluteTrack import panel as abtPnl 
reload ( abtPnl )

from rf_maya.rftool.sim.tools.simtools.track.absoluteTrack import controller as abtCtrl 
reload ( abtCtrl )

# import tools.simtools.track.absoluteTrack.mayaLibrary as abtMl ;
# reload ( abtMl ) ;

# import tools.simtools.track.absoluteTrack.general as abtGl ;
# reload ( abtGl ) ;

# import tools.simtools.track.absoluteTrack.panel as abtPnl ;
# reload ( abtPnl ) ;

# import tools.simtools.track.absoluteTrack.controller as abtCtrl ;
# reload ( abtCtrl ) ;

# # procs 

def trackButtonCmd (*args) :
    
    startFrame = mc.playbackOptions ( q = True , minTime = True ) ; 
    endFrame = mc.playbackOptions ( q = True , maxTime = True ) ;
    
    rotTrack ( startFrame = startFrame ,
        endFrame = endFrame ,
        defaultFrame = startFrame ) ;

# def timeRangeQuery (*args) :
    
#     startFrame = mc.playbackOptions ( q = True , minTime = True ) ; 
#     endFrame = mc.playbackOptions ( q = True , maxTime = True ) ;
    
#     return ( startFrame , endFrame ) ;

def rotTrack ( startFrame = 1 , endFrame = 100 , defaultFrame = 970 ) :
    
    # progressbar = abtMul.progressBar ( progressBar = 'absTrack_progressBar' ) ;
    
    name = 'TRACK_TFM'
        
    axes = [ 'x' , 'y' , 'z' ] ;
               
    roo = mc.xform ( name , q = True , ws = True , roo = True ) ;

    roo = 'xyz' ;
        
    # dummy rivet
    
    dmmyRiv = mc.listRelatives ( mc.createNode ( 'locator' ) , p = True ) ;
    dmmyRiv = mc.rename ( dmmyRiv , '%s_dmmyRiv' % name ) ;
    
    mc.xform ( dmmyRiv , roo = roo ) ;
    
    mc.connectAttr ( '%s.translate' % name , '%s.translate' % dmmyRiv ) ;
    mc.connectAttr ( '%s.rotate' % name , '%s.rotate' % dmmyRiv ) ;
    
    # >>>
    # progressbar.update ( value = 20 ) ;
    
        # bake key
    
    startFrame = startFrame - 20 ;
    endFrame = endFrame + 20 ;

    # sb = pm.floatField ( 'sampleBy_intput' , q = True , v = True ) ;
    
    try : 
    
        showList = abtPnl.activePanelShowQuery () ;
    
    except :
        
        try : 
            abtPnl.activePanelAllDisplay ( switch = False ) ;
            mc.bakeResults ( dmmyRiv , at = [ "tx" , "ty" , "tz" , "rx" , "ry" , "rz" ] , t = ( startFrame , endFrame ) , sb = 1 , simulation = True , minimizeRotation = True ) ;        
            # abtPnl.activePanelAllDisplay ( switch = True ) ;
        
        except : 
            mc.modelEditor ( 'modelPanel4' , e = True , allObjects = False ) ;
            mc.bakeResults ( dmmyRiv , at = [ "tx" , "ty" , "tz" , "rx" , "ry" , "rz" ] , t = ( startFrame , endFrame ) , sb = 1 , simulation = True , minimizeRotation = True ) ;
            #mc.modelEditor ( modelPanel4 , e = True , allObjects = True ) ;
            
    else :
    
        abtPnl.activePanelAllDisplay ( switch = False ) ;
        mc.bakeResults ( dmmyRiv , at = [ "tx" , "ty" , "tz" , "rx" , "ry" , "rz" ] , t = ( startFrame , endFrame ) , sb = 1 , simulation = True , minimizeRotation = True ) ;
        abtPnl.activePanelShowSet ( showList ) ;
        
    abtGl.lockAttr ( dmmyRiv , t = False , r = False , hide = True ) ;
    
    mc.currentTime ( defaultFrame ) ;
    
    
    # >>>
    # progressbar.update ( value = 70 ) ;

    mc.disconnectAttr ( '%s.translate' % name , '%s.translate' % dmmyRiv ) ;
    mc.disconnectAttr ( '%s.rotate' % name , '%s.rotate' % dmmyRiv ) ;
    
    #################
    'global no touch'
    #################
    globalNoTouch = mc.group ( em = True , n = '%s_track_noTouch_grp' % name ) ;
    mc.xform ( globalNoTouch , roo = roo ) ;
    mc.setAttr ( '%s.v' % globalNoTouch , 0 ) ;
    abtGl.lockAttr ( globalNoTouch ) ;
    mc.parent ( dmmyRiv , globalNoTouch ) ;
    
    ###########
    ''' BWD ''' 
    ###########
        
        # ROT: transpose and decompose
    pluginName = 'matrixNodes.mll'

    if not mc.pluginInfo(pluginName, q=True, l=True):
    	mc.loadPlugin(pluginName, qt=True)
    rotTpm = mc.createNode ( 'transposeMatrix' , n = '%s_BWD_rotRev_tpm' % name ) ; 
    mc.connectAttr ( '%s.matrix' % dmmyRiv , '%s.inputMatrix' % rotTpm ) ;    
    
    rotDcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_rotRev_dcm' % name ) ;
    mc.connectAttr ( '%s.outputMatrix' % rotTpm , '%s.inputMatrix' % rotDcm ) ;
    
        # ROT: BWD

    rotBWD = mc.group ( em = True , n = '%s_BWD_rotBWD_grp' % name ) ;
    mc.xform ( rotBWD , roo = roo ) ;
    abtGl.lockAttr ( rotBWD , r = False , hide = True ) ;
    abtGl.lockAttr ( rotBWD , t = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputRotate' % rotDcm , '%s.r' % rotBWD ) ;
    

    ''''''
    
    rotBWD_dmmy = mc.group ( em = True , n = '%s_BWD_rotBWD_dmmyGrp' % name ) ;
    mc.xform ( rotBWD_dmmy , roo = roo ) ; 
    abtGl.lockAttr ( rotBWD_dmmy , r = False , hide = True ) ;
    abtGl.lockAttr ( rotBWD_dmmy , t = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputRotate' % rotDcm , '%s.r' % rotBWD_dmmy ) ;
    
    
    transBWD_ref = mc.group ( em = True , n = '%s_BWD_transBWD_dmmyGrp' % name ) ;
    mc.xform ( transBWD_ref , roo = roo ) ;
    mc.setAttr ( '%s.v' % transBWD_ref , 0 ) ;
    abtGl.lockAttr ( transBWD_ref ) ;
    mc.parent ( transBWD_ref , rotBWD_dmmy ) ;
    mc.connectAttr ( '%s.t' % dmmyRiv , '%s.t' % transBWD_ref ) ;
    mc.connectAttr ( '%s.r' % dmmyRiv , '%s.r' % transBWD_ref ) ;
    
    transBWD_ref_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_transBWD_ref_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % transBWD_ref , '%s.inputMatrix' % transBWD_ref_dcm ) ;
    mc.connectAttr ( '%s.rotateOrder' % transBWD_ref , '%s.inputRotateOrder' % transBWD_ref_dcm ) ;
    
    transBWD_ref_rev = mc.createNode ( 'multiplyDivide' , n = '%s_BWD_transBWD_ref_rev_mdv' % name ) ;
    mc.connectAttr ( '%s.outputTranslate' % transBWD_ref_dcm , '%s.i1' % transBWD_ref_rev ) ;
    for each in axes :
        mc.setAttr ( '%s.i2%s' % ( transBWD_ref_rev , each ) , -1 ) ; 
        
        # TRANS : BWD
        
    transBWD = mc.group ( em = True , n = '%s_BWD_transBWD_grp' % name ) ;
    mc.xform ( transBWD , roo = roo ) ;
    abtGl.lockAttr ( transBWD , t = False , hide = True ) ;
    abtGl.lockAttr ( transBWD , r = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.o' % transBWD_ref_rev , '%s.t' % transBWD ) ;
    mc.parent ( rotBWD , transBWD ) ;
                   
        # controller
        
    ctrlZro = mc.group ( em = True , n = '%s_BWD_ctrlZro_grp' % name ) ;
    mc.xform ( ctrlZro , roo = roo ) ;

    mainCtrl = abtCtrl.createCurve ( jnt = '' , shape = 'fourDirectionalArrow' , name = '%s_BWD_ctrl' % name , 
        gimble = False , color = 'lightBlue' , joint = True , scale = 1*2 ) [0] ;
    mc.xform ( mainCtrl , roo = roo ) ;
    abtGl.lockAttr ( mainCtrl , t = False , r = False , hide = True ) ;

    gmblCtrl = abtCtrl.createCurve ( jnt = '' , shape = 'star' , name = '%s_BWD_gmblCtrl' % name , 
        gimble = False , color = 'white' , joint = False , scale = 3.3*2 ) [0] ;
    mc.xform ( gmblCtrl , roo = roo ) ;
    abtGl.lockAttr ( gmblCtrl , t = False , r = False , hide = True ) ;

    mainCtrlShape = mc.listRelatives ( mainCtrl , shapes = True ) [0] ;
    mc.addAttr ( mainCtrlShape , ln = 'gimbalControl' , at = 'bool' , keyable = True ) ;
    mc.connectAttr ( '%s.gimbalControl' % mainCtrlShape , '%sShape.v' % gmblCtrl ) ;

        # dmmyRiv dmmy
        
    dmmyRivRefGrp = mc.group ( em = True , n = '%s_dmmyRiv_ref_grp' % name ) ; 
    mc.xform ( dmmyRivRefGrp , roo = roo ) ;
    mc.setAttr ( '%s.v' % dmmyRivRefGrp , 0 ) ;
    abtGl.lockAttr ( dmmyRivRefGrp ) ;
    mc.parent ( dmmyRivRefGrp , dmmyRiv ) ;
    
    dmmyRiv_ref_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_dmmyRiv_ref_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % dmmyRivRefGrp , '%s.inputMatrix' % dmmyRiv_ref_dcm ) ;
    mc.connectAttr ( '%s.rotateOrder' % dmmyRivRefGrp , '%s.inputRotateOrder' % dmmyRiv_ref_dcm ) ;
    
            # default tfm
        
    defaultTfm = mc.group ( em = True , n = '%s_BWD_defaultTfm_grp' % name ) ;
    mc.xform ( defaultTfm , roo = roo ) ;
    abtGl.lockAttr ( defaultTfm , t = False , r = False , hide = True ) ;
    
            # AMP

    mc.addAttr ( mainCtrl , ln = '__amplifier__' , at = 'double' , keyable = True ) ;
    mc.setAttr ( '%s.__amplifier__' % mainCtrl , lock = True ) ;
    
    for each in axes :
        mc.addAttr ( mainCtrl , ln = 'trans%s' % each.upper() , at = 'double' , keyable = True , dv = 1 ) ;
        
        temptBc = mc.createNode ( 'blendColors' , n = '%s_BWD_t%sSwitch_bc' % ( name , each ) ) ;
        
        temptTrans = mc.getAttr ( '%s.t%s' % ( dmmyRiv , each ) ) ; 
         
        mc.connectAttr ( '%s.trans%s' % ( mainCtrl , each.upper() ) , '%s.blender' % temptBc ) ;
                
        mc.connectAttr ( '%s.outputTranslate%s' % ( dmmyRiv_ref_dcm , each.upper() ) , '%s.c1r' % temptBc ) ;
        mc.setAttr ( '%s.c2r' % temptBc , temptTrans ) ;

        mc.connectAttr ( '%s.outputR' % temptBc , '%s.t%s' % ( defaultTfm , each ) ) ;
                
    ####

    for each in axes :
        mc.addAttr ( mainCtrl , ln = 'rot%s' % each.upper() , at = 'double' , keyable = True , dv = 1 ) ;
        
        temptBc = mc.createNode ( 'blendColors' , n = '%s_BWD_r%sSwitch_bc' % ( name , each ) ) ;
        
        temptRot = mc.getAttr ( '%s.r%s' % ( dmmyRiv , each ) ) ; 
         
        mc.connectAttr ( '%s.rot%s' % ( mainCtrl , each.upper() ) , '%s.blender' % temptBc ) ;
                
        mc.connectAttr ( '%s.outputRotate%s' % ( dmmyRiv_ref_dcm , each.upper() ) , '%s.c1r' % temptBc ) ;
        mc.setAttr ( '%s.c2r' % temptBc , temptRot ) ;

        mc.connectAttr ( '%s.outputR' % temptBc , '%s.r%s' % ( defaultTfm , each ) ) ;
            
    mc.parent ( transBWD , gmblCtrl ) ;    
    mc.parent ( gmblCtrl , mainCtrl ) ;
    mc.parent ( mainCtrl , ctrlZro ) ;

    #################################################    

    #mc.parent ( ctrlZro , defaultTfm ) ;
    ctrlZro_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_ctrlZro_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % defaultTfm , '%s.inputMatrix' % ctrlZro_dcm ) ; 
    mc.connectAttr ( '%s.rotateOrder' % defaultTfm , '%s.inputRotateOrder' % ctrlZro_dcm ) ; 
    
    mc.connectAttr ( '%s.outputTranslate' % ctrlZro_dcm , '%s.t' % ctrlZro ) ;
    mc.connectAttr ( '%s.outputRotate' % ctrlZro_dcm , '%s.r' % ctrlZro ) ;
    abtGl.lockAttr ( ctrlZro ) ;
    
        # organization 
        
    BWD_noTouch = mc.group ( em = True , n = '%s_BWD_noTouch_grp' % name ) ;
    mc.xform ( BWD_noTouch , roo = roo ) ;
    # mc.setAttr ( '%s.v' % BWD_noTouch , 0 ) ;
    abtGl.lockAttr ( BWD_noTouch ) ;
    
    mc.parent ( rotBWD_dmmy , BWD_noTouch );
    mc.parent ( defaultTfm , BWD_noTouch ) ;
    
    abtGl.lockAttr ( defaultTfm , s = False , v = False ) ;
    
        # BWD group prep

    BWD_dmmy = mc.group ( em = True , n = '%s_BWD_BWDgrp_dmmyGrp' % name ) ;
    mc.xform ( BWD_dmmy , roo = roo ) ;
    mc.setAttr ( '%s.v' % BWD_dmmy , 0 ) ;
    abtGl.lockAttr ( BWD_dmmy ) ;
    mc.parent ( BWD_dmmy , rotBWD ) ;
    
    BWD_dmmy_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_BWDgrp_dmmy_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % BWD_dmmy , '%s.inputMatrix' % BWD_dmmy_dcm ) ;
    mc.connectAttr ( '%s.rotateOrder' % BWD_dmmy , '%s.inputRotateOrder' % BWD_dmmy_dcm ) ;
    
    ########### 
    ''' FWD ''' 
    ###########
    # >>>
    # progressbar.update ( value = 90 ) ;
    
        # ref
        
            # under gimbal control ref
        
    gmblCtrl_ref = mc.group ( em = True , n = '%s_BWD_gmblCtrl_dmmyGrp' % name ) ;
    mc.xform ( gmblCtrl_ref , roo = roo ) ;
    mc.setAttr ( '%s.v' % gmblCtrl_ref , 0 ) ;
    abtGl.lockAttr ( gmblCtrl_ref ) ;
    mc.parent ( gmblCtrl_ref , gmblCtrl ) ;

    gmblCtrl_dmmy_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_BWD_gmblCtrl_dmmy_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % gmblCtrl_ref , '%s.inputMatrix' % gmblCtrl_dmmy_dcm ) ;
    mc.connectAttr ( '%s.rotateOrder' % gmblCtrl_ref , '%s.inputRotateOrder' % gmblCtrl_dmmy_dcm ) ;
      
    gmblCtrl_dmmy_dcm_ref = mc.group ( em = True , n = '%s_BWD_gmblCtrl_dmmy_dcm_ref_grp' % name ) ;
    mc.xform ( gmblCtrl_dmmy_dcm_ref , roo = roo ) ;
    mc.connectAttr ( '%s.outputTranslate' % gmblCtrl_dmmy_dcm , '%s.t' % gmblCtrl_dmmy_dcm_ref ) ;
    mc.connectAttr ( '%s.outputRotate' % gmblCtrl_dmmy_dcm , '%s.r' % gmblCtrl_dmmy_dcm_ref ) ;
        
    FWD_rotTpm = mc.createNode ( 'transposeMatrix' , n = '%s_FWD_rotRev_tpm' % name ) ; 
    mc.connectAttr ( '%s.matrix' % gmblCtrl_dmmy_dcm_ref , '%s.inputMatrix' % FWD_rotTpm ) ;    
    
    FWD_rotDcm = mc.createNode ( 'decomposeMatrix' , n = '%s_FWD_rotRev_dcm' % name ) ;
    mc.connectAttr ( '%s.outputMatrix' % FWD_rotTpm , '%s.inputMatrix' % FWD_rotDcm ) ;
    
    FWD_rotBWD_dmmy = mc.group ( em = True , n = '%s_FWD_rotBWD_dmmyGrp' % name ) ;
    mc.xform ( FWD_rotBWD_dmmy , roo = roo ) ; 
    abtGl.lockAttr ( FWD_rotBWD_dmmy , r = False , hide = True ) ;
    abtGl.lockAttr ( FWD_rotBWD_dmmy , t = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputRotate' % FWD_rotDcm , '%s.r' % FWD_rotBWD_dmmy ) ;
    
    FWD_transBWD_ref = mc.group ( em = True , n = '%s_FWD_transBWD_dmmyGrp' % name ) ;
    mc.xform ( FWD_transBWD_ref , roo = roo ) ;
    mc.setAttr ( '%s.v' % FWD_transBWD_ref , 0 ) ;
    abtGl.lockAttr ( FWD_transBWD_ref ) ;
    mc.parent ( FWD_transBWD_ref , FWD_rotBWD_dmmy ) ;
    mc.connectAttr ( '%s.t' % gmblCtrl_dmmy_dcm_ref , '%s.t' % FWD_transBWD_ref ) ;
    mc.connectAttr ( '%s.r' % gmblCtrl_dmmy_dcm_ref , '%s.r' % FWD_transBWD_ref ) ;
    
    FWD_transBWD_ref_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_FWD_transBWD_ref_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % FWD_transBWD_ref , '%s.inputMatrix' % FWD_transBWD_ref_dcm ) ;
    mc.connectAttr ( '%s.rotateOrder' % FWD_transBWD_ref , '%s.inputRotateOrder' % FWD_transBWD_ref_dcm ) ;
    
    FWD_transBWD_ref_rev = mc.createNode ( 'multiplyDivide' , n = '%s_FWD_transBWD_ref_rev_mdv' % name ) ;
    mc.connectAttr ( '%s.outputTranslate' % FWD_transBWD_ref_dcm , '%s.i1' % FWD_transBWD_ref_rev ) ;
    for each in axes :
        mc.setAttr ( '%s.i2%s' % ( FWD_transBWD_ref_rev , each ) , -1 ) ; 

    FWD_transBWD = mc.group ( em = True , n = '%s_FWD_transBWD_grp' % name ) ;
    mc.xform ( FWD_transBWD , roo = roo ) ;
    abtGl.lockAttr ( FWD_transBWD , t = False , hide = True ) ;
    abtGl.lockAttr ( FWD_transBWD , r = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.o' % FWD_transBWD_ref_rev , '%s.t' % FWD_transBWD ) ;
    
    FWD_rotBWD = mc.group ( em = True , n = '%s_FWD_rotBWD_grp' % name ) ;
    mc.xform ( FWD_rotBWD , roo = roo ) ; 
    abtGl.lockAttr ( FWD_rotBWD , r = False , hide = True ) ;
    abtGl.lockAttr ( FWD_rotBWD , t = False , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputRotate' % FWD_rotDcm , '%s.r' % FWD_rotBWD ) ;
              
    mc.parent ( FWD_rotBWD , FWD_transBWD ) ;
    
    # >>>
    # progressbar.update ( value = 90 ) ;
    
        # default tfm
    
    fwdDefaultTfm = mc.group ( em = True , n = '%s_FWD_defaultTfm_grp' % name ) ;
    mc.xform ( fwdDefaultTfm , roo = roo ) ;
    mc.connectAttr ( '%s.t' % dmmyRiv , '%s.t' % fwdDefaultTfm ) ; 
    mc.connectAttr ( '%s.r' % dmmyRiv , '%s.r' % fwdDefaultTfm ) ;
    
    abtGl.lockAttr ( fwdDefaultTfm ) ;
    
    mc.parent ( FWD_transBWD , fwdDefaultTfm ) ;
    
        # organization 
        
    FWD_noTouch = mc.group ( em = True , n = '%s_FWD_noTouch_grp' % name ) ;
    mc.xform ( FWD_noTouch , roo = roo ) ;
    abtGl.lockAttr ( FWD_noTouch ) ;
    
    mc.parent ( FWD_rotBWD_dmmy , FWD_noTouch );
    mc.parent ( fwdDefaultTfm , FWD_noTouch ) ;
    mc.parent ( gmblCtrl_dmmy_dcm_ref , FWD_noTouch ) ;
    
    abtGl.lockAttr ( gmblCtrl_dmmy_dcm_ref ) ;
    abtGl.lockAttr ( defaultTfm , s = False , v = False ) ;
    
        # FWD group prep

    FWD_dmmy = mc.group ( em = True , n = '%s_FWD_FWDgrp_dmmyGrp' % name ) ;
    mc.xform ( FWD_dmmy , roo = roo ) ;
    mc.setAttr ( '%s.v' % FWD_dmmy , 0 ) ;
    abtGl.lockAttr ( FWD_dmmy ) ;
    mc.parent ( FWD_dmmy , FWD_rotBWD ) ;
    
    FWD_dmmy_dcm = mc.createNode ( 'decomposeMatrix' , n = '%s_FWD_FWDgrp_dmmy_dcm' % name ) ;
    mc.connectAttr ( '%s.worldMatrix' % FWD_dmmy , '%s.inputMatrix' % FWD_dmmy_dcm ) ;
    mc.connectAttr ( '%s.rotateOrder' % FWD_dmmy , '%s.inputRotateOrder' % FWD_dmmy_dcm ) ;
       
    ######################## 
    ''' user interaction ''' 
    ########################
    # >>>
    # progressbar.update ( value = 95 ) ;
    
    mc.parent ( BWD_noTouch , globalNoTouch ) ;    
    mc.parent ( FWD_noTouch , globalNoTouch ) ;
         
    # BWD grp
                    
    BWD_grp = mc.group ( em = True , n = '%s_BWD' % name ) ;
    mc.xform ( BWD_grp , roo = roo ) ;
    abtGl.lockAttr ( BWD_grp , t = False , r = False , v = False , hide = True ) ;
    abtGl.lockAttr ( BWD_grp , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputTranslate' % BWD_dmmy_dcm , '%s.t' % BWD_grp ) ;  
    mc.connectAttr ( '%s.outputRotate' % BWD_dmmy_dcm , '%s.r' % BWD_grp ) ;
    
    # FWD grp
                    
    FWD_grp = mc.group ( em = True , n = '%s_FWD' % name ) ;
    mc.xform ( FWD_grp , roo = roo ) ;
    abtGl.lockAttr ( FWD_grp , t = False , r = False , v = False , hide = True ) ;
    abtGl.lockAttr ( FWD_grp , s = False , v = False ) ;
    mc.connectAttr ( '%s.outputTranslate' % FWD_dmmy_dcm , '%s.t' % FWD_grp ) ;  
    mc.connectAttr ( '%s.outputRotate' % FWD_dmmy_dcm , '%s.r' % FWD_grp ) ;
    
    ### >>>
    
    # progressbar.delete ( ) ;

    # progressbar.insert ( parent = 'absTrack_progressBarLayout' ) ;

    # mc.formLayout ( 'absTrack_progressBarLayout' , e = True ,
    #     af = (
    #     ( 'absTrack_progressBar' , 'left' , 0 ) , ( 'absTrack_progressBar' , 'right' , 0 ) )
    #     ) ;
##################
''' RIVET '''
##################

rivet_cmd = '''

// Copyright (C) 2000-2001 Michael Bazhutkin - Copyright (C) 2000 studio Klassika
    // www.geocites.com/bazhutkin
    // bazhutkin@mail.ru
    //
    //  Rivet (button) 1.0
    //  Script File
    //  MODIFY THIS AT YOUR OWN RISK
    //
    //  Creation Date:  April 13, 2001
    //
    //
    //  Description:
    //  Use "Rivet" to constrain locator to polygon or NURBS surfaces
    //  Select two edges on polygon object
    //  or select one point on NURBS surface and call rivet
    //  Parent your rivets and buttons to this locator
    
    global proc string rivet ( )
    {
    
    string $nameObject;
    string $namePOSI;
    
    string $parts[];
    string $list[] = `filterExpand -sm 32`;
    int $size = size($list);
    if ($size > 0)
    {
        if ($size != 2)
        {   error("No two edges selected");
            return "";
        }
    
        tokenize($list[0],".",$parts);
        $nameObject = $parts[0];
        tokenize($list[0],"[]",$parts);
        float $e1 = $parts[1];
        tokenize($list[1],"[]",$parts);
        float $e2 = $parts[1];
    
        string $nameCFME1 = `createNode curveFromMeshEdge -n "rivetCurveFromMeshEdge1"`;
            setAttr ".ihi" 1;
            setAttr ".ei[0]"  $e1;
        string $nameCFME2 = `createNode curveFromMeshEdge -n "rivetCurveFromMeshEdge2"`;
            setAttr ".ihi" 1;
            setAttr ".ei[0]"  $e2;
        string $nameLoft = `createNode loft -n "rivetLoft1"`;
            setAttr -s 2 ".ic";
            setAttr ".u" yes;
            setAttr ".rsn" yes;
    
        $namePOSI = `createNode pointOnSurfaceInfo -n "rivetPointOnSurfaceInfo1"`;
            setAttr ".turnOnPercentage" 1;
            setAttr ".parameterU" 0.5;
            setAttr ".parameterV" 0.5;
    
        connectAttr -f ($nameLoft + ".os") ($namePOSI + ".is");
        connectAttr ($nameCFME1 + ".oc") ($nameLoft + ".ic[0]");
        connectAttr ($nameCFME2 + ".oc") ($nameLoft + ".ic[1]");
        connectAttr ($nameObject + ".w") ($nameCFME1 + ".im");
        connectAttr ($nameObject + ".w") ($nameCFME2 + ".im");
    }
    else
    {   $list = `filterExpand -sm 41`;
        $size = size($list);
    
        if ($size > 0)
        {
            if ($size != 1)
            {   error("No one point selected");
                return "";
            }
            tokenize($list[0],".",$parts);
            $nameObject = $parts[0];
            tokenize($list[0],"[]",$parts);
            float $u = $parts[1];
            float $v = $parts[2];
            $namePOSI = `createNode pointOnSurfaceInfo -n "rivetPointOnSurfaceInfo1"`;
                    setAttr ".turnOnPercentage" 0;
                    setAttr ".parameterU" $u;
                    setAttr ".parameterV" $v;
            connectAttr -f ($nameObject + ".ws") ($namePOSI + ".is");
        }
        else
        {   error("No edges or point selected");
            return "";
        }
    }
    
    string $nameLocator = `createNode transform -n "rivet1"`;
    createNode locator -n ($nameLocator + "Shape") -p $nameLocator;
    
    string $nameAC = `createNode aimConstraint -p $nameLocator -n ($nameLocator + "_rivetAimConstraint1")`;
        setAttr ".tg[0].tw" 1;
        setAttr ".a" -type "double3" 0 1 0;
        setAttr ".u" -type "double3" 0 0 1;
        setAttr -k off ".v";
        setAttr -k off ".tx";
        setAttr -k off ".ty";
        setAttr -k off ".tz";
        setAttr -k off ".rx";
        setAttr -k off ".ry";
        setAttr -k off ".rz";
        setAttr -k off ".sx";
        setAttr -k off ".sy";
        setAttr -k off ".sz";
    
    connectAttr ($namePOSI + ".position") ($nameLocator + ".translate");
    connectAttr ($namePOSI + ".n") ($nameAC + ".tg[0].tt");
    connectAttr ($namePOSI + ".tv") ($nameAC + ".wu");
    connectAttr ($nameAC + ".crx") ($nameLocator + ".rx");
    connectAttr ($nameAC + ".cry") ($nameLocator + ".ry");
    connectAttr ($nameAC + ".crz") ($nameLocator + ".rz");
    
    select -r $nameLocator;
    return ($nameLocator);
    
    }
    
    rivet;

'''

def rivetTransform(*args ) :
    # select edge, this will create a rivet and a clean transform 

    RIV = pm.mel.eval ( rivet_cmd ) ;
    RIV = pm.rename ( RIV , 'TRACK_RIV' ) ;

    TFM = pm.createNode ( 'transform' , n = 'TRACK_TFM' ) ;
    con = pm.pointConstraint( RIV , TFM , skip = 'none' , mo = False ) ;
    pm.delete ( con ) ;
    
    pm.parentConstraint ( RIV , TFM , mo = True , skipTranslate = 'none' , skipRotate = 'none' ) ;

    trackButtonCmd ()

# def absoluteTrackUI ( *args ) :

#     def filler ( *args ) :
#         pm.text ( label = '' ) ;

#     def separator ( *args ) :
#         pm.separator ( vis = False , h = 5 ) ;

#     width = 300.00 ;
   
#     if pm.window ( 'abTrack_UI' , exists = True ) :
#         pm.deleteUI ( 'abTrack_UI' ) ;
#     else : pass ;
   
#     window = pm.window ( 'abTrack_UI', title = "absolute track v3.0" ,
#         mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    
#     pm.window ( 'abTrack_UI' , e = True , w = width ) ;
#     with window :
    
#         with pm.rowColumnLayout ( nc = 1 ) :

#             with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/3 ) , ( 2 , width/3 ) , ( 3 , width/3 ) ] ) :
#                 startFrameLbl = pm.text ( 'absTrack_startFrameLbl' , l = 'start frame' , h = 15 ) ;
#                 endFrameLbl = pm.text ( 'absTrack_endFrameLbl' , l = 'end frame' , h = 15 ) ;
#                 defaultPosLbl = pm.text ( 'absTrack_defaultPosLbl' , l = 'default pos' , h = 15 ) ;

#                 startFrame = pm.intField ( 'absTrack_startFrame' , v = timeRangeQuery () [0] , h = 25 ) ;
#                 endFrame = pm.intField ( 'absTrack_endFrame' , v = timeRangeQuery () [1] , h = 25 ) ;
#                 defaultPos = pm.intField ( 'absTrack_defaultPos' , v = timeRangeQuery () [0] , h = 25 ) ;

#             separator ( ) ;

#             with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/3*2 ) , ( 2 , width/3 ) ] ) :
#                 filler ( ) ;

#                 with pm.rowColumnLayout ( nc = 1 , w = width/3 ) :
#                     pm.text ( label = 'sampleBy' , w = width/3 ) ;
#                     pm.floatField ( 'sampleBy_intput' , precision = 2 , v = 1 , w = width/3 ) ;

#             separator ( ) ;

#             button = pm.button ( 'absTrack_button' , l = 'track' , c = rivetTransform , h = 25 ) ;

            
#             progressBarLayout = pm.formLayout ( 'absTrack_progressBarLayout' , h = 25) ;
#             with progressBarLayout :

#                 progressBar = pm.progressBar ( 'absTrack_progressBar' , h = 25 , w = width ) ;
                

#     window.show () ;
# #rivetTransform()