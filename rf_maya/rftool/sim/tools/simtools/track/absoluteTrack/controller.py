import maya.cmds as mc ;

from rf_maya.rftool.sim.tools.simtools.track.absoluteTrack import general as abtGl 
reload ( abtGl ) 

from rf_maya.rftool.sim.tools.simtools.track.absoluteTrack import dic as abtDic 
reload ( abtDic ) 


def createCurve ( jnt = '' , shape = 'circle' , name = 'untitled_ctrl' , gimble = False , color = 'default' , joint = False , scale = 1 ) :
    # return [ crv , gimble ]
    
    crv = mc.curve ( n = name , d = 1 , p = ( abtDic.curvePoint [ shape ] ) ) ;

    crvShape = mc.listRelatives ( crv , s = True ) ;
    crvShape = mc.rename ( crvShape , '%sShape' % name ) ;

    # jnt
    
    if jnt != '' :

        crvRotOrder = mc.xform ( jnt , q = True , ws = True , roo = True ) ;
        mc.setAttr ( '%s.rotateOrder' % crv , abtDic.rotateOrder [ crvRotOrder ] ) ;

    # joint controller?
    
    if joint == True :
        jointTransform ( crv ) ;

    # gimble?

    if gimble == True :
        gimble = addGimbleCtrl ( ctrl = crv ) ;

    # scale
        
    mc.xform ( crv , s = ( scale , scale , scale ) ) ;
    mc.makeIdentity ( crv , apply = True , s = True ) ;

    # color

    changeColor ( crv = crv , color = color ) ;

    return crv , gimble ;

def changeColor ( crv = '' , color = '' ) :

    crvShape = mc.listRelatives ( crv , s = True ) ;
    
    mc.setAttr ( '%s.overrideEnabled' % crvShape[0] , 1 ) ;
    mc.setAttr ( '%s.overrideColor' % crvShape[0] , abtDic.overrideColor [ color ] ) ;
        
def jointTransform ( crv = '' ) :

    # This function parent curve's shape into the joint transformation, workable even if the curve is under a parent. However, target's tranformation should not have any value.

    parent = mc.listRelatives ( crv , parent = True ) ;
    
    crvShape = mc.listRelatives ( crv , s = True ) ;

    jointTransform = mc.createNode ( 'joint' ) ;

    if parent != None :

        mc.parent ( jointTransform , parent ) ;

    axes = ( 'x' , 'y' , 'z' ) ;
        
    crvPos = mc.xform ( crv , q = True , r = True , t = True ) ;
    crvRot = mc.xform ( crv , q = True , r = True , ro = True ) ;
    crvRO = mc.xform ( crv , q = True , r = True , roo = True ) ;
    crvScl = mc.xform ( crv , q = True , r = True , s = True ) ;

    for i in range ( 0 , 3 ) :
            
        mc.setAttr ( '%s.t%s' % ( jointTransform , axes [ i ] ) , crvPos [ i ] ) ;
        mc.setAttr ( '%s.r%s' % ( jointTransform , axes [ i ] ) , crvRot [ i ] ) ; 
        mc.setAttr ( '%s.s%s' % ( jointTransform , axes [ i ] ) , crvScl [ i ] ) ;
            
    mc.setAttr ( '%s.rotateOrder' % jointTransform , abtDic.rotateOrder [ crvRO ] ) ;
        
    # mc.makeIdentity ( jointTransform , apply = True , t = True , r = True , scale = True ) ; 
    # makeIdentity = freeze transform
        
    mc.parent ( crvShape [0] , jointTransform , s = True , r = True ) ;
    mc.delete ( crv ) ; 
        
    crv = mc.rename ( jointTransform , crv ) ;
    crvShape = mc.listRelatives ( crv , s = True ) ;
    mc.rename ( crvShape [0] , '%sShape' % crv ) ; 

    mc.setAttr ( '%s.drawStyle' % crv , 2 ) ;
    mc.setAttr ( '%s.radius' % crv , 0 , lock = True , cb = False ) ;

def addGimbleCtrl ( ctrl = '' ) :
    
    if ctrl [ -5 : ] == '_ctrl' :
    
        axes = [ 'x' , 'y' , 'z' ] ;
    
        ctrlNm = ctrl.split ( '_ctrl' ) ;
            
        gimbleShapeTrans = mc.duplicate ( ctrl ) ;
        
        mc.parent ( gimbleShapeTrans , ctrl ) ;
        
        gimbleTransform = mc.group ( em = True ) ;

        mc.parent ( gimbleTransform , ctrl ) ; 
        
        gimbleTrans = mc.xform ( gimbleShapeTrans , q = True , r = True , t = True ) ;
        gimbleRot = mc.xform ( gimbleShapeTrans , q = True , r = True , ro = True ) ;
        gimbleScl = mc.xform ( gimbleShapeTrans , q = True , r = True , s = True ) ;
        gimbleRotOrder = mc.xform ( gimbleShapeTrans , q = True , r = True , roo = True ) ;
       
        for i in range ( 0 , 3 ) :

            mc.setAttr ( '%s.t%s' % ( gimbleTransform , axes [ i ] ) , gimbleTrans [ i ] ) ;
            mc.setAttr ( '%s.r%s' % ( gimbleTransform , axes [ i ] ) , gimbleRot [ i ] ) ;                
            mc.setAttr ( '%s.s%s' % ( gimbleTransform , axes [ i ] ) , gimbleScl [ i ] * 0.75 ) ;
       
        mc.setAttr ( '%s.rotateOrder' % gimbleTransform , abtDic.rotateOrder [ gimbleRotOrder ] ) ;

        gimbleShape = mc.listRelatives ( gimbleShapeTrans , s = True ) ;
                
        mc.parent ( gimbleShape , gimbleTransform , s = True , r = True ) ;

        mc.delete ( gimbleShapeTrans ) ;

        mc.makeIdentity ( gimbleTransform , apply = True , scale = True ) ;
        
        gimbleCtrl = mc.rename ( gimbleTransform , '%sGmbl_ctrl' % ctrlNm [0] ) ;
        
        # add and connect gimble control attribute
        
        gimbleCtrlShape = mc.listRelatives ( gimbleCtrl , s = True ) ;

        gimbleCtrlShape = mc.rename ( gimbleCtrlShape , '%sGmbl_ctrlShape' % ctrlNm [0] ) ;

        ctrlShape = mc.listRelatives ( ctrl , s = True ) ;
    
        mc.addAttr ( ctrlShape , ln = 'gimbleControl' , at = 'bool' , keyable = True ) ;
        
        mc.connectAttr ( '%s.gimbleControl' % ctrlShape [0] , '%s.visibility' % gimbleCtrlShape ) ;

        changeColor ( crv = gimbleCtrl , color = 'white' ) ;

        return gimbleCtrl ;
        
    else :
        
        mc.error ( "This function requires the controller to have '_ctrl' as its suffix." ) ;

def createController ( jnt = '' , shape = 'circle' , gimble = False , color = 'default' , joint = False , scale = 1 , parent = '' , tfmGrp = False ) :
    # return [ ctrl , zroGrp , tfmGrp , gimbleCtrl ]
             
    if jnt [ -4 : ] != '_jnt' :
        
        mc.error ( "This function requires the joint to have '_jnt' as its suffix." ) ;

    jntNm = jnt.split ( '_jnt' ) ;
    
    jntPos = mc.xform ( jnt , q = True , ws = True , rp = True ) ;
    jntRot = mc.xform ( jnt , q = True , ws = True , ro = True ) ;
    jntRotOrder = mc.xform ( jnt , q = True , roo = True ) ; 

    axes = [ 'x' , 'y' , 'z' ] ;

    ctrl = createCurve ( jnt = jnt , shape = shape , name = '%s_ctrl' % jntNm [0] , gimble = gimble , color = color , joint = joint ) ;

    gimbleCtrl = ctrl [1] ;
    ctrl = ctrl [0] ;

    zroGrp = mc.group ( n = '%sCtrlZro_grp' % jntNm [0] , em = True ) ;
    mc.parent ( ctrl , zroGrp ) ;
    
    mc.xform ( zroGrp , ws = True , t = jntPos , ro = jntRot , roo = jntRotOrder ) ;

    mc.setAttr ( '%s.s' % zroGrp , scale , scale , scale , type = 'double3' ) ;        
    mc.makeIdentity ( zroGrp , apply = True , scale = True ) ;

    if tfmGrp == True :

        tfmGrp = mc.group ( n = '%sCtrlTfm_grp' % jntNm [0] , em = True ) ;
        mc.xform ( tfmGrp , ws = True , t = jntPos , ro = jntRot , roo = jntRotOrder ) ;
        
        mc.parent ( tfmGrp , zroGrp ) ;
        
        mc.makeIdentity ( zroGrp , apply = True , scale = True ) ;       

        mc.parent ( ctrl , tfmGrp ) ;
    
    #mc.setAttr ( '%s.t' % zroGrp , jntPos [0] , jntPos [1] , jntPos [2] , type = 'double3' ) ;
    #mc.setAttr ( '%s.r' % zroGrp , jntRot [0] , jntRot [1] , jntRot [2] , type = 'double3' ) ;

    if parent != '' :

        mc.parent ( zroGrp , parent ) ;

    return [ ctrl , zroGrp , tfmGrp , gimbleCtrl ] ;

def controller ( jnt = '' , shape = 'circle' , color = 'default' , joint = True , local = True , tfmGrp = False , gimble = True , scale = 1 , parent = '' , nm = ''  ) :
    # return [ ctrl , gimbleCtrl , zroGrp , tfmGrp ]

    if nm == '' :
    
        nm = str ( jnt ) .split ( '_jnt' ) [0] ;
    
    jntPos = mc.xform ( jnt , q = True , ws = True , rp = True ) ;
    jntRO = mc.xform ( jnt , q = True , ws = True , ro = True ) ;
    jntROO = mc.xform ( jnt , q = True , ws = True , roo = True ) ;
    
    
        # zero group
    
    zroGrp = mc.group ( n = '%sCtrlZro_grp' % nm , em = True ) ;
    
    if local == True :
    
        mc.xform ( zroGrp , ws = True , t = jntPos , ro = jntRO , roo = jntROO ) ;
    
    else :

        mc.xform ( zroGrp , ws = True , t = jntPos , roo = jntROO ) ; 

        # ctrl
        
    ctrl = createCurve ( jnt = '' , shape = shape , name = '%s_ctrl' % nm , gimble = gimble , color = color , joint = joint , scale = scale ) ;
    
    gmblCtrl = ctrl [1] ;
    ctrl = ctrl [0] ;

    mc.xform ( ctrl , ws = True , t = jntPos , ro = jntRO , roo = jntROO ) ;

    if gimble == True :
    
        mc.xform ( gmblCtrl , ws = True , roo = jntROO ) ;
    
    mc.makeIdentity ( ctrl , apply = True ) ;

    #
    
    if tfmGrp == True :

        tfmGrp = abtGen.copyGrp ( oriGrp = zroGrp , newGrpNm = '%sCtrlTfm_grp' % nm , parent = zroGrp ) ;
    
        mc.parent ( ctrl , tfmGrp ) ;
    
    else :
        
        mc.parent ( ctrl , zroGrp ) ;

    #

    if parent != '' :

        mc.parent ( zroGrp , parent ) ;
    
    return ctrl , gmblCtrl , zroGrp , tfmGrp ; 
