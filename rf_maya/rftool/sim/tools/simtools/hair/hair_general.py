import pymel.core as pm ;

def tube2OriSelection_cmd ( *args ) :
	# select ori curve from tube

	selection = pm.ls ( sl = True ) ;

	toSelect_list = [] ;

	for each in selection :
	    
	    shape = pm.listRelatives ( each , shapes = True ) [0] ;
	    
	    try :
	        extrude = pm.listConnections ( shape , type = 'extrude' ) [0] ;
	    
	        curveShape_list = pm.listConnections ( extrude , shapes = True , type = 'nurbsCurve'  ) ;
	        
	        for each in curveShape_list :
	            
	            if '_CRV' in str ( each ) :
	                toSelect_list.append ( each.split('_')[0] + '_ORI' ) ;
	   
	    except : print each + 'is not valid' ;
	   
	pm.select ( toSelect_list ) ;