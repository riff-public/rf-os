import pymel.core as pm ;
import os ;

############
############
''' core '''
############
############

def set_cmd ( *args ) :

    if pm.objExists ( 'crvBSH_set' ) == True :
        pass ;
    else :
        pm.sets ( name = 'crvBSH_set' ) ;

    return pm.general.PyNode ( 'crvBSH_set' ) ;

def blendShape_cmd ( *args ) :

    crvBSH_set = set_cmd ( ) ;

    selection = pm.ls ( sl = True ) ;

    crvBSH_set = set_cmd ( ) ;
    pm.sets ( crvBSH_set , e = True , add = selection ) ;

    driver_suffix = pm.textField ( 'hbsCV_driver_textField' ,  q = True , text = True ) ;
    driven_suffix = pm.textField ( 'hbsCV_driven_textField' ,  q = True , text = True ) ;

    cvBlend_stat = pm.checkBox ( 'hbsCV_cvBlend_checkBox' , q = True , v = True ) ;

    tip_val = pm.intField ( 'hbsCV_tip_floatField' , q = True , v = True ) ;
    base_val = pm.intField ( 'hbsCV_base_floatField' , q = True , v = True ) ;

    envelope = pm.floatField ( 'hbsCV_envelope_floatField' , q = True , v = True ) ;

    for each in selection :

        type = each.getShape() ;
        type = str ( pm.objectType ( type ) ) ;
        if type == 'nurbsCurve' :

            numberOfCv = pm.getAttr ( each + '.' + 'spans' ) + pm.getAttr ( each + '.' + 'degree' ) ;

            cvStart = int ( numberOfCv / 100.0 * base_val ) ;
            cvEnd = int ( numberOfCv / 100.0 * tip_val ) ;

            blendAmount = 1 / float ( cvEnd - cvStart ) ;

            driver = each.split(driven_suffix)[0] + driver_suffix ;
            driven = each ;

            ### still blendshape ###
            if cvBlend_stat == False :

                if pm.objExists ( '%s_cvBSH' % driver ) == True :
                    pm.blendShape ( '%s_cvBSH' % driver , e = True , weight = [ 0 , 1 ] , envelope = envelope ) ;
                    for i in range ( 0 , numberOfCv ) :
                        pm.setAttr ( '%s_cvBSH' % driver + '.inputTarget[0].inputTargetGroup[0].targetWeights[' + str ( i ) + ']' , 1 ) ;

                else :
                    pm.blendShape ( driver , driven  , origin = 'world' , weight = [ 0 , 1 ] , n = '%s_cvBSH' % driver , envelope = envelope ) ;

            ### cv blendshape ###
            
            elif cvBlend_stat == True :

                if pm.objExists ( '%s_cvBSH' % driver ) == False :

                    pm.blendShape ( driver , driven  , origin = 'world' , weight = [ 0 , 1 ] , n = '%s_cvBSH' % driver , envelope = envelope ) ;

                for i in range ( 0 , numberOfCv ) :
                    pm.setAttr ( '%s_cvBSH' % driver + '.inputTarget[0].inputTargetGroup[0].targetWeights[' + str ( i ) + ']' , 0 ) ;
                 
                for i in range ( 0 , cvStart ) :    
                    pm.setAttr ( '%s_cvBSH' % driver + '.inputTarget[0].inputTargetGroup[0].targetWeights[' + str ( i ) + ']' , 1 ) ;
                
                value = 1 ;
                for i in range ( cvStart , cvEnd ) :
                    pm.setAttr ( '%s_cvBSH' % driver + '.inputTarget[0].inputTargetGroup[0].targetWeights[' + str ( i ) + ']' , value ) ;
                    value -= blendAmount ;

                pm.blendShape ( '%s_cvBSH' % driver , edit = True , envelope = envelope ) ;

def deleteBlendshape_cmd ( *args ) :
 
    selection = pm.ls ( sl = True ) ;

    crvBSH_set = set_cmd ( ) ;
    pm.sets ( crvBSH_set , e = True , remove = selection ) ;

    driver_suffix = pm.textField ( 'hbsCV_driver_textField' ,  q = True , text = True ) ;
    driven_suffix = pm.textField ( 'hbsCV_driven_textField' ,  q = True , text = True ) ;

    for each in selection :

        driver = each.split(driven_suffix)[0] + driver_suffix ;
        driven = each ;

        if pm.objExists ( '%s_cvBSH' % driver ) == True :
            pm.delete ( '%s_cvBSH' % driver ) ;
        else : pass ;

def selectCVBSHCurve_cmd ( *args ) :

    crvBSH_set = set_cmd ( ) ;

    driver_suffix = pm.textField ( 'hbsCV_driver_textField' ,  q = True , text = True ) ;
    driven_suffix = pm.textField ( 'hbsCV_driven_textField' ,  q = True , text = True ) ;

    pm.select ( crvBSH_set ) ;

    # if pm.checkBox ( 'hbsCV_selectByEnvelope_checkBox' , q = True , value = True ) == True :

    selection = pm.ls ( sl = True ) ;
    toSelect_list = [] ;
    envelope = pm.floatField ( 'hbsCV_envelope_floatField' , q = True , v = True ) ;

    for each in selection :

        driver = each.split(driven_suffix)[0] + driver_suffix ;
        driven = each ;

        if pm.objExists ( '%s_cvBSH' % driver ) == True :
            if envelope == round ( pm.blendShape ( '%s_cvBSH' % driver , q = True , envelope = True ) , 2 ) :
                toSelect_list.append ( each ) ;
            else : pass ;
        else : pass ;

    pm.select ( toSelect_list ) ;

def getCurrentEnvelope_cmd ( *args ) :

    previousSelection = pm.ls ( sl = True ) ;

    crvBSH_set = set_cmd ( ) ;

    driver_suffix = pm.textField ( 'hbsCV_driver_textField' ,  q = True , text = True ) ;
    driven_suffix = pm.textField ( 'hbsCV_driven_textField' ,  q = True , text = True ) ;

    pm.select ( crvBSH_set ) ;

    selection = pm.ls ( sl = True ) ;

    envelope_list = [] ;

    for each in selection :

        driver = each.split(driven_suffix)[0] + driver_suffix ;
        driven = each ;

        if pm.objExists ( '%s_cvBSH' % driver ) == True :
            envelope = pm.blendShape ( '%s_cvBSH' % driver , q = True , envelope = True ) ;
            envelope = round ( envelope , 2 ) ;
            envelope_list.append ( envelope ) ;

        else : pass ;

    envelope_list = set ( envelope_list ) ;
    envelope_list = list ( envelope_list ) ;

    envelope_list.sort( ) ;

    pm.select ( previousSelection ) ;

    if len ( envelope_list ) == [] :
        return None ;
    else :
        return envelope_list ;

def updateEnvelopeOpt_cmd ( *args ) :

    envelopeOpt_stat = pm.checkBox ( 'hbsCV_selectByEnvelope_checkBox' , q = True , v = True ) ;
    if envelopeOpt_stat == False :

        ### remove all optionMenu items ###
        options_list = pm.optionMenu( 'hbsCV_envelope_opt' , q = True , itemListLong = True ) ;
        if options_list != [] :
            for each in options_list :
                pm.deleteUI ( each ) ;

        ### diable optionMenu ###
        pm.optionMenu( 'hbsCV_envelope_opt' , e = True , enable = False ) ;

    elif envelopeOpt_stat == True :
        ### remove all optionMenu items ###
        options_list = pm.optionMenu( 'hbsCV_envelope_opt' , q = True , itemListLong = True ) ;
        if options_list != [] :
            for each in options_list :
                pm.deleteUI ( each ) ;

        ### enable optionMenu ###
        pm.optionMenu( 'hbsCV_envelope_opt' , e = True , enable = True ) ;

        envelope_list = getCurrentEnvelope_cmd ( ) ;

        for each in envelope_list :
            if envelope_list != None :
                pm.menuItem ( label = str ( each ) , p = 'hbsCV_envelope_opt' ) ;
        
############
############
''' UI '''
############
############



hairDiagram_path ='O:/Pipeline/core/rf_maya/rftool/sim/image_fortools/hair_CVBlendshape_hair.png'

title = 'hair cv blendshape util v2.0' ;
width = 450.00 ;

def separator ( *args ) :
    pm.separator ( vis = False , h = 5 ) ;

def filler ( *args ) :
    pm.text ( label = '' ) ;

def updatePercentageTxt_cmd ( *args ) :

    tip = pm.intField ( 'hbsCV_tip_floatField' , q = True , v = True ) ;
    base = pm.intField ( 'hbsCV_base_floatField' , q = True , v = True ) ;

    for i in range  ( 1 , base + 1 ) :
        cmd = "pm.text ( 'hbsCV_percentageText_%d' , e = True , bgc = ( 0.5 , 0.5 , 0.5 ) )" % i ;
        exec ( cmd ) ;

    counter = 1 ;

    for i in range ( base , tip ) :

        r = 0.5 + 0.5/(tip-base)*counter ;
        g = 0.5 + 0.5/(tip-base)*counter ;
        b = 0.5 - 0.5/(tip-base)*counter ;

        #print r , g , b ;

        cmd = "pm.text ( 'hbsCV_percentageText_%d' , e = True , bgc = ( %f , %f , %f ) )" % ( i , r , g , b ) ;
        #print cmd ;
        exec ( cmd ) ;

        counter += 1 ;

    for i in range  ( tip , 101 ) :
        cmd = "pm.text ( 'hbsCV_percentageText_%d' , e = True , bgc = ( 1 , 1 , 0 ) )" % i ;
        exec ( cmd ) ;

    pm.text ( 'hbsCV_percentageText_%d' % tip , e = True , bgc = ( 0.3 , 0.3 , 0.3 ) ) ;
    pm.text ( 'hbsCV_percentageText_%d' % base , e = True , bgc = ( 0.3 , 0.3 , 0.3 ) ) ;

def cvBlendCheckBox_changeCmd ( *args ) :

    if pm.checkBox ( 'hbsCV_cvBlend_checkBox' , q = True , v = True ) == False :

        for i in range  ( 1 , 101 ) :
            cmd = "pm.text ( 'hbsCV_percentageText_%d' , e = True , bgc = ( 0.5 , 0.5 , 0.5 ) )" % i ;
            exec ( cmd ) ;

        pm.intField ( 'hbsCV_tip_floatField' , e = True , enable = False ) ;
        pm.iconTextButton ( 'hbsCV_hairDiagram_image' , e = True , enable = False ) ;
        pm.intField ( 'hbsCV_base_floatField' , e = True , enable = False ) ;

    elif pm.checkBox ( 'hbsCV_cvBlend_checkBox' , q = True , v = True ) == True :

        updatePercentageTxt_cmd ( ) ;

        pm.intField ( 'hbsCV_tip_floatField' , e = True , enable = True ) ;
        pm.iconTextButton ( 'hbsCV_hairDiagram_image' , e = True , enable = True ) ;
        pm.intField ( 'hbsCV_base_floatField' , e = True , enable = True ) ;

def keyEnvelope ( *args ) :

    driver_suffix = pm.textField( 'hbsCV_driver_textField' ,  q = True , text = True );
    driven_suffix = pm.textField( 'hbsCV_driven_textField' ,  q = True , text = True );

    sels = pm.ls(sl=True);
    for sel in sels:
        nameKey = sel.replace(driven_suffix, driver_suffix + '_cvBSH.en');
        pm.select(nameKey);
        pm.setKeyframe(nameKey);

def deleteKeyEnvelope ( *args ) :

    driver_suffix = pm.textField( 'hbsCV_driver_textField' ,  q = True , text = True );
    driven_suffix = pm.textField( 'hbsCV_driven_textField' ,  q = True , text = True );

    sels = pm.ls(sl=True);
    for sel in sels:
        nameKey = sel.replace(driven_suffix, driver_suffix + '_cvBSH.en');
        pm.select(nameKey);
        pm.cutKey(nameKey);

def run ( *args ) :
    
    # check if window exists
    if pm.window ( 'hair_CVBlendshape_UI' , exists = True ) :
        pm.deleteUI ( 'hair_CVBlendshape_UI' ) ;
    else : pass ;
   
    window = pm.window ( 'hair_CVBlendshape_UI', title = title ,
        mnb = True , mxb = False , sizeable = True , rtf = True ) ;
        
    pm.window ( 'hair_CVBlendshape_UI' , e = True , w = width , h = 10 ) ;
    with window :
    
        mainLayout = pm.rowColumnLayout ( nc = 1 ) ;
        with mainLayout :
    
            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2/6 ) , ( 2 , width/2/6*5 ) ] ) :

                    ### stat bar layout ###
                    with pm.rowColumnLayout ( nc = 1 , width = width/2/6  ) :
                        counter = 1 ;
                        for i in range ( 100 , 0 , -1 ) :
                            cmd = "pm.text ( 'hbsCV_percentageText_%d' , label = '' , h = 2 , w = width/10 , bgc = ( 1 , 0 , 0 ) )" % i ;
                            exec ( cmd ) ;

                    ### base/tip and diagram layout ###
                    with pm.rowColumnLayout ( nc = 1 , width = width/10*3 ) :
                        pm.intField ( 'hbsCV_tip_floatField' , h = 30 , width = width/2/6*5 , max = 100 , min = 1 , v = 60 , cc = updatePercentageTxt_cmd ) ;
                        pm.iconTextButton ( 'hbsCV_hairDiagram_image' , image = hairDiagram_path , height = 140 , width = width/2/6*5 ) ;
                        pm.intField ( 'hbsCV_base_floatField' , h = 30 , width = width/2/6*5 , max = 100 , min = 1 ,  v = 30 , cc = updatePercentageTxt_cmd ) ;

            ### set layout ###
                with pm.rowColumnLayout ( nc = 1 , width = width/3 ) :

                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2/2 ) , ( 2 , width/2/2 ) ] ) :

                        pm.text ( label = 'driver suffix' , width = width/2/2 ) ;
                        pm.textField ( 'hbsCV_driver_textField' ,  width = width/2/2 , text = '_ORI' ) ;

                        pm.text ( label = 'driven suffix' , width = width/2/2 ) ;
                        pm.textField ( 'hbsCV_driven_textField' ,  width = width/2/2 , text = '_OTP' ) ;

                    separator ( ) ;

                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2/2 ) , ( 2 , width/2/2 ) ] ) :

                        pm.text ( label = 'envelope' , w = width/2/2 ) ;
                        pm.floatField ( 'hbsCV_envelope_floatField' , w = width/2/2 , v = 1 , pre = 2 ) ;

                    separator ( ) ;

                    pm.checkBox ( 'hbsCV_cvBlend_checkBox' , label = 'cv blend' , v = True , enable = True , h = 20 , cc = cvBlendCheckBox_changeCmd ) ;

                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2/2 ) , ( 2 , width/2/2 ) ] ) :
                        pm.button ( label = 'blendshape' , w = width/2/2 , c = blendShape_cmd , bgc = ( 1 , 1 , 1 ) ) ;
                        pm.button ( label = 'delete blendshape' , w = width/2/2 , c = deleteBlendshape_cmd , bgc = ( 0.745098, 0.745098, 0.745098 ) ) ;

                    separator ( ) ;

                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2/2 ) , ( 2 , width/2/2 ) ] ) :
                        pm.button ( label = 'select curve with cv bsn' , w = width/2 , c = selectCVBSHCurve_cmd ) ;

                    separator ( ) ;
                    separator ( ) ;
                    separator ( ) ;
                    separator ( ) ;
                    separator ( ) ;

                    with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2/2 ) , ( 2 , width/2/2 ) ] ) :
                        pm.button ( label = 'key envelope' , w = width/2/2 , c = keyEnvelope , bgc = ( 0.941176, 0.501961, 0.501961 ) ) ;
                        pm.button ( label = 'delete key envelope' , w = width/2/2 , c = deleteKeyEnvelope , bgc = ( 0.564706, 0.933333, 0.564706 ) ) ;

                    separator ( ) ;

                    # pm.checkBox ( 'hbsCV_selectByEnvelope_checkBox' , label = 'select by envelope' , v = False , enable = True , h = 20 , width = width/2 , cc = updateEnvelopeOpt_cmd ) ;
                    # pm.optionMenu( 'hbsCV_envelope_opt' , label='envelope' , enable = False , w = width/2 ) ;

                    

    updatePercentageTxt_cmd ( ) ;

    window.show () ;