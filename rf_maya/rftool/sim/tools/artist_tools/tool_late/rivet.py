import pymel.core as pm
import maya.cmds as mc

# polyE = pm.ls(sl=True,fl=True)
# eNum1 = polyE[0].index()
# eNum2 = polyE[1].index()
# getGeo = polyE[0].split('.')[0]

# loftTT = mc.createNode('loft')
# for i in range(2):
#     eNum = polyE[i].index()
#     efm = mc.createNode('curveFromMeshEdge') 
#     mc.setAttr( efm +".edgeIndex[0]",eNum)
#     mc.setAttr( efm +".ihi",1)
#     mc.connectAttr( getGeo +".worldMesh[0]",efm+".inputMesh")
#     mc.connectAttr( efm +".outputCurve",loftTT+".inputCurve[{}]".format(i))
    

# pos =mc.createNode('pointOnSurfaceInfo')
# mc.connectAttr( loftTT +".outputSurface",pos +".inputSurface")
# mc.setAttr(pos +".parameterU", 0.5)
# mc.setAttr(pos +".parameterV", 0.5)
# mc.setAttr (pos+ ".turnOnPercentage" ,1)
# trf =mc.createNode( 'transform', n='TRACK_RIV' )
# mc.connectAttr( pos +".result.position",trf +".translate")
# aim = pm.createNode('aimConstraint')
# aim.attr("tg[0].tw").set(1)
# aim.attr("a").set(0 ,1 ,0)
# aim.attr("u").set(0,0,1)
# mc.connectAttr( pos +".result.normal",aim +".target[0].targetTranslate")
# mc.connectAttr( pos +".result.tangentV",aim +".worldUpVector")
# mc.connectAttr( aim +".constraintRotate",trf +".rotate")
def rivetTransform(*args):
	polyE = pm.ls(sl=True,fl=True)
	eNum1 = polyE[0].index()
	eNum2 = polyE[1].index()
	getGeo = polyE[0].split('.')[0]

	loftTT = pm.createNode('loft')
	for i in range(2):
	    eNum = polyE[i].index()
	    efm = pm.createNode('curveFromMeshEdge') 
	    efm.attr("edgeIndex[0]").set(eNum)
	    efm.attr("ihi").set(1)
	    mc.connectAttr( getGeo +".worldMesh[0]",efm+".inputMesh")
	    efm.outputCurve>>loftTT.inputCurve[i]
	    
	    

	pos =pm.createNode('pointOnSurfaceInfo')
	loftTT.outputSurface>>pos.inputSurface

	pos.attr("parameterU").set(0.5)
	pos.attr("parameterV").set(0.5)
	pos.attr("turnOnPercentage").set(1)

	trf =pm.spaceLocator( n = "TRACK_RIV" )
	pos.result.position>>trf.translate
	aim = pm.createNode('aimConstraint')
	aim.attr("tg[0].tw").set(1)
	aim.attr("a").set(0 ,1 ,0)
	aim.attr("u").set(0,0,1)
	pos.result.normal>>aim.target[0].targetTranslate
	pos.result.tangentV>>aim.worldUpVector
	aim.constraintRotate>>trf.rotate
	pm.parent(aim , trf)
	return (trf)
def timeRangeQuery () :
    
    startFrame = mc.playbackOptions ( q = True , minTime = True ) ; 
    endFrame = mc.playbackOptions ( q = True , maxTime = True ) ;
    
    return ( startFrame , endFrame ) ;

stFrm,EdFrm = timeRangeQuery()  
  
defTck = pm.spaceLocator( n = "defTrack_RIV" )
rvT.t>>defTck.t
rvT.r>>defTck.r
pm.bakeResults(defTck,simulation=True,t=( stFrm-50 , EdFrm+50 ))
mc.curve(d=1,p=[(1,0,1),(1,0,0)])
crv = mc.curve( d= 1 ,p = [(1,0,1),(3,0,1),(3,0,2),(5,0,0),(3,0,-2),(3,0,-1),(1,0,-1),(1,0,-3),(2,0,-3),(0,0,-5),(-2,0,-3),(-1,0,-3),(-1,0,-1),(-3,0,-1),(-3,0,-2),(-5,0,0),(-3,0,2),(-3,0,1),(-1,0,1),(-1,0,3),(-2,0,3),(0,0,5),(2,0,3),(1,0,3),(1,0,1)])
ctrl = mc.rename(crv,"Track_Ctrl")
gmb = mc.duplicate(ctrl,n="Track_Gimble")
gimShape = mc.listRelatives(gmb, ad = True , typ = "shape")[0]

mc.xform('{}.cv[:]'.format(gimShape),s=(0.75,0.75,0.75))
mc.parent(gmb , ctrl)
crvGrp = mc.group( ctrl ,n = "Track_GRP"  )


import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel;

def rivetTransform(*args):
	polyE = pm.ls(sl=True,fl=True)
	eNum1 = polyE[0].index()
	eNum2 = polyE[1].index()
	getGeo = polyE[0].split('.')[0]

	loftTT = pm.createNode('loft')
	for i in range(2):
	    eNum = polyE[i].index()
	    efm = pm.createNode('curveFromMeshEdge') 
	    efm.attr("edgeIndex[0]").set(eNum)
	    efm.attr("ihi").set(1)
	    mc.connectAttr( getGeo +".worldMesh[0]",efm+".inputMesh")
	    efm.outputCurve>>loftTT.inputCurve[i]
	    
	    

	pos =pm.createNode('pointOnSurfaceInfo')
	loftTT.outputSurface>>pos.inputSurface

	pos.attr("parameterU").set(0.5)
	pos.attr("parameterV").set(0.5)
	pos.attr("turnOnPercentage").set(1)

	trf =pm.spaceLocator( n = "TRACK_RIV" )
	pos.result.position>>trf.translate
	aim = pm.createNode('aimConstraint')
	aim.attr("tg[0].tw").set(1)
	aim.attr("a").set(0 ,1 ,0)
	aim.attr("u").set(0,0,1)
	pos.result.normal>>aim.target[0].targetTranslate
	pos.result.tangentV>>aim.worldUpVector
	aim.constraintRotate>>trf.rotate
	pm.parent(aim , trf)
	rvTShape = mc.listRelatives("{}".format(trf), ad = True , typ = "shape")[0]
	return trf
rvT = rivetTransform()

def timeRangeQuery () :
    
    startFrame = mc.playbackOptions ( q = True , minTime = True ) ; 
    endFrame = mc.playbackOptions ( q = True , maxTime = True ) ;
    
    return ( startFrame , endFrame ) ;

stFrm,EdFrm = timeRangeQuery()  

defTck = pm.spaceLocator( n = "defTrack_RIV" )
DT = mc.spaceLocator( n = "delete_Tack" )
mc.delete(mc.parentConstraint("{}".format(rvT),DT,mo = False))
pm.parent(DT , rvT)
mc.delete(mc.pointConstraint(DT,"{}".format(defTck),mo = False))
mc.parentConstraint(DT,"{}".format(defTck),mo = True)
pm.bakeResults(defTck,simulation=True,t=( stFrm-50 , EdFrm+50 ))
still =mc.createNode( 'transform', n='still_RIV_GRP' )
pm.parent(defTck , still)
mc.delete(DT)

crv = mc.curve( d= 1 ,p = [(1,0,1),(3,0,1),(3,0,2),(5,0,0),(3,0,-2),(3,0,-1),(1,0,-1),(1,0,-3),(2,0,-3),(0,0,-5),(-2,0,-3),(-1,0,-3),(-1,0,-1),(-3,0,-1),(-3,0,-2),(-5,0,0),(-3,0,2),(-3,0,1),(-1,0,1),(-1,0,3),(-2,0,3),(0,0,5),(2,0,3),(1,0,3),(1,0,1)])
ctrl = mc.rename(crv,"Track_GRP")
gmb = mc.duplicate(ctrl,n="Track_Gimble")
gimShape = mc.listRelatives(gmb, ad = True , typ = "shape")[0]
mc.xform('{}.cv[:]'.format(gimShape),s=(0.75,0.75,0.75))
ctrlShape = mc.listRelatives(ctrl, ad = True , typ = "shape")[0]
pm.select(cl = True)
jnt = mc.joint( n='Track_Ctrl')
mc.setAttr(jnt +".drawStyle", 2)
pm.select(ctrlShape, r=True)
pm.select(jnt, add=True)
eval = mel.eval("parent -r -s")
mc.parent(jnt , ctrl)
mc.parent(gmb , jnt)
mc.setAttr(jnt+".radi",k = False ,cb = False)
mc.addAttr(ctrlShape,ln = "Gimble",at = "double",dv = 0, k = True)
mc.connectAttr( ctrlShape +".Gimble",gmb[0] +".visibility")
xyz = ['TranX','TranY','TranZ','RolX','RolY','RolZ']
longxyz=['translateX','translateY','translateZ','rotateX','rotateY','rotateZ']
for each in range(len(xyz)):
    mc.addAttr(jnt,ln = xyz[each],at = "double",dv = 0, k = True )
    blendC = mc.createNode("blendColors",n = "Track_{}_BC".format(xyz[each]))
    mc.connectAttr( jnt +".{}".format(xyz[each]),blendC +".blender")
    mc.connectAttr( defTck +".{}".format(longxyz[each]),blendC +".color1.color1R")
    mc.connectAttr( blendC +".output.outputR",ctrl +".{}".format(longxyz[each]))


