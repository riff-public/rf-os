import maya.cmds as mc ;
import maya.mel as mm ;
import sys ;
import os
import pymel.core as pm ;

from rf_maya.rftool.sim.tools.simtools.track.absoluteTrack import Track
reload( Track )
from rf_maya.rftool.sim.tools.rig import rivet ;
reload ( rivet ) ;

#--------------------export----------------------------
def export_cache_still(outputPath, exportGrp) :

    file_path = mc.file(q=True, loc=True) 
    cachefile = file_path.split('maya')[0]

    split_filename = '%s'%exportGrp[0]
    tt = split_filename.split(':')[1]
    # tt = watari_001_sim:TechGeo_Grp
    filename = 'trk_'+tt+'.abc'
    # filename = trk_TechGeo_Grp
    cachefile = cachefile+'cache/'+filename
    # cachefile = P:/Bikey/scene/work/ep1/bk_ep1_q0120_s0130/sim/watari_001/cache/trk_TechGeo_Grp.abc
    pluginName = 'AbcExport.mll'

    if not mc.pluginInfo(pluginName, q=True, l=True):
        mc.loadPlugin(pluginName, qt=True)

    # frame range
    start = int(mc.playbackOptions ( q = True , minTime = True ))
    end = int(mc.playbackOptions ( q = True , maxTime = True ))


    # cmds
    options = []
    options.append('-frameRange %s %s' % (start, end))
    options.append('-uvWrite')
    options.append('-writeUVSets')
    options.append('-writeVisibility')
    options.append('-worldSpace')
    # options.append('-noNormals')
    options.append('-dataFormat ogawa')
    options.append('-root ABC_Sim|%s' % exportGrp[0])
    #bk_ep1_q0120_s0130_sim_watari_001_tech.hero

    output = export_alembic_geo(cachefile, options)
    return cachefile,filename

def export_alembic_geo(filename, options):
    options.append('-file %s' % filename)
    optionCmd = (' ').join(options)
    cmd = 'AbcExport -j "%s";' % optionCmd
    result = mm.eval(cmd)
    
#--------------------import----------------------------

def import_abc(path):
    """ import alembic file """

    cmd = 'file -import -type "Alembic"  -ignoreVersion -ra true -mergeNamespacesOnClash false -namespace "cache"  -pr  -importTimeRange "combine" "%s";' % path
    mm.eval(cmd)
    

def rivet_cmd ( *args ) :

    rivet.rivet ( ) ;
##########################################################################################################################


######################*run*############################

edge1,edge2 = mc.ls( sl = True );
split_ns = edge1.split('_');
splitedge1 = edge1.split(':');
splitedge2 = edge2.split(':');
ns = split_ns[0]+'_'+split_ns[1]
ns_edge1 = ns +':'+ splitedge1[1]
ns_edge2 = ns +':'+ splitedge2[1]

edge_anim  = ns_edge1,ns_edge2

Track.rivetTransform()
anim = mc.select(edge_anim)
rivet_cmd()
mc.setAttr('ABC_Sim.translateX', lock= True)
mc.setAttr('ABC_Sim.translateY', lock= True)
mc.setAttr('ABC_Sim.translateZ', lock= True)
mc.setAttr('ABC_Sim.rotateX', lock= True)
mc.setAttr('ABC_Sim.rotateY', lock= True)
mc.setAttr('ABC_Sim.rotateZ', lock= True)

mc.setAttr ("TRACK_TFM_BWD_ctrl.transX" ,0);
mc.setAttr ("TRACK_TFM_BWD_ctrl.transY" ,0);
mc.setAttr ("TRACK_TFM_BWD_ctrl.transZ" ,0);
mc.setAttr ("TRACK_TFM_BWD_ctrl.rotX" ,0);
mc.setAttr ("TRACK_TFM_BWD_ctrl.rotY" ,0);
mc.setAttr ("TRACK_TFM_BWD_ctrl.rotZ" ,0);

mc.parent('ABC_Sim', 'TRACK_TFM_BWD');

if 'Groom' in edge1 :
    cache_sim = mc.ls('*_sim:TechGeo_Grp')

    file_path = mc.file(q=True, loc=True) 
    cachefile = file_path.split('maya')[0]
    cachefile = cachefile+'cache'
    
    outputPath,filename = export_cache_still(cachefile,cache_sim)
    
    import_abc(outputPath)
    
    cache = mc.ls('cache:TechGeo_Grp')
    print cache

else:
    
    cache_sim = mc.ls('*_sim:Geo_Grp')


    file_path = mc.file(q=True, loc=True) 
    cachefile = file_path.split('maya')[0]
    cachefile = cachefile+'cache'

    outputPath,filename = export_cache_still(cachefile,cache_sim)
    import_abc(outputPath)
    cache = mc.ls('cache:Geo_Grp')



mc.group(cache)
# mc.rename(filename,filename+'_cache')

mc.currentTime(1001) 
mc.parent('group1', 'rivet1');