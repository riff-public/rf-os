import pymel.core as pm
import maya.cmds as mc
import os
from rf_maya.rftool.simulation.sim.sim_preRoll import LWSnap as lw
reload(lw)

def SetKeyTpose(ctrlList = [] ,ns = '', *args):
	# print 'aaaaaaa'
	avoidCtrl = [ns+':Root_Ctrl' , ns+':AllMover_Ctrl' , ns+':Offset_Ctrl']
	avoidAttr = ['scale' , 'Scale' , 'Geo' , 'geo' , 'localWorld' , 'spaceFollow' , 'fkIk']
	for eachsetTpose in ctrlList:
		attrlist = mc.listAttr(eachsetTpose, k = True ,u = True, v= True)
		if not attrlist:
			pass
		else:
			if eachsetTpose in avoidCtrl :
				pass
			else:
				for each_attr in attrlist:
					checkCount = 0
					for check in avoidAttr:
						if  check in each_attr:
							checkCount = 1
					if checkCount == 0:
						mc.setAttr(eachsetTpose + '.' + each_attr , 0)
						# print eachsetTpose + '.' + each_attr+'not-------------------------------------------------------------'
	# print ctrlList
	mc.setKeyframe(ctrlList ,itt='flat',ott='flat')


def timeRangeQuery () :
	
	startFrame = mc.playbackOptions ( q = True , minTime = True ) ; 
	endFrame = mc.playbackOptions ( q = True , maxTime = True ) ;
	
	return ( startFrame , endFrame ) ;

def run(selectItem = [] , *args):
	stFrm,EdFrm = timeRangeQuery() 
	list_char = selectItem
	print stFrm,EdFrm	

	for each in list_char :
		PRE_LOC = mc.spaceLocator(name = "positionPreroll_Loc")[0]
		ns = each.split(":")[0]
		print ns
		ctrlList = mc.ls('{}:*_Ctrl'.format(ns)) 
		print ctrlList
		mc.bakeResults(ctrlList, 
						simulation=True, 
						t=(stFrm,EdFrm), 
						sampleBy=1, 
						oversamplingRate=1, 
						disableImplicitControl=True, 
						preserveOutsideKeys=True, 
						sparseAnimCurveBake=False, 
						removeBakedAttributeFromLayer=False, 
						removeBakedAnimFromLayer=False, 
						bakeOnOverrideLayer=False, 
						minimizeRotation=True, 
						controlPoints=False, 
						shape=True)

		mc.cutKey(ctrlList,clear=1,time = (0,1000))
		root = ns + ':Root_Ctrl'
		mc.parentConstraint(root, PRE_LOC ,mo = False)
		mc.currentTime(1001)
		PRE_LOCS_X = mc.getAttr(PRE_LOC +'.tx' )
		PRE_LOCS_Y = mc.getAttr(PRE_LOC +'.ty' )
		PRE_LOCS_Z = mc.getAttr(PRE_LOC +'.tz' )
		mc.currentTime(1002)
		PRE_LOCE_X = mc.getAttr(PRE_LOC +'.tx' )
		PRE_LOCE_Y = mc.getAttr(PRE_LOC +'.ty' )
		PRE_LOCE_Z = mc.getAttr(PRE_LOC +'.tz' )
		PRE_LOC_X = PRE_LOCE_X - PRE_LOCS_X
		PRE_LOC_Y = PRE_LOCE_Y - PRE_LOCS_Y
		PRE_LOC_Z = PRE_LOCE_Z - PRE_LOCS_Z
		resultX = PRE_LOC_X * 10 
		resultY = PRE_LOC_Y * 10 
		resultZ = PRE_LOC_Z * 10
		allmove = ns +':AllMover_Ctrl'
		allmove_X = mc.getAttr(allmove +'.tx' )
		allmove_Y = mc.getAttr(allmove +'.ty' )
		allmove_Z = mc.getAttr(allmove +'.tz' )
		mc.currentTime(991)
		mc.setAttr(allmove + '.tx' , allmove_X - resultX)
		mc.setAttr(allmove + '.ty' , allmove_Y - resultY)
		mc.setAttr(allmove + '.tz' , allmove_Z - resultZ)
		mc.delete(PRE_LOC)
		mc.currentTime(981)       
		mc.setKeyframe(ctrlList)
		mc.currentTime(980)
		print '1'
		
		for i in ctrlList:
			if mc.objExists(i+'.localWorld'):
				lw.swapToLocal(i)
				mc.setKeyframe(i,s=0,itt='flat',ott='flat')
			elif mc.objExists(i+'.spaceFollow') :
				mc.getAttr(i +'.spaceFollow' )
				grp = mc.createNode('transform')
				mc.delete(mc.parentConstraint(i,grp,mo= False))
				mat = mc.xform(grp ,q = True ,ws = True , m = True )
				mc.setAttr(i +'.spaceFollow',1)
				mc.xform(i,ws = True , m = mat)
				mc.setKeyframe(i,s=0,itt='flat',ott='flat')
				mc.delete(grp)
		mc.currentTime(971)
		print '2'
		SetKeyTpose(ctrlList = ctrlList, ns = ns)
		print '3'

		# mc.cutKey(ctrlList,clear=1,time = (EdFrm+1,EdFrm+10))
		POS_LOC = mc.spaceLocator(name = "positionPosroll_Loc")[0]
		mc.parentConstraint(root, POS_LOC ,mo = False)
		mc.currentTime(EdFrm)

		POS_LOCS_X = mc.getAttr(POS_LOC +'.tx' )
		POS_LOCS_Y = mc.getAttr(POS_LOC +'.ty' )
		POS_LOCS_Z = mc.getAttr(POS_LOC +'.tz' )
		mc.currentTime(EdFrm - 1)
		POS_LOCE_X = mc.getAttr(POS_LOC +'.tx' )
		POS_LOCE_Y = mc.getAttr(POS_LOC +'.ty' )
		POS_LOCE_Z = mc.getAttr(POS_LOC +'.tz' )
		POS_LOC_X = POS_LOCS_X - POS_LOCE_X
		POS_LOC_Y = POS_LOCS_Y - POS_LOCE_Y
		POS_LOC_Z = POS_LOCS_Z - POS_LOCE_Z
		resultX = POS_LOC_X * 10 
		resultY = POS_LOC_Y * 10 
		resultZ = POS_LOC_Z * 10
		print '4'
		allmove = ns +':AllMover_Ctrl'
		allmove_X = mc.getAttr(allmove +'.tx' )
		allmove_Y = mc.getAttr(allmove +'.ty' )
		allmove_Z = mc.getAttr(allmove +'.tz' )
		mc.currentTime(EdFrm + 11)
		mc.setAttr(allmove + '.tx' , allmove_X + resultX)
		mc.setAttr(allmove + '.ty' , allmove_Y + resultY)
		mc.setAttr(allmove + '.tz' , allmove_Z + resultZ)
		mc.delete(POS_LOC)
