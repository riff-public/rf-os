from rftool.on2 import on2_utils as on2
reload(on2)
import pymel.core as pm
import maya.cmds as mc

file_path = mc.file(q=True, loc=True)
episode = file_path.split('/')[4]
shot_entity = file_path.split('/')[5]
char_namespace = file_path.split('/')[7]

# # data file 
src = "P:/Bikey/scene/publ/" + episode + "/"+ shot_entity +"/_data/on2Data/"+ shot_entity +"_on2data.hero.yml"
print src
# namespace from anim 
# ns = 'kaiCasual_001'
node = on2.import_data(src, namespaces= char_namespace, connect=True)

# node.connect()
# # namespace sim 
sim_ns = char_namespace +'_sim'
print sim_ns
on2.connect_retime_cache(sim_ns, pm.PyNode(node.name))