import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel;
import os , shutil
import rf_config as config

from rf_utils.context import context_info
from rf_utils import register_shot
reload(context_info)

def createFolderTxt( *args ):

    myDocuments = os.path.expanduser('~');          
    myDocuments = myDocuments.replace('\\', '/');
    # C:/Users/Legion/Documents
    prefPath = myDocuments + '/lateScriptData/pathInfo';
    if os.path.exists( prefPath ) == False:
        os.makedirs( prefPath );
    else : pass;

    return prefPath;

def saveDefaultPath( *args ):

    prefPath = createFolderTxt( );
    inputPathTxt = open( prefPath + '/inputDocPath.txt' , 'w+' );
    pathName = pm.textField( 'inputDocPath' , q = True , text = True );
    inputPathTxt.write( pathName );

    inputPathTxt.close( );

    return(pathName)

def browsePath( *args ):

    try : 
        startPath = pm.textField( 'inputDocPath' , q = True , text = True );
    except :
        startPath = '/C:';

    browsePath = mc.fileDialog2( ds = 2 , fm = 3 , startingDirectory = startPath , okc = 'select directory' )[0];
 
    if browsePath[-1] != '/':
        browsePath += '/';

    pm.textField( 'inputDocPath' , e = True , text = browsePath );

    saveDefaultPath();

def getDefaultPath( *args ):

    prefPath = createFolderTxt();
    pathname = ""; 
    if os.path.isfile( prefPath + '/inputDocPath.txt' ):
        pathTxt = open( prefPath + '/inputDocPath.txt', 'r' );
        pathname = pathTxt.read();
        pathTxt.close();
    else :
        pathTxt = open( prefPath + '/inputDocPath.txt', 'w' );
        pathTxt.write("");
        pathTxt.close();

    return pathname;

class TextScrollList ( object ) :

    def __init__ ( self, textScrollList ) :
        self.textScrollList = textScrollList ;

    def append ( self , item ) :
        self.item = item ;
        pm.textScrollList ( self.textScrollList , e = True , append = self.item ) ;

    def clear ( self ) :
        pm.textScrollList ( self.textScrollList , e = True , ra = True ) ; 

    def currentItem ( self ) :

        currentItem = pm.textScrollList ( self.textScrollList , q = True , selectItem = True ) ;
        
        if currentItem == [] :
            return None ;
        else :
            if len ( currentItem ) == 1 :

                return currentItem [0] ;

            else :

                return ( currentItem ) ;

    def selectItem ( self , selectItem ) :
        self.selectItem = selectItem ;
        pm.textScrollList ( self.textScrollList , e = True , selectItem = self.selectItem ) ;

def browserClassChange_cmd ( *args ) :
    browserClass = pm.optionMenu( 'browserClass_optionMenu' ,v=True,q = True ) 
    act_textScrollList         = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList     = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    cache_textScrollList         = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;
    character_textScrollList         = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    act_textScrollList.clear ( )
    sequence_textScrollList.clear ( )
    cache_textScrollList.clear ( )
    character_textScrollList.clear ( )
    project = pm.optionMenu( 'Project' ,e=True,v = 'Select Project')
    if browserClass == 'server':
        RFPROJECT = config.Env.projectVar
        path = os.environ[RFPROJECT]
    elif browserClass == 'local':
        path = getDefaultPath()

    
    return(path)

def ChangeProject_cmd(*args):

    browserClass = pm.optionMenu( 'browserClass_optionMenu' ,v=True,q = True ) 

    if browserClass == 'server':
        RFPROJECT = config.Env.projectVar
        path = os.environ[RFPROJECT]
    elif browserClass == 'local':
        path = getDefaultPath()

    project = pm.optionMenu( 'Project' ,v=True,q = True ) 
    if path == os.environ[config.Env.projectVar]:
        context = context_info.Context()
        context.update(project = project, entityType='scene')
        scene = context_info.ContextPathInfo(context=context)
        pathProject = scene.path.publish()
        pathProject = pathProject.replace(pathProject.split('/')[0], path)

    else :
        pathProject = path + project 

    listAct_cmd()
    return(pathProject)

def listdir ( elem = '' ,*args ):

    browserClass = pm.optionMenu( 'browserClass_optionMenu' ,v=True,q = True ) 

    if browserClass == 'server':
        RFPROJECT = config.Env.projectVar
        path = os.environ[RFPROJECT]
    elif browserClass == 'local':
        path = getDefaultPath()

    project = pm.optionMenu( 'Project' ,v=True,q = True ) 
    if path == os.environ[config.Env.projectVar]:
        context = context_info.Context()
        context.update(project = project, entityType='scene')
        scene = context_info.ContextPathInfo(context=context)
        target_path = scene.path.publish()
        target_path = target_path.replace(target_path.split('/')[0], path)

    else :
        target_path = path + project 

    browserClass = pm.optionMenu( 'browserClass_optionMenu' ,v=True,q = True ) 
    act_textScrollList         = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList     = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;

    if elem == 'act' : 
        return os.listdir ( target_path ) ;
    elif elem == 'sequence':
        actSelect = act_textScrollList.currentItem()
        target_path = target_path + '/' + actSelect
        Sequence = os.listdir ( target_path )
        return Sequence
    elif elem == 'cache' :
        sequenceSelect = sequence_textScrollList.currentItem ( ) ;
        actSelect = act_textScrollList.currentItem()
        target_path = target_path + '/' + actSelect+ "/" + sequenceSelect + "/output"
        # print target_path
        outputCahe = os.listdir ( target_path )
        

        return outputCahe,target_path

def listAct_cmd(*args ):

    act = listdir("act")
    act.sort()
    act_textScrollList         = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    act_textScrollList.clear()
    for item in act:
        act_textScrollList.append(item)
    
def listSequence_cmd(*args ):

    sequence_textScrollList     = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    sequence_textScrollList.clear ( ) ;

    sequenceItem_list = listdir ( 'sequence' ) ;
    sequenceItem_list.sort ( ) ;
    for item in sequenceItem_list :
        sequence_textScrollList.append ( item ) ;

def listcahe_cmd(*args):

    if pm.optionMenu( 'browserClass_optionMenu' ,v=True,q = True ) == 'local':
        cache_textScrollList         = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;
        cache_textScrollList.clear ( )
        listCharecterInLocal_cmd()
        return
    cache_textScrollList         = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;
    cache_textScrollList.clear ( )
    cacheItem_list,scene_path = listdir ( 'cache' ) ;
    listCharecterInLocal_cmd()
    scene = context_info.ContextPathInfo(path = scene_path)
    reg = register_shot.Register(scene)
    asset_list = reg.get.asset_list()
    for asset in asset_list: 
        path_aseet_des = reg.get.asset(asset)['description']
        context = context_info.ContextPathInfo(path = path_aseet_des)
        
        file_type = reg.get.asset(asset)['type']

        asset_type = context.type
        getType = pm.optionMenu( 'cacheClass_optionMenu' ,v=True,q = True )
        if file_type != "yeti_cache":
            if getType == asset_type:
                if  "_Tech" in asset:
                    pass
                else:
                    cache_textScrollList.append ( asset ) ;

def directory_cmd ( path = '' , *args ) :

    if os.path.exists ( path ) == False :
        os.makedirs ( path ) ;
    else : pass ;

    return ( path ) ;

def autoNaming_cmd(*args ):

    localProject_path = getDefaultPath()
    project = pm.optionMenu( 'Project' ,v=True,q = True ) 
    act_textScrollList         = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList     = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    cache_textScrollList         = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;

    sequenceSelect = sequence_textScrollList.currentItem ( ) ;
    actSelect = act_textScrollList.currentItem()
    cacheSelect = cache_textScrollList.currentItem()
    # print cacheSelect

    checkBox =  pm.checkBox ( 'cache_cbx',q =True , v= True )

    if checkBox  == True  :
        cacheItem_list,scene_path = listdir ( 'cache' ) ;
        cache_path = scene_path + '/'+cacheSelect
        checkBox_path = localProject_path + project+ '/'+ actSelect + '/' + sequenceSelect + '/' ;
        directory_cmd ( checkBox_path + 'collide' ) ;
        listItem = os.listdir(cache_path)
        for each in listItem:
            if 'layout' in each:
                pass
            else:
                source = cache_path + '/' + each
                destination = checkBox_path+"/collide"
                shutil.copy2(source , destination)
    else :
        autonaming_path = directory_cmd ( localProject_path + project+ '/'+ actSelect + '/' + sequenceSelect + '/' + cacheSelect + '/' ) ;
        # print autonaming_path
        # copy ( self_path + 'asset/workspace.mel' , autonaming_path ) ; 
        fid = open(autonaming_path +'workspace.mel','w') ###############for set project##############
        fid.close()

        ############################create folder in local################################
        directory_cmd ( autonaming_path + 'data' ) ;
        directory_cmd ( autonaming_path + 'version' ) ;
        directory_cmd ( autonaming_path + 'cache' ) ;

        copySetup_cmd(cacheSelect)
        copycache_cmd()
    listCharecterInLocal_cmd()
    print "FIN"



def copycache_cmd(*args ):

####################### copy cam#########################

    localProject_path = getDefaultPath()
    project = pm.optionMenu( 'Project' ,v=True,q = True ) 
    act_textScrollList         = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList     = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    cache_textScrollList         = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;
    sequenceSelect = sequence_textScrollList.currentItem ( ) ;
    actSelect = act_textScrollList.currentItem()
    cacheSelect = cache_textScrollList.currentItem()
    autonaming_path = directory_cmd ( localProject_path + project+ '/'+ actSelect + '/' + sequenceSelect + '/' + cacheSelect + '/' ) ;

    cacheItem_list,scene_path = listdir ( 'cache' ) ;
    listItemOutput = os.listdir(scene_path)

    for each in listItemOutput:
        if 'cam.hero.abc' in each:
            source = scene_path + '/' + each
            destination = autonaming_path+"/data"
            shutil.copy2(source , destination)

###################copy select hero ######################

    cache_path = scene_path + '/'+cacheSelect 
    listItem = os.listdir(cache_path)
    if not listItem :
        print "don't have cache file"
        return
    for each in listItem:
        if 'layout' in each:
            pass
        else:
            source = cache_path + '/' + each
            destination = autonaming_path+"/data"
            shutil.copy2(source , destination)

    cacheTech_path = scene_path + '/'+cacheSelect+"_Tech"
    if checkpath_cmd(cacheTech_path) == False:
        return
    listItem_fortech = os.listdir(cacheTech_path)
    if not listItem_fortech :
        print "don't have cache Tech file"
        return
    for i in listItem_fortech:
        if 'layout' in i:
            pass
        else:
            source = cacheTech_path + '/' + i
            destination = autonaming_path+"/data"
            shutil.copy2(source , destination)

def copySetup_cmd (asset,*args ):

    localProject_path = getDefaultPath()
    project = pm.optionMenu( 'Project' ,v=True,q = True ) 
    act_textScrollList         = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList     = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    cache_textScrollList         = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;
    sequenceSelect = sequence_textScrollList.currentItem ( ) ;
    actSelect = act_textScrollList.currentItem()
    cacheSelect = cache_textScrollList.currentItem()
    autonaming_path = directory_cmd ( localProject_path + project+ '/'+ actSelect + '/' + sequenceSelect + '/' + cacheSelect + '/' ) ;
    version_path = autonaming_path +'version/'
    
    browserClass = pm.optionMenu( 'browserClass_optionMenu' ,v=True,q = True ) 

    if browserClass == 'server':
        RFPROJECT = config.Env.projectVar
        path = os.environ[RFPROJECT]
    elif browserClass == 'local':
        path = getDefaultPath()

    project = pm.optionMenu( 'Project' ,v=True,q = True ) 

    allItem,scene_path = listdir ( 'cache' )
    scene = context_info.ContextPathInfo(path = scene_path)
    reg = register_shot.Register(scene)
    path_aseet_des = reg.get.asset(asset)['description']
    context = context_info.ContextPathInfo(path = path_aseet_des)
    asset_type = context.type
    name = asset.split("_")[0]
    pathSetupsim = path+'/' +project+'/asset/publ/'+asset_type+'/'+name+'/sim'
    if checkpath_cmd(pathSetupsim) == False:
        return
    list_setup = os.listdir(pathSetupsim)
    if not list_setup:
        print "pls set up flie first"
        return
    for each in list_setup:
        pathHero = pathSetupsim+'/'+ each +'/hero/output'
        if each == 'main':
            pass
        else:
            list_Hero = os.listdir(pathHero)
            for setUp in list_Hero:
                if 'hair' in setUp or 'Hair' in setUp :
                    directory_cmd ( version_path + 'hair' ) ;
                    source = pathHero + '/' + list_Hero[0]
                    destination = autonaming_path+"/version/hair"
                    shutil.copy2(source , destination)

                else :
                    directory_cmd ( version_path + 'cloth' ) ;
                    source = pathHero + '/' + list_Hero[0]
                    destination = autonaming_path+"/version/cloth"
                    shutil.copy2(source , destination)

    return(autonaming_path)

def checkpath_cmd(path):

    if not os.path.exists(path) :
        print "path {} do not exist".format(path)
        
        return False

def listCharecterInLocal_cmd(*args ):

    localProject_path = getDefaultPath()
    project = pm.optionMenu( 'Project' ,v=True,q = True ) 
    act_textScrollList         = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList     = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    cache_textScrollList         = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;
    character_textScrollList         = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    sequenceSelect = sequence_textScrollList.currentItem ( ) ;
    actSelect = act_textScrollList.currentItem()
    cacheSelect = cache_textScrollList.currentItem()
    character_textScrollList.clear()
    if os.path.exists(localProject_path + project+ '/'+ actSelect + '/' + sequenceSelect + '/') : 
        character_list = os.listdir(localProject_path + project+ '/'+ actSelect + '/' + sequenceSelect + '/')
        if character_list : 
            for each in character_list:
                character_textScrollList.append(each)

def setProject_cmd(*args ):

    localProject_path = getDefaultPath()
    project = pm.optionMenu( 'Project' ,v=True,q = True ) 
    act_textScrollList         = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList     = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    cache_textScrollList         = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;
    character_textScrollList         = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    sequenceSelect = sequence_textScrollList.currentItem ( ) ;
    actSelect = act_textScrollList.currentItem()
    cacheSelect = cache_textScrollList.currentItem()
    characterSelect = character_textScrollList.currentItem()
    setProject_path = localProject_path + project+ '/'+ actSelect + '/' + sequenceSelect + '/'+characterSelect
    # print setProject_path
    try :
        pm.mel.eval( 'setProject "%s"' % setProject_path ) ;
    except :
        pm.error ( 'your input directory is not a maya project' ) ;

def browserDoubleClick_cmd(*args ):

    browserClass = pm.optionMenu( 'browserClass_optionMenu' ,v=True,q = True ) 

    if browserClass == 'server':
        RFPROJECT = config.Env.projectVar
        path = os.environ[RFPROJECT]
    elif browserClass == 'local':
        path = getDefaultPath()

    localProject_path = getDefaultPath()
    project = pm.optionMenu( 'Project' ,v=True,q = True ) 
    act_textScrollList         = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList     = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    cache_textScrollList         = TextScrollList ( textScrollList = 'cacheFile_textScrollList' ) ;
    character_textScrollList         = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    sequenceSelect = sequence_textScrollList.currentItem ( ) 
    actSelect = act_textScrollList.currentItem()
    cacheSelect = cache_textScrollList.currentItem()
    characterSelect = character_textScrollList.currentItem()

    if path == os.environ[config.Env.projectVar]:
        if actSelect == None:
            pass
        else:
            path_open = path +'/'+ project + '/scene/publ/'+ actSelect
            
            if sequenceSelect == None:
                pass
            else :
                path_open += '/'+ sequenceSelect
                
                if cacheSelect == None:
                    pass
                else:
                    path_open += '/output/'+cacheSelect
                    # print path_open
    else:
        if actSelect == None:
            pass
        else:
            path_open = localProject_path +'/'+ project + '/'+ actSelect
            
            if sequenceSelect == None:
                pass
            else :
                path_open += '/'+ sequenceSelect
                

    os.startfile(path_open)

def charDoubleClick_cmd(*args ):
    localProject_path = getDefaultPath()
    project = pm.optionMenu( 'Project' ,v=True,q = True ) 
    act_textScrollList         = TextScrollList ( textScrollList = 'act_textScrollList' ) ;
    sequence_textScrollList     = TextScrollList ( textScrollList = 'sequence_textScrollList' ) ;
    character_textScrollList         = TextScrollList ( textScrollList = 'character_textScrollList' ) ;
    sequenceSelect = sequence_textScrollList.currentItem ( ) 
    actSelect = act_textScrollList.currentItem()
    characterSelect = character_textScrollList.currentItem()

    path_open = localProject_path +'/'+ project + '/'+ actSelect+ '/'+ sequenceSelect+'/'+ characterSelect

    os.startfile(path_open)

###################################### run UI###############################################

def run (*args ):

    title = 'autoProjectLocal' ;

    width = 700.00 ;
    textScrollHeight = 200.00 ;
    if pm.window ( 'autoProjectLocal_UI11' , exists = True ) :
        pm.deleteUI ( 'autoProjectLocal_UI11' ) ;
        
    window = pm.window ( 'autoProjectLocal_UI11', title = title ,mnb = True , mxb = False , sizeable = True , rtf = True ) ;
    window = pm.window ( 'autoProjectLocal_UI11' , e = True , w = width , h = 100 ) ;

    with window:
        mainLayout = pm.rowColumnLayout ( nc = 1 , w = width) ;
        pm.setParent()
        with mainLayout :

            with pm.frameLayout ( label = 'SET DRIVE' , w = width , bgc = ( 0.6 , 0.4 , 0.4 ) ) :
                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/6*5 ) , ( 2 , width/6*1 ) ] ) :
                    localPath = getDefaultPath();
                    
                    if localPath != '':
                        pm.textField ( 'inputDocPath' , text = localPath);
                    else:
                        pm.textField ( 'inputDocPath' , text = '');
                    pm.button ( label = 'browse' , c = browsePath ) ;
            with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                with pm.optionMenu ( 'browserClass_optionMenu' , label='browser class' ,cc =browserClassChange_cmd, w = width/2 ) :
                        pm.menuItem ( label='server' ) ;
                        pm.menuItem ( label='local' ) ;
            with pm.frameLayout ( label = 'Project' , w = width , bgc = ( 0.5 , 0.5 , 0.4 ) ) :
                with pm.rowColumnLayout ( nc = 1 , w = width ) :
                    with pm.optionMenu( 'Project' , w = width,cc = ChangeProject_cmd ) :
                        
                        pm.menuItem ( label= 'Select Project' ) ;
                        pm.menuItem ( label= 'Hanuman' ) ;
                        pm.menuItem ( label= 'Bikey' ) ;
                        pm.menuItem ( label= 'Atama' ) ;
                        pm.menuItem ( label= 'Crusher' ) ;
                        # pm.menuItem ( label= 'QuantumTest' ) ;

            with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/3 ) , ( 2 , width/3 ) , ( 3 , width/3 )  ] ) :

                 with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :
                    pm.text ( label = 'action' , font = 'boldLabelFont' , bgc =  ( 1 , 0.6 , 0.4 ) , width = width/3 ) ;
                    pm.textScrollList ( 'act_textScrollList' , ams = False , h = textScrollHeight , width = width/3 ,sc = listSequence_cmd ,dcc = browserDoubleClick_cmd )

                 with pm.rowColumnLayout ( nc = 1 , w = width/3 ) :
                    pm.text ( label = 'SEQUENCE' , font = 'boldLabelFont' , bgc =  ( 1 , 0.8 , 0.9 ) , width = width/3 ) ;
                    pm.textScrollList ( 'sequence_textScrollList' , ams = False , h = textScrollHeight , width = width/3 , sc = listcahe_cmd , dcc = browserDoubleClick_cmd )

                 with pm.rowColumnLayout ( nc = 1 , w = width/3 ) :
                    pm.text ( label = 'CHARACTER' , font = 'boldLabelFont' , bgc =  ( 0.1 , 0.55 , 0.5 ) , width = width/3 ) ;
                    pm.textScrollList ( 'character_textScrollList' , ams = False , h = textScrollHeight , width = width/3 , dcc = charDoubleClick_cmd )

            with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , width/3 ) , ( 2 , width/3 ) , ( 3 , width/3 ) ] ) :
                pm.text ( label = '' )
                pm.text ( label = '' )
                pm.button ( label = 'set project' , w = width/4 , bgc = ( 0.8 , 0.3 , 0.3 ),c = setProject_cmd) ;
                
            with pm.frameLayout ( label = 'CACHE' , w = width , bgc = ( 0.1 , 0.1 , 0.45 ) ) :

                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :

                    with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :
                        pm.textScrollList ( 'cacheFile_textScrollList' , ams = True , h = textScrollHeight , width = width/2 ,dcc = browserDoubleClick_cmd )


                    with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :
                        with pm.optionMenu ( 'cacheClass_optionMenu' , label = 'cache class'  , w = width/2 ,cc = listcahe_cmd ) :
                            pm.menuItem ( label='char' ) ;

                            pm.menuItem ( label='prop' ) ;
                        pm.text ( label = '' )
                        pm.text ( label = '' )
                        pm.text ( label = '' )
                        pm.text ( label = '' )
                        pm.text ( label = '' )
                        pm.text ( label = '' )
                        pm.text ( label = '' )
                        pm.text ( label = '' )
                        pm.text ( label = '' )
                        pm.text ( label = '' )
                        pm.checkBox ( 'cache_cbx' , label = 'collide' , value = False ) ;
                        pm.text ( label = '' )
                        pm.button ( label = 'AUTONAMING' ,  bgc = ( 1 , 0.55 , 0 ), h = 25 , width = width/2 ,c = autoNaming_cmd) 

    pm.showWindow( window )
