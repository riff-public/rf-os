import maya.cmds as mc

def setSpaceScale1(*args):

    SS1 = mc.floatFieldGrp("SSField1", q = True, value = True )
    ValueSS1 = SS1[0]

    ItemList = mc.textScrollList( "myControlObj", query=True, allItems = True) 
    mc.setAttr('%s.spaceScale'%ItemList[0], ValueSS1)
    
def setSpaceScale2(*args):

    SS2 = mc.floatFieldGrp("SSField2", q = True, value = True )
    ValueSS2 = SS2[0]
    ItemList = mc.textScrollList( "myControlObj", query=True, allItems = True) 
    mc.setAttr('%s.spaceScale'%ItemList[1], ValueSS2)
    
def setSpaceScale3(*args):

    SS3 = mc.floatFieldGrp("SSField3", q = True, value = True )
    ValueSS3 = SS3[0]
    
    ItemList = mc.textScrollList( "myControlObj", query=True, allItems = True) 
    mc.setAttr('%s.spaceScale'%ItemList[2], ValueSS3)
    
def setSpaceScale4(*args):

    SS4 = mc.floatFieldGrp("SSField4", q = True, value = True )
    ValueSS4 = SS4[0]
    
    ItemList = mc.textScrollList( "myControlObj", query=True, allItems = True) 
    mc.setAttr('%s.spaceScale'%ItemList[3], ValueSS4)

def RefreshValueSS(*args):
    
    listValueSS = [0.0, 0.0, 0.0, 0.0]
    ItemList = mc.textScrollList( "myControlObj", query=True, allItems = True) 
    #NU1
    gSS1 = mc.getAttr("%s.spaceScale"%ItemList[0]) 
    index = 0
    listValueSS[index]= gSS1    
    mc.floatFieldGrp("SSField1", edit = True, value1 = listValueSS[0])
    mc.floatFieldGrp("SSField1", query = True, value1 = True)     
            
    #NU2
    gSS2 = mc.getAttr("%s.spaceScale"%ItemList[1])
    index = 1
    listValueSS[index]= gSS2    
    mc.floatFieldGrp("SSField2", edit = True, value1 = listValueSS[1])
    mc.floatFieldGrp("SSField2", query = True, value1 = True)
        
    #NU3   
    gSS3 = mc.getAttr("%s.spaceScale"%ItemList[2])
    index = 2
    listValueSS[index]= gSS3    
    mc.floatFieldGrp("SSField3", edit = True, value1 = listValueSS[2])
    mc.floatFieldGrp("SSField3", query = True, value1 = True)
    
    #NU4 
    gSS4 = mc.getAttr("%s.spaceScale"%ItemList[3])
    index = 3
    listValueSS[index]= gSS4   
    mc.floatFieldGrp("SSField4", edit = True, value1 = listValueSS[3])
    mc.floatFieldGrp("SSField4", query = True, value1 = True)

def selectNU(): 
    
    ItemList = mc.textScrollList( "myControlObj", query=True, allItems = True) 
    print ItemList
    
    selItem = mc.textScrollList( "myControlObj", query=True, selectIndexedItem = True)
    print selItem
   
    
    if selItem == [1L] :
        mc.textScrollList( "myControlObj", edit = True, selectCommand = selectNU) 
        mc.select(ItemList[0])  
        
        
    elif selItem == [2L] :
        mc.textScrollList( "myControlObj", edit = True, selectCommand = selectNU) 
        mc.select(ItemList[1])           
       
       
    elif selItem == [3L] :
        mc.textScrollList( "myControlObj", edit = True, selectCommand = selectNU) 
        mc.select(ItemList[2]) 
        
    elif selItem == [4L] :
        mc.textScrollList( "myControlObj", edit = True, selectCommand = selectNU) 
        mc.select(ItemList[3]) 

def ReNU(*args):
        
    LNu = mc.ls(type = 'nucleus')
    print LNu
    mc.textScrollList( "myControlObj", edit = True, removeAll = True)
    mc.textScrollList( "myControlObj", edit = True, append = [LNu[0], LNu[1], LNu[2], LNu[3]])
    mc.textScrollList( "myControlObj", query = True, append = True)
    
    listValue = [0.0, 0.0, 0.0, 0.0]
    listValueSS = [0.0, 0.0, 0.0, 0.0]
    ItemList = mc.textScrollList( "myControlObj", query=True, allItems = True) 
    #NU1
    gScaleX1 = mc.getAttr("%s.timeScale"%ItemList[0])
    index = 0
    listValue[index]= gScaleX1    
    mc.floatFieldGrp("NUField1", edit = True, value1 = listValue[0])
    mc.floatFieldGrp("NUField1", query = True, value1 = True) 
    #NU2
    gScaleX2 = mc.getAttr("%s.timeScale"%ItemList[1])
    index = 1
    listValue[index]= gScaleX2    
    mc.floatFieldGrp("NUField2", edit = True, value1 = listValue[1])
    mc.floatFieldGrp("NUField2", query = True, value1 = True)    
    #NU3   
    gScaleX3 = mc.getAttr("%s.timeScale"%ItemList[2])
    index = 2
    listValue[index]= gScaleX3    
    mc.floatFieldGrp("NUField3", edit = True, value1 = listValue[2])
    mc.floatFieldGrp("NUField3", query = True, value1 = True)    
    #NU4 
    gScaleX4 = mc.getAttr("%s.timeScale"%ItemList[3])
    index = 3
    listValue[index]= gScaleX4   
    mc.floatFieldGrp("NUField4", edit = True, value1 = listValue[3])
    mc.floatFieldGrp("NUField4", query = True, value1 = True)
    
    #----------------------------------------------------------------------
    #NU1
    gSS1 = mc.getAttr("%s.spaceScale"%ItemList[0])  
    index = 0
    listValueSS[index]= gSS1    
    mc.floatFieldGrp("SSField1", edit = True, value1 = listValueSS[0])
    mc.floatFieldGrp("SSField1", query = True, value1 = True)     
            
    #NU2
    gSS2 = mc.getAttr("%s.spaceScale"%ItemList[1])
    index = 1
    listValueSS[index]= gSS2    
    mc.floatFieldGrp("SSField2", edit = True, value1 = listValueSS[1])
    mc.floatFieldGrp("SSField2", query = True, value1 = True)
        
    #NU3   
    gSS3 = mc.getAttr("%s.spaceScale"%ItemList[2])
    index = 2
    listValueSS[index]= gSS3    
    mc.floatFieldGrp("SSField3", edit = True, value1 = listValueSS[2])
    mc.floatFieldGrp("SSField3", query = True, value1 = True)
    
    #NU4 
    gSS4 = mc.getAttr("%s.spaceScale"%ItemList[3])
    index = 3
    listValueSS[index]= gSS4   
    mc.floatFieldGrp("SSField4", edit = True, value1 = listValueSS[3])
    mc.floatFieldGrp("SSField4", query = True, value1 = True)
    
        
   
   
        
def RefreshValueTS(*args):
    
    listValue = [0.0, 0.0, 0.0, 0.0]
    ItemList = mc.textScrollList( "myControlObj", query=True, allItems = True) 
    #NU1
    
    gScaleX1 = mc.getAttr("%s.timeScale"%ItemList[0])
    index = 0
    listValue[index]= gScaleX1    
    mc.floatFieldGrp("NUField1", edit = True, value1 = listValue[0])
    mc.floatFieldGrp("NUField1", query = True, value1 = True)     
            
    #NU2
    gScaleX2 = mc.getAttr("%s.timeScale"%ItemList[1])
    index = 1
    listValue[index]= gScaleX2    
    mc.floatFieldGrp("NUField2", edit = True, value1 = listValue[1])
    mc.floatFieldGrp("NUField2", query = True, value1 = True)
        
    #NU3   
    gScaleX3 = mc.getAttr("%s.timeScale"%ItemList[2])
    index = 2
    listValue[index]= gScaleX3    
    mc.floatFieldGrp("NUField3", edit = True, value1 = listValue[2])
    mc.floatFieldGrp("NUField3", query = True, value1 = True)
    
    #NU4 
    gScaleX4 = mc.getAttr("%s.timeScale"%ItemList[3])
    index = 3
    listValue[index]= gScaleX4   
    mc.floatFieldGrp("NUField4", edit = True, value1 = listValue[3])
    mc.floatFieldGrp("NUField4", query = True, value1 = True)


def setTimeScale1(*args):
    
    
    TS1 = mc.floatFieldGrp("NUField1", q = True, value = True )
    ValueTS1 = TS1[0]
    
    ItemList = mc.textScrollList( "myControlObj", query=True, allItems = True)
    mc.setAttr('%s.timeScale'%ItemList[0], ValueTS1)
 

def setTimeScale2(*args):
   
    TS2 = mc.floatFieldGrp("NUField2", q = True, value = True )
    ValueTS2 = TS2[0]
    
    ItemList = mc.textScrollList( "myControlObj", query=True, allItems = True)
    mc.setAttr('%s.timeScale'%ItemList[1], ValueTS2)


    
def setTimeScale3(*args):

    TS3 = mc.floatFieldGrp("NUField3", q = True, value = True )
    ValueTS3 = TS3[0]
    
    ItemList = mc.textScrollList( "myControlObj", query=True, allItems = True)
    mc.setAttr('%s.timeScale'%ItemList[2], ValueTS3)
 
    
def setTimeScale4(*args):
  
    TS4 = mc.floatFieldGrp("NUField4", q = True, value = True )
    ValueTS4 = TS4[0]
    
    ItemList = mc.textScrollList("myControlObj", query=True, allItems = True)
    mc.setAttr('%s.timeScale'%ItemList[3], ValueTS4)
    

def ApplyAllTS(*args):
    ItemList = mc.textScrollList("myControlObj", query=True, allItems = True)
    
    TS1 = mc.floatFieldGrp("NUField1", q = True, value = True )
    ValueTS1 = TS1[0]
    mc.setAttr('%s.timeScale'%ItemList[0], ValueTS1)
    
    TS2 = mc.floatFieldGrp("NUField2", q = True, value = True )
    ValueTS2 = TS2[0]
    mc.setAttr('%s.timeScale'%ItemList[1], ValueTS2)
    
    TS3 = mc.floatFieldGrp("NUField3", q = True, value = True )
    ValueTS3 = TS3[0]
    mc.setAttr('%s.timeScale'%ItemList[2], ValueTS3)
    
    TS4 = mc.floatFieldGrp("NUField4", q = True, value = True )
    ValueTS4 = TS4[0]
    mc.setAttr('%s.timeScale'%ItemList[3], ValueTS4)

def ApplyAllSS(*args):
    ItemList = mc.textScrollList("myControlObj", query=True, allItems = True)
    
    SS1 = mc.floatFieldGrp("SSField1", q = True, value = True )
    ValueSS1 = SS1[0]
    mc.setAttr('%s.spaceScale'%ItemList[0], ValueSS1) 
    
    SS2 = mc.floatFieldGrp("SSField1", q = True, value = True )
    ValueSS2 = SS2[0]
    mc.setAttr('%s.spaceScale'%ItemList[1], ValueSS2)
    
    SS3 = mc.floatFieldGrp("SSField1", q = True, value = True )
    ValueSS3 = SS3[0]
    mc.setAttr('%s.spaceScale'%ItemList[2], ValueSS3)
    
    SS4 = mc.floatFieldGrp("SSField1", q = True, value = True )
    ValueSS4 = SS4[0]
    mc.setAttr('%s.spaceScale'%ItemList[3], ValueSS4)
    
def run ( *args ) :

    windowNameNU = "SettingNucleusV2"
    listValue = [0.0, 0.0, 0.0, 0.0]
    listValueSS = [0.0, 0.0, 0.0, 0.0] 
    LNu = mc.ls( type = 'nucleus')

    if (mc.window(windowNameNU, exists = True )):
        mc.deleteUI(windowNameNU)

    windowNameNU = mc.window ( windowNameNU , title = windowNameNU , w = 200 , sizeable = True , rtf = True ) 
    mc.paneLayout(configuration= "horizontal2")
    mc.textScrollList( "myControlObj", numberOfRows = 2, allowMultiSelection = True, removeAll = True,  
                        append=[LNu[0],LNu[1],LNu[2],LNu[3]],
                        font = "boldLabelFont",selectCommand = selectNU )
                        
    vTS1 = mc.getAttr("%s.timeScale"%LNu[0])
    index = 0
    listValue[index]= vTS1  
    
    vTS2 = mc.getAttr("%s.timeScale"%LNu[1])
    index = 1
    listValue[index]= vTS1
    
    vTS3 = mc.getAttr("%s.timeScale"%LNu[2])
    index = 2
    listValue[index]= vTS1
    
    vTS4 = mc.getAttr("%s.timeScale"%LNu[3])
    index = 3
    listValue[index]= vTS4
    
    vSS1 = mc.getAttr("%s.spaceScale"%LNu[0])
    index = 0
    listValueSS[index]= vSS1
    
    vSS2 = mc.getAttr("%s.spaceScale"%LNu[1])
    index = 1
    listValueSS[index]= vSS2
    
    vSS3 = mc.getAttr("%s.spaceScale"%LNu[2])
    index = 2
    listValueSS[index]= vSS3
    
    vSS4 = mc.getAttr("%s.spaceScale"%LNu[3])
    index = 3
    listValueSS[index]= vSS4
     
            
                                       
    ColA = mc.columnLayout(adjustableColumn = True)
    separatorA = mc.separator(height = 10)
    BgetNU= mc.button("BgetNU", label = "Refresh Nucleus", command = ReNU , width = 80, h = 30, 
                       backgroundColor = [0.490196, 0.619608, 0.752941], enable = True )
    separatorA = mc.separator(height = 14)
    RowA = mc.rowColumnLayout( numberOfColumns= 2, columnWidth =[(1, 130), (2, 60)])
    NUField1 = mc.floatFieldGrp("NUField1", label = "Time Scale1", value1 = vTS1, precision = 3,
                    columnAlign = [1,"left"], adjustableColumn = 1, columnWidth = [2,60])
    mc.button(label = "Apply", command = setTimeScale1 , backgroundColor = [0.196078, 0.803922, 0.196078])


    NUField2 = mc.floatFieldGrp("NUField2" , label = "Time Scale2", value1 = vTS2, precision = 3,
                    columnAlign = [1,"left"], adjustableColumn = 1, columnWidth = [2,60])
    mc.button(label = "Apply", command = setTimeScale2 , backgroundColor = [0.196078, 0.803922, 0.196078])


    NUField3 = mc.floatFieldGrp("NUField3", label = "Time Scale3", value1 = vTS3, precision = 3,
                    columnAlign = [1,"left"], adjustableColumn = 1, columnWidth = [2,60])                
    mc.button(label = "Apply", command = setTimeScale3 , backgroundColor = [0.196078, 0.803922, 0.196078])


    NUField4 = mc.floatFieldGrp("NUField4", label = "Time Scale4", value1 = vTS4, precision = 3,
                    columnAlign = [1,"left"], adjustableColumn = 1, columnWidth = [2,60])                
    mc.button(label = "Apply", command = setTimeScale4 , backgroundColor = [0.196078, 0.803922, 0.196078])


    RowB = mc.rowColumnLayout( numberOfColumns= 1, columnWidth =[(1, 130), (2, 60)], p = ColA)
    mc.button(label = "Apply All", command = ApplyAllTS, p = RowB, w =200, backgroundColor = [0.219608, 0.556863, 0.556863])
    mc.button(label = "Refresh", command = RefreshValueTS , p = RowB, w =200, backgroundColor = [0.529412, 0.807843, 0.980392])
    
    separatorTimeScaleA = mc.separator( height = 10, w = 200 )
    
    #Space Scale
    ColB = mc.columnLayout(adjustableColumn = True, p = ColA) 
    RowC = mc.rowColumnLayout( numberOfColumns= 2, columnWidth =[(1, 130), (2, 60)],p = ColB) 
     
    SSField1 = mc.floatFieldGrp("SSField1", label = "Space Scale1", value1 = vSS1, precision = 3,
                    columnAlign = [1,"left"], adjustableColumn = 1, columnWidth = [2,55])
    BSS1 = mc.button("BSS1", label = "Apply", command = setSpaceScale1 , backgroundColor = [0.196078, 0.803922, 0.196078])


    SSField2 = mc.floatFieldGrp("SSField2", label = "Space Scale2", value1 = vSS2, precision = 3,
                    columnAlign = [1,"left"], adjustableColumn = 1, columnWidth = [2,55])
    BSS2 = mc.button("BSS2", label = "Apply", command = setSpaceScale2 , backgroundColor = [0.196078, 0.803922, 0.196078])


    SSField3 = mc.floatFieldGrp("SSField3", label = "Space Scale3", value1 = vSS3, precision = 3,
                    columnAlign = [1,"left"], adjustableColumn = 1, columnWidth = [2,55])                
    BSS3 = mc.button("BSS3", label = "Apply", command = setSpaceScale3 , backgroundColor = [0.196078, 0.803922, 0.196078])


    SSField4 = mc.floatFieldGrp("SSField4", label = "Space Scale4", value1 = vSS4, precision = 3,
                    columnAlign = [1,"left"], adjustableColumn = 1, columnWidth = [2,55])                
    BSS4 = mc.button("BSS4", label = "Apply", command = setSpaceScale4 , backgroundColor = [0.196078, 0.803922, 0.196078])

    RowB = mc.rowColumnLayout( numberOfColumns = 1, columnWidth =[(1, 130), (2, 60)], p = ColA)
    mc.button(label = "Apply All",command = ApplyAllSS, w =200, backgroundColor = [0.219608, 0.556863, 0.556863])
    mc.button(label = "Refresh", command = RefreshValueSS , w =200, backgroundColor = [0.529412, 0.807843, 0.980392])
    
    separatorTimeScaleA = mc.separator( height = 10, w = 200 )

    mc.showWindow()
    
run ( )