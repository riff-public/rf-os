import maya.cmds as mc
import maya.mel as mel

listNrgd = []


def makeMulCOL():
    
    sels = mc.ls(sl = True)
    
    spl = sels[0].split(':')
    print 'spl:', spl
    
    grpMN = mc.createNode('transform', n = '%s_MNCol_Grp'%spl[1])
    print grpMN
    
    grpMNRg = mc.createNode('transform', n = '%s_MNRig_Grp'%spl[1])
    print grpMNRg
    
    mc.parent(grpMNRg, grpMN)
    
    listDup = []
    
    
    for i in range(4):  
        num = i + 1      
        dup = mc.duplicate( sels, name = '%s_N%s_MNCol'%(spl[1],num) )
        print 'dup', dup
        
        listDup.append(dup[0])
        
        mc.parent(dup , grpMN)
        
        shp = mc.listRelatives(dup)
        print 'shp', shp
        
        for o in shp:
            
            if 'Orig' in o:
                print 'this is Orig'
                mc.delete(o)
                
            else:
                print 'No Orig'
      
      
      
                
    print 'listDup', listDup 
    
    for nR in range(len(listDup)):
        num = nR + 1
        mc.select(listDup[nR], r = True)
        nRgd = mel.eval('makeCollideNCloth;')
        print 'nRgd', nRgd
        
        
        
       
        
        
        mc.select(nRgd, r = True)
        
        pickNR = mc.pickWalk( d = 'up')
        mc.parent(pickNR, grpMNRg)
    
        mel.eval('assignNSolver nucleus%s;'%num)
        
        nameNrgd = mc.rename(pickNR, '%s_N%s_nRigid'%(spl[1], num)) 
        print 'nameNrgd:', nameNrgd
        listNrgd.append(nameNrgd)
    
    
    
    listDup.insert(0,sels[0])
    
    print 'listDup', listDup
    
    for b in range(len(listDup)):
        print 'b',b
        print listDup[b]
        
        if listDup[b] == listDup[4]:
            print 'this is the last one'
            
        else:
            
        
            mc.select(listDup[b],r = True)
            mc.select(listDup[b+1],add = True)
            mc.blendShape(automatic = True, origin = 'world', n = '%s_BSH'%listDup[b] , w = ( 0 , 1 ) )
            
            
    for vb in range(len(listDup)):
        
        
        if listDup[vb] == listDup[1]:
            print 'this geo will not visibility'
            
        else:                    
            mc.setAttr('%s.visibility'%listDup[vb], 0)
            
            
    
    conntNrgdBB()
      
        
      


def conntNrgdBB():
    #selsCOL = mc.ls(sl = True)
    
    print 'listNrgd', listNrgd
    
    listShpNg = []
    
    for i in listNrgd:
        print 'i', i
        
        #nameCOL = i.split('_MNCol')
        #print nameCOL
        nameNrgs = i + 'Shape'
        print nameNrgs
        listShpNg.append(nameNrgs)
    
    
    print 'listShpNg', listShpNg
    
    for ln in range(len(listShpNg)):
        
        if listShpNg[ln] == listShpNg[3]:
            print  'listShpNg[3]', listShpNg[3]
            
        else:
            print 'ok'
            
            
            mc.connectAttr('%s.isDynamic'%listShpNg[ln], '%s.isDynamic'%listShpNg[ln+1])
            mc.connectAttr('%s.cofl'%listShpNg[ln], '%s.cofl'%listShpNg[ln+1])
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.cold";
            mc.connectAttr('%s.cold'%listShpNg[ln], '%s.cold'%listShpNg[ln+1])
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.pou";
            mc.connectAttr('%s.pou'%listShpNg[ln], '%s.pou'%listShpNg[ln+1])
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.por";
            mc.connectAttr('%s.por'%listShpNg[ln], '%s.por'%listShpNg[ln+1])
            
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.cop";
            mc.connectAttr('%s.cop'%listShpNg[ln], '%s.cop'%listShpNg[ln+1])
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.tpc";
            mc.connectAttr('%s.tpc'%listShpNg[ln], '%s.tpc'%listShpNg[ln+1])
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.dcr";
            mc.connectAttr('%s.dcr'%listShpNg[ln], '%s.dcr'%listShpNg[ln+1])
            
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.dcg";
            mc.connectAttr('%s.dcg'%listShpNg[ln], '%s.dcg'%listShpNg[ln+1])
            
            #CBdeleteConnection "BodyFur_GeoN2COL_nRigidShape.dcb";
            mc.connectAttr('%s.dcb'%listShpNg[ln], '%s.dcb'%listShpNg[ln+1])
            
            #CBdeleteConnection "BodyFur_GeoN4COL_nRigidShape.boce";
            mc.connectAttr('%s.boce'%listShpNg[ln], '%s.boce'%listShpNg[ln+1])
            
            
            mc.connectAttr('%s.collideStrength'%listShpNg[ln], '%s.collideStrength'%listShpNg[ln+1])
            mc.connectAttr('%s.collisionLayer'%listShpNg[ln], '%s.collisionLayer'%listShpNg[ln+1])
            mc.connectAttr('%s.thickness'%listShpNg[ln], '%s.thickness'%listShpNg[ln+1])
            
            mc.connectAttr('%s.friction'%listShpNg[ln], '%s.friction'%listShpNg[ln+1])
            mc.connectAttr('%s.stickiness'%listShpNg[ln], '%s.stickiness'%listShpNg[ln+1])
                
    
    
    
    
    

        

            
    
    
    