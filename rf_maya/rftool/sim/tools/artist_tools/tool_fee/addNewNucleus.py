import maya.cmds as mc

def assNSolv(*args):
    
    sels = mc.ls(sl = True)
        
    for i in sels:
        print i
        
        mc.select(i,r=True)
               
        mel.eval('assignNSolver "";')
        
    lsNucleus()
            

def lsNucleus(*args):
    
    
    #removeAll
    mc.textScrollList( 'listNucleus',e=True, removeAll = True) 
    
    lsN = mc.ls(type='nucleus')
    print lsN
        
    for i in range(len(lsN)):
        print "i",i
        num = i + 1
        
        mc.textScrollList( 'listNucleus', e = True, appendPosition= [num,'%s'%lsN[i]]) 
        
        
        

def selNu():
    
    qSelNu = mc.textScrollList( 'listNucleus', q = True, selectItem = True ) 
    print "qSelNu:",qSelNu[0]
    

    
def assNSolvNum(*args):
    
    sels = mc.ls(sl = True)
        
    #num = 2
    
    qSelNu = mc.textScrollList( 'listNucleus', q = True, selectItem = True ) 
    print "qSelNu:",qSelNu[0]
        
    for i in sels:
        print i
        
        mc.select(i,r=True)
               
        mel.eval('assignNSolver %s;'%qSelNu[0])




def conStFrm(*args):
    
    lsN = mc.ls(type='nucleus')
    print lsN
    
    for i in lsN:
        
        if i == 'nucleus1':
            print 'this nucleus1'
            
        else:
            
            mc.connectAttr( 'nucleus1.startFrame', 'nucleus2.startFrame',force = True)
            mc.connectAttr( 'nucleus1.startFrame', 'nucleus3.startFrame',force = True)
            mc.connectAttr( 'nucleus1.startFrame', 'nucleus4.startFrame',force = True)
    
    
def checkNu(*args):
    
    sels = mc.ls(sl=True)
    
    for i in sels:
        print i
    
        lsShp =  mc.listRelatives(i) 
           
        lsCon = mc.listConnections(lsShp, t = 'nucleus')
        
        
        mc.textScrollList( 'checkNucleus',e = True, append = '%s_%s'%(i,lsCon[0])) 
        
def refreshCheck(*args):
    
    mc.textScrollList( 'checkNucleus',e=True, removeAll = True) 

def assNSolvUI():
    
    if mc.window('assNSolv', exists = True, widthHeight=(100, 55) ):
        mc.deleteUI('assNSolv')
        
    mc.window('assNSolv')
    #mc.columnLayout()
    
       
    form = mc.formLayout()
    mc.rowColumnLayout(numberOfColumns=1)
    mc.text(label = 'select hairSystem to assign New Nucleus')
    mc.separator( height = 10, style = 'none')
    mc.button(label = 'assign New Nucleus from seleted', w = 385, c = assNSolv)
    
    mc.separator( height = 40, style = 'shelf')
    
    
    mc.paneLayout(activePaneIndex = 3)
    myCluster = mc.textScrollList( 'listNucleus', numberOfRows=6, allowMultiSelection = True, append=[], selectCommand = selNu ) 
    lsNucleus()
    mc.setParent( '..' )
    
    form = mc.formLayout()
    mc.rowColumnLayout(numberOfColumns=1)
    
    mc.button(label = 'assign hairSystem to Nucleus Number', w = 385, c = assNSolvNum)  
    
    mc.separator( height = 10, style = 'none')
        
    mc.button(label = 'connect Start Frame', w = 385, c = conStFrm)  
    
    mc.separator( height = 10, style = 'shelf') 
    
    mc.paneLayout(activePaneIndex = 3)
    myCluster = mc.textScrollList( 'checkNucleus', numberOfRows=6, allowMultiSelection = True, append=[]) 
    mc.setParent( '..' )
    form = mc.formLayout()
    mc.rowColumnLayout(numberOfColumns=1)
    mc.button(label = 'Check Nucleus', w = 385, c = checkNu)  
    mc.button(label = 'Refresh', w = 385, c = refreshCheck)  
   
   
     
    mc.showWindow('assNSolv')  

assNSolvUI()












