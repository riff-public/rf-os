import maya.cmds as mc   


##---MakePink Color------------------------------------------------------------------------------------
def makePinkCol(*args):  
    listMat = mc.ls(mat = True)
    listMatCol = ["Pl"]      
    ## check there is shade yet
    for i in listMat:  
        nameShader = 'pinkShader'   
        if i == nameShader :
            print 'yes!!'
            listMatCol.insert( 0, i)
                                                  
        else :
            print 'no color yet'
            
    ## it will make shade if they is no shade yet
    for a in listMatCol:    
       
        if a == 'pinkShader' :
            print 'there is shade already'  
            break
                                                            
        else :
            print 'make Color'
            #makeColor
            blueCol = 1, 0.4, 0.8
            print blueCol
            vCol = blueCol
            print vCol
            nameShade = 'pink'
            mc.sets( name='%sMaterialGroup'%nameShade, renderable=True, empty=True )
            mc.shadingNode( 'blinn', name='%sShader'%nameShade, asShader=True )
            mc.setAttr( '%sShader.color'%nameShade, vCol[0],vCol[1],vCol[2] , type='double3', )
            mc.setAttr('%sShader.diffuse'%nameShade, 0.8) 
            mc.setAttr('%sShader.eccentricity'%nameShade, 0.6) 
            mc.setAttr('%sShader.specularRollOff'%nameShade, 0.3) 
            mc.setAttr('%sShader.specularColor'%nameShade, 0.15, 0.15, 0.15 , type = 'double3') 
            mc.surfaceShaderList( '%sShader'%nameShade, add='%sMaterialGroup'%nameShade )
            break
                                                        
##----makeWhiteColor---------------------------------------------------------------------------------------------------                     

##-------------------------------------------------------------------------------------------------------  
def makeWhiteCol(*args):  
    listMat = mc.ls(mat = True)
    listMatCol = ["Wl"]      
    ## check there is shade yet
    for i in listMat:  
        nameShader = 'whiteShader'   
        if i == nameShader :
            print 'yes!!'
            listMatCol.insert( 0, i)
                                                  
        else :
            print 'no color yet'
            
    ## it will make shade if they is no shade yet
    for a in listMatCol:    
       
        if a == 'whiteShader' :
            print 'there is shade already'  
            break
                                                            
        else :
            print 'make Color'
            #makeColor
            blueCol = 1, 1, 1
            print blueCol
            vCol = blueCol
            print vCol
            nameShade = 'white'
            mc.sets( name='%sMaterialGroup'%nameShade, renderable=True, empty=True )
            mc.shadingNode( 'blinn', name='%sShader'%nameShade, asShader=True )
            mc.setAttr( '%sShader.color'%nameShade, vCol[0],vCol[1],vCol[2] , type='double3', )
            mc.setAttr('%sShader.diffuse'%nameShade, 0.8) 
            mc.setAttr('%sShader.eccentricity'%nameShade, 1) 
            mc.setAttr('%sShader.specularRollOff'%nameShade, 0.3) 
            mc.setAttr('%sShader.specularColor'%nameShade, 0.15, 0.15, 0.15 , type = 'double3') 
            mc.surfaceShaderList( '%sShader'%nameShade, add='%sMaterialGroup'%nameShade )
            break
                                                        
##----makeYellowColor---------------------------------------------------------------------------------------------------                      
def makeYellowCol(*args):  
    listMat = mc.ls(mat = True)
    listMatCol = ["Yl"]      
    ## check there is shade yet
    for i in listMat:  
        nameShader = 'yellowShader'   
        if i == nameShader :
            print 'yes!!'
            listMatCol.insert( 0, i)
                                                  
        else :
            print 'no color yet'
            
    ## it will make shade if they is no shade yet
    for a in listMatCol:    
       
        if a == 'yellowShader' :
            print 'there is shade already'  
            break
                                                            
        else :
            print 'make Color'
            #makeColor
            blueCol = 1, 0.9, 0.4
            print blueCol
            vCol = blueCol
            print vCol
            nameShade = 'yellow'
            mc.sets( name='%sMaterialGroup'%nameShade, renderable=True, empty=True )
            mc.shadingNode( 'blinn', name='%sShader'%nameShade, asShader=True )
            mc.setAttr( '%sShader.color'%nameShade, vCol[0],vCol[1],vCol[2] , type='double3', )
            mc.setAttr('%sShader.diffuse'%nameShade, 0.8) 
            mc.setAttr('%sShader.eccentricity'%nameShade, 0.6) 
            mc.setAttr('%sShader.specularRollOff'%nameShade, 0.3) 
            mc.setAttr('%sShader.specularColor'%nameShade, 0.15, 0.15, 0.15 , type = 'double3') 
            mc.surfaceShaderList( '%sShader'%nameShade, add='%sMaterialGroup'%nameShade )
            break
            
##---------------------------------------------------------------------------------------------------------------

def makeGreenCol():  
    listMat = mc.ls(mat = True)
    listMatCol = ["Gl"]      
    ## check there is shade yet
    for i in listMat:  
        nameShader = 'greenShader'   
        if i == nameShader :
            print 'yes!!'
            listMatCol.insert( 0, i)
                                                  
        else :
            print 'no color yet'
            
    ## it will make shade if they is no shade yet
    for a in listMatCol:    
       
        if a == 'greenShader' :
            print 'there is shade already'  
            break
                                                            
        else :
            print 'make Color'
            #makeColor
            blueCol = 0.6, 1, 0.4 
            print blueCol
            vCol = blueCol
            print vCol
            nameShade = 'green'
            mc.sets( name='%sMaterialGroup'%nameShade, renderable=True, empty=True )
            mc.shadingNode( 'blinn', name='%sShader'%nameShade, asShader=True )
            mc.setAttr( '%sShader.color'%nameShade, vCol[0],vCol[1],vCol[2] , type='double3', )
            mc.setAttr('%sShader.diffuse'%nameShade, 0.8) 
            mc.setAttr('%sShader.eccentricity'%nameShade, 0.6) 
            mc.setAttr('%sShader.specularRollOff'%nameShade, 0.3) 
            mc.setAttr('%sShader.specularColor'%nameShade, 0.15, 0.15, 0.15 , type = 'double3') 
            mc.surfaceShaderList( '%sShader'%nameShade, add='%sMaterialGroup'%nameShade )
            break
##--------------------------------------------------------------------------------------------------------------


def makeSkinCol():  
    listMat = mc.ls(mat = True)
    listMatCol = ["Skl"]      
    ## check there is shade yet
    for i in listMat:  
        nameShader = 'skinShader'   
        if i == nameShader :
            print 'yes!!'
            listMatCol.insert( 0, i)
                                                  
        else :
            print 'no color yet'
            
    ## it will make shade if they is no shade yet
    for a in listMatCol:    
       
        if a == 'skinShader' :
            print 'there is shade already'  
            break
                                                            
        else :
            print 'make Color'
            #makeColor
            blueCol = 1, 0.685, 0.55
            print blueCol
            vCol = blueCol
            print vCol
            nameShade = 'skin'
            mc.sets( name='%sMaterialGroup'%nameShade, renderable=True, empty=True )
            mc.shadingNode( 'blinn', name='%sShader'%nameShade, asShader=True )
            mc.setAttr( '%sShader.color'%nameShade, vCol[0],vCol[1],vCol[2] , type='double3', )
            mc.setAttr('%sShader.diffuse'%nameShade, 1) 
            mc.setAttr('%sShader.eccentricity'%nameShade, 0.3) 
            mc.setAttr('%sShader.specularRollOff'%nameShade, 0.7) 
            mc.setAttr('%sShader.specularColor'%nameShade, 0.15, 0.15, 0.15 , type = 'double3') 
            mc.surfaceShaderList( '%sShader'%nameShade, add='%sMaterialGroup'%nameShade )
            break

##-----------------------------------------------------------------------------------------------------------------------------------------
def makeRedCol():  
    listMat = mc.ls(mat = True)
    listMatCol = ["Rl"]      
    ## check there is shade yet
    for i in listMat:  
        nameShader = 'redShader'   
        if i == nameShader :
            print 'yes!!'
            listMatCol.insert( 0, i)
                                                  
        else :
            print 'no color yet'
            
    ## it will make shade if they is no shade yet
    for a in listMatCol:    
       
        if a == 'redShader' :
            print 'there is shade already'  
            break
                                                            
        else :
            print 'make Color'
            #makeColor
            blueCol = 1, 0.4, 0.4
            print blueCol
            vCol = blueCol
            print vCol
            nameShade = 'red'
            mc.sets( name='%sMaterialGroup'%nameShade, renderable=True, empty=True )
            mc.shadingNode( 'blinn', name='%sShader'%nameShade, asShader=True )
            mc.setAttr( '%sShader.color'%nameShade, vCol[0],vCol[1],vCol[2] , type='double3', )
            mc.setAttr('%sShader.eccentricity'%nameShade, 0.6) 
            mc.setAttr('%sShader.specularRollOff'%nameShade, 0.3) 
            mc.setAttr('%sShader.specularColor'%nameShade, 0.15, 0.15, 0.15 , type = 'double3') 
            mc.surfaceShaderList( '%sShader'%nameShade, add='%sMaterialGroup'%nameShade )
            break

###--------------------------------------------------------------------------------------------------------------------------------------
def makeBlueCol():  
    listMat = mc.ls(mat = True)
    listMatCol = ["Bl"]      
    ## check there is shade yet
    for i in listMat:  
        nameShader = 'blueShader'   
        if i == nameShader :
            print 'yes!!'
            listMatCol.insert( 0, i)
                                                  
        else :
            print 'no color yet'
            
    ## it will make shade if they is no shade yet
    for a in listMatCol:    
       
        if a == 'blueShader' :
            print 'there is shade already'  
            break
                                                            
        else :
            print 'make Color'
            #makeColor
            blueCol = 0.4, 0.6, 1
            print blueCol
            vCol = blueCol
            print vCol
            nameShade = 'blue'
            mc.sets( name='%sMaterialGroup'%nameShade, renderable=True, empty=True )
            mc.shadingNode( 'blinn', name='%sShader'%nameShade, asShader=True )
            mc.setAttr( '%sShader.color'%nameShade, vCol[0],vCol[1],vCol[2] , type='double3', )
            mc.setAttr('%sShader.eccentricity'%nameShade, 0.6) 
            mc.setAttr('%sShader.specularRollOff'%nameShade, 0.3) 
            mc.setAttr('%sShader.specularColor'%nameShade, 0.15, 0.15, 0.15 , type = 'double3') 
            mc.surfaceShaderList( '%sShader'%nameShade, add='%sMaterialGroup'%nameShade )
            break
 
##--------------------------------------------------------------------------------------------------------------------------------------

def makeOrangeCol():  

    listMat = mc.ls(mat = True)
    listMatCol = ["Ol"]      
    ## check there is shade yet
    for i in listMat:  
        nameShader = 'orangeShader'   
        if i == nameShader :
            print 'yes!!'
            listMatCol.insert( 0, i)
                                                  
        else :
            print 'no color yet'
            
    ## it will make shade if they is no shade yet
    for a in listMatCol:    
       
        if a == 'orangeShader' :
            print 'there is shade already'  
            break
                                                            
        else :
            print 'make Color'
            #makeColor
            blueCol = 1, 0.6, 0.4
            print blueCol
            vCol = blueCol
            print vCol
            nameShade = 'orange'
            mc.sets( name='%sMaterialGroup'%nameShade, renderable=True, empty=True )
            mc.shadingNode( 'blinn', name='%sShader'%nameShade, asShader=True )
            mc.setAttr( '%sShader.color'%nameShade, vCol[0],vCol[1],vCol[2] , type='double3', )
            mc.setAttr('%sShader.eccentricity'%nameShade, 0.6) 
            mc.setAttr('%sShader.specularRollOff'%nameShade, 0.3) 
            mc.setAttr('%sShader.specularColor'%nameShade, 0.15, 0.15, 0.15 , type = 'double3') 
            mc.surfaceShaderList( '%sShader'%nameShade, add='%sMaterialGroup'%nameShade )
            break
            
    
            
   
###----------------------------------------------------------------------------------------------------------
###-select Geo to Assing Color-----------------------------------------------------------------------------------------------
##--------------------------------------------------------------------------------------------------------------

def selsPinkGeoSRU(*args):
    
    listStart = mc.ls(sl = True)
    print listStart
    numList = len(listStart)
    print numList
    if  numList == 0 :
        print 'You have to select a charecter'
        
    elif numList >= 0 :
        print 'Yes!!!' 
        mc.select(hi = True)
        selsGeoAll = mc.ls(sl = True, shapes = True)
        print 'selsGeoAll:', selsGeoAll
        LGBlueNoNameSpace = ['ShirtsButtonA_GeoShape','ShirtsButtonB_GeoShape','ShirtsButtonC_GeoShape','ShirtsButtonD_GeoShape',
                              'PantsButtonA_GeoShape','PantsButtonB_GeoShape','PantsButtonC_GeoShape','Button_GeoShape',
                              'BeltSim_GeoShape','ButtonSim_GeoShape']
                              
        nameShade = 'pink' 
        
        for b in range(len(selsGeoAll)):
            if '|' in selsGeoAll[b]:
                print "Yes ||||"
                listSplit = selsGeoAll[b].split('|')[-1]
                print listSplit
                for item in LGBlueNoNameSpace:
                    if listSplit == item:
                        print 'OK yessss'
                        print listSplit
                        print item
                        print selsGeoAll[b]
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade )  
                        
                    else:
                        print 'Noooooo'
                
            else:
                print "No ||||"
                for item in LGBlueNoNameSpace:
                    if selsGeoAll[b] == item:
                        print selsGeoAll[b]
                        print item        
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade ) 
                        
###-----------------------------------------------------------------------------------------------------------------
def selsWhiteGeoSRU(*args):
    
    listStart = mc.ls(sl = True)
    print listStart
    numList = len(listStart)
    print numList
    if  numList == 0 :
        print 'You have to select a charecter'
        
    elif numList >= 0 :
        print 'Yes!!!' 
        mc.select(hi = True)
        selsGeoAll = mc.ls(sl = True, shapes = True)
        print 'selsGeoAll:', selsGeoAll
        LGBlueNoNameSpace = ['Eyeball_L_GeoShape','Cornea_L_GeoShape','Eyeball_R_GeoShape','Cornea_R_GeoShape','TeethUp_GeoShape',
                             'TeethDn_GeoShape','EyeDot_R_GeoShape','EyeDot_L_GeoShape','LowerTeeth_GeoShape','UpperTeeth_GeoShape',
                             'HairBand_GeoShape','Shirts_GeoShape','Nail_GeoShape','Shirts_DYNShape','ShirtsSim_GeoShape']
        nameShade = 'white' 
        
        for b in range(len(selsGeoAll)):
            if '|' in selsGeoAll[b]:
                print "Yes ||||"
                listSplit = selsGeoAll[b].split('|')[-1]
                print listSplit
                for item in LGBlueNoNameSpace:
                    if listSplit == item:
                        print 'OK yessss'
                        print listSplit
                        print item
                        print selsGeoAll[b]
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade )  
                        
                    else:
                        print 'Noooooo'
                
            else:
                print "No ||||"
                for item in LGBlueNoNameSpace:
                    if selsGeoAll[b] == item:
                        print selsGeoAll[b]
                        print item        
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade ) 
                        
###-----------------------------------------------------------------------------------------------------------------
def selsYellowGeoSRU(*args):
    
    listStart = mc.ls(sl = True)
    print listStart
    numList = len(listStart)
    print numList
    if  numList == 0 :
        print 'You have to select a charecter'
        
    elif numList >= 0 :
        print 'Yes!!!' 
        mc.select(hi = True)
        selsGeoAll = mc.ls(sl = True, shapes = True)
        print 'selsGeoAll:', selsGeoAll
        LGBlueNoNameSpace = ['Rope_GeoShape','ButtonB_GeoShape','ButtonA_GeoShape','Shirt_GeoShape','Shirt_DYNShape','ShirtSim_GeoShape']

        nameShade = 'yellow' 
        
        for b in range(len(selsGeoAll)):
            if '|' in selsGeoAll[b]:
                print "Yes ||||"
                listSplit = selsGeoAll[b].split('|')[-1]
                print listSplit
                for item in LGBlueNoNameSpace:
                    if listSplit == item:
                        print 'OK yessss'
                        print listSplit
                        print item
                        print selsGeoAll[b]
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade )  
                        
                    else:
                        print 'Noooooo'
                
            else:
                print "No ||||"
                for item in LGBlueNoNameSpace:
                    if selsGeoAll[b] == item:
                        print selsGeoAll[b]
                        print item        
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade )  
                        
###---------------------------------------------------------------------------------------------------------------------------------                        
def selsGreenGeoSRU():
    
    listStart = mc.ls(sl = True)
    print listStart
    numList = len(listStart)
    print numList
    if  numList == 0 :
        print 'You have to select a charecter'
        
    elif numList >= 0 :
        print 'Yes!!!' 
        mc.select(hi = True)
        selsGeoAll = mc.ls(sl = True, shapes = True)
        print 'selsGeoAll:', selsGeoAll
        LGBlueNoNameSpace = ['Belt_GeoShape', 'SholderArmor1_GeoShape','Shoes_GeoShape']
        nameShade = 'green' 
        
        for b in range(len(selsGeoAll)):
            if '|' in selsGeoAll[b]:
                print "Yes ||||"
                listSplit = selsGeoAll[b].split('|')[-1]
                print listSplit
                for item in LGBlueNoNameSpace:
                    if listSplit == item:
                        print 'OK yessss'
                        print listSplit
                        print item
                        print selsGeoAll[b]
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade )  
                        
                    else:
                        print 'Noooooo'
                
            else:
                print "No ||||"
                for item in LGBlueNoNameSpace:
                    if selsGeoAll[b] == item:
                        print selsGeoAll[b]
                        print item        
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade )   
##------------------------------------------------------------------------------------------------------------------------------------- 
def selsSkinGeoSRU():
    
    listStart = mc.ls(sl = True)
    print listStart
    numList = len(listStart)
    print numList
    if  numList == 0 :
        print 'You have to select a charecter'
        
    elif numList >= 0 :
        print 'Yes!!!' 
        mc.select(hi = True)
        selsGeoAll = mc.ls(sl = True, shapes = True)
        print 'selsGeoAll:', selsGeoAll
        LGBlueNoNameSpace = ['Head_GeoShape', 'Body_GeoShape']
        nameShade = 'skin' 
        
        for b in range(len(selsGeoAll)):
            if '|' in selsGeoAll[b]:
                print "Yes ||||"
                listSplit = selsGeoAll[b].split('|')[-1]
                print listSplit
                for item in LGBlueNoNameSpace:
                    if listSplit == item:
                        print 'OK yessss'
                        print listSplit
                        print item
                        print selsGeoAll[b]
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade )  
                        
                    else:
                        print 'Noooooo'
                
            else:
                print "No ||||"
                for item in LGBlueNoNameSpace:
                    if selsGeoAll[b] == item:
                        print selsGeoAll[b]
                        print item        
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade )  
                        
###------------------------------------------------------------------------------------------------------------------------------------------
def selsRedGeoSRU():
    
    listStart = mc.ls(sl = True)
    print listStart
    numList = len(listStart)
    print numList
    if  numList == 0 :
        print 'You have to select a charecter'
        
    elif numList >= 0 :
        print 'Yes!!!' 
        mc.select(hi = True)
        selsGeoAll = mc.ls(sl = True, shapes = True)
        print 'selsGeoAll:', selsGeoAll
        LGBlueNoNameSpace = ['ChestArmor_GeoShape','BeltHead_GeoShape','Bag_GeoShape','Shoes3_GeoShape','Shoes2_GeoShape','Tongue_GeoShape', 
                             'GumUp_GeoShape','GumDn_GeoShape','Caruncle_GeoShape','Gloves1_GeoShape', 'SholderArmor2_GeoShape','Toungue_GeoShape',
                             'UpperGum_GeoShape','LowerGum_GeoShape','headbelt_GeoShape','Headbelt_GeoShape']  
        nameShade = 'red' 
        
        for b in range(len(selsGeoAll)):
            if '|' in selsGeoAll[b]:
                print "Yes ||||"
                listSplit = selsGeoAll[b].split('|')[-1]
                print listSplit
                for item in LGBlueNoNameSpace:
                    if listSplit == item:
                        print 'OK yessss'
                        print listSplit
                        print item
                        print selsGeoAll[b]
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade )  
                        
                    else:
                        print 'Noooooo'
                
            else:
                print "No ||||"
                for item in LGBlueNoNameSpace:
                    if selsGeoAll[b] == item:
                        print selsGeoAll[b]
                        print item        
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade )       
##-------------------------------------------------------------------------------------------------------------- 
def selsBlueGeoSRU():
    
    listStart = mc.ls(sl = True)
    print listStart
    numList = len(listStart)
    print numList
    if  numList == 0 :
        print 'You have to select a charecter'
        
    elif numList >= 0 :
        print 'Yes!!!' 
        mc.select(hi = True)
        selsGeoAll = mc.ls(sl = True, shapes = True)
        print 'selsGeoAll:', selsGeoAll
        LGBlueNoNameSpace = ['GlovePlastic_GeoShape', 'Shoes1_GeoShape', 'Suite_GeoShape','Gloves2_GeoShape','Shoes4_GeoShape',
                             'Pants_GeoShape','Pants_DYNShape','PantsSim_GeoShape']  
        nameShade = 'blue' 
        
        for b in range(len(selsGeoAll)):
            if '|' in selsGeoAll[b]:
                print "Yes ||||"
                listSplit = selsGeoAll[b].split('|')[-1]
                print listSplit
                for item in LGBlueNoNameSpace:
                    if listSplit == item:
                        print 'OK yessss'
                        print listSplit
                        print item
                        print selsGeoAll[b]
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade )  
                        
                    else:
                        print 'Noooooo'
                
            else:
                print "No ||||"
                for item in LGBlueNoNameSpace:
                    if selsGeoAll[b] == item:
                        print selsGeoAll[b]
                        print item        
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade ) 
###--------------------------------------------------------------------------------------------------------------
def selsOrangeGeoSRU():
    
    listStart = mc.ls(sl = True)
    print listStart
    numList = len(listStart)
    print numList
    if  numList == 0 :
        print 'You have to select a charecter'
        
    elif numList >= 0 :
        print 'Yes!!!' 
        mc.select(hi = True)
        selsGeoAll = mc.ls(sl = True, shapes = True)
        print 'selsGeoAll:', selsGeoAll
        LGBlueNoNameSpace = ['Watch1_GeoShape', 'Watch2_GeoShape','WatchGlasses_Geoshape', 'WatchScrew_GeoShape', 
                                'WatchSliver_GeoShape','Radio1_GeoShape','Radio2_GeoShape','Eyelash_GeoShape']  
        nameShade = 'orange' 
        
        for b in range(len(selsGeoAll)):
            if '|' in selsGeoAll[b]:
                print "Yes ||||"
                listSplit = selsGeoAll[b].split('|')[-1]
                print listSplit
                for item in LGBlueNoNameSpace:
                    if listSplit == item:
                        print 'OK yessss'
                        print listSplit
                        print item
                        print selsGeoAll[b]
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade )  
                        
                    else:
                        print 'Noooooo'
                
            else:
                print "No ||||"
                for item in LGBlueNoNameSpace:
                    if selsGeoAll[b] == item:
                        print selsGeoAll[b]
                        print item        
                        assignToObj = mc.sets( '%s'%selsGeoAll[b], e=True, forceElement='%sMaterialGroup'%nameShade )  


##--------------------------------------------------------------------------------------------------------------------------------------
def assignButton(*args):
    name = "Button"
    listStart = mc.ls(sl = True)
    print listStart
    numList = len(listStart)
    print numList
    if  numList == 0 :
        print 'You have to select a charecter'
        
    elif numList >= 0 :
        print 'Yes!!!'
        AssColor = mc.button("ASS", query = True, command = True)
        
        makePinkCol()
        makeWhiteCol()         
        makeYellowCol()        
        makeGreenCol()  
        makeSkinCol()        
        makeRedCol()        
        makeBlueCol()        
        makeOrangeCol()
        mc.select(listStart[0])
        selsPinkGeoSRU()
        selsWhiteGeoSRU()
        selsYellowGeoSRU()
        selsGreenGeoSRU()
        selsSkinGeoSRU()
        selsRedGeoSRU()
        selsBlueGeoSRU()
        selsOrangeGeoSRU()
       
              
                
           
        
               
    
    

##------run--------------------------------------------------------------------------------------------    

def run ( *args ) :

    windowNameCol = "AssignColor"

    if (mc.window(windowNameCol, exists = True )):
        mc.deleteUI(windowNameCol)

    windowNameCol = mc.window(windowNameCol, title = windowNameCol, widthHeight=(200, 600), sizeable = False)                                           
    ColA = mc.columnLayout(adjustableColumn = True)
    separatorA = mc.separator(height = 8)
    ASS= mc.button("ASS", label = "Assign Color", command = assignButton , width = 80, h = 30, 
                       backgroundColor = [0.443137, 0.776471, 0.443137], enable = True )
    separatorA = mc.separator(height = 2)
   
       

    mc.showWindow()
    
    
run ( )


