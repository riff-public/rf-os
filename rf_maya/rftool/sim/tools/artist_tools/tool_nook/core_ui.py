import sys 
import os 
import pymel.core as pm
import maya.cmds as mc 

selfPath = os.path.realpath (__file__) 
selfName = os.path.basename (__file__) 
selfPath = selfPath.replace ( selfName , '' ) 
selfPath = selfPath.replace ( '\\' , '/' ) 

#D:\NookScript\core\rf_maya\rftool\sim\tools\artist_tools\tool_nook
from tools.artist_tools.tool_nook.helpTools import reorderDeformers
reload(reorderDeformers)
#/////////////////////////////////////////////////////////////////////////////////////////////////

def checkUniqueness ( ui ) :
    if pm.window (ui,exists = True):
       pm.deleteUI(ui)
       checkUniqueness ( ui )
         
#///////////////////////////////////////////////////////////////////////////////////////////////
    
def putButtonA (*args) :
    Nook = "Phatpinya Hamakoon"
    print Nook
    
    
def putButtonB (*args) :
    Phatpinya = "Nook"
    print Phatpinya
    
#/////////////////////////////////////////////////////////////////////////////////////////////

def gui ( *args ):

    uiName = "NookTools_ui"
    titleName = "NookTools Utilites"
    w = 300.0
    h = 300.0
    
    checkUniqueness(ui = uiName)
    window = pm.window ( uiName,title = titleName,mnb = True,mxb = False,rtf = True,sizeable = False)
    pm.window (uiName, e = True, w = w,h = h)

    #////////////////////////////////////////////////////////////////////////////////////////

    with window :

         mainLayout = pm.rowColumnLayout (nc = 1, w = w,h = h,adjustableColumn = True)

         with mainLayout :

              #pm.button( label = "Button A" ,c = putButtonA, w = w,h = h/4)
              pm.button( label = "reorderDeformers" ,c = reorderDeformers.run , w = w,h = h/4)
              pm.button( label = "Button A" ,c = putButtonA, w = w,h = h/4)
    
              subLayout = pm.rowColumnLayout (nc = 2, cw = [( 1 ,w/2),(2,w/2)])

              with subLayout :
                   with pm.rowColumnLayout (nc =1 ,w = w/2,h = h,adjustableColumn = True) :
                        pm.button ( label = "Button B",c = putButtonB, w = w/2,h = h/2)
                   with pm.rowColumnLayout (nc = 1, w= w/2,h = h,adjustableColumn = True) :
                        pm.button (label = "Button B",c = putButtonB,w = w/2,h = h/2)
            
    window.show()
    
    #//////////////////////////////////////////////////////////////////////////////////////



