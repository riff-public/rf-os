import pymel.core as pm 
import maya.cmds as mc 
import maya.mel as mel

def convert_reorderDeformers(*args): #Edit Shape Hair Sim
    target = pm.ls (sl = True , o = True)
    histories = pm.listHistory(target, gl=True, pdo=True, lf=True, f=False, il=2)
    last_blend_shape = None
    for h in histories:
        object_type = pm.objectType(h)
        if object_type == "ffd": 
            last_Lattice = h
            print last_Lattice
            continue
        if object_type == "blendShape":
            last_blendShape = h
            print last_blendShape

    pm.reorderDeformers(str(last_Lattice), str(last_blendShape), target)

def run (*args):
    convert_reorderDeformers()
