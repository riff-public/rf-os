import maya.cmds as mc ;
import pymel.core as pm ;


def getTime_01 (*args):    
    T_nucleus1 = float(mc.floatField('nu01_intF',q=True,v=True))    
    mc.setAttr('nucleus1.timeScale',T_nucleus1)
    
def getTime_02 (*args):    
    T_nucleus2 = float(mc.floatField('nu02_intF',q=True,v=True))    
    mc.setAttr('nucleus2.timeScale',T_nucleus2)
    
def getTime_03 (*args):    
    T_nucleus3 = float(mc.floatField('nu03_intF',q=True,v=True))
    mc.setAttr('nucleus3.timeScale',T_nucleus3)
    
def getTime_04 (*args):    
    T_nucleus4 = float(mc.floatField('nu04_intF',q=True,v=True))
    mc.setAttr('nucleus4.timeScale',T_nucleus4)
    
def getTimeP (*args):
    f_plus = float(mc.floatField('nu_intF',q=True,v=True))
    
    T_nucleus1 = mc.getAttr('nucleus1.timeScale')
    T_nucleus2 = mc.getAttr('nucleus2.timeScale')
    T_nucleus3 = mc.getAttr('nucleus3.timeScale')
    T_nucleus4 = mc.getAttr('nucleus4.timeScale')
    
    mc.setAttr('nucleus1.timeScale',T_nucleus1+f_plus)
    mc.setAttr('nucleus2.timeScale',T_nucleus2+f_plus)
    mc.setAttr('nucleus3.timeScale',T_nucleus3+f_plus)
    mc.setAttr('nucleus4.timeScale',T_nucleus4+f_plus)

    T_nucleus1 = mc.getAttr('nucleus1.timeScale')
    mc.floatField('nu01_intF',edit=True,v=T_nucleus1 , h=25)
    T_nucleus2 = mc.getAttr('nucleus2.timeScale')
    mc.floatField('nu02_intF',edit=True,v=T_nucleus2 , h=25)
    T_nucleus3 = mc.getAttr('nucleus3.timeScale')
    mc.floatField('nu03_intF',edit=True,v=T_nucleus3 , h=25)
    T_nucleus4 = mc.getAttr('nucleus4.timeScale')
    mc.floatField('nu04_intF',edit=True,v=T_nucleus4 , h=25)

def getTimeQ (*args):
    f_plus = float(mc.floatField('nu_intF',q=True,v=True))
    
    T_nucleus1 = mc.getAttr('nucleus1.timeScale')
    T_nucleus2 = mc.getAttr('nucleus2.timeScale')
    T_nucleus3 = mc.getAttr('nucleus3.timeScale')
    T_nucleus4 = mc.getAttr('nucleus4.timeScale')
    
    mc.setAttr('nucleus1.timeScale',T_nucleus1-f_plus)
    mc.setAttr('nucleus2.timeScale',T_nucleus2-f_plus)
    mc.setAttr('nucleus3.timeScale',T_nucleus3-f_plus)
    mc.setAttr('nucleus4.timeScale',T_nucleus4-f_plus)

    T_nucleus1 = mc.getAttr('nucleus1.timeScale')
    mc.floatField('nu01_intF',edit=True,v=T_nucleus1 , h=25)
    T_nucleus2 = mc.getAttr('nucleus2.timeScale')
    mc.floatField('nu02_intF',edit=True,v=T_nucleus2 , h=25)
    T_nucleus3 = mc.getAttr('nucleus3.timeScale')
    mc.floatField('nu03_intF',edit=True,v=T_nucleus3 , h=25)
    T_nucleus4 = mc.getAttr('nucleus4.timeScale')
    mc.floatField('nu04_intF',edit=True,v=T_nucleus4 , h=25)
    
def getRefresh(*args):
    T_nucleus1 = mc.getAttr('nucleus1.timeScale')
    mc.floatField('nu01_intF',edit=True,v=T_nucleus1 , h=25)
    T_nucleus2 = mc.getAttr('nucleus2.timeScale')
    mc.floatField('nu02_intF',edit=True,v=T_nucleus2 , h=25)
    T_nucleus3 = mc.getAttr('nucleus3.timeScale')
    mc.floatField('nu03_intF',edit=True,v=T_nucleus3 , h=25)
    T_nucleus4 = mc.getAttr('nucleus4.timeScale')
    mc.floatField('nu04_intF',edit=True,v=T_nucleus4 , h=25)

    Gra1 = mc.getAttr('nucleus1.gravity')
    Gra2 = mc.getAttr('nucleus2.gravity')
    Gra3 = mc.getAttr('nucleus3.gravity')
    Gra4 = mc.getAttr('nucleus4.gravity')
    Gra = (Gra1+Gra2+Gra3+Gra4)/4
    mc.floatField('Gra_intF',edit=True,v=Gra , h=25)

def setTimeAll(*args):
    T_nucleus1 = float(mc.floatField('nu01_intF',q=True,v=True))    
    T_nucleus2 = float(mc.floatField('nu02_intF',q=True,v=True))
    T_nucleus3 = float(mc.floatField('nu03_intF',q=True,v=True))
    T_nucleus4 = float(mc.floatField('nu04_intF',q=True,v=True))

    mc.setAttr('nucleus1.timeScale',T_nucleus1)    
    mc.setAttr('nucleus2.timeScale',T_nucleus2)
    mc.setAttr('nucleus3.timeScale',T_nucleus3)
    mc.setAttr('nucleus4.timeScale',T_nucleus4)

def setgrav(*args):
    Gra = float(mc.floatField('Gra_intF',q=True,v=True))
    mc.setAttr('nucleus1.gravity',Gra)    
    mc.setAttr('nucleus2.gravity',Gra)
    mc.setAttr('nucleus3.gravity',Gra)
    mc.setAttr('nucleus4.gravity',Gra)

def timeGui ():
    
    T_nucleus1 = mc.getAttr('nucleus1.timeScale')
    T_nucleus2 = mc.getAttr('nucleus2.timeScale')
    T_nucleus3 = mc.getAttr('nucleus3.timeScale')
    T_nucleus4 = mc.getAttr('nucleus4.timeScale')

    Gra = mc.getAttr('nucleus1.gravity')
    
    if ( mc.window('time_window',q=True,ex=True) ):
        mc.deleteUI('time_window',window=True)
        
    mc.window('time_window',t=' Time Scale')
    mc.columnLayout('main_Layout',adj=True)

    mc.separator (style='double',w=250,h=10)
    mc.button(l='Refresh',c=getRefresh,bgc=[0,1,1])
    mc.separator (style='double',w=250,h=10)

    mc.rowLayout('Gra_row',numberOfColumns=3)
    mc.text('Gra_text',l=' Gravity         ')
    mc.floatField('Gra_intF',v=Gra,precision=3, h=25)
    mc.button('Gra_but',l=' Apply ',bgc=[1,1,0],h=25,w=102,c=setgrav)
    mc.setParent('..')

    mc.separator (style='double',w=250,h=10)
    
    mc.rowLayout('nu01_row',numberOfColumns=3)
    mc.text('nu01_text',l=' Time Scale1 ')
    mc.floatField('nu01_intF',v=T_nucleus1 ,precision=3, h=25)
    mc.button('nu01_but',l=' Apply ',bgc=[1,1,0],h=25,w=102,c=getTime_01)
    mc.setParent('..')
    
    mc.rowLayout('nu02_row',numberOfColumns=3)
    mc.text('nu02_text',l=' Time Scale2 ')
    mc.floatField('nu02_intF',value=T_nucleus2,precision=3,h=25)
    mc.button('nu02_but',l=' Apply ',bgc=[1,1,0],h=25,w=102,c=getTime_02)
    mc.setParent('..')
    
    mc.rowLayout('nu03_row',numberOfColumns=3)
    mc.text('nu03_text',l=' Time Scale3 ')
    mc.floatField('nu03_intF',value=T_nucleus3,precision=3,h=25)
    mc.button('nu03_but',l=' Apply ',bgc=[1,1,0],h=25,w=102,c=getTime_03)
    mc.setParent('..')
    
    mc.rowLayout('nu04_row',numberOfColumns=3)
    mc.text('nu04_text',l=' Time Scale4 ')
    mc.floatField('nu04_intF',value=T_nucleus4,precision=3,h=25)
    mc.button('nu04_but',l=' Apply ',bgc=[1,1,0],h=25,w=102,c=getTime_04)
    mc.setParent('..')

    mc.separator (style='double',w=250,h=10)
    mc.button(l='Apply All Time Scale',c=setTimeAll,bgc=[0,1,1])
    mc.separator (style='double',w=250,h=10)
    
    mc.rowLayout('nu_row',numberOfColumns=4)
    mc.text('nu_text',l='       + or -     ')
    mc.floatField('nu_intF',value=0,precision=3,h=25)
    mc.button('nuP_but',l=' + ',bgc=[1,1,0],h=25,w=50,c=getTimeP)
    mc.button('nuQ_but',l=' - ',bgc=[1,1,0],h=25,w=50,c=getTimeQ)
    mc.setParent('..')

    mc.separator (style='double',w=250,h=10)
    
    mc.showWindow('time_window')
    mc.window('time_window',e=True,wh=[235,300])
        
timeGui();
