import pymel.core as pm ;
import maya.cmds as mc

def extrudeTube (*args):
	axes = ( 'x' , 'y' , 'z' ) ;
	obj = mc.ls(selection=True)
	listobj = mc.listRelatives(obj) 
	# print listobj
	for each in listobj:
		objectsTyp = mc.pickWalk(each,d='down')
		objType = mc.objectType(objectsTyp)

	if objType == 'nurbsCurve':
		exNoSurList = []
		grplist = []
		for group in obj :

			if '_' in group:
				grpName = group.split ( '_' ) [0]
			else :
				grpName = group

			curveList =  pm.listRelatives(group,children=True)
			baseList = [ ]
			extrudeList = [ ]

			for curv in curveList:
				
				circle = pm.circle ( n = curv.split('_')[0] + '_base' ) [0]
				
				baseList.append ( circle )
				
				for axis in axes :
					pm.setAttr ( circle + '.s' + axis , 0.3 )
				
				extrude = pm.extrude ( circle , curv , ch = True , rn = False , po = 0 , et = 2 , ucp = 0 , fpt = 0 , upn = 0 , rsp = 1 ) [0]
				
				extrudeList.append ( extrude )
			
			grpbase = pm.group ( baseList , n = grpName + '_baseGRP' )
			grplist.append(grpbase)
			
			grpex = pm.group ( extrudeList , n = grpName + '_tubeGRP' )
			grplist.append(grpex)

			### connect nodes so that the base follows hair curves
		
			for i in range ( 0 , len ( curveList ) ) :
				# print curveList
				curve = curveList [i]
				curveShape = pm.listRelatives ( curve , shapes =
				 True ) [0]
				base = baseList [i]
				
				mtp = pm.createNode ( 'motionPath' , n = curve + '_mtp' )
				pm.setAttr ( mtp + '.sideTwist' , 90 )     
				pm.connectAttr ( curveShape + '.worldSpace[0]' , mtp + '.geometryPath' )        
				pm.connectAttr ( mtp + '.rotateX' , base + '.rotateX' )
				pm.connectAttr ( mtp + '.rotateY' , base + '.rotateY' )
				pm.connectAttr ( mtp + '.rotateZ' , base + '.rotateZ' )
				pm.connectAttr ( mtp + '.rotateOrder' , base + '.rotateOrder' )
				pm.connectAttr ( mtp + '.message' , base + '.specifiedManipLocation' )
				
				adlX = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlX' )
				adlY = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlY' )
				adlZ = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlZ' )
				
				pm.connectAttr ( base + '.transMinusRotatePivotX' , adlX + '.input1' )
				pm.connectAttr ( mtp + '.xCoordinate' , adlX + '.input2' )
				pm.connectAttr ( adlX + '.output' , base + '.translateX' )
				
				pm.connectAttr ( base + '.transMinusRotatePivotY' , adlY + '.input1' )
				pm.connectAttr ( mtp + '.yCoordinate' , adlY + '.input2' )
				pm.connectAttr ( adlY + '.output' , base + '.translateY' )
				
				pm.connectAttr ( base + '.transMinusRotatePivotZ' , adlZ + '.input1' )
				pm.connectAttr ( mtp + '.zCoordinate' , adlZ + '.input2' )
				pm.connectAttr ( adlZ + '.output' , base + '.translateZ' )

		for ex in extrudeList:
			pm.reverseSurface(ex,d=0,ch=1)
			
		grpAttr = mc.group(n='HairTube_GRP',em=True)
		mc.addAttr(grpAttr,ln='TubeScale',at = 'double',keyable=True)
		mc.setAttr(grpAttr+".TubeScale",0.1)
		mc.addAttr(grpAttr,ln='TipScale',at = 'double',keyable=True)
		mc.setAttr(grpAttr+".TipScale", 1)

		for grp in grplist:
			mc.parent(str(grp),grpAttr)

		for ex in extrudeList:
			exNoSur = ex.split('dSurface')
			exNoSur = exNoSur[0]+exNoSur[-1]
			exNoSurList.append(exNoSur)

		for i in range(len(extrudeList)):
			mc.connectAttr(grpAttr+'.TubeScale',baseList[i]+'.scaleX')
			mc.connectAttr(grpAttr+'.TubeScale',baseList[i]+'.scaleY')
			mc.connectAttr(grpAttr+'.TubeScale',baseList[i]+'.scaleZ')
			mc.connectAttr(grpAttr+'.TipScale',exNoSurList[i]+'.scale')

	elif objType == 'transform':
		extrudeLists = []
		baseLists = []
		exNoSurList = []
		grplist = []
		for group in listobj:
			# print group
			if '_' in group:
				grpName = group.split ( '_' ) [0]
			else :
				grpName = group

			curveList =  pm.listRelatives(group,children=True)
			baseList = [ ]
			extrudeList = [ ]

			for curv in curveList:
				
				circle = pm.circle ( n = curv.split('_')[0] + '_base' ) [0]
				
				baseList.append ( circle )
				
				for axis in axes :
					pm.setAttr ( circle + '.s' + axis , 0.3 )
				
				extrude = pm.extrude ( circle , curv , ch = True , rn = False , po = 0 , et = 2 , ucp = 0 , fpt = 0 , upn = 0 , rsp = 1 ) [0]
				
				extrudeList.append ( extrude )
			
			grpbase = pm.group ( baseList , n = grpName + '_baseGRP' )
			grplist.append(grpbase)
			
			grpex = pm.group ( extrudeList , n = grpName + '_tubeGRP' )
			grplist.append(grpex)

			### connect nodes so that the base follows hair curves
		 
			for i in range ( 0 , len ( curveList ) ) :
				# print curveList
				curve = curveList [i]
				curveShape = pm.listRelatives ( curve , shapes = True ) [0]
				base = baseList [i]
				
				mtp = pm.createNode ( 'motionPath' , n = curve + '_mtp' )
				pm.setAttr ( mtp + '.sideTwist' , 90 )     
				pm.connectAttr ( curveShape + '.worldSpace[0]' , mtp + '.geometryPath' )        
				pm.connectAttr ( mtp + '.rotateX' , base + '.rotateX' )
				pm.connectAttr ( mtp + '.rotateY' , base + '.rotateY' )
				pm.connectAttr ( mtp + '.rotateZ' , base + '.rotateZ' )
				pm.connectAttr ( mtp + '.rotateOrder' , base + '.rotateOrder' )
				pm.connectAttr ( mtp + '.message' , base + '.specifiedManipLocation' )
				
				adlX = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlX' )
				adlY = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlY' )
				adlZ = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlZ' )
				
				pm.connectAttr ( base + '.transMinusRotatePivotX' , adlX + '.input1' )
				pm.connectAttr ( mtp + '.xCoordinate' , adlX + '.input2' )
				pm.connectAttr ( adlX + '.output' , base + '.translateX' )
				
				pm.connectAttr ( base + '.transMinusRotatePivotY' , adlY + '.input1' )
				pm.connectAttr ( mtp + '.yCoordinate' , adlY + '.input2' )
				pm.connectAttr ( adlY + '.output' , base + '.translateY' )
				
				pm.connectAttr ( base + '.transMinusRotatePivotZ' , adlZ + '.input1' )
				pm.connectAttr ( mtp + '.zCoordinate' , adlZ + '.input2' )
				pm.connectAttr ( adlZ + '.output' , base + '.translateZ' )
			
			for ex in extrudeList:
				pm.reverseSurface(ex,d=0,ch=1)
				exNoSur = ex.split('dSurface')
				exNoSur = exNoSur[0]+exNoSur[-1]
				exNoSurList.append(exNoSur)
				extrudeLists.append(ex)
			for base in baseList:
				baseLists.append(base)
		# print exNoSurList
		# print baseLists
		# print extrudeLists
		# print grplist
		grpAttr = mc.group(n='HairTube_GRP',em=True)
		mc.addAttr(grpAttr,ln='TubeScale',at = 'double',keyable=True)
		mc.setAttr(grpAttr+".TubeScale",0.1)
		mc.addAttr(grpAttr,ln='TipScale',at = 'double',keyable=True)
		mc.setAttr(grpAttr+".TipScale", 1)

		for grp in grplist:
			mc.parent(str(grp),grpAttr)

		for i in range(len(extrudeLists)):
			a=i+1
			mc.connectAttr(grpAttr+'.TubeScale',baseLists[i]+'.scaleX')
			mc.connectAttr(grpAttr+'.TubeScale',baseLists[i]+'.scaleY')
			mc.connectAttr(grpAttr+'.TubeScale',baseLists[i]+'.scaleZ')
			mc.connectAttr(grpAttr+'.TipScale',exNoSurList[i]+'.scale')

	else : print 'mai me crv hai def tum'
extrudeTube()