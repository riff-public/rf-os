import maya.cmds as mc ;
import pymel.core as pm ;

clothSys = pm.ls(type = 'nCloth');
def getNcloth(*args):
	clothSys = pm.ls(type = 'nCloth');

	nclothlist = pm.textScrollList( "ts_nclothlist" ,  q = True , selectItem = True );
	return nclothlist
#collisions
def setThickness ( *args ) :
	name = getNcloth ( ) ;
	for each in name :
		thickness = mc.floatField ('ff_thickness' , q = True ,  v = True ) ;
		pm.setAttr(each+'.thickness',thickness)

def getNcloth_Enable ( *args ) :
	name = getNcloth()
	for each in name :
		Enable = pm.checkBox ('cb_Enable' , q=True , v=True )
		pm.setAttr(each+'.isDynamic',Enable)

def getNcloth_Collide ( *args ) :
	name = getNcloth()
	for each in name :
		Collide = pm.checkBox ('cb_Collide' , q=True , v=True )
		pm.setAttr(each+'.collide',Collide)

def getNcloth_Scollide ( *args ) :
	name = getNcloth()
	for each in name :
		SCollide = pm.checkBox ('cb_SCollide' , q=True , v=True )
		pm.setAttr(each+'.selfCollide',SCollide)

def setselfCollide ( *args ) :
	name = getNcloth()
	for each in name :
		selfCollideWidthScale = mc.floatField ('ff_selfcoll' , q = True ,  v = True ) ;
		pm.setAttr(each+'.selfCollideWidthScale',selfCollideWidthScale)

def setfriction ( *args ) :
	name = getNcloth ( ) ;
	for each in name :
		friction = mc.floatField ('ff_friction' , q = True ,  v = True ) ;
		pm.setAttr(each+'.friction',friction)

def setstickiness ( *args ) :
	name = getNcloth ( ) ;
	for each in name :
		stickiness = mc.floatField ('ff_stickiness' , q = True ,  v = True ) ;
		pm.setAttr(each+'.stickiness',stickiness)

#Dynamic
def setstretch ( *args ) :
	name = getNcloth ( ) ;
	for each in name :
		stretchResistance = mc.floatField ('ff_stretch' , q = True ,  v = True ) ;
		pm.setAttr(each+'.stretchResistance',stretchResistance)

def setcompressionResistance ( *args ) :
	name = getNcloth ( ) ;
	for each in name :
		compressionResistance = mc.floatField ('ff_compression' , q = True ,  v = True ) ;
		pm.setAttr(each+'.compressionResistance',compressionResistance)

def setbendResistance ( *args ) :
	name = getNcloth ( ) ;
	for each in name :
		bendResistance = mc.floatField ('ff_bend' , q = True ,  v = True ) ;
		pm.setAttr(each+'.bendResistance',bendResistance)

def setrigidity ( *args ) :
	name = getNcloth ( ) ;
	for each in name :
		rigidity = mc.floatField ('ff_rigidity' , q = True ,  v = True ) ;
		pm.setAttr(each+'.rigidity',rigidity)

def setinputMeshAttract ( *args ) :
	name = getNcloth ( ) ;
	for each in name :
		inputMeshAttract = mc.floatField ('ff_input' , q = True ,  v = True ) ;
		pm.setAttr(each+'.inputMeshAttract',inputMeshAttract)

def setpointMass ( *args ) :
	name = getNcloth ( ) ;
	for each in name :
		pointMass = mc.floatField ('ff_mass' , q = True ,  v = True ) ;
		pm.setAttr(each+'.pointMass',pointMass)

def setlift ( *args ) :
	name = getNcloth ( ) ;
	for each in name :
		lift = mc.floatField ('ff_lift' , q = True ,  v = True ) ;
		pm.setAttr(each+'.lift',lift)

def setdrag ( *args ) :
	name = getNcloth ( ) ;
	for each in name :
		drag = mc.floatField ('ff_drag' , q = True ,  v = True ) ;
		pm.setAttr(each+'.drag',drag)

def setdamp ( *args ) :
	name = getNcloth ( ) ;
	for each in name :
		damp = mc.floatField ('ff_damp' , q = True ,  v = True ) ;
		pm.setAttr(each+'.damp',damp)

def setstretchDamp ( *args ) :
	name = getNcloth ( ) ;
	for each in name :
		stretchDamp = mc.floatField ('ff_sdamp' , q = True ,  v = True ) ;
		pm.setAttr(each+'.stretchDamp',stretchDamp)

def queryNcloth ( Ncloth = '' , *args ) :

	attributeValues = [] ;

	Enable = pm.getAttr(Ncloth+'.isDynamic')
	collide = pm.getAttr(Ncloth+'.collide')
	selfCollide = pm.getAttr(Ncloth+'.selfCollide')
	attributeValues.extend ( [ Enable , collide , selfCollide ] ) ;

	#collisions
	thickness = pm.getAttr ( Ncloth + '.' + 'thickness' ) ;
	selfCollideWidthScale = pm.getAttr ( Ncloth + '.' + 'selfCollideWidthScale' ) ;
	friction = pm.getAttr ( Ncloth + '.' + 'friction' ) ;
	stickiness = pm.getAttr ( Ncloth + '.' + 'stickiness' ) ;
	attributeValues.extend ( [ thickness , selfCollideWidthScale , friction , stickiness ] ) ;

	#Dynamic
	stretchResistance = pm.getAttr(Ncloth+'.stretchResistance')
	compressionResistance = pm.getAttr(Ncloth+'.compressionResistance')
	bendResistance = pm.getAttr(Ncloth+'.bendResistance')
	rigidity = pm.getAttr(Ncloth+'.rigidity')
	inputMeshAttract = pm.getAttr(Ncloth+'.inputMeshAttract')
	pointMass = pm.getAttr(Ncloth+'.pointMass')
	lift = pm.getAttr(Ncloth+'.lift')
	drag = pm.getAttr(Ncloth+'.drag')
	damp = pm.getAttr(Ncloth+'.damp')
	stretchDamp = pm.getAttr(Ncloth+'.stretchDamp')
	attributeValues.extend ( [ stretchResistance, compressionResistance, bendResistance, rigidity, inputMeshAttract, pointMass, lift, drag, damp,stretchDamp ] ) ;

	return ( attributeValues ) ;

def setClothAttribute ( Ncloth = '' , *args ) :

	attributeValues = queryNcloth ( Ncloth = Ncloth ) ;

	pm.checkBox ('cb_Enable' , e=True , v = attributeValues [0])
	pm.checkBox ('cb_Collide' , e=True , v = attributeValues [1])
	pm.checkBox ('cb_SCollide' , e=True , v = attributeValues [2])

	#collisions
	pm.floatField ( 'ff_thickness' , e = True , v = attributeValues [3] ) ;
	pm.floatField ( 'ff_selfcoll' , e = True , v = attributeValues [4] ) ;
	pm.floatField ( 'ff_friction' , e = True , v = attributeValues [5] ) ;
	pm.floatField ( 'ff_stickiness' , e = True , v = attributeValues [6] ) ;

	#Dynamic
	pm.floatField ('ff_stretch' , e=True , v=attributeValues [7] )
	pm.floatField ('ff_compression' , e=True , v=attributeValues [8] )
	pm.floatField ('ff_bend' , e=True , v=attributeValues [9] )
	pm.floatField ('ff_rigidity' , e=True , v=attributeValues [10] )
	pm.floatField ('ff_input' , e=True , v=attributeValues [11] )
	pm.floatField ('ff_mass' , e=True , v=attributeValues [12] )
	pm.floatField ('ff_lift' , e=True , v=attributeValues [13] )
	pm.floatField ('ff_drag' , e=True , v=attributeValues [14] )
	pm.floatField ('ff_damp' , e=True , v=attributeValues [15] )
	pm.floatField ('ff_sdamp' , e=True , v=attributeValues [16] )

def NclothList_sc ( *args ) :

	Ncloth = getNcloth ( ) ;

	setClothAttribute ( Ncloth = Ncloth[0] ) ;

def updateNclothList ( *args ) :

    nCloth = pm.ls ( type = 'nCloth' ) ;
#    nCloth.sort ( ) ;
 
    pm.textScrollList ( 'ts_nclothlist' , e = True , ra = True ) ;

    for each in nCloth :
        pm.textScrollList ( 'ts_nclothlist' , e = True , append = each ) ;

    pm.textScrollList ( 'ts_nclothlist' , e = True ) ;

    return nCloth ; 

def refresh ( * args ) :
    Ncloth = updateNclothList ( ) ;
    setClothAttribute ( Ncloth = Ncloth[0] ) ;

def ncloth_Gui(*args):

	w=244.00

	if ( mc.window ('ncloth_window',q=True,ex=True) ):
		mc.deleteUI('ncloth_window',window=True)

	mc.window('ncloth_window',t='Ncloth setting')

	mc.rowColumnLayout(nc=1,w=w)
	i = pm.textScrollList('ts_nclothlist', append=clothSys , sc= NclothList_sc , w=243 , h=80 , ams = True)
	mc.setParent('..')
	
	mc.rowColumnLayout(nc=1,w=w)
	pm.button(label="refresh",bgc=(1 ,0.270588 ,0) , c = refresh )

	mc.separator (style="none",w=w,h=4)

	pm.rowColumnLayout( nc=3,cw = [(1,75),(2,75),(3,87)],h=25) ;
	pm.checkBox('cb_Enable',label='Enable',al="center" , cc = getNcloth_Enable , ed = True  );
	pm.checkBox('cb_Collide',label='Collide' , cc = getNcloth_Collide , ed = True) ;
	pm.checkBox('cb_SCollide',label='Self Collide' , cc = getNcloth_Scollide , ed = True ) ;
	pm.setParent('..')


	pm.text(label='Collisions',bgc=(0.854902,0.647059,0.12549),h=20,fn="boldLabelFont")

	pm.rowColumnLayout( nc=2,cw=[(1,140),(2,100)],h=20,cal=(1,"right"));
	pm.text(label="Thickness  ")
	mc.floatField('ff_thickness' , ed=True , cc = setThickness , pre = 3 )
	mc.setParent('..')

	pm.rowColumnLayout( nc=2,cw=[(1,140),(2,100)],h=20,cal=(1,"right"));
	pm.text(label="Self Collide Width Scale  " )
	pm.floatField('ff_selfcoll' , ed=True , cc = setselfCollide , pre = 3 )
	mc.setParent('..')

	pm.rowColumnLayout( nc=2,cw=[(1,140),(2,100)],h=20,cal=(1,"right"));
	pm.text(label="Friction  ")
	pm.floatField('ff_friction', ed=True , cc = setfriction , pre = 3 )
	mc.setParent('..')

	pm.rowColumnLayout( nc=2,cw=[(1,140),(2,100)],h=20,cal=(1,"right"));
	pm.text(label="Stickiness  ")
	pm.floatField('ff_stickiness', ed=True , cc = setstickiness , pre = 3 )
	mc.setParent('..')

	mc.separator (style='double',w=w,h=10)

	pm.text(label='Dynamic Properties',bgc=(1,0.843137,0),h=20,fn="boldLabelFont")

	pm.rowColumnLayout( nc=2,cw=[(1,140),(2,100)],h=20,cal=(1,"right"));
	pm.text(label="Stretch Resistance  ")
	pm.floatField('ff_stretch' , ed=True , cc = setstretch , pre = 3 )
	mc.setParent('..')

	pm.rowColumnLayout( nc=2,cw=[(1,140),(2,100)],h=20,cal=(1,"right"));
	pm.text(label="Compression Resistance  ")
	pm.floatField('ff_compression' , cc = setcompressionResistance , ed=True , pre = 3 )
	pm.setParent('..')

	pm.rowColumnLayout( nc=2,cw=[(1,140),(2,100)],h=20,cal=(1,"right"));
	pm.text(label="Bend Resistance  " )
	pm.floatField('ff_bend'  , ed = True , cc = setbendResistance , pre = 3 )
	pm.setParent('..')

	pm.rowColumnLayout( nc=2,cw=[(1,140),(2,100)],h=20,cal=(1,"right"));
	pm.text(label="Rigidity  ")
	pm.floatField('ff_rigidity' , ed =True , cc = setrigidity , pre = 3 )
	pm.setParent('..')

	pm.rowColumnLayout( nc=2,cw=[(1,140),(2,100)],h=20,cal=(1,"right"));
	pm.text(label="Input Mesh Attract  "  )
	pm.floatField('ff_input' , ed= True , cc = setinputMeshAttract , pre = 3 )
	pm.setParent('..')

	pm.rowColumnLayout( nc=2,cw=[(1,140),(2,100)],h=20,cal=(1,"right"));
	pm.text(label="Mass  ")
	pm.floatField('ff_mass' , ed = True , cc = setpointMass , pre = 3 )
	pm.setParent('..')

	pm.rowColumnLayout( nc=2,cw=[(1,140),(2,100)],h=20,cal=(1,"right"));
	pm.text(label="Lift  ")
	pm.floatField('ff_lift' , ed = True , cc = setlift , pre = 3 )
	pm.setParent('..')

	pm.rowColumnLayout( nc=2,cw=[(1,140),(2,100)],h=20,cal=(1,"right"));
	pm.text(label="Drag  ")
	pm.floatField('ff_drag' , ed = True , cc = setdrag , pre = 3 )
	pm.setParent('..')

	pm.rowColumnLayout( nc=2,cw=[(1,140),(2,100)],h=20,cal=(1,"right"));
	pm.text(label="Damp  ")
	pm.floatField('ff_damp' , ed = True , cc = setdamp , pre = 3 )
	pm.setParent('..')

	pm.rowColumnLayout( nc=2,cw=[(1,140),(2,100)],h=20,cal=(1,"right"));
	pm.text(label="Stretch Damp  ")
	pm.floatField('ff_sdamp' , ed = True, cc = setstretchDamp , pre = 3 )
	pm.setParent('..')


	mc.showWindow('ncloth_window')
	mc.window('ncloth_window',e=True,wh=[245,470])

ncloth_Gui();