- Setup information browse character path.
- Hair DYN Tool.
- Get file. (all chareacter file will reference in scene)
- Duplicate hair group from groom, for example 'HairFront_CRV_Grp'.
- Choose Color For > 'Create Layer Color' click color and switch to 'Select Curve Color' for separate curve to each layer color.

*** Export selection 'Hair_ORI_GRP' to your workspace. 
*** If you already have hair layer, you can import to this scene. 

- Create Hair Tube > set On / Off.
- Number Of Nucleus > set One / Four(will working only four layer color).
- Get your 'ORI_GRP'.
- Get your 'Geo' name for 'Make Selected Curves Dynamic'.
- Select two edges on polygon from reference character CIN.
- Click 'Get', That will going to save Edge and Geo Name.

***For the next hair setup you can 'Load' this information.
***For new Head_Geo but two edges are the same, Click 'Load' > if change 'Head_Geo' > Click 'Get' again.

- Click Run button.
- Hair Tip Constraint form HairORI Layer.
- Random Hair Tube Color > for one nucleus and one hairSystem. > Select object and click button.
- QC OTP, Select output hair group then Select CRV_GRP from groom.
- Copy curve shape from ORI. Duplicate cuve from groom and rename suffix '_SHP'.
- Please check your character setup again.

***For hair setup select two edges at the middle of Head top.
***Dont't forget to make CV blenShape from ORI to OTP for geting hair root shape.