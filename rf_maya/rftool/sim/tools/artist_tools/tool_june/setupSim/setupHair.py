import pymel.core as pm;
import maya.cmds as mc;
import maya.mel as mel;
import os, ast;

mp = os.path.realpath(__file__);
fileName = os.path.basename(__file__);
mp = mp.replace( fileName , '' );
mp = mp.replace( '\\' , '/' );

from rf_maya.rftool.sim.tools.artist_tools.tool_june.setupSim import setupCloth
reload(setupCloth);
from rf_maya.rftool.sim.tools.artist_tools.tool_june import module
reload(module);

class SetupHairDyn( object ):
    def __init__( self ):
        self.module = module.MainModule();

    def fileInfoHairDyn( self,*args ):
        chaPath = pm.textField( 'inputChaPath' , q = True , text = True );
        if chaPath != '':
            chaName = chaPath.split('/')[-1:][0];
            chaGeoPath = chaPath + '/model/main/hero/output/' + str(chaName) + '_mdl_main_md.hero.ma'
            groomGeo = chaPath + '/groom/main/hero/output/' + str(chaName) + '_groomGeo_main_md.hero.abc'

            fileLists = [chaGeoPath.split('/')[-1:], groomGeo.split('/')[-1:]];
            for fileList in fileLists :
                pm.textScrollList( 'fileListsHairDyn' , e = True , a = fileList );

            mc.file( chaGeoPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_CIN');
            mc.file( chaGeoPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_COL');
            mc.file( groomGeo, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_CRV');
        else:
            pm.warning('Character Path Not Found');

    def dupCrvDyn( self,*args ):
        sels = pm.ls( sl = True );
        pm.select(sels, r = True);
        dupCrv = pm.duplicate( rr = True );
        dupSP = dupCrv[0].split('_')[0];
        dupName = pm.rename(dupCrv, dupSP + '_ORI_GRP');
        pm.parent(dupName, w = True);
        pm.select(dupName);
        grpList = pm.ls(sl=True, dag=True, type='transform');
        for crvGrp in grpList:
            SetupHairDyn().deleteOrig( target = crvGrp );
            crvGrp.rename(crvGrp.name().replace('_CRV', '_ORI'));

    def separateHair(self, *args): 
        sels = pm.ls( sl = True );
        pm.select(sels, r = True);
        dupGeo = pm.duplicate( rr = True );
        pm.polySeparate(dupGeo);
        pm.select(dupGeo);  
        dupGeoName = pm.rename(dupGeo[0], dupGeo[0].split('_')[0] + '_GEO_GRP');
        hairName = dupGeoName.split('_')[0];
        pm.parent(w = True);
        pm.delete(ch = True);
        for hairGeo in pm.selected():
            selsGeoGrp = pm.listRelatives( hairGeo );
            for polySurface in selsGeoGrp:
                polySurface.rename(polySurface.name().replace('polySurface', hairName));
                polySurface.rename(polySurface.name() + '_Geo');

    def getHairInfo( self,*args ):
        selected = pm.selected();
        pm.textField('inputOriInfo', e = True, tx = selected[0]);

    def getMainHead( self, *args ):
        sels = pm.ls(selection=True);
        if sels == []:
            pm.warning('No Selection');
        else :
            pm.textField('inputGetMainHead', e = True, tx = sels[0]);

    def getEdge( self, *args ):
        edges = pm.ls ( sl = True );
        try: 
            if len ( edges ) != 2 :
                pm.warning( '2 edges must be selected' );
            else :
                pm.textField( 'inputGetEdge1' , e = True , tx = edges[0].split('.')[1] );
                pm.textField( 'inputGetEdge2' , e = True , tx = edges[1].split('.')[1] );
        except:
            pm.textField( 'inputGetEdge1' , q = True, tx = True);
            pm.textField( 'inputGetEdge2' , q = True, tx = True);

        infoDict = {};
        sels = mc.ls( sl = True );
        cinGeo = sels[0].split('.')[0];
        colorPath = self.module.createFolderTxt();

        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];
        groomGrp = ':' + firstName + '_CRV' ;
        groomNsp = pm.namespaceInfo( groomGrp, listNamespace=True )[0];
        selection = pm.ls(groomNsp, r = True );
        selectionName = selection[0].nodeName();
        getrootGrp = pm.general.PyNode(selectionName);
        rootGroomGrp = getrootGrp.longName().split('|')[1];

        mainHead = pm.textField('inputGetMainHead', q = True, tx = True);
        edge1 = pm.textField('inputGetEdge1', q = True, tx = True);
        edge2 = pm.textField('inputGetEdge2', q = True, tx = True);
        oriGrp = pm.textField('inputOriInfo', q = True, tx = True);
        cinGrp = firstName + '_CIN:Geo_Grp';
        colGrp = firstName + '_COL:Geo_Grp';

        infoDict[(str(firstName)+'Info')] = str(mainHead), str(cinGeo), str(edge1), str(edge2), str(rootGroomGrp), str(cinGrp), str(colGrp);
        prefPath = self.module.createFolderTxt();
        getTxt = open( prefPath + '/' + firstName + '_HairDyn.txt', 'w+' );
        getTxt.write( str(infoDict) );
        getTxt.close();

    def loadInfo( self, *args ):
        try:
            chaName = self.module.getCharacterName();
            firstName = chaName[:1].capitalize() + chaName[1:];
            prefPath = self.module.createFolderTxt();
            getTxt = open( prefPath + '/' + firstName + '_HairDyn.txt', 'r' );
            getNameTxt = getTxt.read();
            getTxt.close();
            nameDict = ast.literal_eval(getNameTxt);
            geoBaseName = nameDict[(str(firstName)+'Info')];
            pm.textField('inputGetMainHead', e = True, tx = geoBaseName[0]);
            pm.textField('inputGetEdge1', e = True, tx = geoBaseName[2]);
            pm.textField('inputGetEdge2', e = True, tx = geoBaseName[3]);
        except :
            pm.warning( 'Character Path Not Found' );

    def readTextFile( self, *args ):
        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];
        prefPath = self.module.createFolderTxt();
        getTxt = open( prefPath + '/' + firstName + '_HairDyn.txt', 'r' );
        getNameTxt = getTxt.read();
        getTxt.close();
        return getNameTxt;

    def deleteOrig ( self, target = '' , *args ) :
        shapes = pm.listRelatives ( target , shapes = True ) ;
        for shape in shapes :
            if 'Orig' in str ( shape ) :
                pm.delete ( shape ) ;
            else : pass ;

    def extrudeHairTube ( self, group ) :

        axes = ( 'x' , 'y' , 'z' ) ;
                       
        groupNm = group.split ( '_GRP' )[0] ;

        curveList =  pm.listRelatives ( group , children = True ) ;
              
        baseList = [ ] ;
        
        extrudeList = [ ] ;
        
        for each in curveList:
            
            circle = pm.circle ( n = each.split('_')[0] + '_base' ) [0] ;
            
            baseList.append ( circle ) ;
            
            for axis in axes :
                pm.setAttr ( circle + '.s' + axis , 0.3 ) ;
            
            extrude = pm.extrude ( circle , each , ch = True , rn = False , po = 0 , et = 2 , ucp = 0 , fpt = 0 , upn = 0 , rsp = 1 , sc = 0.5 ) [0] ;
            pm.reverseSurface( d = 0, ch = 1, rpo = 1);
            extrudeList.append ( extrude ) ;
        
        baseGrp = pm.group ( baseList , n = groupNm + '_baseGRP' ) ;
        
        tubeGrp = pm.group ( extrudeList , n = groupNm + '_tubeGRP' ) ;

        ### connect nodes so that the base follows hair curves
     
        for i in range ( 0 , len ( curveList ) ) :
                
            curve = curveList [i] ;
            curveShape = pm.listRelatives ( curve , shapes = True ) [0] ;
            base = baseList [i] ;
            
            mtp = pm.createNode ( 'motionPath' , n = curve + '_mtp' ) ;
            pm.setAttr ( mtp + '.sideTwist' , 90 ) ;        
            pm.connectAttr ( curveShape + '.worldSpace[0]' , mtp + '.geometryPath' ) ;        
            pm.connectAttr ( mtp + '.rotateX' , base + '.rotateX' ) ;
            pm.connectAttr ( mtp + '.rotateY' , base + '.rotateY' ) ;
            pm.connectAttr ( mtp + '.rotateZ' , base + '.rotateZ' ) ;
            pm.connectAttr ( mtp + '.rotateOrder' , base + '.rotateOrder' ) ;
            pm.connectAttr ( mtp + '.message' , base + '.specifiedManipLocation' ) ;
            
            adlX = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlX' ) ;
            adlY = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlY' ) ;
            adlZ = pm.createNode ( 'addDoubleLinear' , n = curve + '_adlZ' ) ;
            
            pm.connectAttr ( base + '.transMinusRotatePivotX' , adlX + '.input1' ) ;
            pm.connectAttr ( mtp + '.xCoordinate' , adlX + '.input2' ) ;
            pm.connectAttr ( adlX + '.output' , base + '.translateX' ) ;
            
            pm.connectAttr ( base + '.transMinusRotatePivotY' , adlY + '.input1' ) ;
            pm.connectAttr ( mtp + '.yCoordinate' , adlY + '.input2' ) ;
            pm.connectAttr ( adlY + '.output' , base + '.translateY' ) ;
            
            pm.connectAttr ( base + '.transMinusRotatePivotZ' , adlZ + '.input1' ) ;
            pm.connectAttr ( mtp + '.zCoordinate' , adlZ + '.input2' ) ;
            pm.connectAttr ( adlZ + '.output' , base + '.translateZ' ) ;

        return [ baseGrp , tubeGrp ] ;

    def runHairDyn( self, *args ):
        # getInfo
        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];

        getNameTxt = self.readTextFile();
        nameDict = ast.literal_eval(getNameTxt);
        geoBaseName = nameDict[(str(firstName)+'Info')];
        oriGrp = pm.textField('inputOriInfo', q = True, tx = True);

        CIN_object = geoBaseName[1];
        COL_object = geoBaseName[0];
        CIN_objectShape = pm.listRelatives( CIN_object )[0];
        CIN_edge1 = geoBaseName[2];
        CIN_edge2 = geoBaseName[3];
        headGeo  = geoBaseName[0];
        hairOri  = oriGrp;
        hairElem = oriGrp.split('_ORI')[0];
        cinGrp = geoBaseName[5];
        colGrp = geoBaseName[6];
        groomGrp   = geoBaseName[4];
        
        # noTouchGrp
        pm.select( CIN_object + '.' + CIN_edge1, r=True );
        pm.select( CIN_object + '.' + CIN_edge2, add=True );
        pm.loft(ch = True, u = True, c = False, ar = True, d = 1, ss = 1, rn = False, po = 0, rsn = True, n = 'MainNurbs_ForWrap');
        pm.select( 'MainNurbs_ForWrap', r=True );
        pm.delete( ch = True );
        pm.select( CIN_object, add=True );
        mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');
        pm.select( 'MainNurbs_ForWrap', r=True );
        pm.select( 'wrap1', addFirst=True );
        pm.rename('wrap1', 'MainNurbs_ForWrapNode');

        pm.select( 'MainNurbs_ForWrap', r=True );
        mel.eval('createHair 1 1 2 0 0 0 0 0 0 2 1 1');
        pm.select( 'hairSystem1', r=True );
        pm.select( 'hairSystem1OutputCurves', add=True );
        pm.select( 'nucleus1', add=True );
        pm.select( 'curve1', add=True );
        pm.delete();

        geoBaseName = CIN_object + 'Base';
        pm.group( 'MainNurbs_ForWrap', geoBaseName, n='NoTouch_GRP' );
        pm.rename( 'MainNurbs_ForWrapFollicle5050', 'MainNurbs_Follicle');
        pm.parent( 'MainNurbs_Follicle', 'NoTouch_GRP' );
        pm.select( 'hairSystem1Follicles', r=True );
        pm.delete();

        geoLocName = cinGrp.split('_')[0] + '_LOC';
        pm.spaceLocator( n = geoLocName );
        geoLocShapeName = cinGrp.split('_')[0] + '_LOCShape';
        pm.select( geoLocShapeName, r = True );
        mel.eval('toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`');

        # constraint with follicle
        pm.pointConstraint( 'MainNurbs_Follicle', geoLocName, mo = False );
        pm.select( geoLocName + '_pointConstraint1', r = True ),pm.delete();
        pm.select( geoLocName, r = True );
        pm.makeIdentity( a = True, t = 1, r = 1, s = 1, n = 0, pn = 1);
        pm.parentConstraint( 'MainNurbs_Follicle', geoLocName, mo = True, w = 1.0 );
        pm.parent( colGrp, geoLocName );
        constraintName = geoLocName + '_' + 'parentConstraint1';

        # create group
        simGrp = pm.group( em = True, w = True, n = 'SIM_GRP' );
        DycGrp = pm.group( em = True, w = True, n = 'DYC_GRP' );
        nRigidGrp = pm.group( em = True, w = True, n = 'nRigid_GRP' );
        pm.parent( DycGrp, nRigidGrp, simGrp );

        hairOutput = hairOri.split('_')[0];
        hair_grp = pm.group( em = True , w = True , n = hairOutput );
        hair_grp.t.lock();
        hair_grp.r.lock();
        hair_grp.s.lock();

        nucleusCol_grp = pm.group( em = True, w = True, n = cinGrp.split(':')[0] + '_NucleusCol' );
        pm.parent( nucleusCol_grp, geoLocName );
        pm.reorder( constraintName, r = 2 );

        # create hairSystem
        dispLayers = mc.ls(type = 'displayLayer');
        hairSystemList = [];
        nRigidList = [];
        duplicatedObjectList = [];
        outputCurveGrpList = [];

        # many hairSystem per one nucleus
        if pm.radioCollection( 'setSwitch', q = True, sl = True ) == 'oneSwitchOn':

            for i in range( 1, 2 ):
                if ':' in headGeo:
                    headGeoName = headGeo.split(':')[1];
                else:
                    headGeoName = headGeo;
                duplicatedObject = pm.duplicate( headGeo, n = headGeoName + 'N' + str ( i ) + '_COL');
                pm.parent( duplicatedObject , nucleusCol_grp );

                for layers in dispLayers:
                    if 'HairORI' in layers:
                        mel.eval( 'layerEditorSelectObjects %s' % layers );
                        curves = pm.ls ( sl = True ) ;
                        pm.select( curves , headGeoName + 'N' + str ( i ) + '_COL' );
                        mel.eval( 'makeCurvesDynamic 2 { "1", "1", "1", "1", "0"};' );
                        layerName = layers.split('_')[1]
                        outputCurveGrp = pm.general.PyNode( 'hairSystem1OutputCurves' );
                        outputCurveGrp.rename( hairElem + '_' + layerName + '_GRP' );
                        outputCurveGrpList.append(outputCurveGrp)
                        pm.parent( outputCurveGrp , hair_grp );
                        hairSystem = pm.general.PyNode( 'hairSystem1' );
                        hairSystem.rename( hairElem + '_' + layerName + '_HairSystem' );
                        hairSystemList.append( hairSystem );

                pm.select ( duplicatedObject );
                mel.eval ( 'makeCollideNCloth' );
                nRigid = pm.general.PyNode ( 'nRigid%s' % str ( i ) );
                nRigid.rename( headGeoName + 'N' + str ( i ) + '_COL_nRigid' );
                nRigidList.append(nRigid);
                self.deleteOrig( target = duplicatedObject );
                pm.blendShape( COL_object, duplicatedObject, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = COL_object + '_BSH' );

            pm.parent( 'nucleus1', hairSystemList, hairOri, simGrp );
            pm.parent( nRigidList , nRigidGrp );
            pm.reorder( 'nucleus1', hairSystemList, r = -2 );

        # four hairSystem per four nucleus
        if pm.radioCollection( 'setSwitch', q = True, sl = True ) == 'fourSwitchOn':

            for i in range( 1, 5 ):
                if ':' in headGeo:
                    headGeoName = headGeo.split(':')[1];
                else:
                    headGeoName = headGeo;
                duplicatedObject = pm.duplicate( headGeo, n = headGeoName + 'N' + str ( i ) + '_COL');
                pm.parent ( duplicatedObject , nucleusCol_grp );
                duplicatedObjectList.append( duplicatedObject );

            for layers, layerNumber in zip(dispLayers, range( 1, 5 )):
                if 'HairORI' in layers:
                    mel.eval( 'layerEditorSelectObjects %s' % layers );
                    curves = pm.ls ( sl = True );
                    pm.select( curves , headGeoName + 'N' + str ( layerNumber ) + '_COL' );
                    mel.eval( 'makeCurvesDynamic 2 { "1", "1", "1", "1", "0"};' );
                    layerName = layers.split('_')[1]
                    outputCurveGrp = pm.general.PyNode( 'hairSystem1OutputCurves' );
                    outputCurveGrp.rename( hairElem + '_' + layerName + '_GRP' );
                    outputCurveGrpList.append(outputCurveGrp)
                    pm.parent( outputCurveGrp , hair_grp );
                    hairSystem = pm.general.PyNode( 'hairSystem1' );
                    hairSystemName = hairElem + '_' + layerName + '_HairSystem';
                    hairSystem.rename( hairSystemName );
                    hairSystemList.append( hairSystem );
                    if layerNumber != 1: 
                        pm.select( hairSystemName );
                        mel.eval( 'assignNSolver "";' );

            for dupObj in duplicatedObjectList:
                pm.select ( dupObj );
                mel.eval ( 'makeCollideNCloth' );
                nRigid = pm.general.PyNode ( 'nRigid%s' % str ( 1 ) );
                nRigid.rename( dupObj[0] + '_nRigid' );
                nRigidList.append(nRigid);
                self.deleteOrig( target = dupObj );

            for nRigidColList,nucleusNumber in zip(nRigidList[1:],range(2,5)):
                nucleusName = 'nucleus%s' %str(nucleusNumber);
                assignName = 'assignNSolver %s;' %nucleusName
                pm.select(nRigidColList);
                mel.eval(assignName);

            pm.parent( 'nucleus1' , 'nucleus2' , 'nucleus3' , 'nucleus4', hairSystemList, hairOri , simGrp );
            pm.parent( nRigidList , nRigidGrp );
            pm.reorder( 'nucleus1' , 'nucleus2' , 'nucleus3' , 'nucleus4', hairSystemList, r = -2 );

            # connect nucleus1's nRigid Attribute to else nucleus's nRigid
            pm.setAttr( COL_object + '.v' , 0 );
            driver_nRigid = pm.general.PyNode( nRigidList[0] );
            driven_nRigidList = nRigidList[1:];

            for each in driven_nRigidList :
                driven_nRigid = pm.general.PyNode ( each ) ;

                driver_nRigid.isDynamic >> driven_nRigid.isDynamic ;
                driver_nRigid.collide >> driven_nRigid.collide ;
                driver_nRigid.collisionFlag >> driven_nRigid.collisionFlag ;
                driver_nRigid.collideStrength >> driven_nRigid.collideStrength ;
                driver_nRigid.collisionLayer >> driven_nRigid.collisionLayer ;
                driver_nRigid.thickness >> driven_nRigid.thickness ;
                driver_nRigid.solverDisplay >> driven_nRigid.solverDisplay ;
                driver_nRigid.displayColor >> driven_nRigid.displayColor ;
                driver_nRigid.bounce >> driven_nRigid.bounce ;
                driver_nRigid.friction >> driven_nRigid.friction ;
                driver_nRigid.stickiness >> driven_nRigid.stickiness ;
                driver_nRigid.trappedCheck >> driven_nRigid.trappedCheck ;
                driver_nRigid.pushOut >> driven_nRigid.pushOut ;
                driver_nRigid.pushOutRadius >> driven_nRigid.pushOutRadius ;
                driver_nRigid.crossoverPush >> driven_nRigid.crossoverPush ;

            selGrpCols = pm.listRelatives(nucleusCol_grp);
            nameBsh = [selGrpCols[0] + '_BSH', selGrpCols[1] + '_BSH', selGrpCols[2] + '_BSH'];
            pm.blendShape( COL_object, duplicatedObjectList[0], origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = COL_object + '_BSH' );
            pm.blendShape ( selGrpCols[0], selGrpCols[1], origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = nameBsh[0] );
            pm.blendShape ( selGrpCols[1], selGrpCols[2], origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = nameBsh[1] );
            pm.blendShape ( selGrpCols[2], selGrpCols[3], origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = nameBsh[2] );
            pm.select( selGrpCols[1], selGrpCols[2], selGrpCols[3] );
            mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );

        # rename dyn curve
        follicleList = pm.listRelatives( hairOri , c = True );
        hairNameList = [];
        for follicle in follicleList:
            children = pm.listRelatives( follicle , c = True );
            for child in children:
                if 'ORI' in str(child):
                    name = child.split( '_ORI' )[0]; 
                    hairNameList.append( name );

        hairDynList = [];
        hairDynGrpList = pm.listRelatives( hair_grp , c = True );
        for each in hairDynGrpList :
            hairDynList.extend( pm.listRelatives( each , c = True ));
        for i in range( 0 , len( hairNameList )):
            print i
            pm.rename( hairDynList[i], hairNameList[i] + '_OTP' );

        #createHairTubeOn
        if pm.radioCollection( 'tubeSwitch', q = True, sl = True ) == 'tubeSwitchOn':
    
            # tube color
            rampShaderPath = mp + '/' + 'textSetupSimUI/shader16_lambertRamp.ma';
            mel.eval( 'file -import -type "mayaAscii"  -ignoreVersion -mergeNamespacesOnClash false -rpr "shader16_lambertRamp" -options "v=0;"  -pr  -importFrameRate true  -importTimeRange "override" "%s";' %rampShaderPath);

            baseGrpList = [];
            tubeGrpList = [];
            rampShaderList = [];
            for crvGrp in outputCurveGrpList:
                extrude = self.extrudeHairTube( crvGrp );
                baseGrpList.append( extrude [0] );
                tubeGrpList.append( extrude [1] );
                pm.select( extrude [1] );
                rampShader = 'Hair_' + crvGrp.split('_')[1] + '_Shd';
                rampShaderList.append( rampShader );

            for tubeGrp, shader in zip(tubeGrpList, rampShaderList):
                pm.select(tubeGrp, r=True);
                pm.hyperShade(assign = shader);

            hairTube_grp = pm.group ( em = True , w = True , n = 'HairTube_GRP' ) ;
            hairTube_grp.t.lock ( ) ;
            hairTube_grp.r.lock ( ) ;
            hairTube_grp.s.lock ( ) ;
            hairTube_grp.addAttr ( 'hairTube_tubeScale' , keyable = True , attributeType = 'float' , dv = 0.05 ) ;
            hairTube_grp.addAttr ( 'hairTube_tipScale' , keyable = True , attributeType = 'float' , dv = 0.2 ) ;

            baseList = [ ] ; 
            for each in baseGrpList :
                each.v.set ( 0 ) ;
                bases = pm.listRelatives ( each , c = True ) ;
                baseList.extend ( bases ) ;

            for each in baseList :
                hairTube_grp.hairTube_tubeScale >> each.sx ;
                hairTube_grp.hairTube_tubeScale >> each.sy ;
                hairTube_grp.hairTube_tubeScale >> each.sz ;

            extrudeList = pm.ls ( type = 'extrude' ) ;
            for each in extrudeList :
                hairTube_grp.hairTube_tipScale >> each.scale ; 
            pm.parent ( baseGrpList , tubeGrpList , hairTube_grp ) ;
            pm.displaySmoothness ( hairTube_grp , divisionsU = 0 , divisionsV = 0 , pointsWire = 4 , pointsShaded = 1 , polygonObject = 1 ) ;
            pm.parent ( hairTube_grp , simGrp ) ;
            
            hairTube_layer = pm.createDisplayLayer ( name = 'HairTube_Layer' , number = 1 , empty = True ) ;
            hairTube_layer.color.set ( 29 ) ;
            pm.editDisplayLayerMembers ( hairTube_layer , hairTube_grp ) ;
            pm.select('HairTube_GRP', r=True);
            pm.displaySmoothness(du = 3, dv = 3, pw = 16, ps = 4, po = 3);
            pm.select(clear=True);

        #createHairTubeOff
        if pm.radioCollection( 'tubeSwitch', q = True, sl = True ) == 'tubeSwitchOff':
            pass

        hairDYN_layer = pm.createDisplayLayer ( name = 'HairDYN_Layer' , number = 1 , empty = True ) ;
        hairDYN_layer.color.set ( 22 ) ;
        pm.editDisplayLayerMembers ( hairDYN_layer , hair_grp );
        mel.eval('hyperShadePanelMenuCommand("hyperShadePanel1", "deleteUnusedNodes");');

        # blendShape CIN_COL
        oriName = cinGrp.split(':');
        firstFraming = 1.0;
        secondFraming = 10.0;
        mc.blendShape( cinGrp , colGrp, o = 'world' , automatic = True, n = 'CIN_COL_BSH');
        mc.setAttr('CIN_COL_BSH.' + oriName[1] , 1);
        mc.setKeyframe( 'CIN_COL_BSH.' + oriName[1], t=[secondFraming]);
        mc.currentTime( firstFraming );
        mc.setAttr('CIN_COL_BSH.' + oriName[1] , 0);
        mc.setKeyframe( 'CIN_COL_BSH.' + oriName[1], t=[firstFraming]);

        # Outliner Color
        namesGrp = 'NoTouch_GRP', geoLocName, 'SIM_GRP', hairOutput , cinGrp;
        colorCodes = [( 1 , 0 , 0 ), ( 0.498039 , 1 , 0 ), ( 0 , 1 , 1 ), ( 1 , 1 , 0 ), ( 1 , 0 , 1 )];
        for colorCode, nameGrp in zip(colorCodes, namesGrp):
            target = pm.general.PyNode( nameGrp );
            target.useOutlinerColor.set( 1 );       
            target.outlinerColor.set( colorCode );

        # visibility
        pm.parent( groomGrp, 'NoTouch_GRP' );
        pm.select( 'NoTouch_GRP', add = True );
        pm.select( cinGrp, add = True );
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );

    def hairTipConstraint( self, *args ):
        oriGrp = pm.textField('inputOriInfo', q = True, tx = True);
        hairElem = oriGrp.split('_ORI')[0];

        dispLayers = mc.ls(type = 'displayLayer');
        for layers in dispLayers:
            if 'HairORI' in layers:
                mel.eval( 'layerEditorSelectObjects %s' % layers );
                curvesList = pm.ls( sl = True );
                toSelect_list = [];
                for curves in curvesList:
                    selectionShape = pm.listRelatives( curves , shapes = True )[0];
                    nos = pm.getAttr( selectionShape + '.spans' );
                    deg = pm.getAttr( selectionShape + '.degree' );
                    toSelect_list.append( '%s.cv[%s]' % ( curves , str ( nos + deg - 1 ) ) );
 
                pm.select( toSelect_list ); 
                nConShape = mel.eval( 'createNConstraint pointToPoint 0;' )[0];
                nConShape = pm.general.PyNode( nConShape );
                nCon = pm.listRelatives( nConShape , p = True )[0];
                nConName = layers.split('_')[1];
                nCon.rename( hairElem +'_'+ nConName + 'Tip_C2C_DYC' );
                nConShape.connectionMethod.set( 1 );
                nConShape.maxDistance.set( 1 );
                pm.parent( nCon , 'DYC_GRP' );

class Layer( object ) :

    def __init__( self ) :
        self.layer1 = 'HairORI_Green_Layer';
        self.layer2 = 'HairORI_LightBlue_Layer';
        self.layer3 = 'HairORI_Red_Layer';
        self.layer4 = 'HairORI_Yellow_Layer';
        self.layer5 = 'HairORI_LightPink_Layer';
        self.layer6 = 'HairORI_LightBrown_Layer';
        self.layer7 = 'HairORI_DarkGreen_Layer';
        self.layer8 = 'HairORI_LightGrey_Layer';
        self.layer9 = 'HairORI_DarkRed_Layer';
        self.layer10 = 'HairORI_DarkPurple_Layer';
        self.layer11 = 'HairORI_SeaGreen_Layer';
        self.layer12 = 'HairORI_Blue_Layer';
        self.layer13 = 'HairORI_DarkBrown_Layer'; 
        self.layer14 = 'HairORI_YellowGreen_Layer';
        self.layer15 = 'HairORI_DarkPink_Layer';
        self.layer16 = 'HairORI_Aquamarine_Layer';      

    def layerVisibility( self ):
        dispLayers = mc.ls(type = 'displayLayer')
        for layers in dispLayers:
            if layers != 'defaultLayer':
                mel.eval( 'layerEditorLayerButtonVisibilityChange %s' % layers );
                # pm.setAttr( '%s.visibility' % layers, 0);
        
    def layerGreen( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer1 , number = 1 , empty = True );
            createLayer.color.set( 14 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer1 );

    def layerLightBlue( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer2 , number = 1 , empty = True );
            createLayer.color.set( 18 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer2 );

    def layerRed( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer3 , number = 1 , empty = True );
            createLayer.color.set( 13 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer3 );

    def layerYellow( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer4 , number = 1 , empty = True );
            createLayer.color.set( 17 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer4 );

    def layerLightPink( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer5 , number = 1 , empty = True );
            createLayer.color.set( 20 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer5 );

    def layerLightBrown( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer6 , number = 1 , empty = True );
            createLayer.color.set( 21 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer6 );

    def layerDarkGreen( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer7 , number = 1 , empty = True );
            createLayer.color.set( 23 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer7 );

    def layerLightGrey( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer8 , number = 1 , empty = True );
            createLayer.color.set( 3 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer8 );

    def layerDarkRed( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer9 , number = 1 , empty = True );
            createLayer.color.set( 4 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer9 );

    def layerDarkPurple( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer10 , number = 1 , empty = True );
            createLayer.color.set( 30 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer10 );

    def layerSeaGreen( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer11 , number = 1 , empty = True );
            createLayer.color.set( 28 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer11 );

    def layerBlue( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer12 , number = 1 , empty = True );
            createLayer.color.set( 6 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer12 );

    def layerDarkBrown( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer13 , number = 1 , empty = True );
            createLayer.color.set( 10 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer13 );

    def layerYellowGreen( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer14 , number = 1 , empty = True );
            createLayer.color.set( 25 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer14 );

    def layerDarkPink( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer15 , number = 1 , empty = True );
            createLayer.color.set( 9 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer15 );

    def layerAquamarine( self ):
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'layerSwitchOn':
            createLayer = pm.createDisplayLayer( name = self.layer16 , number = 1 , empty = True );
            createLayer.color.set( 19 );
        if pm.radioCollection( 'optionSwitch', q = True, sl = True ) == 'selectSwitchOn':
            mel.eval( "editDisplayLayerMembers -noRecurse %s `ls -selection`;" % self.layer16 );

    def selectStep( self, step = 2 , *args ): 
        selection = pm.ls( sl = True );
        selectionList = [];

        for i in range( 0 , len ( selection ) , step ):

            selectionList.append ( selection [ i ] );
        pm.select( selectionList );

    def selectOption( self, *args):
        #selectStepTwo
        if pm.radioCollection( 'selectStep', q = True, sl = True ) == 'selectStepTwo':
            self.selectStep( step = 2 );
        #selectStepThree
        if pm.radioCollection( 'selectStep', q = True, sl = True ) == 'selectStepThree':
            self.selectStep( step = 3 );
        #selectStepFour
        if pm.radioCollection( 'selectStep', q = True, sl = True ) == 'selectStepFour':
            self.selectStep( step = 4 );

class SetupHairYeti( object ):

    def __init__( self ):
        self.module = module.MainModule();

    def fileInfoHairYeti( self,*args ):
        chaPath = pm.textField( 'inputChaPath' , q = True , text = True );
        '''
        # Hanuman
        if chaPath != '':
            chaName = chaPath.split('/')[-1:][0];
            chaCinPath = chaPath + '/model/main/hero/output/' + str(chaName) + '_mdl_main_md.hero.ma'
            groomGeo = chaPath + '/groom/main/hero/output/' + str(chaName) + '_groomGeo_main_md.hero.abc'
            mtrGroom = chaPath + '/groom/main/hero/output/' + str(chaName) + '_mtr_groom_main_md.hero.ma'
            yetiMain = chaPath + '/groom/main/hero/output/' + str(chaName) + '_yeti_main_md.hero.ma'

            fileLists = [chaCinPath.split('/')[-1:], groomGeo.split('/')[-1:], mtrGroom.split('/')[-1:], yetiMain.split('/')[-1:]];
            for fileList in fileLists :
                pm.textScrollList( 'fileListsHairYeti' , e = True , a = fileList );
        
            mc.file( chaCinPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_CIN');
            mc.file( groomGeo, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_CRV');
            mc.file( mtrGroom, i = True, typ = 'mayaAscii', mnc = False );
            mc.file( yetiMain, i = True, typ = 'mayaAscii', mnc = False );
        '''
        # Bikey
        if chaPath != '':
            chaName = chaPath.split('/')[-1:][0];
            chaCinPath = chaPath + '/model/main/hero/output/' + str(chaName) + '_mdl_main_md.hero.ma'
            groomGeo = chaPath + '/groom/main/hero/output/' + str(chaName) + '_groomGeo_main_md.hero.abc'
            yetiMain = chaPath + '/groom/main/hero/output/' + str(chaName) + '_yeti_main_md.hero.ma'

            fileLists = [chaCinPath.split('/')[-1:], groomGeo.split('/')[-1:], yetiMain.split('/')[-1:]];
            for fileList in fileLists :
                pm.textScrollList( 'fileListsHairYeti' , e = True , a = fileList );
        
            mc.file( chaCinPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_CIN');
            mc.file( groomGeo, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_CRV');
            mc.file( yetiMain, i = True, typ = 'mayaAscii', mnc = False );
        else:
            pm.warning('Character Path Not Found');

    def browseHairOutput( self, *args ):
        try : 
            start = pm.textField( 'inputHairOutputPath' , q = True , text = True );
        except :
            start = 'D:/';
        try:
            startPath = start;
            browseIM = mc.fileDialog2( fm = 4, ff = '*',dir = startPath, okc = 'Import' )[0];
            pm.textField( 'inputHairOutputPath', e = True, text = browseIM.split('/')[-1:][0]);
            mc.file( browseIM, i = True, typ = 'mayaAscii', mnc = False );
        except:
            pm.warning( 'No Path Selected' );

    def getMainHeadYeti( self, *args ):
        sels = pm.ls(sl=True);
        if sels == []:
            pm.warning('No Selected Body For Wrap');
        else :
            pm.textField('inputGetMainHeadYeti', e = True, tx = sels[0]);

    def runYeti( self,*args ):
        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];
        yetiMeshGrp = firstName + '_CRV:YetiMesh_Grp';
        pm.select(yetiMeshGrp, r = True);
        dupGrp = pm.duplicate( rr = True );
        dupSP = dupGrp[0].split('_')[0];
        dupName = pm.rename(dupGrp, dupSP + '_ForWrap_GRP');
        pm.select(dupName);
        for geoGrp in pm.selected():
            pm.parent(w = True );
            geoGrp.rename(geoGrp.name().replace('_Geo', '_Geo_ForWrap'));
            selsGeoGrp = pm.listRelatives( geoGrp );
            for selGeoGrp in selsGeoGrp:
                selGeoGrp.rename(selGeoGrp.name().replace('_Geo', '_Geo_ForWrap'));

        # NoTouch_GRP
        pm.group( n = 'NoTouch_GRP' );
        pm.select( clear=True );
        pm.parent(dupName, 'NoTouch_GRP');
        geoWrap = pm.listRelatives( dupName );

        mainGeo = pm.textField('inputGetMainHeadYeti', q = True, tx = True);

        groomGrp = ':' + firstName + '_CRV' ;
        groomNsp = pm.namespaceInfo( groomGrp, listNamespace=True )[0];
        selection = pm.ls(groomNsp, r = True );
        selectionName = selection[0].nodeName();
        getrootGrp = pm.general.PyNode(selectionName);
        rootGroomGrp = getrootGrp.longName().split('|')[1];
        
        selsWrp = pm.listRelatives( dupName );
        pm.select(selsWrp);
        pm.select(mainGeo, add=True );
        mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');
        wrapBase = mainGeo + 'Base';
        pm.parent(wrapBase, 'NoTouch_GRP');
        pm.parent(rootGroomGrp, 'NoTouch_GRP');

        # Wrap
        for i in range(len(selsWrp)):
            pm.select(mainGeo);
            wrapNodes = pm.listConnections(t = 'wrap');
            pm.select( wrapNodes[i], addFirst=True );
            pm.rename( wrapNodes[i], mainGeo + 'Node' + str(i+1) );

        # BlenShape 
        for selWrp in selsWrp:
            wrapBsh = selWrp + '_BSH'
            selGroom = selWrp.split('_ForWrap')[0];
            pm.blendShape ( selWrp, selGroom, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = wrapBsh );

        # TechGeo_Grp
        pm.select( clear=True );
        pm.group( n = 'TechGeo_Grp' );
        pm.select( clear=True );
        pm.parent('Yeti_Grp', 'TechGeo_Grp');
        pm.parent('YetiNode_Grp', world=True);

        # Outliner
        cinGrp = firstName + '_CIN:Geo_Grp';
        namesGrp = 'NoTouch_GRP', cinGrp, 'TechGeo_Grp', 'YetiNode_Grp';
        colorCodes = [( 1 , 0 , 0 ), ( 1 , 0 , 1 ), ( 1 , 1 , 0 ), ( 0 , 1 , 1 )];
        for colorCode, nameGrp in zip(colorCodes, namesGrp):
            target = pm.general.PyNode( nameGrp );
            target.useOutlinerColor.set( 1 );       
            target.outlinerColor.set( colorCode );
        pm.select( clear=True );
        pm.select( 'NoTouch_GRP', r = True );
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );
        pm.select( clear=True );

        # TechGeo_layer
        techGeoLayer = pm.createDisplayLayer( name = 'TechGeo_Layer' , number = 1 , empty = True );
        techGeoLayer.color.set( 17 );
        pm.editDisplayLayerMembers( techGeoLayer, 'TechGeo_Grp' );
        pm.setAttr( '%s.visibility' % 'TechGeo_Layer', 0);

        '''
        # Hanuman
        try:
            self.SetupHairYeti.yetiShade();
        except:
            pm.warning('Readshift Not Loaded');
        '''

    def renameHairOutput( self,*args ):
        sels = pm.ls( sl = True );
        for obj in sels:
            for layer in obj.listConnections(type="displayLayer"):
                pm.delete(layer);
        for sel in sels:
            target = pm.general.PyNode( sel );
            target.useOutlinerColor.set( 1 );       
            target.outlinerColor.set( ( 1 , 0 , 1 ) );
            doSP = sel.split('_')[0];
        grpList = pm.ls(sl=True, dag=True, type='transform');
        for crvGrp in grpList:
            if 'CRV' in str(crvGrp):
                SetupHairDyn().deleteOrig( target = crvGrp );
                crvGrp.rename(crvGrp.name().replace('CRV', 'CIN'));
            if 'OTP' in str(crvGrp):
                SetupHairDyn().deleteOrig( target = crvGrp );
                crvGrp.rename(crvGrp.name().replace('OTP', 'CIN'));

    def yetiShade( self,*args ):
        try:
            pm.pluginInfo( 'redshift4maya.mll', q=True, vd=True );
        except:
            pm.error('Readshift Can Not Loaded');

        obj = mc.ls('Main_Fur*_ShdSg',type = 'shadingEngine')+mc.ls('Main_Fur*_ShdSG',type = 'shadingEngine');
        shade = mc.listConnections(obj[0]+ '.surfaceShader',s=True,d=False);
        listsYeti = mc.listRelatives('YetiNode_Grp',ad=True);

        for lists in listsYeti:
            if '_YetiNodeShape' in lists:
                pass
            elif '_YetiNode' in lists :
                mc.select(lists)
                mc.hyperShade(assign=shade[0]);
                mc.sets(e=True  , forceElement = obj[0] );

    def hairCrvConnectBsh( self,*args ):
        gcrvs = pm.ls('Hair*_CRV', type='transform'); 
        for gc in gcrvs: 
            name = gc.nodeName();
            pair = name.replace('_CRV', '_BSH');
            pairObjs = pm.ls(pair);
            if pairObjs: 
                pm.connectAttr(gc.worldSpace[0], pariObjs[0].create, f=True); 

class SetupHairAuto( object ):

    def __init__( self ):
        self.module = module.MainModule();

    def fileInfoHairAuto( self,*args ):
        chaPath = pm.textField( 'inputChaPath' , q = True , text = True );
        '''
        # Hanuman
        if chaPath != '':
            chaName = chaPath.split('/')[-1:][0];
            mtrGroom = chaPath + '/groom/main/hero/output/' + str(chaName) + '_mtr_groom_main_md.hero.ma'
            yetiMain = chaPath + '/groom/main/hero/output/' + str(chaName) + '_yeti_main_md.hero.ma'

            fileLists = [mtrGroom.split('/')[-1:], yetiMain.split('/')[-1:]];
            for fileList in fileLists :
                pm.textScrollList( 'fileListsHairAuto' , e = True , a = fileList );
        
            mc.file( mtrGroom, i = True, typ = 'mayaAscii', mnc = False );
            mc.file( yetiMain, i = True, typ = 'mayaAscii', mnc = False );
        '''
        # Bikey
        if chaPath != '':
            chaName = chaPath.split('/')[-1:][0];
            yetiMain = chaPath + '/groom/main/hero/output/' + str(chaName) + '_yeti_main_md.hero.ma'
            fileList = yetiMain.split('/')[-1:];
            pm.textScrollList( 'fileListsHairAuto' , e = True , a = fileList );
            mc.file( yetiMain, i = True, typ = 'mayaAscii', mnc = False );

        else:
            pm.warning('Character Path Not Found');

    def getMainHeadYetiAutoSim( self, *args ):
        sels = pm.ls(selection=True);
        if sels == []:
            pm.warning('No Selection');
        else :
            pm.textField('inputMainHeadYetiAutoSim', e = True, tx = sels[0]);

    def runHairAutoSim( self,*args ):
        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];
        yetiMeshGrp = 'YetiMesh_Grp';
        pm.select(yetiMeshGrp, r = True);
        dupGrp = pm.duplicate( rr = True );
        dupSP = dupGrp[0].split('_')[0];
        dupNameGrp = pm.rename(dupGrp, dupSP + '_ForWrap_GRP');
        pm.select(dupNameGrp);
        for geoGrp in pm.selected():
            pm.parent(w = True );
            geoGrp.rename(geoGrp.name().replace('_Geo', '_Geo_ForWrap'));
            selsGeoGrp = pm.listRelatives( geoGrp );
            for selGeoGrp in selsGeoGrp:
                selGeoGrp.rename(selGeoGrp.name().replace('_Geo', '_Geo_ForWrap'));

        # NoTouch_GRP
        pm.parent(dupNameGrp, 'NoTouch_GRP');
        mainGeo = pm.textField('inputMainHeadYetiAutoSim', q = True, tx = True);
        
        selsWrp = pm.listRelatives( dupNameGrp );
        pm.select(selsWrp);
        pm.select(mainGeo, add=True );
        mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');
        wrapBase = mainGeo + 'Base';
        pm.parent(wrapBase, dupNameGrp);

        # Wrap
        for i in range(len(selsWrp)):
            pm.select(mainGeo);
            wrapNodes = pm.listConnections(t = 'wrap');
            pm.select( wrapNodes[i], addFirst=True );
            pm.rename( wrapNodes[i], mainGeo + 'WrapNode' + str(i+1) );

        # BlenShape 
        for selWrp in selsWrp:
            wrapBsh = selWrp + '_BSH'
            selGroom = selWrp.split('_ForWrap')[0];
            pm.blendShape ( selWrp, selGroom, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = wrapBsh );

        # TechGeo_Grp
        pm.select( clear=True );
        pm.group( n = 'TechGeo_Grp' );
        pm.select( clear=True );
        pm.parent('Yeti_Grp', 'TechGeo_Grp');
        pm.parent('YetiNode_Grp', world=True);

        pm.select( clear=True );
        pm.select( 'YetiMesh_Grp', r = True );
        # pm.select( '*_YetiNode', add = True );
        # mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );

        # turn off yeti display 
        try:
            pm.setAttr( '*_YetiNodeShape.displayOutput' , 0 );
        except:
            pass;
        pm.select( clear=True );

        # Outliner
        namesGrp = 'TechGeo_Grp', 'YetiNode_Grp';
        colorCodes = [( 1 , 1 , 0 ), ( 0 , 1 , 1 )];
        for colorCode, nameGrp in zip(colorCodes, namesGrp):
            target = pm.general.PyNode( nameGrp );
            target.useOutlinerColor.set( 1 );       
            target.outlinerColor.set( colorCode );
        
        pm.parent( firstName + '_LOC', 'SIM_GRP');
        pm.reorder( firstName + '_LOC', r = -6 );
        pm.parent( 'NoTouch_GRP', 'SIM_GRP');
        pm.reorder( 'NoTouch_GRP', r = -7 );
        pm.select( clear=True );

        # TechGeo_layer
        techGeoLayer = pm.createDisplayLayer( name = 'TechGeo_Layer' , number = 1 , empty = True );
        techGeoLayer.color.set( 17 );
        pm.editDisplayLayerMembers( techGeoLayer, 'TechGeo_Grp' );
        pm.setAttr( '%s.visibility' % 'TechGeo_Layer', 0);

        self.module.nucleusTrack();

        '''
        # Hanuman
        try:
            self.SetupHairYeti.yetiShade();
        except:
            pm.warning('Readshift Not Loaded');
        '''

class SetupHairWrp( object ) :

    def __init__( self ):
        self.module = module.MainModule();

    def fileInfoHairWrp( self,*args ):
        chaPath = pm.textField( 'inputChaPath' , q = True , text = True );
        if chaPath != '':
            chaName = chaPath.split('/')[-1:][0];
            chaCinPath = chaPath + '/model/main/hero/output/' + str(chaName) + '_mdl_main_md.hero.ma'
            groomGeo = chaPath + '/groom/main/hero/output/' + str(chaName) + '_groomGeo_main_md.hero.abc'

            fileLists = [chaCinPath.split('/')[-1:], groomGeo.split('/')[-1:]];
            for fileList in fileLists :
                pm.textScrollList( 'fileListsHairWrp' , e = True , a = fileList );
        
            mc.file( chaCinPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_CIN');
            mc.file( groomGeo, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_CRV');

        else:
            pm.warning('Character Path Not Found');
    
    def browseHairOutputHairWrp( self, *args ):
        try : 
            start = pm.textField( 'inputHairOutputHairWrp' , q = True , text = True );
        except :
            start = 'D:/';
        try:
            startPath = start;
            browseIM = mc.fileDialog2( fm = 4, ff = '*',dir = startPath, okc = 'Import' )[0];
            pm.textField( 'inputHairOutputHairWrp', e = True, text = browseIM.split('/')[-1:][0]);
            mc.file( browseIM, i = True, typ = 'mayaAscii', mnc = False );
        except:
            pm.warning( 'No Path Selected' );

    def runHairWrp( self, *args ):
        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];
        sels = pm.ls( sl = True );
        pm.select(sels, r = True);
        dupCrv = pm.duplicate( rr = True );
        pm.select(dupCrv);
        for sel in pm.selected():
            target = pm.general.PyNode( sel );
            target.useOutlinerColor.set( 0 ); 
            doSP = sel.split('_')[0];
            doName = pm.rename(sel, doSP + '_ForTube' );
        grpList = pm.ls(sl=True, dag=True, type='transform');
        for crvGrp in grpList:
            SetupHairDyn().deleteOrig( target = crvGrp );
            crvGrp.rename(crvGrp.name().replace('_CIN', '_ForTube'));

        crvGrp = firstName + '_CRV:YetiGeo_Grp';
        pm.select(clear = True);           
        pm.group( n='NoTouch_GRP' );
        pm.parent(dupCrv, crvGrp, 'NoTouch_GRP');

        # Outliner
        cinGrp = firstName + '_CIN:Geo_Grp';
        namesGrp = 'NoTouch_GRP', cinGrp;
        colorCodes = [( 1 , 0 , 0 ), ( 1 , 0 , 1 )];
        for colorCode, nameGrp in zip(colorCodes, namesGrp):
            target = pm.general.PyNode( nameGrp );
            target.useOutlinerColor.set( 1 );       
            target.outlinerColor.set( colorCode );
        pm.select( clear=True );
        pm.select( 'NoTouch_GRP', r = True );
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );
        pm.select( clear=True );

class SetupHairWire( object ) :

    def __init__( self ):
        self.module = module.MainModule();

    def fileInfoHairWire( self,*args ):
        chaPath = pm.textField( 'inputChaPath' , q = True , text = True );
        if chaPath != '':
            chaName = chaPath.split('/')[-1:][0];
            chaGeoPath = chaPath + '/model/main/hero/output/' + str(chaName) + '_mdl_main_md.hero.ma'

            fileLists = [chaGeoPath.split('/')[-1:]];
            for fileList in fileLists :
                pm.textScrollList( 'fileListsHairWireWrp' , e = True , a = fileList );
        
            mc.file( chaGeoPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_GRP');
            mc.file( chaGeoPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_CIN');

        else:
            pm.warning('Character Path Not Found');

    def browseWireCrv( self,*args ):
        try : 
            start = pm.textField( 'inputRefWireWRPCrvPath' , q = True , text = True );
        except :
            start = 'D:/';
        try:
            startPath = start;
            browseIM = mc.fileDialog2( fm = 4, ff = '*',dir = startPath, okc = 'Import' )[0];
            pm.textField( 'inputRefWireWRPCrvPath', e = True, text = browseIM.split('/')[-1:][0]);
            mc.file( browseIM, i = True, typ = 'mayaAscii', mnc = False );
        except:
            pm.warning( 'No Path Selected' );

    def browseHairGeoWire( self, *args ):
        try : 
            start = pm.textField( 'inputHairGeoWireWRP' , q = True , text = True );
        except :
            start = 'D:/';
        try:
            startPath = start;
            browseIM = mc.fileDialog2( fm = 4, ff = '*',dir = startPath, okc = 'Import' )[0];
            pm.textField( 'inputHairGeoWireWRP', e = True, text = browseIM.split('/')[-1:][0]);
            mc.file( browseIM, i = True, typ = 'mayaAscii', mnc = False );
        except:
            pm.warning( 'No Path Selected' );

    def browseHairWireOutput( self, *args ):
        try : 
            start = pm.textField( 'inputHairGeoWireWRPOutput' , q = True , text = True );
        except :
            start = 'D:/';
        try:
            startPath = start;
            browseIM = mc.fileDialog2( fm = 4, ff = '*',dir = startPath, okc = 'Import' )[0];
            pm.textField( 'inputHairGeoWireWRPOutput', e = True, text = browseIM.split('/')[-1:][0]);
            mc.file( browseIM, i = True, typ = 'mayaAscii', mnc = False );
        except:
            pm.warning( 'No Path Selected' );

    def dupGeoWire( self,*args ):
        sels = pm.ls( sl = True );
        pm.select(sels, r = True);
        dupGeo = pm.duplicate( rr = True );
        dupSP = dupGeo[0].split('_')[0];
        dupName = pm.rename(dupGeo, dupSP + '_Geo_ForWrap');
        pm.parent(w = True );

        # Wire
        hairCrvGrp = dupSP + '_CRV_GRP';
        pm.select(hairCrvGrp, r = True);
        selCurve = pm.ls(sl=True);
        pm.select(selCurve);
        for hairCrv in pm.selected():
            selsCrvGrp = pm.listRelatives( hairCrv );
            pm.select(selsCrvGrp, r = True);         
        curves = pm.ls ( sl = True , typ = 'transform' );
        for crv in range( len ( curves )):
            makeWire = curves[crv].split( '_CRV' )[ 0 ] + '_Geo';
            mc.wire ( makeWire , gw = False , en = 1 , ce = 0 , li = 0 , w = '%s'%curves[crv] , dds = [ 0 , 300 ] , n = makeWire + '_Wire' );
        # Wrap
        hairGeoGrp = dupSP + '_GEO_GRP';
        pm.select( dupName, r=True );
        pm.select( hairGeoGrp, add=True );
        mel.eval('doWrapArgList "7" { "1","0","1", "2", "0", "1", "0", "0" }');
        pm.select( dupName, r=True );
        pm.select( 'wrap1', addFirst=True );
        pm.rename('wrap1', dupName + 'Node');
        
        # blendShape
        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];
        geoGrp = firstName + '_GRP:Geo_Grp';
        cinGrp = firstName + '_CIN:Geo_Grp';
        pm.blendShape( cinGrp, geoGrp, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = 'CIN_GRP_BSH' );
        hairGeoOutput = dupName.split('_')[0] + '_Geo';
        outputName = firstName + '_GRP:' + hairGeoOutput;
        pm.blendShape( dupName, outputName, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = dupName + '_BSH' );

        pm.select(outputName);
        selection = pm.ls( sl = True );
        selectionName = selection[0].nodeName();
        getrootGeo = pm.general.PyNode(selectionName);
        rootGeo = getrootGeo.longName();   
        pm.reorderDeformers( dupName + '_BSH', 'CIN_GRP_BSH', rootGeo );
        pm.select(cl = True); 

        # NoTouch_GRP
        pm.group( n='NoTouch_GRP' );
        pm.parent( dupName, hairCrvGrp, hairGeoGrp, 'NoTouch_GRP' );

        # visibility
        namesGrp = 'NoTouch_GRP', geoGrp, cinGrp;
        colorCodes = [( 1 , 0 , 0 ), ( 1 , 1 , 0 ), ( 1 , 0 , 1 )];
        for colorCode, nameGrp in zip(colorCodes, namesGrp):
            target = pm.general.PyNode( nameGrp );
            target.useOutlinerColor.set( 1 );       
            target.outlinerColor.set( colorCode );
            
        pm.select( 'NoTouch_GRP', r = True );
        pm.select( cinGrp, add = True );
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );
        pm.reorder( 'NoTouch_GRP', r = -3 );
        pm.select(cl = True); 

class SetupYetiNode( object ) :
    def __init__( self ) :
        self.module = module.MainModule();

    def fileInfoHairYetiNode( self,*args ):
        chaPath = pm.textField( 'inputChaPath' , q = True , text = True );
        if chaPath != '':
            chaName = chaPath.split('/')[-1:][0];
            chaGeoPath = chaPath + '/model/main/hero/output/' + str(chaName) + '_mdl_main_md.hero.ma'
            mtrGroom = chaPath + '/groom/main/hero/output/' + str(chaName) + '_mtr_groom_main_md.hero.ma'
            yetiMain = chaPath + '/groom/main/hero/output/' + str(chaName) + '_yeti_main_md.hero.ma'

            fileLists = [chaGeoPath.split('/')[-1:], mtrGroom.split('/')[-1:], yetiMain.split('/')[-1:]];
            for fileList in fileLists :
                pm.textScrollList( 'fileListsHairYetiNode' , e = True , a = fileList );
        
            mc.file( chaGeoPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_CIN');
            mc.file( chaGeoPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_COL');
            mc.file( mtrGroom, i = True, typ = 'mayaAscii', mnc = False );
            mc.file( yetiMain, i = True, typ = 'mayaAscii', mnc = False );
        else:
            pm.warning('Character Path Not Found');

    def getMainHeadYetiNode( self, *args ):
        sels = pm.ls(sl=True);
        if sels == []:
            pm.warning('No Selected Body For Wrap');
        else :
            pm.textField('inputGetMainHeadYetiNode', e = True, tx = sels[0]);

    def outlinerColor( self, *args ):
        getText = self.module.readInputMainName();
        nameDict = ast.literal_eval(getText);
        getGrpCin = nameDict[('MainName')][1];
        splitText = nameDict[('MainName')][0].split('CIN:');
        geoLocName = splitText[0] + 'LOC';
        
        namesGrp = 'NoTouch_GRP', geoLocName, getGrpCin;
        colorCodes = [( 1 , 0 , 0 ), ( 0.498039 , 1 , 0 ), ( 1 , 0 , 1 )];
        
        for colorCode, nameGrp in zip(colorCodes, namesGrp):
            target = pm.general.PyNode( nameGrp );
            target.useOutlinerColor.set( 1 );       
            target.outlinerColor.set( colorCode );

        # visibility
        pm.select( getGrpCin, r = True );
        pm.select( 'NoTouch_GRP', add = True );
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );

    def blendShapeCinCol( self, *args ):
        getText = self.module.readInputMainName();
        nameDict = ast.literal_eval(getText);
        getGrpCin = nameDict[('MainName')][1];
        getGrpCol = nameDict[('MainName')][2];
        oriName = getGrpCin.split(':');

        firstFraming = 1.0;
        secondFraming = 10.0;

        mc.blendShape( getGrpCin , getGrpCol, o = 'world', automatic = True , n = 'CIN_COL_BSH');
        mc.setAttr('CIN_COL_BSH.' + oriName[1] , 1);
        mc.setKeyframe( 'CIN_COL_BSH.' + oriName[1], t=[secondFraming]);
        mc.currentTime( firstFraming );
        mc.setAttr('CIN_COL_BSH.' + oriName[1] , 0);
        mc.setKeyframe( 'CIN_COL_BSH.' + oriName[1], t=[firstFraming]);

    def dupGeoYeti( self,*args ):
        sels = pm.ls( sl = True );
        pm.select(sels, r = True);
        dupGrp = pm.duplicate( rr = True );
        dupSP = dupGrp[0].split('_')[0];
        dupName = pm.rename(dupGrp, dupSP + '_ForWrap_GRP');
        pm.select(dupName);
        for geoGrp in pm.selected():
            pm.parent(w = True );
            geoGrp.rename(geoGrp.name().replace('_Geo', '_Geo_ForWrap'));
            selsGeoGrp = pm.listRelatives( geoGrp );
            for selGeoGrp in selsGeoGrp:
                selGeoGrp.rename(selGeoGrp.name().replace('_Geo', '_Geo_ForWrap'));

        # NoTouch_GRP
        pm.parent(dupName, 'NoTouch_GRP');
        geoWrap = pm.listRelatives( dupName );

        getChaname = SetupHairDyn().getCharacterName();
        nameCha = ast.literal_eval(getChaname);
        firstName = nameCha[('ChaInfo')];
        mainGeo = pm.textField('inputGetMainHeadYeti', q = True, tx = True);

        groomGrp = ':' + firstName + '_CRV' ;
        groomNsp = pm.namespaceInfo( groomGrp, listNamespace=True )[0];
        selection = pm.ls(groomNsp, r = True );
        selectionName = selection[0].nodeName();
        getrootGrp = pm.general.PyNode(selectionName);
        rootGroomGrp = getrootGrp.longName().split('|')[1];
        
        selsWrp = pm.listRelatives( dupName );
        pm.select(selsWrp);
        pm.select(mainGeo, add=True );
        mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');
        wrapBase = mainGeo + 'Base';
        pm.parent(wrapBase, 'NoTouch_GRP');
        pm.parent(rootGroomGrp, 'NoTouch_GRP');

        # Wrap
        for i in range(len(selsWrp)):
            pm.select(mainGeo);
            wrapNodes = pm.listConnections(t = 'wrap');
            pm.select( wrapNodes[i], addFirst=True );
            pm.rename( wrapNodes[i], mainGeo + 'Node' + str(i+1) );

        # BlenShape 
        for selWrp in selsWrp:
            wrapBsh = selWrp + '_BSH'
            selGroom = selWrp.split('_ForWrap')[0];
            pm.blendShape ( selWrp, selGroom, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = wrapBsh );

        # Outliner
        cinGrp = firstName + '_CIN:Geo_Grp';
        namesGrp = 'NoTouch_GRP', cinGrp, 'Yeti_Grp';
        colorCodes = [( 1 , 0 , 0 ), ( 1 , 0 , 1 ), ( 0 , 1 , 1 )];
        for colorCode, nameGrp in zip(colorCodes, namesGrp):
            target = pm.general.PyNode( nameGrp );
            target.useOutlinerColor.set( 1 );       
            target.outlinerColor.set( colorCode );
        pm.select( clear=True );
        pm.select( 'NoTouch_GRP', r = True );
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );
        pm.select( clear=True );

    def runYetiNode( self,*args ):
        self.module.createMainNurbs();
        self.module.creatLoc();
        self.outlinerColor();
        self.blendShapeCinCol();

