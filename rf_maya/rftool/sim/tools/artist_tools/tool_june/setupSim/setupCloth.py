import pymel.core as pm;
import maya.cmds as mc;
import maya.mel as mel;
import os, ast;

mp = os.path.realpath(__file__);
fileName = os.path.basename(__file__);
mp = mp.replace( fileName , '' );
mp = mp.replace( '\\' , '/' );

from rf_maya.rftool.sim.tools.artist_tools.tool_june import module
reload(module);

class SetupSim( object ):

    def __init__( self ):
        self.module = module.MainModule();

    def refChaClothDyn( self,*args ):
        chaPath = pm.textField( 'inputChaPath' , q = True , text = True );
        if chaPath != '':
            chaName = chaPath.split('/')[-1:][0];
            chaGeoPath = chaPath + '/model/main/hero/output/' + str(chaName) + '_mdl_main_md.hero.ma'
            fileList = chaGeoPath.split('/')[-1:][0];
            pm.textField( 'inputRefDynPath', e = True, text = fileList );
        
            mc.file( chaGeoPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_CIN');
            mc.file( chaGeoPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_COL');
        else:
            pm.warning('Character Path Not Found');

    def browseClothDyn( self,*args ):
        try : 
            start = pm.textField( 'inputDynPath' , q = True , text = True );
        except :
            start = 'D:/';
        try:
            startPath = start;
            browseIM = mc.fileDialog2( cap = 'Import', fm = 4, ff = '*',dir = startPath, okc = 'Import' )[0];
            pm.textField( 'inputDynPath', e = True, text = browseIM.split('/')[-1:][0]);
            mc.file( browseIM, i = True, typ = 'mayaAscii', mnc = False );
        except:
            pm.warning( 'No Path Selected' );

        # DYN_WrpFix_GRP
        dupDyn = pm.duplicate( 'DYN_GRP' ), mc.rename('DYN_GRP1', 'DYN_WrpFix_GRP');
        selsFix = pm.listRelatives( 'DYN_WrpFix_GRP' );
        for selFix in selsFix:
            selFix.rename(selFix.name().replace('_DYN', '_DYN_WrpFix'));
            selsFixGrp = pm.listRelatives( selFix );
            for selFixGrp in selsFixGrp:
                selFixGrp.rename(selFixGrp.name().replace('_DYN', '_DYN_WrpFix'));

    def createDyn( self, *args ):
        # DYN_GRP
        simGrp = pm.group( n = 'SIM_GRP' ),pm.select( clear=True );
        DycGrp = pm.group( n = 'DYC_GRP' ),pm.select( clear=True );
        nRigidGrp = pm.group( n = 'nRigid_GRP' ),pm.select( clear=True );
        selsDyn = pm.listRelatives( 'DYN_GRP' );
        nClothGrp = pm.group( n = 'nCloth_GRP' );

        getText = self.module.readInputMainName();
        nameDict = ast.literal_eval(getText);
        splitText = nameDict[('MainName')][0].split('CIN:');
        geoLocName = splitText[0] + 'LOC';
        print 'v'
        for selDyn in selsDyn:
            print '1v'
            if '_GRP' in str(selDyn):
                print '1vv'
                listDyn = pm.listRelatives( selDyn );
                print '10'
                for subDyn in listDyn:
                    print '1vvv'
                    self.createNclothDyn( item = subDyn, nClothGrp = nClothGrp, splitText = splitText );
            else:
                print '1vvvvvvvv'
                self.createNclothDyn( item = selDyn, nClothGrp = nClothGrp, splitText = splitText );

        # reverseList
        print '11'
        for selDyn in selsDyn[::-1]:
            if '_GRP' in str(selDyn):
                listDyn = pm.listRelatives( selDyn );
                for subDyn in listDyn:
                    pm.select(subDyn);
                    pm.createDisplayLayer( noRecurse = True, name = subDyn + '_Layer' );
            else:
                    pm.select(selDyn);
                    pm.createDisplayLayer( noRecurse = True, name = selDyn + '_Layer' );
        pm.parent( 'nucleus1', nClothGrp, DycGrp, nRigidGrp, simGrp[0] );

        # FIX_GRP
        print '12'
        pm.shadingNode('blinn', asShader=True, n = 'inputMesh');
        pm.setAttr( "inputMesh.color", 1, 1, 0, type = "double3" );
        pm.setAttr( "inputMesh.eccentricity", 0.6 );
        pm.setAttr( "inputMesh.specularRollOff", 0.3 );
        pm.setAttr( "inputMesh.specularColor", (0.15, 0.15, 0.15), typ = "double3" );
        pm.select(selsDyn, r = True);
        mel.eval('displayNClothMesh "input";');
        pm.hyperShade(assign = 'inputMesh');
        pm.duplicate( rr = True );
        print '13'
        dups = mc.ls(sl = True);
        for dup in dups:
            name = dup.split('_');
            nameFix = name[0] + '_FIX';
            mc.rename(dup, nameFix);

        fixGrp = pm.group( n = 'FIX_GRP' );
        pm.parent( nameFix, fixGrp );
        pm.parent( fixGrp, geoLocName );
        pm.reorder( fixGrp, r = -1 );
        print '14'
        # FIX_GRP BlendShape to DYN_GRP
        selFix = pm.listRelatives( 'FIX_GRP' );
        selDyn = pm.listRelatives( 'DYN_GRP' );
        for driver, driven in zip(selFix, selDyn):
            nameBsh = driver + '_BSH';
            pm.blendShape ( driver, driven, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = nameBsh );

        # Dyn to current mesh
        pm.select(selsDyn, r = True);
        mel.eval('displayNClothMesh "current";');

        if pm.radioCollection( 'dynWrapFixSwitch', q = True, sl = True ) == 'dynWrapFixSwitchOn':

            # DYN_WrpFix_GRP
            selsFix = pm.listRelatives( 'DYN_WrpFix_GRP' );
            for selFix in selsFix:
                fixGeo = selFix.split('_')[0]
                getCinGeo = splitText[0] + 'CIN:' + fixGeo + '_Geo';
                pm.select(selFix, r = True);
                pm.select(getCinGeo, add = True);
                mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');

            self.module.parentWrapGeoBase( wrapNodeGrp = 'DYN_WrpFix_GRP' );
        print '1'
        if pm.radioCollection( 'dynWrapFixSwitch', q = True, sl = True ) == 'dynWrapFixSwitchOff':
            pass

    def createNclothDyn( self, item, nClothGrp, splitText, *args ):
        print '----1----'
        pm.select(item);
        print '----222----'
        createNCloth = mel.eval( 'createNCloth 0;' );
        print '----3----'
        splitName = item.split('_DYN');
        print '----4----'
        pm.rename( createNCloth[0], splitName[0] + '_' + 'nClothShape' );
        print '----5----'
        pp = pm.selected()
        nCloth = pp[0].name()
        
        nClothName = pm.rename( nCloth, splitName[0] + '_' + 'nCloth' );
        print '----7----'
        pm.parent( nClothName, nClothGrp );
        print '----8----'
        pm.select( item );
        print '----9----'
        shapes = pm.listRelatives( s = True );
        print '----10----'
        if not shapes[-1] == 'outputCloth':
            outputName = item + '_outputCloth'
            pm.rename( shapes[-1], outputName),pm.select( clear=True );

        #hideGeoCol
        print '----11----'
        geoCol = item.split('_')[0];
        getGeoCol = splitText[0] + 'COL:' + geoCol + '_Geo';
        print '----122----'
        pm.select(getGeoCol);
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );

    def reorderEachGrp( self, *args ):
        getText = self.module.readInputMainName();
        nameDict = ast.literal_eval(getText);
        getGrpCin = nameDict[('MainName')][1];
        pm.reorder( getGrpCin, r = 3 );
        reoderCOL = nameDict[('MainName')][2];
        pm.reorder( reoderCOL, r = -1 );

        # visibility
        pm.select( getGrpCin, r = True );
        pm.select( 'NoTouch_GRP', add = True );
        pm.select( 'FIX_GRP', add = True );
        pm.select( 'DYN_WrpFix_GRP', add = True );
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );

    def outlinerColorDyn( self, *args ):
        getText = self.module.readInputMainName();
        nameDict = ast.literal_eval(getText);
        getGrpCin = nameDict[('MainName')][1];
        splitText = nameDict[('MainName')][0].split('CIN:');
        geoLocName = splitText[0] + 'LOC';
        pm.parent( 'DYN_GRP', 'DYN_WrpFix_GRP' , geoLocName);
        
        namesGrp = 'NoTouch_GRP', geoLocName, 'SIM_GRP', 'DYN_GRP' , getGrpCin, 'FIX_GRP', 'DYN_WrpFix_GRP';
        colorCodes = [( 1 , 0 , 0 ), ( 0.498039 , 1 , 0 ), ( 0 , 1 , 1 ), ( 1 , 1 , 0 ), ( 1 , 0 , 1 ), ( 1 , 0 , 1 ), ( 0.4 , 1 , 0.8 )]
        
        for colorCode, nameGrp in zip(colorCodes, namesGrp):
            target = pm.general.PyNode( nameGrp );
            target.useOutlinerColor.set( 1 );       
            target.outlinerColor.set( colorCode );

    def blendShapeCinCol( self, *args ):
        getText = self.module.readInputMainName();
        nameDict = ast.literal_eval(getText);
        getGrpCin = nameDict[('MainName')][1];
        getGrpCol = nameDict[('MainName')][2];
        oriName = getGrpCin.split(':');

        firstFraming = 1.0;
        secondFraming = 10.0;

        mc.blendShape( getGrpCin , getGrpCol, o = 'world', automatic = True , n = 'CIN_COL_BSH');
        mc.setAttr('CIN_COL_BSH.' + oriName[1] , 1);
        mc.setKeyframe( 'CIN_COL_BSH.' + oriName[1], t=[secondFraming]);
        mc.currentTime( firstFraming );
        mc.setAttr('CIN_COL_BSH.' + oriName[1] , 0);
        mc.setKeyframe( 'CIN_COL_BSH.' + oriName[1], t=[firstFraming]);

    def runClothDyn( self, *args ):
        self.module.createMainNurbs();
        print 'CHECK01'
        self.module.creatLoc();
        print 'CHECK21'
        self.createDyn(),pm.select( clear=True );
        print 'CHECK1'
        self.reorderEachGrp();
        print 'CHECK12'
        self.outlinerColorDyn();
        print 'CHECK13'
        self.blendShapeCinCol();
        print 'CHECK14'

class SetupWrp( object ):

    def __init__( self ):
        self.module = module.MainModule();

    def refChaClothWrp( self, *args ):
        chaPath = pm.textField( 'inputChaPath' , q = True , text = True );
        if chaPath != '':
            chaName = chaPath.split('/')[-1:][0];
            chaGeoPath = chaPath + '/model/main/hero/output/' + str(chaName) + '_mdl_main_md.hero.ma'
            fileList = chaGeoPath.split('/')[-1:][0];
            pm.textField( 'inputRefWrpPath', e = True, text = fileList );
            mc.file( chaGeoPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_CIN');
            mc.file( chaGeoPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_GRP');
            cinGrp = chaName[:1].capitalize() + chaName[1:] + '_CIN:Geo_Grp'
            geoGrp = chaName[:1].capitalize() + chaName[1:] + '_GRP:Geo_Grp'
            nameBsh = 'CIN_GEO_BSH';
            pm.blendShape ( cinGrp, geoGrp, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = nameBsh );
        else:
            pm.warning('Character Path Not Found');

    def browseClothWrp( self, *args ):
        try : 
            start = pm.textField( 'inputDynWrpPath' , q = True , text = True );
        except :
            start = 'D:/';
        try:
            startPath = start;
            browseIM = mc.fileDialog2( cap = 'Import', fm = 4, ff = '*',dir = startPath, okc = 'Import' )[0];
            pm.textField( 'inputDynWrpPath', e = True, text = browseIM.split('/')[-1:][0]);
            mc.file( browseIM, i = True, typ = 'mayaAscii', mnc = False );
        except:
            pm.warning( 'No Path Selected' );

        # DYN_WrpFix_GRP
        dupDyn = pm.duplicate( 'DYN_GRP' ), mc.rename('DYN_GRP1', 'DYN_WrpFix_GRP');
        selsFix = pm.listRelatives( 'DYN_WrpFix_GRP' );
        for selFix in selsFix:
            selFix.rename(selFix.name().replace('_DYN', '_DYN_WrpFix'));

    def noTouchWrp(self, *args):
        selsForWrap = pm.listRelatives( 'DYN_GRP' );
        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];
        pm.group( n = 'WrapNode_GRP' );
        pm.select( clear=True );
        pm.parent('WrapNode_GRP', 'NoTouch_GRP');

        # Output_Layer
        outputLayer = pm.createDisplayLayer( name = 'Geo_Output_Layer' , number = 1 , empty = True );
        outputLayer.color.set( 17 );
        geoOutput = firstName + '_GRP:Geo_Grp';
        pm.editDisplayLayerMembers( outputLayer, geoOutput );

        if pm.radioCollection( 'wrapSwitch', q = True, sl = True ) == 'wrapSwitchOn':
        
            # DuplicateGeoGrp / Wrap / DTM / TNS
            for selDyn in selsForWrap:
                if '_GRP' in str( selDyn ):
                    dynGrp = selDyn.split('_')[0];
                    getGeoGrp = firstName + '_GRP:' + dynGrp + '_Geo';
                    pm.select(getGeoGrp);
                    dupWrp = pm.duplicate( rr = True );
                    wrpName = dupWrp[0] + '_ForWrap'; 
                    geoWrp = pm.rename(dupWrp[0], wrpName)
                    pm.parent(geoWrp, 'WrapNode_GRP');
                    pm.select(geoWrp, r = True);
                    pm.select(selDyn, add = True);
                    mel.eval('doWrapArgList "7" { "1","0","1", "2", "0", "1", "0", "0" }');
                else:
                    dynGrp = selDyn.split('_')[0];
                    getGeoGrp = firstName + '_GRP:' + dynGrp + '_Geo';
                    pm.select(getGeoGrp);
                    dupWrp = pm.duplicate( rr = True );
                    wrpName = dupWrp[0] + '_ForWrap'; 
                    geoWrp = pm.rename(dupWrp[0], wrpName)
                    pm.parent(geoWrp, 'WrapNode_GRP');
                    pm.select(geoWrp, r = True);
                    pm.select(selDyn, add = True);
                    mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');
                dtmName = geoWrp + '_DTM';
                tnsName = geoWrp + '_TNS';
                pm.select(geoWrp);
                pm.tension(smoothingIterations = 10, smoothingStep = 1, pinBorderVertices = 1, envelope = 1, n = tnsName);
                pm.select(geoWrp);
                pm.deltaMush(smoothingIterations = 10, smoothingStep = 0.5, pinBorderVertices = 1, envelope = 1, n = dtmName);
                nameBsh = geoWrp + '_BSH';
                pm.blendShape ( geoWrp, getGeoGrp, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = nameBsh );

            # WrapBase
            self.module.parentForWrapBase( getGrp = 'DYN_GRP', wrapNodeGrp = 'WrapNode_GRP' );

            # DYN_WrpFix_GRP
            selsFix = pm.listRelatives( 'DYN_WrpFix_GRP' );
            for selFix in selsFix:
                fixGeo = selFix.split('_')[0]
                getCinGeo = firstName + '_CIN:' + fixGeo + '_Geo';
                pm.select(selFix, r = True);
                pm.select(getCinGeo, add = True);
                mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');

            self.module.parentWrapGeoBase( wrapNodeGrp = 'DYN_WrpFix_GRP' );

            #reorderDeformers
            for selDyn in selsForWrap:
                dynGrp = selDyn.split('_')[0];
                getGeoGrp = firstName + '_GRP:' + dynGrp + '_Geo';
                wrapGeos = selDyn.split('_')[0] + '_Geo_ForWrap';
                nameBsh = wrapGeos + '_BSH';
                pm.select(getGeoGrp);
                selection = pm.ls( sl = True );
                selectionName = selection[0].nodeName();
                getrootGeo = pm.general.PyNode(selectionName);
                rootGeo = getrootGeo.longName();
                pm.reorderDeformers( nameBsh, 'CIN_GEO_BSH', rootGeo );

            # WRP_Layer
            wrpLayer = pm.createDisplayLayer( name = 'WRP_Layer' , number = 1 , empty = True );
            wrpLayer.color.set( 17 );
            for selGeo in selsForWrap:
                dynGrp = selGeo.split('_')[0];
                getGeoGrp = firstName + '_GRP:' + dynGrp + '_Geo';
                pm.editDisplayLayerMembers( wrpLayer, getGeoGrp );

        if pm.radioCollection( 'wrapSwitch', q = True, sl = True ) == 'wrapSwitchOff':
            wrpLayer = pm.createDisplayLayer( name = 'WRP_Layer' , number = 1 , empty = True );
            wrpLayer.color.set( 17 );

        # layer
        dynLayer = pm.createDisplayLayer( name = 'DYN_Layer' , number = 1 , empty = True );
        dynLayer.color.set( 9 );
        pm.editDisplayLayerMembers( dynLayer, selsForWrap );

    def noTouchWrpNew(self, *args):
        selsForWrap = pm.listRelatives( 'DYN_GRP' );
        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];

        # DYN_ForWrap_GRP
        dynForWrapLayer = pm.createDisplayLayer( name = 'DYN_ForWrap_Layer' , number = 1 , empty = True );
        dynForWrapLayer.color.set( 9 );
        dupDyn = pm.duplicate( 'DYN_GRP' ), mc.rename( 'DYN_GRP1', 'DYN_ForWrap_GRP' );
        selsForWrap = pm.listRelatives( 'DYN_ForWrap_GRP' );
        for selForWrap in selsForWrap:
            pm.editDisplayLayerMembers( dynForWrapLayer, selForWrap );
            selForWrap.rename( selForWrap.name().replace( '_DYN', '_DYN_ForWrap' ));
            if '_GRP' in str( selForWrap ):
                listForWrap = pm.listRelatives( selForWrap );
                for subForWrap in listForWrap:
                    subForWrap.rename(subForWrap.name().replace( '_DYN', '_DYN_ForWrap' ));

        pm.group( n = 'WrapNode_GRP' );
        pm.select( clear=True );
        pm.group( n = 'WRP_GRP' );
        pm.select( clear=True );
        pm.blendShape ( 'DYN_GRP', 'DYN_ForWrap_GRP', origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = 'DYN_CIN_ForWrap_BSH' );

        # Output_Layer
        outputLayer = pm.createDisplayLayer( name = 'Geo_Output_Layer' , number = 1 , empty = True );
        outputLayer.color.set( 17 );
        geoOutput = firstName + '_GRP:Geo_Grp';
        pm.editDisplayLayerMembers( outputLayer, geoOutput );

        if pm.radioCollection( 'wrapSwitch', q = True, sl = True ) == 'wrapSwitchOn':

            # Duplicate and smooth DYN_ForWrap_GRP
            selsForWrap = pm.listRelatives( 'DYN_ForWrap_GRP' );
            for selDyn in selsForWrap:
                if '_GRP' in str( selDyn ):
                    dynGrp = selDyn.split('_')[0];
                    getGeoGrp = firstName + '_GRP:' + dynGrp + '_Geo';
                    pm.select(getGeoGrp);
                    dupWrp = pm.duplicate( rr = True );
                    wrpName = dupWrp[0] + '_ForWrap'; 
                    geoWrp = pm.rename(dupWrp[0], wrpName);
                    pm.parent(geoWrp, 'WrapNode_GRP');

                    # Create Hi_Geo in WrapNode_GRP
                    pm.select(geoWrp);
                    dupGeo = pm.duplicate( rr = True );
                    dupGeoHi = dupGeo[0].split('_Geo_ForWrap')[0] + '_Hi_Geo_ForWrap'; 
                    geoWrpHi = pm.rename(dupGeo[0], dupGeoHi);
                    pm.polySmooth(dv = 1, c = 1, kb = 1, khe = 0, kt = 1, peh = 0, ch = 1);
                    pm.delete ( geoWrpHi , ch = True );

                    # Create Hi_DYN and wrap dynHi with dayLow
                    listsDynLow = pm.listRelatives( selDyn );
                    pm.select(selDyn);
                    dupForWrapGrp = pm.duplicate( rr = True );
                    dynForWrapGrp = dupForWrapGrp[0].rename( dupForWrapGrp[0].name().replace( '_DYN_ForWrap_GRP1', '_Hi_DYN_ForWrap_GRP' ));
                    listsDynHi = pm.listRelatives( dynForWrapGrp );
                    for subDynHi, subDynLow in zip(listsDynHi, listsDynLow):
                        dupDynWrpHi = subDynHi.rename( subDynHi.name().replace( '_DYN_ForWrap', '_Hi_DYN_ForWrap' ));
                        pm.select(dupDynWrpHi);
                        pm.polySmooth(dv = 1, c = 1, kb = 1, khe = 0, kt = 1, peh = 0, ch = 1);
                        pm.delete(dupDynWrpHi , ch = True);
                        pm.select(subDynHi, r = True);
                        pm.select(subDynLow, add = True);
                        mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');
                else:
                    dynGrp = selDyn.split('_')[0];
                    getGeoGrp = firstName + '_GRP:' + dynGrp + '_Geo';
                    pm.select(getGeoGrp);
                    dupWrp = pm.duplicate( rr = True );
                    wrpName = dupWrp[0] + '_ForWrap'; 
                    geoWrp = pm.rename(dupWrp[0], wrpName)
                    pm.parent(geoWrp, 'WrapNode_GRP');

                    # Create Hi_Geo in WrapNode_GRP
                    pm.select(geoWrp);
                    dupGeo = pm.duplicate( rr = True );
                    dupGeoHi = dupGeo[0].split('_Geo_ForWrap')[0] + '_Hi_Geo_ForWrap'; 
                    geoWrpHi = pm.rename(dupGeo[0], dupGeoHi);
                    pm.polySmooth(dv = 1, c = 1, kb = 1, khe = 0, kt = 1, peh = 0, ch = 1);
                    pm.delete ( geoWrpHi , ch = True );

                    # Create Hi_DYN and wrap dynHi with dayLow
                    pm.select(selDyn);
                    dupDyn = pm.duplicate( rr = True );
                    dupDynWrpHi = dupDyn[0].split('_DYN_ForWrap')[0] + '_Hi_DYN_ForWrap';
                    dynWrpHi = pm.rename(dupDyn[0], dupDynWrpHi);
                    pm.polySmooth(dv = 1, c = 1, kb = 1, khe = 0, kt = 1, peh = 0, ch = 1);
                    pm.delete(dupDynWrpHi , ch = True) ;
                    pm.select(dupDynWrpHi, r = True);
                    pm.select(selDyn, add = True);
                    mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');

            # Create wrap Hi_Geo with Hi_DYN and blendShape to geo output
            selsForWrap = pm.listRelatives( 'DYN_ForWrap_GRP' );
            listsDynHi = [];
            for dynHi in selsForWrap:
                if '_Hi_' in str(dynHi):
                    listsDynHi.append(dynHi); 

            for wrpHi in listsDynHi:
                if '_GRP' in str(wrpHi):
                    hiGeoName = wrpHi.split('_DYN_ForWrap_GRP')[0] + '_Geo_ForWrap';
                    pm.select(hiGeoName, r = True);
                    pm.select(wrpHi, add = True);
                    mel.eval('doWrapArgList "7" { "1","0","1", "2", "0", "1", "0", "0" }');
                else:
                    hiGeoName = wrpHi.split('_DYN_ForWrap')[0] + '_Geo_ForWrap';
                    pm.select(hiGeoName, r = True);
                    pm.select(wrpHi, add = True);
                    mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');

                dtmName = hiGeoName + 'DTM';
                pm.select(hiGeoName);
                pm.deltaMush(smoothingIterations = 10, smoothingStep = 0.5, pinBorderVertices = 1, envelope = 1, n = dtmName);
                geoForWrap = hiGeoName.split('_')[0] + '_Geo_ForWrap';
                pm.select(geoForWrap, r = True);
                pm.select(hiGeoName, add = True);
                mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');
                geoOutput = firstName + '_GRP:' + hiGeoName.split('_')[0] + '_Geo';
                nameBsh = geoForWrap + '_BSH';
                pm.blendShape ( geoForWrap, geoOutput, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = nameBsh );

            pm.select( clear=True );
            pm.group( n = 'DYN_ForWrapBase_GRP' );
            pm.select( clear=True );
            pm.group( n = 'WrapNodeBase_GRP' );
            pm.select( clear=True );

            # WrapBase
            self.module.parentForWrapBase( getGrp = 'DYN_ForWrap_GRP', wrapNodeGrp = 'DYN_ForWrapBase_GRP' );
            self.module.parentForWrapBase( getGrp = 'WrapNode_GRP', wrapNodeGrp = 'WrapNodeBase_GRP' );
            pm.parent('DYN_ForWrapBase_GRP', 'DYN_ForWrap_GRP');
            pm.parent('WrapNodeBase_GRP', 'WrapNode_GRP');

            # DYN_WrpFix_GRP
            selsFix = pm.listRelatives( 'DYN_WrpFix_GRP' );
            for selFix in selsFix:
                fixGeo = selFix.split('_')[0]
                getCinGeo = firstName + '_CIN:' + fixGeo + '_Geo';
                pm.select(selFix, r = True);
                pm.select(getCinGeo, add = True);
                mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');

            self.module.parentWrapGeoBase( wrapNodeGrp = 'DYN_WrpFix_GRP' );

            #reorderDeformers
            for selDyn in selsForWrap:
                dynGrp = selDyn.split('_')[0];
                getGeoGrp = firstName + '_GRP:' + dynGrp + '_Geo';
                wrapGeos = selDyn.split('_')[0] + '_Geo_ForWrap';
                nameBsh = wrapGeos + '_BSH';
                pm.select(getGeoGrp);
                selection = pm.ls( sl = True );
                selectionName = selection[0].nodeName();
                getrootGeo = pm.general.PyNode(selectionName);
                rootGeo = getrootGeo.longName();
                pm.reorderDeformers( nameBsh, 'CIN_GEO_BSH', rootGeo );

            # WRP_Layer
            wrpLayer = pm.createDisplayLayer( name = 'WRP_Layer' , number = 1 , empty = True );
            wrpLayer.color.set( 17 );
            for selGeo in selsForWrap:
                dynGrp = selGeo.split('_')[0];
                getGeoGrp = firstName + '_GRP:' + dynGrp + '_Geo';
                pm.editDisplayLayerMembers( wrpLayer, getGeoGrp );

        if pm.radioCollection( 'wrapSwitch', q = True, sl = True ) == 'wrapSwitchOff':
            wrpLayer = pm.createDisplayLayer( name = 'WRP_Layer' , number = 1 , empty = True );
            wrpLayer.color.set( 17 );

    def outlinerColorWrp( self, *args ): 
        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];
        cinGrp = firstName + '_CIN:Geo_Grp';
        geoGrp = firstName + '_GRP:Geo_Grp';
        getLoc = firstName + '_LOC'
        namesGrp = 'NoTouch_GRP', geoGrp, 'DYN_GRP' , cinGrp, getLoc, 'DYN_WrpFix_GRP';
        colorCodes = [( 1 , 0 , 0 ), ( 1 , 1 , 0 ), ( 1 , 0 , 1 ), ( 1 , 0 , 1 ), ( 0.498039 , 1 , 0 ), ( 0.4 , 1 , 0.8 )];
        
        for colorCode, nameGrp in zip(colorCodes, namesGrp):
            target = pm.general.PyNode( nameGrp );
            target.useOutlinerColor.set( 1 );       
            target.outlinerColor.set( colorCode );

        # parent grp and visibility
        pm.parent( 'DYN_ForWrap_GRP', 'WRP_GRP');
        pm.parent( 'WrapNode_GRP', 'WRP_GRP');
        pm.parent( 'WRP_GRP', 'NoTouch_GRP');
        pm.parent( 'DYN_WrpFix_GRP', getLoc);
        pm.select( cinGrp, r = True );
        pm.select( 'NoTouch_GRP', add = True );
        pm.select( 'DYN_WrpFix_GRP', add = True );
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );
        pm.select( clear=True );

    def renameDynCinGrp ( self, *args ):
        pm.select('DYN_GRP');
        sels = pm.ls( sl = True );
        doSP = sels[0].split('_')[0] + '_CIN_GRP';
        doName = pm.rename(sels[0], doSP );
        pm.select(doName);
        for dynGrp in pm.selected():
            pm.parent(w = True );
            selsDynGrp = pm.listRelatives( dynGrp );
            for selDynGrp in selsDynGrp:
                selDynGrp.rename(selDynGrp.name().replace('_DYN', '_DYN_CIN'));
                selsDyn = pm.listRelatives( selDynGrp );
                for selDyn in selsDyn:
                    selDyn.rename(selDyn.name().replace('_DYN', '_DYN_CIN'));

        # DYN_CIN_layer
        dynCinLayer = pm.createDisplayLayer( name = 'DYN_CIN_Layer' , number = 1 , empty = True );
        dynCinLayer.color.set( 9 );
        pm.editDisplayLayerMembers( dynCinLayer, 'DYN_CIN_GRP' );

        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];
        geoLocName = firstName + '_LOC';
        pm.parent(doSP, geoLocName);

    def runClothWrp( self, *args ):
        self.module.createMainNurbs();
        self.module.creatLoc();
        self.noTouchWrpNew(),pm.select( clear=True );
        self.outlinerColorWrp();
        self.renameDynCinGrp();

    def reNamespaceOutputWrap( self, *args ):
        currentProject = pm.workspace( q = True, rd = True ).split('/')[4];
        sels = pm.ls( sl = True )[0];
        newName = currentProject + ':' + sels.split(':')[1];
        pm.namespace( ren = (sels.split(':')[0], newName.split(':')[0]) );

class SetupWrpFix( object ):

    def __init__( self ):
        self.module = module.MainModule();

    def refChaClothWrpFix( self, *args ):
        chaPath = pm.textField( 'inputChaPath' , q = True , text = True );
        if chaPath != '':
            chaName = chaPath.split('/')[-1:][0];
            chaGeoPath = chaPath + '/model/main/hero/output/' + str(chaName) + '_mdl_main_md.hero.ma'
            fileList = chaGeoPath.split('/')[-1:][0];
            pm.textField( 'inputRefWrpFixPath', e = True, text = fileList );
        
            mc.file( chaGeoPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_CIN');
            mc.file( chaGeoPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_GRP');
        else:
            pm.warning('Character Path Not Found');

    def browseClothWrpFix( self,*args ):
        try : 
            start = pm.textField( 'inputDynWrpFixPath' , q = True , text = True );
        except :
            start = 'D:/';
        try:
            startPath = start;
            browseIM = mc.fileDialog2( cap = 'Import', fm = 4, ff = '*',dir = startPath, okc = 'Import' )[0];
            pm.textField( 'inputDynWrpFixPath', e = True, text = browseIM.split('/')[-1:][0]);
            mc.file( browseIM, i = True, typ = 'mayaAscii', mnc = False );
        except:
            pm.warning( 'No Path Selected' );

    def noTouchWrpFix(self, *args):
        pm.group( n = 'DYN_WRP_GRP' );
        pm.select( clear=True );
        pm.group( n = 'GEO_WRP_GRP' );
        pm.parent('GEO_WRP_GRP','DYN_WRP_GRP', 'NoTouch_GRP');

        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];
        geoGrp = firstName + '_GRP';

        if pm.radioCollection( 'wrapFixSwitch', q = True, sl = True ) == 'wrapFixSwitchOn':

            # DuplicateGeoGrp
            selsDyn = pm.listRelatives( 'DYN_GRP' );
            for selDyn in selsDyn:
                dynGrp = selDyn.split('_')[0];
                getGeoGrp = geoGrp + ':' + dynGrp + '_Geo';
                pm.select(getGeoGrp);
                mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );
                dupWrpFix = pm.duplicate( rr = True );
                geoWrpFix = pm.rename(dynGrp + '_Geo', dynGrp + '_Geo' + '_ForWrap')
                pm.parent(geoWrpFix, 'GEO_WRP_GRP');
                nameBsh = getGeoGrp + '_BSH';
                pm.blendShape ( getGeoGrp, geoWrpFix, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = nameBsh );

                pm.select(selDyn);
                dupWrpFix = pm.duplicate( rr = True );
                dynWrpFix = pm.rename(dupWrpFix[0], selDyn + '_ForWrap')

                pm.select(dynWrpFix, r = True);
                pm.select(geoWrpFix, add=True );
                mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');

                dtmName = dynWrpFix + '_DTM';
                pm.select(dynWrpFix);
                pm.deltaMush(smoothingIterations = 10, smoothingStep = 0.5, pinBorderVertices = 1, envelope = 1, n = dtmName);
                pm.parent(dynWrpFix, 'DYN_WRP_GRP');
                dynBsh = dynWrpFix + '_BSH'
                pm.blendShape ( dynWrpFix, selDyn, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = dynBsh );

                pm.select(geoWrpFix, r = True);
                wrapNodes = pm.listConnections(t = 'wrap');
                for wrapNode in wrapNodes:
                    pm.select( wrapNode, addFirst=True );
                    pm.rename( wrapNode, geoWrpFix + 'Node');

        if pm.radioCollection( 'wrapFixSwitch', q = True, sl = True ) == 'wrapFixSwitchOff':
            pass

    def renameDynFixGrp ( self, *args ):
        pm.select('DYN_GRP');
        sels = pm.ls( sl = True );
        doSP = sels[0].split('_')[0] + '_FIX_GRP';
        doName = pm.rename(sels[0], doSP );
        pm.select(doName);
        for dynGrp in pm.selected():
            pm.parent(w = True );
            selsDynGrp = pm.listRelatives( dynGrp );
            for selDynGrp in selsDynGrp:
                selDynGrp.rename(selDynGrp.name().replace('_DYN', '_DYN_FIX'));
                selsDyn = pm.listRelatives( selDynGrp );
                for selDyn in selsDyn:
                    selDyn.rename(selDyn.name().replace('_DYN', '_DYN_FIX'));

    def outlinerColorWrpFix( self, *args ): 
        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];
        cinGrp = firstName + '_CIN:Geo_Grp';
        getLoc = firstName + '_LOC'
        selsDyn = pm.listRelatives( 'DYN_FIX_GRP' );
        namesGrp = 'NoTouch_GRP', 'DYN_FIX_GRP' , cinGrp, getLoc;
        colorCodes = [( 1 , 0 , 0 ), ( 1 , 1 , 0 ), ( 1 , 0 , 1 ), ( 0.498039 , 1 , 0 )];
        
        for colorCode, nameGrp in zip(colorCodes, namesGrp):
            target = pm.general.PyNode( nameGrp );
            target.useOutlinerColor.set( 1 );       
            target.outlinerColor.set( colorCode );

        pm.select( clear=True );
        pm.select( cinGrp, r = True );
        pm.select( 'NoTouch_GRP', add = True );
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );
        pm.select( clear=True );
        pm.parent('DYN_FIX_GRP', getLoc);

    def blendShapeCinGrp( self, *args ):
        getText = self.module.readInputMainName();
        nameDict = ast.literal_eval(getText);
        getGrpCin = nameDict[('MainName')][1];
        getGrpCol = nameDict[('MainName')][2];
        oriName = getGrpCin.split(':');

        firstFraming = 1.0;
        secondFraming = 10.0;

        mc.blendShape( getGrpCin , getGrpCol, o = 'world', automatic = True , n = 'CIN_GRP_BSH');
        mc.setAttr('CIN_GRP_BSH.' + oriName[1] , 1);
        mc.setKeyframe( 'CIN_GRP_BSH.' + oriName[1], t=[secondFraming]);
        mc.currentTime( firstFraming );
        mc.setAttr('CIN_GRP_BSH.' + oriName[1] , 0);
        mc.setKeyframe( 'CIN_GRP_BSH.' + oriName[1], t=[firstFraming]);

    def runClothWrpFix( self, *args ):
        self.module.createMainNurbs();
        self.module.creatLoc();
        self.noTouchWrpFix(),pm.select( clear=True );
        self.renameDynFixGrp(),pm.select( clear=True );
        self.outlinerColorWrpFix();
        self.blendShapeCinGrp();

class SetupClothAuto( object ):

    def __init__( self ):
        self.module = module.MainModule();

    def refChaClothAutoSim( self,*args ):
        chaPath = pm.textField( 'inputChaPath' , q = True , text = True );
        try:
            if chaPath != '':
                chaName = chaPath.split('/')[-1:][0];
                chaGeoPath = chaPath + '/model/main/hero/output/' + str(chaName) + '_mdl_main_md.hero.ma'
                fileList = chaGeoPath.split('/')[-1:][0];
                pm.textField( 'inputRefClothAutoSimPath', e = True, text = fileList );
                mc.file( chaGeoPath, r = True , ns = chaName[:1].capitalize() + chaName[1:] + '_GRP');

            colGrp = chaName[:1].capitalize() + chaName[1:] + '_COL:Geo_Grp'
            geoGrp = chaName[:1].capitalize() + chaName[1:] + '_GRP:Geo_Grp'
            nameBsh = 'COL_GEO_BSH';
            pm.blendShape ( colGrp, geoGrp, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = nameBsh );
            pm.group( n = 'WRP_GRP' );
            pm.parent( geoGrp, 'WRP_GRP' );
            pm.select( clear=True );
        except:
            pm.warning('Character Path Not Found');

    def cleanOutputCloth( self, selsName, *args ):
        self.module.deleteOrig( target = selsName );
        pm.select( selsName, r=True ), pm.pickWalk( d='down' ), pm.delete();
        getName = pm.listRelatives(selsName);
        for name in getName:
            name.rename(name.name().replace( '_outputCloth', 'Shape' ));

    def dynLayerVisibility( self ):
        dispLayers = mc.ls(type = 'displayLayer');
        for layers in dispLayers:
            if 'DYN' in layers:
                # mel.eval( 'layerEditorLayerButtonVisibilityChange %s' % layers );
                pm.setAttr( '%s.visibility' % layers, 0);

    def runClothAutoSim( self, *args ):
        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];

        # DYN_ForWrap_GRP
        dynForWrapLayer = pm.createDisplayLayer( name = 'DYN_ForWrap_Layer' , number = 1 , empty = True );
        dynForWrapLayer.color.set( 9 );
        dupDyn = pm.duplicate( 'DYN_GRP' ), mc.rename( 'DYN_GRP1', 'DYN_ForWrap_GRP' );
        selsForWrap = pm.listRelatives( 'DYN_ForWrap_GRP' );
        for selForWrap in selsForWrap:
            pm.editDisplayLayerMembers( dynForWrapLayer, selForWrap );
            selForWrap.rename( selForWrap.name().replace( '_DYN', '_DYN_ForWrap' ));
            if '_GRP' in str( selForWrap ):
                listForWrap = pm.listRelatives( selForWrap );
                for subForWrap in listForWrap:
                    subForWrap.rename(subForWrap.name().replace( '_DYN', '_DYN_ForWrap' ));
                    self.cleanOutputCloth( selsName = subForWrap );
            else:
                self.cleanOutputCloth( selsName = selForWrap );

        pm.group( n = 'WrapNode_GRP' );
        pm.select( clear=True );
        pm.blendShape ( 'DYN_GRP', 'DYN_ForWrap_GRP', origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = 'DYN_ForWrap_BSH' );

        # Output_Layer
        outputLayer = pm.createDisplayLayer( name = 'Geo_Output_Layer' , number = 1 , empty = True );
        outputLayer.color.set( 17 );
        geoOutput = firstName + '_GRP:Geo_Grp';
        pm.editDisplayLayerMembers( outputLayer, geoOutput );

        if pm.radioCollection( 'autoSimSwitch', q = True, sl = True ) == 'autoSimSwitchOn':
        
            # DuplicateGeoGrp / Wrap / DTM / TNS
            for selDyn in selsForWrap:
                if '_GRP' in str( selDyn ):
                    dynGrp = selDyn.split('_')[0];
                    getGeoGrp = firstName + '_GRP:' + dynGrp + '_Geo';
                    pm.select(getGeoGrp);
                    dupWrp = pm.duplicate( rr = True );
                    wrpName = dupWrp[0] + '_ForWrap'; 
                    geoWrp = pm.rename(dupWrp[0], wrpName)
                    pm.parent(geoWrp, 'WrapNode_GRP');
                    pm.select(geoWrp, r = True);
                    pm.select(selDyn, add = True);
                    mel.eval('doWrapArgList "7" { "1","0","1", "2", "0", "1", "0", "0" }');
                else:
                    dynGrp = selDyn.split('_')[0];
                    getGeoGrp = firstName + '_GRP:' + dynGrp + '_Geo';
                    pm.select(getGeoGrp);
                    dupWrp = pm.duplicate( rr = True );
                    wrpName = dupWrp[0] + '_ForWrap'; 
                    geoWrp = pm.rename(dupWrp[0], wrpName)
                    pm.parent(geoWrp, 'WrapNode_GRP');
                    pm.select(geoWrp, r = True);
                    pm.select(selDyn, add = True);
                    mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');
                dtmName = geoWrp + 'DTM';
                tnsName = geoWrp + 'TNS';
                pm.select(geoWrp);
                pm.tension(smoothingIterations = 10, smoothingStep = 1, pinBorderVertices = 1, envelope = 1, n = tnsName);
                pm.select(geoWrp);
                pm.deltaMush(smoothingIterations = 10, smoothingStep = 0.5, pinBorderVertices = 1, envelope = 1, n = dtmName);
                nameBsh = geoWrp + '_BSH';
                pm.blendShape ( geoWrp, getGeoGrp, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = nameBsh );

            # WrapBase
            self.module.parentForWrapBase( getGrp = 'DYN_ForWrap_GRP', wrapNodeGrp = 'WrapNode_GRP' );

            # WRP_Layer
            wrpLayer = pm.createDisplayLayer( name = 'WRP_Layer' , number = 1 , empty = True );
            wrpLayer.color.set( 17 );
            for selGeo in selsForWrap:
                dynGrp = selGeo.split('_')[0];
                getGeoGrp = firstName + '_GRP:' + dynGrp + '_Geo';
                pm.editDisplayLayerMembers( wrpLayer, getGeoGrp );

        if pm.radioCollection( 'autoSimSwitch', q = True, sl = True ) == 'autoSimSwitchOff':
            wrpLayer = pm.createDisplayLayer( name = 'WRP_Layer' , number = 1 , empty = True );
            wrpLayer.color.set( 17 );

        pm.select( 'WrapNode_GRP', r = True );
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );
        pm.select( clear=True );
        pm.parent( 'DYN_ForWrap_GRP', 'WrapNode_GRP', 'WRP_GRP');
        target = pm.general.PyNode( 'DYN_ForWrap_GRP' );
        target.useOutlinerColor.set( 0 );
        pm.parent( firstName + '_LOC', 'SIM_GRP');
        pm.reorder( firstName + '_LOC', r = -4 );
        pm.parent( 'NoTouch_GRP', 'SIM_GRP');
        pm.reorder( 'NoTouch_GRP', r = -5 );
        self.module.nucleusTrack();

        # outliner
        geoGrp = firstName + '_GRP:Geo_Grp';
        namesGrp = geoGrp, 'WRP_GRP';
        colorCodes = [( 1 , 1 , 0 ), ( 1 , 1 , 0 )];
        
        for colorCode, nameGrp in zip(colorCodes, namesGrp):
            target = pm.general.PyNode( nameGrp );
            target.useOutlinerColor.set( 1 );       
            target.outlinerColor.set( colorCode );

    def runClothAutoSimNew( self, *args ):
        chaName = self.module.getCharacterName();
        firstName = chaName[:1].capitalize() + chaName[1:];

        # DYN_ForWrap_GRP
        dynForWrapLayer = pm.createDisplayLayer( name = 'DYN_ForWrap_Layer' , number = 1 , empty = True );
        dynForWrapLayer.color.set( 9 );
        dupDyn = pm.duplicate( 'DYN_GRP' ), mc.rename( 'DYN_GRP1', 'DYN_ForWrap_GRP' );
        selsForWrap = pm.listRelatives( 'DYN_ForWrap_GRP' );
        for selForWrap in selsForWrap:
            pm.editDisplayLayerMembers( dynForWrapLayer, selForWrap );
            selForWrap.rename( selForWrap.name().replace( '_DYN', '_DYN_ForWrap' ));
            if '_GRP' in str( selForWrap ):
                listForWrap = pm.listRelatives( selForWrap );
                for subForWrap in listForWrap:
                    subForWrap.rename(subForWrap.name().replace( '_DYN', '_DYN_ForWrap' ));
                    self.cleanOutputCloth( selsName = subForWrap );
            else:
                self.cleanOutputCloth( selsName = selForWrap );

        pm.group( n = 'WrapNode_GRP' );
        pm.select( clear=True );
        pm.blendShape ( 'DYN_GRP', 'DYN_ForWrap_GRP', origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = 'DYN_ForWrap_BSH' );

        # Output_Layer
        outputLayer = pm.createDisplayLayer( name = 'Geo_Output_Layer' , number = 1 , empty = True );
        outputLayer.color.set( 17 );
        geoOutput = firstName + '_GRP:Geo_Grp';
        pm.editDisplayLayerMembers( outputLayer, geoOutput );

        if pm.radioCollection( 'autoSimSwitch', q = True, sl = True ) == 'autoSimSwitchOn':
        
            for selDyn in selsForWrap:
                if '_GRP' in str( selDyn ):
                    dynGrp = selDyn.split('_')[0];
                    getGeoGrp = firstName + '_GRP:' + dynGrp + '_Geo';
                    pm.select(getGeoGrp);
                    dupWrp = pm.duplicate( rr = True );
                    wrpName = dupWrp[0] + '_ForWrap'; 
                    geoWrp = pm.rename(dupWrp[0], wrpName);
                    pm.parent(geoWrp, 'WrapNode_GRP');

                    # Create Hi_Geo in WrapNode_GRP
                    pm.select(geoWrp);
                    dupGeo = pm.duplicate( rr = True );
                    dupGeoHi = dupGeo[0].split('_Geo_ForWrap')[0] + '_Hi_Geo_ForWrap'; 
                    geoWrpHi = pm.rename(dupGeo[0], dupGeoHi);
                    pm.polySmooth(dv = 1, c = 1, kb = 1, khe = 0, kt = 1, peh = 0, ch = 1);
                    pm.delete ( geoWrpHi , ch = True );

                    # Create Hi_DYN and wrap dynHi with dayLow
                    listsDynLow = pm.listRelatives( selDyn );
                    pm.select(selDyn);
                    dupForWrapGrp = pm.duplicate( rr = True );
                    dynForWrapGrp = dupForWrapGrp[0].rename( dupForWrapGrp[0].name().replace( '_DYN_ForWrap_GRP1', '_Hi_DYN_ForWrap_GRP' ));
                    listsDynHi = pm.listRelatives( dynForWrapGrp );
                    for subDynHi, subDynLow in zip(listsDynHi, listsDynLow):
                        dupDynWrpHi = subDynHi.rename( subDynHi.name().replace( '_DYN_ForWrap', '_Hi_DYN_ForWrap' ));
                        pm.select(dupDynWrpHi);
                        pm.polySmooth(dv = 1, c = 1, kb = 1, khe = 0, kt = 1, peh = 0, ch = 1);
                        pm.delete(dupDynWrpHi , ch = True);
                        pm.select(subDynHi, r = True);
                        pm.select(subDynLow, add = True);
                        mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');
                else:
                    dynGrp = selDyn.split('_')[0];
                    getGeoGrp = firstName + '_GRP:' + dynGrp + '_Geo';
                    pm.select(getGeoGrp);
                    dupWrp = pm.duplicate( rr = True );
                    wrpName = dupWrp[0] + '_ForWrap'; 
                    geoWrp = pm.rename(dupWrp[0], wrpName)
                    pm.parent(geoWrp, 'WrapNode_GRP');

                    # Create Hi_Geo in WrapNode_GRP
                    pm.select(geoWrp);
                    dupGeo = pm.duplicate( rr = True );
                    dupGeoHi = dupGeo[0].split('_Geo_ForWrap')[0] + '_Hi_Geo_ForWrap'; 
                    geoWrpHi = pm.rename(dupGeo[0], dupGeoHi);
                    pm.polySmooth(dv = 1, c = 1, kb = 1, khe = 0, kt = 1, peh = 0, ch = 1);
                    pm.delete ( geoWrpHi , ch = True );

                    # Create Hi_DYN and wrap dynHi with dayLow
                    pm.select(selDyn);
                    dupDyn = pm.duplicate( rr = True );
                    dupDynWrpHi = dupDyn[0].split('_DYN_ForWrap')[0] + '_Hi_DYN_ForWrap';
                    dynWrpHi = pm.rename(dupDyn[0], dupDynWrpHi);
                    pm.polySmooth(dv = 1, c = 1, kb = 1, khe = 0, kt = 1, peh = 0, ch = 1);
                    pm.delete(dupDynWrpHi , ch = True) ;
                    pm.select(dupDynWrpHi, r = True);
                    pm.select(selDyn, add = True);
                    mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');

            # Create wrap Hi_Geo with Hi_DYN and blendShape to geo output
            selsForWrap = pm.listRelatives( 'DYN_ForWrap_GRP' );
            listsDynHi = [];
            for dynHi in selsForWrap:
                if '_Hi_' in str(dynHi):
                    listsDynHi.append(dynHi); 

            for wrpHi in listsDynHi:
                if '_GRP' in str(wrpHi):
                    hiGeoName = wrpHi.split('_DYN_ForWrap_GRP')[0] + '_Geo_ForWrap';
                    pm.select(hiGeoName, r = True);
                    pm.select(wrpHi, add = True);
                    mel.eval('doWrapArgList "7" { "1","0","1", "2", "0", "1", "0", "0" }');
                else:
                    hiGeoName = wrpHi.split('_DYN_ForWrap')[0] + '_Geo_ForWrap';
                    pm.select(hiGeoName, r = True);
                    pm.select(wrpHi, add = True);
                    mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');

                dtmName = hiGeoName + 'DTM';
                pm.select(hiGeoName);
                pm.deltaMush(smoothingIterations = 10, smoothingStep = 0.5, pinBorderVertices = 1, envelope = 1, n = dtmName);
                geoForWrap = hiGeoName.split('_')[0] + '_Geo_ForWrap';
                pm.select(geoForWrap, r = True);
                pm.select(hiGeoName, add = True);
                mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');
                geoOutput = firstName + '_GRP:' + hiGeoName.split('_')[0] + '_Geo';
                nameBsh = geoForWrap + '_BSH';
                pm.blendShape ( geoForWrap, geoOutput, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = nameBsh );

            pm.select( clear=True );
            pm.group( n = 'DYN_ForWrapBase_GRP' );
            pm.select( clear=True );
            pm.group( n = 'WrapNodeBase_GRP' );
            pm.select( clear=True );

            # WrapBase
            self.module.parentForWrapBase( getGrp = 'DYN_ForWrap_GRP', wrapNodeGrp = 'DYN_ForWrapBase_GRP' );
            self.module.parentForWrapBase( getGrp = 'WrapNode_GRP', wrapNodeGrp = 'WrapNodeBase_GRP' );
            pm.parent('DYN_ForWrapBase_GRP', 'DYN_ForWrap_GRP');
            pm.parent('WrapNodeBase_GRP', 'WrapNode_GRP');

            # WRP_Layer
            wrpLayer = pm.createDisplayLayer( name = 'WRP_Layer' , number = 1 , empty = True );
            wrpLayer.color.set( 17 );
            for selGeo in selsForWrap:
                dynGrp = selGeo.split('_')[0];
                getGeoGrp = firstName + '_GRP:' + dynGrp + '_Geo';
                pm.editDisplayLayerMembers( wrpLayer, getGeoGrp );

        if pm.radioCollection( 'autoSimSwitch', q = True, sl = True ) == 'autoSimSwitchOff':
            wrpLayer = pm.createDisplayLayer( name = 'WRP_Layer' , number = 1 , empty = True );
            wrpLayer.color.set( 17 );

        pm.select( 'WrapNode_GRP', r = True );
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );
        pm.select( clear=True );
        pm.parent( 'DYN_ForWrap_GRP', 'WrapNode_GRP', 'WRP_GRP');
        target = pm.general.PyNode( 'DYN_ForWrap_GRP' );
        target.useOutlinerColor.set( 0 );
        pm.parent( firstName + '_LOC', 'SIM_GRP');
        pm.reorder( firstName + '_LOC', r = -4 );
        pm.parent( 'NoTouch_GRP', 'SIM_GRP');
        pm.reorder( 'NoTouch_GRP', r = -5 );
        self.module.nucleusTrack();

        # outliner
        geoGrp = firstName + '_GRP:Geo_Grp';
        namesGrp = geoGrp, 'WRP_GRP';
        colorCodes = [( 1 , 1 , 0 ), ( 1 , 1 , 0 )];
        
        for colorCode, nameGrp in zip(colorCodes, namesGrp):
            target = pm.general.PyNode( nameGrp );
            target.useOutlinerColor.set( 1 );       
            target.outlinerColor.set( colorCode );