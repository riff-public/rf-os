import pymel.core as pm;
import maya.cmds as mc;
import maya.mel as mel;
import os, ast, random;

mp = os.path.realpath(__file__);
fileName = os.path.basename(__file__);
mp = mp.replace( fileName , '' );
mp = mp.replace( '\\' , '/' );

class MainModule( object ):

    def browseCharacterPath( self,*args ):
        startPath = 'P:/Bikey/asset/publ/char';
        try :
            browsePath = mc.fileDialog2( ds = 2 , fm = 3 , startingDirectory = startPath , okc = 'Select Path' )[0];
            pm.textField( 'inputChaPath' , e = True , text = browsePath );
        except :
            pm.warning( 'No Path Selected' );

    def createFolderTxt( self,*args ):
        myDocuments = os.path.expanduser('~');          
        myDocuments = myDocuments.replace('\\', '/');
        prefPath = myDocuments + '/juneScriptData';
        if os.path.exists( prefPath ) == False:
            os.makedirs( prefPath );
        else : pass;
        return prefPath;

    def readInputMainName( self, *args ):
        prefPath = self.createFolderTxt();
        geoTxt = open( prefPath + '/inputMainName.txt', 'r' );
        geoNameTxt = geoTxt.read();
        geoTxt.close();
        return geoNameTxt;

    def getCharacterName( self,*args ):
        chaPath = pm.textField( 'inputChaPath' , q = True , text = True );
        if chaPath != '':
            chaName = chaPath.split('/')[-1:][0];
        else:
            pm.warning('Character Path Not Found');
        return chaName;

    def createMainNurbs( self,*args ):
        geoDict = {};
        sels = mc.ls( sl = True );
        getGeo = sels[0].split('.');

        for sel in sels:
            pm.polyToCurve(sel, f = 2, dg = 1);
        pm.select( 'polyToCurve2', r=True );
        pm.select( 'polyToCurve1', add=True );
        pm.loft(ch = True, u = True, c = False, ar = True, d = 1, ss = 1, rn = False, po = 0, rsn = True, n = 'MainNurbs_ForWrap');

        # inputMainName
        geoName = getGeo[0];
        chaName = self.getCharacterName();
        cinName = chaName[0][:1].capitalize() + chaName[1:] + '_CIN:Geo_Grp';
        colName = chaName[0][:1].capitalize() + chaName[1:] + '_COL:Geo_Grp';
        grpName = chaName[0][:1].capitalize() + chaName[1:] + '_GRP:Geo_Grp';
        colSP = colName.split(':')[0];
        grpSP = grpName.split(':')[0];
        listsSP = pm.namespaceInfo( listNamespace=True );
        for lists in listsSP:
            if colSP == lists:
                nameGrp = colName; 
            elif grpSP == lists:
                nameGrp = grpName;

        geoDict[('MainName')] = str(geoName), str(cinName), str(nameGrp);
        prefPath = self.createFolderTxt();
        inputMainTxt = open( prefPath + '/inputMainName.txt' , 'w+' );
        inputMainTxt.write( str(geoDict) );
        inputMainTxt.close();

        # MainNurbs_ForWrap
        pm.select( 'MainNurbs_ForWrap', r=True );
        pm.delete( 'polyToCurve1', 'polyToCurve2' );
        pm.select( getGeo[0], add=True );
        mel.eval('doWrapArgList "7" { "1","0","1", "2", "1", "1", "0", "0" }');
        pm.select( 'MainNurbs_ForWrap', r=True );
        pm.select( 'wrap1', addFirst=True );
        pm.rename('wrap1', 'MainNurbs_ForWrapNode');

        pm.select( 'MainNurbs_ForWrap', r=True );
        mel.eval('createHair 1 1 2 0 0 0 0 0 0 2 1 1');
        pm.select( 'hairSystem1', r=True );
        pm.select( 'hairSystem1OutputCurves', add=True );
        pm.select( 'nucleus1', add=True );
        pm.select( 'curve1', add=True );
        pm.delete();

        getText = self.readInputMainName();
        nameDict = ast.literal_eval(getText);
        geoBaseName = nameDict[('MainName')][0] + 'Base';

        pm.group( 'MainNurbs_ForWrap', geoBaseName, n='NoTouch_GRP' );
        pm.rename( 'MainNurbs_ForWrapFollicle5050', 'MainNurbs_Follicle');
        pm.parent( 'MainNurbs_Follicle', 'NoTouch_GRP' );
        pm.select( 'hairSystem1Follicles', r=True );
        pm.delete();
        pm.select( clear=True );

    def creatLoc( self, *args ):
        getText = self.readInputMainName();
        nameDict = ast.literal_eval(getText);
        splitText = nameDict[('MainName')][0].split('CIN:');
        geoLocName = splitText[0] + 'LOC';

        pm.spaceLocator( n = geoLocName );
        geoLocShapeName = splitText[0] + 'LOCShape';
        pm.select( geoLocShapeName, r = True );
        mel.eval('toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`');
        
        # constraint with follicle
        pm.pointConstraint( 'MainNurbs_Follicle', geoLocName, mo = False );
        pm.select( geoLocName + '_pointConstraint1', r = True ),pm.delete();
        pm.select( geoLocName, r = True );
        pm.makeIdentity( a = True, t = 1, r = 1, s = 1, n = 0, pn = 1);
        pm.parentConstraint( 'MainNurbs_Follicle', geoLocName, mo = True, w = 1.0 );
        # cha_LOC
        getGrp = nameDict[('MainName')][2];
        pm.parent( getGrp, geoLocName );
        # reorder
        constraintName = geoLocName + '_' + 'parentConstraint1';
        pm.reorder( constraintName, r = 2 );
        pm.select( clear=True );

    def copyCurveShape(self, *args):  
        from rf_maya.rftool.sim.tools.simtools.hairSetup.copyCurveShape import gui;
        reload(gui);
        from rf_maya.rftool.sim.tools.simtools.hairSetup.copyCurveShape import util;
        reload(util);
        class CopyCurveShape ( gui.Gui , util.Util ) :
            def __init__ ( self ) :
                super ( CopyCurveShape , self ).__init__ ( ) ;
        copyCurveShape = CopyCurveShape() ;
        copyCurveShape.callWindow() ;

    def hairQC(self, *args):    
        import pymel.core as pm;
        from rf_utils.pipeline import check;
        reload(check);
        sels = pm.selected();
        curr_grp = sels[0].nodeName();
        abc_grp = sels[1].nodeName().split(':')[-1];
        abc_path = str(pm.referenceQuery(sels[1], f=True));
        if not abc_path.endswith('.abc'):
            print 'Only work with .abc reference';
        else:
            checker = check.CustomCheck(curr_grp, abc_grp, abc_path);
            res = checker.check();

    def assignMaterialPerObject( self, *args ):
        random.seed(0);
        sel = mc.ls(sl=True, l=True);
        for obj in sel:
            if mc.listRelatives(obj, s=True): 
                shd = mc.shadingNode('lambert', asShader=True, n='%s_lmb' % obj);
                sg = mc.sets(n='%s_sg' % obj, renderable=True, noSurfaceShader=True, empty=True);
                r = [random.random() for i in range(3)];
                mc.setAttr('%s.color' % shd, r[0], r[1], r[0], type='double3');
                mc.connectAttr('%s.outColor' % shd, '%s.surfaceShader' % sg, f=True);
                mc.sets(obj, e=True, fe=sg);
        mel.eval('hyperShadePanelMenuCommand("hyperShadePanel1", "deleteUnusedNodes");');

    def lockAttr( self, *args ):
        sels = pm.ls(sl = True);
        for sel in sels:
            attrArray = ['tx','ty','tz','rx','ry','rz','sx','sy','sz'];
            for a in attrArray:
                pm.setAttr(sel + '.' + a, l=True);

    def lockAndHideAttr( self, *args ):
        sels = pm.ls(sl = True);
        for sel in sels:
            attrArray = ['tx','ty','tz','rx','ry','rz','sx','sy','sz', 'v'];
            for a in attrArray:
                pm.setAttr(sel + '.' + a, k=False, l=True);

    def nucleusTrack( self, *args ):
        getName = pm.textField( 'inputFollicleName' , q = True , text = True );
        if pm.objExists(getName):
            follicleName = getName;
        else:
            follicleName = 'MainNurbs_Follicle';

        oriName = 'ORI_LOC';
        pm.spaceLocator( n = oriName ), self.lockAttr();
        pm.parent( oriName, 'NoTouch_GRP'), pm.select( cl=True );

        grpName = ['TXZ_GRP', 'TY_GRP', 'RXYZ_GRP', 'NucleusTrack_GRP'];

        # constraint
        for grp in grpName :
            pm.group( n = grp);
        pm.parent('nucleus1', 'TXZ_GRP');
        pm.pointConstraint( follicleName, 'RXYZ_GRP', mo = False);
        pm.select( 'RXYZ_GRP' + '_pointConstraint1', r = True ),pm.delete();
        pm.pointConstraint( follicleName, 'TXZ_GRP', mo = True, sk = "y" );
        pm.pointConstraint( oriName, 'TXZ_GRP', mo = True, sk = "y" );
        pm.pointConstraint( follicleName, 'TY_GRP', mo = True, sk = ["x","z"]);
        pm.pointConstraint( oriName, 'TY_GRP', mo = True, sk = ["x","z"] );
        pm.parentConstraint( follicleName, 'RXYZ_GRP', mo = True, st = ["x","y","z"]);

        # remapValue
        pm.shadingNode( 'remapValue', au = True, n = 'TXZ_RMV' );
        pm.setAttr('TXZ_RMV.outputMin', 1);
        pm.setAttr('TXZ_RMV.outputMax', 0);
        pm.connectAttr( 'TXZ_GRP_pointConstraint1.' + follicleName +'W0', 'TXZ_RMV.inputValue', f=True );
        pm.connectAttr( 'TXZ_RMV.outValue', 'TXZ_GRP_pointConstraint1.ORI_LOCW1', f=True );

        pm.shadingNode( 'remapValue', au = True, n = 'TY_RMV' );
        pm.setAttr('TY_RMV.outputMin', 1);
        pm.setAttr('TY_RMV.outputMax', 0);
        pm.connectAttr( 'TY_GRP_pointConstraint1.' + follicleName +'W0', 'TY_RMV.inputValue', f=True );
        pm.connectAttr( 'TY_RMV.outValue', 'TY_GRP_pointConstraint1.ORI_LOCW1', f=True );

        # setZeroConstraint
        conName = [ 'TXZ_GRP_pointConstraint1', 'TY_GRP_pointConstraint1' ];
        for name in conName :
            conOffset = [ '.offsetZ', '.offsetX', '.offsetY' ];
            for offset in conOffset:
                pm.setAttr( name + offset, 0 );

        #setAtttr
        pm.select( 'NucleusTrack_GRP' );
        self.lockAndHideAttr();
        sels= pm.ls( sl=True );
        for sel in sels:
            grpName = ['TXZ_GRP', 'TY_GRP', 'RXYZ_GRP' ];
            for grp in grpName:
                pm.addAttr( sel, ln = grp, at ='double', min = 0, max = 1, dv = 1 );
                pm.setAttr( sel +'.' + grp, e =True, k = True );
        pm.connectAttr( 'NucleusTrack_GRP.TXZ_GRP', 'TXZ_GRP_pointConstraint1.' + follicleName + 'W0', f=True );
        pm.connectAttr( 'NucleusTrack_GRP.TY_GRP', 'TY_GRP_pointConstraint1.' + follicleName + 'W0', f=True );
        pm.connectAttr( 'NucleusTrack_GRP.RXYZ_GRP', 'RXYZ_GRP_parentConstraint1.' + follicleName + 'W0', f=True );

        # grpColor
        pm.parent('NucleusTrack_GRP', 'SIM_GRP');
        pm.select( 'NucleusTrack_GRP' );
        target = pm.general.PyNode( 'NucleusTrack_GRP' );
        target.useOutlinerColor.set( 1 );       
        target.outlinerColor.set( ( 0.517647, 0.439216, 1 ) );

    def renameOutputcloth( self, *args ):
        sels = pm.ls(sl=True);
        for sel in sels:
            getName = pm.listRelatives(sel);
            for name in getName:
                name.rename(name.name().replace('_outputCloth', 'Shape'));

    def deleteOrig ( self, target = '' , *args ) :
        shapes = pm.listRelatives ( target , shapes = True ) ;
        for shape in shapes :
            if 'Orig' in str ( shape ) :
                pm.delete ( shape ) ;
            else : pass ;

    def parentForWrapBase( self, getGrp, wrapNodeGrp, *args ):
        selsForWrap = mc.ls(getGrp, dag=True, allPaths=True, tr=True)
        for nodeWrp in selsForWrap:
            if 'Base' in nodeWrp:
                pm.parent(nodeWrp, wrapNodeGrp);

    def parentWrapGeoBase( self, wrapNodeGrp, *args ):
        getText = self.readInputMainName();
        nameDict = ast.literal_eval(getText);
        getGrpCin = nameDict[('MainName')][1];
        hierarchy = mc.ls(getGrpCin, dag=True, allPaths=True, tr=True)
        for nodeWrp in hierarchy:
            if 'GeoBase' in nodeWrp:
                pm.parent(nodeWrp, wrapNodeGrp);