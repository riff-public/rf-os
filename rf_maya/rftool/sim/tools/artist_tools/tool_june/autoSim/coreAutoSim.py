import os ;
import pymel.core as pm ;
import maya.cmds as mc ;

core = '%s/core'%os.environ.get('RFSCRIPT');
imageAutoSim = core + '/rf_maya/rftool/sim/tools/artist_tools/tool_june/autoSim/imageAutoSimUI';

def nObjectLists( *args ):
    from rf_maya.rftool.sim.tools.artist_tools.tool_june.autoSim import autoSimLocal
    reload(autoSimLocal);
    autoSimLocal.AutoSim().nObjectLists();

def nClothListSimulate( *args ):
    from rf_maya.rftool.sim.tools.artist_tools.tool_june.autoSim import autoSimLocal
    reload(autoSimLocal);
    autoSimLocal.AutoSim().nClothListSimulate();

def nHairListSimulate( *args ):
    from rf_maya.rftool.sim.tools.artist_tools.tool_june.autoSim import autoSimLocal
    reload(autoSimLocal);
    autoSimLocal.AutoSim().nHairListSimulate();

def importCacheData( *args ):
    from rf_maya.rftool.sim.tools.artist_tools.tool_june.autoSim import autoSimLocal
    reload(autoSimLocal);
    autoSimLocal.AutoSim().importCacheData();

def autoSimUI( *args ):
    if pm.window( 'autoSimUI' , exists = True ):
        pm.deleteUI( 'autoSimUI' );

    window = pm.window( 'autoSimUI' , 
        title     = '{title} {version}'.format( title = 'Auto Sim Local', version = 1.0 ),
        mnb       = True ,
        mxb       = False,
        sizeable  = False,
        rtf       = True,
        bgc=[0.15, 0.15, 0.15]
        );

    pm.window( window , e = True , w = 270, h = 220 );

    with window:
        pm.columnLayout('columnLayoutAutoSim', adjustableColumn = True);

    with pm.rowColumnLayout( h = 5, w = 270, nc = 1, columnWidth = ( 1, 270 )):
        pm.separator( h = 5, st = 'in' );

    with pm.rowColumnLayout( w = 270, nc = 1, columnWidth = ( 1, 270 )):
        pm.textScrollList( 'nObjectLists', h = 150, numberOfRows = 150, allowMultiSelection = True, parent = 'columnLayoutAutoSim' , dcc = nObjectLists );

    with pm.rowColumnLayout( h = 5, w = 270, nc = 1, columnWidth = ( 1, 270 )):
        pm.separator( h = 5, st = 'in' );

    with pm.rowColumnLayout( h = 15,w = 270, nc = 3, columnWidth = [( 1, 90 ), ( 2, 90 ), ( 3, 90 )]):
        pm.text( label = 'Import Cache Data', fn = 'smallPlainLabelFont');
        pm.text( label = 'Cloth Simulate', fn = 'smallPlainLabelFont');
        pm.text( label = 'Hair Simulate', fn = 'smallPlainLabelFont');

    with pm.rowColumnLayout( w = 270, nc = 3, columnWidth = [( 1, 90 ), ( 2, 90 ), ( 3, 90 )]):
        pm.symbolButton( c = importCacheData, image = imageAutoSim + '/import.png' );
        pm.symbolButton( c = nClothListSimulate, image = imageAutoSim + '/dress.png' );
        pm.symbolButton( c = nHairListSimulate, image = imageAutoSim + '/woman.png' );

    window.show();
    nObjectLists();
