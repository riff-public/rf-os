import pymel.core as pm;
import maya.cmds as mc;
import maya.mel as mel;
import os, ast;

mp = os.path.realpath(__file__);
fileName = os.path.basename(__file__);
mp = mp.replace( fileName , '' );
mp = mp.replace( '\\' , '/' );

class AutoSim( object ):

    def __init__ ( self ):
        self.currentProjectPath = pm.workspace( q = True , rootDirectory = True );
        self.dataProjectPath = self.currentProjectPath + 'data';
        self.shotCachePath = self.currentProjectPath + 'data/shotCache/hero';
        self.camCachePath = self.currentProjectPath + 'data/cam';

    def cachePath( self,*args ):
        listItems = os.listdir(self.dataProjectPath);
        techCache = [];
        anim = [];
        cam = [];
        sim = [];
        for itemAnim in listItems:
            if 'techCache' in itemAnim:
                techCache.append(itemAnim);
            if 'anim' in itemAnim:
                anim.append(itemAnim);
            if 'cam' in itemAnim:
                cam.append(itemAnim);
            if 'sim' in itemAnim:
                sim.append(itemAnim);
        return [techCache,anim,cam,sim];

    def showNone( self,*args ):
        mel.eval('modelEditor -e -allObjects 0 modelPanel4;');
        mel.eval('modelEditor -e -twoSidedLighting true modelPanel4;');
        mel.eval( 'setRendererInModelPanel $gLegacyViewport modelPanel4;' );
        # turn off yeti display
        try:
            pm.setAttr( '*_YetiNodeShape.displayOutput' , 0 );
        except:
            pass;

    def getChaName( self,*args ):
        listNamespace = pm.namespaceInfo( lon=True );
        for chaName in listNamespace:
            if 'CIN' in chaName:
                getCha = chaName.split('_')[0];
        return getCha;

    def importAnimCache( self,*args ):
        [techCache,anim,cam,sim] = self.cachePath();
        if techCache:
            dataAnimPath = self.dataProjectPath + '/' + techCache[0];
        elif anim:
            dataAnimPath = self.dataProjectPath + '/' + anim[0];
        pm.system.importFile( dataAnimPath , namespace = 'ABC' );
        listNamespace = pm.namespaceInfo( lon=True );

        cinGrp = [];
        animGrp = [];
        for namespace in listNamespace:
            if 'CIN' in namespace:
                cinGrp = namespace + ':Geo_Grp';
            if 'ABC' in namespace:
                animGrp = namespace + ':Geo_Grp';
        pm.blendShape ( animGrp, cinGrp, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = 'ABC_CIN_BSH' );
        pm.select( animGrp, r = True );
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );
        pm.select( clear=True );

    def importSimCache( self,*args ):
        [techCache,anim,cam,sim] = self.cachePath();
        if techCache:
            dataAnimPath = self.dataProjectPath + '/' + techCache[0];
        elif anim:
            dataAnimPath = self.dataProjectPath + '/' + anim[0];
        getPath = self.currentProjectPath;
        getName = getPath.split('/');
        cacheName = getName[3] + '_sim_' + getName[4] + '.hero.abc'; 
        workPath = 'P:/'+ getName[1] +'/scene/work/' +  getName[2] + '/' + getName[3] + '/sim/' + getName[4] + '/cache/sim/' + '\\' + cacheName;

        if os.path.exists(workPath):
            cacheGeoPath = workPath;
        else:
            cacheGeoPath = dataAnimPath

        pm.system.importFile( cacheGeoPath , namespace = 'ABC' );
        listNamespace = pm.namespaceInfo( lon=True );

        cinGrp = [];
        cacheGeoGrp = [];
        for namespace in listNamespace:
            if 'CIN' in namespace:
                cinGrp = namespace + ':Geo_Grp';
            if 'ABC' in namespace:
                cacheGeoGrp = namespace + ':Geo_Grp';
        pm.blendShape ( cacheGeoGrp, cinGrp, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = 'ABC_CIN_BSH' );
        pm.select( cacheGeoGrp, r = True );
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );
        pm.select( clear=True );

    def importSimCacheLocal( self,*args ):
        [techCache,anim,cam,sim] = self.cachePath();
        if techCache:
            dataAnimPath = self.dataProjectPath + '/' + techCache[0];
        elif anim:
            dataAnimPath = self.dataProjectPath + '/' + anim[0];
        getCha = self.getChaName();
        getPath = self.currentProjectPath;
        getName = getPath.split('/');
        cacheName = getName[3] + '_sim_' + getName[4] + '.hero.abc'; 
        cachePath = getPath + '/output/Geo/' + getCha + '_GRP/hero' + '\\' + cacheName;

        if os.path.exists(cachePath):
            cacheGeoPath = cachePath;
        else:
            cacheGeoPath = dataAnimPath

        pm.system.importFile( cacheGeoPath , namespace = 'ABC' );

        listNamespace = pm.namespaceInfo( lon=True );
        cinGrp = [];
        cacheGeoGrp = [];
        for namespace in listNamespace:
            if 'CIN' in namespace:
                cinGrp = namespace + ':Geo_Grp';
            if 'ABC' in namespace:
                cacheGeoGrp = namespace + ':Geo_Grp';
        pm.blendShape ( cacheGeoGrp, cinGrp, origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = 'ABC_CIN_BSH' );
        pm.select( cacheGeoGrp, r = True );
        mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );
        pm.select( clear=True );

    def setTimeRange( self,*args ):
        from rftool.preload import duration_check;
        from rf_utils.sg import sg_utils;
        from rf_utils.context import context_info;
        
        project = self.currentProjectPath.split('/')[1];
        entity  = self.currentProjectPath.split('/')[3];

        context = context_info.Context();
        context.use_sg(sg_utils.sg, project, 'scene', entityName= entity);
        scene = context_info.ContextPathInfo(context=context);
        sgDuration = duration_check.get_duration(scene);
        start = scene.projectInfo.scene.start_frame;
        end = start + (sgDuration-1);
        startSim = start - 30;
        endSim = end + 10;
        startCache = start - 5;
        endCache = end + 5;

        pm.playbackOptions( min = startSim , max = endSim );

        return [ startSim , start , end , endSim , startCache , endCache ];

    def setStartSim( self, *args ):
        [ startSim , start , end , endSim , startCache , endCache ] = self.setTimeRange();
        if pm.objExists( 'CIN_COL_BSH' ):
            bsn = pm.general.PyNode( 'CIN_COL_BSH' );
            key = pm.listAttr( bsn.w , m = True ) ;
            pm.cutKey( bsn , at = key , option = 'keys' );
            pm.setKeyframe( bsn , at = key , t = startSim , v = 0 );
            pm.setKeyframe( bsn , at = key , t = startSim + 10 , v = 1 );
        elif pm.objExists( 'CIN_GRP_BSH' ):
            bsn = pm.general.PyNode( 'CIN_GRP_BSH' );
            key = pm.listAttr( bsn.w , m = True );
            pm.cutKey( bsn , at = key , option = 'keys' );
            pm.setKeyframe( bsn , at = key , t = startSim , v = 0 );
            pm.setKeyframe( bsn , at = key , t = startSim + 10 , v = 1 );

        nucleus_list = pm.ls ( type = 'nucleus' );
        if nucleus_list:
            for nucleus in nucleus_list:
                nucleus.startFrame.set( startSim );

        pm.currentTime( startSim, edit=True );

    def shiftOrigin( self,*args ):
        from tools.artist_tools.tool_sam.origin import auto_origin;
        reload( auto_origin );
        auto_origin.auto_origin().load_origin();

    def shiftAnim( self,*args ):
        from tools.artist_tools.tool_sam.origin import auto_origin;
        reload( auto_origin );
        auto_origin.auto_origin().load_anim();
    
    def importCamOri( self,*args ):
        [techCache,anim,cam,sim] = self.cachePath();
        dataAnimPath = self.dataProjectPath + '/' + cam[0];
        pm.system.importFile( dataAnimPath , namespace = 'CAM' );     
        bwdGrp = pm.group( em = True , w = True , n = 'BWD_GRP' );
        bwdGrp.t.lock(), bwdGrp.r.lock(), bwdGrp.s.lock();

        listNamespace = pm.namespaceInfo( lon=True );
        cam = [];
        for namespace in listNamespace:
            if 'CAM' in namespace:
                cam = namespace + ':camshot';
        camLock = pm.ls(cam, r=True)[0];
        camLock.t.lock();
        camLock.r.lock();
        camLock.s.lock();
        pm.parent( cam, bwdGrp );

    def nObjectLists( self,*args ):
        pm.textScrollList( 'nObjectLists', e = True , ra = True );
        scenePath = pm.system.sceneName();
        sceneName = scenePath.split('/')[-1:][0];
        listObject = [];
        if 'cloth' in sceneName:
            listOutliner = pm.ls(typ = 'transform');
            if 'SIM_GRP' in listOutliner:
                listSim = pm.listRelatives('SIM_GRP');
                if 'nCloth_GRP' in listSim:
                    listCloth = pm.listRelatives('nCloth_GRP');
                    for cloth in listCloth:
                        listObject.append( cloth );
                else:
                    listObject.append( 'nObjects Not Found' );
            else:
                listObject.append( 'nObjects Not Found' );

        elif 'hair' in sceneName:
            listHair = pm.ls( type = 'hairSystem' );
            if len(listHair) > 0:
                for hair in listHair:
                    listObject.append( hair );
            else:
                listObject.append( 'nObjects Not Found' );

        pm.textScrollList( 'nObjectLists', e = True, a = listObject );

        return listObject

    def disableNcloth( self,*args ):
        dispLayers = mc.ls(type = 'displayLayer');
        for layers in dispLayers:
            if 'DYN_Layer' in layers:
                mel.eval( 'layerEditorSelectObjects %s' % layers );
                objectEnable = layers.split('_')[0] + '_nClothShape.isDynamic';
                pm.setAttr( objectEnable , 0 );

    def disableNucleus( self,*args ):
        listsNucleus = pm.ls( type = 'nucleus' );
        for nucleus in listsNucleus:
            each = pm.general.PyNode( nucleus );
            if each.enable.get() == True:
                each.enable.set( 0 );

    def enableNucleus( self,*args ):
        listsNucleus = pm.ls( type = 'nucleus' );
        for nucleus in listsNucleus:
            each = pm.general.PyNode( nucleus );
            if each.enable.get() == False:
                each.enable.set( 1 );

    def nClothSimulate( self,*args ):
        self.showNone();
        self.enableNucleus();
        self.disableNcloth();
        dispLayers = mc.ls(type = 'displayLayer');
        dynCachePath = self.currentProjectPath + 'cache/nCache';
        for layers in dispLayers:
            if 'DYN_Layer' in layers:
                dynLayer = mel.eval( 'layerEditorSelectObjects %s' % layers );
                pm.select(dynLayer, r = True);
                objectEnable = layers.split('_')[0] + '_nClothShape.isDynamic';
                pm.setAttr( objectEnable , 1 );

                nCacheName = layers.split('_')[0] + '_nClothShape';
                createNcache = 'doCreateNclothCache 5 { "2", "1", "10", "OneFile", "1", ' + '"'+str(dynCachePath)+'","0",' + '"'+str(nCacheName)+'","0", "add", "0", "1", "1","0","1","mcc" } ;';
                mel.eval( createNcache );
        self.disableNucleus();

    def nClothListSimulate( self,*args ):
        self.showNone();
        self.enableNucleus();
        self.disableNcloth();
        dynCachePath = self.currentProjectPath + 'cache/nCache';
        listsObject = pm.textScrollList( 'nObjectLists',  q = True, selectItem = True );
        for nCloth in listsObject:
            pm.select(nCloth, r = True);
            objectEnable = nCloth.split('_')[0] + '_nClothShape.isDynamic';
            pm.setAttr( objectEnable , 1 );

            nCacheName = nCloth.split('_')[0] + '_nClothShape';
            createNcache = 'doCreateNclothCache 5 { "2", "1", "10", "OneFile", "1", ' + '"'+str(dynCachePath)+'","0",' + '"'+str(nCacheName)+'","0", "add", "0", "1", "1","0","1","mcc" } ;';
            mel.eval( createNcache );
        self.disableNucleus();
        self.setPlayblast();

        # turn off Geo_ForWrap blendShape
        lists = self.nObjectLists();
        for geoName  in lists:
            if geoName not in listsObject:
                forWrapBsh = geoName.split('_')[0] + '_Geo_ForWrap_BSH'
                pm.select( forWrapBsh, addFirst=True );
                pm.setAttr(forWrapBsh + '.envelope', 0);
            elif geoName in listsObject:
                forWrapBsh = geoName.split('_')[0] + '_Geo_ForWrap_BSH'
                pm.select( forWrapBsh, addFirst=True );
                pm.setAttr(forWrapBsh + '.envelope', 1);

    def exportClothHero( self,*args ):
        [ startSim , start , end , endSim , startCache , endCache ] = self.setTimeRange();
        getPath = self.currentProjectPath;
        getName = getPath.split('/');
        geoGrp = pm.ls('*_GRP:Geo_Grp');
        workPath = 'P:/'+ getName[1] +'/scene/work/' +  getName[2] + '/' + getName[3] + '/sim/' + getName[4] + '/cache/sim/';

        cacheGeoPath = workPath
        if not os.path.exists ( cacheGeoPath ):
            os.makedirs ( cacheGeoPath );
        
        cacheName = cacheGeoPath + '\\' + getName[3] + '_sim_' + getName[4] + '.hero.abc';
        export = 'AbcExport -j "-frameRange '+str(startCache)+' '+str(endCache)+' -uvWrite -worldSpace -writeVisibility -dataFormat ogawa -root '+geoGrp[0]+' -file '+cacheName+'"';
        mel.eval(export);

    def nHairSimulate( self,*args ):
        self.showNone();
        self.enableNucleus();
        dynCachePath = self.currentProjectPath + 'cache/nCache';
        hairSystemActive = [];
        hairSystemLists = pm.ls('*_HairSystem', type='transform'); 
        for hairSystem in hairSystemLists: 
            hairSystemName = hairSystem.nodeName();
            sels = pm.select(hairSystemName, add=True);
            objectEnable = hairSystemName + 'Shape.active';
            hairSystemActive.append( objectEnable );
        for active in hairSystemActive:
            pm.setAttr( active , 1 );

        nCacheName = 'HairSystemShape';
        createNcache = 'doCreateNclothCache 5 { "2", "1", "10", "OneFile", "1", ' + '"'+str(dynCachePath)+'","0",' + '"'+str(nCacheName)+'","0", "add", "0", "1", "1","0","1","mcc" } ;';
        mel.eval( createNcache );
        self.disableNucleus();

        # turn on yeti display
        try:
            pm.setAttr( '*_YetiNodeShape.displayOutput' , 1 );
        except:
            pass; 
        self.setPlayblast();

    def nHairListSimulate( self,*args ):
        self.showNone();
        dynCachePath = self.currentProjectPath + 'cache/nCache';
        hairSystemActive = [];
        listsObject = pm.textScrollList( 'nObjectLists',  q = True, selectItem = True );
        for nHair in listsObject:
            sels = pm.select(nHair, add=True);
            objectEnable = nHair + '.active';
            hairSystemActive.append( objectEnable );
        # enable nHair
        for active in hairSystemActive:
            pm.setAttr( active , 1 );

        nCacheName = 'HairSystemShape';
        createNcache = 'doCreateNclothCache 5 { "2", "1", "10", "OneFile", "1", ' + '"'+str(dynCachePath)+'","0",' + '"'+str(nCacheName)+'","0", "add", "0", "1", "1","0","1","mcc" } ;';
        mel.eval( createNcache );
        self.disableNucleus();

        # turn on yeti display
        try:
            pm.setAttr( '*_YetiNodeShape.displayOutput' , 1 );
        except:
            pass; 
        self.setPlayblast();

    def exportTechHero( self,*args ):
        [ startSim , start , end , endSim , startCache , endCache ] = self.setTimeRange();
        getPath = self.currentProjectPath;
        getName = getPath.split('/');
        techGrp = ('TechGeo_Grp');
        workPath = 'P:/'+ getName[1] +'/scene/work/' +  getName[2] + '/' + getName[3] + '/sim/' + getName[4] + '/cache/sim/';

        cacheTechPath = workPath
        if not os.path.exists ( cacheTechPath ):
            os.makedirs ( cacheTechPath );
        
        cacheName = cacheTechPath + '\\' + getName[3] + '_sim_' + getName[4] + '_tech.hero.abc';
        export = 'AbcExport -j "-frameRange '+str(startCache)+' '+str(endCache)+' -uvWrite -worldSpace -writeVisibility -dataFormat ogawa -root '+techGrp+' -file '+cacheName+'"';
        mel.eval(export);

    def setPlayblast( self,*args ):
        # show output cloth and output hair yeti
        mel.eval('modelEditor -e -polymeshes true modelPanel4;modelEditor -e -hos true modelPanel4;');
        mel.eval('modelEditor -e -pluginShapes true modelPanel4;');
        # try:
        #     pm.select( '*_YetiNode', r = True );
        #     mel.eval( 'toggleVisibilityAndKeepSelection `optionVar -query toggleVisibilityAndKeepSelectionBehaviour`;' );
        # except:
        #     pass;
        mel.eval( 'ActivateViewport20;' );
        listNamespace = pm.namespaceInfo( lon=True );
        for camName in listNamespace:
            if 'CAM' in camName:
                mel.eval('lookThroughModelPanel CAM:camshotShape modelPanel4;');
            else:
                pass;
        pm.select( cl = True );

    # def runAutoSim_cloth( self,*args ):
    #     self.importAnimCache();
    #     self.setTimeRange();
    #     self.setStartSim();
    #     self.importCamOri();
    #     self.shiftOrigin();
    #     self.nClothSimulate();
    #     self.exportClothHero();
    #     self.setPlayblast();

    # def runAutoSim_hair( self,*args ):
    #     self.importSimCache();
    #     self.setTimeRange();
    #     self.setStartSim();
    #     self.importCamOri();
    #     self.shiftOrigin();
    #     self.nHairSimulate();
    #     self.exportTechHero();
    #     self.setPlayblast();

    def importCacheData( self,*args ):
        scenePath = pm.system.sceneName();
        sceneName = scenePath.split('/')[-1:][0];
        if 'cloth' in sceneName:
            self.importAnimCache();
            self.setTimeRange();
            self.setStartSim();
            self.importCamOri();
            self.shiftOrigin();

        elif 'hair' in sceneName:
            self.importSimCacheLocal();
            self.setTimeRange();
            self.setStartSim();
            self.importCamOri();
            self.shiftOrigin();
 