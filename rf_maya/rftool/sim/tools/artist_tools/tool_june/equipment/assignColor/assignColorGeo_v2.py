import pymel.core as pm;
import maya.mel as mel;
import os, sys;
import ast;

selfPath = os.path.realpath (__file__) ;
fileName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( fileName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

class AssignColor( object ):

    def createFolderTxt( self, *args ):
        myDocuments = os.path.expanduser('~');          
        myDocuments = myDocuments.replace('\\', '/');
        colorPath = myDocuments + '/juneScriptData/assignColor';
        if os.path.exists( colorPath ) == False:
            os.makedirs( colorPath );
        else : pass;

        return colorPath;
        
    def getGeoGrpName( self, *args ):
        geoDict = {};
        colorPath = self.createFolderTxt();
        fileName = pm.textField('CharacterTF', q = True, tx = True);
        newFileName = fileName.split(':')
        getGeoTxt = open( colorPath + '/' + newFileName[0] +'.txt' , 'w+' );

        sels = pm.ls( sl = True )[0];
        nodeName = sels.listRelatives( ad = True , type = 'transform');
        geoLists =[];

        for node in nodeName:
            if node.getShape():
                geoLists.append( node );

        shapesInSel =  pm.ls(dag = 1,o = 1,s = 1,sl = 1);
        shadingGrps = pm.listConnections(shapesInSel,type = 'shadingEngine');
        shaders = pm.ls(pm.listConnections(shadingGrps),materials = 1);

        try:
            for geoList, shadingGrp, shade in zip(geoLists, shadingGrps, shaders):
                colorLists = pm.getAttr('%s.color' %shade);
                newGeoList = geoList.split(':');
                geoDict[(str(newGeoList[1]))] = str(shadingGrp), str(shade), str(colorLists);
        except:
            for geoList, shadingGrp, shade in zip(geoLists, shadingGrps, shaders):
                colorLists = pm.getAttr('%s.color' %shade);
                newGeoList = geoList;
                geoDict[(str(newGeoList))] = str(shadingGrp), str(shade), str(colorLists);

        getGeoTxt.write( str(geoDict) );
        getGeoTxt.close();
        self.listGetColor(colorPath);

    def click( self, *args ):
        sels = pm.ls(selection=True);
        if sels == []:
            print 'not selection';
        else :
            pm.textField('CharacterTF', e = True, tx = sels[0]);

    def listGetColor( self, *args ):
        colorPath = self.createFolderTxt();
        fileLists = os.listdir ( colorPath );
        pm.textScrollList( 'previewLists' , e = True , ra = True );

        for fileList in fileLists :
            pm.textScrollList( 'previewLists' , e = True , a = fileList );

    def readFile( self, *args ):
        colorPath = self.createFolderTxt();
        selectedList = pm.textScrollList( 'previewLists' , q = True , si = True )[0];
        if os.path.isfile( colorPath + '/' + str(selectedList) ):
            colorTxt = open( colorPath + '/' + str(selectedList), 'r' );
            colorName = colorTxt.read();
            colorTxt.close();
        else : pass;

        return colorName;

    def assignColor( self, *args ):
        sels = pm.ls(selection=True);
        if sels == []:
            print 'not selection';
        else :
            getTxt = self.readFile();
            txtNames = ast.literal_eval(getTxt);
            sels = pm.ls( sl = True )[0];
            geoLists =[];
            for node in sels.listRelatives( ad = True , type = 'transform') :
                if node.getShape():
                    geoLists.append( node );

            try:
                for geoList in geoLists:
                    newGeoList = geoList.split(':');
                    colorName = txtNames[str(newGeoList[1])][1];

                    colorCode = str(txtNames[str(newGeoList[1])][2]);
                    colorCode = colorCode.replace("(", "").replace(")", "");
                    code = colorCode.split(',');
                    if not pm.objExists(colorName):
                        exec('pm.shadingNode ( "blinn" , asShader = True , n = colorName);');
                        exec('pm.setAttr ( "%s.color" % colorName , (float(code[0]), float(code[1]), float(code[2])) , typ="double3");');
                        exec('pm.setAttr ( "%s.eccentricity" % colorName, 0.6 );');
                        exec('pm.setAttr ("%s.specularRollOff" % colorName, 0.3 );');
                        exec('pm.setAttr ("%s.specularColor" % colorName, (0.15, 0.15, 0.15),typ="double3");');
                    pm.select(geoLists);
                    pm.hyperShade( assign = colorName);
            except:
                for geoList in geoLists:
                    newGeoList = geoList;
                    colorName = txtNames[str(newGeoList)][1];

                    colorCode = str(txtNames[str(newGeoList)][2]);
                    colorCode = colorCode.replace("(", "").replace(")", "");
                    code = colorCode.split(',');
                    if not pm.objExists(colorName):
                        exec('pm.shadingNode ( "blinn" , asShader = True , n = colorName);');
                        exec('pm.setAttr ( "%s.color" % colorName , (float(code[0]), float(code[1]), float(code[2])) , typ="double3");');
                        exec('pm.setAttr ( "%s.eccentricity" % colorName, 0.6 );');
                        exec('pm.setAttr ("%s.specularRollOff" % colorName, 0.3 );');
                        exec('pm.setAttr ("%s.specularColor" % colorName, (0.15, 0.15, 0.15),typ="double3");');
                    pm.select(geoLists);
                    pm.hyperShade( assign = colorName);

    def deleteColorList( self, *args ):
        colorPath = self.createFolderTxt();
        fileLists = pm.textScrollList( 'previewLists', q = True, selectItem = True );
        if fileLists :
            for fileList in fileLists:
                os.remove(colorPath + '/' + fileLists[0] );
                self.listGetColor(colorPath);
        else :
            mc.warning( 'No Select List' );

    def openFolder( self, *args ):
        currentPath = self.createFolderTxt();
        os.startfile( currentPath );