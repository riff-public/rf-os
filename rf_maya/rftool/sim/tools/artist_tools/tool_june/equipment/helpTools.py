import pymel.core as pm;
import maya.cmds as mc;
import maya.mel as mel;
import os, ast, random;

mp = os.path.realpath(__file__);
fileName = os.path.basename(__file__);
mp = mp.replace( fileName , '' );
mp = mp.replace( '\\' , '/' );

class MainHelpTools( object ):

    def yetiDisplayOff( self,*args ):
        yeti_nodes = pm.ls(type='pgYetiMaya'); 
        for yn in yeti_nodes: 
            yn.displayOutput.set(False);

    def yetiDisplayOn( self,*args ):
        yeti_nodes = pm.ls(type='pgYetiMaya'); 
        for yn in yeti_nodes: 
            yn.displayOutput.set(True);

    def getInvertedHairCurves( self, *args  ):
        from rf_maya.rftool.sim.tools.artist_tools.tool_june.equipment import hairCurves
        hairCurves.getInvertedHairCurves();

    def fixCurveAroundUv( self, *args  ):
        ### Fix curve near uv edge on geometry  
        # fix = True;
        ### value around uv edge 
        # tol = 0.00001;
        ### value for fix    
        # moveDist = 0.001 
        from rf_maya.rftool.sim.tools.artist_tools.tool_june.equipment import hairCurves as uhc 
        reload(uhc);
        uhc.fix_curves_on_border(fix=True,tol=0.00001,moveDist=0.001);

    def renameOutputCurve( self, *args ):
        sels = mc.ls(sl=True);
        listShape = pm.listRelatives(sels, s=True);
        shapeCon = pm.listConnections(listShape, sh=True);
        nameOtpList = [];
        for fol in shapeCon:
            shapeFol = pm.listConnections(fol, t='transform');
            nameOtp = shapeFol[2].split('_ORI')[0] + '_OTP';
            nameOtpList.append(nameOtp)
            getCrv = pm.listConnections(fol)[-1:][0];
            pm.rename(getCrv, nameOtp);
            
        for name in nameOtpList:
            nameShape = pm.listRelatives(name, s=True)[0];
            nameShape.overrideEnabled.set( 0 );

    def dupAnimFixPrerollPose( self, *args ):
        sels = pm.ls(sl=True);
        getFrame = pm.currentTime(q=True);
        frameNumber = int(getFrame);
        prerollFrame = int(getFrame-1)
        dupSels = pm.duplicate(sels);
        dupSelsName = pm.rename( dupSels, sels[0] + '_f' + str(frameNumber) );
        pm.blendShape( dupSelsName, sels[0], origin = 'world', automatic = True, weight = [ 0 , 1.0 ], n = dupSelsName + '_BSH' );
        pm.setKeyframe( dupSelsName + '_BSH.en', t=[frameNumber]);
        pm.setAttr( dupSelsName + '_BSH.en' , 1);
        pm.setKeyframe( dupSelsName + '_BSH.en', t=[prerollFrame]);
        pm.setAttr( dupSelsName + '_BSH.en' , 0);

