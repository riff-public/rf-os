import pymel.core as pm

def create_retime_curve():
    tt = pm.createNode('animCurveTT')
    sf = int(pm.playbackOptions(q=True, min=True))
    ef = int(pm.playbackOptions(q=True, max=True))

    i = 0
    for f in range(sf, ef-1, 2):
        # print f, (f)
        tt.addKey(f, 0.0, tangentInType='fixed', tangentOutType='step')
        pm.keyframe(tt, index=i, absolute=True, valueChange=f)
        i += 1
    return tt

def track_geo(parent, child, vtx_id=0):
    sf = int(pm.playbackOptions(q=True, min=True))
    ef = int(pm.playbackOptions(q=True, max=True))
    pm.currentTime(sf, e=True)

    piv1 = pm.dt.Vector(pm.xform(child.vtx[vtx_id], q=True, ws=True, t=True))
    piv2 = pm.dt.Vector(pm.xform(parent.vtx[vtx_id], q=True, ws=True, t=True))
    offset = piv2 - piv1

    for f in range(sf, ef+1):
        pm.currentTime(f, e=True)
        piv1 = pm.dt.Vector(pm.xform(child.vtx[vtx_id], q=True, ws=True, t=True))
        piv2 = pm.dt.Vector(pm.xform(parent.vtx[vtx_id], q=True, ws=True, t=True))
        inv_piv = (piv2 - piv1) - offset
        pm.xform(child, ws=True, t=inv_piv, r=True)
        pm.setKeyframe(child.t)

# ---- connect/disconnect ABC cache
def connect_retime_cache(ns):
    tt = create_retime_curve()
    abcs = pm.ls('{}:*', type='AlembicNode')
    for abc in abcs:
        pm.connectAttr(tt.output, abc.time, f=True)

def disconnect_retime_cache(ns):
    abcs = pm.ls('{}:*', type='AlembicNode')
    time_node = pm.PyNode('time1')
    for abc in abcs:
        pm.connectAttr(time_node.outTime, abc.time, f=True)

# ---- connect/disconnect anim key from Ctrl
def connect_retime(timeRemap, namespace, exceptions=[]):
    for ctrl in pm.ls('{}:*_Ctrl'.format(namespace)):
        if ctrl.nodeName() in exceptions:
            continue
        for ac in ctrl.inputs(type='animCurve'):
            if ac.isReferenced():
                continue

            pm.connectAttr(timeRemap.output, ac.input, f=True)

def disconnect_retime(namespace):
    for ctrl in pm.ls('{}:*_Ctrl'.format(namespace)):
        for ac in ctrl.inputs(type='animCurve'):
            if ac.isReferenced():
                continue
            tt_cons = ac.input.inputs(type='animCurveTT', p=True, c=True)
            if tt_cons:
                for des, src in tt_cons:
                    pm.disconnectAttr(src, des, f=True)


'''
# EXAMPLE:
# create TT curve
timeRemap = create_retime_curve()

# make on1 animation become on 2
namespace = 'raam_001'
exceptions = ['raam_001:Root_Ctrl']
connect_retime(namespace=namespace, exceptions=exceptions)


# After connecting retime curve to sim cache, this should make sim cache becomes on 2s.
# But sim cache will lack behind anim cache who's translate is on 1s.
# track geo
sim_geo = pm.PyNode('sim_on1:ClothCover_Geo')
anim_geo = pm.PyNode('anim_on2:ClothCover_Geo')
vtx_id = 0  # point of track. Should be pretty still and hidden

track_geo(parent=anim_geo, child=sim_geo, vtx_id=0)

'''