import maya.cmds as mc ;
import maya.mel as mel ;
import sys ;
import pymel.core as pm ;

def loadMatrixPlugin():

    if not mc.pluginInfo('matrixNodes.mll',q=True,l=True):
        try:
            mc.loadPlugin('matrixNodes.mll', quiet=True);
        except:
            raise Exception('Error loading matrixNodes plugin!');

def creacteTrackTransform() :

    loadMatrixPlugin()
    grpRiv = pm.ls( sl = True )[0];
    axes = ['x' , 'y' , 'z'];
    roo = 'xyz'
    pm.xform( grpRiv , roo = roo );

    # TmpRivet
    tmpRiv = pm.spaceLocator( n = grpRiv + '_TmpRiv' );
    pm.xform( tmpRiv, roo = roo );
    pm.connectAttr( grpRiv + '.t', tmpRiv + '.t' );
    pm.connectAttr( grpRiv + '.r', tmpRiv + '.r' );

    # TRACK_TFM_TmpRiv for bake key
    getStartFrame = mc.playbackOptions ( q = True , minTime = True ) ; 
    getEndFrame = mc.playbackOptions ( q = True , maxTime = True ) ;
    startFrame = getStartFrame - 50 ;
    endFrame = getEndFrame + 50 ;
    defaultFrame = 971;
    pm.bakeResults ( tmpRiv , at = [ 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' ] , t = ( startFrame , endFrame ) , simulation = True , minimizeRotation = True );
    for axis in axes:
        pm.setAttr(tmpRiv + '.s' + axis, l = True, k = False, cb = False);
    pm.setAttr(tmpRiv + '.v', l = True, k = False, cb = False);
    pm.currentTime( defaultFrame );
    pm.disconnectAttr( grpRiv + '.t', tmpRiv + '.t' );
    pm.disconnectAttr( grpRiv + '.r', tmpRiv + '.r' );

    # NoTouch_GRP
    noTouchGRP = pm.group(n = grpRiv + '_Track_NoTouch_GRP');
    pm.xform( noTouchGRP, roo = roo );
    pm.hide(noTouchGRP);
    noTouchGRP.t.lock();
    noTouchGRP.r.lock();
    noTouchGRP.s.lock();
    noTouchGRP.v.lock();

    # BWD_GRP : transpose and decompose
    rotTpm = pm.createNode('transposeMatrix', n = grpRiv + '_BWD_RotTpm' );
    rotDcm = pm.createNode('decomposeMatrix', n = grpRiv + '_BWD_RotDcm' );
    pm.connectAttr( tmpRiv + '.matrix', rotTpm + '.inputMatrix' );
    pm.connectAttr( rotTpm + '.outputMatrix', rotDcm + '.inputMatrix' );

    



    