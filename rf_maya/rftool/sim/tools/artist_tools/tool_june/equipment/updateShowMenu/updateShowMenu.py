import pymel.core as pm;
import maya.mel as mel;
import os ;

selfPath = os.path.realpath (__file__) ;
fileName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( fileName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;
imagePath = selfPath + 'imageUpdateShowMenuUI';

def showAll( *args ):
    mel.eval('modelEditor -e -allObjects 1 modelPanel4;');

def showNone( *args ):
    mel.eval('modelEditor -e -allObjects 0 modelPanel4;');
    mel.eval('modelEditor -e -twoSidedLighting false modelPanel4;');
    
def showCurves( *args ):
    mel.eval('modelEditor -e -nurbsCurves true modelPanel4;');
    mel.eval('modelEditor -e -cv true modelPanel4;');

def showNoneCurves( *args ):
    mel.eval('modelEditor -e -nurbsCurves false modelPanel4;');
    mel.eval('modelEditor -e -cv false modelPanel4;');

def showNurbsSurfaces( *args ):
    mel.eval('modelEditor -e -nurbsSurfaces true modelPanel4;modelEditor -e -hos true modelPanel4;');

def showNoneNurbsSurfaces( *args ):
    mel.eval('modelEditor -e -nurbsSurfaces false modelPanel4;modelEditor -e -hos true modelPanel4;');
    
def showDeformers( *args ):
    mel.eval('modelEditor -e -deformers true modelPanel4;');
    
def showNoneDeformers( *args ):
    mel.eval('modelEditor -e -deformers false modelPanel4;');
    
def showDynamicConstraints( *args ):
    mel.eval('modelEditor -e -dynamicConstraints true modelPanel4;');

def showNoneDynamicConstraints( *args ):
    mel.eval('modelEditor -e -dynamicConstraints false modelPanel4;');
    
def showSimClothDyn( *args ):
    showNone();
    mel.eval('modelEditor -e -polymeshes true modelPanel4;modelEditor -e -hos true modelPanel4;');
    
def showSimClothWrap( *args ):
    showNone();
    mel.eval('modelEditor -e -polymeshes true modelPanel4;modelEditor -e -hos true modelPanel4;');
    
def showSimHairDyn( *args ):
    showNone();
    mel.eval('modelEditor -e -polymeshes true modelPanel4;modelEditor -e -hos true modelPanel4;');
    mel.eval('modelEditor -e -nurbsSurfaces true modelPanel4;modelEditor -e -hos true modelPanel4;');
    mel.eval('modelEditor -e -twoSidedLighting true modelPanel4;');
    
def showPluginShapes( *args ):
    showNone();
    mel.eval('modelEditor -e -polymeshes true modelPanel4;modelEditor -e -hos true modelPanel4;');
    mel.eval('modelEditor -e -pluginShapes true modelPanel4;');

def updateShowMenuUI( version = '1.0' ):
    width = 251;
    
    if pm.window( 'updateShowMenuUI', exists = True ):
        pm.deleteUI( 'updateShowMenuUI' );
        
    window = pm.window( 'updateShowMenuUI', title = "Show Menu %s" %version, width = width,
        mnb = True, mxb = True, sizeable = True, rtf = True );

    window = pm.window( 'updateShowMenuUI', e = True, width = width );
    
    separator1Layout = pm.rowColumnLayout( h = 10,w = width, nc = 1, columnWidth = ( 1, 247 ) );
    with separator1Layout :
        pm.separator( h = 1, st = 'in' );

    showLayout1 = pm.rowColumnLayout( h = 30,w = width, nc = 3, columnAlign = (1, 'left'), columnWidth = [ ( 1, 180 ), ( 2, 20 ), ( 3, 100 )]) ;
    with showLayout1 :
        pm.text( label = ' Show All ' );
        pm.checkBox( 'switchOnOff', l = '',  v = False, onc = showAll, ofc = showNone );
        pm.image( image = imagePath + "/switch.png", h = 35 );

    showLayout2 = pm.rowColumnLayout( h = 30,w = width, nc = 3, columnAlign = (1, 'left'), columnWidth = [ ( 1, 180 ), ( 2, 20 ), ( 3, 100 )]) ;
    with showLayout2 :    
        pm.text( label = ' Show Curves ' );
        pm.checkBox( 'switchOnOff', l = '',  v = False, onc = showCurves, ofc = showNoneCurves );
        pm.image( image = imagePath + "/switch.png", h = 35 );

    showLayout3 = pm.rowColumnLayout( h = 30,w = width, nc = 3, columnAlign = (1, 'left'), columnWidth = [ ( 1, 180 ), ( 2, 20 ), ( 3, 100 )]) ;
    with showLayout3 :    
        pm.text( label = ' Show NURBS Surfaces ' );
        pm.checkBox( 'switchOnOff', l = '',  v = False, onc = showNurbsSurfaces, ofc = showNoneNurbsSurfaces );
        pm.image( image = imagePath + "/switch.png", h = 35 );

    showLayout4 = pm.rowColumnLayout( h = 30,w = width, nc = 3, columnAlign = (1, 'left'), columnWidth = [ ( 1, 180 ), ( 2, 20 ), ( 3, 100 )]) ;
    with showLayout4 :    
        pm.text( label = ' Show Deformers ' );
        pm.checkBox( 'switchOnOff', l = '',  v = False, onc = showDeformers, ofc = showNoneDeformers );
        pm.image( image = imagePath + "/switch.png", h = 35 );

    showLayout5 = pm.rowColumnLayout( h = 30,w = width, nc = 3, columnAlign = (1, 'left'), columnWidth = [ ( 1, 180 ), ( 2, 20 ), ( 3, 100 )]) ;
    with showLayout5 :    
        pm.text( label = ' Show Dynamic Constraints ' );
        pm.checkBox( 'switchOnOff', l = '',  v = False, onc = showDynamicConstraints, ofc = showNoneDynamicConstraints );
        pm.image( image = imagePath + "/switch.png", h = 35 );

    showLayout6 = pm.rowColumnLayout( h = 30,w = width, nc = 3, columnAlign = (1, 'left'), columnWidth = [ ( 1, 180 ), ( 2, 20 ), ( 3, 100 )]) ;
    with showLayout6 :    
        pm.text( label = ' Show Sim Cloth Dyn ' );
        pm.checkBox( 'switchOnOff', l = '',  v = False, onc = showSimClothDyn, ofc = showNone );
        pm.image( image = imagePath + "/switch.png", h = 35 );

    showLayout7 = pm.rowColumnLayout( h = 30,w = width, nc = 3, columnAlign = (1, 'left'), columnWidth = [ ( 1, 180 ), ( 2, 20 ), ( 3, 100 )]) ;
    with showLayout7 :    
        pm.text( label = ' Show Sim Cloth Wrap ' );
        pm.checkBox( 'switchOnOff', l = '',  v = False, onc = showSimClothWrap, ofc = showNone );
        pm.image( image = imagePath + "/switch.png", h = 35 );

    showLayout8 = pm.rowColumnLayout( h = 30,w = width, nc = 3, columnAlign = (1, 'left'), columnWidth = [ ( 1, 180 ), ( 2, 20 ), ( 3, 100 )]) ;
    with showLayout8 :    
        pm.text( label = ' Show Sim Hair Dyn ' );
        pm.checkBox( 'switchOnOff', l = '',  v = False, onc = showSimHairDyn, ofc = showNone );
        pm.image( image = imagePath + "/switch.png", h = 35 );

    showLayout9 = pm.rowColumnLayout( h = 30,w = width, nc = 3, columnAlign = (1, 'left'), columnWidth = [ ( 1, 180 ), ( 2, 20 ), ( 3, 100 )]) ;
    with showLayout9 :    
        pm.text( label = ' Show Plugin Shapes ' );
        pm.checkBox( 'switchOnOff', l = '',  v = False, onc = showPluginShapes, ofc = showNone );
        pm.image( image = imagePath + "/switch.png", h = 35 );

    separator2Layout = pm.rowColumnLayout( h = 10,w = width, nc = 1, columnWidth = ( 1, 247 ) );
    with separator2Layout :
        pm.separator( h = 10, st = 'in' );

    pm.showWindow( window );
    showNone();