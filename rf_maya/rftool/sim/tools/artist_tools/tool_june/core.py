import os ;
import pymel.core as pm ;
import maya.cmds as mc ;

selfPath = os.path.realpath (__file__) ;
selfName = os.path.basename (__file__) ;
selfPath = selfPath.replace ( selfName , '' ) ;
selfPath = selfPath.replace ( '\\' , '/' ) ;

from rf_maya.rftool.sim.tools.artist_tools.tool_june.setupSim import setupCloth;
reload(setupCloth);
from rf_maya.rftool.sim.tools.artist_tools.tool_june.setupSim import setupHair;
reload(setupHair);
from rf_maya.rftool.sim.tools.artist_tools.tool_june.equipment.assignColor import assignColorGeo;
reload( assignColorGeo );
from rf_maya.rftool.sim.tools.artist_tools.tool_june.equipment.playblast import playblastSim;
reload( playblastSim );
from rf_maya.rftool.sim.tools.artist_tools.tool_june import module
reload(module);
from rf_maya.rftool.sim.tools.artist_tools.tool_june.equipment import helpTools;
reload( helpTools );

core = '%s/core'%os.environ.get('RFSCRIPT');
mpSetupSim = core + '/rf_maya/rftool/sim/tools/artist_tools/tool_june/setupSim';
mpEquipment = core + '/rf_maya/rftool/sim/tools/artist_tools/tool_june/equipment';
mpAutoSim = core + '/rf_maya/rftool/sim/tools/artist_tools/tool_june/autoSim' ;

class Ui( object ):

        # nucleusTrack
    def getName(self, *args):
        sels = pm.ls(selection=True);
        if sels == []:
            pm.warning('No Selection');
        else :
            pm.textField('inputFollicleName', e = True, tx = sels[0]);
            
    def nucleusTrack(self, *args): 
        self.module.nucleusTrack();

# -----------------------------------------------------------------------------------
        # ClothSimSetup
    def refChaClothDyn(self, *args): 
        self.setupClothSim.refChaClothDyn();

    def browseClothDyn(self, *args):
        self.setupClothSim.browseClothDyn();

    def runClothDyn(self, *args):
        self.setupClothSim.runClothDyn();

# -----------------------------------------------------------------------------------
        # ColthWrpSetup
    def refChaClothWrp(self, *args):
        self.setupClothWrp.refChaClothWrp();

    def browseClothWrp(self, *args):
        self.setupClothWrp.browseClothWrp();

    def runClothWrp(self, *args):
        self.setupClothWrp.runClothWrp();

# -----------------------------------------------------------------------------------
        # ColthWrpFixSetup
    def refChaClothWrpFix(self, *args):
        self.setupClothWrpFix.refChaClothWrpFix();

    def browseClothWrpFix(self, *args):
        self.setupClothWrpFix.browseClothWrpFix();

    def runClothWrpFix(self, *args):
        self.setupClothWrpFix.runClothWrpFix();

# -----------------------------------------------------------------------------------
        # ClothAutoSim
    def refChaClothAutoSim(self, *args):
        self.setupClothAuto.refChaClothAutoSim();

    def runClothAutoSimNew(self, *args):
        self.setupClothAuto.runClothAutoSimNew();
        self.setupClothAuto.dynLayerVisibility();

# -----------------------------------------------------------------------------------
        # HairDynSetup
    def fileInfoHairDyn(self, *args):
        self.setupHairDyn.fileInfoHairDyn();

    def dupCrvDyn(self, *args):
        self.setupHairDyn.dupCrvDyn();

    def rebuildCurve(self, *args):
        from rf_maya.rftool.sim.tools.simtools.hairSetup import hairSetup_rebuildCurveUtil;
        reload(hairSetup_rebuildCurveUtil);
        hairSetup_rebuildCurveUtil.run ( );

    def createLayer(self, *args):
        self.setupLayer.createLayer();

    def layerVisibility(self, *args):
        self.setupLayer.layerVisibility();

    def layerGreen(self, *args):
        self.setupLayer.layerGreen();

    def layerLightBlue(self, *args):
        self.setupLayer.layerLightBlue();

    def layerRed(self, *args):
        self.setupLayer.layerRed();  

    def layerYellow(self, *args):
        self.setupLayer.layerYellow();   

    def layerLightPink(self, *args):
        self.setupLayer.layerLightPink();   

    def layerLightBrown(self, *args):
        self.setupLayer.layerLightBrown();  

    def layerDarkGreen(self, *args):
        self.setupLayer.layerDarkGreen();  

    def layerLightGrey(self, *args):
        self.setupLayer.layerLightGrey();

    def layerDarkRed(self, *args):
        self.setupLayer.layerDarkRed();

    def layerDarkPurple(self, *args):
        self.setupLayer.layerDarkPurple();

    def layerSeaGreen(self, *args):
        self.setupLayer.layerSeaGreen();

    def layerBlue(self, *args):
        self.setupLayer.layerBlue();

    def layerDarkBrown(self, *args):
        self.setupLayer.layerDarkBrown();

    def layerYellowGreen(self, *args):
        self.setupLayer.layerYellowGreen();

    def layerDarkPink(self, *args):
        self.setupLayer.layerDarkPink();

    def layerAquamarine(self, *args):
        self.setupLayer.layerAquamarine();

    def selectOption(self, *args):
        self.setupLayer.selectOption();

    def getHairInfo(self, *args):
        self.setupHairDyn.getHairInfo();

    def getMainHead(self, *args):
        self.setupHairDyn.getMainHead();

    def getEdge(self, *args):
        self.setupHairDyn.getEdge();

    def loadInfo(self, *args):
        self.setupHairDyn.loadInfo();

    def runHairDyn(self, *args):
        self.setupHairDyn.runHairDyn();

    def hairQC(self, *args):    
        self.module.hairQC();

    def copyCurveShape(self, *args):  
        self.module.copyCurveShape();

    def hairTipConstraint(self, *args):
        self.setupHairDyn.hairTipConstraint();

    def assignMaterialPerObject(self, *args):
        self.module.assignMaterialPerObject();

# -----------------------------------------------------------------------------------
        # HairYetiSetup
    def browseCharacterPath(self, *args):
        self.module.browseCharacterPath();

    def fileInfoHairYeti(self, *args):
        self.setupHairYeti.fileInfoHairYeti();

    def getMainHeadYeti(self, *args):
        self.setupHairYeti.getMainHeadYeti();

    def browseHairOutput(self, *args):
        self.setupHairYeti.browseHairOutput();

    def renameHairOutput(self, *args):
        self.setupHairYeti.renameHairOutput();

    def runYeti(self, *args):
        self.setupHairYeti.runYeti();

# -----------------------------------------------------------------------------------
        # HairYetiAuto
    def fileInfoHairAuto(self, *args):
        self.setupHairAuto.fileInfoHairAuto();

    def getMainHeadYetiAutoSim(self, *args):
        self.setupHairAuto.getMainHeadYetiAutoSim();

    def runHairAutoSim(self, *args):
        self.setupHairAuto.runHairAutoSim();

# -----------------------------------------------------------------------------------
        # HairWrp
    def fileInfoHairWrp(self, *args):
        self.setupHairWrp.fileInfoHairWrp();

    def browseHairOutputHairWrp(self, *args):
        self.setupHairWrp.browseHairOutputHairWrp();

    def runHairWrp(self, *args):
        self.setupHairWrp.runHairWrp();

# -----------------------------------------------------------------------------------
        # HairWireWRPSetup
    def fileInfoHairWire(self, *args):
        self.setupHairWire.fileInfoHairWire();

    def browseWireCrv(self, *args):
        self.setupHairWire.browseWireCrv();

    def browseHairGeoWire(self, *args):
        self.setupHairWire.browseHairGeoWire();

    def browseHairWireOutput(self, *args):
        self.setupHairWire.browseHairWireOutput();

    def dupGeoWire(self, *args):
        self.setupHairWire.dupGeoWire();

# -----------------------------------------------------------------------------------
        # HairYetiNodeSetup
    def fileInfoHairYetiNode(self, *args):
        self.setupYetiNode.fileInfoHairYetiNode();

    def getMainHeadYetiNode(self, *args):
        self.setupYetiNode.getMainHeadYetiNode();

    def runYetiNode(self, *args):
        self.setupYetiNode.runYetiNode();

# -----------------------------------------------------------------------------------
        # helpTools

    def yetiDisplayOn(self, *args):
        self.helpTools.yetiDisplayOn();

    def yetiDisplayOff(self, *args):
        self.helpTools.yetiDisplayOff();

    # def getInvertedHairCurves(self, *args):
    #     self.helpTools.getInvertedHairCurves();

    # def nu_FixCurveUV_cmd(self, *args):
    #     self.helpTools.nu_FixCurveUV_cmd();

    def renameOutputCurve( self, *args ):
        self.helpTools.renameOutputCurve();
        
    def dupAnimFixPrerollPose( self, *args ):
        self.helpTools.dupAnimFixPrerollPose();

        # AssignColor
    def click(self, *args): 
        self.assignShade.click();

    def getGeoGrpName(self, *args): 
        self.assignShade.getGeoGrpName();

    def openFolder(self, *args): 
        self.assignShade.openFolder();

    def assignColor(self, *args): 
        self.assignShade.assignColor();

    def deleteColorList(self, *args):
        self.assignShade.deleteColorList();

    def show(self): 
        self.simUI();
        self.assignShade.listGetColor();

        # Playblast
    def playblastTools(self, *args): 
        self.playblastSim.playblastTools();
        self.playblastSim.setDefaultUI();

# -----------------------------------------------------------------------------------
    # create curve from polygon
    def separateHair(self, *args): 
        self.setupHairDyn.separateHair();

    def cc_maker(self, *args): 
        from rf_maya.rftool.sim.tools.artist_tools.tool_sam.cc_maker import ccMaker;
        reload(ccMaker)
        ccMaker.show();

# -----------------------------------------------------------------------------------
    # auto sim
    def coreAutoSim(*args):
        from rf_maya.rftool.sim.tools.artist_tools.tool_june.autoSim import coreAutoSim
        reload(coreAutoSim);
        coreAutoSim.autoSimUI();

# -----------------------------------------------------------------------------------
    # helpText popup 
    def readTxt( self, txt ):
        if os.path.exists ( self.textSetupSimUI + '/' +  txt):
            setupSimUI = open( self.textSetupSimUI + '/' +  txt, 'r' );
            text = setupSimUI.read( );
            setupSimUI.close( );
        elif os.path.exists ( self.textEquipment + '/' +  txt):
            equipment = open( self.textEquipment + '/' +  txt, 'r' );
            text = equipment.read( );
            equipment.close( );
        elif os.path.exists ( self.textAutoSim + '/' +  txt):
            autoSim = open( self.textAutoSim + '/' +  txt, 'r' );
            text = autoSim.read( );
            autoSim.close( );

        return text;

    def textPopUp( self, txt, title = 'text' ):
        text = self.readTxt(txt = txt);
        if pm.window( 'textPopUpUI', exists = True ):
            pm.deleteUI( 'textPopUpUI' );
        else: pass;
       
        window = pm.window( 'textPopUpUI', title = title,
            mnb = False, mxb = False, sizeable = True, rtf = True );
            
        pm.window( 'textPopUpUI', e = True, w = 500, h = 200 );

        with window:
            mainLayout = pm.formLayout();
            textScroll = pm.scrollField( wordWrap = True , editable = False , text = text ) ;
            pm.formLayout( mainLayout, e = True, attachForm = [( textScroll, 'left', 5 ), ( textScroll, 'right', 5 ), ( textScroll, 'top', 5 ), ( textScroll, 'bottom', 5 ) ])

        window.show();

    def helpNucleusTrack( self, *args ):
        self.textPopUp(txt = 'helpNucleusTrack.txt', title = 'Nucleus Track');

    def helpClothDyn( self, *args ):
        self.textPopUp(txt = 'helpClothDyn.txt', title = 'Help Cloth Dyn Setup');

    def helpClothWrp( self, *args ):
        self.textPopUp(txt = 'helpClothWrp.txt', title = 'Help Cloth Wrp Setup');

    def helpClothWrpFix( self, *args ):
        self.textPopUp(txt = 'helpClothWrpFix.txt', title = 'Help Cloth WrpFix Setup');

    def helpClothAutoSim( self, *args ):
        self.textPopUp(txt = 'helpClothAutoSim.txt', title = 'Help Cloth Auto SIM');

    def helpHairDyn( self, *args ):
        self.textPopUp(txt = 'helpHairDyn.txt', title = 'Help Hair Dyn Setup');

    def helpHairAutoSim( self, *args ):
        self.textPopUp(txt = 'helpHairAutoSim.txt', title = 'Help Hair Auto SIM');

    def helpNucleus( self, *args ):
        self.textPopUp(txt = 'helpNucleus.txt', title = 'Help Number Of Nucleus');

    def helpHairYeti( self, *args ):
        self.textPopUp(txt = 'helpHairYeti.txt', title = 'Help Hair Yeti Setup');

    def helpHairWrp( self, *args ):
        self.textPopUp(txt = 'helpHairWrp.txt', title = 'Help Hair Wrp Setup');

    def helpHairWire( self, *args ):
        self.textPopUp(txt = 'helpHairWire.txt', title = 'Help Hair Wire Wrp Setup');

    def helpHairYetiNode( self, *args ):
        self.textPopUp(txt = 'helpHairYetiNode.txt', title = 'Help Hair YetiNode Setup');

    def helpPlayblast( self, *args ):
        self.textPopUp(txt = 'helpPlayblast.txt', title = 'Help Playblast Tools');

    def helpGetInvertedHairCurves( self, *args ):
        self.textPopUp(txt = 'helpGetInvertedHairCurves.txt', title = 'Help Get Inverted Hair Curves');

    def helpFixCurveAroundUVEdges( self, *args ):
        self.textPopUp(txt = 'helpFixCurveAroundUVEdges.txt', title = 'Help Fix Curve Around UV Edges');

    def helpRenameOutputCurve( self, *args ):
        self.textPopUp(txt = 'helpRenameOutputCurve.txt', title = 'Help Rename Output Curve');

    def helpDupAnimFixPrerollPose( self, *args ):
        self.textPopUp(txt = 'helpDupAnimFixPrerollPose.txt', title = 'Help Duplicate Anim To Fix Preroll Pose');

    def helpYetiDisplayOnOff( self, *args ):
        self.textPopUp(txt = 'helpYetiDisplayOnOff.txt', title = 'Help Yeti Display');

    def helpAutoSim( self, *args ):
        self.textPopUp(txt = 'helpAutoSim.txt', title = 'Help Auto Sim');

# -----------------------------------------------------------------------------------
    # UI
    def __init__( self ):
        self.ui       = 'simUI';
        self.width    = 505.00;
        self.title    = 'Setup Simulation Tools';
        self.version  = 1.0;
        self.module = module.MainModule();
        self.setupClothSim = setupCloth.SetupSim();
        self.setupClothWrp = setupCloth.SetupWrp();
        self.setupClothWrpFix = setupCloth.SetupWrpFix();
        self.setupClothAuto = setupCloth.SetupClothAuto();
        self.setupHairDyn = setupHair.SetupHairDyn();
        self.setupLayer = setupHair.Layer();
        self.setupHairYeti = setupHair.SetupHairYeti();
        self.setupHairAuto = setupHair.SetupHairAuto();
        self.setupHairWrp = setupHair.SetupHairWrp();
        self.setupHairWire = setupHair.SetupHairWire();
        self.setupYetiNode = setupHair.SetupYetiNode();
        self.helpTools = helpTools.MainHelpTools();
        self.assignShade   = assignColorGeo.AssignColor();
        self.playblastSim  = playblastSim

        self.imageSetupSimUI = mpSetupSim + '/imageSetupSimUI';
        self.textSetupSimUI = mpSetupSim + '/textSetupSimUI';
        self.imEquipment = mpEquipment + '/imageEquipment'
        self.imAssigncolor = mpEquipment + '/assignColor/imageAssignColorUI';
        self.textEquipment = mpEquipment + '/textEquipment'
        self.textAutoSim = mpAutoSim + '/textAutoSim'

    def simUI( self ):
        if pm.window( 'simUI' , exists = True ):
            pm.deleteUI( 'simUI' );

        w = self.width;
     
        window = pm.window( 'simUI' , 
            title     = '{title} {version}'.format( title = self.title, version = self.version ),
            mnb       = True ,
            mxb       = False,
            sizeable  = False,
            rtf       = True
            );

        pm.window( window , e = True , w = w, h = 600, bgc=[0.15, 0.15, 0.15] );
        with window:

            with pm.rowColumnLayout( h = 50,w = w, nc = 1, columnAlign = (1, 'left'), columnWidth = ( 1, w )):
                pm.symbolButton( image = self.imageSetupSimUI + "/juneUI.jpg" , width = w , height = 50 );
                pm.separator( vis = False );

                # with pm.scrollLayout ( childResizable = True, h = 660, w = 505, bgc=[0.15, 0.15, 0.15] ):

                with pm.tabLayout ( 'tab', w = 495 ):
# Setup ----------------------------------------------------------------------------------
                    pm.columnLayout("Setup", adjustableColumn = True);
                    pm.separator( vis = False );

                    with pm.scrollLayout ( childResizable = True, h = 520, w = 500, bgc=[0.15, 0.15, 0.15] ):
                        pm.columnLayout("columnLayoutSetup", adjustableColumn = True);

                        with pm.rowColumnLayout( h = 70, w = w, nc = 1, columnWidth = ( 1, 497 )):
                            pm.separator( vis = False );

                            with pm.rowColumnLayout( h = 30, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.text( label = 'Setup Information' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 50 )]):
                                pm.text( label = ' Character Path :' );
                                pm.textField( 'inputChaPath', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.shelfButton( h = 25, c = self.browseCharacterPath ,style = 'iconAndTextVertical', image = self.imageSetupSimUI + "/files.png" );
                                pm.separator( vis = False );

            # NucleusTrack------------------------------------------------------------------------
                        with pm.frameLayout ("layoutFrameNucleusTrack", label = "Nucleus Track", collapsable = True, collapse = True, w = w, parent = "columnLayoutSetup", bgc = [0.9, 0.5, 0.4] ):

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' Follicle Name :' );
                                pm.textField( 'inputFollicleName', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Get Name', h = 25, c = self.getName, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnWidth = [( 1, 90 ), ( 2, 320 ), ( 3, 10 ), ( 4, 35 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Run', h = 25, c = self.nucleusTrack, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.shelfButton( h = 25, c = self.helpNucleusTrack, style = 'iconAndTextVertical', image = self.imageSetupSimUI + "/conversation.png" );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

            # ClothDyn------------------------------------------------------------------------
                        with pm.frameLayout ("layoutFrameClothSim", label = "Cloth DYN / Auto SIM", collapsable = True, collapse = True, w = w, parent = "columnLayoutSetup"):

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 9, columnWidth = [ ( 1, 120 ), ( 2, 100 ), ( 3, 15 ), ( 4, 50 ), ( 5, 15 ), ( 6, 15 ), ( 7, 55 ), ( 8, 75 ), ( 9, 40 ) ]):
                                pm.separator( vis = False );
                                pm.text( label = ' Auto Wrap Fix :' );
                                pm.separator( vis = False );
                                pm.radioCollection( 'dynWrapFixSwitch' );
                                pm.radioButton( 'dynWrapFixSwitchOn', l = 'On');
                                pm.separator( vis = False );
                                pm.radioCollection( 'dynWrapFixSwitch', e = True, select = 'dynWrapFixSwitchOn' );
                                pm.separator( vis = False );
                                pm.radioButton( 'dynWrapFixSwitchOff', l = 'Off');

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' Character Geo :' );
                                pm.textField( 'inputRefDynPath', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Reference', h = 25, c = self.refChaClothDyn, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' DYN_GRP :' );
                                pm.textField( 'inputDynPath', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Import', h = 25, c = self.browseClothDyn, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( h = 10, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnWidth = [( 1, 90 ), ( 2, 320 ), ( 3, 10 ), ( 4, 35 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Run', h = 25, c = self.runClothDyn, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.shelfButton( h = 25, c = self.helpClothDyn, style = 'iconAndTextVertical', image = self.imageSetupSimUI + "/conversation.png" );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 ), bgc = [1, 1, 1]):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 20, w = w, nc = 1, columnWidth = ( 1, 490 )):
                                pm.text( label = 'Create Cloth Wrap For Auto SIM' );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 9, columnWidth = [ ( 1, 120 ), ( 2, 100 ), ( 3, 15 ), ( 4, 50 ), ( 5, 15 ), ( 6, 15 ), ( 7, 55 ), ( 8, 75 ), ( 9, 40 ) ]):
                                pm.separator( vis = False );
                                pm.text( label = ' Auto Wrap :' );
                                pm.separator( vis = False );
                                pm.radioCollection( 'autoSimSwitch' );
                                pm.radioButton( 'autoSimSwitchOn', l = 'On');
                                pm.separator( vis = False );
                                pm.radioCollection( 'autoSimSwitch', e = True, select = 'autoSimSwitchOn' );
                                pm.separator( vis = False );
                                pm.radioButton( 'autoSimSwitchOff', l = 'Off');

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' Character Geo :' );
                                pm.textField( 'inputRefClothAutoSimPath', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Reference', h = 25, c = self.refChaClothAutoSim, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnWidth = [( 1, 90 ), ( 2, 320 ), ( 3, 10 ), ( 4, 35 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Run Auto SIM', h = 25, c = self.runClothAutoSimNew, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.shelfButton( h = 25, c = self.helpClothAutoSim, style = 'iconAndTextVertical', image = self.imageSetupSimUI + "/conversation.png" );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

            # ClothWrp------------------------------------------------------------------------

                        with pm.frameLayout ("layoutFrameClothWrp", label = "Cloth WRP", collapsable = True, collapse = True, w = w, parent = "columnLayoutSetup"):
                            pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 9, columnWidth = [ ( 1, 100 ), ( 2, 120 ), ( 3, 15 ), ( 4, 50 ), ( 5, 15 ), ( 6, 15 ), ( 7, 55 ), ( 8, 75 ), ( 9, 40 ) ]):
                                pm.separator( vis = False );
                                pm.text( label = ' Auto Wrap / Wrap Fix :' );
                                pm.separator( vis = False );
                                pm.radioCollection( 'wrapSwitch' );
                                pm.radioButton( 'wrapSwitchOn', l = 'On');
                                pm.separator( vis = False );
                                pm.radioCollection( 'wrapSwitch', e = True, select = 'wrapSwitchOn' );
                                pm.separator( vis = False );
                                pm.radioButton( 'wrapSwitchOff', l = 'Off');

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' Character Geo :' );
                                pm.textField( 'inputRefWrpPath', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Reference', h = 25, c = self.refChaClothWrp, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' DYN_GRP :' );
                                pm.textField( 'inputDynWrpPath', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Import', h = 25, c = self.browseClothWrp, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( h = 10, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnWidth = [( 1, 90 ), ( 2, 320 ), ( 3, 10 ), ( 4, 35 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Run', h = 25, c = self.runClothWrp, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.shelfButton( h = 25, c = self.helpClothWrp ,style = 'iconAndTextVertical', image = self.imageSetupSimUI + "/conversation.png" );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

            # ClothWrpFix------------------------------------------------------------------------

                        with pm.frameLayout ("layoutFrameClothWrpFix", label = "Cloth WRP Fix", collapsable = True, collapse = True, w = w, parent = "columnLayoutSetup"):
                            pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 9, columnWidth = [ ( 1, 120 ), ( 2, 100 ), ( 3, 15 ), ( 4, 50 ), ( 5, 15 ), ( 6, 15 ), ( 7, 55 ), ( 8, 75 ), ( 9, 40 ) ]):
                                pm.separator( vis = False );
                                pm.text( label = ' Auto Wrap Fix :' );
                                pm.separator( vis = False );
                                pm.radioCollection( 'wrapFixSwitch' );
                                pm.radioButton( 'wrapFixSwitchOn', l = 'On');
                                pm.separator( vis = False );
                                pm.radioCollection( 'wrapFixSwitch', e = True, select = 'wrapFixSwitchOn' );
                                pm.separator( vis = False );
                                pm.radioButton( 'wrapFixSwitchOff', l = 'Off');


                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' Character Geo :' );
                                pm.textField( 'inputRefWrpFixPath', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Reference', h = 25, c = self.refChaClothWrpFix, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' DYN_GRP :' );
                                pm.textField( 'inputDynWrpFixPath', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Import', h = 25, c = self.browseClothWrpFix, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( h = 10, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnWidth = [( 1, 90 ), ( 2, 320 ), ( 3, 10 ), ( 4, 35 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Run', h = 25, c = self.runClothWrpFix, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.shelfButton( h = 25, c = self.helpClothWrpFix ,style = 'iconAndTextVertical', image = self.imageSetupSimUI + "/conversation.png" );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

            # HairDyn------------------------------------------------------------------------

                        with pm.frameLayout ("layoutFrameHairDyn", label = "Hair DYN / Auto SIM", collapsable = True, collapse = True, w = w, parent = "columnLayoutSetup"):
                            pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 20, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.text( label = 'Make Hair Curve From Hair Polygon' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 6, columnWidth = [( 1, 5 ), ( 2, 155 ), ( 3, 5 ), ( 4, 155 ), ( 5, 5 ), ( 6, 155 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Separate Hair Polygon', h = 25, c = self.separateHair, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.button( label = "Create Curve (Sam's Script)", h = 25, c = self.cc_maker, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.button( label = 'Rebuild Curve', h = 25, c = self.rebuildCurve, bgc=[0.25, 0.25, 0.25] );

                            with pm.rowColumnLayout( h = 15, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.text( label = '*** Export selection (.ma) "Hair_CRV_GRP" and "Hair_GEO_GRP" to your workspace.' );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 ), bgc = [1, 1, 1]):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 50, w = w, nc = 4, columnWidth = [ ( 1, w / 5.7 ), ( 2, w / 1.68 ), ( 3, w / 180 ), ( 4, w / 5.65 )]): 

                                with pm.rowColumnLayout( h = 30, w = w, nc = 1, columnWidth = ( 1, w / 10.0 )):
                                    pm.text( label = ' File Info :' );

                                with pm.rowColumnLayout( h = 30, w = w, nc = 1, columnWidth = ( 1, w / 1.70 )):
                                    pm.textScrollList( 'fileListsHairDyn' , h = 50, numberOfRows = 20, allowMultiSelection = 1 );

                                with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, w / 180 )):
                                    pm.separator( h = 5, vis = False );

                                with pm.rowColumnLayout( w = w, nc = 1, columnWidth = ( 1, w / 5.65 )): 
                                    pm.button( label = 'Get File', h = 50, bgc=[0.25, 0.25, 0.25], c = self.fileInfoHairDyn );
                                    pm.separator( h = 5, vis = False );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnWidth = [( 1, 90 ), ( 2, 295 ), ( 3, 10 ), ( 4, 30 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Duplicate CRV_GRP to ORI_GRP', h = 25, c = self.dupCrvDyn, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 9, columnWidth = [ ( 1, 55 ), ( 2, 100 ), ( 3, 15 ), ( 4, 120 ), ( 5, 15 ), ( 6, 15 ), ( 7, 120 ), ( 8, 75 ), ( 9, 40 ) ]):
                                pm.separator( vis = False );
                                pm.text( label = 'Choose Color For :' );
                                pm.separator( vis = False );
                                pm.radioCollection( 'optionSwitch' );
                                pm.radioButton( 'layerSwitchOn', l = 'Create Layer Color');
                                pm.separator( vis = False );
                                pm.radioCollection( 'optionSwitch', e = True, select = 'layerSwitchOn' );
                                pm.separator( vis = False );
                                pm.radioButton( 'selectSwitchOn', l = 'Select Curve Color');
                        
                            with pm.rowColumnLayout( h = 25,w = w, nc = 16, columnWidth = [( 1, 65 ),( 2, 40 ),( 3, 5 ), ( 4, 40 ), ( 5, 5 ), ( 6, 40 ), ( 7, 5 ), ( 8, 40 ), ( 9, 5 ), ( 10, 40 ),( 11, 5 ), ( 12, 40 ), ( 13, 5 ), ( 14, 40 ), ( 15, 5 ), ( 16, 40 )]):
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerGreen , bgc = ( 0.5 , 1 , 0 ) );
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerLightBlue, bgc = ( 0 , 1 , 1 ) );
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerRed, bgc = ( 1 , 0 , 0 ) );
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerYellow, bgc = ( 1 , 1 , 0 ) );
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerLightPink, bgc = ( 1, 0.7, 0.7 ) );
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerLightBrown, bgc = ( 0.8, 0.6, 0.4 ) );
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerDarkGreen, bgc = ( 0.1 , 0.5 , 0.1 ) );
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerLightGrey, bgc = ( 0.7 , 0.7 , 0.7 ) );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 16, columnWidth = [( 1, 65 ),( 2, 40 ),( 3, 5 ), ( 4, 40 ), ( 5, 5 ), ( 6, 40 ), ( 7, 5 ), ( 8, 40 ), ( 9, 5 ), ( 10, 40 ),( 11, 5 ), ( 12, 40 ), ( 13, 5 ), ( 14, 40 ), ( 15, 5 ), ( 16, 40 )]):
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerDarkRed, bgc = ( 0.5 , 0 , 0 ) );
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerDarkPurple, bgc = ( 0.6 , 0 , 0.8 ) );
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerSeaGreen, bgc = ( 0 , 0.5 , 0.5 ) );
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerBlue, bgc = ( 0 , 0 , 0.8 ) );
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerDarkBrown, bgc = ( 0.6 , 0.3 , 0.1 ) );
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerYellowGreen, bgc = ( 0.4 , 0.5 , 0.1 ) );
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerDarkPink, bgc = ( 0.8 , 0 , 0.8 ) );
                                pm.separator( vis = False );
                                pm.button( label = '', h = 25, c = self.layerAquamarine, bgc = ( 0 , 0.9 , 0.6 ) );
                                pm.separator( vis = False );

                            with pm.rowColumnLayout( h = 30,w = w, nc = 3, columnWidth = [( 1, 90 ), ( 2, 295 ), ( 3, 10 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Visibility Layer', h = 25, c = self.layerVisibility, bgc=[0.25, 0.25, 0.25] );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 12, columnAlign = (1, 'left'),columnWidth = [ ( 1, 100 ), ( 2, 10 ), ( 3, 60 ), ( 4, 10 ), ( 5, 10 ), ( 6, 55 ), ( 7, 10 ), ( 8, 55 ), ( 9, 10 ), ( 10, 55 ), ( 11, 20 ), ( 12, 85 ) ]):
                                pm.text( label = ' Select Step :' );
                                pm.separator( vis = False );
                                pm.radioCollection( 'selectStep' );
                                pm.radioButton( 'selectStepManual', l = 'Manual');
                                pm.separator( vis = False );
                                pm.radioCollection( 'selectStep', e = True, select = 'selectStepManual' );
                                pm.separator( vis = False );
                                pm.radioButton( 'selectStepTwo', l = 'Two');
                                pm.separator( vis = False );
                                pm.radioButton( 'selectStepThree', l = 'Three');
                                pm.separator( vis = False );
                                pm.radioButton( 'selectStepFour', l = 'Four');
                                pm.separator( vis = False );
                                pm.button( label = 'Select', h = 25,c = self.selectOption, bgc=[0.25, 0.25, 0.25] );

                            with pm.rowColumnLayout( h = 20, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( vis = False );
                                pm.text( label = '*** Export selection (.ma) "Hair_ORI_GRP" to your workspace.' );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 9, columnWidth = [ ( 1, 120 ), ( 2, 100 ), ( 3, 15 ), ( 4, 50 ), ( 5, 15 ), ( 6, 15 ), ( 7, 55 ), ( 8, 75 ), ( 9, 40 ) ]):
                                pm.separator( vis = False );
                                pm.text( label = ' Create Hair Tube :' );
                                pm.separator( vis = False );
                                pm.radioCollection( 'tubeSwitch' );
                                pm.radioButton( 'tubeSwitchOn', l = 'On');
                                pm.separator( vis = False );
                                pm.radioCollection( 'tubeSwitch', e = True, select = 'tubeSwitchOn' );
                                pm.separator( vis = False );
                                pm.radioButton( 'tubeSwitchOff', l = 'Off');

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 10, columnWidth = [ ( 1, 95 ), ( 2, 30 ), ( 3, 110 ), ( 4, 15 ), ( 5, 50 ), ( 6, 15 ), ( 7, 15 ), ( 8, 55 ), ( 9, 75 ), ( 10, 40 ) ]):
                                pm.separator( vis = False );
                                pm.shelfButton( h = 25, style = 'iconAndTextVertical', image = self.imageSetupSimUI + "/alert.png" );
                                pm.text( label = ' Number Of Nucleus :' );
                                pm.separator( vis = False );
                                pm.radioCollection( 'setSwitch' );
                                pm.radioButton( 'oneSwitchOn', l = 'One');
                                pm.separator( vis = False );
                                pm.radioCollection( 'setSwitch', e = True, select = 'oneSwitchOn' );
                                pm.separator( vis = False );
                                pm.radioButton( 'fourSwitchOn', l = 'Four');

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 295 ), ( 3, 5 ), ( 4, 90 )]):
                                pm.text( label = ' ORI_GRP  :' );
                                pm.textField( 'inputOriInfo', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Get Name', h = 25, c = self.getHairInfo, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 295 ), ( 3, 5 ), ( 4, 90 )]):
                                pm.text( label = ' Head Geo  :' );
                                pm.textField( 'inputGetMainHead', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Get Name', h = 25, c = self.getMainHead, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 10, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 140 ), ( 3, 5 ), ( 4, 5 ), ( 5, 5 ), ( 6, 140 ), ( 7, 5 ), ( 8, 41 ), ( 9, 7 ), ( 10, 41 )]):
                                pm.text( label = ' CIN Edges  :' );
                                pm.textField( 'inputGetEdge1', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.text( label = ' : ' );
                                pm.separator( vis = False );
                                pm.textField( 'inputGetEdge2', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Get', h = 25, c = self.getEdge, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.button( label = 'Load', h = 25, c = self.loadInfo, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );

                            with pm.rowColumnLayout( h = 30,w = w, nc = 4, columnWidth = [( 1, 90 ), ( 2, 295 ), ( 3, 10 ), ( 4, 30 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Run', h = 25, c = self.runHairDyn, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.shelfButton( h = 25, style = 'iconAndTextVertical', image = self.imageSetupSimUI + "/conversation.png", c = self.helpHairDyn );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnWidth = [( 1, 5 ),( 2, 235 ), ( 3, 5 ), ( 4, 235 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Hair Tip Constraint', c = self.hairTipConstraint , h = 25, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.button( label = 'Random Hair Tube Color', c = self.assignMaterialPerObject , h = 25, bgc=[0.25, 0.25, 0.25] );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnWidth = [( 1, 5 ),( 2, 235 ), ( 3, 5 ), ( 4, 235 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'QC CRV', c = self.hairQC , h = 25, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.button( label = 'Copy Curve Shape', c = self.copyCurveShape, h = 25, bgc=[0.25, 0.25, 0.25] );

                            with pm.rowColumnLayout( h = 15, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.text( label = '*** Export selection (.ma) "Hair_Output_GRP" to your workspace.' );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 ), bgc = [1, 1, 1]):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 20, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.text( label = 'Import Hair Yeti For Auto SIM' );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 50, w = w, nc = 4, columnWidth = [ ( 1, w / 5.7 ), ( 2, w / 1.68 ), ( 3, w / 180 ), ( 4, w / 5.65 )]): 

                                with pm.rowColumnLayout( h = 30, w = w, nc = 1, columnWidth = ( 1, w / 10.0 )):
                                    pm.text( label = ' File Info :' );

                                with pm.rowColumnLayout( h = 30, w = w, nc = 1, columnWidth = ( 1, w / 1.70 )):
                                    pm.textScrollList( 'fileListsHairAuto' , h = 50, numberOfRows = 20, allowMultiSelection = 1 );

                                with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, w / 180 )):
                                    pm.separator( h = 5, vis = False );

                                with pm.rowColumnLayout( w = w, nc = 1, columnWidth = ( 1, w / 5.65 )): 
                                    pm.button( label = 'Get File', h = 50, bgc=[0.25, 0.25, 0.25], c = self.fileInfoHairAuto );
                                    pm.separator( h = 5, vis = False );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 295 ), ( 3, 5 ), ( 4, 90 )]):
                                pm.text( label = ' MainWrap COL  :' );
                                pm.textField( 'inputMainHeadYetiAutoSim', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Get Name', h = 25, c = self.getMainHeadYetiAutoSim, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );

                            with pm.rowColumnLayout( h = 30,w = w, nc = 4, columnWidth = [( 1, 90 ), ( 2, 295 ), ( 3, 10 ), ( 4, 30 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Run Auto SIM', h = 25, c = self.runHairAutoSim, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.shelfButton( h = 25, style = 'iconAndTextVertical', image = self.imageSetupSimUI + "/conversation.png", c = self.helpHairAutoSim );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );
                                
                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

            # HairYeti------------------------------------------------------------------------

                        with pm.frameLayout ("layoutFrameHairYeti", label = "Hair Yeti", collapsable = True, collapse = True, w = w, parent = "columnLayoutSetup"):
                            pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 95, w = w, nc = 4, columnWidth = [ ( 1, w / 5.7 ), ( 2, w / 1.68 ), ( 3, w / 20.0 ), ( 4, w / 7.45 )]): 

                                with pm.rowColumnLayout( h = 30, w = w, nc = 1, columnWidth = ( 1, w / 10.0 )):
                                    pm.text( label = ' File Info :' );

                                with pm.rowColumnLayout( h = 30, w = w, nc = 1, columnWidth = ( 1, w / 1.58 )):
                                    pm.textScrollList( 'fileListsHairYeti' , h = 90, numberOfRows = 20, allowMultiSelection = 1 );

                                with pm.rowColumnLayout( h = 30, w = w, nc = 1, columnWidth = ( 1, w / 20.0 )):
                                    pm.separator( h = 5, vis = False );

                                with pm.rowColumnLayout( w = w, nc = 1, columnWidth = ( 1, w / 7.45 )): 
                                    pm.button( label = 'Get File', h = 90, bgc=[0.25, 0.25, 0.25], c = self.fileInfoHairYeti );
                                    pm.separator( h = 5, vis = False );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' Hair Output :' );
                                pm.textField( 'inputHairOutputPath', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Import', h = 25, c = self.browseHairOutput, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( h = 10, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' MainWrap CIN :' );
                                pm.textField( 'inputGetMainHeadYeti', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Get Name', h = 25, c = self.getMainHeadYeti, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( h = 10, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 6, columnWidth = [( 1, 5 ), ( 2, 212 ), ( 3, 5 ), ( 4, 212 ), ( 5, 10 ), ( 6, 35 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Rename Hair Output Curve', h = 25, c = self.renameHairOutput, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.button( label = 'Run', h = 25, c = self.runYeti, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.shelfButton( h = 25, c = self.helpHairYeti ,style = 'iconAndTextVertical', image = self.imageSetupSimUI + "/conversation.png" );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );
            # HairWrp------------------------------------------------------------------------
                        with pm.frameLayout ("layoutFrameHairWrp", label = "Hair WRP", collapsable = True, collapse = True, w = w, parent = "columnLayoutSetup"):
                            pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 50, w = w, nc = 4, columnWidth = [ ( 1, w / 5.7 ), ( 2, w / 1.68 ), ( 3, w / 20.0 ), ( 4, w / 7.45 )]): 

                                with pm.rowColumnLayout( h = 30, w = w, nc = 1, columnWidth = ( 1, w / 10.0 )):
                                    pm.text( label = ' File Info :' );

                                with pm.rowColumnLayout( h = 30, w = w, nc = 1, columnWidth = ( 1, w / 1.58 )):
                                    pm.textScrollList( 'fileListsHairWrp' , h = 90, numberOfRows = 20, allowMultiSelection = 1 );

                                with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, w / 20.0 )):
                                    pm.separator( h = 5, vis = False );

                                with pm.rowColumnLayout( w = w, nc = 1, columnWidth = ( 1, w / 7.45 )): 
                                    pm.button( label = 'Get File', h = 50, bgc=[0.25, 0.25, 0.25], c = self.fileInfoHairWrp );
                                    pm.separator( h = 5, vis = False );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' Hair Output :' );
                                pm.textField( 'inputHairOutputHairWrp', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Import', h = 25, c = self.browseHairOutputHairWrp, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( h = 10, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 6, columnWidth = [( 1, 5 ), ( 2, 212 ), ( 3, 5 ), ( 4, 212 ), ( 5, 10 ), ( 6, 35 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Rename Hair Output Curve', h = 25, c = self.renameHairOutput, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.button( label = 'Run', h = 25, c = self.runHairWrp, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.shelfButton( h = 25, c = self.helpHairWrp ,style = 'iconAndTextVertical', image = self.imageSetupSimUI + "/conversation.png" );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

            # HairWireWRP------------------------------------------------------------------------

                        with pm.frameLayout ("layoutFrameHairWireWRP", label = "Hair Wire WRP", collapsable = True, collapse = True, w = w, parent = "columnLayoutSetup"):
                            pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 50, w = w, nc = 4, columnWidth = [ ( 1, w / 5.7 ), ( 2, w / 1.68 ), ( 3, w / 20.0 ), ( 4, w / 7.45 )]): 

                                with pm.rowColumnLayout( h = 30, w = w, nc = 1, columnWidth = ( 1, w / 10.0 )):
                                    pm.text( label = ' File Info :' );

                                with pm.rowColumnLayout( h = 30, w = w, nc = 1, columnWidth = ( 1, w / 1.58 )):
                                    pm.textScrollList( 'fileListsHairWireWrp' , h = 90, numberOfRows = 20, allowMultiSelection = 1 );

                                with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, w / 20.0 )):
                                    pm.separator( h = 5, vis = False );

                                with pm.rowColumnLayout( w = w, nc = 1, columnWidth = ( 1, w / 7.45 )): 
                                    pm.button( label = 'Get File', h = 50, bgc=[0.25, 0.25, 0.25], c = self.fileInfoHairWire );
                                    pm.separator( h = 5, vis = False );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' Hair_CRV_GRP :' );
                                pm.textField( 'inputRefWireWRPCrvPath', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Import', h = 25, c = self.browseWireCrv, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' Hair_GEO_GRP :' );
                                pm.textField( 'inputHairGeoWireWRP', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Import', h = 25, c = self.browseHairGeoWire, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( h = 10, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' Hair Output :' );
                                pm.textField( 'inputHairGeoWireWRPOutput', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Import', h = 25, c = self.browseHairWireOutput, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( h = 10, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 6, columnWidth = [( 1, 5 ), ( 2, 212 ), ( 3, 5 ), ( 4, 212 ), ( 5, 10 ), ( 6, 35 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Rename Hair Output Curve', h = 25, c = self.renameHairOutput, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.button( label = "Dup Hair_Geo Ref for making Wire", h = 25, c = self.dupGeoWire, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.shelfButton( h = 25, c = self.helpHairWire ,style = 'iconAndTextVertical', image = self.imageSetupSimUI + "/conversation.png" );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

             # YetiNode------------------------------------------------------------------------
                        with pm.frameLayout ("layoutFrameYetiNode", label = "Hair YetiNode", collapsable = True, collapse = True, w = w, parent = "columnLayoutSetup"):

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

                            with pm.rowColumnLayout( h = 50, w = w, nc = 4, columnWidth = [ ( 1, w / 5.7 ), ( 2, w / 1.68 ), ( 3, w / 20.0 ), ( 4, w / 7.45 )]): 

                                with pm.rowColumnLayout( h = 30, w = w, nc = 1, columnWidth = ( 1, w / 10.0 )):
                                    pm.text( label = ' File Info :' );

                                with pm.rowColumnLayout( h = 30, w = w, nc = 1, columnWidth = ( 1, w / 1.58 )):
                                    pm.textScrollList( 'fileListsHairYetiNode' , h = 90, numberOfRows = 20, allowMultiSelection = 1 );

                                with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, w / 20.0 )):
                                    pm.separator( h = 5, vis = False );

                                with pm.rowColumnLayout( w = w, nc = 1, columnWidth = ( 1, w / 7.45 )): 
                                    pm.button( label = 'Get File', h = 50, bgc=[0.25, 0.25, 0.25], c = self.fileInfoHairYetiNode );
                                    pm.separator( h = 5, vis = False );
                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnAlign = (1, 'left'), columnWidth = [ ( 1, 90 ), ( 2, 320 ), ( 3, 5 ), ( 4, 65 )]):
                                pm.text( label = ' MainWrap CIN :' );
                                pm.textField( 'inputGetMainHeadYetiNode', en = True, text = '' );
                                pm.separator( vis = False );
                                pm.button( label = 'Get Name', h = 25, c = self.getMainHeadYetiNode, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( h = 10, st = 'in' );

                            with pm.rowColumnLayout( h = 25,w = w, nc = 4, columnWidth = [( 1, 90 ), ( 2, 320 ), ( 3, 10 ), ( 4, 35 )]):
                                pm.separator( vis = False );
                                pm.button( label = 'Run', h = 25, c = self.runYetiNode, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( vis = False );
                                pm.shelfButton( h = 25, c = self.helpHairYetiNode, style = 'iconAndTextVertical', image = self.imageSetupSimUI + "/conversation.png" );

                            with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                                pm.separator( h = 5, st = 'in' );

# Equipment ----------------------------------------------------------------------------------
                    pm.columnLayout("Equipment", adjustableColumn = True);
            # Help Tools-------------------------------------------------------------------

                    with pm.frameLayout ("layoutFrame01", label = "Help Tools", collapsable = True, collapse = False, w = w, parent = "Equipment"):

                        with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                            pm.separator( h = 5, st = 'in' );

                        with pm.rowColumnLayout( h = 30,w = w, nc = 4, columnWidth = [( 1, 125 ), ( 2, 235 ), ( 3, 10 ), ( 4, 30 )]):
                            pm.separator( vis = False );
                            pm.button( label = 'Playblast Tools', h = 25, c = self.playblastTools, bgc = ( 1 , 0.9 , 0.7 ) );
                            pm.separator( vis = False );
                            pm.shelfButton( h = 25, style = 'iconAndTextVertical', image = self.imEquipment + "/suggestion.png", c = self.helpPlayblast );

                        with pm.rowColumnLayout( h = 30,w = w, nc = 6, columnWidth = [( 1, 125 ), ( 2, 112 ), ( 3, 11 ), ( 4, 112 ), ( 5, 10 ), ( 6, 30 )]):
                            pm.separator( vis = False );
                            pm.button( label = 'Yeti Display On', h = 25, c = self.yetiDisplayOn, bgc = ( 0.9 , 0.7 , 0.8 ) );
                            pm.separator( vis = False );
                            pm.button( label = 'Yeti Display Off', h = 25, c = self.yetiDisplayOff, bgc = ( 0.7 , 0.5 , 0.8 ) );
                            pm.separator( vis = False );
                            pm.shelfButton( h = 25, style = 'iconAndTextVertical', image = self.imEquipment + "/suggestion.png", c = self.helpYetiDisplayOnOff );

                        # with pm.rowColumnLayout( h = 30,w = w, nc = 4, columnWidth = [( 1, 125 ), ( 2, 235 ), ( 3, 10 ), ( 4, 30 )]):
                        #     pm.separator( vis = False );
                        #     pm.button( label = 'Get Inverted Hair Curves', h = 25, c = self.getInvertedHairCurves, bgc = ( 0.5 , 0.9 , 0.6 ) );
                        #     pm.separator( vis = False );
                        #     pm.shelfButton( h = 25, style = 'iconAndTextVertical', image = self.imEquipment + "/suggestion.png", c = self.helpGetInvertedHairCurves );

                        # with pm.rowColumnLayout( h = 30,w = w, nc = 4, columnWidth = [( 1, 125 ), ( 2, 235 ), ( 3, 10 ), ( 4, 30 )]):
                        #     pm.separator( vis = False );
                        #     pm.button( label = 'Fix Curve Around UV Edges', h = 25, c = self.nu_FixCurveUV_cmd, bgc = ( 0.6 , 0.8 , 0.2 ) );
                        #     pm.separator( vis = False );
                        #     pm.shelfButton( h = 25, style = 'iconAndTextVertical', image = self.imEquipment + "/suggestion.png", c = self.helpFixCurveAroundUVEdges );

                        with pm.rowColumnLayout( h = 30,w = w, nc = 4, columnWidth = [( 1, 125 ), ( 2, 235 ), ( 3, 10 ), ( 4, 30 )]):
                            pm.separator( vis = False );
                            pm.button( label = 'Rename Output Curve', h = 25, c = self.renameOutputCurve, bgc = ( 1 , 0.8 , 0 ) );
                            pm.separator( vis = False );
                            pm.shelfButton( h = 25, style = 'iconAndTextVertical', image = self.imEquipment + "/suggestion.png", c = self.helpRenameOutputCurve );

                        with pm.rowColumnLayout( h = 30,w = w, nc = 4, columnWidth = [( 1, 125 ), ( 2, 235 ), ( 3, 10 ), ( 4, 30 )]):
                            pm.separator( vis = False );
                            pm.button( label = 'Duplicate Anim To Fix Preroll Pose', h = 25, c = self.dupAnimFixPrerollPose, bgc = ( 0.9 , 0.5 , 0.5 ) );
                            pm.separator( vis = False );
                            pm.shelfButton( h = 25, style = 'iconAndTextVertical', image = self.imEquipment + "/suggestion.png", c = self.helpDupAnimFixPrerollPose );

                        with pm.rowColumnLayout( h = 30,w = w, nc = 4, columnWidth = [( 1, 125 ), ( 2, 235 ), ( 3, 10 ), ( 4, 30 )]):
                            pm.separator( vis = False );
                            pm.button( label = 'simulate cloth / hair local', h = 25, c = self.coreAutoSim, bgc = ( 0.933333, 0.909804, 0.5 ) );
                            pm.separator( vis = False );
                            pm.shelfButton( h = 25, style = 'iconAndTextVertical', image = self.imEquipment + "/suggestion.png", c = self.helpAutoSim );

                    with pm.frameLayout ("layoutFrame02", label = "Assign Shade Geo", collapsable = True, collapse = True, w = w, parent = "Equipment"):

                        with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                            pm.separator( h = 5, st = 'in' );

                        with pm.rowColumnLayout( h = 25, w = w, nc = 6, columnWidth = [ ( 1, 80 ), ( 2, 290 ), ( 3, 5 ), ( 4, 30 ), ( 5, 5 ), ( 6, 70 )]):
                            pm.text( label = ' Group Name : ' );
                            pm.textField( 'CharacterTF', en = True, text = '' );
                            pm.separator( vis = False );
                            pm.shelfButton( style = 'iconAndTextVertical', h = 25, image = self.imAssigncolor + "/clickingHeart.png", c = self.click );
                            pm.separator( vis = False );
                            pm.button( label = 'Get Color', h = 25, c = self.getGeoGrpName, bgc=[0.25, 0.25, 0.25] );

                        with pm.rowColumnLayout( h = 120, w = w, nc = 2, columnWidth = [ ( 1, w / 2.0 ), ( 2, w / 2.0 )]): 

                            with pm.rowColumnLayout( h = 120, w = w, nc = 1, columnWidth = ( 1, w / 2.0 )):
                                pm.textScrollList( 'previewLists', h = 120, numberOfRows = 20, allowMultiSelection = 1, dcc = self.openFolder );

                            with pm.rowColumnLayout( w = w, nc = 1, columnWidth = ( 1, w / 2.0 )): 
                                pm.button( label = 'Assign Color', h = 90, bgc = (0.60, 0.60, 0.60), c = self.assignColor );
                                pm.separator( h = 5, vis = False );
                                pm.button( label = 'Delete Color List', h = 25, c = self.deleteColorList, bgc=[0.25, 0.25, 0.25] );
                                pm.separator( h = 10, vis = False );
                        
                        with pm.rowColumnLayout( h = 5, w = w, nc = 1, columnWidth = ( 1, 497 )):
                            pm.separator( h = 5, st = 'in' );
                                
            window.show(); 