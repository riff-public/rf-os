import pymel.core as pm
import maya.mel as mel

class SamMultiCollide ( object ) :

	def __init__ ( self ) :
		pass 

	def makeCollide ( self , target ) :
		nucleus = pm.ls ( typ = 'nucleus' )
		lenN = len ( nucleus )
		if ':' in target :
			ign = target.split ( ':' ) [ 1 ]
			if '_' in ign :
				ign = ign.split ( '_' ) [ 0 ]
				grp = pm.group ( em = True , w = True , n = ign + 'MulN_grp' )
			else :
				grp = pm.group ( em = True , w = True , n = ign + 'MulN_grp' )
		else :
			if '_' in target :
				ign = target.split ( '_' ) [ 0 ]
				grp = pm.group ( em = True , w = True , n = ign + 'MulN_grp' )
			else :
				grp = pm.group ( em = True , w = True , n = target + 'MulN_grp' )
		for numN in range ( lenN ) :
			num = numN + 1
			if ':' in target :
				ign = target.split ( ':' ) [ 1 ]
				if '_' in ign :
					ign = ign.split ( '_' ) [ 0 ]
					dup = target.duplicate ( n = ign + 'N%s'%num + '_collide' )
				else :
					dup = target.duplicate ( n = ign + 'N%s'%num + '_collide' )
				show = dup [ 0 ]
				self.clean ( target = dup )
				self.unlockShowVis ( target = show )
			else :
				if '_' in target :
					ign = target.split ( '_' ) [ 0 ]
					dup = target.duplicate ( n = ign + 'N%s'%num + '_collide' )
				else :
					dup = target.duplicate ( n = target + 'N%s'%num + '_collide' )
				show = dup [ 0 ]
				self.clean ( target = dup )
				self.unlockShowVis ( target = show )
			pm.parent ( dup , grp )
			if num < numN  :
				print 'no'
			elif numN - numN < numN  :
				for each in dup :
					self.unlockHideVis ( target = each )

		geo = pm.listRelatives ( grp , typ = 'transform' )
		self.unlockHideVis ( target = target )

		pm.blendShape ( target , geo [ 0 ] , n = target + '_bsh' , o = 'world' , w = ( 0 , 1 ) )
		for each in range ( len ( geo ) ) :
			if each == 0 :
				pass
			else :
				pm.blendShape ( geo [ 0 ] , geo [ each ] , n = geo [ 0 ] + '_bsh' , o = 'world' , w = ( 0 , 1 ) )
		pm.select ( geo , r = True )
		collide = mel.eval ( 'makeCollideNCloth' )
		listCollide = pm.listRelatives ( collide , ap = True )
		nRigid = pm.ls ( collide , typ = 'nRigid' )
		nameDict = []
		for dynamic in nRigid :
			dynamic.setAttr ( 'thickness' , 0.1 )
			dynamic.setAttr ( 'friction' , 0 )
		pm.select ( cl = True )
		for geos in geo :
			name = geos.split ( '_' ) [ 0 ]
			name += '_nRigid'
			nameDict.append ( name )
		for listC in range ( len ( listCollide ) ) :
			pm.select ( listCollide [ listC ] , r = True )
			mel.eval ( 'assignNSolver %s' % nucleus [ listC ] )
			pm.rename ( listCollide [ listC ] , nameDict [ listC ] )
		listBelow = nameDict [ 1: ]
		up = nameDict [ 0 ]
		for down in listBelow :
			pm.connectAttr ( up + '.isDynamic'       , down + '.isDynamic')
			pm.connectAttr ( up + '.collide'         , down + '.collide')
			pm.connectAttr ( up + '.collisionFlag'   , down + '.collisionFlag')
			pm.connectAttr ( up + '.collideStrength' , down + '.collideStrength')
			pm.connectAttr ( up + '.collisionLayer'  , down + '.collisionLayer')
			pm.connectAttr ( up + '.solverDisplay'   , down + '.solverDisplay')
			pm.connectAttr ( up + '.displayColor'    , down + '.displayColor')    
			pm.connectAttr ( up + '.bounce'          , down + '.bounce')
			pm.connectAttr ( up + '.friction'        , down + '.friction')
			pm.connectAttr ( up + '.stickiness'      , down + '.stickiness')
			pm.connectAttr ( up + '.trappedCheck'    , down + '.trappedCheck')
			pm.connectAttr ( up + '.pushOut'         , down + '.pushOut')
			pm.connectAttr ( up + '.pushOutRadius'   , down + '.pushOutRadius')
			pm.connectAttr ( up + '.crossoverPush'   , down + '.crossoverPush')
		pm.parent ( nameDict , grp )

	def multiAll ( self , *args ) :
		select = pm.ls ( sl = True )
		for each in select :
			self.makeCollide ( target = each )

	def clean ( self , target = '' , *args ) :
		t = 'tx','ty','tz'
		s = 'sx','sy','sz'
		r = 'rx','ry','rz'
		for targets in target :
			for t in t :
				if targets.getAttr ( t , l = True ) == True :
					targets.setAttr ( t , l = False )
			for r in r :
				if targets.getAttr ( r , l = True ) == True :
					targets.setAttr ( r , l = False )
			for s in s :
				if targets.getAttr ( s , l = True ) == True :
					targets.setAttr ( s , l = False )
			if targets.getAttr ( 'v' , l = True ) == True :
				targets.setAttr ( 'v' , l = False )
		pm.makeIdentity ( target , apply = True )
		pm.delete ( target ,ch = True )
		pm.xform ( target , cp = True )

	def unlockHideVis ( self , target = '' , *args ) :
		if target.getAttr ( 'v' , l = True ) == True :
			if ':' in target :
				pass
			else :
				target.setAttr ( 'v' , l = False )
				target.setAttr ( 'v' , 0 )
		else :
			target.setAttr ( 'v' , 0 )

	def unlockShowVis ( self , target = '' , *args ) :
		if target.getAttr ( 'v' , l = True ) == True :
			target.setAttr ( 'v' , l = False )
			target.setAttr ( 'v' , 1 )
		else :
			target.setAttr ( 'v' , 1 )

SamMultiCollide().multiAll()