import yaml
import pymel.core as pm

class auto_origin ( object ) :

	def __init__ ( self ) :
		currentProj = pm.workspace ( q = True , fn = True )
		self.project = currentProj.split ('/' )[1]
		self.act = currentProj.split ( '/' )[2]
		self.shot = currentProj.split ( '/' )[3]

	def convert( self , list ) :
		return [ -i for i in list ] 

	def load_anim ( self ) :
		#with open ( r'P:/{project}/scene/publ/{act}/{shot}/anim/main/_data/{shot}_anim_main_workspace_data.hero.yml'.format ( project = self.project , shot = self.shot , act = self.act ) )as file :
		with open ( r'P:/{project}/scene/publ/{act}/{shot}/_data/{shot}_shot_data.hero.yml'.format ( project = self.project , shot = self.shot , act = self.act )) as file : 
			origin = yaml.load ( file )
		#for ori in origin :
		#	print origin[ori]
		ori = origin['cameraDistanceFromOrigin']
		if pm.objExists ( 'ANIM_GRP' ) :
			grp = pm.PyNode ( 'ANIM_GRP' )
		else :
			grp = pm.group ( n = 'ANIM_GRP' , em=True , w = True )
			grp.useOutlinerColor.set ( 1 )
			grp.outlinerColor.set ( 1 , 0 , 0 )
		#grp.t.set ( self.convert ( origin[ori] ))
		#grp.t.set ( self.convert ( ori ))
		grp.t.set ( ori )
		grp.t.lock()
		grp.r.lock()
		grp.s.lock()
		bwd_grp = pm.PyNode ( 'BWD_GRP' )
		# all_transform = pm.ls ( typ = 'transform' )
		# for alls in all_transform :
		# 	if alls.outlinerColor.get() == ( 1 , 0 , 1 ) :
		# 		pm.parent ( alls , bwd_grp )
		bwd_grp.t.lock()
		bwd_grp.r.lock()
		bwd_grp.s.lock()
		pm.parent ( bwd_grp , grp )

	def load_origin ( self ) :
		with open ( r'P:/{project}/scene/publ/{act}/{shot}/_data/{shot}_shot_data.hero.yml'.format ( project = self.project , shot = self.shot , act = self.act )) as file : 
			origin = yaml.load ( file )
		#for ori in origin :
		#	print origin[ori]
		ori = origin['cameraDistanceFromOrigin']
		if pm.objExists ( 'ORI_GRP' ) :
			grp = pm.PyNode ( 'ORI_GRP' )
		else :
			grp = pm.group ( n = 'ORI_GRP' , em=True , w = True )
			grp.useOutlinerColor.set ( 1 )
			grp.outlinerColor.set ( 1 , 0 , 0 )
		#grp.t.set ( self.convert ( origin[ori] ))
		grp.t.set ( self.convert ( ori ))
		grp.t.lock()
		grp.r.lock()
		grp.s.lock()
		#grp.t.set ( ori )
		# bwd_grp = pm.PyNode ( 'BWD_GRP' )
		# all_transform = pm.ls ( typ = 'transform' )
		# for alls in all_transform :
		# 	if alls.outlinerColor.get() == ( 1 , 0 , 1 ) :
		# 		pm.parent ( alls , bwd_grp )
		# bwd_grp.t.lock()
		# bwd_grp.r.lock()
		# bwd_grp.s.lock()
		# pm.parent ( bwd_grp , grp )

auto_origin()