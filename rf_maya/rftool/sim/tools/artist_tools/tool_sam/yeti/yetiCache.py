import pymel.core as pm
import maya.mel as mel
import distutils
import shutil
import sys
import os

from rftool.preload import duration_check
from rf_utils.sg import sg_utils
from rf_utils.context import context_info

class YetiCache ( object ) :

	def __init__ ( self ) :
		self.currentProj = pm.workspace ( fn = True , q = True )
		self.data ()
		self.loadPlugin ()

	# HAVE OR NOT ISN'T IMPORTANT
	def loadPlugin ( self ) :
		pm.mel.eval ( 'pgYetiEnsurePluginLoaded()' )

	def data ( self  ) :
		if 'SevenChickMovie' in self.currentProj :
			self.shot = self.currentProj.split ( '/' ) [ 3 ]
			self.character = self.currentProj.split ( '/' ) [ 4 ]
			self.cachePath = self.currentProj + '/cache'
			self.subFolder = self.shot + '_' + self.character

	def initializeTimeRange ( self , *args ) :
		project = self.currentProj.split('/')[1]
		entity  = self.currentProj.split('/')[3]

		context = context_info.Context()
		context.use_sg ( sg_utils.sg , project , 'scene' , entityName = entity )
		scene = context_info.ContextPathInfo ( context = context )
		sgDuration = duration_check.get_duration ( scene )
		start = scene.projectInfo.scene.start_frame
		end_time = start + ( sgDuration - 1 )

		pm.intField ( self.cacheStart_intField , e = True , value = start - 5 ) 
		pm.intField ( self.cacheEnd_intField , e = True , value = end_time + 5 )

	def yetiLists ( self , *args ) :
		yeti_list = pm.listRelatives ( pm.ls ( type = 'pgYetiMaya' ) , p = True )
		yeti_textScll = pm.textScrollList ( self.yeti_textScll , q = True , ai = True )

		for each in yeti_textScll :
			if not pm.objExists ( each ) :
				pm.textScrollList ( self.yeti_textScll , e = True , ri = each )

		for each in yeti_list :
			if each not in yeti_textScll :
				pm.textScrollList ( self.yeti_textScll , e = True , a = each )

	def openCachePath ( self , *args ) :
		path = pm.textField ( self.yetiPath_txtField , q = True , tx = True )
		if os.path.exists ( path ) :
			os.startfile ( path )

	def browseBtn ( self , *args ) :
		yetiPath = pm.fileDialog2 ( fm = 3 , dir = self.cachePath , cap = 'Export Location' , okc = 'Select' )
		pm.textField ( self.yetiPath_txtField , e = True , tx = yetiPath [ 0 ] )

	def cacheSelected ( self , *args ) :
		currentPath = pm.textField ( self.yetiPath_txtField , q = True , tx = True )
		cacheStart = pm.intField ( self.cacheStart_intField , q = True , v = True )
		cacheEnd = pm.intField ( self.cacheEnd_intField , q = True , v = True )
		cacheNoS = pm.intField ( self.cacheNoS_intField , q = True , v = True )
		folder = currentPath + '/' + self.character
		if not os.path.exists ( folder ) :
			os.makedirs ( folder )
		version = 1
		versionFolder = folder + '/v%03d' %version
		while os.path.exists ( versionFolder ) :
			versionFolder = folder + '/v%03d' %version
			version += 1
			number = version
			number -= 1
		os.makedirs ( versionFolder )
		heroFolder = folder + '/hero'
		# if not os.path.exists ( heroFolder ) :
		# 	os.makedirs ( heroFolder )
		selected = pm.textScrollList ( self.yeti_textScll , q = True , si = True )
		for each in range ( len ( selected ) ) :
			pm.select ( selected [ each ] , r = True )
			name = selected [ each ].split ( '_' ) [ 0 ]
			cacheFolder = versionFolder + '/' + self.character + '_' + name
			if not os.path.exists ( cacheFolder ) :
				os.makedirs ( cacheFolder )
			nameCache = cacheFolder + '/' + self.shot + '_' + self.character + '_' + name + '.v%03d' %number
			pm.mel.eval ( 'pgYetiCommand -writeCache "%s' %nameCache + '.%04d.fur"' + ' -range %s %s -samples %s' % ( cacheStart , cacheEnd , cacheNoS ) )
		oldFiles = []
		for r , d , f in os.walk ( heroFolder ) :
			for file in f :
				if '.fur' in file :
					oldFiles.append ( os.path.join ( r , file ) )
		for f in oldFiles :
			os.remove ( f )
		distutils.dir_util.copy_tree ( r'%s' %versionFolder , r'%s' %heroFolder )
		files = []
		for r , d , f in os.walk ( heroFolder ) :
			for file in f :
				if '.fur' in file :
					files.append ( os.path.join ( r , file ) )
		for f in files :
			heroFile = f.split ( '.v%03d' %number ) [ 0 ] + '.hero' + f.split ( '.v%03d' %number ) [ 1 ]
			os.rename ( f , heroFile )

	def cacheAll ( self , *args ) :
		currentPath = pm.textField ( self.yetiPath_txtField , q = True , tx = True )
		cacheStart = pm.intField ( self.cacheStart_intField , q = True , v = True )
		cacheEnd = pm.intField ( self.cacheEnd_intField , q = True , v = True )
		cacheNoS = pm.intField ( self.cacheNoS_intField , q = True , v = True )
		folder = currentPath + '/' + self.character
		if not os.path.exists ( folder ) :
			os.makedirs ( folder )
		version = 1
		versionFolder = folder + '/v%03d' %version
		while os.path.exists ( versionFolder ) :
			versionFolder = folder + '/v%03d' %version
			version += 1
			number = version
			number -= 1
		os.makedirs ( versionFolder )
		heroFolder = folder + '/hero'
		# if not os.path.exists ( heroFolder ) :
		# 	os.makedirs ( heroFolder )
		selected = pm.textScrollList ( self.yeti_textScll , q = True , ai = True )
		for each in range ( len ( selected ) ) :
			pm.select ( selected [ each ] , r = True )
			name = selected [ each ].split ( '_' ) [ 0 ]
			cacheFolder = versionFolder + '/' + self.character + '_' + name
			if not os.path.exists ( cacheFolder ) :
				os.makedirs ( cacheFolder )
			nameCache = cacheFolder + '/' + self.shot + '_' + self.character + '_' + name + '.v%03d' %number
			pm.mel.eval ( 'pgYetiCommand -writeCache "%s' %nameCache + '.%04d.fur"' + ' -range %s %s -samples %s' % ( cacheStart , cacheEnd , cacheNoS ) )
		oldFiles = []
		for r , d , f in os.walk ( heroFolder ) :
			for file in f :
				if '.fur' in file :
					oldFiles.append ( os.path.join ( r , file ) )
		for f in oldFiles :
			os.remove ( f )
		distutils.dir_util.copy_tree ( r'%s' %versionFolder , r'%s' %heroFolder )
		files = []
		for r , d , f in os.walk ( heroFolder ) :
			for file in f :
				if '.fur' in file :
					files.append ( os.path.join ( r , file ) )
		for f in files :
			heroFile = f.split ( '.v%03d' %number ) [ 0 ] + '.hero' + f.split ( '.v%03d' %number ) [ 1 ]
			os.rename ( f , heroFile )

	def UI ( self , *args ) :
		w = 300.00
		if pm.window ( 'yetiCacheBySam' , ex = True ) :
			pm.deleteUI ( 'yetiCacheBySam' )
		window = pm.window ( 'yetiCacheBySam' , t = 'SamTool : YetiCache' , tb = True , mnb = True , mxb = True , s = False , rtf = True )
		pm.window ( 'yetiCacheBySam' , e = True , w = w , h = 10 )
		with window :
			with pm.rowColumnLayout ( nc = 1 ) :
				yeti_rowColumnLayout = pm.rowColumnLayout ( nc = 1 , w = w )
				with yeti_rowColumnLayout :
					self.yeti_textScll = pm.textScrollList ( w = w , h = 175 , ams = True , sc = self.yetiLists )
					self.yetiLists ()
					pm.button ( l = 'REFRESH' , c = self.yetiLists , bgc = ( 1 , 0.8745 , 0.7294 ) )
					pm.separator ( vis = True , h = 7.5 )
					with pm.rowColumnLayout ( nc = 7 , cw = [ ( 1 , w / 7 ) , ( 2 , w / 7 ) , ( 3 , w / 7 ) , ( 4 , w / 7 ) , ( 5 , w / 7 ) , ( 6 , w / 7 ) , ( 7 , w / 7 ) ] ) :
						pm.text ( l = '' )
						with pm.rowColumnLayout ( nc = 1 , w = w / 5 ) :
							self.cacheStart_intField = pm.intField ( w = w / 5 )
							pm.text ( l = 'START' , w = w / 5 )
						pm.text ( l = '' )
						with pm.rowColumnLayout ( nc = 1 , w = w / 5 ) :
							self.cacheEnd_intField = pm.intField ( w = w / 5 )
							pm.text ( l = 'END' , w = w / 5 )
						pm.text ( l = '' )
						with pm.rowColumnLayout ( nc = 1 , w = w / 5 ) :
							self.cacheNoS_intField = pm.intField ( w = w / 5 , v = 10 )
							pm.text ( l = 'SAMPLE' , w = w / 5 )
						pm.text ( l = '' )
					pm.separator ( vis = True , h = 7.5 )
					with pm.rowColumnLayout ( nc = 1 , w = w ) :
						pm.button ( l = 'CACHE DIRECTORY' , c = self.openCachePath , w = w , bgc = ( 0.7294 , 1 , 1 ) )
					with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w / 6 ) , ( 2 , w / 1.2 ) ] ) :
						pm.button ( l = 'PATH' , en = False )
						self.yetiPath_txtField = pm.textField ( tx = self.cachePath ) 
					with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w / 1.5 ) , ( 2 , w / 3 ) ] ) :
						pm.text ( l = '' )
						pm.button ( 'browseBtn' , l = 'BROWSE' , c = self.browseBtn , bgc = ( 1 , 0.8745 , 0.7294 ) )
					pm.separator ( vis = True , h = 7.5 )
					pm.text ( l = '' , h = 1 )
					with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w / 2 ) , ( 2 , w / 2 ) ] ) :
						pm.button ( l = 'CACHE SELECTED' , c = self.cacheSelected , bgc = ( 0.7294 , 1 , 0.7882 ) )
						pm.button ( l = 'CACHE ALL' , c = self.cacheAll , bgc = ( 1 , 0.7019 , 0.7294 ) )
		self.initializeTimeRange ()

YetiCache().UI()

