import os
import yaml
import shutil
import maya.cmds as mc
import pymel.core as pm
from PySide2.QtGui import*
from PySide2.QtCore import*
from PySide2.QtWidgets import*

import core
reload(core)
import ui
reload(ui)

supported_video = ['mov','qt','mp4']
class DragDropWidget(QListWidget):
	dropped = Signal(list)
	multipleDropped = Signal(list)
	def __init__(self,*args,**kwargs):
		super(DragDropWidget, self).__init__(*args,**kwargs)
		self.setWordWrap(True)
		self.setAcceptDrops(True)

	def dragEnterEvent(self,event):
		if event.mimeData().hasUrls:
			event.accept()
		else: event.ignore()

	def dragMoveEvent(self,event):
		if event.mimeData().hasUrls:
			event.setDropAction(Qt.CopyAction)
			event.accept()
		else: event.ignore()

	def dropEvent(self,event):
		self.clear()
		if event.mimeData().hasUrls:
			event.setDropAction(Qt.CopyAction)
			event.accept()
			links=[]
			for url in event.mimeData().urls():
				links.append(str(url.toLocalFile()))
			self.multipleDropped.emit(links)
			for link in links:
				if link.split('.')[-1] not in supported_video:
					links.remove(link)
			self.addWidgetItems(links)

	def addWidgetItems(self,items):
		for item in items:
			icon = QIcon(core.icon_path+'/video.png')
			item = QListWidgetItem(item)
			item.setIcon(icon)
			self.addItem(item)

class AtamaProject(QMainWindow,ui.Ui_AtamaProject_MainWindow):
	def __init__(self,parent):
		super(AtamaProject,self).__init__(parent)
		'''info'''
		self.setupUi(self)
		self.setWindowTitle(QApplication.translate("AtamaProject_MainWindow","AtamaProject Tool - %s"%core.version,None,-1))
		self.setWindowIcon(QIcon(core.icon_path+'/ghost.png'))
		'''dragDrop'''
		self.tmpLayout = QHBoxLayout(self)
		self.tmpLayout.setEnabled(False)
		self.dragDrop_widget = DragDropWidget()
		self.dragDrop_widget.setGeometry(QRect(0,0,280,80))
		sizePolicy = QSizePolicy(QSizePolicy.Expanding,QSizePolicy.Minimum)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.dragDrop_widget.sizePolicy().hasHeightForWidth())
		self.dragDrop_widget.setSizePolicy(sizePolicy)
		self.dragDrop_widget.setMinimumSize(QSize(60,60))
		self.dragDrop_widget.setObjectName('dragDrop_widget')
		brush = QBrush()
		brush.setStyle(Qt.BDiagPattern)
		palette = self.dragDrop_widget.palette()
		palette.setBrush(QPalette.Background,brush)

		buttonIcon = QIcon(core.icon_path+'/refresh.png')
		self.workspace_pushButton.setIcon(buttonIcon)
		buttonIcon = QIcon(core.icon_path+'/analytics.png')
		self.project_pushButton.setIcon(buttonIcon)

		self.dragDrop_widget.setPalette(palette)
		self.button_dragDropArea = QScrollArea(self.dragDrop_groupBox)
		self.button_dragDropArea.setWidgetResizable(True)
		self.button_dragDropArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
		self.button_dragDropArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
		self.button_dragDropArea.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
		self.button_dragDropArea.setObjectName('button_dragDropArea')
		self.button_dragDropArea.setWidget(self.dragDrop_widget)
		self.dragDrop_layout.addWidget(self.button_dragDropArea)
		'''function'''
		self.loadPlugin()
		self.updateWorkspace()
		self.createDataPath()
		self.setDefaultLocal(core.local_path)
		self.refreshWorkspace()
		self.loadData()
		self.refreshYeti()
		self.defaultYeti()
		self.simTimeRange()
		self.yetiTimeRange()
		self.updateSentPath()
		self.listCamera()
		self.listImport()
		'''localCMD'''
		self.local_pushButton.clicked.connect(self.selectDefault)
		self.local_radioButton.clicked.connect(self.switchRadioButton)
		self.server_radioButton.clicked.connect(self.switchRadioButton)
		self.act_listWidget.itemSelectionChanged.connect(self.loadShot)
		self.autoNaming_pushButton.clicked.connect(self.autoNaming)
		self.autoNaming_pushButton.clicked.connect(self.setProject)
		'''timeRange'''
		self.setTimeRange_pushButton.clicked.connect(self.setTimeRange)
		'''cache'''
		self.localWs_radioButton.clicked.connect(self.switchProject)
		self.serverWs_radioButton.clicked.connect(self.switchProject)
		self.exportGeo_pushButton.clicked.connect(self.exportGeo)
		self.exportCamera_pushButton.clicked.connect(self.exportCam)
		self.import_pushButton.clicked.connect(self.bwdImport)
		self.bwd_pushButton.clicked.connect(self.bwdImport)
		self.anim_pushButton.clicked.connect(self.oepnAnimFile)
		self.openPb_pushButton.clicked.connect(self.openPlayblast)
		'''yetiCMD'''
		self.yeti_pushButton.clicked.connect(self.refreshYeti)
		self.yetiPath_pushButton.clicked.connect(self.yetiPath)
		self.yetiOpen_pushButton.clicked.connect(self.openYetiPath)
		self.cacheAll_pushButton.clicked.connect(self.cacheYetiAll)
		self.cache_pushButton.clicked.connect(self.cacheYetiSelected)
		'''update'''
		self.dragDrop_widget.itemSelectionChanged.connect(self.updateNamePb)
		'''copyCMD'''
		self.sentPath_pushButton.clicked.connect(self.openSentPath)
		self.copyYeti_pushButton.clicked.connect(self.copyYeti)
		self.copyTech_pushButton.clicked.connect(self.copyTech)
		self.copyPb_pushButton.clicked.connect(self.copyPb)
		self.workspace_pushButton.clicked.connect(self.refresh)

	def refresh(self):
		self.refreshWorkspace()
		self.updateWorkspace()
		self.simTimeRange()
		self.updateSentPath()
		self.yetiTimeRange()
		self.defaultYeti()
		self.listCamera()
		self.listImport()

	def loadPlugin(self):
		pm.loadPlugin ( 'redshift4maya.mll' )
		pm.pluginInfo ( 'redshift4maya.mll' , e = True , a = True )

	def updateWorkspace(self):
		workspace = pm.workspace(q=True,rd=True)
		self.project_lineEdit.setText(workspace)
		self.checkWsRadioButton()

	def createDataPath(self):
		self.createFolder(core.scriptData_path)

	def createDefaultText(self,path):
		if not os.path.exists(path):
			self.writeText(path,'')

	def setDefaultLocal(self,path):
		if os.path.exists(path):
			local = self.readText(path)
			self.local_lineEdit.setText(local)

	def selectDefault(self):
		folder = pm.fileDialog2(cap='Select Folder',fm=3,okc='Select',dir=core.default_path)[0]
		self.local_lineEdit.setText(folder)
		local = self.local_lineEdit.text()
		self.writeText(core.local_path,local)

	def writeText(self,path,object):
		with open(r'{path}'.format(path=path),'w+') as file:
			file.write(object)
			file.close()

	def readText(self,path):
		with open(r'{path}'.format(path=path),'r') as file:
			local = file.read()
			file.close()
		return local

	def createFolder(self,name):
		if not os.path.exists(name):
			os.makedirs(name)
		return(name)

	def openFolder(self,path):
		if os.path.exists(path):
			os.startfile(path)

	def copyfile(self,file,path):
		shutil.copy2(file,path)

	def copyFolder(self,folder,sent_folder,symlink,ignore):
		for item in os.listdir(folder):
			wanted = os.path.join(folder,item)
			sent = os.path.join(sent_folder,item)
			if os.path.exists(sent):
				try:
					shutil.rmtree(sent)
				except: os.unlink(sent)
			if os.path.isdir(wanted):
				shutil.copytree(wanted,sent,symlink,ignore)
			else: shutil.copy2(wanted,sent)

	def updateTimeRange(self):
		workspace = pm.workspace(q=True,rd=True)
		currentAct = workspace.split('/')[2]
		currentShot = workspace.split('/')[3]
		with open(r'{path}'.format(path=core.yaml_file)) as file:
			data = yaml.load(file)
			shots = data['allShots']
			for shot in range(len(shots)):
				if shots[shot][0] == currentAct:
					for each in range(len(shots[shot])):
						if shots[shot][each][0] == currentShot:
							start = shots[shot][each][1]
							end = shots[shot][each][2]
				else: pass
		preRoll = start-30
		start = start
		end = end
		postRoll = end+10
		return (preRoll,start,end,postRoll)

	def copyAnimFile(self):
		workspace = pm.workspace(q=True,rd=True)
		currentAct = workspace.split('/')[2]
		currentShot = workspace.split('/')[3]
		with open(r'{path}'.format(path=core.yaml_file)) as file:
			data = yaml.load(file)
			shots = data['allShots']
			for shot in range(len(shots)):
				if shots[shot][0] == currentAct:
					for each in range(len(shots[shot])):
						if shots[shot][each][0] == currentShot:
							file = shots[shot][each][3]
				else:pass
		dataPath = workspace+'data'
		self.copyfile(file,dataPath)

	def refreshWorkspace(self):
		workspace = pm.workspace(q=True,rd=True)

	def simTimeRange(self):
		workspace = pm.workspace(q=True,rd=True)
		project = workspace.split('/')[1]
		if project == core.project:
			time = self.updateTimeRange()
			self.preRoll_lineEdit.setText(str(time[0]))
			self.start_lineEdit.setText(str(time[1]))
			self.end_lineEdit.setText(str(time[2]))
			self.postRoll_lineEdit.setText(str(time[3]))
			self.checkTimeRange()
		else:pass

	def checkTimeRange(self,*args):
		currentStart = pm.playbackOptions(q=True,min=True)
		currentEnd = pm.playbackOptions(q=True,max=True)
		timeRange = self.getTimeRange()
		preRoll = timeRange[0]
		start = timeRange[1]
		end = timeRange[2]
		postRoll = timeRange[3]
		if (currentStart==preRoll) and (currentEnd==postRoll):
			self.setTimeRange_pushButton.setStyleSheet('background-color:#99ccff')
		elif (currentStart==start) and (currentEnd==end):
			self.setTimeRange_pushButton.setStyleSheet('background-color:#ff9966')
		else: pass

	def getTimeRange(self,*args):
		preRoll = int(self.preRoll_lineEdit.text())
		start = int(self.start_lineEdit.text())
		end = int(self.end_lineEdit.text())
		postRoll = int(self.postRoll_lineEdit.text())
		return (preRoll,start,end,postRoll)

	def setTimeRange(self):
		currentStart = pm.playbackOptions(q=True,min=True)
		currentEnd = pm.playbackOptions(q=True,max=True)
		timeRange = self.getTimeRange()
		preRoll = timeRange[0]
		start = timeRange[1]
		end = timeRange[2]
		postRoll = timeRange[3]
		if (currentStart==preRoll) and (currentEnd==postRoll):
			pm.playbackOptions(min=start,max=end)
			self.setTimeRange_pushButton.setStyleSheet('background-color:#ff9966')
		elif (currentStart==start) and (currentEnd==end):
			pm.playbackOptions(min=preRoll,max=postRoll)
			self.setTimeRange_pushButton.setStyleSheet('background-color:#99ccff')
		else : 
			pm.playbackOptions(min=preRoll,max=postRoll)
			self.setTimeRange_pushButton.setStyleSheet('background-color:#99ccff')
		self.setStartBsh(preRoll)
		self.setStartFrame(preRoll)

	def setStartFrame(self,preRoll):
		nucleus = mc.ls(typ='nucleus')
		for nu in nucleus:
			mc.setAttr(nu+'.startFrame',preRoll)

	def setStartBsh(self,preRoll):
		if pm.objExists('CIN_COL_BSH'):
			bsn = pm.general.PyNode('CIN_COL_BSH')
			key = pm.listAttr(bsn.w,m=True)
			pm.cutKey(bsn,at=key,o='keys')
			pm.setKeyframe(bsn,at=key,t=preRoll,v=0)
			pm.setKeyframe(bsn,at=key,t=preRoll+10,v=1)
		elif pm.objExists('CIN_GRP_BSH'):
			bsn = pm.general.PyNode('CIN_GRP_BSH')
			key = pm.listAttr(bsn.w,m=True)
			pm.cutKey(bsn,at=key,o='keys')
			pm.setKeyframe(bsn,at=key,t=preRoll,v=0)
			pm.setKeyframe(bsn,at=key,t=preRoll+10,v=1)
		
	def switchRadioButton(self):
		if self.local_radioButton.isChecked():
			self.autoNaming_pushButton.setText('Set Project')
			self.act_listWidget.setCurrentItem(None)
			self.shot_listWidget.clear()
		elif self.server_radioButton.isChecked():
			self.autoNaming_pushButton.setText('Auto Naming')
			self.act_listWidget.setCurrentItem(None)
			self.shot_listWidget.clear()
		self.loadData()

	def switchProject(self):
		if self.localWs_radioButton.isChecked():
			workspace = self.currentWs_lineEdit.text()
			try:
				pm.mel.eval('setProject "{path}"'.format(path=workspace))
				self.updateWorkspace()
				self.listImport()
			except:pass

		elif self.serverWs_radioButton.isChecked():
			try:
				pm.mel.eval('setProject "{path}"'.format(path=core.server_ws))
				self.updateWorkspace()
				self.listImport()
			except:pass

	def checkWsRadioButton(self):
		if self.project_lineEdit.text() == self.currentWs_lineEdit.text():
			self.localWs_radioButton.setChecked(True)
		else : pass

	def listImport(self):
		self.import_listWidget.clear()
		supported_file = ['abc']
		workspace = pm.workspace(q=True,rd=True)
		dataPath = workspace+'data'
		if os.path.exists(dataPath):
			data = os.listdir(dataPath)
			for each in data:
				if each.split('.')[-1] in supported_file:
					icon = QIcon(core.icon_path+'/file.png')
					item = QListWidgetItem(each)
					item.setIcon(icon)
					self.import_listWidget.addItem(item)
				else : pass

	def oepnAnimFile(self):
		supported_file = ['ma','mb']
		workspace = self.currentWs_lineEdit.text()
		dataPath = workspace+'data'
		if os.path.exists(dataPath):
			data = os.listdir(dataPath)
			for each in data:
				if each.split('.')[-1] in supported_file:
					file = os.path.join(dataPath,each).replace('\\','/')
		mc.file(file,o=True,f=True)

	def listCamera(self):
		self.camera_listWidget.clear()
		cameras = pm.ls(typ='camera')
		basic_cam = ['backShape','bottomShape','frontShape','leftShape','perspShape','sideShape','topShape']
		for cam in cameras :
			if cam not in basic_cam :
				transform = pm.pickWalk(cam,d='up')[0]
				icon = QIcon(core.icon_path+'/camera.png')
				transform = QListWidgetItem(str(transform))
				transform.setIcon(icon)
				self.camera_listWidget.addItem(transform)

	def exportCam(self):
		pm.select(cl=True)
		workspace = self.currentWs_lineEdit.text()
		shot = workspace.split('/')[3]
		selected = self.camera_listWidget.selectedItems()
		root = []
		for each in selected:
			item = each.text()
			camera = pm.PyNode(item)
			pm.select(item,add=True)
		camera_list = mc.ls(sl=True,l=True)
		for cam in range(0,len(camera_list)):
			cmd = '-root {cam}'.format(cam=camera_list[cam])
			root.append(cmd)
		wanted = ''.join(root)
		name = '{path}/data/{shot}_main_cam.hero.abc'.format(path=workspace,shot=shot)
		self.exportSelection(wanted,name)

	def exportGeo(self):
		pm.select(cl=True)
		workspace = self.currentWs_lineEdit.text()
		shot = workspace.split('/')[3]
		geo = mc.ls(('krasue*'+'*:Geo_Grp'),('krasue*'+'*_Geo_Grp'),l=True)[0]
		geo = '-root {geo}'.format(geo=geo)
		name = '{path}/data/{shot}_anim_{char}.hero.abc'.format(path=workspace,shot=shot,char=core.character)
		self.exportSelection(geo,name)

	def exportSelection(self,item,name):
		start = pm.playbackOptions(q=True,min=True)
		end = pm.playbackOptions(q=True,max=True)
		start = int(start)
		end = int(end)
		cmd = 'AbcExport -j "-fr {start} {end} -attr adPath -attr refPath -attr project -attr assetType -attr assetName -attr look -uvWrite -ws -wv -df ogawa {item} -f {name}";'.format(start=start,end=end,item=item,name=name)
		pm.mel.eval(cmd)

	def bwdGrp(self):
		if pm.objExists('BWD_GRP'):
			grp = pm.PyNode('BWD_GRP')
		else:
			grp = pm.group(em=True,w=True,n='BWD_GRP')
			grp.t.lock()
			grp.r.lock()
			grp.s.lock()
		return grp

	def bwdImport(self):
		bwdGrp = self.bwdGrp()
		items = self.importCache()
		for item in items:
			pm.parent(item,bwdGrp)

	def countNs(self,namespace):
		ns = pm.namespaceInfo(lon=True)
		i = 0
		for each in ns:
			if namespace in each:
				i+=1
				new = namespace+str(i)
		return new

	def importCache(self):
		if mc.namespace(ex='CAM'):
			mc.namespace(rm=('CAM'),mnr=True)
		workspace = pm.workspace(q=True,rd=True)
		selected = self.import_listWidget.selectedItems()
		for each in selected:
			text = each.text()
			path = workspace+'data/'+text
			pm.system.importFile(path,ns=('CAM'))

			if pm.objExists('CAM:Geo_Grp'):
				grp = mc.ls('CAM:Geo_Grp')
				if mc.namespace(ex='ABC')==False:
					mc.namespace(add='ABC')
					mc.namespace(mv=('CAM','ABC'))
					mc.namespace(rm=('CAM'))
				elif mc.namespace(ex='ABC')==True:
					if pm.objExists('ABC:Geo_Grp') == True:
						name = self.countNs('ABC')
						mc.namespace(add=name)
						mc.namespace(mv=('CAM',name))
						mc.namespace(rm=('CAM'))
					else:
						mc.namespace(mv=('CAM','ABC'))
						mc.namespace(rm=('CAM'))

			if mc.namespace(ex='CAM')==True:
				mc.namespace(rm=('CAM'),mnr=True)

		if self.bsh_checkBox.isChecked():
			abc = mc.ls('ABC*:Geo_Grp')[-1]
			cin = mc.ls('*CIN:Geo_Grp')[0]
			pm.blendShape((abc,cin),o='world',w=[0,1],n='ABC_BSH')
			pm.setAttr(abc+'.v',0)

		item = pm.ls('ABC*:Geo_Grp',typ='transform')
		allCam = mc.ls(typ='camera')
		basic_cam = ['backShape','bottomShape','frontShape','leftShape','perspShape','sideShape','topShape']
		cams = []
		for each in allCam:
			if each not in basic_cam:
				cam = mc.pickWalk(each,d='up')[0]
				cams.append(cam)
				cam = pm.PyNode(cam)
				cam.t.lock()
				cam.r.lock()
				cam.s.lock()
		return [item,cams]

	def loadData(self):
		self.loadAct()
		self.loadShot()

	def loadAct(self):
		default = self.local_lineEdit.text()
		if self.server_radioButton.isChecked():
			self.act_listWidget.clear()
			with open(r'{path}'.format(path=core.yaml_file)) as file:
				data = yaml.load(file)
				shots = data['allShots']
				for shot in range(len(shots)):
					icon = QIcon(core.icon_path+'/folder.png')
					item = QListWidgetItem(shots[shot][0])
					item.setIcon(icon)
					self.act_listWidget.addItem(item)
		elif self.local_radioButton.isChecked():
			self.act_listWidget.clear()
			if os.path.exists(r'{default}{project}'.format(default=default,project=core.project)):
				acts = os.listdir(r'{default}{project}'.format(default=default,project=core.project))
				for act in acts:
					icon = QIcon(core.icon_path+'/folder.png')
					act = QListWidgetItem(act)
					act.setIcon(icon)
					self.act_listWidget.addItem(act)

	def loadShot(self):
		default = self.local_lineEdit.text()
		if not self.act_listWidget.currentItem():
			pass
		else:
			act = self.act_listWidget.currentItem().text()
			if self.server_radioButton.isChecked():
				self.shot_listWidget.clear()
				with open(r'{path}'.format(path=core.yaml_file)) as file:
					data = yaml.load(file)
					shots = data['allShots']
					for shot in range(len(shots)):
						if shots[shot][0] == act:
							for each in range(len(shots[shot])):
								if each == 0: pass
								else:
									if not shots[shot][each][0] == '':
										icon = QIcon(core.icon_path+'/documents.png')
										item = QListWidgetItem(shots[shot][each][0])
										item.setIcon(icon)
										self.shot_listWidget.addItem(item)
			elif self.local_radioButton.isChecked():
				self.shot_listWidget.clear()
				if os.path.exists(r'{default}{project}'.format(default=default,project=core.project)):
					acts = os.listdir(r'{default}{project}/{act}'.format(default=default,project=core.project,act=act))
					for act in acts:
						icon = QIcon(core.icon_path+'/documents.png')
						act = QListWidgetItem(act)
						act.setIcon(icon)
						self.shot_listWidget.addItem(act)
	'''autoNaming'''
	def autoNaming(self):
		if self.autoNaming_pushButton.text() == 'Auto Naming':
			local_path = self.local_lineEdit.text()
			selected_act = self.act_listWidget.currentItem().text()
			selected_shot = self.shot_listWidget.currentItem().text()
			name = '{local}/{project}/{act}/{shot}/{name}/'.format(local=local_path,project=core.project,act=selected_act,shot=selected_shot,name=core.character)
			autoNaming_path = self.createFolder(name)
			file = open(autoNaming_path+'workspace.mel','w+')
			file.close()
			dataFolder = self.createFolder(autoNaming_path+'data')
			versionFolder = self.createFolder(autoNaming_path+'version')
			cacheFolder = self.createFolder(autoNaming_path+'cache')
			self.copySetup(versionFolder)
			try :
				pm.mel.eval('setProject "{path}"'.format(path=autoNaming_path))
				self.refresh()
			except:pass
			self.copyAnimFile()
			self.setCurrentWS()
		else : pass

	def copySetup(self,path):
		files = []
		folders = os.listdir(core.sim_path)
		for folder in folders:
			files.append(core.sim_path+'/{folder}/hero/output'.format(folder=folder))
		for file in files:
			hero = file+'/{file}'.format(file=os.listdir(file)[0])
			self.copyfile(hero,path)

	def setProject(self):
		if self.autoNaming_pushButton.text() == 'Set Project':
			location = self.local_lineEdit.text()
			act = self.act_listWidget.currentItem().text()
			shot = self.shot_listWidget.currentItem().text()
			selectionPath = '{location}/{project}/{act}/{shot}/{char}'.format(location=location,project=core.project,
				act=act,shot=shot,char=core.character)
			try:
				pm.mel.eval('setProject "{path}"'.format(path=selectionPath))
				self.refresh()
				self.setCurrentWS()
			except:
				pm.error('First,You must autoNaming before use this function')
		else : pass

	def openPlayblast(self):
		workspace = self.currentWs_lineEdit.text()
		currentAct = workspace.split('/')[2]
		currentShot = workspace.split('/')[3]
		with open(r'{path}'.format(path=core.yaml_file)) as file:
			data = yaml.load(file)
			shots = data['allShots']
			for shot in range(len(shots)):
				if shots[shot][0] == currentAct:
					for each in range(len(shots[shot])):
						if shots[shot][each][0] == currentShot:
							playblast = shots[shot][each][4]
				else: pass
		os.startfile(playblast)

	def setCurrentWS(self):
		workspace = pm.workspace(q=True,rd=True)
		self.currentWs_lineEdit.setText(workspace)

	def buildSender(self):
		workspace = pm.workspace(q=True,rd=True)
		act = workspace.split('/')[2]
		shot = workspace.split('/')[3]
		'''S:/2022/05_Atama/Maya/cache/01_CFX/ATM_CG_S09toS11_V2'''
		sentPath = core.sent_path+'/{act}/{shot}'.format(act=act,shot=shot)
		'''S:/2022/05_Atama/Maya/cache/01_CFX/ATM_CG_S09toS11_V2/krasue_Hair'''
		self.createFolder(sentPath+'/{char}_Hair'.format(char=core.character))
		'''S:/2022/05_Atama/Maya/cache/01_CFX/ATM_CG_S09toS11_V2/krasue_Tech'''
		self.createFolder(sentPath+'/{char}_Tech'.format(char=core.character))
		'''S:/2022/05_Atama/Maya/cache/01_CFX/ATM_CG_S09toS11_V2/outputMedia'''
		self.createFolder(sentPath+'/outputMedia')

	def cacheYeti(self,yeti):
		pm.mel.eval('pgYetiEnsurePluginLoaded()')
		pm.select(yeti,r=True)
		yeti = yeti.split('_YetiNode')[0]
		'''D:/Atama/ATM_CG_S09toS11_V2/krasue'''
		workspace = pm.workspace(q=True,rd=True)
		act = workspace.split('/')[2]
		shot = workspace.split('/')[3]
		'''D:/Atama/ATM_CG_S09toS11_V2/krasue/output/Yeti'''
		location = workspace+'output/Yeti'
		'''D:/Atama/ATM_CG_S09toS11_V2/krasue/output/Yeti/krasue_hair'''
		cacheFolder = self.createFolder(location+'/{char}_{yeti}'.format(char=core.character,yeti=yeti))
		'''D:/Atama/ATM_CG_S09toS11_V2/krasue/cache/krasue/krasue_hair/ATM_CG_S09toS11_V2_krasue_krasue_hair.hero.%04d.fur'''
		outputName = '{cacheFolder}/{act}_{shot}_{char}_{yeti}.hero.%04d.fur'.format(cacheFolder=cacheFolder,act=act,shot=shot,char=core.character,yeti=yeti)
		start = int(self.yetiStart_lineEdit.text())
		end = int(self.yetiEnd_lineEdit.text())
		nos = int(self.yetiNos_lineEdit.text())
		pm.mel.eval('pgYetiCommand -writeCache "%s"'%outputName+' -range %s %s -samples %s'%(start,end,nos))

	def yetiTimeRange(self):
		workspace = pm.workspace(q=True,rd=True)
		project = workspace.split('/')[1]
		if project == core.project:
			time = self.updateTimeRange()
			self.yetiStart_lineEdit.setText(str(time[1]-5))
			self.yetiEnd_lineEdit.setText(str(time[2]+5))
			self.yetiNos_lineEdit.setText(str(10))
		else:pass

	def cacheYetiSelected(self,*args):
		selected = self.yeti_listWidget.selectedItems()
		for each in selected:
			each = each.text()
			self.cacheYeti(each)

	def cacheYetiAll(self,*args):
		self.yeti_listWidget.selectAll()
		selected = self.yeti_listWidget.selectedItems()
		for each in selected:
			each = each.text()
			self.cacheYeti(each)

	def copyYeti(self):
		self.buildSender()
		workspace = pm.workspace(q=True,rd=True)
		sentPath = self.sentPath_label.text()+'/{char}_Hair'.format(char=core.character)
		cacheFolder = workspace+'output/Yeti/{char}_Hair'.format(char=core.character)
		self.copyFolder(cacheFolder,sentPath,None,None)

	def copyTech(self):
		self.buildSender()
		workspace = pm.workspace(q=True,rd=True)
		sentPath = self.sentPath_label.text()+'/{char}_Tech'.format(char=core.character)
		cacheFolder = workspace+'output/Yeti/{char}_tech/hero'.format(char=core.character)
		self.createFolder(sentPath)
		for item in os.listdir(cacheFolder):
		    cache = os.path.join(cacheFolder,item).replace('\\','/')
		    sent = os.path.join(sentPath,item).replace('\\','/')
		    shutil.copy2(cache,sent)

	def copyPb(self):
		self.buildSender()
		item = QListWidgetItem(self.dragDrop_widget.currentItem())
		path = item.text()
		want_path = self.sentPath_label.text()+'/outputMedia'
		result = self.pb_label.text()
		self.copyfile(path,result)
		self.updateNamePb()

	def checkVersion(self,path):
		if os.path.exists(path):
			if not os.listdir(path)==[]:
				item = os.listdir(path)[-1]
				version = '%03d'%(int(item.split('.')[-2].split('v')[1])+1)
			else:
				version = '%03d'%(int(1))
		return version

	def openSentPath(self):
		path = self.sentPath_label.text()
		self.openFolder(path)

	def updateSentPath(self):
		workspace = pm.workspace(q=True,rd=True)
		project = workspace.split('/')[1]
		act = workspace.split('/')[2]
		shot = workspace.split('/')[3]
		if project == core.project:
			sentPath = core.sent_path+'/{act}/{shot}'.format(act=act,shot=shot)
			self.sentPath_label.setText(sentPath)
		else:pass

	def updateNamePb(self):
		workspace = pm.workspace(q=True,rd=True)
		act = workspace.split('/')[2]
		shot = workspace.split('/')[3]
		name = core.getUserName()
		path = self.sentPath_label.text()+'/outputMedia'
		self.createFolder(path)
		version = self.checkVersion(path)
		name = '{path}/{act}_{shot}_sim_{name}.v{version}.mov'.format(path=path,act=act,shot=shot,name=name,version=version)
		self.pb_label.setText(name)

	def refreshYeti(self):
		self.yeti_listWidget.clear()
		yetis = pm.listRelatives(pm.ls(typ='pgYetiMaya'),p=True)
		for yeti in yetis:
			icon = QIcon(core.icon_path+'/yeti.png')
			yeti = QListWidgetItem(str(yeti))
			yeti.setIcon(icon)
			self.yeti_listWidget.addItem(yeti)

	def defaultYeti(self):
		workspace = pm.workspace(q=True,rd=True)
		path = workspace+'output/Yeti'
		self.yetiPath_lineEdit.setText(path)

	def yetiPath(self):
		default = self.yetiPath_lineEdit.text()
		folder = pm.fileDialog2(cap='Select Folder',fm=3,okc='Select',dir=default)[0]
		self.yetiPath_lineEdit.setText(folder)

	def openYetiPath(self):
		path = self.yetiPath_lineEdit.text()
		self.openFolder(path)





