import sys

from PySide2 import QtCore,QtWidgets
from shiboken2 import wrapInstance

import app
reload(app)

def mayaRun():
	import maya.OpenMayaUI as omui
	global AtamaProjectApp
	try:
		AtamaProjectApp.close()
	except:
		pass

	ptr = omui.MQtUtil.mainWindow()
	AtamaProjectApp = app.AtamaProject(parent=wrapInstance(long(ptr),QtWidgets.QWidget))
	AtamaProjectApp.show()
	
	return AtamaProjectApp