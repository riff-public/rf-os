import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel
import json
import os
import shutil

class SamAttrYeti ( object ) :
	def __init__ ( self ) :
		self.presetPath = self.getPreferencePath ()
		self.attr = [ 'subdDisplay' , 'featherDisplayStyle' , 'viewportDensity' ,
		'viewportLength' , 'viewportWidth' , 'color' ]
		mel.eval ( 'pgYetiEnsurePluginLoaded()' )

	def getPreferencePath ( self , * args ) :
		myDoc = os.path.expanduser ( '~' )
		myDoc = myDoc.replace ( '\\' , '/' )
		prefPath = myDoc + '/samAttrYeti'
		if not os.path.exists ( prefPath ) :
			os.makedirs ( prefPath )
		return prefPath

	def getAttr ( self , target ) :
		#version = 0
		#path = self.presetPath + '/' + str ( target ) + '.' + str ( version ) + '.json'
		folder = pm.textField ( self.file_txt , q = True , tx = True )
		path = self.presetPath + '/' + folder
		if not os.path.exists ( path ) :
			os.makedirs ( path )
		file = path + '/' + str ( target ) + '.json'
		attrDict = {}
		valueDict = {}
		node = target.getShape ()
		for attr in range ( len ( self.attr ) ) :
			value = node.getAttr ( self.attr [ attr ] )
			valueDict [ self.attr [ attr ] ] = value
		attrDict [ ( str ( node ) ) ] = valueDict 
		color_json = json.dumps ( attrDict )
		#while os.path.exists ( path ) :
		#	path = self.presetPath + '/' + str ( target ) + '.' + str ( version ) + '.json'
		#	version += 1
		with open ( file , 'w' ) as outfile :
			json.dump ( attrDict , outfile )

	def getAll ( self , *args ) :
		pick = pm.ls ( sl = True )
		for each in pick :
			self.getAttr ( target = each )

	def closeFolderUI ( self , *args ) :
		if pm.window ( 'samAddColorYeti' , exists = True ) :
			pm.deleteUI ( 'samAddColorYeti' )
		else : pass

	def createFolder_cmd ( self , *args ) :
		self.getAll ()
		self.closeFolderUI ()
		self.listDict ()
		self.listYeti ()

	def folderUI ( self , *args ) :
		w = 200.00
		rgb = 255.00
		if pm.window ( 'samAddColorYeti' , exists = True ) :
			pm.deleteUI ( 'samAddColorYeti' )
		else : pass
		window = pm.window ( 'samAddColorYeti' , t = 'Add' , tb = True , mnb = True , mxb = True , s = False , rtf = True )
		pm.window ( 'samAddColorYeti' , e = True , w = w , h = 10 )
		with window :
			with pm.rowColumnLayout ( nc = 1 ) :
				self.file_txt = pm.textField ( fn = 'obliqueLabelFont' , w = w )
				pm.button ( l = 'ADD' , c = self.createFolder_cmd , bgc = ( 188 / rgb , 36 / rgb , 60 / rgb ) )

	def listDict ( self , *args , **kwargs ) :
		fileLists = os.listdir ( self.presetPath )
		for file in fileLists :
			if pm.menuItem ( file , ex = True ) :
				pass
			else :
				pm.menuItem ( file , l = file , p = self.optionMenu )
	
	def removeDict ( self , *args ) :
		optionMenu = pm.optionMenu ( 'optMenu' , q = True , v = True )
		try :
			os.remove ( self.presetPath + '/' + optionMenu )
			pm.deleteUI ( optionMenu )
			self.listDict ()
			self.listYeti ()
		except :
			shutil.rmtree ( self.presetPath + '/' + optionMenu )
			pm.deleteUI ( optionMenu )
			self.listDict ()
			self.listYeti ()

	def loadAll ( self , *args ) :
		optionMenu = pm.optionMenu ( 'optMenu' , q = True , v = True )
		path = self.presetPath + '/' + optionMenu
		fileLists = os.listdir ( path )
		for file in fileLists :
			data = open ( path + '/' + file )
			nodeData = json.load ( data )
			for node in nodeData :
				if pm.objExists ( node ) :
					for attr in range ( len ( self.attr ) ) :
						value = nodeData [ node ][ self.attr [ attr ] ]
						pm.setAttr ( node + '.' + self.attr [ attr ] , value )
				else :
					mc.warning ( str ( node ) + ' >> Status : Not Found' )

	def listYeti ( self , *args ) :
		optionMenu = pm.optionMenu ( 'optMenu' , q = True , v = True )
		if not optionMenu == None :
			path = self.presetPath + '/' + optionMenu
			data = os.listdir ( path )
			pm.deleteUI ( self.frameLayout )
			self.frameLayout = pm.frameLayout ( 'Import Yeti' , cl = True , cll = True , p = self.window , cc = self.resize , bgc = ( 72 / 255.00 , 81 / 255.00 , 103 / 255.00 ) )
			pm.window ( 'samYetiAsset' , e = True , h = 10 )
			pm.text ( l = '' , h = 3 )
			for each in data :
				split = each.split ( '.' ) [ 0 ]
				if pm.checkBox ( split , ex = True ) :
					pass
				else :
					pm.checkBox ( split , l = split , p = self.frameLayout )
			pm.button ( l = 'Build YetiNode' , c = self.buildYetiNode , bgc = ( 149 / 255.00 , 222 / 255.00 , 227 / 255.00 ) )
		else :
			pm.deleteUI ( self.frameLayout )
			self.frameLayout = pm.frameLayout ( 'Import Yeti' , cl = True , cll = True , p = self.window , cc = self.resize , bgc = ( 72 / 255.00 , 81 / 255.00 , 103 / 255.00 ) )
			pm.window ( 'samYetiAsset' , e = True , h = 10 )

	def resize ( self , *args ) :
		pm.window ( 'samYetiAsset' , e = True , h = 10 )

	def buildYetiNode ( self , *args ) :
		optionMenu = pm.optionMenu ( 'optMenu' , q = True , v = True )
		path = self.presetPath + '/' + optionMenu
		fileLists = os.listdir ( path )
		nodeList = []
		for each in fileLists :
			data = open ( path + '/' + each )
			nodeData = json.load ( data )
			split = each.split ( '.' ) [ 0 ]
			check = pm.checkBox ( split , q = True , v = True )	
			if check == True :
				if not pm.objExists ( split ) :
					pgYeti = mel.eval ( 'pgYetiCreate()' )
					yeti = pm.listRelatives ( pgYeti , p = True ) 
					pm.rename ( yeti , split )
					yetiShp = pm.listRelatives ( yeti )
					nodeType = pm.nodeType ( yetiShp )
					for node in yetiShp :
						sp = node.split ( nodeType )
						sp = sp [ 0 ]
						if pm.objExists ( sp ) :
							for attr in range ( len ( self.attr ) ) :
								value = nodeData [ sp ][ self.attr [ attr ] ]
								pm.setAttr ( sp + '.' + self.attr [ attr ] , value )
						else : pass

	def mainUI ( self , *args ) :
		w = 250.00
		rgb = 255.00
		if pm.window ( 'samYetiAsset' , exists = True ) :
			pm.deleteUI ( 'samYetiAsset' )
		else : pass
		self.window = pm.window ( 'samYetiAsset' ,
			t = 'samTool : yetiAsset' , tb = True , mnb = True , mxb = True , s = True , rtf = True )
		pm.window ( 'samYetiAsset' , e = True , w = w , h = 10 )
		with self.window :
			with pm.rowColumnLayout ( nc = 1 ) :
				pm.text ( l = '' , h = 10 )
				with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , w / 1.2 ) , ( 2 , w / 6 ) , ( 3 , w / 6 ) ] ) :
					self.optionMenu = pm.optionMenu ( 'optMenu' , cc = self.listYeti )
					pm.button ( l = '+' , c = self.folderUI , bgc = ( 121 / rgb , 199 / rgb , 83 / rgb ) , al = 'center' )
					pm.button ( l = '-' , c = self.removeDict , bgc = ( 188 / rgb , 36 / rgb , 60 / rgb ) , al = 'center' )
				pm.text ( h = 5 , l = '')
				pm.button ( l = 'ASSIGN COLOR' , c = self.loadAll , bgc = ( 239 / rgb , 192 / rgb , 80 / rgb ) )
			self.frameLayout = pm.frameLayout ( 'Import Yeti' , cl = True , cll = True ) 

		self.listDict ()
		self.listYeti ()

SamAttrYeti().mainUI()