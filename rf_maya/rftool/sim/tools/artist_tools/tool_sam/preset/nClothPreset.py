import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel
import os , ast

class Demo ( object ) :
	def __init__ ( self ) :
		self.presetPath = self.getPreferencePath ()

	def getPreferencePath ( self ) :
		myDoc = os.path.expanduser ( '~' )
		myDoc = myDoc.replace ( '\\' , '/' )
		prefPath = myDoc + '/samSavePresetData/nCloth'
		if not os.path.exists ( prefPath ) :
			os.makedirs ( prefPath )
		return prefPath

	def updateNClothList ( self , *args ) :
		nCloth_list = pm.ls ( typ = 'nCloth' )
		textScroll_nCloth_list = pm.textScrollList ( self.nCloth_txtScrll , q = True , ai = True )
		for nCloth in textScroll_nCloth_list :
			if not pm.objExists ( nCloth ) :
				pm.textScrollList ( self.nCloth_txtScrll , e = True , ri = nCloth )
		for nCloth in nCloth_list :
			if nCloth not in textScroll_nCloth_list :
				pm.textScrollList ( self.nCloth_txtScrll , e = True , a = nCloth )

	def savePresets ( self , target ) :
		file = pm.about ( f = True )
		nodeType = pm.nodeType ( target )
		result = '// requires maya "%s";\n' % file 
		result = result + 'startAttrPreset( "%s" );\n' %nodeType 
		result = result + 'blendAttrStr "iconName" "";\n'
		atrs = pm.listAttr ( target , m = True , r = True , w = True , v = True , hd = True , s = True )
		for attr in atrs :
			obj = target + "." + attr
			if pm.objExists ( obj ) :
				value = pm.getAttr ( obj )
				if value == True :
					value = 1
				elif value == False :
					value = 0
				result = result + 'blendAttr "%s" %s;\n' % ( attr , value )
		result = result + 'endAttrPreset();\n'
		print result
		file = open ( self.presetPath + '/' + target + '.mel' , 'w' )
		file.write ( result )
		file.close()
		self.listGetPreset ()

	def saveAttr ( self , *args ) :
		selected = pm.textScrollList ( self.nCloth_txtScrll , q = True , si = True )
		shape = pm.ls ( selected )
		for shp in shape :
			self.savePresets ( target = shp )

	def loadPresets ( self , target ) :
		blendValue = pm.floatField ( self.blendValue_floatField , q = True , v = True )
		name = pm.textScrollList ( self.preview_txtScrll , q = True , si = True ) [ 0 ]
		name = self.presetPath + '/' + name
		print target , name , blendValue
		pm.mel.eval ( 'applyAttrPreset %s \"%s\" %4.1f' % ( target , name, blendValue ) )

	def loadAttr ( self , *args ) :
		selected = pm.textScrollList ( self.nCloth_txtScrll , q = True , si = True )
		shape = pm.ls ( selected )
		for shp in shape :
			self.loadPresets ( target = shp )

	def listGetPreset ( self , *args ) :
		fileLists = os.listdir ( self.presetPath )
		previewList = pm.textScrollList ( self.preview_txtScrll , e = True , ra = True )
		for files in fileLists :
			pm.textScrollList ( self.preview_txtScrll , e = True , a = files )

	def deletePresetList ( self , *args ) :
		fileLists = pm.textScrollList ( self.preview_txtScrll , q = True , si = True )
		if fileLists :
			for files in fileLists :
				os.remove ( self.presetPath + '/' + fileLists [ 0 ] )
				self.listGetPreset ( self.presetPath )
		else :
			mc.warning ( 'NO SELECTED' )

	def openDatas ( self , *args ) :
		fileLists = pm.textScrollList ( self.preview_txtScrll , q = True , si = True )
		os.startfile ( self.presetPath + '/' + fileLists [ 0 ] )

	def convertValue ( self , *args ) :
		value = pm.floatField ( self.blendValue_floatField , q = True , v = True )
		pm.text ( self.blendValue_text , e = True , l = 'Blend :' + str ( value * 100 ) + ' %' )

	def insert ( self , *args ) :
		w = 300.00
		self.nCloth_txtScrll = pm.textScrollList ( 'nCloth_txtScrll' , w = w , 
			h = 80 , sc = self.updateNClothList , ams = True )
		self.updateNClothList ()
		pm.button ( l = 'Refresh', c = self.updateNClothList , bgc = ( 1 , 0.8745 , 0.7294 ) )
		pm.separator ( vis = True , h = 7.5 )	
		with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w / 2 ) , ( 2 , w / 2 ) ] ) :
			self.blendValue_floatField = pm.floatField ( max = 1 , min = 0 , pre = 2 , v = 1 , cc = self.convertValue )
			self.blendValue_text = pm.text ( l = 'Blend : 100.00 %' )
		with pm.rowColumnLayout ( nc = 1 ) :
			pm.text ( l = '' , h = 1 )
			self.preview_txtScrll = pm.textScrollList ( 'previewLists' , h = 80 , dcc = self.openDatas , w = w )
		with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , w / 3 ) , ( 2 , w / 3 ) , ( 3 , w / 3 ) ] ) :
			pm.button ( l = 'Save' , c = self.saveAttr , bgc = ( 0.7294 , 1 , 0.7882 ) )
			pm.button ( l = 'Load' , c = self.loadAttr , bgc = ( 0.7294 , 1 , 1 ) )
			pm.button ( l = 'Delete' , c = self.deletePresetList  , bgc = ( 1 , 0.7019 , 0.7294 ) )
		with pm.rowColumnLayout ( nc = 1 ) :
			pm.button ( l = 'Refresh', c = self.listGetPreset , bgc = ( 1 , 0.8745 , 0.7294 ) , w = w )
		self.listGetPreset()