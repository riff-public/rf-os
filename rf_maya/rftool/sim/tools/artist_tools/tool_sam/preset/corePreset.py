import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel
import os , ast , sys
import json

class Demo ( object ) :

	def __init__ ( self ) :
		pass

	def run ( self , *args ) :

		l = 'left'

		mp = os.path.abspath( __file__ ).split( '\corePreset' ) [ 0 ]
		print 'Location Script >> ' + mp
		if not mp in sys.path :
			sys.path.insert ( 0 , mp )

		import nClothPreset as nCloth
		reload ( nCloth )
		nClothUI = nCloth.Demo ()

		import nHairPreset as nHair
		reload ( nHair )
		nHairUI = nHair.Demo ()

		import nucleusPreset as nucleus
		reload ( nucleus )
		nucleusUI = nucleus.Demo ()

		import nRigidPreset as nRigid
		reload ( nRigid)
		nRigidUI = nRigid.Demo ()

		w = 300.00

		if pm.window ( 'samPreset' , exists = True ) :
			pm.deleteUI ( 'samPreset' )
		else : pass

		window = pm.window ( 'samPreset' , t = 'SamTool : Preset' , tb = True , mnb = True , mxb = True , s = False , rtf = True )
		pm.window ( 'samPreset' , e = True , w = w , h = 10 )
		with window :

			with pm.rowColumnLayout ( nc = 1 , adj = True ) :
				tabLayout = pm.tabLayout ( imh = 5 , imw = 5 )
				with tabLayout :

					nCloth_rowColumnLayout = pm.rowColumnLayout ( nc = 1 , w = w )
					with nCloth_rowColumnLayout :
						nClothUI.insert ( )

					nHair_rowColumnLayout = pm.rowColumnLayout ( nc = 1 , w = w )
					with nHair_rowColumnLayout :
						nHairUI.insert ( )

					nucleus_rowColumnLayout = pm.rowColumnLayout ( nc = 1 , w = w )
					with nucleus_rowColumnLayout :
						nucleusUI.insert ( )

					nRigid_rowColumnLayout = pm.rowColumnLayout ( nc = 1 , w = w )
					with nRigid_rowColumnLayout :
						nRigidUI.insert ( )

				pm.tabLayout ( tabLayout , e = True , tl = ( ( nCloth_rowColumnLayout , 'nCloth' ) ,
					( nHair_rowColumnLayout , 'nHair' ) , ( nucleus_rowColumnLayout , 'nucleus' ) ,
					( nRigid_rowColumnLayout , 'nRigid' ) ) )

Demo().run()