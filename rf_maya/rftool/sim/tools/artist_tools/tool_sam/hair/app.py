import os
import shutil
from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

import maya.cmds as mc
import pymel.core as pm

import core
reload(core) 
import untitled
reload(untitled)

class AW(QMainWindow,untitled.Ui_MainWindow):
	def __init__(self,parent):
		super(AW,self).__init__(parent)

		# ------- setUp UI ---------
		self.setupUi(self)
		self.hairSystem_update()

		# ------- value lineEdit dict ---------
		self.all_value_lineEdit = [self.stretchResistanceValue_lineEdit,self.compressionResistanceValue_lineEdit,self.bendResistanceValue_lineEdit,
		self.twistResistanceValue_lineEdit,self.extraBendLinksValue_lineEdit,self.restLengthScaleValue_lineEdit,self.startCurveAttractValue_lineEdit,
		self.attractionDampValue_lineEdit,self.massValue_lineEdit,self.dragValue_lineEdit,self.tangentialDragValue_lineEdit,self.motionDragValue_lineEdit,
		self.dampValue_lineEdit,self.stretchDampValue_lineEdit,self.dynamicsWeightValue_lineEdit,self.turbulenceStrengthValue_lineEdit,
		self.turbulenceFrequencyValue_lineEdit,self.turbulenceSpeedValue_lineEdit]
		# ------- checkBox dict ---------
		self.attr_chkBox = [self.noStretch_checkBox,self.ignoreSolverGravity_checkBox,self.ignoreSolverWind_checkBox,self.disableFollicleAnim_checkBox]
		# ------- offset lineEdit dict ---------
		self.all_offset_lineEdit = [self.stretchResistanceOffset_lineEdit,self.compressionResistanceOffset_lineEdit,self.bendResistanceOffset_lineEdit,
		self.twistResistanceOffset_lineEdit,self.extraBendLinksOffset_lineEdit,self.restLengthScaleOffset_lineEdit,self.startCurveAttractOffset_lineEdit,
		self.attractionDampOffset_lineEdit,self.massOffset_lineEdit,self.dragOffset_lineEdit,self.tangentialDragOffset_lineEdit,self.motionDragOffset_lineEdit,
		self.dampOffset_lineEdit,self.stretchDampOffset_lineEdit,self.dynamicsWeightOffset_lineEdit,self.turbulenceStrengthOffset_lineEdit,
		self.turbulenceFrequencyOffset_lineEdit,self.turbulenceSpeedOffset_lineEdit]
		# ------- value comboBox dict ---------
		self.all_value_comboBox = [self.stretchResistanceValue_comboBox,self.compressionResistanceValue_comboBox,self.bendResistanceValue_comboBox,
		self.twistResistanceValue_comboBox,self.extraBendLinksValue_comboBox,self.restLengthScaleValue_comboBox,self.startCurveAttractValue_comboBox,
		self.attractionDampValue_comboBox,self.massValue_comboBox,self.dragValue_comboBox,self.tangentialDragValue_comboBox,self.motionDragValue_comboBox,
		self.dampValue_comboBox,self.stretchDampValue_comboBox,self.dynamicsWeightValue_comboBox,self.turbulenceStrengthValue_comboBox,
		self.turbulenceFrequencyValue_comboBox,self.turbulenceSpeedValue_comboBox]
		# ------- offset comboBox dict ---------
		self.all_offset_comboBox = [self.stretchResistanceOffset_comboBox,self.compressionResistanceOffset_comboBox,self.bendResistanceOffset_comboBox,
		self.twistResistanceOffset_comboBox,self.extraBendLinksOffset_comboBox,self.restLengthScaleOffset_comboBox,self.startCurveAttractOffset_comboBox,
		self.attractionDampOffset_comboBox,self.massOffset_comboBox,self.dragOffset_comboBox,self.tangentialDragOffset_comboBox,self.motionDragOffset_comboBox,
		self.dampOffset_comboBox,self.stretchDampOffset_comboBox,self.dynamicsWeightOffset_comboBox,self.turbulenceStrengthOffset_comboBox,
		self.turbulenceFrequencyOffset_comboBox,self.turbulenceSpeedOffset_comboBox]

		# ------- setValidator ---------
		# 1. setValue_lineEdit
		onlyFloat = QDoubleValidator()
		onlyFloat.Notation(QDoubleValidator.StandardNotation)
		onlyFloat.setRange(0,1000000,3)
		for all_val_line in self.all_value_lineEdit:
			all_val_line.setValidator(onlyFloat)
		for all_off_line in self.all_offset_lineEdit:
			all_off_line.setValidator(onlyFloat)

		# ------- connect ---------
		self.hairSystem_listWidget.itemChanged.connect(self.hairSystem_status_changed)
		self.hairSystem_listWidget.itemSelectionChanged.connect(self.hairSystem_update_value_lineEdit)
		self.enable_pushButton.clicked.connect(self.hairSystem_status_enable)
		self.disable_pushButton.clicked.connect(self.hairSystem_status_disable)
		self.refresh_pushButton.clicked.connect(self.hairSystem_update)

		# ------- set styleSheet ---------
		self.enable_pushButton.setStyleSheet("background-color:#336600")
		self.disable_pushButton.setStyleSheet("background-color:#660000")
		self.refresh_pushButton.setStyleSheet("background-color:#009999")
		self.copyGraph_pushButton.setStyleSheet("background-color:#999900")

		# ------- selection Mode ---------
		self.hairSystem_listWidget.setSelectionMode(QAbstractItemView.ExtendedSelection)

		self.hairSystem_listWidget.selectAll()
		self.hairSystem_update_offset_lineEdit()

		# ------- stretchResistance ---------
		self.all_value_lineEdit[0].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[0],
			attr = core.attr_list[0],comboBox = self.all_value_comboBox[0],comboBox2 = self.all_offset_comboBox[0],
			line2 = self.all_offset_lineEdit[0]))
		self.all_value_comboBox[0].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[0],
			attr = core.attr_list[0],comboBox = self.all_value_comboBox[0],comboBox2 = self.all_offset_comboBox[0],
			line2 = self.all_offset_lineEdit[0]))
		self.all_offset_lineEdit[0].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[0],
			attr = core.attr_list[0],comboBox = self.all_value_comboBox[0],comboBox2 = self.all_offset_comboBox[0],
			line2 = self.all_offset_lineEdit[0]))
		# ------- compressionResistance ---------
		self.all_value_lineEdit[1].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[1],
			attr = core.attr_list[1],comboBox = self.all_value_comboBox[1],comboBox2 = self.all_offset_comboBox[1],
			line2 = self.all_offset_lineEdit[1]))
		self.all_value_comboBox[1].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[1],
			attr = core.attr_list[1],comboBox = self.all_value_comboBox[1],comboBox2 = self.all_offset_comboBox[1],
			line2 = self.all_offset_lineEdit[1]))
		self.all_offset_lineEdit[1].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[1],
			attr = core.attr_list[1],comboBox = self.all_value_comboBox[1],comboBox2 = self.all_offset_comboBox[1],
			line2 = self.all_offset_lineEdit[1]))
		# ------- bendResistance ---------
		self.all_value_lineEdit[2].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[2],
			attr = core.attr_list[2],comboBox = self.all_value_comboBox[2],comboBox2 = self.all_offset_comboBox[2],
			line2 = self.all_offset_lineEdit[2]))
		self.all_value_comboBox[2].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[2],
			attr = core.attr_list[2],comboBox = self.all_value_comboBox[2],comboBox2 = self.all_offset_comboBox[2],
			line2 = self.all_offset_lineEdit[2]))
		self.all_offset_lineEdit[2].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[2],
			attr = core.attr_list[2],comboBox = self.all_value_comboBox[2],comboBox2 = self.all_offset_comboBox[2],
			line2 = self.all_offset_lineEdit[2]))
		# ------- twistResistance ---------
		self.all_value_lineEdit[3].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[3],
			attr = core.attr_list[3],comboBox = self.all_value_comboBox[3],comboBox2 = self.all_offset_comboBox[3],
			line2 = self.all_offset_lineEdit[3]))
		self.all_value_comboBox[3].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[3],
			attr = core.attr_list[3],comboBox = self.all_value_comboBox[3],comboBox2 = self.all_offset_comboBox[3],
			line2 = self.all_offset_lineEdit[3]))
		self.all_offset_lineEdit[3].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[3],
			attr = core.attr_list[3],comboBox = self.all_value_comboBox[3],comboBox2 = self.all_offset_comboBox[3],
			line2 = self.all_offset_lineEdit[3]))
		# ------- extraBendLinks ---------
		self.all_value_lineEdit[4].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[4],
			attr = core.attr_list[4],comboBox = self.all_value_comboBox[4],comboBox2 = self.all_offset_comboBox[4],
			line2 = self.all_offset_lineEdit[4]))
		self.all_value_comboBox[4].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[4],
			attr = core.attr_list[4],comboBox = self.all_value_comboBox[4],comboBox2 = self.all_offset_comboBox[4],
			line2 = self.all_offset_lineEdit[4]))
		self.all_offset_lineEdit[4].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[4],
			attr = core.attr_list[4],comboBox = self.all_value_comboBox[4],comboBox2 = self.all_offset_comboBox[4],
			line2 = self.all_offset_lineEdit[4]))
		# ------- restLengthScale ---------
		self.all_value_lineEdit[5].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[5],
			attr = core.attr_list[5],comboBox = self.all_value_comboBox[5],comboBox2 = self.all_offset_comboBox[5],
			line2 = self.all_offset_lineEdit[5]))
		self.all_value_comboBox[5].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[5],
			attr = core.attr_list[5],comboBox = self.all_value_comboBox[5],comboBox2 = self.all_offset_comboBox[5],
			line2 = self.all_offset_lineEdit[5]))
		self.all_offset_lineEdit[5].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[5],
			attr = core.attr_list[5],comboBox = self.all_value_comboBox[5],comboBox2 = self.all_offset_comboBox[5],
			line2 = self.all_offset_lineEdit[5]))
		# ------- startCurveAttract ---------
		self.all_value_lineEdit[6].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[6],
			attr = core.attr_list[6],comboBox = self.all_value_comboBox[6],comboBox2 = self.all_offset_comboBox[6],
			line2 = self.all_offset_lineEdit[6]))
		self.all_value_comboBox[6].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[6],
			attr = core.attr_list[6],comboBox = self.all_value_comboBox[6],comboBox2 = self.all_offset_comboBox[6],
			line2 = self.all_offset_lineEdit[6]))
		self.all_offset_lineEdit[6].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[6],
			attr = core.attr_list[6],comboBox = self.all_value_comboBox[6],comboBox2 = self.all_offset_comboBox[6],
			line2 = self.all_offset_lineEdit[6]))
		# ------- attractionDamp ---------
		self.all_value_lineEdit[7].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[7],
			attr = core.attr_list[7],comboBox = self.all_value_comboBox[7],comboBox2 = self.all_offset_comboBox[7],
			line2 = self.all_offset_lineEdit[7]))
		self.all_value_comboBox[7].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[7],
			attr = core.attr_list[7],comboBox = self.all_value_comboBox[7],comboBox2 = self.all_offset_comboBox[7],
			line2 = self.all_offset_lineEdit[7]))
		self.all_offset_lineEdit[7].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[7],
			attr = core.attr_list[7],comboBox = self.all_value_comboBox[7],comboBox2 = self.all_offset_comboBox[7],
			line2 = self.all_offset_lineEdit[7]))
		# ------- mass ---------
		self.all_value_lineEdit[8].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[8],
			attr = core.attr_list[8],comboBox = self.all_value_comboBox[8],comboBox2 = self.all_offset_comboBox[8],
			line2 = self.all_offset_lineEdit[8]))
		self.all_value_comboBox[8].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[8],
			attr = core.attr_list[8],comboBox = self.all_value_comboBox[8],comboBox2 = self.all_offset_comboBox[8],
			line2 = self.all_offset_lineEdit[8]))
		self.all_offset_lineEdit[8].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[8],
			attr = core.attr_list[8],comboBox = self.all_value_comboBox[8],comboBox2 = self.all_offset_comboBox[8],
			line2 = self.all_offset_lineEdit[8]))
		# ------- drag ---------
		self.all_value_lineEdit[9].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[9],
			attr = core.attr_list[9],comboBox = self.all_value_comboBox[9],comboBox2 = self.all_offset_comboBox[9],
			line2 = self.all_offset_lineEdit[9]))
		self.all_value_comboBox[9].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[9],
			attr = core.attr_list[9],comboBox = self.all_value_comboBox[9],comboBox2 = self.all_offset_comboBox[9],
			line2 = self.all_offset_lineEdit[9]))
		self.all_offset_lineEdit[9].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[9],
			attr = core.attr_list[9],comboBox = self.all_value_comboBox[9],comboBox2 = self.all_offset_comboBox[9],
			line2 = self.all_offset_lineEdit[9]))
		# ------- tangentialDrag ---------
		self.all_value_lineEdit[10].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[10],
			attr = core.attr_list[10],comboBox = self.all_value_comboBox[10],comboBox2 = self.all_offset_comboBox[10],
			line2 = self.all_offset_lineEdit[10]))
		self.all_value_comboBox[10].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[10],
			attr = core.attr_list[10],comboBox = self.all_value_comboBox[10],comboBox2 = self.all_offset_comboBox[10],
			line2 = self.all_offset_lineEdit[10]))
		self.all_offset_lineEdit[10].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[10],
			attr = core.attr_list[10],comboBox = self.all_value_comboBox[10],comboBox2 = self.all_offset_comboBox[10],
			line2 = self.all_offset_lineEdit[10]))
		# ------- motionDrag ---------
		self.all_value_lineEdit[11].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[11],
			attr = core.attr_list[11],comboBox = self.all_value_comboBox[11],comboBox2 = self.all_offset_comboBox[11],
			line2 = self.all_offset_lineEdit[11]))
		self.all_value_comboBox[11].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[11],
			attr = core.attr_list[11],comboBox = self.all_value_comboBox[11],comboBox2 = self.all_offset_comboBox[11],
			line2 = self.all_offset_lineEdit[11]))
		self.all_offset_lineEdit[11].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[11],
			attr = core.attr_list[11],comboBox = self.all_value_comboBox[11],comboBox2 = self.all_offset_comboBox[11],
			line2 = self.all_offset_lineEdit[11]))
		# ------- damp ---------
		self.all_value_lineEdit[12].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[12],
			attr = core.attr_list[12],comboBox = self.all_value_comboBox[12],comboBox2 = self.all_offset_comboBox[12],
			line2 = self.all_offset_lineEdit[12]))
		self.all_value_comboBox[12].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[12],
			attr = core.attr_list[12],comboBox = self.all_value_comboBox[12],comboBox2 = self.all_offset_comboBox[12],
			line2 = self.all_offset_lineEdit[12]))
		self.all_offset_lineEdit[12].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[12],
			attr = core.attr_list[12],comboBox = self.all_value_comboBox[12],comboBox2 = self.all_offset_comboBox[12],
			line2 = self.all_offset_lineEdit[12]))
		# ------- stretchDamp ---------
		self.all_value_lineEdit[13].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[13],
			attr = core.attr_list[13],comboBox = self.all_value_comboBox[13],comboBox2 = self.all_offset_comboBox[13],
			line2 = self.all_offset_lineEdit[13]))
		self.all_value_comboBox[13].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[13],
			attr = core.attr_list[13],comboBox = self.all_value_comboBox[13],comboBox2 = self.all_offset_comboBox[13],
			line2 = self.all_offset_lineEdit[13]))
		self.all_offset_lineEdit[13].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[13],
			attr = core.attr_list[13],comboBox = self.all_value_comboBox[13],comboBox2 = self.all_offset_comboBox[13],
			line2 = self.all_offset_lineEdit[13]))
		# ------- dynamicsWeight ---------
		self.all_value_lineEdit[14].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[14],
			attr = core.attr_list[14],comboBox = self.all_value_comboBox[14],comboBox2 = self.all_offset_comboBox[14],
			line2 = self.all_offset_lineEdit[14]))
		self.all_value_comboBox[14].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[14],
			attr = core.attr_list[14],comboBox = self.all_value_comboBox[14],comboBox2 = self.all_offset_comboBox[14],
			line2 = self.all_offset_lineEdit[14]))
		self.all_offset_lineEdit[14].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[14],
			attr = core.attr_list[14],comboBox = self.all_value_comboBox[14],comboBox2 = self.all_offset_comboBox[14],
			line2 = self.all_offset_lineEdit[14]))
		# ------- turbulenceStrength ---------
		self.all_value_lineEdit[15].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[15],
			attr = core.attr_list[15],comboBox = self.all_value_comboBox[15],comboBox2 = self.all_offset_comboBox[15],
			line2 = self.all_offset_lineEdit[15]))
		self.all_value_comboBox[15].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[15],
			attr = core.attr_list[15],comboBox = self.all_value_comboBox[15],comboBox2 = self.all_offset_comboBox[15],
			line2 = self.all_offset_lineEdit[15]))
		self.all_offset_lineEdit[15].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[15],
			attr = core.attr_list[15],comboBox = self.all_value_comboBox[15],comboBox2 = self.all_offset_comboBox[15],
			line2 = self.all_offset_lineEdit[15]))
		# ------- turbulenceFrequency ---------
		self.all_value_lineEdit[16].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[16],
			attr = core.attr_list[16],comboBox = self.all_value_comboBox[16],comboBox2 = self.all_offset_comboBox[16],
			line2 = self.all_offset_lineEdit[16]))
		self.all_value_comboBox[16].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[16],
			attr = core.attr_list[16],comboBox = self.all_value_comboBox[16],comboBox2 = self.all_offset_comboBox[16],
			line2 = self.all_offset_lineEdit[16]))
		self.all_offset_lineEdit[16].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[16],
			attr = core.attr_list[16],comboBox = self.all_value_comboBox[16],comboBox2 = self.all_offset_comboBox[16],
			line2 = self.all_offset_lineEdit[16]))
		# ------- turbulenceSpeed ---------
		self.all_value_lineEdit[17].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[17],
			attr = core.attr_list[17],comboBox = self.all_value_comboBox[17],comboBox2 = self.all_offset_comboBox[17],
			line2 = self.all_offset_lineEdit[17]))
		self.all_value_comboBox[17].currentIndexChanged.connect(lambda:self.update_value_from_checkBox(line = self.all_value_lineEdit[17],
			attr = core.attr_list[17],comboBox = self.all_value_comboBox[17],comboBox2 = self.all_offset_comboBox[17],
			line2 = self.all_offset_lineEdit[17]))
		self.all_offset_lineEdit[17].returnPressed.connect(lambda:self.offset_value_add(line = self.all_value_lineEdit[17],
			attr = core.attr_list[17],comboBox = self.all_value_comboBox[17],comboBox2 = self.all_offset_comboBox[17],
			line2 = self.all_offset_lineEdit[17]))

		self.copyGraph_pushButton.clicked.connect(lambda:self.copyGraph())
		self.scriptJob()

	def convert_value(self,line):
		value = float(line.text())
		line.setText(str(round(value,3)))
		return value

	def convert_offset(self,line):
		offset = float(line.text())
		line.setText(str(round(offset,3)))
		return offset

	def hairSystem_update(self):
		self.hairSystem_listWidget.clear()
		hairSystem = []
		hair = core.hairSystem_get()
		for i in hair:
			hairSystem.append(str(i))
		for i in hairSystem:
			item = QListWidgetItem(i)
			icon = QIcon("C:/Users/nuntawat.j/Desktop/icon/hairSystem.png")
			item.setIcon(icon)
			item.setFlags(item.flags()|Qt.ItemIsUserCheckable)
			hair = pm.PyNode(i)
			if hair.getAttr('simulationMethod') == 0:
				item.setCheckState(Qt.Unchecked)
			elif hair.getAttr('simulationMethod') == 3:
				item.setCheckState(Qt.Checked)
			self.hairSystem_listWidget.addItem(item)
		self.hairSystem_listWidget.selectAll()

	def hairSystem_status_enable(self):
		for i in xrange(self.hairSystem_listWidget.count()):
			item = self.hairSystem_listWidget.item(i)
			item.setCheckState(Qt.Checked)

	def hairSystem_status_disable(self):
		for i in xrange(self.hairSystem_listWidget.count()):
			item = self.hairSystem_listWidget.item(i)
			item.setCheckState(Qt.Unchecked)

	def hairSystem_status_changed(self):
		hairSystem = core.hairSystem_get()
		hairSystem = [n.nodeName() for n in hairSystem]
		check_dict = {Qt.Unchecked:0,Qt.Checked:3}
		for i in xrange(self.hairSystem_listWidget.count()):
			item = self.hairSystem_listWidget.item(i)
			name = str(item.text())
			if name in hairSystem:
				index = hairSystem.index(name)
				node = hairSystem[index]
				status = item.checkState()
				node = pm.PyNode(node)
				node.simulationMethod.set(check_dict[status])

	def hairSystem_update_value_lineEdit(self):
		try :
			item = self.hairSystem_listWidget.selectedItems()[0]
			node = pm.PyNode(item.text())
			for i in range(len(core.attr_list)):
				value = node.getAttr(core.attr_list[i])
				self.all_value_lineEdit[i].setText(str(round(value,3)))
			for i in range(len(core.checkBox_attr_list)):
				if node.getAttr(core.checkBox_attr_list[i]) == 1 :
					self.attr_chkBox[i].setCheckState(Qt.Checked)
				elif node.getAttr(core.checkBox_attr_list[i]) == 0 :
					self.attr_chkBox[i].setCheckState(Qt.Unchecked)
		except:
			pass

	def hairSystem_update_offset_lineEdit(self):
		default = 0.1
		for i in self.all_offset_lineEdit:
			i.setText(str(round(default,3)))

	def update_value_from_checkBox(self,line,attr,comboBox,comboBox2,line2):
		try:
			val = comboBox.currentText()
			if val == '-':
				pass
			else:
				line.setText(str(val))
				self.offset_value_add(line,attr,comboBox,comboBox2,line2)
			comboBox.setCurrentIndex(0)
		except:
			pass

	def offset_value_add(self,line,attr,comboBox,comboBox2,line2):
		try:
			val = self.convert_value(line)
			offset_value = self.convert_offset(line2)
			offset_symbol = comboBox2.currentText()
			items = self.hairSystem_listWidget.selectedItems()
			nodes = []
			for item in items:
				nodes.append(pm.PyNode(item.text()))
			node = nodes[0]
			old_value = node.getAttr(attr)
			old_value = (float(round(old_value,3)))
			if old_value != val:
				print old_value,val
				comboBox.addItem(str(old_value))
			for i in range(len(nodes)):
				offset = offset_value * i
				print offset
				if offset_symbol == '+':
					new_val = val + offset
				elif offset_symbol == '-':
					new_val = val - offset
				nodes[i].setAttr(attr,new_val)
		except:
			pass

	def copyGraph(self):
		try :
			items = self.hairSystem_listWidget.selectedItems()
			nodes = []
			for item in items:
				nodes.append(pm.PyNode(item.text()))
			driver = nodes[0]
			driven = nodes[1:]
			self.set_attraction_graph(driver=driver,driven=driven)
		except:
			pass

	def query_attraction_graph(self,node):
		size = node.getAttr('attractionScale',size=True)
		data = {}
		counter = 0
		for i in range(0,size):
			name = 'point%s'%(i+1)
			float_value = node.getAttr('attractionScale[%s].attractionScale_FloatValue'%(i+counter))
			position = node.getAttr('attractionScale[%s].attractionScale_Position'%(i+counter))
			interp = node.getAttr('attractionScale[%s].attractionScale_Interp'%(i+counter))
			while (float_value == 0.0) and (position == 0.0) and (interp == 0):
				pm.removeMultiInstance(node+'.attractionScale[%s]'%(i+counter),b=True)
				counter += 1
				float_value = node.getAttr('attractionScale[%s].attractionScale_FloatValue'%(i+counter))
				position= node.getAttr('attractionScale[%s].attractionScale_Position'%(i+counter))
				interp = node.getAttr('attractionScale[%s].attractionScale_Interp'%(i+counter))
				if counter == 100:
					break
			data[name] = [float_value,position,interp]
		return [data,size]

	def set_attraction_graph(self,driver,driven):
		load = self.query_attraction_graph(node=driver)
		data = load[0]
		size_data = load[1]
		for drive in driven:
			counter = 0
			size = drive.getAttr('attractionScale',size=True)
			float_value = drive.getAttr('attractionScale[0].attractionScale_FloatValue')
			position = drive.getAttr('attractionScale[0].attractionScale_Position')
			interp = drive.getAttr('attractionScale[0].attractionScale_Interp')
			if (float_value == 0.0) and (position == 0.0) and (interp == 0):
				size += 1
			else:
				pass
			for i in range(1,size):
				float_value = drive.getAttr('attractionScale[%s].attractionScale_FloatValue'%(i+counter))
				position = drive.getAttr('attractionScale[%s].attractionScale_Position'%(i+counter))
				interp = drive.getAttr('attractionScale[%s].attractionScale_Interp'%(i+counter))
				while (float_value == 0.0) and (position == 0.0) and (interp == 0):
					pm.removeMultiInstance(drive+'.attractionScale[%s]'%(i+counter),b=True)
					counter += 1
					float_value = drive.getAttr('attractionScale[%s].attractionScale_FloatValue'%(i+counter))
					position= drive.getAttr('attractionScale[%s].attractionScale_Position'%(i+counter))
					interp = drive.getAttr('attractionScale[%s].attractionScale_Interp'%(i+counter))
					if counter == 100:
						break
				pm.removeMultiInstance(drive+'.attractionScale[%s]'%(i+counter),b=True)
			for i in range(0,size_data):
				drive.setAttr('attractionScale[%s].attractionScale_FloatValue'%i,data['point%s'%str(i+1)][0])
				drive.setAttr('attractionScale[%s].attractionScale_Position'%i,data['point%s'%str(i+1)][1])
				drive.setAttr('attractionScale[%s].attractionScale_Interp'%i,data['point%s'%str(i+1)][2])

	def scriptJob(self):
		hairSystem = core.hairSystem_get()
		for hair in hairSystem:
			for attr in core.attr_list:
				pm.scriptJob(ac = (hair+'.'+attr,lambda:self.mergeCmd(attr)))
			for checkBox in core.checkBox_attr_list:
				pm.scriptJob(ac = (hair+'.'+checkBox,lambda:self.mergeCmd(attr)))

	def mergeCmd(self,attr):
		self.hairSystem_update_value_lineEdit()
		print attr
		data = self.convert_type(attr=attr)
		print data[0],data[1],data[2],data[3]
		self.offset_value_add(line=data[0],attr=attr,comboBox=data[1],comboBox2=data[2],line2=data[3])

	def convert_type(self,attr):
		index = core.attr.index(attr)
		value_lineEdit = self.all_value_lineEdit[index]
		value_comboBox = self.all_value_comboBox[index]
		offset_comboBox = self.all_offset_comboBox[index]
		offset_lineEdit = self.all_offset_lineEdit[index]
		return value_lineEdit,value_comboBox,offset_comboBox,offset_lineEdit