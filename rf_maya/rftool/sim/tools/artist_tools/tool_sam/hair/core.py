import pymel.core as pm

attr_list = ['stretchResistance','compressionResistance','bendResistance','twistResistance','extraBendLinks','restLengthScale','startCurveAttract',
'attractionDamp','mass','drag','tangentialDrag','motionDrag','damp','stretchDamp','dynamicsWeight','turbulenceStrength','turbulenceFrequency',
'turbulenceSpeed']
checkBox_attr_list = ['noStretch','ignoreSolverGravity','ignoreSolverWind','disableFollicleAnim']

def hairSystem_get(*args):
	try :
		hairSystem = pm.ls (type='hairSystem')
		return hairSystem
	except:
		print 'not found hairSystem'
