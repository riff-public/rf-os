import pymel.core as pm
import maya.mel as mel
import os 
import sys

class SamYetiExport ( object ) :

	def __init__ ( self ) :
		self.currentProj = pm.workspace ( q = True , rd = True )
		self.shot = self.currentProj.split ( '/' ) [ 3 ]
		self.character = self.currentProj.split ( '/' ) [ 4 ]
		self.cachePath = self.currentProj + 'cache'
		self.subFolder = self.shot + '_' + self.character
		mel.eval ( 'pgYetiEnsurePluginLoaded()' )

	def initializeTimeRange ( self , *args ) :
		from rftool.preload import duration_check
		from rf_utils.sg import sg_utils
		from rf_utils.context import context_info

		project = self.currentProj.split('/')[1]
		entity  = self.currentProj.split('/')[3]

		context = context_info.Context()
		context.use_sg ( sg_utils.sg , project , 'scene' , entityName = entity )
		scene = context_info.ContextPathInfo ( context = context )
		sgDuration = duration_check.get_duration ( scene )
		start = scene.projectInfo.scene.start_frame
		end_time = start + ( sgDuration - 1 )

		pm.intField ( self.cacheStart_intField , e = True , value = start - 5 ) 
		pm.intField ( self.cacheEnd_intField , e = True , value = end_time + 5 )

	def yetiLists ( self , *args ) :
		yeti_list = pm.listRelatives ( pm.ls ( type = 'pgYetiMaya' ) , p = True )
		yeti_textScll = pm.textScrollList ( self.yeti_textScll , q = True , ai = True )

		for each in yeti_textScll :
			if not pm.objExists ( each ) :
				pm.textScrollList ( self.yeti_textScll , e = True , ri = each )

		for each in yeti_list :
			if each not in yeti_textScll :
				pm.textScrollList ( self.yeti_textScll , e = True , a = each )

	def openCachePath ( self , *args ) :
		path = pm.textField ( self.yetiPath_txtField , q = True , tx = True )
		if os.path.exists ( path ) :
			os.startfile ( path )

	def browseBtn ( self , *args ) :
		self.yetiPath = pm.fileDialog2 ( fm = 3 , dir = self.cachePath , cap = 'Export Location' , okc = 'Select' )
		pm.textField ( self.yetiPath_txtField , e = True , tx = self.yetiPath [ 0 ] )

	def cache ( self , target ) :
		pm.select ( target , r = True )
		name = target.split ( '_YetiNode' ) [ 0 ]
		if ':' in name :
			name = name.split ( ':' ) [ 1 ]
		self.currentPath = pm.textField ( self.yetiPath_txtField , q = True , tx = True )
		self.folder = self.currentPath + '/' + self.character
		self.cacheFolder = self.folder + '/' + self.character
		self.yetiCacheFolder = self.cacheFolder + '_' + name
		cacheStart = pm.intField ( self.cacheStart_intField , q = True , v = True )
		cacheEnd = pm.intField ( self.cacheEnd_intField , q = True , v = True )
		cacheNoS = pm.intField ( self.cacheNoS_intField , q = True , v = True )

		if os.path.exists ( self.folder ) :
			pass
		else :
			pm.sysFile ( self.folder , md = True )
		outputName = self.yetiCacheFolder + '/' + self.subFolder + '_' + name + '.hero.%04d.fur'
		pm.sysFile ( self.yetiCacheFolder , md = True )
		pm.setAttr ( target + '.outputCacheFileName' , outputName , type = 'string' )
		pm.setAttr ( target + '.outputCacheFrameRangeStart' , cacheStart )
		pm.setAttr ( target + '.outputCacheFrameRangeEnd' , cacheEnd )
		pm.setAttr ( target + '.outputCacheNumberOfSamples' , cacheNoS )
		#mel.eval ( 'AEpgYetiWriteCacheCMD %s' %target )
		pm.mel.eval ( 'pgYetiCommand -writeCache "%s" ' %outputName + ' -range %s %s -samples %s' % ( cacheStart , cacheEnd , cacheNoS ) )

	def cacheSelected ( self , *args ) : 
		selected = pm.textScrollList ( self.yeti_textScll , q = True , si = True )
		for each in selected :
			self.cache ( target = each )

	def cacheAll ( self , *args ) :
		selectAll = pm.textScrollList ( self.yeti_textScll , q = True , ai = True )
		for each in selectAll :
			self.cache ( target = each )

	def resize ( self , *args ) :
		pm.window ( 'yetiCacheBySam' , e = True , h = 10 )

	def ui ( self , *args ) :
		w = 300.00
		if pm.window ( 'yetiCacheBySam' , ex = True ) :
			pm.deleteUI ( 'yetiCacheBySam' )
		else : pass

		window = pm.window ( 'yetiCacheBySam' , t = 'SamTool : YetiCache' , tb = True , mnb = True , mxb = True , s = False , rtf = True )
		
		pm.window ( 'yetiCacheBySam' , e = True , w = w , h = 10 )
		with window :
			with pm.rowColumnLayout ( nc = 1 ) :
				self.yeti_textScll = pm.textScrollList ( 'yeti_textScll' , w = w , h = 200 , ams = True , sc = self.yetiLists )
				self.yetiLists ()
				pm.button ( l = 'REFRESH' , c = self.yetiLists , bgc = ( 152 / 255.00 , 221 / 255.00 , 222 / 255.00 ) )
				pm.text ( l = '' , h = 5 )
			with pm.rowColumnLayout ( nc = 7 , cw = [ ( 1 , w / 7 ) , ( 2 , w / 7 ) , ( 3 , w / 7 ) , ( 4 , w / 7 ) , ( 5 , w / 7 ) , ( 6 , w / 7 ) , ( 7 , w / 7 ) ] ) :
				pm.text ( l = '' )
				with pm.rowColumnLayout ( nc = 1 , w = w / 5 ) :
					self.cacheStart_intField = pm.intField ( 'cacheStart_intField' , w = w / 5 )
					pm.text ( l = 'START' , w = w / 5 )
				pm.text ( l = '' )
				with pm.rowColumnLayout ( nc = 1 , w = w / 5 ) :
					self.cacheEnd_intField = pm.intField ( 'cacheEnd_intField' , w = w / 5 )
					pm.text ( l = 'END' , w = w / 5 )
				pm.text ( l = '' )
				with pm.rowColumnLayout ( nc = 1 , w = w / 5 ) :
					self.cacheNoS_intField = pm.intField ( 'cacheNoS_intField' , w = w / 5 , v = 10 )
					pm.text ( l = 'SAMPLE' , w = w / 5 )
				pm.text ( l = '' )
			with pm.rowColumnLayout ( nc = 1 ) :
				pm.text ( l = '' , h = 5 )
				pm.button ( 'openCachePath' , l = 'CACHE DIRECTORY' , w = w , c = self.openCachePath , bgc = ( 152 / 255.00 , 221 / 255.00 , 222 / 255.00 ) )
				self.yetiPath_txtField = pm.textField ( 'yetiPath_txtField' , w = w , tx = self.cachePath )
			with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w / 3 * 2 ) , ( 2 , w / 3 ) ] ) :
				pm.text ( l = '' )
				pm.button ( 'browseBtn' , l = 'BROWSE' , c = self.browseBtn , bgc = ( 152 / 255.00 , 221 / 255.00 , 222 / 255.00 ) )
			with pm.rowColumnLayout ( nc = 1 ) :
				pm.text ( l = '' , h = 5 )
			with pm.rowColumnLayout ( nc = 5 , cw = [ ( 1 , w / 5) , ( 2 , w / 5 ) , ( 3 , w / 5 ) , ( 4 , w / 5 ) , ( 5 , w / 5 ) ] ) :
				pm.text ( l = '' )
				pm.button ( 'cacheSelected' , l = 'SELECTED' , c = self.cacheSelected , bgc = ( 152 / 255.00 , 221 / 255.00 , 222 / 255.00 ) )
				pm.text ( l = '' )
				pm.button ( 'cacheAll' , l = 'ALL' , c = self.cacheAll , bgc = ( 152 / 255.00 , 221 / 255.00 , 222 / 255.00 ) )
				pm.text ( l = '' )
			with pm.rowColumnLayout ( nc = 1 ) :
				pm.text ( l = '' , h = 5 )
		self.initializeTimeRange ()

SamYetiExport().ui()