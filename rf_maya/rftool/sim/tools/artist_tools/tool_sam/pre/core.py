import pymel.core as pm
import os , sys

class Core ( object ) :

	def __init__ ( self ) :
		self.presetPath = self.getPreferencePath ()
		nCloth = self.presetPath + '/nCloth'
		if not os.path.exists ( nCloth ) :
			os.makedirs ( nCloth )
		nRigid = self.presetPath + '/nRigid'
		if not os.path.exists ( nRigid ) :
			os.makedirs ( nRigid )
		nucleus = self.presetPath + '/nucleus'
		if not os.path.exists ( nucleus ) :
			os.makedirs ( nucleus )
		hairSystem = self.presetPath + '/hairSystem'
		if not os.path.exists ( hairSystem ) :
			os.makedirs ( hairSystem )
	
	def getPreferencePath ( self ) :
		document = os.path.expanduser ( '~' )
		program = pm.about ( a =  True )
		version = pm.about ( v = True )
		path = document + '/' + program + '/' + version + '/presets/attrPresets'
		if not os.path.exists ( path ) :
			os.makedirs ( path )
		return path

	def run ( self , *args ) :

		l = 'left'

		mp = os.path.abspath ( __file__ ).split( '\corePreset' ) [ 0 ]
		print 'Location Script >> ' + mp
		if not mp in sys.path :
			sys.path.insert ( 0 , mp )

		import nCloth as nCloth
		reload ( nCloth )
		nClothUI = nCloth.nClothPreset ()

		import hairSystem as hairSystem
		reload ( hairSystem )
		hairSystemUI = hairSystem.hairSystemPreset ()

		import nucleus as nucleus
		reload ( nucleus )
		nucleusUI = nucleus.nucleusPreset ()

		import nRigid as nRigid
		reload ( nRigid)
		nRigidUI = nRigid.nRigidPreset ()

		w = 300.00

		if pm.window ( 'samPreset' , exists = True ) :
			pm.deleteUI ( 'samPreset' )
		else : pass

		window = pm.window ( 'samPreset' , t = 'SamTool : Preset' , tb = True , mnb = True , mxb = True , s = False , rtf = True )
		pm.window ( 'samPreset' , e = True , w = w , h = 10 )
		with window :

			with pm.rowColumnLayout ( nc = 1 , adj = True ) :
				tabLayout = pm.tabLayout ( imh = 5 , imw = 5 )
				with tabLayout :

					nCloth_rowColumnLayout = pm.rowColumnLayout ( nc = 1 , w = w )
					with nCloth_rowColumnLayout :
						nClothUI.insert ( )

					hairSystem_rowColumnLayout = pm.rowColumnLayout ( nc = 1 , w = w )
					with hairSystem_rowColumnLayout :
						hairSystemUI.insert ( )

					nucleus_rowColumnLayout = pm.rowColumnLayout ( nc = 1 , w = w )
					with nucleus_rowColumnLayout :
						nucleusUI.insert ( )

					nRigid_rowColumnLayout = pm.rowColumnLayout ( nc = 1 , w = w )
					with nRigid_rowColumnLayout :
						nRigidUI.insert ( )

				pm.tabLayout ( tabLayout , e = True , tl = ( ( nCloth_rowColumnLayout , 'nCloth' ) ,
					( hairSystem_rowColumnLayout , 'hairSystem' ) , ( nucleus_rowColumnLayout , 'nucleus' ) ,
					( nRigid_rowColumnLayout , 'nRigid' ) ) )

Core().run()