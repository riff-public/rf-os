import pymel.core as pm
import maya.mel as mel
import os , ast

class Test ( object ) :
	def __init__ ( self ) :
		self.folder = self.path ()

	def path ( self ) :
		document = os.path.expanduser ( '~' )
		program = pm.about ( a =  True )
		version = pm.about ( v = True )
		path = document + '/' + program + '/' + version + '/presets/attrPresets'
		if not os.path.exists ( path ) :
			os.makedirs ( path )
		return path

	def updateOutliner ( self , *args ) :
		outliner = pm.ls ( typ = ( 'nCloth' , 'nRigid' , 'nucleus' , 'hairSystem' ) )
		nCloth_txtScrll = pm.textScrollList ( self.nCloth_txtScrll , q = True , ai = True )
		for each in nCloth_txtScrll :
			if not pm.objExist ( each ) :
				pm.textScrollList ( self.nCloth_txtScrll , e = True , ri = each )
		#####################################################################################
		nRigid_txtScrll = pm.textScrollList ( self.nRigid_txtScrll , q = True , ai = True )
		for each in nRigid_txtScrll :
			if not pm.objExist ( each ) :
				pm.textScrollList ( self.nRigid_txtScrll , e = True , ri = each )
		#####################################################################################
		nucleus_txtScrll = pm.textScrollList ( self.nucleus_txtScrll , q = True , ai = True )
		for each in nucleus_txtScrll :
			if not pm.objExist ( each ) :
				pm.textScrollList ( self.nucleus_txtScrll , e = True , ri = each )
		#####################################################################################
		hairSystem_txtScrll = pm.textScrollList ( self.hairSystem_txtScrll , q = True , ai = True )
		for each in hairSystem_txtScrll :
			if not pm.objExist ( each ) :
				pm.textScrollList ( self.hairSystem_txtScrll , e = True , ri = each )
		#####################################################################################
		for each in outliner :
			node = each.nodeType ()
			if each == 'nCloth' :
				if not each not in nCloth_txtScrll :
					pm.textScrollList ( self.nCloth_txtScrll , e = True , a = each )
			elif each == 'nRigid' :
				if not each not in nRigid_txtScrll :
					pm.textScrollList ( self.nRigid_txtScrll , e = True , a = each )
			elif each == 'nucleus' :
				if not each not in nucleus_txtScrll :
					pm.textScrollList ( self.nucleus_txtScrll , e = True , a = each )
			elif each == 'hairSystem' :
				if not each not in hairSystem_txtScrll :
					pm.textScrollList ( self.hairSystem_txtScrll , e = True , a = each )

	def savePresets ( self , target ) :
		file = pm.about ( f = True )
		nodeType = target.nodeType ()
		result = '// requires maya "%s";\n' % file 
		result = result + 'startAttrPreset( "%s" );\n' %nodeType 
		result = result + 'blendAttrStr "iconName" "";\n'
		atrs = pm.listAttr ( target , m = True , r = True , w = True , v = True , hd = True , s = True )
		for attr in atrs :
			obj = target + "." + attr
			if pm.objExists ( obj ) :
				value = pm.getAttr ( obj )
				if value == True :
					value = 1
				elif value == False :
					value = 0
				result = result + 'blendAttr "%s" %s;\n' % ( attr , value )
		result = result + 'endAttrPreset();\n'
		#####################################################################################
		replace = pm.button ( 'sss' , q = True , v = True )
		version = 0
		data = self.folder + '/' + nodeType + '/' + target + '_' + '%04d'%version + '.mel'
		elif replace == False :
			while os.path.exists ( data ) :
				data = self.folder + '/' + nodeType + '/' + target + '%04d'%version + '.mel'
				version += 1
		text = open ( data , 'w' )
		text.write ( result )
		text.close ()
		self.listGetPreset ()

	def saveNCloth ( self , *args ) :
		selected = pm.textScrollList ( self.nCloth_txtScrll , q = True , si = True )
		shape = pm.ls ( selected )
		for shp in shape :
			self.savePresets ( target = shp )

	def saveNRigid ( self , *args ) :
		selected = pm.textScrollList ( self.nRigid_txtScrll , q = True , si = True )
		shape = pm.ls ( selected )
		for shp in shape :
			self.savePresets ( target = shp )

	def saveNucleus ( self , *args ) :
		selected = pm.textScrollList ( self.nucleus_txtScrll , q = True , si = True )
		shape = pm.ls ( selected )
		for shp in shape :
			self.savePresets ( target = shp )

	def saveHairSystem ( self , *args ) :
		selected = pm.textScrollList ( self.hairSystem_txtScrll , q = True , si = True )
		shape = pm.ls ( selected )
		for shp in shape :
			self.savePresets ( target = shp )


	def loadPresets ( self , target ) :
		blendValue = pm.floatField ( self.blendValue_floatField , q = True , v = True )
		name = pm.textScrollList ( self.preview_txtScrll , q = True , si = True ) [ 0 ]
		name = self.presetPath + '/' + name
		print target , name , blendValue
		pm.mel.eval ( 'applyAttrPreset %s \"%s\" %4.1f' % ( target , name, blendValue ) )

	def loadAttr ( self , *args ) :
		selected = pm.textScrollList ( self.nCloth_txtScrll , q = True , si = True )
		shape = pm.ls ( selected )
		for shp in shape :
			self.loadPresets ( target = shp )


