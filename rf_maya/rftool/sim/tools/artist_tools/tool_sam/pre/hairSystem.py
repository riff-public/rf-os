import pymel.core as pm
import maya.mel as mel
import os , ast

class hairSystemPreset ( object ) :
	def __init__ ( self ) :
		self.presetPath = self.getPreferencePath ()

	def getPreferencePath ( self ) :
		document = os.path.expanduser ( '~' )
		program = pm.about ( a =  True )
		version = pm.about ( v = True )
		path = document + '/' + program + '/' + version + '/presets/attrPresets'
		if not os.path.exists ( path ) :
			os.makedirs ( path )
		return path

	def updateHairSystemList ( self , *args ) :
		hairSystem_list = pm.ls ( typ = 'hairSystem' )
		textScroll_hairSystem_list = pm.textScrollList ( self.hairSystem_txtScrll , q = True , ai = True )
		for hairSystem in textScroll_hairSystem_list :
			if not pm.objExists ( hairSystem ) :
				pm.textScrollList ( self.hairSystem_txtScrll , e = True , ri = hairSystem )
		for hairSystem in hairSystem_list :
			if hairSystem not in textScroll_hairSystem_list :
				pm.textScrollList ( self.hairSystem_txtScrll , e = True , a = hairSystem )

	def savePresets ( self , target ) :
		file = pm.about ( f = True )
		nodeType = pm.nodeType ( target )
		result = '// requires maya "%s";\n' % file 
		result = result + 'startAttrPreset( "%s" );\n' %nodeType 
		result = result + 'blendAttrStr "iconName" "";\n'
		atrs = pm.listAttr ( target , m = True , r = True , w = True , v = True , hd = True , s = True )
		for attr in atrs :
			obj = target + "." + attr
			if pm.objExists ( obj ) :
				value = pm.getAttr ( obj )
				if value == True :
					value = 1
				elif value == False :
					value = 0
				result = result + 'blendAttr "%s" %s;\n' % ( attr , value )
		result = result + 'endAttrPreset();\n'
		version = 0
		data = self.presetPath + '/' + nodeType + '/' + target + '_' + '%04d'%version + '.mel'
		replace = pm.checkBox ( self.replace_chkBox , q = True , v = True )
		if replace == False :
			while os.path.exists ( data ) :
				data = self.presetPath + '/' + nodeType + '/' + target + '_' + '%04d'%version + '.mel'
				version += 1
		elif replace == True :
			print data
		text = open ( data , 'w' )
		text.write ( result )
		text.close ()
		self.listGetPreset ()

	def saveAttr ( self , *args ) :
		selected = pm.textScrollList ( self.hairSystem_txtScrll , q = True , si = True )
		shape = pm.ls ( selected )
		for shp in shape :
			self.savePresets ( target = shp )

	def loadPresets ( self , target ) :
		blendValue = pm.floatField ( self.blendValue_floatField , q = True , v = True )
		name = pm.textScrollList ( self.preview_txtScrll , q = True , si = True ) [ 0 ]
		name = self.presetPath + '/hairSystem/' + name
		pm.mel.eval ( 'applyAttrPreset %s \"%s\" %4.1f' % ( target , name, blendValue ) )

	def loadAttr ( self , *args ) :
		selected = pm.textScrollList ( self.hairSystem_txtScrll , q = True , si = True )
		shape = pm.ls ( selected )
		for shp in shape :
			self.loadPresets ( target = shp )

	def listGetPreset ( self , *args ) :
		fileLists = os.listdir ( self.presetPath + '/hairSystem' )
		previewList = pm.textScrollList ( self.preview_txtScrll , e = True , ra = True )
		for files in fileLists :
			pm.textScrollList ( self.preview_txtScrll , e = True , a = files )

	def deletePresetList ( self , *args ) :
		fileLists = pm.textScrollList ( self.preview_txtScrll , q = True , si = True )
		if fileLists :
			for files in fileLists :
				os.remove ( self.presetPath + '/hairSystem/' + fileLists [ 0 ] )
				self.listGetPreset ( self.presetPath )
		else :
			pm.warning ( 'NO SELECTED' )

	def openDatas ( self , *args ) :
		fileLists = pm.textScrollList ( self.preview_txtScrll , q = True , si = True )
		os.startfile ( self.presetPath + '/hairSystem/' + fileLists [ 0 ] )

	def convertValue ( self , *args ) :
		value = pm.floatField ( self.blendValue_floatField , q = True , v = True )
		pm.text ( self.blendValue_text , e = True , l = 'Blend : ' + str ( value * 100 ) + ' %' )

	def insert ( self , *args ) :
		w = 300.00
		self.hairSystem_txtScrll = pm.textScrollList ( 'hairSystem_txtScrll' , w = w , 
			h = 80 , sc = self.updateHairSystemList , ams = True )
		self.updateHairSystemList ()
		pm.button ( l = 'Refresh', c = self.updateHairSystemList , bgc = ( 1 , 0.8745 , 0.7294 ) )
		pm.separator ( vis = True , h = 7.5 )	
		with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w / 2 ) , ( 2 , w / 2 ) ] ) :
			self.blendValue_floatField = pm.floatField ( max = 1 , min = 0 , pre = 2 , v = 1 , cc = self.convertValue )
			self.blendValue_text = pm.text ( l = 'Blend : 100.00 %' )
		with pm.rowColumnLayout ( nc = 1 ) :
			pm.text ( l = '' , h = 1 )
			self.preview_txtScrll = pm.textScrollList ( 'previewLists' , h = 80 , dcc = self.openDatas , w = w )
		with pm.rowColumnLayout ( nc = 1 ) :
			self.replace_chkBox = pm.checkBox ( l = 'Replace' , v = 0 )
		with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , w / 3 ) , ( 2 , w / 3 ) , ( 3 , w / 3 ) ] ) :
			pm.button ( l = 'Save' , c = self.saveAttr , bgc = ( 0.7294 , 1 , 0.7882 ) )
			pm.button ( l = 'Load' , c = self.loadAttr , bgc = ( 0.7294 , 1 , 1 ) )
			pm.button ( l = 'Delete' , c = self.deletePresetList  , bgc = ( 1 , 0.7019 , 0.7294 ) )
		with pm.rowColumnLayout ( nc = 1 ) :
			pm.button ( l = 'Refresh', c = self.listGetPreset , bgc = ( 1 , 0.8745 , 0.7294 ) , w = w )
		self.listGetPreset()