import pymel.core as pm
import maya.cmds as mc
import shutil
import json
import os

class samCtrlWindKeyframe ( object ) :
	
	def __init__ ( self ) :
		self.path = self.getPreferencePath ()
		self.currentProj = pm.workspace ( q = True , rd = True )
		
	def getPreferencePath ( self , *args ) :
		doc = os.path.expanduser ( '~' )
		doc = doc + '/keyframeWind'
		if not os.path.exists ( doc ) :
			os.makedirs ( doc )
		return doc

	def ctrlWind ( self , *args ) :
		ctrl = [ 'nucleus1Gmbl_Ctrl' , 'nucleus2Gmbl_Ctrl' , 'nucleus3Gmbl_Ctrl' , 'nucleus4Gmbl_Ctrl' ]
		foul = []
		for each in ctrl :
			if not pm.objExists ( each ) :
				foul.append ( each )
		ctrl = [ x for x in ctrl if x not in foul ]
		ctrl_txt = pm.textScrollList ( self.ctrl_txt , q = True , ai = True )
		for each in ctrl_txt :
			if not pm.objExists ( each ) :
				pm.textScrollList ( self.ctrl_txt , e = True , ri = each )
		for each in ctrl :
			if each not in ctrl_txt :
				pm.textScrollList ( self.ctrl_txt , e = True , a = each )

	# def saveKey ( self , *args ) :
		#folder = self.path + '/nucleusCtrl'
		# if not os.path.exists ( folder ) :
		# 	os.makedirs ( folder )
		# timeDict = {}
		# valueDict = {}
		# attrDict = {}
		# sel = mc.textScrollList ( self.ctrl_txt , q = True , si = True )
		# for each in range ( len ( sel ) ) :
		# 	anim = pm.listConnections ( sel [ each ] , t = ( 'animCurveTA' , 'animCurveTU' , 'animCurveTL' ) )
		# 	attr = mc.listConnections ( anim , p = True )
		# 	for attrs in range ( len ( attr ) ) :
		# 		value = pm.keyframe ( attr [ attrs ] , q = True , vc = True )
		# 		valueDict [ attr [ attrs ] ] = value 
		# 		time = pm.keyframe ( attr [ attrs ] , q = True , tc = True )
		# 		timeDict [ attr [ attrs ] ] = time
		# 		attrDict [ ( str ( sel [ each ] ) ) ] = attr
		# attrDict_json = json.dumps ( attrDict )
		# with open ( folder + '/attr.json' , 'w' ) as outfile :
		# 	json.dump ( attrDict , outfile )
		# timeDict_json = json.dumps ( timeDict )
		# with open ( folder + '/time.json' , 'w' ) as outfile :
		# 	json.dump ( timeDict , outfile )
		# valueDict_json = json.dumps ( valueDict )
		# with open ( folder + '/value.json' , 'w' ) as outfile :
		# 	json.dump ( valueDict , outfile )
		# self.listKeyframe ( self.path )

	def saveKey ( self , *args ) :
		shot = self.currentProj.split ( '_' ) [ 2 ] + '_' + self.currentProj.split ( '_' ) [ 3 ].split ( '/' ) [ 0 ]
		folder = self.path + '/' + shot
		if not os.path.exists ( folder ) :
			os.makedirs ( folder )
		timeDict = {}
		valueDict = {}
		attrDict = {}
		ctrl = mc.textScrollList ( self.ctrl_txt , q = True , si = True )
		ctrlShape = pm.listRelatives ( ctrl , s = True )
		for each in range ( len ( ctrlShape ) ) :
			anim = pm.listConnections ( ctrlShape [ each ] , t = ( 'animCurveTA' , 'animCurveTU' , 'animCurveTL' ) )
			attr = mc.listConnections ( anim , p = True )
			for anims in range ( len ( attr ) ) :
				value = pm.keyframe ( anim [ anims ] , q = True , vc = True )
				valueDict [ attr [ anims ].split( '_rand' ) [ 0 ] ] = value 
				time = pm.keyframe ( anim [ anims ] , q = True , tc = True )
				timeDict [ attr [ anims ].split( '_rand' ) [ 0 ] ] = time
				attrDict [ ctrl [ each ] ] = attr
		attrDict_json = json.dumps ( attrDict )
		with open ( folder + '/attr.json' , 'w' ) as outfile :
			json.dump ( attrDict , outfile )
		timeDict_json = json.dumps ( timeDict )
		with open ( folder + '/time.json' , 'w' ) as outfile :
			json.dump ( timeDict , outfile )
		valueDict_json = json.dumps ( valueDict )
		with open ( folder + '/value.json' , 'w' ) as outfile :
			json.dump ( valueDict , outfile )
		self.listKeyframe ( self.path )

	def loadKey ( self , *args ) :
		fileLists = pm.textScrollList ( self.preview_txt , q = True , si = True ) [ 0 ]
		ctrl = pm.textScrollList ( self.ctrl_txt , q = True , si = True )
		time = open ( self.path + '/' + fileLists + '/time.json' )
		tData = json.load ( time )
		value = open ( self.path + '/' + fileLists + '/value.json' )
		vData = json.load ( value )
		attr = []
		for attrs in range ( len ( ctrl ) ) :
			name = open ( self.path + '/' + fileLists + '/attr.json' )
			nData = json.load ( name )
			attr.extend ( nData [ ctrl [ attrs ] ] )
			for each in range ( len ( attr ) ) :
				attr [ each ] = attr [ each ].split ( '_rand' ) [ 0 ]
				t = tData [ attr [ each ] ]
				v = vData [ attr [ each ] ]
				for key in range ( len ( t ) ) :
					pm.setKeyframe ( attr [ each ].split ( 'Shape' ) [ 0 ] + attr [ each ].split ( 'Shape' ) [ 1 ].split ( '_rand' ) [ 0 ] , t = t [ key ] , v = v [ key ] )

	def attr ( self , node , attr , attrType = 'float' , *args ) :
		if not ( pm.attributeQuery ( attr , n = node , ex = True ) ) :
			outputAttr = node.addAttr ( attr , k = True , at = attrType )
		else :
			exec ( 'outputAttr = node.%s' % attr )
		return outputAttr

	def assignKey ( self , *args ) :
		self.loadKey ()
		ctrl = pm.textScrollList ( self.ctrl_txt , q = True , si = True )
		ctrls = pm.ls ( ctrl )
		fileLists = pm.textScrollList ( self.preview_txt , q = True , si = True ) [ 0 ]
		attrDict = []
		attrs = []
		for each in range ( len ( ctrl ) ) :
			file = open ( self.path + '/' + fileLists + '/attr.json' )
			data = json.load ( file )
			dataLoad = data [ ctrl [ each ] ]
			attrDict.extend ( dataLoad )
		for ad in range ( len ( attrDict ) ) :
			for rem in ctrl :
				if rem in attrDict [ ad ] :
					attrs.append ( attrDict [ ad ].split ( '.' ) [ 1 ].split ( '_rand' ) [ 0 ] )
		attrs = list ( dict.fromkeys ( attrs ) )
		if 'v' in attrs :
			attrs.remove ( 'v' )

		for obj in ctrls :
			objShape = obj.getShape()
			self.attr ( objShape , '__random__' , 'float' )
			objShape.__random__.lock ()
			for attr in attrs :
				firstTimeStat = False
				if not ( pm.attributeQuery ( attr + '_ori' , node = objShape , ex = True ) ) :
					firstTimeStat = True
				attrType = pm.attributeQuery ( attr , node = obj , at = True )
				for each in [ '_ori' , '_rand' , '_amp' ] :
					self.attr ( objShape , attr + each , attrType )

				if firstTimeStat == True :
					keyEx = pm.copyKey ( obj , at = attr )
					if not keyEx :
						exec ( 'oriValue = obj.%s.get ()' % attr )
						exec ( 'objShape.%s_ori.set ( oriValue )' % attr )
					else :
						pm.pasteKey ( objShape , at = attr + '_ori' , o = 'insert' , cp = True , c = True , to = False , 
						fo = False , vo = False )

					result = pm.createNode ( 'plusMinusAverage' , n = obj + attr [ 0 ].upper () + attr [ 1: ] + 'Result_pma' )
					amp = pm.createNode ( 'multiplyDivide' , n = obj + attr [ 0 ].upper () + attr [ 1: ] + 'Amp_mdv' )
					amp.operation.set ( 1 )
					result.operation.set ( 1 )
					exec ( 'objShape.%s_rand >> amp.i1x' % attr )        
					exec ( 'objShape.%s_amp.set(1)' % attr )
					exec ( 'objShape.%s_amp >> amp.i2x' % attr )
					if attrType != 'bool' :
						exec ( 'objShape.%s_ori >> result.input1D[0]' % attr )
					amp.ox >> result.input1D [ 1 ]
					exec ( 'result.output1D >> obj.%s' % attr )
				for each in [ '_ori' , '_rand' , '_amp' ] :
					exec ( 'objShape.%s%s.unlock ()' % ( attr , each ) )
				pm.cutKey ( objShape + '.' + attr + '_rand' , cl = 1 )
				pm.pasteKey ( objShape , at = attr + '_rand' , o = 'insert' , cp = True , c = True , to = False , fo = False , vo = False )
				pm.delete ( objShape + '_' + attr + '_ori' )
				pm.setAttr ( objShape+ '.' + attr + '_ori' , 0 )
				for each in [ '_ori' , '_rand' ] :
					exec ( 'objShape.%s%s.lock ()' % ( attr , each ) )
				if attrType == 'bool' :
					exec ( 'objShape.%s_amp.lock ()' % attr )

				if attr == 'rx' :
					attr = 'rotateX'
				if attr == 'ry' :
					attr = 'rotateY'
				if attr == 'rz' :
					attr = 'rotateZ'
				if attr == 'sx' :
					attr = 'scaleX'
				if attr == 'sy' :
					attr = 'scaleY'
				if attr == 'sz' :
					attr = 'scaleZ'
				if attr == 'tx' :
					attr = 'translateX'
				if attr == 'ty' :
					attr = 'translateY'
				if attr == 'tz' :
					attr = 'translateZ'
				pm.delete ( obj + '_' + attr )

	def listKeyframe ( self , *args ) :
	 	fileLists = os.listdir ( self.path )
		previewList = pm.textScrollList ( self.preview_txt , e = True , ra = True )
		for files in fileLists :
			pm.textScrollList ( self.preview_txt , e = True , a = files )

	def deleteList ( self , *args ) :
		fileLists = pm.textScrollList ( self.preview_txt , q = True , si = True )
		try :
			os.remove ( self.path + '/' + fileLists [ 0 ] + '/attr.json' )
			os.remove ( self.path + '/' + fileLists [ 0 ] + '/time.json' )
			os.remove ( self.path + '/' + fileLists [ 0 ] + '/value.json' )
			self.listKeyframe ( self.path )
		except :
			shutil.rmtree ( self.path + '/' + fileLists [ 0 ] + '/attr.json' )
			shutil.rmtree ( self.path + '/' + fileLists [ 0 ] + '/time.json' )
			shutil.rmtree ( self.path + '/' + fileLists [ 0 ] + '/value.json' )
			self.listKeyframe ( self.path )

	def openData ( self , *args ) :
		fileLists = pm.textScrollList ( self.preview_txt , q = True , si = True )
		os.startfile ( self.path + '/' + fileLists [ 0 ] )

	def ui ( self , *args ) :
		w = 300.00
		if pm.window ( 'samCtrlWindKeyframe' , exists = True ) :
			pm.deleteUI ( 'samCtrlWindKeyframe' )
		else :
			pass
		window = pm.window ( 'samCtrlWindKeyframe' ,
			t = 'samTool : ctrlWindKeyframe' , tb = True , 
			mnb = True , mxb = True , s = True , rtf = True )

		pm.window ( 'samCtrlWindKeyframe' , e = True , w = w , h = 10 )
		with window :
			with pm.rowColumnLayout ( nc = 1 ) :
				self.ctrl_txt = pm.textScrollList ( 'ctrl_txt' , w = w , h = 80 , sc = self.ctrlWind , ams = True )
				pm.button ( l = 'Refresh' , c = self.ctrlWind , bgc = ( 255 / 255.00 , 214 / 255.00 , 98 / 255.00 ) , w = w )
				pm.separator ( vis = True , h = 5 )
			self.ctrlWind ()
			with pm.rowColumnLayout ( nc = 1 ) :
				self.preview_txt = pm.textScrollList ( 'preview_txt' , h = 80 , dcc = self.openData , w = w )
			with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , w / 3 ) , ( 2 , w / 3 ) , ( 3 , w / 3 ) ] ) :
				pm.button ( l = 'Save' , c = self.saveKey , bgc = ( 0 / 255.00 , 110 / 255.00 , 81 / 255.00 ) )
				pm.button ( l = 'Load' , c = self.assignKey , bgc = ( 42 / 255.00 , 75 / 255.00 , 124 / 255.00 ) )
				pm.button ( l = 'Delete' , c = self.deleteList  , bgc = ( 119 / 255.00 , 33 / 255.00 , 46 / 255.00 ) )
			with pm.rowColumnLayout ( nc = 1 ) :
				pm.button ( l = 'Refresh' , c = self.listKeyframe , bgc = ( 255 / 255.00 , 214 / 255.00 , 98 / 255.00 ) , w = w )
			self.listKeyframe ()

samCtrlWindKeyframe().ui()



