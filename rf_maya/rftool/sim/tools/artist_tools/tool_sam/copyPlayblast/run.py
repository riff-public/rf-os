import sys

from PySide2 import QtCore, QtWidgets
from shiboken2 import wrapInstance

import app
reload(app)

def mayaRun():

    import maya.OpenMayaUI as omui
    global copyPlayblastToolApp

    try:
        copyPlayblastToolApp.close()
    except:
        pass
    
    ptr = omui.MQtUtil.mainWindow()
    copyPlayblastToolApp = app.copyPlayblastTool(parent=wrapInstance(long(ptr), QtWidgets.QWidget))
    copyPlayblastToolApp.show()

    return copyPlayblastToolApp