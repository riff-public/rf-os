import os
import shutil
from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

import maya.cmds as mc
import pymel.core as pm

import ui
reload(ui)
import core
reload(core)

supportedFormat = ['mov','qt','mp4']
class DragDropWidget(QListWidget):
	dropped = Signal(list)
	multipleDropped = Signal(list)
	def __init__(self,*args,**kwargs):
		super(DragDropWidget,self).__init__(*args,**kwargs)
		#add
		self.setWordWrap(True)
		self.setAcceptDrops(True)

	def dragEnterEvent(self,event):
		if event.mimeData().hasUrls:
			event.accept()
		else:
			event.ignore()

	def dragMoveEvent(self,event):
		if event.mimeData().hasUrls:
			event.setDropAction(Qt.CopyAction)
			event.accept()
		else:
			event.ignore()

	def dropEvent(self,event):
		self.clear()
		if event.mimeData().hasUrls:
			event.setDropAction(Qt.CopyAction)
			event.accept()
			links = []
			for url in event.mimeData().urls():
				links.append(str(url.toLocalFile()))

			self.multipleDropped.emit(links)
			for link in links :
				if link.split('.')[-1] not in supportedFormat :
					links.remove(link)
			self.addWidgetItems(links)
		else:
			event.ignore()

	def addWidgetItems(self,items):
		script_path = os.path.dirname(__file__).replace('\\','/')
		image_folder = script_path + '/icon'
		for it in items:
			icon = QIcon("{image_folder}/video.png".format(image_folder=image_folder))
			item = QListWidgetItem(it)
			item.setIcon(icon)
			self.addItem(item)
		
class copyPlayblastTool(QMainWindow,ui.Ui_CopyPlayblast_MainWindow):
	def __init__(self,parent):
		super(copyPlayblastTool,self).__init__(parent)

		#setup UI
		self.setupUi(self)
		self.setWindowTitle(QApplication.translate("CopyPlayblast_MainWindow","Copy Playblast - %s"%core.VERSION,None,-1))
		self.checkName()
		self.updateData()

		self.tmpLayout = QHBoxLayout(self)
		self.tmpLayout.setEnabled(False)

		self.dragDrop_widget = DragDropWidget()
		self.dragDrop_widget.setGeometry(QRect(0,0,320,80))
		sizePolicy = QSizePolicy(QSizePolicy.Expanding,QSizePolicy.Minimum)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.dragDrop_widget.sizePolicy().hasHeightForWidth())
		self.dragDrop_widget.setSizePolicy(sizePolicy)
		self.dragDrop_widget.setMinimumSize(QSize(60,60))
		self.dragDrop_widget.setObjectName("dragDrop_widget")
		brush = QBrush()
		brush.setStyle(Qt.BDiagPattern)
		palette = self.dragDrop_widget.palette()
		palette.setBrush(QPalette.Background,brush)
		self.dragDrop_widget.setPalette(palette)

		self.button_dragDropArea = QScrollArea(self.dragDrop_groupBox)
		self.button_dragDropArea.setWidgetResizable(True)
		self.button_dragDropArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
		self.button_dragDropArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
		self.button_dragDropArea.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
		self.button_dragDropArea.setObjectName("button_dragDropArea")
		self.button_dragDropArea.setWidget(self.dragDrop_widget)
		self.dragDrop_layout.addWidget(self.button_dragDropArea)

		self.open_pushButton.clicked.connect(self.openWorkPath)
		self.name_pushButton.clicked.connect(self.submitName)
		self.dragDrop_widget.itemSelectionChanged.connect(self.updateNameSelected)
		self.dragDrop_widget.itemDoubleClicked.connect(self.doubleClick)
		self.copy_pushButton.clicked.connect(self.copy2WorkPath)

	def updateData(self):
		workspace = pm.workspace(q=True,rd=True)
		self.project = workspace.split('/')[1]
		self.act = workspace.split('/')[2]
		self.shot = workspace.split('/')[3]
		self.name = self.name_lineEdit.text()
		workPath = "P:/{project}/scene/work/{act}/{shot}/sim/{name}/output".format(project=self.project,act=self.act,shot=self.shot,name=self.name)
		self.path_lineEdit.setText(workPath)
		self.createFolder(workPath)

	def createFolder(self,path=''):
		if not os.path.exists(path):
			os.makedirs(path)

	def openWorkPath(self):
		path = self.path_lineEdit.text()
		if os.path.exists(path):
			os.startfile(path)

	def updateNameSelected(self):
		path = self.path_lineEdit.text()
		version = self.checkVersion(path)
		name = "{path}/{shot}_sim_{name}.v{version}.mov".format(path=path,shot=self.shot,name=self.name,version=version)
		self.copy_lineEdit.setText(name)

	def checkVersion(self,path=''):
		if os.path.exists(path):
			if not os.listdir(path) == []:
				item = os.listdir(path)[-1]
				version = '%03d'%(int(item.split('.')[-2].split('v')[1])+1)
			else :
				version = '%03d'%(int(1))
		return version

	def doubleClick(self):
		item = QListWidgetItem(self.dragDrop_widget.currentItem())
		path = item.text()
		os.startfile(path)

	def copy2WorkPath(self):
		item = QListWidgetItem(self.dragDrop_widget.currentItem())
		path = item.text()
		wanted_path = self.path_lineEdit.text()
		# if not os.path.exists(wanted_path):
		# 	os.makedirs(wanted_path)
		self.createFolder(wanted_path)
		wanted_result = self.copy_lineEdit.text()
		copy = shutil.copy2(path,wanted_result)
		self.updateNameSelected()

	def checkName(self):
		documentPath = os.path.expanduser('~')
		path = documentPath + '/samScriptData'
		# if not os.path.exists(path):
		# 	os.makedirs(path)
		self.createFolder(path)
		nameFile = "{path}/copyPlayblastTool.txt".format(path=path)
		if os.path.exists(nameFile):
			file = open("{path}/copyPlayblastTool.txt".format(path=path),"r")
			name = file.read()
			self.name_lineEdit.setText(name)
			self.open_pushButton.setEnabled(True)
			self.copy_pushButton.setEnabled(True)
			self.name_lineEdit.setEnabled(False)
			self.name_pushButton.setText('Change')
			file.close()

	def submitName(self):
		documentPath = os.path.expanduser('~')
		path = documentPath + '/samScriptData'
		nameFile = "{path}/copyPlayblastTool.txt".format(path=path)
		if self.name_pushButton.text()=='Submit':
			name = self.name_lineEdit.text()
			file = open("{path}/copyPlayblastTool.txt".format(path=path),"w")
			file.write(name)
			file.close()
			self.name_pushButton.setText('Change')
			self.name_lineEdit.setEnabled(False)
			self.copy_pushButton.setEnabled(True)
			self.open_pushButton.setEnabled(True)
			self.updateData()
		elif self.name_pushButton.text()=='Change':
			self.name_pushButton.setText('Submit')
			self.name_lineEdit.setEnabled(True)
			self.copy_pushButton.setEnabled(False)
			self.open_pushButton.setEnabled(False)



