import os
import maya.mel as mel
import maya.cmds as mc
import pymel.core as pm
from rf_utils.sg import sg_utils
from rftool.utils.ui import maya_win
from rf_utils.ui import load as loadUi
from rftool.preload import duration_check
from rf_utils.context import context_info
from Qt import wrapInstance, QtGui, QtCore, QtWidgets
#loadPlugin
pm.loadPlugin ( 'redshift4maya.mll' )
pm.pluginInfo ( 'redshift4maya.mll' , e = True , a = True )
#ui
script_path = os.path.abspath(__file__).replace('\\','/')
uiPath = script_path.split(script_path.split('.')[-1])[0]+'ui'
form_class,base_class = loadUi.loadUiType(uiPath)
#script
class Light2Ren(form_class , base_class):
	#auto_data
	def __init__ (self,parent):
		super(Light2Ren,self).__init__(parent)
		self.setupUi(self)
		#default data
		self.refresh_program()
		#connection
		self.refresh_action.triggered.connect(self.refresh_program)
		self.light_button.clicked.connect(self.swith_light)
		self.refresh_button.clicked.connect(self.update_cam)
		self.browse_button.clicked.connect(self.update_imageFolder)
		self.anim_enable.stateChanged.connect(self.set_animation)
		self.anim_enable.stateChanged.connect(self.anim_time_enable)
		self.anim_enable.stateChanged.connect(self.result_name)
		self.file_name_text.textChanged.connect(self.set_file_name_prefix)
		self.render_open.clicked.connect(self.open_imageFolder)
		self.batch_button.clicked.connect(self.batch_render)
		self.file_name_text.textChanged.connect(self.set_file_name_prefix)
		self.file_name_text.textChanged.connect(self.set_file_name_prefix)
		self.image_format_combo.currentIndexChanged.connect(self.update_image_format)
		self.image_format_combo.currentIndexChanged.connect(self.result_name)
		self.camera_list_widget.itemChanged.connect(self.cam_status)
		self.anim_start.textChanged.connect(self.start_change)
		self.anim_start.textChanged.connect(self.result_name)
		self.anim_start.textChanged.connect(self.real_time_timerange)
		self.anim_end.textChanged.connect(self.end_change)
		self.anim_end.textChanged.connect(self.real_time_timerange)
		self.anim_timerange.stateChanged.connect(self.anim_timerange_data)
		self.anim_timerange.stateChanged.connect(self.update_con)
		self.skip_anim.stateChanged.connect(self.skip_exists)
		self.con_timerange.stateChanged.connect(self.continous_frame)
		self.show()
	#refresh_program
	def refresh_program(self):
		self.workspace = pm.workspace(q=True,rd=True)
		self.fileMaya = mc.file(q=True,sn=True,shn=True)
		if self.fileMaya == '':
			self.fileMaya = 'untitled'
		self.typeFile = mc.file(q=True,typ=True)
		#cam
		self.all_cam = pm.ls(typ='camera')
		self.basic_cam = ['backShape','bottomShape','frontShape','leftShape','perspShape','sideShape','topShape']	
		for cam in self.all_cam :
			if cam in self.basic_cam :
				cam = pm.PyNode(cam)
				cam.renderable.set(0)
		#set render setting
		pm.setAttr('defaultRenderGlobals.currentRenderer','redshift')
		pm.setAttr('defaultRenderGlobals.animation',1)
		#set format and set unified sampling
		try :
			pm.setAttr('redshiftOptions.imageFormat',4)
			pm.setAttr('redshiftOptions.unifiedMinSamples',32)
			pm.setAttr('redshiftOptions.unifiedMaxSamples',64)
		except pm.MayaAttributeError:
			pm.mel.eval('unifiedRenderGlobalsWindow')
		#update start-end cam time
		project = self.workspace.split('/')[1]
		self.lightPath = 'P:/{project}/asset/publ/_global/light/hero/lookDevLight.hero.ma'.format(project=project)
		entity  = self.workspace.split('/')[3]
		context = context_info.Context()
		context.use_sg ( sg_utils.sg,project,'scene',entityName=entity)
		scene = context_info.ContextPathInfo(context=context)
		sgDuration = duration_check.get_duration(scene)
		start = scene.projectInfo.scene.start_frame
		self.anim_start_data = start
		end = start+(sgDuration-1)
		self.anim_end_data = end
		pm.setAttr('defaultRenderGlobals.startFrame',start)
		pm.setAttr('defaultRenderGlobals.endFrame',end)
		#set Resolution
		pm.setAttr('defaultResolution.width',1920)
		pm.setAttr('defaultResolution.height',800)
		pm.setAttr('defaultResolution.deviceAspectRatio',2.4)
		pm.setAttr('defaultResolution.pixelAspect',1.1)
		self.redshift_options()
		self.add_menu_image_format()
		self.check_data()
		self.check_reference()
		self.check_animation()
		self.check_start_end()
		self.check_imageFolder()
		self.get_file_name_prefix()
		self.continous_frame()
		self.anim_time_enable()
		self.result_name()
		self.result_path()
		self.update_cam()
		self.real_time_timerange()
	#redshift_setting
	def redshift_options(self):
		#set format and set unified sampling
		pm.setAttr('redshiftOptions.imageFormat',4)
		pm.setAttr('redshiftOptions.unifiedMinSamples',32)
		pm.setAttr('redshiftOptions.unifiedMaxSamples',64)
	#real_time_update
	def real_time_timerange(self):
		if self.anim_start.text() != str(int(self.anim_start_data)) or self.anim_end.text() != str(int(self.anim_end_data)):
			self.anim_timerange.setCheckState(QtCore.Qt.Unchecked)
		else :
			self.anim_timerange.setCheckState(QtCore.Qt.Checked)
		if self.anim_start.text() != str(int(self.wanted_start)) or self.anim_end.text() != str(int(self.wanted_end)):
			self.con_timerange.setCheckState(QtCore.Qt.Unchecked)
		else :
			self.con_timerange.setCheckState(QtCore.Qt.Checked)
	#anim_timerange
	def anim_timerange_data(self):
		if self.anim_timerange.isChecked():
			self.anim_start.setText(str(int(self.anim_start_data)))
			self.anim_end.setText(str(int(self.anim_end_data)))
	#change_timerange
	def start_change(self):
		start = self.anim_start.text()
		pm.setAttr('defaultRenderGlobals.startFrame',int(start))
	def end_change(self):
		end = self.anim_end.text()
		pm.setAttr('defaultRenderGlobals.endFrame',int(end))
	#anim_enable
	def anim_time_enable(self):
		anim_status = self.anim_enable.isChecked()
		if anim_status == True:
			self.anim_start.setEnabled(True)
			self.anim_end.setEnabled(True)
			self.anim_timerange.setEnabled(True)
			self.con_timerange.setEnabled(True)
			self.real_time_timerange()
		else :
			self.anim_start.setEnabled(False)
			self.anim_end.setEnabled(False)
			self.anim_timerange.setCheckState(QtCore.Qt.Unchecked)
			self.anim_timerange.setEnabled(False)
			self.con_timerange.setCheckState(QtCore.Qt.Unchecked)
			self.con_timerange.setEnabled(False)
	#continous_frame
	def continous_frame(self):
		name = self.file_name_text.text()
		start = self.anim_start.text()
		end = self.anim_end.text()
		timeRange = range(int(start),int(end)+1)
		render_path = self.render_path_text.text()
		path = self.workspace + render_path
		output_list = os.listdir(path)
		#typeFile
		if self.typeFile == [u'mayaAscii']:
			typeFile = '.ma'
		elif self.typeFile == [u'mayaBinary']:
			typeFile = '.mb'
		#image_format
		image_format = self.image_format_combo.currentText()
		if image_format == 'JPEG(jpg)':
			image_format = '.jpg'
		if image_format == 'PNG(png)':
			image_format = '.png'
		name = self.fileMaya.split(typeFile)[0]
		timeRange = range(int(self.anim_start_data),int(self.anim_end_data)+1)
		all_output = []
		wanted_output = []
		time_wanted = []
		for time in timeRange:
			all_output.append(name+'.'+str(time)+image_format)
		for alls in all_output:
			if alls not in output_list:
				wanted_output.append(alls)
		for want in wanted_output:
			#time_wanted.append(int(want.split(name)[1].split('.')[1].split(image_format)[0]))
			time_wanted.append(int(want.split('.')[-2]))
		#print time_wanted
		if time_wanted == []:
			self.wanted_start = int(self.anim_start_data)
			self.wanted_end = int(self.anim_end_data)
		else :
			self.wanted_start = time_wanted[0]
			self.wanted_end = time_wanted[-1]
		if self.con_timerange.isChecked():
			self.anim_start.setText(str(self.wanted_start))
			self.anim_end.setText(str(self.wanted_end))
	#skip_exists
	def skip_exists(self):
		if self.skip_anim.isChecked()==True:
			pm.setAttr('redshiftOptions.skipExistingFrames',1)
		else :
			pm.setAttr('redshiftOptions.skipExistingFrames',0)
	#update_con
	def update_con(self):
		if self.anim_timerange.isChecked():
			self.con_timerange.setCheckState(QtCore.Qt.Unchecked)
	#update_cam
	def update_cam(self):
		self.camera_list_widget.clear()
		all_cam = pm.ls(typ='camera')
		wanted_cam = []
		for cam in all_cam:
			if cam not in self.basic_cam:
				wanted_cam.append(str(cam))
		for wanted in wanted_cam:
			item = QtWidgets.QListWidgetItem(wanted)
			item.setFlags(item.flags()|QtCore.Qt.ItemIsUserCheckable)
			if pm.getAttr(wanted+'.renderable')==0:
				item.setCheckState(QtCore.Qt.Unchecked)
			elif pm.getAttr(wanted+'.renderable')==1:
				item.setCheckState(QtCore.Qt.Checked)
			self.camera_list_widget.addItem(item)
	#camera_change_status
	def cam_status(self):
		all_cam = pm.ls(typ='camera')
		all_cam_name = [cam.nodeName() for cam in all_cam]
		check_dict = {QtCore.Qt.Unchecked:0,QtCore.Qt.Checked:1}
		for count in xrange(self.camera_list_widget.count()):
			item = self.camera_list_widget.item(count)
			name = str(item.text())
			if name in all_cam_name:
				index = all_cam_name.index(name)
				cam = all_cam[index]
				status = item.checkState()
				cam.renderable.set(check_dict[status])
	#render_open
	def open_imageFolder(self):
		imageFolder = self.path_label.text().split('Path : ')[1]
		os.startfile(imageFolder)
	#render_path
	def check_imageFolder(self):
		defaul_imageFolder = pm.workspace(fre='images')
		self.render_path_text.setText(defaul_imageFolder)
	#update_path_label
	def result_path(self):
		render_path_text = self.render_path_text.text()
		path = self.workspace + render_path_text
		self.path_label.setText('Path : '+path)
	#update_name_label
	def result_name(self):
		name = self.file_name_text.text()
		image_format = self.image_format_combo.currentText()
		if image_format == 'JPEG(jpg)':
			image_format = 'jpg'
		if image_format == 'PNG(png)':
			image_format = 'png'
		if name == '(not set; using scene name)':
			if self.typeFile == [u'mayaAscii']:
				typeFile = '.ma'
			elif self.typeFile == [u'mayaBinary']:
				typeFile = '.mb'
			name = self.fileMaya.split(typeFile)[0]
		if name == '' :
			if self.typeFile == [u'mayaAscii']:
				typeFile = '.ma'
			elif self.typeFile == [u'mayaBinary']:
				typeFile = '.mb'
			name = self.fileMaya.split(typeFile)[0]
		if self.anim_enable.isChecked() == True:
			time = self.anim_start.text().split('.')[0]
			self.name_label.setText('Name : '+name+'.'+time+'.'+image_format)
		else:
			self.name_label.setText('Name : '+name+'.'+image_format)
	#add_menu_image_format
	def add_menu_image_format(self):
		image_format = ['PNG(png)','JPEG(jpg)']
		self.image_format_combo.addItems(image_format)
	#update_image_format
	def update_image_format(self):
		image_format = self.image_format_combo.currentText()
		if image_format == 'PNG(png)':
			pm.setAttr('redshiftOptions.imageFormat',2)
		if image_format == 'JPEG(jpg)':
			pm.setAttr('redshiftOptions.imageFormat',4)
	#check_data
	def check_data(self):
		render_using = pm.getAttr('defaultRenderGlobals.currentRenderer')
		self.render_using_text.setText(render_using)
		image_format = pm.getAttr('redshiftOptions.imageFormat')
		if image_format == 2 :
			self.image_format_combo.setCurrentText('PNG(png)')
		if image_format == 4 :
			self.image_format_combo.setCurrentText('JPEG(jpg)')
		#check_resolution
		reso_width = pm.getAttr('defaultResolution.width')
		self.reso_width.setText(str(reso_width))
		reso_height = pm.getAttr('defaultResolution.height')
		self.reso_height.setText(str(reso_height))
		#check_unified_sampling
		unifiedMin = pm.getAttr('redshiftOptions.unifiedMinSamples')
		self.sampling_min.setText(str(unifiedMin))
		unifiedMax = pm.getAttr('redshiftOptions.unifiedMaxSamples')
		self.sampling_max.setText(str(unifiedMax))
	#check_start_end
	def check_start_end(self):
		start = pm.getAttr('defaultRenderGlobals.startFrame')
		self.anim_start.setText(str(int(start)))
		end = pm.getAttr('defaultRenderGlobals.endFrame')
		self.anim_end.setText(str(int(end)))
	#check_animation
	def set_animation(self):
		anim_status = self.anim_enable.isChecked()
		if anim_status == True:
			pm.setAttr('defaultRenderGlobals.animation',1)
		else :
			pm.setAttr('defaultRenderGlobals.animation',0)
	#set_animation
	def check_animation(self):
		anim_status = pm.getAttr('defaultRenderGlobals.animation')
		if anim_status == True:
			self.anim_enable.setChecked(True)
		else :
			self.anim_enable.setChecked(False)
	#check_light_reference
	def check_reference(self):
		if self.lightPath not in pm.listReferences():
			self.light_button.setStyleSheet("background-color:None")
		else :
			self.light_button.setStyleSheet("background-color:#008000")
	#get_name
	def get_file_name_prefix(self):
		get_name = pm.getAttr ('defaultRenderGlobals.imageFilePrefix')
		if get_name == '' or 'None':
			self.file_name_text.setText('(not set; using scene name)')
		else :
			self.file_name_text.setText(get_name)
	#set_name
	def set_file_name_prefix(self):
		set_name = self.file_name_text.text()
		if set_name == '(not set; using scene name)' or '' :
			pm.setAttr('defaultRenderGlobals.imageFilePrefix',u'')
		else :
			pm.setAttr('defaultRenderGlobals.imageFilePrefix',set_name)
		self.result_name()
	#reference light
	def reference_light(self):
		pm.createReference(self.lightPath,namespace='assetLight')
	#swith_light_cmd
	def swith_light(self):
		if self.lightPath in pm.listReferences():
			for ref in pm.listReferences():
				if str(ref.path) == self.lightPath:
					ref.remove()
		else :
			self.reference_light()
		self.check_reference()
	#update image folder
	def update_imageFolder(self):
		selected_folder = pm.fileDialog2(cap='Select Render Location',fm=3,okc='Select',dir=self.workspace)[0]
		len_different = len(self.workspace)-len(selected_folder)
		wanted_len = len(selected_folder)+len_different
		for word in range(len(selected_folder)):
			wanted_word = []
			wanted_word.append(selected_folder[wanted_len:])
		self.render_path_text.setText(wanted_word[0])
		pm.workspace(fr=['images',wanted_word[0]])
		self.result_path()
	#batch render
	def batch_render(self):
		pm.mel.eval('mayaBatchRender()')

def show () :
	global mainUI
	try:
		mainUI.close()
	except Exception, e:
		pass

	mainUI = Light2Ren ( parent = maya_win.getMayaWindow() )
	return mainUI
show ()

