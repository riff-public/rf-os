import pymel.core as pm
import maya.OpenMaya as om



class softClusterMaker ( object ) :

	def __init__ ( self ) :
		pass

	def softCluster_cmd ( self , *args ) :
		solfSel_value = pm.softSelect ( q = True , sse = True )
		if solfSel_value == 1 :
			sel = om.MSelectionList ()
			try :
				softSel = om.MRichSelection ()
				om.MGlobal.getRichSelection ( softSel )
				softSel.getSelection ( sel )
			except RuntimeError :
				raise RuntimeError ( 'no components are selected' )

			dagPath = om.MDagPath ()
			sel.getDagPath ( 0 , dagPath )
			selLocs = pm.xform ( pm.ls ( sl = True ) , q = True , t = True , ws = True )
			selLen = float ( len ( selLocs [ 0::3 ] ) )
			selSums = [ sum ( selLocs [ 0::3 ] ) , sum ( selLocs [ 1::3 ] ) , sum ( selLocs [ 2::3 ] ) ]
			selPivot = [ selSums [ 0 ] / selLen , selSums [ 1 ] / selLen , selSums [ 2 ] / selLen ]
			component = om.MObject ()
			print dagPath.apiType ()

			softSel_mode = pm.softSelect ( q = True , ssf = True )
			if softSel_mode in ( 0 , 1 ) :
				if dagPath.apiType () == 296 :
					objIter = om.MItSelectionList ( sel , om.MFn.kMeshVertComponent )
					compType = 'vtx'
				elif dagPath.apiType () == 267 :
					objIter = om.MItSelectionList ( sel , om.MFn.kCurveCVComponent )
					compType = 'cv'
				elif dagPath.apiType () == 279 :
					objIter = om.MItSelectionList ( sel , om.MFn.kLatticeComponent )
					compType = 'pt'

				elements = []
				elements_lat = []
				lat = []
				while not objIter.isDone () :
					objIter.getDagPath ( dagPath, component )
					if dagPath.apiType () in ( 296 , 267 ) :
						dagPath.pop ()
						node = dagPath.fullPathName ()
						fnComp = om.MFnSingleIndexedComponent ( component )     
						for i in range ( fnComp.elementCount () ) :
							elements.append ( [ node , compType , fnComp.element(i) , fnComp.weight(i).influence() ] )
					elif dagPath.apiType () == 279 :
						dagPath.pop ()
						node = dagPath.fullPathName ()
						u = om.MIntArray ()
						v = om.MIntArray ()
						w = om.MIntArray ()
						fnComp = om.MFnTripleIndexedComponent ( component )
						fnComp.getElements ( u , v ,w )
						for i in range ( u.length () ) :
							lat.append ( '[%i][%i][%i]' % ( u[i] , v[i] , w[i] ) )
							elements_lat.append ( [ node , compType , lat [ i ] , fnComp.weight ( i ).influence () ] )
					objIter.next ()

				softElement = [ '%s.%s[%s]' % ( el [ 0 ] , el [ 1 ] , el [ 2 ] ) for el in elements ]
				softElement2 = [ '%s.%s%s' % ( el [ 0 ] , el [ 1 ] , el [ 2 ] ) for el in elements_lat ]
				pm.select ( softElement , r = True )
				pm.select ( softElement2 , add = True )
				cluster = pm.cluster ( n = 'softCluster#' , rel = True )
				cluster [ 1 ].addAttr ( 'envelope' , k = True , at = 'float' , dv = 1 , max = 1 , min = 0 )
				cluster [ 1 ].envelope >> cluster [ 0 ].envelope
				cluster [ 1 ].origin.set ( selPivot )
				pm.xform ( cluster [ 1 ] , piv = selPivot )

				for i in range ( len ( softElement ) ) :
					pm.percent ( cluster [ 0 ] , softElement [ i ] , v = elements [ i ] [ 3 ] )
				for i in range ( len ( softElement2 ) ) :
					pm.percent ( cluster [ 0 ] , softElement2 [ i ] , v = elements_lat [ i ] [ 3 ] )
				pm.select ( cluster [ 1 ] , r = True )
		
			elif softSel_mode in ( 2 , 3 ) :
				print True
				objIter = om.MItSelectionList ( sel , om.MFn.kMeshVertComponent )
				compType = 'vtx'
				elements = []
				lat = []
				elements_lat = []
				while not objIter.isDone () :
					objIter.getDagPath ( dagPath , component )
					dagPath.pop ()
					node = dagPath.fullPathName ()
					fnComp = om.MFnSingleIndexedComponent ( component )
					for i in range ( fnComp.elementCount () ) :
						elements.append ( [ node , compType , fnComp.element(i) , fnComp.weight(i).influence() ] )
					objIter.next ()

				objIter2 = om.MItSelectionList ( sel , om.MFn.kCurveCVComponent )
				compType = 'cv'
				while not objIter2.isDone () :
					objIter2.getDagPath ( dagPath , component )
					dagPath.pop ()
					node = dagPath.fullPathName ()
					fnComp = om.MFnSingleIndexedComponent ( component )
					for i in range ( fnComp.elementCount () ) :
						elements.append ( [ node , compType , fnComp.element(i) , fnComp.weight(i).influence() ] )
					objIter2.next ()

				objIter3 = om.MItSelectionList ( sel , om.MFn.kLatticeComponent )
				compType = 'pt'
				while not objIter3.isDone () :
					objIter3.getDagPath ( dagPath , component )
					dagPath.pop ()
					node = dagPath.fullPathName ()
					u = om.MIntArray ()
					v = om.MIntArray ()
					w = om.MIntArray ()
					fnComp = om.MFnTripleIndexedComponent ( component )
					fnComp.getElements ( u , v ,w )
					for i in range ( u.length () ) :
						lat.append ( '[%i][%i][%i]' % ( u[i] , v[i] , w[i] ) )
						elements_lat.append ( [ node , compType , lat [ i ] , fnComp.weight ( i ).influence () ] )
					objIter3.next ()

				softElement = [ '%s.%s[%s]' % ( el [ 0 ] , el [ 1 ] , el [ 2 ] ) for el in elements ]
				softElement2 = [ '%s.%s%s' % ( el [ 0 ] , el [ 1 ] , el [ 2 ] ) for el in elements_lat ]
				pm.select ( softElement , r = True )
				pm.select ( softElement2 , add = True )
				cluster = pm.cluster ( n = 'softCluster#' , rel = True )
				cluster [ 1 ].addAttr ( 'envelope' , k = True , at = 'float' , dv = 1 , max = 1 , min = 0 )
				cluster [ 1 ].envelope >> cluster [ 0 ].envelope
				cluster [ 1 ].origin.set ( selPivot )
				pm.xform ( cluster [ 1 ] , piv = selPivot )

				for i in range ( len ( softElement ) ) :
					pm.percent ( cluster [ 0 ] , softElement [ i ] , v = elements [ i ] [ 3 ] )
				for i in range ( len ( softElement2 ) ) :
					pm.percent ( cluster [ 0 ] , softElement2 [ i ] , v = elements_lat [ i ] [ 3 ] )
				pm.select ( cluster [ 1 ] , r = True )
		
		elif solfSel_value == 0 :
			cluster = pm.cluster ( n = 'softCluster#' , rel = True )
			cluster [ 1 ].addAttr ( 'envelope' , k = True , at = 'float' , dv = 1 , max = 1 , min = 0 )
			cluster [ 1 ].envelope >> cluster [ 0 ].envelope 
				
softClusterMaker ().softCluster_cmd ()