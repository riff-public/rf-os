import pymel.core as pm
class selectCL ( object ) :
    def __init__ ( self ) :
        pass

    def listClus ( self , *args ) :
        self.removeList ()
        target = pm.textField ( self.clus_txtField , q = True , tx = True )
        if not target == 'No Model Defined' :
            selected =  pm.ls ( target )
            clusDict = []
            for i in selected :
                shp = i.listRelatives ()
                for i in shp :
                    connect = i.listConnections ( t = 'groupId' )
                    for i in connect :
                        split = i.split ( 'GroupId' ) [ 0 ]
                        allClus = split + 'Handle'
                        clusDict.append ( allClus )
                        for i in clusDict :
                            if pm.objExists ( i ) :
                                if 'DefHandle' in i :
                                    clusDict.remove ( i )
                            if 'groupId' in i :
                                clusDict.remove ( i )
            useClus = pm.ls ( clusDict )
            text_clus_list = pm.textScrollList ( self.clus_txtScrll , q = True , ai = True )
            for i in text_clus_list :
                if not pm.objExists ( i ) :
                    pm.textScrollList ( self.clus_txtScrll , e = True , ri = True )
            for i in useClus :
                if i not in text_clus_list :
                    pm.textScrollList ( self.clus_txtScrll , e = True , a = i )

    def selectTarget ( self , *args ) :
        pm.select ( cl = True )
        selected = pm.textScrollList ( self.clus_txtScrll , q = True , si = True )
        cluster = pm.ls ( selected )
        for i in cluster :
            pm.select ( i , tgl = True )

    def removeList ( self , *args ) :
        pm.textScrollList ( self.clus_txtScrll , e = True , ra = True )

    def selectModel ( self , *args ) :
        target = pm.selected ()
        if not target == [] :
            pm.textField ( self.clus_txtField , e = True , tx = target [ 0 ] )
        self.listClus()

    def selectUnkey ( self , *args ) :
        self.listClus ()
        pm.select ( cl = True )
        selected = pm.textScrollList ( self.clus_txtScrll , q = True , ai = True )
        cluster = pm.ls ( selected )
        for i in cluster :
            if i.tx.get() == 0 and i.ty.get() == 0 and i.tz.get() == 0 and i.rx.get() == 0 and i.ry.get() == 0 and i.rz.get() == 0 and i.sx.get() == 1 and i.sy.get() == 1 and i.sz.get() == 1 :
                pm.select ( i , tgl = True )
        choose = pm.selected ()
        pm.textScrollList ( self.clus_txtScrll , e = True , ra = True )
        pm.textScrollList ( self.clus_txtScrll , e = True , a = choose )
                
    def selectKey ( self , *args ) :
        self.listClus ()
        pm.select ( cl = True )
        selected = pm.textScrollList ( self.clus_txtScrll , q = True , ai = True )
        cluster = pm.ls ( selected )
        for i in cluster :
            if i.tx.get() != 0 or i.ty.get() != 0 or i.tz.get() != 0 or i.rx.get() != 0 or i.ry.get() != 0 or i.rz.get() != 0 or i.sx.get() != 1 or i.sy.get() != 1 or i.sz.get() != 1 :
                pm.select ( i , tgl = True )
        choose = pm.selected ()
        pm.textScrollList ( self.clus_txtScrll , e = True , ra = True )
        pm.textScrollList ( self.clus_txtScrll , e = True , a = choose )

    def selectEnable ( self , *args ) :
        self.listClus ()
        pm.select ( cl = True )
        selected = pm.textScrollList ( self.clus_txtScrll , q = True , ai = True )
        cluster = pm.ls ( selected )
        for i in cluster :
            if i.envelope.get() == 1 :
                pm.select ( i , tgl = True )
        choose = pm.selected ()
        pm.textScrollList ( self.clus_txtScrll , e = True , ra = True )
        pm.textScrollList ( self.clus_txtScrll , e = True , a = choose )

    def selectDisable ( self , *args ) :
        self.listClus ()
        pm.select ( cl = True )
        selected = pm.textScrollList ( self.clus_txtScrll , q = True , ai = True )
        cluster = pm.ls ( selected )
        for i in cluster :
            if i.envelope.get() == 0 :
                pm.select ( i , tgl = True )
        choose = pm.selected ()
        pm.textScrollList ( self.clus_txtScrll , e = True , ra = True )
        pm.textScrollList ( self.clus_txtScrll , e = True , a = choose )

    def selectAll ( self , *args ) :
        self.listClus ()
        pm.select ( cl = True )
        selected = pm.textScrollList ( self.clus_txtScrll , q = True , ai = True )
        cluster = pm.ls ( selected )
        for i in cluster :
            pm.select ( i , tgl = True )

    def ui ( self , *args ) :
        w = 300.00
        if pm.window ( 'sam_ClusSl' , ex = True ) :
            pm.deleteUI ( 'sam_ClusSl' )
        window = pm.window ( 'sam_ClusSl' , t = 'cluster_selector' , tb = True , mnb = True , mxb = True , s = False , rtf = True )
        pm.window ( 'sam_ClusSl' , e = True , w = w , h = 10 )
        with window :
            with pm.rowColumnLayout ( nc = 1 , adj = True ) :
                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w - 100 ) , ( 2 , w - 200 ) ] , adj = True ) :
                    self.clus_txtField = pm.textField ( tx = 'No Model Defined' , en = True , ed = False )
                    pm.button ( l = 'Select Model' , c = self.selectModel , bgc = ( 1 , 0.8745 , 0.7294 ) )
                pm.separator ( vis = True , h = 5 )
                with pm.rowColumnLayout ( nc = 1 , adj = True ) :
                    self.clus_txtScrll = pm.textScrollList ( w = w , h = 500 , dcc = self.selectTarget , ams = True )
                pm.separator ( vis = True , h = 5 )
                with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w - 150 ) , ( 2 , w - 150 ) ] , adj = True ) :
                    pm.button ( l = 'transform Selector' , c = self.selectKey , bgc = ( 0.9216 , 0.6 , 1 ) )
                    pm.button ( l = "transform 0 Selector" , c = self.selectUnkey , bgc = ( 1 , 0.7333 , 0.6 ) )
                with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , w - 200 ) , ( 2 , w - 200 ) , ( 3 , w - 200 ) ] , adj = True ) :
                    pm.button ( l = 'Env. 1 Selecter' , c = self.selectEnable , bgc = ( 0.7294 , 1 , 0.7882 ) )
                    pm.button ( l = 'Env. 0 Selecter' , c = self.selectDisable , bgc = ( 0.7294 , 1 , 1 ) )
                    pm.button ( l = 'All Selector' , c = self.selectAll , bgc = ( 1 , 0.7019 , 0.7294 ) )
                pm.separator ( vis = True , h = 5 )
                with pm.rowColumnLayout ( nc = 1 , adj = True ) :
                    pm.button ( l = 'Refresh' , c = self.listClus , bgc = ( 1 , 0.8745 , 0.7294 ) , w = w )
        self.listClus ()

selectCL().ui()



