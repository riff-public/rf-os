import pymel.core as pm
import maya.OpenMaya as om
import distutils
import os

class softCluseterMaker ( object ) :

	def __init__ ( self ) :
		self.file = os.path.abspath ( __file__ )
		self.icons ()

	def icons ( self , *args ) :
		path = os.path.dirname ( self.file )
		iconFolder = path + '/icons'
		document = os.path.expanduser ( '~' )
		program = pm.about ( a = True )
		version = pm.about ( v = True )
		iconMayaPath = document + '/' + program + '/' + version + '/prefs/icons'
		distutils.dir_util.copy_tree ( iconFolder , iconMayaPath )

	def softCluster_make ( self , *args ) :
		solfSelect_value = pm.softSelect ( q = True , sse = True )
		if solfSelect_value == True :
			sel = om.MSelectionList ()
			try :
				softSel = om.MRichSelection ()
				om.MGlobal.getRichSelection( softSel )
				softSel.getSelection ( sel )
			except RuntimeError :
				raise RuntimeError ( 'no components are selected' )

			dag = om.MDagPath ()
			sel.getDagPath ( 0 , dag )
			selLocs = pm.xform ( pm.ls ( sl = True ) , q = True , t = True , ws = True )
			selLen = float ( len ( selLocs [ 0::3 ] ) )
			selSums = [ sum ( selLocs [ 0::3 ] ) , sum ( selLocs [ 1::3 ] ) , sum ( selLocs [ 2::3 ] ) ]
			selPivot = [ selSums [ 0 ] / selLen , selSums [ 1 ] / selLen , selSums [ 2 ] / selLen ]
			print dag.apiType ()

			if dag.apiType () == 296 :
				objIter = om.MItSelectionList ( sel , om.MFn.kMeshVertComponent )
				componentType = 'vtx'
			elif dag.apiType () == 267 :
				objIter = om.MItSelectionList ( sel , om.MFn.kCurveCVComponent )
				componentType = 'cv'
			elif dag.apiType () == 279 :
				objIter = om.MItSelectionList ( sel , om.MFn.kLatticeComponent )
				componentType = 'pt'
			else :
				raise TypeError ( 'object type not supported' )

			elemList = []
			weights = om.MFloatArray ()
			indexList = om.MIntArray ()
			latticeIndexList = []
			components = om.MObject ()
			while not objIter.isDone () :
				objIter.getDagPath ( dag , components )
				if dag.apiType () in ( 296 , 267 ) :
					fnComp = om.MFnSingleIndexedComponent ( components )
					fnComp.getElements ( indexList )
					for i in range ( indexList.length () ) :
						if fnComp.hasWeights () :
							weights.append ( fnComp.weight ( i ).influence () )
						else :
							weights.append ( 1 )
						elemList.append ( '%s.%s[%i]' % ( dag.fullPathName () , componentType , indexList [ i ] ) )
				elif dag.apiType () == 279 :
					u = om.MIntArray ()
					v = om.MIntArray ()
					w = om.MIntArray ()
					fnComp = om.MFnTripleIndexedComponent ( components )
					fnComp.getElements ( u , v , w )
					for i in range ( u.length () ) :
						if fnComp.hasWeights () :
							weights.append ( fnComp.weight ( i ).influence () )
						else :
							weights.append ( 1 )
						latticeIndexList.append ( '[%i][%i][%i]' % ( u [ i ] , v [ i ] , w [ i ] ) )
						elemList.append ( '%s.pt[%i][%i][%i]' % ( dag.fullPathName() , u [ i ] , v [ i ] , w [ i ] ) )
				objIter.next ()

			pm.select ( elemList )
			cluster = pm.cluster ( n = 'softCluseter#' )
			cluster [ 1 ].addAttr ( 'envelope' , k = True , at = 'float' , dv = 1 , max = 1 , min = 0 )
			cluster [ 1 ].envelope >> cluster [ 0 ].envelope 
			cluster [ 1 ].origin.set ( selPivot )
			pm.xform ( cluster [ 1 ] , piv = selPivot )
			i = 0

			if dag.apiType () in [ 296 , 267 ] :
				while  i < weights.length () :
					pm.percent ( cluster [ 0 ] , '%s.%s[%i]' % ( dag.fullPathName () , componentType , indexList [ i ] ) , v = weights [ i ] )
					i += 1
			else :
				while  1 < weights.length () :
					pm.percent ( cluster [ 0 ] , '%s.pt%s' % ( dag.fullPathName () , latticeIndexList [ i ] ) , v = weights [ i ] )
					i += 1

			return cluster

		elif solfSelect_value == False :
			cluster = pm.cluster ( n = 'softCluseter#' )
			cluster [ 1 ].addAttr ( 'envelope' , k = True , at = 'float' , dv = 1 , max = 1 , min = 0 )
			cluster [ 1 ].envelope >> cluster [ 0 ].envelope 

	def solfSelectEnable ( self , *args ) :
		solfSelect_value = pm.checkBox ( self.solfSelect_chkbox , q = True , v = True )
		if solfSelect_value == True :
			pm.softSelect ( sse = True )
			pm.optionMenu ( self.solfSelect_menu , e = True , en = True )
			pm.floatField ( self.radius_float , e = True , en = True )
			pm.floatSlider ( self.radius_slider , e = True , en = True )
		elif solfSelect_value == False :
			pm.softSelect ( sse = False )
			pm.optionMenu ( self.solfSelect_menu , e = True , en = False )
			pm.floatField ( self.radius_float , e = True , en = False )
			pm.floatSlider ( self.radius_slider , e = True , en = False )

	def solfSelectMode ( self , *args ) :
		solfSelect_mode = pm.optionMenu ( self.solfSelect_menu , q = True , v = True )
		if solfSelect_mode == 'Volumn' :
			pm.softSelect ( ssf = 0 )
		elif solfSelect_mode == 'Surface' :
			pm.softSelect ( ssf = 1 )

	def reset ( self , *args ) :
		default = 5
		pm.softSelect ( ssd = default )
		pm.softSelect ( ssf = 0 )
		pm.floatField ( self.radius_float , e = True , v = default )
		pm.floatSlider ( self.radius_slider , e = True , v = default )
		pm.optionMenu ( self.solfSelect_menu , e = True , v = 'Volumn' )

	def slider_converter ( self , *args ) :
		slider_value = pm.floatSlider ( self.radius_slider , q = True , v = True )
		pm.floatField ( self.radius_float , e = True , v = slider_value )
		pm.softSelect ( ssd = slider_value )

	def float_converter ( self , *args ) :
		float_value = pm.floatField ( self.radius_float , q = True , v = True )
		pm.floatSlider ( self.radius_slider , e = True , v = float_value )
		pm.softSelect ( ssd = float_value )

	def ui ( self , *args ) :
		solfSelect_value = pm.softSelect ( q = True , sse = True )
		solfSelect_mode = pm.softSelect ( q = True , ssf = True )
		radius_value = pm.softSelect ( q = True , ssd = True )
		if pm.window ( 'sam_Clus' , ex = True ) :
			pm.deleteUI ( 'sam_Clus' )
		window = pm.window ( 'sam_Clus' , t = 'cluster_maker' , tb = True , mnb = True , mxb = True , s = False , rtf = True )
		pm.window ( 'sam_Clus' , e = True , w = 300 , h = 10 )
		with window :
			with pm.rowColumnLayout ( nc = 1 ) :
				pm.separator ( vis = True , h = 5 )
				with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , 80 ) , ( 2 , 80 ) , ( 3 , 140 ) ] , adj = True ) :
					pm.text ( l = '     Soft Select :' )
					self.solfSelect_chkbox = pm.checkBox ( l = '' , onc = self.solfSelectEnable , ofc = self.solfSelectEnable , v = solfSelect_value )
					pm.text ( l = '' )
				with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , 80 ) , ( 2 , 80 ) , ( 3 , 140 ) ] , adj = True ) :
					pm.text ( l = ' Falloff mode :' )
					if solfSelect_value == True :
						self.solfSelect_menu = pm.optionMenu ( cc = self.solfSelectMode , en = True )
					elif solfSelect_value == False :
						self.solfSelect_menu = pm.optionMenu ( cc = self.solfSelectMode , en = False )
					with self.solfSelect_menu :
						pm.menuItem ( l = 'Volumn' )
						pm.menuItem ( l = 'Surface' )
					if solfSelect_mode == 0 :
						pm.optionMenu ( self.solfSelect_menu , e = True , v = 'Volumn' )
					elif solfSelect_mode == 1 :
						pm.optionMenu ( self.solfSelect_menu , e = True , v = 'Surface' )
					pm.text ( l = '' )
				with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , 80 ) , ( 2 , 80 ) , ( 3 , 140 ) ] , adj = True ) :
					pm.text ( l = 'Falloff radius :' )
					if solfSelect_value == True :
						self.radius_float = pm.floatField ( pre = 2 , v = radius_value , min = 0 , max = 100 , cc = self.float_converter , en = True ) 
						self.radius_slider = pm.floatSlider ( v = radius_value , min = 0 , max = 100 , cc = self.slider_converter , en = True )
					if solfSelect_value == False :
						self.radius_float = pm.floatField ( pre = 2 , v = radius_value , min = 0 , max = 100 , cc = self.float_converter , en = False ) 
						self.radius_slider = pm.floatSlider ( v = radius_value , min = 0 , max = 100 , cc = self.slider_converter , en = False )
				pm.separator ( vis = True , h = 5 )
				with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , 100 ) , ( 2 , 100 ) , ( 3 , 100 ) ] , adj = True ) :
					pm.text ( l = '' )
					self.clus_button = pm.symbolButton ( i = 'star.png' , c = self.softCluster_make , h = 100 )
					pm.text ( l = '' )
				with pm.rowColumnLayout ( nc = 1 ) :
					pm.text ( l = '    Warning : This Script Can not use with Global Falloff Mode' , fn = 'obliqueLabelFont' )
				pm.separator ( vis = True , h = 5 )
				with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , 220 ) , ( 2 , 80 ) ] , adj = True ) :
					pm.text ( l = 'UI Not Update When Change From Maya' , fn = 'obliqueLabelFont' , bgc = ( 1 , 0 , 0 ) )
					pm.button ( l = 'Reset' , c = self.reset )

softCluseterMaker().ui()