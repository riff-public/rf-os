import pymel.core as pm

class SamFXStatus ( object ) :

	def __init__ ( self ) :
		pass
		
	def listNCloth ( self , *args ) :
		nCloth = pm.ls ( typ = 'nCloth' )
		for each in nCloth :
			value = each.getAttr ( 'isDynamic' )
			if not pm.checkBox ( each , ex = True ) :
				on = "pm.setAttr ( '%s.isDynamic' , 1 )" % each
				off = "pm.setAttr ( '%s.isDynamic' , 0 )" % each
				pm.checkBox ( each , l = each , p = self.nClothLayout , ofc = off , onc = on , v = value )
				
	def listNRigid ( self , *args ) :
		nRigid = pm.ls ( typ = 'nRigid' )
		for each in nRigid :
			value = each.getAttr ( 'isDynamic' )
			if not pm.checkBox ( each , ex = True ) :
				on = "pm.setAttr ( '%s.isDynamic' , 1 )" % each
				off = "pm.setAttr ( '%s.isDynamic' , 0 )" % each
				pm.checkBox ( each , l = each , p = self.nRigidLayout , ofc = off , onc = on , v = value )
				
	def listNucleus ( self , *args ) :
		nucleus = pm.ls ( typ = 'nucleus' )
		for each in nucleus :
			value = each.getAttr ( 'enable' )
			if not pm.checkBox ( each , ex = True ) :
				on = "pm.setAttr ( '%s.enable' , 1 )" % each
				off = "pm.setAttr ( '%s.enable' , 0 )" % each
				pm.checkBox ( each , l = each , p = self.nucleusLayout , ofc = off , onc = on , v = value )

	def resize ( self , *args ) :
		pm.window ( 'SamFXStatus' , e = True , h = 10 )
	
	def delete ( self , *args ) :
		nClothLayout = pm.rowColumnLayout ( self.nClothLayout , q = True , ca = True )
		if not nClothLayout == None :
			pm.deleteUI ( nClothLayout )
		nRigidLayout = pm.rowColumnLayout ( self.nRigidLayout , q = True , ca = True )
		if not nRigidLayout == None :
			pm.deleteUI ( nRigidLayout )
		nucleusLayout = pm.rowColumnLayout ( self.nucleusLayout , q = True , ca = True )
		if not nucleusLayout == None :
			pm.deleteUI ( nucleusLayout )

	def reload ( self , *args ) :
		self.delete()
		self.listNCloth()
		self.listNRigid()
		self.listNucleus()

	def UI ( self , *args ) :
		w = 450.00
		if pm.window ( 'SamFXStatus' , exists = True ) :
			pm.deleteUI ( 'SamFXStatus' )
		else : pass
		window = pm.window ( 'SamFXStatus' ,
			t = 'samTool : FXStatus' , tb = True , mnb = True , mxb = True , s = True , rtf = True )
		pm.window ( 'SamFXStatus' , e = True , w = w , h = 10 )
		with window :
			with pm.rowColumnLayout ( nc = 1 , w = w , adj = True ) :
				with pm.frameLayout ( 'nCloth' , cl = False , cll = True , w = w , bgc = ( 119 / 255.00 , 33 / 255.00 , 46 / 255.00 ) , cc = self.resize ) :
					self.nClothLayout = pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w / 2 ) , ( 2 , w / 2 ) ] , adj = True )
			with pm.rowColumnLayout ( nc = 1 , w = w , adj = True ) :
				with pm.frameLayout ( 'nRigid' , cl = False , cll = True , w = w , bgc = ( 42 / 255.00 , 75 / 255.00 , 124 / 255.00 ) , cc = self.resize ) :
					self.nRigidLayout = pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w / 2 ) , ( 2 , w / 2 ) ] , adj = True )
			with pm.rowColumnLayout ( nc = 1 , w = w , adj = True ) :
				with pm.frameLayout ( 'nucleus' , cl = False , cll = True , w = w , bgc = ( 0 / 255.00 , 110 / 255.00 , 81 / 255.00 ) , cc = self.resize ) :
					self.nucleusLayout = pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , w / 2 ) , ( 2 , w / 2 ) ] , adj = True )
			with pm.rowColumnLayout ( nc = 1 , adj = True ) :
				pm.button ( 'reload' , w = w , c = self.reload , bgc = ( 255 / 255.00 , 214 / 255.00 , 98 / 255.00 ) )
		self.reload ()

SamFXStatus().UI()