import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

layoutWidth = 250
width = 237.5

############################################### def #####################################################

############################# def June #################################

def june_setupSim_cmd ( *args ) :
    from tools.artist_tools.tool_june import core
    reload(core)
    core.Ui().show()

def june_playblast_cmd ( *args ) :
    from tools.artist_tools.tool_june.equipment.playblast import playblastSim;
    reload( playblastSim );
    playblastSim.playblastTools();
    playblastSim.setDefaultUI();

def june_abcCache_cmd ( *args ) :
    from tools.artist_tools.tool_june.equipment.abcCache import abcCacheSim;
    reload( abcCacheSim );
    abcCacheSim.cacheTools( );
    abcCacheSim.setDefaultIC( );

def june_rivetTransform_cmd ( *args ) :
    from tools.artist_tools.tool_june.equipment.rivetTransform import rivetTransformTrack;
    reload( rivetTransformTrack );
    rivetTransformTrack.rivetTransform( );

def june_updateShowMenu( *args ) :
    from tools.artist_tools.tool_june.equipment.updateShowMenu import updateShowMenu ;
    reload ( updateShowMenu ) ;
    updateShowMenu.updateShowMenuUI( );

def june_dupAnimFixPrerollPose( *args ) :
    from rf_maya.rftool.sim.tools.artist_tools.tool_june.equipment import helpTools;
    reload( helpTools );
    helpTools.MainHelpTools().dupAnimFixPrerollPose();

############################# def fee #################################

def fee_multipleNucleusCollider_cmd ( *args ) :
    import tools.artist_tools.tool_fee.fee_makeMNCOL as fee_makeMNCOL ;
    reload ( fee_makeMNCOL ) ;
    fee_makeMNCOL.makeMulCOL( ) ;

def fee_colorAssigner_cmd ( *args ) :
    import tools.artist_tools.tool_fee.colorAssigner.core as fee_colorAssigner ;
    reload ( fee_colorAssigner ) ;
    fee_colorAssigner.run ( ) ;

def fee_nucleusSettingUtilities_cmd ( *args ) :
    import tools.artist_tools.tool_fee.fee_nucleusSettingUtilities as fee_nucleusSettingUtilities ;
    reload ( fee_nucleusSettingUtilities ) ;
    fee_nucleusSettingUtilities.run ( ) ;

def fee_addNewNucleus ( *args ) :
    from tools.artist_tools.tool_fee.addNewNucleus import addNewNucleus
    reload ( addNewNucleus )
    addNewNucleus.assNSolvUI()

############################# def sam #################################

def SamColorYeti_cmd ( *args ) :
    from tools.artist_tools.tool_sam.attrYeti import attrYetiData
    reload ( attrYetiData )

def SamYetiExport_cmd ( *args ) :
    from tools.artist_tools.tool_sam.yetiCache import yetiExport
    reload ( yetiExport )

def SamFXStatus_cmd ( *args ) :
    from tools.artist_tools.tool_sam.fxStatus import FXStatus
    reload ( FXStatus )
    
def samWindCtrlKeyframe_cmd ( *args ) :
    from tools.artist_tools.tool_sam.keyWindCtrl import keyframeWind
    reload ( keyframeWind )

def samSavePreset_cmd ( *args ) :
    from tools.artist_tools.tool_sam.preset import corePreset
    reload ( corePreset )

def samAtama_cmd ( *args ) :
    from tools.artist_tools.tool_sam.atama import run
    reload ( run )
    run.mayaRun()

############################# def eye #################################

def setNucleus_cmd(*args):
    from rf_maya.rftool.sim.tools.artist_tools.tool_eye import nuscleusTC
    reload ( nuscleusTC )

def extrudeTube_cmd(*args):
    from rf_maya.rftool.sim.tools.artist_tools.tool_eye import extrudeHairTube_Edit
    reload ( extrudeHairTube_Edit )

def setDynamicNcloth(*args):
    from rf_maya.rftool.sim.tools.artist_tools.tool_eye import nclothUI
    reload ( nclothUI )

############################# def nook #################################


########################################## def space #########################################################

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

##########################################################################################################################################################################################
############################################### main UI #################################################
def run ( *args ) :

    with pm.scrollLayout ( w = layoutWidth , h = 500) :

        with pm.rowColumnLayout ( nc = 1 , w = width ) :

    ################## June #####################
            with pm.frameLayout ( label = "June's Tools" , collapsable = True , collapse = True ,bgc = ( 0.61569 , 0.49804 , 0.61569 ) , w = width ) :

                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , 237.5 ) ] ) :
                    pm.button ( label = "Setup Sim" , c = june_setupSim_cmd ) ;
                    pm.button ( label = "Playblast" , c = june_playblast_cmd ) ;
                    pm.button ( label = "ABC Cache" , c = june_abcCache_cmd ) ;
                    pm.button ( label = "Show Menu" , c = june_updateShowMenu ) ;
                    pm.button ( label = "Duplicate Anim To Fix Preroll Pose" , c = june_dupAnimFixPrerollPose ) ;
            separator ( ) ;
    ##################### Fee #########################
            with pm.frameLayout ( label = "Fee's Tools" , collapsable = True , collapse = True ,bgc = ( 0.42745 , 0.48627 , 0.34118 ) ) :

                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , 237.5 ) ] ) :
                    pm.button ( label = "fee's multiple nucleus collider" , c = fee_multipleNucleusCollider_cmd ) ;
                    pm.button ( label = "fee's nucleus setting utilities" , c = fee_nucleusSettingUtilities_cmd ) ;
                    pm.button ( label = "fee's color assigner" , c = fee_colorAssigner_cmd , w = 237.5 ) ;
            separator ( ) ;
    ##################### Sam #########################
            with pm.frameLayout ( label = "Sam's Tools" , collapsable = True , collapse = True ,bgc = ( 0.7 , 0.48627 , 0.34118 ) ) :

                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , 237.5 ) ] ) :
                    pm.button ( label = "Sam's Asset Yeti" , c = SamColorYeti_cmd ) ;
                    pm.button ( label = "Sam's Yeti Export" , c = SamYetiExport_cmd ) ;
                    pm.button ( label = "Sam's FX Status" , c = SamFXStatus_cmd ) ;
                    pm.button ( label = "Sam's windCtrlKeyframe" , c = samWindCtrlKeyframe_cmd )
                    pm.button ( l = "Sam's SavePreset" , c = samSavePreset_cmd )
                    pm.button ( l = "Atama Project" , c = samAtama_cmd )

            separator ( ) ;
    ##################### eye #########################
            with pm.frameLayout ( label = "Eye's Tools" , collapsable = True , collapse = True ,bgc = (0.443137 ,0.443137, 0.776471) ) :

                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , 237.5 ) ] ) :
                    pm.button ( label = "Set nucleus" , c = setNucleus_cmd , bgc = (0.972549, 0.972549, 1) ) ;
                    pm.button (label = "ExtrueHairTube " , c = extrudeTube_cmd , bgc = (1, 0.980392, 0.803922) )
                    pm.button (label = "set Dynamic Ncloth " , c = setDynamicNcloth , bgc = ( 0.933333 ,0.866667, 0.509804) )

            separator ( ) ;
    ##################### nook #########################
#            with pm.frameLayout ( label = "Nook's Tools" , collapsable = True , collapse = True ,bgc = (0.2 ,0.443137, 0.776471) ) :

#                with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , 237.5 ) ] ) :
#                    pm.button ( label = "reorderDeformers" , c = reorderDeformers_cmd ) ;