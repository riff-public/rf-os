import pymel.core as pm
import maya.cmds as mc
import os
# from rf_maya.rftool.simulation.sim.sim_preRoll import LWSnap as lw
# reload(lw)

from tools.artist_tools.tool_late import preroll
reload(preroll)


# startPath = pm.textField( 'inputDocPath' , q = True , text = True );
# path = getCachePath_cmd ( ) ;
def createFolderTxt( *args ):

    myDocuments = os.path.expanduser('~');          
    myDocuments = myDocuments.replace('\\', '/');
    # C:/Users/Legion/Documents
    prefPath = myDocuments + '/lateScriptData/pathInfo';
    if os.path.exists( prefPath ) == False:
        os.makedirs( prefPath );
    else : pass;

    return prefPath;

def saveDefaultPath( *args ):

    prefPath = createFolderTxt( );
    pathTxt = open( prefPath + '/inputDocPath.txt' , 'r' );

    pathname = pathTxt.read();
    pathTxt.close();

    return(pathname)

def cacheDirectoryBtn_cmd ( *args ) :

    os.startfile ( path ) ;

def filler ( times = 1 , *args ) :
    for i in range ( 0 , int ( times ) ) :
        pm.text ( label = '' ) ;

def getCharacterList_cmd ( *args ) :
    # charType.char.namespace

    referencePath_list = mc.file ( q = True , r = True ) ;
    
    ### get character path ###
    charRefPath_list = [] ;
    
    for path in referencePath_list :

        if pm.referenceQuery ( path , isLoaded = True ) != True :
            pass ; 
        elif ( '/char/' in str(path) ) and ('mtr' not in str(path)) :
           charRefPath_list.append ( path ) ;
    
    ### make character information ###
    char_list = [] ;
     
    for path in charRefPath_list :
        print 'ext',path 
        if '/char/' in str(path) :
            char = '/char/' ;
        elif '/character/' in str(path) :
            char = '/character/' ;
        
        elem = path.split(char)[1];
        elem = elem.split('/');
        
        charType     = elem[0];
        char         = elem[1] ;  
        namespace    = mc.file ( path , q = True , namespace = True ) ;

        char_list.append ( namespace ) ;
    
    char_list.sort ( ) ;   
    
    return char_list ;

def findStartEnd_cmd ( *args ) :
    # return [ preRoll , start , end , postRoll ] ;
    start       = pm.playbackOptions ( q = True , min = True ) ;
    end         = pm.playbackOptions ( q = True , max = True ) ;
    preRoll     = start - 30 ;
    postRoll    = end + 10 ;
    return [ preRoll , start , end , postRoll ] ;

def updateTimeRange_cmd ( *args ) :
    # included in refresh_cmd
    
    time = findStartEnd_cmd ( ) ;
    pm.intField ( 'preRoll_intField'    , e = True , v = time [0] ) ;
    pm.intField ( 'start_intField'      , e = True , v = time [1] ) ;
    pm.intField ( 'end_intField'        , e = True , v = time [2] ) ;
    pm.intField ( 'postRoll_intField'   , e = True , v = time [3] ) ;   

def charListTxtScollUpdate_cmd ( *args ) :
    # included in refreshed_cmd

    pm.textScrollList ( 'characterList_textScroll' , e = True , ra = True ) ;

    char_list = getCharacterList_cmd ( ) ;    

    for char in char_list :
        pm.textScrollList ( 'characterList_textScroll' , e = True , append = char ) ;

def refresh_cmd ( *args ) :

    charListTxtScollUpdate_cmd ( ) ;
    updateTimeRange_cmd ( ) ;

def pathCache(*args) :
    char_textScrollList = pm.textScrollList ( 'characterList_textScroll' , q = True , selectItem = True)
    DefaultPath = saveDefaultPath()
    path = mc.file(q=True, loc=True)
    elem = path.split('/')
    for each_char in char_textScrollList:
        if pm.checkBox ( 'charFacial_checkBox' , q = True ,v= True):
            path_cache = DefaultPath + elem[1] + '/' +elem[4]+'/'+ elem[5]+'/'+ each_char + '/data/'+ elem[5] +'_sim_'+each_char +'_NoFacial.abc'
        else:
            path_cache = DefaultPath + elem[1] + '/' +elem[4]+'/'+ elem[5]+'/'+ each_char + '/data/'+elem[5] +'_sim_'+each_char +'.abc'
    return(path_cache)
        
def cache (*args):
    path_cache = pathCache()
    elem = path_cache.split('/')
    preRoll , start , end , postRoll = findStartEnd_cmd()
    root = '-root |Char_Grp|'+elem[4] +':Rig_Grp|'+elem[4]+':Geo_Grp'

    pm.select(elem[4]+':Geo_Grp',r=True)

    command = "-frameRange " + '{}'.format(preRoll) + " " + '{}'.format(postRoll) +" -uvWrite -worldSpace " + '{}'.format(root) + " -file " + path_cache

    mc.AbcExport ( j = command )
    mc.confirmDialog(title='Confirm', message='Done!!', button=['Yes'], defaultButton='Yes')

def cacheout_cmd(*args):

    char_textScrollList = pm.textScrollList ( 'characterList_textScroll' , q = True , selectItem = True)
    if pm.checkBox ( 'charFacial_checkBox' , q = True ,v= True):
        for each_char in char_textScrollList:
            list_item = pm.listRelatives(each_char + ':FclRigGeo_Grp' ,ad = True ,type = 'shape')
            for item in list_item:
                BSH = pm.listConnections(item , s= True , type = 'blendShape')
                if BSH:
                    pm.setAttr(BSH[0] + '.envelope', 0)

    preroll.run(char_textScrollList)
    pathCache()
    cache()

def cacheDirectoryBtn_cmd ( *args ) :
    import os ;
    path = pathCache ( ) ;
    elem = path.split('/')
    path_cacheFile = elem[0] +'/'+elem[1] +'/'+elem[2] +'/'+elem[3] +'/'+elem[4] +'/'+elem[5] 
    os.startfile ( path_cacheFile ) ;

def run ( *args ) :

    if pm.window ( 'cacheout_UI' , exists = True ) :
        pm.deleteUI ( 'cacheout_UI' ) ;


    window = pm.window ( 'cacheout_UI', title = "CACHE OUT" , mnb = True , mxb = False , sizeable = False , rtf = True ) ;
    pm.window ( 'cacheout_UI' , e = True , w = 312 , h = 10 ) ;

    main_layout = pm.rowColumnLayout ( p = window , nc = 1 ,adj = 1) ;
    with main_layout :

        #########
        ''' TAB '''
        #########
        with pm.tabLayout ( innerMarginWidth = 5 , innerMarginHeight = 5 , w = 312 ) :

        #     #########
        #     ''' MAIN TAB '''
        #     #########
            with pm.rowColumnLayout ( 'main' , nc = 1 , w = 300 ) :

                pm.button ( label = 'cache directory' ,c = cacheDirectoryBtn_cmd) ;
                    
                with pm.rowColumnLayout ( nc = 1 , w = 300 ) :

        #             #########
        #             ''' CHARACTER '''
        #             #########
                    with pm.rowColumnLayout ( nc = 1 , w = 300 ) :

                        pm.separator ( vis = True , h = 3 ) ;
                        pm.textScrollList ( 'characterList_textScroll' , w = 300 , h = 150 , ams = True ) ;

                        with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , 300/3 ) , ( 2 , 300/3 ) , ( 3 , 300/3 )] ) :
                            filler ( ) ;
                            pm.checkBox ( 'charFacial_checkBox' , label = 'No Facial' , v = False , enable = True , h = 20 ) ;
                            filler ( ) ;


        #         #########
        #         ''' TIME RANGE LAYOUT '''
        #         #########
                with pm.rowColumnLayout ( nc = 4 ,   columnWidth = [ ( 1 , 300/4 ) , ( 2 , 300/4 ) , ( 3 , 300/4 ) , ( 4 , 300/4 ) ] ) :

                    pm.intField ( 'preRoll_intField' ) ;
                    pm.intField ( 'start_intField' ) ;
                    pm.intField ( 'end_intField' ) ;
                    pm.intField ( 'postRoll_intField' ) ;

                    pm.text ( label = 'pre roll -30' , bgc = [ 0.45 , 0.75 , 0.45 ] ) ;
                    pm.text ( label = 'start' , bgc = [ 1 , 1 , 0 ] ) ;
                    pm.text ( label = 'end' , bgc = [ 1 , 1 , 0 ] ) ;
                    pm.text ( label = 'post roll +10' , bgc = [ 0.45 , 0.75 , 0.45 ] ) ;

                pm.separator ( vis = True , h = 3 ) ;

                #########
                ''' REFRESH BUTTON '''
                #########
                with pm.rowColumnLayout ( nc = 3 , cw = [ ( 1 , 300/4 ) , ( 2 , 300/2 ) , ( 3 , 300/4 ) ] ) :

                    filler ( ) ;
                    pm.button ( label = 'refresh' , bgc = (  0.65 , 0.85 , 0.9 ) , c=  refresh_cmd) ;

                pm.separator ( vis = True , h = 10 ) ;


        #         #########
        #         ''' CACHE OUT BUTTON '''
        #         #########

                pm.text ( label = "In viewport, please don't forget to Show > None" , bgc = ( 1 , 0.4 , 0.7 ) ) ;

                pm.button ( label = 'cache out' ,c = cacheout_cmd ) ;
                



    refresh_cmd()
    pm.showWindow( window )


