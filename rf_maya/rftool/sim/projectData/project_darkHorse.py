import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

core = '%s/core'%os.environ.get('RFSCRIPT');
imagePath = core + '/rf_maya/rftool/sim/image_fortools';

############################################### def #####################################################
def darkHorse_setResolution_cmd ( *args ) :

    defaultResolution = pm.general.PyNode ( 'defaultResolution' ) ;

    defaultResolution.aspectLock.set ( 0 ) ;
    defaultResolution.width.set ( 3840 ) ;
    defaultResolution.height.set ( 2160 ) ;
    defaultResolution.dotsPerInch.set ( 72 ) ;
    defaultResolution.deviceAspectRatio.set( 1.778 );

    pm.currentUnit ( time = 'ntsc' ) ;

    pm.select ( defaultResolution ) ;

def resolutionGate_cmd ( *args ) :
    darkHorse_setResolution_cmd();

    import maya.OpenMaya as OpenMaya
    import maya.OpenMayaUI as OpenMayaUI

    view = OpenMayaUI.M3dView.active3dView ( ) ;
    cam = OpenMaya.MDagPath()
    view.getCamera(cam)
    camPath = cam.fullPathName()

    camNode = pm.general.PyNode ( camPath ) ;

    status = pm.camera ( camPath , q = True , displayResolution = True )

    if camNode.overscan.get() != 1 :
        camNode.overscan.unlock() ;
        pm.disconnectAttr ( camNode.overscan ) ;

    if not status :
        pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = True , overscan = 1.0 , lockTransform = True ) ;
    else :
        pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = False , overscan = 1.0 , lockTransform = False ) ;

    camera = pm.general.PyNode ( camPath ) ;
    #cameraShape = pm.listRelatives ( camera , shapes = True ) [0] ;
    camera.displayGateMaskColor.set ( 0 , 0 , 0 ) ;
    camera.displayGateMaskOpacity.set ( 1 ) ;

def toggleAntiAliasing_cmd ( *args ) :
    
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    
    if hardwareRenderingGlobals.multiSampleEnable.get() == 0 :
        hardwareRenderingGlobals.multiSampleEnable.set(1) ;
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = True ) ;

    elif hardwareRenderingGlobals.multiSampleEnable.get() == 1 :
        hardwareRenderingGlobals.multiSampleEnable.set(0) ;
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = False ) ;

    sampleCountChange_cmd ( ) ;

def sampleCountChange_cmd ( *args ) :
    
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    
    multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , q = True , value = True ) ;
    multiSampleCount = int ( multiSampleCount ) ;
    hardwareRenderingGlobals.multiSampleCount.set ( multiSampleCount ) ;

def darkHorse_simPreRoll_cmd (*args):
    from rf_maya.rftool.sim.projectData.sim_preRoll import sprUI
    reload(sprUI)
    sprUI.preRollUI().sprUI()
    
########################################## def space #########################################################

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;
    
##########################################################################################################################################################################################
############################################### main UI #################################################
layoutWidth = 250
width = 237.5

def insert ( project='', *args ) :

    ##################
    ##################
    ''' [ ISL_IDX ] INFO SECTION '''
    ##################
    ##################

    with pm.scrollLayout ( w = layoutWidth , h = 500 ) :

        with pm.rowColumnLayout (  nc = 1 , cw = ( 1 , width ) ) :
            pm.separator( h = 10, st = 'in' );
            pm.text ( label = 'Resolution / FPS' );

        pm.separator ( vis = True , h = 5 ); 
        with pm.rowColumnLayout (  nc = 5 , cw = [ ( 1 , width*0.55/2 ) , ( 2 , width*0.55/2 ), ( 3 , width*0.1/2 ), ( 4 , width*0.55/2 ), ( 5 , width*0.3/2 ) ] ) :       
            pm.intField ( v = 3840 ) ;
            pm.intField ( v = 2160 ) ;
            pm.text ( label = '-' );
            pm.intField ( v = 30 ) ;
            pm.symbolButton( c = resolutionGate_cmd, image = imagePath + '/settings.png' );
        pm.separator( h = 20, st = 'in' );

        with pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 500 )):
            pm.separator( h = 10, st = 'in' );

        separator ( ) ;
        
        ##################
        ##################
        ''' [ UTL_IDX ] UTILITIES SECTION '''
        ##################
        ##################

        pm.separator ( vis = True , h = 10 ) ;
        with pm.rowColumnLayout ( nc = 1 , w = width ) :
            pm.button ( label = 'Sim PreRoll' , c = darkHorse_simPreRoll_cmd , bgc = ( 0 , 1 , 1 ) , w = width) ;
        pm.separator ( vis = True , h = 10 ) ;

        
