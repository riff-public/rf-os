import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

layoutWidth = 250
width = 237.5

############################################### def #####################################################

def setResolution_cmd  ( *args ) :

    defaultResolution = pm.general.PyNode ( 'defaultResolution' ) ;

    defaultResolution.aspectLock.set ( 0 ) ;
    defaultResolution.width.set ( 3840 ) ;
    defaultResolution.height.set ( 1600 ) ;
    defaultResolution.dotsPerInch.set ( 72 ) ;
    defaultResolution.deviceAspectRatio.set( 2.35294127464 );

    pm.currentUnit ( time = 'film' ) ;

    pm.select ( defaultResolution ) ;

def resolutionGate_cmd ( *args ) :

    import maya.OpenMaya as OpenMaya
    import maya.OpenMayaUI as OpenMayaUI

    view = OpenMayaUI.M3dView.active3dView ( ) ;
    cam = OpenMaya.MDagPath()
    view.getCamera(cam)
    camPath = cam.fullPathName()

    camNode = pm.general.PyNode ( camPath ) ;

    status = pm.camera ( camPath , q = True , displayResolution = True )

    if camNode.overscan.get() != 1 :
        camNode.overscan.unlock() ;
        pm.disconnectAttr ( camNode.overscan ) ;

    if not status :
        pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = True , overscan = 1.0 , lockTransform = True ) ;
    else :
        pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = False , overscan = 1.0 , lockTransform = True ) ;

    camera = pm.general.PyNode ( camPath ) ;
    camera.displayGateMaskColor.set ( 0 , 0 , 0 ) ;
    camera.displayGateMaskOpacity.set ( 1 ) ;

def toggleAntiAliasing_cmd ( *args ) :
    
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    
    if hardwareRenderingGlobals.multiSampleEnable.get() == 0 :
        hardwareRenderingGlobals.multiSampleEnable.set(1) ;
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = True ) ;

    elif hardwareRenderingGlobals.multiSampleEnable.get() == 1 :
        hardwareRenderingGlobals.multiSampleEnable.set(0) ;
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = False ) ;

    sampleCountChange_cmd ( ) ;

def sampleCountChange_cmd ( *args ) :
    
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    
    multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , q = True , value = True ) ;
    multiSampleCount = int ( multiSampleCount ) ;
    hardwareRenderingGlobals.multiSampleCount.set ( multiSampleCount ) ;

def autonaming_cmd ( *args ) :
    from tools.artist_tools.tool_late import autoProjectLocal
    reload(autoProjectLocal)
    autoProjectLocal.run()

def autonaming_toShelf_cmd ( *args ) :

    commandHeader = '''
import maya.cmds as mc
import sys
mp = "%s/Pipeline/core/rf_maya/rftool/sim/tools/artist_tools/tool_late"%os.environ['RFSCRIPT'] ;
if not mp in sys.path :
    sys.path.insert( 0 , mp );
    
import autoProjectLocal as auto
reload( auto )

auto.run()
'''
    mel_cmd = '''
global string $gShelfTopLevel;
string $shelves = `tabLayout -q -selectTab $gShelfTopLevel`;
'''
    currentShelf = pm.mel.eval ( mel_cmd );
    pm.shelfButton ( style = 'iconOnly' , image = image_path , command = commandHeader , parent = currentShelf ) ;

def samAutoORI_cmd ( *args ) :
    from tools.artist_tools.tool_sam.origin import auto_origin
    reload ( auto_origin )
    auto_origin.auto_origin().load_origin()

def samAutoAnimORI_cmd ( *args ) :
    from tools.artist_tools.tool_sam.origin import auto_origin
    reload ( auto_origin )
    auto_origin.auto_origin().load_anim()

def cache_out_cmd ( *args ) :
    from projectData import UI_Preroll
    reload(UI_Preroll)
    
    UI_Preroll.run ( ) ;

def browseTechAnim_cmd ( *args ) :
    import os ;
    os.startfile ( r'N:\Staff\_sim' ) ;

def copyPlayblast(*args):
    from tools.artist_tools.tool_sam.copyPlayblast import run
    reload ( run)
    run.mayaRun()

def redshift_Light(*args):
    from tools.artist_tools.tool_sam.Light2Ren import Light2RenV2
    reload ( Light2RenV2 )

def autoSim_cloth(*args):
    from tools.artist_tools.tool_june.autoSim import autoSimLocal
    reload(autoSimLocal);
    autoSimLocal.AutoSim().runAutoSim_cloth();

def autoSim_hair(*args):
    from tools.artist_tools.tool_june.autoSim import autoSimLocal
    reload(autoSimLocal);
    autoSimLocal.AutoSim().runAutoSim_hair();

########################################## def space #########################################################

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;
    
##########################################################################################################################################################################################
############################################### main UI #################################################
def insert ( *args ) :

    with pm.scrollLayout ( w = layoutWidth , h = 500 ) :

        with pm.rowColumnLayout (  nc = 5 , cw = [ ( 1 , width*0.05 ) , ( 2 , width*0.55 ) , ( 3 , width*0.05 ) , ( 4 , width*0.30 ) , ( 5 , width*0.05 )] ) :

            filler ( ) ;
            pm.text ( label = 'resolution' ) ;
            filler ( ) ;
            pm.text ( label = 'fps' ) ;
            filler ( ) ;

            filler ( ) ;      
            with pm.rowColumnLayout (  nc = 2 , cw = [ ( 1 , width*0.55/2 ) , ( 2 , width*0.55/2 ) ] ) :       
                pm.intField ( v = 3840 ) ;
                pm.intField ( v = 1600 ) ;
            filler ( ) ;
            pm.intField ( v = 24 ) ;

        with pm.rowColumnLayout ( nc = 2 , cw = [ ( 1 , width/2 )  , ( 2 , width/2 ) ] ) :
            pm.button ( label = 'set scene' , c = setResolution_cmd   ) ;
            pm.button ( label = 'resolution gate' , c = resolutionGate_cmd  ) ;

     
            with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :
                filler ( ) ;
                pm.button ( label = 'toggle anti-aliasing' , w = width/2 , c = toggleAntiAliasing_cmd ) ;

            with pm.rowColumnLayout ( nc = 1 , w = width/2 ) :
                
                pm.text ( label = 'sample count' , w = width/2 ) ;

                with pm.optionMenu( 'sampleCount_ptm' , w = width/2 , changeCommand = sampleCountChange_cmd ) :
                    pm.menuItem ( label= '16' ) ;
                    pm.menuItem ( label= '8' ) ;
                    pm.menuItem ( label= '4' ) ;


        separator ( ) ;

                
        pm.separator ( vis = True , h = 20 ) ;     
        with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/6*5 ) , ( 2 , width/6 ) ] ) :
            pm.button ( label = 'autonaming project' , c = autonaming_cmd , bgc = ( 0 , 1 , 1 ) , w = width/6*5) ;
            pm.symbolButton ( image = 'O:/Pipeline/core/rf_maya/rftool/sim/image_fortools/toShelf.png' , width = width/6 , c = autonaming_toShelf_cmd ) ;
        pm.separator ( vis = True , h = 10 ) ;       

        with pm.rowColumnLayout ( nc = 1 , w = width ) :
            pm.text ( label = 'Shift ORI' )
            with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                pm.button ( label = 'origin' , c = samAutoORI_cmd , bgc = ( 0 , 1 , 1 ) ) ;
                pm.button ( label = 'anim' , c = samAutoAnimORI_cmd , bgc = ( 0 , 1 , 1 ) ) ;
        pm.separator ( vis = True , h = 10 ) ;

        with pm.rowColumnLayout ( nc = 1 , w = width ) :
            pm.text ( label = 'Fix Anim' ) ;
            with pm.rowColumnLayout ( nc = 1 , columnWidth = [ ( 1 , width) ] ) :
                pm.button ( label = 'cache Out' , c = cache_out_cmd , bgc = ( 1 , 0.9 , 1 ) , w = width/6*5) ;
        pm.separator ( vis = True , h = 10 ) ;

        with pm.rowColumnLayout ( nc = 1 , w = width ) :
            pm.text ( label = 'render' ) ;
            pm.button ( l = "redshift Light" , c = redshift_Light , w = width  )
        filler ( ) ;
        pm.button ( l = "copy playblast" , c = copyPlayblast  , w = width )
        pm.button ( label = 'N:\staff' , c = browseTechAnim_cmd  , w = width ) ;
        pm.separator ( vis = True , h = 10 ) ;  

        with pm.rowColumnLayout ( nc = 1 , w = width ) :
            pm.text ( label = 'Auto Sim' )
            with pm.rowColumnLayout ( nc = 2 , columnWidth = [ ( 1 , width/2 ) , ( 2 , width/2 ) ] ) :
                pm.button ( label = 'cloth' , c = autoSim_cloth , bgc = ( 1, 0.843137, 0 ) ) ;
                pm.button ( label = 'hair' , c = autoSim_hair , bgc = ( 1, 0.843137, 0 ) ) ;
        pm.separator ( vis = True , h = 10 ) ;  
