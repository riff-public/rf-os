import sys , os ;
import pymel.core as pm ;
import maya.cmds as mc ;

core = '%s/core'%os.environ.get('RFSCRIPT');
imagePath = core + '/rf_maya/rftool/sim/image_fortools';

def setResolution_cmd  ( *args ) :

    defaultResolution = pm.general.PyNode ( 'defaultResolution' ) ;
    camWidth = 3840
    camHeight = 2160
    aspectRatio = float(camWidth) / float(camHeight)

    defaultResolution.aspectLock.set ( 0 ) ;
    defaultResolution.width.set ( camWidth ) ;
    defaultResolution.height.set ( camHeight ) ;
    defaultResolution.dotsPerInch.set ( 72 ) ;
    defaultResolution.deviceAspectRatio.set( aspectRatio );

    pm.currentUnit ( time = 'film' ) ;

    pm.select ( defaultResolution ) ;

def resolutionGate_cmd ( *args ) :
    setResolution_cmd();

    import maya.OpenMaya as OpenMaya
    import maya.OpenMayaUI as OpenMayaUI

    view = OpenMayaUI.M3dView.active3dView ( ) ;
    cam = OpenMaya.MDagPath()
    view.getCamera(cam)
    camPath = cam.fullPathName()

    camNode = pm.general.PyNode ( camPath ) ;

    status = pm.camera ( camPath , q = True , displayResolution = True )

    if camNode.overscan.get() != 1 :
        camNode.overscan.unlock() ;
        pm.disconnectAttr ( camNode.overscan ) ;

    if not status :
        pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = True , overscan = 1.0 , lockTransform = True ) ;
    else :
        pm.camera ( camPath , e = True , displayFilmGate = False , displayResolution = False , overscan = 1.0 , lockTransform = True ) ;

    camera = pm.general.PyNode ( camPath ) ;
    camera.displayGateMaskColor.set ( 0 , 0 , 0 ) ;
    camera.displayGateMaskOpacity.set ( 1 ) ;

def toggleAntiAliasing_cmd ( *args ) :
    
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    
    if hardwareRenderingGlobals.multiSampleEnable.get() == 0 :
        hardwareRenderingGlobals.multiSampleEnable.set(1) ;
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = True ) ;

    elif hardwareRenderingGlobals.multiSampleEnable.get() == 1 :
        hardwareRenderingGlobals.multiSampleEnable.set(0) ;
        multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , e = True , enable = False ) ;

    sampleCountChange_cmd ( ) ;

def sampleCountChange_cmd ( *args ) :
    
    hardwareRenderingGlobals = pm.general.PyNode ( 'hardwareRenderingGlobals' ) ;
    
    multiSampleCount = pm.optionMenu( 'sampleCount_ptm' , q = True , value = True ) ;
    multiSampleCount = int ( multiSampleCount ) ;
    hardwareRenderingGlobals.multiSampleCount.set ( multiSampleCount ) ;

def autonaming_cmd ( *args ) :
    from tools.artist_tools.tool_late import autoProjectLocal
    reload(autoProjectLocal)
    autoProjectLocal.run()

def autonaming_toShelf_cmd ( *args ) :

    commandHeader = '''
import maya.cmds as mc
import sys
mp = "%s/Pipeline/core/rf_maya/rftool/sim/tools/artist_tools/tool_late"%os.environ['RFSCRIPT'] ;
if not mp in sys.path :
    sys.path.insert( 0 , mp );
    
import autoProjectLocal as auto
reload( auto )

auto.run()
'''
    mel_cmd = '''
global string $gShelfTopLevel;
string $shelves = `tabLayout -q -selectTab $gShelfTopLevel`;
'''
    currentShelf = pm.mel.eval ( mel_cmd );
    pm.shelfButton ( style = 'iconOnly' , image = imagePath + '/Auto.png', command = commandHeader , parent = currentShelf ) ;

def samAutoORI_cmd ( *args ) :
    from tools.artist_tools.tool_sam.origin import auto_origin
    reload ( auto_origin )
    auto_origin.auto_origin().load_origin()

def samAutoAnimORI_cmd ( *args ) :
    from tools.artist_tools.tool_sam.origin import auto_origin
    reload ( auto_origin )
    auto_origin.auto_origin().load_anim()

def cache_out_cmd ( *args ) :
    from projectData import UI_Preroll
    reload(UI_Preroll)
    
    UI_Preroll.run ( ) ;

def browseTechAnim_cmd ( *args ) :
    import os ;
    os.startfile ( r'N:\Staff\_sim' ) ;

def browseDailySim_cmd ( *args ) :
    import os ;
    os.startfile ( r'P:\Hanuman\daily\sim' ) ;

def copyPlayblast(*args):
    from tools.artist_tools.tool_sam.copyPlayblast import run
    reload ( run)
    run.mayaRun()

def redshift_Light(*args):
    from tools.artist_tools.tool_sam.Light2Ren import Light2RenV2
    reload ( Light2RenV2 )

def simClothHairLocal(*args):
    from tools.artist_tools.tool_june.autoSim import autoSimLocal
    reload(autoSimLocal);
    autoSimLocal.AutoSim().simClothHairLocal();

def coreAutoSim(*args):
    from tools.artist_tools.tool_june.autoSim import coreAutoSim
    reload(coreAutoSim);
    coreAutoSim.autoSimUI();

def simPreRoll_cmd (*args):
    from rf_maya.rftool.sim.projectData.sim_preRoll import sprUI
    reload(sprUI);
    sprUI.preRollUI().sprUI();

def dupAnimFixPrerollPose( *args ) :
    from rf_maya.rftool.sim.tools.artist_tools.tool_june.equipment import helpTools;
    reload( helpTools );
    helpTools.MainHelpTools().dupAnimFixPrerollPose();

def separator ( h = 5 , *args ) :
    pm.separator ( vis = False , h = h ) ;

def filler ( *args ) :
    pm.text ( label = ' ' ) ;

layoutWidth = 250
width = 237.5

def insert ( *args ) :

    with pm.scrollLayout ( w = layoutWidth , h = 500 ) :

        with pm.rowColumnLayout (  nc = 1 , cw = ( 1 , width ) ) :
            pm.separator( h = 10, st = 'in' );
            pm.text ( label = 'Resolution / FPS' );

        pm.separator ( vis = True , h = 5 ); 
        with pm.rowColumnLayout (  nc = 5 , cw = [ ( 1 , width*0.55/2 ) , ( 2 , width*0.55/2 ), ( 3 , width*0.1/2 ), ( 4 , width*0.55/2 ), ( 5 , width*0.3/2 ) ] ) :       
            pm.intField ( v = 3840 ) ;
            pm.intField ( v = 2160 ) ;
            pm.text ( label = '-' );
            pm.intField ( v = 24 ) ;
            pm.symbolButton( c = resolutionGate_cmd, image = imagePath + '/settings.png' );
        pm.separator( h = 20, st = 'in' );

        with pm.rowColumnLayout( h = 10, w = width, nc = 1, columnWidth = ( 1, 500 )):
            pm.separator( h = 10, st = 'in' );
                
        pm.separator ( vis = True , h = 5 ); 
        with pm.rowColumnLayout( h = 15,w = width, nc = 4, columnWidth = [( 1, 60 ), ( 2, 60 ), ( 3, 60 ), ( 4, 60 )]):
            pm.text( label = 'Auto Project', fn = 'smallPlainLabelFont');
            pm.text( label = 'To Shelf', fn = 'smallPlainLabelFont');
            pm.text( label = 'Fix Anim', fn = 'smallPlainLabelFont');
            pm.text( label = 'Auto Sim', fn = 'smallPlainLabelFont');

        with pm.rowColumnLayout ( nc = 4 , columnWidth = [ ( 1 , 60 ) , ( 2 , 60 ), ( 3 , 60 ), ( 4, 60 ) ] ) :
            pm.symbolButton( c = autonaming_cmd, image = imagePath + '/shared-folder.png' );
            pm.symbolButton( c = autonaming_toShelf_cmd, image = imagePath + '/arrow-up.png' );
            pm.symbolButton( c = cache_out_cmd, image = imagePath + '/fix.png' );
            pm.symbolButton( c = coreAutoSim, image = imagePath + '/robot.png' );
        pm.separator ( vis = True , h = 5 ) ;

        with pm.rowColumnLayout( h = 5, w = width, nc = 1, columnWidth = ( 1, 500 )):
            pm.separator( h = 5, st = 'in' ); 

        pm.separator ( vis = True , h = 5 ); 
        with pm.rowColumnLayout( h = 15,w = width, nc = 4, columnWidth = [( 1, 60 ), ( 2, 60 ), ( 3, 60 ), ( 4, 60 )]):
            pm.text( label = 'Shift Origin', fn = 'smallPlainLabelFont');
            pm.text( label = 'Shift Anim', fn = 'smallPlainLabelFont');
            pm.text( label = 'Cache Preroll', fn = 'smallPlainLabelFont');
            pm.text( label = 'DUP Preroll', fn = 'smallPlainLabelFont');

        with pm.rowColumnLayout ( nc = 4 , columnWidth = [ ( 1 , 60 ) , ( 2 , 60 ), ( 3 , 60 ), ( 4, 60 ) ] ) :
            pm.symbolButton( c = samAutoORI_cmd, image = imagePath + '/shiftOrigin.png' );
            pm.symbolButton( c = samAutoAnimORI_cmd, image = imagePath + '/shiftAnim.png' );
            pm.symbolButton( c = simPreRoll_cmd, image = imagePath + '/export.png' );
            pm.symbolButton( c = dupAnimFixPrerollPose, image = imagePath + '/twins.png' );
        pm.separator ( vis = True , h = 5 ) ;

        with pm.rowColumnLayout( h = 5, w = width, nc = 1, columnWidth = ( 1, 500 )):
            pm.separator( h = 5, st = 'in' );  

        pm.separator ( vis = True , h = 5 ); 
        with pm.rowColumnLayout( h = 15,w = width, nc = 4, columnWidth = [( 1, 60 ), ( 2, 60 ), ( 3, 60 ), ( 4, 60 )]):
            pm.text( label = 'Render', fn = 'smallPlainLabelFont');
            pm.text( label = 'CC Playblast', fn = 'smallPlainLabelFont');
            pm.text( label = 'P:\Daily', fn = 'smallPlainLabelFont');
            pm.text( label = 'N:\Staff', fn = 'smallPlainLabelFont');

        with pm.rowColumnLayout ( nc = 4 , columnWidth = [ ( 1 , 60 ) , ( 2 , 60 ), ( 3 , 60 ), ( 4, 60 ) ] ) :
            pm.symbolButton( c = redshift_Light, image = imagePath + '/rendering.png' );
            pm.symbolButton( c = copyPlayblast, image = imagePath + '/upload.png' );
            pm.symbolButton( c = browseDailySim_cmd, image = imagePath + '/gift.png' );
            pm.symbolButton( c = browseTechAnim_cmd, image = imagePath + '/open.png' );
        pm.separator ( vis = True , h = 5 ) ;

        with pm.rowColumnLayout( h = 5, w = width, nc = 1, columnWidth = ( 1, 500 )):
            pm.separator( h = 5, st = 'in' );      
