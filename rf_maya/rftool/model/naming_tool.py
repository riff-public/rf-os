# v.0.0.1 polytag switcher
_title = 'NameingTools'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'NameingTools'

#Import python modules
import sys
import os
import getpass
import maya.cmds as mc
import maya.mel as mm
from functools import partial

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# framework modules
import rf_config as config
from rf_utils import log_utils

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)



class NameingTools(object):
    def __init__(self, parent=None):
        #Setup Window
        self.w = 170
        self.h = 347

        # ui read

    def show(self):
        if mc.window(uiName, q=True, ex=True):
            mc.deleteUI(uiName, window=True)
        self.window = mc.window(uiName, title='%s %s-%s' % (_title, _version, _des), wh=[self.w, self.h])
        mc.columnLayout ( w=170)

        mc.frameLayout( label='Elements', collapsable=True, bgc=[0,0,0], w=170)
        mc.columnLayout ( w=170)
        mc.rowLayout (numberOfColumns = 2)
        self.elemText = mc.textField(w=100 )
        mc.button (l='Add', w=70, c =self.add)
        mc.setParent('..')
        mc.button (l='Upr', w=170, c =partial(self.elem_add, "Upr"))
        mc.button (l='Lwr', w=170, c = partial(self.elem_add, "Lwr"))
        mc.button (l='Frnt', w=170, c = partial(self.elem_add, "Frnt"))
        mc.button (l='Bck', w=170, c = partial(self.elem_add, "Bck"))
        mc.setParent('..')
        mc.setParent('..')

        mc.frameLayout( label='Side', collapsable=True, bgc=[0,0,0], w=170)
        mc.columnLayout ( w=170)
        mc.button (l='L', w=170, c = partial(self.side_add, "_L"))
        mc.button (l='R', w=170, c = partial(self.side_add, "_R"))
        mc.setParent('..')
        mc.setParent('..')

        mc.frameLayout( label='Type', collapsable=True, bgc=[0,0,0], w=170)
        mc.columnLayout ( w=170)
        mc.button (l='Geo', w=170, c = partial(self.type_add, "_Geo"))
        mc.button (l='Nrb', w=170, c = partial(self.type_add, "_Nrb"))
        mc.button (l='Jnt', w=170, c = partial(self.type_add, "_Jnt"))
        mc.button (l='Ctrl', w=170, c = partial(self.type_add, "_Ctrl"))
        mc.button (l='Grp', w=170, c = partial(self.type_add, "_Grp"))
        mc.setParent('..')
        mc.setParent('..')
        
        mc.showWindow(uiName)
        mc.window(uiName, e=True, wh=[self.w, self.h])

    def del_(self, selName, *args):
        sides = ['L', 'R']
        types = ['Geo', 'Nrb', 'Jnt', 'Ctrl', 'Grp'] 
        splitList = selName.split('_')
        for index, split in enumerate(splitList):
            if split in types or split in sides:
                split = '_' + split
                splitList[index] = split
        selName = ''.join(splitList)
        return selName
    
    def add(self, *args):
        sels = mc.ls(sl=True)
        add = mc.textField(self.elemText, q=True, text=True)
        for sel in sels:
            selName = sel
            if '_' in selName:
                selName = self.del_(selName)
                newName = selName  
            if '_' in selName:
                index = selName.find('_')
                newName = selName[:index] + add + selName[index:]
            else:
                newName = selName + add

            mc.rename(sel, newName)
            
    def elem_add(self, add, *args):
        elems = ['Upr', 'Lwr', 'Frnt', 'Bck']
        sels = mc.ls(sl=True)
        for sel in sels:
            selName = sel
            if '_' in selName:
                selName = self.del_(selName)
                newName = selName    
            if not add in selName:
                for elem in elems:
                    if elem in selName:
                        newName = selName.replace(elem,add)
                        break
                    else:
                        if '_' in selName:
                            index = selName.find('_')
                            newName = selName[:index] + add + selName[index:]
                        else:
                            newName = selName + add

                mc.rename(sel, newName)

    def side_add(self, add, *args):
        sides = ['_L', '_R']
        sels = mc.ls(sl=True)
        for sel in sels:
            selName = sel
            if '_' in selName:
                selName = self.del_(selName)
                newName = selName    
            if not add in selName:
                for side in sides:
                    if side in selName:
                        newName = selName.replace(side,add)
                        break
                    else:
                        if '_' in selName:
                            index = selName.find('_')
                            newName = selName[:index] + add + selName[index:]
                        else:
                            newName = selName + add

                mc.rename(sel, newName)


    def type_add(self, add, *args):
        types = ['_Geo', '_Nrb', '_Jnt', '_Ctrl', '_Grp']
        sels = mc.ls(sl=True)
        for sel in sels:
            selName = sel
            if '_' in selName:
                selName = self.del_(selName)
                newName = selName    
            if not add in selName:
                for type in types:
                    if type in selName:
                        newName = selName.replace(type,add)
                        break
                    else:
                        newName = selName + add

                mc.rename(sel, newName)

def show():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI(uiName)
    myApp = NameingTools(maya_win.getMayaWindow())
    myApp.show()
    return myApp
