# --------------------------------------------------------------------------
# nameChecker.py : python script
# --------------------------------------------------------------------------
#
# DESCRIPTION:
#  Rename duplicated object automatically by put digit at the end of the name
#
# REQUIRES:
#  Maya 2012 and newer
#
#
# INSTALLATION AND RUNNING:
#  put name_checker.py into scripts directory
#  run code below
# -----------------------
#  import name_checker
#  name_checker.show()

# -----------------------
#
# AUTHORS:
#  Chanon Vilaiyuk
#  chanon.vilaiyuk@gmail.com

# Code Begin # --------------------------------------------------------------------------


import os, sys
from functools import partial
import maya.cmds as mc
import maya.mel as mm

class NameChecker() :
	def __init__(self) :
		self.ui = 'NameChecker'
		self.win = '%s_win' % self.ui

		self.key = '- '
		self.itemKey = '    '

		self.objs = dict()
		self.allItems = []
		self.renameInfo = []


	# ui part
	def show(self) :
		if mc.window(self.win, exists = True) :
			mc.deleteUI(self.win)

		mc.window(self.win, t = 'PT Name Checker v.1.0')
		mc.columnLayout(adj = 1, rs = 4)

		mc.rowColumnLayout(nc = 3, cw = [(1, 60), (2, 120)])

		mc.text(l = 'Show')
		mc.checkBox('%s_transformCB' % self.win, l = 'Transform', v = True)
		mc.checkBox('%s_shapeCB' % self.win, l = 'Shape')
		mc.text(l = 'Options')
		mc.checkBox('%s_nsCB' % self.win, l = 'Ignore namespace', v=True)

		mc.setParent('..')

		mc.rowColumnLayout(nc = 3, cw = [(1, 100), (2, 140), (3, 40)], cs = [(1, 6), (3, 4)])

		mc.checkBox('%s_grpCB' % self.win, l = 'Specific Group', onc = partial(self.setTextField, True), ofc = partial(self.setTextField, False))
		mc.textField('%s_grpTX' % self.win, enable = False)
		mc.button(l = 'Get', c = partial(self.getSelectedObject))

		mc.setParent('..')

		mc.text('%s_statusTX' % self.win, l = 'Duplicated name')
		mc.textScrollList('%s_TSL' % self.win, numberOfRows = 20, ams = True, sc = partial(self.doSelect))
		mc.button(l = 'Refresh', c = partial(self.listUI), bgc = (1, 1, 0.6))
		mc.button(l = 'Fix All', h = 30, c = partial(self.doFix), bgc = (0.6, 0.8, 1))

		mc.showWindow()
		mc.window(self.win, e = True, wh = [300, 430])

		self.listUI()


	# list all in scene
	def listObjects(self) :
		grpCheckBox = mc.checkBox('%s_grpCB' % self.win, q = True, v = True)
		ignoreNs = mc.checkBox('%s_nsCB' % self.win, q=True, v=True)
		objs = []
		objsNoNs = dict()

		# all items
		if not grpCheckBox :
			objs = mc.ls()

		else :
			grpName = mc.textField('%s_grpTX' % self.win, q = True, tx = True)

			if mc.objExists(grpName) :
				prevObjs = mc.ls(sl = True)
				mc.select(grpName, hi = True)
				objs = mc.ls(sl = True)
				mc.select(prevObjs)


		dupObjects = dict()

		if objs :
			for eachObj in objs :
				objType = mc.objectType(eachObj)
				rawName = eachObj.split(':')[-1].split('|')[-1]
				
				# remove namespace section 
				if ignoreNs: 
					if not rawName in objsNoNs.keys(): 
						objsNoNs[rawName] = {eachObj: objType}

					else: 
						objsNoNs[rawName].update({eachObj: objType})

						for k, v in objsNoNs[rawName].iteritems(): 
							if not rawName in dupObjects.keys(): 
								dupObjects[rawName] = {k: v}
						
							else :
								dupObjects[rawName].update({k: v})

				if not ignoreNs: 
					if '|' in eachObj :
						name = eachObj.split('|')[-1]

						if not name in dupObjects.keys() :
							dupObjects[name] = {eachObj: objType}

						else :
							dupObjects[name].update({eachObj: objType})



			return dupObjects


	# display in UI
	def listUI(self, arg = None) :
		self.objs = self.listObjects()
		objs = self.objs
		showTransform = mc.checkBox('%s_transformCB' % self.win, q = True, v = True)
		showShape = mc.checkBox('%s_shapeCB' % self.win, q = True, v = True)
		status = str()
		bgColor = None

		mc.textScrollList('%s_TSL' % self.win, e = True, ra = True)
		allItems = []
		items = []

		if objs :
			for eachObj in objs :
				key = eachObj
				dupNames = objs[key]
				displayItems = []

				for each in dupNames :
					objType = dupNames[each]
					show = False

					if showTransform :
						if objType == 'transform' :
							show = True

					if showShape :
						if objType == 'mesh' :
							show = True

					if show :
						displayItems.append('%s%s' % (self.itemKey, each))


				if displayItems :
					allItems.append('%s%s' % (self.key, key))
					allItems += displayItems


		if allItems :
			for item in allItems :
				mc.textScrollList('%s_TSL' % self.win, e = True, a = item)

			self.allItems = allItems
			items = [item.replace(self.itemKey, '') for item in self.allItems if not self.key in item]

		if items :
			status = '%s Duplicated Name' % len(items)
			bgColor = (1, 0.2, 0.2)
		else :
			status = 'No Duplicated Name'
			bgColor = (0.4, 1, 0.4)

		mc.text('%s_statusTX' % self.win, e = True, l = status, bgc = bgColor)


	# do select selected item from UI
	def doSelect(self, arg = None) :
		sels = mc.textScrollList('%s_TSL' % self.win, q = True, si = True)
		selections = []
		for each in sels :
			if self.key in each :
				key = each.replace(self.key, '')
				items = self.objs[key]
				selections += items

			if self.itemKey in each :
				item = each.replace(self.itemKey, '')
				selections.append(item)


		if selections :
			mc.select(selections)


	# run fix the name
	def doFix(self, arg = None) :
		self.fix()

		if self.renameInfo :
			for each in self.renameInfo :
				print each

			print '======================='
			print '%s items rename' % len(self.renameInfo)


		''' recursive rename each item '''

	# fix function
	def fix(self, arg = None) :
		# find items on the list
		listItems = mc.textScrollList('%s_TSL' % self.win, q = True, ai = True)

		# if any items, continue
		if listItems :

			# pick the last one
			targetItem = listItems[-1]

			if not self.key in targetItem :
				if self.itemKey in targetItem :
					itemName = targetItem.replace(self.itemKey, '')

					# rename
					name = findUniqueName(itemName)
					mc.rename(itemName, name)
					display = '%s -> %s' % (itemName, name)
					self.renameInfo.append(display)

					# refresh list again
					self.listUI()

					# if there is an item, fix again
					if listItems :
						self.fix()



	def setTextField(self, status, arg = None) :
		mc.textField('%s_grpTX' % self.win, e = True, en = status)


	def getSelectedObject(self, arg = None) :
		grp = mc.ls(sl = True)

		if grp :
			mc.textField('%s_grpTX' % self.win, e = True, tx = grp[0])


''' recursive checking the name '''

def checkPadding(name) :
	digit = str()

	for each in reversed(name) :
		if each.isdigit() :
			digit += each

		else :
			break

	if digit :
		intDigit = int(digit[::-1])

		return intDigit


def findUniqueName(name) :
	# if digit, add up
	if '|' in name :
		name = name.split('|')[-1]

	digit = checkPadding(name)

	if digit :
		newDigit = digit + 1
		newName = name.replace(str(digit), str(newDigit))

	else :
		newName = '%s1' % name

	if not mc.objExists(newName) :
		return newName

	else :
		return findUniqueName(newName)



def show() :
	app = NameChecker()
	app.show()