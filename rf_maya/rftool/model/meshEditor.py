import maya.cmds as mc

THRESHOLD = 1e-06

# --------------- #
# mesh editor GUI #
# --------------- #
class MeshEdtorGui(object) :
	def __init__(self) :
		if mc.window('mshEditor_window', q = True, ex = True) :
			mc.deleteUI('mshEditor_window', window = True)
		
		mc.window('mshEditor_window', t = 'Mesh Editor')
		
		self.mshEditor_colLyo = mc.columnLayout(adj = True)
		
		# mesh editor
		self.mshEditor_frmLyo = mc.frameLayout('mshEditor_frmLyo', l = 'Editor')
		self.mshEditor_formLyo = mc.formLayout('mshEditor_formLyo')
		
		self.axis_txt = mc.text('axis_txt', l = 'Axis', fn = 'boldLabelFont')
		self.axis_radioClt = mc.radioCollection('axis_radioClt')
		self.negX_radioBttn = mc.radioButton('negX_radioBttn', l = '-X', sl = True)
		self.x_radioBttn = mc.radioButton('x_radioBttn', l = '+X', sl = False)
		
		self.delmsh_bttn = mc.button('delmsh_bttn', l = 'Delete', h = 30)
		self.mirrormsh_bttn = mc.button('mirrormsh_bttn', l = 'Mirror', h = 30)
		self.delMirrormsh_bttn = mc.button('delMirrormsh_bttn', l = 'Delete and Mirror', h = 30)
		self.selVtx_bttn = mc.button('selVtx_bttn', l = 'Select Vertex', h = 30)
		mc.setParent('..')	# end mesh editor
		
		# mesh fix
		self.fixMsh_frmLyo = mc.frameLayout('fixMsh_frmLyo', l = 'Fix')
		self.fixMsh_colLyo = mc.columnLayout('fixMsh_colLyo', adj = True)
		
		self.fixCentermsh_bttn = mc.button('fixCentermsh_bttn', l = 'Fix Center Mesh', h = 30, ann = 'select vertex center of mesh.')
		
		mc.setParent('..')
		mc.showWindow('mshEditor_window')
		
		# edit control position
		mc.window('mshEditor_window', e = True, w = 200, h = 220)
		mc.formLayout(self.mshEditor_formLyo, e = True, attachForm = (
				[self.axis_txt, 'top', 12],
				[self.axis_txt, 'left', 10],
				
				[self.negX_radioBttn, 'top', 10],
				[self.negX_radioBttn, 'left', 60],
				
				[self.x_radioBttn, 'top', 10],
				[self.x_radioBttn, 'left', 120],
				
				[self.delmsh_bttn, 'top', 47],
				[self.delmsh_bttn, 'left', 2],
				[self.delmsh_bttn, 'right', 2],
				
				[self.mirrormsh_bttn, 'top', 77],
				[self.mirrormsh_bttn, 'left', 2],
				[self.mirrormsh_bttn, 'right', 2],
				
				[self.delMirrormsh_bttn, 'top', 107],
				[self.delMirrormsh_bttn, 'left', 2],
				[self.delMirrormsh_bttn, 'right', 2],
				
				[self.selVtx_bttn, 'top', 137],
				[self.selVtx_bttn, 'left', 2],
				[self.selVtx_bttn, 'right', 2]	))

	def __str__(self) :
		return 'mshEditor_window'

	def __repr__(self) :
		return 'mshEditor_window'

# --------------------- #
#	mesh information	#
# --------------------- #
class MeshInfo(object) :
	# generic information #
	# ------------------- #
	def __init__(self) :
		self.sel = mc.ls(sl = True)[0]
		self.bb = mc.exactWorldBoundingBox(self.sel)
		self.__vtxAmnt = mc.polyEvaluate(self.sel, v = True)
		self.vtxIds_x = []
		self.vtxIds_negX = []
		self.centerOffset = THRESHOLD

	def __str__(self) :
		return self.sel

	def __repr__(self) :
		return self.sel

	def vertexIds(self) :
		vtxIds = []
		for ix in xrange(self.__vtxAmnt) :
			vtxIds.append('%s.vtx[%d]' % (self.sel, ix))

		return vtxIds

	# world space position #
	# -------------------- #
	def centerObjectXAxisPosition(self) :
		return ((self.bb[3] - self.bb[0]) / 2) + self.bb[0]

	def centerObjectYAxisPosition(self) :
		return ((self.bb[4] - self.bb[1]) / 2) + self.bb[1]

	def centerObjectZAxisPosition(self) :
		return ((self.bb[5] - self.bb[2]) / 2) + self.bb[2]

	def centerObject(self) :
		return (self.centerObjectXAxisPosition(), self.centerObjectYAxisPosition(), self.centerObjectZAxisPosition())

	center = property(centerObject, None, None, None)

	# component #
	# --------- #
	def __negXAxis(self, vtx, posi, space) :
		if space == 'world' :
			if posi < (0.00 + (-self.centerOffset)) :
				self.vtxIds_negX.append(vtx)

		elif space == 'local' :
			if posi < (self.centerObjectXAxisPosition() + (-self.centerOffset)) :
				self.vtxIds_negX.append(vtx)

	def __xAxis(self, vtx, posi, space) :
		if space == 'world' :
			if posi > (0.00 + self.centerOffset) :
				self.vtxIds_x.append(vtx)

		elif space == 'local' :		
			if posi > (self.centerObjectXAxisPosition() + self.centerOffset) :
				self.vtxIds_x.append(vtx)

	def findPosition(self, axis, space) :
		for vtx in self.vertexIds() :
			vtxPosi = mc.xform(vtx, q = True, t = True, ws = True)

			if axis == '-x' :
				self.__negXAxis(vtx, vtxPosi[0], space)

			elif axis == '+x' :
				self.__xAxis(vtx, vtxPosi[0], space)

	def vertexToFace(self, vtxs) :
		fcIds = []
		if not vtxs == [] :
			for vtx in vtxs :
				vtx2Fc = mc.polyInfo(vtx, vf = True)[0]
				
				for num in vtx2Fc.split(' ') :
					if num.isdigit() :
						fcIds.append('%s.f[%d]' % (self.sel, int(num)))
		return fcIds

# ------------------------- #
#	Mesh Editor Function	#
# ------------------------- #
def deleteComponent(*args) :
	msh = MeshInfo()

	if mc.radioButton('negX_radioBttn', q = True, sl = True) :
		msh.findPosition('-x', 'world')
		mc.select(msh.vertexToFace(msh.vtxIds_negX), r = True)
		mc.delete()

	elif mc.radioButton('x_radioBttn', q = True, sl = True) :
		msh.findPosition('+x', 'world')
		mc.select(msh.vertexToFace(msh.vtxIds_x), r = True)
		mc.delete()

	mc.select(msh, r = True)

def mirrorComponent(*args) :
	msh = MeshInfo()

	mc.polyMirrorFace(msh, ws = True,  d = 1, mm = 1, mtt=1, mt = THRESHOLD, p = (0,0,0), ch = False)
	mc.polySoftEdge(msh, a = 180, ch = False)
	mc.select(msh, r = True)

def deleteMirrorComponent(*args) :
	msh = MeshInfo()

	deleteComponent()
	mirrorComponent()

def selectVertexs(*args) :
	msh = MeshInfo()

	if mc.radioButton('negX_radioBttn', q = True, sl = True) :
		msh.findPosition('-x', 'local')
		mc.select(msh.vtxIds_negX, r = True)

	elif mc.radioButton('x_radioBttn', q = True, sl = True) :
		msh.findPosition('+x', 'local')
		mc.select(msh.vtxIds_x, r = True)

def fixSelectedVertexToCenter(*args) :
	msh = MeshInfo()

	vtxIds = mc.ls(sl = True, fl = True)
	vtxIds = mc.polyListComponentConversion(vtxIds, tv=True)
	mc.select(vtxIds, r=True)
	vtxIds = mc.ls(sl = True, fl = True)
	for vtxId in vtxIds :
		vtxPosi = mc.xform(vtxId, q = True, t = True, ws = True)
		# print vtxId, vtxPosi
		mc.xform(vtxId, t = [0, vtxPosi[1], vtxPosi[2]], ws = True)

# --------- #
#	Main	#
# --------- #
def run() :
	mshGui = MeshEdtorGui()

	# set command to control UI
	mc.button(mshGui.delmsh_bttn, e = True, c = deleteComponent)
	mc.button(mshGui.mirrormsh_bttn, e = True, c = mirrorComponent)
	mc.button(mshGui.delMirrormsh_bttn, e = True, c = deleteMirrorComponent)
	mc.button(mshGui.selVtx_bttn, e = True, c = selectVertexs)
	mc.button(mshGui.fixCentermsh_bttn, e = True, c = fixSelectedVertexToCenter)