import math
from collections import defaultdict
import time

import maya.OpenMaya as om
import maya.api.OpenMaya as om2
import maya.cmds as mc
import pymel.core as pm
import pymel.core.datatypes as dt

class Point:
    def __init__(self, xcoord=0, ycoord=0):
        self.x = xcoord
        self.y = ycoord

class Rectangle:
    def __init__(self, bottom_left, top_right):
        self.bottom_left = bottom_left
        self.top_right = top_right

    def intersects(self, other):
        return not (self.top_right.x < other.bottom_left.x or self.bottom_left.x > other.top_right.x or self.top_right.y < other.bottom_left.y or self.bottom_left.y > other.top_right.y)

def getUvShellList(name):
    selList = om.MSelectionList()
    selList.add(name)
    selListIter = om.MItSelectionList(selList, om.MFn.kMesh)
    pathToShape = om.MDagPath()
    selListIter.getDagPath(pathToShape)
    meshNode = pathToShape.fullPathName()
    uvSets = mc.polyUVSet(meshNode, query=True, allUVSets=True)
    allSets = {}
    for uvset in uvSets:
        shapeFn = om.MFnMesh(pathToShape)
        shells = om.MScriptUtil()
        shells.createFromInt(0)
        nbUvShells = shells.asUintPtr()

        uArray = om.MFloatArray()   #array for U coords
        vArray = om.MFloatArray()   #array for V coords
        uvShellIds = om.MIntArray() #The container for the uv shell Ids

        shapeFn.getUVs(uArray, vArray)
        shapeFn.getUvShellsIds(uvShellIds, nbUvShells, uvset)

        # shellCount = shells.getUint(shellsPtr)
        shells = defaultdict(dict)
        for i, n in enumerate(uvShellIds):
            if 'map' not in shells[n] or 'coords' not in shells[n]:
                shells[n]['map'] = []
                shells[n]['coords'] = []
            shells[n]['map'].append('%s.map[%i]' % (name, i))
            shells[n]['coords'].append((uArray[i], vArray[i]))
        allSets[uvset] = shells
    return allSets

def getFaceBoundings(uarray, varray, faceIndices):
    face_center = {}
    face_radius = {}
    face_bbs = {}
    for ii, i in enumerate(faceIndices):
        # get uvs from face
        f_uarray = uarray[i]
        f_varray = varray[i]

        # get face center and radius
        cu = 0.0
        cv = 0.0
        for j in xrange(len(f_uarray)):
            cu += f_uarray[j]
            cv += f_varray[j]

        cu /= len(f_uarray)
        cv /= len(f_varray)
        rsqr = 0.0
        for j in xrange(len(f_varray)):
            du = f_uarray[j] - cu
            dv = f_varray[j] - cv
            dsqr = du * du + dv * dv
            rsqr = dsqr if dsqr > rsqr else rsqr
            
        face_center[i] = (cu, cv)
        face_radius[i] = math.sqrt(rsqr)

        # get face bounding box
        min_u = min(f_uarray)
        max_u = max(f_uarray)
        min_v = min(f_varray)
        max_v = max(f_varray)

        bottom_left = Point(min_u, min_v)
        top_right = Point(max_u, max_v)
        bb = Rectangle(bottom_left, top_right)
        face_bbs[i] = bb

    return face_center, face_radius, face_bbs

def getUVs(meshfn):
    # get uvs
    uarray = []
    varray = []
    for fid in xrange(meshfn.numPolygons):
        us = []
        vs = []
        for vi in xrange(len(meshfn.getPolygonVertices(fid))):
            try:
                u, v = meshfn.getPolygonUV(fid, vi)
                us.append(u)  
                vs.append(v)
            except RuntimeError:
                us.append(None)  
                vs.append(None)
        uarray.append(us)
        varray.append(vs)
    return uarray, varray

def createRayGivenFace(uarray, varray):
    """Represent a face by a series of edges(rays), i.e.
    orig = [orig1u, orig1v, orig2u, orig2v, ... ]
    vec  = [vec1u,  vec1v,  vec2u,  vec2v,  ... ]
    
    return false if no valid uv's.
    return (true, orig, vec) or (false, None, None)"""
    orig = []
    vec = []
    
    # loop throught all vertices to construct edges/rays
    u = uarray[-1]
    v = varray[-1]
    for i in xrange(len(uarray)):
        orig.append(uarray[i])
        orig.append(varray[i])
        vec.append(u - uarray[i])
        vec.append(v - varray[i])
        u = uarray[i]
        v = varray[i]
    return orig, vec

def checkCrossingEdges(face1Orig, face1Vec, face2Orig, face2Vec):
    """Check if there are crossing edges between two faces. Return true 
    if there are crossing edges and false otherwise. A face is represented
    by a series of edges(rays), i.e. 
    faceOrig[] = [orig1u, orig1v, orig2u, orig2v, ... ]
    faceVec[]  = [vec1u,  vec1v,  vec2u,  vec2v,  ... ]"""
    face1Size = len(face1Orig)
    face2Size = len(face2Orig)
    for i in xrange(0, face1Size, 2):
        o1x = face1Orig[i]
        o1y = face1Orig[i+1]
        v1x = face1Vec[i]
        v1y = face1Vec[i+1]
        n1x = v1y
        n1y = -v1x
        for j in xrange(0, face2Size, 2):
            # Given ray1(O1, V1) and ray2(O2, V2)
            # Normal of ray1 is (V1.y, V1.x)
            o2x = face2Orig[j]
            o2y = face2Orig[j+1]
            v2x = face2Vec[j]
            v2y = face2Vec[j+1]
            n2x = v2y
            n2y = -v2x
            
            # Find t for ray2
            # t = [(o1x-o2x)n1x + (o1y-o2y)n1y] / (v2x * n1x + v2y * n1y)
            denum = v2x * n1x + v2y * n1y
            # Edges are parallel if denum is close to 0.
            if math.fabs(denum) < 0.000001: continue
            t2 = ((o1x-o2x)* n1x + (o1y-o2y) * n1y) / denum
            if (t2 < 0.00001 or t2 > 0.99999): continue
            
            # Find t for ray1
            # t = [(o2x-o1x)n2x + (o2y-o1y)n2y] / (v1x * n2x + v1y * n2y)
            denum = v1x * n2x + v1y * n2y
            # Edges are parallel if denum is close to 0.
            if math.fabs(denum) < 0.000001: continue
            t1 = ((o2x-o1x)* n2x + (o2y-o1y) * n2y) / denum
            
            # Edges intersect
            if (t1 > 0.00001 and t1 < 0.99999): return 1
        
    return 0

def get_meshfn(meshName):
    # find polygon mesh node
    selList = om2.MSelectionList()
    selList.add(meshName)
    mesh = selList.getDependNode(0)
    if mesh.apiType() == om2.MFn.kTransform:
        dagPath = selList.getDagPath(0)
        dagFn = om2.MFnDagNode(dagPath)
        child = dagFn.child(0)
        if child.apiType() != om2.MFn.kMesh:
            raise Exception("Can't find polygon mesh")
        mesh = child
    meshfn = om2.MFnMesh(mesh)
    return meshfn

def getOverlapUVShellFace(uvShellData):
    # get uv shells and filter only ones that has bounding circle overlap with other
    
    shell_centers = {}
    shell_radius = {}
    shell_bbs = {}
    # collect shell center radius and bounding box
    for sid, shellData in uvShellData.iteritems():
        # maps = shellData['map']
        uv = shellData['coords']
        shell_uarray = [i[0] for i in uv]
        shell_varray = [i[1] for i in uv]

        # get shell center, radius
        cu = 0.0
        cv = 0.0

        for j in xrange(len(shell_uarray)):
            cu += shell_uarray[j]
            cv += shell_varray[j]
        
        cu /= len(shell_uarray)
        cv /= len(shell_varray)
        rsqr = 0.0
        for j in xrange(len(shell_varray)):
            du = shell_uarray[j] - cu
            dv = shell_varray[j] - cv
            dsqr = du * du + dv * dv
            rsqr = dsqr if dsqr > rsqr else rsqr
            
        shell_centers[sid] = (cu, cv)
        shell_radius[sid] = math.sqrt(rsqr)

        # get bounding box
        min_u = min(shell_uarray)
        max_u = max(shell_uarray)
        min_v = min(shell_varray)
        max_v = max(shell_varray)

        bottom_left = Point(min_u, min_v)
        top_right = Point(max_u, max_v)
        bb = Rectangle(bottom_left, top_right)
        shell_bbs[sid] = bb

    # get shell id that bound circle and bounding box overlap each other
    overlap_shell_ids = set()
    overlap_maps = set()
    compare_sid = uvShellData.keys()
    for sid, shellData in uvShellData.iteritems():
        cui, cvi = shell_centers[sid]
        ri = shell_radius[sid]
        bb1 = shell_bbs[sid]
        for ix, sidx in enumerate(compare_sid):
            if sid == sidx:
                continue
            cuj, cvj = shell_centers[sidx]
            rj = shell_radius[sidx]
            du = cuj - cui
            dv = cvj - cvi
            dsqr = du * du + dv * dv

            bb2= shell_bbs[ix]

            # if any overlap in both bounding circle or bounding box
            if dsqr < (ri + rj) * (ri + rj) and bb1.intersects(bb2): 
                overlap_shell_ids.add(sid)
                overlap_maps.update(set(shellData['map']))
                break

    shell_overlap_faces = pm.polyListComponentConversion(list(overlap_maps), tf=True)
    # get overlap face ids
    overlap_fids = []
    for f in shell_overlap_faces:
        fid_str = f.split('.f[')[-1][:-1]
        if ':' in fid_str:
            fid_splits = fid_str.split(':')
            fid_start = int(fid_splits[0])
            fid_end = int(fid_splits[1])
            overlap_fids.extend(range(fid_start, fid_end+1))
        else:
            overlap_fids.append(int(fid_str))

    overlap_fids.sort()

    return shell_overlap_faces, overlap_fids

def getOverlapUVFaces(meshName):
    """Return overlapping faces"""
    meshfn = get_meshfn(meshName)
    meshName = meshfn.partialPathName()

    uvShellList = getUvShellList(meshName)
    uvShellData = uvShellList[meshfn.currentUVSetName()]

    shell_overlap_faces, overlap_fids = getOverlapUVShellFace(uvShellData)

    uarray, varray = getUVs(meshfn)
    center, radius, face_bb = getFaceBoundings(uarray, varray, overlap_fids)
    # numPolygons = meshfn.numPolygons
    results = set()
    len_fid = len(overlap_fids)
    c = 0
    percent = 0.0
    for ii, i in enumerate(overlap_fids):
        curr_percent = int((float(c)/len_fid)*100)
        if (curr_percent - percent) >= 1.0 or ii == 0:
            print '{percent}% - finding overlap UVs...'.format(percent=curr_percent)
            percent = curr_percent
        if uarray[i] == None or varray[i] == None:
            continue
        face1Orig, face1Vec = createRayGivenFace(uarray[i], varray[i])
        cui, cvi = center[i]
        ri = radius[i]
        bb1 = face_bb[i]
        # Loop through face j where j != i
        for ji in xrange(ii+1, len(overlap_fids)):
            j = overlap_fids[ji]
            if uarray[j] == None or varray[j] == None:
                continue
            cuj, cvj = center[j]
            rj = radius[j]
            du = cuj - cui
            dv = cvj - cvi
            dsqr = du * du + dv * dv

            bb2 = face_bb[j]
            # Quick rejection if bounding circles don't overlap
            if dsqr >= (ri + rj) * (ri + rj) or not bb1.intersects(bb2): 
                continue
            
            face2Orig, face2Vec = createRayGivenFace(uarray[j], varray[j])
            if checkCrossingEdges(face1Orig, face1Vec, face2Orig, face2Vec):
                face1 = '%s.f[%d]' %(meshName, i)
                face2 = '%s.f[%d]' %(meshName, j)
                results.update([face1, face2])
        c += 1
    print '100% - Finding overlap UVs...'
    print 'Done finding overlap UV on: %s' %meshName
    return results

def check_overlap_UV():
    st = time.time()
    overlapFaces = []
    sels = pm.ls(sl=True, type='transform')
    for s in sels:
        if not s.getShape(type='mesh'):
            continue
        curUV = pm.polyUVSet(s, q=1, cuv=1)
        numFaces = s.numFaces()
        # warn user, this may take a while
        if numFaces > 10000:
            result = pm.confirmDialog(title="Warning", 
                    message="{obj} has {fcount} faces\nIt might take some time to process, continue?".format(obj=s.shortName(), fcount=numFaces), 
                    button=("Yes", "No"), defaultButton="Yes", 
                    cancelButton="No", dismissString="No")
            if result != 'Yes':
                continue
        for i, uv in enumerate(pm.polyUVSet(s, q=1, auv=1)): 
            pm.polyUVSet(s, cuv=1, uvSet=uv)
            of = getOverlapUVFaces(str(s))
            if of != []: 
                overlapFaces.extend(of)
        pm.polyUVSet(s, cuv=1, uvSet=str(curUV))

    print 'Process finished in: %s' %(time.time() - st)
    num_overlapFaces = len(overlapFaces)
    if num_overlapFaces:
        pm.select(overlapFaces, r=True)
        om.MGlobal.displayWarning('Found {} faces with overlapping UVs'.format(num_overlapFaces))
    else:
        om.MGlobal.displayInfo('No overlapping face found.')

def check_flip_UV():
    flipped = []
    sels = pm.ls(sl=True, type='transform')
    for s in sels:
        if not s.getShape(type='mesh'):
            continue
        curUV = pm.polyUVSet(s, q=1, cuv=1)
        for i, uv in enumerate(pm.polyUVSet(s, q=1, auv=1)): 
            pm.polyUVSet(s, cuv=1, uvSet=uv)
            flf = []
            for f in pm.ls(pm.PyNode(s).getShape().f, fl=1):
                uvPos = pm.polyEditUV([pm.polyListComponentConversion(vf, fvf=1, toUV=1)[0] for vf in pm.ls(pm.polyListComponentConversion(f, tvf=1), fl=1)], q=1)
                uvAB = dt.Vector([uvPos[2] - uvPos[0], uvPos[3] - uvPos[1]])
                uvBC = dt.Vector([uvPos[4] - uvPos[2], uvPos[5] - uvPos[3]])
                if uvAB.cross(uvBC) * dt.Vector([0, 0, 1]) <= 0: flf.append(f)
            if flf != []: 
                flipped.extend(flf)
        pm.polyUVSet(s, cuv=1, uvSet=str(curUV))

    pm.select(flipped, r=True)

'''

import cProfile
import time
from rf_maya.rftool.model import uv_utils
reload(uv_utils)
st = time.time()
uv_utils.check_overlap_UV()
print time.time() - st
# cProfile.run('uv_utils.check_overlap_UV()')

'''