class Riff():
    _IS_DEFAULT = True
    FILE_SEP = '_'

    DIFFUSE = 'Diffuse'
    ROUGHNESS = 'Roughness'
    NORMAL = 'Normal'
    HEIGHT = 'Height'
    METALLIC = 'Metallic'
    ALPHA = 'Opacity'
    TRANSMISSION = 'Transmission'
    EMISSION = 'Emission'
    ROUGHNESS_BUMP_METALLIC = 'RBM'
    mapType_index = 1
    name_index = 2

    MAP_TYPES = (DIFFUSE, ROUGHNESS, NORMAL, HEIGHT, METALLIC, ALPHA, TRANSMISSION, EMISSION, ROUGHNESS_BUMP_METALLIC)
    COLOR_TYPES = (DIFFUSE)
    DEFAULT_CHECK = (True, True, True, False, False, True, True, True, True)
    HELP_NAMING = 'AssetName_MapType_Name_####.ext'
    HELP_EXAMPLE = 'Arthur_Color_Body_1001.png'

class Miarmy(Riff):
    _IS_DEFAULT = False
    mapType_index = 1
    name_index = '2:'
    HELP_NAMING = 'AssetName_MapType_Name.ext'
    HELP_EXAMPLE = 'Arthur_Color_Body_1.png'

class Psyop():
    FILE_SEP = '_'

    DIFFUSE = 'COL'
    ROUGHNESS = 'RGH'
    NORMAL = 'NRM'
    HEIGHT = 'DSP'
    METALLIC = 'MET'
    ALPHA = 'OPC'
    ROUGHNESS_BUMP_METALLIC = 'RBM'
    mapType_index = 1
    name_index = 0

    MAP_TYPES = (DIFFUSE, ROUGHNESS, NORMAL, HEIGHT, METALLIC, ALPHA, ROUGHNESS_BUMP_METALLIC)
    COLOR_TYPES = (DIFFUSE)
    DEFAULT_CHECK = (True, True, True, True, True, True, True)

    HELP_NAMING = 'Name_MapType_DataType.####.ext'
    HELP_EXAMPLE = 'Rock_COL_srgb.1001.exr'
