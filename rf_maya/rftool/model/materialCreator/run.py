import sys

from PySide2 import QtCore, QtWidgets
from shiboken2 import wrapInstance

import app
reload(app)

def mayaRun():
    '''
    from rf_maya.rftool.model.materialCreator import run  as mtcRun
    reload(mtcRun)
    materialCreatorToolApp = mtcRun.mayaRun()
    '''
    import maya.OpenMayaUI as omui
    global MaterialCreatorToolApp

    try:
        MaterialCreatorToolApp.close()
    except:
        pass
    
    ptr = omui.MQtUtil.mainWindow()
    MaterialCreatorToolApp = app.MaterialCreatorTool(parent=wrapInstance(long(ptr), QtWidgets.QWidget))
    MaterialCreatorToolApp.show()

    return MaterialCreatorToolApp