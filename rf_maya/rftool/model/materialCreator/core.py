import os
import re
import math
import glob
import tempfile
from PySide2 import QtCore
from collections import defaultdict, OrderedDict

import pymel.core as pm
from maya.api import OpenMaya as om

from rf_utils import fileTexturePathResolver as tex_resolver
from rf_utils.oiio import run_oiio
reload(run_oiio)
import presets
reload(presets)

VERSION = '5.2.0'
# ------- CHANGE LOG
# 1.0 initial release as RsMaterialCreator (redshift only)
# 2.0 add Arnold renderer support
# 3.0 add Preview shader creation
# 3.1 add support for Height map
# 3.1.1 change ALPHA from Alpha to Opacity
# 4.0 - fix bug displaying .rstexbim
#     - add "Match Style" option defined in presets module file
# 4.1 - add more match style presets
#     - remove help menu bar, moved to preset comboBox toolTip
# 5.0 - ACES support
# 5.1 - Add Miarmy preset
#     - Add support for Emission map type
# 5.2.0 - add support for useFrameExtension on file node when generating preview textures

SEP = '_'
PREV_FLAT = 'prevFlat'
PREV_TEX = 'prevTex'
SHD_SUFFIX = 'Shd'
FILE_SUFFIX = 'File'
REMHSV_SUFFIX = 'RemapHsv'
REV_SUFFIX = 'Rev'
NML_SUFFIX = 'Normal'
HIG_SUFFIX = 'Height'
# IGNORE_EXT = ('.psd')
MIMAGE_SUPPORT = ('.tif', '.png', '.jpg', '.iff')
RAMP_CONVERT_EXT = '.png'

BLACK_TOL = 0.01
PREVIEW_FILTER_TYPE = 3
DOMIMANT_COLOR_RESIZE = 64
DEFAULT_TEX_PREVIEW_SIZE = 256
DOM_N = 5
DOM_T = 3

# ----------- ACES SETUP
USE_ACES = os.environ['OCIO'] if 'OCIO' in os.environ and os.environ['OCIO'] else ''
ACES_SRGB_TEXTURE = 'Utility - sRGB - Texture'
ACES_ACESCG = 'ACES - ACEScg'
ACES_RAW = 'Utility - Raw'
ACES_FORMATS = ('.exr')

def loadRedshift():
    rs_plugin = 'redshift4maya.mll'
    if not pm.pluginInfo(rs_plugin, q=True, l=True):
        om.MGlobal.displayWarning('Redshift plugin is not loaded. Loading...')
        pm.loadPlugin(rs_plugin)
    dr = pm.PyNode('defaultRenderGlobals')
    if dr.currentRenderer.get() != 'redshift':
        om.MGlobal.displayWarning('Redshift is not the current renderer, this will set it for you.')
        dr.currentRenderer.set('redshift')

    if not pm.objExists('redshiftOptions'):
        pm.createNode('RedshiftOptions')

def loadArnold():
    ai_plugin = 'mtoa.mll'
    if not pm.pluginInfo(ai_plugin, q=True, l=True):
        om.MGlobal.displayWarning('Arnold plugin is not loaded. Loading...')
        pm.loadPlugin(ai_plugin)
    dr = pm.PyNode('defaultRenderGlobals')
    if dr.currentRenderer.get() != 'arnold':
        om.MGlobal.displayWarning('Arnold is not the current renderer, this will set it for you.')
        dr.currentRenderer.set('arnold')

    if not pm.objExists('defaultArnoldRenderOptions'):
        pm.createNode('defaultArnoldRenderOptions')

def convert_aces_preview(paths):
    # convert aces to png - write to temp
    tmp_dir = tempfile.mkdtemp().replace('\\', '/')
    temp_paths = []
    for path in paths:
        basename = os.path.basename(path)
        out_fh, out_temp = tempfile.mkstemp(dir=tmp_dir)
        os.close(out_fh)
        temp_path = '{}/{}.png'.format(tmp_dir, os.path.splitext(basename)[0])
        os.rename(out_temp, temp_path)
        temp_paths.append(temp_path)
    output_paths = run_oiio.convert_colorspace(paths, temp_paths, 'ACES - ACEScg', 'Utility - sRGB - Texture')
    return output_paths

def convert_to_aces(input_paths, output_paths, mapType, preset):
    if mapType in preset.COLOR_TYPES:
        print 'Using Utility - sRGB - Texture to convert mapType: {}'.format(mapType)
        output_paths = run_oiio.convert_colorspace(input_paths, output_paths, 'Utility - sRGB - Texture', 'ACES - ACEScg')
    else:
        print 'Using Utility - Raw to convert mapType: {}'.format(mapType)
        output_paths = run_oiio.convert_colorspace(input_paths, output_paths, 'Utility - Raw', 'ACES - ACEScg')
    return output_paths

def invert_srgb_cc(color):
    cc_col = [0.0, 0.0, 0.0]
    for i in xrange(3):
        cc_col[i] = math.pow(color[i], 2.2)
    return cc_col

def to_srgb_cc(color):
    cc_col = [0.0, 0.0, 0.0]
    for i in xrange(3):
        cc_col[i] = math.pow(color[i], 1.0/2.2)
    return cc_col

def get_dominant_color(image, colorManaged=True):
    from nuTools.util import shaderUtil as nsu
    reload(nsu)
    from nuTools.taken import canvas as cvs
    reload(cvs)
    import colorsys

    rgbs = cvs.image_to_floatarray(image)
    rgbs = [(rgbs[i],rgbs[i+1],rgbs[i+2],rgbs[i+3]) for i in xrange(0, len(rgbs), 4)]
    doms = nsu.get_dominant_color(rgbs, n=DOM_N)

    # cut out the blacks
    dominant_colors = []
    for dom in doms[:DOM_T]:
        hsv = colorsys.rgb_to_hsv(dom[0], dom[1], dom[2])
        if abs(1.0 - (hsv[2]+1.0)) > BLACK_TOL:
            dominant_colors.append(dom)
    if not dominant_colors:
        dominant_colors = doms[:DOM_T]

    gamma_doms = []
    for dom in dominant_colors:
        if colorManaged:
            dom = nsu.invert_srgb_cc(dom)
        gamma_doms.append(dom)
        
    avg = nsu.averageRGBs(gamma_doms)
    # dom_colors = [int(c*255.0) for c in avg] 
    return avg

class MaterialCreator(QtCore.QObject):
    PREVIEW_MATERIAL = 'lambert'
    PREVIEW_COLOR_ATTR = 'color'
    PREVIEW_TRANS_ATTR = 'transparency'

    MATERIALS = ('lambert', 'blinn', 'phong', 'phongE')
    COLOR_ATTR = 'color'
    TRANS_ATTR = 'transparency'
    MORE_MEANS_OPAQUE = False

    preset = presets.Riff

    message = QtCore.Signal(str)
    def __init__(self, directory=''):
        super(MaterialCreator, self).__init__()
        self.directory = os.path.abspath(directory).replace('\\', '/')
        self.prefix = ''
        self.data = defaultdict(dict)  # {name: {mapType:[file1, file2, ...], }}

    def to_udim_path(self, path):
        dirName = os.path.dirname(path)
        baseName = os.path.basename(path)
        fn, ext = os.path.splitext(baseName)
        udimStr = ''
        searchUDIM = re.search('[0-9][0-9][0-9][0-9]', fn)
        if searchUDIM:
            udimStr = searchUDIM.group()
            fn = fn.replace(udimStr, '<UDIM>')

        result = '%s/%s%s' %(dirName, fn, ext)
        return result

    def set_prefix(self, prefix):
        if prefix:
            self.prefix = prefix + SEP
        else:
            self.prefix = ''

    def create_file(self, name):
        # if pm.objExists(name) and pm.nodeType(name) == 'file':
        #     fileNode = pm.PyNode(name)
        # else:
        fileNode = pm.shadingNode('file', asTexture=True, isColorManaged=True, n=name)
        fileNode.filterType.set(0)
        fileNode.ignoreColorSpaceFileRules.set(True)
        p2d = pm.shadingNode('place2dTexture', asUtility=True)
        p2d.outUV >> fileNode.uvCoord
        p2d.outUvFilterSize >> fileNode.uvFilterSize
        p2d.vertexCameraOne >> fileNode.vertexCameraOne
        p2d.vertexUvOne >> fileNode.vertexUvOne
        p2d.vertexUvTwo >> fileNode.vertexUvTwo
        p2d.vertexUvThree >> fileNode.vertexUvThree
        p2d.coverage >> fileNode.coverage
        p2d.mirrorU >> fileNode.mirrorU
        p2d.mirrorV >> fileNode.mirrorV
        p2d.noiseUV >> fileNode.noiseUV
        p2d.offset >> fileNode.offset
        p2d.repeatUV >> fileNode.repeatUV
        p2d.rotateFrame >> fileNode.rotateFrame
        p2d.rotateUV >> fileNode.rotateUV
        p2d.stagger >> fileNode.stagger
        p2d.translateFrame >> fileNode.translateFrame
        p2d.wrapU >> fileNode.wrapU
        p2d.wrapV >> fileNode.wrapV

        return fileNode

    def populate(self):
        pm.waitCursor(st=True)
        self.data = defaultdict(dict)
        if not self.directory or not os.path.exists(self.directory):
            pm.waitCursor(st=False)
            return

        all_files = [f for f in os.listdir(self.directory) if os.path.isfile('%s/%s' %(self.directory, f))\
                    and f.startswith(self.prefix) and os.path.splitext(f)[1] in MIMAGE_SUPPORT]

        if not all_files:
            print 'no files'
            pm.waitCursor(st=False)
            return

        for file in all_files:
            fn, ext = os.path.splitext(file)

            # # UDIM, check for 4 digits
            # udimStr = ''
            # searchUDIM = re.search('[0-9][0-9][0-9][0-9]', fn)
            # if searchUDIM:
            #     udimStr = searchUDIM.group()

            splits = fn.split(self.preset.FILE_SEP)
            len_splits = len(splits)

            try:
                mapType = splits[self.preset.mapType_index]
                if isinstance(self.preset.name_index, (str, unicode)):
                    exec('names = splits[{}]'.format(self.preset.name_index))
                    name = self.preset.FILE_SEP.join(names)
                else:
                    name = splits[self.preset.name_index]
            except IndexError:
                continue

            if mapType not in self.preset.MAP_TYPES:
                continue    

            fp = '%s/%s' %(self.directory, file)
            if mapType in self.data[name]:
                self.data[name][mapType].append(fp)
            else:
                self.data[name][mapType] = [fp]

        pm.waitCursor(st=False)

    def create_preview_shader(self, shaderName, previewName):
        shaderName = '%s%s%s' %(previewName, SEP, shaderName)
        # if pm.objExists(shaderName) and pm.nodeType(shaderName) == self.PREVIEW_MATERIAL:
        #     preview_shader = pm.PyNode(shaderName)
        # else:  # create new
        preview_shader = pm.shadingNode(self.PREVIEW_MATERIAL, asShader=True, n=shaderName)
        return preview_shader

    def get_preview_path(self, shader, attrName):
        shaderAttr = shader.attr(attrName)
        inputNodes = shaderAttr.inputs(type=('file', 'ramp'))
        for childAttrName in shaderAttr.getChildren():
            inputs = childAttrName.inputs(type=('file', 'ramp'))
            if inputs:
                inputNodes.extend(inputs)
        inputNodes = list(set(inputNodes))
        if not inputNodes:
            return None, None

        inputNode = inputNodes[0]
        inputNodeType = inputNode.nodeType()
        result_paths = OrderedDict()
        outputAttrs =  [c for c in inputNode.outputs(c=True) if shader in c]
        outAttr = outputAttrs[0][0].longName()

        useFrameExtension = False
        if inputNodeType == 'file':
            texturePath = inputNode.fileTextureName.get()
            texPathBaseName = os.path.basename(texturePath)

            useFrameExtension = inputNode.useFrameExtension.get()
            uvTilingMode = inputNode.uvTilingMode.get()

            if '<UDIM>' in texPathBaseName:
                texturePaths = sorted(glob.glob(texturePath.replace('<UDIM>', '*')))
                udim_splits = texPathBaseName.split('<UDIM>')
            else:
                if useFrameExtension:
                    pattern = tex_resolver.getFilePatternString(texturePath, useFrameExtension, uvTilingMode)
                    texturePaths = tex_resolver.findAllFilesForPattern(pattern, None)
                else:
                    texturePaths = [texturePath]
                udim_splits = []
            texturePaths = [path.replace('\\', '/') for path in texturePaths]

            # in ACES, convert .exr to .png for preview
            if USE_ACES:
                convertPaths = convert_aces_preview(texturePaths)
                texturePaths = convertPaths

            for path in texturePaths:
                if not os.path.exists(path):
                    continue
                file, ext = os.path.splitext(path)
                if ext not in MIMAGE_SUPPORT:
                    continue
                        
                if udim_splits:
                    pathBaseName = os.path.basename(path)
                    udim_key = str(pathBaseName)
                    for part in udim_splits:
                        udim_key = udim_key.replace(part, '')
                else:
                    udim_key = ''

                if udim_key not in result_paths:
                    result_paths[udim_key] = []
                result_paths[udim_key].append(path)

        elif inputNodeType == 'ramp':
            rampNodeName = inputNode.nodeName().split(':')[-1]
            tmpRampPath = '%s/projects/default/sourceimages/%s%s' %(os.environ['maya_app_dir'], rampNodeName, RAMP_CONVERT_EXT)
            tmpRampPath = tmpRampPath.replace('\\', '/')
            fileNodes = pm.convertSolidTx(inputNode, 
                        samplePlane=True, 
                        alpha=True, f=True, 
                        rx=DEFAULT_TEX_PREVIEW_SIZE, ry=DEFAULT_TEX_PREVIEW_SIZE, 
                        fil=RAMP_CONVERT_EXT,
                        fin=tmpRampPath)
            pm.delete(fileNodes)
            result_paths[''] = tmpRampPath

        return result_paths, outAttr

    def create_plain_preview(self, shader, colorManaged=True):
        if isinstance(shader, (str, unicode)):
            try:
                shader = pm.PyNode(shader)
            except:
                return
        nodeType = shader.nodeType()
        if nodeType != self.MATERIAL and nodeType not in self.MATERIALS:
            return

        preview_shaders = {}
        shader_name = shader.nodeName().split(':')[-1]

        color_attr = None
        current_color_attr = self.COLOR_ATTR
        parent_color_attr = super(self.__class__, self).COLOR_ATTR
        if shader.hasAttr(current_color_attr):
            color_attr = current_color_attr
        elif shader.hasAttr(parent_color_attr):
            color_attr = parent_color_attr
        else:
            return

        trans_attr = None
        more_means_opaque = False
        current_trans_attr = self.TRANS_ATTR
        parent_trans_attr = super(self.__class__, self).TRANS_ATTR
        current_mmo = self.MORE_MEANS_OPAQUE
        parent_mmo = super(self.__class__, self).MORE_MEANS_OPAQUE
        if shader.hasAttr(current_trans_attr):
            trans_attr = current_trans_attr
            more_means_opaque = current_mmo
        elif shader.hasAttr(parent_trans_attr):
            trans_attr = parent_trans_attr
            more_means_opaque = parent_mmo
        else:
            return

        texturePaths, outAttr = self.get_preview_path(shader=shader, attrName=color_attr)
        transPaths, outAttr = self.get_preview_path(shader=shader, attrName=trans_attr)

        # no color texture connected
        colorDict = defaultdict(dict)  # {name: (color, trans)}
        setValueDict = {}  # {attrName: value}
        if not texturePaths:
            old_colors = shader.attr(color_attr).get()
            setValueDict[self.PREVIEW_COLOR_ATTR] = old_colors
        else: # color has file node connected
            for udim, texturePath in texturePaths.iteritems():
                # read image, resize
                image = om.MImage()
                image.readFromFile(texturePath[0])
                image.resize(DOMIMANT_COLOR_RESIZE, DOMIMANT_COLOR_RESIZE)

                # get dominant color
                dom_colors = get_dominant_color(image=image, colorManaged=colorManaged)
                colorDict[udim][self.PREVIEW_COLOR_ATTR] = dom_colors

        # no transparency connected
        if not transPaths:
            trans_value = shader.attr(trans_attr).get()
            if more_means_opaque:
                trans_value = (abs(1.0 - trans_value[0]), abs(1.0 - trans_value[1]), abs(1.0 - trans_value[2]))
            setValueDict[self.PREVIEW_TRANS_ATTR] = trans_value
        else:  # transparency has file node connected
            for udim, texturePath in transPaths.iteritems():
                # read image, resize
                image = om.MImage()
                image.readFromFile(texturePath[0])
                image.resize(DOMIMANT_COLOR_RESIZE, DOMIMANT_COLOR_RESIZE)

                # get dominant color
                dom_trans = get_dominant_color(image=image, colorManaged=colorManaged)
                colorDict[udim][self.PREVIEW_TRANS_ATTR] = dom_trans

        for udim, colorData in colorDict.iteritems():
            # create new shader
            preview_name = '%s%s' %(PREV_FLAT, udim)
            preview_shader = self.create_preview_shader(shaderName=shader_name, previewName=preview_name)
            for attrName, color in colorData.iteritems():
                preview_shader.attr(attrName).set(color)

            preview_shaders[udim] = preview_shader

        if not preview_shaders:
            preview_shaders[''] = self.create_preview_shader(shaderName=shader_name, previewName=PREV_FLAT)
        # set values
        for udim, prvShd in preview_shaders.iteritems():
            for attrName, value in setValueDict.iteritems():
                prvShd.attr(attrName).set(value)

        return preview_shaders

    def create_textured_preview(self, shader, previewTextureSize=DEFAULT_TEX_PREVIEW_SIZE, colorManaged=True):
        if isinstance(shader, (str, unicode)):
            try:
                shader = pm.PyNode(shader)
            except:
                return
        nodeType = shader.nodeType()
        if nodeType != self.MATERIAL and nodeType not in self.MATERIALS:
            return

        preview_shaders = {}  # {shader:[file, file...]}
        shader_name = shader.nodeName().split(':')[-1]

        color_attr = None
        current_color_attr = self.COLOR_ATTR
        parent_color_attr = super(self.__class__, self).COLOR_ATTR
        isDefaultShader = False
        if shader.hasAttr(current_color_attr):
            color_attr = current_color_attr
        elif shader.hasAttr(parent_color_attr):
            color_attr = parent_color_attr
            isDefaultShader = True
        else:
            return

        trans_attr = None
        more_means_opaque = False
        current_trans_attr = self.TRANS_ATTR
        parent_trans_attr = super(self.__class__, self).TRANS_ATTR
        current_mmo = self.MORE_MEANS_OPAQUE
        parent_mmo = super(self.__class__, self).MORE_MEANS_OPAQUE
        
        if shader.hasAttr(current_trans_attr):
            trans_attr = current_trans_attr
            more_means_opaque = current_mmo
        elif shader.hasAttr(parent_trans_attr):
            trans_attr = parent_trans_attr
            more_means_opaque = parent_mmo
        else:
            return

        texturePaths, colOutAttr = self.get_preview_path(shader=shader, attrName=color_attr)
        transPaths, trOutAttr = self.get_preview_path(shader=shader, attrName=trans_attr)

        # no color texture connected
        colorDict = defaultdict(dict) 
        setValueDict = {}
        if not texturePaths:
            old_colors = shader.attr(color_attr).get()
            setValueDict[self.PREVIEW_COLOR_ATTR] = old_colors
        else: # color has file node connected
            for udim, texturePaths in texturePaths.iteritems():
                useFrameExtension = True if len(texturePaths) > 1 else False
                previewPath = ''
                dom_colors = (0.5, 0.5, 0.5)
                for texturePath in texturePaths:
                    dirname = os.path.dirname(texturePath)
                    basename = os.path.basename(texturePath)
                    filename, ext = os.path.splitext(basename)

                    # read image, resize
                    image = om.MImage()
                    image.readFromFile(texturePath)
                    image.resize(previewTextureSize, previewTextureSize)

                    preview_name = '%s%s' %(PREV_TEX, udim)
                    # previewPath = '%s/%s%s%s%s' %('D:/__playground/test_preview_shd', preview_name, SEP, filename, ext)
                    previewPath = '%s/%s%s%s%s' %(dirname, preview_name, SEP, filename, ext)
                    image.writeToFile(previewPath, ext[1:])

                    image.resize(DOMIMANT_COLOR_RESIZE, DOMIMANT_COLOR_RESIZE)
                    dom_colors = get_dominant_color(image=image, colorManaged=colorManaged)

                colorDict[udim][(colOutAttr, self.PREVIEW_COLOR_ATTR)] = {'path':previewPath, 'invert':None, 'dominant': dom_colors, 'useFrameExtension': useFrameExtension}

        # no transparency connected
        if not transPaths:
            trans_value = shader.attr(trans_attr).get()
            if more_means_opaque:
                trans_value = (abs(1.0 - trans_value[0]), abs(1.0 - trans_value[1]), abs(1.0 - trans_value[2]))
            setValueDict[self.PREVIEW_TRANS_ATTR] = trans_value
        else:  # transparency has file node connected
            for udim, transPath in transPaths.iteritems():
                dirname = os.path.dirname(transPath[0])
                basename = os.path.basename(transPath[0])
                filename, ext = os.path.splitext(basename)

                # read image, resize
                image = om.MImage()
                image.readFromFile(transPath[0])
                image.resize(previewTextureSize, previewTextureSize)
                preview_name = '%s%s' %(PREV_TEX, udim)
                previewPath = '%s/%s%s%s%s' %(dirname, preview_name, SEP, filename, ext)
                image.writeToFile(previewPath, ext[1:])

                # only invert tranparency only if self.MORE_MEANS_OPAQUE is not matching
                # between __class__ and the parent class AND shader is not lambert, blinn, etc..
                invert = False
                if current_mmo != parent_mmo and not isDefaultShader:
                    invert = True

                colorDict[udim][(trOutAttr, self.PREVIEW_TRANS_ATTR)] = {'path':previewPath, 'invert':invert, 'dominant': None}

        for udim, colorData in colorDict.iteritems():
            # create new shader
            preview_name = '%s%s' %(PREV_TEX, udim)
            preview_shader = self.create_preview_shader(shaderName=shader_name, previewName=preview_name)

            for attrNames, input_data in colorData.iteritems():
                srcAttr = attrNames[0]
                desAttr = attrNames[1]

                data = input_data['path']
                invert = input_data['invert']
                dominant = input_data['dominant']
                useFrameExtension = input_data.get('useFrameExtension', None)

                prevFileNodeName = '%s%s%s%s%s%s%s' %(preview_name, SEP, shader_name, SEP, desAttr, SEP, FILE_SUFFIX)
                prevFileNode = self.create_file(prevFileNodeName)
                prevFileNode.fileTextureName.set(data)
                if dominant:
                    prevFileNode.defaultColor.set(dominant)
                if USE_ACES:
                    prevFileNode.colorSpace.set(ACES_SRGB_TEXTURE)
                if useFrameExtension:
                    prevFileNode.useFrameExtension.set(True)


                prevFileNode.filterType.set(PREVIEW_FILTER_TYPE)  # Quadratic filter type
                fileSrcAttr = prevFileNode.attr(srcAttr)
                fileSrcAttrTyp = fileSrcAttr.type()
                prvShdDesAttr = preview_shader.attr(desAttr)
                if invert == True:
                    prevRevNodeName = '%s%s%s%s%s' %(preview_name, SEP, shader_name, SEP, REV_SUFFIX)
                    # if pm.objExists(prevRevNodeName) and pm.nodeType(prevRevNodeName) == 'reverse':
                    #     rev = pm.PyNode(prevRevNodeName)
                    # else:
                    rev = pm.shadingNode('reverse', asUtility=True, n=prevRevNodeName)

                    if fileSrcAttrTyp == 'float':
                        pm.connectAttr(fileSrcAttr, rev.inputX, f=True)
                        pm.connectAttr(fileSrcAttr, rev.inputY, f=True)
                        pm.connectAttr(fileSrcAttr, rev.inputZ, f=True)
                    else:
                        pm.connectAttr(fileSrcAttr, rev.input, f=True)
                    pm.connectAttr(rev.output, preview_shader.attr(desAttr), f=True)
                else:
                    if fileSrcAttrTyp == 'float':
                        for cAttr in prvShdDesAttr.getChildren():
                            pm.connectAttr(fileSrcAttr, cAttr, f=True)
                    else:
                        pm.connectAttr(fileSrcAttr, prvShdDesAttr, f=True)

            preview_shaders[udim] = preview_shader

        preview_shader_nodes = preview_shaders.values()
        if not preview_shader_nodes:
            preview_shaders[''] = self.create_preview_shader(shaderName=shader_name, previewName=PREV_TEX)
        # set values
        for udim, prvShd in preview_shaders.iteritems():
            for attrName, value in setValueDict.iteritems():
                prvShd.attr(attrName).set(value)

        return preview_shaders

    def convert_aces_texture(self, mapType, files):
        src_convert_list, dst_convert_list = [], []
        result_list = []
        self.message.emit('Converting images to ACES colorspace..')
        for file in files:
            file_mod_time = os.path.getmtime(file)
            fn, ext = os.path.splitext(file)
            exr_path = '{}.exr'.format(fn)
            if not os.path.exists(exr_path) or os.path.getmtime(exr_path) < file_mod_time:
                # print 'Converting to ACES: {}'.format(file)
                src_convert_list.append(file)
                dst_convert_list.append(exr_path)
            result_list.append(exr_path)
        if src_convert_list:
            convert_to_aces(src_convert_list, dst_convert_list, mapType, self.preset)
        return result_list

class RsMaterialCreator(MaterialCreator):
    MATERIAL = 'RedshiftMaterial'
    COLOR_ATTR = 'diffuse_color'
    TRANS_ATTR = 'opacity_color'
    MORE_MEANS_OPAQUE = True

    def __init__(self, directory=''):
        super(RsMaterialCreator, self).__init__(directory=directory)

    def get_shader(self, name):
        shaderName = '%s%s%s' %(name, SEP, SHD_SUFFIX)
        # if pm.objExists(shaderName) and pm.nodeType(shaderName) == self.MATERIAL:
        #     shader = pm.PyNode(shaderName)
        # else:  # create new
        shader = pm.shadingNode(self.MATERIAL, asShader=True, n=shaderName)
        sg = pm.sets(renderable=True, noSurfaceShader=True, empty=True, name='%sSG' %(shaderName))
        # set BRDF
        shader.refl_brdf.set(1)

        pm.connectAttr(shader.outColor, sg.surfaceShader, f=True)
        return shader, sg

    def create_map(self, shader, sg, name, mapType, files):
        ''' 
        Create and connect map to shader according to mapType
        '''
        fileNode = None
        if mapType == self.preset.DIFFUSE:
            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]

            fileNode.fileTextureName.set(tex_path)
            pm.connectAttr(fileNode.outColor, shader.diffuse_color, f=True)

        elif mapType == self.preset.ROUGHNESS:
            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]

            fileNode.fileTextureName.set(tex_path)
            fileNode.colorSpace.set('Raw')
            fileNode.alphaIsLuminance.set(True)
            pm.connectAttr(fileNode.outColorR, shader.refl_roughness, f=True)

        elif mapType == self.preset.NORMAL:
            rsNmlName = '%s%s%s%s' %(name, mapType, SEP, NML_SUFFIX)
            rsNml = pm.shadingNode('RedshiftNormalMap', asUtility=True, n=rsNmlName)
            
            if len(files) > 1:
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]            

            rsNml.tex0.set(tex_path)
            rsNml.flipY.set(True)  # Flip Normal Y
            pm.connectAttr(rsNml.outDisplacementVector, shader.bump_input, f=True)

        elif mapType == self.preset.HEIGHT:
            rsHigName = '%s%s%s%s' %(name, mapType, SEP, HIG_SUFFIX)
            rsDisp = pm.shadingNode('RedshiftDisplacement', asUtility=True, n=rsHigName)
            
            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)
            if len(files) > 1:
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]            

            fileNode.fileTextureName.set(tex_path)
            pm.connectAttr(fileNode.outColor, rsDisp.texMap)
            pm.connectAttr(rsDisp.out, sg.displacementShader, f=True)

        elif mapType == self.preset.METALLIC:
            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]

            fileNode.fileTextureName.set(tex_path)
            fileNode.colorSpace.set('Raw')
            fileNode.alphaIsLuminance.set(True)
            shader.refl_fresnel_mode.set(2)
            pm.connectAttr(fileNode.outColorR, shader.refl_metalness, f=True)

        elif mapType == self.preset.ALPHA: 
            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]

            fileNode.fileTextureName.set(tex_path)
            pm.connectAttr(fileNode.outAlpha, shader.opacity_colorR, f=True)
            pm.connectAttr(fileNode.outAlpha, shader.opacity_colorG, f=True)
            pm.connectAttr(fileNode.outAlpha, shader.opacity_colorB, f=True)

        elif mapType == self.preset.EMISSION:
            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]

            fileNode.fileTextureName.set(tex_path)
            shader.emission_weight.set(1.0)
            pm.connectAttr(fileNode.outColor, shader.emission_color, f=True)

        elif mapType == self.preset.ROUGHNESS_BUMP_METALLIC:
            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            rsHigName = '%s%s%s%s' %(name, mapType, SEP, HIG_SUFFIX)
            rsDisp = pm.shadingNode('RedshiftDisplacement', asUtility=True, n=rsHigName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]

            fileNode.fileTextureName.set(tex_path)
            fileNode.colorSpace.set('Raw')
            fileNode.alphaIsLuminance.set(True)
            shader.refl_fresnel_mode.set(2)

            pm.connectAttr(fileNode.outColorR, shader.refl_roughness, f=True)
            pm.connectAttr(fileNode.outColorG, rsDisp.texMapR, f=True)
            pm.connectAttr(fileNode.outColorG, rsDisp.texMapG, f=True)
            pm.connectAttr(fileNode.outColorG, rsDisp.texMapB, f=True)
            pm.connectAttr(rsDisp.out, sg.displacementShader, f=True)
            pm.connectAttr(fileNode.outColorB, shader.refl_metalness, f=True)

        # ACES override
        if USE_ACES and fileNode:
            if mapType in self.preset.COLOR_TYPES:
                fileNode.colorSpace.set(ACES_ACESCG)
            else:
                fileNode.colorSpace.set(ACES_RAW)

    def run(self, data): # {name: {mapType:[file1, file2, ...], }}
        # make sure Redshift is loaded
        loadRedshift()
        
        # actually create the shading networks...
        pm.undoInfo(openChunk=True)
        for name, maps in data.iteritems():
            shader, sg = self.get_shader(name=name)
            for mapType, files in maps.iteritems():
                if USE_ACES:
                    files = self.convert_aces_texture(mapType, files)
                self.create_map(shader, sg, name, mapType, files)
        pm.undoInfo(closeChunk=True)

class AiMaterialCreator(MaterialCreator):
    MATERIAL = 'aiStandardSurface'
    COLOR_ATTR = 'baseColor'
    TRANS_ATTR = 'opacity'
    MORE_MEANS_OPAQUE = True

    def __init__(self, directory=''):
        super(AiMaterialCreator, self).__init__(directory=directory)

    def get_shader(self, name):
        shaderName = '%s%s%s' %(name, SEP, SHD_SUFFIX)
        # if pm.objExists(shaderName) and pm.nodeType(shaderName) == self.MATERIAL:
        #     shader = pm.PyNode(shaderName)
        # else:  # create new
        shader = pm.shadingNode(self.MATERIAL, asShader=True, n=shaderName)
        sg = pm.sets(renderable=True, noSurfaceShader=True, empty=True, name='%sSG' %(shaderName))
        pm.connectAttr(shader.outColor, sg.surfaceShader, f=True)
        return shader, sg

    def create_map(self, shader, sg, name, mapType, files):
        ''' 
        Create and connect map to shader according to mapType
        '''

        if mapType == self.preset.DIFFUSE:
            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]

            # remap hsv node
            remapNodeName = '%s%s%s%s' %(name, mapType, SEP, REMHSV_SUFFIX)
            remapNode = pm.shadingNode('remapHsv', asUtility=True, n=remapNodeName)


            fileNode.fileTextureName.set(tex_path)
            pm.connectAttr(fileNode.outColor, shader.baseColor, f=True)
            pm.connectAttr(fileNode.outColor, remapNode.color)
            pm.connectAttr(remapNode.outColorR, shader.specular, f=True)

        elif mapType == self.preset.ROUGHNESS:
            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]

            fileNode.fileTextureName.set(tex_path)
            fileNode.colorSpace.set('Raw')
            fileNode.alphaIsLuminance.set(True)
            pm.connectAttr(fileNode.outColorR, shader.specularRoughness, f=True)

        elif mapType == self.preset.NORMAL:
            bumpName = '%s%s%s%s' %(name, mapType, SEP, NML_SUFFIX)
            bump2d = pm.shadingNode('aiNormalMap', asUtility=True, n=bumpName)

            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]            
            fileNode.fileTextureName.set(tex_path)

            pm.connectAttr(fileNode.outColor, bump2d.input)
            pm.connectAttr(bump2d.outValue, shader.normalCamera, f=True)

        elif mapType == self.preset.HEIGHT:
            dispName = '%s%s%s%s' %(name, mapType, SEP, HIG_SUFFIX)
            dispShd = pm.shadingNode('displacementShader', asUtility=True, n=dispName)
            
            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]            

            fileNode.fileTextureName.set(tex_path)
            pm.connectAttr(fileNode.outAlpha, dispShd.displacement, f=True)
            pm.connectAttr(dispShd.displacement, sg.displacementShader, f=True)

        elif mapType == self.preset.METALLIC:
            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]

            fileNode.fileTextureName.set(tex_path)
            fileNode.colorSpace.set('Raw')
            fileNode.alphaIsLuminance.set(True)
            pm.connectAttr(fileNode.outAlpha, shader.metalness, f=True)

        elif mapType == self.preset.ALPHA: 
            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]

            fileNode.fileTextureName.set(tex_path)
            pm.connectAttr(fileNode.outColor, shader.opacity, f=True)

        elif mapType == self.preset.TRANSMISSION: 
            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]

            fileNode.fileTextureName.set(tex_path)
            fileNode.colorSpace.set('Raw')
            pm.connectAttr(fileNode.outAlpha, shader.transmission, f=True)
            pm.connectAttr(fileNode.outColor, shader.transmissionColor, f=True)

        elif mapType == self.preset.EMISSION:
            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]

            fileNode.fileTextureName.set(tex_path)
            shader.emission.set(1.0)
            pm.connectAttr(fileNode.outColor, shader.emissionColor, f=True)

        elif mapType == self.preset.ROUGHNESS_BUMP_METALLIC:
            dispName = '%s%s%s%s' %(name, mapType, SEP, HIG_SUFFIX)
            dispShd = pm.shadingNode('displacementShader', asUtility=True, n=dispName)

            fileNodeName = '%s%s%s%s' %(name, mapType, SEP, FILE_SUFFIX)
            fileNode = self.create_file(fileNodeName)

            if len(files) > 1:
                fileNode.uvTilingMode.set(3)
                tex_path = self.to_udim_path(files[0])
            else:
                tex_path = files[0]

            fileNode.fileTextureName.set(tex_path)
            fileNode.colorSpace.set('Raw')
            fileNode.alphaIsLuminance.set(True)

            pm.connectAttr(fileNode.outColorR, shader.specularRoughness, f=True)
            pm.connectAttr(fileNode.outColorG, dispShd.displacement, f=True)
            pm.connectAttr(dispShd.displacement, sg.displacementShader, f=True)
            pm.connectAttr(fileNode.outColorB, shader.metalness, f=True)

        # ACES override
        if USE_ACES and fileNode:
            fileNode.colorSpace.set(ACES_ACESCG)

    def run(self, data): # {name: {mapType:[file1, file2, ...], }}
        # make sure arnold is loaded
        loadArnold()
        
        # actually create the shading networks...
        pm.undoInfo(openChunk=True)
        for name, maps in data.iteritems():
            shader, sg = self.get_shader(name=name)
            for mapType, files in maps.iteritems():
                if USE_ACES:
                    files = self.convert_aces_texture(mapType, files)
                self.create_map(shader, sg, name, mapType, files)
        pm.undoInfo(closeChunk=True)

    def create_plain_preview(self, shader):
        pass

    def create_textured_preview(self, shader):
        pass

'''
from nuTools.taken import canvas as cvs
from nuTools.util import shaderUtil as nsu
reload(nsu)

path = 'D:/__playground/Arthur_Body_Diffuse_1001.jpg'
img = cvs.Canvas.from_file(path)
small_img = img.resized(512, 512)

dom = nsu.get_dominant_color(small_img.values())

for i in small_img.values():
    print i
    break

from maya.api.OpenMaya import MImage
reload(nsu)
im = MImage()
im.readFromFile(path)
im.resize(64, 64)

rgbs = cvs.image_to_floatarray(im)
rgbs = [(rgbs[i],rgbs[i+1],rgbs[i+2],rgbs[i+3]) for i in xrange(0, len(rgbs), 4)]
doms = nsu.get_dominant_color(rgbs, 5)


ress = []
for dom in doms[:2]:
    res = [0.0,0.0,0.0]
    if dom[0] <= 0.03928:
        res[0] = dom[0]/12.92
    else:
        res[0] = math.pow((dom[0] + 0.055)/1.055, 2.4)
    if dom[1] <= 0.03928:
        res[1] = dom[1]/12.92
    else:
        res[1] = math.pow((dom[1] + 0.055)/1.055, 2.4)
    if dom[2] <= 0.03928:
        res[2] = dom[2]/12.92
    else:
        res[2] = math.pow((dom[2] + 0.055)/1.055, 2.4)
    ress.append(res)
    
avg = nsu.averageRGBs(ress)
# avg = [avg[0]/255.0, avg[1]/255.0, avg[2]/255.0]
ress = [int(r*255.0) for r in avg] 
    

    



'''