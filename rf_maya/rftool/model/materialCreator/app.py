import os
from collections import defaultdict
import colorsys
import inspect

from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *

import maya.cmds as mc
import pymel.core as pm
import maya.OpenMaya as om
import maya.OpenMayaUI as omUI
from shiboken2 import wrapInstance

import core
import presets
import ui
reload(core)
reload(presets)
reload(ui)

# ----- TO DOs ----- #
#// - support transparency previe shader
#// - render shader conversion
#// - display converted shaders
#// - refresh script jobs

DEFAULT_PATH = ('%s%s' %(os.environ['HOMEDRIVE'], os.environ['HOMEPATH'])).replace('\\', '/')
RENDERERS = {'Redshift':core.RsMaterialCreator,
            'Arnold': core.AiMaterialCreator}
PREVIEW_RESOLUTIONS = (128, 256, 512, 1024)
MIDDLE_GREY = (0.5, 0.5, 0.5)
MIDDLE_GREY_INT = (128, 128, 128)
DEFAULT_PREVIEW_RESOUTION = 256
MAX_COL_PER_ROW = 7

class ColorButton(QPushButton):
    clicked = Signal(int)
    doubleClicked = Signal(int)

    def __init__(self, *args, **kwargs):
        super(ColorButton, self).__init__(*args, **kwargs)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)

        self.setSizePolicy(sizePolicy)
        self.setMinimumSize(QSize(50, 50))
        self.setText("")
        self.setCheckable(True)

        self.timer = QTimer()
        self.timer.setInterval(300)
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.timeout)
        self.click_count = 0

        self.toggled.connect(self.checked)

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.click_count += 1
            if not self.timer.isActive():
                self.timer.start()
                self.clicked.emit(1)

            else:
                self.timeout()
                self.timer.stop()
            
    def timeout(self):
        if self.click_count > 1 and self.timer.isActive():
            self.doubleClicked.emit(self.click_count)
        self.click_count = 0

    def checked(self, state):
        palette = self.palette()
        color = palette.color(QPalette.Button)

        if state:
            self.setStyleSheet("""background-color: rgba(%s, %s, %s, %s); border-style:outset; border-width: 2px;border-color:beige
            """ %(color.red(), color.green(), color.blue(), color.alpha()))
        else:
            self.setStyleSheet("background-color: rgba(%s, %s, %s, %s)" %(color.red(), color.green(), color.blue(), color.alpha()))
    
    def update_color(self):
        self.checked(self.isChecked())

class ColorButtonWidget(QWidget):
    def __init__(self, *args, **kwargs):
        super(ColorButtonWidget, self).__init__(*args, **kwargs)

    def mousePressEvent(self, event):
        ''' when user clicked on empty area on the color button layout '''
        layout = self.layout()
        buttonGrp = None
        for i in reversed(xrange(layout.count())): 
            button = layout.itemAt(i).widget()
            if not buttonGrp:
                buttonGrp = button.group()
                buttonGrp.setExclusive(False)
            if button:
                button.setChecked(False)
        buttonGrp.setExclusive(True)

class MaterialCreatorTool(QMainWindow, ui.Ui_MaterialCreator_MainWindow):

    def __init__(self, parent):
        super(MaterialCreatorTool, self).__init__(parent)

        # setup UI
        self.setupUi(self)
        self.setWindowTitle(QApplication.translate("MaterialCreator_MainWindow", 
                                                "Material Creator - %s" %core.VERSION, 
                                                None, -1))

        # instanciate core object
        self.core = None
        self.available_presets = {}

        self.color_buttons = {}
        self.color_shaders = {}
        self.ctrlFloatFields = []

        self.default_color_button = None
        self.newSceneJobID = None 
        self.sceneOpenedJobID = None
        self.selChangedJobId = None
        self.kPreviewNodeType = None
        
        self._suspend_selChange_scriptJob = False

        self.loadRenderer()
        # --- override UIs
        # create background listView
        self.listview = QListView(parent)
        self.fileModel = QFileSystemModel()
        self.fileModel.setRootPath(self.core.directory)
        self.fileModel.setFilter(QDir.NoDotAndDotDot | QDir.Files)
        self.listview.setModel(self.fileModel)
        self.listview.setRootIndex(self.fileModel.index(self.core.directory))

        # menu
        # self.help_naming_action.triggered.connect(self.help_naming)

        # color button group
        self.colorButtonGrp = QButtonGroup()

        # temp layout for invisible maya controls
        self.tmpLayout = QHBoxLayout(self)
        self.tmpLayout.setEnabled(False)

        # scroll widget
        self.scroll_widget = ColorButtonWidget()
        self.scroll_widget.setGeometry(QRect(0, 0, 380, 197))
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.scroll_widget.sizePolicy().hasHeightForWidth())
        self.scroll_widget.setSizePolicy(sizePolicy)
        self.scroll_widget.setMinimumSize(QSize(150, 150))
        self.scroll_widget.setObjectName("scroll_widget")
        brush = QBrush()
        brush.setStyle(Qt.BDiagPattern)
        palette = self.scroll_widget.palette()
        palette.setBrush(QPalette.Background, brush)
        self.scroll_widget.setPalette(palette)

        # scroll area
        self.colorButton_scrollArea = QScrollArea(self.create_groupBox)
        self.colorButton_scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.colorButton_scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.colorButton_scrollArea.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.colorButton_scrollArea.setWidgetResizable(True)
        self.colorButton_scrollArea.setObjectName("colorButton_scrollArea")
        self.colorButton_scrollArea.setWidget(self.scroll_widget)
        self.scroll_layout.addWidget(self.colorButton_scrollArea)

        # layout for color buttons
        self.colorButton_layout = QGridLayout(self.scroll_widget)
        self.colorButton_layout.setContentsMargins(6, 6, 6, 6)
        self.colorButton_layout.setObjectName("colorButton_layout")
 
        # right click menu
        self.scroll_widget.setContextMenuPolicy(Qt.CustomContextMenu)
        self.scroll_widget.customContextMenuRequested.connect(self.colorButtonRightClicked)

        # --- signals 
        self.cm_checkBox.toggled.connect(self.refresh_all_button_colors)
        self.renderer_comboBox.currentIndexChanged.connect(self.rendererChanged)
        self.preset_comboBox.currentIndexChanged.connect(self.presetChanged)
        self.shd_treeWidget.itemSelectionChanged.connect(self.itemSelected)
        self.browse_pushButton.clicked.connect(self.browse)
        self.dir_lineEdit.returnPressed.connect(self.directoryChanged)
        self.prefix_lineEdit.returnPressed.connect(self.prefixChanged)
        self.fileModel.rowsInserted.connect(self.populate)
        self.fileModel.rowsRemoved.connect(self.populate)

        self.create_pushButton.clicked.connect(self.create)

        self.mode_tabWidget.currentChanged.connect(self.tabSwitch)
        self.toFlat_pushButton.clicked.connect(self.convertToFlat)
        self.toTextured_pushButton.clicked.connect(self.convertToTextured)
        self.currentColor_pushButton.clicked.connect(self.new_preview_shader)

        self.h_horizontalSlider.sliderPressed.connect(self.start_editting_colors)
        self.s_horizontalSlider.sliderPressed.connect(self.start_editting_colors)
        self.v_horizontalSlider.sliderPressed.connect(self.start_editting_colors)
        self.t_horizontalSlider.sliderPressed.connect(self.start_editting_colors)

        self.h_horizontalSlider.valueChanged.connect(self.current_color_changed)
        self.s_horizontalSlider.valueChanged.connect(self.current_color_changed)
        self.v_horizontalSlider.valueChanged.connect(self.current_color_changed)
        self.t_horizontalSlider.valueChanged.connect(self.current_color_changed)

        self.h_horizontalSlider.sliderReleased.connect(self.end_editting_colors)
        self.s_horizontalSlider.sliderReleased.connect(self.end_editting_colors)
        self.v_horizontalSlider.sliderReleased.connect(self.end_editting_colors)
        self.t_horizontalSlider.sliderReleased.connect(self.end_editting_colors)

        self.init_ui()
        self.setupScriptJob()

    def init_ui(self):
        self.populate_presets()
        self.updatePreviewTexResolution()
        self.refreshPreviewShaderButtons()

    def populate_presets(self):
        self.available_presets = {}
        defaultIndex = 0
        i = 0
        for preset_name, preset_cls in inspect.getmembers(presets, inspect.isclass):
            self.available_presets[preset_name] = preset_cls
            self.preset_comboBox.addItem(str(preset_name))

            # default preset
            if hasattr(preset_cls, '_IS_DEFAULT'):
                defaultIndex = i
            i += 1
        self.preset_comboBox.setCurrentIndex(defaultIndex)

    def updatePreviewTexResolution(self):
        for res in PREVIEW_RESOLUTIONS:
            self.resolution_comboBox.addItem(str(res))
        defaultIndex = PREVIEW_RESOLUTIONS.index(DEFAULT_PREVIEW_RESOUTION)
        self.resolution_comboBox.setCurrentIndex(defaultIndex)

    def setupScriptJob(self):
        self.newSceneJobID = pm.scriptJob(e=('NewSceneOpened', self.refreshPreviewShaderButtons))
        self.sceneOpenedJobID = pm.scriptJob(e=('SceneOpened', self.refreshPreviewShaderButtons))
        self.selChangedJobId = pm.scriptJob(e=('SelectionChanged', self.updatePreviewShaderSelections))

    def closeEvent(self, event):
        for jid in [self.newSceneJobID, self.sceneOpenedJobID, self.selChangedJobId]:
            if jid and pm.scriptJob(ex=jid):
                pm.scriptJob(kill=jid, force=True)
        self.clearFloatControls()

    def colorButtonRightClicked(self, pos):
        widgetPos = self.scroll_widget.mapToGlobal(pos)
        mousePos = QCursor.pos()
        button = qApp.widgetAt(mousePos)

        if isinstance(button, ColorButton):
            # button.setChecked(True)
            self.color_button_clicked(button)
            rightClickMenu = QMenu(self.scroll_widget)

            assignAction = QAction('Assign...', self.scroll_widget)
            assignAction.triggered.connect(lambda: self.color_button_double_clicked(button=button))

            selObjAction = QAction('Select objects', self.scroll_widget)
            selObjAction.triggered.connect(lambda: self.select_objects_with_shader(button=button))

            selShdAction = QAction('Select shader', self.scroll_widget)
            selShdAction.triggered.connect(lambda: self.select_shader(button=button))
            
            delAction = QAction('Delete', self.scroll_widget)
            delAction.triggered.connect(lambda: self.delete_color_button(button=button))

            rightClickMenu.addAction(assignAction)
            rightClickMenu.addSeparator()
            rightClickMenu.addAction(selObjAction)
            rightClickMenu.addAction(selShdAction)
            rightClickMenu.addSeparator()
            rightClickMenu.addAction(delAction)

            if button == self.default_color_button:
                delAction.setEnabled(False)
            rightClickMenu.exec_(widgetPos)

    def delete_color_button(self, button):
        pm.undoInfo(openChunk=True)
        shader = self.color_buttons[button]
        pm.delete(shader)

        self._suspend_selChange_scriptJob = True
        self.refreshPreviewShaderButtons()
        self._suspend_selChange_scriptJob = False
        pm.undoInfo(closeChunk=True)

    def select_objects_with_shader(self, button):
        shader = self.color_buttons[button]
        pm.hyperShade(o=shader)
        # button.setChecked(True)

    def select_shader(self, button):
        shader = self.color_buttons[button]
        pm.select(shader, r=True)

    def tabSwitch(self):
        currentIndex = self.mode_tabWidget.currentIndex()
        if currentIndex == 1:  # changing to preview tab 
            self.refreshPreviewShaderButtons()

    def updatePreviewShaderSelections(self):
        mItDep = om.MItDependencyNodes(self.kPreviewNodeType) 
        existing_shaders = ['lambert1']
        while not mItDep.isDone():
            mobj = mItDep.thisNode()
            depFn = om.MFnDependencyNode(mobj)
            nodeName = depFn.name()
            if nodeName.startswith(core.PREV_FLAT):
                existing_shaders.append(nodeName)
            mItDep.next()
        if len(existing_shaders) != len(self.color_shaders):
            self.refreshPreviewShaderButtons()
        else:
            button = None
            current_shader_names = [s.nodeName() for s in self.color_shaders]
            msel = om.MSelectionList()
            om.MGlobal.getActiveSelectionList(msel)
            selIt = om.MItSelectionList(msel)
            while not selIt.isDone():
                mobj = om.MObject()
                selIt.getDependNode(mobj)
                if mobj.apiType() == self.kPreviewNodeType:
                    depFn = om.MFnDependencyNode(mobj)
                    nodeName = depFn.name()
                    if nodeName in current_shader_names:
                        button = self.color_shaders[pm.PyNode(nodeName)]
                        # button.setChecked(True)
                        self.color_button_clicked(button)
                        break
                selIt.next()
                
    def clearFloatControls(self):
        for ff in self.ctrlFloatFields:
            try:
                pm.deleteUI(ff)
            except:
                pass

        # for i in reversed(xrange(self.tmpLayout.count())): 
        #     widget = self.tmpLayout.itemAt(i).widget()
        #     if widget:
        #         widget.setParent(None)

        self.ctrlFloatFields = []

    def clearColorButtons(self):
        for i in reversed(xrange(self.colorButton_layout.count())): 
            button = self.colorButton_layout.itemAt(i).widget()
            if button:
                button.setParent(None)

        self.color_buttons = {}
        self.color_shaders = {}

    def refreshPreviewShaderButtons(self):
        # clear all buttons first
        self.clearColorButtons()
        self.clearFloatControls()

        # add lambert1
        lambert1 = pm.PyNode('lambert1')
        lambert1, self.default_color_button = self.new_preview_shader(shader=lambert1)
        self.default_color_button.setText('default')
        self.lock_preview_color_sliders(True)

        # find other shaders
        shaders = []
        for shd in pm.ls(type=self.core.PREVIEW_MATERIAL):
            shdName = shd.nodeName()
            if shdName.startswith(core.PREV_FLAT):
                shd, button = self.new_preview_shader(shader=shd)
                shaders.append(shd)

    def new_preview_shader(self, shader=None):
        # create shader
        if not shader:
            qcolor = self.currentColor_pushButton.palette().color(QPalette.Button)
            color = qcolor.getRgbF()

            if self.cm_checkBox.isChecked():
                color = core.invert_srgb_cc(color)

            inv_alpha = 1.0 - qcolor.alphaF()

            sels = [s for s in pm.selected() if isinstance(s, (pm.nt.Transform, pm.nt.Mesh, pm.MeshFace))]
            shaderName = '%s_%s' %(core.PREV_FLAT, self.core.PREVIEW_MATERIAL)
            shader = pm.shadingNode(self.core.PREVIEW_MATERIAL, asShader=True, n=shaderName)
            shader.attr(self.core.PREVIEW_COLOR_ATTR).set(color)
            shader.attr(self.core.PREVIEW_TRANS_ATTR).set((inv_alpha, inv_alpha, inv_alpha))
            if sels:
                pm.select(sels, r=True)

        color = shader.attr(self.core.PREVIEW_COLOR_ATTR).get()
        trans = shader.attr(self.core.PREVIEW_TRANS_ATTR).get()[0]

        # create button
        new_button = self.add_preview_shader_button(shader, color, trans)
        self.color_buttons[new_button] = shader
        self.color_shaders[shader] = new_button
        self.color_button_clicked(new_button)

        return shader, new_button

    def get_next_shader_row_column(self):
        numButton = len(self.color_buttons) + 1
        row = ((numButton - 1) / MAX_COL_PER_ROW) 
        column = ((numButton - 1) % MAX_COL_PER_ROW) 
        return row, column

    def add_preview_shader_button(self, shader, color, trans):
        # create the button
        new_button = ColorButton(self.scroll_widget)
        self.set_button_color(new_button, color, trans)

        # add button to layout
        row, column = self.get_next_shader_row_column()
        self.colorButtonGrp.addButton(new_button)
        self.colorButton_layout.addWidget(new_button, row, column)

        # signal
        new_button.clicked.connect(lambda: self.color_button_clicked(new_button))
        new_button.doubleClicked.connect(lambda: self.color_button_double_clicked(new_button))

        # maya attribute control
        colAttr = shader.attr(self.core.PREVIEW_COLOR_ATTR)
        transAttr = shader.attr(self.core.PREVIEW_TRANS_ATTR)
        attrs = colAttr.getChildren() + [transAttr.getChildren()[0]]

        for i, attr in enumerate(attrs):
            col_ff = pm.floatField(vis=False)
            pm.connectControl(col_ff, str(attr))

            ctrlPtr = omUI.MQtUtil.findControl(str(col_ff))
            lineEdit = wrapInstance(long(ctrlPtr), QLineEdit)
            lineEdit.textChanged.connect(lambda: self.shader_color_update(shader))
            self.tmpLayout.addWidget(lineEdit)
            self.ctrlFloatFields.append(col_ff)

        return new_button

    def shader_color_update(self, shader):
        if self._suspend_selChange_scriptJob:
            return
        color = shader.attr(self.core.PREVIEW_COLOR_ATTR).get()
        trans = shader.attr(self.core.PREVIEW_TRANS_ATTR).get()
        trans = trans[0]

        button = self.color_shaders[shader]
        self.set_button_color(button, color, trans)
        if button.isChecked():
            self.color_button_clicked(button)

    def set_button_color(self, button, color, trans):
        if isinstance(color[0], int) and isinstance(color[1], int) and isinstance(color[2], int):
            color = (color[0]/255.0, color[1]/255.0, color[2]/255.0)

        # if pm.colorManagementPrefs(q=True, cmEnabled=True):
        if self.cm_checkBox.isChecked():
            color = core.to_srgb_cc((color[0], color[1], color[2]))
        # print color, button
        palette = button.palette()
        inv_trans = float('%.2f' %(1.0-trans))

        qcolor = QColor.fromRgbF(color[0], color[1], color[2], inv_trans)
        palette.setColor(QPalette.Button, qcolor)
        button.setPalette(palette)

        # only update color on colorButtons NOT currentColor_button which is just regular QPushButton
        if isinstance(button, ColorButton):
            button.update_color()

    def refresh_all_button_colors(self):
        for button, shader in self.color_buttons.iteritems():
            color = shader.attr(self.core.PREVIEW_COLOR_ATTR).get()
            trans = shader.attr(self.core.PREVIEW_TRANS_ATTR).get()
            trans = trans[0]
            self.set_button_color(button, color, trans)

        rgb, trans = self.get_color_slider_values()
        self.set_button_color(self.currentColor_pushButton, rgb, trans)

    def get_color_slider_values(self):
        h = self.h_horizontalSlider.value()
        s = self.s_horizontalSlider.value()
        v = self.v_horizontalSlider.value()
        t = self.t_horizontalSlider.value()
        rgb = colorsys.hsv_to_rgb(float(h/255.0), float(s/255.0), float(v/255.0))
        trans = t/255.0

        return rgb, trans

    def start_editting_colors(self):
        # print 'start'
        if not self._suspend_selChange_scriptJob:
            self._suspend_selChange_scriptJob = True
            pm.undoInfo(openChunk=True)

    def current_color_changed(self):
        rgb, trans = self.get_color_slider_values()
        self.set_button_color(self.currentColor_pushButton, rgb, trans)

        current_button = self.colorButtonGrp.checkedButton()
        if current_button:
            self.set_button_color(current_button, rgb, trans)
            shader = self.color_buttons[current_button]
            # pm.select(shader, r=True)
            shader.attr(self.core.PREVIEW_COLOR_ATTR).set(rgb)
            shader.attr(self.core.PREVIEW_TRANS_ATTR).set((trans, trans, trans))

    def end_editting_colors(self):
        # print 'end'
        self.refresh_all_button_colors()
        self._suspend_selChange_scriptJob = False
        pm.undoInfo(closeChunk=True)

    def lock_preview_color_sliders(self, lock):
        enabled = not lock
        self.h_horizontalSlider.setEnabled(enabled)
        self.s_horizontalSlider.setEnabled(enabled)
        self.v_horizontalSlider.setEnabled(enabled)
        self.t_horizontalSlider.setEnabled(enabled)

    def block_preview_color_sliders(self, block):
        self.h_horizontalSlider.blockSignals(block)
        self.s_horizontalSlider.blockSignals(block)
        self.v_horizontalSlider.blockSignals(block)
        self.t_horizontalSlider.blockSignals(block)

    def color_button_clicked(self, button):
        if button == self.default_color_button:
            self.lock_preview_color_sliders(True)
        else:
            self.lock_preview_color_sliders(False)
        button.setChecked(True)

        # update color sliders
        shader = self.color_buttons[button]
        rgb = shader.attr(self.core.PREVIEW_COLOR_ATTR).get()
        trans = shader.attr(self.core.PREVIEW_TRANS_ATTR).get()
        hsv = colorsys.rgb_to_hsv(rgb[0], rgb[1], rgb[2])

        self.block_preview_color_sliders(True)
        # self.start_editting_colors()
        h = int(hsv[0]*255)
        s = int(hsv[1]*255)
        v = int(hsv[2]*255)
        t = int(trans[0]*255)
        self.h_horizontalSlider.setValue(h)
        self.s_horizontalSlider.setValue(s)
        self.v_horizontalSlider.setValue(v)
        self.t_horizontalSlider.setValue(t)

        self.h_spinBox.setValue(h)
        self.s_spinBox.setValue(s)
        self.v_spinBox.setValue(v)
        self.t_spinBox.setValue(t)

        self.block_preview_color_sliders(False)    

        # self.current_color_changed()
        # self.end_editting_colors()
        
    def color_button_double_clicked(self, button):
        sels = [s for s in pm.ls(sl=True) if isinstance(s, (pm.nt.Transform, pm.nt.Mesh, pm.MeshFace))]
        if not sels:
            return
        shader = self.color_buttons[button]
        pm.hyperShade(assign=shader)

    def convert_confirm_popup(self, mode, shaders):
        title = 'Convert to %s preview shader' %mode
        detailedText = ''
        buttons = []
        if not shaders:
            txt = 'No shader of type "%s" selected.' %self.core.MATERIAL
            icon = QMessageBox.Critical  
        else:
            txt = 'This will convert %s shader(s).\nFor shader list, click "Show Details..."' %(len(shaders))
            detailedText = '\n'.join([node.nodeName() for node in shaders])
            icon = QMessageBox.Information
            buttons = (('OK', QMessageBox.AcceptRole), 
                    ('Cancel', QMessageBox.RejectRole))
        confirmed = self.messageBox(text=txt, title=title, buttons=buttons, icon=icon, detailedText=detailedText)
        return confirmed

    def convert_finish_popup(self, shaders):
        title = 'Convert to preview shader'
        buttons = []
        detailedText = ''
        if shaders:
            txt = 'Some shader failed to convert.'
            detailedText = '\n'.join([node.nodeName() for node in shaders])
            icon = QMessageBox.Critical
        else:
            txt = 'Convert complete.'
            icon = QMessageBox.Information

        self.messageBox(text=txt, title=title, buttons=buttons, icon=icon, detailedText=detailedText)

    def convert_geo_sel_to_shader(self):
        sels = pm.selected(type='transform')
        shaders = set()
        geos = set()
        allow_shader_types = [self.core.MATERIAL] + list(self.core.MATERIALS)
        # print allow_shader_types
        for s in sels:
            shp = s.getShape(ni=True)
            if not shp:
                continue
            sgs = shp.outputs(type='shadingEngine')
            for sg in sgs:
                if sg.nodeName() == 'initialShadingGroup':
                    continue
                connect_shds = [node for node in sg.surfaceShader.inputs() if node.nodeType() in allow_shader_types]
                if connect_shds:
                    shaders.add(connect_shds[0])
                    geos.add(shp)
        return list(shaders), list(geos)

    def convertToFlat(self):
        shaders, geos = self.convert_geo_sel_to_shader()
        confirmed = self.convert_confirm_popup(mode='flat', shaders=shaders)
        if confirmed == QMessageBox.AcceptRole:
            colorManaged = True if self.cm_checkBox.isChecked() else False
            results = {}
            failed = []
            success = True
            self.statusbar.showMessage('Converting...')
            pm.undoInfo(openChunk=True)
            pm.waitCursor(st=True)
            try:
                for shader in shaders:
                    tmpShd = self.core.create_plain_preview(shader=shader, colorManaged=colorManaged)
                    if not tmpShd:
                        failed.append(shader)
                        om.MGlobal.displayError('Failed to convert: %s' %shader)
                        continue
                    results[shader] = tmpShd
            except Exception, e:
                om.MGlobal.displayError('Conversion failed:\n%s' %e)
                success = False

            finally:
                pm.waitCursor(st=False)
                pm.undoInfo(closeChunk=True)
                self.statusbar.showMessage('Ready.')

            self.refreshPreviewShaderButtons()
            if success:
                if self.autoAssign_checkBox.isChecked():
                    self.auto_assign(shaderDict=results, filters=geos)
                self.convert_finish_popup(shaders=failed)
                return results

    def convertToTextured(self):
        shaders, geos = self.convert_geo_sel_to_shader()
        confirmed = self.convert_confirm_popup(mode='textured', shaders=shaders)
        if confirmed == QMessageBox.AcceptRole:
            colorManaged = True if self.cm_checkBox.isChecked() else False
            tex_res = int(self.resolution_comboBox.currentText())
            results = {}
            failed = []
            success = True
            self.statusbar.showMessage('Converting...')
            pm.undoInfo(openChunk=True)
            pm.waitCursor(st=True)
            try:
                for shader in shaders:
                    tmpShd = self.core.create_textured_preview(shader=shader, 
                        previewTextureSize=tex_res, 
                        colorManaged=colorManaged)
                    if not tmpShd:
                        failed.append(shader)
                        om.MGlobal.displayError('Failed to convert: %s' %shader)
                        continue
                    results[shader] = tmpShd
            except Exception, e:
                om.MGlobal.displayError('Conversion failed:\n%s' %e)
                success = False
            finally:
                pm.waitCursor(st=False)
                pm.undoInfo(closeChunk=True)
                self.statusbar.showMessage('Ready.')

            self.refreshPreviewShaderButtons()
            if success:
                if self.autoAssign_checkBox.isChecked():
                    self.auto_assign(shaderDict=results, filters=geos)
                self.convert_finish_popup(shaders=failed)
                return results

    def messageBox(self, text, title, icon=QMessageBox.Information, buttons=[], detailedText=''):
        qmsgBox = QMessageBox(self)
        qmsgBox.setText(text)
        if detailedText:
            qmsgBox.setDetailedText(detailedText)
        qmsgBox.setWindowTitle(title)
        if icon:
            qmsgBox.setIcon(icon)
        for text, role in buttons:
            qmsgBox.addButton(text, role)
        results = qmsgBox.exec_()
        return results

    def auto_assign(self, shaderDict, filters=[]):
        pm.undoInfo(openChunk=True)
        for shader, tmpShd in shaderDict.iteritems():
            sg = shader.outputs(type='shadingEngine')[0]
            members = pm.sets(sg, q=True)
            if not members:
                continue

            toAssign = defaultdict(list)
            if '' in tmpShd:
                assignments = []
                for mem in members:
                    mesh = None
                    if isinstance(mem, pm.nt.Transform):
                        mesh = mem.getShape(ni=True)
                    elif isinstance(mem, pm.nt.Mesh):
                        mesh = mem
                    elif isinstance(mem, pm.MeshFace):
                        mesh = mem.node()

                    if filters and mesh in filters:
                        assignments.append(mem)

                toAssign[tmpShd['']] = [m.name() for m in assignments]
            else:
                assignments = defaultdict(list)  # {mesh: faces}
                for mem in members:
                    faces = None
                    mesh = None
                    if isinstance(mem, pm.nt.Transform):
                        mesh = mem.getShape(ni=True)
                        faces = mesh.f.indices()
                        
                    elif isinstance(mem, pm.nt.Mesh):
                        mesh = mem
                        faces = mesh.f.indices()
                        
                    elif isinstance(mem, pm.MeshFace):
                        faces = mem.indices()
                        mesh = mem.node()

                    if not faces:
                        continue

                    if filters and mesh in filters:
                        assignments[mesh].extend(faces)

                for mesh, faces in assignments.iteritems():
                    meshName = mesh.shortName()
                    msel = om.MSelectionList()
                    msel.add(meshName)
                    dMesh = om.MDagPath()
                    msel.getDagPath(0, dMesh)
                    meshFn = om.MFnMesh(dMesh)
                    uvSetName = meshFn.currentUVSetName()
                    meshIt = om.MItMeshPolygon(dMesh)
                    while not meshIt.isDone():
                        faceId = meshIt.index()
                        if faceId in faces:
                            us = om.MFloatArray()
                            vs = om.MFloatArray()
                            meshIt.getUVs(us, vs, uvSetName)
                            avg_u = int(float(sum(us)) / len(us))
                            avg_v = int(float(sum(vs)) / len(vs))
                            udimStr = str(1000+(avg_u+1)+(avg_v*10))
                            if udimStr in tmpShd:
                                pvShd = tmpShd[udimStr]
                                toAssign[pvShd].append('%s.f[%s]' %(meshName, faceId))
                        meshIt.next()

            for shd, faces in toAssign.iteritems():
                mc.select(faces, r=True)
                pm.hyperShade(assign=shd)
        pm.undoInfo(closeChunk=True)

    def loadRenderer(self):
        rendererStr = str(self.renderer_comboBox.currentText())
        core_module = RENDERERS[rendererStr]
        self.core = core_module(directory=DEFAULT_PATH)
        self.core.message.connect(self.show_message)
        self.previewMaterialChanged()

    def rendererChanged(self):
        self.loadRenderer()
        self.core.prefix = str(self.prefix_lineEdit.text())
        self.presetChanged()

    def show_message(self, msg):
        self.statusbar.showMessage(msg)

    def presetChanged(self):
        presetStr = str(self.preset_comboBox.currentText())
        self.core.preset = self.available_presets[presetStr]
        self.directoryChanged()

        # setup tool tip
        self.setup_preset_tooltip()

    def setup_preset_tooltip(self):
        help_naming = self.core.preset.HELP_NAMING
        help_example = self.core.preset.HELP_EXAMPLE
        mapTypes = ['    {}'.format(t) for t in self.core.preset.MAP_TYPES]

        toolTipLists = ['FORMAT: {}'.format(help_naming), 'EXAMPLE: {}'.format(help_example), 'MAP TYPES:'] + mapTypes
        toolTip = '\n'.join(toolTipLists)
        self.preset_comboBox.setToolTip(toolTip)
        
    def previewMaterialChanged(self):
        nodes = mc.ls(type=self.core.PREVIEW_MATERIAL)
        if not nodes:
            nodes = ['lambert1']
        exec('ktype = om.MFn.%s' %mc.nodeType(nodes[0], api=True))
        self.kPreviewNodeType = ktype

    # def help_naming(self):
    #     dial = QMessageBox()
    #     dial.setText('<mesh>_<map type>_<texture set>\n\nEXAMPLE:\nName_Diffuse_Body_1001\
    #             \n\nmesh\t=  Name\nmap type\t=  Diffuse\ntexture set\t=  Body_1001')
    #     dial.setWindowTitle("Help")
    #     dial.setIcon(QMessageBox.Question)
    #     dial.exec_()

    def itemSelected(self):
        for i in xrange(self.shd_treeWidget.topLevelItemCount()):
            item = self.shd_treeWidget.topLevelItem(i)
            if not item.isSelected():
                for c in xrange(item.childCount()):
                    child = item.child(c)
                    child.setFlags(child.flags() & ~Qt.ItemIsEnabled)
            else:
                for c in xrange(item.childCount()):
                    child = item.child(c)
                    child.setFlags(child.flags() | Qt.ItemIsEnabled)

    def browse(self):
        path = QFileDialog().getExistingDirectory(None, 
                'Look in...', 
                self.core.directory, 
                ~QFileDialog.ShowDirsOnly)
        if path:
            path = str(path).replace('\\', '/')
            self.core.directory = path
            self.dir_lineEdit.setText(path)
            self.populate()

    def directoryChanged(self):
        path = str(self.dir_lineEdit.text())
        if '\\' in path:
            self.dir_lineEdit.returnPressed.disconnect()
            path = path.replace('\\', '/')
            self.dir_lineEdit.setText(path)
            self.dir_lineEdit.returnPressed.connect(self.directoryChanged)
        self.core.directory = path
        self.listview.setRootIndex(self.fileModel.index(self.core.directory))
        self.populate()

    def prefixChanged(self):
        self.core.prefix = str(self.prefix_lineEdit.text())
        self.populate()

    def populate(self):
        self.statusbar.showMessage('Populating...')
        self.core.populate()
        self.shd_treeWidget.clear()

        default_checks = dict(zip(self.core.preset.MAP_TYPES, self.core.preset.DEFAULT_CHECK))
        checkbox_state_map = {True: Qt.CheckState.Checked, False: Qt.CheckState.Unchecked}
        for name, maps in self.core.data.iteritems():
            item = QTreeWidgetItem()
            item.setText(0, name)
            self.shd_treeWidget.addTopLevelItem(item)
            item.setSelected(True)
            item.setToolTip(0, '\n'.join(maps))
            # if name in old_states:
            item.setExpanded(True)

            for i, mapType in enumerate(maps):
                mapItem = QTreeWidgetItem()
                mapItem.setText(0, mapType)
                mapItem.setFlags(mapItem.flags() & ~Qt.ItemIsSelectable)
                mapItem.setCheckState(0, checkbox_state_map[default_checks[mapType]])
                files = [os.path.basename(f) for f in maps[mapType]]
                mapItem.setToolTip(0, '\n'.join(files))

                item.insertChild(i, mapItem)

        self.statusbar.showMessage('Ready.')

    def create(self):
        # warn user to turn off hypershade thumbnail update
        if pm.renderThumbnailUpdate(q=True) == 1:
            msg = 'Hypershade thumbnail render is currently ON.'
            msg += '\nOn heavy asset you might run out of memory.'
            msg += '\n\nTurn it OFF?'

            dial = QMessageBox()
            dial.setText(msg)
            dial.setWindowTitle("Warning!")
            dial.setIcon(QMessageBox.Warning)
            yesButton = dial.addButton('Yes', QMessageBox.YesRole)
            noButton = dial.addButton('No', QMessageBox.RejectRole)
            cancelButton = dial.addButton('Cancel', QMessageBox.RejectRole)
            dial.exec_()
            
            buttonClicked = dial.clickedButton()
            if buttonClicked == cancelButton:
                return
            elif buttonClicked == yesButton:
                pm.renderThumbnailUpdate(False)

        QApplication.setOverrideCursor(Qt.WaitCursor)
        data = defaultdict(dict)
        for i in xrange(self.shd_treeWidget.topLevelItemCount()):
            item = self.shd_treeWidget.topLevelItem(i)
            if item.isSelected():
                name = str(item.text(0))
                count = 0
                for c in xrange(item.childCount()):
                    child = item.child(c)
                    if child.checkState(0):
                        mapType = str(child.text(0))
                        data[name][mapType] = self.core.data[name][mapType]   # {name: {mapType:[file1, file2, ...], }}
                        count += 1
                if count == 0:
                    data[name] = {}
        if data:
            self.core.run(data)

        self.statusbar.showMessage('Finished')
        QApplication.restoreOverrideCursor()
          