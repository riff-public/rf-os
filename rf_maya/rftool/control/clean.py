# run when scene is open
import sys
import os
import maya.cmds as mc
import maya.mel as mm
import logging
import getpass
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rftool.utils import maya_utils
from rftool.utils import pipeline_utils
from rftool.shade import shade_utils
reload(shade_utils)

def scene_open():
	logger.info('On scene_open script running ...')
	logger.info('Cleaning plugin ...')
	maya_utils.remove_plugins()


def check_key():
	# checked project
	sn = mc.file(q=True, sn=True)
	project = [a for a in projects if a in sn]
	validProject = True if project else False

	if validProject:
		polyKey = maya_utils.check_poly_key()
		user = getpass.getuser()

		if polyKey:
			message = '%s has keyed on polygon "%s" - %s' % (user, polyKey, sn)
			pipeline_utils.write_log(project[0], message)
			mm.eval('warning "%s - Do not set keyframe on polygon";' % user)


def fix_shade(): 
	sn = mc.file(q=True, sn=True)
	# only fix if char or prop in sevenchieck 
	refPaths = mc.file(q=True, r=True)
	activeKeys = ['char', 'prop']

	for path in refPaths: 
		if any(a in path for a in activeKeys): 
			shade_utils.fix_unload_reference_shader(path)
			logger.debug('shade fixed %s' % path)

def check_wrong_project():
	from rf_utils.context import context_info
	# reload(context_info)
	sn = mc.file(q=True, loc=True)
	if sn == 'unknown' or not sn:
		return

	scene = context_info.ContextPathInfo(path=sn)
	proj = scene.project
	active_proj = os.environ['active_project']
	if proj and active_proj != proj:
		msg = 'Your current environment is: %s\nPlease re-launch Maya from the correct project settings.' %active_proj
		mc.confirmDialog(title='Warning', message=msg, icon='warning')
