# coding=utf-8
# run when scene is open
# This script clean vaccine.py and userSetup.py which are use to kill Chinese virus earlier this year. 
# But due to its behavior spreading itself accross the machine and files, we need to kill this vaccine too. 

import sys
import os
import maya.cmds as mc
import logging
import getpass
from PySide2 import QtWidgets
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import file_utils 

TARGETNODES = ['vaccine_gene', 'breed_gene']
USERSETUP_VACCINE_MD5 = '1eb2a4457125ab234dba2db309611be7'


def scene_open():
	logger.info('Detecting vaccine.py ...')
	infected = False 

	nodes, files, ids = detect()
	if nodes or files: 
		infected = True

	if infected: 
		dialog()
		clean_scriptJob(ids)
		clean_nodes(nodes)
		clean_files()
		nodes, files, ids = detect()

		if not nodes and not files and not ids: 
			save()
			logger.info('Clean process successful')
		else: 
			logger.error('Clean failed')
			QtWidgets.QMessageBox.warning(None, 'ล้มเหลว', 'กำจัดไวรัสไม่สำเร็จ กรุณาติดต่อ Pipeline. \nFailed to disinfect virus. Please contact Pipeline')

	else: 
		logger.info('No virus detected')


def detect(): 
	nodes = []
	files = []
	ids = []
	for node in TARGETNODES: 
		if mc.objExists(node): 
			nodes.append(node)

	for file, md5 in get_files().items(): 
		if os.path.exists(file): 
			if md5: 
				if file_utils.md5(file) == md5: 
					files.append(file)
			else: 
				files.append(file)

	key = 'leukocyte.antivirus()'

	for job in mc.scriptJob(lj=True): 
		if key in job: 
			id = job.split(':')[0]
			ids.append(int(id))

	return nodes, files, ids

def dialog(): 
	nodes, files, ids = detect()
	if nodes or files: 
		clean_nodes(nodes)
		clean_files()
		details = 'Nodes {}\n files {}'.format(', '.join(nodes), '\n'.join(files))
		title = 'ประกาศจากกระทรวงริฟไปป์ไลน์ (Riff Ministry of Pipeline)'
		message = 'เครื่องคุณได้ติดไวรัสชื่อวัคซีน เราจะทำการลบไฟล์ที่เกี่ยวข้อง \nYou are infected with a virus-like called vaccine. All infected file will be deleted {}'.format(details)

		QtWidgets.QMessageBox.warning(None, title, message)
		# result = mc.confirmDialog( title=title, message=message, button=['Continue'])
		return True 
	return False


def clean_scriptJob(ids): 
	key = 'leukocyte.antivirus()'
	for jobId in ids: 
		mc.scriptJob(kill=jobId, f=True)

	return True


def clean_nodes(nodes): 
	for node in nodes: 
		if mc.objExists(node): 
			mc.delete(node)
			logger.info('Delete "{}"'.format(node))
	
	
def get_files(): 
	file_dict = dict()
	usersetup_file = mc.internalVar(userAppDir=True) + '/scripts/userSetup.py'
	vaccine_file = mc.internalVar(userAppDir=True) + '/scripts/vaccine.py'
	vaccine_filew = mc.internalVar(userAppDir=True) + '/scripts/vaccine.pyc'

	file_dict[usersetup_file] = '1eb2a4457125ab234dba2db309611be7'
	file_dict[vaccine_file] = ''
	file_dict[vaccine_filew] = ''

	return file_dict

def save(): 
	current = mc.file(q=True, loc=True)
	mc.file(rename=current)
	mc.file(save=True, f=True)
	logger.info('Clean file saved')

def clean_files(): 
	file_dict = get_files()

	for file, md5 in file_dict.items(): 
		if os.path.exists(file): 
			if md5: 
				if file_utils.md5(file) == md5: 
					os.remove(file)
					logger.info('Match target content. Delete "{}"'.format(file))
			else: 
				os.remove(file)
				logger.info('Delete "{}"'.format(file))
