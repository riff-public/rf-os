# standalone 
UINAME = 'Crowd Utilities'
import os 
import sys 
import logging 
from PySide2 import QtWidgets 
from PySide2 import QtGui 
from PySide2 import QtCore 
from shiboken2 import wrapInstance
import maya.cmds as mc 
import maya.OpenMayaUI as mui
import pymel.core as pm 
from . import ui
reload(ui)


LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(logging.NullHandler())


class App(QtWidgets.QMainWindow):
    """docstring for App"""
    def __init__(self, parent=None):
        super(App, self).__init__(parent)
        self.ui = ui.Ui(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(UINAME)
        


def show(): 
    delete_ui(UINAME)
    app = App(get_maya_window())
    app.show()
    return app 


def get_maya_window():
    """ Maya Qt pointer """
    ptr = mui.MQtUtil.mainWindow()
    return wrapInstance(long(ptr), QtWidgets.QWidget)


def delete_ui(ui): 
    if mc.window(ui, exists=True): 
        mc.deleteUI(ui)    
