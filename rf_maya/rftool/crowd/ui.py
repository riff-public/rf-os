# standalone 
import os 
import sys 
import logging 
from PySide2 import QtWidgets 
from PySide2 import QtGui 
from PySide2 import QtCore 


LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(logging.NullHandler())


class Ui(QtWidgets.QWidget): 
    def __init__(self, parent=None): 
        super(Ui, self).__init__(parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.button = QtWidgets.QPushButton()
        self.layout.addWidget(self.button)
        self.setLayout(self.layout) 

