import os 
import sys
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import maya.mel as mm 
from rf_maya.lib import sequencer_lib
from rf_app.publish.scene.utils import maya_hook as hook
sgDuration = 0

def run(entity=None): 
	""" Check if duration match Shotgun """ 
	if not entity.name_code == 'all': 
		check(entity)
		check_startend_frame(entity)


def check(entity=None): 
	""" Adjust duration to match Shotgun """ 

	sgDuration = get_duration(entity)
	if sgDuration: 
		duration = get_scene_duration(entity)
		if duration: 
			confirmMessage = 'Duration not match. Shotgun (%s) / Maya (%s)' % (sgDuration, duration)

			if sgDuration == 0: 
				prompt('No shotgun duration found. Contact Mim')
				return 

			if not sgDuration == duration: 
				if sgDuration > duration: 
					# extend tail 
					value = sgDuration - duration
					result = confirm_action('%s\nExtend sequencer "%s" frame?' % (confirmMessage, value))
					
					if result: 
						sequencer_lib.extend_shot(entity.name_code, value, tail=True)

				if sgDuration < duration: 
					# trim tail
					value = duration - sgDuration
					result = confirm_action('%s\nTrim sequencer "%s" frame?' % (confirmMessage, value))

					if result: 
						sequencer_lib.trim_shot(entity.name_code, value, tail=True)

		logger.info('Duration matched "%s"' % sgDuration)
		return True


def get_duration(entity): 
	from rf_utils.sg import sg_utils 
	sg = sg_utils.sg
	filters = [['project.Project.name', 'is', entity.project], ['code', 'is', entity.name]]
	fields = ['sg_working_duration', 'sg_include_prepost_roll']
	result = sg.find_one('Shot', filters, fields)
	global sgDuration 
	sgDuration = result.get('sg_working_duration') if result else 0
	durationIncludePrePostRollOverrride = result.get('sg_include_prepost_roll')

	# check config 
	durationIncludePrePostRoll = entity.projectInfo.scene.include_prepost_sequencer
	preRoll = entity.projectInfo.scene.pre_roll
	postRoll = entity.projectInfo.scene.post_roll

	if durationIncludePrePostRoll: 
		sgDuration = sgDuration + preRoll + postRoll

	else: 
		if durationIncludePrePostRollOverrride: 
			logger.debug('Shot pre/post roll duration override found')
			logger.debug('New duration {} + {} + {} = {}'.format(sgDuration, preRoll, postRoll, (sgDuration + preRoll + postRoll)))
			sgDuration = sgDuration + preRoll + postRoll


	return sgDuration

def check_startend_frame(entity):
	FIXED = 1
	DEFAULT_FRAME = 1000
	DEFAULT_START_FRAME = 1000 + FIXED
	DEFAULT_DURATION_SHOT = sgDuration
	DEFAULT_END_FRAME = DEFAULT_FRAME + sgDuration

	shotName = entity.name_code
	startFrame, endFrame, seqStartFrame, seqEndFrame = sequencer_lib.shot_info(shotName)
	scale = mc.getAttr('%s.scale'% shotName)
	shots = hook.list_shot()
	
	if shots:
		if shotName in shots:
			if DEFAULT_START_FRAME != startFrame:
				mc.setAttr('%s.startFrame'% shotName, DEFAULT_START_FRAME)
				mc.playbackOptions( minTime=DEFAULT_START_FRAME, animationStartTime=DEFAULT_START_FRAME)

			if DEFAULT_END_FRAME != endFrame:
				mc.setAttr('%s.endFrame'% shotName, DEFAULT_END_FRAME)
				mc.playbackOptions( maxTime=DEFAULT_END_FRAME, animationEndTime=DEFAULT_END_FRAME)

			if DEFAULT_START_FRAME != seqStartFrame:
				mc.setAttr('%s.sequenceStartFrame'% shotName, DEFAULT_START_FRAME)

			if scale != FIXED:
				mc.setAttr('%s.scale'% shotName, FIXED)


def get_scene_duration(entity): 
	duration = 0
	durationIncludePrePostRoll = entity.projectInfo.scene.include_prepost_sequencer
	shotName = entity.name_code

	if mc.objExists(shotName): 
		if mc.objectType(shotName, isType='shot'): 
			startFrame, endFrame, seqStartFrame, seqEndFrame = sequencer_lib.shot_info(shotName)
			duration = endFrame - startFrame + 1 

	return duration


def confirm_action(message): 
	result = mc.confirmDialog( title='Confirm', message=message, button=['Yes'])
	return True if result == 'Yes' else False

def prompt(message): 
	return mc.confirmDialog( title='Warning', message=message, button=['OK'])


