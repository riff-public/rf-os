import os 
import sys
import logging 
from datetime import datetime
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import maya.mel as mm 

from rftool.utils import maya_utils
reload(maya_utils)

def run(entity=None): 
	""" Check if duration match Shotgun """ 
	start = datetime.now()
	geoGrps = list_assets(entity)
	maya_utils.fix_shade(geoGrps)


def list_assets(entity): 
	geoGrp = entity.projectInfo.asset.geo_grp() or 'Geo_Grp'
	refs = mc.file(q=True, r=True)
	geoGrps = []

	for ref in refs: 
		ns = mc.referenceQuery(ref, ns=True)
		targetGrp = '%s:%s' % (ns[1:], geoGrp)

		if mc.objExists(targetGrp): 
			geoGrps.append(targetGrp)

	return geoGrps 
