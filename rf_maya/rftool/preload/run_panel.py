import os 
import sys
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import maya.mel as mm 


def run(entity=None): 
    """ Check if duration match Shotgun """ 
    from rf_app.info_panel import maya_dock
    app = maya_dock.show(restore=True)

