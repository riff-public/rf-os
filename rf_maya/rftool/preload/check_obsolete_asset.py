#!/usr/bin/env python
# -- coding: utf-8 --
import os 
import sys
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import maya.mel as mm 
from rf_utils import register_shot 
from rf_app.scene.builder.model import scene_builder_model
from rf_utils.pipeline import cache_list
from rf_utils.widget import dialog


def run(entity=None): 
	""" Check if duration match Shotgun """ 
	removeAssets = check(entity) 

	if removeAssets: 
		message = 'มี Asset ไม่ใช้แล้ว {} assets. กรุณาเอาออก เช็คใน Builder'.format(len(removeAssets))
		dialog.MessageBox.warning('Invalid assets', message)
		run_builder()
		logger.warning(message)

	return True if not removeAssets else False


def check(entity): 
	reg = register_shot.Register(entity)
	registerAssets = reg.get.asset_list(type='abc_cache')

	cache = cache_list.CacheList(entity)
	validAssets = cache.list_keys_status(status=True)

	inValidAssets = [a for a in registerAssets if not a in validAssets]
	removeAssets = []
	
	for asset in inValidAssets: 
		result = scene_builder_model.check_status(asset, reg, '')
		if result['status'] == 'match': 
			removeAssets.append(asset)

	return removeAssets


def run_builder(): 
	from rf_app.scene.builder import app
	app.show()
