import os 
import sys
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import maya.mel as mm 
import pymel.core as pm
from rf_maya.lib import sequencer_lib
from rf_utils.context import context_info
from rf_utils import register_shot
from rf_qc.maya_lib.scene import check_cam
from rf_app.publish.scene.utils import maya_hook
reload(maya_hook)

LOCALSERVER = True 
if os.environ.get('RFSTUDIO') == 'OS': 
    LOCALSERVER = False

def run(entity=None): 
    """ Check if duration match Shotgun """ 
    if entity.step == 'anim': 
        if entity.process == 'editCam' or not LOCALSERVER: # exception for animation process to edit cam
            print 'Skip checking camera for "editCam"'
            return 
        check(entity)


def check(entity=None): 
    """ Lock passive camera to prevent accidental edits. """ 
    path_global_cam = entity.projectInfo.asset.global_asset(key='cam')
    shotName = entity.name_code
    reg= register_shot.Register(entity)
    if reg.get.camera(): 
        cam = reg.get.camera()[0]
        shotCam_path = reg.get.cache(key=cam, hero=True)
        logger.info('Camera cache path: {0}'.format(shotCam_path))

        currentCam = mc.listConnections('%s.currentCamera' %shotName)
        if not currentCam: 
            check_cam.resolve(entity)
            currentCam = mc.listConnections('%s.currentCamera' %shotName)

        cam_shot = currentCam[0]
        # if camera is found and camera is a referenced node
        if cam_shot and mc.referenceQuery(cam_shot, inr=True): 
            cam_path = mc.referenceQuery( cam_shot, filename=True) 
            logger.info('Current camera ref path: %s' %(cam_path))

            # if the camera ref path is using anything other than cached abc cam (ie. riged .ma cam)
            if cam_path and cam_path.split('{')[0] != shotCam_path:
                if os.path.exists(shotCam_path):
                    maya_hook.switch_reference(obj=cam_shot, new_path=shotCam_path)
                    logger.info('Camera switched to passive: %s' %(shotCam_path)) 

                    # fix missing connection from cam to the shot node
                    check_cam.resolve(entity)
                    # group the cached cam under Cam_Grp
                    maya_hook.group_cam(shotName)
                    # set default display
                    maya_hook.set_default_display(shotName)
                    logger.info('Camera connections and display has been cleaned up') 
                        
                    # lock .abc cam
                    ref_node = mc.referenceQuery(cam_shot, referenceNode=True)
                    maya_hook.maya_utils.lock_ref_cam(rnNode=ref_node, state=1)
                    logger.info('Camera transform locked.') 

    return True


def confirm_action(message): 
    result = mc.confirmDialog( title='Confirm', message=message, button=['Yes'])
    return True if result == 'Yes' else False

def prompt(message): 
    return mc.confirmDialog( title='Warning', message=message, button=['OK'])


