import os 
import sys
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import maya.mel as mm

def run(entity=None): 
	""" Check color management """
	if not mc.colorManagementPrefs(q=True , cme=True):
		check(entity)

def check(entity=None):
	""" Adjust Turn on color management """ 
	mc.colorManagementPrefs(e=True , cme=True, outputTransformEnabled=True)


	