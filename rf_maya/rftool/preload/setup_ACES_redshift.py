import os 
import sys
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import maya.mel as mm 

from rf_maya.environment import setup_config


def run(entity=None): 
    """ setup ACES colorspace and view transform of both Maya and Redshift Render View """ 
    logger.info('Setting up ACES colorspace and view transform...')
    setup_config.set_ocio_config(value=True)
    setup_config.set_viewtransform(value='Rec.709 (ACES)')
    setup_config.set_redshift_rv_viewtransform(value='Output - Rec.709')

