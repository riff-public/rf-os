import os 
import sys
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import maya.mel as mm 
import pymel.mayautils as mu
from rf_app.asm import asm_lib
from rf_utils import register_shot
from rf_utils.context import context_info
from rf_utils.widget import dialog
reload(asm_lib)
sgDuration = 0

def run(entity=None): 
    """ Check if duration match Shotgun """ 
    if entity.step in ['layout', 'anim', 'finalcam', '']: 
        check(entity)

    if entity.step in ['light']: 
        logger.debug('Check for update lighting department')
        mu.executeDeferred(check_update, entity)

    else: 
        logger.warning('Skip update set "%s"' % entity.step)


def check(entity=None): 
    """ Adjust duration to match Shotgun """ 
    logger.info('Checking  set ...')
    mu.executeDeferred(update, entity)


def check_gpu_md():
    locators = asm_lib.list_asms()
    if locators: 
        for loc in locators:
            asm_node = asm_lib.MayaRepresentation(loc)
            
            if not asm_node.is_root():
                gpu_path = asm_node.gpu_path
                if gpu_path: 
                    asset = context_info.ContextPathInfo(path= gpu_path)
                    basename = os.path.basename(gpu_path)
                    if '_pr.' in basename:
                        mc.select(loc,add=True)

        asm_lib.UiCmd().switch_gpu('md')


def update(entity=None): 
    logger.info('Updating set (executeDeferred) ...')
    asm_lib.update(build=True, force=False)
    update_with_override(entity)
    check_gpu_md()
    logger.info('Update set completed')

 
def check_update(entity=None): 
    setCheckResult = asm_lib.update(build=True, force=False, run=False)
    overrideCheckResult = update_with_override(entity, run=False)
    batch = mc.about(batch=True)
    
    if any(setCheckResult + overrideCheckResult): 
        logger.debug('Update found')
        yes = 'Update'
        no = 'No'
        if not batch: 
            result = dialog.CustomMessageBox.show('Warning Set update', 'Set has been updated. \nUpdate Now?', [yes, no])
            if result.value == yes: 
                if any(setCheckResult): 
                    asm_lib.update(build=True, force=False)
                    logger.info('Update Set complete')
                if any(overrideCheckResult): 
                    update_with_override(entity)
                    logger.info('Update override complete')
            else: 
                logger.warning('User choose not to update set')
    else: 
        logger.info('Nothing to Update')



def update_with_override(entity=None, run=True): 
    setGrp = entity.projectInfo.asset.set_grp() or 'Set_Grp'
    results = []
    if mc.objExists(setGrp): 
        roots = asm_lib.find_set_root()
        entity = context_info.ContextPathInfo()
        reg = register_shot.Register(entity)

        for root in roots: 
            loc = asm_lib.MayaRepresentation(root)
            overrideData = reg.get.asset(loc.shortname)
            if overrideData: 
                setOverride = overrideData.get(register_shot.Config.setOverride).get(register_shot.Config.heroFile)

                if setOverride: 
                    logger.info('Apply set override ...')
                    logger.debug(setOverride)
                    result = asm_lib.import_override(root, setOverride, build=True, force=False, run=run)
                    results.append(result)

    else: 
        logger.info('Not set to update')

    return results

