import os 
import sys
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import maya.mel as mm 
from rf_maya.lib import sequencer_lib
sgDuration = 0

PRE_BLACKLIST = ('pgYetiVRayPreRender')
POST_BLACKLIST = ('pgYetiVRayPostRender')

def run(entity=None): 
	""" Remove pre/pose MEL """ 
	if mc.objExists('defaultRenderGlobals'):
		curr_premel = mc.getAttr('defaultRenderGlobals.preMel')
		if curr_premel and curr_premel in PRE_BLACKLIST:
			mc.setAttr('defaultRenderGlobals.preMel', '', type='string')

		curr_postmel = mc.getAttr('defaultRenderGlobals.postMel')
		if curr_postmel and curr_postmel in POST_BLACKLIST:
			mc.setAttr('defaultRenderGlobals.postMel', '', type='string')

