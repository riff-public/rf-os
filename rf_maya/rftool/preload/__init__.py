# import all modules in maya_lib 
import os
import sys
import logging 
import glob
from rf_utils import file_utils
from collections import OrderedDict
from datetime import datetime

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

# set version
from maya.cmds import about
maya_version = about(version=True)

def get_module(): 
    modules = glob.glob(os.path.dirname(__file__)+"/*.py")
    return [ os.path.basename(f)[:-3] for f in modules if os.path.isfile(f) and not f.endswith('__init__.py')]

def get_module_data(filters=[]): 
    mods = OrderedDict()
    allModules = get_module()
    allModules = [a for a in allModules if a in filters] if filters else allModules
    # print allModules
    # print filters
    
    for name in allModules: 
        func = __import__(name, globals(), locals(), [], -1)
        reload(func)
        mods[name] = func

    return mods

def preload_module(project, entityType): 
	configFile = '{}/config.yml'.format(moduleDir)
	configData = file_utils.ymlLoader(configFile)
	moduleList = configData.get('Default').get(entityType)

	if project in configData.keys(): 
		moduleList = configData.get(project).get(entityType)

	return moduleList


def project_preload_module(project, entityType, step): 
	defaultConfig = config_path('default')
	defaultData = file_utils.ymlLoader(defaultConfig)

	projectConfig = config_path(project)
	projectData = OrderedDict()

	if os.path.exists(projectConfig): 
		projectData = file_utils.ymlLoader(projectConfig)

	defaultStepModules = defaultData.get(entityType).get('default')
	stepModules = defaultData.get(entityType, dict()).get(step, [])

	defaultModules = stepModules if stepModules else defaultStepModules

	if projectData: 
		projectDefaultStepModules = projectData.get(entityType).get('default')
		projectStepModules = projectData.get(entityType, dict()).get(step, [])
		projectDefaultModules = projectStepModules if projectStepModules else projectDefaultStepModules
		
		if projectDefaultModules: 
			return projectDefaultModules

	return defaultModules


def config_path(project): 
	scriptRoot = os.environ['RFSCRIPT']
	configDir = '{}/core/rf_env/{}/maya/{}/config'.format(scriptRoot, project, maya_version)
	configFile = 'preload_config.yml'
	configPath = '{}/{}'.format(configDir, configFile)
	return configPath


def check(entity): 
	# filters = preload_module(entity.project, entity.entity_type)
	filters = project_preload_module(entity.project, entity.entity_type, entity.step)
	data = get_module_data(filters)
	start = datetime.now()
	if data: 
		logger.info('Checking ...')
		
		for key, func in data.iteritems(): 
			logger.info(func.__name__)
			try: 
				func.run(entity)
				duration('Finished in', start)
			except Exception as e: 
				logger.warning('Preload Failed: {}'.format(func.__name__))
				logger.error(e)

		logger.info('Check complete')

def duration(message, start): 
	mesg = '{} {}'.format(message, datetime.now() - start)
	# print mesg 
	logger.info(mesg)

# from rf_utils.context import context_info 
# scene = context_info.ContextPathInfo()
# scene.entity_type
# preload.run(scene)