#!/usr/bin/env python
# -- coding: utf-8 --

import os 
import sys
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import maya.mel as mm 
import pymel.mayautils as mu
from rf_utils.context import context_info
from rf_utils.pipeline import lockdown
reload(lockdown)
from rf_utils.widget import dialog
from datetime import datetime


def run(entity=None): 
    """ Check if duration match Shotgun """ 
    if entity.step in ['anim']: 
        result = check(entity)
        
        if not result: 
            write_log(entity)
            logger.warning('This shot is lockdown!!')
            message_box()


def check(entity=None): 
    """ Adjust duration to match Shotgun """ 
    result = lockdown.check_status(entity, entity.step, True)
    return True if not result else False


def message_box(): 
    dialog.MessageBox.warning('WARNING', 'Shot นี้ถูกส่งไปเรนเดอร์แล้ว ห้ามแก้ไขอนิเมชันเด็ดขาด')
    

def write_log(entity): 
    date = str(datetime.now()).split(' ')[0]
    logName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]
    logPath = '%s/%s/_data/%s/%s/%s/%s.txt' % (os.environ['RFPROJECT'], entity.project, logName, date, entity.name, logName)
    data = '%s: %s was trying to open animation scene %s\n' % (str(datetime.now()), os.environ.get('RFUSER'), entity.name)

    if not os.path.exists(os.path.dirname(logPath)): 
        os.makedirs(os.path.dirname(logPath))
    writeFile(logPath, data)


def writeFile(file, data) :
    mode = 'a' if os.path.exists(file) else 'w'
    f = open(file, mode)
    f.write(data)
    f.close()
    return True
