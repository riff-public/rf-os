import os 
import sys
import logging 
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import maya.cmds as mc 
import maya.mel as mm 
import pymel.mayautils as mu
from rf_app.asm import asm_lib
from rf_utils import register_shot
from rf_utils.context import context_info
reload(asm_lib)
from rf_maya.environment import setup_config
sgDuration = 0

def run(entity=None): 
    """ Check frame rate """ 
    fps = entity.projectInfo.render.fps()
    check_frame_rate(str(fps), entity.project)

    linear_unit = entity.projectInfo.render.linear()
    check_linear(str(linear_unit), entity.project)

    angular_unit = entity.projectInfo.render.angular()
    check_angular(str(angular_unit), entity.project)

    # setup_config.read_config_and_set(version=mc.about(v=True), project=entity.project)

def check(entity=None): 
    """ Adjust duration to match Shotgun """ 
    pass 

def check_linear(linear, project=''): 
    currentLinear = mc.currentUnit(q = True, linear = True)
    currentFile = mc.file(q = True, location = True).split('/')[-1]
    if not currentLinear == linear : 
        result = mc.confirmDialog(  title='Warning', 
                                message='Linear working unit not matched! \nCurrent file \"%s\" is using %s. \n%s project setting is %s.' %(currentFile, currentLinear, project, linear), 
                                button=['Change Now', 'Skip'], 
                                defaultButton='Change Now' 
                                )

        if result == 'Change Now' : 
            mc.currentUnit(linear = linear)
            mm.eval('print "set linear working unit @ %s\\n";' % linear)

        if result == 'Skip' : 
            mm.eval('warning "You choose ignore wrong linear unit setting!! Please change as soon as possible.\\n";')

    else : 
        mm.eval('print "linear unit matched @ %s\\n";' % linear)


def check_angular(angular, project=''):
    currentAngle = mc.currentUnit(q = True, angle = True)
    currentFile = mc.file(q = True, location = True).split('/')[-1]
    if not currentAngle == angular : 
        result = mc.confirmDialog(  title='Warning', 
                                message='Angular working unit not matched! \nCurrent file \"%s\" is using %s. \n%s project setting is %s.' %(currentFile, currentAngle, project, angular), 
                                button=['Change Now', 'Skip'], 
                                defaultButton='Change Now' 
                                )

        if result == 'Change Now' : 
            mc.currentUnit(angle = angular)
            mm.eval('print "set angular working unit @ %s\\n";' % angular)

        if result == 'Skip' : 
            mm.eval('warning "You choose ignore wrong angular unit setting!! Please change as soon as possible.\\n";')

    else : 
        mm.eval('print "angular unit matched @ %s\\n";' % angular)

def check_frame_rate(fps, project='') : 
    # fps = str(self.ui.fps_label.text())
    time = {'15': 'game', 
            '24': 'film',
            '25': 'pal',
            '30': 'ntsc',
            '48': 'show',
            '50': 'palf',
            '60': 'ntscf' }

    mapFps = {'game': '15', 
            'film': '24',
            'pal': '25',
            'ntsc': '30',
            'show': '48',
            'palf': '50',
            'ntscf': '60'}

    # game: 15 fps
    # film: 24 fps
    # pal: 25 fps
    # ntsc: 30 fps
    # show: 48 fps
    # palf: 50 fps
    # ntscf: 60 fps

    if not fps == 'None' : 
        currentFps = mc.currentUnit(q = True, time = True)
        currentFile = mc.file(q = True, location = True).split('/')[-1]
        if not currentFps == time[fps] : 
            result = mc.confirmDialog(  title='Warning', 
                                    message='Frame Rate not matched! \nCurrent file \"%s\" is using %s fps. \n%s project uses %s fps.' % (currentFile, currentFile, project, fps), 
                                    button=['Change Now', 'Skip'], 
                                    defaultButton='Change Now' 
                                    )

            if result == 'Change Now' : 
                mc.currentUnit(time = time[fps])
                mm.eval('print "set frame rate @ %s\\n";' % fps)

            if result == 'Skip' : 
                mm.eval('warning "You choose ignore wrong frame rate setting!! Please change as soon as possible.\\n";')

        else : 
            mm.eval('print "frame rate matched @ %s\\n";' % fps)
    else : 
        mm.eval('warning "Project frame rate not found\\n";')