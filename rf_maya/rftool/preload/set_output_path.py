from rf_utils.sg import sg_process
from rf_utils.context import context_info

import maya.cmds as mc
import yaml
import os

# disable logging #
import logging
logger = logging.getLogger()
logger.setLevel('INFO')

def run(*args):
    scriptRoot = os.environ["RFSCRIPT"] + "/core"

    scene = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))

    project = scene.project
    name_code = scene.sg_name
    assetType = scene.type
    assetName = scene.name
    colorSpace = "_acescg" if project == "Bikey" else ""
    step = scene.step

    configPath = "{}/rf_config/project/{}/render_config.yml".format(scriptRoot, project)

    if step == "lookdev":
        prefix = "{}/{}/pass/<Camera>/v001/<RenderLayer>/<RenderLayer>{}".format(assetType, assetName, colorSpace)
        workspaceImagePath = "P:/{}/rnd/lookdev/image".format(project)

        w, h = 2048, 2048
        cut_in, cut_out = 1, 26
        tmin, tmax = cut_in, cut_out
        deviceAspect = 1
        pixelAspect = 1
    else:
        extraFields = ['sg_cut_in', 'sg_cut_out', 'sg_cut_duration', 'sg_working_duration']
        shot_entity = sg_process.get_shot_entity(scene.project, name_code, extraFields= extraFields)

        with open(configPath, 'r') as stream:
            renderConfig = yaml.safe_load(stream)

        w, h = renderConfig["rawOutput"]
        deviceAspect = renderConfig["deviceAspect"]
        pixelAspect = renderConfig["pixelAspect"]
        startFrame = int(renderConfig["startFrame"])
        cut_in = shot_entity["sg_cut_in"] if shot_entity["sg_cut_in"] else 1001
        cut_out = shot_entity["sg_cut_out"] if shot_entity["sg_cut_out"] else 1001
        cut_in += startFrame -1
        cut_out += startFrame -1

        prefix = "<RenderLayer>/<Version>/<RenderLayer>"
        workspaceImagePath = mc.file(q=True, loc=True).replace("work", "publ").split("light")[0] + "render/output"

    mc.setAttr("defaultRenderGlobals.imageFilePrefix", prefix, type="string")
    mc.workspace(fileRule=['images', workspaceImagePath])

    mc.setAttr("redshiftOptions.imageFormat", 1)
    mc.setAttr("defaultRenderGlobals.animation", 1)
    mc.setAttr("defaultRenderGlobals.startFrame", cut_in)
    mc.setAttr("defaultRenderGlobals.endFrame", cut_out)
    mc.setAttr("defaultResolution.w", w)
    mc.setAttr("defaultResolution.h", h)
    mc.setAttr("defaultResolution.dar", deviceAspect)
    mc.setAttr("defaultResolution.pa", pixelAspect)
    mc.playbackOptions(ast=cut_in)
    mc.playbackOptions(aet=cut_out)
    mc.playbackOptions(min=cut_in)
    mc.playbackOptions(max=cut_out)

    # set global rs template path #
    template_path = "P:/{}/rnd/light/Light_Template/layers_and_collections".format(project)
    mc.optionVar(sv=("renderSetup_globalTemplateDirectory", template_path))
