#!/usr/bin/env python
# -- coding: utf-8 --

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
import rf_config as config

import os
import sys
import maya.cmds as mc

from collections import OrderedDict
from rf_utils.context import context_info
from rf_qc import qc_widget
from rftool.utils.ui import maya_win
from rf_app.asm import asm_lib

#uiName = 'CleaningSceneUi'
uiName = 'SubmitQueueUi'

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]
mainPath = '%s/core' % os.environ['RFSCRIPT']

class config_qc:
    qcList = ['check_layer', 'check_animlayer', 'check_non_reference',
                'remove_namespace', 'check_unmatch_namespace', 'check_pipeline_asset', 'check_asset',
                'check_on2_node',
                'check_cam', 'check_duration', 'check_time_scale',
                'check_bake_dynFk', 'check_no_geometry',
                'check_pre_roll_sim',
                'add_asset_tag', 'add_mattepaint_tag']

    text_message = {
                'check_layer' : """ check_layer \n   - มี layer ที่ยังไม่ได้ลบออก \n   - ถ้ามี layer FKhairDynFk_DLyr จะไม่สามารถลบได้ ให้ผ่าน QC check_bake_dynFk ก่อน \n\n""",
                'check_animlayer' : """ check_animlayer \n   - มี anim layer มากกว่า 1 layer \n   - มี anim layer ที่ยังไม่ได้ merge \n\n""",
                'check_on2_node' : """ check_on2_node \n   - on2 node ชื่อไม่ตรงกับ asset \n   - กด reslove เพื่อเปลี่ยนชื่อให้ตรงกัน \n\n""",
                'check_non_reference': 
                            """ check_non_reference \n   - มี object ที่ไม่ใช่ reference อยู่ใน scene ,ให้จัดใส่ group ให้ถูกต้องหรือลบทิ้ง"""+
                            """ \n   - Export_Grp จะ export ของที่อยู่ในกรุ๊ปนี้ออกไปให้ lighting"""+
                            """ \n   - Guide_Grp ของที่อยู่ในกรุ๊ปจะไม่ถูก export ออกไป\n\n""",
                'remove_namespace' : """ remove_namespace \n   - มี namespace ของ object ที่ไม่ใช่ reference"""+
                            """ \n   - กด reslove เพื่อลบ namespace ออก \n\n""",
                'check_unmatch_namespace' : """ check_unmatch_namespace \n   - มี namespace ที่ไม่ตรงกับ asset \n\n""",
                'check_pipeline_asset' : """ check_pipeline_asset \n   - มี file reference ที่ไม่ใช่ asset ในระบบ \n\n""",
                'check_asset' : """ check_asset \n   - มี rig ที่ไม่ใช่ md อยู่ใน scene"""+
                            """ \n   - switch asset เป็น rig md"""+
                            """ \n   - กรณีที่ยังไม่มี rig md ในระบบ จะไม่สามารถ switch ได้ ,ติดต่อ manager แผนก \n\n""",
                'check_cam' : """ check_cam \n   - เช็ค camera sequencer (ชื่อ shot, จำนวนเฟรม, กล้อง) \n\n""",
                'check_duration' : """ check_duration \n   - จำนวนเฟรมใน camera sequencer ไม่ตรง \n\n""",
                'check_time_scale' : """ check_time_scale \n   - sequencer end frame ใน camera sequencer ไม่ตรง \n\n""",
                'check_bake_dynFk' : """ check_bake_dynFk \n   - ยังไม่ได้ bake dynamic fk \n   - เปิด tool dynamic fk เพื่อ bake dynamic fk\n\n""",
                'check_no_geometry' : """ check_no_geometry \n   - มี shape ใน asset หาย กด resolve เพื่อซ่อม \n\n""",
                'check_pre_roll_sim' : """ check_pre_roll_sim \n   - ชอทนี้มีแผนก sim ทำงานด้วย"""+
                            """ \n   - กด reslove รัน tool เพื่อสร้าง preroll\n\n""",
                'add_asset_tag' : """ add_asset_tag \n   - ต้องผ่าน QC check_asset \n\n""",
                'add_mattepaint_tag' : """ add_mattepaint_tag \n\n"""}

class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)

        self.layout = QtWidgets.QHBoxLayout()
        self.qclayout = QtWidgets.QVBoxLayout()
        self.textWidget = QtWidgets.QTextEdit()
        self.textWidget.setReadOnly(True)
        self.textWidget.setFontPointSize(10)
        self.qcWidget = qc_widget.QcWidget()
        self.qcWidget.checkButton.setText("Check ALL")
        self.qclayout.addWidget(self.qcWidget)

        self.submitlayout = QtWidgets.QHBoxLayout()
        self.submitButton = QtWidgets.QPushButton()
        self.submitButton.setText("Submit")
        self.submitButton.setEnabled(False)
        self.submitButton.setMinimumHeight(30)

        self.queueButton = QtWidgets.QPushButton()
        self.queueButton.setText("Queue")
        self.queueButton.setEnabled(False)
        self.queueButton.setVisible(False)
        self.queueButton.setMinimumHeight(30)

        self.submitlayout.addWidget(self.submitButton)
        self.submitlayout.addWidget(self.queueButton)

        self.submitlayout.setStretch(0, 2)
        self.submitlayout.setStretch(1, 0)

        self.qclayout.addLayout(self.submitlayout)
        self.layout.addLayout(self.qclayout)
        self.layout.addWidget(self.textWidget)
        self.layout.setStretch(0, 0)
        self.layout.setStretch(1, 2)

        self.setLayout(self.layout)

class CleaningFile(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #Setup Window
        super(CleaningFile, self).__init__(parent)
        self.ui = Ui(parent)
        self.setCentralWidget(self.ui)
        self.setObjectName(uiName)
        self.setWindowTitle(uiName)
        self.setWindowIcon(QtGui.QIcon("%s/icons/shelves/cleaning.png" %(mainPath)))
        self.resize(700,420)

        self.init_qc()
        self.set_qc(config_qc.qcList)
        self.check_permission()
        self.init_signal()

    def init_signal(self):
        self.ui.qcWidget.checkButton.clicked.connect(self.run_check)
        self.ui.submitButton.clicked.connect(self.run_submit)
        self.ui.qcWidget.check.connect(self.qc_pass)

    def init_qc(self):
        self.entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True)) 
        self.entity.context.update(res='default')
        self.ui.qcWidget.update(self.entity.project, context_info.ContextKey.scene, self.entity.step, res=self.entity.res, entity=self.entity, process='default')

    def set_qc(self, qcList):
        self.ui.qcWidget.qcDisplayListWidget.clear()
        for name in qcList:
            func = self.ui.qcWidget.get_func(name)
            if func:
                self.ui.qcWidget.add_qc_item(name, func)

    def qc_pass(self, status):
        self.ui.submitButton.setEnabled(status)

    def check_permission(self):
        from rf_utils import user_info
        self.user = user_info.User()
        
        if self.user.is_admin():
            self.ui.queueButton.setVisible(True)
            self.ui.queueButton.clicked.connect(self.run_queue)

    def run_check(self):
        text = """ """
        for a in range(self.ui.qcWidget.qcDisplayListWidget.count()):
            name = self.ui.qcWidget.qcDisplayListWidget.item(a).text()
            data = self.ui.qcWidget.qcDisplayListWidget.item(a).data(QtCore.Qt.UserRole)
            status = data['status']

            if not status:
                text = text + config_qc.text_message[name]

        self.ui.textWidget.setText(text)

    def run_submit(self):
        from rf_app.save_plus import save_utils
        from rf_utils.widget import dialog
        from rf_app.publish.scene.export import set_asset_list
        nameUser = '.Clean'
        #self.reorder_group()
        self.set_hidden_asm()
        save_utils.save_file_farm(nameUser=nameUser)

        self.set_asset_inview()
        set_asset_list.export2(self.entity)
        self.record_namespace()

        dialog.MessageBox.success('Cleaning complete', 'Complete')
    
        if self.user.is_admin():
            self.ui.queueButton.setEnabled(True)
        else:
            maya_win.deleteUI(uiName)

    def run_queue(self):
        from rf_app.publish.scene import batch
        path = str(mc.file(q=True, sn=True))
        if path:
            path = path.replace('\\','/')
            batch.submit_queue(path)

    def check_hidden_asm(self, grp):
        invalid = []
        asms = asm_lib.sweep_hide_asm(grp)
        if asms: 
            for asm in asms:
                invalid.append(asm)
        return invalid

    def set_hidden_asm(self):
        result_set = self.check_hidden_asm('Set_Grp')
        result_shotdress = self.check_hidden_asm('ShotDress_Grp')

        if result_set:
            for asm in result_set:
                asm_lib.MayaRepresentation(asm).set_hidden(True)

        if result_shotdress:
            for asm in result_shotdress:
                asm_lib.MayaRepresentation(asm).set_hidden(True)

    def set_asset_inview(self):
        from rf_utils.pipeline import cache_list
        from rf_app.publish.scene.export import cache
        from rf_app.asm import asm_lib
        from rf_utils.pipeline import shot_data

        cachewidget = cache_list.CacheList(self.entity)
        geoCacheGrp = self.entity.projectInfo.asset.geo_grp() or 'Geo_Grp'
        cacheList = cache.get_asset_namespace(geoCacheGrp)
        setList = asm_lib.list_set(self.entity)

        keyData = shot_data.Config.buildData
        currentData = shot_data.read_data(self.entity, keyData)

        for asset in cacheList:
            cachewidget.update_asset_data(asset, status=True)

        if not currentData:
            for setName in setList:
                cachewidget.update_asset_data(setName, status=True)
    
        cachewidget.optimize_data(existing_keys=cacheList+setList)
        cachewidget.sort_data()
        cachewidget.write()
        cachewidget.register()

    def record_namespace(self):
        from rf_utils import user_info
        from rf_app.publish.scene.export import alembic_cache
        from rf_app.asm import asm_lib
        from rf_app.publish.scene.export import export_grp
        from rf_app.publish.scene.export import shotdress

        user = user_info.User()
        user.login_user()

        scene = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
        techSuffix = '_Tech'
        alembic_cache.set_config(scene)
        cacheGrp = scene.projectInfo.asset.geo_grp() or 'Geo_Grp'
        techGrp = scene.projectInfo.asset.tech_grp() or 'TechGeo_Grp'

        objGeoDict = alembic_cache.geo_export_dict(scene, scene.name, cacheGrp, 'abc_cache')
        objTechDict = alembic_cache.geo_export_dict(scene, scene.name, techGrp, 'abc_tech_cache', techSuffix)
        setList = asm_lib.list_set(scene) if asm_lib.list_set(scene) else []
        exportgrp = export_grp.run(scene, scene.name) if export_grp.run(scene, scene.name) else []
        shotDressGrp = shotdress.run(scene, scene.name) if shotdress.run(scene, scene.name) else []

        data = OrderedDict()
        for obj in objGeoDict:
            asset = objGeoDict[obj]['namespace']
            typeName = objGeoDict[obj]['type']
            dataFormat = self.data_format(user.login_user(), typeName)
            data[asset] = dataFormat

        for obj in objTechDict:
            asset = objTechDict[obj]['namespace']
            typeName = objTechDict[obj]['type']
            dataFormat = self.data_format(user.login_user(), typeName)
            data[asset] = dataFormat
            
        for setName in setList:
            dataFormat = self.data_format(user.login_user(), 'set')
            data[setName] = dataFormat
            
        for grp in exportgrp:
            nameGrp = exportgrp[grp]['args'][0][grp.lower()]['namespace']
            typeName = exportgrp[grp]['args'][0][grp.lower()]['dataType']
            dataFormat = self.data_format(user.login_user(), typeName)
            data[nameGrp] = dataFormat
            
        for grp in shotDressGrp:
            nameGrp = shotDressGrp[grp]['args'][0][grp]['namespace']
            typeName = shotDressGrp[grp]['args'][0][grp]['dataType']
            dataFormat = self.data_format(user.login_user(), typeName)
            data[nameGrp] = dataFormat

        self.write(data)

    def data_format(self, userName, typeName):
        keyData = OrderedDict()
        keyData['user'] = userName 
        keyData['type'] = typeName 
        #keyData['time'] = datetime.now().strftime("%Y %b %d, %H:%M:%S")
        return keyData

    def datafile(self):
        subdir = 'namespaceData'
        dataDir = '{}/{}'.format(self.entity.path.data().abs_path(), subdir)
        filename = '%s_%s.hero.yml'%(self.entity.name, 'cacheData')
        datafile = '{}/{}'.format(dataDir, filename)
        return datafile

    def write(self, data):
        from rf_utils import file_utils 
        dataPath = self.datafile()
        dirpath = os.path.dirname(dataPath)
        if not os.path.exists(dirpath): 
            os.makedirs(dirpath)

        file_utils.ymlDumper(dataPath, data)

    def reorder_group(self):
        camGrp = self.entity.projectInfo.asset.cam_grp()
        charGrp = self.entity.projectInfo.asset.char_grp()
        propGrp = self.entity.projectInfo.asset.prop_grp()
        setGrp = self.entity.projectInfo.asset.set_grp()
        shotdressGrp = self.entity.projectInfo.asset.shotdress_grp()
        guideGrp = self.entity.projectInfo.asset.guide_grp()
        previsGrp = self.entity.projectInfo.asset.previs_grp()
        exportGrp = self.entity.projectInfo.asset.export_grp()

        grpList = []
        for grp in [camGrp, charGrp, propGrp, setGrp, shotdressGrp, guideGrp, previsGrp, exportGrp]:
            if mc.objExists(grp):
                grpList.append(grp)

        for grp in grpList[::-1]:
            mc.reorder(grp, f=True)

def show():
    if config.isMaya:
        maya_win.deleteUI(uiName)
        myApp = CleaningFile(maya_win.getMayaWindow())
        myApp.show()
        return myApp
