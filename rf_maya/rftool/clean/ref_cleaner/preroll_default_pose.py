from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
import rf_config as config
import os
import sys
import shutil
import logging
import maya.cmds as mc

from rf_utils.widget import dialog 
from rf_maya.rftool.sim.projectData.sim_preRoll import simPreRoll
from rf_utils.widget import collapse_widget
from rf_utils.context import context_info
from rf_utils.sg import sg_process

uiName = 'Pre Roll Sim'

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

class Config: 
    envKey = 'CLEAN NON REFERENCE'

class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)
        self.layout_main = QtWidgets.QVBoxLayout()

        self.layout_task = QtWidgets.QHBoxLayout()
        self.list_widget = QtWidgets.QListWidget()
        self.list_widget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.add_button = QtWidgets.QPushButton('Add')
        self.add_button.setMinimumHeight(40)

        self.option_groupBox = collapse_widget.CollapsibleBox('setting options')
        self.layout_setting = QtWidgets.QVBoxLayout(self.option_groupBox)

        self.layout_animPreRoll = QtWidgets.QHBoxLayout()
        self.label_animPreRoll = QtWidgets.QLabel("Anim PreRoll Frame")
        self.edit_animPreRoll = QtWidgets.QLineEdit()
        self.edit_animPreRoll.setText("991")
        self.layout_animPreRoll.addWidget(self.label_animPreRoll)
        self.layout_animPreRoll.addWidget(self.edit_animPreRoll)

        self.layout_delete = QtWidgets.QHBoxLayout()
        self.label_delete = QtWidgets.QLabel("Delete Frame")
        self.edit_first_delete = QtWidgets.QLineEdit()
        self.edit_first_delete.setText("971")
        self.label_todelete = QtWidgets.QLabel("To")
        self.edit_end_delete = QtWidgets.QLineEdit()
        self.edit_end_delete.setText("990")
        self.layout_delete.addWidget(self.label_delete)
        self.layout_delete.addWidget(self.edit_first_delete)
        self.layout_delete.addWidget(self.label_todelete)
        self.layout_delete.addWidget(self.edit_end_delete)

        self.layout_default_pose = QtWidgets.QHBoxLayout()
        self.label_default_pose = QtWidgets.QLabel("Default Pose Frame")
        self.edit_default_pose = QtWidgets.QLineEdit()
        self.edit_default_pose.setText("971")
        self.layout_default_pose.addWidget(self.label_default_pose)
        self.layout_default_pose.addWidget(self.edit_default_pose)

        self.layout_simPreRoll = QtWidgets.QHBoxLayout()
        self.label_simPreRoll = QtWidgets.QLabel("Sim ProRoll Frame")
        self.edit_simPreRoll = QtWidgets.QLineEdit()
        self.edit_simPreRoll.setText("981")
        self.layout_simPreRoll.addWidget(self.label_simPreRoll)
        self.layout_simPreRoll.addWidget(self.edit_simPreRoll)

        self.layout_fixOnTwo = QtWidgets.QHBoxLayout()
        self.label_fixOnTwo = QtWidgets.QLabel("Fix OnTwo")
        self.edit_fixOnTwo = QtWidgets.QLineEdit()
        self.edit_fixOnTwo.setText("971")
        self.layout_fixOnTwo.addWidget(self.label_fixOnTwo)
        self.layout_fixOnTwo.addWidget(self.edit_fixOnTwo)

        self.layout_main.addWidget(self.list_widget)
        self.layout_main.addWidget(self.add_button)
        self.layout_main.addLayout(self.layout_task)
        self.layout_setting.addLayout(self.layout_animPreRoll)
        self.layout_setting.addLayout(self.layout_delete)
        self.layout_setting.addLayout(self.layout_default_pose)
        self.layout_setting.addLayout(self.layout_simPreRoll)
        self.layout_setting.addLayout(self.layout_fixOnTwo)
        self.option_groupBox.setContentLayout(self.layout_setting)
        self.layout_main.addWidget(self.option_groupBox)

        self.button_create = QtWidgets.QPushButton("Create PreRoll")
        self.button_create.setMinimumHeight(40)
        self.layout_main.addWidget(self.button_create)

        self.setLayout(self.layout_main)

    def add_item(self, namespace):
        item = QtWidgets.QListWidgetItem(self.list_widget)
        item.setText(namespace)
        item.setData(QtCore.Qt.UserRole, namespace)
        item.setSizeHint(QtCore.QSize(20, 20))
        #print(item.sizeHint())

    def get_item_selection(self):
        namespace = []
        for item in self.list_widget.selectionModel().selectedIndexes():
            namespace.append(item.data())

        return namespace

class Preroll(QtWidgets.QMainWindow):
    submitted = QtCore.Signal(object)
    def __init__(self, parent=None):
        super(Preroll, self).__init__(parent)
        self.ui = Ui()
        self.setObjectName(uiName)
        self.setCentralWidget(self.ui)
        self.setWindowTitle('%s' % (uiName))
        #self.setFixedSize(300,500)

        noPreRollList, namespaceList = check()
        tasks = self.get_tasks()
        task_list = self.check_taskName(tasks)
        noPreRollList = noPreRollList if noPreRollList else namespaceList

        if noPreRollList:
            self.add_task_item(tasks)

        self.add_all_item(noPreRollList, task_list)
        self.set_start_Time(970)

        self.set_env_qc()
        self._init_signal()

    def _init_signal(self):
        self.ui.button_create.clicked.connect(self.create_preroll)
        self.ui.add_button.clicked.connect(self.add_item)

    def add_item(self):
        list_namespace = []
        objs = mc.ls(sl=True, sns=True, rn=True)
        if objs:
            for namespace in objs:
                namespace = namespace.split(':')[0]
                
                if namespace not in list_namespace:
                    list_namespace.append(namespace)
                    self.ui.add_item(str(namespace))

    def add_all_item(self, noPreRollList, task_list):
        for namespace in noPreRollList:
            name = namespace.split('_')[0] if '_' in namespace else namespace

            if name in task_list:
                self.ui.add_item(str(namespace))

    def add_task_item(self, tasks):
        if tasks:
            for task in tasks:
                taskName = task['content']
                button = QtWidgets.QPushButton(taskName)
                button.setStyleSheet('background-color: #E5FCC2; color: black')
                self.ui.layout_task.addWidget(button)

    def check_taskName(self, tasks):
        task_list = []
        for task in tasks:
            name = task['content'].split('_')[0] if '_' in task['content'] else task['content']

            if name not in task_list:
                task_list.append(name)

        return task_list

    def set_start_Time(self, value):
        mc.playbackOptions( minTime=value, animationStartTime=value)

    def create_preroll(self):
        tech_cache = self.scene.projectInfo.scene.tech_cache
        namespaces = self.ui.get_item_selection()

        if namespaces:
            anim_preroll = int(self.ui.edit_animPreRoll.text())
            default_pose = int(self.ui.edit_default_pose.text())
            sim_preroll = int(self.ui.edit_simPreRoll.text())
            first_frame = int(self.ui.edit_first_delete.text())
            end_frame = int(self.ui.edit_end_delete.text())
            fixOnTwo_frame = int(self.ui.edit_fixOnTwo.text())

            for ns in namespaces:
                self.check_delete_frame(ns, anim_preroll, first_frame, end_frame)
                simPreRoll.setPreRoll(ns, default_pose, sim_preroll, anim_preroll)

                # fix on2
                if tech_cache:
                    print ('fix on Two : ',tech_cache)
                    simPreRoll.modify_onTwo(ns, fixOnTwo_frame)

                mc.select(clear=True)

    def check_delete_frame(self, namespace, anim_preroll, first_frame, end_frame):
        ctrlList = mc.ls('{}:*_Ctrl'.format(namespace))
        mc.select(ctrlList)
        mc.currentTime(anim_preroll)
        mc.setKeyframe(ctrlList, s=0, itt='flat')

        simPreRoll.delCtrlKey(namespace, first_frame, end_frame)

    def get_tasks(self):
        path = mc.file(q=True,sn=True)
        self.scene = context_info.ContextPathInfo(path=path)
        self.scene.context.use_sg(sg_process.sg, project=self.scene.project, entityType=self.scene.entity_type, entityName=self.scene.name)
        tasks = sg_process.get_tasks_by_step(self.scene.context.sgEntity, 'sim')
        return tasks

    def set_env_qc(self):
        os.environ[Config.envKey] = '1'
        self.submitted.emit(True)

    def closeEvent(self, event):
        self.set_start_Time(1001)
        self.set_env_qc()


def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiName)
        myApp = Preroll(maya_win.getMayaWindow())
        myApp.show()
        return myApp

if __name__ == '__main__': 
    show()


def check():
    from rf_app.publish.scene.export import cache
    entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
    entity.context.use_sg(sg_process.sg, project=entity.project, entityType=entity.entity_type, entityName=entity.name)
    geoCacheGrp = entity.projectInfo.asset.geo_grp() or 'Geo_Grp'
    cacheList = cache.get_asset_namespace(geoCacheGrp)

    task = sg_process.get_tasks_by_step(entity.context.sgEntity, 'sim')
    status = task[0].get('sg_status_list')

    noPreRollList = []
    namespaceList = []
    framePre = 0

    if status != 'omt':
        try:
            shot_node = entity.name_code
            startFrame, endFrame = (mc.shot(shot_node, q=True, st=True), mc.shot(shot_node, q=True, et=True))
        except:
            return [False, 'Cannot get frame range.']

        simList = [name.get('content').split('_')[0] for name in task if '_' in name.get('content')]

        for ns in cacheList:
            if ns.split("_")[0] in simList:

                assetGrp = '%s:Rig_Grp'%(ns)
                refPath = mc.referenceQuery(assetGrp, filename = True)
                
                assetEntity = context_info.ContextPathInfo(path=refPath)
                assetEntity.context.use_sg(sg_process.sg, project=assetEntity.project, entityType=assetEntity.entity_type, entityName=assetEntity.name)
                
                allMover = assetEntity.projectInfo.asset.get('allMoverCtrl')
                root = assetEntity.projectInfo.asset.get('rootCtrl')
                framePre = startFrame - 30
                
                preAllMover = mc.keyframe( '%s:%s'%(ns, allMover) ,time=(framePre,framePre), query=True, keyframeCount=True)
                preRoot = mc.keyframe( '%s:%s'%(ns, root) ,time=(framePre,framePre), query=True, keyframeCount=True)

                if not preAllMover and not preRoot:
                    noPreRollList.append(ns)

                namespaceList.append(ns)

    return noPreRollList, namespaceList