#Import python modules
import os, sys
from functools import partial

# Import Maya module
import maya.OpenMayaUI as mui
import maya.cmds as mc

from rftool.utils import maya_utils
reload(maya_utils)


moduleFile = sys.modules[__name__].__file__
moduleDir = os.path.dirname(moduleFile)
sys.path.append(moduleDir)


def deleteUI(ui): 
    if mc.window(ui, exists=True): 
        mc.deleteUI(ui)
        deleteUI(ui)

def run(): 
    app = RefCleaner()
    app.ui()
    return app

class RefCleaner(object):

    def __init__(self):
        self.count = 0
        #Setup Window
        super(RefCleaner, self).__init__()

        self.mayaUI = 'RefCleaner'
        self.w = 300
        self.h = 520

    def ui(self): 
        deleteUI(self.mayaUI)
        mc.window(self.mayaUI, t='Reference Cleaner')
        mc.columnLayout(adj=1, rs=4)

        mc.text(l='Reference Utilities')

        mc.button(l='Remove unloaded references', h=30, bgc=[0.5, 1, 0.5], c=partial(self.remove_unloaded_references))
        mc.separator()

        mc.text(l='Analyze step : ')
        mc.textField('%s_stepTX' % self.mayaUI, tx='4')
        mc.checkBox('%s_timelineCB' % self.mayaUI, l='Timeline', v=True)
        mc.button(l='Analyze viewport objects', h=30, bgc=[0.5, 1, 0.5], c=partial(self.select_viewport_objs))
        mc.button(l='Remove unselected references', h=30, bgc=[0.6, 1, 0.6], c=partial(self.remove_unselected_references))
        mc.button(l='Remove selected object references', h=30, bgc=[0.6, 0.6, 0.6], c=partial(self.remove_selected_references))

        mc.separator()
        mc.button(l='Select Non-Reference Plys', h=30, bgc=[0.7, 1, 0.7], c=partial(self.select_non_ref_plys))

        mc.separator()
        mc.button(l='Clear Display Layers', h=30, bgc=[1, 0.6, 0.6], c=partial(self.clear_layers))

        mc.separator()
        mc.frameLayout(borderStyle='etchedIn', l='Advanced')
        mc.columnLayout(adj=1, rs=4)
        mc.button(l='Fix vray subdiv', h=30, bgc=[1, 0.6, 0.6], c=partial(self.fix_subdiv_vray))
        mc.checkBox('%s_shaderCB' % self.mayaUI, l='Selected reference', v=True)
        mc.button(l='Remove shader override', h=30, bgc=[0.4, 0.6, 1], c=partial(self.remove_shader_override))
        mc.checkBox('%s_failedEditsCB' % self.mayaUI, l='Selected reference', v=False)
        mc.button(l='Remove failed edits', h=30, bgc=[0.4, 0.7, 1], c=partial(self.remove_failed_edits))

        mc.setParent('..')
        mc.setParent('..')

        mc.separator()
        mc.button(l='Easy Reference Editor', h=30, c=partial(self.run_ref_editor))

        mc.showWindow()
        mc.window(self.mayaUI, e=True, wh=[self.w, self.h])


    def remove_unloaded_references(self, *args): 
        maya_utils.remove_unloaded_references()


    def select_viewport_objs(self, *args): 
        startFrame = int(mc.playbackOptions(q=True, min=True))
        endFrame = int(mc.playbackOptions(q=True, max=True))
        step = int(mc.textField('%s_stepTX' % self.mayaUI, q=True, tx=True))

        if not mc.checkBox('%s_timelineCB' % self.mayaUI, q=True, v=True): 
            startFrame = mc.currentTime(q=True)
            endFrame = startFrame + 1

        maya_utils.select_object_range(startFrame, endFrame, step)

    
    def remove_unselected_references(self, *args): 
        maya_utils.remove_unselected_references()

    def remove_selected_references(self, *args): 
        maya_utils.remove_selected_references()


    def run_ref_editor(self, *args): 
        from rftool.utils.refEditor import app
        app.run()


    def select_non_ref_plys(self, *args): 
        objs = maya_utils.get_transform(type='mesh', reference=False)

        if objs: 
            mc.select(objs)


    def clear_layers(self, *args): 
        maya_utils.remove_displayLayer()


    def remove_shader_override(self, *args): 
        selected = mc.checkBox('%s_shaderCB' % self.mayaUI, q=True, v=True)
        sels = mc.ls(sl=True)
        targets = []

        # for selected objs
        if selected: 
            if sels: 
                for sel in sels: 
                    path = mc.referenceQuery(sel, f=True)
                    targets.append(path)

        # list all refs 
        else: 
            targets = mc.file(q=True, r=True)

        if targets: 
            for path in targets: 
                maya_utils.remove_shader_override(path)


    def remove_failed_edits(self, *args): 
        selected = mc.checkBox('%s_failedEditsCB' % self.mayaUI, q=True, v=True)
        sels = mc.ls(sl=True)
        targets = []

        # for selected objs
        if selected: 
            if sels: 
                for sel in sels: 
                    path = mc.referenceQuery(sel, f=True)
                    targets.append(path)

        # list all refs 
        else: 
            targets = mc.file(q=True, r=True)

        if targets: 
            for path in targets: 
                maya_utils.remove_failed_edits(path)
          

    def fix_subdiv_vray(self, *args): 
        from rftool.fix import misc
        reload(misc)
        smGeo = [a for a in mc.ls(type='mesh') if '_SmGeo' in a]
        misc.fix_vray_subdiv(smGeo, 1)
        geo = [a for a in mc.ls(type='mesh') if '_Geo' in a]
        misc.fix_vray_subdiv(geo, 0)

