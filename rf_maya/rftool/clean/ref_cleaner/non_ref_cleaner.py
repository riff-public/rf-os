from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui
import rf_config as config
import os
import sys
import shutil
import logging
import maya.cmds as mc
from rf_utils.widget import dialog 

_title = 'qc'
_version = 'v.0.0.1'
_des = 'clear non reference'
uiName = 'clear'

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

class Config: 
    envKey = 'CLEAN NON REFERENCE'

class Ui(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Ui, self).__init__(parent)
        self.layout = QtWidgets.QVBoxLayout()
        self.layout_list = QtWidgets.QHBoxLayout()

        self.tree_Widget = QtWidgets.QTreeWidget()
        self.tree_Widget.setSelectionMode(QtWidgets.QListWidget.ExtendedSelection)
        self.tree_Widget.setHeaderItem(QtWidgets.QTreeWidgetItem(["select asset non-reference"]))

        self.layoutButton_up = QtWidgets.QHBoxLayout()
        self.button_selection = QtWidgets.QPushButton('Select All')
        self.button_selection.setFixedSize(225,24)
        self.button_refresh = QtWidgets.QPushButton()
        self.button_refresh.setFixedSize(50,24)
        self.refresh_icon = '%s/core/icons/16/refresh_icon.png' % os.environ.get('RFSCRIPT')
        self.button_refresh.setIcon(QtGui.QPixmap(self.refresh_icon))
        self.button_refresh.setIconSize(QtCore.QSize(20, 20))

        self.layoutButton_up.addWidget(self.button_selection)
        self.layoutButton_up.addWidget(self.button_refresh)

        self.layoutButton_down = QtWidgets.QHBoxLayout()
        self.button_guide = QtWidgets.QPushButton('Guide_Grp')
        self.button_export = QtWidgets.QPushButton('Export_Grp')
        self.button_delete = QtWidgets.QPushButton('Delete')
        self.layoutButton_down.addWidget(self.button_guide)
        self.layoutButton_down.addWidget(self.button_export)
        self.layoutButton_down.addWidget(self.button_delete)

        self.layout.addWidget(self.tree_Widget)
        self.layout.addLayout(self.layoutButton_up)
        self.layout.addLayout(self.layoutButton_down)
        self.setLayout(self.layout)

        self._init_signal()

    def _init_signal(self):
        self.tree_Widget.itemSelectionChanged.connect(self.get_item_selection)

    def create_treeWidgetItem(self,objs):
        for obj in sorted(objs):
            text = obj
            parent = QtWidgets.QTreeWidgetItem(self.tree_Widget, [obj])
            for text in objs[text]:
                child = QtWidgets.QTreeWidgetItem(parent, [text[1::]])

    def get_item_selection(self):
        objs = []
        for item in self.tree_Widget.selectionModel().selectedIndexes():
            objs.append(item.data())
        return objs

    def get_item_all(self):
        item = [str(self.tree_Widget.topLevelItem(i).text(0)) for i in range(self.tree_Widget.topLevelItemCount())]
        return item

class Cleaner(QtWidgets.QMainWindow):
    submitted = QtCore.Signal(object)
    def __init__(self, parent=None):
        super(Cleaner, self).__init__(parent)
        self.ui = Ui()
        self.setObjectName(uiName)
        self.setCentralWidget(self.ui)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setFixedSize(300,500)

        self.ui.create_treeWidgetItem(self.list_obj())
        self._init_signal()
        self.set_env_qc()

    def _init_signal(self):
        self.ui.tree_Widget.itemSelectionChanged.connect(self.select_item_to_maya)
        self.ui.button_selection.clicked.connect(self.select_all)
        self.ui.button_guide.clicked.connect(self.guide_grp)
        self.ui.button_export.clicked.connect(self.export_grp)
        self.ui.button_delete.clicked.connect(self.remove_obj)
        self.ui.button_refresh.clicked.connect(self.refresh_item)

    def refresh_item(self):
        #clear and create items
        self.ui.tree_Widget.clear()
        self.ui.create_treeWidgetItem(self.list_obj())

    def select_item_to_maya(self):
        #fucntion signal
        mc.select(self.ui.get_item_selection())

    def select_all(self):
        #fucntion signal
        items = self.ui.get_item_all()
        for item in items:
            index = self.ui.tree_Widget.findItems(item,QtCore.Qt.MatchExactly)
            index[0].setSelected(True)

    def remove_obj(self):
        #fucntion signal
        sel_nonRefObjs = self.ui.get_item_selection()
        mc.select(None)
        if sel_nonRefObjs:
            self.ui.tree_Widget.itemSelectionChanged.disconnect(self.select_item_to_maya)
            mc.delete(sel_nonRefObjs)
            self.refresh_item()
            self.ui.tree_Widget.itemSelectionChanged.connect(self.select_item_to_maya)
        else:
            dialog.MessageBox.warning('Not Select','Not Select Asset')

    def guide_grp(self):
        #fucntion signal
        grp = 'Guide_Grp'
        nonRefObjs = self.ui.get_item_selection()
        if nonRefObjs:
            self.ui.tree_Widget.clear()
            self.create_grp(grp,nonRefObjs)
            mc.setAttr('%s.visibility '%grp, 0)
            self.refresh_item()
        else:
            dialog.MessageBox.warning('Not Select','Not Select Asset')

    def export_grp(self):
        #fucntion signal
        grp = 'Export_Grp'
        nonRefObjs = self.ui.get_item_selection()
        if nonRefObjs:
            self.ui.tree_Widget.clear()
            self.create_grp(grp,nonRefObjs)
            self.refresh_item()
        else:
            dialog.MessageBox.warning('Not Select','Not Select Asset')

    def create_grp(self,grp,objs):
        #create group
        if not mc.ls(grp,l=True):
            grp = mc.group( em=True, name=grp )
        elif not mc.ls(grp,l=True)[0][1:] == mc.ls(grp,sn=True)[0]:
            grp = mc.group( em=True, name=grp )

        nonRef_grp = []
        for obj in objs:
            obj_grp = self.list_top_non_ref(obj)
            if mc.listRelatives(obj_grp,p=True,f=True):
                obj_top = mc.listRelatives(obj_grp,p=True,f=True)
                obj_grp = mc.group(obj_grp, name='constraint' )
                mc.parentConstraint(obj_top,obj_grp,mo=True)
            if obj_grp not in nonRef_grp:
                nonRef_grp.append(obj_grp)

        mc.parent(nonRef_grp,mc.ls(grp,sn=True)[0])

    def list_top_non_ref(self, obj):
        #get top hierarchy non reference
        if mc.listRelatives(obj, p=True,f=True):
            obj_Before = obj
            if not mc.referenceQuery(mc.listRelatives(obj, p=True,f=True), inr=True) and len(obj.split('|')) != 1:
                #obj = mc.listRelatives(obj, p=True,f=True)[0][1::]
                obj = mc.listRelatives(obj, p=True,f=True)[0]
                ref = [i for i in mc.listRelatives(obj,f=True) if mc.referenceQuery(i, inr=True)]
                if ref:
                    return obj_Before
                else:
                    return self.list_top_non_ref(obj)
            else:
                return obj
        else:
            return obj

    def list_obj(self):
        #list obj for create item
        nonRefObjs = self.get_non_pipeline_objs()
        nonRef_grp = []
        nonRef_set = {}
        for obj in nonRefObjs:
            obj_grp = self.list_top_non_ref(obj)
            if obj_grp not in nonRef_grp:
                nonRef_grp.append(obj_grp)

        for obj in nonRef_grp:
            nonRef_set[obj] = []
            for ref_obj in nonRefObjs:
                if obj == ref_obj.split('|')[1]:
                    nonRef_set[obj].append(ref_obj)
                elif len(obj.split('|')) > 1 and obj in ref_obj:
                    nonRef_set[obj].append(ref_obj)

        return nonRef_set

    def get_non_reference(self):
        #list all obj non reference 
        allObjs = mc.ls(l=True, dag=True, type='mesh')
        nonRefParent = []

        for obj in allObjs: 
            if not mc.referenceQuery(obj, inr=True):
                tr = mc.listRelatives(obj, parent=True,f=True)[0]
                if not mc.referenceQuery(tr, inr=True) and not 'camshot_grp' in tr and tr not in nonRefParent:
                    nonRefParent.append(tr)

        return nonRefParent

    def get_non_pipeline_objs(self):
        from rf_utils.context import context_info
        entity = context_info.ContextPathInfo()
        nonRefObjs = self.get_non_reference()
        # exception groups
        tempGrp = entity.projectInfo.asset.temp_grp()
        guideGrp = entity.projectInfo.asset.guide_grp()
        previsGrp = entity.projectInfo.asset.previs_grp()
        exportGrp = entity.projectInfo.asset.export_grp()
        shotdressGrp = entity.projectInfo.asset.shotdress_grp()

        nonRefObjs = [a for a in nonRefObjs if not tempGrp in a] if nonRefObjs else []
        nonRefObjs = [a for a in nonRefObjs if not guideGrp in a] if nonRefObjs else []
        nonRefObjs = [a for a in nonRefObjs if not previsGrp in a] if nonRefObjs else []
        nonRefObjs = [a for a in nonRefObjs if not exportGrp in a] if nonRefObjs else []
        nonRefObjs = [a for a in nonRefObjs if not shotdressGrp in a] if nonRefObjs else []

        return nonRefObjs

    def set_env_qc(self):
        os.environ[Config.envKey] = '1'
        self.submitted.emit(True)

    def closeEvent(self, event):
        nonRefObjs = self.get_non_pipeline_objs()
        if not nonRefObjs:
            self.set_env_qc()

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        maya_win.deleteUI(uiName)
        myApp = Cleaner(maya_win.getMayaWindow())
        myApp.show()
        return myApp

if __name__ == '__main__': 
    show()