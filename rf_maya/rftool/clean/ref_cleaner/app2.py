#Import python modules
import os, sys
from functools import partial
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

# Import Maya module
import maya.OpenMayaUI as mui
import maya.cmds as mc

from rftool.utils import maya_utils
reload(maya_utils)
from rf_utils.context import context_info
from rf_maya.rftool.utils import objectsInCameraView as oic
reload(oic)
from rf_utils.sg import sg_process
from rf_utils.widget import dialog
from rf_maya.rftool.anim import anim_utils
reload(anim_utils)

moduleFile = sys.modules[__name__].__file__
moduleDir = os.path.dirname(moduleFile)
sys.path.append(moduleDir)


def deleteUI(ui): 
    if mc.window(ui, exists=True): 
        mc.deleteUI(ui)
        deleteUI(ui)

def run(): 
    app = RefCleaner()
    app.ui()
    return app

class RefCleaner(object):

    def __init__(self):
        self.count = 0
        #Setup Window
        super(RefCleaner, self).__init__()

        self.mayaUI = 'RefCleaner'
        self.w = 300
        self.h = 400

    def ui(self): 
        deleteUI(self.mayaUI)
        mc.window(self.mayaUI, t='Reference Cleaner')
        mc.columnLayout(adj=1, rs=5)

        mc.text(l='Animation Scene Cleaner')
        mc.separator()
        mc.text(l='Remove unused reference')

        # mc.rowColumnLayout(nc=2)
        mc.intFieldGrp('%s_stepTX' % self.mayaUI, value1=5, l='Frame Step')
        # mc.checkBox('%s_timelineCB' % self.mayaUI, l='Timeline', v=True)

        # mc.rowColumnLayout(nc=2, co=[(1, 'both', 5), (2, 'both', 5)])
        mc.button(l='1. Select UNSEEN asset', h=30, bgc=[0.5, 1, 0.5], c=partial(self.select_unseen_objects))
        # mc.button(l='Remove unused references', h=30, bgc=[0.6, 1, 0.6], c=partial(self.remove_unselected_references))
        mc.button(l='Remove selected asset', h=30, bgc=[0.6, 0.6, 0.6], c=partial(self.remove_selected_references))
        # mc.setParent('..')

        mc.separator()
        mc.button(l='2. Check Camera', h=30, bgc=[0.9, 0.6, 0.4], c=partial(self.check_cam))
        mc.button(l='3. Switch to md', h=30, bgc=[0.9, 0.9, 0.9], c=partial(self.switch_md))
        mc.button(l='4. Clean key range', h=30, bgc=[0.5, 0.8, 0.7], c=partial(self.clean_key_range))
        mc.separator()

        mc.text(l='Optimized Set')
        mc.button(l='5. Optimized Set', h=30, bgc=[0.6, 0.7, 0.9], c=partial(self.optimized_set))
        mc.button(l='Hide Selected GPU', h=30, bgc=[0.6, 0.7, 0.9], c=partial(self.hide_asm))
        mc.button(l='Set Asset List', h=30, bgc=[0.4, 0.6, 1], c=partial(self.set_asset_list))


        mc.showWindow()
        mc.window(self.mayaUI, e=True, wh=[self.w, self.h])


    def remove_unloaded_references(self, *args): 
        maya_utils.remove_unloaded_references()

    def clean_key_range(self, *args):
        # get start end
        try:
            entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
            shot_node = entity.name_code
            frame_start_end = [mc.shot(shot_node, q=True, st=True), mc.shot(shot_node, q=True, et=True)]
        except:
            mi = mc.playbackOptions(q=True, min=True)
            mx = mc.playbackOptions(q=True, max=True)
            frame_start_end = (mi, mx)
    
        cleaned_keys = anim_utils.clean_key_outside_range(startFrame=frame_start_end[0], endFrame=frame_start_end[1])
        # print cleaned_keys
        print 'Key(s) cleaned: %s' %len(cleaned_keys),

    def select_unseen_objects(self, *args):
        # get setp
        step = mc.intFieldGrp('%s_stepTX' % self.mayaUI, q=True, v1=True)

        # get start end
        entity = context_info.ContextPathInfo(path=mc.file(q=True, sn=True))
        shot_node = entity.name_code
        frame_start_end = [mc.shot(shot_node, q=True, st=True), mc.shot(shot_node, q=True, et=True)]
    
        # camera
        cameraName = mc.shot(shot_node, q=True, currentCamera=True)
        if mc.nodeType(cameraName) == 'camera':
            cameraName = mc.listRelatives(cameraName, p=True)[0]
        # look thru
        mc.lookThru(cameraName)
        mc.setAttr('%s.panZoomEnabled' %cameraName, 0)
        mc.setAttr('%s.displayFilmGate' %cameraName, 1)
        mc.setAttr('%s.displayResolution' %cameraName, 1)

        # group name
        grpName = entity.projectInfo.asset.geo_grp()

        # get all ref nodes
        refs = mc.file(q=True, r=True)
        allRefNodes = []
        for ref in refs:
            rfn = mc.referenceQuery(ref, rfn=True)
            allRefNodes.append(rfn)

        # get unseen
        unSeenRefNodes = oic.getUnseenReferenceNodes(cameraName, grpName, 
            refs=refs, timeRange=frame_start_end, 
            step=step, viewport='legacy')

        seenNss = set()
        for n in allRefNodes:
            if n not in unSeenRefNodes:
                ns = mc.referenceQuery(n, ns=True)
                if ns != ':':
                    ns = ns[1:]
                    seenNss.add(ns)


        depends = maya_utils.getDependenciesList()
        unused_nss = []
        sels = []

        for rn in unSeenRefNodes:
            ns = mc.referenceQuery(rn, ns=True)
            if ns != ':':
                ns = ns[1:]
                skip = False
                for dep in depends:
                    if ns in dep and set(dep).intersection(seenNss):
                        skip = True
                        break
                if skip:
                    continue

                # only include reference with Geo_Grp (char, prop, etc)
                geoGrp = '%s:%s' %(ns, grpName)
                ls = mc.ls(geoGrp)
                if ls:
                    unused_nss.append(ns)
                    sels.append(ls[0])

        mc.select(sels, r=True, ne=True)
        if unused_nss:
            msg = 'Off-camera assets: \n\n%s' %('\n'.join(unused_nss))
            msg += '\n\nPlease check your camera for selected assets and hit\n"Remove selected references" if they are not appearing on the screen.'
            logger.info(msg)
            mc.confirmDialog(message=msg, title=self.mayaUI)


    def current_camera(self): 
        panel = mc.getPanel(withFocus=True)
        camera = mc.modelPanel(panel, q=True, cam=True)
        return camera

    def find_shader_ref(self): 
        # hard coded with mtr 
        key = '_mtr_'
        shaderNs = [mc.referenceQuery(a, ns=True) for a in mc.file(q=True, r=True) if key in os.path.basename(a)]
        contents = []
        # print shaderNs
        for ns in shaderNs: 
            contents += mc.ls('%s:*' % ns[1:])

        return contents, shaderNs

    def remove_unselected_references(self, *args): 
        maya_utils.remove_unselected_references()

    def remove_selected_references(self, *args): 
        maya_utils.remove_selected_references()

    def check_cam(self, *args): 
        from rftool.preload import shotCam_check
        reload(shotCam_check)
        from rf_utils.context import context_info
        entity = context_info.ContextPathInfo()
        shotCam_check.check(entity)

    def switch_md(self, *args): 
        datas = check_asset_data()
        missing = [v[0].name for k, v in datas.iteritems() if not v[2]]
        switchable = [v[1] for k, v in datas.iteritems() if v[2]]
        message = ''
        if missing: 
            message = 'missing rig md %s assets - %s\n' % (len(missing), ','.join(missing))
        if len(switchable): 
            message += 'Switch %s assets to "md"?' % len(switchable)
            switch = True
        else: 
            message += 'No asset to switch'
            switch = False 

        result = self.dialog(message)

        if result == 'Yes' and switch == True: 
            for rnNode, data in datas.iteritems(): 
                if data[2]: 
                    path = data[1]          
                    # print os.path.exists(path)          
                    mc.file(path, loadReference=rnNode)
                    logger.info('Switch to %s' % path)
                else: 
                    print 'missing', data[1]

        return switch,missing

    
    def optimized_set(self, *args): 
        from rf_app.asm import asm_lib
        reload(asm_lib)
        # define context
        scene = context_info.ContextPathInfo()

        # select asm 
        with maya_utils.CameraBaseSelection(): 
            self.asms = asm_lib.select_viewport_asm(timeline=True)
        # self.asms = [a for a in self.asms if not asm_lib.MayaRepresentation(a).is_root()]
        allAsms = asm_lib.list_asms()
        hiddenAsms = [a for a in allAsms if not a in self.asms]
        hiddenAsms = [a for a in hiddenAsms if not asm_lib.MayaRepresentation(a).is_root()]
        # assetDict = asm_lib.sg_asset_dict(self.asms)



        # shotEntity = sg_process.get_shot_entity(scene.project, scene.name)
        # sg_process.sanity_check(assetDict)
        # sg_process.update_asset_list(shotEntity.get('id'), assetDict, sg_field='sg_setdresses')
        mc.select(hiddenAsms)

    def hide_asm(self, *args): 
        from rf_app.asm import asm_lib
        reload(asm_lib)
        allAsms = asm_lib.list_asms()
        nodes = []
        keys = []
        action1 = 'Remove keys and set hidden'
        action2 = 'Select key'

        for asm in allAsms: 
            key = mc.keyframe(asm, q=True, n=True)
            if key: 
                keys+=key
                nodes.append(asm)

        if keys: 
            result = dialog.CustomMessageBox.show('Keyframe', 'Key detected. Please Remove before set hidden?', buttons=[action1, action2])

            if result.value == action1: 
                mc.delete(keys)

            if result.value == action2: 
                mc.select(nodes)
                return 

        ui = asm_lib.UiCmd()
        ui.hide_asm()
        # allAsms = asm_lib.list_asms()
        # visibleAsms = [a for a in allAsms if not asm_lib.MayaRepresentation(a).is_hidden()]

        # mc.select(visibleAsms)

    def set_asset_list(self, *args): 
        from rf_app.publish.scene.export import set_asset_list
        scene = context_info.ContextPathInfo()
        reload(set_asset_list)
        set_asset_list.export2(scene)

        message = 'Set asset list complete'
        mc.confirmDialog( title='Confirm', message=message, button=['OK'])


    def dialog(self, message): 
        result = mc.confirmDialog( title='Confirm', message=message, button=['Yes', 'Cancel'])
        return result


def check_asset_data(checkRes='md', switchRes='md'): 
    mtrKey = '_mtr_'
    cameraKey = '_cam'
    refs = mc.file(q=True, r=True)
    assetPaths = [a for a in refs if not mtrKey in a]
    switchDict = dict()
    entity = context_info.ContextPathInfo()
    path_output = entity.path.hero().abs_path()

    for path in assetPaths: 
        if not os.path.splitext(path)[-1] == '.abc': 
            if not cameraKey in path and not path_output in path: 
                rnNode = mc.referenceQuery(path, referenceNode=True)
                asset = context_info.ContextPathInfo(path=path)
                if not asset.type == 'mattepaint' and not asset.type == '_global' and not '_instdress' in path :
                    filename = os.path.basename(path).split('{')[0]
                    outputList = asset.guess_outputKey(filename)
                    outputKey, step = outputList[0]
                    asset.context.update(step=step)
                    asset.extract_name(filename, outputKey=outputKey)
                    
                    if asset.res not in checkRes:
                        asset.context.update(step='rig', res=switchRes)
                        heroDir = asset.path.hero().abs_path()
                        heroDirRel = asset.path.hero()
                        rigFile = asset.output_name(outputKey='rig', hero=True)
                        switchFileRel = '%s/%s' % (heroDirRel, rigFile)
                        switchFileAbs = '%s/%s' % (heroDir, rigFile)
                        exists = os.path.exists(switchFileAbs)
                        switchDict[rnNode] = [asset, switchFileAbs, exists]
                        
                        if not exists:
                            logger.info('Missing: %s' % (switchFileAbs))

    return switchDict


def combine_dict(dict1, dict2):
    sgEntities = []
    combDict = OrderedDict()

    for k, v in dict1.iteritems():
        if not k in combDict.keys():
            combDict[k] = v

    for k, v in dict2.iteritems():
        if not k in combDict.keys():
            combDict[k] = v

    for k, v in combDict.iteritems():
        if v.get('id'):
            entity = {'type': 'Asset', 'id': v.get('id'), 'code': v.get('name')}
            if not entity in sgEntities:
                sgEntities.append(entity)
    return sgEntities
