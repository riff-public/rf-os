import maya.cmds as mc
import os
import sys

def run():
    sel = mc.ls(sl=True)
    path = mc.referenceQuery(sel[0], f=True) if sel else ''
    if path:
        remove_shader_override(path)

def remove_reference_edits(path, successfulEdits, failedEdits, editCommand, keyword=None):
    rnNode = mc.referenceQuery(path, referenceNode=True)
    targetNodes = mc.referenceQuery(rnNode, editNodes=True, editAttrs=True, successfulEdits=successfulEdits, failedEdits=failedEdits, editCommand=editCommand)

    for node in targetNodes:
        remove = True
        if keyword:
            if not keyword in node:
                remove = False

        if remove:
            mc.referenceEdit(node, failedEdits=failedEdits, successfulEdits=successfulEdits, removeEdits=True, editCommand=editCommand)

def remove_shader_override(path):
    # unload
    rnNode = mc.referenceQuery(path, referenceNode=True)
    mc.file(path, unloadReference=rnNode)

    # remove connectAttr / disconnectAttr
    remove_reference_edits(path, successfulEdits=True, failedEdits=True, editCommand='connectAttr', keyword='instObjGroups')
    remove_reference_edits(path, successfulEdits=True, failedEdits=True, editCommand='disconnectAttr', keyword='instObjGroups')

    # load back
    try:
        mc.file(path, loadReference=rnNode, prompt=False)
    except Exception as e:
        logger.error(e)