import os
import sys 
import maya.cmds as mc 
import maya.app.general.fileTexturePathResolver as ftpr
from rf_maya.rftool.shade import shade_utils 

from collections import defaultdict


def list_file(nodeTypes):
    nodes = defaultdict(list)
    for typ in nodeTypes:
        nodes[typ].extend(mc.ls(type=typ))
    return nodes

def get_attr(node, attr):
    if attr == 'fileTextureName':
        typ = mc.nodeType(node)
        if typ == 'file':
            attr = 'fileTextureName'
        elif typ == 'RedshiftNormalMap':
            attr = 'tex0'
    node_attr = "%s.%s"%(node, attr)
    if mc.objExists(node_attr):
        return mc.getAttr(node_attr)

def get_uv_mode(node):
    typ = mc.nodeType(node)
    if typ == 'file':
        uvMode = get_attr(node, attr= 'uvTilingMode')
    elif typ == 'RedshiftNormalMap':
        uvMode = 3
    return uvMode

def pattern(currentFile, useFrame, uvMode):
    return ftpr.getFilePatternString( currentFile , useFrame , uvMode )

def allFiles(pattern):
    return ftpr.findAllFilesForPattern( pattern, None)

def set_attr(node, attr, uvMode):
    if attr == 'uvTilingMode':
        typ = mc.nodeType(node)
        if typ == 'RedshiftNormalMap':
            return
    mc.setAttr("%s.%s"%(node, attr), uvMode)

def select(obj):
    mc.select(obj)

def set_string_attr (nodeFile, attr, filePath, useFrame):
    if attr == 'fileTextureName':
        typ = mc.nodeType(nodeFile)
        if typ == 'file':
            attr = 'fileTextureName'
        elif typ == 'RedshiftNormalMap':
            attr = 'tex0'
            pat = pattern( filePath, useFrame, 3 )
            num_files = len(allFiles( pat ))
            if num_files > 1:
                filePath = pat

    mc.setAttr("%s.%s"%(nodeFile, attr), filePath, type="string")

def selcet_object_by_shade(objects):

    color_attr = mc.listConnections('%s.outColor'%objects, p=True) #### TerrainZoneC_Mat.color
    alpha_attr = mc.listConnections('%s.outAlpha'%objects, p=True) #### TerrainZoneC_Mat.alpha

    if color_attr:
        return mc.select(color_attr[0].split('.color')[0])
    elif alpha_attr:
        return mc.select(alpha_attr[0].split('.alpha')[0])
    # mc.hyperShade(objects= objects)

def list_sel():
    return mc.ls(sl=True)