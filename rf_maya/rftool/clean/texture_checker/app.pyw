# v.0.0.1 polytag switcher
_title = 'RF Texture Check'
_version = 'v.0.0.1'
_des = 'wip'
uiName = 'RFTextureCheckUI'

#Import python modules
import sys
import os
import json
import getpass

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

# config file
config_file = '%s/local_config.json' % moduleDir
with open(config_file) as configData:
    localConfig = json.load(configData)

SCRIPT = localConfig['SCRIPT']
ROOT = os.environ.get(SCRIPT)
core = localConfig['CORE']

# import config
sys.path.append('%s/%s' % (ROOT, core))
import rf_config as config

# framework modules
from rf_utils import log_utils
from rf_utils.ui import load
from rf_utils.ui import stylesheet
from rftool.utils import maya_utils
from rf_utils import icon
from rf_utils import file_utils
from rf_utils.pipeline import user_pref
import maya_hook as hook
from rf_utils import admin
reload(hook)


from rf_utils.context import context_info


user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

import texture_widget
reload (texture_widget)

VALID_EXT = ('.jpg', '.tif', '.png', '.tga', '.exr')
globalTextureFolder = "P:/Bikey/asset/publ/_global/textures/shares"

class CleanTextureUI(QtWidgets.QMainWindow):
    def __init__(self, entity, parent=None):
        #Setup Window
        super(CleanTextureUI, self).__init__(parent)
        self.asset = entity
        # ui read
        uiFile = '%s/ui.ui' % moduleDir
        if config.isMaya:
            self.ui = load.setup_ui_maya(uiFile, parent)
        else:
            self.ui = load.setup_ui(uiFile, self)

        self.check = None

        self.ui.show()
        self.ui.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setup_widgets()
        self.init_signals()
        self.list_node_file()

    def setup_widgets(self):
        self.ItemWidget = texture_widget.TextureWidget()
        self.ui.widget_Table_Layout.addWidget(self.ItemWidget)

    def init_signals(self):
        self.ui.copyTexture_pushButton.clicked.connect(self.copy_all)
        self.ItemWidget.itemSelected.connect(self.select_node)
        self.ItemWidget.listWidget.customContextMenuRequested.connect(self.show_menu)
        self.ui.refresh_pushButton.clicked.connect(self.refresh_ui)

    # def copy_texture(self):
    #     self.list_node_file()

    def list_node_file(self):
        self.asset.context.update(look=self.asset.process)
        assetPath = self.asset.path.texture().abs_path()
        assetPublPath = self.asset.path.publish_texture().abs_path()
        self.dictsTrue = {}
        self.dictsFalse = {}
        self.dictsError = {}
        allFileNodes = hook.list_file(['file', 'RedshiftNormalMap'])

        text =[]

        for typ, nodes in allFileNodes.iteritems():
            # hook.selcet_object_by_shade(node)
            # if hook.list_sel() != []:
            for node in nodes:
                if ':' in node:
                    continue
                currentFile = hook.get_attr(node, attr='fileTextureName')

                if globalTextureFolder in currentFile:
                    continue

                uvMode = hook.get_uv_mode(node)
                useFrame = hook.get_attr(node, attr='useFrameExtension')
                try:
                    pattern = hook.pattern( currentFile, useFrame, uvMode )
                    allFiles = hook.allFiles( pattern )
                except :
                    allFiles = []
                    currentFile  = '%s - deiend permission'% node

                if allFiles == []:
                    dataFile = [node, [currentFile], uvMode, useFrame]
                    self.dictsError[node] = dataFile

                for index, file in enumerate(allFiles):
                #check path in Asset
                    text.append(file)
                    dataFile = [node, [file], uvMode, useFrame]
                    # path in asset          
                    if assetPath in file or assetPublPath in file:
                        if not os.path.exists(file):
                            if node in self.dictsError:
                                file_add = self.dictsError[node][1]
                                file_list = file_add.append(file)
                                dataFile = [node, file_list, uvMode, useFrame] 
                            self.dictsError[node] = dataFile
                        else:
                            if node in self.dictsTrue and self.dictsTrue[node][1] != None:
                                file_add = self.dictsTrue[node][1]
                                file_list = file_add.append(file)
                                dataFile = [node, file_list, uvMode, useFrame] 
                            self.dictsTrue[node] = dataFile
                    # path not in Asset
                    else:
                        if not os.path.exists(file):
                            if node in self.dictsError:
                                file_add = self.dictsError[node][1]
                                file_list = file_add.append(file)
                                dataFile = [node, file_list, uvMode, useFrame] 
                            self.dictsError[node] = dataFile
                        else:
                            if node in self.dictsFalse:
                                file_add = self.dictsFalse[node][1]
                                file_add.append(file)
                                # print file_add
                                dataFile = [node, file_add, uvMode, useFrame] 
                            self.dictsFalse[node] = dataFile

        # check extension
        # pull out from dictsTrue
        to_remove = []
        for node, data in self.dictsTrue.iteritems():
            files = data[1]
            if files:
                for f in files:
                    fn, ext = os.path.splitext(f)
                    if not ext or ext not in VALID_EXT:
                        to_remove.append((node, data))
                        break

        if to_remove:
            for node, data in to_remove:
                del self.dictsTrue[node]
                self.dictsError[node] = data

        # pull out from dictsFalse
        to_remove = []
        for node, data in self.dictsFalse.iteritems():
            files = data[1]
            if files:
                for f in files:
                    fn, ext = os.path.splitext(f)
                    if not ext or ext not in VALID_EXT:
                        to_remove.append((node, data))
                        break

        if to_remove:
            for node, data in to_remove:
                del self.dictsFalse[node]
                self.dictsError[node] = data

        print len(text)
        print 'True', self.dictsTrue
        print 'Error', self.dictsError
        print 'False', self.dictsFalse

            # pattern = ftpr.getFilePatternString( currentFile , useFrame , uvMode )
            # allFiles = ftpr.findAllFilesForPattern( pattern , frameNumber )
        self.ItemWidget.listWidget.clear()
        self.send_to_widgets(self.dictsTrue,'True')
        self.send_to_widgets(self.dictsError,'Error')
        self.send_to_widgets(self.dictsFalse,'False')
        logger.debug('list_node_file :')

    def send_to_widgets(self, dictData, status):
        self.ItemWidget.set_display(dictData, status)

    def select_node(self, dictData):
        hook.select(dictData['node'])
        logger.debug("Select : %s"%dictData['node'])
        path = hook.get_attr(dictData['node'], "fileTextureName")
        self.fileTextureSelected = path
        self.selectedNode = dictData['node']

    def copy_texture(self):
        self.check = None
        selectedItem = self.ItemWidget.current_item()
        logger.debug('copy: %s'% selectedItem['path'])
        self.copy_file_texture(selectedItem['path'] ,
                               selectedItem['node'],
                               selectedItem['uvMode'],
                               selectedItem['useFrame'])


    def copy_all(self):
        self.check = None
        items = self.dictsFalse
        for node, data in items.iteritems():
            logger.debug('copy: %s %s'% (node, data))
            self.copy_file_texture(data[1], data[0], data[2], data[3])

                    #for item in items:
            #logger.debug('copy: %s'% item['path'])
            #self.copy_file_texture(item['path'],item['node'])

    def copy_file_texture(self, srcFile, node, uvMode, useFrame):
        assetTexturePath = self.asset.path.texture().abs_path()
        assetPublPath = self.asset.path.publish_texture().abs_path()
        len_srcFile = len(srcFile)
        for file in srcFile:
            file =file.replace('\\', '/')
            name = os.path.basename(file)
            dstPath = os.path.join(assetTexturePath, name).replace('\\', '/')
            if os.path.exists(dstPath):

                if self.check == True:
                    if self.count == 0:
                        continue
                else:
                    msg = QtWidgets.QMessageBox()
                    msg.setIcon(QtWidgets.QMessageBox.Warning)

                    msg.setText("%s is already exsist. \nDo you want to replace a file in destination?"%(dstPath))

                    msg.setWindowTitle("Replace path")
                    yes_button = msg.addButton('Yes', QtWidgets.QMessageBox.ButtonRole.YesRole)
                    # replace_button = msg.addButton('sss', QtWidgets.QMessageBox.ButtonRole.HelpRole)
                    no_button = msg.addButton('No', QtWidgets.QMessageBox.ButtonRole.RejectRole)
                    cb = QtWidgets.QCheckBox("Do this to all items.")
                    msg.setCheckBox(cb)

                    msg.exec_()

                    self.check = cb.isChecked()

                    if msg.clickedButton() == no_button:
                        self.count = 0
                        continue
                    elif msg.clickedButton() == yes_button:
                        self.count = 1

            publ_dst_path = os.path.join(assetPublPath, name).replace('\\', '/')
            if file.lower() != dstPath.lower() or file.lower() == publ_dst_path.lower():
                # texture = file_utils.copy(file, dstPath)
                dstDir = dstPath.replace(dstPath.split('/')[-1], '')
                if not os.path.exists(dstDir) :
                    os.makedirs(dstDir)
                admin.copyfile(file, dstPath)
            else:
                print 'Already IN Asset'
                logger.warning('Already IN Asset')

            pattern = hook.pattern( dstPath, useFrame, 3 )
            dstPath = dstPath if len_srcFile == 1 else pattern
            self.relink_texture(dstPath, node, uvMode, useFrame)
            self.refresh_ui()
            logger.debug("copyfile : %s , %s"%(file,dstPath))

    def refresh_ui(self):
        self.ItemWidget.listWidget.clear()
        self.list_node_file()


    def relink_texture(self, filePath, nodeFile, uvMode, useFrame):
        hook.set_attr(nodeFile, 'uvTilingMode', uvMode )

        hook.set_string_attr(nodeFile, 'fileTextureName', filePath, useFrame)
        logger.debug('conectAttr %s'% nodeFile)

        # mc.setAttr('%s.ftn'% self.selectedNode,filePath,type='string' )

    def show_menu(self, pos):
        data = self.ItemWidget.current_item()
        menu = QtWidgets.QMenu(self)

        command = menu.addAction('Copy')
        command.triggered.connect(self.copy_texture)

        menu.popup(self.ItemWidget.listWidget.mapToGlobal(pos))

def show(entity):
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = CleanTextureUI(entity, maya_win.getMayaWindow())
        return myApp

if __name__ == '__main__':
    show()
