import os 
import sys 
import yaml

from Qt import QtWidgets
from Qt import QtGui
from Qt import QtCore

import subprocess
import shutil
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
moduleDir = os.path.dirname(sys.modules[__name__].__file__)

from rf_utils import file_utils 
from rftool.utils import maya_utils
from rf_utils import icon 

class TextureWidget(QtWidgets.QWidget):
    """docstring for BrowseWidget"""
    itemSelected = QtCore.Signal(dict)
    def __init__(self, parent=None):
        super(TextureWidget, self).__init__(parent=parent)
        
        # layout
        self.layout = QtWidgets.QVBoxLayout()
        
        # widget 
        self.label = QtWidgets.QLabel('Texture')
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

        self.layout.addWidget(self.label)
        self.layout.addWidget(self.listWidget)
        self.setLayout(self.layout)
        self.init_signals()

    def init_signals(self): 
        self.listWidget.clicked[QtCore.QModelIndex].connect(self.emit_signal)
            
    def set_display(self, dicts, status):
        for node, data in dicts.iteritems():
            nameShow = data[1]
            dictData = {'node': data[0], 'path': data[1], 'uvMode': data[2], 'useFrame': data[3]}
            self.add_item(nameShow, dictData, status)
            
    def add_item(self, list_fileName, dictData, check):
        if list_fileName:
            for fileName in list_fileName:
                item = QtWidgets.QListWidgetItem(self.listWidget)
                item.setText(fileName)
                item.setData(QtCore.Qt.UserRole, dictData)
                if check == 'True':
                    item.setIcon(QtGui.QPixmap(icon.success))
                if check == 'False': # not in the pipeline 
                    item.setIcon(QtGui.QPixmap(icon.needFix))
                if check == 'Error': # file not exists 
                    item.setIcon(QtGui.QPixmap(icon.no))

    def emit_signal(self): 
        data = self.current_item()
        self.itemSelected.emit(data)

    def current_item(self): 
        index = self.listWidget.currentIndex()
        data = index.data(QtCore.Qt.UserRole)
        return data

    def get_all_items(self): 
        listItemData = []
        for a in range(self.listWidget.count()):
            index = self.listWidget.item(a)
            data = index.data(QtCore.Qt.UserRole)
            listItemData.append(data)

        return listItemData

