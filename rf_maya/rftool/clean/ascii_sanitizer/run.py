import os, sys
import shutil
import time
import argparse
import logging
# from pprint import pprint

import pymel.core as pm
from rf_maya.rftool.clean.ascii_sanitizer import main as ascl
import rf_config as config
from rf_utils import log_utils

DEFAULT_FILE_LIST = '%s\\core\\rf_maya\\rftool\\clean\\ascii_sanitizer\\fileList.txt' %os.environ['RFSCRIPT']
DEFAULT_CONFIG = '%s\\core\\rf_maya\\rftool\\clean\\ascii_sanitizer\\config.yml' %os.environ['RFSCRIPT']

BACKUP_DRIVE = 'R:'
HOME_DIR = '%s/%s' %(os.environ['HOME'], 'ascii_sanitizer')
LOG_LEVEL = logging.INFO

def setup_parser(title):
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-p', dest='paths', nargs='+', help='Paths to maya scene file', default='')
    parser.add_argument('-f', dest='file_list', type=str, help='Path to config text file', default=DEFAULT_FILE_LIST)
    parser.add_argument('-c', dest='config_path', type=str, help='Path to config text file', default=DEFAULT_CONFIG)
    parser.add_argument('-r', dest='restore', type=bool, help='Toggle restore mode', default=0)
    parser.add_argument('-b', dest='binary', type=bool, help='Toggle generate binary mode', default=0)
    
    return parser

def restore(files):
    timestr = time.strftime("%Y%m%d-%H%M%S")
    log_file = 'restore_result.%s.log' %(timestr)
    log_path = '%s/%s' %(HOME_DIR, log_file)
    fh1 = logging.FileHandler(log_path)
    fh1.setLevel(LOG_LEVEL)
    logger.addHandler(fh1)

    for i, fp in enumerate(files):
        if os.path.exists(fp) and os.path.isfile(fp) and fp.endswith('.ma'):
            header = '%s:%s' %(i, fp)
            print header
            logger.info(header)

            dirName = os.path.dirname(fp)
            baseName = os.path.basename(fp)
            name, ext = os.path.splitext(baseName)
            drive, spPath = os.path.splitdrive(fp)
            backup_path = '%s/%s' %(BACKUP_DRIVE, spPath)
            backup_folder = os.path.dirname(backup_path)
            backup_files = [f for f in os.listdir(backup_folder) if f.startswith(name) and f.endswith(ext)]
            logger.debug('\t%s' %backup_files)
            if backup_files:
                recent_backup = sorted(backup_files)[-1]
                recent_backup_path = '%s/%s' %(backup_folder, recent_backup)
                logger.info('\tRestoring: \t%s\t%s' %(recent_backup_path, fp))
                shutil.copyfile(recent_backup_path, fp)
                
                # restore .mb too
                bk_name, bk_ext = os.path.splitext(recent_backup)
                mb_bk_path = '%s/%s.mb' %(backup_folder, bk_name)
                if os.path.exists(mb_bk_path):
                    mb_fp = '%s/%s.mb' %(dirName, name)
                    logger.info('\tRestoring: \t%s\t%s' %(mb_bk_path, mb_fp))
                    shutil.copyfile(mb_bk_path, mb_fp)

def generateBinary(files):
    timestr = time.strftime("%Y%m%d-%H%M%S")
    log_file = 'generateBinary_result.%s.log' %(timestr)
    log_path = '%s/%s' %(HOME_DIR, log_file)
    fh1 = logging.FileHandler(log_path)
    fh1.setLevel(LOG_LEVEL)
    logger.addHandler(fh1)

    logger.info('Files are...')
    logger.info(files)

    num_files = len(files)
    for i, fp in enumerate(files):
        if os.path.exists(fp) and os.path.isfile(fp) and fp.endswith('.ma'):
            percent = ((i+1) / float(num_files))*100.0
            print 'Progress %s%%: %s' %(round(percent, 2), fp)

            logger.info('\tOpenning: %s' %fp)
            try:
                pm.openFile(fp, f=True)
            except:
                logger.error('Failed to open: %s' %fp)
                continue
            result_path, ext = os.path.splitext(fp)
            mb_path = result_path + '.mb'
            logger.info('\tBinary path: %s' %mb_path)

            pm.saveAs(mb_path, f=True)
            logger.info('\tBinary file generated: %s' %mb_path)

def clean(files, config_path):
    ''' 
    Clean list of ascii in file_list text file. Output is written to local 
    if infected, backup to R: drive, copy local to server, then generate .mb from .ma
    - files = paths to ma files
    - config_path = path to config file
    - log_path = path to log file
    '''
    if not os.path.exists(HOME_DIR):
        logger.debug('Script home dir created: %s' %HOME_DIR)
        os.makedirs(HOME_DIR)

    timestr = time.strftime("%Y%m%d-%H%M%S")
    log_file = 'clean_result.%s.log' %(timestr)
    log_path = '%s/%s' %(HOME_DIR, log_file)
    fh1 = logging.FileHandler(log_path)
    fh1.setLevel(LOG_LEVEL)
    logger.addHandler(fh1)


    logger.debug('Files are...\n')
    logger.debug(files)

    num_files = len(files)
    for i, fp in enumerate(files):
        if os.path.exists(fp) and os.path.isfile(fp) and fp.endswith('.ma'):

            percent = ((i+1) / float(num_files))*100.0
            print 'Progress %s%%: %s' %(round(percent, 2), fp)
            
            p_basename = os.path.basename(fp)

            # clean the file
            local_path = '%s/%s' %(HOME_DIR, p_basename)
            # logger.debug('\tLocal path: %s' %local_path)

            e = ''
            infected = set()
            try:
                infected = ascl.clean(config_path=config_path, 
                            input_path=fp, 
                            output_path=local_path)
            except Exception, e:
                logger.error('Ascii Sanitizer failed: %s' %fp)
                continue

            # only if found infection in the ascii
            if infected:
                logger.info(fp)
                logger.info('\tinfected: %s' %infected)
                logger.info('\tlocal path: %s' %local_path)

                # backup to backup drive
                drive, spPath = os.path.splitdrive(fp)
                backup_path = '%s/%s' %(BACKUP_DRIVE, spPath)
                backup_folder = os.path.dirname(backup_path)
                backup_basename = os.path.basename(backup_path)
                bn, ext = os.path.splitext(backup_basename)
                timestr = time.strftime("%Y%m%d-%H%M%S")
                bn = '%s.%s' %(bn, timestr)
                backup_basename = bn + ext
                
                if not os.path.exists(backup_folder):
                    logger.debug('\tMaking dir: %s' %backup_folder)
                    os.makedirs(backup_folder)

                backup_path = '%s/%s' %(backup_folder, backup_basename)
                shutil.copyfile(fp, backup_path)
                logger.info('\tBacked up to: %s' %(backup_path))

                # copy local .ma to server
                shutil.copyfile(local_path, fp)
                logger.info('\tCopied local to: %s' %fp)

                # generate .mb
                if '_rig_' in p_basename:
                    logger.debug('\tOpenning: %s' %local_path)
                    pm.openFile(local_path, f=True)
                    result_path, ext = os.path.splitext(fp)
                    mb_path = result_path + '.mb'
                    logger.info('\tBinary path: %s' %mb_path)

                    # backup .mb too
                    if os.path.exists(mb_path):
                        mb_baseName = os.path.basename(mb_path)
                        mb_name, mb_ext = os.path.splitext(mb_baseName)
                        mb_name = '%s.%s' %(mb_name, timestr)
                        mb_name = mb_name + mb_ext
                        mb_bk_path = '%s/%s' %(backup_folder, mb_name)
                        shutil.copyfile(mb_path, mb_bk_path)
                        logger.info('\tBinary Backed up to: %s' %(mb_bk_path))

                    pm.saveAs(mb_path, f=True)
                    logger.info('\tBinary file generated: %s' %mb_path)


if __name__ == "__main__":
    
    parser = setup_parser(':::Ascii Sanitizer:::')
    params = parser.parse_args()

    if params.restore and params.binary:
        print 'Invlid flags, either choose -r(retore mode) or -b(generate binary mode).'
    else:
        if params.paths:
            files = params.paths
        elif params.file_list:
            files = []
            with open(params.file_list, 'r') as f:
                files = [l.rstrip() for l in f.readlines()]

        if files:
            if params.restore:
                restore(files=files)
            elif params.binary:
                generateBinary(files=files)
            else:
                if not os.path.exists(params.config_path):
                    print 'Path does not exist: %s' %params.config_path
                else:
                    clean(files=files,
                        config_path=params.config_path)



'''
"C:/Program Files/Autodesk/Maya2017/bin/mayapy.exe" "D:/dev/core/rf_maya/rftool/clean/ascii_sanitizer/run.py" -f "P:/SevenChickMovie/asset/publ/prop/adeptCabinA/hero/adeptCabinA_rig_main_pr.hero.ma" "P:/SevenChickMovie/asset/publ/prop/adeptCabinB/hero/adeptCabinB_rig_main_pr.hero.ma"

"C:/Program Files/Autodesk/Maya2017/bin/mayapy.exe" "D:/dev/core/rf_maya/rftool/clean/ascii_sanitizer/run.py" -f "C:/Users/nattapong.n/Documents/no_mb.txt" -b 1

cleaned = "D:/dev/core/rf_maya/rftool/clean/ascii_sanitizer/fileList.txt"
paths = []
with open(cleaned, 'r') as f:
    paths = f.readlines()
err_path = 'D:/__playground/ascii/err.txt'
for fp in paths:
    print 'Openning: %s' %fp
    res = None
    try:
        res = pm.openFile(fp, f=True)
    except:
        pass
    if not res:
        with open(err_path, 'a') as f:
            f.write('%s\n' %fp)
'''