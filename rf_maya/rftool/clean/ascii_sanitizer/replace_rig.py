import os, sys
import shutil
import time
import argparse
import logging
# from pprint import pprint

import pymel.core as pm
from rf_utils import ascii_utils
# reload(ascii_utils)
from rf_utils.context import context_info
reload(context_info)
from rf_utils import file_utils
from rf_maya.rftool.clean.ascii_sanitizer import main as ascl

DEFAULT_FILE_LIST = '%s\\core\\rf_maya\\rftool\\clean\\ascii_sanitizer\\sceneList.txt' %os.environ['RFSCRIPT']
DEFAULT_CONFIG = '%s\\core\\rf_maya\\rftool\\clean\\ascii_sanitizer\\config.yml' %os.environ['RFSCRIPT']
HOME_DIR = '%s/%s' %(os.environ['HOME'], 'replace_rig')
LOG_LEVEL = logging.INFO

def setup_parser(title):
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-f', dest='file_list', type=str, help='Path to text file containing file paths', default=DEFAULT_FILE_LIST)
    parser.add_argument('-p', dest='paths', nargs='+', help='Paths to maya scene file', default='')
    parser.add_argument('-c', dest='config_path', type=str, help='The path to config file', default=DEFAULT_CONFIG)
    return parser

def main(files, config_path):
    ''' 
    - files = list of paths
    '''
    timestr = time.strftime("%Y%m%d-%H%M%S")
    log_file = 'replace_rig_result.%s.log' %(timestr)
    log_path = '%s/%s' %(HOME_DIR, log_file)
    fh1 = logging.FileHandler(log_path)
    fh1.setLevel(LOG_LEVEL)
    logger.addHandler(fh1)

    if not os.path.exists(HOME_DIR):
        logger.debug('# Script home dir created: %s' %HOME_DIR)
        os.makedirs(HOME_DIR)

    config = {}
    if os.path.exists(config_path):
        config = file_utils.ymlLoader(config_path)
    if not config:
        return

    # read the path list
    logger.info('# Starting ascii_sanitizer.replace_rig')
    logger.info('# Files are...')
    logger.info('\n'.join(files))

    num_files = len(files)
    for i, fp in enumerate(files):
        if os.path.exists(fp) and os.path.isfile(fp) and fp.endswith('.ma'):
            logger.info(fp)

            percent = ((i+1) / float(num_files))*100.0
            print 'Progress %s%%: %s' %(round(percent, 2), fp)
            
            logger.info('\t- Reading: %s' %fp)
            ma = ascii_utils.MayaAscii(path=fp)
            ma.read()

            # clean plugin in the scene file
            data, infected = ascl.sanitize(ma.data, config)
            found = False
            if infected:
                logger.info('Infected: %s' %infected)
                ma.data = data
                found = True

            refData = ma.listReferences()
            logger.debug('\t# %s' %refData)
            for data in refData:
                path = data['path']
                path = path.replace('\\', '/')
                try:
                    scene = context_info.ContextPathInfo(path=path)
                except:
                    logger.info('\tSkipping invalid path: %s' %path)
                    continue
                baseName = os.path.basename(path)
                fileName, pExt = os.path.splitext(baseName)
                if not fileName.startswith('%s_rig_main_md.hero' %scene.name) and not fileName.startswith('%s_rig_main_pr.hero' %scene.name):
                    logger.info('\tSkipping: %s' %path)
                    continue

                res = fileName.split('.')[0].split('_')[-1]
                # get the correct path
                scene.context.update(res=res, step='rig')
                heroDir = scene.path.hero()
                rigHeroName = scene.output_name('rig', hero=True)
                correct_path = '%s/%s' %(heroDir, rigHeroName)
                correct_path, cext = os.path.splitext(correct_path)
                curr_path, ext = os.path.splitext(path)

                if curr_path != correct_path or path.endswith('.ma'):
                    logger.info('\tInvalid: %s' %data['path'])
                    # find correct path, see if binary is available
                    mb_path = correct_path + '.mb'
                    ma_path = correct_path + '.ma'
                    if os.path.exists(mb_path.replace('$RFPUBL', os.environ['RFPUBL'])):
                        correct_path = mb_path
                    elif os.path.exists(ma_path.replace('$RFPUBL', os.environ['RFPUBL'])):
                        correct_path = ma_path
                    else:
                        logger.error('\t***Hero does not exist: %s' %mb_path)
                        continue

                    if correct_path != fp:
                        logger.info('\t- Replace: \n\t\t%s\n\t\t%s' %(data['path'], correct_path))
                        ma.replaceReference(node=data['refNode'], path=correct_path)
                        found = True
            if found:
                # get incremental save path
                fp_dir = os.path.dirname(fp)
                scene = context_info.ContextPathInfo(path=fp_dir)
                version = file_utils.calculate_version(file_utils.list_file(fp_dir))
                scene.context.update(version=version)
                workDir = scene.path.workspace().abs_path()
                baseName = os.path.basename(fp)
                if '_split' in baseName:
                    workFile = scene.work_name(user=False)
                    workFile = workFile.replace('_main', '_split')
                else:
                    workFile = scene.work_name(user=True)
                    # change user name to AI
                    bns = baseName.split('.')
                    wfp = workFile.split('.')
                    workFile = '%s.%s.AI.%s' %(bns[0], wfp[1], wfp[3])
                
                out_path = '%s/%s' %(workDir, workFile)
                logger.info('\t- outpath: %s' %out_path)
                # save as the file
                local_out_path = '%s/%s' %(HOME_DIR, workFile)
                ma.write(local_out_path)

                logger.info('\t- copying: \n\t\t%s\n\t\t%s' %(local_out_path, out_path))
                shutil.copyfile(local_out_path, out_path)

            logger.info('\n')

if __name__ == "__main__":
    parser = setup_parser(':::Replace Rig:::')
    params = parser.parse_args()

    if os.path.exists(params.config_path):

        if params.paths:
                files = params.paths
        elif params.file_list:
            files = []
            with open(params.file_list, 'r') as f:
                files = [l.rstrip() for l in f.readlines()]
        if files:
            main(files=files, config_path=params.config_path)
    else:
        print 'Config path does not exists: %s' %params.config_path

'''
"C:/Program Files/Autodesk/Maya2018/bin/mayapy.exe" "D:/dev/core/rf_maya/rftool/clean/ascii_sanitizer/replace_rig.py"

'''