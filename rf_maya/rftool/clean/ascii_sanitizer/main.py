import os, sys
import re
import collections
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

from rf_utils import file_utils
from rf_utils import ascii_utils
# reload(ascii_utils)

def sanitize(data, config):
    infected = set()
    if 'requires' in config and 'requires' in data:
        new_data = []
        for chunk in data['requires']:
            found = False
            combined_chunk = '*SPLIT*'.join(chunk)
            for kw in config['requires']:
                if kw in combined_chunk:
                    infected.add(kw)
                    found = True
                    break
            if not found:
                new_data.append(chunk)
        data['requires'] = new_data

    if 'createNode' in config and 'createNode' in data:
        new_data = []
        for chunk in data['createNode']:
            new_lines = []
            for line in chunk:
                do_break = False
                skip_line = False

                for kw in config['createNode']:
                    if type(kw) == str:
                        if kw in line:
                            # try to get the name of the node
                            line_splits = line.split()
                            if '-n' in line_splits and line_splits.index('-n') < len(line_splits) - 1:
                                node_name = line_splits[line_splits.index('-n') + 1]
                                node_name = node_name.replace(';', '')
                                node_name = node_name.replace('"', '')
                                if 'connectAttr' not in config:
                                    config['connectAttr'] = []
                                config['connectAttr'].append(node_name)

                                if 'relationship' not in config:
                                    config['relationship'] = []
                                config['relationship'].append(node_name)

                            infected.add(kw)
                            do_break = True
                            new_lines = []
                            # if node type is in line, don't write the whole chunk
                            break
                        else:
                            continue
                    elif type(kw) == collections.OrderedDict:
                        sub_cmd = kw.keys()[0]
                        line_splits = line.split()
                        for skw in kw[sub_cmd]:
                            # if sub command and sub-keyword in line, just skip that line
                            # but still consider other line in the chunk
                            search = re.search(skw, line)
                            if search:
                                infected.add(skw)
                                skip_line = True
                                do_break = False
                                break
                            
                if do_break:
                    do_break = False
                    break
                if not skip_line:
                    new_lines.append(line)
            new_data.append(new_lines)
        data['createNode'] = new_data

        if 'connectAttr' in config and 'connectAttr' in data:
            new_data = []
            for chunk in data['connectAttr']:
                found = False
                cmd_line = chunk[0]
                for kw in config['connectAttr']:
                    if kw in cmd_line:
                        found = True
                        break
                if not found:
                    new_data.append(chunk)
            data['connectAttr'] = new_data

        if 'relationship' in config and 'relationship' in data:
            new_data = []
            for chunk in data['relationship']:
                found = False
                cmd_line = chunk[0]
                for kw in config['relationship']:
                    if kw in cmd_line:
                        found = True
                        break
                if not found:
                    new_data.append(chunk)
            data['relationship'] = new_data

    return data, infected

def clean(config_path, input_path, output_path=None):
    # read the config
    config = {}
    if os.path.exists(config_path):
        config = file_utils.ymlLoader(config_path)
    if not config:
        return

    reader = ascii_utils.MayaAscii(path=input_path)
    data = reader.read()
    data, infected = sanitize(data, config)
    

    # only write new file only if output_path is provided and the ascii is infected
    if output_path:
        result_lines = []
        for cmd, cmd_chunk in data.iteritems():
            for chunk in cmd_chunk:
                for line in chunk:
                    result_lines.append(line)

        with open(output_path, 'w') as f:
            f.writelines(result_lines)

    return infected

def get_default_config(): 
    config = '%s/config.yml' % os.path.dirname(sys.modules[__name__].__file__).replace('\\', '/')
    return config

def detect_project_config(): 
    from rf_utils.context import context_info 
    entity = context_info.ContextPathInfo()
    configPath = entity.projectInfo.config_dict().get('clean_config')
    if configPath and os.path.exists(configPath): 
        global _configFile
        _configFile = configPath


def run_clean(filename): 
    # clean only if .ma 
    name, ext = os.path.splitext(filename)

    if ext == '.ma': 
        checkResult = is_clean(filename)
        if not checkResult: 
            # infect 
            clean(_configFile, filename, filename)
            return filename

    return filename

def is_clean(filename): 
    checkResult = clean(_configFile, filename)
    return True if not checkResult else False


_configFile = get_default_config()

try: 
    detect_project_config()
except Exception as e: 
    print e

'''
from rf_maya.rftool.clean.ascii_sanitizer import main as ascl
reload(ascl)

in_path = 'P:/SevenChickMovie/asset/work/prop/fluteGold/rig/bodyRig/maya/fluteGold_rig_bodyRig.v008.KenR.ma'
out_path = 'D:/__playground/ascii/output.ma'
config_path = "D:/dev/core/rf_maya/rftool/clean/ascii_sanitizer/config.yml"
res = ascl.clean(config_path, in_path, out_path)

import cProfile
cProfile.run('res = ascl.clean(in_path, out_path)')
'''