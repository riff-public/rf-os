import os, sys
import shutil
import time
import argparse
import logging
# from pprint import pprint

import pymel.core as pm
from rf_utils import log_utils
        
from rf_utils.context import context_info
reload(context_info)
from rf_utils.pipeline import check
reload(check)

DEFAULT_FILE_LIST = '%s\\core\\rf_maya\\rftool\\clean\\ascii_sanitizer\\fileList.txt' %os.environ['RFSCRIPT']

HOME_DIR = '%s/%s' %(os.environ['HOME'], 'checkGeo')
LOG_LEVEL = logging.INFO

def setup_parser(title):
    parser = argparse.ArgumentParser(description=title, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-p', dest='paths', nargs='+', help='Paths to maya scene file', default='')
    parser.add_argument('-f', dest='file_list', type=str, help='Path to file list text file', default=DEFAULT_FILE_LIST)
    
    return parser

def main(files):
    if not os.path.exists(HOME_DIR):
        logger.debug('Script home dir created: %s' %HOME_DIR)
        os.makedirs(HOME_DIR)

    timestr = time.strftime("%Y%m%d-%H%M%S")
    log_file = 'checkGeo_result.%s.log' %(timestr)
    log_path = '%s/%s' %(HOME_DIR, log_file)
    fh1 = logging.FileHandler(log_path)
    fh1.setLevel(LOG_LEVEL)
    logger.addHandler(fh1)

    logger.debug('Files are...\n')
    logger.debug(files)

    num_files = len(files)
    for i, fp in enumerate(files):
        if os.path.exists(fp) and os.path.isfile(fp) and fp.endswith('.ma') or fp.endswith('.mb'):

            percent = ((i+1) / float(num_files))*100.0
            print 'Progress %s%%: %s' %(round(percent, 2), fp)
            print fp
            
            try:
                pm.openFile(fp, f=True)
            except Exception, e:
                logger.info('\n----- %s' %(fp))
                logger.info(e)
                continue
            scene = context_info.ContextPathInfo(path=str(pm.sceneName()))

            scene.context.update(step='model', process='main', res='md')
            publishPath = scene.path.output_hero().abs_path()
            dataFile = scene.output(outputKey='cache')

            abcPath = '%s/%s' % (publishPath, dataFile)
            if not os.path.exists(abcPath):
                logger.info('\n----- %s' %(fp))
                logger.info('ABC not found: %s' %abcPath)
                continue

            geoGrp = scene.projectInfo.asset.geo_grp()
            mdGeoGrp = scene.projectInfo.asset.md_geo_grp()
            checkGrp = '|'.join([geoGrp, mdGeoGrp])
            if not pm.ls(checkGrp):
                logger.info('\n----- %s' %(fp))
                logger.info('Cannot find in scene: %s' %checkGrp)
                continue

            # short name check
            sn_result = None
            sn_errors = []
            try:
                sn_checker = check.CheckGeo(checkGrp=checkGrp, inputData=abcPath, includeShape=False)
                sn_result = sn_checker.check()
            except Exception, e:
                # logger.info('\n----- %s' %(fp))
                # logger.info(e)
                sn_errors.append(e)
                continue

            # long name check
            ln_result = None
            ln_errors = []
            try:
                ln_checker = check.CheckHierarchy(checkGrp=mdGeoGrp, inputData=abcPath, addNamespaceToAbc=False, ignoreShape=True)
                ln_result = ln_checker.check()
            except Exception, e:
                # logger.info('\n----- %s' %(fp))
                # logger.info(e)
                ln_errors.append(e)
                continue

            if sn_errors or ln_errors or sn_result != None or ln_result != None:
                logger.info('\n----- %s' %(fp))
                logger.info('\tABC path: %s' %(abcPath))

            if sn_result != None or ln_result != None:
                sn_info, ln_info = [], []
                if sn_result:
                    for key in ('alien', 'mismatch topology', 'missing'):
                        if sn_result[key]:
                            sn_info.append('%s: %s' %(key, sn_result[key]))
                if sn_info:
                    logger.info('\tShort Name Check')
                    for info in sn_info:
                        logger.info('\t\t%s' %(info))
                if sn_errors:
                    for e in sn_errors:
                        logger.info(e)

                if ln_result:
                    for key in ('alien', 'mismatch topology', 'missing'):
                        if ln_result[key]:
                            ln_info.append('%s: %s' %(key, ln_result[key]))
                if ln_info:
                    logger.info('\tLong Name Check')
                    for info in ln_info:
                        logger.info('\t\t%s' %(info))
                if ln_errors:
                    for e in ln_errors:
                        logger.info(e)

if __name__ == "__main__":
    
    parser = setup_parser(':::Check Geo:::')
    params = parser.parse_args()

    if params.paths:
        files = params.paths
    elif params.file_list:
        files = []
        with open(params.file_list, 'r') as f:
            files = [l.rstrip() for l in f.readlines()]

    if files:
        main(files=files)



'''
"C:/Program Files/Autodesk/Maya2018/bin/mayapy.exe" "D:/dev/core/rf_maya/rftool/clean/ascii_sanitizer/checkGeo.py"
'''