import os

def get_heroes(root_paths, file_list, filterStr='', exclude=''):
    '''
    Collect paths of files and write them to a text file

    - root_paths = list(asset type directory)
    - file_list = str(output text path)
    
    from rf_maya.rftool.clean.ascii_sanitizer import run as ast
    reload(ast)
    fileList = ast.get_files(root_paths=['P:\\SevenChickMovie\\asset\\publ\\prop', 'P:\\SevenChickMovie\\asset\\publ\\char'],
            file_list='D:\\__playground\\ascii\\fileList.txt')
    '''
    files = []
    for root_path in root_paths:
        for di in os.listdir(root_path):
            asset_path = '%s/%s' %(root_path, di)
            if not os.path.isdir(asset_path):
                continue

            hero_path = '%s/%s' %(asset_path, 'hero')
            if not os.path.exists(hero_path):
                continue

            for fl in os.listdir(hero_path):
                fp = '%s/%s' %(hero_path, fl)
                if os.path.isfile(fp) and fl.endswith('.ma'):
                    # infected = set()
                    # print fp
                    # try:
                    #     infected = ascl.clean(config_path=DEFAULT_CONFIG, 
                    #                 input_path=fp, 
                    #                 output_path=None)
                    # except Exception, e:
                    #     print 'Ascii Sanitizer failed!'
                    #     continue
                    # if infected:
                    if filterStr and filterStr not in fl:
                        continue
                    if exclude and exclude in fl:
                        continue

                    files.append(fp.replace('\\', '/'))

    with open(file_list, 'w') as f:
        f.write('\n'.join(files))

    return files


def get_anim_split(root_paths):
    '''
    P:/SevenChickMovie/scene/work/act1
    '''
    result = []
    for root_path in root_paths:
        for fol in os.listdir(root_path):
            if fol.endswith('_all'):
                continue

            fol_path = '%s/%s' %(root_path, fol)
            anim_path = '%s/anim/main/maya' %(fol_path)

            if not os.path.exists(anim_path):
                continue

            files = [f for f in os.listdir(anim_path) if f.endswith('.ma') and os.path.isfile('%s/%s' %(anim_path, f))]
            sp_files = []
            skip = False
            for f in files:
                splits = f.split('.')
                if splits[0].endswith('_split'):
                    sp_files.append(f)
                else:
                    skip = True

            if skip:
                continue

            if sp_files:
                sp_files = sorted(sp_files)
                result.append('%s/%s' %(anim_path, sp_files[-1]))

    return result


def get_heroes_without_binary(root_paths, file_list):
    '''
    Collect paths of files and write them to a text file

    - root_paths = list(asset type directory)
    - file_list = str(output text path)
    
    from rf_maya.rftool.clean.ascii_sanitizer import run as ast
    reload(ast)
    fileList = ast.get_files(root_paths=['P:\\SevenChickMovie\\asset\\publ\\prop', 'P:\\SevenChickMovie\\asset\\publ\\char'],
            file_list='D:\\__playground\\ascii\\fileList.txt')
    '''
    files = []
    for root_path in root_paths:
        for di in os.listdir(root_path):
            asset_path = '%s/%s' %(root_path, di)
            if not os.path.isdir(asset_path):
                continue
            asset_name = asset_path.split('/')[-1]
            hero_path = '%s/%s' %(asset_path, 'hero')
            if not os.path.exists(hero_path):
                continue

            for fl in os.listdir(hero_path):
                fp = '%s/%s' %(hero_path, fl)
                if os.path.isfile(fp) and fl.endswith('.ma'):
                    if fl == '%s_rig_main_md.hero.ma' %(asset_name) or fl == '%s_rig_main_pr.hero.ma' %(asset_name):
                        p, ext = os.path.splitext(fp)
                        mb_path = '%s.mb' %p
                        if not os.path.exists(mb_path):
                            files.append(fp.replace('\\', '/'))

    with open(file_list, 'w') as f:
        f.write('\n'.join(files))

    return files

def get_rig_hero(root_paths, file_list):
    '''
    Collect paths of files and write them to a text file

    - root_paths = list(asset type directory)
    - file_list = str(output text path)
    
    from rf_maya.rftool.clean.ascii_sanitizer import run as ast
    reload(ast)
    fileList = ast.get_files(root_paths=['P:\\SevenChickMovie\\asset\\publ\\prop', 'P:\\SevenChickMovie\\asset\\publ\\char'],
            file_list='D:\\__playground\\ascii\\fileList.txt')
    '''
    files = []
    for root_path in root_paths:
        for di in os.listdir(root_path):
            asset_path = '%s/%s' %(root_path, di)
            if not os.path.isdir(asset_path):
                continue
            asset_name = asset_path.split('/')[-1]
            hero_path = '%s/%s' %(asset_path, 'hero')
            if not os.path.exists(hero_path):
                continue

            for fl in os.listdir(hero_path):
                fp = '%s/%s' %(hero_path, fl)
                if os.path.isfile(fp) and fl.endswith('.ma'):
                    if fl == '%s_rig_main_md.hero.ma' %(asset_name):
                        p, ext = os.path.splitext(fp)
                        mb_path = '%s.mb' %p
                        if os.path.exists(mb_path):
                            res_path = mb_path
                        else:
                            res_path = fp
                        files.append(res_path.replace('\\', '/'))

    with open(file_list, 'w') as f:
        f.write('\n'.join(files))

    return files