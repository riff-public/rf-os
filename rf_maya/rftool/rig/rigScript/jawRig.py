import maya.cmds as mc
import maya.mel as mm

import core 
reload( core )
import fkRig
reload(fkRig)

cn = core.Node()
# -------------------------------------------------------------------------------------------------------------
#
#  JAW RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run ( object ):
    def __init__( self , jawUpr1TmpJnt      = 'JawUpr1_TmpJnt' ,
                         jawUprEndTmpJnt    = 'JawUprEnd_TmpJnt' ,
                         jawLwr1TmpJnt      = 'JawLwr1_TmpJnt' ,
                         jawLwr2TmpJnt      = 'JawLwr2_TmpJnt' ,
                         jawLwrEndTmpJnt    = 'JawLwrEnd_TmpJnt' ,
                         parent             = 'Head_Jnt' ,
                         ctrlGrp            = 'Ctrl_Grp' ,
                         skinGrp            = 'Skin_Grp' ,
                         elem               = '' ,
                         side               = '' ,
                         size               = 1
                ):

        ##-- Info
        elem = elem.capitalize()
        self.side = side

        if side == '':
            side = '_'
        else:
            side = '_{}_'.format(side)

        #-- Create Main Group
        self.jawCtrlGrp = cn.createNode('transform' , 'Jaw{}Ctrl{}Grp'.format(elem , side))


        #-- Create JawLwr Rig
        jawLwr1_obj = fkRig.Run( name       = 'Jaw{}Lwr1'.format(elem) ,
                                side       = self.side ,
                                tmpJnt     = jawLwr1TmpJnt ,
                                parent     = parent ,
                                ctrlGrp    = ctrlGrp ,  
                                shape      = 'square' ,
                                jnt_ctrl   = 0  ,
                                size       = size ,
                                color      = 'red',
                                scale_constraint =1,
                                localWorld = 0 ,
                                jnt = 1 ,)

        self.jawLwr1Jnt = jawLwr1_obj.jnt
        self.jawLwr1Ctrl = jawLwr1_obj.ctrl
        self.jawLwr1Gmbl = jawLwr1_obj.gmbl
        self.jawLwr1Zro = jawLwr1_obj.zro
        self.jawLwr1Ofst = jawLwr1_obj.ofst
        self.jawLwr1Pars = jawLwr1_obj.pars
        self.jawLwr1Grp = jawLwr1_obj.ctrlGrp

        jawLwr2_obj = fkRig.Run( name       = 'Jaw{}Lwr2'.format(elem) ,
                                side       = self.side ,
                                tmpJnt     = jawLwr2TmpJnt ,
                                parent     = self.jawLwr1Jnt ,
                                ctrlGrp    = ctrlGrp ,  
                                shape      = 'square' ,
                                jnt_ctrl   = 0  ,
                                size       = size ,
                                color      = 'yellow',
                                scale_constraint =1,
                                localWorld = 0 ,
                                jnt        = 1 ,
                                drv        = 1)

        self.jawLwr2Jnt = jawLwr2_obj.jnt
        self.jawLwr2Ctrl = jawLwr2_obj.ctrl
        self.jawLwr2Gmbl = jawLwr2_obj.gmbl
        self.jawLwr2Zro = jawLwr2_obj.zro
        self.jawLwr2Ofst = jawLwr2_obj.ofst
        self.jawLwr2Drv = jawLwr2_obj.drv
        self.jawLwr2Pars = jawLwr2_obj.pars
        self.jawLwr2Grp = jawLwr2_obj.ctrlGrp

        self.jawLwrEndJnt = cn.joint( 'Jaw{}LwrEnd{}Jnt'.format(elem , side) , jawLwrEndTmpJnt )
        self.jawLwrEndJnt.parent(self.jawLwr2Jnt)
        self.jawLwrEndJnt.attr('ssc').v = 0

        for each in ( self.jawLwr1Jnt , self.jawLwr2Jnt , self.jawLwr1Ctrl , self.jawLwr2Ctrl ):
            each.setRotateOrder( 'zyx' )

        self.jawLwr1DrvMdv = cn.createNode('multiplyDivide' , 'Jaw{}Lwr1Drv{}Mdv'.format(elem , side))
        self.jawLwr1DrvPma = cn.createNode('plusMinusAverage' , 'Jaw{}Lwr1Drv{}Pma'.format(elem , side))
        self.jawLwr1DrvCmp = cn.createNode('clamp' , 'Jaw{}Lwr1Drv{}Cmp'.format(elem , side))

        self.jawTransformGrp = cn.createNode('transform' , 'Jaw{}Lwr1Transform{}Grp'.format(elem , side))
        mc.parentConstraint(self.jawLwr1Gmbl , self.jawTransformGrp , mo=False)
        mc.scaleConstraint(self.jawLwr1Gmbl , self.jawTransformGrp , mo=False)
        self.jawTransformGrp.parent(self.jawLwr1Grp)


        self.jawLwr1DrvCmp.attr('opr') >> self.jawLwr1DrvMdv.attr('i1x')
        self.jawLwr1DrvCmp.attr('opr') >> self.jawLwr1DrvMdv.attr('i1y')
        self.jawLwr1DrvCmp.attr('opr') >> self.jawLwr1DrvMdv.attr('i1z')
        self.jawLwr1DrvMdv.attr('ox') >> self.jawLwr2Drv.attr('rx')
        self.jawLwr1DrvMdv.attr('oy') >> self.jawLwr2Drv.attr('ty')
        self.jawLwr1DrvMdv.attr('oz') >> self.jawLwr2Drv.attr('tz')
        self.jawLwr1DrvPma.addAttr('constant')
        self.jawLwr1DrvPma.attr('constant').v = -(self.jawTransformGrp.attr('rx').getVal())
        self.jawLwr1DrvPma.attr('constant') >> self.jawLwr1DrvPma.attr('i1[0]')
        self.jawTransformGrp.attr('rx') >> self.jawLwr1DrvPma.attr('i1[1]')
        self.jawLwr1DrvPma.attr('o1') >> self.jawLwr1DrvCmp.attr('ipr')
        self.jawLwr1DrvCmp.attr('mxr').v = 100

        self.jawLwr1CtrlShape = core.Dag(self.jawLwr1Ctrl.shape)
        self.jawLwr1CtrlShape.addAttr('jawRx' , chanelBox = 0)
        self.jawLwr1CtrlShape.addAttr('jawTy' , chanelBox = 0)
        self.jawLwr1CtrlShape.addAttr('jawTz' , chanelBox = 0)
        self.jawLwr1CtrlShape.attr('jawRx') >> self.jawLwr1DrvMdv.attr('i1x')
        self.jawLwr1CtrlShape.attr('jawTy') >> self.jawLwr1DrvMdv.attr('i1y')
        self.jawLwr1CtrlShape.attr('jawTz') >> self.jawLwr1DrvMdv.attr('i1z')


        #-- Create JawUpr Rig
        jawUpr_obj = fkRig.Run( name       = 'Jaw{}Upr1'.format(elem) ,
                                side       = self.side ,
                                tmpJnt     = jawUpr1TmpJnt ,
                                parent     = parent ,
                                ctrlGrp    = ctrlGrp ,  
                                shape      = 'square' ,
                                jnt_ctrl   = 1  ,
                                size       = size ,
                                color      = 'red',
                                scale_constraint =1,
                                localWorld = 0 ,
                                jnt = 1 )

        self.jawUprJnt = jawUpr_obj.jnt
        self.jawUprCtrl = jawUpr_obj.ctrl
        self.jawUprGmbl = jawUpr_obj.gmbl
        self.jawUprZro = jawUpr_obj.zro
        self.jawUprOfst = jawUpr_obj.ofst
        self.jawUprPars = jawUpr_obj.pars
        self.jawUprGrp = jawUpr_obj.ctrlGrp

        self.jawUprEndJnt = cn.joint( 'Jaw{}UprEnd{}Jnt'.format(elem , side) , jawUprEndTmpJnt )
        self.jawUprEndJnt.parent(self.jawUprJnt)
        self.jawUprEndJnt.attr('ssc').v = 0

        for each in (self.jawUprGrp , self.jawUprZro):
            each.lockAttrs('tx','ty','tz','rx','ry','rz','sx','sy','sz')

        #-- Adjust Hierachy
        self.jawLwr2Grp.parent(self.jawLwr1Gmbl)
        mc.parent(self.jawLwr1Grp , self.jawUprGrp , self.jawCtrlGrp)
        mc.parent(self.jawCtrlGrp , ctrlGrp)
        mc.parentConstraint(parent , self.jawCtrlGrp,mo=1)
        mc.scaleConstraint(parent , self.jawCtrlGrp,mo=1)

        mc.select( cl = True )