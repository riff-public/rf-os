//Maya ASCII 2018ff09 scene
//Name: facialDir_skel_tmp.ma
//Last modified: Wed, Dec 15, 2021 04:41:33 PM
//Codeset: 1252
requires maya "2018ff09";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201903222215-65bada0e52";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "6C6520D0-4B17-FDE8-6CBA-F6814069558B";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.84320814931291554 8.3765708612992356 4.931163045831517 ;
	setAttr ".r" -type "double3" 3.8616472625994449 -11.800000000001667 -5.0769049483542233e-17 ;
	setAttr ".rp" -type "double3" -5.5511151231257827e-17 -1.7763568394002505e-15 0 ;
	setAttr ".rpt" -type "double3" -6.0471168025102384e-16 1.2398679963282753e-15 4.8159336524909672e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "014F48C2-4B4C-CE9F-7536-E28263E6CC4B";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 4.5802861673488033;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.091315701603889465 8.6850414276123047 0.45784968137741089 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "DD32CD22-40DB-69DA-AA13-EF926BCC9882";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.0022426313020622729 1000.1000667131312 0.36368903553653814 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "C5C07616-4A14-CDF1-8403-76A8F6C07758";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 991.36965946258636;
	setAttr ".ow" 1.0526315789473684;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" -0.0022426313020622729 8.7304072505449248 0.36368903553631804 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "75BA9182-41C8-CA08-59C9-69A99379185F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.13566823823727506 5.9915592489618872 1000.1041658418742 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "2712B234-4027-5CDD-596A-DF9BB8E97C3B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 999.95404135934291;
	setAttr ".ow" 0.58882273357000847;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 0 5.9299793394073799 0.15012448253127603 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "EFFAFADE-4B49-A1EE-34B4-FD8F892F1118";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1029955355383 8.6387566677481527 0.24274152635738838 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "F6E70646-4854-916E-FCFA-05894704DF4A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1029955355383;
	setAttr ".ow" 1.1069883853323041;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 0 8.6867497751334497 0.27527922627940149 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "TmpJnt_Grp";
	rename -uid "6BA8E516-4F28-6B06-1830-409B9EC07C43";
	setAttr ".t" -type "double3" 0 8.8209039615094742 -0.028715296486824041 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
createNode joint -n "JawLwr1_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "B29614FE-4EC0-C2C5-8D8C-2D99B9767360";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -2.6680951781497485e-27 0.038142838238528043 0.12340445265692213 ;
	setAttr ".r" -type "double3" 31.983454365182002 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 25.105065775409837 0.43032997634803533 1;
	setAttr -k on -cb off ".radi" 0.01;
createNode joint -n "JawLwr2_TmpJnt" -p "JawLwr1_TmpJnt";
	rename -uid "4875664B-4929-8E37-A6EE-B9A5B5A52DF6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 1.8088038343442083e-27 -0.082766831424757825 0.13445919201355316 ;
	setAttr ".r" -type "double3" 29.850729625274738 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -31.983454365182009 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 0.55021567872571864 1;
	setAttr -k on -cb off ".radi" 0.01;
createNode joint -n "JawLwrEnd_TmpJnt" -p "JawLwr2_TmpJnt";
	rename -uid "3C9AB848-4C99-025C-0016-1589F09E5E85";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 4.9331013663932883e-28 2.6645352591003757e-15 0.3758002041935673 ;
	setAttr ".r" -type "double3" 29.850729625274738 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -29.850729625274738 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 24.169010636166544 2.0776527139297967 1;
	setAttr -k on -cb off ".radi" 0.01;
createNode joint -n "tongue1_TmpJnt" -p "JawLwr1_TmpJnt";
	rename -uid "93527497-4514-E7DB-A030-2E9324B85B54";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.5873240686715317e-43 -0.17585999557750398 0.20150073825769699 ;
	setAttr ".r" -type "double3" -27.149135150602856 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 44.78303139994113 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.1293487851944074e-14 0.92496633972913433 0.380049036794053 0 5.3422893411477993e-14 -0.38004903679405405 0.92496633972913445 0
		 2.7578487462019971e-12 2.1020391679035786 43.517308688170466 1;
	setAttr ".radi" 0.01;
createNode joint -n "tongue2_TmpJnt" -p "tongue1_TmpJnt";
	rename -uid "E7CA0EA2-48A6-A3E4-FA99-C5ABC0619315";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.5873240686715317e-43 0.056 -2.4012291837243432e-16 ;
	setAttr ".r" -type "double3" -10.547260713076517 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 11.444396191580532 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.1293487851944074e-14 0.92496633972913433 0.380049036794053 0 5.3422893411477993e-14 -0.38004903679405405 0.92496633972913445 0
		 2.7578487462019971e-12 2.1020391679035786 43.517308688170466 1;
	setAttr ".radi" 0.01;
createNode joint -n "tongue3_TmpJnt" -p "tongue2_TmpJnt";
	rename -uid "FD97CCAA-4160-4B2D-5913-098E067EE64F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.5873240686715317e-43 0.056 -2.4012291837243432e-16 ;
	setAttr ".r" -type "double3" 26.070798424064598 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 11.444396191580532 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.1293487851944074e-14 0.92496633972913433 0.380049036794053 0 5.3422893411477993e-14 -0.38004903679405405 0.92496633972913445 0
		 2.7578487462019971e-12 2.1020391679035786 43.517308688170466 1;
	setAttr ".radi" 0.01;
createNode joint -n "tongue4_TmpJnt" -p "tongue3_TmpJnt";
	rename -uid "D9758B96-442A-6AAF-B498-C4B60CA596DA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.5873240686715317e-43 0.056 -2.4012291837243432e-16 ;
	setAttr ".r" -type "double3" 0.32924677141760628 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 11.444396191580532 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.1293487851944074e-14 0.92496633972913433 0.380049036794053 0 5.3422893411477993e-14 -0.38004903679405405 0.92496633972913445 0
		 2.7578487462019971e-12 2.1020391679035786 43.517308688170466 1;
	setAttr ".radi" 0.01;
createNode joint -n "tongue5_TmpJnt" -p "tongue4_TmpJnt";
	rename -uid "44A6BBBC-4FF6-1A97-B366-DCBCD6EEE4B0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.5873240686715317e-43 0.056 -2.4012291837243432e-16 ;
	setAttr ".r" -type "double3" -9.4161249149775799 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 11.444396191580532 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.1293487851944074e-14 0.92496633972913433 0.380049036794053 0 5.3422893411477993e-14 -0.38004903679405405 0.92496633972913445 0
		 2.7578487462019971e-12 2.1020391679035786 43.517308688170466 1;
	setAttr ".radi" 0.01;
createNode joint -n "tongue6_TmpJnt" -p "tongue5_TmpJnt";
	rename -uid "F5D034B5-4FEE-8F4C-61DE-57B6C871D4E5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.5873240686715317e-43 0.056 -2.4012291837243432e-16 ;
	setAttr ".r" -type "double3" -11.444396191580546 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 11.444396191580532 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.1293487851944074e-14 0.92496633972913433 0.380049036794053 0 5.3422893411477993e-14 -0.38004903679405405 0.92496633972913445 0
		 2.7578487462019971e-12 2.1020391679035786 43.517308688170466 1;
	setAttr ".radi" 0.01;
createNode joint -n "JawUpr1_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "C59E6C57-4E26-260F-7302-1F984C3D9AA1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 3.9277861924872762e-18 -0.02738352506227848 0.46423121746059287 ;
	setAttr ".r" -type "double3" -33.743971626517265 3.5311250384401278e-31 180 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-16 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0 24.387496966804516 0.55021567872571864 1;
	setAttr -k on -cb off ".radi" 0.01;
createNode joint -n "JawUpr2_TmpJnt" -p "JawUpr1_TmpJnt";
	rename -uid "82528E28-4543-2C64-CB0D-498EAA51366C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 9.0635036857283008e-18 -2.6645352591003757e-15 0.13323388438741546 ;
	setAttr ".r" -type "double3" -33.743971626517265 3.5311250384401278e-31 180 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -33.743971626517265 -3.8976609553334635e-15 -180 ;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-16 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 -3.2163030071266839e-32 24.387496966804516 2.0776527139297962 1;
	setAttr -k on -cb off ".radi" 0.01;
createNode joint -n "EyeTrgt_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "5D4B92E7-4AEA-D5E9-654C-3F87E6B95D36";
	setAttr ".t" -type "double3" 1.6913997496342814e-05 0.24136143460868986 1.9755646538661793 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeTrgt_L_TmpJnt" -p "EyeTrgt_TmpJnt";
	rename -uid "D8837D82-4EC2-F4C4-24C8-57968A731E91";
	setAttr ".t" -type "double3" 0.13505408049884929 1.7763568394002505e-15 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeTrgt_R_TmpJnt" -p "EyeTrgt_TmpJnt";
	rename -uid "8B596E14-4B8E-5589-78DD-7ABE72D315D0";
	setAttr ".t" -type "double3" -0.13508792339500317 1.7763568394002505e-15 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.01;
createNode joint -n "Eye_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "D605AB78-408A-ECA0-FF81-4F888F825C9E";
	setAttr ".t" -type "double3" -0.12658336479216814 0.21789389386772307 0.35831180105587784 ;
	setAttr ".r" -type "double3" 7.4130216494859278 12.043279718259079 2.0325832837916537e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -30.000000000000011 0 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeInner_R_TmpJnt" -p "Eye_R_TmpJnt";
	rename -uid "01D5F912-4394-F5FF-ED6A-D68A90F2AB4F";
	setAttr ".t" -type "double3" 0.071274791516361916 -0.024473932254124975 0.13430746498032575 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999987206346 24.9999972270219 3.0671954935609063e-07 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeOuter_R_TmpJnt" -p "Eye_R_TmpJnt";
	rename -uid "69C4762B-496B-05E6-89EA-798469EDD7C9";
	setAttr ".t" -type "double3" -0.1060325780028722 -0.0036948899233610888 0.10877183555502634 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999984863814 -40.000002772978185 4.5808135522004765e-07 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidUpr_R_TmpJnt" -p "Eye_R_TmpJnt";
	rename -uid "E57792D6-46AC-4555-2780-D8A088D4B988";
	setAttr ".t" -type "double3" 1.9146513644333063e-07 2.0502584803239188e-06 -8.6420569489487065e-07 ;
	setAttr ".r" -type "double3" -2.5239038574111223 4.7785737227539764 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999988405017 -2.7729781444474603e-06 3.6078784595282584e-07 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidUpr1_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "C91F1CE9-4E51-4A94-6EB4-6CAAA8276EB7";
	setAttr ".t" -type "double3" 0.0536925984135046 -0.00079012244375498142 -0.14390407829915719 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 2.0913097867553089e-06 -19.999999999999993 1.3591588092127473e-14 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidUpr2_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "913F6743-456E-A238-1FF8-4298146459DE";
	setAttr ".t" -type "double3" 0.00085037877553681418 -0.001417631644429207 -0.15467515270869714 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.909095910416422e-06 -1.1922312930398962e-22 2.1195222160994587e-22 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidUpr3_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "9D273829-489E-1671-77F1-BE96A2DDEFCC";
	setAttr ".t" -type "double3" -0.049762331314629427 0.0013192697814954357 -0.14449702803302439 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.9090959022978687e-06 29.999999999999979 -3.0298853112808844e-14 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidUpr4_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "C243D933-44DB-541C-C41C-A880D41AB135";
	setAttr ".t" -type "double3" 0.060936401217415365 -0.012928019114013978 -0.14231921699284994 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -5.0000000000004299 -19.999999999999972 1.1740593474171946e-14 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidUpr5_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "303C0699-4D9D-1947-5610-A788277ACF24";
	setAttr ".t" -type "double3" 0.0033839276856741374 -0.023791956414408233 -0.15355746804500575 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -5.000000000000357 2.9975108551539556e-16 -2.9081944776103177e-16 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidUpr6_R_TmpJnt" -p "EyeLidUpr_R_TmpJnt";
	rename -uid "0DD65B7D-4B8C-9D84-2DA7-75A9E8C35EB6";
	setAttr ".t" -type "double3" -0.051803660747817892 -0.020132292841847033 -0.14196137187296815 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -5.0000000000003633 29.999999999999979 -2.9151168861555162e-14 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidLwr_R_TmpJnt" -p "Eye_R_TmpJnt";
	rename -uid "EF3150D5-40B2-97D5-50D5-54A5772B2E36";
	setAttr ".t" -type "double3" 1.9146513644333063e-07 2.0502584803239188e-06 -8.6420569489487065e-07 ;
	setAttr ".r" -type "double3" -11.465381190066205 -5.7208387264084122 -180 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999988405017 -2.7729781444474603e-06 3.6078784595282584e-07 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidLwr1_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "872121E0-4230-386A-D6A8-65B01F0CF71C";
	setAttr ".t" -type "double3" -0.042736555987589075 0.0051453643103887003 -0.14634582659383533 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 2.9531017770383431e-15 22.499999999999986 1.4846245187971893e-14 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidLwr2_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "092F87C8-4DEB-07BA-8EF5-8EB645F6F304";
	setAttr ".t" -type "double3" 0.0056310208535914341 0.0035383401646544854 -0.15502035205103049 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidLwr3_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "E1C029E0-4B51-7ECC-D81C-098F6C5FF2A4";
	setAttr ".t" -type "double3" 0.055457899052087811 0.0077048238128050173 -0.1420879124899157 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4460350561588427e-15 -24.999999999999989 -2.4565476639057368e-14 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidLwr4_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "B6EE8734-4A36-50B1-2B63-43AEF141EE12";
	setAttr ".t" -type "double3" -0.042271628141717366 -0.004211693065997224 -0.14823808036994457 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 2.9531017770383431e-15 22.499999999999986 1.4846245187971893e-14 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidLwr5_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "4A43773C-448F-17B9-4CBA-3196BF86C641";
	setAttr ".t" -type "double3" 0.0078531696766043213 -0.0077616936817452853 -0.15658403030392698 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidLwr6_R_TmpJnt" -p "EyeLidLwr_R_TmpJnt";
	rename -uid "EC4D3A9B-44E1-1744-D1F4-2C82F8AEC211";
	setAttr ".t" -type "double3" 0.06015475249932295 -0.0041698464666506396 -0.14426481337176345 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4460350561588427e-15 -24.999999999999989 -2.4565476639057368e-14 ;
	setAttr ".radi" 0.01;
createNode joint -n "Eye_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "7D1CB30E-40EC-0913-0B5D-5BBB97C00057";
	setAttr ".t" -type "double3" 0.12658338434994221 0.21789389386772307 0.35831179360529725 ;
	setAttr ".r" -type "double3" 7.4130215335360496 -12.043276921908669 1.0162916313137204e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 30.000000000000011 0 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeInner_L_TmpJnt" -p "Eye_L_TmpJnt";
	rename -uid "B9E5A293-4909-56BA-CE50-8B9E8065B834";
	setAttr ".t" -type "double3" -0.071274711255076048 -0.02447016402255997 0.13430676815569631 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -24.999999999999996 0 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeOuter_L_TmpJnt" -p "Eye_L_TmpJnt";
	rename -uid "274E87F7-478F-44FD-139F-4BAF3622D4B7";
	setAttr ".t" -type "double3" 0.10603276269457572 -0.0036939424459774273 0.10877138485893501 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 40.000000000000057 0 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidUpr_L_TmpJnt" -p "Eye_L_TmpJnt";
	rename -uid "BC1CF2B7-4D14-3CFE-BE5C-8997074991D3";
	setAttr ".t" -type "double3" -8.6736173798840355e-17 0 2.2204460492503131e-16 ;
	setAttr ".r" -type "double3" -2.5239038574111223 4.7785737227539764 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidUpr1_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "894C34B8-4249-8882-3C03-E9B684B0275A";
	setAttr ".t" -type "double3" -0.053693086382445979 0.00079095584871602398 0.14390339157911486 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -20 0 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidUpr2_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "C10A42BE-4207-EE9B-992A-25BC9EB1BD00";
	setAttr ".t" -type "double3" -0.00085073222506952539 0.0014150615440513548 0.15467499488439834 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidUpr3_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "2F95E7B9-4969-2533-25E4-7DA5B31C439A";
	setAttr ".t" -type "double3" 0.049761827078188839 -0.0013217135879060038 0.14449698185326471 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 29.999999999999996 0 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidUpr4_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "A817D8CA-430A-66BD-1561-C9B84756E3DA";
	setAttr ".t" -type "double3" -0.060936757624818413 0.012926736600027411 0.14231840988147904 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.9999999999999991 -19.999999999999996 -1.0577111237992746e-16 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidUpr5_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "9C704B69-4BF0-1437-70C9-608152C1A4FE";
	setAttr ".t" -type "double3" -0.003383938522143623 0.023792076510623872 0.15355701524688975 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -5 0 0 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidUpr6_L_TmpJnt" -p "EyeLidUpr_L_TmpJnt";
	rename -uid "8C56B7AB-48CE-4597-D468-DEAA16309B01";
	setAttr ".t" -type "double3" 0.051803095771682225 0.020138650533478852 0.14196044775304706 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -5.0000000000000018 29.999999999999996 2.295367626894107e-16 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidLwr_L_TmpJnt" -p "Eye_L_TmpJnt";
	rename -uid "AAAC1EBD-4D88-C410-1D71-80A7CD5B3555";
	setAttr ".t" -type "double3" -2.3939183968479938e-16 -1.7763568394002505e-15 1.1102230246251565e-16 ;
	setAttr ".r" -type "double3" -11.465381190066205 -5.7208387264084122 -180 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidLwr1_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "EE44E47C-4F2B-3A66-C7A7-EA93885B1F8D";
	setAttr ".t" -type "double3" 0.042736613437774698 -0.0051444490181289382 0.14634505149197174 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 22.5 0 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidLwr2_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "98C61B26-4E86-0B2F-B4ED-FDA120B197B3";
	setAttr ".t" -type "double3" -0.0056304545613140716 -0.0035408875629538983 0.15501868503062655 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidLwr3_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "C7E4B306-4F64-DE30-D8D9-1DA33013ADC5";
	setAttr ".t" -type "double3" -0.055457389646409491 -0.0077043020342912882 0.14208766586137767 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -25 0 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidLwr4_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "E3632CD5-4F65-69ED-EB9B-F3A0621E1BEB";
	setAttr ".t" -type "double3" 0.042271334694353471 0.0042120267116381171 0.14823746161713958 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 22.5 0 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidLwr5_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "CB9DDB46-4615-D68A-9145-40A6A9833347";
	setAttr ".t" -type "double3" -0.0078532414718234383 0.0077595787768558466 0.15658232052151488 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeLidLwr6_L_TmpJnt" -p "EyeLidLwr_L_TmpJnt";
	rename -uid "8D8E8178-4D4F-072C-EDC8-7888FADE196E";
	setAttr ".t" -type "double3" -0.060154384883599574 0.0041702823876157424 0.1442645792321211 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 -25 0 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeBrowMid_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "B1343A3D-4D34-26B8-BC6E-A8B192D65463";
	setAttr ".t" -type "double3" 0.20292416318734963 0.30488264450461422 0.51916796269562293 ;
	setAttr ".r" -type "double3" 0 30.495399750307595 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeBrowInner_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "DE7AE583-481F-5134-7A3D-CE923319181A";
	setAttr ".t" -type "double3" 0.091578087716260928 0.29450355403401218 0.5847437722777169 ;
	setAttr ".r" -type "double3" 0 19.403308982493463 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeBrowOuter_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "3BA5B68C-4A12-103C-8B96-F78E2ACAA523";
	setAttr ".t" -type "double3" 0.30577337741851807 0.27902244314966573 0.3803891379632644 ;
	setAttr ".r" -type "double3" 0 70.094592380326432 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeBrow_C_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "97798EF6-4839-4909-A875-9198EE012CCC";
	setAttr ".t" -type "double3" 0 0.28881572979243231 0.58721510956665779 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeBrowMid_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "C39DDEBD-4B63-6B45-0DBA-E3831DD53B2F";
	setAttr ".t" -type "double3" -0.202924 0.30488603849052609 0.51916829648682394 ;
	setAttr ".r" -type "double3" 180 210.4953997503076 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 0 0 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeBrowInner_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "80D7F373-42F8-922E-1EA3-36ABDF05A665";
	setAttr ".t" -type "double3" -0.091578099999999996 0.29450603849052648 0.58474329648682377 ;
	setAttr ".r" -type "double3" 180 199.40330898249348 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 0 0 ;
	setAttr ".radi" 0.01;
createNode joint -n "EyeBrowOuter_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "8734C26C-4BAF-97C9-DFE7-B6AB6B4E387A";
	setAttr ".t" -type "double3" -0.305773 0.27902603849052632 0.38038929648682396 ;
	setAttr ".r" -type "double3" 180.00000000000003 250.09459238032645 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 0 0 ;
	setAttr ".radi" 0.01;
createNode joint -n "cheekMain_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "1F0ADBA6-4715-145C-BC90-45BA0107D9FF";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.2769999999999998 -0.022999999999999687 0.36200000000000015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.3571629951904127 0 0 ;
	setAttr ".bps" -type "matrix" 0.30059000016696891 0.78149195566352603 0.54673208707082366 0
		 -0.93333913514334965 0.35899590361154021 1.2381588809784461e-16 0 -0.19627457963141395 -0.51028645330180111 0.83730760474701238 0
		 6.6990529801024516 -0.0097290463850563391 43.888290904269802 1;
	setAttr ".radi" 0.01;
createNode joint -n "cheekMain_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "C25E2D9B-4990-66DF-23B8-C6A3CA21CCC5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.2769999999999998 -0.022999999999999687 0.36200000000000015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.3571629951904114 180.00000000000003 0 ;
	setAttr ".bps" -type "matrix" 0.30059000016697568 -0.78149195566351159 -0.54673208707084009 0
		 -0.93333913514334965 -0.35899590361153993 -2.1260120400268256e-14 0 -0.19627457963140341 0.51028645330182298 -0.83730760474700139 0
		 -6.6990536300000096 -0.0097290020000073696 43.888298849999956 1;
	setAttr ".radi" 0.01;
createNode joint -n "teethMainUp_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "8C365AB9-4041-5EDA-0B61-589CD2E6A006";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.0154449495310571e-28 -0.10642742245951453 0.44378631300479876 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 88.642837004809607 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957452e-14 0.8771178665751479 0.48027517959463645 0 5.0732167128880986e-14 -0.48027517959463778 0.87711786657514823 0
		 0.01592607367873344 0.12501306961924419 46.465345148587645 1;
	setAttr ".radi" 0.01;
createNode joint -n "teethUp3_TmpJnt" -p "teethMainUp_TmpJnt";
	rename -uid "66256226-473D-605B-3A2A-03AF323B858F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0.056693267500588548 -1.8719292609048461e-15 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957436e-14 0.87711786657514745 0.48027517959463617 0 5.0732167128881012e-14 -0.480275179594638 0.87711786657514867 0
		 2.7804598901060018e-12 -5.1175310879145268 43.594734558218917 1;
	setAttr ".radi" 0.01;
createNode joint -n "teethUp2_TmpJnt" -p "teethMainUp_TmpJnt";
	rename -uid "4F9E36A9-41A2-9305-1B0F-DAB62311E7F3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.072 0.019 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -47.468722969008596 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957436e-14 0.87711786657514745 0.48027517959463617 0 5.0732167128881012e-14 -0.480275179594638 0.87711786657514867 0
		 4.4033707420688959 -0.012855669558010679 46.389853659938275 1;
	setAttr ".radi" 0.01;
createNode joint -n "teethUp1_TmpJnt" -p "teethMainUp_TmpJnt";
	rename -uid "244CACB2-4871-D7C9-FB9D-A8A30F2B1AE4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.10783959799637235 -0.064 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -69.396937989093885 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957436e-14 0.87711786657514745 0.48027517959463617 0 5.0732167128881012e-14 -0.480275179594638 0.87711786657514867 0
		 4.4033707420688959 -0.012855669558010679 46.389853659938275 1;
	setAttr ".radi" 0.01;
createNode joint -n "teethUp4_TmpJnt" -p "teethMainUp_TmpJnt";
	rename -uid "298F04A8-48CA-6827-C9B0-F7962C708C39";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.072 0.019 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 0 47.468722969008596 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957436e-14 0.87711786657514745 0.48027517959463617 0 5.0732167128881012e-14 -0.480275179594638 0.87711786657514867 0
		 4.4033707420688959 -0.012855669558010679 46.389853659938275 1;
	setAttr ".radi" 0.01;
createNode joint -n "teethUp5_TmpJnt" -p "teethMainUp_TmpJnt";
	rename -uid "FE56F878-4F49-A473-6258-48A50ADE735A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.108 -0.064 0 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 0 69.396937989093885 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957436e-14 0.87711786657514745 0.48027517959463617 0 5.0732167128881012e-14 -0.480275179594638 0.87711786657514867 0
		 4.4033707420688959 -0.012855669558010679 46.389853659938275 1;
	setAttr ".radi" 0.01;
createNode joint -n "teethMainDn_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "3A135FF3-48B2-9727-703C-35964B8CBFFC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.0154449495310571e-28 -0.15335729912367846 0.39313701002743673 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1.0000000000000002 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 91.647594484837725 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.9511115583222014e-14 0.85283227421620023 0.52218494047149933 0 4.9361039518923608e-14 -0.52218494047150066 0.85283227421620034 0
		 -1.8521245151397982e-09 2.045740909568214 43.000912267151918 1;
	setAttr ".radi" 0.01;
createNode joint -n "teethDn3_TmpJnt" -p "teethMainDn_TmpJnt";
	rename -uid "743FE92A-4106-D50C-717D-2A84A2AE732D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.017869788627737e-43 0.095290173798464398 -2.1521684174380337e-15 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000002 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 3.8632187485347138e-14 0.7336441155904575 0.6795338929439011 0 4.2602491188333267e-14 -0.67953389294390298 0.7336441155904585 0
		 -1.8521245151398003e-09 -2.7053269250402261 40.091857169921767 1;
	setAttr ".radi" 0.01;
createNode joint -n "teethDn2_TmpJnt" -p "teethMainDn_TmpJnt";
	rename -uid "D85D2C20-4C35-913E-F819-E98095BE9D97";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.067 0.065 -0.002 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -46.204473895803758 ;
	setAttr ".bps" -type "matrix" 0.97478167878208011 -0.19031861039197456 -0.11653113424856819 0
		 0.22316065672691957 0.83132527598001194 0.50901631290751892 0 4.9361039518923627e-14 -0.52218494047150088 0.85283227421620067 0
		 4.534801201274111 2.0316239137271781 42.992268501131633 1;
	setAttr ".radi" 0.01;
createNode joint -n "teethDn1_TmpJnt" -p "teethMainDn_TmpJnt";
	rename -uid "2E7F699E-4911-AE22-6D86-91839BFC0D56";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.097 -0.03 0.003 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -60.988217253353504 ;
	setAttr ".bps" -type "matrix" 0.97478167878208011 -0.19031861039197456 -0.11653113424856819 0
		 0.22316065672691957 0.83132527598001194 0.50901631290751892 0 4.9361039518923627e-14 -0.52218494047150088 0.85283227421620067 0
		 4.534801201274111 2.0316239137271781 42.992268501131633 1;
	setAttr ".radi" 0.01;
createNode joint -n "teethDn4_TmpJnt" -p "teethMainDn_TmpJnt";
	rename -uid "B909F13A-4BEC-105F-18E8-19BA4D2F8181";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.067 0.065 -0.002 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 0 46.204473895803758 ;
	setAttr ".bps" -type "matrix" 0.97478167878208011 -0.19031861039197456 -0.11653113424856819 0
		 0.22316065672691957 0.83132527598001194 0.50901631290751892 0 4.9361039518923627e-14 -0.52218494047150088 0.85283227421620067 0
		 4.534801201274111 2.0316239137271781 42.992268501131633 1;
	setAttr ".radi" 0.01;
createNode joint -n "teethDn5_TmpJnt" -p "teethMainDn_TmpJnt";
	rename -uid "527409D8-4DF4-00EF-5AE4-C08446A8A267";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.097 -0.03 0.003 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -180 0 60.988217253353504 ;
	setAttr ".bps" -type "matrix" 0.97478167878208011 -0.19031861039197456 -0.11653113424856819 0
		 0.22316065672691957 0.83132527598001194 0.50901631290751892 0 4.9361039518923627e-14 -0.52218494047150088 0.85283227421620067 0
		 4.534801201274111 2.0316239137271781 42.992268501131633 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipUp1_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "4D660E12-4020-AC3B-2F77-66A5B3DC6472";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.046999999999999958 -0.11700000000000088 0.56100000000000028 ;
	setAttr ".r" -type "double3" 0 -8.886 0 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1.0000000000000002 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.7535069086380073 39.283808946973714 -1.1104619406885787 ;
	setAttr ".bps" -type "matrix" 0.82995227039268116 0.46635673384895515 0.30608924460648446 0
		 -0.55006845717557218 0.77543132774191237 0.3100499127187446 0 -0.092757324727058291 -0.42569666753470092 0.90009912007474502 0
		 2.3731795743456856 -5.0930367687478064 41.718719177983019 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipUp2_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "886A696A-4FC6-7990-A5B4-FCA4D0CC42F0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.084999999999999937 -0.13000000000000078 0.51600000000000024 ;
	setAttr ".r" -type "double3" 0 21.748 0 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.5152082576348622 26.399624047253205 -0.67383201961001171 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5812682868491317 -3.6850020464801649 42.372932534218236 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipDn1_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "48B9F348-48FE-36F0-26E3-84BE282C96CB";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.047999999999999959 -0.1509999999999998 0.54200000000000026 ;
	setAttr ".r" -type "double3" 0 3.7280000000000006 0 ;
	setAttr ".s" -type "double3" 0.99999999999999867 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.8517675568402741 35.694779611265581 -2.991773865439384 ;
	setAttr ".bps" -type "matrix" 0.79760097970714605 0.49443431716630121 0.34549585117406678 0
		 -0.60292369248800015 0.63664164337077689 0.48080187079793268 0 0.017767898211013058 -0.59179567752261442 0.80589216267358887 0
		 2.392434881244601 -3.7081886358561831 40.044019820819173 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipDn2_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "538043E7-4913-6103-91F1-C883BB1FBB18";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.083999999999999936 -0.14100000000000001 0.50900000000000023 ;
	setAttr ".r" -type "double3" 0 13.743 0 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.7246488442724341 38.097067798011004 -1.0642998176610465 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5258731406293524 -2.4182823207010311 41.191899026875838 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipCnr_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "86796760-46CB-0FBD-8E2E-10B5D0CF3E11";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.1059999999999999 -0.13700000000000045 0.48300000000000021 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.7492758872254055 39.114141599327205 -1.1037674574758292 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5812682868491317 -3.6850020464801649 42.372932534218236 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipMainUp_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "48E2A221-40C8-6540-D2F5-D28907C03E86";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.11849135622382967 0.57876531670471942 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1.0000000000000002 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.3571629951904127 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.3929716163367511e-14 0.90487439190602448 0.4256786756142501 0 5.2295189546417125e-14 -0.42567867561425116 0.90487439190602459 0
		 0.0030707051091594622 -6.0628083042306447 41.178591889943434 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipMainDn_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "395985A6-4B41-B030-A17F-9A86C1EA0333";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.15669841036201326 0.55719666073700702 ;
	setAttr ".r" -type "double3" 0 -1.8391287186326793e-14 5.6294003190949772e-15 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1.0000000000000002 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.3571629951904127 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 3.2042934045430673e-14 0.8244659544551155 0.56591155664504211 0 4.7756345579568861e-14 -0.56591155664504367 0.82446595445511561 0
		 0.0030707051091592406 -4.3823947943021277 39.530465108334667 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipUp1_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "2B0CEB80-473E-9FB2-EBFB-C0927BDB5E13";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.046999999999999958 -0.11700000000000088 0.56100000000000028 ;
	setAttr ".r" -type "double3" 0 188.88600000000002 0 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1.0000000000000002 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.7535069086381421 -39.2838089469737 1.1104619406886684 ;
	setAttr ".bps" -type "matrix" 0.82995227039268116 0.46635673384895515 0.30608924460648446 0
		 -0.55006845717557218 0.77543132774191237 0.3100499127187446 0 -0.092757324727058291 -0.42569666753470092 0.90009912007474502 0
		 2.3731795743456856 -5.0930367687478064 41.718719177983019 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipUp2_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "3D27FEE3-4EA5-8B5F-D8DB-F887BA01F9F9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.084999999999999937 -0.13000000000000078 0.51600000000000024 ;
	setAttr ".r" -type "double3" 0 158.25200000000004 0 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.5152082576349761 -26.399624047253191 0.67383201961006522 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5812682868491317 -3.6850020464801649 42.372932534218236 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipDn1_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "452528E3-4ACD-4CB3-D897-9FAF037785C4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.047999999999999959 -0.1509999999999998 0.54200000000000026 ;
	setAttr ".r" -type "double3" 0 176.272 0 ;
	setAttr ".s" -type "double3" 0.99999999999999867 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.8517675568404206 -35.694779611265602 2.9917738654394772 ;
	setAttr ".bps" -type "matrix" 0.79760097970714605 0.49443431716630121 0.34549585117406678 0
		 -0.60292369248800015 0.63664164337077689 0.48080187079793268 0 0.017767898211013058 -0.59179567752261442 0.80589216267358887 0
		 2.392434881244601 -3.7081886358561831 40.044019820819173 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipDn2_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "69C6B0E1-44DA-B8FD-0FA6-96924E593BB3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.083999999999999936 -0.14100000000000001 0.50900000000000023 ;
	setAttr ".r" -type "double3" 0 166.257 0 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.7246488442725687 -38.097067798011039 1.0642998176610343 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5258731406293524 -2.4182823207010311 41.191899026875838 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipCnr_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "84919E06-464D-A062-FAD0-BE810650696F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.1059999999999999 -0.13700000000000045 0.48300000000000021 ;
	setAttr ".r" -type "double3" 0 360 0 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.7492758872253988 140.8858584006727 1.1037674574758127 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5812682868491317 -3.6850020464801649 42.372932534218236 1;
	setAttr ".radi" 0.01;
createNode joint -n "cheekDtl1_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "1B6C734E-468A-44C0-DA21-458F4EAA8B6C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.10250479727983475 -0.0531274722516617 0.52478952596565998 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.3571629951904127 0 0 ;
	setAttr ".bps" -type "matrix" 0.30059000016696891 0.78149195566352603 0.54673208707082366 0
		 -0.93333913514334965 0.35899590361154021 1.2381588809784461e-16 0 -0.19627457963141395 -0.51028645330180111 0.83730760474701238 0
		 6.6990529801024516 -0.0097290463850563391 43.888290904269802 1;
	setAttr ".radi" 0.01;
createNode joint -n "cheekDtl2_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "B7C98B66-4AEF-BF4A-BCEC-8EAC5D718C5C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.13546100258827209 -0.11722773775459139 0.4816593069829635 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.3571629951904127 0 0 ;
	setAttr ".bps" -type "matrix" 0.30059000016696891 0.78149195566352603 0.54673208707082366 0
		 -0.93333913514334965 0.35899590361154021 1.2381588809784461e-16 0 -0.19627457963141395 -0.51028645330180111 0.83730760474701238 0
		 6.6990529801024516 -0.0097290463850563391 43.888290904269802 1;
	setAttr ".radi" 0.01;
createNode joint -n "cheekDtl3_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "CEBA547C-4E25-A330-08F2-8092A87AEA60";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.12086276710033417 -0.18310469850898592 0.464106126526802 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.3571629951904127 0 0 ;
	setAttr ".bps" -type "matrix" 0.30059000016696891 0.78149195566352603 0.54673208707082366 0
		 -0.93333913514334965 0.35899590361154021 1.2381588809784461e-16 0 -0.19627457963141395 -0.51028645330180111 0.83730760474701238 0
		 6.6990529801024516 -0.0097290463850563391 43.888290904269802 1;
	setAttr ".radi" 0.01;
createNode joint -n "cheekDtl1_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "BD5D60A9-492E-76A0-0338-F59908E88668";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.1029999999999999 -0.0531274722516617 0.52478952596565998 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.3571629951904114 180.00000000000003 0 ;
	setAttr ".bps" -type "matrix" 0.30059000016696891 0.78149195566352603 0.54673208707082366 0
		 -0.93333913514334965 0.35899590361154021 1.2381588809784461e-16 0 -0.19627457963141395 -0.51028645330180111 0.83730760474701238 0
		 6.6990529801024516 -0.0097290463850563391 43.888290904269802 1;
	setAttr ".radi" 0.01;
createNode joint -n "cheekDtl2_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "2B30E3CF-437F-08E8-B5A4-43B3DE66CE9E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.1349999999999999 -0.11722773775459139 0.48165930698296355 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.3571629951904114 180.00000000000003 0 ;
	setAttr ".bps" -type "matrix" 0.30059000016696891 0.78149195566352603 0.54673208707082366 0
		 -0.93333913514334965 0.35899590361154021 1.2381588809784461e-16 0 -0.19627457963141395 -0.51028645330180111 0.83730760474701238 0
		 6.6990529801024516 -0.0097290463850563391 43.888290904269802 1;
	setAttr ".radi" 0.01;
createNode joint -n "cheekDtl3_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "95C401DE-41A2-07B4-6FE8-31B9C593456F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.12099999999999989 -0.18310469850898592 0.46410612652680205 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.3571629951904114 180.00000000000003 0 ;
	setAttr ".bps" -type "matrix" 0.30059000016696891 0.78149195566352603 0.54673208707082366 0
		 -0.93333913514334965 0.35899590361154021 1.2381588809784461e-16 0 -0.19627457963141395 -0.51028645330180111 0.83730760474701238 0
		 6.6990529801024516 -0.0097290463850563391 43.888290904269802 1;
	setAttr ".radi" 0.01;
createNode joint -n "cheekUpr1_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "812EA556-40FE-F590-F725-8480BBB4F565";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.12268289178609848 0.12216168180107267 0.51296336839577472 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.3571629951904127 0 0 ;
	setAttr ".bps" -type "matrix" 0.91228105091926848 0.40956474962285894 -9.9746599868666408e-18 0
		 -0.40956474962285994 0.91228105091926825 3.4694469519536142e-18 0 8.8901712446213503e-17 -4.5970172113385388e-17 1.0000000000000002 0
		 3.9427188542344718 -5.3237105726561431 45.022308497181172 1;
	setAttr ".radi" 0.01;
createNode joint -n "cheekUpr2_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "9D2E5529-42EE-9485-32C2-D5BD296BC920";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.20383092761039734 0.11221962705253752 0.48630817125222009 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.3571629951904127 0 0 ;
	setAttr ".bps" -type "matrix" 0.91228105091926848 0.40956474962285894 -9.9746599868666408e-18 0
		 -0.40956474962285994 0.91228105091926825 3.4694469519536142e-18 0 8.8901712446213503e-17 -4.5970172113385388e-17 1.0000000000000002 0
		 3.9427188542344718 -5.3237105726561431 45.022308497181172 1;
	setAttr ".radi" 0.01;
createNode joint -n "cheekUpr3_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "A911A632-4843-6D12-2C75-EA9B9904F7D3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.27451804280281067 0.14169865384697111 0.42359475682160175 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.3571629951904127 0 0 ;
	setAttr ".bps" -type "matrix" 0.91228105091926848 0.40956474962285894 -9.9746599868666408e-18 0
		 -0.40956474962285994 0.91228105091926825 3.4694469519536142e-18 0 8.8901712446213503e-17 -4.5970172113385388e-17 1.0000000000000002 0
		 3.9427188542344718 -5.3237105726561431 45.022308497181172 1;
	setAttr ".radi" 0.01;
createNode joint -n "cheekUpr1_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "39541A7E-43F9-8893-AFD9-DF8758A15B47";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.12299999999999989 0.12216168180107267 0.51296336839577472 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.3571629951904114 180.00000000000003 0 ;
	setAttr ".bps" -type "matrix" 0.91228105091926848 0.40956474962285894 -9.9746599868666408e-18 0
		 -0.40956474962285994 0.91228105091926825 3.4694469519536142e-18 0 8.8901712446213503e-17 -4.5970172113385388e-17 1.0000000000000002 0
		 3.9427188542344718 -5.3237105726561431 45.022308497181172 1;
	setAttr ".radi" 0.01;
createNode joint -n "cheekUpr2_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "F1DA97F5-453B-B599-FED5-FF9B46783281";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.20399999999999979 0.11221962705253752 0.48630817125222014 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.3571629951904114 180.00000000000003 0 ;
	setAttr ".bps" -type "matrix" 0.91228105091926848 0.40956474962285894 -9.9746599868666408e-18 0
		 -0.40956474962285994 0.91228105091926825 3.4694469519536142e-18 0 8.8901712446213503e-17 -4.5970172113385388e-17 1.0000000000000002 0
		 3.9427188542344718 -5.3237105726561431 45.022308497181172 1;
	setAttr ".radi" 0.01;
createNode joint -n "cheekUpr3_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "0F43D1AA-49ED-F2A7-E029-DC9FBE3C2850";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.2749999999999998 0.14169865384697111 0.42359475682160175 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.3571629951904114 180.00000000000003 0 ;
	setAttr ".bps" -type "matrix" 0.91228105091926848 0.40956474962285894 -9.9746599868666408e-18 0
		 -0.40956474962285994 0.91228105091926825 3.4694469519536142e-18 0 8.8901712446213503e-17 -4.5970172113385388e-17 1.0000000000000002 0
		 3.9427188542344718 -5.3237105726561431 45.022308497181172 1;
	setAttr ".radi" 0.01;
createNode joint -n "nose_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "917D7D41-4C43-D9BE-CF89-EB9EF4C851B6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.0037128086602183788 -0.0075012805791772053 0.61051899630440798 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.3571629951903994 0 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957452e-14 0.8771178665751479 0.48027517959463645 0 5.0732167128880986e-14 -0.48027517959463778 0.87711786657514823 0
		 0.01592607367873344 0.12501306961924419 46.465345148587645 1;
	setAttr ".radi" 0.01;
createNode joint -n "nose_L_TmpJnt" -p "nose_TmpJnt";
	rename -uid "31BBB4A8-46C9-BF1A-3A6E-DB8961040E11";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.05 -0.02 -0.039 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957436e-14 0.87711786657514745 0.48027517959463617 0 5.0732167128881012e-14 -0.480275179594638 0.87711786657514867 0
		 4.4033707420688959 -0.012855669558010679 46.389853659938275 1;
	setAttr ".radi" 0.01;
createNode joint -n "nose_R_TmpJnt" -p "nose_TmpJnt";
	rename -uid "20BEAD76-45BD-66F0-F888-329DEC65F3BE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.05 -0.02000000000000135 -0.038999999999999924 ;
	setAttr ".s" -type "double3" 1 0.99999999999999933 1.0000000000000004 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 180.00000000000003 0 ;
	setAttr ".bps" -type "matrix" 1.0000000000000002 5.7677653070061962e-16 -5.7428840619523691e-14 0
		 2.7086258208957436e-14 0.87711786657514745 0.48027517959463617 0 5.0732167128881012e-14 -0.480275179594638 0.87711786657514867 0
		 4.4033707420688959 -0.012855669558010679 46.389853659938275 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipUp3_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "D3DE0476-479D-7479-6D5E-CE8D3F7B8D5D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.102 -0.13629897465865559 0.4829833917325892 ;
	setAttr ".r" -type "double3" 0 21.748 0 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.5152082576348613 26.399624047253205 -0.67383201961001116 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5812682868491317 -3.6850020464801649 42.372932534218236 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipDn3_L_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "BA563889-4188-E591-E3FF-43BC4AE3E498";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.102 -0.13785681004227213 0.48302029902410515 ;
	setAttr ".r" -type "double3" 0 13.743 0 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.7246488442724341 38.097067798011004 -1.0642998176610465 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5258731406293524 -2.4182823207010311 41.191899026875838 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipUp3_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "8053D6FF-4105-999A-4188-8A86C1F1DA74";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.10200000000000002 -0.13629897465865559 0.48298339173258925 ;
	setAttr ".r" -type "double3" 0 158.25200000000004 0 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.5152082576349752 -26.39962404725318 0.67383201961006511 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5812682868491317 -3.6850020464801649 42.372932534218236 1;
	setAttr ".radi" 0.01;
createNode joint -n "lipDn3_R_TmpJnt" -p "TmpJnt_Grp";
	rename -uid "F354D2DC-4BF7-448B-E56B-E48912BEB669";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.102 -0.13785681004227213 0.48302029902410515 ;
	setAttr ".r" -type "double3" 0 166.257 0 ;
	setAttr ".s" -type "double3" 0.99999999999999889 1 1.0000000000000013 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.7246488442725687 -38.097067798011039 1.0642998176610345 ;
	setAttr ".bps" -type "matrix" 0.8185523695232757 0.45266987781597179 0.35364106105754067 0
		 -0.57195586300494139 0.58516454486653979 0.57484688935872963 0 0.053277660629052009 -0.67280936166882299 0.73789501538409097 0
		 4.5258731406293524 -2.4182823207010311 41.191899026875838 1;
	setAttr ".radi" 0.01;
createNode transform -n "Mouth_Nrb" -p "TmpJnt_Grp";
	rename -uid "1653D0E8-4DC8-75BE-D4CA-8CBF53E2408C";
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 0 0 1.3877787807814457e-17 ;
	setAttr ".s" -type "double3" 0.99999999999999911 0.99999999999999978 1.0000000000000004 ;
	setAttr ".rp" -type "double3" -0.0022426313020622911 0.018994530887974644 0.3089605541733304 ;
	setAttr ".sp" -type "double3" -0.0022426313020622933 0.018994530887974648 0.30896055417333035 ;
	setAttr ".spt" -type "double3" 2.1684043449710069e-18 -3.4694469519536134e-18 1.110223024625157e-16 ;
createNode nurbsSurface -n "Mouth_NrbShape" -p "Mouth_Nrb";
	rename -uid "BE13F48E-42E2-BCF4-649E-19913EC54928";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 0 no 
		12 0 0 0 0.875 1.75 2.3346621519999999 2.625 2.9660440100000001 3.2404370990000002
		 3.5 3.5 3.5
		15 0 0 0 1 2 3 4 5 6 7 8 9 10 10 10
		
		130
		0.21760155888028282 -0.36911270840381977 0.1002497897430983
		0.21383697901948695 -0.37410854050469378 0.12342574645403756
		0.20440924653346193 -0.38529400094000338 0.17086055946000581
		0.16355135005914959 -0.40076631374825727 0.24014569359680224
		0.12422387475171771 -0.41121920932151845 0.28850793942669173
		0.045733348388163841 -0.41955607960105495 0.3185743115810184
		0.0014272657612931971 -0.42181741375392828 0.32514398002697692
		-0.042948786580554496 -0.4193650740589156 0.31719925444970343
		-0.12361300597071913 -0.41110426282192181 0.28767260551890617
		-0.16463052091041322 -0.40092120239790707 0.24115381309692061
		-0.20892774686186688 -0.38553056491058563 0.17210429134299743
		-0.21719114919889593 -0.3742906326199496 0.12458733566830613
		-0.22143774991657439 -0.36928306492401064 0.10137393683610299
		0.21509696971769998 -0.33102406678429508 0.14369799501405586
		0.21138387414955864 -0.33399169499851006 0.17174829148570131
		0.20029807728726035 -0.35237765428028356 0.21297037263675486
		0.15901204674086689 -0.36816465284596744 0.29199308213149883
		0.11583760853501793 -0.38016606904447098 0.3360056766631499
		0.043436993836252158 -0.39002681907594366 0.36805669002451186
		0.0013931810775015946 -0.39408850993970079 0.37432709595611358
		-0.040740352700172192 -0.38959698338174376 0.36727733282451819
		-0.11581271801235718 -0.38070777298997299 0.33365420340526569
		-0.16013261816967855 -0.36768785017287559 0.29214910216233558
		-0.20501308359032597 -0.35259421800301061 0.21449853751697789
		-0.21483911916195014 -0.33411486417835934 0.17300119813535314
		-0.2190488020655367 -0.33112200819644549 0.14492736211741564
		0.21461171530240081 -0.27097015740139341 0.18970876468001735
		0.21020018024154596 -0.27706344356289797 0.21697725015263372
		0.19545800530791468 -0.28715896443638039 0.28926478244378828
		0.15474486172331942 -0.30134093495761849 0.36974698115836618
		0.10819468172043203 -0.30857131134402899 0.41571086034738708
		0.044573533605218353 -0.31559918055835001 0.44606313366717004
		0.0014775377966108325 -0.31850380807737583 0.45787149912369562
		-0.041879656642762116 -0.3155626940233579 0.44454147314481318
		-0.10813469539211081 -0.30860968310898074 0.41513972636144442
		-0.15612703416433543 -0.30127348763391498 0.36960524755089452
		-0.20052101445947462 -0.28707628951298503 0.29116691219541918
		-0.21396865105570056 -0.27710047043349811 0.21802766354045117
		-0.21884742692008291 -0.27091360615714055 0.19120014985996045
		0.21167595828129587 -0.21070965421942056 0.23835782221164575
		0.20608108323982349 -0.21544446713876358 0.27478352583014298
		0.19603642302629468 -0.22506861376323775 0.3338099073098581
		0.15435730393730077 -0.23671351680258088 0.42452375981210405
		0.10497220003843612 -0.24023778293109227 0.47462514209333412
		0.047254660162612989 -0.24450867880838101 0.50130922158953217
		0.0015837357962468599 -0.24547369753953813 0.51070030871527661
		-0.04445121008519308 -0.24452086152598759 0.50055603947773553
		-0.10495013733722844 -0.24030244506079459 0.4740674579961911
		-0.15581754979496104 -0.23682189174416907 0.42429496849180925
		-0.20142813342437607 -0.22518263962315274 0.33615774096270834
		-0.21011394906295236 -0.21546941503567787 0.27604670127965669
		-0.21623701659810632 -0.21073650201541227 0.23970916864835778
		0.20831095117286869 -0.15074555362269398 0.26880142513190097
		0.20245253271987818 -0.15329250938126709 0.30745672065301544
		0.19251413585155241 -0.15952175910726957 0.37165173562320469
		0.15231913231390393 -0.16849727075728083 0.46555542040097958
		0.10337286775857489 -0.17460494724869968 0.51127367364283205
		0.048682552148917736 -0.17857227347302213 0.53777821936467496
		0.0016474932730772111 -0.17920561989534245 0.5469376535349062
		-0.045807615650042703 -0.17858252196481494 0.53724131369778061
		-0.10334195788130331 -0.17463004504150789 0.51098519115513419
		-0.15381762919345066 -0.16854180705953525 0.46556463934197351
		-0.19810495664808092 -0.15955588692982381 0.37392927950920352
		-0.20659558176136661 -0.15328559144618117 0.30876635869405011
		-0.21300693083361638 -0.15074852777374112 0.27014295036926561
		0.20406641048921548 -0.080849487204295759 0.28985776605298308
		0.19821434409745725 -0.08229947091085936 0.32949543293913519
		0.18830059012490818 -0.086185146016161701 0.39616299417458306
		0.14986777159046127 -0.093183906700684091 0.49224927455093442
		0.10209335617364677 -0.099752814961222402 0.53550649362831804
		0.04900985392836156 -0.10328476325349162 0.56139711935780157
		0.0016684350731396617 -0.10407154586118712 0.57014271100952729
		-0.046123922378638126 -0.10330260511694389 0.56085526109937966
		-0.10207252258647663 -0.099763808826697137 0.53531449431833655
		-0.1513867233709929 -0.093204540367686911 0.49230326715237371
		-0.19398113563995539 -0.086189567056074598 0.3983133481578599
		-0.20239740249946542 -0.082281187400184502 0.33084437216240081
		-0.20881177567696263 -0.080838995160684671 0.29120272066437952
		0.19950096555483826 -0.021669271592522757 0.29965392865985874
		0.19373673139689893 -0.0226029748096114 0.33987576754478899
		0.18418141920588402 -0.025096816587360599 0.4069584462549658
		0.14740358479169258 -0.029989514811580728 0.50562389987655687
		0.10091430177798096 -0.034859888707458625 0.54755033672221709
		0.048783489926999199 -0.037604204174215415 0.57219428434300179
		0.0016670034472166585 -0.03864345434888377 0.58029156518015412
		-0.045919297865291225 -0.037614623718960523 0.57161825541816835
		-0.10091816778294758 -0.034863762605935274 0.54736919005371443
		-0.14893509931120624 -0.029995406227314232 0.50569779729561448
		-0.18989493291121687 -0.025089779004790088 0.40904472430060357
		-0.19792997074164498 -0.022589931965511817 0.34124101732388812
		-0.20425953939038161 -0.021660733613407576 0.30100292212054824
		0.19382891535823962 0.061639771640909176 0.30412443824129393
		0.18811981531174776 0.06108011334000496 0.34394289302981884
		0.17902682528113525 0.059628196884230163 0.41007261907280806
		0.14429606939223522 0.056751630098282019 0.51075679518417372
		0.099480319821696167 0.05404887853229827 0.55284815687136701
		0.046659024226659865 0.052174104630302136 0.57682253348729851
		0.0016010130335833441 0.051046295655368029 0.58361406039454944
		-0.043936714250331362 0.05216850967599293 0.57616635237617242
		-0.099552696061175644 0.054046445238017861 0.55261590816165485
		-0.14582210680771485 0.056750947387697724 0.51085094450307345
		-0.18463836666585218 0.059638753865095304 0.41210204704106512
		-0.19223851526876201 0.061086741793731379 0.34530277853503888
		-0.1985006381602725 0.06164528890699484 0.30546074699869291
		0.18972526830681777 0.10935076851000453 0.30102798762510252
		0.1839880914927472 0.10892591854855391 0.33979431324986203
		0.17515169999278141 0.10784382194847364 0.40443436376729286
		0.14196113553228221 0.1057996288470288 0.50558771924921664
		0.098414070015219152 0.10406020958934381 0.54891656743036799
		0.04373035870757537 0.10259125724860696 0.57329677189468864
		0.0015058473337370079 0.10160191848113402 0.5787826419271378
		-0.041198117208562257 0.10258725535795259 0.57256046955829432
		-0.0985640160541182 0.10405782107376957 0.54861799578095927
		-0.14346807510898585 0.10579943637339734 0.50569756964160351
		-0.18058674311092776 0.10785228508675587 0.40643254387663602
		-0.18798856202725886 0.10892973995748828 0.34114131189335128
		-0.19425827835933582 0.10935463177021186 0.30234196317345974
		0.18758437227982325 0.13012201600893236 0.2975177589966278
		0.18180868155309632 0.12973433678153962 0.33551814102668526
		0.17307155225335946 0.12874557520136878 0.39919588553685637
		0.1407104456836433 0.12692519512160264 0.5002879516542198
		0.097842749900184905 0.12542643921590488 0.54456981686865591
		0.041781674800934983 0.12408451145806239 0.56945691664903109
		0.0014419888800781841 0.12323262632098718 0.57415481383932498
		-0.039375109372782706 0.12408094029117284 0.56867396671565351
		-0.098041564365018569 0.12542433232472663 0.54423068027977806
		-0.142202941014136 0.12692507377330212 0.5004081508483742
		-0.17838408830654054 0.12875196360310401 0.4011793195751438
		-0.18572821476889942 0.12973745225347758 0.33685607651916
		-0.19202232674715183 0.13012529378161089 0.29881605351223428
		
		;
	setAttr ".nufa" 4.5;
	setAttr ".nvfa" 4.5;
createNode transform -n "EyeLidSurface_R_Nrb" -p "TmpJnt_Grp";
	rename -uid "EE6EF0D6-414D-023D-1C55-648E62DA127C";
	setAttr ".t" -type "double3" -0.12658336479216814 0.21789389386772307 0.35831180105587762 ;
	setAttr ".s" -type "double3" 0.155 0.15499999999999997 0.15499999999999997 ;
createNode nurbsSurface -n "EyeLidSurface_R_NrbShape" -p "EyeLidSurface_R_Nrb";
	rename -uid "7D8A7A13-4F89-12D1-2517-59B3FA13F23D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 2 no 
		9 0 0 0 1 2 3 4 4 4
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		
		77
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		0.19991679083637276 -1 -0.19991679083637284
		0.28272503694690354 -1 -1.1177774761168466e-16
		0.19991679083637276 -1 0.19991679083637268
		4.7890007918352597e-17 -1 0.2827250369469036
		-0.19991679083637273 -1 0.19991679083637273
		-0.28272503694690371 -1 3.2191243563517383e-19
		-0.19991679083637276 -1 -0.19991679083637279
		-7.1788605542032352e-17 -1 -0.28272503694690371
		0.19991679083637276 -1 -0.19991679083637284
		0.28272503694690354 -1 -1.1177774761168466e-16
		0.19991679083637276 -1 0.19991679083637268
		0.61642997969058977 -0.78361162489122427 -0.61642997969058977
		0.87176363753180319 -0.78361162489122427 -4.7776033311964345e-17
		0.61642997969058966 -0.78361162489122427 0.61642997969058977
		-8.3940866021681387e-18 -0.78361162489122427 0.87176363753180353
		-0.61642997969058977 -0.78361162489122427 0.61642997969058977
		-0.87176363753180375 -0.78361162489122427 -1.4243681125611212e-17
		-0.61642997969058966 -0.78361162489122427 -0.61642997969058977
		-6.529563193606176e-17 -0.78361162489122427 -0.87176363753180353
		0.61642997969058977 -0.78361162489122427 -0.61642997969058977
		0.87176363753180319 -0.78361162489122427 -4.7776033311964345e-17
		0.61642997969058966 -0.78361162489122427 0.61642997969058977
		0.86720244749154218 1.7637621209264518e-16 -0.86720244749154185
		1.2264094625656801 1.2327537701598182e-16 7.5095921138754302e-17
		0.86720244749154174 7.0174541939318428e-17 0.86720244749154207
		-8.6614559170421727e-17 4.8179455877227502e-17 1.2264094625656805
		-0.86720244749154207 7.0174541939318453e-17 0.86720244749154185
		-1.2264094625656807 1.2327537701598182e-16 -2.7341567632466632e-17
		-0.86720244749154174 1.7637621209264521e-16 -0.86720244749154207
		-1.7053183114584738e-17 1.9837129815473613e-16 -1.2264094625656805
		0.86720244749154218 1.7637621209264518e-16 -0.86720244749154185
		1.2264094625656801 1.2327537701598182e-16 7.5095921138754302e-17
		0.86720244749154174 7.0174541939318428e-17 0.86720244749154207
		0.61642997969058988 0.78361162489122471 -0.61642997969058966
		0.87176363753180319 0.78361162489122471 1.5453628814360203e-16
		0.61642997969058955 0.78361162489122471 0.61642997969058988
		-1.147416612379651e-16 0.78361162489122471 0.87176363753180353
		-0.61642997969058988 0.78361162489122471 0.61642997969058966
		-0.87176363753180375 0.78361162489122471 -2.4626508941638749e-17
		-0.61642997969058955 0.78361162489122471 -0.61642997969058988
		4.105194269973521e-17 0.78361162489122471 -0.87176363753180353
		0.61642997969058988 0.78361162489122471 -0.61642997969058966
		0.87176363753180319 0.78361162489122471 1.5453628814360203e-16
		0.61642997969058955 0.78361162489122471 0.61642997969058988
		0.19991679083637259 1 -0.19991679083637234
		0.28272503694690315 1 1.4640157876526888e-16
		0.19991679083637237 1 0.19991679083637262
		-8.7824638543865684e-17 1 0.28272503694690332
		-0.19991679083637257 1 0.19991679083637245
		-0.28272503694690332 1 -1.2928054111847823e-17
		-0.19991679083637237 1 -0.19991679083637251
		6.3926040920185966e-17 1 -0.28272503694690321
		0.19991679083637259 1 -0.19991679083637234
		0.28272503694690315 1 1.4640157876526888e-16
		0.19991679083637237 1 0.19991679083637262
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		
		;
	setAttr ".nufa" 4.5;
	setAttr ".nvfa" 4.5;
createNode nurbsSurface -n "EyeLidSurface_R_NrbShapeOrig" -p "EyeLidSurface_R_Nrb";
	rename -uid "C6AC70C5-467C-3897-5AD9-7D9B86935256";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 2 no 
		9 0 0 0 1 2 3 4 4 4
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		
		77
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		0.19991679083637276 -1 -0.19991679083637284
		0.28272503694690354 -1 -1.1177774761168466e-16
		0.19991679083637276 -1 0.19991679083637268
		4.7890007918352597e-17 -1 0.2827250369469036
		-0.19991679083637273 -1 0.19991679083637273
		-0.28272503694690371 -1 3.2191243563517383e-19
		-0.19991679083637276 -1 -0.19991679083637279
		-7.1788605542032352e-17 -1 -0.28272503694690371
		0.19991679083637276 -1 -0.19991679083637284
		0.28272503694690354 -1 -1.1177774761168466e-16
		0.19991679083637276 -1 0.19991679083637268
		0.61642997969058977 -0.78361162489122427 -0.61642997969058977
		0.87176363753180319 -0.78361162489122427 -4.7776033311964345e-17
		0.61642997969058966 -0.78361162489122427 0.61642997969058977
		-8.3940866021681387e-18 -0.78361162489122427 0.87176363753180353
		-0.61642997969058977 -0.78361162489122427 0.61642997969058977
		-0.87176363753180375 -0.78361162489122427 -1.4243681125611212e-17
		-0.61642997969058966 -0.78361162489122427 -0.61642997969058977
		-6.529563193606176e-17 -0.78361162489122427 -0.87176363753180353
		0.61642997969058977 -0.78361162489122427 -0.61642997969058977
		0.87176363753180319 -0.78361162489122427 -4.7776033311964345e-17
		0.61642997969058966 -0.78361162489122427 0.61642997969058977
		0.86720244749154218 1.7637621209264518e-16 -0.86720244749154185
		1.2264094625656801 1.2327537701598182e-16 7.5095921138754302e-17
		0.86720244749154174 7.0174541939318428e-17 0.86720244749154207
		-8.6614559170421727e-17 4.8179455877227502e-17 1.2264094625656805
		-0.86720244749154207 7.0174541939318453e-17 0.86720244749154185
		-1.2264094625656807 1.2327537701598182e-16 -2.7341567632466632e-17
		-0.86720244749154174 1.7637621209264521e-16 -0.86720244749154207
		-1.7053183114584738e-17 1.9837129815473613e-16 -1.2264094625656805
		0.86720244749154218 1.7637621209264518e-16 -0.86720244749154185
		1.2264094625656801 1.2327537701598182e-16 7.5095921138754302e-17
		0.86720244749154174 7.0174541939318428e-17 0.86720244749154207
		0.61642997969058988 0.78361162489122471 -0.61642997969058966
		0.87176363753180319 0.78361162489122471 1.5453628814360203e-16
		0.61642997969058955 0.78361162489122471 0.61642997969058988
		-1.147416612379651e-16 0.78361162489122471 0.87176363753180353
		-0.61642997969058988 0.78361162489122471 0.61642997969058966
		-0.87176363753180375 0.78361162489122471 -2.4626508941638749e-17
		-0.61642997969058955 0.78361162489122471 -0.61642997969058988
		4.105194269973521e-17 0.78361162489122471 -0.87176363753180353
		0.61642997969058988 0.78361162489122471 -0.61642997969058966
		0.87176363753180319 0.78361162489122471 1.5453628814360203e-16
		0.61642997969058955 0.78361162489122471 0.61642997969058988
		0.19991679083637259 1 -0.19991679083637234
		0.28272503694690315 1 1.4640157876526888e-16
		0.19991679083637237 1 0.19991679083637262
		-8.7824638543865684e-17 1 0.28272503694690332
		-0.19991679083637257 1 0.19991679083637245
		-0.28272503694690332 1 -1.2928054111847823e-17
		-0.19991679083637237 1 -0.19991679083637251
		6.3926040920185966e-17 1 -0.28272503694690321
		0.19991679083637259 1 -0.19991679083637234
		0.28272503694690315 1 1.4640157876526888e-16
		0.19991679083637237 1 0.19991679083637262
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		
		;
createNode transform -n "EyeLidSurface_L_Nrb" -p "TmpJnt_Grp";
	rename -uid "417AB5AC-4C32-B8D4-952C-05960E95783E";
	setAttr ".t" -type "double3" 0.12658338434994221 0.21789389386772307 0.35831179360529702 ;
	setAttr ".s" -type "double3" 0.155 0.15499999999999997 0.15499999999999997 ;
createNode nurbsSurface -n "EyeLidSurface_L_NrbShape" -p "EyeLidSurface_L_Nrb";
	rename -uid "D2C9AC9C-46D0-7B77-058F-06A4DD9F02C7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 2 no 
		9 0 0 0 1 2 3 4 4 4
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		
		77
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		0.19991679083637276 -1 -0.19991679083637284
		0.28272503694690354 -1 -1.1177774761168466e-16
		0.19991679083637276 -1 0.19991679083637268
		4.7890007918352597e-17 -1 0.2827250369469036
		-0.19991679083637273 -1 0.19991679083637273
		-0.28272503694690371 -1 3.2191243563517383e-19
		-0.19991679083637276 -1 -0.19991679083637279
		-7.1788605542032352e-17 -1 -0.28272503694690371
		0.19991679083637276 -1 -0.19991679083637284
		0.28272503694690354 -1 -1.1177774761168466e-16
		0.19991679083637276 -1 0.19991679083637268
		0.61642997969058977 -0.78361162489122427 -0.61642997969058977
		0.87176363753180319 -0.78361162489122427 -4.7776033311964345e-17
		0.61642997969058966 -0.78361162489122427 0.61642997969058977
		-8.3940866021681387e-18 -0.78361162489122427 0.87176363753180353
		-0.61642997969058977 -0.78361162489122427 0.61642997969058977
		-0.87176363753180375 -0.78361162489122427 -1.4243681125611212e-17
		-0.61642997969058966 -0.78361162489122427 -0.61642997969058977
		-6.529563193606176e-17 -0.78361162489122427 -0.87176363753180353
		0.61642997969058977 -0.78361162489122427 -0.61642997969058977
		0.87176363753180319 -0.78361162489122427 -4.7776033311964345e-17
		0.61642997969058966 -0.78361162489122427 0.61642997969058977
		0.86720244749154218 1.7637621209264518e-16 -0.86720244749154185
		1.2264094625656801 1.2327537701598182e-16 7.5095921138754302e-17
		0.86720244749154174 7.0174541939318428e-17 0.86720244749154207
		-8.6614559170421727e-17 4.8179455877227502e-17 1.2264094625656805
		-0.86720244749154207 7.0174541939318453e-17 0.86720244749154185
		-1.2264094625656807 1.2327537701598182e-16 -2.7341567632466632e-17
		-0.86720244749154174 1.7637621209264521e-16 -0.86720244749154207
		-1.7053183114584738e-17 1.9837129815473613e-16 -1.2264094625656805
		0.86720244749154218 1.7637621209264518e-16 -0.86720244749154185
		1.2264094625656801 1.2327537701598182e-16 7.5095921138754302e-17
		0.86720244749154174 7.0174541939318428e-17 0.86720244749154207
		0.61642997969058988 0.78361162489122471 -0.61642997969058966
		0.87176363753180319 0.78361162489122471 1.5453628814360203e-16
		0.61642997969058955 0.78361162489122471 0.61642997969058988
		-1.147416612379651e-16 0.78361162489122471 0.87176363753180353
		-0.61642997969058988 0.78361162489122471 0.61642997969058966
		-0.87176363753180375 0.78361162489122471 -2.4626508941638749e-17
		-0.61642997969058955 0.78361162489122471 -0.61642997969058988
		4.105194269973521e-17 0.78361162489122471 -0.87176363753180353
		0.61642997969058988 0.78361162489122471 -0.61642997969058966
		0.87176363753180319 0.78361162489122471 1.5453628814360203e-16
		0.61642997969058955 0.78361162489122471 0.61642997969058988
		0.19991679083637259 1 -0.19991679083637234
		0.28272503694690315 1 1.4640157876526888e-16
		0.19991679083637237 1 0.19991679083637262
		-8.7824638543865684e-17 1 0.28272503694690332
		-0.19991679083637257 1 0.19991679083637245
		-0.28272503694690332 1 -1.2928054111847823e-17
		-0.19991679083637237 1 -0.19991679083637251
		6.3926040920185966e-17 1 -0.28272503694690321
		0.19991679083637259 1 -0.19991679083637234
		0.28272503694690315 1 1.4640157876526888e-16
		0.19991679083637237 1 0.19991679083637262
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		
		;
	setAttr ".nufa" 4.5;
	setAttr ".nvfa" 4.5;
createNode nurbsSurface -n "EyeLidSurface_L_NrbShapeOrig" -p "EyeLidSurface_L_Nrb";
	rename -uid "DF46F67A-4221-FF7C-5620-2AAB17FE07A9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 2 no 
		9 0 0 0 1 2 3 4 4 4
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		
		77
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		9.5964746819769475e-17 -1 -2.5316183359690652e-16
		0.19991679083637276 -1 -0.19991679083637284
		0.28272503694690354 -1 -1.1177774761168466e-16
		0.19991679083637276 -1 0.19991679083637268
		4.7890007918352597e-17 -1 0.2827250369469036
		-0.19991679083637273 -1 0.19991679083637273
		-0.28272503694690371 -1 3.2191243563517383e-19
		-0.19991679083637276 -1 -0.19991679083637279
		-7.1788605542032352e-17 -1 -0.28272503694690371
		0.19991679083637276 -1 -0.19991679083637284
		0.28272503694690354 -1 -1.1177774761168466e-16
		0.19991679083637276 -1 0.19991679083637268
		0.61642997969058977 -0.78361162489122427 -0.61642997969058977
		0.87176363753180319 -0.78361162489122427 -4.7776033311964345e-17
		0.61642997969058966 -0.78361162489122427 0.61642997969058977
		-8.3940866021681387e-18 -0.78361162489122427 0.87176363753180353
		-0.61642997969058977 -0.78361162489122427 0.61642997969058977
		-0.87176363753180375 -0.78361162489122427 -1.4243681125611212e-17
		-0.61642997969058966 -0.78361162489122427 -0.61642997969058977
		-6.529563193606176e-17 -0.78361162489122427 -0.87176363753180353
		0.61642997969058977 -0.78361162489122427 -0.61642997969058977
		0.87176363753180319 -0.78361162489122427 -4.7776033311964345e-17
		0.61642997969058966 -0.78361162489122427 0.61642997969058977
		0.86720244749154218 1.7637621209264518e-16 -0.86720244749154185
		1.2264094625656801 1.2327537701598182e-16 7.5095921138754302e-17
		0.86720244749154174 7.0174541939318428e-17 0.86720244749154207
		-8.6614559170421727e-17 4.8179455877227502e-17 1.2264094625656805
		-0.86720244749154207 7.0174541939318453e-17 0.86720244749154185
		-1.2264094625656807 1.2327537701598182e-16 -2.7341567632466632e-17
		-0.86720244749154174 1.7637621209264521e-16 -0.86720244749154207
		-1.7053183114584738e-17 1.9837129815473613e-16 -1.2264094625656805
		0.86720244749154218 1.7637621209264518e-16 -0.86720244749154185
		1.2264094625656801 1.2327537701598182e-16 7.5095921138754302e-17
		0.86720244749154174 7.0174541939318428e-17 0.86720244749154207
		0.61642997969058988 0.78361162489122471 -0.61642997969058966
		0.87176363753180319 0.78361162489122471 1.5453628814360203e-16
		0.61642997969058955 0.78361162489122471 0.61642997969058988
		-1.147416612379651e-16 0.78361162489122471 0.87176363753180353
		-0.61642997969058988 0.78361162489122471 0.61642997969058966
		-0.87176363753180375 0.78361162489122471 -2.4626508941638749e-17
		-0.61642997969058955 0.78361162489122471 -0.61642997969058988
		4.105194269973521e-17 0.78361162489122471 -0.87176363753180353
		0.61642997969058988 0.78361162489122471 -0.61642997969058966
		0.87176363753180319 0.78361162489122471 1.5453628814360203e-16
		0.61642997969058955 0.78361162489122471 0.61642997969058988
		0.19991679083637259 1 -0.19991679083637234
		0.28272503694690315 1 1.4640157876526888e-16
		0.19991679083637237 1 0.19991679083637262
		-8.7824638543865684e-17 1 0.28272503694690332
		-0.19991679083637257 1 0.19991679083637245
		-0.28272503694690332 1 -1.2928054111847823e-17
		-0.19991679083637237 1 -0.19991679083637251
		6.3926040920185966e-17 1 -0.28272503694690321
		0.19991679083637259 1 -0.19991679083637234
		0.28272503694690315 1 1.4640157876526888e-16
		0.19991679083637237 1 0.19991679083637262
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		-8.6998366831816947e-17 1 2.4419545360895402e-16
		
		;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "92D3298E-45B6-A3F0-F760-AC947E20A766";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "C231B5D2-4160-8819-E440-258425269096";
	setAttr ".bsdt[0].bscd" -type "Int32Array" 0 ;
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "97A8F338-4B24-A1B1-8761-4EA582FB345F";
createNode displayLayerManager -n "layerManager";
	rename -uid "8AF2B68E-4D67-6B41-F8A4-79AB3F3C361C";
	setAttr ".cdl" 2;
	setAttr -s 10 ".dli[1:9]"  2 10 1 4 5 6 7 8 
		9;
createNode displayLayer -n "defaultLayer";
	rename -uid "57FE681D-4785-8ECD-BA25-DCBD58E8D132";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "A0F52253-44CA-1073-256E-1A92A21329C4";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "D5DB003F-40A4-0C00-4ABC-EE8B26CAEE2E";
	setAttr ".g" yes;
createNode reference -n "main_mdRN";
	rename -uid "87C9A0C8-497A-47F4-49CE-0AB5EEAA0722";
	setAttr ".ed" -type "dataReferenceEdits" 
		"main_mdRN"
		"main_mdRN" 1
		5 3 "main_mdRN" "|main_md:Geo_Grp|main_md:MdGeo_Grp.instObjGroups" "main_mdRN.placeHolderList[1]" 
		"";
lockNode -l 1 ;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "D37ACBED-4D7A-B10E-9277-02982C0E8B82";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "C2097AD0-4286-7848-926E-7A89DC4CFE86";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -323.80951094248991 -330.95236780151544 ;
	setAttr ".tgi[0].vh" -type "double2" 324.99998708566085 330.95236780151544 ;
createNode plusMinusAverage -n "bussaba_001:JawLwr1Drv_Pma";
	rename -uid "C8D237E6-48C2-6326-B767-DBB1FB155C39";
	addAttr -ci true -k true -sn "constant" -ln "constant" -at "float";
	setAttr -s 2 ".i1[1]"  31.98345375;
	setAttr -k on ".constant" -31.983453750610352;
createNode plusMinusAverage -n "bussaba_001:UpArmFkStrt_L_Pma";
	rename -uid "77C9F53F-4556-6695-BDB8-55AA0E9D7578";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:UpArmFkStrt_L_Mdv";
	rename -uid "9D6987E1-41FC-9379-1C79-F0B36B01A2CD";
createNode plusMinusAverage -n "bussaba_001:ForearmFkStrt_L_Pma";
	rename -uid "00F05C5B-49A7-3BE4-08C0-E084C07B8669";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:ForearmFkStrt_L_Mdv";
	rename -uid "057869AC-4667-A83D-F153-5BA43A34B42D";
createNode plusMinusAverage -n "bussaba_001:UpArmFkStrt_R_Pma";
	rename -uid "62D60B20-4377-B734-E6E8-C9A4874DAC29";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:UpArmFkStrt_R_Mdv";
	rename -uid "27F061CA-4313-6740-9160-C3882CBF4B4D";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode plusMinusAverage -n "bussaba_001:ForearmFkStrt_R_Pma";
	rename -uid "C1B172A1-4189-216C-8F1E-E594390BC4BE";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:ForearmFkStrt_R_Mdv";
	rename -uid "95E65F5E-49FC-F823-93DC-5BA2BD3BE0EE";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode plusMinusAverage -n "bussaba_001:UpLegFkStrt_L_Pma";
	rename -uid "7B7E5433-4CC6-01A0-C6A8-3EB9084BF52B";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:UpLegFkStrt_L_Mdv";
	rename -uid "4E84CFE7-454A-60F0-FFF5-3BA5ADDE8807";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode plusMinusAverage -n "bussaba_001:LowLegFkStrt_L_Pma";
	rename -uid "963842F5-4228-0A2D-6FD0-B7B09C617BF6";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:LowLegFkStrt_L_Mdv";
	rename -uid "B8B452A5-4441-DA19-881D-69A898C2F422";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode plusMinusAverage -n "bussaba_001:UpLegFkStrt_R_Pma";
	rename -uid "BA659D76-40BC-0580-F204-D2AE2F69A38F";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:UpLegFkStrt_R_Mdv";
	rename -uid "0DB6A018-4CDC-DA13-CB10-4C8252588272";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode plusMinusAverage -n "bussaba_001:LowLegFkStrt_R_Pma";
	rename -uid "8706A2BC-4334-E2B6-C493-96B676573A4F";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:LowLegFkStrt_R_Mdv";
	rename -uid "B8280139-46C2-DCC0-FDD1-F3BC1C9627EF";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Hair1Strt_Pma";
	rename -uid "50BC8C90-473D-0793-9850-218C579C647E";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:Hair1Strt_Mdv";
	rename -uid "6B74D33C-4B35-692D-358A-A79E564D37A4";
createNode plusMinusAverage -n "bussaba_001:Hair2Strt_Pma";
	rename -uid "3E9E1643-402C-D874-4AAB-EFA56F9DD6EE";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:Hair2Strt_Mdv";
	rename -uid "F44AB3D4-4DD5-B974-E61B-C0AA5EAA96EE";
createNode plusMinusAverage -n "bussaba_001:Hair3Strt_Pma";
	rename -uid "6B1304E2-4572-39D3-AF6A-B5BBCAB93B36";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:Hair3Strt_Mdv";
	rename -uid "1FBA02C8-4995-9356-C19D-1DB43A836598";
createNode plusMinusAverage -n "bussaba_001:Hair4Strt_Pma";
	rename -uid "6B93AE9D-470F-B417-5D97-879257FEE967";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:Hair4Strt_Mdv";
	rename -uid "6EA54FFF-4A00-0044-F611-E4B17A99F9F9";
createNode plusMinusAverage -n "bussaba_001:Hair5Strt_Pma";
	rename -uid "BCA10B1B-4926-18F4-8197-028E902EFF8C";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:Hair5Strt_Mdv";
	rename -uid "411F8FB4-45FA-4DB2-B621-0099BC291E6F";
createNode plusMinusAverage -n "bussaba_001:Hair6Strt_Pma";
	rename -uid "CA23FAC3-45D9-F046-C71F-7695B4FBBBD1";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:Hair6Strt_Mdv";
	rename -uid "A2CFEFEF-4D35-7F75-B6B4-DE81A614EA12";
createNode plusMinusAverage -n "bussaba_001:HairSide1Strt_L_Pma";
	rename -uid "985CD119-4D23-29E2-75BF-D6A943EA277C";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:HairSide1Strt_L_Mdv";
	rename -uid "A9CBCFDD-4412-B0D5-2559-1CBE31D25615";
createNode plusMinusAverage -n "bussaba_001:HairSide2Strt_L_Pma";
	rename -uid "CE36A236-442C-9014-70BA-39AAB645F60D";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:HairSide2Strt_L_Mdv";
	rename -uid "54354C27-4478-8F3A-3832-82A33D61D1AF";
createNode plusMinusAverage -n "bussaba_001:HairSide3Strt_L_Pma";
	rename -uid "7D85510D-4FA5-D981-F6A5-53836A47B876";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:HairSide3Strt_L_Mdv";
	rename -uid "AFEB79F7-477F-1D53-6DB7-DD930BD1B403";
createNode plusMinusAverage -n "bussaba_001:HairSide4Strt_L_Pma";
	rename -uid "01B3A6C2-4194-910A-4FC9-31B748467549";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:HairSide4Strt_L_Mdv";
	rename -uid "60996AC8-402F-80AD-A90E-778C718F63BD";
createNode plusMinusAverage -n "bussaba_001:HairSide5Strt_L_Pma";
	rename -uid "CDBDF9F1-481B-849B-40AF-028FB05706D9";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:HairSide5Strt_L_Mdv";
	rename -uid "DD1C7547-452A-8505-C2E4-E1B2D6322B42";
createNode plusMinusAverage -n "bussaba_001:HairSide1Strt_R_Pma";
	rename -uid "442B51DA-4308-EB20-7202-B2A8168972CE";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:HairSide1Strt_R_Mdv";
	rename -uid "05C54A11-41D4-195F-8E97-9FBBA0B504E1";
createNode plusMinusAverage -n "bussaba_001:HairSide2Strt_R_Pma";
	rename -uid "1C2281C5-49B0-66C0-4ABC-F7AD2000E5E4";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:HairSide2Strt_R_Mdv";
	rename -uid "E5AD53E7-45A6-142D-83A2-3DBDD8298707";
createNode plusMinusAverage -n "bussaba_001:HairSide3Strt_R_Pma";
	rename -uid "9C4226D5-446F-1ADF-EB97-FCA8BEC5B7CF";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:HairSide3Strt_R_Mdv";
	rename -uid "4B0FAA8E-4A87-B856-42CB-6F9FC977A70C";
createNode plusMinusAverage -n "bussaba_001:HairSide4Strt_R_Pma";
	rename -uid "18DF1A4A-4721-CBD8-41EA-0280D8DDCA30";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:HairSide4Strt_R_Mdv";
	rename -uid "AA26D2D0-4593-B417-7F4D-7EB55EF6679D";
createNode plusMinusAverage -n "bussaba_001:HairSide5Strt_R_Pma";
	rename -uid "D792E79E-4624-1AC8-0CAC-6F9887FE8E54";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:HairSide5Strt_R_Mdv";
	rename -uid "72E5C6D8-4829-9A63-D517-539F1996FEDB";
createNode plusMinusAverage -n "bussaba_001:Breast1Strt_L_Pma";
	rename -uid "FB42BD09-4D5C-529B-F875-9791F253B426";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:Breast1Strt_L_Mdv";
	rename -uid "52B0F244-4657-5A03-EA6C-55927837B534";
createNode plusMinusAverage -n "bussaba_001:Breast1Strt_R_Pma";
	rename -uid "A32A7699-4EFC-FE01-2680-42A50EBAE425";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:Breast1Strt_R_Mdv";
	rename -uid "98034639-4522-F6A0-34FC-31A318FBEF50";
createNode plusMinusAverage -n "bussaba_001:PelvisSqsh_Pma";
	rename -uid "DE5EED61-463E-5937-D60D-E081468B3A7B";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:PelvisSqsh_Mdv";
	rename -uid "3CF6017A-45E0-FB07-210E-97ACF5700CE4";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Spine1Sqsh_Pma";
	rename -uid "731AE619-4050-1EDA-7015-B4887A47EAA8";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:Spine1Sqsh_Mdv";
	rename -uid "C9BA8D63-4377-645F-0AC7-8B8A3C503848";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode blendColors -n "bussaba_001:SpineAutoSqsh_Bcl";
	rename -uid "E504C634-42B8-6370-EE56-7D8B86293674";
	setAttr ".c2" -type "float3" 1 0 1 ;
createNode multiplyDivide -n "bussaba_001:SpineSqshDiv_Mdv";
	rename -uid "0A6878AD-43E0-D6A0-A98A-3284050AAC47";
	setAttr ".op" 2;
	setAttr ".i1" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "bussaba_001:SpineSqshPow_Mdv";
	rename -uid "5589C102-48FC-E306-949B-4FA6A1D673B4";
	setAttr ".op" 3;
	setAttr ".i2" -type "float3" 2 1 1 ;
createNode multiplyDivide -n "bussaba_001:SpineSqshNorm_Mdv";
	rename -uid "D2CD207E-43AF-3F76-7D48-7AA3A129ED10";
	setAttr ".op" 2;
createNode plusMinusAverage -n "bussaba_001:Spine1Brth_Pma";
	rename -uid "1C5BAD50-478A-168D-9DA1-D2BB28D23A97";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 3 ".i1";
	setAttr -s 3 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Spine1Brth_Mdv";
	rename -uid "5AFA2BDC-45BA-C157-71B8-FB91280D851B";
	setAttr ".i2" -type "float3" 1 0 1 ;
createNode plusMinusAverage -n "bussaba_001:Spine2Sqsh_Pma";
	rename -uid "2F1F52D6-46AE-4CA2-B42C-DEBC98FD9840";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:Spine2Sqsh_Mdv";
	rename -uid "11362445-4FF3-9805-5B89-8BB2E47DB007";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:Spine2Brth_Pma";
	rename -uid "C2587145-4B56-80FB-00ED-6DAD69AD34DE";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 3 ".i1";
	setAttr -s 3 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Spine2Brth_Mdv";
	rename -uid "0ADFD648-45FA-EF9E-09D9-FD9EFD6D1525";
	setAttr ".i2" -type "float3" 0.85000002 0.15000001 1 ;
createNode plusMinusAverage -n "bussaba_001:Spine3Sqsh_Pma";
	rename -uid "A74467FF-4DB9-B6BF-8B37-0098E4A982EE";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:Spine3Sqsh_Mdv";
	rename -uid "A4D685E5-437E-DA24-546F-A791BA0F792F";
	setAttr ".i2" -type "float3" 0.5 0.5 1 ;
createNode plusMinusAverage -n "bussaba_001:Spine3Brth_Pma";
	rename -uid "5A593117-414B-16D1-1103-E7804AB5B21D";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 3 ".i1";
	setAttr -s 3 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Spine3Brth_Mdv";
	rename -uid "3D4FBA2A-49DD-49A8-41CB-D6BBB95D3CE3";
	setAttr ".i2" -type "float3" 0.5 0.5 1 ;
createNode plusMinusAverage -n "bussaba_001:Spine4Sqsh_Pma";
	rename -uid "33E9410F-4325-ED44-1F69-219EE4FE651D";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:Spine4Sqsh_Mdv";
	rename -uid "CE2E58BB-4E37-77D5-DB1E-05A03532A43D";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:Spine4Brth_Pma";
	rename -uid "95A78E87-4392-1E28-C332-DB8A494EAAAA";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 3 ".i1";
	setAttr -s 3 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Spine4Brth_Mdv";
	rename -uid "42ADB558-4F51-57A2-000F-8B992C7D46EF";
	setAttr ".i2" -type "float3" 0.15000001 0.85000002 1 ;
createNode plusMinusAverage -n "bussaba_001:Spine5Sqsh_Pma";
	rename -uid "39EF58AE-40F0-8AB7-7CC4-D4B40825EE94";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:Spine5Sqsh_Mdv";
	rename -uid "CB7E589F-4C86-4FE4-980C-7897464FBD46";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode plusMinusAverage -n "bussaba_001:Spine5Brth_Pma";
	rename -uid "311FC0F6-4637-86B3-7E8B-EC88BA800852";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 3 ".i1";
	setAttr -s 3 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Spine5Brth_Mdv";
	rename -uid "10CB01D4-47D7-D87B-6F62-71B14E3D0648";
	setAttr ".i2" -type "float3" 0 1 1 ;
createNode plusMinusAverage -n "bussaba_001:NeckStrt_Pma";
	rename -uid "778E13DB-4822-03C7-F2E1-4E97CDEEEC4F";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 0.6201019287109375;
createNode multiplyDivide -n "bussaba_001:NeckStrt_Mdv";
	rename -uid "552F81FD-4219-438C-7ED5-4B94516DB2C6";
createNode plusMinusAverage -n "bussaba_001:Hair1Sqsh_Pma";
	rename -uid "E8987291-47B5-2690-2334-EB8334758BD3";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Hair1Sqsh_Mdv";
	rename -uid "5D8ADE66-4484-198B-B06D-F68B9A9D0999";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Hair2Sqsh_Pma";
	rename -uid "F89B2305-4A2D-AF30-7E75-8FA1FB4244C1";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Hair2Sqsh_Mdv";
	rename -uid "3DF96C86-42EC-5C81-F7A1-D58770DDF80F";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Hair3Sqsh_Pma";
	rename -uid "7A982AD3-4503-501D-BBED-4C9829622494";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Hair3Sqsh_Mdv";
	rename -uid "7979DDB2-4B56-A45D-77F5-07BF9BFE7F03";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Hair4Sqsh_Pma";
	rename -uid "3826A988-4158-ECE9-A307-67BEB242D5B7";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Hair4Sqsh_Mdv";
	rename -uid "4AC1A014-4E45-7588-E5F1-01AEC07C0F06";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Hair5Sqsh_Pma";
	rename -uid "5A7A575C-4EA2-A1E8-A26A-F1B6FB7E8812";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Hair5Sqsh_Mdv";
	rename -uid "2F154312-43E0-8C73-094C-5E820544BDB7";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Hair6Sqsh_Pma";
	rename -uid "3E91A30F-4655-B6B1-21AD-DDA4A584BC7D";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Hair6Sqsh_Mdv";
	rename -uid "52E58FA2-4541-2FDF-7B4B-6A92F69121F5";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Hair7Sqsh_Pma";
	rename -uid "7BD1EFF1-4F5B-A112-095A-D6A14235B8EC";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Hair7Sqsh_Mdv";
	rename -uid "EBE665A9-40CB-C7D5-6F73-35A45124C3E6";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Hair7Strt_Pma";
	rename -uid "AF094CAD-41CA-A770-B57A-7FAAA8D5E337";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 0.21359668672084808;
createNode multiplyDivide -n "bussaba_001:Hair7Strt_Mdv";
	rename -uid "96D02DF1-4BF9-1913-E800-3F80D86AA349";
createNode plusMinusAverage -n "bussaba_001:HairSide1Sqsh_L_Pma";
	rename -uid "AEB3E3F6-45D7-DAA0-423C-26AE2A20EDEF";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:HairSide1Sqsh_L_Mdv";
	rename -uid "37C5A436-4E87-84D9-9042-A4B523C74A75";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:HairSide2Sqsh_L_Pma";
	rename -uid "62607A66-4A92-5107-89C3-66A100DC3D0C";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:HairSide2Sqsh_L_Mdv";
	rename -uid "69EDD4F2-49FB-F79F-1439-C8941309F71F";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:HairSide3Sqsh_L_Pma";
	rename -uid "7895EF90-4BC9-51CE-5684-269F31275C22";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:HairSide3Sqsh_L_Mdv";
	rename -uid "35AB2D9F-4CF8-7623-7D49-00B2167D3EF9";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:HairSide4Sqsh_L_Pma";
	rename -uid "BA64612F-4564-537E-A585-C1B3AB1D0E11";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:HairSide4Sqsh_L_Mdv";
	rename -uid "3DFFE594-465D-50C5-F362-B6BC610CBB90";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:HairSide5Sqsh_L_Pma";
	rename -uid "F92A3F55-4827-284A-E019-3CA1D3A14AE9";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:HairSide5Sqsh_L_Mdv";
	rename -uid "5F1E82AD-4EE9-F913-B616-F0B62BD1FAAB";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:HairSide6Sqsh_L_Pma";
	rename -uid "F3F90929-4C8C-D115-4BDB-F9895F877CFD";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:HairSide6Sqsh_L_Mdv";
	rename -uid "ED87C2E0-4C39-6F2A-2866-55AEF63D4BBE";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:HairSide6Strt_L_Pma";
	rename -uid "64EF5A7E-4E7C-775F-5784-B5A96F0D66CC";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 0.091387949883937836;
createNode multiplyDivide -n "bussaba_001:HairSide6Strt_L_Mdv";
	rename -uid "A4047315-4D1C-8D84-F6BD-0D9321A6C0FA";
createNode plusMinusAverage -n "bussaba_001:HairSide1Sqsh_R_Pma";
	rename -uid "027BA15D-4294-844B-C24F-8C8F3959E644";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:HairSide1Sqsh_R_Mdv";
	rename -uid "F64E757D-4B25-F9C7-35CE-31B2300EC77F";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:HairSide2Sqsh_R_Pma";
	rename -uid "12BDE6CF-4B79-730B-2D7F-62A7F4E32165";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:HairSide2Sqsh_R_Mdv";
	rename -uid "31C480D0-4ECE-5F23-C866-4AA7D212B46B";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:HairSide3Sqsh_R_Pma";
	rename -uid "47C7684D-47D5-B600-D5DA-2AA558C2DE2B";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:HairSide3Sqsh_R_Mdv";
	rename -uid "AAB6C761-437E-8E58-6689-0BB4DC15D311";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:HairSide4Sqsh_R_Pma";
	rename -uid "3B35B69F-46BD-A365-9D0E-F2B39153AD20";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:HairSide4Sqsh_R_Mdv";
	rename -uid "DBEE32ED-448B-307D-93AE-72B36E5C14D6";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:HairSide5Sqsh_R_Pma";
	rename -uid "1D178BFF-4D59-A1B3-3B66-A4BDC1836C5C";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:HairSide5Sqsh_R_Mdv";
	rename -uid "698384CC-4FA2-E7F6-579A-378B2C3530EC";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:HairSide6Sqsh_R_Pma";
	rename -uid "73EB1FC6-4434-5E6C-ED3A-3AB835AA084F";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:HairSide6Sqsh_R_Mdv";
	rename -uid "ABE43994-4D31-2E97-DA78-0E90C473EB51";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:HairSide6Strt_R_Pma";
	rename -uid "81FC5C9D-45FE-DD6E-F1FA-EFB0478551FB";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" -0.091389946639537811;
createNode multiplyDivide -n "bussaba_001:HairSide6Strt_R_Mdv";
	rename -uid "B090EF6D-4999-9601-575E-6DA37786D1B6";
createNode plusMinusAverage -n "bussaba_001:ClavStrt_L_Pma";
	rename -uid "C6A07B0F-43C3-94FA-5066-6EB3E06FA427";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 0.50789618492126465;
createNode multiplyDivide -n "bussaba_001:ClavStrt_L_Mdv";
	rename -uid "A9CE7336-4656-00E4-5451-F6AA06DC3081";
createNode plusMinusAverage -n "bussaba_001:Index1Sqsh_L_Pma";
	rename -uid "DA6AFAA8-4581-29E2-9768-19B9CA4E2E2A";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Index1Sqsh_L_Mdv";
	rename -uid "89DCD6D1-4E1D-7C36-843A-AE8A2143D397";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Index2Sqsh_L_Pma";
	rename -uid "AAD066D4-4632-612B-7023-BE82DA8523AA";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Index2Sqsh_L_Mdv";
	rename -uid "7088C34D-4091-4F60-31A4-7E8A25477069";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Index3Sqsh_L_Pma";
	rename -uid "E7AEBA4F-4A01-AEE3-ACBC-E59277E247D3";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Index3Sqsh_L_Mdv";
	rename -uid "E006A98A-44A4-CD61-FED8-DF926942EB51";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Index4Sqsh_L_Pma";
	rename -uid "7E7A903B-4805-000C-3C2C-ABBC824690B9";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Index4Sqsh_L_Mdv";
	rename -uid "A5E5B8FA-4F6A-9988-5032-AAAE17A55543";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Middle1Sqsh_L_Pma";
	rename -uid "F7C02558-4FA3-B539-B337-F3BE27A10FF7";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Middle1Sqsh_L_Mdv";
	rename -uid "C624CD90-4161-87D9-B996-CE9CE7755C52";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Middle2Sqsh_L_Pma";
	rename -uid "3FA06895-47BE-8A54-F253-F1BE10DFD5FE";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Middle2Sqsh_L_Mdv";
	rename -uid "5402B117-4FD6-B885-0A8C-D09EE5075E70";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Middle3Sqsh_L_Pma";
	rename -uid "69672878-4186-492F-3EB8-DA806C49D9A6";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Middle3Sqsh_L_Mdv";
	rename -uid "D5387E92-4F4B-FA6C-6051-D6ABB8608C39";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Middle4Sqsh_L_Pma";
	rename -uid "65671AFF-4D38-E177-2B06-6EA622423C15";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Middle4Sqsh_L_Mdv";
	rename -uid "D066C9CD-433E-14C9-4EA2-2BA73A86AB13";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Ring1Sqsh_L_Pma";
	rename -uid "1901CA65-42D1-4C8E-50BA-7DB843A61B09";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Ring1Sqsh_L_Mdv";
	rename -uid "79A3338B-435A-D2EC-D236-88A081F4D86D";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Ring2Sqsh_L_Pma";
	rename -uid "10DDD6A2-4461-CC05-2746-ACAAD893E1ED";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Ring2Sqsh_L_Mdv";
	rename -uid "E1480A1D-47C5-28AD-D04A-4BADE76D7DCC";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Ring3Sqsh_L_Pma";
	rename -uid "553FDF97-429B-0191-F87F-08A98673EF90";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Ring3Sqsh_L_Mdv";
	rename -uid "23ED1FF7-4485-C4A3-6547-C3A7EDB02529";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Ring4Sqsh_L_Pma";
	rename -uid "8E22B90E-49FF-6CBF-20D5-0EBC8869EE64";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Ring4Sqsh_L_Mdv";
	rename -uid "8423FAB3-4C88-6FA2-34D2-1EB34974EE96";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Pinky1Sqsh_L_Pma";
	rename -uid "A27B918C-453D-FE34-4722-9488AA0849A8";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Pinky1Sqsh_L_Mdv";
	rename -uid "630DD32A-49DC-60B0-9295-2D9A6DC28A37";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Pinky2Sqsh_L_Pma";
	rename -uid "D521A1E9-4CC2-1303-BDB0-AF9A57E351E6";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Pinky2Sqsh_L_Mdv";
	rename -uid "BDFE4581-4EBB-12D2-D70A-1E962DA831BB";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Pinky3Sqsh_L_Pma";
	rename -uid "5195F252-478B-D11E-E19B-55ADC6DC9144";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Pinky3Sqsh_L_Mdv";
	rename -uid "835E4102-4E4C-9551-95D5-F19ADA42FF12";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Pinky4Sqsh_L_Pma";
	rename -uid "534F35B3-4D16-04CB-E4BA-06BE2AFAF6E3";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Pinky4Sqsh_L_Mdv";
	rename -uid "DE118E03-4926-9513-A457-01BFE72EB2A4";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Thumb1Sqsh_L_Pma";
	rename -uid "2079F479-4940-7CB5-5A9A-0E9BEAFF5107";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Thumb1Sqsh_L_Mdv";
	rename -uid "429A9866-4E56-1A7E-1239-4C98AECEF018";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Thumb2Sqsh_L_Pma";
	rename -uid "4E912A2A-44C0-D175-184A-F08EC9A02287";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Thumb2Sqsh_L_Mdv";
	rename -uid "94F4BCC1-48DA-B316-06A8-22A438DF15E4";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Thumb3Sqsh_L_Pma";
	rename -uid "2D0FE048-4729-CC13-4218-0E81F7623712";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Thumb3Sqsh_L_Mdv";
	rename -uid "39EBA4F1-4AC2-BAB1-D338-0584720FF78A";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:ClavStrt_R_Pma";
	rename -uid "FB2D3F1E-4C1E-56DD-084E-4793D8E5AAEB";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" -0.50789600610733032;
createNode multiplyDivide -n "bussaba_001:ClavStrt_R_Mdv";
	rename -uid "6C13208D-4A35-E606-23F1-149D39823930";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Index1Sqsh_R_Pma";
	rename -uid "09D0BEE7-4347-F3C3-EB88-209A7D75897E";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Index1Sqsh_R_Mdv";
	rename -uid "267FD1DE-412B-8CA2-B0B7-DC9349FB861A";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Index2Sqsh_R_Pma";
	rename -uid "C3C5F564-4CED-C6E8-C067-EFA070CF0344";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Index2Sqsh_R_Mdv";
	rename -uid "1F001F95-4192-02D1-BE1D-3489495854CE";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Index3Sqsh_R_Pma";
	rename -uid "03D3F553-49CC-334A-1563-B4AFD90ABC80";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Index3Sqsh_R_Mdv";
	rename -uid "59A411B2-433C-79F6-619D-6989E78302EA";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Index4Sqsh_R_Pma";
	rename -uid "7A41E530-4497-D376-F264-E886A9420A1B";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Index4Sqsh_R_Mdv";
	rename -uid "ED567C2E-486F-9E2A-3266-DABEF2E8EF4B";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Middle1Sqsh_R_Pma";
	rename -uid "2977AF09-4DDE-0E31-7420-AEBE81D37A56";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Middle1Sqsh_R_Mdv";
	rename -uid "8A82D0BB-43F1-6FB3-9EFF-91BB37483427";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Middle2Sqsh_R_Pma";
	rename -uid "35273B6C-464E-51A8-E2EE-499408788136";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Middle2Sqsh_R_Mdv";
	rename -uid "0CB20FCF-4EF2-9313-8731-11A1F5F8D7CF";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Middle3Sqsh_R_Pma";
	rename -uid "53AF57EA-417F-2CC8-5CFC-8DAD44A5FEC5";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Middle3Sqsh_R_Mdv";
	rename -uid "E03A7B2E-4787-DBB4-AB02-41812371710A";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Middle4Sqsh_R_Pma";
	rename -uid "D748CC70-434C-543B-EC58-4B9B6714C1BD";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Middle4Sqsh_R_Mdv";
	rename -uid "B53393D0-4A6D-449D-ABEF-98AD1C50A310";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Ring1Sqsh_R_Pma";
	rename -uid "6C3213E0-4F41-016A-8056-D787B076A50E";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Ring1Sqsh_R_Mdv";
	rename -uid "00022B01-4723-EE5B-18E2-F296031BF4BB";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Ring2Sqsh_R_Pma";
	rename -uid "7C76391E-46CD-71A4-3577-9785774A3D5F";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Ring2Sqsh_R_Mdv";
	rename -uid "80B1C589-4533-E655-9BDD-5D8B08DEA63B";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Ring3Sqsh_R_Pma";
	rename -uid "FABDD91E-408F-6C56-BE95-E496C4FBEB4E";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Ring3Sqsh_R_Mdv";
	rename -uid "31659911-4AF8-F645-2A0C-089920ADB6E7";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Ring4Sqsh_R_Pma";
	rename -uid "4623B219-4484-2694-3BAD-F5A57D4EB014";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Ring4Sqsh_R_Mdv";
	rename -uid "7AF611BB-423A-E63E-8D8D-07ACB732D85C";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Pinky1Sqsh_R_Pma";
	rename -uid "CA4431AC-4212-44C7-1C7D-9B9A1DFA6CF9";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Pinky1Sqsh_R_Mdv";
	rename -uid "7F8BBCBC-4208-F0B6-B658-A4863AC0482F";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Pinky2Sqsh_R_Pma";
	rename -uid "43C3B52F-438B-8FF9-4FF9-FBABCB63C87C";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Pinky2Sqsh_R_Mdv";
	rename -uid "869F2B22-4F81-796B-C88E-4E9E1E0F5B71";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Pinky3Sqsh_R_Pma";
	rename -uid "C513ACB0-4C8F-36F8-ACFB-99BEC6E8A004";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Pinky3Sqsh_R_Mdv";
	rename -uid "3050F7EA-449E-4BF6-4E8D-4BBABEE3F582";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Pinky4Sqsh_R_Pma";
	rename -uid "F509579F-4BA4-708F-F2B2-9680E10241C5";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Pinky4Sqsh_R_Mdv";
	rename -uid "5CB7B0FA-48FD-8EBF-3D78-B9923B227940";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Thumb1Sqsh_R_Pma";
	rename -uid "86A41716-437D-739C-BE5B-A18A6CD13EE4";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Thumb1Sqsh_R_Mdv";
	rename -uid "3A7B4B71-4BDC-CA4C-7FA4-698361AA256B";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Thumb2Sqsh_R_Pma";
	rename -uid "36681A53-4020-777E-FA6F-61AE9ADDA77F";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Thumb2Sqsh_R_Mdv";
	rename -uid "CBDD7378-444B-9FDC-D01F-1BB1DBF5E375";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Thumb3Sqsh_R_Pma";
	rename -uid "AFFB6E41-435D-C8AB-D362-12803AEC4F5B";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Thumb3Sqsh_R_Mdv";
	rename -uid "9AFC0B1B-459F-8F30-F4F1-CC95EC11DDC6";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Neck1RbnDtlSqsh_Pma";
	rename -uid "6AF7B199-4612-5056-8590-BF88F9F07F1E";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:Neck1RbnDtlSqsh_Mdv";
	rename -uid "75BDE37C-4DE4-87C0-F807-A3BB36C5BAC6";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode blendColors -n "bussaba_001:NeckRbnAutoSqsh_Bcl";
	rename -uid "B38B572F-4BEC-A73A-5E26-1B9803C32695";
	setAttr ".c2" -type "float3" 1 0 1 ;
createNode multiplyDivide -n "bussaba_001:NeckRbnSqshDiv_Mdv";
	rename -uid "D13EED3E-4269-E598-B15D-B9BA6651247C";
	setAttr ".op" 2;
	setAttr ".i1" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "bussaba_001:NeckRbnSqshPow_Mdv";
	rename -uid "26DE7265-4929-A3D0-39B4-B78E547D5036";
	setAttr ".op" 3;
	setAttr ".i2" -type "float3" 2 1 1 ;
createNode multiplyDivide -n "bussaba_001:NeckRbnSqshNorm_Mdv";
	rename -uid "E07CE033-4268-47F0-E4E6-129962048E28";
	setAttr ".op" 2;
createNode plusMinusAverage -n "bussaba_001:Neck2RbnDtlSqsh_Pma";
	rename -uid "DC744B28-4462-10CC-F200-55B7E4FB4E85";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:Neck2RbnDtlSqsh_Mdv";
	rename -uid "1AE2E435-4A8A-752A-DDE8-B9B7EA88E590";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:Neck3RbnDtlSqsh_Pma";
	rename -uid "0DC563B8-4DFA-EEBC-0E53-EAA61B439FF6";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:Neck3RbnDtlSqsh_Mdv";
	rename -uid "CB4B6E6E-4A8E-C243-2E75-90BC98BE03FF";
	setAttr ".i2" -type "float3" 0.5 0.5 1 ;
createNode plusMinusAverage -n "bussaba_001:Neck4RbnDtlSqsh_Pma";
	rename -uid "4BAC5150-4593-7051-7F0A-628464579AAC";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:Neck4RbnDtlSqsh_Mdv";
	rename -uid "D41B7BC7-4B42-CD83-FA55-1CA6886DE2BA";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:Neck5RbnDtlSqsh_Pma";
	rename -uid "89D1E836-43D9-598E-EA7C-A4AB709489B1";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:Neck5RbnDtlSqsh_Mdv";
	rename -uid "60F264E8-44A4-4183-73A2-279EC889917B";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode plusMinusAverage -n "bussaba_001:UpArm1RbnDtlSqsh_L_Pma";
	rename -uid "C6FF287D-4785-91BA-D180-CDBF925ECEB1";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:UpArm1RbnDtlSqsh_L_Mdv";
	rename -uid "87B51329-4BA3-CFF5-D0A8-A2AF91D496E1";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode blendColors -n "bussaba_001:UpArmRbnAutoSqsh_L_Bcl";
	rename -uid "E8723AA9-4E55-A3DF-58C4-E6956BF3BB6B";
	setAttr ".c2" -type "float3" 1 0 1 ;
createNode multiplyDivide -n "bussaba_001:UpArmRbnSqshDiv_L_Mdv";
	rename -uid "D1AC8525-4044-F0BD-BF25-D1B212A54E62";
	setAttr ".op" 2;
	setAttr ".i1" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "bussaba_001:UpArmRbnSqshPow_L_Mdv";
	rename -uid "731046A6-4168-3E8D-0811-14879454F45E";
	setAttr ".op" 3;
	setAttr ".i2" -type "float3" 2 1 1 ;
createNode multiplyDivide -n "bussaba_001:UpArmRbnSqshNorm_L_Mdv";
	rename -uid "6C79EB08-4DFE-87F0-FD89-38B29D143C43";
	setAttr ".op" 2;
createNode plusMinusAverage -n "bussaba_001:UpArm2RbnDtlSqsh_L_Pma";
	rename -uid "C3235A53-446C-BD56-EA42-B6BD8F3F27C8";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:UpArm2RbnDtlSqsh_L_Mdv";
	rename -uid "48F0A7F4-4890-C9B2-7CDE-C38D556421EC";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:UpArm3RbnDtlSqsh_L_Pma";
	rename -uid "5D9AD34B-4F2E-5541-F20E-46BD4D0A73C3";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:UpArm3RbnDtlSqsh_L_Mdv";
	rename -uid "F2C631A8-4F64-5433-E175-E6A7405D7D44";
	setAttr ".i2" -type "float3" 0.5 0.5 1 ;
createNode plusMinusAverage -n "bussaba_001:UpArm4RbnDtlSqsh_L_Pma";
	rename -uid "96FC27C5-48BF-4F30-659E-98AA0F209769";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:UpArm4RbnDtlSqsh_L_Mdv";
	rename -uid "8D63B6B6-4C97-B86B-245A-3DA56DA5EFB6";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:UpArm5RbnDtlSqsh_L_Pma";
	rename -uid "949C3BFC-4A96-A185-C9BF-CC9161AF6AEE";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:UpArm5RbnDtlSqsh_L_Mdv";
	rename -uid "7B18CF65-4E12-A5CC-11D4-1AA4B1699952";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode plusMinusAverage -n "bussaba_001:Forearm1RbnDtlSqsh_L_Pma";
	rename -uid "AC283468-4B7A-853E-08E2-81BFE00A4B2B";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:Forearm1RbnDtlSqsh_L_Mdv";
	rename -uid "7229A64D-45FA-F7B1-3BD8-7388449E0DFA";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode blendColors -n "bussaba_001:ForearmRbnAutoSqsh_L_Bcl";
	rename -uid "2840555F-499F-BCCB-8371-3F9C9F3B2284";
	setAttr ".c2" -type "float3" 1 0 1 ;
createNode multiplyDivide -n "bussaba_001:ForearmRbnSqshDiv_L_Mdv";
	rename -uid "F698A226-4601-F09E-74E8-4D99A8653C2A";
	setAttr ".op" 2;
	setAttr ".i1" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "bussaba_001:ForearmRbnSqshPow_L_Mdv";
	rename -uid "F8AE19AB-4D6F-AFB8-7CC8-35ABF6986D7A";
	setAttr ".op" 3;
	setAttr ".i2" -type "float3" 2 1 1 ;
createNode multiplyDivide -n "bussaba_001:ForearmRbnSqshNorm_L_Mdv";
	rename -uid "8B52AF6F-40E4-99B7-57F1-3BA8F5D26EF7";
	setAttr ".op" 2;
createNode plusMinusAverage -n "bussaba_001:Forearm2RbnDtlSqsh_L_Pma";
	rename -uid "A3705044-4585-7A22-919C-E6A99972A9B6";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:Forearm2RbnDtlSqsh_L_Mdv";
	rename -uid "F321A466-4F0B-6E65-279D-E9BC92DDB9AC";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:Forearm3RbnDtlSqsh_L_Pma";
	rename -uid "745FF5F3-40A5-5532-D383-EA8E3A76F12D";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:Forearm3RbnDtlSqsh_L_Mdv";
	rename -uid "787ED3E8-46EA-D63E-3BBD-50B006AA6739";
	setAttr ".i2" -type "float3" 0.5 0.5 1 ;
createNode plusMinusAverage -n "bussaba_001:Forearm4RbnDtlSqsh_L_Pma";
	rename -uid "FE12605F-41A7-5447-C24B-9B87DB191BC8";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:Forearm4RbnDtlSqsh_L_Mdv";
	rename -uid "3DC28999-47AD-6896-0C18-67AE6301EFF7";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:Forearm5RbnDtlSqsh_L_Pma";
	rename -uid "8994F41F-4EBA-5B27-EDAB-8FAB4C3F9D9B";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:Forearm5RbnDtlSqsh_L_Mdv";
	rename -uid "7914F16D-4C92-95A0-94A2-379B3C1D64A6";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode plusMinusAverage -n "bussaba_001:UpArm1RbnDtlSqsh_R_Pma";
	rename -uid "0074F0CB-476C-BDD7-27E2-D2BD84F914B4";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:UpArm1RbnDtlSqsh_R_Mdv";
	rename -uid "05A73A01-422C-CD67-4AEF-F19F40E48716";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode blendColors -n "bussaba_001:UpArmRbnAutoSqsh_R_Bcl";
	rename -uid "30FDC0E8-474A-3C97-3661-47B63D9B9771";
	setAttr ".c2" -type "float3" 1 0 1 ;
createNode multiplyDivide -n "bussaba_001:UpArmRbnSqshDiv_R_Mdv";
	rename -uid "733F02E6-4251-7557-3444-B49F5910C18C";
	setAttr ".op" 2;
	setAttr ".i1" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "bussaba_001:UpArmRbnSqshPow_R_Mdv";
	rename -uid "9622BF35-45EC-8B3F-8BDF-058D00EF1A08";
	setAttr ".op" 3;
	setAttr ".i2" -type "float3" 2 1 1 ;
createNode multiplyDivide -n "bussaba_001:UpArmRbnSqshNorm_R_Mdv";
	rename -uid "32FF700A-49DF-B459-01F0-2F80AB8074A5";
	setAttr ".op" 2;
createNode plusMinusAverage -n "bussaba_001:UpArm2RbnDtlSqsh_R_Pma";
	rename -uid "09C2C36F-4953-1033-CB5D-058B381F7A2B";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:UpArm2RbnDtlSqsh_R_Mdv";
	rename -uid "CDD813EC-47D3-52EE-A34B-D4BE64A965CC";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:UpArm3RbnDtlSqsh_R_Pma";
	rename -uid "3891BCC4-43B6-F001-8F12-279978B223F1";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:UpArm3RbnDtlSqsh_R_Mdv";
	rename -uid "BC645D86-4F61-864D-8929-599E73A46DAD";
	setAttr ".i2" -type "float3" 0.5 0.5 1 ;
createNode plusMinusAverage -n "bussaba_001:UpArm4RbnDtlSqsh_R_Pma";
	rename -uid "0BFF8CDF-487C-FD64-924F-E582EDBAF6BC";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:UpArm4RbnDtlSqsh_R_Mdv";
	rename -uid "6CEDAA08-4DE5-F250-52FD-6990CF1E82E4";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:UpArm5RbnDtlSqsh_R_Pma";
	rename -uid "AD8B1590-4AFA-F58F-C82F-0B846135C6D1";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:UpArm5RbnDtlSqsh_R_Mdv";
	rename -uid "9FCDD98F-4885-99D2-CCB3-AEAD81616425";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode plusMinusAverage -n "bussaba_001:Forearm1RbnDtlSqsh_R_Pma";
	rename -uid "5B76DFD3-4090-1F25-8379-1293F4CED8FF";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:Forearm1RbnDtlSqsh_R_Mdv";
	rename -uid "1AF5E020-4263-1871-B773-2F86FA18C131";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode blendColors -n "bussaba_001:ForearmRbnAutoSqsh_R_Bcl";
	rename -uid "72B6FD5F-4E86-0B8F-6D84-FD8A30138EC2";
	setAttr ".c2" -type "float3" 1 0 1 ;
createNode multiplyDivide -n "bussaba_001:ForearmRbnSqshDiv_R_Mdv";
	rename -uid "52B17448-40AE-0256-AD79-4C92743598A5";
	setAttr ".op" 2;
	setAttr ".i1" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "bussaba_001:ForearmRbnSqshPow_R_Mdv";
	rename -uid "ECC415E2-4C1C-4BDE-F336-01B2FEADCA54";
	setAttr ".op" 3;
	setAttr ".i2" -type "float3" 2 1 1 ;
createNode multiplyDivide -n "bussaba_001:ForearmRbnSqshNorm_R_Mdv";
	rename -uid "ED4613B9-4489-A1CD-8412-338E13FCB1FE";
	setAttr ".op" 2;
createNode plusMinusAverage -n "bussaba_001:Forearm2RbnDtlSqsh_R_Pma";
	rename -uid "B95E954B-45BC-025B-C664-FA814A49CCA5";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:Forearm2RbnDtlSqsh_R_Mdv";
	rename -uid "3D77E762-4F05-9EF5-D80C-169425476F26";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:Forearm3RbnDtlSqsh_R_Pma";
	rename -uid "424D8263-400D-3451-A90C-8F988B55011A";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:Forearm3RbnDtlSqsh_R_Mdv";
	rename -uid "1A2DEA36-4D6B-D42C-C3E0-FDB5F93531BC";
	setAttr ".i2" -type "float3" 0.5 0.5 1 ;
createNode plusMinusAverage -n "bussaba_001:Forearm4RbnDtlSqsh_R_Pma";
	rename -uid "866D4B0E-4EE2-1F14-142A-73A9FFD52245";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:Forearm4RbnDtlSqsh_R_Mdv";
	rename -uid "BDAC6A2B-42FC-5BC8-4213-5CB069601B80";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:Forearm5RbnDtlSqsh_R_Pma";
	rename -uid "2A1BC9D5-4A66-1352-B981-B5A12538FBFA";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:Forearm5RbnDtlSqsh_R_Mdv";
	rename -uid "A7A84540-454E-FB27-3A96-8DA18571D3A4";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode plusMinusAverage -n "bussaba_001:UpLeg1RbnDtlSqsh_L_Pma";
	rename -uid "08948C12-4EAF-8F0F-183B-F48B99BDD6FD";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:UpLeg1RbnDtlSqsh_L_Mdv";
	rename -uid "ADE48A73-4E8A-AEA7-9853-C29D293F5DBB";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode blendColors -n "bussaba_001:UpLegRbnAutoSqsh_L_Bcl";
	rename -uid "3A650173-466C-EFEE-BCA4-50AA48824A5A";
	setAttr ".c2" -type "float3" 1 0 1 ;
createNode multiplyDivide -n "bussaba_001:UpLegRbnSqshDiv_L_Mdv";
	rename -uid "68B594DE-4295-58CD-ABD9-AEAE4FCB0525";
	setAttr ".op" 2;
	setAttr ".i1" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "bussaba_001:UpLegRbnSqshPow_L_Mdv";
	rename -uid "4D559C22-44CE-D649-45E9-15AC7A970D2E";
	setAttr ".op" 3;
	setAttr ".i2" -type "float3" 2 1 1 ;
createNode multiplyDivide -n "bussaba_001:UpLegRbnSqshNorm_L_Mdv";
	rename -uid "201B567E-4B91-4E42-A1D0-749402377032";
	setAttr ".op" 2;
createNode plusMinusAverage -n "bussaba_001:UpLeg2RbnDtlSqsh_L_Pma";
	rename -uid "AF551451-4D5A-4D46-3F7B-25A6AD5BD864";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:UpLeg2RbnDtlSqsh_L_Mdv";
	rename -uid "0C2AAF6F-4F5E-C342-91B3-F982C85AE246";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:UpLeg3RbnDtlSqsh_L_Pma";
	rename -uid "2A39F486-4B2B-7BB4-6AF9-B4A352AC7313";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:UpLeg3RbnDtlSqsh_L_Mdv";
	rename -uid "05AA4043-4331-5422-3BC8-3389F52D5032";
	setAttr ".i2" -type "float3" 0.5 0.5 1 ;
createNode plusMinusAverage -n "bussaba_001:UpLeg4RbnDtlSqsh_L_Pma";
	rename -uid "523EFD4B-4061-FCAF-34BB-2D86E85713EA";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:UpLeg4RbnDtlSqsh_L_Mdv";
	rename -uid "EE877024-4165-35A0-F4BB-A3804E959006";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:UpLeg5RbnDtlSqsh_L_Pma";
	rename -uid "06056536-4496-C2D5-6D33-EEB4DC0ED5B3";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:UpLeg5RbnDtlSqsh_L_Mdv";
	rename -uid "50A8FA72-4ABE-062E-51D3-8AB51B322FF1";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode plusMinusAverage -n "bussaba_001:LowLeg1RbnDtlSqsh_L_Pma";
	rename -uid "4077B431-4849-2FEF-FFCD-BF8878512A57";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:LowLeg1RbnDtlSqsh_L_Mdv";
	rename -uid "302D0ACC-4CC7-C5F1-5F24-9CBA18EB67F8";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode blendColors -n "bussaba_001:LowLegRbnAutoSqsh_L_Bcl";
	rename -uid "951E651D-465A-AB09-C949-5DA2EFE91FC3";
	setAttr ".c2" -type "float3" 1 0 1 ;
createNode multiplyDivide -n "bussaba_001:LowLegRbnSqshDiv_L_Mdv";
	rename -uid "D5A9F70E-4AF8-7AAD-98D6-8E9A1B61D4D7";
	setAttr ".op" 2;
	setAttr ".i1" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "bussaba_001:LowLegRbnSqshPow_L_Mdv";
	rename -uid "A5AED214-4921-D66C-A88C-AABE3BF92438";
	setAttr ".op" 3;
	setAttr ".i2" -type "float3" 2 1 1 ;
createNode multiplyDivide -n "bussaba_001:LowLegRbnSqshNorm_L_Mdv";
	rename -uid "07BE80E6-44DE-E9E3-957B-498DF4070A18";
	setAttr ".op" 2;
createNode plusMinusAverage -n "bussaba_001:LowLeg2RbnDtlSqsh_L_Pma";
	rename -uid "B85DC93A-421A-A0EF-31D1-949E7D223FF3";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:LowLeg2RbnDtlSqsh_L_Mdv";
	rename -uid "729002E1-4F6E-AA58-0A7E-088D74851C71";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:LowLeg3RbnDtlSqsh_L_Pma";
	rename -uid "206306B5-4783-9CAB-BCAC-28BB44BE8EB7";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:LowLeg3RbnDtlSqsh_L_Mdv";
	rename -uid "055A7095-435C-FF29-8F1A-A7A0A224F279";
	setAttr ".i2" -type "float3" 0.5 0.5 1 ;
createNode plusMinusAverage -n "bussaba_001:LowLeg4RbnDtlSqsh_L_Pma";
	rename -uid "5D9CA6D6-4119-D86C-B921-1C9E855D6FA9";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:LowLeg4RbnDtlSqsh_L_Mdv";
	rename -uid "15891F22-44DB-122B-236A-7DB78318C415";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:LowLeg5RbnDtlSqsh_L_Pma";
	rename -uid "36306653-4C50-6332-A137-589A0ECFE624";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:LowLeg5RbnDtlSqsh_L_Mdv";
	rename -uid "DA18EEE8-4F2C-343B-81E1-8DBBEF2809E5";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode plusMinusAverage -n "bussaba_001:UpLeg1RbnDtlSqsh_R_Pma";
	rename -uid "171B4FA1-4991-5ABA-0C22-F4A342F4DDE7";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:UpLeg1RbnDtlSqsh_R_Mdv";
	rename -uid "BF9DC8FA-4889-C1ED-0C28-D7B0DDA3CAAB";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode blendColors -n "bussaba_001:UpLegRbnAutoSqsh_R_Bcl";
	rename -uid "53FA3521-4AEC-2DFB-D5ED-9EAB74AD5E18";
	setAttr ".c2" -type "float3" 1 0 1 ;
createNode multiplyDivide -n "bussaba_001:UpLegRbnSqshDiv_R_Mdv";
	rename -uid "5F5A5240-47A6-6EF0-EB2C-90A6E7905FB0";
	setAttr ".op" 2;
	setAttr ".i1" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "bussaba_001:UpLegRbnSqshPow_R_Mdv";
	rename -uid "8CB04E76-4F50-EA1F-BE48-17BF3D34FA25";
	setAttr ".op" 3;
	setAttr ".i2" -type "float3" 2 1 1 ;
createNode multiplyDivide -n "bussaba_001:UpLegRbnSqshNorm_R_Mdv";
	rename -uid "C2D04F14-458E-2EC7-1E45-2D9E7FEFDB28";
	setAttr ".op" 2;
createNode plusMinusAverage -n "bussaba_001:UpLeg2RbnDtlSqsh_R_Pma";
	rename -uid "289CC0FE-4666-93DA-2202-08B3DB4C5D6B";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:UpLeg2RbnDtlSqsh_R_Mdv";
	rename -uid "611EDE99-4CE4-721B-68B0-A09C05DFF7AB";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:UpLeg3RbnDtlSqsh_R_Pma";
	rename -uid "2DE02F3E-48F0-27C9-95D1-6A841F330C13";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:UpLeg3RbnDtlSqsh_R_Mdv";
	rename -uid "5A6AA119-4095-7FC3-8F79-94A6F2ED2ED8";
	setAttr ".i2" -type "float3" 0.5 0.5 1 ;
createNode plusMinusAverage -n "bussaba_001:UpLeg4RbnDtlSqsh_R_Pma";
	rename -uid "A5CDE280-4566-E2E4-F0C7-2B83940228C9";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:UpLeg4RbnDtlSqsh_R_Mdv";
	rename -uid "04DE3C69-465E-5289-7E0F-9ABC52571D5C";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:UpLeg5RbnDtlSqsh_R_Pma";
	rename -uid "CAE76FD0-4D31-1C2B-B704-DDBF72AA3ECF";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:UpLeg5RbnDtlSqsh_R_Mdv";
	rename -uid "09F12D4D-47A8-E38B-947E-68BBDBF7A3FC";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode plusMinusAverage -n "bussaba_001:LowLeg1RbnDtlSqsh_R_Pma";
	rename -uid "2A20C612-4782-9E79-8185-03B76EB6304A";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:LowLeg1RbnDtlSqsh_R_Mdv";
	rename -uid "2CD95BF9-4F21-8C94-AF79-5B8DF3046DF2";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode blendColors -n "bussaba_001:LowLegRbnAutoSqsh_R_Bcl";
	rename -uid "8866D5F4-4B18-05E4-44B6-A38541D083C1";
	setAttr ".c2" -type "float3" 1 0 1 ;
createNode multiplyDivide -n "bussaba_001:LowLegRbnSqshDiv_R_Mdv";
	rename -uid "ACB90883-45FC-4ED5-0302-BEA04E8C003D";
	setAttr ".op" 2;
	setAttr ".i1" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "bussaba_001:LowLegRbnSqshPow_R_Mdv";
	rename -uid "B57D5E4D-4C36-63F5-339A-D4B4558FAE96";
	setAttr ".op" 3;
	setAttr ".i2" -type "float3" 2 1 1 ;
createNode multiplyDivide -n "bussaba_001:LowLegRbnSqshNorm_R_Mdv";
	rename -uid "783E8C33-45A5-6863-2F1B-3AA7F1DDD2C4";
	setAttr ".op" 2;
createNode plusMinusAverage -n "bussaba_001:LowLeg2RbnDtlSqsh_R_Pma";
	rename -uid "2C54E3D0-4A80-5049-ECF9-D0BB8E4C4FEE";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:LowLeg2RbnDtlSqsh_R_Mdv";
	rename -uid "75A65964-455C-389E-EFEF-E2A438148613";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:LowLeg3RbnDtlSqsh_R_Pma";
	rename -uid "7E170134-4378-62CD-B411-14A61156F047";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:LowLeg3RbnDtlSqsh_R_Mdv";
	rename -uid "37EC1250-43F9-E272-1D37-F6856E2474B9";
	setAttr ".i2" -type "float3" 0.5 0.5 1 ;
createNode plusMinusAverage -n "bussaba_001:LowLeg4RbnDtlSqsh_R_Pma";
	rename -uid "E161A923-4015-BCDA-E840-9B87AE7A2305";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.34000000357627869;
createNode multiplyDivide -n "bussaba_001:LowLeg4RbnDtlSqsh_R_Mdv";
	rename -uid "C24BCA6D-45D3-642E-1A97-3A8C32A164CB";
	setAttr ".i2" -type "float3" 0.5 0.5 0.66000003 ;
createNode plusMinusAverage -n "bussaba_001:LowLeg5RbnDtlSqsh_R_Pma";
	rename -uid "53B798BC-4F03-98AF-78AC-C0912BD7BBA8";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 4 ".i1";
	setAttr -s 4 ".i1";
	setAttr -k on ".default" 0.67000001668930054;
createNode multiplyDivide -n "bussaba_001:LowLeg5RbnDtlSqsh_R_Mdv";
	rename -uid "71E858F6-47B4-3385-2BAD-7C875D23508B";
	setAttr ".i2" -type "float3" 0.5 0.5 0.33000001 ;
createNode plusMinusAverage -n "bussaba_001:Breast1Sqsh_L_Pma";
	rename -uid "A1F71D2C-417F-3A7C-2197-9ABDAFB318C1";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Breast1Sqsh_L_Mdv";
	rename -uid "8E912771-4213-C17E-1B6F-C28C941E4BAC";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Breast2Sqsh_L_Pma";
	rename -uid "BA09C460-414F-0F9B-B944-74BCE1F95643";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Breast2Sqsh_L_Mdv";
	rename -uid "1E0629C1-42C8-FBBB-DAAA-D9B6D6F6CF6F";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Breast2Strt_L_Pma";
	rename -uid "5BFE95F6-48AB-8F26-1316-BF8AA251CC90";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 0.01315497700124979;
createNode multiplyDivide -n "bussaba_001:Breast2Strt_L_Mdv";
	rename -uid "947F723E-4B5B-6779-9A1B-30ADD9A61B70";
createNode plusMinusAverage -n "bussaba_001:Breast1Sqsh_R_Pma";
	rename -uid "8DB0C58D-43D1-48C0-FBAD-638D8C3850B3";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Breast1Sqsh_R_Mdv";
	rename -uid "A53635B4-4EF1-B81A-2794-44996C3CA045";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Breast2Sqsh_R_Pma";
	rename -uid "70E3B9D4-4B95-8282-DD53-EDAD35B01FBF";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 1;
createNode multiplyDivide -n "bussaba_001:Breast2Sqsh_R_Mdv";
	rename -uid "E8B51398-4859-F3B6-248D-8794B59BC596";
	setAttr ".i2" -type "float3" 0.5 1 1 ;
createNode plusMinusAverage -n "bussaba_001:Breast2Strt_R_Pma";
	rename -uid "F7C8AACC-4817-1B3A-284E-B6912D9DBB9B";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" -0.013156550005078316;
createNode multiplyDivide -n "bussaba_001:Breast2Strt_R_Mdv";
	rename -uid "2F18BFBA-4B70-A983-DEAE-D394E554D0D8";
createNode plusMinusAverage -n "bussaba_001:BallFkStrt_L_Pma";
	rename -uid "A7BD8947-49E6-B074-B711-B485169748B9";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 0.31991550326347351;
createNode multiplyDivide -n "bussaba_001:BallFkStrt_L_Mdv";
	rename -uid "DB12E6DC-42DE-84D9-5178-B897DD1D1DA7";
createNode plusMinusAverage -n "bussaba_001:ToeIkStrt_L_Pma";
	rename -uid "91B86A77-4740-5663-1326-D88FE4BAD412";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" 0.31991550326347351;
createNode multiplyDivide -n "bussaba_001:ToeIkStrt_L_Mdv";
	rename -uid "3439DE2D-4B99-D54A-E3F9-03B5A9B7CF26";
createNode plusMinusAverage -n "bussaba_001:BallFkStrt_R_Pma";
	rename -uid "618A7645-4377-F267-B35A-288F1E08F27B";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" -0.3199160099029541;
createNode multiplyDivide -n "bussaba_001:BallFkStrt_R_Mdv";
	rename -uid "85D664D9-4C9F-156B-9B1C-3B9D8EF6FCC7";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode plusMinusAverage -n "bussaba_001:ToeIkStrt_R_Pma";
	rename -uid "28A798F6-4A32-8B7D-9794-DCA36161ECA9";
	addAttr -ci true -k true -sn "default" -ln "default" -at "float";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
	setAttr -k on ".default" -0.3199160099029541;
createNode multiplyDivide -n "bussaba_001:ToeIkStrt_R_Mdv";
	rename -uid "8D7DC466-43C8-C9E0-B2C3-4294A6E71E86";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode blendTwoAttr -n "bussaba_001:RbnHdUpHdDfmRigAtSqs_Blnd";
	rename -uid "4D140CB0-4520-F5C0-852C-C7920ACD8C01";
	addAttr -ci true -k true -sn "default" -ln "default" -dv 1 -min 1 -max 1 -at "double";
	setAttr -s 2 ".i";
	setAttr -s 2 ".i";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:RbnHdUpHdDfmRigSqsDiv_Mdv";
	rename -uid "F242D803-476C-C95A-6B70-74BF91241C51";
	setAttr ".op" 2;
	setAttr ".i1" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "bussaba_001:RbnHdUpHdDfmRigSqsPow_Mdv";
	rename -uid "2059A20A-46EB-6473-CC78-C6BE7CC767CA";
	setAttr ".op" 3;
	setAttr ".i2" -type "float3" 2 1 1 ;
createNode multiplyDivide -n "bussaba_001:RbnHdUpHdDfmRigSqsNorm_Mdv";
	rename -uid "42F0573A-4E72-16CC-3AA0-DCB0D0EFE53B";
	setAttr ".op" 2;
createNode blendTwoAttr -n "bussaba_001:RbnHdLowHdDfmRigAtSqs_Blnd";
	rename -uid "E22D6CEE-4964-1A6B-897F-EA8E56CDC669";
	addAttr -ci true -k true -sn "default" -ln "default" -dv 1 -min 1 -max 1 -at "double";
	setAttr -s 2 ".i";
	setAttr -s 2 ".i";
	setAttr -k on ".default";
createNode multiplyDivide -n "bussaba_001:RbnHdLowHdDfmRigSqsDiv_Mdv";
	rename -uid "527B1A70-47C0-2B0D-041D-3B8378BB649D";
	setAttr ".op" 2;
	setAttr ".i1" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "bussaba_001:RbnHdLowHdDfmRigSqsPow_Mdv";
	rename -uid "E4D6CCC4-4F80-2C97-0AD5-A496BB82BB4F";
	setAttr ".op" 3;
	setAttr ".i2" -type "float3" 2 1 1 ;
createNode multiplyDivide -n "bussaba_001:RbnHdLowHdDfmRigSqsNorm_Mdv";
	rename -uid "35642C7E-4678-5C81-6AA4-7885188FE79F";
	setAttr ".op" 2;
createNode renderLayerManager -n "bussaba_001:renderLayerManager";
	rename -uid "FB7C87B4-4DCE-72C6-0B0A-AD9FA93C1C1D";
createNode renderLayer -n "bussaba_001:defaultRenderLayer";
	rename -uid "103AD589-4D64-2E9B-6C34-55BB80984F42";
	setAttr ".g" yes;
createNode nodeGraphEditorInfo -n "facialDirTmp_hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "9BCE7841-4E54-33A5-8F6D-BFAF837F2F46";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -330.95236780151544 -323.80951094248991 ;
	setAttr ".tgi[0].vh" -type "double2" 317.85713022663526 338.09522466054096 ;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo1";
	rename -uid "3EE1BF65-40F3-1AE9-6086-CEA63C37797B";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -229.76189563198733 -38.095236581469415 ;
	setAttr ".tgi[0].vh" -type "double2" 221.42856262979089 222.6190387729618 ;
createNode lambert -n "lambert61";
	rename -uid "FD84DAAD-419C-FD26-0CC3-E68D2756860B";
	setAttr ".c" -type "float3" 1 0 0 ;
createNode shadingEngine -n "lambert61SG";
	rename -uid "3325EF3D-4361-ABCD-3263-89980DBF87D3";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo20";
	rename -uid "7D464D74-4EED-59CA-5447-8C9539811E6E";
createNode nodeGraphEditorInfo -n "MayaNodeEditorSavedTabsInfo";
	rename -uid "9E19D853-41C9-FC96-3318-509547F22D9C";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -1183.1413106743016 -549.404740073379 ;
	setAttr ".tgi[0].vh" -type "double2" 1184.3317868174727 549.404740073379 ;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo2";
	rename -uid "03C12155-4DFB-BBF9-4F48-718A1D01E735";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -140.47618489416843 -134.52380417831384 ;
	setAttr ".tgi[0].vh" -type "double2" 138.0952326078266 138.0952326078266 ;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 0;
	setAttr -av -k on ".unw";
	setAttr -av -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 3 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
	setAttr -s 2 ".r";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".bbx";
	setAttr -k on ".vwm";
	setAttr -k on ".tpv";
	setAttr -k on ".uit";
	setAttr -s 3 ".dsm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro";
select -ne :defaultRenderGlobals;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".macc";
	setAttr -av -k on ".macd";
	setAttr -av -k on ".macq";
	setAttr -av -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -av -k on ".clip";
	setAttr -av -k on ".edm";
	setAttr -av -k on ".edl";
	setAttr -av -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -av -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -av -cb on ".imfkey";
	setAttr -av -k on ".gama";
	setAttr -k on ".exrc";
	setAttr -k on ".expt";
	setAttr -av -k on ".an";
	setAttr -cb on ".ar";
	setAttr -av -k on ".fs";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -av -k on ".be";
	setAttr -av -cb on ".ep";
	setAttr -av -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -av -k on ".pff";
	setAttr -av -cb on ".peie";
	setAttr -av -cb on ".ifp";
	setAttr -k on ".rv";
	setAttr -av -k on ".comp";
	setAttr -av -k on ".cth";
	setAttr -av -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -av -k on ".rd";
	setAttr -av -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -av -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -av -k on ".mm";
	setAttr -av -k on ".npu";
	setAttr -av -k on ".itf";
	setAttr -av -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -av -k on ".uf";
	setAttr -av -k on ".oi";
	setAttr -av -k on ".rut";
	setAttr -av -k on ".mot";
	setAttr -av -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -av -k on ".mbso";
	setAttr -av -k on ".mbsc";
	setAttr -av -k on ".afp";
	setAttr -av -k on ".pfb";
	setAttr -av -k on ".pram" -type "string" "";
	setAttr -av -k on ".poam";
	setAttr -av -k on ".prlm";
	setAttr -av -k on ".polm";
	setAttr -av -cb on ".prm";
	setAttr -av -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -av -k on ".ubc";
	setAttr -av -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -av -k on ".udbx";
	setAttr -av -k on ".smc";
	setAttr -av -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -av -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -av -k on ".tlwd";
	setAttr -av -k on ".tlht";
	setAttr -av -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -av -k on ".ope";
	setAttr -av -k on ".oppf";
	setAttr -av -k on ".rcp";
	setAttr -av -k on ".icp";
	setAttr -av -k on ".ocp";
	setAttr -cb on ".hbl";
connectAttr "JawLwr1_TmpJnt.s" "JawLwr2_TmpJnt.is";
connectAttr "JawLwr2_TmpJnt.s" "JawLwrEnd_TmpJnt.is";
connectAttr "JawLwr1_TmpJnt.s" "tongue1_TmpJnt.is";
connectAttr "tongue1_TmpJnt.s" "tongue2_TmpJnt.is";
connectAttr "tongue2_TmpJnt.s" "tongue3_TmpJnt.is";
connectAttr "tongue3_TmpJnt.s" "tongue4_TmpJnt.is";
connectAttr "tongue4_TmpJnt.s" "tongue5_TmpJnt.is";
connectAttr "tongue5_TmpJnt.s" "tongue6_TmpJnt.is";
connectAttr "JawUpr1_TmpJnt.s" "JawUpr2_TmpJnt.is";
connectAttr "EyeTrgt_TmpJnt.s" "EyeTrgt_L_TmpJnt.is";
connectAttr "EyeTrgt_TmpJnt.s" "EyeTrgt_R_TmpJnt.is";
connectAttr "Eye_R_TmpJnt.s" "EyeInner_R_TmpJnt.is";
connectAttr "Eye_R_TmpJnt.s" "EyeOuter_R_TmpJnt.is";
connectAttr "Eye_R_TmpJnt.s" "EyeLidUpr_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr1_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr2_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr3_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr4_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr5_R_TmpJnt.is";
connectAttr "EyeLidUpr_R_TmpJnt.s" "EyeLidUpr6_R_TmpJnt.is";
connectAttr "Eye_R_TmpJnt.s" "EyeLidLwr_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr1_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr2_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr3_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr4_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr5_R_TmpJnt.is";
connectAttr "EyeLidLwr_R_TmpJnt.s" "EyeLidLwr6_R_TmpJnt.is";
connectAttr "Eye_L_TmpJnt.s" "EyeInner_L_TmpJnt.is";
connectAttr "Eye_L_TmpJnt.s" "EyeOuter_L_TmpJnt.is";
connectAttr "Eye_L_TmpJnt.s" "EyeLidUpr_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr1_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr2_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr3_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr4_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr5_L_TmpJnt.is";
connectAttr "EyeLidUpr_L_TmpJnt.s" "EyeLidUpr6_L_TmpJnt.is";
connectAttr "Eye_L_TmpJnt.s" "EyeLidLwr_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr1_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr2_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr3_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr4_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr5_L_TmpJnt.is";
connectAttr "EyeLidLwr_L_TmpJnt.s" "EyeLidLwr6_L_TmpJnt.is";
connectAttr "teethMainUp_TmpJnt.s" "teethUp3_TmpJnt.is";
connectAttr "teethMainUp_TmpJnt.s" "teethUp2_TmpJnt.is";
connectAttr "teethMainUp_TmpJnt.s" "teethUp1_TmpJnt.is";
connectAttr "teethMainUp_TmpJnt.s" "teethUp4_TmpJnt.is";
connectAttr "teethMainUp_TmpJnt.s" "teethUp5_TmpJnt.is";
connectAttr "teethMainDn_TmpJnt.s" "teethDn3_TmpJnt.is";
connectAttr "teethMainDn_TmpJnt.s" "teethDn2_TmpJnt.is";
connectAttr "teethMainDn_TmpJnt.s" "teethDn1_TmpJnt.is";
connectAttr "teethMainDn_TmpJnt.s" "teethDn4_TmpJnt.is";
connectAttr "teethMainDn_TmpJnt.s" "teethDn5_TmpJnt.is";
connectAttr "nose_TmpJnt.s" "nose_L_TmpJnt.is";
connectAttr "nose_TmpJnt.s" "nose_R_TmpJnt.is";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert61SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert61SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "bussaba_001:JawLwr1Drv_Pma.constant" "bussaba_001:JawLwr1Drv_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpArmFkStrt_L_Pma.default" "bussaba_001:UpArmFkStrt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpArmFkStrt_L_Mdv.ox" "bussaba_001:UpArmFkStrt_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:ForearmFkStrt_L_Pma.default" "bussaba_001:ForearmFkStrt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:ForearmFkStrt_L_Mdv.ox" "bussaba_001:ForearmFkStrt_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpArmFkStrt_R_Pma.default" "bussaba_001:UpArmFkStrt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpArmFkStrt_R_Mdv.ox" "bussaba_001:UpArmFkStrt_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:ForearmFkStrt_R_Pma.default" "bussaba_001:ForearmFkStrt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:ForearmFkStrt_R_Mdv.ox" "bussaba_001:ForearmFkStrt_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpLegFkStrt_L_Pma.default" "bussaba_001:UpLegFkStrt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpLegFkStrt_L_Mdv.ox" "bussaba_001:UpLegFkStrt_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:LowLegFkStrt_L_Pma.default" "bussaba_001:LowLegFkStrt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:LowLegFkStrt_L_Mdv.ox" "bussaba_001:LowLegFkStrt_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpLegFkStrt_R_Pma.default" "bussaba_001:UpLegFkStrt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpLegFkStrt_R_Mdv.ox" "bussaba_001:UpLegFkStrt_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:LowLegFkStrt_R_Pma.default" "bussaba_001:LowLegFkStrt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:LowLegFkStrt_R_Mdv.ox" "bussaba_001:LowLegFkStrt_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Hair1Strt_Pma.default" "bussaba_001:Hair1Strt_Pma.i1[0]"
		;
connectAttr "bussaba_001:Hair1Strt_Mdv.ox" "bussaba_001:Hair1Strt_Pma.i1[1]";
connectAttr "bussaba_001:Hair2Strt_Pma.default" "bussaba_001:Hair2Strt_Pma.i1[0]"
		;
connectAttr "bussaba_001:Hair2Strt_Mdv.ox" "bussaba_001:Hair2Strt_Pma.i1[1]";
connectAttr "bussaba_001:Hair3Strt_Pma.default" "bussaba_001:Hair3Strt_Pma.i1[0]"
		;
connectAttr "bussaba_001:Hair3Strt_Mdv.ox" "bussaba_001:Hair3Strt_Pma.i1[1]";
connectAttr "bussaba_001:Hair4Strt_Pma.default" "bussaba_001:Hair4Strt_Pma.i1[0]"
		;
connectAttr "bussaba_001:Hair4Strt_Mdv.ox" "bussaba_001:Hair4Strt_Pma.i1[1]";
connectAttr "bussaba_001:Hair5Strt_Pma.default" "bussaba_001:Hair5Strt_Pma.i1[0]"
		;
connectAttr "bussaba_001:Hair5Strt_Mdv.ox" "bussaba_001:Hair5Strt_Pma.i1[1]";
connectAttr "bussaba_001:Hair6Strt_Pma.default" "bussaba_001:Hair6Strt_Pma.i1[0]"
		;
connectAttr "bussaba_001:Hair6Strt_Mdv.ox" "bussaba_001:Hair6Strt_Pma.i1[1]";
connectAttr "bussaba_001:HairSide1Strt_L_Pma.default" "bussaba_001:HairSide1Strt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide1Strt_L_Mdv.ox" "bussaba_001:HairSide1Strt_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide2Strt_L_Pma.default" "bussaba_001:HairSide2Strt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide2Strt_L_Mdv.ox" "bussaba_001:HairSide2Strt_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide3Strt_L_Pma.default" "bussaba_001:HairSide3Strt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide3Strt_L_Mdv.ox" "bussaba_001:HairSide3Strt_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide4Strt_L_Pma.default" "bussaba_001:HairSide4Strt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide4Strt_L_Mdv.ox" "bussaba_001:HairSide4Strt_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide5Strt_L_Pma.default" "bussaba_001:HairSide5Strt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide5Strt_L_Mdv.ox" "bussaba_001:HairSide5Strt_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide1Strt_R_Pma.default" "bussaba_001:HairSide1Strt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide1Strt_R_Mdv.ox" "bussaba_001:HairSide1Strt_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide2Strt_R_Pma.default" "bussaba_001:HairSide2Strt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide2Strt_R_Mdv.ox" "bussaba_001:HairSide2Strt_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide3Strt_R_Pma.default" "bussaba_001:HairSide3Strt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide3Strt_R_Mdv.ox" "bussaba_001:HairSide3Strt_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide4Strt_R_Pma.default" "bussaba_001:HairSide4Strt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide4Strt_R_Mdv.ox" "bussaba_001:HairSide4Strt_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide5Strt_R_Pma.default" "bussaba_001:HairSide5Strt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide5Strt_R_Mdv.ox" "bussaba_001:HairSide5Strt_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Breast1Strt_L_Pma.default" "bussaba_001:Breast1Strt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Breast1Strt_L_Mdv.ox" "bussaba_001:Breast1Strt_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Breast1Strt_R_Pma.default" "bussaba_001:Breast1Strt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Breast1Strt_R_Mdv.ox" "bussaba_001:Breast1Strt_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:PelvisSqsh_Pma.default" "bussaba_001:PelvisSqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:PelvisSqsh_Mdv.ox" "bussaba_001:PelvisSqsh_Pma.i1[1]";
connectAttr "bussaba_001:Spine1Sqsh_Pma.default" "bussaba_001:Spine1Sqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Spine1Sqsh_Mdv.ox" "bussaba_001:Spine1Sqsh_Pma.i1[1]";
connectAttr "bussaba_001:Spine1Sqsh_Mdv.oy" "bussaba_001:Spine1Sqsh_Pma.i1[2]";
connectAttr "bussaba_001:Spine1Sqsh_Mdv.oz" "bussaba_001:Spine1Sqsh_Pma.i1[3]";
connectAttr "bussaba_001:SpineAutoSqsh_Bcl.opr" "bussaba_001:Spine1Sqsh_Mdv.i1z"
		;
connectAttr "bussaba_001:SpineSqshDiv_Mdv.ox" "bussaba_001:SpineAutoSqsh_Bcl.c1r"
		;
connectAttr "bussaba_001:SpineSqshPow_Mdv.ox" "bussaba_001:SpineSqshDiv_Mdv.i2x"
		;
connectAttr "bussaba_001:SpineSqshNorm_Mdv.ox" "bussaba_001:SpineSqshPow_Mdv.i1x"
		;
connectAttr "bussaba_001:Spine1Brth_Pma.default" "bussaba_001:Spine1Brth_Pma.i1[0]"
		;
connectAttr "bussaba_001:Spine1Brth_Mdv.ox" "bussaba_001:Spine1Brth_Pma.i1[1]";
connectAttr "bussaba_001:Spine1Brth_Mdv.oy" "bussaba_001:Spine1Brth_Pma.i1[2]";
connectAttr "bussaba_001:Spine2Sqsh_Pma.default" "bussaba_001:Spine2Sqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Spine2Sqsh_Mdv.ox" "bussaba_001:Spine2Sqsh_Pma.i1[1]";
connectAttr "bussaba_001:Spine2Sqsh_Mdv.oy" "bussaba_001:Spine2Sqsh_Pma.i1[2]";
connectAttr "bussaba_001:Spine2Sqsh_Mdv.oz" "bussaba_001:Spine2Sqsh_Pma.i1[3]";
connectAttr "bussaba_001:SpineAutoSqsh_Bcl.opr" "bussaba_001:Spine2Sqsh_Mdv.i1z"
		;
connectAttr "bussaba_001:Spine2Brth_Pma.default" "bussaba_001:Spine2Brth_Pma.i1[0]"
		;
connectAttr "bussaba_001:Spine2Brth_Mdv.ox" "bussaba_001:Spine2Brth_Pma.i1[1]";
connectAttr "bussaba_001:Spine2Brth_Mdv.oy" "bussaba_001:Spine2Brth_Pma.i1[2]";
connectAttr "bussaba_001:Spine3Sqsh_Pma.default" "bussaba_001:Spine3Sqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Spine3Sqsh_Mdv.ox" "bussaba_001:Spine3Sqsh_Pma.i1[1]";
connectAttr "bussaba_001:Spine3Sqsh_Mdv.oy" "bussaba_001:Spine3Sqsh_Pma.i1[2]";
connectAttr "bussaba_001:Spine3Sqsh_Mdv.oz" "bussaba_001:Spine3Sqsh_Pma.i1[3]";
connectAttr "bussaba_001:SpineAutoSqsh_Bcl.opr" "bussaba_001:Spine3Sqsh_Mdv.i1z"
		;
connectAttr "bussaba_001:Spine3Brth_Pma.default" "bussaba_001:Spine3Brth_Pma.i1[0]"
		;
connectAttr "bussaba_001:Spine3Brth_Mdv.ox" "bussaba_001:Spine3Brth_Pma.i1[1]";
connectAttr "bussaba_001:Spine3Brth_Mdv.oy" "bussaba_001:Spine3Brth_Pma.i1[2]";
connectAttr "bussaba_001:Spine4Sqsh_Pma.default" "bussaba_001:Spine4Sqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Spine4Sqsh_Mdv.ox" "bussaba_001:Spine4Sqsh_Pma.i1[1]";
connectAttr "bussaba_001:Spine4Sqsh_Mdv.oy" "bussaba_001:Spine4Sqsh_Pma.i1[2]";
connectAttr "bussaba_001:Spine4Sqsh_Mdv.oz" "bussaba_001:Spine4Sqsh_Pma.i1[3]";
connectAttr "bussaba_001:SpineAutoSqsh_Bcl.opr" "bussaba_001:Spine4Sqsh_Mdv.i1z"
		;
connectAttr "bussaba_001:Spine4Brth_Pma.default" "bussaba_001:Spine4Brth_Pma.i1[0]"
		;
connectAttr "bussaba_001:Spine4Brth_Mdv.ox" "bussaba_001:Spine4Brth_Pma.i1[1]";
connectAttr "bussaba_001:Spine4Brth_Mdv.oy" "bussaba_001:Spine4Brth_Pma.i1[2]";
connectAttr "bussaba_001:Spine5Sqsh_Pma.default" "bussaba_001:Spine5Sqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Spine5Sqsh_Mdv.ox" "bussaba_001:Spine5Sqsh_Pma.i1[1]";
connectAttr "bussaba_001:Spine5Sqsh_Mdv.oy" "bussaba_001:Spine5Sqsh_Pma.i1[2]";
connectAttr "bussaba_001:Spine5Sqsh_Mdv.oz" "bussaba_001:Spine5Sqsh_Pma.i1[3]";
connectAttr "bussaba_001:SpineAutoSqsh_Bcl.opr" "bussaba_001:Spine5Sqsh_Mdv.i1z"
		;
connectAttr "bussaba_001:Spine5Brth_Pma.default" "bussaba_001:Spine5Brth_Pma.i1[0]"
		;
connectAttr "bussaba_001:Spine5Brth_Mdv.ox" "bussaba_001:Spine5Brth_Pma.i1[1]";
connectAttr "bussaba_001:Spine5Brth_Mdv.oy" "bussaba_001:Spine5Brth_Pma.i1[2]";
connectAttr "bussaba_001:NeckStrt_Pma.default" "bussaba_001:NeckStrt_Pma.i1[0]";
connectAttr "bussaba_001:NeckStrt_Mdv.ox" "bussaba_001:NeckStrt_Pma.i1[1]";
connectAttr "bussaba_001:Hair1Sqsh_Pma.default" "bussaba_001:Hair1Sqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Hair1Sqsh_Mdv.ox" "bussaba_001:Hair1Sqsh_Pma.i1[1]";
connectAttr "bussaba_001:Hair2Sqsh_Pma.default" "bussaba_001:Hair2Sqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Hair2Sqsh_Mdv.ox" "bussaba_001:Hair2Sqsh_Pma.i1[1]";
connectAttr "bussaba_001:Hair3Sqsh_Pma.default" "bussaba_001:Hair3Sqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Hair3Sqsh_Mdv.ox" "bussaba_001:Hair3Sqsh_Pma.i1[1]";
connectAttr "bussaba_001:Hair4Sqsh_Pma.default" "bussaba_001:Hair4Sqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Hair4Sqsh_Mdv.ox" "bussaba_001:Hair4Sqsh_Pma.i1[1]";
connectAttr "bussaba_001:Hair5Sqsh_Pma.default" "bussaba_001:Hair5Sqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Hair5Sqsh_Mdv.ox" "bussaba_001:Hair5Sqsh_Pma.i1[1]";
connectAttr "bussaba_001:Hair6Sqsh_Pma.default" "bussaba_001:Hair6Sqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Hair6Sqsh_Mdv.ox" "bussaba_001:Hair6Sqsh_Pma.i1[1]";
connectAttr "bussaba_001:Hair7Sqsh_Pma.default" "bussaba_001:Hair7Sqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Hair7Sqsh_Mdv.ox" "bussaba_001:Hair7Sqsh_Pma.i1[1]";
connectAttr "bussaba_001:Hair7Strt_Pma.default" "bussaba_001:Hair7Strt_Pma.i1[0]"
		;
connectAttr "bussaba_001:Hair7Strt_Mdv.ox" "bussaba_001:Hair7Strt_Pma.i1[1]";
connectAttr "bussaba_001:HairSide1Sqsh_L_Pma.default" "bussaba_001:HairSide1Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide1Sqsh_L_Mdv.ox" "bussaba_001:HairSide1Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide2Sqsh_L_Pma.default" "bussaba_001:HairSide2Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide2Sqsh_L_Mdv.ox" "bussaba_001:HairSide2Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide3Sqsh_L_Pma.default" "bussaba_001:HairSide3Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide3Sqsh_L_Mdv.ox" "bussaba_001:HairSide3Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide4Sqsh_L_Pma.default" "bussaba_001:HairSide4Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide4Sqsh_L_Mdv.ox" "bussaba_001:HairSide4Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide5Sqsh_L_Pma.default" "bussaba_001:HairSide5Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide5Sqsh_L_Mdv.ox" "bussaba_001:HairSide5Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide6Sqsh_L_Pma.default" "bussaba_001:HairSide6Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide6Sqsh_L_Mdv.ox" "bussaba_001:HairSide6Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide6Strt_L_Pma.default" "bussaba_001:HairSide6Strt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide6Strt_L_Mdv.ox" "bussaba_001:HairSide6Strt_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide1Sqsh_R_Pma.default" "bussaba_001:HairSide1Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide1Sqsh_R_Mdv.ox" "bussaba_001:HairSide1Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide2Sqsh_R_Pma.default" "bussaba_001:HairSide2Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide2Sqsh_R_Mdv.ox" "bussaba_001:HairSide2Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide3Sqsh_R_Pma.default" "bussaba_001:HairSide3Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide3Sqsh_R_Mdv.ox" "bussaba_001:HairSide3Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide4Sqsh_R_Pma.default" "bussaba_001:HairSide4Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide4Sqsh_R_Mdv.ox" "bussaba_001:HairSide4Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide5Sqsh_R_Pma.default" "bussaba_001:HairSide5Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide5Sqsh_R_Mdv.ox" "bussaba_001:HairSide5Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide6Sqsh_R_Pma.default" "bussaba_001:HairSide6Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide6Sqsh_R_Mdv.ox" "bussaba_001:HairSide6Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:HairSide6Strt_R_Pma.default" "bussaba_001:HairSide6Strt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:HairSide6Strt_R_Mdv.ox" "bussaba_001:HairSide6Strt_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:ClavStrt_L_Pma.default" "bussaba_001:ClavStrt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:ClavStrt_L_Mdv.ox" "bussaba_001:ClavStrt_L_Pma.i1[1]";
connectAttr "bussaba_001:Index1Sqsh_L_Pma.default" "bussaba_001:Index1Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Index1Sqsh_L_Mdv.ox" "bussaba_001:Index1Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Index2Sqsh_L_Pma.default" "bussaba_001:Index2Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Index2Sqsh_L_Mdv.ox" "bussaba_001:Index2Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Index3Sqsh_L_Pma.default" "bussaba_001:Index3Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Index3Sqsh_L_Mdv.ox" "bussaba_001:Index3Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Index4Sqsh_L_Pma.default" "bussaba_001:Index4Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Index4Sqsh_L_Mdv.ox" "bussaba_001:Index4Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Middle1Sqsh_L_Pma.default" "bussaba_001:Middle1Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Middle1Sqsh_L_Mdv.ox" "bussaba_001:Middle1Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Middle2Sqsh_L_Pma.default" "bussaba_001:Middle2Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Middle2Sqsh_L_Mdv.ox" "bussaba_001:Middle2Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Middle3Sqsh_L_Pma.default" "bussaba_001:Middle3Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Middle3Sqsh_L_Mdv.ox" "bussaba_001:Middle3Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Middle4Sqsh_L_Pma.default" "bussaba_001:Middle4Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Middle4Sqsh_L_Mdv.ox" "bussaba_001:Middle4Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Ring1Sqsh_L_Pma.default" "bussaba_001:Ring1Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Ring1Sqsh_L_Mdv.ox" "bussaba_001:Ring1Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Ring2Sqsh_L_Pma.default" "bussaba_001:Ring2Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Ring2Sqsh_L_Mdv.ox" "bussaba_001:Ring2Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Ring3Sqsh_L_Pma.default" "bussaba_001:Ring3Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Ring3Sqsh_L_Mdv.ox" "bussaba_001:Ring3Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Ring4Sqsh_L_Pma.default" "bussaba_001:Ring4Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Ring4Sqsh_L_Mdv.ox" "bussaba_001:Ring4Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Pinky1Sqsh_L_Pma.default" "bussaba_001:Pinky1Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Pinky1Sqsh_L_Mdv.ox" "bussaba_001:Pinky1Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Pinky2Sqsh_L_Pma.default" "bussaba_001:Pinky2Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Pinky2Sqsh_L_Mdv.ox" "bussaba_001:Pinky2Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Pinky3Sqsh_L_Pma.default" "bussaba_001:Pinky3Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Pinky3Sqsh_L_Mdv.ox" "bussaba_001:Pinky3Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Pinky4Sqsh_L_Pma.default" "bussaba_001:Pinky4Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Pinky4Sqsh_L_Mdv.ox" "bussaba_001:Pinky4Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Thumb1Sqsh_L_Pma.default" "bussaba_001:Thumb1Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Thumb1Sqsh_L_Mdv.ox" "bussaba_001:Thumb1Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Thumb2Sqsh_L_Pma.default" "bussaba_001:Thumb2Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Thumb2Sqsh_L_Mdv.ox" "bussaba_001:Thumb2Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Thumb3Sqsh_L_Pma.default" "bussaba_001:Thumb3Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Thumb3Sqsh_L_Mdv.ox" "bussaba_001:Thumb3Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:ClavStrt_R_Pma.default" "bussaba_001:ClavStrt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:ClavStrt_R_Mdv.ox" "bussaba_001:ClavStrt_R_Pma.i1[1]";
connectAttr "bussaba_001:Index1Sqsh_R_Pma.default" "bussaba_001:Index1Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Index1Sqsh_R_Mdv.ox" "bussaba_001:Index1Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Index2Sqsh_R_Pma.default" "bussaba_001:Index2Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Index2Sqsh_R_Mdv.ox" "bussaba_001:Index2Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Index3Sqsh_R_Pma.default" "bussaba_001:Index3Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Index3Sqsh_R_Mdv.ox" "bussaba_001:Index3Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Index4Sqsh_R_Pma.default" "bussaba_001:Index4Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Index4Sqsh_R_Mdv.ox" "bussaba_001:Index4Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Middle1Sqsh_R_Pma.default" "bussaba_001:Middle1Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Middle1Sqsh_R_Mdv.ox" "bussaba_001:Middle1Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Middle2Sqsh_R_Pma.default" "bussaba_001:Middle2Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Middle2Sqsh_R_Mdv.ox" "bussaba_001:Middle2Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Middle3Sqsh_R_Pma.default" "bussaba_001:Middle3Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Middle3Sqsh_R_Mdv.ox" "bussaba_001:Middle3Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Middle4Sqsh_R_Pma.default" "bussaba_001:Middle4Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Middle4Sqsh_R_Mdv.ox" "bussaba_001:Middle4Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Ring1Sqsh_R_Pma.default" "bussaba_001:Ring1Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Ring1Sqsh_R_Mdv.ox" "bussaba_001:Ring1Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Ring2Sqsh_R_Pma.default" "bussaba_001:Ring2Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Ring2Sqsh_R_Mdv.ox" "bussaba_001:Ring2Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Ring3Sqsh_R_Pma.default" "bussaba_001:Ring3Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Ring3Sqsh_R_Mdv.ox" "bussaba_001:Ring3Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Ring4Sqsh_R_Pma.default" "bussaba_001:Ring4Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Ring4Sqsh_R_Mdv.ox" "bussaba_001:Ring4Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Pinky1Sqsh_R_Pma.default" "bussaba_001:Pinky1Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Pinky1Sqsh_R_Mdv.ox" "bussaba_001:Pinky1Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Pinky2Sqsh_R_Pma.default" "bussaba_001:Pinky2Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Pinky2Sqsh_R_Mdv.ox" "bussaba_001:Pinky2Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Pinky3Sqsh_R_Pma.default" "bussaba_001:Pinky3Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Pinky3Sqsh_R_Mdv.ox" "bussaba_001:Pinky3Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Pinky4Sqsh_R_Pma.default" "bussaba_001:Pinky4Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Pinky4Sqsh_R_Mdv.ox" "bussaba_001:Pinky4Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Thumb1Sqsh_R_Pma.default" "bussaba_001:Thumb1Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Thumb1Sqsh_R_Mdv.ox" "bussaba_001:Thumb1Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Thumb2Sqsh_R_Pma.default" "bussaba_001:Thumb2Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Thumb2Sqsh_R_Mdv.ox" "bussaba_001:Thumb2Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Thumb3Sqsh_R_Pma.default" "bussaba_001:Thumb3Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Thumb3Sqsh_R_Mdv.ox" "bussaba_001:Thumb3Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Neck1RbnDtlSqsh_Pma.default" "bussaba_001:Neck1RbnDtlSqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Neck1RbnDtlSqsh_Mdv.ox" "bussaba_001:Neck1RbnDtlSqsh_Pma.i1[1]"
		;
connectAttr "bussaba_001:Neck1RbnDtlSqsh_Mdv.oy" "bussaba_001:Neck1RbnDtlSqsh_Pma.i1[2]"
		;
connectAttr "bussaba_001:Neck1RbnDtlSqsh_Mdv.oz" "bussaba_001:Neck1RbnDtlSqsh_Pma.i1[3]"
		;
connectAttr "bussaba_001:NeckRbnAutoSqsh_Bcl.opr" "bussaba_001:Neck1RbnDtlSqsh_Mdv.i1z"
		;
connectAttr "bussaba_001:NeckRbnSqshDiv_Mdv.ox" "bussaba_001:NeckRbnAutoSqsh_Bcl.c1r"
		;
connectAttr "bussaba_001:NeckRbnSqshPow_Mdv.ox" "bussaba_001:NeckRbnSqshDiv_Mdv.i2x"
		;
connectAttr "bussaba_001:NeckRbnSqshNorm_Mdv.ox" "bussaba_001:NeckRbnSqshPow_Mdv.i1x"
		;
connectAttr "bussaba_001:Neck2RbnDtlSqsh_Pma.default" "bussaba_001:Neck2RbnDtlSqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Neck2RbnDtlSqsh_Mdv.ox" "bussaba_001:Neck2RbnDtlSqsh_Pma.i1[1]"
		;
connectAttr "bussaba_001:Neck2RbnDtlSqsh_Mdv.oy" "bussaba_001:Neck2RbnDtlSqsh_Pma.i1[2]"
		;
connectAttr "bussaba_001:Neck2RbnDtlSqsh_Mdv.oz" "bussaba_001:Neck2RbnDtlSqsh_Pma.i1[3]"
		;
connectAttr "bussaba_001:NeckRbnAutoSqsh_Bcl.opr" "bussaba_001:Neck2RbnDtlSqsh_Mdv.i1z"
		;
connectAttr "bussaba_001:Neck3RbnDtlSqsh_Pma.default" "bussaba_001:Neck3RbnDtlSqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Neck3RbnDtlSqsh_Mdv.ox" "bussaba_001:Neck3RbnDtlSqsh_Pma.i1[1]"
		;
connectAttr "bussaba_001:Neck3RbnDtlSqsh_Mdv.oy" "bussaba_001:Neck3RbnDtlSqsh_Pma.i1[2]"
		;
connectAttr "bussaba_001:Neck3RbnDtlSqsh_Mdv.oz" "bussaba_001:Neck3RbnDtlSqsh_Pma.i1[3]"
		;
connectAttr "bussaba_001:NeckRbnAutoSqsh_Bcl.opr" "bussaba_001:Neck3RbnDtlSqsh_Mdv.i1z"
		;
connectAttr "bussaba_001:Neck4RbnDtlSqsh_Pma.default" "bussaba_001:Neck4RbnDtlSqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Neck4RbnDtlSqsh_Mdv.ox" "bussaba_001:Neck4RbnDtlSqsh_Pma.i1[1]"
		;
connectAttr "bussaba_001:Neck4RbnDtlSqsh_Mdv.oy" "bussaba_001:Neck4RbnDtlSqsh_Pma.i1[2]"
		;
connectAttr "bussaba_001:Neck4RbnDtlSqsh_Mdv.oz" "bussaba_001:Neck4RbnDtlSqsh_Pma.i1[3]"
		;
connectAttr "bussaba_001:NeckRbnAutoSqsh_Bcl.opr" "bussaba_001:Neck4RbnDtlSqsh_Mdv.i1z"
		;
connectAttr "bussaba_001:Neck5RbnDtlSqsh_Pma.default" "bussaba_001:Neck5RbnDtlSqsh_Pma.i1[0]"
		;
connectAttr "bussaba_001:Neck5RbnDtlSqsh_Mdv.ox" "bussaba_001:Neck5RbnDtlSqsh_Pma.i1[1]"
		;
connectAttr "bussaba_001:Neck5RbnDtlSqsh_Mdv.oy" "bussaba_001:Neck5RbnDtlSqsh_Pma.i1[2]"
		;
connectAttr "bussaba_001:Neck5RbnDtlSqsh_Mdv.oz" "bussaba_001:Neck5RbnDtlSqsh_Pma.i1[3]"
		;
connectAttr "bussaba_001:NeckRbnAutoSqsh_Bcl.opr" "bussaba_001:Neck5RbnDtlSqsh_Mdv.i1z"
		;
connectAttr "bussaba_001:UpArm1RbnDtlSqsh_L_Pma.default" "bussaba_001:UpArm1RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpArm1RbnDtlSqsh_L_Mdv.ox" "bussaba_001:UpArm1RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpArm1RbnDtlSqsh_L_Mdv.oy" "bussaba_001:UpArm1RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpArm1RbnDtlSqsh_L_Mdv.oz" "bussaba_001:UpArm1RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpArmRbnAutoSqsh_L_Bcl.opr" "bussaba_001:UpArm1RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:UpArmRbnSqshDiv_L_Mdv.ox" "bussaba_001:UpArmRbnAutoSqsh_L_Bcl.c1r"
		;
connectAttr "bussaba_001:UpArmRbnSqshPow_L_Mdv.ox" "bussaba_001:UpArmRbnSqshDiv_L_Mdv.i2x"
		;
connectAttr "bussaba_001:UpArmRbnSqshNorm_L_Mdv.ox" "bussaba_001:UpArmRbnSqshPow_L_Mdv.i1x"
		;
connectAttr "bussaba_001:UpArm2RbnDtlSqsh_L_Pma.default" "bussaba_001:UpArm2RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpArm2RbnDtlSqsh_L_Mdv.ox" "bussaba_001:UpArm2RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpArm2RbnDtlSqsh_L_Mdv.oy" "bussaba_001:UpArm2RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpArm2RbnDtlSqsh_L_Mdv.oz" "bussaba_001:UpArm2RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpArmRbnAutoSqsh_L_Bcl.opr" "bussaba_001:UpArm2RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:UpArm3RbnDtlSqsh_L_Pma.default" "bussaba_001:UpArm3RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpArm3RbnDtlSqsh_L_Mdv.ox" "bussaba_001:UpArm3RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpArm3RbnDtlSqsh_L_Mdv.oy" "bussaba_001:UpArm3RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpArm3RbnDtlSqsh_L_Mdv.oz" "bussaba_001:UpArm3RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpArmRbnAutoSqsh_L_Bcl.opr" "bussaba_001:UpArm3RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:UpArm4RbnDtlSqsh_L_Pma.default" "bussaba_001:UpArm4RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpArm4RbnDtlSqsh_L_Mdv.ox" "bussaba_001:UpArm4RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpArm4RbnDtlSqsh_L_Mdv.oy" "bussaba_001:UpArm4RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpArm4RbnDtlSqsh_L_Mdv.oz" "bussaba_001:UpArm4RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpArmRbnAutoSqsh_L_Bcl.opr" "bussaba_001:UpArm4RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:UpArm5RbnDtlSqsh_L_Pma.default" "bussaba_001:UpArm5RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpArm5RbnDtlSqsh_L_Mdv.ox" "bussaba_001:UpArm5RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpArm5RbnDtlSqsh_L_Mdv.oy" "bussaba_001:UpArm5RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpArm5RbnDtlSqsh_L_Mdv.oz" "bussaba_001:UpArm5RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpArmRbnAutoSqsh_L_Bcl.opr" "bussaba_001:UpArm5RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:Forearm1RbnDtlSqsh_L_Pma.default" "bussaba_001:Forearm1RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Forearm1RbnDtlSqsh_L_Mdv.ox" "bussaba_001:Forearm1RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Forearm1RbnDtlSqsh_L_Mdv.oy" "bussaba_001:Forearm1RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:Forearm1RbnDtlSqsh_L_Mdv.oz" "bussaba_001:Forearm1RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:ForearmRbnAutoSqsh_L_Bcl.opr" "bussaba_001:Forearm1RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:ForearmRbnSqshDiv_L_Mdv.ox" "bussaba_001:ForearmRbnAutoSqsh_L_Bcl.c1r"
		;
connectAttr "bussaba_001:ForearmRbnSqshPow_L_Mdv.ox" "bussaba_001:ForearmRbnSqshDiv_L_Mdv.i2x"
		;
connectAttr "bussaba_001:ForearmRbnSqshNorm_L_Mdv.ox" "bussaba_001:ForearmRbnSqshPow_L_Mdv.i1x"
		;
connectAttr "bussaba_001:Forearm2RbnDtlSqsh_L_Pma.default" "bussaba_001:Forearm2RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Forearm2RbnDtlSqsh_L_Mdv.ox" "bussaba_001:Forearm2RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Forearm2RbnDtlSqsh_L_Mdv.oy" "bussaba_001:Forearm2RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:Forearm2RbnDtlSqsh_L_Mdv.oz" "bussaba_001:Forearm2RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:ForearmRbnAutoSqsh_L_Bcl.opr" "bussaba_001:Forearm2RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:Forearm3RbnDtlSqsh_L_Pma.default" "bussaba_001:Forearm3RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Forearm3RbnDtlSqsh_L_Mdv.ox" "bussaba_001:Forearm3RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Forearm3RbnDtlSqsh_L_Mdv.oy" "bussaba_001:Forearm3RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:Forearm3RbnDtlSqsh_L_Mdv.oz" "bussaba_001:Forearm3RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:ForearmRbnAutoSqsh_L_Bcl.opr" "bussaba_001:Forearm3RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:Forearm4RbnDtlSqsh_L_Pma.default" "bussaba_001:Forearm4RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Forearm4RbnDtlSqsh_L_Mdv.ox" "bussaba_001:Forearm4RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Forearm4RbnDtlSqsh_L_Mdv.oy" "bussaba_001:Forearm4RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:Forearm4RbnDtlSqsh_L_Mdv.oz" "bussaba_001:Forearm4RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:ForearmRbnAutoSqsh_L_Bcl.opr" "bussaba_001:Forearm4RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:Forearm5RbnDtlSqsh_L_Pma.default" "bussaba_001:Forearm5RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Forearm5RbnDtlSqsh_L_Mdv.ox" "bussaba_001:Forearm5RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Forearm5RbnDtlSqsh_L_Mdv.oy" "bussaba_001:Forearm5RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:Forearm5RbnDtlSqsh_L_Mdv.oz" "bussaba_001:Forearm5RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:ForearmRbnAutoSqsh_L_Bcl.opr" "bussaba_001:Forearm5RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:UpArm1RbnDtlSqsh_R_Pma.default" "bussaba_001:UpArm1RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpArm1RbnDtlSqsh_R_Mdv.ox" "bussaba_001:UpArm1RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpArm1RbnDtlSqsh_R_Mdv.oy" "bussaba_001:UpArm1RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpArm1RbnDtlSqsh_R_Mdv.oz" "bussaba_001:UpArm1RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpArmRbnAutoSqsh_R_Bcl.opr" "bussaba_001:UpArm1RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:UpArmRbnSqshDiv_R_Mdv.ox" "bussaba_001:UpArmRbnAutoSqsh_R_Bcl.c1r"
		;
connectAttr "bussaba_001:UpArmRbnSqshPow_R_Mdv.ox" "bussaba_001:UpArmRbnSqshDiv_R_Mdv.i2x"
		;
connectAttr "bussaba_001:UpArmRbnSqshNorm_R_Mdv.ox" "bussaba_001:UpArmRbnSqshPow_R_Mdv.i1x"
		;
connectAttr "bussaba_001:UpArm2RbnDtlSqsh_R_Pma.default" "bussaba_001:UpArm2RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpArm2RbnDtlSqsh_R_Mdv.ox" "bussaba_001:UpArm2RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpArm2RbnDtlSqsh_R_Mdv.oy" "bussaba_001:UpArm2RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpArm2RbnDtlSqsh_R_Mdv.oz" "bussaba_001:UpArm2RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpArmRbnAutoSqsh_R_Bcl.opr" "bussaba_001:UpArm2RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:UpArm3RbnDtlSqsh_R_Pma.default" "bussaba_001:UpArm3RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpArm3RbnDtlSqsh_R_Mdv.ox" "bussaba_001:UpArm3RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpArm3RbnDtlSqsh_R_Mdv.oy" "bussaba_001:UpArm3RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpArm3RbnDtlSqsh_R_Mdv.oz" "bussaba_001:UpArm3RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpArmRbnAutoSqsh_R_Bcl.opr" "bussaba_001:UpArm3RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:UpArm4RbnDtlSqsh_R_Pma.default" "bussaba_001:UpArm4RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpArm4RbnDtlSqsh_R_Mdv.ox" "bussaba_001:UpArm4RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpArm4RbnDtlSqsh_R_Mdv.oy" "bussaba_001:UpArm4RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpArm4RbnDtlSqsh_R_Mdv.oz" "bussaba_001:UpArm4RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpArmRbnAutoSqsh_R_Bcl.opr" "bussaba_001:UpArm4RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:UpArm5RbnDtlSqsh_R_Pma.default" "bussaba_001:UpArm5RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpArm5RbnDtlSqsh_R_Mdv.ox" "bussaba_001:UpArm5RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpArm5RbnDtlSqsh_R_Mdv.oy" "bussaba_001:UpArm5RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpArm5RbnDtlSqsh_R_Mdv.oz" "bussaba_001:UpArm5RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpArmRbnAutoSqsh_R_Bcl.opr" "bussaba_001:UpArm5RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:Forearm1RbnDtlSqsh_R_Pma.default" "bussaba_001:Forearm1RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Forearm1RbnDtlSqsh_R_Mdv.ox" "bussaba_001:Forearm1RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Forearm1RbnDtlSqsh_R_Mdv.oy" "bussaba_001:Forearm1RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:Forearm1RbnDtlSqsh_R_Mdv.oz" "bussaba_001:Forearm1RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:ForearmRbnAutoSqsh_R_Bcl.opr" "bussaba_001:Forearm1RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:ForearmRbnSqshDiv_R_Mdv.ox" "bussaba_001:ForearmRbnAutoSqsh_R_Bcl.c1r"
		;
connectAttr "bussaba_001:ForearmRbnSqshPow_R_Mdv.ox" "bussaba_001:ForearmRbnSqshDiv_R_Mdv.i2x"
		;
connectAttr "bussaba_001:ForearmRbnSqshNorm_R_Mdv.ox" "bussaba_001:ForearmRbnSqshPow_R_Mdv.i1x"
		;
connectAttr "bussaba_001:Forearm2RbnDtlSqsh_R_Pma.default" "bussaba_001:Forearm2RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Forearm2RbnDtlSqsh_R_Mdv.ox" "bussaba_001:Forearm2RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Forearm2RbnDtlSqsh_R_Mdv.oy" "bussaba_001:Forearm2RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:Forearm2RbnDtlSqsh_R_Mdv.oz" "bussaba_001:Forearm2RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:ForearmRbnAutoSqsh_R_Bcl.opr" "bussaba_001:Forearm2RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:Forearm3RbnDtlSqsh_R_Pma.default" "bussaba_001:Forearm3RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Forearm3RbnDtlSqsh_R_Mdv.ox" "bussaba_001:Forearm3RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Forearm3RbnDtlSqsh_R_Mdv.oy" "bussaba_001:Forearm3RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:Forearm3RbnDtlSqsh_R_Mdv.oz" "bussaba_001:Forearm3RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:ForearmRbnAutoSqsh_R_Bcl.opr" "bussaba_001:Forearm3RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:Forearm4RbnDtlSqsh_R_Pma.default" "bussaba_001:Forearm4RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Forearm4RbnDtlSqsh_R_Mdv.ox" "bussaba_001:Forearm4RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Forearm4RbnDtlSqsh_R_Mdv.oy" "bussaba_001:Forearm4RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:Forearm4RbnDtlSqsh_R_Mdv.oz" "bussaba_001:Forearm4RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:ForearmRbnAutoSqsh_R_Bcl.opr" "bussaba_001:Forearm4RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:Forearm5RbnDtlSqsh_R_Pma.default" "bussaba_001:Forearm5RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Forearm5RbnDtlSqsh_R_Mdv.ox" "bussaba_001:Forearm5RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Forearm5RbnDtlSqsh_R_Mdv.oy" "bussaba_001:Forearm5RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:Forearm5RbnDtlSqsh_R_Mdv.oz" "bussaba_001:Forearm5RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:ForearmRbnAutoSqsh_R_Bcl.opr" "bussaba_001:Forearm5RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:UpLeg1RbnDtlSqsh_L_Pma.default" "bussaba_001:UpLeg1RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpLeg1RbnDtlSqsh_L_Mdv.ox" "bussaba_001:UpLeg1RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpLeg1RbnDtlSqsh_L_Mdv.oy" "bussaba_001:UpLeg1RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpLeg1RbnDtlSqsh_L_Mdv.oz" "bussaba_001:UpLeg1RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpLegRbnAutoSqsh_L_Bcl.opr" "bussaba_001:UpLeg1RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:UpLegRbnSqshDiv_L_Mdv.ox" "bussaba_001:UpLegRbnAutoSqsh_L_Bcl.c1r"
		;
connectAttr "bussaba_001:UpLegRbnSqshPow_L_Mdv.ox" "bussaba_001:UpLegRbnSqshDiv_L_Mdv.i2x"
		;
connectAttr "bussaba_001:UpLegRbnSqshNorm_L_Mdv.ox" "bussaba_001:UpLegRbnSqshPow_L_Mdv.i1x"
		;
connectAttr "bussaba_001:UpLeg2RbnDtlSqsh_L_Pma.default" "bussaba_001:UpLeg2RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpLeg2RbnDtlSqsh_L_Mdv.ox" "bussaba_001:UpLeg2RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpLeg2RbnDtlSqsh_L_Mdv.oy" "bussaba_001:UpLeg2RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpLeg2RbnDtlSqsh_L_Mdv.oz" "bussaba_001:UpLeg2RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpLegRbnAutoSqsh_L_Bcl.opr" "bussaba_001:UpLeg2RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:UpLeg3RbnDtlSqsh_L_Pma.default" "bussaba_001:UpLeg3RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpLeg3RbnDtlSqsh_L_Mdv.ox" "bussaba_001:UpLeg3RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpLeg3RbnDtlSqsh_L_Mdv.oy" "bussaba_001:UpLeg3RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpLeg3RbnDtlSqsh_L_Mdv.oz" "bussaba_001:UpLeg3RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpLegRbnAutoSqsh_L_Bcl.opr" "bussaba_001:UpLeg3RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:UpLeg4RbnDtlSqsh_L_Pma.default" "bussaba_001:UpLeg4RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpLeg4RbnDtlSqsh_L_Mdv.ox" "bussaba_001:UpLeg4RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpLeg4RbnDtlSqsh_L_Mdv.oy" "bussaba_001:UpLeg4RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpLeg4RbnDtlSqsh_L_Mdv.oz" "bussaba_001:UpLeg4RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpLegRbnAutoSqsh_L_Bcl.opr" "bussaba_001:UpLeg4RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:UpLeg5RbnDtlSqsh_L_Pma.default" "bussaba_001:UpLeg5RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpLeg5RbnDtlSqsh_L_Mdv.ox" "bussaba_001:UpLeg5RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpLeg5RbnDtlSqsh_L_Mdv.oy" "bussaba_001:UpLeg5RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpLeg5RbnDtlSqsh_L_Mdv.oz" "bussaba_001:UpLeg5RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpLegRbnAutoSqsh_L_Bcl.opr" "bussaba_001:UpLeg5RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:LowLeg1RbnDtlSqsh_L_Pma.default" "bussaba_001:LowLeg1RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:LowLeg1RbnDtlSqsh_L_Mdv.ox" "bussaba_001:LowLeg1RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:LowLeg1RbnDtlSqsh_L_Mdv.oy" "bussaba_001:LowLeg1RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:LowLeg1RbnDtlSqsh_L_Mdv.oz" "bussaba_001:LowLeg1RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:LowLegRbnAutoSqsh_L_Bcl.opr" "bussaba_001:LowLeg1RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:LowLegRbnSqshDiv_L_Mdv.ox" "bussaba_001:LowLegRbnAutoSqsh_L_Bcl.c1r"
		;
connectAttr "bussaba_001:LowLegRbnSqshPow_L_Mdv.ox" "bussaba_001:LowLegRbnSqshDiv_L_Mdv.i2x"
		;
connectAttr "bussaba_001:LowLegRbnSqshNorm_L_Mdv.ox" "bussaba_001:LowLegRbnSqshPow_L_Mdv.i1x"
		;
connectAttr "bussaba_001:LowLeg2RbnDtlSqsh_L_Pma.default" "bussaba_001:LowLeg2RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:LowLeg2RbnDtlSqsh_L_Mdv.ox" "bussaba_001:LowLeg2RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:LowLeg2RbnDtlSqsh_L_Mdv.oy" "bussaba_001:LowLeg2RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:LowLeg2RbnDtlSqsh_L_Mdv.oz" "bussaba_001:LowLeg2RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:LowLegRbnAutoSqsh_L_Bcl.opr" "bussaba_001:LowLeg2RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:LowLeg3RbnDtlSqsh_L_Pma.default" "bussaba_001:LowLeg3RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:LowLeg3RbnDtlSqsh_L_Mdv.ox" "bussaba_001:LowLeg3RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:LowLeg3RbnDtlSqsh_L_Mdv.oy" "bussaba_001:LowLeg3RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:LowLeg3RbnDtlSqsh_L_Mdv.oz" "bussaba_001:LowLeg3RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:LowLegRbnAutoSqsh_L_Bcl.opr" "bussaba_001:LowLeg3RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:LowLeg4RbnDtlSqsh_L_Pma.default" "bussaba_001:LowLeg4RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:LowLeg4RbnDtlSqsh_L_Mdv.ox" "bussaba_001:LowLeg4RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:LowLeg4RbnDtlSqsh_L_Mdv.oy" "bussaba_001:LowLeg4RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:LowLeg4RbnDtlSqsh_L_Mdv.oz" "bussaba_001:LowLeg4RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:LowLegRbnAutoSqsh_L_Bcl.opr" "bussaba_001:LowLeg4RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:LowLeg5RbnDtlSqsh_L_Pma.default" "bussaba_001:LowLeg5RbnDtlSqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:LowLeg5RbnDtlSqsh_L_Mdv.ox" "bussaba_001:LowLeg5RbnDtlSqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:LowLeg5RbnDtlSqsh_L_Mdv.oy" "bussaba_001:LowLeg5RbnDtlSqsh_L_Pma.i1[2]"
		;
connectAttr "bussaba_001:LowLeg5RbnDtlSqsh_L_Mdv.oz" "bussaba_001:LowLeg5RbnDtlSqsh_L_Pma.i1[3]"
		;
connectAttr "bussaba_001:LowLegRbnAutoSqsh_L_Bcl.opr" "bussaba_001:LowLeg5RbnDtlSqsh_L_Mdv.i1z"
		;
connectAttr "bussaba_001:UpLeg1RbnDtlSqsh_R_Pma.default" "bussaba_001:UpLeg1RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpLeg1RbnDtlSqsh_R_Mdv.ox" "bussaba_001:UpLeg1RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpLeg1RbnDtlSqsh_R_Mdv.oy" "bussaba_001:UpLeg1RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpLeg1RbnDtlSqsh_R_Mdv.oz" "bussaba_001:UpLeg1RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpLegRbnAutoSqsh_R_Bcl.opr" "bussaba_001:UpLeg1RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:UpLegRbnSqshDiv_R_Mdv.ox" "bussaba_001:UpLegRbnAutoSqsh_R_Bcl.c1r"
		;
connectAttr "bussaba_001:UpLegRbnSqshPow_R_Mdv.ox" "bussaba_001:UpLegRbnSqshDiv_R_Mdv.i2x"
		;
connectAttr "bussaba_001:UpLegRbnSqshNorm_R_Mdv.ox" "bussaba_001:UpLegRbnSqshPow_R_Mdv.i1x"
		;
connectAttr "bussaba_001:UpLeg2RbnDtlSqsh_R_Pma.default" "bussaba_001:UpLeg2RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpLeg2RbnDtlSqsh_R_Mdv.ox" "bussaba_001:UpLeg2RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpLeg2RbnDtlSqsh_R_Mdv.oy" "bussaba_001:UpLeg2RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpLeg2RbnDtlSqsh_R_Mdv.oz" "bussaba_001:UpLeg2RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpLegRbnAutoSqsh_R_Bcl.opr" "bussaba_001:UpLeg2RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:UpLeg3RbnDtlSqsh_R_Pma.default" "bussaba_001:UpLeg3RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpLeg3RbnDtlSqsh_R_Mdv.ox" "bussaba_001:UpLeg3RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpLeg3RbnDtlSqsh_R_Mdv.oy" "bussaba_001:UpLeg3RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpLeg3RbnDtlSqsh_R_Mdv.oz" "bussaba_001:UpLeg3RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpLegRbnAutoSqsh_R_Bcl.opr" "bussaba_001:UpLeg3RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:UpLeg4RbnDtlSqsh_R_Pma.default" "bussaba_001:UpLeg4RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpLeg4RbnDtlSqsh_R_Mdv.ox" "bussaba_001:UpLeg4RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpLeg4RbnDtlSqsh_R_Mdv.oy" "bussaba_001:UpLeg4RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpLeg4RbnDtlSqsh_R_Mdv.oz" "bussaba_001:UpLeg4RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpLegRbnAutoSqsh_R_Bcl.opr" "bussaba_001:UpLeg4RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:UpLeg5RbnDtlSqsh_R_Pma.default" "bussaba_001:UpLeg5RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:UpLeg5RbnDtlSqsh_R_Mdv.ox" "bussaba_001:UpLeg5RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:UpLeg5RbnDtlSqsh_R_Mdv.oy" "bussaba_001:UpLeg5RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:UpLeg5RbnDtlSqsh_R_Mdv.oz" "bussaba_001:UpLeg5RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:UpLegRbnAutoSqsh_R_Bcl.opr" "bussaba_001:UpLeg5RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:LowLeg1RbnDtlSqsh_R_Pma.default" "bussaba_001:LowLeg1RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:LowLeg1RbnDtlSqsh_R_Mdv.ox" "bussaba_001:LowLeg1RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:LowLeg1RbnDtlSqsh_R_Mdv.oy" "bussaba_001:LowLeg1RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:LowLeg1RbnDtlSqsh_R_Mdv.oz" "bussaba_001:LowLeg1RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:LowLegRbnAutoSqsh_R_Bcl.opr" "bussaba_001:LowLeg1RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:LowLegRbnSqshDiv_R_Mdv.ox" "bussaba_001:LowLegRbnAutoSqsh_R_Bcl.c1r"
		;
connectAttr "bussaba_001:LowLegRbnSqshPow_R_Mdv.ox" "bussaba_001:LowLegRbnSqshDiv_R_Mdv.i2x"
		;
connectAttr "bussaba_001:LowLegRbnSqshNorm_R_Mdv.ox" "bussaba_001:LowLegRbnSqshPow_R_Mdv.i1x"
		;
connectAttr "bussaba_001:LowLeg2RbnDtlSqsh_R_Pma.default" "bussaba_001:LowLeg2RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:LowLeg2RbnDtlSqsh_R_Mdv.ox" "bussaba_001:LowLeg2RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:LowLeg2RbnDtlSqsh_R_Mdv.oy" "bussaba_001:LowLeg2RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:LowLeg2RbnDtlSqsh_R_Mdv.oz" "bussaba_001:LowLeg2RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:LowLegRbnAutoSqsh_R_Bcl.opr" "bussaba_001:LowLeg2RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:LowLeg3RbnDtlSqsh_R_Pma.default" "bussaba_001:LowLeg3RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:LowLeg3RbnDtlSqsh_R_Mdv.ox" "bussaba_001:LowLeg3RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:LowLeg3RbnDtlSqsh_R_Mdv.oy" "bussaba_001:LowLeg3RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:LowLeg3RbnDtlSqsh_R_Mdv.oz" "bussaba_001:LowLeg3RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:LowLegRbnAutoSqsh_R_Bcl.opr" "bussaba_001:LowLeg3RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:LowLeg4RbnDtlSqsh_R_Pma.default" "bussaba_001:LowLeg4RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:LowLeg4RbnDtlSqsh_R_Mdv.ox" "bussaba_001:LowLeg4RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:LowLeg4RbnDtlSqsh_R_Mdv.oy" "bussaba_001:LowLeg4RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:LowLeg4RbnDtlSqsh_R_Mdv.oz" "bussaba_001:LowLeg4RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:LowLegRbnAutoSqsh_R_Bcl.opr" "bussaba_001:LowLeg4RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:LowLeg5RbnDtlSqsh_R_Pma.default" "bussaba_001:LowLeg5RbnDtlSqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:LowLeg5RbnDtlSqsh_R_Mdv.ox" "bussaba_001:LowLeg5RbnDtlSqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:LowLeg5RbnDtlSqsh_R_Mdv.oy" "bussaba_001:LowLeg5RbnDtlSqsh_R_Pma.i1[2]"
		;
connectAttr "bussaba_001:LowLeg5RbnDtlSqsh_R_Mdv.oz" "bussaba_001:LowLeg5RbnDtlSqsh_R_Pma.i1[3]"
		;
connectAttr "bussaba_001:LowLegRbnAutoSqsh_R_Bcl.opr" "bussaba_001:LowLeg5RbnDtlSqsh_R_Mdv.i1z"
		;
connectAttr "bussaba_001:Breast1Sqsh_L_Pma.default" "bussaba_001:Breast1Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Breast1Sqsh_L_Mdv.ox" "bussaba_001:Breast1Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Breast2Sqsh_L_Pma.default" "bussaba_001:Breast2Sqsh_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Breast2Sqsh_L_Mdv.ox" "bussaba_001:Breast2Sqsh_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Breast2Strt_L_Pma.default" "bussaba_001:Breast2Strt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:Breast2Strt_L_Mdv.ox" "bussaba_001:Breast2Strt_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:Breast1Sqsh_R_Pma.default" "bussaba_001:Breast1Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Breast1Sqsh_R_Mdv.ox" "bussaba_001:Breast1Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Breast2Sqsh_R_Pma.default" "bussaba_001:Breast2Sqsh_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Breast2Sqsh_R_Mdv.ox" "bussaba_001:Breast2Sqsh_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:Breast2Strt_R_Pma.default" "bussaba_001:Breast2Strt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:Breast2Strt_R_Mdv.ox" "bussaba_001:Breast2Strt_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:BallFkStrt_L_Pma.default" "bussaba_001:BallFkStrt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:BallFkStrt_L_Mdv.ox" "bussaba_001:BallFkStrt_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:ToeIkStrt_L_Pma.default" "bussaba_001:ToeIkStrt_L_Pma.i1[0]"
		;
connectAttr "bussaba_001:ToeIkStrt_L_Mdv.ox" "bussaba_001:ToeIkStrt_L_Pma.i1[1]"
		;
connectAttr "bussaba_001:BallFkStrt_R_Pma.default" "bussaba_001:BallFkStrt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:BallFkStrt_R_Mdv.ox" "bussaba_001:BallFkStrt_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:ToeIkStrt_R_Pma.default" "bussaba_001:ToeIkStrt_R_Pma.i1[0]"
		;
connectAttr "bussaba_001:ToeIkStrt_R_Mdv.ox" "bussaba_001:ToeIkStrt_R_Pma.i1[1]"
		;
connectAttr "bussaba_001:RbnHdUpHdDfmRigAtSqs_Blnd.default" "bussaba_001:RbnHdUpHdDfmRigAtSqs_Blnd.i[0]"
		;
connectAttr "bussaba_001:RbnHdUpHdDfmRigSqsDiv_Mdv.ox" "bussaba_001:RbnHdUpHdDfmRigAtSqs_Blnd.i[1]"
		;
connectAttr "bussaba_001:RbnHdUpHdDfmRigSqsPow_Mdv.ox" "bussaba_001:RbnHdUpHdDfmRigSqsDiv_Mdv.i2x"
		;
connectAttr "bussaba_001:RbnHdUpHdDfmRigSqsNorm_Mdv.ox" "bussaba_001:RbnHdUpHdDfmRigSqsPow_Mdv.i1x"
		;
connectAttr "bussaba_001:RbnHdLowHdDfmRigAtSqs_Blnd.default" "bussaba_001:RbnHdLowHdDfmRigAtSqs_Blnd.i[0]"
		;
connectAttr "bussaba_001:RbnHdLowHdDfmRigSqsDiv_Mdv.ox" "bussaba_001:RbnHdLowHdDfmRigAtSqs_Blnd.i[1]"
		;
connectAttr "bussaba_001:RbnHdLowHdDfmRigSqsPow_Mdv.ox" "bussaba_001:RbnHdLowHdDfmRigSqsDiv_Mdv.i2x"
		;
connectAttr "bussaba_001:RbnHdLowHdDfmRigSqsNorm_Mdv.ox" "bussaba_001:RbnHdLowHdDfmRigSqsPow_Mdv.i1x"
		;
connectAttr "bussaba_001:renderLayerManager.rlmi[0]" "bussaba_001:defaultRenderLayer.rlid"
		;
connectAttr "lambert61.oc" "lambert61SG.ss";
connectAttr "lambert61SG.msg" "materialInfo20.sg";
connectAttr "lambert61.msg" "materialInfo20.m";
connectAttr "lambert61SG.pa" ":renderPartition.st" -na;
connectAttr "lambert61.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "bussaba_001:defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "Mouth_NrbShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "EyeLidSurface_R_NrbShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "EyeLidSurface_L_NrbShape.iog" ":initialShadingGroup.dsm" -na;
dataStructure -fmt "raw" -as "name=faceConnectMarkerStructure:bool=faceConnectMarker:string[200]=faceConnectOutputGroups";
dataStructure -fmt "raw" -as "name=faceConnectOutputStructure:bool=faceConnectOutput:string[200]=faceConnectOutputAttributes:string[200]=faceConnectOutputGroups";
dataStructure -fmt "raw" -as "name=FBXFastExportSetting_FBX:string=54";
dataStructure -fmt "raw" -as "name=externalContentTablZ:string=nodZ:string=key:string=upath:uint32=upathcrc:string=rpath:string=roles";
dataStructure -fmt "raw" -as "name=idStructure:int32=ID";
dataStructure -fmt "raw" -as "name=FBXFastExportSetting_MB:string=19424";
// End of facialDir_skel_tmp.ma
