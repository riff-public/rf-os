from rigScript import characterRig
reload(characterRig)

def Run():
     charRig = characterRig.HumanRig(   head        = True ,
                                        hairRoot    = True ,
                                        hair        = [] ,
                                        neck        = True ,
                                        eyeL        = True ,
                                        eyeR        = True ,
                                        addEye      = [] ,
                                        iris        = True ,
                                        pupil       = True ,
                                        eyeDot      = True ,
                                        jaw         = True ,
                                        torso       = True ,
                                        clavL       = True ,
                                        clavR       = True ,
                                        armL        = True ,
                                        armR        = True ,
                                        fingersL    = ['Thumb','Index','Middle','Ring','Pinky'] ,
                                        fingersR    = ['Thumb','Index','Middle','Ring','Pinky'] ,
                                        breastL     = False ,
                                        breastR     = False ,
                                        legL        = True ,
                                        legR        = True ,
                                        footL       = True,
                                        footR       = True ,
                                        toeFingersL = [] ,
                                        toeFingersR = [] ,
                                        sex         = 'Male' ,
                                        ribbon      = True ,
                                        hi          = True ,
                                        inbtw       = True ,
                                )


     print '----------DONE CHARACTER RIG----------'