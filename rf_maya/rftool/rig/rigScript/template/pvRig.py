from rigScript import characterRig
reload(characterRig)
from rigScript import ctrlData
reload(ctrlData)
import os

def Run():
     charRig = characterRig.HumanRig(   head        = True ,
                                        hairRoot    = False ,
                                        hair        = [] ,
                                        neck        = True ,
                                        eyeL        = True ,
                                        eyeR        = True ,
                                        addEye      = [] ,
                                        iris        = False ,
                                        pupil       = False ,
                                        eyeDot      = False ,
                                        jaw         = True ,
                                        torso       = True ,
                                        clavL       = True ,
                                        clavR       = True ,
                                        armL        = True ,
                                        armR        = True ,
                                        fingersL    = ['Thumb','Index','Middle','Ring','Pinky'] ,
                                        fingersR    = ['Thumb','Index','Middle','Ring','Pinky'] ,
                                        breastL     = False ,
                                        breastR     = False ,
                                        legL        = True ,
                                        legR        = True ,
                                        footL       = True,
                                        footR       = True ,
                                        toeFingersL = [] ,
                                        toeFingersR = [] ,
                                        sex         = 'Male' ,
                                        ribbon      = True ,
                                        hi          = True ,
                                        inbtw       = False ,
                                )

     corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
     pathScript = "{}/rf_maya/rftool/rig/rigScript/template".format(corePath)

     ctrlData.read_ctrl(pathFld=pathScript ,pcssName="pvRig")

     print '----------DONE CHARACTER RIG----------'