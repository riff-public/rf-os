from DevScript import characterRig
reload(characterRig)
def Run(size = 1):
     char = characterRig.HumanRig(head        = True ,
                                   neck        = True ,
                                   eyeL        = True ,
                                   eyeR        = True ,
                                   jaw         = True ,
                                   torso       = True ,
                                   clavL       = True ,
                                   clavR       = True ,
                                   armL        = True ,
                                   armR        = True ,
                                   fingersL    = ['Thumb','Index','Middle','Ring','Pinky'] ,
                                   fingersR    = ['Thumb','Index','Middle','Ring','Pinky'] ,
                                   breastL     = True ,
                                   breastR     = True ,
                                   legL        = True ,
                                   legR        = True ,
                                   footL       = True,
                                   footR       = True ,
                                   sex         = 'Female' ,
                                   ribbon      = True ,
                                   hi          = True ,
                                   size        = size 
                           )



Run()