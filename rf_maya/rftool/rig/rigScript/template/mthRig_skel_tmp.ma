//Maya ASCII 2018ff09 scene
//Name: mthRig_skel_tmp.ma
//Last modified: Thu, May 20, 2021 04:07:01 PM
//Codeset: 1252
requires maya "2018ff09";
requires "stereoCamera" "10.0";
requires "mtoa" "3.3.0.2";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201903222215-65bada0e52";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "B359EA12-40D8-0EF1-7F85-3BB9FE7E3825";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 10.343236409998667 12.648818972671762 5.0714282734675935 ;
	setAttr ".r" -type "double3" -21.33835272960355 64.600000000000733 -3.7075007778323825e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "3078C7D7-47D5-B165-88A9-1CB9AAE6CC39";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 12.292746374864732;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0 8.1757982723813498 0.16009853312238312 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "5FA6B4FE-4B71-C9C6-5E38-E290202DB923";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "D20D1ADF-415A-16A4-CF0D-29BFB6E02D3A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "58B7DBF1-413F-06EF-7130-D99992170046";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "18DAB3FA-4DE2-D0F0-9220-66B7BD9F3889";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "5792D981-4211-477D-2C3A-AF99490069A6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "DDF97792-4216-3BB4-6001-5CAE08F70CED";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "Export_Grp";
	rename -uid "60368D78-4A7B-CD87-CB0B-5CA7D964167F";
createNode transform -n "LipsIn_TmpCrv" -p "Export_Grp";
	rename -uid "0BD2BCD1-4E86-7A99-E83F-33B4056610B5";
	setAttr ".t" -type "double3" 0 2 0 ;
createNode nurbsCurve -n "LipsIn_TmpCrvShape" -p "LipsIn_TmpCrv";
	rename -uid "55313066-40AD-FA5E-4B7F-DAA4649ED901";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 80 2 no 3
		85 -0.025000000000000001 -0.012500000000000001 0 0.012500000000000001 0.025000000000000001
		 0.037499999999999999 0.050000000000000003 0.0625 0.074999999999999997 0.087499999999999981
		 0.10000000000000001 0.1125 0.125 0.13750000000000001 0.14999999999999999 0.16250000000000001
		 0.17499999999999996 0.1875 0.20000000000000001 0.21249999999999999 0.22500000000000001
		 0.23749999999999999 0.25 0.26250000000000001 0.27500000000000002 0.28749999999999998
		 0.29999999999999999 0.3125 0.32500000000000001 0.33750000000000002 0.34999999999999992
		 0.36249999999999999 0.375 0.38750000000000001 0.40000000000000002 0.41249999999999998
		 0.42499999999999999 0.4375 0.45000000000000001 0.46249999999999997 0.47499999999999998
		 0.48750000000000004 0.5 0.51249999999999996 0.52500000000000002 0.53749999999999998
		 0.55000000000000004 0.5625 0.57499999999999996 0.58750000000000002 0.59999999999999998
		 0.61250000000000004 0.625 0.63749999999999996 0.65000000000000002 0.66249999999999998
		 0.67500000000000004 0.6875 0.69999999999999984 0.71249999999999991 0.72499999999999998
		 0.73750000000000004 0.75 0.76250000000000007 0.77500000000000002 0.78749999999999998
		 0.80000000000000004 0.81249999999999989 0.82499999999999996 0.83750000000000002 0.84999999999999998
		 0.86250000000000004 0.875 0.88749999999999996 0.90000000000000002 0.91249999999999987
		 0.92499999999999993 0.9375 0.94999999999999996 0.96250000000000002 0.97500000000000009
		 0.98750000000000004 1 1.0125 1.0249999999999999
		83
		0.61354586168120717 4.7759637515424624e-16 -7.7997407168625035
		-1.6472794139396699e-15 4.7915503898965337e-16 -7.8251956290296798
		-0.61354586168120184 4.7759637515424624e-16 -7.7997407168625035
		-1.2230124756209102 4.7301906208077842e-16 -7.7249875214651622
		-1.8246918907081606 4.6551898112701274e-16 -7.6025019042408939
		-2.4150602559706664 4.5518735888042355e-16 -7.433773708425039
		-2.990777905926159 4.4212080663209703e-16 -7.2203807161365674
		-3.5486946643838713 4.2642426587652658e-16 -6.9640367520401814
		-4.0855809561615652 4.0818311579607537e-16 -6.6661361639987566
		-4.5978135049922102 3.8744997405435917e-16 -6.32753826367109
		-5.0813947453194057 3.642773540155866e-16 -5.9491006593772422
		-5.5326691946715965 3.3877857403730671e-16 -5.5326739803374769
		-5.9490990075643762 3.1114614950509193e-16 -5.0814022413927713
		-6.327535837974402 2.8153529694077877e-16 -4.5978203207127777
		-6.6661297170883582 2.5016992954665687e-16 -4.085584998398482
		-6.9640278012936179 2.1729505467656134e-16 -3.5486975481886001
		-7.2203780801895086 1.8313269227690224e-16 -2.9907838309691717
		-7.4337811272845356 1.4788038268090842e-16 -2.4150699252040391
		-7.6025115293066765 1.1173076067762124e-16 -1.8247017957408218
		-7.7249783146900119 7.4888262719487728e-17 -1.2230181432169269
		-7.799703635501646 3.7568990469670332e-17 -0.61354817561810382
		-7.8251448172061391 1.3863562235988262e-22 -2.2640915330831551e-06
		-7.7997040383944523 -3.7568714436909147e-17 0.61354366766100898
		-7.7249789748720605 -7.4887988674960937e-17 1.2230136677301711
		-7.6025124544052343 -1.1173048988530234e-16 1.8246973733666529
		-7.4337825683140411 -1.4788012026067943e-16 2.4150656395564694
		-7.2203801803115812 -1.8313244530227896e-16 2.9907797975674835
		-6.9640301835722518 -2.1729481591372762e-16 3.5486936488956022
		-6.6661317629787327 -2.5016968111144185e-16 4.0855809411435251
		-6.3275374930981032 -2.8153503045858377e-16 4.5978159687282778
		-5.9491013758626785 -3.1114591606666447e-16 5.0813984290539409
		-5.5326733108083275 -3.3877843512520035e-16 5.5326717117306154
		-5.0814001782092939 -3.6427731003944627e-16 5.9490999411923475
		-4.5978178226088433 -3.8744984729503653e-16 6.3275361935342378
		-4.0855828651508341 -4.0818277332131208e-16 6.6661305709614389
		-3.5486956453777676 -4.2642379849731074e-16 6.9640291191583579
		-2.9907818809881759 -4.4212072013655443e-16 7.2203793035571628
		-2.4150678001645218 -4.551878604689075e-16 7.4337818999866236
		-1.8246995818887646 -4.6551959681250949e-16 7.6025119591480692
		-1.2230159019360614 -4.7301851621540945e-16 7.7249786068071797
		-0.61354592185750045 -4.7759411618318218e-16 7.7997038250653601
		-3.4858639764276685e-15 -4.7915192753508277e-16 7.8251448151203826
		0.61354592185749557 -4.7759411618318159e-16 7.7997038250653521
		1.2230159019360616 -4.7301851621540935e-16 7.7249786068071673
		1.8246995818887473 -4.655195968125088e-16 7.6025119591480657
		2.4150678001645285 -4.5518786046890789e-16 7.4337818999866245
		2.9907818809881714 -4.4212072013655492e-16 7.2203793035571886
		3.5486956453777707 -4.2642379849731128e-16 6.9640291191583534
		4.0855828651508324 -4.0818277332131233e-16 6.6661305709614558
		4.5978178226088371 -3.8744984729503613e-16 6.3275361935342209
		5.0814001782092868 -3.6427731003944617e-16 5.9490999411923378
		5.532673310808323 -3.387784351252001e-16 5.5326717117306039
		5.9491013758626909 -3.1114591606666447e-16 5.0813984290539409
		6.3275374930980979 -2.8153503045858254e-16 4.5978159687282654
		6.6661317629787602 -2.5016968111144209e-16 4.0855809411435198
		6.9640301835722402 -2.1729481591372612e-16 3.5486936488955916
		7.2203801803115919 -1.8313244530227876e-16 2.9907797975674684
		7.4337825683140357 -1.4788012026067847e-16 2.415065639556456
		7.6025124544052476 -1.1173048988530167e-16 1.8246973733666401
		7.7249789748720632 -7.4887988674960001e-17 1.2230136677301562
		7.799704038394454 -3.756871443690818e-17 0.61354366766099511
		7.825144817206148 1.3863562301634918e-22 -2.2640915440904926e-06
		7.7997036355016434 3.7568990469670801e-17 -0.6135481756181087
		7.7249783146900262 7.4888262719487851e-17 -1.2230181432169358
		7.6025115293066605 1.1173076067762171e-16 -1.8247017957408251
		7.4337811272845471 1.4788038268090846e-16 -2.415069925204044
		7.2203780801895094 1.831326922769032e-16 -2.9907838309691734
		6.9640278012936241 2.1729505467656144e-16 -3.5486975481886027
		6.6661297170883556 2.5016992954665647e-16 -4.0855849983984713
		6.3275358379744118 2.8153529694077788e-16 -4.597820320712783
		5.9490990075643726 3.1114614950509109e-16 -5.0814022413927535
		5.5326691946715991 3.3877857403730661e-16 -5.5326739803374885
		5.0813947453193915 3.6427735401558749e-16 -5.9491006593772182
		4.5978135049922102 3.8744997405435912e-16 -6.3275382636711077
		4.0855809561615537 4.0818311579607587e-16 -6.6661361639987433
		3.548694664383877 4.2642426587652515e-16 -6.9640367520401858
		2.9907779059261554 4.4212080663209698e-16 -7.2203807161365674
		2.4150602559706758 4.5518735888042236e-16 -7.4337737084250408
		1.8246918907081646 4.6551898112701245e-16 -7.6025019042408966
		1.223012475620914 4.7301906208077872e-16 -7.7249875214651622
		0.61354586168120717 4.7759637515424624e-16 -7.7997407168625035
		-1.6472794139396699e-15 4.7915503898965337e-16 -7.8251956290296798
		-0.61354586168120184 4.7759637515424624e-16 -7.7997407168625035
		;
createNode transform -n "LipsOut_TmpCrv" -p "Export_Grp";
	rename -uid "3F28EB63-4241-DD5B-0D6C-48835904E36F";
createNode nurbsCurve -n "LipsOut_TmpCrvShape" -p "LipsOut_TmpCrv";
	rename -uid "B2509D4F-4CCC-AAD6-09A3-8DAF27D09E6D";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 80 2 no 3
		85 -0.025000000000000001 -0.012500000000000001 0 0.012500000000000001 0.025000000000000001
		 0.037499999999999999 0.050000000000000003 0.0625 0.074999999999999997 0.087499999999999981
		 0.10000000000000001 0.1125 0.125 0.13750000000000001 0.14999999999999999 0.16250000000000001
		 0.17499999999999996 0.1875 0.20000000000000001 0.21249999999999999 0.22500000000000001
		 0.23749999999999999 0.25 0.26250000000000001 0.27500000000000002 0.28749999999999998
		 0.29999999999999999 0.3125 0.32500000000000001 0.33750000000000002 0.34999999999999992
		 0.36249999999999999 0.375 0.38750000000000001 0.40000000000000002 0.41249999999999998
		 0.42499999999999999 0.4375 0.45000000000000001 0.46249999999999997 0.47499999999999998
		 0.48750000000000004 0.5 0.51249999999999996 0.52500000000000002 0.53749999999999998
		 0.55000000000000004 0.5625 0.57499999999999996 0.58750000000000002 0.59999999999999998
		 0.61250000000000004 0.625 0.63749999999999996 0.65000000000000002 0.66249999999999998
		 0.67500000000000004 0.6875 0.69999999999999984 0.71249999999999991 0.72499999999999998
		 0.73750000000000004 0.75 0.76250000000000007 0.77500000000000002 0.78749999999999998
		 0.80000000000000004 0.81249999999999989 0.82499999999999996 0.83750000000000002 0.84999999999999998
		 0.86250000000000004 0.875 0.88749999999999996 0.90000000000000002 0.91249999999999987
		 0.92499999999999993 0.9375 0.94999999999999996 0.96250000000000002 0.97500000000000009
		 0.98750000000000004 1 1.0125 1.0249999999999999
		83
		0.61354586168120717 4.7759637515424624e-16 -7.7997407168625035
		-1.6472794139396699e-15 4.7915503898965337e-16 -7.8251956290296798
		-0.61354586168120184 4.7759637515424624e-16 -7.7997407168625035
		-1.2230124756209102 4.7301906208077842e-16 -7.7249875214651622
		-1.8246918907081606 4.6551898112701274e-16 -7.6025019042408939
		-2.4150602559706664 4.5518735888042355e-16 -7.433773708425039
		-2.990777905926159 4.4212080663209703e-16 -7.2203807161365674
		-3.5486946643838713 4.2642426587652658e-16 -6.9640367520401814
		-4.0855809561615652 4.0818311579607537e-16 -6.6661361639987566
		-4.5978135049922102 3.8744997405435917e-16 -6.32753826367109
		-5.0813947453194057 3.642773540155866e-16 -5.9491006593772422
		-5.5326691946715965 3.3877857403730671e-16 -5.5326739803374769
		-5.9490990075643762 3.1114614950509193e-16 -5.0814022413927713
		-6.327535837974402 2.8153529694077877e-16 -4.5978203207127777
		-6.6661297170883582 2.5016992954665687e-16 -4.085584998398482
		-6.9640278012936179 2.1729505467656134e-16 -3.5486975481886001
		-7.2203780801895086 1.8313269227690224e-16 -2.9907838309691717
		-7.4337811272845356 1.4788038268090842e-16 -2.4150699252040391
		-7.6025115293066765 1.1173076067762124e-16 -1.8247017957408218
		-7.7249783146900119 7.4888262719487728e-17 -1.2230181432169269
		-7.799703635501646 3.7568990469670332e-17 -0.61354817561810382
		-7.8251448172061391 1.3863562235988262e-22 -2.2640915330831551e-06
		-7.7997040383944523 -3.7568714436909147e-17 0.61354366766100898
		-7.7249789748720605 -7.4887988674960937e-17 1.2230136677301711
		-7.6025124544052343 -1.1173048988530234e-16 1.8246973733666529
		-7.4337825683140411 -1.4788012026067943e-16 2.4150656395564694
		-7.2203801803115812 -1.8313244530227896e-16 2.9907797975674835
		-6.9640301835722518 -2.1729481591372762e-16 3.5486936488956022
		-6.6661317629787327 -2.5016968111144185e-16 4.0855809411435251
		-6.3275374930981032 -2.8153503045858377e-16 4.5978159687282778
		-5.9491013758626785 -3.1114591606666447e-16 5.0813984290539409
		-5.5326733108083275 -3.3877843512520035e-16 5.5326717117306154
		-5.0814001782092939 -3.6427731003944627e-16 5.9490999411923475
		-4.5978178226088433 -3.8744984729503653e-16 6.3275361935342378
		-4.0855828651508341 -4.0818277332131208e-16 6.6661305709614389
		-3.5486956453777676 -4.2642379849731074e-16 6.9640291191583579
		-2.9907818809881759 -4.4212072013655443e-16 7.2203793035571628
		-2.4150678001645218 -4.551878604689075e-16 7.4337818999866236
		-1.8246995818887646 -4.6551959681250949e-16 7.6025119591480692
		-1.2230159019360614 -4.7301851621540945e-16 7.7249786068071797
		-0.61354592185750045 -4.7759411618318218e-16 7.7997038250653601
		-3.4858639764276685e-15 -4.7915192753508277e-16 7.8251448151203826
		0.61354592185749557 -4.7759411618318159e-16 7.7997038250653521
		1.2230159019360616 -4.7301851621540935e-16 7.7249786068071673
		1.8246995818887473 -4.655195968125088e-16 7.6025119591480657
		2.4150678001645285 -4.5518786046890789e-16 7.4337818999866245
		2.9907818809881714 -4.4212072013655492e-16 7.2203793035571886
		3.5486956453777707 -4.2642379849731128e-16 6.9640291191583534
		4.0855828651508324 -4.0818277332131233e-16 6.6661305709614558
		4.5978178226088371 -3.8744984729503613e-16 6.3275361935342209
		5.0814001782092868 -3.6427731003944617e-16 5.9490999411923378
		5.532673310808323 -3.387784351252001e-16 5.5326717117306039
		5.9491013758626909 -3.1114591606666447e-16 5.0813984290539409
		6.3275374930980979 -2.8153503045858254e-16 4.5978159687282654
		6.6661317629787602 -2.5016968111144209e-16 4.0855809411435198
		6.9640301835722402 -2.1729481591372612e-16 3.5486936488955916
		7.2203801803115919 -1.8313244530227876e-16 2.9907797975674684
		7.4337825683140357 -1.4788012026067847e-16 2.415065639556456
		7.6025124544052476 -1.1173048988530167e-16 1.8246973733666401
		7.7249789748720632 -7.4887988674960001e-17 1.2230136677301562
		7.799704038394454 -3.756871443690818e-17 0.61354366766099511
		7.825144817206148 1.3863562301634918e-22 -2.2640915440904926e-06
		7.7997036355016434 3.7568990469670801e-17 -0.6135481756181087
		7.7249783146900262 7.4888262719487851e-17 -1.2230181432169358
		7.6025115293066605 1.1173076067762171e-16 -1.8247017957408251
		7.4337811272845471 1.4788038268090846e-16 -2.415069925204044
		7.2203780801895094 1.831326922769032e-16 -2.9907838309691734
		6.9640278012936241 2.1729505467656144e-16 -3.5486975481886027
		6.6661297170883556 2.5016992954665647e-16 -4.0855849983984713
		6.3275358379744118 2.8153529694077788e-16 -4.597820320712783
		5.9490990075643726 3.1114614950509109e-16 -5.0814022413927535
		5.5326691946715991 3.3877857403730661e-16 -5.5326739803374885
		5.0813947453193915 3.6427735401558749e-16 -5.9491006593772182
		4.5978135049922102 3.8744997405435912e-16 -6.3275382636711077
		4.0855809561615537 4.0818311579607587e-16 -6.6661361639987433
		3.548694664383877 4.2642426587652515e-16 -6.9640367520401858
		2.9907779059261554 4.4212080663209698e-16 -7.2203807161365674
		2.4150602559706758 4.5518735888042236e-16 -7.4337737084250408
		1.8246918907081646 4.6551898112701245e-16 -7.6025019042408966
		1.223012475620914 4.7301906208077872e-16 -7.7249875214651622
		0.61354586168120717 4.7759637515424624e-16 -7.7997407168625035
		-1.6472794139396699e-15 4.7915503898965337e-16 -7.8251956290296798
		-0.61354586168120184 4.7759637515424624e-16 -7.7997407168625035
		;
createNode transform -n "mthJawLwr1_Loc" -p "Export_Grp";
	rename -uid "8B4ECF16-4131-2A40-E6BB-B6A660F44E15";
	setAttr ".t" -type "double3" -2.0194839173657902e-28 8.2858464271867476 -0.063547247568244258 ;
	setAttr ".s" -type "double3" 0.64845294605989723 0.64845294605989723 0.64845294605989723 ;
createNode locator -n "mthJawLwr1_LocShape" -p "mthJawLwr1_Loc";
	rename -uid "F408B8A4-4ED8-50AB-571D-E1BA9F409226";
	setAttr -k off ".v";
createNode transform -n "mthJaw_Grp" -p "Export_Grp";
	rename -uid "D5EDD5E8-456B-8F4B-D465-F3934BB7A44F";
	setAttr ".t" -type "double3" -2.0194839173657902e-28 8.2858464271867476 -0.063547247568244258 ;
createNode joint -n "mthJawLwr1_Jnt" -p "mthJaw_Grp";
	rename -uid "E6811F5C-4D51-2DBB-7B5F-948AB0325F66";
	setAttr ".t" -type "double3" -2.4714936705377201e-27 -0.029429451830784714 0.18344870692857787 ;
	setAttr ".jo" -type "double3" 31.983454365182009 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "mthJawLwr2_Jnt" -p "mthJawLwr1_Jnt";
	rename -uid "7C14238A-4155-576E-B083-42A8D17A3D78";
	setAttr ".t" -type "double3" 1.8124286917276632e-27 -0.064865005761177486 0.087897085122842 ;
	setAttr ".jo" -type "double3" -12.99367517147877 0 0 ;
	setAttr ".radi" 0.05;
createNode joint -n "mthHead_Jnt" -p "Export_Grp";
	rename -uid "B5611ECA-4A72-39E5-DDE6-E28589C59218";
	setAttr ".t" -type "double3" -2.0194839173657902e-28 8.2858464271867476 -0.063547247568244258 ;
	setAttr ".radi" 0.05;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "DE30468D-485B-E18A-9C16-BB877956A5EF";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "A8985EA2-4264-1493-272E-CA93657AA89B";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "D7464352-4214-8C5B-9199-3DBB021FED88";
createNode displayLayerManager -n "layerManager";
	rename -uid "60A2815A-4E4B-BC75-50EF-65A3A707A522";
createNode displayLayer -n "defaultLayer";
	rename -uid "E886BF9B-4796-05D5-8773-C0A06CEF586A";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "958013C3-4179-5DB9-2A95-D9A0553B24C4";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "B9317D85-4B79-5449-1840-8FA0E3C813DD";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "D0405487-48E3-C6D1-ABBB-7CB2D6A485D2";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "705C7D58-4BDA-B025-8E99-D7B49D1A5471";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -279.76189364516591 -199.99999205271436 ;
	setAttr ".tgi[0].vh" -type "double2" 399.99998410542878 208.3333250549108 ;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av -k on ".unw" 1;
	setAttr -av -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -av -k on ".ihi";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr -k on ".hwi";
	setAttr -av ".ta";
	setAttr -av ".tq";
	setAttr -av ".etmr";
	setAttr -av ".tmr";
	setAttr -av ".aoon";
	setAttr -av ".aoam";
	setAttr -av ".aora";
	setAttr -k on ".hff";
	setAttr -av -k on ".hfd";
	setAttr -av -k on ".hfs";
	setAttr -av -k on ".hfe";
	setAttr -av -k on ".hfcr";
	setAttr -av -k on ".hfcg";
	setAttr -av -k on ".hfcb";
	setAttr -av -k on ".hfa";
	setAttr -av ".mbe";
	setAttr -av -k on ".mbsof";
	setAttr -k on ".blen";
	setAttr -k on ".blat";
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".bbx";
	setAttr -k on ".vwm";
	setAttr -k on ".tpv";
	setAttr -k on ".uit";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".macc";
	setAttr -av -k on ".macd";
	setAttr -av -k on ".macq";
	setAttr -av -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -av -k on ".clip";
	setAttr -av -k on ".edm";
	setAttr -av -k on ".edl";
	setAttr -cb on ".ren" -type "string" "arnold";
	setAttr -av -k on ".esr";
	setAttr -av -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -av -cb on ".imfkey";
	setAttr -av -k on ".gama";
	setAttr -k on ".exrc";
	setAttr -k on ".expt";
	setAttr -av -k on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -av -k on ".be";
	setAttr -av -cb on ".ep";
	setAttr -av -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -av -k on ".pff";
	setAttr -av -cb on ".peie";
	setAttr -av -cb on ".ifp";
	setAttr -k on ".rv";
	setAttr -av -k on ".comp";
	setAttr -av -k on ".cth";
	setAttr -av -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -av -k on ".rd";
	setAttr -av -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -av -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -av -k on ".mm";
	setAttr -av -k on ".npu";
	setAttr -av -k on ".itf";
	setAttr -av -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -av -k on ".uf";
	setAttr -av -k on ".oi";
	setAttr -av -k on ".rut";
	setAttr -av -k on ".mot";
	setAttr -av -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -av -k on ".mbso";
	setAttr -av -k on ".mbsc";
	setAttr -av -k on ".afp";
	setAttr -av -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -av -k on ".ubc";
	setAttr -av -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -av -k on ".udbx";
	setAttr -av -k on ".smc";
	setAttr -av -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -av -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -av -k on ".tlwd";
	setAttr -av -k on ".tlht";
	setAttr -av -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -av -k on ".ope";
	setAttr -av -k on ".oppf";
	setAttr -av -k on ".rcp";
	setAttr -av -k on ".icp";
	setAttr -av -k on ".ocp";
	setAttr -cb on ".hbl";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -av -k off -cb on ".fbfm";
	setAttr -av -k off -cb on ".ehql";
	setAttr -av -k off -cb on ".eams";
	setAttr -av -k off -cb on ".eeaa";
	setAttr -av -k off -cb on ".engm";
	setAttr -av -k off -cb on ".mes";
	setAttr -av -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -av -k off -cb on ".mbs";
	setAttr -av -k off -cb on ".trm";
	setAttr -av -k off -cb on ".tshc";
	setAttr -av -k off -cb on ".enpt";
	setAttr -av -k off -cb on ".clmt";
	setAttr -av -k off -cb on ".tcov";
	setAttr -av -k off -cb on ".lith";
	setAttr -av -k off -cb on ".sobc";
	setAttr -av -k off -cb on ".cuth";
	setAttr -av -k off -cb on ".hgcd";
	setAttr -av -k off -cb on ".hgci";
	setAttr -av -k off -cb on ".mgcs";
	setAttr -av -k off -cb on ".twa";
	setAttr -av -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
connectAttr "mthJawLwr1_Jnt.s" "mthJawLwr2_Jnt.is";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
dataStructure -fmt "raw" -as "name=faceConnectOutputStructure:bool=faceConnectOutput:string[200]=faceConnectOutputAttributes:string[200]=faceConnectOutputGroups";
dataStructure -fmt "raw" -as "name=faceConnectMarkerStructure:bool=faceConnectMarker:string[200]=faceConnectOutputGroups";
dataStructure -fmt "raw" -as "name=FBXFastExportSetting_FBX:string=54";
dataStructure -fmt "raw" -as "name=FBXFastExportSetting_MB:string=19424";
dataStructure -fmt "raw" -as "name=externalContentTablZ:string=nodZ:string=key:string=upath:uint32=upathcrc:string=rpath:string=roles";
dataStructure -fmt "raw" -as "name=idStructure:int32=ID";
// End of mthRig_skel_tmp.ma
