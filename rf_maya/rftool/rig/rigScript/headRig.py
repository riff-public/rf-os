import maya.cmds as mc
import maya.mel as mm

import core
import rigTools as rt
import ctrlRig as cr
reload( core )
reload( rt )
reload(cr)
import fkRig
reload(fkRig)

cn = core.Node()
# -------------------------------------------------------------------------------------------------------------
#
#  HEAD RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , headTmpJnt       = 'Head_TmpJnt' ,
                         headEndTmpJnt    = 'HeadEnd_TmpJnt' ,
                         parent           = 'NeckEnd_Jnt' ,
                         ctrlGrp          = 'Ctrl_Grp' ,
                         skinGrp          = 'Skin_Grp' ,
                         elem             = '' ,
                         side             = '' ,
                         size             = 1 
                 ):

        ##-- Info
        elem = elem.capitalize()

        if side == '' :
            sideRbn = ''
            side = '_'
        else :
            sideRbn = '{}'.format(side)
            side = '_{}_'.format(side)

        self.headJnt =  'Head{}{}Jnt' .format( elem , side )  

        if not mc.objExists( self.headJnt) :
            fkTmpJnt = headTmpJnt
            fkJntAs = 1
        else :
            fkTmpJnt = self.headJnt
            fkJntAs = 0

        #-- Create Controls Head
        self.headFk =   fkRig.Run   (name           = 'Head{}'.format( elem ) ,
                                     side           = sideRbn ,
                                     tmpJnt         = fkTmpJnt ,
                                     parent         = parent ,
                                     ctrlGrp        = ctrlGrp , 
                                     shape          = 'cube' ,
                                     jnt_ctrl       = 1,
                                     size           = size ,
                                     color          = 'blue',
                                     localWorld     = 1,
                                     defaultWorld   = 1,
                                     constraint     = 1,
                                     scale_constraint = 1,
                                     jnt            = fkJntAs,
                                     dtl            = 0,
                                     dtlShape       = 'cube',
                                     dtlColor       = 'blue')

        self.headCtrlGrp    = self.headFk.ctrlGrp
        self.headCtrl       = self.headFk.ctrl
        self.headZro        = self.headFk.zro 
        self.headOfst       = self.headFk.ofst
        self.headDrv        = self.headFk.drv
        self.headInv        = self.headFk.inv
        self.headPars       = self.headFk.pars
        self.headGmbl       = self.headFk.gmbl
        self.headJnt        = self.headFk.jnt

        #-- AddneckEndJnt
        self.headEndJnt = cn.joint( 'HeadEnd{}{}Jnt'.format( elem , side ) , headEndTmpJnt )
        self.headEndJnt.parent(self.headJnt)

        #-- Adjust Rotate Order
        for obj in ( self.headCtrl , self.headJnt , self.headEndJnt ) :
            obj.setRotateOrder( 'xzy' )     

        #-- Rig process Head
        mc.scaleConstraint( self.headGmbl , self.headJnt , mo = False )

        self.headGmbl.attr('ssc').value = 0
        self.headEndJnt.attr('ssc').value = 0

        #-- Cleanup Head
        for obj in ( self.headCtrlGrp , self.headZro ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for obj in ( self.headCtrl , self.headGmbl ) :
            obj.lockHideAttrs( 'v' )

        attrs = ('sx' , 'sy' , 'sz' )
        for attr in attrs :
            mc.setAttr( '{}.{}'.format( self.headCtrl, attr ) , l = False , k = True )

        self.headJnt.attr('ssc').value = 0
    
    mc.select( cl = True )