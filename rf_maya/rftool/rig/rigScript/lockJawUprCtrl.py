import maya.cmds as mc
import pymel.core as pm
import core
reload( core )
import rigTools as rt
reload( rt )


def Run(    jawUprCtrl = 'bodyRig_md:JawUpr1_Ctrl' ,
            jawUprGmbl = 'bodyRig_md:JawUpr1Gmbl_Ctrl'):

    jawUprCtrl = core.Dag(jawUprCtrl)
    jawUprGmbl = core.Dag(jawUprGmbl)

    for each in (jawUprCtrl , jawUprGmbl):
        pm.PyNode(each).tx.lock()
        pm.PyNode(each).ty.lock()
        pm.PyNode(each).tz.lock()
        pm.PyNode(each).rx.lock()
        pm.PyNode(each).ry.lock()
        pm.PyNode(each).rz.lock()
        pm.PyNode(each).sx.lock()
        pm.PyNode(each).sy.lock()
        pm.PyNode(each).sz.lock()
        pm.PyNode(each).v.unlock()
        each.attr('v').v = 0
        pm.PyNode(each).v.lock()