import os, json, re
import maya.cmds as mc
import pymel.core as pmc
from functools import partial

'''

1. Select Render Layers
2. Select Geo
3. Run Script

from rf_maya.rftool.rig.rigScript import caTextureSeqRig
reload(caTextureSeqRig)
caTextureSeqRig.textureSeqUI()

'''
def textureSeqUI():
    
    width = 270
    
    if pmc.window('TextureSeqUI', exists = True):
        pmc.deleteUI('TextureSeqUI')
        
    window = pmc.window('TextureSeqUI', title = "Texture Seq Tools", width = width ,
        mnb = True , mxb = False , sizeable = True , rtf = True )

    window = pmc.window('TextureSeqUI', e = True, width = width )
    
    layout1 = pmc.rowColumnLayout(nc = 1, columnWidth = (1,width))
    with layout1 :
        pmc.separator(h = 7, vis = False)
        pmc.text(label = '1. Import Shad Preview')
        pmc.text(label = '2. Select Render Layers')
        pmc.text(label = '3. Select Geo')
        pmc.separator(vis = False)
        pmc.separator(h = 7, st = 'in')
        pmc.button(label = 'Setup Look', h = 30, c = import_look)
    
    layout2 = pmc.rowColumnLayout(nc = 4, columnWidth = [(1,70),(2,173),(3,4),(4,20)])
    with layout2 :
        pmc.text(label = 'Geo Group : ', al = 'right')
        pmc.textField('geoGroupTF', en = True)
        pmc.separator( vis = False )
        pmc.button(label = '+', c = partial(add_selected_in_textField, 'geoGroupTF'))
        
        pmc.text(label = 'Controller : ', al = 'right')
        pmc.textField('controllerTF', en = True)
        pmc.separator( vis = False )
        pmc.button(label = '+', c = partial(add_selected_in_textField, 'controllerTF'))
        
        pmc.text(label = 'Attr Name : ', al = 'right')
        pmc.textField('attrNameTF', en = True)
        pmc.popupMenu()
        for attr in('costumeColors', 'hairColors', 'eyeColors', 'eyebrowColors', 'bodyColors'):
            pmc.menuItem(l = attr, c = partial(change_textField,  'attrNameTF', attr))
        pmc.separator( vis = False )
        pmc.separator( vis = False )
    
    layout3 = pmc.rowColumnLayout(nc = 1, columnWidth = (1,width))
    with layout3 :
        pmc.separator(h = 7, st = 'in')
        pmc.button(label = 'RUN', h = 30, c = run_button)
        pmc.separator(h = 7, st = 'in')
        pmc.button(label = 'Open Folder Texture', h = 30, c = find_texture_location)

    pmc.showWindow( window )

def import_look(*args):
    from rftool.asset.setup import setup_app
    reload(setup_app)
    setup_app.AssetSetup().setup_looks()

def import_lookdev_look(*args):
    scnPath = pmc.sceneName()
    tmpAry = scnPath.split('/')
    tmpAry[3] = 'publ'
    tmpAry[6] = 'texture'
    tmpAry[8] = 'hero'
    mtrLdvNm = tmpAry[5] + '_mtr_mdl_main_md.hero.ma'
    mtrLdvPath = '/'.join(tmpAry[0:-1]) + '/output/' + mtrLdvNm
    
    mc.editRenderLayerGlobals(crl = 'main_lookdev')
    
    from rf_maya.rftool.shade import shade_utils
    reload(shade_utils)
    
    shade_utils.apply_ref_shade(shadeFile = mtrLdvPath,
                                shadeNamespace = '{}_mdlLdvMtr'.format(tmpAry[5]),
                                targetNamespace = 'bodyRig_md',
                                connectionInfo = [])         
                                
def add_selected_in_textField(obj = '', *args):
    sels = pmc.ls(sl = True)
    texts = ', '.join([ str(x) for x in sels ])
    pmc.textField(obj, e = True, tx = texts)

def change_textField(obj = '', text = '', *args):
    pmc.textField(obj, e = True, tx = text)

def run_button(*args):
    geoGrpNm = pmc.textField('geoGroupTF', q = True, tx = True)
    ctrlNm = pmc.textField('controllerTF', q = True, tx = True)
    attrNm = pmc.textField('attrNameTF', q = True, tx = True)
    add(ctrl = ctrlNm, attrName = attrNm, geoGrp = geoGrpNm)
    
def find_texture_location(*args):
    scnPath = mc.file(q = True, sn = True)
    array = scnPath.split('/')
    array[3] = 'publ'
    previewPath = '/'.join(array[0:6]) + '/texture/main/hero/texPreview'
    if os.path.isdir(previewPath):
        os.startfile(previewPath)
    
def add(ctrl = 'bodyRig_md:CostumeSub_Ctrl', attrName = 'costumeColors', geoGrp = 'bodyRig_md:Geo_Grp', max = 100):
    sels = mc.ls(sl = True)
    maxLen = 0
    txrFl = []
    
    if not mc.objExists('%s.%s'%(geoGrp,'sc__imageSeqEnable')):
        mc.addAttr(geoGrp, ln = 'sc__imageSeqEnable', at = 'long', k = 0, min = 1, max = 1)
        
    # if not '_' in attrName:
    #     attrName = attrName + '_'
    if not mc.objExists('%s.%s'%(ctrl,attrName)):
        mc.addAttr(ctrl, ln = attrName , at = 'long', k = 1, min = 1, max = max)
    for chldTrns in sels:
        mattAll = get_materials_on_selected_meshes(mesh = chldTrns)
        
        for matt in mattAll:
            if matt.nodeType() == 'RedshiftMaterial' :
                txrFile = mc.listConnections('%s.diffuse_color' %matt)
            else :
                txrFile = mc.listConnections('%s.color' %matt)
            
            if txrFile:
                if not txrFile[0] in txrFl:
                    txrFl.append(txrFile[0])
                    txrPath = mc.getAttr('%s.fileTextureName' %txrFile[0])
                    len = check_lenght_seq(files = txrPath)
                    
                    if len > 0:
                        objName = chldTrns.split(':')[-1]
                        attrCon = 'sc__%s' %objName
                        
                        if not mc.objExists('%s.%s'%(geoGrp,attrCon)):
                            mc.addAttr(geoGrp, ln = attrCon, at = 'long', k = 1, dv = 1)
                        
                        cmp = '%s_Cmp'%objName
                        
                        if not mc.objExists(cmp):
                            mc.createNode('clamp', n = cmp)
                            
                        mc.setAttr('%s.minR'%cmp, 1)
                        mc.setAttr('%s.maxR'%cmp, max)
                        
                        connect_attr('%s.%s'%(ctrl,attrName), '%s.inputR'%cmp)
                        connect_attr('%s.outputR'%cmp, '%s.%s'%(geoGrp,attrCon))
                        connect_attr('%s.%s'%(geoGrp,'sc__imageSeqEnable'), '%s.useFrameExtension'%txrFile[0])
                        
                        exp = mc.listConnections('{}.frameExtension'.format(txrFile[0]))
                        
                        if exp:
                            if mc.nodeType(exp) == 'expression':
                                mc.delete(exp)
                            
                        connect_attr('%s.%s'%(geoGrp,attrCon), '{}.frameExtension'.format(txrFile[0]))
                        
    # add Adl Node for SuitSeq_
    ctrl = 'fclRig_md:Variety_Ctrl'
    aadl = 'fclRig_md:HeadDefault_Adl'
    if mc.objExists(aadl):
        adl = '{}Adl'.format(attrName)
        if mc.objExists(adl):
            mc.delete(adl)
        # else:
        mc.duplicate(aadl)
        mc.rename('HeadDefault_Adl', adl)
        mc.connectAttr('{}.default'.format(adl),'{}.input1'.format(adl), f = True)
        mc.connectAttr('{}.Variety'.format(ctrl),'{}.input2'.format(adl))
        mc.connectAttr('{}.output'.format(adl),'{}Shape.{}'.format(ctrl, attrName))        
    # mc.select(ctrl)
    
def get_materials_on_selected_meshes(mesh = 'bodyRig_md:HatA_Geo'):
    all_materials = list()
    
    mesh = pmc.PyNode(mesh)
    for shading_engine in mesh.getShape().listConnections(type = 'shadingEngine'):
        materials = pmc.listConnections('%s.surfaceShader' %shading_engine)
        if materials:
            all_materials.extend(materials)
            
    all_materials = list(set(all_materials))
    return all_materials
    
def check_lenght_seq(files = ''):
    filesAry = files.split("/")
    dir_list = os.listdir('/'.join(filesAry[0:-1]))
    name = filesAry[-1].split('.')[0]
    pattern = []
    
    for dl in dir_list:
        if re.search('^{}.[0-9]+.png$'.format(name),dl):
            pattern.append(dl)
            
    return len(pattern)

def connect_attr(*args):
    conns = mc.listConnections(args[-1], d = False, s = True, p = True)
    if conns:
        for con in conns:
            mc.disconnectAttr(con, args[-1])
    mc.connectAttr(args[0], args[-1], f = True)