import maya.cmds as mc
import maya.mel as mm

import core
import rigTools as rt
import ctrlRig as cr
reload( core )
reload( rt )
reload(cr)
import fkRig
reload(fkRig)

cn = core.Node()
# -------------------------------------------------------------------------------------------------------------
#
#  EYE RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run(object):
	def __init__(self , eyeMainTrgtTmpJnt	= 'EyeTrgt_TmpJnt' ,
						eyeTrgtTmpJnt		= 'EyeTrgt_L_TmpJnt' ,
						eyeTmpJnt 			= 'Eye_L_TmpJnt' ,
						parent				= 'Head_Jnt' ,
						ctrlGrp				= 'MainCtrl_Grp' ,
						elem 				= '' ,
						side 				= '' ,
						clrTrgt 			= 'red',
						size				= 1 
				):
	
		##-- Info
		elem = elem.capitalize()

		if side == '' :
			sideRbn = ''
			side = '_'
		else :
			sideRbn = '{}'.format(side)
			side = '_{}_'.format(side)


		self.eyeCtrlGrp = core.Dag('Eye{}Ctrl_Grp'.format( elem ))
		
		# create main group
		if not mc.objExists('Eye{}Ctrl_Grp'.format( elem )):
			self.eyeCtrlGrp 	= cn.createNode( 'transform' , 'Eye{}Ctrl_Grp'.format( elem ))
			self.eyeCtrlGrp.snap(parent)
			mc.parent(self.eyeCtrlGrp ,ctrlGrp)
			mc.parentConstraint(parent , self.eyeCtrlGrp , mo=False)
			mc.scaleConstraint(parent , self.eyeCtrlGrp , mo=False)

		# create eye joint
		self.eyeAdjJnt 		= cn.joint('EyeAdj{}{}Jnt'.format(elem,side) , eyeTmpJnt)
		self.eyeJnt 		= cn.joint('Eye{}{}Jnt'.format(elem,side) 	, eyeTmpJnt)
		self.eyeLidJnt 		= cn.joint('EyeLid{}{}Jnt'.format(elem,side) , eyeTmpJnt)
		mc.parent(self.eyeJnt , self.eyeAdjJnt)
		mc.parent(self.eyeLidJnt , self.eyeJnt)
		mc.parent(self.eyeAdjJnt , parent)


		# create control
		self.eye = cr.Run(  name       = 'Eye{}'.format(elem) ,
							tmpJnt     = self.eyeJnt ,
							side       = sideRbn ,
							shape      = 'sphere' ,
							jnt_ctrl   = 0,
							jnt        = 0,
							gimbal     = 1 ,
							size       = 1 ,
							drv        = 0,
							inv        = 0,
							color      = 'red',
							constraint = 1,
							consType   = ['parent','scale'],
							vis 	   = 1
						  )

		self.eyeCtrl		= self.eye.ctrl
		self.eyeZro			= self.eye.zro
		self.eyeOfst		= self.eye.ofst
		self.eyeGmbl		= self.eye.gmbl

		self.eyeCtrl.scaleShape( size * 0.5 )

		mc.parent(self.eyeZro, self.eyeCtrlGrp)
		aimName = str(self.eyeOfst).split('Ofst')
		self.eyeAim = mc.rename(self.eyeOfst , '{}Aim{}'.format(aimName[0],aimName[1]))

		#-- Rig process Eye
		self.eyeCtrl.addAttr( 'eyelidsFollow' , 0 , 1 )
		self.lidCons = mc.orientConstraint( parent , self.eyeLidJnt , mo = True)
		

		# check main eye target
		self.eyeMainTrgtCtrl 	= str('EyeTrgt{}_Ctrl'.format(elem))
		self.eyeMainTrgtZro		= str('EyeTrgtCtrlZro{}_Grp'.format(elem))

		if not mc.objExists('EyeTrgt{}_Ctrl'.format(elem)):
			self.eyeMainTrg = cr.Run (  name       = 'EyeTrgt{}'.format(elem) ,
										tmpJnt     = eyeMainTrgtTmpJnt ,
										side       = '' ,
										shape      = 'capsule' ,
										jnt_ctrl   = 0,
										jnt        = 0,
										gimbal     = 0 ,
										size       = 1 ,
										drv        = 0,
										inv        = 0,
										color      = 'yellow',
										constraint = 0,
										consType   = ['parent','scale'],
										vis 	   = 1
									  )

			self.eyeMainTrgtCtrl 	= self.eyeMainTrg.ctrl
			self.eyeMainTrgtZro		= self.eyeMainTrg.zro
			self.eyeMainTrgtOfst		= self.eyeMainTrg.ofst

			self.eyeMainTrgtCtrl.scaleShape( size * 0.5 )

			mc.parent(self.eyeMainTrgtCtrl,self.eyeMainTrgtZro)
			mc.parent(self.eyeMainTrgtZro,self.eyeCtrlGrp)
			mc.delete(self.eyeMainTrgtOfst)

			self.eyeMainTrgtZro.lockAttrs( 'tx','ty','tz','rx','ry','rz','sx','sy','sz','v' )

		# eye target	
		self.eyeTrg = cr.Run (  name       = 'EyeTrgt{}'.format(elem) ,
								tmpJnt     = eyeTrgtTmpJnt ,
								side       = sideRbn,
								shape      = 'circle' ,
								jnt_ctrl   = 0,
								jnt        = 0,
								gimbal     = 0 ,
								size       = 1 ,
								drv        = 0,
								inv        = 0,
								color      = clrTrgt,
								constraint = 0,
								consType   = ['parent','scale'],
								vis 	   = 1
							  )

		self.eyeTrgtCtrl 	= self.eyeTrg.ctrl
		self.eyeTrgtZro		= self.eyeTrg.zro
		self.eyeTrgtOfst	= self.eyeTrg.ofst

		self.eyeTrgtCtrl.rotateShape( 90 , 0 , 0 )
		self.eyeTrgtCtrl.scaleShape( size * 0.5 )

		mc.parent(self.eyeTrgtCtrl,self.eyeTrgtZro)
		mc.parent(self.eyeTrgtZro,self.eyeMainTrgtCtrl)
		mc.delete(self.eyeTrgtOfst)

		# rig process
		#rt.localWorld( self.eyeMainTrgtCtrl , ctrlGrp , self.eyeCtrlGrp , self.eyeMainTrgtZro , 'parent' )

		self.eyeJnt.attr('ssc').value = 0
		self.eyeLidJnt.attr('ssc').value = 0

		mc.aimConstraint( self.eyeTrgtCtrl , self.eyeAim , aim = (0,0,1) , u = (0,1,0) , wut = "objectrotation" , wu = (0,1,0) , wuo = self.eyeZro , mo = 1 )[0]
		

		#-- Cleanup
		for obj in (self.eyeCtrlGrp , self.eyeZro, self.eyeTrgtZro):
			obj.lockAttrs( 'tx','ty','tz','rx','ry','rz','sx','sy','sz','v' )

		attrs = ('sx' , 'sy' , 'sz' )
		for attr in attrs :
		    mc.setAttr( '{}.{}'.format( self.eyeTrgtCtrl, attr ) , l = True , k = False )
		    mc.setAttr( '{}.{}'.format( self.eyeMainTrgtCtrl, attr ) , l = True , k = False )

		self.eyeCtrl.lockHideAttrs('v')

		mc.select(cl = True)   
