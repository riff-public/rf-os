import maya.cmds as mc
import re, pickle
import os

import core
reload( core )
import rigTools as rt
reload( rt )
from rf_utils.context import context_info
reload(context_info)
from rf_utils import admin
reload(admin)

cn = core.Node()

def getDataFld():
	context = context_info.ContextPathInfo()
	
	if context.name:
		projectPath = os.environ['RFPROJECT']
		rigProcess = context.process
		
		fileWorkPath = (context.path.name()).replace('$RFPROJECT',projectPath)
		path = fileWorkPath + '/rig/rigData'
		
		admin.makedirs(path)
		return path
		
	filePath = mc.file(q=True , loc=True)
	if not filePath:
		print "Please save file."
		return

	elemPath = filePath.split('/')
	localPath = '/'.join(elemPath[0:-1])
	path = '{}/rigData'.format(localPath)
	if not os.path.isdir(path):
		admin.makedirs(path)

	return path

def writeAttr(*args):
	# get all ctrl from selected Char
	listAll = mc.ls(allPaths=True)
	if listAll:
		listCtrl = []
		listJnt = []
		for des in listAll:
			typ = mc.objectType(des)
			if '_Ctrl' in des:
				if typ == 'transform' or typ == 'joint' or typ == 'nurbsCurve':
					listCtrl.append(des)
			elif '_Jnt' in des:
				if typ == 'joint':
					listJnt.append(des)


		# get attr and value from all ctrl
		dictCtrl = {}
		dictAttr = {}
		listRemove = ['controlPoints.xValue', 'controlPoints.yValue', 'controlPoints.zValue', 'colorSet.clamped', 'colorSet.representation']
		for ctrl in listCtrl:
			listAttrs = mc.listAttr(ctrl, keyable=True, s=True, unlocked=True)
			if listAttrs:
				for rm in listRemove:
					if rm in listAttrs:
						listAttrs.remove(rm)

				for attri in listAttrs:
					attr = '{}.{}'.format(ctrl, attri)
					value = mc.getAttr(attr)

					if ':' in attr:
						attr = attr.split(':')[1] # remove namespace
					dictAttr[attr] = value

				if ':' in ctrl:
					ctrl = ctrl.split(':')[1] # remove namespace

				dictCtrl[ctrl] = dictAttr
				dictAttr = {}

		listRemoveMainAttr = ['translateX','translateY','translateZ','rotateX','rotateY','rotateZ','scaleX','scaleY','scaleZ','visibility']
		for jnt in listJnt:
			listAttrs = mc.listAttr(jnt, keyable=True, s=True, unlocked=True)
			if listAttrs:
				for rm in listRemoveMainAttr:
					if rm in listAttrs:
						listAttrs.remove(rm)

				for attri in listAttrs:
					attr = '{}.{}'.format(jnt, attri)
					value = mc.getAttr(attr)

					if ':' in attr:
						attr = attr.split(':')[1] # remove namespace
					dictAttr[attr] = value

				if ':' in jnt:
					jnt = jnt.split(':')[1] # remove namespace

				dictCtrl[jnt] = dictAttr
				dictAttr = {}

		# write file
		# getPath
		proPath = getDataFld()
		mainPath = os.path.join(proPath, 'attrCtrl.txt')
		bckupPath = os.path.join(proPath, 'backup_attrCtrl')

		with open(mainPath, "w") as file:
			pickle.dump(dictCtrl, file)
			
		# backup file
		admin.makedirs(bckupPath)
		listDir = os.listdir(bckupPath)
		listDir = [i for i in listDir if 'bck' not in i]

		if listDir:
			lastVer = re.findall('\\d+',listDir[-1])
			ver = int(lastVer[0]) + 1
		else:
			ver = 1

		bckupFile = os.path.join(bckupPath, 'attrCtrl_v%03d.txt'%(ver))
		admin.copyfile(mainPath, bckupFile)
		print 'Backup attrCtrl file . . .'
			
		print '>>>>>DONE<<<<<'


def readAttr(*args):
	proPath = getDataFld()
	path = os.path.join(proPath, 'attrCtrl.txt')

	with open(path, "r") as file:
			dataCtrl = pickle.load(file)

	try:
		rigGrp = mc.ls('*:Rig_Grp')[0]
		ns = rigGrp.split(':')[0]

	except:
		rigGrp = mc.ls('Rig_Grp')[0]
		ns = ''

	for ctrl, dataAttr in dataCtrl.iteritems():
		for attri, value in dataAttr.iteritems():
			attr = core.Dag('None')

			if ns:
				attr = core.Dag( '*:{}'.format(attri) )
			else:
				attr = core.Dag( '{}'.format(attri) )
			
			if attr.getExists():
				if not attr.attr().lock:
					try:
						attr.attr().v = value
					except:
						print 'Can not set {}'.format(attr)

	print '>> Copy Attr is Done!'

def readAttrSelected(*args):
	sel = mc.ls(sl=True)
	if sel:
		proPath = getDataFld()
		path = os.path.join(proPath, 'attrCtrl.txt')

		with open(path, "r") as file:
			dataCtrl = pickle.load(file)

		ns = sel[0].split(':')
		if len(ns) > 1:
			ns = ns[0]
		else:
			ns = ''

		for ctrl, dataAttr in dataCtrl.iteritems():
			ctrlCheck = core.Dag('None')

			if ns:
				ctrlCheck = core.Dag( '{}:{}'.format(ns, ctrl) )# ex >> tiny:ALlMover_ctrl
			else:
				ctrlCheck = core.Dag( '{}'.format(ctrl) )
			
			if ctrlCheck.__str__() in sel: # add specify selected ctrl
				for attri, value in dataAttr.iteritems():
					attr = core.Dag('None')

					if ns:
						attr = core.Dag( '{}:{}'.format(ns, attri) )
					else:
						attr = core.Dag( '{}'.format(attri) )

					if attr.getExists():
						mc.setAttr(attr, value)

	print '>> Copy Attr Selected is Done!'