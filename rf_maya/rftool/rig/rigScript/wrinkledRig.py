import maya.cmds as mc
import core
import rigTools as rt
import fkRig
reload(core)
reload(rt)
reload(fkRig)

cn = core.Node()

class Run(object):
	def __init__(self, elem="", size=0.01, *args):

		sel = mc.ls(sl=True, fl=True)
		if sel:
			geoShape = mc.listRelatives(sel[0], ap=True)
			geo = core.Dag(mc.listRelatives(geoShape, ap=True)[0])

			name = geo.getName()
			side = geo.getElemName()['side']
			isSide = geo.getSide()

			## Get Cv curve
			span_crv = 4
			crv_name = mc.polyToCurve(n='{}WnkRig{}Crv'.format(name, side), degree=1, ch=False)[0]
			crv_weight = mc.duplicate(crv_name, n=crv_name.replace("WnkRig", "WeightWnkRig"))[0]
			nurb_sub = mc.getAttr(crv_weight + ".spans")

			mc.xform(crv_name, cp=True)
			mc.rebuildCurve(crv_name, spans=span_crv, degree=3, ch=0)

			## Create Group
			if not mc.objExists("WnkRigCtrl_Grp"):
			    self.ctrlGrp = cn.createNode("transform", "WnkRigCtrl_Grp")
			else: self.ctrlGrp = core.Dag("WnkRigCtrl_Grp")

			if not mc.objExists("WnkRigJnt_Grp"):
			    self.jntGrp = cn.createNode("transform", "WnkRigJnt_Grp")
			else: self.jntGrp = core.Dag("WnkRigJnt_Grp")

			if not mc.objExists("WnkRigStill_Grp"):
			    self.stillGrp = cn.createNode("transform", "WnkRigStill_Grp")
			else: self.stillGrp = core.Dag("WnkRigStill_Grp")

			if not mc.objExists("WnkRigGeo_Grp"):
			    self.geoGrp = cn.createNode("transform", "WnkRigGeo_Grp")
			else: self.geoGrp = core.Dag("WnkRigGeo_Grp")

			self.allCtrlGrp = cn.createNode("transform", "{}{}WnkRigCtrl{}Grp".format(elem, name, side))
			self.allJntGrp = cn.createNode("transform", "{}{}WnkRigJnt{}Grp".format(elem, name, side))
			self.allStillGrp = cn.createNode("transform", "{}{}WnkRigStill{}Grp".format(elem, name, side))
			self.allGeoGrp = cn.createNode("transform", "{}{}WnkRigGeo{}Grp".format(elem, name, side))

			self.allCtrlGrp.parent(self.ctrlGrp)
			self.allJntGrp.parent(self.jntGrp)
			self.allStillGrp.parent(self.stillGrp)
			self.allGeoGrp.parent(self.geoGrp)

			## Create Joint
			jntCv = [0, 3, 6]
			sub = ['Low', 'Mid', 'Hight']
			col = ['yellow', 'red', 'blue']

			self.tmpjnt = []
			for idx, num in enumerate(jntCv):
				jnt = rt.createJointFromSelection('{}.cv[{}]'.format(crv_name, num), name="{}{}{}WnkRig{}TmpJnt".format(elem, name, sub[idx], side))
				self.tmpjnt.append(jnt)

			## Create controller
			self.jnt = []
			self.ctrl = []
			self.gmbl = []
			self.zro = []
			self.ofst = []
			self.pars = []

			for idx, jnt in enumerate(self.tmpjnt):
				ctrl = fkRig.Run(	name 			= '{}{}{}WnkRig'.format(elem, name, sub[idx]) ,
									side			= isSide ,
									tmpJnt			= jnt ,
									parent 			= '' ,
									ctrlGrp 		= 'Ctrl_Grp' , 
									skinGrp 		= '' , 
									shape 			= 'sphere' ,
									jnt_ctrl 		= 0	,
									size 			= size ,
									color 			= col[idx],
									localWorld 		= 0,
									constraint 		= 1,
									dtl 			= 0)


				mc.parent(ctrl.zro, self.allCtrlGrp)
				mc.parent(ctrl.jnt, self.allJntGrp)
				mc.delete(ctrl.ctrlGrp)
				del(ctrl.ctrlGrp)

				self.jnt.append(ctrl.jnt)
				self.zro.append(ctrl.zro)
				self.ofst.append(ctrl.ofst)
				self.pars.append(ctrl.pars)
				self.ctrl.append(ctrl.ctrl)
				self.gmbl.append(ctrl.gmbl)

			## create main controller
			ctrl = fkRig.Run(	name 			= '{}{}{}AllMoverWnkRig'.format(elem, name, sub[idx]) ,
								side			= isSide ,
								tmpJnt			= self.jnt[1] ,
								parent 			= '' ,
								ctrlGrp 		= 'Ctrl_Grp' , 
								skinGrp 		= '' , 
								shape 			= 'cube' ,
								jnt_ctrl 		= 0	,
								size 			= size*2 ,
								color 			= 'red',
								localWorld 		= 0,
								constraint 		= 0,
								dtl 			= 0)

			mc.parent(ctrl.zro, self.allCtrlGrp)
			mc.parent(ctrl.jnt, self.allJntGrp)
			mc.delete(ctrl.ctrlGrp)
			del(ctrl.ctrlGrp)

			self.amvZro = ctrl.zro
			self.amvOfst = ctrl.ofst
			self.amvPars = ctrl.pars
			self.amvCtrl = ctrl.ctrl
			self.amvCtrl = ctrl.gmbl

			## calculate axis
			lowWs = mc.xform(self.ctrl[0], q=1, ws=1, t=1)
			hightWs = mc.xform(self.ctrl[2], q=1, ws=1, t=1)

			get_max = []
			for L, H in zip(lowWs, hightWs):
			    get_max.append(H - L)
				
			if max(get_max) == get_max[0]:
			    aim = (1, 0, 0)
			    up = (0, 1, 0)
			    wu = (0, 1, 0)
			elif max(get_max) == get_max[1]:
			    aim = (0, 1, 0)
			    up = (0, 0, 1)
			    wu = (0, 0, 1)
			elif max(get_max) == get_max[2]:
			    aim = (0, 0, 1)
			    up = (1, 0, 0)
			    wu = (1, 0, 0)

			## constraint object
			parCon = core.Dag(mc.parentConstraint(self.gmbl[0], self.gmbl[-1], self.pars[1], sr=("x", "y", "z"), mo=True)[0])
			parCon.attr("int").v = 2
			aimCon = core.Dag(mc.aimConstraint(self.gmbl[-1], self.pars[1], aim=aim, u=up, wut="vector", wu=wu, mo=True)[0])

			lowCon = core.Dag(mc.parentConstraint(self.amvCtrl, self.ofst[0], mo=True)[0])
			midCon = core.Dag(mc.parentConstraint(self.amvCtrl, self.ofst[1], mo=True)[0])
			higthCon = core.Dag(mc.parentConstraint(self.amvCtrl, self.ofst[2], mo=True)[0])
			allParCon = [lowCon, midCon, higthCon]

			## loft and skin
			crvA_loft = mc.offsetCurve(crv_weight, d=0.001, ugn=False, ch=False)
			crvB_loft = mc.offsetCurve(crv_weight, d=-0.001, ugn=False, ch=False)

			loft_name = mc.loft(crvA_loft, crvB_loft, d=1, rsn=True, ch=False)[0]
			loft_name = mc.rename(loft_name, crv_name.replace("_Crv", "_Nrb"))

			skin_name = rt.skinRbnNrb(nurbs=loft_name, jntLs=[self.jnt[0], self.jnt[1], self.jnt[2]], weightStyle=4, nurbSub=nurb_sub)
			mc.skinCluster(self.jnt, geo, tsb=True)[0]
			mc.select(loft_name + ".cv[:]", geo.__str__() + ".vtx[:]")
			mc.copySkinWeights(nm=True, sa="closestPoint", ia="closestJoint")

			mc.delete(crv_name, crv_weight, crvA_loft, crvB_loft, loft_name)

			## tan create Follicle
			self.get_nrb = []
			self.get_fol = []
			get_folCon = []

			for jnt, zro in zip(self.jnt, self.zro):
				nrb = core.Dag(mc.nurbsPlane(n=jnt.__str__().replace("_Jnt", "_Nrb"), w=0.005, ax=(0,0,1), ch=False)[0])
				nrb.snap(jnt)

				fol = rt.createFollicle(nrb, name=jnt.__str__().replace("_Jnt", "_Fol"), side=isSide, pru=0.5, prv=0.5)
				folCon = core.Dag(mc.parentConstraint(fol, zro, mo=True)[0])

				self.get_nrb.append(nrb)
				self.get_fol.append(fol)
				get_folCon.append(folCon)

			allCon = mc.parentConstraint(self.get_fol[1], self.amvZro, mo=True)[0]

			## create attributes
			self.ctrl[1].addAttr("wrinkledVis", 0, 1, True)
			self.ctrl[1].addAttr("midFollow", 0, 1, True)
			self.ctrl[1].addAttr("allmoverFollow", 0, 1, True)
			self.ctrl[1].attr("midFollow").v = 1
			
			self.ctrl[1].attr("wrinkledVis") >> self.zro[0].attr("v")
			self.ctrl[1].attr("wrinkledVis") >> self.zro[2].attr("v")

			self.ctrl[1].attr("midFollow") >> parCon.attr("{}W0".format(self.gmbl[0]))
			self.ctrl[1].attr("midFollow") >> parCon.attr("{}W1".format(self.gmbl[-1]))
			self.ctrl[1].attr("midFollow") >> aimCon.attr("{}W0".format(self.gmbl[-1]))

			rev = cn.createNode("reverse", '{}{}WnkRig{}Rev'.format(elem, name, side))
			self.ctrl[1].attr("allmoverFollow") >> rev.attr("ix")
			for fol, fcnst, acnst  in zip(self.get_fol, get_folCon, allParCon):
				rev.attr("ox") >> fcnst.attr("{}W0".format(fol))
				self.ctrl[1].attr("allmoverFollow") >> acnst.attr("{}W0".format(self.amvCtrl))
			self.ctrl[1].attr("allmoverFollow") >> self.amvZro.attr("v")

			## clean
			self.geo = core.Dag(geo.rename("{}{}{}Geo".format(name, "WnkRig", side)))
			self.geo.parent(self.allGeoGrp)

			mc.parent(self.get_nrb, self.allStillGrp)
			mc.parent(self.get_fol, self.allStillGrp)
			
			self.jntGrp.attr("v").v = 0
			self.stillGrp.attr("v").v = 0

			mc.delete(self.tmpjnt)