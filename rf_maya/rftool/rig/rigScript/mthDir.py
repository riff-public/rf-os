import maya.cmds as mc
import pymel.core as pm
import core
reload(core)
import rigTools as rt
reload(rt)
import fkRig
reload(fkRig)
import fkChain
reload(fkChain)

cn = core.Node()

class Run(object):
    def __init__(self , lipUprCenTmpJnt         = '' ,
                        lipLwrCenTmpJnt         = '' ,
                        lipCnrLTmpJnt           = '' ,
                        lipCnrRTmpJnt           = '' ,
                        lipUprLTmpJnt           = [] ,
                        lipUprRTmpJnt           = [] ,
                        lipLwrLTmpJnt           = [] ,
                        lipLwrRTmpJnt           = [] ,

                        cheekMainLTmpJnt        = '' , # puff
                        cheekUprLTmpJnt         = [] , # under eyelid
                        cheekDetailLTmpJnt      = [] , # beside lip corner
                        cheekMainRTmpJnt        = '' ,
                        cheekUprRTmpJnt         = [] ,
                        cheekDetailRTmpJnt      = [] ,

                        jawUprTmpJnt            = [] ,
                        jawLwrTmpJnt            = [] ,
                        tongueTmpJnt            = [] ,
                        teethUprMainTmpJnt      = '' ,
                        teethLwrMainTmpJnt      = '' ,
                        teethUprTmpJnt          = [] ,
                        teethLwrTmpJnt          = [] ,

                        noseMainTmpJnt          = [] ,
                        noseLTmpJnt             = [] ,
                        noseRTmpJnt             = [] ,

                        nrb                     = '' ,
                        parent                  = '' , # head
                        elem                    = '' ,
                        eyeDirOfstL             = ['EyeLidLwrOfst_L_Grp'] ,
                        eyeDirOfstR             = ['EyeLidLwrOfst_R_Grp'] ,
                        ctrlGrp                 = 'MainCtrl_Grp',
                        stillGrp                = 'Still_Grp',
                        size                    = 1 
                        ):

        # info
        elem = elem.capitalize()
        side = ''
        self.side = '_'

        #-- create main group
        self.mthCtrlGrp = cn.createNode('transform' , 'MthRig{}Ctrl{}Grp'.format(elem , self.side))
        mc.parentConstraint(parent , self.mthCtrlGrp , mo=False)
        mc.scaleConstraint(parent , self.mthCtrlGrp , mo=False)

        # self.lipsCtrlGrp = cn.createNode('transform' , 'Lips{}{}Ctrl_Grp'.format(elem , self.side))
        self.teethCtrlGrp = cn.createNode('transform' , 'Teeth{}Ctrl{}Grp'.format(elem , self.side))
        self.cheekCtrlGrp = cn.createNode('transform' , 'Cheek{}Ctrl{}Grp'.format(elem , self.side))
        self.noseCtrlGrp = cn.createNode('transform' , 'Nose{}Ctrl{}Grp'.format(elem , self.side))

        self.teethCtrlGrp.parent(self.mthCtrlGrp)
        self.cheekCtrlGrp.parent(self.mthCtrlGrp)
        self.noseCtrlGrp.parent(self.mthCtrlGrp)

        self.mthStillGrp = cn.createNode('transform', 'MthStill{}{}Grp'.format(elem , self.side))
        self.mthNormGrp = cn.createNode('transform', 'MthNorm{}{}Grp'.format(elem , self.side))
        self.mthNormGrp.parent(self.mthStillGrp)


        #-- Jaw Rig
        print '# Generate Jaw Lwr Rig'
        jawLwrRig = fkChain.Run( name       = 'JawLwr{}'.format(elem) ,
                                 side       = side ,
                                 tmpJnt     = jawLwrTmpJnt ,
                                 parent     = parent ,
                                 ctrlGrp    = self.mthCtrlGrp , 
                                 skinGrp    = parent ,
                                 shape      = 'square' ,
                                 size       = size ,
                                 no_tip     = True,
                                 color      = 'yellow',
                                 localWorld = 1 )

        print '# Generate Jaw Upr Rig'
        jawUprRig = fkChain.Run( name       = 'JawUpr{}'.format(elem) ,
                                 side       = side ,
                                 tmpJnt     = jawUprTmpJnt ,
                                 parent     = parent ,
                                 ctrlGrp    = self.mthCtrlGrp , 
                                 skinGrp    = parent ,
                                 shape      = 'square' ,
                                 size       = size ,
                                 no_tip     = True,
                                 color      = 'yellow',
                                 localWorld = 1 )


        #-- Tongue Rig
        if tongueTmpJnt:
            print '# Generate Tongue Rig'
            tongueRig = fkChain.Run(     name       = 'Tongue{}'.format(elem) ,
                                         side       = side ,
                                         tmpJnt     = tongueTmpJnt ,
                                         parent     = jawLwrRig.jointChain[-1] ,
                                         ctrlGrp    = self.mthCtrlGrp , 
                                         skinGrp    = parent ,
                                         shape      = 'circle' ,
                                         size       = size ,
                                         no_tip     = False,
                                         color      = 'red',
                                         localWorld = 1 )


        #-- Teeth Rig
        if teethUprTmpJnt:
            print '# Generate Teeth Upr Rig'
            ttUprRig = fkRig.Run(   name           = 'TeethUprMain{}'.format(elem) ,
                                     side           = side ,
                                     tmpJnt         = teethUprMainTmpJnt ,
                                     parent         = jawUprRig.jointChain[-1] ,
                                     ctrlGrp        = self.teethCtrlGrp , 
                                     skinGrp        = jawUprRig.jointChain[-1] , 
                                     shape          = 'circle' ,
                                     jnt_ctrl       = 0 ,
                                     size           = size ,
                                     color          = 'yellow')

            for i in range(len(teethUprTmpJnt)):
                ttRig = fkRig.Run(   name           = 'TeethUpr{}{}'.format(elem , i+1) ,
                                     side           = side ,
                                     tmpJnt         = teethUprTmpJnt[i] ,
                                     parent         = ttUprRig.jnt ,
                                     ctrlGrp        = self.teethCtrlGrp , 
                                     skinGrp        = ttUprRig.jnt , 
                                     shape          = 'cube' ,
                                     jnt_ctrl       = 0 ,
                                     size           = size ,
                                     color          = 'cyan')

        if teethLwrTmpJnt:
            print '# Generate Teeth Lwr Rig'
            ttLwrRig = fkRig.Run(    name           = 'TeethLwrMain{}'.format(elem) ,
                                     side           = side ,
                                     tmpJnt         = teethLwrMainTmpJnt ,
                                     parent         = jawLwrRig.jointChain[-1] ,
                                     ctrlGrp        = self.teethCtrlGrp , 
                                     skinGrp        = jawLwrRig.jointChain[-1] , 
                                     shape          = 'circle' ,
                                     jnt_ctrl       = 0 ,
                                     size           = size ,
                                     color          = 'yellow')

            for i in range(len(teethLwrTmpJnt)):
                ttRig = fkRig.Run(   name           = 'TeethLwr{}{}'.format(elem , i+1) ,
                                     side           = side ,
                                     tmpJnt         = teethLwrTmpJnt[i] ,
                                     parent         = ttLwrRig.jnt ,
                                     ctrlGrp        = self.teethCtrlGrp , 
                                     skinGrp        = ttLwrRig.jnt , 
                                     shape          = 'cube' ,
                                     jnt_ctrl       = 0 ,
                                     size           = size ,
                                     color          = 'pink')


        #-- Cheek rig
        if cheekMainLTmpJnt:
            print '# Generate Cheek Rig'
            cheekMainLRig = fkRig.Run(name           = 'CheekMain{}'.format(elem) ,
                                     side           = 'L' ,
                                     tmpJnt         = cheekMainLTmpJnt ,
                                     parent         = parent ,
                                     ctrlGrp        = self.cheekCtrlGrp , 
                                     skinGrp        = parent , 
                                     shape          = 'cube' ,
                                     jnt_ctrl       = 0 ,
                                     size           = size ,
                                     color          = 'green')

            cheekMainRRig = fkRig.Run(name           = 'CheekMain{}'.format(elem) ,
                                     side           = 'R' ,
                                     tmpJnt         = cheekMainRTmpJnt ,
                                     parent         = parent ,
                                     ctrlGrp        = self.cheekCtrlGrp , 
                                     skinGrp        = parent , 
                                     shape          = 'cube' ,
                                     jnt_ctrl       = 0 ,
                                     size           = size ,
                                     color          = 'green')

            cheekUprLArray = []
            cheekUprRArray = []
            cheekDtlLArray = []
            cheekDtlRArray = []

            for i in range(len(cheekUprLTmpJnt)):
                print '# Generate Cheek Upr Left Rig'
                cheekUprLRig = fkRig.Run(name           = 'CheekUpr{}{}'.format(elem , i+1) ,
                                         side           = 'L' ,
                                         tmpJnt         = cheekUprLTmpJnt[i] ,
                                         parent         = parent ,
                                         ctrlGrp        = self.cheekCtrlGrp , 
                                         skinGrp        = parent , 
                                         shape          = 'cube' ,
                                         jnt_ctrl       = 0 ,
                                         size           = size ,
                                         color          = 'green',
                                         localWorld     = 0 )

                print '# Generate Cheek Upr Right Rig'
                cheekUprRRig = fkRig.Run(name           = 'CheekUpr{}{}'.format(elem , i+1) ,
                                         side           = 'R' ,
                                         tmpJnt         = cheekUprRTmpJnt[i] ,
                                         parent         = parent ,
                                         ctrlGrp        = self.cheekCtrlGrp , 
                                         skinGrp        = parent , 
                                         shape          = 'cube' ,
                                         jnt_ctrl       = 0 ,
                                         size           = size ,
                                         color          = 'green',
                                         localWorld     = 0 )
                cheekUprLArray.append(cheekUprLRig)
                cheekUprRArray.append(cheekUprRRig)

            for i in range(len(cheekDetailLTmpJnt)):
                print '# Generate Cheek Detail Left Rig'
                cheekDetailLRig = fkRig.Run(name        = 'CheekDetail{}{}'.format(elem , i+1) ,
                                         side           = 'L' ,
                                         tmpJnt         = cheekDetailLTmpJnt[i] ,
                                         parent         = cheekMainLRig.jnt ,
                                         ctrlGrp        = self.cheekCtrlGrp , 
                                         skinGrp        = cheekMainLRig.jnt , 
                                         shape          = 'cube' ,
                                         jnt_ctrl       = 0 ,
                                         size           = size ,
                                         color          = 'green',
                                         localWorld     = 0 )

                print '# Generate Cheek Detail Right Rig'
                cheekDetailRRig = fkRig.Run(name        = 'CheekDetail{}{}'.format(elem , i+1) ,
                                         side           = 'R' ,
                                         tmpJnt         = cheekDetailRTmpJnt[i] ,
                                         parent         = cheekMainRRig.jnt ,
                                         ctrlGrp        = self.cheekCtrlGrp , 
                                         skinGrp        = cheekMainRRig.jnt , 
                                         shape          = 'cube' ,
                                         jnt_ctrl       = 0 ,
                                         size           = size ,
                                         color          = 'green',
                                         localWorld     = 0 )

                cheekDetailLLocWor = rt.localWorld( obj = cheekDetailLRig.ctrl , worldObj = parent , localObj = cheekMainLRig.jnt , consGrp = cheekDetailLRig.ofst , type = 'parent' )
                cheekDetailRLocWor = rt.localWorld( obj = cheekDetailRRig.ctrl , worldObj = parent , localObj = cheekMainRRig.jnt , consGrp = cheekDetailRRig.ofst , type = 'parent' )

                cheekDtlLArray.append(cheekDetailLRig)
                cheekDtlRArray.append(cheekDetailRRig)


        #-- Nose rig
        if noseMainTmpJnt:
            print '# Generate Nose Rig'
            noseMainRig = fkRig.Run(name           = 'NoseMain{}'.format(elem) ,
                                     side           = '' ,
                                     tmpJnt         = noseMainTmpJnt ,
                                     parent         = parent ,
                                     ctrlGrp        = self.noseCtrlGrp , 
                                     skinGrp        = parent , 
                                     shape          = 'cube' ,
                                     jnt_ctrl       = 0 ,
                                     size           = size ,
                                     color          = 'red',
                                     localWorld     = 0 )
            noseLArray = []
            noseRArray = []

            for i in range(len(noseLTmpJnt)):
                nameNoseL = fkRig.Run(name           = 'Nose{}{}'.format(elem , i+1) ,
                                     side           = 'L' ,
                                     tmpJnt         = noseLTmpJnt ,
                                     parent         = noseMainRig.jnt ,
                                     ctrlGrp        = self.noseCtrlGrp , 
                                     skinGrp        = noseMainRig.jnt , 
                                     shape          = 'cube' ,
                                     jnt_ctrl       = 0 ,
                                     size           = size ,
                                     color          = 'red',
                                     localWorld     = 0 )
                noseLArray.append(nameNoseL)

            for eachNose in range(len(noseRTmpJnt)):
                nameNoseR = fkRig.Run(name           = 'Nose{}{}'.format(elem , i+1) ,
                                     side           = 'R' ,
                                     tmpJnt         = noseRTmpJnt ,
                                     parent         = noseMainRig.jnt ,
                                     ctrlGrp        = self.noseCtrlGrp , 
                                     skinGrp        = noseMainRig.jnt , 
                                     shape          = 'cube' ,
                                     jnt_ctrl       = 0 ,
                                     size           = size ,
                                     color          = 'red',
                                     localWorld     = 0 )
                noseRArray.append(nameNoseR)


        #-- Mouth Rig
        #-- create main lips
        lipMainUprJnt = cn.joint('LipMainUpr{}{}Jnt'.format(elem , self.side) , lipUprCenTmpJnt)
        lipMainLwrJnt = cn.joint('LipMainLwr{}{}Jnt'.format(elem , self.side) , lipLwrCenTmpJnt)
        lipMainUprJnt.parent(parent)
        lipMainLwrJnt.parent(parent)

        normGrpArray = []

        #-- create main mouth control
        print '# Generate Mouth Rig'
        mthJnt = cn.joint('Mouth{}{}TmpJnt'.format(elem , self.side) , parent)

        mthRig = fkRig.Run(  name       = 'Mouth{}'.format(elem) ,
                             side       = side ,
                             tmpJnt     = mthJnt ,
                             parent     = parent ,
                             ctrlGrp    = self.mthCtrlGrp , 
                             skinGrp     = parent , 
                             shape      = 'circle' ,
                             jnt_ctrl   = 0 ,
                             size       = size ,
                             color      = 'yellow',
                             localWorld = 0 )
        mthRig.jnt.attr('drawStyle').v = 2

        mc.delete(mthJnt)

        #-- create center upper
        lipUprMainRig = fkRig.Run(  name       = 'LipCenUprMain{}'.format(elem) ,
                                side       = side ,
                                tmpJnt     = lipUprCenTmpJnt ,
                                parent     = '' ,
                                ctrlGrp    = jawUprRig.gmblArray[-1] , 
                                skinGrp    = parent , 
                                shape      = 'circle' ,
                                jnt_ctrl   = 0 ,
                                size       = size ,
                                color      = 'red',
                                localWorld = 0 )

        mc.delete(lipUprMainRig.jnt,constraints=True)
        ponLipUprMain = self.createNormOnNrb(   name         = 'LipCenUprMain{}'.format(elem) ,
                                            side         = '' , 
                                            position     = lipUprCenTmpJnt ,
                                            parent       = lipUprMainRig.gmbl ,
                                            child        = lipUprMainRig.jnt ,
                                            nrb          = nrb )
        normGrpArray.append(ponLipUprMain)

        lipUprRig = fkRig.Run(  name       = 'LipCenUpr{}'.format(elem) ,
                                side       = side ,
                                tmpJnt     = lipUprCenTmpJnt ,
                                parent     = '' ,
                                ctrlGrp    = lipUprMainRig.gmbl , 
                                skinGrp     = parent , 
                                shape      = 'circle' ,
                                jnt_ctrl   = 0 ,
                                size       = size ,
                                color      = 'cyan',
                                localWorld = 0 )

        ponLipUpr = self.createNormOnNrb(   name         = 'LipCenUpr{}'.format(elem) ,
                                            side         = '' , 
                                            position     = lipUprCenTmpJnt ,
                                            parent       = lipUprRig.gmbl ,
                                            child        = lipUprRig.jnt ,
                                            nrb          = nrb )
        normGrpArray.append(ponLipUpr)

        #-- create center lower
        lipLwrMainRig = fkRig.Run(  name       = 'LipCenLwrMain{}'.format(elem) ,
                                side       = side ,
                                tmpJnt     = lipLwrCenTmpJnt ,
                                parent     = '' ,
                                ctrlGrp    = jawLwrRig.gmblArray[-1], 
                                skinGrp     = parent , 
                                shape      = 'circle' ,
                                jnt_ctrl   = 0 ,
                                size       = size ,
                                color      = 'red',
                                localWorld = 0 )

        mc.delete(lipLwrMainRig.jnt,constraints=True)
        ponLipLwrMain = self.createNormOnNrb(   name         = 'LipCenLwrMain{}'.format(elem) ,
                                            side         = '' , 
                                            position     = lipLwrCenTmpJnt ,
                                            parent       = lipLwrMainRig.gmbl ,
                                            child        = lipLwrMainRig.jnt ,
                                            nrb          = nrb )
        normGrpArray.append(ponLipLwrMain)

        lipLwrRig = fkRig.Run(  name       = 'LipCenLwr{}'.format(elem) ,
                                side       = side ,
                                tmpJnt     = lipLwrCenTmpJnt ,
                                parent     = '' ,
                                ctrlGrp    = lipLwrMainRig.gmbl, 
                                skinGrp     = parent , 
                                shape      = 'circle' ,
                                jnt_ctrl   = 0 ,
                                size       = size ,
                                color      = 'cyan',
                                localWorld = 0 )

        mc.delete(lipLwrRig.jnt,constraints=True)
        ponLipLwr = self.createNormOnNrb(   name         = 'LipCenLwr{}'.format(elem) ,
                                            side         = '' , 
                                            position     = lipLwrCenTmpJnt ,
                                            parent       = lipLwrRig.gmbl ,
                                            child        = lipLwrRig.jnt ,
                                            nrb          = nrb )
        normGrpArray.append(ponLipLwr)

        lipCenUprLocWor = rt.localWorld( obj = lipUprRig.ctrl , worldObj = jawUprRig.jointChain[-1]  , localObj = lipLwrMainRig.gmbl , consGrp = lipUprRig.zro , type = 'parent' )
        lipCenLwrLocWor = rt.localWorld( obj = lipLwrRig.ctrl , worldObj = jawLwrRig.jointChain[-1]  , localObj = lipUprMainRig.gmbl , consGrp = lipLwrRig.zro , type = 'parent' )
        lipUprRig.ctrl.attr("localWorld").v = 1
        lipLwrRig.ctrl.attr("localWorld").v = 1

        #-- create corner Left
        lipCnrMainLRig = fkRig.Run(  name       = 'LipCnrMain{}'.format(elem) ,
                                side       = 'L' ,
                                tmpJnt     = lipCnrLTmpJnt ,
                                parent     = '' ,
                                ctrlGrp    = self.mthCtrlGrp , 
                                skinGrp     = parent , 
                                shape      = 'circle' ,
                                jnt_ctrl   = 0 ,
                                size       = size ,
                                color      = 'red',
                                localWorld = 0 )

        mc.delete(lipCnrMainLRig.jnt,constraints=True)
        ponLipCnrMainL = self.createNormOnNrb(   name         = 'LipCnrMain{}'.format(elem) ,
                                            side         = 'L' , 
                                            position     = lipCnrLTmpJnt ,
                                            parent       = lipCnrMainLRig.gmbl ,
                                            child        = lipCnrMainLRig.jnt ,
                                            nrb          = nrb )
        normGrpArray.append(ponLipCnrMainL)
        #-- create corner Right
        lipCnrMainRRig = fkRig.Run(  name       = 'LipCnrMain{}'.format(elem) ,
                                side       = 'R' ,
                                tmpJnt     = lipCnrRTmpJnt ,
                                parent     = '' ,
                                ctrlGrp    = self.mthCtrlGrp , 
                                skinGrp     = parent , 
                                shape      = 'circle' ,
                                jnt_ctrl   = 0 ,
                                size       = size ,
                                color      = 'red',
                                localWorld = 0 )

        mc.delete(lipCnrMainRRig.jnt,constraints=True)
        ponLipCnrMainR = self.createNormOnNrb(   name         = 'LipCnrMain{}'.format(elem) ,
                                            side         = 'R' , 
                                            position     = lipCnrRTmpJnt ,
                                            parent       = lipCnrMainRRig.gmbl ,
                                            child        = lipCnrMainRRig.jnt ,
                                            nrb          = nrb )
        normGrpArray.append(ponLipCnrMainR)

        #-- Corner Follow Jaw
        lipCnrMainLLocWor = rt.localWorld( obj = lipCnrMainLRig.ctrl , worldObj = jawUprRig.jointChain[-1]  , localObj = jawLwrRig.jointChain[-1] , consGrp = lipCnrMainLRig.zro , type = 'parent' )
        lipCnrMainRLocWor = rt.localWorld( obj = lipCnrMainRRig.ctrl , worldObj = jawUprRig.jointChain[-1]  , localObj = jawLwrRig.jointChain[-1] , consGrp = lipCnrMainRRig.zro , type = 'parent' )
        lipCnrMainLRig.ctrl.attr('localWorld').v = 0.7
        lipCnrMainRRig.ctrl.attr('localWorld').v = 0.7


        #-- follow jaw
        headPosGrp = cn.createNode('transform' , 'HeadPos{}_Grp'.format(elem))
        mc.parentConstraint(parent , headPosGrp , mo=False)
        mc.scaleConstraint(parent , headPosGrp , mo=False)

        lipUprFollowGrp = cn.createNode('transform' , 'LipUprFollowJaw{}Pos_Grp'.format(elem))
        lipUprFollowGrp.snap(lipUprCenTmpJnt)
        lipUprFollowZro = rt.addGrp(lipUprFollowGrp , 'Zro')
        lipUprFollowZro.parent(headPosGrp)
        mc.parentConstraint(mthRig.gmbl , lipUprFollowGrp , mo=True)
        mc.scaleConstraint(mthRig.gmbl , lipUprFollowGrp , mo=True)
        rt.connectTRS(lipUprFollowGrp , lipUprMainRig.ofst)

        lipLwrFollowGrp = cn.createNode('transform' , 'LipLwrFollowJaw{}Pos_Grp'.format(elem))
        lipLwrFollowGrp.snap(lipLwrCenTmpJnt)
        lipLwrFollowZro = rt.addGrp(lipLwrFollowGrp , 'Zro')
        lipLwrFollowZro.parent(headPosGrp)
        mc.parentConstraint(mthRig.gmbl , lipLwrFollowGrp , mo=True)
        mc.scaleConstraint(mthRig.gmbl , lipLwrFollowGrp , mo=True)
        rt.connectTRS(lipLwrFollowGrp , lipLwrMainRig.ofst)

        lipCnrLFollowGrp = cn.createNode('transform' , 'LipCnrFollowJaw{}Pos_L_Grp'.format(elem))
        lipCnrLFollowGrp.snap(lipCnrLTmpJnt)
        lipCnrLFollowZro = rt.addGrp(lipCnrLFollowGrp , 'Zro')
        lipCnrLFollowZro.parent(headPosGrp)
        mc.parentConstraint(mthRig.gmbl , lipCnrLFollowGrp , mo=True)
        mc.scaleConstraint(mthRig.gmbl , lipCnrLFollowGrp , mo=True)
        rt.connectTRS(lipCnrLFollowGrp , lipCnrMainLRig.ofst)

        lipCnrRFollowGrp = cn.createNode('transform' , 'LipCnrFollowJaw{}Pos_R_Grp'.format(elem))
        lipCnrRFollowGrp.snap(lipCnrRTmpJnt)
        lipCnrRFollowZro = rt.addGrp(lipCnrRFollowGrp , 'Zro')
        lipCnrRFollowZro.parent(headPosGrp)
        mc.parentConstraint(mthRig.gmbl , lipCnrRFollowGrp , mo=True)
        mc.scaleConstraint(mthRig.gmbl , lipCnrRFollowGrp , mo=True)
        rt.connectTRS(lipCnrRFollowGrp , lipCnrMainRRig.ofst)

        for attr in 'trs':
            lipUprCenFollowPma = cn.createNode('plusMinusAverage' , 'LipCenUprFollow{}{}_Pma'.format(elem , attr.upper()) )
            lipLwrCenFollowPma = cn.createNode('plusMinusAverage' , 'LipCenLwrFollow{}{}_Pma'.format(elem , attr.upper()) )

            core.Dag(mthRig.ctrl).attr(attr) >> lipUprCenFollowPma.attr("i3[0]")
            core.Dag(lipUprMainRig.ctrl).attr(attr) >> lipUprCenFollowPma.attr("i3[1]")
            lipUprCenFollowPma.attr("o3") >> core.Dag(lipUprRig.ofst).attr(attr)

            core.Dag(mthRig.ctrl).attr(attr) >> lipLwrCenFollowPma.attr("i3[0]")
            core.Dag(lipLwrMainRig.ctrl).attr(attr) >> lipLwrCenFollowPma.attr("i3[1]")
            lipLwrCenFollowPma.attr("o3") >> core.Dag(lipLwrRig.ofst).attr(attr)

            if attr == 's':
                lipUprCenFollowPma.addAttr('value')
                lipLwrCenFollowPma.addAttr('value')

                lipUprCenFollowPma.attr('value').v = -1
                lipLwrCenFollowPma.attr('value').v = -1

                for ax in 'xyz':
                    lipUprCenFollowPma.attr("value") >> lipUprCenFollowPma.attr("i3[2].i3{}".format(ax))
                    lipLwrCenFollowPma.attr("value") >> lipLwrCenFollowPma.attr("i3[2].i3{}".format(ax))

        headPosGrp.parent(self.mthNormGrp)

        #-- calculate number ctrl
        num = len(lipUprLTmpJnt)
        cnrNum = int((num+1)/2) # for rotateZ when rotate corner lip, each grp will have amp to set

        lipUprLArray = []
        lipUprRArray = []
        lipLwrLArray = []
        lipLwrRArray = []
        lipUprLLocWorArray = []
        lipUprRLocWorArray = []
        lipLwrLLocWorArray = []
        lipLwrRLocWorArray = []


        for i in range(num):
            lipUprLRig = fkRig.Run( name       = 'LipUpr{}{}'.format(elem , i+1) ,
                                    side       = 'L' ,
                                    tmpJnt     = lipUprLTmpJnt[i] ,
                                    parent     = '' ,
                                    ctrlGrp    = mthRig.gmbl , 
                                    skinGrp     = parent , 
                                    shape      = 'circle' ,
                                    jnt_ctrl   = 0 ,
                                    size       = size ,
                                    color      = 'cyan',
                                    localWorld = 0 )

            mc.delete(lipUprLRig.jnt,constraints=True)
            ponlipUprL = self.createNormOnNrb(   name         = 'LipUpr{}{}'.format(elem , i+1) ,
                                                side         = 'L' , 
                                                position     = lipUprLTmpJnt[i] ,
                                                parent       = lipUprLRig.gmbl ,
                                                child        = lipUprLRig.jnt ,
                                                nrb          = nrb )
            normGrpArray.append(ponlipUprL)
            lipUprLArray.append(lipUprLRig)

            lipUprRRig = fkRig.Run( name       = 'LipUpr{}{}'.format(elem , i+1) ,
                                    side       = 'R' ,
                                    tmpJnt     = lipUprRTmpJnt[i] ,
                                    parent     = '' ,
                                    ctrlGrp    = mthRig.gmbl , 
                                    skinGrp     = parent , 
                                    shape      = 'circle' ,
                                    jnt_ctrl   = 0 ,
                                    size       = size ,
                                    color      = 'cyan',
                                    localWorld = 0 )

            mc.delete(lipUprRRig.jnt,constraints=True)
            ponlipUprR = self.createNormOnNrb(   name         = 'LipUpr{}{}'.format(elem , i+1) ,
                                                side         = 'R' , 
                                                position     = lipUprRTmpJnt[i] ,
                                                parent       = lipUprRRig.gmbl ,
                                                child        = lipUprRRig.jnt ,
                                                nrb          = nrb )
            normGrpArray.append(ponlipUprR)
            lipUprRArray.append(lipUprRRig)

            lipLwrLRig = fkRig.Run( name       = 'LipLwr{}{}'.format(elem , i+1) ,
                                    side       = 'L' ,
                                    tmpJnt     = lipLwrLTmpJnt[i] ,
                                    parent     = '' ,
                                    ctrlGrp    = mthRig.gmbl , 
                                    skinGrp     = parent , 
                                    shape      = 'circle' ,
                                    jnt_ctrl   = 0 ,
                                    size       = size ,
                                    color      = 'cyan',
                                    localWorld = 0 )

            mc.delete(lipLwrLRig.jnt,constraints=True)
            ponlipLwrL = self.createNormOnNrb(   name         = 'LipLwr{}{}'.format(elem , i+1) ,
                                                side         = 'L' , 
                                                position     = lipLwrLTmpJnt[i] ,
                                                parent       = lipLwrLRig.gmbl ,
                                                child        = lipLwrLRig.jnt ,
                                                nrb          = nrb )
            normGrpArray.append(ponlipLwrL)
            lipLwrLArray.append(lipLwrLRig)

            lipLwrRRig = fkRig.Run( name       = 'LipLwr{}{}'.format(elem , i+1) ,
                                    side       = 'R' ,
                                    tmpJnt     = lipLwrRTmpJnt[i] ,
                                    parent     = '' ,
                                    ctrlGrp    = mthRig.gmbl , 
                                    skinGrp     = parent , 
                                    shape      = 'circle' ,
                                    jnt_ctrl   = 0 ,
                                    size       = size ,
                                    color      = 'cyan',
                                    localWorld = 0 )

            mc.delete(lipLwrRRig.jnt,constraints=True)
            ponlipLwrR = self.createNormOnNrb(   name         = 'LipLwr{}{}'.format(elem , i+1) ,
                                                side         = 'R' , 
                                                position     = lipLwrRTmpJnt[i] ,
                                                parent       = lipLwrRRig.gmbl ,
                                                child        = lipLwrRRig.jnt ,
                                                nrb          = nrb )
            normGrpArray.append(ponlipLwrR)
            lipLwrRArray.append(lipLwrRRig)

            #-- create localWorld
            lipUprLocWor = rt.localWorld( obj = lipUprLRig.ctrl , worldObj = lipUprMainRig.gmbl , localObj = lipCnrMainLRig.gmbl , consGrp = lipUprLRig.ofst , type = 'parent' )
            lipUprRocWor = rt.localWorld( obj = lipUprRRig.ctrl , worldObj = lipUprMainRig.gmbl , localObj = lipCnrMainRRig.gmbl , consGrp = lipUprRRig.ofst , type = 'parent' )

            lipLwrLocWor = rt.localWorld( obj = lipLwrLRig.ctrl , worldObj = lipLwrMainRig.gmbl , localObj = lipCnrMainLRig.gmbl , consGrp = lipLwrLRig.ofst , type = 'parent' )
            lipLwrRocWor = rt.localWorld( obj = lipLwrRRig.ctrl , worldObj = lipLwrMainRig.gmbl , localObj = lipCnrMainRRig.gmbl , consGrp = lipLwrRRig.ofst , type = 'parent' )

            lipUprLLocWorArray.append(lipUprLocWor)
            lipUprRLocWorArray.append(lipUprRocWor)
            lipLwrLLocWorArray.append(lipLwrLocWor)
            lipLwrRLocWorArray.append(lipLwrRocWor)


        lipCnrMainLRigShape = core.Dag(lipCnrMainLRig.ctrl.shape)
        lipCnrMainRRigShape = core.Dag(lipCnrMainRRig.ctrl.shape)

        #-- create corner pos group
        print "# Calculate Rotate Corner Mouth"
        listSideRtt = [{'lipCnrRig'         : lipCnrMainLRig ,
                        'ctrlShape'         : lipCnrMainLRigShape ,
                        'side'              : '_L_',
                        'lipLwrArray'       : lipLwrLArray,
                        'lipUprArray'       : lipUprLArray,
                        'lipUprLocWorArray' : lipUprLLocWorArray,
                        'lipLwrLocWorArray' : lipLwrLLocWorArray},
                        {'lipCnrRig'        : lipCnrMainRRig ,
                        'ctrlShape'         : lipCnrMainRRigShape ,
                        'side'              : '_R_',
                        'lipLwrArray'       : lipLwrRArray,
                        'lipUprArray'       : lipUprRArray,
                        'lipUprLocWorArray' : lipUprRLocWorArray,
                        'lipLwrLocWorArray' : lipLwrRLocWorArray}]

        for each in listSideRtt:
            for i in range(-1,(cnrNum*-1)-1,-1):
                valueAttr = (cnrNum*30.0)*(-1.0/i)

                each['ctrlShape'].addAttr('Cnr{}{}_Amp'.format(elem,(i*-1)))
                each['ctrlShape'].attr('Cnr{}{}_Amp'.format(elem,i*-1)).v = valueAttr

                cnrLipAmpGrp = cn.createNode('transform','LipCnr{}{}PosRtt{}Grp'.format(elem , i*-1 , each['side']))
                cnrLipAmpGrp.snap(each['lipCnrRig'].gmbl)
                cnrLipAmpGrp.parent(each['lipCnrRig'].gmbl)
                each['lipLwrArray'][i].ctrlGrp.parent(cnrLipAmpGrp)
                each['lipUprArray'][i].ctrlGrp.parent(cnrLipAmpGrp)

                cnrLipCmp = cn.createNode('clamp' , 'LipCnr{}{}PosRtt{}Cmp'.format(elem , i*-1 , each['side']))
                cnrLipCmp.attr('minR').v = -0.5
                cnrLipCmp.attr('maxR').v = 0.5
                each['lipCnrRig'].ctrl.attr('ty') >> cnrLipCmp.attr('ipr')

                cnrLipMdv = cn.createNode('multiplyDivide' , 'LipCnr{}{}PosRttAmp{}Mdv'.format(elem , i*-1 , each['side']))
                cnrLipMulMdv = cn.createNode('multiplyDivide' , 'LipCnr{}{}PosRttMulAmp{}Mdv'.format(elem , i*-1 , each['side']))
                cnrLipMulMdv.attr('i2x').v = 10

                cnrLipCmp.attr('opr') >> cnrLipMdv.attr('i1x')
                each['ctrlShape'].attr('Cnr{}{}_Amp'.format(elem,i*-1)) >> cnrLipMdv.attr('i2x')
                cnrLipMdv.attr('ox') >> cnrLipMulMdv.attr('i1x')
                cnrLipMulMdv.attr('ox') >> cnrLipAmpGrp.attr('rz')

                each['lipUprLocWorArray'][i]['spaceGrp'].parent(cnrLipAmpGrp)
                each['lipLwrLocWorArray'][i]['spaceGrp'].parent(cnrLipAmpGrp)


        #-- Calculate LocalWorld and Cheek, Nose and Eyes Follow Mouth Rig
        #-- Lips Calculate
        print "# Calculate Auto Lips"
        locWorValueArray = [0]
        numJnt = len(lipUprLArray)

        for i in range(numJnt-1):
            valueLocWor = (((0.5 / (numJnt-1)) * (i+1) * 2) - ((0.5 / ((numJnt-1) * 2)) * 2))
            locWorValueArray.append(valueLocWor)

        locWorValueArray = locWorValueArray[::-1]

        for eachArray in [lipUprLArray , lipUprRArray , lipLwrLArray , lipLwrRArray]:
            for i in range(numJnt):
                eachArray[i].ctrl.attr('localWorld').v = locWorValueArray[i]

        listSideCnr  = [{'ctrl' : lipCnrMainLRig.ctrl ,
                        'ctrlShape' : lipCnrMainLRigShape ,
                        'side' : '_L_',
                        'cheekMainOfst' : cheekMainLRig.ofst,
                        'cheekDtl' : cheekDtlLArray,
                        'cheekUpr' : cheekUprLArray},
                        {'ctrl' : lipCnrMainRRig.ctrl ,
                        'ctrlShape' : lipCnrMainRRigShape ,
                        'side' : '_R_',
                        'cheekMainOfst' : cheekMainRRig.ofst,
                        'cheekDtl' : cheekDtlRArray,
                        'cheekUpr' : cheekUprRArray}]

        listSideNose = [{   'ctrl'      : lipCnrMainLRig.ctrl ,
                            'ctrlShape' : lipCnrMainLRigShape ,
                            'side'      : '_L_' ,
                            'noseArray' : noseLArray},
                        {   'ctrl'      : lipCnrMainRRig.ctrl ,
                            'ctrlShape' : lipCnrMainRRigShape ,
                            'side'      : '_R_' ,
                            'noseArray' : noseRArray}]

        #-- Add Amp Attribute
        mthCtrlShape = core.Dag(mthRig.ctrl.shape)
        mthCtrlShape.addAttr('PuffMain{}LTx_Amp'.format(elem))
        mthCtrlShape.addAttr('PuffMain{}RTx_Amp'.format(elem))
        mthCtrlShape.addAttr('PuffMain{}LRy_Amp'.format(elem))
        mthCtrlShape.addAttr('PuffMain{}RRy_Amp'.format(elem))

        mthCtrlShape.attr('PuffMain{}LTx_Amp'.format(elem)).v = 0.1
        mthCtrlShape.attr('PuffMain{}RTx_Amp'.format(elem)).v = -0.1
        mthCtrlShape.attr('PuffMain{}LRy_Amp'.format(elem)).v = 0.001
        mthCtrlShape.attr('PuffMain{}RRy_Amp'.format(elem)).v = -0.001

        for each in listSideCnr:
            each['ctrlShape'].addAttr('PuffMain{}X_Amp'.format(elem))
            each['ctrlShape'].addAttr('PuffMain{}Y_Amp'.format(elem))

            for i in range(len(each['cheekUpr'])):
                each['ctrlShape'].addAttr('PuffUpr{}{}_Amp'.format(elem , i+1))

            for i in range(len(each['cheekDtl'])):
                each['ctrlShape'].addAttr('PuffDtl{}{}X_Amp'.format(elem , i+1))
                each['ctrlShape'].addAttr('PuffDtl{}{}Y_Amp'.format(elem , i+1))

        if noseMainTmpJnt:
            mthCtrlShape.addAttr('NoseMain{}Tx_Amp'.format(elem))
            mthCtrlShape.addAttr('NoseMain{}Ty_Amp'.format(elem))
            mthCtrlShape.addAttr('NoseMain{}Rx_Amp'.format(elem))
            mthCtrlShape.addAttr('NoseMain{}Ry_Amp'.format(elem))

            mthCtrlShape.attr('NoseMain{}Tx_Amp'.format(elem)).v = 1
            mthCtrlShape.attr('NoseMain{}Ty_Amp'.format(elem)).v = 1
            mthCtrlShape.attr('NoseMain{}Rx_Amp'.format(elem)).v = 0.1
            mthCtrlShape.attr('NoseMain{}Ry_Amp'.format(elem)).v = -0.1

            for each in listSideNose:
                for i in range(len(each['noseArray'])):
                    each['ctrlShape'].addAttr('NoseDtl{}{}X_Amp'.format(elem , i+1))
                    each['ctrlShape'].addAttr('NoseDtl{}{}Y_Amp'.format(elem , i+1))

        if eyeDirOfstL:
            for each in eyeDirOfstL:
                nameEyeOfst = each.split('Ofst')[0]
                lipCnrMainLRigShape.addAttr('{}_Amp'.format(nameEyeOfst))

        if eyeDirOfstR:
            for each in eyeDirOfstR:
                nameEyeOfst = each.split('Ofst')[0]
                lipCnrMainRRigShape.addAttr('{}_Amp'.format(nameEyeOfst))

        lipCnrMainLRigShape.addTitleAttr('LimitSetting')
        lipCnrMainRRigShape.addTitleAttr('LimitSetting')

        #-- Cheek Calculate
        print "# Calculate Auto Cheek"
        for each in listSideCnr:
            calNum = 1
            if each['side'] == '_L_':
                puffSideAttr = 'PuffMain{}L'.format(elem)
            else:
                puffSideAttr = 'PuffMain{}R'.format(elem)

            #-- Mouth Auto Cheek
            mthCtrlShape.addTitleAttr('PuffMain')
            mthCtrlShape.addAttr('{}Tx_Min'.format(puffSideAttr))
            mthCtrlShape.addAttr('{}Tx_Max'.format(puffSideAttr))
            mthCtrlShape.addAttr('{}Ry_Min'.format(puffSideAttr))
            mthCtrlShape.addAttr('{}Ry_Max'.format(puffSideAttr))
            mthCtrlShape.attr('{}Tx_Min'.format(puffSideAttr)).v = -2
            mthCtrlShape.attr('{}Tx_Max'.format(puffSideAttr)).v = 2
            mthCtrlShape.attr('{}Ry_Min'.format(puffSideAttr)).v = -20
            mthCtrlShape.attr('{}Ry_Max'.format(puffSideAttr)).v = 20

            puffMthCmp = cn.createNode('clamp' , 'PuffMthMain{}{}Cmp'.format(elem , each['side']))
            puffMthMdv = cn.createNode('multiplyDivide' , 'PuffMthMain{}{}Mdv'.format(elem , each['side']))
            puffMthRevMdv = cn.createNode('multiplyDivide' , 'PuffMthMainRev{}{}Mdv'.format(elem , each['side']))
            puffMainPma = cn.createNode('plusMinusAverage' , 'PuffMain{}Tx{}Pma'.format(elem , each['side']))
            puffMainPma.attr('o1') >> each['cheekMainOfst'].attr('tx')
            puffMthRevMdv.attr('i2x').v = calNum
            puffMthRevMdv.attr('i2y').v = calNum

            mthRig.ctrl.attr('tx') >> puffMthCmp.attr('ipr')
            mthCtrlShape.attr('{}Tx_Min'.format(puffSideAttr)) >> puffMthCmp.attr('minR')
            mthCtrlShape.attr('{}Tx_Max'.format(puffSideAttr)) >> puffMthCmp.attr('maxR')
            puffMthCmp.attr('opr') >> puffMthMdv.attr('i1x')
            mthCtrlShape.attr('{}Tx_Amp'.format(puffSideAttr)) >> puffMthMdv.attr('i2x')
            puffMthMdv.attr('ox') >> puffMthRevMdv.attr('i1x')
            puffMthRevMdv.attr('ox') >> puffMainPma.attr('i1[0]')

            mthRig.ctrl.attr('ry') >> puffMthCmp.attr('ipg')
            mthCtrlShape.attr('{}Ry_Min'.format(puffSideAttr)) >> puffMthCmp.attr('minG')
            mthCtrlShape.attr('{}Ry_Max'.format(puffSideAttr)) >> puffMthCmp.attr('maxG')
            puffMthCmp.attr('opg') >> puffMthMdv.attr('i1y')
            mthCtrlShape.attr('{}Ry_Amp'.format(puffSideAttr)) >> puffMthMdv.attr('i2y')
            puffMthMdv.attr('oy') >> puffMthRevMdv.attr('i1y')
            puffMthRevMdv.attr('oy') >> puffMainPma.attr('i1[1]')


            #-- Lip Coner Auto Cheek
            each['ctrlShape'].addTitleAttr('PuffMain')
            each['ctrlShape'].addAttr('PuffMain{}X_Min'.format(elem))
            each['ctrlShape'].addAttr('PuffMain{}X_Max'.format(elem))

            each['ctrlShape'].attr('PuffMain{}X_Amp'.format(elem)).v = 0.5
            each['ctrlShape'].attr('PuffMain{}X_Min'.format(elem)).v = -2
            each['ctrlShape'].attr('PuffMain{}X_Max'.format(elem)).v = 2

            puffCmp = cn.createNode('clamp' , 'PuffMain{}{}Cmp'.format(elem , each['side']))
            puffMdv = cn.createNode('multiplyDivide' , 'PuffMain{}{}Mdv'.format(elem , each['side']))
            puffRevMdv = cn.createNode('multiplyDivide' , 'PuffMainRev{}{}Mdv'.format(elem , each['side']))
            puffRevMdv.attr('i2x').v = calNum

            each['ctrl'].attr('tx') >> puffCmp.attr('ipr')
            each['ctrlShape'].attr('PuffMain{}X_Min'.format(elem)) >> puffCmp.attr('minR')
            each['ctrlShape'].attr('PuffMain{}X_Max'.format(elem)) >> puffCmp.attr('maxR')
            puffCmp.attr('opr') >> puffMdv.attr('i1x')
            each['ctrlShape'].attr('PuffMain{}X_Amp'.format(elem)) >> puffMdv.attr('i2x')
            puffMdv.attr('ox') >> puffRevMdv.attr('i1x')
            puffRevMdv.attr('ox') >> puffMainPma.attr('i1[2]')

            each['ctrlShape'].addAttr('PuffMain{}Y_Min'.format(elem))
            each['ctrlShape'].addAttr('PuffMain{}Y_Max'.format(elem))

            each['ctrlShape'].attr('PuffMain{}Y_Amp'.format(elem)).v = 0.2
            each['ctrlShape'].attr('PuffMain{}Y_Min'.format(elem)).v = -4
            each['ctrlShape'].attr('PuffMain{}Y_Max'.format(elem)).v = 4

            each['ctrl'].attr('ty') >> puffCmp.attr('ipg')
            each['ctrlShape'].attr('PuffMain{}Y_Min'.format(elem)) >> puffCmp.attr('minG')
            each['ctrlShape'].attr('PuffMain{}Y_Max'.format(elem)) >> puffCmp.attr('maxG')
            puffCmp.attr('opg') >> puffMdv.attr('i1y')
            each['ctrlShape'].attr('PuffMain{}Y_Amp'.format(elem)) >> puffMdv.attr('i2y')
            puffMdv.attr('oy') >> puffRevMdv.attr('i1y')
            puffRevMdv.attr('oy') >> each['cheekMainOfst'].attr('ty')

            for i in range(len(each['cheekUpr'])):
                each['ctrlShape'].addTitleAttr('PuffUpr')
                each['ctrlShape'].addAttr('PuffUpr{}{}_Min'.format(elem , i+1))
                each['ctrlShape'].addAttr('PuffUpr{}{}_Max'.format(elem , i+1))

                each['ctrlShape'].attr('PuffUpr{}{}_Amp'.format(elem , i+1)).v = 0.25
                each['ctrlShape'].attr('PuffUpr{}{}_Min'.format(elem , i+1)).v = -2
                each['ctrlShape'].attr('PuffUpr{}{}_Max'.format(elem , i+1)).v = 2

                puffUprCmp = cn.createNode('clamp' , 'PuffUpr{}{}{}Cmp'.format(elem , i+1 , each['side']))
                puffUprMdv = cn.createNode('multiplyDivide' , 'PuffUpr{}{}{}Mdv'.format(elem , i+1 , each['side']))

                each['ctrl'].attr('ty') >> puffUprCmp.attr('ipg')
                each['ctrlShape'].attr('PuffUpr{}{}_Min'.format(elem , i+1)) >> puffUprCmp.attr('minG')
                each['ctrlShape'].attr('PuffUpr{}{}_Max'.format(elem , i+1)) >> puffUprCmp.attr('maxG')
                puffUprCmp.attr('opg') >> puffUprMdv.attr('i1y')
                each['ctrlShape'].attr('PuffUpr{}{}_Amp'.format(elem , i+1)) >> puffUprMdv.attr('i2y')
                puffUprMdv.attr('oy') >> each['cheekUpr'][i].ofst.attr('ty')


            for i in range(len(each['cheekDtl'])):
                each['ctrlShape'].addTitleAttr('PuffDtl')
                each['ctrlShape'].addAttr('PuffDtl{}{}X_Min'.format(elem , i+1))
                each['ctrlShape'].addAttr('PuffDtl{}{}X_Max'.format(elem , i+1))

                each['ctrlShape'].attr('PuffDtl{}{}X_Amp'.format(elem , i+1)).v = 0.25
                each['ctrlShape'].attr('PuffDtl{}{}X_Min'.format(elem , i+1)).v = -2
                each['ctrlShape'].attr('PuffDtl{}{}X_Max'.format(elem , i+1)).v = 2

                puffDtlCmp = cn.createNode('clamp' , 'PuffDtl{}{}{}Cmp'.format(elem , i+1 , each['side']))
                puffDtlMdv = cn.createNode('multiplyDivide' , 'PuffDtl{}{}{}Mdv'.format(elem , i+1 , each['side']))
                puffDtlRevMdv = cn.createNode('multiplyDivide' , 'PuffDtlRev{}{}Mdv'.format(elem , each['side']))
                puffDtlRevMdv.attr('i2x').v = calNum

                each['ctrl'].attr('tx') >> puffDtlCmp.attr('ipr')
                each['ctrlShape'].attr('PuffDtl{}{}X_Min'.format(elem , i+1)) >> puffDtlCmp.attr('minR')
                each['ctrlShape'].attr('PuffDtl{}{}X_Max'.format(elem , i+1)) >> puffDtlCmp.attr('maxR')
                puffDtlCmp.attr('opr') >> puffDtlMdv.attr('i1x')
                each['ctrlShape'].attr('PuffDtl{}{}X_Amp'.format(elem , i+1)) >> puffDtlMdv.attr('i2x')
                puffDtlMdv.attr('ox') >> puffDtlRevMdv.attr('i1x')
                puffDtlRevMdv.attr('ox') >> each['cheekDtl'][i].ofst.attr('tx')

                each['ctrlShape'].addAttr('PuffDtl{}{}Y_Min'.format(elem , i+1))
                each['ctrlShape'].addAttr('PuffDtl{}{}Y_Max'.format(elem , i+1))

                each['ctrlShape'].attr('PuffDtl{}{}Y_Amp'.format(elem , i+1)).v = 0.5
                each['ctrlShape'].attr('PuffDtl{}{}Y_Min'.format(elem , i+1)).v = -2
                each['ctrlShape'].attr('PuffDtl{}{}Y_Max'.format(elem , i+1)).v = 2

                each['ctrl'].attr('ty') >> puffDtlCmp.attr('ipg')
                each['ctrlShape'].attr('PuffDtl{}{}Y_Min'.format(elem , i+1)) >> puffDtlCmp.attr('minG')
                each['ctrlShape'].attr('PuffDtl{}{}Y_Max'.format(elem , i+1)) >> puffDtlCmp.attr('maxG')
                puffDtlCmp.attr('opg') >> puffDtlMdv.attr('i1y')
                each['ctrlShape'].attr('PuffDtl{}{}Y_Amp'.format(elem , i+1)) >> puffDtlMdv.attr('i2y')
                puffDtlMdv.attr('oy') >> puffDtlRevMdv.attr('i1y')
                puffDtlRevMdv.attr('oy') >> each['cheekDtl'][i].ofst.attr('ty')


        #-- Nose Calculate
        if noseMainTmpJnt:
            print "# Calculate Auto Nose"
            #-- Calculate Main Nose follow Main Mouth

            noseMainTxAmpMdv = cn.createNode('multiplyDivide' , 'NoseMain{}TxAmp{}Mdv'.format(elem , self.side))
            noseMainTyAmpMdv = cn.createNode('multiplyDivide' , 'NoseMain{}TyAmp{}Mdv'.format(elem , self.side))
            noseMainTslMdv = cn.createNode('multiplyDivide' , 'NoseMain{}Tsl{}Mdv'.format(elem , self.side))
            noseMainRttMdv = cn.createNode('multiplyDivide' , 'NoseMain{}Rtt{}Mdv'.format(elem , self.side))
            noseMainTxTotal = cn.createNode('plusMinusAverage' , 'NoseMain{}TxTotal{}Mdv'.format(elem , self.side))
            noseMainTyTotal = cn.createNode('plusMinusAverage' , 'NoseMain{}TyTotal{}Mdv'.format(elem , self.side))

            mthRig.ctrl.attr('tx') >> noseMainTxAmpMdv.attr('i1x')
            mthRig.ctrl.attr('ty') >> noseMainTyAmpMdv.attr('i1y')
            mthRig.ctrl.attr('rx') >> noseMainTyAmpMdv.attr('i1x')
            mthRig.ctrl.attr('ry') >> noseMainTxAmpMdv.attr('i1y')
            mthCtrlShape.attr('NoseMain{}Tx_Amp'.format(elem)) >> noseMainTxAmpMdv.attr('i2x')
            mthCtrlShape.attr('NoseMain{}Ty_Amp'.format(elem)) >> noseMainTyAmpMdv.attr('i2y')
            mthCtrlShape.attr('NoseMain{}Rx_Amp'.format(elem)) >> noseMainTyAmpMdv.attr('i2x')
            mthCtrlShape.attr('NoseMain{}Ry_Amp'.format(elem)) >> noseMainTxAmpMdv.attr('i2y')

            noseMainTxAmpMdv.attr('ox') >> noseMainTslMdv.attr('i1x')
            noseMainTslMdv.attr('i2x').v = 0.1
            noseMainTyAmpMdv.attr('oy') >> noseMainTslMdv.attr('i1y')
            noseMainTslMdv.attr('i2y').v = 0.1

            noseMainTyAmpMdv.attr('ox') >> noseMainRttMdv.attr('i1x')
            noseMainRttMdv.attr('i2x').v = -0.01
            noseMainTxAmpMdv.attr('oy') >> noseMainRttMdv.attr('i1y')
            noseMainRttMdv.attr('i2y').v = -0.01

            noseMainTslMdv.attr('ox') >> noseMainTxTotal.attr('i1[0]')
            noseMainTslMdv.attr('oy') >> noseMainTyTotal.attr('i1[0]')
            noseMainRttMdv.attr('ox') >> noseMainTyTotal.attr('i1[1]')
            noseMainRttMdv.attr('oy') >> noseMainTxTotal.attr('i1[1]')

            noseMainTxTotal.attr('o1') >> noseMainRig.ofst.attr('tx')
            noseMainTyTotal.attr('o1') >> noseMainRig.ofst.attr('ty')


            #-- Calculate Detail Nose follow Corner Lip
            for each in listSideNose:
                calNum = 1

                for i in range(len(each['noseArray'])):
                    each['ctrlShape'].attr('NoseDtl{}{}X_Amp'.format(elem , i+1)).v = 0.1
                    each['ctrlShape'].attr('NoseDtl{}{}Y_Amp'.format(elem , i+1)).v = 0.1

                    noseDtlMdv = cn.createNode('multiplyDivide' , 'NoseDtl{}{}{}Mdv'.format(elem , i+1 , each['side']))
                    noseDtlRevMdv = cn.createNode('multiplyDivide' , 'NoseDtlMainRev{}{}Mdv'.format(elem , each['side']))
                    noseDtlRevMdv.attr('i2x').v = calNum

                    each['ctrl'].attr('tx') >> noseDtlMdv.attr('i1x')
                    each['ctrlShape'].attr('NoseDtl{}{}X_Amp'.format(elem , i+1)) >> noseDtlMdv.attr('i2x')
                    each['ctrl'].attr('ty') >> noseDtlMdv.attr('i1y')
                    each['ctrlShape'].attr('NoseDtl{}{}Y_Amp'.format(elem , i+1)) >> noseDtlMdv.attr('i2y')
                    noseDtlMdv.attr('ox') >> noseDtlRevMdv.attr('i1x')
                    noseDtlMdv.attr('oy') >> noseDtlRevMdv.attr('i1y')
                    noseDtlRevMdv.attr('ox') >> each['noseArray'][i].ofst.attr('tx')
                    noseDtlRevMdv.attr('oy') >> each['noseArray'][i].ofst.attr('ty')

        if eyeDirOfstL:
            lipCnrMainLRigShape.addTitleAttr('EyeDir')
            for each in eyeDirOfstL:
                nameEyeOfst = each.split('Ofst')[0]
                eyeOfst = core.Dag(each)

                lipCnrMainLRigShape.attr('{}_Amp'.format(nameEyeOfst)).v = 5

                lipCnrMainLRigShape.addAttr('{}_Min'.format(nameEyeOfst))
                lipCnrMainLRigShape.addAttr('{}_Max'.format(nameEyeOfst))
                lipCnrMainLRigShape.attr('{}_Min'.format(nameEyeOfst)).v = -1
                lipCnrMainLRigShape.attr('{}_Max'.format(nameEyeOfst)).v = 1

                eyeCmp = cn.createNode('clamp' , '{}LipCnr_L_Cmp'.format(nameEyeOfst))
                eyeMdv = cn.createNode('multiplyDivide' , '{}LipCnrAmp_L_Mdv'.format(nameEyeOfst))
                lipCnrMainLRigShape.attr('{}_Min'.format(nameEyeOfst)) >> eyeCmp.attr('minR')
                lipCnrMainLRigShape.attr('{}_Max'.format(nameEyeOfst)) >> eyeCmp.attr('maxR')

                lipCnrMainLRig.ctrl.attr('ty') >> eyeCmp.attr('ipr')
                lipCnrMainLRigShape.attr('{}_Amp'.format(nameEyeOfst)) >> eyeMdv.attr('i1x')
                eyeCmp.attr('opr') >> eyeMdv.attr('i2x')

                src = eyeOfst.attr('rx').source
                eyeOfst.unlockAttrs('rx')

                if not src:
                    eyeMdv.attr('ox') >> eyeOfst.attr('rx')
                else:
                    srcDag = core.Dag(src[0])
                    if not srcDag.getNodeType() == 'plusMinusAverage':
                        eyeFollowPma = cn.createNode('{}{}FollowLipCnr_L_Pma'.format(nameEyeOfst , elem))
                        srcAttr = pm.listConnections(eyeOfst.attr('rx') , p=True , d=False , scn=True)
                        mc.connectAttr(srcAttr , eyeFollowPma.attr('i1[0]'))
                        eyeMdv.attr('ox') >> eyeFollowPma.attr('i1[1]')
                        eyeFollowPma.attr('o1') >> eyeOfst.attr('rx')
                    else:
                        sizeInputPma = srcDag.attr('i1').getSize()
                        eyeMdv.attr('ox') >> srcDag.attr('i1[{}]'.format(sizeInputPma))

                eyeOfst.attr('rx').lock = True

        if eyeDirOfstR:
            lipCnrMainRRigShape.addTitleAttr('EyeDir')
            for each in eyeDirOfstR:
                nameEyeOfst = each.split('Ofst')[0]
                eyeOfst = core.Dag(each)

                lipCnrMainRRigShape.attr('{}_Amp'.format(nameEyeOfst)).v = 5

                lipCnrMainRRigShape.addAttr('{}_Min'.format(nameEyeOfst))
                lipCnrMainRRigShape.addAttr('{}_Max'.format(nameEyeOfst))
                lipCnrMainRRigShape.attr('{}_Min'.format(nameEyeOfst)).v = -1
                lipCnrMainRRigShape.attr('{}_Max'.format(nameEyeOfst)).v = 1

                eyeCmp = cn.createNode('clamp' , '{}LipCnr_R_Cmp'.format(nameEyeOfst))
                eyeMdv = cn.createNode('multiplyDivide' , '{}LipCnrAmp_R_Mdv'.format(nameEyeOfst))
                lipCnrMainRRigShape.attr('{}_Min'.format(nameEyeOfst)) >> eyeCmp.attr('minR')
                lipCnrMainRRigShape.attr('{}_Max'.format(nameEyeOfst)) >> eyeCmp.attr('maxR')

                lipCnrMainRRig.ctrl.attr('ty') >> eyeCmp.attr('ipr')
                lipCnrMainRRigShape.attr('{}_Amp'.format(nameEyeOfst)) >> eyeMdv.attr('i1x')
                eyeCmp.attr('opr') >> eyeMdv.attr('i2x')

                src = eyeOfst.attr('rx').source
                eyeOfst.unlockAttrs('rx')

                if not src:
                    eyeMdv.attr('ox') >> eyeOfst.attr('rx')
                else:
                    srcDag = core.Dag(src[0])
                    if not srcDag.getNodeType() == 'plusMinusAverage':
                        eyeFollowPma = cn.createNode('{}{}FollowLipCnr_R_Pma'.format(nameEyeOfst , elem))
                        srcAttr = pm.listConnections(eyeOfst.attr('rx') , p=True , d=False , scn=True)
                        mc.connectAttr(srcAttr , eyeFollowPma.attr('i1[0]'))
                        eyeMdv.attr('ox') >> eyeFollowPma.attr('i1[1]')
                        eyeFollowPma.attr('o1') >> eyeOfst.attr('rx')
                    else:
                        sizeInputPma = srcDag.attr('i1').getSize()
                        eyeMdv.attr('ox') >> srcDag.attr('i1[{}]'.format(sizeInputPma))

                eyeOfst.attr('rx').lock = True


        #-- skin nrb
        mc.skinCluster( parent , nrb , tsb = True , mi = 1 , n = 'Mouth{}Nrb{}Skc'.format(elem , self.side))

        #-- Adjust Grp
        self.mthCtrlGrp.parent(ctrlGrp)
        self.mthStillGrp.parent(stillGrp)
        mc.parent(nrb , self.mthStillGrp)

        for each in normGrpArray:
            each['normGrp'].parent(self.mthNormGrp)

        # #-- Create Lip Seal
        # upLipL = [core.Dag(i.ctrl) for i in lipUprLArray[::-1]]
        # upLipR = [core.Dag(i.ctrl) for i in lipUprRArray]
        # upLipC = [core.Dag(lipUprRig.ctrl)]
        # lowLipL = [core.Dag(i.ctrl) for i in lipLwrLArray[::-1]]
        # lowLipR = [core.Dag(i.ctrl) for i in lipLwrRArray]
        # lowLipC = [core.Dag(lipLwrRig.ctrl)]

        # cnrMainLip = [core.Dag(lipCnrMainLRig.ctrl) , core.Dag(lipCnrMainRRig.ctrl)]
        # upLip = upLipL + upLipC + upLipR
        # lowLip = lowLipL + lowLipC + lowLipR

        # self.createLipSeal(upLip , lowLip , cnrMainLip)

    def createNormOnNrb(self ,  name         = '' ,
                                side         = '' , 
                                position     = '' ,
                                parent       = '' ,
                                child        = '' ,
                                nrb          = '' ,
                                *args):

        if side:
            side = '_{}_'.format(side)
        else:
            side = ''

        normGrp = cn.createNode('transform' , '{}Norm{}Grp'.format(name,side))
        pioGrp = cn.createNode('transform' , '{}Pio{}Grp'.format(name,side))
        pioGrp.parent(normGrp)
        normGrp.snap(position)
        mc.normalConstraint(nrb , normGrp , w = 1 , aim = (0,0,1) , u = (0,1,0) , worldUpType ='vector' , wu = (0,1,0))
        mc.geometryConstraint(nrb , normGrp )
        mc.pointConstraint(parent , normGrp , mo=True)
        mc.scaleConstraint(parent , normGrp , mo=True)
        mc.orientConstraint(parent , pioGrp , mo=True)
        mc.parentConstraint(pioGrp , child , mo=True)
        mc.scaleConstraint(pioGrp , child , mo=True)

        return {'normGrp' : normGrp , 'pioGrp' : pioGrp}

    # def createLipSeal(self, upLipLs, lowLipLs, cnrLs):  
    #     # find max min value
    #     numCtr = len(upLipLs)
        
    #     minVal = []
    #     maxVal = []
    #     for num in range(numCtr):
    #         minVal.append(float(num) * 10 / numCtr)
    #         maxVal.append(float(num+1) * 10 / numCtr)

    #     # create seal
    #     for cnr in cnrLs:
    #         if not mc.objExists(cnr.attr(".seal")):
    #             cnr.addAttr("seal" , 0 , 10 , True)
                
    #     ## up seal
    #     for num, ctr in enumerate(upLipLs):
    #         upStr = cn.createNode("setRange", ctr.__str__().replace("_Ctrl", "_Str") , on = (minVal[num], minVal[::-1][num], 0) , om = (maxVal[num], maxVal[::-1][num], 0))
    #         upMdl = cn.createNode("multDoubleLinear", ctr.__str__().replace("_Ctrl", "_Mdl") , i2 = -2)
    #         upPma = cn.createNode("plusMinusAverage", ctr.__str__().replace("_Ctrl", "_Pma"))
    #         upCmp = cn.createNode("clamp", ctr.__str__().replace("_Ctrl", "_Cmp") , mxr = 1)
            
    #         if "_L_" not in upCmp.__str__() and "_R_" not in upCmp.__str__():
    #             upCmp.attr("mnr").v = 0.5
            
    #         cnrLs[0].attr("seal") >> upStr.attr("vx")
    #         cnrLs[1].attr("seal") >> upStr.attr("vy")
    #         ctr.attr("localWorld") >> upStr.attr("nx")
    #         ctr.attr("localWorld") >> upStr.attr("ny")
    #         ctr.attr("localWorld") >> upMdl.attr("i1")
            
    #         ctr.attr("localWorld") >> upPma.attr("i1[0]")
    #         upStr.attr("ox") >> upPma.attr("i1[1]")
    #         upStr.attr("oy") >> upPma.attr("i1[2]")
    #         upMdl.attr("o") >> upPma.attr("i1[3]")

    #         upPma.attr("o1") >> upCmp.attr("ipr")
            
    #         rev = core.Dag(mc.listConnections(ctr.attr("localWorld"), t="reverse", p=1)[0])
    #         lcw = core.Dag(mc.listConnections(ctr.attr("localWorld"), t="constraint", p=1)[0])
    #         ctr.attr("localWorld") // rev
    #         ctr.attr("localWorld") // lcw
            
    #         upCmp.attr("opr") >> rev
    #         upCmp.attr("opr") >> lcw
        
    #     ## low seal
    #     for num, ctr in enumerate(lowLipLs):
    #         lowStr = cn.createNode("setRange", ctr.__str__().replace("_Ctrl", "_Str") , on = (minVal[num], minVal[::-1][num], 0) , om = (maxVal[num], maxVal[::-1][num], 0))
    #         lowMdl = cn.createNode("multDoubleLinear", ctr.__str__().replace("_Ctrl", "_Mdl") , i2 = -2)
    #         lowPma = cn.createNode("plusMinusAverage", ctr.__str__().replace("_Ctrl", "_Pma"))
    #         lowCmp = cn.createNode("clamp", ctr.__str__().replace("_Ctrl", "_Cmp") , mxr = 1)
            
    #         if "_L_" not in lowCmp.__str__() and "_R_" not in lowCmp.__str__():
    #             lowCmp.attr("mnr").v = 0.5
            
    #         cnrLs[0].attr("seal") >> lowStr.attr("vx")
    #         cnrLs[1].attr("seal") >> lowStr.attr("vy")
    #         ctr.attr("localWorld") >> lowStr.attr("nx")
    #         ctr.attr("localWorld") >> lowStr.attr("ny")
    #         ctr.attr("localWorld") >> lowMdl.attr("i1")
            
    #         ctr.attr("localWorld") >> lowPma.attr("i1[0]")
    #         lowStr.attr("ox") >> lowPma.attr("i1[1]")
    #         lowStr.attr("oy") >> lowPma.attr("i1[2]")
    #         lowMdl.attr("o") >> lowPma.attr("i1[3]")

    #         lowPma.attr("o1") >> lowCmp.attr("ipr")
            
    #         rev = core.Dag(mc.listConnections(ctr.attr("localWorld"), t="reverse", p=1)[0])
    #         lcw = core.Dag(mc.listConnections(ctr.attr("localWorld"), t="constraint", p=1)[0])
    #         ctr.attr("localWorld") // rev
    #         ctr.attr("localWorld") // lcw
                
    #         lowCmp.attr("opr") >> rev
    #         lowCmp.attr("opr") >> lcw


# Example
# from rigScript import mthDir
# reload(mthDir)
# mthDir.Run(             lipUprCenTmpJnt         = 'lipMainUp_TmpJnt' ,
#                         lipLwrCenTmpJnt         = 'lipMainDn_TmpJnt' ,
#                         lipCnrLTmpJnt           = 'lipCnr_L_TmpJnt' ,
#                         lipCnrRTmpJnt           = 'lipCnr_R_TmpJnt' ,
#                         lipUprLTmpJnt           = ['lipUp1_L_TmpJnt','lipUp2_L_TmpJnt','lipUp3_L_TmpJnt'] ,
#                         lipUprRTmpJnt           = ['lipUp1_R_TmpJnt','lipUp2_R_TmpJnt','lipUp3_R_TmpJnt'] ,
#                         lipLwrLTmpJnt           = ['lipDn1_L_TmpJnt','lipDn2_L_TmpJnt','lipDn3_L_TmpJnt'] ,
#                         lipLwrRTmpJnt           = ['lipDn1_R_TmpJnt','lipDn2_R_TmpJnt','lipDn3_R_TmpJnt'] ,
#                         cheekMainLTmpJnt        = 'cheekMain_L_TmpJnt' , # puff
#                         cheekUprLTmpJnt         = ['cheekUpr1_L_TmpJnt','cheekUpr2_L_TmpJnt','cheekUpr3_L_TmpJnt'] , # under eyelid
#                         cheekDetailLTmpJnt      = ['cheekDtl1_L_TmpJnt','cheekDtl2_L_TmpJnt','cheekDtl3_L_TmpJnt'] , # beside lip corner
#                         cheekMainRTmpJnt        = 'cheekMain_R_TmpJnt' ,
#                         cheekUprRTmpJnt         = ['cheekUpr1_R_TmpJnt','cheekUpr2_R_TmpJnt','cheekUpr3_R_TmpJnt'] ,
#                         cheekDetailRTmpJnt      = ['cheekDtl1_R_TmpJnt','cheekDtl2_R_TmpJnt','cheekDtl3_R_TmpJnt'] ,

#                         jawUprTmpJnt            = ['JawUpr1_TmpJnt','JawUpr2_TmpJnt'] ,
#                         jawLwrTmpJnt            = ['JawLwr1_TmpJnt','JawLwr2_TmpJnt','JawLwrEnd_TmpJnt'] ,
#                         tongueTmpJnt            = ['tongue1_TmpJnt',
#                                                     'tongue2_TmpJnt',
#                                                     'tongue3_TmpJnt',
#                                                     'tongue4_TmpJnt',
#                                                     'tongue5_TmpJnt',
#                                                     'tongue6_TmpJnt'] ,
#                         teethUprMainTmpJnt      = 'teethMainUp_TmpJnt' ,
#                         teethLwrMainTmpJnt      = 'teethMainDn_TmpJnt' ,
#                         teethUprTmpJnt          = ['teethUp1_TmpJnt',
#                                                     'teethUp2_TmpJnt',
#                                                     'teethUp3_TmpJnt',
#                                                     'teethUp4_TmpJnt',
#                                                     'teethUp5_TmpJnt'] ,
#                         teethLwrTmpJnt          = ['teethDn1_TmpJnt',
#                                                     'teethDn2_TmpJnt',
#                                                     'teethDn3_TmpJnt',
#                                                     'teethDn4_TmpJnt',
#                                                     'teethDn5_TmpJnt',] ,

#                         noseMainTmpJnt          = 'nose_TmpJnt' ,
#                         noseLTmpJnt             = ['nose_L_TmpJnt'] ,
#                         noseRTmpJnt             = ['nose_R_TmpJnt'] ,

#                         nrb                     = 'Mouth_Nrb' ,
#                         parent                  = 'Head_Jnt' ,
#                         elem                    = 'A',
#                         eyeDirOfstL             = ['EyeLidLwrOfst_L_Grp'] ,
#                         eyeDirOfstR             = ['EyeLidLwrOfst_R_Grp'])


# mirror nose dtl
# change amp default
# jaw upr
# scale nurb