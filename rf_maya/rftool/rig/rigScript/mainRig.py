import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
import core
import rigTools as rt
import ctrlRig
reload( core )
reload( rt )
cn = core.Node()

# -------------------------------------------------------------------------------------------------------------
#
#  MAIN RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , size = 1 ):

        #-- Create Main Group
        self.rigGrp = cn.createNode( 'transform' , 'Rig_Grp' )
        self.geoGrp = cn.createNode( 'transform' , 'Geo_Grp' )
        self.jntGrp = cn.createNode( 'transform' , 'Jnt_Grp' )
        self.ikhGrp = cn.createNode( 'transform' , 'Ikh_Grp' )
        self.ctrlGrp = cn.createNode( 'transform' , 'Ctrl_Grp' )
        self.mainCtrlGrp = cn.createNode( 'transform' , 'MainCtrl_Grp' )
        self.allMoverGrp = cn.createNode( 'transform' , 'AllMover_Grp' )
        self.facialCtrlGrp = cn.createNode( 'transform' , 'FacialCtrl_Grp' )
        self.addRigCtrlGrp = cn.createNode( 'transform' , 'AddCtrl_Grp' )
        self.skinGrp = cn.createNode( 'transform' , 'Skin_Grp' )
        self.stillGrp = cn.createNode( 'transform' , 'Still_Grp' )
        self.techGrp = cn.createNode( 'transform' , 'TechGeo_Grp' )

        #-- Create Main Controls
        self.allMoverCtrl = rt.createCtrl( 'AllMover_Ctrl' , 'square' , 'yellow' )
        self.offsetCtrl = rt.createCtrl( 'Offset_Ctrl' , 'arrowCross' , 'yellow' )

        #-- Add Tag
        self.allMoverCtrl.addBoolTag( 'lockScale' , val = 0)
        
        #-- Adjust Shape Controls
        self.allMoverCtrl.rotateShape( 0 , 45 , 0 )
        self.allMoverCtrl.scaleShape( size * 7 )
        self.offsetCtrl.scaleShape( size * 1.5 )

        #-- Adjust Hierarchy
        mc.parent( self.offsetCtrl , self.allMoverCtrl )
        mc.parent( self.allMoverCtrl , self.allMoverGrp )
        mc.parent( self.ctrlGrp , self.skinGrp , self.jntGrp , self.ikhGrp , self.offsetCtrl )
        mc.parent( self.mainCtrlGrp , self.facialCtrlGrp , self.addRigCtrlGrp , self.ctrlGrp )
        mc.parent( self.geoGrp , self.allMoverGrp , self.stillGrp , self.techGrp, self.rigGrp )
        
        self.allMoverGrp.addAttr('scaleable')
        mc.setAttr('{}.scaleable'.format(self.allMoverGrp), 1)
        self.allMoverGrp.attr('scaleable') >> self.allMoverGrp.attr('sy')
        self.allMoverGrp.attr('sy') >> self.allMoverGrp.attr('sx')
        self.allMoverGrp.attr('sy') >> self.allMoverGrp.attr('sz')

        #-- Cleanup
        for obj in ( self.rigGrp , self.allMoverGrp , self.geoGrp , self.techGrp, self.jntGrp , self.ikhGrp , self.ctrlGrp , self.skinGrp , self.stillGrp , self.mainCtrlGrp , self.facialCtrlGrp , self.addRigCtrlGrp ) :
            obj.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

        self.allMoverCtrl.lockHideAttrs( 'v' )
        self.offsetCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        self.jntGrp.attr('v').value = 0
        self.ikhGrp.attr('v').value = 0
        self.stillGrp.attr('v').value = 0

        
        mc.select( cl = True )

# -------------------------------------------------------------------------------------------------------------
#
#  PROP RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Prop( Run ):
    
    def __init__( self , assetName = '' , pars = 'MdGeo_Grp' , size = 1 ):
        
        sels = mc.ls( sl = True )
        if not sels:
            return
        sel = sels[0]

        super( Prop , self ).__init__( assetName , size )

        # col = [ 'pink' , 'cyan' , 'red' , 'green' , 'blue' , 'brown' ]
        
        self.mdGrp = cn.createNode( 'transform' , 'MdGeo_Grp' )
        self.prGrp = cn.createNode( 'transform' , 'PrGeo_Grp' )

        mc.parent( self.mdGrp , self.prGrp , self.geoGrp )

        # nameSplit = sel.split('_')
        # propName = '_'.join( nameSplit[0:-1] )

        # self.porpCtrl = rt.createCtrl( '%s_Ctrl' %propName , 'square' , col[i%6] )
        # self.porpGmbl = rt.addGimbal( self.porpCtrl )
        # self.porpZro = rt.addGrp( self.porpCtrl )
        # self.porpPars = rt.addGrp( self.porpCtrl , 'Pars' )
        # self.porpZro.snap( sel )

        mc.parentConstraint( self.offsetCtrl , pars , mo = False )
        mc.scaleConstraint( self.offsetCtrl , pars , mo = False )
        # mc.parent( self.porpZro , self.ctrlGrp )
        mc.parent( sel , pars , r=True)

        mc.select( cl = True )

class PropPv( Run ):
    
    def __init__( self  , size = 1 ):
        
        sels = mc.ls( sl = True )
        if not sels:
            return
        sel = sels[0]

        super( PropPv , self ).__init__(  size )

        # col = [ 'pink' , 'cyan' , 'red' , 'green' , 'blue' , 'brown' ]

        self.pvGrp = cn.createNode( 'transform' , 'MdGeo_Grp' )
        self.scaleGrp = cn.createNode( 'transform' , 'SclGeo_Grp' )
        mc.parent( self.pvGrp , self.geoGrp )
        mc.parent(self.scaleGrp,self.pvGrp)
        # nameSplit = sel.split('_')
        # propName = '_'.join( nameSplit[0:-1] )

        # self.porpCtrl = rt.createCtrl( '%s_Ctrl' %propName , 'square' , col[i%6] )
        # self.porpGmbl = rt.addGimbal( self.porpCtrl )
        # self.porpZro = rt.addGrp( self.porpCtrl )
        # self.porpPars = rt.addGrp( self.porpCtrl , 'Pars' )
        # self.porpZro.snap( sel )
        self.scaleCtrl = ctrlRig.Run( name       = 'ScaleAdjust' ,
                         tmpJnt     = '' ,
                         side       = '' ,
                         shape      = 'circle' ,
                         jnt_ctrl   = 0,
                         jnt        = 0,
                         gimbal     = 0 ,
                         size       = size ,
                         drv        = 0,
                         inv        = 0,
                         color      = 'cyan',)

        mc.parent(self.scaleCtrl.zro, self.offsetCtrl)
        mc.parentConstraint( self.offsetCtrl , self.pvGrp , mo = False )
        mc.scaleConstraint( self.offsetCtrl , self.pvGrp , mo = False )
        mc.scaleConstraint( self.scaleCtrl.ctrl , self.scaleGrp , mo = False )
        # mc.parent( self.porpZro , self.ctrlGrp )
        mc.parent( sel , self.scaleGrp , r=True)

        mc.select( cl = True )        