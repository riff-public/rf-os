import maya.cmds as mc
import sys
import core
import rigTools as rt
import fkRig
reload(core)
reload(rt)
reload(fkRig)

cn = core.Node()

class Teet(object):
	def __init__(	self,
					mainTmpLoc 		= 	"",
					cntTmpLoc 		= 	"",
					lftTmpLoc 		= 	"",
					rgtTmpLoc 		= 	"",
					lftMidTmpLoc 	= 	"",
					rgtMidTmpLoc 	= 	"",
					elem			= 	"" ,
					size			=	1):

		## create group
		self.ttCtrlGrp = cn.createNode("transform", "{}TtRigCtrl_Grp".format(elem))
		self.ttSkinGrp = cn.createNode("transform", "{}TtRigSkin_Grp".format(elem))
		self.ttJntGrp = cn.createNode("transform", "{}TtRigJnt_Grp".format(elem))
		self.ttStillGrp = cn.createNode("transform", "{}TtRigStill_Grp".format(elem))

		self.ttMainJntGrp = cn.createNode("transform", "{}TtRigMainJnt_Grp".format(elem))
		self.ttMainJntZro = rt.addGrp(self.ttMainJntGrp)
		self.ttMainJntZro.snap(mainTmpLoc)
		self.ttMainJntZro.parent(self.ttJntGrp)

		## create joint
		self.cntJnt = cn.joint("Teeth{}MidTtRig_Jnt".format(elem), cntTmpLoc)
		self.midLJnt = cn.joint("Teeth{}MidTtRig_L_Jnt".format(elem), lftMidTmpLoc)
		self.midRJnt = cn.joint("Teeth{}MidTtRig_R_Jnt".format(elem), rgtMidTmpLoc)
		self.inLJnt = cn.joint("Teeth{}InTtRig_L_Jnt".format(elem), lftTmpLoc)
		self.inRJnt = cn.joint("Teeth{}InTtRig_R_Jnt".format(elem), rgtTmpLoc)

		self.cntJntZro = rt.addGrp(self.cntJnt, "JntZro")
		self.midLJntZro = rt.addGrp(self.midLJnt, "JntZro")
		self.midRJntZro = rt.addGrp(self.midRJnt, "JntZro")
		self.inLJntZro = rt.addGrp(self.inLJnt, "JntZro")
		self.inRJntZro = rt.addGrp(self.inRJnt, "JntZro")

		## create controller
		self.mainCtrl = rt.createCtrl('Teeth{}MainTtRig_Ctrl'.format(elem), "stick", "yellow", traget=mainTmpLoc)
		self.mainCtrlZro = rt.addGrp(self.mainCtrl)
		self.mainCtrlZro.parent(self.ttCtrlGrp)

		for attr in ("trs"):
			self.mainCtrl.attr(attr) >> self.ttMainJntGrp.attr(attr)

		self.cntCtrl = rt.createCtrl("Teeth{}MidTtRig_Ctrl".format(elem), "cube", "yellow", traget=cntTmpLoc)
		self.midLCtrl = rt.createCtrl("Teeth{}MidTtRig_L_Ctrl".format(elem), "cube", "red", traget=lftMidTmpLoc)
		self.midRCtrl = rt.createCtrl("Teeth{}MidTtRig_R_Ctrl".format(elem), "cube", "red", traget=rgtMidTmpLoc)
		self.inLCtrl = rt.createCtrl("Teeth{}InTtRig_L_Ctrl".format(elem), "cube", "blue", traget=lftTmpLoc)
		self.inRCtrl = rt.createCtrl("Teeth{}InTtRig_R_Ctrl".format(elem), "cube", "blue", traget=rgtTmpLoc)

		self.cntCtrlZro = rt.addGrp(self.cntCtrl, "CtrlZro")
		self.midLCtrlZro = rt.addGrp(self.midLCtrl, "CtrlZro")
		self.midRCtrlZro = rt.addGrp(self.midRCtrl, "CtrlZro")
		self.inLCtrlZro = rt.addGrp(self.inLCtrl, "CtrlZro")
		self.inRCtrlZro = rt.addGrp(self.inRCtrl, "CtrlZro")

		## list setup
		allJnt = [self.inLJnt, self.midLJnt, self.cntJnt, self.midRJnt, self.inRJnt]
		allJntZro = [self.inLJntZro, self.midLJntZro, self.cntJntZro, self.midRJntZro, self.inRJntZro]
		allCtrl = [self.inLCtrl, self.midLCtrl, self.cntCtrl, self.midRCtrl, self.inRCtrl]
		allCtrlZro = [self.inLCtrlZro, self.midLCtrlZro, self.cntCtrlZro, self.midRCtrlZro, self.inRCtrlZro]

		## create nurb
		crv = rt.drawCuvOnJnt("Teeth{}TtRig_Crv".format(elem), allJnt)
		crv1 = mc.offsetCurve(crv.name, d=0.01, ugn=True, ch=False)
		crv2 = mc.offsetCurve(crv.name, d=-0.01, ugn=True, ch=False)

		ttNrb = mc.loft(crv1, crv2, n="Teeth{}TtRig_Nrb".format(elem), d=3, ch=False)[0]
		self.ttNrb = core.Dag(ttNrb)
		self.ttNrb.parent(self.ttStillGrp)
		mc.delete(crv, crv1, crv2)

		## skin
		skin = mc.skinCluster(	self.cntJnt,
								self.midLJnt,
								self.midRJnt,
								self.inLJnt,
								self.inRJnt,
								ttNrb, dr=7, mi=2, tsb=True)[0]

		mc.skinPercent(skin, "{}.cv[0:3][0]".format(ttNrb), tv=[self.inLJnt, 1])
		mc.skinPercent(skin, "{}.cv[0:3][1]".format(ttNrb), tv=[(self.inLJnt, 0.8), (self.midLJnt, 0.2)])
		mc.skinPercent(skin, "{}.cv[0:3][2]".format(ttNrb), tv=[self.midLJnt, 1])
		mc.skinPercent(skin, "{}.cv[0:3][3]".format(ttNrb), tv=[self.cntJnt, 1])
		mc.skinPercent(skin, "{}.cv[0:3][4]".format(ttNrb), tv=[self.midRJnt, 1])
		mc.skinPercent(skin, "{}.cv[0:3][5]".format(ttNrb), tv=[(self.inRJnt, 0.8), (self.midRJnt, 0.2)])
		mc.skinPercent(skin, "{}.cv[0:3][6]".format(ttNrb), tv=[self.inRJnt, 1])

		## create skin joint and controller
		no = 9
		for num in range(no):
			## create follicle
			fol = rt.createFollicle(self.ttNrb, "Teeth{}{}TtRig".format(elem, num+1), side="", pru=0.5, prv=float(num)/(no-1))
			fol.parent(self.ttStillGrp)

			## create skin joint
			skinJnt = cn.joint("Teeth{}{}TtRig_Jnt".format(elem, num+1), target=fol)
			skinJntZro = rt.addGrp(skinJnt)

			skinJnt.attr("radi").v = 0.01
			skinJntZro.parent(self.ttSkinGrp)
			mc.pointConstraint(fol, skinJntZro, mo=True)
			mc.orientConstraint(fol, skinJntZro, mo=True)

			self.mainCtrl.attr("s") >> skinJnt.attr("s")

		## clean
		for jnt, jntZro, ctrl, ctrlZro in zip(allJnt, allJntZro, allCtrl, allCtrlZro):
			jnt.attr("radi").v = 0.01
			jntZro.parent(self.ttMainJntGrp)

			ctrl.lockHideAttrs("rx", "ry", "rz", "sx", "sy", "sz", "v")
			ctrlZro.parent(self.mainCtrl)

			ctrl.attr('t') >> jnt.attr('t')

		for obj in [self.mainCtrl, self.cntCtrl, self.midLCtrl, self.midRCtrl, self.inLCtrl, self.inRCtrl]:
			obj.scaleShape(size)

class Tongue(object):
	def __init__(	self,
					jawPostLoc 			=	"JawPost_TmpLoc",
					tngTmpLocLs			=	[
												"Tongue1_TmpJnt",
												"Tongue2_TmpJnt",
												"Tongue3_TmpJnt",
												"Tongue4_TmpJnt",
												"Tongue5_TmpJnt",
												"Tongue6_TmpJnt"
											],
					elem				=	"",
					size				=	1):

		## create group
		self.tngSkinGrp = cn.createNode("transform", "Tongue{}TtRigSkin_Grp".format(elem))
		self.tngCtrlGrp = cn.createNode("transform", "Tongue{}TtRigCtrl_Grp".format(elem))

		## create joint
		self.tngJnt = []
		self.tngSclJnt = []
		self.tngJntGrp = []

		for num, tmp in enumerate(tngTmpLocLs):
			jnt = cn.joint("Tongue{}{}TtRig_Jnt".format(elem, num+1))
			jntScl = cn.joint("Tongue{}Scl{}TtRig_Jnt".format(elem, num+1))
			jntGrp = rt.addGrp(jnt, "JntZro")

			jntScl.parent(jnt)
			jntGrp.snap(tmp)
			jnt.attr("radi").v = 0.01
			jntScl.attr("radi").v = 0.05

			self.tngJnt.append(jnt)
			self.tngSclJnt.append(jntScl)
			self.tngJntGrp.append(jntGrp)

			if num != 0:
				self.tngJntGrp[num].parent(self.tngJnt[num-1])

		self.tngJntGrp[0].parent(self.tngSkinGrp)

		## create controller
		self.tngCtrl = []
		self.tngCtrlZro = []

		num = 0
		for jnt, scl in zip(self.tngJnt, self.tngSclJnt):
			ctrl = rt.createCtrl('Tongue{}{}TtRig_Ctrl'.format(elem, num+1), "circle", "pink")
			ctrlGrp = rt.addGrp(ctrl, "CtrlZro")

			ctrl.scaleShape(size)
			ctrlGrp.snap(jnt)
			ctrlGrp.parent(self.tngCtrlGrp)

			self.tngCtrl.append(ctrl)
			self.tngCtrlZro.append(ctrlGrp)

			if num != 0:
				mc.parentConstraint(self.tngCtrl[num-1], self.tngCtrlZro[num], mo=True)

			## connect attributes
			ctrl.attr("t") >> jnt.attr("t")
			ctrl.attr("r") >> jnt.attr("r")
			ctrl.attr("s") >> scl.attr("s")

			ctrl.lockHideAttrs("sy", "v")

			num += 1

class Run(object):
	def __init__(self,
				jawPostLoc			=	"JawPost_TmpLoc",

				mainUpTthTmpLoc		=	"MainUpTth_TmpLoc",
				cntUpTthTmpLoc		=	"CntUpTth_TmpLoc",
				lftUpTthTmpLoc		=	"InUpTth_L_TmpLoc",
				rgtUpTthTmpLoc		=	"InUpTth_R_TmpLoc",
				lftMidUpTthTmpLoc	=	"MidUpTth_L_TmpLoc",
				rgtMidUpTthTmpLoc	=	"MidUpTth_R_TmpLoc",

				mainLowTthTmpLoc	=	"MainLowTth_TmpLoc",
				cntLowTthTmpLoc		=	"CntLowTth_TmpLoc",
				lftLowTthTmpLoc		=	"InLowTth_L_TmpLoc",
				rgtLowTthTmpLoc		=	"InLowTth_R_TmpLoc",
				lftMidLowTthTmpLoc	=	"MidLowTth_L_TmpLoc",
				rgtMidLowTthTmpLoc	=	"MidLowTth_R_TmpLoc",

				tngTmpLocLs			=	[
											"Tongue1_TmpJnt",
											"Tongue2_TmpJnt",
											"Tongue3_TmpJnt",
											"Tongue4_TmpJnt",
											"Tongue5_TmpJnt",
											"Tongue6_TmpJnt"
										],
				elem				=	"",
				tongue				=	True,
				offsetCurve			=	0.1,
				size				=	0.25):

		## create group
		ctrlGrp = 'TtRig{}Ctrl_Grp'.format(elem)
		skinGrp = 'TtRig{}Skin_Grp'.format(elem)
		jntGrp = 'TtRig{}Jnt_Grp'.format(elem)
		stillGrp = 'TtRig{}Still_Grp'.format(elem)

		if not mc.objExists(ctrlGrp):
			self.ctrlGrp = cn.createNode("transform", ctrlGrp)
		else:
			self.ctrlGrp = core.Dag(ctrlGrp)

		if not mc.objExists(skinGrp):
			self.skinGrp = cn.createNode("transform", skinGrp)
		else:
			self.skinGrp = core.Dag(skinGrp)

		if not mc.objExists(jntGrp):
			self.jntGrp = cn.createNode("transform", jntGrp)
		else:
			self.jntGrp = core.Dag(jntGrp)

		if not mc.objExists(stillGrp):
			self.stillGrp = cn.createNode("transform", stillGrp)
		else:
			self.stillGrp = core.Dag(stillGrp)

		## jaw position group
		self.jawPosJntZro = cn.createNode("transform", "JawPosi{}TtRigJntZro_Grp".format(elem))
		self.jawPosJntGrp = cn.createNode("transform", "JawPosi{}TtRigJnt_Grp".format(elem))
		self.jawPosJntGrp.snap(jawPostLoc)
		self.jawPosJntZro.parent(self.jawPosJntGrp)
		self.jawPosJntGrp.parent(self.jntGrp)

		self.jawPosSkinZro = cn.createNode("transform", "JawPosi{}TtRigSkinZro_Grp".format(elem))
		self.jawPosSkinGrp = cn.createNode("transform", "JawPosi{}TtRigSkin_Grp".format(elem))
		self.jawPosSkinZro.parent(self.jawPosSkinGrp)
		self.jawPosSkinGrp.parent(self.skinGrp)

		self.jawPosCtrlZro = cn.createNode("transform", "JawPosi{}TtRigCtrlZro_Grp".format(elem))
		self.jawPosCtrlGrp = cn.createNode("transform", "JawPosi{}TtRigCtrl_Grp".format(elem))
		self.jawPosCtrlGrp.snap(jawPostLoc)
		self.jawPosCtrlZro.parent(self.jawPosCtrlGrp)
		self.jawPosCtrlGrp.parent(self.ctrlGrp)

		for attr in ("t", "r", "s"):
			self.jawPosCtrlZro.attr(attr) >> self.jawPosJntZro.attr(attr)
			self.jawPosCtrlZro.attr(attr) >> self.jawPosSkinZro.attr(attr)

		## teeth variable
		self.ttCtrlGrp = ""
		self.ttSkinGrp = ""
		self.ttJntGrp = ""
		self.ttStillGrp = ""

		self.ttMainJntGrp = ""
		self.ttMainJntZro = ""

		self.ttMainCtrlGrp = ""
		self.ttMainCtrlZro = ""

		self.cntJnt = ""
		self.midLJnt = ""
		self.midRJnt = ""
		self.inLJnt = ""
		self.inRJnt = ""

		self.cntJntZro = ""
		self.midLJntZro = ""
		self.midRJntZro = ""
		self.inLJntZro = ""
		self.inRJntZro = ""

		self.cntCtrl = ""
		self.midLCtrl = ""
		self.midRCtrl = ""
		self.inLCtrl = ""
		self.inRCtrl = ""

		self.cntCtrlZro = ""
		self.midLCtrlZro = ""
		self.midRCtrlZro = ""
		self.inLCtrlZro = ""
		self.inRCtrlZro = ""

		self.fol = []
		self.skinJnt = []
		self.skinJntZro = []

		## run teeth rig
		self.ttUp = Teet(	mainTmpLoc 		= 	mainUpTthTmpLoc,
							cntTmpLoc 		= 	cntUpTthTmpLoc,
							lftTmpLoc 		= 	lftUpTthTmpLoc,
							rgtTmpLoc 		= 	rgtUpTthTmpLoc,
							lftMidTmpLoc 	= 	lftMidUpTthTmpLoc,
							rgtMidTmpLoc 	= 	rgtMidUpTthTmpLoc,
							elem			= 	"Up" + elem ,
							size			=	size)

		self.ttUp.ttSkinGrp.parent(self.skinGrp)
		self.ttUp.ttCtrlGrp.parent(self.ctrlGrp)
		self.ttUp.ttJntGrp.parent(self.jntGrp)
		self.ttUp.ttStillGrp.parent(self.stillGrp)

		self.ttLow = Teet(	mainTmpLoc 		= 	mainLowTthTmpLoc,
							cntTmpLoc 		= 	cntLowTthTmpLoc,
							lftTmpLoc 		= 	lftLowTthTmpLoc,
							rgtTmpLoc 		= 	rgtLowTthTmpLoc,
							lftMidTmpLoc 	= 	lftMidLowTthTmpLoc,
							rgtMidTmpLoc 	= 	rgtMidLowTthTmpLoc,
							elem			= 	"Low" + elem ,
							size			=	size)

		self.ttLow.ttSkinGrp.parent(self.skinGrp)
		self.ttLow.ttCtrlGrp.parent(self.jawPosCtrlZro)
		self.ttLow.ttJntGrp.parent(self.jawPosJntZro)
		self.ttLow.ttStillGrp.parent(self.stillGrp)

		## run tongue rig
		if tongue == True:
			self.tng = Tongue(	jawPostLoc 	= 	jawPostLoc,
								tngTmpLocLs	=	tngTmpLocLs,
								elem		=	elem,
								size		=	size )

			self.tng.tngSkinGrp.parent(self.jawPosSkinZro)
			self.tng.tngCtrlGrp.parent(self.jawPosCtrlZro)