import maya.cmds as mc
import pymel.core as pm
import maya.mel as mm
from functools import partial
import core
reload( core )
import rigTools as rt
reload( rt )
from rf_utils.context import context_info
reload(context_info)
from rf_utils import file_utils
reload(file_utils)

class Run(object):
    def __init__(self):
        self.winName = 'assemblerFclRigUi'

    def show(self , *args):
        # check if window exists
        if mc.window ( self.winName , exists = True ) :
            mc.deleteUI ( self.winName ) 
        else : pass

        winWidth = 700

        FclRigGeoList = []
        BodyRigGeoList = []
        pm.window(self.winName,title = 'FclRig Assembler' , width = winWidth , rtf = True, mxb = False , s=False)

        mainLayout = pm.columnLayout()
        winLayout = pm.rowColumnLayout( w = winWidth , p = mainLayout , nc = 9 , 
                                        cw = [  (1, winWidth*0.004),(2, winWidth*0.246),
                                                (3, winWidth*0.004),(4, winWidth*0.246),
                                                (5, winWidth*0.004),(6, winWidth*0.246),
                                                (7, winWidth*0.004),(8, winWidth*0.246),
                                                (9, winWidth*0.004)])

        pm.rowColumnLayout( p = winLayout , nc = 1 )
        bodyFclRigLayout = pm.rowColumnLayout( p = winLayout , nc = 1 , cw=(1 , winWidth*0.246) )
        pm.rowColumnLayout( p = winLayout , nc = 1 )
        bodyBodyRigLayout = pm.rowColumnLayout( p = winLayout , nc = 1 , cw=(1 , winWidth*0.246))
        pm.rowColumnLayout( p = winLayout , nc = 1 )
        allFclRigLayout = pm.rowColumnLayout( p = winLayout , nc = 1 , cw=(1 , winWidth*0.246))
        pm.rowColumnLayout( p = winLayout , nc = 1 )
        allBodyRigLayout = pm.rowColumnLayout( p = winLayout , nc = 1 , cw=(1 , winWidth*0.246))
        pm.rowColumnLayout( p = winLayout , nc = 1 )

        with bodyFclRigLayout:
            pm.text(l='FclRig : bodyGeo' , al='center')
            bodyFclRigScrollRow = pm.rowColumnLayout(  nc = 1  )
            with bodyFclRigScrollRow:
                pm.textScrollList(  'fclRigBodyGeoScroll' ,  
                                    numberOfRows=8, 
                                    allowMultiSelection=True,
                                    showIndexedItem=4, 
                                    h=200)

            bodyFclRigButton1Row = pm.rowColumnLayout(  nc = 3  , cw=[(1,winWidth*0.12),(2,winWidth*0.004),(3,winWidth*0.12)])
            with bodyFclRigButton1Row:
                pm.button('addFclRigBodyGeo'  ,label = 'Add' , h = 20 , c = partial(self.addToDisplayGeo,'fclRigBodyGeoScroll'))
                pm.text(l='')
                pm.button('delFclRigBodyGeo'  ,label = 'Del' , h = 20 , c = partial(self.removeGeo,'fclRigBodyGeoScroll'))

            bodyFclRigButton2Row = pm.rowColumnLayout(  nc = 3  , cw=[(1,winWidth*0.12),(2,winWidth*0.004),(3,winWidth*0.12)])
            with bodyFclRigButton2Row:
                pm.button('upFclRigBodyGeo'  ,label = 'Up' , h = 20 , c = partial(self.addToDisplayGeo,'fclRigBodyGeoScroll'))
                pm.text(l='')
                pm.button('downFclRigBodyGeo'  ,label = 'Down' , h = 20 , c = partial(self.removeGeo,'fclRigBodyGeoScroll'))

            bodyFclRigButton3Row = pm.rowColumnLayout(  nc = 1 , cw=[(1,winWidth*0.246)])
            with bodyFclRigButton3Row:
                pm.button('ClearFclRigBodyGeo'  ,label = 'Clear' , h = 20 , c = partial(self.clearScroll,'fclRigBodyGeoScroll'))


        with bodyBodyRigLayout:
            pm.text(l='BodyRig : bodyGeo' , al='center')
            bodyBodyRigScrollRow = pm.rowColumnLayout(  nc = 1  )
            with bodyBodyRigScrollRow:
                pm.textScrollList(  'bodyRigBodyGeoScroll' ,  
                                    numberOfRows=8, 
                                    allowMultiSelection=True,
                                    showIndexedItem=4, 
                                    h=200)

            bodyBodyRigButton1Row = pm.rowColumnLayout(  nc = 3  , cw=[(1,winWidth*0.12),(2,winWidth*0.004),(3,winWidth*0.12)])
            with bodyBodyRigButton1Row:
                pm.button('addBodyRigBodyGeo'  ,label = 'Add' , h = 20 , c = partial(self.addToDisplayGeo,'bodyRigBodyGeoScroll'))
                pm.text(l='')
                pm.button('delBodyRigBodyGeo'  ,label = 'Del' , h = 20 , c = partial(self.removeGeo,'bodyRigBodyGeoScroll'))

            bodyBodyRigButton2Row = pm.rowColumnLayout(  nc = 3  , cw=[(1,winWidth*0.12),(2,winWidth*0.004),(3,winWidth*0.12)])
            with bodyBodyRigButton2Row:
                pm.button('upBodyRigBodyGeo'  ,label = 'Up' , h = 20 , c = partial(self.addToDisplayGeo,'bodyRigBodyGeoScroll'))
                pm.text(l='')
                pm.button('downBodyRigBodyGeo'  ,label = 'Down' , h = 20 , c = partial(self.removeGeo,'bodyRigBodyGeoScroll'))

            bodyBodyRigButton3Row = pm.rowColumnLayout(  nc = 1 , cw=[(1,winWidth*0.246)])
            with bodyBodyRigButton3Row:
                pm.button('ClearBodyRigBodyGeo'  ,label = 'Clear' , h = 20 , c = partial(self.clearScroll,'bodyRigBodyGeoScroll'))


        with allFclRigLayout:
            pm.text(l='FclRig : allGeo' , al='center')
            allFclRigScrollRow = pm.rowColumnLayout(  nc = 1  )
            with allFclRigScrollRow:
                pm.textScrollList(  'fclRigAllGeoScroll' ,  
                                    numberOfRows=8, 
                                    allowMultiSelection=True,
                                    showIndexedItem=4, 
                                    h=200)

            allFclRigButton1Row = pm.rowColumnLayout(  nc = 3  , cw=[(1,winWidth*0.12),(2,winWidth*0.004),(3,winWidth*0.12)])
            with allFclRigButton1Row:
                pm.button('addFclRigAllGeo'  ,label = 'Add' , h = 20 , c = partial(self.addToDisplayGeo,'fclRigAllGeoScroll'))
                pm.text(l='')
                pm.button('delFclRigAllGeo'  ,label = 'Del' , h = 20 , c = partial(self.removeGeo,'fclRigAllGeoScroll'))

            allFclRigButton2Row = pm.rowColumnLayout(  nc = 3  , cw=[(1,winWidth*0.12),(2,winWidth*0.004),(3,winWidth*0.12)])
            with allFclRigButton2Row:
                pm.button('upFclRigAllGeo'  ,label = 'Up' , h = 20 , c = partial(self.addToDisplayGeo,'fclRigAllGeoScroll'))
                pm.text(l='')
                pm.button('downFclRigAllGeo'  ,label = 'Down' , h = 20 , c = partial(self.removeGeo,'fclRigAllGeoScroll'))

            allFclRigButton3Row = pm.rowColumnLayout(  nc = 1 , cw=[(1,winWidth*0.246)])
            with allFclRigButton3Row:
                pm.button('ClearFclRigAllGeo'  ,label = 'Clear' , h = 20 , c = partial(self.clearScroll,'fclRigAllGeoScroll'))


        with allBodyRigLayout:
            pm.text(l='BodyRig : allGeo' , al='center')
            allBodyRigScrollRow = pm.rowColumnLayout(  nc = 1  )
            with allBodyRigScrollRow:
                pm.textScrollList(  'bodyRigAllGeoScroll' ,  
                                    numberOfRows=8, 
                                    allowMultiSelection=True,
                                    showIndexedItem=4, 
                                    h=200)

            allBodyRigButton1Row = pm.rowColumnLayout(  nc = 3  , cw=[(1,winWidth*0.12),(2,winWidth*0.004),(3,winWidth*0.12)])
            with allBodyRigButton1Row:
                pm.button('addBodyRigAllGeo'  ,label = 'Add' , h = 20 , c = partial(self.addToDisplayGeo,'bodyRigAllGeoScroll'))
                pm.text(l='')
                pm.button('delBodyRigAllGeo'  ,label = 'Del' , h = 20 , c = partial(self.removeGeo,'bodyRigAllGeoScroll'))

            allBodyRigButton2Row = pm.rowColumnLayout(  nc = 3  , cw=[(1,winWidth*0.12),(2,winWidth*0.004),(3,winWidth*0.12)])
            with allBodyRigButton2Row:
                pm.button('upBodyRigAllGeo'  ,label = 'Up' , h = 20 , c = partial(self.addToDisplayGeo,'bodyRigAllGeoScroll'))
                pm.text(l='')
                pm.button('downBodyRigAllGeo'  ,label = 'Down' , h = 20 , c = partial(self.removeGeo,'bodyRigAllGeoScroll'))

            allBodyRigButton3Row = pm.rowColumnLayout(  nc = 1 , cw=[(1,winWidth*0.246)])
            with allBodyRigButton3Row:
                pm.button('ClearBodyRigAllGeo'  ,label = 'Clear' , h = 20 , c = partial(self.clearScroll,'bodyRigAllGeoScroll'))


        loadLayout = pm.rowColumnLayout( w = winWidth , p = mainLayout , nc = 3 , cw = [  (1, winWidth*0.006),(2, winWidth*0.99),(3, winWidth*0.006)])
        with loadLayout:
            pm.separator(vis = False)
            pm.button('autoLoadAllGeo'  ,label = 'Auto Load All Geo to Scroll' , h = 30 , c = self.autoloadAllGeo)


        pm.rowColumnLayout( w = winWidth , p = mainLayout , nc = 1 )

        writeReadLayout = pm.rowColumnLayout( w = winWidth , p = mainLayout , nc = 5 , cw = [  (1, winWidth*0.006),(2, winWidth*0.49),(3, winWidth*0.006),(4, winWidth*0.49),(3, winWidth*0.006)])
        with writeReadLayout:
            pm.separator(vis = False)
            pm.button('writeAssembleGeo'  ,label = 'Write Assembled Geo' , h = 30 , c = self.writeAsbGeo)
            pm.separator(vis = False)
            pm.button('readAssembleGeo'  ,label = 'Read Assembled Geo' , h = 30 , c = self.readAsbGeo)


        pm.rowColumnLayout( w = winWidth , p = mainLayout , nc = 1 )



        checkBoxLayout = pm.rowColumnLayout( w = winWidth , p = mainLayout , nc = 3 , cw = [  (1, winWidth*0.45),(2, winWidth*0.1),(3, winWidth*0.45)])
        with checkBoxLayout:
            pm.separator(vis = False)
            pm.checkBox('wrapGeo' , l='Wrap Geo' , v=True , al='center')


        runLayout = pm.rowColumnLayout( w = winWidth , p = mainLayout , nc = 3 , cw = [  (1, winWidth*0.006),(2, winWidth*0.99),(3, winWidth*0.006)])
        with runLayout:
            pm.separator(vis = False)
            pm.button('fclRigAssemble'  ,label = 'FclRig Assemble' , h = 60 , c = self.assembleFclRig)

        mc.showWindow()

    def test(self , *args):
        return

    def addToDisplayGeo(self,scrollName,*args):
        geoList = mc.textScrollList(scrollName,q=1,ai=1)
        sel = mc.ls(sl=True)
        for each in sel:
            if not geoList:
                mc.textScrollList(scrollName ,edit=True,append=['{}'.format(each)])
            else:
                if not each in mc.textScrollList(scrollName,q=1,ai=1):
                    mc.textScrollList(scrollName ,edit=True,append=['{}'.format(each)])

    def removeGeo(self,scrollName , *args):
        List = mc.textScrollList( scrollName ,q=True, si=True)
        for each in List:
            mc.textScrollList(scrollName ,edit=True , ri='{}'.format(each))

    def autoloadAllGeo(self , *args):
        mc.textScrollList('fclRigBodyGeoScroll' ,edit=True,ra=True) 
        mc.textScrollList('bodyRigBodyGeoScroll' ,edit=True,ra=True) 
        mc.textScrollList('fclRigAllGeoScroll' ,edit=True,ra=True) 
        mc.textScrollList('bodyRigAllGeoScroll' ,edit=True,ra=True) 
        
        fclGeoGrp = mc.ls('*:FclRigGeo_Grp')[0]
        fclBodyGeo = mc.ls('*:BodyFclRig_Geo')[0]
        bodyGeo = mc.ls('*:Body_Geo')[0]
        wnkRig = mc.ls('*:WnkRigGeo_Grp')
        fclGeoShape = mc.listRelatives(fclGeoGrp , ad=True , type='mesh')
        fclGeos = []

        if wnkRig:
            wnkGeoShape = mc.listRelatives(wnkRig , ad=True , type='mesh')
            for each in wnkGeoShape:
                fclGeoShape.append(each)

        for each in fclGeoShape:
            pr = mc.listRelatives(each , p=True)[0]
            if pr != fclBodyGeo:
                if not pr in fclGeos:
                    fclGeos.append(pr)
        bodyGeos = []
        for each in fclGeos:
            fullName = each.split(':')[-1]
            if 'FclRig' in fullName:
                name = fullName.split('FclRig')
            elif 'WnkRig' in fullName:
                name = fullName.split('WnkRig')
            geo = mc.ls('*:{}{}'.format(name[0],name[-1]))[0]
            bodyGeos.append(geo)


        mc.select(cl = True)
        mc.select(fclBodyGeo)
        self.addToDisplayGeo('fclRigBodyGeoScroll')

        mc.select(cl = True)
        mc.select(bodyGeo)
        self.addToDisplayGeo('bodyRigBodyGeoScroll')

        mc.select(cl = True)
        mc.select(fclGeos)
        self.addToDisplayGeo('fclRigAllGeoScroll')

        mc.select(cl = True)
        mc.select(bodyGeos)
        self.addToDisplayGeo('bodyRigAllGeoScroll')
        mc.select(cl = True)

    def getDataPath(self , *args):
        context = context_info.ContextPathInfo()
        projectPath = os.environ['RFPROJECT']
        fileWorkPath = (context.path.name()).replace('$RFPROJECT',projectPath)
        path = fileWorkPath + '/rig/rigData'
        if not os.path.isdir(path):
            os.mkdir(path)
        return path

    def writeAsbGeo(self, *args):
        geoList = {}
        geoList['fclBodyGeoList'] = pm.textScrollList('fclRigBodyGeoScroll', q = 1, ai =1)
        geoList['bodyRigBodyGeoList'] = pm.textScrollList('bodyRigBodyGeoScroll', q = 1, ai =1)
        geoList['fclAllGeoList'] = pm.textScrollList('fclRigAllGeoScroll', q = 1, ai =1)
        geoList['bodyRigAllGeoList'] = pm.textScrollList('bodyRigAllGeoScroll', q = 1, ai =1)

        path = self.getDataPath()
        file_utils.ymlDumper("{}/AssembleListGeo.yml".format(path), geoList) 

    def readAsbGeo(self, *args):
        mc.textScrollList('fclRigBodyGeoScroll' ,edit=True,ra=True) 
        mc.textScrollList('bodyRigBodyGeoScroll' ,edit=True,ra=True) 
        mc.textScrollList('fclRigAllGeoScroll' ,edit=True,ra=True) 
        mc.textScrollList('bodyRigAllGeoScroll' ,edit=True,ra=True) 

        path = self.getDataPath()
        geoList = file_utils.ymlLoader("{}/AssembleListGeo.yml".format(path)) 

        mc.select(cl = True)
        mc.select(geoList['fclBodyGeoList'])
        self.addToDisplayGeo('fclRigBodyGeoScroll')

        mc.select(cl = True)
        mc.select(geoList['bodyRigBodyGeoList'])
        self.addToDisplayGeo('bodyRigBodyGeoScroll')

        mc.select(cl = True)
        mc.select(geoList['fclAllGeoList'])
        self.addToDisplayGeo('fclRigAllGeoScroll')

        mc.select(cl = True)
        mc.select(geoList['bodyRigAllGeoList'])
        self.addToDisplayGeo('bodyRigAllGeoScroll')
        mc.select(cl = True)


    def clearScroll(self , scrollName , *args):
        mc.textScrollList(scrollName ,edit=True,ra=True) 

    def upScroll(self,scrollName , *args):
        intList = mc.textScrollList(scrollName,q=1,sii=1)
        strList = mc.textScrollList(scrollName,q=1,si=1)
        selList = []
        for num in range(len(strList)):
            idx = intList[num]
            if 1 not  in intList:
                if idx  >= 2:
                    mc.textScrollList(scrollName,e=1,rii=idx)
                    mc.textScrollList(scrollName,e=1,ap=[idx-1,strList[num]])
                    selList.append(idx-1)

                else:
                    selList.append(idx)
                    pass

                mc.textScrollList(scrollName,e=1,sii=selList)
            else:
                mc.textScrollList(scrollName,e=1,sii = intList)

    def downScroll(self,scrollName,*args):
            intList = mc.textScrollList(scrollName,q=1,sii=1)
            strList = mc.textScrollList(scrollName,q=1,si=1)
            maxNum = mc.textScrollList(scrollName,q=1,ni=1)
            selList = []

            for num in range(len(strList))[::-1]:
                idx = intList[num]
                if maxNum not in intList:
                    if idx  <= maxNum-1:
                        #print 'idx : {} < strList : {}'.format(idx+1,strList[num])
                        mc.textScrollList(scrollName,e=1,rii=idx)
                        mc.textScrollList(scrollName,e=1,ap=[idx+1,strList[num]])
                        selList.append(idx+1)
                    else:
                        #print 'there something in this case : idx: {} and strList :{}'.format(idx+1,strList[num])
                        selList.append(idx)

                    mc.textScrollList(scrollName,e=1,sii=selList)
                else:
                    mc.textScrollList(scrollName,e=1,sii = intList)

    def assembleFclRig(self,*args):
        import fclRigTools as fclRig 
        reload(fclRig)
        checkWrap = pm.checkBox('wrapGeo' , q=True , v=True)
        fclGeoList = mc.textScrollList('fclRigAllGeoScroll',q=1,ai=1)
        bodyGeoList = mc.textScrollList('bodyRigAllGeoScroll',q=1,ai=1)
        fclRig.RunAssemble(wrapGeo = checkWrap , fclGeoList = fclGeoList ,bodyGeoList = bodyGeoList)