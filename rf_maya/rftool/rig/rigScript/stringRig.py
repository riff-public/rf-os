import maya.cmds as mc

import core
reload( core )
import rigTools as rt
reload( rt )
import ctrlRig
reload( ctrlRig )

cn = core.Node()

class Run(object):
	def __init__(   self ,
					name				= '' ,
					jnt 				= [] ,
					geo					= '' ,
					side 				= '' ,
					color				= 'red',
					spans				= 100 ,
					shape				= 'cube',
					isNoiseAttr			= True ,
					noiseAxis 			= 'tx',
					newNoiseCtrl		= True ,
					newNoiseAxis		= 'tx',
					valueCv				= [] , 
					size				= 1
				):

		if side:
			isSide = "_{}_".format(side)
		else:
			isSide = "_"

		clstJnt = core.Dag(jnt[2])

		## Create Control at cluster
		ctrl = ctrlRig.Run(	name		= name ,
							tmpJnt		= jnt[2] ,
							side 		= side ,
							shape		= shape ,
							jnt 		= 0 ,
							gimbal		= 0 ,
							size		= size ,
							color		= color,
						  )

		self.jnt = ctrl.obj
		self.zro = ctrl.zro
		self.ofst = ctrl.ofst
		self.pars = ctrl.pars
		self.ctrl = ctrl.ctrl
		self.ctrl.scaleShape(size)

		## Create Group
		self.ctrlGrp = cn.createNode("transform", "{}Ctrl{}Grp".format(name, isSide))
		self.jntGrp = cn.createNode("transform", "{}Jnt{}Grp".format(name, isSide)) 
		self.stillGrp = cn.createNode("transform", "{}Still{}Grp".format(name, isSide))
		crvGrp = cn.createNode("transform", "{}Crv{}Grp".format(name, isSide))
		clstJntGrp = cn.createNode("transform", "{}ClstJnt{}Grp".format(name, isSide))
		noiJntGrp = cn.createNode("transform", "{}NoiJnt{}Grp".format(name, isSide))

		## Create Cluster on curve anc rebuild
		pntPosi = []
		for obj in jnt:                                
			posi = mc.xform(obj, q=True, ws=True, t=True)    
			pntPosi.append(tuple(posi))

		posiCrv = core.Dag( mc.curve(n='{}{}Crv'.format(name, isSide), d=1, p=[pntPosi[0], pntPosi[1], pntPosi[2], pntPosi[3]] ) )
		clst = mc.cluster('{}.cv[1:2]'.format(posiCrv), n='{}{}Clst'.format(name, isSide))
		mc.percent(clst[0], '{}.cv[1]'.format(posiCrv), v=0.5)
		mc.rebuildCurve(posiCrv, d=3, s=spans)

		clstJntGrp.snap(clstJnt)
		clstJnt.parent(clstJntGrp)
		mc.cluster(clst[-1], e=True, bs=True, wn=(clstJnt, clstJnt))

		self.ctrl.attr("t") >> clstJnt.attr("t")
		self.ctrl.attr("r") >> clstJnt.attr("r")
		self.ctrl.attr("s") >> clstJnt.attr("s")

		## create attribute on ctrl
		if isNoiseAttr:
			self.noiseAttrFunction(	name 	= name,
									side 	= side,
									source 	= self.ctrl,
									target 	= self.ofst,
									jnt 	= clstJnt,
									axis 	= noiseAxis
								  )

		## create noise controller
		if newNoiseCtrl:
			ctrl = ctrlRig.Run(	name		= '{}Noise'.format(name) ,
								tmpJnt		= jnt[1] ,
								side 		= side ,
								shape		= shape ,
								jnt 		= 0 ,
								gimbal		= 0 ,
								size		= size ,
								color		= color,
							  )

			self.noiJnt = ctrl.obj
			self.noiZro = ctrl.zro
			self.noiOfst = ctrl.ofst
			self.noiPars = ctrl.pars
			self.noiCtrl = ctrl.ctrl
			self.noiCtrl.scaleShape(size)

			## create noise curve
			noiCrvGrp = cn.createNode("transform", "{}NoiCrv{}Grp".format(name, isSide))
			noiCrv = core.Dag( mc.curve(n='{}Noi{}Crv'.format(name, isSide), d=1, p=[pntPosi[0], pntPosi[1], pntPosi[2], pntPosi[3]] ) )
			mc.rebuildCurve(noiCrv, d=3, s=6)
			noiCrv.parent(noiCrvGrp)

			## connect noise controllers and noise joints
			noiJntZroGrp = cn.createNode("transform", "{}NoiJntZro{}Grp".format(name, isSide))
			noiJnt = core.Dag(jnt[1])
			noiJntZroGrp.snap(noiJnt)
			noiJnt.parent(noiJntZroGrp)

			self.noiCtrl.attr("t") >> noiJnt.attr("t")
			self.noiCtrl.attr("r") >> noiJnt.attr("r")
			self.noiCtrl.attr("s") >> noiJnt.attr("s")

			## attributes
			self.noiCtrl.addAttr("geoVis", minVal=0, maxVal=1)
			self.noiCtrl.attr("geoVis").v = 1
			if geo:
				self.noiCtrl.attr("geoVis") >> core.Dag(geo).attr("v")

			if isNoiseAttr:
				self.noiseAttrFunction(	name 	= "{}Noi".format(name),
										side 	= side,
										source 	= self.noiCtrl,
										target 	= self.noiOfst,
										jnt 	= noiJnt,
										axis 	= newNoiseAxis
									  )

			## skin and wire
			skin = mc.skinCluster(jnt[0], jnt[1], jnt[3], noiCrv, n="{}Noi{}Skin".format(name, isSide), tsb=True)[0]
			wireNode = core.Dag( mc.wire(posiCrv, w=noiCrv, n='{}Noi{}Wire'.format(name, isSide))[0] )
			wireNode.attr("dds[0]").v = 1000000

			## paint skinWeight
			for num, val in enumerate(valueCv):
				mc.skinPercent(skin, '{}.cv[{}]'.format(noiCrv, num), transformValue=[(jnt[1], val*0.01)])

		## clean
		posiCrv.parent(crvGrp)	
		mc.parent(noiJntGrp, clstJntGrp, self.jntGrp)		

		self.zro.parent(self.ctrlGrp)
		crvGrp.parent(self.stillGrp)
		if newNoiseCtrl:
			self.noiZro.parent(self.ctrlGrp)
			noiCrvGrp.parent(self.stillGrp)
			mc.parent(jnt[0], noiJntZroGrp, jnt[3], noiJntGrp)

			noiCrvGrp.attr("v").v = 0
			self.noiCtrl.lockHideAttrs("sx", "sy", "sz")
		else:
			mc.parent(jnt[0], jnt[1], jnt[3], noiJntGrp)
		
		clstJntGrp.attr("v").v = 0
		self.ctrl.lockHideAttrs("rx", "ry", "rz", "sx", "sy", "sz")

		mc.delete(clst[-1])

	print ('================================== Generate is DONE ==================================')

	def noiseAttrFunction(self, name, side, source, target, jnt, axis):
		if side:
			isSide = "_{}_".format(side)
		else:
			isSide = "_"

		source.addAttr("frequency")
		source.addAttr("value")
		source.attr("value").v = 1

		time = core.Dag("time1")
		crvNoi = cn.createNode("noise", "{}Crv{}Noi".format(name, side))
		crvRem = cn.createNode("remapValue", "{}Crv{}Rem".format(name, side))
		crvMdv = cn.createNode("multiplyDivide", "{}Crv{}Mdv".format(name, side))
		crvValMdv = cn.createNode("multiplyDivide", "{}CrvVal{}Mdv".format(name, side))
		crvPma = cn.createNode("plusMinusAverage", "{}Crv{}Pma".format(name, side))

		time.attr("o") >> crvNoi.attr("ti")
		crvNoi.attr("oc") >> crvRem.attr("color[0].color_Color")
		crvRem.attr("oc") >> crvMdv.attr("i1")
		crvMdv.attr("o") >> crvValMdv.attr("i1")
		crvValMdv.attr("ox") >> target.attr("{}".format(axis))

		crvValMdv.attr("ox") >> crvPma.attr("i1[0]")
		source.attr("{}".format(axis)) >> crvPma.attr("i1[1]")
		crvPma.attr("o1") >> jnt.attr("{}".format(axis))

		source.attr("frequency") >> crvMdv.attr("i2x")
		source.attr("value") >> crvValMdv.attr("i2x")