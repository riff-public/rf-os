from collections import OrderedDict
import re
from rf_utils import file_utils


class RigNode(): 
    def __init__(self, node): 
        self.node = node 

    @property 
    def name(self): 
        return self.node.get('name')
        
    @property 
    def attr(self):
        return self.node.get('attr')
        
    @property 
    def source(self):
        return self.node.get('source')

    @property
    def target(self):
        return self.node.get('target')
        
    @property
    def pos(self):
        return self.target.get('pos' , None)

    @property
    def neg(self):
        return self.target.get('neg' , None)
        
    @property
    def limitPos(self):
        return self.target.get('limitPos' , None)

    @property
    def limitNeg(self):
        return self.target.get('limitNeg' , None)

    @property
    def transform(self):
        return self.node.get('transform' , None)

    @property
    def max(self):
        return self.transform.get('max', 0.5)

    @property
    def min(self):
        return self.transform.get('min' , -0.5)
    
    @property
    def type(self):
        return self.transform.get('type')

    @property
    def autoAttr(self):
        return self.transform.get('autoAttr' , None)
        
    @property
    def jnt(self):
        return self.transform.get('jnt' , False)


class BshNode(): 
    def __init__(self, node): 
        self.node = node

    @property
    def bshGrp(self):
        return self.node.get('bshGrp')
    
    @property
    def InbtwGrp(self):
        return self.node.get('InbtwGrp')

    @property 
    def allGrp(self): 
        return self.bshGrp + self.InbtwGrp

    @property
    def allGrpName(self):
        self.allGrpName = []
        for eachGrp in self.allGrp:
            eachGrpName = eachGrp.keys()[0]
            self.allGrpName.append(eachGrpName)
        return self.allGrpName

    @property
    def allGrpBshName(self):
        self.allGrpName = []
        for eachGrp in self.bshGrp:
            eachGrpName = eachGrp.keys()[0]
            self.allGrpName.append(eachGrpName)
        return self.allGrpName

    @property
    def allGrpInbtwName(self):
        self.allGrpName = []
        for eachGrp in self.InbtwGrp:
            eachGrpName = eachGrp.keys()[0]
            self.allGrpName.append(eachGrpName)
        return self.allGrpName


    def grpMember(self , key):
        for eachGrp in self.bshGrp:
            self.grp = eachGrp.get(key , {})
            if self.grp:
                return self.grp

    def getIndex(self , key):
        for eachInbtwGrp in self.InbtwGrp:
            grpName = eachInbtwGrp.keys()[0]
            for each in eachInbtwGrp[grpName]:
                grpMember = each.keys()[0]
                if grpMember == key:
                    return each[key].get('attrIndex')

    def getValue(self , key):
        for eachInbtwGrp in self.InbtwGrp:
            grpName = eachInbtwGrp.keys()[0]
            for each in eachInbtwGrp[grpName]:
                grpMember = each.keys()[0]
                if grpMember == key:
                    return each[key].get('value')
