import maya.cmds as mc
import maya.mel as mm
import core
import rigTools as rt
import fkRig
reload( core )
reload( rt )
reload(fkRig)




cn = core.Node()
# -------------------------------------------------------------------------------------------------------------
#
#  CLAVICLE RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , clavTmpJnt  = 'Clav_L_TmpJnt' ,
                         upArmTmpJnt = 'UpArm_L_TmpJnt' ,
                         parent      = 'Spine4Pos_jnt' ,
                         ctrlGrp     = 'Ctrl_Grp' ,
                         jntGrp      = 'Jnt_Grp' ,
                         skinGrp     = 'Skin_Grp' ,
                         elem        = '' ,
                         side        = 'L' ,
                         axis        = 'y' ,
                         size        = 1 
                 ):

        ##-- Info
        elem = elem.capitalize()

        if side == '' :
            sideRbn = ''
            side = '_'
        else :
            sideRbn = '{}'.format(side)
            side = '_{}_'.format(side)


        if 'L' in side  :
            valueSide = 1
            axisNonroll = 'y+'
        elif 'R' in side :
            valueSide = -1
            axisNonroll = 'y-'

        

        #-- Create Main Group
        self.clavJntGrp = cn.createNode( 'transform' , 'Clav{}Jnt{}Grp'.format( elem , side ))
        self.clavJntGrp.snap( parent )
        
        #-- Create Joint
        self.clav_obj = fkRig.Run(name       = 'Clav{}'.format(elem) ,
                                    side        = sideRbn ,
                                    tmpJnt      = clavTmpJnt ,
                                    ctrlGrp     = ctrlGrp , 
                                    parent      = parent , 
                                    shape       = 'dumbbell' ,
                                    jnt_ctrl    = 1 ,
                                    color       = 'red',
                                    localWorld  = 0,
                                    constraint  = 1,
                                    jnt         = 1,
                                    dtl         = 0)


        self.clavEndJnt = cn.joint( 'Clav{}End{}Jnt'.format( elem , side ) , upArmTmpJnt )
        self.clavEndJnt.parent( self.clav_obj.jnt )

        self.ctrl = self.clav_obj.ctrl
        self.gmbl = self.clav_obj.gmbl
        self.jnt = self.clav_obj.jnt

        #-- Adjust Shape Controls
        for eachCtrl in ( self.ctrl , self.gmbl ) :
            eachCtrl.scaleShape( size )

            if 'L' in side :
                eachCtrl.rotateShape( 90 , 0 ,0 )
            else :
                eachCtrl.rotateShape( -90 , 0 ,0 )
        
        #-- Adjust Rotate Order
        for obj in ( self.ctrl , self.jnt , self.clavEndJnt ) :
            obj.setRotateOrder( 'xyz' )

        #-- Rig process
        rt.addFkStretch( self.ctrl , self.clavEndJnt , axis , valueSide )
        
        clavNonRollJnt = rt.addNonRollJnt( 'Clav' , elem , self.jnt , self.clavEndJnt , parent , axisNonroll )

        self.clavNonRollJntGrp = clavNonRollJnt['jntGrp']
        self.clavNonRollRootNr = clavNonRollJnt['rootNr']
        self.clavNonRollRootNrZro = clavNonRollJnt['rootNrZro']
        self.clavNonRollEndNr = clavNonRollJnt['endNr']
        self.clavNonRollIkhNr = clavNonRollJnt['ikhNr']
        self.clavNonRollIkhNrZro = clavNonRollJnt['ikhNrZro']

        #-- Adjust Hierarchy
        mc.parent( self.clavNonRollJntGrp , self.clavJntGrp )
        mc.parent( self.clavJntGrp , jntGrp )

        #-- Cleanup
        for obj in ( self.clav_obj.ctrlGrp , self.clavJntGrp  , self.clav_obj.zro ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for obj in ( self.ctrl , self.gmbl ) :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

    mc.select( cl = True )