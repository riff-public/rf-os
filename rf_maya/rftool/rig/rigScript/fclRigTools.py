import maya.cmds as mc
import maya.mel as mm

import rigTools as rt
reload( rt )
import core
reload( core )
from rf_utils import file_utils
reload(file_utils)
from rf_utils.context import context_info



cn = core.Node()

def createMainGrp():
	Geo = 'BodyFclRig_Geo'

	ctrlGrp  = cn.createNode('transform' , 'FclRigCtrl_Grp')
	jntGrp 	 = cn.createNode('transform' , 'FclRigJnt_Grp')
	skinGrp	 = cn.createNode('transform' , 'FclRigSkin_Grp')
	stillGrp = cn.createNode('transform' , 'FclRigStill_Grp')
	rvtGrp	 = cn.createNode('transform' , 'FclRigRvt_Grp')
	mc.parent(rvtGrp,ctrlGrp)

	GeoPos = mc.duplicate(Geo, name = 'BodyFclRigPost_Geo')
	mc.parent(GeoPos,stillGrp)
	mc.parent('FclRigGeo_Grp',stillGrp)

def parentFcl(part=''):
	if part == 'HdRig':
		nsp = 'hdRig_md'
	elif part == 'MthRig':
		nsp = 'mthRig_md'
	elif part == 'TtRig':
		nsp = 'ttRig_md'
	elif part == 'DtlRig':
		nsp = 'dtlRig_md'
	elif part == 'BshRig':
		asset = context_info.ContextPathInfo()         
		char = asset.name
		nsp = '{}_md'.format(char)	
			
	elif part == 'WnkRig':
		nsp = 'wnkRig_md'	
				
				
	geoGrp   = '{}:{}Geo_Grp'.format(nsp,part)
	ctrlGrp  = '{}:{}Ctrl_Grp'.format(nsp,part)
	jntGrp   = '{}:{}Jnt_Grp'.format(nsp,part)
	skinGrp  = '{}:{}Skin_Grp'.format(nsp,part)
	stillGrp = '{}:{}Still_Grp'.format(nsp,part)

	if part != 'WnkRig':
		listGeo = []
		allGeo = mc.listRelatives(geoGrp, c= True, ad = True, typ = 'transform')
		for each in allGeo:
			if '_Geo' in each:
				listGeo.append(each)

		for geo in listGeo:
			nameGeo = geo.split(':')[1]
			name,typ = nameGeo.split(part)
			fclGeo = '{}FclRig{}'.format(name,typ)

			if mc.objExists(fclGeo):
				# Check Mactching Geo
				bbGeo = mc.xform(geo , q=True , boundingBox=True)
				bbFclGeo = mc.xform(fclGeo , q=True , boundingBox=True)

				check = []
				for i in range(len(bbGeo)):
					if bbGeo[i] == bbFclGeo[i]:
						check.append(True)
					else:
						check.append(False)
				if False in check :
					print '{} Not Matching'. format(geo)

				else:
					mc.select(geo, r = True)
					mc.select(fclGeo, add = True)
					rt.blendShape(bshGeo = geo , target = fclGeo)
					mc.select(cl=True)

			else:
				print 'No Object Name {}'.format(fclGeo)
				mc.select(cl=True)
				pass

	if part == 'DtlRig' or part == 'WnkRig':
		postGeo = 'BodyFclRigPost_Geo'
		mrkStill = mc.listRelatives(stillGrp, c = True)

		for still_grp in mrkStill:
			nrbShape = mc.listRelatives(still_grp, ad=True, typ="nurbsSurface")
			allNrb = mc.listRelatives(nrbShape, ap=True)
			for nrb in allNrb:
				mc.select(nrb, add=True)

		mc.select(postGeo, add=True)
		mc.CreateWrap()
		mc.select(cl=True)

	else: pass
		
	allObj = mc.select(ado=True)
	objInSc = mc.ls(sl = True)
	mc.select(d = True)

	if ctrlGrp in objInSc:
		mc.parent(ctrlGrp, 'FclRigCtrl_Grp')
	if jntGrp in objInSc:
		mc.parent(jntGrp, 'FclRigJnt_Grp')
	if skinGrp in objInSc:
		mc.parent(skinGrp, 'FclRigSkin_Grp')
	if stillGrp in objInSc:
		mc.parent(stillGrp, 'FclRigStill_Grp')
	if geoGrp in objInSc:
		mc.parent(geoGrp, 'FclRigStill_Grp')

	print '>>>>>>>>>> Parent {} Done <<<<<<<<<<'.format(part)


def RunAssemble(wrapGeo = True , fclGeoList = [] , bodyGeoList = []):
    #-- Info
    #---- bodyRig object
    headJnt = core.Dag(mc.ls('*:Head_Jnt')[0])
    fclMainCtrlGrp = core.Dag(mc.ls('*:FacialCtrl_Grp')[0])
    stillGrp = core.Dag(mc.ls('*:Still_Grp')[0])
    bodyGeo = core.Dag(mc.ls('*:Body_Geo')[0])
    #---- fclRig object
    fclCtrlGrp = core.Dag(mc.ls('*:FclRigCtrl_Grp')[0])
    fclJntGrp = core.Dag(mc.ls('*:FclRigJnt_Grp')[0])
    fclSkinGrp = core.Dag(mc.ls('*:FclRigSkin_Grp')[0])
    fclStillGrp = core.Dag(mc.ls('*:FclRigStill_Grp')[0])
    fclBodyGeo = core.Dag(mc.ls('*:BodyFclRig_Geo')[0])

    fclCtrlPar = cn.createNode('transform' , 'FclRigCtrlPar_Grp')

    fclStillPar = cn.createNode('transform' , 'FclRigStillPar_Grp')

    mc.parentConstraint(headJnt , fclCtrlPar , mo = 1)
    mc.scaleConstraint(headJnt , fclCtrlPar , mo = 1)

    #--Adjust Hierachy
    fclCtrlPar.parent(fclMainCtrlGrp)
    fclStillPar.parent(stillGrp)
    fclCtrlGrp.parent(fclCtrlPar)
    fclJntGrp.parent(fclStillPar)
    fclSkinGrp.parent(fclStillPar)
    fclStillGrp.parent(fclStillPar)

    context = context_info.ContextPathInfo()
    projectName = context.project
    assetName = context.name

    #-- Run from yml each project
    data = readData(local = True)
    importAndRun(data)

    #-- Check if head geo if have cutted
    bbBody = mc.xform(bodyGeo , q=True , boundingBox=True)
    bbFcl = mc.xform(fclBodyGeo , q=True , boundingBox=True)
    compareBB = []
    for i in range(len(bbBody)):
        if bbBody[i] == bbFcl[i]:
            compareBB.append(True)
        else:
            compareBB.append(False)

    if False in compareBB or wrapGeo:
        wrapGeo = mc.duplicate(bodyGeo,n='BodyFclRigWrapped_Geo')[0]
        mc.parent(wrapGeo , stillGrp)
        mc.select(wrapGeo, r=True)
        mc.select(fclBodyGeo, add=True)
        wrap = mm.eval('doWrapArgList "7"{"1","0","0.1","2","1","0","1"};')[0]
        rt.blendShape(wrapGeo , bodyGeo)
    else:
        rt.blendShape(fclBodyGeo , bodyGeo)

    if len(fclGeoList) == len(bodyGeoList):
	    for i in range(len(fclGeoList)):
	        rt.blendShape(fclGeoList[i] , bodyGeoList[i])
    else:
    	print 'len fclAllGeo in scroll dose not macth with bodyAllGeo in scroll.'

def readData(local = False):
    context = context_info.ContextPathInfo()
    if context.name:
        if local:
            projectPath = os.environ['RFPROJECT']
            fileWorkPath = (context.path.name()).replace('$RFPROJECT',projectPath)
            path = fileWorkPath + '/rig/rigData/assembleFclRig.yml'
            data = file_utils.ymlLoader(path)
            print '---------- Read data from local ----------'
        else:
            corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
            path = '{}/rf_maya/rftool/rig/rigScript/template/assembleFclRig.yml'
            data = file_utils.ymlLoader(path)
            print '---------- Read data from template  ----------'

    else:
        corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        path = '{}/rf_maya/rftool/rig/rigScript/template/assembleFclRig.yml'
        data = file_utils.ymlLoader(path)
        print "---------- This character doesn't have data, Read data from template ----------"

    return data


def importAndRun(module =[]):
    for each in module:
        exec('from DevScript import {}'.format(each))
        exec('reload({})'.format(each))
        exec('{}.Run()'.format(each))     