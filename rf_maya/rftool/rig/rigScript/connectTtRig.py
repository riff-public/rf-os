import maya.cmds as mc
import maya.mel as mm

def Run(    jawLwr2Jnt      = 'bodyRig_md:JawLwr2_Jnt' ,
            jawUprJnt       = 'bodyRig_md:JawUpr1_Jnt' ,
            jawPostZrGrp    = 'fclRig_md:JawPosiTtRigCtrl_Grp' ,
            ctrlUpTtRigGrp  = 'fclRig_md:TeethUpMainTtRig_Ctrl',
            jwaLwrCtrl      = 'bodyRig_md:JawLwr1_Ctrl'):

    if not mc.objExists(jawPostZrGrp):
        print 'This scene does not have TtRig objects.'
        return

    mc.parentConstraint(jawUprJnt , ctrlUpTtRigGrp , mo=True)
    mc.scaleConstraint(jawUprJnt , ctrlUpTtRigGrp , mo=True)
    mc.parentConstraint(jawLwr2Jnt , jawPostZrGrp , mo=True)
    mc.scaleConstraint(jawLwr2Jnt , jawPostZrGrp , mo=True)

    mc.select(cl=True)

    return

