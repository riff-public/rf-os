//Maya ASCII 2018ff09 scene
//Name: fxCtrl.ma
//Last modified: Fri, May 27, 2022 05:39:15 PM
//Codeset: 1252
requires maya "2018ff09";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201903222215-65bada0e52";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "25FD8262-488D-F554-1559-45A9BCFC68A0";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 23.552203664065157 13.850996079833166 31.948510305312873 ;
	setAttr ".r" -type "double3" -18.338352729603216 36.200000000001509 -9.8535040497034953e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "47E612B3-423A-4AE8-D0BB-4596E0406EB1";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 42.892555961576882;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "5EB53DC4-44A0-78FD-8F92-31986C5399B7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "EC338B2A-4CED-B05A-5ED2-869FA9C4C32F";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "AC569EC9-4C3A-F76C-5591-01A54B72A3DD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "2880B795-4F12-951C-2AA0-5DA1E15DA25C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "1E58AE24-469F-2693-DE73-F28367833588";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "51E85F48-4F2C-DE7E-D96B-05A94188A844";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "FxCtrl_Grp";
	rename -uid "08D58E01-494B-865D-0940-CE8A3FF99D16";
createNode transform -n "Fx_Ctrl" -p "FxCtrl_Grp";
	rename -uid "E27BDF95-4459-BE4F-B2F4-E3B1286A4F40";
	addAttr -ci true -k true -sn "fxRes" -ln "fxRes" -nn "fx Res" -min 0 -max 3 -en 
		"None:Low:Mid:High" -at "enum";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -k on ".fxRes";
createNode nurbsCurve -n "Fx_CtrlShape" -p "Fx_Ctrl";
	rename -uid "E750BD11-413C-BDB3-7F04-4AB9E4513B95";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 6 0 no 3
		7 0 1 2 3 4 5 6
		7
		0 2.4999999999999991 -6.8938963839746202e-16
		-1 -0.25318156881709503 6.0010903224518672e-17
		0.0028741814264430232 -0.25318156881709503 6.0010903224518672e-17
		0 -2.4999999999999991 7.5390029361524148e-16
		1 0.25318156881709553 -9.378035814553606e-18
		0 0.25318156881709553 -9.378035814553606e-18
		0 2.4999999999999991 -6.8938963839746202e-16
		;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "E38DF388-498D-1D51-A8D8-7B9BE0581C5A";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "38BEFB45-45BD-ED03-C18D-EFB9A54DE967";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "87CC8A2D-4514-1F3F-6711-92816BB1BFC2";
createNode displayLayerManager -n "layerManager";
	rename -uid "EF88CCBC-4A8B-4252-24AC-479E912F88D3";
createNode displayLayer -n "defaultLayer";
	rename -uid "0ECF6432-459B-6A0C-BE46-0F84747E6750";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "6DCEAB5C-435E-0ADA-E349-578E88EE614E";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "A7F6BC60-4EE0-50EF-3050-30A8AAC5CF66";
	setAttr ".g" yes;
createNode condition -n "LowVis_cond";
	rename -uid "E3D8E003-46A2-07DB-AAC5-E8A460C45B37";
	setAttr ".st" 1;
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode condition -n "MidVis_cond";
	rename -uid "4D068009-4A4A-61C8-94EE-0CB34A7558F5";
	setAttr ".st" 2;
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode condition -n "HighVis_cond";
	rename -uid "1A2BCB9B-474C-75B0-29BC-9991AAB5E8BE";
	setAttr ".st" 3;
	setAttr ".ct" -type "float3" 1 0 0 ;
	setAttr ".cf" -type "float3" 0 1 1 ;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "22570154-4E79-2A1C-F1D6-F4BB6AD48430";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".cons" no;
	setAttr ".ta" 0;
	setAttr ".tmr" 1024;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "Fx_Ctrl.fxRes" "LowVis_cond.ft";
connectAttr "Fx_Ctrl.fxRes" "MidVis_cond.ft";
connectAttr "Fx_Ctrl.fxRes" "HighVis_cond.ft";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of fxCtrl.ma
