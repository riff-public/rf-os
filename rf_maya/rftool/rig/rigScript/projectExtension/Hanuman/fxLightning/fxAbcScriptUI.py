import maya.cmds as mc
import os
from rf_maya.rftool.rig.rigScript import ngSkinLayersTools
reload(ngSkinLayersTools)

from rf_maya.rftool.rig.rigScript.projectExtension.Hanuman.fxLightning import fxAbcScript
reload(fxAbcScript)


ui = 'FxUI'
def fxResVis(*args):
	fxAbcScript.importFxCtrl()
	fxAbcScript.fxResVis()

def abc_cycle(*args):
	fxAbcScript.abc_cycle()

def merge_abc(*args):
	fxAbcScript.merge_abc()

def connectAbcToCache(self):
	fxAbcScript.connectAbcToCache()

def wrapAbcToMesh(self):
	sel = mc.ls(sl=True)
	wrapTo = sel[-1]
	fxAbcScript.wrapAbcToMesh(sel[:-1],wrapTo)

# def selectedWrapAbcToMesh(self):

# 	sel = mc.ls(sl=True)
# 	wrapTo = sel[-1]
# 	wrap_list = []
# 	for i in sel[:-1]:
# 		if 'FxLow' in i:
# 			low = i
# 			mid = i.replace('FxLow','FxMid')
# 			high =i.replace('FxLow','FxHigh')

# 			A = mid.replace('FxMid','FxMidSlowA')
# 			B = mid.replace('FxMid','FxMidSlowB')
# 			C = mid.replace('FxMid','FxMidSlowC')

# 		elif 'FxMid' in i:
# 			low = i.replace('FxMid','FxMid')
# 			mid = i
# 			high =i.replace('FxMid','FxHigh')   
			
# 			A = mid.replace('FxMid','FxMidSlowA')
# 			B = mid.replace('FxMid','FxMidSlowB')
# 			C = mid.replace('FxMid','FxMidSlowC')

# 		elif 'FxHigh' in i:
# 			low = i.replace('FxHigh','FxLow')
# 			mid = i.replace('FxHigh','FxMid')
# 			high =i

# 			A = mid.replace('FxMid','FxMidSlowA')
# 			B = mid.replace('FxMid','FxMidSlowB')
# 			C = mid.replace('FxMid','FxMidSlowC')

# 		if 'SlowA' in i:



# 		for res in low,mid,high,A,B,C:
# 			if mc.objExists(res) and res not in wrap_list:
# 				wrap_list.append(res)



# 	fxAbcScript.wrapAbcToMesh(wrap_list,wrapTo)

def selectedWrapAbcToMesh(self):
	sel = mc.ls(sl=True)
	wrapTo = sel[-1]
	wrap_list = []
	for i in sel[:-1]:
		ns = i.split(':')[0]+':'
		nameSplit = i.split(':')[-1].split('_')
		if len(nameSplit) ==3:
			name = nameSplit[0].split('Fx')[0]
			side = '_{}_'.format(nameSplit[1])

		else:
			name = name = nameSplit[0].split('Fx')[0]
			side = '_'
		print name
		print side


		low = '{}{}FxLow{}Geo'.format(ns,name,side)
		mid = '{}{}FxMid{}Geo'.format(ns,name,side)
		high ='{}{}FxHigh{}Geo'.format(ns,name,side)
		A = '{}{}FxMidSlowA{}Geo'.format(ns,name,side)
		B = '{}{}FxMidSlowB{}Geo'.format(ns,name,side)
		C = '{}{}FxMidSlowC{}Geo'.format(ns,name,side)

		print low
		print mid
		print high
		print A
		print B
		print C
		for res in low,mid,high,A,B,C:
			if mc.objExists(res) and res not in wrap_list:
				wrap_list.append(res)
	print wrap_list
	fxAbcScript.wrapAbcToMesh(wrap_list,wrapTo)



def showUI():
	if mc.window(ui,exists=1):
		mc.deleteUI(ui)
		
	ui_win = mc.window(ui,w=130,h=180,s=0)
	mc.columnLayout(adj=1,co = ['both',10])
	mc.separator(h=10,style = 'none')
	mc.text(l='Import Alembic cache to Lighitning Geo.')
	mc.button(l = 'Import Abc Node', c=merge_abc )
	mc.separator(h=10,style = 'none')
	mc.text(l='Import FXCtrl. and Connect Fx Resolution Geo visibility.')
	mc.button(l = 'Import FxCtrl', c=fxResVis )
	mc.separator(h=10,style = 'none')
	mc.text(l='Set All Alembic Cache mode as Loop.')
	mc.button(l = 'Abc cycle', c=abc_cycle )
	mc.separator(h=10,style = 'none')
	mc.text(l= 'Select Alembic Geo and add Attribute to FxCtrl.')
	mc.separator(h=3,style = 'none')
	mc.button(l = 'Connect abc to ctrl', c=connectAbcToCache)
	mc.separator(h=10,style = 'none')
	mc.text(l= 'Do in Main Process\nWrap selected alembic geo to selected Mesh.')
	mc.button(l = 'wrap abc to mesh ', c=selectedWrapAbcToMesh)
	mc.separator(h=10,style = 'none')
	mc.showWindow(ui)
#showUI()