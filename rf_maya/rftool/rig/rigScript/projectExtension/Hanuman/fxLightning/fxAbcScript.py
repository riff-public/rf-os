import maya.cmds as mc
import os
import maya.mel as mel
def importFxCtrl():
    if not mc.objExists('Fx_Ctrl'):
        file_path = os.path.realpath(__file__)
        file_location = os.path.dirname(file_path)
        mc.file(os.path.join(file_location,'fxCtrl.ma'),i=1,ns =':')

def fxResVis():
    for num,i in enumerate(['Low','Mid','High']):
        cond = '{}Vis_cond'.format(i)
        if not mc.objExists(cond):
            cond = mc.createNode('condition', n = '{}Vis_cond'.format(i))
            mc.setAttr(cond+'.colorIfTrueR',1)
            mc.setAttr(cond+'.colorIfFalseR',0)
            mc.setAttr(cond+'.secondTerm',num+1)
            mc.connectAttr('Fx_Ctrl.fxRes',cond+'.firstTerm')
        mc.connectAttr('{}Vis_cond.outColorR'.format(i),'Fx{}Geo_Grp.v'.format(i),f=1)
    
    if mc.ls('FxSlow*'):
        mc.addAttr('Fx_Ctrl',ln = 'speed',enumName = 'normal:slow',k=1,at='enum')
        for num,i in enumerate(['','Slow']):
            cond = 'Fx{}Vis_cond'.format(i)
            if not mc.objExists(cond):
                cond = mc.createNode('condition', n = 'Fx{}Vis_cond'.format(i))
                mc.setAttr(cond+'.colorIfTrueR',1)
                mc.setAttr(cond+'.colorIfFalseR',0)
                mc.setAttr(cond+'.secondTerm',num)
                mc.connectAttr('Fx_Ctrl.speed',cond+'.firstTerm')
            mc.connectAttr('Fx{}Vis_cond.outColorR'.format(i),'Fx{}Geo_Grp.v'.format(i),f=1)
            
        mc.addAttr('Fx_Ctrl',ln = 'type',enumName = 'A:B:C',k=1,at='enum')

        for i in ['Low','Mid','High']:
            if mc.objExists('Fx{}SlowGeo_Grp'.format(i)):
                for num, letter in enumerate('ABC'):
                    cond = 'FxSlow{}Vis_cond'.format(letter)
                    if not mc.objExists(cond):
                        cond = mc.createNode('condition', n = cond)
                        mc.setAttr(cond+'.colorIfTrueR',1)
                        mc.setAttr(cond+'.colorIfFalseR',0)
                        mc.setAttr(cond+'.secondTerm',num)
                        mc.connectAttr('Fx_Ctrl.type',cond+'.firstTerm')
                    mc.connectAttr(cond+'.outColorR','Fx{}Slow{}Geo_Grp.v'.format(i,letter),f=1)            
    
        for i in ['Low','Mid','High']:
            if mc.objExists('Fx{}SlowGeo_Grp'.format(i)):
                    mc.connectAttr('{}Vis_cond.outColorR'.format(i),'Fx{}SlowGeo_Grp.v'.format(i),f=1)



def abc_cycle():
    abc = mc.ls(type='AlembicNode')
    if abc:
        for i in abc:
            mc.setAttr(i+'.cycleType',1)

def merge_abc():
    file_loc = mc.file(location=1,q=1)
    file_list = file_loc.split('/')[:-4]
    file_list.append('fx')
    file_list.append('main')
    file_list.append('output')
    fx_path = '/'.join(file_list)
    abcFile = mc.fileDialog2(dir = fx_path,fm = 4,ds=1)
    for i in abcFile:
        mc.AbcImport(i,mode = 'import',connect = '/' )


def connectAbcToCache():
    sel = mc.ls(sl=True)
    for i in sel:
        abc = mc.listConnections(i+'.inMesh',type = 'AlembicNode')[0]
        if len(i.split('_')) ==3:
            side = '_{}'.format(i.split('_')[1])
            part_attr  = i.split('Fx')[0]
        else:
            side = ''
            part_attr  = i.split('Fx')[0]        
            
        if not mc.objExists('Fx_Ctrl.{}{}'.format(part_attr,side)):
            mc.addAttr('Fx_Ctrl', ln=part_attr+side ,dv=0,max=1,min=0,k=1)
            mc.setAttr('Fx_Ctrl.{}{}'.format(part_attr,side),l=1)
            mc.addAttr('Fx_Ctrl',ln = '{}{}_Auto'.format(part_attr,side) ,k =1 ,dv=1,max=1,min=0)
            mc.addAttr('Fx_Ctrl', ln = '{}{}_Offset'.format(part_attr,side),k=1)
            mc.addAttr('Fx_Ctrl', ln = '{}{}_Vis'.format(part_attr,side),k=1,dv=1,max=1,min=0)
        mdv = '{}Fx{}_Mdv'.format(part_attr,side)
        if not mc.objExists( mdv):
            mdv = mc.createNode('multiplyDivide', n = '{}Fx{}_Mdv'.format(part_attr,side))
            mc.connectAttr('Fx_Ctrl.{}{}_Auto'.format(part_attr,side),mdv+'.input2X',f=1)
            mc.connectAttr('time1.outTime',mdv+'.input1X',f=1)
        mc.connectAttr(mdv+'.outputX',abc+'.time',f=1)
        mc.connectAttr('Fx_Ctrl.{}{}_Offset'.format(part_attr,side),abc+'.offset',f=1)
        mc.connectAttr('Fx_Ctrl.{}{}_Vis'.format(part_attr,side),i+'.v',f=1)
        
def wrapAbcToMesh(objList=[],wrapTo=''):
    objList.append(wrapTo)
    mc.select(objList)
    mel.eval('CreateWrap;')
    #wrap = mc.deformer(type='wrap')
    #mc.setAttr(wrap[0]+'.exclusiveBind',1)     
