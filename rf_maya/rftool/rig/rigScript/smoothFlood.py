import maya.cmds as mc
def smoothFlood(skinCluster = '',iterations=1,geometry = ''):
	'''
	Smooth flood all influences using artisan.
	@param skinCluster: The skinCluster to smooth flood influence weights on
	@type skinCluster: str
	@param iterations: Number of smooth iterations
	@type iterations: int
	'''
	# Check zero iterations
	if not iterations: return
	
	# Get current tool
	currentTool = mc.currentCtx()
	
	# Select geometry
	# geometry = glTools.utils.deformer.getAffectedGeometry(skinCluster).keys()
	mc.select(geometry)
	
	# Get skinCluster influence list
	influenceList = mc.skinCluster(skinCluster,q=True,inf=True)
	
	# Unlock influence weights
	for influence in influenceList: mc.setAttr(influence+'.lockInfluenceWeights',0)
	
	# Initialize paint context
	skinPaintTool = 'artAttrSkinContext'
	if not mc.artAttrSkinPaintCtx(skinPaintTool,ex=True):
		mc.artAttrSkinPaintCtx(skinPaintTool,i1='paintSkinWeights.xpm',whichTool='skinWeights')
	mc.setToolTo(skinPaintTool)
	mc.artAttrSkinPaintCtx(skinPaintTool,edit=True,sao='smooth')
	
	# Smooth Weights
	for i in range(iterations):
		print(skinCluster+': Smooth Iteration - '+str(i+1))
		for influence in influenceList:
			# Lock current influence weights
			mc.setAttr(influence+'.lockInfluenceWeights',1)
			# Smooth Flood
			mm.eval('artSkinSelectInfluence artAttrSkinPaintCtx "'+influence+'" "'+influence+'"')
			mc.artAttrSkinPaintCtx(skinPaintTool,e=True,clear=True)
			# Unlock current influence weights
			mc.setAttr(influence+'.lockInfluenceWeights',0)
	
	# Reset current tool
	mc.setToolTo(currentTool)