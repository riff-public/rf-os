import core
reload(core)
import rigTools as rt
reload(rt)
import maya.cmds as mc

cn = core.Node()

class Run( object ):
	def __init__( self ,	lipsInTmpCrv		= 'LipsIn_TmpCrv' ,
							lipsOutTmpCrv		= 'LipsOut_TmpCrv' ,
							jawTmpJnt			= 'mthJawLwr2_Jnt' ,
							headTmpJnt			= 'mthHead_Jnt' , 
							jawCtrl				= 'mthJawLwr1_Loc' ,
							numDtlJnt			= 28 ,
							skipJntCen			= 4 ,
							skinGrp				= '' ,
							elem				= '' ,
							side				= '' ):

		#-- Info
		elem = elem.capitalize()

		if side == '':
			side = '_'
		else:
			side = '_{}_'.format(side)

		mthJawGrp = mc.rename('mthJaw_Grp', 'JawMthRig_Grp')
		jawJnt = mc.rename('mthJawLwr1_Jnt', 'JawLwr1MthRig_Jnt')
		jawTmpJnt = mc.rename(jawTmpJnt, 'JawLwr2MthRig_Jnt')
		jawCtrl = mc.rename(jawCtrl, 'JawLwr1MthRig_Loc')
		headTmpJnt = mc.rename(headTmpJnt, 'HeadMthRig_Jnt')

		#-- Calculate to Find Skip Joint

		jntMidUpr = numDtlJnt / 4
		jntMidLwr = jntMidUpr * 3
		jntCnrR	  = numDtlJnt / 2
		jntCnrL   = jntCnrR * 2

		listJntSkip = [jntMidUpr , jntMidLwr , jntCnrR , jntCnrL]

		# cnr joint are numJnt / 2 (Right) and lastest joint (left) ex. num = 36 -> cnrR = 18 , cnrL = 36

		# it's number of joint which is have to skip for mouth shape
		for i in range( 1 , skipJntCen + 1 ):
			jntMidUprPlus = jntMidUpr + i
			jntMidUprSub = jntMidUpr - i
			jntMidLwrPlus = jntMidLwr + i
			jntMidLwrSub = jntMidLwr - i

			for num in (jntMidUprPlus , jntMidUprSub , jntMidLwrPlus , jntMidLwrSub):
				listJntSkip.append(num)

		#-- Calculate open mouth
		openValList = []
		openAvgList = []
		quaterJnt = (numDtlJnt / 4) - skipJntCen

		animCrv = cn.createNode('animCurveTU' , 'openMouthValue')
		mc.setKeyframe(animCrv , t = 0 , v = 0.5)
		mc.setKeyframe(animCrv , t = quaterJnt , v = 1.0)
		mc.keyTangent(animCrv , e = True , a = True , t = (0,0) , outAngle = 0 , outWeight = (quaterJnt / 4) * 1.4)
		mc.keyTangent(animCrv , e = True , a = True , t = (quaterJnt , quaterJnt) , inAngle = 0 , inWeight = (quaterJnt / 4) * 1.75)

		for i in range(1 , quaterJnt):
			val = mc.keyframe(animCrv , t = (i,i) , q = True , ev = True)[0]
			openAvgList.append(val)

		for i in range(2):
			for j in openAvgList:
				openValList.append(j)
			for j in openAvgList[::-1]:
				openValList.append(j)

		#-- Calculate seal mouth
		maxAvgList = []
		minAvgList = []
		avgMax = 20.0 / ((numDtlJnt / 2) - 1)
		maxStartValue = 0

		for i in range(1 , (numDtlJnt / 2)):
			maxVal = maxStartValue + avgMax
			maxAvgList.append(maxVal)

			if i == 1 :
				minAvgList.append(0)
			elif i > ((numDtlJnt / 2) - 1):
				pass
			else:
				minVal = float(maxAvgList[i - 2]) / 2
				minAvgList.append(minVal)

			maxStartValue += avgMax

		#-- Calculate seal mouth left
		maxAvgListL = []
		minAvgListL = []

		for avgMax in maxAvgList:
			maxAvgListL.append(avgMax)
		for avgMin in minAvgList:
			minAvgListL.append(avgMin)

		for i in range(1 , (numDtlJnt / 2)):
			maxAvgListL.append(maxAvgList[-i])
			minAvgListL.append(minAvgList[-i])

		#-- Calculate seal mouth right
		maxAvgListR = []
		minAvgListR = []

		for avgMax in maxAvgList[::-1]:
			maxAvgListR.append(avgMax)
		for avgMin in minAvgList[::-1]:
			minAvgListR.append(avgMin)

		for i in range(1 , (numDtlJnt / 2)):
			maxAvgListR.append(maxAvgList[i-1])
			minAvgListR.append(minAvgList[i-1])

		#-- Create mouth skin group lips
		self.mthStillGrp = cn.createNode('transform' , 'MthRig{}Still{}Grp'.format(elem , side))
		self.mthSkinGrp = cn.createNode('transform' , 'MthRig{}Skin{}Grp'.format(elem , side))
		self.lipsSkinGrp = cn.createNode('transform' , 'Lips{}MthRigSkin{}Grp'.format(elem , side))

		#-- Create Nurb
		self.lipsNrb = core.Dag(mc.loft(lipsInTmpCrv , lipsOutTmpCrv , n = 'Lips{}MthRig{}Nrb'.format(elem , side) , ch = False , d = 3)[0])

		#-- Add Attr lips seal
		self.jawCtrl = core.Dag(jawCtrl)
		self.jawCtrlShape = core.Dag(self.jawCtrl.shape)
		self.jawCtrl.addTitleAttr('Lips')
		self.jawCtrl.addAttr('LipsSealL' , 0 , 20)
		self.jawCtrl.addAttr('LipsSealR' , 0 , 20)

		#-- Create detail control & joint
		lengthDtl = 0
		openList = 0
		sealList = 0
		posiLength = 1 / float(numDtlJnt)
		for i in range(numDtlJnt):
			# set position joint
			self.rbnDtlJnt = cn.joint('Lips{}{}DtlMthRig{}Jnt'.format(elem , i+1 , side))
			self.rbnDtlJnt.attr('radius').v = 0.01
			self.rbnDtlJntZroGrp = rt.addGrp(self.rbnDtlJnt)
			self.rbnPosGrp = cn.createNode('transform' , 'Lips{}{}PosMthRig{}Grp'.format(elem , i+1 , side))
			self.posi = cn.createNode('pointOnSurfaceInfo' , 'Lips{}{}PosMthRig{}Posi'.format(elem , i+1 , side))
			self.lipsNrb.attr('worldSpace[0]') >> self.posi.attr('inputSurface')
			self.posi.attr('position') >> self.rbnPosGrp.attr('t')
			self.posi.attr('parameterU').v = 0.5
			self.posi.attr('parameterV').v = lengthDtl
			self.rbnDtlJnt.parent(self.rbnDtlJntZroGrp)
			self.rbnDtlJntZroGrp.snap(self.rbnPosGrp)
			self.rbnDtlJntZroGrp.parent(self.lipsSkinGrp)
			mc.delete(self.rbnPosGrp)

			# Add Attr
			self.jawCtrlShape.addTitleAttr('Lips{}'.format(i+1))
			self.jawCtrlShape.addAttr('Lips_{}_Open'.format(i+1))
			self.jawCtrlShape.addAttr('Lips_{}_Seal'.format(i+1))
			self.jawCtrlShape.addAttr('Lips_{}_L_Min'.format(i+1))
			self.jawCtrlShape.addAttr('Lips_{}_L_Max'.format(i+1))
			self.jawCtrlShape.addAttr('Lips_{}_R_Min'.format(i+1))
			self.jawCtrlShape.addAttr('Lips_{}_R_Max'.format(i+1))		
			self.jawCtrlShape.attr('Lips_{}_Seal'.format(i+1)).v = 0.5

			# set default open mouth
			if i+1 in listJntSkip:
				self.jawCtrlShape.attr('Lips_{}_Open'.format(i+1)).v = 1
			else:
				self.jawCtrlShape.attr('Lips_{}_Open'.format(i+1)).v = openValList[openList]
				openList += 1

			if i+1 in (jntCnrL , jntCnrR):
				self.jawCtrlShape.attr('Lips_{}_Open'.format(i+1)).v = 0.5

			# set default min / max seal
			if not i+1 in (jntCnrL , jntCnrR):
				self.jawCtrlShape.attr('Lips_{}_L_Min'.format(i+1)).v = minAvgListL[sealList]
				self.jawCtrlShape.attr('Lips_{}_L_Max'.format(i+1)).v = maxAvgListL[sealList]
				self.jawCtrlShape.attr('Lips_{}_R_Min'.format(i+1)).v = minAvgListR[sealList]
				self.jawCtrlShape.attr('Lips_{}_R_Max'.format(i+1)).v = maxAvgListR[sealList]
				sealList += 1

			# create node & set value & connect node
			self.remL = cn.createNode('remapValue' , 'Lips{}{}LftMthRig{}Rem'.format(elem , i+1 , side))
			self.remR = cn.createNode('remapValue' , 'Lips{}{}RgtMthRig{}Rem'.format(elem , i+1 , side))
			self.pmaSum = cn.createNode('plusMinusAverage' , 'Lips{}{}SumMthRig{}Pma'.format(elem , i+1 , side))
			self.pmaSub = cn.createNode('plusMinusAverage' , 'Lips{}{}SubMthRig{}Pma'.format(elem , i+1 , side))
			self.pmaSeal = cn.createNode('plusMinusAverage' , 'Lips{}{}SealMthRig{}Pma'.format(elem , i+1 , side))
			self.cmpTotal = cn.createNode('clamp' , 'Lips{}{}TotalMthRig{}Cmp'.format(elem , i+1 , side))
			self.cmp = cn.createNode('clamp' , 'Lips{}{}MthRig{}Cmp'.format(elem , i+1 , side))
			self.rev = cn.createNode('reverse' , 'Lips{}{}MthRig{}Rev'.format(elem , i+1 , side))
			self.pmaSub.attr('operation').v = 2
			self.pmaSeal.attr('operation').v = 2

			self.parsCons = core.Dag(mc.parentConstraint(headTmpJnt , jawTmpJnt , self.rbnDtlJntZroGrp , mo=True)[0])

			self.jawCtrlShape.attr('Lips_{}_Open'.format(i+1)) >> self.pmaSeal.attr('i1[0]')
			self.jawCtrlShape.attr('Lips_{}_Open'.format(i+1)) >> self.pmaSub.attr('i1[0]')
			self.jawCtrlShape.attr('Lips_{}_Open'.format(i+1)) >> self.cmpTotal.attr('mnr')
			self.jawCtrlShape.attr('Lips_{}_Seal'.format(i+1)) >> self.pmaSeal.attr('i1[1]')
			self.jawCtrlShape.attr('Lips_{}_L_Min'.format(i+1)) >> self.remL.attr('inputMin')
			self.jawCtrlShape.attr('Lips_{}_L_Max'.format(i+1)) >> self.remL.attr('inputMax')
			self.jawCtrlShape.attr('Lips_{}_R_Min'.format(i+1)) >> self.remR.attr('inputMin')
			self.jawCtrlShape.attr('Lips_{}_R_Max'.format(i+1)) >> self.remR.attr('inputMax')

			self.jawCtrl.attr('LipsSealL') >> self.pmaSum.attr('i1[0]')
			self.jawCtrl.attr('LipsSealR') >> self.pmaSum.attr('i1[1]')
			self.jawCtrl.attr('LipsSealL') >> self.remL.attr('inputValue')
			self.jawCtrl.attr('LipsSealR') >> self.remR.attr('inputValue')
			self.pmaSeal.attr('o1') >> self.remL.attr('outputMax')
			self.pmaSeal.attr('o1') >> self.remR.attr('outputMax')
			self.pmaSum.attr('o1') >> self.cmpTotal.attr('ipr')
			self.remL.attr('outValue') >> self.pmaSub.attr('i1[1]')
			self.remR.attr('outValue') >> self.pmaSub.attr('i1[2]')
			self.pmaSub.attr('o1') >> self.cmp.attr('ipr')
			self.cmp.attr('opr') >> self.cmpTotal.attr('mxr')
			self.cmp.attr('mnr').v = 0.5
			self.cmp.attr('mxr').v = 1

			if i+1 > (numDtlJnt / 2):
				self.cmpTotal.attr('opr') >> self.parsCons.attr('{}W1'.format(jawTmpJnt))
				self.parsCons.attr('{}W1'.format(jawTmpJnt)) >> self.rev.attr('ix')
				self.rev.attr('ox') >> self.parsCons.attr('{}W0'.format(headTmpJnt))

			else:
				self.cmpTotal.attr('opr') >> self.parsCons.attr('{}W0'.format(headTmpJnt))
				self.parsCons.attr('{}W0'.format(headTmpJnt)) >> self.rev.attr('ix')
				self.rev.attr('ox') >> self.parsCons.attr('{}W1'.format(jawTmpJnt))

			lengthDtl += posiLength

		#-- Adjust Hierachy
		# mthJawGrp = 'mthJaw_Grp'
		mc.parent(self.lipsSkinGrp , mthJawGrp , headTmpJnt , self.mthSkinGrp)
		mc.parent(jawCtrl , self.mthStillGrp)
		mc.delete(self.lipsNrb , animCrv)

		mc.select( cl =True )


		