from collections import defaultdict
from collections import OrderedDict
import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
import re
import core
reload( core )
import rigTools as rt
reload( rt )
from rf_utils.context import context_info
reload(context_info)
from rf_utils import file_utils
reload(file_utils)
import bsh_utils
reload(bsh_utils)

cn = core.Node()

# --------------------------------------------------------------------------------------------
# Duplicate blendshape description::
#   1. Prepare hdGeo and naming them to bshRigGeo.
#   2. Import controlers and scaling.
#   3. run runDefault() or runFromData()
#       For manual rigging.
#       3.1 Duplicate main script have 2 type
#           1. Duplicate from default data (from template for your information)
#           2. Duplicate from character's data
#           main script for duplicate is duplicateBshMainScript()
#           for rig you should run duplicateBlendshape() for duplicate default blendshape
#           either is fine because it's same by default (except spacial head ex.third eyes)
#       3.2 Connect locator to blendshape (doesn't have to had any rig node)
#       3.3 Connect blendshape rig to locator node.
#   ----- Done default blendshape -----
#
# Special script::
#   If character have to add more geo after start working.
#       - Disconnect locator to blendshape
#       - Parent new geo group to BshRigGeo_Grp
#       - Run script addMoreGeoBlendShape()
#       - This script will duplicate new geo group to other group and delete old blendshape node then add new blendshape node.
#
#   If you want Inbetween blendshape.
#       - Run script duplicateInbtwBlendShape()
#       - default value inbetween is 0.5
#
# --------------------------------------------------------------------------------------------


def duplicateBshMainScript(bshList = [] , grpName = '' , side = '' , gi = 0 , geoGrp = 'BshRigGeo_Grp' , allBshGrp = 'Bsh_Grp' , allTxtGrp = 'Text_Grp',row = 'x',column = 'y'):

    #-- Info
    fontText = 'MS Shell Dlg 2, 27.8pt'

    name = grpName.split('_')[0]
    if side:
        sideGrp = '_{}_'.format(side)
        side = '_{}'.format(side)
    else:
        side = ''
        sideGrp = '_'

    bshGrp = '{}Bsh{}Grp'.format(name , sideGrp)
    txtGrp = '{}Text{}Grp'.format(name , sideGrp)

    bb = mc.exactWorldBoundingBox(geoGrp)

    objWidth = abs(bb[0] - bb[3])

    yOffset = float( abs( bb[1] - bb[4] ) * 1.2 )
    xOffset = float( abs( bb[0] - bb[3] ) * 1.3 )
    xVal = 0
    yVal = 0

    data = defaultdict(list)

    child = mc.listRelatives(allBshGrp , c=True , type='transform')
    if child:
        member = len(child)
    else:
        member = 0
        
    for each in bshList:

        if not mc.objExists(bshGrp):
            bshGrp = cn.createNode('transform' , bshGrp)
            mc.parent(bshGrp , allBshGrp)
        else:
            bshGrp = core.Dag(bshGrp)

        if not mc.objExists(txtGrp):
            txtGrp = cn.createNode('transform' , txtGrp)
            mc.parent(txtGrp , allTxtGrp)
        else:
            txtGrp = core.Dag(txtGrp)

        dupGrp = mc.duplicate( geoGrp , rr=True )[0]
        bshGeo = mc.rename( dupGrp , '{}{}'.format(each,side))
        mc.parent(bshGeo , bshGrp)
        if column == 'x':
            mc.move( yVal , 0  , 0 , bshGeo , r = True)
        elif column == 'z':
            mc.move( 0 , 0  , yVal , bshGeo , r = True)
        else:
            mc.move( 0 , yVal  , 0 , bshGeo , r = True)


        txtCrv = mc.textCurves(n = '{}_'.format(bshGeo) , f = fontText , t = bshGeo)[0]
        txtBb = mc.exactWorldBoundingBox(txtCrv)
        mc.xform(txtCrv , cp = True)

        txtWidth = abs(txtBb[0] - txtBb[3])

        tmpLoc = mc.spaceLocator()
        sclVal = objWidth / txtWidth
        mc.scale( sclVal , sclVal , sclVal , tmpLoc , r = True)
        
        if column == 'x':
            xColVal = bb[0]+yVal
            if row == 'y':
                yColVal = 0
                zColVal = bb[5]
            elif row == 'z':
                yColVal = bb[4]
                zColVal = 0 
        elif column == 'y':
            yColVal = bb[1]+yVal
            if row == 'x':
                xColVal = 0
                zColVal = bb[5]
            elif row == 'z':
                xColVal = bb[3]
                zColVal = 0
        elif column == 'z':
            zColVal = bb[2]+yVal
            if row == 'x':
                xColVal = 0
                yColVal = bb[1]
            elif row == 'y':
                xColVal = bb[3]
                yColVal = 0                
        mc.move( xColVal , yColVal,zColVal , tmpLoc , r = True)

            
        mc.setAttr( '{}.overrideEnabled'.format(txtCrv), 1 )
        mc.setAttr( '{}.overrideDisplayType'.format(txtCrv), 2 )

        rt.snapObj(tmpLoc , txtCrv)
        mc.parentConstraint(bshGeo , txtCrv , mo=True)

        rt.setColor(txtCrv , 'gray')

        mc.delete( tmpLoc )
        mc.parent( txtCrv , txtGrp )

        data['bshGrpList'].append(bshGeo)
        data['groupIndex'].append(gi)
        gi += 1
        yVal += yOffset

    data['gi'] = gi
        
    xVal = xOffset * (1 + member)
    if row == 'y':
        mc.move( 0 , xVal , 0 , bshGrp , r = True )
    if row == 'z':
        mc.move( 0 , 0 , xVal , bshGrp , r = True )        
    else :
        mc.move( xVal , 0 , 0 , bshGrp , r = True )
    return data


def duplicateBlendShape(    bshList     = [] , 
                            grpName     = '' , 
                            side        = '' , 
                            geoGrp      = 'BshRigGeo_Grp' , 
                            bshNode     = 'FacialAll_Bsn' ,
                            facialLoc   = 'FacialAll_Loc',
                            row = 'x',
                            column = 'y'):
    #-- Info
    stillGrp = 'BshRigStill_Grp'
    allBshGrp = 'Bsh_Grp'
    allTxtGrp = 'Text_Grp'
    delGrp = 'Delete_Grp'

    bshGrpList = []
    groupData = defaultdict(list)

    #-- Setting group
    if not mc.objExists (stillGrp):
        stillGrp = cn.createNode( 'transform' , stillGrp )
        if not mc.objExists(delGrp):
            delGrp = cn.createNode( 'transform' , delGrp )

        allBshGrp = cn.createNode( 'transform' , 'Bsh_Grp' )
        allTxtGrp = cn.createNode( 'transform' , 'Text_Grp' )

        mc.parent( allBshGrp , allTxtGrp , delGrp )
        mc.parent( geoGrp , stillGrp )

    if mc.objExists(bshNode):
        bshSize = len(mc.listAttr(bshNode + '.w' , m=True))
    else:
        bshSize = 0

    addDict = duplicateBshMainScript(bshList = bshList , grpName = grpName , side = side , gi = bshSize ,row = row ,column =  column)      

    for bshGeo in addDict['bshGrpList']:
        bshGrpList.append(bshGeo)
    for i in addDict['groupIndex']:
        if side:
            elem = '_{}'.format(side)
        else:
            elem = ''
        groupData['{}{}'.format(grpName , elem)].append(i)

    gi = addDict['gi']

    #-- Blendshape
    if not mc.objExists(bshNode):
        bshNode = mc.blendShape( bshGrpList , geoGrp , n = bshNode)[0]

    else:
        for each in bshGrpList:
            mc.setAttr(bshNode + '.parentDirectory[{}]'.format(bshSize) , 0)
            mc.blendShape(bshNode , e = True , t = (geoGrp , bshSize , each , 1))
            bshSize += 1

    if not mc.objExists(facialLoc):
        fclLoc = cn.locator('FacialAll_Loc')
        fclGrp = rt.addGrp(fclLoc)
        fclLocShape = core.Dag(fclLoc.shape)

        for attr in ('localPositionX' , 'localPositionY' , 'localPositionZ' , 'localScaleX' , 'localScaleY' , 'localScaleZ'):
            fclLocShape.attr(attr).setLockHide()

        for attr in ('tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v'):
            fclLoc.attr(attr).setLockHide()

        mc.parent(fclGrp, stillGrp)

    else:
        fclLoc = core.Dag(facialLoc)

    fclLocShape = core.Dag(fclLoc.shape)
    for attr in bshGrpList:
        fclLocShape.addAttr(attr , 0 , 10)


    for name, indices in groupData.iteritems():
        groupInShapeEditor(bshNode=bshNode, 
                            name=name, 
                            indices=indices)


    print '----------Duplicate {} DONE----------'.format(grpName)

    return

def addNewGeoBlendShape(newItemLs = []):
    #-- info
    allBshGrp = 'Bsh_Grp'
    geoGrp = 'BshRigGeo_Grp'

    if not newItemLs:
        newItemLs = mc.ls(sl=True)

    if not newItemLs:
        mc.confirmDialog(m="No object is selected")
        return

    if isinstance(newItemLs, (str, unicode)):
        newItemLs = [newItemLs]
        
    if isinstance(newItemLs, (tuple, set)):
        newItemLs = list(newItemLs)

    mc.parent(newItemLs, geoGrp)

    #-- duplicate
    get_dupItem = []
    get_ibwAttr = []
    IbwGrpDict = OrderedDict()
    eachBshGrp = mc.listRelatives(allBshGrp, c=True)

    for eachGrp in eachBshGrp:
        bshSrc = mc.listRelatives(eachGrp, c=True)

        for grp in bshSrc:              
            for newItem in newItemLs:
                dupItem = mc.duplicate(newItem)[0]
                mc.parent(dupItem, grp)
                get_dupItem.append(dupItem)
                
                mc.setAttr("{}.t".format(dupItem), 0, 0, 0)
                mc.setAttr("{}.r".format(dupItem), 0, 0, 0)
                mc.setAttr("{}.s".format(dupItem), 1, 1, 1)
                
            #-- find inbetween
            geoShape = mc.listRelatives(grp, ad=True, f=True, typ="mesh")[0]
            bshPlug = mc.listConnections(geoShape, p=1, scn=1, t='blendShape')[0]
            tgNode , tgIp , tgId , tgV , tgGeo = bshPlug.split('.')
            vBsh = re.findall(r'\d+' , tgV)[0]
                
            if vBsh[0] == '5':
                idIbw = re.findall(r'\d+' , tgId)[0]
                idIbw = int(idIbw)
                vIbw = '0.{}'.format(vBsh[1::])
                vIbw = float(vIbw)
                IbwGrpDict[grp] = {'attrIndex' : idIbw,
                                   'value'     : vIbw }          
    #-- rename
    num = 0
    for count in range(len(newItemLs)):
        for idx in range(count, len(get_dupItem), len(newItemLs)):
            mc.rename(get_dupItem[idx], newItemLs[num])
            
        num += 1
    return IbwGrpDict

def blendShapNewGeo(newItemLs   =   [], 
                    geoGrp      =   'BshRigGeo_Grp' , 
                    bshNode     =   'FacialAll_Bsn' ,
                    facialLoc   =   'FacialAll_Loc',
                    local       =   False ):
    #-- info
    if newItemLs:
        ibwAttr = addNewGeoBlendShape(newItemLs)
    else:
        ibwAttr = addNewGeoBlendShape()

    # if not ibwAttr:
    #     return

    locShape = mc.listRelatives(facialLoc, c=True)
    bshAttr = mc.listAttr(locShape, k=True)

    #-- delete and add blendShape
    mc.delete(bshNode)
    bshNode = mc.blendShape(bshAttr, geoGrp, n=bshNode)[0]
    
    #-- add inbetween
    if ibwAttr:
        for grp, elem in ibwAttr.iteritems():
            idx = elem['attrIndex']
            val = elem['value']
            mc.blendShape(bshNode, e=True, ib=True, t=(geoGrp, idx, grp, val))

    #-- re-connect locator
    for attr in bshAttr:
        mc.connectAttr("{}.{}".format(facialLoc, attr), "{}.{}".format(bshNode, attr))

    #-- create group in shapeEditor
    data = readData(local=local)
    # path = "P:/Hanuman/rnd/rigDev/DevScript/fah_test/test_default.yml"
    # data = file_utils.ymlLoader(path)
    bshG = data["bshDict"].get("bshGrp")

    bshEditorGrp = []
    allBsh = []
    indices = []

    num = 0
    for node in bshG:
        grp = node.keys()[0] #Str
        bshEditorGrp.append(grp)
        name = grp.split('_')

        if len(name) > 1:
            side = '_{}'.format(name[-1])
        else:
            side = ''
        bshLs = node.get(grp,{}) #List

        idc = []
        for ls in bshLs:
            idc.append(num)
            allBsh.append('{}{}'.format(ls, side))

            num += 1
        indices.append(idc)

    for num, BG in enumerate(bshEditorGrp):
        groupInShapeEditor('FacialAll_Bsn', BG, indices[num])

def duplicateInbtwBlendShape(  InbtwName   = '' , 
                            side        = '' , 
                            target      = '' ,
                            value       = 0.5 ,
                            geoGrp      = 'BshRigGeo_Grp' , 
                            bshNode     = 'FacialAll_Bsn' ,
                            facialLoc   = 'FacialAll_Loc'):
     #-- Info
    stillGrp = 'BshRigStill_Grp'
    allBshGrp = 'Bsh_Grp'
    allTxtGrp = 'Text_Grp'
    delGrp = 'Delete_Grp'

    if side:
        grpName = '{}_{}'.format(InbtwName , side)
    else:
        grpName = InbtwName

    bshList = [InbtwName]
    btwGrpList = []

    #-- Setting group
    if not mc.objExists (stillGrp):
        stillGrp = cn.createNode( 'transform' , stillGrp )
        if not mc.objExists(delGrp):
            delGrp = cn.createNode( 'transform' , delGrp )

        allBshGrp = cn.createNode( 'transform' , 'Bsh_Grp' )
        allTxtGrp = cn.createNode( 'transform' , 'Text_Grp' )

        mc.parent( allBshGrp , allTxtGrp , delGrp )
        mc.parent( geoGrp , stillGrp )


    btwDict = duplicateBshMainScript(bshList = bshList , grpName = InbtwName , side = side )      

    btwGeo = '{}'.format(btwDict['bshGrpList'][0])

    if type(target) == str:
        bshMember = mc.listAttr(bshNode + '.w' , m=True)
        target = bshMember.index(target)

    mc.blendShape(bshNode , e = True , ib = True , t = (geoGrp , target , btwGeo , value))

    return

def reBlendshape(   local       = True ,
                    geoGrp      = 'BshRigGeo_Grp' , 
                    bshNode     = 'FacialAll_Bsn' ,
                    facialLoc   = 'FacialAll_Loc'):
    data = readData(local=local)
    bshDict = bsh_utils.BshNode(data['bshDict'])
    rigList = data['bshRig']

    allBshMember = []
    for eachGrp in bshDict.allGrpBshName:
        grpName = eachGrp.split('_')
        if len(grpName) > 1:
            grpSide = '_{}'.format(grpName[-1])
        else:
            grpSide = ''

        member = bshDict.grpMember(eachGrp)
        for mb in member:
            allBshMember.append('{}{}'.format(mb,grpSide))

    #-- Blendshape
    if not mc.objExists(bshNode):
        bshNode = mc.blendShape( allBshMember , geoGrp , n = bshNode)[0]

    groupData = OrderedDict()
    num = 0
    for eachGrp in bshDict.allGrpBshName:
        idList = []
        grpList = bshDict.grpMember(eachGrp)
        for eachMember in grpList:
            idList.append(num)
            num += 1

        groupData[eachGrp] = idList

    for name, indices in groupData.iteritems():
        groupInShapeEditor( bshNode=bshNode, 
                            name=name, 
                            indices=indices)

    if data['bshDict']['InbtwGrp']:
        for eachIb in bshDict.allGrpInbtwName:
            mc.blendShape(bshNode , e = True , ib = True , t = (geoGrp , bshDict.getIndex(eachIb) , eachIb , bshDict.getValue(eachIb) ))


def groupInShapeEditor(bshNode, name, indices):
    # create group in shape editor > bshNode = 'FacialAll_Bsn' , n = group name ex. Eyebrow_L , indices is index geo member in bshNode
    argLists = []
    for i in indices:
        #-- index member in bshNode
        argLists.append('"{}.{}"'.format(bshNode, i))

    mm.eval('blendShapeCreateTargetGroup {{{}}};'.format(','.join(argLists)))

    # name it
    bshNode = pm.PyNode(bshNode)
    nextIndex = max(bshNode.targetDirectory.getArrayIndices())
    print 'Grouping in shapeEditor {}: {}'.format(nextIndex, name)
    bshNode.targetDirectory[nextIndex].directoryName.set(name)

def connectBothPosNeg(  name = '' ,
                        attr = '' ,
                        source = '' ,
                        target = '' ,
                        limitTarget = None ,
                        transform = '' ,
                        valueMax = 0.5 ,
                        valueMin = -0.5 ,
                        jnt = False , *args):
    #-- setting
    obj = core.Dag(source)
    bshCtrlGrp = core.Dag('BshRigCtrl_Grp')
    targetDict = {  'pos' : '{}'.format(target) ,
                    'neg' : None ,
                    'limitPos' : limitTarget ,
                    'limitNeg' : None }

    transformDict = {   'type' : transform ,
                        'max' : valueMax ,
                        'min' : valueMin ,
                        'jnt' : jnt}

    data = {'attr' : attr ,
            'source' : '{}'.format(source) ,
            'target' : targetDict,
            'transform' : transformDict }
    bshCtrlGrp.addTag(name , '{}'.format(data))

    elem = (name).split('_')
    name = elem[0]

    if len(elem) > 1:
        side = '_{}_'.format(elem[-1])
    else:
        side = '_'

    if not attr:
        attr = name

    cmp = rt.checkObjExist('{}Bsh{}Cmp'.format(name,side) , 'clamp')
    mdv = rt.checkObjExist('{}Bsh{}Mdv'.format(name,side) , 'multiplyDivide')
    rt.checkObjExist(obj , attr = attr)

    try:
        cmp.attr('minR').v = -10
        cmp.attr('maxR').v = 10
        mdv.attr('i2x').v = 0.1
    except:
        pass

    obj.attr(attr) >> cmp.attr('ipr')
    cmp.attr('opr') >> mdv.attr('i1x')


    checkConnectAttr(mdv.attr('ox') , target , limitTarget)
    #mc.connectAttr(mdv.attr('ox') , target , f=True)

    if transform:
        if not mc.objExists('{}'.format(obj.shape)):
            sourceObj = obj
        else:
            sourceObj = core.Dag(obj.shape)

        axis = attr[-1]

        if transform == 'translate':
            limit = 'Trans{}'.format(attr[-1].capitalize())
        elif transform == 'rotate':
            limit = 'Rot{}'.format(attr[-1].capitalize())

        nameLimit = '{}{}'.format(name , attr) if jnt else attr

        rt.setLimitTransform(obj , attr , 0 , 0)
        div = rt.checkObjExist('{}Bsh{}Div'.format(name,side) , 'multiplyDivide')
        div.attr('op').v = 2
        div.attr('i1x').v = 1
        div.attr('i1y').v = 1
        rt.checkObjExist(sourceObj , attr = '{}_Max'.format(attr) , min = None , max = None)
        rt.checkObjExist(sourceObj , attr = '{}_Min'.format(attr) , min = None , max = None)
        sourceObj.attr('{}_Max'.format(nameLimit)).v = valueMax
        sourceObj.attr('{}_Min'.format(nameLimit)).v = valueMin
        sourceObj.attr('{}_Max'.format(nameLimit)) >> cmp.attr('maxR')
        sourceObj.attr('{}_Min'.format(nameLimit)) >> cmp.attr('minR')
        sourceObj.attr('{}_Max'.format(nameLimit)) >> div.attr('i2x')
        sourceObj.attr('{}_Min'.format(nameLimit)) >> div.attr('i2y')
        div.attr('ox') >> mdv.attr('i2x')
        div.attr('oy') >> mdv.attr('i2y')

        if not jnt:
            rt.setLimitTransform(obj , attr , 1 , 1)
            sourceObj.attr('{}_Max'.format(nameLimit)) >> obj.attr('max{}Limit'.format(limit))
            sourceObj.attr('{}_Min'.format(nameLimit)) >> obj.attr('min{}Limit'.format(limit))

        return {'cmp' : cmp , 'mdv' : mdv , 'div' : div}

    return {'cmp' : cmp , 'mdv' : mdv}

def connectSeperatePosNeg(  name = '' ,
                            attr = '' ,
                            source = '' ,
                            targetPositive = '' ,
                            targetNegative = '' ,
                            limitPositive = None ,
                            limitNegative = None ,
                            transform = '' ,
                            valueMax = 0.5 ,
                            valueMin = -0.5 ,
                            autoAttr = '' ,
                            jnt = False , *args):
    #-- setting
    obj = core.Dag(source)
    bshCtrlGrp = core.Dag('BshRigCtrl_Grp')

    targetDict = {  'pos' : '{}'.format(targetPositive) ,
                    'neg' : '{}'.format(targetNegative) ,
                    'limitPos' : limitPositive ,
                    'limitNeg' : limitNegative }

    transformDict = {   'type' : transform ,
                        'max' : valueMax ,
                        'min' : valueMin ,
                        'autoAttr' : autoAttr ,
                        'jnt' : jnt}

    data = {'attr' : attr ,
            'source' : '{}'.format(source) ,
            'target' : targetDict,
            'transform' : transformDict }

    data = {'attr' : attr ,
            'source' : '{}'.format(source) ,
            'target' : targetDict ,
            'transform' : transformDict }

    bshCtrlGrp.addTag(name , '{}'.format(data))

    elem = (name).split('_')
    name = elem[0]

    if len(elem) > 1:
        side = '_{}_'.format(elem[-1])
    else:
        side = '_'

    if not attr:
        attr = name

    cmp = rt.checkObjExist('{}Bsh{}Cmp'.format(name,side) , 'clamp')
    mdv = rt.checkObjExist('{}Bsh{}Mdv'.format(name,side) , 'multiplyDivide')
    rt.checkObjExist(obj , attr = attr)
    #-- R postive , G negative
    try:
        cmp.attr('maxR').v = 10
        cmp.attr('minG').v = -10
        mdv.attr('i2x').v = 0.1
        mdv.attr('i2y').v = -0.1
    except:
        pass

    obj.attr(attr) >> cmp.attr('ipr')
    obj.attr(attr) >> cmp.attr('ipg')
    cmp.attr('opr') >> mdv.attr('i1x')
    cmp.attr('opg') >> mdv.attr('i1y')

    checkConnectAttr(mdv.attr('ox') , targetPositive , limitPositive )
    checkConnectAttr(mdv.attr('oy') , targetNegative , limitNegative )
    
    if transform:
        if not mc.objExists('{}'.format(obj.shape)):
            sourceObj = obj
        else:
            sourceObj = core.Dag(obj.shape)

        axis = attr[-1]

        if transform == 'translate':
            limit = 'Trans{}'.format(attr[-1].capitalize())
        elif transform == 'rotate':
            limit = 'Rot{}'.format(attr[-1].capitalize())

        nameLimit = '{}{}'.format(name , attr) if jnt else attr

        rt.setLimitTransform(obj , attr , 0 , 0)
        div = rt.checkObjExist('{}Bsh{}Div'.format(name,side) , 'multiplyDivide')
        div.attr('op').v = 2
        div.attr('i1x').v = 1
        div.attr('i1y').v = 1
        rt.checkObjExist(sourceObj , attr = '{}_Max'.format(nameLimit) , min = None , max = None)
        rt.checkObjExist(sourceObj , attr = '{}_Min'.format(nameLimit) , min = None , max = None)
        sourceObj.attr('{}_Max'.format(nameLimit)).v = valueMax
        sourceObj.attr('{}_Min'.format(nameLimit)).v = valueMin
        sourceObj.attr('{}_Max'.format(nameLimit)) >> cmp.attr('maxR')
        sourceObj.attr('{}_Min'.format(nameLimit)) >> cmp.attr('minG')
        sourceObj.attr('{}_Max'.format(nameLimit)) >> div.attr('i2x')
        sourceObj.attr('{}_Min'.format(nameLimit)) >> div.attr('i2y')
        div.attr('ox') >> mdv.attr('i2x')
        div.attr('oy') >> mdv.attr('i2y')

        if not jnt:
            rt.setLimitTransform(obj , attr , 1 , 1)
            sourceObj.attr('{}_Max'.format(nameLimit)) >> obj.attr('max{}Limit'.format(limit))
            sourceObj.attr('{}_Min'.format(nameLimit)) >> obj.attr('min{}Limit'.format(limit))

        if autoAttr:
            #-- autoAttrIsNeeded
            bcl = rt.checkObjExist('{}{}Bcl'.format(name , side) , 'blendColors')
            rt.checkObjExist(sourceObj , attr=autoAttr , min=0 , max=1)
            sourceObj.attr(autoAttr) >> bcl.attr('blender')
            obj.attr(attr) >> bcl.attr('c1r')
            obj.attr(attr) >> bcl.attr('c1g')
            bcl.attr('opr') >> cmp.attr('ipr')
            bcl.attr('opg') >> cmp.attr('ipg')

        return {'cmp' : cmp , 'mdv' : mdv , 'div' : div}

    return {'cmp' : cmp , 'mdv' : mdv}

def checkConnectAttr(source , target , limit = None):
    target = core.Dag(target)

    nameAttr = (target.name).split('.')[-1]
    elem = nameAttr.split('_')
    name = elem[0]
    if len(elem) > 1:
        side = '_{}_'.format(elem[-1].capitalize())
    else:
        side = '_'

    nodeConnect = mc.listConnections(target , s=True , d=False , scn=True)
    nodeSource = None
    if nodeConnect:
        if mc.nodeType(nodeConnect[0]) == 'clamp':
            if mc.objExists(nodeConnect[0]+'.limit'):
                nodeSource = mc.listConnections(nodeConnect[0]+'.ipr' , p=True , d=False , scn=True)[0]
                target = '{}.ipr'.format(nodeConnect[0])
                nodeConnect = mc.listConnections(nodeConnect[0] , s=True , d=False , scn=True)
            else:
                nodeSource = mc.listConnections(target , p=True , d=False , scn=True)[0]
                nodeConnect = nodeConnect
        else:
            nodeSource = mc.listConnections(target , p=True , d=False , scn=True)[0]

        if mc.nodeType(nodeConnect[0]) == 'plusMinusAverage':
            member = len(mc.listAttr(nodeConnect[0]+'.i1' , m =True))
            mc.connectAttr(source , '{}.i1[{}]'.format(nodeConnect[0] , member) , f=True) 
            pma = core.Dag(nodeConnect[0])
            nodeSource = pma.attr('o1')

        else:
            pma = cn.createNode('plusMinusAverage' , '{}Total{}Pma'.format(name,side))
            mc.connectAttr(nodeSource , pma.attr('i1[0]'))
            mc.connectAttr(source , pma.attr('i1[1]'))
            mc.connectAttr(pma.attr('o1') , target , f=True)
            nodeSource = pma.attr('o1')

    else:
        nodeSource = source
        mc.connectAttr(nodeSource , target , f=True)

    if limit:
        if not mc.objExists('{}Limit{}Cmp'.format(name , side)):
            limCmp = cn.createNode('clamp' , '{}Limit{}Cmp'.format(name , side))
            limCmp.addBoolTag('limit')
        limCmp.attr('minR').v = limit[0]
        limCmp.attr('maxR').v = limit[1]
        mc.connectAttr(nodeSource , limCmp.attr('ipr') , f=True)
        mc.connectAttr(limCmp.attr('opr') , target , f=True)


def connectBshLocAttrToBshNode(facialLoc = 'FacialAll_Loc' , connection = True , *args):
    BshRigStillGrp = 'BshRigStill_Grp'

    if not mc.objExists(facialLoc):
        fclLoc = cn.locator('FacialAll_Loc')
        fclGrp = rt.addGrp(fclLoc)
        fclLocShape = core.Dag(fclLoc.shape)

        for attr in ('localPositionX' , 'localPositionY' , 'localPositionZ' , 'localScaleX' , 'localScaleY' , 'localScaleZ'):
            fclLocShape.attr(attr).setLockHide()

        bshGrpList = mc.listAttr('FacialAll_Bsn.w' , m = True)

        for attr in bshGrpList:
            fclLocShape.addAttr(attr , 0 , 10)

        for attr in ('tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v'):
            fclLoc.attr(attr).setLockHide()

        BshRigStillGrp = rt.checkObjExist(BshRigStillGrp , 'transform')
        mc.parent(fclGrp, BshRigStillGrp)

    else:
        fclLoc = core.Dag(facialLoc) 
        fclLocShape = core.Dag(fclLoc.shape)

    attrList = mc.listAttr('{}'.format(fclLocShape) , k=True)
    if connection:
        bshNodes = mc.ls(type='blendShape')
        for bshNode in bshNodes:
            if bshNode == 'FacialAll_Bsn':

                for attr in attrList:
                    if mc.objExists('{}.{}'.format(bshNode , attr)):
                        mc.connectAttr('{}.{}'.format(fclLocShape , attr) , '{}.{}'.format(bshNode , attr))
            else:
                attrBsh = mc.listAttr(bshNode+'.w' , m=True)
                for attr in range(len(attrBsh)):
                    if mc.objExists('{}.{}'.format(bshNode , attrBsh[attr])):
                        mc.connectAttr('{}.{}'.format(fclLocShape , attrList[attr]) , '{}.{}'.format(bshNode , attrBsh[attr]))

    else:
        for attr in attrList:
            objOutput = mc.listConnections('{}.{}'.format(fclLocShape , attr) , s = False , d =  True)
            if objOutput:
                mc.disconnectAttr('{}.{}'.format(fclLocShape , attr) , '{}.{}'.format(objOutput[0] , attr))

    return

def runRig(rigDict):
    rig = bsh_utils.RigNode(rigDict)
    targetPos = rig.pos
    targetNeg = rig.neg

    if rig.pos and not rig.neg:
        target = rig.pos
        limitTarget = rig.limitPos

    elif rig.neg and not rig.pos:
        target = rig.neg
        limitTarget = rig.limitNeg

    else:
        target = None

    if target:
        connectBothPosNeg(  name = rig.name ,
                            attr = rig.attr ,
                            source = rig.source ,
                            target = target ,
                            limitTarget = limitTarget ,
                            transform = rig.type ,
                            valueMax = rig.max ,
                            valueMin = rig.min ,
                            jnt = rig.jnt )

    else:
        connectSeperatePosNeg(  name = rig.name ,
                                attr = rig.attr ,
                                source = rig.source ,
                                targetPositive = rig.pos ,
                                targetNegative = rig.neg ,
                                limitPositive = rig.limitPos ,
                                limitNegative = rig.limitNeg ,
                                transform = rig.type ,
                                valueMax = rig.max ,
                                valueMin = rig.min ,
                                autoAttr = rig.autoAttr ,
                                jnt = rig.jnt)

def searchRigInDict(name , local = False):
    #-- path
    data = readData(local=local)
    rigList = data['bshRig']
    for eachDict in rigList:
        rigDict = bsh_utils.RigNode(eachDict)
        if rigDict.name == name:
            return eachDict

def runDefault( eyebrow     = True ,
                eye         = True ,
                mouth       = True ,
                nose        = True ,
                rig         = True ):
    
    #-- path
    data = readData(local=False)
    bshDict = bsh_utils.BshNode(data['bshDict'])
    rigList = data['bshRig']
    rigName = rigNodeList()

    if eyebrow:
        duplicateBlendShape(    bshList     = bshDict.grpMember('Eyebrow_L') , 
                                grpName     = 'Eyebrow' , 
                                side        = 'L' )

        duplicateBlendShape(    bshList     = bshDict.grpMember('Eyebrow_R') , 
                                grpName     = 'Eyebrow' , 
                                side        = 'R' )

        if rig:
            for eachRig in rigName['eyebrow']:
                rigDict = searchRigInDict(eachRig)
                runRig(rigDict)

    if eye:
        duplicateBlendShape(    bshList     = bshDict.grpMember('EyeLids_L') , 
                                grpName     = 'EyeLids' , 
                                side        = 'L' )

        duplicateBlendShape(    bshList     = bshDict.grpMember('EyeLids_R') , 
                                grpName     = 'EyeLids' , 
                                side        = 'R' )

        if rig:
            for eachRig in rigName['eye']:
                rigDict = searchRigInDict(eachRig)
                runRig(rigDict)


    if mouth:
        duplicateBlendShape(    bshList     = bshDict.grpMember('Mouth') , 
                                grpName     = 'Mouth' , 
                                side        = '' )

        duplicateBlendShape(    bshList     = bshDict.grpMember('Lips_L') , 
                                grpName     = 'Lips' , 
                                side        = 'L' )

        duplicateBlendShape(    bshList     = bshDict.grpMember('Lips_R') , 
                                grpName     = 'Lips' , 
                                side        = 'R' )

        if rig:
            for eachRig in rigName['mouth']:
                rigDict = searchRigInDict(eachRig)
                runRig(rigDict)

            if eye:
                for eachRig in rigName['mouthLid']:
                    rigDict = searchRigInDict(eachRig)
                    runRig(rigDict)

    if nose:
        duplicateBlendShape(    bshList     = bshDict.grpMember('Nose') , 
                                grpName     = 'Nose' , 
                                side        = '' )

        duplicateBlendShape(    bshList     = bshDict.grpMember('Nose_L') , 
                                grpName     = 'Nose' , 
                                side        = 'L' )

        duplicateBlendShape(    bshList     = bshDict.grpMember('Nose_R') , 
                                grpName     = 'Nose' , 
                                side        = 'R' )

        duplicateBlendShape(    bshList     = bshDict.grpMember('Head') , 
                                grpName     = 'Head' , 
                                side        = '' )

        if rig:
            for eachRig in rigName['nose']:
                rigDict = searchRigInDict(eachRig)
                runRig(rigDict)

            if mouth:
                for eachRig in rigName['mouthNose']:
                    rigDict = searchRigInDict(eachRig)
                    runRig(rigDict)

    #-- Connect locator to blendshape node
    connectBshLocAttrToBshNode(connection = True)

def runFromData():
    data = readData(local=True)
    bshDict = bsh_utils.BshNode(data['bshDict'])
    rigList = data['bshRig']
    for eachGrp in bshDict.allGrpBshName:
        grpName = eachGrp.split('_')
        duplicateBlendShape(    bshList     = bshDict.grpMember(eachGrp) , 
                                grpName     = grpName[0] , 
                                side        = grpName[-1] )

    if data['bshDict']['InbtwGrp']:
        for eachIb in bshDict.allGrpInbtwName:
            grpName = eachIb.split('_')
            duplicateInbtwBlendShape(   InbtwName   = grpName[0] , 
                                        side        = grpName[-1] , 
                                        target      = bshDict.getIndex(eachIb) ,
                                        value       = bshDict.getValue(eachIb) )

    for eachRig in rigList:
        runRig(eachRig)
    connectBshLocAttrToBshNode(connection = True)


def rigNodeList():
    #-- for run default bsh, it's can choose group to run, so this dict exist just for scope group
    eyebrow = ['EyebrowAllIO_L','EyebrowAllUD_L','EyebrowPull_L','EyebrowTurnFC_L','EyebrowSquintIn_L','EyebrowSquintOut_L',
                        'EyebrowInnerIO_L','EyebrowInnerUD_L','EyebrowInnerTurnFC_L','EyebrowTwist_L',
                        'EyebrowMiddleIO_L','EyebrowMiddleUD_L','EyebrowMiddleTurnFC_L',
                        'EyebrowOuterIO_L','EyebrowOuterUD_L','EyebrowOuterTurnFC_L',
                        'EyebrowAllIO_R','EyebrowAllUD_R','EyebrowPull_R','EyebrowTurnFC_R','EyebrowSquintIn_R','EyebrowSquintOut_R',
                        'EyebrowInnerIO_R','EyebrowInnerUD_R','EyebrowInnerTurnFC_R','EyebrowTwist_R',
                        'EyebrowMiddleIO_R','EyebrowMiddleUD_R','EyebrowMiddleTurnFC_R',
                        'EyebrowOuterIO_R','EyebrowOuterUD_R','EyebrowOuterTurnFC_R']

    eye = ['EyeBallUprFollowUD_L','EyeBallLwrFollowUD_L','EyeBallFollowIO_L','EyeBallIO_L','EyeBallUD_L','EyeBallTurnFC_L',
                        'EyeLidUprUD_L','EyeLidsUprInUD_L','EyeLidsUprMidUD_L','EyeLidsUprOutUD_L',
                        'EyeLidsUprTurnFC_L','EyeLidLwrUD_L','EyeLidsLwrInUD_L',
                        'EyeLidsLwrMidUD_L','EyeLidsLwrOutUD_L','EyeLidsLwrTurnFC_L',
                        'EyeBallUprFollowUD_R','EyeBallLwrFollowUD_R','EyeBallFollowIO_R','EyeBallIO_R','EyeBallUD_R','EyeBallTurnFC_R',
                        'EyeLidUprUD_R','EyeLidsUprInUD_R','EyeLidsUprMidUD_R','EyeLidsUprOutUD_R',
                        'EyeLidsUprTurnFC_R','EyeLidLwrUD_R','EyeLidsLwrInUD_R',
                        'EyeLidsLwrMidUD_R','EyeLidsLwrOutUD_R','EyeLidsLwrTurnFC_R',]

    mouth = ['MouthLR','MouthUD','MouthPull','MouthTurnCF','MouthClench','MouthU','LipsAllUprUD',
                        'LipsUprMidUD','LipsUprCurlIO','LipsAllLwrUD','LipsLwrMidUD','LipsLwrCurlIO',
                        'LipsLwrUD_L','LipsUprUD_L','CornerIO_L','PuffIOAuto_L','PuffIO_L','CornerUD_L','CheekUDAuto_L',
                        'CheekUD_L','PuckerIO_L','LipsPartIO_L','LipUprUD_L','LipLwrUD_L',
                        'LipsLwrUD_R','LipsUprUD_R','CornerIO_R','PuffIOAuto_R','PuffIO_R','CornerUD_R','CheekUDAuto_R',
                        'CheekUD_R','PuckerIO_R','LipsPartIO_R','LipUprUD_R','LipLwrUD_R']

    mouthLid = ['MouthLoLidUD_L','MouthLoLidUD_R']

    mouthNose = ['MouthNoseLR','MouthNoseUD']

    nose = ['NoseLR','NoseUD','NoseStSq','NoseSnarl','NoseUD_L','NoseFC_L','NoseUD_R','NoseFC_R','EyebrowPull']

    return {'eyebrow':eyebrow , 'eye':eye , 'mouth':mouth , 'mouthLid':mouthLid , 'mouthNose':mouthNose , 'nose':nose}

def writeData():
    bshCtrlGrp = 'BshRigCtrl_Grp'

    data = OrderedDict()
    dictBsh = OrderedDict()
    dictRig = OrderedDict()

    bshList = []
    ibList = []
    for allGrp in (mc.listRelatives('Bsh_Grp',c=True)):
        grp = OrderedDict()
        all = mc.listRelatives(allGrp,c=True,type='transform')
        grpList = []
        ibGrpList = []
        for eachGrp in all:
            each = mc.listRelatives(eachGrp , type='transform',c=True,f=True)
            allShapes = mc.listRelatives(each , ad=True , f=True , type='shape')
            shape = [shape for shape in allShapes if not 'Orig' in shape][0]

            bshPlug = mc.listConnections(shape , s=0 , d=1 , p=1 , scn=1 , type='blendShape')[0]
            thNode , tgIp , tgId , tgV , tgGeo = bshPlug.split('.')
            vBsh = re.findall(r'\d+' , tgV)[0]
            #-- Check Inbtw group if vBsh[0] = 5 it's mean inbetween blendshape , vBsh[0] = 6 mean blendshape default
            if vBsh[0] == '5':
                idInbtw = re.findall(r'\d+' , tgId)[0]
                idInbtw = int(idInbtw)
                vInbtw = '0.{}'.format(vBsh[1::])
                vInbtw = float(vInbtw)
                inbtwDict = {  'attrIndex' : idInbtw ,
                                'value' : vInbtw }
                inbDict = {'{}'.format(eachGrp) : inbtwDict}
                ibGrpList.append(inbDict)
            else:
                bshGrpName = eachGrp.split('_')[0]
                grpList.append(bshGrpName)

        if ibGrpList:
            grpName = allGrp.split('Bsh')[0]
            elem = allGrp.split('_')
            if len(elem) == 2:
                sideGrp = ''
            else:
                sideGrp = '_{}'.format(elem[1])
            grpDict = {'{}{}'.format(grpName , sideGrp) : ibGrpList}
            ibList.append(grpDict)
        else:
            grpName = allGrp.split('Bsh')[0]
            elem = allGrp.split('_')
            if len(elem) == 2:
                sideGrp = ''
            else:
                sideGrp = '_{}'.format(elem[1])
            grpDict = {'{}{}'.format(grpName , sideGrp) : grpList}
            bshList.append(grpDict)
        
    dictBsh = {'bshGrp':bshList , 'InbtwGrp':ibList}
    dictRig = []
    allAttr = mc.listAttr(bshCtrlGrp , cb=False , ud=True)
    for attr in allAttr:
        data = mc.getAttr('{}.{}'.format(bshCtrlGrp , attr))
        dataDict = eval(data)
        dataDict['name'] = '{}'.format(attr)
        dictRig.append(dataDict)
    data = {'bshDict':dictBsh , 'bshRig':dictRig}

    context = context_info.ContextPathInfo()
    if context.name:
        projectPath = os.environ['RFPROJECT']
        fileWorkPath = (context.path.name()).replace('$RFPROJECT',projectPath)
        path = fileWorkPath + '/rig/rigData/bshRigData.yml'

    else:
        corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        path = '{}/rf_maya/rftool/rig/rigScript/template/bshDict.yml'.format(corePath)
        # path = 'P:/Hanuman/rnd/rigDev/DevScript/fah_test/testDictJa/test_dump.yml'

    print 'file data dumper >> {}'.format(path)
    file_utils.ymlDumper(path , data)
    return

def readData(local = False):
    context = context_info.ContextPathInfo()
    if context.name:
        if local:
            projectPath = os.environ['RFPROJECT']
            fileWorkPath = (context.path.name()).replace('$RFPROJECT',projectPath)
            path = fileWorkPath + '/rig/rigData/bshRigData.yml'
            data = file_utils.ymlLoader(path)
            print '---------- Read data from local ----------'
        else:
            corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
            # path = 'P:/Hanuman/rnd/rigDev/DevScript/template/bshRigData.yml'
            # data = file_utils.ymlLoader(path)
            path = '{}/rf_maya/rftool/rig/rigScript/template/bshRigData.yml'.format(corePath)
            data = file_utils.ymlLoader(path)
            print '---------- Read data from template  ----------'

    else:
        corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        # path = 'P:/Hanuman/rnd/rigDev/DevScript/template/bshRigData.yml'
        # data = file_utils.ymlLoader(path)
        path = '{}/rf_maya/rftool/rig/rigScript/template/bshDict.yml'.format(corePath)
        data = file_utils.ymlLoader(path)
        print "---------- This character doesn't have data, Read data from template ----------"

    return data