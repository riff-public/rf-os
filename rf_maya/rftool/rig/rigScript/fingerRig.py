import maya.cmds as mc
import maya.mel as mm
import rigTools
import core 
import fkRig
import fkChain
reload(fkChain)
reload(fkRig)
reload( core )
reload( rigTools )
cn = core.Node()

# -------------------------------------------------------------------------------------------------------------
#
#  FINGER RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__(self , fngr          = 'Index' ,
                        parent        = 'Hand_L_Jnt' , 
                        armCtrl       = 'Arm_L_Ctrl' ,
                        innerCup      = 'HandCupInner_L_Jnt' , 
                        outerCup      = 'HandCupOuter_L_Jnt' ,
                        innerValue    = 1 , 
                        outerValue    = 1 ,
                        ctrlGrp       = 'Ctrl_Grp' ,
                        elem          = '' ,
                        side          = 'L' ,
                        size          = 1 ,
                        no_tip        = 1,
                 ):

        #-- Info
        name = '{}{}'.format(fngr , elem)

        if side == '':
            side = '_'
        else:
            side = '_{}_'.format(side)

        if 'L' in side:
            # old value 0.025
            valueSide = 0.1
        elif 'R' in side:
            # old value -0.025
            valueSide = -0.1

        self.fngrJntDict = []
        self.fngrCtrlDict = []
        self.fngrGmblDict = []
        self.fngrZroDict = []
        self.fngrDrvDict = []
        self.fngrDrvAMdvDict = []
        self.fngrDrvBMdvDict = []
        self.fngrRxPmaDict = []
        self.fngrRyPmaDict = []
        self.fngrRzPmaDict = []
        self.fngrTyPmaDict = []

        self.fingerJntObj = []

        slideValue = { '1' : 0 , '2' : 3.5 , '3' : -7 , '4' : 3.5 , '5' : 0 , '6' : 0 }
        scruchValue = { '1' : 1 , '2' : 3.5 , '3' : -8 , '4' : -8 , '5' : 0 , '6' : 0 }
        #default twistAmp 0.1
        twistAmp = 2

        #-- FkChain -------------------------------------------------------------------------------
        if side == '_':
            side = ''
        if '_L_' in side:
            sideFk = 'L'
        elif '_R_' in side:
            sideFk = 'R'

        fkChain_Obj = fkChain.Run(   name       = fngr ,
                                    side       = sideFk ,
                                    tmpJnt     = [ ] ,
                                    parent     = '' ,
                                    ctrlGrp    = ctrlGrp , 
                                    axis       = 'y' ,
                                    shape      = 'circle' ,
                                    jnt_ctrl   = 0 ,
                                    size       = size ,
                                    no_tip     = no_tip,
                                    color      = 'red',
                                    localWorld = 0,
                                    defaultWorld = 1,
                                    dtl = 0,
                                    dtlShape = '',
                                    dtlColor = '',
                                    stretch     =1,)

        self.fngrJntDict = fkChain_Obj.jointChain
        self.fngrCtrlDict = fkChain_Obj.ctrlArray
        self.fngrGmblDict = fkChain_Obj.gmblArray
        self.fngrZroDict = fkChain_Obj.zroArray
        self.fngrCtrlGrp = fkChain_Obj.ctrlGrpArray
        self.fngrMember = len(fkChain_Obj.jointChain)
        self.fngrCtrlObj =  fkChain_Obj.ctrlGrp

        #-- Correct ctrlGrp position
        child = mc.listRelatives(self.fngrCtrlObj , c=True)
        mc.parent(child,w=True)
        self.fngrCtrlObj.snap(self.fngrCtrlGrp[0])
        self.fngrCtrlGrp[0].unlockAttrs('tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        mc.parent(child , self.fngrCtrlObj)
        self.fngrCtrlGrp[0].lockAttrs('tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

    
        #-- Create Controls -------------------------------------------------------------------------------
        self.fngrMainCtrl = rigTools.createCtrl('{}Attr{}Ctrl'.format(name,side) , 'stick' , 'blue')
        self.fngrMainZro = rigTools.addGrp(self.fngrMainCtrl)
        self.fngrMainZro.snap(self.fngrJntDict[0])

        #-- Adjust Shape Controls -------------------------------------------------------------------------------
        if 'L' in side:
            self.fngrMainCtrl.rotateShape(90 , 0 , 0)
        else:
            self.fngrMainCtrl.rotateShape(-90 , 0 , 0)

        #-- Adjust Shape Main Control -------------------------------------------------------------------------------
        self.fngrMainCtrl.scaleShape( size * 0.15)

        #-- Add Attribute -------------------------------------------------------------------------------
        self.fngrMainCtrl.addAttr( 'fold' )
        self.fngrMainCtrl.addAttr( 'twist' )
        self.fngrMainCtrl.addAttr( 'slide' )
        self.fngrMainCtrl.addAttr( 'scruch' )
        self.fngrMainCtrl.addAttr( 'stretch' )

        for i in range(self.fngrMember):
            self.fngrMainCtrl.addTitleAttr('Finger{}'.format(i))
            self.fngrMainCtrl.addAttr( 'fold{}'.format(i) )
            self.fngrMainCtrl.addAttr( 'twist{}'.format(i) )
            self.fngrMainCtrl.addAttr( 'spread{}'.format(i) )

        self.fngrMainCtrlShape = core.Dag(self.fngrMainCtrl.shape)
        self.fngrMainCtrlShape.addAttr('detailCtrl' , 0 , 1)
        self.fngrCtrlGrp[0].unlockAttrs('v')
        self.fngrMainCtrlShape.attr('detailCtrl') >> self.fngrCtrlGrp[0].attr('v')
        self.fngrCtrlGrp[0].lockAttrs('v')


        #-- Adjust Shape Controls, Scale Control, ScaleGmbl Control, setRotateOrder
        for ctrl, gmbl, objOrder, jntOrder in zip(self.fngrCtrlDict, self.fngrGmblDict, self.fngrCtrlDict, self.fngrJntDict):
            ctrl.scaleShape( size * 0.08 )
            gmbl.scaleShape( size * 0.08 )
            objOrder.setRotateOrder( 'xyz' )
            jntOrder.setRotateOrder( 'xyz' )

        #-- Read data for attr setting
        dataDict = readData()

        #-- Rig process
        #-- Unlock ofsetGrp -------------------------------------------------------------------------------
        for i in fkChain_Obj.ofstArray:
            Drv = i
            Drv.unlockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
            self.fngrDrvDict.append(Drv)
        for i in range(len(self.fngrDrvDict)):

            if i == 0:
                self.fngrDrvAMdv = cn.createNode( 'multiplyDivide' , '{}DrvA{}Mdv'.format( name , side ))
                self.fngrDrvBMdv = cn.createNode( 'multiplyDivide' , '{}DrvB{}Mdv'.format( name , side ))
                self.fngrRxPma = cn.createNode( 'plusMinusAverage' , '{}Rx{}Pma'.format( name , side ))
                self.fngrRyPma = cn.createNode( 'plusMinusAverage' , '{}Ry{}Pma'.format( name , side ))
                self.fngrRzPma = cn.createNode( 'plusMinusAverage' , '{}Rz{}Pma'.format( name , side ))
                self.fngrTyPma = cn.createNode( 'plusMinusAverage' , '{}Ty{}Pma'.format( name , side ))

                self.fngrDrvAMdv.attr('ox') >> self.fngrRxPma.attr('i1[0]')
                self.fngrDrvAMdv.attr('oy') >> self.fngrRyPma.attr('i1[0]')
                self.fngrDrvAMdv.attr('oz') >> self.fngrRzPma.attr('i1[0]')
                self.fngrDrvBMdv.attr('ox') >> self.fngrRxPma.attr('i1[1]')
                self.fngrDrvBMdv.attr('oy') >> self.fngrRxPma.attr('i1[2]')
                self.fngrDrvBMdv.attr('oz') >> self.fngrTyPma.attr('i1[0]')

                self.fngrRxPma.attr('o1') >> self.fngrDrvDict[i].attr('rx')
                self.fngrRyPma.attr('o1') >> self.fngrDrvDict[i].attr('ry')
                self.fngrRzPma.attr('o1') >> self.fngrDrvDict[i].attr('rz')
                self.fngrTyPma.attr('o1') >> self.fngrDrvDict[i].attr('ty')
                
                #-- Adjust Attribute Value
                self.fngrDrvAMdv.attr('i2x').value = -9
                self.fngrDrvAMdv.attr('i2y').value = twistAmp
                self.fngrDrvAMdv.attr('i2z').value = 1
                self.fngrDrvBMdv.attr('i2x').value = slideValue['{}'.format(i+1)]
                self.fngrDrvBMdv.attr('i2y').value = scruchValue['{}'.format(i+1)]
                self.fngrDrvBMdv.attr('i2z').value = valueSide
                # self.fngrDrvBMdv.attr('i2z').value = 0.1

                ##-- Info
                self.fngrDrvAMdvDict.append( self.fngrDrvAMdv )
                self.fngrDrvBMdvDict.append( self.fngrDrvBMdv )
                self.fngrRxPmaDict.append( self.fngrRxPma )
                self.fngrRyPmaDict.append( self.fngrRyPma )
                self.fngrRzPmaDict.append( self.fngrRzPma )
                self.fngrTyPmaDict.append( self.fngrTyPma )

                self.fngrFoldMdv = cn.createNode( 'multiplyDivide' , '{}Fold{}Mdv'.format( name , side ))
                self.fngrTwistMdv = cn.createNode( 'multiplyDivide' , '{}Twist{}Mdv'.format( name , side ))
                self.fngrTwistMdv.attr('i2x').value = twistAmp
                
                self.fngrMainCtrl.attr('fold') >> self.fngrFoldMdv.attr('i1x')
                self.fngrMainCtrl.attr('twist') >> self.fngrTwistMdv.attr('i1x')
                self.fngrFoldMdv.attr('i2x').value = -9

                ## --valueSide for L and R
                mc.setAttr ("{}{}Strt{}Mdv.i2x".format(fngr, i+1, side), valueSide)


            else:
                #-- Create Node
                #-- DrvA == fold(x) , twist(y) , spread(z)
                #-- DrvB == slide(x) , scruch(y) , stretch(z)
                self.fngrDrvAMdv = cn.createNode( 'multiplyDivide' , '{}{}DrvA{}Mdv'.format( name , i, side ))
                self.fngrDrvBMdv = cn.createNode( 'multiplyDivide' , '{}{}DrvB{}Mdv'.format( name , i, side ))
                self.fngrRxPma = cn.createNode( 'plusMinusAverage' , '{}{}Rx{}Pma'.format( name , i, side ))
                self.fngrRyPma = cn.createNode( 'plusMinusAverage' , '{}{}Ry{}Pma'.format( name , i, side ))
                self.fngrRzPma = cn.createNode( 'plusMinusAverage' , '{}{}Rz{}Pma'.format( name , i, side ))
                self.fngrTyPma = cn.createNode( 'plusMinusAverage' , '{}{}Ty{}Pma'.format( name , i, side ))

                self.fngrDrvAMdv.attr('ox') >> self.fngrRxPma.attr('i1[0]')
                self.fngrDrvAMdv.attr('oy') >> self.fngrRyPma.attr('i1[0]')
                self.fngrDrvAMdv.attr('oz') >> self.fngrRzPma.attr('i1[0]')
                self.fngrDrvBMdv.attr('ox') >> self.fngrRxPma.attr('i1[1]')
                self.fngrDrvBMdv.attr('oy') >> self.fngrRxPma.attr('i1[2]')
                self.fngrDrvBMdv.attr('oz') >> self.fngrTyPma.attr('i1[0]')

                self.fngrRxPma.attr('o1') >> self.fngrDrvDict[i].attr('rx')
                self.fngrRyPma.attr('o1') >> self.fngrDrvDict[i].attr('ry')
                self.fngrRzPma.attr('o1') >> self.fngrDrvDict[i].attr('rz')
                self.fngrTyPma.attr('o1') >> self.fngrDrvDict[i].attr('ty')


                #-- Adjust Attribute Value
                self.fngrDrvAMdv.attr('i2x').value = -9
                self.fngrDrvAMdv.attr('i2y').value = twistAmp
                self.fngrDrvAMdv.attr('i2z').value = 1
                self.fngrDrvBMdv.attr('i2x').value = slideValue['{}'.format(i+1)]
                self.fngrDrvBMdv.attr('i2y').value = scruchValue['{}'.format(i+1)]
                self.fngrDrvBMdv.attr('i2z').value = valueSide
                # self.fngrDrvBMdv.attr('i2z').value = 0.1

                ##-- connect Stretch for finger
                mc.connectAttr('{}{}Strt{}Pma.o1'.format(fngr, i, side), self.fngrTyPma.attr('i1[1]'))
                ## -- valueSide for L and R
                mc.setAttr ("{}{}Strt{}Mdv.i2x".format(fngr, i+1, side), valueSide)

                ##-- Info
                self.fngrDrvAMdvDict.append( self.fngrDrvAMdv )
                self.fngrDrvBMdvDict.append( self.fngrDrvBMdv )
                self.fngrRxPmaDict.append( self.fngrRxPma )
                self.fngrRyPmaDict.append( self.fngrRyPma )
                self.fngrRzPmaDict.append( self.fngrRzPma )
                self.fngrTyPmaDict.append( self.fngrTyPma )

            self.fngrMainCtrl.attr('fold{}'.format(i)) >> self.fngrDrvAMdv.attr('i1x')
            self.fngrMainCtrl.attr('twist{}'.format(i)) >> self.fngrDrvAMdv.attr('i1y')
            self.fngrMainCtrl.attr('spread{}'.format(i)) >> self.fngrDrvAMdv.attr('i1z')
                    
            self.fngrMainCtrl.attr('slide') >> self.fngrDrvBMdv.attr('i1x')
            self.fngrMainCtrl.attr('scruch') >> self.fngrDrvBMdv.attr('i1y')
            self.fngrMainCtrl.attr('stretch') >> self.fngrDrvBMdv.attr('i1z')

        for i in range(self.fngrMember) :
                self.fngrFoldMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[3]')
                self.fngrTwistMdv.attr('ox') >> self.fngrRyPmaDict[i].attr('i1[1]')
        
        #-- Create Attribute Main Controls
        if armCtrl :
            self.armCtrl = core.Dag(armCtrl)
            self.armCtrlShape = core.Dag(self.armCtrl.shape)
            
            if not mc.objExists('{}.{}'.format(self.armCtrl , 'Finger')):
                self.armCtrl.addTitleAttr('Finger')

            for attr in ('fist','relax','slide','scruch','baseSpread','spread','baseBreak','break','baseFlex','flex') :
                if not mc.objExists( '{}.{}'.format( self.armCtrl , attr )) :
                    self.armCtrl.addAttr(attr)
            
            self.armCtrlShape.addTitleAttr( '{}'.format(fngr) )
            
            #-- Fist
            for i in range(self.fngrMember):
                fngrNum = '{}{}'.format(fngr,i+1)

                self.fistMainMdv = cn.createNode( 'multiplyDivide' , '{}{}Fist{}Mdv'.format( name , i+1 , side ))
                
                self.armCtrl.attr('fist') >> self.fistMainMdv.attr('i1x')
                self.armCtrl.attr('fist') >> self.fistMainMdv.attr('i1y')
                self.armCtrl.attr('fist') >> self.fistMainMdv.attr('i1z')

                self.fistMainMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[4]')
                self.fistMainMdv.attr('oy') >> self.fngrRyPmaDict[i].attr('i1[2]')
                self.fistMainMdv.attr('oz') >> self.fngrRzPmaDict[i].attr('i1[1]')

                for fistAttr in ('x','y','z'):
                    self.armCtrlShape.addAttr( '{}FistR{}'.format(fngrNum,fistAttr))
                    self.armCtrlShape.attr('{}FistR{}'.format(fngrNum,fistAttr)) >> self.fistMainMdv.attr('i2{}'.format(fistAttr))
                    self.armCtrlShape.attr('{}FistR{}'.format(fngrNum,fistAttr)).v = dataDict[fngr]['{}FistR{}'.format(i+1,fistAttr)]
            
            #-- Relax
            for i in range(self.fngrMember):
                fngrNum = '{}{}'.format(fngr,i+1)

                self.relaxMainMdv = cn.createNode( 'multiplyDivide' , '{}{}Relax{}Mdv'.format( name , i+1 , side ))
                
                self.armCtrl.attr('relax') >> self.relaxMainMdv.attr('i1x')
                self.armCtrl.attr('relax') >> self.relaxMainMdv.attr('i1y')
                self.armCtrl.attr('relax') >> self.relaxMainMdv.attr('i1z')

                self.relaxMainMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[5]')
                self.relaxMainMdv.attr('oy') >> self.fngrRyPmaDict[i].attr('i1[3]')
                self.relaxMainMdv.attr('oz') >> self.fngrRzPmaDict[i].attr('i1[2]')

                for relaxAttr in ('x','y','z'):
                    self.armCtrlShape.addAttr( '{}RelaxR{}'.format(fngrNum,relaxAttr))
                    self.armCtrlShape.attr('{}RelaxR{}'.format(fngrNum,relaxAttr)) >> self.relaxMainMdv.attr('i2{}'.format(relaxAttr))
                    self.armCtrlShape.attr('{}RelaxR{}'.format(fngrNum,relaxAttr)).v = dataDict[fngr]['{}RelaxR{}'.format(i+1,relaxAttr)]
            
            #-- Slide
            for i in range(self.fngrMember):
                fngrNum = '{}{}'.format(fngr,i+1)

                self.slideMainMdv = cn.createNode( 'multiplyDivide' , '{}{}Slide{}Mdv'.format( name , i+1 , side ))
                
                self.armCtrl.attr('slide') >> self.slideMainMdv.attr('i1x')
                self.armCtrl.attr('slide') >> self.slideMainMdv.attr('i1y')
                self.armCtrl.attr('slide') >> self.slideMainMdv.attr('i1z')

                self.slideMainMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[6]')
                self.slideMainMdv.attr('oy') >> self.fngrRyPmaDict[i].attr('i1[4]')
                self.slideMainMdv.attr('oz') >> self.fngrRzPmaDict[i].attr('i1[3]')

                for slideAttr in ('x','y','z'):
                    self.armCtrlShape.addAttr( '{}SlideR{}'.format(fngrNum,slideAttr))
                    self.armCtrlShape.attr('{}SlideR{}'.format(fngrNum,slideAttr)) >> self.slideMainMdv.attr('i2{}'.format(slideAttr))
                    self.armCtrlShape.attr('{}SlideR{}'.format(fngrNum,slideAttr)).v = dataDict[fngr]['{}SlideR{}'.format(i+1,slideAttr)]
            
            #-- Scruch
            for i in range(self.fngrMember):
                fngrNum = '{}{}'.format(fngr,i+1)

                self.scruchMainMdv = cn.createNode( 'multiplyDivide' , '{}{}Scruch{}Mdv'.format( name , i+1 , side ))
                
                self.armCtrl.attr('scruch') >> self.scruchMainMdv.attr('i1x')
                self.armCtrl.attr('scruch') >> self.scruchMainMdv.attr('i1y')
                self.armCtrl.attr('scruch') >> self.scruchMainMdv.attr('i1z')

                self.scruchMainMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[7]')
                self.scruchMainMdv.attr('oy') >> self.fngrRyPmaDict[i].attr('i1[5]')
                self.scruchMainMdv.attr('oz') >> self.fngrRzPmaDict[i].attr('i1[4]')

                for scruchAttr in ('x','y','z'):
                    self.armCtrlShape.addAttr( '{}ScruchR{}'.format(fngrNum,scruchAttr))
                    self.armCtrlShape.attr('{}ScruchR{}'.format(fngrNum,scruchAttr)) >> self.scruchMainMdv.attr('i2{}'.format(scruchAttr))
                    self.armCtrlShape.attr('{}ScruchR{}'.format(fngrNum,scruchAttr)).v = dataDict[fngr]['{}ScruchR{}'.format(i+1,scruchAttr)]
            
            #-- Spread
            self.armCtrlShape.addAttr( '{}BaseSpread'.format(fngr))
            self.armCtrlShape.addAttr( '{}Spread'.format(fngr))
            self.spreadMainMdv = cn.createNode( 'multiplyDivide' , '{}Spread{}Mdv'.format( name , side ))
            self.armCtrl.attr('baseSpread') >> self.spreadMainMdv.attr('i1x')
            self.armCtrl.attr('spread') >> self.spreadMainMdv.attr('i1y')
            self.spreadMainMdv.attr('ox') >> self.fngrRzPmaDict[0].attr('i1[5]')
            self.spreadMainMdv.attr('oy') >> self.fngrRzPmaDict[1].attr('i1[5]')
            self.armCtrlShape.attr('{}BaseSpread'.format(fngr)) >> self.spreadMainMdv.attr('i2x')
            self.armCtrlShape.attr('{}Spread'.format(fngr)) >> self.spreadMainMdv.attr('i2y')
            self.armCtrlShape.attr('{}BaseSpread'.format(fngr)).v = dataDict[fngr]['BaseSpread']
            self.armCtrlShape.attr('{}Spread'.format(fngr)).v = dataDict[fngr]['Spread']
            
            #-- Break
            self.armCtrlShape.addAttr( '{}BaseBreak'.format(fngr))
            self.armCtrlShape.addAttr( '{}Break'.format(fngr))
            self.breakMainMdv = cn.createNode( 'multiplyDivide' , '{}Break{}Mdv'.format( name , side ))
            self.armCtrl.attr('baseBreak') >> self.breakMainMdv.attr('i1x')
            self.armCtrl.attr('break') >> self.breakMainMdv.attr('i1y')
            self.breakMainMdv.attr('ox') >> self.fngrRzPmaDict[0].attr('i1[6]')
            self.breakMainMdv.attr('oy') >> self.fngrRzPmaDict[1].attr('i1[6]')
            self.armCtrlShape.attr('{}BaseBreak'.format(fngr)) >> self.breakMainMdv.attr('i2x')
            self.armCtrlShape.attr('{}Break'.format(fngr)) >> self.breakMainMdv.attr('i2y')
            self.armCtrlShape.attr('{}BaseBreak'.format(fngr)).v = dataDict[fngr]['BaseBreak']
            self.armCtrlShape.attr('{}Break'.format(fngr)).v = dataDict[fngr]['Break']

            #-- Flex
            self.armCtrlShape.addAttr( '{}BaseFlex'.format(fngr))
            self.armCtrlShape.addAttr( '{}Flex'.format(fngr))
            self.flexMainMdv = cn.createNode( 'multiplyDivide' , '{}Flex{}Mdv'.format( name , side ))
            self.armCtrl.attr('baseFlex') >> self.flexMainMdv.attr('i1x')
            self.armCtrl.attr('flex') >> self.flexMainMdv.attr('i1y')
            self.flexMainMdv.attr('ox') >> self.fngrRxPmaDict[0].attr('i1[8]')
            self.flexMainMdv.attr('oy') >> self.fngrRxPmaDict[1].attr('i1[8]')
            self.armCtrlShape.attr('{}BaseFlex'.format(fngr)) >> self.flexMainMdv.attr('i2x')
            self.armCtrlShape.attr('{}Flex'.format(fngr)) >> self.flexMainMdv.attr('i2y')
            self.armCtrlShape.attr('{}BaseFlex'.format(fngr)).v = dataDict[fngr]['BaseFlex']
            self.armCtrlShape.attr('{}Flex'.format(fngr)).v = dataDict[fngr]['Flex']



        #-- Create ParentConstraint Inner or Outter Grp -------------------------------------------------------------------------------
        parCons = []
        sclCons = []
        if innerCup:
            parCons = mc.parentConstraint(innerCup , self.fngrCtrlObj , mo=True)
            sclCons = mc.scaleConstraint(innerCup , self.fngrCtrlObj , mo=True)

        if outerCup:
            parCons = mc.parentConstraint(outerCup , self.fngrCtrlObj , mo=True)
            sclCons = mc.scaleConstraint(outerCup , self.fngrCtrlObj , mo=True)

        if parCons:
            self.parCons = core.Dag(parCons[0])
            self.sclCons = core.Dag(sclCons[0])

            # Set Shortest
            self.parCons.attr('interpType').value = 2

            if armCtrl:
                self.armCtrl = core.Dag(armCtrl)
                self.armCtrlShape = core.Dag(self.armCtrl.shape)
                conslen = 'W0'

                if innerCup:
                    inner = core.Dag(innerCup)
                    conslen = 'W1'
                    attrName = inner.getName()
                    # print attrName
                    # print inner
                    self.armCtrlShape.addAttr('{}{}ParCons'.format(name , attrName))
                    self.armCtrlShape.addAttr('{}{}ScaCons'.format(name , attrName))
                    self.armCtrlShape.attr('{}{}ParCons'.format(name , attrName)).v = dataDict[name]['{}ParCons'.format(attrName)]
                    self.armCtrlShape.attr('{}{}ScaCons'.format(name , attrName)).v = dataDict[name]['{}ScaCons'.format(attrName)]
                    self.armCtrlShape.attr('{}{}ParCons'.format(name , attrName)) >> self.parCons.attr('{}W0'.format(innerCup))
                    self.armCtrlShape.attr('{}{}ScaCons'.format(name , attrName)) >> self.sclCons.attr('{}W0'.format(innerCup))

                if outerCup:
                    outer = core.Dag(outerCup)
                    attrName = outer.getName()
                    self.armCtrlShape.addAttr('{}{}ParCons'.format(name , attrName))
                    self.armCtrlShape.addAttr('{}{}ScaCons'.format(name , attrName))
                    self.armCtrlShape.attr('{}{}ParCons'.format(name , attrName)).v = dataDict[name]['{}ParCons'.format(attrName)]
                    self.armCtrlShape.attr('{}{}ScaCons'.format(name , attrName)).v = dataDict[name]['{}ScaCons'.format(attrName)]
                    self.armCtrlShape.attr('{}{}ParCons'.format(name , attrName)) >> self.parCons.attr('{}{}'.format(outerCup , conslen))
                    self.armCtrlShape.attr('{}{}ScaCons'.format(name , attrName)) >> self.sclCons.attr('{}{}'.format(outerCup , conslen))

        else:
            mc.parentConstraint(parent , self.fngrCtrlObj , mo=True)
            mc.scaleConstraint(parent , self.fngrCtrlObj , mo=True)


        #-- Adjust Hierarchy
        self.allFngrGrp = 'Finger{}Ctrl{}Grp'.format( elem , side )
        if not mc.objExists( self.allFngrGrp) :
            self.allFngrGrp = cn.createNode( 'transform' , 'Finger{}Ctrl{}Grp'.format( elem , side ))
            self.allFngrGrp.snap( parent )
            self.allFngrGrp.parent( ctrlGrp )
            
        mc.parent( self.fngrMainZro  , self.fngrCtrlObj )
        mc.parent( self.fngrCtrlObj , self.allFngrGrp )
        mc.parent( self.fngrJntDict[0] , parent )

        for grp in self.fngrDrvDict :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        self.fngrMainCtrl.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        mc.select( cl = True )

class HandCup( object ):

    def __init__(self , name          = 'HandCupInner' ,
                        handCupTmpJnt = 'HandCupInner_L_TmpJnt' ,
                        armCtrl       = 'Arm_L_Ctrl' ,
                        parent        = 'Hand_L_Jnt' ,
                        ctrlGrp       = '' ,
                        elem          = '' ,
                        side          = 'L' ,
                        size          = 1
                 ):

        #-- Info
        if side == '':
            side = '_'
        else:
            side = '_{}_'.format(side)

        #-- Create Main Group
        self.handCupCtrlGrp = cn.createNode('transform' , '{}{}Ctrl{}Grp'.format(name , elem , side))
        mc.parentConstraint( parent , self.handCupCtrlGrp , mo = False)
        mc.scaleConstraint( parent , self.handCupCtrlGrp , mo = False)

        #-- Create Controls
        self.handCupCtrl = rigTools.createCtrl( '{}{}{}Ctrl'.format(name , elem , side) , 'cube' , 'yellow' )
        self.handCupGmbl = rigTools.addGimbal( self.handCupCtrl )
        self.handCupZro = rigTools.addGrp( self.handCupCtrl )
        self.handCupDrv = rigTools.addGrp( self.handCupCtrl , 'Drv' )
        self.handCupZro.snap( handCupTmpJnt )

        #-- Adjust Shape Control
        for ctrl in (self.handCupCtrl , self.handCupGmbl):
            ctrl.scaleShape( size * 0.35 )

        #-- Create Joint
        self.handCupJnt = cn.joint( '{}{}{}Jnt'.format(name , elem , side) , handCupTmpJnt)

        #-- Adjust Shape Controls
        for ctrl in (self.handCupCtrl , self.handCupGmbl):
            ctrl.scaleShape( size * 0.6)

        #-- Adjust Rotate Order
        for obj in (self.handCupCtrl , self.handCupJnt):
            obj.setRotateOrder( 'xyz' )

        #-- Rig process
        mc.parentConstraint(self.handCupGmbl , self.handCupJnt , mo = False)
        mc.scaleConstraint(self.handCupGmbl , self.handCupJnt , mo = False)
        self.handCupJnt.attr('ssc').value = 0

        if armCtrl:
            self.armCtrl = core.Dag(armCtrl)
            self.armCtrlShape = core.Dag(self.armCtrl.shape)

            if not mc.objExists('{}.{}'.format(self.armCtrl , 'Finger')):
                self.armCtrl.addTitleAttr('Finger')

            for attr in ('fist','relax','slide','scruch','baseSpread','spread','baseBreak','break','baseFlex','flex') :
                if not mc.objExists( '{}.{}'.format( self.armCtrl , attr )) :
                    self.armCtrl.addAttr(attr)

            if not mc.objExists('{}.{}'.format(self.armCtrlShape , '{}{}FistRx'.format(name , elem))):
                self.armCtrlShape.addAttr('{}{}FistRx'.format(name , elem))
                self.armCtrlShape.addAttr('{}{}FistRy'.format(name , elem))
                self.armCtrlShape.addAttr('{}{}FistRz'.format(name , elem))
                if 'Inner' in name:
                    self.armCtrlShape.attr('{}{}FistRx'.format(name , elem)).value = 0
                    self.armCtrlShape.attr('{}{}FistRy'.format(name , elem)).value = 0
                    self.armCtrlShape.attr('{}{}FistRz'.format(name , elem)).value = 0
                else:
                    self.armCtrlShape.attr('{}{}FistRx'.format(name , elem)).value = 0
                    self.armCtrlShape.attr('{}{}FistRy'.format(name , elem)).value = -1
                    self.armCtrlShape.attr('{}{}FistRz'.format(name , elem)).value = 0

        self.handCupMdv = cn.createNode('multiplyDivide' , '{}{}{}Mdv'.format(name , elem , side))
        #self.handCupPma = rt.createNode('plusMinusAverage' , '{}{}{}Pma'.format(name , elem , side))

        self.armCtrl.attr('fist') >> self.handCupMdv.attr('i1x')
        self.armCtrl.attr('fist') >> self.handCupMdv.attr('i1y')
        self.armCtrl.attr('fist') >> self.handCupMdv.attr('i1z')

        self.armCtrlShape.attr('{}{}FistRx'.format(name , elem)) >> self.handCupMdv.attr('i2x')
        self.armCtrlShape.attr('{}{}FistRy'.format(name , elem)) >> self.handCupMdv.attr('i2y')
        self.armCtrlShape.attr('{}{}FistRz'.format(name , elem)) >> self.handCupMdv.attr('i2z')

        # self.handCupMdv.attr('ox') >> self.handCupPma.attr('i1[0]')
        # self.handCupMdv.attr('oy') >> self.handCupPma.attr('i1[1]')
        #self.handCupPma.attr('o1') >> self.handCupDrv.attr('ry')
        self.handCupMdv.attr('ox') >> self.handCupDrv.attr('rx')
        self.handCupMdv.attr('oy') >> self.handCupDrv.attr('ry')
        self.handCupMdv.attr('oz') >> self.handCupDrv.attr('rz')

        #-- Adjust Hierarchy
        if not mc.objExists( 'Finger{}Ctrl{}Grp'.format(elem , side)):
            self.allFngrGrp = cn.createNode('transform' , 'Finger{}Ctrl{}Grp'.format(elem , side))
            self.allFngrGrp.snap( parent )
            self.allFngrGrp.parent( ctrlGrp )
        else:
            self.allFngrGrp = 'Finger{}Ctrl{}Grp'.format(elem , side)

        mc.parent( self.handCupZro , self.handCupCtrlGrp )
        mc.parent( self.handCupCtrlGrp , self.allFngrGrp )
        mc.parent( self.handCupJnt , parent )

        #-- Cleanup
        self.handCupCtrl.lockHideAttrs('sx' , 'sy' , 'sz' , 'v')

        for grp in (self.handCupJnt , self.handCupZro , self.handCupDrv):
            grp.lockAttrs('tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')

        mc.select( cl = True)

def readData(local = False):
    from rf_utils import file_utils
    reload(file_utils)
    import os
    corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
    path = '{}/rf_maya/rftool/rig/rigScript/template/fingerDict.yml'.format(corePath)
    data = file_utils.ymlLoader(path)

    return data