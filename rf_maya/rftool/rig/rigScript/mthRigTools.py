import maya.cmds as mc
import core
import array 

cn = core.Node()
class MthRigTools( object ):
	def __init__( self ):
		self.ctrlSels = [		'MouthCnrBsh_L_Ctrl', 
								'Jaw_Ctrl']

		self.ctrlShapeSels = [	'{}Shape'.format(self.ctrlSels[0]), 
								'{}Shape'.format(self.ctrlSels[-1])]

		self.ctrlCnrMthAttrs = ['CornerUpCor',
								'CornerDnCor', 
								'CornerInCor', 
								'CornerOutCor']

		self.ctrlJawAttrs = [	'JawOpenCor', 
								'JawCloseCor']

	
		self.ctrlCnrTx = []
		self.ctrlCnrTy = []
		self.ctrlJawRx = []

		self.mthGeo = 'BodyMthRig_Geo'
		# self.valueCnr = []


		self.nameSp = cn.nameSpaceLists()
		nameSp = cn.nameSpaceLists()
		if nameSp:
			self.nameSp = nameSp[0]

	def getValueMth(self, *args):

		## getValue Cnr
		for attr in self.ctrlCnrMthAttrs:
			ctrlSel = '{}:{}.{}'.format(self.nameSp, self.ctrlShapeSels[0], attr)
			if mc.objExists(ctrlSel):
				if 'Up' in attr or 'Dn' in attr:
					self.ctrlCnrTy.append(('{:.2f}'.format(mc.getAttr(ctrlSel))))
				else:
					self.ctrlCnrTx.append(('{:.2f}'.format(mc.getAttr(ctrlSel))))

				# self.valueCnr[attr, {value}]
		## getValue Jaw
		for attr in self.ctrlJawAttrs:
			ctrlSel = '{}:{}.{}'.format(self.nameSp, self.ctrlShapeSels[-1], attr)
			if mc.objExists(ctrlSel):
				self.ctrlJawRx.append(mc.getAttr(ctrlSel))

		return self.ctrlCnrTy, self.ctrlCnrTx, self.ctrlJawRx


	def setAttrCtrl(self,*args):
		## setAttr cnr

		lists = [self.ctrlCnrTy, self.ctrlCnrTx]
		for x in range(len(lists)):
			for i in range(lists[x],0):
				print lists[x][i], '..i'
				# if lists[x] == 0:
				# 	mc.setAttr('{}:{}.ty'.format(self.nameSp, self.ctrlSels[0]), float(lists[x][i]))
				# 	print  '00000'
				# else:
				# 	mc.setAttr('{}:{}.tx'.format(self.nameSp, self.ctrlSels[0]), float(lists[x][i]))

				# 	print '1111'
			## setAttr default
			# mc.setAttr('{}:{}.ty'.format(self.nameSp, self.ctrlSels[0]), 0)
