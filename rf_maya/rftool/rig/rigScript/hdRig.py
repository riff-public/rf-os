import maya.cmds as mc
import core
reload(core)
import rigTools as rt
reload(rt)
import ctrlRig as cr
reload(cr)

cn = core.Node()
# -------------------------------------------------------------------------------------------------------------
#
#  HEAD DEFORMER RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):
	def __init__(self ,	headLwrTmpJnt		= 'HeadLwrHdRig_TmpJnt' ,
						headMidTmpJnt		= 'HeadMidHdRig_TmpJnt' ,
						headUprTmpJnt		= 'HeadUprHdRig_TmpJnt' ,
						headLwrTmpCtrl		= 'HeadLwrHdRig_TmpCtrl' ,
						headUprTmpCtrl		= 'HeadUprHdRig_TmpCtrl' ,
						axis				= 'y+' ,
						elem				= 'hd' ,
						side				= '' ,
						**kwargs):

		#-- Info
		elem = elem.capitalize()
		if side == '':
			self.side = '_'
		else:
			self.side = '_{}_'.format(side)

		if axis == 'x+':
			self.axis 		= 'z'
			self.aimTop 		= (1 , 0 , 0)
			self.upVecTop 		= (0 , 0 , 1)
			self.aimBtm 		= (-1 , 0 , 0)
			self.upVecBtm 		= (0 , 0 , 1)

		elif axis == 'x-':
			self.axis 		= 'z'
			self.aimTop 		= (-1 , 0 , 0)
			self.upVecTop 		= (0 , 0 , 1)
			self.aimBtm 		= (1 , 0 , 0)
			self.upVecBtm 		= (0 , 0 , 1)

		elif axis == 'y+':
			self.axis 		= 'z'
			self.aimTop 		= (0 , 1 , 0)
			self.upVecTop 		= (0 , 0 , 1)
			self.aimBtm 		= (0 , -1 , 0)
			self.upVecBtm 		= (0 , 0 , 1)

		elif axis == 'y-':
			self.axis 		= 'z'
			self.aimTop 		= (0 , -1 , 0)
			self.upVecTop 		= (0 , 0 , 1)
			self.aimBtm 		= (0 , 1 , 0)
			self.upVecBtm 		= (0 , 0 , 1)

		elif axis == 'z+':
			self.axis 		= 'y'
			self.aimTop 		= (0 , 0 , 1)
			self.upVecTop 		= (0 , 1 , 0)
			self.aimBtm 		= (0 , 0 , -1)
			self.upVecBtm 		= (0 , 1 , 0)

		elif axis == 'z-':
			self.axis 		= 'y'
			self.aimTop 		= (0 , 0 , -1)
			self.upVecTop 		= (0 , 1 , 0)
			self.aimBtm 		= (0 , 0 , -1)
			self.upVecBtm 		= (0 , 1 , 0)


		#-- Create main group
		self.ctrlGrp = cn.createNode('transform' , '{}RigCtrl{}Grp'.format(elem , self.side))
		self.skinGrp = cn.createNode('transform' , '{}RigSkin{}Grp'.format(elem , self.side))
		self.stillGrp = cn.createNode('transform' , '{}RigStill{}Grp'.format(elem , self.side))

		#-- Create group position
		self.hdLwrPosGrp = cn.createNode('transform' , 'HeadLwrPos{}Rig{}Grp'.format(elem , self.side))
		self.mthPosGrp = cn.createNode('transform' , 'MouthPos{}Rig{}Grp'.format(elem , self.side))
		self.nsePosGrp = cn.createNode('transform' , 'NosePos{}Rig{}Grp'.format(elem , self.side))
		self.hdMidPosGrp = cn.createNode('transform' , 'HeadMidPos{}Rig{}Grp'.format(elem , self.side))
		self.hdUprPosGrp = cn.createNode('transform' , 'HeadUprPos{}Rig{}Grp'.format(elem , self.side))

		#-- Set up group position
		self.hdLwrPosGrp.snap(headLwrTmpJnt)
		self.mthPosGrp.snap([ headMidTmpJnt , headLwrTmpJnt ])
		self.nsePosGrp.snap(headMidTmpJnt)
		self.hdMidPosGrp.snap(headMidTmpJnt)
		self.hdUprPosGrp.snap(headUprTmpJnt)

		valueFnt = self.hdLwrPosGrp.attr('t{}'.format(self.axis)).getVal()
		valueBck = self.hdUprPosGrp.attr('t{}'.format(self.axis)).getVal()
		self.nsePosGrp.attr('t{}'.format(self.axis)).v = valueFnt
		self.mthPosGrp.attr('t{}'.format(self.axis)).v = valueFnt
		self.hdMidPosGrp.attr('t{}'.format(self.axis)).v = valueBck

		#-- Create Controls
		self.hdLwrCtrl = rt.createCtrl(name = 'HeadLwr{}Rig{}Ctrl'.format(elem , self.side) , shape = 'cube' , col = 'green' , traget = headLwrTmpCtrl , jnt = False)
		self.hdMidCtrl = rt.createCtrl(name = 'HeadMid{}Rig{}Ctrl'.format(elem , self.side) , shape = 'nrbCircle' , col = 'yellow' , traget = self.hdMidPosGrp , jnt = False)
		self.hdUprCtrl = rt.createCtrl(name = 'HeadUpr{}Rig{}Ctrl'.format(elem , self.side) , shape = 'cube' , col = 'green' , traget = headUprTmpCtrl , jnt = False)
		self.hdLwrCtrlZro = rt.addGrp(self.hdLwrCtrl)
		self.hdMidCtrlZro = rt.addGrp(self.hdMidCtrl)
		self.hdUprCtrlZro = rt.addGrp(self.hdUprCtrl)

		self.hdLwrCtrlZro.snap(headLwrTmpCtrl)
		self.hdMidCtrlZro.snap(self.hdMidPosGrp)
		self.hdUprCtrlZro.snap(headUprTmpCtrl)

		#-- Create Joints
		self.root = cn.joint('Root{}Rig{}Jnt'.format(elem , self.side))
		self.hdLwrJnt = cn.joint('HeadLwr{}Rig{}Jnt'.format(elem , self.side))
		self.hdMidJnt = cn.joint('HeadMid{}Rig{}Jnt'.format(elem , self.side))
		self.hdUprJnt = cn.joint('HeadUpr{}Rig{}Jnt'.format(elem , self.side))
		self.mthJnt = cn.joint('Mouth{}Rig{}Jnt'.format(elem , self.side))
		self.chkJnt = cn.joint('Cheek{}Rig{}Jnt'.format(elem , self.side))
		self.chkJnt.parent(self.mthJnt)

		self.hdLwrJnt.snap(self.hdLwrPosGrp)
		self.hdMidJnt.snap(self.hdMidPosGrp)
		self.hdUprJnt.snap(self.hdUprPosGrp)
		self.mthJnt.snap(self.mthPosGrp)
		self.root.snap(self.hdLwrPosGrp)
		self.root.attr('t{}'.format(self.axis)).v = valueBck

		#-- Head Deform Rigging
		#-- Lock attr ctrls
		for ctrl in (self.hdLwrCtrl , self.hdMidCtrl , self.hdUprCtrl):
			for attr in ('rx','ry','rz','sx','sy','sz','v'):
				ctrl.attr(attr).setLockHide()

		#-- Create Rig Groups
		self.mthZro = rt.addGrp(self.mthPosGrp)
		self.hdLwrZro = rt.addGrp(self.hdLwrPosGrp)
		self.hdUprZro = rt.addGrp(self.hdUprPosGrp)

		self.hdMidOfst = rt.addGrp(self.hdMidPosGrp , 'Ofst')
		self.nseOfst = rt.addGrp(self.nsePosGrp , 'Ofst')

		self.hdMidCnnt = rt.addGrp(self.hdMidPosGrp , 'Cnnt')
		self.nseCnnt = rt.addGrp(self.nsePosGrp , 'Cnnt')
		self.hdLwrCnnt = rt.addGrp(self.hdLwrPosGrp , 'Cnnt')
		self.hdUprCnnt = rt.addGrp(self.hdUprPosGrp , 'Cnnt')

		self.hdMidGrp = cn.createNode('transform' , 'HeadMid{}Rig{}Grp'.format(elem , self.side))
		self.hdMidGrp.snap(self.hdMidPosGrp)

		#-- Rigging Groups
		mc.aimConstraint(self.hdMidPosGrp , self.hdUprPosGrp , aim = self.aimBtm , wu = self.upVecBtm , wuo = self.hdUprCnnt)
		mc.aimConstraint(self.nsePosGrp , self.hdLwrPosGrp , aim = self.aimTop , wu = self.upVecTop , wuo = self.hdLwrCnnt)
		mc.aimConstraint(self.hdLwrPosGrp , self.mthPosGrp , aim = self.aimBtm , wu = self.upVecBtm , wuo = self.mthZro)
		mc.pointConstraint(self.nsePosGrp , self.hdLwrPosGrp , self.mthZro , mo=True)

		#-- Connect Groups
		self.hdLwrTslDvd = cn.createNode('multiplyDivide' , 'HeadLwrTsl{}Rig{}Dvd'.format(elem , self.side))
		self.hdLwrTslDvd.attr('i2x').v = 0.5
		self.hdLwrTslDvd.attr('i2y').v = 0.5
		self.hdLwrTslDvd.attr('i2z').v = 0.5

		self.hdLwrCtrl.attr('tx') >> self.hdLwrTslDvd.attr('i1x')
		self.hdLwrCtrl.attr('ty') >> self.hdLwrTslDvd.attr('i1y')
		self.hdLwrCtrl.attr('tz') >> self.hdLwrTslDvd.attr('i1z')
		self.hdLwrTslDvd.attr('ox') >> self.hdLwrCnnt.attr('tx')
		self.hdLwrTslDvd.attr('oy') >> self.hdLwrCnnt.attr('ty')
		self.hdLwrTslDvd.attr('oz') >> self.hdLwrCnnt.attr('tz')

		self.hdMidCtrl.attr('tx') >> self.hdMidCnnt.attr('tx')
		self.hdMidCtrl.attr('ty') >> self.hdMidCnnt.attr('ty')
		self.hdMidCtrl.attr('tz') >> self.hdMidCnnt.attr('tz')

		self.hdMidCtrl.attr('tx') >> self.nseCnnt.attr('tx')
		self.hdMidCtrl.attr('ty') >> self.nseCnnt.attr('ty')
		self.hdMidCtrl.attr('tz') >> self.nseCnnt.attr('tz')

		self.hdUprCtrl.attr('tx') >> self.hdUprCnnt.attr('tx')
		self.hdUprCtrl.attr('ty') >> self.hdUprCnnt.attr('ty')
		self.hdUprCtrl.attr('tz') >> self.hdUprCnnt.attr('tz')

		#-- Constraint Joints
		mc.parentConstraint(self.hdLwrPosGrp , self.hdLwrJnt , mo=True)
		mc.parentConstraint(self.mthPosGrp , self.mthJnt , mo=True)
		mc.parentConstraint(self.hdMidPosGrp , self.hdMidJnt , mo=True)
		mc.parentConstraint(self.hdUprPosGrp , self.hdUprJnt , mo=True)

		#-- Rig Squash
		"""if clamp node is needed you should put between norm and pow and set min default 0 with max default 1
		min value will not negative , min is squash | max value will over 1 , max is stretch"""
		#-- Setting for squash
		self.hdLwrCtrlShape = core.Dag(self.hdLwrCtrl.shape)
		self.hdUprCtrlShape = core.Dag(self.hdUprCtrl.shape)

		self.hdLwrCtrl.addAttr('autoSquash' , 0 , 1)
		self.hdLwrCtrl.addAttr('cheekSquash')
		self.hdLwrCtrl.addAttr('mouthSquash')
		self.hdLwrCtrl.attr('autoSquash').v = 1

		self.hdUprCtrl.addAttr('autoSquash' , 0 , 1)
		self.hdUprCtrl.addAttr('headMidSquash')
		self.hdUprCtrl.addAttr('headUprSquash')
		self.hdUprCtrl.attr('autoSquash').v = 1

		self.hdLwrCtrlShape.addAttr('cheekSqshAmp')
		self.hdLwrCtrlShape.addAttr('mouthSqshAmp')
		self.hdLwrCtrlShape.attr('cheekSqshAmp').v = 2
		self.hdLwrCtrlShape.attr('mouthSqshAmp').v = 0.6

		self.hdUprCtrlShape.addAttr('headMidSqshAmp')
		self.hdUprCtrlShape.addAttr('headUprSqshAmp')
		self.hdUprCtrlShape.attr('headMidSqshAmp').v = 0.5
		self.hdUprCtrlShape.attr('headUprSqshAmp').v = 2.5


		self.distLwrRoot = cn.createNode('distanceBetween' , 'HeadLwrRoot{}{}Dist'.format(elem , self.side))
		self.distLwrEnd = cn.createNode('distanceBetween' , 'HeadLwrEnd{}{}Dist'.format(elem , self.side))

		self.distUprRoot = cn.createNode('distanceBetween' , 'HeadUprRoot{}{}Dist'.format(elem , self.side))
		self.distUprEnd = cn.createNode('distanceBetween' , 'HeadUprEnd{}{}Dist'.format(elem , self.side))

		self.hdLwrPosGrp.attr('worldMatrix[0]') >> self.distLwrRoot.attr('inMatrix1')
		self.mthPosGrp.attr('worldMatrix[0]') >> self.distLwrRoot.attr('inMatrix2')
		self.mthPosGrp.attr('worldMatrix[0]') >> self.distLwrEnd.attr('inMatrix1')
		self.nsePosGrp.attr('worldMatrix[0]') >> self.distLwrEnd.attr('inMatrix2')

		self.hdLwrZro.attr('worldMatrix[0]') >> self.distUprRoot.attr('inMatrix1')
		self.hdMidPosGrp.attr('worldMatrix[0]') >> self.distUprRoot.attr('inMatrix2')
		self.hdMidPosGrp.attr('worldMatrix[0]') >> self.distUprEnd.attr('inMatrix1')
		self.hdUprPosGrp.attr('worldMatrix[0]') >> self.distUprEnd.attr('inMatrix2')

		self.distLwrPma = cn.createNode('plusMinusAverage' , 'HeadLwrDist{}Rig{}Pma'.format(elem , self.side))
		self.distUprPma = cn.createNode('plusMinusAverage' , 'HeadUprDist{}Rig{}Pma'.format(elem , self.side))
		self.hdLwrNorm = cn.createNode('multiplyDivide' , 'HeadLwrNorm{}Rig{}Mdv'.format(elem , self.side))
		self.hdUprNorm = cn.createNode('multiplyDivide' , 'HeadUprNorm{}Rig{}Mdv'.format(elem , self.side))
		self.mthPow = cn.createNode('multiplyDivide' , 'MouthPow{}Rig{}Mdv'.format(elem , self.side))
		self.chkPow = cn.createNode('multiplyDivide' , 'CheekPow{}Rig{}Mdv'.format(elem , self.side))
		self.hdMidPow = cn.createNode('multiplyDivide' , 'HeadMidPow{}Rig{}Mdv'.format(elem , self.side))
		self.hdUprPow = cn.createNode('multiplyDivide' , 'HeadUprPow{}Rig{}Mdv'.format(elem , self.side))
		self.mthDvd = cn.createNode('multiplyDivide' , 'MouthDvd{}Rig{}Mdv'.format(elem , self.side))
		self.chkDvd = cn.createNode('multiplyDivide' , 'CheekDvd{}Rig{}Mdv'.format(elem , self.side))
		self.hdMidDvd = cn.createNode('multiplyDivide' , 'HeadMidDvd{}Rig{}Mdv'.format(elem , self.side))
		self.hdUprDvd = cn.createNode('multiplyDivide' , 'HeadUprDvd{}Rig{}Mdv'.format(elem , self.side))
		self.mthBlc = cn.createNode('blendColors' , 'MouthAutoSqsh{}Rig{}Blc'.format(elem , self.side))
		self.chkBlc = cn.createNode('blendColors' , 'CheekAutoSqsh{}Rig{}Blc'.format(elem , self.side))
		self.hdMidBlc = cn.createNode('blendColors' , 'HeadMidAutoSqsh{}Rig{}Blc'.format(elem , self.side))
		self.hdUprBlc = cn.createNode('blendColors' , 'HeadUprAutoSqsh{}Rig{}Blc'.format(elem , self.side))
		self.mthTotal = cn.createNode('plusMinusAverage' , 'MouthTotalSqsh{}Rig{}Pma'.format(elem , self.side))
		self.chkTotal = cn.createNode('plusMinusAverage' , 'CheekTotalSqsh{}Rig{}Pma'.format(elem , self.side))
		self.hdMidTotal = cn.createNode('plusMinusAverage' , 'HeadMidTotalSqsh{}Rig{}Pma'.format(elem , self.side))
		self.hdUprTotal = cn.createNode('plusMinusAverage' , 'HeadUprTotalSqsh{}Rig{}Pma'.format(elem , self.side))

		self.distLwrPma.addAttr('default')
		self.distUprPma.addAttr('default')
		self.hdLwrNorm.attr('op').v = 2
		self.hdUprNorm.attr('op').v = 2
		self.mthPow.attr('op').v = 3
		self.chkPow.attr('op').v = 3
		self.hdMidPow.attr('op').v = 3
		self.hdUprPow.attr('op').v = 3
		self.mthDvd.attr('op').v = 2
		self.chkDvd.attr('op').v = 2
		self.hdMidDvd.attr('op').v = 2
		self.hdUprDvd.attr('op').v = 2
		self.mthDvd.attr('i1x').v = 1
		self.chkDvd.attr('i1x').v = 1
		self.hdMidDvd.attr('i1x').v = 1
		self.hdUprDvd.attr('i1x').v = 1
		self.mthBlc.attr('c2r').v = 1
		self.chkBlc.attr('c2r').v = 1
		self.hdMidBlc.attr('c2r').v = 1
		self.hdUprBlc.attr('c2r').v = 1

		self.distLwrRoot.attr('d') >> self.distLwrPma.attr('i1[0]')
		self.distLwrEnd.attr('d') >> self.distLwrPma.attr('i1[1]')
		self.distUprRoot.attr('d') >> self.distUprPma.attr('i1[0]')
		self.distUprEnd.attr('d') >> self.distUprPma.attr('i1[1]')

		self.distLwrPma.attr('default').v = self.distLwrPma.attr('o1').v
		self.distUprPma.attr('default').v = self.distUprPma.attr('o1').v

		self.distLwrPma.attr('o1') >> self.hdLwrNorm.attr('i1x')
		self.distLwrPma.attr('default') >> self.hdLwrNorm.attr('i2x')
		self.distUprPma.attr('o1') >> self.hdUprNorm.attr('i1x')
		self.distUprPma.attr('default') >> self.hdUprNorm.attr('i2x')
		self.hdLwrNorm.attr('ox') >> self.mthPow.attr('i1x')
		self.hdLwrCtrlShape.attr('mouthSqshAmp') >> self.mthPow.attr('i2x')
		self.hdLwrNorm.attr('ox') >> self.chkPow.attr('i1x')
		self.hdLwrCtrlShape.attr('cheekSqshAmp') >> self.chkPow.attr('i2x')
		self.hdUprNorm.attr('ox') >> self.hdMidPow.attr('i1x')
		self.hdUprCtrlShape.attr('headMidSqshAmp') >> self.hdMidPow.attr('i2x')
		self.hdUprNorm.attr('ox') >> self.hdUprPow.attr('i1x')
		self.hdUprCtrlShape.attr('headUprSqshAmp') >> self.hdUprPow.attr('i2x')

		self.mthPow.attr('ox') >> self.mthDvd.attr('i2x')
		self.chkPow.attr('ox') >> self.chkDvd.attr('i2x')
		self.hdMidPow.attr('ox') >> self.hdMidDvd.attr('i2x')
		self.hdUprPow.attr('ox') >> self.hdUprDvd.attr('i2x')
		self.hdLwrCtrl.attr('autoSquash') >> self.mthBlc.attr('b')
		self.mthDvd.attr('ox') >> self.mthBlc.attr('c1r')
		self.hdLwrCtrl.attr('autoSquash') >> self.chkBlc.attr('b')
		self.chkDvd.attr('ox') >> self.chkBlc.attr('c1r')
		self.hdUprCtrl.attr('autoSquash') >> self.hdMidBlc.attr('b')
		self.hdMidDvd.attr('ox') >> self.hdMidBlc.attr('c1r')
		self.hdUprCtrl.attr('autoSquash') >> self.hdUprBlc.attr('b')
		self.hdUprDvd.attr('ox') >> self.hdUprBlc.attr('c1r')
		self.mthBlc.attr('opr') >> self.mthTotal.attr('i1[0]')
		self.hdLwrCtrl.attr('mouthSquash') >> self.mthTotal.attr('i1[1]')
		self.chkBlc.attr('opr') >> self.chkTotal.attr('i1[0]')
		self.hdLwrCtrl.attr('cheekSquash') >> self.chkTotal.attr('i1[1]')
		self.hdMidBlc.attr('opr') >> self.hdMidTotal.attr('i1[0]')
		self.hdUprCtrl.attr('headMidSquash') >> self.hdMidTotal.attr('i1[1]')
		self.hdUprBlc.attr('opr') >> self.hdUprTotal.attr('i1[0]')
		self.hdUprCtrl.attr('headUprSquash') >> self.hdUprTotal.attr('i1[1]')
		self.mthTotal.attr('o1') >> self.mthJnt.attr('sx')
		self.mthTotal.attr('o1') >> self.mthJnt.attr('sz')
		self.chkTotal.attr('o1') >> self.chkJnt.attr('sx')
		self.chkTotal.attr('o1') >> self.chkJnt.attr('sz')
		self.hdMidTotal.attr('o1') >> self.hdMidJnt.attr('sx')
		self.hdMidTotal.attr('o1') >> self.hdMidJnt.attr('sz')
		self.hdUprTotal.attr('o1') >> self.hdUprJnt.attr('sx')
		self.hdUprTotal.attr('o1') >> self.hdUprJnt.attr('sz')

		#-- Lock Groups
		for grp in (self.hdMidGrp , self.hdLwrZro , self.hdUprZro):
			for attr in ('tx','ty','tz','rx','ry','rz','sx','sy','sz','v'):
				grp.attr(attr).setLockHide()
			
		#-- Set Joints Radius
		for jnt in (self.hdLwrJnt , self.mthJnt , self.chkJnt , self.hdMidJnt , self.hdUprJnt , self.root):
			jnt.attr('radius').v = 0.1

		#-- Adjust Hierachy
		mc.parent(self.hdLwrCtrlZro , self.hdMidCtrlZro , self.hdUprCtrlZro , self.ctrlGrp)
		mc.parent(self.hdMidOfst , self.nseOfst , self.hdMidGrp)
		mc.parent(self.mthZro , self.hdMidGrp , self.hdLwrZro , self.hdUprZro , self.stillGrp)
		mc.parent(self.hdLwrJnt , self.mthJnt , self.hdMidJnt , self.hdUprJnt , self.root , self.skinGrp)