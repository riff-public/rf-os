# please do not delete
# follicle connect script
# written by Bjorn Blaabjerg Sorensen 2015
# this script may not to be sold or redistributed without prior agreement
# blaabjergb@gmail.com
# blaabjergb.com


import pymel.core as pm
import maya.cmds as mc
def createFollicle(*args):
    ''' connect via follicle '''
    ''' first select joint/locator to attatch to surface, then select the surface to attatch to '''
    list=pm.ls(sl=True)
    for element in list:
        if element==list[-1]:
            print 'pass'
        else:
            closest=mc.createNode('closestPointOnMesh')
            pm.connectAttr(list[-1]+'.outMesh',closest+'.inMesh')
            x=pm.xform(element,t=True,q=True)
            pm.setAttr(closest+'.inPositionX',x[0])
            pm.setAttr(closest+'.inPositionY',x[1])
            pm.setAttr(closest+'.inPositionZ',x[2])
            
            folicle=pm.createNode("follicle")
            folicleTrans=pm.listRelatives(folicle,type='transform',p=True)
            pm.connectAttr(folicle + ".outRotate", folicleTrans[0] + ".rotate")
            pm.connectAttr(folicle + ".outTranslate", folicleTrans[0] + ".translate")
            pm.connectAttr(list[-1]+'.worldMatrix',folicle+'.inputWorldMatrix')
            pm.connectAttr(list[-1]+'.outMesh',folicle+'.inputMesh')
            pm.setAttr(folicle + ".simulationMethod", 0)
            u=pm.getAttr(closest+'.result.parameterU')
            v=pm.getAttr(closest+'.result.parameterV')
            pm.setAttr(folicle+'.parameterU',u)
            pm.setAttr(folicle+'.parameterV',v)
            pm.parentConstraint(folicleTrans[0],element,mo=True)
            
            pm.delete(closest)



''' for nurbs surface '''

'''
list=pm.ls(sl=True)
for element in list:
    if element==list[-1]:
        print 'pass'
    else:
        closest=pm.createNode('closestPointOnSurface')
        pm.connectAttr(list[-1]+'Shape.worldSpace',closest+'.inputSurface')
        x=pm.xform(list[0],t=True,q=True)
        pm.setAttr(closest+'.inPositionX',x[0])
        pm.setAttr(closest+'.inPositionY',x[1])
        pm.setAttr(closest+'.inPositionZ',x[2])
        
        folicle=pm.createNode("follicle")
        folicleTrans=pm.listRelatives(folicle,type='transform',p=True)
        pm.connectAttr(folicle + ".outRotate", folicleTrans[0] + ".rotate")
        pm.connectAttr(folicle + ".outTranslate", folicleTrans[0] + ".translate")
        pm.connectAttr(list[-1]+'Shape.worldSpace',folicle+'.inputSurface')
        pm.setAttr(folicle + ".simulationMethod", 0)
        u=pm.getAttr(closest+'.result.parameterU')
        v=pm.getAttr(closest+'.result.parameterV')
        pm.setAttr(folicle+'.parameterU',u)
        pm.setAttr(folicle+'.parameterV',v)
        pm.parentConstraint(folicleTrans[0],element,mo=True)
        pm.delete(closest)
'''


''' end of folicle connect script '''    