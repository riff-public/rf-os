import maya.cmds as mc
import pymel.core as pm
import os,sys

import core
reload( core )
import rigTools as rt
reload( rt )
import ctrlRig
reload(ctrlRig)
import fkRig
reload(fkRig)
import fkChain
reload(fkChain)

cn = core.Node()

class Run ( object ):
    def  __init__( self, clavMusSpineRoot  = 'SpineMusRoot_TmpJnt',
                         elem                    = 'ClavMus',
                         side                    = 'L',
                         men                     = True,
                         ctrlGrp                 = '' ,
                         skinGrp                 = '' ,
                         jntGrp                  = '' ,
                         ikhGrp                  = '' ,
                         stillGrp                = '' ,
                         parent                  = '' ,
                         upArmParentGrp          = [] ,
                         spineJnt                = '' ,
                         upArmJnt                = '' ,
                         neckJnt                 = '' ):

        # Name
        elem = elem.capitalize()
        # self.side = side

        if side == '':
            self.side = '_'
        else:
            self.side = '_{}_'.format(side)

        #-- TmpJnt
        clavMusTmpJnt           = 'ClavMus1{}TmpJnt'.format(self.side)
        armPointTmpJnt          = 'ArmPoint1Skin{}TmpJnt'.format(self.side)
        upArmRootAdjSkinTmpJnt  = 'UpArmRootAdjSkin{}TmpJnt'.format(self.side)
        scapRoot1TmpJnt         = 'ScapRoot1{}TmpJnt'.format(self.side)
        scap1SkinTmpJnt         = 'Scap1Skin{}TmpJnt'.format(self.side)
        scap2SkinTmpJnt         = 'Scap2Skin{}TmpJnt'.format(self.side)
        trap1SkinTmpJnt         = 'Trap1Skin{}TmpJnt'.format(self.side)
        trap2TmpJnt             = 'Trap2{}TmpJnt'.format(self.side)
        trapBckTmpJnt           = 'Trap1Bck{}TmpJnt'.format(self.side)
        pec1SkinTmpJnt          = 'Pec1Skin{}TmpJnt'.format(self.side)
        pec2SkinTmpJnt          = 'Pec2Skin{}TmpJnt'.format(self.side)
        Breast1TmpJnt           = 'Breast1{}TmpJnt'.format(self.side)
        Breast2TmpJnt           = 'Breast2{}TmpJnt'.format(self.side)
        Breast3TmpJnt           = 'Breast3{}TmpJnt'.format(self.side)
        lateSkinTmpJnt           = 'Late1Skin{}TmpJnt'.format(self.side)

        ## ControlAll
        controlList = []

        ## Create All Joint
        # SpineMusRoot
        self.clavMusSpineRootJnt = 'SpineMusRoot_Jnt'
        if not mc.objExists('SpineMusRoot_Jnt'):
            self.clavMusSpineRootJnt = cn.joint( 'SpineMusRoot_Jnt', clavMusSpineRoot)

        # clavMusJnt  
        self.clavMus1Jnt = cn.joint( 'ClavMus1{}Jnt'.format(self.side), clavMusTmpJnt)
        self.clavMus2Jnt = cn.joint( 'ClavMus2{}Jnt'.format(self.side), armPointTmpJnt)
        mc.parent(self.clavMus2Jnt,self.clavMus1Jnt)
        mc.parent(self.clavMus1Jnt,self.clavMusSpineRootJnt)

        # armPoint1Skin  
        self.armPointJnt = cn.joint( 'ArmPoint1Skin{}Jnt'.format(self.side), armPointTmpJnt)
        mc.parent(self.armPointJnt,self.clavMus2Jnt)

        # armPoint1Skin  
        self.armRootAdjSkinJnt = cn.joint( 'UpArmRootAdjSkin{}Jnt'.format(self.side), upArmRootAdjSkinTmpJnt)
        mc.parent(self.armRootAdjSkinJnt,self.armPointJnt)

        # scap  
        self.scapRoot1Jnt = cn.joint( 'ScapRoot1{}Jnt'.format(self.side), scapRoot1TmpJnt)
        self.scap1SkinJnt = cn.joint( 'Scap1Skin{}Jnt'.format(self.side), scap1SkinTmpJnt)
        self.scap2SkinJnt = cn.joint( 'Scap2Skin{}Jnt'.format(self.side), scap2SkinTmpJnt)  
        mc.parent(self.scap2SkinJnt,self.scap1SkinJnt)
        mc.parent(self.scap1SkinJnt,self.scapRoot1Jnt)
        mc.parent(self.scapRoot1Jnt,self.armPointJnt)

        # trapSkin
        self.trap1SkinJnt = cn.joint( 'Trap1Skin{}Jnt'.format(self.side), trap1SkinTmpJnt)
        self.trap2Jnt = cn.joint( 'Trap2{}Jnt'.format(self.side), trap2TmpJnt)
        mc.parent( self.trap2Jnt, self.trap1SkinJnt)
        mc.parent( self.trap1SkinJnt, self.clavMus2Jnt)

        # trapSkinBck
        self.trap1BckJnt = cn.joint( 'Trap1Bck{}Jnt'.format(self.side), trapBckTmpJnt)
        self.trap2BckJnt = cn.joint( 'Trap2Bck{}Jnt'.format(self.side), trapBckTmpJnt)
        self.trap3SkinBckJnt = cn.joint( 'Trap3SkinBck{}Jnt'.format(self.side), trapBckTmpJnt)
        mc.parent( self.trap3SkinBckJnt, self.trap2BckJnt)
        mc.parent( self.trap2BckJnt, self.trap1BckJnt)
        mc.parent( self.trap1BckJnt, self.clavMus1Jnt)

        # pecSkin
        self.pec1SkinJnt = cn.joint( 'Pec1Skin{}Jnt'.format(self.side), pec1SkinTmpJnt)
        self.pec2SkinJnt = cn.joint( 'Pec2Skin{}Jnt'.format(self.side), pec2SkinTmpJnt)
        mc.parent( self.pec2SkinJnt, self.pec1SkinJnt)
        mc.parent( self.pec1SkinJnt, self.clavMus1Jnt)

        # # Breast
        # self.Breast1Jnt = cn.joint( 'Breast1{}Jnt'.format(side), Breast1TmpJnt)
        # self.Breast2Jnt = cn.joint( 'Breast2{}Jnt'.format(side), Breast2TmpJnt)
        # self.Breast3Jnt = cn.joint( 'Breast3{}Jnt'.format(side), Breast3TmpJnt)
        # mc.parent( self.Breast3Jnt, self.Breast2Jnt)
        # mc.parent( self.Breast2Jnt, self.Breast1Jnt)
        # mc.parent( self.Breast1Jnt, self.pec1SkinJnt)

        # lateSkin
        self.lateSkinJnt = cn.joint( 'Lat1Skin{}Jnt'.format(self.side), lateSkinTmpJnt)
        mc.parent( self.lateSkinJnt, self.clavMusSpineRootJnt)


        #################### Create Joint Done ####################

        ## Group
        self.clavMusSkin = core.Dag('{}Skin_Grp'.format(elem))
        if not mc.objExists('{}Skin_Grp'.format(elem)):
            self.clavMusSkin  = cn.createNode('transform' , '{}Skin_Grp'.format(elem))

        self.clavMusCtrl = core.Dag('{}Ctrl_Grp'.format(elem))
        if not mc.objExists('{}Ctrl_Grp'.format(elem)):
            self.clavMusCtrl  = cn.createNode('transform' , '{}Ctrl_Grp'.format(elem))

        self.clavMusIkh = core.Dag('{}Ikh_Grp'.format(elem))
        if not mc.objExists('{}Ikh_Grp'.format(elem)):
            self.clavMusIkh  = cn.createNode('transform' , '{}Ikh_Grp'.format(elem))

        self.clavMusStill = core.Dag('{}Still_Grp'.format(elem))
        if not mc.objExists('{}Still_Grp'.format(elem)):
            self.clavMusStill  = cn.createNode('transform' , '{}Still_Grp'.format(elem))

        #################### Create Group Done ####################

        ## Parent SpinePos to Spine
        ## Unlocak Ik and Fk Control Group
        for each in upArmParentGrp:
            if mc.objExists(each):
                eachDag = core.Dag(each)
                eachDag.unlockHideAttrs("tx", "ty", "tz", "rx", "ry", "rz", "sx", "sy", "sz", "v")

                mc.parentConstraint(self.armPointJnt, eachDag, mo =True)

        ## Parent Constraint
        mc.parentConstraint(spineJnt, self.clavMusSpineRootJnt, mo =True)

        # pec1SkinJnt  = 'Pec1Skin{}TmpJnt'.format(side)
        pec1Skin = fkRig.Run(   name        = 'Pec1Skin',
                                side        = side , 
                                parent      = '',
                                tmpJnt      = self.pec1SkinJnt, 
                                ctrlGrp     = self.clavMusCtrl , 
                                shape       = 'circle',
                                jnt_ctrl    = 0, 
                                color       = 'yellow', 
                                localWorld  = 0, 
                                constraint  = 0, 
                                jnt         = 0, 
                                dtl         = 0 )

        mc.parentConstraint(pec1Skin.gmbl, self.pec1SkinJnt, mo = True)
        self.pec1SkinJnt.attr('segmentScaleCompensate').value = 0

        ## Pioint Constraint 'y', 'z'
        mc.pointConstraint(self.clavMus2Jnt, spineJnt, pec1Skin.ofst, mo = True, skip = 'x')
        pec1SkinPntCon = core.Dag(mc.listConnections( '{}.ty'.format(pec1Skin.ofst))[0])
        pec1SkinPntCon.attr('{}W1'.format(spineJnt)).value = 0.5
        pec1SkinPntCon.attr('{}W0'.format(self.clavMus2Jnt)).value = 0.5


        # generate late control and parent joint
        late1Skin = fkRig.Run(  name        = 'Late1Skin',
                                side        = side , 
                                parent      = '',
                                tmpJnt      = lateSkinTmpJnt,
                                ctrlGrp     = self.clavMusCtrl , 
                                shape       = 'circle',
                                jnt_ctrl    = 0, 
                                color       = 'yellow', 
                                localWorld  = 0, 
                                constraint  = 0, 
                                jnt         = 0, 
                                dtl         = 0 )

        mc.parentConstraint(late1Skin.gmbl, self.lateSkinJnt, mo = True)
        mc.scaleConstraint(late1Skin.gmbl, self.lateSkinJnt, mo = True)
        mc.delete(lateSkinTmpJnt)

        ## set secmentScale fix Double Scale 
        mc.setAttr('{}.segmentScaleCompensate'.format(self.lateSkinJnt), 0)


        ## Pioint Constraint 'x' (old skip = ['y', 'z'])
        spinePar = 'Spine4_Jnt'
        mc.pointConstraint(self.clavMus2Jnt, spinePar, late1Skin.ofst, mo = True)
        lateSkincon = core.Dag(mc.listConnections( '{}.tx'.format(late1Skin.ofst))[0])
        lateSkincon.attr('{}W1'.format(spinePar)).value = 0.5
        lateSkincon.attr('{}W0'.format(self.clavMus2Jnt)).value = 0.5






        ## Generate ikhandle
        rt.addIkh( name = elem , type = 'ikSCsolver' , stJnt = self.clavMus1Jnt , enJnt = self.clavMus2Jnt)
        clavIkh     = '{}{}Ikh'.format(elem, self.side)
        clavIkhZro  = '{}IkhZro{}Grp'.format(elem, self.side)
        mc.parent(clavIkhZro,self.clavMusIkh)


        ## Generate control for upArm
        clavUpArmJnt = cn.createNode('joint' , 'clavUpArm{}Jnt'.format(self.side))

        if 'L' in side:
            clavUpArmJnt.snap(self.clavMus2Jnt)

        elif 'R' in side:
            clavUpArmJnt.snap(self.clavMus2Jnt)
            clavUpArmJnt.attr('rx').value = -360
            clavUpArmJnt.attr('ry').value = 180
            mc.makeIdentity ( clavUpArmJnt , apply = True )    


        ## Generate Control
        clavUpArm = fkRig.Run(  name        = 'ClavUpArm',
                                side        = side , 
                                parent      = '',
                                tmpJnt      = clavUpArmJnt, 
                                ctrlGrp     = self.clavMusCtrl , 
                                shape       = 'plus',
                                jnt_ctrl    = 0, 
                                color       = 'green', 
                                localWorld  = 0, 
                                constraint  = 0, 
                                jnt         = 0, 
                                dtl         = 0 )

        mc.delete(clavUpArmJnt)
        mc.parentConstraint(clavUpArm.gmbl,clavIkhZro, mo = True)
        mc.scaleConstraint(clavUpArm.gmbl,clavIkhZro, mo = True)

        ## ParentConstraint clav to upArm Control
        mc.parentConstraint(parent,clavUpArm.zro, mo = True)

        ## LocakAttr
        clavUpArm.ctrl.lockHideAttrs('rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')

        ## Generate locator
        spineLoc = cn.locator(name = 'shldrWuo{}loc'.format(self.side), target = '')
        spineLoc.snap(spineJnt)
        mc.parentConstraint(spineJnt, spineLoc, mo = True)
        mc.scaleConstraint(spineJnt, spineLoc, mo = True)

        ## Create Control
        trap1Skin = fkRig.Run(   name        = 'Trap1Skin',
                                side        = side , 
                                parent      = '',
                                tmpJnt      = self.trap1SkinJnt, 
                                ctrlGrp     = self.clavMusCtrl , 
                                shape       = 'circle',
                                jnt_ctrl    = 0, 
                                color       = 'yellow', 
                                localWorld  = 0, 
                                constraint  = 0, 
                                jnt         = 0, 
                                dtl         = 0 )

        mc.parentConstraint(trap1Skin.gmbl,self.trap1SkinJnt, mo = True)

        trap1SkinGrpCtrlName    = trap1Skin.zro
        trap1SkinGrpCtrlOfsName = trap1Skin.ofst
        trap1SkinCtrlName       = trap1Skin.ctrl

        mc.parentConstraint(self.armPointJnt, trap1Skin.zro, mo =True)

        # ## Generate TrapBck Control Vis
        # ctrlVis = clavUpArm.ctrl.addAttr( 'CtrlVis', 0 , 1 )
        # clavUpArm.ctrl.attr('CtrlVis') >> trap1Skin.zro.attr('visibility')

        ## Generate spineLoc aimConstraint
        aimCon = mc.aimConstraint(neckJnt, trap1Skin.ofst, aim=(1, 0, 0), u=(0, 1, 0), wut="objectrotation", wu=(0, 1, 0), wuo = spineLoc, mo=True)[0]
        aimCon = mc.aimConstraint(neckJnt, self.scapRoot1Jnt, aim=(1, 0, 0), u=(0, 1, 0), wut="objectrotation", wu=(0, 1, 0), wuo = spineLoc, mo=True)[0]

        ## Generate Point Of head for point not orient
        if not mc.objExists('HeadTrapPos_{}_Jnt'.format(side)):
            headPosJnt = mc.createNode('joint', n = 'HeadTrapPos_{}_Jnt'.format(side))

        else:
            headPosJnt =  'HeadTrapPos_{}_Jnt'.format(side)
        mc.delete(mc.parentConstraint('Head_Jnt', headPosJnt, mo = False))
        mc.parent(headPosJnt, 'Spine5_Jnt')


        ## Generate Head and ArmPointSkin to trap1Skin.ofst
        pointCon = mc.pointConstraint(self.armPointJnt, headPosJnt, trap1Skin.ofst, mo= True, skip = 'z')[0]
        mc.setAttr('{}.{}W0'.format(pointCon, self.armPointJnt), 0.5)
        mc.setAttr('{}.{}W1'.format(pointCon, headPosJnt), 0.5)

        ## Create Control
        ## Generate spineLoc aimConstraint
        aimCon = mc.aimConstraint(spineJnt, self.scap1SkinJnt, aim=(1, 0, 0), u=(0, 1, 0), wut="objectrotation", wu=(0, 1, 0), wuo = spineLoc, mo=True)[0]

        ## Generate upArmRootAdjSkin Rig
        ## Generate trapSkin Bck Rig
        trapSkinBckJnt = [self.trap1BckJnt, self.trap2BckJnt, self.trap3SkinBckJnt]
        valueXAmp = [0, -1.6, -0.8]
        attrAmp = ['translateX', 'translateY', 'translateZ']

        ## name
        name = self.trap1BckJnt.getName()

        ## trap1SkinBck #1
        mc.parentConstraint(spineJnt, self.trap1BckJnt, mo = True)
        mc.pointConstraint(self.scap1SkinJnt, spineJnt, self.trap2BckJnt, mo = True)
        trap2BckCon = core.Dag(mc.listConnections( '{}.tx'.format(self.trap2BckJnt))[0])
        trap2BckCon.attr('{}W1'.format(spineJnt)).value = 0.5
        trap2BckCon.attr('{}W0'.format(self.scap1SkinJnt)).value = 0.5

        ## trap2Bck #2
        ## Add Attribute shape
        clavUpArmCtrlShape = core.Dag(clavUpArm.ctrl.getShape())
        mc.addAttr( clavUpArmCtrlShape , ln = '{}_Fun'.format(name) , at = 'enum', en = '0:1', k = True )
        clavUpArmCtrlShape.lockAttrs('{}_Fun'.format(name))

        for attrEach in attrAmp:
            clavUpArmCtrlShape.addAttr('{}_Amp'.format(attrEach),-200,200)

        ## trap2Bck Mdv1
        trap2BckMdv = cn.createNode('multiplyDivide', '{}{}Mdv'.format(name, self.side))
        self.clavMus1Jnt.attr('rx') >> trap2BckMdv.attr('input1X')
        self.clavMus1Jnt.attr('ry') >> trap2BckMdv.attr('input1Y')
        self.clavMus1Jnt.attr('rz') >> trap2BckMdv.attr('input1Z')

        ## trap2Bck Mdv2
        trap2BckAvrMdv = cn.createNode('multiplyDivide', '{}Avr{}Mdv'.format(name, self.side))
        trap2BckMdv.attr('outputX') >> trap2BckAvrMdv.attr('input1X')
        trap2BckMdv.attr('outputX') >> trap2BckAvrMdv.attr('input1Y')
        trap2BckMdv.attr('outputZ') >> trap2BckAvrMdv.attr('input1Z')    

        for (each, value) in zip(attrAmp, valueXAmp):
            if 'X' in each:
                clavUpArmCtrlShape.attr('{}_Amp'.format(each)).value = value
                clavUpArmCtrlShape.attr('{}_Amp'.format(each)) >> trap2BckMdv.attr('input2X')
                trap2BckAvrMdv.attr('input2X').value = 0.01
            elif 'Y' in each:
                clavUpArmCtrlShape.attr('{}_Amp'.format(each)).value = value
                clavUpArmCtrlShape.attr('{}_Amp'.format(each)) >> trap2BckMdv.attr('input2Y')
                trap2BckAvrMdv.attr('input2Y').value = 0.01
            elif 'Z' in each:
                clavUpArmCtrlShape.attr('{}_Amp'.format(each)).value = value
                clavUpArmCtrlShape.attr('{}_Amp'.format(each)) >> trap2BckMdv.attr('input2Z')
                trap2BckAvrMdv.attr('input2Z').value = 0.01


        ## Generate ClampNode
        trap2BckCmp = cn.createNode('clamp', '{}{}Cmp'.format(name, self.side))
        trap2BckCmp.attr('maxR').value = 0
        trap2BckCmp.attr('maxG').value = 50
        trap2BckCmp.attr('maxB').value = 50
        trap2BckAvrMdv.attr('outputX') >> trap2BckCmp.attr('inputR')
        trap2BckAvrMdv.attr('outputY') >> trap2BckCmp.attr('inputG')
        trap2BckAvrMdv.attr('outputZ') >> trap2BckCmp.attr('inputB')

        ## Generate plusMinusAverage
        trap2BckPma = cn.createNode('plusMinusAverage', '{}{}Pma'.format(name, self.side))
        trap2BckCmp.attr('outputG') >> trap2BckPma.attr('input2D[0].input2Dx')
        trap2BckCmp.attr('outputB') >> trap2BckPma.attr('input2D[1].input2Dx')

        ## Connect Side L or R
        if side == '_R_':
            trapSideRMdv = cn.createNode('multiplyDivide','{}Mirror{}Mdv'.format(name, self.side))
            trapSideRMdv.attr('input2X').value = -1
            trap2BckPma.attr('output2Dx') >> trapSideRMdv.attr('input1X')
            trapSideRMdv.attr('outputX') >> self.trap3SkinBckJnt.attr('ty')

        else:
            trap2BckPma.attr('output2Dx') >> self.trap3SkinBckJnt.attr('ty')

        ############################################################################################################################

        ## Generate Setrange node for clav front to back
        self.generateSetRange(  ctrl = clavUpArm.ctrl,
                                side = self.side,
                                elem = 'Scap',
                                driver = self.clavMus1Jnt, 
                                driverAxis = 'rotateY', 
                                driven = self.scap1SkinJnt ,
                                drivenAxis = 'translateX')


        ## Wrap Group
        mc.parent(spineLoc, self.clavMusStill)
        if not self.clavMusSpineRootJnt in self.clavMusSkin.getChild():
            mc.parent(self.clavMusSpineRootJnt, self.clavMusSkin)

        if not self.clavMusSkin in core.Dag(skinGrp).getChild():
            mc.parent(self.clavMusSkin, skinGrp)

        if not self.clavMusCtrl  in core.Dag(ctrlGrp).getChild():
            mc.parent(self.clavMusCtrl , ctrlGrp)
        mc.parentConstraint(spineJnt,self.clavMusCtrl, mo =True)
        mc.scaleConstraint(spineJnt,self.clavMusCtrl, mo =True)   

        if not self.clavMusStill  in core.Dag(stillGrp).getChild():
            mc.parent(self.clavMusStill , stillGrp)

        if not self.clavMusIkh  in core.Dag(ikhGrp).getChild():
            mc.parent(self.clavMusIkh , ikhGrp)

        # if mc.objExists('SpineMusRootJnt_Grp'):
        #     mc.delete('SpineMusRootJnt_Grp')

        ## setColor 
        # greenCtrl = [clavUpArmCtrlName]
        # yellowCtrl = [pec1Skin.ctrl, trap1SkinCtrlName]
        # utaCore.assignColor(obj = greenCtrl, col = 'green')
        # utaCore.assignColor(obj = yellowCtrl, col = 'yellow')

        ## Generate TrapBck Control Vis
        ctrlVis = 'Trap1Skin_CtrlVis'
        clavUpArm.ctrl.addAttr( 'Trap1Skin_CtrlVis', 0 , 1)
        clavUpArm.ctrl.attr('Trap1Skin_CtrlVis') >> trap1Skin.zro.attr('v')
        mc.setAttr('{}.{}'.format(clavUpArm.ctrl, 'Trap1Skin_CtrlVis'), 1)

        ## Set  segmentScaleCompensate Joint
        trap1Skin.jnt.attr('segmentScaleCompensate').value = 0
        pec1Skin.jnt.attr('segmentScaleCompensate').value = 0

        #############################################################################################################

        if not men:
            print "# Generate >> Breast {}".format(side)
            BreastRig = fkChain.Run(    name        = 'Breast', 
                                        side        = side ,
                                        tmpJnt      = [ Breast1TmpJnt, 
                                                        Breast2TmpJnt, 
                                                        Breast3TmpJnt ] ,
                                        parent      = self.pec1SkinJnt ,
                                        ctrlGrp     = ctrlGrp , 
                                        axis        = 'y' ,
                                        shape       = 'circle',
                                        jnt_ctrl    = 0 , 
                                        size        = 1 ,
                                        no_tip      = 1,
                                        color       = 'pink',
                                        localWorld  = 1,
                                        defaultWorld = 1,
                                        dtl         = 0,
                                        dtlShape    = 'cube',
                                        dtlColor    = 'blue'    )

            # Generate >> Fix some node
            pec1SkinPntCon.attr('{}W1'.format(spineJnt)).value = 0
            pec1SkinPntCon.attr('{}W0'.format(self.clavMus2Jnt)).value = 0

            self.generateSetRange(   ctrl = pec1Skin.ctrl,
                                      side = self.side,
                                      elem = 'MilkInOut',
                                      driver = self.clavMus1Jnt, 
                                      driverAxis = 'rotateY', 
                                      driven = pec1Skin.zro,
                                      drivenAxis = 'translateX')

            self.generateSetRange(   ctrl = BreastRig.ctrlArray[0],
                                      side = self.side,
                                      elem = 'MilkUpDn',
                                      driver = self.clavMus1Jnt, 
                                      driverAxis = 'rotateZ', 
                                      driven = BreastRig.ofstArray[0],
                                      drivenAxis = 'translateZ')

            mc.aimConstraint(spineJnt, pec1Skin.zro, aim=(1, 0, 0), u=(0, 1, 0), wut="objectrotation",  wu=(0, 1, 0), wuo = spineLoc, mo=True,)
            mc.aimConstraint(spineJnt, BreastRig.ofstArray[0], aim=(1, 0, 0), u=(0, 1, 0), wut="objectrotation",  wu=(0, 1, 0), wuo = spineLoc, mo=True, skip = ['y', 'z'])

            # fix aim Constraint Node
            aimRevNode = core.Dag(mc.listConnections( '{}.rx'.format(BreastRig.ofstArray[0]))[0])
            aimRevNode.attr('constraintRotate.constraintRotateX') // BreastRig.ofstArray[0].attr('rx')
            aimRevMdv = cn.createNode('multiplyDivide', '{}AimRev{}Mdv'.format(elem, self.side))
            aimRevMdv.attr('input2X').value = -2
            aimRevNode.attr('constraintRotate.constraintRotateX') >> aimRevMdv.attr('input1X')
            aimRevMdv.attr('outputX') >> BreastRig.ofstArray[0].attr('rx')


            ## setAttribute 'Breast', 'Pec1Skin'
            clavUpArmCtrlShape = core.Dag(clavUpArm.ctrl.getShape())
            pec1SkinCtrlShape = core.Dag(pec1Skin.ctrl.getShape())
            Breast1CtrlShape = core.Dag(BreastRig.ctrlArray[0].getShape())

            breastValue = [0.15, 1.5, 5, -100, 400]
            pec1SkinValue = [0.25, -0.15, 20, -100, 400]
            clavUpArm = [-0.8, -0.9, 9.1, -100, 200,]

            attrLists = ['minZ', 'maxX', 'oldMinX', 'oldMinZ', 'oldMaxX']
            for i in range(len(attrLists)):
                Breast1CtrlShape.attr(attrLists[i]).value = breastValue[i]
                pec1SkinCtrlShape.attr(attrLists[i]).value = pec1SkinValue[i]
                clavUpArmCtrlShape.attr(attrLists[i]).value = clavUpArm[i]

        # clavUpArmCtrlShape.lockHideAttrs('{}Sca_Fun'.format(elem), 'maxZ', 'minX')

    # 3 use but long ok to keep like this
    def generateSetRange( self ,
                          ctrl = '',
                          side = '',
                          elem = '',
                          driver = 'ClavMus1_L_Jnt', 
                          driverAxis = 'rotateY', 
                          driven = 'Scap1Skin_L_jnt',
                          drivenAxis = 'translateX',*args):
        ## Split Name
        name = ctrl.getName()
        ctrlShape = core.Dag(ctrl.getShape())

        driven.unlockHideAttrs("tx", "ty", "tz", "rx", "ry", "rz", "sx", "sy", "sz", "v")

        ## Add Attribute shape
        attrAmp = ['minX', 'minY', 'minZ', 'maxX', 'maxY', 'maxZ', 'oldMinX', 'oldMinY', 'oldMinZ', 'oldMaxX', 'oldMaxY', 'oldMaxZ']
        mc.addAttr(ctrl, ln = '{}{}OnOff'.format(name, elem), min = 0, max = 1, at = 'long',dv = 1, k =True)
        mc.addAttr(ctrlShape, ln = '{}{}_Fun'.format(name, elem.capitalize()), at = 'enum', en = '0:1', k =True )
        ctrlShape.lockAttrs('{}{}_Fun'.format(name, elem.capitalize()))
        for ata in attrAmp:
            ctrlShape.addAttr('{}'.format(ata),-1000, 1000)

        ## Generate Setrange node for clav front to back
        clavMusSrg = cn.createNode('setRange', '{}Mus{}Srg'.format(name, side))
        clavMusCon = cn.createNode('condition', '{}Mus{}con'.format(name, side))

        ## SetAttribute Default
        clavMusCon.attr('operation').value = 2
        ctrlShape.attr('oldMinX').value = 5
        ctrlShape.attr('oldMinZ').value = -100
        ctrlShape.attr('oldMaxX').value = 200

        drivenValue = mc.getAttr('{}.{}'.format(driven, drivenAxis))
        ctrlShape.attr('minX').value = drivenValue
        ctrlShape.attr('minZ').value = -3
        ctrlShape.attr('maxX').value = -2.8
        ctrlShape.attr('maxZ').value = drivenValue

        ## Connect Attribute
        for each in attrAmp:
            if 'L' in side:
                if 'maxX' in each:
                    mirrorValuePosiMdv = cn.createNode('multiplyDivide', '{}{}MusPosi{}Mdv'.format(name, each, side))
                    mirrorValuePosiMdv.attr('input2X').value = -1
                    ctrlShape.attr(each) >> mirrorValuePosiMdv.attr('input1X')
                    mirrorValuePosiMdv.attr('outputX') >> clavMusSrg.attr(each)

                else:
                    ctrlShape.attr(each) >> clavMusSrg.attr(each)

            elif 'R' in side :
                if 'minZ' in each:
                    mirrorValueMdv = cn.createNode('multiplyDivide', '{}{}Mus{}Mdv'.format(name, each, side))
                    mirrorValueMdv.attr('input2X').value = -1
                    ctrlShape.attr(each) >> mirrorValueMdv.attr('input1X')
                    mirrorValueMdv.attr('outputX') >> clavMusSrg.attr(each)

                else:
                    ctrlShape.attr(each) >> clavMusSrg.attr(each)

            ## Lock Attribute minX, maxZ
            if 'minX' in each or 'maxZ' in each:
                ctrlShape.lockAttrs(each)

        clavMusSrg.attr('outValueX') >> clavMusCon.attr('colorIfTrueR')
        clavMusSrg.attr('outValueZ') >> clavMusCon.attr('colorIfFalseR')
        driver.attr(driverAxis) >> clavMusCon.attr('firstTerm')
        driver.attr(driverAxis) >> clavMusSrg.attr('valueX')
        driver.attr(driverAxis) >> clavMusSrg.attr('valueZ')

        onOffSwitchBcol = cn.createNode('blendColors', '{}{}OnOff{}Bcol'.format(name, elem, side))
        onOffSwitchBcol.attr('color2R').value = drivenValue
        ctrl.attr('{}{}OnOff'.format(name,elem)) >> onOffSwitchBcol.attr('blender')

        clavMusCon.attr('outColorR') >> onOffSwitchBcol.attr('color1R')
        onOffSwitchBcol.attr('outputR') >> driven.attr(drivenAxis)

        attr = ['minX', 'minY', 'minZ', 'maxX', 'maxY', 'maxZ', 'oldMinX', 'oldMinY', 'oldMinZ', 'oldMaxX', 'oldMaxY', 'oldMaxZ']

    mc.select( cl = True)