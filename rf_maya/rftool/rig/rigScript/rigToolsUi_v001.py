import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
from functools import partial
import subprocess
import shutil
import  re
import os , sys
from collections import defaultdict
import core
reload( core )
import rigTools as rt
reload( rt )
from rf_utils.context import context_info
reload(context_info)
import keyAnim
reload(keyAnim)
########## setWinDowIcon ############################
from maya import OpenMayaUI as omui
# Special cases for different Maya versions
try:
    from shiboken2 import wrapInstance
except ImportError:
    from shiboken import wrapInstance
try:
    from PySide2.QtGui import QIcon
    from PySide2.QtWidgets import QWidget
except ImportError:
    from PySide.QtGui import QIcon, QWidget
######################################################

cn = core.Node()

#-- ui
class Run(object):
    def __init__(self):
        self.winName = 'rigToolsUi'
        self.corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        self.pathScript = "{}/rf_maya/rftool/rig/rigScript".format(self.corePath)
        self.sublimePath = "C:/Program Files/Sublime Text 3/sublime_text.exe"

    def show(self , *args):
        # check if window exists
        if pm.window ( self.winName , exists = True ) :
            pm.deleteUI ( self.winName ) 
        else : pass

        # open popup help from preference
        if not mc.help(q=True,pm=True):
            mc.help(pm=True)

        winWidth = 520
        winHeight = 440

        pm.window(self.winName,title = 'rigTools' , width = winWidth , rtf = True, mxb = False , s=1)
        #-- png dir path script , check environ base to drive local
        #-- split into two columns left is main rigging tools process right is rigging tools for working in process
        #-- place cursor at button it will show script name(just some button)
        #-- right click to open script in sublime or read text(how to use)

        # Head Icon UI ---
        qw = omui.MQtUtil.findWindow(self.winName)
        widget = wrapInstance(long(qw), QWidget)
        
        # Create a QIcon object
        icon = QIcon("{}{}".format(self.imagePath() ,'rigToolsUiIcon.png'))
        
        # Assign the icon
        widget.setWindowIcon(icon)
        # End Head Icon UI ---

        mainLayout = pm.columnLayout()
        bannerRow = pm.rowColumnLayout( w = winWidth, h = 75, nc = 1 , p = mainLayout)
        with bannerRow:
            pm.symbolButton('openDirScriptPath',image=("{}{}".format(self.imagePath(),"Banner-01.png")),ann='Open Directory Script Path',c=partial(self.openInExplorer_cmd,'rftool/rig/rigScript',self.pathModule()),w=winWidth*0.99)
                    

        pm.separator(h = 10)

        winLayout = pm.rowColumnLayout( w = winWidth , p = mainLayout , nc = 3 , cw = [(1, winWidth*0.5), (2, winWidth*0.02), (3, winWidth*0.47)]) ;
        tmpLayout = pm.rowColumnLayout( p = winLayout , nc = 1  )
        spaceLayout = pm.rowColumnLayout( p = winLayout , nc = 1 )
        toolLayout = pm.rowColumnLayout( p = winLayout , nc = 1 )


        endMarginRow2 = pm.rowColumnLayout( w = winWidth, h = 100, nc = 1 , p = mainLayout)
        with endMarginRow2:
            pm.text(l='05-2021 @RiFF Studio',al='right',w=winWidth*0.98 , h=160)


        mainTab_module = pm.tabLayout( innerMarginWidth = 5 , innerMarginHeight = 5 , p = tmpLayout , w=winWidth*0.5)
        with mainTab_module:
            mainRigTab = pm.rowColumnLayout( "Main", nc = 1 , cw=(1 , winWidth*0.49) , h=winHeight)
            with mainRigTab:
                pm.text( l='# Previs', al = "left" , h = 20)
                pvRow = pm.rowColumnLayout( nc=4 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.225),(3 , winWidth*0.01),(4 , winWidth*0.225)])
                with pvRow:
                    pm.separator(vis = False)

                    pm.button('previsTmpJnt' , l='Previs Tmp Jnt' , h=40 , c=partial(self.importTemplateRig_cmd,'pvRig_skel_tmp'))
                    pm.popupMenu(p='previsTmpJnt' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"template"))

                    pm.separator(vis = False)

                    pm.button('previsRunRig' , l='Previs Run Script' , h=40 , c=partial(self.runTemplateRig_cmd,'pvRig'))
                    pm.popupMenu(p='previsRunRig' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"template"))


                pm.text( l='' )

                pm.separator(h = 7, st = 'in')
                pm.text( l='# Main Rig', al = "left" , h=20)
                humanRigRow = pm.rowColumnLayout( nc=4 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.225),(3 , winWidth*0.01),(4 , winWidth*0.225)])
                with humanRigRow:
                    pm.separator(vis = False)
                    pm.button('humanTmpJnt' , l='Human Tmp Jnt' , h=30 , c=partial(self.importTemplateRig_cmd,'humanRig_skel_tmp'))
                    pm.popupMenu(p='humanTmpJnt' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"template"))
                    pm.separator(vis = False)
                    pm.button('humanCopyScript' , l='Human Copy Script' , h=30 , c=partial(self.copyCharacterTemplate_cmd,'human','py'))
                    pm.popupMenu(p='humanCopyScript' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"template"))
                creatureRigRow = pm.rowColumnLayout( nc=4 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.225),(3 , winWidth*0.01),(4 , winWidth*0.225)])
                with creatureRigRow:
                    pm.separator(vis = False)
                    pm.button('creatureTmpJnt' , l='Creature Tmp Jnt' , h=30)
                    pm.popupMenu(p='creatureTmpJnt' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=self.test)
                    pm.separator(vis = False)
                    pm.button('creatureCopyScript' , l='Creature Copy Script' , h=30)
                    pm.popupMenu(p='creatureCopyScript' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=self.test)

                pm.text( l='' )

                runRigRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with runRigRow:
                    pm.separator(vis = False)
                    pm.button('runMainRig' , l='Run mainRig' , h=50 , c=self.runMainRig_cmd )
                    pm.popupMenu(p='runMainRig' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=self.openDataPathInExplorer_cmd)
                    pm.menuItem(d=True)
                    pm.menuItem(l='backupScript' , c=partial(self.backupScript,'moduleRig'))
                    pm.separator(vis = False)

                pm.text( l='' )
                pm.separator(h = 7, st = 'in')

                pm.text( l='# Model Med Geometry', al = "left" , h=20)

                mdlGeoRefRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mdlGeoRefRow:
                    pm.separator(vis = False)
                    pm.button('referenceGeoMdl' , l='Reference Model Med Geo' , h=20 , c=partial(self.modelGeo_cmd , 'ref') )
                    pm.separator(vis = False)

                mdlGeoImRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mdlGeoImRow:
                    pm.separator(vis = False)
                    pm.button('importGeoMdl' , l='Import Model Med Geo' , h=20 , c=partial(self.modelGeo_cmd , 'import') )
                    pm.separator(vis = False)
                pm.text( l='' )
                pm.separator(h = 7, st = 'in')


        with mainTab_module:
            hdRigTab = pm.rowColumnLayout( "hdRig", nc = 1 , cw=(1 , winWidth*0.49) , h=winHeight)
            with hdRigTab:
                pm.text( l='# Import Head and Rename from process', al = "left" , h=20)

                mdlGeoImHdRigRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mdlGeoImHdRigRow:
                    pm.separator(vis = False)
                    pm.button(l='Import Head Geo' , h=20 , c=partial(self.importHdGeoAndRename_cmd , 'import') )
                    pm.separator(vis = False)

                mdlGeoRefHdRigRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mdlGeoRefHdRigRow:
                    pm.separator(vis = False)
                    pm.button(l='Reference Head Geo' , h=20 , c=partial(self.importHdGeoAndRename_cmd , 'ref') )
                    pm.separator(vis = False)

                pm.text( l='' )
                pm.separator(h = 7, st = 'in')

                pm.text( l='# HdRig', al = "left" , h = 20)
                hdRigRow = pm.rowColumnLayout( nc=4 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.225),(3 , winWidth*0.01),(4 , winWidth*0.225)])
                with hdRigRow:
                    pm.separator(vis = False)

                    pm.button('hdRigTmpJnt' , l='HdRig Tmp Jnt' , h=40 , c=partial(self.importTemplateRig_cmd,'hdRig_skel_tmp'))
                    pm.popupMenu(p='hdRigTmpJnt' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"template"))
                    pm.separator(vis = False)
                    pm.button('hdRigRun' , l='HdRig Run Script' , h=30 , c=partial(self.runModuleSctipt_cmd,'hdRig'))
                    pm.popupMenu(p='hdRigRun' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"template"))
                    pm.menuItem(l='openTipInExplorer' , c=partial(self.openInExplorer_cmd,"template/tip/hdRig"))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"hdRig.py"))

                pm.text( l='' )

                hdRigRefRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)])
                with hdRigRefRow:
                    pm.separator(vis = False)

                    pm.button('hdRigRefCtrl' , l='Reference HdRig Ctrl' , h=30 , c=partial(self.referenceRigHero_cmd,'hdRigCtrl'))

                pm.text( l='' )
                pm.separator(h = 7, st = 'in')


        with mainTab_module:
            mthRigTab = pm.rowColumnLayout( "MthRig", nc = 1 , cw=(1 , winWidth*0.49) , h=winHeight)
            with mthRigTab:
                pm.text( l='# Import Head and Rename from process', al = "left" , h=20)

                mdlGeoImMthRigRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mdlGeoImMthRigRow:
                    pm.separator(vis = False)
                    pm.button(l='Import Head Geo' , h=20 , c=partial(self.importHdGeoAndRename_cmd , 'import') )
                    pm.separator(vis = False)

                mdlGeoRefMthRigRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mdlGeoRefMthRigRow:
                    pm.separator(vis = False)
                    pm.button(l='Reference Head Geo' , h=20 , c=partial(self.importHdGeoAndRename_cmd , 'ref') )
                    pm.separator(vis = False)

                pm.text( l='' )
                pm.separator(h = 7, st = 'in')

                pm.text( l='# MthRig', al = "left" , h = 20)
                mthRigRow1 = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mthRigRow1:
                    pm.separator(vis = False)

                    pm.button('mthRigTmpJnt' , l='MthRig Tmp Jnt' , h=30 , c=partial(self.importTemplateRig_cmd,'mthRig_skel_tmp'))
                    pm.popupMenu(p='mthRigTmpJnt' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"template"))
                    pm.separator(vis = False)

                mthRigRow2 = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mthRigRow2:
                    pm.separator(vis = False)

                    pm.button('dupJntFromCtrl' , l='Duplicate Jnt from Ctrl' , h=30 , c=self.duplicateMthObj_cmd)
                    pm.popupMenu(p='dupJntFromCtrl' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.separator(vis = False)
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                mthRigRow3 = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mthRigRow3:
                    pm.separator(vis = False)

                    pm.button('crvSnapUi' , l='Curve Snap ' , h=30 , c=self.runMouthRigSctipt_cmd)
                    pm.popupMenu(p='crvSnapUi' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"template"))
                    pm.menuItem(l='openTipInExplorer' , c=partial(self.openInExplorer_cmd,"template/tip/mthRig"))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"mouthLoop.py"))
                    pm.separator(vis = False)

                mthRigRow4 = pm.rowColumnLayout( nc=7 , cw=[(1,winWidth*0.01),(2,winWidth*0.125),(3,winWidth*0.1),(4,winWidth*0.01),(5,winWidth*0.125),(6,winWidth*0.1),(7,winWidth*0.01)] )
                with mthRigRow4:
                    pm.separator(vis = False)

                    pm.text( l='numJnt >>' )
                    pm.intField('numJntValue' ,ec='enter',v=28, ed = True )
                    pm.text( l='' )
                    pm.text( l='skipJnt >>' )
                    pm.intField('skipJntValue' ,ec='enter',v=4, ed = True )

                mthRigRow5 = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mthRigRow5:
                    pm.separator(vis = False)

                    pm.button('mthRigRun' , l='MthRig Run Script' , h=40 , c=partial(self.runModuleSctipt_cmd,'mouthRig'))
                    pm.popupMenu(p='mthRigRun' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"template"))
                    pm.menuItem(l='openTipInExplorer' , c=partial(self.openInExplorer_cmd,"template/tip/mthRig"))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"mouthRig.py"))

                pm.text( l='' )

                mthRigRefRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)])
                with mthRigRefRow:
                    pm.separator(vis = False)

                    pm.button('mthRigRefCtrl' , l='Reference MthRig Ctrl' , h=30 , c=partial(self.referenceRigHero_cmd,'mthRigCtrl'))

                pm.text( l='' )
                pm.separator(h = 7, st = 'in')


        with mainTab_module:
            ttRigTab = pm.rowColumnLayout( "TtRig", nc = 1 , cw=(1 , winWidth*0.49) , h=winHeight)
            with ttRigTab:
                pm.text( l='# Import Head and Rename from process', al = "left" , h=20)

                mdlGeoImTtRigRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mdlGeoImTtRigRow:
                    pm.separator(vis = False)
                    pm.button(l='Import Head Geo' , h=20 , c=partial(self.importHdGeoAndRename_cmd , 'import') )
                    pm.separator(vis = False)

                mdlGeoRefTtRigRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mdlGeoRefTtRigRow:
                    pm.separator(vis = False)
                    pm.button(l='Reference Head Geo' , h=20 , c=partial(self.importHdGeoAndRename_cmd , 'ref') )
                    pm.separator(vis = False)

                pm.text( l='' )
                pm.separator(h = 7, st = 'in')

                pm.text( l='# TtRig', al = "left" , h = 20)
                ttRigRow = pm.rowColumnLayout( nc=4 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.225),(3 , winWidth*0.01),(4 , winWidth*0.225)] )
                with ttRigRow:
                    pm.separator(vis = False)

                    pm.button('ttRigTmpJnt' , l='TtRig Tmp Jnt' , h=30 , c=partial(self.importTemplateRig_cmd,'ttRig_skel_tmp'))
                    pm.popupMenu(p='ttRigTmpJnt' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"template"))
                    pm.separator(vis = False)
                    pm.button('ttRigRun' , l='TtRig Run Script' , h=30 , c=partial(self.runModuleSctipt_cmd,'ttRig'))
                    pm.popupMenu(p='ttRigRun' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"template"))
                    pm.menuItem(l='openTipInExplorer' , c=partial(self.openInExplorer_cmd,"template/tip/ttRig"))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"ttRig.py"))

                pm.text( l='' )

                ttRigRefRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)])
                with ttRigRefRow:
                    pm.separator(vis = False)

                    pm.button('ttRigRefCtrl' , l='Reference TtRig Ctrl' , h=30 , c=partial(self.referenceRigHero_cmd,'ttRigCtrl'))

                pm.text( l='' )
                pm.separator(h = 7, st = 'in')


        with mainTab_module:
            dtlRigTab = pm.rowColumnLayout( "DtlRig", nc = 1 , cw=(1 , winWidth*0.49) , h=winHeight)
            with dtlRigTab:
                pm.text( l='# Import Head and Rename from process', al = "left" , h=20)

                mdlGeoImdtlRigRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mdlGeoImdtlRigRow:
                    pm.separator(vis = False)
                    pm.button(l='Import Head Geo' , h=20 , c=partial(self.importHdGeoAndRename_cmd , 'import') )
                    pm.separator(vis = False)

                mdlGeoRefdtlRigRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mdlGeoRefdtlRigRow:
                    pm.separator(vis = False)
                    pm.button(l='Reference Head Geo' , h=20 , c=partial(self.importHdGeoAndRename_cmd , 'ref') )
                    pm.separator(vis = False)

                pm.text( l='' )
                pm.separator(h = 7, st = 'in')

                pm.text( l='# DtlRig', al = "left" , h = 20)
                dtlRigRow = pm.rowColumnLayout( nc=4 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.225),(3 , winWidth*0.01),(4 , winWidth*0.225)] )
                with dtlRigRow:
                    pm.separator(vis = False)

                    pm.button('dtlRigTmpJnt' , l='DtlRig Tmp Jnt' , h=30 , c=partial(self.importTemplateRig_cmd,'dtlRig_skel_tmp'))
                    pm.popupMenu(p='dtlRigTmpJnt' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"template"))
                    pm.separator(vis = False)
                    pm.button('dtlRigRun' , l='dtlRig Run Script' , h=30 , c=partial(self.runModuleSctipt_cmd,'dtlRig'))
                    pm.popupMenu(p='dtlRigRun' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"template"))
                    pm.menuItem(l='openTipInExplorer' , c=partial(self.openInExplorer_cmd,"template/tip/dtlRig"))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"dtlRig.py"))

                pm.text( l='' )

                ttRigRefRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)])
                with ttRigRefRow:
                    pm.separator(vis = False)

                    pm.button('dtlRigRefCtrl' , l='Reference DtlRig Ctrl' , h=30 , c=partial(self.referenceRigHero_cmd,'dtlRigCtrl'))

                pm.text( l='' )
                pm.separator(h = 7, st = 'in')


        with mainTab_module:
            bshRigTab = pm.rowColumnLayout( "BshRig", nc = 1 , cw=(1 , winWidth*0.49) , h=winHeight)
            with bshRigTab:
                pm.text( l='# Import Head and Rename from process', al = "left" , h=20)

                mdlGeoImbshRigRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mdlGeoImbshRigRow:
                    pm.separator(vis = False)
                    pm.button(l='Import Head Geo' , h=20 , c=partial(self.importHdGeoAndRename_cmd , 'import') )
                    pm.separator(vis = False)

                mdlGeoRefbshRigRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mdlGeoRefbshRigRow:
                    pm.separator(vis = False)
                    pm.button(l='Reference Head Geo' , h=20 , c=partial(self.importHdGeoAndRename_cmd , 'ref') )
                    pm.separator(vis = False)

                pm.text( l='' )
                pm.separator(h = 7, st = 'in')

                pm.text( l='# bshRig', al = "left" , h = 20)
                bshRigCtrlRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)])
                with bshRigCtrlRow:
                    pm.separator(vis = False)

                    pm.button('importBshRigCtrl' , l='Import BshRig Ctrl' , h=20 , c=partial(self.importTemplateRig_cmd,'bshRig_ctrl_tmp'))

                bshRigRow = pm.rowColumnLayout( nc=4 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.225),(3 , winWidth*0.01),(4 , winWidth*0.225)] )
                with bshRigRow:
                    pm.separator(vis = False)

                    pm.button('runBshRigDefault' , l='Run Default' , h=30 , c=self.runBshRigDefault_cmd)
                    pm.popupMenu(p='runBshRigDefault' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"template"))
                    pm.menuItem(d=True)
                    pm.menuItem(l='copyDataToChar' , c=self.copyBshDefaultDataToCharData_cmd)
                    pm.separator(vis = False)
                    pm.button('runBshRigCharData' , l='Run Char Data' , h=30 , c=self.runBshRigCharData_cmd)
                    pm.popupMenu(p='runBshRigCharData' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=self.openDataPathInExplorer_cmd)

                pm.text( l='' )
                pm.separator(h = 7, st = 'in')

                pm.text( l='# bshRig Main Tools', al = "left" , h = 20)
                bshRigMainToolsRow = pm.rowColumnLayout( nc=4 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.225),(3 , winWidth*0.01),(4 , winWidth*0.225)] )
                with bshRigMainToolsRow:
                    pm.separator(vis = False)

                    pm.button('reBsh' , l='Re-BshRig' , h=30 , c=self.reBshRig_cmd)
                    pm.separator(vis = False)
                    pm.button('reRig' , l='Re-Blendshape' , h=30 , c=self.reBlendshape_cmd)

                pm.text( l='' )
                pm.separator(h = 7, st = 'in')


        with mainTab_module:
            bshRigToolsTab = pm.rowColumnLayout( "BshTools", nc = 1 , cw=(1 , winWidth*0.49) , h=winHeight)
            with bshRigToolsTab:
                pm.text( l='# Connect Locator to Blendshape', al = "left" , h = 20)
                bshRigToolsRow1 = pm.rowColumnLayout( nc=4 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.225),(3 , winWidth*0.01),(4 , winWidth*0.225)] )
                with bshRigToolsRow1:
                    pm.separator(vis = False)

                    pm.button('connectBshRig' , l='Connect' , h=20 , c=partial(self.connectBshRig_cmd , True ))
                    pm.popupMenu(p='connectBshRig' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.separator(vis = False)
                    pm.button('disconnectBshRig' , l='Disconnect' , h=20 , c=partial(self.connectBshRig_cmd , False ))
                    pm.popupMenu(p='disconnectBshRig' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))

                pm.separator( h=7 , st = 'in')

                pm.text( l='# Add Inbetween blendShape', al = "left" , h = 15)
                bshRigToolsRow2 = pm.rowColumnLayout( nc=6 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.11),(3 , winWidth*0.192),(4 , winWidth*0.01),(5 , winWidth*0.1),(6 , winWidth*0.05)] )
                with bshRigToolsRow2:
                    pm.separator(vis = False)
                    pm.text( l='Grp Name', al = "left" , h = 20)
                    pm.textField('inbtwnName',tx='Name',ed=True,ec='enter')
                    pm.separator(vis = False)
                    pm.text( l='Grp Side', al = "left" , h = 20)
                    pm.textField('inbtwnSide',tx='L',ed=True,ec='enter')

                bshRigToolsRow3 = pm.rowColumnLayout( nc=4 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.11),(3 , winWidth*0.25),(4 , winWidth*0.1)] )
                with bshRigToolsRow3:
                    pm.separator(vis = False)
                    pm.text( l='Target', al = "left" , h = 20)
                    pm.textField('inbtwnTarget',tx='UpLidDn_L',ed=True,ec='enter')
                    pm.button('getTargetName' , l='Get' , h=20 , c=self.getTargetGrpBshName_cmd)

                bshRigToolsRow4 = pm.rowColumnLayout( nc=4 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.11),(3 , winWidth*0.1),(4 , winWidth*0.25)] )
                with bshRigToolsRow4:
                    pm.separator(vis = False)
                    pm.text( l='Value', al = "left" , h = 20)
                    pm.floatField('inbtwnValue',v=0.5,ed=True,ec='enter')
                    pm.button('runAddInbtwn' , l='Add Inbetween Bsh' , h=20 , c=self.addInbetweenBlendshape_cmd)

                pm.separator( h=7 , st = 'in')

                pm.text( l='# Add Item Geo blendshape', al = "left" , h = 15)
                bshRigToolsRow5 = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)])
                with bshRigToolsRow5:
                    pm.separator(vis = False)

                    pm.button('addGeoBsh' , l='Add Item Geo blendshape' , h=20 , c=self.addMoreBshGeo_cmd)

                pm.separator( h=7 , st = 'in')

                pm.text( l='# Add blendshape head', al = "left" , h = 15)
                bshRigToolsRow6 = pm.rowColumnLayout( nc=3 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.11),(3 , winWidth*0.35)] )
                with bshRigToolsRow6:
                    pm.separator(vis = False)
                    pm.text( l='Grp Name', al = "left" , h = 20)
                    pm.textField('bshList',tx='NameA,NameB',ed=True,ec='enter')

                bshRigToolsRow7 = pm.rowColumnLayout( nc=6 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.11),(3 , winWidth*0.192),(4 , winWidth*0.01),(5 , winWidth*0.1),(6 , winWidth*0.05)] )
                with bshRigToolsRow7:
                    pm.separator(vis = False)
                    pm.text( l='Set Name', al = "left" , h = 20)
                    pm.textField('setName',tx='Name',ed=True,ec='enter')
                    pm.separator(vis = False)
                    pm.text( l='Set Side', al = "left" , h = 20)
                    pm.textField('setSide',tx='L',ed=True,ec='enter')

                bshRigToolsRow8 = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)])
                with bshRigToolsRow8:
                    pm.separator(vis = False)

                    pm.button('runAddBsh' , l='Add blendshape' , h=20 , c=self.addBshGrp_cmd)

                pm.separator( h=7 , st = 'in')

                pm.text( l='# Add Rig (add attr before run script)', al = "left" , h = 15)
                bshRigToolsRow9 = pm.rowColumnLayout( nc=6 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.11),(3 , winWidth*0.146),(4 , winWidth*0.01),(5 , winWidth*0.05),(6 , winWidth*0.146)] )
                with bshRigToolsRow9:
                    pm.separator(vis = False)
                    pm.text( l='Name', al = "left" , h = 20)
                    pm.textField('bshRigName',tx='NameAB_L',ed=True,ec='enter')
                    pm.separator(vis = False)
                    pm.text( l='Attr', al = "left" , h = 20)
                    pm.textField('bshRigAttr',tx='NameAB',ed=True,ec='enter')

                bshRigToolsRow10 = pm.rowColumnLayout( nc=3 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.11),(3 , winWidth*0.35)] )
                with bshRigToolsRow10:
                    pm.separator(vis = False)
                    pm.text( l='Source', al = "left" , h = 20)
                    pm.textField('source',tx='NoseBsh_Ctrl',ed=True,ec='enter')

                bshRigToolsRow11 = pm.rowColumnLayout( nc=3 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.11),(3 , winWidth*0.35)] )
                with bshRigToolsRow11:
                    pm.separator(vis = False)
                    pm.text( l='Target Pos', al = "left" , h = 20)
                    pm.textField('targetPos',tx='FacialAll_LocShape.NameA_L',ed=True,ec='enter')

                bshRigToolsRow12 = pm.rowColumnLayout( nc=3 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.11),(3 , winWidth*0.35)] )
                with bshRigToolsRow12:
                    pm.separator(vis = False)
                    pm.text( l='Target Neg', al = "left" , h = 20)
                    pm.textField('targetNeg',tx='FacialAll_LocShape.NameB_L',ed=True,ec='enter')

                bshRigToolsRow13 = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)])
                with bshRigToolsRow13:
                    pm.separator(vis = False)

                    pm.button('runBshAddRig' , l='Run blendshape Add Rig' , h=20 , c=self.addBshAddRig_cmd)

                pm.separator( h=7 , st = 'in')

                pm.text( l='# Write BshRig Data', al = "left" , h = 15)
                bshRigToolsRow14 = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)])
                with bshRigToolsRow14:
                    pm.separator(vis = False)

                    pm.button('writeBshRigData' , l='Write BshRig Data' , h=30 , c=self.writeBshRigData_cmd)
                    pm.popupMenu(p='writeBshRigData' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=self.openDataPathInExplorer_cmd)

        with mainTab_module:
            wrinkleRigTab = pm.rowColumnLayout( "wnkRig", nc = 1 , cw=(1 , winWidth*0.49) , h=winHeight)
            with wrinkleRigTab:
                pm.text( l='# Import Model', al = "left" , h = 20)  
                wrinkleRigRow1 = pm.rowColumnLayout( nc=5 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)])
                with wrinkleRigRow1:
                    pm.separator(vis = False)
                    pm.button('wnkRigRun' , l='Import Model Med Geo' , h=20 ,c =partial(self.modelGeo_cmd , 'import'))
                    pm.separator(vis = False)

                pm.separator( h=5 , st = 'none')    
                pm.separator( h=7 , st = 'in')

                pm.text( l='# WrinkleRig', al = "left" , h = 20) 
                wrinkleRigRow2 = pm.rowColumnLayout( nc=3 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.11),(3 , winWidth*0.35)] )
                with wrinkleRigRow2:
                    pm.separator(vis = False)
                    pm.text( l='Name', al = "left" , h = 20)
                    pm.textField('wnkRigElem',ed=True,ec='enter')

                wrinkleRigRow3 = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)])
                with wrinkleRigRow3:
                    pm.separator(vis = False)
                    pm.button('wnkRigRun' , l='WnkRig Run Script' , h=20 , c=partial(self.runWnkRigSctipt_cmd))
                    pm.separator(vis = False)


                pm.separator( h=7 , st = 'in')

        with mainTab_module:
            fclTab = pm.rowColumnLayout( "FclRig", nc = 1 , cw=(1 , winWidth*0.49) , h=winHeight)
            with fclTab:
                pm.text( l='# Import HeadGeo', al = "left" , h = 20)
                mdlGeoImFclRigRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with mdlGeoImFclRigRow:
                    pm.separator(vis = False)
                    pm.button(l='Import Head Geo' , h=20 , c=partial(self.importHdGeoAndRename_cmd , 'import') )
                    pm.separator(vis = False)

                createMainGrpRow = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with createMainGrpRow:
                    pm.separator(vis = False)
                    pm.button(l='Create Main Grp' , h=20 , c=self.createMainGrp_cmd )
                    pm.separator(vis = False)

                pm.separator( h=5 , st = 'none')
                pm.separator( h=7 , st = 'in')

                pm.text( l='# Create Reference FacialRig', al = "left" , h = 20)
                hdRigRefRow = pm.rowColumnLayout( nc=6 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.15),(3 , winWidth*0.025),(4 , winWidth*0.14),(5 , winWidth*0.01),(6 , winWidth*0.14)] )
                with hdRigRefRow:
                    pm.separator(vis = False)
                    pm.text( l='HdRig File', al = "left" , h = 20)
                    pm.text( l=':', al = "left" , h = 20)
                    pm.button('RefHdRig' , l='Ref' , h=20 , c=partial(self.referenceRigHero_cmd,'hdRig'))
                    pm.separator(vis = False)
                    pm.button('ParentHdRig' , l='Parent' , h=20 , c=partial(self.parentFclRig_cmd,'HdRig'))

                mthRigRefRow = pm.rowColumnLayout( nc=6 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.15),(3 , winWidth*0.025),(4 , winWidth*0.14),(5 , winWidth*0.01),(6 , winWidth*0.14)] )
                with mthRigRefRow:
                    pm.separator(vis = False)
                    pm.text( l='MthRig File', al = "left" , h = 20)
                    pm.text( l=':', al = "left" , h = 20)
                    pm.button('RefMthRig' , l='Ref' , h=20 , c=partial(self.referenceRigHero_cmd,'mthRig'))
                    pm.separator(vis = False)
                    pm.button('ParentMthRig' , l='Parent' , h=20 , c=partial(self.parentFclRig_cmd,'MthRig'))

                ttRigRefRow = pm.rowColumnLayout( nc=6 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.15),(3 , winWidth*0.025),(4 , winWidth*0.14),(5 , winWidth*0.01),(6 , winWidth*0.14)] )
                with ttRigRefRow:
                    pm.separator(vis = False)
                    pm.text( l='TtRig File', al = "left" , h = 20)
                    pm.text( l=':', al = "left" , h = 20)
                    pm.button('RefTtRig' , l='Ref' , h=20 , c=partial(self.referenceRigHero_cmd,'ttRig'))
                    pm.separator(vis = False)
                    pm.button('ParentTtRig' , l='Parent' , h=20 , c=partial(self.parentFclRig_cmd,'TtRig'))

                dtlRigRefRow = pm.rowColumnLayout( nc=6 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.15),(3 , winWidth*0.025),(4 , winWidth*0.14),(5 , winWidth*0.01),(6 , winWidth*0.14)] )
                with dtlRigRefRow:
                    pm.separator(vis = False)
                    pm.text( l='DtlRig File', al = "left" , h = 20)
                    pm.text( l=':', al = "left" , h = 20)
                    pm.button('RefDtlRig' , l='Ref' , h=20 , c=partial(self.referenceRigHero_cmd,'dtlRig'))
                    pm.separator(vis = False)
                    pm.button('ParentDtlRig' , l='Parent' , h=20 , c=partial(self.parentFclRig_cmd,'DtlRig'))

                bshRigRefRow = pm.rowColumnLayout( nc=6 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.15),(3 , winWidth*0.025),(4 , winWidth*0.14),(5 , winWidth*0.01),(6 , winWidth*0.14)] )
                with bshRigRefRow:
                    pm.separator(vis = False)
                    pm.text( l='BshRig File', al = "left" , h = 20)
                    pm.text( l=':', al = "left" , h = 20)
                    pm.button('RefBshRig' , l='Ref' , h=20 , c=partial(self.modelBshGeo_cmd,'ref'))
                    pm.separator(vis = False)
                    pm.button('ParentBshRig' , l='Parent' , h=20 , c=partial(self.parentFclRig_cmd ,'BshRig')) 

                wnkRigRefRow = pm.rowColumnLayout( nc=6 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.15),(3 , winWidth*0.025),(4 , winWidth*0.14),(5 , winWidth*0.01),(6 , winWidth*0.14)] )
                with wnkRigRefRow:
                    pm.separator(vis = False)
                    pm.text( l='WnkRig File', al = "left" , h = 20)
                    pm.text( l=':', al = "left" , h = 20)
                    pm.button('RefWnkRig' , l='Ref' , h=20 , c=partial(self.referenceRigHero_cmd,'wnkRig'))
                    pm.separator(vis = False)
                    pm.button('ParentWnkRig' , l='Parent' , h=20 , c=partial(self.parentFclRig_cmd,'WnkRig'))

                pm.separator( h=5 , st = 'none')
                pm.separator( h=7 , st = 'in')


                pm.text( l='# Facial Assemble', al = "left" , h = 20)
                fclAsbRow1 = pm.rowColumnLayout( nc=3 , cw=[(1,winWidth*0.01),(2,winWidth*0.46),(3,winWidth*0.01)] )
                with fclAsbRow1:
                    pm.separator(vis = False)
                    pm.button(l='Copy yml assemble' , h=20 , c=partial(self.copyTemplateSctipt_cmd , 'assembleFclRig' , 'yml') )
                    pm.separator(vis = False)

                fclAsbRow2 = pm.rowColumnLayout( nc=4 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.225),(3 , winWidth*0.01),(4 , winWidth*0.225)] )
                with fclAsbRow2:
                    pm.separator(vis = False)
                    pm.button('fclRigRef' , l='FclRig Ref' , h=20 , c=partial(self.referenceRigHero_cmd , 'fclRig' ))
                    pm.separator(vis = False)
                    pm.button('fclAssemble' , l='Assemble' , h=20 , c=self.assemblerFclRigUi_cmd)
                    pm.popupMenu(p='fclAssemble' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"assemblerFclRigUi.py"))

                pm.separator( h=5 , st = 'none')    
                pm.separator( h=7 , st = 'in')


                pm.text( l='# Facial Add-On', al = "left" , h = 20)

                addOnRow1 = pm.rowColumnLayout( nc=4 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.225),(3 , winWidth*0.01),(4 , winWidth*0.225)] )
                with addOnRow1:
                    pm.separator(vis = False)
                    pm.button('eyeLattice' , l='Add EyeLattice' , h=20 , c=partial(self.runModuleSctipt_cmd,'eyeLattice'))
                    pm.separator(vis = False)
                    pm.button('butt1' , l='' , h=20 , c='')
                    # pm.popupMenu(p='fclAssemble' , ctl=False , button = 3)
                    # pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    # pm.menuItem(d=True)
                    # pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"assemblerFclRigUi.py"))
                addOnRow2 = pm.rowColumnLayout( nc=4 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.225),(3 , winWidth*0.01),(4 , winWidth*0.225)] )
                with addOnRow2:
                    pm.separator(vis = False)
                    pm.button('butt2' , l='' , h=20 , c='')
                    pm.separator(vis = False)
                    pm.button('butt3' , l='' , h=20 , c='')

                pm.separator( h=5 , st = 'none')    
                pm.separator( h=7 , st = 'in')

                pm.text( l='# Connnect FurGuide', al = "left" , h = 20)
                furGuideRow1 = pm.rowColumnLayout( nc=7 , cw=[(1 , winWidth*0.01),(2 , winWidth*0.1),(3 , winWidth*0.01),(4 , winWidth*0.21),(5 , winWidth*0.05),(6 , winWidth*0.01),(7 , winWidth*0.08)] )
                with furGuideRow1:
                    pm.separator(vis = False)
                    pm.text( l='BodyFur :', al = "left" , h = 20)
                    pm.separator(vis = False)
                    pm.textField('furGuideName',tx='BodyFurGeo',ed=True,ec='enter')
                    pm.button('getGuideName' , l='<<' , h=20 , c=self.getFurGuideName_cmd)
                    pm.separator(vis = False)
                    pm.button('connectFurGuide' , l='Run' , h=20 , c=self.addFurGuide_cmd)


        mainTab_tools = pm.tabLayout( innerMarginWidth = 5 , innerMarginHeight = 5 , p = toolLayout , w=winWidth*0.47)
        with mainTab_tools:
            rigToolsTab = pm.rowColumnLayout( "Rig Tools", nc = 1 , cw=(1 , winWidth*0.49) , h=winHeight)
            pm.text( l='', al = "left")
            with rigToolsTab:
                toolsRow_1 = pm.rowColumnLayout (nc=12)
                with toolsRow_1:
                    pm.separator(w = 2, st = 'none')

                    pm.iconTextButton('writeWeight',style='iconOnly',image=("{}{}".format(self.imagePath(),"WriteWeight.png")),ann='Write Weight',c=self.writeWeight_cmd , rpt=True)
                    pm.popupMenu(p='writeWeight' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"weightTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('readWeight',style='iconOnly',image=("{}{}".format(self.imagePath(),"ReadWeight.png")),ann='Read Weight',c=self.readWeight_cmd , rpt=True)
                    pm.popupMenu(p='readWeight' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"weightTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('replacedWeightUi',style='iconOnly',image=("{}{}".format(self.imagePath(),"ReadWeightReplace.png")),ann='UI Replaced Weight',c=self.replacedWeightUi_cmd , rpt=True)
                    pm.popupMenu(p='replacedWeightUi' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"replacedWeightUi.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('paintWeight',style='iconOnly',image=("{}{}".format(self.imagePath(),"PaintWeight.png")),ann='Paint Weight(default maya)',c=self.paintSkinWeight_cmd , rpt=True)

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('paintWeightAVGBrush',style='iconOnly',image=("{}{}".format(self.imagePath(),"PaintAverage.png")),ann='Paint Weight Average Brush(plug-in)',c=self.paintSkinWeightAvg_cmd , rpt=True)
                    pm.popupMenu(p='paintWeightAVGBrush' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"plugin"))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"plugin/averageVertexSkinWeightBrush.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('copySelectedWeight',style='iconOnly',image=("{}{}".format(self.imagePath(),"CopyWeight.png")),ann='Copy Selected Weight',c=self.copySelectedWeight_cmd , rpt=True)
                    pm.popupMenu(p='copySelectedWeight' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"weightTools.py"))

                toolsRow_2 = pm.rowColumnLayout (nc=12)
                with toolsRow_2:
                    pm.separator(w = 2, st = 'none')
                    
                    pm.iconTextButton('writeCtrl',style='iconOnly',image=("{}{}".format(self.imagePath(),"WriteCtrl.png")),ann='Write Controls',c=self.writeCtrlShape_cmd , rpt=True)
                    pm.popupMenu(p='writeCtrl' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"ctrlData.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('readCtrl',style='iconOnly',image=("{}{}".format(self.imagePath(),"ReadCtrl.png")),ann='Read Controls',c=self.readCtrlShape_cmd , rpt=True)
                    pm.popupMenu(p='readCtrl' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"ctrlData.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('createJCtrlUi',style='iconOnly',image=("{}{}".format(self.imagePath(),"Ctrl.png")),ann='UI Create Control',c=self.createCtrlUi_cmd , rpt=True)
                    pm.popupMenu(p='createJCtrlUi' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"createCtrlUi.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('scaleGmbl',style='iconOnly',image=("{}{}".format(self.imagePath(),"ScaleGimbal.png")),ann='Scaling All Gimbal Control Size',c=self.setGimbalSize_cmd , rpt=True)
                    pm.popupMenu(p='scaleGmbl' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('mirrorShapeCtrl',style='iconOnly',image=("{}{}".format(self.imagePath(),"MirrorShape.png")),ann='Mirror Shape Controls',c=self.mirrorCtrlShape_cmd , rpt=True)
                    pm.popupMenu(p='mirrorShapeCtrl' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('parentHierachy',style='iconOnly',image=("{}{}".format(self.imagePath(),"ParentHire.png")),ann='Parent Hierachy',c=self.parentHierachy_cmd , rpt=True)
                    pm.popupMenu(p='parentHierachy' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                toolsRow_3 = pm.rowColumnLayout (nc=12)
                with toolsRow_3:
                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('zroGrp',style='iconOnly',image=("{}{}".format(self.imagePath(),"ZroGrp.png")),ann='Add Zro Group',c=self.createZroGroup_cmd , rpt=True)
                    pm.popupMenu(p='zroGrp' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('exportGrp',style='iconOnly',image=("{}{}".format(self.imagePath(),"ExportGrp.png")),ann='Add Export Group',c=self.createExportGrp_cmd , rpt=True)
                    pm.popupMenu(p='exportGrp' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))
                    
                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('deleteGrp',style='iconOnly',image=("{}{}".format(self.imagePath(),"DeleteGrp.png")),ann='Move Object To Delete Group',c=self.deleteGroup_cmd , rpt=True)
                    pm.popupMenu(p='deleteGrp' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('duplicateObj',style='iconOnly',image=("{}{}".format(self.imagePath(),"DupObj.png")),ann='Duplicate Object as Proxy',c=self.duplicateObj)
                    pm.popupMenu(p='duplicateObj' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('importRef',style='iconOnly',image=("{}{}".format(self.imagePath(),"ImportRef.png")),ann='Import Reference',c=self.importSelectRefFile_cmd , rpt=True)
                    pm.popupMenu(p='importRef' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('deleteRef',style='iconOnly',image=("{}{}".format(self.imagePath(),"DeleteRef.png")),ann='Remove Reference',c=self.removeSelectRefFile_cmd , rpt=True)
                    pm.popupMenu(p='deleteRef' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigTools.py"))

                toolsRow_4 = pm.rowColumnLayout (nc=12)
                with toolsRow_4:
                    pm.separator(w = 2, st = 'none')
                    
                    pm.iconTextButton('snapPoint',style='iconOnly',image=("{}{}".format(self.imagePath(),"SnapPoint.png")),ann='Snap Point ObjectA to ObjectB',c=self.snapPoint_cmd , rpt=True)
                    pm.popupMenu(p='snapPoint' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('snapParent',style='iconOnly',image=("{}{}".format(self.imagePath(),"SnapParent.png")),ann='Snap Parent ObjectA to ObjectB',c=self.snapParent_cmd , rpt=True)
                    pm.popupMenu(p='snapParent' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('snapOrient',style='iconOnly',image=("{}{}".format(self.imagePath(),"SnapOrient.png")),ann='Snap Orient ObjectA to ObjectB',c=self.snapOrient_cmd , rpt=True)
                    pm.popupMenu(p='snapOrient' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('createJntFromObj',style='iconOnly',image=("{}{}".format(self.imagePath(),"JointFromObj.png")),ann='Create Joint From Selected Object',c=self.createJointOnPosition_cmd , rpt=True)
                    pm.popupMenu(p='createJntFromObj' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('selectAllJnt',style='iconOnly',image=("{}{}".format(self.imagePath(),"SelectJoint.png")),ann='Selecte All Joints',c=self.selectJnt_cmd , rpt=True)
                    pm.popupMenu(p='selectAllJnt' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('jointFkBuildUi',style='iconOnly',image=("{}{}".format(self.imagePath(),"JointFkBuild.png")),ann='UI Generate Joint and Axis',c=self.generateJntAndAxisUi_cmd , rpt=True)
                    pm.popupMenu(p='jointFkBuildUi' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"generateJntAndAxisUi.py"))

                    pm.separator(w = 4, st = 'none')

                toolsRow_5 = pm.rowColumnLayout (nc=12)
                with toolsRow_5:
                    pm.separator(w = 2, st = 'none')
                    
                    pm.iconTextButton('createMainRig',style='iconOnly',image=("{}{}".format(self.imagePath(),"AllMover.png")),ann='Create Rig Group',c=self.createMainRig_cmd , rpt=True)
                    pm.popupMenu(p='createMainRig' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"mainRig.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('createMainRigWithRoot',style='iconOnly',image=("{}{}".format(self.imagePath(),"AllMoverRoot.png")),ann='Create Rig Group With Root Control',c=self.createMainRigWithRoot_cmd , rpt=True)
                    pm.popupMenu(p='createMainRigWithRoot' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('parScaConstraint',style='iconOnly',image=("{}{}".format(self.imagePath(),"ParentScaleConstaint.png")),ann='Parent and Scale Constraint Object',c=self.parScaConstraint_cmd , rpt=True)
                    pm.popupMenu(p='parScaConstraint' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('connectCtrlGmbl',style='iconOnly',image=("{}{}".format(self.imagePath(),"GmblConstraint.png")),ann='Constraint and Connect Visibility Selected Control To Geo Group',c=self.connectCtrlGmbl_cmd , rpt=True)
                    pm.popupMenu(p='connectCtrlGmbl' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('queryConstraint',style='iconOnly',image=("{}{}".format(self.imagePath(),"QueryConstraint.png")),ann='Query Constraint in Geo Group',c=self.queryConnectConstraint_cmd , rpt=True)
                    pm.popupMenu(p='queryConstraint' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('reConnectConstraint',style='iconOnly',image=("{}{}".format(self.imagePath(),"ReConConstraint.png")),ann='Re-Connect Constraint in Geo Group',c=self.reConnectConstraint_cmd , rpt=True)
                    pm.popupMenu(p='reConnectConstraint' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')

                toolsRow_6 = pm.rowColumnLayout (nc=12)
                with toolsRow_6:
                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('connectTRS',style='iconOnly',image=("{}{}".format(self.imagePath(),"ConnectAll.png")),ann='Connect All Translate Rotate Scale',c=self.connectTRS_cmd , rpt=True)
                    pm.popupMenu(p='connectTRS' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('connectTranslate',style='iconOnly',image=("{}{}".format(self.imagePath(),"ConnectTrans.png")),ann='Connect Translate',c=self.connectTranslate_cmd , rpt=True)
                    pm.popupMenu(p='connectTranslate' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('connectRotate',style='iconOnly',image=("{}{}".format(self.imagePath(),"ConnectRotate.png")),ann='Connect Rotate',c=self.connectRotate_cmd , rpt=True)
                    pm.popupMenu(p='connectRotate' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('connectScale',style='iconOnly',image=("{}{}".format(self.imagePath(),"ConnectScale.png")),ann='Connect Scale',c=self.connectScale_cmd , rpt=True)
                    pm.popupMenu(p='connectScale' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('connectMirrorObj',style='iconOnly',image=("{}{}".format(self.imagePath(),"ConnectLR.png")),ann='Connect Mirror Object',c=self.connectMirrorObj_cmd , rpt=True)
                    pm.popupMenu(p='connectMirrorObj' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('blendShapeObj',style='iconOnly',image=("{}{}".format(self.imagePath(),"BlendShape.png")),ann='Blendshape Selected Object A to B',c=self.blendShapeObj_cmd , rpt=True)
                    pm.popupMenu(p='blendShapeObj' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigTools.py"))

                toolsRow_7 = pm.rowColumnLayout (nc=12)
                with toolsRow_7:
                    pm.separator(w = 2, st = 'none')
                    
                    pm.iconTextButton('localAxisToggle',style='iconOnly',image=("{}{}".format(self.imagePath(),"LRAOn.png")),ann='Local Axis Display Toggle',c=self.displayLocalAxis_cmd , rpt=True)
                    pm.popupMenu(p='localAxisToggle' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('allLocalAxisOff',style='iconOnly',image=("{}{}".format(self.imagePath(),"LRAOff.png")),ann='All Local Axis Display Off',c=self.deleteDisplayLocalAxis_cmd , rpt=True)
                    pm.popupMenu(p='allLocalAxisOff' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('writeAllAttr',style='iconOnly',image=("{}{}".format(self.imagePath(),"WriteAttr.png")),ann='Write All Attribute',c=self.writeAttr_cmd , rpt=True)
                    pm.popupMenu(p='writeAllAttr' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"attrTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('readAllAttr',style='iconOnly',image=("{}{}".format(self.imagePath(),"ReadAttr.png")),ann='Read All Attribute',c=self.readAttr_cmd , rpt=True)
                    pm.popupMenu(p='readAllAttr' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"attrTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('readSelectedAttr',style='iconOnly',image=("{}{}".format(self.imagePath(),"ReadSelectAttr.png")),ann='Read Selected Attribute',c=self.readSelectedAttr_cmd , rpt=True)
                    pm.popupMenu(p='readSelectedAttr' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"attrTools.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('copyAttr',style='iconOnly',image=("{}{}".format(self.imagePath(),"CopyAttr.png")),ann='Copy Attribute ObjectA to ObjectB',c=self.copyAttrs_cmd , rpt=True)
                    pm.popupMenu(p='copyAttr' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')

                toolsRow_8 = pm.rowColumnLayout (nc=12)
                with toolsRow_8:
                    pm.separator(w = 2, st = 'none')
                    
                    pm.iconTextButton('checkModelSymUi',style='iconOnly',image=("{}{}".format(self.imagePath(),"Symystry.png")),ann="UI Check Model's Symmetry",c=self.checkModelSymUi_cmd , rpt=True)
                    pm.popupMenu(p='checkModelSymUi' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"plugin"))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"plugin/abSymMesh.mel"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('checkModelTopoUi',style='iconOnly',image=("{}{}".format(self.imagePath(),"CheckTopo.png")),ann="UI Check Model's Topology",c=self.checkModelTopologyUi_cmd , rpt=True)
                    pm.popupMenu(p='checkModelTopoUi' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,'nuTools/util',self.pathModule()))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd, "geoReplacer.py" , "{}nuTools/util".format(self.pathModule())))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('nameCheckerUi',style='iconOnly',image=("{}{}".format(self.imagePath(),"CheckName.png")),ann="UI Check Model's Name",c=self.nameCheckUi_cmd , rpt=True)
                    pm.popupMenu(p='nameCheckerUi' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"rftool/model",self.pathModule()))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd, "name_checker.py" , "{}rftool/model".format(self.pathModule())))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('cometRename',style='iconOnly',image=("{}{}".format(self.imagePath(),"CometRename.png")),ann='UI Comet Rename',c=self.cometRenameUi_cmd , rpt=True)
                    pm.popupMenu(p='cometRename' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"plugin"))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"plugin/cometRename.mel"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('stringRigUi',style='iconOnly',image=("{}{}".format(self.imagePath(),"String.png")),ann='UI String Rig',c=self.stringRigUi_cmd , rpt=True)
                    pm.popupMenu(p='stringRigUi' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"stringRig.py"))

                    pm.separator(w = 4, st = 'none')

                    pm.iconTextButton('copyKeyOffsetUi',style='iconOnly',image=("{}{}".format(self.imagePath(),"CopyKey.png")),ann='Copy Attribute ObjectA to ObjectB',c=self.keyOffsetUi_cmd , rpt=True)
                    pm.popupMenu(p='copyKeyOffsetUi' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"plugin"))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"plugin/keyOffset.mel"))

                    pm.separator(w = 4, st = 'none')

                toolsRow_9 = pm.rowColumnLayout (nc=12)
                with toolsRow_9:
                    pm.separator(w = 2, st = 'none')
                    
                    pm.iconTextButton('cutProxyUi',image=("{}{}".format(self.imagePath(),"CutPr.png")),ann="UI Cut Proxy",c=self.cutProxyUi_cmd , rpt=True)
                    pm.popupMenu(p='cutProxyUi' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"nuTools/util/proxyRigGenerator",self.pathModule()))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"app.py", "{}nuTools/util/proxyRigGenerator".format(self.pathModule())))

                    pm.separator(w = 4, st = 'none')
                    
                    pm.iconTextButton('addMemberDeformer',image=("{}{}".format(self.imagePath(),"AddMemDfm.png")),ann="UI Add Member Deformer",c=self.addDeformerUi_cmd , rpt=True)
                    pm.popupMenu(p='addMemberDeformer' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,"plugin"))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"plugin/addDeformerMemberUi.py"))

                    pm.separator(w = 4, st = 'none')
                    
                    pm.iconTextButton('ribbonUi',image=("{}{}".format(self.imagePath(),"Ribbon.png")),ann="UI Ribbon Rig",c=self.ribbonRigUi_cmd , rpt=True)
                    pm.popupMenu(p='ribbonUi' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"ribbonRigUi.py"))

                    pm.separator(w = 4, st = 'none')
                    
                    pm.iconTextButton('convertConstraintToMatrix',image=("{}{}".format(self.imagePath(),"ParentMatrix.png")),ann="Convert Constraint Grp to Matrix Parent",c=self.convertConstraintToMatrix_cmd , rpt=True)
                    pm.popupMenu(p='convertConstraintToMatrix' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')


                    pm.iconTextButton('keyAnim', image = 'keyIntoclip.png',ann="check animation by auto keyframe",c=self.keyAnim_cmd , rpt=True)
                    pm.popupMenu(p='keyAnim' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"keyAnim.py"))

                    pm.separator(w = 4, st = 'none')
                toolsRow_10 = pm.rowColumnLayout (nc=12)
                with toolsRow_10:
                    pm.separator(w = 4, st = 'none')
                    pm.symbolButton('reloadUi',image=("{}{}".format(self.imagePath(),"ReloadUi.png")),ann="Reload This UI",c=self.reloadUi_cmd)
                    pm.separator(w = 4, st = 'none')


                pm.separator(h = 12, st = 'none')
                pm.separator(w = 4, st = 'in')

                toolsRow_end = pm.rowColumnLayout (nc=12)
                with toolsRow_end:
                    pm.separator(w = 2, st = 'none')
                    
                    pm.symbolButton('openTaskManager',image=("{}{}".format(self.iconPipelinePath(),"taskManager_sm.png")),ann="Open Task Manager",c=self.openTaskManager_cmd)

                    pm.separator(w = 4, st = 'none')
                    
                    pm.symbolButton('openShotgunManager',image=("{}{}".format(self.iconPipelinePath(),"sg_manager_icon.png")),ann="Open Shotgun Manager",c=self.openShotgunManager_cmd)

                    pm.separator(w = 4, st = 'none')
                    
                    pm.symbolButton('saveIncrement',image=("{}{}".format(self.iconPipelinePath(),"save_sm.png")),ann="Save Increment",c=self.saveIncrement_cmd)

                    pm.separator(w = 4, st = 'none')
                    
                    
                    pm.symbolButton('openPublish',image=("{}{}".format(self.iconPipelinePath(),"publish_sm.png")),ann="Open Publish",c=self.openPublish_cmd)

                    pm.separator(w = 4, st = 'none')
                    
                    pm.symbolButton('openWorkPathInExplorer',image=("{}{}".format(self.imagePath(),"OpenWorkPath.png")),ann="Open Work Path in Explorer",c=self.openWorkPathInExplorer_cmd)
                    pm.popupMenu(p='openWorkPathInExplorer' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')
                    
                    pm.symbolButton('openDataPathInExplorer',image=("{}{}".format(self.imagePath(),"OpenDataPath.png")),ann="Open Data Path in Explorer",c=self.openDataPathInExplorer_cmd)
                    pm.popupMenu(p='openDataPathInExplorer' , ctl=False , button = 3)
                    pm.menuItem(l='openInExplorer' , c=partial(self.openInExplorer_cmd,""))
                    pm.menuItem(d=True)
                    pm.menuItem(l='openInSublime' , c=partial(self.openInSublime_cmd,"rigToolsUi_v001.py"))

                    pm.separator(w = 4, st = 'none')


        pm.showWindow()

    def test(self , *args):
        print 'test'
        return

    def iconPipelinePath(self , *args):
        return "O:/Pipeline/core/icons/"
    
    def imagePath(self , *args):
        return self.pathScript+"/imageIcon/rigToolsIcon/"

    def pathModule(self , *args):
        drv = os.environ['MAYA_MODULE_PATH']
        mainDir = drv.split('modules')[0]
        return mainDir

    def openInExplorer_cmd(self , fldPath = '' , dirPath = '', *args):
        corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        path = '{}/rf_maya/rftool/rig/rigScript'.format(corePath)
        if fldPath:
            path = "{}/{}".format(path , fldPath)
        if dirPath:
            path = "{}/{}".format(self.pathModule() , fldPath)
        print path
        os.startfile(path)

    def openInSublime_cmd(self , fileName = '' , path = '' , *args):
        if not path:
            modulePath = "{}/{}".format(self.pathScript , fileName)
        else:
            modulePath = "{}/{}".format(path , fileName)

        print modulePath
        if os.path.exists(modulePath):
            subprocess.Popen([ self.sublimePath , modulePath ])


    #-------------------------------------------------------------------------------------
    #------------------------------------ Run Modules ------------------------------------
    #-------------------------------------------------------------------------------------

    def runMainRig_cmd(self , *args):
        self.backupScript('moduleRig')
        path = self.getDataPath()
        modulePath = os.path.join(path , 'moduleRig.py')
        print modulePath
        if os.path.exists(modulePath):
            sys.path.insert(0,path)

            import moduleRig
            reload(moduleRig)
            moduleRig.Run()
            self.readCtrlShape_cmd()
            # except Exception as e:
            #     print e
            
            del(moduleRig)
            sys.path.remove(path)

    def importTemplateRig_cmd(self , tmpName='' , *args):
        corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        pathFld = '{}/rf_maya/rftool/rig/rigScript/template'.format(corePath)
        mc.file('{}/{}.ma'.format(pathFld , tmpName), i=True)

    def runTemplateRig_cmd(self , modName='' , *args):
        exec("from rigScript.template import {} as tmp".format(modName))
        exec("reload(tmp)")

        tmp.Run()

    def runModuleSctipt_cmd(self , modName='' , *args):
        exec("import {} as tmp".format(modName))
        exec("reload(tmp)")

        tmp.Run()

    def runWnkRigSctipt_cmd(self , *args):
        import wrinkledRig as wnkRig
        reload(wnkRig)

        txt = pm.textField('wnkRigElem', q=True, tx=True)
        wnkRig.Run(elem=txt)

    def runMouthRigSctipt_cmd(self , *args):
        import mouthRig
        reload(mouthRig)
        num = pm.intField('numJnt' , q=True , v=True)
        skip = pm.intField('skipJnt' , q=True , v=True)

        mouthRig.Run(numJnt = num , skipJnt = skip)

    def copyTemplateSctipt_cmd(self , scriptName='' , fileType = 'py' , *args):
        corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        print fileType
        tmpPath = '{}/rf_maya/rftool/rig/rigScript/template/{}.{}'.format(corePath , scriptName , fileType)
        dataPath = self.getDataPath()
        dst = os.path.join(dataPath , '{}.{}'.format(scriptName , fileType))
        shutil.copy2(tmpPath , dst)

    def copyCharacterTemplate_cmd(self , charType = 'human', *args):
        corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        tmpPath = '{}/rf_maya/rftool/rig/rigScript/template/characterRig/{}Rig/moduleRig.py'.format(corePath , charType)
        dataPath = self.getDataPath()
        dst = os.path.join(dataPath , '{}.py'.format('moduleRig'))
        shutil.copy2(tmpPath , dst)    	

    def getDataPath(self , *args):
        context = context_info.ContextPathInfo()
        projectPath = os.environ['RFPROJECT']
        fileWorkPath = (context.path.name()).replace('$RFPROJECT',projectPath)
        path = fileWorkPath + '/rig/rigData'
        if not os.path.isdir(path):
            os.mkdir(path)
        return path

    def backupScript(self , moduleName = '' , *args):
        checkMessage = mc.confirmDialog(    title='Confirm', 
                                            message='Backup Module Script', 
                                            button=['Yes','No'], 
                                            defaultButton='Yes', 
                                            cancelButton='No', 
                                            dismissString='No' )

        if checkMessage == 'Yes':
            path = self.getDataPath()
            mainPath = os.path.join(path , '{}.py'.format(moduleName))
            bckPath = os.path.join(path , '_bck_{}'.format(moduleName))

            if os.path.exists(mainPath):
                if not os.path.exists(bckPath):
                    os.mkdir(bckPath)
                listDir = os.listdir(bckPath)
                if listDir:
                    lastVer = re.findall('\\d+' , listDir[-1])
                    newVer = int(lastVer[0])+1
                else:
                    newVer = 1
                fileName = '{}_v{:03}.py'.format(moduleName,newVer)
                dst = os.path.join(bckPath , fileName)
                shutil.copy2(mainPath , dst)
                print 'Backup file {} is done.'.format(fileName)
        else:
            print 'Cancel backup file {}.'.format(moduleName)

    def modelGeo_cmd(self , opr = 'import' , *args):
        entity = context_info.ContextPathInfo()
        asset = entity.copy()
        asset.context.update(step='model',process='main',res='md')
        filePath = asset.path.output_hero().abs_path()
        fileName = asset.publish_name(hero=True)

        projectPath = os.environ['RFPROJECT']
        fileHeroPath = filePath.replace('$RFPROJECT',projectPath)
        path = '{}/{}'.format(fileHeroPath,fileName)

        if opr == 'import':
            mc.file(path, i=True)
        elif opr == 'ref':
            mc.file(path, r=True , namespace='{}_md'.format(asset.name) )

    def modelBshGeo_cmd(self , opr = 'import' , *args):
        entity = context_info.ContextPathInfo()
        asset = entity.copy()
        asset.context.update(step='model',process='modelBsh',res='md')
        filePath = asset.path.output_hero().abs_path()
        fileName = asset.publish_name(hero=True)

        projectPath = os.environ['RFPROJECT']
        fileHeroPath = filePath.replace('$RFPROJECT',projectPath)
        path = '{}/{}'.format(fileHeroPath,fileName)

        if opr == 'import':
            mc.file(path, i=True)
        elif opr == 'ref':
            mc.file(path, r=True , namespace='{}_md'.format(asset.name) )


    def importHdGeoAndRename_cmd(self , opr = 'import' , *args):
        entity = context_info.ContextPathInfo()
        asset = entity.copy()
        asset.context.update(step='rig',process='hdGeo',res='md')
        filePath = asset.path.output_hero().abs_path()
        fileName = asset.publish_name(hero=True)

        projectPath = os.environ['RFPROJECT']
        fileHeroPath = filePath.replace('$RFPROJECT',projectPath)
        path = '{}/{}'.format(fileHeroPath,fileName)

        #-- import with mc , you can change to dcc_hook from rf_app later
        if opr == 'import':
            mc.file(path, i=True)
            geoGrp = ['FclRigGeo_Grp']
            child = mc.listRelatives(geoGrp,ad=True)
            for each in child:
                name = each.split('Fcl')
                processName = '{}'.format(((entity.process).split('Rig')[0]).capitalize())
                mc.rename(each,'{}{}{}'.format(name[0],processName,name[1]))
            childNew = mc.listRelatives(geoGrp,c=True)
            mc.rename(geoGrp , '{}RigGeo_Grp'.format(processName))
        elif opr == 'ref':
            mc.file(path, r=True , namespace='{}_md'.format(asset.name) )

    def referenceRigHero_cmd(self , process = 'ctrl' , *args):
        entity = context_info.ContextPathInfo()
        asset = entity.copy()
        asset.context.update(step='rig',process=process,res='md')
        filePath = asset.path.output_hero().abs_path()
        fileName = asset.publish_name(hero=True)

        projectPath = os.environ['RFPROJECT']
        fileHeroPath = filePath.replace('$RFPROJECT',projectPath)
        path = '{}/{}'.format(fileHeroPath,fileName)

        mc.file(path, r=True , namespace='{}_md'.format(process) )
        return path

    def duplicateMthObj_cmd(self , *args):
        path = self.referenceRigHero_cmd(process = 'ctrl')

        jaw1Jnt = mc.ls('*:JawLwr1_Jnt')[0]
        jaw2Jnt = mc.ls('*:JawLwr2_Jnt')[0]
        headJnt = mc.ls('*:Head_Jnt')[0]
        prnt = mc.listRelatives(jaw1Jnt , p=True)[0]
        jawGrp = cn.createNode('transform' , 'mthJaw_Grp')
        jawGrp.snap(prnt)
        mc.delete(['mthJaw_Grp','mthHead_Jnt'])

        mthJaw1Jnt = cn.createNode('joint' , 'mthJawLwr1_Jnt')
        mthJaw2Jnt = cn.createNode('joint' , 'mthJawLwr2_Jnt')
        mthHeadJnt = cn.createNode('joint' , 'mthHead_Jnt')
        mthJaw1Jnt.snap(jaw1Jnt)
        mthJaw2Jnt.snap(jaw2Jnt)
        mthHeadJnt.snap(headJnt)

        for each in (mthJaw1Jnt , mthJaw2Jnt , mthHeadJnt):
            each.attr('radius').v = 0.05
            mc.makeIdentity(each , apply=True)

        mc.parent(mthJaw2Jnt , mthJaw1Jnt)
        mc.parent(mthJaw1Jnt , jawGrp)
        mc.parent(jawGrp , headJnt , 'Export_Grp')

        mthJaw1Jnt.attr('rotateOrder').v = 5
        mthJaw2Jnt.attr('rotateOrder').v = 5
        headJnt.attr('rotateOrder').v = 3

        mc.file(path, rr=True)

    def runBshRigDefault_cmd(self , *args):
        import bshTools as bt
        reload(bt)
        bt.runDefault()

    def runBshRigCharData_cmd(self , *args):
        import bshTools as bt
        reload(bt)
        bt.runFromData()

    def copyBshDefaultDataToCharData_cmd(self , *args):
        corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        tmpPath = '{}/rf_maya/rftool/rig/rigScript/template/bshRigData.yml'
        dataPath = self.getDataPath()
        dst = os.path.join(dataPath , 'bshRigData.yml')
        shutil.copy2(tmpPath , dst)

    def reBshRig_cmd(self , *args):
        import bshTools as bt
        reload(bt)
        data = bt.readData(local=True)
        rigList = data['bshRig']
        for eachRig in rigList:
            bt.runRig(eachRig)
        bt.connectBshLocAttrToBshNode(connection = True)

    def reBlendshape_cmd(self , *args):
        import bshTools as bt
        reload(bt)
        bt.reBlendshape()

    def connectBshRig_cmd(self , connect = True , *args):
        import bshTools as bt
        reload(bt)
        bt.connectBshLocAttrToBshNode(connection = connect)

    def getTargetGrpBshName_cmd(self , *args):
        obj = mc.ls(sl=True)[0]
        if '_Geo' in obj:
            target = mc.listRelatives(obj , p=True)[0]
        else:
            target = obj
        pm.textField('inbtwnTarget',e=True,tx='{}'.format(target))

    def addInbetweenBlendshape_cmd(self , *args):
        import bshTools as bt
        reload(bt)
        grpName = pm.textField('inbtwnName',q=True,tx=True)
        grpSide = pm.textField('inbtwnSide',q=True,tx=True)
        target = pm.textField('inbtwnTarget',q=True,tx=True)
        value = pm.floatField('inbtwnValue',q=True,v=True)

        bt.duplicateInbtwBlendShape(InbtwName   = '{}'.format(grpName),
                                    side        = '{}'.format(grpSide) ,
                                    target      = '{}'.format(target) ,
                                    value       = value )

    def addMoreBshGeo_cmd(self , *args):
        import bshTools as bt
        reload(bt)
        bt.blendShapNewGeo()

    def addBshGrp_cmd(self , *args):
        import bshTools as bt
        reload(bt)
        bshListName = pm.textField('bshList',q=True,tx=True)
        grpName = pm.textField('setName',q=True,tx=True)
        side = pm.textField('setSide',q=True,tx=True)

        bshName = bshListName.split(',')
        bshList = []
        for each in bshName:
            bshList.append(each)

        bt.duplicateBlendShape( bshList     = bshList, 
                                grpName     = grpName , 
                                side        = side )

    def addBshAddRig_cmd(self , *args):
        import bshTools as bt
        reload(bt)
        bshRigName = pm.textField('bshRigName',q=True,tx=True)
        bshRigAttr = pm.textField('bshRigAttr',q=True,tx=True)
        source = pm.textField('source',q=True,tx=True)
        targetPos = pm.textField('targetPos',q=True,tx=True)
        targetNeg = pm.textField('targetNeg',q=True,tx=True)


        targetDict = {  'pos' : '{}'.format(targetPos) ,
                        'neg' : '{}'.format(targetNeg) ,
                        'limitPos' : None ,
                        'limitNeg' : None }

        transformDict = {   'type' : '' ,
                            'max' : 0.5 ,
                            'min' : -0.5 ,
                            'jnt' : False}

        data = {'name' : bshRigName ,
                'attr' : bshRigAttr ,
                'source' : '{}'.format(source) ,
                'target' : targetDict,
                'transform' : transformDict }

        bt.connectBshLocAttrToBshNode(connection = False)
        bt.runRig(data)
        bt.connectBshLocAttrToBshNode(connection = True)

    def writeBshRigData_cmd(self , *args):
        import bshTools as bt
        reload(bt)
        bt.writeData()

    def createMainGrp_cmd(self, * args):
        import fclRigTools as fct
        reload(fct)
        fct.createMainGrp() 

    def parentFclRig_cmd(self,part = '', *args):
        import fclRigTools as fct
        reload(fct)
        fct.parentFcl(part = part)

    def assemblerFclRigUi_cmd(self, * args):
        import assemblerFclRigUi as assemUi
        reload(assemUi)
        assemUi = assemUi.Run()
        assemUi.show()

    def getFurGuideName_cmd(self , *args):
        obj = mc.ls(sl=True)[0]
        pm.textField('furGuideName',e=True,tx='{}'.format(obj))

    def addFurGuide_cmd(self, *args):
        import connectFurGuide as conFG
        reload(conFG)
        furGeo = pm.textField ( 'furGuideName' , q  = True , text = True  )
        print furGeo
        conFG.Run(bodyFurGeo = furGeo)

    #-------------------------------------------------------------------------------------
    #------------------------------------- Rig Tools -------------------------------------
    #-------------------------------------------------------------------------------------

    def writeWeight_cmd(self , *args):
        import weightTools as wt 
        reload(wt)
        wt.writeSelectedWeight()

    def readWeight_cmd(self , *args):
        import weightTools as wt 
        reload(wt)
        wt.readSelectedWeight()

    def replacedWeightUi_cmd(self , *args):
        import replacedWeightUi
        reload(replacedWeightUi)
        rwUI = replacedWeightUi.Run()
        rwUI.show()

    def paintSkinWeight_cmd(self , *args):
        mm.eval("ArtPaintSkinWeightsTool")

    def paintSkinWeightAvg_cmd(self , *args):
        from rigScript.plugin import averageVertexSkinWeightBrush as AVSWBrush
        reload(AVSWBrush)
        try:
            AVSWBrush.paint()
        except:pass

    def copySelectedWeight_cmd(self , *args):
        import weightTools as wt 
        reload(wt)
        wt.copySelectedWeight()

    def writeCtrlShape_cmd(self , *args):
        import ctrlData
        reload(ctrlData)
        ctrlData.write_ctrl()

    def readCtrlShape_cmd(self , *args):
        import ctrlData
        reload(ctrlData)
        ctrlData.read_ctrl()

    def createJointOnPosition_cmd(self , *args):
        rt.createJointOnPosition(obj='')

    def selectJnt_cmd(self , *args):
        mc.select(mc.ls(type='joint'))

    def parentHierachy_cmd(self , *args):
        obj = mc.ls(sl=True)
        for i in range(1,len(obj)):
            mc.parent(obj[i] , obj[i-1])

    def createZroGroup_cmd(self , *args):
        sel = mc.ls(sl=True)
        for each in sel:
            rt.addGrp(each)

    def importSelectRefFile_cmd(self , *args):
        rt.editSelectedReference(opr = 'import')

    def removeSelectRefFile_cmd(self , *args):
        rt.editSelectedReference(opr = 'remove')

    def displayLocalAxis_cmd(self , *args):
        mm.eval("ToggleLocalRotationAxes")

    def deleteDisplayLocalAxis_cmd(self , *args):
        nodes = mc.ls( type='transform' ) + mc.ls( type='joint' )
        for node in nodes :
            if mc.getAttr( '{}.displayLocalAxis'.format(node) ) :
                mc.setAttr( '{}.displayLocalAxis'.format(node) , 0 )

    def deleteGroup_cmd(self , *args):
        sels = mc.ls(sl=True)
        delGrp = 'Delete_Grp'
        if not mc.objExists(delGrp):
            delGrp =mc.createNode('transform' , n = delGrp)
        if sels:
            mc.parent(sels , delGrp)

    def snapPoint_cmd(self , *args):
        sels = mc.ls(sl=True)
        for i in range(1,len(sels)):
            mc.delete(mc.pointConstraint(sels[0] , sels[i] , mo=False))

    def snapParent_cmd(self , *args):
        sels = mc.ls(sl=True)
        for i in range(1,len(sels)):
            mc.delete(mc.parentConstraint(sels[0] , sels[i] , mo=False))

    def snapOrient_cmd(self , *args):
        sels = mc.ls(sl=True)
        for i in range(1,len(sels)):
            mc.delete(mc.orientConstraint(sels[0] , sels[i] , mo=False))

    def createMainRig_cmd(self , *args):
        import mainRig
        reload(mainRig)
        mainRig.Run()

    def createMainRigWithRoot_cmd(self , *args):
        import mainRig
        reload(mainRig)
        import fkRig
        reload(fkRig)

        geoGrpObj = mc.rename('Geo_Grp' , 'Geo_GrpObj')

        main = mainRig.Run()
        root = cn.createNode('joint','Root_TmpJnt')

        root_obj = fkRig.Run(name      = 'Root' ,
                            tmpJnt     = root ,
                            ctrlGrp    = main.mainCtrlGrp , 
                            shape      = 'circle' ,
                            jnt_ctrl   = 1 ,
                            color      = 'green',
                            localWorld = 0,
                            constraint = 0,
                            jnt = 0,
                            dtl = 0)

        for obj in ( root_obj.ctrl , root_obj.gmbl ) :
            obj.scaleShape( 2.3 )

        root_obj.ctrl.setRotateOrder( 'xzy' )
        root_obj.ctrl.addAttr('geoVis' , 0 , 1)
        root_obj.ctrl.attr('geoVis').v = 1
        
        obj = mc.listRelatives(geoGrpObj , c=True)[0]
        mc.parent(obj , 'Geo_Grp')
        mc.delete(geoGrpObj , root)

        self.connectCtrlGmbl_cmd(ctrl = root_obj.ctrl.name , obj = obj)

        for obj in ( root_obj.ctrlGrp , root_obj.zro ,root_obj.ofst ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        mc.select( cl = True )

    def parScaConstraint_cmd(self , *args):
        objs = mc.ls(sl=True)
        for i in range(1,len(objs)):
            mc.parentConstraint(objs[0] , objs[i] , mo=True)
            mc.scaleConstraint(objs[0] , objs[i] , mo=True)

    def connectCtrlGmbl_cmd(self , ctrl = '' , obj = '' , *args):
        if not ctrl:
            ctrl = mc.ls(sl=True)[0]
            obj = mc.ls(sl=True)[1]
        if not '_Grp' in obj:
            obj = mc.listRelatives(obj , p=True)[0]

        gmbl = ''
        if not 'Gmbl' in ctrl:
            elem = ctrl.split('_')
            gmbl = '{}Gmbl_{}'.format(elem[0] , ('_').join(elem[1:]))

        if mc.objExists(gmbl):
            mc.parentConstraint(gmbl , obj , mo=True)
            mc.scaleConstraint(gmbl , obj , mo=True)
        else:
            mc.parentConstraint(ctrl , obj , mo=True)
            mc.scaleConstraint(ctrl , obj , mo=True)

        if mc.objExists(ctrl+'.GeoVis'):
            mc.connectAttr(ctrl+'.GeoVis' , obj+'.v' , f=True)
        elif mc.objExists(ctrl+'.geoVis'):
            mc.connectAttr(ctrl+'.geoVis' , obj+'.v' , f=True)

    def createExportGrp_cmd(self , *args):
        expGrp = 'Export_Grp'
        delGrp = 'Delete_Grp'
        allItem = mc.ls(tr=True,o=True)
        check = mc.objExists( expGrp )
        if check :
            #check export group in other group
            prnt = mc.listRelatives(expGrp,ap=True)
            if prnt:
                mc.error('Found export group in other group.')
                return
        else:
            mc.createNode( 'transform' , n = 'Export_Grp' )

        delGrp = [expGrp,delGrp,'persp','top','front','side','bottom']
        for each in allItem:
            prnt = mc.listRelatives(each,p=True)
            chd = mc.listRelatives(each,c=True,type='camera')
            if prnt or chd:
                delGrp.append(each)

        #remove delGrp from allItem
        for each in delGrp:
            if each in allItem:
                allItem.remove(each)

        mc.parent(allItem,expGrp)


    def queryConnectConstraint_cmd(self , *args):
        cons = pm.listRelatives('Geo_Grp' , ad=True , type='constraint')
        trans = pm.listRelatives('Geo_Grp' , ad=True , type='transform')

        delGrp = 'Delete_Grp'
        dataConsGrp = 'DataConstraint_Grp'
        dataVisGrp = 'DataVisibility_Grp'

        if not mc.objExists(delGrp):
            delGrp = cn.createNode('transform' , delGrp)

        if mc.objExists(dataConsGrp):
            mc.delete(dataConsGrp)
        dataConsGrp = cn.createNode('transform' , dataConsGrp)
        mc.parent(dataConsGrp , delGrp)

        if mc.objExists(dataVisGrp):
            mc.delete(dataVisGrp)
        dataVisGrp = cn.createNode('transform' , dataVisGrp)
        mc.parent(dataVisGrp , delGrp)

        for con in cons:
            obj = con.getParent()
            src = con.getTargetList()[0]
            typeCon = con.nodeType()

            if not mc.objExists('{}.{}_{}'.format(dataConsGrp.name , src , typeCon)):
                dataConsGrp.addTag(attrName = '{}_{}'.format(src , typeCon) , str = obj)
            else:
                val = dataConsGrp.attr('{}_{}'.format(src , typeCon)).getVal()
                if not '{}'.format(obj) in val:
                    if mc.objExists(val):
                        dataConsGrp.attr('{}_{}'.format(src , typeCon)).v = '{},{}'.format(val,obj)
                    else:
                        dataConsGrp.attr('{}_{}'.format(src , typeCon)).v = '{}'.format(obj)

        for tran in trans:
            srcCtrlVis = mc.listConnections(tran+'.v',s=True,d=False,p=True)
            if srcCtrlVis:
                if not mc.objExists('{}.{}_vis'.format(dataVisGrp.name , tran)):
                    dataVisGrp.addTag(attrName = '{}_vis'.format(tran) , str = srcCtrlVis[0])
                else:
                    val = dataVisGrp.attr('{}_vis'.format(tran)).getVal()
                    if not '{}'.format(srcCtrlVis[0]) in val:
                        if mc.objExists(val):
                            dataVisGrp.attr('{}_vis'.format(tran)).v = '{},{}'.format(val,srcCtrlVis)[0]
                        else:
                            dataVisGrp.attr('{}_vis'.format(tran)).v = '{}'.format(srcCtrlVis[0])


    def reConnectConstraint_cmd(self , *args):
        dataGrp = 'DataConstraint_Grp'
        dataVisGrp = 'DataVisibility_Grp'
        if not dataGrp:
            print "This scene doesen't have DataConstraint_Grp."
            return

        dataGrp = core.Dag(dataGrp)
        attrs = mc.listAttr(dataGrp)
        for attr in attrs:
            if 'Constraint' in attr:
                elem = attr.split('_')
                typeCon = elem[-1]
                src = '_'.join(elem[0:-1])
                objs = (dataGrp.attr(attr).getVal()).split(',')
                if typeCon == 'parentConstraint':
                    for obj in objs:
                        try:
                            mc.parentConstraint(src , obj ,mo=True)
                        except:
                            print 'Can not re-parentConstraint, Lost object - {} or {}.'.format(src , obj)
                elif typeCon == 'scaleConstraint':
                    for obj in objs:
                        try:
                            mc.scaleConstraint(src , obj ,mo=True)
                        except:
                            print 'Can not re-scaleConstraint, Lost object - {} or {}.'.format(src , obj)

        dataVisGrp = core.Dag(dataVisGrp)
        attrs = mc.listAttr(dataVisGrp)
        for attr in attrs:
            if '_vis' in attr:
                src = dataVisGrp.attr(attr).getVal()
                des = attr.split('_vis')[0]
                try:
                    mc.connectAttr(src , des+'.v')
                except:
                    print '{} are not exists or {} already have connection.'.format(src , des)

    def connectTRS_cmd(self , *args):
        obj = mc.ls(sl=True)[0]
        targets = mc.ls(sl=True)[1:]
        for target in targets:
            rt.connectTRS(obj ,target)

    def connectTranslate_cmd(self , *args):
        obj = mc.ls(sl=True)[0]
        targets = mc.ls(sl=True)[1:]
        for target in targets:
            rt.connectTRS(obj ,target , rotate = False , scale = False)

    def connectRotate_cmd(self , *args):
        obj = mc.ls(sl=True)[0]
        targets = mc.ls(sl=True)[1:]
        for target in targets:
            rt.connectTRS(obj ,target , translate = False , scale = False)

    def connectScale_cmd(self , *args):
        obj = mc.ls(sl=True)[0]
        targets = mc.ls(sl=True)[1:]
        for target in targets:
            rt.connectTRS(obj ,target , translate = False , rotate = False)

    def connectMirrorObj_cmd(self , *args):
        objA = core.Dag(mc.ls(sl=True)[0])
        if objA.elem['side'] == '_L_':
            sideMirror = '_R_'
        else:
            sideMirror = '_L_'
        objB = core.Dag('{}{}{}'.format(objA.elem['name'] , sideMirror , objA.elem['type']))

        tslMdv = cn.createNode('multiplyDivide' , '{}MirrorTsl{}Mdv'.format(objA.elem['name'] , sideMirror ))
        rttMdv = cn.createNode('multiplyDivide' , '{}MirrorRtt{}Mdv'.format(objA.elem['name'] , sideMirror ))
        scaMdv = cn.createNode('multiplyDivide' , '{}MirrorSca{}Mdv'.format(objA.elem['name'] , sideMirror ))
        tslMdv.attr('i2x').v = -1
        rttMdv.attr('i2y').v = -1
        rttMdv.attr('i2z').v = -1
        scaMdv.attr('i2x').v = -1

        objA.attr('tx') >> tslMdv.attr('i1x')
        objA.attr('rx') >> rttMdv.attr('i1y')
        objA.attr('rz') >> rttMdv.attr('i1z')
        objA.attr('sx') >> scaMdv.attr('i1x')
        tslMdv.attr('ox') >> objB.attr('tx')
        objA.attr('ty') >> objB.attr('ty')
        objA.attr('tz') >> objB.attr('tz')
        objA.attr('rx') >> objB.attr('rx')
        rttMdv.attr('oy') >> objB.attr('ry')
        rttMdv.attr('oz') >> objB.attr('rz')
        scaMdv.attr('ox') >> objB.attr('sx')
        objA.attr('sy') >> objB.attr('sy')
        objA.attr('sz') >> objB.attr('sz')

    def duplicateObj(self , *args):
        sel = mc.ls(sl=True)
        for each in sel:
            obj = mc.duplicate(each)[0]
            obj = mc.rename(obj , each+'_Proxy')
            obj = core.Dag(obj)
            mc.parent(obj,w=True)
            for attr in ('tx','ty','tz','rx','ry','rz','sx','sy','sz','v'):
                obj.attr(attr).l = False

    def copyAttrs_cmd(self , *args):
        sels = mc.ls(sl=True)
        objA = core.Dag(sels[0])
        objB = core.Dag(sels[1])

        listAttrs = objA.listAttr()

        for attr in listAttrs:
            if not objA.attr(attr).getLock():
                value = objA.attr(attr).getVal()
                objB.attr(attr).v = value

        if objA.getNodeType() == 'transform' or objA.getNodeType() == 'joint':
            objAShape = core.Dag(objA.shape)
            objBShape = core.Dag(objB.shape)
            listShapeAttrs = objAShape.listAttr()

            for attr in listShapeAttrs:
                if not objAShape.attr(attr).getLock():
                    value = objAShape.attr(attr).getVal()
                    objBShape.attr(attr).v = value

        print 'Copy attribute {} from {} to {} .'.format(attr , objAShape.name , objBShape.name)

    def writeAttr_cmd(self , *args):
        import attrTools as at
        reload(at)
        at.writeAttr()

    def readAttr_cmd(self , *args):
        import attrTools as at
        reload(at)
        at.readAttr()

    def readSelectedAttr_cmd(self , *args):
        import attrTools as at
        reload(at)
        at.readAttrSelected()

    def keyOffsetUi_cmd(self , *args):
        # mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/plugin/keyOffset.mel"')
        corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        path = '{}/rf_maya/rftool/rig/rigScript/plugin/keyOffset.mel'.format(corePath)
        mm.eval('source "'+path+'"')
        mm.eval("keyOffset()")

    def cometRenameUi_cmd(self , *args):
        # mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/plugin/cometRename.mel"')
        corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        path = '{}/rf_maya/rftool/rig/rigScript/plugin/cometRename.mel'.format(corePath)
        mm.eval('source "'+path+'"')
        mm.eval("cometRename()")

    def mirrorCtrlShape_cmd(self , *args):
        ctrls = mc.ls(sl=True)
        shapeSrc = []
        shapeDes = []
        for ctrl in ctrls:
            shapes = mc.listRelatives(ctrl , c=True , type='nurbsCurve')
            for shape in shapes:
                shape = core.Dag(shape)
                side = shape.getSide()
                if side:
                    shapeSrc.append(shape)
                if side == 'L':
                    opsShape = core.Dag('{}_R_{}'.format(shape.elem['name'] , shape.elem['type']))
                    shapeDes.append(opsShape)
                elif side == 'R':
                    opsShape = core.Dag('{}_L_{}'.format(shape.elem['name'] , shape.elem['type']))
                    shapeDes.append(opsShape)
                    
        for i in range(len(shapeSrc)):
            src = shapeSrc[i]
            des = shapeDes[i]
            crvCvs = src.attr('spans').getVal() + src.attr('degree').getVal()
            for j in range(crvCvs):
                vct = mc.xform('{}.cv[{}]'.format(src.name , j) , q=True , t=True , ws=True)
                mc.xform('{}.cv[{}]'.format(des.name , j) , ws=True , t=(-vct[0] , vct[1] , vct[2]))
                    
    def setGimbalSize_cmd(self , *args):
        rt.clear_scale_gimbal_control()

    def checkModelSymUi_cmd(self , *args):
        # mm.eval('source "O:/Pipeline/core/rf_maya/rftool/rig/plugin/abSymMesh.mel"')
        corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        path = '{}/rf_maya/rftool/rig/rigScript/plugin/abSymMesh.mel'.format(corePath)
        mm.eval('source "'+path+'"')
        mm.eval("abSymMesh()")

    def checkModelTopologyUi_cmd(self , *args):
        from nuTools.util import geoReplacer
        reload(geoReplacer)
        geoReplacer = geoReplacer.GeoReplacer()
        geoReplacer.UI()
        
    def createCtrlUi_cmd(self , *args):
        import createCtrlUi
        reload(createCtrlUi)

        createCtrlUi = createCtrlUi.Run()
        createCtrlUi.show()

    def nameCheckUi_cmd(self , *args):
        from rftool.model import name_checker
        reload(name_checker)
        name_checker.show()

    def generateJntAndAxisUi_cmd(self , *args):
        import generateJntAndAxisUi as gntJntUi
        reload(gntJntUi)

        gntJntUi = gntJntUi.Run()
        gntJntUi.show()

    def cutProxyUi_cmd(self , *args):
        from nuTools.util.proxyRigGenerator import app as prgapp
        reload(prgapp)
        prgapp.show()

    def stringRigUi_cmd(self , *args):
        import stringRigUi
        reload(stringRigUi)

        stgUi = stringRigUi.Run()
        stgUi.show()

    def addDeformerUi_cmd(self , *args):
        import addDeformerMemberUi as dfmUi
        reload(dfmUi)
        dfmUi = dfmUi.Run()
        dfmUi.show()

    def ribbonRigUi_cmd(self , *args):
        import ribbonRigUi
        reload(ribbonRigUi)
        rbnUi = ribbonRigUi.Run()
        rbnUi.show()

    def blendShapeObj_cmd(self , *args):
        sel = mc.ls(sl=True)
        base = sel[0]
        child = sel[-1]
        rt.blendShape(bshGeo = base , target = child)

    def convertConstraintToMatrix_cmd(self , *args):
        from nuTools.util import matrixConstraint as mtcon
        reload(mtcon)
        sel = pm.ls(sl=True , type='transform')
        if sel:
            for each in sel:
                if not '_Grp' in '{}'.format(each):
                    sel.remove(each)

        if not sel:
            obj = pm.ls(type='transform')
        else:
            obj = sel

        # convert constraints into matrix constraint
        old_constraints = mtcon.convertToMatrixConstraint(objs=obj)

        # delete old constraints
        pm.delete(old_constraints) 

    def openTaskManager_cmd(self , *args):
        from rf_app.task_manager import app
        reload(app)
        app.show()

    def openShotgunManager_cmd(self , *args):
        from rf_app.sg_manager import app
        reload(app)
        myApp = app.show()

    def openPublish_cmd(self , *args):
        from rf_app.publish.asset import asset_app
        reload(asset_app)
        app = asset_app.show()

    def openWorkPathInExplorer_cmd(self , *args):
        context = context_info.ContextPathInfo()
        projectPath = os.environ['RFPROJECT']
        fileWorkPath = (context.path.name()).replace('$RFPROJECT',projectPath)
        os.startfile(fileWorkPath)

    def openDataPathInExplorer_cmd(self , *args):
        path = self.getDataPath()
        os.startfile(path)

    def saveIncrement_cmd(self , *args):
        from rf_app.save_plus import save_utils
        reload(save_utils)
        save_utils.save_increment()

    def reloadUi_cmd(self , *args):
        import rigToolsUi_v001 as rtUi
        reload(rtUi)
        rtUi = rtUi.Run()
        rtUi.show()
    
    def miarmyRig_cmd(self , *args):
        print 'a'

    def fxRigUi_cmd(self , *args):
        print 'a' 

    def keyAnim_cmd(self, *args):
        keyAnim.buildKeyFreameUI()
     

'''
mod = pm.getModifiers()
if mod == 1:
    print 'Shift'
    return
elif mod == 4:
    print 'Ctrl'
    return
elif mod == 8:
    print 'Alt'
    return
elif mod == 16:
    print 'Command/Windows'
    return

pm.button('KeyRotate',l='Key Rotate', width=winWidth*0.5 , c = 'KeyRotate_cmd()' , ann = 'Hi')

'''