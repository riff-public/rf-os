from rf_maya.rftool.rig.rigScript import core
from rf_maya.rftool.rig.rigScript import ctrlRig
from rf_maya.rftool.rig.rigScript import crvFunc
from rf_maya.rftool.rig.rigScript import weightTools
cn = core.Node()
import maya.cmds as mc
import maya.mel as mel
import pymel.core as pm
import re
from utaTools.utapy import utaCore
reload(utaCore)
import json
import math
from rf_maya.rftool.shade import shade_utils
reload(shade_utils)
from rf_utils.context import context_info
reload(context_info)
import os
from rf_maya.rftool.rig.rigScript import ctrlRig
reload(ctrlRig)
from rf_maya.rftool.rig.rigScript import crvFunc
reload(crvFunc)
def caTmpJntSelected():
    sel = mc.ls(sl=True)
    jnt_list = []
    for i in sel:
        mc.select(i)
        clus = mc.cluster()
        jnt = mc.createNode('joint',n = i.replace('_Geo','_TmpJnt'))
        mc.delete(mc.parentConstraint(clus[1],jnt,mo=0))
        mc.delete(clus)
        jnt_list.append(jnt)
    return jnt_list

def caCtrl(obj,ctrlShape = 'circle',color = 'green'):
    splitList = obj.split('_')
    if len(splitList) ==3:
        name = splitList[0]
        side = splitList[1]
    else:
        name = splitList[0]
        side= ''    
    ctrl = ctrlRig.Run(name       = name ,
                         tmpJnt     = obj ,
                         side       = side ,
                         shape      = ctrlShape ,
                         jnt_ctrl   = 0,
                         jnt        = 0,
                         gimbal     = 0 ,
                         size       = 1 ,
                         drv        = 0,
                         inv        = 0,
                         color      = color,
                         constraint = 0,
                         vis = 1,)
    return ctrl

def caWrapJoint(obj, extra = '',parent = '',no_suffixGrp = 0):
    ori = core.PyNode(obj)
    prefix = ori.getName()
    suffix = ori.elem['type']
    side = ori.elem['side']
    jnt = cn.joint('{}{}{}Jnt'.format(prefix,extra, side),obj)
    if  no_suffixGrp:
        grp = cn.transform('{}{}Zro{}Grp'.format(prefix,extra, side))
    else:
        grp = cn.transform('{}{}{}Zro{}Grp'.format(prefix,suffix,extra, side))
    drvGrp = cn.transform('{}Drv{}Zro{}Grp'.format(prefix,extra, side))
    drvGrp.parent(grp)
    grp.snapPoint(jnt)
    jnt.parent(drvGrp)
    jnt.freeze()
    if parent:
        grp.parent(parent)
    return(jnt,grp)
    
    
def caDefCtrl(obj,ctrlShape = 'circle'):
    splitList = obj.split('_')
    if len(splitList) ==3:
        name = splitList[0]
        side = splitList[1]
    else:
        name = splitList[0]
        side= ''    
    ctrl = ctrlRig.Run(name       = name ,
                         tmpJnt     = obj ,
                         side       = side ,
                         shape      = ctrlShape ,
                         jnt_ctrl   = 0,
                         jnt        = 1,
                         gimbal     = 0 ,
                         size       = 1 ,
                         drv        = 0,
                         inv        = 0,
                         color      = 'red',
                         constraint = 0,
                         vis = 1,)
    #zro = ctrl.jnt.group()
    cn.group(ctrl.jnt.name)
    for t in 'trs':
        for ax in 'xyz':
            mc.connectAttr('{}.{}{}'.format(ctrl.ctrl.name,t,ax),'{}.{}{}'.format(ctrl.jnt.name,t,ax))
    
    return ctrl
    
def connectWrapCtrl(jnt = '',grp= '' ,ctrl = ''):
    splitList = ctrl.split('_')
    if len(splitList) ==3:
        name = splitList[0]
        side = '_{}_'.format(splitList[1])
    else:
        name = splitList[0]
        side= '_'
    mdv = mc.createNode('multiplyDivide',n='{}{}Mdv'.format(name,side))
    mc.connectAttr(ctrl+'.tx',mdv+'.input1X')
    mc.connectAttr(ctrl+'.ty',mdv+'.input1Y')
    mc.setAttr(mdv+'.input2X',3)
    mc.setAttr(mdv+'.input2Y',-3)
    mc.connectAttr(mdv+'.outputX',grp+'.ry')
    mc.connectAttr(mdv+'.outputY',grp+'.rx')
    mc.connectAttr(ctrl+'.rz',jnt+'.rz')
    for ax in 'xyz':
        mc.connectAttr(ctrl+'.s{}'.format(ax),jnt+'.s{}'.format(ax))


def createFollicleOnMesh(obj_list = [] , meshes=''):
    meshes = mc.listRelatives(sel[-1],s=1)[0]
    cpom = mc.createNode('closestPointOnMesh')
    mc.connectAttr('{}.worldMesh'.format(meshes),'{}.im'.format(cpom))
    fol_list = []
    for i in obj_list:
        pos = mc.xform(i,ws=1,t=1,q=1)
        mc.setAttr('{}.ip'.format(cpom),pos[0],pos[1],pos[2],type = 'double3')
        paramU = mc.getAttr('{}.u'.format(cpom))
        paramV = mc.getAttr('{}.v'.format(cpom))
        splitList = i.split('_')
        if len(splitList) ==3:
            name = splitList[0]
            side = '_{}_'.format(splitList[1])
        else:
            name = splitList[0]
            side = '_'
        folShape = mc.createNode('follicle')
        fol = mc.listRelatives(folShape,p=1)[0]
        fol = mc.rename(fol,'{}{}Fol'.format(name,side))
        folShape = mc.listRelatives(fol,s=1)[0]
        mc.connectAttr('{}.worldMesh'.format(meshes),'{}.inputMesh'.format(folShape))
        mc.connectAttr('{}.outTranslate'.format(folShape),'{}.t'.format(fol))
        mc.connectAttr('{}.outRotate'.format(folShape),'{}.r'.format(fol))
        mc.setAttr(folShape+'.parameterU',paramU)
        mc.setAttr(folShape+'.parameterV',paramV)
        fol_list.append(fol)
    mc.delete(cpom)
    return fol_list
    
def createFclWrapCtrlSet(obj,ctrlGrp ='SwRigCtrl_Grp',skinGrp = 'SwRigSkin_Grp'):
    ctrl = caCtrl(obj)
    wrap = obj.replace('Fcl','FclWrap')
    jnt,grp = caWrapJoint(wrap)
    connectWrapCtrl(jnt.name,grp.name,ctrl.ctrl.name)
    ctrl.zro.parent(ctrlGrp)
    grp.parent(skinGrp)
    
def caShrinkWrapJnt(swTo = 'HeadSwRig_Geo', faceFclRigGeoGrp = 'FacePlaneSwRigGeo_Grp', skinGrp = 'SwRigSkin_Grp'):
    if skinGrp:
        if not mc.objExists(skinGrp):
            grp = utaCore.createGrp(lists= [skinGrp])
            skinGrp = grp[0]

    sels = utaCore.listGeo(sels = faceFclRigGeoGrp, nameing = '_Geo', namePass = '_Grp')

    for objName in sels:
        obj = objName.split('|')[-1]
        split = obj.split('_')
        if len(split) == 3:
            side = split[1]
        else:
            side = ''
        mc.select(obj)
        tmpJnt = caTmpJntSelected()[0]
        jnt,grp = caWrapJoint(tmpJnt,no_suffixGrp=1)
        dtlJnt,dtlGrp = caWrapJoint(jnt.name,'Dtl',jnt.name)
        if side == 'R':
            mc.setAttr(grp.name +'.ry',180)
            mc.setAttr(grp.name +'.sz',-1)

        mc.select([dtlJnt,obj])
        mc.skinCluster(tsb=1)
        mc.delete(tmpJnt)
        grp.parent(skinGrp)
        mc.select(swTo)
        sw = mc.deformer(obj,type= 'shrinkWrap',n = obj.replace('_Geo','_Sw'))[0]
        num = mc.getAttr(swTo+'.worldMesh',s=1)
        mc.connectAttr('{}.worldMesh[{}]'.format(swTo,num-1),sw+'.targetGeom',f=1)
        mc.setAttr(sw+'.projection',4)
        mc.setAttr(sw+'.targetSmoothLevel',2)
        mc.addAttr(jnt.name,ln='wrap',dv=1,max=1,min=0)
        mc.connectAttr(jnt.name+'.wrap',sw+'.envelope',f=1)
        mc.addAttr(jnt.name,ln='offset',dv=.02)
        mc.connectAttr(jnt.name+'.offset',sw+'.offset',f=1)

def caSwShadowParent(swSkinGrp = 'SwRigSkin_Grp'):
    jntContent = mc.listRelatives(swSkinGrp)
    for grp in jntContent:
        if 'Shadow' in grp:
            mc.parent(grp,grp.replace('Shadow','').replace('Zro','Dtl').replace('_Grp','_Jnt'))

def caOverallJnt(swSkinGrp = 'SwRigSkin_Grp'):
    lineGrp = 'GuideLineSwRigGeo_Grp'
    lineXGeo = 'GuildeLineXSwRig_Geo'
    lineYGeo = 'GuildeLineYSwRig_Geo'
    lineXGrp = 'GuildeLineXSwRigGeo_Grp'
    lineYGrp = 'GuildeLineYSwRigGeo_Grp'

    guide_list = [lineGrp,lineXGeo,lineYGeo,lineXGrp,lineYGrp]
    for obj in guide_list:
        mc.xform(obj,cp=1)
    jntContent = mc.listRelatives(swSkinGrp)
    tmpJnt = cn.joint('OverallPlaneSwRig_TmpJnt','GuideLineSwRigGeo_Grp')
    jnt,grp = caWrapJoint(tmpJnt,no_suffixGrp=1)
    mc.parent(grp.name,swSkinGrp)
    mc.parent(jntContent,jnt)
    mc.delete(tmpJnt)

def caShrinkWrapLine(swTo = 'HeadSwRig_Geo', ctrlGrp = 'SwRigCtrl_Grp'):
    if ctrlGrp:
        if not mc.objExists(ctrlGrp):
            grp = utaCore.createGrp(lists= [ctrlGrp])
            ctrlGrp = grp[0]

    #sels = utaCore.listGeo(sels = faceFclRigGeoGrp, nameing = '_Geo', namePass = '_Grp')
    lineXGeo = 'GuildeLineXSwRig_Geo'
    lineYGeo = 'GuildeLineYSwRig_Geo'
    lineXGrp = 'GuildeLineXSwRigGeo_Grp'
    lineYGrp = 'GuildeLineYSwRigGeo_Grp'

    guideCtrl = ctrlRig.Run(name       = 'GuideLine' ,
                         tmpJnt     = lineXGeo ,
                         side       = '' ,
                         shape      = 'circle' ,
                         jnt_ctrl   = 0,
                         jnt        = 0,
                         gimbal     = 0 ,
                         size       = .5 ,
                         drv        = 0,
                         inv        = 0,
                         color      = 'yellow',
                         constraint = 0,
                         vis = 1,)
    for obj in [lineXGeo,lineYGeo]:
        sw = mc.deformer(obj,type= 'shrinkWrap',n = obj.replace('_Geo','_Sw'))[0]
        num = mc.getAttr(swTo+'.worldMesh',s=1)
        mc.connectAttr('{}.worldMesh[{}]'.format(swTo,num-1),sw+'.targetGeom',f=1)
        mc.setAttr(sw+'.projection',4)
        mc.setAttr(sw+'.targetSmoothLevel',2)
        mc.setAttr(sw+'.offset',0.09)

    for obj in [lineXGrp,lineYGrp]:
        for t in 'tr':
            for ax in 'xyz':
                mc.connectAttr(guideCtrl.ctrl.name+'.{}{}'.format(t,ax),obj+'.{}{}'.format(t,ax))

    mc.parent(guideCtrl.zro.name,ctrlGrp)    
    crvFunc.set_shape(guideCtrl.ctrl.name,'caGuideLine')

    return guideCtrl


    # for objName in sels:
    #     obj = objName.split('|')[-1]
    #     split = obj.split('_')
    #     if len(split) == 3:
    #         side = split[1]
    #     else:
    #         side = ''
    #     mc.select(obj)
    #     tmpJnt = caTmpJntSelected()[0]
    #     jnt,grp = caWrapJoint(tmpJnt)
    #     dtlJnt,dtlGrp = caWrapJoint(jnt.name,'Dtl',jnt.name)
    #     if side == 'R':
    #         mc.setAttr(grp.name +'.ry',180)

    #     mc.select([dtlJnt,obj])
    #     mc.skinCluster(tsb=1)
    #     mc.delete(tmpJnt)
    #     grp.parent(ctrlGrp)
    #     mc.select(swTo)
    #     sw = mc.deformer(obj,type= 'shrinkWrap',n = obj.replace('_Geo','_Sw'))[0]
    #     num = mc.getAttr(swTo+'.worldMesh',s=1)
    #     mc.connectAttr('{}.worldMesh[{}]'.format(swTo,num-1),sw+'.targetGeom',f=1)
    #     mc.setAttr(sw+'.projection',4)
    #     mc.setAttr(sw+'.targetSmoothLevel',2)
    #     mc.addAttr(jnt.name,ln='wrap',dv=1,max=1,min=0)
    #     mc.connectAttr(jnt.name+'.wrap',sw+'.envelope',f=1)
    #     mc.addAttr(jnt.name,ln='offset',dv=.02)
    #     mc.connectAttr(jnt.name+'.offset',sw+'.offset',f=1)


#############################################################################################################
def findMaxMinUV(geo = ''):
    mc.select(geo)
    mc.select(mc.polyListComponentConversion(tuv = True), r = True)
    upox = mc.ls(sl=True, fl=True)
    u_list = []
    v_list = []
    for i in upox:
        u = mc.polyEditUV(i, query = True)[0]
        v = mc.polyEditUV(i, query = True)[1]
        u_list.append(u)
        v_list.append(v)
    mc.select(d=1)
    return [min(u_list),max(u_list)],[min(v_list),max(v_list)]

def caStraightUV(geo_list):
    for i in geo_list:
        mc.select(i)
        mel.eval('texStraightenUVs "UV" 30;')
        mel.eval('uvTkDoNormalizeUV;')
        #mel.eval('texStraightenUVs "UV" 30;')

def divideUVPosition(geo,u_list,v_list,u_num=3,v_num=3):
    folShp = mc.createNode('follicle')
    fol = mc.listRelatives(folShp,p=1)[0]
    if mc.nodeType(geo) != 'mesh':
        geoShp = mc.listRelatives(geo,s=1)[0]
    else:
        geoShp = geo
    mc.connectAttr(geoShp+'.outMesh',folShp+'.inputMesh')
    mc.connectAttr(folShp+'.outTranslate',fol+'.t')
    if u_num != 1:
        u_step = abs(u_list[:][1]-u_list[:][0])/(u_num-1)
    else:
        u_step = abs(u_list[:][1]-u_list[:][0])/2
    if v_num != 1:  
        v_step = abs(v_list[:][1]-v_list[:][0])/(v_num-1)
    else:
        v_step = abs(v_list[:][1]-v_list[:][0])/2
    uv_list = []
    
    if u_num !=1:
        u_cur = math.ceil(u_list[:][0]*100)/100
    else:
        u_cur = u_list[:][0] + u_step

    for unum in range(u_num):
        if v_num != 1:
            v_cur = math.ceil(v_list[:][0]*100)/100
        else:
            v_cur = v_list[:][0]+v_step
        for vnum in range(v_num):
            uv_list.append([u_cur,v_cur])
            #print u_cur,v_cur
            v_cur += v_step
            if v_cur > v_list[:][1]:
                v_cur = math.floor(v_list[:][1]*100)/100
                #v_cur = v_list[:][1]

        u_cur += u_step
        if u_cur > u_list[:][1]:
            u_cur = math.floor(u_list[:][1]*100)/100
            #v_cur = v_list[:][1]

    t_list = []
    # print uv_list
    pm.PyNode(geo).uv
    allU_list = []
    allV_list = []
    # for i in range(mc.getAttr('{}Shape.uvpt'.format(geo),s=1)):
    #     uv = mc.getAttr('{}.uvpt[{}]'.format(geo,i))
    #     allU_list.append(uv[0][0])
    #     allV_list.append(uv[0][1])
    for uv in uv_list:
        #print closest_value(allU_list,uv[0]),closest_value(allV_list,uv[1])
        mc.setAttr(folShp +'.parameterU' ,uv[0])
        mc.setAttr(folShp+'.parameterV', uv[1])
        t_list.append(mc.getAttr(folShp+'.outTranslate'))
    jnt_list = []
    for i in t_list:
        #print i
        jnt = mc.createNode('joint')
        mc.xform(jnt,ws=1,t=i[0])
        jnt_list.append(jnt)
    mc.delete(fol) 
    return jnt_list
    
def closest_value(list_a, k):
    return list_a[min(range(len(list_a)),key = lambda i: abs(list_a[i]-k))]

def CATmpFromGeo(geo,u_num =3, v_num= 3,extra = ''):
    splitList = geo.split('_')
    if len(splitList) == 3:
        name = splitList[0].replace('DtlRig','')
        side = '_{}_'.format(splitList[1])
    else:
        name = splitList[0].replace('DtlRig','')
        side = '_'
    skinGeo = mc.duplicate(geo,rr=1,n = '{}{}Dtl{}Geo'.format(name,extra,side))[0]
    for t in 'trs':
        for ax in 'xyz':
            mc.setAttr('{}.{}{}'.format(skinGeo,t,ax),l=0)
    locGrp = mc.createNode('transform',n='{}{}TmpJnt{}Grp'.format(name,extra,side))
    # create plane for ctrl
    caStraightUV([skinGeo])

    # do skin set
    skinU,skinV = findMaxMinUV(skinGeo)
    sknPos_list = divideUVPosition(skinGeo,skinU,skinV,u_num=u_num ,v_num=v_num)
    jnt_list = []
    for num,i in enumerate(sknPos_list):
        jnt = mc.rename(i,'{}{}Dtl{}{}TmpJnt'.format(name,extra,num+1,side))
        jnt_list.append(jnt)
    mc.parent(jnt_list,locGrp)
    mc.delete(skinGeo)

# def createDtlFromGeo(geo,visCtrl,u_num =3, v_num= 3,skinGrp='',geoLoc=''):
#     splitList = geo.split('_')
#     if len(splitList) == 3:
#         name = splitList[0]
#         side = '_{}_'.format(splitList[1])
#     else:
#         name = splitList[0]
#         side = ''

#     dtlGeoGrp = 'DtlGeoGrp'.format(name)
#     if not mc.objExists(dtlGeoGrp):
#         dtlGeoGrp = mc.createNode('transform',n = dtlGeoGrp)
#     mc.delete(mc.parentConstraint(geoLoc,dtlGeoGrp,mo=0))

#     # create plane for skin
#     skinGeo = mc.duplicate(geo,rr=1,n = '{}Dtl{}Geo'.format(name,side))[0]
#     for t in 'trs':
#         for ax in 'xyz':
#             mc.setAttr('{}.{}{}'.format(skinGeo,t,ax),l=0)
#     mc.parent(skinGeo,dtlGeoGrp,r=0)
#     # create plane for ctrl
#     ctrlGeo = mc.duplicate(geo,rr=1)[0]
#     caStraightUV([skinGeo,ctrlGeo])
#     for t in 'trs':
#         for ax in 'xyz':
#             mc.setAttr('{}.{}{}'.format(ctrlGeo,t,ax),l=0)
#     mc.parent(ctrlGeo,w=1,r=0)
#     mc.setAttr(ctrlGeo+'.sz',0)
#     mc.makeIdentity(ctrlGeo,t=0,r=0,s=1,a=1)

#     # do skin set
#     skinU,skinV = findMaxMinUV(skinGeo)
#     sknPos_list = divideUVPosition(skinGeo,skinU,skinV,u_num=u_num ,v_num=v_num)
#     # create all jntGrp
#     ctrlU,ctrlV = findMaxMinUV(ctrlGeo)
#     jntAllGrp = mc.createNode('transform',n = '{}DtlJntAll{}Grp'.format(name,side))
#     jnt_list = []
#     for num,i in enumerate(sknPos_list):
#         jnt = mc.rename(i,'{}Dtl{}{}Jnt'.format(name,num+1,side))
#         jnt_list.append(jnt)
#         jntGrp = mc.createNode('transform',n = '{}{}DtlJnt{}Grp'.format(name,num+1,side))
#         ofstGrp = mc.createNode('transform',n = '{}{}OfstDtlJnt{}Grp'.format(name,num+1,side))
#         mc.parent(ofstGrp,jntGrp)
#         mc.delete(mc.parentConstraint(jnt,jntGrp,mo=0))
#         mc.parent(jnt,ofstGrp)
#         mc.parent(jntGrp,jntAllGrp)
#     mc.delete(mc.parentConstraint(geoLoc,jntAllGrp,mo=0))
#     # do ctrl set
#     ctrl_list = []
#     ctrlPos_list = divideUVPosition(ctrlGeo,ctrlU,ctrlV,u_num=u_num ,v_num=v_num)
#     ctrlAllGrp =mc.createNode('transform',n = '{}DtlCtrlAll{}Grp'.format(name,side))
#     for num,i in enumerate(ctrlPos_list):
#         ctrlJnt = mc.rename(i,'{}Dtl{}{}TmpJnt'.format(name,num+1,side))
#         ctrl = caCtrl(ctrlJnt)
#         ctrl_list.append(ctrl)
#         for t in 'trs':
#             for ax in 'xyz':
#                 mc.connectAttr('{}.{}{}'.format(ctrl.ctrl.name,t,ax),'{}.{}{}'.format(jnt_list[num],t,ax))
#         ctrl.zro.parent(ctrlAllGrp)
#         mc.delete(ctrlJnt)
#     mc.delete(ctrlGeo)
#     mc.parent(jntAllGrp , skinGrp)
#     # coonnect ctrl

def createDtlFromGeo(geo,u_num =3, v_num= 3,extra = '',crvShape = 'sphere',color='green'):
    splitList = geo.split('_')
    if len(splitList) == 3:
        name = splitList[0]
        side = '_{}_'.format(splitList[1])
    else:
        name = splitList[0]
        side = '_'

    # create plane for skin
    skinGeo = mc.duplicate(geo,rr=1,n = '{}{}Dtl{}Geo'.format(name,extra,side))[0]
    for t in 'trs':
        for ax in 'xyz':
            mc.setAttr('{}.{}{}'.format(skinGeo,t,ax),l=0)
    caStraightUV([skinGeo])

    # do skin set
    skinU,skinV = findMaxMinUV(skinGeo)
    sknPos_list = divideUVPosition(skinGeo,skinU,skinV,u_num=u_num ,v_num=v_num)
    # create all jntGrp
    ctrlU,ctrlV = findMaxMinUV(skinGeo)
    jntAllGrp = mc.createNode('transform',n = '{}{}DtlJntAll{}Grp'.format(name,extra,side))
    jnt_list = []
    jntGrp_list = []
    for num,i in enumerate(sknPos_list):
        jnt = mc.rename(i,'{}{}Dtl{}{}Jnt'.format(name,extra,num+1,side))
        jnt_list.append(jnt)
        jntGrp = mc.createNode('transform',n = '{}{}{}DtlJnt{}Grp'.format(name,extra,num+1,side))
        ofstGrp = mc.createNode('transform',n = '{}{}{}OfstDtlJnt{}Grp'.format(name,extra,num+1,side))
        jntGrp_list.append(jntGrp)
        mc.parent(ofstGrp,jntGrp)
        mc.delete(mc.parentConstraint(jnt,jntGrp,mo=0))
        mc.parent(jnt,ofstGrp)
        mc.parent(jntGrp,jntAllGrp)

    # do ctrl set
    ctrl_list = []
    ctrlPos_list = divideUVPosition(skinGeo,ctrlU,ctrlV,u_num=u_num ,v_num=v_num)
    ctrlAllGrp =mc.createNode('transform',n = '{}{}DtlCtrlAll{}Grp'.format(name,extra,side))
    for num,i in enumerate(ctrlPos_list):
        ctrlJnt = mc.rename(i,'{}{}Dtl{}{}TmpJnt'.format(name,extra,num+1,side))
        ctrl = caCtrl(ctrlJnt,crvShape,color)
        ctrl_list.append(ctrl)
        for t in 'trs':
            for ax in 'xyz':
                mc.connectAttr('{}.{}{}'.format(ctrl.ctrl.name,t,ax),'{}.{}{}'.format(jnt_list[num],t,ax))
        ctrl.zro.parent(ctrlAllGrp)
        mc.delete(ctrlJnt)
    mc.delete(skinGeo)
    return (ctrl_list,jnt_list,ctrlAllGrp,jntAllGrp,jntGrp_list)
    # coonnect ctrl

def caDtlSet(geo,u_num=3,v_num=3):

    name, side, lastName = cn.splitName(sels = [geo])

    ctrl_list,jnt_list,ctrlAllGrp,jntAllGrp,jntGrp_list = createDtlFromGeo(geo,u_num,v_num,'','sphere','green')
    p_list = []
    for jnt in jnt_list:
        p_list.append(mc.xform(jnt,q=1,t=1,ws=1))
    posCrv = mc.curve(d=1,p=p_list)
    udCrv = mc.curve(d=1,p=p_list)
    lrCrv = mc.curve(d=1,p=p_list)
    wrapCrv = mc.curve(d=1,p=p_list)
    
    posCrv = mc.rename(posCrv, '{}Pos{}Crv'.format(name,side))
    udCrv = mc.rename(udCrv, '{}UpDw{}Crv'.format(name,side))
    lrCrv =mc.rename(lrCrv, '{}Side{}Crv'.format(name,side))
    wrapCrv =mc.rename(wrapCrv, '{}Wrap{}Crv'.format(name,side))
    
    crvGrp = mc.createNode('transform',n='{}Crv{}Grp'.format(name,side))
    mc.parent(posCrv,udCrv,lrCrv,wrapCrv,crvGrp)

    for crv in posCrv,udCrv,lrCrv:
        mc.rebuildCurve (crv,ch =1 ,rpo =1 ,rt= 0 ,end =1, kr= 0, kcp= 1 ,kep= 1 ,kt= 0 ,s= 4, d =1, tol =0.01 )
    udCtrl_list,udJnt_list,udCtrlGrp,udJntGrp,udCtrlAllGrp = createDtlFromGeo(geo,1,2,'UpDw','cube','yellow')
    lrCtrl_list,lrJnt_list,lrCtrlGrp,lrJntGrp,lrJntAllGrp = createDtlFromGeo(geo,2,1,'Side','cube','yellow')
    mc.select(udJnt_list,udCrv)
    mc.skinCluster(tsb=1,wd=0,sw = .1)
    mc.select(lrJnt_list,lrCrv)
    mc.skinCluster(tsb=1,wd=0,sw = .1)
    mc.select(udCrv,lrCrv,wrapCrv,posCrv)
    mc.blendShape(foc=1,w = [[0,1.0],[1,1.0],[2,1.0]])

    ud_step = 1.0/(v_num-1)
    d_weight = 1
    u_weight = 0
    #print jnt_list
    for num in range(v_num):
        for jnt in jnt_list[num::v_num]:
            idx = jnt_list.index(jnt)
            skn = mel.eval('findRelatedSkinCluster "{}";'.format(udCrv))
            #print d_weight,u_weight
            mc.skinPercent(skn,udCrv+'.cv[{}]'.format(idx),tv = [[udJnt_list[0],d_weight],[udJnt_list[1],u_weight]])
        if num+1 != v_num-1:
            d_weight -= ud_step
            u_weight += ud_step
        else:
            d_weight = 0
            u_weight = 1
    
    side_step = 1.0/(u_num-1)
    l_weight = 0
    r_weight = 1
    for num in range(u_num):
        start = num*v_num
        end = start+v_num
        for jnt in jnt_list[start:end]:
            idx = jnt_list.index(jnt)
            #print idx
            skn = mel.eval('findRelatedSkinCluster "{}";'.format(lrCrv))
            mc.skinPercent(skn,lrCrv+'.cv[{}]'.format(idx),tv = [[lrJnt_list[0],r_weight],[lrJnt_list[1],l_weight]])
        if num+1 != u_num-1:
            r_weight -= side_step
            l_weight += side_step
        else:
            r_weight = 0
            l_weight = 1
    mc.select(jnt_list,geo)
    mc.skinCluster(tsb=1,dr =4.0)
    mc.select(geo)
    skinWeightDetail()

    nopc = mc.createNode('nearestPointOnCurve')
    mc.connectAttr(posCrv+'Shape.worldSpace[0]',nopc+'.inputCurve')
    for num,jnt in enumerate(jnt_list):
        t = mc.xform(jnt,ws=1,q=1,t=1)
        #print t
        mc.setAttr(nopc+'.ip',t[0],t[1],t[2],type='double3')
        pinf = mc.createNode('pointOnCurveInfo',n='{}{}{}pinf'.format(name,num+1,side))
        param = mc.getAttr(nopc+'.parameter')
        #print param
        mc.setAttr(pinf+'.parameter',param)
        mc.connectAttr(posCrv+'Shape.worldSpace[0]',pinf+'.inputCurve')
        mc.connectAttr('{}.p'.format(pinf),'{}.t'.format(ctrl_list[num].zro.name))
        mc.connectAttr('{}.p'.format(pinf),'{}.t'.format(jntGrp_list[num]))

    stillGrp = 'DtlRigStill_Grp'
    ctrlGrp = 'DtlRigCtrl_Grp'
    jntGrp = 'DtlRigJnt_Grp'
    allCtrlGrp = '{}AllDtlCtrl{}Grp'.format(name,side)
    allJntGrp = '{}AllDtlJnt{}Grp'.format(name,side)

    for grp in stillGrp,ctrlGrp,jntGrp,allCtrlGrp,allJntGrp:
        if not mc.objExists(grp):
            mc.createNode('transform',n=grp)

    mc.parent(ctrlAllGrp,udCtrlGrp,lrCtrlGrp,allCtrlGrp)
    mc.parent(jntAllGrp,udJntGrp,lrJntGrp,allJntGrp)
    mc.parent(allCtrlGrp,ctrlGrp)
    mc.parent(allJntGrp,jntGrp)    
    mc.parent(crvGrp,stillGrp)

def caDtlSetRun(geoGrp = '',*args):

    if not geoGrp:
        geoAll = mc.ls(sl = True)
    else:
        geoAll = utaCore.listGeo(sels = geoGrp, nameing = '_Geo', namePass = '_Grp')

    for sel in geoAll:
        sel = sel.split('|')[-1]
        mc.select(sel)
        bb = mc.exactWorldBoundingBox()
        Offset = float( abs( bb[1] - bb[4] ) * 1.2 )

        ## smalePlanGeo
        if Offset <= 2:
            u_num = 3
            v_num = 3
            objValue = sel
            # print 'small', Offset
        ## midPlaneGeo
        elif Offset <= 4:
            u_num = 5
            v_num = 5
            objValue = sel
            # print 'mid', Offset
        ## bigPlanGeo
        else :
            u_num = 6
            v_num = 6
            objValue = sel
            # print 'mid', Offset
        if 'Shadow' in objValue:
            print 'This geo is Shadow parts : {}'.format(objValue)
            continue
        else:
            caDtlSet(objValue,u_num = u_num, v_num = v_num)

def dtlCopyWeightToShadow(shadowGeo = ''):
    geo = shadowGeo.replace('Shadow','')
    print shadowGeo
    if mc.objExists(geo):
        mc.select(geo,shadowGeo)
        weightTools.copySelectedWeight()

def dtlCopyWeightToShadowSet(geoGrp=''):
    if not geoGrp:
        geoAll = mc.ls(sl = True)
    else:
        geoAll = utaCore.listGeo(sels = geoGrp, nameing = '_Geo', namePass = '_Grp')
    for geo in geoAll:
        geo = geo.split('|')[-1]
        if 'Shadow' in geo:
            dtlCopyWeightToShadow(geo)    

def skinWeightDetail(numberOfSmooth = 4):
    sel_tr = mc.ls(sl=1)
    sel_sh = mc.listRelatives(sel_tr,c=1,type="mesh")
    
    if sel_sh:
        for i in sel_sh:
            if "Orig" in i:
                sel_sh.remove(i)
        lsHis = mc.listHistory(sel_sh[0], lv=2)
        
        skinClst = mc.ls(lsHis,type="skinCluster")
        joints = mc.ls(lsHis,type="joint")
        
        if skinClst and joints:
            for i in joints:
                mc.setAttr("%s.liw"%i,0)
            
            mc.setAttr("%s.normalizeWeights"%skinClst[0],2)
            mc.ArtPaintSkinWeightsToolOptions()
            mc.artAttrSkinPaintCtx(mc.currentCtx(),e=1,sao="smooth")
            
            for obj in joints:
                mel.eval("artSkinInflListChanging %s 1;"%obj)
                mel.eval("artSkinInflListChanged artAttrSkinPaintCtx;")
                
                for i in range(numberOfSmooth):
                    mc.artAttrSkinPaintCtx(mc.currentCtx(),e=1,clr=1,sao="smooth")
            
            mc.setAttr("%s.normalizeWeights"%skinClst[0],1)
        
    # skinWeightDetail(numberOfSmooth = 4)


def connectWrap_ctrl(ctrl= [],search='',replace='',    uiNs = 'uiCtrl_md' ,wrapNs = 'swRig_md'):

    if not ctrl:
        ctrl = mc.ls(sl = True)
    jnt_list = []
    geo_list = []
    jntGrp_list = []
    ctrlLists = utaCore.listGeo(sels = ctrl, nameing = '_Ctrl', namePass = '_Grp')
    ctrl = []
    ctrlGrp = []
    for each in ctrlLists:
        ctrl.append(each.split('|')[-1])
        ctrlSplit = each.split('|')[-1]
        ctrlGrp.append(ctrlSplit.replace('UiRigGrp','UiRigCtrl'))
    for i in ctrl:
        jnt = i.replace(uiNs,wrapNs).replace('_Ctrl','_Jnt').replace('Ui','PlaneSw').replace(search,replace)
        geo = jnt.replace('_Jnt','_Geo').replace('SwRigDtl','Fcl')
        if mc.objExists(jnt):
            jnt_list.append([i,jnt])
        if mc.objExists(geo):
            geo_list.append([i,geo])
    
    for i in jnt_list:
        for t in 'trs':
            for ax in 'xyz':
                mc.connectAttr('{}.{}{}'.format(i[0],t,ax),'{}.{}{}'.format(i[1],t,ax))
    for i in jnt_list:
        if 'Overall' not in i[0]:
            # print i
            pma = mc.createNode('plusMinusAverage',n= i[0].replace('_Ctrl','_Pma'))
            mdv = mc.createNode('multiplyDivide',n= i[0].replace('_Ctrl','_Mdv'))
            shp = mc.listRelatives(i[0],s=1)[0]
            mc.addAttr(i[0],ln='wrap',max=1,min=0,dv=1,k=1)
            mc.addAttr(i[0],ln='offset',k=1)
            mc.addAttr(shp,ln='offsetAmp',k=1)
            mc.connectAttr(i[0]+'.wrap',i[1]+'.wrap')
            mc.connectAttr(i[0]+'.offset',pma+'.input1D[0]')
            mc.connectAttr(shp+'.offsetAmp',pma+'.input1D[1]')
            mc.connectAttr(pma+'.output1D',mdv+'.input1X')
            mc.connectAttr(mdv+'.outputX',i[1]+'.offset')
            mc.setAttr(mdv+'.input2X',.03)
        else:
            pass

def connectWrapLocalWorld(uiNs='uiCtrl_md',wrapNs='swRig_md'):
    ctrl_list = mc.ls('{}:*Ctrl'.format(uiNs))
    for ctrl in ctrl_list:
        if mc.objExists(ctrl+'.localWorld'):
            ctrlGrp = mc.listRelatives(ctrl,p=1)[0]
            jntGrp = ctrlGrp.replace(uiNs,wrapNs).replace('UiRigCtrl','PlaneSwRigDrvZro')
            # print mc.objExists(jntGrp)
            if mc.objExists(jntGrp):
                for t in 'trs':
                    for ax in 'xyz':
                        mc.connectAttr('{}.{}{}'.format(ctrlGrp,t,ax),'{}.{}{}'.format(jntGrp,t,ax))

def headSqsh(geo = 'HdRigGeo_Grp',name= 'HdUpr',deformHandleGrp= 'HdRigStill_Grp',ctrlGrp= 'HdRigCtrl_Grp'):
    bottom =1
    if not mc.objExists(deformHandleGrp):
        deformHandleGrp = mc.createNode('transform',n=deformHandleGrp)
    if not mc.objExists(ctrlGrp):
        ctrlGrp = mc.createNode('transform',n=ctrlGrp)
    # find bounding box
    bb = mc.xform(geo,bb=1,q=1,ws=1)
    length = abs(bb[4]-bb[1])
    # create deformer
    mc.select(geo)
    sqd,sqdh = mc.nonLinear(type='squash',n = '{}_Sqsh'.format(name))
    loc = mc.spaceLocator(n = '{}_Loc'.format(name))[0]
    # place deformer to buttom or top
    if bottom:
        mc.setAttr(sqdh+'.ty',bb[1])
        mc.setAttr(sqd+'.lowBound',(length/2)*-1)
        mc.setAttr(sqd+'.highBound',length/2)
        mc.setAttr(loc+'.ty',bb[4])
    else:
        mc.setAttr(sqdh+'.ty',bb[4])
        mc.setAttr(sqd+'.highBound',(length/2)*-1)
        mc.setAttr(sqd+'.lowBound',length/2)
        mc.setAttr(loc+'.ty',bb[1])
    ctrl = caCtrl(loc,ctrlShape = 'circle')
    mdv = mc.createNode('multiplyDivide',n = '{}Sqsh_Mdv'.format(name))
    mc.connectAttr(ctrl.ctrl.name+'.ty',mdv+'.input1X')
    mc.setAttr(mdv+'.input2X',1/length)
    mc.connectAttr(mdv+'.outputX',sqdh+'.factor')
    mc.delete(loc)
    mc.parent(sqdh,deformHandleGrp)

    ctrl.zro.parent(ctrlGrp)

    ctrl = ctrl.ctrl.name
    bb = mc.xform(geo,bb=1,q=1,ws=1)
    length = abs(bb[4]-bb[1])
    width = abs(bb[0]-bb[3])
    mc.select(geo)
    bend,bendH = mc.nonLinear(type='bend')
    mc.setAttr(bend+'.lowBound',0)
    mc.setAttr(bend+'.highBound',length)
    mc.setAttr(bendH+'.ty',bb[1])
    bendMdv = mc.createNode('multiplyDivide',n = '{}BendLR_Mdv'.format(name))
    mc.setAttr(bendMdv+'.input2X',5)
    mc.connectAttr(ctrl+'.tx',bendMdv+'.input1X')
    mc.connectAttr(bendMdv+'.outputX',bendH+'.curvature')
    mc.parent(bendH,deformHandleGrp)

    bb = mc.xform(geo,bb=1,q=1,ws=1)
    length = abs(bb[4]-bb[1])
    width = abs(bb[0]-bb[3])
    mc.select(geo)
    bend,bendH = mc.nonLinear(type='bend')
    mc.setAttr(bend+'.lowBound',0)
    mc.setAttr(bend+'.highBound',length)
    mc.setAttr(bendH+'.ry',-90)
    mc.setAttr(bendH+'.ty',bb[1])
    bendMdv = mc.createNode('multiplyDivide',n = '{}BendFB_Mdv'.format(name))
    mc.setAttr(bendMdv+'.input2X',5)
    mc.connectAttr(ctrl+'.tz',bendMdv+'.input1X')
    mc.connectAttr(bendMdv+'.outputX',bendH+'.curvature')
    mc.parent(bendH,deformHandleGrp)

# geo = 'MouthPlaneUi_Geo'
# visCtrl = 'MouthUi_Ctrl'
# geoLoc = 'locator1'
# u_num = 3
# v_num = 3
# skinGrp = 'FaceUiJnt_Grp'
# #createDtlFromGeo(geo,visCtrl,u_num,v_num,skinGrp,geoLoc)
############################## fix body Rig #####################################
def changeRbnSetup(name,side):
    if side:
        side = '_{}_'.format(side)
    else:
        side = '_'
    ribbonCtrl = '{}Rbn{}Ctrl'.format(name,side)
    ribDtlList = mc.ls('{}*RbnDtl{}Ctrl'.format(name,side))
    sqshPma = mc.createNode('plusMinusAverage',n='{}Scl{}Pma'.format(name,side))
    for ax in 'xyz':
        mc.setAttr('{}.s{}'.format(ribbonCtrl,ax),l=0,k=1)
        mc.setAttr(sqshPma+'.input3D[1].input3D{}'.format(ax),-1)
    mc.connectAttr(ribbonCtrl+'.s'.format(ax),sqshPma+'.input3D[0]',f=1)
    
    value_list = [.25,.5,1,.5,.25]
        
    for num,dtl in enumerate(ribDtlList):
        for ax in 'xyz':
            mc.setAttr('{}.s{}'.format(dtl,ax),l=0,k=1)
        selfSquash = dtl.replace('_Ctrl','_Pma').replace('RbnDtl','RbnSquash')
        totalSquash = dtl.replace('_Ctrl','_Pma').replace('RbnDtl','RbnTotalSquash')
        jnt = dtl.replace('_Ctrl','_Jnt')
        # connect Total Squash to Jnt
        
        mc.connectAttr(dtl+'.s',totalSquash+'.input3D[0]',f=1)
        for ax in 'xyz':
            mc.setAttr('{}.s{}'.format(jnt,ax),l=0,k=1)
        for ax in 'xyz':
            mc.connectAttr(totalSquash+'.output3D{}'.format(ax),jnt+'.s{}'.format(ax),f=1)
            mc.connectAttr(selfSquash+'.output1D',totalSquash+'.input3D[1].input3D{}'.format(ax),f=1)
        mc.setAttr(selfSquash+'.input1D[2]',0)
        
        # create SquashMdv for Each joint
        dtlSqshMdv = mc.createNode('multiplyDivide',n = '{}{}RbnMainSqsh{}Mdv'.format(name,num,side))
        mc.connectAttr(sqshPma+'.output3D',dtlSqshMdv+'.input1',f=1)
        for ax in 'xyz':
            mc.setAttr(dtlSqshMdv+'.input2{}'.format(ax.upper()),value_list[num])
        mc.connectAttr(dtlSqshMdv+'.output',totalSquash+'.input3D[2]',f=1)




def changeMidRibbonSetup(name,side,partList,value_list):
    allPart_list = []
    if side:
        side = '_{}_'.format(side)
    else:
        side = '_'

    ribbonCtrl = '{}Rbn{}Ctrl'.format(name,side)
    sqshPma = mc.createNode('plusMinusAverage',n='{}MidScl{}Pma'.format(name,side))
    for ax in 'xyz':
        mc.setAttr('{}.s{}'.format(ribbonCtrl,ax),l=0,k=1)
        mc.setAttr(sqshPma+'.input3D[1].input3D{}'.format(ax),-1)
        mc.connectAttr('{}.s{}'.format(ribbonCtrl,ax),sqshPma+'.input3D[0].input3D{}'.format(ax),f=1)
    mc.connectAttr(ribbonCtrl+'.s'.format(ax),sqshPma+'.input3D[0]',f=1)
    count = 0
    chooseRibDtl = []
    for part in partList:
        ribDtlList = mc.ls('{}*RbnDtl{}Ctrl'.format(part,side))
        #print ribDtlList
        if not count:
            for i in ribDtlList[:][3:][:]:
                chooseRibDtl.append(i)
        else:
            for i in ribDtlList[:][:2][:]:
                chooseRibDtl.append(i)
        count+=1
    for num,dtl in enumerate(chooseRibDtl):
        selfSquash = dtl.replace('_Ctrl','_Pma').replace('RbnDtl','RbnSquash')
        totalSquash = dtl.replace('_Ctrl','_Pma').replace('RbnDtl','RbnTotalSquash')
        jnt = dtl.replace('_Ctrl','_Jnt')
        dtlMidMdv = mc.createNode('multiplyDivide', n =dtl.replace('_Ctrl','_Mdv').replace('RbnDtl','RbnMidScl'))
        #print dtlMidMdv
        for ax in 'xyz':
              mc.connectAttr(sqshPma+'.output3D{}'.format(ax),dtlMidMdv+'.input1{}'.format(ax.upper()),f=1)
              mc.setAttr(dtlMidMdv +'.input2{}'.format(ax.upper()),value_list[num])
              mc.connectAttr(dtlMidMdv+'.output{}'.format(ax.upper()),totalSquash+'.input3D[3].input3D{}'.format(ax),f=1)
              

def addTwistMidRibbon(name,side,partList,value_list):
    allPart_list = []
    if side:
        side = '_{}_'.format(side)
    else:
        side = '_'    
    ribbonCtrl = '{}Rbn{}Ctrl'.format(name,side)
    #twMdv = mc.createNode('multiplyDivide',n='{}MidTw{}Mdv'.format(name,side))
    #for ax in 'y':
        #mc.connectAttr('{}.r{}'.format(ribbonCtrl,ax),twMdv+'.input1{}'.format(ax.upper()),f=1)
    count = 0
    chooseRibDtl = []
    for part in partList:
        ribDtlList = mc.ls('{}*RbnDtl{}Ctrl'.format(part,side))
        #print ribDtlList
        if not count:
            for i in ribDtlList[:][:][:]:
                chooseRibDtl.append(i)
        else:
            for i in ribDtlList[:][:][:]:
                chooseRibDtl.append(i)
        count+=1
    # print chooseRibDtl
    for num,dtl in enumerate(chooseRibDtl):
        twsPma = dtl.replace('_Ctrl','_Pma').replace('RbnDtl','RbnDtlTws')
        #totalSquash = dtl.replace('_Ctrl','_Pma').replace('RbnDtl','RbnTotalSquash')
        #jnt = dtl.replace('_Ctrl','_Jnt')
        dtlMidMdv = mc.createNode('multiplyDivide', n =dtl.replace('_Ctrl','_Mdv').replace('RbnDtl','RbnMidTw'))
        #print dtlMidMdv
        for ax in 'y':
              mc.connectAttr(ribbonCtrl +'.r{}'.format(ax),dtlMidMdv+'.input1{}'.format(ax.upper()),f=1)
              mc.setAttr(dtlMidMdv +'.input2{}'.format(ax.upper()),value_list[num])
              mc.connectAttr(dtlMidMdv+'.output{}'.format(ax.upper()),twsPma+'.input1D[2]'.format(ax),f=1)
              
def changeRbnSpine():
    ctrl_list = mc.ls('Spine*_Ctrl')
    spineCtrl = []
    for i in ctrl_list:
        if re.search('Spine[0-9]_Ctrl',i):
            spineCtrl.append(i)
    for ctrl in spineCtrl:
        pma = ctrl.replace('_Ctrl','Sqsh_Pma')
        mdv = ctrl.replace('_Ctrl','Sqsh_Mdv')
        scaJnt = ctrl.replace('_Ctrl','Sca_Jnt')
        jnt = ctrl.replace('_Ctrl','_Jnt')
        brthJnt = ctrl.replace('_Ctrl','Brth_Jnt')
        for num,ax in enumerate('xyz'):
            mc.connectAttr(mdv+'.output{}'.format(ax.upper()),pma+'.input3D[{}].input3Dx'.format(num),f=1)
            mc.connectAttr(mdv+'.output{}'.format(ax.upper()),pma+'.input3D[{}].input3Dz'.format(num),f=1)
            
        for ax in 'xz':
            mc.connectAttr(pma+'.default',pma+'.input3D[3].input3D{}'.format(ax),f=1)
            
        for ax in 'xyz':
            mc.setAttr('{}.s{}'.format(ctrl,ax),l=0,k=1)
            mc.connectAttr(pma+'.output3D{}'.format(ax),scaJnt+'.s{}'.format(ax),f=1)
        
        ctrlPma = mc.createNode('plusMinusAverage',n= ctrl.replace('_Ctrl','SqshCtrl_Pma'))
        mc.setAttr(ctrlPma+'.input3D[0]',-1,0,-1,type='double3')
        mc.connectAttr(ctrl+'.s',ctrlPma+'.input3D[1]',f=1)
        mc.connectAttr(ctrlPma+'.output3D',pma+'.input3D[4]',f=1)

        #mc.disconnectAttr('{}.s'.format(jnt),'{}.inverseScale'.format(scaJnt))
        #mc.disconnectAttr('{}.s'.format(scaJnt),'{}.inverseScale'.format(brthJnt))
        mc.setAttr(brthJnt+'.ssc',0)
# sides = ['L','R']
# parts = ['UpArm','Forearm','UpLeg','LowLeg']
# for side in sides:
#     for part in parts:
#         changeRbnSetup(part,side)

# names = ['Leg','Arm']
# sides = ['L','R']
# partList = [['UpLeg','LowLeg'],['UpArm','Forearm']]
# value_list = [.5,.75,.75,.5,]
# for num,name in enumerate(names):
#     for side in sides:
#         changeMidRibbonSetup(name,side,partList[num],value_list)


def wrapGroup():
    grpAll =['FclRigGeo_Grp', 'FclRigStill_Grp', 'FclRigCtrl_Grp']
    createGrp = []
    for eachGrp in grpAll:
        if not mc.objExists(eachGrp):
            group = utaCore.createGrp(lists= [eachGrp]) 
            createGrp.append(group[0])

    stillGrp = ['swRig_md:SwRigSkin_Grp', 'swRig_md:SwRigGeo_Grp', 'HdRigStill_Grp', 'dtlRig_md:DtlRigGeo_Grp', 'dtlRig_md:DtlRigJnt_Grp', 'dtlRig_md:DtlRigStill_Grp']
    ctrlGrp = ['uiCtrl_md:FacialUiRigCtrl_Grp', 'HdRigCtrl_Grp', 'dtlRig_md:DtlRigCtrl_Grp',  'swRig_md:SwRigCtrl_Grp']

    ## StillGrp
    for each in stillGrp:
        # sels = mc.ls(each)
        if mc.objExists(each):
            if mc.objExists(each):
                mc.select(each)
                mc.select(grpAll[1], add = True)
                mc.parent(each, grpAll[1])
                mc.setAttr('{}.v'.format(grpAll[0]), 0)
                mc.setAttr('{}.v'.format(grpAll[1]), 0)
    ## ctrlGrp
    for each in ctrlGrp:
        # sels = mc.ls(each)
        if mc.objExists(each):
            mc.select(each)
            mc.select(grpAll[2], add = True)
            mc.parent(each, grpAll[2])


    ## parent 'RivetAll_Grp' to Still_Grp
    # mc.parent('RivetAll_Grp', 'FclRigStill_Grp')


# import os
# import pymel.core as pmc
def clearOldFcl():
    headGeo = mc.ls(sl=True)[0]
    if ':' in headGeo:
        ns = ':'.join(headGeo.split(':')[:-1])+':'
    else:
        ns = ''
    print '{}FacePlaneGeo_Grp'.format(ns)
    # print headGeo
    mc.editRenderLayerGlobals(crl = 'defaultRenderLayer')
    assignFclLambert('{}FacePlaneGeo_Grp'.format(ns),headGeo)
    mc.editRenderLayerGlobals(crl = 'main_tech')
    assignFclLambert('{}FacePlaneGeo_Grp'.format(ns),headGeo)
    mc.editRenderLayerGlobals(crl = 'main_preview')
    assignFclLambert('{}FacePlaneGeo_Grp'.format(ns),headGeo)
    mel.eval('MLdeleteUnused')
    mc.editRenderLayerGlobals(crl = 'main_texture')
    copyTextureToModel(headGeo)
    setFileNodeUDIM()

def assignFclLambert(facePlaneGrp='',headGeo = ''):
    mc.select(headGeo)
    mc.sets(facePlaneGrp,e=True,fe='initialShadingGroup')
    mc.sets(headGeo,e=True,fe='initialShadingGroup')


def copyTextureToModel(headGeo = 'Body_Geo'):
    context = context_info.ContextPathInfo()
    context.context.update(process = 'uiCtrl')
    context.context.update(step='rig')
    context.context.update(res='md')
    path = context.path.output_hero().abs_path()
    heroFile = context.publish_name(hero=1)

    hp = os.path.join(path , heroFile)

    mc.file(hp,i=1,ns='uiCtrl_md')
    x = shade_utils.list_shader('uiCtrl_md:FacialUiRigCtrl_Grp')
    x.pop('lambert1')
    if ':' in headGeo:
        ns = ':'.join(headGeo.split(':')[:-1])+':'
    else:
        ns = ''
    # print ns
    mc.connectAttr('{}:Head_GeoShape.uvSet[1].uvSetName'.format(ns),'uiCtrl_md:HeadRampHead_Uvc.uvSets[0]',f=1)

    for i in x.keys():
        if x[i]['objs']:
            sg = x[i]['shadingEngine']
            geos =  x[i]['objs']
            obj_list = []
            for geo in geos:
                if geo == 'Head_Geo' or 'Plane' in geo:
                    model_geo = ns + geo.split(':')[-1].replace('UiRig','')
                    if mc.objExists(model_geo):
                        obj_list.append(model_geo)
            if obj_list:
                #mc.select(obj_list)
                mc.sets(obj_list, e=True, nw=True, fe=sg)

    mc.sets([headGeo],e = 1 ,nw=1,fe=x['uiCtrl_md:HeadRamp_Bln']['shadingEngine'])

    nodeType_list = ['plusMinusAverage','multiplyDivide','addDoubleLinear']
    for nt in nodeType_list:
        # print mc.ls('uiCtrl_md:*',type= nt)
        all_list = mc.ls('uiCtrl_md:*',type= nt)
        except_list = []
        for i in all_list:
            if mc.listConnections(i,type = 'file',s=1,d=0,c=1) or mc.listConnections(i,type = 'layeredTexture',s=1,d=0,c=1):
                except_list.append(i)
        del_list = list(set(all_list) - set(except_list))
        mc.delete(del_list)

    mc.delete('uiCtrl_md:FacialUiRigCtrl_Grp')

    for i in mc.ls('uiCtrl_md:*_File'):
        renPath = mc.getAttr(i+'.fileTextureName').replace('texPreview','texRen').replace('preview','hi')
        mc.setAttr(i+'.fileTextureName',renPath,type='string')

        if ('/HeadSeq/' in renPath )  or ('/HairSeq/'in renPath )  or ('/IrisSeq/'in renPath )  or ('/BlinkSeq/'in renPath ):
            mc.setAttr(i+'.frameOffset',0)
            print 'Do Exception'
        
        elif '_L_' in i:
            mc.setAttr(i+'.frameOffset',19)

        else:
            mc.setAttr(i+'.frameOffset',9)



    for i in mc.ls('uiCtrl_md:*'):
        mc.rename(i,i.split(':')[-1])
    mc.namespace(rm='uiCtrl_md')

def setFileNodeUDIM():
    allFileNodes = mc.ls(type="file")

    for f in allFileNodes:
        if not mc.getAttr("{}.useFrameExtension".format(f)):
            continue
            
        mc.setAttr("{}.uvTilingMode".format(f), 3)


def store_uiCtrl_data():
    layerTextList = mc.ls('fclRig_md:*',type='layeredTexture')
    fileList = mc.ls('fclRig_md:*',type='file')
    texture2d = mc.ls('fclRig_md:*',type ='place2dTexture')
    ramp = mc.ls('fclRig_md:*',type ='ramp')
    mdl = mc.ls('fclRig_md:*',type ='multDoubleLinear')
    mdv = mc.ls('fclRig_md:*',type ='multiplyDivide')
    pma = mc.ls('fclRig_md:*',type ='plusMinusAverage')    
    shd_dict = {}
    for lists in layerTextList,fileList,texture2d,ramp,mdl,mdv,pma:
        for node in lists:
            cons = mc.listConnections(node,c=1,p=1,s=1,d=0,scn=1)
            print cons
            if cons:
                if mc.nodeType(cons[0]) != 'uvChooser' and mc.nodeType(cons[1]) !='uvChooser':
                    shd_dict[node] = cons
    return shd_dict

        
def create_uiCtrl_connect(dictStr = {} ,asset = '',fcl_ns = 'fclRig_md:',mtr_ns = 'mdlMainMtr:'):
    if not asset:
        contextPath = context_info.ContextPathInfo()
        asset = contextPath.name
    ns = '{}_{}'.format(asset,mtr_ns)
    do_list = []
    for node in dictStr.keys():
        if mc.objExists(node):
            do_list.append(node)
    print do_list
    for node in do_list:
        con_list = dictStr[node]
        if con_list:
            in_list = con_list[::2]
            out_list = con_list[1::2]
            for num,node in enumerate(out_list):
                fclAttr = node
                mtrAttr = in_list[num].replace(fcl_ns,ns)
                print mtrAttr , fclAttr
                print mc.objExists(mtrAttr) , mc.objExists(fclAttr)
                if mc.objExists(mtrAttr) and mc.objExists(fclAttr):
                    con = mc.listConnections(mtrAttr,s=1,c=1,d=0,p=1)
                    if con:
                        contype = mc.nodeType(con)

                    if not mc.listConnections(mtrAttr,s=1,c=1,d=0,p=1) :
                        attrType = mc.getAttr(fclAttr,type=1)
                        
                        sc_attr = 'sc__{}'.format(fclAttr.split(':')[-1].replace('.','_'))
                        geoGrpAttr = 'bodyRig_md:Geo_Grp.{}'.format(sc_attr)

                        if not mc.objExists(geoGrpAttr):
                            print attrType
                            if attrType not in  ['float','long','doubleLinear','float3','float2','double']:
                                x = mc.addAttr('bodyRig_md:Geo_Grp',ln= sc_attr, dt = attrType) 
                            else:
                                x = mc.addAttr('bodyRig_md:Geo_Grp',ln= sc_attr,at = attrType) 
                            mc.connectAttr(fclAttr,geoGrpAttr)
                        mc.connectAttr(geoGrpAttr,mtrAttr,f=1)

def dummy_shdConnect():
    shdDict = store_uiCtrl_data()
    context = context_info.ContextPathInfo()
    context.context.update(process = 'uiCtrl')
    context.context.update(step='rig')
    context.context.update(res='md')
    path = context.path.output_hero().abs_path()
    heroFile = context.publish_name(hero=1)

    hp = os.path.join(path , heroFile)
    ns = '{}_uiCtrl_md'.format(context.name)
    mc.file(hp,i=1,ns= ns)  
    nodeType_list = ['plusMinusAverage','multiplyDivide','addDoubleLinear']
    for nt in nodeType_list:
        # print mc.ls('uiCtrl_md:*',type= nt)
        mc.delete(mc.ls('{}:*'.format(ns),type= nt))
    mc.delete('{}:FacialUiRigCtrl_Grp'.format(ns))     
    print context.name
    print shdDict
    create_uiCtrl_connect(shdDict,context.name,fcl_ns = 'fclRig_md:',mtr_ns='uiCtrl_md:')
    #mc.delete('uiCtrl_md:FacialUiRigCtrl_Grp')

    mc.delete(mc.ls('{}:*'.format(ns)))
    mc.namespace(rm=ns)

#x = caRigModule.store_uiCtrl_data()   
#caRigModule.create_uiCtrl_connect(x,asset = '',fcl_ns = 'fclRig_md:',mtr_ns = 'mdlMainMtr')   



def swapLayerOrder(lytNode = '',index = []):
    if mc.objExists(lytNode) and len(index) == 2:
        for attr in ['color','alpha']:
            con1 = mc.listConnections('{}.inputs[{}].{}'.format(lytNode,index[0],attr),s=1,d=0,p=1,c=1)
            con2 = mc.listConnections('{}.inputs[{}].{}'.format(lytNode,index[1],attr),s=1,d=0,p=1,c=1)
            #print con1,con2
            if con1:
                mc.disconnectAttr(con1[1],con1[0])
            if con2:
                mc.disconnectAttr(con2[1],con2[0])
            if con1:
                mc.connectAttr(con1[1],'{}.inputs[{}].{}'.format(lytNode,index[1],attr),f=1)
            if con2:
                mc.connectAttr(con2[1],'{}.inputs[{}].{}'.format(lytNode,index[0],attr),f=1)
        
        for attr in ['blendMode']:
             attr1 = mc.getAttr('{}.inputs[{}].{}'.format(lytNode,index[0],attr))
             attr2 = mc.getAttr('{}.inputs[{}].{}'.format(lytNode,index[1],attr))
             
             mc.setAttr('{}.inputs[{}].{}'.format(lytNode,index[1],attr),attr1)
             mc.setAttr('{}.inputs[{}].{}'.format(lytNode,index[0],attr),attr2)  
             
#swapLayerOrder(lytNode = 'EyeSeqAll_L_Lyt',index = [2,1])
def changeLayerInput(lytNode = '' , newLayerOrder = []):
    from utaTools.utapy import utaCore 
    reload(utaCore)
    if mc.objExists(lytNode) and newLayerOrder:
        layterTexture, connectLists, objConnect = utaCore.removeMultiInstanceIndexOrderLayerTexture(layterTexture = [lytNode], removeMultiInstance = False)
        # curIndex = range(len(newLayerOrder))
        curIndex = connectLists

        for id,num in enumerate(newLayerOrder):
            newIdx = id
            curIdx = curIndex.index(num)
            newIdxVal = curIndex[newIdx]
            curIdxVal = curIndex[curIdx]
            if newIdx != curIdx:
                swapLayerOrder(lytNode,[newIdx,curIdx])
                curIndex[newIdx] = curIdxVal
                curIndex[curIdx] = newIdxVal
            
#changeLayerInput('EyeSeqAll_L_Lyt',newLayerOrder = [3,0,1,2])

#changeLayerInput('EyeSeqAll_L_Lyt',newLayerOrder = [1,3,0,2])

def prepPoseActionDriven(name,side,attachNode = 'Ctrl_Grp'):
    
    if side:
        side = '_{}_'.format(side)
    else:
        side = '_'
    # print side
    locShape = '{}Reader{}LocShape'.format(name,side)
    mdvZPos = '{}ActZ{}Mdv'.format(name,side)
    mdvXPos = '{}ActX{}Mdv'.format(name,side)
    mdvXNeg = '{}ActXNeg{}Mdv'.format(name,side)
    mdvZNeg = '{}ActZNeg{}Mdv'.format(name,side)
    conditionX = '{}CheckXPN{}Cond'.format(name,side)
    conditionZ = '{}CheckZPN{}Cond'.format(name,side)
    for obj in [mdvXNeg,mdvZNeg,conditionX,conditionZ]:
        if not mc.objExists(obj):
            if 'Mdv' in obj:
                obj = mc.createNode('multiplyDivide',n=obj)
                mc.setAttr(obj+'.input2X',-1)
                mc.setAttr(obj+'.input2Y',-1)
                mc.setAttr(obj+'.input2Z',-1)
            elif 'Cond' in obj:
                obj = mc.createNode('condition', n = obj)
                mc.setAttr(obj+'.operation',2)
    intensityx = ['Intensityxn','Intensityxp']
    intensityz = ['Intensityzp','Intensityzn']

    # set MDV value

    mc.connectAttr('{}.Intensityxn'.format(locShape),'{}.input1X'.format(mdvXNeg))
    mc.connectAttr('{}.Intensityzn'.format(locShape),'{}.input1Z'.format(mdvZNeg))

    # connect intensity x
    mc.connectAttr('{}.Intensityxn'.format(locShape),'{}.firstTerm'.format(conditionX,))
    mc.connectAttr('{}.Intensityxp'.format(locShape),'{}.secondTerm'.format(conditionX))
    mc.connectAttr('{}.Intensityxp'.format(locShape),'{}.colorIfFalse.colorIfFalseR'.format(conditionX))
    mc.connectAttr('{}.outputX'.format(mdvXNeg),'{}.colorIfTrue.colorIfTrueR'.format(conditionX))

    # connect intensity zq
    mc.connectAttr('{}.Intensityzn'.format(locShape),'{}.firstTerm'.format(conditionZ,))
    mc.connectAttr('{}.Intensityzp'.format(locShape),'{}.secondTerm'.format(conditionZ))
    mc.connectAttr('{}.Intensityzp'.format(locShape),'{}.colorIfFalse.colorIfFalseR'.format(conditionZ))
    mc.connectAttr('{}.outputZ'.format(mdvZNeg),'{}.colorIfTrue.colorIfTrueR'.format(conditionZ))

    attachAttrX = '{}{}XPN'.format(name,side)
    attachAttrZ = '{}{}ZPN'.format(name,side)
    mc.addAttr(attachNode,ln=attachAttrX)
    mc.addAttr(attachNode,ln=attachAttrZ)
    mc.connectAttr('{}.outColorR'.format(conditionX),'{}.{}'.format(attachNode,attachAttrX))
    mc.connectAttr('{}.outColorR'.format(conditionZ),'{}.{}'.format(attachNode,attachAttrZ))
#prepPoseActionDriven('ShoulderCrr','L')


def addDrivenkey(driver = '',driver_attr = '',driver_value = 0, driven= '',driven_attr = '',driven_value = 0):
    if driver and driven:
        if ':' in driver:
            ns = driver.split(':')[0]
            driver_obj = driver.split(':')[1]
            splitList = driver_obj.split(':')
            name = ns+':'+splitList[0]
        else:
            splitList = driven.split('_')
            name = splitList[0]
        if len(splitList) ==3:
            side = '_{}_'.format(splitList[1])
        else:
            side = '_'
        
        if ':' in driven:
            ns = driven.split(':')[0]
            driven_obj = driven.split(':')[1]
            driven_splitList = driven_obj.split(':')
            driven_name = ns+':'+driven_splitList[0]
        else:
            driven_name = driven.split('_')
            name = driven_splitList[0]
        if len(splitList) ==3:
            driven_side = '_{}_'.format(driven_splitList[1])
        else:
            driven_side = '_'

        # create animCurve node
        animCurve =  '{}Drv{}{}_AnimCrv'.format(driven_name,driven_side,driven_attr)
        if not mc.objExists(animCurve):
            animCurve = mc.createNode('animCurveUU',n =animCurve)
        # connect input
        con = mc.listConnections(animCurve+'.input')

        if con:
            if driver not in con[0]:
                print 'this attr has another driver'
                return None
            else:
                pass
        else:
            mc.connectAttr('{}.{}'.format(driver,driver_attr),animCurve+'.input')   
        #connect driven
        # check driven lock
        isLock = mc.getAttr('{}.{}'.format(driven,driven_attr),l=1)
        if isLock:
            mc.setAttr('{}.{}'.format(driven,driven_attr),l=0)
        # check driven connection
        drivenCon = mc.listConnections('{}.{}'.format(driven,driven_attr),s=1,d=0,p=1,c=1)
        if drivenCon:
            if animCurve not in drivenCon[0]:
                print '{} attr has been driven'
                mc.disconnectAttr(drivenCon[1],drivenCon[0])
        # disconnect driven
        # connect driven
        mc.connectAttr(animCurve+'.output','{}.{}'.format(driven,driven_attr))
        scaleAttr_list = ['scaleX','scaleY','scaleZ','sx','sy','sz']
        if driven_attr in scaleAttr_list:
            defaultValue = 1
        else:
            defaultValue=0
        mc.setKeyframe(animCurve,f=0,v = defaultValue ,itt='linear',ott='linear')
        mc.setKeyframe(animCurve,f=driver_value,v = driven_value,itt='linear',ott='linear')


# name = 'ShoulderCrr'
# side = 'L'
# driven = 'UpArmDrvUp2Sub_L_Ctrl'
# axis = 'x'
def setDrivenKey(name = '',side = '',driven ='',axis = 'x'):
    # name = 'ShoulderCrr'
    # side = 'L'
    # driven = 'UpArmDrvUp2Sub_L_Ctrl'
    # axis = 'x'    
    if side:
        side = '_{}_'.format(side)
    else:
        side = '_'
    condition = '{}Check{}PN{}Cond'.format(name,axis.upper(),side)

    idt_mattrix = [1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0]
    intensity = mc.getAttr('{}.outColorR'.format(condition))
    driven_par = mc.listRelatives(driven,p=1)[0]
    cur_m = pm.datatypes.Matrix( mc.getAttr(driven+'.worldMatrix'))
    grnadPar_m =pm.datatypes.Matrix( mc.getAttr(driven_par+'.parentInverseMatrix'))
    new_m = cur_m*grnadPar_m
    dummy = mc.createNode('transform')
    pm.xform(dummy,m=new_m)
    nt = mc.xform(dummy,q=1,t=1)
    nr = mc.xform(dummy,q=1,ro=1)
    ns = mc.xform(dummy,q=1,s=1,r=1)
    trans_list = [nt,nr,ns]
    mc.delete(dummy)
    for count,t in enumerate('trs'):
        val_list = trans_list[count]
        for num,ax in enumerate('xyz'):
            addDrivenkey(condition,'outColorR',intensity,driven_par,'{}{}'.format(t,ax),val_list[num])
    mc.xform(driven,m=idt_mattrix,os=1)

def mirrorDrivenKeys(driven):
    # driven  = object that has side to mirror
    if ':' not in driven:
        splitList = driven.split('_')
    else:
        splitList = driven.split(':')[1].split('_')

    if len(splitList) == 3:
        side = splitList[1]
        if side == 'L':
            side  = 'L'
            mir_side = 'R'
        else:
            side = 'R'
            mir_side = 'L'
        driven_par = mc.listRelatives(driven,p=1)[0]
        mir_driven = driven.replace('_{}_'.format(side),'_{}_'.format(mir_side))
        mir = driven_par.replace('_{}_'.format(side),'_{}_'.format(mir_side))
        for t in 'trs':
            for ax in 'xyz':
                anim_crv = mc.listConnections('{}.{}{}'.format(driven_par,t,ax),s=1,d=0,p=0,scn=1)[0]
                mir_crv = mc.listConnections('{}.{}{}'.format(mir,t,ax),s=1,d=0,p=0,scn=1)
                if mir_crv:
                    mir_crv = mir_crv[0]
                else:
                    driver = mc.listConnections('{}.input'.format(anim_crv,t,ax),s=1,d=0,p=0,scn=1)[0]
                    mir_driver = driver.replace('_{}_'.format(side),'_{}_'.format(mir_side))
                    #mir_split = mir_driver.split('_')
                    #mir_name = mir_split[0]
                    #reader_name = mir_name.split('Check')[0]
                    reader_name = mir_driver.split('Check')[0]
                    reader_ax = mir_driver.split('Check')[1].split('PN')[0]
                    setDrivenKey(reader_name,mir_side,mir_driven,reader_ax)
                #if mc.objExists(mir_crv):
                mir_crv = mc.listConnections('{}.{}{}'.format(mir,t,ax),s=1,d=0,p=0,scn=1)
                mir_key = mc.keyframe(mir_crv,fc=1,q=1)
                inputs = mc.keyframe(anim_crv,fc=1,q=1)
                outputs = mc.keyframe(anim_crv,vc=1,q=1)
                match = []
                non_match = []
                for key in mir_key:
                    if key in inputs:
                        match.append(key)
                    else:
                        non_match.append(key)
                for key in non_match:
                    mc.cutKey(mir_crv,t=(key,key))
                for idx,key in enumerate(inputs):
                    if t != 't':
                        mc.setKeyframe(mir_crv,f=key,v=outputs[idx],itt='linear',ott='linear')
                    else:
                        mc.setKeyframe(mir_crv,f=key,v=-outputs[idx],itt='linear',ott='linear')     

def addZroKey(ctrl):
    # driven  = object that has side to mirror
    drv = mc.listRelatives(ctrl,p=1)[0]
    for t in 'trs':
        for ax in 'xyz':
            anim_crv = mc.listConnections('{}.{}{}'.format(drv,t,ax),s=1,d=0,p=0,scn=1)
    if anim_crv:
        mc.setKeyframe(anim_crv[0],f=0,v=0,itt='linear',ott='linear')
        
    #addDrivenkey('UpArmFk_L_Ctrl','tx',10,'UpArmDrvUp2Sub_L_Ctrl','tx',5 )  

    #addDrivenkey(conditionZ,'outColorR',.4,'UpArmDrvUp2SubCtrlPars_L_Grp','tx',5 )  
#prepPoseActionDriven('ShoulderCrr','L')
#prepPoseActionDriven('ShoulderCrr','R')        

#setDrivenKey(readerName = 'ShoulderCrr',side = 'L',driven ='UpArmDrvUp2Sub_L_Ctrl',axis = 'z')

#mirrorDrivenKeys('UpArmDrvUp2Sub_L_Ctrl')


def add_keepShadeAttr(fclNs='fclRig_md',bodyNs = 'bodyRig_md'):
    fclRigGeo =  mc.ls('{}:*FclRig*_Geo'.format(fclNs))
    bodyRigGeo = []
    for geo in fclRigGeo:
        bodyGeo = geo.replace(fclNs,bodyNs).replace('FclRig','')
        if mc.objExists(bodyGeo):
            shps = mc.listRelatives(bodyGeo,s=1)
            if shps:
                for shp in shps:
                    if not mc.objExists(shp+'.keepShade'):
                        mc.addAttr(shp, ln ='keepShade',at = 'bool',dv=1)
                    else:
                        mc.setAttr(shp+'.keepShade',1)


def createShadeData( targetGrp=None, objs=None):
    exceptions = ['initialShadingGroup']

    # EXAMPLE: {shaderName: {'objs':['sphereShape1', 'sphere2.f[0]'], 'shadingEngine':'lambert2SG'}}
    shaderInfo = shade_utils.list_shader(targetGrp=targetGrp, objs=objs, collapseFaceAssignment=True)  
    shadingEngines = []
    if shaderInfo :
        disconnect_attrs = []
        # iterate thru the shader info
        for shader, info in shaderInfo.iteritems():
            # SG
            shadingEngine = info['shadingEngine']
            if mc.lockNode(shadingEngine, q=True)[0]:
                mc.lockNode(shadingEngine, l=False)

            # --- assigned objects
            objs = info['objs']
            objs = shade_utils.clean_namespace(objs) # clean namespace
            # add attibute of assigned objects to SG
            shade_utils.add_attr(shadingEngine, 'objs', str(objs))
            if objs != []:
                if not shadingEngine in exceptions: 
                    shadingEngines.append(shadingEngine)

                # --- handle uvlink
                uvLinks, uvChoosers = shade_utils.getUvLinks(shader=shader, sg=shadingEngine)
                # print shader, uvLinks
                if uvLinks:
                    shade_utils.add_attr(shadingEngine, 'uvLink', str(uvLinks))
                    for uvc in uvChoosers:
                        out_cons = mc.listConnections(uvc, d=True, s=False, c=True, p=True)
                        if out_cons:
                            for i in xrange(0, len(out_cons), 2):
                                src = out_cons[i]
                                des = out_cons[i+1]
                                disconnect_attrs.append((src, des))





def copyWaveGeo(geo =''):
    if geo:
        ns = geo.split(':')[0]
        asset = geo.split(':')[0].replace('_','')
        elem = '{}Wave'.format(asset)
        cluster = mc.cluster(geo)
        clusterTransform = cluster[1]
        print clusterTransform
        
        stillGrp = '{}:Still_Grp'.format(ns)
        ctrlGrp = '{}:Ctrl_Grp'.format(ns)
        headCtrl = '{}:HeadGmbl_Ctrl'.format(ns)        
        
        # duplicate mesh
        dups = mc.duplicate(geo,rc=1)[0]
        mc.parent(dups,w=1)
        shps = mc.listRelatives(dups,s=1)
        
        # use origShape instead of geoShape
        # delete geoShape
        print shps
        for i in shps:
            if not '{}ShapeOrig'.format(dups) == i:
                mc.delete(i)
            else:
                origShape = i
        print i
        
        # check Name
        nameList = dups.split('_')
        if len(nameList) == 3:  
            name = nameList[0]
            side = '_{}_'.format(nameList[1])
        elif len(nameList) ==2:
            name = nameList[0]
            side = '_'
        
        # rename geo
        geoName = '{}{}{}Geo'.format(name,elem,side)
        mc.setAttr(origShape + '.intermediateObject',0)
        mc.rename(origShape,'{}Shape'.format(geoName))
        newGeo = mc.rename(dups,'{}'.format(geoName))

        fclGeo = '{}:{}FclRig{}Geo'.format(ns,name,side)
        bsnNode = '{}:{}FclRig{}Bsn'.format(ns,name,side)
        
        targetIdx = mc.getAttr(bsnNode+'.w',s=1)
        mc.blendShape(bsnNode,e=1,t=(fclGeo,targetIdx+1,newGeo,1.0))
        mc.setAttr(bsnNode+'.{}'.format(newGeo),1.0)

        waveGeoAllGrp = '{}WaveAllGeo_Grp'.format(asset)
        if not mc.objExists(waveGeoAllGrp):
            waveGeoAllGrp = mc.createNode('transform',n=waveGeoAllGrp)
            mc.parent(waveGeoAllGrp,stillGrp)
        mc.parent(newGeo,waveGeoAllGrp)
        mc.delete(cluster)
    return newGeo


def duplicateGeoList(geo_list=[]):
    newGeo_list = []
    if geo_list:
        for geo in geo_list:
            newGeo_list.append(copyWaveGeo(geo))

    print newGeo_list
    return newGeo_list

# sel = mc.ls(sl=True)
# duplicateGeoList(sel)

def createWaveCtrl(ns='',name='',side ='',geo_list=[]):
    asset = ns.replace('_','')
    elem = '{}Wave'.format(asset)
    geoShape_list = []
    for geo in geo_list:
        geoShape_list.append(geo+'Shape')
    cluster = mc.cluster(geoShape_list)
    clusterTransform = cluster[1]
    print clusterTransform
    
    stillGrp = '{}:Still_Grp'.format(ns)
    ctrlGrp = '{}:Ctrl_Grp'.format(ns)
    headCtrl = '{}:HeadGmbl_Ctrl'.format(ns)
    newGeo = duplicateGeoList(geo_list)
    
    # create deformer
    if side:
        sideStr = '_{}_'.format(side)
    else:
        sideStr = '_'

    zroGrp = mc.createNode('transform',n = '{}{}Zro{}Grp'.format(name,elem,sideStr))
    ofstGrp = mc.createNode('transform',n = '{}{}Ofst{}Grp'.format(name,elem,sideStr))
    wave = mc.nonLinear(newGeo,type='wave',n = '{}{}{}Wave'.format(name,elem,sideStr))
    print wave
    bb = mc.xform(newGeo[0],bb=1,q=1)
    distance = ((bb[0]**2+bb[1]**2)**.5)/2

    mc.parent(ofstGrp,zroGrp)
    mc.delete(mc.parentConstraint(wave,zroGrp,mo=0))
    mc.parent(wave[1],ofstGrp)
    mc.setAttr(wave[1]+'.tx',- distance)
    mc.delete(mc.pointConstraint(clusterTransform,zroGrp,mo=0))
    mc.delete(mc.orientConstraint(headCtrl,zroGrp,mo=0))
    
    # create ctrl
    waveCtrl = ctrlRig.Run(name       = '{}{}'.format(name,elem),
                 tmpJnt     = clusterTransform,
                 side       = side ,
                 shape      = 'circle' ,
                 jnt_ctrl   = 0,
                 jnt        = 0,
                 gimbal     = 0 ,
                 size       = 1 ,
                 drv        = 0,
                 inv        = 0,
                 color      = 'green',
                 constraint = 0,
                 vis = 0)
    ctrl = waveCtrl.ctrl.name
    crvFunc.set_shape(ctrl,'wave')
    
    # create connection ctrl to deformer
    # addAttr
    mc.addAttr(ctrl,ln = 'wave',dv=1,k=1,max=1,min=0)
    waveMdv = mc.createNode('multiplyDivide',n='{}{}Switch{}Mdv'.format(name,elem,sideStr))
    mc.connectAttr(ctrl+'.wave',waveMdv + '.input1X')
    mc.connectAttr(waveMdv+'.outputX',wave[0]+'.envelope')    


    for i,value in zip(['amplitude','wavelength'],[.04,.4]):
        mc.addAttr(ctrl,ln = i,k=1)
        mc.connectAttr(ctrl+'.{}'.format(i),wave[1]+'Shape.{}'.format(i))
        mc.setAttr(ctrl+'.{}'.format(i),value)

    for i,value in zip(['offset'],[0]):
        mc.addAttr(ctrl,ln = i,k=1)
        pma = mc.createNode('plusMinusAverage',n='{}{}Offset{}Pma'.format(name,elem,sideStr))
        mdv = mc.createNode('multiplyDivide',n='{}{}Offset{}Mdv'.format(name,elem,sideStr))
        mc.connectAttr(pma+'.output1D',mdv+'.input1X')
        mc.connectAttr(ctrl+'.amplitude',mdv+'.input2X')
        mc.connectAttr(ctrl+'.offset',pma+'.input1D[0]')
        mc.connectAttr(mdv+'.outputX',wave[1]+'Shape.{}'.format(i))
        #mc.setAttr(ctrl+'.{}'.format(i),value)


    mc.setAttr(wave[0]+'.maxRadius',100)
    for t in 'trs':
        for ax in 'xyz':
            mc.connectAttr('{}.{}{}'.format(ctrl,t,ax),'{}.{}{}'.format(ofstGrp,t,ax))
    
    # set hierarchy
    waveAllGrp = '{}WaveAllDef_Grp'.format(asset)
    waveCtrlAllGrp = '{}WaveAllCtrl_Grp'.format(asset)
    waveAllCtrl = '{}WaveAll_Ctrl'.format(asset)
    waveAllMdv = '{}WaveAll_Mdv'.format(asset)
    waveAmpMdv = '{}WaveAllAmp_Mdv'.format(asset)

    if not mc.objExists(waveAllGrp):
        waveAllGrp = mc.createNode('transform',n=waveAllGrp)
        mc.parent(waveAllGrp,stillGrp)
    mc.parent(zroGrp,waveAllGrp)
    
    if not mc.objExists(waveCtrlAllGrp):
        waveCtrlAllGrp = mc.createNode('transform',n=waveCtrlAllGrp)
        waveAllCtrlTmp = mc.circle(ch=0)
        waveAllCtrl = mc.rename(waveAllCtrlTmp,waveAllCtrl)
        mc.setAttr(waveAllCtrl+'Shape.ove',1)
        mc.setAttr(waveAllCtrl+'Shape.ovc',22)
        mc.move(0,1.2,1,waveAllCtrl+'.cv[:]',r=1)
        mc.scale(3,3,3,waveAllCtrl+'.cv[:]',r=1)
        for t in 'trs':
            for ax in 'xyz':
                mc.setAttr(waveAllCtrl+'.{}{}'.format(t,ax),l=1,k=0)
        mc.parent(waveAllCtrl,waveCtrlAllGrp)
        mc.addAttr(waveAllCtrl,ln = 'wave',dv=0,max=1,min=0,k=1)
        mc.addAttr(waveAllCtrl,ln = 'dtlVis',dv=0,max=1,min=0,k=1)
        mc.addAttr(waveAllCtrl,ln = 'offset',dv=0,k=1)
        mc.addAttr(waveAllCtrl,ln='auto',dv=0,max=1,min=0,k=1)
        mc.addAttr(waveAllCtrl+'Shape',ln='autoAmp',dv=1,max=1,min=0,k=1)
        waveAllMdv = mc.createNode('multiplyDivide',n = waveAllMdv)
        waveAmpMdv = mc.createNode('multiplyDivide',n = waveAllMdv)
        mc.parent(waveCtrlAllGrp,ctrlGrp)
        mc.delete(mc.parentConstraint(headCtrl,waveCtrlAllGrp))
        mc.parentConstraint(headCtrl,waveCtrlAllGrp,mo=1)
        mc.connectAttr(waveAllCtrl +'.auto',waveAllMdv +'.input1X')
        mc.connectAttr('time1.outTime',waveAmpMdv+'.input1X')
        mc.connectAttr(waveAllCtrl+'Shape.autoAmp',waveAmpMdv+'.input2X')
        mc.connectAttr(waveAmpMdv+'.outputX',waveAllMdv+'.input2X')



    mc.delete(mc.orientConstraint(headCtrl,waveCtrl.zro,mo=0))
    mc.parent(waveCtrl.zro.name,waveAllCtrl)
    mc.connectAttr(waveAllCtrl+'.dtlVis',waveCtrl.zro.name+'.v')
    mc.connectAttr(waveAllCtrl+'.wave',waveMdv+'.input2X')
    mc.connectAttr(waveAllCtrl+'.offset',pma+'.input1D[1]')
    mc.connectAttr(waveAllMdv +'.outputX',pma +'.input1D[2]')
    
    mc.delete(cluster)

def createWave_function():
    sel = mc.ls(sl=True)
    allmover = sel[0]
    ns = ns = allmover.split(':')[0]
    otherParts = sel[1:]
    facePlaneTrans = mc.listRelatives('{}:FacePlaneGeo_Grp'.format(ns),type='transform')
    facePlaneTrans = facePlaneTrans+otherParts
    for i in facePlaneTrans:
        planeTrans = mc.listRelatives(i,type='transform')
        if not mc.listRelatives(i,type='transform'):
           planeTrans = [i]
        #print planeTrans
        leftList = []
        rightList = []
        midList = []
        part = i.split(ns+':')[1].split('Plane')[0]
        print part
        for planes in planeTrans:
            if '_L_' in planes:
                side = 'L'
                leftList.append(planes)
                
            elif '_R_' in planes:
                side = 'R'
                rightList.append(planes)
                 
            else:
                side = ''
                midList.append(planes)
        if leftList:  
            createWaveCtrl(ns,part,'L',leftList)
        if rightList:
            createWaveCtrl(ns,part,'R',rightList) 
        if midList: 
            createWaveCtrl(ns,part,'',midList)


from rftool.rig.rigScript import core 
reload(core)
cn = core.Node()

from utaTools.utapy import utaCore
reload(utaCore)

from rf_maya.rftool.rig.rigScript import caFacialModule
reload(caFacialModule) 

#MouthTextureUiRig_Ctrl
def createPlaneTabUi(part,maxValue =1):
    side = ''
    ctrl = '{}TextureUiRig_Ctrl'.format(part)
    planeGeo = '{}UiRig_Geo'.format(part)
    planeTabGeo = '{}UiRigPlaneTab_Geo'.format(part)
    cond = '{}UiRigPlaneTabVis_Cond'.format(part)
    planeTabMdv = '{}UiRigPlaneTab_Mdv'.format(part)
    uiRigPma = '{}UiRig_Pma'.format(part)
    shadowPma = '{}UiRigShadow_Pma'.format(part)

    # create PlaneTabGeo
    tmpPlane = mc.duplicate(planeGeo,rr=1,rc=1)[0]
    planeTabGeo = mc.rename(tmpPlane,planeTabGeo)
    if side:
        side = '_{}_'.format(side)
    else:
        side = '_'
    # assign shade to planeTabGeo
    ## planeGeo Ui
    texturePlaneUiFile , texturePlaneUiPlace2DTexture = cn.generateFilePlace2DTexture(name = part, side = side, elem = 'UiRigPlaneTab', node = 'file')
    myPlaneUiLambert, myPlaneUiLambertSG = utaCore.generateMaterial(materialName = 'lambert', name = '{}UiRigPlaneTab'.format(part), side = side, shortName = '_Lmp')
    mc.connectAttr('{}.outColor'.format(texturePlaneUiFile), '{}.color'.format(myPlaneUiLambert))

    mc.select(planeTabGeo)
    utaCore.assignSelectionToShader(myPlaneUiLambert)
    # mc.setAttr("{}.overrideEnabled".format(textureUiGeo), 1)
    # mc.setAttr("{}.overrideDisplayType".format(textureUiGeo), 2)
    # mc.setAttr("{}.selectionChildHighlighting".format(textureUiGeo), 0)
    mc.setAttr('{}.useFrameExtension'.format(texturePlaneUiFile),1)


    pathName = mc.getAttr('{}UiRigPlaneUi{}File.fileTextureName'.format(part,side))
    newPathPlaneUi = pathName.replace('.png','.00.png')
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(texturePlaneUiFile, newPathPlaneUi))
    ## abc XY menu File
    #caFacialModule.generateTitleBarTextureFile(uiRigCtrl = uiRigCtrl , axis = ['X', 'Y'], texturePath = texturePath)


    # add plantTabAttr
    mc.addAttr(ctrl,ln='planeTab',min=0,dv=0,k=1,at='long',max=maxValue)
    mc.connectAttr(ctrl+'.planeTab',texturePlaneUiFile+'.frameExtension',f=1)
    # connect visibility UiRigGeo / PlaneTabGeo
    cond = mc.createNode('condition',n = cond)
    mc.connectAttr(ctrl+'.planeTab',cond+'.firstTerm')
    mc.setAttr(cond+'.colorIfTrueR',1)
    mc.setAttr(cond+'.colorIfFalseR',0)

    # connect PlaneTab attr to planeTab Mdv
    mc.connectAttr(cond+'.outColorR',planeGeo+'.v')
    mc.connectAttr(ctrl+'.planeTab',planeTabGeo+'.v')

    # connect planteTab Mdv to UiRigPma / ShadowPma
    planeTabMdv = mc.createNode('multiplyDivide',n=planeTabMdv)
    mc.connectAttr(ctrl+'.planeTab',planeTabMdv+'.input1X')
    mc.setAttr(planeTabMdv+'.input2X',100)
    mc.connectAttr(planeTabMdv+'.outputX',uiRigPma+'.input1D[2]')
    mc.connectAttr(planeTabMdv+'.outputX',shadowPma+'.input1D[2]')


