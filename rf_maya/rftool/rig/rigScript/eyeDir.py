import maya.cmds as mc
import maya.mel as mm
import core
import rigTools as rt
reload( core )
reload( rt )
import fkRig
reload(fkRig)

cn = core.Node()

class Run(object):
    def __init__(self , eyeMainTrgtTmpJnt   = 'EyeTrgt_TmpJnt' ,
                        eyeTrgtTmpJnt       = '' ,
                        eyeTmpJnt           = '' ,
                        eyeInner            = '' ,
                        eyeOuter            = '' ,
                        eyeLidMainUpr       = '' ,
                        eyeLidMainLwr       = '' ,
                        eyeLidUpr           = ['EyeLidUpr1_L_TmpJnt','EyeLidUpr2_L_TmpJnt','EyeLidUpr3_L_TmpJnt'] ,
                        eyeLidLwr           = ['EyeLidLwr1_L_TmpJnt','EyeLidLwr2_L_TmpJnt','EyeLidLwr3_L_TmpJnt'] ,
                        eyeNrb              = '' ,
                        eyeBrowIn           = 'EyeBrowInner_L_TmpJnt',
                        eyeBrowMid          = 'EyeBrowMid_L_TmpJnt',
                        eyeBrowOut          = 'EyeBrowOuter_L_TmpJnt',
                        eyeBrow             = 'EyeBrow_C_TmpJnt',
                        parent              = 'HeadPos_Jnt' ,
                        ctrlGrp             = 'MainCtrl_Grp',
                        stillGrp            = 'Still_Grp',
                        elem                = '' ,
                        side                = 'L' ,
                        clrEyeTrgt          = 'red' ,
                        size                = 1 
                        ):
        # info
        elem = elem.capitalize()
        if side == '':
            side = '_'
        else:
            side = '_{}_'.format(side)

        # create main group
        self.eyeCtrlGrp = cn.createNode('transform' , 'Eye{}Ctrl{}Grp'.format(elem,side))
        mc.parentConstraint(parent , self.eyeCtrlGrp , mo=False)
        mc.scaleConstraint(parent , self.eyeCtrlGrp , mo=False)

        self.eyeStillGrp = cn.createNode('transform', 'EyeStill{}{}Grp'.format(elem , side))

        # create eye joint
        self.eyeAdjJnt = cn.joint( 'EyeAdj{}{}Jnt'.format(elem,side) , eyeTmpJnt )
        self.eyeJnt = cn.joint('Eye{}{}Jnt'.format(elem,side) , eyeTmpJnt)
        self.eyeLidJnt = cn.joint('EyeLid{}{}Jnt'.format(elem,side) , eyeTmpJnt)
        mc.parent(self.eyeJnt , self.eyeAdjJnt)
        mc.parent(self.eyeLidJnt , self.eyeJnt)
        mc.parent(self.eyeAdjJnt , parent)

        # create control
        self.eyeCtrl = rt.createCtrl( 'Eye{}{}Ctrl'.format( elem , side ) , 'sphere' , 'red' )
        self.eyeGmbl = rt.addGimbal( self.eyeCtrl )
        self.eyeZro = rt.addGrp( self.eyeCtrl )
        self.eyeAim = rt.addGrp( self.eyeCtrl , 'Aim' )
        self.eyeZro.snap( self.eyeJnt )
        self.eyeCtrl.addAttr( 'eyelidUprFollow' , 0 , 1 )
        self.eyeCtrl.addAttr( 'eyelidLwrFollow' , 0 , 1 )
        self.eyeCtrl.attr('eyelidUprFollow').value = 0.3
        self.eyeCtrl.attr('eyelidLwrFollow').value = 0.2

        # check main eye target
        if not mc.objExists('EyeMainTrgt{}_Ctrl'.format(elem)):
            self.eyeMainTrgtCtrl = rt.createCtrl( 'EyeMainTrgt{}_Ctrl'.format( elem ) , 'capsule' , 'yellow' )
            self.eyeMainTrgtZro = rt.addGrp( self.eyeMainTrgtCtrl )
            self.eyeMainTrgtZro.snap( eyeMainTrgtTmpJnt )
            mc.parent(self.eyeMainTrgtZro , self.eyeCtrlGrp)
            rt.localWorld( self.eyeMainTrgtCtrl , ctrlGrp , self.eyeCtrlGrp , self.eyeMainTrgtZro , 'parent' )

        else:
            self.eyeMainTrgtCtrl = core.Dag('EyeMainTrgt{}_Ctrl'.format(elem))
            self.eyeMainTrgtZro = core.Dag('EyeMainTrgt{}CtrlZro_Grp'.format(elem))


        self.eyeTrgtCtrl = rt.createCtrl( 'EyeTrgt{}{}Ctrl'.format( elem , side ) , 'circle' , clrEyeTrgt )
        self.eyeTrgtZro = rt.addGrp( self.eyeTrgtCtrl )
        self.eyeTrgtZro.snap( eyeTrgtTmpJnt )


        # rig process
        mc.parentConstraint(self.eyeGmbl , self.eyeJnt , mo=False)
        mc.scaleConstraint(self.eyeGmbl , self.eyeJnt , mo=False)

        self.eyeJnt.attr('ssc').value = 0
        self.eyeLidJnt.attr('ssc').value = 0
        self.eyeLidJnt.attr('drawStyle').value = 2

        mc.aimConstraint(self.eyeTrgtCtrl,self.eyeAim,aim=(0,0,1),u=(0,1,0),wut='objectrotation',wu=(0,1,0),wuo=self.eyeZro,mo=True)

        mc.orientConstraint( parent , self.eyeLidJnt , mo = True )

        #-- Rig Eyelid Follow

        #-- Eye Inner and Outer
        self.eyeInnrCtrl = rt.createCtrl( 'EyeInner{}{}Ctrl'.format( elem , side ) , 'cube' , 'green' )
        self.eyeInnrZro = rt.addGrp(self.eyeInnrCtrl)
        self.eyeInnrOfst = rt.addGrp(self.eyeInnrCtrl , 'Ofst')
        self.eyeInnrZro.snap(eyeInner)

        self.eyeInnrJnt = cn.joint( 'EyeInner{}{}Jnt'.format(elem,side) , eyeInner )
        mc.parentConstraint(self.eyeInnrCtrl , self.eyeInnrJnt)
        mc.scaleConstraint(self.eyeInnrCtrl , self.eyeInnrJnt)

        self.eyeOtrCtrl = rt.createCtrl( 'EyeOuter{}{}Ctrl'.format( elem , side ) , 'cube' , 'green' )
        self.eyeOtrZro = rt.addGrp(self.eyeOtrCtrl)
        self.eyeOtrOfst = rt.addGrp(self.eyeOtrCtrl , 'Ofst')
        self.eyeOtrZro.snap(eyeOuter)

        self.eyeOtrJnt = cn.joint( 'EyeOuter{}{}Jnt'.format(elem,side) , eyeInner )
        mc.parentConstraint(self.eyeOtrCtrl , self.eyeOtrJnt)
        mc.scaleConstraint(self.eyeOtrCtrl , self.eyeOtrJnt)

        mc.parent(self.eyeInnrJnt , self.eyeOtrJnt , self.eyeLidJnt)

        #-- Eyelid Upr
        self.eyeLidUprCtrl = rt.createCtrl( 'EyeLidUpr{}{}Ctrl'.format( elem , side ) , 'circle' , 'red' )
        self.eyeLidUprZro = rt.addGrp(self.eyeLidUprCtrl)
        self.eyeLidUprOfst = rt.addGrp(self.eyeLidUprCtrl , 'Ofst')
        self.eyeLidUprNorm = cn.createNode('transform' , 'EyeLidUprNorm{}{}Grp'.format( elem , side ))
        self.eyeLidUprNorm.snap( self.eyeJnt )
        self.eyeLidUprZro.snap( self.eyeJnt )
        self.eyeLidUprZro.snapOrient(eyeLidMainUpr)
        mc.parentConstraint(self.eyeLidJnt , self.eyeLidUprZro , mo=True)
        mc.scaleConstraint(self.eyeLidJnt , self.eyeLidUprZro , mo=True)
        mc.parent(self.eyeLidUprNorm , self.eyeStillGrp)

        self.eyeLidUprCtrlShape = core.Dag(self.eyeLidUprCtrl.shape)
        self.eyeLidUprCtrlShape.addAttr('detailCtrl' , 0 , 1)
        self.eyeLidUprCtrlShape.attr('detailCtrl').value = 1

        # rig main eyelid upr follow
        self.eyeLidUprFollowMdv = cn.createNode('multiplyDivide' , 'EyeLidUprFollow{}{}Mdv'.format(elem , side))
        self.eyeCtrl.attr('eyelidUprFollow') >> self.eyeLidUprFollowMdv.attr('i1x')
        self.eyeJnt.attr('rx') >> self.eyeLidUprFollowMdv.attr('i2x')
        self.eyeLidUprFollowMdv.attr('ox') >> self.eyeLidUprOfst.attr('rx')

        for i in range(len(eyeLidUpr)):
            self.eyeLidUprSubJnt = cn.joint( 'EyeLidUpr{}{}{}Jnt'.format(i+1 , elem,side) , eyeLidUpr[i] )
            self.eyeLidUprSubCtrl = rt.createCtrl( 'EyeLidUpr{}{}{}Ctrl'.format(i+1 , elem , side ) , 'circle' , 'pink' )
            self.eyeLidUprSubZro = rt.addGrp(self.eyeLidUprSubCtrl)
            self.eyeLidUprSubZro.snap( eyeLidUpr[i] )

            mc.parent(self.eyeLidUprSubZro , self.eyeLidUprCtrl)
            mc.parent(self.eyeLidUprSubJnt , self.eyeLidJnt)

            self.eyeLidUprCtrlShape.attr('detailCtrl') >> self.eyeLidUprSubZro.attr('v')
            self.eyeLidUprCtrlShape.addAttr('eyelids{}Follow'.format(i+1) , 0 , 1)
            self.eyeLidUprCtrlShape.attr('eyelids{}Follow'.format(i+1)).value = 1
            self.eyeLidUprSubJnt.attr('ssc').value = 0

            self.eyeLidUprSubNorm = cn.createNode('transform' , 'EyeLidUprNorm{}{}{}Grp'.format(i+1 , elem , side))
            self.eyeLidUprSubOrnt = cn.createNode('transform' , 'EyeLidUprOrnt{}{}{}Grp'.format(i+1 , elem , side))
            mc.parent(self.eyeLidUprSubOrnt , self.eyeLidUprSubNorm)
            mc.parent(self.eyeLidUprSubNorm , self.eyeLidUprNorm)
            self.eyeLidUprSubNorm.snap( eyeLidUpr[i]  )

            #-- Constraint parent each follow
            self.headJnt = core.Dag(parent)
            self.cons = core.Dag(mc.parentConstraint(self.eyeLidUprCtrl , self.headJnt , self.eyeLidUprSubZro , mo=True)[0])
            self.cons.attr('interpType').value = 0
            self.followRev = cn.createNode('reverse' , 'EyeLidUprFollow{}{}{}Rev'.format(i+1 , elem , side))
            self.eyeLidUprCtrlShape.attr('eyelids{}Follow'.format(i+1)) >> self.cons.attr('{}W0'.format(self.eyeLidUprCtrl))
            self.eyeLidUprCtrlShape.attr('eyelids{}Follow'.format(i+1)) >> self.followRev.attr('ix')
            self.followRev.attr('ox') >> self.cons.attr('{}W1'.format(self.headJnt))

            #-- Constraint Norm and Ornt to eyeLidUprSubNorm
            mc.normalConstraint(eyeNrb , self.eyeLidUprSubNorm , w = 1 , aim = (0,0,1) , u = (0,1,0) , wu = (0,1,0) , wuo=self.eyeZro)
            mc.geometryConstraint(eyeNrb , self.eyeLidUprSubNorm )
            mc.pointConstraint(self.eyeLidUprSubCtrl , self.eyeLidUprSubNorm , mo=True)
            mc.scaleConstraint(self.eyeLidUprSubCtrl , self.eyeLidUprSubNorm , mo=True)
            mc.orientConstraint(self.eyeLidUprSubCtrl , self.eyeLidUprSubOrnt , mo=True)

            #-- Constraint eyeLidUprSubOrnt to Joint
            mc.parentConstraint(self.eyeLidUprSubOrnt , self.eyeLidUprSubJnt , mo=True)
            mc.scaleConstraint(self.eyeLidUprSubOrnt , self.eyeLidUprSubJnt , mo=True)

        #-- Eyelid Lwr
        self.eyeLidLwrCtrl = rt.createCtrl( 'EyeLidLwr{}{}Ctrl'.format( elem , side ) , 'circle' , 'blue' )
        self.eyeLidLwrZro = rt.addGrp(self.eyeLidLwrCtrl)
        self.eyeLidLwrOfst = rt.addGrp(self.eyeLidLwrCtrl , 'Ofst')
        self.eyeLidLwrNorm = cn.createNode('transform' , 'EyeLidLwrNorm{}{}Grp'.format( elem , side ))
        self.eyeLidLwrNorm.snap( self.eyeJnt )
        self.eyeLidLwrZro.snap( self.eyeJnt )
        self.eyeLidLwrZro.snapOrient(eyeLidMainLwr)
        mc.parentConstraint(self.eyeLidJnt , self.eyeLidLwrZro , mo=True)
        mc.scaleConstraint(self.eyeLidJnt , self.eyeLidLwrZro , mo=True)
        mc.parent(self.eyeLidLwrNorm , self.eyeStillGrp)

        self.eyeLidLwrCtrlShape = core.Dag(self.eyeLidLwrCtrl.shape)
        self.eyeLidLwrCtrlShape.addAttr('detailCtrl' , 0 , 1 )
        self.eyeLidLwrCtrlShape.attr('detailCtrl').value = 1

        # rig main eyelid Lwr follow
        self.eyeLidLwrFollowMdv = cn.createNode('multiplyDivide' , 'EyeLidLwrFollow{}{}Mdv'.format(elem , side))
        self.eyeLidLwrFollowRevRotateMdv = cn.createNode('multiplyDivide' , 'EyeLidLwrFollowRevRotate{}{}Mdv'.format(elem , side))
        self.eyeLidLwrFollowPma = cn.createNode('plusMinusAverage' , 'EyeLidLwrFollow{}{}Pma'.format(elem , side))
        self.eyeCtrl.attr('eyelidLwrFollow') >> self.eyeLidLwrFollowRevRotateMdv.attr('i1x')
        self.eyeLidLwrFollowRevRotateMdv.attr('i2x').value = -1
        self.eyeLidLwrFollowRevRotateMdv.attr('ox') >> self.eyeLidLwrFollowMdv.attr('i1x')
        self.eyeJnt.attr('rx') >> self.eyeLidLwrFollowMdv.attr('i2x')
        self.eyeLidLwrFollowMdv.attr('ox') >> self.eyeLidLwrFollowPma.attr('input1D[0]')
        self.eyeLidLwrFollowPma.attr('o1') >> self.eyeLidLwrOfst.attr('rx')

        for i in range(len(eyeLidLwr)):
            self.eyeLidLwrSubJnt = cn.joint( 'EyeLidLwr{}{}{}Jnt'.format(i+1 , elem,side) , eyeLidLwr[i] )
            self.eyeLidLwrSubCtrl = rt.createCtrl( 'EyeLidLwr{}{}{}Ctrl'.format(i+1 , elem , side ) , 'circle' , 'cyan' )
            self.eyeLidLwrSubZro = rt.addGrp(self.eyeLidLwrSubCtrl)
            self.eyeLidLwrSubZro.snap( eyeLidLwr[i] )

            mc.parent(self.eyeLidLwrSubZro , self.eyeLidLwrCtrl)
            mc.parent(self.eyeLidLwrSubJnt , self.eyeLidJnt)

            self.eyeLidLwrCtrlShape.attr('detailCtrl') >> self.eyeLidLwrSubZro.attr('v')
            self.eyeLidLwrCtrlShape.addAttr('eyelids{}Follow'.format(i+1) , 0 , 1)
            self.eyeLidLwrCtrlShape.attr('eyelids{}Follow'.format(i+1)).value = 1
            self.eyeLidLwrSubJnt.attr('ssc').value = 0

            self.eyeLidLwrSubNorm = cn.createNode('transform' , 'EyeLidLwrNorm{}{}{}Grp'.format(i+1 , elem , side))
            self.eyeLidLwrSubOrnt = cn.createNode('transform' , 'EyeLidLwrOrnt{}{}{}Grp'.format(i+1 , elem , side))
            mc.parent(self.eyeLidLwrSubOrnt , self.eyeLidLwrSubNorm)
            mc.parent(self.eyeLidLwrSubNorm , self.eyeLidLwrNorm)
            self.eyeLidLwrSubNorm.snap( eyeLidLwr[i]  )

            #-- Constraint parent each follow
            self.headJnt = core.Dag(parent)
            self.cons = core.Dag(mc.parentConstraint(self.eyeLidLwrCtrl , self.headJnt , self.eyeLidLwrSubZro , mo=True)[0])
            self.cons.attr('interpType').value = 0
            self.followRev = cn.createNode('reverse' , 'EyeLidLwrFollow{}{}{}Rev'.format(i+1 , elem , side))
            self.eyeLidLwrCtrlShape.attr('eyelids{}Follow'.format(i+1)) >> self.cons.attr('{}W0'.format(self.eyeLidLwrCtrl))
            self.eyeLidLwrCtrlShape.attr('eyelids{}Follow'.format(i+1)) >> self.followRev.attr('ix')
            self.followRev.attr('ox') >> self.cons.attr('{}W1'.format(self.headJnt))

            #-- Constraint Norm and Ornt to eyeLidLwrSubNorm
            mc.normalConstraint(eyeNrb , self.eyeLidLwrSubNorm , w = 1 , aim = (0,0,1) , u = (0,-1,0) , wu = (0,1,0) , wuo=self.eyeZro)
            mc.geometryConstraint(eyeNrb , self.eyeLidLwrSubNorm )
            mc.pointConstraint(self.eyeLidLwrSubCtrl , self.eyeLidLwrSubNorm , mo=True)
            mc.scaleConstraint(self.eyeLidLwrSubCtrl , self.eyeLidLwrSubNorm , mo=True)
            mc.orientConstraint(self.eyeLidLwrSubCtrl , self.eyeLidLwrSubOrnt , mo=True)

            #-- Constraint eyeLidLwrSubOrnt to Joint
            mc.parentConstraint(self.eyeLidLwrSubOrnt , self.eyeLidLwrSubJnt , mo=True)
            mc.scaleConstraint(self.eyeLidLwrSubOrnt , self.eyeLidLwrSubJnt , mo=True)

        if mc.objExists(eyeBrowIn):
            #-- Rig Eyebrow
            self.eyebrowInJnt = cn.joint('EyeBrowInner{}{}Jnt'.format(elem,side) , eyeBrowIn)
            self.eyebrowInCtrl = rt.createCtrl( 'EyeBrowInner{}{}Ctrl'.format( elem , side ) , 'circle' , 'red' )
            self.eyebrowInZro = rt.addGrp(self.eyebrowInCtrl)
            self.eyebrowInZro.snap(eyeBrowIn)
            self.eyebrowInJnt.attr('ssc').value = 0
            mc.parentConstraint(self.eyebrowInCtrl , self.eyebrowInJnt , mo=True)
            mc.scaleConstraint(self.eyebrowInCtrl , self.eyebrowInJnt , mo=True)

            self.eyebrowMidJnt = cn.joint('EyeBrowMid{}{}Jnt'.format(elem,side) , eyeBrowMid)
            self.eyebrowMidCtrl = rt.createCtrl( 'EyeBrowMid{}{}Ctrl'.format( elem , side ) , 'circle' , 'red' )
            self.eyebrowMidZro = rt.addGrp(self.eyebrowMidCtrl)
            self.eyebrowMidZro.snap(eyeBrowMid)
            self.eyebrowMidJnt.attr('ssc').value = 0
            mc.parentConstraint(self.eyebrowMidCtrl , self.eyebrowMidJnt , mo=True)
            mc.scaleConstraint(self.eyebrowMidCtrl , self.eyebrowMidJnt , mo=True)

            self.eyebrowOutJnt = cn.joint('EyeBrowOuter{}{}Jnt'.format(elem,side) , eyeBrowOut)
            self.eyebrowOutCtrl = rt.createCtrl( 'EyeBrowOuter{}{}Ctrl'.format( elem , side ) , 'circle' , 'red' )
            self.eyebrowOutZro = rt.addGrp(self.eyebrowOutCtrl)
            self.eyebrowOutZro.snap(eyeBrowOut)
            self.eyebrowOutJnt.attr('ssc').value = 0
            mc.parentConstraint(self.eyebrowOutCtrl , self.eyebrowOutJnt , mo=True)
            mc.scaleConstraint(self.eyebrowOutCtrl , self.eyebrowOutJnt , mo=True)

            self.eyebrowAllCtrl = rt.createCtrl( 'EyeBrowAll{}{}Ctrl'.format( elem , side ) , 'circle' , 'yellow' )
            self.eyebrowAllZro = rt.addGrp(self.eyebrowAllCtrl)
            self.eyebrowAllOfst = rt.addGrp(self.eyebrowAllCtrl , 'Ofst')
            self.eyebrowAllZro.snap(eyeBrowMid)
            self.eyebrowAllCtrlShape = (self.eyebrowAllCtrl.shape)
            mc.xform('{}.cv[0:]'.format(self.eyebrowAllCtrlShape) , s = (1.5,1.5,1.5))
            mc.parent(self.eyebrowInZro , self.eyebrowMidZro , self.eyebrowOutZro , self.eyebrowAllCtrl)
            mc.parentConstraint(parent , self.eyebrowAllZro , mo=True)
            mc.scaleConstraint(parent , self.eyebrowAllZro , mo=True)

            if not mc.objExists('EyeBrow_C_Jnt'):
                self.eyebrowJnt = cn.joint('EyeBrow_C_Jnt', eyeBrow)
                self.eyebrowCtrl = rt.createCtrl( 'EyeBrow{}_C_Ctrl'.format( elem  ) , 'circle' , 'red' )
                self.eyebrowZro = rt.addGrp(self.eyebrowCtrl)
                self.eyebrowOfst = rt.addGrp(self.eyebrowCtrl , 'Ofst')
                self.eyebrowZro.snap(eyeBrow)
                self.eyebrowJnt.attr('ssc').value = 0
                mc.parentConstraint(self.eyebrowCtrl , self.eyebrowJnt , mo=True)
                mc.scaleConstraint(self.eyebrowCtrl , self.eyebrowJnt , mo=True)
                mc.parentConstraint(parent , self.eyebrowZro , mo=True)
                mc.scaleConstraint(parent , self.eyebrowZro , mo=True)
                mc.parent(self.eyebrowJnt , parent)
                mc.parent(self.eyebrowZro , ctrlGrp)


            mc.parent(self.eyebrowInJnt , self.eyebrowMidJnt , self.eyebrowOutJnt , parent)
            mc.parent(self.eyebrowAllZro , ctrlGrp)


        # adjust hierachy
        mc.parent(self.eyeLidUprZro , self.eyeLidLwrZro ,self.eyeInnrZro , self.eyeOtrZro ,self.eyeZro , self.eyeCtrlGrp)
        mc.parent(self.eyeTrgtZro , self.eyeMainTrgtCtrl)
        mc.parent(self.eyeCtrlGrp , ctrlGrp)
        mc.parent(eyeNrb , self.eyeStillGrp)
        mc.parent(self.eyeStillGrp , stillGrp)

        self.nrbSkc = mc.skinCluster( self.eyeLidJnt , eyeNrb , tsb = True , mi = 1 , n = 'EyeNrb{}{}Skc'.format( elem , side ))

        #-- Cleanup Eye
        for grp in ( self.eyeCtrlGrp,self.eyeZro,self.eyeTrgtZro,self.eyeMainTrgtZro,self.eyeLidUprZro,self.eyeLidLwrZro,self.eyeLidUprOfst,self.eyeLidLwrOfst,self.eyeInnrZro,self.eyeOtrZro ) :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for ctrlLid in ( self.eyeLidUprCtrl , self.eyeLidLwrCtrl ) :
            ctrlLid.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'sx' , 'sy' , 'sz' , 'v' )

        for ctrl in ( self.eyeGmbl , self.eyeTrgtCtrl , self.eyeMainTrgtCtrl ) :
            ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        self.eyeCtrl.lockHideAttrs( 'v' )

        #-- connect facial
        if mc.objExists('lipCnrSubRevEyeliddn{}Mdv'.format(side)):
            self.lipCnr = core.Dag('lipCnrSubRevEyeliddn{}Mdv'.format(side))
            self.lipCnr.attr('ox') >> self.eyeLidLwrFollowPma.attr('input1D[1]')

        if 'L' in side:
            sideEye = 'L'
        elif 'R' in side:
            sideEye = 'R'
        else:
            sideEye = ''

        if mc.objExists('EyeDot{}{}TmpJnt'.format(elem,side)):
            print "# Generate >> EyeDot{}".format(elem)
            EyeDot = fkRig.Run(     name           = 'EyeDot{}'.format(elem),
                                     side           = sideEye ,
                                     tmpJnt         = 'EyeDot{}{}TmpJnt'.format(elem,side)  ,
                                     parent         = self.eyeJnt ,
                                     ctrlGrp        = self.eyeCtrlGrp , 
                                     shape          = 'circle' ,
                                     color          = 'cyan' ,
                                     size           = size   )


        if mc.objExists('Iris{}{}TmpJnt'.format(elem,side)):
            print "# Generate >> Iris{} Rig".format(elem)
            IrisRigObj = fkRig.Run( name           = 'Iris{}'.format(elem),
                                     side           = sideEye ,
                                     tmpJnt         = 'Iris{}{}TmpJnt'.format(elem,side)   ,
                                     parent         = self.eyeJnt ,
                                     ctrlGrp        = self.eyeCtrlGrp , 
                                     shape          = 'circle' ,
                                     color          = 'cyan' ,
                                     size           = size   )


        if mc.objExists('Pupil{}{}TmpJnt'.format(elem,side)):
            print "# Generate >> Pupil{} Rig".format(elem)
            PupilRigObj = fkRig.Run(  name       = 'Pupil{}'.format(elem) ,
                                        tmpJnt     = 'Pupil{}{}TmpJnt'.format(elem,side) ,
                                        parent     = IrisRigObj.jnt  ,
                                        ctrlGrp    =  self.eyeCtrlGrp ,
                                        shape      = 'circle' ,
                                        side       = sideEye ,
                                        color          = 'cyan' ,
                                        size       = size  
                                        )



        mc.select(cl=True)



# Example
# Run(eyeMainTrgtTmpJnt   = 'EyeTrgt_TmpJnt' ,
#     eyeTrgtTmpJnt       = 'EyeTrgt_L_TmpJnt' ,
#     eyeTmpJnt           = 'Eye_L_TmpJnt' ,
#     eyeInner            = 'EyeInner_L_TmpJnt' ,
#     eyeOuter            = 'EyeOuter_L_TmpJnt' ,
#     eyeLidMainUpr       = 'EyeLidUpr_L_TmpJnt' ,
#     eyeLidMainLwr       = 'EyeLidLwr_L_TmpJnt' ,
#     eyeLidUpr           = ['EyeLidUpr1_L_TmpJnt','EyeLidUpr2_L_TmpJnt','EyeLidUpr3_L_TmpJnt',
#                             'EyeLidUpr4_L_TmpJnt','EyeLidUpr5_L_TmpJnt','EyeLidUpr6_L_TmpJnt'] ,
#     eyeLidLwr           = ['EyeLidLwr1_L_TmpJnt','EyeLidLwr2_L_TmpJnt','EyeLidLwr3_L_TmpJnt',
#                             'EyeLidLwr4_L_TmpJnt','EyeLidLwr5_L_TmpJnt','EyeLidLwr6_L_TmpJnt'] ,
#     eyeNrb              = 'EyeLidSurface_L_Nrb' ,
#     eyeBrowIn           = 'EyeBrowInner_L_TmpJnt',
#     eyeBrowMid          = 'EyeBrowMid_L_TmpJnt',
#     eyeBrowOut          = 'EyeBrowOuter_L_TmpJnt',
#     eyeBrow             = 'EyeBrow_C_TmpJnt' ,
#     parent              = 'Head_Jnt' ,
#     elem                = '' ,
#     side                = 'L' ,
#     clrEyeTrgt          = 'red' ,
#     size                = 1 
#     )