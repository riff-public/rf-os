import maya.cmds as mc
import maya.mel as mm
import core
import rigTools as rt
import ribbonRig
import ctrlRig as cr
reload( core )
reload( rt )
reload( ribbonRig )
reload(cr)
import fkRig
reload(fkRig)


cn = core.Node()

# -------------------------------------------------------------------------------------------------------------
#
#  NECK RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , neckTmpJnt = 'Neck_TmpJnt' ,
                         headTmpJnt = 'Head_TmpJnt' ,
                         parent     = 'Spine5_Jnt' ,
                         ctrlGrp    = 'Ctrl_Grp' ,
                         jntGrp     = 'Jnt_Grp' ,
                         skinGrp    = 'Skin_Grp' ,
                         stillGrp   = 'Still_Grp' ,
                         elem       = '' ,
                         side       = '' ,
                         ribbon     = True ,
                         hi         = True ,
                         head       = True ,
                         axis       = 'y' ,
                         size       = 1
                 ):

        ##-- Info
        elem = elem.capitalize()

        if side == '' :
            sideRbn = ''
            side = '_'
        else :
            sideRbn = '{}'.format(side)
            side = '_{}_'.format(side)

        self.neckFk =   fkRig.Run   (name           = 'Neck{}'.format( elem  ) ,
                                     side           = sideRbn ,
                                     tmpJnt         = neckTmpJnt ,
                                     parent         = parent ,
                                     ctrlGrp        = ctrlGrp , 
                                     shape          = 'circle' ,
                                     jnt_ctrl       = 0,
                                     size           = size ,
                                     color          = 'red',
                                     localWorld     = 1,
                                     defaultWorld   = 0,
                                     constraint     = 1,
                                     jnt            = 1,
                                     dtl            = 0,
                                     dtlShape       = 'cube',
                                     dtlColor       = 'blue')

        self.neckCtrlGrp    = self.neckFk.ctrlGrp
        self.neckCtrl       = self.neckFk.ctrl
        self.neckZro        = self.neckFk.zro 
        self.neckOfst       = self.neckFk.ofst
        self.neckDrv        = self.neckFk.drv
        self.neckInv        = self.neckFk.inv
        self.neckPars       = self.neckFk.pars
        self.neckGmbl       = self.neckFk.gmbl
        self.neckJnt        = self.neckFk.jnt

        #-- AddneckEndJnt
        self.neckEndJnt = cn.joint( 'NeckEnd{}{}Jnt'.format( elem , side ) , headTmpJnt )
        self.neckEndJnt.parent(self.neckJnt)

        #-- Adjust Rotate Order
        for obj in ( self.neckCtrl , self.neckJnt , self.neckEndJnt ) :
            obj.setRotateOrder( 'xzy' )    

        #-- Rig process
        rt.addFkStretch( self.neckCtrl , self.neckEndJnt , axis )

        #-- NonRoll
        neckNonRollJnt = rt.addNonRollJnt( 'Neck' , '' , self.neckJnt , self.neckEndJnt , parent , '{}+'.format(axis) )
        self.neckNonRollJntGrp = neckNonRollJnt['jntGrp']
        self.neckNonRollRootNr = neckNonRollJnt['rootNr']
        self.neckNonRollRootNrZro = neckNonRollJnt['rootNrZro']
        self.neckNonRollEndNr = neckNonRollJnt['endNr']
        self.neckNonRollIkhNr = neckNonRollJnt['ikhNr']
        self.neckNonRollIkhNrZro = neckNonRollJnt['ikhNrZro']

        #-- Adjust Hierarchy
        self.neckJntGrp = cn.createNode( 'transform' , 'Neck{}Jnt{}Grp'.format( elem , side ))
        self.neckJntGrp.snap( parent )
        self.neckJntGrp.parent(jntGrp)
        self.neckNonRollJntGrp.parent(self.neckJntGrp)

        #-- Cleanup
        for obj in ( self.neckCtrlGrp , self.neckZro ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )


        #-- Create Ribbon
        if head :
            self.headJnt = cn.joint( 'Head{}{}Jnt'.format( elem , side ) , headTmpJnt )
            mc.parent( self.headJnt , self.neckEndJnt )
            posi = self.headJnt
        else :
            posi = self.neckEndJnt

        distRbnNeck= rt.distance( self.headJnt , self.neckEndJnt )

        if ribbon == True :
            if hi == True:
                rbn = ribbonRig.Run  (  name        = 'Neck{}'.format(elem) , 
                                        posiA       = self.neckJnt, 
                                        posiB       = posi , 
                                        axis        = '{}+'.format(axis) , 
                                        side        = sideRbn,
                                        numJnts     = 5 ,
                                        nurbValue   = 5 ,  
                                        )

            else:

                rbn = ribbonRig.Run  (  name        = 'Neck{}'.format(elem) , 
                                        posiA       = self.neckJnt, 
                                        posiB       = posi , 
                                        axis        = '{}+'.format(axis) , 
                                        side        = sideRbn,
                                        numJnts     = 1 ,
                                        nurbValue   = 3 ,  
                                        )

            #-- Twist Distribute
            self.rbnCtrl = core.Dag(rbn['rbnCtrl'])
            self.rbnCtrlShape = core.Dag(self.rbnCtrl.shape)

            #print self.rbnCtrl, self.rbnCtrlShape

            if head :
                self.headJnt.attr('r{}'.format(axis)) >> self.rbnCtrlShape.attr( 'endAbsoluteTwist' )

            self.rbnCtrl.attr('autoTwist').value = 1

            #-- Adjust Hierarchy
            mc.parent( rbn['rbnCtrlGrp'] , self.neckCtrlGrp )
            mc.parent( rbn['rbnJntGrp'] , self.neckJntGrp )
            mc.parent( rbn['rbnSkinGrp'] , skinGrp )

            if hi == True :
                mc.parent( rbn['rbnStillGrp'] , stillGrp )
            else:
                mc.parent( rbn['rbnStillGrp'] , stillGrp )

        mc.select( cl = True )
