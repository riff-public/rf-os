import string
import sys, os
import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
import json
from rf_utils import admin
from rftool.rig.rigScript import core 
reload(core)
cn = core.Node()

from utaTools.utapy import utaCore
reload(utaCore)

from ncscript2.pymel import bshTools
reload(bshTools)
import pymel.core as pmc

from ncscript2.pymel import rivetTools
from ncscript2.pymel import core

from utaTools.utapy import utaCore, DeleteGrp
reload(utaCore)
reload(DeleteGrp)
from lpRig import rigTools as lrr
reload(lrr)

from utils.crvScript import ctrlData
reload(ctrlData)
from rf_maya.rftool.rig.rigScript import crvFunc
reload(crvFunc)

#path project config
rootScript = os.environ['RFSCRIPT']
# Path ---------------------------------------
corePath = '%s/core' % os.environ.get('RFSCRIPT')
path = '{}/rf_maya/rftool/rig/'.format(corePath)


def generateBlinkFollowIris(ctrl = 'BlinkUi_R_Ctrl', BlinkCorPlace2DTexture = 'BlinkCorPlace2DTexture', IrisCorePlace2DTexture = 'IrisCor_L_place2dTexture', *args):

    ## split name
    name, side, lastName = cn.splitName(sels = [ctrl])

    elem = 'followIris'
    irisFollowAttr = cn.addAttrCtrl (ctrl = ctrl, elem = [elem], min = 0, max = 1, at = 'long')
    mc.setAttr('{}.{}'.format(ctrl, elem), 1)

    ## Get Attribute connect
    sourceAttrBlinkCorP2DTLists, targetAttrBlinkCorP2DTLists = cn.checkConnectLists2(obj = BlinkCorPlace2DTexture)
    sourceAttrIrisCorP2DTLists, targetAttrIrisCorP2DTLists = cn.checkConnectLists2(obj = IrisCorePlace2DTexture)

    ## remove 'mirror' for attr
    for eachBlink in sourceAttrBlinkCorP2DTLists:
        if 'mirror' in eachBlink:
            sourceAttrBlinkCorP2DTLists.remove(eachBlink)
    for eachIris in sourceAttrIrisCorP2DTLists:
        if 'mirror' in eachIris:
            sourceAttrIrisCorP2DTLists.remove(eachIris)
    for eachBlinkCorP2DT in targetAttrBlinkCorP2DTLists:
        if 'mirror' in eachBlinkCorP2DT:
            targetAttrBlinkCorP2DTLists.remove(eachBlinkCorP2DT)
    for eachIrisCorP2DT in targetAttrIrisCorP2DTLists:
        if 'mirror' in eachIrisCorP2DT:
            targetAttrIrisCorP2DTLists.remove(eachIrisCorP2DT)

    ## generate node for follow 
    for i in range(len(targetAttrBlinkCorP2DTLists)):
        blinkAttr = targetAttrBlinkCorP2DTLists[i].split('.')[-1]
        blinkNameSp = targetAttrBlinkCorP2DTLists[i].split('.')[0]
        blinkNeme, blinkSide, linkLastName = cn.splitName(sels = [blinkNameSp])

        if 'coverage' in blinkAttr:

            blinkBcrNode = mc.createNode('blendColors', n = '{}{}{}Bcr'.format(blinkNeme, blinkAttr,  blinkSide))
            mc.connectAttr('{}.{}'.format(ctrl, elem), '{}.blender'.format(blinkBcrNode))
            blinkMdvNode = mc.createNode('multiplyDivide', n = '{}{}{}Mdv'.format(blinkNeme, blinkAttr,  blinkSide))
            blinkPmaNode = mc.createNode('plusMinusAverage',  n = '{}{}{}Pma'.format(blinkNeme, blinkAttr,  blinkSide))
            mc.setAttr('{}.input2X'.format(blinkMdvNode), -1)
            mc.connectAttr('{}.{}'.format(ctrl, elem), '{}.input1X'.format(blinkMdvNode))
            mc.connectAttr('{}.outputX'.format(blinkMdvNode), '{}.color2R'.format(blinkBcrNode))
            mc.connectAttr(sourceAttrIrisCorP2DTLists[i], '{}.color1R'.format(blinkBcrNode))
            mc.connectAttr(sourceAttrBlinkCorP2DTLists[i], '{}.input2D[0].input2Dx '.format(blinkPmaNode))
            mc.connectAttr('{}.output.outputR'.format(blinkBcrNode), '{}.input2D[1].input2Dx '.format(blinkPmaNode))
            mc.connectAttr('{}.outputX'.format(blinkMdvNode), '{}.input2D[2].input2Dx '.format(blinkPmaNode))

            mc.connectAttr('{}.output2Dx'.format(blinkPmaNode),  targetAttrBlinkCorP2DTLists[i], f = True) 

        elif 'offset' in blinkAttr:
            blinkPmaNode = mc.createNode('plusMinusAverage',  n = '{}{}{}Pma'.format(blinkNeme, blinkAttr,  blinkSide))
            irisMdvNode = mc.createNode('multiplyDivide', n = '{}{}{}Mdv'.format(blinkNeme, blinkAttr, blinkSide))
            mc.connectAttr('{}.{}'.format(ctrl, elem), '{}.input2X'.format(irisMdvNode))
            mc.connectAttr(sourceAttrIrisCorP2DTLists[i], '{}.input1X'.format(irisMdvNode))
            mc.connectAttr('{}.outputX'.format(irisMdvNode), '{}.input2D[1].input2Dx'.format(blinkPmaNode))
            mc.connectAttr(sourceAttrBlinkCorP2DTLists[i], '{}.input2D[0].input2Dx'.format(blinkPmaNode))

            mc.connectAttr('{}.output2Dx'.format(blinkPmaNode), targetAttrBlinkCorP2DTLists[i], f = True)


        elif 'translateFrame' in blinkAttr:
            blinkPmaNode = mc.createNode('plusMinusAverage',  n = '{}{}{}Pma'.format(blinkNeme, blinkAttr,  blinkSide))
            irisMdvNode = mc.createNode('multiplyDivide', n = '{}{}{}Mdv'.format(blinkNeme, blinkAttr, blinkSide))
            mc.connectAttr('{}.{}'.format(ctrl, elem), '{}.input2X'.format(irisMdvNode))
            mc.connectAttr(sourceAttrIrisCorP2DTLists[i], '{}.input1X'.format(irisMdvNode))
            mc.connectAttr('{}.outputX'.format(irisMdvNode), '{}.input2D[1].input2Dx'.format(blinkPmaNode))
            mc.connectAttr(sourceAttrBlinkCorP2DTLists[i], '{}.input2D[0].input2Dx'.format(blinkPmaNode))

            mc.connectAttr('{}.output2Dx'.format(blinkPmaNode), targetAttrBlinkCorP2DTLists[i], f = True)


        elif 'rotateFrame' in blinkAttr:
            blinkPmaNode = mc.createNode('plusMinusAverage',  n = '{}{}{}Pma'.format(blinkNeme, blinkAttr,  blinkSide))
            irisMdvNode = mc.createNode('multiplyDivide', n = '{}{}{}Mdv'.format(blinkNeme, blinkAttr, blinkSide))
            mc.connectAttr('{}.{}'.format(ctrl, elem), '{}.input2X'.format(irisMdvNode))
            mc.connectAttr(sourceAttrIrisCorP2DTLists[i], '{}.input1X'.format(irisMdvNode))
            mc.connectAttr('{}.outputX'.format(irisMdvNode), '{}.input2D[1].input2Dx'.format(blinkPmaNode))
            mc.connectAttr(sourceAttrBlinkCorP2DTLists[i], '{}.input2D[0].input2Dx'.format(blinkPmaNode))

            mc.connectAttr('{}.output2Dx'.format(blinkPmaNode), targetAttrBlinkCorP2DTLists[i], f = True)



def generateSwitchSeqEyeBall(ctrl = 'Eye_L_Ctrl', elem = 'Eye', eyeBallAllLyt = 'EyeBallAll_L_Lyt',*args):

    ## split name
    name, side, lastName = cn.splitName(sels = [ctrl])
    nameAttr = '{}TxrVis'.format(elem)
    if 'Eye' == elem:
        dv = 1
    else:
        dv = 0
    cn.addAttrCtrl (ctrl = ctrl, elem = [nameAttr], min = 0, max = 1, at = 'long', dv = dv)

    # switch seq or eyeBall
    eyeSwitchRev = mc.createNode('reverse', n = '{}SwitchSeqEyeBall{}Rev'.format(name, side))
    # connect switch seq or eyeBall
    if mc.objExists(ctrl):
        mc.connectAttr('{}.{}'.format(ctrl, nameAttr), '{}.inputX'.format(eyeSwitchRev))
        mc.connectAttr('{}.outputX'.format(eyeSwitchRev), '{}.inputs[0].alpha'.format(eyeBallAllLyt))
        # mc.connectAttr('{}.outputX'.format(eyeSwitchRev), '{}.inputs[1].alpha'.format(eyeBallAllLyt))

    irisBlinkGrp = ['{}UiRig{}{}Grp'.format('Iris' , lastName, side), '{}UiRig{}{}Grp'.format('Blink' , lastName, side)]
    for grp in irisBlinkGrp:
        if mc.objExists(grp):
            mc.connectAttr('{}.{}'.format(ctrl, nameAttr), '{}.v'.format(grp))


def generateSpecular(ctrl = 'Specular_R_Ctrl', elem = 'EyeBall', EyeBallAllLyt = 'EyeBallAll_R_Lyt', texturePath = 'P:/CA/asset/work/char/GB/texture/main', *args):

    ## split name
    name, side, lastName = cn.splitName(sels = [ctrl])

    # Get the list of all files and directories
    dir_list = os.listdir(texturePath + '/' + elem)
    for each in dir_list:
        if elem.split('Ball')[0] == 'Eye':
            if 'SpecularCor' in each:
                SpecularCorPath = texturePath + '/' + elem + '/' + 'SpecularCor.png'
        elif elem.split('Ball')[0] == 'Mouth':
            if 'TeethDnCor' in each:
                SpecularCorPath = texturePath + '/' + elem + '/' + 'TeethDnCor.png'

    ## genetate file texture
    SpecularCorFile , SpecularCorPlace2DTexture = cn.generateFilePlace2DTexture(name = name + 'Cor', side = side, elem = elem, node = 'file')

    ## connect attribute
    connectTRSFacial2( ctrl = ctrl, elem = elem, place2dTexture = [SpecularCorPlace2DTexture], positionDefault = [0, 0])

    ## connect eyeBall Lyt to eyeBallAll lyt
    mc.connectAttr('{}.outColor'.format(SpecularCorFile), '{}.inputs[1].color'.format(EyeBallAllLyt))
    mc.connectAttr('{}.outAlpha'.format(SpecularCorFile),'{}.inputs[1].alpha'.format(EyeBallAllLyt))

    ## assign file path
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(SpecularCorFile, SpecularCorPath))

    ## mirrorU  specular for R side
    generateAttributeForUser(ctrl = ctrl, nodeName = [SpecularCorPlace2DTexture], attr = ['mirrorU'], shape = False)

    ## locck attributre
    cn.lockAttr(listObj = [ctrl], attrs = ['tz', 'rx', 'ry', 'sz'],lock = True, keyable = False)

    return SpecularCorFile, SpecularCorPlace2DTexture


def generateSpecularIrisAlpha(ctrl = 'Specular_R_Ctrl', elem = 'EyeBall', irisAlphaSeq = 'IrisUiRigAlphaEyeBall_R_File', EyeBallAllLyt = 'EyeBallAll_R_Lyt', texturePath = 'P:/CA/asset/work/char/GB/texture/main', *args):

    ## split name
    name, side, lastName = cn.splitName(sels = [ctrl])

    # Get the list of all files and directories
    dir_list = os.listdir(texturePath + '/' + elem)
    for each in dir_list:
        if elem.split('Ball')[0] == 'Eye':
            nameCor = name.split('UiRig')[0]
            if '{}Cor'.format(nameCor) in each:
                SpecularCorPath = texturePath + '/' + elem + '/' + '{}Cor.png'.format(nameCor)
        elif elem.split('Ball')[0] == 'Mouth':
            nameCor = name.split('UiRig')[0]
            if '{}Cor'.format(nameCor) in each:
                SpecularCorPath = texturePath + '/' + elem + '/' + '{}Cor.png'.format(nameCor)
    ## genetate file texture
    SpecularCorFile , SpecularCorPlace2DTexture = cn.generateFilePlace2DTexture(name = name + 'Cor', side = side, elem = elem, node = 'file')

    ## connect attribute
    connectTRSFacial2( ctrl = ctrl, elem = elem, place2dTexture = [SpecularCorPlace2DTexture], positionDefault = [0, 0])

    ## connect eyeBall Lyt to eyeBallAll lyt
    specularLyt = mc.createNode('layeredTexture', n='{}{}Alpha{}Lyt'.format(name, elem, side))
    mc.connectAttr('{}.outColor'.format(SpecularCorFile), '{}.inputs[0].color'.format(specularLyt))
    mc.connectAttr('{}.outAlpha'.format(irisAlphaSeq),'{}.inputs[0].alpha'.format(specularLyt))

    idxOrder = utaCore.checkIndexOrderLayerTexture(layterTexture = EyeBallAllLyt)
    mc.connectAttr('{}.outColor'.format(specularLyt), '{}.inputs[{}].color'.format(EyeBallAllLyt, idxOrder + 1))    
    mc.connectAttr('{}.outAlpha'.format(SpecularCorFile),'{}.inputs[{}].alpha'.format(EyeBallAllLyt, idxOrder + 1))

    mc.setAttr('{}.inputs[{}].blendMode'.format(EyeBallAllLyt, idxOrder + 1), 4)

    # mc.connectAttr('{}.outAlpha'.format(SpecularCorFile),'{}.inputs[1].alpha'.format(EyeBallAllLyt))

    ## assign file path
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(SpecularCorFile, SpecularCorPath))

    ## mirrorU  specular for R side
    generateAttributeForUser(ctrl = ctrl, nodeName = [SpecularCorPlace2DTexture], attr = ['mirrorU'], shape = False)

    ## locck attributre
    cn.lockAttr(listObj = [ctrl], attrs = ['tz', 'rx', 'ry', 'sz'],lock = True, keyable = False)

    # ## clean index order is None
    # utaCore.removeMultiInstanceIndexOrderLayerTexture(layterTexture = [EyeBallAllLyt], removeMultiInstance = True)

    return SpecularCorFile, SpecularCorPlace2DTexture, specularLyt


def generateBlink(ctrl = 'Blink_R_Ctrl', elem = 'EyeBall', EyeBallLyt = 'EyeBall_R_Lyt', texturePath = 'P:/CA/asset/work/char/GB/texture/main', *args):

    ## split name
    name, side, lastName = cn.splitName(sels = [ctrl])

    # Get the list of all files and directories
    dir_list = os.listdir(texturePath + '/' + elem)
    
    for each in dir_list:
        if elem.split('Ball')[0] == 'Eye':
            if 'BlinkCor' in each:
                BlinkCorPath = texturePath + '/' + elem + '/' + 'BlinkCor.png'
        elif elem.split('Ball')[0] == 'Mouth':
            if 'TongueCor' in each:
                BlinkCorPath = texturePath + '/' + elem + '/' + 'TongueCor.png'
    ## genetate file texture
    BlinkCorFile , BlinkCorPlace2DTexture = cn.generateFilePlace2DTexture(name = name + 'Cor', side = side, elem = elem, node = 'file')

    ## connect attribute
    connectTRSFacial2( ctrl = ctrl, elem = elem,  place2dTexture = [BlinkCorPlace2DTexture], positionDefault = [0, 0])

    ## connect eyeBall Lyt to eyeBallAll lyt
    mc.connectAttr('{}.outColor'.format(BlinkCorFile), '{}.inputs[0].color'.format(EyeBallLyt))
    mc.connectAttr('{}.outAlpha'.format(BlinkCorFile),'{}.inputs[0].alpha'.format(EyeBallLyt))

    ## assign file path
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(BlinkCorFile, BlinkCorPath))

    ## mirror U for blink R side
    generateAttributeForUser(ctrl = ctrl, nodeName = [BlinkCorPlace2DTexture], attr = ['mirrorU'], shape = False)

    ## locck attributre
    cn.lockAttr(listObj = [ctrl], attrs = ['tz', 'rx', 'ry', 'sz'],lock = True, keyable = False)

    return BlinkCorFile, BlinkCorPlace2DTexture


def generateIris(uiRigctrl = 'Iris_L_Ctrl', rextureUiRigCtrl = 'EyeTextureUiRig_L_Ctrl', seqNode = '', elemSeq = 'EyeSeq', elem = 'EyeBall', texturePath = 'P:/CA/asset/work/char/GB/texture/main', *args):

    ## split name
    name, side, lastName = cn.splitName(sels = [uiRigctrl])

    ## locck attributre
    cn.lockAttr(listObj = [uiRigctrl], attrs = ['tz', 'rx', 'ry', 'sz'],lock = True, keyable = False)

    # Get the list of all files and directories
    dir_list = os.listdir(texturePath + '/' + elem)
    if elem.split('Ball')[0] == 'Eye':
        irisAlphaSeq = 'IrisAlphaSeq'
        for each in dir_list:
            if 'IrisCor' in each:
                IrisCorPath = texturePath + '/' + elem + '/' + 'IrisCor.png'
            elif 'IrisAlpha' in each:
                IrisAlphaPath = texturePath + '/' + elem + '/' + irisAlphaSeq + '/' + 'IrisAlphaSeq.010.png'
            elif 'PupilCor' in each:
                PupilCorPath = texturePath + '/' + elem + '/' + 'PupilCor.png'
    elif elem.split('Ball')[0] == 'Mouth':
        irisAlphaSeq = 'TeethUpAlphaSeq'
        for each in dir_list:
            if 'TeethUpCor' in each:
                IrisCorPath = texturePath + '/' + elem + '/' + 'TeethUpCor.png'
            elif 'TeethUpAlpha' in each:
                IrisAlphaPath = texturePath + '/' + elem + '/' + irisAlphaSeq + '/' + 'TeethUpAlphaSeq.010.png'
            elif 'PupilCor' in each:
                PupilCorPath = texturePath + '/' + elem + '/' + 'PupilCor.png'


    ## genetate file texture
    IrisCorFile , IrisCorPlace2DTexture = cn.generateFilePlace2DTexture(name = name + 'Cor', side = side, elem = elem, node = 'file')
    IrisAlphaFile , IrisAlphalace2DTexture = cn.generateFilePlace2DTexture(name = name + 'Alpha', side = side, elem = elem, node = 'file')
    PupilCorFile , PupilPlace2DTexture = cn.generateFilePlace2DTexture(name = name + 'PupilCor', side = side, elem = elem, node = 'file')

    ## add Attribute mirror U for user setting
    generateAttributeForUser(ctrl = uiRigctrl, nodeName = [IrisCorPlace2DTexture, IrisAlphalace2DTexture], attr = ['mirrorU'], shape = False)

    ## generate node
    EyeBallLyt = mc.createNode('layeredTexture', n='{}{}{}Lyt'.format(name, elem, side))
    EyeBallAllLyt = '{}All{}Lyt'.format(elemSeq, side)

    ## connect attribute
    connectTRSFacial2( ctrl = uiRigctrl, elem = elem,  place2dTexture = [IrisCorPlace2DTexture, IrisAlphalace2DTexture], positionDefault = [0, 0])

    mc.connectAttr('{}.outColor'.format(IrisCorFile), '{}.inputs[1].color'.format(EyeBallLyt))
    # mc.connectAttr('{}.outAlpha'.format(IrisCorFile),'{}.inputs[1].alpha'.format(EyeBallLyt))
    mc.connectAttr('{}.outColor'.format(IrisAlphaFile), '{}.inputs[2].color'.format(EyeBallLyt))
    mc.connectAttr('{}.outAlpha'.format(IrisAlphaFile),'{}.inputs[2].alpha'.format(EyeBallLyt))

    ## connect eyeBall Lyt to eyeBallAll lyt
    mc.connectAttr('{}.outColor'.format(EyeBallLyt),'{}.inputs[2].color'.format(EyeBallAllLyt))
    mc.connectAttr('{}.outAlpha'.format(IrisAlphaFile),'{}.inputs[2].alpha'.format(EyeBallAllLyt))
    mc.connectAttr('{}.outColor'.format(PupilCorFile),'{}.inputs[3].color'.format(EyeBallAllLyt))
    # mc.connectAttr('{}.outAlpha'.format(PupilCorFile),'{}.inputs[3].alpha'.format(EyeBallAllLyt))

    ## assign file path
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(IrisCorFile, IrisCorPath))
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(IrisAlphaFile, IrisAlphaPath))
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(PupilCorFile, PupilCorPath))

    ## generate itisSeq for eyeBall
    generateIrisAlphaSeq(eyeTextureUiCtrl = rextureUiRigCtrl, eyeSeqNode = seqNode, irisAlphaFileNode = IrisAlphaFile)


    return EyeBallLyt, IrisCorFile, IrisCorPlace2DTexture, IrisAlphaFile, IrisAlphalace2DTexture

def addSeqToCtrl(mainCtrl = '', subCtrl = '', elem ='', subName = '', texturePath = '', *args):
    ## get char name
    charName = utaCore.checkCharacterName()

    ## split name
    if subCtrl:
        name, side, lastName = cn.splitName(sels = [subCtrl])
        name = name.split('UiRig')[0]
        # side = '_{}_'.format(side)
    else:
        name = subName
        side = '_'
    ## prepare part texture
    if elem:
        newPath = texturePath + '/' + elem + '/' + '{}Seq'.format(subName) + '/' +  '{}Seq.001.png'.format(subName)
    else:
        if subName:
            newPath = texturePath + '/' + '{}Seq'.format(subName) + '/' +  '{}_Diffuse_{}Seq.001.png'.format(charName, subName)
        else:
            newPath = texturePath + '/' + '{}Seq'.format(subName) + '/' +  '{}Seq.001.png'.format('Test')

    if not mainCtrl:
        mainCtrl = subCtrl
        if 'Head' in subName:
            elemAttr = '{}Seq'.format(subName)
        else:
            elemAttr = '{}Seq'.format(name)
    else:
        if 'Head' in subName:
            elemAttr = '{}Seq{}'.format(subName, side)
        else:
            elemAttr = '{}Seq{}'.format(name, side)
        nameMainC, sideMainC, lastNameMainC = cn.splitName(sels = [mainCtrl])
        # add main veriety control 
        if not mc.objExists(mainCtrl):
            mainC = utaCore.curveCtrlShape(curveCtrl = 'eye', elem  = nameMainC)
            mainCtrl = mc.rename(mainC, '{}{}{}'.format(mainC, '_', 'Ctrl'))
            utaCore.colors(cor = 'green',ctrl = [mainCtrl])  
            utaCore.lockAttrObj(listObj = [mainCtrl], lock = True,keyable = False)
            mainCGrp = cn.group(obj = mainCtrl, type = 'Zro')

            ## snap position
            mc.delete(mc.parentConstraint('FacialUiRig_Ctrl', mainCGrp, mo = False))
            mc.setAttr('{}.ty'.format(mainCGrp), 20)
            mc.parent(mainCGrp, 'FacialUiRig_Ctrl')

            ## fix scale 
            mc.select('{}.cv[0:56]'.format(mainCtrl))
            mc.scale(0.6, 0.6, 0.6, p= (-0.0007105, 20.000003, 1.187612), r = True)

        else:
            mainCGrp = '{}CtrlZro{}Grp'.format(nameMainC, sideMainC)
    ## Add attribute
    if not mc.objExists('{}.VarietyOptions'.format(mainCtrl)):
        utaCore.attrNameTitle(obj = mainCtrl, ln = 'VarietyOptions')
        utaCore.attrEnum(obj = mainCtrl, ln = 'Variety', en = 'A : B : C : D : E : F')
    cn.addAttrCtrl (ctrl = '{}Shape'.format(mainCtrl) , elem = [elemAttr], min = 1, max = 10, at = 'long', dv = 1)
    # mc.connectAttr('{}.Variety'.format(mainCtrl), '{}Shape.{}'.format(mainCtrl, elemAttr))

    adlNode = mc.createNode('addDoubleLinear',n = '{}Default{}Adl'.format(subName, side))
    utaCore.addAttrCtrl (ctrl = adlNode, elem = ['default'], min = -200, max = 200, at = 'float')
    mc.setAttr( '{}.default'.format(adlNode), 1)

    mc.connectAttr('{}.Variety'.format(mainCtrl), '{}.input2'.format(adlNode))
    mc.connectAttr('{}.default'.format(adlNode), '{}.input1'.format(adlNode))
    mc.connectAttr('{}.output'.format(adlNode), '{}Shape.{}'.format(mainCtrl, elemAttr)) 

    utaCore.lockAttr(listObj = ['{}Shape'.format(mainCtrl)], attrs = [elemAttr],lock = True, keyable = False)

    ## assign file path
    if 'Head' in subName:
        fileNode = '{}Ramp{}PlaneUi_File'.format(subName, subName)
    else:
        if mc.objExists('{}UiRigCorEyeBall{}File'.format(name, side)):
            fileNode = '{}UiRigCorEyeBall{}File'.format(name, side)
        else:
            # fileNode = '{}UiRigCorEyeBall{}File'.format(name, side)
            ## genetate file texture
            fileNode , fileNodeCorPlace2DTexture = cn.generateFilePlace2DTexture(name = name + 'Cor', side = side, elem = subName, node = 'file')
            fileNodeUiLambert, fileNodeUiLambertSG = utaCore.generateMaterial(materialName = 'lambert', name = name, side = side, shortName = 'Lmp')
            mc.connectAttr('{}.outColor'.format(fileNode), '{}.color'.format(fileNodeUiLambert))
            mc.select('{}UiRig{}Geo'.format(name, side))
            utaCore.assignSelectionToShader(fileNodeUiLambert)

    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(fileNode, newPath))
    mc.setAttr("{}.useFrameExtension".format(fileNode), 1)

    mc.connectAttr('{}.{}'.format(mainCtrl, elemAttr), '{}.frameExtension'.format(fileNode), f = True)

 
    mc.select(cl = True)

def rePathTexture(fileNode = 'IrisUiRigPupilCorEyeBall_R_File', texturePath = '', elem = 'EyeBall', fileName = 'PupilCor_R.png', *args):
    ## prepare part texture
    if not 'Seq' in elem:
        newPath = texturePath + '/' + elem + '/' +  fileName
    else:
        newPath = texturePath + '/' + elem + '/' +  '{}Seq.001.png'.format(elem)
    ## apply new path file    
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(fileNode, newPath))


def addSeq(ctrl = '', fileNode = '', elem ='', subName = '', texturePath = '', *args):
    ## split name
    name, side, lastName = cn.splitName(sels = [ctrl])
    name = name.split('UiRig')[0]
    if subName:
        name = subName
    if elem:
        newPath = texturePath + '/' + elem + '/' + '{}Seq'.format(name) + '/' +  '{}Seq.001.png'.format(name)
    else:
        newPath = texturePath + '/' + '{}Seq'.format(name) + '/' +  '{}Seq.001.png'.format(name)

    utaCore.addAttrCtrl (ctrl = ctrl, elem = ['{}Seq'.format(name)], min = 1, max = 10, at = 'long', dv = 1)
    ## assign file path
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(fileNode, newPath))
    mc.setAttr("{}.useFrameExtension".format(fileNode), 1)

    mc.connectAttr('{}.{}'.format(ctrl, '{}Seq'.format(name)), '{}.frameExtension'.format(fileNode), f = True)


def addSeq(ctrl = '', fileNode = '', elem ='', subName = '', texturePath = '', *args):
    if not ctrl:
        objName = utaCore.curveCtrlShape(curveCtrl = 'eye', elem  = 'Variety')
        ctrlName = mc.rename(objName, '{}{}{}'.format(name, '_', 'Ctrl'))
    ## split name
    name, side, lastName = cn.splitName(sels = [ctrl])
    name = name.split('UiRig')[0]
    if subName:
        name = subName
    if elem:
        newPath = texturePath + '/' + elem + '/' + '{}Seq'.format(name) + '/' +  '{}Seq.001.png'.format(name)
    else:
        newPath = texturePath + '/' + '{}Seq'.format(name) + '/' +  '{}Seq.001.png'.format(name)

    utaCore.addAttrCtrl (ctrl = ctrl, elem = ['{}Seq'.format(name)], min = 1, max = 10, at = 'long', dv = 1)
    ## assign file path
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(fileNode, newPath))
    mc.setAttr("{}.useFrameExtension".format(fileNode), 1)

    mc.connectAttr('{}.{}'.format(ctrl, '{}Seq'.format(name)), '{}.frameExtension'.format(fileNode), f = True)

def generateIrisAlphaSeq(eyeTextureUiCtrl = '', eyeSeqNode = '', irisAlphaFileNode = '', *args):

    sourceAttrLists, targetAttrLists = cn.checkConnectLists2(obj = eyeSeqNode)
    mc.setAttr("{}.useFrameExtension".format(irisAlphaFileNode), 1)

    for i in range(len(targetAttrLists)):
        obj = targetAttrLists[i].split('.')[-1]
        if 'frameExtension' in obj:
            mc.connectAttr(sourceAttrLists[i], '{}.frameExtension'.format(irisAlphaFileNode), f = True)

        elif 'frameOffset' in obj:
            mc.connectAttr(sourceAttrLists[i], '{}.frameOffset'.format(irisAlphaFileNode), f = True)


def eyeMaskFacial(ctrl = ['EyeMask_R_Ctrl'], shadowTranslateMdvNode = '', elem = 'Mask', maskSeq = True, whiteLineSeq = True, outLyt = 'eyeTextureOut_R_Lyt', maskShadow = True, whiteLineShadow = False, shadowLyt = '', material = '', seqFile = '', texturePath = 'P:/CA/asset/work/char/GB/texture/main', *args):
    
    if ctrl:
        # Get the list of all files and directories
        dir_list = os.listdir(texturePath)
        for each in dir_list:
            if elem in each:
                EyeMaskPath = texturePath + '/' + 'EyeMask.png'
        for eachCtrl in ctrl:
            ## split name
            name, side, lastName = cn.splitName(sels = [eachCtrl])
            
            ## generate mask for seq file ---------------------------------------------
            if maskSeq:
                ## genetate file texture
                fileNode , place2DTexture = cn.generateFilePlace2DTexture(name = name , side = side, elem = elem, node = 'file')
                mc.setAttr('{}.repeatV'.format(place2DTexture), 0.5)
                mc.setAttr('{}.repeatU'.format(place2DTexture), 0.5)

                # Connect control
                connectTRSFacial2( ctrl = eachCtrl, elem = elem,  place2dTexture = [place2DTexture], positionDefault = [0, 0])

                ## assign file EyeMaskPath
                mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(fileNode, EyeMaskPath))

                ## get number of layerTexute
                if 'Up' in eachCtrl:
                    indexNumberOutNode = 1
                elif 'Dn' in eachCtrl:
                    indexNumberOutNode = 2
                # indexNumberOutNode = utaCore.checkNumberLayerTexture(layterTexture = outLyt)
                mc.connectAttr('{}.outColor'.format(fileNode), '{}.inputs[{}].color'.format(outLyt, indexNumberOutNode))
                mc.connectAttr('{}.outAlpha'.format(fileNode), '{}.inputs[{}].alpha'.format(outLyt, indexNumberOutNode))

                mc.setAttr('{}.inputs[{}].blendMode'.format(outLyt, indexNumberOutNode), 3)

                ## set Default Amp for valueTyAmp
                if 'MaskDn' in eachCtrl:
                    mc.setAttr('{}Shape.valueTyAmp'.format(eachCtrl), -0.19)
                else:
                    mc.setAttr('{}Shape.valueTyAmp'.format(eachCtrl), 0.19)

                ## locck attributre
                cn.lockAttr(listObj = [eachCtrl], attrs = ['tx', 'tz', 'rx', 'ry', 'sx', 'sy', 'sz', 'v'],lock = True, keyable = False)



            ## generate mask for shadow---------------------------------------------------
            if maskShadow:
                elem = elem + 'Shadow'
                ## genetate file texture  " Shadow ""
                shadowFileNode , shadowPlace2DTexture = cn.generateFilePlace2DTexture(name = name , side = side, elem = elem, node = 'file')
                mc.setAttr('{}.repeatV'.format(shadowPlace2DTexture), 0.5)
                mc.setAttr('{}.repeatU'.format(shadowPlace2DTexture), 0.5)

                # Connect control
                rotateMdv, UvOptionMdv, translateMdv = connectTRSFacial2( ctrl = eachCtrl, elem = elem,  place2dTexture = [shadowPlace2DTexture], positionDefault = [0, 0])
                
                shadowAverageMdv = mc.createNode('multiplyDivide', n = '{}{}Average{}Mdv'.format(name, elem, side))
                shadowAveragePma = mc.createNode('plusMinusAverage',  n = '{}{}Average{}Pma'.format(name, elem, side))

                mc.setAttr('{}.input2X'.format(shadowAverageMdv), 0.4)
                mc.connectAttr('{}.outputY'.format(shadowTranslateMdvNode), '{}.input1X'.format(shadowAverageMdv))
                mc.connectAttr('{}.outputX'.format(shadowAverageMdv),'{}.input2D[0].input2Dx'.format(shadowAveragePma))
                mc.connectAttr('{}.outputY'.format(UvOptionMdv), '{}.input2D[1].input2Dx'.format(shadowAveragePma))
                mc.connectAttr('{}.output2Dx'.format(shadowAveragePma), '{}.offsetV'.format(shadowPlace2DTexture), f = True)

                ## generate attribute amp for shadow control
                cn.addAttrCtrl (ctrl = eachCtrl + 'Shape' , elem = ['shadowMaskValueAmp'], min = -10, max = 10, at = 'float', dv = 0.4)
                mc.connectAttr('{}Shape.shadowMaskValueAmp'.format(eachCtrl), '{}.input2X'.format(shadowAverageMdv))

                ## assign file EyeMaskPath
                mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(shadowFileNode, EyeMaskPath))

                ## get number of layerTexute
                if 'Up' in eachCtrl:
                    indexNumberShadowNode = 1
                elif 'Dn' in eachCtrl:
                    indexNumberShadowNode = 2

                indexNumberShadowNode = utaCore.checkNumberLayerTexture(layterTexture = shadowLyt)
                mc.connectAttr('{}.outColor'.format(shadowFileNode), '{}.inputs[{}].color'.format(shadowLyt, indexNumberShadowNode - 1))
                mc.connectAttr('{}.outAlpha'.format(shadowFileNode), '{}.inputs[{}].alpha'.format(shadowLyt, indexNumberShadowNode - 1))

                mc.setAttr('{}.inputs[{}].blendMode'.format(shadowLyt, indexNumberShadowNode - 1), 3)

                ## set Default Amp for valueTyAmp
                if 'MaskDn' in eachCtrl:
                    mc.setAttr('{}Shape.valueTyAmp'.format(eachCtrl), -0.19)
                else:
                    mc.setAttr('{}Shape.valueTyAmp'.format(eachCtrl), 0.19)

                ## locck attributre
                cn.lockAttr(listObj = [eachCtrl], attrs = ['tx', 'tz', 'rx', 'ry', 'sx', 'sy', 'sz', 'v'],lock = True, keyable = False)
        

        ## whiteLine for eyeSeq file
        if whiteLineSeq:
            textureWhiteLineFile, textureWhiteLinePlace2DTexture = generateMaskWhiteLine(ctrl = ctrl, elem = 'WhiteLine', seqOutLyt = outLyt, material = material, seqFile = seqFile, texturePath = texturePath)
        
        ## whiteLine for eyeShadow file
        if whiteLineShadow:
            textureWhiteLineShadowFile, textureWhiteLineShadowPlace2DTexture = generateMaskWhiteLine(ctrl = ctrl, elem = 'WhiteLine', seqOutLyt = shadowLyt, material = '', seqFile = '', texturePath = texturePath)

def generateTitleBarTextureFile(uiRigCtrl = 'EyeUiRig_L_Ctrl', axis = ['X', 'Y'], texturePath = 'P:/CA/asset/work/char/GB/texture/main', *args):
    
    axisGeo = []
    if uiRigCtrl:
        ## split name
        nameSp, side, lastName = cn.splitName(sels = [uiRigCtrl])
        

        for i in range(len(axis)):
            ## path Texture abc XY menu file
            titleBarPathPlaneUi = texturePath + '/' + 'Title{}Bar'.format(axis[i]) + '.png'

            name = nameSp.replace('Ui', 'Title{}BarUi'.format(axis[i]))
            titleBarGeo = '{}_Geo'.format(name)
            axisGeo.append(titleBarGeo)

            textureTitleBarPlaneUiFile , textureTitleBarPlaneUiPlace2DTexture = cn.generateFilePlace2DTexture(name = name, side = side, elem = 'Title{}BarPlaneUi'.format(axis[i]), node = 'file')
            myTitleBarPlaneUiLambert, myTitleBarPlaneUiLambertSG = utaCore.generateMaterial(materialName = 'lambert', name = name, side = side, shortName = 'Lmp')
            mc.connectAttr('{}.outColor'.format(textureTitleBarPlaneUiFile), '{}.color'.format(myTitleBarPlaneUiLambert))
            mc.connectAttr('{}.outTransparency'.format(textureTitleBarPlaneUiFile), '{}.transparency'.format(myTitleBarPlaneUiLambert))
            mc.select(titleBarGeo)
            utaCore.assignSelectionToShader(myTitleBarPlaneUiLambert)
            mc.setAttr("{}.overrideEnabled".format(titleBarGeo), 1)
            mc.setAttr("{}.overrideDisplayType".format(titleBarGeo), 2)
            mc.setAttr("{}.selectionChildHighlighting".format(titleBarGeo), 0)

            if titleBarPathPlaneUi:
                mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(textureTitleBarPlaneUiFile, titleBarPathPlaneUi))

def imageSeqFacialProp(uiRigCtrl = 'EyeUiRig_L_Ctrl', textureUiRigCtrl = 'EyeTextureUiRig_L_Ctrl', attrOrTranslate = True, shaDowCtrl = True, elem = 'EyeSeq', planeGeo = 'EyeWrap_R_Geo', shadowPlaneGeo = '', textureUiGeo = 'Eye_R_geo_UI', texturePath = 'P:/CA/asset/work/char/GB/texture/main/images/preview', *args):

    '''
    from rf_maya.rftool.rig.rigScript import caFacialModule
    reload(caFacialModule)
    for each in ['Eff1_L_Ctrl', 'Eff2_L_Ctrl', 'Eff1_R_Ctrl', 'Eff2_R_Ctrl']    
        eachGeo = each.replace('Ctrl', 'Geo')
        caFacialModule.imageSeqFacialProp(uiRigCtrl = each, 
                                        textureUiRigCtrl = each, 
                                        attrOrTranslate = True, 
                                        shaDowCtrl = True, 
                                        elem = 'EffSeq', 
                                        planeGeo = eachGeo, 
                                        shadowPlaneGeo = '', 
                                        textureUiGeo = '', 
                                        texturePath = 'D:/Staff/KenR/CA/prop/texture')
    '''

    if attrOrTranslate  == True:
        cn.addAttrCtrl (ctrl = uiRigCtrl, elem = [elem], min = 0, max = 1000, at = 'long', dv = 0)

    if not textureUiRigCtrl:
        textureUiRigCtrl = uiRigCtrl
    txtyAverageValueUiAmp = 1
    # else:
    #     txtyAverageValueUiAmp =  [1]

    ## split name
    name, side, lastName = cn.splitName(sels = [uiRigCtrl])

    # # Get the list of all files and directories -------------------------------------
    # for each in dir_list:
    newPath = texturePath + '/' + elem + '/' + '{}.010.png'.format(elem)
    if shaDowCtrl:
        newShadowPath = newPath
    else:
        newShadowPath = ''

    ## Ui path
    if 'Seq' in elem:  
        elemRe = elem.replace('Seq', 'Ui')
        newPathPlaneUi = texturePath + '/Ui/' + elemRe + '.png'

    else:
        newPathPlaneUi = ''
    ## check value pma of side R
    # if '_L' in side:
        # valuePma = 20
    # else:
    valuePma = 10

    ## addAttribute
    attrTxInt = 'TxInt'
    attrTxTyUiCtrlAmp =['Txr{}Amp'.format(elem)]

    cn.addAttrCtrl (ctrl = textureUiRigCtrl + 'Shape' , elem = attrTxTyUiCtrlAmp, min = -10, max = 10, at = 'float', dv = 0)
    # for x in range(len(attrTxTyUiCtrlAmp)):
    mc.setAttr("{}.{}".format(textureUiRigCtrl + 'Shape', attrTxTyUiCtrlAmp[0]), txtyAverageValueUiAmp)

    mc.addAttr( textureUiRigCtrl , ln = attrTxInt , at = 'long' , k = True )
    textureAverageMdv = mc.createNode('multiplyDivide', n = '{}Average{}Mdv'.format(name, side))
    mc.connectAttr('{}Shape.{}'.format(textureUiRigCtrl, attrTxTyUiCtrlAmp[0]), '{}.input2X'.format(textureAverageMdv))
    if attrOrTranslate  == True:
        mc.connectAttr('{}.{}'.format(textureUiRigCtrl, elem), '{}.input1X'.format(textureAverageMdv))
    else:
        mc.connectAttr('{}.tx'.format(textureUiRigCtrl), '{}.input1X'.format(textureAverageMdv))
    mc.connectAttr('{}.outputX'.format(textureAverageMdv), '{}.{}'.format(textureUiRigCtrl, attrTxInt))

    ## generate node
    textureMdv = mc.createNode('multiplyDivide', n = '{}{}Mdv'.format(name, side))
    mc.setAttr("{}.input2X".format(textureMdv), 10)

    # tyRevMdv = mc.createNode('multiplyDivide', n = '{}TyRev{}Mdv'.format(name, side))
    # mc.connectAttr('{}Shape.{}'.format(textureUiRigCtrl, attrTxTyUiCtrlAmp[1]), '{}.input2X'.format(tyRevMdv))

    texturePma = mc.createNode('plusMinusAverage',  n = '{}{}Pma'.format(name, side))
    mc.setAttr('{}.input1D[0]'.format(texturePma), valuePma)

    textureSeqFile , place2DTextureTextureSeq = cn.generateFilePlace2DTexture(name = name, side = side, elem = 'Seq', node = 'file')
    # textureSeqFile = mc.shadingNode('file', asShader=True, n = '{}Seq{}File'.format(name, side))
    mc.setAttr("{}.useFrameExtension".format(textureSeqFile), 1)

    ## generate Layer texture node
    lytAllNode = mc.createNode('layeredTexture', n='{}All{}Lyt'.format(elem, side))
    lytOutNode = mc.createNode('layeredTexture', n='{}Out{}Lyt'.format(elem, side))
    lytAnswerNode = mc.createNode('layeredTexture', n='{}Answer{}Lyt'.format(elem, side))

    myShaderBlinn, myShaderBlinnSG = utaCore.generateMaterial(materialName = 'blinn', name = name, side = side, shortName = 'Bln')
    mc.setAttr("{}.specularColor".format(myShaderBlinn), 0,0,0 )

    if planeGeo:       
        if mc.objExists(planeGeo):
            mc.select(planeGeo)
            utaCore.assignSelectionToShader(myShaderBlinn)
            mc.setAttr("{}.overrideEnabled".format(planeGeo), 1)
            mc.setAttr("{}.overrideDisplayType".format(planeGeo), 2)
            mc.setAttr("{}.selectionChildHighlighting".format(planeGeo), 0)

    if textureUiGeo:
        if mc.objExists(textureUiGeo):
            ## planeGeo Ui
            texturePlaneUiFile , texturePlaneUiPlace2DTexture = cn.generateFilePlace2DTexture(name = name, side = side, elem = 'PlaneUi', node = 'file')
            myPlaneUiLambert, myPlaneUiLambertSG = utaCore.generateMaterial(materialName = 'lambert', name = name, side = side, shortName = 'Lmp')
            mc.connectAttr('{}.outColor'.format(texturePlaneUiFile), '{}.color'.format(myPlaneUiLambert))

            mc.select(textureUiGeo)
            utaCore.assignSelectionToShader(myPlaneUiLambert)
            mc.setAttr("{}.overrideEnabled".format(textureUiGeo), 1)
            mc.setAttr("{}.overrideDisplayType".format(textureUiGeo), 2)
            mc.setAttr("{}.selectionChildHighlighting".format(textureUiGeo), 0)
            mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(texturePlaneUiFile, newPathPlaneUi))
            ## abc XY menu File
            generateTitleBarTextureFile(uiRigCtrl = uiRigCtrl , axis = ['X', 'Y'], texturePath = texturePath)

    ## connect Node
    # mc.connectAttr('{}.{}'.format(ctrl, attrTxInt), '{}.input1.input1X'.fromat(textureMdv))
    mc.connectAttr('{}.{}'.format(textureUiRigCtrl, attrTxInt), '{}.input1.input1X'.format(textureMdv))
    # if attrOrTranslate  == True:
    #     mc.connectAttr('{}.{}'.format(uiRigCtrl, elem), '{}.input1.input1X'.format(tyRevMdv))
    # else:
    # mc.connectAttr('{}.ty'.format(textureUiRigCtrl), '{}.input1.input1X'.format(tyRevMdv))

    mc.connectAttr('{}.outputX'.format(textureMdv), '{}.input1D[1]'.format(texturePma))

    mc.connectAttr('{}.output1D'.format(texturePma),'{}.frameExtension'.format(textureSeqFile))
    # mc.connectAttr('{}.outputX'.format(tyRevMdv),'{}.frameOffset'.format(textureSeqFile))

    ## setAttribute texture path file
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(textureSeqFile, newPath))

    ## Seq output layeredTexture
    mc.connectAttr('{}.outColor'.format(textureSeqFile), '{}.inputs[0].color'.format(lytAllNode))

    mc.connectAttr('{}.outColor'.format(lytAllNode), '{}.inputs[3].color'.format(lytOutNode))
    mc.connectAttr('{}.outAlpha'.format(textureSeqFile),'{}.inputs[3].alpha'.format(lytOutNode))

    ## Connect outLyt to answerLyt Node
    ## connect to lytOutNode
    indexNumberAnswerNode = utaCore.checkNumberLayerTexture(layterTexture = lytAnswerNode)
    mc.connectAttr('{}.outColor'.format(lytOutNode),'{}.inputs[{}].color'.format(lytAnswerNode, indexNumberAnswerNode + 1))
    mc.connectAttr('{}.outAlpha'.format(lytOutNode),'{}.inputs[{}].alpha'.format(lytAnswerNode, indexNumberAnswerNode + 1))

    ## connect to blinn shde
    mc.connectAttr('{}.outColor'.format(lytAnswerNode), '{}.color'.format(myShaderBlinn))
    mc.connectAttr('{}.outTransparency'.format(lytAnswerNode), '{}.transparency'.format(myShaderBlinn))

    if shaDowCtrl:

        shadowCtrlName = '{}{}Ctrl'.format(name, side)
        ## generate new Control for shadow Control
        nameGimbal = 'Shadow'
        newShadowCtrlName = utaCore.addGimbalCtrl(ctrl = [shadowCtrlName], nameGmbl = nameGimbal, shapeAttr = False)
        ## generate node
        textureShadowMdv = mc.createNode('multiplyDivide', n = '{}Shadow{}Mdv'.format(name, side))
        mc.setAttr("{}.input2X".format(textureShadowMdv), 10)

        # tyRevShadowMdv = mc.createNode('multiplyDivide', n = '{}ShadowTyRev{}Mdv'.format(name, side))
        # mc.connectAttr('{}Shape.{}'.format(textureUiRigCtrl, attrTxTyUiCtrlAmp[1]), '{}.input2X'.format(tyRevShadowMdv))

        textureShadowPma = mc.createNode('plusMinusAverage',  n = '{}Shadow{}Pma'.format(name, side))
        mc.setAttr('{}.input1D[0]'.format(textureShadowPma), valuePma)

        ## Generate textureFile
        textureShadowSeqFile , place2DTextureShadow = cn.generateFilePlace2DTexture(name = name + 'ShadowSeq', side = side, elem = '', node = 'file')
        mc.setAttr("{}.useFrameExtension".format(textureShadowSeqFile), 1)


        ## Connect Shadow control
        if 'R_' in side:
            posiValue = [1.3, 2.2]
        else:
            posiValue = [1.3, 2.2]
        rotateMdv, UvOptionMdv, shadowTranslateMdvNode = connectTRSFacial2( ctrl = newShadowCtrlName, elem = 'Shadow',  place2dTexture = [place2DTextureShadow], positionDefault = posiValue)
        mc.connectAttr('{}.{}'.format(textureUiRigCtrl, attrTxInt), '{}.input1.input1X'.format(textureShadowMdv))
        # mc.connectAttr('{}.ty'.format(textureUiRigCtrl), '{}.input1.input1X'.format(tyRevShadowMdv))

        mc.connectAttr('{}.outputX'.format(textureShadowMdv), '{}.input1D[1]'.format(textureShadowPma))

        mc.connectAttr('{}.output1D'.format(textureShadowPma),'{}.frameExtension'.format(textureShadowSeqFile))
        # mc.connectAttr('{}.outputX'.format(tyRevShadowMdv),'{}.frameOffset'.format(textureShadowSeqFile))

        mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(textureShadowSeqFile, newShadowPath))

        ## generate metrial of shadowPlaneGeo
        if shadowPlaneGeo:
            ## duplicate shadowPlaneGeo
            shadowPlaneGeo, avravgeValueTz = generateShadowPlane(planeGeo = planeGeo, shadowPlaneGeo = shadowPlaneGeo, centerPlaneGeo = shadowPlaneGeo)
            ## connect to mateiral
            myShaderShadowBlinn, myShaderShadowBlinnSG = utaCore.generateMaterial(materialName = 'blinn', name = name + 'Shadow', side = side, shortName = 'Bln')
            mc.setAttr("{}.specularColor".format(myShaderShadowBlinn), 0,0,0 )
            # mc.connectAttr('{}.outColor'.format(textureShadowSeqFile), '{}.color'.format(myShaderShadowBlinn))
            mc.connectAttr('{}.outTransparency'.format(textureShadowSeqFile), '{}.transparency'.format(myShaderShadowBlinn))
            

            ## generate lambert for shadow matterial
            shadowRampMatterial, shadowRampMatterialSG = utaCore.generateMaterial(materialName = 'ramp', name = name + 'ShadowCor', side = side, shortName = 'Rmp')
            # mc.setAttr('{}.colorGain'.format(shadowRampMatterial),t =  double3, ( 0.309, 0.0951682, 0.034299))
            mc.setAttr('{}.colorEntryList[1].color'.format(shadowRampMatterial), 0.309, 0.0951682, 0.034299, type = "double3")
            mc.connectAttr('{}.outColor'.format(shadowRampMatterial), '{}.color'.format(myShaderShadowBlinn))
            # disconnectAttr ramp1.outColor EyebrowUiRigShadow_L_Bln.color;


            ## assign metrial to shadowPlaneGeo
            if mc.objExists(shadowPlaneGeo):
                mc.select(shadowPlaneGeo)
                utaCore.assignSelectionToShader(myShaderShadowBlinn)
                mc.setAttr("{}.overrideEnabled".format(shadowPlaneGeo), 1)
                mc.setAttr("{}.overrideDisplayType".format(shadowPlaneGeo), 2)
                mc.setAttr("{}.selectionChildHighlighting".format(shadowPlaneGeo), 0)

                lytShadowNode = ''
        else:
            lytShadowNode = mc.createNode('layeredTexture', n='{}Shadow{}{}Lyt'.format(name, elem, side))
            # eyeMaskFacial(ctrl = ['EyeMask_R_Ctrl'], elem = 'Mask', lytOut = 'eyeTextureOut_R_Lyt', texturePath = 'P:/CA/asset/work/char/GB/texture/main', whiteLine = False, material = ''
            ## connect to lytOutNode
            indexNumberShadowNode = utaCore.checkNumberLayerTexture(layterTexture = lytAnswerNode)
            # mc.connectAttr('{}.outColor'.format(textureShadowSeqFile),'{}.inputs[2].color'.format(lytShadowNode))
            mc.connectAttr('{}.outAlpha'.format(textureShadowSeqFile),'{}.inputs[2].alpha'.format(lytShadowNode))
            mc.connectAttr('{}.outColor'.format(lytShadowNode),'{}.inputs[{}].color'.format(lytAnswerNode, indexNumberShadowNode + 1))
            mc.connectAttr('{}.outAlpha'.format(lytShadowNode),'{}.inputs[{}].alpha'.format(lytAnswerNode, indexNumberShadowNode + 1))


            ## generate lambert for shadow matterial
            shadowRampCor, shadowRampCorSG = utaCore.generateMaterial(materialName = 'ramp', name = name + 'ShadowCor', side = side, shortName = 'Rmp')
            # mc.setAttr('{}.colorGain'.format(shadowRampMatterial),t =  double3, ( 0.309, 0.0951682, 0.034299))
            mc.setAttr('{}.colorEntryList[1].color'.format(shadowRampCor), 0.309, 0.0951682, 0.034299, type = "double3")
            mc.connectAttr('{}.outColor'.format(shadowRampCor), '{}.inputs[2].color'.format(lytShadowNode))



        ## lock attributre Ui Control
        uiShadowCtrl = '{}{}{}Ctrl'.format(name, nameGimbal, side)
        if mc.objExists(uiShadowCtrl):
            cn.lockAttr(listObj = [uiShadowCtrl], attrs = ['tz', 'rx', 'ry', 'sz', 'v'],lock = True, keyable = False)

    else:
        lytShadowNode = ''
    ## mirrorU shadow of R side
    generateAttributeForUser(ctrl = uiRigCtrl, nodeName = [place2DTextureTextureSeq, place2DTextureShadow], attr = ['mirrorU'], shape = False)

    ## lock attributre Seq
    cn.lockAttr(listObj = [textureUiRigCtrl], attrs = ['v', attrTxInt],lock = True, keyable = False)

    ## locck attributre Ui Control
    # uiCtrl = '{}{}Ctrl'.format(name, side)
    if mc.objExists(uiRigCtrl):
        cn.lockAttr(listObj = [uiRigCtrl], attrs = ['v'],lock = True, keyable = False)

        ## add Geo Ui Visibility 
        if not mc.objExists('{}.GeoVis'.format(uiRigCtrl)):
            geoVis = cn.addAttrCtrl (ctrl = uiRigCtrl , elem = ['GeoVis'], min = 0, max = 1, at = 'long', dv = 1)[0]
        else:
            geoVis = 'GeoVis'
        if mc.objExists(planeGeo):
            try:
                mc.connectAttr('{}.{}'.format(uiRigCtrl, geoVis), '{}.v'.format(planeGeo))
            except Exception as e:
                print e
        if mc.objExists(shadowPlaneGeo):
            try:
                mc.connectAttr('{}.{}'.format(uiRigCtrl, geoVis), '{}.v'.format(shadowPlaneGeo))
            except Exception as e:
                print e

    ## set Attribute Default
    mc.setAttr('{}.rotateFrameAmp'.format(newShadowCtrlName), -1)
    mc.setAttr('{}.valueTxAmp'.format(newShadowCtrlName), 0.25)
    ## clear attr none use
    utaCore. deleteAttribute(obj = newShadowCtrlName, attrs = ['GeoVis', elem, 'TxInt'] ) 
    utaCore. deleteAttribute(obj = '{}Shape'.format(newShadowCtrlName), attrs = ['gimbalControl', 'TxrEffSeqAmp'] ) 

    return lytAllNode, lytOutNode, lytShadowNode, lytAnswerNode, textureSeqFile , place2DTextureTextureSeq, myShaderBlinn, shadowTranslateMdvNode


def imageSeqFacial(uiRigCtrl = 'EyeUiRig_L_Ctrl', textureUiRigCtrl = 'EyeTextureUiRig_L_Ctrl', shaDowCtrl = True, elem = 'EyeSeq', planeGeo = 'EyeWrap_R_Geo', shadowPlaneGeo = '', textureUiGeo = 'Eye_R_geo_UI', texturePath = 'P:/CA/asset/work/char/GB/texture/main/images/preview', *args):

    ## Generate Folder for new plane
    createTmpFromElem(elem = elem, subSourcePath = 'caTemplate/texture/main', subTargetPath = 'texture/main')

    if not textureUiRigCtrl:
        textureUiRigCtrl = uiRigCtrl
        txtyAverageValueUiAmp = [0, 0]
    else:
        txtyAverageValueUiAmp =  [2, -2]

    ## split name
    name, side, lastName = cn.splitName(sels = [uiRigCtrl])

    # # Get the list of all files and directories -------------------------------------
    # for each in dir_list:
    newPath = texturePath + '/' + elem + '/' + '{}.010.png'.format(elem)
    if shaDowCtrl:
        newShadowPath = newPath
    else:
        newShadowPath = ''

    ## Ui path
    if 'Seq' in elem:  
        elemRe = elem.replace('Seq', 'Ui')
        # texturePath = texturePath.replace('preview', 'hi')
        newPathPlaneUi = texturePath + '/Ui/' + elemRe + '.png'

    else:
        newPathPlaneUi = ''
    ## check value pma of side R
    if '_L' in side:
        valuePma = 20
    else:
        valuePma = 10

    ## addAttribute
    attrTxInt = 'TxInt'
    attrTxTyUiCtrlAmp =['TxUiCtrlAmp', 'TyUiCtrlAmp']

    cn.addAttrCtrl (ctrl = textureUiRigCtrl + 'Shape' , elem = attrTxTyUiCtrlAmp, min = -10, max = 10, at = 'float', dv = 0)
    for x in range(len(attrTxTyUiCtrlAmp)):
        mc.setAttr("{}.{}".format(textureUiRigCtrl + 'Shape', attrTxTyUiCtrlAmp[x]), txtyAverageValueUiAmp[x])

    mc.addAttr( textureUiRigCtrl , ln = attrTxInt , at = 'long' , k = True )
    textureAverageMdv = mc.createNode('multiplyDivide', n = '{}Average{}Mdv'.format(name, side))
    mc.connectAttr('{}Shape.{}'.format(textureUiRigCtrl, attrTxTyUiCtrlAmp[0]), '{}.input2X'.format(textureAverageMdv))
    mc.connectAttr('{}.tx'.format(textureUiRigCtrl), '{}.input1X'.format(textureAverageMdv))
    mc.connectAttr('{}.outputX'.format(textureAverageMdv), '{}.{}'.format(textureUiRigCtrl, attrTxInt))

    ## generate node
    textureMdv = mc.createNode('multiplyDivide', n = '{}{}Mdv'.format(name, side))
    mc.setAttr("{}.input2X".format(textureMdv), 10)

    tyRevMdv = mc.createNode('multiplyDivide', n = '{}TyRev{}Mdv'.format(name, side))
    mc.connectAttr('{}Shape.{}'.format(textureUiRigCtrl, attrTxTyUiCtrlAmp[1]), '{}.input2X'.format(tyRevMdv))

    texturePma = mc.createNode('plusMinusAverage',  n = '{}{}Pma'.format(name, side))
    mc.setAttr('{}.input1D[0]'.format(texturePma), valuePma)

    textureSeqFile , place2DTextureTextureSeq = cn.generateFilePlace2DTexture(name = name, side = side, elem = 'Seq', node = 'file')
    # textureSeqFile = mc.shadingNode('file', asShader=True, n = '{}Seq{}File'.format(name, side))
    mc.setAttr("{}.useFrameExtension".format(textureSeqFile), 1)

    ## generate Layer texture node
    lytAllNode = mc.createNode('layeredTexture', n='{}All{}Lyt'.format(elem, side))
    lytOutNode = mc.createNode('layeredTexture', n='{}Out{}Lyt'.format(elem, side))
    lytAnswerNode = mc.createNode('layeredTexture', n='{}Answer{}Lyt'.format(elem, side))

    myShaderBlinn, myShaderBlinnSG = utaCore.generateMaterial(materialName = 'blinn', name = name, side = side, shortName = 'Bln')
    mc.setAttr("{}.specularColor".format(myShaderBlinn), 0,0,0 )

    if planeGeo:       
        if mc.objExists(planeGeo):
            mc.select(planeGeo)
            utaCore.assignSelectionToShader(myShaderBlinn)
            mc.setAttr("{}.overrideEnabled".format(planeGeo), 1)
            mc.setAttr("{}.overrideDisplayType".format(planeGeo), 2)
            mc.setAttr("{}.selectionChildHighlighting".format(planeGeo), 0)

    if textureUiGeo:
        if mc.objExists(textureUiGeo):
            ## planeGeo Ui
            texturePlaneUiFile , texturePlaneUiPlace2DTexture = cn.generateFilePlace2DTexture(name = name, side = side, elem = 'PlaneUi', node = 'file')
            myPlaneUiLambert, myPlaneUiLambertSG = utaCore.generateMaterial(materialName = 'lambert', name = name, side = side, shortName = 'Lmp')
            mc.connectAttr('{}.outColor'.format(texturePlaneUiFile), '{}.color'.format(myPlaneUiLambert))

            mc.select(textureUiGeo)
            utaCore.assignSelectionToShader(myPlaneUiLambert)
            mc.setAttr("{}.overrideEnabled".format(textureUiGeo), 1)
            mc.setAttr("{}.overrideDisplayType".format(textureUiGeo), 2)
            mc.setAttr("{}.selectionChildHighlighting".format(textureUiGeo), 0)
            mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(texturePlaneUiFile, newPathPlaneUi))
            ## abc XY menu File
            generateTitleBarTextureFile(uiRigCtrl = uiRigCtrl , axis = ['X', 'Y'], texturePath = texturePath)
    ## connect Node
    # mc.connectAttr('{}.{}'.format(ctrl, attrTxInt), '{}.input1.input1X'.fromat(textureMdv))
    mc.connectAttr('{}.{}'.format(textureUiRigCtrl, attrTxInt), '{}.input1.input1X'.format(textureMdv))
    mc.connectAttr('{}.ty'.format(textureUiRigCtrl), '{}.input1.input1X'.format(tyRevMdv))

    mc.connectAttr('{}.outputX'.format(textureMdv), '{}.input1D[1]'.format(texturePma))

    mc.connectAttr('{}.output1D'.format(texturePma),'{}.frameExtension'.format(textureSeqFile))
    mc.connectAttr('{}.outputX'.format(tyRevMdv),'{}.frameOffset'.format(textureSeqFile))

    ## setAttribute texture path file
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(textureSeqFile, newPath))

    ## Seq output layeredTexture
    mc.connectAttr('{}.outColor'.format(textureSeqFile), '{}.inputs[0].color'.format(lytAllNode))

    mc.connectAttr('{}.outColor'.format(lytAllNode), '{}.inputs[3].color'.format(lytOutNode))
    mc.connectAttr('{}.outAlpha'.format(textureSeqFile),'{}.inputs[3].alpha'.format(lytOutNode))

    ## Connect outLyt to answerLyt Node
    ## connect to lytOutNode
    indexNumberAnswerNode = utaCore.checkNumberLayerTexture(layterTexture = lytAnswerNode)
    mc.connectAttr('{}.outColor'.format(lytOutNode),'{}.inputs[{}].color'.format(lytAnswerNode, indexNumberAnswerNode + 1))
    mc.connectAttr('{}.outAlpha'.format(lytOutNode),'{}.inputs[{}].alpha'.format(lytAnswerNode, indexNumberAnswerNode + 1))

    ## connect to blinn shde
    mc.connectAttr('{}.outColor'.format(lytAnswerNode), '{}.color'.format(myShaderBlinn))
    mc.connectAttr('{}.outTransparency'.format(lytAnswerNode), '{}.transparency'.format(myShaderBlinn))

    if shaDowCtrl:

        shadowCtrlName = '{}{}Ctrl'.format(name, side)
        ## generate new Control for shadow Control
        nameGimbal = 'Shadow'
        newShadowCtrlName = utaCore.addGimbalCtrl(ctrl = [shadowCtrlName], nameGmbl = nameGimbal, shapeAttr = False)
        ## generate node
        textureShadowMdv = mc.createNode('multiplyDivide', n = '{}Shadow{}Mdv'.format(name, side))
        mc.setAttr("{}.input2X".format(textureShadowMdv), 10)

        tyRevShadowMdv = mc.createNode('multiplyDivide', n = '{}ShadowTyRev{}Mdv'.format(name, side))
        mc.connectAttr('{}Shape.{}'.format(textureUiRigCtrl, attrTxTyUiCtrlAmp[1]), '{}.input2X'.format(tyRevShadowMdv))

        textureShadowPma = mc.createNode('plusMinusAverage',  n = '{}Shadow{}Pma'.format(name, side))
        mc.setAttr('{}.input1D[0]'.format(textureShadowPma), valuePma)

        ## Generate textureFile
        textureShadowSeqFile , place2DTextureShadow = cn.generateFilePlace2DTexture(name = name + 'ShadowSeq', side = side, elem = '', node = 'file')
        mc.setAttr("{}.useFrameExtension".format(textureShadowSeqFile), 1)


        ## Connect Shadow control
        if 'R_' in side:
            posiValue = [1.3, 2.2]
        else:
            posiValue = [1.3, 2.2]
        rotateMdv, UvOptionMdv, shadowTranslateMdvNode = connectTRSFacial2( ctrl = newShadowCtrlName, elem = 'Shadow',  place2dTexture = [place2DTextureShadow], positionDefault = posiValue)
        mc.connectAttr('{}.{}'.format(textureUiRigCtrl, attrTxInt), '{}.input1.input1X'.format(textureShadowMdv))
        mc.connectAttr('{}.ty'.format(textureUiRigCtrl), '{}.input1.input1X'.format(tyRevShadowMdv))

        mc.connectAttr('{}.outputX'.format(textureShadowMdv), '{}.input1D[1]'.format(textureShadowPma))

        mc.connectAttr('{}.output1D'.format(textureShadowPma),'{}.frameExtension'.format(textureShadowSeqFile))
        mc.connectAttr('{}.outputX'.format(tyRevShadowMdv),'{}.frameOffset'.format(textureShadowSeqFile))

        mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(textureShadowSeqFile, newShadowPath))

        ## generate metrial of shadowPlaneGeo
        if shadowPlaneGeo:
            ## duplicate shadowPlaneGeo
            shadowPlaneGeo, avravgeValueTz = generateShadowPlane(planeGeo = planeGeo, shadowPlaneGeo = shadowPlaneGeo, centerPlaneGeo = shadowPlaneGeo)
            ## connect to mateiral
            myShaderShadowBlinn, myShaderShadowBlinnSG = utaCore.generateMaterial(materialName = 'blinn', name = name.replace('UiRig', 'Shadow' + 'UiRig'), side = side, shortName = 'Bln')
            mc.setAttr("{}.specularColor".format(myShaderShadowBlinn), 0,0,0 )
            mc.connectAttr('{}.outTransparency'.format(textureShadowSeqFile), '{}.transparency'.format(myShaderShadowBlinn))
            

            ## generate lambert for shadow matterial
            shadowRampMatterial, shadowRampMatterialSG = utaCore.generateMaterial(materialName = 'ramp', name = name.replace('UiRig', 'ShadowCor' + 'UiRig'), side = side, shortName = 'Rmp')
            # mc.setAttr('{}.colorGain'.format(shadowRampMatterial),t =  double3, ( 0.309, 0.0951682, 0.034299))
            mc.setAttr('{}.colorEntryList[1].color'.format(shadowRampMatterial), 0.309, 0.0951682, 0.034299, type = "double3")
            mc.connectAttr('{}.outColor'.format(shadowRampMatterial), '{}.color'.format(myShaderShadowBlinn))
            # disconnectAttr ramp1.outColor EyebrowUiRigShadow_L_Bln.color;


            ## assign metrial to shadowPlaneGeo
            if mc.objExists(shadowPlaneGeo):
                mc.select(shadowPlaneGeo)
                utaCore.assignSelectionToShader(myShaderShadowBlinn)
                mc.setAttr("{}.overrideEnabled".format(shadowPlaneGeo), 1)
                mc.setAttr("{}.overrideDisplayType".format(shadowPlaneGeo), 2)
                mc.setAttr("{}.selectionChildHighlighting".format(shadowPlaneGeo), 0)

                lytShadowNode = ''

            ## add attribute tx
            ## getAttr translate Z of shadowPlaneGeo
            attrTz = float(mc.getAttr('{}.tz'.format(shadowPlaneGeo)))
            shadowPlaneGeoTzAttrNode = mc.createNode('addDoubleLinear',n = '{}Default{}Adl'.format(name, side))
            utaCore.addAttrCtrl (ctrl = shadowPlaneGeoTzAttrNode, elem = ['default'], min = -200, max = 200, at = 'float')
            mc.setAttr( '{}.default'.format(shadowPlaneGeoTzAttrNode), attrTz)
            mc.connectAttr('{}.default'.format(shadowPlaneGeoTzAttrNode), '{}.input1'.format(shadowPlaneGeoTzAttrNode))

            offsetShadowPlaneAttr = cn.addAttrCtrl (ctrl = '{}Shape'.format(shadowCtrlName) , elem = ['OffsetShadowPlane'], min = -1000, max = 1000, at = 'float', dv = 0)[0]
            offsetShadowPlaneMdv = mc.createNode('multiplyDivide', n = '{}{}{}Mdv'.format(name, elem, side))
            mc.setAttr('{}.input2X'.format(offsetShadowPlaneMdv), -0.1)
            mc.connectAttr('{}Shape.{}'.format(shadowCtrlName, offsetShadowPlaneAttr), '{}.input1X'.format(offsetShadowPlaneMdv))

            # ## connect node to shadowPlaneGeo tz
            mc.connectAttr('{}.outputX'.format(offsetShadowPlaneMdv), '{}.input2'.format(shadowPlaneGeoTzAttrNode))
            mc.connectAttr('{}.output'.format(shadowPlaneGeoTzAttrNode), '{}.tz'.format(shadowPlaneGeo))
            answer = float(mc.getAttr('{}.tz'.format(shadowPlaneGeo)))

        else:
            lytShadowNode = mc.createNode('layeredTexture', n='{}Shadow{}{}Lyt'.format(name, elem, side))
            # eyeMaskFacial(ctrl = ['EyeMask_R_Ctrl'], elem = 'Mask', lytOut = 'eyeTextureOut_R_Lyt', texturePath = 'P:/CA/asset/work/char/GB/texture/main', whiteLine = False, material = ''
            ## connect to lytOutNode
            indexNumberShadowNode = utaCore.checkNumberLayerTexture(layterTexture = lytAnswerNode)
            # mc.connectAttr('{}.outColor'.format(textureShadowSeqFile),'{}.inputs[2].color'.format(lytShadowNode))
            mc.connectAttr('{}.outAlpha'.format(textureShadowSeqFile),'{}.inputs[2].alpha'.format(lytShadowNode))
            mc.connectAttr('{}.outColor'.format(lytShadowNode),'{}.inputs[{}].color'.format(lytAnswerNode, indexNumberShadowNode + 1))
            mc.connectAttr('{}.outAlpha'.format(lytShadowNode),'{}.inputs[{}].alpha'.format(lytAnswerNode, indexNumberShadowNode + 1))




            ## generate lambert for shadow matterial
            shadowRampCor, shadowRampCorSG = utaCore.generateMaterial(materialName = 'ramp', name = name.replace('UiRig', 'ShadowCor' + 'UiRig'), side = side, shortName = 'Rmp')
            # mc.setAttr('{}.colorGain'.format(shadowRampMatterial),t =  double3, ( 0.309, 0.0951682, 0.034299))
            mc.setAttr('{}.colorEntryList[1].color'.format(shadowRampCor), 0.309, 0.0951682, 0.034299, type = "double3")
            mc.connectAttr('{}.outColor'.format(shadowRampCor), '{}.inputs[2].color'.format(lytShadowNode))



        ## locck attributre Ui Control
        uiShadowCtrl = '{}{}{}Ctrl'.format(name, nameGimbal, side)
        if mc.objExists(uiShadowCtrl):
            cn.lockAttr(listObj = [uiShadowCtrl], attrs = ['tz', 'rx', 'ry', 'sz', 'v'],lock = True, keyable = False)

    else:
        lytShadowNode = ''
        place2DTextureShadow = ''
        shadowTranslateMdvNode = ''

    ## mirrorU shadow of R side
    if place2DTextureShadow:
        generateAttributeForUser(ctrl = uiRigCtrl, nodeName = [place2DTextureTextureSeq, place2DTextureShadow], attr = ['mirrorU'], shape = False)
    else:
        generateAttributeForUser(ctrl = uiRigCtrl, nodeName = [place2DTextureTextureSeq], attr = ['mirrorU'], shape = False)

    ## locck attributre Seq
    cn.lockAttr(listObj = [textureUiRigCtrl], attrs = ['tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v', attrTxInt],lock = True, keyable = False)

    ## locck attributre Ui Control
    # uiCtrl = '{}{}Ctrl'.format(name, side)
    if mc.objExists(uiRigCtrl):
        cn.lockAttr(listObj = [uiRigCtrl], attrs = ['rx', 'ry', 'sz', 'v'],lock = True, keyable = False)

        ## add Geo Ui Visibility 
        geoVis = cn.addAttrCtrl (ctrl = uiRigCtrl , elem = ['GeoVis'], min = 0, max = 1, at = 'long', dv = 1)[0]
        if mc.objExists(planeGeo):
            try:
                mc.connectAttr('{}.{}'.format(uiRigCtrl, geoVis), '{}.v'.format(planeGeo))
            except Exception as e:
                print e
        if mc.objExists(shadowPlaneGeo):
            try:
                mc.connectAttr('{}.{}'.format(uiRigCtrl, geoVis), '{}.v'.format(shadowPlaneGeo))
            except Exception as e:
                print e

    return lytAllNode, lytOutNode, lytShadowNode, lytAnswerNode, textureSeqFile , place2DTextureTextureSeq, myShaderBlinn, shadowTranslateMdvNode

def generateMaskWhiteLine(ctrl = [], elem = 'WhiteLine', seqOutLyt = '', material = '', seqFile = '', texturePath = '', pupilFileName = '', *args):
    if ctrl:
        seqOutLytNameSp, seqOutLytSide, seqOutLytLastName = cn.splitName(sels = [seqOutLyt])
        seqName = seqOutLytNameSp.replace('UiRig', '')
        # nameSp, side, lastName = cn.splitName(sels = [ctrl])
        whiteLineAllLytNode = mc.createNode('layeredTexture', n='{}{}AllUiRig{}Lyt'.format(seqName, elem, seqOutLytSide))
        whiteLineAlphaLytNode = mc.createNode('layeredTexture', n='{}{}AlphaUiRig{}Lyt'.format(seqName, elem, seqOutLytSide))



        for i in range(len(ctrl)):
            nameSp, side, lastName = cn.splitName(sels = [ctrl[i]])
            name = nameSp.replace('UiRig', '')

            # Get the list of all files and directories
            dir_list = os.listdir(texturePath)
            for each in dir_list:
                if elem in each:
                    if 'Dn' in ctrl[i]:
                        WhiteLinePath = texturePath + '/' + '{}Dn.png'.format(elem)

                    else:
                        WhiteLinePath = texturePath + '/' + '{}.png'.format(elem)

            PupilWhiteLinePath = texturePath + '/' + 'Eyeball' + '/' + '{}.png'.format('PupilCor')
            texturePupilWhiteLineFile , texturePupilWhiteLinePlace2DTexture = cn.generateFilePlace2DTexture(name = 'Pupil', side = side, elem = elem, node = 'file')

            textureWhiteLineFile , textureWhiteLinePlace2DTexture = cn.generateFilePlace2DTexture(name = name, side = side, elem = elem, node = 'file')

            mc.setAttr('{}.repeatV'.format(textureWhiteLinePlace2DTexture), 0.5)
            mc.setAttr('{}.repeatU'.format(textureWhiteLinePlace2DTexture), 0.5)
            indexNumber = utaCore.checkNumberLayerTexture(layterTexture = whiteLineAllLytNode)
            indexAlphaNumber = utaCore.checkNumberLayerTexture(layterTexture = whiteLineAlphaLytNode)
            
            if seqFile:
            #     mc.connectAttr('{}.outColor'.format(textureWhiteLineFile), '{}.inputs[{}].color'.format(whiteLineAllLytNode, indexNumber + i))

                ## avarage alpha generate
                # whiteLineAvrageMdlNode = mc.createNode('multDoubleLinear', n = '{}{}{}Mdl'.format(name, elem, side))
                # mc.connectAttr('{}.outAlpha'.format(textureWhiteLineFile), '{}.input2'.format(whiteLineAvrageMdlNode))
                # mc.connectAttr('{}.outAlpha'.format(seqFile), '{}.input1'.format(whiteLineAvrageMdlNode))
                mc.connectAttr('{}.outColor'.format(textureWhiteLineFile), '{}.inputs[{}].color'.format(whiteLineAllLytNode, indexNumber + i))
                mc.connectAttr('{}.outAlpha'.format(textureWhiteLineFile), '{}.inputs[{}].alpha'.format(whiteLineAllLytNode, indexNumber + i))

                mc.connectAttr('{}.outColor'.format(whiteLineAllLytNode), '{}.inputs[{}].color'.format(whiteLineAlphaLytNode, indexAlphaNumber + i))
                # mc.connectAttr('{}.outAlpha'.format(whiteLineAllLytNode), '{}.inputs[{}].alpha'.format(whiteLineAlphaLytNode, indexAlphaNumber + i))

                ## set blendMode for whitelineAlpha
                if indexNumber + i == 0:
                    mc.setAttr('{}.inputs[{}].blendMode'.format(whiteLineAllLytNode, indexNumber + i), 1)
                elif indexNumber + i == 1:
                    mc.setAttr('{}.inputs[{}].blendMode'.format(whiteLineAllLytNode, indexNumber + i), 0)
                if indexAlphaNumber + i == 0:
                    mc.setAttr('{}.inputs[{}].blendMode'.format(whiteLineAlphaLytNode, indexAlphaNumber + i ), 2)
                    mc.connectAttr('{}.outAlpha'.format(whiteLineAllLytNode), '{}.inputs[{}].alpha'.format(whiteLineAlphaLytNode, indexAlphaNumber + i))
                elif indexAlphaNumber + i == 1:
                    mc.setAttr('{}.inputs[{}].blendMode'.format(whiteLineAlphaLytNode, indexAlphaNumber + i ), 1)
                    mc.connectAttr('{}.outAlpha'.format(seqFile), '{}.inputs[{}].alpha'.format(whiteLineAlphaLytNode, indexAlphaNumber + i))
            else:
                mc.connectAttr('{}.outColor'.format(textureWhiteLineFile), '{}.inputs[{}].color'.format(whiteLineAllLytNode, indexNumber + i))
                mc.connectAttr('{}.outAlpha'.format(textureWhiteLineFile), '{}.inputs[{}].alpha'.format(whiteLineAllLytNode, indexNumber + i))

            if WhiteLinePath:
                ## assign file EyeMaskPath
                mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(textureWhiteLineFile, WhiteLinePath))

            connectTRSFacial2(ctrl = ctrl[i], elem = elem, place2dTexture = [textureWhiteLinePlace2DTexture], positionDefault = [0, 0])

        ## generate mdl for alpha
        # if seqFile:
        #     whiteLineAvrageMdlNode = mc.createNode('multDoubleLinear', n = '{}{}{}Mdv'.format(name, elem, side))
        #     mc.connectAttr('{}.outColor'.format(whiteLineAllLytNode),'{}.inputs[0].color'.format(whiteLineAllLytNode))
        #     mc.connectAttr('{}.outAlpha'.format(whiteLineAllLytNode),'{}.input2'.format(whiteLineAvrageMdlNode))
        #     mc.connectAttr('{}.outAlpha'.format(seqFile), '{}.input1'.format(whiteLineAvrageMdlNode))
        #     mc.connectAttr('{}..output'.format(whiteLineAvrageMdlNode), '{}.inputs[0].alpha'.format(whiteLineAllLytNode))

        ## WhiteLine pupil alpha
        mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(texturePupilWhiteLineFile, PupilWhiteLinePath))

        # else:
        mc.connectAttr('{}.outColor'.format(texturePupilWhiteLineFile),'{}.inputs[0].color'.format(seqOutLyt))
        mc.connectAttr('{}.outAlpha'.format(whiteLineAlphaLytNode),'{}.inputs[0].alpha'.format(seqOutLyt))



        # mc.connectAttr('{}.outColor'.format(whiteLineLytNode),'{}.color'.format(material), f = True)

    return textureWhiteLineFile, textureWhiteLinePlace2DTexture

def generateAttributeForUser(ctrl = '', nodeName = [], attr = [], shape = True, *args):
    if shape:
        shapeName = 'Shape'
    else:
        shapeName = ''
    name, side, lastName = cn.splitName(sels = [ctrl])
    checkName = ['Eye', 'Specular', 'Blink', 'Pupil', 'Iris' ]

    if '_R' in side:
        valueAmp = 1
        if 'Eyebrow' in name:
            valueAmp = 1
        for each in checkName:
            if (each in name) and ('Eyebrow' not in name):
                valueAmp = 1

    else:
        valueAmp = 1


    for at in attr:
        if not mc.objExists('{}{}.{}Amp'.format(ctrl, shapeName, at)):
            cn.addAttrCtrl (ctrl = '{}{}'.format(ctrl, shapeName), elem = ['{}Amp'.format(at)], min = 0, max = 1, at = 'long', dv = valueAmp)
    for eachNodeName in nodeName:
        if eachNodeName:
            for i in range(len(attr)): 
                eachAttrAmp = '{}Amp'.format(attr[i])  
                if 'mirror' in eachAttrAmp:
                    mc.setAttr('{}.repeatU'.format(eachNodeName), -1)
                mc.connectAttr('{}{}.{}'.format(ctrl, shapeName, eachAttrAmp), '{}.{}'.format(eachNodeName, attr[i]))
    return eachAttrAmp

def connectTRSFacial2(ctrl = 'Blink_L_Ctrl', elem = '', place2dTexture = ['BlinkCor_L_place2dTexture'], positionDefault = [0, 0], *args):
    # place2dTextureColor = place2dTexture

    ## split name
    name, side, lastName = cn.splitName(sels = [ctrl])

    ## addAttribute Shape
    nameAttrValueAmp = ['valueTxAmp', 'valueTyAmp', 'offsetUValueAmp', 'offsetVValueAmp']
    nameAttrValueAmpCheck = []
    ## check Attribute
    for i in range(len(nameAttrValueAmp)):
        if not mc.objExists('{}Shape.{}'.format(ctrl, nameAttrValueAmp[i])): 
            if '_R_' in side:
                if 'valueTx' in nameAttrValueAmp[i]:
                    valueTxAmp_N = -0.25 
                else:
                    valueTxAmp_N = 0.25 
            else:
                valueTxAmp_N = 0.25 
            cn.addAttrCtrl (ctrl = ctrl + 'Shape', elem = [nameAttrValueAmp[i]], min = -100, max = 100, at = 'float', dv = valueTxAmp_N)

    ## generate value amp 
    listCheckValue = ['EyeShadow', 'Blink', 'Iris', 'Specular']
    nameCheck = ctrl.split('UiRig')[0]

    if nameCheck in listCheckValue:
        if '_R' in side:
            UVFlipValue = 1 
            rotateFrameValue = 1
        else:
            UVFlipValue = 1 
            rotateFrameValue = -1
    else:   
        if '_R' in side:
            if 'MaskDn' in nameCheck:
                UVFlipValue = 1 
                rotateFrameValue = -1
            else:
                UVFlipValue = 1 
                rotateFrameValue = 1
        else:
            if 'MaskDn' in nameCheck:
                UVFlipValue = 1 
                rotateFrameValue = 1    
            else:
                UVFlipValue = 1 
                rotateFrameValue = -1

        # UVFlipValue = 1
        # rotateFrameValue = -1

    nameUVFlipAmp  = 'UVFlipAmp'
    if not mc.objExists('{}Shape.{}'.format(ctrl, nameUVFlipAmp)):
        cn.addAttrCtrl (ctrl = ctrl + 'Shape', elem = [nameUVFlipAmp], min = -1, max = 1, at = 'long', dv = UVFlipValue)

    ## add rotateFrameAmp
    rotateFrameAmp = 'rotateFrameAmp'
    if not mc.objExists('{}Shape.{}'.format(ctrl, rotateFrameAmp)):
        cn.addAttrCtrl (ctrl = ctrl + 'Shape', elem = [rotateFrameAmp], min = -1, max = 1, at = 'long', dv = rotateFrameValue)

    ## generate Scale  node
    ## add scaleAmp Coverage
    scaleAmpAttr = 'scaleAmp'
    if not 'Mask' in ctrl:
        if not mc.objExists('{}Shape.{}'.format(ctrl, scaleAmpAttr)):
            cn.addAttrCtrl (ctrl = ctrl + 'Shape', elem = [scaleAmpAttr], min = -100, max = 100, at = 'float', dv = 1)

        scaleAmpCoverageMdv = mc.createNode('multiplyDivide', n = '{}{}ScaleAmpCoverage{}Mdv'.format(name, elem, side))  

        ## connect scaleAmp Coverage
        mc.connectAttr('{}.scale'.format(ctrl), '{}.input1'.format(scaleAmpCoverageMdv))
        mc.connectAttr('{}Shape.{}'.format(ctrl, scaleAmpAttr), '{}.input2X'.format(scaleAmpCoverageMdv))
        mc.connectAttr('{}Shape.{}'.format(ctrl, scaleAmpAttr), '{}.input2Y'.format(scaleAmpCoverageMdv))
        mc.connectAttr('{}Shape.{}'.format(ctrl, scaleAmpAttr), '{}.input2Z'.format(scaleAmpCoverageMdv))

        scaleCoverageUVMdv = mc.createNode('multiplyDivide', n = '{}{}Scale{}Mdv'.format(name, elem, side))
        mc.setAttr("{}.input2X".format(scaleCoverageUVMdv), 0.5)
        mc.setAttr("{}.input2Y".format(scaleCoverageUVMdv), 0.5)
        mc.setAttr("{}.input2Z".format(scaleCoverageUVMdv), 0.5)
        mc.connectAttr('{}.output'.format(scaleAmpCoverageMdv), '{}.input1'.format(scaleCoverageUVMdv))
        # mc.connectAttr('{}.scale'.format(ctrl), '{}.input1'.format(scaleCoverageUVMdv))
    else:
        scaleCoverageUVMdv = mc.createNode('multiplyDivide', n = '{}{}Scale{}Mdv'.format(name, elem, side))
        mc.setAttr("{}.input2X".format(scaleCoverageUVMdv), 0.5)
        mc.setAttr("{}.input2Y".format(scaleCoverageUVMdv), 0.5)
        mc.setAttr("{}.input2Z".format(scaleCoverageUVMdv), 0.5)
        # mc.connectAttr('{}.output'.format(scaleAmpCoverageMdv), '{}.input1'.format(scaleCoverageUVMdv))
        mc.connectAttr('{}.scale'.format(ctrl), '{}.input1'.format(scaleCoverageUVMdv))

    scaleCoverageUVPma = mc.createNode('plusMinusAverage',  n = '{}ScaleCoverageUV{}Pma'.format(name, side))
    mc.setAttr('{}.input2D[0].input2Dx'.format(scaleCoverageUVPma),1)
    mc.setAttr('{}.input2D[0].input2Dy'.format(scaleCoverageUVPma),1)
    mc.setAttr('{}.input2D[2].input2Dx'.format(scaleCoverageUVPma),-0.5)
    mc.setAttr('{}.input2D[2].input2Dy'.format(scaleCoverageUVPma),-0.5)
    mc.connectAttr('{}.outputX'.format(scaleCoverageUVMdv), '{}.input2D[1].input2Dx'.format(scaleCoverageUVPma))
    mc.connectAttr('{}.outputY'.format(scaleCoverageUVMdv), '{}.input2D[1].input2Dy'.format(scaleCoverageUVPma))

    ## add scaleAmp TFUV
    if not 'Mask' in ctrl:
        scaleAmpTFUVMdv = mc.createNode('multiplyDivide', n = '{}{}ScaleAmpTFUV{}Mdv'.format(name, elem, side))  

        ## connect scaleAmp TFUV
        mc.connectAttr('{}.sx'.format(ctrl), '{}.input1X'.format(scaleAmpTFUVMdv))
        mc.connectAttr('{}.sy'.format(ctrl), '{}.input1Y'.format(scaleAmpTFUVMdv))
        mc.connectAttr('{}Shape.{}'.format(ctrl, scaleAmpAttr), '{}.input2X'.format(scaleAmpTFUVMdv))
        mc.connectAttr('{}Shape.{}'.format(ctrl, scaleAmpAttr), '{}.input2Y'.format(scaleAmpTFUVMdv))

        scaleTFUVPma = mc.createNode('plusMinusAverage',  n = '{}ScaleTFUV{}Pma'.format(name, side))
        mc.setAttr('{}.input2D[1].input2Dx'.format(scaleTFUVPma),-1)
        mc.setAttr('{}.input2D[1].input2Dy'.format(scaleTFUVPma),-1)
        mc.connectAttr('{}.outputX'.format(scaleAmpTFUVMdv), '{}.input2D[0].input2Dx'.format(scaleTFUVPma))
        mc.connectAttr('{}.outputY'.format(scaleAmpTFUVMdv), '{}.input2D[0].input2Dy'.format(scaleTFUVPma))
        # mc.connectAttr('{}.scaleX'.format(ctrl), '{}.input2D[0].input2Dx'.format(scaleTFUVPma))
        # mc.connectAttr('{}.scaleY'.format(ctrl), '{}.input2D[0].input2Dy'.format(scaleTFUVPma))
    else:
        scaleTFUVPma = mc.createNode('plusMinusAverage',  n = '{}ScaleTFUV{}Pma'.format(name, side))
        mc.setAttr('{}.input2D[1].input2Dx'.format(scaleTFUVPma),-1)
        mc.setAttr('{}.input2D[1].input2Dy'.format(scaleTFUVPma),-1)
        # mc.connectAttr('{}.outputX'.format(scaleAmpTFUVMdv), '{}.input2D[0].input2Dx'.format(scaleTFUVPma))
        # mc.connectAttr('{}.outputY'.format(scaleAmpTFUVMdv), '{}.input2D[0].input2Dy'.format(scaleTFUVPma))
        mc.connectAttr('{}.scaleX'.format(ctrl), '{}.input2D[0].input2Dx'.format(scaleTFUVPma))
        mc.connectAttr('{}.scaleY'.format(ctrl), '{}.input2D[0].input2Dy'.format(scaleTFUVPma))

    scaleTFUVMdv = mc.createNode('multiplyDivide', n = '{}{}ScaleTFUV{}Mdv'.format(name, elem, side))
    mc.setAttr('{}.input2X'.format(scaleTFUVMdv),-0.25)
    mc.setAttr('{}.input2Y'.format(scaleTFUVMdv),-0.25)
    mc.connectAttr('{}.output2Dx'.format(scaleTFUVPma), '{}.input1X'.format(scaleTFUVMdv))
    mc.connectAttr('{}.output2Dy'.format(scaleTFUVPma), '{}.input1Y'.format(scaleTFUVMdv))

    ## generate translate node
    translateMdv = mc.createNode('multiplyDivide', n = '{}{}Translate{}Mdv'.format(name, elem, side))
    offsetUVPma = mc.createNode('plusMinusAverage',  n = '{}{}OffsetUV{}Pma'.format(name, elem, side))        
    mc.connectAttr('{}.outputX'.format(translateMdv), "{}.input2D[0].input2Dx".format(offsetUVPma))
    mc.connectAttr('{}.outputY'.format(translateMdv), "{}.input2D[0].input2Dy".format(offsetUVPma))
    # setDefault of offset U,V
    offsetUVAverageMdv = mc.createNode('multiplyDivide', n = '{}{}offsetUV{}Mdv'.format(name, elem, side))
    mc.setAttr("{}.input2X".format(offsetUVAverageMdv), 0.01)
    mc.setAttr("{}.input2Y".format(offsetUVAverageMdv), 0.01)
    mc.setAttr("{}Shape.{}".format(ctrl, nameAttrValueAmp[2]), positionDefault[0])
    mc.setAttr("{}Shape.{}".format(ctrl, nameAttrValueAmp[3]), positionDefault[1])
    mc.connectAttr("{}Shape.{}".format(ctrl, nameAttrValueAmp[2]), "{}.input1X".format(offsetUVAverageMdv))
    mc.connectAttr("{}Shape.{}".format(ctrl, nameAttrValueAmp[3]), "{}.input1Y".format(offsetUVAverageMdv))
    mc.connectAttr("{}.outputX".format(offsetUVAverageMdv), "{}.input2D[1].input2Dx".format(offsetUVPma))
    mc.connectAttr("{}.outputY".format(offsetUVAverageMdv), "{}.input2D[1].input2Dy".format(offsetUVPma))

    ## connect Attribute
    # translate
    mc.connectAttr('{}.tx'.format(ctrl), '{}.input1X'.format(translateMdv))
    mc.connectAttr('{}.ty'.format(ctrl), '{}.input1Y'.format(translateMdv))
    mc.connectAttr('{}Shape.{}'.format(ctrl, nameAttrValueAmp[0]), "{}.input2X".format(translateMdv))
    revTXYValueAmpMdv = mc.createNode('multiplyDivide', n = '{}{}TranslateXYRev{}Mdv'.format(name, elem, side))
    mc.setAttr('{}.input2X'.format(revTXYValueAmpMdv), -1)
    mc.connectAttr('{}Shape.{}'.format(ctrl, nameAttrValueAmp[1]), '{}.input1X'.format(revTXYValueAmpMdv))
    mc.connectAttr('{}.outputX'.format(revTXYValueAmpMdv), "{}.input2Y".format(translateMdv))

    ## generate rotate node
    rotateMdv = mc.createNode('multiplyDivide', n = '{}{}Rotate{}Mdv'.format(name, elem, side))
    # mc.setAttr("{}.input2X".format(rotateMdv), -1)
    mc.connectAttr('{}.rotateZ'.format(ctrl), '{}.input1X'.format(rotateMdv))
    mc.connectAttr('{}Shape.{}'.format(ctrl, rotateFrameAmp), '{}.input2X'.format(rotateMdv))

    #If geometry uv flip , side left and right not same uv
    UvOptionMdv = mc.createNode('multiplyDivide', n = '{}{}UvOption{}Mdv'.format(name, elem, side))
    mc.connectAttr('{}.{}'.format(ctrl, nameUVFlipAmp), '{}.input2X'.format(UvOptionMdv))
    mc.setAttr('{}.input2Y'.format(UvOptionMdv), 1)

    mc.connectAttr('{}.output2D.output2Dx'.format(offsetUVPma), '{}.input1X'.format(UvOptionMdv))
    mc.connectAttr('{}.output2D.output2Dy'.format(offsetUVPma), '{}.input1Y'.format(UvOptionMdv))

    for eachPlace2dTexture in place2dTexture:
        ## connect Scale coverage UV node
        coverageUVCheck = utaCore.checkConnectLists(object = eachPlace2dTexture, attr = ['coverageU', 'coverageV'])
        if coverageUVCheck:
            coverageUVPma = mc.createNode('plusMinusAverage',  n = '{}{}CoverageUV{}Pma888'.format(name, elem, side))
            mc.setAttr('{}.input2D[0].input2Dx'.format(coverageUVPma), -1)
            mc.setAttr('{}.input2D[0].input2Dy'.format(coverageUVPma), -1)

            mc.connectAttr(coverageUVCheck[0], '{}.input2D[1].input2Dx'.format(coverageUVPma))
            mc.connectAttr(coverageUVCheck[-1], '{}.input2D[1].input2Dy'.format(coverageUVPma))
        
            mc.connectAttr('{}.output2Dx'.format(scaleCoverageUVPma), '{}.input2D[2].input2Dx'.format(coverageUVPma))
            mc.connectAttr('{}.output2Dy'.format(scaleCoverageUVPma), '{}.input2D[2].input2Dy'.format(coverageUVPma))

            mc.connectAttr('{}.output2Dx'.format(coverageUVPma), '{}.coverageU'.format(eachPlace2dTexture), f = True)
            mc.connectAttr('{}.output2Dy'.format(coverageUVPma), '{}.coverageV'.format(eachPlace2dTexture), f = True)

        else:
            mc.connectAttr('{}.output2Dx'.format(scaleCoverageUVPma), '{}.coverageU'.format(eachPlace2dTexture))
            mc.connectAttr('{}.output2Dy'.format(scaleCoverageUVPma), '{}.coverageV'.format(eachPlace2dTexture))

        ## connect Scale translateFrame UV node
        translateFrameUVCheck = utaCore.checkConnectLists(object = eachPlace2dTexture, attr = ['translateFrameU', 'translateFrameU'])
        if translateFrameUVCheck:
            coverageTFUVPma = mc.createNode('plusMinusAverage',  n = '{}{}TranslateUV{}Pma888'.format(name, elem, side))
      
            mc.connectAttr(translateFrameUVCheck[0], '{}.input2D[0].input2Dx'.format(coverageTFUVPma))
            mc.connectAttr(translateFrameUVCheck[-1], '{}.input2D[0].input2Dy'.format(coverageTFUVPma))

            mc.connectAttr('{}.outputX'.format(scaleTFUVMdv), '{}.input2D[1].input2Dx'.format(coverageTFUVPma))
            mc.connectAttr('{}.outputY'.format(scaleTFUVMdv), '{}.input2D[1].input2Dy'.format(coverageTFUVPma))          

            mc.connectAttr('{}.output2Dx'.format(coverageTFUVPma), '{}.translateFrame.translateFrameU'.format(eachPlace2dTexture), f = True)
            mc.connectAttr('{}.output2Dy'.format(coverageTFUVPma), '{}.translateFrame.translateFrameV'.format(eachPlace2dTexture), f = True)

        else:
            mc.connectAttr('{}.outputX'.format(scaleTFUVMdv), '{}.translateFrame.translateFrameU'.format(eachPlace2dTexture))
            mc.connectAttr('{}.outputY'.format(scaleTFUVMdv), '{}.translateFrame.translateFrameV'.format(eachPlace2dTexture))

        ## connect translate offsetU node
        offsetUVCheck = utaCore.checkConnectLists(object = eachPlace2dTexture, attr = ['offsetU', 'offsetV'])
        if offsetUVCheck:
            offsetUVPma = mc.createNode('plusMinusAverage',  n = '{}{}OffsetUV{}Pma888'.format(name, elem, side))
 
            mc.connectAttr(offsetUVCheck[0], '{}.input2D[0].input2Dx'.format(offsetUVPma))
            mc.connectAttr(offsetUVCheck[-1], '{}.input2D[0].input2Dy'.format(offsetUVPma))

            mc.connectAttr('{}.outputX'.format(UvOptionMdv), '{}.input2D[1].input2Dx'.format(offsetUVPma))
            mc.connectAttr('{}.outputY'.format(UvOptionMdv), '{}.input2D[1].input2Dy'.format(offsetUVPma))          

            mc.connectAttr('{}.output2Dx'.format(offsetUVPma), '{}.offset.offsetU'.format(eachPlace2dTexture), f = True)
            mc.connectAttr('{}.output2Dy'.format(offsetUVPma), '{}.offset.offsetV'.format(eachPlace2dTexture), f = True)

        else:
            mc.connectAttr('{}.outputX'.format(UvOptionMdv), '{}.offset.offsetU'.format(eachPlace2dTexture))
            mc.connectAttr('{}.outputY'.format(UvOptionMdv), '{}.offset.offsetV'.format(eachPlace2dTexture))

        ## connect rotate rotateFrame node
        rotateFrameCheck = utaCore.checkConnectLists(object = eachPlace2dTexture, attr = ['rotateFrame'])
        if rotateFrameCheck:
            rotateFramePma = mc.createNode('plusMinusAverage',  n = '{}{}RotateFrame{}Pma888'.format(name, elem, side))
            mc.connectAttr(rotateFrameCheck[0], '{}.input2D[0].input2Dx'.format(rotateFramePma))
            mc.connectAttr('{}.outputX'.format(rotateMdv),'{}.input2D[1].input2Dx'.format(rotateFramePma))
            mc.connectAttr('{}.output2Dx'.format(rotateFramePma), '{}.rotateFrame'.format(eachPlace2dTexture), f = True)

        else:
            mc.connectAttr('{}.outputX'.format(rotateMdv),'{}.rotateFrame'.format(eachPlace2dTexture))

        ## connection Old
        ## connect Scale node
        # mc.connectAttr('{}.output2Dx'.format(scaleCoverageUVPma), '{}.coverageU'.format(eachPlace2dTexture))
        # mc.connectAttr('{}.output2Dy'.format(scaleCoverageUVPma), '{}.coverageV'.format(eachPlace2dTexture))

        # mc.connectAttr('{}.outputX'.format(scaleTFUVMdv), '{}.translateFrame.translateFrameU'.format(eachPlace2dTexture))
        # mc.connectAttr('{}.outputY'.format(scaleTFUVMdv), '{}.translateFrame.translateFrameV'.format(eachPlace2dTexture))

        # ## connect translate node
        # mc.connectAttr('{}.outputX'.format(UvOptionMdv), '{}.offset.offsetU'.format(eachPlace2dTexture))
        # mc.connectAttr('{}.outputY'.format(UvOptionMdv), '{}.offset.offsetV'.format(eachPlace2dTexture))

        ## connect rotate Node
        # mc.connectAttr('{}.outputX'.format(rotateMdv),'{}.rotateFrame'.format(eachPlace2dTexture))


    return rotateMdv, UvOptionMdv, translateMdv

def connectTRSFacial(ctrl = 'Blink_L_Ctrl', elem = '', place2dTexture = ['BlinkCor_L_place2dTexture'], positionDefault = [0, 0],*args):

    place2dTextureColor = place2dTexture


    ## split name
    name, side, lastName = cn.splitName(sels = [ctrl])

    ## addAttribute Shape
    nameAttrValueAmp = ['valueTxAmp', 'valueTyAmp', 'TFUValueAmp', 'TFVValueAmp']
    cn.addAttrCtrl (ctrl = ctrl + 'Shape', elem = nameAttrValueAmp, min = -100, max = 100, at = 'float', dv = 0.25)

    ## addAttribute UVFlipAmp
    if 'L' in side:
        UVFlipValue = 1
    elif 'R' in side:
        UVFlipValue = 1
    else:
        UVFlipValue = 1
    nameUVFlipAmp  = 'UVFlipAmp'
    cn.addAttrCtrl (ctrl = ctrl + 'Shape', elem = [nameUVFlipAmp], min = -1, max = 1, at = 'long', dv = UVFlipValue)

    ## generate Mdv node
    translateMdv = mc.createNode('multiplyDivide', n = '{}{}Translate{}Mdv'.format(name, elem, side))
    rotateMdv = mc.createNode('multiplyDivide', n = '{}{}Rotate{}Mdv'.format(name, elem, side))
    mc.setAttr("{}.input2X".format(rotateMdv), -1)
    mc.setAttr("{}.input2Y".format(rotateMdv), -1)
    mc.connectAttr('{}Shape.{}'.format(ctrl, nameAttrValueAmp[0]), "{}.input2X".format(translateMdv))
    mc.connectAttr('{}Shape.{}'.format(ctrl, nameAttrValueAmp[1]), "{}.input2Y".format(translateMdv))

    ## generate animCureUU node
    # offsetU
    offsetUAnimCurve = mc.createNode('animCurveUU', n = '{}{}Scale{}offsetU'.format(name, elem, side))
    mc.setKeyframe(offsetUAnimCurve,f= 0.5,v=-0.5,itt='auto',ott = 'auto')
    mc.setKeyframe(offsetUAnimCurve,f= 1,  v= 0,  itt='auto',ott = 'auto')
    mc.setKeyframe(offsetUAnimCurve,f= 2,  v=0.25,itt='auto',ott = 'auto')
    mc.setAttr(offsetUAnimCurve+'.preInfinity',0)
    mc.setAttr(offsetUAnimCurve+'.postInfinity',0)
    mc.keyTangent(offsetUAnimCurve, wt=0)
    # offsetV
    offsetVAnimCurve = mc.createNode('animCurveUU', n = '{}{}Scale{}offsetV'.format(name, elem, side))
    mc.setKeyframe(offsetVAnimCurve,f= 0.5,v=-0.5,itt='auto',ott = 'auto')
    mc.setKeyframe(offsetVAnimCurve,f= 1,  v= 0,  itt='auto',ott = 'auto')
    mc.setKeyframe(offsetVAnimCurve,f= 2,  v=0.25,itt='auto',ott = 'auto')
    mc.setAttr(offsetVAnimCurve+'.preInfinity',0)
    mc.setAttr(offsetVAnimCurve+'.postInfinity',0)
    mc.keyTangent(offsetVAnimCurve, wt=0)
    # repeatU
    repeatUAnimCurve = mc.createNode('animCurveUU', n = '{}{}Scale{}repeatU'.format(name, elem, side))
    mc.setKeyframe(repeatUAnimCurve,f= 0.5,v=2,itt='auto',ott = 'auto')
    mc.setKeyframe(repeatUAnimCurve,f= 1,  v=1,  itt='auto',ott = 'auto')
    mc.setKeyframe(repeatUAnimCurve,f= 2,  v=0.5,itt='auto',ott = 'auto')
    mc.setAttr(repeatUAnimCurve+'.preInfinity',0)
    mc.setAttr(repeatUAnimCurve+'.postInfinity',0)
    mc.keyTangent(repeatUAnimCurve, wt=0)
    # repeatV
    repeatVAnimCurve = mc.createNode('animCurveUU', n = '{}{}Scale{}repeatV'.format(name, elem, side))
    mc.setKeyframe(repeatVAnimCurve,f= 0.5,v=2,itt='auto',ott = 'auto')
    mc.setKeyframe(repeatVAnimCurve,f= 1,  v= 1,  itt='auto',ott = 'auto')
    mc.setKeyframe(repeatVAnimCurve,f= 2,  v=0.5,itt='auto',ott = 'auto')
    mc.setAttr(repeatVAnimCurve+'.preInfinity',0)
    mc.setAttr(repeatVAnimCurve+'.postInfinity',0)
    mc.keyTangent(repeatVAnimCurve, wt=0)

    ## connect Attribute
    # translate
    mc.connectAttr('{}.tx'.format(ctrl), '{}.input1X'.format(translateMdv))
    mc.connectAttr('{}.ty'.format(ctrl), '{}.input1Y'.format(translateMdv))
    # place2DTexture Color
    for eachPlace2dT in place2dTextureColor:

        ## create TFUV (position default of texture)
        TFUVAverageMdv = mc.createNode('multiplyDivide', n = '{}{}TFUVAverage{}Mdv'.format(name, elem, side))
        mc.setAttr('{}.input1X'.format(TFUVAverageMdv), 0.01)
        mc.setAttr('{}.input1Y'.format(TFUVAverageMdv), 0.01)
        mc.setAttr("{}Shape.{}".format(ctrl, nameAttrValueAmp[2]), positionDefault[0])
        mc.setAttr("{}Shape.{}".format(ctrl, nameAttrValueAmp[3]), positionDefault[1])
        TFUVPma = mc.createNode('plusMinusAverage',  n = '{}TFUV{}Pma'.format(name, side))

        mc.connectAttr('{}Shape.{}'.format(ctrl, nameAttrValueAmp[2]), "{}.input2X".format(TFUVAverageMdv))
        mc.connectAttr('{}Shape.{}'.format(ctrl, nameAttrValueAmp[3]), "{}.input2Y".format(TFUVAverageMdv))
        mc.connectAttr('{}.outputX'.format(TFUVAverageMdv), "{}.input2D[0].input2Dx".format(TFUVPma))
        mc.connectAttr('{}.outputY'.format(TFUVAverageMdv), "{}.input2D[0].input2Dy".format(TFUVPma))
        mc.connectAttr('{}.outputX'.format(translateMdv), "{}.input2D[1].input2Dx".format(TFUVPma))
        mc.connectAttr('{}.outputY'.format(translateMdv), "{}.input2D[1].input2Dy".format(TFUVPma))


        ## If geometry uv flip , side left and right not same uv
        UvOptionMdv = mc.createNode('multiplyDivide', n = '{}{}UvOption{}Mdv'.format(name, elem, side))
        mc.connectAttr('{}.{}'.format(ctrl, nameUVFlipAmp), '{}.input2X'.format(UvOptionMdv))
        mc.setAttr('{}.input2Y'.format(UvOptionMdv), 1)

        mc.connectAttr('{}.output2D.output2Dx'.format(TFUVPma), '{}.input1X'.format(UvOptionMdv))
        mc.connectAttr('{}.output2D.output2Dy'.format(TFUVPma), '{}.input1Y'.format(UvOptionMdv))

        mc.connectAttr('{}.outputX'.format(UvOptionMdv), '{}.translateFrame.translateFrameU'.format(eachPlace2dT))
        mc.connectAttr('{}.outputY'.format(UvOptionMdv), '{}.translateFrame.translateFrameV'.format(eachPlace2dT))


    # rotate
    mc.connectAttr('{}.rotateZ'.format(ctrl), '{}.input1X'.format(rotateMdv))
    mc.connectAttr('{}.rotateZ'.format(ctrl), '{}.input1Y'.format(rotateMdv))
    for eachPlace2dT in place2dTextureColor:
        mc.connectAttr('{}.outputX'.format(rotateMdv),'{}.rotateUV'.format(eachPlace2dT))

    # scale 
    mc.connectAttr('{}.scaleX'.format(ctrl),'{}.input'.format(offsetUAnimCurve))
    mc.connectAttr('{}.scaleY'.format(ctrl),'{}.input'.format(offsetVAnimCurve))
    mc.connectAttr('{}.scaleX'.format(ctrl),'{}.input'.format(repeatUAnimCurve))
    mc.connectAttr('{}.scaleY'.format(ctrl),'{}.input'.format(repeatVAnimCurve))

    # Color
    for eachPlace2dT in place2dTextureColor:
        mc.connectAttr('{}.output'.format(offsetUAnimCurve),'{}.offset.offsetU'.format(eachPlace2dT))
        mc.connectAttr('{}.output'.format(offsetVAnimCurve),'{}.offset.offsetV'.format(eachPlace2dT))
        mc.connectAttr('{}.output'.format(repeatUAnimCurve),'{}.repeatUV.repeatU'.format(eachPlace2dT))
        mc.connectAttr('{}.output'.format(repeatVAnimCurve),'{}.repeatUV.repeatV'.format(eachPlace2dT))



################################################### Tools ##############################################
################################################### Tools ##############################################


def connectGeoVisMain(geoGrp = '', *args):

    if geoGrp:
        if mc.objExists(geoGrp):
            geoAll = utaCore.listGeo(sels = geoGrp, nameing = '_Geo', namePass = '_Grp')

    ## check ui geo
    ## get and assign material to geo
    for eachGeo in geoAll:

        geoName = eachGeo.split('|')[-1]

        if ':' in geoName:
            nameSp = eachGeo.split(':')
            ns = nameSp[0]
            geo = nameSp[-1]

        nameSp, side, lastName = cn.splitName(sels = [geo]) 
        name = nameSp.split('Plane')[0]
        if name == 'Eye':
            name = '{}Ball'.format(name)
        elif 'Shadow' in name:
            name = name.replace('Shadow', '')
        if mc.objExists(geoName):
            mc.connectAttr('{}.{}{}Vis'.format('fclRig_md:VisViewUiRig_Ctrl', name, side), '{}.v'.format(geoName))

def assignMaterial(geoGrp = 'bodyRig_md:AllGeo_Grp', *args):
    from rf_utils.context import context_info

    ## delete layer render
    utaCore.clearRenderLayer()
    
    ## check geoGrp
    entity = context_info.ContextPathInfo()
    asset = entity.name
    if mc.objExists(geoGrp):
        pass
    elif mc.objExists('bodyRig_md:{}Geo_Grp'.format(asset)):
        geoGrp = 'bodyRig_md:{}Geo_Grp'.format(asset)
    else:
        geoGrp = 'bodyRig_md:MdGeo_Grp'

    ## lists Geo
    if geoGrp:
        listsGrp = geoGrp
    else:
        listsGrp = mc.ls('FclRigGeo_Grp')

    if listsGrp:
        if mc.objExists(listsGrp):
            geoAll = utaCore.listGeo(sels = listsGrp, nameing = '_Geo', namePass = '_Grp')

    ## check ui geo
    ## get and assign material to geo
    for eachGeo in geoAll:
        geo = eachGeo.split('|')[-1]
        if ':' in geo:
            nameSp = eachGeo.split(':')
            ns = nameSp[0]
            geo = nameSp[-1]

        name, side, lastName = cn.splitName(sels = [geo]) 
        if 'FclRig' in name:
            uiGeo = '{}{}{}'.format(name.replace('FclRig', 'UiRig'), side, lastName)
        else:
            uiGeo = '{}UiRig{}{}'.format(name, side, lastName)
        
        uiGeoNameSp = mc.ls('*:{}'.format(uiGeo), uiGeo)
        for each in uiGeoNameSp:
            if uiGeoNameSp:
                material = utaCore.getMaterialformGeo(geometry = each)
                if material:
                    if mc.objExists(material[0]):
                        mc.select(eachGeo)
                        previewObj = utaCore.assignSelectionToShader(material[0])
def assignMaterialNew(geoGrp = 'bodyRig_md:AllGeo_Grp', passName = '', *args):
    from rf_utils.context import context_info

    ## check geoGrp
    entity = context_info.ContextPathInfo()
    asset = entity.name
    if mc.objExists(geoGrp):
        pass
    elif mc.objExists('bodyRig_md:{}Geo_Grp'.format(asset)):
        geoGrp = 'bodyRig_md:{}Geo_Grp'.format(asset)
    else:
        geoGrp = 'bodyRig_md:MdGeo_Grp'
    ## lists Geo
    if geoGrp:
        listsGrp = geoGrp
    else:
        listsGrp = mc.ls('FclRigGeo_Grp')

    bodyGeoAll = []
    if listsGrp:
        if mc.objExists(listsGrp):
            bodyGeo = utaCore.listGeo(sels = listsGrp, nameing = '_Geo', namePass = '_Grp')
            for geo in bodyGeo:
                geoSel = geo.split('|')[-1]
                if 'Plane' in geoSel:
                    bodyGeoAll.append(geoSel)
    ## check ui geo
    ## get and assign material to geo
    uiGeoNameSpGeoAll = []
    for eachGeo in bodyGeoAll:
        geo = eachGeo.split('|')[-1]
        if ':' in geo:
            nameSp = eachGeo.split(':')
            ns = nameSp[0]
            geo = nameSp[-1]

        name, side, lastName = cn.splitName(sels = [geo]) 
        
        if not passName in name:
            if 'FclRig' in name:
                uiGeo = '{}{}{}'.format(name.replace('FclRig', 'UiRig'), side, lastName)
                uiGeoNameSp = mc.ls('*:{}'.format(uiGeo), uiGeo)
                uiGeoNameSpGeoAll.append(uiGeoNameSp)
            else:
                uiGeo = '{}UiRig{}{}'.format(name, side, lastName)
                uiGeoNameSp = mc.ls('*:{}'.format(uiGeo), uiGeo)
                if uiGeoNameSp:
                    uiGeoNameSpGeoAll.append(uiGeoNameSp[0])


    for each in uiGeoNameSpGeoAll:
        if each:
            material = utaCore.getMaterialformGeo(geometry = each)
            if material:
                if not 'lambert1' in material:
                    if mc.objExists(material[0]):
                        if not 'SG' in material[0]:
                            for x in range(len(bodyGeoAll)):
                                name, side, lastName = cn.splitName(sels = [bodyGeoAll[x].split(':')[-1]])
                                name = name.split('Plane')[0]
                                if name in material[0]:
                                    if side in material[0]:
                                        nameMat, sideMat, lastNameMat = cn.splitName(sels = [material[0].split(':')[-1]])
                                        nameMat = nameMat.split('UiRig')[0]


                                        print name, '>>>', nameMat
                                        if name == nameMat:

                                            mc.select(bodyGeoAll[x])
                                            previewObj = utaCore.assignSelectionToShader(material[0])  

                                        # if 'Eye' in name:
                                        #     if name == 'Eyebrow':
                                        #         mc.select(bodyGeoAll[x])
                                        #         previewObj = utaCore.assignSelectionToShader(material[0])
                                        #         print bodyGeoAll[x], '....>>>..1', material[0]
                                        #         print True
                                        #         print name, '::', side, '::::', material[0]
                                        #     else:
                                        #         mc.select(bodyGeoAll[x])
                                        #         previewObj = utaCore.assignSelectionToShader(material[0])

                                        # else:
                                        #     mc.select(bodyGeoAll[x])
                                        #     previewObj = utaCore.assignSelectionToShader(material[0])
                                        #     print bodyGeoAll[x], '....>>>..1', material[0]
                                        #     print True
                                        #     print name, '::', side, '::::', material[0]
                                    # else:a
                                    # print False
                                # print name, '::', side, '::::'
                                # if name and side in material[0]:
                                #     mc.select(bodyGeoAll[x])
                                #     previewObj = utaCore.assignSelectionToShader(material[0])
                                #     print bodyGeoAll[x], '....>>>..1', material[0]
                
                                # else:
                                #     mc.select(bodyGeoAll[x])
                                #     previewObj = utaCore.assignSelectionToShader(material[0])
                                #     print bodyGeoAll[x], '....>>>..3', material[0]


                        # fclRigGeoSplit = each.split(':')
                        # fclRigGeoNameSp = fclRigGeoSplit[0]
                        # fclRigGeoName = fclRigGeoSplit[-1]
                        # bodyRigGeeName = fclRigGeoName.replace('UiRig', '')
                        # bodyRigGeo = 'bodyRig_md:{}'.format(bodyRigGeeName)

                        # if passName in bodyRigGeo:
                            # mc.select(bodyRigGeo)
                            # previewObj = utaCore.assignSelectionToShader(material[0])
                        #     print 'A'
                        # else:
                        #     mc.select(bodyRigGeo)
                        #     previewObj = utaCore.assignSelectionToShader(material[0])
                        #     print 'B'


                        # if '|' in eachGeo:
                        #     eachObj = eachGeo.split('|')[-1]
                        #     mc.select(eachObj)
                        #     previewObj = utaCore.assignSelectionToShader(material[0])
                        #     print eachObj, '....>>>....', material[0]
                        # else:
                        #     mc.select(eachObj)
                        #     previewObj = utaCore.assignSelectionToShader(material[0])
                        #     print eachObj, '....2>>>2....', material[0]
def parentPlaneUiRigGeoToUiCtrl(*args):
    sels = mc.ls(sl = True)
    for each in sels:
        if '_Ctrl' in each:
            checkOldGeo = utaCore.listGeo(sels = each, nameing = '_Geo', namePass = '_Grp')
            if checkOldGeo:
                geoOld = []
                for eachGeo in checkOldGeo:
                    mc.select(eachGeo)
                    mc.delete(eachGeo)
            name, side, lastName = cn.splitName(sels = [each])
            planeName = name.replace('Ui', 'PlaneFcl')
            planeNameFcl = '{}{}Geo'.format(planeName, side)

            if mc.objExists(planeNameFcl):
                planeNameUiRig = planeNameFcl.replace('Fcl', 'Ui')
                mc.rename(planeNameFcl, planeNameUiRig)
            else: 
                planeNameUiRig = ''

            if planeNameUiRig:

                # utaCore.centerPivotObj (obj = [planeNameUiRig])

                mc.delete(mc.pointConstraint(each, planeNameUiRig, mo = False))
                mc.select(planeNameUiRig)
                mc.select(each,add = True)
                mc.parent(planeNameUiRig, each)

                valueSz = mc.getAttr('%s.scaleZ' % planeNameUiRig)
                if str(valueSz) == '-1.0':
                    utaCore.clean ( target = planeNameUiRig)
                    mc.polyNormal(planeNameUiRig, nm = 0, ch = 0)
                    pm.xform ( planeNameUiRig , cp = True ) 
                    
    mc.select(cl = True)


def addAttrMenuUiCtrl(ctrlMain = '',ctrlGrpVis = '',*args):
    
    ctrlGrpLists = mc.listRelatives(ctrlGrpVis, c = True, f = True)

    nameAttrLists = []
    nameGrpLists = []
    for i in range(len(ctrlGrpLists)):
        name = ctrlGrpLists[i].split('|')[-1]
        nameGrpLists.append(name)
        name, side, lastName = cn.splitName(sels = [name]) 
        if 'UiRig' in name:
            nameAttrLists.append('{}{}Vis'.format(name.replace('UiRigCtrl', ''), side))

    utaCore.attrNameTitle(obj = ctrlMain, ln = 'FacialControlVis')        
    irisFollowAttr = cn.addAttrCtrl (ctrl = ctrlMain, elem = nameAttrLists, min = 0, max = 1, at = 'long')
    for x in range(len(nameAttrLists)):
         
        mc.setAttr('{}.{}'.format(ctrlMain, nameAttrLists[x]), 1)   
        mc.connectAttr('{}.{}'.format(ctrlMain, nameAttrLists[x]), '{}.v'.format(nameGrpLists[x]))


def generateRampCtrl(headRamp = True, ctrl = '', folderName = '', elem = '', geo ='', texturePath = '', *args):
    # Get the list of all files and directories
    dir_list = os.listdir(texturePath)
    for each in dir_list:
        if folderName == each:
            recheckLists = texturePath + '/' + folderName
            lists = os.listdir(recheckLists)
    checkElem = [elem, elem.capitalize(), elem.lower()]
    for objName in checkElem:
        for i in range(len(lists)):
            if objName in lists[i] and 'Col' in lists[i]:
                pathFileName = texturePath + '/' + folderName + '/' + lists[i]

    ## split Name
    name, side, lastName = cn.splitName(sels= [ctrl])


    lytNode, lytSGNode = utaCore.generateMaterial(materialName = 'layeredTexture', name = name, side = side, shortName = 'Lyt')
    blinnNode, blinnSGNode = utaCore.generateMaterial(materialName = 'blinn', name = name, side = side, shortName = 'Bln')

    ## generate file head color and link path texture file
    headFile = mc.shadingNode('file', asShader=True, n = '{}Head{}File'.format(name, side))
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(headFile, pathFileName))


    if headRamp:
        # attrName = addAttrCtrl (ctrl = ctrl, elem = ['RampSlide'], min = 0, max = 10, at = 'float', dv = 0)[0]
        rampNode = utaCore.create_shader('{}{}Rmp'.format(name, side), node_type="ramp")[0]
        mc.setAttr("{}.interpolation".format(rampNode), 4)

        rampAlphaNode = utaCore.create_shader('{}Alpha{}Rmp'.format(name, side), node_type="ramp")[0]
        mc.setAttr("{}.interpolation".format(rampAlphaNode), 4)

        mc.setAttr("{}.colorEntryList[1].color".format(rampNode),0.0627, 0.2274, 0.2941)
        mc.setAttr("{}.colorEntryList[2].color".format(rampNode),0.0627, 0.2274, 0.2941)
        mc.setAttr("{}.colorEntryList[0].color".format(rampNode),0, 0, 0)

        mc.setAttr("{}.colorEntryList[1].color".format(rampAlphaNode),1, 1, 1)
        mc.setAttr("{}.colorEntryList[0].color".format(rampAlphaNode),0, 0, 0)

        adlXNode = mc.createNode("addDoubleLinear", n = '{}X{}Adl'.format(name, side))
        adlXAttrName = utaCore.addAttrCtrl (ctrl = adlXNode, elem = ['default'], min = 0, max = 10, at = 'float', dv = 0.645)[0]
        adlYNode = mc.createNode("addDoubleLinear", n = '{}Y{}Adl'.format(name, side))
        adlYAttrName = utaCore.addAttrCtrl (ctrl = adlYNode, elem = ['default'], min = 0, max = 10, at = 'float', dv = 0.645)[0]

        rampAvgMdv = mc.createNode('multiplyDivide', n = '{}{}Mdv'.format(name, side))
        mc.setAttr('{}.input2X'.format(rampAvgMdv), 0.15)
        mc.setAttr('{}.input2Y'.format(rampAvgMdv), 0.08)

        mc.connectAttr('{}.outputX'.format(rampAvgMdv), '{}.input2'.format(adlXNode))
        mc.connectAttr('{}.outputY'.format(rampAvgMdv), '{}.input2'.format(adlYNode))
        mc.connectAttr('{}.{}'.format(adlXNode, adlXAttrName), '{}.input1'.format(adlXNode))
        mc.connectAttr('{}.{}'.format(adlYNode, adlYAttrName), '{}.input1'.format(adlYNode))

        mc.connectAttr('{}.ty'.format(ctrl), '{}.input1X'.format(rampAvgMdv))
        mc.connectAttr('{}.ty'.format(ctrl), '{}.input1Y'.format(rampAvgMdv))
        mc.connectAttr('{}.output'.format(adlXNode), '{}.colorEntryList[0].position'.format(rampNode))
        mc.connectAttr('{}.output'.format(adlYNode), '{}.colorEntryList[2].position'.format(rampNode))
        mc.connectAttr('{}.output'.format(adlXNode), '{}.colorEntryList[0].position'.format(rampAlphaNode))
        mc.connectAttr('{}.output'.format(adlYNode), '{}.colorEntryList[1].position'.format(rampAlphaNode))
        mc.setAttr('{}.colorEntryList[1].position'.format(rampNode), 1)

        mc.connectAttr('{}.outColor'.format(rampNode), '{}.inputs[0].color'.format(lytNode))
        mc.connectAttr('{}.outAlpha'.format(rampAlphaNode), '{}.inputs[0].alpha'.format(lytNode))

        mc.setAttr("{}.inputs[0].blendMode".format(lytNode), 1)



    mc.connectAttr('{}.outColor'.format(headFile), '{}.inputs[1].color'.format(lytNode))
    mc.connectAttr('{}.outColor'.format(lytNode), '{}.color'.format(blinnNode))
    mc.setAttr("{}.inputs[1].blendMode".format(lytNode), 1)

    ## set selectionChildHighlighting
    faceUiGeoGrp = 'FaceUiRigGeo_Grp'
    if mc.objExists(faceUiGeoGrp):
        geoLists = utaCore.listGeo(sels = faceUiGeoGrp, nameing = '_Geo', namePass = '_Grp')
        for each in geoLists:
            each = each.split('|')[-1]
            mc.setAttr('{}.selectionChildHighlighting'.format(each), 0)

    mc.select(geo)
    previewObj = utaCore.assignSelectionToShader(blinnNode)


    ## locck attributre Seq
    cn.lockAttr(listObj = [ctrl], attrs = ['tx', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v'],lock = True, keyable = False)

def generateUvSetRampCtrl(headRamp = True, ctrl = '', folderName = '', elem = '', geo ='', texturePath = '', *args):
    ## checkName Char
    # charName = utaCore.checkCharacterName()

    # Get the list of all files and directories
    # dir_list = os.listdir(texturePath)
    # for each in dir_list:
    #     if folderName == each:
    #         recheckLists = texturePath + '/' + folderName
    #         lists = os.listdir(recheckLists)
    # checkElem = [elem, elem.capitalize(), elem.lower()]
    # for objName in checkElem:
    #     for i in range(len(lists)):
    #         if objName in lists[i] and 'Col' in lists[i]:
    #             pathFileName = texturePath + '/' + folderName + '/' + lists[i]

    texturePath = texturePath.split('/preview')[0]
    charName = texturePath.split('/')[5]
    dir_list = os.listdir(texturePath)
    for each in dir_list:
        if '{}_Diffuse_Head.png'.format(charName) == each:
            pathFileName = texturePath + '/' + '{}_Diffuse_Head.png'.format(charName)
    ## split Name
    name, side, lastName = cn.splitName(sels= [ctrl])

    lytNode, lytSGNode = utaCore.generateMaterial(materialName = 'layeredTexture', name = name, side = side, shortName = 'Lyt')
    blinnNode, blinnSGNode = utaCore.generateMaterial(materialName = 'blinn', name = name, side = side, shortName = 'Bln')

    mc.select(geo)
    previewObj = utaCore.assignSelectionToShader(blinnNode)
    mc.connectAttr('{}.outColor'.format(lytNode), '{}.color'.format(blinnNode))
    mc.setAttr("{}.inputs[1].blendMode".format(lytNode), 1)


    ## generate file head color and link path texture file
    # headFile = mc.shadingNode('file', asShader=True, n = '{}Head{}File'.format(name, side))
    texturePlaneUiFile , texturePlaneUiPlace2DTexture = cn.generateFilePlace2DTexture(name = name + 'Head', side = side, elem = 'PlaneUi', node = 'file')
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(texturePlaneUiFile, pathFileName))
    mc.connectAttr('{}.outColor'.format(texturePlaneUiFile), '{}.inputs[1].color'.format(lytNode))

    if headRamp:
        # attrName = addAttrCtrl (ctrl = ctrl, elem = ['RampSlide'], min = 0, max = 10, at = 'float', dv = 0)[0]
        rampNode , rampNodePlace2DTexture = cn.generateFilePlace2DTexture(name = name , side = side, elem = 'PlaneUi', node = 'ramp',)
        # rampNode = utaCore.create_shader('{}{}Rmp'.format(name, side), node_type="ramp")[0]
        mc.setAttr("{}.interpolation".format(rampNode), 4)

        rampAlphaNode , rampAlphaNodePlace2DTexture = cn.generateFilePlace2DTexture(name = name + 'Alpha' , side = side, elem = 'PlaneUi', node = 'ramp')
        # rampAlphaNode = utaCore.create_shader('{}Alpha{}Rmp'.format(name, side), node_type="ramp")[0]
        mc.setAttr("{}.interpolation".format(rampAlphaNode), 4)

        mc.setAttr("{}.colorEntryList[1].color".format(rampNode),0.0627, 0.2274, 0.2941)
        # mc.setAttr("{}.colorEntryList[2].color".format(rampNode),0.0627, 0.2274, 0.2941)
        # mc.setAttr("{}.colorEntryList[0].color".format(rampNode),0, 0, 0)

        mc.setAttr("{}.colorEntryList[1].color".format(rampAlphaNode),1, 1, 1)
        mc.setAttr("{}.colorEntryList[0].color".format(rampAlphaNode),0, 0, 0)

        adlXNode = mc.createNode("addDoubleLinear", n = '{}X{}Adl'.format(name, side))
        adlXAttrName = utaCore.addAttrCtrl (ctrl = adlXNode, elem = ['default'], min = 0, max = 10, at = 'float', dv = 0.99)[0]
        adlYNode = mc.createNode("addDoubleLinear", n = '{}Y{}Adl'.format(name, side))
        adlYAttrName = utaCore.addAttrCtrl (ctrl = adlYNode, elem = ['default'], min = 0, max = 10, at = 'float', dv = 1)[0]

        rampAvgMdv = mc.createNode('multiplyDivide', n = '{}{}Mdv'.format(name, side))
        mc.setAttr('{}.input2X'.format(rampAvgMdv), 0.4)
        mc.setAttr('{}.input2Y'.format(rampAvgMdv), 0.142)

        mc.connectAttr('{}.outputX'.format(rampAvgMdv), '{}.input2'.format(adlXNode))
        mc.connectAttr('{}.outputY'.format(rampAvgMdv), '{}.input2'.format(adlYNode))
        mc.connectAttr('{}.{}'.format(adlXNode, adlXAttrName), '{}.input1'.format(adlXNode))
        mc.connectAttr('{}.{}'.format(adlYNode, adlYAttrName), '{}.input1'.format(adlYNode))

        mc.connectAttr('{}.ty'.format(ctrl), '{}.input1X'.format(rampAvgMdv))
        mc.connectAttr('{}.ty'.format(ctrl), '{}.input1Y'.format(rampAvgMdv))
        # mc.connectAttr('{}.output'.format(adlXNode), '{}.colorEntryList[0].position'.format(rampNode))
        # mc.connectAttr('{}.output'.format(adlYNode), '{}.colorEntryList[2].position'.format(rampNode))
        mc.connectAttr('{}.output'.format(adlXNode), '{}.colorEntryList[0].position'.format(rampAlphaNode))
        mc.connectAttr('{}.output'.format(adlYNode), '{}.colorEntryList[1].position'.format(rampAlphaNode))
        # mc.setAttr('{}.colorEntryList[1].position'.format(rampNode), 1)

        mc.connectAttr('{}.outColor'.format(rampNode), '{}.inputs[0].color'.format(lytNode))
        mc.connectAttr('{}.outAlpha'.format(rampAlphaNode), '{}.inputs[0].alpha'.format(lytNode))

        mc.setAttr("{}.inputs[0].blendMode".format(lytNode), 1)

        ## uvChoo
        uvChooserNode = mc.createNode('uvChooser', n = '{}Head{}Uvc'.format(name, side))
        mc.connectAttr('{}Shape.uvSet[1].uvSetName'.format(geo), '{}.uvSets[0]'.format(uvChooserNode))
        # mc.connectAttr('{}Shape.uvSet[1].uvSetName HeadRampHead_Uvc.uvSets[0];

        mc.connectAttr('{}.outUv'.format(uvChooserNode), '{}.uvCoord'.format(rampAlphaNodePlace2DTexture))
        mc.connectAttr('{}.outVertexCameraOne'.format(uvChooserNode), '{}.vertexCameraOne'.format(rampAlphaNodePlace2DTexture))
        mc.connectAttr('{}.outVertexUvOne'.format(uvChooserNode), '{}.vertexUvOne'.format(rampAlphaNodePlace2DTexture))
        mc.connectAttr('{}.outVertexUvThree'.format(uvChooserNode), '{}.vertexUvThree'.format(rampAlphaNodePlace2DTexture))
        mc.connectAttr('{}.outVertexUvTwo'.format(uvChooserNode), '{}.vertexUvTwo'.format(rampAlphaNodePlace2DTexture))

    ## set selectionChildHighlighting
    faceUiGeoGrp = 'FaceUiRigGeo_Grp'
    if mc.objExists(faceUiGeoGrp):
        geoLists = utaCore.listGeo(sels = faceUiGeoGrp, nameing = '_Geo', namePass = '_Grp')
        for each in geoLists:
            each = each.split('|')[-1]
            mc.setAttr('{}.selectionChildHighlighting'.format(each), 0)

    ## locck attributre Seq
    cn.lockAttr(listObj = [ctrl], attrs = ['tx', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v'],lock = True, keyable = False)


def addBshToFclRig( source = '', 
                    target = '',
                    sourceProcess = '', 
                    targetProcess = '', 
                    frontOfChain = True, 
                    origin = 'local', 
                    changOrderBsh = False, *args):
    
    sourceGeo = utaCore.listGeo(sels = source, nameing = '_Geo', namePass = '_Grp')
    targetGeo = utaCore.listGeo(sels = target, nameing = '_Geo', namePass = '_Grp')

    ## target Sp
    if ':' in target:
        targetNsLists = target.split(':')[0]
        targetNs  = targetNsLists + ':'
    else:
        targetNs = ''
    for x in range(len(sourceGeo)):
        ## source Sp
        sourceGeoSp = sourceGeo[x].split('|')[-1]
        sourceNs = sourceGeo[x].split(':')[0]
        sourceNameSp, sourceSide, sourceLastName = cn.splitName(sels = [sourceGeoSp])
        sourceName = sourceNameSp.replace(sourceProcess, targetProcess)
        targetGeoSel = '{}{}{}{}'.format(targetNs, sourceName, sourceSide, sourceLastName)
        mc.select(sourceGeoSp)
        mc.select(targetGeoSel,add = True)
        # cn.add_blendShape(sourceGeoSp, targetGeoSel, frontOfChain, origin)

        # mc.select(fclRigGeo)
        # mc.select(newBodyGeo, add = True)
        bshTools.add_blendShape(sourceGeoSp, targetGeoSel, origin)
    ## chang order blendShape
    if changOrderBsh:
        changOrderBlendShape(sels = target, skinChangeBsn = True,)

def addBshFclToFaceGeo(fclGeoGrp = 'fclRig_md:FclRigGeo_Grp', origin = 'world', *args):
    geoAll = utaCore.listGeo(sels = fclGeoGrp, nameing = '_Geo', namePass = '_Grp')
    for eachGeo in geoAll:
        fclRigGeo = eachGeo.split('|')[-1]

        elemSp, side, lastName = cn.splitName(sels = [fclRigGeo])
        nameGeo = elemSp.split('FclRig')[0]

        newBodyGeo = '{}:{}{}{}'.format('bodyRig_md', nameGeo, side, lastName)
        if mc.objExists(newBodyGeo):
            mc.select(fclRigGeo)
            mc.select(newBodyGeo, add = True)
            bshTools.add_blendShape(fclRigGeo, newBodyGeo, origin)
            # cn.add_blendShape(fclRigGeo, newBodyGeo, True , origin)
        else:
            print False, ':::', newBodyGeo

def setDefaultPublishMain(*args):
    locLists = mc.listRelatives('fclRig_md:PlaneUiRigGeo_Grp', c = True, f = True)
    for eachLoc in locLists:
        obj = eachLoc.split('|')[-1]
        viewLists =  mc.listRelatives(obj, c = True, f = True)
        for viewObj in viewLists:
            if 'View' in viewObj:
                viewObj = viewObj.split('|')[-1]

                mc.setAttr('{}.overrideEnabled'.format(viewObj), 1)
                mc.setAttr('{}.overrideDisplayType'.format(viewObj), 2)
                mc.setAttr('{}.selectionChildHighlighting'.format(viewObj), 1)

def addAttrDtlVisToUiCtrl(dtlRigCtrlGrp = 'dtlRig_md:DtlRigCtrl_Grp', attrVis = 'detailVis', *args):

    dtlCtrls = []
    uiCtrlAll = []
    dtlCtrlList = mc.listRelatives(dtlRigCtrlGrp, c = True, f = True)
    for dtlCtrl in dtlCtrlList:
        ctrl = dtlCtrl.split('|')[-1]
        ctrlName = ctrl.split(':')[-1]
        ctrlNs = ctrl.split(':')[0]
        elemSp, side, lastName = cn.splitName(sels = [ctrlName])
        dtlCtrls.append(ctrl)

        nameSp = elemSp.split('Plane')[0]
        uiCtrlAllCheck = 'uiCtrl_md:{}UiRig{}Ctrl'.format(nameSp, side)

        if mc.objExists(uiCtrlAllCheck):
            cn.addAttrCtrl (ctrl = uiCtrlAllCheck, elem = [attrVis], min = 0, max = 1, at = 'long')
            mc.connectAttr('{}.{}'.format(uiCtrlAllCheck, attrVis), '{}.v'.format(ctrl))


def stick_detail_controls(sels = [], mesh = '',*args):
    reload(rivetTools)
    reload(core)
    Node = core.Node()

    sels = pmc.ls(sl = True)
    # mesh = get_text_in_textField('MeshStickTF')
    rvtGrp = 'RivetAll_Grp'

    if sels :
        if mesh :
            if not pmc.objExists(rvtGrp) :
                rvtGrp = Node.transform(rvtGrp)
            else :
                rvtGrp = core.Dag(rvtGrp)

            for sel in sels :
                rvt = rivetTools.stick_control_to_closest_vertex(sel.nodeName(), mesh)
                pmc.parent(rvt[0], rvtGrp)

def stickDetailControl(dtlCtrlGrp = ['fclRig_md:DtlRigCtrl_Grp'], dtlRigGeoGrp = 'fclRig_md:FacePlaneDtlRigGeo_Grp',swRigGeoGrp = 'fclRig_md:FacePlaneSwRigGeo_Grp', *args):

    ## addBlendShape DtlRigGeo to SwRigGeo
    addBshToFclRig(source = dtlRigGeoGrp, target = swRigGeoGrp, sourceProcess = 'DtlRig', targetProcess = 'SwRig', frontOfChain = True, origin = 'local', changOrderBsh = True)

    dtlCtrlLists = utaCore.listGeo(sels = dtlCtrlGrp, nameing = '_Ctrl', namePass = '_Grp')
    ## add Stick detail control
    for i in range(len(dtlCtrlLists)):
        selDtlCtrl = dtlCtrlLists[i].split('|')[-1] 
        nameSp, side, lastName = cn.splitName(sels = [selDtlCtrl])

        planeGeo = 'bodyRig_md:{}{}Geo'.format(nameSp.split('DtlRig')[0], side )
        if mc.objExists(planeGeo):
            mc.select(selDtlCtrl)
            stick_detail_controls(sels = [selDtlCtrl], mesh = planeGeo)
        else:
            print False, ':', planeGeo


def changOrderBlendShape(sels = '', skinChangeBsn = True, *args):
    ## Chang order of blensShape node
    geoAll = utaCore.listGeo(sels = sels, nameing = '_Geo', namePass = '_Grp')
    for eachGeo in geoAll:
        obj = eachGeo.split('|')[-1]
        if ':' in obj:
            BsnName = obj.split(':')[-1]

        skin = utaCore.getSkinClusterMel(geo = obj)
        bsn = BsnName.replace('_Geo', '_Bsn')
        # print skin, '>>', bsn, '>>>>', obj

        if skin:
            if skinChangeBsn:
                # print 'Skin, bsn, obj'
                mc.reorderDeformers( skin, bsn, obj)
            else:
                # print 'bsn, skin, obj'
                mc.reorderDeformers( bsn, skin, obj)

def orientScaleConstraintDtlRigControl(dtlCtrlGrp = ['fclRig_md:DtlRigCtrl_Grp'], headGmblCtrl = 'bodyRig_md:HeadGmbl_Ctrl', *args):

    # add orient and scale contraint
    dtlCtrlChild = mc.listRelatives(dtlCtrlGrp, c = True, f = True)
    for eachCtrl in dtlCtrlChild:
        obj = eachCtrl.split('|')[-1]
        utaCore.parentScaleConstraintLoop(obj = headGmblCtrl, lists = [obj],par = False, sca = True, ori = True, poi = False)
    nameSp = dtlCtrlGrp[0].split(':')[0]
    haRigCtrl = '{}:HdRigCtrl_Grp'.format(nameSp)
    faceUiRig = '{}:FacialUiRigCtrl_Grp'.format(nameSp)
    if mc.objExists(haRigCtrl):
        utaCore.parentScaleConstraintLoop(obj = headGmblCtrl, lists = [haRigCtrl],par = True, sca = True, ori = False, poi = False)
    else:
        print False, ':', haRigCtrl

    if mc.objExists(faceUiRig):
        utaCore.parentScaleConstraintLoop(obj = 'bodyRig_md:Offset_Ctrl', lists = [faceUiRig],par = False, sca = False, ori = False, poi = True)
    else:
        print False, ':', faceUiRig

def wrapGroupProcessMain(*args):
    fclStillGrp = 'FclRigStillGrp_Grp'
    fclCtrlGrp = 'FclRigCtrlGrp_Grp'

    if not mc.objExists(fclCtrlGrp):
        fclCtrlGrp = mc.group(em = True, n = fclCtrlGrp)

    if not mc.objExists(fclStillGrp):
        fclStillGrp = mc.group(em = True, n = fclStillGrp)

    ## hide geo un-use
    guideLineGeo = ['fclRig_md:GuildeLineYUiRig_Geo', 'fclRig_md:GuildeLineXUiRig_Geo']
    for each in guideLineGeo:
        if each:
            mc.setAttr('{}.v'.format(each), 0)

    mc.parent('fclRig_md:FclRigCtrl_Grp', fclCtrlGrp)
    mc.parent('fclRig_md:FclRigGeo_Grp', 'fclRig_md:FclRigStill_Grp', fclStillGrp)
    mc.parent(fclCtrlGrp, 'bodyRig_md:FacialCtrl_Grp')
    mc.parent(fclStillGrp, 'RivetAll_Grp', 'bodyRig_md:Still_Grp')

def generateMainUiCtrl(fclRigGeoGrp = 'FclRigGeo_Grp', fclPlaneGeoGrp = 'FacePlaneFclRigGeo_Grp', *args):
    faceUiRigPos = mc.group(n = 'FacialUiRig_Grp', em = True)
    headUiRigPos = mc.group(n = 'HeadRamp_Grp', em = True)

    mc.select(faceUiRigPos)
    faceUiRigCtrlNameGrp, faceUiRigCtrlOfsName, faceUiRigCtrlParsName, faceUiRigCtrlName, faceUiRigGmblCtrlName = utaCore.createControlCurve(curveCtrl = 'cube', parentCon = False, connections = False, geoVis = False)
    faceUiRigCtrlNameGrp = mc.rename(faceUiRigCtrlNameGrp, faceUiRigCtrlNameGrp.replace('Zro', ''))
    mc.parent(faceUiRigCtrlName, faceUiRigCtrlNameGrp)
    mc.setAttr('{}.scaleX'.format(faceUiRigCtrlNameGrp), 4)
    mc.setAttr('{}.scaleY'.format(faceUiRigCtrlNameGrp), 4)
    mc.setAttr('{}.scaleZ'.format(faceUiRigCtrlNameGrp), 0.5)
    ## assignColor color
    mc.setAttr('{}.overrideEnabled'.format(faceUiRigCtrlName), 1)
    utaCore.assignColor(obj = [faceUiRigCtrlName], col = 'blue')
    ## freez transform
    pm.makeIdentity(faceUiRigCtrlNameGrp, apply = True)
    cn.lockAttr(listObj = ['{}Shape'.format(faceUiRigCtrlName)], attrs = ['gimbalControl'],lock = True, keyable = False)
    mc.delete(faceUiRigPos, faceUiRigCtrlOfsName, faceUiRigGmblCtrlName)


    mc.select(headUiRigPos)
    headCtrlNameGrp, headCtrlOfsName, headCtrlParsName, headCtrlName, headGmblCtrlName = utaCore.createControlCurve(curveCtrl = 'arrowLR', parentCon = False, connections = False, geoVis = False)
    headCtrlNameGrp = mc.rename(headCtrlNameGrp, headCtrlNameGrp.replace('Zro', ''))
    mc.parent(headCtrlName, headCtrlNameGrp)
    mc.setAttr('{}.translateY'.format(headCtrlNameGrp), 3.3)
    mc.setAttr('{}.translateX'.format(headCtrlNameGrp), -4.5)
    mc.setAttr('{}.rotateX'.format(headCtrlNameGrp), 90)
    mc.setAttr('{}.rotateZ'.format(headCtrlNameGrp), -90)
    mc.setAttr('{}.scaleX'.format(headCtrlNameGrp), 0.14)
    mc.setAttr('{}.scaleY'.format(headCtrlNameGrp), 0.14)
    mc.setAttr('{}.scaleZ'.format(headCtrlNameGrp), 0.14)

    # ## set limit control for plane geo 
    utaCore.controlLimitTRS (obj = [headCtrlName], translate = True, rotate = False, ValueX = [], ValueY = [-7, 0], ValueZ = [])


    ## assignColor color
    mc.setAttr('{}.overrideEnabled'.format(headCtrlName), 1)
    utaCore.assignColor(obj = [headCtrlName], col = 'softBlue')
    ## freez transform
    pm.makeIdentity(headCtrlNameGrp, apply = True)
    cn.lockAttr(listObj = ['{}Shape'.format(headCtrlName)], attrs = ['gimbalControl'],lock = True, keyable = False)
    mc.delete(headUiRigPos, headCtrlOfsName, headGmblCtrlName)    


    ## Geo grp
    FaceUiRigGeoGrp = mc.group(n = 'FaceUiRigGeo_Grp', em = True)
    CtrlUiRigGrp = mc.group(n = 'CtrlUiRigCtrl_Grp' , em = True)
    PlaneUiRigGeoGrp = mc.group(n = 'PlaneUiRigGeo_Grp', em = True)


    ## generete Vis_ctrl Title
    visVisCtrlGrp = utaCore.geneteateCurveType(menuName = ['Vis'], elem = 'UiRig', side = '', color = 'greenWhite', template = False)[0]
    mc.setAttr('{}.translateX'.format(visVisCtrlGrp), 2.7)
    mc.setAttr('{}.translateY'.format(visVisCtrlGrp), 4.2)
    visCtrl = utaCore.listGeo(sels = visVisCtrlGrp, nameing = '_Ctrl', namePass = '_Grp')[0]
    visCtrlName = visCtrl.split('|')[-1]
    ## freez transform
    pm.makeIdentity(visVisCtrlGrp, apply = True)


    # generete Facial_ctrl Title
    facialVisCtrlGrp = utaCore.geneteateCurveType(menuName = ['FacialUI'], elem = 'ViewUiRig', side = '', color = '', template = True)[0]
    mc.setAttr('{}.translateX'.format(facialVisCtrlGrp), -1.8)
    mc.setAttr('{}.translateY'.format(facialVisCtrlGrp), 4.2)
    ## freez transform
    pm.makeIdentity(facialVisCtrlGrp, apply = True)

    ## locck attributre
    cn.lockAttr(listObj = [headCtrlName], attrs = ['tx', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz'],lock = True, keyable = False)
    cn.lockAttr(listObj = [visCtrlName], attrs = ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v'],lock = True, keyable = False)

    ## wrap group
    mc.parent(FaceUiRigGeoGrp, CtrlUiRigGrp, PlaneUiRigGeoGrp, visVisCtrlGrp, facialVisCtrlGrp, headCtrlNameGrp, faceUiRigCtrlName)

    if fclPlaneGeoGrp:
        utaCore.centerPivotObj (obj = [fclPlaneGeoGrp])
        utaCore.snapObj(fclPlaneGeoGrp, faceUiRigCtrlNameGrp)
    return faceUiRigCtrlNameGrp, FaceUiRigGeoGrp, CtrlUiRigGrp, PlaneUiRigGeoGrp, visVisCtrlGrp, visCtrlName

def generateMenuViewUiCtrl (name = '', elem = '', axis = ['Y', 'X'], snapPos = [], parent = '',*args):

    TitleBar = []
    for i in range(len(axis)):
        sideEmpty = '_'
        if 'Y' in axis[i]:
            rotateValue = 90
            w = 5
            h = 0.25
        elif 'X' in axis[i]:
            rotateValue = 0
            w = 0.25
            h = 5

        ## generate polyPlane  
        polyPlaneObj = mc.polyPlane(n = '{}Title{}Bar{}{}Geo'.format(name, axis[i], elem, sideEmpty),  w = w, h = h, sx = 1, sy = 1,ax=( 0 ,1,0), cuv= 2 ,ch= 1)[0]

        mc.setAttr('{}.rx'.format(polyPlaneObj), 90)
        utaCore.clean ( target = polyPlaneObj)
        ## set value render stats
        mc.setAttr('{}Shape.castsShadows'.format(polyPlaneObj), 0)
        mc.setAttr('{}Shape.receiveShadows'.format(polyPlaneObj), 0)
        mc.setAttr('{}Shape.motionBlur'.format(polyPlaneObj), 0)
        mc.setAttr('{}Shape.primaryVisibility'.format(polyPlaneObj), 0)
        mc.setAttr('{}Shape.smoothShading'.format(polyPlaneObj), 0)
        mc.setAttr('{}Shape.doubleSided'.format(polyPlaneObj), 0)

        ## generate position of locator
        mc.select('{}.vtx[1]'.format(polyPlaneObj))
        select = lrr.locatorOnMidPos()
        mc.select(select)
        jntLocPos = utaCore.jointFromLocator()
        jntLocPos = mc.rename(jntLocPos, '{}Title{}Bar{}{}Jnt'.format(name, axis[i], elem, sideEmpty))
        mc.setAttr("{}.radius".format(jntLocPos), 0.1)
        mc.setAttr("{}.drawStyle".format(jntLocPos), 2)

        ## wrap obj of Texture Ui Ctrl
        mc.parent(polyPlaneObj, jntLocPos)

        if snapPos:
            utaCore.snapObj(objA = snapPos[i], objB = jntLocPos)

        if parent:
            mc.parent(jntLocPos, parent)

        TitleBar.append(jntLocPos)

    return TitleBar

def generateViewUiCtrl(numberNo = 1, name = 'Face', elem = 'UiRig',  menuName = ['cheek', 'jaw'], sides = [['R', 'L'],['J'] ], position =[0,5], parent= '',*args):

    sideEmpty = '_'
    ## generate localtor
    locatorObj = mc.spaceLocator(n = '{}{}{}loc'.format(name, elem, sideEmpty), p = (0, 0, 0))[0]

    ## generate polyPlane
    polyPlaneObj = mc.polyPlane(n = '{}{}{}Geo'.format(name, elem, sideEmpty),  w = 5, h = 5, sx = 10, sy = 10,ax=( 0 ,1,0), cuv= 2 ,ch= 1)[0]
    mc.setAttr('{}.rx'.format(polyPlaneObj), 90)
    utaCore.clean ( target = polyPlaneObj)
    ## set value render stats
    mc.setAttr('{}Shape.castsShadows'.format(polyPlaneObj), 0)
    mc.setAttr('{}Shape.receiveShadows'.format(polyPlaneObj), 0)
    mc.setAttr('{}Shape.motionBlur'.format(polyPlaneObj), 0)
    mc.setAttr('{}Shape.primaryVisibility'.format(polyPlaneObj), 0)
    mc.setAttr('{}Shape.smoothShading'.format(polyPlaneObj), 0)
    mc.setAttr('{}Shape.doubleSided'.format(polyPlaneObj), 0)

    ## generate position of locator
    mc.select('{}.vtx[10]'.format(polyPlaneObj))
    select = lrr.locatorOnMidPos()
    mc.select(select)
    jntLocPos = utaCore.jointFromLocator()
    utaCore.snapObj(objA = jntLocPos, objB = locatorObj)
    mc.delete(jntLocPos)
    pm.makeIdentity(locatorObj, apply = True)

    ## generate curve type
    menuNameLists = []
    textureUiCtrlLists = []
    maneNamePos = [-3.7, 2.2]
    menuPositionVtxValue = [110, 117]
    sidePositionVtxValue = [99, 44]

    ## dicts Positoin of side L R control
    dictsPosi = {}
    dictsPosi[0] = 99
    dictsPosi[1] = 88
    dictsPosi[2] = 77
    dictsPosi[3] = 66
    dictsPosi[4] = 55
    dictsPosi[5] = 44   
    dictsPosi[6] = 33
    dictsPosi[7] = 22
    dictsPosi[8] = 11
    dictsPosi[9] = 00
    position = [dictsPosi[position[0]], dictsPosi[position[-1]]]
 


    axis = ['sx', 'sy', 'sz']
    fontText = 'MS Shell Dlg 2, 27.8pt'
    for i in range(len(menuName)):
        attrLists = mc.textCurves(n = '{}{}View{}Grp'.format(menuName[i], elem, sideEmpty), f = fontText, t = menuName[i])[0]
    
        ## generate position of menuName
        mc.select('{}.vtx[{}]'.format(polyPlaneObj, menuPositionVtxValue[i]))
        select = lrr.locatorOnMidPos()
        mc.select(select)
        jntPos = utaCore.jointFromLocator()
        mc.setAttr('{}.translateY'.format(jntPos), 2.85)
        utaCore.snapObj(objA = jntPos, objB = attrLists)
        mc.delete(jntPos)
    
        ## scale viewCtrl
        for x in range(len(axis)):
            mc.setAttr('{}.{}'.format(attrLists, axis[x]), 0.3)
        
        curveLists = utaCore.listGeo(sels = attrLists, nameing = 'curve', namePass = 'Char')
        mc.parent(curveLists, attrLists)

        ## clean shape of menuName
        targetCtrl = ''
        sourceCtrlShape = []        
        for x in range(len(curveLists)):
            if x == 0:
                splitNameTarget = curveLists[x].split('|')[-1]
                utaCore.clean ( target = splitNameTarget)   
                targetCtrl = mc.rename(splitNameTarget, '{}View{}{}Ctrl'.format(menuName[i], elem, sideEmpty))

            else:
                splitNameSource = curveLists[x].split('|')[-1]
                utaCore.clean ( target = splitNameTarget)  
                sourceCtrlShape.append(mc.rename(splitNameSource, '{}{}View{}{}Ctrl'.format(menuName[i], elem, x+1, sideEmpty)))

        ## parent Shape
        utaCore.parentShapeCurve(source = sourceCtrlShape, target = targetCtrl)

        mc.setAttr("{}.template".format(targetCtrl), 1)
        mc.delete(sourceCtrlShape)

        ## rename
        newObj = mc.rename(attrLists, '{}ViewUiRigCtrl{}Grp'.format(menuName[i], sideEmpty))
        ## delete char empty 
        charEmpty = utaCore.listGeo(sels = newObj, nameing = 'Char', namePass = '_Ctrl')
        mc.delete(charEmpty)
        ## freez transform
        pm.makeIdentity(newObj, apply = True)
        menuNameLists.append(newObj)

        # ## generate curve of Side 'R' 'L'
        side = sides[i]
        if len(sides) == 2:
            fullOrHalf = False

        else:
            fullOrHalf = True
        menuSideLists, textureUiCtrlGrp, sideListsGrp = generateCurveType(nameMenu = menuName[i], elem = elem, sides = side, ctrlTranslateLimit = True, fullOrHalf = fullOrHalf, position = position, sclaeAxis = 0.3)

        ## generate position of menuName
        mc.select('{}.vtx[{}]'.format(polyPlaneObj, position[i]))
        select = lrr.locatorOnMidPos()
        mc.select(select)
        jntPos = utaCore.jointFromLocator()
        utaCore.snapObj(objA = jntPos, objB = textureUiCtrlGrp)
        pm.makeIdentity(textureUiCtrlGrp, apply = True)

        mc.delete(jntPos)

        ## set positoin of L side
        for eachSide in menuSideLists:
            if 'L' in eachSide:
                mc.setAttr('{}.translateX'.format(eachSide), 0.2)
                pm.makeIdentity(sideListsGrp, apply = True)

        textureUiCtrlLists.append(textureUiCtrlGrp)

    ## generate MenuTitle ABC, 123---------------------
    ## generate position for TitleBar axis Y
    mc.select('{}.vtx[120]'.format(polyPlaneObj))
    selectYPos = lrr.locatorOnMidPos()
    ## generate position for TitleBar axis X
    mc.select('{}.vtx[0]'.format(polyPlaneObj))
    selectXPos = lrr.locatorOnMidPos()

    TitleBar = generateMenuViewUiCtrl (name = name, elem = elem, axis = ['Y', 'X'], snapPos = [selectYPos, selectXPos], parent = locatorObj)
    mc.delete(selectYPos, selectXPos)


    ## set center L R 
    for each in textureUiCtrlLists:
        mc.setAttr('{}.translateX'.format(each), 0.09)
        mc.setAttr('{}.translateY'.format(each), 0.15)
        pm.makeIdentity(each, apply = True)

    ## wrap obj of Texture Ui Ctrl
    mc.parent(polyPlaneObj, menuNameLists, textureUiCtrlLists, locatorObj)

    if parent:
        mc.parent(locatorObj , parent)

    ## set position of texture plane uiCtrl
    texturePlaneNo(numberNo = numberNo, obj = locatorObj)
    mc.select(cl = True)


    return locatorObj 


def generateLocatorFacialPosition(*args):
    loc = [(7.2, 1.5), (12.5, 1.5), (18, 1.5), (7, -4), (12.5, -4), (18, -4)]


def generateFacialUiCtrl(fclRigGeoGrp = '', fclPlaneGeoGrp = '', ctrlUiRigGrp = '', faceUiRigGeoGrp = '', process = '', visCtrl = '', organizePlaneUiGeo = True, writeOrRead = False, distanceUiCtrlAmp = 1, mouthOpOne = 'TeethOne', mouthOpTwo = 'TeethTwo', mouthOpThree = 'TeethThree',*args):

    # organize planeUi Geo
    if organizePlaneUiGeo:
        generateOrganizePlaneUiGeo( FacePlaneFclRigGeoGrp = fclPlaneGeoGrp, 
                                    distanceUiCtrlAmp = distanceUiCtrlAmp,
                                    writeOrRead = writeOrRead)

    if not fclPlaneGeoGrp:
        geoAll = mc.ls(sl = True)
    else:
        geoAll = utaCore.listGeo(sels = fclPlaneGeoGrp, nameing = '_Geo', namePass = '_Grp')

    ctrlAll = []
    ctrlGrpAll = []
    shadowList = ''
    for eachGeo in geoAll:


        eachGeo = eachGeo.split('|')[-1]
        checkName = eachGeo.split('Plane')[0]
        oldName, side, lastName = cn.splitName(sels = [eachGeo])
        name = oldName.replace('PlaneFcl', 'Ui')
        if 'Shadow' in checkName:
            shadowList = checkName

        ## get crvDict shape for read shape control
        if 'Eyebrow' in checkName:
            ctrlShapeName = checkName
        elif 'EyeMask' in checkName:
            ctrlShapeName = checkName
        elif 'Wrinkle' in checkName:
            ctrlShapeName = checkName
        elif 'Eye' in checkName:
            ctrlShapeEyeName = checkName
            ctrlShapeBlinkName = 'Blink'
            ctrlShapeIrisName = 'Iris'
            ctrlShapeSpecularName = 'Specular'  
            ctrlShapeSpecularSpBName = 'SpecularSpB'  
            ctrlShapeSpecularSpAName = 'SpecularSpA'  

            ctrlShapeEyeMaskName = 'EyeMask'  
        elif 'Cheek' in checkName:
            ctrlShapeName = checkName    
        elif 'Mouth' in checkName:
            ctrlShapeName = checkName
            # ctrlShapeTeethUpName = ''
            # ctrlShapeTongueName = ''
            # ctrlShapeTeethDnName = ''
            ctrlShapeTeethUpName = mouthOpOne
            ctrlShapeTongueName = mouthOpTwo
            ctrlShapeTeethDnName = mouthOpThree

        elif 'Jaw' in checkName:
            ctrlShapeName = checkName 
        elif 'MouthEffUp' in checkName:
            ctrlShapeName = checkName       
        elif 'MouthEffDn' in checkName:
            ctrlShapeName = checkName   
        elif 'Tear' in checkName:
            ctrlShapeName = checkName   
        else:
            ctrlShapeName = checkName


        if 'Eye' == checkName:
            rootGrp = mc.group(n = '{}Ball{}Ctrl{}Grp'.format(checkName, process, side), em = True)
            eyeCtrl = mc.circle( n = '{}{}{}Ctrl'.format(checkName, process, side),nr=(0, 0, 1), c=(0, 0, 0) , ch= 0)[0]
            # set Ctrl Shape
            crvFunc.set_shape(eyeCtrl,'ca{}'.format(checkName))
            zroCtrlGrp = utaCore.generateGroup(sels = [eyeCtrl], elem = '')
            eyeCtrlGrp = utaCore.generateGroup(sels = [zroCtrlGrp], elem = '')

            utaCore.setAndCleanAttr(obj = eyeCtrlGrp, translate =  [0,0,0] , rotate = [0,0,0], scale = [1,1,1], color = 'yellow')
            mc.parent(eyeCtrlGrp, rootGrp)

            # irisName = 'Iris'
            irisCtrl = utaCore.curveCtrlShape(curveCtrl = 'circle', elem  = '{}{}{}Ctrl'.format(ctrlShapeIrisName, process, side))
            # set Ctrl Shape
            crvFunc.set_shape(irisCtrl,'ca{}'.format(ctrlShapeIrisName))
            irisCtrlGrp = utaCore.generateGroup(sels = [irisCtrl], elem = '')
            utaCore.setAndCleanAttr(obj = irisCtrlGrp, translate =  [0,0,0] , rotate = [0,0,0], scale = [1,1,1], color = 'red')
            mc.parent(irisCtrlGrp, eyeCtrl)

            # specularName = 'specular'
            specularCtrl = utaCore.curveCtrlShape(curveCtrl = 'circle', elem  = '{}{}{}Ctrl'.format(ctrlShapeSpecularName, process, side))
            # set Ctrl Shape
            crvFunc.set_shape(specularCtrl,'ca{}'.format(ctrlShapeSpecularName))
            specularCtrlGrp = utaCore.generateGroup(sels = [specularCtrl], elem = '')
            utaCore.setAndCleanAttr(obj = specularCtrlGrp, translate =  [0,0,0] , rotate = [0,0,0], scale = [1,1,1], color = 'white')
            mc.parent(specularCtrlGrp, irisCtrl)

            # specularSpAName = 'specularSpA'
            specularSpACtrl = utaCore.curveCtrlShape(curveCtrl = 'circle', elem  = '{}{}{}Ctrl'.format(ctrlShapeSpecularSpAName, process, side))
            # set Ctrl Shape
            crvFunc.set_shape(specularSpACtrl,'ca{}{}'.format(ctrlShapeSpecularSpAName, side))
            specularSpACtrlGrp = utaCore.generateGroup(sels = [specularSpACtrl], elem = '')
            utaCore.setAndCleanAttr(obj = specularSpACtrlGrp, translate =  [0,0,0] , rotate = [0,0,0], scale = [1,1,1], color = 'white')
            utaCore.centerPivotObj (obj = [specularSpACtrl])
            mc.parent(specularSpACtrlGrp, irisCtrl)

            # specularSpBName = 'specularSpB'
            specularSpBCtrl = utaCore.curveCtrlShape(curveCtrl = 'circle', elem  = '{}{}{}Ctrl'.format(ctrlShapeSpecularSpBName, process, side))
            # set Ctrl Shape
            crvFunc.set_shape(specularSpBCtrl,'ca{}{}'.format(ctrlShapeSpecularSpBName, side))
            specularSpBCtrlGrp = utaCore.generateGroup(sels = [specularSpBCtrl], elem = '')
            utaCore.setAndCleanAttr(obj = specularSpBCtrlGrp, translate =  [0,0,0] , rotate = [0,0,0], scale = [1,1,1], color = 'white')
            utaCore.centerPivotObj (obj = [specularSpBCtrl])
            mc.parent(specularSpBCtrlGrp, irisCtrl)


            # blinkName = 'blink'
            blinkCtrl = utaCore.curveCtrlShape(curveCtrl = 'circle', elem  = '{}{}{}Ctrl'.format(ctrlShapeBlinkName, process, side))
            # set Ctrl Shape
            crvFunc.set_shape(blinkCtrl,'ca{}'.format(ctrlShapeBlinkName))
            blinkCtrlGrp = utaCore.generateGroup(sels = [blinkCtrl], elem = '')
            utaCore.setAndCleanAttr(obj = blinkCtrlGrp, translate =  [0,0.7,0] , rotate = [0,0,0], scale = [1,1,1], color = 'white')
            mc.parent(blinkCtrlGrp, eyeCtrl)

            eyeMaskUpName = ctrlShapeEyeMaskName + 'Up'
            eyeMaskUpCtrl = utaCore.curveCtrlShape(curveCtrl = 'circle', elem  = '{}{}{}Ctrl'.format(eyeMaskUpName, process, side))
            # set Ctrl Shape
            crvFunc.set_shape(eyeMaskUpCtrl,'ca{}'.format(eyeMaskUpName))
            eyeMaskUpCtrlGrp = utaCore.generateGroup(sels = [eyeMaskUpCtrl], elem = '')
            utaCore.setAndCleanAttr(obj = eyeMaskUpCtrlGrp, translate =  [0,1.25,0] , rotate = [0,0,0], scale = [1,1,1], color = 'red')
            mc.parent(eyeMaskUpCtrlGrp, rootGrp)

            eyeMaskDnName = ctrlShapeEyeMaskName + 'Dn'
            eyeMaskDnCtrl = utaCore.curveCtrlShape(curveCtrl = 'circle', elem  = '{}{}{}Ctrl'.format(eyeMaskDnName, process, side))
            # set Ctrl Shape
            crvFunc.set_shape(eyeMaskDnCtrl,'ca{}'.format(eyeMaskDnName))            
            eyeMaskDnCtrlGrp = utaCore.generateGroup(sels = [eyeMaskDnCtrl], elem = '')
            utaCore.setAndCleanAttr(obj = eyeMaskDnCtrlGrp, translate =  [0,-1.25,0] , rotate = [0,0,0], scale = [1,1,1], color = 'red')
            mc.setAttr('{}.rx'.format(eyeMaskDnCtrlGrp), 180)
            mc.parent(eyeMaskDnCtrlGrp, rootGrp)

            ## snap position 
            utaCore.snapPosition(selSource = rootGrp, selTarget = eachGeo)

            ## uiCtrlGeo parent eyeCtrl
            uiCtrlGeoName = oldName.replace('Fcl', 'Ui')
            newGeoName = mc.rename(eachGeo, '{}{}{}'.format(uiCtrlGeoName, side, lastName))

            ## get position of facialUiCtrl (same position tx)
            findFacialUiCtrlPos(grp = fclRigGeoGrp, tzValue = 2, objSource = newGeoName, objTarget = rootGrp)

            ## rotate Axis
            if '_R_' in rootGrp:
                mc.setAttr('{}.ry'.format(rootGrp), -180)
                mc.setAttr('{}.sz'.format(rootGrp), -1)

            mc.parent(newGeoName, eyeCtrl)

            ctrlAll.append(eyeCtrl)
            ctrlGrpAll.append(rootGrp)

        else:
            if not 'Shadow' in checkName:

                if 'Mouth' == checkName:
                    # MouthName = 'Mouth'
                    mouthCtrl = utaCore.curveCtrlShape(curveCtrl = 'circle', elem  = '{}{}{}Ctrl'.format(ctrlShapeName, process, side))
                    # set Ctrl Shape
                    crvFunc.set_shape(mouthCtrl,'ca{}'.format(ctrlShapeName))
                    mouthCtrlGrp = utaCore.generateGroup(sels = [mouthCtrl], elem = '')
                    utaCore.setAndCleanAttr(obj = mouthCtrlGrp, translate =  [0,0,0] , rotate = [0,0,0], scale = [1,1,1], color = 'yellow')
                    ctrlAll.append(mouthCtrl)
                    ctrlGrpAll.append(mouthCtrlGrp)

                    # TeethUpName = 'TeethUp'
                    if ctrlShapeTeethUpName:
                        teethUpCtrl = utaCore.curveCtrlShape(curveCtrl = 'circle', elem  = '{}{}{}Ctrl'.format(ctrlShapeTeethUpName, process, side))
                        # set Ctrl Shape
                        crvFunc.set_shape(teethUpCtrl,'ca{}'.format(ctrlShapeTeethUpName))
                        teethUpCtrlGrp = utaCore.generateGroup(sels = [teethUpCtrl], elem = '')
                        utaCore.setAndCleanAttr(obj = teethUpCtrlGrp, translate =  [0,0,0] , rotate = [0,0,0], scale = [1,1,1], color = 'white')
                        ctrlAll.append(teethUpCtrl)
                        ctrlGrpAll.append(teethUpCtrlGrp)
                        mc.parent(teethUpCtrlGrp, mouthCtrl)

                    # TongueName = 'TeethDn'
                    if ctrlShapeTongueName:
                        tongueCtrl = utaCore.curveCtrlShape(curveCtrl = 'circle', elem  = '{}{}{}Ctrl'.format(ctrlShapeTongueName, process, side))
                        # set Ctrl Shape
                        crvFunc.set_shape(tongueCtrl,'ca{}'.format(ctrlShapeTongueName))
                        tongueCtrlGrp = utaCore.generateGroup(sels = [tongueCtrl], elem = '')
                        utaCore.setAndCleanAttr(obj = tongueCtrlGrp, translate =  [0,0,0] , rotate = [0,0,0], scale = [1,1,1], color = 'pinkyWhite')
                        ctrlAll.append(tongueCtrl)
                        ctrlGrpAll.append(tongueCtrlGrp)
                        mc.parent(tongueCtrlGrp, mouthCtrl)

                    # TongueName = 'TeethDn'
                    if ctrlShapeTeethDnName:
                        teethDnCtrl = utaCore.curveCtrlShape(curveCtrl = 'circle', elem  = '{}{}{}Ctrl'.format(ctrlShapeTeethDnName, process, side))
                        # set Ctrl Shape
                        crvFunc.set_shape(teethDnCtrl,'ca{}'.format(ctrlShapeTeethDnName))
                        teethDnCtrlGrp = utaCore.generateGroup(sels = [teethDnCtrl], elem = '')
                        utaCore.setAndCleanAttr(obj = teethDnCtrlGrp, translate =  [0,0,0] , rotate = [0,0,0], scale = [1,1,1], color = 'white')
                        ctrlAll.append(teethDnCtrl)
                        ctrlGrpAll.append(teethDnCtrlGrp)

                        mc.parent(teethDnCtrlGrp, mouthCtrl)

                    ## snap position             
                    utaCore.snapPosition(selSource = mouthCtrlGrp, selTarget = eachGeo)
                    # utaCore.snapPosition(selSource = teethUpCtrlGrp, selTarget = eachGeo)

                    ## uiCtrlGeo parent eyeCtrl
                    uiCtrlGeoName = oldName.replace('Fcl', 'Ui')
                    newGeoName = mc.rename(eachGeo, '{}{}{}'.format(uiCtrlGeoName, side, lastName))
                    ## get position of facialUiCtrl (same position tx)
                    findFacialUiCtrlPos(grp = fclRigGeoGrp, tzValue = 2, objSource = newGeoName, objTarget = mouthCtrlGrp)

                    ## rotate Axis
                    if '_R_' in mouthCtrlGrp:

                        mc.setAttr('{}.ry'.format(mouthCtrlGrp), -180)
                        mc.setAttr('{}.sz'.format(mouthCtrlGrp), -1)
                    mc.parent(newGeoName, mouthCtrl)
                    # mc.parent(newGeoName, teethUpCtrlGrp, mouthCtrl)

                else: 
                    ctrl = utaCore.curveCtrlShape(curveCtrl = 'circle', elem  = '{}{}{}Ctrl'.format(checkName, process, side))
                    # set Ctrl Shape
                    crvFunc.set_shape(ctrl,'ca{}'.format(ctrlShapeName))
                
                    zroCtrlGrp = utaCore.generateGroup(sels = [ctrl], elem = '')
                    ctrlGrp = utaCore.generateGroup(sels = [zroCtrlGrp], elem = '')

                    # utaCore.setAndCleanAttr(obj = ctrlGrp, translate =  [0,0,0] , rotate = [90,0,0], scale = [1,1,1], color = color)
                    
                    ## snap position             
                    utaCore.snapPosition(selSource = ctrlGrp, selTarget = eachGeo)

                    ## uiCtrlGeo parent eyeCtrl
                    uiCtrlGeoName = oldName.replace('Fcl', 'Ui')
                    newGeoName = mc.rename(eachGeo, '{}{}{}'.format(uiCtrlGeoName, side, lastName))

                    ## get position of facialUiCtrl (same position tx)
                    findFacialUiCtrlPos(grp = fclRigGeoGrp, tzValue = 2, objSource = newGeoName, objTarget = ctrlGrp)

                    ## rotate Axis
                    if '_R_' in ctrlGrp:
                        mc.setAttr('{}.ry'.format(ctrlGrp), -180)
                        mc.setAttr('{}.sz'.format(ctrlGrp), -1)
                    mc.parent(newGeoName, ctrl)

                    ctrlAll.append(ctrl)
                    ctrlGrpAll.append(ctrlGrp)


            else:
                nameSp, side, lastName = cn.splitName(sels = [eachGeo])
                nameCtrl = nameSp.split('Shadow')[0]
                ctrlNameUse = '{}UiRig{}Ctrl'.format(nameCtrl, side)
                nameRe = '{}{}{}'.format(nameSp.replace('Fcl', 'Ui'), side, lastName)
                geoNameUse = mc.rename(eachGeo, nameRe)

                if ctrlNameUse:
                    mc.parent(geoNameUse, ctrlNameUse)
            
            

    ## add overall control plus
    overallCtrlName = 'Overall'
    overallCtrl = mc.circle( n = '{}{}{}Ctrl'.format(overallCtrlName, process, '_'),nr=(0, 0, 1), c=(0, 0, 0) , ch= 0)[0]
    crvFunc.set_shape(overallCtrl, 'plus')
    overallCtrlGrp = utaCore.generateGroup(sels = [overallCtrl], elem = '')
    utaCore.setAndCleanAttr(obj = overallCtrlGrp, translate =  [0,0,0] , rotate = [0,0,0], scale = [0.05,0.05,0.05], color = 'green')
    utaCore.snapPosition(selSource = overallCtrlGrp, selTarget = ctrlGrpAll)
    mc.parent(overallCtrlGrp, ctrlUiRigGrp)
    
    ## wrap control
    if ctrlUiRigGrp:
        mc.parent(ctrlGrpAll, overallCtrl)

    for eachCtrl in ctrlAll:
        if 'Mouth' in eachCtrl:
            utaCore.assignColor(obj = [eachCtrl], col = 'yellow')
        elif 'Cheek' in eachCtrl:
            utaCore.assignColor(obj = [eachCtrl], col = 'pinkyWhite')
        elif 'Jaw' in eachCtrl:
            utaCore.assignColor(obj = [eachCtrl], col = 'red')
        elif 'Eyebrow' in eachCtrl:
            utaCore.assignColor(obj = [eachCtrl], col = 'blue')
        elif 'Wrinkle' in eachCtrl:
            utaCore.assignColor(obj = [eachCtrl], col = 'pinkyWhite')
        else:
            utaCore.assignColor(obj = [eachCtrl], col = 'yellow')

    # another facialGeo parent to geoGrp
    headGeoAll = utaCore.listGeo(sels = fclRigGeoGrp, nameing = '_Geo', namePass = '_Grp')
    if headGeoAll:
        for each in headGeoAll:
            eachGeoName = each.split('|')[-1]
            headUiRigGeo = mc.rename(eachGeoName, eachGeoName.replace('Fcl', 'Ui'))
            mc.parent(headUiRigGeo, faceUiRigGeoGrp)

    ## addAttribute visCtrl
    if visCtrl:
        for eachCtrlName in ctrlGrpAll:
            obj = eachCtrlName.split('|')[-1]
            oldName, side, lastName = cn.splitName(sels= [obj])
            newName = oldName.split(process)[0] + side
            cn.addAttrCtrl (ctrl = visCtrl, elem = [newName + 'Vis'], min = 0, max = 1, at = 'long', dv = 1)
            mc.connectAttr('{}.{}'.format(visCtrl, newName + 'Vis'), '{}.v'.format(eachCtrlName))



    ## clear delete grp
    mc.delete(fclRigGeoGrp)

    return ctrlGrpAll, ctrlAll


def findFacialUiCtrlPos(grp = 'FclRigGeo_Grp', tzValue = 1.5, objSource = '', objTarget = '',  *args):

    facePosGuideLoc = mc.spaceLocator(n = 'FacePosGuide_Loc', p = (0, 0, 0))[0]
    utaCore.snapPosition(selSource = facePosGuideLoc, selTarget = grp)
    pm.makeIdentity(facePosGuideLoc, apply = True)
    mc.setAttr('{}.tz'.format(facePosGuideLoc), tzValue)

    utaCore.centerPivotObj (obj = [objSource])
    geoPosGuideLoc = mc.spaceLocator(n = 'FacePosGuide_Loc', p = (0, 0, 0))[0]

    ## snap position 
    utaCore.snapPosition(selSource = geoPosGuideLoc, selTarget = objSource) 
    mc.pointConstraint(facePosGuideLoc,geoPosGuideLoc,sk = ['x','y'],mo = False)  
    utaCore.snapObj(geoPosGuideLoc, objTarget)    
    mc.delete(facePosGuideLoc, geoPosGuideLoc)

def texturePlaneNo(numberNo = 1, obj = '',*args):
    
    if numberNo == 1:
        #011
        mc.setAttr('{}.tx'.format(obj), 7)
        mc.setAttr('{}.ty'.format(obj), 1.5)
    elif numberNo == 2:
        #012
        mc.setAttr('{}.tx'.format(obj), 12.5)
        mc.setAttr('{}.ty'.format(obj), 1.5)
    elif numberNo == 3:
        #013
        mc.setAttr('{}.tx'.format(obj), 18)
        mc.setAttr('{}.ty'.format(obj), 1.5)
    elif numberNo == 4:
        #021
        mc.setAttr('{}.tx'.format(obj), 7)
        mc.setAttr('{}.ty'.format(obj), -4.316)
    elif numberNo == 5:
        #022
        mc.setAttr('{}.tx'.format(obj), 12.5)
        mc.setAttr('{}.ty'.format(obj), -4.316)
    elif numberNo == 6:
        #023
        mc.setAttr('{}.tx'.format(obj), 18)
        mc.setAttr('{}.ty'.format(obj), -4.316)
    elif numberNo == 7:
        #031
        mc.setAttr('{}.tx'.format(obj), 7)
        mc.setAttr('{}.ty'.format(obj), -10.316)
    elif numberNo == 8:
        #032
        mc.setAttr('{}.tx'.format(obj), 12.5)
        mc.setAttr('{}.ty'.format(obj), -10.316)
    elif numberNo == 9:
        #033
        mc.setAttr('{}.tx'.format(obj), 18)
        mc.setAttr('{}.ty'.format(obj), -10.316)
    elif numberNo == 10:
        #041
        mc.setAttr('{}.tx'.format(obj), 7)
        mc.setAttr('{}.ty'.format(obj), -15)
    elif numberNo == 11:
        #042
        mc.setAttr('{}.tx'.format(obj), 12.5)
        mc.setAttr('{}.ty'.format(obj), -15)
    elif numberNo == 12:
        #043
        mc.setAttr('{}.tx'.format(obj), 18)
        mc.setAttr('{}.ty'.format(obj), -15)
    else:
        mc.setAttr('{}.tx'.format(obj), 0)
        mc.setAttr('{}.ty'.format(obj), 0)

def generateShadowPlane(planeGeo = '', shadowPlaneGeo = '', centerPlaneGeo = '', *args):
    name, side, lastName = cn.splitName(sels = [planeGeo])
    # name = nameSp.replace('Plane', 'ShadowPlane')
    # shadowGeo = mc.duplicate(planeGeo, n= '{}{}{}'.format(name, side, lastName))[0]

    ## shadowPlaneGeo
    if planeGeo:
        planeGeoPosLoc = mc.spaceLocator(n = '{}PlanePose{}Loc'.format(name, side), p = (0, 0, 0))[0]
        # mc.delete(mc.pointConstraint(planeGeo, planeGeoPosLoc, mo = False))
        utaCore.snapPosition(selSource = planeGeoPosLoc, selTarget = planeGeo)

    ## centerplaneGeo
    if centerPlaneGeo:
        centerPlaneGeoPosLoc = mc.spaceLocator(n = '{}PlaneCenter{}Loc'.format(name, side), p = (0, 0, 0))[0]
        # mc.delete(mc.pointConstraint(centerPlaneGeo, centerPlaneGeoPosLoc, mo = False))
        utaCore.snapPosition(selSource = centerPlaneGeoPosLoc, selTarget = centerPlaneGeo)

    # position answar
    positionAnswar = mc.spaceLocator(n = '{}PlaneShadow{}Loc'.format(name, side), p = (0, 0, 0))[0]
    utaCore.snapPosition(selSource = positionAnswar, selTarget = centerPlaneGeo)
    planeGeoPosLocTz = mc.getAttr('{}.tz'.format(planeGeoPosLoc))
    centerPlaneGeoPosLocTz = mc.getAttr('{}.tz'.format(centerPlaneGeoPosLoc))
    avravgeValueTz = (centerPlaneGeoPosLocTz - (planeGeoPosLocTz - centerPlaneGeoPosLocTz))
    utaCore.snapPosition(selSource = positionAnswar, selTarget = planeGeoPosLoc)
    mc.setAttr('{}.tz'.format(positionAnswar), avravgeValueTz)
    mc.delete(mc.pointConstraint(positionAnswar, shadowPlaneGeo, mo = False))

    # clear loc
    mc.delete(planeGeoPosLoc, centerPlaneGeoPosLoc, positionAnswar)

    return shadowPlaneGeo, avravgeValueTz

def generateOrganizePlaneUiGeo(FacePlaneFclRigGeoGrp = 'FacePlaneFclRigGeo_Grp', distanceUiCtrlAmp = 1, writeOrRead = False, *args):
    from rf_utils.context import context_info
    from rf_maya.rftool.rig.rigScript import copyObjFile
    reload(copyObjFile)

    organizeDicts = {}
    planeGeoLocAll = []
    # utaCore.centerPivotObj (obj = [HeadFclRigGeo])
    # headLoc = mc.spaceLocator(n = 'headPose_Loc', p = (0, 0, 0))[0]
    # utaCore.snapPosition(selSource = headLoc, selTarget = HeadFclRigGeo)

    planeGeo = utaCore.listGeo(sels = FacePlaneFclRigGeoGrp, nameing = '_Geo', namePass = '_Grp')
    
    for each in planeGeo:
        utaCore.centerPivotObj (obj = [each])
        name, side, lastName = cn.splitName(sels = [each])
        planeGeoPosLoc = mc.spaceLocator(n = '{}PlanePose{}Loc'.format(name, side), p = (0, 0, 0))[0]
        utaCore.snapPosition(selSource = planeGeoPosLoc, selTarget = each)
        valueTz = mc.getAttr('{}.tz'.format(planeGeoPosLoc))
        answarValue = valueTz * distanceUiCtrlAmp
        answarValue = '%.2f'%answarValue
        mc.setAttr('{}.tz'.format(planeGeoPosLoc), float(answarValue))
        planeGeoLocAll.append(planeGeoPosLoc)
        ## data dicts

        objName = each.split('|')[-1].split('Plane')[0]


    
    ## get path ---------------------------------------
    asset = context_info.ContextPathInfo() 
    bpath = copyObjFile.getRigDataPath(rigData = 'rigData', filePyName = 'ognPlaneUiDicts', lastNameType = '.dat')
    bpath = bpath.split('ognPlaneUiDicts')[0]

    # Path ---------------------------------------
    # corePath = '%s/core' % os.environ.get('RFSCRIPT')
    # path = '{}/rf_maya/rftool/rig/{}'.format(corePath, 'template/')
    # print path, '...path..88'
    # print bpath, '...bpath..'
    ## read dicts oganizedata planeUiGeo
    value_dicts = generateWriteOrReadDicts( writeOrRead = writeOrRead, 
                                            dicts = organizeDicts, 
                                            path = bpath, 
                                            subFolder = '/caTemplate/rigData/', 
                                            dataName = 'ognPlaneUiDicts',
                                            lastNameType = 'dat')

    ## set attribute translate Z
    for i in range(len(planeGeo)):

        objName = planeGeo[i].split('|')[-1]
        nameSp, side, lastName = cn.splitName(sels = [objName])
        name = nameSp.split('Plane')[0]
        if name in value_dicts.keys():
            guideLoc = mc.spaceLocator(n = '{}LocGuide{}Loc'.format(name, side), p = (0, 0, 0))[0]
            valueTx = mc.getAttr('{}.tx'.format(planeGeoLocAll[i]))
            valueTy = mc.getAttr('{}.ty'.format(planeGeoLocAll[i]))
            valueTz = float(value_dicts[name]) * float(distanceUiCtrlAmp)
            mc.setAttr('{}.tx'.format(guideLoc), float(valueTx))
            mc.setAttr('{}.ty'.format(guideLoc), float(valueTy))
            mc.setAttr('{}.tz'.format(guideLoc), float(valueTz))

            utaCore.snapPosition(selSource = objName, selTarget = guideLoc)
            planeGeoLocAll.append(guideLoc)

        elif "Shadow" in name:
            shadowName = 'Shadow'
            nameNew = name.split(shadowName)[0]
            if nameNew in value_dicts.keys():
                guideLoc = mc.spaceLocator(n = '{}LocGuide{}Loc'.format(nameNew, side), p = (0, 0, 0))[0]
                valueTx = mc.getAttr('{}.tx'.format(planeGeoLocAll[i]))
                valueTy = mc.getAttr('{}.ty'.format(planeGeoLocAll[i]))
                valueTz = float(value_dicts[name]) * float(distanceUiCtrlAmp)
                mc.setAttr('{}.tx'.format(guideLoc), float(valueTx))
                mc.setAttr('{}.ty'.format(guideLoc), float(valueTy))
                mc.setAttr('{}.tz'.format(guideLoc), float(valueTz))

                utaCore.snapPosition(selSource = objName, selTarget = guideLoc)
                planeGeoLocAll.append(guideLoc)
        else:

            guideLoc = mc.spaceLocator(n = '{}LocGuide{}Loc'.format(name, side), p = (0, 0, 0))[0]
            valueTx = mc.getAttr('{}.tx'.format(planeGeoLocAll[i]))
            valueTy = mc.getAttr('{}.ty'.format(planeGeoLocAll[i]))
            valueTz = float(value_dicts[name]) * float(distanceUiCtrlAmp)
            mc.setAttr('{}.tx'.format(guideLoc), float(valueTx))
            mc.setAttr('{}.ty'.format(guideLoc), float(valueTy))
            mc.setAttr('{}.tz'.format(guideLoc), float(valueTz))

            utaCore.snapPosition(selSource = objName, selTarget = guideLoc)
            planeGeoLocAll.append(guideLoc)
    ## clear loc
    mc.delete(planeGeoLocAll)

def generateWriteOrReadDicts(writeOrRead = False, dicts = {}, path = '', subFolder = 'caTemplate/rigData', dataName = 'ognPlaneUiDicts', lastNameType = 'py',*args):

    if path:
        d_path = path + '/' + dataName + '.' + lastNameType
    else:
        corePath = '%s/core' % os.environ.get('RFSCRIPT')
        d_path = '{}/rf_maya/rftool/rig/template/{}/{}.{}'.format(corePath, subFolder, dataName, lastNameType)
    # print d_path, '...d_path...88'
    if writeOrRead == True:
        ## write data
        with open(d_path,'w') as f:
            json.dump(dicts,f)
        value_dicts = dicts
    elif writeOrRead == False:
        ## read data
        value_dicts = {}
        with open(d_path,'r') as f:
            value_dicts = json.load(f)

    return value_dicts   

def copyFolderTexture(sourcePath = '', subSourcePath = 'caTemplate/texture/main',  targetPath = '', subTargetPath = 'texture/main', *args):
    from rf_utils import admin
    from rf_maya.rftool.rig.rigScript import copyObjFile
    reload(copyObjFile)

    # dir(admin)
    if not sourcePath:
        corePath = '%s/core' % os.environ.get('RFSCRIPT')
        sourcePath = '{}/rf_maya/rftool/rig/template/{}'.format(corePath, subSourcePath,)


    if not targetPath:
        # t_path
        targetPath = copyObjFile.getDataFld(subPath = subTargetPath)

    admin.win_copy_directory(sourcePath, targetPath)
    return sourcePath, targetPath

def copySeqOrMask(sourcePath = '',subSourcePath = 'caTemplate/texture/main',targetPath = '',subTargetPath = 'texture/main',copyFolder=0,copyFile=0):
    from rf_maya.rftool.rig.rigScript import copyObjFile
    reload(copyObjFile)

    # dir(admin)
    if not sourcePath:
        corePath = '%s/core' % os.environ.get('RFSCRIPT')
        tmpPath = '{}/rf_maya/rftool/rig/template/{}'.format(corePath, subSourcePath,)
        tmpImgPath = os.path.join(tmpPath,'images')
        tmpPrev = os.path.join(tmpImgPath,'preview')
        tmpHi = os.path.join(tmpImgPath,'hi')
    
    
    if not targetPath:
        # t_path
        texMainpath = copyObjFile.getDataFld(subPath = subTargetPath)
        imgPath = os.path.join(texMainpath,'images')
        texPrev = os.path.join(imgPath,'preview')
        texHi = os.path.join(imgPath,'hi')
    
    hiContent = os.listdir(tmpHi)
    prevContent = os.listdir(tmpPrev)
    hiFileContent = []
    hiFdContent = []
    prevFileContent = []
    prevFdContent = []
    
    # hi check
    for content in hiContent:
        check = os.path.join(tmpHi,content)
        if os.path.isdir(check) and content != 'txr':
            hiFdContent.append(check)
        elif os.path.isfile(check):
            if '.png' in check:
                hiFileContent.append(check)
    #prev check
    for content in prevContent:
        check = os.path.join(tmpPrev,content)
        if os.path.isdir(check) and content != 'txr':
            prevFdContent.append(check)
        elif os.path.isfile(check):
            if '.png' in check:
                prevFileContent.append(check)
    
    # copy Mask File
    if copyFile:
        for f in prevFileContent:
            tarPrevFile = f.replace(tmpPrev,texPrev)
            #print f + '>>>>' +tarPrevFile
            if not os.path.isfile(tarPrevFile):
                admin.copyfile(f, tarPrevFile)
        
        for f in hiFileContent:
            tarHiFile = f.replace(tmpHi,texHi)
            #print f + '>>>>'+ tarHiFile
            if not os.isfile(tarPrevFile):
                admin.copyfile(f, tarHiFile)
    
    
    if copyFolder:
    # copy Seq
        for fd in prevFdContent:
            tarPrevFd = fd.replace(tmpPrev,texPrev)
            #print fd + '>>>>' +tarPrevFd
            admin.win_copy_directory(fd, tarPrevFd)
        
        for fd in hiFdContent:
            tarHiFd = fd.replace(tmpHi,texHi)
            #print fd + '>>>>'+ tarHiFd
            admin.win_copy_directory(fd, tarHiFd)
#copySeqOrMask(copyFolder=0,copyFile=1)

#copyHeadTexTxr()

def copyHeadTexTxr(sourcePath = '',subSourcePath = 'caTemplate/texture/main',targetPath = '',subTargetPath = 'texture/main'):
    from rf_utils.context import context_info
    reload(context_info)
    from rf_maya.rftool.rig.rigScript import copyObjFile
    reload(copyObjFile)

    # dir(admin)
    context = context_info.ContextPathInfo()
    assets = context.name
    if not sourcePath:
        corePath = '%s/core' % os.environ.get('RFSCRIPT')
        tmpPath = '{}/rf_maya/rftool/rig/template/{}'.format(corePath, subSourcePath,)
        tmpImgPath = os.path.join(tmpPath,'images')
        #tmpPrev = os.path.join(tmpImgPath,'preview')
        #tmpTxr = os.path.join(tmpPrev,'txr')
        tmpFile = 'GB_Diffuse_Head.png'.format(assets)
        tmpTxrPath = os.path.join(tmpImgPath,tmpFile)

 
    if not targetPath:
        # t_path
        texMainpath = copyObjFile.getDataFld(subPath = subTargetPath)
        imgPath = os.path.join(texMainpath,'images')
        imgDf = '{}_Diffuse_Head.png'.format(assets)
        txrDf = os.path.join(imgPath,imgDf)        
    
    texRenPath ='P:/CA/asset/publ/char/{}/texture/main/hero/texRen'.format(assets)

    renDf = os.path.join(texRenPath,imgDf) 
    # print renDf
    if os.path.isfile(renDf):
        tmpTxrPath = renDf
    # print tmpTxrPath
    # print txrDf
    if not os.path.isfile(txrDf) or os.path.isfile(renDf):
        admin.copyfile(tmpTxrPath,txrDf)
        #print imgPath
    else:
        print 'Already has texture in {}'.format(imgPath)

def createTmpFromElem(elem = '',subSourcePath = 'caTemplate/texture/main', subTargetPath = 'texture/main'):
    from rf_utils.context import context_info
    reload(context_info)
    from rf_maya.rftool.rig.rigScript import copyObjFile
    reload(copyObjFile)

    context = context_info.ContextPathInfo
    assets = context.name
    seqFd = '{}'.format(elem)
    seqFile = '{}010.png'.format(elem)

        
    corePath = '%s/core' % os.environ.get('RFSCRIPT')
    tmpPath = '{}/rf_maya/rftool/rig/template/{}'.format(corePath, subSourcePath,)
    tmpImgPath = os.path.join(tmpPath,'images')
    tmpPrev = os.path.join(tmpImgPath,'preview')
    tmpSeq = os.path.join(tmpPrev,'TmpSeq')
    tmpFile = os.path.join(tmpSeq,'TmpSeq010.png')
    
    texMainpath = copyObjFile.getDataFld(subPath = subTargetPath)
    imgPath = os.path.join(texMainpath,'images')
    texPrev = os.path.join(imgPath,'preview')
    texHi = os.path.join(imgPath,'hi')
    prevSeq = os.path.join(texPrev,seqFd)
    hiSeq = os.path.join(texHi,seqFd)
    prevFile = os.path.join(prevSeq,seqFile)
    hiFile = os.path.join(hiSeq,seqFile)
    
    # check if seqPath exists
    if not os.path.isdir(prevSeq):
        # If not create SeqFolder , copy File from TmpSeq and rename to part Seq name
        print 'do copy {}'.format(prevSeq)
        admin.makedirs(prevSeq)

        print 'do copy {}'.format(seqFile)
        admin.win_copy_file(tmpFile,prevFile)
    
    if not os.path.isdir(hiSeq):
        # If not create SeqFolder , copy File from TmpSeq and rename to part Seq name
        print 'do copy {}'.format(hiSeq)
        admin.makedirs(hiSeq)

        print 'do copy {}'.format(seqFile)
        admin.win_copy_file(tmpFile,hiFile)

def generateCurveType(nameMenu = '', elem = '', sides = [], ctrlTranslateLimit = True, fullOrHalf = True, position = [], sclaeAxis = 0.3, *args):
    ## generate curve type
    menuNameLists = []
    textureUiCtrlLists = []
    maneNamePos = [-3.7, 2.2]
    menuPositionVtxValue = [110, 117]
    sidePositionVtxValue = [99, 44]
    axis = ['sx', 'sy', 'sz']
    fontText = 'MS Shell Dlg 2, 27.8pt'


    ## generate curve of Side 'R' 'L'
    menuSideLists = []

    ## generate group for L R 
    textureUiCtrlGrp = mc.group(n = '{}Texture{}Ctrl_Grp'.format(nameMenu, elem, ), em = True)

    # for i in range(len(sides)):



    # side = sides[i]
    for u in range(len(sides)):

        sideCrv = utaCore.findSide(side = sides[u])
        sideLists = mc.textCurves(n = '{}Texture{}Ctrl{}Grp'.format(nameMenu ,elem, sideCrv), f = fontText, t = sides[u])[0]

        curveViewLists = utaCore.listGeo(sels = sideLists, nameing = 'curve', namePass = 'Char')

        ## clean shape of mentName
        targetViewCtrl = ''
        sourceViewCtrlShape = []        
        for p in range(len(curveViewLists)):
            if p == 0:
                splitNameTarget = curveViewLists[p].split('|')[-1]
                utaCore.clean ( target = splitNameTarget)   
                targetViewCtrl = mc.rename(splitNameTarget, '{}Texture{}{}Ctrl'.format(nameMenu, elem, sideCrv))

            else:
                splitNameViewCtrlSource = curveViewLists[p].split('|')[-1]
                utaCore.clean ( target = splitNameTarget)  
                sourceViewCtrlShape.append(mc.rename(splitNameViewCtrlSource, '{}Texture{}{}{}Ctrl'.format(nameMenu, elem, p+1, sideCrv)))

        mc.select(sourceViewCtrlShape)
        mc.select(targetViewCtrl, add = True)
        if sourceViewCtrlShape:
            utaCore.parentShapeCurve(source = sourceViewCtrlShape, target = targetViewCtrl)
            mc.delete(sourceViewCtrlShape)

        ## assignColor color
        mc.setAttr('{}.overrideEnabled'.format(targetViewCtrl), 1)
        utaCore.assignColor(obj = [targetViewCtrl], col = 'softBlue')
        mc.parent(targetViewCtrl, sideLists)

        sideListsGrp = mc.rename(sideLists, sideLists.replace('Shape', ''))
        mc.parent(sideListsGrp, textureUiCtrlGrp)   

        ## move control L
        if u == 1:
            mc.setAttr('{}.tx'.format(sideListsGrp), 1.3)

        ## set limit control for plane geo 
        ## value limit control of texture control
        if ctrlTranslateLimit:
            if fullOrHalf:
                limitTx = [ 0, 4.5]
                limitTy = [ -4.5, 0]
                if u == 1:
                    limitTx = [-0.7, 4.15]
                    limitTy = [ -4.5, 0]
            else:
                limitTx = [ 0, 4.5]
                limitTy = [ -2, 0]
                if u == 1:
                    limitTx = [-0.7, 4.15]
                    limitTy = [ -2, 0]

 # ## dicts Positoin of side L R control
 #    dictsPosi = {}
 #    dictsPosi[0] = 99
 #    dictsPosi[1] = 88
 #    dictsPosi[2] = 77
 #    dictsPosi[3] = 66
 #    dictsPosi[4] = 55
 #    dictsPosi[5] = 44   
 #    dictsPosi[6] = 33
 #    dictsPosi[7] = 22
 #    dictsPosi[8] = 11
 #    dictsPosi[9] = 00
 #    position = [dictsPosi[position[0]], dictsPosi[position[-1]]]

        # ## O
        # if position[0] == 0:
        #     limitTx = [ 0, 4.5]
        #     limitTy = [ -4.5, 0]

        # ## -1
        # if position[-1] == 3:
        #     limitTx = [ 0, 4.5]
        #     limitTy = [ -4.5, 0]
        # elif position[-1] == 4:
        #     limitTx = [ 0, 4.5]
        #     limitTy = [ -4.5, 0]
        # elif position[-1] == 5:
        #     limitTx = [ 0, 4.5]
        #     limitTy = [ -4.5, 0]
        # elif position[-1] == 6:
        #     limitTx = [ 0, 4.5]
        #     limitTy = [ -4.5, 0]
        # else:
        #     limitTx = [ 0, 4.5]
        #     limitTy = [ -4.5, 0]


            utaCore.controlLimitTRS (obj = [targetViewCtrl], translate = True, rotate = False, ValueX = limitTx, ValueY = limitTy, ValueZ = [])

        ## delete char empty 
        charEmpty = utaCore.listGeo(sels = sideListsGrp, nameing = 'Char', namePass = '_Ctrl')
        mc.delete(charEmpty)
        menuSideLists.append(sideListsGrp)

    ## scale textureCtrl
    if sclaeAxis:
        sclaeAxis = sclaeAxis
    else:
        sclaeAxis = 1    
    for x in range(len(axis)):
        mc.setAttr('{}.{}'.format(textureUiCtrlGrp, axis[x]), sclaeAxis)
        pm.makeIdentity(textureUiCtrlGrp, apply = True)

    return menuSideLists, textureUiCtrlGrp, sideListsGrp

def dtlRigCtrlDiconnMatrixAndParentConstraint(dtlRigCtrlGrp = 'fclRig_md:DtlRigCtrl_Grp', *args):
    ofst_list = []
    for grp in mc.listRelatives(dtlRigCtrlGrp,type='constraint',ad=1):
        if 'Ofst' in grp:
            ofst_list.append(grp)
            
    for i in ofst_list:
        con =  mc.listConnections(i+'.constraintParentInverseMatrix',s=1,d=0,p=1,c=1)
        if con:
            mc.disconnectAttr(con[1],con[0])

    utaCore.parentScaleConstraintLoop(obj = 'bodyRig_md:HeadGmbl_Ctrl', lists = [dtlRigCtrlGrp],par = True, sca = False, ori = False, poi = False)


def setAttributeOffsetAmp( writeOrRead = False, rigGrp = 'fclRig_md:OverallUiRig_Ctrl', dataName = 'OffsetAmp', lastNameType = '.py', *args):
    from rf_utils import publish_info
    from rf_utils.context import context_info
    from rf_maya.rftool.rig.rigScript import copyObjFile
    reload(copyObjFile)
    from utaTools.utapy import utaCore
    reload(utaCore)
    from utaTools.utapy import utaCore
    reload(utaCore)

    ## check process
    asset = context_info.ContextPathInfo()
    publishInfo = publish_info.PreRegister(asset)
    process = publishInfo.entity.process

    ## get path ---------------------------------------
    asset = context_info.ContextPathInfo() 
    bpath = copyObjFile.getRigDataPath(rigData = 'rigData', filePyName = dataName, lastNameType = lastNameType)
    bpath = bpath.split(dataName)[0]
    checkFirst = utaCore.listGeo(sels = rigGrp, nameing = '_Ctrl', namePass = '_Grp')
    ctrlLists = []
    offsetAmpDicts = {}
    for each in checkFirst:
        obj = each.split('|')[-1]
        if not '_Geo' in obj:
            if not 'Specular' in obj:
                if not 'Iris' in obj:
                    if not 'Blink' in obj:
                        if not 'Mask' in obj:
                            ctrlLists.append(obj)

    for obj in ctrlLists:
        if mc.objExists('{}.offsetAmp'.format('{}Shape'.format(obj))):
            offsetAmpValue = mc.getAttr('{}.offsetAmp'.format('{}Shape'.format(obj)))

            offsetAmpDicts['{}.offsetAmp'.format('{}Shape'.format(obj))] = float('%.3f'%offsetAmpValue)

    if writeOrRead == True:
        ## Write
        ## get path ---------------------------------------
        # asset = context_info.ContextPathInfo() 
        # bpath = copyObjFile.getRigDataPath(rigData = 'rigData', filePyName = 'ognPlaneUiDicts')
        generateWriteOrReadDicts(writeOrRead = writeOrRead,
                                dicts = offsetAmpDicts, 
                                path = bpath, 
                                subFolder = '', 
                                dataName = dataName,
                                lastNameType = lastNameType)
    elif writeOrRead == False:
        ## Read
        value_dicts = generateWriteOrReadDicts(writeOrRead = writeOrRead,
                                dicts = offsetAmpDicts, 
                                path = bpath, 
                                subFolder = '', 
                                dataName = dataName,
                                lastNameType = lastNameType)

        value_dicts.keys()
        for eachCtrl in value_dicts.keys():

            if mc.objExists(eachCtrl):
                mc.setAttr(eachCtrl, value_dicts[eachCtrl])
            

def setAttributeForProcess( writeOrRead = True, allMoverGrp = 'AllMover_Grp', dataName = '', lastNameType = 'py', *args):

    from utaTools.utapy import utaCore
    reload(utaCore)
    from rf_utils import publish_info
    from rf_utils.context import context_info
    ## check process
    asset = context_info.ContextPathInfo()
    publishInfo = publish_info.PreRegister(asset)
    process = publishInfo.entity.process

    ## check control all in scene
    checkCtrlAll = utaCore.listGeo3(sels = 'bodyRig_md:Rig_Grp', nameing = '_Ctrl', namePass = '_Grp')
    for ctrl in checkCtrlAll:
        print ctrl.split('|')[-1]


    checkFirst = utaCore.listGeo3(sels = 'bodyRig_md:ClavicleDriveUpSubCtrlZro_L_Grp', nameing = '_Ctrl', namePass = '_Grp')
    for each in checkFirst:
        listsShapeAttr = utaCore.listAttributeOptions(obj = ['{}Shape'.format(each)], k = True, v = True, l = False)


    for obj in ctrlLists:
        if mc.objExists('{}.offsetAmp'.format('{}Shape'.format(obj))):
            offsetAmpValue = mc.getAttr('{}.offsetAmp'.format('{}Shape'.format(obj)))
            offsetAmpDicts['{}.offsetAmp'.format('{}Shape'.format(obj))] = float('%.3f'%offsetAmpValue)

    if not dataName:
        dataName = '{}Attr'.format(process)
        
    if writeOrRead == True:
        generateWriteOrReadDicts(writeOrRead = writeOrRead,
                        dicts = offsetAmpDicts, 
                        path = r'D:/a', 
                        subFolder = '', 
                        dataName = dataName,
                        lastNameType = lastNameType)
    elif writeOrRead == False:
        value_dicts = generateWriteOrReadDicts(writeOrRead = writeOrRead,
                        dicts = offsetAmpDicts, 
                        path = r'D:/a', 
                        subFolder = '', 
                        dataName = dataName,
                        lastNameType = lastNameType)

        value_dicts.keys()
        for eachCtrl in value_dicts.keys():

            if mc.objExists(eachCtrl):
                mc.setAttr(eachCtrl, value_dicts[eachCtrl])

def generateShRigProcess(shRigGeoGrp = 'shRig_md:ShRigGeo_Grp', shRigCtrlGrp = 'shRig_md:ShRigCtrl_Grp', bodyGeo = 'bodyRig_md:Body_Geo', *args):
    from lpRig import rigTools as lrr
    reload(lrr)
    from rf_maya.ncscript2.pymel import ncTools
    reload(ncTools)

    addRigCtrlGrp = mc.group(n = 'AddRigCtrlPar', em = True)
    addRigStillGrp = mc.group(n = 'AddRigStillPar', em = True)

    if shRigGeoGrp:
        shRigGeo = mc.listRelatives(shRigGeoGrp, c = True, f = True)
        if mc.objExists(bodyGeo):
            for each in shRigGeo:
                geoName = each.split('|')[-1]
                ## add blendShape
                mc.select(geoName, r = True)   
                mc.select(bodyGeo, add= True)
                lrr.doAddBlendShape()

    ## add vertex Constraint
    shRigCtrlList = []
    if shRigCtrlGrp:
        shRigCtrl = mc.listRelatives(shRigCtrlGrp, c = True, f = True)
        for each in shRigCtrl:
            obj = mc.listRelatives(each, c = True, f = True)[0]
            buttRockGrp = obj.split('|')[-1]
            shRigCtrlList.append(buttRockGrp)
    mc.select(bodyGeo, r = True)
    mc.select(shRigCtrlList, add = True)
    #run
    ncTools.vertex_constraint()


    ## wrap group    
    if mc.objExists('shRig_md:ShRigCtrl_Grp'):
        mc.parent('shRig_md:ShRigCtrl_Grp', addRigCtrlGrp)
    if mc.objExists('shRig_md:ShRigStill_Grp'):
        mc.parent('shRig_md:ShRigStill_Grp', addRigStillGrp)

    mc.parent(addRigCtrlGrp, 'bodyRig_md:AddCtrl_Grp')
    mc.parent(addRigStillGrp, 'bodyRig_md:Still_Grp')


def changLayterTextureOrder(*args):
    ## chang layer texture
    from rf_maya.rftool.rig.rigScript import caRigModule
    reload(caRigModule)

    # SpecularIrisAlphaLyt = 'EyeBallSpecularIrisAlpha_L_Lyt'
    # caRigModule.swapLayerOrder(lytNode = SpecularIrisAlphaLyt,index = [0,1,2,])
    # caRigModule.changeLayerInput(lytNode = SpecularIrisAlphaLyt , newLayerOrder = [0,1,2])

    for side in ['L', 'R']:

        AllLyt = 'EyeSeqAll_{}_Lyt'.format(side)
        utaCore.changeIndexOrder(lytNode = AllLyt, curIndex = [], changeIndex = [0, 5, 6, 4, 2, 3])





        # layterTexture, connectLists, objConnect = utaCore.removeMultiInstanceIndexOrderLayerTexture(layterTexture = [AllLyt], removeMultiInstance = False)
        # print layterTexture, '>>', connectLists, '>>', objConnect
        # # caRigModule.swapLayerOrder(lytNode = layterTexture[0],index = connectLists)
        # caRigModule.changeLayerInput(lytNode = AllLyt , newLayerOrder = [0, 3, 4, 5, 1, 2])


    # OutLyt = 'EyeSeqOut_L_Lyt'
    # caRigModule.swapLayerOrder(lytNode = OutLyt,index = [0,1,2,3])
    # caRigModule.changeLayerInput(lytNode = OutLyt , newLayerOrder = [0,1,2,3])

    # AnswerLyt = 'EyeSeqAnswer_L_Lyt'
    # caRigModule.swapLayerOrder(lytNode = AnswerLyt,index = [1,2])
    # caRigModule.changeLayerInput(lytNode = AnswerLyt , newLayerOrder = [1,2])




def speculatAutoVis(       specularCtrl = 'SpecularUiRig_L_Ctrl', 
                            specularSpACtrl = 'SpecularSpAUiRig_L_Ctrl', 
                            positionValue = [[82, 83, 84],[72, 73, 74]],
                            specularLyt = 'SpecularSpAUiRigEyeBallAlpha_L_Lyt',
                            EyeUiRigTyRevMdv = 'EyeUiRigTyRev', 
                            EyeUiRigPma = 'EyeUiRig', *args):
    ## splite name
    name, side, lastName = cn.splitName(sels = [specularSpACtrl])

    ## check group for control
    speculatCtrlGrp = mc.listRelatives(specularSpACtrl, p = True)[0]

    ## add Attribute Auto Vis
    specularAttr = 'specularSpAutoVis'
    if not mc.objExists('{}.{}'.format(specularCtrl, specularAttr)) :
        specularSpAutoVisAttr = cn.addAttrCtrl (ctrl = specularCtrl, elem = [specularAttr], min = 0, max = 1, at = 'long', dv = 1)



    ## generate node and setting
    addDoubleLinearXPositionNode = mc.createNode('addDoubleLinear', n = '{}XPosition{}Adl'.format(name, side))
    mc.addAttr( addDoubleLinearXPositionNode , ln = 'Int' , at = 'long' , k = True )
    mc.connectAttr('{}.Int'.format(addDoubleLinearXPositionNode), '{}.input1'.format(addDoubleLinearXPositionNode))

    addDoubleLinearYPositionNode = mc.createNode('addDoubleLinear', n = '{}YPosition{}Adl'.format(name, side))
    mc.addAttr( addDoubleLinearYPositionNode , ln = 'Int' , at = 'long' , k = True )
    mc.connectAttr('{}.Int'.format(addDoubleLinearYPositionNode), '{}.input1'.format(addDoubleLinearYPositionNode))


    ## eyeNode connectiion
    EyeUiRigTyRevMdv = 'EyeUiRigTyRev{}Mdv'.format(side)
    EyeUiRigPma = 'EyeUiRig{}Pma'.format(side)
    mc.connectAttr('{}.outputX'.format(EyeUiRigTyRevMdv), '{}.Int'.format(addDoubleLinearXPositionNode))
    mc.connectAttr('{}.output1D'.format(EyeUiRigPma), '{}.Int'.format(addDoubleLinearYPositionNode))


    ## pma value All
    spcularValueAllPma = mc.createNode('plusMinusAverage',  n = '{}ValueAll{}Pma'.format(name, side))
    mc.connectAttr('{}.output'.format(addDoubleLinearXPositionNode), '{}.input2D[0].input2Dx'.format(spcularValueAllPma))
    mc.connectAttr('{}.output'.format(addDoubleLinearYPositionNode), '{}.input2D[1].input2Dx'.format(spcularValueAllPma))

    spcularReveseRev = mc.createNode('reverse', n = '{}{}Rev'.format(name, side))   
    mc.connectAttr('{}.{}'.format(specularCtrl, specularAttr), '{}.inputX'.format(spcularReveseRev))

    ## pma select some value 1
    spcularSelectValuePma = mc.createNode('plusMinusAverage',  n = '{}SelectValue{}Pma'.format(name, side))

    ## Answer Con
    spcularPositionAnswerCon = mc.createNode('condition', n = '{}PositionAmswer{}Con'.format(name, side))
    mc.setAttr('{}.secondTerm'.format(spcularPositionAnswerCon), 1)
    mc.setAttr('{}.colorIfTrueR'.format(spcularPositionAnswerCon), 1)
    mc.connectAttr('{}.output2Dx'.format(spcularSelectValuePma), '{}.firstTerm'.format(spcularPositionAnswerCon))

    mc.connectAttr('{}.outputX'.format(spcularReveseRev), '{}.colorIfFalseR'.format(spcularPositionAnswerCon)) 

    for i in range(len(positionValue[0])):

        ## setAttribute Node
        if '_L' in side:
            sideValue = positionValue[1][i]
        if '_R' in side:
            sideValue = positionValue[0][i]


        
        ## add Attribute Shape Position Value
        specPositionShapeAttr = 'specularPosition'
        ## ascii
        asciiStr = string.ascii_uppercase[i]
        attShp = '{}Shape.{}{}'.format(specularCtrl, specPositionShapeAttr, asciiStr)
        if not mc.objExists(attShp) :
            specularSpAutoVisAttr = cn.addAttrCtrl (ctrl = specularCtrl + 'Shape', elem = [specPositionShapeAttr + asciiStr], min = -200, max = 200, at = 'long', dv = sideValue)
        else:
            attShp = '{}Shape.{}{}'.format(specularCtrl, specPositionShapeAttr, asciiStr)


        ## Check Positoin Value Con
        spcularPositionCon = mc.createNode('condition', n = '{}Position{}{}Con'.format(name, asciiStr, side))
        mc.setAttr('{}.secondTerm'.format(spcularPositionCon), sideValue)
        mc.setAttr('{}.colorIfTrueR'.format(spcularPositionCon), 1)
        mc.setAttr('{}.colorIfFalseR'.format(spcularPositionCon), 0)
        mc.connectAttr('{}.output2Dx'.format(spcularValueAllPma),'{}.firstTerm'.format(spcularPositionCon))
        mc.connectAttr(attShp, '{}.secondTerm'.format(spcularPositionCon))


        ## Check Sum Positoin Value 0 or 1 Con
        spcularSumPositionCon = mc.createNode('condition', n = '{}SumPosition{}{}Con'.format(name, asciiStr, side))
        mc.setAttr('{}.secondTerm'.format(spcularSumPositionCon), 1)
        mc.setAttr('{}.colorIfTrueR'.format(spcularSumPositionCon), 1)
        mc.setAttr('{}.colorIfFalseR'.format(spcularSumPositionCon), 0)
        mc.connectAttr('{}.outColorR'.format(spcularPositionCon), '{}.firstTerm'.format(spcularSumPositionCon))

        mc.connectAttr('{}.outColorR'.format(spcularSumPositionCon), '{}.input2D[{}].input2Dx'.format(spcularSelectValuePma, i))

    # connect Layter Texture
    mc.connectAttr('{}.outColorR'.format(spcularPositionAnswerCon),'{}.v'.format(speculatCtrlGrp))
    mc.connectAttr('{}.outColorR'.format(spcularPositionAnswerCon), '{}.inputs[0].isVisible'.format(specularLyt))



















# def addShadowToEye(ctrl=  [] , 
#     counterCtrl =[],
#     targetLayer = '',
#     mergeAlpha=[]):
#     pass


# def createFloat2DTextureSeq(name = 'EyebrowShadowEye',side = '',
#     seqName = 'EyebrowShadow',texturePath = ''):
#     textureShadowSeqFile , place2DTextureShadow = cn.generateFilePlace2DTexture(name = name + 'ShadowSeq', 
#     side = side, elem = '', 
#     node = 'file'):

#     mc.addAttr( textureUiRigCtrl , ln = attrTxInt , at = 'long' , k = True )
#     textureAverageMdv = mc.createNode('multiplyDivide', n = '{}Average{}Mdv'.format(name, side))
#     mc.connectAttr('{}Shape.{}'.format(textureUiRigCtrl, attrTxTyUiCtrlAmp[0]), '{}.input2X'.format(textureAverageMdv))
#     mc.connectAttr('{}.tx'.format(textureUiRigCtrl), '{}.input1X'.format(textureAverageMdv))
#     mc.connectAttr('{}.outputX'.format(textureAverageMdv), '{}.{}'.format(textureUiRigCtrl, attrTxInt))

#     ## generate node
#     textureMdv = mc.createNode('multiplyDivide', n = '{}{}Mdv'.format(name, side))
#     mc.setAttr("{}.input2X".format(textureMdv), 10)

#     tyRevMdv = mc.createNode('multiplyDivide', n = '{}TyRev{}Mdv'.format(name, side))
#     mc.connectAttr('{}Shape.{}'.format(textureUiRigCtrl, attrTxTyUiCtrlAmp[1]), '{}.input2X'.format(tyRevMdv))

#     texturePma = mc.createNode('plusMinusAverage',  n = '{}{}Pma'.format(name, side))
#     mc.setAttr('{}.input1D[0]'.format(texturePma), valuePma)

def generateShadow(shadow = True, shadowPlaneGeo = True, shadowUsesAlpha = False, uiRigCtrl = '', localUiRigCtrl = '', filePlace2DTextureMirror = '', name = '', elem = '', side = '', lytAnswerNode = '', texturePath = '', *args):

    ## split name
    ctrlName, side, lastName = cn.splitName(sels = [uiRigCtrl])
    name = ctrlName.split('UiRig')[0]

    txtyAverageValueUiAmp =  [2, -2]
    valuePma = 10

    # Get the list of all files and directories
    dir_list = os.listdir(texturePath + '/' + elem)
    for each in dir_list:
        if '{}Cor'.format(name) in each:
            uPath = texturePath + '/' + elem + '/' + '{}Cor.png'.format(name)
        if '{}AlphaSeq'.format(elem) == each :
            mouthShadowAlphaPath = texturePath + '/' + elem + '/' + each + '/' + '{}.010.png'.format(each)   

    if shadow:
        shadowCtrlName = '{}{}Ctrl'.format(name, side)

        ## generate new Control for shadow Control
        nameGimbal = 'Shadow'
        newShadowCtrlName = utaCore.addGimbalCtrl(ctrl = [uiRigCtrl], nameGmbl = nameGimbal, shapeAttr = False)
        textureUiRigCtrl = newShadowCtrlName
        attrTxInt = 'TxInt'
        attrTxTyUiCtrlAmp =['TxUiCtrlAmp', 'TyUiCtrlAmp']

        cn.addAttrCtrl (ctrl = textureUiRigCtrl + 'Shape' , elem = attrTxTyUiCtrlAmp, min = -10, max = 10, at = 'float', dv = 0)
        for x in range(len(attrTxTyUiCtrlAmp)):
            mc.setAttr("{}.{}".format(textureUiRigCtrl + 'Shape', attrTxTyUiCtrlAmp[x]), txtyAverageValueUiAmp[x])
                
        ## Generate textureFile
        textureShadowSeqFile , place2DTextureShadow = cn.generateFilePlace2DTexture(name = name + 'ShadowSeq', side = side, elem = '', node = 'file')
        
        ## Connect Shadow control
        if 'R_' in side:
            posiValue = [1.3, 2.2]
        else:
            posiValue = [1.3, 2.2]
        rotateMdv, UvOptionMdv, shadowTranslateMdvNode = connectTRSFacial2( ctrl = newShadowCtrlName, elem = 'Shadow',  place2dTexture = [place2DTextureShadow], positionDefault = posiValue)

        if uPath:
            mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(textureShadowSeqFile, uPath))

        ## generate metrial of shadowPlaneGeo
        if shadowPlaneGeo:
            ## duplicate shadowPlaneGeo
            shadowPlaneGeo, avravgeValueTz = generateShadowPlane(planeGeo = planeGeo, shadowPlaneGeo = shadowPlaneGeo, centerPlaneGeo = shadowPlaneGeo)
            ## connect to mateiral
            myShaderShadowBlinn, myShaderShadowBlinnSG = utaCore.generateMaterial(materialName = 'blinn', name = name + 'Shadow', side = side, shortName = 'Bln')
            mc.setAttr("{}.specularColor".format(myShaderShadowBlinn), 0,0,0 )
            # mc.connectAttr('{}.outColor'.format(textureShadowSeqFile), '{}.color'.format(myShaderShadowBlinn))
            mc.connectAttr('{}.outTransparency'.format(textureShadowSeqFile), '{}.transparency'.format(myShaderShadowBlinn))
            
            ## generate lambert for shadow matterial
            shadowRampMatterial, shadowRampMatterialSG = utaCore.generateMaterial(materialName = 'ramp', name = name + 'ShadowCor', side = side, shortName = 'Rmp')
            # mc.setAttr('{}.colorGain'.format(shadowRampMatterial),t =  double3, ( 0.309, 0.0951682, 0.034299))
            mc.setAttr('{}.colorEntryList[1].color'.format(shadowRampMatterial), 0.309, 0.0951682, 0.034299, type = "double3")
            mc.connectAttr('{}.outColor'.format(shadowRampMatterial), '{}.color'.format(myShaderShadowBlinn))
            # disconnectAttr ramp1.outColor EyebrowUiRigShadow_L_Bln.color;

            ## assign metrial to shadowPlaneGeo
            if mc.objExists(shadowPlaneGeo):
                mc.select(shadowPlaneGeo)
                utaCore.assignSelectionToShader(myShaderShadowBlinn)
                mc.setAttr("{}.overrideEnabled".format(shadowPlaneGeo), 1)
                mc.setAttr("{}.overrideDisplayType".format(shadowPlaneGeo), 2)
                mc.setAttr("{}.selectionChildHighlighting".format(shadowPlaneGeo), 0)

                lytShadowNode = ''
        else:
            lytShadowNode = mc.createNode('layeredTexture', n='{}Shadow{}{}Lyt'.format(name, elem, side))
            # eyeMaskFacial(ctrl = ['EyeMask_R_Ctrl'], elem = 'Mask', lytOut = 'eyeTextureOut_R_Lyt', texturePath = 'P:/CA/asset/work/char/GB/texture/main', whiteLine = False, material = ''
            
            alphaShadowMdvNode = mc.createNode('multiplyDivide', n = '{}AlphaShadow{}{}Mdv'.format(ctrlName, elem,  side))

            ## connect to lytOutNode
            indexNumberShadowNode = utaCore.checkIndexOrderLayerTexture(layterTexture = lytAnswerNode)
            mc.connectAttr('{}.outAlpha'.format(textureShadowSeqFile),'{}.inputs[2].alpha'.format(lytShadowNode))
            mc.connectAttr('{}.outColor'.format(lytShadowNode),'{}.inputs[{}].color'.format(lytAnswerNode, indexNumberShadowNode +1))
            mc.connectAttr('{}.outAlpha'.format(lytShadowNode),'{}.input1X'.format(alphaShadowMdvNode))
            mc.connectAttr('{}.outputX'.format(alphaShadowMdvNode),'{}.inputs[{}].alpha'.format(lytAnswerNode, indexNumberShadowNode +1))

            ## generate lambert for shadow matterial
            shadowRampCor, shadowRampCorSG = utaCore.generateMaterial(materialName = 'ramp', name = name + 'ShadowCor', side = side, shortName = 'Rmp')
            # mc.setAttr('{}.colorGain'.format(shadowRampMatterial),t =  double3, ( 0.309, 0.0951682, 0.034299))
            mc.setAttr('{}.colorEntryList[1].color'.format(shadowRampCor), 0.309, 0.0951682, 0.034299, type = "double3")
            mc.connectAttr('{}.outColor'.format(shadowRampCor), '{}.inputs[2].color'.format(lytShadowNode))

        ## locck attributre Ui Control
        uiShadowCtrl = '{}{}{}Ctrl'.format(name, nameGimbal, side)
        if mc.objExists(uiShadowCtrl):
            cn.lockAttr(listObj = [uiShadowCtrl], attrs = ['tz', 'rx', 'ry', 'sz', 'v'],lock = True, keyable = False)
    else:
        lytShadowNode = ''

    ## mirrorU shadow of R side
    generateAttributeForUser(ctrl = uiRigCtrl, nodeName = [place2DTextureShadow, filePlace2DTextureMirror], attr = ['mirrorU'], shape = False)

    ## add Geo Ui Visibility 
    if not mc.objExists('{}{}'.format(uiRigCtrl, 'GeoVis')):
        geoVis = cn.addAttrCtrl (ctrl = uiRigCtrl , elem = ['GeoVis'], min = 0, max = 1, at = 'long', dv = 1)[0]
    else:
        geoVis = 'GeoVis'

    ## locck attributre Ui Control
    if mc.objExists(uiRigCtrl):
        if mc.objExists(shadowPlaneGeo):
            try:
                mc.connectAttr('{}.{}'.format(uiRigCtrl, geoVis), '{}.v'.format(shadowPlaneGeo))
                shadowPlaneGeo = shadowPlaneGeo
            except Exception as e:
                print e


    if shadowUsesAlpha:
        ## generate Node
        textureMouthShadowAlphaSeqFile , place2DTextureMouthShadowAlphaSeq = cn.generateFilePlace2DTexture(name = name + 'ShadowAlphaSeq', side = side, elem = 'Seq', node = 'file')
        mouthShadowAlphaSeqMdl = mc.createNode('multDoubleLinear', n = '{}AlphaAlpha{}{}Mdl'.format(name, elem,  side))

        ## Connect Mirror UAmp file Shadow Texture
        if not localUiRigCtrl:
            localUiRigCtrl = uiRigCtrl
        mc.setAttr('{}.repeatU'.format(place2DTextureMouthShadowAlphaSeq), -1)
        mc.connectAttr('{}.mirrorUAmp'.format(localUiRigCtrl), '{}.mirrorU'.format(place2DTextureMouthShadowAlphaSeq))

        ## connection
        mc.connectAttr('{}.outAlpha'.format(textureMouthShadowAlphaSeqFile), '{}.input2'.format(mouthShadowAlphaSeqMdl))
        mc.connectAttr('{}.outputX'.format(alphaShadowMdvNode), '{}.input1'.format(mouthShadowAlphaSeqMdl))
        mc.connectAttr('{}.output'.format(mouthShadowAlphaSeqMdl), '{}.inputs[{}].alpha'.format(lytAnswerNode, indexNumberShadowNode +1), f = True)

        ## assign file shadow path
        if mouthShadowAlphaPath:
            mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(textureMouthShadowAlphaSeqFile, mouthShadowAlphaPath))

        if 'Ball' in elem:
            namePrc = elem.split('Ball')[0]      
        elif 'Hole' in elem:
            namePrc = elem.split('Hole')[0]        
        ## texture uiRig control
        shadowAlphaSeqFrameExtensionPma = '{}UiRigShadow{}Pma'.format(namePrc, side)
        shadowAlphaSeqFrameOffsetMdv = '{}UiRigShadowTyRev{}Mdv'.format(namePrc, side)

        ## control connect texture file
        mc.connectAttr('{}.output1D'.format(shadowAlphaSeqFrameExtensionPma), '{}.frameExtension'.format(textureMouthShadowAlphaSeqFile))
        mc.setAttr('{}.useFrameExtension'.format(textureMouthShadowAlphaSeqFile), 1)
        mc.connectAttr('{}.outputX'.format(shadowAlphaSeqFrameOffsetMdv), '{}.frameOffset'.format(textureMouthShadowAlphaSeqFile))

    return place2DTextureShadow, alphaShadowMdvNode


def addTextureToLayer(  ctrl='TeethOneUiRig_Ctrl',
                        shadow = True,
                        elem = 'MouthHole',
                        answerLyt= 'MouthSeqAnswer_Lyt',
                        changLayer = True,
                        curIndex = [1, 2, 3, 4],
                        changeIndex = [1, 3, 2, 4],
                        localCtrl = 'MouthUiRig_Ctrl',
                        worldCtrl = 'FacialUiRig_Ctrl',
                        usesAlphaSeq = False,
                        texturePath = r'P:/CA/asset/work/char/GB/texture/main/images/preview'
                        ):

    ## split name
    ctrlName, side, lastName = cn.splitName(sels = [ctrl])
    name = ctrlName.split('UiRig')[0]

    ## group ctrl
    ctrlGrp = mc.listRelatives(ctrl, p = True)

    # Get the list of all files and directories
    dir_list = os.listdir(texturePath + '/' + elem)
    for each in dir_list:
        if '{}Cor'.format(name) in each:
            uPath = texturePath + '/' + elem + '/' + '{}Cor.png'.format(name)
        if 'MouthHoleAlphaSeq' == each:
            mouthAlphaSeqPath = texturePath + '/' + elem + '/' + each + '/' + '{}.010.png'.format(each)        

    # generate Node
    textureSeqFile , place2DTextureTextureSeq = cn.generateFilePlace2DTexture(name = name, side = side, elem = 'Seq', node = 'file')
    alphaMdvNode = mc.createNode('multiplyDivide', n = '{}HoleAlpha{}{}Mdv'.format(ctrlName, elem,  side))

    ## connect to lytOutNode
    indexNumberAnswerNode = utaCore.checkIndexOrderLayerTexture(layterTexture = answerLyt)
    mc.connectAttr('{}.outColor'.format(textureSeqFile), '{}.inputs[{}].color'.format(answerLyt, indexNumberAnswerNode + 1))
    mc.connectAttr('{}.outAlpha'.format(textureSeqFile), '{}.input1X'.format(alphaMdvNode))
    mc.connectAttr('{}.outputX'.format(alphaMdvNode), '{}.inputs[{}].alpha'.format(answerLyt, indexNumberAnswerNode + 1))

    ## assign file path
    if uPath:
        mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(textureSeqFile, uPath))

    if shadow:
        place2DTextureShadow, alphaShadowMdvNode = generateShadow(shadow = True, shadowPlaneGeo = False, shadowUsesAlpha = usesAlphaSeq, uiRigCtrl = ctrl, localUiRigCtrl = localCtrl, filePlace2DTextureMirror = place2DTextureTextureSeq, name = name, elem = elem, side = side, lytAnswerNode = answerLyt, texturePath = texturePath)

    ## add Geo Ui Visibility 
    if not mc.objExists('{}.GeoVis'.format(ctrl)):
        cn.addAttrCtrl (ctrl = ctrl , elem = ['GeoVis'], min = 0, max = 1, at = 'long', dv = 1)[0]
            ## connect to lytOutNode
    indexNumberAnswerVisNode = utaCore.checkIndexOrderLayerTexture(layterTexture = answerLyt)
    mc.connectAttr('{}.GeoVis'.format(ctrl),  '{}.input2X'.format(alphaMdvNode))
    mc.connectAttr('{}.GeoVis'.format(ctrl),  '{}.input2X'.format(alphaShadowMdvNode))


    if usesAlphaSeq:
        ## generate Node
        textureMouthAlphaSeqFile , place2DTextureMouthAlphaSeq = cn.generateFilePlace2DTexture(name = name + 'HoleAlphaSeq', side = side, elem = 'Seq', node = 'file')
        mouthAlphaSeqMdl = mc.createNode('multDoubleLinear', n = '{}HoleAlpha{}{}Mdl'.format(name, elem,  side))
        ## connection
        mc.connectAttr('{}.outAlpha'.format(textureMouthAlphaSeqFile), '{}.input2'.format(mouthAlphaSeqMdl))
        mc.connectAttr('{}.outputX'.format(alphaMdvNode), '{}.input1'.format(mouthAlphaSeqMdl))
        mc.connectAttr('{}.output'.format(mouthAlphaSeqMdl), '{}.inputs[{}].alpha'.format(answerLyt, indexNumberAnswerNode + 1), f = True)

        ## assign file path
        if mouthAlphaSeqPath:
            mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(textureMouthAlphaSeqFile, mouthAlphaSeqPath))

        namePrc = elem.split('Hole')[0]         
        ## texture uiRig control
        alphaSeqFrameExtensionPma = '{}UiRigShadow{}Pma'.format(namePrc, side)
        alphaSeqFrameOffsetMdv = '{}UiRigShadowTyRev{}Mdv'.format(namePrc, side)

        ## control connect texture file
        mc.connectAttr('{}.output1D'.format(alphaSeqFrameExtensionPma), '{}.frameExtension'.format(textureMouthAlphaSeqFile))
        mc.setAttr('{}.useFrameExtension'.format(textureMouthAlphaSeqFile), 1)
        mc.connectAttr('{}.outputX'.format(alphaSeqFrameOffsetMdv), '{}.frameOffset'.format(textureMouthAlphaSeqFile))

        ## Connect Mirror UAmp file Texture
        mc.setAttr('{}.repeatU'.format(place2DTextureMouthAlphaSeq), -1)
        # print place2DTextureMouthAlphaSeq, '...place2DTextureMouthAlphaSeq...9999'
        # print place2DTextureTextureSeq, '....place2DTextureTextureSeq....888'
        # print place2DTextureShadow, '...place2DTextureShadow...4444'
        # print localCtrl, '....localCtrl.....9999'
        mc.connectAttr('{}.mirrorUAmp'.format(localCtrl), '{}.mirrorU'.format(place2DTextureMouthAlphaSeq))

        # ## re-connect mirrorU mouthUiCtrl to holeShadowAlpha
        # mc.connectAttr('{}.mirrorUAmp'.format(localCtrl), '{}.mirrorU'.format(place2DTextureMouthAlphaSeq))

        

    ## connect Translate Rotete Scale to place2DTexture
    connectTRSFacial2( ctrl = ctrl, elem = elem, place2dTexture = [place2DTextureTextureSeq, place2DTextureShadow], positionDefault = [0, 0])

    ## chang TextureLayer
    if changLayer:
        utaCore.changeIndexOrder(lytNode = answerLyt, curIndex = curIndex, changeIndex = changeIndex)

    ## localWorld
    utaCore.parentLocalWorldCtrl( ctrl = ctrl , localObj = localCtrl , worldObj = worldCtrl , parGrp = ctrlGrp)


def addUvChooser(uvChooser = 'fclRig_md:HeadRampHead_Uvc', *args):
    if not uvChooser:
        uvChooser = 'fclRig_md:HeadRampHead_Uvc'

    uiCtrlGeo = mc.listConnections(uvChooser+'.uvSets[0]',s=1,d=0,c=1,p=1)
    if uiCtrlGeo:
        # print uiCtrlGeo
        bodyGeo = uiCtrlGeo[1].replace('fclRig_md','bodyRig_md').replace('UiRig','')
        # print bodyGeo
        if mc.objExists(bodyGeo):
            mc.connectAttr(bodyGeo , uvChooser+'.uvSets[1]',f=1)
        else:
            mc.error(c = "bodyGeo doesn't Exists")

def generateLayerBlackRims(ctrl = 'IrisUiRig_L_Ctrl', elem = 'EyeBall', IrisUiRigEyeBallLyt = 'IrisUiRigEyeBall_L_Lyt', EyeSeqAllLyt = 'EyeSeqAll_L_Lyt', IrisUiRigPupilCorEyeBallFile = 'IrisUiRigPupilCorEyeBall_L_File', texturePath = '', *args):

    ## split name
    name, side, lastName = cn.splitName(sels = [ctrl])
    # name = ctrlName.split('UiRig')[0]
    # generate Node
    textureSeqFile , place2DTextureTextureSeq = cn.generateFilePlace2DTexture(name = '{}BlackRims'.format(name), side = side, elem = elem, node = 'file')
    mc.setAttr('{}.repeatU'.format(place2DTextureTextureSeq), -1)

    ## mirrorU  specular for R side
    generateAttributeForUser(ctrl = ctrl, nodeName = [place2DTextureTextureSeq], attr = ['mirrorU'], shape = False)

    # Get the list of all files and directories
    dir_list = os.listdir(texturePath + '/' + elem)
    for each in dir_list:
        if elem.split('Ball')[0] == 'Mouth':
            if '{}Cor'.format(name) in each:
                uPath = texturePath + '/' + elem + '/' + '{}Cor.png'.format(name)

    # Get the list of all files and directories
    dir_list = os.listdir(texturePath + '/' + elem)
    if elem.split('Ball')[0] == 'Eye':
        PupilAlphaSeq = 'PupilAlphaSeq'
        for each in dir_list:
            if 'PupilAlpha' in each:
                PupilAlphaPath = texturePath + '/' + elem + '/' + PupilAlphaSeq + '/' + 'PupilAlphaSeq.010.png'

    ## assign file path
    mm.eval('setAttr -type "string" {}.fileTextureName "{}";'.format(textureSeqFile, PupilAlphaPath))

    ## connect attribute
    connectTRSFacial2( ctrl = ctrl, elem = 'BlackRims', place2dTexture = [place2DTextureTextureSeq], positionDefault = [0, 0])

    ## check index Order IrisUiRigEyeBallLyt
    indexNumberIrisUiRigEyeBallNode = utaCore.checkIndexOrderLayerTexture(layterTexture = IrisUiRigEyeBallLyt)
    mc.connectAttr('{}.outColor'.format(IrisUiRigPupilCorEyeBallFile), '{}.inputs[{}].color'.format(IrisUiRigEyeBallLyt, indexNumberIrisUiRigEyeBallNode + 1))
    mc.connectAttr('{}.outAlpha'.format(IrisUiRigPupilCorEyeBallFile), '{}.inputs[{}].alpha'.format(IrisUiRigEyeBallLyt, indexNumberIrisUiRigEyeBallNode + 1))
    

    ## check index Order EyeSeqAllLyt
    # print EyeSeqAllLyt, '...EyeSeqAllLyt..88'
    indexNumberEyeSeqAllNode = utaCore.checkIndexOrderLayerTexture(layterTexture = EyeSeqAllLyt)
    if mc.objExists('{}.inputs[{}].color'.format(EyeSeqAllLyt, indexNumberEyeSeqAllNode)):
    # print indexNumberEyeSeqAllNode, '...indexNumberEyeSeqAllNode..22'
    # print IrisUiRigPupilCorEyeBallFile, '...IrisUiRigPupilCorEyeBallFile..99'
        mc.disconnectAttr('{}.outColor'.format(IrisUiRigPupilCorEyeBallFile), '{}.inputs[{}].color'.format(EyeSeqAllLyt, indexNumberEyeSeqAllNode))
    # try:
    mc.connectAttr('{}.outColor'.format(textureSeqFile), '{}.inputs[{}].color'.format(EyeSeqAllLyt, indexNumberEyeSeqAllNode))
    mc.connectAttr('{}.outAlpha'.format(textureSeqFile), '{}.inputs[{}].alpha'.format(EyeSeqAllLyt, indexNumberEyeSeqAllNode))

    # except Exception as e:
        # print e
    ## connect frameExtension
    mc.connectAttr('EyeUiRig{}Pma.output1D'.format(side), '{}.frameExtension'.format(textureSeqFile))
    mc.setAttr('{}.useFrameExtension'.format(textureSeqFile), 1)
    mc.connectAttr('EyeUiRigTyRev{}Mdv.outputX'.format(side), '{}.frameOffset'.format(textureSeqFile))


def generateLayerVis(ctrl = '', elem = '', file = '', lytNode = '', indexLayer = '',*args):
    ## split name
    name, side, lastName = cn.splitName(sels = [ctrl])
    layerVisMdv = mc.createNode('multiplyDivide', n = '{}LayerVis{}{}Mdv'.format(name, elem,  side))

    shadowVisAttr = utaCore.addAttrCtrl (ctrl = ctrl, elem = ['shadowLayerVis'], min = 0, max = 1, at = 'long', dv = 1)[0]


    mc.connectAttr('{}.outAlpha'.format(file), '{}.input1X'.format(layerVisMdv))
    mc.connectAttr('{}.{}'.format(ctrl, shadowVisAttr), '{}.input2X'.format(layerVisMdv))
    mc.connectAttr('{}.outputX'.format(layerVisMdv), '{}.inputs[{}].alpha'.format(lytNode, int(indexLayer)))


def generateCharFclFx(*args):
    from utaTools.utapy import utaCore
    reload(utaCore)

    ## check selected
    sels = mc.ls(sl = True)
    if not sels or not len(sels) == 2:
        print 'Please select "1.FxPlane_Ctrl , 2.Head_Geo And Run Scrip."'
        return 
        
    ## get name Space
    fxNameSp = utaCore.getNs(nodeName=sels[0])
    charNameSp = utaCore.getNs(nodeName=sels[-1])
    fxGeoGrp = '{}Geo_Grp'.format(fxNameSp)
    charStillGrp = '{}HeadFclRig_Geo'.format(charNameSp)
    ## check control
    name, side, lastName = cn.splitName(sels = [sels[0]])
    offsetCtrlGrp = '{}{}CtrlOfst_Grp'.format(fxNameSp, name)

    ## lists Geo
    fxGeoAll = utaCore.listGeo(sels = fxGeoGrp, nameing = '_Geo', namePass = '_Grp')

    fxGeoLists = []
    for geo in fxGeoAll:
        obj = geo.split('|')[-1]
        fxGeoLists.append(obj)

    # ## add deformer geo blendShape
    # utaCore.addDeformerObj(geometry = fxGeoLists,
    #                     deformerNameLists = ['{}HdUpr_Sqsh'.format(charNameSp),
    #                                         '{}bend1'.format(charNameSp),
    #                                         '{}bend2'.format(charNameSp)],
    #                     orderAddOrRemove = True)
    
    ## fix bug
    ## remove deformer
    orderDeform = ['{}HdUpr_Sqsh'.format(charNameSp), '{}bend1'.format(charNameSp), '{}bend2'.format(charNameSp)]
    for geo in fxGeoLists:
        for deforms in orderDeform:
            mc.select(geo)
            if mc.objExists(deforms):
                utaCore.addDeformerObj(geometry = fxGeoLists, deformerNameLists = [deforms], orderAddOrRemove = False)

    ## duplicate geo for blendShape Buffer
    fxGeoBshGeo, dupBshGrp = utaCore.duplicateRename(dupGrp = '', dupGeo = fxGeoLists, oldName = 'Plane', newName = 'PlaneBsh')
    if dupBshGrp:
        parentChild =  mc.listRelatives(dupBshGrp, p = True)
        if parentChild:
            if not parentChild[0] == '{}Still_Grp'.format(fxNameSp):
                mc.parent(dupBshGrp, '{}Still_Grp'.format(fxNameSp))
        else:
            mc.parent(dupBshGrp, '{}Still_Grp'.format(fxNameSp))
    ## add deformer geo blendShape
    for geo in fxGeoBshGeo:
        for deforms in orderDeform:
            mc.select(geo)
            if mc.objExists(deforms):
                utaCore.addDeformerObj(geometry = fxGeoBshGeo, deformerNameLists = [deforms], orderAddOrRemove = True)

    if fxGeoBshGeo:
        for x in range(len(fxGeoLists)):
            ## add deform [bsnName, 'HdUpr_Sqsh', 'bend2', 'bend1']
            mc.select(fxGeoBshGeo[x], r = True)
            mc.select(fxGeoLists[x], add = True)
            lrr.doAddBlendShape()

        ## parentConstraint Ctrl
        utaCore.parentScaleConstraintLoop(obj = '{}Head_Jnt'.format(charNameSp), lists = [offsetCtrlGrp],par = True, sca = True, ori = False, poi = False)


def addBlendShape(ctrl = '', bshNode = [], attributeTitle = '', getAttrNameFromGrp = 'blendshapeGeo_Grp', attrListsName = [],*args):
    ##Ctrl
    if not ctrl:
        ctrl = mc.ls(sl = True)[0]


    ##Check attribute name
    if not attrListsName:
        lists = mc.listRelatives(getAttrNameFromGrp, c = True)
    else:
        lists = attrListsName

    ## attribute name title
    utaCore.attrNameTitle(obj = ctrl, ln = attributeTitle)

    ## get geo from group
    geoLists = utaCore.listGeo(sels = getAttrNameFromGrp, nameing = '_Geo', namePass = '_Grp') 
    ## create node mdv and attribute
    nodeMdvAll = []
    for i in range(len(lists)):
        utaCore.addAttrCtrl (ctrl = ctrl, elem = [lists[i]], min = 0, max = 10, at = 'float')
        nodeMdv = mc.createNode('multiplyDivide', n = '{}_Mdv'.format(lists[i]))
        mc.setAttr('{}.input2X'.format(nodeMdv), 0.1)
        mc.connectAttr('{}.{}'.format(ctrl, lists[i]), '{}.input1X'.format(nodeMdv))
        nodeMdvAll.append(nodeMdv)

    ## check blendShape Node
    bshNodeAll = []
    for geo in geoLists:
        my_mesh =  geo.split('|')[-1]   
        my_history = mc.listHistory(my_mesh)
        my_blendShape_node = mc.ls( my_history, type='blendShape')
        if my_blendShape_node:
            if not my_blendShape_node[0] in bshNodeAll:
                bshNodeAll.append(my_blendShape_node[0])
        else:
            bshNodeAll.append(my_mesh)
    # check blendShape Name and connect
    for bshEach in bshNodeAll:
        my_blendShape_names = mc.listAttr( bshEach + '.w' , m=True )   
        for x in range(len(nodeMdvAll)):         
            mc.connectAttr('{}.outputX'.format(nodeMdvAll[x]), '{}.{}'.format(bshEach, my_blendShape_names[x]))

    # print my_mesh, '::', my_blendShape_names
    # print my_mesh, '>>', my_blendShape_node[0], '>>', my_blendShape_names[i]

def addBlendShapeForMain(ctrl = '', bffrName = 'Bffr', attributeTitle = '', getAttrNameFromGrp = 'BlendshapeGeo_Grp', *args):
    ##Ctrl
    if not ctrl:
        ctrl = mc.ls(sl = True)[0]

    ##Check attribute name
    if getAttrNameFromGrp:
        lists = mc.listRelatives(getAttrNameFromGrp, c = True)
    else:
        print 'Please typing "getAttrNameFromGrp" Grp'
        return 

    ## attribute name title
    if attributeTitle:
        if not mc.objExists('{}.{}'.format(ctrl, attributeTitle)):
            utaCore.attrNameTitle(obj = ctrl, ln = attributeTitle)
    ## get geo from group
    bffrListGrp = mc.listRelatives(getAttrNameFromGrp, c = True)
    bffrGeoGrp = utaCore.listGeo(sels = bffrListGrp[0], nameing = '_Geo', namePass = '_Grp')
    geoDupBffr = bffrListGrp[0].split('Geo_Grp')[0]
    bffrGeo = []
    for each in bffrGeoGrp:
        geoName = each.split('|')[-1] 
        geo = 'bodyRig_md:' + geoName.replace(geoDupBffr, '')
        if mc.objExists(geo):
            bffrGeo.append(geo)

    ## duplicate fclGeoGrp
    dupBffrGeoGrp = []
    dupGrpName = 'dupBsh{}Geo_Grp'.format(bffrName)
    if bffrGeo:
        if not mc.objExists(dupGrpName):
            dupBshGrp = mc.group(n = dupGrpName, em = True)

        else:
            dupBshGrp = dupGrpName
        if mc.objExists('BshRigStill_Grp'):
            mc.parent(dupBshGrp, 'BshRigStill_Grp')
        ## duplicate fclGeoGrp
        for geoDup in bffrGeo:
            geoObj = mc.duplicate(geoDup, rc = True, )[0]
            geoEach = geoObj.replace('_Geo', '{}_Geo'.format(bffrName))
            each = mc.rename(geoObj, geoEach)
            dupBffrGeoGrp.append(each)

            mc.parent(each, dupBshGrp)
            
    bffrGeoLists = utaCore.listGeo(sels = dupGrpName, nameing = '_Geo', namePass = '_Grp') 

    ## Add blendShape Buffer Geo to bodyRig Geo
    for x in range(len(bffrGeoLists)):
        sourceBsh = bffrGeoLists[x]
        targetBsh = bffrGeo[x]
        mc.select(sourceBsh, r = True)
        mc.select(targetBsh, add = True)
        lrr.doAddBlendShape()

    ## Add blendShape geo to bffr geo
    for u in range(len(lists)):
        bshNameGrp = lists[u].split('Geo_Grp')[0]
        bshGeoLists = utaCore.listGeo(sels = lists[u], nameing = '_Geo', namePass = '_Grp')

        ## generate node mdv for bsh
        nodeMdvAll = []
        if 'Geo_Grp' in lists[u]:
            attrName = lists[u].replace('Geo_Grp', 'Bsh')
        else:
            attrName = lists[u]
        utaCore.addAttrCtrl (ctrl = ctrl, elem = [attrName], min = 0, max = 10, at = 'float')
        nodeMdv = mc.createNode('multiplyDivide', n = '{}_Mdv'.format(attrName))
        mc.setAttr('{}.input2X'.format(nodeMdv), 0.1)
        mc.connectAttr('{}.{}'.format(ctrl, attrName), '{}.input1X'.format(nodeMdv))
        nodeMdvAll.append(nodeMdv)


        for x in range(len(bshGeoLists)):
            sourceBshGeoName = bshGeoLists[x].split('|')[-1]
            sourceBshSp = sourceBshGeoName.split(bshNameGrp)[0]
  
            targetBffrGeoName = sourceBshSp + 'Bffr_Geo'
            mc.select(sourceBshGeoName, r = True)
            mc.select(targetBffrGeoName, add = True)
            lrr.doAddBlendShape()

            ## connect node mdv
            mc.connectAttr('{}.outputX'.format(nodeMdv), '{}Bffr_Bsn.{}{}_Geo'.format(sourceBshSp, sourceBshSp, bshNameGrp))

    ## wrap group
    if mc.objExists('BshRigStill_Grp'):
        mc.parent('BshRigStill_Grp', 'bodyRig_md:Still_Grp')
        mc.parent('BshRigCtrl_Grp', 'bodyRig_md:AddCtrl_Grp')

def ignarePullUv(*args):
    import maya.cmds as mc
    mc.addAttr('Head_Geo', ln = 'ignorePullUV', at = 'bool', k =1 )
    mc.setAttr('Head_Geo.ignorePullUV',1)


def addSeq(Iris = True, IrisSide = ['L', 'R'], Blink = True, BlinkSide = ['L', 'R'], Head = True, Hair = True, Hat = False, texturePath = '',*args):
    if Iris:
        ## add Seq -----------------------------------------------------------------
        sides = IrisSide
        for selSide in sides:
            selSide = '_{}_'.format(selSide)
            addSeqToCtrl(mainCtrl = 'Variety_Ctrl', 
                            subCtrl = 'IrisUiRig{}Ctrl'.format(selSide),
                            elem = 'EyeBall', 
                            subName = 'Iris',
                            texturePath = texturePath)

            print 'Process :: Iris addSeq {} >> DONE'.format(selSide)
    if Blink:
        sides = BlinkSide
        for selSide in sides:
            selSide = '_{}_'.format(selSide)
            addSeqToCtrl(mainCtrl = 'Variety_Ctrl', 
                            subCtrl = 'BlinkUiRig{}Ctrl'.format(selSide),
                            elem = 'EyeBall', 
                            subName = 'Blink',
                            texturePath = texturePath)

            print 'Process :: Blink addSeq {} >> DONE'.format(selSide)
    if Head:
        addSeqToCtrl(mainCtrl = 'Variety_Ctrl', 
                        subCtrl = 'FacialUiRig_Ctrl',
                        elem = '', 
                        subName = 'Head',
                        texturePath = texturePath)

        print 'Process :: Head addSeq >> DONE'
    if Hair:
        addSeqToCtrl(mainCtrl = 'Variety_Ctrl', 
                        subCtrl = '',
                        elem = '', 
                        subName = 'Hair',
                        texturePath = texturePath)
    if Hat:
        addSeqToCtrl(mainCtrl = 'Variety_Ctrl', 
                        subCtrl = '',
                        elem = '', 
                        subName = 'Hat',
                        texturePath = texturePath)
        print 'Process :: Hair addSeq >> DONE'
        
def setUvTilingMode(*args):


    import maya.cmds as cmds

    allFileNodes = cmds.ls(type="file")

    for f in allFileNodes:
        if not cmds.getAttr("{}.useFrameExtension".format(f)):
            continue
            
        cmds.setAttr("{}.uvTilingMode".format(f), 0)



def uiCtrlLocalWorld(ctrl = 'EyelashUiRig_L_Ctrl', local = 'EyeUiRig_L_Ctrl', world = 'FacialUiRig_Ctrl', sides = ['L', 'R'], parent = 'EyelashUiRigCtrl_L_Grp', *args):
    for side in sides:
        if 'L' in side:
            side = '_L_'
        elif 'R' in side:
            side = '_R_'
        else:
            side = '_'

        ## ctrl split new side
        ctrlName, ctrlSide, ctrlLastName = cn.splitName(sels = [ctrl])  
        newCtrl = '{}{}{}'.format(ctrlName, side, ctrlLastName)

        ## local split new side
        localName, localSide, localLastName = cn.splitName(sels = [local])  
        newLocal = '{}{}{}'.format(localName, side, localLastName)     
   
        ## parent split new side
        parentName, parentSide, parentLastName = cn.splitName(sels = [parent])  
        newParent = '{}{}{}'.format(parentName, side, parentLastName)    

        utaCore.parentLocalWorldCtrl( ctrl = newCtrl , localObj = newLocal , worldObj = world , parGrp = newParent)

        # ## 03 generate Specular SpB follow iris switch 
        # cfm.generateBlinkFollowIris(    ctrl = newEyeBallSpecularSpBCtrl, 
        #                                 BlinkCorPlace2DTexture = SpecularSpBCorPlace2DTexture, 
        #                                 IrisCorePlace2DTexture = IrisCorPlace2DTexture,
        #                                 )


def facialMirror(uiCtrlGrp = 'fclRig_md:OverallUiRig_Ctrl', swGeoGrp = 'fclRig_md:FacePlaneSwRigGeo_Grp',fclGeoGrp = 'bodyRig_md:FacePlaneGeo_Grp',*args):
    uiCtrl = []
    uiCtrlAll = utaCore.listGeo(sels = uiCtrlGrp, nameing = '_Ctrl', namePass = '_Grp')

    ## off wrap = 0
    for ctrl in uiCtrlAll:
        ctrl = ctrl.split('|')[-1]
        if not '_Geo' in ctrl:
            if mc.objExists('{}.wrap'.format(ctrl)):
                mc.setAttr('{}.wrap'.format(ctrl), 0)
            uiCtrl.append(ctrl)

    swGeoGrpDup = mc.duplicate(swGeoGrp)
    ## rename Sw > SwMrr
    swMrrGeoGrpDup = []
    for geoEach in swGeoGrpDup:
        newName = geoEach.replace('Sw', 'MrrSw')
        mc.rename(geoEach, newName)
        swMrrGeoGrpDup.append(newName)

    ## off wrap = 1
    for ctrl in uiCtrlAll:
        ctrl = ctrl.split('|')[-1]
        if not '_Geo' in ctrl:
            if mc.objExists('{}.wrap'.format(ctrl)):
                mc.setAttr('{}.wrap'.format(ctrl), 1)
            uiCtrl.append(ctrl)

    mc.setAttr('{}.scaleX'.format(swMrrGeoGrpDup[0]), -1)
    ## addBlendShape DtlRigGeo to SwRigGeo
    addBshToFclRig(source = swGeoGrp, target = swMrrGeoGrpDup, sourceProcess = 'Sw', targetProcess = 'MrrSw', frontOfChain = True, origin = 'local', changOrderBsh = False)

    # ## listsGeo and turn off envelope
    fclNameSp = fclGeoGrp.split(':')[0]
    fclGeoAll = utaCore.listGeo(sels = fclGeoGrp, nameing = '_Geo', namePass = '_Grp')
    fclSwMrrGeoAll = utaCore.listGeo(sels = swMrrGeoGrpDup, nameing = '_Geo', namePass = '_Grp')

    ## duplicate fclGeoGrp
    fclMrrGeoGrp = utaCore.duplicateRename(dupGrp = fclGeoGrp, oldName = 'PlaneFclRig', newName = 'PlaneMrr')

    ## lists FclMrr Geo
    fclMrrGeoLists = utaCore.listGeo(sels = fclMrrGeoGrp, nameing = '_Geo', namePass = '_Grp')
    ## listsGeo
    for i in range(len(fclGeoAll)):
        geo = fclGeoAll[i].split('|')[-1]
        bsnName = geo.replace('_Geo', 'Mrr_Bsn')
        deformLists = ['HdUpr_Sqsh', 'bend1', 'bend2']

        ## add deform [bsnName, 'HdUpr_Sqsh', 'bend2', 'bend1']
        mc.select(fclSwMrrGeoAll[i], r = True)
        mc.select(fclMrrGeoLists[i], add = True)
        lrr.doAddBlendShape()

        for defomAdd in deformLists:
            utaCore.addDeformerMemberObj(deformerName = '{}:{}'.format(fclNameSp, defomAdd), geometry = fclMrrGeoLists[i])

    ## ignore attribute
    utaCore.ignorePullUV(geoGrp  = fclMrrGeoGrp)
    ## mirror -1
    mc.setAttr('{}.scaleX'.format(fclMrrGeoGrp[0]), -1)
    ##BindSkin MrrFclRig
    for o in range(len(fclMrrGeoLists)):
        mrrFclGeo = fclMrrGeoLists[o].split('|')[-1]
        utaCore.bindSkinCluster(jntObj = ['bodyRig_md:JawUpr1_Jnt'], obj = [mrrFclGeo])
    # ## wrap grup
    mc.parent(fclMrrGeoGrp[0], 'bodyRig_md:AllGeo_Grp', )

    return fclMrrGeoGrp[0]



def facialMirrorAttribute(fclCtrl = 'bodyRig_md:Head_Ctrl',fclGeoGrp =['bodyRig_md:FacePlaneGeo_Grp', 'FacePlaneMrrGeo_Grp'], uiCtrl = 'fclRig_md:OverallUiRig_Ctrl', teethVisList = ['bodyRig_md:Teeth_Geo', 'dtlLipsRig_md:TeethCtrl_Grp'], *args):
    ## add attribute fcl function
    name = 'facialSide'
    sideOp = ['R', 'L']
    utaCore.attrNameTitle(obj = fclCtrl, ln = 'facialFunction')
    utaCore.attrEnum(obj = fclCtrl, ln = name, en ='{} : {}'.format(sideOp[0], sideOp[-1]))

    ## addAttribute Teeth Vis
    utaCore.addAttrCtrl (ctrl = fclCtrl, elem = ['teethVis'], min = 0, max = 1, at = 'long')
    mc.setAttr('{}.teethVis'.format(fclCtrl), 1)
    for obj in teethVisList:
        mc.connectAttr('{}.teethVis'.format(fclCtrl), '{}.v'.format(obj))
    
    ## connection attribute fcl function
    for x in range(len(fclGeoGrp)):
        fclCon = mc.createNode('condition', n = '{}ShadowMrr_{}_Con'.format(name, sideOp[x]))
        mc.setAttr('{}.colorIfTrueR'.format(fclCon),1)
        mc.setAttr('{}.colorIfFalseR'.format(fclCon),0)
        mc.setAttr('{}.secondTerm'.format(fclCon),x)
        mc.connectAttr('{}.{}'.format(fclCtrl, name), '{}.firstTerm'.format(fclCon))
        mc.connectAttr('{}.outColor.outColorR'.format(fclCon, ), '{}.v'.format(fclGeoGrp[x]))

    ## check nameSp Node
    nameSp = uiCtrl.split(':')[0]

        
    ## check shadow control
    uiCtrlLists = []
    uiCtrlAll = []
    uiCtrlLists = utaCore.listGeo(sels = uiCtrl, nameing = '_Ctrl', namePass = '_Grp')
    for each in uiCtrlLists:
        if not '_Geo' in each.split('|')[-1]:
            if 'Shadow' in each.split('|')[-1]:
                uiCtrlAll.append(each.split('|')[-1])

    ## connect control to shadow node
    for w in range(len(uiCtrlAll)):
        ## split name
        name, side, lastName = cn.splitName(sels = [uiCtrlAll[w]])

        fclUiCon = mc.createNode('condition', n = '{}ShadowMrr{}Con'.format(name, side))
        mc.setAttr('{}.colorIfTrueR'.format(fclUiCon),1)
        mc.setAttr('{}.colorIfFalseR'.format(fclUiCon),-1)
        mc.setAttr('{}.secondTerm'.format(fclUiCon),0)

        fclUiMdv = mc.createNode('multiplyDivide', n = '{}ShadowMrr{}Mdv'.format(name, side))

        mc.connectAttr('{}.facialSide'.format(fclCtrl),  '{}.firstTerm'.format(fclUiCon))
        mc.connectAttr('{}.outColor.outColorR'.format(fclUiCon),  '{}.input2X'.format(fclUiMdv))
        mc.connectAttr('{}Shape.offsetUValueAmp'.format(uiCtrlAll[w]),  '{}.input1X'.format(fclUiMdv))
        mc.connectAttr('{}.outputX'.format(fclUiMdv),  '{}:{}ShadowoffsetUV{}Mdv.input1X'.format(nameSp, name, side), f = True)

def assambleCakeHoundMainGenerateBuffer(geoBffrLists = [    'bodyRig_md:Head_Geo', 
                                                            'bodyRig_md:Cream_Geo', 
                                                            'bodyRig_md:Teeth_Geo',
                                                            'bodyRig_md:Tongue_Geo'], *args):
    ## create group
    if not mc.objExists('AddRigStillGrp_Grp'):
        addRigStillGrp = mc.group(n = 'AddRigStillGrp_Grp', em = True)
    else:
        addRigStillGrp = 'AddRigStillGrp_Grp'
    if not mc.objExists('AddRigCtrlGrp_Grp'):
        addRigCtrlGrp = mc.group(n = 'AddRigCtrlGrp_Grp', em = True)
    else:
        addRigCtrlGrp = 'AddRigCtrlGrp_Grp'

    bffrGrp = mc.group(n = 'fclBffrGeo_Grp', em = True)
    bffrGeo = []
    ## duplicate fclGeoGrp
    for x in range(len(geoBffrLists)):
        geoObj = utaCore.duplicateRename(dupGrp = geoBffrLists[x], oldName = '_Geo', newName = 'Bffr_Geo')
        bffrGeo.append(geoObj[0])
        mc.parent(geoObj, bffrGrp)
        ## add bffr blendShape
        mc.select(geoObj[0], r = True)
        mc.select(geoBffrLists[x], add = True)
        lrr.doAddBlendShape()

    return bffrGeo

def assambleCakeHoundMainGenerateBsh(   geoBffrLists = ['bodyRig_md:Head_Geo', 
                                                        'bodyRig_md:Cream_Geo', 
                                                        'bodyRig_md:Teeth_Geo',
                                                        'bodyRig_md:Tongue_Geo'],
                                        ctrlAttrBsh =   'bodyRig_md:Head_Ctrl', 
                                        bshGeoGrp =     'modelBsh_md:BlendshapeGeo_Grp', *args):
    ## create group
    if not mc.objExists('AddRigStillGrp_Grp'):
        addRigStillGrp = mc.group(n = 'AddRigStillGrp_Grp', em = True)
    else:
        addRigStillGrp = 'AddRigStillGrp_Grp'
    if not mc.objExists('AddRigCtrlGrp_Grp'):
        addRigCtrlGrp = mc.group(n = 'AddRigCtrlGrp_Grp', em = True)
        mc.setAttr('{}.inheritsTransform'.format(addRigCtrlGrp), 0)
    else:
        addRigCtrlGrp = 'AddRigCtrlGrp_Grp'

    ## add title blendShape
    if not mc.objExists('{}.fclBshFunction'.format(ctrlAttrBsh)):
        utaCore.attrNameTitle(obj = ctrlAttrBsh, ln = 'fclBshFunction')

    ## buffer form blendShape
    bffrGeo = []
    if not mc.objExists('fclBffrGeo_Grp'):
        bffrGeo = assambleCakeHoundMainGenerateBuffer(geoBffrLists = geoBffrLists)
    else:
        bffrGeoLists = utaCore.listGeo(sels = 'fclBffrGeo_Grp', nameing = '_Geo', namePass = '_Grp')
        for geo in bffrGeoLists:
            bffrGeo.append(geo.split('|')[-1])

    ## check Name for add blendShape
    for geo in bffrGeo:
        nameGeo = geo.split('Bffr')[0]

        ## bshGeo liste Geo
        bshGeoGrpLists = mc.listRelatives(bshGeoGrp, c = True)
        for y in range(len(bshGeoGrpLists)):
        # for bshGrp in bshGeoGrpLists:
            bshGeoLists = utaCore.listGeo(sels = bshGeoGrpLists[y], nameing = '_Geo', namePass = '_Grp')
            for u in range(len(bshGeoLists)):
                obj = bshGeoLists[u].split('|')[-1]
                if nameGeo in obj:
                    ## add bshGeo blendShape
                    mc.select(bshGeoLists[u], r = True)
                    mc.select(geo, add = True)
                    lrr.doAddBlendShape()

            nameLists = []
            attrNameBsh = bshGeoGrpLists[y].split(':')[-1].replace('_Grp', '')
            nameLists.append(attrNameBsh)
            ## create Node mdv and connect control to blendShape node
            if not mc.objExists('{}.{}'.format(ctrlAttrBsh, attrNameBsh)):
                utaCore.addAttrCtrl (ctrl = ctrlAttrBsh, elem = [attrNameBsh], min = 0, max = 10, at = 'float')

            nameNode = mc.createNode('multiplyDivide', n = '{}{}{}_Mdv'.format(attrNameBsh, nameGeo, y + 1))
            mc.setAttr('{}.input2X'.format(nameNode),0.1)
            mc.connectAttr('{}.{}'.format(ctrlAttrBsh, attrNameBsh), '{}.input1X'.format(nameNode))
            if y == 0:
                y = ''
            mc.connectAttr('{}.outputX'.format(nameNode), '{}Bffr_Bsn.{}_Geo{}'.format(nameGeo, nameGeo, y))

    ## wrap group
    ## check parent
    DeleteGrp.deleteGrp(obj = 'modelBsh_md:BshRigStill_Grp')
    mc.setAttr('Delete_Grp.v', 0)

    still = ['fclBffrGeo_Grp', 'modelBsh_md:BshRigCtrl_Grp']
    for obj in still:
        if mc.listRelatives(obj, p = True):
            a = mc.listRelatives(obj, p = True)[0]
            if not 'AddRigStillGrp_Grp' == a:
                mc.parent(obj, addRigStillGrp)
        else:
            mc.parent(obj, addRigStillGrp)
    
    if mc.listRelatives(addRigStillGrp, p = True):
        b = mc.listRelatives(addRigStillGrp, p = True)[0]
        if not 'bodyRig_md:Still_Grp' == b:
            mc.parent(addRigStillGrp, 'bodyRig_md:Still_Grp')
    else:
        mc.parent(addRigStillGrp, 'bodyRig_md:Still_Grp')

    if mc.listRelatives(addRigCtrlGrp, p = True):
        c = mc.listRelatives(addRigCtrlGrp, p = True)[0]
        if not 'bodyRig_md:AddCtrl_Grp' == c:
            mc.parent(addRigCtrlGrp, 'bodyRig_md:AddCtrl_Grp')
    else:
        mc.parent(addRigCtrlGrp, 'bodyRig_md:AddCtrl_Grp')

    ## turn off blendShape ear
    mc.setAttr('Strawberry_Bsn.envelope',0)
    
def assambleCakeHoundLctRig(geoBffrLists = [  'bodyRig_md:Head_Geo', 
                                            'bodyRig_md:Cream_Geo', 
                                            'bodyRig_md:Teeth_Geo',
                                            'bodyRig_md:Tongue_Geo'], 
                            lctRigGeoGrp = 'lctRig_md:LatticeGeo_Grp',*args):

    ## create group
    if not mc.objExists('AddRigStillGrp_Grp'):
        addRigStillGrp = mc.group(n = 'AddRigStillGrp_Grp', em = True)
    else:
        addRigStillGrp = 'AddRigStillGrp_Grp'
    if not mc.objExists('AddRigCtrlGrp_Grp'):
        addRigCtrlGrp = mc.group(n = 'AddRigCtrlGrp_Grp', em = True)
        mc.setAttr('{}.inheritsTransform'.format(addRigCtrlGrp), 0)
    else:
        addRigCtrlGrp = 'AddRigCtrlGrp_Grp'

    ## buffer form blendShape
    bffrGeo = []
    if not mc.objExists('fclBffrGeo_Grp'):
        bffrGeo = assambleCakeHoundMainGenerateBuffer(geoBffrLists = geoBffrLists)
    else:
        bffrGeoLists = utaCore.listGeo(sels = 'fclBffrGeo_Grp', nameing = '_Geo', namePass = '_Grp')
        for geo in bffrGeoLists:
            bffrGeo.append(geo.split('|')[-1]) 

    ## check Name for add blendShape
    for geo in bffrGeo:
        nameGeo = geo.split('Bffr')[0]

        ## lctRig liste Geo
        lctRigGeoLists = utaCore.listGeo(sels = lctRigGeoGrp, nameing = '_Geo', namePass = '_Grp')
        for u in range(len(lctRigGeoLists)):
            obj = lctRigGeoLists[u].split('|')[-1]
            if nameGeo in obj:
                ## add lctRig blendShape
                mc.select(lctRigGeoLists[u], r = True)
                mc.select(geo, add = True)
                lrr.doAddBlendShape()

    ## parentConstraint lctRig Ctrl
    lctRigDtlUp = ['lctRig_md:HeadLctRigLttSmearUpCtrl_C_Grp', 'lctRig_md:HeadLctRigLttSmearMidCtrl_C_Grp']
    for grop in lctRigDtlUp:
        lctDtlCtrl = mc.listRelatives(grop, c = True)
        for lctDtl in lctDtlCtrl:
            utaCore.parentScaleConstraintLoop(obj = 'bodyRig_md:JawUpr1_Jnt', lists = [lctDtl],par = True, sca = True, ori = False, poi = False)
    lctRigDtlDnGrp = 'lctRig_md:HeadLctRigLttSmearDnCtrl_C_Grp'
    lctRigDtlDn = mc.listRelatives(lctRigDtlDnGrp, c = True)
    for lctDtl in lctRigDtlDn:
        utaCore.parentScaleConstraintLoop(obj = 'bodyRig_md:JawLwr2_Jnt', lists = [lctDtl],par = True, sca = True, ori = False, poi = False)

    ## wrap group
    ## check parent
    still = ['fclBffrGeo_Grp', 'lctRig_md:LctRigStill_Grp']
    ctrl = ['lctRig_md:LctRigCtrl_Grp']
    mc.parent(ctrl, addRigCtrlGrp)

    for obj in still:
        if mc.listRelatives(obj, p = True):
            a = mc.listRelatives(obj, p = True)[0]
            if not 'AddRigStillGrp_Grp' == a:
                mc.parent(obj, addRigStillGrp)
        else:
            mc.parent(obj, addRigStillGrp)
    
    if mc.listRelatives(addRigStillGrp, p = True):
        b = mc.listRelatives(addRigStillGrp, p = True)[0]
        if not 'bodyRig_md:Still_Grp' == b:
            mc.parent(addRigStillGrp, 'bodyRig_md:Still_Grp')
    else:
        mc.parent(addRigStillGrp, 'bodyRig_md:Still_Grp')

    if mc.listRelatives(addRigCtrlGrp, p = True):
        c = mc.listRelatives(addRigCtrlGrp, p = True)[0]
        if not 'bodyRig_md:AddCtrl_Grp' == c:
            mc.parent(addRigCtrlGrp, 'bodyRig_md:AddCtrl_Grp')
    else:
        mc.parent(addRigCtrlGrp, 'bodyRig_md:AddCtrl_Grp')


def assambleCakeHoundDtlLipsRig(geoBffrLists = ['bodyRig_md:Head_Geo', 
                                                'bodyRig_md:Cream_Geo', 
                                                'bodyRig_md:Teeth_Geo',
                                                'bodyRig_md:Tongue_Geo'], 
                            dtlLipsWrapGepStill = ['Head', 'Teeth', 'Tongue'],
                            dtlLipsRigGeoGrp = 'dtlLipsRig_md:DtlLipsGeo_Grp',
                            dtlLipsUpDnGeo = ['dtlLipsRig_md:LipsUpWrap_Geo', 'dtlLipsRig_md:LipsDnWrap_Geo'],
                            dtlLipsWrapGeo = ['TeethWrap_Geo', 
                                            'dtlLipsRig_md:LipsUpWrap_Geo', 
                                            'dtlLipsRig_md:LipsDnWrap_Geo', 
                                            'TongueWrap_Geo'],
                            dtlLipsNrbGrp = ['dtlLipsRig_md:TeethStill_Grp', 
                                            'dtlLipsRig_md:LipsUpDtlNrb_Grp', 
                                            'dtlLipsRig_md:LipsDnDtlNrb_Grp', 
                                            'dtlLipsRig_md:TongueStill_Grp'], 
                            dtlLipsLocalUpConstraint = 'bodyRig_md:JawUpr1_Jnt',
                            dtlLipsLocalDnConstraint = 'bodyRig_md:JawLwr2_Jnt',
                            dtlLipsLocalUpCtrl = ['dtlLipsRig_md:TeethUpDtlCtrl_Grp', 'dtlLipsRig_md:LipsDtlUpCtrlZro_Grp'],
                            dtlLipsLocalDnCtrl = ['dtlLipsRig_md:TeethDnDtlCtrl_Grp', 'dtlLipsRig_md:LipsDtlDnCtrlZro_Grp', 'dtlLipsRig_md:TongueCtrl_Grp'],
                                            *args):


    ## create group
    if not mc.objExists('AddRigStillGrp_Grp'):
        addRigStillGrp = mc.group(n = 'AddRigStillGrp_Grp', em = True)
    else:
        addRigStillGrp = 'AddRigStillGrp_Grp'
    if not mc.objExists('AddRigCtrlGrp_Grp'):
        addRigCtrlGrp = mc.group(n = 'AddRigCtrlGrp_Grp', em = True)
        mc.setAttr('{}.inheritsTransform'.format(addRigCtrlGrp), 0)
    else:
        addRigCtrlGrp = 'AddRigCtrlGrp_Grp'

    ## buffer form blendShape
    bffrGeo = []
    if not mc.objExists('fclBffrGeo_Grp'):
        bffrGeo = assambleCakeHoundMainGenerateBuffer(geoBffrLists = geoBffrLists)
    else:
        bffrGeoLists = utaCore.listGeo(sels = 'fclBffrGeo_Grp', nameing = '_Geo', namePass = '_Grp')
        for geo in bffrGeoLists:
            bffrGeo.append(geo.split('|')[-1]) 

    ## check Name for add blendShape
    for geo in bffrGeo:
        nameGeo = geo.split('Bffr')[0]

        ## dtlLipsRig liste Geo
        dtlLipsRigLists = utaCore.listGeo(sels = dtlLipsRigGeoGrp, nameing = '_Geo', namePass = '_Grp')
        for u in range(len(dtlLipsRigLists)):
            obj = dtlLipsRigLists[u].split('|')[-1]
            if nameGeo in obj:
                ## add dtlLipsRig blendShape
                mc.select(dtlLipsRigLists[u], r = True)
                mc.select(geo, add = True)
                lrr.doAddBlendShape()

    ## name space
    nameSp = dtlLipsRigGeoGrp.split(':')[0]

    ## wrap dtl up and dn
    for geoUpDn in geoBffrLists:
        for geoWrap in dtlLipsWrapGepStill:
            if geoWrap in geoUpDn:
                headWrapGeo = utaCore.duplicateRename(dupGrp = geoUpDn, oldName = '_Geo', newName = 'Wrap_Geo')
                ## add dtlLipsRig blendShape
                mc.select(geoUpDn, r = True)
                mc.select(headWrapGeo, add = True)
                lrr.doAddBlendShape()
                mc.parent(headWrapGeo, addRigStillGrp)
                headStillName = '{}:{}Still_Grp'.format(nameSp, geoWrap)

                if 'Head' in geoWrap:
                    for wrapGeo in dtlLipsUpDnGeo:
                        mc.select(wrapGeo, r=True)
                        mc.select(headWrapGeo, add=True)
                        mc.CreateWrap()
    ##headWrapGeo
    ##TongueWrapGeo
    ##TeethWrapGeo

    ## wrap nrb
    for p in range(len(dtlLipsWrapGeo)):
        nrbObj = mc.listRelatives(dtlLipsNrbGrp[p], c = True)
        mc.select(nrbObj, r=True)
        mc.select(dtlLipsWrapGeo[p], add=True)
        mc.CreateWrap()
    ## constraint local Up
    for u in range(len(dtlLipsLocalUpCtrl)):
        objLocalUp = mc.listRelatives(dtlLipsLocalUpCtrl[u], c = True)
        utaCore.parentScaleConstraintLoop(obj = dtlLipsLocalUpConstraint, lists = objLocalUp,par = False, sca = True, ori = True, poi = False)

    ## constraint local Dn
    for u in range(len(dtlLipsLocalDnCtrl)):
        objLocalDn = mc.listRelatives(dtlLipsLocalDnCtrl[u], c = True)
        utaCore.parentScaleConstraintLoop(obj = dtlLipsLocalDnConstraint, lists = objLocalDn,par = False, sca = True, ori = True, poi = False)

    ## wrap group
    ## check parent
    still = ['fclBffrGeo_Grp', 'dtlLipsRig_md:DtlLipsJnt_Grp', 'dtlLipsRig_md:DtlLipsStill_Grp', 'dtlLipsRig_md:DtlLipsGeo_Grp']
    ctrl = ['dtlLipsRig_md:DtlLipsCtrl_Grp']
    mc.parent(ctrl, addRigCtrlGrp)

    for obj in still:
        if mc.listRelatives(obj, p = True):
            a = mc.listRelatives(obj, p = True)[0]
            if not 'AddRigStillGrp_Grp' == a:
                mc.parent(obj, addRigStillGrp)
        else:
            mc.parent(obj, addRigStillGrp)
    
    if mc.listRelatives(addRigStillGrp, p = True):
        b = mc.listRelatives(addRigStillGrp, p = True)[0]
        if not 'bodyRig_md:Still_Grp' == b:
            mc.parent(addRigStillGrp, 'bodyRig_md:Still_Grp')
    else:
        mc.parent(addRigStillGrp, 'bodyRig_md:Still_Grp')

    if mc.listRelatives(addRigCtrlGrp, p = True):
        c = mc.listRelatives(addRigCtrlGrp, p = True)[0]
        if not 'bodyRig_md:AddCtrl_Grp' == c:
            mc.parent(addRigCtrlGrp, 'bodyRig_md:AddCtrl_Grp')
    else:
        mc.parent(addRigCtrlGrp, 'bodyRig_md:AddCtrl_Grp')

    ## set inheritsTransform
    mc.setAttr('bodyRig_md:Ear1_L_Jnt.inheritsTransform', 0)
    mc.setAttr('bodyRig_md:Ear1_R_Jnt.inheritsTransform', 0)
    mc.setAttr('bodyRig_md:Tail1_Jnt.inheritsTransform', 0)
    mc.setAttr('bodyRig_md:Ear1_L_Jnt.inheritsTransform', 1)
    mc.setAttr('bodyRig_md:Ear1_R_Jnt.inheritsTransform', 1)
    mc.setAttr('bodyRig_md:Tail1_Jnt.inheritsTransform', 1)
    ## fix  scape tail and ear
    utaCore.parentScaleConstraintLoop(obj = 'bodyRig_md:Offset_Ctrl', lists = ['bodyRig_md:TailRivRvt_Grp', 'bodyRig_md:EarRivRvt_L_Grp', 'bodyRig_md:EarRivRvt_R_Grp'],par = False, sca = True, ori = False, poi = False)

def assambleCakeHoundMain(geoBffrLists = [  'bodyRig_md:Head_Geo', 
                                            'bodyRig_md:Cream_Geo', 
                                            'bodyRig_md:Teeth_Geo',
                                            'bodyRig_md:Tongue_Geo'], 
                            lctRigGeoGrp = 'lctRig_md:LatticeGeo_Grp', 
                            dtlLipsRigGeoGrp = 'dtlLipsRig_md:DtlLipsGeo_Grp',
                            ctrlAttrBsh = 'bodyRig_md:Head_Ctrl', 
                            bshGeoGrp = 'modelBsh_md:BlendshapeGeo_Grp',*args):

    ## create group
    addRigStillGrp = mc.group(n = 'AddRigStillGrp_Grp', em = True)
    addRigCtrlGrp = mc.group(n = 'AddRigCtrlGrp_Grp', em = True)
    mc.setAttr('{}.inheritsTransform'.format(addRigCtrlGrp), 0)

    bffrGrp = mc.group(n = 'fclBffrGeo_Grp', em = True)
    bffrGeo = []
    ## duplicate fclGeoGrp
    for x in range(len(geoBffrLists)):
        geoObj = utaCore.duplicateRename(dupGrp = geoBffrLists[x], oldName = '_Geo', newName = 'Bffr_Geo')
        bffrGeo.append(geoObj[0])
        mc.parent(geoObj, bffrGrp)

        ## add bffr blendShape
        mc.select(geoObj[0], r = True)
        mc.select(geoBffrLists[x], add = True)
        lrr.doAddBlendShape()

    ## add title blendShape
    utaCore.attrNameTitle(obj = ctrlAttrBsh, ln = 'fclBshFunction')

    ## check Name for add blendShape
    for geo in bffrGeo:
        nameGeo = geo.split('Bffr')[0]

        ## lctRig liste Geo
        lctRigGeoLists = utaCore.listGeo(sels = lctRigGeoGrp, nameing = '_Geo', namePass = '_Grp')
        for u in range(len(lctRigGeoLists)):
            obj = lctRigGeoLists[u].split('|')[-1]
            if nameGeo in obj:
                ## add lctRig blendShape
                mc.select(lctRigGeoLists[u], r = True)
                mc.select(geo, add = True)
                lrr.doAddBlendShape()

        ## dtlLipsRig liste Geo
        dtlLipsRigLists = utaCore.listGeo(sels = dtlLipsRigGeoGrp, nameing = '_Geo', namePass = '_Grp')
        for u in range(len(dtlLipsRigLists)):
            obj = dtlLipsRigLists[u].split('|')[-1]
            if nameGeo in obj:
                ## add dtlLipsRig blendShape
                mc.select(dtlLipsRigLists[u], r = True)
                mc.select(geo, add = True)
                lrr.doAddBlendShape()

        ## bshGeo liste Geo
        bshGeoGrpLists = mc.listRelatives(bshGeoGrp, c = True)
        for y in range(len(bshGeoGrpLists)):
        # for bshGrp in bshGeoGrpLists:
            bshGeoLists = utaCore.listGeo(sels = bshGeoGrpLists[y], nameing = '_Geo', namePass = '_Grp')
            for u in range(len(bshGeoLists)):
                obj = bshGeoLists[u].split('|')[-1]
                if nameGeo in obj:
                    ## add bshGeo blendShape
                    mc.select(bshGeoLists[u], r = True)
                    mc.select(geo, add = True)
                    lrr.doAddBlendShape()

            nameLists = []
            attrNameBsh = bshGeoGrpLists[y].split(':')[-1].replace('_Grp', '')
            nameLists.append(attrNameBsh)
            ## create Node mdv and connect control to blendShape node
            if not mc.objExists('{}.{}'.format(ctrlAttrBsh, attrNameBsh)):
                utaCore.addAttrCtrl (ctrl = ctrlAttrBsh, elem = [attrNameBsh], min = 0, max = 10, at = 'float')

            nameNode = mc.createNode('multiplyDivide', n = '{}{}{}_Mdv'.format(attrNameBsh, nameGeo, y + 1))
            mc.setAttr('{}.input2X'.format(nameNode),0.1)
            mc.connectAttr('{}.{}'.format(ctrlAttrBsh, attrNameBsh), '{}.input1X'.format(nameNode))
            if y == 0:
                y = ''
            mc.connectAttr('{}.outputX'.format(nameNode), '{}Bffr_Bsn.{}_Geo{}'.format(nameGeo, nameGeo, y))

    ## wrap group
    still = ['fclBffrGeo_Grp', 'lctRig_md:LctRigStill_Grp', 'dtlLipsRig_md:DtlLipsJnt_Grp', 'dtlLipsRig_md:DtlLipsStill_Grp', 'dtlLipsRig_md:DtlLipsGeo_Grp', 'modelBsh_md:BshRigStill_Grp', 'modelBsh_md:BshRigCtrl_Grp']
    ctrl = ['lctRig_md:LctRigCtrl_Grp', 'dtlLipsRig_md:DtlLipsCtrl_Grp']
    mc.parent(still, addRigStillGrp)
    mc.parent(ctrl, addRigCtrlGrp)
    mc.parent(addRigStillGrp, 'bodyRig_md:Still_Grp')
    mc.parent(addRigCtrlGrp, 'bodyRig_md:AddCtrl_Grp')
