import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
import core
import re
import rigTools as rt

reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  FK RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------
cn = core.Node()

class Run( object ):

	def __init__( self , name       = '' ,
						 tmpJnt     = '' ,
						 side       = '' ,
						 shape      = 'square' ,
						 jnt_ctrl   = 0,
						 jnt        = 1,
						 gimbal     = 1 ,
						 size       = 1 ,
						 drv        = 0,
						 inv        = 0,
						 color      = 'red',
						 constraint = 0,
						 consType = ['parent','scale'],
						 vis = 1,
				 **kwargs):

		##-- Info
		if side == '' :
			side = '_'
		else :
			side = '_{}_'.format(side)

		# add on group
		self.drv = ''
		self.inv = ''
		self.gmbl = ''
		self.obj = tmpJnt
		self.gimbal = gimbal
		self.consType = consType
		#-- Create Main Group
		#self.ctrlGrp = cn.createNode( 'transform' , '{}Ctrl{}Grp'.format( name , side ))

		#-- Rig process

		#-- Joint
		if jnt:
			self.jnt = cn.joint( '{}{}Jnt'.format( name , side ) , tmpJnt )
			self.obj = self.jnt

		#-- Controls
		self.ctrl = rt.createCtrl( '{}{}Ctrl'.format( name , side ) , shape , color , jnt = jnt_ctrl )
		if jnt_ctrl:
			self.ctrl.attr('radius').hide = True
		
		self.zro = rt.addGrp( self.ctrl.name , 'Zro' )
		self.ofst = rt.addGrp( self.ctrl.name , 'Ofst' )

		if drv:
			self.drv = rt.addGrp(self.ctrl.name, 'Drv')
		if inv:
			self.inv = rt.addGrp(self.ctrl.name, 'Inv')

		self.pars = rt.addGrp( self.ctrl.name , 'Pars')

		if tmpJnt:
			if jnt_ctrl:
				self.zro.snapPoint(tmpJnt)
				self.ctrl.snapJntOrient(tmpJnt)
			else:
				self.zro.snap(tmpJnt)

		self.ctrl.scaleShape( size )

		if gimbal:
			self.gmbl = rt.addGimbal( self.ctrl.name)
			self.gmbl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )
			if jnt_ctrl:
				self.gmbl.attr('ssc').v = 0
				self.gmbl.attr('radius').hide = True

		#-- Adjust Shape Controls
		
		if vis:
			self.ctrl.lockHideAttrs('v')

		if tmpJnt:
			if constraint:
				self.do_constraint()


	def do_constraint(self , * args):
		types = self.consType
		if args:
			types = args
			
		for typ in types:
			if typ =='parent':
				if self.gimbal:
					pm.parentConstraint(self.gmbl,self.obj,mo=1)
				else:
					pm.parentConstraint(self.ctrl,self.obj,mo=1)
			
			elif typ =='scale':
				if self.gimbal:
					pm.scaleConstraint(self.gmbl,self.obj,mo=1)
				else:
					pm.scaleConstraint(self.ctrl,self.obj,mo=1)

			elif typ =='point':
				if self.gimbal:
					pm.pointConstraint(self.gmbl,self.obj,mo=1)
				else:
					pm.pointConstraint(self.ctrl,self.obj,mo=1)

			elif typ =='orient':
				if self.gimbal:
					pm.orientConstraint(self.gmbl,self.obj,mo=1)
				else:
					pm.orientConstraint(self.ctrl,self.obj,mo=1)
