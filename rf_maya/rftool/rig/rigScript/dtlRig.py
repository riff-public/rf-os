import maya.cmds as mc
import os,sys
import core
import rigTools as rt
from rf_utils import file_utils
reload(core)
reload(rt)
reload(file_utils)

cn = core.Node()

class Run(object):
    def __init__(self, nrbLs=[], elem="DtlRig", size=1, *args):
        if nrbLs == []:
            nrbLs = mc.ls(sl=True)

        get_nrb = []
        for i in nrbLs:
            shape = mc.listRelatives(i, s=1, typ="nurbsSurface")
            if shape:
                get_nrb.append(shape[0])

        ## create group
        stillGrp = "DtlRigStill_Grp"
        nrbGrp = "DtlRigNrb_Grp"
        skinGrp = "DtlRigSkin_Grp"
        ctrlGrp = "DtlRigCtrl_Grp"

        ctrlHiGrp = "DtlRigHiCtrl_Grp"
        ctrlMidGrp = "DtlRigMidCtrl_Grp"
        ctrlLowGrp = "DtlRigLowCtrl_Grp"
        ctrlMidLowGrp = "DtlRigMidLowCtrl_Grp"

        self.stillGrp = rt.checkObjExist(stillGrp , node='transform')
        self.nrbGrp = rt.checkObjExist(nrbGrp , node='transform')
        self.skinGrp = rt.checkObjExist(skinGrp , node='transform')
        self.ctrlGrp = rt.checkObjExist(ctrlGrp , node='transform')
        self.ctrlHiGrp = rt.checkObjExist(ctrlHiGrp , node='transform')
        self.ctrlMidGrp = rt.checkObjExist(ctrlMidGrp , node='transform')
        self.ctrlLowGrp = rt.checkObjExist(ctrlLowGrp , node='transform')
        self.ctrlMidLowGrp = rt.checkObjExist(ctrlMidLowGrp , node='transform')
        

        if not mc.listRelatives(self.ctrlHiGrp , p=True):
            mc.parent(self.ctrlHiGrp,self.ctrlMidGrp,self.ctrlLowGrp,self.ctrlMidLowGrp,self.ctrlGrp)   
        if not mc.listRelatives(self.nrbGrp , p=True):
            mc.parent(self.nrbGrp, self.stillGrp)


        ## set up
        self.ctrl = []
        self.grp = []
        self.ofst = []
        self.jnt = []
        self.nrb = []
        self.posi = []

        for nrb in get_nrb:
            dag = core.Dag(nrb)
            name = dag.getName()
            side = dag.elem["side"]

            ## create surface
            dupNrb = core.Dag(mc.duplicate(nrb, n="{}{}{}Nrb".format(name, elem, side))[0])
            posi = cn.createNode("pointOnSurfaceInfo", "{}{}{}Posi".format(name, elem, side), top=1)

            ## create controller
            ctrl = rt.createCtrl(name="{}{}{}Ctrl".format(name, elem, side), shape="cube" ,col="green", traget=dupNrb)
            shp = core.Dag(ctrl.getShape())
            grp = rt.addGrp(ctrl, "CtrlZro")
            ofst = rt.addGrp(ctrl, "CtrlOfst")
            rev = cn.createNode("multiplyDivide", "{}{}Rev{}Mdv".format(name, elem, side), i2=(-1, -1, -1))

            ## connect attributes
            shp.addAttr("parameterU", 0, 1)
            shp.addAttr("parameterV", 0, 1)
            shp.attr("parameterU").v = 0.5
            shp.attr("parameterV").v = 0.5

            dupNrb.attr("ws[0]") >> posi.attr("is")
            posi.attr("p") >> grp.attr("t")
            shp.attr("parameterU") >> posi.attr("u")
            shp.attr("parameterV") >> posi.attr("v")

            ctrl.attr("t") >> rev.attr("i1")
            rev.attr("o") >> ofst.attr("t")

            ## create surface
            jnt = cn.joint("{}{}{}Jnt".format(name, elem, side), ctrl)
            jntGrp = rt.addGrp(jnt, "JntZro")

            ctrl.attr("t") >> jnt.attr("t")
            ctrl.attr("r") >> jnt.attr("r")
            ctrl.attr("s") >> jnt.attr("s")

            ## clean
            self.ctrl.append(ctrl)
            self.grp.append(grp)
            self.ofst.append(ofst)
            self.jnt.append(jnt)
            self.nrb.append(dupNrb)
            self.posi.append(posi)

            ctrl.lockHideAttrs("v")
            ctrl.scaleShape(size)
            jnt.attr("radi").v = 0.25

            dupNrb.parent(self.nrbGrp)
            jntGrp.parent(self.skinGrp)
            grp.parent(self.ctrlGrp)

        corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        path = '{}/rf_maya/rftool/rig/rigScript/template/listDtlCtrlGrp.yml'.format(corePath)

        dtlGrpName = file_utils.ymlLoader(path)

        for hiGrp in dtlGrpName['HiGrp']:
            if mc.objExists(hiGrp):
                mc.parent(hiGrp,self.ctrlHiGrp)
        for midGrp in dtlGrpName['MidGrp']:
            if mc.objExists(midGrp):
                mc.parent(midGrp,self.ctrlMidGrp)
        for lowGrp in dtlGrpName['LowGrp']:
            if mc.objExists(lowGrp):
                mc.parent(lowGrp,self.ctrlLowGrp)
        for midlowGrp in dtlGrpName['MidLowGrp']:
            if mc.objExists(midlowGrp):
                mc.parent(midlowGrp,self.ctrlMidLowGrp)