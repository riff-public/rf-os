# Maya modules
import maya.cmds as mc
import maya.mel as mm

# Custom modules
import core
import rigTools as rt
import fkRig
reload( core )
reload( rt )
reload( fkRig )

# RubberLineRig class use for closed Curve
# - number of main contrillers are depend on user define
# - number of detail contrillers equal to the 'jntNo' * 'spans'

# RubberBandRig class use for opened Curve
# - number of main contrillers are depend on curve's spans

cn = core.Node()
class RubberLineRig(object):

	def  __init__(
					self ,
					crv = '' ,
					name = '' ,
					elem = '' ,
					side = '' ,
					spans = 5 ,
					jntNo = 3 ,
					axis = 'z',
					parent = '' ,
					ctrlGrp = '' ,
					jntGrp = '' ,
					skinGrp = '' ,
					stillGrp = '',
					size = 1
				):

		if side == "":
			isSide = "_"
		else:
			isSide = "_{}_".format(side.upper())

		col = 'yellow'
		secCol = 'cyan'

		self.crv = core.Dag(crv)
		mc.xform(self.crv , cp=True)

		# Main groups
		self.Ctrl_Grp = cn.createNode("transform", '{}{}AllRblCtrl{}Grp'.format(name, elem, isSide))
		if mc.objExists(parent):
			mc.parentConstraint(parent, self.Ctrl_Grp)
			mc.scaleConstraint(parent, self.Ctrl_Grp)

		if mc.objExists(ctrlGrp):
			self.Ctrl_Grp.parent(ctrlGrp)

		self.Jnt_Grp = cn.createNode("transform", '{}{}RblJnt{}Grp'.format(name, elem, isSide))
		if mc.objExists(jntGrp):
			self.Jnt_Grp.parent(jntGrp)

		self.Skin_Grp = cn.createNode("transform", '{}{}RblSkin{}Grp'.format(name, elem, isSide))
		if mc.objExists(skinGrp):
			self.Skin_Grp.parent(skinGrp)
		if mc.objExists(parent):
			mc.parentConstraint(parent, self.Skin_Grp)
			mc.scaleConstraint(parent, self.Skin_Grp)

		self.Still_Grp = cn.createNode("transform", '{}{}RblStill{}Grp'.format(name, elem, isSide))
		if mc.objExists(stillGrp):
			self.Still_Grp.parent(stillGrp)

		# Creating NURBs surface
		mc.rebuildCurve(self.crv, d=3, s=spans, ch=False)

		crvA = core.Dag(mc.offsetCurve(self.crv, d=0.05, cb=0, ugn=False, ch=False)[0])
		crvB = core.Dag(mc.offsetCurve(self.crv, d=-0.05, cb=0, ugn=False, ch=False)[0])

		self.Rbl_Nrb = core.Dag(mc.loft(crvB, crvA, n='{}{}Rbl{}Nrb'.format(name, elem, isSide), ch=False)[0])
		mc.rebuildSurface(self.Rbl_Nrb, su=1, du=3, sv=spans, dv=3, ch=False)
 		self.Rbl_Nrb.parent(self.Still_Grp)

		# Main controllers
		Rbl_Ctrl = fkRig.Run(name       = '{}{}Rbl'.format(name, elem) ,
							 side       = side ,
							 tmpJnt     = self.crv ,
							 parent     = '' ,
							 ctrlGrp    = self.Ctrl_Grp , 
							 shape      = 'square' ,
							 size       = size*2 ,
							 color      = col,
							 localWorld = 0,
							 constraint = 0, 
							 gimbal = 0)

		self.Rbl_Ctrl = Rbl_Ctrl.ctrl
		self.RblCtrlZro_Grp = Rbl_Ctrl.zro

		self.ctrls = []
		self.ofstGrps = []
		self.jnts = []

		for ix in range(spans):

			ctrls = fkRig.Run(name       = '{}{}Rbl{}'.format(name, elem, ix+1) ,
							 side       = side ,
							 tmpJnt     = self.crv ,
							 parent     = '' ,
							 ctrlGrp    = self.Rbl_Ctrl , 
							 shape      = 'sphere' ,
							 size       = size ,
							 color      = col,
							 localWorld = 0,
							 jnt = 1,
							 gimbal = 0)

			jnt = ctrls.jnt
			ctrl = ctrls.ctrl
			ctrlUpGrp = ctrls.ctrlGrp
			ctrlZroGrp = ctrls.zro
			ctrlOfstGrp = ctrls.ofst
			
			self.jnts.append(jnt)
			self.ctrls.append(ctrl)
			self.ofstGrps.append(ctrlOfstGrp)
			jnt.parent(self.Jnt_Grp)

			# Positioning
			posi = cn.createNode('pointOnSurfaceInfo', 'posi_1' ,top=1, u=0.5 , v=float(ix)/(spans-1))

			self.Rbl_Nrb.attr('ws[0]') >> posi.attr('is')
			mc.xform(ctrlUpGrp , ws = True , t = posi.attr('p').value)

			# Orientation
			if axis == 'x':
				aimCon = cn.createNode('aimConstraint', 'aimConstraint1', a=(1, 0, 0), u=(0, 1, 0), t=(0, 0, 0), r=(0, 0, 0))
			elif axis == 'y':
				aimCon = cn.createNode('aimConstraint', 'aimConstraint1', a=(0, 1, 0), u=(0, 1, 0), t=(0, 0, 0), r=(0, 0, 0))
			elif axis == 'z':
				aimCon = cn.createNode('aimConstraint', 'aimConstraint1', a=(0, 0, 1), u=(0, 1, 0), t=(0, 0, 0), r=(0, 0, 0))
			#aimCon = cn.createNode('aimConstraint', 'aimConstraint1', a=(0, 0, 1), u=(0, 1, 0), t=(0, 0, 0), r=(0, 0, 0))
			
			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tv') >> aimCon.attr('tg[0].tt')

			ctrlUpGrp.attr('r').value = aimCon.attr('cr').value

			mc.delete(posi)

		# Binding skin
		skc = mc.skinCluster(self.jnts, self.Rbl_Nrb, mi=2)[0]
		for ix in range(spans - 1):
			currIdx = ix + 1
			if ix == 0:
				currIdx = '0:1'
			elif ix == len(self.jnts) - 1:
				currIdx = '{}:{}'.format(spans+1, spans+2)
			
			currCvs = '{}.cv[0:3][{}]'.format(self.Rbl_Nrb, currIdx)
			mc.skinPercent(skc, currCvs, tv=[self.jnts[ix], 1])
		
		mc.skinPercent(skc, '{}.cv[0:3][1]'.format(self.Rbl_Nrb), tv=[self.jnts[1], 0.3])
		mc.skinPercent(skc, '{}.cv[0:3][{}]'.format(self.Rbl_Nrb, spans+1), tv=[self.jnts[spans-1], 0.7])
		
		# Generating POSIs
		self.DtlCtrl_Grp = cn.createNode("transform", '{}{}RblDtlCtrl{}Grp'.format(name, elem, isSide))
		self.DtlCtrl_Grp.parent(self.Rbl_Ctrl)

		self.skinJnts = []
		self.dtlCtrls = []

		len_allJnt = (spans * jntNo) + (spans - jntNo)
		for ix in range(len_allJnt):
			# Point on surface
			pos = cn.createNode("transform", '{}{}RblPos{}{}Grp'.format(name, elem, ix+1, isSide))
			pos.parent(self.Still_Grp)

			div = float(jntNo)
			mult = float(ix)
			posi = cn.createNode("pointOnSurfaceInfo", '{}{}Rbl{}{}Poci'.format(name, elem, ix+1, isSide), top=1, u=0.5, v=float(ix) / (len_allJnt - 1) )
			posi.addAttr(attrName="offset")
			
			self.Rbl_Nrb.attr('ws[0]') >> posi.attr('is')
			posi.attr('p') >> pos.attr('t')

			aimCon = cn.createNode('aimConstraint', '{}{}{}{}Grp_aimConstraint1'.format(name, elem, ix+1, isSide), a=(0, 1, 0), u=(1, 0, 0), t=(0, 0, 0), r=(0, 0, 0))
			aimCon.parent(pos)
			
			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tv') >> aimCon.attr('tg[0].tt')
			aimCon.attr('cr') >> pos.attr('r')			

			# Detail controller
			ctrls = fkRig.Run(name       = '{}{}RblDtl{}'.format(name, elem, ix+1) ,
							 side       = side ,
							 tmpJnt     = pos ,
							 parent     = '' ,
							 ctrlGrp    = self.DtlCtrl_Grp , 
							 shape      = 'cube' ,
							 size       = size*0.5 ,
							 color      = secCol,
							 localWorld = 0,
							 jnt = 1,
							 gimbal = 0)

			skinJnt = ctrls.jnt
			ctrl = ctrls.ctrl
			ctrlUpGrp = ctrls.ctrlGrp
			ctrlZroGrp = ctrls.zro
			ctrlOfstGrp = ctrls.ofst

			self.skinJnts.append(skinJnt)
			self.dtlCtrls.append(ctrl)

			skinJnt.parent(self.Skin_Grp)
			mc.parentConstraint(pos, ctrlUpGrp, mo=False)

			# Adding offset along V
			ctrlShp = core.Dag(ctrl.getShape())
			ctrlShp.addAttr(attrName="offsetV")

			ofstUMdv = cn.createNode('multiplyDivide', '{}{}RblCtrlOfstV{}{}Mdv'.format(name, elem, ix+1, isSide), i2x=0.1)
			ctrlShp.attr('offsetV') >> ofstUMdv.attr('i1x')

			ofstUPma = cn.createNode('plusMinusAverage', '{}{}RblCtrlOfstV{}{}Pma'.format(name, elem, ix+1, isSide))
			ofstUPma.addAttr(attrName='default')
			ofstUPma.attr('default').value = posi.attr('parameterV').value
			ofstUPma.attr('default') >> ofstUPma.attr('i1[0]')

			ofstUMdv.attr('ox') >> ofstUPma.attr('i1[1]')
			ofstUPma.attr('output1D') >> posi.attr('parameterV')

		# Cleanup
		mc.delete(self.crv, crvA, crvB)
		mc.select(cl=True)

class RubberBandRig(object):

	def __init__(
					self,
					crv=  '' ,
					name = '' ,
					elem = '' ,
					side = '' ,
					jntNo = 3 ,
					parent = '' ,
					ctrlGrp = '' ,
					jntGrp = '' ,
					skinGrp = '' ,
					stillGrp = '' ,
					size = 1
				):

		if side == "":
			isSide = "_"
		else:
			isSide = "_{}_".format(side.upper())

		col = 'yellow'
		secCol = 'cyan'

		self.crv = core.Dag(crv)
		mc.xform(self.crv , cp=True)
		self.crvShp = core.Dag(self.crv.shape)
		self.crvSpan = self.crvShp.attr('spans').value

		# Main groups
		self.Ctrl_Grp = cn.createNode("transform", '{}{}AllRblCtrl{}Grp'.format(name, elem, isSide))
		if mc.objExists(parent):
			mc.parentConstraint(parent, self.Ctrl_Grp)
			mc.scaleConstraint(parent, self.Ctrl_Grp)

		if mc.objExists(ctrlGrp):
			self.Ctrl_Grp.parent(ctrlGrp)

		self.Jnt_Grp = cn.createNode("transform", '{}{}RblJnt{}Grp'.format(name, elem, isSide))
		if mc.objExists(jntGrp):
			self.Jnt_Grp.parent(jntGrp)

		self.Skin_Grp = cn.createNode("transform", '{}{}RblSkin{}Grp'.format(name, elem, isSide))
		if mc.objExists(skinGrp):
			self.Skin_Grp.parent(skinGrp)
		if mc.objExists(parent):
			mc.parentConstraint(parent, self.Skin_Grp)
			mc.scaleConstraint(parent, self.Skin_Grp)

		self.Still_Grp = cn.createNode("transform", '{}{}RblStill{}Grp'.format(name, elem, isSide))
		if mc.objExists(stillGrp):
			self.Still_Grp.parent(stillGrp)

		# Creating NURBs surface
		crvA = core.Dag(mc.duplicate(self.crv, rr=True)[0])
		crvA.scaleShape(0.95)

		crvB = core.Dag(mc.duplicate(self.crv, rr=True)[0])
		crvB.scaleShape(1.05)

		self.Rbb_Nrb = core.Dag(mc.loft(crvA, crvB, ch=False)[0])
		mc.rebuildSurface(self.Rbb_Nrb, n='{}{}Rbb{}Nrb'.format(name, elem, isSide), su=1, du=3, sv=self.crvSpan, dv=3, ch=False)
		self.Rbb_Nrb.parent(self.Still_Grp)

		# Main controllers
		Rbb_Ctrl = fkRig.Run(name       = '{}{}Rbb'.format(name, elem) ,
							 side       = side ,
							 tmpJnt     = self.crv ,
							 parent     = '' ,
							 ctrlGrp    = self.Ctrl_Grp , 
							 shape      = 'square' ,
							 size       = size*2 ,
							 color      = col,
							 localWorld = 0,
							 constraint = 0, 
							 gimbal = 0)

		self.Rbb_Ctrl = Rbb_Ctrl.ctrl
		self.RblCtrlZro_Grp = Rbb_Ctrl.zro

		self.ctrls = []
		self.ofstGrps = []
		self.jnts = []

		for ix in range(self.crvSpan):

			ctrls = fkRig.Run(name       = '{}{}Rbb{}'.format(name, elem, ix+1) ,
							 side       = side ,
							 tmpJnt     = self.crv ,
							 parent     = '' ,
							 ctrlGrp    = self.Rbb_Ctrl , 
							 shape      = 'sphere' ,
							 size       = size ,
							 color      = col,
							 localWorld = 0,
							 jnt = 1, 
							 gimbal = 0)

			jnt = ctrls.jnt
			ctrl = ctrls.ctrl
			ctrlUpGrp = ctrls.ctrlGrp
			ctrlZroGrp = ctrls.zro
			ctrlOfstGrp = ctrls.ofst

			self.jnts.append(jnt)	
			self.ctrls.append(ctrl)
			self.ofstGrps.append(ctrlOfstGrp)
			jnt.parent(self.Jnt_Grp)
			
			# Positioning
			posi = cn.createNode('pointOnSurfaceInfo', 'Posi_1', u=0.5, v=(self.crvSpan - 1) + ix)

			self.Rbb_Nrb.attr('ws[0]') >> posi.attr('is')
			mc.xform(ctrlUpGrp, t=posi.attr('position').value, ws=True)

			# Orientation
			aimCon = cn.createNode('aimConstraint', 'aimConstraint1', a=(0, 0, 1), u=(0, 1, 0), t=(0, 0, 0), r=(0, 0, 0))
			
			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tv') >> aimCon.attr('tg[0].tt')

			ctrlUpGrp.attr('r').value = aimCon.attr('cr').value

			mc.delete(posi)

		# Binding skin
		skc = mc.skinCluster(self.jnts, self.Rbb_Nrb)[0]
		for ix, jnt in enumerate(self.jnts):
			currCvs = '%s.cv[0:3][%s]' % (self.Rbb_Nrb, ix)
			mc.skinPercent(skc, currCvs, tv=[(jnt, 1)])
		
		# Generating POSIs
		self.DtlCtrl_Grp = cn.createNode('transform', '{}{}RbbDtlCtrl{}Grp'.format(name, elem, isSide))
		self.DtlCtrl_Grp.parent(self.Rbb_Ctrl)

		self.skinJnts = []
		self.dtlCtrls = []

		len_allJnt = (jntNo * self.crvSpan) + self.crvSpan
		for ix in range(len_allJnt):
			
			# Point on surface
			pos = cn.createNode('transform', '{}{}RbbPos{}{}Grp'.format(name, elem, ix+1, isSide))
			pos.parent(self.Still_Grp)

			# div = float(jntNo)
			# mult = float(ix)
			posi = cn.createNode('pointOnSurfaceInfo', '{}{}Rbb{}{}Posi'.format(name, elem, ix+1, isSide), u=0.5, v=float(ix)/len_allJnt, top=1)
			posi.addAttr(attrName='offset')
			
			self.Rbb_Nrb.attr('ws[0]') >> posi.attr('is')
			posi.attr('p') >> pos.attr('t')

			aimCon = cn.createNode('aimConstraint', '{}{}Rbb{}{}Grp_aimConstraint1'.format(name, elem, ix+1, isSide), a=(0, 1, 0), u=(1, 0, 0), t=(0, 0, 0), r=(0, 0, 0))
			aimCon.parent(pos)
			
			posi.attr('n') >> aimCon.attr('wu')
			posi.attr('tv') >> aimCon.attr('tg[0].tt')
			aimCon.attr('cr') >> pos.attr('r')			
	
			# Detail controller
			ctrls = fkRig.Run(name       = '{}{}RbbDtl{}'.format(name, elem, ix+1) ,
							 side       = side ,
							 tmpJnt     = pos ,
							 parent     = '' ,
							 ctrlGrp    = self.DtlCtrl_Grp , 
							 shape      = 'cube' ,
							 size       = size*0.5 ,
							 color      = secCol,
							 localWorld = 0,
							 jnt = 1,
							 gimbal = 0)

			skinJnt = ctrls.jnt
			ctrl = ctrls.ctrl
			ctrlUpGrp = ctrls.ctrlGrp
			ctrlZroGrp = ctrls.zro
			ctrlOfstGrp = ctrls.ofst

			self.skinJnts.append(skinJnt)
			self.dtlCtrls.append(ctrl)

			skinJnt.parent(self.Skin_Grp)
			mc.parentConstraint(pos, ctrlUpGrp, mo=False)
		
			# Adding offset along V
			ctrlShp = core.Dag(ctrl.shape)
			ctrlShp.addAttr(attrName='offsetV')

			ofstUMdv = cn.createNode('multiplyDivide', '{}{}RbbCtrlOfstV{}{}Mdv'.format(name, elem, ix+1, side), i2x=0.1)
			ctrlShp.attr('offsetV') >> ofstUMdv.attr('i1x')

			ofstUPma = cn.createNode('plusMinusAverage', '{}{}RbbCtrlOfstV{}{}Pma'.format(name, elem, ix+1, side))

			ofstUPma.addAttr(attrName='default')
			ofstUPma.attr('default').value = posi.attr('v').value
			ofstUPma.attr('default') >> ofstUPma.attr('i1[0]')

			ofstUMdv.attr('ox') >> ofstUPma.attr('i1[1]')
			ofstUPma.attr('o1') >> posi.attr('v')

		# Cleanup
		mc.delete(self.crv.name, crvA, crvB)
		mc.select(cl=True)