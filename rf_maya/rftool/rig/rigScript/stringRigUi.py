import maya.cmds as mc
from functools import partial
    
import stringRig
reload( stringRig )

class Run():
    def __init__(self):
        self.winName = "string_app_v001"
        self.titleName = "String Tools v.0.0.2"
        self.mainAttr = ["translateX", "translateY", "translateZ", "rotateX", "rotateY", "rotateZ", "scaleX", "scaleY", "scaleZ"]
        self.defaultCV = [0, 50, 100, 78, 48, 25, 5, 0, 0]

    def show(self):
        if mc.window(self.winName, ex=True):
            mc.deleteUI(self.winName)

        mc.window(self.winName, t=self.titleName, wh=(500, 500), mnb=True, mxb=False, s=False, bgc=[0, 0, 0])
        self.main_layout = mc.columnLayout("main_layout", cal="center", cat=["both", 10])

        mc.separator(st="none", h=10, p=self.main_layout)
        self.variebleLayout()
        self.editComponentWeight()
        self.commandSignal()

        mc.showWindow(self.winName)

    def variebleLayout(self):
        self.var_layout = mc.columnLayout("var_layout", cal="left", cat=["both", 10], w=480, bgc=[0.25, 0.25, 0.25], p=self.main_layout)

        mc.separator(st="none", h=10, p=self.var_layout)
        mc.text(l="String Options <<" + "-"*100, w=460, p=self.var_layout)
        mc.separator(st="none", h=10, p=self.var_layout)

        self.nameSide_layout = mc.rowLayout("nameSide_layout", nc=10, w=460, p=self.var_layout)
        mc.separator(st="none", h=10, p=self.var_layout)
        self.addJnt_layout = mc.rowLayout("addJnt_layout", nc=10, w=460, p=self.var_layout)
        mc.separator(st="none", h=10, p=self.var_layout)
        self.spanShape_layout = mc.rowLayout("spanShape_layout", nc=10, w=460, p=self.var_layout)

        mc.separator(st="none", h=10, p=self.var_layout)
        mc.text(l="Noise Options <<" + "-"*100, w=460, p=self.var_layout)
        mc.separator(st="none", h=10, p=self.var_layout)

        self.noiseAttr_layout = mc.rowLayout("noiseAttr_layout", nc=10, w=460, p=self.var_layout)
        mc.separator(st="none", h=10, p=self.var_layout)
        self.newCtrl_layout = mc.rowLayout("newCtrl_layout", nc=10, w=460, p=self.var_layout)
        mc.separator(st="in", w=460, h=10, p=self.var_layout)

        #########################################################################################################################################
        #########################################################################################################################################
        #########################################################################################################################################

        ## name and side
        mc.text(l="Name :                ", w=81, p=self.nameSide_layout)
        self.name_txtFld = mc.textField(tx="Test", w=200, p=self.nameSide_layout)
        mc.separator(st="none", w=20, p=self.nameSide_layout)
        mc.text(l="Side :      ", w=50, p=self.nameSide_layout)

        self.side_optMenu = mc.optionMenu("side_optMenu", w=100, bgc=[0.4, 0.4, 0.4], p=self.nameSide_layout)
        mc.menuItem(l='', p=self.side_optMenu)
        mc.menuItem(l='L', p=self.side_optMenu)
        mc.menuItem(l='R', p=self.side_optMenu)

        ## add joint
        mc.text(l="Joint :                ", w=81, p=self.addJnt_layout)
        self.joint_txtFld = mc.textField(tx="joint1,joint2,joint3,joint4", w=252, p=self.addJnt_layout)
        mc.separator(st="none", w=20, p=self.addJnt_layout)
        self.joint_butt = mc.button(l="<<", w=100, bgc=[0.4, 0.4, 0.4], p=self.addJnt_layout)

        ## span and shape
        mc.text(l="Spans :                ", w=83, p=self.spanShape_layout)
        self.spans_txtFld = mc.intField(v=100, w=200, p=self.spanShape_layout)
        mc.separator(st="none", w=9, p=self.spanShape_layout)
        mc.text(l="Shape :      ", w=60, p=self.spanShape_layout)

        self.shape_optMenu = mc.optionMenu("shape_optMenu", w=100, bgc=[0.4, 0.4, 0.4], p=self.spanShape_layout)
        mc.menuItem(l='cube', p=self.shape_optMenu)
        mc.menuItem(l='sphere', p=self.shape_optMenu)

        ## add noise attributes
        mc.text(l="Nosie Attr :        ", w=81, p=self.noiseAttr_layout)
        self.noiseAttr_cbx = mc.checkBox(l="Frequency and Value", v=True, w=210, p=self.noiseAttr_layout)
        mc.text(l="Axis :", w=60, p=self.noiseAttr_layout)

        self.noiAx_optMenu = mc.optionMenu("noiAx_optMenu", w=100, bgc=[0.4, 0.4, 0.4], p=self.noiseAttr_layout)
        for ax in self.mainAttr:
            mc.menuItem(l=ax, p=self.noiAx_optMenu)

        ## add new controller
        mc.text(l="New Ctrl :        ", w=81, p=self.newCtrl_layout)
        self.newCtrl_cbx = mc.checkBox(l="New Noise Ctrl", v=True, w=210, p=self.newCtrl_layout)
        mc.text(l="Axis :", w=60, p=self.newCtrl_layout)

        self.newAx_optMenu = mc.optionMenu("newAx_optMenu", w=100, bgc=[0.4, 0.4, 0.4], p=self.newCtrl_layout)
        for ax in self.mainAttr:
            mc.menuItem(l=ax, p=self.newAx_optMenu)

    def editComponentWeight(self):
        self.com_layout = mc.columnLayout("com_layout", cal="left", cat=["both", 10], w=480, bgc=[0.25, 0.25, 0.25], p=self.main_layout)
        
        mc.text(l="New Ctrl Component Options (.CV)", w=460, p=self.com_layout)
        mc.separator(st="none", h=10, p=self.com_layout)

        self.comVal_layout = mc.rowLayout("comVal_layout", nc=10, w=460, p=self.com_layout)
        for i in range(9):
            exec( 'self.com{0}_floFld = mc.floatField("com{0}_floFld", pre=2, v={1}, w=49, p=self.comVal_layout)'.format(i+1, self.defaultCV[i]) )

        mc.separator(st="none", h=5, p=self.com_layout)

        self.comSlider_layout = mc.rowLayout("comSlider_layout", nc=10, w=460, p=self.com_layout)
        for i in range(9):
            exec( 'self.com{0}_slider = mc.floatSlider("com{0}_slider", hr=False, s=1, w=49, v={1}, min=0, max=100, p=self.comSlider_layout)'.format(i+1, self.defaultCV[i]) )

        mc.separator(st="none", h=5, p=self.com_layout)

        self.comTxt_layout = mc.rowLayout("comTxt_layout", nc=10, w=460, p=self.com_layout)
        for i in range(9):
            exec( 'mc.text(l={}, w=49, p=self.comTxt_layout)'.format(i+1) )

        mc.separator(st="none", h=7, p=self.com_layout)
        self.refresh_butt = mc.button(l="Refresh Sliders", w=460, bgc=[0.4, 0.4, 0.4], p=self.com_layout)
        mc.separator(st="none", h=10, p=self.com_layout)
        self.generate_butt = mc.button(l="Generate Strings", w=460, h=50, bgc=[0.4, 0.4, 0.4], p=self.com_layout)
        mc.separator(st="none", h=10, p=self.com_layout)

    #########################################################################################################################################
    #########################################################################################################################################
    #########################################################################################################################################

    def commandSignal(self):
        for i in range(9):
            exec('mc.floatSlider(self.com{0}_slider, e=True, dc=partial(self.sliderChange, self.com{0}_slider, self.com{0}_floFld))'.format(i+1))

        for i in range(9):
            exec('mc.floatField(self.com{0}_floFld, e=True, cc=partial(self.fieldChange, self.com{0}_slider, self.com{0}_floFld))'.format(i+1))

        mc.button(self.refresh_butt, e=True, c=self.refreshButton)
        mc.button(self.generate_butt, e=True, c=self.generateStringRig)

    def sliderChange(self, sliderName, fieldName, *args):
        value = mc.floatSlider(sliderName, q=True, v=True)
        if value:
            mc.floatField(fieldName, e=True, v=value)

        elif value == 0:
            mc.floatField(fieldName, e=True, v=0)

    def fieldChange(self, sliderName, fieldName, *args):
        value = mc.floatField(fieldName, q=True, v=True)
        if value:
            mc.floatSlider(sliderName, e=True, v=value)

        elif value == 0:
            mc.floatSlider(sliderName, e=True, v=0)

    def refreshButton(self, *args):
        for i in range(9):
            exec( 'mc.floatField(self.com{}_floFld, e=True, v={})'.format(i+1, self.defaultCV[i]) )

        for i in range(9):
            exec( 'mc.floatSlider(self.com{}_slider, e=True, v={})'.format(i+1, self.defaultCV[i]) )

    def generateStringRig(self, *args):
        name = mc.textField(self.name_txtFld, q=True, tx=True)
        side = mc.optionMenu(self.side_optMenu, q=True, v=True)
        joint = mc.textField(self.joint_txtFld, q=True, tx=True)
        spans =  mc.intField(self.spans_txtFld, q=True, v=True)
        shape = mc.optionMenu(self.shape_optMenu, q=True, v=True)
        noiseAttr = mc.checkBox(self.noiseAttr_cbx, q=True, v=True)
        noiAxis = mc.optionMenu(self.noiAx_optMenu, q=True, v=True)
        newCtrl = mc.checkBox(self.newCtrl_cbx, q=True, v=True)
        newAxis = mc.optionMenu(self.newAx_optMenu, q=True, v=True)

        value = [mc.floatField(self.com1_floFld, q=True, v=True)]
        value += [mc.floatField(self.com2_floFld, q=True, v=True)]
        value += [mc.floatField(self.com3_floFld, q=True, v=True)]
        value += [mc.floatField(self.com4_floFld, q=True, v=True)]
        value += [mc.floatField(self.com5_floFld, q=True, v=True)]
        value += [mc.floatField(self.com6_floFld, q=True, v=True)]
        value += [mc.floatField(self.com7_floFld, q=True, v=True)]
        value += [mc.floatField(self.com8_floFld, q=True, v=True)]
        value += [mc.floatField(self.com9_floFld, q=True, v=True)]

        joint = joint.split(",")
        geo = mc.ls(sl=True)[0]
        if not geo:
            geo = ''

        stringRig.Run(  name                = name , 
                            jnt                 = joint , 
                            geo                 = geo ,
                            side                = side , 
                            color               = 'red' , 
                            spans               = spans , 
                            shape               = shape ,
                            isNoiseAttr         = noiseAttr ,
                            noiseAxis           = noiAxis ,
                            newNoiseCtrl        = newCtrl ,
                            newNoiseAxis        = 'tx',
                            valueCv             = value ,
                            size                = 1
                        )