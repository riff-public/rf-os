
import maya.cmds as mc

def bakeAnimToBsh(obj = '', time = (30,40)):
    mc.refresh(suspend=1)
    mc.currentTime(time[0])
    
    dupObj = mc.duplicate(obj, name = '{}_Baked'.format(obj), rr = True, rc = True)[0]
    lenght = 1.0 / len(range(time[0],time[1]))
    
    # unparent to world
    parObj = mc.listRelatives(dupObj, p = True)
    if parObj:
        mc.parent(dupObj, w = True)
        
    # rename child object
    childObj = mc.listRelatives(dupObj, f = True, ad = True)
    if childObj:
        for chd in childObj:
            if mc.nodeType(chd) == 'transform':
                chdShort = mc.ls(chd, sn = True)[0]
                mc.rename(chd, '{}_Baked'.format(chdShort))
                
    frameRange = range(time[0]+1,time[1]+1)
    for idx , t in enumerate(frameRange[::-1]):
        print t
        mc.currentTime(t)
        tObj = mc.duplicate(obj, name = '{}_{}'.format(obj,t))[0]
        
        if t == time[1]:
            mc.blendShape(tObj, dupObj, frontOfChain = True, origin = 'local', n = '{}_Bsh'.format(obj))
        else:
            weight = round(1-(lenght*idx), 3)
            mc.blendShape('{}_Bsh'.format(obj), edit = True, ib = True, t = (dupObj, 0, tObj, weight))

        mc.delete(tObj)
    mc.refresh(suspend=0)