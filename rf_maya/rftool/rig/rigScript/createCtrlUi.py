import maya.cmds as mc
import maya.mel as mm
import os
from functools import partial
import core
reload( core )
import rigTools as rt
reload( rt )
import ctrlRig
reload(ctrlRig)

cn = core.Node()

class Run(object):
    def __init__(self):
        self.winName = 'createCtrlUi'

    def show(self , *args):
        # check if window exists
        if mc.window ( self.winName , exists = True ) :
            mc.deleteUI ( self.winName ) 
        else : pass

        winWidth = 300
        mc.window(self.winName,title = 'Create Control UI' , width = winWidth , s=False, mnb=True, mxb=False)
        mc.columnLayout()
        mc.radioButtonGrp('pivot',l=' Pivot Position',la2=['Object','Center'],numberOfRadioButtons=2,cw3=[winWidth*0.35, winWidth*0.325,winWidth*0.325],cl3=('left','left','left'))
        mc.setParent('..')
        mc.radioButtonGrp('connectionType',l=' Connection Type',la2=['Constraint','Connection'],numberOfRadioButtons=2,cw3=[winWidth*0.35, winWidth*0.325,winWidth*0.325],cl3=('left','left','left'))
        mc.setParent('..')
        mc.rowLayout(numberOfColumns=2 , columnWidth2=[winWidth*0.35,winWidth*0.65])
        mc.text(l=' Geometry Visibility')
        mc.checkBox('geoVis',l='Geo Vis',align='left', value= True)
        mc.setParent('..')
        mc.rowLayout(numberOfColumns=2 , columnWidth2=[winWidth*0.35,winWidth*0.65])
        mc.text(l=' Connect FK Chain')
        mc.checkBox('fkCahin',l='FK Chain',align='left', value= False)
        mc.setParent('..')
        mc.rowColumnLayout( nc = 1  )
        mc.text(label = '-------------------------------------------------------------------------',align='center',width = winWidth)
        mc.setParent('..')
        mc.text(l='Control Shape' , align='center' ,width = winWidth)
        mc.separator(h = 3, st = 'none')
        mc.setParent('..')

        # png column 1
        mc.rowColumnLayout(nc = 17)
        mc.separator(w = 4, st = 'none')
        mc.symbolButton('circle1' , image=("{}{}".format(self.imagePath(),"Sphere-01.png")) , c=partial(self.rig, 'sphere'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle2' , image=("{}{}".format(self.imagePath(),"Cylinder-01.png")) , c=partial(self.rig, 'cylinder'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle3' , image=("{}{}".format(self.imagePath(),"Stick-01.png")) , c=partial(self.rig, 'stick'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle4' , image=("{}{}".format(self.imagePath(),"SquareStick-01.png")) , c=partial(self.rig, 'stickSquare'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle5' , image=("{}{}".format(self.imagePath(),"CircleStick-01.png")) , c=partial(self.rig, 'stickCircle'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle6' , image=("{}{}".format(self.imagePath(),"Circle-01.png")) , c=partial(self.rig, 'circle'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle7' , image=("{}{}".format(self.imagePath(),"ArrowBall-01.png")) , c=partial(self.rig, 'arrowBall'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle8' , image=("{}{}".format(self.imagePath(),"ArrowCircle-01.png")) , c=partial(self.rig, 'arrowCircle'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 4, st = 'none')
        mc.setParent('..')

        mc.separator(h = 2, st = 'none')

        # png column 2
        mc.rowColumnLayout(nc = 17)
        mc.separator(w = 4, st = 'none')
        mc.symbolButton('circle9' , image=("{}{}".format(self.imagePath(),"Pyramid-01.png")) , c=partial(self.rig, 'pyramid'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle10' , image=("{}{}".format(self.imagePath(),"Capsule-01.png")) , c=partial(self.rig, 'capsule'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle11' , image=("{}{}".format(self.imagePath(),"ArrowCross-01.png")) , c=partial(self.rig, 'arrowCross'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle12' , image=("{}{}".format(self.imagePath(),"Triangle-01.png")) , c=partial(self.rig, 'triangle'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle13' , image=("{}{}".format(self.imagePath(),"Drop-01.png")) , c=partial(self.rig, 'drop'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle14' , image=("{}{}".format(self.imagePath(),"Plus-01.png")) , c=partial(self.rig, 'plus'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle15' , image=("{}{}".format(self.imagePath(),"Diamond-01.png")) , c=partial(self.rig, 'diamond'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle16' , image=("{}{}".format(self.imagePath(),"Square-01.png")) , c=partial(self.rig, 'square'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 4, st = 'none')
        mc.setParent('..')

        mc.separator(h = 2, st = 'none')

        # png column 3
        mc.rowColumnLayout(nc = 18)
        mc.separator(w = 4, st = 'none')
        mc.symbolButton('circle17' , image=("{}{}".format(self.imagePath(),"Cube-01.png")) , c=partial(self.rig, 'cube'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle18' , image=("{}{}".format(self.imagePath(),"Arrow-01.png")) , c=partial(self.rig, 'arrow'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle19' , image=("{}{}".format(self.imagePath(),"Locator-01.png")) , c=partial(self.rig, 'locator'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle20' , image=("{}{}".format(self.imagePath(),"Null-01.png")) , c=partial(self.rig, 'null'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle21' , image=("{}{}".format(self.imagePath(),"Line-01.png")) , c=partial(self.rig, 'line'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle22' , image=("{}{}".format(self.imagePath(),"Dumbbell-01.png")) , c=partial(self.rig, 'dumbbell'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle23' , image=("{}{}".format(self.imagePath(),"RotateCircle-01.png")) , c=partial(self.rig, 'rotateCircle'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle24' , image=("{}{}".format(self.imagePath(),"ArcCapsule-01.png")) , c=partial(self.rig, 'arcCapsule'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 4, st = 'none')
        mc.setParent('..')

        mc.separator(h = 2, st = 'none')

        # png column 4
        mc.rowColumnLayout(nc = 17)
        mc.separator(w = 4, st = 'none')
        mc.symbolButton('circle25' , image=("{}{}".format(self.imagePath(),"F-01.png")) , c=partial(self.rig, 'F'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle26' , image=("{}{}".format(self.imagePath(),"B-01.png")) , c=partial(self.rig, 'B'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle27' , image=("{}{}".format(self.imagePath(),"L-01.png")) , c=partial(self.rig, 'L'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle28' , image=("{}{}".format(self.imagePath(),"R-01.png")) , c=partial(self.rig, 'R'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle29' , image=("{}{}".format(self.imagePath(),"NrbCircle-01.png")) , c=partial(self.rig, 'nrbCircle'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('circle30' , image=("{}{}".format(self.imagePath(),"Gear-01.png")) , c=partial(self.rig, 'gear'), bgc = (0.2, 0.2, 0.2))
        mc.separator(w = 4, st = 'none')
        mc.setParent('..')

        mc.rowColumnLayout( nc = 1  )
        mc.text(label = '-------------------------------------------------------------------------',align='center',width = winWidth)
        mc.setParent('..')
        mc.text(l='Color' , align='center' ,width = winWidth)
        mc.separator(h = 3, st = 'none')
        mc.setParent('..')

        # color column1
        mc.rowColumnLayout(nc = 19)
        mc.separator(w = 3, st = 'none')
        mc.button(l='' , c=partial(self.color, 'white'), width = 31, bgc = (0.99, 0.99, 0.99) )
        mc.separator(w = 2, st = 'none')
        mc.button(l = '' , c=partial(self.color, 'magenta'), width = 31, bgc = (0.79, 0, 0.78) )        
        mc.separator(w = 2, st = 'none')
        mc.button(l='' , c=partial(self.color, 'red'), width = 31, bgc = (0.99, 0, 0) )
        mc.separator(w = 2, st = 'none')
        mc.button(l='' , c=partial(self.color, 'yellow'), width = 31, bgc = (.99, .99, 0) )
        mc.separator(w = 2, st = 'none')
        mc.button(l='' , c=partial(self.color, 'green'), width = 31, bgc = (0, 0.99, 0) )
        mc.separator(w = 2, st = 'none')
        mc.button(l='' , c=partial(self.color, 'softGreen'), width = 31, bgc = (0.26, 0.99, 0.64) )
        mc.separator(w = 2, st = 'none')
        mc.button(l='' , c=partial(self.color, 'cyan'), width = 31, bgc = (0.39, 0.78 , 0.88) )
        mc.separator(w = 2, st = 'none')
        mc.button(l = '' , c=partial(self.color, 'blue'), width = 31, bgc = (0, 0, 0.99) )
        mc.separator(w = 2, st = 'none')
        mc.button(l = '' , c=partial(self.color, 'darkBlue'), width = 31, bgc = (0, 0.01 , 0.38) )
        mc.separator(w = 3, st = 'none')
        mc.setParent('..')

        # color column2
        mc.rowColumnLayout(nc = 19)
        mc.separator(w = 3, st = 'none')
        mc.button(l = '' , c=partial(self.color, 'darkGreen'), width = 31, bgc = (0.01, 0.27, 0.1) )
        mc.separator(w = 2, st = 'none')   
        mc.button(l = '' , c=partial(self.color, 'brown'), width = 31, bgc = (0.25, 0.14, 0.12) )     
        mc.separator(w = 2, st = 'none')        
        mc.button(l = '' , c=partial(self.color, 'darkRed'), width = 31, bgc = (0.61, 0.0, 0.16) )
        mc.separator(w = 2, st = 'none')
        mc.button(l='' , c=partial(self.color, 'purple'), width = 31, bgc = (0.44, 0.19, 0.63) )       
        mc.separator(w = 2, st = 'none') 
        mc.button(l='' , c=partial(self.color, 'pink'), width = 31, bgc = (1, 0.69, 0.7) )       
        mc.separator(w = 2, st = 'none')
        mc.button(l = '' , c=partial(self.color, 'softGray'), width = 31, bgc = (0.67, 0.67, 0.67) )        
        mc.separator(w = 2, st = 'none') 
        mc.button(l = '' , c=partial(self.color, 'gray'), width = 31, bgc = (0.33, 0.33, 0.33) )       
        mc.separator(w = 2, st = 'none')
        mc.button(l = '' , c=partial(self.color, 'black'), width = 31, bgc = (0, 0, 0) )        
        mc.separator(w = 2, st = 'none')
        mc.symbolButton('none' , image=("{}{}".format(self.imagePath(),"none.png")) , c=partial(self.color, 'none'))        
        mc.separator(w = 3, st = 'none')
        mc.setParent('..')

        mc.rowColumnLayout( nc = 1  )

        mc.showWindow()


    def rig(self , shape = '' , *args):
        pvt = mc.radioButtonGrp('pivot',q = True, select = True)
        con = mc.radioButtonGrp('connectionType',q = True, select = True)
        geoVis = mc.checkBox('geoVis',q = True, value = True)
        fkCahin = mc.checkBox('fkCahin',q = True, value = True)

        if con == 2:
            gimbal = 0
        else:
            gimbal = 1

        objs = mc.ls(sl=True)

        gmbls = []
        grps = []
        for each in objs:
            obj = core.Dag(each)
            name = obj.elem['name']

            if 'Geo' in name:
                name = name.replace('Geo','')
            side = obj.getSide()

            # create control
            ctrl_obj = ctrlRig.Run(name = name,
                                        tmpJnt = '',
                                        side = side,
                                        shape = shape,
                                        jnt_ctrl = 0 ,
                                        size = 1,
                                        color = 'red',
                                        constraint = 0 ,
                                        jnt = 0 ,
                                        gimbal = gimbal)

            if side == '':
                side = '_'
            else:
                side = '_{}_'.format(side)
            
            ctrlGrp = cn.createNode('transform','{}Ctrl{}Grp'.format( name , side ))

            # define FkRig Attr
            zro = ctrl_obj.zro
            gmbl = ctrl_obj.gmbl
            ctrl = core.Dag(ctrl_obj.ctrl)
            gmbls.append(gmbl)
            grps.append(ctrlGrp)

            # parent zroGrp to ctrlGrp
            zro.parent(ctrlGrp)
            if pvt == 2:
                tslDefault = mc.xform(obj , q=True , piv=True , ws=True)
                tslCenter = mc.xform(obj , cp=True)
                ctrlGrp.snap(obj)
                
                mc.xform(obj , piv=(tslDefault[0],tslDefault[1],tslDefault[2]) , ws=True)

            else:
                ctrlGrp.snap(obj)

            # connect object
            if con == 1:
                mc.parentConstraint(gmbl , obj , mo=1)
                mc.scaleConstraint(gmbl , obj , mo=1)
            elif con == 2:
                rt.connectTRS(ctrl , obj)

            if geoVis:
                ctrl.addAttr(attrName = 'geoVis' , minVal = 0 , maxVal = 1)
                ctrl.attr('geoVis').v = 1
                ctrl.attr('geoVis') >> obj.attr('v')

        if fkCahin:
            for i in range(1,len(grps)):
                mc.parent(grps[i],gmbls[i-1])

    def color(self , color = '' , *args):
        objs = mc.ls(sl=True)
        for obj in objs:
            ctrl = core.Dag(obj)
            if not ctrl.getNodeType() == 'shape':
                ctrl = ctrl.shape
            rt.setColor(ctrl , color)

    def imagePath(self , *args):
        corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        path = '{}/rf_maya/rftool/rig/rigScript/imageIcon/ctrlShapeIcon/'.format(corePath)
        
        return path