import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm 
import os , sys
import pickle
import math
import maya.mel as mm
import rigTools as rt
reload( rt )

from rf_utils.context import context_info

def creatCtrl(*args):
    part        = pm.textField('fillPartName', q = True, text= True)
    size        = pm.floatField('fillsize', q = True, v = True)
    #size        = pm.textField('fillsize', q= True, text = True)
    # parent      = pm.textField('fillParentName', q = True, text= True)

    sel = mc.ls(os=1, fl=1)  
    refName = sel[0].split('.')

    if ':' in refName[0]:
        ns,geoname = refName[0].split(':')
    else:
        ns = ''
        geoname = refName[0]  
    
    fullName = geoname.split('_')
    name = fullName[0]
    if len(fullName) == 2:
        side = '_'
    elif len(fullName) == 3:
        side = '_'+fullName[1]+'_'
     
    pathCrv = mc.polyToCurve( form = 2, degree =3, conformToSmoothMeshPreview = 1, n = '{}{}Ctrl'.format(name,side))[0]
    #mc.xform(pathCrv, cp =True)
    #mc.delete(pathCrv, ch = True)
    #mc.scale(1.035, 1.035, 1.035, pathCrv + ".cp[:]", r=True)
    mc.setAttr(pathCrv+'.overrideEnabled', 1)
    mc.setAttr(pathCrv+'.overrideColor', 9)
    
    ######################getPosi
    edgePosi = mc.xform(sel[0],q=True,t=True,ws = 1)
    
    edgePoint = [(edgePosi[0] + edgePosi[3])/2, (edgePosi[1]+edgePosi[4])/2,(edgePosi[2]+edgePosi[5])/2]    
    cvs = mc.getAttr(pathCrv + '.spans')+mc.getAttr(pathCrv + '.degree')
    startCv = mc.xform(pathCrv + '.cv[{}]'.format(0),q=True,t=True,ws = 1)
    endCv = mc.xform(pathCrv + '.cv[{}]'.format(cvs-1),q=True,t=True,ws = 1)
    
    disStart = math.sqrt(math.pow(edgePoint[0]-startCv[0],2) + math.pow(edgePoint[1]-startCv[1],2) + math.pow(edgePoint[2]-startCv[2],2))
    disEnd = math.sqrt(math.pow(edgePoint[0]-endCv[0],2) + math.pow(edgePoint[1]-endCv[1],2) + math.pow(edgePoint[2]-endCv[2],2))
    
    if disStart > disEnd:
        mc.reverseCurve(pathCrv, ch = 1)
        mc.delete(pathCrv,ch = True)
    else:
        pass
        
    ####################################### 
       
    stillGrp = 'FxRigStill_Grp'
    if not mc.objExists(stillGrp):
        mc.createNode('transform', name = stillGrp)
    
    ctrlGrp = 'FxRigCtrl_Grp'
    if not mc.objExists(ctrlGrp):
        mc.createNode('transform', name = ctrlGrp)

    # jntGrp = 'FxRigJnt_Grp'
    # if not mc.objExists(jntGrp):
    #     mc.createNode('transform', name = jntGrp)
    #     mc.parent(jntGrp,stillGrp)
    
    loc = 'FxRig_Loc'
    if not mc.objExists(loc):
        mc.createNode('transform', name = loc)
        mc.parent(loc,stillGrp)
        attr = ('tx', 'ty', 'tz','rx','ry','rz','sx','sy','sz','visibility')
        for at in attr:
            mc.setAttr(loc + '.' + at, l = True, k = False)

    visLoc = 'VisFxRig_Loc'
    if not mc.objExists(visLoc):
        mc.createNode('transform', name = visLoc)
        mc.parent(visLoc,stillGrp)
        attr = ('tx', 'ty', 'tz','rx','ry','rz','sx','sy','sz','visibility')
        for at in attr:
            mc.setAttr(visLoc + '.' + at, l = True, k = False)     

    ########################################   
    
    listCrv = [pathCrv]
    if pm.checkBox('checkMirror', v = True, q = True) == True:
        if side == '_L_':
            xside = '_R_'
            
        else:
            xside = '_L_'
        xpathCrv = mc.duplicate(pathCrv, name = '{}{}Ctrl'.format(name,xside))[0]
        mc.scale(-1, 1, 1, xpathCrv)
        mc.makeIdentity(xpathCrv, a = True)
        mc.delete(xpathCrv,ch = True)
        listCrv.append(xpathCrv)
    else:
        pass

    for obj in listCrv:
        pathGrp = part + 'Ctrl_Grp'
        if not mc.objExists(pathGrp):
            pathGrp = mc.createNode( 'transform' , name = pathGrp )
            mc.parent(pathGrp,ctrlGrp)
            #mc.parentConstraint(parent,pathGrp, mo =False)
    
        objName = obj.split('_')
        name = objName[0]

        print obj
        mc.xform(pathCrv, cp =True)
        mc.delete(pathCrv, ch = True)
        mc.scale(size, size, size, pathCrv + ".cp[:]", r=True)

        type = objName[-1]
        if len(objName) == 2:
            objSide = '_'
        else:
            objSide = '_' + objName[1] + '_'
     
        zro = mc.createNode( 'transform' , name =  '{}{}Zro{}Grp'.format(name,type,objSide))
        ofst = mc.createNode( 'transform' , name =  '{}{}Ofst{}Grp'.format(name,type,objSide))
        mc.parent(obj,ofst)
        mc.parent(ofst,zro)
        mc.parent(zro,pathGrp)

        ############################LockCtrl
        mc.addAttr(obj, longName = 'fx', at = 'float', k = True, min = 0, max = 10, dv = 1)
        mc.addAttr(obj, longName = 'reverse', at = 'long', k = True, min = 0, max = 1)
        attr = ('tx', 'ty','tz','rx','ry','rz','sx','sy','sz','visibility')
        for at in attr:
            mc.setAttr(obj + '.' + at, l = True, k = False)

        ############################ConnectLocGeovis 
        fxMult = mc.createNode('multiplyDivide', name = '{}Fx{}Mdv'.format(name,objSide))
        visCnd = mc.createNode('condition', name = '{}Vis{}Cnd'.format(name,objSide))
        revVisCnd = mc.createNode('condition', name = '{}RevVis{}Cnd'.format(name,objSide))
        mc.connectAttr(obj + '.reverse', revVisCnd + '.firstTerm')
        mc.setAttr(revVisCnd + '.colorIfTrue.colorIfTrueR', 0)
        mc.setAttr(revVisCnd + '.colorIfFalse.colorIfFalseR', 1)
        mc.setAttr(visCnd + '.operation', 0)
        mc.setAttr(visCnd + '.colorIfTrue.colorIfTrueR', 0)
        mc.setAttr(visCnd + '.colorIfFalse.colorIfFalseR', 1)

        mc.setAttr(fxMult + '.operation', 1)
        mc.setAttr(fxMult + '.input2X', 0.1)
        mc.connectAttr(obj + '.fx', fxMult + '.input1X')
        mc.connectAttr(fxMult + '.outputX', visCnd + '.firstTerm')
        mc.connectAttr(revVisCnd + '.outColor.outColorR', visCnd + '.secondTerm')
        
        mc.addAttr(visLoc, longName = '{}{}Geo'.format(name,objSide), at = 'long', k = True, min = 0, max = 1)
        mc.connectAttr(visCnd + '.outColor.outColorR', visLoc + '.{}{}Geo'.format(name,objSide))

        ############################ConnectLoc
        fxName = 'sc__{}{}Rmp'.format(name,objSide)
        revName = 'sc__{}{}Cnd'.format(name,objSide)
        mc.addAttr(loc, longName = fxName, at = 'float', k = True, min = 0, max = 1)
        mc.addAttr(loc, longName = revName, at = 'long', k = True, min = 0, max = 1)
        mc.connectAttr(fxMult + '.outputX',loc + '.' + fxName)
        mc.connectAttr(obj + '.reverse',loc + '.' + revName) 
        print name + objSide
        print '--------------- DONE ---------------'

        pm.text ('process', edit=True , label = name + objSide + 'Geo is DONE')

def connectFxRig(*args):

    ########################################## Create ParsGrp

    stillParsGrp = 'FxRigStillPars_Grp'
    if not mc.objExists(stillParsGrp):
        mc.createNode('transform', name = stillParsGrp)
        mc.parent(stillParsGrp, 'bodyRig_md:Still_Grp')
    
    ctrlParsGrp = 'FxRigCtrlPars_Grp'
    if not mc.objExists(ctrlParsGrp):
        mc.createNode('transform', name = ctrlParsGrp)
        mc.parent(ctrlParsGrp, 'bodyRig_md:AddCtrl_Grp')
        mc.setAttr(ctrlParsGrp + '.inheritsTransform',0)

    wrapGeoGrp = 'FxRigWrapGeo_Grp'
    if not mc.objExists(wrapGeoGrp):
        mc.createNode('transform', name = wrapGeoGrp)
        mc.parent(wrapGeoGrp, stillParsGrp)

    ########################################## Connect Shade

    locAttr = mc.listAttr('fxRig_md:FxRig_Loc', k = True)

    asset = context_info.ContextPathInfo() 
    charName = asset.name

    for attr in locAttr:
        if not mc.objExists('bodyRig_md:Geo_Grp.{}'.format(attr)):
            mc.addAttr('bodyRig_md:Geo_Grp', ln = attr, at = 'float', k =True)
        mc.connectAttr('fxRig_md:FxRig_Loc.{}'.format(attr),'bodyRig_md:Geo_Grp.{}'.format(attr), f = True)
        
        nodeName = attr.split('sc__')[-1]
        nsShade = charName + '_mdlMainMtr'
        ShadeNode = '{}:{}'.format(nsShade,nodeName)
        typeNode = nodeName.split('_')[-1]
        
        if typeNode == 'Rmp':
            mc.connectAttr('bodyRig_md:Geo_Grp.{}'.format(attr),'{}.colorEntryList[0].position'.format(ShadeNode), f = True)
        elif typeNode == 'Cnd':
            mc.connectAttr('bodyRig_md:Geo_Grp.{}'.format(attr),'{}.firstTerm'.format(ShadeNode), f = True)

    ########################################## Connect Geo

    listGeo = []
    locVisAttr = mc.listAttr('fxRig_md:VisFxRig_Loc', k = True)
    for visAttr in locVisAttr:
        geoName = visAttr.split('sc__')[-1]
        geo = 'bodyRig_md:' +  geoName
        name = geoName.split('_')
        if len(name) == 2:
            geoSide = '_'
        else:
            geoSide = '_' + name[1] + '_'

        cnd = charName + '_mdlMainMtr:{}{}Cnd'.format(name[0],geoSide)
        shadeCon = mc.listConnections(cnd + '.outColor', d = True)[0]
        shlr = mc.listConnections(shadeCon + '.outColor', d = True)[0]
        
        if mc.nodeType(shlr) != 'layeredShader':
            mc.connectAttr('fxRig_md:VisFxRig_Loc.{}'.format(visAttr),geo + '.visibility', f = True)
            listGeo.append(geo)

        else: pass 


    ########################################### Wrap

    headCtrlGrp = 'fxRig_md:HeadCtrl_Grp'
    if not mc.objExists(headCtrlGrp):
        headPath = []

    else :   
        headPathShape = mc.listRelatives('fxRig_md:HeadCtrl_Grp',ad= True, type = 'nurbsCurve')
        headPath = mc.listRelatives(headPathShape,p= True)

        for each in headPath:
            eachFullName = each.split(':')[1]
            eName = eachFullName.split('_')
            eachName = eName[0]

            if len(eName) == 2:
                eachSide = '_'
            else:
                eachSide = '_{}_'.format(eName[1])

            bodyGeo = 'bodyRig_md:{}{}Geo'.format(eachName,eachSide)
            fxGeo = mc.duplicate(bodyGeo,name = '{}FxRigWrap{}Geo'.format(eachName,eachSide))
            mc.parent (fxGeo,wrapGeoGrp)

            # mc.select(bodyGeo)
            # mc.select(fxGeo, add =True)
            rt.blendShape(bshGeo = bodyGeo , target = fxGeo)
            # lrr.doAddBlendShape()

            headPathName = 'fxRig_md:{}{}Ctrl'.format(eachName,eachSide)
            mc.select(headPathName)
            mc.select(fxGeo, add= True)
            wrap = mm.eval('doWrapArgList "7"{"1","0","10","1","0","1","1","0"};')[0]

            #mc.CreateWrap()

    ########################################### CopyWeight
    for obj in listGeo:
        ns,objFullName = obj.split(':')
        objName = objFullName.split('_')
        if len(objName) == 2:
            objSide = '_'
        else:
            objSide = '_{}_'.format(objName[1])

        pathName = 'fxRig_md:{}{}Ctrl'.format(objName[0],objSide)
        mc.setAttr(pathName+'.overrideEnabled', 1)
        mc.setAttr(pathName+'.overrideColor', 9)

        if not pathName in headPath:
            sels = [obj,pathName]
            selsMas = sels[0]
            seleChi = sels[1:]
            for sel in seleChi:
                jnts = mc.skinCluster( selsMas , q = True , inf = True )
            
            oSkn = mm.eval('findRelatedSkinCluster %s' %sels[-1])
            if oSkn :
                mc.skinCluster( oSkn , e = True , ub = True )
            
            skn = mc.skinCluster( jnts , sel , tsb = True )[0]
            
            mc.select( selsMas , r=True )
            mc.select( sel , add=True )
            mm.eval( 'copySkinWeights  -noMirror -surfaceAssociation closestPoint -influenceAssociation closestJoint;' )
            mc.select(cl=True)

    ########################################### Keep Grp

    listStillParsGrp = mc.listRelatives(stillParsGrp, c = True)
    if listStillParsGrp:
        if not 'fxRig_md:FxRigStill_Grp' in listStillParsGrp:
            mc.parent('fxRig_md:FxRigStill_Grp',stillParsGrp)
    else :
        mc.parent('fxRig_md:FxRigStill_Grp',stillParsGrp)

   
    listCtrlParsGrp = mc.listRelatives(ctrlParsGrp, c = True)
    if listCtrlParsGrp:
        if not 'fxRig_md:FxRigCtrl_Grp' in listCtrlParsGrp:
            mc.parent('fxRig_md:FxRigCtrl_Grp',ctrlParsGrp)
    else :
        mc.parent('fxRig_md:FxRigCtrl_Grp',ctrlParsGrp)


    # if not 'fxRig_md:FxRigCtrl_Grp' in mc.listRelatives(ctrlParsGrp, c = True):
    #     mc.parent('fxRig_md:FxRigCtrl_Grp',ctrlParsGrp)

    print ' - C O N N E CT  D O N E -'

    mc.confirmDialog( title = 'Connect FxRig', message = 'D O N E !', button = ['OK'], defaultButton = 'OK')

def connectLookDev(*args):
    grpAttr = mc.listAttr('bodyRig_md:Geo_Grp', k = True)

    asset = context_info.ContextPathInfo() 
    charName = asset.name

    for attr in grpAttr:
        if not attr == 'visibility' : 
            nodeName = attr.split('sc__')[-1]
            nsLdvShade = charName + '_ldvMainMtr'
            ShadeNode = '{}:{}'.format(nsLdvShade,nodeName)
            typeNode = nodeName.split('_')[-1]
            
            if typeNode == 'Rmp':
                mc.connectAttr('bodyRig_md:Geo_Grp.{}'.format(attr),'{}.colorEntryList[0].position'.format(ShadeNode))
            elif typeNode == 'Cnd':
                mc.connectAttr('bodyRig_md:Geo_Grp.{}'.format(attr),'{}.firstTerm'.format(ShadeNode))

    print ' - C O N N E CT  D O N E -'

    mc.confirmDialog( title = 'Connect LookDev', message = 'D O N E !', button = ['OK'], defaultButton = 'OK')

def refShade(*args):
    from rf_utils.context import context_info
    entity = context_info.ContextPathInfo()
    from rftool.asset.setup import setup_app
    reload(setup_app)
    setup_app.show(entity)

def Run():
    windowname = 'FxRigTools2'
    if mc.window(windowname, exists = True):
        mc.deleteUI(windowname)

    width = 230
    highth = 270

    ctrlWin = pm.window(windowname,widthHeight=(width, highth), sizeable = True)
    allColumn = pm.columnLayout(adj = True, parent = ctrlWin) 

    #setup 
    pm.separator(h = 5, st = 'none') 
    setup = pm.columnLayout(cal = 'center', adj = True, parent = allColumn)
    #pm.separator(h = 5, st = 'none')  
    pm.text(label = '-  S E T U P  -', fn = 'boldLabelFont',bgc = [0,0,0] )

    pm.separator(h = 10)

    pm.text(label = u'\u203C\u0020\u0E16\u0E49\u0E32\u0E42\u0E14\u0E19 Bsh \u0E17\u0E35\u0E48\u0E2B\u0E31\u0E27 part = Head \u203C' , fn = 'boldLabelFont')
    pm.separator(h = 5, st = 'none')

    partRow= pm.rowColumnLayout( numberOfColumns=3, cw = [(1,width*0.25),(2,width*0.55),(3,width*0.2)], p = setup)
    pm.text('  Part', p = partRow, al = 'left')
    part = pm.textField('fillPartName', p = partRow)
    pm.rowColumnLayout (p = partRow) 

    mirrorRow= pm.rowColumnLayout( numberOfColumns=2, cw = [(1,width*0.25),(2,width*0.75)], p = setup)
    pm.text('  Mirror', p = mirrorRow, al = 'left')
    pm.checkBox('checkMirror', label = '', p = mirrorRow, v = False)
    pm.separator(h = 5, st = 'none')

    sizeRow= pm.rowColumnLayout( numberOfColumns=3, cw = [(1,width*0.25),(2,width*0.55),(3,width*0.2)], p = setup)
    pm.text('  ctrlSize', p = sizeRow, al = 'left')
    size = pm.floatField('fillsize', v = 1.035 , p = sizeRow, ed = True)
    pm.rowColumnLayout (p = sizeRow) 

    pm.separator(h = 5, st = 'none', p = setup)  
    pm.separator(h = 5, p = setup)

    ############################################################################

    #Path
    createCtrl= pm.rowColumnLayout( numberOfColumns=1, cw = [(1,width)], p = allColumn)
    #createCtrl = pm.columnLayout(cal = 'center', adj = True, cw = [(1,width)] ,parent = allColumn)
      
    pathRow = pm.columnLayout(adj = True, parent = createCtrl)
    pm.button(command = creatCtrl, bgc = [0.3,0.5,0.5] ,label = u'\u25E6' + '  ' + u'\u0E2A\u0E23\u0E49\u0E32\u0E07\u0E04\u0E2D\u0E19\u0E42\u0E17\u0E23\u0E25'+ '  ' + u'\u25E6')
    #pm.button(command = 'creatCtrl()',label = u'\u25A0' + '   CREATE CTRL   ' + u'\u25A0')
    pm.separator(h = 3, st = 'none')
    process = pm.text ('process', label = '',bgc = [0.3,0.3,.4] )
    pm.separator(h = 10)

    #main 
    main = pm.columnLayout(cal = 'center', adj = True, parent = allColumn)
    #pm.separator(h = 5, st = 'none')  
    pm.text(label = '-  M A I N  -', fn = 'boldLabelFont',bgc = [0,0,0] )

    pm.separator(h = 10)

    connectMain= pm.rowColumnLayout( numberOfColumns=1, cw = [(1,width)], p = allColumn)
    pm.button('refRelodShade', bgc = [0.2,0.15,0.3] , label = 'Ref / Reload Texture',command = refShade,p = connectMain)
    connectRow = pm.rowColumnLayout( numberOfColumns=2, cw = [(1,width*0.5),(2,width*0.5)], p = connectMain)
    pm.button('connectMdlShd', bgc = [0.3,0.5,0.5] , label = 'Connect FxRig',command = connectFxRig,p = connectRow, w = width/2)
    pm.button('connectLdvShd', bgc = [0.3,0.5,0.5] , label = 'Connect LookDev',command = connectLookDev,p = connectRow, w = width/2)

    pm.separator(h = 3, st = 'none')

    warn= pm.rowColumnLayout( numberOfColumns=1, cw = [(1,width)], p = allColumn)
    #pm.text('*Please Ref Mdl Shade Before Run', p = warn)
    pm.text(label = u'\u203C\u0020\u0E42\u0E1B\u0E23\u0E14\u0E40\u0E23\u0E1F\u0E40\u0E17\u0E01\u0E40\u0E08\u0E2D\u0E23\u0E4C\u0E42\u0E21\u0E40\u0E14\u0E25\u0E01\u0E48\u0E2D\u0E19\u0E01\u0E14\u0E1B\u0E38\u0E48\u0E21\u0020\u203C',fn = 'boldLabelFont', p = warn)
    pm.separator(h = 10)

    ############################################################################
        
    mc.showWindow(windowname)