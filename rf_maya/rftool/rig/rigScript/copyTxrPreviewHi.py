from rf_maya.rftool.rig.rigScript import caFacialModule
reload(caFacialModule)
from rf_utils.context import context_info
reload(context_info)
import os, shutil  
from utaTools.utapy import utaCore
reload(utaCore)
from rf_utils import admin
def copyTxrPreviewHi(*args):
    '''
    ## copy texture hi and preview
    from rf_maya.rftool.rig.rigScript import copyTxrPreviewHi
    reload(copyTxrPreviewHi)
    copyTxrPreviewHi.copyTxrPreviewHi()

    '''
    asset = context_info.ContextPathInfo()
    process = asset.process 
    charName = asset.name 
    projectName = asset.project 

    if projectName in 'CA':
        if process in 'uiCtrl':
            folder = ['hi', 'preview']
            ## check source path
            sourcePath = 'P:/{}/asset/work/char/{}/texture/main/images'.format(projectName, charName)
            dir_list = os.listdir(sourcePath)

            ## check texRen and texPreview folder 
            texFolder = 'P:/{}/asset/publ/char/{}/texture/main/hero'.format(projectName, charName)
            dirTex_list = os.listdir(texFolder)
            if not 'texRen' in dirTex_list:
                utaCore.createFolderSource(path = texFolder, fldName = 'texRen')
            elif not 'texPreview' in dirTex_list:
                utaCore.createFolderSource(path = texFolder, fldName = 'texPreview')

            ## check target ren path
            sourceRenPath = 'P:/{}/asset/publ/char/{}/texture/main/hero/texRen'.format(projectName, charName)
            dirRen_list = os.listdir(sourceRenPath)

            ## check target preview path
            sourcePreviewPath = 'P:/{}/asset/publ/char/{}/texture/main/hero/texPreview'.format(projectName, charName)
            dirPreview_list = os.listdir(sourcePreviewPath)

            for txr in folder:
                if txr in 'hi':
                    if txr in dirRen_list:
                        caFacialModule.copyFolderTexture(   sourcePath = '{}/{}'.format(sourcePath, txr),
                                                            subSourcePath = '',  
                                                            targetPath = '{}/{}'.format(sourceRenPath, txr), 
                                                            subTargetPath = '')   
                        print 'Copy >>', txr, ' texture file is Done!!'
                    else:
                        source = '{}/{}'.format(sourcePath, txr)
                        targetPath = '{}/{}'.format(sourceRenPath, txr)  
                        #shutil.copytree(source, targetPath)
                        admin.win_copy_directory(source,targetPath)
                        print 'Copy >>', txr, ' texture folder is Done!!'

                elif txr in 'preview':
                    if txr in dirPreview_list:
                        caFacialModule.copyFolderTexture(   sourcePath = '{}/{}'.format(sourcePath, txr),
                                                            subSourcePath = '',  
                                                            targetPath = '{}/{}'.format(sourcePreviewPath, txr), 
                                                            subTargetPath = '')   
                        print 'Copy >>', txr, ' texture file is Done!!'
                    else:
                        source = '{}/{}'.format(sourcePath, txr)
                        targetPath = '{}/{}'.format(sourcePreviewPath, txr)  
                        #shutil.copytree(source, targetPath)
                        admin.win_copy_directory(source,targetPath)
                        print 'Copy >>', txr, ' texture folder is Done!!'
            prevTxr = os.path.join(sourcePath,'{}_Diffuse_Head.png'.format(charName))
            prevTxrPub = os.path.join(sourcePreviewPath,'{}_Diffuse_Head.png'.format(charName))
            admin.copyfile(prevTxr, prevTxrPub)
            print 'Done!!'
           
