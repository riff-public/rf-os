import maya.cmds as mc
import maya.mel as mm
import core
import rigTools as rt
import ribbonRig
import fkRig
reload( core )
reload( rt )
reload( ribbonRig )
reload(fkRig)
from rf_utils.context import context_info
reload(context_info)
cn = core.Node()
#--------------------------------------------------------------------------------------------------------------
#
#  LEG RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self ,    upLegTmpJnt   = 'UpLeg_L_TmpJnt' ,
                            lowLegTmpJnt  = 'LowLeg_L_TmpJnt' ,
                            ankleTmpJnt   = 'Ankle_L_TmpJnt' ,
                            ballTmpJnt    = 'Ball_L_TmpJnt' ,
                            toeTmpJnt     = 'Toe_L_TmpJnt' ,
                            heelTmpJnt    = 'Heel_L_TmpJnt' ,
                            footInTmpJnt  = 'FootIn_L_TmpJnt' ,
                            footOutTmpJnt = 'FootOut_L_TmpJnt' ,
                            kneeTmpJnt    = 'Knee_L_TmpJnt' ,
                            heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_L_TmpJnt',
                            toeIkTwisyPivTmpJnt = 'ToeIkTwistPiv_L_TmpJnt' ,
                            smartIkTmpJnt = 'FootSmartIk_L_TmpJnt' ,
                            parent        = 'Pelvis_Jnt' , 
                            ctrlGrp       = 'Ctrl_Grp' ,
                            jntGrp        = 'Jnt_Grp' ,
                            skinGrp       = 'Skin_Grp' ,
                            stillGrp      = 'Still_Grp' ,
                            ikhGrp        = 'Ikh_Grp' ,
                            elem          = '' ,
                            side          = 'L' ,
                            ribbon        = True ,
                            hi            = True ,
                            foot          = True ,
                            kneePos       = '' ,
                            size          = 1 
                 ):

        ##-- Info
        elem = elem.capitalize()
        rootJnt = 'Root_Jnt'

        if side == '' :
            sideRbn = ''
            side = '_'
        else :
            sideRbn = '{}'.format(side)
            side = '_{}_'.format(side)

        if 'L' in side  :
            valueSide = 1
            axis = 'y+'
            aimVec = [0 , 1 ,0]
            upVec = [ 0 , 0 , 1 ]
            kneeAim = [ 0 , -1 , 0 ]
            kneeUpVec = [ 1 , 0 , 0 ]
            rootTwsAmp = 1
            endTwsAmp = -1
        elif 'R' in side :
            valueSide = -1
            axis = 'y-'
            aimVec = [0, -1, 0]
            upVec = [ 0 , 0 , -1 ]
            kneeAim = [ 0 , 1 , 0 ]
            kneeUpVec = [ -1 , 0 , 0 ]
            rootTwsAmp = -1
            endTwsAmp = 1

        #-- Create Main Group
        self.legCtrlGrp = cn.createNode( 'transform' , 'Leg{}Ctrl{}Grp'.format( elem , side ))
        self.legJntGrp = cn.createNode( 'transform' , 'Leg{}Jnt{}Grp'.format( elem , side ))
        mc.parentConstraint( parent , self.legCtrlGrp , mo = False )
        self.legJntGrp.snap( parent )
        
        #-- Create Main Joint
        self.upLegJnt = cn.joint( 'UpLeg{}{}Jnt'.format( elem , side ) , upLegTmpJnt )
        self.lowLegJnt = cn.joint( 'LowLeg{}{}Jnt'.format( elem , side ) , lowLegTmpJnt )
        self.ankleJnt = cn.joint( 'Ankle{}{}Jnt'.format( elem , side ) , ankleTmpJnt )
        self.ankleJnt.parent( self.lowLegJnt )
        self.lowLegJnt.parent( self.upLegJnt )
        self.upLegJnt.parent( parent )

        if foot == True :
            self.ballJnt = cn.joint( 'Ball{}{}Jnt'.format( elem , side ) , ballTmpJnt )
            self.toeJnt = cn.joint( 'Toe{}{}Jnt'.format( elem , side ) , toeTmpJnt )
            self.toeJnt.parent( self.ballJnt )
            self.ballJnt.parent( self.ankleJnt )

        #-- Create Controls
        self.legCtrl = rt.createCtrl( 'Leg{}{}Ctrl'.format( elem , side ) , 'stick' , 'green' )
        self.legZro = rt.addGrp( self.legCtrl )

        #-- Adjust Shape Controls
        self.legCtrl.scaleShape( size )

        if 'L' in side :
            self.legCtrl.rotateShape( -90 , 0 , 0 )
        else :
            self.legCtrl.rotateShape( 90 , 0 , 0 )
        
        #-- Adjust Rotate Order
        for obj in ( self.upLegJnt , self.lowLegJnt , self.ankleJnt ) :
            obj.setRotateOrder( 'yzx' )

        if foot == True :
            for obj in ( self.ballJnt , self.toeJnt ) :
                obj.setRotateOrder( 'yzx' )

        #-- Rig process
        mc.parentConstraint( self.ankleJnt , self.legZro , mo = False )
        upLegNonRollJnt = rt.addNonRollJnt( 'UpLeg' , elem , self.upLegJnt , self.lowLegJnt , parent , axis )
        #lowLegNonRollJnt = rt.addNonRollJnt( 'LowLeg' , elem , self.lowLegJnt , self.ankleJnt , self.upLegJnt , axis )
        ankleNonRollJnt =  rt.addNonRollJnt( 'Ankle' , elem , self.ankleJnt , self.ballJnt , self.lowLegJnt , axis )        
        self.upLegNonRollJntGrp = upLegNonRollJnt['jntGrp']
        self.upLegNonRollRootNr = upLegNonRollJnt['rootNr']
        self.upLegNonRollRootNrZro = upLegNonRollJnt['rootNrZro']
        self.upLegNonRollEndNr = upLegNonRollJnt['endNr']
        self.upLegNonRollIkhNr = upLegNonRollJnt['ikhNr']
        self.upLegNonRollIkhNrZro = upLegNonRollJnt['ikhNrZro']
        self.upLegNonRollTwistGrp = upLegNonRollJnt['twistGrp']

        self.ankleNonRollJntGrp = ankleNonRollJnt['jntGrp']
        self.ankleNonRollRootNr = ankleNonRollJnt['rootNr']
        self.ankleNonRollRootNrZro = ankleNonRollJnt['rootNrZro']
        self.ankleNonRollEndNr = ankleNonRollJnt['endNr']
        self.ankleNonRollIkhNr = ankleNonRollJnt['ikhNr']
        self.ankleNonRollIkhNrZro = ankleNonRollJnt['ikhNrZro']
        self.ankleNonRollTwistGrp = ankleNonRollJnt['twistGrp']
        
        #-- Adjust Hierarchy
        mc.parent( self.legZro , self.legCtrlGrp )
        mc.parent( self.legCtrlGrp , ctrlGrp )
        mc.parent( self.upLegNonRollJntGrp , self.ankleNonRollJntGrp , self.legJntGrp )
        mc.parent( self.legJntGrp , jntGrp )

        #-- Cleanup
        for obj in ( self.legZro , self.legJntGrp , self.legCtrlGrp ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        self.legCtrl.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        #-- Fk
        #-- Create Main Group Fk
        self.legFkCtrlGrp = cn.createNode( 'transform' , 'Leg{}FkCtrl{}Grp'.format( elem , side ))
        self.legFkJntGrp = cn.createNode( 'transform' , 'Leg{}FkJnt{}Grp'.format( elem , side ))

        self.legFkCtrlGrp.snap( self.legCtrlGrp )
        self.legFkJntGrp.snap( self.legCtrlGrp )

        self.upLegFk_obj = fkRig.Run(name       = 'UpLeg{}Fk'.format(elem) ,
                                    side        = sideRbn ,
                                    tmpJnt      = upLegTmpJnt ,
                                    ctrlGrp     = ctrlGrp , 
                                    shape       = 'cylinder' ,
                                    jnt_ctrl    = 1 ,
                                    color       = 'red',
                                    localWorld  = 1,
                                    constraint  = 0,
                                    jnt         = 1,
                                    dtl         = 0)

        self.lowLegFk_obj = fkRig.Run(name       = 'LowLeg{}Fk'.format(elem) ,
                                    side        = sideRbn ,
                                    tmpJnt      = lowLegTmpJnt ,
                                    ctrlGrp     = ctrlGrp , 
                                    shape       = 'cylinder' ,
                                    jnt_ctrl    = 1 ,
                                    color       = 'red',
                                    localWorld  = 0,
                                    constraint  = 0,
                                    jnt         = 1,
                                    dtl         = 0)

        self.ankleFk_obj = fkRig.Run(name       = 'Ankle{}Fk'.format(elem) ,
                                    side        = sideRbn ,
                                    tmpJnt      = ankleTmpJnt ,
                                    ctrlGrp     = ctrlGrp , 
                                    shape       = 'cylinder' ,
                                    jnt_ctrl    = 1 ,
                                    color       = 'red',
                                    localWorld  = 0,
                                    constraint  = 0,
                                    jnt         = 1,
                                    dtl         = 0)

        if foot == True :
            self.ballFk_obj = fkRig.Run(name       = 'Ball{}Fk'.format(elem) ,
                                        side        = sideRbn ,
                                        tmpJnt      = ballTmpJnt ,
                                        ctrlGrp     = ctrlGrp , 
                                        shape       = 'cylinder' ,
                                        jnt_ctrl    = 1 ,
                                        color       = 'red',
                                        localWorld  = 0,
                                        constraint  = 0,
                                        jnt         = 1,
                                        dtl         = 0)
            
            self.ballFkSclOfst = cn.createNode( 'transform' , 'Ball{}FkCtrlSclOfst{}Grp'.format( elem , side ) )
            self.ballFkSclOfst.snap( self.ballFk_obj.jnt )
            self.ballFk_obj.zro.snapPoint( self.ballFk_obj.jnt )
            self.ballFk_obj.ctrl.snapJntOrient( self.ballFk_obj.jnt )
            self.ballFk_obj.zro.parent( self.ballFkSclOfst )

            self.toeFkJnt = cn.joint( 'Toe{}Fk{}Jnt'.format( elem , side ) , toeTmpJnt )
            self.toeFkJnt.parent( self.ballFk_obj.jnt )
            self.ballFk_obj.jnt.parent( self.ankleFk_obj.jnt )

        self.ankleFk_obj.jnt.parent( self.lowLegFk_obj.jnt )
        self.lowLegFk_obj.jnt.parent( self.upLegFk_obj.jnt )
        self.upLegFk_obj.jnt.parent( self.legFkJntGrp )

        #-- Adjust Shape Controls Fk
        for ctrl in ( self.upLegFk_obj.ctrl , self.upLegFk_obj.gmbl , self.lowLegFk_obj.ctrl , self.lowLegFk_obj.gmbl , self.ankleFk_obj.ctrl , self.ankleFk_obj.gmbl ) :
            ctrl.scaleShape( size )
            
        if foot == True :
            for ctrl in ( self.ballFk_obj.ctrl , self.ballFk_obj.gmbl ) :
                ctrl.scaleShape( size )
    
        #-- Adjust Rotate Order Fk
        for obj in ( self.upLegFk_obj.jnt , self.lowLegFk_obj.jnt , self.ankleFk_obj.jnt , self.upLegFk_obj.ctrl , self.lowLegFk_obj.ctrl , self.ankleFk_obj.ctrl ) :
            obj.setRotateOrder( 'yzx' )

        if foot == True :
            for obj in ( self.ballFk_obj.jnt , self.toeFkJnt , self.ballFk_obj.ctrl ) :
                obj.setRotateOrder( 'yzx' )

        self.upLegFk_obj.ctrl_obj.do_constraint('parent')
        self.lowLegFk_obj.ctrl_obj.do_constraint('parent')
        self.ankleFk_obj.ctrl_obj.do_constraint('parent')

        rt.addFkStretch( self.upLegFk_obj.ctrl , self.lowLegFk_obj.ofst , 'y' , valueSide )
        rt.addFkStretch( self.lowLegFk_obj.ctrl , self.ankleFk_obj.ofst , 'y' , valueSide )

        if foot == True :
            self.ballFk_obj.ctrl_obj.do_constraint('parent')
            rt.addFkStretch( self.ballFk_obj.ctrl , self.toeFkJnt , 'y' , valueSide )

        #-- Adjust Hierarchy Fk
        mc.parent( self.legFkCtrlGrp , self.legCtrlGrp )
        mc.parent( self.legFkJntGrp , self.legJntGrp )
        mc.parent( self.upLegFk_obj.ctrlGrp , self.legFkCtrlGrp )
        mc.parent( self.lowLegFk_obj.ctrlGrp , self.upLegFk_obj.gmbl )
        mc.parent( self.ankleFk_obj.ctrlGrp , self.lowLegFk_obj.gmbl )

        if foot == True :
            mc.parent( self.ballFkSclOfst , self.ballFk_obj.ctrlGrp )
            mc.parent( self.ballFk_obj.ctrlGrp , self.ankleFk_obj.gmbl )

        #-- Ik
        #-- Create Main Group Ik
        self.legIkCtrlGrp = cn.createNode( 'transform' , 'Leg{}IkCtrl{}Grp'.format( elem , side ))
        self.legIkJntGrp = cn.createNode( 'transform' , 'Leg{}IkJnt{}Grp'.format( elem , side ))
        self.legIkIkhGrp = cn.createNode( 'transform' , 'Leg{}Ikh{}Grp'.format( elem , side ))

        self.legIkCtrlGrp.snap( self.legCtrlGrp )
        self.legIkJntGrp.snap( self.legCtrlGrp )
        self.legIkIkhGrp.snap( self.legCtrlGrp )

        if foot == True :
            #-- Create Pivot Group Ik
            self.footPivGrp = cn.createNode( 'transform' , 'Foot{}IkPiv{}Grp'.format( elem , side ))
            self.ballPivGrp = cn.createNode( 'transform' , 'Ball{}IkPiv{}Grp'.format( elem , side ))
            self.ballPivCtrl = rt.createCtrl( 'Ball{}IkPiv{}Ctrl'.format( elem , side ) , 'arcCapsule' , 'blue' , jnt = 0 )
            
            self.bendPivGrp = cn.createNode( 'transform' , 'Bend{}IkPiv{}Grp'.format( elem , side ))
            self.bendPivCtrl = rt.createCtrl( 'Bend{}IkPiv{}Ctrl'.format( elem , side ) , 'arcCapsule' , 'blue' , jnt = 0 )

            
            self.heelPivGrp = cn.createNode( 'transform' , 'Heel{}IkPiv{}Grp'.format( elem , side ))
            self.heelPivCtrl = rt.createCtrl( 'Heel{}IkPiv{}Ctrl'.format( elem , side ) , 'sphere' , 'yellow' , jnt = 0 )
            
            
            self.heelTwistPivGrp = cn.createNode( 'transform' , 'Heel{}IkTwistPiv{}Grp'.format( elem , side ))
            self.heelTwistPivCtrl = rt.createCtrl( 'Heel{}IkTwistPiv{}Ctrl'.format( elem , side ) , 'rotateCircle' , 'yellow' , jnt = 0 )

            self.toePivGrp = cn.createNode( 'transform' , 'Toe{}IkPiv{}Grp'.format( elem , side ))
            self.toePivCtrl = rt.createCtrl( 'Toe{}IkPiv{}Ctrl'.format( elem , side ) , 'sphere' , 'yellow' , jnt = 0 )
            
            self.toeTwistPivGrp = cn.createNode( 'transform' , 'Toe{}IkTwistPiv{}Grp'.format( elem , side ))
            self.toeTwistPivCtrl = rt.createCtrl( 'Toe{}IkTwistPiv{}Ctrl'.format( elem , side ) , 'rotateCircle' , 'yellow' , jnt = 0 )


            self.inPivGrp = cn.createNode( 'transform' , 'In{}IkPiv{}Grp'.format( elem , side ))
            self.inPivCtrl  = rt.createCtrl( 'In{}IkPiv{}Ctrl'.format( elem , side ) , 'sphere' , 'yellow' , jnt = 0 )

            self.outPivGrp = cn.createNode( 'transform' , 'Out{}IkPiv{}Grp'.format( elem , side ))
            self.outPivCtrl = rt.createCtrl( 'Out{}IkPiv{}Ctrl'.format( elem , side ) , 'sphere' , 'yellow' , jnt = 0 )
           
            self.anklePivGrp = cn.createNode( 'transform' , 'Ankle{}IkPiv{}Grp'.format( elem , side ))

            self.footPivGrp.snapPoint( ankleTmpJnt )
            self.ballPivGrp.snapPoint( ballTmpJnt )
            self.bendPivGrp.snapPoint( ballTmpJnt )
            self.toePivGrp.snapPoint( toeTmpJnt )
            self.heelPivGrp.snapPoint( heelTmpJnt )
            self.inPivGrp.snapPoint( footInTmpJnt )
            self.outPivGrp.snapPoint( footOutTmpJnt )
            self.anklePivGrp.snapPoint( ankleTmpJnt )
            
            self.toeTwistPivGrp.snapPoint(toeIkTwisyPivTmpJnt)
            mc.delete(mc.pointConstraint( heelIkTwistPivTmpJnt , self.heelTwistPivGrp , mo = False ))

            for obj in ( self.footPivGrp , self.ballPivGrp , self.bendPivGrp , self.toePivGrp , self.toeTwistPivGrp , self.heelPivGrp , self.heelTwistPivGrp , self.inPivGrp , self.outPivGrp ) :
                obj.snapOrient( self.ballFk_obj.jnt )
            
            self.ballPivCtrl.snap( self.ballPivGrp )
            self.bendPivCtrl.snap( self.bendPivGrp )
            self.toePivCtrl.snap( self.toePivGrp )
            self.heelPivCtrl.snap( self.heelPivGrp )
            self.inPivCtrl.snap( self.inPivGrp )
            self.outPivCtrl.snap( self.outPivGrp )          
            self.toeTwistPivCtrl.snap( self.toeTwistPivGrp )
            self.heelTwistPivCtrl.snap( self.heelTwistPivGrp )
     

            self.anklePivGrp.parent(self.ballPivCtrl)
            self.ballPivCtrl.parent(self.ballPivGrp)
            self.bendPivCtrl.parent(self.bendPivGrp)
            mc.parent(self.bendPivGrp,self.ballPivGrp,self.outPivCtrl)
            self.outPivCtrl.parent(self.outPivGrp)
            self.outPivGrp.parent(self.inPivCtrl)
            self.inPivCtrl.parent(self.inPivGrp)
            self.inPivGrp.parent( self.heelTwistPivCtrl)
            self.heelTwistPivCtrl.parent( self.heelTwistPivGrp)
            self.heelTwistPivGrp.parent( self.heelPivCtrl)
            self.heelPivCtrl.parent( self.heelPivGrp)
            self.heelPivGrp.parent( self.toeTwistPivCtrl)
            self.toeTwistPivCtrl.parent( self.toeTwistPivGrp)
            self.toeTwistPivGrp.parent( self.toePivCtrl)
            self.toePivCtrl.parent(self.toePivGrp)
            self.toePivGrp.parent(self.footPivGrp )

            for ctrl in ( self.ballPivCtrl , self.bendPivCtrl , self.toePivCtrl , self.heelPivCtrl,self.inPivCtrl,self.outPivCtrl,self.heelTwistPivCtrl,self.toeTwistPivCtrl) :
                ctrl.scaleShape( size )
                for ax in 'xyz':
                    ctrl.attr('s{}'.format(ax)).lock
                    ctrl.attr('t{}'.format(ax)).lock
                ctrl.attr('v').lock

        self.upLegIk_obj = fkRig.Run(name       = 'UpLeg{}Ik'.format(elem) ,
                                    side        = sideRbn ,
                                    tmpJnt      = upLegTmpJnt ,
                                    ctrlGrp     = ctrlGrp , 
                                    shape       = 'cube' ,
                                    jnt_ctrl    = 1 ,
                                    color       = 'blue',
                                    localWorld  = 0,
                                    constraint  = 0,
                                    jnt         = 1,
                                    dtl         = 0)

        self.ankleIk_obj = fkRig.Run(name       = 'Ankle{}Ik'.format(elem) ,
                                    side        = sideRbn ,
                                    tmpJnt      = ankleTmpJnt ,
                                    ctrlGrp     = ctrlGrp , 
                                    shape       = 'cube' ,
                                    jnt_ctrl    = 1 ,
                                    color       = 'blue',
                                    localWorld  = 0,
                                    constraint  = 0,
                                    jnt         = 1,
                                    dtl         = 0)

        self.kneeIkCtrl = rt.createCtrl( 'Knee{}Ik{}Ctrl'.format( elem , side ) , 'sphere' , 'blue' , jnt = True )
        self.kneeIkZro = rt.addGrp( self.kneeIkCtrl )
        self.kneeIkZro.snapPoint( kneeTmpJnt )

        #-- Create Joint Ik
        self.lowLegIkJnt = cn.joint( 'LowLeg{}Ik{}Jnt'.format( elem , side ) , lowLegTmpJnt )
        self.ankleIk_obj.jnt.parent( self.lowLegIkJnt )
        self.lowLegIkJnt.parent( self.upLegIk_obj.jnt )
        self.upLegIk_obj.jnt.parent( self.legIkJntGrp )

        if foot == True :
            self.ballIkJnt = cn.joint( 'Ball{}Ik{}Jnt'.format( elem , side ) , ballTmpJnt )
            self.toeIkJnt = cn.joint( 'Toe{}Ik{}Jnt'.format( elem , side ) , toeTmpJnt )
            self.toeIkJnt.parent( self.ballIkJnt )
            self.ballIkJnt.parent( self.ankleIk_obj.jnt )

        #-- Adjust Shape Controls Ik
        for ctrl in ( self.upLegIk_obj.ctrl , self.upLegIk_obj.gmbl , self.ankleIk_obj.ctrl , self.ankleIk_obj.gmbl) :
            ctrl.scaleShape( size )
        
        self.kneeIkCtrl .scaleShape( size*0.25 )

        #-- Adjust Rotate Order Ik
        for obj in ( self.upLegIk_obj.jnt , self.lowLegIkJnt , self.ankleIk_obj.jnt , self.upLegIk_obj.ctrl , self.ankleIk_obj.ctrl ) :
            obj.setRotateOrder( 'yzx' )
        
        if foot == True :
            for obj in ( self.ballIkJnt , self.toeIkJnt ) :
                obj.setRotateOrder( 'yzx' )

        #-- Rig process Ik
        self.ankleIkLocWor = rt.localWorld( self.ankleIk_obj.ctrl , ctrlGrp , self.upLegIk_obj.gmbl , self.ankleIk_obj.zro , 'parent' )
        self.kneeIkLocWor = rt.localWorld( self.kneeIkCtrl , ctrlGrp , self.ankleIk_obj.ctrl , self.kneeIkZro , 'parent' )

        #-- Add more space local world knee ik
        rt.addMoreSpace(obj = self.kneeIkLocWor ,
                        spaces = [ ('Ankle' , self.kneeIkLocWor['localSpace'] ) ,
                                  ('Root', rootJnt )] )

        #-- Prefered Ankle
        # rt.preferredAnkleIk(obj = [self.lowLegIkJnt], pfx = -45, pfy = 0, pfz = 0)

        self.crv = rt.drawLine( self.kneeIkCtrl , self.lowLegIkJnt )

        self.ankleIkhDict = rt.addIkh( 'Ankle{}'.format(elem) , 'ikRPsolver' , self.upLegIk_obj.jnt , self.ankleIk_obj.jnt )
        
        self.ankleIkh = self.ankleIkhDict['ikh']
        self.ankleIkhZro = self.ankleIkhDict['ikhZro']

        self.polevector = mc.poleVectorConstraint( self.kneeIkCtrl , self.ankleIkh )[0]

        self.ankleIk_obj.ctrl.addAttr( 'twist' )

        self.ankleIk_obj.ctrl.attr( 'twist' ) >> self.ankleIkh.attr('twist')

        mc.pointConstraint( self.upLegIk_obj.gmbl , self.upLegIk_obj.jnt , mo = False )
        
        if not foot == True :
            mc.parentConstraint( self.ankleIk_obj.gmbl , self.ankleIkhZro , mo = True )

        self.ikStrt = rt.addIkStretch( self.ankleIk_obj.ctrl , self.kneeIkCtrl ,'ty' , 'Leg{}'.format(elem) , self.upLegIk_obj.jnt , self.lowLegIkJnt , self.ankleIk_obj.jnt )
        
        self.ankleIk_obj.ctrl.attr('localWorld').value = 1
        self.ankleIk_obj.ctrl.attr('autoStretch').value = 1

        if foot == True :
            self.ankleIk_obj.ctrl.addAttr( 'toeStretch' )
            self.toeStrtMdv = cn.createNode( 'multiplyDivide' , 'Toe{}IkStrt{}Mdv'.format( elem , side ))
            self.toeStrtMdv.attr('i2x').value = valueSide

            self.toeStrtPma = cn.createNode( 'plusMinusAverage' , 'Toe{}IkStrt{}Pma'.format( elem , side ))
            self.toeStrtPma.addAttr( 'default' )
            self.toeStrtPma.attr('default').value = self.toeIkJnt.attr('ty').value

            self.ankleIk_obj.ctrl.attr( 'toeStretch' ) >> self.toeStrtMdv.attr('i1x')
            self.toeStrtPma.attr( 'default' ) >> self.toeStrtPma.attr('i1[0]')
            self.toeStrtMdv.attr( 'ox' ) >> self.toeStrtPma.attr('i1[1]')
            self.toeStrtPma.attr( 'o1' ) >> self.toeIkJnt.attr('ty')

            self.ballIkhDict = rt.addIkh( 'Ball{}'.format(elem) , 'ikSCsolver' , self.ankleIk_obj.jnt , self.ballIkJnt )
            self.toeIkhDict = rt.addIkh( 'Toe{}'.format(elem) , 'ikSCsolver' , self.ballIkJnt , self.toeIkJnt )
        
            self.ballIkh = self.ballIkhDict['ikh']
            self.ballIkhZro = self.ballIkhDict['ikhZro']

            self.toeIkh = self.toeIkhDict['ikh']
            self.toeIkhZro = self.toeIkhDict['ikhZro']

            self.ankleIkhZro.clearConsLocal()
            self.ikStrt[2].clearConsLocal()

            self.ankleIkCtrlShape = core.Dag(self.ankleIk_obj.ctrl.shape)

            mc.parentConstraint( self.ballPivCtrl , self.ankleIkhZro , mo = 1 )
            mc.parentConstraint( self.ballPivCtrl , self.ballIkhZro , mo = 1 )
            mc.parentConstraint( self.bendPivCtrl , self.toeIkhZro , mo = 1 )
            mc.parentConstraint( self.anklePivGrp , self.ikStrt[2] , mo = 1 )

            self.ankleIk_obj.ctrl.addTitleAttr( 'Foot' )
            self.ankleIk_obj.ctrl.addAttr( 'ballRoll' )
            self.ankleIk_obj.ctrl.addAttr( 'toeHeelRoll' )
            self.ankleIk_obj.ctrl.addAttr( 'heelTwist' )
            self.ankleIk_obj.ctrl.addAttr( 'toeTwist' )
            self.ankleIk_obj.ctrl.addAttr( 'toeBend' )
            self.ankleIk_obj.ctrl.addAttr( 'toeSide' )
            self.ankleIk_obj.ctrl.addAttr( 'toeSwirl' )
            self.ankleIk_obj.ctrl.addAttr( 'footRock' )
            self.ankleIkCtrlShape.addAttr( 'toeBreak' )
            self.ankleIkCtrlShape.attr('toeBreak').value = -90

            self.ballBreakCmp = cn.createNode( 'clamp' , 'Ball{}IkBreak{}Cmp'.format( elem , side ))

            self.toeBreakCnd = cn.createNode( 'condition' , 'Toe{}IkBreak{}Cnd'.format( elem , side ))
            self.toeBreakAmpMdv = cn.createNode( 'multiplyDivide' , 'Toe{}IkBreakAmp{}Mdv'.format( elem , side ))
            self.toeBreakAmpPma = cn.createNode( 'plusMinusAverage' , 'Toe{}IkBreakAmp{}Pma'.format( elem , side ))
            self.toeBreakPma = cn.createNode( 'plusMinusAverage' , 'Toe{}IkBreak{}Pma'.format( elem , side ))
            self.toeBreakCnd.attr('operation').value = 2
            self.toeBreakAmpMdv.attr('i2x').value = -1
            self.toeBreakCnd.attr('cfr').value = 0

            self.heelBreakCmp = cn.createNode( 'clamp' , 'Heel{}IkBreak{}Cmp'.format( elem , side ))
            self.heelBreakPma = cn.createNode( 'plusMinusAverage' , 'Heel{}IkBreak{}Pma'.format( elem , side ))
            self.heelBreakCmp.attr('maxR').value = 100

            self.ankleIk_obj.ctrl.attr( 'ballRoll' ) >> self.ballBreakCmp.attr('inputR')
            self.ankleIkCtrlShape.attr( 'toeBreak' ) >> self.ballBreakCmp.attr('minR')

            self.ankleIkCtrlShape.attr( 'toeBreak' ) >> self.toeBreakAmpMdv.attr('i1x')
            self.toeBreakAmpMdv.attr( 'ox' ) >> self.toeBreakAmpPma.attr('i1[0]')
            self.toeBreakAmpPma.attr( 'o1' ) >> self.toeBreakCnd.attr('ctr')
            self.ankleIkCtrlShape.attr( 'toeBreak' ) >> self.toeBreakCnd.attr('ft')
            self.toeBreakCnd.attr( 'ocr' ) >> self.toeBreakPma.attr('i1[0]')
            self.toeBreakPma.attr( 'o1' ) >> self.toePivGrp.attr('rx')

            self.ankleIk_obj.ctrl.attr( 'ballRoll' ) >> self.heelBreakCmp.attr('inputR')
            self.heelBreakCmp.attr( 'outputR' ) >> self.heelBreakPma.attr('i1[0]')
            self.heelBreakPma.attr( 'o1' ) >> self.heelPivGrp.attr('rx')

            self.ankleIk_obj.ctrl.attr( 'toeHeelRoll' ) >> self.heelBreakPma.attr('i1[1]')
            self.ankleIk_obj.ctrl.attr( 'toeHeelRoll' ) >> self.toeBreakPma.attr('i1[1]')

            self.ankleIk_obj.ctrl.attr( 'heelTwist' ) >> self.heelTwistPivGrp.attr('rz')
            self.ankleIk_obj.ctrl.attr( 'toeTwist' ) >> self.toeTwistPivGrp.attr('rz')
            self.ankleIk_obj.ctrl.attr( 'toeBend' ) >> self.bendPivGrp.attr('rx')
            self.ankleIk_obj.ctrl.attr( 'toeSide' ) >> self.bendPivGrp.attr('rz')
            self.ankleIk_obj.ctrl.attr( 'toeSwirl' ) >> self.bendPivGrp.attr('ry')

            # create smart control
            self.smartToeCtrl = rt.createCtrl( 'Foot{}SmartIk{}Ctrl'.format( elem , side ) , 'cube' , 'red' , jnt = 0 )
            self.smartToeZro = rt.addGrp( self.smartToeCtrl )
            self.smartToeOfst = rt.addGrp( self.smartToeCtrl , 'Ofst' )
            self.smartToeCtrl.scaleShape(size)
            
            self.smartToeZro.snapOrient(self.ballFk_obj.jnt)
            if side =='_R_':
                self.smartToeZro.attr('sx').v = valueSide
                self.smartToeZro.attr('sy').v = valueSide
            self.smartToeCtrl.attr('tz').lock
            self.smartToeCtrl.attr('v').lock

            for ax in 'xyz':
                self.smartToeCtrl.attr('r{}'.format(ax)).lock
                self.smartToeCtrl.attr('s{}'.format(ax)).lock

            # create math utility node for smart toe system
            self.smartAmp_mdv = cn.createNode('multiplyDivide', 'Foot{}SmartIkAmp{}Mdv'.format( elem , side ) )
            self.smartToeCtrl.getShape().addAttr('footSmartAmp',k=1,dv=30)

            self.ball_pma =  cn.createNode( 'plusMinusAverage' , 'Ball{}SmartIk{}Pma'.format( elem , side ))
            self.footRock_pma = cn.createNode( 'plusMinusAverage' , 'FootRock{}SmartIk{}Pma'.format( elem , side ))
            self.smart_mdv = cn.createNode( 'multiplyDivide' , 'Foot{}SmartIk{}Mdv'.format( elem , side ))

            # do smart toe rock
            # connect smart control to smart mdv
            self.smartToeCtrl.attr('ty') >> self.smart_mdv.attr('i1x')
            self.smartToeCtrlShape = core.Dag(self.smartToeCtrl.shape)
            # set smart mdv attr
            self.smartToeCtrlShape.attr('footSmartAmp') >> self.smartAmp_mdv.attr('i1x')
            self.smartAmp_mdv.attr('i2x').v = 1
            self.smartAmp_mdv.attr('ox') >> self.smart_mdv.attr('i2x')
            
            # connect to smart ball
            self.smart_mdv.attr('outputX') >>  self.ball_pma.attr('input1D[0]')
            self.ankleIk_obj.ctrl.attr('ballRoll') >>  self.ball_pma.attr('input1D[1]')

            # create ball utility pma
            self.ball_mdv = cn.createNode( 'multiplyDivide' , 'Ball{}SmartIk{}Mdv'.format( elem , side ))
            self.ball_pma.attr('output1D') >>  self.ball_mdv.attr('input1X')
            self.ball_mdv.attr('input2X').v = -1

            # connect to ballPivGrp and Toe Break system
            self.ball_mdv.attr('outputX')  >> self.ballPivGrp.attr('rx')
            self.ball_mdv.attr('outputX')  >> self.heelBreakPma.attr('input1D[2]')
            self.ball_mdv.attr('outputX')  >> self.toeBreakAmpPma.attr('input1D[1]')
            self.ball_mdv.attr('outputX')  >> self.toeBreakCnd.attr ('secondTerm')

            # do smart foot rock
            #connect tx of smart ctrl to footRock_pma
            self.smartToeCtrl.getShape().attr('footSmartAmp') >> self.smartAmp_mdv.attr('i1y')
            self.smartAmp_mdv.attr('i2y').v = 1

            self.smartAmp_mdv.attr('oy') >> self.smart_mdv.attr('i2y')
            self.smartToeCtrl.attr('tx') >>  self.footRock_pma.attr('input1D[0]')
            self.ankleIk_obj.ctrl.attr('footRock') >>  self.footRock_pma.attr('input1D[1]')
            self.footRock_pma.attr('output1D') >> self.smart_mdv.attr('input1Y')
            self.smart_mdv.attr('outputY') >>  self.inPivGrp.attr('ry')
            self.smart_mdv.attr('outputY') >>  self.outPivGrp.attr('ry')

            mc.transformLimits( self.inPivGrp , ery = ( True , False ) , ry = ( 0 , 0 ))
            mc.transformLimits( self.outPivGrp , ery = ( False , True ) , ry = ( 0 , 0 ))

            mc.transformLimits( self.toePivGrp , erx = ( False , True ) , rx = ( 0 , 0 ))
            mc.transformLimits( self.heelPivGrp , erx = ( True , False ) , rx = ( 0 , 0 ))

            self.smartToeZro.snapPoint(smartIkTmpJnt)

            self.smartToeZro.parent(self.ankleIk_obj.gmbl)

        #-- Adjust Hierarchy Ik
        mc.parent( self.legIkCtrlGrp , self.legCtrlGrp )
        mc.parent( self.legIkJntGrp , self.legJntGrp )
        mc.parent( self.legIkIkhGrp , ikhGrp )
        mc.parent( self.upLegIk_obj.ctrlGrp , self.ikStrt[0] , self.crv , self.legIkCtrlGrp )
        mc.parent( self.ankleIk_obj.ctrlGrp , self.kneeIkZro , self.upLegIk_obj.gmbl )
        mc.parent( self.ankleIkhZro , self.legIkIkhGrp )

        if foot == True :
            mc.parent( self.footPivGrp , self.ankleIk_obj.gmbl )
            mc.parent( self.ballIkhZro , self.toeIkhZro , self.legIkIkhGrp )

        #-- Blending Fk Ik 
        rt.blendFkIk( self.legCtrl , self.upLegFk_obj.jnt , self.upLegIk_obj.jnt , self.upLegJnt )
        rt.blendFkIk( self.legCtrl , self.lowLegFk_obj.jnt , self.lowLegIkJnt , self.lowLegJnt )
        rt.blendFkIk( self.legCtrl , self.ankleFk_obj.jnt , self.ankleIk_obj.jnt , self.ankleJnt )
        
        if foot == True :
            rt.blendFkIk( self.legCtrl , self.ballFk_obj.jnt , self.ballIkJnt , self.ballJnt )
            rt.blendFkIk( self.legCtrl , self.toeFkJnt , self.toeIkJnt , self.toeJnt )
        
        if mc.objExists( 'Leg{}FkIk{}Rev'.format( elem , side )) :
            self.revFkIk = core.Dag('Leg{}FkIk{}Rev'.format( elem , side ))

            self.legCtrl.attr( 'fkIk' ) >> self.legIkCtrlGrp.attr('v')
            self.revFkIk.attr( 'ox' ) >> self.legFkCtrlGrp.attr('v')

            self.legCtrl.attr('fkIk').value = 1

        #-- Scale Foot
        if foot == True :
            sclAnkle = core.Dag(mc.scaleConstraint( self.ankleFk_obj.jnt , self.ankleIk_obj.jnt , self.ankleJnt , mo = 0 )[0])
            sclBall = core.Dag(mc.scaleConstraint( self.ballFk_obj.jnt , self.ballIkJnt , self.ballJnt , mo = 0 )[0])
            sclToe = core.Dag(mc.scaleConstraint( self.toeFkJnt , self.toeIkJnt , self.toeJnt , mo = 0 )[0])
            
            self.legCtrl.attr('fkIk') >> sclAnkle.attr('{}W1'.format(self.ankleIk_obj.jnt))
            self.revFkIk.attr('ox') >> sclAnkle.attr('{}W0'.format(self.ankleFk_obj.jnt))
            self.legCtrl.attr('fkIk') >> sclBall.attr('{}W1'.format(self.ballIkJnt))
            self.revFkIk.attr('ox') >> sclBall.attr('{}W0'.format(self.ballFk_obj.jnt))
            self.legCtrl.attr('fkIk') >> sclToe.attr('{}W1'.format(self.toeIkJnt))
            self.revFkIk.attr('ox') >> sclToe.attr('{}W0'.format(self.toeFkJnt))

            self.legCtrl.addAttr('footScale')
            self.legCtrl.attr('footScale').value = 1
            self.ankleJnt.attr('segmentScaleCompensate').value = 0
            self.ballJnt.attr('segmentScaleCompensate').value = 0
            self.ballFk_obj.jnt.attr('segmentScaleCompensate').value = 0
            self.ballIkJnt.attr('segmentScaleCompensate').value = 0
            self.toeJnt.attr('segmentScaleCompensate').value = 0
            self.toeFkJnt.attr('segmentScaleCompensate').value = 0
            self.toeIkJnt.attr('segmentScaleCompensate').value = 0

            self.legCtrl.attr('footScale') >> self.ballFkSclOfst.attr('sx')
            self.legCtrl.attr('footScale') >> self.ballFkSclOfst.attr('sy')
            self.legCtrl.attr('footScale') >> self.ballFkSclOfst.attr('sz')

            self.legCtrl.attr('footScale') >> self.ankleFk_obj.jnt.attr('sx')
            self.legCtrl.attr('footScale') >> self.ankleFk_obj.jnt.attr('sy')
            self.legCtrl.attr('footScale') >> self.ankleFk_obj.jnt.attr('sz')

            self.legCtrl.attr('footScale') >> self.footPivGrp.attr('sx')
            self.legCtrl.attr('footScale') >> self.footPivGrp.attr('sy')
            self.legCtrl.attr('footScale') >> self.footPivGrp.attr('sz')

            self.legCtrl.attr('footScale') >> self.ankleIk_obj.jnt.attr('sx')
            self.legCtrl.attr('footScale') >> self.ankleIk_obj.jnt.attr('sy')
            self.legCtrl.attr('footScale') >> self.ankleIk_obj.jnt.attr('sz')

        #-- Cleanup Fk
        for grp in ( self.legFkCtrlGrp , self.legFkJntGrp , self.upLegFk_obj.zro , self.upLegFk_obj.ofst , self.lowLegFk_obj.zro , self.lowLegFk_obj.ofst , self.ankleFk_obj.zro , self.ankleFk_obj.ofst ) :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( self.upLegFk_obj.ctrl , self.upLegFk_obj.gmbl , self.lowLegFk_obj.ctrl , self.lowLegFk_obj.gmbl , self.ankleFk_obj.ctrl , self.ankleFk_obj.gmbl ) :
            ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )
            
        if foot == True :
            for grp in ( self.ballFk_obj.zro , self.ballFk_obj.ofst , self.ballFkSclOfst ) :
                grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
                
            for ctrl in ( self.ballFk_obj.ctrl , self.ballFk_obj.gmbl ) :
                ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        #-- Cleanup Ik
        for grp in ( self.legIkCtrlGrp , self.legIkJntGrp , self.legIkIkhGrp , self.upLegIk_obj.zro , self.ankleIk_obj.zro ) :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for ctrl in ( self.upLegIk_obj.ctrl , self.upLegIk_obj.gmbl , self.kneeIkCtrl , self.ankleIk_obj.ctrl , self.ankleIk_obj.gmbl ) :
            ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )
            
        if foot == True :
            for grp in ( self.footPivGrp , self.ballPivGrp , self.bendPivGrp , self.heelPivGrp , self.heelTwistPivGrp , self.toePivGrp , self.toeTwistPivGrp , self.inPivGrp , self.outPivGrp ) :
                grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        #-- Ribbon
        if ribbon == True :

            #-- Create Controls Ribbon
            self.legRbn_obj = fkRig.Run(name       = 'Leg{}Rbn'.format(elem) ,
                                    side        = sideRbn ,
                                    tmpJnt      = self.lowLegJnt ,
                                    ctrlGrp     = ctrlGrp , 
                                    shape       = 'locator' ,
                                    jnt_ctrl    = 1 ,
                                    color       = 'yellow',
                                    localWorld  = 0,
                                    constraint  = 0,
                                    jnt         = 0,
                                    dtl         = 0)
            mc.parent( self.legRbn_obj.ctrlGrp , self.legCtrlGrp )
            self.lowLegJnt.parent(self.upLegJnt)

            #-- Adjust Shape Controls Ribbon
            self.legRbn_obj.ctrl.scaleShape( size )

            #-- Rig process Ribbon
            mc.parentConstraint( self.lowLegJnt , self.legRbn_obj.zro , mo = True )

            distRbnUpLeg = rt.distance( self.upLegJnt , self.lowLegJnt )
            distRbnLowLeg = rt.distance( self.lowLegJnt , self.ankleJnt )

            try:
                context = context_info.ContextPathInfo()
                project = context.project
            except:
                project = ''
                
            if project == 'CA':
                if hi:
                    self.rbnUpLeg = ribbonRig.RibbonHi(  name = 'UpLeg{}'.format(elem) , 
                                                         axis = axis ,
                                                         side = sideRbn ,
                                                         dist = distRbnUpLeg )
                    
                    self.rbnLowLeg = ribbonRig.RibbonHi(  name = 'LowLeg{}'.format(elem) , 
                                                          axis = axis ,
                                                          side = sideRbn ,
                                                          dist = distRbnLowLeg )

                else:
                    self.rbnUpLeg = ribbonRig.RibbonLow(name   = 'UpLeg{}'.format(elem) ,
                                                        axis     = axis ,
                                                        side   = sideRbn ,
                                                        dist   = distRbnUpLeg )
                    
                    self.rbnLowLeg = ribbonRig.RibbonLow(name   = 'LowLeg{}'.format(elem) ,
                                                        axis     = axis ,
                                                        side   = sideRbn ,
                                                        dist   = distRbnLowLeg )
            else:
                if hi:
                    self.rbnUpLeg = ribbonRig.RibbonHi(  name = 'UpLeg{}'.format(elem) , 
                                                         axis = 'y-' ,
                                                         side = sideRbn ,
                                                         dist = distRbnUpLeg )
                    
                    self.rbnLowLeg = ribbonRig.RibbonHi(  name = 'LowLeg{}'.format(elem) , 
                                                          axis = 'y-' ,
                                                          side = sideRbn ,
                                                          dist = distRbnLowLeg )

                else:
                    self.rbnUpLeg = ribbonRig.RibbonLow(name   = 'UpLeg{}'.format(elem) ,
                                                        axis     = 'y-' ,
                                                        side   = sideRbn ,
                                                        dist   = distRbnUpLeg )
                    
                    self.rbnLowLeg = ribbonRig.RibbonLow(name   = 'LowLeg{}'.format(elem) ,
                                                        axis     = 'y-' ,
                                                        side   = sideRbn ,
                                                        dist   = distRbnLowLeg )
                # self.rbnUpLeg = ribbonRig.RibbonHi(  name = 'UpLeg{}'.format(elem) , 
                #                                      axis = 'y-' ,
                #                                      side = sideRbn ,
                #                                      dist = distRbnUpLeg,
                #                                      numJnts = 1  )
                
                # self.rbnLowLeg = ribbonRig.RibbonHi(  name = 'LowLeg{}'.format(elem) , 
                #                                       axis = 'y-' ,
                #                                       side = sideRbn ,
                #                                       dist = distRbnLowLeg,
                #                                       numJnts = 1  )
            self.rbnUpLeg.rbnCtrlGrp.snapPoint(self.upLegJnt)

            
            if project == 'CA':

                mc.delete(mc.aimConstraint( self.lowLegJnt , self.rbnUpLeg.rbnCtrlGrp ,
                                            aim = aimVec ,
                                            u = upVec ,
                                            wut='objectrotation' ,
                                            wuo = self.upLegJnt ,
                                            wu= upVec))
            else:
                mc.delete(mc.aimConstraint( self.lowLegJnt , self.rbnUpLeg.rbnCtrlGrp ,
                                aim = (0,-1,0)  ,
                                u = upVec ,
                                wut='objectrotation' ,
                                wuo = self.upLegJnt ,
                                wu= (0,0,1)))


            mc.parentConstraint( self.upLegJnt , self.rbnUpLeg.rbnCtrlGrp , mo = True )
            mc.parentConstraint( self.upLegJnt , self.rbnUpLeg.rbnRootCtrl , mo = True )
            mc.parentConstraint( self.legRbn_obj.ctrl , self.rbnUpLeg.rbnEndCtrl , mo = True )

            for ctrl in ( self.rbnUpLeg.rbnRootCtrl , self.rbnUpLeg.rbnEndCtrl) :
                ctrl.lockHideAttrs('tx', 'ty', 'tz' , 'rx' , 'ry' , 'rz' )
                ctrl.hide()

            self.rbnLowLeg.rbnCtrlGrp.snapPoint(self.lowLegJnt)
            if project == 'CA':    
                mc.delete( 
                               mc.aimConstraint( self.ankleJnt , self.rbnLowLeg.rbnCtrlGrp , 
                                                 aim = aimVec,
                                                 u = upVec ,
                                                 wut='objectrotation' ,
                                                 wuo = self.lowLegJnt ,
                                                 wu= upVec)
                         )
            else:
                mc.delete( 
                               mc.aimConstraint( self.ankleJnt , self.rbnLowLeg.rbnCtrlGrp , 
                                                 aim = (0,-1,0)  ,
                                                 u = upVec ,
                                                 wut='objectrotation' ,
                                                 wuo = self.lowLegJnt ,
                                                 wu= (0,0,1))
                         )                    
            mc.parentConstraint( self.lowLegJnt , self.rbnLowLeg.rbnCtrlGrp , mo = True )
            mc.parentConstraint( self.legRbn_obj.ctrl , self.rbnLowLeg.rbnRootCtrl , mo = True )
            mc.parentConstraint( self.ankleJnt , self.rbnLowLeg.rbnEndCtrl , mo = True )

            for ctrl in ( self.rbnLowLeg.rbnRootCtrl , self.rbnLowLeg.rbnEndCtrl) :
                ctrl.lockHideAttrs('tx', 'ty', 'tz' , 'rx' , 'ry' , 'rz' )
                ctrl.hide()
    
            #-- Adjust Ribbon
            self.rbnUpLegCtrlShape = core.Dag(self.rbnUpLeg.rbnCtrl.shape)
            self.rbnLowLegCtrlShape = core.Dag(self.rbnLowLeg.rbnCtrl.shape)

            self.rbnUpLegCtrlShape.attr('rootTwistAmp').value = rootTwsAmp
            self.rbnUpLegCtrlShape.attr('endTwistAmp').value = endTwsAmp
            self.rbnLowLegCtrlShape.attr('rootTwistAmp').value = rootTwsAmp
            self.rbnLowLegCtrlShape.attr('endTwistAmp').value = endTwsAmp
                           
            #-- Twist Distribute Ribbon
            self.upLegNonRollTwistGrp.attr('ry') >> self.rbnUpLegCtrlShape.attr('rootAbsoluteTwist')
            #self.ankleJnt.attr('ry') >> self.rbnLowLegCtrlShape.attr('endAbsoluteTwist')
            self.ankleNonRollTwistGrp.attr('ry') >> self.rbnLowLegCtrlShape.attr('endAbsoluteTwist')
            
            #-- Adjust Hierarchy Ribbon
            mc.parent( self.rbnUpLeg.rbnCtrlGrp , self.rbnLowLeg.rbnCtrlGrp , self.legCtrlGrp )
            mc.parent( self.rbnUpLeg.rbnJntGrp , self.rbnLowLeg.rbnJntGrp , self.legJntGrp )
            mc.parent( self.rbnUpLeg.rbnSkinGrp , self.rbnLowLeg.rbnSkinGrp , skinGrp )
            mc.parent( self.rbnUpLeg.rbnStillGrp , self.rbnLowLeg.rbnStillGrp , stillGrp )
        
            self.rbnUpLeg.rbnCtrl.attr('autoTwist').value = 1
            self.rbnLowLeg.rbnCtrl.attr('autoTwist').value = 1        

               
            if kneePos: 
                kneePosLoc = cn.locator(name = 'KneePos{}{}Loc'.format(elem,side))
                kneePosGrp = cn.group(kneePosLoc)
                mc.parent(kneePosGrp, stillGrp)
                kneePosGrp.snap(kneePos)

                ## clear old Node and Attribute
                self.rbnLowLeg.rbnRootCtrl.clearConsLocal()
                self.rbnLowLeg.rbnRootCtrl.unlockHideAttrs('tx','ty','tz','rx','ry','rz')

                ## create Node 
                clampNode = cn.createNode ('clamp', 'KneePos{}{}Cmp'.format(elem,side))
                reverseNode = cn.createNode('reverse', 'KneePos{}{}Rev'.format(elem, side))
                multiANode = cn.createNode('multiplyDivide', 'KneePosA{}{}Mdv'.format(elem, side))
                multiAmpNode = cn.createNode('multiplyDivide', 'KneePosAmp{}{}Mdv'.format(elem, side))

                ## setAttr Node
                clampNode.attr('minR').v = -135
                clampNode.attr('maxR').v = -80
                multiANode.attr('operation').v = 2

                ## new parent
                if ribbon:
                    if hi:
                        mc.parentConstraint('UpLeg{}5RbnDtl{}Jnt'.format(elem,side), kneePosGrp, mo = True)
                    else:
                        mc.parentConstraint('UpLeg{}Rbn{}Jnt'.format(elem,side), kneePosGrp, mo = True)
                else:
                    mc.parentConstraint(self.upLegJnt, kneePosGrp, mo = True)

                consPos = core.Dag(mc.parentConstraint(self.legRbn_obj.ctrl, kneePosLoc, self.rbnLowLeg.rbnRootCtrl, mo = False)[0])
                ## addAttr kneePosAmp
                self.legRbnCtrl = core.Dag(self.legRbn_obj.ctrl)
                self.legRbnCtrl.addAttr('KneePosAmp' , -200 , 200)
                self.legRbnCtrl.attr('KneePosAmp').v = 0.5

                ## connect Attr
                self.lowLegJnt.attr('rx') >> clampNode.attr('inputR')
                clampNode.attr('outputR') >> multiANode.attr('i2x')
                self.lowLegJnt.attr('rx') >> multiANode.attr('i1x')
                self.legRbnCtrl.attr('KneePosAmp') >> multiAmpNode.attr('i2x')
                multiANode.attr('ox') >> multiAmpNode.attr('i1x')
                multiAmpNode.attr('ox') >> consPos.attr('{}W1'.format(kneePosLoc))
                multiAmpNode.attr('ox') >> reverseNode.attr('inputX')
                reverseNode.attr('ox') >> consPos.attr('Leg{}Rbn{}CtrlW0'.format(elem,side,elem, side))
                self.rbnLowLeg.rbnRootCtrl.lockHideAttrs('tx','ty','tz','rx','ry','rz')


        mc.select( cl = True )