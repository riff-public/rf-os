import maya.cmds as mc
import maya.mel as mm
import core
reload( core )
import rigTools as rt
reload( rt )

cn = core.Node()

def Run(    headJnt             = 'bodyRig_md:Head_Jnt' , 
            eyeJnts             = ['bodyRig_md:Eye_L_Jnt' , 'bodyRig_md:Eye_R_Jnt'] ,
            eyeBshJnts          = ['fclRig_md:EyeLidsBshRig_L_Jnt' , 'fclRig_md:EyeLidsBshRig_R_Jnt'] ,
            eyeCtrls            = ['bodyRig_md:Eye_L_Ctrl' , 'bodyRig_md:Eye_R_Ctrl'],
            mouthDtlCtrls       = ['fclRig_md:MouthUprBshDtlRig_C_Ctrl', 'fclRig_md:MouthLwrBshDtlRig_C_Ctrl', 'fclRig_md:MouthCnrBshDtlRig_L_Ctrl', 'fclRig_md:MouthCnrBshDtlRig_R_Ctrl'],
            mouthBshGrp         = 'fclRig_md:MouthBshRigCtrlZro_Grp' ,
            jawLwr2Jnt          = 'bodyRig_md:JawLwr2_Jnt' ,
            jawUprJnt           = 'bodyRig_md:JawUpr1_Jnt' ):

    if not mc.objExists(eyeBshJnts[0]):
        print 'This scene does not have EyeLidBsh joints.'
        return

    for i , jnt in enumerate(eyeJnts):
        fullName = jnt.split(':')[-1]
        elem = fullName.split('_')

        if len(elem) > 2:
            name = elem[0]
            side = '_{}_'.format(elem[-1])
        else:
            name = elem[0]
            side = '_'

        rev = cn.createNode('reverse' , '{}{}Rev'.format(name , side))
        parCon = mc.parentConstraint(jnt , headJnt , eyeBshJnts[i] , mo=1)[0]
        mc.connectAttr('{}.eyelidsFollow'.format(eyeCtrls[i]) , '{}.input.inputX'.format(rev))
        mc.connectAttr('{}.eyelidsFollow'.format(eyeCtrls[i]) , '{}.{}W0'.format(parCon , fullName))
        mc.connectAttr('{}.outputX'.format(rev) , '{}.{}W1'.format(parCon , headJnt.split(':')[-1]))
        mc.setAttr('{}.eyelidsFollow'.format(eyeCtrls[i]) , 1 )

        #parentConStraint Mouth Bsh Ctrl
        mc.parentConstraint(jawUprJnt, jawLwr2Jnt, mouthBshGrp, mo =True)
        nameGrp = mouthBshGrp.split(':')[1]
        mc.setAttr('{}_parentConstraint1.interpType'.format(nameGrp), 2)

    if not mc.objExists(mouthDtlCtrls[0]):
        print 'This scene does not have mouthDtlCtrl for Parent BshCtrl.'
        return

    else:
        for each in mouthDtlCtrls:
            namespc,nameeach = each.split(':')
            name = nameeach.split('Dtl')
            nameCtrl = name[0]+name[1]
            nameSplit = nameCtrl.split('_')
            if len(nameSplit) == 2:
                ctrlZro = '{}:{}CtrlZro_Grp'.format(namespc,nameSplit[0])
            elif len(nameSplit) == 3:
                nameZro =  nameSplit[0]
                if nameSplit[1] == 'C':
                    side = '_'
                else:
                    side = '_{}_'.format(nameSplit[1])  
                
                nameCtrlZro = '{}CtrlZro{}Grp'.format(nameZro,side)
                ctrlZro = '{}:{}'.format(namespc,nameCtrlZro)

            if not 'Lwr' in ctrlZro:
                mc.parentConstraint(each, ctrlZro, mo =True)
                mc.setAttr('{}_parentConstraint1.interpType'.format(nameCtrlZro), 2)
            else:
                mc.parentConstraint(each, ctrlZro, mo =True)