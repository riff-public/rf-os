import os

import maya.cmds as mc
from rf_utils.context import context_info
reload(context_info)
from rf_maya.rftool.rig.rigScript import copyObjFile
reload(copyObjFile)
from rf_utils import file_utils

#path project config
rootScript = os.environ['RFSCRIPT']
pathProjectJson = r"{}\core\rf_maya\rftool\rig\export_config\project_config.json".format(rootScript)
projectData = file_utils.json_loader(pathProjectJson)


# entity = context_info.ContextPathInfo()

CTRL_GRP = 'FclRigCtrl_Grp'
JNT_GRP = 'FclRigJnt_Grp'
SKIN_GRP = 'FclRigSkin_Grp'
STILL_GRP = 'FclRigStill_Grp'
RVT_GRP = 'FclRigRvt_Grp'

HD_LOW_GRP = 'HdLowFclRigSpc_Grp'
HD_MID_GRP = 'HdMidFclRigSpc_Grp'
HD_HI_GRP = 'HdHiFclRigSpc_Grp'
def refElem(elem=''):
    # from rf_utils.context import context_info
    # asset = context_info.ContextPathInfo() 
    # procName = asset.process
    # folder = copyObjFile.getDataFld(subPath = '')
    # print folder, '..folder'


    asset = context_info.ContextPathInfo() 
    scnLoc = os.path.normpath(mc.file(q=True, l=True)[0])
    pu = os.path.pub(mc.file(q=True, l=True)[0])
    # print pu, '...pu..'
    fldPath, flNmExt = os.path.split(scnLoc)
    # print fldPath, '..fldPath'
    # print flNmExt, '...flNmExt..'
    assetPath, taskFld = os.path.split(fldPath)
    catPath, assetNm = os.path.split(assetPath)
    namePath, nameCha = os.path.split(catPath)
    charPath, char = os.path.split(namePath)

    heroPath = "hero"
    fldHePath = "%s/%s" % (assetPath , heroPath)
    # print assetPath, '...assetPath'
    # print fldHePath, '...fldHePath..'
    projectName = asset.project if mc.file(q=True, loc=True) else ''

    # asset.context.update(process = elem)

    # if projectName in projectData['project']:
    #     dataPath = asset.path.output_hero()
    #     fldHePath = '%s/' % dataPath
    #     if elem == 'bsh':
    #         nameCha = 'mdl'
    #         elem = 'modelBsh'
    #         reMod = fldHePath.replace('rig', 'model')
    #         fldHePath = reMod.replace('bsh', 'modelBsh')

    #     elemPath = os.path.join(fldHePath,'%s_%s_%s_md.hero.ma' % (char, nameCha, elem))
    #     mc.file(elemPath, r=True, ns=elem)

    # else:
    #     elemPath = os.path.join(fldHePath,'%s_%s_%s.ma' % (nameCha, assetNm, elem))
    #     mc.file(elemPath, r=True, ns=elem)

def refElemStep(stepDep = '', elem='' ):

    ## Check Path Fire is Ready
    path = mc.file(q = True, loc = True)
    # if path:
    #     if 'Oa' in (path.split('/')[5]):
    #         print path.split('/')[5]
    #     else:
    #         return
    asset = context_info.ContextPathInfo() 
    charName = asset.name
    if 'Oa' in charName:
        print charName
    elif 'OA' in charName:
        print charName
    elif asset.type == 'prop':
        print asset.type , '---' , asset.name
    else:
            return

    asset = context_info.ContextPathInfo(path = path) 
    procName = asset.process
    char = asset.name
    stepDepOld = asset.step
    ## Check stepDep
    if not stepDep:
        stepDep = stepDepOld

    ## Check Project
    projectName = asset.project if mc.file(q=True, loc=True) else ''
    dataPath = asset.path.hero()
    fldPath, hero = os.path.split(dataPath)

    ## Hero Path
    fldHePath = '{}/{}/{}/hero/output'.format(fldPath, stepDep, elem)
    if projectName in projectData:
        if elem == 'bsh':
            nameCha = 'mdl'
            elem = 'modelBsh'
            reMod = fldHePath.replace('rig', 'model')
            fldHePath = reMod.replace('bsh', 'modelBsh')
        else:
            elemPath = os.path.join(fldHePath,'{}_{}_{}_md.hero.ma'.format(char, stepDep, elem))    

        mc.file(elemPath, r = True, ns = elem + '_md')

    return fldHePath

def refElemStepBatch(char = '', stepDep = '', elem='', path = ''):

    ## Check Path Fire is Ready
    if path:
        path = path
    else:
        path = mc.file(q = True, loc = True)

    ## Check Project
    asset = context_info.ContextPathInfo(path = path) 
    projectName = asset.project if mc.file(q=True, loc=True) else ''

    if projectName in projectData['project']:
        asset = context_info.ContextPathInfo(path = path) 
        procName = asset.process
        charSp = asset.name
        dataPath = asset.path.hero()
        fldPath, hero = os.path.split(dataPath)

        if char:
            newPath, flNmExt = os.path.split(fldPath)
            fldPath = '{}/{}'.format(newPath, char)

        else:
            char = charSp

        if elem in ['bodyRig', 'main']:
            fldFile = 'maya'
        else:
            fldFile = 'output'

        ## Check Project
        fldPath = '{}/{}/{}/hero/{}'.format(fldPath, stepDep, elem, fldFile)
        nameHero = '{}_{}_{}_md.hero.ma'.format(char, stepDep, elem)
    else:
        pathGuide = '$RFPUBL/SevenChickMovie/asset/publ/char'
        fldPath = '{}/{}/{}/{}/hero/output'.format(pathGuide, char, stepDep, elem)
        nameHero = '{}_{}_{}_md.hero.ma'.format(char, stepDep, elem)

    ## Hero Path
    fldHePath = '{}/{}'.format(fldPath, nameHero)
    elemPath = os.path.join(fldHePath)  

    if elem in ['bodyRig', 'main']:
        mc.file(elemPath, o = True, type='mayaAscii')
    else:
        mc.file(elemPath, r = True, type='mayaAscii', ns = elem + '_md')

    return fldHePath

def entityFirst(*args):
    entityFirst = 'P:/SevenChickMovie/asset/publ/char'
    return entityFirst


def refElemPath(path = "", elem = ""):
    # Eyebrow Temp
    fullPath = mc.file( "%s/%s.ma" % (path, elem), r = True , ns = elem)
    return fullPath
    
def removeSelectedReference(sels= []):
    # Remove selected reference from the scene
    editSelectedReference(opr='remove', sels = sels)

def importSelectedReference(sels= []):
    # Import selected reference from the scene
    editSelectedReference(opr='import', sels = sels)

def reloadSelectedReference(sels= []):
    # Reload selected reference from the scene
    editSelectedReference(opr='reload', sels = sels)

def editSelectedReference(opr='reload', sels=[]):
   
    for sel in sels:
        
        if mc.objExists(sel) and mc.referenceQuery(sel, isNodeReferenced=True):
            
            refNode = mc.referenceQuery(sel, referenceNode=True, topReference=True)
            fileName = mc.referenceQuery(refNode, filename=True)

            if opr == 'reload ':
                mc.file(fileName, lr=True)
            elif opr == 'remove':
                mc.file(fileName, rr=True)
            elif opr == 'import':
                mc.file(fileName, i=True)




