import maya.cmds as mc
import maya.mel as mm
import core
reload( core )
import rigTools as rt
reload( rt )

#-- build generate jnt and axis ui

class Run(object):
    def __init__(self):
        self.winName = 'generateJntAndAxisUi'

    def show(self , *args):
        # check if window exists
        if mc.window ( self.winName , exists = True ) :
            mc.deleteUI ( self.winName ) 
        else : pass

        winWidth = 200
        mc.window(self.winName,title = 'Generate Joint And Axis UI' , w = winWidth , s=False, mnb=True, mxb=False)
        mc.columnLayout()
        mc.rowLayout(h=10)
        mc.setParent('..')

        mc.rowLayout(nc=4 , cw4=[winWidth*0.05,winWidth*0.3, winWidth*0.6,winWidth*0.05])
        mc.text(l = '')
        mc.text(l = 'Jnt Size', align='center' , w=winWidth*0.3)
        mc.floatField('jntSize' , v=0.1 ,ec='enter', ed = True , w=winWidth*0.6)
        mc.text(l = '')
        mc.setParent('..')

        mc.rowLayout(h=10)
        mc.setParent('..')

        mc.rowLayout(nc=3 , cw3=[winWidth*0.05,winWidth*0.9,winWidth*0.05])
        mc.text(l = '')
        mc.button('createJnt',l='Create Joint' , h = 40 , w=winWidth*0.9 , al='center' , c = self.createJnt_cmd)
        mc.text(l = '')
        mc.setParent('..')

        mc.rowLayout(h=10)
        mc.setParent('..')

        mc.rowLayout(nc=6 , cw6=[winWidth*0.05,winWidth*0.3,winWidth*0.025,winWidth*0.2,winWidth*0.025,winWidth*0.3])
        mc.text(l = '')
        mc.textField('jntName', text = 'Name' , ed = True , w=winWidth*0.3)
        mc.text(l = '')
        mc.textField('jntSide', text = 'L' , ed = True ,w=winWidth*0.2)
        mc.text(l = '')
        mc.textField('jntSuffix', text = 'TmpJnt' , ed = True,w=winWidth*0.3)
        mc.setParent('..')
        mc.rowLayout(nc=6 , cw6=[winWidth*0.05,winWidth*0.3,winWidth*0.025,winWidth*0.2,winWidth*0.025,winWidth*0.3])
        mc.text(l = '')
        mc.text(l='Prefix' , align='center' ,w=winWidth*0.3)
        mc.text(l='')
        mc.text(l='Side' , align='center' ,w=winWidth*0.2)
        mc.text(l='')
        mc.text(l='Suffix' , align='center' ,w=winWidth*0.3)
        mc.setParent('..')

        mc.rowLayout(h=10)
        mc.setParent('..')

        mc.rowLayout(nc=3 , cw3=[winWidth*0.05,winWidth*0.9,winWidth*0.05])
        mc.text(l = '')
        mc.button('run',l='Parent Joint and Aim Axis' , h = 40 , w=winWidth*0.9 , al='center' , c = self.run_cmd)
        mc.text(l = '')
        mc.setParent('..')

        mc.rowLayout(h=10)
        mc.setParent('..')
        mc.showWindow()

    def createJnt_cmd(self , *args):
        value = mc.floatField('jntSize' , q=True , v=True)
        jnt = rt.createJointOnPosition(obj='')
        jnt.attr('radius').v = value

    def run_cmd(self , *args):
        name = mc.textField('jntName' , q=True , tx=True)
        side = mc.textField('jntSide' , q=True , tx=True)
        suffix = mc.textField('jntSuffix' , q=True , tx=True)
        if side:
            side = '_{}_'.format(side)
        else:
            side = '_'

        obj = mc.ls(sl=True)
        mc.createNode('transform',n='a1')
        mc.delete(mc.aimConstraint(obj[-2] , obj[-1], aim = (0, -1, 0)))
        mc.delete('a1')

        for i in range(1,len(obj)):
            print obj[i] ,'-' , obj[i-1]
            mc.delete(mc.aimConstraint(obj[i] , obj[i-1], aim = (0, 1, 0)))
            mc.parent(obj[i] , obj[i-1])

        for i in range(len(obj)):
            mc.rename(obj[i] , '{}{}{}{}'.format(name , i+1 , side , suffix))