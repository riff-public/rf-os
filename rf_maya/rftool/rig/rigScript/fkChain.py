import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
import core
import re
import rigTools as rt
import ctrlRig
import fkRig

reload(fkRig)
reload( core )
reload( rt )
reload(ctrlRig)
# -------------------------------------------------------------------------------------------------------------
#
#  FK RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------
cn = core.Node()

class Run( object ):

    def __init__( self , name       = '' ,
                         side       = '' ,
                         tmpJnt     = [ ] ,
                         parent     = '' ,
                         skinGrp    = '' ,
                         ctrlGrp    = 'Ctrl_Grp' , 
                         axis       = 'y' ,
                         shape      = 'square' ,
                         jnt_ctrl   = 0 ,
                         size       = 1 ,
                         no_tip     = 0,
                         color      = 'red',
                         localWorld = 1,
                         defaultWorld = 0,
                         dtl = 0,
                         dtlShape = 'cube',
                         dtlColor = 'blue',
                         stretch    = 1,
                         squash     = 1,
                         inv_stretch = 0,
                         scalable = 0,
                         **kwargs
                 ):

        ##-- Info
        self.name = name
        self.side = side
        if side == '' :
            side = '_'
        else :
            side = '_{}_'.format(side)

        if tmpJnt:
            tmpJnt = tmpJnt
        else:
            search_str = '*{}*{}*TmpJnt'.format(name,side)
            found_list = mc.ls(search_str)
            tmpJnt = []
            for found in found_list:
                match_obj = re.search('{}[0-9]+{}TmpJnt'.format(name,side),found)
                if match_obj:
                    tmpJnt.append(found)    
        
        if axis == 'x':
            axisSq = ( 'sy' , 'sz'  )
        elif axis == 'y':
            axisSq = ( 'sx' , 'sz'  )
        elif axis == 'z':
            axisSq = ( 'sx' , 'sy'  )

        length = len(tmpJnt) - no_tip
        self.ctrlObjArray = []
        self.ctrlGrpArray = []
        self.ctrlArray = []
        self.gmblArray = []
        self.zroArray = []
        self.ofstArray = []
        self.parsArray = []
        self.jntArray = []
        self.dtlArray = []
        self.jointChain = []
        self.locWorDict = {}
        # add on group
        self.drvArray = []
        self.invArray = []

        #-- Create Main Group
        self.ctrlGrp = cn.createNode( 'transform' , '{}Ctrl{}Grp'.format( name , side ))
        self.ctrlGrp.parent(ctrlGrp)

        if mc.objExists(parent):
            mc.parentConstraint( parent , self.ctrlGrp , mo = False )
            mc.scaleConstraint( parent , self.ctrlGrp , mo = False )
        
        #-- Rig process
        # -- Joint

        for i in range(len(tmpJnt)):
            new_jnt = cn.joint('{}{}{}Jnt'.format(self.name,i+1,side), tmpJnt[i])
            self.jntArray.append(new_jnt)

        # set jnt hierarchy
        for i in range(len(self.jntArray)-1):
            self.jntArray[i+1].parent(self.jntArray[i])

        for i in range(length) :
            if not i == length:
                #-- Controls
                if i ==0:
                    localWorldVal = localWorld
                    defaultLWVal = defaultWorld
                    parent_val = parent
                    ctrlGrp_val = self.ctrlGrp
                else:
                    localWorldVal = 0
                    defaultLWVal = 0 
                    parent_val = ''
                    ctrlGrp_val = ''

                fkRig_obj = fkRig.Run(  name        = '{}{}'.format(self.name,i+1),
                                        side        = self.side,
                                        tmpJnt      = self.jntArray[i],
                                        parent      = parent_val,
                                        ctrlGrp     = ctrlGrp ,
                                        skinGrp     = skinGrp ,
                                        shape       = shape ,
                                        jnt_ctrl    = jnt_ctrl,
                                        size        = size ,
                                        color       = color,
                                        localWorld  = localWorldVal,
                                        defaultWold = defaultLWVal,
                                        jnt         = 0,
                                        constraint  = 0,
                                        dtl = dtl,
                                        dtlShape = dtlShape,
                                        dtlColor = dtlColor,
                                        **kwargs)
                self.jointChain.append(fkRig_obj.jnt)
                self.ctrlArray.append(fkRig_obj.ctrl)
                self.gmblArray.append(fkRig_obj.gmbl)
                self.zroArray.append(fkRig_obj.zro)
                self.ofstArray.append(fkRig_obj.ofst)
                self.parsArray.append(fkRig_obj.pars)
                self.ctrlObjArray.append(fkRig_obj.ctrl_obj)
                self.ctrlGrpArray.append(fkRig_obj.ctrlGrp)
                if i ==0:
                    self.locWorDict = fkRig_obj.locWor

                if dtl:
                    self.dtlArray.append(fkRig_obj.dtl_obj)
                
                if fkRig_obj.drv:
                    self.drvArray.append(fkRig_obj.drv)

                if fkRig_obj.inv:
                    self.invArray.append(fkRig_obj.inv)
                        
                
                if not i == length and i != 0:
                    if self.gmblArray[i-1]:
                        fkRig_obj.ctrlGrp.parent( self.gmblArray[i-1] )
                    else:
                        fkRig_obj.ctrlGrp.parent( self.ctrlArray[i-1])

                ## joint Chain
                # if fkRig_obj.jnt:
                #     if not i == length:
                #         if not i ==0:
                #             mc.parent(fkRig_obj.jnt, self.jntArray[i-1])



        #-- Add Stretch
        if stretch:
            for i in range(length):
                if not i == 0 :
                    if not i > length :
                        if not inv_stretch:
                            rt.addFkStretch( self.ctrlArray[i-1], self.ofstArray[i] , axis )
                        else:
                            rt.addFkStretch( self.ctrlArray[i-1], self.ofstArray[i] , axis ,-.1)
        if no_tip:
            if not inv_stretch:
                rt.addFkStretch( self.ctrlArray[-(no_tip)], self.jntArray[-(no_tip)] , axis )
            else:
                rt.addFkStretch( self.ctrlArray[-(no_tip)], self.jntArray[-(no_tip)] , axis ,-.1)
            


        #-- Add Squash
        if squash:
            for i in range(length):
                if not i == length :
                    pma,mdv = rt.addSquash( self.ctrlArray[i], self.jntArray[i] , ax = axisSq)
                if scalable:
                    mdv.attr('ox') >> pma.attr('i3[0].i3{}'.format(axisSq[0][1]))
                    mdv.attr('ox') >> pma.attr('i3[0].i3{}'.format(axisSq[1][1]))
                    self.ctrlArray[i].attr(axisSq[0]) >> pma.attr('i3[1].i3{}'.format(axisSq[0][1]))
                    self.ctrlArray[i].attr(axisSq[1]) >> pma.attr('i3[1].i3{}'.format(axisSq[1][1]))
                    self.ctrlArray[i].attr('s'+axis) >> pma.attr('i3[1].i3{}'.format(axis))
                    pma.attr('o3{}'.format(axisSq[0][1])) >> self.jntArray[i].attr('s'+axisSq[0][1])
                    pma.attr('o3{}'.format(axisSq[1][1])) >> self.jntArray[i].attr('s'+axisSq[1][1])

        if dtl:
            for dtl_obj in self.dtlArray:
                dtl_obj.do_constraint('parent')
                dtl_obj.ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' )
        else:
            for ctrl_obj in self.ctrlObjArray:
                ctrl_obj.do_constraint('parent')
            
        #-- Cleanup
        for ctrl_obj in self.ctrlObjArray :
            if not scalable:
                ctrl_obj.ctrl.lockHideAttrs('sx' , 'sy' , 'sz')
            else:
                pass

        for zro in self.zroArray :
            zro.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for ofst in self.ofstArray :
            ofst.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for jnt in self.jntArray :
            jnt.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for ctrlGrp in self.ctrlGrpArray :
            ctrlGrp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        if parent:
            pm.parentConstraint(parent,self.ctrlGrp,mo=1)

        self.ctrlGrpArray[0].parent(self.ctrlGrp)
