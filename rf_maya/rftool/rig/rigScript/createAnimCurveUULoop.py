k =mc.createNode('animCurveUU')
mc.setKeyframe(k,f= 0.0,itt='linear',ott = 'linear',v=0.0)
mc.setKeyframe(k,f= 100.0,v=1.0,itt='linear',ott = 'linear')
mc.setAttr(k+'.preInfinity',3)
mc.setAttr(k+'.postInfinity',3)
mc.connectAttr('Device_Ctrl.Roll',k+'.input')
mc.connectAttr(k+'.output','motionPath2.uValue',f=1)