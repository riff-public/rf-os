import maya.cmds as mc
import maya.mel as mm
import core
reload( core )
import rigTools as rt
reload( rt )

def Run(    wmkCtrlGrp      = 'fclRig_md:WnkRigCtrl_Grp' ,
            headJnt         = 'bodyRig_md:Head_Jnt' ):

    if not mc.objExists(wmkCtrlGrp):
        print 'This scene does not have WnkRig objects.'
        return

    #-- Info
    headJnt = core.Dag(mc.ls(headJnt)[0])

    get_midCtrl = []
    wrinkledGrp = mc.listRelatives(wmkCtrlGrp, c=True)

    for grp in wrinkledGrp:
        ctrlGrp = mc.listRelatives(grp, c=True)
        for sca in ctrlGrp:
            mc.scaleConstraint(headJnt, sca, mo=True)
            if "Mid" in sca:
                all_children = mc.listRelatives(sca, c=True, ad=True, typ="nurbsCurve")
                if not "Gmbl" in all_children:
                    midCtrl = mc.listRelatives(all_children, ap=True)[0]
                    get_midCtrl.append(midCtrl)

    wnkGeos = mc.listRelatives('*:WnkRigGeo_Grp', c=True)
    if wnkGeos:
        all_wnkGeoGrp = []; [all_wnkGeoGrp.append(grp) for grp in wnkGeos if "WnkRig" in grp]
        wnkGeoGrp = mc.listRelatives(all_wnkGeoGrp, c=True)
        for i in range(len(wnkGeos)):
            if "WnkRig" in wnkGeos[i]:

                wnkMeshs = mc.listRelatives(wnkGeos[i] , ad = True , type = 'mesh')
                wnkGeoList = []
                if wnkMeshs:
                    for eachMesh in wnkMeshs:
                        prnt = mc.listRelatives(eachMesh,p=True)[0]
                        if not prnt in wnkGeoList:
                            if prnt.endswith('_Geo'):
                                wnkGeoList.append(prnt)

                for eachGeo in wnkGeoList:
                    if not 'Base' in eachGeo:
                        geo = eachGeo.replace("WnkRig_", "_").replace("fclRig_md:", "bodyRig_md:")
                        rt.blendShape(eachGeo , geo)

                    mc.connectAttr(get_midCtrl[i] + ".wrinkledVis", geo + ".v")

    return