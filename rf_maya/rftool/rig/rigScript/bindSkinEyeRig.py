import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mm
from ncscript2.pymel import rivetTools
reload(rivetTools)
from ncscript2.pymel import core
reload(core)
from ncscript.pymel import rigTools as rt
reload(rt)


Node = core.Node()
def run(*args):
    reload(rivetTools)

    ## select Geo
    sels = mc.ls(sl = True)
    if sels:
        for each in sels:
            sel = each
    else:
        print '>> Please Select Geometry'
        return False

    ## createLidBuffrJnt for bikdSkin
    buffJnt = createLidBuffJnt()

    ## check SkinNode
    skinNode = checkSkinNode(buffJnt = buffJnt, selsGeo = sel)

    ## find Lid Dtl Jnts
    dtlJnt = findLidsSkinJnt()
    # print len(dtlJnt), '....ken'
    ## bindSkin Jnt
    addInfluence(skinNode =skinNode, dtlJnt = dtlJnt)

    ## SkinPercent
    vtxAll = []
    for jnt in dtlJnt :
        vtx = skinPercentVtx(jntSels = jnt, mesh = sel, skinBase = buffJnt, skinClusterNode = skinNode)
        vtxAll.append(vtx)
    ## PaintSkinWeight Fall Off
    checkVtxFallOff(mesh = sel, dtlJnt = dtlJnt, vtxBase = vtxAll, findUprLwr = ['LidsUprF', 'LidsLwrF'], fallOffStep = 3)

    ## SmoothFlood SkinWeight
    # smoothFlood(FloodStep = 2)

    print '>> Everything Is Ok'

def skinPercentVtx(jntSels = '', mesh = '', skinBase = '', skinClusterNode = '', *args):

    vtxGeoAll = []
    jntSelsObj = core.Dag(jntSels)
    invGrp = core.Dag(jntSelsObj.pynode.getParent())
    posiGrp = invGrp.pynode.getParent()

    # mdv = counter_controls(jntSels)
    vtx = find_closest_vertex(jntSels, mesh)

    mc.skinPercent( skinClusterNode, vtx, transformValue=[(skinBase, 0), (jntSels, 1)])
    print vtx, '::', 'Success'

    return vtx



def find_closest_vertex(obj = '', mesh = ''):
    objPos = pmc.xform(obj, q = True, ws = True, rp = True)
    vtxNo = pmc.polyEvaluate(mesh, v = True)

    closestVtx = None
    minLen = None

    for ix in range(vtxNo):

        vtx = '{}.vtx[{}]'.format(mesh, ix)

        vtxPos = pmc.xform(vtx, q = True, ws = True, t = True)

        vct = rt.diff(objPos, vtxPos)
        dist = rt.mag(vct)

        if minLen == None :
            closestVtx = vtx
            minLen = dist

        if dist < minLen :
            closestVtx = vtx
            minLen = dist

    return closestVtx

#------------------------------------------------------------------------------------------
def createLidBuffJnt(*args):
    #Bind Skin Generated Jnt
    buffJnt = mc.createNode('joint' , n='LidBuff_Jnt')

    ## parent buffJnt 
    LidsRigSkinGrp = mc.ls('*:LidsRigSkin_Grp')
    if mc.objExists(LidsRigSkinGrp[0]):
        mc.parent(buffJnt ,LidsRigSkinGrp )
    return buffJnt

def checkSkinNode(buffJnt = '', selsGeo = '',*args):

    ## check SkinNode
    skinNode = mc.skinCluster( buffJnt, selsGeo, dr=4,lw=0 )[0]
    return skinNode

def findLidsSkinJnt(*args):

    #Find Lid Dtl Jnts
    dtlJnt = []
    LidsSkinOfstLGrp = mc.ls('*:LidsSkinOfst_L_Grp')
    LidsSkinOfstRGrp = mc.ls('*:LidsSkinOfst_R_Grp')

    dtlGrp = [LidsSkinOfstLGrp[0], LidsSkinOfstRGrp[0]]
    for a in dtlGrp:
        listJnt = mc.listRelatives(a , ad = 1 , type='joint')
        for jnt in listJnt:
            dtlJnt.append(jnt)
    return dtlJnt

def addInfluence(skinNode ='', dtlJnt = [],*args):

    #add inf jnts
    mc.skinCluster(skinNode, edit = True, addInfluence = dtlJnt, weight = 0)


def checkVtxFallOff(mesh = '', dtlJnt = [], vtxBase = [], findUprLwr = ['LidsUprF', 'LidsLwrF'], fallOffStep = 3,*args):
    LidsFJnt = []
    for obj in findUprLwr:
        for i in range(len(dtlJnt)):
            if obj in dtlJnt[i]:
                LidsFJnt.append(dtlJnt[i])

    vtxLidsFCV = []
    new = []
    for vtx in LidsFJnt:
        vtx = find_closest_vertex(vtx, mesh)
        vtxLidsFCV.append(vtx)
    mc.select(vtxLidsFCV)

    for x in range(fallOffStep):
        mm.eval('select `ls -sl`;PolySelectTraverse 1;select `ls -sl`;')

    cvSels = mc.ls(sl = True, flatten=True)

    for i in range(len(cvSels)):
        if not cvSels[i] in vtxBase:
            new.append(cvSels[i])

    mc.select(cl = True)
    mc.select(new)

    ## SmoothFlood SkinWeight
    smoothFlood(FloodStep = 2)

def smoothFlood(FloodStep = 2, *args):
    # Get current tool
    currentTool = mc.currentCtx()

    skinPaintTool = 'artAttrSkinContext'
    if not mc.artAttrSkinPaintCtx(skinPaintTool,ex=True):
        mc.artAttrSkinPaintCtx(skinPaintTool,i1='paintSkinWeights.xpm',whichTool='skinWeights')
    mc.setToolTo(skinPaintTool)
    mc.artAttrSkinPaintCtx(skinPaintTool,edit=True,sao='smooth')
    for x in range(FloodStep):
        mm.eval('artAttrSkinPaintCtx -e -clear `currentCtx`;')

    mc.select(cl = True)

    # Reset current tool
    mc.setToolTo(currentTool)