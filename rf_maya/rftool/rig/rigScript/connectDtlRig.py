import maya.cmds as mc
import maya.mel as mm
import core
reload( core )
import rigTools as rt
reload( rt )
from rf_utils import file_utils
reload(file_utils)
from rf_utils.context import context_info

def Run(    bodyGeo         = 'bodyRig_md:Body_Geo' ,
            bodyGeoPost     = 'fclRig_md:BodyFclRigPost_Geo' ,
            dtlRigCtrl      = 'fclRig_md:DtlRigCtrl_Grp' , 
            dtlRigCtrlHi    = 'fclRig_md:DtlRigHiCtrl_Grp' , 
            dtlRigCtrlMid   = 'fclRig_md:DtlRigMidCtrl_Grp' , 
            dtlRigCtrlLow   = 'fclRig_md:DtlRigLowCtrl_Grp' , 
            dtlRigCtrlMidLow = 'fclRig_md:DtlRigMidLowCtrl_Grp',
            hdRigJnt        = 'bodyRig_md:Head_Jnt' , 
            jawUprRigJnt    = 'bodyRig_md:JawUpr1_Jnt' , 
            jawLwr2Jnt      = 'bodyRig_md:JawLwr2_Jnt' , 
            stillGrp        = 'bodyRig_md:Still_Grp' ,
            bshRig          = True):

    if not mc.objExists(bodyGeo):
        bodyGeo = 'bodyRig_md:Head_Geo'
        if not mc.objExists(bodyGeo):
            print "This scene doesn't have bodyGeo, Plese re-check again."
            return

    if not mc.objExists(dtlRigCtrlMidLow):
        print 'This scene does not have DtlRig objects.'
        return

    bodyGeoWrap = core.Dag(mc.duplicate(bodyGeo , rr=True , n='BodyDtlWrapBs_Geo')[0])
    mc.delete(bodyGeoWrap , ch=True)
    bodyGeoWrap.attr('v').v = 0

    mc.parent(bodyGeoWrap , stillGrp)

    #-- create wrap
    mc.select(bodyGeoPost , r=True)
    mc.select(bodyGeoWrap , add=True)

    wrap = mm.eval('doWrapArgList "7"{"1","0","0.1","2","1","0","1"};')[0]

    if mc.objExists(bodyGeoPost):
        rt.blendShape(bodyGeo , bodyGeoWrap)



    ## Dtrl Group
    # Orient Scale Constraint DtlCtrlRigHi
    dtlRigCtrlHiList = mc.listRelatives(dtlRigCtrlHi, c = True, s = False)

    for each in dtlRigCtrlHiList:
        mc.scaleConstraint(hdRigJnt , each , mo = True)
        mc.orientConstraint(hdRigJnt , each , mo = True)


    # Orient Scale Constraint DtlCtrlRigMid
    dtlRigCtrlMidList = mc.listRelatives(dtlRigCtrlMid, c = True, s = False)

    for each in dtlRigCtrlMidList:
        mc.scaleConstraint(jawUprRigJnt , each , mo = True)
        mc.orientConstraint(jawUprRigJnt , each , mo = True)

    # Orient Scale Constraint DtlCtrlRigLow
    dtlRigCtrlLowList = mc.listRelatives(dtlRigCtrlLow, c = True, s = False)

    for each in dtlRigCtrlLowList:
        mc.scaleConstraint(jawLwr2Jnt , each , mo = True)
        mc.orientConstraint(jawLwr2Jnt , each , mo = True)
    # transformGrp Fix Filp MouthCnr
    jawPosZro = mc.createNode('transform', name = 'JawUprPosZro_Grp')
    jawPosGrp = mc.createNode('transform', name = 'JawUprPos_Grp')
    mc.parent(jawPosGrp,jawPosZro)
    mc.delete(mc.parentConstraint(jawUprRigJnt,jawPosZro,mo=False))
    mc.parent(jawPosZro,jawUprRigJnt)
    mc.setAttr(jawPosZro+'.rz',180)

    # Orient Scale Constraint DtlCtrlRigMidLow      
    dtlRigCtrlMidLowList = mc.listRelatives(dtlRigCtrlMidLow, c = True, s = False)
    if dtlRigCtrlMidLowList != None:
        for each in dtlRigCtrlMidLowList:
            if 'Cnr' in each:
                mc.scaleConstraint(jawPosGrp , jawLwr2Jnt , each , mo = True)
                mc.orientConstraint(jawPosGrp , jawLwr2Jnt , each , mo = True)
            elif 'Lwr' in each:
                mc.scaleConstraint(jawLwr2Jnt , each , mo = True)
                mc.orientConstraint(jawLwr2Jnt, each , mo = True)
            else:
                mc.scaleConstraint(jawPosGrp , each , mo = True)
                mc.orientConstraint(jawPosGrp, each , mo = True)

            cons = mc.listRelatives(each,c=True,type='orientConstraint')[0]
            mc.setAttr(cons+'.interpType' , 2)

    mc.setAttr(dtlRigCtrlMidLow + '.v', 0)

    if bshRig ==True:

        if mc.objExists('fclRig_md:MouthUprBsh_Ctrl'):
            # connect bsh ctrl with ctrl
            udBsh_follow_list = ['fclRig_md:MouthUprBsh_Ctrl','fclRig_md:MouthLwrBsh_Ctrl' , 'fclRig_md:NoseBsh_Ctrl']
            lrBsh_follow_list = ['fclRig_md:MouthCnrBsh_L_Ctrl','fclRig_md:MouthCnrBsh_R_Ctrl']

            for ctrl in udBsh_follow_list:
                rivet = ctrl.replace('Bsh_','BshDtl_C_').replace('_Ctrl','_ctrl')
                zrGrp = ctrl.replace('Bsh_','BshCtrlZro_').replace('_Ctrl','_Grp')
                invGrp = ctrl.replace('Bsh_','BshCtrlOfst_').replace('_Ctrl','_Grp')

                if mc.objExists(invGrp):
                    mul = ctrl.replace('Bsh_','BshCtrlOfst_').replace('_Ctrl','_Mdv')
                    mdv = mc.createNode('multiplyDivide',n=  mul)
                    for ax in 'XYZ':
                        mc.setAttr(mdv+'.input2{}'.format(ax),-1)

                    mc.connectAttr(ctrl+'.ty',mdv+'.input1Y')
                    mc.connectAttr(mdv+'.outputY',invGrp+'.ty')
                    mc.parentConstraint(rivet,zrGrp,mo=1)

                    mc.setAttr(rivet +'.v',0)
                    for tf in 'trs':
                        for ax in 'xyz':
                            mc.setAttr(rivet+'.{}{}'.format(tf,ax))
                

            for ctrl in lrBsh_follow_list:
                
                rivet = ctrl.replace('Bsh_','BshDtl_').replace('_Ctrl','_ctrl')
                zrGrp = ctrl.replace('Bsh_','BshCtrlZro_').replace('_Ctrl','_Grp')
                invGrp = ctrl.replace('Bsh_','BshCtrlOfst_').replace('_Ctrl','_Grp')
                
                if mc.objExists(invGrp):
                    mul = ctrl.replace('Bsh_','BshCtrlOfst_').replace('_Ctrl','_Mdv')
                    mdv = mc.createNode('multiplyDivide',n=mul)
                    
                    for ax in 'XYZ':
                        mc.setAttr(mdv+'.input2{}'.format(ax),-1)
                    
                    mc.connectAttr(ctrl+'.tx',mdv+'.input1X')
                    mc.connectAttr(ctrl+'.ty',mdv+'.input1Y')
                    mc.connectAttr(mdv+'.outputX',invGrp+'.tx')
                    mc.connectAttr(mdv+'.outputY',invGrp+'.ty')
                    mc.parentConstraint(rivet,zrGrp,mo=1)
                    
                    mc.setAttr(rivet +'.v',0)
                    for tf in 'trs':
                        for ax in 'xyz':
                            mc.setAttr(rivet+'.{}{}'.format(tf,ax))

            mouthBsh = 'fclRig_md:MouthBshCtrlZro_Grp'
            if mc.objExists(mouthBsh):
                cons = mc.parentConstraint([jawPosGrp,jawLwr2Jnt],mouthBsh,mo=1)
                mc.setAttr(cons[0]+'.interpType',2)

    asset = context_info.ContextPathInfo()         
    char = asset.name
    charCrv = 'bodyRig_md:{}_Crv'.format(char)
    mc.connectAttr(charCrv + '.DetailVis', dtlRigCtrl + '.v')