import os
import sys
import maya.cmds as mc
from rf_maya.rftool.rig.rigScript import copyObjFile
reload(copyObjFile)
from rf_utils.context import context_info
import shutil
import re
# from utaTools.utapy import utaCore
# reload(utaCore)
from rf_utils import file_utils
# from utils.crvScript import ctrlData
# reload(ctrlData)
from rf_maya.rftool.rig.rigScript import ctrlData
reload(ctrlData)

#path project config
rootScript = os.environ['RFSCRIPT']
pathProjectJson = r"{}\core\rf_maya\rftool\rig\export_config\project_config.json".format(rootScript)
projectData = file_utils.json_loader(pathProjectJson)



def runFacialUiCtrl():
    filePyName = 'caFacialCtrlRun'
    asset = context_info.ContextPathInfo() 
    charName = asset.name
    bpath = copyObjFile.getRigDataPath(rigData = 'rigData', filePyName = filePyName)
    projectName = asset.project if mc.file(q=True, loc=True) else ''
    # getPath
    proPath = copyObjFile.getDataFld()
    mainPath = bpath
    if projectName == 'CA' :
        if os.path.exists(mainPath):
            CheckBackUpScrip(filePyName = filePyName)
            sys.path.insert(0, copyObjFile.getDataFld())
            import caFacialCtrlRun
            reload(caFacialCtrlRun)

            caFacialCtrlRun.FacialCtrlRun()


            del(caFacialCtrlRun)
            print 'PROJECT NAME :', projectName
            print 'CHARACTER Name ::', charName 
    else:
        if os.path.exists(mr_path):
            sys.path.insert(0, bpath)
            import caFacialCtrlRun
            reload(caFacialCtrlRun)

            caFacialCtrlRun.main()

            del(caFacialCtrlRun)
            print 'PROJECT NAME :', projectName
            print 'CHARACTER Name ::', charName 
def runPrevisRig():
    # Path ---------------------------------------
    corePath = '%s/core' % os.environ.get('RFSCRIPT')
    previsPath = '{}/rf_maya/rftool/rig/template/templatePrevis/data'.format(corePath)
    asset = context_info.ContextPathInfo() 
    projectName = asset.project if mc.file(q=True, sn=True) else ''

    sys.path.insert(0, previsPath)

    import mainRig
    reload(mainRig)
    mainRig.main()


def runMainRigCa():
    filePyName = 'moduleRig'
    # self.backupScript('moduleRig')

    path = copyObjFile.getRigDataPath(rigData = 'rigData', filePyName = '', lastNameType = '')
    modulePath = os.path.join(path , 'moduleRig.py')
    if os.path.exists(modulePath):
        sys.path.insert(0,path)

        import moduleRig
        reload(moduleRig)
        try:
            moduleRig.Run()
            ctrlData.read_ctrl()
        except Exception as e:
            print e
        
        del(moduleRig)
        sys.path.remove(path)

def runFacialUiRig():
    filePyName = 'caFacialRun'
    asset = context_info.ContextPathInfo() 
    charName = asset.name
    bpath = copyObjFile.getRigDataPath(rigData = 'rigData', filePyName = filePyName)
    projectName = asset.project if mc.file(q=True, loc=True) else ''
    # getPath
    proPath = copyObjFile.getDataFld()
    mainPath = bpath
    if projectName == 'CA' :
        if os.path.exists(mainPath):
            CheckBackUpScrip(filePyName = filePyName)
            sys.path.insert(0, copyObjFile.getDataFld())
            import caFacialRun
            reload(caFacialRun)


            caFacialRun.main()

            del(caFacialRun)
            print 'PROJECT NAME :', projectName
            print 'CHARACTER Name ::', charName 
    else:
        if os.path.exists(mr_path):
            sys.path.insert(0, bpath)
            import caFacialRun
            reload(caFacialRun)

            caFacialRun.main()


            del(caFacialRun)
            print 'PROJECT NAME :', projectName
            print 'CHARACTER Name ::', charName 

    # print '# Generate >> PROJECT NAME :', projectName

#backup runFacialRig
def backupMainRig(filePyName = '', *args):
    proPath = copyObjFile.getDataFld()
    # mainPath = os.path.join(proPath, 'caFacialCtrlRun.py')
    dstDir = os.path.join(proPath, '_bak_moduleUiRig')
    mainPath = copyObjFile.getRigDataPath(rigData = 'rigData', filePyName = filePyName)

    if os.path.exists(mainPath) :
        if not os.path.exists(dstDir) :
            os.makedirs(dstDir)
        listDir = os.listdir(dstDir)
        if listDir:
            lastVer = re.findall('\\d+', listDir[-1])
            newVer = int(lastVer[0])+1
        else :
            newVer = 1

        newName = '{}_v%03d.py'.format(filePyName) % newVer
        dst = os.path.join(dstDir, newName)

        shutil.copy2(mainPath, dst)
        print 'Backup {}.py to : '.format(filePyName) + dst 

    if os.path.exists(dst):
        return dst

def CheckBackUpScrip(filePyName = '', *args):
    checkMessage = mc.confirmDialog(    title='Confirm', 
                                        message='Do you want backup Scrip?', 
                                        button=['Yes','No'], 
                                        defaultButton='Yes', 
                                        cancelButton='No', 
                                        dismissString='No' )
    if checkMessage == 'Yes':
        backupMainRig(filePyName = filePyName)
        print 'Backup >> {}.py is Done!!'.format(filePyName)
    if checkMessage == 'No':
        print 'Thank you'


def backupBpmRig():
    proPath = copyObjFile.getDataFld()
    bpmPath = os.path.join(proPath, 'bpmRig.py')
    dstDir = os.path.join(proPath, 'backup_bpmRig')

    if os.path.exists(bpmPath) :
        if not os.path.exists(dstDir) :
            os.makedirs(dstDir)
        listDir = os.listdir(dstDir)

        if listDir:
            lastVer = re.findall('\\d+', listDir[-1])
            newVer = int(lastVer[0])+1
        else :
            newVer = 1

        newName = 'bpmRig_v%03d.py' % newVer
        dst = os.path.join(dstDir, newName)

        shutil.copy2(bpmPath, dst)
        print 'bpmRig File already Backup to : ' + dst 

    if os.path.exists(dst):
        return dst

def CheckBackUpBpmScrip(*args):
    checkMessage = mc.confirmDialog( title='Confirm', 
                message='Do you want backup Scrip?', 
                button=['Yes','No'], 
                defaultButton='Yes', 
                cancelButton='No', 
                dismissString='No' )
    if checkMessage == 'Yes':
        backupBpmRig()
        print 'Backup "bpmRig.py" Done!!'
    if checkMessage == 'No':
        print 'Thank you'
