from rf_maya.rftool.rig.rigScript import core
reload(core)
from rf_maya.rftool.rig.rigScript import rigTools as rt
reload(rt)
import maya.cmds as mc
import pymel.core as pm

cn = core.Node()


class TwistSet( object ):
	def __init__(self, name =  '',
						side  = 'L' ,
						twistFromJnt = '',
						posiA = '',
						posiB = '',
						parent ='',
						parentDistGrp = '',
						axis  = 'y' ,
						inverseAxis = 0,
						inverseRoot = 0,
						numJnts = 5):
		self.numJnts = numJnts
		## -- Info
		if side == '' :
			side = '_'
		else :
			side = '_{}_'.format(side)

		#-- rigProcess
		twistFromJnt = core.PyNode(twistFromJnt)

		if inverseAxis:
			mul = -1
		else:
			mul = 1

		if self.numJnts != 1:
			step = 1.0/(self.numJnts -1)
		else:
			step = .5
		
		if inverseRoot:
			stepVal = 1
		else:
			stepVal = 0

		self.twstJntArray = []
		self.pvAmpArray = []

		for i in range(0,self.numJnts):
			pV = ((0.5 / (self.numJnts)) * (i+1) * 2) - ((0.5 / ((self.numJnts) * 2)) * 2)
			#stepVal = (1.0 / (self.numJnts - 1) * i)
			if inverseRoot:
				if stepVal < 0:
					stepVal == 0
			else:
				if stepVal > 1:
					stepVal == 1

			if self.numJnts ==1:
				stepVal = .5
			#-- Create Joint
			twstJnt = cn.joint( '{}{}TwstDtl{}Jnt'.format( name , i+1 , side ))

			# snap Jnt & control position along PosiA,PosiB
			consCtrl = mc.parentConstraint([posiA,posiB],twstJnt,mo=0)[0]

			for w,val in zip(mc.parentConstraint(consCtrl,q=1,wal=1),[1-pV,pV]):
				mc.setAttr('{}.{}'.format(consCtrl,w),val)
			mc.delete(consCtrl)
			mc.makeIdentity(twstJnt,a=1,t=0,r=1,s=1)
			mc.parent(twstJnt, parent)

			pointCons = mc.pointConstraint([posiA,posiB],twstJnt,mo=0)[0]

			for w,val in zip(mc.pointConstraint(pointCons,q=1,wal=1),[1-pV,pV]):
				mc.setAttr('{}.{}'.format(pointCons,w),val)

			twstAmpMdv = cn.createNode( 'multiplyDivide' , '{}{}AmpTwist{}Mdv'.format( name, i, side ))
			
			twistFromJnt.attr('r{}'.format(axis)) >> twstAmpMdv.attr('input1X')
			twstAmpMdv.attr('input2X').setVal((stepVal)*mul)
			twstAmpMdv.attr('outputX') >> twstJnt.attr('r{}'.format(axis))
			stretchMdv = cn.createNode('multiplyDivide', '{}{}AmpStretch{}Mdv'.format(name,i,side))
			self.twstJntArray.append(twstJnt)
			self.pvAmpArray.append((stepVal)*mul)


			if inverseRoot:
				stepVal -= step
			else:
				stepVal += step



		# # stretch
		# # create stretch Node
		# stretchDist = cn.createNode('distanceBetween', '{}Stretch{}Dist'.format(name,side))
		# posiGrp = cn.createNode('transform', '{}Strt{}Grp'.format(name,side))
		# posiStart = cn.createNode('transform','{}StartStrtDist{}Grp'.format(name,side)) 
		# posiEnd = cn.createNode('transform','{}EndStrtDist{}Grp'.format(name,side))
		# mc.parent(posiStart,posiEnd , posiGrp)
		# mc.delete(mc.parentConstraint(posiA, posiGrp, mo=0))
		# mc.parentConstraint(posiA, posiStart, mo=0)
		# mc.parentConstraint(posiB, posiEnd, mo=0)
		
		# stretchMdv = cn.createNode('multiplyDivide','{}Stretch{}Mdv'.format(name,side))
		# stretchPma = cn.createNode('plusMinusAverage','{}Stretch{}Pma'.format(name,side))
		# dist = rt.distance( posiA , posiB )
		# # connect transform with stretch
		# posiStart.attr('t') >> stretchDist.attr('p1')
		# posiEnd.attr('t') >> stretchDist.attr('p2')
		# stretchDist.attr('distance') >> stretchPma.attr('input1D[0]')
		# stretchPma.attr('input1D[1]').setVal(dist)
		# stretchPma.attr('operation').setVal(2)
		# #mc.setAttr(stretchMdv+'.input2X',dist)
		# #mc.setAttr(stretchMdv+'.operation',2)
		# stretchPma.attr('output1D') >> stretchMdv.attr('input1X')
		# stretchMdv.attr('input2X').setVal(dist)
		# stretchMdv.attr('operation').setVal(2)
		# #print self.twstJntArray
		# # create mdv for each joint
		# if inverseAxis:
		# 	step = -1*1.0/(self.numJnts-1)
		# else:
		# 	step = 1.0/(self.numJnts-1)
		# stepVal = step

		# for i in range(len(self.twstJntArray)):
		# 	print self.twstJntArray[i]
		# 	twstJntStrtMdv = cn.createNode('multiplyDivide','{}{}Stretch{}Mdv'.format( name, i, side ))
		# 	twstJntStrtPma = cn.createNode('plusMinusAverage','{}{}Stretch{}Pma'.format( name, i, side ))
		# 	stretchMdv.attr('outputX') >> twstJntStrtMdv.attr('input1X')
		# 	pvMdv = cn.createNode('multiplyDivide','{}{}PvStretch{}Mdv'.format( name, i, side ))
		# 	jntAxisPos = mc.getAttr(self.twstJntArray[i].attr('t{}'.format(axis)))

		# 	twstJntStrtMdv.attr('input2X').setVal(stepVal)
		# 	twstJntStrtPma.attr('input1D[0]').setVal(jntAxisPos)
		# 	twstJntStrtMdv.attr('outputX') >> twstJntStrtPma.attr('input1D[1]')
		# 	twstJntStrtPma.attr('output1D') >> self.twstJntArray[i].attr('t{}'.format(axis))
		# 	stepVal+=step
		# if parentDistGrp:
		# 	mc.parent(posiGrp,parentDistGrp)

# TwistSet( name =  'ForeArm',
# side  = 'L' ,
# twistFromJnt = 'WristNonRollTwist_L_Grp',
# posiA = 'Forearm_L_Jnt',
# posiB = 'Wrist_L_Jnt',
# parent ='Forearm_L_Jnt',
# parentDistGrp = 'ArmCtrl_L_Grp',
# axis  = 'y' ,
# inverseAxis = 0,
# inverseRoot = 0,
# numJnts = 5 )



# TwistSet( name =  'ForeArm',
# side  = 'R' ,
# twistFromJnt = 'WristNonRollTwist_R_Grp',
# posiA = 'Forearm_R_Jnt',
# posiB = 'Wrist_R_Jnt',
# parent ='Forearm_R_Jnt',
# parentDistGrp = 'ArmCtrl_R_Grp',
# axis  = 'y' ,
# inverseAxis = 1,
# inverseRoot = 0,
# numJnts = 5 )



# TwistSet( name =  'UpArm',
# side  = 'L' ,
# twistFromJnt = 'UpArmNonRollTwist_L_Grp',
# posiA = 'UpArm_L_Jnt',
# posiB = 'Forearm_L_Jnt',
# parent ='UpArm_L_Jnt',
# parentDistGrp = 'ArmCtrl_L_Grp',
# axis  = 'y' ,
# inverseAxis = 0,
# inverseRoot = 1,
# numJnts = 5 )



# TwistSet( name =  'UpArm',
# side  = 'R' ,
# twistFromJnt = 'UpArmNonRollTwist_R_Grp',
# posiA = 'UpArm_R_Jnt',
# posiB = 'Forearm_R_Jnt',
# parent ='UpArm_R_Jnt',
# parentDistGrp = 'ArmCtrl_R_Grp',
# axis  = 'y' ,
# inverseAxis = 1,
# inverseRoot = 1,
# numJnts = 5 )