"""
This module is for controller modification.
This module can read/write control shape data
and use it to create/redraw any 'curve' controller.
"""

import maya.cmds as mc
import maya.mel as mel
import pymel.core as pmc
import maya.OpenMaya as om
import os
import json
import pprint

def get_ctrlDict_path():
	# get ctrl file path
	crvDictPath = os.path.dirname(__file__)+'\\crvDict.txt'
	return (crvDictPath)

def load_ctrl_dict():
	# load and return cotrol dict
 	getPath = get_ctrlDict_path()
 	with open(getPath, 'r') as f:
 		ctrlDict = json.load(f)
 	return ctrlDict

def add_ctrl_to_dict(shape,crv):
	# add new key and crv data
 	getPath = get_ctrlDict_path()
 	ctrlDict = load_ctrl_dict()
 	ctrlDict[shape]= get_shape(crv)
 	with open(getPath, 'w') as f:
		json.dump(ctrlDict, f, indent=4)
	
	return ctrlDict

def delete_key_fromDict(shape):
	# delete crv data by key
	getPath = get_ctrlDict_path()
 	with open(getPath, 'w') as f:
 		ctrlDict = json.load(f)
 		try:
 			ctrlDict.pop(shape)
 		except:
 			pass

	with open(getCtrlDictPath(), 'w') as outfile:
	    json.dump(ctrlDict, outfile, indent=4)
	return ctrlDict

def get_ctrlDict_key():
	# check key in controller dict
	ctrlDict= load_ctrl_dict()
	pprint.pprint(ctrlDict.keys())


def get_shape(crv=None):
	# get crv data from curve input
	# vlidate_curve to get shapeNode
	crvShapes =  validate_curve(crv)
	for crvShape in crvShapes:
		# create controlShape dict
		# get point, dorm, degree, colour by get attribute command
		crvShapeDict = {
		"points": mc.getAttr(crvShape+'.controlPoints[*]'),
		"knots": [],
		"form": mc.getAttr(crvShape + ".form"),
		"degree": mc.getAttr(crvShape + ".degree"),
		"colour": mc.getAttr(crvShape + ".overrideColor")}
		# get knot by get_knots function
		crvShapeDict["knots"] = get_knots(crvShape)
		return crvShapeDict

def get_knots(crvShape=None):
	# get knots data by using maya api
	mObj = om.MObject()
	sel = om.MSelectionList()
	sel.add(crvShape)
	sel.getDependNode(0, mObj)
	fnCurve = om.MFnNurbsCurve(mObj)
	tmpKnots = om.MDoubleArray()
	fnCurve.getKnots(tmpKnots)
	return [tmpKnots[i] for i in range(tmpKnots.length())]

def create_curve(shape='circle', name=''):
	# create curve by define shape from input key
	ctrlDict = load_ctrl_dict()
	# shape is key for ctrlDict
	# get data dict from ctrlDict
	useDict = ctrlDict[shape]
	# create curve from data dict
	tmpCrv = create_curve_base(useDict)
	
	# if name is '' this function wont rename curve
	if name:
		crv = mc.rename(tmpCrv,name)
	else:
		crv = tmpCrv
	return crv

def create_curve_base(useDict):
	# create curve from input dictionary data
	tmpCrv = mc.curve(p=useDict["points"], k = useDict["knots"], d=useDict["degree"], per=bool(useDict["form"]))
	return tmpCrv

################# Adjusting Crv Shape #################################################################################################

def set_shape(crv, shape):
	# adjust shape my using maya curve command
	crvDict = load_ctrl_dict()
	# print crvDict.keys()
	if shape in crvDict.keys():
		crvShapeDict = load_ctrl_dict()[shape]
	else:
		mc.warning("{}: shape not found".format(shape))
		crvShapeDict = load_ctrl_dict()['square2']
	
	set_shape_base(crv,crvShapeDict)

def set_shape_base(crv,crvShapeDict):
	# base function for change shape from crvShapeDict data
	mc.curve(crv, r=1, p = crvShapeDict["points"], k=crvShapeDict["knots"], d=crvShapeDict["degree"], per=bool(crvShapeDict["form"]))

def validate_curve(crv=None):
	# check input that is nurbsCurves shape or not
	if (mc.nodeType(crv) == "transform" or mc.nodeType(crv) == 'joint') and mc.nodeType(mc.listRelatives(crv, c=1, s=1)[0]) == "nurbsCurve":
		crvShapes = mc.listRelatives(crv, c=1, s=1)
	elif mc.nodeType(crv) == "nurbsCurve":
		crvShapes = mc.listRelatives(mc.listRelatives(crv, p=1)[0], c=1, s=1)
	else:
		mc.error("The object " + crv + " passed to validateCurve() is not a curve")
	return crvShapes

def copy_crv_shape(original='' , copy_list = [] ):
	if original and copy_list:
		crv_dict = get_shape(original)
		#print crv_dict
		for crv in copy_list:
			set_shape_base(crv,crv_dict)

def select_copy_crv_shape():
	sel = mc.ls(sl=True)
	if len(sel) >= 2:
		copy_crv_shape(sel[0],sel[1:])


def multiple_set_shape(shape = '', listCtrl = []):
	if listCtrl:
		for ctrl in listCtrl:
			set_shape(crv = ctrl , shape = shape)

def select_set_shape(shape):
	listCtrl = mc.ls(sl = True)
	multiple_set_shape(shape, listCtrl)

def scaleCtrl(objs = [] ,size = 0.8):	
	objCvLst = []	
	for obj in objs:
		objCv = '{}.cv[:]' .format(obj)		
		objCvLst.append(objCv)	
	mc.scale(size,size,size,objCvLst,a = True, os = True)





