import maya.cmds as mc
import core
import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  TORSO RIGGING MODULE 
#  
# -------------------------------------------------------------------------------------------------------------
cn = core.Node()
class Run( object ):

    def __init__( self , pelvisTmpJnt = 'Pelvis_TmpJnt' ,
                         spineTmpJnt  = ['Spine1_TmpJnt' ,
                                         'Spine2_TmpJnt' ,
                                         'Spine3_TmpJnt' , 
                                         'Spine4_TmpJnt' ,
                                         'Spine5_TmpJnt'] ,
                         parent       = 'Root_Jnt' ,
                         ctrlGrp      = 'Ctrl_Grp' ,
                         skinGrp      = 'Skin_Grp' ,
                         jntGrp       = 'Jnt_Grp' ,
                         stillGrp     = 'Still_Grp' ,
                         elem         = '' ,
                         axis         = 'y' ,
                         size         = 1 
                 ):

        ##-- Info
        if len(spineTmpJnt) > 1:
            elem = elem.capitalize()
            dist = rt.distance( spineTmpJnt[0] , spineTmpJnt[-1] )

            if axis == 'y' :
                rotateOrder = 'xzy'
                sqshAx = [ 'sx' , 'sz' ]
                aimVec =  [ 0 , 1 , 0 ]
                upVec = [ 0 , 0 , 1 ]
            
            elif axis == 'z' :
                rotateOrder = 'xzy'
                sqshAx = [ 'sx' , 'sy' ]
                aimVec = [ 0 , 0 , 1 ]
                upVec = [ 0 , -1 , 0 ]

            #-- Create Main Group
            self.spineCtrlGrp = cn.createNode( 'transform' , 'Spine{}Ctrl_Grp'.format(elem) )
            self.spineJntGrp = cn.createNode( 'transform' , 'Spine{}Jnt_Grp'.format(elem) )
            self.spineSkinGrp = cn.createNode( 'transform' , 'Spine{}Skin_Grp'.format(elem) )
            self.spineStillGrp = cn.createNode( 'transform' , 'Spine{}Still_Grp'.format(elem) )
            self.spineMainCtrlGrp = cn.createNode( 'transform' , 'Spine{}MainCtrl_Grp'.format(elem) )
            self.spineMainJntGrp = cn.createNode( 'transform' , 'Spine{}MainJnt_Grp'.format(elem) )
            mc.parentConstraint( parent , self.spineCtrlGrp , mo = False )
            self.spineCtrlGrp.parent( ctrlGrp )
            self.spineSkinGrp.parent( skinGrp )
            self.spineJntGrp.parent( jntGrp )
            self.spineStillGrp.parent( stillGrp )
            self.spineMainCtrlGrp.parent( self.spineCtrlGrp )
            self.spineMainJntGrp.parent( self.spineJntGrp )

            #-- Create Controls
            self.spineCtrl = rt.createCtrl( 'Spine{}_Ctrl'.format(elem) , 'stick' , 'green' )
            self.spineZro = rt.addGrp( self.spineCtrl )

            self.midCtrl = rt.createCtrl( 'Spine{}Mid_Ctrl'.format(elem) , 'square' , 'yellow' )
            self.midCtrlGmbl = rt.addGimbal( self.midCtrl )
            self.midCtrlZro = rt.addGrp( self.midCtrl )
            self.midCtrlOfst = rt.addGrp( self.midCtrl , 'Ofst')
            self.midCtrlPars = rt.addGrp( self.midCtrl , 'Pars')

            self.midCtrlShape = core.Dag(self.midCtrl.shape)
            self.midCtrlShape.addAttr( 'detailControl' , 0 , 1 )

            mc.parent( self.spineZro , self.midCtrlZro , self.spineMainCtrlGrp )

            #-- Adjust Shape Controls
            for ctrl in ( self.spineCtrl , self.midCtrl , self.midCtrlGmbl ) :
                ctrl.scaleShape( size )
            
            #-- Adjust Rotate Order
            self.midCtrl.setRotateOrder( rotateOrder )

            #-- Create Joint
            #-- Rig Process
            self.rootJnt = cn.joint( 'Spine{}Root_Jnt'.format(elem) , spineTmpJnt[0] )
            self.endJnt = cn.joint( 'Spine{}End_Jnt'.format(elem) , spineTmpJnt[-1] )
            if len(spineTmpJnt) % 2 == 1:
                idx = (len(spineTmpJnt) / 2)
                self.midJnt = cn.joint( 'Spine{}Mid_Jnt'.format(elem) , spineTmpJnt[idx] )
                self.midCtrlZro.snap( spineTmpJnt[idx] )
            else:
                idx1 = (len(spineTmpJnt) / 2)
                idx2 = (len(spineTmpJnt) / 2) - 1
                self.midJnt = cn.joint( 'Spine{}Mid_Jnt'.format(elem) , spineTmpJnt[idx2] )
                self.midCtrlZro.snap( spineTmpJnt[idx2] )

                mc.delete(mc.parentConstraint(spineTmpJnt[idx1], spineTmpJnt[idx2], self.midJnt, mo=False))
                mc.delete(mc.parentConstraint(spineTmpJnt[idx1], spineTmpJnt[idx2], self.midCtrlZro, mo=False))

            mc.parent( self.rootJnt , self.midJnt , self.endJnt , self.spineMainJntGrp )

            mc.parentConstraint( self.rootJnt , self.spineZro , mo = False )
            mc.parentConstraint( self.midCtrlGmbl , self.midJnt , mo = False )

            #-- Cleanup
            self.spineCtrl.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

            #-- Fk
            #-- Create Main Group Fk
            self.spineFkCtrlGrp = cn.createNode( 'transform' , 'Spine{}FkCtrl_Grp'.format(elem) )
            self.spineFkJntGrp = cn.createNode( 'transform' , 'Spine{}FkJnt_Grp'.format(elem) )
            self.spineFkCtrlGrp.snap( parent )
            self.spineFkJntGrp.snap( parent )
            self.spineFkCtrlGrp.parent( self.spineCtrlGrp )
            self.spineFkJntGrp.parent( self.spineJntGrp )

            #-- Create Joint Fk
            self.rootFkJnt = cn.joint( 'Spine{}FkRoot_Jnt'.format(elem) , self.rootJnt )
            self.midFkJnt  = cn.joint( 'Spine{}FkMid_Jnt'.format(elem) , self.midJnt )
            self.endFkJnt  = cn.joint( 'Spine{}FkEnd_Jnt'.format(elem) ,  self.endJnt )

            #-- Create Controls Fk
            self.spineFkCtrl = rt.createCtrl( 'Spine{}Fk_Ctrl'.format(elem) , 'circle' , 'red' )
            self.spineFkGmbl = rt.addGimbal( self.spineFkCtrl )
            self.spineFkZro = rt.addGrp( self.spineFkCtrl )
            self.spineFkOfst = rt.addGrp( self.spineFkCtrl , 'Ofst' )
            self.spineFkPars = rt.addGrp( self.spineFkCtrl , 'Pars' )
            self.spineFkZro.snap( self.rootFkJnt )

            self.spineMidFkCtrl = rt.createCtrl( 'Spine{}MidFk_Ctrl'.format(elem) , 'circle' , 'red' )
            self.spineMidFkGmbl = rt.addGimbal( self.spineMidFkCtrl )
            self.spineMidFkZro = rt.addGrp( self.spineMidFkCtrl )
            self.spineMidFkOfst = rt.addGrp( self.spineMidFkCtrl , 'Ofst' )
            self.spineMidFkPars = rt.addGrp( self.spineMidFkCtrl , 'Pars' )
            self.spineMidFkZro.snap( self.midFkJnt )

            self.chestFkCtrl = rt.createCtrl( 'Chest{}Fk_Ctrl'.format(elem) , 'cube' , 'red' )
            self.chestFkGmbl = rt.addGimbal( self.chestFkCtrl )
            self.chestFkZro = rt.addGrp( self.chestFkCtrl )
            self.chestFkOfst = rt.addGrp( self.chestFkCtrl , 'Ofst' )
            self.chestFkPars = rt.addGrp( self.chestFkCtrl , 'Pars' )
            self.chestFkZro.snap( self.endFkJnt )

            #-- Adjust Shape Controls Fk
            for ctrl in ( self.spineFkCtrl , self.spineFkGmbl , self.spineMidFkCtrl , self.spineMidFkGmbl , self.chestFkCtrl , self.chestFkGmbl ) :
                ctrl.scaleShape( size )
            
            #-- Adjust Rotate Order Fk
            for ctrl in ( self.spineFkCtrl , self.spineMidFkCtrl , self.chestFkCtrl ) :
                ctrl.setRotateOrder( rotateOrder )

            #-- Rig Process Fk
            mc.orientConstraint( self.spineFkGmbl , self.rootFkJnt , mo = False )
            mc.parentConstraint( self.spineMidFkGmbl , self.midFkJnt , mo = False )
            mc.parentConstraint( self.chestFkGmbl , self.endFkJnt , mo = False )

            rt.localWorld( self.spineFkCtrl , ctrlGrp , self.spineFkCtrlGrp , self.spineFkZro , 'orient' )

            #-- Adjust Hierarchy Fk
            mc.parent( self.chestFkZro , self.spineMidFkGmbl )
            mc.parent( self.spineMidFkZro , self.spineFkGmbl )
            mc.parent( self.spineFkZro , self.spineFkCtrlGrp )
            mc.parent( self.rootFkJnt , self.midFkJnt , self.endFkJnt , self.spineFkJntGrp )
            
            #-- Cleanup Fk
            for obj in ( self.spineFkCtrl , self.spineMidFkCtrl , self.chestFkCtrl ) :
                obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

            self.spineIkCtrlGrp = cn.createNode( 'transform' , 'Spine{}IkCtrl_Grp'.format(elem) )
            self.spineIkJntGrp = cn.createNode( 'transform' , 'Spine{}IkJnt_Grp'.format(elem) )
            self.spineIkCtrlGrp.snap( parent )
            self.spineIkJntGrp.snap( parent )
            self.spineIkCtrlGrp.parent( self.spineCtrlGrp )
            self.spineIkJntGrp.parent( self.spineJntGrp )

            #-- Create Joint Ik
            self.rootIkJnt = cn.joint( 'Spine{}IkRoot_Jnt'.format(elem) , self.rootJnt )
            self.midIkJnt  = cn.joint( 'Spine{}IkMid_Jnt'.format(elem) , self.midJnt )
            self.endIkJnt  = cn.joint( 'Spine{}IkEnd_Jnt'.format(elem) , self.endJnt )

            #-- Create Controls Ik
            self.spineIkCtrl = rt.createCtrl( 'Spine{}Ik_Ctrl'.format(elem) , 'square' , 'blue' )
            self.spineIkGmbl = rt.addGimbal( self.spineIkCtrl )
            self.spineIkZro = rt.addGrp( self.spineIkCtrl )
            self.spineIkOfst = rt.addGrp( self.spineIkCtrl , 'Ofst' )
            self.spineIkPars = rt.addGrp( self.spineIkCtrl , 'Pars' )
            self.spineIkZro.snap( self.rootIkJnt )
            self.spineIkZro.attr('v').value = 0

            self.chestIkCtrl = rt.createCtrl( 'Chest{}Ik_Ctrl'.format(elem) , 'cube' , 'blue' )
            self.chestIkGmbl = rt.addGimbal( self.chestIkCtrl )
            self.chestIkZro = rt.addGrp( self.chestIkCtrl )
            self.chestIkOfst = rt.addGrp( self.chestIkCtrl , 'Ofst' )
            self.chestIkPars = rt.addGrp( self.chestIkCtrl , 'Pars' )
            self.chestIkZro.snap( self.endIkJnt )

            #-- Adjust Shape Controls Ik
            for ctrl in ( self.spineIkCtrl , self.spineIkGmbl , self.chestIkCtrl , self.chestIkGmbl ) :
                ctrl.scaleShape( size )
            
            #-- Adjust Rotate Order Ik
            self.chestIkCtrl.setRotateOrder( rotateOrder )

            #-- Rig Process Ik
            mc.parentConstraint( self.chestIkGmbl , self.endIkJnt , mo = False )
            mc.parentConstraint( self.spineIkGmbl , self.rootIkJnt , mo = True )
            mc.pointConstraint( self.rootIkJnt , self.endIkJnt , self.midIkJnt , mo = True )
            
            self.aimMidCons  = mc.aimConstraint( self.endIkJnt , self.midIkJnt , aim = aimVec , u = upVec , wut = "objectrotation" , wu = upVec , wuo = self.midCtrlZro , mo = True )
        
            #-- Adjust Hierarchy Ik
            mc.parent( self.spineIkZro , self.chestIkZro , self.spineIkCtrlGrp )
            mc.parent( self.rootIkJnt , self.midIkJnt , self.endIkJnt , self.spineIkJntGrp )
            
            #-- Cleanup Ik
            self.chestIkCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

            #-- Create Nurb Surface
            loft1 = rt.createCtrl('Loft1_Cuv', 'line', 0)
            loft2 = rt.createCtrl('Loft2_Cuv', 'line', 0)
            loft3 = rt.createCtrl('Loft3_Cuv', 'line', 0)
            loft4 = rt.createCtrl('Loft4_Cuv', 'line', 0)
            loft5 = rt.createCtrl('Loft5_Cuv', 'line', 0)
            
            loft1.snap(spineTmpJnt[0])
            loft2.snap(spineTmpJnt[1])
            loft3.snap(spineTmpJnt[2])
            loft4.snap(spineTmpJnt[3])
            loft5.snap(spineTmpJnt[4])
        
            for ctrl in (loft1, loft2, loft3, loft4, loft5):
                ctrl.scaleShape(size * 1.5)
        
            self.spineNrb = core.Dag(mc.loft(loft1, loft2, loft3, loft4, loft5, ch = True, rn = True, ar = True, n = 'Spine{}_Nrb'.format(elem))[0])
            self.spineNrbShape = core.Dag(self.spineNrb.shape)
            mc.delete(self.spineNrb, ch = True)
            mc.delete(loft1, loft2, loft3, loft4, loft5)

            #-- Rig Process
            self.rbnSkc = mc.skinCluster(self.rootJnt, self.midJnt, self.endJnt, self.spineNrb, dr = 7, mi = 2, n = 'Spine{}_Skc'.format(elem))[0]

            mc.skinPercent(self.rbnSkc, '{}.cv[6][0:3]'.format(self.spineNrb), tv = [ self.endJnt, 1 ])
            mc.skinPercent(self.rbnSkc, '{}.cv[5][0:3]'.format(self.spineNrb), tv = [ self.midJnt, 0.3 ])
            mc.skinPercent(self.rbnSkc, '{}.cv[4][0:3]'.format(self.spineNrb), tv = [ self.endJnt, 0.25 ])
            mc.skinPercent(self.rbnSkc, '{}.cv[3][0:3]'.format(self.spineNrb), tv = [ (self.midJnt, 0.8),(self.endJnt, 0.05),(self.rootJnt, 0.15)])
            mc.skinPercent(self.rbnSkc, '{}.cv[2][0:3]'.format(self.spineNrb), tv = [ self.rootJnt, 0.9 ])
            mc.skinPercent(self.rbnSkc, '{}.cv[1][0:3]'.format(self.spineNrb), tv = [ self.rootJnt, 1 ])
            mc.skinPercent(self.rbnSkc, '{}.cv[0][0:3]'.format(self.spineNrb), tv = [ self.rootJnt, 1 ])

            #-- Squash Atribute
            self.midCtrl.addTitleAttr( 'Breathe' )
            self.midCtrl.addAttr( 'breatheLwr' )
            self.midCtrl.addAttr( 'breatheUpr' )
            self.midCtrl.addTitleAttr( 'Squash' )
            self.midCtrl.addAttr( 'autoSquash' , 0 , 1 )
            self.midCtrlShape.addAttr( 'length' )

            self.rbnPosGrp = cn.createNode( 'transform' , 'Spine{}Pos_Grp'.format(elem) )
            self.rootPosGrp = cn.createNode( 'transform' , 'Spine{}RootPos_Grp'.format(elem) )
            self.midPosGrp = cn.createNode( 'transform' , 'Spine{}MidPos_Grp'.format(elem) )
            self.endPosGrp = cn.createNode( 'transform' , 'Spine{}EndPos_Grp'.format(elem) )

            mc.pointConstraint(self.rootJnt, self.rootPosGrp)
            mc.pointConstraint(self.midJnt, self.midPosGrp)
            mc.pointConstraint(self.endJnt, self.endPosGrp)

            self.rootDist = cn.createNode( 'distanceBetween' , 'Spine{}Root_Dist'.format(elem) )
            self.endDist = cn.createNode( 'distanceBetween' , 'Spine{}End_Dist'.format(elem) )

            self.rootPosGrp.attr('t') >> self.rootDist.attr('p1')
            self.midPosGrp.attr('t') >> self.rootDist.attr('p2')
            self.midPosGrp.attr('t') >> self.endDist.attr('p1')
            self.endPosGrp.attr('t') >> self.endDist.attr('p2')

            self.lenPma = cn.createNode( 'plusMinusAverage' , 'Spine{}Len_Pma'.format(elem) )

            self.rootDist.attr('d') >> self.lenPma.attr('i1[0]')
            self.endDist.attr('d') >> self.lenPma.attr('i1[1]')

            self.midCtrlShape.attr('length').value = dist
            self.midCtrlShape.lockHideAttrs( 'length' )

            self.sqshDivMdv = cn.createNode( 'multiplyDivide' , 'Spine{}SqshDiv_Mdv'.format(elem) , op = 2, i1x = 1)
            self.sqshPowMdv = cn.createNode( 'multiplyDivide' , 'Spine{}SqshPow_Mdv'.format(elem) , op = 3, i2x = 2)
            self.sqshNormMdv = cn.createNode( 'multiplyDivide' , 'Spine{}SqshNorm_Mdv'.format(elem) , op=2)
            
            self.lenPma.attr('o1') >> self.sqshNormMdv.attr('i1x')
            self.midCtrlShape.attr('length') >> self.sqshNormMdv.attr('i2x')
            self.sqshNormMdv.attr('ox') >> self.sqshPowMdv.attr('i1x')
            self.sqshPowMdv.attr('ox') >> self.sqshDivMdv.attr('i2x')

            #-- Create Detail Controls
            self.posGrpDict = []
            self.posiDict = []
            self.aimDict = []
            self.jntDict = []
            self.ctrlDict = []
            self.ctrlZroDict = []
            self.sqshMdvDict = []
            self.sqshPmaDict = []

            #-- Create Detail Group
            self.rbnDetailCtrlGrp = cn.createNode( 'transform' , 'Spine{}DtlCtrl_Grp'.format(elem) )
            self.rbnDetailCtrlGrp.parent( self.spineCtrlGrp )

            cpos = cn.createNode( 'closestPointOnSurface' , 'closestPointOnSurface1')
            mc.connectAttr('{}.ws[0]'.format(self.spineNrb), '{}.is'.format(cpos))

            tmpJnt = {}
            posiValue = {}
            breatheValue = {}
            for num, jnt in enumerate(spineTmpJnt):
                posi = mc.xform(jnt, q=True, ws=True, t=True)
                mc.setAttr('{}.ip'.format(cpos), *posi)
                uval = mc.getAttr('{}.u'.format(cpos))

                idx = "{}".format(num)
                tmpJnt[idx] = jnt
                posiValue[idx] = uval
                breatheValue[idx] = (1-float( num ) / ( len( spineTmpJnt ) - 1 ) , float( num ) / ( len( spineTmpJnt ) - 1 ))
            mc.delete(cpos)

            percen = posiValue["{}".format(len(posiValue)-1)]
            for num, value in posiValue.iteritems():
                posiValue[num] = value / percen

            if len(spineTmpJnt) % 2 == 1:
                half_len = len(spineTmpJnt) / 2 + 1

                squashPmaValue = []
                for i in range( half_len )[::-1]:
                    squashPmaValue.append( float(i) / half_len )
                for i in range( half_len )[1:]:
                    squashPmaValue.append( float(i) / half_len )
                squashMdvValue = [ 1-i for i in squashPmaValue]

            else:
                half_len = len(spineTmpJnt) / 2 + 1

                squashPmaValue = []
                for i in range( half_len )[:0:-1]:
                    squashPmaValue.append( float(i) / half_len )
                for i in range( half_len )[1:]:
                    squashPmaValue.append( float(i) / half_len )
                squashMdvValue = [ 1-i for i in squashPmaValue]
            # tmpJnt = { '1' : spine1TmpJnt , '2' : spine2TmpJnt , '3' : spine3TmpJnt , '4' : spine4TmpJnt , '5' : spine5TmpJnt }
            # posiValue = { '1' : 0 , '2' : 0.25 , '3' : 0.5 , '4' : 0.75 , '5' : 1 }
            # breatheValue = { '1' : (1,0) , '2' : (0.85,0.15) , '3' : (0.5,0.5) , '4' : (0.15,0.85) , '5' : (0,1) }
            for i in range( len(spineTmpJnt) ) :
                #-- Create Joint Detail
                jnt = cn.joint( 'Spine{}{}_Jnt'.format( elem , i+1 ))
                jntSca = cn.joint( 'Spine{}{}Sca_Jnt'.format( elem , i+1 ))
                jntBreathe = cn.joint( 'Spine{}{}Brth_Jnt'.format( elem , i+1 ))

                jntSca.parent( jnt )
                jntBreathe.parent( jntSca )
                jntBreathe.attr('tz').value = 0.02
                jnt.snap( tmpJnt['{}'.format(i)] )

                self.jntDict.append( jnt )

                #-- Create Controls Detail
                dtlCtrl = rt.createCtrl( 'Spine{}{}_Ctrl'.format( elem , i+1 ) , 'circle' , 'cyan' , jnt = False )
                dtlCtrlZro = rt.addGrp( dtlCtrl )
                dtlCtrlZro.snap( jnt )
                
                dtlCtrl.addAttr( 'squash' )

                self.ctrlDict.append( dtlCtrl )
                self.ctrlZroDict.append( dtlCtrlZro )

                #-- Adjust Shape Controls Detail
                dtlCtrl.scaleShape( size )
                
                #-- Rig Process Detail
                mc.parentConstraint( dtlCtrl , jnt , mo = False)

                self.midCtrlShape.attr( 'detailControl' ) >> dtlCtrlZro.attr('v')

                if i == len(spineTmpJnt) - 1 :
                    mc.parentConstraint( self.endJnt , dtlCtrlZro , mo = False )

                #-- Squash
                self.midCtrl.addAttr( 'squash{}'.format(i+1) )

                dtlSqshMdv = cn.createNode( 'multiplyDivide' , 'Spine{}{}Sqsh_Mdv'.format( elem , i+1 ), i2=(0.5, 0.5, squashMdvValue[i]))
                dtlSqshPma = cn.createNode( 'plusMinusAverage' , 'Spine{}{}Sqsh_Pma'.format( elem , i+1 ))

                self.sqshMdvDict.append( dtlSqshMdv )
                self.sqshPmaDict.append( dtlSqshPma )

                dtlSqshPma.addAttr( 'default' )
                dtlSqshPma.attr('default').value = squashPmaValue[i]

                dtlSqshPma.attr('default') >> dtlSqshPma.attr('i1[0]')
                dtlCtrl.attr('squash') >> dtlSqshMdv.attr('i1x')
                self.midCtrl.attr('squash{}'.format(i+1)) >> dtlSqshMdv.attr('i1y')
                dtlSqshMdv.attr('ox') >> dtlSqshPma.attr('i1[1]')
                dtlSqshMdv.attr('oy') >> dtlSqshPma.attr('i1[2]')
                dtlSqshMdv.attr('oz') >> dtlSqshPma.attr('i1[3]')
                dtlSqshPma.attr('o1') >> jntSca.attr('{}'.format(sqshAx[0]))
                dtlSqshPma.attr('o1') >> jntSca.attr('{}'.format(sqshAx[1]))

                 #-- Breathe
                dtlBrthMdv = cn.createNode( 'multiplyDivide' , 'Spine{}{}Brth_Mdv'.format( elem , i+1 ), i2x = breatheValue['{}'.format(i)][0], i2y = breatheValue['{}'.format(i)][1])
                dtlBrthPma = cn.createNode( 'plusMinusAverage' , 'Spine{}{}Brth_Pma'.format( elem , i+1 ))

                dtlBrthPma.addAttr( 'default' )
                dtlBrthPma.attr('default').value = 1

                self.midCtrl.attr( 'breatheLwr' ) >> dtlBrthMdv.attr('i1x')
                self.midCtrl.attr( 'breatheUpr' ) >> dtlBrthMdv.attr('i1y')
                
                dtlBrthPma.attr('default') >> dtlBrthPma.attr('i1[0]')
                dtlBrthMdv.attr('ox') >> dtlBrthPma.attr('i1[1]')
                dtlBrthMdv.attr('oy') >> dtlBrthPma.attr('i1[2]')
                dtlBrthPma.attr('o1') >> jntBreathe.attr('{}'.format(sqshAx[0]))
                dtlBrthPma.attr('o1') >> jntBreathe.attr('{}'.format(sqshAx[1]))

                #-- Cleanup
                dtlCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

                if not i == len(spineTmpJnt) - 1 :
                    #-- Posi Group
                    posGrp = cn.createNode( 'transform' , 'Spine{}{}Pos_Grp'.format( elem , i+1 ))
                    posi = cn.createNode( 'pointOnSurfaceInfo' , 'Spine{}{}DtlPos_Posi'.format( elem , i+1 ), top = 1, u = posiValue['{}'.format(i)], v = 0.5)
                    posGrp.snap(tmpJnt['{}'.format(i)])

                    self.spineNrb.attr( 'worldSpace[0]' ) >> posi.attr('inputSurface')
                    posi.attr( 'position' ) >> posGrp.attr('t')
                    
                    mc.parentConstraint( posGrp , dtlCtrlZro , mo = False )

                    self.posGrpDict.append( posGrp )
                    self.posiDict.append( posi )
                    
                    #-- Aim Constraint
                    rbnAim = cn.createNode( 'aimConstraint' , 'Spine{}{}_aimConstraint'.format( elem , i+1 ), 
                                            a = (aimVec[0] , aimVec[1] , aimVec[2]), 
                                            u = (upVec[0] , upVec[1] , upVec[2]),
                                            t = (0, 0, 0), 
                                            r = (0, 0, 0)
                                          )
                    
                    posi.attr('n') >> rbnAim.attr('wu')
                    posi.attr('tu') >> rbnAim.attr('tg[0].tt')
                    rbnAim.attr('cr') >> posGrp.attr('r')

                    mc.parent( rbnAim , posGrp )

                    self.aimDict.append( rbnAim )

                    mc.select( cl = True )

            #-- Rig Auto Squash
            self.dtlSqshBcl = cn.createNode( 'blendColors' , 'Spine{}AutoSqsh_Bcl'.format(elem), c2r = 1)

            self.midCtrl.attr('autoSquash') >> self.dtlSqshBcl.attr('b')
            self.sqshDivMdv.attr('ox') >> self.dtlSqshBcl.attr('c1r')
            for i in range( len(spineTmpJnt) ):
                self.dtlSqshBcl.attr('opr') >> self.sqshMdvDict[i].attr('i1z')

            #-- Adjust Hierarchy Detail

            mc.parent( self.jntDict , self.spineSkinGrp )
            mc.parent( self.ctrlZroDict , self.rbnPosGrp , self.rbnDetailCtrlGrp )
            mc.parent( self.rootPosGrp , self.midPosGrp , self.endPosGrp , self.rbnPosGrp )
            mc.parent( self.spineNrb , self.posGrpDict , self.spineStillGrp )

            #-- Cleanup 
            for grp in ( self.spineCtrlGrp , self.spineJntGrp ) :
                grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

            self.midCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

            #-- Blending Fk Ik 
            rt.blendFkIk( self.spineCtrl , self.rootFkJnt , self.rootIkJnt , self.rootJnt )
            rt.blendFkIk( self.spineCtrl , self.midFkJnt , self.midIkJnt , self.midCtrlOfst )
            rt.blendFkIk( self.spineCtrl , self.endFkJnt , self.endIkJnt , self.endJnt )

            if mc.objExists( 'Spine{}FkIk_Rev'.format(elem) ) :
                self.revFkIk = core.Dag('Spine{}FkIk_Rev'.format(elem) )

                self.spineCtrl.attr( 'fkIk' ) >> self.spineIkCtrlGrp.attr('v')
                self.revFkIk.attr( 'ox' ) >> self.spineFkCtrlGrp.attr('v')

            # Pelvis
            #-- Create Main Group
            self.pelvisCtrlGrp = cn.createNode( 'transform' , 'PelvisCtrl_Grp' )
            mc.parentConstraint( parent , self.pelvisCtrlGrp , mo = False )
            
            #-- Create Joint
            self.pelvisJnt = cn.joint( 'Pelvis_Jnt' , pelvisTmpJnt )

            #-- Create Controls
            self.pelvisCtrl = rt.createCtrl( 'Pelvis_Ctrl' , 'cube' , 'yellow' , jnt = True )
            self.pelvisGmbl = rt.addGimbal( self.pelvisCtrl )
            self.pelvisZro = rt.addGrp( self.pelvisCtrl )

            self.pelvisZro.snapPoint( self.pelvisJnt )
            self.pelvisCtrl.snapJntOrient( self.pelvisJnt )

            #-- Adjust Shape Controls
            for ctrl in ( self.pelvisCtrl , self.pelvisGmbl ) :
                ctrl.scaleShape( size )

            #-- Adjust Rotate Order
            for obj in ( self.pelvisCtrl , self.pelvisJnt ) :
                obj.setRotateOrder( 'xzy' )

            #-- Rig process
            mc.pointConstraint( self.pelvisGmbl , self.rootFkJnt , mo = True )
            mc.parentConstraint( self.pelvisGmbl , self.spineIkZro , mo = True )
            mc.parentConstraint( self.pelvisGmbl , self.pelvisJnt , mo = False )
            rt.addSquash( self.pelvisCtrl , self.pelvisJnt )
            rt.localWorld( self.pelvisCtrl , ctrlGrp , self.pelvisCtrlGrp , self.pelvisZro , 'orient' )
            
            #-- Adjust Hierarchy
            mc.parent( self.pelvisZro , self.pelvisCtrlGrp )
            mc.parent( self.pelvisCtrlGrp , ctrlGrp )
            mc.parent( self.pelvisJnt , parent )

            #-- Cleanup
            for obj in ( self.pelvisCtrlGrp , self.pelvisZro ) :
                obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

            for obj in ( self.pelvisCtrl , self.pelvisGmbl ) :
                obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )