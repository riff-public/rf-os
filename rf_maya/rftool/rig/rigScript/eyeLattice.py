import maya.cmds as mc
import maya.mel as mm

import core
import rigTools as rt
import weightTools
reload(core)
reload(rt)
reload(weightTools)

cn = core.Node()

class Run(object):
	# poiJnt = [mc.ls('*:EyeAdjLFT_Jnt'), mc.ls('*:EyeAdjRGT_Jnt')]
	poiJnt = mc.ls('*:Head_Jnt')[0]
	# fclRigGeo = [mc.ls('*:EyeFclRigGeo_L_Grp'), mc.ls('*:EyeFclRigGeo_R_Grp')]

	lttAddRigGrp = 'LttAddRig_Grp'
	addRigPar = 'AddRigStillPar_Grp'
	fclRigPar = 'FclRigStillPar_Grp'
	stillGrp = mc.ls('*:Still_Grp')[0]

	def __init__(self, elem='Ltt'):
		objSel = mc.ls(sl=True, v=True)

		for num, obj in enumerate(objSel):
			if ":" in obj:
				grp = obj.split(":")[-1]

			grp = core.Dag(grp)
			grpName = grp.elem["name"].replace("Geo", "")
			grpSide = grp.elem["side"]

			allGeo = mc.listRelatives(obj, c=True, ad=True, typ="mesh")
			skinGeo = []
			dupGeo = []

			if allGeo:
				for eye in allGeo:
					if "Orig" not in eye:
						skinGeo.extend(mc.listRelatives(eye, ap=True))

				# find bsh in skin geo and delete
				if skinGeo:
					for geo in skinGeo:
						bsh = rt.findBshNode(geo)

						if bsh:
							mc.delete(bsh)

				# duplicate new geo for fclRig bsh
				eyeZro = cn.createNode("transform", "{}{}GeoZro{}Grp".format(grpName, elem, grpSide))
				eyeGrp = cn.createNode("transform", "{}{}Geo{}Grp".format(grpName, elem, grpSide))
				eyeZro.parent(eyeGrp)

				for geo in skinGeo:
					if ":" in geo:
						old = geo.split(":")[-1]

					old = core.Dag(old)
					name = old.elem["name"]
					side = old.elem["side"]
	
					eyeGeo = rt.duplicateWithoutChild(geo, name="{}{}{}Geo".format(name, elem, side))[0]
					eyeGeo.parent(eyeZro)
					dupGeo.append(eyeGeo)

					skin = mm.eval("findRelatedSkinCluster {};".format(geo))
					mc.setAttr("{}.en".format(skin), 0)

				# do blendShape
				try:
					rt.blendShape(obj.replace("bodyRig", "fclRig").replace("_", "FclRigGeo_", 1), eyeZro)
				except:
					rt.blendShape(obj.replace("bodyRig", "fclRig").replace("Geo_", "FclRigGeo_", 1), eyeZro)

				rt.blendShape(eyeZro, obj)

				# copy skin weight
				for sn, dp in zip(skinGeo, dupGeo):
					mc.select(sn, dp)
					weightTools.copySelectedWeight()

				# do lattice
				lttBaseGrp = cn.createNode("transform", "{}{}Base{}Grp".format(grpName, elem, grpSide))
				lttDg, ltt, lttBase = mc.lattice(eyeZro, n="{}{}{}{}".format(grpName, elem, grpSide, elem), dv=(6, 6, 6), oc= True)
				mc.parent(ltt, lttBase, lttBaseGrp)
				mc.setAttr("{}.ot".format(lttDg), 1)

				mc.parentConstraint(self.poiJnt, lttBaseGrp, mo=True)
				mc.scaleConstraint(self.poiJnt, lttBaseGrp, mo=True)

				# lattice group
				if not mc.objExists(self.lttAddRigGrp):
					self.lttAddRigGrp = cn.createNode("transform", self.lttAddRigGrp)
				else:
					self.lttAddRigGrp = core.Dag(self.lttAddRigGrp)

				mc.parent(eyeGrp, lttBaseGrp, self.lttAddRigGrp)

		if not mc.objExists(self.addRigPar):
			self.addRigPar = cn.createNode("transform", self.addRigPar)
		else:
			self.addRigPar = core.Dag(self.addRigPar)

		self.lttAddRigGrp.parent(self.addRigPar)
		parGrp = mc.listRelatives(self.addRigPar, ap=True)

		if not parGrp:
			self.addRigPar.parent(self.stillGrp)

		mc.select(cl=True)
		print ("Generate >>  Done !")