import maya.cmds as mc
import maya.mel as mm

def Run(    jawLwr1Ctrl = 'bodyRig_md:JawLwr1_Ctrl',
            teethUprGeo = 'bodyRig_md:TeethUpr_Geo',
            teethLwrGeo = 'bodyRig_md:TeethLwr_Geo'):

    if not mc.objExists(teethUprGeo):
        print 'This scene does not have Teeth objects.'
        return

    mc.addAttr(jawLwr1Ctrl, at= 'long', ln = 'TeethUprVis', min = 0, max = 1, k = True, dv = 1)
    mc.addAttr(jawLwr1Ctrl, at= 'long', ln = 'TeethLwrVis', min = 0, max = 1, k = True, dv = 1)
    mc.connectAttr(jawLwr1Ctrl + '.TeethUprVis', teethUprGeo + '.v')
    mc.connectAttr(jawLwr1Ctrl + '.TeethLwrVis', teethLwrGeo + '.v')

    return