import maya.cmds as mc
from rf_maya.rftool.rig.rigScript import core
reload(core)
from utaTools.utapy import utaCore
reload(utaCore)


def treeFloat(*args):
	cn = core.Node()
	value = [0.25, 0.25, 0.5, 0, 0]
	sels = mc.ls(sl = True)
	typeName = sels[0].split('_')[-1]
	name, side, lastName = utaCore.splitName(sels = [sels[0]])

	if len(sels) == 2:

		if 'Grp' in typeName:
			
			sel = sels[0]
			subCtrl = sels[-1]


			mc.select(subCtrl)
			grpCtrlName, grpCtrlOfsName, grpCtrlParsName, ctrlName, GmblCtrlName = utaCore.createControlCurve(curveCtrl = 'circle', parentCon = True, connections = False, geoVis = True)
			


			ctrl = ctrlName
			target = GmblCtrlName

			## pivot of control rig
			mc.select(sel)
			utaCore.mainProxyRig(GeoVis = True, size = 0.75)
			utaCore.createCvAxis(axisCheck = '')

			utaCore.attrNameTitle(obj = ctrl, ln = 'Functions')
			utaCore.addAttrCtrl (ctrl = ctrl, elem = ['rotateSpeed', 'floatSpeed', 'floatHeight', 'rotateOffset', 'floatOffset'], min = -1000, max = 1000, at = 'float', dv = 0)
			mc.setAttr('{}.rotateSpeed'.format(ctrl), value[0])
			mc.setAttr('{}.floatSpeed'.format(ctrl), value[1])
			mc.setAttr('{}.floatHeight'.format(ctrl), value[2])
			mc.setAttr('{}.rotateOffset'.format(ctrl), value[3])
			mc.setAttr('{}.floatOffset'.format(ctrl), value[4])

			mc.parent(grpCtrlName, 'MainCtrl_Grp')
			utaCore.parentScaleConstraintLoop(obj='Root_Ctrl', lists = [grpCtrlName],par = True, sca = True, ori = False, poi = False)

			## Gig pr Grp
			namePr, sidePr, lastNamePr = utaCore.splitName(sels = [sels[-1]])
			prGrp = '{}{}Pr{}'.format(namePr, sidePr, lastNamePr)
			utaCore.parentScaleConstraintLoop(obj= GmblCtrlName, lists = [prGrp],par = True, sca = True, ori = False, poi = False)
			mc.connectAttr('{}.GeoVis'.format(ctrlName), prGrp + '.v')

			## Clean
			if mc.objExists('Geo_Grp1'):
				mc.delete('Geo_Grp1')

		else: 
			ctrl = ''
			target = ''
			
	else:
		if 'Ctrl' in typeName:
			name, side, lastName = utaCore.splitName(sels = [sel])
			ctrl = sel
			target = '{}Gmbl{}{}'.format(name, side, lastName)


			utaCore.attrNameTitle(obj = ctrl, ln = 'Functions')
			utaCore.addAttrCtrl (ctrl = ctrl, elem = ['rotateSpeed', 'floatSpeed', 'floatHeight', 'rotateOffset', 'floatOffset'], min = -1000, max = 1000, at = 'float', dv = 0)
			mc.setAttr('{}.rotateSpeed'.format(ctrl), value[0])
			mc.setAttr('{}.floatSpeed'.format(ctrl), value[1])
			mc.setAttr('{}.floatHeight'.format(ctrl), value[2])
			mc.setAttr('{}.rotateOffset'.format(ctrl), value[3])
			mc.setAttr('{}.floatOffset'.format(ctrl), value[4])

		else: 
			ctrl = ''
			target = ''

	if target and ctrl:
		## write expression
		mc.expression( s='{}.rotateY = (time * {}.rotateSpeed) + {}.rotateOffset;'.format(target, ctrl, ctrl) )
		mc.expression( s='{}.translateY = (sin(time * {}.floatSpeed) * {}.floatHeight) + {}.floatOffset;'.format(target, ctrl, ctrl, ctrl))

		## lockAttribute Gmbl
		utaCore.lockAttrObj(listObj = [target], lock = False,keyable = False)
		mc.setAttr('{}Shape.gimbalControl'.format(ctrl),lock = True )
	else:
		print 'Please Select 2 Group  Or Select 1 Control'

	print '>> Done <<'
