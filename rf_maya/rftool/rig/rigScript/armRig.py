import maya.cmds as mc
import maya.mel as mm
import core
import rigTools as rt
import ribbonRig
import fkRig
from rf_utils.context import context_info
reload(context_info)


reload( core )
reload( rt )
reload( ribbonRig )

cn = core.Node()
# -------------------------------------------------------------------------------------------------------------
#
#  ARM RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , upArmTmpJnt   = 'UpArm_L_tmpJnt' ,
                         forearmTmpJnt = 'Forearm_L_tmpJnt' ,
                         wristTmpJnt   = 'Wrist_L_tmpJnt' ,
                         handTmpJnt    = 'Hand_L_tmpJnt' ,
                         elbowTmpJnt   = 'Elbow_L_tmpJnt' ,
                         parent        = 'ClavEnd_L_Jnt' , 
                         ctrlGrp       = 'Ctrl_Grp' ,
                         jntGrp        = 'Jnt_Grp' ,
                         skinGrp       = 'Skin_Grp' ,
                         stillGrp      = 'Still_Grp' ,
                         ikhGrp        = 'Ikh_Grp' ,
                         elem          = '' ,
                         side          = 'L' ,
                         ribbon        = True ,
                         hi            = True ,
                         elbowPos      = '' , 
                         size          = 1 ,
                         spaces         = []
                 ):

        ##-- Info
        elem = elem.capitalize()

        if side == '' :
            sideRbn = ''
            side = '_'
        else :
            sideRbn = '{}'.format(side)
            side = '_{}_'.format(side)

        if 'L' in side  :
            valueSide = 1
            axis = 'y+'
            upVec = [ 0 , 0 , 1 ]
            elbowAim = [ 0 , -1 , 0 ]
            elbowUpVec = [ 1 , 0 , 0 ]
        elif 'R' in side :
            valueSide = -1
            axis = 'y-'
            upVec = [ 0 , 0 , -1 ]
            elbowAim = [ 0 , 1 , 0 ]
            elbowUpVec = [ -1 , 0 , 0 ]

        #-- Create Main Group
        self.armCtrlGrp = cn.createNode( 'transform' , 'Arm{}Ctrl{}Grp'.format( elem , side ))
        self.armJntGrp = cn.createNode( 'transform' , 'Arm{}Jnt{}Grp'.format( elem , side ))
        mc.parentConstraint( parent , self.armCtrlGrp , mo = False )
        self.armJntGrp.snap( parent )
        
        #-- Create Joint
        self.upArmJnt = cn.joint( 'UpArm{}{}Jnt'.format( elem , side ) , upArmTmpJnt )
        self.forearmJnt = cn.joint( 'Forearm{}{}Jnt'.format( elem , side ) , forearmTmpJnt )
        self.wristJnt = cn.joint( 'Wrist{}{}Jnt'.format( elem , side ) , wristTmpJnt )
        self.handJnt = cn.joint( 'Hand{}{}Jnt'.format( elem , side ) , handTmpJnt )
        self.handJnt.parent( self.wristJnt )
        self.wristJnt.parent( self.forearmJnt )
        self.forearmJnt.parent( self.upArmJnt )
        self.upArmJnt.parent( parent )

        #-- Create Controls
        self.armCtrl = rt.createCtrl( 'Arm{}{}Ctrl'.format( elem , side ) , 'stick' , 'green' )
        self.armZro = rt.addGrp( self.armCtrl )

        #-- Adjust Shape Controls
        self.armCtrl.scaleShape( size )

        if 'L' in side :
            self.armCtrl.rotateShape( 0 , 90 , 90 )
        else :
            self.armCtrl.rotateShape( 0 , 90 , -90 )
        
        #-- Adjust Rotate Order
        for obj in ( self.upArmJnt , self.forearmJnt , self.wristJnt , self.handJnt ) :
            obj.setRotateOrder( 'yxz' )

        #-- Rig Process
        mc.parentConstraint( self.wristJnt , self.armZro , mo = False )
        upArmNonRollJnt = rt.addNonRollJnt( 'UpArm' , elem , self.upArmJnt , self.forearmJnt , parent , axis )
        # forearmNonRollJnt = rt.addNonRollJnt( 'Forearm' , elem , self.forearmJnt , self.wristJnt , self.upArmJnt , axis )
        wristNonRollJnt = rt.addNonRollJnt('Wrist' , elem,  self.wristJnt , self.handJnt, self.forearmJnt , axis )

        self.upArmNonRollJntGrp = upArmNonRollJnt['jntGrp']
        self.upArmNonRollRootNr = upArmNonRollJnt['rootNr']
        self.upArmNonRollRootNrZro = upArmNonRollJnt['rootNrZro']
        self.upArmNonRollEndNr = upArmNonRollJnt['endNr']
        self.upArmNonRollIkhNr = upArmNonRollJnt['ikhNr']
        self.upArmNonRollIkhNrZro = upArmNonRollJnt['ikhNrZro']
        self.upArmNonRollTwistGrp = upArmNonRollJnt['twistGrp']

        # self.forearmNonRollJntGrp = forearmNonRollJnt['jntGrp']
        # self.forearmNonRollRootNr = forearmNonRollJnt['rootNr']
        # self.forearmNonRollRootNrZro = forearmNonRollJnt['rootNrZro']
        # self.forearmNonRollEndNr = forearmNonRollJnt['endNr']
        # self.forearmNonRollIkhNr = forearmNonRollJnt['ikhNr']
        # self.forearmNonRollIkhNrZro = forearmNonRollJnt['ikhNrZro']
        # self.forearmNonRollTwistGrp = forearmNonRollJnt['twistGrp']

        self.wristNonRollJntGrp = wristNonRollJnt['jntGrp']
        self.wristNonRollRootNr = wristNonRollJnt['rootNr']
        self.wristNonRollRootNrZro = wristNonRollJnt['rootNrZro']
        self.wristNonRollEndNr = wristNonRollJnt['endNr']
        self.wristNonRollIkhNr = wristNonRollJnt['ikhNr']
        self.wristNonRollIkhNrZro = wristNonRollJnt['ikhNrZro']
        self.wristNonRollTwistGrp = wristNonRollJnt['twistGrp']

        #-- Adjust Hierarchy
        mc.parent( self.armZro , self.armCtrlGrp )
        mc.parent( self.armCtrlGrp , ctrlGrp )
        mc.parent( self.upArmNonRollJntGrp , self.wristNonRollJntGrp , self.armJntGrp )
        mc.parent( self.armJntGrp , jntGrp )

        #-- Cleanup
        for obj in ( self.armZro , self.armJntGrp , self.upArmNonRollJntGrp , self.wristNonRollJntGrp , self.armCtrlGrp ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        self.armCtrl.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        #-------------------------------------------# 
        #--------------     FK     -----------------# 
        #-------------------------------------------# 
       
        #-- Create Main Group Fk
        self.armFkCtrlGrp = cn.createNode( 'transform' , 'Arm{}FkCtrl{}Grp'.format( elem , side ))
        self.armFkJntGrp = cn.createNode( 'transform' , 'Arm{}FkJnt{}Grp'.format( elem , side ))

        self.armFkCtrlGrp.snap( self.armCtrlGrp )
        self.armFkJntGrp.snap( self.armCtrlGrp )
    
        #-- Create Joint Fk
        self.upArmFk_obj = fkRig.Run(name       = 'UpArm{}Fk'.format(elem) ,
                                side        = sideRbn ,
                                tmpJnt      = upArmTmpJnt ,
                                ctrlGrp     = ctrlGrp , 
                                shape       = 'cylinder' ,
                                jnt_ctrl    = 1 ,
                                color       = 'red',
                                localWorld  = 0,
                                constraint  = 0,
                                jnt         = 1,
                                dtl         = 0)

        self.forearmFk_obj = fkRig.Run(name       = 'Forearm{}Fk'.format(elem) ,
                            side        = sideRbn ,
                            tmpJnt      = forearmTmpJnt ,
                            ctrlGrp     = ctrlGrp ,
                            shape       = 'cylinder' ,
                            jnt_ctrl    = 1 ,
                            color       = 'red',
                            localWorld  = 0,
                            constraint  = 0,
                            jnt         = 1,
                            dtl         = 0)
        self.wristFk_obj = fkRig.Run(name       = 'Wrist{}Fk'.format(elem) ,
                        side        = sideRbn ,
                        tmpJnt      = wristTmpJnt ,
                        ctrlGrp     = ctrlGrp ,
                        shape       = 'cylinder' ,
                        jnt_ctrl    = 1 ,
                        color       = 'red',
                        localWorld  = 0,
                        constraint  = 0,
                        jnt         = 1,
                        dtl         = 0)
        self.handFkJnt = cn.joint( 'Hand{}Fk{}Jnt'.format( elem , side ) , handTmpJnt )
        self.handFkJnt.parent( self.wristFk_obj.jnt )
        self.wristFk_obj.jnt.parent( self.forearmFk_obj.jnt )
        self.forearmFk_obj.jnt.parent( self.upArmFk_obj.jnt )
        self.upArmFk_obj.jnt.parent( self.armFkJntGrp )

        #-- Create Controls Fk
        self.upArmFk_obj.zro.snap( self.upArmFk_obj.jnt )
        self.upArmFk_obj.ofst.snapWorldOrient()
        self.upArmFk_obj.ctrl.snapJntOrient( self.upArmFk_obj.jnt )


        self.forearmFk_obj.zro.snap( self.forearmFk_obj.jnt )
        self.forearmFk_obj.ofst.snapWorldOrient()
        self.forearmFk_obj.ctrl.snapJntOrient( self.forearmFk_obj.jnt )


        self.wristFk_obj.zro.snap( self.wristFk_obj.jnt )
        self.wristFk_obj.ofst.snapWorldOrient()
        self.wristFk_obj.ctrl.snapJntOrient( self.wristFk_obj.jnt )

        # #-- Adjust Shape Controls Fk
        for ctrl in ( self.upArmFk_obj.ctrl , self.upArmFk_obj.gmbl , self.forearmFk_obj.ctrl , self.forearmFk_obj.gmbl , self.wristFk_obj.ctrl , self.wristFk_obj.gmbl ) :
            ctrl.scaleShape( size )
    
        #-- Adjust Rotate Order Fk
        for obj in ( self.upArmFk_obj.jnt , self.forearmFk_obj.jnt , self.wristFk_obj.jnt , self.handFkJnt , self.upArmFk_obj.ctrl , self.forearmFk_obj.ctrl , self.wristFk_obj.ctrl ) :
            obj.setRotateOrder( 'yxz' )

        # #-- Rig Process Fk
        mc.parentConstraint( self.upArmFk_obj.gmbl , self.upArmFk_obj.jnt , mo = False )
        mc.parentConstraint( self.forearmFk_obj.gmbl , self.forearmFk_obj.jnt , mo = False )
        mc.parentConstraint( self.wristFk_obj.gmbl , self.wristFk_obj.jnt , mo = False )

        rt.addFkStretch( self.upArmFk_obj.ctrl , self.forearmFk_obj.ofst , 'y' , valueSide )
        rt.addFkStretch( self.forearmFk_obj.ctrl , self.wristFk_obj.ofst , 'y' , valueSide )

        self.upArmFkLocWor = rt.localWorld( self.upArmFk_obj.ctrl , ctrlGrp , self.armFkCtrlGrp , self.upArmFk_obj.zro , 'orient' )

        self.upArmFk_obj.ctrl.attr('localWorld').value = 1
    
        #-- Adjust Hierarchy Fk
        mc.parent( self.armFkCtrlGrp , self.armCtrlGrp )
        mc.parent( self.armFkJntGrp , self.armJntGrp )
        mc.parent( self.upArmFk_obj.ctrlGrp , self.armFkCtrlGrp )
        mc.parent( self.forearmFk_obj.ctrlGrp , self.upArmFk_obj.gmbl )
        mc.parent( self.wristFk_obj.ctrlGrp , self.forearmFk_obj.gmbl )

        #-------------------------------------------# 
        #--------------     IK     -----------------# 
        #-------------------------------------------# 

        #-- Create Main Group Ik
        self.armIkCtrlGrp = cn.createNode( 'transform' , 'Arm{}IkCtrl{}Grp'.format( elem , side ))
        self.armIkJntGrp = cn.createNode( 'transform' , 'Arm{}IkJnt{}Grp'.format( elem , side ))
        self.armIkhGrp = cn.createNode( 'transform' , 'Arm{}Ikh{}Grp'.format( elem , side ))

        self.armIkCtrlGrp.snap( self.armCtrlGrp )
        self.armIkJntGrp.snap( self.armCtrlGrp )
        self.armIkhGrp.snap( self.armCtrlGrp )
    
        #-- Create Joint Ik
        self.upArmIkJnt = cn.joint( 'UpArm{}Ik{}Jnt'.format( elem , side ) , upArmTmpJnt )
        self.forearmIkJnt = cn.joint( 'Forearm{}Ik{}Jnt'.format( elem , side ) , forearmTmpJnt )
        self.wristIkJnt = cn.joint( 'Wrist{}Ik{}Jnt'.format( elem , side ) , wristTmpJnt )
        self.handIkJnt = cn.joint( 'Hand{}Ik{}Jnt'.format( elem , side ) , handTmpJnt )
        self.handIkJnt.parent( self.wristIkJnt )
        self.wristIkJnt.parent( self.forearmIkJnt )
        self.forearmIkJnt.parent( self.upArmIkJnt )
        self.upArmIkJnt.parent( self.armIkJntGrp )

        #-- Create Controls Ik
        self.upArmIkCtrl = rt.createCtrl( 'UpArm{}Ik{}Ctrl'.format( elem , side ) , 'cube' , 'blue' , jnt = True )
        self.upArmIkGmbl = rt.addGimbal( self.upArmIkCtrl )
        self.upArmIkZro = rt.addGrp( self.upArmIkCtrl )
        self.upArmIkOfst = rt.addGrp( self.upArmIkCtrl , 'Ofst' )
        self.upArmIkPars = rt.addGrp( self.upArmIkCtrl , 'Pars' )
        self.upArmIkZro.snapPoint( self.upArmIkJnt )
        self.upArmIkCtrl.snapJntOrient( self.upArmIkJnt )

        self.wristIkCtrl = rt.createCtrl( 'Wrist{}Ik{}Ctrl'.format( elem , side ) , 'cube' , 'blue' , jnt = True )
        self.wristIkGmbl = rt.addGimbal( self.wristIkCtrl )
        self.wristIkZro = rt.addGrp( self.wristIkCtrl )
        self.wristIkOfst = rt.addGrp( self.wristIkCtrl , 'Ofst' )
        self.wristIkPars = rt.addGrp( self.wristIkCtrl , 'Pars' )
        self.wristIkZro.snapPoint( self.wristIkJnt )
        self.wristIkCtrl.snapJntOrient( self.wristIkJnt )

        self.elbowIkCtrl = rt.createCtrl( 'Elbow{}Ik{}Ctrl'.format( elem , side ) , 'sphere' , 'blue' , jnt = True )
        self.elbowIkZro = rt.addGrp( self.elbowIkCtrl )
        self.elbowIkZro.snapPoint( elbowTmpJnt )

        #-- Adjust Shape Controls Ik
        for ctrl in ( self.upArmIkCtrl , self.upArmIkGmbl , self.wristIkCtrl , self.wristIkGmbl ) :
            ctrl.scaleShape( size )

        self.elbowIkCtrl.scaleShape( size )

        #-- Adjust Rotate Order Ik
        for obj in ( self.upArmIkJnt , self.wristIkJnt , self.handIkJnt , self.upArmIkCtrl , self.wristIkCtrl ) :
            obj.setRotateOrder( 'yxz' )

        #-- Rig Process Ik
        self.wristIkLocWor = rt.localWorld( self.wristIkCtrl , ctrlGrp , self.upArmIkGmbl , self.wristIkZro , 'parent' )
        self.elbowIkLocWor = rt.localWorld( self.elbowIkCtrl , ctrlGrp , self.wristIkCtrl , self.elbowIkZro , 'parent' )

        #-- Prefered Ankle
        rt.preferredAnkleIk(obj = [self.forearmIkJnt], pfx = 45, pfy = 0, pfz = 0)
        # mc.setAttr ('%s.preferredAngleX' % self.forearmIkJnt, 45)

        self.crv = rt.drawLine( self.elbowIkCtrl , self.forearmIkJnt )

        self.wristIkhDict = rt.addIkh( 'Wrist{}'.format(elem) , 'ikRPsolver' , self.upArmIkJnt , self.wristIkJnt )
        self.handIkhDict = rt.addIkh( 'Hand{}'.format(elem) , 'ikSCsolver' , self.wristIkJnt , self.handIkJnt )

        self.wristIkh = self.wristIkhDict['ikh']
        self.wristIkhZro = self.wristIkhDict['ikhZro']
        self.handIkh = self.handIkhDict['ikh']
        self.handIkhZro = self.handIkhDict['ikhZro']

        self.polevector = mc.poleVectorConstraint( self.elbowIkCtrl , self.wristIkh )[0]

        self.wristIkCtrl.addAttr( 'twist' )

        self.wristIkCtrl.attr( 'twist' ) >> self.wristIkh.attr('twist')

        mc.pointConstraint( self.upArmIkGmbl , self.upArmIkJnt , mo = False )
        mc.parentConstraint( self.wristIkGmbl , self.wristIkhZro , mo = True )
        mc.parentConstraint( self.wristIkGmbl , self.handIkhZro , mo = True )

        self.ikStrt = rt.addIkStretch( self.wristIkCtrl , self.elbowIkCtrl ,'ty' , 'Arm%s' %elem , self.upArmIkJnt , self.forearmIkJnt , self.wristIkJnt )
        
        self.wristIkCtrl.attr('localWorld').value = 1
        self.wristIkCtrl.attr('autoStretch').value = 1

        #-- Adjust Hierarchy Ik
        mc.parent( self.armIkCtrlGrp , self.armCtrlGrp )
        mc.parent( self.armIkJntGrp , self.armJntGrp )
        mc.parent( self.armIkhGrp , ikhGrp )
        mc.parent( self.upArmIkZro , self.ikStrt[0] , self.crv , self.armIkCtrlGrp )
        mc.parent( self.wristIkZro , self.elbowIkZro , self.upArmIkGmbl )
        mc.parent( self.wristIkhZro , self.handIkhZro , self.armIkhGrp )

        #-- Blending Fk Ik 
        rt.blendFkIk( self.armCtrl , self.upArmFk_obj.jnt , self.upArmIkJnt , self.upArmJnt )
        rt.blendFkIk( self.armCtrl , self.forearmFk_obj.jnt , self.forearmIkJnt , self.forearmJnt )
        rt.blendFkIk( self.armCtrl , self.wristFk_obj.jnt , self.wristIkJnt , self.wristJnt )
        
        if mc.objExists( 'Arm{}FkIk{}Rev'.format( elem , side )) :
            self.revFkIk = core.Dag('Arm{}FkIk{}Rev'.format( elem , side ))

            self.armCtrl.attr( 'fkIk' ) >> self.armIkCtrlGrp.attr('v')
            self.revFkIk.attr( 'ox' ) >> self.armFkCtrlGrp.attr('v')

        #-- Add Hand Scale
        self.armCtrl.addAttr('handScale')
        self.armCtrl.attr('handScale').value = 1

        self.handSclMdv = cn.createNode( 'multiplyDivide' , 'Hand{}Scale{}Mdv'.format( elem , side ))

        self.armCtrl.attr('handScale') >> self.handSclMdv.attr('i1x')
        self.handSclMdv.attr('ox') >> self.wristJnt.attr('sx')
        self.handSclMdv.attr('ox') >> self.wristJnt.attr('sy')
        self.handSclMdv.attr('ox') >> self.wristJnt.attr('sz')
        self.handSclMdv.attr('ox') >> self.wristFk_obj.jnt.attr('sx')
        self.handSclMdv.attr('ox') >> self.wristFk_obj.jnt.attr('sy')
        self.handSclMdv.attr('ox') >> self.wristFk_obj.jnt.attr('sz')
        self.handSclMdv.attr('i2x').value = 1
        self.handJnt.attr('ssc').value = 0

        #-- Cleanup Fk
        for obj in ( self.armFkCtrlGrp , self.armFkJntGrp , self.upArmFk_obj.zro , self.upArmFk_obj.ofst , self.forearmFk_obj.zro , self.forearmFk_obj.ofst , self.wristFk_obj.zro , self.wristFk_obj.ofst ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for obj in ( self.upArmFk_obj.ctrl , self.upArmFk_obj.gmbl , self.forearmFk_obj.ctrl , self.forearmFk_obj.gmbl , self.wristFk_obj.ctrl , self.wristFk_obj.gmbl ) :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        #-- Cleanup Ik
        for obj in ( self.armIkCtrlGrp , self.armIkJntGrp , self.armIkhGrp , self.upArmIkZro , self.elbowIkZro , self.wristIkZro ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for obj in ( self.upArmIkCtrl , self.upArmIkGmbl , self.elbowIkCtrl , self.wristIkCtrl , self.wristIkGmbl ) :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        #-- Ribbon   
        if ribbon == True :
            self.upArmPar   = self.upArmJnt.getParent()
            self.forearmPar = self.forearmJnt.getParent()
            self.wristPar   = self.wristJnt.getParent()

            #-- Create Controls Ribbon
            self.armRbnCtrl = rt.createCtrl( 'Arm{}Rbn{}Ctrl'.format( elem , side ) , 'locator' , 'yellow' , jnt = True )
            self.armRbnZro = rt.addGrp( self.armRbnCtrl )
            self.armRbnPars = rt.addGrp( self.armRbnCtrl , 'Pars' )
            self.armRbnZro.snapPoint( self.forearmJnt )
            self.armRbnCtrl.snapJntOrient( self.forearmJnt )
            mc.parent( self.armRbnZro , self.armCtrlGrp )

            #-- Adjust Shape Controls Ribbon
            self.armRbnCtrl.scaleShape( size )

            #-- Rig Process Ribbon
            mc.parentConstraint( self.forearmJnt , self.armRbnZro , mo = True )

            distRbnUpArm = rt.distance( self.upArmJnt , self.forearmJnt )
            distRbnForearm = rt.distance( self.forearmJnt , self.wristJnt )

            if hi:
                self.rbnUpArm = ribbonRig.RibbonHi(  name = 'UpArm{}'.format(elem) , 
                                                     axis = axis ,
                                                     side = sideRbn ,
                                                     dist = distRbnUpArm )
                
                self.rbnForearm = ribbonRig.RibbonHi(  name = 'Forearm{}'.format(elem) , 
                                                       axis = axis ,
                                                       side = sideRbn ,
                                                       dist = distRbnForearm )

            else:
                self.rbnUpArm = ribbonRig.RibbonLow(  name = 'UpArm{}'.format(elem) , 
                                                     axis = axis ,
                                                     side = sideRbn ,
                                                     dist = distRbnUpArm )
                
                self.rbnForearm = ribbonRig.RibbonLow(  name = 'Forearm{}'.format(elem) , 
                                                       axis = axis ,
                                                       side = sideRbn ,
                                                       dist = distRbnForearm )

            self.rbnUpArm.rbnCtrlGrp.snapPoint(self.upArmJnt)
            
            try:
                context = context_info.ContextPathInfo()
                project = context.project
            except:
                project = ''
            
            if project == 'CA':
                mc.delete( 
                               mc.aimConstraint( self.forearmJnt , self.rbnUpArm.rbnCtrlGrp , 
                                                 aim = self.rbnUpArm.aimVec ,
                                                 u = upVec ,
                                                 wut='objectrotation' ,
                                                 wuo = self.upArmJnt ,
                                                 wu= upVec)
                         )
            else:

                mc.delete( 
                               mc.aimConstraint( self.forearmJnt , self.rbnUpArm.rbnCtrlGrp , 
                                                 aim = self.rbnUpArm.aimVec ,
                                                 u = upVec ,
                                                 wut='objectrotation' ,
                                                 wuo = self.upArmJnt ,
                                                 wu= (0,0,1))
                         )   
            mc.parentConstraint( self.upArmJnt , self.rbnUpArm.rbnCtrlGrp , mo = True )
            mc.parentConstraint( self.upArmJnt , self.rbnUpArm.rbnRootCtrl , mo = True )
            mc.parentConstraint( self.armRbnCtrl , self.rbnUpArm.rbnEndCtrl , mo = True )

            for ctrl in ( self.rbnUpArm.rbnRootCtrl , self.rbnUpArm.rbnEndCtrl) :
                ctrl.lockHideAttrs('tx', 'ty', 'tz' , 'rx' , 'ry' , 'rz' )
                ctrl.hide()

            self.rbnForearm.rbnCtrlGrp.snapPoint(self.forearmJnt)

            if project == 'CA':
                mc.delete( 
                               mc.aimConstraint( self.wristJnt , self.rbnForearm.rbnCtrlGrp , 
                                                 aim = self.rbnForearm.aimVec ,
                                                 u = upVec ,
                                                 wut='objectrotation' ,
                                                 wuo = self.forearmJnt ,
                                                 wu= upVec)
                         )
            else:
                mc.delete( 
                               mc.aimConstraint( self.wristJnt , self.rbnForearm.rbnCtrlGrp , 
                                                 aim = self.rbnForearm.aimVec ,
                                                 u = upVec ,
                                                 wut='objectrotation' ,
                                                 wuo = self.forearmJnt ,
                                                 wu= (0,0,1))
                         )

            mc.parentConstraint( self.forearmJnt , self.rbnForearm.rbnCtrlGrp , mo = True )
            mc.parentConstraint( self.armRbnCtrl , self.rbnForearm.rbnRootCtrl , mo = True )
            mc.parentConstraint( self.wristJnt , self.rbnForearm.rbnEndCtrl , mo = True )

            for ctrl in ( self.rbnForearm.rbnRootCtrl , self.rbnForearm.rbnEndCtrl) :
                ctrl.lockHideAttrs('tx', 'ty', 'tz' , 'rx' , 'ry' , 'rz' )
                ctrl.hide()

            #-- Adjust Ribbon
            self.rbnUpArmCtrlShape = core.Dag(self.rbnUpArm.rbnCtrl.shape)
            self.rbnForearmCtrlShape = core.Dag(self.rbnForearm.rbnCtrl.shape)

            self.rbnUpArmCtrlShape.attr('rootTwistAmp').value = -1
            self.rbnForearmCtrlShape.attr('rootTwistAmp').value = -1

            #-- Twist Distribute Ribbon
            self.upArmNonRollTwistGrp.attr('ry') >> self.rbnUpArmCtrlShape.attr('rootAbsoluteTwist')
            self.wristNonRollTwistGrp.attr('ry') >> self.rbnForearmCtrlShape.attr('endAbsoluteTwist')

            #-- Adjust Hierarchy Ribbon
            mc.parent( self.rbnUpArm.rbnCtrlGrp , self.rbnForearm.rbnCtrlGrp , self.armCtrlGrp )
            mc.parent( self.rbnUpArm.rbnJntGrp , self.rbnForearm.rbnJntGrp , self.armJntGrp )
            mc.parent( self.rbnUpArm.rbnSkinGrp , self.rbnForearm.rbnSkinGrp , skinGrp )
            mc.parent( self.rbnUpArm.rbnStillGrp , self.rbnForearm.rbnStillGrp , stillGrp )
            
            self.rbnUpArm.rbnCtrl.attr('autoTwist').value = 1
            self.rbnForearm.rbnCtrl.attr('autoTwist').value = 1

        mc.select( cl = True )

        #-- Add more space local world Ik        
        spacesList = [('Shoulder' , self.wristIkLocWor['localSpace'] )] + spaces
        rt.addMoreSpace( obj = self.wristIkLocWor ,
                       spaces = spacesList)

        if elbowPos: 
            elbowPosLoc = cn.locator(name = 'ElbowPos{}{}Loc'.format(elem,side))
            elbowPosGrp = cn.group(elbowPosLoc)
            mc.parent(elbowPosGrp, stillGrp)
            elbowPosGrp.snap(elbowPos)

            ## clear old Node and Attribute
            self.rbnUpArm.rbnRootCtrl.clearConsLocal()
            self.rbnUpArm.rbnRootCtrl.unlockHideAttrs('tx','ty','tz','rx','ry','rz')

            ## create Node 
            clampNode = cn.createNode ('clamp', 'elbowPos{}{}Cmp'.format(elem,side))
            reverseNode = cn.createNode('reverse', 'elbowPos{}{}Rev'.format(elem, side))
            multiANode = cn.createNode('multiplyDivide', 'elbowPosA{}{}Mdv'.format(elem, side))
            multiAmpNode = cn.createNode('multiplyDivide', 'elbowPosAmp{}{}Mdv'.format(elem, side))

            ## setAttr Node
            clampNode.attr('minR').v = 135
            clampNode.attr('maxR').v = 80
            multiANode.attr('operation').v = 2

            ## new parent
            if ribbon:
                mc.parentConstraint('Forearm{}{}Jnt'.format(elem,side), elbowPosGrp, mo = True)
            else:
                mc.parentConstraint(self.forearmJnt, elbowPosGrp, mo = True)
            consPos = core.Dag(mc.parentConstraint(self.armRbnCtrl, elbowPosLoc, self.rbnForearm.rbnRootCtrl, mo = False)[0])

            # addAttr ElbowPosAmp
            self.upArmRbnCtrl = core.Dag(self.armRbnCtrl)
            self.upArmRbnCtrl.addAttr('ElbowPosAmp' , -200 , 200)
            self.upArmRbnCtrl.attr('ElbowPosAmp').v = 0.25
            
            ## connect Attr
            self.forearmJnt.attr('rx') >> clampNode.attr('inputR')
            clampNode.attr('outputR') >> multiANode.attr('i2x')
            self.forearmJnt.attr('rx') >> multiANode.attr('i1x')
            self.upArmRbnCtrl.attr('ElbowPosAmp') >> multiAmpNode.attr('i2x')
            multiANode.attr('ox') >> multiAmpNode.attr('i1x')
            multiAmpNode.attr('ox') >> consPos.attr('{}W1'.format(elbowPosLoc))
            multiAmpNode.attr('ox') >> reverseNode.attr('inputX')
            reverseNode.attr('ox') >> consPos.attr('Arm{}Rbn{}CtrlW0'.format(elem,side,elem, side))
            self.rbnUpArm.rbnRootCtrl.lockHideAttrs('tx','ty','tz','rx','ry','rz')


        mc.select( cl = True )
