import maya.cmds as mc
from rf_maya.rftool.rig.rigScript import ngSkinLayersTools
reload(ngSkinLayersTools)
ui = 'ngCustomTools'

def write_ngSkinLayer(*args):
    ngSkinLayersTools.writeSelectedNgSkinLayers()
def read_ngSkinLayer(*args):
    ngSkinLayersTools.readSelectedNgSkinLayers()
def remove_ngSkinLayer(*args):
    confirm = mc.confirmDialog(m = 'This process will delete all of ngSkinLayer data in selected Objects.nSkin Weight will return to the version before initialize ngSkinLayer.\nAre you sure to proceed?',
        cb='cancel',button = ['Confirm','Cancel'],ma='center')
    
    if confirm == 'Confirm':
        ngSkinLayersTools.selectedRemoveNgSkinLayers()
    else:
        pass
def clean_ngSkinLayer(*args):
    confirm = mc.confirmDialog(m = 'This process will delete all of ngSkinLayer node in selected Objects.\nSkin Weight will be applied from ngSkinLayer node.\nAre you sure to proceed?',
        cb='cancel',button = ['Confirm','Cancel'],ma='center')
    
    if confirm == 'Confirm':
        ngSkinLayersTools.selectedCleanNgSkinLayers()
    else:
        pass
    
def enabled_ngSkinLayer(*args):
    ngSkinLayersTools.selectedEnabledNgLayer()
def disabled_ngSkinLayer(*args):
    ngSkinLayersTools.selectedDisabledNgLayer()
def transfer_ngSkinLayer(*args):
    ngSkinLayersTools.selectedTransferNgLayers()




def showUI():
    if mc.window(ui,exists=1):
        mc.deleteUI(ui)
        
    ui_win = mc.window(ui,w=130,h=340,s=0)
    mc.columnLayout(adj=1,co = ['both',10])
    mc.separator(h=10,style = 'none')
    mc.text(l='transfer ngSkinLayer.\n (please close ngSkinTools before use.)')
    mc.button(l = 'transfer ngskinLayer', c=transfer_ngSkinLayer )
    mc.separator(h=10,style = 'none')
    mc.text(l= 'read/write ngSkinLayer.')
    mc.separator(h=3,style = 'none')
    mc.button(l = 'write ngSkinLayer', c=write_ngSkinLayer)
    mc.button(l = 'read ngskinLayer', c=read_ngSkinLayer)
    mc.separator(h=10,style = 'none')
    mc.text(l= 'enabled/disabled ngSkinLayers.')
    mc.button(l = 'enabled ngskinLayer', c=enabled_ngSkinLayer)
    mc.button(l = 'disabled ngskinLayer', c=disabled_ngSkinLayer)
    mc.separator(h=10,style = 'none')
    mc.text(l='remove ngSkinlayer node(Clean ngSkinLayer node\n Weight from ngSkinLayer will be gone.).')
    mc.button(l ='remove ngskinLayer', c=remove_ngSkinLayer)
    mc.separator(h=10,style = 'none')
    mc.text(l='clean ngSkinlayer node.(Clean ngSkinLayer node\n Remain Weight from ngSkinLayer.)')
    mc.button(l ='clear ngskinLayer', c=clean_ngSkinLayer)
    mc.separator(h=10,style = 'none')
    mc.showWindow(ui)
#showUI()