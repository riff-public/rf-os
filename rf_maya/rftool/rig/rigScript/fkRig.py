import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
import core
import re
import rigTools as rt
import ctrlRig
reload(ctrlRig)
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  FK RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------
cn = core.Node()

class Run( object ):

	def __init__( self , name       = '' ,
						 side       = '' ,
						 tmpJnt     = '' ,
						 parent     = '' ,
						 ctrlGrp    = 'Ctrl_Grp' , 
						 skinGrp     = 'Skin_Grp' , 
						 shape      = 'square' ,
						 jnt_ctrl   = 0	,
						 size       = 1 ,
						 color      = 'red',
						 localWorld = 1,
						 defaultWorld = 0,
						 constraint = 1,
						 scale_constraint = 0,
						 jnt = 1,
						 dtl = 0,
						 dtlShape = 'cube',
						 dtlColor = 'blue',
						 **kwargs
				 ):

		##-- Info
		self.name = name
		self.side = side
		self.shape = shape
		self.jnt_ctrl = jnt_ctrl
		self.color = color
		self.size = size
		self.kwargs = kwargs
		self.skinGrp = skinGrp

		self.dtl =dtl
		self.dtlShape = dtlShape
		self.dtlColor = dtlColor

		if side == '' :
			side = '_'
		else :
			side = '_{}_'.format(side)

		# search tmpJnt if tmpJnt not define
		if not tmpJnt:
			search_str = '*{}{}*TmpJnt'.format(name,side)
			found_list = mc.ls(search_str)
			for found in found_list:
				match_obj = re.search('{}{}TmpJnt'.format(name,side),found)
			   
				if match_obj:
					tmpJnt = found

		else:
			tmpJnt = tmpJnt

		# create controller Grp
		self.ctrlGrp = cn.createNode('transform','{}Ctrl{}Grp'.format( name , side ))
		self.ctrlGrp.snap(tmpJnt)
		
		# create control
		self.ctrl_obj = ctrlRig.Run(name = self.name,
			tmpJnt = tmpJnt,
			side = self.side,
			shape = shape,
			jnt_ctrl = self.jnt_ctrl ,
			size = size,
			color = color,
			constraint = 0 ,
			jnt = jnt,
			**kwargs)

		# define FkRig Attr
		self.ctrl = self.ctrl_obj.ctrl
		self.zro = self.ctrl_obj.zro 
		self.ofst = self.ctrl_obj.ofst
		self.drv = self.ctrl_obj.drv
		self.inv = self.ctrl_obj.inv
		self.pars = self.ctrl_obj.pars
		self.gmbl = self.ctrl_obj.gmbl
		self.jnt = core.Dag(self.ctrl_obj.obj)
		self.jnt.attr('ssc').v = 0

		# parent zroGrp to ctrlGrp
		self.zro.parent(self.ctrlGrp)

	   # check ctrlGrp
		if ctrlGrp and mc.objExists(ctrlGrp):
			# do local world if localWorld args is 1
			if localWorld:
				self.locWor = rt.localWorld( self.ctrl , ctrlGrp , self.ctrlGrp , self.zro , 'orient' )
				# set default LocalWorld
				if defaultWorld:
					self.ctrl.attr('localWorld').value = 1
			else:
				self.locWor = {}
			self.ctrlGrp.parent(ctrlGrp)

		if parent:
			self.jnt.parent(parent)
			pm.parentConstraint(parent,self.ctrlGrp,mo=1)
		else:
			self.jnt.parent(self.skinGrp)

		if self.dtl:
			self.addDtl()

		if constraint:
			if not self.dtl:
				self.ctrl_obj.do_constraint()
			else:
				self.dtl_obj.do_constraint()

		if scale_constraint:
			print 'do scale'
			print self.ctrl_obj.obj
			self.ctrl_obj.consType = ['scale']
			if not self.dtl:
				self.ctrl_obj.do_constraint()
			else:
				self.dtl_obj.do_constraint()
			pm.scaleConstraint(parent,self.ctrlGrp,mo=1)			
		self.ctrl.lockHideAttrs( 'v' )


	def addDtl(self):
		#Create Controls
		# define FkRig Attr
		self.dtl_obj = ctrlRig.Run(name = self.name+'Dtl',
						tmpJnt = self.jnt,
						side = self.side,
						shape = self.dtlShape,
						jnt_ctrl = self.jnt_ctrl,
						size = self.size,
						color = self.dtlColor,
						jnt = 0,
						**self.kwargs)
		self.dtlCtrl = self.dtl_obj.ctrl
		self.dtlZro = self.dtl_obj.zro 
		self.dtlOfst = self.dtl_obj.ofst
		self.dtlDrv = self.dtl_obj.drv
		self.dtlInv = self.dtl_obj.inv
		self.dtlPars = self.dtl_obj.pars
		self.dtlGmbl = self.dtl_obj.gmbl
		self.dtlJnt = core.Dag(self.dtl_obj.obj)


		if self.dtl_obj.gimbal:
			self.dtlZro.parent(self.gmbl)
		else:
			self.dtlZro.parent(self.ctrl)

		mc.select( cl = True )

