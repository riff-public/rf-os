import core
reload(core)
import rigTools as rt
reload(rt)
import maya.cmds as mc
import pymel.core as pm

cn = core.Node()

# -------------------------------------------------------------------------------------------------------------
#
#  RIBBON RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------


def Run( name = '' , posiA = '' , posiB = '' , axis = '' , side = '' , numJnts = 5 , nurbValue = 5 , sineDef = False , startSpace = True , hi = True):

    dist = rt.distance( posiA , posiB )
    if hi:
        ribbon = RibbonHi( name = name , 
                            axis = axis , 
                            side = side , 
                            dist = dist , 
                            numJnts = numJnts  , 
                            nurbValue = nurbValue , 
                            sineDef = sineDef  , 
                            startSpace = startSpace)

        posiA = str(posiA)
        posiB = str(posiB)
        mc.connectAttr(posiA+'.r{}'.format(axis[0]) , ribbon.rbnAbsTwMdv.attr('i1x'))
        mc.connectAttr(posiB+'.r{}'.format(axis[0]) , ribbon.rbnAbsTwMdv.attr('i1y'))


    else:
        ribbon = RibbonLow(name        = name ,
                         axis       = axis ,
                         side       = side ,
                         dist       = dist )

    ribbon.rbnCtrlGrp.snapPoint(posiA)

    mc.delete( 
                   mc.aimConstraint( posiB , ribbon.rbnCtrlGrp , 
                                     aim = ribbon.aimVec , 
                                     u = ribbon.upVec , 
                                     wut='objectrotation' , 
                                     wuo = posiA , 
                                     wu= ribbon.upVec )
              )

    mc.parentConstraint( posiA , ribbon.rbnCtrlGrp , mo = True )
    mc.parentConstraint( posiA , ribbon.rbnRootCtrl)
    mc.parentConstraint( posiB , ribbon.rbnEndCtrl)

    for ctrl in ( ribbon.rbnRootCtrl , ribbon.rbnEndCtrl) :
        ctrl.lockHideAttrs('tx', 'ty', 'tz' , 'rx' , 'ry' , 'rz' )
        ctrl.hide()

    return {    
             'rbnCtrlGrp'   : ribbon.rbnCtrlGrp , 
             'rbnJntGrp'    : ribbon.rbnJntGrp , 
             'rbnSkinGrp'   : ribbon.rbnSkinGrp , 
             'rbnStillGrp'  : ribbon.rbnStillGrp ,
             'rbnCtrl'      : ribbon.rbnCtrl ,
             'rbnRootCtrl'  : ribbon.rbnRootCtrl ,
             'rbnEndCtrl'   : ribbon.rbnEndCtrl
            }

class Ribbon( object ):

    def __init__( self , name           = '' ,
                         axis           = 'y+' ,
                         side           = 'L' ,
                         dist           = 1 ,
                         numJnts        = 5,
                         nurbValue      = 5 ,
                         sineDef        = False
                 ):

        ##-- Info
        rbnInfo = self.ribbonInfo( axis )

        self.postAx    = rbnInfo[ 'postAx' ]
        self.twstAx    = rbnInfo[ 'twstAx' ]
        self.sqshAx    = rbnInfo[ 'sqshAx' ]
        self.aimVec    = rbnInfo[ 'aimVec' ]
        self.invAimVec = rbnInfo[ 'invAimVec' ]
        self.upVec     = rbnInfo[ 'upVec' ]
        self.rotate    = rbnInfo[ 'rotate' ]
        self.rotateOrder    = rbnInfo[ 'rotateOrder' ]
        self.sineT     = rbnInfo['sineT']
        self.numJnts    = numJnts

        if side == '' :
            side = '_'
        else :
            side = '_{}_'.format(side)

        #-- Create Main Group
        self.rbnCtrlGrp = cn.createNode( 'transform' , '{}RbnCtrl{}Grp'.format( name , side ))
        self.rbnJntGrp = cn.createNode( 'transform' , '{}RbnJnt{}Grp'.format( name , side ))
        self.rbnSkinGrp = cn.createNode( 'transform' , '{}RbnSkin{}Grp'.format( name , side ))
        
        #-- Create Joint
        self.rbnRootJnt = cn.joint( '{}RbnRoot{}Jnt'.format( name , side ))
        self.rbnMidJnt = cn.joint( '{}RbnMid{}Jnt'.format( name , side ))
        self.rbnEndJnt = cn.joint( '{}RbnEnd{}Jnt'.format( name , side ))
        self.rbnRootJnt.attr('r').value = self.rotate
        self.rbnMidJnt.attr('r').value = self.rotate
        self.rbnEndJnt.attr('r').value = self.rotate

        self.rbnMidJnt.attr('ty').value = dist/2
        self.rbnEndJnt.attr('ty').value = dist

        self.rbnRootJntAim = cn.createNode( 'transform' , '{}RbnRootJntAim{}Grp'.format( name , side ))
        self.rbnMidJntAim = cn.createNode( 'transform' , '{}RbnMidJntAim{}Grp'.format( name , side ))
        self.rbnEndJntAim = cn.createNode( 'transform' , '{}RbnEndJntAim{}Grp'.format( name , side ))

        #-- Create Controls
        self.rbnRootCtrl = rt.createCtrl( '{}RbnRoot{}Ctrl'.format( name , side ) , 'locator' , 'yellow' )
        self.rbnRootCtrlZro = rt.addGrp( self.rbnRootCtrl )

        self.rbnEndCtrl = rt.createCtrl( '{}RbnEnd{}Ctrl'.format( name , side ) , 'locator' , 'yellow' )
        self.rbnEndCtrlZro = rt.addGrp( self.rbnEndCtrl )

        self.rbnCtrl = rt.createCtrl( '{}Rbn{}Ctrl'.format( name , side ) , 'circle' , 'yellow' )
        self.rbnCtrlZro = rt.addGrp( self.rbnCtrl )
        self.rbnCtrlAim = rt.addGrp( self.rbnCtrl , 'Aim' )
        self.rbnCtrlPars = rt.addGrp( self.rbnCtrl , 'Pars' )

        mc.parent( self.rbnMidJntAim , self.rbnCtrl )
        mc.parent( self.rbnEndJntAim , self.rbnEndCtrl )
        mc.parent( self.rbnRootJntAim , self.rbnRootCtrl )

        #-- Adjust Shape Controls
        self.rbnCtrl.scaleShape( dist*0.25 )
        self.rbnCtrl.rotateShape( self.rotate[0] , self.rotate[1] , self.rotate[2] )
        
        #-- Adjust Rotate Order
        self.rbnCtrl.setRotateOrder( self.rotateOrder )

        #-- Rig Process
        mc.parentConstraint( self.rbnCtrlGrp , self.rbnSkinGrp , mo = False )
        mc.scaleConstraint( self.rbnCtrlGrp , self.rbnSkinGrp , mo = False )

        mc.parentConstraint( self.rbnRootJntAim , self.rbnRootJnt , mo = False )
        mc.parentConstraint( self.rbnMidJntAim , self.rbnMidJnt , mo = False )
        mc.parentConstraint( self.rbnEndJntAim , self.rbnEndJnt , mo = False )

        if '-' in axis :
            self.rbnCtrlZro.attr(self.postAx ).value = -(dist/2)
            self.rbnEndCtrlZro.attr(self.postAx).value = -dist
        else :
            self.rbnCtrlZro.attr(self.postAx).value = dist/2
            self.rbnEndCtrlZro.attr(self.postAx).value = dist

        mc.pointConstraint( self.rbnRootCtrl , self.rbnEndCtrl , self.rbnCtrlZro , mo = False )

        self.aimRootCons = mc.aimConstraint( self.rbnEndCtrl , self.rbnRootJntAim , aim = self.aimVec , u = self.upVec , wut = "objectrotation" , wu = self.upVec , wuo = self.rbnRootCtrlZro )
        self.aimEndCons  = mc.aimConstraint( self.rbnRootCtrl , self.rbnEndJntAim , aim = self.invAimVec , u = self.upVec , wut = "objectrotation" , wu = self.upVec , wuo = self.rbnEndCtrlZro )
        self.aimMidCons  = mc.aimConstraint( self.rbnEndCtrl , self.rbnCtrlAim , aim = self.aimVec , u = self.upVec , wut = "objectrotation" , wu = self.upVec , wuo = self.rbnCtrlZro )
    
        #-- Twist Distribute
        self.rbnCtrl.addTitleAttr( 'Twist' )
        self.rbnCtrl.addAttr( 'autoTwist' , 0 , 1 )
        self.rbnCtrl.addAttr( 'rootTwist' )
        self.rbnCtrl.addAttr( 'endTwist' )

        self.rbnCtrlShape = core.Dag(self.rbnCtrl.shape)

        self.rbnCtrlShape.addAttr( 'rootTwistAmp' )
        self.rbnCtrlShape.addAttr( 'endTwistAmp' )

        self.rbnCtrlShape.addAttr( 'rootAbsoluteTwist' )
        self.rbnCtrlShape.addAttr( 'endAbsoluteTwist' )

        self.rbnAbsTwMdv = cn.createNode('multiplyDivide','{}RbnTwistAbs{}Mdv'.format( name , side ))
        if axis[-1] == '+':
            mdvTwistAmp = 1
        else:
            mdvTwistAmp = -1
        self.rbnAbsTwMdv.attr('i2x').value = mdvTwistAmp
        self.rbnAbsTwMdv.attr('i2y').value = mdvTwistAmp

        self.rbnAbsTwMdv.attr('ox') >> self.rbnCtrlShape.attr( 'rootAbsoluteTwist' )
        self.rbnAbsTwMdv.attr('oy') >> self.rbnCtrlShape.attr( 'endAbsoluteTwist' )


        self.rbnCtrlShape.attr('rootTwistAmp').value = 1
        self.rbnCtrlShape.attr('endTwistAmp').value = 1

        self.rbnAmpTwMdv = cn.createNode( 'multiplyDivide' , '{}RbnAmpTwist{}Mdv'.format( name , side ))
        self.rbnAutoTwMdv = cn.createNode( 'multiplyDivide' , '{}RbnAutoTwist{}Mdv'.format( name , side ))
        self.rbnRootPma = cn.createNode( 'plusMinusAverage' , '{}RbnRootTwist{}Pma'.format( name , side ))
        self.rbnEndPma = cn.createNode( 'plusMinusAverage' , '{}RbnEndTwist{}Pma'.format( name , side ))

        self.rbnCtrlShape.attr( 'rootAbsoluteTwist' ) >> self.rbnAmpTwMdv.attr('i1x')
        self.rbnCtrlShape.attr( 'rootTwistAmp' ) >> self.rbnAmpTwMdv.attr('i2x')
        self.rbnCtrlShape.attr( 'endAbsoluteTwist' ) >> self.rbnAmpTwMdv.attr('i1y')
        self.rbnCtrlShape.attr( 'endTwistAmp' ) >> self.rbnAmpTwMdv.attr('i2y')

        self.rbnCtrl.attr( 'autoTwist' ) >> self.rbnAutoTwMdv.attr('i1x')
        self.rbnCtrl.attr( 'autoTwist' ) >> self.rbnAutoTwMdv.attr('i1y')

        self.rbnAmpTwMdv.attr( 'ox' ) >> self.rbnAutoTwMdv.attr('i2x')
        self.rbnAmpTwMdv.attr( 'oy' ) >> self.rbnAutoTwMdv.attr('i2y')

        self.rbnCtrl.attr( 'rootTwist' ) >> self.rbnRootPma.attr('i1[0]')
        self.rbnCtrl.attr( 'endTwist' ) >> self.rbnEndPma.attr('i1[0]')

        self.rbnAutoTwMdv.attr( 'ox' ) >> self.rbnRootPma.attr('i1[1]')
        self.rbnAutoTwMdv.attr( 'oy' ) >> self.rbnEndPma.attr('i1[1]')
        
        #-- Squash Atribute
        self.rbnCtrl.addTitleAttr( 'Squash' )
        self.rbnCtrl.addAttr( 'autoSquash' , 0 , 1 )
        self.rbnCtrl.addAttr( 'ampSquash' )
        self.rbnCtrl.addAttr( 'squash' )
        self.rbnCtrl.addAttr( 'squashPosition',-10,10 )
        self.rbnCtrl.attr( 'ampSquash').value = 1
        self.rbnCtrlShape.addAttr( 'length' )

        self.rbnPosGrp = cn.createNode( 'transform' , '{}RbnPos{}Grp'.format( name , side ))
        self.rbnRootPosGrp = cn.createNode( 'transform' , '{}RbnRootPos{}Grp'.format( name , side ))
        self.rbnMidPosGrp = cn.createNode( 'transform' , '{}RbnMidPos{}Grp'.format( name , side ))
        self.rbnEndPosGrp = cn.createNode( 'transform' , '{}RbnEndPos{}Grp'.format( name , side ))

        mc.pointConstraint(self.rbnRootJnt, self.rbnRootPosGrp)
        mc.pointConstraint(self.rbnMidJnt, self.rbnMidPosGrp)
        mc.pointConstraint(self.rbnEndJnt, self.rbnEndPosGrp)

        self.rootDist = cn.createNode( 'distanceBetween' , '{}RbnRoot{}Dist'.format( name , side ))
        self.endDist = cn.createNode( 'distanceBetween' , '{}RbnEnd{}Dist'.format( name , side ))

        self.rbnRootPosGrp.attr('t') >> self.rootDist.attr('p1')
        self.rbnMidPosGrp.attr('t') >> self.rootDist.attr('p2')
        self.rbnMidPosGrp.attr('t') >> self.endDist.attr('p1')
        self.rbnEndPosGrp.attr('t') >> self.endDist.attr('p2')

        self.rbnLenPma = cn.createNode( 'plusMinusAverage' , '{}RbnLen{}Pma'.format( name , side ))

        self.rootDist.attr('d') >> self.rbnLenPma.attr('i1[0]')
        self.endDist.attr('d') >> self.rbnLenPma.attr('i1[1]')

        self.rbnCtrlShape.attr('length').value = dist
        self.rbnCtrlShape.lockAttrs( 'length' )

        self.rbnSqshDivMdv = cn.createNode( 'multiplyDivide' , '{}RbnSqshDiv{}Mdv'.format( name , side ) , op=2 , i1x=1)
        self.rbnSqshPowMdv = cn.createNode( 'multiplyDivide' , '{}RbnSqshPow{}Mdv'.format( name , side ) , op=3 , i2x=2)
        self.rbnSqshNormMdv = cn.createNode( 'multiplyDivide' , '{}RbnSqshNorm{}Mdv'.format( name , side ) , op=2)
        
        self.rbnLenPma.attr('o1') >> self.rbnSqshNormMdv.attr('i1x')
        self.rbnCtrlShape.attr('length') >> self.rbnSqshNormMdv.attr('i2x')
        self.rbnSqshNormMdv.attr('ox') >> self.rbnSqshPowMdv.attr('i1x')
        self.rbnSqshPowMdv.attr('ox') >> self.rbnSqshDivMdv.attr('i2x')

        mc.parent( self.rbnRootPosGrp , self.rbnMidPosGrp , self.rbnEndPosGrp , self.rbnPosGrp )
        mc.parent(self.rbnPosGrp , self.rbnCtrlGrp)

        #-- Adjust Hierarchy
        mc.parent( self.rbnRootJnt , self.rbnMidJnt , self.rbnEndJnt , self.rbnJntGrp )

        mc.parent( self.rbnCtrlZro , self.rbnRootCtrlZro , self.rbnEndCtrlZro , self.rbnCtrlGrp )

        #-- Cleanup
        for obj in ( self.rbnJntGrp , self.rbnSkinGrp ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

        for obj in ( self.rbnRootCtrl , self.rbnEndCtrl ) :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' )

        for obj in ( self.rbnRootJnt , self.rbnMidJnt , self.rbnEndJnt ) :
            obj.attr('v').value = 0

        self.rbnCtrlShape.attr( 'rootTwistAmp' ).hide = 1
        self.rbnCtrlShape.attr( 'endTwistAmp' ).hide = 1

        self.rbnCtrlShape.attr( 'rootAbsoluteTwist' ).hide = 1
        self.rbnCtrlShape.attr( 'endAbsoluteTwist' ).hide = 1

        self.rbnCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

    def ribbonInfo( self , axis = '' ) :

        if axis == 'x+' :
            postAx = 'tx'
            twstAx =  'rx'
            sqshAx = [ 'sy' , 'sz' ]
            aimVec = [ 1 , 0 , 0 ]
            invAimVec = [ -1 , 0 , 0 ]
            upVec = [ 0 , 0 , 1 ]
            rotate = [ 0 , 0 , 90 ]
            rotateOrder = 0
            sineT = ['ty','tz']
            
        elif axis == 'x-' :
            postAx = 'tx'
            twstAx =  'rx'
            sqshAx = [ 'sy' , 'sz' ]
            aimVec = [ -1 , 0 , 0 ]
            invAimVec = [ 1 , 0 , 0 ]
            upVec = [ 0 , 0 , 1 ]
            rotate = [ 0 , 0 , -90 ]
            rotateOrder = 0
            sineT = ['ty','tz']
        
        elif axis == 'y+' :
            postAx = 'ty'
            twstAx =  'ry'
            sqshAx = [ 'sx' , 'sz' ]
            aimVec =  [ 0 , 1 , 0 ]
            invAimVec = [ 0 , -1 , 0 ]
            upVec = [ 0 , 0 , 1 ]
            rotate = [ 0 , 0 , 0 ]
            rotateOrder = 1
            sineT = ['tx','tz']
            
        elif axis == 'y-' :
            postAx = 'ty'
            twstAx =  'ry'
            sqshAx = [ 'sx' , 'sz' ]
            aimVec = [ 0 , -1 , 0 ]
            invAimVec = [ 0 , 1 , 0 ]
            upVec = [ 0 , 0 , 1 ]
            rotate = [ 0 , 0 , 180 ]
            rotateOrder = 1
            sineT = ['tx','tz']
        
        elif axis == 'z+' :
            postAx = 'tz'
            twstAx =  'rz'
            sqshAx = [ 'sx' , 'sy' ]
            aimVec = [ 0 , 0 , 1 ]
            invAimVec = [ 0 , 0 , -1 ]
            upVec = [ 0 , 1 , 0 ]
            rotate = [ 90 , 0 , 180 ]
            sineT = ['tx','ty']
            rotateOrder = 2
        
        elif axis == 'z-' :
            postAx = 'tz'
            twstAx =  'rz'
            sqshAx = [ 'sx' , 'sy' ]
            aimVec = [ 0 , 0 , -1 ]
            invAimVec = [ 0 , 0 , 1 ]
            upVec = [ 0 , 1 , 0 ]
            rotate = [ -90 , 0 , 0 ]
            rotateOrder = 2
            sineT = ['tx','ty']

        return { 'postAx' : postAx , 
                 'twstAx' : twstAx , 
                 'sqshAx' : sqshAx , 
                 'aimVec' : aimVec , 
                 'invAimVec' : invAimVec , 
                 'upVec' : upVec , 
                 'rotate' : rotate , 
                 'rotateOrder' : rotateOrder ,
                 'sineT' : sineT }

    def nonLinearDeformer(self,name = None , side = None , obj = None , defType = None , *args):

        nonL = mc.nonLinear(obj , typ = defType)
        hndNon = mc.rename(nonL[0],'{}Node{}Hnd'.format(name,side))
        hndNonD = mc.rename(nonL[1],'{}NodeDag{}Hnd'.format(name,side))
        
        ver = mc.about(version = True)

        if ver >= "2020":
            hdnShape = (core.Dag(hndNonD)).shape
            return { 'handleDag' : core.Dag(hndNonD) , 
                     'handleNode' : core.Dag(hdnShape)  }


        return { 'handleDag' : core.Dag(hndNonD) , 
                 'handleNode' : core.Dag(hndNon)  }

class RibbonHi( Ribbon ):

    def __init__( self , name           = '' ,
                         axis           = 'y+' ,
                         side           = 'L' ,
                         dist           = 1 ,
                         numJnts        = 5 ,
                         nurbValue      = 5 ,
                         sineDef        = False ,
                         startSpace     = True 
                 ):
        
        super( RibbonHi , self ).__init__( name , axis , side , dist , numJnts , nurbValue , sineDef )

        ##-- Info
        if side == '' :
            side = '_'
        else :
            side = '_{}_'.format(side)

        #-- Create Main Group
        self.rbnDetailGrp = cn.createNode( 'transform' , '{}RbnDtl{}Grp'.format( name , side ))
        self.rbnStillGrp = cn.createNode( 'transform' , '{}RbnStill{}Grp'.format( name , side ))

        #-- Create Attribute
        self.rbnCtrlShape.addAttr( 'detailControl' , 0 , 1 )

        #-- Create Nurbs Surface
         #-- Create Nurbs Surface
        self.rbnNrb = core.Dag(mc.nurbsPlane( p = ( 0 , dist/2 , 0 ) , ax = ( 0 , 0 , 1 ) , w = True , lr = dist , d = 3 , u = 1 , v = nurbValue , ch = 0 , n = '{}Rbn{}Nrb'.format( name , side ))[0])
        self.rbnNrb.attr('r').value = self.rotate
        self.rbnNrb.freeze()
        self.nrbSq = core.Dag(mc.duplicate(self.rbnNrb,n='{}RbnSquash{}Nrb'.format(name,side))[0])

        self.rbnSkc = mc.skinCluster( self.rbnRootJnt , self.rbnMidJnt , self.rbnEndJnt , self.rbnNrb , dr = 7 , mi = 2 , n = '{}Rbn{}Skc'.format( name , side ))[0]
        
        cvValue = nurbValue + 2
        skinPart = cvValue / 2
        skinMiddle = nurbValue / 2.0

        mc.skinPercent(  self.rbnSkc , '{}.cv[0:3][0]'.format(self.rbnNrb) , tv = [ self.rbnRootJnt , 1 ] )
        mc.skinPercent(  self.rbnSkc , '{}.cv[0:3][{}]'.format( self.rbnNrb,cvValue) , tv = [ self.rbnEndJnt , 1 ] )

        if skinPart == (cvValue+1) / 2:
            endRoot = skinPart
            mc.skinPercent(  self.rbnSkc , '{}.cv[0:3][{}]'.format(self.rbnNrb,skinPart) , tv = [ self.rbnMidJnt , 1 ] )
        else:
            endRoot = skinPart+1

        self.animCrv = mc.createNode('animCurveTU' , n ='rbnWeightValue')
        mc.setKeyframe(self.animCrv , t=0 , v = 0)
        mc.setKeyframe(self.animCrv , t =endRoot , v =endRoot)
        mc.keyTangent(self.animCrv , e=True , a=True , t=(0,0) , outAngle=90 , outWeight=(endRoot/4)*0.2)

        mc.keyTangent(self.animCrv , e=True , a=True , t=(endRoot,endRoot) , inAngle=0 , inWeight=(endRoot/4)*3.5)
        for v in range(2,endRoot):
            val = (mc.keyframe(self.animCrv , t=(v-1,v-1) , q=True , ev=True)[0])/endRoot
            mc.skinPercent( self.rbnSkc , '{}.cv[0:3][{}]'.format(self.rbnNrb,v) , tv = [ ( self.rbnRootJnt , 1.0-val), ( self.rbnMidJnt , val)] )

        value = 1
        for v in range(nurbValue,skinPart,-1):
            val = (mc.keyframe(self.animCrv , t=(value,value) , q=True , ev=True)[0])/endRoot
            mc.skinPercent( self.rbnSkc , '{}.cv[0:3][{}]'.format(self.rbnNrb,v) , tv = [ ( self.rbnEndJnt , 1.0-val), ( self.rbnMidJnt , val)] )
            value += 1
        val = (mc.keyframe(self.animCrv , t=(1,1) , q=True , ev=True)[0])/endRoot*0.3
        mc.skinPercent(  self.rbnSkc , '{}.cv[0:3][1]'.format( self.rbnNrb ) , tv = [ (self.rbnRootJnt , 1 - val) , (self.rbnMidJnt , val) ] )
        mc.skinPercent(  self.rbnSkc , '{}.cv[0:3][{}]'.format( self.rbnNrb,cvValue-1) , tv = [ (self.rbnEndJnt , 1 - val) , (self.rbnMidJnt , val) ] )
        mc.delete(self.animCrv)

        #-- Detail Controls
        self.rbnPosGrpDict = []
        self.posiDtlDict = []
        self.rbnDtlAimDict = []
        self.rbnDtlJntDict = []
        self.rbnDtlJntZroDict = []
        self.rbnDtlCtrlDict = []
        self.rbnDtlCtrlZroDict = []
        self.rbnDtlCtrlTwsDict = []
        self.rbnDtlTwsMdvDict = []
        self.rbnDtlTwsPmaDict = []
        self.rbnDtlSqshMdvDict = []
        self.rbnDtlSqshPmaDict = []

        for i in range(0,self.numJnts) :

            pV = ((0.5 / (self.numJnts)) * (i+1) * 2) - ((0.5 / ((self.numJnts) * 2)) * 2)

            if not startSpace:
                pV = (1.0 / (self.numJnts - 1) * i)

            #-- Posi Node
            self.rbnPosGrp = cn.createNode( 'transform' , '{}{}RbnDtlPos{}Grp'.format( name , i+1 , side ))
            self.posi = cn.createNode( 'pointOnSurfaceInfo' , '{}{}RbnDtlPos{}Posi'.format( name , i+1 , side ))
            
            self.posi.attr('turnOnPercentage').value = 1
            self.posi.attr('parameterU').value = 0.5

            self.rbnNrb.attr('worldSpace[0]') >> self.posi.attr('inputSurface')
            self.posi.attr('position') >> self.rbnPosGrp.attr('t')

            self.rbnPosGrpDict.append( self.rbnPosGrp )
            self.posiDtlDict.append( self.posi )

            #-- Aim Constraint Node
            self.rbnAim = cn.createNode( 'aimConstraint' , '{}{}RbnDtl{}aimConstraint'.format( name , i+1 , side ))

            self.rbnAim.attr('ax').value = self.aimVec[0]
            self.rbnAim.attr('ay').value = self.aimVec[1]
            self.rbnAim.attr('az').value = self.aimVec[2]
            self.rbnAim.attr('ux').value = self.upVec[0]
            self.rbnAim.attr('uy').value = self.upVec[1]
            self.rbnAim.attr('uz').value = self.upVec[2]

            self.posi.attr('n') >> self.rbnAim.attr('wu')
            self.posi.attr('tv') >> self.rbnAim.attr('tg[0].tt')
            self.rbnAim.attr('cr') >> self.rbnPosGrp.attr('r')
        
            self.rbnDtlAimDict.append( self.rbnAim )
            mc.parent( self.rbnAim , self.rbnPosGrp )
            
            mc.select( cl = True )

            #-- Create Joint
            self.rbnJnt = cn.joint( '{}{}RbnDtl{}Jnt'.format( name , i+1 , side ))
            self.rbnJntZro = rt.addGrp( self.rbnJnt )
            
            self.rbnDtlJntDict.append( self.rbnJnt )
            self.rbnDtlJntZroDict.append( self.rbnJntZro )

            #-- Create Controls
            self.rbnDtlCtrl = rt.createCtrl( '{}{}RbnDtl{}Ctrl'.format( name , i+1 , side ) , 'circle' , 'cyan' )
            self.rbnDtlCtrlZro = rt.addGrp( self.rbnDtlCtrl )
            self.rbnDtlCtrlTws = rt.addGrp( self.rbnDtlCtrl , 'Twist' )

            self.rbnDtlCtrl.addAttr( 'squash' )
        
            self.rbnDtlCtrlDict.append( self.rbnDtlCtrl )
            self.rbnDtlCtrlZroDict.append( self.rbnDtlCtrlZro )
            self.rbnDtlCtrlTwsDict.append( self.rbnDtlCtrlTws )

            #-- Adjust Shape Controls
            self.rbnDtlCtrl.scaleShape( dist*0.2 )
            self.rbnDtlCtrl.rotateShape( self.rotate[0] , self.rotate[1] , self.rotate[2] )

            #-- Rig Process
            #-- Twist
            mc.parentConstraint( self.rbnPosGrp , self.rbnDtlCtrlZro , mo = False )
            mc.parentConstraint( self.rbnDtlCtrl , self.rbnJntZro , mo = False )

            self.rbnDtlTwsMdv = cn.createNode( 'multiplyDivide' , '{}{}RbnDtlTws{}Mdv'.format( name , i+1 , side ))
            self.rbnDtlTwsPma = cn.createNode( 'plusMinusAverage' , '{}{}RbnDtlTws{}Pma'.format( name , i+1 , side ))

            self.rbnDtlTwsMdvDict.append( self.rbnDtlTwsMdv )
            self.rbnDtlTwsPmaDict.append( self.rbnDtlTwsPma )

            self.rbnRootPma.attr('o1') >> self.rbnDtlTwsMdv.attr('i2x')
            self.rbnEndPma.attr('o1') >> self.rbnDtlTwsMdv.attr('i2y')

            self.rbnDtlTwsMdv.attr('ox') >> self.rbnDtlTwsPma.attr('i1[0]')
            self.rbnDtlTwsMdv.attr('oy') >> self.rbnDtlTwsPma.attr('i1[1]')

            self.rbnDtlTwsPma.attr('o1') >> self.rbnDtlCtrlTws.attr(self.twstAx)

            self.rbnCtrlShape.attr('detailControl') >> self.rbnDtlCtrlZro.attr('v')

            #-- Adjust Detail Controls Position
            self.posiDtlDict[i].attr('parameterV').value = pV
            self.rbnDtlTwsMdvDict[i].attr('i1x').value = 1-pV
            self.rbnDtlTwsMdvDict[i].attr('i1y').value = pV

        #-- Squash
        hndSq = self.nonLinearDeformer(name = '{}RbnSquash'.format(name) , side = side, obj = self.nrbSq , defType = 'squash' )
        dGrp = cn.createNode('transform','{}RbnSquashDel{}Grp'.format(name,side))
        hndSqGrp = cn.createNode('transform' , '{}RbnSquash{}Grp'.format(name,side))
        mc.delete(mc.parentConstraint(hndSq['handleDag'],hndSqGrp))
        mc.parent(hndSq['handleDag'],hndSqGrp)
        mc.delete(dGrp)
        hndSqGrp.attr('r').v = self.rotate
        sqPosMdv = cn.createNode('multiplyDivide','{}RbnSquashPos{}Mdv'.format(name,side))
        scaSqNode = mc.getAttr('{}.sy'.format(hndSq['handleDag']))
        sqPosMdv.attr('i2x').value = 5/scaSqNode
        self.rbnCtrl.attr('squashPosition') >> sqPosMdv.attr('i1x')
        sqPosMdv.attr('ox') >> hndSq['handleDag'].attr('ty')

        BclSq = cn.createNode('blendColors' , '{}RbnAutoSqsh{}Bcl'.format(name,side) , c2r=1 , c2g=0)
        normPma = cn.createNode('plusMinusAverage' , '{}RbnSqshNorm{}Pma'.format(name,side) , op=2)
        facMdv = cn.createNode('multiplyDivide' , '{}RbnSqshFactor{}Mdv'.format(name,side))
        ampMdv = cn.createNode('multiplyDivide' , '{}RbnSqshAmp{}Mdv'.format(name,side) , i2x=0.1)
        normPma.attr('i1[1]').value = dist

        BclSq.attr('opr') >> facMdv.attr('i1x')
        BclSq.attr('opg') >> facMdv.attr('i2x')
        self.rbnLenPma.attr('o1') >> normPma.attr('i1[0]')
        self.rbnCtrl.attr('autoSquash') >> BclSq.attr('blender')
        normPma.attr('o1') >> BclSq.attr('c1r')
        self.rbnCtrl.attr('ampSquash') >> ampMdv.attr('i1x')
        ampMdv.attr('ox') >> BclSq.attr('c1g')
        facMdv.attr('ox') >> hndSq['handleNode'].attr('factor')
        
        SqPosGrp = cn.createNode('transform','{}RbnSquashPos{}Grp'.format(name,side))

        mc.parent(SqPosGrp,self.rbnStillGrp)
        for posi in range(numJnts):
            pV = ((0.5 / (numJnts)) * (posi+1) * 2) - ((0.5 / ((numJnts) * 2)) * 2)
            posSq = cn.createNode('pointOnSurfaceInfo','{}{}RbnSquash{}Pos'.format(name,posi+1,side) , parameterU=1 , parameterV=pV)
            posGrp = cn.createNode('transform','{}{}RbnSquashPos{}Grp'.format(name,posi+1,side))
            pmaSqPos = cn.createNode('plusMinusAverage','{}{}RbnSquashPos{}Pma'.format(name,posi+1,side))

            mc.parent(posGrp,SqPosGrp)
            self.nrbSq.attr('worldSpace[0]') >> posSq.attr('inputSurface')
            posSq.attr('position') >> posGrp.attr('t')

            sqAbsPowPosMdv = cn.createNode('multiplyDivide','{}{}RbnSquashAbsPowPos{}Mdv'.format(name,posi+1,side))
            sqAbsPowPosMdv.attr('op').v = 3
            sqAbsPowPosMdv.attr('i2x').v = 2
            sqAbsSqrPosMdv = cn.createNode('multiplyDivide','{}{}RbnSquashAbsSqrPos{}Mdv'.format(name,posi+1,side))
            sqAbsSqrPosMdv.attr('op').v = 3
            sqAbsSqrPosMdv.attr('i2x').v = 0.5
            posGrp.attr('tx') >> sqAbsPowPosMdv.attr('i1x')
            sqAbsPowPosMdv.attr('ox') >> sqAbsSqrPosMdv.attr('i1x')
            posTx = sqAbsSqrPosMdv.attr('ox').getVal()

            pm.addAttr(pmaSqPos,ln='default',at='double',dv=-posTx,k=True)
            pmaSqPos.attr('default') >> pmaSqPos.attr('i1[0]')
            sqAbsSqrPosMdv.attr('ox') >> pmaSqPos.attr('i1[1]')
            
            pmaSq = cn.createNode('plusMinusAverage','{}{}RbnSquash{}Pma'.format(name,posi+1,side))
            mdvPosSq = cn.createNode('multiplyDivide','{}{}RbnPosSquash{}Mdv'.format(name,posi+1,side) , i2x=10)
            pmaTotalSq = cn.createNode('plusMinusAverage','{}{}RbnTotalSquash{}Pma'.format(name,posi+1,side))

            self.rbnCtrl.attr('squash') >> pmaSq.attr('i1[0]')
            pmaSqPos.attr('o1') >> mdvPosSq.attr('i1x')
            mdvPosSq.attr('ox') >> pmaSq.attr('i1[1]')
            pmaSq.attr('o1') >> pmaTotalSq.attr('i1[0]')

            self.rbnDtlCtrlDict[posi].attr('squash') >> pmaTotalSq.attr('i1[1]')
            pmaSq.attr('i1[2]').value = 1
            pmaTotalSq.attr('o1') >> self.rbnDtlJntDict[posi].attr(self.sqshAx[0])
            pmaTotalSq.attr('o1') >> self.rbnDtlJntDict[posi].attr(self.sqshAx[1])

        ##------sine------##
        if sineDef:
            self.nrbSn = core.Dag(mc.duplicate(self.rbnNrb,n='{}RbnSine{}Nrb'.format(name,side))[0])
            self.rbnCtrl.addTitleAttr( 'Sinewave' )
            self.rbnCtrl.addAttr( 'autoSine' , 0 , 1 )
            self.rbnCtrl.addAttr( 'amplitude' )
            self.rbnCtrl.addAttr( 'offset' )
            self.rbnCtrl.addAttr( 'width' , 0 )
            self.rbnSineWigthMdv = cn.createNode( 'multiplyDivide' , '{}RbnSineWidth{}Mdv'.format( name , side ))
            self.rbnCtrl.attr('width') >> self.rbnSineWigthMdv.attr('i1x')
            self.rbnCtrl.attr( 'width').value = 1
            self.rbnCtrl.addAttr( 'position' )
            self.rbnCtrl.addAttr( 'twist' )
            self.rbnCtrl.addAttr( 'sineLength' )
            self.rbnCtrl.attr( 'sineLength').value = 2
            self.rbnCtrl.addAttr( 'dropoff' , 0 , 1)
            self.rbnCtrl.attr( 'dropoff').value = 1

            hndSn = self.nonLinearDeformer(name = '{}RbnSine'.format(name) , side = side , obj = self.nrbSn , defType = 'sine' )
            dGrp = cn.createNode('transform','{}RbnSineDel{}Grp'.format(name,side))
            hndSnGrp = mc.group(dGrp,n='{}RbnSine{}Grp'.format(name,side))
            mc.delete(mc.parentConstraint(hndSn['handleDag'],hndSnGrp))
            mc.parent(hndSn['handleDag'],hndSnGrp)
            mc.delete(dGrp)
            mc.setAttr(hndSnGrp+'.r' , self.rotate[0] , self.rotate[1] , self.rotate[2])
            scaleSn = hndSn['handleDag'].attr('sx').getVal()

            self.rbnSineWigthMdv.attr('i2x').value = scaleSn
            hndSn['handleNode'].attr('dropoff').value = 1
            
            autoAPma = cn.createNode('plusMinusAverage','{}RbnSineAutoAmp{}Pma'.format(name,side))
            autoOPma = cn.createNode('plusMinusAverage','{}RbnSineAutoOffset{}Pma'.format(name,side))
            autoCnd = cn.createNode('condition','{}RbnSineAutoTime{}Cnd'.format(name,side) , st=1 , cfr=0)
            autoMdv = cn.createNode('multiplyDivide','{}RbnSineAutoTime{}Mdv'.format(name,side) , i2x=0.1)
            self.rbnCtrl.attr('autoSine') >> autoCnd.attr('ft')

            pm.connectAttr('time1.outTime',autoMdv.name+'.input1X')
            autoMdv.attr('ox') >> autoCnd.attr('ctr')
            self.rbnCtrl.attr('autoSine') >> autoAPma.attr('i1[0]')
            self.rbnCtrl.attr('amplitude') >> autoAPma.attr('i1[1]')
            autoAPma.attr('o1') >> hndSn['handleNode'].attr('amplitude')
            self.rbnCtrl.attr('offset') >> autoOPma.attr('i1[0]')
            autoCnd.attr('ocr') >> autoOPma.attr('i1[1]')
            autoOPma.attr('o1') >> hndSn['handleNode'].attr('offset')
            self.rbnSineWigthMdv.attr('ox') >> hndSn['handleDag'].attr('sy')
            self.rbnCtrl.attr('position') >> hndSn['handleDag'].attr('ty')
            self.rbnCtrl.attr('sineLength') >> hndSn['handleNode'].attr('wavelength')
            self.rbnCtrl.attr('dropoff') >> hndSn['handleNode'].attr('dropoff')
            self.rbnCtrl.attr('twist') >> hndSn['handleDag'].attr('ry')

            SnPosGrp = cn.createNode('transform','{}RbnSinePos{}Grp'.format(name,side))
            mc.parent(SnPosGrp,self.rbnStillGrp)
            for posi in range(numJnts):
                pV = ((0.5 / (numJnts)) * (posi+1) * 2) - ((0.5 / ((numJnts) * 2)) * 2)
                posSn = cn.createNode('pointOnSurfaceInfo','{}{}RbnSine{}pos'.format(name,posi+1,side),parameterU=0.5,parameterV=pV)
                posGrp = cn.createNode('transform','{}{}RbnSinePos{}Grp'.format(name,posi+1,side))
                mc.parent(posGrp,SnPosGrp)
                self.nrbSn.attr('worldSpace[0]') >> posSn.attr('inputSurface')
                posSn.attr('position') >> posGrp.attr('t')
                posGrp.attr(self.sineT[0]) >> self.rbnDtlCtrlTwsDict[posi].attr(self.sineT[0])
                posGrp.attr(self.sineT[1]) >> self.rbnDtlCtrlTwsDict[posi].attr(self.sineT[1])
            mc.parent( self.nrbSn , hndSnGrp , self.rbnStillGrp)

        #-- Adjust Hierarchy
        # mc.parent( self.nrbSn , self.nrbSq , hndSnGrp , hndSqGrp , self.rbnStillGrp)
        mc.parent( self.nrbSq , hndSqGrp , self.rbnStillGrp)
        mc.parent( self.rbnNrb , self.rbnPosGrpDict , self.rbnStillGrp )
        mc.parent( self.rbnDtlJntZroDict , self.rbnSkinGrp )
        mc.parent( self.rbnDtlCtrlZroDict , self.rbnDetailGrp )
        mc.parent( self.rbnDetailGrp , self.rbnCtrlGrp )

        #-- Cleanup
        for grp in ( self.rbnStillGrp , self.rbnDetailGrp ) :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

        for dicts in ( self.rbnDtlCtrlZroDict , self.rbnDtlJntZroDict , self.rbnDtlCtrlTwsDict , self.rbnDtlJntDict ) :
            for each in dicts :
                each.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for dtlCtrl in self.rbnDtlCtrlDict :
            dtlCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        mc.select( cl = True )

class RibbonLow( Ribbon ):
    def __init__( self , name   = '' ,
                         axis     = 'y+' ,
                         side   = 'L' ,
                         dist   = 1 
                 ):

        super( RibbonLow , self ).__init__( name , axis , side , dist )



        ##-- Info
        if side == '' :
            side = '_'
        else :
            side = '_{}_'.format(side)

        # #-- Create Main Group
        # self.rbnDetailGrp = cn.createNode( 'transform' , '{}RbnDtl{}Grp'.format( name , side ))
        self.rbnStillGrp = cn.createNode( 'transform' , '{}RbnStill{}Grp'.format( name , side ))


        #-- Create Joint
        self.rbnJnt = cn.joint( '{}Rbn{}Jnt'.format( name , side ))
        self.rbnJntZro = rt.addGrp( self.rbnJnt )
        self.rbnJntOfst = rt.addGrp( self.rbnJnt , 'Ofst' )

        mc.parentConstraint( self.rbnCtrl , self.rbnJntZro )

        #-- Twist Distribute
        self.rbnTwsMdv = cn.createNode( 'multiplyDivide' , '{}RbnTws{}Mdv'.format( name , side ))
        self.rbnTwsPma = cn.createNode( 'plusMinusAverage' , '{}RbnTws{}Pma'.format( name , side ))
        
        self.rbnTwsMdv.attr('i1x').value = 0.5
        self.rbnTwsMdv.attr('i1y').value = 0.5
        
        self.rbnRootPma.attr('o1') >> self.rbnTwsMdv.attr('i2y')
        self.rbnEndPma.attr('o1') >> self.rbnTwsMdv.attr('i2x')
        self.rbnTwsMdv.attr('ox') >> self.rbnTwsPma.attr('i1[0]')
        self.rbnTwsMdv.attr('oy') >> self.rbnTwsPma.attr('i1[1]')
        self.rbnTwsPma.attr('output1D') >> self.rbnJntOfst.attr( self.twstAx )

        #-- Squash
        self.rbnSqshMdv = cn.createNode( 'multiplyDivide' , '{}RbnSqsh{}Mdv'.format( name , side ))
        self.rbnSqshBcl = cn.createNode( 'blendColors' , '{}RbnSqsh{}Bcl'.format( name , side ))
        self.rbnSqshPma = cn.createNode( 'plusMinusAverage' , '{}RbnSqsh{}Pma'.format( name , side ))

        self.rbnSqshMdv.attr('i2x').value = 0.5
        self.rbnCtrl.attr('squash') >> self.rbnSqshMdv.attr('i1x')
        self.rbnCtrl.attr('autoSquash') >> self.rbnSqshBcl.attr('b')
        self.rbnSqshDivMdv.attr('ox') >> self.rbnSqshBcl.attr('c1r')

        self.rbnSqshBcl.attr('c2r').value = 1
        
        self.rbnSqshMdv.attr('ox') >> self.rbnSqshPma.attr('i1[0]')
        self.rbnSqshBcl.attr('opr') >> self.rbnSqshPma.attr('i1[1]')
        self.rbnSqshPma.attr('o1') >> self.rbnJnt.attr( self.sqshAx[0] )
        self.rbnSqshPma.attr('o1') >> self.rbnJnt.attr( self.sqshAx[1] ) 
        

        #-- Adjust Hierarchy
        mc.parent( self.rbnJntZro , self.rbnSkinGrp )

        #-- Adjust Hierarchy
        # mc.parent( self.nrbSn , self.nrbSq , hndSnGrp , hndSqGrp , self.rbnStillGrp)
        # print self.rbnStillGrp, '...self.rbnStillGrp'
        # mc.parent(self.rbnStillGrp, Still_Grp)
        # mc.parent( self.rbnNrb , self.rbnPosGrpDict , self.rbnStillGrp )
        # mc.parent( self.rbnDtlJntZroDict , self.rbnSkinGrp )
        # mc.parent( self.rbnDtlCtrlZroDict , self.rbnDetailGrp )
        # mc.parent( self.rbnDetailGrp , self.rbnCtrlGrp )

        # #-- Cleanup
        # for grp in ( self.rbnStillGrp , self.rbnDetailGrp ) :
        #     grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

        # for dicts in ( self.rbnDtlCtrlZroDict , self.rbnDtlJntZroDict , self.rbnDtlCtrlTwsDict , self.rbnDtlJntDict ) :
        #     for each in dicts :
        #         each.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        # for dtlCtrl in self.rbnDtlCtrlDict :
        #     dtlCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        mc.select( cl = True )