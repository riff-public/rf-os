import maya.cmds as mc
import maya.mel as mm
import core
reload( core )
import rigTools as rt
reload( rt )

cn = core.Node()

def Run( 	jawLwr1Jnt 			= 'bodyRig_md:JawLwr1_Jnt' , 
			jawLwr1ZroGrp 		= 'bodyRig_md:JawLwr1CtrlZro_Grp' ,
			headLwrPosHdRig 	= 'fclRig_md:HeadLwrPosHdRig_Grp' ,
			jawLwr1Ctrl 		= 'bodyRig_md:JawLwr1_Ctrl' ,
			jawUprOfstGrp 		= 'bodyRig_md:JawUpr1CtrlOfst_Grp' ,
			mthHdRigPma    	  	= 'fclRig_md:MouthTotalSqshHdRig_Pma',
			cheekHdRigPma     	= 'fclRig_md:CheekTotalSqshHdRig_Pma',
			side 				= '' ,
			elem 				= '' ):

	if not mc.objExists(headLwrPosHdRig):
		print 'This scene does not have HdRig objects.'
		return

	#-- Info
	elem = elem.capitalize()

	if side:
		side = '_{}_'.format(side)
	else:
		side = '_'

	jawLwr1Jnt = core.Dag(jawLwr1Jnt)
	jawLwr1ZroGrp = core.Dag(jawLwr1ZroGrp)
	headLwrPosHdRig = core.Dag(headLwrPosHdRig)
	jawLwr1Ctrl = core.Dag(jawLwr1Ctrl)
	jawUprOfstGrp = core.Dag(jawUprOfstGrp)
	mthHdRigPma = core.Dag(mthHdRigPma)
	cheekHdRigPma = core.Dag(cheekHdRigPma)

	jawCtrlShape = rt.checkObjExist('{}'.format(jawLwr1Ctrl.shape) , attr='HeadLwrStretchAmp')
	jawCtrlShape.attr('HeadLwrStretchAmp').v = -0.005
	jawCtrlShape = rt.checkObjExist('{}'.format(jawLwr1Ctrl.shape) , attr='HeadLwrSquashAmp')
	jawCtrlShape.attr('HeadLwrSquashAmp').v = -0.01
	jawCtrlShape = rt.checkObjExist('{}'.format(jawLwr1Ctrl.shape) , attr='HeadUprSquashAmp')
	jawCtrlShape.attr('HeadUprSquashAmp').v = 1

	#-- create Amp for jawLwr squash stretch
	sqstCmp = cn.createNode('clamp' , 'JawLwr{}AmpHdRig{}Cmp'.format(elem , side))
	sqstMdv = cn.createNode('multiplyDivide' , 'JawLwr{}AmpHdRig{}Mdv'.format(elem , side))
	lwrPma = cn.createNode('plusMinusAverage' , 'JawLwr{}AmpHdRig{}Pma'.format(elem , side))
	uprPma = cn.createNode('plusMinusAverage' , 'JawUpr{}AmpHdRig{}Pma'.format(elem , side))
	sqstCmp.attr('maxR').v = 100000
	sqstCmp.attr('minG').v = -100000
	sqstCmp.attr('minB').v = -100000

	jawCtrlShape.attr('HeadLwrSquashAmp') >> sqstMdv.attr('i2x')
	jawCtrlShape.attr('HeadLwrStretchAmp') >> sqstMdv.attr('i2y')
	jawCtrlShape.attr('HeadUprSquashAmp') >> sqstMdv.attr('i2z')
	jawLwr1Jnt.attr('rx') >> sqstMdv.attr('i1x')
	jawLwr1Jnt.attr('rx') >> sqstMdv.attr('i1y')
	jawLwr1Jnt.attr('rx') >> sqstMdv.attr('i1z')
	sqstMdv.attr('ox') >> sqstCmp.attr('ipr')
	sqstMdv.attr('oy') >> sqstCmp.attr('ipg')
	sqstMdv.attr('oz') >> sqstCmp.attr('ipb')
	sqstCmp.attr('opr') >> lwrPma.attr('i1[0]')
	sqstCmp.attr('opg') >> uprPma.attr('i1[0]')
	sqstCmp.attr('opb') >> jawUprOfstGrp.attr('rx')
	lwrPma.attr('o1') >> mthHdRigPma.attr('i1[1]')
	lwrPma.attr('o1') >> cheekHdRigPma.attr('i1[1]')
	uprPma.attr('o1') >> mthHdRigPma.attr('i1[2]')
	uprPma.attr('o1') >> cheekHdRigPma.attr('i1[2]')
	# sqstMdv.attr('ox') >> lwrPma.attr('i1[0]')
	# sqstMdv.attr('oy') >> uprPma.attr('i1[0]')
	# lwrPma.attr('o1') >> jawLwr1ZroGrp.attr('ty')
	# sqstMdv.attr('oz') >> jawUprOfstGrp.attr('rx')


	mc.select(cl=True)
