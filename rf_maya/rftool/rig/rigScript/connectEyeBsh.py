import maya.cmds as mc
import maya.mel as mm
import core
reload( core )
import rigTools as rt
reload( rt )

cn = core.Node()

def Run(    headJnt             = 'bodyRig_md:Head_Jnt' , 
            eyeJnts             = ['bodyRig_md:Eye_L_Jnt' , 'bodyRig_md:Eye_R_Jnt'] ,
            eyeBshJnts          = ['fclRig_md:EyeLidsBshRig_L_Jnt' , 'fclRig_md:EyeLidsBshRig_R_Jnt'] ,
            eyeCtrls            = ['bodyRig_md:Eye_L_Ctrl' , 'bodyRig_md:Eye_R_Ctrl'] ):

    if not mc.objExists(eyeBshJnts[0]):
        print 'This scene does not have EyeLidBsh joints.'
        return

    for i , jnt in enumerate(eyeJnts):
        fullName = jnt.split(':')[-1]
        elem = fullName.split('_')

        if len(elem) > 2:
            name = elem[0]
            side = '_{}_'.format(elem[-1])
        else:
            name = elem[0]
            side = '_'

        rev = cn.createNode('reverse' , '{}{}Rev'.format(name , side))
        parCon = mc.parentConstraint(jnt , headJnt , eyeBshJnts[i] , mo=1)[0]
        mc.connectAttr('{}.eyelidsFollow'.format(eyeCtrls[i]) , '{}.input.inputX'.format(rev))
        mc.connectAttr('{}.eyelidsFollow'.format(eyeCtrls[i]) , '{}.{}W0'.format(parCon , fullName))
        mc.connectAttr('{}.outputX'.format(rev) , '{}.{}W1'.format(parCon , headJnt.split(':')[-1]))
        mc.setAttr('{}.eyelidsFollow'.format(eyeCtrls[i]) , 1 )