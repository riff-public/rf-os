import maya.cmds as mc
import maya.mel as mm
import core
reload( core )
import rigTools as rt
reload( rt )
from functools import partial

from rf_utils import file_utils
reload(file_utils)

#-- build replaced weight ui

class Run(object):
    def __init__(self):
        self.winName = 'replacedWeightUI'

    def show(self , *args):
        # check if window exists
        if mc.window ( self.winName , exists = True ) :
            mc.deleteUI ( self.winName ) 
        else : pass

        winWidth = 500

        SourceJntsList = []
        TargetJntsList = []
        mc.window(self.winName,title = 'Replaced Weight' , width = winWidth , rtf = True, mxb = False , s=False)

        mainCL = mc.columnLayout()
        mc.rowLayout( w = winWidth , p = mainCL , nc=2 , h=20)
        mc.text(label = 'Source Jnts :' , w=winWidth*0.5)
        mc.text(label = 'Target Jnts :', w=winWidth*0.5)
        mc.setParent('..')

        mc.rowColumnLayout( w = winWidth , p = mainCL , nc = 3 , cw = [(1, winWidth*.475) , (2,winWidth*.045) , (3, winWidth*.475)]) ;
        mc.rowColumnLayout( nc = 1  )
        mc.textScrollList('SourceJntsScroll' ,  
                                    numberOfRows=8, 
                                    allowMultiSelection=True,
                                    append=SourceJntsList,
                                    showIndexedItem=4, 
                                    h=130)
        mc.setParent('..')
        mc.rowColumnLayout( nc = 1  )
        mc.setParent('..')
        mc.rowColumnLayout( nc = 1  )
        mc.textScrollList('TargetJntsScroll' ,  
                                    numberOfRows=8, 
                                    allowMultiSelection=True,
                                    append=TargetJntsList,
                                    showIndexedItem=4, 
                                    h=130)
        mc.setParent('..')

        mc.separator(style='none' , h=10)
        mc.setParent('..')

        mc.rowLayout(numberOfColumns=4)
        mc.button('LoadSourceJnts'  ,label = 'Load Source Joints', w=.2325*winWidth , h = 20 , c = self.loadSourceJnts)
        mc.button('ClearSourceJnts'  ,label = 'Clear ', w=.2325*winWidth , h = 20 , c = partial(self.clearScroll,'SourceJntsScroll'))
        mc.text(l='' , w=winWidth*.05)
        mc.button('ClearTargetJnts'  ,label = 'Clear ', w=.468*winWidth , h = 20 , c = partial(self.clearScroll,'TargetJntsScroll'))

        mc.setParent('..')
           
        mc.rowLayout(numberOfColumns=9)
        mc.button('AddSourceJnts'  ,label = 'Add', w=.115*winWidth , h = 30 , c = partial(self.addToDisplayGeo,'SourceJntsScroll'))
        mc.button('RemoveSourceJnts' ,label = 'Del' , w=.115*winWidth , h = 30 ,c=partial(self.removeGeo,'SourceJntsScroll'))
        mc.button('UpSourceJnts'  ,l = 'Up', w=.115*winWidth , h = 30 , c = partial(self.upScroll,'SourceJntsScroll'))
        mc.button('DownSourceJnts'  ,l = 'Down', w=.115*winWidth , h = 30 , c = partial(self.downScroll,'SourceJntsScroll'))  
        mc.text(l='' , w=winWidth*.05)
        mc.button('AddTargetJnts'  ,l = 'Add', w=.115*winWidth , h = 30 , c = partial(self.addToDisplayGeo,'TargetJntsScroll'))
        mc.button('RemoveTargetJnts' ,l = 'Del' , w=.115*winWidth , h = 30 ,c=partial(self.removeGeo,'TargetJntsScroll'))
        mc.button('UpTargetJnts' ,l = 'Up' , w=.115*winWidth , h = 30 , c = partial(self.upScroll,'TargetJntsScroll'))
        mc.button('DownTargetJnts' ,l = 'Down' , w=.115*winWidth , h = 30 , c = partial(self.downScroll,'TargetJntsScroll'))  
     
        mc.setParent('..')

        mc.separator(style='none' , h=10)
        writeReadRow = mc.rowColumnLayout( nc=3 , cw=[(1 , winWidth*0.49),(2 , winWidth*0.017),(3 , winWidth*0.49)] )
        mc.button('writeJoints' ,l = 'Write Joints' , w=winWidth*0.5 , h = 50 , c = self.writeReplaceJoint, p = writeReadRow)
        mc.separator(style='none')
        mc.button('readJoints' ,l = 'Read Joints' , w=winWidth*0.5 , h = 50 , c =self.readReplaceJoint , p = writeReadRow)
        mc.setParent('..')

        mc.separator(style='none' , h=10)
        mc.button('readReplaceWeight' ,l = 'Read Replace Weights' , w=winWidth , h = 50 , c = self.doReadReplaceWeight)  
        mc.separator(style='none' , h=10)
        mc.button('transferWeight' ,l = 'Transfer Weights' , w=winWidth , h = 50 , c = self.doTransferWeight)  
        mc.showWindow()

    def addToDisplayGeo(self,scrollName,*args):
        jntList = mc.textScrollList(scrollName,q=1,ai=1)
        sel = mc.ls(sl=True)
        for each in sel:
            if not jntList:
                mc.textScrollList(scrollName ,edit=True,append=['{}'.format(each)])
            else:
                if not each in mc.textScrollList(scrollName,q=1,ai=1):
                    mc.textScrollList(scrollName ,edit=True,append=['{}'.format(each)])

    def removeGeo(self,scrollName , *args):
        List = mc.textScrollList( scrollName ,q=True, si=True)

        for each in List:
            mc.textScrollList(scrollName ,edit=True , ri='{}'.format(each))

    def loadSourceJnts(self , *args):
        import weightTools as wt 
        reload(wt)
        import pickle
        infsList = mc.textScrollList('SourceJntsScroll',q=1,ai=1)
        sel = mc.ls(sl=True)
        for each in sel:
            path = wt.getDataFld()
            fn = '{}/{}Weight.txt'.format(path , each)
            fid = open(fn , 'r')
            wDct = pickle.load(fid)
            fid.close()
            infs = wDct['influences']
            for inf in infs:
                if not infsList:
                    mc.textScrollList('SourceJntsScroll' ,edit=True,append=['{}'.format(inf)]) 
                else:
                    if not inf in mc.textScrollList('SourceJntsScroll',q=1,ai=1):
                        mc.textScrollList('SourceJntsScroll' ,edit=True,append=['{}'.format(inf)])

    def clearScroll(self , scrollName , *args):
        mc.textScrollList(scrollName ,edit=True,ra=True) 


    def upScroll(self,scrollName , *args):
        intList = mc.textScrollList(scrollName,q=1,sii=1)
        strList = mc.textScrollList(scrollName,q=1,si=1)
        selList = []
        for num in range(len(strList)):
            idx = intList[num]
            if 1 not  in intList:
                if idx  >= 2:
                    mc.textScrollList(scrollName,e=1,rii=idx)
                    mc.textScrollList(scrollName,e=1,ap=[idx-1,strList[num]])
                    selList.append(idx-1)

                else:
                    selList.append(idx)
                    pass

                mc.textScrollList(scrollName,e=1,sii=selList)
            else:
                mc.textScrollList(scrollName,e=1,sii = intList)

    def downScroll(self,scrollName,*args):
            intList = mc.textScrollList(scrollName,q=1,sii=1)
            strList = mc.textScrollList(scrollName,q=1,si=1)
            maxNum = mc.textScrollList(scrollName,q=1,ni=1)
            selList = []

            for num in range(len(strList))[::-1]:
                idx = intList[num]
                if maxNum not in intList:
                    if idx  <= maxNum-1:
                        #print 'idx : {} < strList : {}'.format(idx+1,strList[num])
                        mc.textScrollList(scrollName,e=1,rii=idx)
                        mc.textScrollList(scrollName,e=1,ap=[idx+1,strList[num]])
                        selList.append(idx+1)
                    else:
                        #print 'there something in this case : idx: {} and strList :{}'.format(idx+1,strList[num])
                        selList.append(idx)

                    mc.textScrollList(scrollName,e=1,sii=selList)
                else:
                    mc.textScrollList(scrollName,e=1,sii = intList)

    def doReadReplaceWeight(self , *args):
        import weightTools as wt 
        reload(wt)
        srcList = mc.textScrollList('SourceJntsScroll',q=1,ai=1)
        trgList = mc.textScrollList('TargetJntsScroll',q=1,ai=1)
        if not len(srcList) == len(trgList):
            print 'len source joints are not equal to target joints.'
            return
        wt.readSelectedWeightReplace(searchFor=srcList , replaceWith=trgList)


    def doTransferWeight(self , *args):
        import weightTools as wt 
        reload(wt)
        srcList = mc.textScrollList('SourceJntsScroll',q=1,ai=1)
        trgList = mc.textScrollList('TargetJntsScroll',q=1,ai=1)
        if not len(srcList) == len(trgList):
            print 'len source joints are not equal to target joints.'
            return
        wt.transferSelectedWeight(searchFor=srcList , transferTo=trgList)


    def writeReplaceJoint(self, *args):
        import weightTools as wt 
        reload(wt)

        jointList = {}
        jointList['sourceJoint'] = mc.textScrollList('SourceJntsScroll', q = 1, ai =1)
        jointList['targetJoint'] = mc.textScrollList('TargetJntsScroll', q = 1, ai =1)

        path = wt.getDataFld()
        file_utils.ymlDumper("{}/replaceWeightJoint.yml".format(path), jointList) 


    def readReplaceJoint(self, *args):
        import weightTools as wt 
        reload(wt)

        mc.textScrollList('SourceJntsScroll' ,edit=True,ra=True) 
        mc.textScrollList('TargetJntsScroll' ,edit=True,ra=True) 

        path = wt.getDataFld()
        jointList = file_utils.ymlLoader("{}/replaceWeightJoint.yml".format(path)) 

        mc.select(cl = True)
        mc.select(jointList['sourceJoint'])
        self.addToDisplayGeo('SourceJntsScroll')

        mc.select(cl = True)
        mc.select(jointList['targetJoint'])
        self.addToDisplayGeo('TargetJntsScroll')
