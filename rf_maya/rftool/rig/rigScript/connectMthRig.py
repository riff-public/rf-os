import maya.cmds as mc
import maya.mel as mm
import core
reload( core )
import rigTools as rt
reload( rt )

def Run(    mouthBshCnrL        = 'fclRig_md:MouthCnrBshRig_L_Ctrl' , 
            mouthBshCnrR        = 'fclRig_md:MouthCnrBshRig_R_Ctrl' ,
            mouthLoc            = 'fclRig_md:mthJawLwr1_Loc' ,
            jawLwr1Jnt          = 'bodyRig_md:JawLwr1_Jnt' ,
            jawLwr2Jnt          = 'bodyRig_md:JawLwr2_Jnt' ,
            jawLwr1MthJnt       = 'fclRig_md:mthJawLwr1_Jnt' ,
            jawLwr2MthJnt       = 'fclRig_md:mthJawLwr2_Jnt'  ):

    if not mc.objExists(jawLwr1MthJnt):
        print 'This scene does not have MthRig objects.'
        return

    if mc.objExists(mouthBshCnrL):
        mouthBshCnrL = core.Dag(mouthBshCnrL)
        mouthBshCnrR = core.Dag(mouthBshCnrR)
        mouthLoc = core.Dag(mouthLoc)

        mouthBshCnrL.attr('LipsSeal') >> mouthLoc.attr('LipsSealL')
        mouthBshCnrR.attr('LipsSeal') >> mouthLoc.attr('LipsSealR')

    rt.connectTRS(obj = jawLwr1Jnt , target = jawLwr1MthJnt)
    rt.connectTRS(obj = jawLwr2Jnt , target = jawLwr2MthJnt)

    mc.select(cl=True)