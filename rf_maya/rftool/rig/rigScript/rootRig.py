import maya.cmds as mc
import maya.mel as mm
import core
import rigTools as rt
import fkRig
reload(fkRig)
reload( core )
reload( rt )

cn=core.Node()

# -------------------------------------------------------------------------------------------------------------
#
#  ROOT RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , rootTmpJnt = 'Root_TmpJnt' , 
                         ctrlGrp    = 'Ctrl_Grp' , 
                         skinGrp    = 'Skin_Grp' , 
                         size       = 1 
                 ):

        #-- Create Controls & Joint
        self.root_obj = fkRig.Run(name      = 'Root' ,
                                 tmpJnt     = rootTmpJnt ,
                                 ctrlGrp    = ctrlGrp , 
                                 shape      = 'circle' ,
                                 jnt_ctrl   = 1 ,
                                 color      = 'green',
                                 localWorld = 0,
                                 constraint = 0,
                                 jnt = 1,
                                 dtl = 0)


        #-- Adjust Shape Controls
        for obj in ( self.root_obj.ctrl , self.root_obj.gmbl ) :
            obj.scaleShape( size * 2.3 )

        #-- Adjust Rotate Order
        for obj in ( self.root_obj.jnt , self.root_obj.ctrl ) :
            obj.setRotateOrder( 'xzy' )
        
        #-- Adjust Hierarchy
        #mc.parent( self.root_obj.jnt , skinGrp )

        #-- Constraint Parent
        self.root_obj.ctrl_obj.do_constraint('parent')

        #-- Assign RootJnt
        self.rootJnt = self.root_obj.jnt

        #-- Cleanup
        for obj in ( self.root_obj.ctrlGrp , self.root_obj.zro ,self.root_obj.ofst ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for obj in ( self.root_obj.ctrl , self.root_obj.gmbl ) :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' )
        
        mc.select( cl = True )