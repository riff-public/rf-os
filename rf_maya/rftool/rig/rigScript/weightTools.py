import maya.cmds as mc
import maya.mel as mm
import pickle
import os , sys
from collections import defaultdict
from rigScript import core
reload( core )
from rigScript import rigTools as rt
reload( rt )
from rf_utils.context import context_info


def getDataFld():
	context = context_info.ContextPathInfo()
	if context.name:
		projectPath = os.environ['RFPROJECT']
		fileWorkPath = (context.path.name()).replace('$RFPROJECT',projectPath)
		path = fileWorkPath + '/rig/rigData'
		if not os.path.isdir(path):
			os.mkdir(path)

		path = '{}/skinWeight'.format(path)
		if not os.path.isdir(path):
			os.mkdir(path)
		return path
 
	filePath = mc.file(q=True , loc=True)
	if not filePath:
		print "Please save file."
		return

	elemPath = filePath.split('/')
	localPath = '/'.join(elemPath[0:-1])
	path = '{}/rigData'.format(localPath)
	if not os.path.isdir(path):
		os.mkdir(path)

	sknPath = '{}/skinWeight'.format(path)
	if not os.path.isdir(sknPath):
		os.mkdir(sknPath)
	return sknPath

def writeWeight(geo = '' , fn = ''):
	skn = mm.eval('findRelatedSkinCluster("{}")'.format(geo))

	if skn:
		sknMeth = mc.getAttr('{}.skinningMethod'.format(skn))
		infs = mc.skinCluster(skn , q=True , inf=True)
		sknSet = mc.listConnections('{}.message'.format(skn) , d=True , s=False)[0]
		print fn
		fid = open(fn , 'w')
		wDct = {}
		wDct['influences'] = infs
		wDct['name'] = skn
		wDct['set'] = sknSet
		wDct['skinningMethod'] = sknMeth

		for ix in xrange(mc.polyEvaluate( geo , v=True)):
			crntVtx = '{}.vtx[{}]'.format(geo , ix)
			sknVal = mc.skinPercent(skn , crntVtx , q=True , v=True)
			wDct[ix] = sknVal
		pickle.dump(wDct , fid)
		fid.close

	else:
		print '{} has no related skinCluster node.'.format(geo)

def writeSelectedWeight(userSuffix = ''):
	for sel in mc.ls(sl=True):
		dataFld = getDataFld()
		if userSuffix:
			userSuffix_str ='_{}'.format(userSuffix)
		else:
			userSuffix_str = ''

		fn = '{}/{}Weight{}.txt'.format(dataFld , sel, userSuffix_str)
		writeWeight(sel , fn)

	mc.confirmDialog( title='Progress' , message='Exporting weight has done.' )

def readWeight(geo = '' , fn = '' , force = False):
	print 'Loading {}'.format(fn)
	fid = open(fn , 'r')
	wDct = pickle.load(fid)
	fid.close()

	infs = wDct['influences']

	lostJnt = []
	for inf in infs:
		if not mc.objExists(inf):
			print 'Scene has no {}'.format(inf)
			lostJnt.append(inf)

	if lostJnt:
		if force:
			for jnt in lostJnt:
				cn.createNode('joint' , jnt)
	
	oSkn = mm.eval('findRelatedSkinCluster "{}"'.format(geo))
	if oSkn:
		mc.skinCluster(oSkn , e=True , ub=True)

	tmpSkn = mc.skinCluster(infs , geo , tsb=True)[0]
	skn = mc.rename(tmpSkn , wDct['name'])
	mc.setAttr('{}.skinningMethod'.format(skn) , wDct['skinningMethod'])

	sknSet = mc.listConnections('{}.message'.format(skn) , d=True , s=False)[0]
	mc.rename(sknSet , wDct['set'])

	for inf in infs:
		mc.setAttr('{}.liw'.format(inf) , False)

	mc.setAttr('{}.normalizeWeights'.format(skn) , False)
	mc.skinPercent(skn , geo , nrm=False , prw=100)
	mc.setAttr('{}.normalizeWeights'.format(skn) , True)

	vtxNo = mc.polyEvaluate(geo , v=True)
	for ix in xrange(vtxNo):
		for iy in xrange(len(infs)):
			wVal = wDct[ix][iy]
			if wVal:
				wlAttr = '{}.weightList[{}].weights[{}]'.format(skn , ix , iy)
				mc.setAttr(wlAttr , wVal)

		#-- Percent calculation
		if ix == (vtxNo-1):
			print '100%% done.'
		else:
			prePrcnt = 0
			if ix > 0:
				prePrcnt = int((float(ix-1) / vtxNo) * 100.00)

			prcnt = int((float(ix) / vtxNo) * 100.00)

			if not prcnt == prePrcnt:
				print '{}%% done.'.format(prcnt)

def readWeightReplace(geo='' , fn='' , searchFor='' , replaceWith='' , force = False):
	print 'Loading {}'.format(fn)
	fid = open(fn , 'r')
	wDct = pickle.load(fid)
	fid.close()

	infs = wDct['influences']

	crntInfs = []
	for inf in infs :
		crntInf = inf 
		if searchFor:
			for idx in range(len(searchFor)):
				if inf == searchFor[idx]:
					crntInf = inf.replace(searchFor[idx] , replaceWith[idx])

		if not mc.objExists(crntInf):
			print 'Scene has no {}'.format(crntInf)
			if force:
				cn.createNode('joint' , crntInf)

		crntInfs.append(crntInf)

	oSkn = mm.eval( 'findRelatedSkinCluster "%s"' % geo )
	if oSkn:
		mc.skinCluster(oSkn , e=True , ub=True)

	tmpSkn = mc.skinCluster(crntInfs , geo , tsb=True)[0]
	skn = mc.rename(tmpSkn , wDct['name'])
	mc.setAttr('{}.skinningMethod'.format(skn) , wDct['skinningMethod'])

	sknSet = mc.listConnections('{}.message'.format(skn) , d=True , s=False)[0]
	mc.rename(sknSet , wDct['set'])

	for currInf in crntInfs:
		mc.setAttr('{}.liw'.format(currInf) , False)

	mc.setAttr('{}.normalizeWeights'.format(skn) , False)
	mc.skinPercent(skn , geo , nrm=False , prw=100)
	mc.setAttr('{}.normalizeWeights'.format(skn) , True)

	vtxNo = mc.polyEvaluate(geo , v=True)
	for ix in xrange(vtxNo):
		for iy in xrange(len(crntInfs)):
			wVal = wDct[ix][iy]
			if wVal:
				wlAttr = '{}.weightList[{}].weights[{}]'.format(skn , ix , iy)
				mc.setAttr(wlAttr , wVal)

		#-- Percent calculation
		if ix == (vtxNo-1):
			print '100%% done.'
		else:
			prePrcnt = 0
			if ix > 0:
				prePrcnt = int((float(ix-1) / vtxNo) * 100.00)

			prcnt = int((float(ix) / vtxNo) * 100.00)

			if not prcnt == prePrcnt:
				print '{}%% done.'.format(prcnt)

def readSelectedWeight(weightFolderPath = '' , force = False, userSuffix = ''):

	if not weightFolderPath:
		weightFolderPath = getDataFld()
	if userSuffix:
		userSuffix_str = '_{}'.format(userSuffix)
	else:
		userSuffix_str = ''

	sels = mc.ls(sl=True)
	for sel in sels:
		fn = '{}/{}Weight{}.txt'.format(weightFolderPath , sel, userSuffix_str)
		try:
			readWeight(sel , fn , force)
			print 'Import {} done!!.'.format(fn)
		except:
			print 'Cannot find weight file for {}'.format(sel)

	mc.select(sels)
	mc.confirmDialog( title='Progress' , message='Importing weight is done!!.' )

def readSelectedWeightReplace(weightFolderPath='' , searchFor='' , replaceWith='' , force=False):

	if not weightFolderPath:
		weightFolderPath = getDataFld()
	
	sels = mc.ls(sl=True)
	for sel in sels:
		fn = '{}/{}Weight.txt'.format(weightFolderPath , sel)
		try:
			readWeightReplace(sel , fn , searchFor , replaceWith , force)
			print 'Import {} done!!.'.format(fn)
		except:
			print 'Cannot find weight file for {}'.format(sel)

	mc.select(sels)
	mc.confirmDialog( title='Progress' , message='Importing weight is done!!.' )

def copySelectedWeight():
	# Copy skin weights from source object to target object.
	# Select source geometry then target geometry then run script.
	sels = mc.ls( sl=True )
	selsMas = sels[0]
	seleChi = sels[1:]
	for sel in seleChi:
		jnts = mc.skinCluster( selsMas , q = True , inf = True )
		
		oSkn = mm.eval( 'findRelatedSkinCluster( "{}" )'.format(sel) )
		if oSkn :
			mc.skinCluster( oSkn , e = True , ub = True )
		
		skn = mc.skinCluster( jnts , sel , tsb = True )[0]
				
		mc.select( selsMas , r=True )
		mc.select( sel , add=True )
		mc.copySkinWeights(nm = True , sa = 'closestPoint' , ia = ('oneToOne','oneToOne'))        #mm.eval( 'copySkinWeights  -noMirror -surfaceAssociation closestPoint -influenceAssociation oneToOne -influenceAssociation oneToOne;' )
		mc.select(cl=True)


def transferSelectedWeight(searchFor='' , transferTo=''):
	# Copy skin weights from source object to target object.
	# Select source geometry then target geometry then run script.
	geo = mc.ls( sl=True )[0]

	skn = mm.eval('findRelatedSkinCluster("{}")'.format(geo))

	if skn:
		infs = mc.skinCluster(skn , q=True , inf=True)
		for inf in infs:
			mc.setAttr('{}.liw'.format(inf) , 1)
		mc.skinCluster(skn , ai = transferTo , lw=True , edit=True , wt=False)

		allJnts = mc.skinCluster(skn , q=True , inf=True)
		for jnt in allJnts:
			mc.setAttr('{}.liw'.format(jnt) , 1)

		for i in range(len(infs)):
			if infs[i] in searchFor:
				idx = searchFor.index(infs[i])
				mc.setAttr('{}.liw'.format(infs[i]) , 0)
				mc.setAttr('{}.liw'.format(transferTo[idx]) , 0)
				mc.skinPercent(skn, '{}.vtx[:]'.format(geo), transformValue=[('{}'.format(transferTo[idx]), 1)])
				mc.skinCluster(skn , inf=infs[i] , e=True , lw=True)
				mc.skinCluster(skn , inf=transferTo[idx] , e=True , lw=True)
				mc.skinCluster(skn , e=True , ri=infs[i])
	else:
		print "This object doesn't have skinCluster node."
		return

	mc.select(geo)

def addSkinDeformer(geo='',flPath='',ns = '',searchFor='',replaceWith = ''):
	'Loading %s' % flPath
	flObj = open(flPath, 'r')
	wDct = pickle.load(flObj)
	flObj.close()    

	infs = wDct['influences']

	crntInfs = []
	for inf in infs :
		if ns:
			crntInf = ns+':' +inf.replace(searchFor , replaceWith)
		else:
			crntInf = inf.replace(searchFor , replaceWith)

		if not mc.objExists(crntInf):
			print 'Scene has no {}'.format(crntInf)

		crntInfs.append(crntInf)

	tmpSkn = mc.deformer(crntInfs , geo , foc=1,type='skinCluster' )[0]
	skn = mc.rename(tmpSkn , wDct['name'])
	mc.setAttr('{}.skinningMethod'.format(skn) , wDct['skinningMethod'])

	# sknSet = mc.listConnections('{}.message'.format(skn) , d=True , s=False)[0]
	# mc.rename(sknSet , wDct['set'])

	# for currInf in crntInfs:
	# 	mc.setAttr('{}.liw'.format(currInf) , False)

	mc.setAttr('{}.normalizeWeights'.format(skn) , False)
	mc.skinPercent(skn , geo , nrm=False , prw=100)
	mc.setAttr('{}.normalizeWeights'.format(skn) , True)

	for num,jnt in enumerate(crntInfs):
		mc.connectAttr(jnt+'.worldMatrix' , skn+'.matrix[{}]'.format(num))
		mc.setAttr(skn+'.bindPreMatrix[{}]'.format(num),mc.getAttr(jnt+'.worldInverseMatrix'),type='matrix')

	vtxNo = mc.polyEvaluate(geo , v=True)
	for ix in xrange(vtxNo):
		for iy in xrange(len(crntInfs)):
			wVal = wDct[ix][iy]
			if wVal:
				wlAttr = '{}.weightList[{}].weights[{}]'.format(skn , ix , iy)
				mc.setAttr(wlAttr , wVal)

		#-- Percent calculation
		if ix == (vtxNo-1):
			print '100%% done.'
		else:
			prePrcnt = 0
			if ix > 0:
				prePrcnt = int((float(ix-1) / vtxNo) * 100.00)

			prcnt = int((float(ix) / vtxNo) * 100.00)

			if not prcnt == prePrcnt:
				print '{}%% done.'.format(prcnt)

def readWeightToSkinCluster(geo='', flPath='',prefix ='',suffix= '',searchFor ='',replaceWith =''):
	'Loading %s' % flPath
	flObj = open(flPath, 'r')
	wDct = pickle.load(flObj)
	flObj.close()

	sknName = '{}Weight.txt'.format(geo)


# def readSelectedWeightToSkinCLuster(userSuffix = ''):
#     sels = mc.ls(sl=True)
#     if userSuffix:
#         userSuffix_str = '_%s'%(userSuffix)
#     else:
#         userSuffix_str = ''
#         print userSuffix_str
#     for sel in sels:
#         dataFld = getDataFld()
#         flNm = '%sWeight%s.dat' % (sel,userSuffix_str)
#         skn = '%s_skn%s' % (sel, userSuffix_str)
#         print skn

#         flPath = os.path.join(dataFld, flNm)
#         print flPath
#         try:
#             print
#             'Importing %s.' % sel
#             readWeight_to_SkinCluster(sel, skn, flPath)
#             #readWeight(sel, os.path.join(dataFld, flNm), searchFor, replaceWith, prefix, suffix)

#             'Importing %s done.' % flNm
#         except:
#             print
#             'Cannot find weight file for %s' % sel

#     mc.select(sels)
#     mc.confirmDialog(title='Progress', message='Importing weight is done.')


def writeNrbsWeight(obj = '',fn=''):
	skn = mm.eval('findRelatedSkinCluster "{}"'.format(obj))
	print skn
	if skn:
		infs = mc.skinCluster(skn,inf=1,q=1)
		wDict = {}
		wDict['infs'] = infs[:]
		wDict['cv'] = {}
		for cv in mc.ls(obj+'.cv[:][:]',fl=1):
			print cv
			v_list = mc.skinPercent(skn,cv,q=1,v=1)
			wDict['cv'][cv] = []
			for i in range(len(infs)):
				wDict['cv'][cv].append([infs[i],v_list[i]])
		print wDict
		print fn
		with open(fn,'w') as f:

			pickle.dump(wDict,f)
			return wDict
	else:
		print '{} has not related Skincluster node'.format(skn)
	

def writeSelectedNrbsWeight(userSuffix = ''):
	for sel in mc.ls(sl=True):
		dataFld = getDataFld()
		if userSuffix:
			userSuffix_str ='_{}'.format(userSuffix)
		else:
			userSuffix_str = ''
		if ':' in sel:
			sel_fn = sel.replace(':','')
		else:
			sel_fn = sel
		print '{}/{}Weight{}.txt'.format(dataFld , sel_fn, userSuffix_str)
		fn = '{}/{}Weight{}.txt'.format(dataFld , sel_fn, userSuffix_str)
		print fn
		wDict = writeNrbsWeight(sel , fn)
		return wDict
	mc.confirmDialog( title='Progress' , message='Exporting weight has done.' )

def readNrbsWeightDict(obj = '',wDict=''):
	oSkn = mm.eval('findRelatedSkinCluster {};'.format(obj))
	if oSkn:
		mc.delete(oSkn)
	skn = mc.skinCluster(wDict['infs'], obj, tsb=1)[0]
	for i in wDict['cv'].keys():
		mc.skinPercent(skn,i, tv=wDict['cv'][i])

def readNrbsWeight(obj ='', fn =''):
	with open(fn,'r') as f:
		wDict = pickle.load(f)
		readNrbsWeightDict(obj,wDict)


def readSelectedNrbsWeight(weightFolderPath = '' , userSuffix = ''):

	if not weightFolderPath:
		weightFolderPath = getDataFld()
	if userSuffix:
		userSuffix_str = '_{}'.format(userSuffix)
	else:
		userSuffix_str = ''

	sels = mc.ls(sl=True)
	for sel in sels:
		if ':' in sel:
			sel_fn = sel.replace(':','')
		else:
			sel_fn = sel

		fn = '{}/{}Weight{}.txt'.format(weightFolderPath , sel_fn, userSuffix_str)
		print fn
		try:
			readNrbsWeight(sel , fn )
			print 'Import {} done!!.'.format(fn)
		except Exception as e:
			print 'Cannot find weight file for {}'.format(sel)
			print e

	mc.select(sels)
	mc.confirmDialog( title='Progress' , message='Importing weight is done!!.' )
