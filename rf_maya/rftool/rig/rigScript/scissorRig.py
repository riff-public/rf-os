import maya.cmds as mc
import core
reload( core )
import rigTools as rt
reload( rt )
import fkRig
reload(fkRig)
import ctrlRig
reload(ctrlRig)
cn = core.Node()

#-------------------------------------------------------------------------------------------

class Run( object ):
	def __init__( self ,	name 				= 'ScissorAA',
							rootScissor			= 'ScissorPlateRootAA_TmpJnt' ,
							endScissor 			= 'ScissorPlateAA_TmpJnt' ,
							scissorArmList		= ['ScissorAAA','ScissorAAB','ScissorAAC'],
							scissorPipeList		= ['ScissorPipeAA'] ,
							ctrlGrp 			= '' ,
							ikhGrp 				= '' ,
							parent 				= 'Root_Jnt' ,
							elem				= '' ,
							side				= '' ):

		#-- Info
		elem = elem.capitalize()

		if side == '':
			self.side = '_'
		else:
			self.side = '_{}_'.format(side)

		self.ctrlGrp = cn.createNode('transform','{}{}Ctrl{}Grp'.format( name , elem , self.side ))
		self.ikhGrp = cn.createNode('transform','{}{}Ikh{}Grp'.format( name , elem , self.side ))

		self.root = fkRig.Run(	name       = '{}{}Root'.format(name , elem) ,
								side       = side ,
								tmpJnt     = rootScissor ,
								parent     = parent ,
								ctrlGrp    = self.ctrlGrp , 
								shape      = 'cube' ,
								color      = 'blue')
		mc.scaleConstraint(parent,self.root.zro)

		for i in range(len(scissorArmList)):
			obj_name = scissorArmList[i]
			self.scrRootJnt = cn.joint('{}{}1{}Jnt'.format(obj_name , elem , self.side),'{}1{}TmpJnt'.format(obj_name,self.side))
			self.scrMidJnt = cn.joint('{}{}2{}Jnt'.format(obj_name , elem , self.side),'{}2{}TmpJnt'.format(obj_name,self.side))
			self.scrEndJnt = cn.joint('{}{}3{}Jnt'.format(obj_name , elem , self.side),'{}3{}TmpJnt'.format(obj_name,self.side))
			self.scrEndJnt.parent(self.scrMidJnt)
			self.scrMidJnt.parent(self.scrRootJnt)

			if not i == (len(scissorArmList))-1:
				jntEnd = '{}1{}TmpJnt'.format(scissorArmList[i+1] , self.side)
			else:
				jntEnd = '{}End{}TmpJnt'.format(obj_name , self.side)

			self.scrNonRollRoot = cn.joint('{}{}NonRollRoot{}Jnt'.format(obj_name , elem , self.side),'{}3{}TmpJnt'.format(obj_name,self.side))
			self.scrNonRollEnd = cn.joint('{}{}NonRollEnd{}Jnt'.format(obj_name , elem , self.side),jntEnd)
			self.scrNonRollEnd.parent(self.scrNonRollRoot)
			self.scrNonRollRoot.parent(self.scrEndJnt)

			self.ikh = rt.addIkh('{}{}'.format(obj_name , elem) , 'ikRPsolver' , self.scrRootJnt , self.scrEndJnt)
			self.ikhNonRoll = rt.addIkh('{}{}IkNonRoll'.format(obj_name , elem) , 'ikRPsolver' , self.scrNonRollRoot , self.scrNonRollEnd)
			self.ikh['ikhZro'].parent(self.ikhGrp)
			self.ikhNonRoll['ikhZro'].parent(self.ikhGrp)

			if i == 0:
				self.ctrl_obj = ctrlRig.Run(name 			= obj_name,
											tmpJnt 			= '',
											side 			= side,
											shape 			= 'cylinder',
											jnt 			= 0 ,
											gimbal 			= 0 ,
											color 			= 'red',
											constraint 		= 0)
				self.ctrl_obj.zro.snap(self.scrEndJnt)
				self.ctrl_obj.zro.parent(self.ctrlGrp)

				mc.pointConstraint(self.ctrl_obj.ctrl,self.ikh['ikh'])
				mc.parentConstraint(self.scrEndJnt,self.ikhNonRoll['ikhZro'],mo=True)
				mc.parentConstraint(self.root.jnt,self.ctrl_obj.zro,mo=True)
				mc.scaleConstraint(self.root.jnt,self.ctrl_obj.zro)
				mc.parentConstraint(self.root.jnt,self.ikhGrp,mo=True)

				self.scrRootJnt.parent(self.root.jnt)
				obj_lockIkh = self.ctrl_obj.ctrl

			else:
				rootHoldJnt = '{}1{}Jnt'.format(scissorArmList[i-1],self.side)
				EndHoldJnt = '{}3{}Jnt'.format(scissorArmList[i],self.side)
				obj_lockIkh = rootHoldJnt

				self.scrNonRollHoldRoot = cn.joint('{}{}NonRollHoldRoot{}Jnt'.format(obj_name , elem , self.side),rootHoldJnt)
				self.scrNonRollHoldEnd = cn.joint('{}{}NonRollHoldEnd{}Jnt'.format(obj_name , elem , self.side),EndHoldJnt)
				self.scrNonRollHoldEnd.parent(self.scrNonRollHoldRoot)

				self.ikhNonRollHold = rt.addIkh('{}{}IkNonRollHold'.format(obj_name , elem) , 'ikRPsolver' , self.scrNonRollHoldRoot , self.scrNonRollHoldEnd)
				self.ikhNonRollHold['ikhZro'].parent(self.ikhGrp)
				mc.parentConstraint(rootHoldJnt,self.ikhNonRollHold['ikhZro'],mo=True)

				self.scrNonRollHoldRoot.parent(rootHoldJnt)
				self.scrRootJnt.parent('{}3{}Jnt'.format(scissorArmList[i-1],self.side))
				mc.parentConstraint(rootHoldJnt,self.ikhNonRollHold['ikhZro'],mo=True)


			mc.parentConstraint(obj_lockIkh,self.ikh['ikhZro'],mo=True)
			mc.parentConstraint(self.scrEndJnt,self.ikhNonRoll['ikhZro'],mo=True)


		self.ctrlEnd_obj = ctrlRig.Run( name 			= '{}{}End'.format(name , elem),
										tmpJnt 			= endScissor,
										side 			= side,
										shape 			= 'cube',
										jnt 			= 1 ,
										gimbal 			= 0 ,
										color 			= 'cyan',
										constraint 		= 0)
		self.ctrlEnd_shape = core.Dag(self.ctrlEnd_obj.ctrl.shape)
		self.ctrlEnd_obj.zro.snap(endScissor)
		self.ctrlEnd_obj.zro.parent(self.root.gmbl)
		self.ctrlEnd_obj.jnt.parent(self.root.jnt)
		mc.pointConstraint(self.scrNonRollEnd,self.ctrlEnd_obj.jnt,mo=True)
		mc.parentConstraint(self.ctrlEnd_obj.jnt,self.ctrlEnd_obj.zro,mo=True)
		mc.scaleConstraint(self.ctrlEnd_obj.jnt,self.ctrlEnd_obj.zro)

		self.ctrlEnd_obj.ctrl.addAttr('translatePlate',minVal=-5,maxVal=10)
		self.ctrlEnd_shape.addAttr('translatePlateAmp')
		self.plateMdv = cn.createNode('multiplyDivide','{}{}Plate{}Mdv'.format(name,elem,self.side))
		self.ctrlEnd_shape.attr('translatePlateAmp').v = -0.183305
		self.ctrlEnd_obj.ctrl.attr('translatePlate') >> self.plateMdv.attr('i1x')
		self.ctrlEnd_shape.attr('translatePlateAmp') >> self.plateMdv.attr('i2x')
		self.plateMdv.attr('ox') >> self.ctrl_obj.ofst.attr('tx')

		self.ctrlEnd_obj.ctrl.lockHideAttrs('tx','ty','tz','rx','ry','rz','sx','sy','sz','v')

		for each in scissorPipeList:
			self.rootPipeJnt = cn.joint('{}{}1{}Jnt'.format(each,elem,self.side),'{}1{}TmpJnt'.format(each,self.side))
			self.midPipeJnt = cn.joint('{}{}2{}Jnt'.format(each,elem,self.side),'{}2{}TmpJnt'.format(each,self.side))
			self.endPipeJnt = cn.joint('{}{}End{}Jnt'.format(each,elem,self.side),'{}End{}TmpJnt'.format(each,self.side))
			self.midPipeJnt.parent(self.rootPipeJnt)

			prRootJnt = mc.listRelatives('{}1{}TmpJnt'.format(each,self.side),p=True)[0]
			prEndJnt = mc.listRelatives('{}End{}TmpJnt'.format(each,self.side),p=True)[0]
			prRootJntName = (core.Dag(prRootJnt)).getName()
			prEndJntName = (core.Dag(prEndJnt)).getName()
			self.rootPipeJnt.parent('{}{}Jnt'.format(prRootJntName,self.side))
			self.endPipeJnt.parent('{}{}Jnt'.format(prEndJntName,self.side))

			self.ikhNonRollPipe = rt.addIkh('{}{}IkNonRoll'.format(each , elem) , 'ikRPsolver' , self.rootPipeJnt , self.midPipeJnt)
			mc.pointConstraint(self.endPipeJnt,self.ikhNonRollPipe['ikhZro'])
			self.ikhNonRollPipe['ikhZro'].parent(self.ikhGrp)
			mc.aimConstraint( self.rootPipeJnt , self.endPipeJnt , 
                                     aim = (0,-1,0) , 
                                     u = (1,0,0) , 
                                     wut='objectrotation' , 
                                     wuo = prEndJnt , 
                                     wu= (0,1,0) )
              
		jnts = mc.ls(type='joint')
		for jnt in jnts:
			mc.setAttr(jnt+'.ssc',0)

		self.ctrlGrp.parent(ctrlGrp)
		self.ikhGrp.parent(ikhGrp)

		mc.select( cl = True)