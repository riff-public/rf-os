from rf_maya.rftool.rig.rigScript import caFacialModule as cfm     
reload(cfm) 
from utaTools.utapy import utaCore
reload(utaCore)
import maya.cmds as mc
from rftool.rig.rigScript import core 
reload(core)
cn = core.Node()

'''
##Process by hong

################################# fclRig Process / Module ###################################
## select facial ui Ctrl and run
from rf_maya.rftool.rig.rigScript import caRigModule
reload(caRigModule)
from rf_maya.rftool.rig.rigScript import caFacialModule
reload(caFacialModule)
from rf_maya.rftool.rig.rigScript import utaAttrWriteRead
reload(utaAttrWriteRead)

## 01. Connect ShrinkWrap Control
caRigModule.connectWrap_ctrl(ctrl= ['uiCtrl_md:CtrlUiRigPos_Grp'],search='',replace='')

## 02. Add BlendShape SwRigGeo_Grp to FclRigGeo_Grp
caFacialModule.addBshToFclRig(source = 'swRig_md:SwRigGeo_Grp', target = 'FclRigGeo_Grp',sourceProcess = 'SwRig', targetProcess = 'FclRig', frontOfChain = True)

## 03. Add Attribute detailVis at uiCtrl
caFacialModule.addAttrDtlVisToUiCtrl(uiCtrlGrp = ['uiCtrl_md:CtrlUiRigPos_Grp'], attrVis = 'detailVis', nameSp = 'dtlRig_md',)

## 04. Add stick detail control (main process)
caFacialModule.stikDetailControl(dtlCtrlGrp = ['dtlRig_md:DtlRigCtrl_Grp'], dtlRigGeoGrp = 'dtlRig_md:FacePlaneDtlRigGeo_Grp',swRigGeoGrp = 'swRig_md:FacePlaneSwRigGeo_Grp')

## 05. Add Squash to geoGrp in fclGrp
caRigModule.headSqsh(geo = 'FclRigGeo_Grp',name= 'HdUpr',deformHandleGrp= 'HdRigStill_Grp',ctrlGrp= 'HdRigCtrl_Grp')


## 06. Copy texture to fclRigGeo
caFacialModule.assignMaterial(geoGrp = 'FclRigGeo_Grp')

## 07. Read attribute from process name
utaAttrWriteRead.attrReadProcess(process = True)

## 08. Wrap Group
caRigModule.wrapGroup()




##Process by kenr

################################### Generate Facial Ui Control Template Process / Module ###################################
from rf_maya.rftool.rig.rigScript import caFacialModule
reload(caFacialModule)
## generate main Ui
caFacialModule.generateMainUiCtrl()
## generate sub textureUiCtrl "Eyebrow"
locatorObj = caFacialModule.generateViewUiCtrl(name = 'Eye', elem = 'UiCtrl',  menuName = ['Eye'], side = ['R', 'L'])
mc.setAttr('{}.tx'.format(locatorObj), 7)
mc.setAttr('{}.ty'.format(locatorObj), 1.5)
## generate sub textureUiCtrl "Mouth"
locatorObj = caFacialModule.generateViewUiCtrl(name = 'Mouth', elem = 'UiCtrl',  menuName = ['Mouth'], side = ['M'])
mc.setAttr('{}.tx'.format(locatorObj), 12.5)
mc.setAttr('{}.ty'.format(locatorObj), 1.5)
## generate sub textureUiCtrl "Eyebrow"
locatorObj = caFacialModule.generateViewUiCtrl(name = 'EyebrowWrinkle', elem = 'UiCtrl',  menuName = ['Eyebrow', 'Wrinkle'], side = ['R', 'L'])
mc.setAttr('{}.tx'.format(locatorObj), 18)
mc.setAttr('{}.ty'.format(locatorObj), 1.5)


## generate sub textureUiCtrl "Eyebrow"
locatorObj = caFacialModule.generateViewUiCtrl(name = 'Cheek', elem = 'UiCtrl',  menuName = ['Cheek'], side = ['R', 'L'])
mc.setAttr('{}.tx'.format(locatorObj), 7)
mc.setAttr('{}.ty'.format(locatorObj), -4)
## generate sub textureUiCtrl "Eyebrow"
locatorObj = caFacialModule.generateViewUiCtrl(name = 'Jaw', elem = 'UiCtrl',  menuName = ['Jaw'], side = ['J'])
mc.setAttr('{}.tx'.format(locatorObj), 12.5)
mc.setAttr('{}.ty'.format(locatorObj), -4)
## generate sub textureUiCtrl "Mouth"
locatorObj = caFacialModule.generateViewUiCtrl(name = 'MouthEffUpDn', elem = 'UiCtrl',  menuName = ['MouthEffUp', 'MouthEffDn'], side = ['R', 'L'])
mc.setAttr('{}.tx'.format(locatorObj), 18)
mc.setAttr('{}.ty'.format(locatorObj), -4)

## Tools Replace PlaneGeo
#caFacialModule.parentPlaneUiRigGeoToUiCtrl()



################################### Run Facial Ui Control Rig Process / Module ###################################
# Run facialUiRig
from rf_maya.rftool.rig.rigScript import caFacialRig
reload(caFacialRig)
caFacialRig.facialUiRig(    eyeSeq = True, 
                            eyeShaDowCtrl = True,
                            specular = False, 
                            eyeSide = ['L','R'],
                            eyeUiRigCtrl = 'EyeUiRig_L_Ctrl',
                            eyeTextureUiRigCtrl = 'EyeTextureUiRig_L_Ctrl',
                            eyeElem = 'EyeSeq',
                            eyePlaneGeo = 'EyePlaneUiRig_L_Geo', 
                            eyeTextureUiGeo = 'EyesUiRig_Geo',
                            eyeMaskCtrl = ['EyeMaskUpUiRig_L_Ctrl', 'EyeMaskDnUiRig_L_Ctrl'],
                            eyeMaskElem = 'Mask',
                            whiteLine = True, 

                            eyeBall = True, 
                            eyeBallSide = ['L' , 'R'],
                            eyeBallEyeUiRigCtrl = 'EyeUiRig_L_Ctrl',
                            eyeBallElem = 'EyeBall',
                            eyeBallIrisCtrl = 'IrisUiRig_L_Ctrl',
                            eyeBallBlinkCtrl = 'BlinkUiRig_L_Ctrl',
                            eyeBallSpecularCtrl = 'SpecularUiRig_L_Ctrl',

                            mouth = True, 
                            mouthShadowCtrl = True,
                            mouthUiRigCtrl = 'MouthUiRig_Ctrl',
                            mouthTextureUiRigCtrl = 'MouthTextureUiRig_Ctrl',
                            mouthElem = 'MouthSeq',
                            mouthPlaneGeo = 'MouthPlaneUiRig_Geo',
                            mouthTextureUiGeo = 'MouthUiRig_Geo', 

                            eyebrow = True, 
                            eyebrwoShadowCtrl = True,
                            eyebrowSide = ['L' , 'R'],
                            eyebrowUiRigCtrl = 'EyebrowUiRig_L_Ctrl',
                            eyebrowTextureUiRigCtrl = 'EyebrowTextureUiRig_L_Ctrl',
                            eyebrowElem = 'EyebrowSeq',
                            eyebrowPlaneGeo = 'EyebrowPlaneUiRig_L_Geo',
                            eyebrowTextureUiGeo = 'EyebrowUiRig_Geo',

                            cheek = True, 
                            cheekShadowCtrl = True,
                            checkSide = ['L','R'],
                            cheekUiRigCtrl = 'CheekUiRig_L_Ctrl',
                            cheekTextureUiRigCtrl = 'CheekTextureUiRig_L_Ctrl',
                            cheekElem = 'CheekSeq',
                            cheekPlaneGeo = 'CheekPlaneUiRig_L_Geo',
                            cheekTextureUiGeo = 'CheekUiRig_Geo',

                            wrinkle = True, 
                            wrinkleShadowCtrl = True,
                            wrinkleSide = ['L','R'],
                            wrinkleUiRigCtrl = 'WrinkleUiRig_L_Ctrl',
                            wrinkleTextureUiRigCtrl = 'WrinkleTextureUiRig_L_Ctrl',
                            wrinkleElem = 'WrinkleSeq',
                            wrinklePlaneGeo = 'WrinklePlaneUiRig_L_Geo',
                            wrinkleTextureUiGeo = '',

                            jaw = True, 
                            jawShadowCtrl = True,
                            jawUiRigCtrl = 'JawUiRig_Ctrl',
                            jawTextureUiRigCtrl = 'JawTextureUiRig_Ctrl',
                            jawElem = 'JawSeq',
                            jawPlaneGeo = 'JawPlaneUiRig_Geo', 
                            jawTextureUiGeo = '',
        
                            mouthEffUp = True,
                            mouthEffUpShadowCtrl = True,
                            mouthEffUpSide = ['L', 'R'],
                            mouthEffUpElem = 'MouthEffUpSeq',
                            mouthEffUpUiRigCtrl = 'MouthEffUpUiRig_L_Ctrl',
                            mouthEffUpTextureUiRigCtrl = 'MouthEffUpTextureUiRig_L_Ctrl',
                            mouthEffUpPlaneGeo = 'MouthEffUpPlaneUiRig_L_Geo',
                            mouthEffUpTextureUiGeo = 'MouthEffUiRig_Geo',
        
                            mouthEffDn = True,
                            mouthEffDnShadowCtrl = True,
                            mouthEffDnSide = ['L', 'R'],
                            mouthEffDnElem = 'MouthEffDnSeq',
                            mouthEffDnUiRigCtrl = 'MouthEffDnUiRig_L_Ctrl',
                            mouthEffDnTextureUiRigCtrl = 'MouthEffDnTextureUiRig_L_Ctrl',
                            mouthEffDnPlaneGeo = 'MouthEffDnPlaneUiRig_L_Geo',
                            mouthEffDnTextureUiGeo = '',

                            head = True,
                            headRamp = True,
                            headRampCtrl = 'headRamp_Ctrl',
                            headRampFolderName = 'txr',
                            headRampElem = 'head',
                            headRampGeo = 'HeadUiRig_Geo',


                            texturePath = 'P:/CA/asset/work/char/GB/texture/main'
                            )


## generateAttribute UiCtrlVis Menu
caFacialModule.addAttrMenuUiCtrl(ctrlMain = 'VisUiRig_Ctrl',ctrlGrpVis = 'CtrlUiRigPos_Grp')

## parent UiRigGeo to ui control
caFacialModule.parentPlaneUiRigGeoToUiCtrl()

#create plane Ui Rig 


################################### main  Process / Module ###################################
from rf_maya.rftool.rig.rigScript import caFacialModule
reload(caFacialModule)
from rf_maya.rftool.rig.rigScript import utaAttrWriteRead
reload(utaAttrWriteRead)

## create scrip connect geoVis from uiCtrl to skinGeo
caFacialModule.connectGeoVisMain(geoGrp = 'bodyRig_md:FacePlaneGeo_Grp')

## oreint scale constraint to dtlRig control (HeadGmbl_ctrl)
caFacialModule.orientScaleConstraintDtlRigControl(dtlCtrlGrp = ['fclRig_md:DtlRigCtrl_Grp'], headGmblCtrl = 'bodyRig_md:HeadGmbl_Ctrl')

## Add Blend Shape fcl to faceGeo
caFacialModule.addBshFclToFaceGeo(fclGeoGrp = 'fclRig_md:FclRigGeo_Grp')

## 06. Copy texture to fclRigGeo
caFacialModule.assignMaterial(geoGrp = 'bodyRig_md:AllGeo_Grp')

## 07. Read attribute from process name
utaAttrWriteRead.attrReadProcess(process = True)

## set attribute for publish main
caFacialModule.setDefaultPublishMain()

## wrap  group process  main
caFacialModule.wrapGroupProcessMain()
'''

## generate Geo_Grp connection (generate conection for matterial) 
# from rf_maya.rftool.rig.rigScript import caRigModule
# reload(caRigModule)
# x = caRigModule.store_uiCtrl_data()   
# caRigModule.create_uiCtrl_connect(x,asset = '',fcl_ns = 'fclRig_md:',mtr_ns = 'mdlMainMtr') 


def facialUiRig(        eyeSeq = True, 
                        eyeShaDowCtrl = True,
                        eyeSide = ['R', 'L'],
                        eyeUiRigCtrl = 'EyeUiRig_L_Ctrl',
                        eyeTextureUiRigCtrl = 'EyeTextureUiRig_L_Ctrl',
                        eyeElem = 'EyeSeq',
                        eyePlaneGeo = 'EyePlaneUiRig_L_Geo', 
                        eyeTextureUiGeo = 'EyeUiRig_Geo',
                        eyeMaskCtrl = ['EyeMaskUpUiRig_L_Ctrl', 'EyeMaskDnUiRig_L_Ctrl'],
                        eyeMaskElem = 'Mask',
                        eyeMaskSeq = True, 
                        eyeWhiteLineSeq = True,
                        eyeMaskShadow = True, 
                        eyeWhiteLineShadow = False,

                        eyeBall = True, 
                        eyeBallSide = ['R', 'L'],
                        eyeBallEyeUiRigCtrl = 'EyeUiRig_L_Ctrl',
                        eyeBallElem = 'EyeBall',
                        eyeBallIrisCtrl = 'IrisUiRig_L_Ctrl',
                        eyeBallBlinkCtrl = 'BlinkUiRig_L_Ctrl',
                        eyeBallSpecularCtrl = 'SpecularUiRig_L_Ctrl',
                        eyeBallSpecularSpACtrl = 'SpecularSpAUiRig_L_Ctrl',
                        eyeBallSpecularSpBCtrl = 'SpecularSpBUiRig_L_Ctrl',
                        eyeBallSpecularPosition = [[94, 0, 0],[104, 0, 0]], 

                        mouth = True, 
                        mouthShadowCtrl = True,
                        mouthSide = [],
                        mouthUiRigCtrl = 'MouthUiRig_Ctrl',
                        mouthTextureUiRigCtrl = 'MouthTextureUiRig_Ctrl',
                        mouthElem = 'MouthSeq',
                        mouthPlaneGeo = 'MouthPlaneUiRig_Geo',
                        mouthShadowPlaneGeo = '',
                        mouthTextureUiGeo = 'MouthUiRig_Geo', 

                        mouthBall = True, 
                        mouthBallSide = [],
                        mouthBallMouthUiRigCtrl = 'MouthUiRig_Ctrl',
                        mouthBallElem = 'MouthBall',
                        mouthBallTeethUpCtrl = 'TeethUpUiRig_Ctrl',
                        mouthBallTongueCtrl = 'TongueUiRig_Ctrl',
                        mouthBallTeethDnCtrl = 'TeethDnUiRig_Ctrl',

                        # mouthBall = True, 
                        # mouthBallShadowCtrl = True,
                        # mouthBallCtrl='TeethUpUiRig_Ctrl',
                        # mouthBallElem = 'MouthBall',
                        # mouthBallAnswerLyt= 'MouthSeqAnswer_Lyt',

                        eyebrow = True, 
                        eyebrwoShadowCtrl = True,
                        eyebrowSide = ['R', 'L'],
                        eyebrowUiRigCtrl = 'EyebrowUiRig_L_Ctrl',
                        eyebrowTextureUiRigCtrl = 'EyebrowTextureUiRig_L_Ctrl',
                        eyebrowElem = 'EyebrowSeq',
                        eyebrowPlaneGeo = 'EyebrowPlaneUiRig_L_Geo',
                        eyebrowShadowPlaneGeo = 'EyebrowShadowPlaneUiRig_L_Geo',
                        eyebrowTextureUiGeo = 'EyebrowUiRig_Geo',

                        cheek = True, 
                        cheekShadowCtrl = True,
                        checkSide = ['R', 'L'],
                        cheekUiRigCtrl = 'CheekUiRig_L_Ctrl',
                        cheekTextureUiRigCtrl = 'CheekTextureUiRig_L_Ctrl',
                        cheekElem = 'CheekSeq',
                        cheekPlaneGeo = 'CheekPlaneUiRig_L_Geo',
                        cheekShadowPlaneGeo = 'CheekShadowPlaneUiRig_L_Geo',
                        cheekTextureUiGeo = 'CheekUiRig_Geo',

                        wrinkle = True, 
                        wrinkleShadowCtrl = True,
                        wrinkleSide = ['R', 'L'],
                        wrinkleUiRigCtrl = 'WrinkleUiRig_Ctrl',
                        wrinkleTextureUiRigCtrl = 'WrinkleTextureUiRig_Ctrl',
                        wrinkleElem = 'WrinkleSeq',
                        wrinklePlaneGeo = 'WrinklePlaneUiRig_Geo',
                        wrinkleShadowPlaneGeo = '',
                        wrinkleTextureUiGeo = 'WrinkleUiRig_Geo',
                        wrinkleFollow = True,
                        wrinkleFollowCtrl = 'EyebrowUiRig_L_Ctrl',

                        jaw = True, 
                        jawShadowCtrl = True,
                        jawSide = [],
                        jawUiRigCtrl = 'JawUiRig_Ctrl',
                        jawTextureUiRigCtrl = 'JawTextureUiRig_Ctrl',
                        jawElem = 'JawSeq',
                        jawPlaneGeo = 'JawPlaneUiRig_Geo', 
                        jawShadowPlaneGeo = '', 
                        jawTextureUiGeo = '',
    
                        mouthEffUp = True,
                        mouthEffUpShadowCtrl = True,
                        mouthEffUpSide = ['L', 'R'],
                        mouthEffUpElem = 'MouthEffUpSeq',
                        mouthEffUpUiRigCtrl = 'MouthEffUpUiRig_L_Ctrl',
                        mouthEffUpTextureUiRigCtrl = 'MouthEffUpTextureUiRig_L_Ctrl',
                        mouthEffUpPlaneGeo = 'MouthEffUpPlaneUiRig_L_Geo',
                        mouthEffUpShadowPlaneGeo = '',
                        mouthEffUpTextureUiGeo = 'MouthEffUpUiRig_Geo',
                        mouthEffUpFollow = True,
                        mouthEffUpFollowCtrl = 'MouthUiRig_Ctrl', 
    
                        mouthEffDn = True,
                        mouthEffDnShadowCtrl = True,
                        mouthEffDnSide = ['R', 'L'],
                        mouthEffDnElem = 'MouthEffDnSeq',
                        mouthEffDnUiRigCtrl = 'MouthEffDnUiRig_L_Ctrl',
                        mouthEffDnTextureUiRigCtrl = 'MouthEffDnTextureUiRig_L_Ctrl',
                        mouthEffDnPlaneGeo = 'MouthEffDnPlaneUiRig_L_Geo',
                        mouthEffDnShadowPlaneGeo = '',
                        mouthEffDnTextureUiGeo = '',
                        mouthEffDnFollow = True,
                        mouthEffDnFollowCtrl = 'MouthUiRig_Ctrl', 

                        head = True,
                        headRamp = True,
                        headRampCtrl = 'HeadRamp_Ctrl',
                        headRampFolderName = 'txr',
                        headRampElem = 'Head',
                        headRampGeo = 'HeadUiRig_Geo',


                        texturePath = 'P:/CA/asset/work/char/{}/texture/main/images/preview'.format(''),         
                        *args):

     

##--------------------------------------------Eye Seq -------------------------------------------
##--------------------------------------------Eye Seq -------------------------------------------
    if eyeSeq:
        eyelytAllNodeLists = []
        if not eyeSide:
            eyeSide = ['_']
        for eachSide in eyeSide:
            side = utaCore.findSide(side = eachSide)

            nameEyeUiCtrl, sideEyeUiCtrl, lastNameEyeUiCtrl = cn.splitName(sels = [eyeUiRigCtrl] )
            newEyeUiCtrl = '{}{}{}'.format(nameEyeUiCtrl, side, lastNameEyeUiCtrl)

            nameEyeTxCtrl, sideEyeTxCtrl, lastNameEyeTxCtrl = cn.splitName(sels = [eyeTextureUiRigCtrl] )
            newEyeTxCtrl = '{}{}{}'.format(nameEyeTxCtrl, side, lastNameEyeTxCtrl)

            nameSp, sideSp, lastNameSp = cn.splitName(sels = [eyePlaneGeo])
            newEyePlaneGeo = '{}{}{}'.format(nameSp, side, lastNameSp)
            ## eye Seq Control        
            lytAllNode, lytOutNode, lytShadowNode, lytAnswerNode, textureSeqFile , place2DTextureTextureSeq, myShaderBlinn, shadowTranslateNode = cfm.imageSeqFacial(
                                                    uiRigCtrl = newEyeUiCtrl,
                                                    textureUiRigCtrl = newEyeTxCtrl, 
                                                    shaDowCtrl = eyeShaDowCtrl, 
                                                    elem = eyeElem,
                                                    planeGeo = newEyePlaneGeo,
                                                    shadowPlaneGeo = '',
                                                    textureUiGeo = eyeTextureUiGeo,
                                                    texturePath = texturePath
                                                    )
            eyelytAllNodeLists.append(lytAllNode)

            ## eye mask Up-Dn control 
            newEyeMaskCtrl = []
            for eachEyeMask in eyeMaskCtrl:

                nameEyeMaskCtrl, sideEyeMaskCtrl, lastNameEyeMaskCtrl = cn.splitName(sels= [eachEyeMask])
                if not side == '_':
                    newEyeMaskCtrl.append('{}{}{}'.format(nameEyeMaskCtrl, side, lastNameEyeMaskCtrl))
                else:
                    newEyeMaskCtrl.append(eachEyeMask)
            cfm.eyeMaskFacial(  
                                                    ctrl = newEyeMaskCtrl, 
                                                    shadowTranslateMdvNode = shadowTranslateNode,
                                                    elem = eyeMaskElem, 
                                                    maskSeq = eyeMaskSeq, 
                                                    whiteLineSeq = eyeWhiteLineSeq,
                                                    outLyt = lytOutNode, 
                                                    maskShadow = eyeMaskShadow, 
                                                    whiteLineShadow = eyeWhiteLineShadow,
                                                    shadowLyt = lytShadowNode, 
                                                    material = myShaderBlinn, 
                                                    seqFile = textureSeqFile, 
                                                    texturePath = texturePath,
                                                    )
          
        print 'Process :: {} >> "DONE"'.format(eyeElem)


##--------------------------------------------Eye Ball -------------------------------------------
##--------------------------------------------Eye Ball -------------------------------------------
    if eyeBall:
        if not eyeBallSide:
            eyeBallSide = ['_']
        for i in range(len(eyeBallSide)):
            side = utaCore.findSide(side = eyeBallSide[i])
            nameEyeBallIrisCtrl, sideEyeBallIrisCtrl, lastNameEyeBallIrisCtrl = cn.splitName(sels = [eyeBallIrisCtrl] )
            newEyeBallCtrl = '{}{}{}'.format(nameEyeBallIrisCtrl, side, lastNameEyeBallIrisCtrl)

            ## split eye Ctrl
            nameEyeCtrl, sideEyeCtrl, lastNameEyeCtrl = cn.splitName(sels = [eyeTextureUiRigCtrl] )
            newEyeCtrl = '{}{}{}'.format(nameEyeCtrl, side, lastNameEyeCtrl)

            ## split side of textureSeqFile
            nameTextureSeqFile, sideTextureSeqFile, lastNameTextureSeqFile = cn.splitName(sels = [textureSeqFile] )
            newTextureSeqFile = '{}{}{}'.format(nameTextureSeqFile, side, lastNameTextureSeqFile)
            ## iris Eyeball Control  
            EyeBallLyt, IrisCorFile, IrisCorPlace2DTexture, IrisAlphaFile, IrisAlphalace2DTexture = cfm.generateIris(    
                                                    uiRigctrl = newEyeBallCtrl, 
                                                    rextureUiRigCtrl = newEyeCtrl, 
                                                    seqNode = newTextureSeqFile, 
                                                    elemSeq = eyeElem,
                                                    elem = eyeBallElem, 
                                                    texturePath = texturePath
                                                    )

            ## Blink EyeBall Control 
            nameEyeBallBlinkCtrl, sideEyeBallBlinkCtrl, lastNameEyeBallBlinkCtrl = cn.splitName(sels = [eyeBallBlinkCtrl] )
            newEyeBallBlinkCtrl = '{}{}{}'.format(nameEyeBallBlinkCtrl, side, lastNameEyeBallBlinkCtrl)
            BlinkCorFile, BlinkCorPlace2DTexture = cfm.generateBlink(   
                                                    ctrl = newEyeBallBlinkCtrl, 
                                                    elem = eyeBallElem, 
                                                    EyeBallLyt = EyeBallLyt,
                                                    texturePath = texturePath
                                                    )


            ## generate blink follow iris switch 
            cfm.generateBlinkFollowIris(            ctrl = newEyeBallBlinkCtrl, 
                                                    BlinkCorPlace2DTexture = BlinkCorPlace2DTexture, 
                                                    IrisCorePlace2DTexture = IrisCorPlace2DTexture,
                                                    )


            ## 01 specular Eyeball Control 
            nameEyeBallSpecularCtrl, sideEyeBallSpecularCtrl, lastNameEyeBallSpecularCtrl = cn.splitName(sels = [eyeBallSpecularCtrl] )
            newEyeBallSpecularCtrl = '{}{}{}'.format(nameEyeBallSpecularCtrl, side, lastNameEyeBallSpecularCtrl)
            SpecularCorFile, SpecularCorPlace2DTexture, specularLyt = cfm.generateSpecularIrisAlpha(
                                                    ctrl = newEyeBallSpecularCtrl, 
                                                    elem = eyeBallElem, 
                                                    irisAlphaSeq = IrisAlphaFile,
                                                    EyeBallAllLyt = eyelytAllNodeLists[i],
                                                    texturePath = texturePath
                                                    )
         
            ## 01 generate Specular follow iris switch 
            cfm.generateBlinkFollowIris(            ctrl = newEyeBallSpecularCtrl, 
                                                    BlinkCorPlace2DTexture = SpecularCorPlace2DTexture, 
                                                    IrisCorePlace2DTexture = IrisCorPlace2DTexture,
                                                    )
            
            ## 02 specular SpA Eyeball Control 
            nameEyeBallSpecularSpACtrl, sideEyeBallSpecularSpACtrl, lastNameEyeBallSpecularSpACtrl = cn.splitName(sels = [eyeBallSpecularSpACtrl] )
            newEyeBallSpecularSpACtrl = '{}{}{}'.format(nameEyeBallSpecularSpACtrl, side, lastNameEyeBallSpecularSpACtrl)
            SpecularSpACorFile, SpecularSpACorPlace2DTexture, specularSpALyt = cfm.generateSpecularIrisAlpha(
                                                    ctrl = newEyeBallSpecularSpACtrl, 
                                                    elem = eyeBallElem, 
                                                    irisAlphaSeq = IrisAlphaFile,
                                                    EyeBallAllLyt = eyelytAllNodeLists[i],
                                                    texturePath = texturePath
                                                    )


            ## 02 generate Specular SpA follow iris switch 
            cfm.generateBlinkFollowIris(            ctrl = newEyeBallSpecularSpACtrl, 
                                                    BlinkCorPlace2DTexture = SpecularSpACorPlace2DTexture, 
                                                    IrisCorePlace2DTexture = IrisCorPlace2DTexture,
                                                    )

            ## 02 generate specular auto vis 
            cfm.speculatAutoVis(                    specularCtrl = newEyeBallSpecularCtrl, 
                                                    specularSpACtrl = newEyeBallSpecularSpACtrl, 
                                                    positionValue = eyeBallSpecularPosition,
                                                    specularLyt = specularSpALyt,
                                                    EyeUiRigTyRevMdv = 'EyeUiRigTyRev', 
                                                    EyeUiRigPma = 'EyeUiRig')


            ## 03 specular SpB Eyeball Control 
            nameEyeBallSpecularSpBCtrl, sideEyeBallSpecularSpBCtrl, lastNameEyeBallSpecularSpBCtrl = cn.splitName(sels = [eyeBallSpecularSpBCtrl] )
            newEyeBallSpecularSpBCtrl = '{}{}{}'.format(nameEyeBallSpecularSpBCtrl, side, lastNameEyeBallSpecularSpBCtrl)
            SpecularSpBCorFile, SpecularSpBCorPlace2DTexture, specularSpBLyt = cfm.generateSpecularIrisAlpha(
                                                    ctrl = newEyeBallSpecularSpBCtrl, 
                                                    elem = eyeBallElem, 
                                                    irisAlphaSeq = IrisAlphaFile,
                                                    EyeBallAllLyt = eyelytAllNodeLists[i],
                                                    texturePath = texturePath
                                                    )


            ## 03 generate Specular SpB follow iris switch 
            cfm.generateBlinkFollowIris(            ctrl = newEyeBallSpecularSpBCtrl, 
                                                    BlinkCorPlace2DTexture = SpecularSpBCorPlace2DTexture, 
                                                    IrisCorePlace2DTexture = IrisCorPlace2DTexture,
                                                    )

            ## 03 generate specular SpA, SpB auto vis 
            cfm.speculatAutoVis(                    specularCtrl = newEyeBallSpecularCtrl, 
                                                    specularSpACtrl = newEyeBallSpecularSpBCtrl, 
                                                    positionValue = eyeBallSpecularPosition,
                                                    specularLyt = specularSpBLyt,
                                                    EyeUiRigTyRevMdv = 'EyeUiRigTyRev', 
                                                    EyeUiRigPma = 'EyeUiRig')


            ## switch seq or eyeBall 
            nameEyeBallUiRigCtrl, sideEyeBallUiRigCtrl, lastNameEyeBallUiRigCtrl = cn.splitName(sels = [eyeBallEyeUiRigCtrl] )
            newEyeBallUiRigCtrl = '{}{}{}'.format(nameEyeBallUiRigCtrl, side, lastNameEyeBallUiRigCtrl)
            cfm.generateSwitchSeqEyeBall(
                                                    ctrl = newEyeBallUiRigCtrl, 
                                                    elem = 'Eye',
                                                    eyeBallAllLyt = eyelytAllNodeLists[i],
                                                    )



    # ## Generate shadow gray on eye of eyebrow
    # ## Side 'L'
    # cfm.addSeqToLayer(  name = 'EyebrowShadowEye',
    #                     side = 'L',
    #                     seqName = 'Eyebrow',
    #                     toLayer = 'Eye',
    #                     ctrls = ['EyebrowUiRig_L_Ctrl','EyebrowShadowUiRig_L_Ctrl'],
    #                     counterCtrls = ['EyeUiRig_L_Ctrl'],
    #                     seqCtrl = 'EyeUiRig_L_Ctrl',
    #                     texturePath = r'P:/CA/asset/work/char/GB/texture/main/preview',
    #                     mirrorUCtrl = 'EyebrowUiRig_L_Ctrl',
    #                     shadowVisCtrl = 'EyebrowUiRig_L_Ctrl'
    #                     )
    # ## Side 'R'
    # cfm.addSeqToLayer(  name='EyebrowShadowEye',
    #                     side='R',
    #                     seqName = 'Eyebrow',
    #                     toLayer= 'Eye',
    #                     ctrls = ['EyebrowUiRig_R_Ctrl','EyebrowShadowUiRig_R_Ctrl'],
    #                     counterCtrls = ['EyeUiRig_R_Ctrl'],
    #                     seqCtrl = 'EyeUiRig_R_Ctrl',
    #                     texturePath = r'P:/CA/asset/work/char/GB/texture/main/preview',
    #                     mirrorUCtrl = 'EyebrowUiRig_R_Ctrl',
    #                     shadowVisCtrl = 'EyebrowUiRig_R_Ctrl'
    #                     )


    print 'Process :: {} >> "DONE"'.format(eyeBallElem)

##----------------------------------------------------------------------------------------------------
##----------------------------------------------------------------------------------------------------

    if mouth:
        mouthlytAllNodeLists = []
        ## mouth Control          
        lytAllNode, lytOutNode, lytShadowNode, lytAnswerNode, textureSeqFile , place2DTextureTextureSeq, myShaderBlinn, shadowTranslateNode = cfm.imageSeqFacial(
                            uiRigCtrl = mouthUiRigCtrl, 
                            textureUiRigCtrl = mouthTextureUiRigCtrl, 
                            shaDowCtrl = mouthShadowCtrl, 
                            elem = mouthElem, 
                            planeGeo = mouthPlaneGeo,
                            shadowPlaneGeo = mouthShadowPlaneGeo,
                            textureUiGeo = mouthTextureUiGeo,
                            texturePath = texturePath
                            )

        mouthlytAllNodeLists.append(lytAllNode)

        print 'Process :: {} >> "DONE"'.format(mouthElem)



##--------------------------------------------Mouth Ball -------------------------------------------
##--------------------------------------------Mouth Ball -------------------------------------------
    # if mouthBall:
        
    #     ## TeethUp Mouthball Control  (Iris)
    #     cfm.addTextureToLayer(  ctrl= mouthBallCtrl,
    #                             shadowCtrl = True,
    #                             elem = mouthBallElem,
    #                             answerLyt= mouthBallAnswerLyt,
    #                             texturePath = texturePath
    #                             )

    #     if mouthBallShadowCtrl:
    #         print 'Shadow True....'
    #     print 'Process :: {} >> "DONE"'.format(mouthBallElem)


    if eyebrow:
        if not eyebrowSide:
            eyebrowSide = ['_']
        for eachEyebrowSide in eyebrowSide:
            side = utaCore.findSide(side = eachEyebrowSide)

            nameEyebrowUiRigCtrl, sideEyebrowUiRigCtrl, lastNameEyebrowUiRigCtrl = cn.splitName(sels = [eyebrowUiRigCtrl] )
            newEyebrowUiRigCtrl = '{}{}{}'.format(nameEyebrowUiRigCtrl, side, lastNameEyebrowUiRigCtrl)

            nameEyebrowTexUiRigCtrl, sideEyebrowTexUiRigCtrl, lastNameEyebrowTexUiRigCtrl = cn.splitName(sels = [eyebrowTextureUiRigCtrl] )
            newEyebrowTexUiRigCtrl = '{}{}{}'.format(nameEyebrowTexUiRigCtrl, side, lastNameEyebrowTexUiRigCtrl)

            nameSp, sideSp, lasNameSp = cn.splitName(sels = [eyebrowPlaneGeo])
            eyebrowPlaneGeo = '{}{}{}'.format(nameSp, side, lasNameSp)

            shadowNameSp, shadowSideSp, shadowLastNameSp = cn.splitName(sels = [eyebrowShadowPlaneGeo])
            shadowPlaneGeoName = '{}{}{}'.format(shadowNameSp, side, shadowLastNameSp)
            ## eyebrow Control         
            cfm.imageSeqFacial(
                            uiRigCtrl = newEyebrowUiRigCtrl, 
                            textureUiRigCtrl = newEyebrowTexUiRigCtrl,     
                            shaDowCtrl = eyebrwoShadowCtrl, 
                            elem = eyebrowElem, 
                            planeGeo = eyebrowPlaneGeo,
                            shadowPlaneGeo = shadowPlaneGeoName,
                            textureUiGeo = eyebrowTextureUiGeo,
                            texturePath = texturePath
                            )

        print 'Process :: {} >> "DONE"'.format(eyebrowElem)


    if cheek:
        if not checkSide:
            checkSide = ['_']
        for eachCheekSide in checkSide:
            side = utaCore.findSide(side = eachCheekSide)

            nameCheekUiRigCtrl, sideCheekUiRigCtrl, lastNameCheekUiRigCtrl = cn.splitName(sels = [cheekUiRigCtrl] )
            newCheekUiRigCtrl = '{}{}{}'.format(nameCheekUiRigCtrl, side, lastNameCheekUiRigCtrl)

            nameCheekTexUiRigCtrl, sideCheekTexUiRigCtrl, lastNameCheekTexUiRigCtrl = cn.splitName(sels = [cheekTextureUiRigCtrl] )
            newCheekTexUiRigCtrl = '{}{}{}'.format(nameCheekTexUiRigCtrl, side, lastNameCheekTexUiRigCtrl)

            nameSp, sideSp, lasNameSp = cn.splitName(sels = [cheekPlaneGeo])
            cheekPlaneGeo = '{}{}{}'.format(nameSp, side, lasNameSp)

            cheekNameSp, cheekSideSp, cheekLastNameSp = cn.splitName(sels = [cheekShadowPlaneGeo])
            cheekPlaneGeoName = '{}{}{}'.format(cheekNameSp, side, cheekLastNameSp)
            ## Cheek Control       
            cfm.imageSeqFacial(
                            uiRigCtrl = newCheekUiRigCtrl, 
                            textureUiRigCtrl = newCheekTexUiRigCtrl, 
                            shaDowCtrl = cheekShadowCtrl, 
                            elem = cheekElem, 
                            planeGeo = cheekPlaneGeo,
                            shadowPlaneGeo = cheekPlaneGeoName,
                            textureUiGeo = cheekTextureUiGeo,
                            texturePath = texturePath
                            )

        print 'Process :: {} >> "DONE"'.format(cheekElem)


    if wrinkle:
        if not wrinkleSide:
            wrinkleSide = ['_']
        for eachWrinkleSide in wrinkleSide:
            side = utaCore.findSide(side = eachWrinkleSide)

            nameWrinkleUiRigCtrl, sideWrinkleUiRigCtrl, lastNameWrinkleUiRigCtrl = cn.splitName(sels = [wrinkleUiRigCtrl] )
            newWrinkleUiRigCtrl = '{}{}{}'.format(nameWrinkleUiRigCtrl, side, lastNameWrinkleUiRigCtrl)

            nameWrinkleTexUiRigCtrl, sideWrinkleTexUiRigCtrl, lastNameWrinkleTexUiRigCtrl = cn.splitName(sels = [wrinkleTextureUiRigCtrl] )
            newWrinkleTexUiRigCtrl = '{}{}{}'.format(nameWrinkleTexUiRigCtrl, side, lastNameWrinkleTexUiRigCtrl)

            nameSp, sideSp, lasNameSp = cn.splitName(sels = [wrinklePlaneGeo])
            wrinklePlaneGeo = '{}{}{}'.format(nameSp, side, lasNameSp)




            ## wrinkle Control       
            cfm.imageSeqFacial(
                            uiRigCtrl = newWrinkleUiRigCtrl, 
                            textureUiRigCtrl = newWrinkleTexUiRigCtrl, 
                            shaDowCtrl = wrinkleShadowCtrl, 
                            elem = wrinkleElem, 
                            planeGeo = wrinklePlaneGeo,
                            shadowPlaneGeo = wrinkleShadowPlaneGeo,
                            textureUiGeo = wrinkleTextureUiGeo,
                            texturePath = texturePath
                            )
            ## add localworld
            if wrinkleFollow:
                wrinkleUiRigCtrlGrp = '{}{}Grp'.format(nameWrinkleUiRigCtrl.replace('UiRig', 'UiRigCtrl'), side)
                eyebrowNameSp, eyebrowSideSp, eyebrowLasNameSp = cn.splitName(sels = [wrinkleFollowCtrl])
                eyebrowLoWoCtrl = '{}{}{}'.format(eyebrowNameSp, side, eyebrowLasNameSp)
                utaCore.parentLocalWorldCtrl( ctrl = newWrinkleUiRigCtrl , localObj = eyebrowLoWoCtrl , worldObj = 'FacialUiRig_Ctrl' , parGrp = wrinkleUiRigCtrlGrp)

        print 'Process :: {} >> "DONE"'.format(wrinkleElem)


    if mouthEffUp:
        if not mouthEffUpSide:
            mouthEffUpSide = ['_']
        for eachMouthEffUpSide in mouthEffUpSide:
            side = utaCore.findSide(side = eachMouthEffUpSide)

            nameMouthEffUpUiRigCtrl, sideMouthEffUpUiRigCtrl, lastNameMouthEffUpUiRigCtrl = cn.splitName(sels = [mouthEffUpUiRigCtrl] )
            newMouthEffUpUiRigCtrl = '{}{}{}'.format(nameMouthEffUpUiRigCtrl, side, lastNameMouthEffUpUiRigCtrl)

            nameMouthEffUpTexUiRigCtrl, sideMouthEffUpTexUiRigCtrl, lastNameMouthEffUpTexUiRigCtrl = cn.splitName(sels = [mouthEffUpTextureUiRigCtrl] )
            newMouthEffUpTexUiRigCtrl = '{}{}{}'.format(nameMouthEffUpTexUiRigCtrl, side, lastNameMouthEffUpTexUiRigCtrl)

            nameSp, sideSp, lasNameSp = cn.splitName(sels = [mouthEffUpPlaneGeo])
            mouthEffUpPlaneGeo = '{}{}{}'.format(nameSp, side, lasNameSp)
            ## mouthEffUp Control         
            cfm.imageSeqFacial(
                            uiRigCtrl = newMouthEffUpUiRigCtrl, 
                            textureUiRigCtrl = newMouthEffUpTexUiRigCtrl, 
                            shaDowCtrl = mouthEffUpShadowCtrl, 
                            elem = mouthEffUpElem, 
                            planeGeo = mouthEffUpPlaneGeo,
                            shadowPlaneGeo = mouthEffUpShadowPlaneGeo,
                            textureUiGeo = mouthEffUpTextureUiGeo,
                            texturePath = texturePath
                            )


            ## add localworld
            if mouthEffUpFollow:
                mouthEffUpUiRigCtrlGrp = '{}{}Grp'.format(nameMouthEffUpUiRigCtrl.replace('UiRig', 'UiRigCtrl'), side)
                utaCore.parentLocalWorldCtrl( ctrl = newMouthEffUpUiRigCtrl , localObj = mouthEffUpFollowCtrl , worldObj = 'FacialUiRig_Ctrl' , parGrp = mouthEffUpUiRigCtrlGrp)



        print 'Process :: {} >> "DONE"'.format(mouthEffUpElem)


    if mouthEffDn:
        if not mouthEffDnSide:
            mouthEffDnSide = ['_']
        for eachMouthEffDnSide in mouthEffDnSide:
            side = utaCore.findSide(side = eachMouthEffDnSide)

            nameMouthEffDnUiRigCtrl, sideMouthEffDnUiRigCtrl, lastNameMouthEffDnUiRigCtrl = cn.splitName(sels = [mouthEffDnUiRigCtrl] )
            newMouthEffDnUiRigCtrl = '{}{}{}'.format(nameMouthEffDnUiRigCtrl, side, lastNameMouthEffDnUiRigCtrl)

            nameMouthEffDnTexUiRigCtrl, sideMouthEffDnTexUiRigCtrl, lastNameMouthEffDnTexUiRigCtrl = cn.splitName(sels = [mouthEffDnTextureUiRigCtrl] )
            newMouthEffDnTexUiRigCtrl = '{}{}{}'.format(nameMouthEffDnTexUiRigCtrl, side, lastNameMouthEffDnTexUiRigCtrl)

            nameSp, sideSp, lasNameSp = cn.splitName(sels = [mouthEffDnPlaneGeo])
            mouthEffDnPlaneGeo = '{}{}{}'.format(nameSp, side, lasNameSp)
            ## mouthEffDn Control         
            cfm.imageSeqFacial(
                            uiRigCtrl = newMouthEffDnUiRigCtrl, 
                            textureUiRigCtrl = newMouthEffDnTexUiRigCtrl, 
                            shaDowCtrl = mouthEffDnShadowCtrl, 
                            elem = mouthEffDnElem, 
                            planeGeo = mouthEffDnPlaneGeo,
                            shadowPlaneGeo = mouthEffDnShadowPlaneGeo,
                            textureUiGeo = mouthEffDnTextureUiGeo,
                            texturePath = texturePath
                            )

            ## add localworld
            if mouthEffDnFollow:
                mouthEffDnUiRigCtrlGrp = '{}{}Grp'.format(nameMouthEffDnUiRigCtrl.replace('UiRig', 'UiRigCtrl'), side)
                utaCore.parentLocalWorldCtrl( ctrl = newMouthEffDnUiRigCtrl , localObj = mouthEffDnFollowCtrl, worldObj = 'FacialUiRig_Ctrl'  , parGrp = mouthEffDnUiRigCtrlGrp)

        print 'Process :: {} >> "DONE"'.format(mouthEffDnElem)


    ## Jaw Control          
    if jaw:
        cfm.imageSeqFacial  (
                            uiRigCtrl = jawUiRigCtrl, 
                            textureUiRigCtrl = jawTextureUiRigCtrl, 
                            shaDowCtrl = jawShadowCtrl, 
                            elem = jawElem, 
                            planeGeo = jawPlaneGeo,
                            shadowPlaneGeo = jawShadowPlaneGeo,
                            textureUiGeo = jawTextureUiGeo,
                            texturePath = texturePath
                            )

        print 'Process :: {} >> "DONE"'.format(jawElem)


    ## head ramp control
    if head:
        cfm.generateUvSetRampCtrl(headRamp = headRamp,
                        ctrl = headRampCtrl, 
                        folderName = headRampFolderName, 
                        elem = headRampElem, 
                        geo = headRampGeo,
                        texturePath = texturePath)

        print 'Process :: {} >> "DONE"'.format(headRampElem)



    ## CleanZing Texture Layer Node
    ## chang layerTexutre order
    # utaCore.removeMultiInstanceIndexOrderLayerTexture(layterTexture = [], removeMultiInstance = True)
    cfm.changLayterTextureOrder()

