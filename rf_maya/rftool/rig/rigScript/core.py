import pymel.core as pm
import maya.cmds as mc
import maya.mel as mm
import shutil 
import os
import math

class PyNode( object ) :

    def __init__( self , name ) :
        self.name = str( name )
    
    def __str__( self ) :
        return str( self.name )
        
    def getElemName( self ) :
        elem = self.name.split('_')

        lenElem = len(elem)
        if lenElem == 2 :
            name = elem[0]
            side = '_'
            type = elem[-1]
        else :
            name = elem[0]
            side = '_{}_'.format(elem[1])
            type = elem[-1]
        
        return {'name' : name , 'side' : side , 'type' : type}

    elem = property(getElemName)

    def getName( self ) :
        return self.elem['name']

    def getSide( self ) :
        sides = [ 'LFT' , 'RGT' , 'CEN' ,
                  'Lft' , 'Rgt' , 'Cen' , 
                  'lft' , 'rgt' , 'cen' ,
                  'L' , 'R' , 'C' ,'Fnt', 'Bck']
        
        currentSide = ''
        for side in sides :
            if '_{}_'.format(side) in self.name :
                currentSide = side

        return currentSide

    def getType( self ) :
        return self.elem['type']
            
    def rename( self , newName ) :
        self.name = str(pm.rename(self.name , newName ))
        return self.name

    def getExists( self ) :
        return pm.objExists( self.name )


    def getNodeType( self ) :
        return pm.nodeType( self.name )

    def addTag( self , attrName = '' , str = '' ):
        pm.addAttr( self.name , ln = attrName , dt = 'string' , k = False )
        pm.setAttr( '{}.{}'.format( self.name , attrName ) , str )

    def addBoolTag( self , attrName = '' , val = True ):
        pm.addAttr( self.name , ln = attrName , at = 'bool' , k = False )
        pm.setAttr( '{}.{}'.format( self.name , attrName ) , val )

    def addMsgTag( self , msg = '' ):
        pm.addAttr( msg , ln = 'inbox' , dt = 'string' , k = False )
        pm.connectAttr( '{}.message'.format(self.name) , '{}.inbox'.format(msg) )

    def attr( self , attrName = '' ) :
        return Attribute( '{}.{}'.format( self.name , attrName ))

    def addAttr( self , attrName = '' , minVal = None , maxVal = None , chanelBox = True ) :
        pm.addAttr( self.name , ln = attrName , at = 'float' , k = chanelBox )

        if not minVal == None :
            pm.addAttr( '{}.{}'.format( self.name , attrName ) , e = True , min = int(minVal))
        
        if not maxVal == None :
            pm.addAttr( '{}.{}'.format( self.name , attrName ) , e = True , max = int(maxVal))

    def addAttrFunc( self , attrName = '' , minVal = None , maxVal = None , chanelBox = True , at = 'integer') :
        pm.addAttr( self.name , ln = attrName , at = at , k = chanelBox )
        if not minVal == None :
            pm.addAttr( '{}.{}'.format( self.name , attrName ) , e = True , min = int(minVal))
        
        if not maxVal == None :
            pm.addAttr( '{}.{}'.format( self.name , attrName ) , e = True , max = int(maxVal))
            
    def addTitleAttr( self , attrName = '' ) :
        if not pm.objExists( '{}.{}'.format( self.name , attrName )) :
            pm.addAttr( self.name , ln = attrName , nn = '__{}'.format(attrName) , at = 'float' , k = True )
            self.lockAttrs( attrName )

    def lockHideAttrs( self , *args ) :
        for arg in args :
            pm.setAttr( '{}.{}'.format( self , arg ) , l = True , k = False )
        
    def unlockHideAttrs( self , *args ) :
        for arg in args :
            pm.setAttr( '{}.{}'.format( self , arg ) , l = False , k = True )

    def lockAttrs( self , *args ) :
        for arg in args :
            pm.setAttr( '{}.{}'.format( self , arg ) , l = True )

    def unlockAttrs( self , *args ) :
        for arg in args :
            pm.setAttr( '{}.{}'.format( self , arg ) , l = False )

    def lockHideKeyableAttrs( self ) :
        attrs = pm.listAttr( self.name , k = True )
        
        for attr in attrs :
            if pm.attributeQuery( attr.split( '.' )[ 0 ] , n = self.name , multi = True ) :
                continue
            else :
                pm.setAttr( '{}.{}'.format( self.name , attr ) , l = True , k = False )
    
    def lockKeyableAttrs( self ) :
        attrs = pm.listAttr( self.name , k = True )
        
        for attr in attrs :
            if pm.attributeQuery( attr.split( '.' )[ 0 ] , n = self.name , multi = True ) :
                continue
            else :
                pm.setAttr( '{}.{}'.format( self.name , attr ) , l = True )  

    def listAttr( self ) :
        return pm.listAttr( self.name , s = True , r = True , k = True , ud = True)

class Attribute( object ) :
    
    def __init__( self , attrName = '' ) :
        self.name = str( attrName )
    
    def __str__( self ) :
        return str( self.name )
    
    def __repr__( self ) :
        return str( self.name )

    def __rshift__( self , attr = '' ):
        if pm.objExists( attr ):
            conds = pm.listConnections( self.name , p = True )
            try :
                if conds and not attr in conds:
                    pm.connectAttr( self.name , attr , f = True )
                if conds and attr in conds:
                    print '{} already connected to {} '.format( self.name , attr )
                if not conds:
                    pm.connectAttr( self.name , attr , f = True )
            except:
                print 'Can\'t connect {} to {}'.format( self.name , attr )
        else:
            pm.warning('{} doest\'t exists!'.format(attr) )

    def __floordiv__( self , attr = ''):
        if pm.objExists( attr ):
            conds = pm.listConnections( self.name , p = True )
            try :
                if conds and attr in conds:
                    pm.disconnectAttr( self.name , attr )
                if conds and not attr in conds:
                    print '{} is connected to {} '.format( self.name , conds )
                if not conds:
                    pm.disconnectAttr( self.name , attr , f = True )
            except:
                print 'Can\'t disconnect {} from {}'.format( self.name , attr )
        else:
            pm.warning('{} doest\'t exists!'.format(attr) )

    def getLock( self ) :
        return pm.getAttr( self.name , l = True )

    def setLock( self , lock = True ) :
        pm.setAttr( self.name , l = lock )

    lock = property( getLock , setLock )
    l = property( getLock , setLock )

    def getHide( self ) :
        return pm.getAttr( self.name , k = False )

    def setHide( self , hide = True ) :
        pm.setAttr( self.name , k = not hide )
        pm.setAttr( self.name , cb = not hide )

    hide = property( getHide , setHide )
    h = property( getHide , setHide )

    def setLockHide( self ) :
        pm.setAttr( self.name , l = True , k = False , cb = False )

    def getVal( self ):
        val = pm.getAttr( self.name )
        if type( val ) == type([]) or type( val ) == type(()):
            return val[0]
        else:
            return val

    def setVal( self , val ) :
        lockAttr = pm.getAttr( self.name , l = True )
    
        if lockAttr == True :
            pm.setAttr( self.name , lock = False )
            pm.setAttr( self.name , val )
            pm.setAttr( self.name , lock = True )
        else :
            pm.setAttr( self.name , val )

    value = property( getVal , setVal )
    v = property( getVal , setVal )

    def setMin( self , minVal ) :
        pm.addAttr( self.name , e = True , minValue = minVal )

    def setMax( self , maxVal ) :
        pm.addAttr( self.name , e = True , maxValue = maxVal )

    def setRange( self , minVal , maxVal ) :
        pm.addAttr( self.name , e = True , minValue = minVal , maxValue = maxVal )

    def hasMin( self , has = True) :
        pm.addAttr( self.name , e = True , hnv = has)

    def hasMax( self , has = True) :
        pm.addAttr( self.name , e = True , hxv = has)

    def getConnections( self ) :
        return pm.listConnections( self.name )

    def getPlugs( self ):
        return pm.listConnections( self.name , p = True)

    def getConnectionsSource( self ) :
        return pm.listConnections( self.name , s = True , d = False , p = False , scn = True)

    def getConnectionsDestination( self ) :
        return pm.listConnections( self.name , s = False , d = True , p = False , scn = True)

    connect = property( getConnections )
    source = property( getConnectionsSource )
    destination = property( getConnectionsDestination )
    des = property( getConnectionsDestination )
    plug = property( getPlugs )

    def getExists( self ) :
        return pm.objExists( self )

    exists = property( getExists )

    def getSize( self ) :
        return pm.getAttr( self , s = True)

class Dag( PyNode ) :

    def pynode( self ):
        return pm.general.PyNode( self.name )

    def getShape( self ) :
        shapes = pm.listRelatives( self.name , shapes = True )
        if shapes :
            if len( shapes ) > 1 :
                return shapes[ 0 ]
            else :
                return shapes[ 0 ]

    def getAllShapes( self ):
        shapes = pm.listRelatives( self.name , shapes = True )
        if len( shapes ) > 1 :
            return shapes
        else :
            return shapes[ 0 ]

    def correctShapeName( self ) :
        return pm.rename( self.shape , '{}Shape'.format(self.name) )

    shape = property( getShape)
    allShapes = property( getAllShapes)

    def getColor( self ) :
        return pm.getAttr( '{}.overrideColor'.format(self.shape) )

    def setColor( self , col ) :
        colDict = { 'black'         : 1 ,
                    'gray'          : 2 ,
                    'softGray'      : 3 ,
                    'darkRed'       : 4 ,
                    'darkBlue'      : 5 ,
                    'blue'          : 6 ,
                    'darkGreen'     : 7 ,
                    'magenta'       : 9 ,
                    'brown'         : 11 ,
                    'red'           : 13 ,
                    'green'         : 14 ,
                    'white'         : 16 ,
                    'yellow'        : 17 ,
                    'cyan'          : 18 ,
                    'softGreen'     : 19 ,
                    'pink'          : 20 ,
                    'none'          : 0  }
        
        if type( col ) == type( str() ) :
            if col in colDict.keys() :
                colId = colDict[col]
            else :
                colId = 0
        else :
            colId = col
        
        pm.setAttr( '{}.overrideEnabled'.format(self.shape) , 1 )
        pm.setAttr( '{}.overrideColor'.format(self.shape) , colId )

    def getRotateOrder( self ) :
        dict = { 'xyz' : 0 ,
                 'yzx' : 1 ,
                 'zxy' : 2 ,
                 'xzy' : 3 ,
                 'yxz' : 4 ,
                 'zyx' : 5 }
                  
        id = pm.getAttr( '{}.rotateOrder'.format(self.name) )
        
        for key in dict.keys() :
            if id == dict[ key ] :
                return id

    def setRotateOrder( self , rotateOrder ) :
        dict = { 'xyz' : 0 ,
                 'yzx' : 1 ,
                 'zxy' : 2 ,
                 'xzy' : 3 ,
                 'yxz' : 4 ,
                 'zyx' : 5 }
        
        if rotateOrder in dict.keys() :
            val = dict[ rotateOrder ]
        else :
            val = rotateOrder
            
        pm.setAttr( '{}.rotateOrder'.format(self.name) , val )

    def getPivot( self ) :
        return pm.xform( self.name , q = True , t = True , ws = True  )

    @property
    def getLocalSpace( self ) :
        pars = self.getParent()
        null = Dag( mc.createNode( 'transform' ))
        null.parent(pars)
        null.snap(self.name)
        tr = mc.xform( null, q=True, os=True, t=True )
        ro = mc.xform( null, q=True, os=True, ro=True )
        mc.delete( null )

        return { 'translate' : tr , 'rotate' : ro }

    @property
    def getWorldSpace( self ) :
        null = Dag( mc.createNode( 'transform' ))
        null.snap(self.name)
        tr = mc.xform( null, q=True, ws=True, t=True )
        ro = mc.xform( null, q=True, ws=True, ro=True )
        mc.delete( null )

        return { 'translate' : tr , 'rotate' : ro , 'tx' : tr[0] , 'ty' : tr[1] , 'tz' : tr[2] , 'rx' : ro[0] , 'ry' : ro[1] , 'rz' : ro[2] }

    def getDef( self , deformers ) :
        if deformers :
            listDef = [ deformers ]
        else :
            listDef = [ 'skinCluster' , 'tweak' , 'cluster' ]
        
        deformers = []

        for df in listDef :
            try :
                defNode = pm.listConnections( self.shape , type = df , s=True , d=False )
                defSet = pm.listConnections( defNode , type = 'objectSet' )
                if defNode:
                    deformers.append( defNode[0] )
                if defSet:
                    deformers.append( defSet[0] )
            except TypeError :
                continue

        return deformers

    def getParent( self ) :
        par = pm.listRelatives( self.name , p = True )

        if par :
            return par[0]

    def getChild( self ) :
        return pm.listRelatives( self.name , ad = True , type = 'transform' )

    def parent( self , target = '' ) :
        if target :
            pm.parent( self.name , target )

        pm.select( cl = True )

    def parentShape( self , target = '' ) :
        pm.parent( self.shape , target , r = True , s = True )
        
        target = Dag(target)
        target.correctShapeName()
        
        pm.delete( self.name )

    def freeze( self ) :
        pm.makeIdentity( self.name , a = True )
        pm.select( cl = True )

    def snap( self , target ) :
        pm.delete( pm.parentConstraint ( target , self.name , mo = False ))
        pm.select( cl = True )

    def snapPoint( self , target ) :
        pm.delete( pm.pointConstraint ( target , self.name , mo = False ))
        pm.select( cl = True )

    def snapOrient( self , target ) :
        pm.delete( pm.orientConstraint ( target , self.name , mo = False ))
        pm.select( cl = True )
    
    def snapScale( self , target ) :
        pm.delete( pm.scaleConstraint ( target , self.name , mo = False ))
        pm.select( cl = True )

    def snapAim( self , target , aim = (0,1,0) , upvec = (1,0,0)) :
        pm.delete( pm.aimConstraint ( target , self.name , mo = False  , aimVector = aim , upVector = upvec ))
        pm.select( cl = True )

    def snapJntOrient( self , target ) :
        pm.delete( pm.orientConstraint (( target , self.name ) , mo = False ))
        ro = pm.getAttr( '{}.rotate'.format(self.name) )
        jo = pm.getAttr( '{}.jointOrient'.format(self.name) )
        pm.setAttr( '{}.jointOrient'.format(self.name) , jo[0]+ro[0] , jo[1]+ro[1] , jo[2]+ro[2] )
        pm.setAttr( '{}.rotate'.format(self.name) , 0 , 0 , 0 )

    def snapWorldOrient( self ) :
        worGrp = pm.createNode( 'transform' )
        pm.delete( pm.orientConstraint (( worGrp , self.name ) , mo = False ))
        pm.delete( worGrp )

    def clearConsLocal( self ) :
        listAttr = [ 'translateX' , 'translateY' , 'translateZ' , 'rotateX' , 'rotateY' , 'rotateZ' , 'scaleX' , 'scaleY' , 'scaleZ' ]
        
        for attr in listAttr :
            pm.setAttr( '{}.{}'.format( self.name , attr ) , l = False )

        cons = pm.listRelatives( self.name , ad = True , type = 'constraint' )
        lists = pm.listConnections( self.name , s = True )

        if cons :
            for con in cons :
                for list in lists :
                    if con == list :
                        try :
                            pm.delete(con)
                        except :
                            pass

    def clearConsWorld( self ) :
        listAttr = [ 'translateX' , 'translateY' , 'translateZ' , 'rotateX' , 'rotateY' , 'rotateZ' , 'scaleX' , 'scaleY' , 'scaleZ' ]

        for attr in listAttr :
            pm.setAttr( '{}.{}'.format( self.name , attr ) , l = False )

        lists = pm.listConnections( self.name , s = True )
        for cons in lists:
            if 'Constraint' in pm.objectType(cons):
                try :
                    pm.delete(cons)
                except :
                    pass

    def translateShape( self  , tx , ty , tz ) :
        pm.move( tx ,ty ,tz , self.name + '.cv[:]' , r = True , os = True , wd = True )

    def rotateShape( self , rx , ry , rz ) :
        piv = self.getPivot()
        mc.rotate( rx ,ry ,rz , self.name + '.cv[:]' , r = True , os = True ,  p = ( piv[0] , piv[1] , piv[2] ))

    def scaleShape( self , value ) :
        mc.scale( value , value , value , self.name + '.cv[:]' , r = True )

    def show( self ) :
        pm.setAttr( '{}.v'.format(self.name) , 1 )

    def hide( self ) :
        pm.setAttr( '{}.v'.format(self.name) , 0 )

    def setDisplay( self , type ):
        typeDics = { 'normal'    : 0 ,
                     'template'  : 1 ,
                     'reference' : 2 }

        if type in typeDics.keys():
            pm.setAttr( '{}.overrideEnabled'.format(self.shape) , 1 )
            pm.setAttr( '{}.overrideDisplayType'.format(self.shape), typeDics[type] )

    def hideAttributeAi( self ) :
        for attr in ( 'aiRenderCurve' , 'aiCurveWidth' , 'aiSampleRate' , 'aiCurveShaderR' , 'aiCurveShaderG' , 'aiCurveShaderB' ) :
            if pm.objExists( '{}.{}'.format(self.shape , attr)) :
                pm.setAttr( '{}.{}'.format(self.shape , attr) , k = False , cb = False )

class Node( object ):

    def createNode(self , node="", name="", **kwargs):    
        obj = pm.createNode(node, n=name)
        if kwargs:
            for attr, value in kwargs.iteritems():
                pm.setAttr("{}.{}".format(obj, attr) , value)
        return Dag(obj)

    def group(self, obj = '', type = 'Zro'):
        obj = Dag(obj)
        par = obj.getParent()
        side = obj.elem['side']
        prefix = obj.getName()
        suffix = obj.elem['type']

        grp = self.transform('{}{}{}{}Grp'.format(prefix, suffix, type, side))
        
        grp.snap(obj)
        grp.snapScale(obj)
        obj.parent(grp)

        if par:
            grp.parent(par)

        mc.select(cl = True)
        return grp

    def joint(self, name = 'joint', target = ''):
        jnt = Dag(pm.createNode('joint',n=name))

        if target :
            jnt.snap(target)
            jnt.snapScale(target)
            jnt.freeze()

        mc.select(cl = True)
        return jnt

    def transform(self, name = 'transform' , target = ''):
        transform = Dag(pm.createNode('transform',n=name))

        if target :
            transform.snap(target)
            transform.snapScale(target)
            
        mc.select(cl = True)
        return transform

    def locator(self , name = 'locator' , target = ''):
        obj = pm.spaceLocator(n=name)
        transform = Dag(mc.rename(obj.getParent() , name))
        transform.correctShapeName()
        if target:
            transform.snap(target)
        return transform


    def getNs(nodeName=''):
        """
        Get namespace from given name
        Ex. aaa:bbb:ccc:ddd_grp > aaa:bbb:ccc:
        """
        ns = ''

        if '|' in nodeName:
            nodeName = nodeName.split('|')[-1]

        if ':' in nodeName:

            nmElms = nodeName.split(':')

            if nmElms:
                ns = '%s:' % ':'.join(nmElms[0:-1])
        return ns

    # get NameSpace in scenes
    def nameSpaceLists(self):
        nsList = mc.namespaceInfo(lon = True)
        nameSp = []
        for lists in nsList:
            if lists in ['UI' , 'shared']:
                pass

            else:
                nameSp.append(lists)
        return nameSp


    def createCvAxis(self, axisCheck = ''):
        ## check axis for project and fix axis control
        if axisCheck:
            axisCheck = axisCheck
        else:
            axisCheck = mc.upAxis(q = True, axis = True);

        if 'z' == axisCheck:
            allCtrl = mc.ls('*:*_Ctrl', '*_Ctrl')
            for ctrl in allCtrl:
                ctrlShape = ctrl + 'Shape'
                allcv = mc.ls('{}.cv[:]'.format(ctrlShape), fl = True)
                mc.select(allcv)    
                mm.eval('rotate -r -ocp -os -fo 90 0 0 ;')
        mc.select(cl = True)


    def splitName(self, sels = [], optionSide = True):
        for each in sels:
            if ":" in each:
                objSp = each.split( ":" )
                sp = objSp[0]
                obj = objSp[-1]
            else:
                obj = each
            nameSp = obj.split( "_" )
            if len(nameSp) == 1:
                name = nameSp[0]
                side = ''
                lastName = ''      

            elif len(nameSp) == 2:
                name = nameSp[0]
                if nameSp[-1] == 'L':
                    side = '_' + nameSp[-1] + '_'
                    lastName = ''
                elif nameSp[-1] == 'R':
                    side = '_' + nameSp[-1] + '_'
                    lastName = ''
                else:
                    side = '_'
                    lastName = nameSp[-1]

            else:
                name = nameSp[0]
                side = '_' + nameSp[1] + '_'
                lastName = nameSp[-1]

        if optionSide == True:
            if side == 'L':
                side = side.replace(side, '_L_')
            elif side == 'R':
                side = side.replace(side, '_R_')
            if side == '':
                side == side.replace(side, '_')
        else:
            if side == '_L_':
                side = side.replace(side, 'L')
            elif side == '_R_':
                side = side.replace(side, 'R')
            elif side == '_':
                side = side.replace(side, '')

        return name, side, lastName

    def addAttrCtrl (self, ctrl = '', elem = [], min = 0, max = 1, at = 'long', dv = 0):

        for each in elem:
            
            mc.addAttr(ctrl, ln = each , at = at, min = min , max = max, k = True, dv = dv)
        return elem


    def checkConnectLists(self, object = '', attr = []):
        attrLists = []
        ## Check Connect of FacialBshNode
        for eachAttr in attr:
            conCheck = mc.listConnections(object + '.' + eachAttr , p = True, s = True)
            for eachConCheck in conCheck:
                if 'unit' in eachConCheck:
                    newObject = eachConCheck.replace('.output', '')
                    conCheck = mc.listConnections(newObject + '.input' , p = True, s = True)
            
            if conCheck:
                conCheck = [item for item in conCheck if mc.objectType(item) != 'combinationShape' ]
                attrLists.append(conCheck[0])

        return attrLists

    def checkConnectLists2(self, obj = ''):
        sourceAttrLists = []
        targetAttrLists = []

        ## Check Connect of FacialBshNode
        listsAttr = mc.listAttr( obj, k = True, v = True, l = False)
        for eachAttr in listsAttr:
            if mc.connectionInfo('{}.{}'.format(obj, eachAttr), isDestination=True):
                sourceCon = mc.connectionInfo('{}.{}'.format(obj, eachAttr), sourceFromDestination=True)
                if 'unit' in sourceCon:
                    unitObj = sourceCon.split('.')[0]
                    if mc.connectionInfo('{}.input'.format(unitObj), isDestination=True):
                        unitCon = mc.connectionInfo('{}.input'.format(unitObj), sourceFromDestination=True)
                        sourceAttrLists.append(unitCon)
                        targetCon = '{}.{}'.format(obj, eachAttr)
                        targetAttrLists.append(targetCon)

                else:
                    sourceAttrLists.append(sourceCon)
                    targetCon = '{}.{}'.format(obj, eachAttr)
                    targetAttrLists.append(targetCon)

        return sourceAttrLists, targetAttrLists

    def generateFilePlace2DTexture(self, name = '', side = '', elem = '', node = 'file',):
        fileNode = mc.createNode(node, n = '{}{}{}{}'.format(name, elem, side, node.capitalize()))
        place2DTexture = mc.createNode('place2dTexture', n = '{}{}{}place2DTexture'.format(name, elem, side))

        if node  == 'file':
            mc.connectAttr('{}.coverage'.format(place2DTexture), '{}.coverage'.format(fileNode))
            mc.connectAttr('{}.translateFrame'.format(place2DTexture), '{}.translateFrame'.format(fileNode))
            mc.connectAttr('{}.rotateFrame'.format(place2DTexture), '{}.rotateFrame'.format(fileNode))
            mc.connectAttr('{}.mirrorU'.format(place2DTexture), '{}.mirrorU'.format(fileNode))
            mc.connectAttr('{}.mirrorV'.format(place2DTexture), '{}.mirrorV'.format(fileNode))
            mc.connectAttr('{}.stagger'.format(place2DTexture), '{}.stagger'.format(fileNode))
            mc.connectAttr('{}.wrapU'.format(place2DTexture), '{}.wrapU'.format(fileNode))
            mc.connectAttr('{}.wrapV'.format(place2DTexture), '{}.wrapV'.format(fileNode))
            mc.connectAttr('{}.repeatUV'.format(place2DTexture), '{}.repeatUV'.format(fileNode))
            mc.connectAttr('{}.offset'.format(place2DTexture), '{}.offset'.format(fileNode))
            mc.connectAttr('{}.rotateUV'.format(place2DTexture), '{}.rotateUV'.format(fileNode))
            mc.connectAttr('{}.noiseUV'.format(place2DTexture), '{}.noiseUV'.format(fileNode))
            mc.connectAttr('{}.vertexUvOne'.format(place2DTexture), '{}.vertexUvOne'.format(fileNode))
            mc.connectAttr('{}.vertexUvTwo'.format(place2DTexture), '{}.vertexUvTwo'.format(fileNode))
            mc.connectAttr('{}.vertexUvThree'.format(place2DTexture), '{}.vertexUvThree'.format(fileNode))
            mc.connectAttr('{}.vertexCameraOne'.format(place2DTexture), '{}.vertexCameraOne'.format(fileNode))
            mc.connectAttr('{}.outUV'.format(place2DTexture),'{}.uv'.format(fileNode))
            mc.connectAttr('{}.outUvFilterSize'.format(place2DTexture),'{}.uvFilterSize'.format(fileNode))
            
        elif node == 'ramp':
            mc.connectAttr('{}.outUV'.format(place2DTexture),'{}.uvCoord'.format(fileNode))
            mc.connectAttr('{}.outUvFilterSize'.format(place2DTexture),'{}.uvFilterSize'.format(fileNode))


        return fileNode, place2DTexture

    def lockAttr(self, listObj = [], attrs = [],lock = True, keyable = True):
        # listBow = ("bowARig_lft_grp","bowBRig_rgt_grp")
        # attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')

        for i in range(len(listObj)):
            for x in range(len(attrs)):
                mc.setAttr("%s.%s" % (listObj[i], attrs[x]) , lock = lock, keyable = keyable)


    def add_blendShape(self, src = '', tar = '' , frontOfChain = True, origin = 'local'):
        '''
        Add src shape to tar.
        '''
        bshs = self.find_related_blendshape(tar)

        if not bshs:
            # Create front of chain blend shape if no blend shape is connected to the target.
            self.add_frontOfChain_blendShape(src, tar,frontOfChain , origin)
        else:
            self.append_blendShape(bshs[0], src, tar)

        return True


    def find_related_blendshape(self, obj = ''):
        '''
        Find the blend shape nodes that connected to the shape node of given transform node.
        '''
        chdn = pm.listRelatives(obj, ad=True)

        if not chdn: return False
        
        objSets = pm.listConnections(chdn[0], type = 'objectSet', d = True, s = False)

        if not objSets: return False
        
        bshs = []
        
        for objSet in objSets:

            relBshs = pm.listConnections(objSet, type = 'blendShape', s = True, d = False)

            if not relBshs: continue

            for relBsh in relBshs:
                if not relBsh in bshs:
                    bshs.append(relBsh)

        return bshs


    def add_frontOfChain_blendShape(self, src = '', tar = '', frontOfChain = True, origin = 'local'):
        '''
        Create blend shape node with 'frontOfChain' option to 'tar' object.
        '''
        localName = tar.split(':')[-1]
        tmpNameList = localName.split('_')

        if len(tmpNameList) > 1:
            tmpNameList[-1] = 'Bsn'
        else:
            tmpNameList.append('Bsn')

        bsn = '_'.join(tmpNameList)
        pm.blendShape(src, tar, frontOfChain = frontOfChain, origin = origin, n = bsn)
        pm.setAttr('{}.w[0]'.format(bsn), 1)

    def append_blendShape(self, bsn = '', bshObj = '', targetObj = ''):
        from ncscript2.pymel import rigTools 
        import rigTools
        '''
        Add bshObj to bsn as the last blend shape attr.
        '''
        bshAttrs = pm.aliasAttr(bsn, q = True)
        
        bshIdx = 0
        if bshAttrs:
            bshAttrDict = {}
            for ix in range(1, len(bshAttrs), 2):
                exp = r'([0-9]+)'
                m = rigTools.search(exp, bshAttrs[ix])
                bshAttrDict[int(m.group(1))] = bshAttrs[ix-1]

            bshIdx = sorted(bshAttrDict.keys())[-1] + 1

        pm.blendShape(bsn, e=True, t=(targetObj, bshIdx, bshObj, 1))

        pm.setAttr('{}.weight[{}]'.format(bsn, bshIdx), 1)

        ## Check Project Name
    def checkProject(*args):
        from rf_utils.context import context_info
        reload(context_info)

        asset = context_info.ContextPathInfo()
        projectName = asset.project 
        return projectName