import maya.cmds as mc
import maya.mel as mm
import core
reload( core )
import rigTools as rt
reload( rt )
import ribbonRig as rbn
reload(rbn)

#-- build ribbon weight ui

class Run(object):
    def __init__(self):
        self.winName = 'ribbonRigUI'

    def show(self , *args):
        # check if window exists
        if mc.window ( self.winName , exists = True ) :
            mc.deleteUI ( self.winName ) 
        else : pass

        winWidth = 250
        mc.window(self.winName,title = 'RibbonRig UI' , width = winWidth , s=False, mnb=True, mxb=False)
        mc.columnLayout()
        mc.rowLayout(numberOfColumns=4 , columnWidth4=[winWidth*0.15, winWidth*0.4,winWidth*0.15, winWidth*0.3])
        mc.text(label = 'Prefix', align='center' , width=winWidth*0.15)
        mc.textField('prefix' ,ec='enter', ed = True , width=winWidth*0.4)
        mc.text(label = ' Side ', align='center' , width=winWidth*0.15)
        mc.textField('side' , tx = 'L' ,ec='enter', ed = True , width=winWidth*0.3)
        mc.setParent('..')
        mc.radioButtonGrp('axis',l=' Axis',la3=['X','Y','Z'],numberOfRadioButtons=3,cw4=[winWidth*0.25, winWidth*0.25,winWidth*0.25, winWidth*0.25],cl4=['left','left','left','left'],sl=2)
        mc.radioButtonGrp('direction',l='  Axis Direction',la2=['+','-'],numberOfRadioButtons=2,cw3=[winWidth*0.5, winWidth*0.25,winWidth*0.25],cl3=['left','left','left'],sl=1)
        mc.setParent('..')
        mc.rowLayout(numberOfColumns=3,columnWidth3=[winWidth*0.15,winWidth*0.65, winWidth*0.2])
        mc.text(label = 'PosA', align='center' , width=winWidth*0.15)
        mc.textField('posA' ,ec='enter', ed = True , width=winWidth*0.65)
        mc.button('posAButton' , l = '<<', width=winWidth*0.2 , c = self.posA_cmd)
        mc.setParent('..')
        mc.rowLayout(numberOfColumns=3,columnWidth3=[winWidth*0.15,winWidth*0.65, winWidth*0.2])
        mc.text(label = 'PosB', align='center' , width=winWidth*0.15)
        mc.textField('posB' ,ec='enter', ed = True , width=winWidth*0.65)
        mc.button('posBButton' , l = '<<', width=winWidth*0.2 , c = self.posB_cmd)
        mc.setParent('..')
        mc.radioButtonGrp('hi',l='  hi',la2=['True','False'],numberOfRadioButtons=2,cw3=[winWidth*0.3, winWidth*0.35,winWidth*0.35],cl3=['left','left','left'],sl=1)
        mc.setParent('..')
        mc.radioButtonGrp('startSpace',l='  startSpace',la2=['True','False'],numberOfRadioButtons=2,cw3=[winWidth*0.3, winWidth*0.35,winWidth*0.35],cl3=['left','left','left'],sl=1)
        mc.setParent('..')
        mc.radioButtonGrp('sineDef',l='  sineDef',la2=['True','False'],numberOfRadioButtons=2,cw3=[winWidth*0.3, winWidth*0.35,winWidth*0.35],cl3=['left','left','left'],sl=1)
        mc.setParent('..')
        mc.rowLayout(numberOfColumns=2 , columnWidth2=[winWidth*0.25,winWidth*0.75])
        mc.text(label = 'NumJnts', align='center' , width=winWidth*0.25)
        mc.intField('numJnts' , v = 5  ,ec='enter', ed = True , width=winWidth*0.75)
        mc.setParent('..')
        mc.rowLayout(numberOfColumns=2 , columnWidth2=[winWidth*0.25,winWidth*0.75])
        mc.text(label = 'NurbValue', align='center' , width=winWidth*0.25)
        mc.intField('nurbValue' , v = 5  ,ec='enter', ed = True , width=winWidth*0.75)
        mc.setParent('..')
        mc.rowLayout(numberOfColumns=1,columnWidth1=winWidth)
        mc.button('createRibbon',l='Create Ribbon', width=winWidth*1.01 , height = 40 , al='center' , c = self.run_cmd)
        mc.showWindow()

    def posA_cmd(self , *args):
        strObj = ''
        obj = mc.ls(sl = True)
        if obj != []: strObj = obj[0]
        else: strObj=strObj
        mc.textField('posA', e=True , text=strObj) 
        mc.button('posAButton', e=True, bgc= (0.498039, 1, 0))

    def posB_cmd(self , *args):
        strObj = ''
        obj = mc.ls(sl = True)
        if obj != []: strObj = obj[0]
        else: strObj=strObj
        mc.textField('posB', e=True , text=strObj) 
        mc.button('posBButton', e=True, bgc= (0.498039, 1, 0))

    def parent_cmd(self , *args):
        strObj = ''
        obj = mc.ls(sl = True)
        if obj != []: strObj = obj[0]
        else: strObj=strObj
        mc.textField('parent', e=True , text=strObj) 
        mc.button('parentButton', e=True, bgc= (0.498039, 1, 0))

    def run_cmd(self , *args):
        nameRbn = mc.textField('prefix' , q=True , tx=True)
        sideRbn = mc.textField('side' , q=True , tx=True)
        posARbn = mc.textField('posA' , q=True , tx=True)
        posBRbn = mc.textField('posB' , q=True , tx=True)
        hiRbnBut = mc.radioButtonGrp('hi',query=True, sl=True)
        spcRbnBut = mc.radioButtonGrp('startSpace',query=True, sl=True)
        sneRbnBut = mc.radioButtonGrp('sineDef',query=True, sl=True)
        numJntsRbn = mc.intField('numJnts' , q=True , v=True)
        nurbValueRbn = mc.intField('nurbValue' , q=True , v=True)
        Axis = mc.radioButtonGrp('axis',query=True, sl=True)
        Drt = mc.radioButtonGrp('direction',query=True, sl=True)

        if Axis == 1: ax='x'
        elif Axis == 2: ax='y'
        elif Axis == 3: ax='z'
        if Drt == 1:direction = '+'
        else: direction = '-'
        if hiRbnBut == 1: hiRbn = True
        else: hiRbn = False

        rbn.Run(name = nameRbn,
                axis = '{}{}'.format(ax,direction),
                posiA=posARbn,
                posiB=posBRbn,
                side=sideRbn,
                hi = hiRbn ,
                numJnts=numJntsRbn,
                nurbValue = nurbValueRbn,
                sineDef = sneRbnBut,
                startSpace = spcRbnBut) 
        mc.button('createRibbon', e=True, bgc= (0.498039, 1, 0))