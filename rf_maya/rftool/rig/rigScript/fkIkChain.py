import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
import core
import re
import rigTools as rt
import ctrlRig
import fkRig

reload(fkRig)
reload( core )
reload( rt )
reload(ctrlRig)
# -------------------------------------------------------------------------------------------------------------
#
#  FK RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------
cn = core.Node()

class Run( object ):

    def __init__( self , name       = '' ,
                         side       = '' ,
                         tmpJnt     = [ ] ,
                         parent     = '' ,
                         ctrlGrp    = 'Ctrl_Grp' , 
                         axis       = 'y' ,
                         shape      = 'square' ,
                         jnt_ctrl   = 0 ,
                         size       = 1 ,
                         no_tip     = 0,
                         color      = 'red',
                         localWorld = 1,
                         defaultWorld = 0,
                         dtl = 0,
                         dtlShape = 'cube',
                         dtlColor = 'blue',
                         **kwargs
                 ):

        ##-- Info
        self.name = name
        self.side = side
        if side == '' :
            side = '_'
        else :
            side = '_{}_'.format(side)

        if tmpJnt:
            tmpJnt = tmpJnt
        else:
            search_str = '*{}*{}*TmpJnt'.format(name,side)
            found_list = mc.ls(search_str)
            tmpJnt = []
            for found in found_list:
                match_obj = re.search('{}[0-9]+{}TmpJnt'.format(name,side),found)
                if match_obj:
                    tmpJnt.append(found)    
        
        if axis == 'x':
            axisSq = ( 'sy' , 'sz'  )
        elif axis == 'y':
            axisSq = ( 'sx' , 'sz'  )
        elif axis == 'z':
            axisSq = ( 'sx' , 'sy'  )

        length = len(tmpJnt) - no_tip
        self.ctrlObjArray = []
        self.ctrlGrpArray = []
        self.ctrlArray = []
        self.gmblArray = []
        self.zroArray = []
        self.ofstArray = []
        self.parsArray = []
        self.jntFkArray = []
        self.jntIkArray = []
        self.jntSkinArray = []
        self.dtlArray = []
        self.jointChain = []
        # add on group
        self.drvArray = []
        self.invArray = []

        #-- Create Main Group
        self.ctrlGrp = cn.createNode( 'transform' , '{}Ctrl{}Grp'.format( name , side ))
        self.ctrlGrp.parent(ctrlGrp)

        self.jntGrp = cn.createNode( 'transform' , '{}Jnt{}Grp'.format( name , side ))
        self.jntGrp.parent('Jnt_Grp')

        if mc.objExists(parent):
            mc.parentConstraint( parent , self.ctrlGrp , mo = False )
            mc.scaleConstraint( parent , self.ctrlGrp , mo = False )
        
        #-- Rig process

        #- - Skin Jnt
        for i in range(len(tmpJnt)):
            new_jnt = cn.joint('{}{}{}Jnt'.format(self.name,i+1,side), tmpJnt[i])
            self.jntSkinArray.append(new_jnt)

        # set jnt hierarchy
        for i in range(len(self.jntSkinArray)-1):
            self.jntSkinArray[i+1].parent(self.jntSkinArray[i])
        
        mc.parent(self.jntSkinArray[0] , parent)
        # -- FKJoint

        for i in range(len(tmpJnt)):
            fk_jnt = cn.joint('{}{}{}Jnt'.format(self.name+'Fk',i+1,side), tmpJnt[i])
            self.jntFkArray.append(fk_jnt)

        # set jnt hierarchy
        for i in range(len(self.jntFkArray)-1):
            self.jntFkArray[i+1].parent(self.jntFkArray[i])

        for i in range(length) :
            if not i == length:
                #-- Controls
                if i ==0:
                    localWorldVal = localWorld
                    defaultLWVal = defaultWorld
                    parent_val = parent
                    ctrlGrp_val = self.ctrlGrp
                else:
                    localWorldVal = 0
                    defaultLWVal = 0 
                    parent_val = ''
                    ctrlGrp_val = ''

                fkRig_obj = fkRig.Run(  name        = '{}{}'.format(self.name,i+1),
                                        side        = self.side,
                                        tmpJnt      = self.jntFkArray[i],
                                        parent      = parent_val,
                                        ctrlGrp     = ctrlGrp ,
                                        shape       = shape ,
                                        jnt_ctrl    = jnt_ctrl,
                                        size        = size ,
                                        color       = color,
                                        localWorld  = localWorldVal,
                                        defaultWold = defaultLWVal,
                                        jnt         = 0,
                                        constraint  = 0,
                                        dtl = dtl,
                                        dtlShape = dtlShape,
                                        dtlColor = dtlColor,
                                        **kwargs)
                self.jointChain.append(fkRig_obj.jnt)
                self.ctrlArray.append(fkRig_obj.ctrl)
                self.gmblArray.append(fkRig_obj.gmbl)
                self.zroArray.append(fkRig_obj.zro)
                self.ofstArray.append(fkRig_obj.ofst)
                self.parsArray.append(fkRig_obj.pars)
                self.ctrlObjArray.append(fkRig_obj.ctrl_obj)
                self.ctrlGrpArray.append(fkRig_obj.ctrlGrp)
                if dtl:
                    self.dtlArray.append(fkRig_obj.dtl_obj)
                
                if fkRig_obj.drv:
                    self.drvArray.append(fkRig_obj.drv)

                if fkRig_obj.inv:
                    self.invArray.append(fkRig_obj.inv)
                        
                
                if not i == length and i != 0:
                    if self.gmblArray[i-1]:
                        fkRig_obj.ctrlGrp.parent( self.gmblArray[i-1] )
                    else:
                        fkRig_obj.ctrlGrp.parent( self.ctrlArray[i-1])

                ## joint Chain
                if fkRig_obj.jnt:
                    if not i == length:
                        if not i ==0:
                            mc.parent(fkRig_obj.jnt, self.jntFkArray[i-1])



        #-- Add Stretch
        for i in range(length):
            if not i == 0 :
                if not i > length :
                    rt.addFkStretch( self.ctrlArray[i-1], self.ofstArray[i] , axis )

        rt.addFkStretch( self.ctrlArray[-1], self.jntFkArray[-1] , axis )
            


        #-- Add Squash
        for i in range(length):
            if not i == length :
                rt.addSquash( self.ctrlArray[i], self.jntFkArray[i] , ax = axisSq)

        if dtl:
            for dtl_obj in self.dtlArray:
                dtl_obj.do_constraint('parent')
                dtl_obj.ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' )
        else:
            for ctrl_obj in self.ctrlObjArray:
                ctrl_obj.do_constraint('parent')

        #-- Cleanup
        for ctrl_obj in self.ctrlObjArray :
            ctrl_obj.ctrl.lockHideAttrs('sx' , 'sy' , 'sz')

        for zro in self.zroArray :
            zro.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for ofst in self.ofstArray :
            ofst.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for jnt in self.jntFkArray :
            jnt.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for ctrlGrpA in self.ctrlGrpArray :
            ctrlGrpA.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz'   )

        if parent:
            pm.parentConstraint(parent,self.ctrlGrp,mo=1)

        mc.parent(self.jntFkArray[0] , self.jntGrp)

        self.ctrlGrpArray[0].parent(self.ctrlGrp)




        #-------------------------------------------# 
        #--------------     IK     -----------------# 
        #-------------------------------------------# 

        #- - IK Jnt
        for i in range(len(tmpJnt)):
            ikPosJnt = cn.joint('{}{}{}Jnt'.format(self.name+'Ik',i+1,side), tmpJnt[i])
            self.jntIkArray.append(ikPosJnt)

        # set jnt hierarchy
        for i in range(len(self.jntIkArray)-1):
            self.jntIkArray[i+1].parent(self.jntIkArray[i])

        listLoft = []
        for each in self.jntIkArray:
            loft = rt.createCtrl( '{}_Cuv'.format(each), 'line' , 0 )
            loft.snap(each)
            listLoft.append(loft)

        for ctrl in listLoft:
            ctrl.scaleShape( size * 1 )

        self.spineNrb = core.Dag(mc.loft( listLoft , ch = True , rn = True , ar = True , n = '{}{}Nrb'.format( name , side )) [0])

        self.spineNrbShape = core.Dag(self.spineNrb.shape)
        mc.delete( self.spineNrb , ch = True )
        mc.delete(listLoft)

        mm.eval('rebuildSurface -ch 1 -rpo 1 -rt 0 -end 1 -kr 0 -kcp 1 -kc 0 -su 4 -du 0 -sv 4 -dv 0 -tol 0.01 -fr 0  -dir 2 "{}";'.format(self.spineNrb.name))

        a= mc.createNode('closestPointOnSurface' , n = 'test_CPOS' )
        mc.connectAttr('{}.worldSpace'.format(self.spineNrbShape.name) , '{}.inputSurface'.format(a) , f=1)

        # self.spineNrbShape.worldSpace >> a.inputSurface
        folGrp = mc.createNode('transform' , n = '{}Fol{}Grp'.format(name , side) )

        for each in self.jntIkArray:

            tv = mc.xform(each , ws=1 , q=1 , t=1)

            mc.setAttr('{}.inPositionX'.format(a) , tv[0])
            mc.setAttr('{}.inPositionY'.format(a) , tv[1])
            mc.setAttr('{}.inPositionZ'.format(a) , tv[2])

            uv = mc.getAttr('{}.result.parameterU'.format(a))

            fol = rt.createFollicle(self.spineNrb, each, side, uv, .5)
            mc.parentConstraint(fol.name , each , mo=1)
            mc.parent(fol , folGrp)

        mc.parent(self.spineNrb , 'Still_Grp')
        mc.parent(folGrp , 'Still_Grp')
        mc.delete(a)



        #-- Create Main Group Ik
        # self.IkCtrlGrp = cn.createNode( 'transform' , '{}IkCtrl{}Grp'.format( self.name , side ))
        self.IkJntGrp = cn.createNode( 'transform' , '{}IkJnt{}Grp'.format( self.name , side ))
        # self.IkhGrp = cn.createNode( 'transform' , '{}Ikh{}Grp'.format( self.name , side ))

        # self.IkCtrlGrp.snap( self.ctrlGrp )
        self.IkJntGrp.snap( self.ctrlGrp )
        # self.IkhGrp.snap( self.ctrlGrp )
        mc.parent(self.jntIkArray[0] , self.IkJntGrp)
        # #-- Create Joint Ik
        # self.upArmIkJnt = self.jntIkArray[0]
        # # self.upArmIkJnt = cn.joint( 'UpArm{}Ik{}Jnt'.format( elem , side ) , upArmTmpJnt )
        # # self.forearmIkJnt = cn.joint( 'Forearm{}Ik{}Jnt'.format( elem , side ) , forearmTmpJnt )
        # # self.wristIkJnt = cn.joint( 'Wrist{}Ik{}Jnt'.format( elem , side ) , wristTmpJnt )
        # # self.handIkJnt = cn.joint( 'Hand{}Ik{}Jnt'.format( elem , side ) , handTmpJnt )
        # # self.handIkJnt.parent( self.wristIkJnt )
        # # self.wristIkJnt.parent( self.forearmIkJnt )
        # # self.forearmIkJnt.parent( self.upArmIkJnt )
        # self.upArmIkJnt.parent( self.IkJntGrp )

        # #-- Create Controls Ik
        # self.upArmIkCtrl = rt.createCtrl( '{}IkStart{}Ctrl'.format( self.name , side ) , 'cube' , 'blue' , jnt = True )
        # self.upArmIkGmbl = rt.addGimbal( self.upArmIkCtrl )
        # self.upArmIkZro = rt.addGrp( self.upArmIkCtrl )
        # self.upArmIkOfst = rt.addGrp( self.upArmIkCtrl , 'Ofst' )
        # self.upArmIkPars = rt.addGrp( self.upArmIkCtrl , 'Pars' )
        # self.upArmIkZro.snapPoint( self.jntIkArray[0] )
        # self.upArmIkCtrl.snapJntOrient( self.jntIkArray[0])

        # self.wristIkCtrl = rt.createCtrl( '{}IkEnd{}Ctrl'.format( self.name , side ) , 'cube' , 'blue' , jnt = True )
        # self.wristIkGmbl = rt.addGimbal( self.wristIkCtrl )
        # self.wristIkZro = rt.addGrp( self.wristIkCtrl )
        # self.wristIkOfst = rt.addGrp( self.wristIkCtrl , 'Ofst' )
        # self.wristIkPars = rt.addGrp( self.wristIkCtrl , 'Pars' )
        # self.wristIkZro.snapPoint( self.jntIkArray[-2] )
        # self.wristIkCtrl.snapJntOrient( self.jntIkArray[-2] )

        # self.elbowIkCtrl = rt.createCtrl( '{}IkPole{}Ctrl'.format( self.name , side ) , 'sphere' , 'blue' , jnt = True )
        # self.elbowIkZro = rt.addGrp( self.elbowIkCtrl )
        # self.elbowIkZro.snapPoint( '{}Pole{}TmpJnt'.format(self.name , side) )
        # self.elbowIkZro.snapOrient( '{}Pole{}TmpJnt'.format(self.name , side) )
        # #-- Adjust Shape Controls Ik
        # for ctrl in ( self.upArmIkCtrl , self.upArmIkGmbl , self.wristIkCtrl , self.wristIkGmbl ) :
        #     ctrl.scaleShape( size )

        # self.elbowIkCtrl.scaleShape( size )

        # #-- Adjust Rotate Order Ik
        # for obj in ( self.jntIkArray[0] , self.jntIkArray[-2]  , self.upArmIkCtrl , self.wristIkCtrl ) :
        #     obj.setRotateOrder( 'yxz' )

        # #-- Rig Process Ik
        # self.wristIkLocWor = rt.localWorld( self.wristIkCtrl , ctrlGrp , self.upArmIkGmbl , self.wristIkZro , 'parent' )
        # self.elbowIkLocWor = rt.localWorld( self.elbowIkCtrl , ctrlGrp , self.wristIkCtrl , self.elbowIkZro , 'parent' )

        # #-- Prefered Ankle
        # # rt.preferredAnkleIk(obj = [self.forearmIkJnt], pfx = 45, pfy = 0, pfz = 0)
        # # mc.setAttr ('%s.preferredAngleX' % self.forearmIkJnt, 45)

        # self.crv = rt.drawLine( self.elbowIkCtrl , self.jntIkArray[-2] )

        # self.wristIkhDict = rt.addIkh( '{}{}'.format(self.name , side) , 'ikRPsolver' , self.jntIkArray[0] , self.jntIkArray[-1] )
        # # self.handIkhDict = rt.addIkh( 'Hand{}'.format(elem) , 'ikSCsolver' , self.wristIkJnt , self.handIkJnt )

        # self.wristIkh = self.wristIkhDict['ikh']
        # self.wristIkhZro = self.wristIkhDict['ikhZro']
        # # self.handIkh = self.handIkhDict['ikh']
        # # self.handIkhZro = self.handIkhDict['ikhZro']

        # self.polevector = mc.poleVectorConstraint( self.elbowIkCtrl , self.wristIkh )[0]

        # self.wristIkCtrl.addAttr( 'twist' )
        # # self.wristIkCtrl.addAttr( 'autoStretch' )
        # self.wristIkCtrl.attr( 'twist' ) >> self.wristIkh.attr('twist')

        # mc.pointConstraint( self.upArmIkGmbl , self.jntIkArray[0] , mo = False )
        # mc.parentConstraint( self.wristIkGmbl , self.wristIkhZro , mo = True )
        # # mc.parentConstraint( self.wristIkGmbl , self.handIkhZro , mo = True )

        # # self.ikStrt = rt.addIkStretch( self.wristIkCtrl , self.elbowIkCtrl ,'ty' , 'Arm%s' %elem , self.upArmIkJnt , self.forearmIkJnt , self.wristIkJnt )
        
        # self.wristIkCtrl.attr('localWorld').value = 1
        # # self.wristIkCtrl.attr('autoStretch').value = 1



        self.armCtrl = rt.createCtrl( '{}Switch{}Ctrl'.format( self.name , side  ) , 'stick' , 'green' )
        self.armZro = rt.addGrp( self.armCtrl )
        mc.parent(self.armZro , self.ctrlGrp)

        #-- Cleanup
        # for obj in ( self.armZro , self.jntGrp , self.ctrlGrp ) :
        #     obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        self.armCtrl.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )



        mc.parentConstraint( self.jntSkinArray[-1] , self.armZro , mo = False )



        # # #-- Adjust Hierarchy Ik
        # ikhGrp = 'Ikh_Grp'
        # mc.parent( self.IkCtrlGrp , self.ctrlGrp )
        mc.parent( self.IkJntGrp , self.jntGrp )
        # mc.parent( self.IkhGrp , ikhGrp )
        # mc.parent( self.upArmIkZro ,self.crv , self.IkCtrlGrp )
        # mc.parent( self.wristIkZro , self.elbowIkZro , self.upArmIkGmbl )
        # mc.parent( self.wristIkhZro , self.IkhGrp )

        #-- Blending Fk Ik 
        for i in range(len(self.jntFkArray)):
            rt.blendFkIk( self.armCtrl , self.jntFkArray[i] , self.jntIkArray[i] , self.jntSkinArray[i] )
        # rt.blendFkIk( self.armCtrl , self.forearmFk_obj.jnt , self.forearmIkJnt , self.forearmJnt )
        # rt.blendFkIk( self.armCtrl , self.wristFk_obj.jnt , self.wristIkJnt , self.wristJnt )
        
        # if mc.objExists( '{}SwitchFkIk{}Rev'.format( self.name , side)) :
        #     self.revFkIk = core.Dag('{}SwitchFkIk{}Rev'.format( self.name , side))
        #     self.armCtrl.attr( 'fkIk' ) >> self.IkCtrlGrp.attr('v')
        #     self.revFkIk.attr( 'ox' ) >> self.ctrlGrpArray[0].attr('v')
        # mc.select(cl=1)
        # mc.select(self.revFkIk)
        # #-- Add Hand Scale
        # self.armCtrl.addAttr('handScale')
        # self.armCtrl.attr('handScale').value = 1

        # self.handSclMdv = cn.createNode( 'multiplyDivide' , 'Hand{}Scale{}Mdv'.format( elem , side ))

        # self.armCtrl.attr('handScale') >> self.handSclMdv.attr('i1x')
        # self.handSclMdv.attr('ox') >> self.wristJnt.attr('sx')
        # self.handSclMdv.attr('ox') >> self.wristJnt.attr('sy')
        # self.handSclMdv.attr('ox') >> self.wristJnt.attr('sz')
        # self.handSclMdv.attr('ox') >> self.wristFk_obj.jnt.attr('sx')
        # self.handSclMdv.attr('ox') >> self.wristFk_obj.jnt.attr('sy')
        # self.handSclMdv.attr('ox') >> self.wristFk_obj.jnt.attr('sz')
        # self.handSclMdv.attr('i2x').value = 1
        # self.handJnt.attr('ssc').value = 0

        # #-- Cleanup Fk
        # for obj in ( self.armFkCtrlGrp , self.armFkJntGrp , self.upArmFk_obj.zro , self.upArmFk_obj.ofst , self.forearmFk_obj.zro , self.forearmFk_obj.ofst , self.wristFk_obj.zro , self.wristFk_obj.ofst ) :
        #     obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        # for obj in ( self.upArmFk_obj.ctrl , self.upArmFk_obj.gmbl , self.forearmFk_obj.ctrl , self.forearmFk_obj.gmbl , self.wristFk_obj.ctrl , self.wristFk_obj.gmbl ) :
        #     obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        # #-- Cleanup Ik
        # for obj in ( self.IkCtrlGrp , self.IkJntGrp , self.IkhGrp , self.upArmIkZro , self.elbowIkZro , self.wristIkZro ) :
        #     obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        # for obj in ( self.upArmIkCtrl , self.upArmIkGmbl , self.elbowIkCtrl , self.wristIkCtrl , self.wristIkGmbl ) :
        #     obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )
