import maya.cmds as mc
import maya.mel as mm
from functools import partial
import os , sys
import core
reload( core )
import rigTools as rt
reload( rt )

cn = core.Node()

class Run(object):
    def __init__(self):
        self.winName = 'addMemberDeformerUi'

    def show(self , *args):
        # check if window exists
        if mc.window ( self.winName , exists = True ) :
            mc.deleteUI ( self.winName ) 
        else : pass

        winWidth = 300
        mc.window(self.winName,title = 'Add Member Deformer UI' , width = winWidth , s=False, mnb=True, mxb=False)
        mc.columnLayout()
        mc.rowLayout()
        mc.text(label = '*** Warning, Can not undo ***' , h =30 , w=winWidth)
        mc.setParent('..')
        mc.rowLayout()
        mc.button(label = 'Add All Deform', w=winWidth , h = 60 , c = partial(self.addDeformer, 'all'))
        mc.setParent('..')
        mc.text(label = '' , width = winWidth*1.005 , align = 'center',bgc=(0,0,0))
        mc.setParent('..')
        mc.rowLayout(nc=2)
        mc.button(label = 'Lattice', w=winWidth*0.4975 , h = 40 , c = partial(self.addDeformer, 'ffd'))
        mc.button(label = 'Wire', w=winWidth*0.4975 , h = 40 , c = partial(self.addDeformer, 'wire'))
        mc.setParent('..')
        mc.rowLayout(nc=3)
        mc.button(label = 'Bend', w=winWidth*0.33 , h = 40 , c = partial(self.addDeformer, 'deformBend'))
        mc.button(label = 'Flare', w=winWidth*0.33 , h = 40 , c = partial(self.addDeformer, 'deformFlare'))
        mc.button(label = 'Sine', w=winWidth*0.33 , h = 40 , c = partial(self.addDeformer, 'deformSine'))
        mc.setParent('..')
        mc.rowLayout(nc=3)
        mc.button(label = 'Squash', w=winWidth*0.33 , h = 40 , c = partial(self.addDeformer, 'deformSquash'))
        mc.button(label = 'Twist', w=winWidth*0.33 , h = 40 , c = partial(self.addDeformer, 'deformTwist'))
        mc.button(label = 'Wave', w=winWidth*0.33 , h = 40 , c = partial(self.addDeformer, 'deformWave'))
        mc.setParent('..')


        mc.showWindow()

    def selectedObjs(self , *args):
        objs = mc.ls(sl=True)
        return {'base' : objs[0] , 'child' : objs[1:]}

    def checkNonlinearType(self , obj=''):
        nl = mc.nonLinear(obj,q=True,dt=True)[0]
        nl = core.Dag(nl)
        nlShape = core.Dag(nl.shape)
        return nlShape.getNodeType()

    def queryDeformer(self , objBase = '' , *args):
        prnt = mc.listRelatives( objBase , f=True)

        nodeList = mc.listHistory(prnt , pdo=True)
        dfmList = ['ffd' , 'wire']
        dfmName = []
        dfmType = []
        for each in nodeList:
            nt = mc.objectType(each)
            if nt == 'nonLinear':
                nl = self.checkNonlinearType(objBase)
                dfmName.append(each)
                dfmType.append(nl)
            for eachDfm in dfmList:
                if nt == eachDfm:
                    dfmName.append(each)
                    dfmType.append(nt)

        return {'name' : dfmName , 'type' : dfmType}

    def addDeformer(self , deformerType = 'all' , *args):
        objs = self.selectedObjs()
        objBase = objs['base']
        objChild = objs['child']
        dfm = self.queryDeformer(objBase)
        dfmNameList = dfm['name']
        dfmTypeList = dfm['type']

        for dfmName , dfmType in zip(dfmNameList , dfmTypeList):
            if deformerType == 'all':
                mc.select(objChild , r=True)
                rt.addDeformerMember(deformerName=(dfmName))
                print '----- Add all deformer complete. -----'
            elif deformerType == dfmType:
                mc.select(objChild , r=True)
                rt.addDeformerMember(deformerName=(dfmName))
                print '----- Add {} complete. -----'.format(dfmType)



