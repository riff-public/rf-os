import shutil 
# import os
import maya.cmds as mc
import pymel.core as pm
import maya.mel as mm
import os, sys

# from rf_utils.context import context_info
from rf_utils.context import context_info
from rf_utils import file_utils

#path project config
rootScript = os.environ['RFSCRIPT']
pathProjectJson = r"{}\core\rf_maya\rftool\rig\export_config\project_config.json".format(rootScript)
projectData = file_utils.json_loader(pathProjectJson)


def getDataFld(subPath = '', *args) : 
	# Project Name 
	asset = context_info.ContextPathInfo()
	projectName = asset.project  
	if projectName == 'CA' and not subPath:
		asset.context.update(process='rigData')
		rigDataPath = asset.path.process()
		dataFld = rigDataPath.replace('$RFPROJECT', 'P:')
		if not os.path.isdir( dataFld ) :
			os.mkdir( dataFld ) 

		# return dataFld  
	elif subPath:
		wfn = os.path.normpath(pm.sceneName()) 
		tmpAry = wfn.split( '\\' )
		tmpAry[-4] = subPath 
		dataFld = '\\'.join( tmpAry[0:-3] ) 
		if not os.path.isdir( dataFld ) :
			os.mkdir( dataFld ) 

 	else :
		wfn = os.path.normpath(pm.sceneName()) 
		tmpAry = wfn.split( '\\' )
		tmpAry[-3] = 'rigData' 
		dataFld = '\\'.join( tmpAry[0:-2] ) 
		if not os.path.isdir( dataFld ) :
			os.mkdir( dataFld ) 
	return dataFld  


def copyObjFile(src,*args):
    shutil.copy(src, getDataFld())

def getRigDataPath(rigData = 'rigData', filePyName = 'caFacialCtrlRun', lastNameType = '.py',*args):

	asset = context_info.ContextPathInfo() 
	scn_path = os.path.normpath( mc.file( q = True , l = True)[0])
	scn_fld_path , scn_nm_ext = os.path.split(scn_path)
	scn_fld_data , scn_he_ext = os.path.split(scn_fld_path)

	rig_path, xx = os.path.split(scn_fld_data)
	rd_path = os.path.join(rig_path, rigData)
	fullPath = os.path.join(rd_path, '{}{}'.format(filePyName, lastNameType))

	return fullPath