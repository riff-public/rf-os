import maya.cmds as mc
# check if window exists
if mc.window ( 'KeyAnimatedUI' , exists = True ) :
	mc.deleteUI ( 'KeyAnimatedUI' ) 
else : pass

def buildKeyFreameUI():
    winWidth = 250
    tmpWidth = [winWidth*0.4, winWidth*0.6]
    tmpWidth2 = [winWidth*0.4,winWidth*0.2,winWidth*0.2,winWidth*0.2]
    tmpWidth3 = [winWidth*0.4,winWidth*0.2,winWidth*0.2]
    mc.window('KeyAnimatedUI',title = 'KeyAnimated' , width = winWidth)
    mainCL = mc.columnLayout()
    mc.rowLayout(numberOfColumns=2 , columnWidth2=tmpWidth)
    mc.text(label = '  KeyValue (300f)', align='left' , width=tmpWidth[0])
    mc.floatField('KeyValue' ,ec='enter',v=0, ed = True , width=tmpWidth[1])
    mc.setParent('..')
    mc.radioButtonGrp('Axis',l='  Axis T (in Rtt)',la3=['X','Y','Z'],numberOfRadioButtons=3,cw4=tmpWidth2,cl4=['left','left','left','left'],sl=2)
    mc.radioButtonGrp('Direction',l='  Axis Direction',la2=['+','-'],numberOfRadioButtons=2,cw3=tmpWidth3,cl3=['left','left','left'],sl=1)
    mc.setParent('..')
    mc.rowLayout(numberOfColumns=2)
    mc.button('Key Rotate', width=winWidth*0.5 , c = KeyRotate_cmd)
    mc.button('Key Translate', width=winWidth*0.5 , c = KeyTranslate_cmd)
    mc.setParent(mainCL)
    mc.text(label = '' , width = winWidth*1.015 , align = 'center',bgc=(0,0,0))
    mc.text(label = 'deleteKeyValue' , align = 'center' , width = winWidth)
    mc.rowLayout(numberOfColumns=2)
    mc.button('Clear Selected', width=winWidth*0.5 , c = ClearSelected_cmd)
    mc.button('Clear All', width=winWidth*0.5 , c = ClearAll_cmd)
    mc.setParent('..')
    mc.rowLayout(numberOfColumns=1)
    mc.button('Set All Ctrl', width=winWidth*1.015,c=SetAllCtrl_cmd)
    mc.setParent('..')
    mc.rowLayout(numberOfColumns=1)
    mc.button('Set All Ctrl (Char)', width=winWidth*1.015,c=SetCharAllCtrl_cmd)
    mc.setParent('..')
    mc.showWindow()
    
#buildKeyFreameUI()

def KeyRotate_cmd(*args):
    Value = mc.floatField('KeyValue' , q=True , value= True)
    Axis = mc.radioButtonGrp('Axis',query=True, sl=True)
    Drt = mc.radioButtonGrp('Direction',query=True, sl=True)
    if Drt == 1:ValueT = Value
    else: ValueT = -Value
    mc.setKeyframe(v=0, at='rx' ,t=1)
    mc.setKeyframe(v=Value, at='rx' ,t=20)
    mc.setKeyframe(v=Value*-1, at='rx' ,t=60)
    mc.setKeyframe(v=0, at='rx' ,t=80)
    mc.setKeyframe(v=0, at='ry' ,t=80)
    mc.setKeyframe(v=Value, at='ry' ,t=100)
    mc.setKeyframe(v=Value*-1, at='ry' ,t=140)
    mc.setKeyframe(v=0, at='ry' ,t=160)
    mc.setKeyframe(v=0, at='rz' ,t=160)
    mc.setKeyframe(v=Value, at='rz' ,t=180)
    mc.setKeyframe(v=Value*-1, at='rz' ,t=220)
    mc.setKeyframe(v=0, at='rz' ,t=240)
    mc.setKeyframe(at='translate' ,t=240)
    if Axis == 1: mc.setKeyframe(v=ValueT*0.1, at='tx' ,t=300)
    elif Axis == 2: mc.setKeyframe(v=ValueT*0.1, at='ty' ,t=300)
    elif Axis == 3: mc.setKeyframe(v=ValueT*0.1, at='tz' ,t=300)
    print ">>Key Rotate %f"%Value

def KeyTranslate_cmd(*args):
    Value = mc.floatField('KeyValue' , q=True , value= True)
    print Value
    mc.setKeyframe(v=0, at='tx' ,t=1)
    mc.setKeyframe(v=Value, at='tx' ,t=20)
    mc.setKeyframe(v=Value*-1, at='tx' ,t=60)
    mc.setKeyframe(v=0, at='tx' ,t=80)
    mc.setKeyframe(v=0, at='ty' ,t=80)
    mc.setKeyframe(v=Value, at='ty' ,t=100)
    mc.setKeyframe(v=Value*-1, at='ty' ,t=140)
    mc.setKeyframe(v=0, at='ty' ,t=160)
    mc.setKeyframe(v=0, at='tz' ,t=160)
    mc.setKeyframe(v=Value, at='tz' ,t=180)
    mc.setKeyframe(v=Value*-1, at='tz' ,t=220)
    mc.setKeyframe(v=0, at='tz' ,t=240)
    print ">>Key Translate %f"%Value
    
def ClearSelected_cmd(*args):
    sel = mc.ls(sl=True)
    mc.currentTime(0)
    mc.cutKey(sel)
    print ">>Clear Selected"
    
def ClearAll_cmd(*args):
    sel = mc.ls("*:"+"*_Ctrl","*:"+"*_ctrl","*_Ctrl","*_ctrl")
    mc.currentTime(0)
    mc.cutKey(sel)
    print ">>Clear All Ctrl"
    
def SetAllCtrl_cmd(*args):
    sel = mc.ls("*:"+"*_Ctrl","*:"+"*_ctrl")
    attr = ['.translateX' , '.translateY' , '.translateZ' , '.rotateX' , '.rotateY' , '.rotateZ' , '.scaleX' , '.scaleY' , '.scaleZ']
    for eachCtrl in sel:
        for eachAttr in attr:
            obj = eachCtrl+eachAttr
            if mc.listConnections(obj , s=True , d=False):
                pass
            else:
                if not mc.getAttr(obj , l = True ):
                    if eachAttr == '.scaleX':
                        mc.setAttr(obj,1)
                    elif eachAttr == '.scaleY':
                        mc.setAttr(obj,1)
                    elif eachAttr == '.scaleZ':
                        mc.setAttr(obj,1)
                    else:
                        mc.setAttr(obj,0)
                elif mc.getAttr(obj , l = True ):
                    pass
    print ">>Clean All Ctrl"

def SetCharAllCtrl_cmd(*args):
    sel = mc.ls("*:"+"*_Ctrl","*:"+"*_ctrl","*_Ctrl","*_ctrl")
    for eachCtrl in sel:
        attr = mc.listAttr(eachCtrl,k=True,v=True)
        for eachAttr in attr:
            obj = eachCtrl+'.'+eachAttr
            if mc.listConnections(obj , s=True , d=False):
                pass
            else:
                if not mc.getAttr(obj , l = True ):
                    if eachAttr == 'scaleX':
                        mc.setAttr(obj,1)
                    elif eachAttr == 'scaleY':
                        mc.setAttr(obj,1)
                    elif eachAttr == 'scaleZ':
                        mc.setAttr(obj,1)
                    elif eachAttr == 'handScale':
                        mc.setAttr(obj,1)
                    elif eachAttr == 'footScale':
                        mc.setAttr(obj,1)
                    elif eachAttr == 'eyelidsFollow':
                        pass
                    elif eachAttr == 'visibility':
                        pass
                    elif eachAttr == 'localWorld':
                        pass
                    elif eachAttr == 'fkIk':
                        pass
                    elif eachAttr == 'autoStretch':
                        pass
                    elif eachAttr == 'autoTwist':
                        pass
                    elif eachAttr == 'spaceFollow':
                        pass
                    else:
                        mc.setAttr(obj,0)
                elif mc.getAttr(obj , l = True ):
                    pass
    mc.setAttr("*:"+'Arm_L_Ctrl.fkIk',0)
    mc.setAttr("*:"+'Arm_R_Ctrl.fkIk',0)
    mc.setAttr("*:"+'Leg_L_Ctrl.fkIk',1)
    mc.setAttr("*:"+'Leg_R_Ctrl.fkIk',1)
    CtrlS = mc.ls("*:"+"*_CtrlShape","*_CtrlShape")
    for eachCtrlS in CtrlS:
        attrS = mc.listAttr(eachCtrlS,k=True , v=True)
        for setS in attrS:
            if 'detailCtrl' in attrS:
                mc.setAttr(eachCtrlS+'.detailCtrl',0)
            elif 'gimbalControl' in attrS:
                mc.setAttr(eachCtrlS+'.gimbalControl',0)
            else:
                pass
    print ">>Clean All Ctrl"