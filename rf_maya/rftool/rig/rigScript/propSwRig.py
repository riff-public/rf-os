from rf_maya.rftool.rig.rigScript import core
from rf_maya.rftool.rig.rigScript import ctrlRig
from rf_maya.rftool.rig.rigScript import crvFunc
from rf_maya.rftool.rig.rigScript import weightTools
cn = core.Node()
import maya.cmds as mc
import maya.mel as mel
import pymel.core as pm
import re
from utaTools.utapy import utaCore
reload(utaCore)
import json
import math
from rf_maya.rftool.shade import shade_utils
reload(shade_utils)
from rf_utils.context import context_info
reload(context_info)
import os

def caTmpJntSelected():
    sel = mc.ls(sl=True)
    jnt_list = []
    for i in sel:
        mc.select(i)
        clus = mc.cluster()
        jnt = mc.createNode('joint',n = i.replace('_Geo','_TmpJnt'))
        mc.delete(mc.parentConstraint(clus[1],jnt,mo=0))
        mc.delete(clus)
        jnt_list.append(jnt)
    return jnt_list


def caWrapJoint(obj, extra = '',parent = '',no_suffixGrp = 0):
    ori = core.PyNode(obj)
    prefix = ori.getName()
    suffix = ori.elem['type']
    side = ori.elem['side']
    jnt = cn.joint('{}{}{}Jnt'.format(prefix,extra, side),obj)
    if  no_suffixGrp:
        grp = cn.transform('{}{}Zro{}Grp'.format(prefix,extra, side))
    else:
        grp = cn.transform('{}{}{}Zro{}Grp'.format(prefix,suffix,extra, side))
    drvGrp = cn.transform('{}Drv{}Zro{}Grp'.format(prefix,extra, side))
    drvGrp.parent(grp)
    grp.snapPoint(jnt)
    jnt.parent(drvGrp)
    jnt.freeze()
    if parent:
        grp.parent(parent)
    return(jnt,grp)
    
    
def caPropShrinkWrapJnt(swTo = 'SC_001:Hood_Geo', faceFclRigGeoGrp = 'Eff_md:EffGeo_Grp', skinGrp = 'SC_001:Skin_Grp'):
    # swTo = 'HoodBffr_Geo'
    # faceFclRigGeoGrp = 'Eff_md:EffGeo_Grp'
    # skinGrp = 'SC_001:Skin_Grp'
    ## duplicate headBffrGeo
    if not swTo:
        sels = mc.ls(sl = True)
        swTo = sels[0]
        nsObjA = utaCore.getNs(nodeName = sels[0])
        nameSpObjA, sideObjA, lastNameObjA = cn.splitName(sels = [sels[0]])
        nameObjA = nameSpObjA.split(':')[-1]
        headBffrGeo = mc.duplicate(swTo, n = '{}Bffr{}{}'.format(nameObjA, sideObjA, lastNameObjA))[0]

        swTo = headBffrGeo

    if not faceFclRigGeoGrp:
        sels = mc.ls(sl = True)
        faceFclRigGeoGrp = sels[-1]
        nsObjB = utaCore.getNs(nodeName = sels[-1])
        nameSpObjB, sideObjB, lastNameObjB = cn.splitName(sels = [sels[-1]])
        nameObjB = nameSpObjB.split(':')[-1]

        faceFclRigGeoGrp = '{}:Geo{}{}'.format(nameObjB, sideObjB, lastNameObjB)

    if skinGrp:
        if not mc.objExists(skinGrp):
            grp = utaCore.createGrp(lists= [skinGrp])
            skinGrp = grp[0]

    sels = utaCore.listGeo(sels = faceFclRigGeoGrp, nameing = '_Geo', namePass = '_Grp')

    for objName in sels:
        obj = objName.split('|')[-1]
        objNoNs = obj.split(':')[-1]
        split = objNoNs.split('_')
        if len(split) == 3:
            side = split[1]
        else:
            side = ''
        mc.select(obj)
        tmpJnt = mc.createNode('joint',n=objNoNs.replace('_Geo','_TmpJnt'))
        jnt,grp = caWrapJoint(tmpJnt,no_suffixGrp=1)
        dtlJnt,dtlGrp = caWrapJoint(jnt.name,'Dtl',jnt.name)
        if side == 'R':
            mc.setAttr(grp.name +'.ry',180)
            mc.setAttr(grp.name +'.sz',-1)


        mc.select([dtlJnt,obj])
        mc.skinCluster(tsb=1)
        mc.delete(tmpJnt)
        grp.parent(skinGrp)
        mc.select(swTo)
        sw = mc.deformer(obj,type= 'shrinkWrap',n = objNoNs.replace('_Geo','_Sw'))[0]
        num = mc.getAttr(swTo+'.worldMesh',s=1)
        mc.connectAttr('{}.worldMesh[{}]'.format(swTo,num-1),sw+'.targetGeom',f=1)
        mc.setAttr(sw+'.projection',4)
        mc.setAttr(sw+'.targetSmoothLevel',2)
        mc.addAttr(jnt.name,ln='wrap',dv=1,max=1,min=0)
        mc.connectAttr(jnt.name+'.wrap',sw+'.envelope',f=1)
        mc.addAttr(jnt.name,ln='offset',dv=.02)
        mc.connectAttr(jnt.name+'.offset',sw+'.offset',f=1)
        
        ctrl = obj.replace('_Geo','_Ctrl')
        pma = mc.createNode('plusMinusAverage',n= ctrl.replace('_Ctrl','_Pma'))
        mdv = mc.createNode('multiplyDivide',n= ctrl.replace('_Ctrl','_Mdv'))
        shp = mc.listRelatives(ctrl,s=1)[0]
        mc.addAttr(ctrl,ln='wrap',max=1,min=0,dv=1,k=1)
        mc.addAttr(ctrl,ln='offset',k=1)
        mc.addAttr(shp,ln='offsetAmp',k=1)
        mc.connectAttr(ctrl+'.wrap',jnt.name+'.wrap')
        mc.connectAttr(ctrl+'.offset',pma+'.input1D[0]')
        mc.connectAttr(shp+'.offsetAmp',pma+'.input1D[1]')
        mc.connectAttr(pma+'.output1D',mdv+'.input1X')
        mc.connectAttr(mdv+'.outputX',jnt.name+'.offset')
        mc.setAttr(mdv+'.input2X',.03)    

    