import os
import maya.cmds as mc
import maya.mel as mel
from ngSkinTools2.api import InfluenceMappingConfig, VertexTransferMode
from ngSkinTools2 import api as ngst_api
from ngSkinTools2 import operations as ngst_op
from ngSkinTools2 import operations as ngst_op
from ngSkinTools2.api import plugin
from rf_utils.context import context_info

def getDataFld():
    context = context_info.ContextPathInfo()
    if context.name:
        projectPath = os.environ['RFPROJECT']
        fileWorkPath = (context.path.name()).replace('$RFPROJECT',projectPath)
        path = fileWorkPath + '/rig/rigData'
        if not os.path.isdir(path):
            os.mkdir(path)

        path = '{}/ngLayers'.format(path)
        if not os.path.isdir(path):
            os.mkdir(path)
        return path
 
    filePath = mc.file(q=True , loc=True)
    if not filePath:
        print "Please save file."
        return

    elemPath = filePath.split('/')
    localPath = '/'.join(elemPath[0:-1])
    path = '{}/rigData'.format(localPath)
    if not os.path.isdir(path):
        os.mkdir(path)

    sknPath = '{}/ngLayers'.format(path)
    if not os.path.isdir(sknPath):
        os.mkdir(sknPath)
    return sknPath

def writeNgSkinLayers(geo,flPath):
    ngst_api.export_json(geo, file = flPath)
    
def writeSelectedNgSkinLayers(userSuffix = ''):
    for sel in mc.ls(sl=True):
        dataFld = getDataFld()
        if userSuffix:
            userSuffix_str ='_{}'.format(userSuffix)
        else:
            userSuffix_str = ''

        fn = '{}/{}NgLayers{}.json'.format(dataFld , sel, userSuffix_str)
        writeNgSkinLayers(sel , fn)

    mc.confirmDialog( title='Progress' , message='Exporting NgLayers has done.' )

def readNgSkinLayers(geo,flPath):
    # prerequisites: mesh with skinCluster attached, "sampleMesh" in our case
    # configure how influences described in a file will be matched against the scene
    config = InfluenceMappingConfig()
    config.use_distance_matching = True
    config.use_name_matching = True
    # old_skinLayer = ngst_api.target_info.get_related_data_node(geo)
    # if old_skinLayer:
    #     clearNgSkinLayers(geo)

    ngst_op.layers.initializeLayers(geo)

    # run the import
    data = plugin.ngst2tools(
        tool="importJsonFile",
        file= flPath)
    influences = ngst_api.target_info.unserialize_influences_from_json_data(data['influences'])

    jnt_list = []
    for inf in data['influences']:
        jnt_list.append( inf['path'])

    jnt_list = mc.ls(jnt_list)
    geo_skn = mel.eval('findRelatedSkinCluster "{}";'.format(geo))
    infs = mc.skinCluster(geo_skn,inf=1,q=1)

    nonMatch_jnt = list(set(jnt_list)-set(infs))
    mc.skinCluster(geo_skn,e=1,ai = nonMatch_jnt)

    ngst_api.import_json(
        geo,
        file = flPath,
        vertex_transfer_mode=VertexTransferMode.vertexId,
        influences_mapping_config=config,)
    geo_layer = ngst_api.init_layers(geo_skn)

    deletedTransferBaseWeightLayer(geo_layer)
    return geo_layer

def readSelectedNgSkinLayers(layerFolderPath = '' , userSuffix = ''):

    if not layerFolderPath:
        layerFolderPath = getDataFld()
    if userSuffix:
        userSuffix_str = '_{}'.format(userSuffix)
    else:
        userSuffix_str = ''

    sels = mc.ls(sl=True)
    for sel in sels:
        fn = '{}/{}NgLayers{}.json'.format(layerFolderPath , sel, userSuffix_str)
        print fn
        try:
            readNgSkinLayers(sel , fn)
            print 'Import {} done!!.'.format(fn)
        except Exception as e:
            print 'Found some issue from process {}: '.format(sel)
            print e

    mc.select(sels)
    mc.confirmDialog( title='Progress' , message='Importing NgLayers is done!!.' )


def disabledNgLayer(obj):
    ngLayers = ngst_api.init_layers(obj)
    for layer in ngLayers.list()[1:]:
        layer.enabled =0

def enabledNgLayer(obj):    
    ngLayers = ngst_api.init_layers(obj)
    for layer in ngLayers.list()[1:]:
        layer.enabled =1

def selectedDisabledNgLayer():
    sel = mc.ls(sl=True)
    for obj in sel:
        disabledNgLayer(obj)

def selectedEnabledNgLayer():
    sel = mc.ls(sl=True)
    for obj in sel:
        enabledNgLayer(obj)


def deletedTransferBaseWeightLayer(layers):
    for i in layers.list()[1:]:
        if i.name.lower() == 'base weights':
            idx = i.index
            layers.delete(i)
            print 'delete BaseWeights'
        else:
            print 'old BaseWeights not found'
            #plugin.ngst2Layers(layers.mesh, removeLayer=True, id=idx)

def transferNgLayer(main,target):
    main_skn = mel.eval('findRelatedSkinCluster "{}";'.format(main))
    main_infs = mc.skinCluster(main_skn,q=1,inf=1)
    target_skn = mel.eval('findRelatedSkinCluster "{}";'.format(target))
    target_infs = mc.skinCluster(target_skn,q=1,inf=1)
    remain_infs = list(set(main_infs)-set(target_infs))
    mc.skinCluster(target_skn,e=1,ai=remain_infs,lw=1,wt=0,)
    mc.select(target)
    ngst_op.layers.initializeLayers()
    ngst_api.transfer_layers(main, target)
    #tar_layer = ngst_api.init_layers(target_skn)
    tar_layer = ngst_api.init_layers(target)
    deletedTransferBaseWeightLayer(tar_layer)
    mc.select(target)
    mel.eval('removeUnusedInfluences')
    mc.select(d=1)

def selectedTransferNgLayers():
    sel = mc.ls(sl=True)
    main = sel[0]
    targets = sel[1:]
    layer_list = []
    for target in targets:
        mc.select(d=1)
        print main , target
        transferNgLayer(main,target)
        mc.refresh()
        #layer_list.append(tar_layer)
    #return layer_list

def removeNgSkinLayers(geo):
    disabledNgLayer(geo)
    mc.delete(ngst_api.target_info.get_related_data_node(geo))

def selectedRemoveNgSkinLayers():
    sel = mc.ls(sl=True)
    for geo in sel:
        removeNgSkinLayers(geo)

def cleanNgSkinLayers(geo):
    mc.delete(ngst_api.target_info.get_related_data_node(geo))

def selectedCleanNgSkinLayers():
    sel = mc.ls(sl=True)
    for geo in sel:
        cleanNgSkinLayers(geo)
