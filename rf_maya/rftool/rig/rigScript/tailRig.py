import maya.cmds as mc
import maya.mel as mm
import core
import rigTools as rt
import ribbonRig
import ctrlRig
import fkRig
reload( core )
reload( rt )
reload( ribbonRig )

# -------------------------------------------------------------------------------------------------------------
#
#  TAIL RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------
cn = core.Node()
class Run( object ):

    def __init__( self , name          = 'Tail' ,
                         numCtrl       = 8 ,
                         curveTmp      = 'Tail_CuvTemp' ,
                         curveUpVecTmp = 'TailUpvec_CuvTemp' ,
                         parent        = 'Pelvis_Jnt' ,
                         ctrlGrp       = 'Ctrl_Grp' ,
                         jntGrp        = 'Jnt_Grp' ,
                         skinGrp       = 'Skin_Grp' ,
                         stillGrp      = 'Still_Grp' ,
                         ikhGrp        = 'Ikh_Grp' ,
                         elem          = '' ,
                         side          = '' ,
                         axis          = 'y' ,
                         size          = 1
                 ):

        ##-- Info
        if 'x' in axis :
            self.aimVec = [ 1 , 0 , 0 ]
            self.invAimVec = [ -1 , 0 , 0 ]
            self.upVec = [ 0 , 0 , 1 ]
        
        elif 'y' in axis :
            self.aimVec =  [ 0 , 1 , 0 ]
            self.invAimVec = [ 0 , -1 , 0 ]
            self.upVec = [ 1 , 0 , 0 ]
        
        elif 'z' in axis :
            self.aimVec = [ 0 , 0 , 1 ]
            self.invAimVec = [ 0 , 0 , -1 ]
            self.upVec = [ 0 , 1 , 0 ]
            
        fkNum = numCtrl
        ikNum = ((numCtrl-1)*3)+numCtrl
        print ikNum, '..ikNum'
        
        self.side = side

        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        

        #-- Create Main Group
        self.tailCtrlGrp = cn.createNode( 'transform' , '{}{}Ctrl{}Grp'.format( name ,  elem , side ))
        self.tailJntGrp = cn.createNode( 'transform' , '{}{}Jnt{}Grp'.format( name , elem , side ))
        self.tailIkhGrp = cn.createNode( 'transform' , '{}{}Ikh{}Grp'.format( name , elem , side ))
        self.tailStillGrp = cn.createNode( 'transform' , '{}{}Still{}Grp'.format( name , elem , side ))
        self.tailCtrlGrp.parent( ctrlGrp )
        self.tailJntGrp.parent( jntGrp )
        self.tailIkhGrp.parent( ikhGrp )
        self.tailStillGrp.parent( stillGrp )
        
        if parent :
            mc.parentConstraint( parent , self.tailCtrlGrp , mo = False )
        
        #-- Curve for Draw Joint
        cuvPosi = mc.rename( mc.duplicate( curveTmp )[0] , 'TempPosi_Cuv' )
        cuvUpvec = mc.rename( mc.duplicate( curveUpVecTmp )[0] , 'TempUpvec_Cuv' )
        mc.rebuildCurve( cuvPosi , rt   = 0 , s = ((numCtrl-1)*3)+(numCtrl-1))
        mc.rebuildCurve( cuvUpvec , rt = 0 , s = ((numCtrl-1)*3)+(numCtrl-1))
    
        #-- Create Joint
        self.jntFkDict = []
        self.jntIkDict = []
        self.jntIkPosDict = []
        self.jntIkUpvecDict = []
    
        eps = mc.getAttr( '{}.spans'.format(cuvPosi) )
        
        #-- Create Joint Fk
        ep = 0
        numLoop = 1
        while ep <= eps :
            posi = mc.pointPosition( '{}.ep[{}]'.format(cuvPosi , ep))
            jntFk = cn.joint( '{}Fk{}{}{}Jnt'.format(name , elem , numLoop , side))
            mc.setAttr( '{}.t'.format(jntFk) , posi[0] , posi[1] , posi[2] )
            self.jntFkDict.append(jntFk)
            
            ep += 4
            numLoop += 1

        #-- Create Joint Ik
        num = 1
        ep = 0
        while num <= numCtrl :
            for each in [ '' , 'A' , 'B' , 'C' ] :
                if not ep > ((numCtrl-1)*3)+numCtrl-1 :
                    posi = mc.pointPosition( '{}.ep[{}]'.format(cuvPosi , ep))
                    posiUpvec = mc.pointPosition( '{}.ep[{}]'.format(cuvUpvec , ep))
                    jntIkPos = cn.joint( '{}Ik{}{}{}Pos{}Jnt'.format(name , elem , num , each , side))
                    jntIk = cn.joint( '{}Ik{}{}{}{}Jnt'.format(name , elem , num , each , side))
                    jntIkUpvec = cn.joint( '{}Ik{}{}{}Upvec{}Jnt'.format(name , elem , num , each , side))
                    mc.setAttr( '{}.t'.format(jntIkPos) , posi[0] , posi[1] , posi[2] )
                    mc.setAttr( '{}.t'.format(jntIk) , posi[0] , posi[1] , posi[2] )
                    mc.setAttr( '{}.t'.format(jntIkUpvec) , posiUpvec[0] , posiUpvec[1] , posiUpvec[2] )
                    self.jntIkPosDict.append(jntIkPos)
                    self.jntIkDict.append(jntIk)
                    self.jntIkUpvecDict.append(jntIkUpvec)
                
                ep += 1
            num += 1
    
        #-- Orient Joint Ik
        length = ((numCtrl-1)*3)+(numCtrl-1)
        for i in range(0,length) :
            if not i == length :
                rt.orientJnt( self.jntIkPosDict[i] , self.jntIkPosDict[i+1] , self.jntIkUpvecDict[i] , aimVec = self.aimVec , upVec = self.upVec )
            
            self.jntIkPosDict[-1].snapOrient( self.jntIkPosDict[-2] )
        
        for i in range(0,((numCtrl-1)*3)+(numCtrl-1)+1) :
            self.jntIkDict[i].snapJntOrient( self.jntIkPosDict[i] )
            self.jntIkDict[i].parent( self.jntIkPosDict[i] )
        
        mc.delete(self.jntIkUpvecDict)
        
        #-- Orient Joint Fk
        for each in self.jntFkDict :
            nameSplit = each.name.split('Fk')
            mc.delete(mc.parentConstraint( '{}Ik{}'.format(nameSplit[0] , nameSplit[-1]) , each , mo = False ))
    
        
        self.jntFkDict[0].freeze()
        #-- Joint Hierarchy Ik
        lenArrayIkDict = ((numCtrl-1)*3)+(numCtrl-1)
        
        while lenArrayIkDict > 0 :
            self.jntIkPosDict[lenArrayIkDict].parent( self.jntIkPosDict[lenArrayIkDict-1] )
            
            lenArrayIkDict -= 1
        
        self.jntIkPosDict[0].freeze()
    
        #-- Create Controls
        self.mainCtrl = rt.createCtrl( '{}{}{}Ctrl'.format(name , elem , side) , 'stick' , 'green' , jnt = False )
        self.mainZro = rt.addGrp( self.mainCtrl )

        #-- Adjust Shape Controls
        self.mainCtrl.scaleShape( size )
        
        self.mainCtrl.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        self.mainCtrl.addAttr( 'autoStretch' , 0 ,  1 )
        self.mainCtrl.addAttr( 'autoSquash' , 0 , 1 )
        self.mainCtrl.addAttr( 'ampSquash' )
        self.mainCtrl.addAttr( 'adjustScale' )
        self.mainCtrl.attr( 'autoStretch' ).v = 1
        self.mainCtrl.attr( 'ampSquash' ).v = 1
        
        mc.parentConstraint( self.jntFkDict[0] , self.mainZro , mo = False )
        self.mainZro.parent( self.tailCtrlGrp )
    
        #-- Fk Ik Controls
        self.fkGrpArray = []
        self.fkCtrlArray = []
        self.fkGmblArray = []
        self.fkZroArray = []
        self.fkOfstArray = []
        
        self.ikGrpArray = []
        self.ikCtrlArray = []
        self.ikGmblArray = []
        self.ikZroArray = []
        self.ikOfstArray = []
        
        self.ampSqshArray = []
    
        for i in range(1,numCtrl+1) :
            # fkCtrl = rt.createCtrl( '{}Fk{}{}{}Ctrl'.format(name , elem , str(i) , side) , 'circle' , 'red' , jnt = False )
            # fkGmbl = rt.addGimbal( fkCtrl.name )
            # fkZro = rt.addGrp( fkCtrl )
            # fkOfst = rt.addGrp( fkCtrl , 'Ofst' )
            # fkPars = rt.addGrp( fkCtrl , 'Pars' )
            tmp_fkCtrl = fkRig.Run(name = '{}Fk{}{}'.format(name,elem,str(i)),
                                    side = self.side,
                                    tmpJnt = self.jntFkDict[i-1],
                                    shape = 'circle',
                                    color = 'red',
                                    jnt_ctrl=0,
                                    localWorld = 0,
                                    constraint = 0,
                                    )
            fkCtrl = tmp_fkCtrl.ctrl
            fkGmbl = tmp_fkCtrl.gmbl
            fkGrp = tmp_fkCtrl.ctrlGrp
            fkZro = tmp_fkCtrl.zro
            fkOfst = tmp_fkCtrl.ofst
            fkPars = tmp_fkCtrl.pars

            tmp_ikCtrl = fkRig.Run(name = '{}Ik{}{}'.format(name,elem,str(i)),
                                    side = self.side,
                                    tmpJnt = self.jntFkDict[i-1],
                                    shape = 'square',
                                    color = 'blue',
                                    jnt_ctrl=0,
                                    constraint = 0,
                                    localWorld = 0,
                                    )
            ikCtrl = tmp_ikCtrl.ctrl
            ikGmbl = tmp_ikCtrl.gmbl
            ikGrp = tmp_ikCtrl.ctrlGrp
            ikZro = tmp_ikCtrl.zro
            ikOfst = tmp_ikCtrl.ofst
            ikPars = tmp_ikCtrl.pars       
            # ikCtrl = rt.createCtrl( '{}Ik{}{}{}Ctrl'.format(name , elem , str(i) , side) , 'square' , 'blue' , jnt = False )
            # ikGmbl = rt.addGimbal( ikCtrl.name )
            # ikZro = rt.addGrp( ikCtrl )
            # ikOfst = rt.addGrp( ikCtrl , 'Ofst' )
            # ikPars = rt.addGrp( ikCtrl , 'Pars' )
            
            for ctrl in ( fkCtrl , fkGmbl ) :
                ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )
            
            for ctrl in ( ikCtrl , ikGmbl ) :
                ctrl.lockHideAttrs( 'rx' , 'ry' , 'rz' ,'sx' , 'sy' , 'sz' , 'v' )
            
            ikCtrl.addAttr( 'squash' )
            ampSqshMdv = cn.createNode( 'multiplyDivide' , '{}Ik{}{}AmpSqsh{}Mdv'.format(name , elem , str(i) , side))
            
            ikCtrl.attr('squash') >> ampSqshMdv.attr('i1x')
            self.mainCtrl.attr('ampSquash') >> ampSqshMdv.attr('i2x')
            
            #fkZro.snap( self.jntFkDict[i-1] )
            ikZro.snap( self.jntFkDict[i-1] )
            
            self.fkCtrlArray.append(fkCtrl)
            self.fkGmblArray.append(fkGmbl)
            self.fkGrpArray.append(fkGrp)
            self.fkZroArray.append(fkZro)
            self.fkOfstArray.append(fkOfst)
            
            self.ikCtrlArray.append(ikCtrl)
            self.ikGmblArray.append(ikGmbl)
            self.ikGrpArray.append(ikGrp)
            self.ikZroArray.append(ikZro)
            self.ikOfstArray.append(ikOfst)
            
            self.ampSqshArray.append(ampSqshMdv)
            
        for fkCtrl in self.fkCtrlArray :
            fkCtrl.scaleShape( size )
            
        for fkGmbl in self.fkGmblArray :
            fkGmbl.scaleShape( size )
            
        for ikCtrl in self.ikCtrlArray :
            ikCtrl.scaleShape( size )
            
        for ikGmbl in self.ikGmblArray :
            ikGmbl.scaleShape( size )
         
        #-- Adjust Hierarchy
        length = numCtrl-1
        
        while length >= 0 :
            self.ikGrpArray[length].parent( self.fkGmblArray[length] )
            
            if not length == 0 :
                self.fkGrpArray[length].parent( self.fkGmblArray[length-1] )
                
            length -= 1
        
        self.fkGrpArray[0].parent( self.tailCtrlGrp )
        
        #-- Rig Process Fk
        rt.localWorld( self.fkCtrlArray[0] , ctrlGrp , self.tailCtrlGrp , self.fkZroArray[0] , 'orient' )

        #-- Rig Process Ik
        for i in range( 0 ,numCtrl ) :
            mc.pointConstraint( self.ikGmblArray[i] , self.jntFkDict[i] , mo = True )
            
        cuvIkSpine = mc.rename( mc.duplicate( curveTmp )[0] , '{}Ik{}{}Cuv'.format( name , elem , side ) )
        self.cuvIkSpine = core.Dag( cuvIkSpine )
        mc.rebuildCurve( self.cuvIkSpine , rt = 0 , s = numCtrl-1)
        
        self.ikhSpineHandle = mc.ikHandle( n = '{}Ik{}{}Ikh'.format( name , elem , side ) , sol = 'ikSplineSolver' , sj = self.jntIkPosDict[0] , ee = self.jntIkPosDict[-1] , scv = False , ccv = False , c = self.cuvIkSpine )
        self.spineSkc = mc.skinCluster( self.jntFkDict , self.cuvIkSpine , dr = 7 , mi = 2 , n = '{}{}{}Skc'.format( name , elem , side ))[0]
        
        #-- SkinCluster
        loop = 0
        for i in range(1,numCtrl+1) :
            mc.skinPercent( self.spineSkc , '{}.cv[{}]'.format(self.cuvIkSpine,i) , tv = [ self.jntFkDict[loop] , 1 ])
            loop += 1
        
        mc.skinPercent( self.spineSkc , '{}.cv[0]'.format(self.cuvIkSpine) , tv = [ self.jntFkDict[0] , 1 ])
        mc.skinPercent( self.spineSkc , '{}.cv[{}]'.format(self.cuvIkSpine,numCtrl+1) , tv = [ self.jntFkDict[-1] , 1 ])
        mc.skinPercent( self.spineSkc , '{}.cv[{}]'.format(self.cuvIkSpine,numCtrl) , tv = [( self.jntFkDict[-1] , 0.65 ),( self.jntFkDict[-2] , 0.35 )] )
        #Set Skin on CV[1]
        mc.skinPercent( self.spineSkc , '{}.cv[1]'.format(self.cuvIkSpine) , tv = [ (self.jntFkDict[0] , 0.667 ) , (self.jntFkDict[1] , 0.333)])

        deformerSet = self.cuvIkSpine.getDef( 'tweak' )
        
        #-- Auto Stretch
        self.cuvInfo = cn.createNode( 'curveInfo' ,'{}{}{}Cif'.format( name , elem , side ))
        self.autoStrtBcl = cn.createNode( 'blendColors' , '{}{}AutoStrt{}Bcl'.format( name , elem , side ))
        self.autoStrtMdv = cn.createNode( 'multiplyDivide' , '{}{}AutoStrt{}Mdv'.format( name , elem , side ))
        self.gblSclMdv = cn.createNode( 'multiplyDivide' , '{}{}GblScl{}Mdv'.format( name , elem , side ))
        
        mc.setAttr( '{}.operation'.format(self.autoStrtMdv) , 2 )
        
        self.mainCtrl.attr('autoStretch') >> self.autoStrtBcl.attr('b')
        self.cuvIkSpine.attr('worldSpace[0]') >> self.cuvInfo.attr('inputCurve')
        self.cuvInfo.attr('arcLength') >> self.autoStrtBcl.attr('color1R')
        self.autoStrtBcl.attr('outputR') >> self.autoStrtMdv.attr('i1x')

        
        mc.connectAttr( 'AllMover_Ctrl.sy' , '{}.i1x'.format(self.gblSclMdv) )
        self.gblSclMdv.attr('ox') >> self.autoStrtBcl.attr('color2R')
        self.gblSclMdv.attr('ox') >> self.autoStrtMdv.attr('i2x')
        self.gblSclMdv.attr('i2x').value = self.cuvInfo.attr('arcLength').value

        for jnt in self.jntIkPosDict :
            mc.connectAttr( '{}.ox'.format(self.autoStrtMdv) , '{}.s{}'.format(jnt,axis) )
         
        #-- Squash
        self.sqshMdvArray = []
        
        for ikCtrl in self.ikCtrlArray :
            n = ikCtrl.getName()
            sqshMdv = cn.createNode( 'multiplyDivide' , '{}Sqsh{}Mdv'.format( n , side ))
            
            ampSqshMdv = core.Dag('{}AmpSqsh{}Mdv'.format( n , side ))
            
            for attr in ( 'i1x' , 'i1y' , 'i1z' ) :
                ampSqshMdv.attr('ox') >> sqshMdv.attr('{}'.format(attr) )
            
            sqshMdv.attr('i2x').value = 0.75
            sqshMdv.attr('i2y').value = 0.5
            sqshMdv.attr('i2z').value = 0.25
            
            self.sqshMdvArray.append(sqshMdv)
        
        if 'x' in axis :
            s1 = 'y'
            s2 = 'z'
        elif 'y' in axis :
            s1 = 'x'
            s2 = 'z'
        elif 'z' in axis :
            s1 = 'x'
            s2 = 'y'
            
        for jnt in self.jntIkDict :
            n = jnt.getName()
            
            sqshPma = cn.createNode( 'plusMinusAverage' , '{}Sqsh{}Pma'.format( n , side ))
            sqshPma.addAttr( 'constant' )
            sqshPma.attr('constant').value = 1

            sqshPma.attr('constant') >> sqshPma.attr('i1[0]')
            sqshPma.attr('o1') >> jnt.attr('s{}'.format(s1))
            sqshPma.attr('o1') >> jnt.attr('s{}'.format(s2))

        for i in range(1,len(self.sqshMdvArray)+1) :
            mc.connectAttr( '{}.ox'.format(self.ampSqshArray[i-1]) , '{}Ik{}{}Sqsh{}Pma.i1[1]'.format(name , elem , i , side))
            mc.connectAttr( '{}.adjustScale'.format(self.mainCtrl) , '{}Ik{}{}Sqsh{}Pma.i1[2]'.format(name , elem , i , side))
            
            if not i == len(self.sqshMdvArray) :
                mc.connectAttr( '{}.ox'.format(self.sqshMdvArray[i-1]) , '{}Ik{}{}ASqsh{}Pma.i1[1]'.format(name , elem , i , side))
                mc.connectAttr( '{}.oy'.format(self.sqshMdvArray[i-1]) , '{}Ik{}{}BSqsh{}Pma.i1[1]'.format(name , elem , i , side))
                mc.connectAttr( '{}.oz'.format(self.sqshMdvArray[i-1]) , '{}Ik{}{}CSqsh{}Pma.i1[1]'.format(name , elem , i , side))
                
                mc.connectAttr( '{}.ox'.format(self.sqshMdvArray[i]) , '{}Ik{}{}CSqsh{}Pma.i1[2]'.format(name , elem , i , side))
                mc.connectAttr( '{}.oy'.format(self.sqshMdvArray[i]) , '{}Ik{}{}BSqsh{}Pma.i1[2]'.format(name , elem , i , side))
                mc.connectAttr( '{}.oz'.format(self.sqshMdvArray[i]) , '{}Ik{}{}ASqsh{}Pma.i1[2]'.format(name , elem , i , side))

                mc.connectAttr( '{}.adjustScale'.format(self.mainCtrl) , '{}Ik{}{}ASqsh{}Pma.i1[3]'.format(name , elem , i , side))
                mc.connectAttr( '{}.adjustScale'.format(self.mainCtrl) , '{}Ik{}{}BSqsh{}Pma.i1[3]'.format(name , elem , i , side))
                mc.connectAttr( '{}.adjustScale'.format(self.mainCtrl) , '{}Ik{}{}CSqsh{}Pma.i1[3]'.format(name , elem , i , side))
        
        #-- Twist
        #-- CreateNode Pma
        for jnt in self.jntIkDict :
            n = jnt.getName()
            
            self.twistPma = cn.createNode( 'plusMinusAverage' , '{}Twist{}Pma'.format( n , side ))
            
            self.twistPma.attr('o1') >> jnt.attr('r{}'.format(axis))
        
        #-- CreateNode Mdv
        self.twistMdvArray = []
        
        for ikCtrl in self.ikCtrlArray :
            n = ikCtrl.getName()
            twistMdv = cn.createNode( 'multiplyDivide' , '{}Twist{}Mdv'.format( n , side ))
            
            for attr in ( 'i1x' , 'i1y' , 'i1z' ) :
                mc.connectAttr( '{}{}Jnt.r{}'.format( n , side , axis ) , '{}.{}'.format( twistMdv , attr ))
            
            twistMdv.attr('i2x').value = 0.75
            twistMdv.attr('i2y').value = 0.5
            twistMdv.attr('i2z').value = 0.25
            
            self.twistMdvArray.append(twistMdv)
        
        #-- Connecting Node
        i = 1
        while i <= numCtrl :
            k = 0
            for j in range(0,i) :
                
                for ctrlFk in ( self.fkCtrlArray[j] , self.fkGmblArray[j] ) :
                    mc.connectAttr( '{}.r{}'.format(ctrlFk , axis) , '{}Ik{}{}Twist{}Pma.i1[{}]'.format(name , elem , i , side , k))
                    k += 1
                
                if j == i-1 :
                    for ctrlIk in ( self.ikCtrlArray[j] , self.ikGmblArray[j] ) :
                        mc.connectAttr( '{}.r{}'.format(ctrlIk , axis) , '{}Ik{}{}Twist{}Pma.i1[{}]'.format(name , elem , i , side , k))
                        k += 1
            i += 1
        
        for i in range(1,len(self.twistMdvArray)+1) :
            
            if not i == len(self.twistMdvArray) :
                mc.connectAttr( '{}.ox'.format(self.twistMdvArray[i-1]) , '{}Ik{}{}ATwist{}Pma.i1[1]'.format(name , elem , i , side))
                mc.connectAttr( '{}.oy'.format(self.twistMdvArray[i-1]) , '{}Ik{}{}BTwist{}Pma.i1[1]'.format(name , elem , i , side))
                mc.connectAttr( '{}.oz'.format(self.twistMdvArray[i-1]) , '{}Ik{}{}CTwist{}Pma.i1[1]'.format(name , elem , i , side))
                
                mc.connectAttr( '{}.ox'.format(self.twistMdvArray[i]) , '{}Ik{}{}CTwist{}Pma.i1[2]'.format(name , elem , i , side))
                mc.connectAttr( '{}.oy'.format(self.twistMdvArray[i]) , '{}Ik{}{}BTwist{}Pma.i1[2]'.format(name , elem , i , side))
                mc.connectAttr( '{}.oz'.format(self.twistMdvArray[i]) , '{}Ik{}{}ATwist{}Pma.i1[2]'.format(name , elem , i , side))
        
        #-- CleanUp
        #-- Joint Hierarchy Fk
        lenArrayFkDict = numCtrl-1
        
        while lenArrayFkDict > 0 :
            self.jntFkDict[lenArrayFkDict].parent( self.jntFkDict[lenArrayFkDict-1] )
            
            lenArrayFkDict -= 1

        mc.delete( cuvPosi , cuvUpvec )
        
        mc.parent( self.jntFkDict[0] , self.tailJntGrp )
        mc.parent( self.ikhSpineHandle[0] , self.tailIkhGrp )
        mc.parent( self.cuvIkSpine , self.tailStillGrp )
        
        if parent :
            mc.parent( self.jntIkPosDict[0] , parent )
        elif skinGrp :
            mc.parent( self.jntIkPosDict[0] , skinGrp )

# -------------------------------------------------------------------------------------------------------------
#
#  ADD MORE FUNCTION CONTROL AND TOUCHING
#  
# -------------------------------------------------------------------------------------------------------------

class AddMore( Run ):

    def __init__( self , name          = 'Tail' ,
                         numCtrl       = 8 ,
                         curveTmp      = 'Tail_CuvTemp' ,
                         curveUpVecTmp = 'TailUpvec_CuvTemp' ,
                         parent        = 'Pelvis_Jnt' ,
                         ctrlGrp       = 'Ctrl_Grp' ,
                         jntGrp        = 'Jnt_Grp' ,
                         skinGrp       = 'Skin_Grp' ,
                         stillGrp      = 'Still_Grp' ,
                         ikhGrp        = 'Ikh_Grp' ,
                         elem          = '' ,
                         side          = '' ,
                         axis          = 'y' ,
                         size          = 1
                 ):

        super( AddMore , self ).__init__( name , numCtrl , curveTmp , curveUpVecTmp , parent , ctrlGrp , jntGrp , skinGrp , stillGrp , ikhGrp , elem , side , axis , size )
        
        #-- Info
        if side == '' :
            side = '_'
        else :
            side = '_{}_'.format(side)

        dtlJnt = ((numCtrl-1)*3)+numCtrl
        divCtrl = numCtrl-2
        avg = 10.0/divCtrl

        #-- Create Controls
        self.curlCtrlObj = ctrlRig.Run( name = '{}Curl{}'.format(name , elem),
                                    side = self.side,
                                    tmpJnt = '',
                                    shape = 'arrowCircle',
                                    color = 'yellow',
                                    jnt_ctrl=0,
                                    constraint = 0,
                                    )
        self.curlCtrl = self.curlCtrlObj.ctrl
        self.curlZro = self.curlCtrlObj.zro
        self.curlOfst = self.curlCtrlObj.ofst
        #self.curlCtrlGrp = self.curlCtrl.ctrlGrp

        self.curlCtrl.lockHideAttrs( 'ty' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        self.curlZro.parent( self.tailCtrlGrp )
        self.curlZro.attr( 'inheritsTransform' ).value = 0
        mc.scaleConstraint( ctrlGrp , self.curlZro , mo = True )

        #-- Adjust Shape Controls
        self.curlCtrl.scaleShape( size )

        #-- Create Nurb Surface
        loftArray = [ ]

        for i in range(dtlJnt) :
            loftCuv = rt.createCtrl( 'Loft{}_Cuv'.format(i) , 'line' , 0 )
            loftCuv.snap( self.jntIkDict[i] )
            loftCuv.scaleShape( size )
            loftArray.append( loftCuv )

        self.nrb = core.Dag(mc.loft( loftArray , ch = True , rn = True , ar = True , n = '{}{}Curl{}Nrb'.format( name , elem , side ))[0])
        self.nrbShape = core.Dag(self.nrb.shape)
        mc.delete( self.nrb , ch = True )
        mc.delete( loftArray )
        self.nrb.parent( stillGrp )

        #-- Skin Nurb Surface
        nrbSkc = mc.skinCluster( self.jntIkDict , self.nrb , dr = 7 , mi = 2 , n = '{}{}Curl{}Skc'.format( name , elem , side ))[0]
        
        for i in range(dtlJnt) :
            mc.skinPercent( nrbSkc , '{}.cv[{}][0:1]'.format(self.nrb,i+1) , tv = [ self.jntIkDict[i] , 1 ] )

        mc.skinPercent( nrbSkc , '{}.cv[0][0:1]'.format(self.nrb) , tv = [ self.jntIkDict[0] , 1 ] )
        mc.skinPercent( nrbSkc , '{}.cv[{}][0:1]'.format(self.nrb,dtlJnt+1) , tv = [ self.jntIkDict[dtlJnt-1] , 1 ] )

        #-- Create Node
        self.curlCtrl.addAttr( 'adjustScale' )
        self.curlCtrl.addAttr( 'amplitude' )
        self.curlCtrl.addAttr( 'slidePosition' , 0 , 10 )
        self.curlCtrl.addAttr( 'autoCurl' , 0 , 10 )
        self.curlCtrl.addAttr( 'slideCurl' , 0 , 10 )
        self.curlCtrl.attr( 'amplitude' ).value = 1
        self.curlCtrl.addTitleAttr( 'extraCtrl' )
        self.curlCtrl.addAttr( 'control1' , 0 , 1 )
        self.curlCtrl.addAttr( 'control2' , 0 , 1 )
        self.curlCtrl.addAttr( 'control3' , 0 , 1 )
        self.curlCtrl.addAttr( 'control4' , 0 , 1 )
        
        self.curlCtrl.attr('adjustScale') >> self.mainCtrl.attr('adjustScale')

        self.posi = cn.createNode( 'pointOnSurfaceInfo' , '{}{}Curl{}Posi'.format( name , elem , side ))
        self.posi.attr('turnOnPercentage').value = 1
        self.posi.attr('parameterV').value = 0.5

        self.nrb.attr('worldSpace[0]') >> self.posi.attr('inputSurface')
        self.posi.attr('position') >> self.curlZro.attr('t')
        self.mdvAmpPosi = cn.createNode( 'multiplyDivide' , '{}{}AmpPosi{}Mdv'.format( name , elem , side ))
        self.mdvAmpPosi.attr('i2x').value = 0.1
        self.curlCtrl.attr('slidePosition') >> self.mdvAmpPosi.attr('i1x')
        self.mdvAmpPosi.attr('ox') >> self.posi.attr('parameterU')
        
        self.nrbAim = cn.createNode( 'aimConstraint' , '{}{}CurlCtrl{}aimConstraint'.format( name , elem , side ))
        
        self.nrbAim.attr('ax').value = -1
        self.nrbAim.attr('ay').value = 0
        self.nrbAim.attr('az').value = 0
        self.nrbAim.attr('ux').value = 0
        self.nrbAim.attr('uy').value = 0
        self.nrbAim.attr('uz').value = 1

        self.posi.attr('n') >> self.nrbAim.attr('wu')
        self.posi.attr('tv') >> self.nrbAim.attr('tg[0].tt')
        self.nrbAim.attr('cr') >> self.curlZro.attr('r')
    
        mc.parent( self.nrbAim , self.curlZro )
        
        mdvRev = cn.createNode( 'multiplyDivide' , '{}{}Rev{}Mdv'.format( name , elem , side ))
        self.curlCtrl.attr('t') >> mdvRev.attr('i1')
        mdvRev.attr('o') >> self.curlOfst.attr('t')
        mdvRev.attr('i2x').value = -1
        mdvRev.attr('i2y').value = -1
        mdvRev.attr('i2z').value = -1

        mdvAuto = cn.createNode( 'multiplyDivide' , '{}{}Auto{}Mdv'.format( name , elem , side ))
        revAuto = cn.createNode( 'reverse' , '{}{}CurlAuto{}Rev'.format( name , elem , side ))
        mdvCurlAuto = cn.createNode( 'multiplyDivide' , '{}{}CurlAuto{}Mdv'.format( name , elem , side ))
        bclAuto = cn.createNode( 'blendColors' , '{}{}CurlAuto{}Bcl'.format( name , elem , side ))
        remAuto = cn.createNode( 'remapValue' , '{}{}CurlAuto{}Rem'.format( name , elem , side ))
        mdvAmpRev = cn.createNode( 'multiplyDivide' , '{}{}CurlAmpRev{}Mdv'.format( name , elem , side ))
        mdvAmpRev.attr('i2x').value = -1
        remAuto.attr('inputMax').value = 10

        self.curlCtrl.attr('slidePosition') >> mdvAuto.attr('i1x')
        mdvAuto.attr('i2x').value = 0.1
        mdvAuto.attr('ox') >> revAuto.attr('ix')
        revAuto.attr('ox') >> mdvCurlAuto.attr('i1x')
        mdvCurlAuto.attr('i2x').value = 10
        mdvCurlAuto.attr('ox') >> bclAuto.attr('c1r')
        self.curlCtrl.attr('autoCurl') >> remAuto.attr('inputValue')
        remAuto.attr('outValue') >> bclAuto.attr('blender')
        self.curlCtrl.attr('amplitude') >> mdvAmpRev.attr('i1x')

        for extraCtrl in ('control1','control2','control3','control4') :
            ctrl = rt.createCtrl( '{}{}{}Ctrl'.format(extraCtrl , elem , side) , 'cube' , 'yellow' , jnt = False )
            zro = rt.addGrp( ctrl )
            ofst = rt.addGrp( ctrl  , 'Ofst' )
            zro.parent( self.tailCtrlGrp )
            zro.attr( 'inheritsTransform' ).value = 0
            mc.scaleConstraint( ctrlGrp , ctrl , mo = True )
            ctrl.scaleShape( size )
            ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )
            self.curlCtrl.attr(extraCtrl) >> zro.attr('v')


            ctrl.addAttr( 'slidePosition' , 0 , 10 )

            posi = cn.createNode( 'pointOnSurfaceInfo' , '{}{}{}Posi'.format( extraCtrl , elem , side ))
            posi.attr('turnOnPercentage').value = 1
            posi.attr('parameterV').value = 0.5

            self.nrb.attr('worldSpace[0]') >> posi.attr('inputSurface')
            posi.attr('position') >> zro.attr('t')
            mdvAmpPosi = cn.createNode( 'multiplyDivide' , '{}{}AmpPosi{}Mdv'.format( extraCtrl , elem , side ))
            mdvAmpPosi.attr('i2x').value = 0.1
            ctrl.attr('slidePosition') >> mdvAmpPosi.attr('i1x')
            mdvAmpPosi.attr('ox') >> posi.attr('parameterU')
            
            aim = cn.createNode( 'aimConstraint' , '{}{}Ctrl{}aimConstraint'.format( extraCtrl , elem , side ))
            
            aim.attr('ax').value = -1
            aim.attr('ay').value = 0
            aim.attr('az').value = 0
            aim.attr('ux').value = 0
            aim.attr('uy').value = 0
            aim.attr('uz').value = 1

            posi.attr('n') >> aim.attr('wu')
            posi.attr('tv') >> aim.attr('tg[0].tt')
            aim.attr('cr') >> zro.attr('r')
        
            mc.parent( aim , zro )

        for i in range(numCtrl) :
            rem = cn.createNode( 'remapValue' , '{}{}Curl{}{}Rem'.format( name , elem , i+1 , side ))
            bcl = cn.createNode( 'blendColors' , '{}{}Curl{}{}Bcl'.format( name , elem , i+1 , side ))
            mdv = cn.createNode( 'multiplyDivide' , '{}{}CurlAmp{}{}Mdv'.format( name , elem , i+1 , side ))
            pma = cn.createNode( 'plusMinusAverage' , '{}{}Curl{}{}Pma'.format( name , elem , i+1 , side ))
            
            self.curlCtrl.attr('slideCurl') >> pma.attr('i1[0]')
            bclAuto.attr('outputR') >> pma.attr('i1[1]')
            pma.attr('o1') >> rem.attr('inputValue')
            self.curlCtrl.attr('tx') >> bcl.attr('c2b')
            self.curlCtrl.attr('tz') >> bcl.attr('c2r')
            rem.attr('outValue') >> bcl.attr('blender')
            bcl.attr('output') >> mdv.attr('i1')
            bcl.attr('c1r').value = 0
            bcl.attr('c1g').value = 0
            bcl.attr('c1b').value = 0
            mdv.attr('output') >> self.fkOfstArray[i].attr('r')

            if not i == numCtrl-1 :
                self.curlCtrl.attr('amplitude') >> mdv.attr('i2x')
                mdvAmpRev.attr('ox') >> mdv.attr('i2z')
                minValue = (((avg*(divCtrl-i)) + (avg*(divCtrl-(i+1))))/2)
                maxValue = avg*(divCtrl-(i-1))

                if minValue < 0 :
                    minValue = 0

                rem.attr('inputMin').value = minValue
                rem.attr('inputMax').value = maxValue
            else :
                mdv.attr('i2x').value = 0
                mdv.attr('i2y').value = 0
                mdv.attr('i2z').value = 0

        mc.select( cl = True )