def body_stretch(ctrl='Spine_Ctrl'):
    # add attr
    if not mc.objExists('{}.bodyStretch'.format(ctrl)):
        mc.addAttr(ctrl ,ln = 'bodyStretch',dv=0,k=0)
    
    # create stretch world group
    chestFkWorld = mc.createNode('transform',n = 'ChestFkWorld_Grp')
    chestFkWorldZro = mc.createNode('transform',n = 'ChesFkWorldZro_Grp')
    spineFkWorld = mc.createNode('transform',n = 'SpineFkWorld_Grp')
    spineFkWorldZro = mc.createNode('transform',n = 'SpineFkWorldZro_Grp')
    
    chestIkWorld = mc.createNode('transform',n = 'ChestIkWorld_Grp')
    chestIkWorldZro = mc.createNode('transform',n = 'ChesIkWorldZro_Grp')
    
    mc.parent(chestFkWorld,chestFkWorldZro)
    mc.parent(spineFkWorld,spineFkWorldZro)
    mc.parent(chestIkWorld,chestIkWorldZro)
    
    mc.delete(mc.pointConstraint('ChestFk_Ctrl', chestFkWorldZro,mo=0))
    mc.delete(mc.pointConstraint('SpineMidFk_Ctrl', spineFkWorldZro,mo=0))
    mc.delete(mc.pointConstraint('ChestIk_Ctrl', chestIkWorldZro,mo=0))

    mc.parent(chestFkWorldZro,'ChestFkCtrlZro_Grp')
    mc.parent(spineFkWorldZro,'SpineFkCtrlZro_Grp')
    mc.parent(chestIkWorldZro,'ChestIkCtrlZro_Grp')

    mc.parent('ChestFkCtrlOfst_Grp', chestFkWorld)
    mc.parent('SpineMidFkCtrlOfst_Grp', spineFkWorld)
    mc.parent('ChestIkCtrlOfst_Grp', chestIkWorld)

    # create utility node to connect stretch
    bodyMdv = mc.createNode('multiplyDivide',n= 'BodyStrt_Mdv')
    bodyHalfMdv = mc.createNode('multiplyDivide',n = 'BodyHalfStrt_mdv')

    bodyPma = mc.createNode('plusMinusAverage',n = 'BodyStretch_Pma')
    bodyHalfPma = mc.createNode('plusMinusAverage',n = 'BodyHalfStretch_Pma')

    mc.setAttr(bodyMdv+'.input2X',.1)
    mc.setAttr(bodyHalfMdv+'.input2X',.5)
    mc.connectAttr(ctrl+'.bodyStretch',bodyMdv+'.input1X')
    mc.connectAttr(bodyMdv+'.outputX',bodyHalfMdv+'.input1X')
    mc.connectAttr(bodyMdv+'.outputX', bodyPma+'.input1D[0]')
    mc.connectAttr(bodyHalfMdv+'.outputX',bodyHalfPma+'.input1D[0]')
    mc.connectAttr(bodyHalfPma+'.output1D',chestFkWorld+'.ty')
    mc.connectAttr(bodyHalfPma+'.output1D',spineFkWorld+'.ty')
    mc.connectAttr(bodyPma+'.output1D',chestIkWorld+'.ty')

def bodyFk_stretch(ctrl='Spine_Ctrl'):
    # add attr
    if not mc.objExists('{}.bodyStretch'.format(ctrl)):
        mc.addAttr(ctrl ,ln = 'bodyStretch',dv=0,k=0)

    # create utility node to connect stretch
    bodyMdv = mc.createNode('multiplyDivide',n= 'BodyStrt_Mdv')

    bodyPma = mc.createNode('plusMinusAverage',n = 'BodyStretch_Pma')

    mc.setAttr(bodyMdv+'.input2X',.1)
    mc.connectAttr(ctrl+'.bodyStretch',bodyMdv+'.input1X')
    mc.connectAttr(bodyMdv+'.outputX', bodyPma+'.input1D[0]')
    for i in range(3):
        mc.connectAttr(bodyPma+'.output1D','Spine{}Strt_Pma.input1D[2]'.format(i+1))




def arm_stretch(upArmCtrl = '',foreArmCtrl = ''):
    if not mc.objExists('{}.upperArm'.format(upArmCtrl)):
        mc.addAttr(upArmCtrl, ln='upperArm',dv=0,k=0)
    if not mc.objExists('{}.lowerArm'.format(foreArmCtrl)):    
        mc.addAttr(foreArmCtrl, ln ='lowerArm',dv=0 ,k =1)

    # fk stretch
    upperArmMdv = mc.createNode('multiplyDivide',n='upperArmStrt_Mdv')
    lowerArmMdv = mc.createNode('multiplyDivide',n='lowerArmStrt_Mdv')
    upperArmInvMdv = mc.createNode('multiplyDivide',n='upperArmStrtInv_Mdv')
    lowerArmInvMdv = mc.createNode('multiplyDivide',n='lowerArmStrtInv_Mdv')
    mc.connectAttr(upArmCtrl+'.upperArm', upperArmMdv+'.input1X')
    mc.connectAttr(foreArmCtrl+'.lowerArm', lowerArmMdv+'.input1X')
    mc.connectAttr(upArmCtrl+'.upperArm', upperArmInvMdv+'.input1X')
    mc.connectAttr(foreArmCtrl+'.lowerArm', lowerArmInvMdv+'.input1X')
    mc.setAttr(upperArmMdv +'.input2X',.1)
    mc.setAttr(lowerArmMdv +'.input2X',.1)
    mc.setAttr(upperArmInvMdv +'.input2X',-.1)
    mc.setAttr(lowerArmInvMdv +'.input2X',-.1)
    mc.connectAttr(upperArmMdv+'.outputX','UpArmFkStrt_L_Pma.input1D[2]')
    mc.connectAttr(lowerArmMdv+'.outputX','ForearmFkStrt_L_Pma.input1D[2]')
    mc.connectAttr(upperArmInvMdv+'.outputX','UpArmFkStrt_R_Pma.input1D[2]')
    mc.connectAttr(lowerArmInvMdv+'.outputX','ForearmFkStrt_R_Pma.input1D[2]')

    # ik stretch
    bodyWorldTrans = mc.createNode('transform',n='bodyStrtWorldPosi_Grp')
    bodyWorldTransZro = mc.createNode('transform',n='bodyStrtWorldPosiZro_Grp')
    mc.parent(bodyWorldTrans,bodyWorldTransZro)

    # left Ik stretch
    armtAutoStrt_bcl = 'ArmIkAutoStrt_L_Bcl'
    armStrtDiv_mdv = 'ArmIkStrtDiv_L_Mdv'
    armStrt_mdv = 'ArmIkStrt_L_Mdv'
    autoStrtDiv = 'WristIk_L_CtrlShape.autoStrtDiv'
    autoStrtCon = 'WristIk_L_CtrlShape.autoStrtCon'
    armStrtCnd = 'ArmIkAutoStrt_L_Cnd'
    armStrtPma = 'ArmIkAutoStrt_L_Pma'
    upperValue = mc.getAttr(armStrt_mdv+'.input2X')
    lowerValue = mc.getAttr(armStrt_mdv+'.input2Y')

    # create utility Node
    upperValue_pma = mc.createNode('plusMinusAverage',n = 'upperArmIkStrt_L_pma') 
    lowerValue_pma = mc.createNode('plusMinusAverage',n = 'lowerArmIkStrt_L_pma')
    length_pma = mc.createNode('plusMinusAverage', n = 'lengthArmIkStrt_L_pma')
    currentLength_pma = mc.createNode('plusMinusAverage', n = 'lengthArmIkStrt_L_pma')

    mc.setAttr(upperValue_pma +'.input1D[0]',upperValue)
    mc.setAttr(lowerValue_pma +'.input1D[0]',lowerValue)
    mc.connectAttr(upperArmMdv + '.outputX',upperValue_pma+'.input1D[1]')
    mc.connectAttr(lowerArmMdv + '.outputX',lowerValue_pma+'.input1D[1]')

    # plugh new stretch with rig
    mc.connectAttr(autoStrtDiv , length_pma+'.input1D[0]')
    mc.connectAttr(upperArmMdv+'.outputX', length_pma+'.input1D[1]')
    mc.connectAttr(lowerArmMdv+'.outputX', length_pma+'.input1D[2]')

    mc.connectAttr(autoStrtCon , currentLength_pma+'.input1D[0]')
    mc.connectAttr(upperArmMdv+'.outputX', currentLength_pma+'.input1D[1]')
    mc.connectAttr(lowerArmMdv+'.outputX', currentLength_pma+'.input1D[2]')

    for ax in 'XYZ':
        mc.connectAttr(length_pma+'.output1D',armStrtDiv_mdv+'.input2{}'.format(ax),f=1)
    mc.connectAttr(currentLength_pma+'.output1D',armStrtCnd +'.secondTerm',f=1)
    mc.connectAttr(upperValue_pma+'.output1D',armStrt_mdv+ '.input2X',f=1)
    mc.connectAttr(lowerValue_pma+'.output1D',armStrt_mdv+ '.input2Y',f=1)
    # mc.connectAttr(upperValue_pma+'.output1D',armStrtPma+'.input3D[0].input3Dx')
    # mc.connectAttr(lowerValue_pma+'.output1D',armStrtPma+'.input3D[0].input3Dy')

    # create new Ik Position
    ikWorldSpace = 'WristIk_L_Ctrl'
    ikWorldSpaceCons = 'WristIkWorldSpace_L_Grp_parentConstraint1'
    ikWorldSpaceGrp = 'WristIkWorldSpace_L_Grp'

    newIkWorld = mc.createNode('transform',n='ArmIkStrtWorldPosi_L_Grp')
    newIkWorldZro = mc.createNode('transform',n='ArmIkStrtWorldPosiZro_L_Grp')
    
    mc.parent(newIkWorld, newIkWorldZro)
    mc.parent(newIkWorldZro,bodyWorldTrans)
    mc.delete(mc.parentConstraint(ikWorldSpace,newIkWorldZro,mo=0))
    mc.delete(ikWorldSpaceCons)
    mc.parentConstraint(newIkWorld,ikWorldSpaceGrp,mo=1)


    # add body Pma
    addLength_pma = mc.createNode('plusMinusAverage',n='addArmLength_L_pma')
    mc.connectAttr(upperArmMdv+'.outputX',addLength_pma+'.input1D[0]')
    mc.connectAttr(lowerArmMdv+'.outputX',addLength_pma+'.input1D[1]')
    mc.connectAttr(addLength_pma+'.output1D',newIkWorld+'.ty')


    # right Ik stretch
    armtAutoStrt_bcl = 'ArmIkAutoStrt_R_Bcl'
    armStrtDiv_mdv = 'ArmIkStrtDiv_R_Mdv'
    armStrt_mdv = 'ArmIkStrt_R_Mdv'
    autoStrtDiv = 'WristIk_R_CtrlShape.autoStrtDiv'
    autoStrtCon = 'WristIk_R_CtrlShape.autoStrtCon'
    armStrtCnd = 'ArmIkAutoStrt_R_Cnd'
    armStrtPma = 'ArmIkAutoStrt_R_Pma'
    upperValue = - mc.getAttr(armStrt_mdv+'.input2X')
    lowerValue = - mc.getAttr(armStrt_mdv+'.input2Y')

    # create utility Node
    upperValue_pma = mc.createNode('plusMinusAverage',n = 'upperArmIkStrt_R_pma') 
    lowerValue_pma = mc.createNode('plusMinusAverage',n = 'lowerArmIkStrt_R_pma')
    length_pma = mc.createNode('plusMinusAverage', n = 'lengthArmIkStrt_R_pma')
    currentLength_pma = mc.createNode('plusMinusAverage', n = 'lengthArmIkStrt_R_pma')

    mc.setAttr(upperValue_pma +'.input1D[0]',upperValue)
    mc.setAttr(lowerValue_pma +'.input1D[0]',lowerValue)
    mc.connectAttr(upperArmMdv + '.outputX',upperValue_pma+'.input1D[1]')
    mc.connectAttr(lowerArmMdv + '.outputX',lowerValue_pma+'.input1D[1]')

    upperValueInvPma_mdv = mc.createNode('multiplyDivide', n ='upperArmIkStrtInv_R_mdv')
    lowerValueInvPma_mdv = mc.createNode('multiplyDivide', n ='upperArmIkStrtInv_R_mdv')


    mc.connectAttr(upperValue_pma+'.output1D',upperValueInvPma_mdv+'.input1X')
    mc.connectAttr(lowerValue_pma+'.output1D',lowerValueInvPma_mdv+'.input1X')
    mc.setAttr(upperValueInvPma_mdv+'.input2X',-1)
    mc.setAttr(lowerValueInvPma_mdv+'.input2X',-1)

    # plugh new stretch with rig
    mc.connectAttr(autoStrtDiv , length_pma+'.input1D[0]')
    mc.connectAttr(upperArmMdv+'.outputX', length_pma+'.input1D[1]')
    mc.connectAttr(lowerArmMdv+'.outputX', length_pma+'.input1D[2]')

    mc.connectAttr(autoStrtCon , currentLength_pma+'.input1D[0]')
    mc.connectAttr(upperArmMdv+'.outputX', currentLength_pma+'.input1D[1]')
    mc.connectAttr(lowerArmMdv+'.outputX', currentLength_pma+'.input1D[2]')

    for ax in 'XYZ':
        mc.connectAttr(length_pma+'.output1D',armStrtDiv_mdv+'.input2{}'.format(ax),f=1)
    mc.connectAttr(currentLength_pma+'.output1D',armStrtCnd +'.secondTerm',f=1)
    mc.connectAttr(upperValueInvPma_mdv+'.outputX',armStrt_mdv+ '.input2X',f=1)
    mc.connectAttr(lowerValeInvPma_mdv+'.outputX',armStrt_mdv+ '.input2Y',f=1)
    # mc.connectAttr(upperValueInvPma_mdv+'.outputX'u,armStrtPma+'.input3D[0].input3Dx')
    # mc.connectAttr(lowerValueInvPma_mdv+'.outputX',armStrtPma+'.input3D[0].input3Dy')

    # create new Ik Position
    ikWorldSpace = 'WristIk_R_Ctrl'
    ikWorldSpaceGrp = 'WristIkWorldSpace_R_Grp'
    ikWorldSpaceCons = 'WristIkWorldSpace_R_Grp_parentConstraint1'

    newIkWorld = mc.createNode('transform',n='ArmIkStrtWorldPosi_R_Grp')
    newIkWorldZro = mc.createNode('transform',n='ArmIkStrtWorldPosiZro_R_Grp')
    
    mc.parent(newIkWorld, newIkWorldZro)
    mc.parent(newIkWorldZro,bodyWorldTrans)
    mc.delete(mc.parentConstraint(ikWorldSpace,newIkWorldZro,mo=0))
    mc.delete(ikWorldSpaceCons)
    mc.parentConstraint(newIkWorld,ikWorldSpaceGrp,mo=1)


    # add body Pma
    addLength_pma = mc.createNode('plusMinusAverage',n='addArmLength_R_pma')
    mc.connectAttr(upperArmMdv+'.outputX',addLength_pma+'.input1D[0]')
    mc.connectAttr(lowerArmMdv+'.outputX',addLength_pma+'.input1D[1]')
    addLengthInv_mdv = mc.createNode('multiplyDivide',n='addLengthInv_R_mdv')
    mc.connectAttr(addLength_pma+'.output1D',addLengthInv_mdv+'.input1X')
    mc.setAttr(addLengthInv_mdv+'.input2X',-1)
    mc.connectAttr(addLengthInv_mdv+'.outputX',newIkWorld+'.ty')

    if mc.objExists('bodyStrtWorldPosi_Grp'):
        bodyStrtWorld_pma = mc.createNode('plusMinusAverage', n = 'bodyStrtWorldPosi_Pma')
        mc.connectAttr(bodyStrtWorld_pma+'.output1D','bodyStrtWorldPosi_Grp.ty')
        mc.connectAttr('BodyStretch_Pma.output1D',bodyStrtWorld_pma+'.input1D[0]')
        mc.connectAttr('RootStrt_Pma.output1D',bodyStrtWorld_pma+'.input1D[1]')


def leg_stretch(upLegCtrl = '',lowLegCtrl = ''):
    if not mc.objExists('{}.upperLeg'.format(upLegCtrl)):
        mc.addAttr(upLegCtrl, ln='upperLeg',dv=0,k=0)
    if not mc.objExists('{}.lowerLeg'.format(lowLegCtrl)):
        mc.addAttr(lowLegCtrl, ln ='lowerLeg',dv=0 ,k =1)

    # fk stretch
    upperLegMdv = mc.createNode('multiplyDivide',n='upperLegStrt_Mdv')
    lowerLegMdv = mc.createNode('multiplyDivide',n='lowerLegStrt_Mdv')
    upperLegInvMdv = mc.createNode('multiplyDivide',n='upperLegStrtInv_Mdv')
    lowerLegInvMdv = mc.createNode('multiplyDivide',n='lowerLegStrtInv_Mdv')
    mc.connectAttr(upLegCtrl+'.upperLeg', upperLegMdv+'.input1X')
    mc.connectAttr(lowLegCtrl+'.lowerLeg', lowerLegMdv+'.input1X')
    mc.connectAttr(upLegCtrl+'.upperLeg', upperLegInvMdv+'.input1X')
    mc.connectAttr(lowLegCtrl+'.lowerLeg', lowerLegInvMdv+'.input1X')
    mc.setAttr(upperLegMdv +'.input2X',.1)
    mc.setAttr(lowerLegMdv +'.input2X',.1)
    mc.setAttr(upperLegInvMdv +'.input2X',-.1)
    mc.setAttr(lowerLegInvMdv +'.input2X',-.1)
    mc.connectAttr(upperLegInvMdv+'.outputX','UpLegFkStrt_R_Pma.input1D[2]')
    mc.connectAttr(upperLegInvMdv+'.outputX','LowLegFkStrt_R_Pma.input1D[2]')
    mc.connectAttr(upperLegInvMdv+'.outputX','UpLegFkStrt_L_Pma.input1D[2]')
    mc.connectAttr(lowerLegInvMdv+'.outputX','LowLegFkStrt_L_Pma.input1D[2]')

    # ik stretch 
    # left Ik stretch
    LegtAutoStrt_bcl = 'LegIkAutoStrt_R_Bcl'
    LegStrtDiv_mdv = 'LegIkStrtDiv_R_Mdv'
    LegStrt_mdv = 'LegIkStrt_R_Mdv'
    autoStrtDiv = 'AnkleIk_R_CtrlShape.autoStrtDiv'
    autoStrtCon = 'AnkleIk_R_CtrlShape.autoStrtCon'
    LegStrtCnd = 'LegIkAutoStrt_R_Cnd'
    LegStrtPma = 'LegIkAutoStrt_R_Pma'
    upperValue = - mc.getAttr(LegStrt_mdv+'.input2X')
    lowerValue = - mc.getAttr(LegStrt_mdv+'.input2Y')

    # create utility Node
    upperValue_pma = mc.createNode('plusMinusAverage',n = 'upperLegIkStrt_R_pma') 
    lowerValue_pma = mc.createNode('plusMinusAverage',n = 'lowerLegIkStrt_R_pma')
    length_pma = mc.createNode('plusMinusAverage', n = 'lengthLegIkStrt_R_pma')
    currentLength_pma = mc.createNode('plusMinusAverage', n = 'lengthLegIkStrtCur_R_pma')

    mc.setAttr(upperValue_pma +'.input1D[0]',upperValue)
    mc.setAttr(lowerValue_pma +'.input1D[0]',lowerValue)
    mc.connectAttr(upperLegMdv + '.outputX',upperValue_pma+'.input1D[1]')
    mc.connectAttr(lowerLegMdv + '.outputX',lowerValue_pma+'.input1D[1]')
    upperValueInvPma_mdv = mc.createNode('multiplyDivide', n ='upperLegIkStrt_R_mdv')
    lowerValueInvPma_mdv = mc.createNode('multiplyDivide', n ='upperLegIkStrtInv_R_mdv')


    mc.connectAttr(upperValue_pma+'.output1D',upperValueInvPma_mdv+'.input1X')
    mc.connectAttr(lowerValue_pma+'.output1D',lowerValueInvPma_mdv+'.input1X')
    mc.setAttr(upperValueInvPma_mdv+'.input2X',-1)
    mc.setAttr(lowerValueInvPma_mdv+'.input2X',-1)

    # plugh new stretch with rig
    mc.connectAttr(autoStrtDiv , length_pma+'.input1D[0]')
    mc.connectAttr(upperLegMdv+'.outputX', length_pma+'.input1D[1]')
    mc.connectAttr(lowerLegMdv+'.outputX', length_pma+'.input1D[2]')

    mc.connectAttr(autoStrtCon , currentLength_pma+'.input1D[0]')
    mc.connectAttr(upperLegMdv+'.outputX', currentLength_pma+'.input1D[1]')
    mc.connectAttr(lowerLegMdv+'.outputX', currentLength_pma+'.input1D[2]')

    for ax in 'XYZ':
        mc.connectAttr(length_pma+'.output1D',LegStrtDiv_mdv+'.input2{}'.format(ax),f=1)
    mc.connectAttr(currentLength_pma+'.output1D',LegStrtCnd +'.secondTerm',f=1)
    mc.connectAttr(upperValueInvPma_mdv+'.outputX',LegStrt_mdv+ '.input2X',f=1)
    mc.connectAttr(lowerValueInvPma_mdv+'.outputX',LegStrt_mdv+ '.input2Y',f=1)
    # mc.connectAttr(upperValueInvPma_mdv+'.outputX',LegStrtPma+'.input3D[0].input3Dx')
    # mc.connectAttr(lowerValueInvPma_mdv+'.outputX',LegStrtPma+'.input3D[0].input3Dy')



    # right Ik stretch
    LegtAutoStrt_bcl = 'LegIkAutoStrt_L_Bcl'
    LegStrtDiv_mdv = 'LegIkStrtDiv_L_Mdv'
    LegStrt_mdv = 'LegIkStrt_L_Mdv'
    autoStrtDiv = 'AnkleIk_L_CtrlShape.autoStrtDiv'
    autoStrtCon = 'AnkleIk_L_CtrlShape.autoStrtCon'
    LegStrtCnd = 'LegIkAutoStrt_L_Cnd'
    LegStrtPma = 'LegIkAutoStrt_L_Pma'
    upperValue = - mc.getAttr(LegStrt_mdv+'.input2X')
    lowerValue = - mc.getAttr(LegStrt_mdv+'.input2Y')

    # create utility Node
    upperValue_pma = mc.createNode('plusMinusAverage',n = 'upperLegIkStrt_L_pma') 
    lowerValue_pma = mc.createNode('plusMinusAverage',n = 'lowerLegIkStrt_L_pma')
    length_pma = mc.createNode('plusMinusAverage', n = 'lengthLegIkStrt_L_pma')
    currentLength_pma = mc.createNode('plusMinusAverage', n = 'lengthLegIkStrtCur_L_pma')

    mc.setAttr(upperValue_pma +'.input1D[0]',upperValue)
    mc.setAttr(lowerValue_pma +'.input1D[0]',lowerValue)
    mc.connectAttr(upperLegInvMdv + '.outputX',upperValue_pma+'.input1D[1]')
    mc.connectAttr(lowerLegInvMdv + '.outputX',lowerValue_pma+'.input1D[1]')

    upperValueInvPma_mdv = mc.createNode('multiplyDivide', n ='upperLegIkStrt_L_mdv')
    lowerValueInvPma_mdv = mc.createNode('multiplyDivide', n ='upperLegIkStrtInv_L_mdv')


    mc.connectAttr(upperValue_pma+'.output1D',upperValueInvPma_mdv+'.input1X')
    mc.connectAttr(lowerValue_pma+'.output1D',lowerValueInvPma_mdv+'.input1X')
    mc.setAttr(upperValueInvPma_mdv+'.input2X',- 1)
    mc.setAttr(lowerValueInvPma_mdv+'.input2X',- 1)

    # plugh new stretch with rig
    mc.connectAttr(autoStrtDiv , length_pma+'.input1D[0]')
    mc.connectAttr(upperLegMdv+'.outputX', length_pma+'.input1D[1]')
    mc.connectAttr(lowerLegMdv+'.outputX', length_pma+'.input1D[2]')

    mc.connectAttr(autoStrtCon , currentLength_pma+'.input1D[0]')
    mc.connectAttr(upperLegMdv+'.outputX', currentLength_pma+'.input1D[1]')
    mc.connectAttr(lowerLegMdv+'.outputX', currentLength_pma+'.input1D[2]')

    for ax in 'XYZ':
        mc.connectAttr(length_pma+'.output1D',LegStrtDiv_mdv+'.input2{}'.format(ax),f=1)
    mc.connectAttr(currentLength_pma+'.output1D',LegStrtCnd +'.secondTerm',f=1)
    mc.connectAttr(upperValueInvPma_mdv+'.outputX',LegStrt_mdv+ '.input2X',f=1)
    mc.connectAttr(lowerValueInvPma_mdv+'.outputX',LegStrt_mdv+ '.input2Y',f=1)
    # mc.connectAttr(upperValueInvPma_mdv+'.outputX',LegStrtPma+'.input3D[0].input3Dx')
    # mc.connectAttr(lowerValueInvPma_mdv+'.outputX',LegStrtPma+'.input3D[0].input3Dy')

    # body stretch
    upperLegBodyMdv = mc.createNode('multiplyDivide',n='upperLegBodyStrt_Mdv')
    lowerLegBodyMdv = mc.createNode('multiplyDivide',n='lowerLegBodyStrt_Mdv')
    mc.setAttr(upperLegBodyMdv +'.input2X',.1)
    mc.setAttr(lowerLegBodyMdv +'.input2X',.1)
    mc.connectAttr(upLegCtrl+'.upperLeg',upperLegBodyMdv+'.input1X')    
    mc.connectAttr(lowLegCtrl+'.lowerLeg',lowerLegBodyMdv+'.input1X') 

    root_pma = mc.createNode('plusMinusAverage',n = 'RootStrt_Pma')
    mc.connectAttr(upperLegBodyMdv+'.outputX',root_pma+'.input1D[0]')
    mc.connectAttr(lowerLegBodyMdv+'.outputX',root_pma+'.input1D[1]')

    mc.connectAttr(root_pma+'.output1D','RootCtrlPars_Grp.ty')


def neckStretch(ctrl = ''):
    # add attr
    if not mc.objExists('{}.neckStretch'.format(ctrl)):
        mc.addAttr(ctrl ,ln = 'neckStretch',dv=0,k=0)
    
    # create stretch world group
    neckFkWorld = mc.createNode('transform',n = 'NeckWorld_Grp')
    neckFkWorldZro = mc.createNode('transform',n = 'NeckFkWorldZro_Grp')

    
    mc.parent(neckFkWorld,neckFkWorldZro)
    
    mc.delete(mc.pointConstraint('Head_Ctrl', neckFkWorldZro,mo=0))


    mc.parent(neckFkWorldZro,'HeadCtrlZro_Grp')


    mc.parent('HeadCtrlPars_Grp', neckFkWorld)


    # create utility node to connect stretch
    neckMdv = mc.createNode('multiplyDivide',n= 'NeckMdvStrt_Mdv')

    neckMdvPma = mc.createNode('plusMinusAverage',n = 'NeckMdvStretch_Pma')

    mc.setAttr(neckMdv+'.input2X',.1)
    mc.connectAttr(ctrl+'.neckStretch',neckMdv+'.input1X')
    mc.connectAttr(neckMdv+'.outputX', neckMdvPma+'.input1D[0]')
    mc.connectAttr(neckMdvPma+'.output1D',neckFkWorld+'.ty')


def run():
    body_stretch(ctrl='AllMover_Ctrl')
    #bodyFk_stretch('Spine_Ctrl')
    leg_stretch('AllMover_Ctrl','AllMover_Ctrl')
    arm_stretch('AllMover_Ctrl','AllMover_Ctrl')
    neckStretch('AllMover_Ctrl')

# run()
