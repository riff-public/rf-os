import maya.cmds as mc
import pymel.core as pm
from rf_maya.rftool.rig.rigScript import mainRig
reload(mainRig)
from rf_maya.rftool.rig.rigScript import rootRig
reload(rootRig)

from rf_maya.rftool.rig.rigScript import fkRig
reload(fkRig)
from rf_maya.rftool.rig.rigScript import fkChain
reload(fkChain)
from rf_maya.rftool.rig.rigScript import rigTools as rt
reload(rt)
from nuTools.rigTools import proc
reload(proc)
from rf_maya.rftool.rig.rigScript import charTextName
reload(charTextName)


def run():
  sel = mc.listRelatives('AdjTmpJnt_Grp',type='joint',ad=1)

  for i in sel:
      splitList = i.split('_')
      name = splitList[0]
      if len(splitList) ==3 :
          side = '{}'.format(splitList[1])
      else:
          side = ''
       
      fk = fkRig.Run( name     = name,
                     side       = side ,
                     tmpJnt     = i ,
                     parent     = '' ,
                     ctrlGrp    = 'AdjCtrl_Grp', 
                     skinGrp    = 'AdjJnt_Grp' , 
                     shape      = 'square' ,
                     jnt_ctrl   = 0,
                     color      = 'red',
                     jnt        = 1,
                     defaultWorld = 0,
                     constraint =0,
                     gimbal=0)
      rt.addGrp(fk.jnt)
      for t in 'trs':
          for ax in 'xyz':
              fk.ctrl.attr('{}{}'.format(t,ax)) >> fk.jnt.attr('{}{}'.format(t,ax))

  for i in mc.ls('*_L_Ctrl'):
    ctrl = i.replace('_L_','_R_')
    if ctrl != 'ChestFntAdj_R_Ctrl':
      for t in 'trs':
        for ax in 'xyz':
            mc.connectAttr('{}.{}{}'.format(i,t,ax),'{}.{}{}'.format(ctrl,t,ax))

  for i in mc.ls('*L_Ctrl'):
    mc.connectAttr('{}.sz'.format(i),'{}.sx'.format(i))

  mid_ctrl = [u'ChestAdj_Ctrl', u'SpineMidAdj_Ctrl', u'SpineRootAdj_Ctrl', u'NeckAdj_Ctrl']
  for ctrl in mid_ctrl:
      mc.connectAttr('{}.sz'.format(ctrl),'{}.sx'.format(ctrl))
      
      for t in 'trs':
          for ax in 'xyz':
              mc.setAttr('{}.{}{}'.format(ctrl,t,ax),l=1,k=0)
              
      mc.setAttr('{}.sz'.format(ctrl),l=0,k=1)

  if mc.objExists('ChestFntAdj_L_Ctrl'):
    mdv = mc.createNode('multiplyDivide',n='ChestInv_Mdv')
    for ax in 'xyz':
        mc.connectAttr('ChestFntAdj_L_Ctrl.r{}'.format(ax),'ChestFntAdj_R_Ctrl.r{}'.format(ax))
        mc.connectAttr('ChestFntAdj_L_Ctrl.s{}'.format(ax),'ChestFntAdj_R_Ctrl.s{}'.format(ax))
        mc.connectAttr('ChestFntAdj_L_Ctrl.t{}'.format(ax),mdv+'.input1{}'.format(ax.upper()))
        mc.setAttr(mdv+'.input2{}'.format(ax.upper()),-1)
        mc.connectAttr(mdv+'.output{}'.format(ax.upper()),'ChestFntAdj_R_Ctrl.t{}'.format(ax))

  mc.addAttr('NeckAdj_Ctrl',ln = 'neckStretch', dv=0 ,k=1)
  mc.addAttr('ChestAdj_Ctrl',ln = 'bodyStretch', dv=0 ,k=1)
  mc.addAttr('UpArmAdj_L_Ctrl',ln = 'upperArmStretch', dv=0 ,k=1)
  mc.addAttr('ForeArmAdj_L_Ctrl',ln = 'lowerArmStretch', dv=0 ,k=1)
  mc.addAttr('UpLegAdj_L_Ctrl',ln = 'upperLegStretch', dv=0 ,k=1)
  mc.addAttr('LowLegAdj_L_Ctrl',ln = 'lowerLegStretch', dv=0 ,k=1)

  for i in mc.ls('*_Ctrl'):
      mc.deleteAttr(i+'.localWorld')
  for i in mc.ls('*L_Ctrl'):
    ctrl = i.replace('_L_','_R_')
    for t in 'trs':
      for ax in 'xyz':
        mc.setAttr('{}.{}{}'.format(i,t,ax),l=1,k=0)
        mc.setAttr('{}.{}{}'.format(ctrl,t,ax),l=1,k=0)

    mc.setAttr('{}.sz'.format(i), l=0,k=1)

  for i in mc.ls('*_R_Ctrl'):
      mc.setAttr('{}Shape.v'.format(i),0)