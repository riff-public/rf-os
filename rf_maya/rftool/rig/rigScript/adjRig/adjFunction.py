
from rf_maya.rftool.rig.rigScript.adjRig import stretchBody
reload(stretchBody)
from rf_maya.rftool.rig.rigScript.adjRig import adjRig
reload(adjRig)
filePath = adjRig.__file__
directory = os.path.dirname(filePath)
import pickle
from rf_maya.rftool.rig.rigScript import weightTools
reload(weightTools)
from rf_maya.rftool.rig.rigScript import ctrlData
reload(ctrlData)

# add stretching body
def createStretchBody():
	stretchBody.run()
# import adjTmpJnt
def importAdjJnt():
	fileName = 'adjTmpJnt.ma'
	fileName = os.path.join(directory,fileName)
	mc.file(fileName,i=1,ns=':')
# snap adjTmpJnt
def snapAdjTmpJnt():
	pass
# run adjTmpJnt
def createAdjControl():
	adjRig.run()
	file_name = os.path.join(directory,'ctrl_shape.dat')
	with open(file_name) as f:
		data = pickle.load(f)
	ctrlData.read_crv_dict_list(data)

def exportWeight():
	dataFolder = weightTools.getDataFld()
	path_name = os.path.join(dataFolder,'adjust')
	if not os.path.isdir(path_name):
		os.mkdir(path_name)
		
	for sel in mc.ls(sl=True):
		fn = '{}/{}Weight{}.txt'.format(path_name , sel, '_adjust')
		weightTools.writeWeight(sel , fn)

def importWeight():
	dataFolder = weightTools.getDataFld()
	path_name = os.path.join(dataFolder,'adjust')

	sels = mc.ls(sl=True)
	for sel in sels:
		geo = sel.split(':')[-1]
		fn = '{}/{}Weight{}.txt'.format(path_name , geo, '_adjust')
		print fn
		try:
			weightTools.readWeight(sel , fn , force)
			print 'Import {} done!!.'.format(fn)
		except Exception as e:
			print 'Cannot find weight file for {}'.format(sel)
			print e
	mc.select(sels)
	mc.confirmDialog( title='Progress' , message='Importing weight is done!!.' )


# export adjTmpJnt weight
def combineAddRig():
	sel = mc.ls(sl=True)
	addRigNs = pmc.PyNode(sel[1]).namespace()
	bodyNS = pmc.PyNode(sel[0]).namespace()
	mc.connectAttr('{}NeckAdj_Ctrl.neckStretch'.format(addRigNs), '{}AllMover_Ctrl.neckStretch'.format(bodyNS))
	mc.connectAttr('{}ChestAdj_Ctrl.bodyStretch'.format(addRigNs), '{}AllMover_Ctrl.bodyStretch'.format(bodyNS))
	mc.connectAttr('{}UpArmAdj_L_Ctrl.upperArmStretch'.format(addRigNs), '{}AllMover_Ctrl.upperArmStretch'.format(bodyNS))
	mc.connectAttr('{}ForeArmAdj_L_Ctrl.lowerArmStretch'.format(addRigNs), '{}AllMover_Ctrl.lowerArmStretch'.format(bodyNS))
	mc.connectAttr('{}UpLegAdj_L_Ctrl.upperLegStretch'.format(addRigNs), '{}AllMover_Ctrl.upperLegStretch'.format(bodyNS))
	mc.connectAttr('{}LowLegAdj_L_Ctrl.lowerLegStretch'.format(addRigNs), '{}AllMover_Ctrl.lowerLegStretch'.format(bodyNS))


def read_crv_shape(file_path = '', ns = None, search_for = '', replace_as = ''):
	ctrlData.read_crv_shape(file_path,ns)

#ctrlData.write_crv_shape(mc.ls('*CtrlShape'), r'D:\Script\core\rf_maya\rftool\rig\rigScript\adjRig\ctrlShape')