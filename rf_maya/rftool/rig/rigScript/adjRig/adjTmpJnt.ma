//Maya ASCII 2018ff09 scene
//Name: adjTmpJnt.ma
//Last modified: Mon, Mar 07, 2022 03:07:40 PM
//Codeset: 1252
requires maya "2018ff09";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201903222215-65bada0e52";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -n "AdjTmpJnt_Grp";
	rename -uid "F1972AB4-474C-4717-FEC0-0FB3CE346FBA";
createNode joint -n "NeckAdj_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "9FE91229-44BF-73A4-7B55-F49C7810DD4D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.483526865641276e-10 9.2444484074910509 -0.0075701025935510325 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 2.483526865641276e-10 9.2444484074910473 -0.0075701025935510794 1;
	setAttr ".radi" 20;
createNode joint -n "ClavicleAdj_L_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "6EC91BDD-41F2-839E-10FA-44A6869A321D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.88131641452795995 8.5253719839248081 -0.043837812033815655 ;
	setAttr ".jo" -type "double3" -4.0008469607451707 0 -135.52648327328961 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.71357434745187454 -0.70057951059007673 0 0 0.69887221162883684 -0.71183538032001281 -0.069771219981314817 0
		 0.048880287147782114 -0.049786952769087914 0.99756301899294508 0 0.88131641452796128 8.5253719839248081 -0.043837812033815607 1;
	setAttr ".radi" 20;
createNode joint -n "ClavicleAdj_R_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "89261F2E-4728-70EB-43A1-2B8B53BD7760";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.88131579999999921 8.5253700000000023 -0.043837800000000322 ;
	setAttr ".jo" -type "double3" 4.00084712311887 0 -44.473523285789476 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.71357426725126061 -0.70057959227829814 0 0 0.69887229297946118 0.71183530017375218 0.069771222808364175 0
		 -0.048880294827842077 -0.049786949190702916 0.99756301879521658 0 -0.88131579999999865 8.5253699999999988 -0.043837800000000038 1;
	setAttr ".radi" 20;
createNode joint -n "UpArmAdj_L_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "805474C2-4311-D68D-EF93-6081C0582028";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.3327016511638559 8.0656141394360699 -0.088901413243884481 ;
	setAttr ".jo" -type "double3" -4.0008469607451707 0 -135.52648327328961 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.71357434745187454 -0.70057951059007673 0 0 0.69887221162883684 -0.71183538032001281 -0.069771219981314817 0
		 0.048880287147782114 -0.049786952769087914 0.99756301899294508 0 1.3327016511638554 8.0656141394360716 -0.088901413243884481 1;
	setAttr ".radi" 20;
createNode joint -n "ForeArmAdj_L_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "8C8641CB-416A-9D51-715C-F6BD5B59B45A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.4849468247308493 6.8919963196142175 -0.070054795989381843 ;
	setAttr ".jo" -type "double3" 4.9933161746212757 0 -135.52648327328961 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.71357434745187476 -0.70057951059007673 0 0 0.69792071217731433 -0.71086623179374619 0.087039531301991352 0
		 -0.060978112241538762 0.062109176751335482 0.99620485844565609 0 2.4849468247308488 6.8919963196142167 -0.070054795989381913 1;
	setAttr ".radi" 20;
createNode joint -n "ChestAdj_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "12D11D2C-4645-7681-A412-4DB05316948C";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.5755804721196809e-19 7.7990094858894414 0.2436122777681492 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 7.7990094858894379 0.24361227776814898 1;
	setAttr ".radi" 20;
createNode joint -n "ChestFntAdj_R_TmpJnt" -p "ChestAdj_TmpJnt";
	rename -uid "E0F54B13-4DB4-0740-004C-588F98A87482";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.41242200000000007 0.41068051411056228 0.27293072223185144 ;
	setAttr ".jo" -type "double3" -64.166640486795004 -4.6645435716907064 -0.80535198565369248 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99658944473283595 -0.014009027484015824 0.081321742455813806 0
		 0.07931243368537827 0.43468337885287833 -0.89708410866091448 0 -0.022781933851192128 0.90047437903473804 0.4343119572289057 0
		 -0.41242200000000007 8.2096900000000002 0.51654300000000042 1;
createNode joint -n "ChestFntAdj_L_TmpJnt" -p "ChestAdj_TmpJnt";
	rename -uid "E4E78A2C-40F4-C7A1-82F2-4EB7DEC7D94E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.4124224227672551 0.41067970420035582 0.27293069015861726 ;
	setAttr ".jo" -type "double3" 115.83335951320501 4.6645435716907606 0.80535198565362232 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99658944473283595 0.014009027484014638 -0.081321742455814666 0
		 0.079312433685378533 -0.43468337885287855 0.89708410866091448 0 -0.022781933851193571 -0.90047437903473804 -0.43431195722890581 0
		 0.4124224227672551 8.2096891900897937 0.51654296792676624 1;
createNode joint -n "SpineMidAdj_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "AD6AA8F9-46B6-16D7-F89E-EDB57920C87B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 7.2395569221752387 0.24361227776814898 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 7.2395569221752387 0.24361227776814898 1;
	setAttr ".radi" 20;
createNode joint -n "SpineRootAdj_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "30E5CED3-4963-5D93-0BB5-D881C84071B0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 6.7275441705923864 0.24361227776814903 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 6.7275441705923864 0.243612277768149 1;
	setAttr ".radi" 20;
createNode joint -n "RootAdj_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "BC299134-4AC7-B5EB-7A29-339DB592BD3A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.0473874458383577e-17 6.1792107528018745 0.243612277768149 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 6.1792107528018745 0.24361227776814895 1;
	setAttr ".radi" 20;
createNode joint -n "UpLegAdj_L_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "019D68DF-4182-B40B-83F0-348078D68A99";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.42348951560710535 5.1274441585129784 0.21625929344512648 ;
	setAttr ".jo" -type "double3" -6.8923386119683526e-05 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.2246467991473535e-16 0 0 -1.2246467991464675e-16 -0.99999999999927658 -1.202940019411546e-06 0
		 -1.4731766443386046e-22 -1.202940019411546e-06 0.99999999999927647 0 0.42348951560710535 5.1274441585129793 0.21625929344512648 1;
	setAttr ".radi" 20;
createNode joint -n "UpLegAdj_R_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "4A5A5299-4740-C401-D5C3-0CB7718BF71B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.42349000000000014 5.127 0.21625957692151077 ;
	setAttr ".jo" -type "double3" 179.99993107661396 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.2246467991473535e-16 0 0 1.2246467991464675e-16 0.99999999999927647 1.202940017941462e-06 0
		 1.4731766425382711e-22 1.202940017941462e-06 -0.99999999999927658 0 -0.42349000000000014 5.1270000000000007 0.21625957692151079 1;
	setAttr ".radi" 20;
createNode joint -n "LowLegAdj_L_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "48C63998-48EE-2319-A455-0E9CC30F235B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.42348951560710424 1.9492744195003222 0.094094388759001418 ;
	setAttr ".jo" -type "double3" -3.8143083941083002 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-16 0 0 -1.2219340677237799e-16 -0.99778488669103438 -0.06652307788248922 0
		 -8.1467274398220521e-18 -0.06652307788248922 0.99778488669103438 0 0.42348951560710429 1.9492744195003227 0.09409438875900146 1;
	setAttr ".radi" 20;
createNode joint -n "LowLegAdj_R_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "F26B23BE-4F09-754A-9315-4FAA99B1806D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.42349 1.9492700000000003 0.094094399999999995 ;
	setAttr ".jo" -type "double3" 176.18569160589172 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-16 0 0 1.2219340677237799e-16 0.99778488669103438 0.066523077882489068 0
		 8.1467274398220336e-18 0.066523077882489068 -0.99778488669103438 0 -0.42349000000000003 1.9492700000000001 0.094094399999999995 1;
	setAttr ".radi" 20;
createNode joint -n "ElbowAdj_L_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "EEB501AE-49DD-0ABF-9AC6-3B91D3F45FB1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.8887238133313602 7.4992784908195764 -0.14441133901509048 ;
	setAttr ".jo" -type "double3" 5.0128007418298219 5.0469778756919847 -135.08437473958008 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.70540175548991124 -0.7033272357137127 -0.087972511928778155 0
		 0.69792071217826901 -0.71086623179280894 0.087039531301991116 0 -0.12375396100460506 -5.2041704279304223e-16 0.99231293306883328 0
		 1.8887238133313593 7.4992784908195764 -0.14441133901509046 1;
	setAttr ".radi" 20;
createNode joint -n "ElbowAdj_R_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "4A0A9B92-4159-405F-2962-3A94EADD537E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.8887200000000002 7.49928 -0.144411 ;
	setAttr ".jo" -type "double3" -5.0127692882618522 5.0469535234341771 -44.915582456045158 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.70540230740758769 -0.70332673512406674 -0.087972088549562621 0
		 0.69792027524013645 0.71086672707317011 -0.087038989820719337 0 0.12375327920012211 5.4123372450476381e-16 0.99231301809822925 0
		 -1.8887199999999993 7.4992800000000006 -0.14441099999999998 1;
	setAttr ".radi" 20;
createNode joint -n "KneeAdj_L_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "DC898512-4047-B70A-367C-5D9E1CDAF450";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.42348951560710441 3.2373284541917435 0.17996993168368045 ;
	setAttr ".jo" -type "double3" -3.8143083941083002 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-16 0 0 -1.2219340677237799e-16 -0.99778488669103438 -0.06652307788248922 0
		 -8.1467274398220521e-18 -0.06652307788248922 0.99778488669103438 0 0.42348951560710446 3.2373284541917435 0.17996993168368042 1;
	setAttr ".radi" 20;
createNode joint -n "KneeAdj_R_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "B0A312C7-47C5-3167-E9C1-4EB127976EE2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.42348999999999987 3.2373299582807902 0.17996999999995242 ;
	setAttr ".jo" -type "double3" 176.18569160589172 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-16 0 0 1.2219340677237799e-16 0.99778488669103438 0.066523077882489068 0
		 8.1467274398220336e-18 0.066523077882489068 -0.99778488669103438 0 -0.42348999999999987 3.2373299582807911 0.17996999999995245 1;
	setAttr ".radi" 20;
createNode joint -n "ForeArmAdj_R_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "AEC904D8-4E7E-45DA-116D-61AF626852D6";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.4849499999999991 6.892 -0.070054800000000222 ;
	setAttr ".jo" -type "double3" -175.00668382537864 1.987846675914698e-16 135.52648327328961 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.71357434745187431 0.70057951059007662 -3.4694469519536134e-18 0
		 0.69792071217731422 0.71086623179374597 -0.087039531301992934 0 -0.060978112241539872 -0.06210917675133662 -0.99620485844565576 0
		 -2.48495 6.8920000000000012 -0.070054800000000125 1;
	setAttr ".radi" 20;
createNode joint -n "UpLegTipAdj_L_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "A0B7A950-486A-D10A-19FC-B095506AD43D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.42348951560710535 6.203104222065523 0.21626058739966433 ;
	setAttr ".jo" -type "double3" -6.8923386119683526e-05 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.2246467991473535e-16 0 0 -1.2246467991464675e-16 -0.99999999999927658 -1.202940019411546e-06 0
		 -1.4731766443386046e-22 -1.202940019411546e-06 0.99999999999927647 0 0.42348951560710535 6.2031042220655239 0.21626058739966433 1;
	setAttr ".radi" 20;
createNode joint -n "UpLegTipAdj_R_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "F00C545F-4278-B419-9E40-31AE19425A57";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.42348951101303101 6.2031042220655239 0.21626058739966436 ;
	setAttr ".jo" -type "double3" 179.99993107661396 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.2246467991473535e-16 0 0 1.2246467991464675e-16 0.99999999999927647 1.202940017941462e-06 0
		 1.4731766425382711e-22 1.202940017941462e-06 -0.99999999999927658 0 -0.42348951101303095 6.2031042220655239 0.21626058739966433 1;
	setAttr ".radi" 20;
createNode joint -n "UpArmAdj_R_TmpJnt" -p "AdjTmpJnt_Grp";
	rename -uid "E72D02E5-4864-BAA3-E4B2-4D851BC96C99";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.3326999999999998 8.0656099999999977 -0.088901399999999894 ;
	setAttr ".jo" -type "double3" 175.99915303925491 0 135.52648327328961 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.71357434745187454 0.70057951059007662 0 0 0.69887221162883695 0.71183538032001281 0.069771219981313817 0
		 0.048880287147781414 0.049786952769087192 -0.99756301899294497 0 -1.3327 8.0656099999999995 -0.08890139999999995 1;
	setAttr ".radi" 20;
createNode transform -s -n "persp";
	rename -uid "02894686-4774-51AB-9D6B-19A4A830E56E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 28 21 28 ;
	setAttr ".r" -type "double3" -27.938352729602379 44.999999999999972 -5.172681101354183e-14 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "93CA69A4-49D3-4B78-C587-7E9BCEBBBC73";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 44.82186966202994;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "0C479653-46B9-3F3C-D0F4-E7AC9184CA29";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "ED79539C-41E4-C8FD-D203-708D7649CE15";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "0FC6C6A6-4A62-6364-04AE-12B990A864CB";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "56740864-4180-5531-D120-9395F2E41D17";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "7AFE59A4-4284-6678-B15D-FC83D96C8F1A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "6B85967E-4A46-E802-29ED-83B5242F4D8C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "AdjCtrl_Grp";
	rename -uid "35EEDABD-44CA-8D91-3D3C-5EA23A8090D7";
createNode transform -n "AdjJnt_Grp";
	rename -uid "27817281-4CC8-21BC-5398-F890F1D1A6AF";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "CF93409F-4106-196B-3155-DFB3D3BF5897";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "1E302114-4B13-0079-F2CB-1484A394DCB6";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "86358C87-4A35-CEB8-0AD7-A7A4342E9801";
createNode displayLayerManager -n "layerManager";
	rename -uid "2FA9F1FC-4A80-2E72-0946-2CB2C14202D6";
createNode displayLayer -n "defaultLayer";
	rename -uid "6C96D469-4F59-56A0-3018-3A9347F82DB7";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "89177A22-49BA-2520-527C-8080B4F7A364";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "2410ADE0-4DFD-811F-B02D-DFB6889BCF02";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "66A49808-4C8C-4B37-88C3-379C6509C76B";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av -k on ".unw" 1;
	setAttr -av -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".cons" no;
	setAttr -k on ".hwi";
	setAttr -av ".ta" 0;
	setAttr -av ".tq";
	setAttr -av ".etmr";
	setAttr -av ".tmr" 1024;
	setAttr -av ".aoon";
	setAttr -av ".aoam";
	setAttr -av ".aora";
	setAttr -k on ".hff";
	setAttr -av -k on ".hfd";
	setAttr -av -k on ".hfs";
	setAttr -av -k on ".hfe";
	setAttr -av ".hfc";
	setAttr -av -k on ".hfcr";
	setAttr -av -k on ".hfcg";
	setAttr -av -k on ".hfcb";
	setAttr -av -k on ".hfa";
	setAttr -av ".mbe";
	setAttr -av -k on ".mbsof";
	setAttr -k on ".blen";
	setAttr -k on ".blat";
	setAttr -av ".msaa";
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -k on ".fzn";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".bbx";
	setAttr -k on ".vwm";
	setAttr -k on ".tpv";
	setAttr -k on ".uit";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 0 -lu 1;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "O:/Pipeline/core/rf_lib/OpenColorIO-Configs/aces_1.2/config.ocio";
	setAttr ".vtn" -type "string" "sRGB (ACES)";
	setAttr ".wsn" -type "string" "ACES - ACEScg";
	setAttr ".otn" -type "string" "sRGB (ACES)";
	setAttr ".potn" -type "string" "sRGB (ACES)";
select -ne :hardwareRenderGlobals;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -av -k off -cb on ".fbfm";
	setAttr -av -k off -cb on ".ehql";
	setAttr -av -k off -cb on ".eams";
	setAttr -av -k off -cb on ".eeaa";
	setAttr -av -k off -cb on ".engm";
	setAttr -av -k off -cb on ".mes";
	setAttr -av -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -av -k off -cb on ".mbs";
	setAttr -av -k off -cb on ".trm";
	setAttr -av -k off -cb on ".tshc";
	setAttr -av -k off -cb on ".enpt";
	setAttr -av -k off -cb on ".clmt";
	setAttr -av -k off -cb on ".tcov";
	setAttr -av -k off -cb on ".lith";
	setAttr -av -k off -cb on ".sobc";
	setAttr -av -k off -cb on ".cuth";
	setAttr -av -k off -cb on ".hgcd";
	setAttr -av -k off -cb on ".hgci";
	setAttr -av -k off -cb on ".mgcs";
	setAttr -av -k off -cb on ".twa";
	setAttr -av -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
connectAttr "ChestAdj_TmpJnt.s" "ChestFntAdj_R_TmpJnt.is";
connectAttr "ChestAdj_TmpJnt.s" "ChestFntAdj_L_TmpJnt.is";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
dataStructure -fmt "raw" -as "name=externalContentTablZ:string=nodZ:string=key:string=upath:uint32=upathcrc:string=rpath:string=roles";
dataStructure -fmt "raw" -as "name=faceConnectOutputStructure:bool=faceConnectOutput:string[200]=faceConnectOutputAttributes:string[200]=faceConnectOutputGroups";
dataStructure -fmt "raw" -as "name=faceConnectMarkerStructure:bool=faceConnectMarker:string[200]=faceConnectOutputGroups";
dataStructure -fmt "raw" -as "name=FBXFastExportSetting_MB:string=19424";
dataStructure -fmt "raw" -as "name=idStructure:int32=ID";
dataStructure -fmt "raw" -as "name=FBXFastExportSetting_FBX:string=54";
// End of adjTmpJnt.ma
