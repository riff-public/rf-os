# import sys
# pathRig = r'P:/Hanuman/rnd/rigDev/DevScript'
# if not pathRig in sys.path:
#     sys.path.append(pathRig)
import maya.cmds as mc
import pymel.core as pm
from rf_maya.rftool.rig.rigScript import mainRig
reload(mainRig)
from rf_maya.rftool.rig.rigScript import rootRig
reload(rootRig)
from rf_maya.rftool.rig.rigScript import torsoRig
reload(torsoRig)
from rf_maya.rftool.rig.rigScript import neckRig
reload(neckRig)
from rf_maya.rftool.rig.rigScript import headRig
reload(headRig)
from rf_maya.rftool.rig.rigScript import eyeRig
reload(eyeRig)
from rf_maya.rftool.rig.rigScript import jawRig
reload(jawRig)
from rf_maya.rftool.rig.rigScript import legRig
reload(legRig)
from rf_maya.rftool.rig.rigScript import clavicleRig
reload(clavicleRig)
from rf_maya.rftool.rig.rigScript import armRig
reload(armRig)
from rf_maya.rftool.rig.rigScript import fingerRig
reload(fingerRig)
from rf_maya.rftool.rig.rigScript import clavMuscleRig
reload(clavMuscleRig)
from rf_maya.rftool.rig.rigScript import fkRig
reload(fkRig)
from rf_maya.rftool.rig.rigScript import fkChain
reload(fkChain)
from nuTools.rigTools import proc
reload(proc)
import charTextName
reload(charTextName)

from rigScript import twistSet
reload(twistSet)
class  HumanRig( object ):

    def __init__( self , head        = True ,
                         hairRoot    = True ,
                         hair        = [] ,
                         hairMirror  = [] ,
                         neck        = True ,
                         eyeL        = True ,
                         eyeR        = True ,
                         addEye      = [] ,
                         iris        = True ,
                         pupil       = True ,
                         eyeDot      = True ,
                         jaw         = True ,
                         torso       = True ,
                         clavL       = True ,
                         clavR       = True ,
                         armL        = True ,
                         armR        = True ,
                         fingersL    = ['Thumb','Index','Middle','Ring','Pinky'] ,
                         fingersR    = ['Thumb','Index','Middle','Ring','Pinky'] ,
                         # fingersL    = [] ,
                         # fingersR    = [] ,
                         breastL     = False ,
                         breastR     = False ,
                         legL        = True ,
                         legR        = True ,
                         footL       = True,
                         footR       = True ,
                         toeFingersL = [] ,
                         toeFingersR = [] ,
                         sex         = 'Male' ,
                         ribbon      = True ,
                         hi          = True ,
                         inbtw       = True ,
                         size        = 1 
                 ):
        #-- sex info
        if sex == 'Male':
            sex = True
        else:
            sex = False

        print "# Generate >> Main Group"
        self.main = mainRig.Run( 
                                        size      = size
                                )

        print "# Generate >> Root"
        self.root = rootRig.Run( 
                                        rootTmpJnt = 'Root_TmpJnt' ,
                                        ctrlGrp    =  self.main.mainCtrlGrp ,
                                        skinGrp    =  self.main.skinGrp ,
                                        size       =  size
                                )

        if torso:
            print "# Generate >> Torso"
            self.torso = torsoRig.Run(
                                            pelvisTmpJnt = 'Pelvis_TmpJnt' ,
                                            spineTmpJnt  = ['Spine1_TmpJnt' ,
                                                             'Spine2_TmpJnt' ,
                                                             'Spine3_TmpJnt' , 
                                                             'Spine4_TmpJnt' ,
                                                             'Spine5_TmpJnt'] ,
                                            parent       =  self.root.rootJnt ,
                                            ctrlGrp      =  self.main.mainCtrlGrp ,
                                            skinGrp      =  self.main.skinGrp ,
                                            jntGrp       =  self.main.jntGrp ,
                                            stillGrp     =  self.main.stillGrp ,
                                            elem         = '' ,
                                            axis         = 'y' ,
                                            size         = size 
                                     )

            if neck:
                print "# Generate >> Neck"
                self.neck = neckRig.Run(    
                                                neckTmpJnt = 'Neck_TmpJnt' ,
                                                headTmpJnt = 'Head_TmpJnt' ,
                                                parent     =  self.torso.jntDict[-1] ,
                                                ctrlGrp    =  self.main.mainCtrlGrp ,
                                                skinGrp    =  self.main.skinGrp ,
                                                elem       = '' ,
                                                side       = '' ,
                                                ribbon     = ribbon ,
                                                hi         = hi ,
                                                head       = True ,
                                                axis       = 'y' ,
                                                size       = size
                                   )

                if head:
                    print "# Generate >> Head"
                    self.head = headRig.Run(    
                                                    headTmpJnt      = 'Head_TmpJnt' ,
                                                    headEndTmpJnt   = 'HeadEnd_TmpJnt' ,
                                                    parent          =  self.neck.neckEndJnt ,
                                                    ctrlGrp         =  self.main.mainCtrlGrp ,
                                                    skinGrp         =  self.main.skinGrp ,
                                                    elem            = '' ,
                                                    side            = '' ,
                                                    size            = size 
                                       )

                    if eyeL:
                        print "# Generate >> Eye Left"
                        self.eyeL = eyeRig.Run(     eyeMainTrgtTmpJnt   = 'EyeTrgt_TmpJnt' ,
                                                    eyeTrgtTmpJnt       = 'EyeTrgt_L_TmpJnt' ,
                                                    eyeTmpJnt           = 'Eye_L_TmpJnt' ,
                                                    parent              = self.head.headJnt ,
                                                    ctrlGrp             = self.main.mainCtrlGrp ,
                                                    elem                = '' ,
                                                    side                = 'L' ,
                                                    clrTrgt             = 'red',
                                                    size                = size 
                                                )
                        if iris:
                             print "# Generate >> Iris Left"
                             self.irisL = fkRig.Run(   name       = 'Iris' ,
                                                       side       = 'L' ,
                                                       tmpJnt     = 'Iris_L_TmpJnt' ,
                                                       parent     = self.eyeL.eyeJnt ,
                                                       ctrlGrp    = self.main.addRigCtrlGrp , 
                                                       skinGrp    = self.main.skinGrp , 
                                                       shape      = 'circle' ,
                                                       jnt_ctrl   = 1,
                                                       color      = 'cyan',
                                                       jnt        = 1 ,
                                                       scale_constraint = 1,
                                                       defaultWorld = 0,)
                        if pupil:
                             print "# Generate >> Pupil Left"
                             self.pupilL = fkRig.Run(  name       = 'Pupil' ,
                                                       side       = 'L' ,
                                                       tmpJnt     = 'Pupil_L_TmpJnt' ,
                                                       parent     = self.irisL.jnt ,
                                                       ctrlGrp    = self.main.addRigCtrlGrp , 
                                                       skinGrp    = self.main.skinGrp , 
                                                       shape      = 'circle' ,
                                                       jnt_ctrl   = 1,
                                                       color      = 'cyan',
                                                       jnt        = 1,
                                                       scale_constraint = 1,
                                                       defaultWorld = 0,)
                        if eyeDot:
                             print "# Generate >> EyeDot Left"
                             self.eyeDotL = fkRig.Run( name     = 'EyeDot' ,
                                                       side       = 'L' ,
                                                       tmpJnt     = 'EyeDot_L_TmpJnt' ,
                                                       parent     = self.eyeL.eyeJnt ,
                                                       ctrlGrp    = self.main.addRigCtrlGrp , 
                                                       skinGrp    = self.main.skinGrp , 
                                                       shape      = 'circle' ,
                                                       jnt_ctrl   = 1,
                                                       color      = 'cyan',
                                                       jnt        = 1,
                                                       scale_constraint = 1,
                                                       defaultWorld = 0,)



                    if eyeR:
                        print "# Generate >> Eye Right"
                        self.eyeR = eyeRig.Run(     eyeMainTrgtTmpJnt   = 'EyeTrgt_TmpJnt' ,
                                                    eyeTrgtTmpJnt       = 'EyeTrgt_R_TmpJnt' ,
                                                    eyeTmpJnt           = 'Eye_R_TmpJnt' ,
                                                    parent              = self.head.headJnt ,
                                                    ctrlGrp             = self.main.mainCtrlGrp ,
                                                    elem                = '' ,
                                                    side                = 'R' ,
                                                    clrTrgt             = 'blue',
                                                    size                = size 
                                                )
                        if iris:
                             print "# Generate >> Iris Right"
                             self.irisR = fkRig.Run(   name       = 'Iris' ,
                                                       side       = 'R' ,
                                                       tmpJnt     = 'Iris_R_TmpJnt' ,
                                                       parent     = self.eyeR.eyeJnt ,
                                                       ctrlGrp    = self.main.addRigCtrlGrp , 
                                                       skinGrp    = self.main.skinGrp , 
                                                       shape      = 'circle' ,
                                                       jnt_ctrl   = 1,
                                                       color      = 'cyan',
                                                       jnt        = 1,
                                                       scale_constraint = 1,
                                                       defaultWorld = 0,)
                        if pupil:
                             print "# Generate >> Pupil Right"
                             self.pupilR = fkRig.Run(   name       = 'Pupil' ,
                                                       side       = 'R' ,
                                                       tmpJnt     = 'Pupil_R_TmpJnt' ,
                                                       parent     = self.irisR.jnt ,
                                                       ctrlGrp    = self.main.addRigCtrlGrp , 
                                                       skinGrp    = self.main.skinGrp , 
                                                       shape      = 'circle' ,
                                                       jnt_ctrl   = 1,
                                                       color      = 'cyan',
                                                       jnt        = 1,
                                                       scale_constraint = 1,
                                                       defaultWorld = 0,)
                        if eyeDot:
                             print "# Generate >> EyeDot Right"
                             self.eyeDotR = fkRig.Run( name     = 'EyeDot' ,
                                                       side       = 'R' ,
                                                       tmpJnt     = 'EyeDot_R_TmpJnt' ,
                                                       parent     = self.eyeR.eyeJnt ,
                                                       ctrlGrp    = self.main.addRigCtrlGrp , 
                                                       skinGrp    = self.main.skinGrp , 
                                                       shape      = 'circle' ,
                                                       jnt_ctrl   = 1,
                                                       color      = 'cyan',
                                                       scale_constraint = 1,
                                                       jnt = 1,
                                                       defaultWorld = 0,)


                    if addEye:
                        for elemEye , sideEye , colorEye in addEye:
                            print "# Generate >> Eye {} {}".format(elemEye , sideEye)
                            self.addEye = eyeRig.Run(   eyeMainTrgtTmpJnt   = 'EyeTrgt_TmpJnt' ,
                                                        eyeTrgtTmpJnt       = 'Eye{}Trgt_{}_TmpJnt'.format(elemEye,sideEye) ,
                                                        eyeTmpJnt           = 'Eye{}_{}_TmpJnt'.format(elemEye,sideEye)  ,
                                                        parent              = self.head.headJnt ,
                                                        ctrlGrp             = self.main.mainCtrlGrp ,
                                                        elem                = elemEye ,
                                                        side                = sideEye ,
                                                        clrTrgt             = colorEye,
                                                        size                = size 
                                                )

                    if jaw:
                        print "# Generate >> Jaw"
                        self.jaw = jawRig.Run(  jawUpr1TmpJnt     = 'JawUpr1_TmpJnt' ,
                                                jawUprEndTmpJnt    = 'JawUprEnd_TmpJnt' ,
                                                jawLwr1TmpJnt      = 'JawLwr1_TmpJnt' ,
                                                jawLwr2TmpJnt      = 'JawLwr2_TmpJnt' ,
                                                jawLwrEndTmpJnt    = 'JawLwrEnd_TmpJnt' ,
                                                parent             = self.head.headJnt ,
                                                ctrlGrp            = self.main.mainCtrlGrp ,
                                                skinGrp            = self.main.skinGrp ,
                                                elem               = '' ,
                                                side               = '' ,
                                                size                = size 
                                            )

                    if hairRoot:
                        print "# Generate >> Hair Root"
                        self.hairRoot = fkRig.Run(  name       = 'HairRoot' ,
                                                    tmpJnt     = 'HairRoot_TmpJnt' ,
                                                    parent     = self.head.headJnt ,
                                                    ctrlGrp    = self.main.addRigCtrlGrp , 
                                                    skinGrp     = self.head.headJnt , 
                                                    shape      = 'circle' ,
                                                    jnt_ctrl   = 1,
                                                    scale_constraint = 1,
                                                    size       = size ,
                                                    color      = 'cyan',
                                                    jnt         = 1 ,
                                                    )
                        if hair:
                            for each in hair:
                                print "# Generate >> {}".format(each)
                                fkChain.Run(name       = each ,
                                            parent     = self.hairRoot.jnt ,
                                            ctrlGrp    = self.main.addRigCtrlGrp , 
                                            shape      = 'circle' ,
                                            jnt_ctrl   = 0 ,
                                            size       = size ,
                                            no_tip     = 0,
                                            color      = 'pink',)

                        if hairMirror:
                            for each in hairMirror:
                                for eachSide in ('L','R'):
                                    print "# Generate >> {} {}".format(each , eachSide)
                                    fkChain.Run(name       = each ,
                                                side       = eachSide ,
                                                parent     = self.hairRoot.jnt ,
                                                ctrlGrp    = self.main.addRigCtrlGrp , 
                                                shape      = 'circle' ,
                                                jnt_ctrl   = 0 ,
                                                size       = size ,
                                                no_tip     = 0,
                                                color      = 'pink',)


            if legL:
                print "# Generate >> Leg Left"
                self.legL = legRig.Run(upLegTmpJnt   = 'UpLeg_L_TmpJnt' ,
                                    lowLegTmpJnt  = 'LowLeg_L_TmpJnt' ,
                                    ankleTmpJnt   = 'Ankle_L_TmpJnt' ,
                                    ballTmpJnt    = 'Ball_L_TmpJnt' ,
                                    toeTmpJnt     = 'Toe_L_TmpJnt' ,
                                    heelTmpJnt    = 'Heel_L_TmpJnt' ,
                                    footInTmpJnt  = 'FootIn_L_TmpJnt' ,
                                    footOutTmpJnt = 'FootOut_L_TmpJnt' ,
                                    kneeTmpJnt    = 'Knee_L_TmpJnt' ,
                                    heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_L_TmpJnt',
                                    toeIkTwisyPivTmpJnt = 'ToeIkTwistPiv_L_TmpJnt' ,
                                    smartIkTmpJnt = 'FootSmartIk_L_TmpJnt' ,
                                    parent        = self.torso.pelvisJnt , 
                                    ctrlGrp       = self.main.mainCtrlGrp ,
                                    jntGrp        = self.main.jntGrp ,
                                    skinGrp       = self.main.skinGrp ,
                                    stillGrp      = self.main.stillGrp ,
                                    ikhGrp        = self.main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    ribbon        = ribbon ,
                                    hi            = hi ,
                                    foot          = footL ,
                                    kneePos       = 'KneePos_L_TmpJnt' ,
                                    size          = size
                            )

                if toeFingersL:
                  self.toeFingersL = {}
                  for toeFinger in toeFingersL:
                    print "# Generate >> {} Left".format(toeFinger)
                    self.toeFingersL[toeFinger] = fingerRig.Run(  fngr          = toeFinger ,
                                                                  parent        = self.legL.toeJnt , 
                                                                  armCtrl       = self.legL.legCtrl ,
                                                                  innerCup      = ''  , 
                                                                  outerCup      = self.legL.toeJnt ,
                                                                  ctrlGrp       = self.main.mainCtrlGrp ,
                                                                  elem          = '' ,
                                                                  side          = 'L' ,
                                                                  size          = size ,
                                                                  no_tip        = 1,
                                                             )


            if legR:
                print "# Generate >> Leg Right"
                self.legR = legRig.Run(upLegTmpJnt   = 'UpLeg_R_TmpJnt' ,
                                    lowLegTmpJnt  = 'LowLeg_R_TmpJnt' ,
                                    ankleTmpJnt   = 'Ankle_R_TmpJnt' ,
                                    ballTmpJnt    = 'Ball_R_TmpJnt' ,
                                    toeTmpJnt     = 'Toe_R_TmpJnt' ,
                                    heelTmpJnt    = 'Heel_R_TmpJnt' ,
                                    footInTmpJnt  = 'FootIn_R_TmpJnt' ,
                                    footOutTmpJnt = 'FootOut_R_TmpJnt' ,
                                    kneeTmpJnt    = 'Knee_R_TmpJnt' ,
                                    heelIkTwistPivTmpJnt = 'HeelIkTwistPiv_R_TmpJnt',
                                    toeIkTwisyPivTmpJnt = 'ToeIkTwistPiv_R_TmpJnt' ,
                                    smartIkTmpJnt = 'FootSmartIk_R_TmpJnt' ,
                                    parent        = self.torso.pelvisJnt , 
                                    ctrlGrp       = self.main.mainCtrlGrp ,
                                    jntGrp        = self.main.jntGrp ,
                                    skinGrp       = self.main.skinGrp ,
                                    stillGrp      = self.main.stillGrp ,
                                    ikhGrp        = self.main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    ribbon        = ribbon ,
                                    hi            = hi ,
                                    foot          = footL ,
                                    kneePos       = 'KneePos_R_TmpJnt' ,
                                    size          = size
                            )

                if toeFingersR:
                  self.toeFingersR = {}
                  for toeFinger in toeFingersR:
                    print "# Generate >> {} Left".format(toeFinger)
                    self.toeFingersL[toeFinger] = fingerRig.Run(  fngr          = toeFinger ,
                                                                   parent        = self.legR.toeJnt , 
                                                                   armCtrl       = self.legR.legCtrl ,
                                                                   innerCup      = ''  , 
                                                                   outerCup      = self.legR.toeJnt ,
                                                                   ctrlGrp       = self.main.mainCtrlGrp ,
                                                                   elem          = '' ,
                                                                   side          = 'R' ,
                                                                   size          = size ,
                                                                   no_tip        = 1,
                                                             )


            if clavL:
                print "# Generate >> Clavicle Left"
                self.clavL = clavicleRig.Run(clavTmpJnt  = 'Clav_L_TmpJnt' ,
                                         upArmTmpJnt = 'UpArm_L_TmpJnt' ,
                                         parent      = self.torso.jntDict[-1] ,
                                         ctrlGrp     = self.main.mainCtrlGrp ,
                                         jntGrp      = self.main.jntGrp ,
                                         skinGrp     = self.main.skinGrp ,
                                         elem        = '' ,
                                         side        = 'L' ,
                                         axis        = 'y' ,
                                         size        = size 
                                 )

                if armL:
                    print "# Generate >> Arm Left"
                    self.armL = armRig.Run( upArmTmpJnt   = 'UpArm_L_TmpJnt' ,
                                            forearmTmpJnt = 'Forearm_L_TmpJnt' ,
                                            wristTmpJnt   = 'Wrist_L_TmpJnt' ,
                                            handTmpJnt    = 'Hand_L_TmpJnt' ,
                                            elbowTmpJnt   = 'Elbow_L_TmpJnt' ,
                                            parent        = self.clavL.clavEndJnt , 
                                            ctrlGrp       = self.main.mainCtrlGrp ,
                                            jntGrp        = self.main.jntGrp ,
                                            skinGrp       = self.main.skinGrp ,
                                            stillGrp      = self.main.stillGrp ,
                                            ikhGrp        = self.main.ikhGrp ,
                                            elem          = '' ,
                                            side          = 'L' ,
                                            ribbon        = ribbon ,
                                            hi            = hi ,
                                            elbowPos      = 'ElbowPos_L_TmpJnt' , 
                                            size          = size ,
                                            spaces        = [('Head', self.head.headJnt ) ,
                                                            ('Chest', self.torso.jntDict[-1] ) ,
                                                            ('Pelvis', self.torso.pelvisJnt )]
                                     )
                    if fingersL:
                        self.fingersL = {}
                        print "# Generate >> HandCupInner Left"
                        self.handCupInnerL = fingerRig.HandCup(  name          = 'HandCupInner' ,
                                                                handCupTmpJnt = 'HandCupInner_L_TmpJnt' ,
                                                                armCtrl       = self.armL.armCtrl ,
                                                                parent        = self.armL.handJnt ,
                                                                ctrlGrp       = self.main.mainCtrlGrp ,
                                                                elem          = '' ,
                                                                side          = 'L' ,
                                                                size          = size
                                                         )

                        print "# Generate >> HandCupOuter Left"
                        self.handCupOuterL = fingerRig.HandCup(  name          = 'HandCupOuter' ,
                                                                handCupTmpJnt = 'HandCupOuter_L_TmpJnt' ,
                                                                armCtrl       = self.armL.armCtrl ,
                                                                parent        = self.armL.handJnt ,
                                                                ctrlGrp       = self.main.mainCtrlGrp ,
                                                                elem          = '' ,
                                                                side          = 'L' ,
                                                                size          = size
                                                         )

                        for finger in fingersL:
                            print "# Generate >> {} Left".format(finger)
                            if 'Thumb' in finger :
                                self.fingersL[finger] = fingerRig.Run(  fngr          = finger ,
                                                                        parent        = self.armL.handJnt , 
                                                                        armCtrl       = self.armL.armCtrl ,
                                                                        innerCup      = self.armL.handJnt , 
                                                                        outerCup      = '' ,
                                                                        ctrlGrp       = self.main.mainCtrlGrp ,
                                                                        elem          = '' ,
                                                                        side          = 'L' ,
                                                                        size          = size ,
                                                                        no_tip        = 1,
                                                                 )
                                

                            else:
                                self.fingersL[finger] = fingerRig.Run(  fngr          = finger ,
                                                                        parent        = self.armL.handJnt , 
                                                                        armCtrl       = self.armL.armCtrl ,
                                                                        innerCup      = 'HandCupInner_L_Jnt' , 
                                                                        outerCup      = 'HandCupOuter_L_Jnt' ,
                                                                        ctrlGrp       = self.main.mainCtrlGrp ,
                                                                        elem          = '' ,
                                                                        side          = 'L' ,
                                                                        size          = size ,
                                                                        no_tip        = 1,
                                                                 )

                        # print self.fingersL['Index'].fngrJntDict[0].name



                    if breastL:
                        self.breastL = clavMuscleRig.Run (  elem                    = 'clavMus',
                                                            side                    = 'L',
                                                            men                     = sex,
                                                            ctrlGrp                 = self.main.addRigCtrlGrp ,
                                                            skinGrp                 = self.main.skinGrp ,
                                                            jntGrp                  = self.main.jntGrp ,
                                                            ikhGrp                  = self.main.ikhGrp ,
                                                            stillGrp                = self.main.stillGrp ,
                                                            parent                  = self.clavL.gmbl ,
                                                            upArmParentGrp          = [self.armL.armFkCtrlGrp , self.armL.armIkCtrlGrp] ,
                                                            spineJnt                = self.torso.jntDict[-1] ,
                                                            upArmJnt                = self.armL.upArmJnt ,
                                                            neckJnt                 = self.neck.neckJnt )

            if clavR:
                print "# Generate >> Clavicle Right"
                self.clavR = clavicleRig.Run(clavTmpJnt  = 'Clav_R_TmpJnt' ,
                                         upArmTmpJnt = 'UpArm_R_TmpJnt' ,
                                         parent      = self.torso.jntDict[-1] ,
                                         ctrlGrp     = self.main.mainCtrlGrp ,
                                         jntGrp      = self.main.jntGrp ,
                                         skinGrp     = self.main.skinGrp ,
                                         elem        = '' ,
                                         side        = 'R' ,
                                         axis        = 'y' ,
                                         size        = size )

                if armR:
                    print "# Generate >> Arm Right"
                    self.armR = armRig.Run( upArmTmpJnt   = 'UpArm_R_TmpJnt' ,
                                            forearmTmpJnt = 'Forearm_R_TmpJnt' ,
                                            wristTmpJnt   = 'Wrist_R_TmpJnt' ,
                                            handTmpJnt    = 'Hand_R_TmpJnt' ,
                                            elbowTmpJnt   = 'Elbow_R_TmpJnt' ,
                                            parent        = self.clavR.clavEndJnt , 
                                            ctrlGrp       = self.main.mainCtrlGrp ,
                                            jntGrp        = self.main.jntGrp ,
                                            skinGrp       = self.main.skinGrp ,
                                            stillGrp      = self.main.stillGrp ,
                                            ikhGrp        = self.main.ikhGrp ,
                                            elem          = '' ,
                                            side          = 'R' ,
                                            ribbon        = ribbon ,
                                            hi            = hi ,
                                            elbowPos      = 'ElbowPos_R_TmpJnt' , 
                                            size          = size ,
                                            spaces        = [('Head', self.head.headJnt ) ,
                                                            ('Chest', self.torso.jntDict[-1] ) ,
                                                            ('Pelvis', self.torso.pelvisJnt )]
                                     )

                    if fingersR:
                        self.fingersR = {}
                        print "# Generate >> HandCupInner Right"
                        self.handCupInnerR = fingerRig.HandCup(  name          = 'HandCupInner' ,
                                                                handCupTmpJnt = 'HandCupInner_R_TmpJnt' ,
                                                                armCtrl       = self.armR.armCtrl ,
                                                                parent        = self.armR.handJnt ,
                                                                ctrlGrp       = self.main.mainCtrlGrp ,
                                                                elem          = '' ,
                                                                side          = 'R' ,
                                                                size          = size
                                                         )

                        print "# Generate >> HandCupOuter Right"
                        self.handCupOuterR = fingerRig.HandCup(  name          = 'HandCupOuter' ,
                                                                handCupTmpJnt = 'HandCupOuter_R_TmpJnt' ,
                                                                armCtrl       = self.armR.armCtrl ,
                                                                parent        = self.armR.handJnt ,
                                                                ctrlGrp       = self.main.mainCtrlGrp ,
                                                                elem          = '' ,
                                                                side          = 'R' ,
                                                                size          = size
                                                         )

                        for finger in fingersR:
                            print "# Generate >> {} Right".format(finger)
                            if 'Thumb' in finger :
                                self.fingersR[finger] = fingerRig.Run(  fngr          = finger ,
                                                                        parent        = self.armR.handJnt , 
                                                                        armCtrl       = self.armR.armCtrl ,
                                                                        innerCup      = self.armR.handJnt , 
                                                                        outerCup      = '' ,
                                                                        ctrlGrp       = self.main.mainCtrlGrp ,
                                                                        elem          = '' ,
                                                                        side          = 'R' ,
                                                                        size          = size ,
                                                                        no_tip        = 1,
                                                                 )
                            else:
                                self.fingersR[finger] = fingerRig.Run(  fngr          = finger ,
                                                                        parent        = self.armR.handJnt , 
                                                                        armCtrl       = self.armR.armCtrl ,
                                                                        innerCup      = 'HandCupInner_R_Jnt' , 
                                                                        outerCup      = 'HandCupOuter_R_Jnt' ,
                                                                        ctrlGrp       = self.main.mainCtrlGrp ,
                                                                        elem          = '' ,
                                                                        side          = 'R' ,
                                                                        size          = size ,
                                                                        no_tip        = 1,
                                                                 )

                    if breastR:
                        self.breastR = clavMuscleRig.Run (  elem                    = 'clavMus',
                                                            side                    = 'R',
                                                            men                     = sex,
                                                            ctrlGrp                 = self.main.addRigCtrlGrp ,
                                                            skinGrp                 = self.main.skinGrp ,
                                                            jntGrp                  = self.main.jntGrp ,
                                                            ikhGrp                  = self.main.ikhGrp ,
                                                            stillGrp                = self.main.stillGrp ,
                                                            parent                  = self.clavR.gmbl ,
                                                            upArmParentGrp          = [self.armR.armFkCtrlGrp , self.armR.armIkCtrlGrp] ,
                                                            spineJnt                = self.torso.jntDict[-1] ,
                                                            upArmJnt                = self.armL.upArmJnt ,
                                                            neckJnt                 = self.neck.neckJnt )


        if inbtw:
          print "# Generate >> BtwJnt"

          ## Recheck Ribbon Hi Or Low
          if hi:
            ## Neck
            Neck5Rbn = 'Neck5RbnDtl_Jnt'
            Neck1Rbn = 'Neck1RbnDtl_Jnt'

            ## Arm
            UpArm1RbnL = 'UpArm1RbnDtl_L_Jnt'
            UpArm5RbnL = 'UpArm5RbnDtl_L_Jnt'
            Forearm1RbnL = 'Forearm1RbnDtl_L_Jnt'
            Forearm5RbnL = 'Forearm5RbnDtl_L_Jnt'
            UpArm1RbnR = 'UpArm1RbnDtl_R_Jnt'
            UpArm5RbnR = 'UpArm5RbnDtl_R_Jnt'
            Forearm1RbnR = 'Forearm1RbnDtl_R_Jnt'
            Forearm5RbnR = 'Forearm5RbnDtl_R_Jnt'

            ## Leg
            UpLeg1RbnL = 'UpLeg1RbnDtl_L_Jnt'
            UpLeg5RbnL = 'UpLeg5RbnDtl_L_Jnt'
            LowLeg1RbnL = 'LowLeg1RbnDtl_L_Jnt'
            LowLeg5RbnL = 'LowLeg5RbnDtl_L_Jnt'
            UpLeg1RbnR = 'UpLeg1RbnDtl_R_Jnt'
            UpLeg5RbnR = 'UpLeg5RbnDtl_R_Jnt'
            LowLeg1RbnR = 'LowLeg1RbnDtl_R_Jnt'
            LowLeg5RbnR = 'LowLeg5RbnDtl_R_Jnt'

          else:
            Neck5Rbn = 'Neck1RbnDtl_Jnt'
            Neck1Rbn = 'Neck1RbnDtl_Jnt'

            ## Arm
            UpArm1RbnL = 'UpArmRbn_L_Jnt'
            UpArm5RbnL = 'UpArmRbn_L_Jnt'
            Forearm1RbnL = 'ForearmRbn_L_Jnt'
            Forearm5RbnL = 'ForearmRbn_L_Jnt'
            UpArm1RbnR = 'UpArmRbn_R_Jnt'
            UpArm5RbnR = 'UpArmRbn_R_Jnt'
            Forearm1RbnR = 'ForearmRbn_R_Jnt'
            Forearm5RbnR = 'ForearmRbn_R_Jnt'

            ## Leg
            UpLeg1RbnL = 'UpLegRbn_L_Jnt'
            UpLeg5RbnL = 'UpLegRbn_L_Jnt'
            LowLeg1RbnL = 'LowLegRbn_L_Jnt'
            LowLeg5RbnL = 'LowLegRbn_L_Jnt'
            UpLeg1RbnR = 'UpLegRbn_R_Jnt'
            UpLeg5RbnR = 'UpLegRbn_R_Jnt'
            LowLeg1RbnR = 'LowLegRbn_R_Jnt'
            LowLeg5RbnR = 'LowLegRbn_R_Jnt'



          if head:
              proc.btwJnt(jnts=[Neck5Rbn, 'Head_Jnt', 'HeadEnd_Jnt'], axis='y', pointConstraint=True, elem="Head", side="")
          if neck:
              proc.btwJnt(jnts=['Spine5_Jnt', 'Neck_Jnt', Neck1Rbn], axis='y', pointConstraint=True, elem="Neck", side="")
          
          if armL:
              proc.btwJnt(jnts=['Spine5_Jnt', 'Clav_L_Jnt', 'UpArm_L_Jnt'],axis='y', pointConstraint=True, elem="Clavicle", side="L")
              proc.btwJnt(jnts=['ClavEnd_L_Jnt', 'UpArm_L_Jnt', UpArm1RbnL], axis='y', pointConstraint=True, elem="Shoulder", side="L")
              proc.btwJnt(jnts=['ClavEnd_L_Jnt', 'UpArm_L_Jnt', UpArm1RbnL], axis='y', pointConstraint=True, elem="ArmPit", side="L")
              proc.btwJnt(jnts=[UpArm5RbnL, 'Forearm_L_Jnt', Forearm1RbnL], axis='y', pointConstraint=False, elem="ArmLimb", side="L")
              proc.btwJnt(jnts=[Forearm5RbnL, 'Wrist_L_Jnt', 'Hand_L_Jnt'], axis='y', pointConstraint=True, elem="Wrist", side="L")
          if armR:
              proc.btwJnt(jnts=['Spine5_Jnt', 'Clav_R_Jnt', 'UpArm_R_Jnt'],axis='y', pointConstraint=True, elem="Clavicle", side="R")
              proc.btwJnt(jnts=['ClavEnd_R_Jnt', 'UpArm_R_Jnt', UpArm1RbnR], axis='y', pointConstraint=True, elem="Shoulder", side="R")
              proc.btwJnt(jnts=['ClavEnd_R_Jnt', 'UpArm_R_Jnt', UpArm1RbnR], axis='y', pointConstraint=True, elem="ArmPit", side="R")
              proc.btwJnt(jnts=[UpArm5RbnR, 'Forearm_R_Jnt', Forearm1RbnR], axis='y', pointConstraint=False, elem="ArmLimb", side="R")
              proc.btwJnt(jnts=[Forearm5RbnR, 'Wrist_R_Jnt', 'Hand_R_Jnt'], axis='y', pointConstraint=True, elem="Wrist", side="R")

          if legL:
              proc.btwJntPelvis(jnts=['Pelvis_Jnt', 'UpLeg_L_Jnt', UpLeg1RbnL], axis='y', pointConstraint=True, elem="Hip", side="L")
              proc.btwJnt(jnts=[UpLeg5RbnL, 'LowLeg_L_Jnt', LowLeg1RbnL], axis='y', pointConstraint=False, elem="LegLimb", side="L")
              proc.btwJnt(jnts=[LowLeg5RbnL, 'Ankle_L_Jnt', 'Ball_L_Jnt'], axis='y', pointConstraint=True, elem="Foot", side="L")
          if legR:
              proc.btwJntPelvis(jnts=['Pelvis_Jnt', 'UpLeg_R_Jnt', UpLeg1RbnR], axis='y', pointConstraint=True, elem="Hip", side="R")
              proc.btwJnt(jnts=[UpLeg5RbnR, 'LowLeg_R_Jnt', LowLeg1RbnR], axis='y', pointConstraint=False, elem="LegLimb", side="R")
              proc.btwJnt(jnts=[LowLeg5RbnR, 'Ankle_R_Jnt', 'Ball_R_Jnt'], axis='y', pointConstraint=True, elem="Foot", side="R")


          if fingersL:
              fngrListL = fingersL
              for eachFngr in fngrListL:
                  if eachFngr == 'Thumb':
                      #proc.btwJnt(jnts=['Hand_L_Jnt', '{}1_L_Jnt'.format(eachFngr), '{}2_L_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                      proc.btwJnt(jnts=['{}1_L_Jnt'.format(eachFngr), '{}2_L_Jnt'.format(eachFngr), '{}3_L_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                      proc.btwJnt(jnts=['{}2_L_Jnt'.format(eachFngr), '{}3_L_Jnt'.format(eachFngr), '{}4_L_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                  else:
                      if eachFngr == 'Index' or eachFngr == 'Middle':
                          #proc.btwJnt(jnts=['HandCupInner_L_Jnt', '{}1_L_Jnt'.format(eachFngr), '{}2_L_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                          proc.btwJnt(jnts=['{}1_L_Jnt'.format(eachFngr), '{}2_L_Jnt'.format(eachFngr), '{}3_L_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                          proc.btwJnt(jnts=['{}2_L_Jnt'.format(eachFngr), '{}3_L_Jnt'.format(eachFngr), '{}4_L_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                          proc.btwJnt(jnts=['{}3_L_Jnt'.format(eachFngr), '{}4_L_Jnt'.format(eachFngr), '{}5_L_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                      else:
                          #proc.btwJnt(jnts=['HandCupOuter_L_Jnt', '{}1_L_Jnt'.format(eachFngr), '{}2_L_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                          proc.btwJnt(jnts=['{}1_L_Jnt'.format(eachFngr), '{}2_L_Jnt'.format(eachFngr), '{}3_L_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                          proc.btwJnt(jnts=['{}2_L_Jnt'.format(eachFngr), '{}3_L_Jnt'.format(eachFngr), '{}4_L_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                          proc.btwJnt(jnts=['{}3_L_Jnt'.format(eachFngr), '{}4_L_Jnt'.format(eachFngr), '{}5_L_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)

          if fingersR:
              fngrListR = fingersR
              for eachFngr in fngrListR:
                  if eachFngr == 'Thumb':
                      #proc.btwJnt(jnts=['Hand_R_Jnt', '{}1_R_Jnt'.format(eachFngr), '{}2_R_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                      proc.btwJnt(jnts=['{}1_R_Jnt'.format(eachFngr), '{}2_R_Jnt'.format(eachFngr), '{}3_R_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                      proc.btwJnt(jnts=['{}2_R_Jnt'.format(eachFngr), '{}3_R_Jnt'.format(eachFngr), '{}4_R_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                  else:
                      if eachFngr == 'Index' or eachFngr == 'Middle':
                          #proc.btwJnt(jnts=['HandCupInner_R_Jnt', '{}1_R_Jnt'.format(eachFngr), '{}2_R_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                          proc.btwJnt(jnts=['{}1_R_Jnt'.format(eachFngr), '{}2_R_Jnt'.format(eachFngr), '{}3_R_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                          proc.btwJnt(jnts=['{}2_R_Jnt'.format(eachFngr), '{}3_R_Jnt'.format(eachFngr), '{}4_R_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                          proc.btwJnt(jnts=['{}3_R_Jnt'.format(eachFngr), '{}4_R_Jnt'.format(eachFngr), '{}5_R_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                      else:
                          #proc.btwJnt(jnts=['HandCupOuter_R_Jnt', '{}1_R_Jnt'.format(eachFngr), '{}2_R_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                          proc.btwJnt(jnts=['{}1_R_Jnt'.format(eachFngr), '{}2_R_Jnt'.format(eachFngr), '{}3_R_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                          proc.btwJnt(jnts=['{}2_R_Jnt'.format(eachFngr), '{}3_R_Jnt'.format(eachFngr), '{}4_R_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)
                          proc.btwJnt(jnts=['{}3_R_Jnt'.format(eachFngr), '{}4_R_Jnt'.format(eachFngr), '{}5_R_Jnt'.format(eachFngr)],axis='y', pointConstraint=True)


        print "# Generate >> Scale Radius"
        self.allJoint = mc.ls('*Jnt',type='joint')
        for each in self.allJoint:
            if not mc.getAttr('{}.radius'.format(each) , l=True) :
                mc.setAttr('{}.radius'.format(each) , size*0.05)

        if 'HeadEnd_Jnt':
          print True
        else:
          print False
        print "# Generate >> Character Text Name"
        charTextName.Run()




        print '----------DONE HUMAN RIG----------'


class BGHumanRig(HumanRig):
    def __init__( self , head        = True ,
                         hairRoot    = True ,
                         hair        = [] ,
                         hairMirror  = [] ,
                         neck        = True ,
                         eyeL        = True ,
                         eyeR        = True ,
                         addEye      = [] ,
                         iris        = True ,
                         pupil       = True ,
                         eyeDot      = True ,
                         jaw         = True ,
                         torso       = True ,
                         clavL       = True ,
                         clavR       = True ,
                         armL        = True ,
                         armR        = True ,
                         fingersL    = ['Thumb','Index','Middle','Ring','Pinky'] ,
                         fingersR    = ['Thumb','Index','Middle','Ring','Pinky'] ,
                         # fingersL    = [] ,
                         # fingersR    = [] ,                         
                         breastL     = False ,
                         breastR     = False ,
                         legL        = True ,
                         legR        = True ,
                         footL       = True,
                         footR       = True ,
                         toeFingersL = [] ,
                         toeFingersR = [] ,
                         sex         = 'male',
                         size        = 1 
                 ):
      super(BGHumanRig,self).__init__(head        = head ,
                         hairRoot    = hairRoot ,
                         hair        = hair ,
                         hairMirror  = hairMirror ,
                         neck        = neck ,
                         eyeL        = eyeL ,
                         eyeR        = eyeR ,
                         addEye      = addEye ,
                         iris        = iris ,
                         pupil       = pupil ,
                         eyeDot      = eyeDot ,
                         jaw         = jaw ,
                         torso       = torso ,
                         clavL       = clavL ,
                         clavR       = clavR ,
                         armL        = armL ,
                         armR        = armR ,
                         fingersL    = fingersL ,
                         fingersR    = fingersR,
                         breastL     = breastL ,
                         breastR     = breastR ,
                         legL        = legL ,
                         legR        = legR ,
                         footL       = footL,
                         footR       = footR ,
                         toeFingersL = toeFingersL ,
                         toeFingersR = toeFingersR ,
                         sex         = sex ,
                         ribbon      = 0 ,
                         hi          = 0 ,
                         inbtw       = 0 ,
                         size        = size 
                 )


      twistSet.TwistSet( name =  'ForeArm',
      side  = 'L' ,
      twistFromJnt = self.armL.wristNonRollTwistGrp,
      posiA = self.armL.forearmJnt,
      posiB = self.armL.wristJnt,
      parent =self.armL.forearmJnt,
      parentDistGrp = self.armL.armCtrlGrp,
      axis  = 'y' ,
      inverseAxis = 0,
      inverseRoot = 0,
      numJnts = 5 )

      # add twist joint to arm
      twistSet.TwistSet( name =  'ForeArm',
      side  = 'R' ,
      twistFromJnt = self.armR.wristNonRollTwistGrp,
      posiA = self.armR.forearmJnt,
      posiB = self.armR.wristJnt,
      parent =self.armR.forearmJnt,
      parentDistGrp = self.armR.armCtrlGrp,
      axis  = 'y' ,
      inverseAxis = 1,
      inverseRoot = 0,
      numJnts = 5 )

      twistSet.TwistSet( name =  'UpArm',
      side  = 'L' ,
      twistFromJnt = self.armL.upArmNonRollTwistGrp,
      posiA = self.armL.upArmJnt,
      posiB = self.armL.forearmJnt,
      parent =self.armL.upArmJnt,
      parentDistGrp = self.armL.armCtrlGrp,
      axis  = 'y' ,
      inverseAxis = 0,
      inverseRoot = 1,
      numJnts = 5 )

      twistSet.TwistSet( name =  'UpArm',
      side  = 'R' ,
      twistFromJnt = self.armR.upArmNonRollTwistGrp,
      posiA = self.armR.upArmJnt,
      posiB = self.armR.forearmJnt,
      parent =self.armR.upArmJnt,
      parentDistGrp = self.armR.armCtrlGrp,
      axis  = 'y' ,
      inverseAxis = 1,
      inverseRoot = 1,
      numJnts = 5 )
      
      # add twist joint to leg
      twistSet.TwistSet( name =  'LowLeg',
      side  = 'L' ,
      twistFromJnt = self.legL.ankleNonRollTwistGrp,
      posiA = self.legL.lowLegJnt,
      posiB = self.legL.ankleJnt,
      parent =self.legL.lowLegJnt,
      parentDistGrp = self.legL.legCtrlGrp,
      axis  = 'y' ,
      inverseAxis = 0,
      inverseRoot = 0,
      numJnts = 5 )

      twistSet.TwistSet( name =  'LowLeg',
      side  = 'R' ,
      twistFromJnt = self.legR.ankleNonRollTwistGrp,
      posiA = self.legR.lowLegJnt,
      posiB = self.legR.ankleJnt,
      parent =self.legR.lowLegJnt,
      parentDistGrp = self.legR.legCtrlGrp,
      axis  = 'y' ,
      inverseAxis = 1,
      inverseRoot = 0,
      numJnts = 5 )

      twistSet.TwistSet( name =  'UpLeg',
      side  = 'L' ,
      twistFromJnt = self.legL.upLegNonRollTwistGrp,
      posiA = self.legL.upLegJnt,
      posiB = self.legL.lowLegJnt,
      parent =self.legL.upLegJnt,
      parentDistGrp = self.legL.legCtrlGrp,
      axis  = 'y' ,
      inverseAxis = 0,
      inverseRoot = 1,
      numJnts = 5 )

      twistSet.TwistSet( name =  'UpLeg',
      side  = 'R' ,
      twistFromJnt = self.legR.upLegNonRollTwistGrp,
      posiA = self.legR.upLegJnt,
      posiB = self.legR.lowLegJnt,
      parent =self.legR.upLegJnt,
      parentDistGrp = self.legR.legCtrlGrp,
      axis  = 'y' ,
      inverseAxis = 1,
      inverseRoot = 1,
      numJnts = 5 )

