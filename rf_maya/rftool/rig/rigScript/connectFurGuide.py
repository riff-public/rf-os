import maya.cmds as mc
import maya.mel as mm

import core
reload( core )
import rigTools as rt
reload( rt )

cn = core.Node()

def Run( bodyFurGeo     = '',  *args):

    bodyGeo        = 'bodyRig_md:Body_Geo'
    bodyFclRigWrap = 'BodyFclRigWrapped_Geo'
    stillGrp       = 'bodyRig_md:Still_Grp'

    # Duplicate BodyFurFcl_Geo, BodyFurGuide_Geo
    bodyFurFcl = 'BodyFurFcl_Geo'
    boduFurGuide = 'BodyFurGuide_Geo'

    dupBodyGeo = mc.duplicate(bodyGeo, n = bodyFurFcl)
    dupBodyFurGuideGeo = mc.duplicate(bodyFurGeo, n = boduFurGuide)
    mc.parent( bodyFurFcl, boduFurGuide, stillGrp)
    mc.setAttr('{}.v'.format(bodyFurFcl), 0)
    mc.setAttr('{}.v'.format(boduFurGuide), 0)

    # Bsh 
    rt.blendShape(bshGeo = boduFurGuide , target = bodyFurFcl) 
    rt.blendShape(bshGeo = bodyFclRigWrap , target = bodyFurFcl) 
    rt.blendShape(bshGeo = bodyFurFcl , target = bodyFurGeo) 
