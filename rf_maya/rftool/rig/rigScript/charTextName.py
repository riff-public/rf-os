import maya.cmds as mc
reload(mc)
from rf_utils import file_utils
reload(file_utils)
from collections import OrderedDict
from rf_utils.context import context_info
import os,sys

import core
reload( core )

cn = core.Node()

class Run ( object ):
    def  __init__( self, char   = '', 
                         objPar = 'HeadEnd_Jnt' ):

        if char == '':
            asset = context_info.ContextPathInfo()         
            char = asset.name

        fontText = 'MS Shell Dlg 2, 27.8pt'
        rigGrp = 'Rig_Grp'
        ctrlGrp = 'Ctrl_Grp'

        bb = mc.exactWorldBoundingBox(ctrlGrp, ii=True)
        txtCrv = mc.textCurves(n = '{}_'.format(char) , f = fontText , t = char)[0]
        txtBb = mc.exactWorldBoundingBox(txtCrv, ii=True)

        listObj = mc.listRelatives(txtCrv, type = 'transform', c = True, ad = True, s = False)
        listCrv = []
        for each in listObj:
            if 'curve' in each:
                mc.parent(each,w = True)
                mc.move("{}.scalePivot" .format(each),"{}.rotatePivot".format(each), absolute=True) 
                mc.makeIdentity (each , a = True , jo = False, n = 1, t =True, r = True, s = True)
                listCrv.append(each)

        crvGrp = cn.createNode('transform' , '{}Crv_Grp'.format(char))
        crvCtrl = cn.createNode('transform' , '{}_Crv'.format(char))
        mc.parent (crvCtrl, crvGrp)

        # CombineShape
        for crv in listCrv:
            shapes = mc.listRelatives(crv, type = 'nurbsCurve', c = True, ad = True, s = False)
            mc.parent(shapes,crvCtrl, r = True, s = True)
        mc.delete(txtCrv, listCrv )

        ################################################################################################

        mc.xform( crvGrp , cp = True )

        objWidth = abs(bb[0] - bb[3])
        txtWidth = abs(txtBb[0] - txtBb[3])

        tmpLoc = cn.locator( target = '')
        sclVal = (objWidth/txtWidth)/6
        mc.scale( sclVal , sclVal , sclVal , tmpLoc , r = True )

        posUp = mc.xform('HeadEnd_Jnt' , t=True , q=True ,ws=True)[1]

        upAx = mc.upAxis(q=True, ax=True)
        if upAx == "y":
            mc.move( 0 , posUp*1.1 , 0 , tmpLoc , r = True )

        elif upAx == "z":
            mc.move( 0 , 0 , posUp*1.1 , tmpLoc , r = True )

        mc.delete( mc.parentConstraint( tmpLoc , crvGrp, mo = False ))  
        mc.delete( mc.scaleConstraint( tmpLoc , crvGrp, mo = False ))
        mc.delete(tmpLoc)
        if mc.objExists(rigGrp):
            mc.parent('{}Crv_Grp'.format(char), rigGrp )
        if objPar:
            mc.parentConstraint( objPar, crvGrp, mo = True)
            mc.scaleConstraint( objPar, crvGrp, mo = True)
        
        ################################################################################################

        ListAttr = ('Control','MasterVis','BlockingVis','SecondaryVis','AddRigVis','Facial','FacialVis','DetailVis','Geometry','GeoVis')
        for attr in ListAttr:
            mc.addAttr( crvCtrl , ln = attr , at = 'long', min = 0, max = 1 , k = True )

        crvCtrl.lockHideAttrs('tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')
        crvCtrl.lockAttrs('Control')
        crvCtrl.lockAttrs('Facial')
        crvCtrl.lockAttrs('Geometry')
        crvCtrl.attr("overrideEnabled").v = 1
        crvCtrl.attr("overrideColor").v = 19

        corePath = '{}/core'.format(os.environ.get('RFSCRIPT'))
        path = '{}/rf_maya/rftool/rig/rigScript/template/listCtrlGrp.yml'.format(corePath)
        data = file_utils.ymlLoader(path)

        ctrlGrpName = file_utils.ymlLoader(path)  

        AttrConnect = ('MasterVis','BlockingVis','SecondaryVis','AddRigVis','FacialVis','DetailVis','GeoVis')
        for at in AttrConnect:
            for grpname in ctrlGrpName[at]:
                crvCtrl.attr(at).value = 1
                if mc.objExists(grpname): 
                    core.Dag(grpname).unlockHideAttrs("v")
                    mc.connectAttr('{}.{}'.format(crvCtrl,at),'{}.visibility'.format(grpname),f = True)
                else:
                    pass   