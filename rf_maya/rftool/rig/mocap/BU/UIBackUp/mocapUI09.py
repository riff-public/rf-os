import maya.cmds as mc
import maya.mel as mel
import sys
#sys.path.append(r'N:\Staff\Hong\Scripts\Mocap\DarkHorse')

#Mocap HIK UI V09
import HIKDefinition as HIKD
reload(HIKD)

import MoCapDefinition as MCD
reload(MCD)

import wholeMocapDef as wmd
reload(wmd)


mocapUI = 'mocapUI'
def createChar(*args):
    NS= mc.textField('nsTextField',q=1,text=1)
    charName = mc.textField('CharacterName',q=1,text=1)
    HIKD.crateChar(NS,charName)
    HIKD.createCustomRigMap(NS)


def createMocapDefinition(*args):
    MCD.createDefinition()

def changeDirectory(*args):
    browseDir = mc.fileDialog2(ds=1,cap= 'Select directory of Mocap Data Folder.',fileMode=3)[0]
    mc.textField('dir',e=1,text=browseDir)

def createWholeMocapFromFolder(*args):
    dir = mc.textField('dir',q=1,text=1)
    wmd.folderMocapDef(dir)

def referenceMocap(*args):
    mocFile = mc.fileDialog2(ds=1,cap= 'Select Mocap Data File.',fileMode=4)

    for files in mocFile:
        #print files
        namespaces = files.split('/')[-1].split('.')[0]
        mc.file(files,ns=namespaces,r=1)
    return mocFile


def moHIKUI():
    if mc.window(mocapUI,exists=1):
        mc.deleteUI(mocapUI)
    win = mc.window(mocapUI,title = 'mocap Tool')
    mc.showWindow(win)

    main = mc.columnLayout('main',rs=5,co=['both',12])
    mc.separator(h=5,st='none')
    mc.text('Head',label = 'Mocap HIK UI V.09',h=20)
    
    tabLayout1 = mc.tabLayout('layout1')
   
    layout1 = mc.columnLayout('HIKtoRig',rs=5,co=['both',12])
    mc.separator(h=2,st='singleDash',w=300)
    mc.text('text1',label = '1.Please select reference Character Rig to get namespace.')
    nsTextField = mc.textField('nsTextField',w=150,text='')
    TPoseCheck = mc.checkBox('TposeCheck',label = 'Auto TPose Set',v=1)    
    
    def getNSText(*args):
        NS = HIKD.getNS()
        mc.textField('nsTextField',e=1,text=NS)
        
        if mc.checkBox('TposeCheck',q=1,v=1):
            HIKD.TPoseSet(NS)
    
    nsButton= mc.button('nameSpace',label = 'Get NameSpace',w=150,c=getNSText)      
    mc.separator(h=5,w=300,st='singleDash')
    
    mc.text('text2',label = '2. Please fill name for Creating HIK Character Node.' )
    charName = mc.textField('CharacterName',w=150,text='Character1')
    charButton= mc.button('CharacterCreate',label = 'Create HIK Character',w=150,c=createChar)
    mc.separator(h=5,w=300,st='singleDash')
    
    mc.text('text4',label = '3. Reference MotionCapture Data File.')
    moRef = mc.button('MoRef',label = 'Reference',w=150,c=referenceMocap)
    mc.separator(h=5,w=300,st='singleDash')

    mc.text('text5',label = '4. Select Source from MAYA HIK UI to apply motion to Rig.')
    mc.separator(h=6,w=300,st='singleDash')
    
    mc.text('text6',label = '5. Bake Animation to Controller from MAYA HIK UI.')
    mc.separator(h=6,w=300,st='singleDash')
    
    mc.text('text7',label = '6. Delete Character Node')
    charDel = mc.button('charDel',label = 'Delete',w=150,c="HIKD.delUnknownHIKNode(mc.textField('CharacterName',q=1,text=1))")    
    mc.separator(h=2,w=300,st='singleDash')
    
    mc.text('text8',label = '7. Create Anim Layer for Mocap')
    charAnim = mc.button('charAnim',label='Create',w=150,c="HIKD.moveAnimLayer(HIKD.HIKCharList.partCtlList,mc.textField('nsTextField',q=1,text=1))")
    mc.separator(h=5,w=300,st='none')
    mc.separator(h=5,w=300,st='none',p=main)
    
    layout2 = mc.columnLayout('MocapToHIK',rs=5,co=['both',12],p = tabLayout1)
    mc.separator(h=2,st='singleDash',w=300)
    mc.text('text9',label = '1.Please select Mocap Jnt Chain Data.')     
    mc.separator(h=5,w=300,st='singleDash')
    mc.text('test10',label = '2. Please fill name for Creating HIK Character Node.' )
    charName2 = mc.textField('CharacterName2',w=150,text='Character1')
    moCapButton2= mc.button('moCapCreate2',label = 'Create HIK Definition',w=150,c=createMocapDefinition)
    mc.separator(h=5,w=300,st='singleDash')
    
    frameLayout1 = mc.frameLayout('frameLayout1',label = 'Whole Folder Creating Mocap Definition',cl=1,cll=1,w=300)
    layout3 = mc.columnLayout('loopMocap',w=300,p=frameLayout1)
    mc.text('test11',label = '1. Please fill directory of Mocap Folder.' )
    mc.separator(h=3,w=300,st='none')
    rowLayout1 = mc.rowLayout('textFieldLayout',w=300,numberOfColumns =2)
    dir = mc.textField('dir',w=225,text='')
    browse = mc.button('browser',w=75,label = 'Browse',c=changeDirectory)
    mc.setParent(layout3)
    mc.separator(h=3,w=300,st='none')
    loopMocapButton= mc.button('loopMocapButton',label = 'create mocap file',w=150,c=createWholeMocapFromFolder)
    mc.separator(h=5,w=300,st='none')
    
moHIKUI()

