import maya.cmds as mc

mocapUI = 'mocapUI'

def moHIKUI():
    if mc.window(mocapUI,exists=1):
        mc.deleteUI(mocapUI)
    win = mc.window(mocapUI,title = 'mocap Tool')
    mc.showWindow(win)
    
    layout1 = mc.columnLayout('column1',rs=5,co=['both',12])
    mc.separator(h=5,st='none')
    mc.text('Head',label = 'Mocap HIK UI V.01',h=20)
    
    mc.separator(h=2,st='singleDash',w=300)
    mc.text('text1',label = '1.Please select reference Character Rig.')
    
    mc.separator(h=5,w=300,st='singleDash')
    mc.text('text2',label = '2. Please fill name for Creating HIK Character Node.' )
    charName = mc.textField('CharacterName',w=150)
    charButton = mc.button('CharacterCreate',label = 'Create HIK Character',w=150)
   
    mc.separator(h=5,w=300,st='singleDash')
    mc.text('text3',label = '3. Connect Controller to Custom Rig Map.')
    CustomMap = mc.button('CustomMap',label = 'Connect',w=150)
    
    mc.separator(h=5,w=300,st='singleDash')
    mc.text('text4',label = '4. Reference MotionCapture Data File.')
    moRef = mc.button('MoRef',label = 'Reference',w=150)
    mc.separator(h=5,w=300,st='singleDash')

    mc.text('text5',label = '5. Select Source from MAYA HIK UI to apply motion to Rig.')
    mc.separator(h=6,w=300,st='singleDash')
    
    mc.text('text6',label = '6. Bake Animation to Controller from MAYA HIK UI.')
    mc.separator(h=6,w=300,st='singleDash')
    
    mc.text('text7',label = '7. Delete Character Node')
    charDel = mc.button('charDel',label = 'Delete',w=150)    
    mc.separator(h=2,w=300,st='singleDash')
    
    mc.text('text8',label = '8. Create Anim Layer for Mocap')
    charAnim = mc.button('charAnim',label='Create',w=150)
    mc.separator(h=5,w=300,st='none')
moHIKUI()