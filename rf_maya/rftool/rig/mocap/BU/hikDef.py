# HIK DEFINITION NEWRIG V 2.0
import maya.cmds as mc
import maya.mel as mel
import pymel.core as pmc
import os
import maya.app.hik.retargeter as rte
import sys

from mocapData import char_list as cl
reload(cl)

from mocapData import mocapData
reload(mocapData)


def get_ns():
    ''' get NameSpace for another usage'''
    # sel = mc.ls(sl=True)
    # if ':' in sel[0]:
    #     ns = sel[0].split(':')[0]
    #     return ns
    # else:
    #     return None
    ns = pmc.PyNode(mc.ls(sl=True)[0]).namespace()
    return ns

def tPose_set(from_asset = 0):
    '''
    set Character Rig to Tpose Position and switch IK to FK Control.
    '''

    ns = pmc.PyNode(mc.ls(sl=True)[0]).namespace()
    if from_asset:
        fld_path = mocapData.get_asset_path(mc.ls(sl=True)[0])
    else:
        fld_path = mocapData.get_mocap_folder()
    
    tPose_file_name = 'tPose'
    tPose_path = os.path.join(fld_path, tPose_file_name)
    
    if os.path.isdir( fld_path ):
        mocapData.read_tpose(tPose_path,ns)
    else:
        pass
    
# create HIK Definition for Character
def create_char(ns='',char_name='Character1'):
    # get rig hik data from path
    char_dict = cl.char_dict.copy()
    partNameList = char_dict.keys()
    #char_name = char
    #mel.eval('hikCreateDefinition();string $char = hikGetCurrentCharacter();string $newName =`rename $char "%s"`;'%(char_name))
    
    mel.eval('hikCreateCharacter( "%s" );'%char_name)
    mel.eval('hikUpdateCharacterList();')
    mel.eval('hikSelectDefinitionTab();')
    
    for part in partNameList:
        part_data = char_dict[part]
        partNumList = part_data[0]
        charJntList = part_data[1]
        
        if ns:
            if ns[-1] == ':':
                charJnt = '{}{}'.format(ns,charJntList)
            else: 
                charJnt = '{}:{}'.format(ns,charJntList)

        else:
            charJnt = charJntList

        #mel.eval(str("setCharacterObject("+"\""+charJnt+"\",$newName,"+"\""+str(partNumList)+"\",0);"))
        mel.eval('setCharacterObject("%s", "%s" , %s ,0);'%(charJnt, char_name, partNumList))
    
    #mel.eval("hikCreateCustomRig(hikGetCurrentCharacter());")
    mel.eval('hikSetCurrentCharacter "%s" ;'% char_name)
    mel.eval('hikSetCurrentSourceFromCharacter("%s");'%char_name)
    mel.eval("hikUpdateSourceList();")

    mel.eval("hikDefinitionUpdateCharacterLists;")
    mel.eval("hikDefinitionUpdateBones")
    mel.eval("HIKCharacterControlsTool;")

# connect Controller to CustomRig Map
def create_hik_custom_rig(ns='', char_name = ''):
    ''' This Part is creating customRig map node by using MAYA HIK MEL MODULE'''
    # create rig custom map
    try:
        mel.eval("hikCreateCustomRig(hikGetCurrentCharacter());")
    except:
        mel.eval("hikCreateCustomRig(hikGetCurrentCharacter());")
    
    char_dict = cl.char_dict.copy()
    partNameList = char_dict.keys()    
    
    for part in partNameList:
        part_data = char_dict[part]
        partNum = part_data[0]
        partCtlList = part_data[2]
        
        if ns:
            if ns[-1] == ':':
                partCtl = '{}{}'.format(ns,partCtlList)
            else:
                partCtl = '{}:{}'.format(ns,partCtlList)
        else:
            partCtl = partCtlList
        
        print partCtl
       
        char_name = mel.eval("hikGetCurrentCharacter() ;")
        mc.select(partCtl)    
        # get character name
        retargeter = mel.eval('RetargeterGetName("%s");'%(char_name))

        # check character control
        if mc.objExists(partCtl):

            # do rig custom map
            if part == 'Hips':
                retargetCom = 'RetargeterAddMapping("%s", "%s", "R", "%s",%s) ;' %(retargeter, part, partCtl, partNum)
                mel.eval(retargetCom)
        
                retargetCom = 'RetargeterAddMapping("%s", "%s", "T", "%s",%s) ;' %(retargeter, part, partCtl, partNum)
                mel.eval(retargetCom)
            else :
                retargetCom = 'RetargeterAddMapping("%s", "%s", "R", "%s",%s) ;' %(retargeter, part, partCtl, partNum)
                mel.eval(retargetCom)
            
            # if part == 'Hips':
            #     retargetCom = "RetargeterAddMapping("+'"'+retargeter+'"'+","+'"'+part+'",'+'"R"'+',"'+partCtl +'",'+str(partNum)+' );'
            #     mel.eval(retargetCom)
            #     retargetCom = "RetargeterAddMapping("+'"'+retargeter+'"'+","+'"'+part+'",'+'"T"'+',"'+partCtl +'",'+str(partNum) +' );'
            #     mel.eval(retargetCom)
            # else :
            #     retargetCom = "RetargeterAddMapping("+'"'+retargeter+'"'+","+'"'+part+'",'+'"R"'+',"'+partCtl +'",'+str(partNum) +' );'
            #     mel.eval(retargetCom)
            
            # print retargetCom


            mel.eval('hikUpdateCustomRigAssignedMappings "%s";'%(char_name))
            mel.eval('hikCustomRigToolWidget -e -sl %s;'%(partNum))
            mel.eval("hikUpdateCustomRigUI;")


def delete_HIK_unknown_node(Character='Character1'):
    '''' This script will delete Character Node and delete etc Node that made during retargeting '''
    delCom = "deleteCharacter("+'"'+Character+'"'+");"
    mel.eval(delCom)
    mc.delete(mc.ls(type='composeMatrix'))
    mc.delete(mc.ls(type='eulerToQuat'))
    mc.delete(mc.ls(type='quatInvert'))
    mc.delete(mc.ls(type='CustomRigDefaultMappingNode'))
    
# Anim Layer 
# def createControlList(ctlList=[],ns=''):
#     '''createController List for another usage  '''
#     if ns!='':          
#         newCtlList = [ns+':'+i for i in ctlList if i != '']
#     else:
#         newCtlList = ctlList
#     print newCtlList
#     return newCtlList

# def moveAnimLayer(ctlList=[],ns=''):
#     ''' create Mocap and Animation AnimLayer by using controllerList and namespace '''
#     newCtlList = createControlList(ctlList,ns)
#     anim = mc.animLayer('%s:Anim'%ns)
#     mc.animLayer(anim,newCtlList,aso=1,e=1)

#     mc.select(newCtlList)
#     mel.eval("string $lSelection[]=`ls -selection`;string $layers[]={" +"\"" +"BaseAnimation" +"\"" +"}; layerEditorExtractObjectsAnimLayer($lSelection, $layers );")
#     mocapLayer = mc.rename('BaseAnimation_extract','%s:Mocap'%ns)
    




# run script
#def runAll()
#NS=get_ns()
#create_hik_custom_rig(NS)
#create_hik_custom_rig(NS)
#delete_HIK_unknown_node(Character)
#moveAnimLayer(HIKCharList.partCtlList,NS)