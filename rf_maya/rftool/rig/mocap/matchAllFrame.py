import maya.cmds as mc
from rftool.rig.ncmel import match
reload(match)


def matchAllFrame(NS = '',types = 'Leg',side = 'L'):
	
	ctrlKeyList = []
####### leg Left ###################   
	legSwitchL = 'Leg_L_Ctrl'
	legL = 'UpLegFk_L_Ctrl'
	kneeL = 'LowLegFk_L_Ctrl'
	ankleL = 'AnkleFk_L_Ctrl'
	
	legIKL = 'UpLegIk_L_Ctrl'
	kneeIKL = 'KneeIk_L_Ctrl'
	ankleIKL = 'AnkleIk_L_Ctrl'
	
####### leg Right ###################        
	legSwitchR = 'Leg_R_Ctrl'
	legR = 'UpLegFk_R_Ctrl'
	kneeR = 'LowLegFk_R_Ctrl'
	ankleR = 'AnkleFk_R_Ctrl'    
	
	legIKR = 'UpLegIk_R_Ctrl'
	kneeIKR = 'KneeIk_R_Ctrl'
	ankleIKR = 'AnkleIk_R_Ctrl'
	
####### Arm Left #######################   
	armSwitchL = 'Arm_L_Ctrl'
	armL = 'UpArmFk_L_Ctrl'
	elbowL = 'ForearmFk_L_Ctrl'
	wristL = 'WristFk_L_Ctrl'

	armIKL = 'UpArmIk_L_Ctrl'
	elbowIKL = 'ElbowIk_L_Ctrl'
	wristIKL = 'WristIk_L_Ctrl'
	
####### Arm Right #######################
	armSwitchR = 'Arm_R_Ctrl'        
	armR = 'UpArmFk_R_Ctrl'
	elbowR = 'ForearmFk_R_Ctrl'
	wristR = 'WristFk_R_Ctrl'    

	armIKR = 'UpArmIk_R_Ctrl'
	elbowIKR = 'ElbowIk_R_Ctrl'
	wristIKR = 'WristIk_R_Ctrl'      
########case Leg #################################################################
	if types == 'Leg':
		if side == 'L':
			leg = legL
			knee = kneeL
			ankle = ankleL
			legSwitch = legSwitchL
			
			legIK = legIKL
			kneeIK = kneeIKL
			ankleIK = ankleIKL
			
		elif side == 'R':
			leg = legR
			knee = kneeR
			ankle = ankleR
			legSwitch = legSwitchR
			legIK = legIKR
			kneeIK = kneeIKR
			ankleIK = ankleIKR
						
		ctrlList = (leg,knee,ankle)
		switchName = '%s:%s'%(NS,legSwitch)
		IKCtrlList = (legIK,kneeIK,ankleIK)
		for ctrl in ctrlList:
			ctrlName = '%s:%s'%(NS,ctrl)
			keys = mc.keyframe(ctrlName,q=1,timeChange=1)
			ctrlKeyList = set(list(ctrlKeyList) + list(keys))
		
		for key in ctrlKeyList:
			mc.currentTime(key,e=1)
			if mc.getAttr(switchName+'.fkIk')== 1:
				mc.setAttr(switchName+'.fkIk',0)                
			mc.select(switchName)
			match.run()
			for IKCon in IKCtrlList:
				IKCtrl = '%s:%s'%(NS,IKCon) 
				mc.setKeyframe(IKCtrl,s=0)

########case Arm #################################################################
	elif types == 'Arm':
		if side == 'L':
			arm= armL
			elbow = elbowL
			wrist = wristL
			armSwitch = armSwitchL
			
			armIK = armIKL
			elbowIK = elbowIKL
			wristIK = wristIKL
			
		elif side == 'R':
			arm= armR
			elbow = elbowR
			wrist = wristR
			armSwitch = armSwitchR
			
			armIK = armIKR
			elbowIK = elbowIKR
			wristIK = wristIKR
						
		ctrlList = (arm,elbow,wrist)
		switchName = '%s:%s'%(NS,armSwitch)
		IKCtrlList = (armIK,elbowIK,wristIK)
		for ctrl in ctrlList:
			ctrlName = '%s:%s'%(NS,ctrl)
			keys = mc.keyframe(ctrlName,q=1,timeChange=1)
			ctrlKeyList = set(list(ctrlKeyList) + list(keys))
		
		for key in ctrlKeyList:
			mc.currentTime(key,e=1)
			if mc.getAttr(switchName+'.fkIk')== 1:
				mc.setAttr(switchName+'.fkIk',0)                
			mc.select(switchName)
			match.run()
			for IKCon in IKCtrlList:
				IKCtrl = '%s:%s'%(NS,IKCon) 
				mc.setKeyframe(IKCtrl,s=0)

def matchAllAllFrame(NS = ''):
	
	rLegCtrlKeyList = []
	lLegCtrlKeyList= []
	rArmCtrlKeyList = []
	lArmCtrlKeyList= []
	ctrlKeyList =[]
####### leg Left ###################   
	legSwitchL = 'Leg_L_Ctrl'
	legL = 'UpLegFk_L_Ctrl'
	kneeL = 'LowLegFk_L_Ctrl'
	ankleL = 'AnkleFk_L_Ctrl'
	
	legIKL = 'UpLegIk_L_Ctrl'
	kneeIKL = 'KneeIk_L_Ctrl'
	ankleIKL = 'AnkleIk_L_Ctrl'
	
####### leg Right ###################        
	legSwitchR = 'Leg_R_Ctrl'
	legR = 'UpLegFk_R_Ctrl'
	kneeR = 'LowLegFk_R_Ctrl'
	ankleR = 'AnkleFk_R_Ctrl'    
	
	legIKR = 'UpLegIk_R_Ctrl'
	kneeIKR = 'KneeIk_R_Ctrl'
	ankleIKR = 'AnkleIk_R_Ctrl'
	

########case Leg #################################################################
	lleg = legL
	lknee = kneeL
	lankle = ankleL
	llegSwitch = legSwitchL
	
	llegIK = legIKL
	lkneeIK = kneeIKL
	lankleIK = ankleIKL
			
	rleg = legR
	rknee = kneeR
	rankle = ankleR
	rlegSwitch = legSwitchR
	rlegIK = legIKR
	rkneeIK = kneeIKR
	rankleIK = ankleIKR
						
	lLegCtrlList = (lleg,lknee,lankle)
	lLegSwitchName = '%s:%s'%(NS,llegSwitch)
	lLegIKCtrlList = (llegIK,lkneeIK,lankleIK)

	rLegCtrlList = (rleg,rknee,rankle)
	rLegSwitchName = '%s:%s'%(NS,rlegSwitch)
	rLegIKCtrlList = (rlegIK,rkneeIK,rankleIK)


####### Arm Left #######################   
	armSwitchL = 'Arm_L_Ctrl'
	armL = 'UpArmFk_L_Ctrl'
	elbowL = 'ForearmFk_L_Ctrl'
	wristL = 'WristFk_L_Ctrl'

	armIKL = 'UpArmIk_L_Ctrl'
	elbowIKL = 'ElbowIk_L_Ctrl'
	wristIKL = 'WristIk_L_Ctrl'
	
####### Arm Right #######################
	armSwitchR = 'Arm_R_Ctrl'        
	armR = 'UpArmFk_R_Ctrl'
	elbowR = 'ForearmFk_R_Ctrl'
	wristR = 'WristFk_R_Ctrl'    

	armIKR = 'UpArmIk_R_Ctrl'
	elbowIKR = 'ElbowIk_R_Ctrl'
	wristIKR = 'WristIk_R_Ctrl'      

########case Arm #################################################################

	larm= armL
	lelbow = elbowL
	lwrist = wristL
	larmSwitch = armSwitchL
	
	larmIK = armIKL
	lelbowIK = elbowIKL
	lwristIK = wristIKL
	
	rarm= armR
	relbow = elbowR
	rwrist = wristR
	rarmSwitch = armSwitchR
	
	rarmIK = armIKR
	relbowIK = elbowIKR
	rwristIK = wristIKR

	lArmCtrlList = (larm,lelbow,lwrist)
	lArmSwitchName = '%s:%s'%(NS,larmSwitch)
	lArmIKCtrlList = (larmIK,lelbowIK,lwristIK)

	rArmCtrlList = (rarm,relbow,rwrist)
	rArmSwitchName = '%s:%s'%(NS,rarmSwitch)
	rArmIKCtrlList = (rarmIK,relbowIK,rwristIK)


	for ctrl in lLegCtrlList:
		ctrlName = '%s:%s'%(NS,ctrl)
		keys = mc.keyframe(ctrlName,q=1,timeChange=1)
		if keys != None:
			lLegCtrlKeyList = set(list(lLegCtrlKeyList) + list(keys))


	for ctrl in rLegCtrlList:
		ctrlName = '%s:%s'%(NS,ctrl)
		keys = mc.keyframe(ctrlName,q=1,timeChange=1)
		if keys != None:
			rLegCtrlKeyList = set(list(rLegCtrlKeyList) + list(keys))
 

	for ctrl in lArmCtrlList:
		ctrlName = '%s:%s'%(NS,ctrl)
		keys = mc.keyframe(ctrlName,q=1,timeChange=1)
		if keys != None:
			lArmCtrlKeyList = set(list(lArmCtrlKeyList) + list(keys))

	for ctrl in rArmCtrlList:
		ctrlName = '%s:%s'%(NS,ctrl)
		keys = mc.keyframe(ctrlName,q=1,timeChange=1)
		if keys != None:
			rArmCtrlKeyList = set(list(rArmCtrlKeyList) + list(keys)) 

	ctrlKeyList = set(list(rLegCtrlKeyList)+list(lLegCtrlKeyList)+list(rArmCtrlKeyList)+list(lArmCtrlKeyList))

	print ctrlKeyList
	
	for key in ctrlKeyList:
		mc.currentTime(key,e=1)

		if key in lLegCtrlKeyList:
			if mc.getAttr(lLegSwitchName+'.fkIk')== 1:
				mc.setAttr(lLegSwitchName+'.fkIk',0)
			mc.select(lLegSwitchName)
			match.run()
			for IKCon in lLegIKCtrlList:
				IKCtrl = '%s:%s'%(NS,IKCon) 
				mc.setKeyframe(IKCtrl,s=0)

		if key in rLegCtrlKeyList:
			if mc.getAttr(rLegSwitchName+'.fkIk')== 1:
				mc.setAttr(rLegSwitchName+'.fkIk',0)             
			mc.select(rLegSwitchName)
			match.run()
			for IKCon in rLegIKCtrlList:
				IKCtrl = '%s:%s'%(NS,IKCon) 
				mc.setKeyframe(IKCtrl,s=0)

		if key in lArmCtrlKeyList:
			if mc.getAttr(lArmSwitchName+'.fkIk')== 1:
				mc.setAttr(lArmSwitchName+'.fkIk',0)
			mc.select(lArmSwitchName)
			match.run()
			for IKCon in lArmIKCtrlList:
				IKCtrl = '%s:%s'%(NS,IKCon) 
				mc.setKeyframe(IKCtrl,s=0)

		if key in rArmCtrlKeyList:
			if mc.getAttr(rArmSwitchName+'.fkIk')== 1:
				mc.setAttr(rArmSwitchName+'.fkIk',0)             
			mc.select(rArmSwitchName)
			match.run()
			for IKCon in rArmIKCtrlList:
				IKCtrl = '%s:%s'%(NS,IKCon) 
				mc.setKeyframe(IKCtrl,s=0)

	
