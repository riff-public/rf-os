import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mm
import os
import json
import pickle

from mocapData import ioData as iod
reload(iod)

from mocapData import poseData as posd
reload(posd)

from mocapData import ctrlData as ctrld
reload(ctrld)

from mocapData import mocapData
reload(mocapData)

from mocapData import hikList as hikl
reload(hikl)
from rf_utils import admin
reload(admin)
from rf_utils.pipeline import file_info 
reload(file_info)
from rf_utils.pipeline import create_asset
reload(create_asset)
from rf_utils.context import context_info
reload(context_info)
from rf_utils.sg import sg_process
reload(sg_process)
from rf_utils import file_utils
reload(file_utils)
if not mc.pluginInfo('mayaHIK',loaded=1,q=1):
	mc.loadPlugin('mayaHIK')

class mocapRig(object):
	def __init__(self):
		self.winName = 'mocapRig'
		self.ns = ''
		self.joint_scroll_data_list =  []
		self.ctrl_scroll_data_list = []
	
	def adjColumn(self,*args):
		tabIdx = mc.tabLayout('tmpSetupTab',q=1,sti=1)
		
		if tabIdx ==1:
			mc.tabLayout('tmpSetupTab',e=1, w=580,h=500)
			mc.columnLayout('mainCol',e=1, w=580,h=500)
			mc.columnLayout('tmpColumn',co= ['left',20],e=1,w=580)
			mc.window(self.winName,e=1, w=580,h=500)
			
			
			mc.nodeIconButton('loadTmp',w=260,e=1)
			mc.nodeIconButton('saveTmp',w=260,e=1)
			mc.nodeIconButton('browseFile',w=535,e=1)		
		
		elif tabIdx ==2:
			mc.tabLayout('tmpSetupTab',e=1, w=640, h = 340)
			mc.columnLayout('mainCol',e=1, w=640, h = 340)
			mc.columnLayout('tmpColumn',co= ['left',10],e=1,w=640)
			mc.window(self.winName,e=1, w=640, h = 340)

			
			mc.nodeIconButton('loadTmp',w=300,e=1)
			mc.nodeIconButton('saveTmp',w=300,e=1)
			mc.nodeIconButton('browseFile',w=615,e=1)	

		elif tabIdx ==3:
			mc.tabLayout('tmpSetupTab',e=1, w=400, h = 500)
			mc.columnLayout('mainCol',e=1, w=400, h = 500)
			mc.columnLayout('tmpColumn',co= ['left',15],e=1,w=400)
			mc.window(self.winName,e=1, w=400, h = 500)

			
			mc.nodeIconButton('loadTmp',w=180,e=1)
			mc.nodeIconButton('saveTmp',w=180,e=1)
			mc.nodeIconButton('browseFile',w=375,e=1)	
	
	def template_extend(self,*args):
		if not mc.frameLayout('tmpLayout',q=1,cl=1):
			h = mc.window(self.winName,q=1,h=1) +180
		else:
			h = mc.window(self.winName,q=1,h=1)-180
		mc.window(self.winName,e=1,h=h)

	def get_folder(self,*args):
		data_folder = mc.fileDialog2(ds=1,cap= 'Select Mocap Data File.',fileMode=3)
		if data_folder:
			check_folder(data_folder[0])
	

	def check_folder(self, folder_path=''):
		mc.textField('file_text',e=1,tx = folder_path)
		if os.path.isdir(folder_path):
			mc.textField('file_text',e=1 , bgc = [.5,1,.5])
		else:
			mc.textField('file_text',e=1,bgc=[1,.5,.5])


	def get_project_asset(self,path):
		self.project = path.split('/')[1]
		self.asset = path.split('/')[5]

		return self.project,self.asset

	def get_ns(self,*args):
		character = pmc.selected()[0]
		self.ns = pmc.selected()[0].namespace()
		mc.textField('ns',e=1,tx=self.ns,bgc=[.5,1,.5])
		#self.fld_path = mocapData.get_project_from_scene()

		ref_file = character.referenceFile().path
		f = file_info.File(ref_file)
		path = f.redirect()

		tmp_mocap_context =  context_info.PathToContext(path = path)
		self.project = tmp_mocap_context.project
		self.asset = tmp_mocap_context.entity_name

		tmp_asset_context = create_asset.get_context(self.project,self.asset,'char','all')
		asset_context = context_info.ContextPathInfo(context = tmp_asset_context)
		asset_context.context.update(step ='mocap')

		asset_mocap_path = asset_context.path.scheme(key='publishStepPath').abs_path()
		self.char_folder_path = asset_context.path.scheme(key='publishAssetTypePath').abs_path()
		self.asset_data_path = os.path.join(asset_mocap_path,'data').replace('\\','/')
		self.tPose_path = os.path.join(self.asset_data_path,'tpose').replace('\\','/')
		self.bindPose_path = os.path.join(self.asset_data_path,'bindPose').replace('\\','/')
		self.char_path = os.path.join(self.asset_data_path,'char_list').replace('\\','/')
		self.fkIkDict_path = os.path.join(self.asset_data_path,'fkIkDict').replace('\\','/')

		print self.asset_data_path
		print self.fkIkDict_path

		if self.check_temp_folder():
			mc.nodeIconButton('loadTmp',e=1,en=1)

		self.load_char_list()
		self.load_snap_asset()
		self.convert_list_to_scroll()
		mc.textField('tpose_text_field', tx='{}*_Ctrl, *_Ctrl'.format(self.ns),e=1)

# -------------------------------------------------------- template part ----------------------------------------------------#

	def check_temp_folder(self,*args):
		tmp_mocap_asset = create_asset.get_context(self.project,'mocap','char','all')
		asset_context = context_info.ContextPathInfo(context = tmp_mocap_asset)
		asset_context.context.update(step ='mocap')

		self.tmp_mocap_work = asset_context.path.scheme(key='workAssetNamePath').abs_path()
		self.tmp_mocap_path = asset_context.path.scheme(key='publishStepPath').abs_path()
		self.tmp_mocap_data_path = os.path.join(self.tmp_mocap_path,'data').replace('\\','/')
		self.tmp_mocap_char_path = os.path.join(self.tmp_mocap_data_path,'char_list').replace('\\','/')
		self.tmp_mocap_fkIk_path = os.path.join(self.tmp_mocap_data_path,'fkIkDict').replace('\\','/')
		print self.tmp_mocap_char_path
		print self.tmp_mocap_data_path
		print self.tmp_mocap_path
		if os.path.isdir(self.tmp_mocap_data_path):
			return True
		else:
			return False
		
	def create_mocap_asset(self):
		create_asset.create(str(self.project), 'mocap', 'char', assetSubtype='main', mainAsset='', tags='', template='default', setEntity=[], link=True) 

	def create_mocap_data_dir(self):
		admin.makedirs(self.tmp_mocap_data_path)

	def load_template(self,*args):
		with open(self.tmp_mocap_char_path,'r') as f:
			self.char_list = json.load(f)
		self.convert_list_to_scroll()	

		with open(self.tmp_mocap_fkIk_path,'r') as f:
			self.fkikDict = json.load(f)
			self.fkIkDict_to_ui()

	def save_template(self,*args):
		# confirmation to save template
		
		if not os.path.isdir(self.tmp_mocap_data_path):
			confirm = mc.confirmDialog(m = 'Project template folder has not created yet.\nCreate a template Folder ?' ,
			 button = ['create','cancel'])
			print confirm
			
			if confirm =='create':
				self.create_mocap_data_dir()
			else:
				return

		if os.path.isfile(self.tmp_mocap_char_path):
			overwrite = mc.confirmDialog(m = 'The mocap data is already exists.\nDo you want to overwrite it ?' ,
			 button = ['confirm','cancel'])
		else:
			overwrite = mc.confirmDialog(m = 'The mocap data is not exists.\nDo you want to create data ?' ,
			 button = ['confirm','cancel'])

		if overwrite =='confirm':

			self.convert_scroll_to_list()
			with open(self.tmp_mocap_char_path,'w') as f:
				json.dump(self.char_list, f,indent=4)

			with open(self.tmp_mocap_fkIk_path,'w') as f:
				json.dump(self.fkikDict, f,indent=4)
		
		if self.check_temp_folder():
			mc.nodeIconButton('loadTmp',e=1,en=1)
		
# ------------------------------------------------------------------------------char list function--------------------------------------------------------------------------
	# load char list function
	def load_char_list(self,*args):
		if os.path.exists(self.char_path):
			with open(self.char_path, 'r') as f:
				self.char_list = json.load(f)
		else:
			with open(os.path.join(os.path.dirname(__file__),'mocapData','char_list'),'r') as f:
				self.char_list = json.load(f)
		
		self.convert_list_to_scroll()

	# load default char list function
	def load_default_char_list(self,*args):
		with open(os.path.join(os.path.dirname(__file__),'mocapData','char_list'),'r') as f:
			self.char_list = json.load(f)
		self.convert_list_to_scroll()

	def load_from_file(self,*args):
		folder_path = mc.fileDialog2(ds=1,cap= 'Select Mocap Data Folder.',fileMode=3 , dir = self.char_folder_path)
		if folder_path != None:
			data_path = os.path.join(folder_path[0],'mocap','data').replace('\\','/')

			char_path = os.path.join(data_path,'char_list').replace('\\','/')
			fkIkDict_path = os.path.join(data_path,'fkIkDict').replace('\\','/')

			if os.path.isfile(char_path) and os.path.isfile(fkIkDict_path):
				try:
					with open(char_path,'r') as f:
						self.char_list = json.load(f)
						self.convert_list_to_scroll()

					with open(fkIkDict_path,'r') as f:
						self.fkikDict = json.load(f)
						self.fkIkDict_to_ui()

					mc.nodeIconButton('loadTmp',e=1,en=1)

				except:
					mc.confirmDialog(m = 'There is something wrong\n with data in mocap folder.')
					return
			else:
				mc.confirmDialog(m = 'This folder does not has mocap data.')
				return

	# save char list function 
	def save_char_list(self):
		self.convert_scroll_to_list()
		if not os.path.isdir(self.asset_data_path):
			#os.mkdir(self.asset_data_path)
			admin.makedirs(self.asset_data_path)

		with open(self.char_path,'w') as f:
			print 'finish_save_char'
			json.dump(self.char_list, f)

	# scrollist function
	def convert_list_to_scroll(self):
		self.joint_scroll_data_list =  []
		self.ctrl_scroll_data_list = []

		for part_data in self.char_list:
			part = part_data[0]
			joint_data = part_data[2]
			ctrl_data = part_data[3]
			self.joint_scroll_data_list.append('{}  :   {}'.format(part,joint_data))
			self.ctrl_scroll_data_list.append('{}  :   {}'.format(part,ctrl_data))

		mc.textScrollList('jnt_scroll', e=1, ra=1)
		mc.textScrollList('jnt_scroll', e=1, a = self.joint_scroll_data_list)

		mc.textScrollList('ctrl_scroll', e=1, ra=1)
		mc.textScrollList('ctrl_scroll', e=1, a = self.ctrl_scroll_data_list)

	def convert_scroll_to_list(self,*args):
		self.joint_scroll_data_list = mc.textScrollList('jnt_scroll', q=1, ai=1)
		self.ctrl_scroll_data_list = mc.textScrollList('ctrl_scroll', q=1, ai=1)
		
		for num_process in range(len(self.joint_scroll_data_list)):
			self.char_list[num_process][2]= self.joint_scroll_data_list[num_process].split('  :   ')[-1]
			self.char_list[num_process][3]= self.ctrl_scroll_data_list[num_process].split('  :   ')[-1]

		self.convert_list_to_scroll()
	
	# replace function
	def edit_obj_from_scroll(self, obj , text_scroll):
		#replace select idx
		idxs = mc.textScrollList(text_scroll, q=1, sii=1)
		if idxs:
			for idx in idxs:
				if text_scroll == 'jnt_scroll':
					self.char_list[idx-1][2] = obj

				if text_scroll == 'ctrl_scroll':
					self.char_list[idx-1][3] = obj

			self.convert_list_to_scroll()
		mc.textScrollList(text_scroll,e=1,sii=idx)

	def remove_obj_from_scroll(self, text_scroll):
		self.edit_obj_from_scroll('' , text_scroll)

	def edit_joint_scroll(self,*args):
		sel_obj = pmc.ls(sl=True)
		if sel_obj:
			obj = sel_obj[0].stripNamespace().__str__()
			self.edit_obj_from_scroll(obj , 'jnt_scroll')

	def edit_ctrl_scroll(self,*args):
		sel_obj = pmc.ls(sl=True)
		if sel_obj:
			obj = sel_obj[0].stripNamespace().__str__()
			self.edit_obj_from_scroll(obj , 'ctrl_scroll')	

	def remove_joint_scroll(self,*args):
		self.remove_obj_from_scroll('jnt_scroll')

	def remove_ctrl_scroll(self,*args):
		self.remove_obj_from_scroll('ctrl_scroll')	

	# # use replace function as ''
	# def write_tPose(self,*args):
	# 	obj_str = mc.textField('tpose_text_field',q=1,tx=1)
	# 	self.obj_list = obj_str.split(',')
	# 	mocapData.write_tpose(self.proj, self.asset ,self.tPose_file_name, mc.ls(self.obj_list))
	# 	print 'write tPose complete'

	# def read_tPose(self,*args):
	# 	mocapData.read_tpose(self.proj, self.asset, self.ns)
	# 	print 'read tPose complete'


	# use replace function as ''
	def write_tPose(self,*args):
		obj_str = mc.textField('tpose_text_field',q=1,tx=1)
		obj_list = obj_str.split(',')
		self.obj_list = mc.ls(obj_list)
		#mocapData.write_tpose(self.proj, self.asset ,self.tPose_file_name, mc.ls(self.obj_list))
		posd.write_pose(self.obj_list, file_name='tpose', fld_path = self.asset_data_path)
		print 'write tPose complete'

	def read_tPose(self,*args):
		#mocapData.read_tpose(self.proj, self.asset, self.ns)
		posd.read_pose(file_path = self.tPose_path , ns = self.ns)
		print 'read tPose complete'

	def browse_tPose(self,*args):
		file_dialog = mc.fileDialog2(ds=1,fileMode=3,dir =  self.char_folder_path)
		if file_dialog:
			print file_dialog
			tPose_file = os.path.join( file_dialog[0],'mocap/data/tpose' )
			print tPose_file
			if os.path.isfile(tPose_file):
				print 'doTPose'
				posd.read_pose(file_path = tPose_file , ns = self.ns)
			else:
				print 'tPose not found'
				mc.confirmDialog(m = 'TPose not found')

	def write_bindPose(self,*args):
		obj_str = mc.textField('tpose_text_field',q=1,tx=1)
		obj_list = obj_str.split(',')
		self.obj_list = mc.ls(obj_list)
		#mocapData.write_tpose(self.proj, self.asset ,self.tPose_file_name, mc.ls(self.obj_list))
		posd.write_pose(self.obj_list, file_name='bindPose', fld_path = self.asset_data_path)
		print 'write bindPose complete'
	
	def read_bindPose(self,*args):
		#mocapData.read_tpose(self.proj, self.asset, self.ns)
		posd.read_pose(file_path = self.bindPose_path , ns = self.ns)
		print 'read bindPose complete'

	def check_file(self,*args):
		mc.textField('file_field',e=1,tx=self.ns,bgc=[.5,1,.5])

	def find_obj(self,text_scroll):
		strs  = mc.textScrollList(text_scroll, q=1,si = 1 )
		if strs:
			obj = strs[0].split('  :   ')[-1] 
			pmc.select('{}{}'.format(self.ns,obj))

	def find_joint(self,*args):
		self.find_obj('jnt_scroll')

	def find_ctrl(self,*args):
		self.find_obj('ctrl_scroll')


#-------------------------------------------------------------  snap  fkik  ------------------------------------------------------------------------------

	def add_select_obj(self,text_field, obj_list):
		add_list = []
		if obj_list and obj_list != None:
			for obj in obj_list:
				pymel_obj = pmc.PyNode(obj)
				if pymel_obj.namespace():
					add_obj = pymel_obj.stripNamespace().__str__()
					add_list.append(add_obj)
				else:
					add_list.append(obj)
			mc.textField(text_field,e=1,text=' , '.join(add_list))
		else:
			pass

	def add_skin_field(self,*args):
		obj_list = mc.ls(sl=True)
		self.add_select_obj('skin_field',obj_list)

	def add_fk_ctrl_field(self,*args):
		obj_list = mc.ls(sl=True)
		self.add_select_obj('fk_field',obj_list)
	
	def add_ik_ctrl_field(self,*args):
		obj_list = mc.ls(sl=True)
		self.add_select_obj('ik_field',obj_list)
	
	def add_switch_ctrl_field(self,*args):
		obj_list = mc.ls(sl=True)
		self.add_select_obj('ctrl_field',obj_list)
	
	def add_list_obj(self,text_field, lists):
		mc.textField(text_field,e=1,text=' , '.join(lists))

	def add_str_obj(self,text_field, texts):
		mc.textField(text_field,e=1,text= texts)

	def field_to_list(self,text_field):
		txtField = mc.textField(text_field,q=1,text=1)
		lists = txtField.split(' , ')
		return lists


	def add_part_key(self,*args):
		result = mc.promptDialog(
						title='add part',
						message='insert new part:',
						button=['OK', 'Cancel'],
						defaultButton='OK',
						cancelButton='Cancel',
						dismissString='Cancel')
		if result == 'OK':
			text = mc.promptDialog(query=True, text=True)

		mc.textScrollList('snap_scroll',e=1,a=text)
		mc.textScrollList('snap_scroll',e=1,si= text)
		
		skinJnt_field =mc.textField('skin_field', tx='',e=1 )
		fk_field =mc.textField('fk_field', tx='',e=1)
		ik_field =mc.textField('ik_field', tx='',e=1 )
		ctrl_field =mc.textField('ctrl_field', tx='',e=1)
		fkIk_field =mc.textField('attr_field', tx='',e=1)
		def_fk_field =mc.textField('def_fk_field', tx='0',e=1)

	def delete_part_key(self,*args):
		sel_idx = mc.textScrollList('snap_scroll',q=1,sii=1)
		for idx in sel_idx:
			mc.textScrollList('snap_scroll',e=1,rii= idx)

	def load_snap_from_file(self):
		data = mc.fileDialog2(ds=1,cap= 'Select Mocap Data File.',fileMode=1)
		if data:
			with open(data[0],'r') as f:
				self.fkikDict = json.load(f)
			#self.convert_list_to_scroll()
			self.fkIkDict_to_ui()

	def load_snap_default(self):
		with open(os.path.join(os.path.dirname(__file__),'mocapData','fkIkDict'),'r'.replace('\\','/')) as f:
			self.fkikDict = json.load(f)
		#self.convert_list_to_scroll()
		self.fkIkDict_to_ui()

	def load_snap_asset(self):
		if os.path.exists(self.fkIkDict_path):
			with open(self.fkIkDict_path, 'r') as f:
				self.fkikDict = json.load(f)
		else:
			with open(os.path.join(os.path.dirname(__file__),'mocapData','fkIkDict').replace('\\','/'),'r') as f:
				self.fkikDict = json.load(f)
		self.fkIkDict_to_ui()
	
	def write_snap(self):
		if not os.path.isdir(self.asset_data_path):
			admin.makedirs(self.asset_data_path)

		with open(self.fkIkDict_path,'w') as f:
			json.dump(self.fkikDict, f,indent=4)

	def fkIkDict_to_ui(self,*args):
		key = sorted(self.fkikDict.keys())
		mc.textScrollList('snap_scroll',e=1,ra=1)
		mc.textScrollList('snap_scroll',e=1,a=key)
		
		mc.textScrollList('snap_scroll',e=1,sii=1)
		self.key_to_txtField()
	
	def key_to_txtField(self,*args):
		key = mc.textScrollList('snap_scroll',q=1,si=1)
		if key:
			self.add_list_obj('skin_field', self.fkikDict[key[0]]["skin_jnt"])
			self.add_list_obj('fk_field', self.fkikDict[key[0]]["fk_ctrl"])
			self.add_list_obj('ik_field', self.fkikDict[key[0]]["ik_ctrl"])
			self.add_str_obj('ctrl_field', self.fkikDict[key[0]]["ctrl"])
			self.add_str_obj('attr_field', self.fkikDict[key[0]]["attr"])
			self.add_str_obj('def_fk_field', self.fkikDict[key[0]]["fk_default"])

	def update_key(self,*args):
		key = mc.textScrollList('snap_scroll',q=1,si=1)
		if key:
			self.fkikDict[key[0]]["skin_jnt"] = self.field_to_list('skin_field')
			self.fkikDict[key[0]]["fk_ctrl"] = self.field_to_list('fk_field')
			self.fkikDict[key[0]]["ik_ctrl"] = self.field_to_list('ik_field')
			self.fkikDict[key[0]]["ctrl"] = mc.textField('ctrl_field',q=1,tx=1)
			self.fkikDict[key[0]]["attr"] = mc.textField('attr_field',q=1,tx=1)
			self.fkikDict[key[0]]["fk_default"] = mc.textField('def_fk_field',q=1,tx=1)


#--------------------------------------------------------- ui part ----------------------------------------------------------#

	# UI Layout Part
	def show_ui(self):
		if mc.window(self.winName,exists=1):
			mc.deleteUI(self.winName)
		
		ui = mc.window(self.winName,title = 'mocapRig',w=600)

		# main layout
		mainLayout = mc.columnLayout('mainCol',cal='center',co=['both',15],w=600,adj=1)
		nsColumn = mc.columnLayout('nsColumn',cal='center',adj=1)
		mc.separator(h=10, style ='none')
		mc.text('Please fill namespace of character.')
		ns_text_field =mc.textField('ns',w=200,ed=0,bgc=[1,.5,.5],ip = 0)
		mc.separator(h=5,style ='none')
		mc.button('getNS',l='get namespace' , c= self.get_ns)
		mc.separator(h=5,style ='none')
		
		template_layout = mc.frameLayout('tmpLayout',label = 'Template for project', cl=1, cll=1 ,cc = self.template_extend)
		mc.columnLayout('tmpColumn',co= ['left',20],bgc = [.225,.225,.225])
		mc.separator(style ='none', h = 10)
		mc.rowLayout('resetRow2',nc=3)		#mc.separator(style ='none', w = 10)
		mc.nodeIconButton('loadTmp',l='Load Project Template',w=250,h=40 ,mw=20, st =  "iconAndTextHorizontal" ,
		i = 'openScript.png',bgc = [0.35,0.35,0.35] , c =self.load_template,en=0)
		mc.separator(style ='none', w = 10)
		mc.nodeIconButton('saveTmp',l='Save Project Template',w=250,h=40,mw=20 , st =  "iconAndTextHorizontal" ,  
		i = 'save.png',bgc = [0.35,0.35,0.35], c =self.save_template)
		mc.setParent('tmpColumn')
		mc.separator(style ='none', h = 8)
		mc.nodeIconButton('browseFile',l='Browse from Folder',w=515,h=35 ,st =  "iconAndTextHorizontal" ,  i = 'zoom.png' ,c = self.load_from_file,bgc = [0.35,0.35,0.35])
		mc.separator(style ='none', w = 25)
		mc.separator(h=12,style='singleDash')
		mc.setParent(template_layout)
		

		# file_field =mc.textField('file_text',w=200,ed=0,ip = 0,bgc=[1,.5,.5])
		# mc.separator(h=5,style ='none')
		# mc.button('browse',l='browse mocap folder' , c= self.get_folder)
		mc.setParent(mainLayout)
		mc.separator(h=14, style ='none')
		tab_layout = mc.tabLayout('tmpSetupTab', p=mainLayout, cr=1, w=560 , sc=self.adjColumn)

		# first layout
		mc.setParent(tab_layout)
		mocap_tab = mc.columnLayout('mocap_tab', cal='center', co=['both',15], w=570, adj=1)
		mocap_row_layout = mc.rowLayout(numberOfColumns=7)
		#mc.separator(w = 45,h=12, style='none')
		
		# jnt_column
		jnt_column = mc.columnLayout('jnt_column', cal='center', w=250, adj=1)
		mc.separator(h=6, style ='none')
		mc.text('jnt_text', l='Joint')
		mc.separator(h=3, style ='none')
		jnt_add_rem_column = mc.columnLayout('jnt_add_rem_column', cal='center', adj=1)
		jnt_add_rem_row = mc.rowLayout('jnt_add_rem_row', nc=2, w=180)
		mc.button('jnt_replace_button', l='replace', w=125 , c= self.edit_joint_scroll)
		mc.button('jnt_remove_button', l='remove', w=125 , c= self.remove_joint_scroll)
		mc.setParent(jnt_column)
		mc.separator(h=3, style ='none')
		mc.textScrollList('jnt_scroll', w=250, h=300, allowMultiSelection=1, shi=1 )
		mc.separator(h=6, style='none')
		mc.button('jnt_find',l='Find', w=100 , c= self.find_joint)

		jnt_order_column = mc.columnLayout('jnt_order_column', cal='center', adj=1)
		jnt_order_row = mc.rowLayout('jnt_order_row', nc=2, w=200)
		mc.setParent(jnt_column)
		mc.separator(h=3, style='none')
		mc.setParent(jnt_column)
		mc.setParent(mocap_row_layout)
		mc.separator(w=30, style='none',h=20)

		# ctrl_column
		mc.setParent(mocap_row_layout)
		ctrl_column = mc.columnLayout('ctrl_column', cal='center', w=250, adj=1)
		mc.separator(h=6, style ='none')
		mc.text('ctrl_text', l='Ctrl')
		mc.separator(h=3, style ='none')
		ctrl_add_rem_column = mc.columnLayout('ctrl_add_rem_column', cal='center', adj=1)
		ctrl_add_reomve_row = mc.rowLayout('ctrl_add_reomve_row', nc=2, w=180)
		mc.button('ctrl_replace_button', l='replace', w=125, c= self.edit_ctrl_scroll)
		mc.button('ctrl_remove_button', l='remove', w=125, c = self.remove_ctrl_scroll)
		mc.setParent(ctrl_column)
		mc.separator(h=3,style ='none')
		mc.textScrollList('ctrl_scroll',w=250,h=300,allowMultiSelection=1,shi=1 )
		mc.separator(h=6, style='none')
		mc.button('ctrl_find',l='Find',w=100 , c = self.find_ctrl)

		ctrl_column = mc.columnLayout('ctrl_column',cal='center',adj=1)
		ctrl_row = mc.rowLayout('ctrl_row',nc=2,w=200)
		mc.setParent(ctrl_column)
		mc.separator(h=3,style='none')
		mc.setParent(ctrl_column)
		# Foot
		mc.setParent(mocap_row_layout)
		mc.separator(h=8,style='none')

		mc.setParent(mocap_tab)

		mc.separator(w=25,style='none')
		mc.separator(h=10,style ='singleDash',w=200)
		mc.separator(h=6,style ='none')

		resetRow = mc.rowLayout('resetRow',nc=10)
		
		#mc.separator(style ='none', w = 10)
		mc.nodeIconButton('hik_map_load',l='Load Asset',w=250,h=40,mw=100 , st =  "iconAndTextHorizontal" ,  i = 'openScript.png', c=self.load_char_list)

		mc.separator(style ='none', w = 30)
		mc.nodeIconButton('hik_map_write',l='Save Asset',w=250,h=40,mw=100 , st =  "iconAndTextHorizontal" ,  i = 'save.png', c= self.save_char_list)

		mc.setParent(mocap_tab)
		mc.separator(h=5,style = 'none')


		mc.setParent(mainLayout)
		mc.separator(h=20,style ='none')
		# fkik_snap
		mc.setParent(tab_layout)
		fkIk = mc.columnLayout('fkik_tab', cal='center', co=['both',15], w=300, adj=1)
		fkIk_row = mc.rowLayout('fk_ik_low',numberOfColumns=3)
		
		part_cloumn = mc.columnLayout('part_cloumn', cal='center', adj=1)
		mc.separator(h=6, style ='none')
		mc.text('snap_text', l='Snap Part')
		mc.separator(h=3, style ='none')
		snap_rem_column = mc.columnLayout('snap_rem_column', cal='center', adj=1)
		mc.setParent(part_cloumn)
		mc.separator(h=3, style ='none')
		mc.textScrollList('snap_scroll', w=125, h=150, allowMultiSelection=1, shi=1 ,a = ['leftLeg','rightLeg','leftArm','rightArm'],sc=self.key_to_txtField)
		mc.separator(h=6, style='none')
		snap_add_rem_row = mc.rowLayout('snap_add_rem_row', nc=2, w=180)
		mc.button('snap add button', l='add', w=90 ,c= self.add_part_key )
		mc.button('snap remove button', l='delete', w=90,c=self.delete_part_key)
		

		mc.setParent(fkIk_row)
		mc.separator(w=15,style ='none')
		mc.setParent(fkIk_row)
		all_part_layout = mc.columnLayout('all_part_layout')
		mc.separator(h=30,style ='none')
		part_value_row = mc.rowLayout('part_value_row',numberOfColumns = 5)
		title_column = mc.columnLayout('fkik_column', cal='left', adj=1)
		mc.text('skin joint :', h=25)
		mc.separator(h=2,style='none')
		mc.text('fk ctrl:', h=25)
		mc.separator(h=2,style='none')
		mc.text('ik ctrl:', h=25)
		mc.separator(h=2,style='none')
		mc.text('switch ctrl:', h=25)
		mc.separator(h=2,style='none')
		mc.text('fk/ik attr:', h=25)
		mc.separator(h=2,style='none')
		mc.text('default fk:', h=25)

		mc.setParent(part_value_row)
		mc.separator(w=5)

		textField_column = mc.columnLayout('fkik_text_column', cal='center', adj=1)
		skinJnt_field =mc.textField('skin_field', tx='first_skin_jnt , second_skin_joint, third_skin_joint' , w=270, ip = 0, h=25)
		mc.separator(h=2,style='none')
		fk_field =mc.textField('fk_field', tx='first_fk_ctrl , second_fk_ctrl , third_fk_ctrl' , w=270, ip = 0, h=25)
		mc.separator(h=2,style='none')
		ik_field =mc.textField('ik_field', tx='first_ik_ctrl , poleVector_ik_ctrl , tip_ik_ctrl' , w=270, ip = 0, h=25)
		mc.separator(h=2,style='none')
		ctrl_field =mc.textField('ctrl_field', tx='swtich_ctrl' , w=270, ip = 0, h=25)
		mc.separator(h=2,style='none')
		fkIk_field =mc.textField('attr_field', tx='fkik' , w=270, ip = 0, h=25)
		mc.separator(h=2,style='none')
		def_fk_field =mc.textField('def_fk_field', tx='0' , w=270, ip = 0, h=25)


		mc.setParent(part_value_row)
		mc.separator(w=5)
		button_column = mc.columnLayout('fkik_button_column',cal ='center',adj=1)
		#mc.separator(h=5,style ='none')
		#mc.text('define skeleton , fk/ik ctrl , switch attr , attrName')
		mc.button('skin_button',w=40, h=25,l = '+' , c =self.add_skin_field)
		mc.separator(h=2,style='none')
		mc.button('fk_button',w=40, h=25,l = '+', c =self.add_fk_ctrl_field)
		mc.separator(h=2,style='none')
		mc.button('ik_button',w=40, h=25,l = '+', c =self.add_ik_ctrl_field)
		mc.separator(h=2,style='none')
		mc.button('ctrl_button',w=40, h=25,l = '+', c =self.add_switch_ctrl_field)
		mc.separator(h=2,style='none')
		mc.separator(h=25,style='none')
		mc.separator(h=2,style='none')
		mc.separator(h=25,style='none')
		# mc.button('attr_button',w=40, h=25,l = '+', c =self.add_attr_field_field)

		mc.setParent(all_part_layout)
		mc.separator(h=10)
		update_value_row = mc.rowLayout('update_value',numberOfColumns = 4)
		mc.separator(w=175,style = 'none')
		mc.button('reload_part' , l = 'reload',w=75 , c= self.key_to_txtField)
		mc.separator(w=6,style = 'none')
		mc.button('update_part', l = 'update',w=75 , c=self.update_key)

		mc.setParent(fkIk)
		mc.separator(h=10,style ='none')
		
		# mc.separator(style ='none', w = 10)
		# mc.nodeIconButton('write_fk_ik',l='write tPose',w=120,h=60 ,  st =  "iconAndTextVertical" ,i='poseEditor.png', c = self.write_tPose)

		mc.separator(h=5,style ='singleDash')
		mc.separator(h=10,style ='none')
		fkIk_file_row = mc.rowLayout('fkIk_file_row',nc=10)
		##mc.nodeIconButton('fkik_load',l='load from file',w=135,h=50, st =  "iconAndTextVertical" ,  i = 'openScript.png' ,c = self.load_snap_from_file )	
		
		#mc.separator(style ='none', w = 10)
		#mc.nodeIconButton('fkik_default_load',l='default template',w=135,h=50 , st =  "iconAndTextVertical" ,  i = 'openScript.png' , c = self.load_snap_default)

		#mc.separator(style ='none', w = 10)
		mc.nodeIconButton('fkik_asset_load',l='load by asset',w=300,h=40, mw=20 , st =  "iconAndTextHorizontal" ,  i = 'openScript.png', c=self.load_snap_asset)

		mc.separator(style ='none', w = 10)
		mc.nodeIconButton('fkik_write',l='save',w=300,h=40 , mw=20 ,st =  "iconAndTextHorizontal" ,  i = 'save.png', c= self.write_snap)

		mc.setParent(mocap_tab)
		mc.separator(h=5,style ='none')

		mc.setParent(mainLayout)


		# TPoseTab
		mc.setParent(tab_layout)
		tPose_tab = mc.columnLayout('tpose_tab', cal='center', co=['both',15], w=300, adj=1)
		tpose_column = mc.columnLayout('tpose_column', cal='center', w=200, adj=1)
		mc.separator(h=5,style ='none')
		mc.text('Type suffix of ctrl to write tPose Data.')
		mc.separator(h=5,style ='none')
		tpose_text_field =mc.textField('tpose_text_field', tx='*:*_Ctrl, *_Ctrl' , w=200, ip = 0)
		mc.separator(h=5,style ='none')
		mc.separator(h=6, style='none')
		
		mc.text('TPose')
		mc.separator(style ='none',h =3 ,w = 10)
		mc.nodeIconButton('hik_write_bindPose',l='write tPose',w=120,h=60 ,  st =  "iconAndTextVertical" ,i='save.png' , c = self.write_tPose)
		mc.separator(style ='none',h =3, w = 10)
		mc.nodeIconButton('hik_read_tpose',l='read tPose',w=120,h=60 ,  st =  "iconAndTextVertical" ,i='poseEditor.png', c = self.read_tPose)	
		mc.separator(style ='none',h =3, w = 10)
		mc.nodeIconButton('hik_read_tpose',l='read tPose',w=120,h=60 ,  st =  "iconAndTextVertical" ,i='zoom.png', c = self.browse_tPose)
		mc.separator(style = 'none',h=10)	

		mc.text('BindPose')
		mc.separator(style ='none',h =3 ,w = 10)
		mc.nodeIconButton('hik_write_bindPose',l='write bind pose',w=120,h=60 ,  st =  "iconAndTextVertical" ,i='save.png' , c = self.write_bindPose)

		mc.separator(style ='none',h =3, w = 10)
		mc.nodeIconButton('hik_read_tpose',l='load bindPose',w=120,h=60 ,  st =  "iconAndTextVertical" ,i='poseEditor.png', c = self.read_bindPose)

		# mc.separator(style ='none',h =3 ,w = 10)
		# mc.nodeIconButton('hik_read_tpose',l='read tPose',w=120,h=60 ,  st =  "iconAndTextVertical" ,i='poseEditor.png' , c = self.read_tPose)

		#print os.path.join(os.path.dirname(__file__),'mocapData','char_list')
		with open(os.path.join(os.path.dirname(__file__),'mocapData','char_list'),'r') as f:
			self.char_list = json.load(f)


		self.convert_list_to_scroll()


		mc.showWindow(self.winName)

		return self

# import mocapRigUI
# reload(mocapRigUI)

# mocapRigUI.mocapRig().show_ui()