import math
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mm
import os
import json
import pickle


def get_data_folder(*args):
	''' create data feild  '''
	scnPath = os.path.normpath(mc.file(q=True, l=True)[0])
	fldPath = os.path.split(scnPath)[0]
	
	for arg in args:
		fldPath = os.path.join(fldPath,arg)

		if not os.path.exists(fldPath):
			os.mkdir(fldPath)
	return fldPath

def write_json_data(data,file_path):
	with open(file_path, 'w') as attr_file:
		json.dump(data,attr_file,indent=4)

def read_json_data(file_path):
	with open(file_path, 'r') as attr_file:
		return json.load(attr_file)

def write_pickle_data(data, file_path):
	with open(file_path, 'w') as attr_file:
		pickle.dump(data,attr_file)

def read_pickle_data(file_path):
	with open(file_path, 'r') as attr_file:
		return pickle.load(attr_file)

def list_folder(file_path):
    dirs = []
    if os.path.exists(file_path): 
        dirs = [d for d in os.listdir(file_path) if os.path.isdir(os.path.join(file_path,d))]
    return dirs 

def list_file(file_path):
    files = []
    if os.path.exists(file_path): 
        files = [d for d in os.listdir(file_path) if os.path.isfile(os.path.join(file_path, d))]
    return files

def get_obj_file_name(obj , task = ''):
	name = pmc.PyNode(obj).stripNamespace().__str__()
	file_name = '%s_%s'%(name,task)
	return file_name

def importer(file_path =''):
	if file_path:
		mc.file( file_path, i = 1, iv=1 , pn=1)

def mul_import(file_pathlist = []):
	for file_path in file_pathlist:
		try:
			importer(file_path)
		except:
			mc.warning(m='error import "%s"'%file_path)

def export_seleceted(obj_list = [], file_name = '', file_path = '',file_type = 'MayaAscii',**kwargs):
	if file_path and file_name:
		mc.select(obj_list)
		file_path =  os.path.join(file_path,file_name)
		mc.file(file_path, exportSelected = 1, typ = file_type, **kwargs)


def export_all(file_path = '' ,file_type = 'MayaAscii' ,**kwargs):
	if file_path:
		mc.file(file_path, exportAll = 1, typ = file_type, **kwargs)

# fbx to anim function
def create_folder(path):
	if '\\' in path:
		path = path.replace('\\','/')
	print 'path: %s'%path

	for level in range(len(path.split('/'))):
		print level
		path_check = '/'.join(path.split('/')[:level+1]).replace('\\','/')
		print path_check
		if not os.path.isdir(path_check):
			print 'print create folder'
			os.mkdir(path_check)

	return path_check