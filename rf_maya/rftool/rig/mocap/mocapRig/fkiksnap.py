#fkIkDict = {}
import maya.OpenMaya as om
import maya.cmds as mc
import maya.OpenMayaAnim as oma
import copy

def get_polevector(start, mid, end, offsetPoleVec=2):
	x = lambda vec, loc : mc.move(vec.x, vec.y, vec.z, loc)
	pos = mc.xform(start, q=1, ws=1, t=True)
	a = om.MVector(pos[0], pos[1], pos[2])
	pos2 = mc.xform(end, q=1, ws=1, t=True)
	b= om.MVector(pos2[0], pos2[1], pos2[2])
	pos3 = mc.xform(mid, q=1, ws=1, t=True)
	c = om.MVector(pos3[0], pos3[1], pos3[2])
	i = b-a
	d= i* .5
	e = a+d
	f = c-e
	g = f *2
	h = e+g
	return (h.x, h.y, h.z)

def ik_to_fk( fk_ctrl_list , skin_joint_list,ctrl, attr ,fk_default):
	for num in range(len(skin_joint_list)):
		mat = mc.xform(skin_joint_list[num], q=1, ws=1, m=1)
		mc.xform(fk_ctrl_list[num] , ws=1, m=mat)
	if fk_default:
		mc.setAttr('%s.%s'%(ctrl,attr),0)
	elif fk_default:
		mc.setAttr('%s.%s'%(ctrl,attr),1)
	
def fk_to_ik( ik_ctrl_list, skin_joint_list ,ctrl, attr ,fk_default):
	# get tip
	tip_mat = mc.xform(skin_joint_list[-1],ws=1,m=1,q=1)
	mc.xform(ik_ctrl_list[-1],ws=1,m=tip_mat)
	
	# get base
	base_mat = mc.xform(skin_joint_list[0],ws=1,m=1,q=1)
	mc.xform(ik_ctrl_list[0],ws=1,m=base_mat)

	# ge pole vector
	t = get_polevector(skin_joint_list[0], skin_joint_list[1], skin_joint_list[2], 0)
	mc.xform(ik_ctrl_list[1],ws=1,t=t)
	
	# if fk_default:
	# 	mc.setAttr('%s.%s'%(ctrl,attr),0)
	# elif not fk_default:
	# 	mc.setAttr('%s.%s'%(ctrl,attr),1)

def create_fkik_snap_dict(swicth_ctrl = '', attr = 'fkIk', fk_list = [],ik_list = [],skin_list = [], fk_at = 0):
	snap_dict = {}

	snap_dict["ctrl"] = swicth_ctrl
	snap_dict["attr"] = attr
	snap_dict["fk_ctrl"] = fk_list
	snap_dict["ik_ctrl"] = ik_list
	snap_dict["skin_jnt"] = skin_list
	snap_dict["fk_at"] = fk_at

	return snap_dict


def key_anim_crv(anim_crv,time,value):
	sel = om.MSelectionList()
	sel.add('pCube1_translateX')
	mObj = om.MObject()
	sel.getDependNode(0,mObj)
	animCrv = oma.MFnAnimCurve(mObj)
	animCrv.addKeyframe(om.MTime(time),value)

def collect_fkik_dict(key_list= [],dict_list = []):
	if len(key_list) == len(dict_list):
		fkik_dict = {}
		for key in key_list:
			fkik_dict[key] = dict_list[key]
	return fkik_dict

def fk_to_Ik_all(ns, fkIk_dict):
	fkIk_dict = copy.deepcopy(fkIk_dict)
	#all_fk_ctrl =[]
	all_ik_ctrl =[]
	#all_skin_jnt =[]
	#all_attr =[]
	#all_fkDefault =[]
	kf = []
	for fkIk_key in fkIk_dict.keys():
		part_dict = fkIk_dict[fkIk_key]
		ns_fk_ctrl =[]
		ns_ik_ctrl =[]
		ns_skin_jnt =[]
		part_dict['ctrl'] = '%s%s'%(ns,part_dict['ctrl'])

		for num,fk_ctrl in enumerate(part_dict['fk_ctrl']):
			#ns_fk_ctrl.append('%s%s'%(ns,fk_ctrl))
			part_dict['fk_ctrl'][num] = ('%s%s'%(ns,fk_ctrl))
			
		for num,ik_ctrl in enumerate(part_dict['ik_ctrl']):
			#ns_ik_ctrl.append('%s%s'%(ns,ik_ctrl))
			part_dict['ik_ctrl'][num] = ('%s%s'%(ns,ik_ctrl))

		for num,skin_jnt in enumerate(part_dict['skin_jnt']):
			#ns_skin_jnt.append('%s%s'%(ns,skin_jnt))
			part_dict['skin_jnt'][num] = ('%s%s'%(ns,skin_jnt))


		kf = set(kf) | set(mc.keyframe(part_dict['fk_ctrl'],q=1,tc=1))
		kf = sorted(kf)

		all_ik_ctrl.extend(part_dict['ik_ctrl'])

	for keyFrame in kf:
		mc.currentTime(keyFrame,update=1)
		for fkIk_key in fkIk_dict.keys():
			fk_to_ik(fkIk_dict[fkIk_key]['ik_ctrl'], fkIk_dict[fkIk_key]['skin_jnt'], fkIk_dict[fkIk_key]['ctrl'], fkIk_dict[fkIk_key]['attr'],fkIk_dict[fkIk_key]['fk_default'])
		
		mc.setKeyframe(all_ik_ctrl,s=0)
				