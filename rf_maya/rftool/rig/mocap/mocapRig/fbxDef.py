import maya.cmds as mc
import maya.mel as mel
import maya.app.hik.retargeter as r
import sys
import pymel.core as pmc
import shutil
import os
import maya.standalone

from mocapData import hikList as hikd
reload(hikd)

from mocapData import ioData as iod
reload(iod)


def get_ns(obj):
	ns = pmc.PyNode(obj).namespace()
	if ns:
		return ns
	else:
		return ''

# create HIK Definition for Character
def create_hik_char(reference_joint = '',char='Character1'): 
	# define variable from hik list
	ns = get_ns(reference_joint)

	hik_joint_list = [reference_joint]
	hik_joint_list.extend(mc.listRelatives(reference_joint,ad=1))
	part_dict = hikd.part_dict
	charName = char

	# create hik character definition map
	mel.eval('hikCreateDefinition();string $char = hikGetCurrentCharacter();string $newName =`rename $char "%s"`;'%(charName))
	
	for part_jnt in hik_joint_list:
		mc.select(charName)
		if ns:
			search_part = part_jnt.split(ns)[-1]
		else:
			search_part = part_jnt

		if search_part in part_dict.keys():
			part_num = part_dict[search_part][0]
			mel.eval('setCharacterObject("%s",$newName, %s,0) ;'%(part_jnt,part_num))

	mel.eval("HIKCharacterControlsTool;")
	

def create_fbx_def(char='Character1'):
	mel.eval("HIKCharacterControlsTool")
	ns=get_ns(mc.ls(sl=True)[0])
	create_hik_char(mc.ls(sl=True)[0],char)


def create_def_in_scene():
	root_list = ['Reference','*:*Reference','Hips','*:*Hips']
	for root in root_list:
		print root
		if mc.ls(root):
			print mc.ls(root)
			if mc.objExists(mc.ls(root)[0]):
				reference_joint = root
				break
	mc.select(reference_joint)
	create_fbx_def()

def publish_fbx_def(file_name ='' , location = ''):
	# check output path
	if not location:
		mc.confirmDialog(title = 'folderMocapDef Error', m='no output directory assigned.',b='OK')
		return 
	
	# publish file 
	if file_name:
		if '\\' in file_name:
			fbx_file_name = file_name.split('\\')[-1]
		elif '/' in file_name:
			fbx_file_name = file_name.split('/')[-1]

		mc.file(new=1,f=1)
		mc.file('%s'%file_name ,i=1)
		create_def_in_scene()
		mc.file(rename = '%s/%s'%( location ,fbx_file_name.split('.fbx')[0]))
		mc.file(save=1,type='mayaBinary')

	else:
		mc.confirmDialog(title = 'folderMocapDef Error', m='Please type directory of mocap data folder.',b='OK')

def mul_publish_fbx_def(file_list=[],location = ''):
	# check output path
	if not location:
		mc.confirmDialog(title = 'folderMocapDef Error', m='no output directory assigned.',b='OK')
		return 

	# publish each file in list
	if file_list:
		for f in file_list:
			if '.fbx' in f:
				publish_fbx_def(f, location)
	else:
		mc.confirmDialog(title = 'folderMocapDef Error', m='Please type directory of mocap data folder.',b='OK')

def get_min_max_frame(obj_list):
	kf = mc.keyframe(obj_list,q=1,tc=1)
	return [min(kf),max(kf)]


def create_action_folder(path,fbx_path):
	if '\\' in fbx_path:
		file_name = fbx_path.split('\\')[-1].split('.')[0]
	elif '/' in fbx_path:
		file_name = fbx_path.split('/')[-1].split('.')[0]
	

	action_fld_path = iod.create_folder(os.path.join(path,file_name))
	# copy file to action folder
	shutil.copy(fbx_path,action_fld_path)
	publish_fbx_def(fbx_path , action_fld_path)
	preview_fld = os.path.join(action_fld_path,'prev')
	preview_path = iod.create_folder(preview_fld)
	playblast_file = os.path.join(preview_path,file_name)

	maya.standalone.initialize()


	#mel.eval("lookThroughModelPanel persp modelPanel1;")
	mel.eval("FrameSelectedWithoutChildren;")
	
	# start_frame, end_frame = get_min_max_frame(mc.ls(type='animCurve'))
	# mc.playblast(filename = playblast_file,st=start_frame,et=end_frame,
	# 	fmt='image',w = 384,h=216,qlt=100,compression = 'jpg',os=1,orn=0)
