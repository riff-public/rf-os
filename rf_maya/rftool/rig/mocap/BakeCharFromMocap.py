import maya.cmds as mc
import maya.mel as mel
from rf_utils import file_utils as fu
import sys
import math

import HIKDefinition as HIKD
reload(HIKD)

import matchAllFrame as maf
reload(maf)

def importChar(assetName = ''):
	ns = '{}_001'.format(assetName)
	path = r'P:\DarkHorse\asset\publ\char\{}\hero'.format(assetName).replace('\\','/')
	fileName = '{}_rig_main_md.hero.ma'.format(assetName)
	filePath = '{}/{}'.format(path,fileName)
	try:
	    mc.file(filePath,type='mayaAscii',r=1,ns=ns,f=1)
	except RuntimeError:
	    mc.confirmDialog(m='File not Found.')
	    
def CreateHIK(assetName =''):
	ns = '{}_001'.format(assetName)
	HIKD.TPoseSet(ns)
	HIKD.crateChar(ns,assetName)
	HIKD.createCustomRigMap(ns)

def RefFile(Path='',fileName = ''):
	if Path != '':
		try:
			mc.file(Path,r=1,ns=fileName)
		except RuntimeError:
			mc.confirmDialog(m='File not Found.')

def setTimeSlider(fileName=''):
	mc.select('*{}*:*Hips*'.format(fileName),hi=1)
	sel = mc.ls(sl=True)
	keys = mc.keyframe(sel,q=1,timeChange=1)
	minKey= math.floor(min(keys))
	maxKey =  math.ceil(max(keys))
	mc.playbackOptions(min=minKey,max=maxKey)


def SelectSource(fileName =''):
	mel.eval("HIKCharacterControlsTool;")
	sourceName = ' {}:Character1'.format(fileName)
	mel.eval('optionMenuGrp -e -v "{}" hikSourceList;'.format(sourceName))
	#setCurSource = "hikSetCurrentSourceFromCharacter( {} )".format(assetName)
	#me.eval(setCurSource)
	updateHIKSource = "hikUpdateCurrentSourceFromUI(); hikUpdateContextualUI();"
	mel.eval(updateHIKSource)


def BakeToCtrl():
	mc.currentUnit(time = 'ntsc')
	bakeCmd = 'hikBakeCharacter 0; hikSetCurrentSourceFromCharacter(hikGetCurrentCharacter()); hikUpdateSourceList; hikUpdateContextualUI;'
	mel.eval(bakeCmd)

def removeRefFile(filePath=''):
	mc.file(filePath,rr=1)

def changeFKArm(ns):
	lArm = '{}:Arm_L_Ctrl.fkIk'.format(ns)
	rArm = '{}:Arm_R_Ctrl.fkIk'.format(ns)
	mc.setAttr(lArm,0)
	mc.setAttr(rArm,0)

def singleBake(assetName = '',filePath = '',destinationFolder=''):
	mc.file(new=1,f=1)
	ns = '{}_001'.format(assetName)
	if '//' in filePath:
		fileName = (filePath).split('\\')[-1].split('.m')[0]
	else:
		fileName = (filePath).split('/')[-1].split('.m')[0]
	print fileName
	importChar(assetName)
	CreateHIK(assetName)
	RefFile((filePath),fileName)
	setTimeSlider(fileName)
	SelectSource(fileName)
	BakeToCtrl()
	HIKD.delUnknownHIKNode(assetName)
	maf.matchAllAllFrame(ns)
	changeFKArm(ns)
	removeRefFile(filePath)
	saveTo(fileName,destinationFolder)

def saveTo(fileName='',destinationFolder=''):
	savePath = (destinationFolder)+'\\{}.ma'.format(fileName)
	mc.file(rename = savePath,)
	mc.file(save=1,type='mayaAscii')

def mulBake(assetName='',filePathList=[],destinationFolder=''):
	for filePath in filePathList:
		singleBake(assetName,(filePath),destinationFolder)

