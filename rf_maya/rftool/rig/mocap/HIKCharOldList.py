partNumList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 170, 171]

partNameList = ["Reference","Hips","LeftUpLeg","LeftLeg","LeftFoot","RightUpLeg","RightLeg","RightFoot","Spine","LeftArm","LeftForeArm","LeftHand","RightArm","RightForeArm","RightHand","Head","LeftToeBase","RightToeBase","LeftShoulder","RightShoulder","Neck","LeftFingerBase","RightFingerBase","Spine1","Spine2","Spine3","Spine4","Spine5","Spine6","Spine7","Spine8","Spine9","Neck1","Neck2","Neck3","Neck4","Neck5","Neck6","Neck7","Neck8","Neck9","LeftShoulderExtra","RightShoulderExtra"]

partJntList = ['',"Hips","LeftUpLeg","LeftLeg","LeftFoot","RightUpLeg","RightLeg","RightFoot","Spine","LeftArm","LeftForeArm","LeftHand","RightArm","RightForeArm","RightHand","Head","LeftToeBase","RightToeBase","LeftShoulder","RightShoulder","Neck","","","Spine1","Spine2","Spine3","","","","","","","","","","","","","","","","",""]

partCtlList = ['',"Root_Ctrl","UpLegFk_L_Ctrl","LowLegFk_L_Ctrl","AnkleFk_L_Ctrl","UpLegFk_R_Ctrl","LowLegFk_R_Ctrl","AnkleFk_R_Ctrl","Spine1_Ctrl","UpArmFk_L_Ctrl",
				"ForearmFk_L_Ctrl","WristFk_L_Ctrl","UpArmFk_R_Ctrl","ForearmFk_R_Ctrl","WristFk_R_Ctrl","Head_Ctrl","","","Clav_L_Ctrl","Clav_R_Ctrl",
				"Neck_Ctrl","","","Spine2_Ctrl","Spine3_Ctrl","Spine4_Ctrl","","","","","","","","","","","","","","","","",""]

charJntList =  ['',"Root_SkinJnt","UpLeg_L_SkinJnt","LowLeg_L_SkinJnt","Ankle_L_SkinJnt","UpLeg_R_SkinJnt","LowLeg_R_SkinJnt","Ankle_R_SkinJnt","Spine1Pos_SkinJnt","UpArm_L_SkinJnt","ForeArm_L_SkinJnt","Wrist_L_SkinJnt","UpArm_R_SkinJnt",
 			"ForeArm_R_SkinJnt","Wrist_R_SkinJnt","Head1_SkinJnt","","","Clav1_L_SkinJnt","Clav1_R_SkinJnt","Neck1_SkinJnt","","","Spine2Pos_SkinJnt","Spine3Pos_SkinJnt","Spine4Pos_SkinJnt","","","","","","","","","","","","","","","","",""]


AnimCtrlList = ['WristFk_R_Ctrl','ForearmFk_R_Ctrl','UpArmFk_R_Ctrl','UpArmFk_L_Ctrl',
            'ForearmFk_L_Ctrl','WristFk_L_Ctrl','Neck_Ctrl','Head_Ctrl','Spine2_Ctrl',
            'Spine3_Ctrl','Spine4_Ctrl','Root_Ctrl','Spine1_Ctrl','UpLegFk_L_Ctrl',
            'UpLegFk_R_Ctrl','LowLegFk_L_Ctrl','LowLegFk_R_Ctrl','LegRbn_R_Ctrl','AnkleFk_R_Ctrl',
            'AnkleFk_L_Ctrl','BallFk_R_Ctrl','UpLegIk_L_Ctrl','KneeIk_L_Ctrl','AnkleIk_L_Ctrl',
            'UpLegIk_R_Ctrl','KneeIk_R_Ctrl','AnkleIk_R_Ctrl','UpArmIk_L_Ctrl','ElbowIk_L_Ctrl','WristIk_L_Ctrl'
            ,'UpArmIk_R_Ctrl','ElbowIk_R_Ctrl','WristIk_R_Ctrl','Clav_L_Ctrl','Clav_R_Ctrl'
            ]

 


