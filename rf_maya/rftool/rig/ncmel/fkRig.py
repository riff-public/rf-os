import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  FK RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , name       = '' ,
                         tmpJnt     = [ ] ,
                         parent     = '' ,
                         ctrlGrp    = 'Ctrl_Grp' , 
                         axis       = 'y' ,
                         shape      = 'square' ,
                         side       = '' ,
                         size       = 1 
                 ):

        ##-- Info
        lenght = len(tmpJnt)

        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        self.fkCtrlArray = []
        self.fkGmblArray = []
        self.fkZroArray = []
        self.fkOfstArray = []
        self.fkParsArray = []
        self.fkJntArray = []
        self.fkPosiArray = []

        #-- Create Main Group
        self.fkCtrlGrp = rt.createNode( 'transform' , '%sCtrl%sGrp' %( name , side ))

        if mc.objExists(parent):
            mc.parentConstraint( parent , self.fkCtrlGrp , mo = False )
            mc.scaleConstraint( parent , self.fkCtrlGrp , mo = False )

        #-- Rig process
        for i in range(1,lenght+1) :
            
            #-- Create Joint Position
            fkPosJnt = rt.createJnt( '%s%sPos%sJnt' %( name , i , side ) , tmpJnt[i-1] )
            self.fkPosiArray.append(fkPosJnt)

            if not i == lenght :
                #-- Joint
                fkJnt = rt.createJnt( '%s%s%sJnt' %( name , i , side ) , tmpJnt[i-1] )
                self.fkJntArray.append(fkJnt)
                fkJnt.parent( fkPosJnt )
                
                #-- Controls
                fkCtrl = rt.createCtrl( '%s%s%sCtrl' %( name , i , side ) , shape , 'pink' , jnt = False )
                fkGmbl = rt.addGimbal( fkCtrl )
                fkZro = rt.addGrp( fkCtrl )
                fkOfst = rt.addGrp( fkCtrl , 'Ofst' )
                fkPars = rt.addGrp( fkCtrl , 'Pars')
                fkZro.snap( fkPosJnt)
                
                self.fkCtrlArray.append(fkCtrl)
                self.fkGmblArray.append(fkGmbl)
                self.fkZroArray.append(fkZro)
                self.fkOfstArray.append(fkOfst)
                self.fkParsArray.append(fkPars)
            
            #-- Adjust Shape Controls
            for ctrl in ( fkCtrl , fkGmbl ) :
                ctrl.scaleShape( size )
            
            #-- Hierarchy
            if i == 1 :
                if not i == lenght :
                    if mc.objExists(ctrlGrp):
                        mc.parent( self.fkCtrlGrp , ctrlGrp )
                   
                if mc.objExists(parent): 
                    mc.parent( fkPosJnt , parent )

                fkZro.parent( self.fkCtrlGrp )
                
            else :
                if not i == lenght :
                    fkZro.parent( self.fkGmblArray[i-2] )

                fkPosJnt.parent( self.fkPosiArray[i-2] )
                
            #-- Rig process
            if not i == lenght :
                mc.parentConstraint( fkGmbl , fkPosJnt , mo = False  )
                rt.addSquash( fkCtrl , fkJnt , ax = ( 'sx' , 'sz'  ))
                
            
            if not i == 1 :
                if not i == lenght :
                    rt.addFkStretch( self.fkCtrlArray[i-2] , fkOfst , axis )
                else :
                    rt.addFkStretch( self.fkCtrlArray[i-2] , fkPosJnt , axis )
            

        if mc.objExists(ctrlGrp):
            self.locWor = rt.localWorld( self.fkCtrlArray[0] , ctrlGrp , self.fkCtrlGrp , self.fkZroArray[0] , 'orient' )
        
        self.fkPosiArray[0].attr('ssc').value = 0
        
        #-- Cleanup
        for zro in self.fkZroArray :
            zro.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for ofst in self.fkOfstArray :
            ofst.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for jnt in self.fkPosiArray :
            jnt.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for jnt in self.fkJntArray :
            jnt.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for ctrl in self.fkCtrlArray :
            ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        for gmbl in self.fkGmblArray :
            gmbl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

    mc.select( cl = True )


# -------------------------------------------------------------------------------------------------------------
#
#  ADD MORE FUNCTION FK RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class AddIk( Run ):

    def __init__( self , name       = '' ,
                         tmpJnt     = [ ] ,
                         parent     = '' ,
                         ctrlGrp    = 'Ctrl_Grp' , 
                         axis       = 'y' ,
                         shape      = 'square' ,
                         side       = '' ,
                         size       = 1 
                 ):

        super( AddIk , self ).__init__( name , tmpJnt , parent , ctrlGrp , axis , shape , side , size )

        ##-- Info
        lenght = len(tmpJnt)

        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        #Create Controls
        for numik in range(1,lenght):
            self.ikCtrl = rt.createCtrl('%s%sIK%sCtrl'%(name,numik,side) , 'circle' , 'cyan',jnt=False)
            self.ikZro = rt.addGrp( self.ikCtrl )
            self.ikOfst = rt.addGrp( self.ikCtrl  , 'Ofst' )

            mc.delete(mc.parentConstraint('%s%sGmbl%sCtrl'%(name,numik,side) , self.ikZro ))
            mc.parent( self.ikZro , '%s%sGmbl%sCtrl'%(name,numik,side) )


            self.ikZro.parent( '%s%sGmbl%sCtrl'%(name,numik,side))

            posJnt = '%s%sPos%sJnt' %( name , numik , side )
            posJnt = core.Dag( posJnt )

            posJnt.unlockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

            mc.delete( posJnt , cn=True)

            mc.parentConstraint('%s'%(self.ikCtrl), posJnt ,mo=True)

            lockjnt = posJnt.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')

        mc.select( cl = True )