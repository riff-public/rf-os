_title = 'Riff Add BlendShap'
_version = 'v.0.0.1'
_des = 'beta'
uiName = 'RFAddBlendShp'

#Import python modules
import sys
import os
import getpass
import json
import logging

moduleDir = os.path.dirname(sys.modules[__name__].__file__)
appName = os.path.splitext(os.path.basename(sys.modules[__name__].__file__))[0]

core = '%s/core' % os.environ.get('RFSCRIPT')
if not core in sys.path:
    sys.path.append(core)

# import config
import rf_config as config
from rf_utils import log_utils
# sg

user = '%s-%s' % (config.Env.localuser, getpass.getuser()) or 'unknown'
logFile = log_utils.name(uiName, user)
logger = log_utils.init_logger(logFile)

os.environ['QT_PREFERRED_BINDING'] = os.pathsep.join(['PySide', 'PySide2'])
from Qt import wrapInstance
from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

from ncmel import bshTools_test
reload(bshTools_test)
from ncmel import bshTools
reload(bshTools)


import maya.cmds as mc
import maya.mel as mm
import ui
reload(ui)
from ncmel.add_blendshape import agrument 
reload(agrument)
from ncmel.add_blendshape import agrument_bak
reload(agrument_bak)
from rf_utils import file_utils
from lpRig import rigTools 
from rf_utils.widget import dialog
from collections import defaultdict
from utaTools.utapy import utaCore
reload(utaCore)

class RFAddBlendShp(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        #SetUp Window
        super(RFAddBlendShp, self).__init__(parent)
        #ui read
        self.ui = ui.AddBlendShpUI(parent)
        self.setObjectName(uiName)
        self.setWindowTitle('%s %s-%s' % (_title, _version, _des))
        self.setCentralWidget(self.ui)
        self.setFixedSize(270, 600)
        self.adjustSize()
        self.init_state()

        # self.setFixedSize(50,195)
        # self.set_logo()

    def init_state(self):
        self.check_bsh()
        self.add_bsh_grp_UI()
        self.ui.addAttr_pushButton.clicked.connect(self.add_attr)
        self.ui.dup_pushButton.clicked.connect(self.duplicate_bsh)
        self.ui.connect_pushButton.clicked.connect(self.connect_bsh_new_node)
        self.ui.all_checkBox.stateChanged.connect(self.chk_all)
        self.ui.refresh_pushButton.clicked.connect(self.check_bsh)
        self.ui.list_wid.itemSelectionChanged.connect(self.select_wid_itm)
        self.ui.dup_pushButton_2.clicked.connect(self.dup_blend)
        # self.ui.connect_pushButton.clicked.connect(self.connect_bsh_new_node)
        self.ui.disConnect_pushButton.clicked.connect(self.disConnect_bsh_new_node)
        self.ui.connect_pushButton_2.clicked.connect(self.connect_bsh_new_node)
        self.ui.dicConnect_pushButton_2.clicked.connect(self.disConnect_bsh_new_node)
        self.ui.refresh_pushButton_2.clicked.connect(self.add_bsh_grp_UI)
        self.ui.edit_pushButton.clicked.connect(self.edit_)
        self.ui.add_pushButton.clicked.connect(self.add_)
        # self.ui.itm_sel_label.clicked.connect(self.part_)

        ####disConnect_pushButton
        ####dicConnect_pushButton_2
    def edit_(self):
        txt = self.ui.list_wid.currentItem()
        if txt:
            show_edit_ui(txt.text())

    # def part_(self):
    #     part_name = self.ui.itm_sel_label.currentItem()
    #     return part_name

    def add_(self):
        show_add_ui()

    def add_bsh_grp_UI(self):
        bsh_grp = mc.ls('Bsh_Grp')
        if bsh_grp:
            list_column = mc.listRelatives(bsh_grp, c=True)
            self.ui.list_wid.clear()
            for head in list_column:
                self.ui.list_wid.addItem(head)

    def connect_bsh_new_node(self):

        facialBshNode = mc.ls('Facial*_Bsn')
        utaCore.connectOrDisconnect(bshNode = facialBshNode, connectBsh = True)

    def disConnect_bsh_new_node(self):

        facialBshNode = mc.ls('Facial*_Bsn')
        utaCore.connectOrDisconnect(bshNode = facialBshNode, connectBsh = False)

    def select_wid_itm(self):
        itm = self.ui.list_wid.currentItem().text()
        if itm:
            mc.select(itm)
        return itm

    def dup_blend(self, select_grp=[]):
        root_grp = mc.ls(sl=True)
        result = defaultdict(list)
        objs = mc.listRelatives(root_grp, ad=True, f=True)
        error = ''


        for ln in objs:
            sn = ln.split('|')[-1]
            result[sn].append(ln)
            
        for sn, objs in result.iteritems():
            if len(objs) > 1:
                error = error + sn + '\n'

        if error:
            dialog.MessageBox.error('Warning', error)

        else:
            mc.parent(root_grp[0], 'BshRigStill_Grp')

            wid_items_sels = self.ui.list_wid.selectedItems()
            part_ = self.ui.itm_sel_line_edit.text()
            if not part_:
                part_  = 'New'

            lisWid = [a.text() for a in self.ui.list_wid.selectedItems()]
            dup_grp = mc.rename(root_grp[0], 'BshRigGeo_Grp_%s'%part_)
            bshNodeAll = []
            inBtwBshNodeAll = []
            all_grp = []
            if wid_items_sels:
                for wid_item_sel in wid_items_sels:
                    itm = wid_item_sel.text()
                    
                    bshNode = bshTools.wrapAndDupNewObj(bshNode = 'FacialAll_Bsn', targetObjs = [itm], part = part_ , dupObj = dup_grp)
                    if bshNode:
                        mc.parent( bshNode, itm, relative=True ) 
                        for each in bshNode:
                            if not 'InBtw' in each:
                                bshNodeAll.append(each)
                            else:
                                inBtwBshNodeAll.append(each)

            bsh_name = 'Facial%s_Bsn'%part_ 
            if bshNodeAll:
                bsnName = mc.blendShape( bshNodeAll , dup_grp , n = bsh_name )
            if inBtwBshNodeAll:
                for eachInBtwBshNodeAll in inBtwBshNodeAll:

                    checkBshCount = eachInBtwBshNodeAll.replace('InBtw', '')
                    countBsnNode = mc.blendShape(bsh_name, q=True,wc=True)
                    for x in range(countBsnNode):
                        bsc = mc.aliasAttr("{}.w[{}]".format(bsh_name, x), q=True)
                        if checkBshCount == bsc:
                            targetBsh = x

                    valueInBtw = 0.5
                    mc.blendShape( bsh_name , edit = True, ib = True, t = (dup_grp, targetBsh , eachInBtwBshNodeAll, valueInBtw) )

    def test(self, test_obj = []):
        for ix in test_obj:
            print ix

    def chk_all(self):
        if self.ui.all_checkBox.isChecked():
            if self.ui.eye_brown_checkBox.isEnabled():
                self.ui.eye_brown_checkBox.setChecked(1)
            if self.ui.eye_checkBox.isEnabled():
                self.ui.eye_checkBox.setChecked(1)
            if self.ui.mouth_checkBox.isEnabled():
                self.ui.mouth_checkBox.setChecked(1)
            if self.ui.nose_checkBox.isEnabled():
                self.ui.nose_checkBox.setChecked(1)
            if self.ui.head_checkBox.isEnabled():
                self.ui.head_checkBox.setChecked(1)
        else:
            if self.ui.eye_brown_checkBox.isEnabled():
                self.ui.eye_brown_checkBox.setChecked(0)
            if self.ui.eye_checkBox.isEnabled():
                self.ui.eye_checkBox.setChecked(0)
            if self.ui.mouth_checkBox.isEnabled():
                self.ui.mouth_checkBox.setChecked(0)
            if self.ui.nose_checkBox.isEnabled():
                self.ui.nose_checkBox.setChecked(0)
            if self.ui.head_checkBox.isEnabled():
                self.ui.head_checkBox.setChecked(0)

    def check_bsh(self):
        eyebrow = True
        eye = True
        mouth = True
        nose = True
        head = True
        bsh_node = mc.ls(type='blendShape')
        if bsh_node:
            for node in bsh_node:
                list_attr = mc.listAttr('%s.w'%node, m=True)
                if list_attr:
                    for attr in list_attr:
                        if 'Eyebrow' in attr:
                            eyebrow =False
                            self.ui.eye_brown_checkBox.setChecked(0)
                            self.ui.eye_brown_checkBox.setEnabled(0)
                        elif 'Eye' in attr:
                            eye=False
                            self.ui.eye_checkBox.setChecked(0)
                            self.ui.eye_checkBox.setEnabled(0)
                        elif 'Mouth' in attr:
                            mouth=False
                            self.ui.mouth_checkBox.setChecked(0)
                            self.ui.mouth_checkBox.setEnabled(0)
                        elif 'Nose' in attr:
                            nose =False
                            self.ui.nose_checkBox.setChecked(0)
                            self.ui.nose_checkBox.setEnabled(0)
                        elif 'Head' in attr:
                            head = False
                            self.ui.head_checkBox.setChecked(0)
                            self.ui.head_checkBox.setEnabled(0)
            if eyebrow == True:
                self.ui.eye_brown_checkBox.setEnabled(1)
            if eye == True:
                self.ui.eye_checkBox.setEnabled(1)
            if mouth == True:
                self.ui.mouth_checkBox.setEnabled(1)
            if nose == True:
                self.ui.nose_checkBox.setEnabled(1)
            if head == True:
                self.ui.head_checkBox.setEnabled(1)
        else:
            self.ui.eye_brown_checkBox.setEnabled(1)
            self.ui.eye_checkBox.setEnabled(1)
            self.ui.mouth_checkBox.setEnabled(1)
            self.ui.nose_checkBox.setEnabled(1)
            self.ui.head_checkBox.setEnabled(1)

        self.ui.dup_pushButton.setStyleSheet("background-color:grey; color: white;");
        self.ui.connect_pushButton.setStyleSheet("background-color: grey; color: white;");

    def duplicate_bsh(self):
        # from ncmel import bshTools as bshTools7
        # reload(bshTools7)

        eyebrow = False
        eye = False
        mouth = False
        nose = False
        head = False
        if self.ui.eye_brown_checkBox.isChecked():
            eyebrow = True
        if self.ui.eye_checkBox.isChecked():
            eye = True
        if self.ui.mouth_checkBox.isChecked():
            mouth = True
        if self.ui.nose_checkBox.isChecked():
            nose = True
        if self.ui.head_checkBox.isChecked():
            head = True
        # print eyebrow, eye, mouth, nose,head
        self.bsnName = bshTools.dupBlendshape(eyebrow=eyebrow, eye= eye, mouth=mouth, nose=nose, head=head)
        bshTools.connectBshNodeToLocAttr() 
        self.ui.dup_pushButton.setStyleSheet("background-color:rgb(0, 240, 0); color: black;");



        # sel = mc.ls(sl=True)
        # bsh_grp, attr_grp = self.chk_wid_item() #bsh_grp == transform grp_bsh, attr_grp == all transforms in grp_bsh
        # if bsh_grp and attr_grp:
        #     self.dup_bsh([bsh_grp, attr_grp])

        # bshTools7.dupBlendshape()




    def connect_bsh(self):
        eyebrow = False
        eye = False
        mouth = False
        nose = False
        head = False
        
        if self.ui.eye_brown_checkBox.isChecked():
            eyebrow = True
            bsh_node = self.get_bsh_node('Eyebrow')
            bshTools_test.connectBshMoverCtrl(eyebrow=eyebrow, eye=False, mouth=False, nose=False, head=False, bshName = bsh_node)
        if self.ui.eye_checkBox.isChecked():
            eye = True
            bsh_node = self.get_bsh_node('Eye')
            bshTools_test.connectBshMoverCtrl(eyebrow=False, eye=eye, mouth=False, nose=False, head=False, bshName = bsh_node)
        if self.ui.mouth_checkBox.isChecked():
            mouth = True
            bsh_node = self.get_bsh_node('Mouth')
            bshTools_test.connectBshMoverCtrl(eyebrow=False, eye=False, mouth=mouth, nose=False, head=False, bshName = bsh_node)
        if self.ui.nose_checkBox.isChecked():
            nose = True
            bsh_node = self.get_bsh_node('Nose')
            bshTools_test.connectBshMoverCtrl(eyebrow=False, eye=False, mouth=False, nose=nose, head=False, bshName = bsh_node)
        if self.ui.head_checkBox.isChecked():
            head = True
            bsh_node = self.get_bsh_node('Head')
            bshTools_test.connectBshMoverCtrl(eyebrow=False, eye=False, mouth=False, nose=False, head=head, bshName = bsh_node)
        self.ui.connect_pushButton.setStyleSheet("background-color:rgb(0, 240, 0); color: black;");

    def add_attr(self):
        attr_name = self.file_dialog()
        self.ui.add_attr_chk_box(attr_name)

    def get_bsh_node(self, attr_name):
        bsh_node = mc.ls(type='blendShape')
        if bsh_node:
            for node in bsh_node:
                list_attr = mc.listAttr('%s.w'%node, m=True)
                for attr in list_attr:
                    if attr_name == 'Eye' and 'Eyebrow' not in attr:
                        return node
                    elif attr_name in attr and attr_name != 'Eye':
                        return node

                        
    def file_dialog (self): 
        result = mc.promptDialog(
            title='Attribrute Name',
            message='Enter Name:',
            button=['OK', 'Cancel'],
            defaultButton='OK',
            cancelButton='Cancel',
            dismissString='Cancel')
        if result == 'OK':
            text = mc.promptDialog(query=True, text=True)
        return text



class RFeditBlendShp(QtWidgets.QMainWindow):
    def __init__(self, parent=None, *args):
        #SetUp Window
        super(RFeditBlendShp, self).__init__(parent)
        #ui read
        self.editUi = ui.EditUi(parent)
        self.setCentralWidget(self.editUi)
        self.bsh_node_grp = args[0]
        self.init_state()
            

    def init_state(self):
        self.editUi.set_text_bsh_node(self.bsh_node_grp)
        self.get_bsh_txt(self.bsh_node_grp)
        self.editUi.edit_pushButton.clicked.connect(self.list_all_item)
        self.editUi.reset_pushButton.clicked.connect(self.reset)

    def get_bsh_txt(self, itm):
        self.editUi.list_wid_edit.clear()
        for key, values in agrument.arg_name.iteritems():
            if itm == key:
                all_grp = values
                for value in values:
                    self.editUi.add_list_wid_item(value)

    def list_all_item(self):
        data = []
        list_wid_all_item = self.editUi.list_wid_edit.selectedItems()
        for wid_item in list_wid_all_item:
            text = wid_item.text()
            if text:
                data.append(text)

        RFAddBlendShp().dup_blend(select_grp = data)

    def reset(self):
        file = os.path.join(moduleDir, 'agrument').replace('\\', '/')
        file_utils.ymlDumper(file, agrument_bak.arg_name)
        item = self.editUi.lineEdit.placeholderText()
        self.get_bsh_txt(item)

class RFeditAddBlendShp(QtWidgets.QMainWindow):
    """docstring for RFeditAddBlendShp"""
    def __init__(self, parent=None, *args):
        super(RFeditAddBlendShp, self).__init__(parent)
        self.addUi = ui.AddUi(parent)
        self.setCentralWidget(self.addUi)
        self.init_state()

    def init_state(self):
        self.addUi.add_pushButton.clicked.connect(self.add_attr)
        self.addUi.duplicate_pushButton.clicked.connect(self.duplicate)
        self.addUi.select_ctrl_pushButton.clicked.connect(self.select_ctrl)
        self.addUi.con_1_way_pushButton.clicked.connect(self.connect_one_bsh)
        self.addUi.bsn_chk_box.stateChanged.connect(self.test)
        self.addUi.con_2_way_pushButton.clicked.connect(self.connect_two_bsh)

    def add_attr(self):
        self.addUi.add_list_wid_item()

    def select_ctrl(self):
        select_item = mc.ls(sl=True)[0]
        type_obj = mc.nodeType(mc.listRelatives(select_item, s=True)[0])
        if type_obj == 'nurbsCurve':
            self.addUi.lineEdit_ctrl.setText(select_item)

    def chk_wid_item(self):
        wid_items_counts =self.addUi.list_wid_edit.count()
        bsh_grp = []
        attr_grp = []
        if self.addUi.lineEdit.text():
            self.addUi.lineEdit.setStyleSheet("QLineEdit { background-color: green }")
            bsh_grp.append(self.addUi.lineEdit.text())
        for ix in range(0, wid_items_counts):
            if self.addUi.list_wid_edit.item(ix).text() and self.addUi.list_wid_edit.item(ix).text() != 'rename' :
                attr_grp.append(self.addUi.list_wid_edit.item(ix).text())

        return bsh_grp, attr_grp

    def duplicate(self):
        # from ncmel import bshTools as bshTools7
        # reload(bshTools7)
        sel = mc.ls(sl=True)
        bsh_grp, attr_grp = self.chk_wid_item() #bsh_grp == transform grp_bsh, attr_grp == all transforms in grp_bsh
        if bsh_grp and attr_grp:
            self.dup_bsh([bsh_grp, attr_grp])

        # bshTools7.dupBlendshape()
        # bshTools7.connectBshNodeToLocAttr() 

    def dup_bsh(self, data):
        if mc.objExists('Bsh_Grp'):
            list_hire = mc.listRelatives('Bsh_Grp', c=True) #['EyebrowBsh_L_Grp','EyebrowBsh_R_Grp','LidBsh_L_Grp','LidBsh_R_Grp','LipBsh_Grp','LipBsh_L_Grp','LipBsh_R_Grp','NoseBsh_Grp','NoseBsh_L_Grp','NoseBsh_R_Grp','HeadBsh_Grp','InBetweenBsh_L_Grp','InBetweenBsh_R_Grp']
            head_row_1 = list_hire[0] #'EyebrowBsh_L_Grp'
            head_row_2 = list_hire[1] #'EyebrowBsh_R_Grp'
            head_row_3 = list_hire[-1]
            xValOffset = mc.getAttr('%s.tx'%head_row_2) - mc.getAttr('%s.tx'%head_row_1)
            xValAway = mc.getAttr('%s.tx'%head_row_3) + xValOffset

            list_child_hire = mc.listRelatives(head_row_1, c=True)
            head_column_1 = list_child_hire[0]
            head_column_2 = list_child_hire[1]
            yValAway = mc.getAttr('%s.ty'%head_column_2) - mc.getAttr('%s.ty'%head_column_1)
            if self.addUi.bsn_chk_box.isChecked():
                new_bsn_state = True
            else:
                new_bsn_state = False
                
            self.bsnName = bshTools_test.dupBlendshape(eyebrow=False, eye= False, mouth=False, nose=False, head=False, add_attr= data, val_Away= [ xValAway, yValAway], new_bsn_node=new_bsn_state) 
            

    def connect_one_bsh(self):
        list_bsh_wid = self.addUi.list_wid_edit.selectedItems()
        ctrl = self.addUi.lineEdit_ctrl.text()
        list_bsh = []
        node_name = self.addUi.lineEdit_node.text()
        for bsh in list_bsh_wid:
            text = bsh.text()
            list_bsh.append(text)
            MdvNode = self.create_node_ctrl(ctrl, node_name)
            bshName = text.split('_')[0]
            bsh.setBackground(QtGui.QColor('#39FF14') )
        mc.connectAttr('%s.ox' %(MdvNode) , '%s.%s' %(self.bsnName[0] ,bshName))

    def connect_two_bsh(self):
        list_bsh_wid = self.addUi.list_wid_edit.selectedItems()
        ctrl = self.addUi.lineEdit_ctrl.text()
        list_bsh = []
        node_name = self.addUi.lineEdit_node.text()
        bsh_list =[]
        for bsh in list_bsh_wid:
            text = bsh.text()
            list_bsh.append(text)
            bshName = text.split('_')[0]
            bsh_list.append(bshName)
            bsh.setBackground(QtGui.QColor('#39FF14') )
        MdvNode = self.create_node_ctrl_2way(ctrl, node_name)
        mc.connectAttr('%s.ox' %(MdvNode) , '%s.%s' %(self.bsnName[0] ,bsh_list[0]))
        mc.connectAttr('%s.oy' %(MdvNode) , '%s.%s' %(self.bsnName[0] ,bsh_list[1]))
        

    def create_node_ctrl(self, ctrl , node_name):
        if ctrl != None or ctrl != '':
            MdvNode = bshTools_test.add_attr_to_ctrl(ctrl= ctrl, node_name= node_name)
            self.addUi.lineEdit_ctrl.setStyleSheet("QLineEdit { background-color: green }")
            return MdvNode
        else:
            self.addUi.lineEdit_ctrl.setStyleSheet("QLineEdit { background-color: red }")

    def create_node_ctrl_2way(self, ctrl , node_name):
        if ctrl != None or ctrl != '':
            MdvNode = bshTools_test.add_attr_to_ctrl_2_way(ctrl= ctrl, node_name= node_name)
            self.addUi.lineEdit_ctrl.setStyleSheet("QLineEdit { background-color: green }")
            return MdvNode
        else:
            self.addUi.lineEdit_ctrl.setStyleSheet("QLineEdit { background-color: red }")


def show_edit_ui(txt):  
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI('RFeditBlendShp')
    myApp = RFeditBlendShp(maya_win.getMayaWindow(), txt)
    myApp.show()
    return myApp


def show_add_ui():
    from rftool.utils.ui import maya_win
    logger.info('Run in Maya\n')
    maya_win.deleteUI('RFeditBlendShp')
    myApp = RFeditAddBlendShp(maya_win.getMayaWindow())
    myApp.show()
    return myApp

def show():
    if config.isMaya:
        from rftool.utils.ui import maya_win
        logger.info('Run in Maya\n')
        maya_win.deleteUI(uiName)
        myApp = RFAddBlendShp(maya_win.getMayaWindow())
        myApp.show()
        return myApp
if __name__ == '__main__':
    show()