import os
import sys

from Qt import QtCore
from Qt import QtWidgets
from Qt import QtGui

module_path = sys.modules[__name__].__file__
module_dir  = os.path.dirname(module_path)

import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
from rf_utils import icon

class AddBlendShpUI(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(AddBlendShpUI, self).__init__(parent)
        self.resize(328, 292)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout()
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label = QtWidgets.QLabel()
        self.label.setObjectName("label")
        self.horizontalLayout_3.addWidget(self.label)
        self.lineEdit = QtWidgets.QLineEdit()
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout_3.addWidget(self.lineEdit)
        self.verticalLayout_6.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.groupBox_2 = QtWidgets.QGroupBox()
        self.groupBox_2.setObjectName("groupBox_2")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.groupBox_2)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.all_checkBox = QtWidgets.QCheckBox(self.groupBox_2)
        self.eye_checkBox = QtWidgets.QCheckBox(self.groupBox_2)
        self.eye_brown_checkBox = QtWidgets.QCheckBox(self.groupBox_2)
        self.mouth_checkBox = QtWidgets.QCheckBox(self.groupBox_2)
        self.nose_checkBox = QtWidgets.QCheckBox(self.groupBox_2)
        self.head_checkBox = QtWidgets.QCheckBox(self.groupBox_2)
        self.head_checkBox.setObjectName("head_checkBox")
        self.verticalLayout_4.addWidget(self.all_checkBox)
        self.verticalLayout_4.addWidget(self.eye_brown_checkBox)
        self.verticalLayout_4.addWidget(self.eye_checkBox)
        self.verticalLayout_4.addWidget(self.mouth_checkBox)
        self.verticalLayout_4.addWidget(self.nose_checkBox)
        self.verticalLayout_4.addWidget(self.head_checkBox)
        self.verticalLayout_5.addLayout(self.verticalLayout_4)
        self.horizontalLayout_2.addWidget(self.groupBox_2)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.groupBox = QtWidgets.QGroupBox()
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.verticalLayout_8 = QtWidgets.QVBoxLayout()
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.verticalLayout_7.addLayout(self.verticalLayout_8)
        self.addAttr_pushButton = QtWidgets.QPushButton(self.groupBox)
        self.addAttr_pushButton.setObjectName("pushButton")
        self.verticalLayout_7.addWidget(self.addAttr_pushButton)
        self.verticalLayout_3.addWidget(self.groupBox)
        # self.horizontalLayout_2.addLayout(self.verticalLayout_3)s
        self.verticalLayout_6.addLayout(self.horizontalLayout_2)
        self.list_wid = QtWidgets.QListWidget()
        self.list_wid.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.horizontal = QtWidgets.QHBoxLayout(self.list_wid)
        
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.dup_pushButton = QtWidgets.QPushButton("Duplicate")
        self.dup_pushButton.setMinimumSize(QtCore.QSize(0, 50))

        self.connect_pushButton = QtWidgets.QPushButton('Connect')
        self.connect_pushButton.setMinimumSize(QtCore.QSize(0, 25))
        self.horizontalLayout.addWidget(self.connect_pushButton)

        self.disConnect_pushButton = QtWidgets.QPushButton('DisConnect')
        self.disConnect_pushButton.setMinimumSize(QtCore.QSize(0, 25))
        self.horizontalLayout.addWidget(self.disConnect_pushButton)

        self.refresh_pushButton = QtWidgets.QPushButton('Refresh')

        self.verticalLayout_6.addWidget(self.refresh_pushButton)
        self.verticalLayout_6.addWidget(self.dup_pushButton)
        self.verticalLayout_6.addLayout(self.horizontalLayout)


        self.add_Layout_2 = QtWidgets.QHBoxLayout()
        self.dup_pushButton_2 = QtWidgets.QPushButton("Duplicate Part")
        self.dup_pushButton_2.setMinimumSize(QtCore.QSize(0, 50))

        self.dicConnect_pushButton_2 = QtWidgets.QPushButton("DisConnect")
        self.dicConnect_pushButton_2.setMinimumSize(QtCore.QSize(0, 25))

        self.connect_pushButton_2 = QtWidgets.QPushButton("Connect")
        self.connect_pushButton_2.setMinimumSize(QtCore.QSize(0, 25))

        self.refresh_pushButton_2 = QtWidgets.QPushButton('Refresh')
        self.edit_pushButton = QtWidgets.QPushButton('Edit')
        self.add_pushButton = QtWidgets.QPushButton('Add')

        self.horizontalLayout_edit = QtWidgets.QHBoxLayout()
        self.horizontalLayout_edit.addWidget(self.add_pushButton)
        self.horizontalLayout_edit.addWidget(self.edit_pushButton)
        self.add_Layout_2.addWidget(self.connect_pushButton_2)
        self.add_Layout_2.addWidget(self.dicConnect_pushButton_2)


        self.itm_sel_hor_layout = QtWidgets.QHBoxLayout()
        self.itm_sel_label = QtWidgets.QLabel('part')
        self.itm_sel_line_edit = QtWidgets.QLineEdit()
        self.itm_sel_hor_layout.addWidget(self.itm_sel_label)
        self.itm_sel_hor_layout.addWidget(self.itm_sel_line_edit)

        self.verticalLayout_6.addLayout(self.itm_sel_hor_layout)

        self.verticalLayout_6.addWidget(self.list_wid)
        self.verticalLayout_6.addWidget(self.refresh_pushButton_2)
        self.verticalLayout_6.addLayout(self.horizontalLayout_edit)
        self.verticalLayout_6.addWidget(self.dup_pushButton_2)
        self.verticalLayout_6.addLayout(self.add_Layout_2)
        self.verticalLayout_6.setStretch(1, 2)
        self.label.setText("Blend_Grp:")
        self.groupBox_2.setTitle("default")
        self.all_checkBox.setText("All")
        self.eye_checkBox.setText("Eye")
        self.eye_brown_checkBox.setText("Eyebrown")
        self.mouth_checkBox.setText( "Mouth")
        self.nose_checkBox.setText( "Nose")
        self.head_checkBox.setText( "Head")
        self.groupBox.setTitle( "Add Attribrute")
        self.addAttr_pushButton.setText( "Add Attr")
        # self.dup_pushButton.setText( "Duplicate")
        self.verticalLayout.addLayout(self.verticalLayout_6)
        self.lineEdit.setPlaceholderText("FacialAll_Bsn")
        self.lineEdit.setReadOnly(True)
        self.setLayout(self.verticalLayout)

    def add_attr_chk_box(self, name):
        chk_box_item = QtWidgets.QCheckBox(self.groupBox)
        chk_box_item.setText(name)
        self.verticalLayout_8.addWidget(chk_box_item)


class EditUi(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(EditUi, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Edit blendshape')
        self.list_wid_edit = QtWidgets.QListWidget()
        self.edit_pushButton =QtWidgets.QPushButton('Edit')
        self.reset_pushButton =QtWidgets.QPushButton('Reset')
        self.lineEdit = QtWidgets.QLineEdit()
        self.lineEdit.setReadOnly(True)
        self.list_wid_edit.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.lineEdit)
        self.allLayout.addWidget(self.list_wid_edit)
        self.allLayout.addWidget(self.reset_pushButton)
        self.allLayout.addWidget(self.edit_pushButton)
        self.setLayout(self.allLayout)

    def set_text_bsh_node(self, text):
        self.lineEdit.setPlaceholderText(text)

    def add_list_wid_item(self, name_item):
        item = QtWidgets.QListWidgetItem(name_item)
        item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
        self.list_wid_edit.addItem(item)

    def get_text(self, item_wid):
        return item_wid.text()


class AddUi(QtWidgets.QWidget):
    def __init__(self, parent=None) :
        super(AddUi, self).__init__(parent)

        self.allLayout = QtWidgets.QVBoxLayout()
        self.label = QtWidgets.QLabel('Add Attr blendshape')
        self.list_wid_edit = QtWidgets.QListWidget()
        self.add_pushButton =QtWidgets.QPushButton('Add')
        self.duplicate_pushButton =QtWidgets.QPushButton('duplicate')
        self.bsn_chk_box = QtWidgets.QCheckBox('New BlendShape Node')
        self.con_1_way_pushButton =QtWidgets.QPushButton('Connect 1 Way')
        self.con_2_way_pushButton =QtWidgets.QPushButton('Connect 2 Way')
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.addWidget(self.con_1_way_pushButton)
        self.horizontalLayout.addWidget(self.con_2_way_pushButton)
        self.lineEdit = QtWidgets.QLineEdit()
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.label_2 = QtWidgets.QLabel('Ctrl')
        self.label_2.setText('Ctrl')
        self.lineEdit_ctrl = QtWidgets.QLineEdit()
        self.select_ctrl_pushButton = QtWidgets.QPushButton('Select')
        self.horizontalLayout_2.addWidget(self.label_2)
        self.horizontalLayout_2.addWidget(self.lineEdit_ctrl)
        self.horizontalLayout_2.addWidget(self.select_ctrl_pushButton)

        self.label_3 = QtWidgets.QLabel('Node')
        self.lineEdit_node = QtWidgets.QLineEdit()
        self.enter_node_pushButton = QtWidgets.QPushButton('Enter')
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.addWidget(self.label_3)
        self.horizontalLayout_3.addWidget(self.lineEdit_node)
        self.horizontalLayout_3.addWidget(self.enter_node_pushButton)


        self.list_wid_edit.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.allLayout.addWidget(self.label)
        self.allLayout.addWidget(self.lineEdit)
        self.allLayout.addWidget(self.list_wid_edit)
        self.allLayout.addWidget(self.add_pushButton)
        self.allLayout.addWidget(self.duplicate_pushButton)
        self.allLayout.addWidget(self.bsn_chk_box)
        self.allLayout.addLayout(self.horizontalLayout_2)
        self.allLayout.addLayout(self.horizontalLayout_3)
        self.allLayout.addLayout(self.horizontalLayout)
        self.setLayout(self.allLayout)

    def set_text_bsh_node(self, text):
        self.lineEdit.setPlaceholderText(text)

    def add_list_wid_item(self):
        item = QtWidgets.QListWidgetItem('rename')
        item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
        self.list_wid_edit.addItem(item)

    def get_text(self, item_wid):
        return item_wid.text()
