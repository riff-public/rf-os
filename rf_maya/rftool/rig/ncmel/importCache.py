import maya.cmds as mc
import os
import re

class ImportCache(object):
    
    def __init__(self):
        self.set_value()
        if mc.window('importCache', q=True, ex=True):
            mc.deleteUI('importCache', window=True)
        
        mc.window('importCache' , t='importCache')
        mc.columnLayout (adj=True,cal='left')
        mc.rowLayout(numberOfColumns=2 , adj=True)
        mc.optionMenu( ['fileCache'],label='file cache : ', cc=self.refresh_display)
        mc.menuItem( 'setDress',label='setDress', p = 'fileCache')
        mc.menuItem( 'finalCam',label='finalCam', p = 'fileCache')
        mc.button('browse', l='browse cache', w=100, h=30, c=self.browse_file)
        mc.setParent('..')
        
        mc.frameLayout ("cacheList", label ="Cache List",w=200 )
        mc.iconTextScrollList('displayCache',ams=True, h=200, append=self.cache) # change append path
        
        mc.setParent('..')
        mc.rowLayout(numberOfColumns=2)
        mc.button('importCache', l='import', w=150, h=30, al='Left', c=self.import_cache)
        mc.button('refresh', l='refresh', w=150, h=30, al='Left', c=self.refresh_display)
        
        mc.showWindow('importCache')
        
        mc.window('importCache' ,e=True)
        self.refresh_display()

    def set_value(self, *args):
        filePath = mc.file(q=True, sn=True)
        self.fileName = mc.file(q=True, sn=True, shn=True)
        if re.match('[a-z]+[_][a-z]+[0-9]+[_][q][0-9]+[_][sS][0-9]+[A-Z]*[_][a-zA-Z]+[_][v][0-9]+',self.fileName):
            self.fileNames = self.fileName.split ('_')
            self.sequence = self.fileNames[2]
            self.shot = self.fileNames[3]
            self.assetName = self.fileName[4]
            self.path = 'P:/Two_Heroes/scene/film001/'+self.sequence+'/'+self.shot+'/'+self.assetName+'/cache'
        self.cache = []
        self.cacheImportPath = []

    def refresh_display(self, *args):
        self.set_value()
        
        if re.match('[a-z]+[_][a-z]+[0-9]+[_][q][0-9]+[_][sS][0-9]+[A-Z]*[_][a-zA-Z]+[_][v][0-9]+',self.fileName):
            sel = mc.optionMenu( ['fileCache'], q=True, select=True) # 1 , 2
            menuName = mc.optionMenu( ['fileCache'], q=True, ils=True)
    
            self.assetName = menuName[sel-1]
            self.path = 'P:/Two_Heroes/scene/film001/'+self.sequence+'/'+self.shot+'/'+self.assetName+'/cache'
            belowPath = os.path.dirname(self.path)
            self.cache = []
            self.cacheImportPath = []
            if "cache" in os.listdir(belowPath):
                for file in os.listdir(self.path):
                    if self.assetName in file:
                        pathCache = self.path+'/'+file #P:/Two_Heroes/scene/film001/'+sequence[0]+'/'+shot[0]+'/setDress/cache/xxxx.setDress
                        for abc in os.listdir(pathCache):
                            pathImport = pathCache+'/'+abc
                            self.cacheImportPath.append(pathImport)
                            self.cache.append(abc)
                mc.iconTextScrollList('displayCache', e=True, removeAll=True, append=self.cache) # change append path
            else:
                mc.iconTextScrollList('displayCache', e=True, removeAll=True)

    def import_cache(self, *agrs):
        sels = mc.iconTextScrollList('displayCache', q=True, selectItem=True)
        for cacheImport in self.cacheImportPath:
            for sel in sels:
                if sel in cacheImport:
                    abcName = mc.AbcImport(cacheImport)
                    print abcName

    def browse_file(self, *agrs):
        os.startfile(self.path)
                    
ImportCache()