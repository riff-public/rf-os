import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  RIBBON RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

def run( name = '' , posiA = '' , posiB = '' , axis = '' , side = '' , hi = True ):

    dist = rt.getDist( posiA , posiB )

    if hi :
        ribbon = RibbonHi( name = name , axis = axis , side = side , dist = dist )
    else :
        ribbon = RibbonLow( name = name , axis = axis , side = side , dist = dist )

    ribbon.rbnCtrlGrp.snapPoint(posiA)
    
    mc.delete( 
                   mc.aimConstraint( posiB , ribbon.rbnCtrlGrp , 
                                     aim = ribbon.aimVec , 
                                     u = ribbon.upVec , 
                                     wut='objectrotation' , 
                                     wuo = posiA , 
                                     wu= ribbon.upVec )
              )
    
    mc.parentConstraint( posiA , ribbon.rbnCtrlGrp , mo = True )
    mc.parentConstraint( posiA , ribbon.rbnRootCtrl)
    mc.parentConstraint( posiB , ribbon.rbnEndCtrl)

    for ctrl in ( ribbon.rbnRootCtrl , ribbon.rbnEndCtrl) :
        ctrl.lockHideAttrs('tx', 'ty', 'tz' , 'rx' , 'ry' , 'rz' )
        ctrl.hide()
    
    if not hi :
        ribbon.rbnStillGrp = ''

    return {    
             'rbnCtrlGrp'   : ribbon.rbnCtrlGrp , 
             'rbnJntGrp'    : ribbon.rbnJntGrp , 
             'rbnSkinGrp'   : ribbon.rbnSkinGrp , 
             'rbnStillGrp'  : ribbon.rbnStillGrp ,
             'rbnCtrl'      : ribbon.rbnCtrl ,
             'rbnRootCtrl'  : ribbon.rbnRootCtrl ,
             'rbnEndCtrl'   : ribbon.rbnEndCtrl
            }
    
class Ribbon( object ):

    def __init__( self , name   = '' ,
                         axis     = 'y+' ,
                         side   = 'L' ,
                         dist   = 1 
                 ):

        ##-- Info
        rbnInfo = self.ribbonInfo( axis )
    
        self.postAx    = rbnInfo[ 'postAx' ]
        self.twstAx    = rbnInfo[ 'twstAx' ]
        self.sqshAx    = rbnInfo[ 'sqshAx' ]
        self.aimVec    = rbnInfo[ 'aimVec' ]
        self.invAimVec = rbnInfo[ 'invAimVec' ]
        self.upVec     = rbnInfo[ 'upVec' ]
        self.rotate    = rbnInfo[ 'rotate' ]
        self.rotateOrder    = rbnInfo[ 'rotateOrder' ]

        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        #-- Create Main Group
        self.rbnCtrlGrp = rt.createNode( 'transform' , '%sRbnCtrl%sGrp' %( name , side ))
        self.rbnJntGrp = rt.createNode( 'transform' , '%sRbnJnt%sGrp' %( name , side ))
        self.rbnSkinGrp = rt.createNode( 'transform' , '%sRbnSkin%sGrp' %( name , side ))
        
        #-- Create Joint
        self.rbnRootJnt = rt.createJnt( '%sRbnRoot%sJnt' %( name , side ))
        self.rbnMidJnt = rt.createJnt( '%sRbnMid%sJnt' %( name , side ))
        self.rbnEndJnt = rt.createJnt( '%sRbnEnd%sJnt' %( name , side ))

        self.rbnRootJntAim = rt.createNode( 'transform' , '%sRbnRootJntAim%sGrp' %( name , side ))
        self.rbnMidJntAim = rt.createNode( 'transform' , '%sRbnMidJntAim%sGrp' %( name , side ))
        self.rbnEndJntAim = rt.createNode( 'transform' , '%sRbnEndJntAim%sGrp' %( name , side ))

        #-- Create Controls
        self.rbnRootCtrl = rt.createCtrl( '%sRbnRoot%sCtrl' %( name , side ) , 'locator' , 'yellow' )
        self.rbnRootCtrlZro = rt.addGrp( self.rbnRootCtrl )

        self.rbnEndCtrl = rt.createCtrl( '%sRbnEnd%sCtrl' %( name , side ) , 'locator' , 'yellow' )
        self.rbnEndCtrlZro = rt.addGrp( self.rbnEndCtrl )

        self.rbnCtrl = rt.createCtrl( '%sRbn%sCtrl' %( name , side ) , 'circle' , 'yellow' )
        self.rbnCtrlZro = rt.addGrp( self.rbnCtrl )
        self.rbnCtrlAim = rt.addGrp( self.rbnCtrl , 'Aim' )
        self.rbnCtrlPars = rt.addGrp( self.rbnCtrl , 'Pars' )

        mc.parent( self.rbnMidJntAim , self.rbnCtrl )
        mc.parent( self.rbnEndJntAim , self.rbnEndCtrl )
        mc.parent( self.rbnRootJntAim , self.rbnRootCtrl )

        #-- Adjust Shape Controls
        self.rbnCtrl.scaleShape( dist*0.25 )
        self.rbnCtrl.rotateShape( self.rotate[0] , self.rotate[1] , self.rotate[2] )
        
        #-- Adjust Rotate Order
        self.rbnCtrl.setRotateOrder( self.rotateOrder )

        #-- Rig Process
        mc.parentConstraint( self.rbnCtrlGrp , self.rbnSkinGrp , mo = False )
        mc.scaleConstraint( self.rbnCtrlGrp , self.rbnSkinGrp , mo = False )

        mc.parentConstraint( self.rbnRootJntAim , self.rbnRootJnt , mo = False )
        mc.parentConstraint( self.rbnMidJntAim , self.rbnMidJnt , mo = False )
        mc.parentConstraint( self.rbnEndJntAim , self.rbnEndJnt , mo = False )

        if '-' in axis :
            self.rbnCtrlZro.attr( '%s' %self.postAx ).value = -(dist/2)
            self.rbnEndCtrlZro.attr( '%s' %self.postAx ).value = -dist
        else :
            self.rbnCtrlZro.attr( '%s' %self.postAx ).value = dist/2
            self.rbnEndCtrlZro.attr( '%s' %self.postAx ).value = dist

        mc.pointConstraint( self.rbnRootCtrl , self.rbnEndCtrl , self.rbnCtrlZro , mo = False )

        self.aimRootCons = mc.aimConstraint( self.rbnEndCtrl , self.rbnRootJntAim , aim = self.aimVec , u = self.upVec , wut = "objectrotation" , wu = self.upVec , wuo = self.rbnRootCtrlZro )
        self.aimEndCons  = mc.aimConstraint( self.rbnRootCtrl , self.rbnEndJntAim , aim = self.invAimVec , u = self.upVec , wut = "objectrotation" , wu = self.upVec , wuo = self.rbnEndCtrlZro )
        self.aimMidCons  = mc.aimConstraint( self.rbnEndCtrl , self.rbnCtrlAim , aim = self.aimVec , u = self.upVec , wut = "objectrotation" , wu = self.upVec , wuo = self.rbnCtrlZro )
    
        #-- Twist Distribute
        self.rbnCtrl.addTitleAttr( 'Twist' )
        self.rbnCtrl.addAttr( 'autoTwist' , 0 , 1 )
        self.rbnCtrl.addAttr( 'rootTwist' )
        self.rbnCtrl.addAttr( 'endTwist' )

        self.rbnCtrlShape = core.Dag(self.rbnCtrl.shape)

        self.rbnCtrlShape.addAttr( 'rootTwistAmp' )
        self.rbnCtrlShape.addAttr( 'endTwistAmp' )

        self.rbnCtrlShape.addAttr( 'rootAbsoluteTwist' )
        self.rbnCtrlShape.addAttr( 'endAbsoluteTwist' )

        self.rbnCtrlShape.attr('rootTwistAmp').value = 1
        self.rbnCtrlShape.attr('endTwistAmp').value = 1

        self.rbnAmpTwMdv = rt.createNode( 'multiplyDivide' , '%sRbnAmpTwist%sMdv' %( name , side ))
        self.rbnAutoTwMdv = rt.createNode( 'multiplyDivide' , '%sRbnAutoTwist%sMdv' %( name , side ))
        self.rbnRootPma = rt.createNode( 'plusMinusAverage' , '%sRbnRootTwist%sPma' %( name , side ))
        self.rbnEndPma = rt.createNode( 'plusMinusAverage' , '%sRbnEndTwist%sPma' %( name , side ))

        self.rbnCtrlShape.attr( 'rootAbsoluteTwist' ) >> self.rbnAmpTwMdv.attr('i1x')
        self.rbnCtrlShape.attr( 'rootTwistAmp' ) >> self.rbnAmpTwMdv.attr('i2x')
        self.rbnCtrlShape.attr( 'endAbsoluteTwist' ) >> self.rbnAmpTwMdv.attr('i1y')
        self.rbnCtrlShape.attr( 'endTwistAmp' ) >> self.rbnAmpTwMdv.attr('i2y')

        self.rbnCtrl.attr( 'autoTwist' ) >> self.rbnAutoTwMdv.attr('i1x')
        self.rbnCtrl.attr( 'autoTwist' ) >> self.rbnAutoTwMdv.attr('i1y')

        self.rbnAmpTwMdv.attr( 'ox' ) >> self.rbnAutoTwMdv.attr('i2x')
        self.rbnAmpTwMdv.attr( 'oy' ) >> self.rbnAutoTwMdv.attr('i2y')

        self.rbnCtrl.attr( 'rootTwist' ) >> self.rbnRootPma.attr('i1[0]')
        self.rbnCtrl.attr( 'endTwist' ) >> self.rbnEndPma.attr('i1[0]')

        self.rbnAutoTwMdv.attr( 'ox' ) >> self.rbnRootPma.attr('i1[1]')
        self.rbnAutoTwMdv.attr( 'oy' ) >> self.rbnEndPma.attr('i1[1]')
        
        #-- Squash Atribute
        self.rbnCtrl.addTitleAttr( 'Squash' )
        self.rbnCtrl.addAttr( 'autoSquash' , 0 , 1 )
        self.rbnCtrl.addAttr( 'squash' )
        self.rbnCtrlShape.addAttr( 'length' )

        self.rbnPosGrp = rt.createNode( 'transform' , '%sRbnPos%sGrp' %( name , side ))
        self.rbnRootPosGrp = rt.createNode( 'transform' , '%sRbnRootPos%sGrp' %( name , side ))
        self.rbnMidPosGrp = rt.createNode( 'transform' , '%sRbnMidPos%sGrp' %( name , side ))
        self.rbnEndPosGrp = rt.createNode( 'transform' , '%sRbnEndPos%sGrp' %( name , side ))

        mc.pointConstraint(self.rbnRootJnt, self.rbnRootPosGrp)
        mc.pointConstraint(self.rbnMidJnt, self.rbnMidPosGrp)
        mc.pointConstraint(self.rbnEndJnt, self.rbnEndPosGrp)

        self.rootDist = rt.createNode( 'distanceBetween' , '%sRbnRoot%sDist' %( name , side ))
        self.endDist = rt.createNode( 'distanceBetween' , '%sRbnEnd%sDist' %( name , side ))

        self.rbnRootPosGrp.attr('t') >> self.rootDist.attr('p1')
        self.rbnMidPosGrp.attr('t') >> self.rootDist.attr('p2')
        self.rbnMidPosGrp.attr('t') >> self.endDist.attr('p1')
        self.rbnEndPosGrp.attr('t') >> self.endDist.attr('p2')

        self.rbnLenPma = rt.createNode( 'plusMinusAverage' , '%sRbnLen%sPma' %( name , side ))

        self.rootDist.attr('d') >> self.rbnLenPma.attr('i1[0]')
        self.endDist.attr('d') >> self.rbnLenPma.attr('i1[1]')

        self.rbnCtrlShape.attr('length').value = dist
        self.rbnCtrlShape.lockAttrs( 'length' )

        self.rbnSqshDivMdv = rt.createNode( 'multiplyDivide' , '%sRbnSqshDiv%sMdv' %( name , side ))
        self.rbnSqshPowMdv = rt.createNode( 'multiplyDivide' , '%sRbnSqshPow%sMdv' %( name , side ))
        self.rbnSqshNormMdv = rt.createNode( 'multiplyDivide' , '%sRbnSqshNorm%sMdv' %( name , side ))
        
        self.rbnSqshNormMdv.attr('operation').value = 2
        self.rbnLenPma.attr('o1') >> self.rbnSqshNormMdv.attr('i1x')
        self.rbnCtrlShape.attr('length') >> self.rbnSqshNormMdv.attr('i2x')
        
        self.rbnSqshPowMdv.attr('operation').value = 3
        self.rbnSqshNormMdv.attr('ox') >> self.rbnSqshPowMdv.attr('i1x')
        self.rbnSqshPowMdv.attr('i2x').value = 2
        
        self.rbnSqshDivMdv.attr('operation').value = 2
        self.rbnSqshDivMdv.attr('i1x').value = 1
        self.rbnSqshPowMdv.attr('ox') >> self.rbnSqshDivMdv.attr('i2x')
        
        #-- Adjust Hierarchy
        mc.parent( self.rbnRootJnt , self.rbnMidJnt , self.rbnEndJnt , self.rbnJntGrp )
        mc.parent( self.rbnRootPosGrp , self.rbnMidPosGrp , self.rbnEndPosGrp , self.rbnPosGrp )

        mc.parent( self.rbnCtrlZro , self.rbnRootCtrlZro , self.rbnEndCtrlZro , self.rbnPosGrp , self.rbnCtrlGrp )

        #-- Cleanup
        for obj in ( self.rbnJntGrp , self.rbnSkinGrp ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

        for obj in ( self.rbnRootCtrl , self.rbnEndCtrl ) :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' )

        for obj in ( self.rbnRootJnt , self.rbnMidJnt , self.rbnEndJnt ) :
            obj.attr('v').value = 0

        self.rbnCtrlShape.attr( 'rootTwistAmp' ).hide = 1
        self.rbnCtrlShape.attr( 'endTwistAmp' ).hide = 1

        self.rbnCtrlShape.attr( 'rootAbsoluteTwist' ).hide = 1
        self.rbnCtrlShape.attr( 'endAbsoluteTwist' ).hide = 1

        self.rbnCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

    def ribbonInfo( self , axis = '' ) :

        if axis == 'x+' :
            postAx = 'tx'
            twstAx =  'rx'
            sqshAx = [ 'sy' , 'sz' ]
            aimVec = [ 1 , 0 , 0 ]
            invAimVec = [ -1 , 0 , 0 ]
            upVec = [ 0 , 0 , 1 ]
            rotate = [ 0 , 0 , -90 ]
            rotateOrder = 0
            
        elif axis == 'x-' :
            postAx = 'tx'
            twstAx =  'rx'
            sqshAx = [ 'sy' , 'sz' ]
            aimVec = [ -1 , 0 , 0 ]
            invAimVec = [ 1 , 0 , 0 ]
            upVec = [ 0 , 0 , 1 ]
            rotate = [ 0 , 0 , 90 ]
            rotateOrder = 0
        
        elif axis == 'y+' :
            postAx = 'ty'
            twstAx =  'ry'
            sqshAx = [ 'sx' , 'sz' ]
            aimVec =  [ 0 , 1 , 0 ]
            invAimVec = [ 0 , -1 , 0 ]
            upVec = [ 0 , 0 , 1 ]
            rotate = [ 0 , 0 , 0 ]
            rotateOrder = 1
            
        elif axis == 'y-' :
            postAx = 'ty'
            twstAx =  'ry'
            sqshAx = [ 'sx' , 'sz' ]
            aimVec = [ 0 , -1 , 0 ]
            invAimVec = [ 0 , 1 , 0 ]
            upVec = [ 0 , 0 , 1 ]
            rotate = [ 0 , 0 , 180 ]
            rotateOrder = 1
        
        elif axis == 'z+' :
            postAx = 'tz'
            twstAx =  'rz'
            sqshAx = [ 'sx' , 'sy' ]
            aimVec = [ 0 , 0 , 1 ]
            invAimVec = [ 0 , 0 , -1 ]
            upVec = [ 0 , 1 , 0 ]
            rotate = [ 90 , 0 , 180 ]
            rotateOrder = 2
        
        elif axis == 'z-' :
            postAx = 'tz'
            twstAx =  'rz'
            sqshAx = [ 'sx' , 'sy' ]
            aimVec = [ 0 , 0 , -1 ]
            invAimVec = [ 0 , 0 , 1 ]
            upVec = [ 0 , 1 , 0 ]
            rotate = [ -90 , 0 , 0 ]
            rotateOrder = 2

        return { 'postAx' : postAx , 
                 'twstAx' : twstAx , 
                 'sqshAx' : sqshAx , 
                 'aimVec' : aimVec , 
                 'invAimVec' : invAimVec , 
                 'upVec' : upVec , 
                 'rotate' : rotate , 
                 'rotateOrder' : rotateOrder }


class RibbonHi( Ribbon ):

    def __init__( self , name   = '' ,
                         axis     = 'y+' ,
                         side   = 'L' ,
                         dist   = 1 
                 ):
        
        super( RibbonHi , self ).__init__( name , axis , side , dist )

        ##-- Info
        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        #-- Create Main Group
        self.rbnDetailGrp = rt.createNode( 'transform' , '%sRbnDtl%sGrp' %( name , side ))
        self.rbnStillGrp = rt.createNode( 'transform' , '%sRbnStill%sGrp' %( name , side ))

        #-- Create Attribute
        self.rbnCtrlShape.addAttr( 'detailControl' , 0 , 1 )

        #-- Create Nurbs Surface
        self.rbnNrb = core.Dag(mc.nurbsPlane( p = ( 0 , dist/2 , 0 ) , ax = ( 0 , 0 , 1 ) , w = True , lr = dist , d = 3 , u = 1 , v = 5 , ch = 0 , n = '%sRbn%sNrb' % ( name , side ))[0])
        self.rbnNrb.attr('rx').value = self.rotate[0]
        self.rbnNrb.attr('ry').value = self.rotate[1]
        self.rbnNrb.attr('rz').value = self.rotate[2]
        self.rbnNrb.freeze()

        self.rbnSkc = mc.skinCluster( self.rbnRootJnt , self.rbnMidJnt , self.rbnEndJnt , self.rbnNrb , dr = 7 , mi = 2 , n = '%sRbn%sSkc' % ( name , side ))[0]
        
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][7]' %self.rbnNrb , tv = [ self.rbnEndJnt , 1 ] )
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][6]' %self.rbnNrb , tv = [ self.rbnMidJnt , 0.2 ] )
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][5]' %self.rbnNrb , tv = [ self.rbnMidJnt , 0.5 ] )
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][4]' %self.rbnNrb , tv = [ self.rbnEndJnt , 0.1 ] )
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][3]' %self.rbnNrb , tv = [ self.rbnRootJnt , 0.1 ] )
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][2]' %self.rbnNrb , tv = [ self.rbnRootJnt , 0.5 ] )
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][1]' %self.rbnNrb , tv = [ self.rbnRootJnt , 0.8 ] )
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][0]' %self.rbnNrb , tv = [ self.rbnRootJnt , 1 ] )

        #-- Detail Controls
        self.rbnPosGrpDict = []
        self.posiDtlDict = []
        self.rbnDtlAimDict = []
        self.rbnDtlJntDict = []
        self.rbnDtlJntZroDict = []
        self.rbnDtlCtrlDict = []
        self.rbnDtlCtrlZroDict = []
        self.rbnDtlCtrlTwsDict = []
        self.rbnDtlTwsMdvDict = []
        self.rbnDtlTwsPmaDict = []
        self.rbnDtlSqshMdvDict = []
        self.rbnDtlSqshPmaDict = []

        for i in range(1,6) :

            #-- Posi Node
            self.rbnPosGrp = rt.createNode( 'transform' , '%s%sRbnDtlPos%sGrp' %( name , i , side ))
            self.posi = rt.createNode( 'pointOnSurfaceInfo' , '%s%sRbnDtlPos%sPosi' %( name , i , side ))
            
            self.posi.attr('turnOnPercentage').value = 1
            self.posi.attr('parameterU').value = 0.5

            self.rbnNrb.attr('worldSpace[0]') >> self.posi.attr('inputSurface')
            self.posi.attr('position') >> self.rbnPosGrp.attr('t')

            self.rbnPosGrpDict.append( self.rbnPosGrp )
            self.posiDtlDict.append( self.posi )
        
            #-- Aim Constraint Node
            self.rbnAim = rt.createNode( 'aimConstraint' , '%s%sRbnDtl%saimConstraint' %( name , i , side ))

            self.rbnAim.attr('ax').value = self.aimVec[0]
            self.rbnAim.attr('ay').value = self.aimVec[1]
            self.rbnAim.attr('az').value = self.aimVec[2]
            self.rbnAim.attr('ux').value = self.upVec[0]
            self.rbnAim.attr('uy').value = self.upVec[1]
            self.rbnAim.attr('uz').value = self.upVec[2]

            self.posi.attr('n') >> self.rbnAim.attr('wu')
            self.posi.attr('tv') >> self.rbnAim.attr('tg[0].tt')
            self.rbnAim.attr('cr') >> self.rbnPosGrp.attr('r')
        
            self.rbnDtlAimDict.append( self.rbnAim )
            mc.parent( self.rbnAim , self.rbnPosGrp )
            
            mc.select( cl = True )
        
            #-- Create Joint
            self.rbnJnt = rt.createJnt( '%s%sRbnDtl%sJnt' %( name , i , side ))
            self.rbnJntZro = rt.addGrp( self.rbnJnt )
            
            self.rbnDtlJntDict.append( self.rbnJnt )
            self.rbnDtlJntZroDict.append( self.rbnJntZro )

            #-- Create Controls
            self.rbnDtlCtrl = rt.createCtrl( '%s%sRbnDtl%sCtrl' %( name , i , side ) , 'circle' , 'cyan' )
            self.rbnDtlCtrlZro = rt.addGrp( self.rbnDtlCtrl )
            self.rbnDtlCtrlTws = rt.addGrp( self.rbnDtlCtrl , 'Twist' )

            self.rbnDtlCtrl.addAttr( 'squash' )
        
            self.rbnDtlCtrlDict.append( self.rbnDtlCtrl )
            self.rbnDtlCtrlZroDict.append( self.rbnDtlCtrlZro )
            self.rbnDtlCtrlTwsDict.append( self.rbnDtlCtrlTws )

            #-- Adjust Shape Controls
            self.rbnDtlCtrl.scaleShape( dist*0.2 )
            self.rbnDtlCtrl.rotateShape( self.rotate[0] , self.rotate[1] , self.rotate[2] )

            #-- Rig Process
            #-- Twist
            mc.parentConstraint( self.rbnPosGrp , self.rbnDtlCtrlZro , mo = False )
            mc.parentConstraint( self.rbnDtlCtrl , self.rbnJntZro , mo = False )

            self.rbnDtlTwsMdv = rt.createNode( 'multiplyDivide' , '%s%sRbnDtlTws%sMdv' %( name , i , side ))
            self.rbnDtlTwsPma = rt.createNode( 'plusMinusAverage' , '%s%sRbnDtlTws%sPma' %( name , i , side ))

            self.rbnDtlTwsMdvDict.append( self.rbnDtlTwsMdv )
            self.rbnDtlTwsPmaDict.append( self.rbnDtlTwsPma )

            self.rbnRootPma.attr('o1') >> self.rbnDtlTwsMdv.attr('i2x')
            self.rbnEndPma.attr('o1') >> self.rbnDtlTwsMdv.attr('i2y')

            self.rbnDtlTwsMdv.attr('ox') >> self.rbnDtlTwsPma.attr('i1[0]')
            self.rbnDtlTwsMdv.attr('oy') >> self.rbnDtlTwsPma.attr('i1[1]')

            self.rbnDtlTwsPma.attr('o1') >> self.rbnDtlCtrlTws.attr(self.twstAx)

            #-- Squash
            self.rbnDtlSqshMdv = rt.createNode( 'multiplyDivide' , '%s%sRbnDtlSqsh%sMdv' %( name , i , side ))
            self.rbnDtlSqshPma = rt.createNode( 'plusMinusAverage' , '%s%sRbnDtlSqsh%sPma' %( name , i , side ))

            self.rbnDtlSqshMdvDict.append( self.rbnDtlSqshMdv )
            self.rbnDtlSqshPmaDict.append( self.rbnDtlSqshPma )

            self.rbnDtlSqshPma.addAttr( 'default' )

            self.rbnDtlSqshMdv.attr('i2x').value = 0.5
            self.rbnDtlSqshMdv.attr('i2y').value = 0.5

            self.rbnDtlSqshPma.attr('default') >> self.rbnDtlSqshPma.attr('i1[0]')
            self.rbnDtlCtrl.attr('squash') >> self.rbnDtlSqshMdv.attr('i1x')
            self.rbnCtrl.attr('squash') >> self.rbnDtlSqshMdv.attr('i1y')
            self.rbnDtlSqshMdv.attr('ox') >> self.rbnDtlSqshPma.attr('i1[1]')
            self.rbnDtlSqshMdv.attr('oy') >> self.rbnDtlSqshPma.attr('i1[2]')
            self.rbnDtlSqshMdv.attr('oz') >> self.rbnDtlSqshPma.attr('i1[3]')
            self.rbnDtlSqshPma.attr('o1') >> self.rbnJnt.attr('%s' %self.sqshAx[0])
            self.rbnDtlSqshPma.attr('o1') >> self.rbnJnt.attr('%s' %self.sqshAx[1])

            self.rbnCtrlShape.attr('detailControl') >> self.rbnDtlCtrlZro.attr('v')

        #-- Adjust Detail Controls Position
        self.posiDtlDict[0].attr('parameterV').value = 0.1
        self.posiDtlDict[1].attr('parameterV').value = 0.3
        self.posiDtlDict[2].attr('parameterV').value = 0.5
        self.posiDtlDict[3].attr('parameterV').value = 0.7
        self.posiDtlDict[4].attr('parameterV').value = 0.9

        #-- Set Value Twist Distribute
        self.rbnDtlTwsMdvDict[0].attr('i1x').value = 1.00
        self.rbnDtlTwsMdvDict[0].attr('i1y').value = 0.00
        self.rbnDtlTwsMdvDict[1].attr('i1x').value = 0.75
        self.rbnDtlTwsMdvDict[1].attr('i1y').value = 0.25
        self.rbnDtlTwsMdvDict[2].attr('i1x').value = 0.50
        self.rbnDtlTwsMdvDict[2].attr('i1y').value = 0.50
        self.rbnDtlTwsMdvDict[3].attr('i1x').value = 0.25
        self.rbnDtlTwsMdvDict[3].attr('i1y').value = 0.75
        self.rbnDtlTwsMdvDict[4].attr('i1x').value = 0.00
        self.rbnDtlTwsMdvDict[4].attr('i1y').value = 1.00

        #-- Rig Auto Squash
        self.rbnDtlSqshBcl = rt.createNode( 'blendColors' , '%sRbnAutoSqsh%sBcl' %( name , side ))

        self.rbnCtrl.attr('autoSquash') >> self.rbnDtlSqshBcl.attr('b')
        self.rbnSqshDivMdv.attr('ox') >> self.rbnDtlSqshBcl.attr('c1r')
        self.rbnDtlSqshBcl.attr('opr') >> self.rbnDtlSqshMdvDict[0].attr('i1z')
        self.rbnDtlSqshBcl.attr('opr') >> self.rbnDtlSqshMdvDict[1].attr('i1z')
        self.rbnDtlSqshBcl.attr('opr') >> self.rbnDtlSqshMdvDict[2].attr('i1z')
        self.rbnDtlSqshBcl.attr('opr') >> self.rbnDtlSqshMdvDict[3].attr('i1z')
        self.rbnDtlSqshBcl.attr('opr') >> self.rbnDtlSqshMdvDict[4].attr('i1z')

        #-- Set Value Auto Squash
        self.rbnDtlSqshBcl.attr('c2r').value = 1

        self.rbnDtlSqshPmaDict[0].attr('default').value = 0.67
        self.rbnDtlSqshPmaDict[1].attr('default').value = 0.34
        self.rbnDtlSqshPmaDict[2].attr('default').value = 0.00
        self.rbnDtlSqshPmaDict[3].attr('default').value = 0.34
        self.rbnDtlSqshPmaDict[4].attr('default').value = 0.67

        self.rbnDtlSqshMdvDict[0].attr('i2z').value = 0.33
        self.rbnDtlSqshMdvDict[1].attr('i2z').value = 0.66
        self.rbnDtlSqshMdvDict[2].attr('i2z').value = 1.00
        self.rbnDtlSqshMdvDict[3].attr('i2z').value = 0.66
        self.rbnDtlSqshMdvDict[4].attr('i2z').value = 0.33

        #-- Adjust Hierarchy
        mc.parent( self.rbnNrb , self.rbnPosGrpDict , self.rbnStillGrp )
        mc.parent( self.rbnDtlJntZroDict , self.rbnSkinGrp )
        mc.parent( self.rbnDtlCtrlZroDict , self.rbnDetailGrp )
        mc.parent( self.rbnDetailGrp , self.rbnCtrlGrp )

        #-- Cleanup
        for grp in ( self.rbnStillGrp , self.rbnDetailGrp ) :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

        for dicts in ( self.rbnDtlCtrlZroDict , self.rbnDtlJntZroDict , self.rbnDtlCtrlTwsDict , self.rbnDtlJntDict ) :
            for each in dicts :
                each.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for dtlCtrl in self.rbnDtlCtrlDict :
            dtlCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

    mc.select( cl = True )


class RibbonLow( Ribbon ):

    def __init__( self , name   = '' ,
                         axis     = 'y+' ,
                         side   = 'L' ,
                         dist   = 1 
                 ):
        
        super( RibbonLow , self ).__init__( name , axis , side , dist )

        ##-- Info
        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side
        
        #-- Create Joint
        self.rbnJnt = rt.createJnt( '%sRbn%sJnt' %( name , side ))
        self.rbnJntZro = rt.addGrp( self.rbnJnt )
        self.rbnJntOfst = rt.addGrp( self.rbnJnt , 'Ofst' )

        mc.parentConstraint( self.rbnCtrl , self.rbnJntZro )

        #-- Twist Distribute
        self.rbnTwsMdv = rt.createNode( 'multiplyDivide' , '%sRbnTws%sMdv' %( name , side ))
        self.rbnTwsPma = rt.createNode( 'plusMinusAverage' , '%sRbnTws%sPma' %( name , side ))
        
        self.rbnTwsMdv.attr('i1x').value = 0.5
        self.rbnTwsMdv.attr('i1y').value = 0.5
        
        self.rbnRootPma.attr('o1') >> self.rbnTwsMdv.attr('i2y')
        self.rbnEndPma.attr('o1') >> self.rbnTwsMdv.attr('i2x')
        self.rbnTwsMdv.attr('ox') >> self.rbnTwsPma.attr('i1[0]')
        self.rbnTwsMdv.attr('oy') >> self.rbnTwsPma.attr('i1[1]')
        self.rbnTwsPma.attr('output1D') >> self.rbnJntOfst.attr( self.twstAx )

        #-- Squash
        self.rbnSqshMdv = rt.createNode( 'multiplyDivide' , '%sRbnSqsh%sMdv' %( name , side ))
        self.rbnSqshBcl = rt.createNode( 'blendColors' , '%sRbnSqsh%sBcl' %( name , side ))
        self.rbnSqshPma = rt.createNode( 'plusMinusAverage' , '%sRbnSqsh%sPma' %( name , side ))

        self.rbnSqshMdv.attr('i2x').value = 0.5
        self.rbnCtrl.attr('squash') >> self.rbnSqshMdv.attr('i1x')
        self.rbnCtrl.attr('autoSquash') >> self.rbnSqshBcl.attr('b')
        self.rbnSqshDivMdv.attr('ox') >> self.rbnSqshBcl.attr('c1r')

        self.rbnSqshBcl.attr('c2r').value = 1
        
        self.rbnSqshMdv.attr('ox') >> self.rbnSqshPma.attr('i1[0]')
        self.rbnSqshBcl.attr('opr') >> self.rbnSqshPma.attr('i1[1]')
        self.rbnSqshPma.attr('o1') >> self.rbnJnt.attr( self.sqshAx[0] )
        self.rbnSqshPma.attr('o1') >> self.rbnJnt.attr( self.sqshAx[1] ) 

        #-- Adjust Hierarchy
        mc.parent( self.rbnJntZro , self.rbnSkinGrp )

class RibbonMid( Ribbon ):

    def __init__( self , name   = '' ,
                         axis     = 'y+' ,
                         side   = 'L' ,
                         dist   = 1 
                 ):

        super( RibbonMid , self ).__init__( name , axis , side , dist )

        ##-- Info
        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        #-- Create Main Group
        self.rbnDetailGrp = rt.createNode( 'transform' , '%sRbnDtl%sGrp' %( name , side ))
        self.rbnStillGrp = rt.createNode( 'transform' , '%sRbnStill%sGrp' %( name , side ))

        #-- Create Attribute
        self.rbnCtrlShape.addAttr( 'detailControl' , 0 , 1 )

        #-- Create Nurbs Surface
        self.rbnNrb = core.Dag(mc.nurbsPlane( p = ( 0 , dist/2 , 0 ) , ax = ( 0 , 0 , 1 ) , w = True , lr = dist , d = 3 , u = 1 , v = 5 , ch = 0 , n = '%sRbn%sNrb' % ( name , side ))[0])
        self.rbnNrb.attr('rx').value = self.rotate[0]
        self.rbnNrb.attr('ry').value = self.rotate[1]
        self.rbnNrb.attr('rz').value = self.rotate[2]
        self.rbnNrb.freeze()

        self.rbnSkc = mc.skinCluster( self.rbnRootJnt , self.rbnMidJnt , self.rbnEndJnt , self.rbnNrb , dr = 7 , mi = 2 , n = '%sRbn%sSkc' % ( name , side ))[0]
        
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][7]' %self.rbnNrb , tv = [ self.rbnEndJnt , 1 ] )
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][6]' %self.rbnNrb , tv = [ self.rbnMidJnt , 0.2 ] )
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][5]' %self.rbnNrb , tv = [ self.rbnMidJnt , 0.5 ] )
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][4]' %self.rbnNrb , tv = [ self.rbnEndJnt , 0.1 ] )
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][3]' %self.rbnNrb , tv = [ self.rbnRootJnt , 0.1 ] )
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][2]' %self.rbnNrb , tv = [ self.rbnRootJnt , 0.5 ] )
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][1]' %self.rbnNrb , tv = [ self.rbnRootJnt , 0.8 ] )
        mc.skinPercent( self.rbnSkc , '%s.cv[0:3][0]' %self.rbnNrb , tv = [ self.rbnRootJnt , 1 ] )

        #-- Detail Controls
        self.rbnPosGrpDict = []
        self.posiDtlDict = []
        self.rbnDtlAimDict = []
        self.rbnDtlJntDict = []
        self.rbnDtlJntZroDict = []
        self.rbnDtlCtrlDict = []
        self.rbnDtlCtrlZroDict = []
        self.rbnDtlCtrlTwsDict = []
        self.rbnDtlTwsMdvDict = []
        self.rbnDtlTwsPmaDict = []
        self.rbnDtlSqshMdvDict = []
        self.rbnDtlSqshPmaDict = []

        for i in range(1,4) :

            #-- Posi Node
            self.rbnPosGrp = rt.createNode( 'transform' , '%s%sRbnDtlPos%sGrp' %( name , i , side ))
            self.posi = rt.createNode( 'pointOnSurfaceInfo' , '%s%sRbnDtlPos%sPosi' %( name , i , side ))
            
            self.posi.attr('turnOnPercentage').value = 1
            self.posi.attr('parameterU').value = 0.5

            self.rbnNrb.attr('worldSpace[0]') >> self.posi.attr('inputSurface')
            self.posi.attr('position') >> self.rbnPosGrp.attr('t')

            self.rbnPosGrpDict.append( self.rbnPosGrp )
            self.posiDtlDict.append( self.posi )
        
            #-- Aim Constraint Node
            self.rbnAim = rt.createNode( 'aimConstraint' , '%s%sRbnDtl%saimConstraint' %( name , i , side ))

            self.rbnAim.attr('ax').value = self.aimVec[0]
            self.rbnAim.attr('ay').value = self.aimVec[1]
            self.rbnAim.attr('az').value = self.aimVec[2]
            self.rbnAim.attr('ux').value = self.upVec[0]
            self.rbnAim.attr('uy').value = self.upVec[1]
            self.rbnAim.attr('uz').value = self.upVec[2]

            self.posi.attr('n') >> self.rbnAim.attr('wu')
            self.posi.attr('tv') >> self.rbnAim.attr('tg[0].tt')
            self.rbnAim.attr('cr') >> self.rbnPosGrp.attr('r')
        
            self.rbnDtlAimDict.append( self.rbnAim )
            mc.parent( self.rbnAim , self.rbnPosGrp )
            
            mc.select( cl = True )
        
            #-- Create Joint
            self.rbnJnt = rt.createJnt( '%s%sRbnDtl%sJnt' %( name , i , side ))
            self.rbnJntZro = rt.addGrp( self.rbnJnt )
            
            self.rbnDtlJntDict.append( self.rbnJnt )
            self.rbnDtlJntZroDict.append( self.rbnJntZro )

            #-- Create Controls
            self.rbnDtlCtrl = rt.createCtrl( '%s%sRbnDtl%sCtrl' %( name , i , side ) , 'circle' , 'cyan' )
            self.rbnDtlCtrlZro = rt.addGrp( self.rbnDtlCtrl )
            self.rbnDtlCtrlTws = rt.addGrp( self.rbnDtlCtrl , 'Twist' )

            self.rbnDtlCtrl.addAttr( 'squash' )
        
            self.rbnDtlCtrlDict.append( self.rbnDtlCtrl )
            self.rbnDtlCtrlZroDict.append( self.rbnDtlCtrlZro )
            self.rbnDtlCtrlTwsDict.append( self.rbnDtlCtrlTws )

            #-- Adjust Shape Controls
            self.rbnDtlCtrl.scaleShape( dist*0.2 )
            self.rbnDtlCtrl.rotateShape( self.rotate[0] , self.rotate[1] , self.rotate[2] )

            #-- Rig Process
            #-- Twist
            mc.parentConstraint( self.rbnPosGrp , self.rbnDtlCtrlZro , mo = False )
            mc.parentConstraint( self.rbnDtlCtrl , self.rbnJntZro , mo = False )

            self.rbnDtlTwsMdv = rt.createNode( 'multiplyDivide' , '%s%sRbnDtlTws%sMdv' %( name , i , side ))
            self.rbnDtlTwsPma = rt.createNode( 'plusMinusAverage' , '%s%sRbnDtlTws%sPma' %( name , i , side ))

            self.rbnDtlTwsMdvDict.append( self.rbnDtlTwsMdv )
            self.rbnDtlTwsPmaDict.append( self.rbnDtlTwsPma )

            self.rbnRootPma.attr('o1') >> self.rbnDtlTwsMdv.attr('i2x')
            self.rbnEndPma.attr('o1') >> self.rbnDtlTwsMdv.attr('i2y')

            self.rbnDtlTwsMdv.attr('ox') >> self.rbnDtlTwsPma.attr('i1[0]')
            self.rbnDtlTwsMdv.attr('oy') >> self.rbnDtlTwsPma.attr('i1[1]')

            self.rbnDtlTwsPma.attr('o1') >> self.rbnDtlCtrlTws.attr(self.twstAx)

            #-- Squash
            self.rbnDtlSqshMdv = rt.createNode( 'multiplyDivide' , '%s%sRbnDtlSqsh%sMdv' %( name , i , side ))
            self.rbnDtlSqshPma = rt.createNode( 'plusMinusAverage' , '%s%sRbnDtlSqsh%sPma' %( name , i , side ))

            self.rbnDtlSqshMdvDict.append( self.rbnDtlSqshMdv )
            self.rbnDtlSqshPmaDict.append( self.rbnDtlSqshPma )

            self.rbnDtlSqshPma.addAttr( 'default' )

            self.rbnDtlSqshMdv.attr('i2x').value = 0.5
            self.rbnDtlSqshMdv.attr('i2y').value = 0.5

            self.rbnDtlSqshPma.attr('default') >> self.rbnDtlSqshPma.attr('i1[0]')
            self.rbnDtlCtrl.attr('squash') >> self.rbnDtlSqshMdv.attr('i1x')
            self.rbnCtrl.attr('squash') >> self.rbnDtlSqshMdv.attr('i1y')
            self.rbnDtlSqshMdv.attr('ox') >> self.rbnDtlSqshPma.attr('i1[1]')
            self.rbnDtlSqshMdv.attr('oy') >> self.rbnDtlSqshPma.attr('i1[2]')
            self.rbnDtlSqshMdv.attr('oz') >> self.rbnDtlSqshPma.attr('i1[3]')
            self.rbnDtlSqshPma.attr('o1') >> self.rbnJnt.attr('%s' %self.sqshAx[0])
            self.rbnDtlSqshPma.attr('o1') >> self.rbnJnt.attr('%s' %self.sqshAx[1])

            self.rbnCtrlShape.attr('detailControl') >> self.rbnDtlCtrlZro.attr('v')

        #-- Adjust Detail Controls Position
        self.posiDtlDict[0].attr('parameterV').value = 0.1
        self.posiDtlDict[1].attr('parameterV').value = 0.5
        self.posiDtlDict[2].attr('parameterV').value = 0.9

        #-- Set Value Twist Distribute
        self.rbnDtlTwsMdvDict[0].attr('i1x').value = 1.00
        self.rbnDtlTwsMdvDict[0].attr('i1y').value = 0.00
        self.rbnDtlTwsMdvDict[1].attr('i1x').value = 0.50
        self.rbnDtlTwsMdvDict[1].attr('i1y').value = 0.50
        self.rbnDtlTwsMdvDict[2].attr('i1x').value = 0.00
        self.rbnDtlTwsMdvDict[2].attr('i1y').value = 1.00

        #-- Rig Auto Squash
        self.rbnDtlSqshBcl = rt.createNode( 'blendColors' , '%sRbnAutoSqsh%sBcl' %( name , side ))

        self.rbnCtrl.attr('autoSquash') >> self.rbnDtlSqshBcl.attr('b')
        self.rbnSqshDivMdv.attr('ox') >> self.rbnDtlSqshBcl.attr('c1r')
        self.rbnDtlSqshBcl.attr('opr') >> self.rbnDtlSqshMdvDict[0].attr('i1z')
        self.rbnDtlSqshBcl.attr('opr') >> self.rbnDtlSqshMdvDict[1].attr('i1z')
        self.rbnDtlSqshBcl.attr('opr') >> self.rbnDtlSqshMdvDict[2].attr('i1z')

        #-- Set Value Auto Squash
        self.rbnDtlSqshBcl.attr('c2r').value = 1

        self.rbnDtlSqshPmaDict[0].attr('default').value = 0.75
        self.rbnDtlSqshPmaDict[1].attr('default').value = 0.00
        self.rbnDtlSqshPmaDict[2].attr('default').value = 0.75

        self.rbnDtlSqshMdvDict[0].attr('i2z').value = 0.25
        self.rbnDtlSqshMdvDict[1].attr('i2z').value = 1.00
        self.rbnDtlSqshMdvDict[2].attr('i2z').value = 0.25

        #-- Adjust Hierarchy
        mc.parent( self.rbnNrb , self.rbnPosGrpDict , self.rbnStillGrp )
        mc.parent( self.rbnDtlJntZroDict , self.rbnSkinGrp )
        mc.parent( self.rbnDtlCtrlZroDict , self.rbnDetailGrp )
        mc.parent( self.rbnDetailGrp , self.rbnCtrlGrp )

        #-- Cleanup
        for grp in ( self.rbnStillGrp , self.rbnDetailGrp ) :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

        for dicts in ( self.rbnDtlCtrlZroDict , self.rbnDtlJntZroDict , self.rbnDtlCtrlTwsDict , self.rbnDtlJntDict ) :
            for each in dicts :
                each.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for dtlCtrl in self.rbnDtlCtrlDict :
            dtlCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

    mc.select( cl = True )