import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  MAIN RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , assetName = '' , size = 1 ):
        
        if assetName :
            self.assetName = '%s' %assetName
        else:
            self.assetName = 'Rig'

        #-- Create Main Group
        self.assetGrp = rt.createNode( 'transform' , '%s_Grp' %self.assetName )
        self.geoGrp = rt.createNode( 'transform' , 'Geo_Grp' )
        self.jntGrp = rt.createNode( 'transform' , 'Jnt_Grp' )
        self.ikhGrp = rt.createNode( 'transform' , 'Ikh_Grp' )
        self.ctrlGrp = rt.createNode( 'transform' , 'Ctrl_Grp' )
        self.mainCtrlGrp = rt.createNode( 'transform' , 'MainCtrl_Grp' )
        self.facialCtrlGrp = rt.createNode( 'transform' , 'FacialCtrl_Grp' )
        self.addRigCtrlGrp = rt.createNode( 'transform' , 'AddRigCtrl_Grp' )
        self.skinGrp = rt.createNode( 'transform' , 'Skin_Grp' )
        self.stillGrp = rt.createNode( 'transform' , 'Still_Grp' )
        
        #-- Create Main Controls
        self.allMoverCtrl = rt.createCtrl( 'AllMover_Ctrl' , 'square' , 'yellow' )
        self.offsetCtrl = rt.createCtrl( 'Offset_Ctrl' , 'arrowCross' , 'yellow' )

        #-- Adjust Shape Controls
        self.allMoverCtrl.rotateShape( 0 , 45 , 0 )
        self.allMoverCtrl.scaleShape( size * 7 )
        self.offsetCtrl.scaleShape( size * 1.5 )
        
        #-- Rig process
        self.allMoverCtrl.attr('sy') >> self.allMoverCtrl.attr('sx')
        self.allMoverCtrl.attr('sy') >> self.allMoverCtrl.attr('sz')

        #-- Adjust Hierarchy
        mc.parent( self.offsetCtrl , self.allMoverCtrl )
        mc.parent( self.ctrlGrp , self.skinGrp , self.jntGrp , self.ikhGrp , self.offsetCtrl )
        mc.parent( self.mainCtrlGrp , self.facialCtrlGrp , self.addRigCtrlGrp , self.ctrlGrp )
        mc.parent( self.geoGrp , self.allMoverCtrl , self.stillGrp , self.assetGrp )
        
        #-- Cleanup
        for obj in ( self.assetGrp , self.geoGrp , self.jntGrp , self.ikhGrp , self.ctrlGrp , self.skinGrp , self.stillGrp , self.mainCtrlGrp , self.facialCtrlGrp , self.addRigCtrlGrp ) :
            obj.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

        self.allMoverCtrl.lockHideAttrs( 'sx' , 'sz' , 'v' )
        self.offsetCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        self.jntGrp.attr('v').value = 0
        self.ikhGrp.attr('v').value = 0
        self.stillGrp.attr('v').value = 0

        mc.select( cl = True )

# -------------------------------------------------------------------------------------------------------------
#
#  PROP RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Prop( Run ):
    
    def __init__( self , assetName = '' , size = 1 ):
        
        sels = mc.ls( sl = True )

        super( Prop , self ).__init__( assetName , size )

        col = [ 'pink' , 'cyan' , 'red' , 'green' , 'blue' , 'brown' ]

        ##-- Select Multiple Object
        i = 0
        for sel in sels :
            nameSplit = sel.split('_')
            propName = '_'.join( nameSplit[0:-1] )

            self.porpCtrl = rt.createCtrl( '%s_Ctrl' %propName , 'square' , col[i%6] )
            self.porpGmbl = rt.addGimbal( self.porpCtrl )
            self.porpZro = rt.addGrp( self.porpCtrl )
            self.porpZro.snap( sel )

            mc.parentConstraint( self.porpGmbl , sel , mo = False )
            mc.scaleConstraint( self.porpGmbl , sel , mo = False )
            mc.parent( self.porpZro , self.ctrlGrp )
            mc.parent( sel , self.geoGrp )
            
            i+=1

        mc.select( cl = True )