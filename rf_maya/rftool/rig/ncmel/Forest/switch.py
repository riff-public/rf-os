import sys
import os
import re
import subprocess
import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel
from rftool.utils import maya_utils

def show() :
    width = 230

    if pm.window( 'forestTools' , exists = True ) :
        pm.deleteUI( 'forestTools' )
        
    window = pm.window( 'forestTools', title = "Forest Tools " , width = width ,
        mnb = True , mxb = False , sizeable = True , rtf = True )

    window = pm.window( 'forestTools', e = True , width = width )
    listLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = ( 1 , width-2 ))
    
    with listLayout :
        pm.textScrollList( 'listAssetTS' , h = 170 , numberOfRows = 12 , append = [ 'Rig' , 'RigRen' , '' , 'Gpu' , '' , 'WindLow(Abc)' , 'WindMid(Abc)' , 'WindHight(Abc)' , '' , 'WindLow(Rs)' , 'WindMid(Rs)' , 'WindHight(Rs)' ] , allowMultiSelection = 0 )
        pm.button( label = 'Switch' , h = 35  , c = switch )
        
    randomLayout = pm.rowColumnLayout ( w = width , nc = 3 , columnWidth = ( [1 , width/3] , [2 , width/3] , [3 , width/3] ))
    with randomLayout :
        pm.button( label = 'Switch' , h = 35  , c = switch )
        pm.button( label = 'Switch' , h = 35  , c = switch )
        pm.button( label = 'Switch' , h = 35  , c = switch )

    pm.showWindow( window )

def switch( *args ):

    lists = { 'Rig' : 'med'  ,
              'RigRen' : 'shad'  ,
              'Gpu' : 'gpu'  ,
              'WindLow(Abc)' : 'lowAbc' ,
              'WindMid(Abc)' : 'midAbc' ,
              'WindHight(Abc)' : 'hiAbc' ,
              'WindLow(Rs)' : 'lowRs' ,
              'WindMid(Rs)' : 'midRs' ,
              'WindHight(Rs)' : 'hiRs' }

    selObj = mc.ls(sl=True)
    noRef = []
    
    for sel in selObj :
        try :
            srcPath = mc.referenceQuery( sel , f = True ) 
            tmpAry = srcPath.split( '_' )
            pathDir = ''.join( tmpAry[0:-1] )
            level = pm.textScrollList( 'listAssetTS' , q = True , si = True )[0]
            dstPath = '%s_%s.ma' %(pathDir,lists[level])
    
            rnNode = mc.referenceQuery( srcPath , referenceNode = True )
            mc.file( dstPath , loadReference = rnNode , prompt = False )
        except :
            noRef.append( sel )
    
    if noRef :
        mc.select( noRef )
        mc.confirmDialog( title = 'Notify' , message = ' Object selected not Reference. ' )
    else :
        mc.select( selObj )

def createLocatorTempUI( *args ) :
    width = 350

    if pm.window( 'createLocatorTemp' , exists = True ) :
        pm.deleteUI( 'createLocatorTemp' )
        
    window = pm.window( 'createLocatorTemp', title = "CreateLocatorTemp Tools " , width = width ,
        mnb = True , mxb = False , sizeable = True , rtf = True )

    window = pm.window( 'createLocatorTemp', e = True , width = width )
    infoLayout = pm.rowColumnLayout ( w = width , nc = 2 , columnWidth = [( 1 , 45 ),( 2 , width-47 )])
    with infoLayout :
        pm.text( label = 'Name  :' )
        pm.textField( 'locNameTF' , en = True )
        pm.text( label = 'Path   :' )
        pm.textField( 'pathTF' , en = True )
    
    buttonLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = ( 1 , width-2 ))
    with buttonLayout :
        pm.button( label = 'Create Locator' , h = 35 , c = createLocatorTemp )

    pm.showWindow( window )

def createLocatorTemp( *args ):
    locName = pm.textField( 'locNameTF' , q = True , text = True )
    pathName = pm.textField( 'pathTF' , q = True , text = True )
    
    loc = locatorTemplate( locName )
    addTagLocator( loc , pathName )

def locatorTemplate( name = '' ) :
    loc = mc.createNode( 'locator' )
    loc = mc.listRelatives( loc , p = True )[0]
    loc = mc.rename( loc , name )

    return loc

def addTagLocator( locator = '' , path = '' ) :
    mc.addAttr( locator , ln = 'path' , dt = 'string' )
    mc.setAttr( '%s.path' %locator , path , type = 'string' )

def listObjects( path = r'' ) :
    listObjects = mc.getFileList( fld = path )

    return listObjects