import maya.cmds as mc

def createLayerDisplayForAnim( layerName ):
	mc.createDisplayLayer( n = '%s' %layerName , e = True )