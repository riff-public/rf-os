import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  ROOT RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , rootTmpJnt = 'Root_TmpJnt' , 
                         ctrlGrp    = 'Ctrl_Grp' , 
                         skinGrp    = 'Skin_Grp' , 
                         size       = 1 
                 ):

        #-- Create Main Group
        self.rootCtrlGrp = rt.createNode( 'transform' , 'RootCtrl_Grp' )
        self.rootCtrlGrp.snap( rootTmpJnt )
        
        #-- Create Joint
        self.rootJnt = rt.createJnt( 'Root_Jnt' , rootTmpJnt )

        #-- Create Controls
        self.rootCtrl = rt.createCtrl( 'Root_Ctrl' , 'arrowCross' , 'green' , jnt = True )
        self.rootGmbl = rt.addGimbal( self.rootCtrl )
        self.rootZro = rt.addGrp( self.rootCtrl )

        self.fntTxt = rt.createCtrl( 'FrontTxt_Ctrl' , 'F' , 'red' )
        self.bakTxt = rt.createCtrl( 'BackTxt_Ctrl' , 'B' , 'red' )
        self.lftTxt = rt.createCtrl( 'LeftTxt_Ctrl' , 'L' , 'red' )
        self.rgtTxt = rt.createCtrl( 'RightTxt_Ctrl' , 'R' , 'red' )

        self.fntTxtZro = rt.addGrp( self.fntTxt )
        self.bakTxtZro = rt.addGrp( self.bakTxt )
        self.lftTxtZro = rt.addGrp( self.lftTxt )
        self.rgtTxtZro = rt.addGrp( self.rgtTxt )

        self.fntTxtZro.parent( self.rootGmbl )
        self.bakTxtZro.parent( self.rootGmbl )
        self.lftTxtZro.parent( self.rootGmbl )
        self.rgtTxtZro.parent( self.rootGmbl )
        
        self.rootZro.snapPoint( self.rootJnt )
        self.rootCtrl.snapJntOrient( self.rootJnt )

        #-- Adjust Shape Controls
        for ctrl in ( self.rootCtrl , self.rootGmbl , self.fntTxt , self.bakTxt , self.lftTxt , self.rgtTxt ) :
            ctrl.scaleShape( size * 2.3 )
        #-- Adjust Rotate Order
        for obj in ( self.rootJnt , self.rootCtrl ) :
            obj.setRotateOrder( 'xzy' )

        #-- Rig process
        mc.parentConstraint( self.rootGmbl , self.rootJnt , mo = False )

        self.rootCtrlShape = core.Dag(self.rootCtrl.shape)
        self.rootCtrlShape.addAttr( 'positionText' , 0 , 1 )
        self.rootCtrlShape.attr('positionText') >> self.fntTxtZro.attr('v')
        self.rootCtrlShape.attr('positionText') >> self.bakTxtZro.attr('v')
        self.rootCtrlShape.attr('positionText') >> self.lftTxtZro.attr('v')
        self.rootCtrlShape.attr('positionText') >> self.rgtTxtZro.attr('v')

        #-- Adjust Hierarchy
        mc.parent( self.rootZro , self.rootCtrlGrp )
        mc.parent( self.rootCtrlGrp , ctrlGrp )
        mc.parent( self.rootJnt , skinGrp )

        #-- Cleanup
        for obj in ( self.rootCtrlGrp , self.rootZro , self.fntTxt , self.bakTxt , self.lftTxt , self.rgtTxt ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for obj in ( self.rootCtrl , self.rootGmbl ) :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )
        
        mc.select( cl = True )