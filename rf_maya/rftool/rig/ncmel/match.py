import maya.cmds as mc
import maya.mel as mm
import re
import pymel.core as pm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

def run() :

    sels = mc.ls( sl = True )

    if '_L_' in sels[0] :
        side = '_L_'
    else :
        side = '_R_'

    if ':' in sels[0] :
        nameSpace = sels[0].split( ':' )
        np = '%s:' %nameSpace[0]
        obj = nameSpace[-1]
    else :
        np = '' 
        obj = nameSpace [-1]

    for i in range(len(sels)):
        if core.Dag(sels[i]).attr('localWorld').exists:
            localWorld( sels[i] )
            
        elif core.Dag(sels[i]).attr('spaceFollow').exists:
            localWorld( sels[i] , 'World' )
            
        elif core.Dag(sels[i]).attr('follow').exists:
            localWorld( sels[i] )
    
    if 'Arm' in obj :
    
        matchObj = re.search( r'.*(Arm)(.+)_(.*)_(Ctrl)', obj)

        if matchObj:
            elem = matchObj.group(2)
        else :
            elem = ''

        if core.Dag(sels[i]).attr('fkIk').exists:
            armMatch( '%s' %np , '%s' %elem )

    elif 'Leg' in obj :

        matchObj = re.search( r'.*(Leg)(.+)_(.*)_(Ctrl)', obj )

        if matchObj:
            elem = matchObj.group(2)
        else :
            elem = ''

        if core.Dag(sels[i]).attr('fkIk').exists:
            if mc.objExists('MidLeg%sFk%sCtrl' %( elem , side )):
                legAnimalMatch( '%s' %np , '%s' %elem )
            else :
                legMatch( '%s' %np , '%s' %elem )
            
def PrintModifiers():
    mods = cmds.getModifiers()
    print 'Modifiers are:'
    if (mods & 1) > 0: print ' Shift'
    if (mods & 2) > 0: print ' CapsLock'
    if (mods & 4) > 0: print ' Ctrl'
    if (mods & 8) > 0: print ' Alt'
    print '\n'

def localWorld( ctrl = '' , following = '' ):
    
    # Control
    ctrl = core.Dag(ctrl)
    grp = core.Dag(ctrl.getParent())
    
    # Template
    prxy = rt.createJnt()
    tmp = rt.createJnt()
    tmpGrp = rt.addGrp( tmp )
    
    # Rotate order
    prxy.attr('rotateOrder').value = ctrl.attr('rotateOrder').value
    tmp.attr('rotateOrder').value = ctrl.attr('rotateOrder').value
    
    # Joint orient 
    if mc.nodeType( ctrl ) == 'joint':
        for axis in ( 'X' , 'Y' , 'Z' ) :
            prxy.attr('jointOrient%s' %axis).value = ctrl.attr('jointOrient%s' %axis).value
            tmp.attr('jointOrient%s' %axis).value = ctrl.attr('jointOrient%s' %axis).value
    
    # Snap
    prxy.snap(ctrl)
    tmpGrp.snap(ctrl)
    
    tmpCons = mc.parentConstraint( prxy , tmp , mo = False )
    mods = mc.getModifiers() # 4 = Ctrl , 8 = Alt

    if ctrl.attr('localWorld').exists:
        if ctrl.attr('localWorld').value:
            ctrl.attr('localWorld').value = 0
        else:
            ctrl.attr('localWorld').value = 1
    
    elif ctrl.attr('follow').exists:
        if mods :
            if mods == 4 :
                ctrl.attr('follow').value = 0
            elif mods == 8 :
                ctrl.attr('follow').value = 1
        else :
            if ctrl.attr('follow').value:
                ctrl.attr('follow').value = 0
            else:
                ctrl.attr('follow').value = 1
    
    elif ctrl.attr('spaceFollow').exists:
        attrEnum = mc.attributeQuery( 'spaceFollow' , node = ctrl , listEnum = True )[0].split(':')
        ctrl.attr('spaceFollow').value = attrEnum.index(following)
    
    tmpGrp.snap(grp)
    
    for attr in ('tx','ty','tz','rx','ry','rz') :
        if not ctrl.attr(attr).lock:
            ctrl.attr(attr).value = tmp.attr(attr).value
    
    # Cleanup
    mc.delete(tmpGrp)
    mc.delete(prxy)

    mc.select(ctrl)
    
def armMatch( nameSpace = '' , elem = '' ) :

    sel = mc.ls( sl = True )[0]
    
    if '_L_' in sel :
        side = '_L_'
    else :
        side = '_R_'

    #-- Arm control
    arm = '%sArm%s%sCtrl' %( nameSpace , elem , side )
    
    #-- Fk control
    upArmFkCtrl = '%sUpArm%sFk%sCtrl' %( nameSpace , elem , side )
    foreArmFkCtrl = '%sForearm%sFk%sCtrl' %( nameSpace , elem , side )
    wristFkCtrl = '%sWrist%sFk%sCtrl' %( nameSpace , elem , side )
    upArmFkGmblCtrl = '%sUpArm%sFkGmbl%sCtrl' %( nameSpace , elem , side )
    foreArmFkGmblCtrl = '%sForearm%sFkGmbl%sCtrl' %( nameSpace , elem , side )
    wristFkGmblCtrl = '%sWrist%sFkGmbl%sCtrl' %( nameSpace , elem , side )
    
    #-- Ik control
    upArmIkCtrl = '%sUpArm%sIk%sCtrl' %( nameSpace , elem , side )
    elbowIkCtrl = '%sElbow%sIk%sCtrl' %( nameSpace , elem , side )
    wristIkCtrl = '%sWrist%sIk%sCtrl' %( nameSpace , elem , side )
    upArmIkGmblCtrl = '%sUpArm%sIkGmbl%sCtrl' %( nameSpace , elem , side )
    wristIkGmblCtrl = '%sWrist%sIkGmbl%sCtrl' %( nameSpace , elem , side )
    
    #-- Fk joint
    upArmFkJnt = '%sUpArm%sFk%sJnt' %( nameSpace , elem , side )
    foreArmFkJnt = '%sForearm%sFk%sJnt' %( nameSpace , elem , side )
    wristFkJnt = '%sWrist%sFk%sJnt' %( nameSpace , elem , side )
    handFkJnt = '%sHand%sFk%sJnt' %( nameSpace , elem , side )
    
    #-- Ik joint
    upArmIkJnt = '%sUpArm%sIk%sJnt' %( nameSpace , elem , side )
    foreArmIkJnt = '%sForearm%sIk%sJnt' %( nameSpace , elem , side )
    wristIkJnt = '%sWrist%sIk%sJnt' %( nameSpace , elem , side )
    handIkJnt = '%sHand%sIk%sJnt' %( nameSpace , elem , side )
    
    #-- Template position
    upArm = mc.createNode( 'joint' )
    upArmGrp = mc.group( upArm )
    foreArm = mc.createNode( 'joint' )
    foreArmGrp = mc.group( foreArm )
    wrist = mc.createNode( 'joint' )
    wristGrp = mc.group( wrist )
    
    #-- Fk to Ik
    if mc.getAttr('%s.fkIk' %sel) == 0 :
        #-- Reset gimbal control
        for gmbl in ( upArmIkGmblCtrl , wristIkGmblCtrl ) :
            for attr in ( 't' , 'r' ) :
                mc.setAttr( '%s.%s' %(gmbl,attr) , 0 , 0 , 0 )
        
        #-- Adjust attribute
        mc.setAttr( '%s.rotateOrder' %upArm , mc.getAttr( '%s.rotateOrder' %upArmIkCtrl ))
        mc.setAttr( '%s.rotateOrder' %foreArm , mc.getAttr( '%s.rotateOrder' %elbowIkCtrl ))
        mc.setAttr( '%s.rotateOrder' %wrist , mc.getAttr( '%s.rotateOrder' %wristIkCtrl ))
        
        wristJntOri = mc.getAttr( '%s.jointOrient' %wristIkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %wrist , wristJntOri[0] , wristJntOri[1] , wristJntOri[2] )
        
        #-- Adjust position
        # upArm
        mc.delete( mc.parentConstraint ( mc.listRelatives( upArmIkCtrl , p = True ) , upArmGrp , mo = False ))
        mc.delete( mc.parentConstraint ( upArmFkJnt , upArm , mo = False ))
        upArmPosi = mc.getAttr( '%s.t' %upArm )[0]
        mc.setAttr( '%s.t' %upArmIkCtrl , upArmPosi[0] , upArmPosi[1] , upArmPosi[2] )
        
        # wrist
        mc.delete( mc.parentConstraint ( mc.listRelatives( wristIkCtrl , p = True ) , wristGrp , mo = False ))
        mc.delete( mc.parentConstraint ( wristFkJnt , wrist , mo = False ))
        wristPosi = mc.getAttr( '%s.t' %wrist )[0]
        wristOri = mc.getAttr( '%s.r' %wrist )[0]
        mc.setAttr( '%s.t' %wristIkCtrl , wristPosi[0] , wristPosi[1] , wristPosi[2] )
        mc.setAttr( '%s.r' %wristIkCtrl , wristOri[0] , wristOri[1] , wristOri[2] )
        
        # elbow
        mc.delete( mc.parentConstraint ( mc.listRelatives( elbowIkCtrl , p = True ) , foreArmGrp , mo = False ))
        mc.delete( mc.parentConstraint ( foreArmFkJnt , foreArm , mo = False ))
        elbowPosi = mc.getAttr( '%s.t' %foreArm )[0]
        mc.setAttr( '%s.t' %elbowIkCtrl , elbowPosi[0] , elbowPosi[1] , elbowPosi[2] )
        
        #-- Adjust twist and stretch
        foreArmFkPos = mc.createNode( 'joint' )
        foreArmFkOriGrp = mc.group( foreArmFkPos )
        wristFkPos = mc.createNode( 'joint' )
        wristFkOriGrp = mc.group( wristFkPos )
        
        mc.delete( mc.parentConstraint ( mc.listRelatives( foreArmFkCtrl , p = True ) , foreArmFkOriGrp , mo = False ))
        mc.delete( mc.parentConstraint ( foreArmFkJnt , foreArmFkPos , mo = False ))
        mc.delete( mc.parentConstraint ( mc.listRelatives( wristFkCtrl , p = True ) , wristFkOriGrp , mo = False ))
        mc.delete( mc.parentConstraint ( wristFkJnt , wristFkPos , mo = False ))
        
        mc.setAttr( '%s.twist' %wristIkCtrl , 0 )
        mc.setAttr( '%s.autoStretch' %wristIkCtrl , 0 )
        upArmStretch = ( mc.getAttr( '%s.stretch' %upArmFkCtrl ) + mc.getAttr( '%s.tx' %foreArmFkPos ))
        mc.setAttr( '%s.UpArmStretch' %wristIkCtrl , upArmStretch )
        foreArmStretch = ( mc.getAttr( '%s.stretch' %foreArmFkCtrl ) + mc.getAttr( '%s.tx' %wristFkPos ))
        mc.setAttr( '%s.ForearmStretch' %wristIkCtrl , foreArmStretch )
        
        mc.delete( foreArmFkOriGrp , wristFkOriGrp )
        
        #-- Switch fk to ik
        mc.setAttr( '%s.fkIk' %arm , 1 )
    
    #-- Ik to Fk
    elif mc.getAttr('%s.fkIk' %sel) == 1 :
        #-- Reset gimbal control
        for gmbl in ( upArmFkGmblCtrl , foreArmFkGmblCtrl , wristFkGmblCtrl ) :
            for attr in ( 't' , 'r' ) :
                mc.setAttr( '%s.%s' %(gmbl,attr) , 0 , 0 , 0 )
                
        #-- Adjust attribute
        mc.setAttr( '%s.rotateOrder' %upArm , mc.getAttr( '%s.rotateOrder' %upArmFkCtrl ))
        mc.setAttr( '%s.rotateOrder' %foreArm , mc.getAttr( '%s.rotateOrder' %foreArmFkCtrl ))
        mc.setAttr( '%s.rotateOrder' %wrist , mc.getAttr( '%s.rotateOrder' %wristFkCtrl ))
        
        upArmJntOri = mc.getAttr( '%s.jointOrient' %upArmFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %upArm , upArmJntOri[0] , upArmJntOri[1] , upArmJntOri[2] )
        
        foreArmJntOri = mc.getAttr( '%s.jointOrient' %foreArmFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %foreArm , foreArmJntOri[0] , foreArmJntOri[1] , foreArmJntOri[2] )
        
        wristJntOri = mc.getAttr( '%s.jointOrient' %wristFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %wrist , wristJntOri[0] , wristJntOri[1] , wristJntOri[2] )
        
        #-- Adjust position
        # upArm
        mc.delete( mc.parentConstraint ( mc.listRelatives( upArmFkCtrl , p = True ) , upArmGrp , mo = False ))
        mc.delete( mc.parentConstraint ( upArmIkJnt , upArm , mo = False ))
        upArmPosi = mc.getAttr( '%s.t' %upArm )[0]
        upArmOri = mc.getAttr( '%s.r' %upArm )[0]
        mc.setAttr( '%s.t' %upArmFkCtrl , upArmPosi[0] , upArmPosi[1] , upArmPosi[2] )
        mc.setAttr( '%s.r' %upArmFkCtrl , upArmOri[0] , upArmOri[1] , upArmOri[2] )
        
        mc.setAttr( '%s.stretch' %upArmFkCtrl , 0 )
        mc.setAttr( '%s.stretch' %upArmFkCtrl , (( mc.getAttr( '%s.ty' %foreArmIkJnt ) / ( mc.getAttr( '%s.ty' %foreArmFkJnt )) - 1 ) * 10 ))
        
        # foreArm
        mc.delete( mc.parentConstraint ( mc.listRelatives( foreArmFkCtrl , p = True ) , foreArmGrp , mo = False ))
        mc.delete( mc.parentConstraint ( foreArmIkJnt , foreArm , mo = False ))
        foreArmPosi = mc.getAttr( '%s.t' %foreArm )[0]
        foreArmOri = mc.getAttr( '%s.r' %foreArm )[0]
        mc.setAttr( '%s.t' %foreArmFkCtrl , foreArmPosi[0] , foreArmPosi[1] , foreArmPosi[2] )
        mc.setAttr( '%s.r' %foreArmFkCtrl , foreArmOri[0] , foreArmOri[1] , foreArmOri[2] )
        
        mc.setAttr( '%s.stretch' %foreArmFkCtrl , 0 )
        mc.setAttr( '%s.stretch' %foreArmFkCtrl , (( mc.getAttr( '%s.ty' %wristIkJnt ) / ( mc.getAttr( '%s.ty' %wristFkJnt )) - 1 ) * 10 ))
        
        # wrist
        mc.delete( mc.parentConstraint ( mc.listRelatives( wristFkCtrl , p = True ) , wristGrp , mo = False ))
        mc.delete( mc.parentConstraint ( wristIkJnt , wrist , mo = False ))
        wristPosi = mc.getAttr( '%s.t' %wrist )[0]
        wristOri = mc.getAttr( '%s.r' %wrist )[0]
        mc.setAttr( '%s.t' %wristFkCtrl , wristPosi[0] , wristPosi[1] , wristPosi[2] )
        mc.setAttr( '%s.r' %wristFkCtrl , wristOri[0] , wristOri[1] , wristOri[2] )
        
        #-- Switch fk to ik
        mc.setAttr( '%s.fkIk' %arm , 0 )
        
    #-- Cleanup
    mc.delete( upArmGrp , foreArmGrp , wristGrp )
    mc.select( sel )


def legMatch( nameSpace = '' , elem = '' ) :
    sel = mc.ls( sl = True )[0]
    
    if '_L_' in sel :
        side = '_L_'
    else :
        side = '_R_'
    
    #-- Leg control
    leg = '%sLeg%s%sCtrl' %( nameSpace , elem , side )
    
    #-- Fk control
    upLegFkCtrl = '%sUpLeg%sFk%sCtrl' %( nameSpace , elem , side )
    lowLegFkCtrl = '%sLowLeg%sFk%sCtrl' %( nameSpace , elem , side )
    ankleFkCtrl = '%sAnkle%sFk%sCtrl' %( nameSpace , elem , side )
    ballFkCtrl = '%sBall%sFk%sCtrl' %( nameSpace , elem , side )
    upLegFkGmblCtrl = '%sUpLeg%sFkGmbl%sCtrl' %( nameSpace , elem , side )
    lowLegFkGmblCtrl = '%sLowLeg%sFkGmbl%sCtrl' %( nameSpace , elem , side )
    ankleFkGmblCtrl = '%sAnkle%sFkGmbl%sCtrl' %( nameSpace , elem , side )
    ballFkGmblCtrl = '%sBall%sFkGmbl%sCtrl' %( nameSpace , elem , side )
    
    #-- Ik control
    upLegIkCtrl = '%sUpLeg%sIk%sCtrl' %( nameSpace , elem , side )
    kneeIkCtrl = '%sKnee%sIk%sCtrl' %( nameSpace , elem , side )
    ankleIkCtrl = '%sAnkle%sIk%sCtrl' %( nameSpace , elem , side )
    upLegIkGmblCtrl = '%sUpLeg%sIkGmbl%sCtrl' %( nameSpace , elem , side )
    ankleIkGmblCtrl = '%sAnkle%sIkGmbl%sCtrl' %( nameSpace , elem , side )
    
    #-- Fk joint
    upLegFkJnt = '%sUpLeg%sFk%sJnt' %( nameSpace , elem , side )
    lowLegFkJnt = '%sLowLeg%sFk%sJnt' %( nameSpace , elem , side )
    ankleFkJnt = '%sAnkle%sFk%sJnt' %( nameSpace , elem , side )
    ballFkJnt = '%sBall%sFk%sJnt' %( nameSpace , elem , side )
    toeFkJnt = '%sToe%sFk%sJnt' %( nameSpace , elem , side )
    
    #-- Ik joint
    upLegIkJnt = '%sUpLeg%sIk%sJnt' %( nameSpace , elem , side )
    lowLegIkJnt = '%sLowLeg%sIk%sJnt' %( nameSpace , elem , side )
    ankleIkJnt = '%sAnkle%sIk%sJnt' %( nameSpace , elem , side )
    ballIkJnt = '%sBall%sIk%sJnt' %( nameSpace , elem , side )
    toeIkJnt = '%sToe%sIk%sJnt' %( nameSpace , elem , side )
    
    #-- Template position
    upLeg = mc.createNode( 'joint' )
    upLegGrp = mc.group( upLeg )
    lowLeg = mc.createNode( 'joint' )
    lowLegGrp = mc.group( lowLeg )
    ankle = mc.createNode( 'joint' )
    ankleGrp = mc.group( ankle )
    
    #-- Fk to Ik
    if mc.getAttr('%s.fkIk' %sel) == 0 :
        #-- Reset gimbal control
        for gmbl in ( upLegIkGmblCtrl , ankleIkGmblCtrl ) :
            for attr in ( 't' , 'r' ) :
                mc.setAttr( '%s.%s' %(gmbl,attr) , 0 , 0 , 0 )

        #-- Adjust attribute
        mc.setAttr( '%s.rotateOrder' %upLeg , mc.getAttr( '%s.rotateOrder' %upLegIkCtrl ))
        mc.setAttr( '%s.rotateOrder' %lowLeg , mc.getAttr( '%s.rotateOrder' %kneeIkCtrl ))
        mc.setAttr( '%s.rotateOrder' %ankle , mc.getAttr( '%s.rotateOrder' %ankleIkCtrl ))
        
        upLegJntOri = mc.getAttr( '%s.jointOrient' %upLegIkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %upLeg , upLegJntOri[0] , upLegJntOri[1] , upLegJntOri[2] )
        
        ankleJntOri = mc.getAttr( '%s.jointOrient' %ankleIkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %ankle , ankleJntOri[0] , ankleJntOri[1] , ankleJntOri[2] )
        
        #-- Adjust position
        # upLeg
        mc.delete( mc.parentConstraint ( mc.listRelatives( upLegIkCtrl , p = True ) , upLegGrp , mo = False ))
        mc.delete( mc.parentConstraint ( upLegFkJnt , upLeg , mo = False ))
        upLegPosi = mc.getAttr( '%s.t' %upLeg )[0]
        mc.setAttr( '%s.t' %upLegIkCtrl , upLegPosi[0] , upLegPosi[1] , upLegPosi[2] )
        
        # ankle
        mc.delete( mc.parentConstraint ( mc.listRelatives( ankleIkCtrl , p = True ) , ankleGrp , mo = False ))
        mc.delete( mc.parentConstraint ( ankleFkJnt , ankle , mo = False ))
        anklePosi = mc.getAttr( '%s.t' %ankle )[0]
        ankleOri = mc.getAttr( '%s.r' %ankle )[0]
        mc.setAttr( '%s.t' %ankleIkCtrl , anklePosi[0] , anklePosi[1] , anklePosi[2] )
        mc.setAttr( '%s.r' %ankleIkCtrl , ankleOri[0] , ankleOri[1] , ankleOri[2] )
        
        # knee
        mc.delete( mc.parentConstraint ( mc.listRelatives( kneeIkCtrl , p = True ) , lowLegGrp , mo = False ))
        mc.delete( mc.parentConstraint ( lowLegFkJnt , lowLeg , mo = False ))
        kneePosi = mc.getAttr( '%s.t' %lowLeg )[0]
        mc.setAttr( '%s.t' %kneeIkCtrl , kneePosi[0] , kneePosi[1] , kneePosi[2] )
        
        # ball
        if mc.objExists(ballFkJnt) :
            mc.setAttr( '%s.ballRoll' %ankleIkCtrl , 0 )
            mc.setAttr( '%s.toeBend' %ankleIkCtrl , 0 )
            mc.setAttr( '%s.toeBend' %ankleIkCtrl , ( mc.getAttr( '%s.rx' %ballFkCtrl )) )
        
        #-- Adjust twist and stretch
        lowLegFkPos = mc.createNode( 'joint' )
        lowLegFkOriGrp = mc.group( lowLegFkPos )
        ankleFkPos = mc.createNode( 'joint' )
        ankleFkOriGrp = mc.group( ankleFkPos )
        
        mc.delete( mc.parentConstraint ( mc.listRelatives( lowLegFkCtrl , p = True ) , lowLegFkOriGrp , mo = False ))
        mc.delete( mc.parentConstraint ( lowLegFkJnt , lowLegFkPos , mo = False ))
        mc.delete( mc.parentConstraint ( mc.listRelatives( ankleFkCtrl , p = True ) , ankleFkOriGrp , mo = False ))
        mc.delete( mc.parentConstraint ( ankleFkJnt , ankleFkPos , mo = False ))

        mc.setAttr( '%s.twist' %ankleIkCtrl , 0 )
        mc.setAttr( '%s.autoStretch' %ankleIkCtrl , 0 )
        upLegStretch = ( mc.getAttr( '%s.stretch' %upLegFkCtrl ) + (- mc.getAttr( '%s.ty' %lowLegFkPos )))
        mc.setAttr( '%s.UpLeg%sStretch' %(ankleIkCtrl,elem) , upLegStretch )
        lowLegStretch = ( mc.getAttr( '%s.stretch' %lowLegFkCtrl ) + (- mc.getAttr( '%s.ty' %ankleFkPos )))
        mc.setAttr( '%s.LowLeg%sStretch' %(ankleIkCtrl,elem) , lowLegStretch )
        
        mc.delete( lowLegFkOriGrp , ankleFkOriGrp )

        #-- Switch fk to ik
        mc.setAttr( '%s.fkIk' %leg , 1 )
    
    #-- Ik to Fk
    elif mc.getAttr('%s.fkIk' %sel) == 1 :
        #-- Reset gimbal control
        for gmbl in ( upLegFkGmblCtrl , lowLegFkGmblCtrl , ankleFkGmblCtrl , ballFkGmblCtrl ) :
            for attr in ( 't' , 'r' ) :
                if mc.objExists(gmbl):
                    mc.setAttr( '%s.%s' %(gmbl,attr) , 0 , 0 , 0 )

        #-- Adjust attribute
        mc.setAttr( '%s.rotateOrder' %upLeg , mc.getAttr( '%s.rotateOrder' %upLegFkCtrl ))
        mc.setAttr( '%s.rotateOrder' %lowLeg , mc.getAttr( '%s.rotateOrder' %lowLegFkCtrl ))
        mc.setAttr( '%s.rotateOrder' %ankle , mc.getAttr( '%s.rotateOrder' %ankleFkCtrl ))
        
        upLegJntOri = mc.getAttr( '%s.jointOrient' %upLegFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %upLeg , upLegJntOri[0] , upLegJntOri[1] , upLegJntOri[2] )
        
        lowLegJntOri = mc.getAttr( '%s.jointOrient' %lowLegFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %lowLeg , lowLegJntOri[0] , lowLegJntOri[1] , lowLegJntOri[2] )
        
        ankleJntOri = mc.getAttr( '%s.jointOrient' %ankleFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %ankle , ankleJntOri[0] , ankleJntOri[1] , ankleJntOri[2] )
        
        #-- Adjust position
        # upLeg
        mc.delete( mc.parentConstraint ( mc.listRelatives( upLegFkCtrl , p = True ) , upLegGrp , mo = False ))
        mc.delete( mc.parentConstraint ( upLegIkJnt , upLeg , mo = False ))
        upLegPosi = mc.getAttr( '%s.t' %upLeg )[0]
        upLegOri = mc.getAttr( '%s.r' %upLeg )[0]
        mc.setAttr( '%s.t' %upLegFkCtrl , upLegPosi[0] , upLegPosi[1] , upLegPosi[2] )
        mc.setAttr( '%s.r' %upLegFkCtrl , upLegOri[0] , upLegOri[1] , upLegOri[2] )
        
        mc.setAttr( '%s.stretch' %upLegFkCtrl , 0 )
        mc.setAttr( '%s.stretch' %upLegFkCtrl , (( mc.getAttr( '%s.ty' %lowLegIkJnt ) / ( mc.getAttr( '%s.ty' %lowLegFkJnt )) - 1 ) * 10 ))
        
        # lowLeg
        mc.delete( mc.parentConstraint ( mc.listRelatives( lowLegFkCtrl , p = True ) , lowLegGrp , mo = False ))
        mc.delete( mc.parentConstraint ( lowLegIkJnt , lowLeg , mo = False ))
        lowLegPosi = mc.getAttr( '%s.t' %lowLeg )[0]
        lowLegOri = mc.getAttr( '%s.r' %lowLeg )[0]
        mc.setAttr( '%s.t' %lowLegFkCtrl , lowLegPosi[0] , lowLegPosi[1] , lowLegPosi[2] )
        mc.setAttr( '%s.r' %lowLegFkCtrl , lowLegOri[0] , lowLegOri[1] , lowLegOri[2] )
        
        mc.setAttr( '%s.stretch' %lowLegFkCtrl , 0 )
        mc.setAttr( '%s.stretch' %lowLegFkCtrl , (( mc.getAttr( '%s.ty' %ankleIkJnt ) / ( mc.getAttr( '%s.ty' %ankleFkJnt )) - 1 ) * 10 ))
        
        # ankle
        mc.delete( mc.parentConstraint ( mc.listRelatives( ankleFkCtrl , p = True ) , ankleGrp , mo = False ))
        mc.delete( mc.parentConstraint ( ankleIkJnt , ankle , mo = False ))
        anklePosi = mc.getAttr( '%s.t' %ankle )[0]
        ankleOri = mc.getAttr( '%s.r' %ankle )[0]
        mc.setAttr( '%s.t' %ankleFkCtrl , anklePosi[0] , anklePosi[1] , anklePosi[2] )
        mc.setAttr( '%s.r' %ankleFkCtrl , ankleOri[0] , ankleOri[1] , ankleOri[2] )
        
        # ball
        if mc.objExists(ballFkJnt) :
            mc.setAttr( '%s.r' %ballFkCtrl , 0 , 0 , 0 )
            mc.setAttr( '%s.rx' %ballFkCtrl , ( - mc.getAttr( '%s.ballRoll' %ankleIkCtrl )) - ( - mc.getAttr( '%s.toeBend' %ankleIkCtrl )) )
            
            mc.setAttr( '%s.stretch' %ballFkCtrl , 0 )
            mc.setAttr( '%s.stretch' %ballFkCtrl , mc.getAttr( '%s.toeStretch' %ankleIkCtrl ))

        #-- Switch fk to ik
        mc.setAttr( '%s.fkIk' %leg , 0 )
        
    #-- Cleanup
    mc.delete( upLegGrp , lowLegGrp , ankleGrp )
    mc.select( sel )


def legAnimalMatch( nameSpace = '' , elem = '' ) :
    sel = mc.ls( sl = True )[0]
    
    if '_L_' in sel :
        side = '_L_'
    else :
        side = '_R_'
    
    matchObj = re.search( r'.*(Leg)(.+)_(.*)_(Ctrl)', sel )

    if matchObj:
        elem = matchObj.group(2)
    else :
        elem = ''
    
    #-- Leg control
    leg = '%sLeg%s%sCtrl' %( nameSpace , elem , side )
    
    #-- Fk control
    upLegFkCtrl = '%sUpLeg%sFk%sCtrl' %( nameSpace , elem , side )
    midLegFkCtrl = '%sMidLeg%sFk%sCtrl' %( nameSpace , elem , side )
    lowLegFkCtrl = '%sLowLeg%sFk%sCtrl' %( nameSpace , elem , side )
    ankleFkCtrl = '%sAnkle%sFk%sCtrl' %( nameSpace , elem , side )
    ballFkCtrl = '%sBall%sFk%sCtrl' %( nameSpace , elem , side )
    
    #-- Ik control
    upLegIkCtrl = '%sUpLeg%sIk%sCtrl' %( nameSpace , elem , side )
    kneeIkCtrl = '%sKnee%sIk%sCtrl' %( nameSpace , elem , side )
    ankleIkCtrl = '%sAnkle%sIk%sCtrl' %( nameSpace , elem , side )
    
    #-- Fk joint
    upLegFkJnt = '%sUpLeg%sFk%sJnt' %( nameSpace , elem , side )
    midLegFkJnt = '%sMidLeg%sFk%sJnt' %( nameSpace , elem , side )
    lowLegFkJnt = '%sLowLeg%sFk%sJnt' %( nameSpace , elem , side )
    ankleFkJnt = '%sAnkle%sFk%sJnt' %( nameSpace , elem , side )
    ballFkJnt = '%sBall%sFk%sJnt' %( nameSpace , elem , side )
    toeFkJnt = '%sToe%sFk%sJnt' %( nameSpace , elem , side )
    
    #-- Ik joint
    upLegIkJnt = '%sUpLeg%sIk%sJnt' %( nameSpace , elem , side )
    midLegIkJnt = '%sMidLeg%sIk%sJnt' %( nameSpace , elem , side )
    lowLegIkJnt = '%sLowLeg%sIk%sJnt' %( nameSpace , elem , side )
    ankleIkJnt = '%sAnkle%sIk%sJnt' %( nameSpace , elem , side )
    ballIkJnt = '%sBall%sIk%sJnt' %( nameSpace , elem , side )
    toeIkJnt = '%sToe%sIk%sJnt' %( nameSpace , elem , side )
    
    #-- Template position
    upLeg = mc.createNode( 'joint' )
    upLegGrp = mc.group( upLeg )
    midLeg = mc.createNode( 'joint' )
    midLegGrp = mc.group( midLeg )
    lowLeg = mc.createNode( 'joint' )
    lowLegGrp = mc.group( lowLeg )
    ankle = mc.createNode( 'joint' )
    ankleGrp = mc.group( ankle )
    
    #-- Fk to Ik
    if mc.getAttr('%s.fkIk' %sel) == 0 :
        #-- Adjust attribute
        mc.setAttr( '%s.rotateOrder' %upLeg , mc.getAttr( '%s.rotateOrder' %upLegIkCtrl ))
        mc.setAttr( '%s.rotateOrder' %lowLeg , mc.getAttr( '%s.rotateOrder' %kneeIkCtrl ))
        mc.setAttr( '%s.rotateOrder' %ankle , mc.getAttr( '%s.rotateOrder' %ankleIkCtrl ))
        
        upLegJntOri = mc.getAttr( '%s.jointOrient' %upLegIkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %upLeg , upLegJntOri[0] , upLegJntOri[1] , upLegJntOri[2] )
        
        ankleJntOri = mc.getAttr( '%s.jointOrient' %ankleIkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %ankle , ankleJntOri[0] , ankleJntOri[1] , ankleJntOri[2] )
        
        #-- Adjust position
        # upLeg
        mc.delete( mc.parentConstraint ( mc.listRelatives( upLegIkCtrl , p = True ) , upLegGrp , mo = False ))
        mc.delete( mc.parentConstraint ( upLegFkJnt , upLeg , mo = False ))
        upLegPosi = mc.getAttr( '%s.t' %upLeg )[0]
        mc.setAttr( '%s.t' %upLegIkCtrl , upLegPosi[0] , upLegPosi[1] , upLegPosi[2] )
        
        # ankle
        mc.delete( mc.parentConstraint ( mc.listRelatives( ankleIkCtrl , p = True ) , ankleGrp , mo = False ))
        mc.delete( mc.parentConstraint ( ankleFkJnt , ankle , mo = False ))
        anklePosi = mc.getAttr( '%s.t' %ankle )[0]
        ankleOri = mc.getAttr( '%s.r' %ankle )[0]
        mc.setAttr( '%s.t' %ankleIkCtrl , anklePosi[0] , anklePosi[1] , anklePosi[2] )
        mc.setAttr( '%s.r' %ankleIkCtrl , ankleOri[0] , ankleOri[1] , ankleOri[2] )
        
        # knee
        mc.delete( mc.parentConstraint ( mc.listRelatives( kneeIkCtrl , p = True ) , lowLegGrp , mo = False ))
        mc.delete( mc.parentConstraint ( lowLegFkJnt , lowLeg , mo = False ))
        kneePosi = mc.getAttr( '%s.t' %lowLeg )[0]
        mc.setAttr( '%s.t' %kneeIkCtrl , kneePosi[0] , kneePosi[1] , kneePosi[2] )
        
        # ball
        mc.setAttr( '%s.toeBend' %ankleIkCtrl , 0 )
        mc.setAttr( '%s.toeBend' %ankleIkCtrl , -( mc.getAttr( '%s.rx' %ballFkCtrl )) )

        #-- Adjust twist and stretch
        mc.setAttr( '%s.twistLowLeg' %ankleIkCtrl , 0 )
        mc.setAttr( '%s.autoStretch' %ankleIkCtrl , 0 )
        upLegStretch = mc.getAttr( '%s.stretch' %upLegFkCtrl )
        mc.setAttr( '%s.UpLegStretch' %ankleIkCtrl , upLegStretch )
        midLegStretch = mc.getAttr( '%s.stretch' %midLegFkCtrl )
        mc.setAttr( '%s.MidLegStretch' %ankleIkCtrl , midLegStretch )
        lowLegStretch = mc.getAttr( '%s.stretch' %lowLegFkCtrl )
        mc.setAttr( '%s.LowLegStretch' %ankleIkCtrl , lowLegStretch )

        #-- Switch fk to ik
        mc.setAttr( '%s.fkIk' %leg , 1 )
    
    #-- Ik to Fk
    elif mc.getAttr('%s.fkIk' %sel) == 1 :
        #-- Adjust attribute
        mc.setAttr( '%s.rotateOrder' %upLeg , mc.getAttr( '%s.rotateOrder' %upLegFkCtrl ))
        mc.setAttr( '%s.rotateOrder' %midLeg , mc.getAttr( '%s.rotateOrder' %midLegFkCtrl ))
        mc.setAttr( '%s.rotateOrder' %lowLeg , mc.getAttr( '%s.rotateOrder' %lowLegFkCtrl ))
        mc.setAttr( '%s.rotateOrder' %ankle , mc.getAttr( '%s.rotateOrder' %ankleFkCtrl ))
        
        upLegJntOri = mc.getAttr( '%s.jointOrient' %upLegFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %upLeg , upLegJntOri[0] , upLegJntOri[1] , upLegJntOri[2] )
        
        midLegJntOri = mc.getAttr( '%s.jointOrient' %midLegFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %midLeg , midLegJntOri[0] , midLegJntOri[1] , midLegJntOri[2] )
        
        lowLegJntOri = mc.getAttr( '%s.jointOrient' %lowLegFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %lowLeg , lowLegJntOri[0] , lowLegJntOri[1] , lowLegJntOri[2] )
        
        ankleJntOri = mc.getAttr( '%s.jointOrient' %ankleFkCtrl )[0]
        mc.setAttr( '%s.jointOrient' %ankle , ankleJntOri[0] , ankleJntOri[1] , ankleJntOri[2] )
        
        #-- Adjust position
        # upLeg
        mc.delete( mc.parentConstraint ( mc.listRelatives( upLegFkCtrl , p = True ) , upLegGrp , mo = False ))
        mc.delete( mc.parentConstraint ( upLegIkJnt , upLeg , mo = False ))
        upLegPosi = mc.getAttr( '%s.t' %upLeg )[0]
        upLegOri = mc.getAttr( '%s.r' %upLeg )[0]
        mc.setAttr( '%s.t' %upLegFkCtrl , upLegPosi[0] , upLegPosi[1] , upLegPosi[2] )
        mc.setAttr( '%s.r' %upLegFkCtrl , upLegOri[0] , upLegOri[1] , upLegOri[2] )
        
        mc.setAttr( '%s.stretch' %upLegFkCtrl , 0 )
        mc.setAttr( '%s.stretch' %upLegFkCtrl , (( mc.getAttr( '%s.ty' %midLegIkJnt ) / ( mc.getAttr( '%s.ty' %midLegFkJnt )) - 1 ) * 10 ))
        
        # midLeg
        mc.delete( mc.parentConstraint ( mc.listRelatives( midLegFkCtrl , p = True ) , midLegGrp , mo = False ))
        mc.delete( mc.parentConstraint ( midLegIkJnt , midLeg , mo = False ))
        midLegPosi = mc.getAttr( '%s.t' %midLeg )[0]
        midLegOri = mc.getAttr( '%s.r' %midLeg )[0]
        mc.setAttr( '%s.t' %midLegFkCtrl , midLegPosi[0] , midLegPosi[1] , midLegPosi[2] )
        mc.setAttr( '%s.r' %midLegFkCtrl , midLegOri[0] , midLegOri[1] , midLegOri[2] )
        
        mc.setAttr( '%s.stretch' %midLegFkCtrl , 0 )
        mc.setAttr( '%s.stretch' %midLegFkCtrl , (( mc.getAttr( '%s.ty' %lowLegIkJnt ) / ( mc.getAttr( '%s.ty' %lowLegFkJnt )) - 1 ) * 10 ))
        
        # lowLeg
        mc.delete( mc.parentConstraint ( mc.listRelatives( lowLegFkCtrl , p = True ) , lowLegGrp , mo = False ))
        mc.delete( mc.parentConstraint ( lowLegIkJnt , lowLeg , mo = False ))
        lowLegPosi = mc.getAttr( '%s.t' %lowLeg )[0]
        lowLegOri = mc.getAttr( '%s.r' %lowLeg )[0]
        mc.setAttr( '%s.t' %lowLegFkCtrl , lowLegPosi[0] , lowLegPosi[1] , lowLegPosi[2] )
        mc.setAttr( '%s.r' %lowLegFkCtrl , lowLegOri[0] , lowLegOri[1] , lowLegOri[2] )
        
        mc.setAttr( '%s.stretch' %lowLegFkCtrl , 0 )
        mc.setAttr( '%s.stretch' %lowLegFkCtrl , (( mc.getAttr( '%s.ty' %ankleIkJnt ) / ( mc.getAttr( '%s.ty' %ankleFkJnt )) - 1 ) * 10 ))
        
        # ankle
        mc.delete( mc.parentConstraint ( mc.listRelatives( ankleFkCtrl , p = True ) , ankleGrp , mo = False ))
        mc.delete( mc.parentConstraint ( ankleIkJnt , ankle , mo = False ))
        anklePosi = mc.getAttr( '%s.t' %ankle )[0]
        ankleOri = mc.getAttr( '%s.r' %ankle )[0]
        mc.setAttr( '%s.t' %ankleFkCtrl , anklePosi[0] , anklePosi[1] , anklePosi[2] )
        mc.setAttr( '%s.r' %ankleFkCtrl , ankleOri[0] , ankleOri[1] , ankleOri[2] )
        
        # ball
        mc.setAttr( '%s.r' %ballFkCtrl , 0 , 0 , 0 )
        mc.setAttr( '%s.rx' %ballFkCtrl , -( mc.getAttr( '%s.toeBend' %ankleIkCtrl )) )

        #-- Switch fk to ik
        mc.setAttr( '%s.fkIk' %leg , 0 )
        
    #-- Cleanup
    mc.delete( upLegGrp , lowLegGrp , ankleGrp )
    mc.select( sel )