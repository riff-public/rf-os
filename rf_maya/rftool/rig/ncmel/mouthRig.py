import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  MOUTH RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , lipsInTmpCuv     = 'LipsIn_CuvTemp' ,
                         lipsOutTmpCuv    = 'LipsOut_CuvTemp' ,
                         jawTmpJnt        = 'JawLwr2_Jnt' ,
                         headTmpJnt       = 'Head_Jnt' ,
                         jawCtrl          = 'JawLwr1_Ctrl' ,
                         numDtlJnt        =  28 ,
                         skipJntCen       =  4 ,
                         skinGrp          = '' ,
                         elem             = '' ,
                         side             = '' ,
                 ):

        #-- Info
        elem = elem.capitalize()

        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        #-- Find Skip Joint
        listJntSkip = [ ]
        
        jntMidUpr = numDtlJnt / 4
        jntMidLwr = jntMidUpr * 3
        jntCnrRgt = numDtlJnt / 2
        jntCnrLft = jntCnrRgt * 2

        for each in ( jntMidUpr , jntMidLwr , jntCnrLft , jntCnrRgt) :
            listJntSkip.append(each)

        for i in range(1,skipJntCen+1) :
            jntMidUprPlus = jntMidUpr + i
            jntMidUprSub = jntMidUpr - i
            jntMidLwrPlus = jntMidLwr + i
            jntMidLwrSub = jntMidLwr - i
    
            for num in ( jntMidUprPlus , jntMidUprSub , jntMidLwrPlus , jntMidLwrSub ) :
                listJntSkip.append(num)

        #-- Calculate Open Mouth
        openValList = [ ]
        openAvgList = [ ]
        quaterJnt = ( numDtlJnt/4 ) - skipJntCen
        

        # self.animCuv = mc.createNode( 'animCurveTU' , name = 'openMouthValue' )
        # mc.setKeyframe( self.animCuv , t = 0 , v = 0.5 )
        # mc.setKeyframe( self.animCuv , t = quaterJnt , v = 1.0 )
        # mc.keyTangent( self.animCuv , e = True , a = True , t = (0,0) , outAngle = 90 , outWeight = 0 )
        # mc.keyTangent( self.animCuv , e = True , a = True , t = (quaterJnt,quaterJnt) , inAngle = 0 , inWeight = (quaterJnt/4)*3.5 )


        self.animCuv = mc.createNode( 'animCurveTU' , name = 'openMouthValue' )
        mc.setKeyframe( self.animCuv , t = 0 , v = 0.5 )
        mc.setKeyframe( self.animCuv , t = quaterJnt , v = 1.0 )
        mc.keyTangent( self.animCuv , e = True , a = True , t = (0,0) , outAngle = 0 , outWeight = (quaterJnt/4)*1.4 )
        mc.keyTangent( self.animCuv , e = True , a = True , t = (quaterJnt,quaterJnt) , inAngle = 0 , inWeight = (quaterJnt/4)*1.75 )

        for j in range( 1 , quaterJnt ) :
            val = mc.keyframe( self.animCuv , t= (j,j) , q = True , eval = True )[0]
            openAvgList.append(val)

        for k in range(2) :
            for i in openAvgList :
                openValList.append(i)
            for i in openAvgList[::-1] :
                openValList.append(i)

        #-- Calculate Seal Mouth
        maxAvgList = [ ]
        minAvgList = [ ]
        avgMax = 20.0 / (( numDtlJnt/2 )-1)
        maxStartValue = 0

        for j in range( 1 , numDtlJnt/2 ) :
            maxVal = maxStartValue + avgMax
            maxAvgList.append(maxVal)
            
            if j == 1 :
                minAvgList.append(0)
            elif j > (( numDtlJnt/2 )-1) :
                pass
            else :
                minVal = float(maxAvgList[j-2]) / 2
                minAvgList.append(minVal)

            maxStartValue+=avgMax

        #-- Calculate Seal Mouth Left
        maxAvgListLf = [ ]
        minAvgListLf = [ ]

        for avgMax in maxAvgList :
            maxAvgListLf.append(avgMax)
        for avgMin in minAvgList :
            minAvgListLf.append(avgMin)

        for l in range(1 , ( numDtlJnt/2 )) :
            maxAvgListLf.append(maxAvgList[-l])
            minAvgListLf.append(minAvgList[-l])

        #-- Calculate Seal Mouth Right
        maxAvgListRt = [ ]
        minAvgListRt = [ ]

        for avgMax in maxAvgList[::-1] :
            maxAvgListRt.append(avgMax)
        for avgMin in minAvgList[::-1] :
            minAvgListRt.append(avgMin)

        for l in range(1 , ( numDtlJnt/2 )) :
            maxAvgListRt.append(maxAvgList[l-1])
            minAvgListRt.append(minAvgList[l-1])

        #-- Create Mouth Skin Group Lips
        self.listMthStillGrp = rt.createNode ( 'transform', '%sRigStill_Grp' % elem )
        self.listMthSkinGrp = rt.createNode ( 'transform', '%sRigSkin_Grp' % elem )
        #-- Create Main Group Lips
        self.lipsSkinGrp = rt.createNode( 'transform' , 'Lips%sSkin%sGrp' %( elem , side ))

        #-- Create Loft Surface
        self.lipsNrb = core.Dag( mc.loft ( lipsInTmpCuv , lipsOutTmpCuv , n = 'Lips%s%sNrb' %( elem , side ) , ch = False , d = 3 ) [0] )

        #-- Add Attribute Lips Seal
        self.jawCtrl = core.Dag( jawCtrl )
        self.jawCtrlShape = core.Dag( self.jawCtrl.shape )
        self.jawCtrl.addTitleAttr( 'Lips' )
        self.jawCtrl.addAttr( 'LipsSealL' , 0 , 20 )
        self.jawCtrl.addAttr( 'LipsSealR' , 0 , 20 )

        #-- Create Detail Control & Joint
        numDtl = 1
        lengthDtl = 0 
        posiLength = 1 / float( numDtlJnt )
        openList = 0
        sealList = 0

        while numDtl <= numDtlJnt :
            #-- Set Position Joint
            self.rbnDtlJnt = rt.createNode( 'joint' , 'Lips%s%sDtl%sJnt' %( elem , numDtl , side ))
            self.rbnDtlJnt.attr('radius').v = 0.01
            self.rbnDtlJntZroGrp = rt.createNode( 'transform' , 'Lips%s%sDtlJntZro%sGrp' %( elem , numDtl , side ))
            self.rbnPosGrp = rt.createNode( 'transform' , 'Lips%s%sPos%sGrp' %( elem , numDtl , side ))
            self.posi = rt.createNode( 'pointOnSurfaceInfo' , 'Lips%s%sPos%sPosi' %( elem , numDtl , side ))
            self.lipsNrb.attr('worldSpace[0]') >> self.posi.attr('inputSurface')
            self.posi.attr('position') >> self.rbnPosGrp.attr('t')
            self.posi.attr('parameterU').v = 0.5
            self.posi.attr('parameterV').v = lengthDtl
            self.rbnDtlJnt.parent( self.rbnDtlJntZroGrp )
            self.rbnDtlJntZroGrp.snap( self.rbnPosGrp )
            self.rbnDtlJntZroGrp.parent( self.lipsSkinGrp )
            mc.delete( self.rbnPosGrp )

            #-- Add Attribute
            self.jawCtrlShape.addTitleAttr( 'Lips%s' %numDtl )
            self.jawCtrlShape.addAttr( 'Lips_%s_Open' %numDtl )
            self.jawCtrlShape.addAttr( 'Lips_%s_Seal' %numDtl )
            self.jawCtrlShape.addAttr( 'Lips_%s_L_Min' %numDtl )
            self.jawCtrlShape.addAttr( 'Lips_%s_L_Max' %numDtl )
            self.jawCtrlShape.addAttr( 'Lips_%s_R_Min' %numDtl )
            self.jawCtrlShape.addAttr( 'Lips_%s_R_Max' %numDtl )
            self.jawCtrlShape.attr('Lips_%s_Seal' %numDtl).v = 0.5

            #-- Set Default Open Mouth
            if numDtl in listJntSkip :
                self.jawCtrlShape.attr('Lips_%s_Open' %numDtl).v = 1
            else :
                self.jawCtrlShape.attr('Lips_%s_Open' %numDtl).v = openValList[openList]
                openList+=1

            if numDtl in ( jntCnrLft , jntCnrRgt ) :
                self.jawCtrlShape.attr('Lips_%s_Open' %numDtl).v = 0.5

            #-- Set Default Min/Max Seal
            if not numDtl in ( jntCnrLft , jntCnrRgt ) :
                self.jawCtrlShape.attr('Lips_%s_L_Min' %numDtl).v = minAvgListLf[sealList]
                self.jawCtrlShape.attr('Lips_%s_L_Max' %numDtl).v = maxAvgListLf[sealList]
                self.jawCtrlShape.attr('Lips_%s_R_Min' %numDtl).v = minAvgListRt[sealList]
                self.jawCtrlShape.attr('Lips_%s_R_Max' %numDtl).v = maxAvgListRt[sealList]
                sealList+=1

            #-- Create Node & Set Value & Connect Node
            self.remLf = rt.createNode( 'remapValue' , 'Lips%s%sLf%sRem' %( elem , numDtl , side ))
            self.remRt = rt.createNode( 'remapValue' , 'Lips%s%sRt%sRem' %( elem , numDtl , side ))
            self.pmaSum = rt.createNode( 'plusMinusAverage' , 'Lips%s%sSum%sPma' %( elem , numDtl , side ))
            self.pmaSub = rt.createNode( 'plusMinusAverage' , 'Lips%s%sSub%sPma' %( elem , numDtl , side ))
            self.pmaSeal = rt.createNode( 'plusMinusAverage' , 'Lips%s%sSeal%sPma' %( elem , numDtl , side ))
            self.cmpTotal = rt.createNode( 'clamp' , 'Lips%s%sTotal%sCmp' %( elem , numDtl , side ))
            self.cmp = rt.createNode( 'clamp' , 'Lips%s%s%sCmp' %( elem , numDtl , side ))
            self.rev = rt.createNode( 'reverse' , 'Lips%s%s%sRev' %( elem , numDtl , side ))
            self.pmaSub.attr('operation').v = 2
            self.pmaSeal.attr('operation').v = 2

            self.parsCons = core.Dag(mc.parentConstraint( headTmpJnt , jawTmpJnt , self.rbnDtlJntZroGrp , mo = True )[0])

            self.jawCtrlShape.attr( 'Lips_%s_Open' %numDtl ) >> self.pmaSeal.attr('i1[0]')
            self.jawCtrlShape.attr( 'Lips_%s_Open' %numDtl ) >> self.pmaSub.attr('i1[0]')
            self.jawCtrlShape.attr( 'Lips_%s_Open' %numDtl ) >> self.cmpTotal.attr('mnr')
            self.jawCtrlShape.attr( 'Lips_%s_Seal' %numDtl ) >> self.pmaSeal.attr('i1[1]')
            self.jawCtrlShape.attr( 'Lips_%s_L_Min' %numDtl ) >> self.remLf.attr('inputMin')
            self.jawCtrlShape.attr( 'Lips_%s_L_Max' %numDtl ) >> self.remLf.attr('inputMax')
            self.jawCtrlShape.attr( 'Lips_%s_R_Min' %numDtl ) >> self.remRt.attr('inputMin')
            self.jawCtrlShape.attr( 'Lips_%s_R_Max' %numDtl ) >> self.remRt.attr('inputMax')

            self.jawCtrl.attr( 'LipsSealL' ) >> self.pmaSum.attr('i1[0]')
            self.jawCtrl.attr( 'LipsSealR' ) >> self.pmaSum.attr('i1[1]')
            self.jawCtrl.attr( 'LipsSealL' ) >> self.remLf.attr('inputValue')
            self.jawCtrl.attr( 'LipsSealR' ) >> self.remRt.attr('inputValue')
            self.pmaSeal.attr( 'o1' ) >> self.remLf.attr('outputMax')
            self.pmaSeal.attr( 'o1' ) >> self.remRt.attr('outputMax')
            self.pmaSum.attr('o1') >> self.cmpTotal.attr('ipr')
            self.remLf.attr('outValue') >> self.pmaSub.attr('i1[1]')
            self.remRt.attr('outValue') >> self.pmaSub.attr('i1[2]')
            self.pmaSub.attr( 'o1' ) >> self.cmp.attr('ipr')
            self.cmp.attr( 'opr' ) >> self.cmpTotal.attr('mxr')
            self.cmp.attr('mnr').v = 0.5
            self.cmp.attr('mxr').v = 1

            if numDtl > (numDtlJnt/2) :
                self.cmpTotal.attr('opr') >> self.parsCons.attr('%sW1' %jawTmpJnt)
                self.parsCons.attr('%sW1' %jawTmpJnt) >> self.rev.attr('inputX')
                self.rev.attr( 'outputX' ) >> self.parsCons.attr('%sW0' %headTmpJnt)
            else :
                self.cmpTotal.attr('opr') >> self.parsCons.attr('%sW0' %headTmpJnt)
                self.parsCons.attr('%sW0' %headTmpJnt) >> self.rev.attr('inputX')
                self.rev.attr( 'outputX' ) >> self.parsCons.attr('%sW1' %jawTmpJnt)

            lengthDtl+=posiLength
            numDtl+=1
        
        #-- Adjust Hierarchy
        # mthJawLwr1 = mc.listRelatives(jawTmpJnt, p = True)
        mthJawGrp = 'mthJawJnt_Grp'
        mc.parent( self.lipsSkinGrp , mthJawGrp, headTmpJnt , self.listMthSkinGrp )
        mc.parent( jawCtrl, self.listMthStillGrp )

        mc.delete( self.lipsNrb , self.animCuv )
        mc.select( cl = True )