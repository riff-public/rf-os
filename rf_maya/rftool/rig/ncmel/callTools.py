import maya.cmds as mc
import maya.mel as mm
import pymel.core as pmc

textEditor = "C:/Program Files/Sublime Text 3/sublime_text.exe"
imagePath = "C:/Users/Reborn/Dropbox/script/pipeTools/ncmel/Pipeline/Playblast/test.jpg"

def show():
    
    ui = 'RigToolsUI'
    width = 350
        
    if pmc.window( ui , exists = True ):
        pmc.deleteUI( ui )
        
    window = pmc.window( ui , title = "Rig Tools v.1"  , width = width , height = 500 ,
                         mnb = True , mxb = False , sizeable = True , rtf = True , menuBar = True )
    
    mainLayout = pmc.rowColumnLayout( w = width  , nc = 1 , columnWidth = ( 1 , 348 ))
    with mainLayout :
        
        pictureLayout = pmc.rowColumnLayout( w = width  , nc = 1 , columnWidth = ( 1 , 346 ))
        with pictureLayout :
            pmc.picture( h = 60 , image = imagePath )
            
        tabs = pmc.tabLayout( bs = 'full' , height = 500 )
        with tabs :
            
            data = pmc.rowColumnLayout( 'Data' , nc = 1 , columnWidth = ( 1 , 340 ) )
            with data :
                dataDirFL = pmc.frameLayout ( label = 'Directory Path' )
                
                dataDirRCL = pmc.rowColumnLayout( nc = 4 , columnWidth = [(1,32),(2,281),(3,2),(4,25)] )
                with dataDirRCL :
                    pmc.text( label = 'Dir :' )
                    pmc.textField( 'dataDirTF' , en = False )
                    pmc.separator( vis = False )
                    pmc.button( label = '...' )
                
                pmc.separator( h = 2 , vis = False )    
                moduleRigFL = pmc.frameLayout ( label = 'Module Rig' )
                
                moduleRigDirRCL = pmc.rowColumnLayout( nc = 2 , columnWidth = [(1,32),(2,308)] )
                with moduleRigDirRCL :
                    pmc.text( label = 'Dir :' )
                    pmc.textField( 'moduleRigDirTF' , en = False )
                    
                pmc.separator( h = 2 , vis = False )  
                moduleRigRunRCL = pmc.rowColumnLayout( nc = 1 , columnWidth = (1,340) )
                with moduleRigRunRCL :
                    pmc.button( label = 'Run' , h = 40 )
                
                pmc.separator( h = 2 , vis = False )    
                ctrlShapeFL = pmc.frameLayout ( label = 'Controls Shape' )
                
                ctrlShapeDirRCL = pmc.rowColumnLayout( nc = 2 , columnWidth = [(1,32),(2,308)] )
                with ctrlShapeDirRCL :
                    pmc.text( label = 'Dir :' )
                    pmc.textField( 'ctrlShapeDirTF' , en = False )
                    
                pmc.separator( h = 2 , vis = False )  
                ctrlShapeRunRCL = pmc.rowColumnLayout( nc = 2 , columnWidth = [(1,170),(2,170)] )
                with ctrlShapeRunRCL :
                    pmc.button( label = 'Write Shape' , h = 40 )
                    pmc.button( label = 'Read Shape' , h = 40 )
                
                pmc.separator( h = 2 , vis = False )    
                skinWeightFL = pmc.frameLayout ( label = 'Skin Weight' )
                
                skinWeightDirRCL = pmc.rowColumnLayout( nc = 2 , columnWidth = [(1,32),(2,308)] )
                with skinWeightDirRCL :
                    pmc.text( label = 'Dir :' )
                    pmc.textField( 'skinWeightDirTF' , en = False )
                    
                pmc.separator( h = 2 , vis = False )  
                skinWeightRunRCL = pmc.rowColumnLayout( nc = 2 , columnWidth = [(1,170),(2,170)] )
                with skinWeightRunRCL :
                    pmc.button( label = 'Write Weight' , h = 40 )
                    pmc.button( label = 'Read Weight' , h = 40 )
                
                pmc.separator( h = 2 , vis = False )    
                ctrlAttrFL = pmc.frameLayout ( label = 'Controls Attribute' )
                
                ctrlAttrDirRCL = pmc.rowColumnLayout( nc = 2 , columnWidth = [(1,32),(2,308)] )
                with ctrlAttrDirRCL :
                    pmc.text( label = 'Dir :' )
                    pmc.textField( 'ctrlAttrDirTF' , en = False )
                    
                pmc.separator( h = 2 , vis = False )  
                ctrlAttrRunRCL = pmc.rowColumnLayout( nc = 2 , columnWidth = [(1,170),(2,170)] )
                with ctrlAttrRunRCL :
                    pmc.button( label = 'Write Attribute' , h = 40 )
                    pmc.button( label = 'Read Attribute' , h = 40 )
                
                
                
            tools = pmc.rowColumnLayout( 'Tools' , nc = 1 , columnWidth = ( 1 , 340 ) )
            with tools :
                dataDirFL = pmc.frameLayout ( label = 'Directory Path' )
            
    pmc.showWindow( window )