import maya.cmds as mc
import maya.mel as mm
import re

def bake_loc_to_center_mesh():
    sel = mc.ls( sl = True )
    
    if sel :
        obj = sel[0].split('.')[0]
        type = mc.nodeType( sel[0] )
        
        if type == 'mesh' :
            edge = mc.filterExpand( sm = 32 )
            
            edgeNum = []
            for each in edge :
                e = re.findall(r'\d+', '%s\n' %each)[-1]
                edgeNum.append(e)
                
            cfme1 = mc.createNode( 'curveFromMeshEdge' , n = 'Loft1_Cfme' )
            mc.setAttr( '%s.ihi' %cfme1 , 1 )
            mc.setAttr( '%s.ei[0]' %cfme1 , int(edgeNum[0]) )
                
            cfme2 = mc.createNode( 'curveFromMeshEdge' , n = 'Loft2_Cfme' )
            mc.setAttr( '%s.ihi' %cfme2 , 1 )
            mc.setAttr( '%s.ei[0]' %cfme2 , int(edgeNum[1]) )
            
            loft = mc.createNode( 'loft' , n = 'Loft_Loft' )
            mc.setAttr( '%s.u' %loft , True )
            mc.setAttr( '%s.rsn' %loft , True )
            
            posi = mc.createNode( 'pointOnSurfaceInfo' , n = 'Post_Posi' )
            mc.setAttr( '%s.turnOnPercentage' %posi , 1 )
            mc.setAttr( '%s.parameterU' %posi , 0.5 )
            mc.setAttr( '%s.parameterV' %posi , 0.5 )
            
            mc.connectAttr( '%s.w' %obj , '%s.im' %cfme1 )
            mc.connectAttr( '%s.w' %obj , '%s.im' %cfme2 )
            mc.connectAttr( '%s.oc' %cfme1 , '%s.ic[0]' %loft )
            mc.connectAttr( '%s.oc' %cfme2 , '%s.ic[1]' %loft )
            mc.connectAttr( '%s.os' %loft , '%s.is' %posi )
            
            grp = mc.createNode( 'transform' )
            loc = mc.spaceLocator()[0]
            parCons = mc.parentConstraint( grp , loc , mo = False )
            
            aimCons = mc.createNode( 'aimConstraint' )
            mc.setAttr( '%s.ax' %aimCons , 0 )
            mc.setAttr( '%s.ay' %aimCons , 1 )
            mc.setAttr( '%s.az' %aimCons , 0 )
            mc.setAttr( '%s.ux' %aimCons , 0 )
            mc.setAttr( '%s.uy' %aimCons , 0 )
            mc.setAttr( '%s.uz' %aimCons , 1 )
    
            mc.connectAttr( '%s.position' %posi , '%s.t' %grp )
            mc.connectAttr( '%s.n' %posi , '%s.tg[0].tt' %aimCons )
            mc.connectAttr( '%s.tv' %posi , '%s.wu' %aimCons )
            mc.connectAttr( '%s.crx' %aimCons , '%s.rx' %grp )
            mc.connectAttr( '%s.cry' %aimCons , '%s.ry' %grp )
            mc.connectAttr( '%s.crz' %aimCons , '%s.rz' %grp )
            
            startTime = int(mc.playbackOptions(q=True, min=True))
            endTime = int(mc.playbackOptions(q=True, max=True))
            mc.bakeResults(loc, t=(startTime, endTime) , sb = 1 )
            
            mc.delete( cfme1 , cfme2 , loft , grp , aimCons , parCons )