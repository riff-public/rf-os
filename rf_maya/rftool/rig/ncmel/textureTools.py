import maya.cmds as mc
import maya.mel as mm
import maya.OpenMaya as om
import os
import shutil
import maya.app.general.fileTexturePathResolver as ftpr

def sceneInfo( folderName = 'texture' , step = -1 ):
    # Get path file and create folder texture
    # ex.folderName = 'texture/rig' , step = -1
    # Do create folder "texture" in path file with out last / -1 step
    # and create folder "rig" in folder "texture"

    filePath = mc.file( q = True , sn = True )
    scnDir = '/'.join( filePath.split('/')[:step] )

    if not os.path.exists( '%s/%s' %( scnDir , folderName )) :
        os.makedirs( '%s/%s' %( scnDir , folderName ))

    return '%s/%s' %( scnDir , folderName )

def getTextureFile():
    ## Return all texture file and uv tiles  ( UDIM , ZBrush , MudBox )

    txrFilePath = []
    
    txrFile = mc.ls( type = 'file' )
    if txrFile :
        for fl in txrFile : 
            txrPath = mc.getAttr( '%s.fileTextureName' %fl )
            uvMode = mc.getAttr( '%s.uvTilingMode' %fl )
            
            useFrame = False
            frameNumber = None
            
            pattern = ftpr.getFilePatternString( txrPath , useFrame , uvMode )
            allFiles = ftpr.findAllFilesForPattern( pattern , frameNumber )
            
            for files in allFiles :
                txrFilePath.append( files )
    
    rsNormalMap = mc.ls( type = 'RedshiftNormalMap' )
    if rsNormalMap :
        for fl in rsNormalMap : 
            txrPath = mc.getAttr( '%s.tex0' %fl )
    
            useFrame = False
            frameNumber = None
            
            pattern = ftpr.getFilePatternString( txrPath , useFrame , 0 )
            allNormalMapRs = ftpr.findAllFilesForPattern( pattern , frameNumber )
            
            for normalMapRs in allNormalMapRs :
                txrFilePath.append( normalMapRs )
            
    return txrFilePath

def fixTexturePath( path = '' ) :
    ## Copy texture to folder texture
    
    txrAllReady = []
    txrSuccess = []
    txrFail = []
    
    if not path :
        filePath = mc.file( q = True , sn = True )
        scnDir = '/'.join( filePath.split('/')[:-2] )
        path = mc.fileDialog2( cap = 'Browser Path File' , dialogStyle = 1 , fm = 3 , dir = scnDir )[0]

    if path :
        
        txrFile = mc.ls( type = 'file' )
        if txrFile :
            for fl in txrFile : 
                txrPath = mc.getAttr( '%s.fileTextureName' %fl )
                uvMode = mc.getAttr( '%s.uvTilingMode' %fl )
                
                useFrame = False
                frameNumber = None
                
                pattern = ftpr.getFilePatternString( txrPath , useFrame , uvMode )
                allFiles = ftpr.findAllFilesForPattern( pattern , frameNumber )
                
                '''
                uvcoords = ftpr.computeUVForFiles(allFiles, pattern) # To get the Maya UVs coordinates of the files
                uvcoords = [uvcoords[i:i+2] for i in xrange(0, len(uvcoords)-1, 2)] # The default output being awfull, refactor it to be a real list of list of uv coordinates
                '''
                
                if os.path.exists( txrPath ) :
                    for files in allFiles :
                        shutil.copy2( files , '%s/%s' % ( path , files.split('/')[-1]))
                        txrSuccess.append( txrPath )
                    
                        if not uvMode == 0 :
                            mc.setAttr( '%s.uvTilingMode' %fl , 0 )
                            mc.setAttr( '%s.fileTextureName' %fl , '%s\\%s' % ( path , txrPath.split('/')[-1]) , type = 'string' )
                            mc.setAttr( '%s.uvTilingMode' %fl , uvMode )
                        else :
                            mc.setAttr('%s.fileTextureName' % fl, '%s\\%s' % ( path , txrPath.split('/')[-1]) , type = 'string' )
                else :
                    txrFail.append( txrPath )
    
        rsNormalMap = mc.ls( type = 'RedshiftNormalMap' )
        if rsNormalMap :
            for fl in rsNormalMap : 
                txrPath = mc.getAttr( '%s.tex0' %fl )
        
                useFrame = False
                frameNumber = None
                
                normalMapRs = ftpr.getFilePatternString( txrPath , useFrame , 0 )
                
                if os.path.exists( txrPath ) :
                    shutil.copy2( normalMapRs , '%s/%s' % ( path , normalMapRs.split('/')[-1]))
                    mc.setAttr('%s.tex0' % fl, '%s\\%s' % ( path , txrPath.split('/')[-1]) , type = 'string' )
                    txrSuccess.append( txrPath )
                else :
                    txrFail.append( txrPath )

        if txrAllReady :
            print '\n============== ALL READY ==============='
            for txr in txrAllReady :
                print '%s' %txr

        if txrSuccess :
            print '\n============== SUCCESS ==============='
            for txr in txrSuccess :
                print '%s' %txr
            
        if txrFail :
            print '\n================ FAIL ================'
            for txr in txrFail :
                print '%s' %txr

        if path :
            print '\n================ PATH ================'
            print '%s' %path

def resizeTexture( sourceImage , outputImage , width , height ) :
    format = sourceImage.split('.')[-1]
    image = om.MImage()
    image.readFromFile( '%s' %sourceImage )

    image.resize( width, height )
    image.writeToFile( '%s' %outputImage , format )

def getSizeTexture( sourceImage ) :
    image = om.MImage()
    image.readFromFile( '%s' %sourceImage )
    
    scriptUtil = om.MScriptUtil()
    width = om.MScriptUtil()
    height = om.MScriptUtil()
    width.createFromInt(0)
    height.createFromInt(0)
    pWidth = width.asUintPtr()
    pHeight = height.asUintPtr() 
    image.getSize( pWidth, pHeight )
    vWidth = width.getUint(pWidth)
    vHeight = height.getUint(pHeight)

    return vWidth,vHeight

def transferShade() :
    ## Transfer material from frist obj to any obj selected

    sels = mc.ls( sl = True , l = True )
    mc.hyperShade( smn = True )
    mat = mc.ls( sl = True )[0]
    
    for sel in sels :
        if not sel == sels[0]:
            mc.select( sel , r = True )
            mc.hyperShade( assign = mat )
    
    mc.select( sels , r = True )

def transferShadeWithOutRef() :
    ## Transfer material from frist obj to any obj selected with out reference

    sels = mc.ls( sl = True , l = True )
    mc.hyperShade( smn = True )
    refMat = mc.ls( sl = True )[0]
    
    if ':' in refMat:
        mat = refMat.split(':')[-1]

        if not mc.objExists( mat ):
            mc.duplicate( upstreamNodes = True )
    
        for sel in sels :
            if not sel == sels[0]:
                mc.select( sel , r = True )
                mc.hyperShade( assign = mat )
    else:
        transferShade()
    
    mc.select( sels , r = True )

def transferUv() :
    ## Transfer uv from frist obj to any obj selected

    sels = mc.ls( sl = True )
    for sel in sels :
        if not sel == sels[0] :
            mc.select( sels[0] , r = True )
            mc.select( sel , add = True )

            cmd = 'transferAttributes -pos 0 -nml 0 -uvs 2 -col 2 -spa 4 -suv "map1" -tuv "map1" -sm 3 -fuv 0 -clb 1;'
            mm.eval( cmd )
            mc.delete( sel , ch = True )

    mc.select( sels , r = True )