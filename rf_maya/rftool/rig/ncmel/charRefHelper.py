import pymel.core as pm
import maya.cmds as mc
import os

class CharRefHelper( object ) :
    
    def __init__( self , version = '1.0' ) :
        
        if pm.window( 'RefTools' , exists = True ) :
            pm.deleteUI( 'RefTools' )
        
        width = 240
        window = pm.window( 'RefTools' , title = "Ref Helper %s" %version , width = width , mnb = True , mxb = False , sizeable = True , rtf = True )
        
        allLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] )
        with allLayout :
            demonFrameLayout = pm.frameLayout ( label = 'Character Demon' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            
            with demonFrameLayout :
                demonRefLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width-4 ) ] )
                
                with demonRefLayout :
                    
                    listDemons = ( 'bodyDemonBone' , 'earsDemonBone' , 'noseDemonBone' , 'tongueDemonBone' , 'skeletonDemon' )
                    
                    for listDemon in listDemons :
                        with demonRefLayout :
                            self.makeButtonDemon( listDemon )
                            pm.separator( vis = False )
            
            sruFrameLayout = pm.frameLayout ( label = 'Character SRU' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            
            with sruFrameLayout :
                sruRefLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width-4 ) ] )
                
                with sruRefLayout :
                    
                    listSRUs = ( 'hanumanSRU' , 'sunnySRU' , 'captainSRU' , 'ericSRU' , 'hippoSRU' , 'kevinSRU' , 'lilySRU' )
                    
                    for listSRU in listSRUs :
                        with sruRefLayout :
                            self.makeButtonSRU( listSRU )
                            pm.separator( vis = False )
            
            mainFrameLayout = pm.frameLayout ( label = 'Character Main' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            
            with mainFrameLayout :
                mainRefLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width-4 ) ] )
                
                with mainRefLayout :
                    
                    listMains = ( 'hanumanAncientArmor' , 'sonGokuAncientArmor' )
                    
                    for listMain in listMains :
                        with mainRefLayout :
                            self.makeButtonMain( listMain )
                            pm.separator( vis = False )
            
            godFrameLayout = pm.frameLayout ( label = 'Character God' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            
            with godFrameLayout :
                godRefLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width-4 ) ] )
                
                with godRefLayout :
                    
                    listGodsRS = [( 'princessIronFanOldGod' )]
                    listGodsYeti = [( 'spiderGirlGod' )]
                    
                    for listGodRs in listGodsRS :
                        with godRefLayout :
                            self.makeButtonGodRS( listGodRs )
                            pm.separator( vis = False )
                    
                    for listGodYeti in listGodsYeti :
                        with godRefLayout :
                            self.makeButtonGodYeti( listGodYeti )
                            pm.separator( vis = False )
            
            bullFrameLayout = pm.frameLayout ( label = 'Character Bull' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            
            with bullFrameLayout :
                bullRefLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width-4 ) ] )
                
                with bullRefLayout :
                    
                    listBullsRS = [( 'bullDemonBaby')]
                    listBullsYeti = ( 'bullDemon' , 'bullDemonKing' )
                    
                    for listBullYeti in listBullsYeti :
                        with bullRefLayout :
                            self.makeButtonBullYeti( listBullYeti )
                            pm.separator( vis = False )
                    
                    for listBullRS in listBullsRS :
                        with bullRefLayout :
                            self.makeButtonBullRS( listBullRS )
                            pm.separator( vis = False )
                            pm.separator( vis = False )
            
            secondaryFrameLayout = pm.frameLayout ( label = 'Character Secondary' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            
            with secondaryFrameLayout :
                secondaryRefLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width-4 ) ] )
                
                with secondaryRefLayout :
                    
                    listSecondarysRS = [( 'powerplantManagerProximitySuit')]
                    listSecondarysYeti = []
                    
                    for listsecondaryYeti in listSecondarysYeti :
                        with secondaryRefLayout :
                            self.makeButtonSecondaryYeti( listSecondaryYeti )
                            pm.separator( vis = False )
                    
                    for listSecondaryRS in listSecondarysRS :
                        with secondaryRefLayout :
                            self.makeButtonSecondaryRS( listSecondaryRS )
                            pm.separator( vis = False )
                    
        pm.showWindow( window )
    
    def makeButtonDemon( self , char ):
        pathFile = r'P:\Two_Heroes\asset\character\demon'
        
        def createRef( *args ):
            fileMA = '%s/%s/lib/%s_ren_rs.ma' %( pathFile , char , char )
            
            if os.path.exists( fileMA ):
                mc.file( fileMA , r = True , type = 'mayaAscii' , namespace = '%s001' %char )
        
        button = pm.button( '%s' %char , label = '%s' %char , h = 35 , c = createRef )
    
    def makeButtonSRU( self , char ):
        pathFile = r'P:\Two_Heroes\asset\character\SRU'
        
        def createRef( *args ):
            fileMA = '%s/%s/lib/%s_ren_rs.ma' %( pathFile , char , char )
            
            if os.path.exists( fileMA ):
                mc.file( fileMA , r = True , type = 'mayaAscii' , namespace = '%s001' %char )
        
        button = pm.button( '%s' %char , label = '%s' %char , h = 35 , c = createRef )
    
    def makeButtonMain( self , char ):
        pathFile = r'P:\Two_Heroes\asset\character\main'
        
        def createRef( *args ):
            fileMA = '%s/%s/lib/%s_ren_yeti.ma' %( pathFile , char , char )
            
            if os.path.exists( fileMA ):
                mc.file( fileMA , r = True , type = 'mayaAscii' , namespace = '%s001' %char )
        
        button = pm.button( '%s' %char , label = '%s' %char , h = 35 , c = createRef )
    
    def makeButtonBullRS( self , char ):
        pathFile = r'P:\Two_Heroes\asset\character\demon'
        
        def createRef( *args ):
            fileMA = '%s/%s/lib/%s_ren_rs.ma' %( pathFile , char , char )
            
            if os.path.exists( fileMA ):
                mc.file( fileMA , r = True , type = 'mayaAscii' , namespace = '%s001' %char )
        
        button = pm.button( '%s' %char , label = '%s' %char , h = 35 , c = createRef )
    
    def makeButtonBullYeti( self , char ):
        pathFile = r'P:\Two_Heroes\asset\character\demon'
        
        def createRef( *args ):
            fileMA = '%s/%s/lib/%s_ren_yeti.ma' %( pathFile , char , char )
            
            if os.path.exists( fileMA ):
                mc.file( fileMA , r = True , type = 'mayaAscii' , namespace = '%s001' %char )
        
        button = pm.button( '%s' %char , label = '%s' %char , h = 35 , c = createRef )
    
    def makeButtonGodRS( self , char ):
        pathFile = r'P:\Two_Heroes\asset\character\god'
        
        def createRef( *args ):
            fileMA = '%s/%s/lib/%s_ren_rs.ma' %( pathFile , char , char )
            
            if os.path.exists( fileMA ):
                mc.file( fileMA , r = True , type = 'mayaAscii' , namespace = '%s001' %char )
        
        button = pm.button( '%s' %char , label = '%s' %char , h = 35 , c = createRef )
    
    def makeButtonGodYeti( self , char ):
        pathFile = r'P:\Two_Heroes\asset\character\god'
        
        def createRef( *args ):
            fileMA = '%s/%s/lib/%s_ren_yeti.ma' %( pathFile , char , char )
            
            if os.path.exists( fileMA ):
                mc.file( fileMA , r = True , type = 'mayaAscii' , namespace = '%s001' %char )
        
        button = pm.button( '%s' %char , label = '%s' %char , h = 35 , c = createRef )
    
    def makeButtonSecondaryRS( self , char ):
        pathFile = r'P:\Two_Heroes\asset\character\secondary'
        
        def createRef( *args ):
            fileMA = '%s/%s/lib/%s_ren_rs.ma' %( pathFile , char , char )
            
            if os.path.exists( fileMA ):
                mc.file( fileMA , r = True , type = 'mayaAscii' , namespace = '%s001' %char )
        
        button = pm.button( '%s' %char , label = '%s' %char , h = 35 , c = createRef )
    
    def makeButtonSecondaryYeti( self , char ):
        pathFile = r'P:\Two_Heroes\asset\character\secondary'
        
        def createRef( *args ):
            fileMA = '%s/%s/lib/%s_ren_yeti.ma' %( pathFile , char , char )
            
            if os.path.exists( fileMA ):
                mc.file( fileMA , r = True , type = 'mayaAscii' , namespace = '%s001' %char )
        
        button = pm.button( '%s' %char , label = '%s' %char , h = 35 , c = createRef )