import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  SUB RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , name       = '' ,
                         tmpJnt     = '' ,
                         parent     = '' ,
                         ctrlGrp    = 'Ctrl_Grp' ,
                         shape      = 'square' ,
                         side       = '' ,
                         size       = 1 
                 ):

        ##-- Info
        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        #-- Create Main Group
        self.grp = rt.createNode( 'transform' ,  '%sSubCtrl%sGrp' %( name , side ))
        self.grp.snap( tmpJnt )

        if mc.objExists(parent):
            self.parsCons = mc.parentConstraint( parent , self.grp , mo = True )
            self.sclsCons = mc.scaleConstraint( parent , self.grp , mo = True )
        
        #-- Create Joint
        self.jnt = rt.createJnt( '%sSub%sJnt' %( name , side ) , tmpJnt )
        self.jnt.attr('ssc').v = 0

        #-- Create Controls
        self.ctrl = rt.createCtrl( '%sSub%sCtrl' %( name , side ) , shape , 'cyan' , jnt = True )
        self.gmbl = rt.addGimbal( self.ctrl )
        self.zroGrp = rt.addGrp( self.ctrl )
        self.offsetGrp = rt.addGrp( self.ctrl , 'Ofst')
        self.parentGrp = rt.addGrp( self.ctrl , 'Pars')
        self.zroGrp.snap( tmpJnt )

        #-- Adjust Shape Controls
        for ctrl in ( self.ctrl , self.gmbl ) :
            ctrl.scaleShape( size )

        #-- Rig process
        mc.parentConstraint( self.gmbl , self.jnt , mo = False )
        mc.scaleConstraint( self.gmbl , self.jnt , mo = False )

        if mc.objExists(ctrlGrp):
            rt.localWorld( self.ctrl , ctrlGrp , self.grp , self.zroGrp , 'parent' )
        
        #-- Adjust Hierarchy
        if mc.objExists(ctrlGrp):
            self.grp.parent(ctrlGrp)

        if mc.objExists(parent):
            self.jnt.parent(parent)
        elif mc.objExists('Skin_Grp'):
            self.jnt.parent('Skin_Grp')

        self.zroGrp.parent(self.grp)

        #-- Cleanup
        for obj in ( self.grp , self.zroGrp , self.offsetGrp ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

        for obj in ( self.ctrl , self.gmbl ) :
            obj.lockHideAttrs( 'v' )
        
        mc.select( cl = True )

# -------------------------------------------------------------------------------------------------------------
#
#  ARMOR RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Armor( Run ):

    ## MaintainOffset Orient. Should be use with Non-Roll.
    
    def __init__( self , name       = '' ,
                         tmpJnt     = '' ,
                         parent     = '' ,
                         ctrlGrp    = 'Ctrl_Grp' ,
                         shape      = 'square' ,
                         side       = '' ,
                         size       = 1 
                 ):
        
        super( Armor , self ).__init__( name , tmpJnt , parent , ctrlGrp , shape , side , size )

        '''
        ##-- Info
        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        ##-- Adjust Constraint
        if mc.objExists(parent):
            mc.delete( self.parsCons , self.sclsCons )

            for attr in ('tx','ty','tz','rx','ry','rz','sx','sy','sz') :
                mc.setAttr( '%s.%s' % ( self.grp , attr ) , l = False )

            self.pntsCons = mc.pointConstraint( parent , self.grp , mo = True )
            self.orisCons = mc.orientConstraint( parent , self.grp , mo = True )
            self.sclsCons = mc.scaleConstraint( parent , self.grp , mo = True )

            for attr in ('tx','ty','tz','rx','ry','rz','sx','sy','sz') :
                mc.setAttr( '%s.%s' % ( self.grp , attr ) , l = True )

        ##-- Create Node
        self.rxCmp = rt.createNode( 'clamp' , '%sRx%sCmp' %( name , side ))
        self.ryCmp = rt.createNode( 'clamp' , '%sRy%sCmp' %( name , side ))
        self.rzCmp = rt.createNode( 'clamp' , '%sRz%sCmp' %( name , side ))

        self.rxMdv = rt.createNode( 'multiplyDivide' , '%sRx%sMdv' %( name , side ))
        self.ryMdv = rt.createNode( 'multiplyDivide' , '%sRy%sMdv' %( name , side ))
        self.rzMdv = rt.createNode( 'multiplyDivide' , '%sRz%sMdv' %( name , side ))

        self.rxPma = rt.createNode( 'plusMinusAverage' , '%sRx%sPma' %( name , side ))
        self.ryPma = rt.createNode( 'plusMinusAverage' , '%sRy%sPma' %( name , side ))
        self.rzPma = rt.createNode( 'plusMinusAverage' , '%sRz%sPma' %( name , side ))

        ##-- Connect Node
        if mc.objExists(parent):
            parent = core.Dag(parent)
            parent.attr('rx') >> self.rxCmp.attr('ipr')
            parent.attr('rx') >> self.rxCmp.attr('ipg')
            parent.attr('ry') >> self.ryCmp.attr('ipr')
            parent.attr('ry') >> self.ryCmp.attr('ipg')
            parent.attr('rz') >> self.rzCmp.attr('ipr')
            parent.attr('rz') >> self.rzCmp.attr('ipg')

        self.rxCmp.attr('opr') >> self.rxMdv.attr('i1x')
        self.rxCmp.attr('opg') >> self.rxMdv.attr('i1y')
        self.ryCmp.attr('opr') >> self.ryMdv.attr('i1x')
        self.ryCmp.attr('opg') >> self.ryMdv.attr('i1y')
        self.rzCmp.attr('opr') >> self.rzMdv.attr('i1x')
        self.rzCmp.attr('opg') >> self.rzMdv.attr('i1y')

        self.rxMdv.attr('ox') >> self.rxPma.attr('i1[0]')
        self.rxMdv.attr('oy') >> self.rxPma.attr('i1[1]')
        self.ryMdv.attr('ox') >> self.ryPma.attr('i1[0]')
        self.ryMdv.attr('oy') >> self.ryPma.attr('i1[1]')
        self.rzMdv.attr('ox') >> self.rzPma.attr('i1[0]')
        self.rzMdv.attr('oy') >> self.rzPma.attr('i1[1]')

        for attr in ('rx','ry','rz') :
            mc.setAttr( '%s.%s' % ( self.offsetGrp , attr ) , l = False )

        self.rxPma.attr('o1') >> self.offsetGrp.attr('rx')
        self.ryPma.attr('o1') >> self.offsetGrp.attr('ry')
        self.rzPma.attr('o1') >> self.offsetGrp.attr('rz')

        for attr in ('rx','ry','rz') :
            mc.setAttr( '%s.%s' % ( self.offsetGrp , attr ) , l = True )

        ##-- Create Attribute & Adjust Value
        self.rxCmp.attr('mnr').value = -500
        self.rxCmp.attr('mxg').value = 500
        self.ryCmp.attr('mnr').value = -500
        self.ryCmp.attr('mxg').value = 500
        self.rzCmp.attr('mnr').value = -500
        self.rzCmp.attr('mxg').value = 500

        self.ctrlShape = core.Dag(self.ctrl.shape)
        self.ctrlShape.addTitleAttr( 'Value' )
        self.ctrlShape.addAttr( 'rxMin' )
        self.ctrlShape.addAttr( 'rxMax' )
        self.ctrlShape.addAttr( 'ryMin' )
        self.ctrlShape.addAttr( 'ryMax' )
        self.ctrlShape.addAttr( 'rzMin' )
        self.ctrlShape.addAttr( 'rzMax' )

        self.ctrlShape.attr( 'rxMin' ) >> self.rxMdv.attr('i2x')
        self.ctrlShape.attr( 'rxMax' ) >> self.rxMdv.attr('i2y')
        self.ctrlShape.attr( 'ryMin' ) >> self.ryMdv.attr('i2x')
        self.ctrlShape.attr( 'ryMax' ) >> self.ryMdv.attr('i2y')
        self.ctrlShape.attr( 'rzMin' ) >> self.rzMdv.attr('i2x')
        self.ctrlShape.attr( 'rzMax' ) >> self.rzMdv.attr('i2y')
        
        mc.select( cl = True )
        '''