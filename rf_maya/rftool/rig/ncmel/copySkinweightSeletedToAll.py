import maya.cmds as mc
import maya.mel as mm

sels = mc.ls(sl = True)
for sel in sels :
    if not sel == sels[0] :
        mc.select( sels[0] , r = True )
        mc.select( sel , add = True )
        mc.copySkinWeights( noMirror = True , surfaceAssociation = 'closestPoint' )