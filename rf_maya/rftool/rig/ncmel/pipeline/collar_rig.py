
import maya.cmds as mc
import maya.mel as mm
def curveCollar(): 
	cru = mc.polyToCurve(form =2, degree=3) 
	span_curve = 9 
	cur_name = mc.rename(cru[0],'collar_curve') 
	curev_rebuild = mc.rebuildCurve(cur_name, ch = 1, rpo =1 ,end = 1, rt=0, keepRange =0, keepEndPoints=True,kt=0 ,spans=span_curve, degree=3, tol=0.01) 
	grp_cls_name = mc.group( em=True, name='%s_cluster_Grp'%cur_name ) 
	list_grp = [] 
	grpAll =[]
	rig_grp = mc.group( em=True, name='rig_collar_grp') 
	ctrl_all_grp = mc.group( em=True, name='ctrl_collar_All_Grp') 
	for cv in range(0, span_curve): 
		cv = cv + 1
		list_grp = []
		cv_curve = '%s.cv[%d]'%(cur_name, cv)
		cls = mc.cluster(cv_curve, name= '%s_cluster_haddle'%cur_name ) 
		grp_name = mc.group( em=True, name='%s_cluster_haddle_Grp%d'%(cur_name,cv )) 
		list_grp.append(grp_name) 
		mc.matchTransform(grp_name, cls[1] , pivots=True, pos=True, rot=True, scl=True) 
		mc.parent(cls, grp_name) 
		mc.parent(grp_name, grp_cls_name) 
		grpAll.append(grp_name)

		## Rig Selection for KeepOut
		mc.select(list_grp) 
		mm.eval("cMuscle_rigKeepOutSel();") 
		createJoint(list_grp) 

	## Convert Surface to cMuscle/Bone
	createMus(geoObj = 'ArmMuscle_Geo')

	return grpAll
def createJoint(list_grp): 
	rig_grp ='rig_collar_grp'
	ctrl_all_grp = 'ctrl_collar_All_Grp'
	mc.select(cl=True) 
	for index, grp in enumerate(list_grp): 
		pos = mc.xform(grp, t=True,q=True) 
		print pos 
		mc.select(cl=True) 
		chk_jnt_over = chk_overwrite('collar_rig_%d'%index)
		jnt = mc.joint(position = pos, name = chk_jnt_over) 
		mc.parent(jnt, rig_grp) 
		ctrl_grp = createCtrl(index, jnt, ctrl_all_grp) 
		list_in_grp = mc.listConnections(grp) 
		print jnt, list_in_grp 
		cluster_handdle = mc.listRelatives(list_in_grp[-1]) 
		print ctrl_grp, cluster_handdle[0], jnt 
		constraint_jnt(ctrl_grp, cluster_handdle[0], jnt) 

def createCtrl(index, jnt, ctrl_all_grp): 
	ctrl_chk_index = chk_overwrite('ctrl_collar_Grp_%d'%index)
	ctrl_ = mc.group( em=True, name=ctrl_chk_index) 
	mc.xform(ctrl_, cp=True) 
	ctrl_collar_chk = chk_overwrite('ctrl_collar_%d'%index)
	ctrl_curve = mc.circle(c= [0,0,0], n=ctrl_collar_chk, nr =[0,1,0], sw=360, r=1, d=3, ut=0, tol=0.01, s=8, ch=1) 
	mc.makeIdentity( apply=True, t=1, r=1, s=1, n=0 , pn=1 ) 
	mc.xform(ctrl_curve, cp=True) 
	mc.rotate(90 ,90 ,0 ,ctrl_curve) 
	mc.select([jnt, ctrl_curve[0]]) 
	snap_object() 
	mc.parent(ctrl_curve[0], ctrl_) 
	mc.parent(ctrl_, ctrl_all_grp) 
	return ctrl_ 

def snap_object(): 
	parents = mc.pointConstraint(offset= [0, 0, 0] , weight=1) 
	mc.delete(parents) 

def constraint_jnt(ctrl_grp, cluster_handle, jnt): 
	mc.parentConstraint(ctrl_grp, cluster_handle, mo=True) 
	mc.parentConstraint(jnt, ctrl_grp ,mo=True) 
	
def chk_overwrite(ctrl_collar_name):
	chk_exist = mc.objExists(ctrl_collar_name)
	if chk_exist:
		index = ctrl_collar_name.split('_')[-1]
		new_ctrl_collar_name = ctrl_collar_name.replace(index, '%0d'%(int(index)+1))
		return chk_overwrite(new_ctrl_collar_name)
	else:
		return ctrl_collar_name
	
def createMus(geoObj = 'ArmMuscle_Geo'):
	cMuscleNode = mc.createNode("cMuscleObject", n = 'cMuscleObj')
	mc.connectAttr('{}.worldMatrix[0]'.format(geoObj), '{}.worldMatrixStart'.format(cMuscleNode))
	mc.connectAttr('{}Shape.worldMesh[0]'.format(geoObj), '{}.meshIn'.format(cMuscleNode))
	# mc.select(clsGrp)


	# mc.parent(cMuscleNode, obj)
# curveCollar()