import sys
import os
import webbrowser as web
reload( web )
import pymel.core as pm
import maya.cmds as mc

def dbmBrowserUI( version = '1.0' ) :
    
    width = 502
    
    # check if window exists
    if pm.window( 'DBMBrowserUI' , exists = True ) :
        pm.deleteUI( 'DBMBrowserUI' )
        
    # create window 
    window = pm.window( 'DBMBrowserUI', title = "DBM Browser Tools  %s" %version , width = width ,
        mnb = True , mxb = False , sizeable = True , rtf = True )

    window = pm.window( 'DBMBrowserUI', e = True , width = width , h = 10 )

    titleLayout = pm.rowColumnLayout ( w = width , p = window , nc = 1 ,
        columnWidth = [ ( 1 , width ) ] )
        
    # picture title
    with titleLayout :
        pm.picture( image = 'P:/_pipe/DBMBrowserToolsTitle.jpg' , width = 500 , height = 50 )
       
    separator1Layout = pm.rowColumnLayout ( h = 10 , w = width , nc = 1 , columnWidth = ( 1 , 497 ))
    with separator1Layout :
        pm.separator( h = 5 , st = 'in' )
        
    documentTxLayout = pm.rowColumnLayout ( w = width , p = window , nc = 1 , columnWidth = ( 1 , width ))
    with documentTxLayout :
        pm.text( label = ' Document :' , al = 'left' , fn = 'boldLabelFont' , h = 20 )
        
    documentDpm = pm.rowColumnLayout ( 'Document' ,  nc = 3 , cw = [ ( 1 , width / 3.01 ) , ( 2 , width / 3.01 ) , ( 3 , width / 3.01 )] )
    with documentDpm :
        pm.button( label = 'DBM Document' , h = 35 , c = openDbmDoc )
        pm.button( label = 'All Daily' , h = 35 , c = allDaily )
        pm.popupMenu()
        pm.menuItem( label = 'Model' , c = modelDaily )
        pm.menuItem( label = 'Animation' , c = animDaily )
        pm.menuItem( label = 'Layout' , c = layoutDaily )
        pm.menuItem( label = 'SetDress' , c = setDressDaily )
        pm.menuItem( label = 'Sim' , c = simDaily )
        pm.menuItem( label = 'Edit' , c = editDaily )
        pm.button( label = 'Edit' , h = 35 , c = edit )
       
    separator2Layout = pm.rowColumnLayout ( h = 10 , w = width , nc = 1 , columnWidth = ( 1 , 497 ))
    with separator2Layout :
        pm.separator( h = 5 , st = 'in' )
        
    layoutTxLayout = pm.rowColumnLayout ( w = width , p = window , nc = 1 , columnWidth = ( 1 , width ))
    with layoutTxLayout :
        pm.text( label = ' Layout :' , al = 'left' , fn = 'boldLabelFont' , h = 20 )
        
    layoutDpm = pm.rowColumnLayout ( 'Layout' ,  nc = 3 , cw = [ ( 1 , width / 3.01 ) , ( 2 , width / 3.01 ) , ( 3 , width / 3.01 )] )
    with layoutDpm :
        pm.button( label = "Footage Final Layout" , h = 35 , c = dressingFld )
        pm.button( label = "Footage Dressing" , h = 35 , c = dressingFld )
        pm.button( label = "Footage Previz" , h = 35 , c = previzFld )
       
    separator3Layout = pm.rowColumnLayout ( h = 10 , w = width , nc = 1 , columnWidth = ( 1 , 497 ))
    with separator3Layout :
        pm.separator( h = 5 , st = 'in' )
        
    animTxLayout = pm.rowColumnLayout ( w = width , p = window , nc = 1 , columnWidth = ( 1 , width ))
    with animTxLayout :
        pm.text( label = ' Animation :' , al = 'left' , fn = 'boldLabelFont' , h = 20 )
        
    animationDpm = pm.rowColumnLayout ( 'Animation' ,  nc = 3 , cw = [ ( 1 , width / 3.01 ) , ( 2 , width / 3.01 ) , ( 3 , width / 3.01 )] )
    with animationDpm :
        pm.button( label = "Animation Reference" , h = 35 , c = animRefFld )
        
    pm.showWindow( window )

### DOCUMENT ###
def openDbmDoc( *args ) :
    web.open( 'https://docs.google.com/spreadsheets/d/19_B9R0fvifJro6eycBZcYjrvRva2NZZhqQSTxu4slWM/edit?ts=59674e82#gid=891493353' )

def allDaily( *args ) :
    os.startfile ( r'P:\Doublemonkeys\all\daily' )

def modelDaily( *args ) :
    os.startfile ( r'P:\Doublemonkeys\all\daily\model' )

def animDaily( *args ) :
    os.startfile ( r'P:\Doublemonkeys\all\daily\anim' )

def layoutDaily( *args ) :
    os.startfile ( r'P:\Doublemonkeys\all\daily\layout' )

def setDressDaily( *args ) :
    os.startfile ( r'P:\Doublemonkeys\all\daily\setDress' )

def simDaily( *args ) :
    os.startfile ( r'P:\Doublemonkeys\all\daily\sim' )

def editDaily( *args ) :
    os.startfile ( r'P:\Doublemonkeys\all\daily\edit' )

def edit( *args ) :
    os.startfile ( r'P:\Two_Heroes\edit' )

### LAYOUT ###
def dressingFld( *args ) :
    os.startfile ( r'P:\Two_Heroes\edit\footage\dressing' )
    
def previzFld( *args ) :
    os.startfile ( r'P:\Two_Heroes\edit\footage\pre_viz' )

### ANIMATION ###
def animRefFld( *args ) :
    os.startfile ( r'P:\Two_Heroes\Anim_Reference' )