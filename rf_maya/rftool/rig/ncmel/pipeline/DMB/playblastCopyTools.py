import sys
import os
import glob
import shutil
import re
import subprocess
import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel

pathPrj = 'P:\\Two_Heroes\\scene\\film001'

def pbCopyUI( version = '1.0' ) :
    
    width = 175
    
    if pm.window( 'PbCopyUI' , exists = True ) :
        pm.deleteUI( 'PbCopyUI' )
        
    window = pm.window( 'PbCopyUI', title = "PB Copy Tools  %s" %version , width = width ,
                         mnb = True , mxb = False , sizeable = True , rtf = True )

    window = pm.window( 'PbCopyUI', e = True , width = width )
    
    separator1Layout = pm.rowColumnLayout ( h = 10 ,w = width , nc = 1 , columnWidth = ( 1 , width ))
    with separator1Layout :
        pm.separator( vis = False )
    
    seqLayout = pm.rowColumnLayout ( w = width , nc = 4 , columnWidth = [( 1 , 35 ) , ( 2 , 110 ) , ( 3 , 2 ) , ( 4 , 23 )])
    with seqLayout :
        pm.text( label = 'SEQ :' )
        pm.optionMenu ( 'sequanceOM' , label = '' , cc = loadShot )
        pm.separator( vis = False )
        pm.button( label = '...' , h = 1 , c = browserDpmDir )
    
    dpmLayout = pm.rowColumnLayout ( w = width , nc = 4 , columnWidth = [( 1 , 35 ) , ( 2 , 135 )])
    with dpmLayout :
        pm.text( label = 'DPM :' )
        pm.optionMenu ( 'departmentOM' , label = '' )
        pm.menuItem ( l = 'layout' )
        pm.menuItem ( l = 'finalCam' )
        pm.menuItem ( l = 'anim' )
        pm.menuItem ( l = 'setDress' )
        pm.separator( vis = False )
        
    separator2Layout = pm.rowColumnLayout ( h = 3 ,w = width , nc = 1 , columnWidth = ( 1 , width ))
    with separator2Layout :
        pm.separator( vis = False )
        
    listShot = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = ( 1 , 171 ))
    with listShot :
        pm.textScrollList( 'shotListsTS' , h = 150 , numberOfRows = 15 , allowMultiSelection = 1 )
        
    separator3Layout = pm.rowColumnLayout ( h = 5 ,w = width , nc = 1 , columnWidth = ( 1 , width ))
    with separator3Layout :
        pm.separator( vis = False )
        
    allShotCB = pm.rowColumnLayout ( w = width , nc = 2 , columnWidth = [( 1 , 80 ) , ( 2 , 150 )] )
    with allShotCB :
        pm.separator( vis = False )
        pm.checkBox( 'allShotCB' , label = 'Select All Shot' , cc = selectAllShot )
        
    separator4Layout = pm.rowColumnLayout ( h = 5 , w = width , nc = 1 , columnWidth = ( 1 , width-4 ))
    with separator4Layout :
        pm.separator( h = 5 , st = 'in' )
        
    button1Layout = pm.rowColumnLayout ( h = 40 ,w = width , nc = 1 , columnWidth = ( 1 , 171 ))
    with button1Layout :
        pm.button( label = 'COPY' , h = 35 , c = findFilePlayblast )
        
    pm.showWindow( window )

def loadSeq( *args ) :
    seqLists = pm.getFileList( fld = pathPrj )
    seqLists.sort()
    
    for seq in seqLists :
        pm.menuItem ( p = 'sequanceOM' , l = seq )

def loadShot( *args ) :
    selectSeq = pm.optionMenu ( 'sequanceOM' , q = True , v = True )
    shotLists = pm.getFileList( fld = pathPrj + '\\' + selectSeq )
    shotLists.sort()
    
    exception = ( 'all' , 'playblast' )
    
    if shotLists :
        pm.textScrollList( 'shotListsTS' , e = True , ra = True )
        
        for shots in shotLists :
            if not shots in exception :
                pm.textScrollList( 'shotListsTS' , e = True , a = shots )

def selectAllShot( *args ) :
    selectAllValue = pm.checkBox( 'allShotCB' , q = True , v = True )
    shotLists = pm.textScrollList( 'shotListsTS' , q = True , ai = True )
    
    if selectAllValue == True :
        pm.textScrollList( 'shotListsTS' , e = True , si = shotLists )
    else :
        pm.textScrollList( 'shotListsTS' , e = True , da = True )

def browserDpmDir( *args ) :
    selectSeq = pm.optionMenu ( 'sequanceOM' , q = True , v = True )
    dir = pathPrj + '\\' + selectSeq + '\\playblast'
        
    if not os.path.isdir( dir ) :
        pm.warning( 'Folder Playblast  %s  Not Found !!' %selectSeq )
    else :
        os.startfile( dir )

def findFilePlayblast( *args ) :
    selectSeq = pm.optionMenu ( 'sequanceOM' , q = True , v = True )
    selectDpm = pm.optionMenu ( 'departmentOM' , q = True , v = True )
    selectShot = pm.textScrollList( 'shotListsTS' , q = True , si = True )
    dir = pathPrj + '\\' + selectSeq + '\\playblast'
    
    if not os.path.isdir( dir ) :
        os.mkdir( dir )
    
    if not os.path.isdir( '%s\\%s' %( dir , selectDpm )) :
        os.mkdir( '%s\\%s' %( dir , selectDpm ))
            
    for shots in selectShot :
        pathPb = '%s\\%s\\%s\\%s\\maya\\preview' %( pathPrj , selectSeq , shots , selectDpm )
        pbLists = pm.getFileList( fld = pathPb , fs = '*.mp4' )
        
        pbFiles = glob.glob( '%s\\*.mp4' %pathPb )
        pbFiles.sort( key = os.path.getmtime )
        
        oldFile = pm.getFileList( fld = '%s\\%s' %( dir , selectDpm ) , fs = 'th_film001_%s_%s_%s_*.mp4' %( selectSeq , shots , selectDpm ))
        
        if oldFile :
            os.remove( '%s\\%s\\%s' %( dir , selectDpm , oldFile[0] ))
        
        try :    
            shutil.copy2( pbFiles[-1] , '%s\\%s' %( dir , selectDpm ))
        except :
            pass
            
    if shots == selectShot[-1] :
        mc.confirmDialog( title = 'Confirm' , message = 'Copy %s %s is Done!!' %( selectDpm , selectSeq ))