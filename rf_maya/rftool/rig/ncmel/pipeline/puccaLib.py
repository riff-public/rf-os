import sys
import os
import pymel.core as pm
import maya.cmds as mc

path = r'Y:\NPC\prod\01_asset'
width = 430
hight = 602
useStyle = 'iconAndTextVertical'
size = 100
    
def puccaAssetLibUI( version = '1.0' ) :
    
    # check if window exists
    if pm.window( 'PuccaAssetLibUI' , exists = True ) :
        pm.deleteUI( 'PuccaAssetLibUI' )
        
    window = pm.window( 'PuccaAssetLibUI', title = "Pucca Asset Lib %s" %version , w = width )
    
    form = pm.formLayout( w = width , h = hight )
    pm.scrollLayout( 'scrollLayout' , w = width , h = hight )
    tabs = pm.tabLayout( w = width )
    
    eps = getEpLists()
    
    charCL = pm.rowColumnLayout()
    for ep in eps :
        with charCL :
            pm.frameLayout( label = '%s' %ep , borderStyle = 'out' , cll = True , w = 645 )
            buttoncharLayout = pm.rowColumnLayout ( w = width , nc = 4 , columnWidth = [( 1 , 100 ) , ( 2 , 100 ) , ( 3 , 100 ) , ( 4 , 100 )])
            
            charLists = pm.getFileList( fld = '%s\\%s\\01_ch\\main' %( path , ep ))
            
            if charLists :
                charLists.sort()
                
                for charList in charLists :
                    with buttoncharLayout :
                        makeButton( ep , '01_ch' , 'main' , charList )
    
    
    propCL = pm.rowColumnLayout()
    for ep in eps :
        with propCL :
            pm.frameLayout( label = '%s' %ep , borderStyle = 'out' , cll = True , w = 645 )
            buttonPropLayout = pm.rowColumnLayout ( w = width , nc = 4 , columnWidth = [( 1 , 100 ) , ( 2 , 100 ) , ( 3 , 100 ) , ( 4 , 100 )])
            
            propLists = pm.getFileList( fld = '%s\\%s\\02_pr\\main' %( path , ep ))
            
            if propLists :
                propLists.sort()
                
                for propList in propLists :
                    with buttonPropLayout :
                        makeButton( ep , '02_pr' , 'main' , propList )
    
    
    bgCL = pm.rowColumnLayout()
    for ep in eps :
        with bgCL :
            pm.frameLayout( label = '%s' %ep , borderStyle = 'out' , cll = True , w = 645 )
            buttonBgLayout = pm.rowColumnLayout ( w = width , nc = 4 , columnWidth = [( 1 , 100 ) , ( 2 , 100 ) , ( 3 , 100 ) , ( 4 , 100 )])
            
            bgLists = pm.getFileList( fld = '%s\\%s\\03_bg' %( path , ep ))
            
            if bgLists :
                bgLists.sort()
                
                for bgList in bgLists :
                    with buttonBgLayout :
                        makeButton( ep , '03_bg' , '' , bgList )
                
    mc.tabLayout( tabs , edit = True , tabLabel = [( charCL , 'Char' ) , ( propCL , 'Prop' ) , ( bgCL , 'Bg' )])
    pm.showWindow( window )

def makeButton( episode , category , type , char ):
    pathFile = '%s/%s/%s/%s/%s' %( path , episode , category , type , char )
    pathPic = '%s/thumb.png' %pathFile
    
    def createRef( *args ):
        fileMA = '%s/%s_%s_%s_ani_v00.ma' %( pathFile , episode , category.split('_')[-1] , char )
        
        if os.path.exists( fileMA ):
            mc.file( fileMA , r = True , type = 'mayaAscii' , namespace = '%s001' %char )
        else :
            pm.warning( "File %s_%s_%s_ani_v00.ma Not Found " %( episode , category.split('_')[-1] , char ))
        
    buttonTx = pm.iconTextButton( '%sITB' %char , label = '%s' %char , style = useStyle , w = 100 , h = 100 , c = createRef )
    
    if os.path.exists( pathPic ):
        pm.iconTextButton( '%sITB' %char , e = True , image = pathPic )
        
    return buttonTx
    
def getEpLists( *args ) :
    epLists = pm.getFileList( fld = '%s' %path )
    epLists.sort()
    
    return epLists