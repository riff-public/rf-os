#
# Panupat's Playblast Script
#
# This script only works if the current scene is saved to disk.
# The script will go up 1 level and output playblast
# to 'output' directory. 
#
# Hero playblast is saved right there.
# Version playblast will be saved in 'preview' sub directory
#
# Requirements
# - Python 2.6
# - PyQt 4.73
#
# panupatc@gmail.com
# riffcg.com

import os
import re
import shutil
import thread
import subprocess

import maya.cmds as cmds
import maya.mel as mel
from PyQt4 import QtCore
from PyQt4 import QtGui

from joey.lib.mayaLib import *
from ui.playblastUI import Ui_playblastWindow
from ui import resource
QT = "C:\\Program Files (x86)\\QuickTime\\QuickTimePlayer.exe"
TMP = "C:\\3delight_cache\\pbTemp\\pbTemp"
ffmpeg = r"P:\lib\ffmpeg\bin\ffmpeg.exe"

from ninePlayblastDDR import addPlay
# =========================== UI =========================

class PlayblastUI(QtGui.QMainWindow, Ui_playblastWindow):

    def __init__(self, *args, **kwargs):

        super(PlayblastUI, self).__init__(*args, **kwargs)
        self.setupUi(self)
                
        self.setWindowIcon(QtGui.QIcon(":/ico/hero.png"))
        self.versionCheck.setIcon(QtGui.QIcon(":/ico/version.png"))
        self.heroCheck.setIcon(QtGui.QIcon(":/ico/hero.png"))
        self.overrideCheck.setIcon(QtGui.QIcon(":/ico/trumpet.png"))
        self.playBtn.setIcon(QtGui.QIcon(":/ico/play.png"))
        self.deleteBtn.setIcon(QtGui.QIcon(":/ico/delete.png"))

# =========================== APP ========================


class Playblast(object):

    def __init__(self, *args, **kwargs):

        self.ui = PlayblastUI(*args, **kwargs)


        self.initVars()
        self.initConnections()
        self.getInfo()

        self.show()


    def initVars(self):

        self.sizeList = [ None,
                          None,
                        [320, 240],
                        [480, 272],
                        [852, 480],
                        [960, 400],
                        [960, 540],
                        [1280, 720],
                        [1920, 800],
                        [1920, 1080],
                          None,
                    ]

        self.isValid = False
        self.isHero  = False

        # variables for this class
        # c = current
        # o = output
        self.cPath = ''
        self.cFile = ''
        self.cVer  = 0
        self.cList = [] # list of full path of playblast

        self.oPath = ''
        self.oFile = ''

    def initConnections(self):

        self.ui.browseBtn.clicked.connect(self.browse)
        #self.ui.playBtn.clicked.connect(self.play)
        #self.ui.deleteBtn.clicked.connect(self.delete)
        self.ui.versionCheck.clicked.connect(self.clickVersion)
        self.ui.heroCheck.clicked.connect(self.clickHero)
        self.ui.overrideCheck.toggled.connect(self.toggleFilename)
        self.ui.sizeChoose.currentIndexChanged.connect(self.changeSize)
        self.ui.playblastBtn.clicked.connect(self.doPlayblast)

    def getInfo(self):

        self.cPath = os.path.split(cmds.file(q=True, sn=True))[0]
        self.cPath = os.path.normpath(self.cPath)
        
        if not cmds.file(q=True, sn=True):
            self.clear()
            print "No scene file open"
            return
            

        self.isValid = True
        # store output path to self.oPath
        self.oPath = self.cPath.replace("\\version", "")
        self.oPath = self.oPath.replace("\\hero", "")
        #self.cFile = os.path.basename(self.oPath)
        self.oPath = os.path.join(self.oPath, "output")

        # redefine self.cFile using filename subtracts _v\d\d\d
        #self.cFile = "TestString"
        f = cmds.file(q=True,sn=True,shn=True)
        f = os.path.splitext(f)[0]
        ver = re.findall("_v\d\d\d", f)
        if ver:
            f = f.replace( ver[0], "")
        self.cFile = f


        if not os.path.exists(os.path.join(self.oPath, "preview")):
            os.makedirs(os.path.join(self.oPath, "preview"))

        self.ui.browseLabel.setText(self.oPath)
        self.updateFileList()
        self.changeSize()

    def browse(self):

        path = str(self.ui.browseLabel.text())
        if path and os.path.exists(path):
            os.startfile(path)

    def updateFileList(self):

        self.ui.fileChoose.clear()
        self.cVer = 1
        self.cList = []

        if not self.isValid:
            return

        # get list of files in preview
        path = self.oPath
        cs = [os.path.join(path, d) for d in os.listdir(path) \
                if os.path.isfile(os.path.join(path, d)) \
                and (d.endswith(".avi") or d.endswith(".mov") or d.endswith(".mp4")) ]
        self.cList += cs
        if len(cs) > 0:
            for c in cs:
                item = QtGui.QListWidgetItem(os.path.basename(c))
                item.setIcon(QtGui.QIcon(":/ico/hero.png"))
                self.ui.fileChoose.addItem(item)

        path = os.path.join(self.oPath, "preview")
        cs = [os.path.join(path, d) for d in os.listdir(path) \
                if os.path.isfile(os.path.join(path, d)) \
                and ( d.endswith(".avi") or d.endswith(".mov") or d.endswith(".mp4"))]
        self.cList += sorted(cs)
        if len(cs) > 0:
            for c in sorted(cs):
                item = QtGui.QListWidgetItem(os.path.basename(c))
                item.setIcon(QtGui.QIcon( ":/ico/version.png"))
                self.ui.fileChoose.addItem(item)
                tmp = re.findall("v\d\d\d", os.path.basename(c))
                if len(tmp) > 0:
                    tmp = int(tmp[0].replace("v", ""))
                    self.cVer = tmp + 1 if tmp >= self.cVer else self.cVer
        
        self.updateFilename()

    def updateFilename(self):

        if self.isValid:

            if self.isHero:
                self.oFile = "%s.mp4" % (self.cFile)
            else:
                self.oFile = "%s_v%03d.mp4" % (self.cFile, self.cVer)

            self.ui.filenameText.setText(self.oFile.replace("techAnim", "anim"))

        else:
            self.ui.filenameText.clear()

    def play(self):

        if len(self.ui.fileChoose.selectedIndexes()) < 1:
            return

        item = self.cList[self.ui.fileChoose.currentRow()]
        # if os.path.exists(QT) and os.path.exists(item):
        #     subprocess.Popen([QT, item])
        if os.path.exists(item):
            os.startfile(item)

    def delete(self):

        if len(self.ui.fileChoose.selectedIndexes()) < 1:
            return
            
        item = self.cList[self.ui.fileChoose.currentRow()]
        if os.path.exists(item):
            try:
                os.remove(item)
            except:
                QtGui.QMessageBox.about(self.ui, "Error", "Cannot delete - %s\nCheck if Quicktime is playing the file" % os.path.basename(item))

        self.updateFileList()

    def clickVersion(self):
        self.isHero = False
        # self.updateFilename()

    def clickHero(self):
        self.isHero = True
        # self.updateFilename()

    def toggleFilename(self):

        if self.ui.overrideCheck.isChecked():
            self.ui.filenameText.setEnabled(True)
        else:
            self.ui.filenameText.setEnabled(False)

    def changeSize(self):

        if self.ui.sizeChoose.currentIndex() == 10:
            self.ui.customWidth.setEnabled(True)
            self.ui.customHeight.setEnabled(True)
        else:
            self.ui.customWidth.setEnabled(False)
            self.ui.customHeight.setEnabled(False)

            if self.ui.sizeChoose.currentIndex() <= 1:

                width = cmds.control(cmds.getPanel(withFocus = True), q=True, w=True)
                height = cmds.control(cmds.getPanel(withFocus = True), q=True, h=True)
                if self.ui.sizeChoose.currentIndex() == 1:              
                    width = width/2
                    height = height/2
                width = width - (width%16)
                height = height - (height%16)

                self.ui.customWidth.setText(str(width))
                self.ui.customHeight.setText(str(height))
            else:
                self.ui.customWidth.setText(str(self.sizeList[self.ui.sizeChoose.currentIndex()][0]))
                self.ui.customHeight.setText(str(self.sizeList[self.ui.sizeChoose.currentIndex()][1]))

    def doPlayblast(self):

        if not cmds.file(q=True, sn=True):
            self.clear()
            QtGui.QMessageBox.about(self.ui, "Error", "Empty scene.")
            return

        # check if file name is empty
        name = str(self.ui.filenameText.text())
        if not name or name.strip() == '':
            QtGui.QMessageBox.about(self.ui, "Error", "Output filename is empty.")
            return
        name = name.replace("techAnim", "anim")

        # check size
        if self.ui.sizeChoose.currentIndex() == 9:

            width  = str(self.ui.customWidth.text())
            height = str(self.ui.customHeight.text())

            if not width.isdigit() or not height.isdigit():
                QtGui.QMessageBox.about(self.ui, "Error", "With and height must be number.")
                return

            width = int(width)
            height = int(height)

        elif self.ui.sizeChoose.currentIndex() == 0:

            width = cmds.control(cmds.getPanel(withFocus = True), q=True, w=True)
            height = cmds.control(cmds.getPanel(withFocus = True), q=True, h=True)

        elif self.ui.sizeChoose.currentIndex() == 1:
            
            width = cmds.control(cmds.getPanel(withFocus = True), q=True, w=True)
            height = cmds.control(cmds.getPanel(withFocus = True), q=True, h=True)
            width = width/2
            height = height/2

        else:

            width = self.sizeList[self.ui.sizeChoose.currentIndex()][0]
            height = self.sizeList[self.ui.sizeChoose.currentIndex()][1]

        if width < 160 or height < 160:
            QtGui.QMessageBox.about(self.ui, "Error", "Playblast size is too small.\nMinimum size is 160 x 160")
            return
        
        width = width - (width % 16)
        height = height - (height % 16)

        # delete temp path, recreate output path just in case
        if os.path.exists(os.path.dirname(TMP)):
            shutil.rmtree(os.path.dirname(TMP))
        os.makedirs(os.path.dirname(TMP))
        if not os.path.exists(os.path.join(self.oPath, "preview")):
            os.makedirs(os.path.join(self.oPath, "preview"))

        # prepare UI
        self.preUI()

        # in case add sequence switch
        sequence = False
        
        if sequence:
            args = {
                "format"         : "iff",
                "filename"       : TMP,
                "sequenceTime"   : 0,
                "forceOverwrite" : True,
                "clearCache"     : True,
                "viewer"         : 0,
                "showOrnaments"  : 1,
                "fp"             : 4,
                "percent"        : 100,
                "quality"        : 100,
                "width"          : width,
                "height"         : height,
                "offScreen"      : True,
                "indexFromZero"  : True,
            }
            
        else:
            args = {
                "format"         : "avi",
                "filename"       : TMP,
                "sequenceTime"   : 0,
                "forceOverwrite" : True,
                "clearCache"     : True,
                "viewer"         : 0,
                "showOrnaments"  : 1,
                "fp"             : 4,
                "percent"        : 100,
                "quality"        : 100,
                                "compression"    : "none",
                "width"          : width,
                "height"         : height,
                "offScreen"      : True,
            }

            # get active sound node
            gp = mel.eval('$tmpVar=$gPlayBackSlider')
                        
            if cmds.timeControl(gp, q=True, s=True):
                args["sound"] = cmds.timeControl(gp, q=True, s=True)
        
        # Add time range if "selection" is checked
        if self.ui.range_selection.isChecked():
            slider = mel.eval('$tmpVar=$gPlayBackSlider')
            startframe, endframe = cmds.timeControl(slider,q=True,rangeArray=True)
            args["startTime"] = startframe
            args["endTime"] = endframe



        # store old resolution & prepare render settings
        oldWidth  = cmds.getAttr( "defaultResolution.width" )
        oldHeight = cmds.getAttr( "defaultResolution.height" )
        self.HD(width, height)

        # add user name
        username = str(self.ui.e.text()).rstrip()
        cmds.headsUpDisplay('userHUD', section=6, block=1, blockSize='medium', label=username, labelFontSize='large')

        # playblast
        cmds.playblast(**args)
        QtGui.qApp.processEvents()

        # reset UI & render size
        cmds.headsUpDisplay('userHUD', remove=1)
        self.HD(oldWidth, oldHeight)
        self.postUI()

        # set fps
        fpsMaya = cmds.currentUnit(q=True, time=True)
        if 'fps' in fpsMaya:
            fps = fpsMaya.replace("fps", "")
        else:
            fpsList = {
                "game" : 15,
                "film" : 24,
                "pal"  : 25,
                "ntsc" : 30,
                "show" : 48,
                "palf" : 50,
                "ntscf" : 60,
            }
            try:
                fps = fpsList[fpsMaya]
            except:
                fps = 30
        if sequence:
            ffmpegCommand  = "%s -y -i \"%s.%s.png\" -r %s " % (ffmpeg, TMP, "%04d", fps)
        else:
            ffmpegCommand = "%s -y -i \"%s.avi\" -r %s " % (ffmpeg, TMP, fps)

        # switch codec. 0 for h264, 1 for MJPEG
        if self.ui.codecChoose.currentIndex() == 0: # -- h264
            ffmpegCommand += "-vcodec libx264 -vprofile baseline -crf 22 -bf 0 "
        else: #----------------------------------------- MJPEG
            ffmpegCommand += "-vcodec mjpeg -q:v 2 "
        ffmpegCommand += "-pix_fmt yuv420p -f mov "


        # no longer separate. Always play to version and COPY to hero
        # if self.isHero:
        #   outPath = os.path.join(self.oPath, name)
        #   ffmpegCommand += "\"%s\"" % outPath
        # else:
        #   outPath = os.path.join(self.oPath, "preview", name)
        #   ffmpegCommand += "\"%s\"" % outPath
        outPath = os.path.join(self.oPath, "preview", name)
        tmpHero = os.path.dirname(os.path.dirname(self.oPath))
        if "/nine/" in tmpHero or "\\nine\\" in tmpHero:
            tmpHero = os.path.dirname(tmpHero)
        tmpHero = os.path.join(tmpHero, 'preview')
        tmpHero = os.path.normpath(tmpHero)
        if not os.path.isdir(tmpHero):
            os.makedirs(tmpHero)
        heroPath = os.path.join(tmpHero, "%s.mp4" % self.cFile)
        ffmpegCommand += "\"%s\"" % outPath

        subprocess.call(ffmpegCommand)
        QtGui.qApp.processEvents()

        # copy outPath to heroPath
        shutil.copyfile(outPath, heroPath)

        if os.path.exists(QT) and os.path.exists(outPath):
            if r"P:\nine\all" in outPath:
                addPlay(outPath.replace("\\", "/"))
            subprocess.Popen([QT, outPath])

        self.updateFileList()

    def preUI(self):
        
        self.OVList = [ # list of option vars to query
            "selectDetailsVisibility",
            "objectDetailsVisibility",
            "polyCountVisibility",
            "animationDetailsVisibility",
            "fbikDetailsVisibility",
            "frameRateVisibility",
            "currentFrameVisibility",
            "currentContainerVisibility",
            "cameraNamesVisibility",
            "focalLengthVisibility",
            "viewAxisVisibility"
        ]
        
        self.userOV = []
        for i, ov in enumerate(self.OVList):
            self.userOV.append( cmds.optionVar(q=ov) )
            
        self.sets = [
            'setSelectDetailsVisibility',
            'setObjectDetailsVisibility',
            'setPolyCountVisibility',
            'setAnimationDetailsVisibility',
            'setFbikDetailsVisibility',
            'setFrameRateVisibility',
            'setCurrentFrameVisibility',
            'setCurrentContainerVisibility',
            'setCameraNamesVisibility',
            'setFocalLengthVisibility',
            'setViewAxisVisibility'
        ]
        setOV = [ 0,0,0,0,0,0,0,1,0,1,1,0 ]

        for i, s in enumerate(self.sets):
            mel.eval('%s(%s)' % (s, setOV[i] ) )

        self.frameOn()

    def postUI(self):

        for i, s in enumerate(self.sets):
            mel.eval('%s(%s)' % (s, self.userOV[i] ) )

        self.frameOff()

    def frameOn(self):

        if not self.ui.frameCheck.isChecked():
            return

        view = cmds.modelPanel(cmds.getPanel(wf=True), q=True, cam=True)
        cam = cmds.listRelatives(view)[0]
        self.camList = [
            "displayResolution",
            "displayGateMask",
            "displayGateMaskOpacity",
            "displayGateMaskColor"
        ]
        self.userCam = []
        for v in self.camList:
            self.userCam.append( cmds.getAttr("%s.%s" % (cam,v) ))

        cmds.camera(view, e=True, displayFilmGate=False, displayResolution=True, overscan=1.3)
        cmds.setAttr("%s.displayGateMask" % cam, 1)
        cmds.setAttr("%s.displayGateMaskOpacity" % cam, 1)
        cmds.setAttr("%s.displayGateMaskColor" % cam, 0, 0, 0)

        cmds.headsUpDisplay(rp=(3, 0))
        cmds.headsUpDisplay("RiffHUD", section=3, block=0, label="Scene Name:  %s" % os.path.basename(cmds.file(q=True, sn=True)))

    def frameOff(self):
        
        if not self.ui.frameCheck.isChecked():
            return

        view = cmds.modelPanel(cmds.getPanel(wf=True), q=True, cam=True)
        cam = cmds.listRelatives(view)[0]
        cmds.setAttr("%s.displayResolution" % cam,self.userCam[0])
        cmds.setAttr("%s.displayGateMask" % cam, self.userCam[1])
        cmds.setAttr("%s.displayGateMaskOpacity" % cam, self.userCam[2])
        cmds.setAttr("%s.displayGateMaskColor" % cam, self.userCam[3][0][0], self.userCam[3][0][1], self.userCam[3][0][2])

        cmds.headsUpDisplay(rp=(3, 0))

    def HD(self, width=None, height=None, sequence=False):
        
        if sequence:
            # switch render globals to png
            # 8=jpg, 32=png, 19=tga
            cmds.setAttr("defaultRenderGlobals.imageFormat", 32) # png
        
        
        if width is not None: cmds.setAttr( "defaultResolution.width", width)
        if height is not None: cmds.setAttr( "defaultResolution.height", height)
        #cmds.setAttr( "defaultResolution.deviceAspectRatio", 1.777)
        cmds.setAttr( "defaultResolution.pixelAspect", 1)

    def clear(self):

        self.ui.browseLabel.clear()
        self.ui.fileChoose.clear()
        self.ui.filenameText.clear()

    def show(self):

        self.ui.show()

# ====================== MAYA ============================

def runInMaya():
    global playblastApp
    try:
        playblastApp.ui.close()
    except:
        pass
    playblastApp = Playblast(parent=getMayaWindow())
