import sys
import os
import re
import subprocess
import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel

if os.path.isdir( "C:\\Users\\AuntieHome" ) :
    comName = 'AuntieHome'
elif os.path.isdir( "C:\\Users\\Riff" ) :
    comName = 'Riff'
    
quickTime = "C:\\Program Files (x86)\\QuickTime\\QuickTimePlayer.exe"
pathIcon = "C:\\Users\\AuntieHome\\Dropbox\\script\\pipeTools\\ncmel\\Pipeline\\Playblast"    
pathUserName = "C:\\Users\\%s\\Documents\\maya\\scripts" %comName
nameTxt = "username.txt"
pathTxt = pathUserName + "\\" + nameTxt
extention = "mov"

def playblastUI( version = '1.0') :
    
    width = 501
    
    if pm.window( 'PlayblastUI' , exists = True ) :
        pm.deleteUI( 'PlayblastUI' )
        
    window = pm.window( 'PlayblastUI', title = "Playblast Tools  %s" %version , width = width ,
        mnb = True , mxb = False , sizeable = True , rtf = True )

    window = pm.window( 'PlayblastUI', e = True , width = width )
    
    listLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = ( 1 , 500 ))
    with listLayout :
        pm.textScrollList( 'previewListsTS' , h = 300 , numberOfRows = 15 , allowMultiSelection = 1 )
        pm.separator( vis = False , h = 3 )
    
    button1Layout = pm.rowColumnLayout ( h = 40 ,w = width , nc = 3 , columnWidth = [ ( 1 , 246 ) , ( 2 , 5 ) , ( 3 , 246 )])
    with button1Layout :
        pm.button( label = 'PLAY' , h = 35 , c = playButton )
        pm.separator( vis = False )
        pm.button( label = 'DELETE' , h = 35 , c = deleteButton )
    
    separator1Layout = pm.rowColumnLayout ( h = 10 ,w = width , nc = 1 , columnWidth = ( 1 , 497 ))
    with separator1Layout :
        pm.separator( h = 5 , st = 'in' )
    
    pathLayout = pm.rowColumnLayout ( h = 25 ,w = width , nc = 4 , columnWidth = [ ( 1 , 60 ) , ( 2 , 405 ) , ( 3 , 5 ) , ( 4 , 25 )]) 
    with pathLayout :
        pm.text( label = 'Path File :' )
        pm.textField( 'currPathTF' , en = False , text = '' )
        pm.separator( vis = False )
        pm.button( label = '...' , h = 1 , c = browserDir )
    
    separator2Layout = pm.rowColumnLayout ( h = 10 ,w = width , nc = 1 , columnWidth = ( 1 , 497 ))
    with separator2Layout :
        pm.separator( h = 5 , st = 'in' )
        
    fileNameLayout = pm.rowColumnLayout ( h = 25 , w = width , nc = 4 , columnWidth = [ ( 1 , 65 ) , ( 2 , 305 ) , ( 3 , 5 ) , ( 4 , 125 ) ])
    with fileNameLayout :
        pm.text( label = 'File Name :' )
        pm.textField( 'fileNameTF' , en = False )
        pm.separator( vis = False )
        pm.checkBox( 'overrideFileNameCB' , l = 'Override File Name' , onc = "mc.textField( 'fileNameTF' , e = True , en = True )" 
                                                                     , ofc = "mc.textField( 'fileNameTF' , e = True , en = False )" 
                    )
    
    separator3Layout = pm.rowColumnLayout ( h = 10 ,w = width , nc = 1 , columnWidth = ( 1 , 497 ))
    with separator3Layout :
        pm.separator( h = 5 , st = 'in' )
    
    resolutionLayout = pm.rowColumnLayout ( h = 25 , w = width , nc = 6 , columnWidth = [ ( 1 , 70 ) , ( 2 , 110 ) , ( 3 , 20 ) , ( 4 , 110 ) , ( 5 , 10 ) , ( 6 , 175 )])
    with resolutionLayout :
        pm.text( label = 'Resolution :' )
        pm.textField( 'widthTF' , en = False )
        pm.text( label = 'X' )
        pm.textField( 'heightTF' , en = False )
        pm.separator( vis = False )
        pm.optionMenu ( 'resolutionOM' , label = '' , w = 175 , cc = setResolution )
        mc.menuItem ( l = 'From Window' )
        mc.menuItem ( l = 'Custom' )
        mc.menuItem ( l = '320 x 240' )
        mc.menuItem ( l = '480 x 270' )
        mc.menuItem ( l = '720 x 405' )
        mc.menuItem ( l = '960 x 400' )
        mc.menuItem ( l = '960 x 540' )
        mc.menuItem ( l = '1280 x 720' )
        mc.menuItem ( l = '1920 x 800' )
        mc.menuItem ( l = '1920 x 1080' )
    
    separator3Layout = pm.rowColumnLayout ( h = 10 ,w = width , nc = 1 , columnWidth = ( 1 , 497 ))
    with separator3Layout :
        pm.separator( h = 5 , st = 'in' )
    
    subLayout = pm.rowColumnLayout ( w = width , nc = 2 , columnWidth = [ ( 1 , width / 2.0 ) , ( 2 , width / 2.0 )])
    with subLayout :
        subLeftLayout = pm.rowColumnLayout ( w = width / 2.0 , nc = 1 , columnWidth = ( 1 , width / 2.0 ))
        with subLeftLayout :
            pbRangeLayout = pm.rowColumnLayout ( w = width / 2.0 , nc = 4 , columnWidth = [ ( 1 , 93 ) , ( 2 , 7 ) , ( 3 , 70 ) , ( 4 , 70 )])
            with pbRangeLayout :
                pm.text( label = 'Playblast Range :' )
                pm.separator( vis = False )
                pm.radioCollection( 'pbRangeRC' )
                pm.radioButton( 'timelineRB' , l = 'Timeline' )
                pm.radioButton( 'selectionRB' , l = 'Selection' )
                pm.radioCollection( 'pbRangeRC' , e = True , select = 'timelineRB' )
    
            separator3Layout = pm.rowColumnLayout ( h = 5 , w = width , nc = 1 , columnWidth = ( 1 , width / 2.0 ))
            with separator3Layout :
                pm.separator( vis = False )
            
            addUserNameLayout = pm.rowColumnLayout ( w = width / 2.0 , nc = 4 , columnWidth = [ ( 1 , 95 ) , ( 2 , 130 ) , ( 3 , 5 ) , ( 4 , 50 )])
            with addUserNameLayout :
                pm.text( label = 'Add Use Name  :' )
                pm.textField( 'userNameTF' , text = '' )
                pm.separator( vis = False )
                pm.checkBox( 'userNameTxtCB' , l = '' , v = True , onc = "mc.textField( 'userNameTF' , e = True , en = True )" 
                                                                 , ofc = "mc.textField( 'userNameTF' , e = True , en = False )" 
                            )
            
            addUserNameBTLayout = pm.rowColumnLayout ( w = width / 2.0 , nc = 2 , columnWidth = [ ( 1 , 95 ) , ( 2 , 149 )])
            with addUserNameBTLayout :
                pm.separator( vis = False )
                pm.button( label = 'SET' , h = 20 , c = setUserName )
            
        subRightLayout = pm.rowColumnLayout ( w = width / 2.0 , nc = 1 , columnWidth = ( 1 , width / 2.0 ) )
        with subRightLayout :
            buttonPlayblastLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = ( 1 , 244 ))
            with buttonPlayblastLayout :
                pm.button( label = '. . . PLAYBLAST . . .' , h = 68 , c = playBlastButton )
                # pm.symbolButton ( image = '%s\\PlayblastButton.jpg' %pathIcon , height = 68 , c = playBlastButton )
        
    pm.showWindow( window )
    
def getUserName( *args ) :
    if not os.path.isfile( pathTxt ) :
        userTxt = open( pathTxt , "w" )
    
    userTxt = open( pathTxt , "r" )
    userName = userTxt.read()
    userTxt.close()
    
    if userName :
        pm.textField( 'userNameTF' , e = True , text = userName , en = False )
        pm.checkBox( 'userNameTxtCB' , e = True , v = False )
            
    return userName

def setUserName( *args ) :
    userName = pm.textField( 'userNameTF' , q = True , text = True )
    userTxt = open( pathTxt , "w" )
    userTxt.write( "%s" %userName )
    userTxt.close()
    
    pm.textField( 'userNameTF' , e = True , text = userName , en = False )
    pm.checkBox( 'userNameTxtCB' , e = True , v = False )
    
    setDefault()
     
    return userName
    
def getDataFld( *args ) :
    file = os.path.normpath( mc.file( q = True , sn = True ))
    
    if not file == '.' :
        tmpAry = file.split( '\\' )
        tmpAry[-2] = 'playblast'
        
        outputFld = '\\'.join( tmpAry[0:-1] )
        
        if not os.path.isdir( outputFld ) :
            os.mkdir( outputFld )
        
        return outputFld

def getFilesList( *args ) :
    path = getDataFld()
    fileLists = pm.getFileList( fld = path , fs = '*.%s' %extention )
    fileLists.sort()
        
    return fileLists

def getFilesVersion( *args ) :
    fileLists = getFilesList()
    
    if fileLists :
        lastestVersion = re.findall(r'_v([0-9]{3})' , fileLists[-1] )

        if lastestVersion:
            nextVersion = '%03d' % (int(lastestVersion[0]) + 1)
    else :
        nextVersion = '001'
    
    return nextVersion

def getFilesName( *args ) :
    if pm.checkBox( 'overrideFileNameCB' , q = True , v = True ) == True :
        name = pm.textField( 'fileNameTF' , q = True , text = True )
        fileName = name.split( "." )[0]
    else :
        file = os.path.normpath( mc.file( q = True , sn = True ))
        fileMA = file.split( '\\' )[-1]
        version = re.findall(r'_v([0-9]{3})' , fileMA )
        
        if version :
            name = fileMA.split( '_v' + version[0] )[0]
        else :
            name = fileMA.split( '.ma' )[0]
        
        version = getFilesVersion()
        userName = getUserName()
        if userName : 
            userName = '_' + userName
        
        fileName = name + '_v' + version + userName
        
    return fileName

def browserDir( *args ) :
    dir = pm.textField( 'currPathTF' , q = True , tx = True )
    os.startfile( dir )

def getResolution( *args ) :
    selectRes = pm.optionMenu ( 'resolutionOM' , q = True , v = True )
    
    if selectRes == 'From Window' :
        width = pm.control ( pm.getPanel ( withFocus = True ) , q = True , w = True )
        height = pm.control ( pm.getPanel ( withFocus = True ) , q = True , h = True )
        
    elif selectRes == 'Custom' :
        pass
        
    else :
        width , height = selectRes.split( ' x ' )
    
    return int(width) , int(height)

def setResolution( *args ) :
    if pm.optionMenu ( 'resolutionOM' , q = True , v = True ) == 'Custom' :
        pm.textField( 'widthTF' , e = True , en = True , text = '' )
        pm.textField( 'heightTF' , e = True , en = True , text = '' )
    else :
        width , height = getResolution()
        pm.textField( 'widthTF' , e = True , en = False , text = width )
        pm.textField( 'heightTF' , e = True , en = False , text = height )

def playButton( *args ) :
    item = pm.textScrollList( 'previewListsTS' ,  q = True , selectItem = True )[0]

    if item :
        subprocess.Popen( [ quickTime , getDataFld() + "\\" + item ] )
    else :
        mc.warning( 'No Select List' )

def deleteButton( *args ) :
    items = pm.textScrollList( 'previewListsTS' ,  q = True , selectItem = True )

    if items :
        for item in items :
            os.remove( getDataFld() + "\\" + item ) 
    else :
        mc.warning( 'No Select List' )
        
    setDefault()

def playBlastButton( *args ) :
    path = getDataFld()
    fileName = getFilesName()
    userName = getUserName()
    width , height = getResolution()
    
    filePath = os.path.normpath( mc.file( q = True , sn = True ))
    sceneName = filePath.split( '\\' )[-1]
    
    startTime = pm.playbackOptions( q = True , minTime = True )
    endTime = pm.playbackOptions( q = True , maxTime = True )
    range = (endTime - startTime)+1
    
    activeModelView = pm.playblast ( ae = True )
    activeModelView = activeModelView.split( '|' )[-1]
    camera = pm.modelPanel ( activeModelView , q = True , cam = True )
    cameraShape = camera
    
    disResolution = pm.getAttr( cameraShape + '.displayResolution' )
    disGateMask = pm.getAttr( cameraShape + '.displayGateMask' )
    disGateMaskOpacity = pm.getAttr( cameraShape + '.displayGateMaskOpacity' )
    disGateMaskColor = pm.getAttr( cameraShape + '.displayGateMaskColor' )
    disOverScan = pm.getAttr( cameraShape + '.overscan' )
    
    mc.setAttr( cameraShape + '.displayResolution' , 1 )
    mc.setAttr( cameraShape + '.displayGateMask' , 1 )
    mc.setAttr( cameraShape + '.displayGateMaskOpacity' , 1.0 )
    mc.setAttr( cameraShape + '.displayGateMaskColor' , 0 , 0 , 0 )
    
    clearHUD()
    
    pm.headsUpDisplay( 'HudSCN' , s = 5 , b = 0 , bs = 'small' , l = 'Scene Name  :  ' + sceneName )
    pm.headsUpDisplay( 'HudUSN' , s = 5 , b = 1 , bs = 'small' , l = 'User  :  ' + userName )
    pm.headsUpDisplay( 'HudFRN' , s = 9 , b = 0 , bs = 'small' , l = 'Frame Range  :   %s  -  %s  ( %s )' %(int(startTime),int(endTime),int(range)))
    pm.headsUpDisplay( 'HudFNB' , s = 9 , b = 1 , bs = 'small' , l = 'Frame Number  :' , c = "pm.currentTime( q = True )" , atr = True )
    
    cmdPlayblast = {
                          "filename"       : path + "\\" + fileName ,
                          "sequenceTime"   : 0 ,
                          "format"         : "qt" , 
                          "widthHeight"    : ( width , height ) ,
                          "quality"        : 100 , 
                          "percent"        : 100 , 
                          "framePadding"   : 4 , 
                          "forceOverwrite" : True ,
                          "clearCache"     : True , 
                          "viewer"         : False , 
                          "showOrnaments"  : True ,
                          "compression"    : "H.264" ,
                          "offScreen"      : True ,
                    }
      
    timeCtrl = mel.eval('$tmpVar=$gPlayBackSlider')
        
    if pm.timeControl( timeCtrl , q = True , s = True ):
        cmdPlayblast["sound"] = pm.timeControl( timeCtrl , q = True , s = True )
        
    if pm.radioCollection( 'pbRangeRC' , q = True , sl = True ) == 'selectionRB' :
        startframe , endframe = pm.timeControl( timeCtrl , q = True , rangeArray = True )
        cmdPlayblast["startTime"] = startframe
        cmdPlayblast["endTime"] = endframe
        
    pm.playblast(**cmdPlayblast)
    
    outputPath = '%s\%s.%s' %( path , fileName , extention )
    clearHUD()
    
    mc.setAttr( cameraShape + '.displayResolution' , disResolution )
    mc.setAttr( cameraShape + '.displayGateMask' , disGateMask )
    mc.setAttr( cameraShape + '.displayGateMaskOpacity' , disGateMaskOpacity )
    
    setDefault()
    subprocess.Popen( [ quickTime , outputPath ] )
    
def clearHUD( *args ) :
    pm.headsUpDisplay (rp = ( 5 , 0 ))
    pm.headsUpDisplay (rp = ( 5 , 1 ))
    pm.headsUpDisplay (rp = ( 9 , 0 ))
    pm.headsUpDisplay (rp = ( 9 , 1 ))
    
def setDefault( *args ) :
    path = getDataFld()
    
    if path :
        pm.textField( 'currPathTF' , e = True , tx = path )
        
        fileLists = getFilesList()
    
        if fileLists == [] :
            pm.textScrollList( 'previewListsTS' , e = True , ra = True )
        else :
            pm.textScrollList( 'previewListsTS' , e = True , ra = True )
            pm.textScrollList( 'previewListsTS' , e = True , a = fileLists )
            
        pm.checkBox( 'overrideFileNameCB' , e = True , v = False )
        pm.textField( 'fileNameTF' , e = True , en = False )
        
        fileName = getFilesName() + "." + extention
        pm.textField( 'fileNameTF' , e = True , text = fileName )
        
        setResolution()