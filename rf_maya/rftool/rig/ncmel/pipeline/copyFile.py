import os
import shutil
import maya.cmds as mc
from shutil import copytree

pathMain = r'P:\ThePool\all\scene\anim'
shotAll = os.listdir( pathMain )

exception = [ 'preview' , '.mayaSwatches' ]

pathManiTrack = r'C:\Users\nathachon.c\Desktop\New folder (2)'
shTracks = os.listdir( pathManiTrack )
pathTrack = r'P:\ThePool\all\scene\track'
pathComp = r'P:\ThePool\all\scene\comp'
pathNtp = r'P:\ThePool\all\scene\ntp'

for shTrack in shTracks :
    num = shTrack.split( '_' )[1]
    if os.path.exists ( '%s\\cg%s0' %(pathTrack,num)) == False :
        os.makedirs ( '%s\\cg%s0' %(pathTrack,num))
        for folder in ( 'hero' , 'output' , 'version' ) :
            os.makedirs ( '%s\\cg%s0\\%s' %(pathTrack,num,folder))


for shots in shotAll :
    if not shots in exception :
        if os.path.exists ( '%s\\%s' %(pathTrack,shots)) == False :
            os.makedirs ( '%s\\%s' %(pathTrack,shots))
            for folder in ( 'hero' , 'output' , 'version' ) :
                os.makedirs ( '%s\\%s\\%s' %(pathTrack,shots,folder))

for shTrack in shTracks :
    num = shTrack.split( '_' )[1]
    shCg = 'cg%s0' %num
    inTrack = os.listdir( r'%s\%s' %(pathManiTrack,shTrack))
    
    for each in inTrack :
        pathCopy = r'%s\%s\%s' %(pathManiTrack,shTrack,each)
        if '.ma' in each :
            if os.path.exists ( r'%s\%s\hero\%s_track.ma' %(pathTrack,shCg,shCg)) == False :
                shutil.copy2( pathCopy , r'%s\%s\hero\%s_track.ma' %(pathTrack,shCg,shCg))
                print pathCopy + '>>' + r'%s\%s\hero\%s_track.ma' %(pathTrack,shCg,shCg)
        elif '.mp4' in each :
            if os.path.exists ( r'%s\%s\output\%s_track.mp4' %(pathTrack,shCg,shCg)) == False :
                shutil.copy2( pathCopy , r'%s\%s\output\%s_track.mp4' %(pathTrack,shCg,shCg))
                print pathCopy + '>>' + r'%s\%s\output\%s_track.mp4' %(pathTrack,shCg,shCg)
        elif '.ntp' in each :
            if os.path.exists ( r'%s\%s\%s_ntp.ntp' %(pathNtp,shCg,shCg)) == False :
                shutil.copy2( pathCopy , r'%s\%s\%s_ntp.ntp' %(pathNtp,shCg,shCg))
                print pathCopy + '>>' + r'%s\%s\%s_ntp.ntp' %(pathNtp,shCg,shCg)
        elif '.exr' in each :
            if os.path.exists ( r'%s\%s\%s' %(pathComp,shCg,each)) == False :
                shutil.copy2( pathCopy , r'%s\%s' %(pathNtp,shCg))
                print pathCopy + '>>' + r'%s\%s' %(pathNtp,shCg)
        else :
            shutil.copytree( pathCopy , r'%s\%s\image\%s' %(pathTrack,shCg,each))
            print pathCopy + '>>' + r'%s\%s\image' %(pathTrack,shCg)





for shots in shotAll :
    if not shots in exception :
        if os.path.exists ( '%s\\%s' %(pathComp,shots)) == False :
            os.makedirs ( '%s\\%s' %(pathComp,shots))
            for folder in ( 'ae' , 'mocha' , 'render' , 'retouch' ) :
                os.makedirs ( '%s\\%s\\%s' %(pathComp,shots,folder))
            

for shots in shotAll :
    if not shots in exception :
        if os.path.exists ( '%s\\%s' %(pathNtp,shots)) == False :
            os.makedirs ( '%s\\%s' %(pathNtp,shots))
            for folder in ( 'hero' , 'output' , 'version' , 'original_dpx' ) :
                os.makedirs ( '%s\\%s\\%s' %(pathNtp,shots,folder))
                
#####################################################################################################################
                
pathMain = r'C:\Users\nathachon.c\Desktop\New folder (2)'
pathCopy = r'P:\ThePool\all\scene\track'











import errno
 
def copy(src, dest):
    try:
        shutil.copytree(src, dest)
    except OSError as e:
        # If the error was caused because the source wasn't a directory
        if e.errno == errno.ENOTDIR:
            shutil.copy(src, dest)
        else:
            print('Directory not copied. Error: %s' % e)






