import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  SPINE RIGGING MODULE [ FK Only ]
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , spine1TmpJnt = 'Spine1_TmpJnt' , 
                         spine2TmpJnt = 'Spine2_TmpJnt' , 
                         spine3TmpJnt = 'Spine3_TmpJnt' , 
                         spine4TmpJnt = 'Spine4_TmpJnt' , 
                         parent       = 'Root_jnt' ,
                         ctrlGrp      = 'Ctrl_Grp' , 
                         skinGrp      = 'Skin_Grp' , 
                         elem         = '' ,
                         size         = 1 
                 ):

        ##-- Info
        elem = elem.capitalize()

        #-- Create Main Group
        self.spineCtrlGrp = rt.createNode( 'transform' , 'Spine%sCtrl_Grp' %elem )
        mc.parentConstraint( parent , self.spineCtrlGrp , mo = False )
        
        #-- Create Joint
        self.spine1PosJnt = rt.createJnt( 'Spine%s1Pos_Jnt' %elem , spine1TmpJnt )
        self.spine2PosJnt = rt.createJnt( 'Spine%s2Pos_Jnt' %elem , spine2TmpJnt )
        self.spine3PosJnt = rt.createJnt( 'Spine%s3Pos_Jnt' %elem , spine3TmpJnt )
        self.spine4PosJnt = rt.createJnt( 'Spine%s4Pos_Jnt' %elem , spine4TmpJnt )

        self.spine1Jnt = rt.createJnt( 'Spine%s1_Jnt' %elem , spine1TmpJnt )
        self.spine2Jnt = rt.createJnt( 'Spine%s2_Jnt' %elem , spine2TmpJnt )
        self.spine3Jnt = rt.createJnt( 'Spine%s3_Jnt' %elem , spine3TmpJnt )
        self.spine4Jnt = rt.createJnt( 'Spine%s4_Jnt' %elem , spine4TmpJnt )

        mc.parent( self.spine4PosJnt , self.spine3PosJnt )
        mc.parent( self.spine3PosJnt , self.spine2PosJnt )
        mc.parent( self.spine2PosJnt , self.spine1PosJnt )
        mc.parent( self.spine4Jnt , self.spine4PosJnt )
        mc.parent( self.spine3Jnt , self.spine3PosJnt )
        mc.parent( self.spine2Jnt , self.spine2PosJnt )
        mc.parent( self.spine1Jnt , self.spine1PosJnt )

        #-- Create Controls
        self.spine1Ctrl = rt.createCtrl( 'Spine%s1_Ctrl' %elem , 'circle' , 'red' )
        self.spine1Gmbl = rt.addGimbal( self.spine1Ctrl )
        self.spine1Zro = rt.addGrp( self.spine1Ctrl )
        self.spine1Ofst = rt.addGrp( self.spine1Ctrl , 'Ofst')
        self.spine1Zro.snap( self.spine1Jnt )

        self.spine2Ctrl = rt.createCtrl( 'Spine%s2_Ctrl' %elem , 'circle' , 'red' )
        self.spine2Gmbl = rt.addGimbal( self.spine2Ctrl )
        self.spine2Zro = rt.addGrp( self.spine2Ctrl )
        self.spine2Ofst = rt.addGrp( self.spine2Ctrl , 'Ofst')
        self.spine2Zro.snap( self.spine2Jnt )

        self.spine3Ctrl = rt.createCtrl( 'Spine%s3_Ctrl' %elem , 'circle' , 'red' )
        self.spine3Gmbl = rt.addGimbal( self.spine3Ctrl )
        self.spine3Zro = rt.addGrp( self.spine3Ctrl )
        self.spine3Ofst = rt.addGrp( self.spine3Ctrl , 'Ofst')
        self.spine3Zro.snap( self.spine3Jnt )

        self.spine4Ctrl = rt.createCtrl( 'Spine%s4_Ctrl' %elem , 'circle' , 'red' )
        self.spine4Gmbl = rt.addGimbal( self.spine4Ctrl )
        self.spine4Zro = rt.addGrp( self.spine4Ctrl )
        self.spine4Ofst = rt.addGrp( self.spine4Ctrl , 'Ofst')
        self.spine4Zro.snap( self.spine4Jnt )

        #-- Adjust Shape Controls
        for ctrl in ( self.spine1Ctrl , self.spine1Gmbl , self.spine2Ctrl , self.spine2Gmbl , self.spine3Ctrl , self.spine3Gmbl , self.spine4Ctrl , self.spine4Gmbl ) :
            ctrl.scaleShape( size * 1.8 )
        
        #-- Adjust Rotate Order
        for obj in ( self.spine1Ctrl , self.spine2Ctrl , self.spine3Ctrl , self.spine4Ctrl , self.spine1PosJnt , self.spine2PosJnt , self.spine3PosJnt , self.spine4PosJnt , self.spine1Jnt , self.spine2Jnt , self.spine3Jnt , self.spine4Jnt) :
            obj.setRotateOrder( 'xzy' )

        #-- Rig Process
        mc.parentConstraint( self.spine1Gmbl , self.spine1PosJnt , mo = False )
        mc.parentConstraint( self.spine2Gmbl , self.spine2PosJnt , mo = False )
        mc.parentConstraint( self.spine3Gmbl , self.spine3PosJnt , mo = False )
        mc.parentConstraint( self.spine4Gmbl , self.spine4PosJnt , mo = False )

        rt.addSquash( self.spine1Ctrl , self.spine1Jnt )
        rt.addSquash( self.spine2Ctrl , self.spine2Jnt )
        rt.addSquash( self.spine3Ctrl , self.spine3Jnt )
        rt.addSquash( self.spine4Ctrl , self.spine4Jnt )

        rt.addFkStretch( self.spine1Ctrl , self.spine2Ofst )
        rt.addFkStretch( self.spine2Ctrl , self.spine3Ofst )
        rt.addFkStretch( self.spine3Ctrl , self.spine4Ofst )

        rt.localWorld( self.spine1Ctrl , ctrlGrp , self.spineCtrlGrp , self.spine1Zro , 'orient' )
        
        #-- Adjust Hierarchy
        mc.parent( self.spine2Zro , self.spine1Gmbl )
        mc.parent( self.spine3Zro , self.spine2Gmbl )
        mc.parent( self.spine4Zro , self.spine3Gmbl )

        mc.parent( self.spine1Zro , self.spineCtrlGrp )
        mc.parent( self.spineCtrlGrp , ctrlGrp )
        mc.parent( self.spine1PosJnt , parent )

        #-- Cleanup
        for obj in ( self.spineCtrlGrp , self.spine1Zro , self.spine2Zro , self.spine3Zro , self.spine4Zro , self.spine1Ofst , self.spine2Ofst , self.spine3Ofst , self.spine4Ofst ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for obj in ( self.spine1Ctrl , self.spine2Ctrl , self.spine3Ctrl , self.spine4Ctrl , self.spine1Gmbl , self.spine2Gmbl , self.spine3Gmbl , self.spine4Gmbl ) :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        mc.select( cl = True )