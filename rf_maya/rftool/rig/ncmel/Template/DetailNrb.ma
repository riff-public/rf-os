//Maya ASCII 2018ff09 scene
//Name: DetailNrb.ma
//Last modified: Mon, Aug 24, 2020 12:42:00 PM
//Codeset: 1252
requires maya "2018ff09";
requires -nodeType "ilrOptionsNode" -nodeType "ilrUIOptionsNode" -nodeType "ilrBakeLayerManager"
		 "Turtle" "2018.0.0";
requires "stereoCamera" "10.0";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" "mtoa" "1.4.2.3";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201903222215-65bada0e52";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
fileInfo "vrayBuild" "3.52.03 b05f456";
createNode transform -s -n "persp";
	rename -uid "6927C922-4345-3114-CFF5-FFBE45CC1720";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.6853523952635006 17.046067384393119 8.0638969343547764 ;
	setAttr ".r" -type "double3" -21.338352729599098 18.600000000000296 -8.3895882366224765e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "F193F058-43EC-CCEF-3265-93917E3394A9";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 4.0202549582467544;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.01394990086555481 15.639416259966055 4.5615927419248123 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "CB523160-41C1-3C68-E28D-7E9119431255";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "705ED737-47DF-4182-1645-4A8D952EBF92";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "6541BC72-459B-5254-937F-FEB21EBCB45F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "B54B6D6A-40C4-036E-0BC6-96A1DAEE78F0";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "FB7F1093-430F-F18F-61D8-B4A9453DD4FE";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "1348EBF9-4E82-B1CC-491D-A39713BCDCC6";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "Export_Grp";
	rename -uid "F2BEDAA7-4213-4412-6526-3AAA003064A6";
	setAttr ".t" -type "double3" 0 3.9581175272084863 2.0177212199750438 ;
createNode transform -n "DtlNrbTmp_Grp" -p "Export_Grp";
	rename -uid "7DB60438-4544-865C-6040-D491464674F5";
createNode transform -n "EyeBrowA_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "F1D978A5-4E74-D43B-FCC4-E8BE16990DA2";
	setAttr ".t" -type "double3" 0.22418715059757233 12.572835922241211 1.1254059076309204 ;
createNode nurbsSurface -n "EyeBrowA_L_NrbTmpShape" -p "EyeBrowA_L_NrbTmp";
	rename -uid "EDB1E5FB-4D82-E9D4-BEEF-2C9AF9EAF059";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.01439887679863645 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761486 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761486 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605489 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeBrowA_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "102C9DDE-450E-6497-EC6F-2A9C83E1D194";
createNode nurbsSurface -n "EyeBrowA_R_NrbTmpShape" -p "EyeBrowA_R_NrbTmp";
	rename -uid "E30AE11B-40F5-A25F-4CE5-77A7B200B190";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.01439887679863645 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761486 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761486 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605489 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeBrowB_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "6BA90EB0-4FB2-31A8-BBE8-8885B76D2252";
	setAttr ".t" -type "double3" 0.68189629912376404 12.709766864776611 0.91846656799316406 ;
createNode nurbsSurface -n "EyeBrowB_L_NrbTmpShape" -p "EyeBrowB_L_NrbTmp";
	rename -uid "7812DEE6-4443-351F-301D-30AC5F9B4698";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848812706824e-07
		-0.017102687047033882 0.014399414365761488 4.3343411795238044e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713708258477e-07
		0.01711969698638818 0.014398882604605491 -6.3609848786061465e-07
		
		;
createNode transform -n "EyeBrowB_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "60BE59B2-4B88-2386-3B50-3CAF4BC6FA5A";
createNode nurbsSurface -n "EyeBrowB_R_NrbTmpShape" -p "EyeBrowB_R_NrbTmp";
	rename -uid "FD1BFBE0-46DE-E129-1F4F-65913406EE21";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798638228 6.3609848790502363e-07
		-0.017102687047033882 0.014399414365759712 4.3343411773033583e-07
		0.017119670543499145 -0.014399414365763264 -3.5226713730462937e-07
		0.01711969698638818 0.014398882604603714 -6.3609848808265925e-07
		
		;
createNode transform -n "EyeBrowC_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "DF04FC66-4D30-8E01-17FE-9396BC271453";
	setAttr ".t" -type "double3" 1.0314302485988747 12.625239845461845 0.60929659008979797 ;
createNode nurbsSurface -n "EyeBrowC_L_NrbTmpShape" -p "EyeBrowC_L_NrbTmp";
	rename -uid "06C4E6E4-4A96-454F-4502-EBA1B3EB626C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.0143988767986347 6.3609848801488783e-07
		-0.017102687047033882 0.01439941436576324 4.3343411784020119e-07
		0.017119670543499145 -0.014399414365759687 -3.5226713719244902e-07
		0.01711969698638818 0.014398882604607291 -6.3609848797047884e-07
		
		;
createNode transform -n "EyeBrowC_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "81B05A57-4545-9A6B-79C6-EB91ED63C221";
createNode nurbsSurface -n "EyeBrowC_R_NrbTmpShape" -p "EyeBrowC_R_NrbTmp";
	rename -uid "BDCD01DB-49AF-E4E9-0063-079FF46E1A78";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848812706824e-07
		-0.017102687047033882 0.014399414365761488 4.3343411795238044e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713708258477e-07
		0.01711969698638818 0.014398882604605491 -6.3609848786061465e-07
		
		;
createNode transform -n "EyeLid1_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "96E82425-4B21-7905-AE7A-8C8F3B742DA2";
	setAttr ".t" -type "double3" 0.33224295079708099 12.09290599822998 0.93386787176132202 ;
createNode nurbsSurface -n "EyeLid1_L_NrbTmpShape" -p "EyeLid1_L_NrbTmp";
	rename -uid "BA551AD9-4D0C-B4F9-8585-F182A15BC83E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798638228 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365759712 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365763264 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604603714 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid1_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "DC3AB472-4608-2A5C-D0E2-3B8D877CA94F";
createNode nurbsSurface -n "EyeLid1_R_NrbTmpShape" -p "EyeLid1_R_NrbTmp";
	rename -uid "62A86D7E-4DAF-DE30-009B-C0A5BD7C0AB1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798638228 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365759712 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365763264 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604603714 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid2_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "A7E56228-4B56-0E72-1727-74BDDD7559EE";
	setAttr ".t" -type "double3" 0.39541274309158325 12.214681148529053 0.97241514921188354 ;
createNode nurbsSurface -n "EyeLid2_L_NrbTmpShape" -p "EyeLid2_L_NrbTmp";
	rename -uid "890F1D3B-4026-8802-9F45-35A70D2DD92E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid2_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "892E1C69-4A31-E63A-B819-2087B7530561";
createNode nurbsSurface -n "EyeLid2_R_NrbTmpShape" -p "EyeLid2_R_NrbTmp";
	rename -uid "9EF05EE0-489A-0C7B-47CA-FE8EEB441265";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid3_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "8D707D10-4346-8A2B-4ABC-0A9EA10A3D2B";
	setAttr ".t" -type "double3" 0.51512342691421509 12.294593811035156 0.98238548636436462 ;
createNode nurbsSurface -n "EyeLid3_L_NrbTmpShape" -p "EyeLid3_L_NrbTmp";
	rename -uid "481258A0-4855-311E-39FA-8D89C0E154E8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.01439887679863645 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761486 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761486 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605489 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid3_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "D068AE45-465A-7536-C011-82B35A307BB6";
createNode nurbsSurface -n "EyeLid3_R_NrbTmpShape" -p "EyeLid3_R_NrbTmp";
	rename -uid "DFC361D7-49C5-E161-5028-29A14C6B397B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.01439887679863645 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761486 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761486 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605489 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid4_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "12792F78-4B02-CFD3-33E9-D88E4B4C6586";
	setAttr ".t" -type "double3" 0.69323298335075378 12.305427074432373 0.93767714500427246 ;
createNode nurbsSurface -n "EyeLid4_L_NrbTmpShape" -p "EyeLid4_L_NrbTmp";
	rename -uid "C9942EB5-4D43-B0BD-05CE-9FB9F81650A4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid4_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "20801690-4732-019C-4FC9-D1853C53F104";
createNode nurbsSurface -n "EyeLid4_R_NrbTmpShape" -p "EyeLid4_R_NrbTmp";
	rename -uid "E3B98317-4E87-973C-948C-0F80491D5AF4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848812706824e-07
		-0.017102687047033882 0.014399414365761488 4.3343411795238044e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713708258477e-07
		0.01711969698638818 0.014398882604605491 -6.3609848786061465e-07
		
		;
createNode transform -n "EyeLid5_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "D9A193B0-45DD-5E34-27E2-96860BDA2C14";
	setAttr ".t" -type "double3" 0.85068419575691223 12.285143852233887 0.81072080135345459 ;
createNode nurbsSurface -n "EyeLid5_L_NrbTmpShape" -p "EyeLid5_L_NrbTmp";
	rename -uid "7AB255B4-4AC8-5025-817F-A68A2A567107";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid5_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "2C1D49BC-421B-B14B-D650-17B69EA4417A";
createNode nurbsSurface -n "EyeLid5_R_NrbTmpShape" -p "EyeLid5_R_NrbTmp";
	rename -uid "B4A16C13-48A5-066B-8D28-7AB9972F5C64";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid6_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "83A73A35-46B5-096B-980A-6C883F2E4D79";
	setAttr ".t" -type "double3" 0.91663414239883423 12.256645202636719 0.68348917365074158 ;
createNode nurbsSurface -n "EyeLid6_L_NrbTmpShape" -p "EyeLid6_L_NrbTmp";
	rename -uid "B7BD5D5A-4FA8-7ECD-3661-8B8E7F5EDAAA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid6_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "18953806-4AE0-0AC4-7FA5-329630041BED";
createNode nurbsSurface -n "EyeLid6_R_NrbTmpShape" -p "EyeLid6_R_NrbTmp";
	rename -uid "4242299A-41B3-3945-ACE6-D48248CEC504";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid7_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "040A51C1-4702-88F6-718A-0D930E826E2D";
	setAttr ".t" -type "double3" 0.92938172817230225 12.237788200378418 0.62530389428138733 ;
createNode nurbsSurface -n "EyeLid7_L_NrbTmpShape" -p "EyeLid7_L_NrbTmp";
	rename -uid "55A149B2-4960-E9EB-121F-968CCF56999A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.017119696986388291 -0.014398876798638226 6.3609848801604593e-07
		-0.017102687047033993 0.01439941436575971 4.3343411784135813e-07
		0.017119670543499034 -0.014399414365763263 -3.5226713719360707e-07
		0.017119696986388069 0.014398882604603713 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid7_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "9727745A-494F-06A1-5D8E-6F9D56A2F8BE";
createNode nurbsSurface -n "EyeLid7_R_NrbTmpShape" -p "EyeLid7_R_NrbTmp";
	rename -uid "6E1A5D61-478E-7966-F5F5-FFB324861C71";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.017119696986388069 -0.014398876798638226 6.3609848801604593e-07
		-0.017102687047033771 0.01439941436575971 4.3343411784135813e-07
		0.017119670543499256 -0.014399414365763263 -3.5226713719360707e-07
		0.017119696986388291 0.014398882604603713 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid8_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "4DF2A9F1-43B6-F8F9-FE54-DB9D06FFEEE3";
	setAttr ".t" -type "double3" 0.92200514674186707 12.185025215148926 0.6685350239276886 ;
createNode nurbsSurface -n "EyeLid8_L_NrbTmpShape" -p "EyeLid8_L_NrbTmp";
	rename -uid "82D0795A-4376-4358-EE66-85A6B409A357";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid8_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "A0EECBAD-4230-F85F-5293-01821845C8C5";
createNode nurbsSurface -n "EyeLid8_R_NrbTmpShape" -p "EyeLid8_R_NrbTmp";
	rename -uid "C36B53A1-46E8-C4A3-EC4B-479998EC2F77";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.017119696986388069 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033771 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499256 -0.014399414365761488 -3.5226713719360707e-07
		0.017119696986388291 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid9_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "9437D292-444E-3F95-70E2-7AA3FB058BCD";
	setAttr ".t" -type "double3" 0.86666557192802429 12.100058555603027 0.77638784050941467 ;
createNode nurbsSurface -n "EyeLid9_L_NrbTmpShape" -p "EyeLid9_L_NrbTmp";
	rename -uid "21BA591B-408E-BC14-04EA-5B9B19466DBE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.017119696986388069 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033771 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499256 -0.014399414365761488 -3.5226713719360707e-07
		0.017119696986388291 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid9_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "CDBE5874-45C4-D101-5598-C1B815791CAA";
createNode nurbsSurface -n "EyeLid9_R_NrbTmpShape" -p "EyeLid9_R_NrbTmp";
	rename -uid "73054545-4B15-4DEF-A433-3C8B884FD8B4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848812706824e-07
		-0.017102687047033882 0.014399414365761488 4.3343411795238044e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713708258477e-07
		0.01711969698638818 0.014398882604605491 -6.3609848786061465e-07
		
		;
createNode transform -n "EyeLid10_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "7D76606F-48F4-1D98-24AC-C4BA6308DD71";
	setAttr ".t" -type "double3" 0.73327669501304626 12.039028644561768 0.89694172143936157 ;
createNode nurbsSurface -n "EyeLid10_L_NrbTmpShape" -p "EyeLid10_L_NrbTmp";
	rename -uid "675939A7-4D9D-8DFF-C206-A394774AB984";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid10_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "DCF55B3E-48C2-3027-E8BD-268CE5DC6A3C";
createNode nurbsSurface -n "EyeLid10_R_NrbTmpShape" -p "EyeLid10_R_NrbTmp";
	rename -uid "12FDB8F0-4900-BAD3-29F7-B18FDC0FBFC7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.01439887679863645 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761486 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761486 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605489 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid11_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "2461D96B-4A42-38EC-629B-F8BD196BBC23";
	setAttr ".t" -type "double3" 0.54668587446212769 12.018935680389404 0.94914796948432922 ;
createNode nurbsSurface -n "EyeLid11_L_NrbTmpShape" -p "EyeLid11_L_NrbTmp";
	rename -uid "73218425-4926-EDE6-0E1D-2BB1CFA3AAF0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid11_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "1E005776-43E4-5A25-D746-498ACE53F61E";
createNode nurbsSurface -n "EyeLid11_R_NrbTmpShape" -p "EyeLid11_R_NrbTmp";
	rename -uid "BE85819F-4FA1-D3B8-1268-698BA8BEF34F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid12_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "8544CB67-4EC8-13E3-7685-1B82BF7620F3";
	setAttr ".t" -type "double3" 0.38521066308021545 12.054333686828613 0.94242087006568909 ;
createNode nurbsSurface -n "EyeLid12_L_NrbTmpShape" -p "EyeLid12_L_NrbTmp";
	rename -uid "6EFF20AC-4F79-3E50-43C9-0485E354E5BB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeLid12_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "FBEF9896-430D-EF32-499C-898597797B3D";
createNode nurbsSurface -n "EyeLid12_R_NrbTmpShape" -p "EyeLid12_R_NrbTmp";
	rename -uid "63276E26-47DC-8F59-5CE0-C0AA64182AB2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848812686844e-07
		-0.017102687047033882 0.014399414365761488 4.3343411795258028e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713708278461e-07
		0.01711969698638818 0.014398882604605491 -6.3609848786041485e-07
		
		;
createNode transform -n "Mouth1_C_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "8B062122-432E-2012-F222-8697E4691ECD";
	setAttr ".t" -type "double3" 0 11.285415649414063 1.754729151725769 ;
createNode nurbsSurface -n "Mouth1_C_NrbTmpShape" -p "Mouth1_C_NrbTmp";
	rename -uid "D1424473-4E77-B810-EBEF-C69C64B7BB31";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth2_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "5376727F-4181-3FF6-B97E-CC98044DF33F";
	setAttr ".t" -type "double3" 0.19268487393856049 11.303689002990723 1.6963689923286438 ;
createNode nurbsSurface -n "Mouth2_L_NrbTmpShape" -p "Mouth2_L_NrbTmp";
	rename -uid "387D6D46-4D35-D595-5414-D98997E785E1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848823809054e-07
		-0.017102687047033882 0.014399414365761488 4.3343411806340274e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713697156246e-07
		0.01711969698638818 0.014398882604605491 -6.3609848774959234e-07
		
		;
createNode transform -n "Mouth2_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "2FABC172-49AD-5DB6-1089-92B1D50960ED";
createNode nurbsSurface -n "Mouth2_R_NrbTmpShape" -p "Mouth2_R_NrbTmp";
	rename -uid "9B766AF4-45FB-3079-4FEA-1AB6E29CD319";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth3_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "1F6317FD-4364-1336-3F34-4CA6918514F0";
	setAttr ".t" -type "double3" 0.36464349925518036 11.365231990814209 1.519255518913269 ;
createNode nurbsSurface -n "Mouth3_L_NrbTmpShape" -p "Mouth3_L_NrbTmp";
	rename -uid "64B47EE2-4B94-60C2-58EB-E59772E65946";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848779400133e-07
		-0.017102687047033882 0.014399414365761488 4.3343411761931353e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713741565167e-07
		0.01711969698638818 0.014398882604605491 -6.3609848819368155e-07
		
		;
createNode transform -n "Mouth3_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "2CF5AEDB-48DD-000E-6C2D-77A8169E1541";
createNode nurbsSurface -n "Mouth3_R_NrbTmpShape" -p "Mouth3_R_NrbTmp";
	rename -uid "34B8C20F-40A6-648D-C70E-12BC0FBDE2F8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.017119696986388235 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033938 0.014399414365761488 4.3343411784135813e-07
		0.01711967054349909 -0.014399414365761488 -3.5226713719360707e-07
		0.017119696986388124 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth4_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "A3B85B20-45DA-2D2B-8304-57B19E5E0565";
	setAttr ".t" -type "double3" 0.49515649676322937 11.429200172424316 1.2015820145606995 ;
createNode nurbsSurface -n "Mouth4_L_NrbTmpShape" -p "Mouth4_L_NrbTmp";
	rename -uid "168525D8-483C-58B7-0EF3-C49A68189A85";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth4_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "036204AC-4FBF-CE75-5EBF-4B9A007B8A49";
createNode nurbsSurface -n "Mouth4_R_NrbTmpShape" -p "Mouth4_R_NrbTmp";
	rename -uid "5DFD0865-42E8-B79A-5A4B-EB81FAE997E5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth5_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "198E6B9F-43F5-087D-F8B1-6FB8B0B8F73A";
	setAttr ".t" -type "double3" 0.66269847750663757 11.466302871704102 0.82376033067703247 ;
createNode nurbsSurface -n "Mouth5_L_NrbTmpShape" -p "Mouth5_L_NrbTmp";
	rename -uid "0CA663D0-4836-802F-1807-BCB27B6658F5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848790502363e-07
		-0.017102687047033882 0.014399414365761488 4.3343411773033583e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713730462937e-07
		0.01711969698638818 0.014398882604605491 -6.3609848808265925e-07
		
		;
createNode transform -n "Mouth5_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "F53259F2-4084-4B5E-955B-20B36A3B3EFE";
createNode nurbsSurface -n "Mouth5_R_NrbTmpShape" -p "Mouth5_R_NrbTmp";
	rename -uid "BF7819E2-4D25-2733-AA75-7C969F4C56E4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848790502363e-07
		-0.017102687047033882 0.014399414365761488 4.3343411773033583e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713730462937e-07
		0.01711969698638818 0.014398882604605491 -6.3609848808265925e-07
		
		;
createNode transform -n "Mouth7_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "D99B7493-4BEB-59E0-721C-ADA0BA27D94B";
	setAttr ".t" -type "double3" 0.73242363333702087 11.428490161895752 0.66724222898483276 ;
createNode nurbsSurface -n "Mouth7_L_NrbTmpShape" -p "Mouth7_L_NrbTmp";
	rename -uid "45B1D0CC-48DA-5C52-01ED-8983DAC1334D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.017119696986388291 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033993 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499034 -0.014399414365761488 -3.5226713719360707e-07
		0.017119696986388069 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth7_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "B814393F-49D5-D465-B08D-C98367730D19";
createNode nurbsSurface -n "Mouth7_R_NrbTmpShape" -p "Mouth7_R_NrbTmp";
	rename -uid "C5E792DC-4306-C21E-BDD6-7E8F33FC9D09";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth9_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "3B8CE8F8-4356-0F7A-3C20-ECA0A9BB0BE4";
	setAttr ".t" -type "double3" 0.62663620710372925 11.421159744262695 0.78917673230171204 ;
createNode nurbsSurface -n "Mouth9_L_NrbTmpShape" -p "Mouth9_L_NrbTmp";
	rename -uid "377FB66C-404C-6192-B842-A7BEDFAFF178";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth9_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "77712880-4491-31FC-BD9D-26A43ABDF58E";
createNode nurbsSurface -n "Mouth9_R_NrbTmpShape" -p "Mouth9_R_NrbTmp";
	rename -uid "26F6C4A2-4AD1-3406-17A1-27869172BE69";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth10_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "AC7D3A77-4E6E-9F21-E24E-91BA86A780CA";
	setAttr ".t" -type "double3" 0.47497643530368805 11.416275978088379 1.1639820337295532 ;
createNode nurbsSurface -n "Mouth10_L_NrbTmpShape" -p "Mouth10_L_NrbTmp";
	rename -uid "D55F725D-47BA-0F3A-659D-62A7DACCB8B8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.017119696986388124 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033827 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499201 -0.014399414365761488 -3.5226713719360707e-07
		0.017119696986388235 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth10_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "F6F08474-4A0F-F912-320D-E1B0B490CF3C";
createNode nurbsSurface -n "Mouth10_R_NrbTmpShape" -p "Mouth10_R_NrbTmp";
	rename -uid "016989AA-487C-80DA-66E7-FDBB80BF1686";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth11_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "0F859B2F-4A3C-4582-1CB6-1CA542EBDF81";
	setAttr ".t" -type "double3" 0.33148273825645447 11.367685317993164 1.4640331864356995 ;
createNode nurbsSurface -n "Mouth11_L_NrbTmpShape" -p "Mouth11_L_NrbTmp";
	rename -uid "DCD5AB5E-46E7-DBAC-6BB3-ACA6FE9AF4D3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth11_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "EA4FE08C-4D35-A061-A1DB-10A284D51E4B";
createNode nurbsSurface -n "Mouth11_R_NrbTmpShape" -p "Mouth11_R_NrbTmp";
	rename -uid "C61B6BC8-4232-B8DD-4CBA-1F803CD01EDC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth12_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "8127639E-4776-7B2D-B1C8-D98EDDF864D4";
	setAttr ".t" -type "double3" 0.18167725205421448 11.288053512573242 1.6330307722091675 ;
createNode nurbsSurface -n "Mouth12_L_NrbTmpShape" -p "Mouth12_L_NrbTmp";
	rename -uid "A9434CB6-42DB-6637-7F08-52A0910573D2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth12_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "265CE3E2-4C6F-72EF-7E43-4E94062A0D35";
createNode nurbsSurface -n "Mouth12_R_NrbTmpShape" -p "Mouth12_R_NrbTmp";
	rename -uid "DC99CDBB-41FD-94FE-53AF-58AF49E21CD9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Mouth13_C_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "5E3FD7F2-4D75-B3A5-D4AB-9AA1976D291E";
	setAttr ".t" -type "double3" 5.9977173805236816e-07 11.265630722045898 1.6902474164962769 ;
createNode nurbsSurface -n "Mouth13_C_NrbTmpShape" -p "Mouth13_C_NrbTmp";
	rename -uid "41519A60-4D8B-DC54-41A1-57B32B7E30FB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848779400133e-07
		-0.017102687047033882 0.014399414365761488 4.3343411761931353e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713741565167e-07
		0.01711969698638818 0.014398882604605491 -6.3609848819368155e-07
		
		;
createNode transform -n "CheekA_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "D3F5A8DD-40D8-14D4-02F8-15BE9AEE97E3";
	setAttr ".t" -type "double3" 0.33644667267799377 11.947360992431641 1.0570429563522339 ;
createNode nurbsSurface -n "CheekA_L_NrbTmpShape" -p "CheekA_L_NrbTmp";
	rename -uid "1B99B11F-4E17-A07F-5B4F-148D63E96C49";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "CheekA_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "04074735-4775-1330-9987-5A837EE1EF93";
createNode nurbsSurface -n "CheekA_R_NrbTmpShape" -p "CheekA_R_NrbTmp";
	rename -uid "A6D85C60-4734-3F26-F023-BCB7BB94A75B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "CheekB_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "7A2634AD-4FE3-A84F-5C6E-6ABEA847A678";
	setAttr ".t" -type "double3" 0.7272392213344574 11.734553337097168 0.83202031254768372 ;
createNode nurbsSurface -n "CheekB_L_NrbTmpShape" -p "CheekB_L_NrbTmp";
	rename -uid "F14D1226-4525-6ECB-0FFD-B69AB5344802";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798638228 6.3609848790502363e-07
		-0.017102687047033882 0.014399414365759712 4.3343411773033583e-07
		0.017119670543499145 -0.014399414365763264 -3.5226713730462937e-07
		0.01711969698638818 0.014398882604603714 -6.3609848808265925e-07
		
		;
createNode transform -n "CheekB_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "EEEF542B-425F-B5FA-21EC-F3B68B2727EC";
createNode nurbsSurface -n "CheekB_R_NrbTmpShape" -p "CheekB_R_NrbTmp";
	rename -uid "3AFA8958-47DB-A280-32BA-A19DA732C0FA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798638228 6.3609848790502363e-07
		-0.017102687047033882 0.014399414365759712 4.3343411773033583e-07
		0.017119670543499145 -0.014399414365763264 -3.5226713730462937e-07
		0.01711969698638818 0.014398882604603714 -6.3609848808265925e-07
		
		;
createNode transform -n "CheekC_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "D45B034B-4D7A-8D4C-2D39-F984ED845013";
	setAttr ".t" -type "double3" 1.0801971554756165 12.057019710540771 0.50731188058853149 ;
createNode nurbsSurface -n "CheekC_L_NrbTmpShape" -p "CheekC_L_NrbTmp";
	rename -uid "B64C9222-463A-E846-8E39-4EA3E9E8CE0E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "CheekC_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "3A19063B-4725-33B7-99EB-E895D68D4529";
createNode nurbsSurface -n "CheekC_R_NrbTmpShape" -p "CheekC_R_NrbTmp";
	rename -uid "F2EB0CFC-474B-08A3-82AD-EF804A5B3ED3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.360984880169963e-07
		-0.017102687047033882 0.014399414365761488 4.334341178423075e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719455739e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797258732e-07
		
		;
createNode transform -n "Puff_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "63561D42-41DB-8CF7-B3CD-3E88530C2A7B";
	setAttr ".t" -type "double3" 1.2524614930152893 11.585706233978271 0.3280823826789856 ;
createNode nurbsSurface -n "Puff_L_NrbTmpShape" -p "Puff_L_NrbTmp";
	rename -uid "C7288910-4ED4-C419-943F-4488977DD0E1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "Puff_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "6ACB45A3-432C-3696-F618-349E735D16F3";
createNode nurbsSurface -n "Puff_R_NrbTmpShape" -p "Puff_R_NrbTmp";
	rename -uid "88A13839-4925-A4F3-8FFF-FB89FB1B4EA2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "EyeBrow_C_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "193567D6-419C-D6F7-75A0-009B42E648DD";
	setAttr ".t" -type "double3" 0 12.614371299743652 1.1549332737922668 ;
createNode nurbsSurface -n "EyeBrow_C_NrbTmpShape" -p "EyeBrow_C_NrbTmp";
	rename -uid "9279E90F-4C25-B163-A811-3FA1B5F1132A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "NsA_C_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "5B3C1365-4EC1-9F48-1FF7-CB9332DA117E";
	setAttr ".t" -type "double3" 0 12.197513580322266 1.2953210473060608 ;
createNode nurbsSurface -n "NsA_C_NrbTmpShape" -p "NsA_C_NrbTmp";
	rename -uid "88AE4658-4BE1-E5B2-B765-21BED6947BE7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "NsB_C_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "5A774102-445C-665D-908E-858D48C10097";
	setAttr ".t" -type "double3" -0.01394990086555481 11.681298732757568 2.5438715219497681 ;
createNode nurbsSurface -n "NsB_C_NrbTmpShape" -p "NsB_C_NrbTmp";
	rename -uid "59DBF243-47FF-CC2D-9AAF-EBA77BCE2E1F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "MouthUprBsh_C_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "075079AD-43C5-D4BB-D058-1F98FA821DAA";
	setAttr ".t" -type "double3" -0.01394990086555481 11.424397424647902 1.7649176430484155 ;
createNode nurbsSurface -n "MouthUprBsh_C_NrbTmpShape" -p "MouthUprBsh_C_NrbTmp";
	rename -uid "D1DF7F89-4D7A-AAE6-B706-2B90AD48E2D9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "MouthLwrBsh_C_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "B87D23C0-44F6-4EC6-3426-DEA46B5E9680";
	setAttr ".t" -type "double3" -0.01394990086555481 11.134121297219743 1.7649176430484155 ;
createNode nurbsSurface -n "MouthLwrBsh_C_NrbTmpShape" -p "MouthLwrBsh_C_NrbTmp";
	rename -uid "5B778904-4B71-7E10-691F-3E83B974F837";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "MouthCnrBsh_L_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "CB7CDB0B-43B2-C2F0-F258-7CA6FCE29161";
	setAttr ".t" -type "double3" 0.8 11.42466527664352 0.6 ;
createNode nurbsSurface -n "MouthCnrBsh_L_NrbTmpShape" -p "MouthCnrBsh_L_NrbTmp";
	rename -uid "785392AA-4639-87FD-DFF2-28AA6CF8FDB4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "MouthCnrBsh_R_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "E258BC85-4FFB-8EE3-AD0E-B8AFE75ABE25";
	setAttr ".t" -type "double3" -0.8 11.42466527664352 0.6 ;
createNode nurbsSurface -n "MouthCnrBsh_R_NrbTmpShape" -p "MouthCnrBsh_R_NrbTmp";
	rename -uid "22063DDB-4026-CBC5-0B9A-148A2944CD61";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode transform -n "NoseBsh_C_NrbTmp" -p "DtlNrbTmp_Grp";
	rename -uid "52A8414E-4ECD-E130-561F-2CB5F5197A3E";
	setAttr ".t" -type "double3" -0.01394990086555481 11.681298732757568 2.5317940332433082 ;
createNode nurbsSurface -n "NoseBsh_C_NrbTmpShape" -p "NoseBsh_C_NrbTmp";
	rename -uid "19490CD0-4684-D53A-27B3-C48390BA9136";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		1 1 0 0 no 
		2 0 0.35066535514094321
		2 1 2
		
		4
		-0.01711969698638818 -0.014398876798636452 6.3609848801604593e-07
		-0.017102687047033882 0.014399414365761488 4.3343411784135813e-07
		0.017119670543499145 -0.014399414365761488 -3.5226713719360707e-07
		0.01711969698638818 0.014398882604605491 -6.3609848797163695e-07
		
		;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "14328BD1-4017-6524-8FC1-3881F8C3687E";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "9CAFA2E0-47FB-4F95-871F-70A244F85B5D";
	setAttr -s 2 ".dli[1]"  1;
createNode displayLayer -n "defaultLayer";
	rename -uid "199487AA-48A8-2FB2-3B7D-1FB47ECD542B";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "826A4559-4932-C29D-78EF-BA876ABDB507";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "CD666B82-4FFF-7B41-145E-B3A1A55E1F77";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "2B01EC33-4B09-DB9C-C2B2-F9983A9AC3D5";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "D3E5D8AF-4858-84E5-D0CD-69911766BB56";
	setAttr -s 2 ".bsdt";
	setAttr ".bsdt[0].bscd" -type "Int32Array" 1 -1 ;
	setAttr ".bsdt[1].bscd" -type "Int32Array" 0 ;
	setAttr ".bsdt[1].bsdn" -type "string" "DetailNrb";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "9425EE34-4C35-059B-D6BC-8580EF9AEBD4";
createNode ilrOptionsNode -s -n "TurtleRenderOptions";
	rename -uid "2C32DE0A-4BB4-8A89-6512-758A3CB6CE0A";
lockNode -l 1 ;
createNode ilrUIOptionsNode -s -n "TurtleUIOptions";
	rename -uid "9BAA5B7A-4995-2735-11F4-71874B464F45";
lockNode -l 1 ;
createNode ilrBakeLayerManager -s -n "TurtleBakeLayerManager";
	rename -uid "C2140F9F-4F39-E1F6-24FD-E18F6D54CA59";
lockNode -l 1 ;
createNode renderGlobals -s -n "swiffworksRenderGlobals";
	rename -uid "5FED26A8-4AE3-DF31-686B-7B8BD4ADBEBD";
	addAttr -ci true -sn "rm" -ln "renderMode" -at "short";
	addAttr -ci true -sn "twos" -ln "twoSided" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "ctc" -ln "convertToCurves" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "cqlty" -ln "curveQuality" -at "short";
	addAttr -ci true -sn "inttype" -ln "intersType" -at "short";
	addAttr -ci true -sn "dlt" -ln "defaultLineThickness" -dv 0.2 -at "float";
	addAttr -ci true -sn "dlo" -ln "defaultLineOpacity" -dv 1 -at "float";
	addAttr -ci true -sn "dlcr" -ln "defaultLineColorR" -at "float";
	addAttr -ci true -sn "dlcg" -ln "defaultLineColorG" -at "float";
	addAttr -ci true -sn "dlcb" -ln "defaultLineColorB" -at "float";
	addAttr -ci true -sn "bw" -ln "bandwidth" -dv 14400 -at "float";
	addAttr -ci true -sn "oswg" -ln "onlySWG" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "fa" -ln "defaultFoldAngle" -dv 45 -at "float";
	addAttr -ci true -sn "df" -ln "defaultDisplayFolds" -at "short";
createNode renderGlobals -s -n "inkPaintRenderGlobals";
	rename -uid "D6EBB825-48C5-3B68-87DC-74B03DD11574";
	addAttr -ci true -sn "regMode" -ln "regionMode" -at "short";
	addAttr -ci true -sn "dpi" -ln "dpiValue" -dv 100 -at "short";
createNode renderGlobals -s -n "inkworksRenderGlobals";
	rename -uid "97CABEDD-4E80-7836-78F9-7C9FF34C7603";
	addAttr -ci true -sn "bb" -ln "blendBackgr" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "rm" -ln "renderMode" -at "short";
	addAttr -ci true -sn "as" -ln "adaptiveSubdv" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "twos" -ln "twoSided" -dv 1 -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "oiw" -ln "onlyIW" -min 0 -max 1 -at "bool";
createNode animLayer -s -n "BaseAnimation";
	rename -uid "5C43FD3F-44E5-FCAF-2C08-91B5109ABD1A";
	setAttr ".ovrd" yes;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "8B8D26C4-43F1-113A-80C6-6D85688DD86D";
	setAttr ".AA_samples" 6;
	setAttr ".GI_sss_samples" 5;
	setAttr ".GI_volume_samples" 0;
	setAttr ".usesmpclamp" 1;
	setAttr ".usesmpclampaovs" 1;
	setAttr ".AA_sample_clamp" 5;
	setAttr ".sss_use_autobump" 1;
	setAttr ".GI_diffuse_depth" 2;
	setAttr ".GI_glossy_depth" 3;
	setAttr ".GI_reflection_depth" 3;
	setAttr ".GI_refraction_depth" 3;
	setAttr ".version" -type "string" "1.2.6.0";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "43C0F1A1-4230-9C12-90D5-EDB96D6CD3D4";
	setAttr ".ai_translator" -type "string" "blackman_harris";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "A52CA42E-41A3-AE5A-57F5-CCA3F86EA131";
	setAttr ".merge_AOVs" 1;
	setAttr ".tiled" 1;
	setAttr ".half_precision" 1;
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "CDB2D9FE-4DC3-5231-A225-058929FB90AF";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode renderLayerManager -n "DetailNrb_renderLayerManager";
	rename -uid "FC4A2B34-42E3-12E0-6DD6-95AA01A321CF";
createNode renderLayer -n "DetailNrb_defaultRenderLayer";
	rename -uid "30988C43-40DC-F166-063F-009EC80C8A70";
	setAttr ".g" yes;
createNode lambert -n "lambert2";
	rename -uid "CD0B85A0-47B4-D8F7-A5F5-0FA87088C390";
	setAttr ".c" -type "float3" 1 0 0 ;
createNode shadingEngine -n "lambert2SG";
	rename -uid "0BF20467-4267-C97A-0E70-D1BB83D54538";
	setAttr ".ihi" 0;
	setAttr -s 66 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "60F717B5-43E0-B64E-8DA9-5BA14FABC9A6";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "561799D4-4AA2-C7A4-9A7F-1C84B15E5F50";
	setAttr ".def" no;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -258.33332306808944 -288.09522664736227 ;
	setAttr ".tgi[0].vh" -type "double2" 555.95235886081889 319.0476063698062 ;
	setAttr -s 2 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" -95.714286804199219;
	setAttr ".tgi[0].ni[0].y" 151.42857360839844;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" 211.42857360839844;
	setAttr ".tgi[0].ni[1].y" 151.42857360839844;
	setAttr ".tgi[0].ni[1].nvs" 1923;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo1";
	rename -uid "B4E200EA-4E47-C5D9-DF12-1C8900AB4A87";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -5664.7521380830876 -3731.0221443928149 ;
	setAttr ".tgi[0].vh" -type "double2" -606.85912775486622 40.21387207998162 ;
createNode multiplyDivide -n "EyeLid8Tr_L_Mdv";
	rename -uid "62ABFCE6-4FA9-B411-BF9A-A9A8803B0D06";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeLid6Tr_L_Mdv";
	rename -uid "31B556F4-424E-C774-0C5E-229655112429";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "CheekCTr_L_Mdv";
	rename -uid "6DEF6F37-49D7-AB4E-EADC-9990C7B87CC4";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeLid4Tr_L_Mdv";
	rename -uid "933839AC-41BF-5030-A77A-03BABE5B1C66";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeLid5Tr_L_Mdv";
	rename -uid "D1115384-4492-145D-F511-8FBE94B5C1DE";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "Mouth10Tr_L_Mdv";
	rename -uid "707408D3-4E81-B891-D35E-07A08DA3D6DA";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeLid3Tr_L_Mdv";
	rename -uid "C14A8016-4A80-DA80-DF00-95B2C1898C14";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeLid7Tr_L_Mdv";
	rename -uid "E4600BDE-4622-750B-F0E9-7CBDADA32B65";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeBrowATr_L_Mdv";
	rename -uid "33670E23-4FB1-B811-58C4-4CA977220777";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "Mouth11Tr_L_Mdv";
	rename -uid "278D3983-4F98-BC0B-617D-58A493762A74";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "PuffTr_L_Mdv";
	rename -uid "63366602-4904-AFBB-927B-7383DBFF485A";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "CheekBTr_L_Mdv";
	rename -uid "C3D7182E-4497-5159-3FAC-3E8DE26C13BB";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeLid2Tr_L_Mdv";
	rename -uid "A9BE414C-4158-7833-BB80-289A60B32151";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "Mouth12Tr_L_Mdv";
	rename -uid "D8BAA87E-4BF8-18C6-8D59-22AA16190D43";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "CheekATr_L_Mdv";
	rename -uid "DEB59CD9-4355-9EC5-2662-63971F7EED50";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeLid1Tr_L_Mdv";
	rename -uid "72FD994B-45F4-CE63-7C05-D4AFB44D557F";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeBrowBTr_L_Mdv";
	rename -uid "74C5BDC6-4C01-9FE4-C909-4E918CAF69A0";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeBrowCTr_L_Mdv";
	rename -uid "C88124F7-4FD0-2F50-F7DF-F3AFFAA318AF";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "Mouth7Tr_L_Mdv";
	rename -uid "E53309E1-4887-7EA2-D8CA-5793DC79DC4F";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "Mouth5Tr_L_Mdv";
	rename -uid "21817508-4F5F-134E-9381-559204AFC1DD";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "Mouth9Tr_L_Mdv";
	rename -uid "E5379F9E-4C35-B3FD-7346-F294D6DC16F8";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "Mouth2Tr_L_Mdv";
	rename -uid "A4ED8945-4EF6-03E2-B14C-3C9BCB1CB6C6";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeLid12Tr_L_Mdv";
	rename -uid "63E82DF0-4710-ACB1-93B6-FF81F942D06F";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "Mouth3Tr_L_Mdv";
	rename -uid "67E26E9B-4472-CB80-1EE1-C1B2CBD3E339";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "Mouth4Tr_L_Mdv";
	rename -uid "49921EDC-4E4F-A25E-6CF1-08A845400A14";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeLid11Tr_L_Mdv";
	rename -uid "F6CC4F25-409B-8698-1359-28B482382B5B";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeLid10Tr_L_Mdv";
	rename -uid "23BB9B2F-4E00-E1FE-A83D-67AE819494C7";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeLid9Tr_L_Mdv";
	rename -uid "5E58DD13-44CF-4D59-6106-87B31DFFFC77";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode multiplyDivide -n "EyeLid8Ro_L_Mdv";
	rename -uid "F26D2558-4BA3-554D-7452-588140C3EDD5";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion1";
	rename -uid "0FC6FECA-4C27-3D19-8F6E-598096371D2B";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion2";
	rename -uid "A729DF6E-4E36-33C0-E926-10AE452FD4B6";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion3";
	rename -uid "9805C170-41F3-C65E-31AF-9EA9E08F5103";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion4";
	rename -uid "4C4C3AD6-4813-B40B-BFA9-01A1850D6AE8";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion5";
	rename -uid "6CC0F89D-4704-3BD1-0F3B-96812877BA2E";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion6";
	rename -uid "B9734344-4F72-2CF8-7D7C-70B1C98C5042";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "EyeLid6Ro_L_Mdv";
	rename -uid "0A497D8E-4888-6D66-F04D-56B4D4E9EFBC";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion7";
	rename -uid "CF51BF1A-4DC8-74C9-F2D8-969FECA21F2D";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion8";
	rename -uid "585A2C34-40B2-9B59-CA19-4DB50D4FC471";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion9";
	rename -uid "7D60B922-4E2A-8FE2-C12B-33AC74D83E20";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion10";
	rename -uid "EFC9B31B-4FA9-9919-BA61-E6825067998A";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion11";
	rename -uid "F0F6DA49-4389-F4B7-064F-84ABE8EF5632";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion12";
	rename -uid "6D25381B-4A5D-967B-0673-6FBDFB19FEC3";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "CheekCRo_L_Mdv";
	rename -uid "DF624AA7-4080-DDDF-B35E-6A9C007D559D";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion13";
	rename -uid "BC6FDFD8-4B3F-C033-2AAE-9E9E020C54A7";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion14";
	rename -uid "F4C1AA5D-4721-CE42-6B3C-0DB05294B290";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion15";
	rename -uid "252B9968-4C3B-98F8-A6A6-9E81387FBB6B";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion16";
	rename -uid "3B2B94F4-4669-03EB-F7ED-2FB4BA1F55C3";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion17";
	rename -uid "6B1F4AB5-4C50-A864-56E6-908C43E28088";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion18";
	rename -uid "0A7C0F16-44B0-1BE5-2996-A1B80EDA61BC";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "EyeLid4Ro_L_Mdv";
	rename -uid "38D1D110-4176-0727-E67E-5CAE5D394644";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion19";
	rename -uid "A6B209C6-4FC3-5654-27A5-5CB1354384FB";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion20";
	rename -uid "D0B6BD0C-4C31-59C0-499C-41A848134E52";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion21";
	rename -uid "8700ACC8-4A11-DDFD-7176-14A5883E97BF";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion22";
	rename -uid "208F3310-4FEE-C376-A155-A181256D7B6C";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion23";
	rename -uid "FBADEACC-470C-FCC4-B386-498080F10A24";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion24";
	rename -uid "7F3EE08C-420B-2886-002E-AD9E92E97DB0";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "EyeLid5Ro_L_Mdv";
	rename -uid "374CDA94-44A2-9A39-AC3F-8A898CDF2D32";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion25";
	rename -uid "6056942C-4850-000A-2EE4-3199BD32A245";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion26";
	rename -uid "CD039B22-4232-68D4-9621-6B99AB3056FE";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion27";
	rename -uid "D6A48097-4AD2-8AB5-68C4-32AE2AF14617";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion28";
	rename -uid "ED5AB126-493E-BBF5-13AC-F38943E34E3C";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion29";
	rename -uid "100FC4D8-4C81-8BD4-D52B-D0A1B33DEBF6";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion30";
	rename -uid "D7ADB4E7-403B-32CE-94DC-9BAB8D9BFC1C";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "Mouth10Ro_L_Mdv";
	rename -uid "44BB28CF-485E-9A2A-37BB-1292C45127F8";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion31";
	rename -uid "CBFCB32B-49EE-C515-AC52-3E97FD64C846";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion32";
	rename -uid "825746AB-4DDF-AABA-2AB1-3A868BE7EE4F";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion33";
	rename -uid "023580C9-4234-39AE-DBCB-2DA0739DE034";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion34";
	rename -uid "30800376-4B6D-37CF-C2A9-E19F580B32D3";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion35";
	rename -uid "52D285BE-4BB2-8BE9-75F7-30848F0BAA45";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion36";
	rename -uid "CBA84714-406D-9924-E67E-8982E1CA2F81";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "EyeLid3Ro_L_Mdv";
	rename -uid "F8A80E87-468C-F7BA-5AC9-86879BE45276";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion37";
	rename -uid "D79E8C89-47D0-4345-F8A3-66AECC6102FA";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion38";
	rename -uid "37E78C74-42CD-64F5-673E-3E9B46D4362F";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion39";
	rename -uid "82674308-4F86-3186-2B1F-C194F6EF33FC";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion40";
	rename -uid "9EE0CB07-4505-F079-BD6E-C1BCC9460811";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion41";
	rename -uid "ACF633E6-4774-84A9-8458-2A980A079DA9";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion42";
	rename -uid "CFFE8CE1-460D-10F9-0B63-82877C21ED4A";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "EyeLid7Ro_L_Mdv";
	rename -uid "6D7FFC87-4720-2647-30CB-1DAEC838271D";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion43";
	rename -uid "49C0236D-415D-14C4-E321-60827D49EF96";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion44";
	rename -uid "1FFF0624-4D78-ECDA-9C72-65BD74DD5E9E";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion45";
	rename -uid "9FE009A2-4EF8-6AD0-A8D7-12A8FDFC56B5";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion46";
	rename -uid "9C81A025-4D84-FB55-37A3-D9ABDADA8F7C";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion47";
	rename -uid "DE295A17-4136-E8D6-30B9-8A8458AFE4D3";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion48";
	rename -uid "D598BE8C-400C-B92C-E4BA-98972A30FD70";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "EyeBrowARo_L_Mdv";
	rename -uid "A93A8799-4F69-12E9-077B-AFA77B288486";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion49";
	rename -uid "05C33658-4099-BC46-579A-3EB15F52C634";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion50";
	rename -uid "4D31F930-428F-909F-6DE4-F8B9CBCC53AA";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion51";
	rename -uid "63B80EC0-44F6-F46A-B1D0-CEA5F8620D36";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion52";
	rename -uid "1DE61FA5-4C3B-D6D8-20C5-49B0FF945B6D";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion53";
	rename -uid "BD49011B-4206-22CF-CF0C-628AFFC05C63";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion54";
	rename -uid "0187A46F-4D47-17B5-623A-7D84771AC5F6";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "Mouth11Ro_L_Mdv";
	rename -uid "7E63E6F3-41C5-D3E0-D156-12A9A9FD63DA";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion55";
	rename -uid "AC34D3FD-4377-B1E9-D376-B08F2CF386C2";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion56";
	rename -uid "F76085DF-4070-13E2-795C-03B281107AB9";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion57";
	rename -uid "A50E292B-435F-FA44-F42C-7EA05DE7A14A";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion58";
	rename -uid "48BA76B3-464A-78D9-D6AD-9A8E09C59BAD";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion59";
	rename -uid "409333D7-42A1-6697-2AD9-EF9A9F33865D";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion60";
	rename -uid "77369D2D-4E13-0CF4-D133-2E906793AAD2";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "PuffRo_L_Mdv";
	rename -uid "026592D9-45B0-FDF9-9F8B-06833D360A5D";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion61";
	rename -uid "649014F9-4795-EB1A-3B86-B5B7ECD438A7";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion62";
	rename -uid "19BE6E87-488B-5440-F2DE-7584048A4C37";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion63";
	rename -uid "9B5A1116-429B-14E2-1489-66A2651B0D72";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion64";
	rename -uid "51DA7C0E-4A56-C97A-2AD2-E798CD74D95F";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion65";
	rename -uid "F0DF8639-4EDA-6753-A560-8EA645546254";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion66";
	rename -uid "C967CC65-44BC-E788-E81A-5499BA2C29B0";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "CheekBRo_L_Mdv";
	rename -uid "9222CBD8-4BBC-9C4E-EC6E-538614D1475B";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion67";
	rename -uid "61C1D9BC-4618-DB62-39F4-6EBCC4E7789A";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion68";
	rename -uid "65DF0855-4AE7-3AF0-D4FE-06AB8B35D032";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion69";
	rename -uid "A8EC730B-420E-4474-54D3-5FA43FC16F82";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion70";
	rename -uid "A8FF096E-47E2-61C1-00DC-0CB9AA7E324A";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion71";
	rename -uid "EA7F1BA1-47AE-2554-AC23-099D1EC2FA56";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion72";
	rename -uid "7C2E05FA-491B-1C53-3D7D-2D8BBCBD3D6F";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "EyeLid2Ro_L_Mdv";
	rename -uid "CDEEDF93-48CA-379B-42B3-9FB1D7346D28";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion73";
	rename -uid "4145281A-45DF-CB53-1F05-14AE817A02DF";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion74";
	rename -uid "3F9E75D6-45CF-2FF6-AA95-9882701DB653";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion75";
	rename -uid "0331C093-4255-8DDE-D35A-00A09D5305BA";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion76";
	rename -uid "E62DF7EB-4963-685C-1A15-359B932AF853";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion77";
	rename -uid "A267741F-44D4-F284-F00D-89888E6F050C";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion78";
	rename -uid "A4CFB4CE-49AD-FC77-0355-AB9199A5C64A";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "Mouth12Ro_L_Mdv";
	rename -uid "702CD465-4374-AE42-5429-2BB1D02C817B";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion79";
	rename -uid "AABE041B-406F-865B-F1E8-1C819E863537";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion80";
	rename -uid "3D5EDC31-469E-17D4-5D7F-5B9C27D936E6";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion81";
	rename -uid "BD0EF721-416D-FD0A-C2CC-F2886F395DD3";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion82";
	rename -uid "2A17A056-4E64-0322-B3BB-2EBFAD8F25F5";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion83";
	rename -uid "9FA2DFC0-48B1-65B4-31C6-E2A90A3C8A17";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion84";
	rename -uid "8159A4FE-48C2-E00B-02A3-71AFC78C6455";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "CheekARo_L_Mdv";
	rename -uid "1FDBCCD9-4D6D-C619-36DD-AC8E1506672B";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion85";
	rename -uid "DBCE7F37-489D-28CF-2486-43917AB74BB2";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion86";
	rename -uid "3D8CFE51-4D40-E072-1758-2B81CABF9E40";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion87";
	rename -uid "04304969-407B-003B-C646-27ACD3E49393";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion88";
	rename -uid "93684CC5-4710-C85D-FC4C-868D5CD42D79";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion89";
	rename -uid "19B179BD-457A-23B1-ED5B-C3892557CB68";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion90";
	rename -uid "444A4E6C-40D8-81AD-559D-DAB0ECC74F42";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "EyeLid1Ro_L_Mdv";
	rename -uid "304CA856-4221-2CD2-8B0E-5B8E4ABF437A";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion91";
	rename -uid "77B35491-448B-C1A2-BC41-F0A16B3FEC8F";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion92";
	rename -uid "96F2189F-47CE-38C6-D2E4-CA83FCADB4D1";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion93";
	rename -uid "C1032067-4BE1-F996-C580-3F884DF72398";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion94";
	rename -uid "E849ABB9-4F78-29BE-B7AC-34ACFABFD450";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion95";
	rename -uid "0EB65B87-468A-551A-3674-D7866821DF44";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion96";
	rename -uid "3DAFD946-4C52-F347-AC85-D49BD9B3D33E";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "EyeBrowBRo_L_Mdv";
	rename -uid "B415A413-4529-01E7-33AD-74A2D61CD0F7";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion97";
	rename -uid "E619CD53-47A4-FD88-D787-81AD7F6B5A5F";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion98";
	rename -uid "96DCD853-4BEB-2FBE-3CC1-B3923CB84AE6";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion99";
	rename -uid "1203CE58-4101-D8B3-1116-439D8357111B";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion100";
	rename -uid "7CF7B5AF-4B30-7B65-FE94-5CAA42887FAB";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion101";
	rename -uid "9871B0C0-4313-1C29-EEB7-C8B6EAFF5B41";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion102";
	rename -uid "A0DBC50E-46CA-BCA2-73BD-A793BABDB20A";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "EyeBrowCRo_L_Mdv";
	rename -uid "78C84AB2-49EA-952D-B49A-0DB7AE238758";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion103";
	rename -uid "1C2D7E16-49F1-AEE8-BEEC-1794E8C59CAD";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion104";
	rename -uid "B9234645-4969-7247-80B1-A0A531CB0F00";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion105";
	rename -uid "4E9E382D-4FFD-CB5E-B338-A48FBE8BC1BE";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion106";
	rename -uid "E3AFE0A6-4118-D05A-DCFD-86AE7B2504BE";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion107";
	rename -uid "2D0C2D68-4BF7-32CE-1CC6-7286FD8B6CEA";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion108";
	rename -uid "A58F874D-45BD-B8AD-9564-01906548711E";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "Mouth7Ro_L_Mdv";
	rename -uid "E8124BC1-4ED8-200F-077A-29BDF2AED1EF";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion109";
	rename -uid "5BAE111B-47BA-CFB0-FF53-C8AC8E6697AC";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion110";
	rename -uid "87F02B5D-498F-2291-E1B9-BE8B89B3216F";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion111";
	rename -uid "9A6C27F3-4D83-B7B4-3367-EEA806151AAE";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion112";
	rename -uid "113BFA53-4AE3-56BF-B77B-E89CF74D2674";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion113";
	rename -uid "357D4886-45A1-872F-9252-24978519A26F";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion114";
	rename -uid "478085D2-4E32-1493-3D32-2B834CE35EA7";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "Mouth5Ro_L_Mdv";
	rename -uid "A545BC22-4A34-78C3-2C5E-5A87980AF182";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion115";
	rename -uid "D151965A-49FB-0B03-739F-8281EC308332";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion116";
	rename -uid "1AA99538-4ACC-471A-E140-54BFF0C1EB69";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion117";
	rename -uid "F1F56FDC-4E0E-7038-8FF4-929BCB53F044";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion118";
	rename -uid "895D595C-4B9A-EA35-4594-049933AA6019";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion119";
	rename -uid "6296C59C-44B6-F319-506F-CAADF4A8932E";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion120";
	rename -uid "6E771E4E-428D-09C2-6048-27B5079A59A4";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "Mouth9Ro_L_Mdv";
	rename -uid "EB5873C2-49E2-28FB-F6C8-738CD432C516";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion121";
	rename -uid "5EF42DD8-46C7-8A67-4B2F-A896800D0296";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion122";
	rename -uid "14D0D726-4851-EC9B-D619-498E77D8333B";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion123";
	rename -uid "5BDEADA8-4773-C3F1-FF62-9F914B253E0A";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion124";
	rename -uid "55CE8B53-4F27-C682-04A0-77B5C8771BFD";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion125";
	rename -uid "9D370338-4A74-A62A-0436-4597E244731E";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion126";
	rename -uid "11B4007E-4597-417C-F462-3A8CBB609476";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "Mouth2Ro_L_Mdv";
	rename -uid "B33E9F0E-439C-DB9A-BBEF-6DB7C764D6F6";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion127";
	rename -uid "F1B94FD8-4CAB-8508-9D00-F68C4DDCDC17";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion128";
	rename -uid "6ABBA8BA-4C1A-CD3F-6062-3ABF1FA9D1DF";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion129";
	rename -uid "CDEDB41F-49DF-6145-EB9D-C6BDF524A6C1";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion130";
	rename -uid "78A2D9B4-485F-FF11-326E-AB8F4775B92D";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion131";
	rename -uid "67AF573F-44D9-576E-15D5-CDB3E4FF4585";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion132";
	rename -uid "7763E495-47ED-6CC8-95D4-2D841D7285D4";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "EyeLid12Ro_L_Mdv";
	rename -uid "CAD966A6-4207-D73C-200A-6FB1A3A3EB82";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion133";
	rename -uid "C87C7AFF-4603-5F72-A207-CA85C38F58B7";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion134";
	rename -uid "BC85872C-4FF8-3E2F-CC88-F0B9E21A99CE";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion135";
	rename -uid "42F14ABF-47FC-C677-0D4A-3EA99A7E834A";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion136";
	rename -uid "2DF40BD3-43FC-2B49-C862-38A68A43531D";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion137";
	rename -uid "46ABFA1A-4CEF-BEC1-F75D-50BE680A75CE";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion138";
	rename -uid "BEB7D3EC-49E7-8D32-28A5-6AA53052CAC0";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "Mouth3Ro_L_Mdv";
	rename -uid "B2081D52-4ADE-F5D8-8114-6F85E38425AC";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion139";
	rename -uid "83FD28E5-4622-71D5-9BFF-489A02034F13";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion140";
	rename -uid "A19547C1-4CA5-5D58-A349-F9953EA24764";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion141";
	rename -uid "505DACBD-4311-0772-C326-93B69C6399A6";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion142";
	rename -uid "C6CEE353-474E-E821-27C0-E0920D19C8E3";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion143";
	rename -uid "1616919B-4F93-0417-7AC3-22B6EE3023E4";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion144";
	rename -uid "95EDD954-403D-F44B-5C43-63AA7327D23D";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "Mouth4Ro_L_Mdv";
	rename -uid "951F0204-42FD-9E81-339F-92B854E3F75A";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion145";
	rename -uid "B1C4BDEC-4307-550B-8CAD-4CB7BA90BE1C";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion146";
	rename -uid "91D31C01-4171-27B6-2664-2CB57CE962B2";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion147";
	rename -uid "0FEE8A1B-43F0-F2CD-2D10-4CA771F34F66";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion148";
	rename -uid "C297679A-4092-2C2F-2739-319D57713A49";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion149";
	rename -uid "65A50DE4-40FA-6BFF-C864-41A7D18E9F93";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion150";
	rename -uid "0725788C-4DE5-A79F-18F7-1B8761C9B727";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "EyeLid11Ro_L_Mdv";
	rename -uid "3EC49314-4971-8AA5-1325-0BB8E0DDB149";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion151";
	rename -uid "7D2B9190-4D2E-61A1-F5FF-168F6EDFBCB8";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion152";
	rename -uid "5EF5A6ED-4A25-CE62-850F-C6BF9D1243AC";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion153";
	rename -uid "F61E981B-4AB6-7F78-A817-4AA0E746AD75";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion154";
	rename -uid "224B6A7A-4070-DC72-0772-EBA924D45883";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion155";
	rename -uid "B9B04577-4CE7-36FC-8920-84B8A523EEE2";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion156";
	rename -uid "A71B3FDD-4371-C2B2-D6B4-2FBE5D700BB1";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "EyeLid10Ro_L_Mdv";
	rename -uid "27BF9F9F-4D18-BDA4-0049-DB8F5A0D3636";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion157";
	rename -uid "AF261AA3-4892-0D87-3C3A-4E802E926BBD";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion158";
	rename -uid "58D7CC25-4AB1-96A2-AD72-17A44362F1AF";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion159";
	rename -uid "9F27DBB6-4125-9308-C9CA-4181E1758584";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion160";
	rename -uid "86687A8C-4A63-959A-9CD2-03BD308A0238";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion161";
	rename -uid "0E956423-4460-7650-43FD-17B1DCC42D7C";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion162";
	rename -uid "931871D2-488A-0702-8A88-E39961B424EE";
	setAttr ".cf" 0.017453292519943295;
createNode multiplyDivide -n "EyeLid9Ro_L_Mdv";
	rename -uid "B18089F1-4875-0CFD-7380-F6A93733A9D8";
	setAttr ".i2" -type "float3" 1 -1 -1 ;
createNode unitConversion -n "unitConversion163";
	rename -uid "D04A9B6D-435B-46E9-1341-E48314423D85";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion164";
	rename -uid "9F78EA8A-49D6-47FF-C289-47ADBC6DD0CA";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion165";
	rename -uid "5FC1FD3A-43DC-F3AF-DD62-299C27CD5ACD";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion166";
	rename -uid "E4CB88E5-4D06-181E-596B-5F9147A02794";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion167";
	rename -uid "A3EBB8D8-4C28-4FB1-221A-C2A392DEE4BB";
	setAttr ".cf" 0.017453292519943295;
createNode unitConversion -n "unitConversion168";
	rename -uid "8A673C86-4727-837C-D372-F78A4E356936";
	setAttr ".cf" 0.017453292519943295;
createNode nodeGraphEditorInfo -n "MayaNodeEditorSavedTabsInfo";
	rename -uid "C5B5E277-4C41-B045-B95F-C8B9F16520D6";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -3359.4416509552766 3834.6532583592266 ;
	setAttr ".tgi[0].vh" -type "double2" -2322.3957879685081 4362.239608158382 ;
	setAttr -s 228 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" -2320;
	setAttr ".tgi[0].ni[0].y" 367.14285278320313;
	setAttr ".tgi[0].ni[0].nvs" 18304;
	setAttr ".tgi[0].ni[1].x" -2934.28564453125;
	setAttr ".tgi[0].ni[1].y" 468.57144165039063;
	setAttr ".tgi[0].ni[1].nvs" 18304;
	setAttr ".tgi[0].ni[2].x" -2320;
	setAttr ".tgi[0].ni[2].y" 570;
	setAttr ".tgi[0].ni[2].nvs" 18304;
	setAttr ".tgi[0].ni[3].x" -2627.142822265625;
	setAttr ".tgi[0].ni[3].y" 801.4285888671875;
	setAttr ".tgi[0].ni[3].nvs" 18304;
	setAttr ".tgi[0].ni[4].x" -2934.28564453125;
	setAttr ".tgi[0].ni[4].y" 902.85711669921875;
	setAttr ".tgi[0].ni[4].nvs" 18304;
	setAttr ".tgi[0].ni[5].x" -2934.28564453125;
	setAttr ".tgi[0].ni[5].y" 801.4285888671875;
	setAttr ".tgi[0].ni[5].nvs" 18304;
	setAttr ".tgi[0].ni[6].x" -2934.28564453125;
	setAttr ".tgi[0].ni[6].y" 367.14285278320313;
	setAttr ".tgi[0].ni[6].nvs" 18304;
	setAttr ".tgi[0].ni[7].x" -2320;
	setAttr ".tgi[0].ni[7].y" 468.57144165039063;
	setAttr ".tgi[0].ni[7].nvs" 18304;
	setAttr ".tgi[0].ni[8].x" -2934.28564453125;
	setAttr ".tgi[0].ni[8].y" 1032.857177734375;
	setAttr ".tgi[0].ni[8].nvs" 18304;
	setAttr ".tgi[0].ni[9].x" -2320;
	setAttr ".tgi[0].ni[9].y" 1134.2857666015625;
	setAttr ".tgi[0].ni[9].nvs" 18304;
	setAttr ".tgi[0].ni[10].x" -2320;
	setAttr ".tgi[0].ni[10].y" 1032.857177734375;
	setAttr ".tgi[0].ni[10].nvs" 18304;
	setAttr ".tgi[0].ni[11].x" -2627.142822265625;
	setAttr ".tgi[0].ni[11].y" 1467.142822265625;
	setAttr ".tgi[0].ni[11].nvs" 18304;
	setAttr ".tgi[0].ni[12].x" -2934.28564453125;
	setAttr ".tgi[0].ni[12].y" 1467.142822265625;
	setAttr ".tgi[0].ni[12].nvs" 18304;
	setAttr ".tgi[0].ni[13].x" -2934.28564453125;
	setAttr ".tgi[0].ni[13].y" 1568.5714111328125;
	setAttr ".tgi[0].ni[13].nvs" 18304;
	setAttr ".tgi[0].ni[14].x" -2320;
	setAttr ".tgi[0].ni[14].y" 1235.7142333984375;
	setAttr ".tgi[0].ni[14].nvs" 18304;
	setAttr ".tgi[0].ni[15].x" -2934.28564453125;
	setAttr ".tgi[0].ni[15].y" 1365.7142333984375;
	setAttr ".tgi[0].ni[15].nvs" 18304;
	setAttr ".tgi[0].ni[16].x" -2320;
	setAttr ".tgi[0].ni[16].y" 1568.5714111328125;
	setAttr ".tgi[0].ni[16].nvs" 18304;
	setAttr ".tgi[0].ni[17].x" -2320;
	setAttr ".tgi[0].ni[17].y" 1467.142822265625;
	setAttr ".tgi[0].ni[17].nvs" 18304;
	setAttr ".tgi[0].ni[18].x" -2934.28564453125;
	setAttr ".tgi[0].ni[18].y" 570;
	setAttr ".tgi[0].ni[18].nvs" 18304;
	setAttr ".tgi[0].ni[19].x" -2627.142822265625;
	setAttr ".tgi[0].ni[19].y" 468.57144165039063;
	setAttr ".tgi[0].ni[19].nvs" 18304;
	setAttr ".tgi[0].ni[20].x" -2320;
	setAttr ".tgi[0].ni[20].y" 1365.7142333984375;
	setAttr ".tgi[0].ni[20].nvs" 18304;
	setAttr ".tgi[0].ni[21].x" -2320;
	setAttr ".tgi[0].ni[21].y" 1698.5714111328125;
	setAttr ".tgi[0].ni[21].nvs" 18304;
	setAttr ".tgi[0].ni[22].x" -2934.28564453125;
	setAttr ".tgi[0].ni[22].y" 2132.857177734375;
	setAttr ".tgi[0].ni[22].nvs" 18304;
	setAttr ".tgi[0].ni[23].x" -2934.28564453125;
	setAttr ".tgi[0].ni[23].y" 1901.4285888671875;
	setAttr ".tgi[0].ni[23].nvs" 18304;
	setAttr ".tgi[0].ni[24].x" -2627.142822265625;
	setAttr ".tgi[0].ni[24].y" 9122.857421875;
	setAttr ".tgi[0].ni[24].nvs" 18304;
	setAttr ".tgi[0].ni[25].x" -3241.428466796875;
	setAttr ".tgi[0].ni[25].y" 9224.2861328125;
	setAttr ".tgi[0].ni[25].nvs" 18304;
	setAttr ".tgi[0].ni[26].x" -2320;
	setAttr ".tgi[0].ni[26].y" 2465.71435546875;
	setAttr ".tgi[0].ni[26].nvs" 18304;
	setAttr ".tgi[0].ni[27].x" -2934.28564453125;
	setAttr ".tgi[0].ni[27].y" 1800;
	setAttr ".tgi[0].ni[27].nvs" 18304;
	setAttr ".tgi[0].ni[28].x" -2320;
	setAttr ".tgi[0].ni[28].y" 2234.28564453125;
	setAttr ".tgi[0].ni[28].nvs" 18304;
	setAttr ".tgi[0].ni[29].x" -2320;
	setAttr ".tgi[0].ni[29].y" 2900;
	setAttr ".tgi[0].ni[29].nvs" 18304;
	setAttr ".tgi[0].ni[30].x" -2320;
	setAttr ".tgi[0].ni[30].y" 1800;
	setAttr ".tgi[0].ni[30].nvs" 18304;
	setAttr ".tgi[0].ni[31].x" -2627.142822265625;
	setAttr ".tgi[0].ni[31].y" 2132.857177734375;
	setAttr ".tgi[0].ni[31].nvs" 18304;
	setAttr ".tgi[0].ni[32].x" -2934.28564453125;
	setAttr ".tgi[0].ni[32].y" 9198.5712890625;
	setAttr ".tgi[0].ni[32].nvs" 18304;
	setAttr ".tgi[0].ni[33].x" -2320;
	setAttr ".tgi[0].ni[33].y" 1901.4285888671875;
	setAttr ".tgi[0].ni[33].nvs" 18304;
	setAttr ".tgi[0].ni[34].x" -2627.142822265625;
	setAttr ".tgi[0].ni[34].y" 1800;
	setAttr ".tgi[0].ni[34].nvs" 18304;
	setAttr ".tgi[0].ni[35].x" -2934.28564453125;
	setAttr ".tgi[0].ni[35].y" 1698.5714111328125;
	setAttr ".tgi[0].ni[35].nvs" 18304;
	setAttr ".tgi[0].ni[36].x" -2934.28564453125;
	setAttr ".tgi[0].ni[36].y" 2234.28564453125;
	setAttr ".tgi[0].ni[36].nvs" 18304;
	setAttr ".tgi[0].ni[37].x" -2934.28564453125;
	setAttr ".tgi[0].ni[37].y" 2031.4285888671875;
	setAttr ".tgi[0].ni[37].nvs" 18304;
	setAttr ".tgi[0].ni[38].x" -2320;
	setAttr ".tgi[0].ni[38].y" 2031.4285888671875;
	setAttr ".tgi[0].ni[38].nvs" 18304;
	setAttr ".tgi[0].ni[39].x" -2320;
	setAttr ".tgi[0].ni[39].y" 2132.857177734375;
	setAttr ".tgi[0].ni[39].nvs" 18304;
	setAttr ".tgi[0].ni[40].x" -3241.428466796875;
	setAttr ".tgi[0].ni[40].y" 9325.7138671875;
	setAttr ".tgi[0].ni[40].nvs" 18304;
	setAttr ".tgi[0].ni[41].x" -3241.428466796875;
	setAttr ".tgi[0].ni[41].y" 9122.857421875;
	setAttr ".tgi[0].ni[41].nvs" 18304;
	setAttr ".tgi[0].ni[42].x" -2627.142822265625;
	setAttr ".tgi[0].ni[42].y" 9325.7138671875;
	setAttr ".tgi[0].ni[42].nvs" 18304;
	setAttr ".tgi[0].ni[43].x" -2627.142822265625;
	setAttr ".tgi[0].ni[43].y" 9224.2861328125;
	setAttr ".tgi[0].ni[43].nvs" 18304;
	setAttr ".tgi[0].ni[44].x" -2627.142822265625;
	setAttr ".tgi[0].ni[44].y" 2465.71435546875;
	setAttr ".tgi[0].ni[44].nvs" 18304;
	setAttr ".tgi[0].ni[45].x" -2934.28564453125;
	setAttr ".tgi[0].ni[45].y" 2567.142822265625;
	setAttr ".tgi[0].ni[45].nvs" 18304;
	setAttr ".tgi[0].ni[46].x" -2934.28564453125;
	setAttr ".tgi[0].ni[46].y" 2465.71435546875;
	setAttr ".tgi[0].ni[46].nvs" 18304;
	setAttr ".tgi[0].ni[47].x" -2934.28564453125;
	setAttr ".tgi[0].ni[47].y" 2364.28564453125;
	setAttr ".tgi[0].ni[47].nvs" 18304;
	setAttr ".tgi[0].ni[48].x" -2934.28564453125;
	setAttr ".tgi[0].ni[48].y" 6244.28564453125;
	setAttr ".tgi[0].ni[48].nvs" 18304;
	setAttr ".tgi[0].ni[49].x" -2320;
	setAttr ".tgi[0].ni[49].y" 3131.428466796875;
	setAttr ".tgi[0].ni[49].nvs" 18304;
	setAttr ".tgi[0].ni[50].x" -2320;
	setAttr ".tgi[0].ni[50].y" 6142.85693359375;
	setAttr ".tgi[0].ni[50].nvs" 18304;
	setAttr ".tgi[0].ni[51].x" -2320;
	setAttr ".tgi[0].ni[51].y" 6244.28564453125;
	setAttr ".tgi[0].ni[51].nvs" 18304;
	setAttr ".tgi[0].ni[52].x" -2627.142822265625;
	setAttr ".tgi[0].ni[52].y" 2798.571533203125;
	setAttr ".tgi[0].ni[52].nvs" 18304;
	setAttr ".tgi[0].ni[53].x" -2934.28564453125;
	setAttr ".tgi[0].ni[53].y" 2697.142822265625;
	setAttr ".tgi[0].ni[53].nvs" 18304;
	setAttr ".tgi[0].ni[54].x" -2320;
	setAttr ".tgi[0].ni[54].y" 2697.142822265625;
	setAttr ".tgi[0].ni[54].nvs" 18304;
	setAttr ".tgi[0].ni[55].x" -2320;
	setAttr ".tgi[0].ni[55].y" 2798.571533203125;
	setAttr ".tgi[0].ni[55].nvs" 18304;
	setAttr ".tgi[0].ni[56].x" -2934.28564453125;
	setAttr ".tgi[0].ni[56].y" 6345.71435546875;
	setAttr ".tgi[0].ni[56].nvs" 18304;
	setAttr ".tgi[0].ni[57].x" -2934.28564453125;
	setAttr ".tgi[0].ni[57].y" 3131.428466796875;
	setAttr ".tgi[0].ni[57].nvs" 18304;
	setAttr ".tgi[0].ni[58].x" -2320;
	setAttr ".tgi[0].ni[58].y" 3232.857177734375;
	setAttr ".tgi[0].ni[58].nvs" 18304;
	setAttr ".tgi[0].ni[59].x" -2320;
	setAttr ".tgi[0].ni[59].y" 6345.71435546875;
	setAttr ".tgi[0].ni[59].nvs" 18304;
	setAttr ".tgi[0].ni[60].x" -2934.28564453125;
	setAttr ".tgi[0].ni[60].y" 2900;
	setAttr ".tgi[0].ni[60].nvs" 18304;
	setAttr ".tgi[0].ni[61].x" -2934.28564453125;
	setAttr ".tgi[0].ni[61].y" 2798.571533203125;
	setAttr ".tgi[0].ni[61].nvs" 18304;
	setAttr ".tgi[0].ni[62].x" -2934.28564453125;
	setAttr ".tgi[0].ni[62].y" 3030;
	setAttr ".tgi[0].ni[62].nvs" 18304;
	setAttr ".tgi[0].ni[63].x" -2320;
	setAttr ".tgi[0].ni[63].y" 3030;
	setAttr ".tgi[0].ni[63].nvs" 18304;
	setAttr ".tgi[0].ni[64].x" -2934.28564453125;
	setAttr ".tgi[0].ni[64].y" 3232.857177734375;
	setAttr ".tgi[0].ni[64].nvs" 18304;
	setAttr ".tgi[0].ni[65].x" -2627.142822265625;
	setAttr ".tgi[0].ni[65].y" 6244.28564453125;
	setAttr ".tgi[0].ni[65].nvs" 18304;
	setAttr ".tgi[0].ni[66].x" -2934.28564453125;
	setAttr ".tgi[0].ni[66].y" 6142.85693359375;
	setAttr ".tgi[0].ni[66].nvs" 18304;
	setAttr ".tgi[0].ni[67].x" -2627.142822265625;
	setAttr ".tgi[0].ni[67].y" 3464.28564453125;
	setAttr ".tgi[0].ni[67].nvs" 18304;
	setAttr ".tgi[0].ni[68].x" -2320;
	setAttr ".tgi[0].ni[68].y" 3797.142822265625;
	setAttr ".tgi[0].ni[68].nvs" 18304;
	setAttr ".tgi[0].ni[69].x" -2320;
	setAttr ".tgi[0].ni[69].y" 3695.71435546875;
	setAttr ".tgi[0].ni[69].nvs" 18304;
	setAttr ".tgi[0].ni[70].x" -2934.28564453125;
	setAttr ".tgi[0].ni[70].y" 3565.71435546875;
	setAttr ".tgi[0].ni[70].nvs" 18304;
	setAttr ".tgi[0].ni[71].x" -2934.28564453125;
	setAttr ".tgi[0].ni[71].y" 3362.857177734375;
	setAttr ".tgi[0].ni[71].nvs" 18304;
	setAttr ".tgi[0].ni[72].x" -2320;
	setAttr ".tgi[0].ni[72].y" 3464.28564453125;
	setAttr ".tgi[0].ni[72].nvs" 18304;
	setAttr ".tgi[0].ni[73].x" -2320;
	setAttr ".tgi[0].ni[73].y" 3565.71435546875;
	setAttr ".tgi[0].ni[73].nvs" 18304;
	setAttr ".tgi[0].ni[74].x" -2627.142822265625;
	setAttr ".tgi[0].ni[74].y" 4130;
	setAttr ".tgi[0].ni[74].nvs" 18304;
	setAttr ".tgi[0].ni[75].x" -2934.28564453125;
	setAttr ".tgi[0].ni[75].y" 4231.4287109375;
	setAttr ".tgi[0].ni[75].nvs" 18304;
	setAttr ".tgi[0].ni[76].x" -2934.28564453125;
	setAttr ".tgi[0].ni[76].y" 4028.571533203125;
	setAttr ".tgi[0].ni[76].nvs" 18304;
	setAttr ".tgi[0].ni[77].x" -2320;
	setAttr ".tgi[0].ni[77].y" 4231.4287109375;
	setAttr ".tgi[0].ni[77].nvs" 18304;
	setAttr ".tgi[0].ni[78].x" -2320;
	setAttr ".tgi[0].ni[78].y" 4130;
	setAttr ".tgi[0].ni[78].nvs" 18304;
	setAttr ".tgi[0].ni[79].x" -2320;
	setAttr ".tgi[0].ni[79].y" 4028.571533203125;
	setAttr ".tgi[0].ni[79].nvs" 18304;
	setAttr ".tgi[0].ni[80].x" -2320;
	setAttr ".tgi[0].ni[80].y" 3362.857177734375;
	setAttr ".tgi[0].ni[80].nvs" 18304;
	setAttr ".tgi[0].ni[81].x" -2627.142822265625;
	setAttr ".tgi[0].ni[81].y" 3131.428466796875;
	setAttr ".tgi[0].ni[81].nvs" 18304;
	setAttr ".tgi[0].ni[82].x" -2934.28564453125;
	setAttr ".tgi[0].ni[82].y" 3464.28564453125;
	setAttr ".tgi[0].ni[82].nvs" 18304;
	setAttr ".tgi[0].ni[83].x" -2934.28564453125;
	setAttr ".tgi[0].ni[83].y" 4130;
	setAttr ".tgi[0].ni[83].nvs" 18304;
	setAttr ".tgi[0].ni[84].x" -2627.142822265625;
	setAttr ".tgi[0].ni[84].y" 4225.71435546875;
	setAttr ".tgi[0].ni[84].nvs" 18304;
	setAttr ".tgi[0].ni[85].x" -2627.142822265625;
	setAttr ".tgi[0].ni[85].y" 3185.71435546875;
	setAttr ".tgi[0].ni[85].nvs" 18304;
	setAttr ".tgi[0].ni[86].x" -2627.142822265625;
	setAttr ".tgi[0].ni[86].y" 3445.71435546875;
	setAttr ".tgi[0].ni[86].nvs" 18304;
	setAttr ".tgi[0].ni[87].x" -2320;
	setAttr ".tgi[0].ni[87].y" -1062.857177734375;
	setAttr ".tgi[0].ni[87].nvs" 18304;
	setAttr ".tgi[0].ni[88].x" -2320;
	setAttr ".tgi[0].ni[88].y" -932.85711669921875;
	setAttr ".tgi[0].ni[88].nvs" 18304;
	setAttr ".tgi[0].ni[89].x" -2627.142822265625;
	setAttr ".tgi[0].ni[89].y" 5525.71435546875;
	setAttr ".tgi[0].ni[89].nvs" 18304;
	setAttr ".tgi[0].ni[90].x" -2627.142822265625;
	setAttr ".tgi[0].ni[90].y" 3797.142822265625;
	setAttr ".tgi[0].ni[90].nvs" 18304;
	setAttr ".tgi[0].ni[91].x" -2320;
	setAttr ".tgi[0].ni[91].y" -802.85711669921875;
	setAttr ".tgi[0].ni[91].nvs" 18304;
	setAttr ".tgi[0].ni[92].x" -2934.28564453125;
	setAttr ".tgi[0].ni[92].y" 3695.71435546875;
	setAttr ".tgi[0].ni[92].nvs" 18304;
	setAttr ".tgi[0].ni[93].x" -2320;
	setAttr ".tgi[0].ni[93].y" -672.85711669921875;
	setAttr ".tgi[0].ni[93].nvs" 18304;
	setAttr ".tgi[0].ni[94].x" -2627.142822265625;
	setAttr ".tgi[0].ni[94].y" 4095.71435546875;
	setAttr ".tgi[0].ni[94].nvs" 18304;
	setAttr ".tgi[0].ni[95].x" -2320;
	setAttr ".tgi[0].ni[95].y" -542.85711669921875;
	setAttr ".tgi[0].ni[95].nvs" 18304;
	setAttr ".tgi[0].ni[96].x" -2320;
	setAttr ".tgi[0].ni[96].y" -412.85714721679688;
	setAttr ".tgi[0].ni[96].nvs" 18304;
	setAttr ".tgi[0].ni[97].x" -2627.142822265625;
	setAttr ".tgi[0].ni[97].y" 5135.71435546875;
	setAttr ".tgi[0].ni[97].nvs" 18304;
	setAttr ".tgi[0].ni[98].x" -2627.142822265625;
	setAttr ".tgi[0].ni[98].y" 3965.71435546875;
	setAttr ".tgi[0].ni[98].nvs" 18304;
	setAttr ".tgi[0].ni[99].x" -2627.142822265625;
	setAttr ".tgi[0].ni[99].y" 4875.71435546875;
	setAttr ".tgi[0].ni[99].nvs" 18304;
	setAttr ".tgi[0].ni[100].x" -2627.142822265625;
	setAttr ".tgi[0].ni[100].y" 4355.71435546875;
	setAttr ".tgi[0].ni[100].nvs" 18304;
	setAttr ".tgi[0].ni[101].x" -2627.142822265625;
	setAttr ".tgi[0].ni[101].y" 2925.71435546875;
	setAttr ".tgi[0].ni[101].nvs" 18304;
	setAttr ".tgi[0].ni[102].x" -2627.142822265625;
	setAttr ".tgi[0].ni[102].y" 4485.71435546875;
	setAttr ".tgi[0].ni[102].nvs" 18304;
	setAttr ".tgi[0].ni[103].x" -2627.142822265625;
	setAttr ".tgi[0].ni[103].y" 9021.4287109375;
	setAttr ".tgi[0].ni[103].nvs" 18304;
	setAttr ".tgi[0].ni[104].x" -2627.142822265625;
	setAttr ".tgi[0].ni[104].y" 3055.71435546875;
	setAttr ".tgi[0].ni[104].nvs" 18304;
	setAttr ".tgi[0].ni[105].x" -2320;
	setAttr ".tgi[0].ni[105].y" -282.85714721679688;
	setAttr ".tgi[0].ni[105].nvs" 18304;
	setAttr ".tgi[0].ni[106].x" -2320;
	setAttr ".tgi[0].ni[106].y" -152.85714721679688;
	setAttr ".tgi[0].ni[106].nvs" 18304;
	setAttr ".tgi[0].ni[107].x" -2320;
	setAttr ".tgi[0].ni[107].y" -22.857143402099609;
	setAttr ".tgi[0].ni[107].nvs" 18304;
	setAttr ".tgi[0].ni[108].x" -2627.142822265625;
	setAttr ".tgi[0].ni[108].y" 2665.71435546875;
	setAttr ".tgi[0].ni[108].nvs" 18304;
	setAttr ".tgi[0].ni[109].x" -2627.142822265625;
	setAttr ".tgi[0].ni[109].y" 2275.71435546875;
	setAttr ".tgi[0].ni[109].nvs" 18304;
	setAttr ".tgi[0].ni[110].x" -2320;
	setAttr ".tgi[0].ni[110].y" 107.14286041259766;
	setAttr ".tgi[0].ni[110].nvs" 18304;
	setAttr ".tgi[0].ni[111].x" -2627.142822265625;
	setAttr ".tgi[0].ni[111].y" 5265.71435546875;
	setAttr ".tgi[0].ni[111].nvs" 18304;
	setAttr ".tgi[0].ni[112].x" -2320;
	setAttr ".tgi[0].ni[112].y" 237.14285278320313;
	setAttr ".tgi[0].ni[112].nvs" 18304;
	setAttr ".tgi[0].ni[113].x" -2627.142822265625;
	setAttr ".tgi[0].ni[113].y" 5655.71435546875;
	setAttr ".tgi[0].ni[113].nvs" 18304;
	setAttr ".tgi[0].ni[114].x" -2934.28564453125;
	setAttr ".tgi[0].ni[114].y" 3898.571533203125;
	setAttr ".tgi[0].ni[114].nvs" 18304;
	setAttr ".tgi[0].ni[115].x" -2934.28564453125;
	setAttr ".tgi[0].ni[115].y" 3797.142822265625;
	setAttr ".tgi[0].ni[115].nvs" 18304;
	setAttr ".tgi[0].ni[116].x" -2320;
	setAttr ".tgi[0].ni[116].y" 3898.571533203125;
	setAttr ".tgi[0].ni[116].nvs" 18304;
	setAttr ".tgi[0].ni[117].x" -2627.142822265625;
	setAttr ".tgi[0].ni[117].y" 1134.2857666015625;
	setAttr ".tgi[0].ni[117].nvs" 18304;
	setAttr ".tgi[0].ni[118].x" -2934.28564453125;
	setAttr ".tgi[0].ni[118].y" 1235.7142333984375;
	setAttr ".tgi[0].ni[118].nvs" 18304;
	setAttr ".tgi[0].ni[119].x" -2934.28564453125;
	setAttr ".tgi[0].ni[119].y" 1134.2857666015625;
	setAttr ".tgi[0].ni[119].nvs" 18304;
	setAttr ".tgi[0].ni[120].x" -2934.28564453125;
	setAttr ".tgi[0].ni[120].y" 4564.28564453125;
	setAttr ".tgi[0].ni[120].nvs" 18304;
	setAttr ".tgi[0].ni[121].x" -2934.28564453125;
	setAttr ".tgi[0].ni[121].y" 4361.4287109375;
	setAttr ".tgi[0].ni[121].nvs" 18304;
	setAttr ".tgi[0].ni[122].x" -2320;
	setAttr ".tgi[0].ni[122].y" 4361.4287109375;
	setAttr ".tgi[0].ni[122].nvs" 18304;
	setAttr ".tgi[0].ni[123].x" -2627.142822265625;
	setAttr ".tgi[0].ni[123].y" 4462.85693359375;
	setAttr ".tgi[0].ni[123].nvs" 18304;
	setAttr ".tgi[0].ni[124].x" -2934.28564453125;
	setAttr ".tgi[0].ni[124].y" 4462.85693359375;
	setAttr ".tgi[0].ni[124].nvs" 18304;
	setAttr ".tgi[0].ni[125].x" -2320;
	setAttr ".tgi[0].ni[125].y" 4564.28564453125;
	setAttr ".tgi[0].ni[125].nvs" 18304;
	setAttr ".tgi[0].ni[126].x" -2934.28564453125;
	setAttr ".tgi[0].ni[126].y" 4897.14306640625;
	setAttr ".tgi[0].ni[126].nvs" 18304;
	setAttr ".tgi[0].ni[127].x" -2934.28564453125;
	setAttr ".tgi[0].ni[127].y" 4795.71435546875;
	setAttr ".tgi[0].ni[127].nvs" 18304;
	setAttr ".tgi[0].ni[128].x" -2934.28564453125;
	setAttr ".tgi[0].ni[128].y" 4694.28564453125;
	setAttr ".tgi[0].ni[128].nvs" 18304;
	setAttr ".tgi[0].ni[129].x" -2320;
	setAttr ".tgi[0].ni[129].y" 4694.28564453125;
	setAttr ".tgi[0].ni[129].nvs" 18304;
	setAttr ".tgi[0].ni[130].x" -2320;
	setAttr ".tgi[0].ni[130].y" 4462.85693359375;
	setAttr ".tgi[0].ni[130].nvs" 18304;
	setAttr ".tgi[0].ni[131].x" -2320;
	setAttr ".tgi[0].ni[131].y" 2364.28564453125;
	setAttr ".tgi[0].ni[131].nvs" 18304;
	setAttr ".tgi[0].ni[132].x" -2627.142822265625;
	setAttr ".tgi[0].ni[132].y" 4795.71435546875;
	setAttr ".tgi[0].ni[132].nvs" 18304;
	setAttr ".tgi[0].ni[133].x" -2320;
	setAttr ".tgi[0].ni[133].y" 4897.14306640625;
	setAttr ".tgi[0].ni[133].nvs" 18304;
	setAttr ".tgi[0].ni[134].x" -2320;
	setAttr ".tgi[0].ni[134].y" 4795.71435546875;
	setAttr ".tgi[0].ni[134].nvs" 18304;
	setAttr ".tgi[0].ni[135].x" -2320;
	setAttr ".tgi[0].ni[135].y" 2567.142822265625;
	setAttr ".tgi[0].ni[135].nvs" 18304;
	setAttr ".tgi[0].ni[136].x" -3548.571533203125;
	setAttr ".tgi[0].ni[136].y" 9172.857421875;
	setAttr ".tgi[0].ni[136].nvs" 18304;
	setAttr ".tgi[0].ni[137].x" -2631.428466796875;
	setAttr ".tgi[0].ni[137].y" 4077.142822265625;
	setAttr ".tgi[0].ni[137].nvs" 18304;
	setAttr ".tgi[0].ni[138].x" -2938.571533203125;
	setAttr ".tgi[0].ni[138].y" 4077.142822265625;
	setAttr ".tgi[0].ni[138].nvs" 18304;
	setAttr ".tgi[0].ni[139].x" -2320;
	setAttr ".tgi[0].ni[139].y" 9172.857421875;
	setAttr ".tgi[0].ni[139].nvs" 18304;
	setAttr ".tgi[0].ni[140].x" -2320;
	setAttr ".tgi[0].ni[140].y" 5128.5712890625;
	setAttr ".tgi[0].ni[140].nvs" 18304;
	setAttr ".tgi[0].ni[141].x" -2320;
	setAttr ".tgi[0].ni[141].y" 5360;
	setAttr ".tgi[0].ni[141].nvs" 18304;
	setAttr ".tgi[0].ni[142].x" -2627.142822265625;
	setAttr ".tgi[0].ni[142].y" 5794.28564453125;
	setAttr ".tgi[0].ni[142].nvs" 18304;
	setAttr ".tgi[0].ni[143].x" -2320;
	setAttr ".tgi[0].ni[143].y" 6025.71435546875;
	setAttr ".tgi[0].ni[143].nvs" 18304;
	setAttr ".tgi[0].ni[144].x" -2934.28564453125;
	setAttr ".tgi[0].ni[144].y" 6358.5712890625;
	setAttr ".tgi[0].ni[144].nvs" 18304;
	setAttr ".tgi[0].ni[145].x" -2320;
	setAttr ".tgi[0].ni[145].y" 6561.4287109375;
	setAttr ".tgi[0].ni[145].nvs" 18304;
	setAttr ".tgi[0].ni[146].x" -2934.28564453125;
	setAttr ".tgi[0].ni[146].y" 6792.85693359375;
	setAttr ".tgi[0].ni[146].nvs" 18304;
	setAttr ".tgi[0].ni[147].x" -2934.28564453125;
	setAttr ".tgi[0].ni[147].y" 7024.28564453125;
	setAttr ".tgi[0].ni[147].nvs" 18304;
	setAttr ".tgi[0].ni[148].x" -2627.142822265625;
	setAttr ".tgi[0].ni[148].y" 7458.5712890625;
	setAttr ".tgi[0].ni[148].nvs" 18304;
	setAttr ".tgi[0].ni[149].x" -2627.142822265625;
	setAttr ".tgi[0].ni[149].y" 7125.71435546875;
	setAttr ".tgi[0].ni[149].nvs" 18304;
	setAttr ".tgi[0].ni[150].x" -2934.28564453125;
	setAttr ".tgi[0].ni[150].y" 6228.5712890625;
	setAttr ".tgi[0].ni[150].nvs" 18304;
	setAttr ".tgi[0].ni[151].x" -2320;
	setAttr ".tgi[0].ni[151].y" 5461.4287109375;
	setAttr ".tgi[0].ni[151].nvs" 18304;
	setAttr ".tgi[0].ni[152].x" -2934.28564453125;
	setAttr ".tgi[0].ni[152].y" 6561.4287109375;
	setAttr ".tgi[0].ni[152].nvs" 18304;
	setAttr ".tgi[0].ni[153].x" -2934.28564453125;
	setAttr ".tgi[0].ni[153].y" 6460;
	setAttr ".tgi[0].ni[153].nvs" 18304;
	setAttr ".tgi[0].ni[154].x" -2934.28564453125;
	setAttr ".tgi[0].ni[154].y" 700;
	setAttr ".tgi[0].ni[154].nvs" 18304;
	setAttr ".tgi[0].ni[155].x" -2320;
	setAttr ".tgi[0].ni[155].y" 902.85711669921875;
	setAttr ".tgi[0].ni[155].nvs" 18304;
	setAttr ".tgi[0].ni[156].x" -2934.28564453125;
	setAttr ".tgi[0].ni[156].y" 5128.5712890625;
	setAttr ".tgi[0].ni[156].nvs" 18304;
	setAttr ".tgi[0].ni[157].x" -2320;
	setAttr ".tgi[0].ni[157].y" 7227.14306640625;
	setAttr ".tgi[0].ni[157].nvs" 18304;
	setAttr ".tgi[0].ni[158].x" -2320;
	setAttr ".tgi[0].ni[158].y" 5230;
	setAttr ".tgi[0].ni[158].nvs" 18304;
	setAttr ".tgi[0].ni[159].x" -2934.28564453125;
	setAttr ".tgi[0].ni[159].y" 7458.5712890625;
	setAttr ".tgi[0].ni[159].nvs" 18304;
	setAttr ".tgi[0].ni[160].x" -2627.142822265625;
	setAttr ".tgi[0].ni[160].y" 6127.14306640625;
	setAttr ".tgi[0].ni[160].nvs" 18304;
	setAttr ".tgi[0].ni[161].x" -2320;
	setAttr ".tgi[0].ni[161].y" 7125.71435546875;
	setAttr ".tgi[0].ni[161].nvs" 18304;
	setAttr ".tgi[0].ni[162].x" -2320;
	setAttr ".tgi[0].ni[162].y" 7458.5712890625;
	setAttr ".tgi[0].ni[162].nvs" 18304;
	setAttr ".tgi[0].ni[163].x" -2627.142822265625;
	setAttr ".tgi[0].ni[163].y" 5128.5712890625;
	setAttr ".tgi[0].ni[163].nvs" 18304;
	setAttr ".tgi[0].ni[164].x" -2934.28564453125;
	setAttr ".tgi[0].ni[164].y" 5562.85693359375;
	setAttr ".tgi[0].ni[164].nvs" 18304;
	setAttr ".tgi[0].ni[165].x" -2320;
	setAttr ".tgi[0].ni[165].y" 801.4285888671875;
	setAttr ".tgi[0].ni[165].nvs" 18304;
	setAttr ".tgi[0].ni[166].x" -2934.28564453125;
	setAttr ".tgi[0].ni[166].y" 7357.14306640625;
	setAttr ".tgi[0].ni[166].nvs" 18304;
	setAttr ".tgi[0].ni[167].x" -2320;
	setAttr ".tgi[0].ni[167].y" 7357.14306640625;
	setAttr ".tgi[0].ni[167].nvs" 18304;
	setAttr ".tgi[0].ni[168].x" -2934.28564453125;
	setAttr ".tgi[0].ni[168].y" 5461.4287109375;
	setAttr ".tgi[0].ni[168].nvs" 18304;
	setAttr ".tgi[0].ni[169].x" -2934.28564453125;
	setAttr ".tgi[0].ni[169].y" 5360;
	setAttr ".tgi[0].ni[169].nvs" 18304;
	setAttr ".tgi[0].ni[170].x" -2320;
	setAttr ".tgi[0].ni[170].y" 5895.71435546875;
	setAttr ".tgi[0].ni[170].nvs" 18304;
	setAttr ".tgi[0].ni[171].x" -2320;
	setAttr ".tgi[0].ni[171].y" 5794.28564453125;
	setAttr ".tgi[0].ni[171].nvs" 18304;
	setAttr ".tgi[0].ni[172].x" -2320;
	setAttr ".tgi[0].ni[172].y" 5562.85693359375;
	setAttr ".tgi[0].ni[172].nvs" 18304;
	setAttr ".tgi[0].ni[173].x" -2934.28564453125;
	setAttr ".tgi[0].ni[173].y" 5230;
	setAttr ".tgi[0].ni[173].nvs" 18304;
	setAttr ".tgi[0].ni[174].x" -2934.28564453125;
	setAttr ".tgi[0].ni[174].y" 6025.71435546875;
	setAttr ".tgi[0].ni[174].nvs" 18304;
	setAttr ".tgi[0].ni[175].x" -2627.142822265625;
	setAttr ".tgi[0].ni[175].y" 6792.85693359375;
	setAttr ".tgi[0].ni[175].nvs" 18304;
	setAttr ".tgi[0].ni[176].x" -2934.28564453125;
	setAttr ".tgi[0].ni[176].y" 7125.71435546875;
	setAttr ".tgi[0].ni[176].nvs" 18304;
	setAttr ".tgi[0].ni[177].x" -2320;
	setAttr ".tgi[0].ni[177].y" 700;
	setAttr ".tgi[0].ni[177].nvs" 18304;
	setAttr ".tgi[0].ni[178].x" -2934.28564453125;
	setAttr ".tgi[0].ni[178].y" 6894.28564453125;
	setAttr ".tgi[0].ni[178].nvs" 18304;
	setAttr ".tgi[0].ni[179].x" -2627.142822265625;
	setAttr ".tgi[0].ni[179].y" 6460;
	setAttr ".tgi[0].ni[179].nvs" 18304;
	setAttr ".tgi[0].ni[180].x" -2934.28564453125;
	setAttr ".tgi[0].ni[180].y" 7560;
	setAttr ".tgi[0].ni[180].nvs" 18304;
	setAttr ".tgi[0].ni[181].x" -2320;
	setAttr ".tgi[0].ni[181].y" 7024.28564453125;
	setAttr ".tgi[0].ni[181].nvs" 18304;
	setAttr ".tgi[0].ni[182].x" -2320;
	setAttr ".tgi[0].ni[182].y" 7560;
	setAttr ".tgi[0].ni[182].nvs" 18304;
	setAttr ".tgi[0].ni[183].x" -2934.28564453125;
	setAttr ".tgi[0].ni[183].y" 5027.14306640625;
	setAttr ".tgi[0].ni[183].nvs" 18304;
	setAttr ".tgi[0].ni[184].x" -2934.28564453125;
	setAttr ".tgi[0].ni[184].y" 6127.14306640625;
	setAttr ".tgi[0].ni[184].nvs" 18304;
	setAttr ".tgi[0].ni[185].x" -2320;
	setAttr ".tgi[0].ni[185].y" 6228.5712890625;
	setAttr ".tgi[0].ni[185].nvs" 18304;
	setAttr ".tgi[0].ni[186].x" -2320;
	setAttr ".tgi[0].ni[186].y" 5027.14306640625;
	setAttr ".tgi[0].ni[186].nvs" 18304;
	setAttr ".tgi[0].ni[187].x" -2320;
	setAttr ".tgi[0].ni[187].y" 6127.14306640625;
	setAttr ".tgi[0].ni[187].nvs" 18304;
	setAttr ".tgi[0].ni[188].x" -2934.28564453125;
	setAttr ".tgi[0].ni[188].y" 5895.71435546875;
	setAttr ".tgi[0].ni[188].nvs" 18304;
	setAttr ".tgi[0].ni[189].x" -2934.28564453125;
	setAttr ".tgi[0].ni[189].y" 7227.14306640625;
	setAttr ".tgi[0].ni[189].nvs" 18304;
	setAttr ".tgi[0].ni[190].x" -2627.142822265625;
	setAttr ".tgi[0].ni[190].y" 5461.4287109375;
	setAttr ".tgi[0].ni[190].nvs" 18304;
	setAttr ".tgi[0].ni[191].x" -2934.28564453125;
	setAttr ".tgi[0].ni[191].y" 5794.28564453125;
	setAttr ".tgi[0].ni[191].nvs" 18304;
	setAttr ".tgi[0].ni[192].x" -2934.28564453125;
	setAttr ".tgi[0].ni[192].y" 5692.85693359375;
	setAttr ".tgi[0].ni[192].nvs" 18304;
	setAttr ".tgi[0].ni[193].x" -2320;
	setAttr ".tgi[0].ni[193].y" 5692.85693359375;
	setAttr ".tgi[0].ni[193].nvs" 18304;
	setAttr ".tgi[0].ni[194].x" -2320;
	setAttr ".tgi[0].ni[194].y" 6460;
	setAttr ".tgi[0].ni[194].nvs" 18304;
	setAttr ".tgi[0].ni[195].x" -2320;
	setAttr ".tgi[0].ni[195].y" 6358.5712890625;
	setAttr ".tgi[0].ni[195].nvs" 18304;
	setAttr ".tgi[0].ni[196].x" -2320;
	setAttr ".tgi[0].ni[196].y" 6691.4287109375;
	setAttr ".tgi[0].ni[196].nvs" 18304;
	setAttr ".tgi[0].ni[197].x" -2320;
	setAttr ".tgi[0].ni[197].y" 6894.28564453125;
	setAttr ".tgi[0].ni[197].nvs" 18304;
	setAttr ".tgi[0].ni[198].x" -2320;
	setAttr ".tgi[0].ni[198].y" 6792.85693359375;
	setAttr ".tgi[0].ni[198].nvs" 18304;
	setAttr ".tgi[0].ni[199].x" -2627.142822265625;
	setAttr ".tgi[0].ni[199].y" 7791.4287109375;
	setAttr ".tgi[0].ni[199].nvs" 18304;
	setAttr ".tgi[0].ni[200].x" -2934.28564453125;
	setAttr ".tgi[0].ni[200].y" 7892.85693359375;
	setAttr ".tgi[0].ni[200].nvs" 18304;
	setAttr ".tgi[0].ni[201].x" -2934.28564453125;
	setAttr ".tgi[0].ni[201].y" 7791.4287109375;
	setAttr ".tgi[0].ni[201].nvs" 18304;
	setAttr ".tgi[0].ni[202].x" -2934.28564453125;
	setAttr ".tgi[0].ni[202].y" 7690;
	setAttr ".tgi[0].ni[202].nvs" 18304;
	setAttr ".tgi[0].ni[203].x" -2934.28564453125;
	setAttr ".tgi[0].ni[203].y" 6691.4287109375;
	setAttr ".tgi[0].ni[203].nvs" 18304;
	setAttr ".tgi[0].ni[204].x" -2320;
	setAttr ".tgi[0].ni[204].y" 7690;
	setAttr ".tgi[0].ni[204].nvs" 18304;
	setAttr ".tgi[0].ni[205].x" -2627.142822265625;
	setAttr ".tgi[0].ni[205].y" 8124.28564453125;
	setAttr ".tgi[0].ni[205].nvs" 18304;
	setAttr ".tgi[0].ni[206].x" -2934.28564453125;
	setAttr ".tgi[0].ni[206].y" 8124.28564453125;
	setAttr ".tgi[0].ni[206].nvs" 18304;
	setAttr ".tgi[0].ni[207].x" -2320;
	setAttr ".tgi[0].ni[207].y" 8225.7138671875;
	setAttr ".tgi[0].ni[207].nvs" 18304;
	setAttr ".tgi[0].ni[208].x" -2627.142822265625;
	setAttr ".tgi[0].ni[208].y" 8457.142578125;
	setAttr ".tgi[0].ni[208].nvs" 18304;
	setAttr ".tgi[0].ni[209].x" -2320;
	setAttr ".tgi[0].ni[209].y" 8457.142578125;
	setAttr ".tgi[0].ni[209].nvs" 18304;
	setAttr ".tgi[0].ni[210].x" -2320;
	setAttr ".tgi[0].ni[210].y" 8124.28564453125;
	setAttr ".tgi[0].ni[210].nvs" 18304;
	setAttr ".tgi[0].ni[211].x" -2934.28564453125;
	setAttr ".tgi[0].ni[211].y" 8891.4287109375;
	setAttr ".tgi[0].ni[211].nvs" 18304;
	setAttr ".tgi[0].ni[212].x" -2320;
	setAttr ".tgi[0].ni[212].y" 8355.7138671875;
	setAttr ".tgi[0].ni[212].nvs" 18304;
	setAttr ".tgi[0].ni[213].x" -2320;
	setAttr ".tgi[0].ni[213].y" 8891.4287109375;
	setAttr ".tgi[0].ni[213].nvs" 18304;
	setAttr ".tgi[0].ni[214].x" -2320;
	setAttr ".tgi[0].ni[214].y" 8558.5712890625;
	setAttr ".tgi[0].ni[214].nvs" 18304;
	setAttr ".tgi[0].ni[215].x" -2320;
	setAttr ".tgi[0].ni[215].y" 7892.85693359375;
	setAttr ".tgi[0].ni[215].nvs" 18304;
	setAttr ".tgi[0].ni[216].x" -2934.28564453125;
	setAttr ".tgi[0].ni[216].y" 8790;
	setAttr ".tgi[0].ni[216].nvs" 18304;
	setAttr ".tgi[0].ni[217].x" -2934.28564453125;
	setAttr ".tgi[0].ni[217].y" 8688.5712890625;
	setAttr ".tgi[0].ni[217].nvs" 18304;
	setAttr ".tgi[0].ni[218].x" -2320;
	setAttr ".tgi[0].ni[218].y" 8790;
	setAttr ".tgi[0].ni[218].nvs" 18304;
	setAttr ".tgi[0].ni[219].x" -2934.28564453125;
	setAttr ".tgi[0].ni[219].y" 8022.85693359375;
	setAttr ".tgi[0].ni[219].nvs" 18304;
	setAttr ".tgi[0].ni[220].x" -2934.28564453125;
	setAttr ".tgi[0].ni[220].y" 8558.5712890625;
	setAttr ".tgi[0].ni[220].nvs" 18304;
	setAttr ".tgi[0].ni[221].x" -2320;
	setAttr ".tgi[0].ni[221].y" 8688.5712890625;
	setAttr ".tgi[0].ni[221].nvs" 18304;
	setAttr ".tgi[0].ni[222].x" -2934.28564453125;
	setAttr ".tgi[0].ni[222].y" 8355.7138671875;
	setAttr ".tgi[0].ni[222].nvs" 18304;
	setAttr ".tgi[0].ni[223].x" -2320;
	setAttr ".tgi[0].ni[223].y" 7791.4287109375;
	setAttr ".tgi[0].ni[223].nvs" 18304;
	setAttr ".tgi[0].ni[224].x" -2934.28564453125;
	setAttr ".tgi[0].ni[224].y" 8457.142578125;
	setAttr ".tgi[0].ni[224].nvs" 18304;
	setAttr ".tgi[0].ni[225].x" -2627.142822265625;
	setAttr ".tgi[0].ni[225].y" 8790;
	setAttr ".tgi[0].ni[225].nvs" 18304;
	setAttr ".tgi[0].ni[226].x" -2320;
	setAttr ".tgi[0].ni[226].y" 8022.85693359375;
	setAttr ".tgi[0].ni[226].nvs" 18304;
	setAttr ".tgi[0].ni[227].x" -2934.28564453125;
	setAttr ".tgi[0].ni[227].y" 8225.7138671875;
	setAttr ".tgi[0].ni[227].nvs" 18304;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av -k on ".unw" 1;
	setAttr -av -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -av -k on ".ihi";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".cons" no;
	setAttr -av ".ta" 0;
	setAttr -av ".tq";
	setAttr ".tmr" 1024;
	setAttr -av ".aoam";
	setAttr -av ".aora";
	setAttr -av ".hfd";
	setAttr -av ".hfe";
	setAttr -av ".hfa";
	setAttr -av ".mbe";
	setAttr -av -k on ".mbsof";
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 3 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
	setAttr -s 2 ".r";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".macc";
	setAttr -av -k on ".macd";
	setAttr -av -k on ".macq";
	setAttr -av -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -av -k on ".clip";
	setAttr -av -k on ".edm";
	setAttr -av -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -av -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -av -cb on ".imfkey";
	setAttr -av -k on ".gama";
	setAttr -av -k on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs";
	setAttr -k on ".ef";
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -av -k on ".be";
	setAttr -av -cb on ".ep";
	setAttr -av -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -av -k on ".pff";
	setAttr -av -cb on ".peie";
	setAttr -av -cb on ".ifp";
	setAttr -k on ".rv";
	setAttr -av -k on ".comp";
	setAttr -av -k on ".cth";
	setAttr -av -k on ".soll";
	setAttr -cb on ".sosl";
	setAttr -av -k on ".rd";
	setAttr -av -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -av -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -av -k on ".mm";
	setAttr -av -k on ".npu";
	setAttr -av -k on ".itf";
	setAttr -av -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -av -k on ".uf";
	setAttr -av -k on ".oi";
	setAttr -av -k on ".rut";
	setAttr -av -k on ".mot";
	setAttr -av -cb on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -av -k on ".mbso";
	setAttr -av -k on ".mbsc";
	setAttr -av -k on ".afp";
	setAttr -av -k on ".pfb";
	setAttr -k on ".pram" -type "string" "pgYetiVRayPreRender";
	setAttr -k on ".poam" -type "string" "pgYetiVRayPostRender";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -av -k on ".ubc";
	setAttr -av -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -av -k on ".udbx";
	setAttr -av -k on ".smc";
	setAttr -av -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -av -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -av -k on ".tlwd";
	setAttr -av -k on ".tlht";
	setAttr -av -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -av -k on ".ope";
	setAttr -av -k on ".oppf";
	setAttr -av -k on ".rcp";
	setAttr -av -k on ".icp";
	setAttr -av -k on ".ocp";
	setAttr -cb on ".hbl";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :defaultColorMgtGlobals;
	setAttr ".cme" no;
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -cb on ".hwcc";
	setAttr -cb on ".hwdp";
	setAttr -cb on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
connectAttr "EyeBrowATr_L_Mdv.ox" "EyeBrowA_R_NrbTmp.tx";
connectAttr "EyeBrowATr_L_Mdv.oy" "EyeBrowA_R_NrbTmp.ty";
connectAttr "EyeBrowATr_L_Mdv.oz" "EyeBrowA_R_NrbTmp.tz";
connectAttr "unitConversion52.o" "EyeBrowA_R_NrbTmp.rx";
connectAttr "unitConversion53.o" "EyeBrowA_R_NrbTmp.ry";
connectAttr "unitConversion54.o" "EyeBrowA_R_NrbTmp.rz";
connectAttr "EyeBrowBTr_L_Mdv.ox" "EyeBrowB_R_NrbTmp.tx";
connectAttr "EyeBrowBTr_L_Mdv.oy" "EyeBrowB_R_NrbTmp.ty";
connectAttr "EyeBrowBTr_L_Mdv.oz" "EyeBrowB_R_NrbTmp.tz";
connectAttr "unitConversion100.o" "EyeBrowB_R_NrbTmp.rx";
connectAttr "unitConversion101.o" "EyeBrowB_R_NrbTmp.ry";
connectAttr "unitConversion102.o" "EyeBrowB_R_NrbTmp.rz";
connectAttr "EyeBrowCTr_L_Mdv.ox" "EyeBrowC_R_NrbTmp.tx";
connectAttr "EyeBrowCTr_L_Mdv.oy" "EyeBrowC_R_NrbTmp.ty";
connectAttr "EyeBrowCTr_L_Mdv.oz" "EyeBrowC_R_NrbTmp.tz";
connectAttr "unitConversion106.o" "EyeBrowC_R_NrbTmp.rx";
connectAttr "unitConversion107.o" "EyeBrowC_R_NrbTmp.ry";
connectAttr "unitConversion108.o" "EyeBrowC_R_NrbTmp.rz";
connectAttr "EyeLid1Tr_L_Mdv.ox" "EyeLid1_R_NrbTmp.tx";
connectAttr "EyeLid1Tr_L_Mdv.oy" "EyeLid1_R_NrbTmp.ty";
connectAttr "EyeLid1Tr_L_Mdv.oz" "EyeLid1_R_NrbTmp.tz";
connectAttr "unitConversion94.o" "EyeLid1_R_NrbTmp.rx";
connectAttr "unitConversion95.o" "EyeLid1_R_NrbTmp.ry";
connectAttr "unitConversion96.o" "EyeLid1_R_NrbTmp.rz";
connectAttr "EyeLid2Tr_L_Mdv.ox" "EyeLid2_R_NrbTmp.tx";
connectAttr "EyeLid2Tr_L_Mdv.oy" "EyeLid2_R_NrbTmp.ty";
connectAttr "EyeLid2Tr_L_Mdv.oz" "EyeLid2_R_NrbTmp.tz";
connectAttr "unitConversion76.o" "EyeLid2_R_NrbTmp.rx";
connectAttr "unitConversion77.o" "EyeLid2_R_NrbTmp.ry";
connectAttr "unitConversion78.o" "EyeLid2_R_NrbTmp.rz";
connectAttr "EyeLid3Tr_L_Mdv.ox" "EyeLid3_R_NrbTmp.tx";
connectAttr "EyeLid3Tr_L_Mdv.oy" "EyeLid3_R_NrbTmp.ty";
connectAttr "EyeLid3Tr_L_Mdv.oz" "EyeLid3_R_NrbTmp.tz";
connectAttr "unitConversion40.o" "EyeLid3_R_NrbTmp.rx";
connectAttr "unitConversion41.o" "EyeLid3_R_NrbTmp.ry";
connectAttr "unitConversion42.o" "EyeLid3_R_NrbTmp.rz";
connectAttr "EyeLid4Tr_L_Mdv.ox" "EyeLid4_R_NrbTmp.tx";
connectAttr "EyeLid4Tr_L_Mdv.oy" "EyeLid4_R_NrbTmp.ty";
connectAttr "EyeLid4Tr_L_Mdv.oz" "EyeLid4_R_NrbTmp.tz";
connectAttr "unitConversion22.o" "EyeLid4_R_NrbTmp.rx";
connectAttr "unitConversion23.o" "EyeLid4_R_NrbTmp.ry";
connectAttr "unitConversion24.o" "EyeLid4_R_NrbTmp.rz";
connectAttr "EyeLid5Tr_L_Mdv.ox" "EyeLid5_R_NrbTmp.tx";
connectAttr "EyeLid5Tr_L_Mdv.oy" "EyeLid5_R_NrbTmp.ty";
connectAttr "EyeLid5Tr_L_Mdv.oz" "EyeLid5_R_NrbTmp.tz";
connectAttr "unitConversion28.o" "EyeLid5_R_NrbTmp.rx";
connectAttr "unitConversion29.o" "EyeLid5_R_NrbTmp.ry";
connectAttr "unitConversion30.o" "EyeLid5_R_NrbTmp.rz";
connectAttr "EyeLid6Tr_L_Mdv.ox" "EyeLid6_R_NrbTmp.tx";
connectAttr "EyeLid6Tr_L_Mdv.oy" "EyeLid6_R_NrbTmp.ty";
connectAttr "EyeLid6Tr_L_Mdv.oz" "EyeLid6_R_NrbTmp.tz";
connectAttr "unitConversion10.o" "EyeLid6_R_NrbTmp.rx";
connectAttr "unitConversion11.o" "EyeLid6_R_NrbTmp.ry";
connectAttr "unitConversion12.o" "EyeLid6_R_NrbTmp.rz";
connectAttr "EyeLid7Tr_L_Mdv.ox" "EyeLid7_R_NrbTmp.tx";
connectAttr "EyeLid7Tr_L_Mdv.oy" "EyeLid7_R_NrbTmp.ty";
connectAttr "EyeLid7Tr_L_Mdv.oz" "EyeLid7_R_NrbTmp.tz";
connectAttr "unitConversion46.o" "EyeLid7_R_NrbTmp.rx";
connectAttr "unitConversion47.o" "EyeLid7_R_NrbTmp.ry";
connectAttr "unitConversion48.o" "EyeLid7_R_NrbTmp.rz";
connectAttr "EyeLid8Tr_L_Mdv.ox" "EyeLid8_R_NrbTmp.tx";
connectAttr "EyeLid8Tr_L_Mdv.oy" "EyeLid8_R_NrbTmp.ty";
connectAttr "EyeLid8Tr_L_Mdv.oz" "EyeLid8_R_NrbTmp.tz";
connectAttr "unitConversion4.o" "EyeLid8_R_NrbTmp.rx";
connectAttr "unitConversion5.o" "EyeLid8_R_NrbTmp.ry";
connectAttr "unitConversion6.o" "EyeLid8_R_NrbTmp.rz";
connectAttr "EyeLid9Tr_L_Mdv.ox" "EyeLid9_R_NrbTmp.tx";
connectAttr "EyeLid9Tr_L_Mdv.oy" "EyeLid9_R_NrbTmp.ty";
connectAttr "EyeLid9Tr_L_Mdv.oz" "EyeLid9_R_NrbTmp.tz";
connectAttr "unitConversion166.o" "EyeLid9_R_NrbTmp.rx";
connectAttr "unitConversion167.o" "EyeLid9_R_NrbTmp.ry";
connectAttr "unitConversion168.o" "EyeLid9_R_NrbTmp.rz";
connectAttr "EyeLid10Tr_L_Mdv.ox" "EyeLid10_R_NrbTmp.tx";
connectAttr "EyeLid10Tr_L_Mdv.oy" "EyeLid10_R_NrbTmp.ty";
connectAttr "EyeLid10Tr_L_Mdv.oz" "EyeLid10_R_NrbTmp.tz";
connectAttr "unitConversion160.o" "EyeLid10_R_NrbTmp.rx";
connectAttr "unitConversion161.o" "EyeLid10_R_NrbTmp.ry";
connectAttr "unitConversion162.o" "EyeLid10_R_NrbTmp.rz";
connectAttr "EyeLid11Tr_L_Mdv.ox" "EyeLid11_R_NrbTmp.tx";
connectAttr "EyeLid11Tr_L_Mdv.oy" "EyeLid11_R_NrbTmp.ty";
connectAttr "EyeLid11Tr_L_Mdv.oz" "EyeLid11_R_NrbTmp.tz";
connectAttr "unitConversion154.o" "EyeLid11_R_NrbTmp.rx";
connectAttr "unitConversion155.o" "EyeLid11_R_NrbTmp.ry";
connectAttr "unitConversion156.o" "EyeLid11_R_NrbTmp.rz";
connectAttr "EyeLid12Tr_L_Mdv.ox" "EyeLid12_R_NrbTmp.tx";
connectAttr "EyeLid12Tr_L_Mdv.oy" "EyeLid12_R_NrbTmp.ty";
connectAttr "EyeLid12Tr_L_Mdv.oz" "EyeLid12_R_NrbTmp.tz";
connectAttr "unitConversion136.o" "EyeLid12_R_NrbTmp.rx";
connectAttr "unitConversion137.o" "EyeLid12_R_NrbTmp.ry";
connectAttr "unitConversion138.o" "EyeLid12_R_NrbTmp.rz";
connectAttr "Mouth2Tr_L_Mdv.ox" "Mouth2_R_NrbTmp.tx";
connectAttr "Mouth2Tr_L_Mdv.oy" "Mouth2_R_NrbTmp.ty";
connectAttr "Mouth2Tr_L_Mdv.oz" "Mouth2_R_NrbTmp.tz";
connectAttr "unitConversion130.o" "Mouth2_R_NrbTmp.rx";
connectAttr "unitConversion131.o" "Mouth2_R_NrbTmp.ry";
connectAttr "unitConversion132.o" "Mouth2_R_NrbTmp.rz";
connectAttr "Mouth3Tr_L_Mdv.ox" "Mouth3_R_NrbTmp.tx";
connectAttr "Mouth3Tr_L_Mdv.oy" "Mouth3_R_NrbTmp.ty";
connectAttr "Mouth3Tr_L_Mdv.oz" "Mouth3_R_NrbTmp.tz";
connectAttr "unitConversion142.o" "Mouth3_R_NrbTmp.rx";
connectAttr "unitConversion143.o" "Mouth3_R_NrbTmp.ry";
connectAttr "unitConversion144.o" "Mouth3_R_NrbTmp.rz";
connectAttr "Mouth4Tr_L_Mdv.ox" "Mouth4_R_NrbTmp.tx";
connectAttr "Mouth4Tr_L_Mdv.oy" "Mouth4_R_NrbTmp.ty";
connectAttr "Mouth4Tr_L_Mdv.oz" "Mouth4_R_NrbTmp.tz";
connectAttr "unitConversion148.o" "Mouth4_R_NrbTmp.rx";
connectAttr "unitConversion149.o" "Mouth4_R_NrbTmp.ry";
connectAttr "unitConversion150.o" "Mouth4_R_NrbTmp.rz";
connectAttr "Mouth5Tr_L_Mdv.ox" "Mouth5_R_NrbTmp.tx";
connectAttr "Mouth5Tr_L_Mdv.oy" "Mouth5_R_NrbTmp.ty";
connectAttr "Mouth5Tr_L_Mdv.oz" "Mouth5_R_NrbTmp.tz";
connectAttr "unitConversion118.o" "Mouth5_R_NrbTmp.rx";
connectAttr "unitConversion119.o" "Mouth5_R_NrbTmp.ry";
connectAttr "unitConversion120.o" "Mouth5_R_NrbTmp.rz";
connectAttr "Mouth7Tr_L_Mdv.ox" "Mouth7_R_NrbTmp.tx";
connectAttr "Mouth7Tr_L_Mdv.oy" "Mouth7_R_NrbTmp.ty";
connectAttr "Mouth7Tr_L_Mdv.oz" "Mouth7_R_NrbTmp.tz";
connectAttr "unitConversion112.o" "Mouth7_R_NrbTmp.rx";
connectAttr "unitConversion113.o" "Mouth7_R_NrbTmp.ry";
connectAttr "unitConversion114.o" "Mouth7_R_NrbTmp.rz";
connectAttr "Mouth9Tr_L_Mdv.ox" "Mouth9_R_NrbTmp.tx";
connectAttr "Mouth9Tr_L_Mdv.oy" "Mouth9_R_NrbTmp.ty";
connectAttr "Mouth9Tr_L_Mdv.oz" "Mouth9_R_NrbTmp.tz";
connectAttr "unitConversion124.o" "Mouth9_R_NrbTmp.rx";
connectAttr "unitConversion125.o" "Mouth9_R_NrbTmp.ry";
connectAttr "unitConversion126.o" "Mouth9_R_NrbTmp.rz";
connectAttr "Mouth10Tr_L_Mdv.ox" "Mouth10_R_NrbTmp.tx";
connectAttr "Mouth10Tr_L_Mdv.oy" "Mouth10_R_NrbTmp.ty";
connectAttr "Mouth10Tr_L_Mdv.oz" "Mouth10_R_NrbTmp.tz";
connectAttr "unitConversion34.o" "Mouth10_R_NrbTmp.rx";
connectAttr "unitConversion35.o" "Mouth10_R_NrbTmp.ry";
connectAttr "unitConversion36.o" "Mouth10_R_NrbTmp.rz";
connectAttr "Mouth11Tr_L_Mdv.ox" "Mouth11_R_NrbTmp.tx";
connectAttr "Mouth11Tr_L_Mdv.oy" "Mouth11_R_NrbTmp.ty";
connectAttr "Mouth11Tr_L_Mdv.oz" "Mouth11_R_NrbTmp.tz";
connectAttr "unitConversion58.o" "Mouth11_R_NrbTmp.rx";
connectAttr "unitConversion59.o" "Mouth11_R_NrbTmp.ry";
connectAttr "unitConversion60.o" "Mouth11_R_NrbTmp.rz";
connectAttr "Mouth12Tr_L_Mdv.ox" "Mouth12_R_NrbTmp.tx";
connectAttr "Mouth12Tr_L_Mdv.oy" "Mouth12_R_NrbTmp.ty";
connectAttr "Mouth12Tr_L_Mdv.oz" "Mouth12_R_NrbTmp.tz";
connectAttr "unitConversion82.o" "Mouth12_R_NrbTmp.rx";
connectAttr "unitConversion83.o" "Mouth12_R_NrbTmp.ry";
connectAttr "unitConversion84.o" "Mouth12_R_NrbTmp.rz";
connectAttr "CheekATr_L_Mdv.ox" "CheekA_R_NrbTmp.tx";
connectAttr "CheekATr_L_Mdv.oy" "CheekA_R_NrbTmp.ty";
connectAttr "CheekATr_L_Mdv.oz" "CheekA_R_NrbTmp.tz";
connectAttr "unitConversion88.o" "CheekA_R_NrbTmp.rx";
connectAttr "unitConversion89.o" "CheekA_R_NrbTmp.ry";
connectAttr "unitConversion90.o" "CheekA_R_NrbTmp.rz";
connectAttr "CheekBTr_L_Mdv.ox" "CheekB_R_NrbTmp.tx";
connectAttr "CheekBTr_L_Mdv.oy" "CheekB_R_NrbTmp.ty";
connectAttr "CheekBTr_L_Mdv.oz" "CheekB_R_NrbTmp.tz";
connectAttr "unitConversion70.o" "CheekB_R_NrbTmp.rx";
connectAttr "unitConversion71.o" "CheekB_R_NrbTmp.ry";
connectAttr "unitConversion72.o" "CheekB_R_NrbTmp.rz";
connectAttr "CheekCTr_L_Mdv.ox" "CheekC_R_NrbTmp.tx";
connectAttr "CheekCTr_L_Mdv.oy" "CheekC_R_NrbTmp.ty";
connectAttr "CheekCTr_L_Mdv.oz" "CheekC_R_NrbTmp.tz";
connectAttr "unitConversion16.o" "CheekC_R_NrbTmp.rx";
connectAttr "unitConversion17.o" "CheekC_R_NrbTmp.ry";
connectAttr "unitConversion18.o" "CheekC_R_NrbTmp.rz";
connectAttr "PuffTr_L_Mdv.ox" "Puff_R_NrbTmp.tx";
connectAttr "PuffTr_L_Mdv.oy" "Puff_R_NrbTmp.ty";
connectAttr "PuffTr_L_Mdv.oz" "Puff_R_NrbTmp.tz";
connectAttr "unitConversion64.o" "Puff_R_NrbTmp.rx";
connectAttr "unitConversion65.o" "Puff_R_NrbTmp.ry";
connectAttr "unitConversion66.o" "Puff_R_NrbTmp.rz";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "DetailNrb_renderLayerManager.rlmi[0]" "DetailNrb_defaultRenderLayer.rlid"
		;
connectAttr "lambert2.oc" "lambert2SG.ss";
connectAttr "NsB_C_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "NsA_C_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeBrow_C_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Puff_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Puff_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "CheekC_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "CheekC_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "CheekB_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "CheekB_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "CheekA_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "CheekA_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth13_C_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth12_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth12_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth11_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth11_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth10_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth10_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth9_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth9_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth7_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth7_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth5_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth5_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth4_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth4_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth3_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth3_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth2_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth2_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "Mouth1_C_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid12_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid12_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid11_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid11_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid10_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid10_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid9_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid9_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid8_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid8_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid7_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid7_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid6_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid6_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid5_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid5_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid4_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid4_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid3_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid3_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid2_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid2_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid1_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeLid1_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeBrowC_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeBrowC_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeBrowB_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeBrowB_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeBrowA_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "EyeBrowA_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "MouthUprBsh_C_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "MouthLwrBsh_C_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "MouthCnrBsh_L_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "MouthCnrBsh_R_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "NoseBsh_C_NrbTmpShape.iog" "lambert2SG.dsm" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "lambert2.msg" "materialInfo1.m";
connectAttr "lambert2.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "lambert2SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "EyeLid8_L_NrbTmp.tx" "EyeLid8Tr_L_Mdv.i1x";
connectAttr "EyeLid8_L_NrbTmp.ty" "EyeLid8Tr_L_Mdv.i1y";
connectAttr "EyeLid8_L_NrbTmp.tz" "EyeLid8Tr_L_Mdv.i1z";
connectAttr "EyeLid6_L_NrbTmp.tx" "EyeLid6Tr_L_Mdv.i1x";
connectAttr "EyeLid6_L_NrbTmp.ty" "EyeLid6Tr_L_Mdv.i1y";
connectAttr "EyeLid6_L_NrbTmp.tz" "EyeLid6Tr_L_Mdv.i1z";
connectAttr "CheekC_L_NrbTmp.tx" "CheekCTr_L_Mdv.i1x";
connectAttr "CheekC_L_NrbTmp.ty" "CheekCTr_L_Mdv.i1y";
connectAttr "CheekC_L_NrbTmp.tz" "CheekCTr_L_Mdv.i1z";
connectAttr "EyeLid4_L_NrbTmp.tx" "EyeLid4Tr_L_Mdv.i1x";
connectAttr "EyeLid4_L_NrbTmp.ty" "EyeLid4Tr_L_Mdv.i1y";
connectAttr "EyeLid4_L_NrbTmp.tz" "EyeLid4Tr_L_Mdv.i1z";
connectAttr "EyeLid5_L_NrbTmp.tx" "EyeLid5Tr_L_Mdv.i1x";
connectAttr "EyeLid5_L_NrbTmp.ty" "EyeLid5Tr_L_Mdv.i1y";
connectAttr "EyeLid5_L_NrbTmp.tz" "EyeLid5Tr_L_Mdv.i1z";
connectAttr "Mouth10_L_NrbTmp.tx" "Mouth10Tr_L_Mdv.i1x";
connectAttr "Mouth10_L_NrbTmp.ty" "Mouth10Tr_L_Mdv.i1y";
connectAttr "Mouth10_L_NrbTmp.tz" "Mouth10Tr_L_Mdv.i1z";
connectAttr "EyeLid3_L_NrbTmp.tx" "EyeLid3Tr_L_Mdv.i1x";
connectAttr "EyeLid3_L_NrbTmp.ty" "EyeLid3Tr_L_Mdv.i1y";
connectAttr "EyeLid3_L_NrbTmp.tz" "EyeLid3Tr_L_Mdv.i1z";
connectAttr "EyeLid7_L_NrbTmp.tx" "EyeLid7Tr_L_Mdv.i1x";
connectAttr "EyeLid7_L_NrbTmp.ty" "EyeLid7Tr_L_Mdv.i1y";
connectAttr "EyeLid7_L_NrbTmp.tz" "EyeLid7Tr_L_Mdv.i1z";
connectAttr "EyeBrowA_L_NrbTmp.tx" "EyeBrowATr_L_Mdv.i1x";
connectAttr "EyeBrowA_L_NrbTmp.ty" "EyeBrowATr_L_Mdv.i1y";
connectAttr "EyeBrowA_L_NrbTmp.tz" "EyeBrowATr_L_Mdv.i1z";
connectAttr "Mouth11_L_NrbTmp.tx" "Mouth11Tr_L_Mdv.i1x";
connectAttr "Mouth11_L_NrbTmp.ty" "Mouth11Tr_L_Mdv.i1y";
connectAttr "Mouth11_L_NrbTmp.tz" "Mouth11Tr_L_Mdv.i1z";
connectAttr "Puff_L_NrbTmp.tx" "PuffTr_L_Mdv.i1x";
connectAttr "Puff_L_NrbTmp.ty" "PuffTr_L_Mdv.i1y";
connectAttr "Puff_L_NrbTmp.tz" "PuffTr_L_Mdv.i1z";
connectAttr "CheekB_L_NrbTmp.tx" "CheekBTr_L_Mdv.i1x";
connectAttr "CheekB_L_NrbTmp.ty" "CheekBTr_L_Mdv.i1y";
connectAttr "CheekB_L_NrbTmp.tz" "CheekBTr_L_Mdv.i1z";
connectAttr "EyeLid2_L_NrbTmp.tx" "EyeLid2Tr_L_Mdv.i1x";
connectAttr "EyeLid2_L_NrbTmp.ty" "EyeLid2Tr_L_Mdv.i1y";
connectAttr "EyeLid2_L_NrbTmp.tz" "EyeLid2Tr_L_Mdv.i1z";
connectAttr "Mouth12_L_NrbTmp.tx" "Mouth12Tr_L_Mdv.i1x";
connectAttr "Mouth12_L_NrbTmp.ty" "Mouth12Tr_L_Mdv.i1y";
connectAttr "Mouth12_L_NrbTmp.tz" "Mouth12Tr_L_Mdv.i1z";
connectAttr "CheekA_L_NrbTmp.tx" "CheekATr_L_Mdv.i1x";
connectAttr "CheekA_L_NrbTmp.ty" "CheekATr_L_Mdv.i1y";
connectAttr "CheekA_L_NrbTmp.tz" "CheekATr_L_Mdv.i1z";
connectAttr "EyeLid1_L_NrbTmp.tx" "EyeLid1Tr_L_Mdv.i1x";
connectAttr "EyeLid1_L_NrbTmp.ty" "EyeLid1Tr_L_Mdv.i1y";
connectAttr "EyeLid1_L_NrbTmp.tz" "EyeLid1Tr_L_Mdv.i1z";
connectAttr "EyeBrowB_L_NrbTmp.tx" "EyeBrowBTr_L_Mdv.i1x";
connectAttr "EyeBrowB_L_NrbTmp.ty" "EyeBrowBTr_L_Mdv.i1y";
connectAttr "EyeBrowB_L_NrbTmp.tz" "EyeBrowBTr_L_Mdv.i1z";
connectAttr "EyeBrowC_L_NrbTmp.tx" "EyeBrowCTr_L_Mdv.i1x";
connectAttr "EyeBrowC_L_NrbTmp.ty" "EyeBrowCTr_L_Mdv.i1y";
connectAttr "EyeBrowC_L_NrbTmp.tz" "EyeBrowCTr_L_Mdv.i1z";
connectAttr "Mouth7_L_NrbTmp.tx" "Mouth7Tr_L_Mdv.i1x";
connectAttr "Mouth7_L_NrbTmp.ty" "Mouth7Tr_L_Mdv.i1y";
connectAttr "Mouth7_L_NrbTmp.tz" "Mouth7Tr_L_Mdv.i1z";
connectAttr "Mouth5_L_NrbTmp.tx" "Mouth5Tr_L_Mdv.i1x";
connectAttr "Mouth5_L_NrbTmp.ty" "Mouth5Tr_L_Mdv.i1y";
connectAttr "Mouth5_L_NrbTmp.tz" "Mouth5Tr_L_Mdv.i1z";
connectAttr "Mouth9_L_NrbTmp.tx" "Mouth9Tr_L_Mdv.i1x";
connectAttr "Mouth9_L_NrbTmp.ty" "Mouth9Tr_L_Mdv.i1y";
connectAttr "Mouth9_L_NrbTmp.tz" "Mouth9Tr_L_Mdv.i1z";
connectAttr "Mouth2_L_NrbTmp.tx" "Mouth2Tr_L_Mdv.i1x";
connectAttr "Mouth2_L_NrbTmp.ty" "Mouth2Tr_L_Mdv.i1y";
connectAttr "Mouth2_L_NrbTmp.tz" "Mouth2Tr_L_Mdv.i1z";
connectAttr "EyeLid12_L_NrbTmp.tx" "EyeLid12Tr_L_Mdv.i1x";
connectAttr "EyeLid12_L_NrbTmp.ty" "EyeLid12Tr_L_Mdv.i1y";
connectAttr "EyeLid12_L_NrbTmp.tz" "EyeLid12Tr_L_Mdv.i1z";
connectAttr "Mouth3_L_NrbTmp.tx" "Mouth3Tr_L_Mdv.i1x";
connectAttr "Mouth3_L_NrbTmp.ty" "Mouth3Tr_L_Mdv.i1y";
connectAttr "Mouth3_L_NrbTmp.tz" "Mouth3Tr_L_Mdv.i1z";
connectAttr "Mouth4_L_NrbTmp.tx" "Mouth4Tr_L_Mdv.i1x";
connectAttr "Mouth4_L_NrbTmp.ty" "Mouth4Tr_L_Mdv.i1y";
connectAttr "Mouth4_L_NrbTmp.tz" "Mouth4Tr_L_Mdv.i1z";
connectAttr "EyeLid11_L_NrbTmp.tx" "EyeLid11Tr_L_Mdv.i1x";
connectAttr "EyeLid11_L_NrbTmp.ty" "EyeLid11Tr_L_Mdv.i1y";
connectAttr "EyeLid11_L_NrbTmp.tz" "EyeLid11Tr_L_Mdv.i1z";
connectAttr "EyeLid10_L_NrbTmp.tx" "EyeLid10Tr_L_Mdv.i1x";
connectAttr "EyeLid10_L_NrbTmp.ty" "EyeLid10Tr_L_Mdv.i1y";
connectAttr "EyeLid10_L_NrbTmp.tz" "EyeLid10Tr_L_Mdv.i1z";
connectAttr "EyeLid9_L_NrbTmp.tx" "EyeLid9Tr_L_Mdv.i1x";
connectAttr "EyeLid9_L_NrbTmp.ty" "EyeLid9Tr_L_Mdv.i1y";
connectAttr "EyeLid9_L_NrbTmp.tz" "EyeLid9Tr_L_Mdv.i1z";
connectAttr "unitConversion1.o" "EyeLid8Ro_L_Mdv.i1x";
connectAttr "unitConversion2.o" "EyeLid8Ro_L_Mdv.i1y";
connectAttr "unitConversion3.o" "EyeLid8Ro_L_Mdv.i1z";
connectAttr "EyeLid8_L_NrbTmp.rx" "unitConversion1.i";
connectAttr "EyeLid8_L_NrbTmp.ry" "unitConversion2.i";
connectAttr "EyeLid8_L_NrbTmp.rz" "unitConversion3.i";
connectAttr "EyeLid8Ro_L_Mdv.ox" "unitConversion4.i";
connectAttr "EyeLid8Ro_L_Mdv.oy" "unitConversion5.i";
connectAttr "EyeLid8Ro_L_Mdv.oz" "unitConversion6.i";
connectAttr "unitConversion7.o" "EyeLid6Ro_L_Mdv.i1x";
connectAttr "unitConversion8.o" "EyeLid6Ro_L_Mdv.i1y";
connectAttr "unitConversion9.o" "EyeLid6Ro_L_Mdv.i1z";
connectAttr "EyeLid6_L_NrbTmp.rx" "unitConversion7.i";
connectAttr "EyeLid6_L_NrbTmp.ry" "unitConversion8.i";
connectAttr "EyeLid6_L_NrbTmp.rz" "unitConversion9.i";
connectAttr "EyeLid6Ro_L_Mdv.ox" "unitConversion10.i";
connectAttr "EyeLid6Ro_L_Mdv.oy" "unitConversion11.i";
connectAttr "EyeLid6Ro_L_Mdv.oz" "unitConversion12.i";
connectAttr "unitConversion13.o" "CheekCRo_L_Mdv.i1x";
connectAttr "unitConversion14.o" "CheekCRo_L_Mdv.i1y";
connectAttr "unitConversion15.o" "CheekCRo_L_Mdv.i1z";
connectAttr "CheekC_L_NrbTmp.rx" "unitConversion13.i";
connectAttr "CheekC_L_NrbTmp.ry" "unitConversion14.i";
connectAttr "CheekC_L_NrbTmp.rz" "unitConversion15.i";
connectAttr "CheekCRo_L_Mdv.ox" "unitConversion16.i";
connectAttr "CheekCRo_L_Mdv.oy" "unitConversion17.i";
connectAttr "CheekCRo_L_Mdv.oz" "unitConversion18.i";
connectAttr "unitConversion19.o" "EyeLid4Ro_L_Mdv.i1x";
connectAttr "unitConversion20.o" "EyeLid4Ro_L_Mdv.i1y";
connectAttr "unitConversion21.o" "EyeLid4Ro_L_Mdv.i1z";
connectAttr "EyeLid4_L_NrbTmp.rx" "unitConversion19.i";
connectAttr "EyeLid4_L_NrbTmp.ry" "unitConversion20.i";
connectAttr "EyeLid4_L_NrbTmp.rz" "unitConversion21.i";
connectAttr "EyeLid4Ro_L_Mdv.ox" "unitConversion22.i";
connectAttr "EyeLid4Ro_L_Mdv.oy" "unitConversion23.i";
connectAttr "EyeLid4Ro_L_Mdv.oz" "unitConversion24.i";
connectAttr "unitConversion25.o" "EyeLid5Ro_L_Mdv.i1x";
connectAttr "unitConversion26.o" "EyeLid5Ro_L_Mdv.i1y";
connectAttr "unitConversion27.o" "EyeLid5Ro_L_Mdv.i1z";
connectAttr "EyeLid5_L_NrbTmp.rx" "unitConversion25.i";
connectAttr "EyeLid5_L_NrbTmp.ry" "unitConversion26.i";
connectAttr "EyeLid5_L_NrbTmp.rz" "unitConversion27.i";
connectAttr "EyeLid5Ro_L_Mdv.ox" "unitConversion28.i";
connectAttr "EyeLid5Ro_L_Mdv.oy" "unitConversion29.i";
connectAttr "EyeLid5Ro_L_Mdv.oz" "unitConversion30.i";
connectAttr "unitConversion31.o" "Mouth10Ro_L_Mdv.i1x";
connectAttr "unitConversion32.o" "Mouth10Ro_L_Mdv.i1y";
connectAttr "unitConversion33.o" "Mouth10Ro_L_Mdv.i1z";
connectAttr "Mouth10_L_NrbTmp.rx" "unitConversion31.i";
connectAttr "Mouth10_L_NrbTmp.ry" "unitConversion32.i";
connectAttr "Mouth10_L_NrbTmp.rz" "unitConversion33.i";
connectAttr "Mouth10Ro_L_Mdv.ox" "unitConversion34.i";
connectAttr "Mouth10Ro_L_Mdv.oy" "unitConversion35.i";
connectAttr "Mouth10Ro_L_Mdv.oz" "unitConversion36.i";
connectAttr "unitConversion37.o" "EyeLid3Ro_L_Mdv.i1x";
connectAttr "unitConversion38.o" "EyeLid3Ro_L_Mdv.i1y";
connectAttr "unitConversion39.o" "EyeLid3Ro_L_Mdv.i1z";
connectAttr "EyeLid3_L_NrbTmp.rx" "unitConversion37.i";
connectAttr "EyeLid3_L_NrbTmp.ry" "unitConversion38.i";
connectAttr "EyeLid3_L_NrbTmp.rz" "unitConversion39.i";
connectAttr "EyeLid3Ro_L_Mdv.ox" "unitConversion40.i";
connectAttr "EyeLid3Ro_L_Mdv.oy" "unitConversion41.i";
connectAttr "EyeLid3Ro_L_Mdv.oz" "unitConversion42.i";
connectAttr "unitConversion43.o" "EyeLid7Ro_L_Mdv.i1x";
connectAttr "unitConversion44.o" "EyeLid7Ro_L_Mdv.i1y";
connectAttr "unitConversion45.o" "EyeLid7Ro_L_Mdv.i1z";
connectAttr "EyeLid7_L_NrbTmp.rx" "unitConversion43.i";
connectAttr "EyeLid7_L_NrbTmp.ry" "unitConversion44.i";
connectAttr "EyeLid7_L_NrbTmp.rz" "unitConversion45.i";
connectAttr "EyeLid7Ro_L_Mdv.ox" "unitConversion46.i";
connectAttr "EyeLid7Ro_L_Mdv.oy" "unitConversion47.i";
connectAttr "EyeLid7Ro_L_Mdv.oz" "unitConversion48.i";
connectAttr "unitConversion49.o" "EyeBrowARo_L_Mdv.i1x";
connectAttr "unitConversion50.o" "EyeBrowARo_L_Mdv.i1y";
connectAttr "unitConversion51.o" "EyeBrowARo_L_Mdv.i1z";
connectAttr "EyeBrowA_L_NrbTmp.rx" "unitConversion49.i";
connectAttr "EyeBrowA_L_NrbTmp.ry" "unitConversion50.i";
connectAttr "EyeBrowA_L_NrbTmp.rz" "unitConversion51.i";
connectAttr "EyeBrowARo_L_Mdv.ox" "unitConversion52.i";
connectAttr "EyeBrowARo_L_Mdv.oy" "unitConversion53.i";
connectAttr "EyeBrowARo_L_Mdv.oz" "unitConversion54.i";
connectAttr "unitConversion55.o" "Mouth11Ro_L_Mdv.i1x";
connectAttr "unitConversion56.o" "Mouth11Ro_L_Mdv.i1y";
connectAttr "unitConversion57.o" "Mouth11Ro_L_Mdv.i1z";
connectAttr "Mouth11_L_NrbTmp.rx" "unitConversion55.i";
connectAttr "Mouth11_L_NrbTmp.ry" "unitConversion56.i";
connectAttr "Mouth11_L_NrbTmp.rz" "unitConversion57.i";
connectAttr "Mouth11Ro_L_Mdv.ox" "unitConversion58.i";
connectAttr "Mouth11Ro_L_Mdv.oy" "unitConversion59.i";
connectAttr "Mouth11Ro_L_Mdv.oz" "unitConversion60.i";
connectAttr "unitConversion61.o" "PuffRo_L_Mdv.i1x";
connectAttr "unitConversion62.o" "PuffRo_L_Mdv.i1y";
connectAttr "unitConversion63.o" "PuffRo_L_Mdv.i1z";
connectAttr "Puff_L_NrbTmp.rx" "unitConversion61.i";
connectAttr "Puff_L_NrbTmp.ry" "unitConversion62.i";
connectAttr "Puff_L_NrbTmp.rz" "unitConversion63.i";
connectAttr "PuffRo_L_Mdv.ox" "unitConversion64.i";
connectAttr "PuffRo_L_Mdv.oy" "unitConversion65.i";
connectAttr "PuffRo_L_Mdv.oz" "unitConversion66.i";
connectAttr "unitConversion67.o" "CheekBRo_L_Mdv.i1x";
connectAttr "unitConversion68.o" "CheekBRo_L_Mdv.i1y";
connectAttr "unitConversion69.o" "CheekBRo_L_Mdv.i1z";
connectAttr "CheekB_L_NrbTmp.rx" "unitConversion67.i";
connectAttr "CheekB_L_NrbTmp.ry" "unitConversion68.i";
connectAttr "CheekB_L_NrbTmp.rz" "unitConversion69.i";
connectAttr "CheekBRo_L_Mdv.ox" "unitConversion70.i";
connectAttr "CheekBRo_L_Mdv.oy" "unitConversion71.i";
connectAttr "CheekBRo_L_Mdv.oz" "unitConversion72.i";
connectAttr "unitConversion73.o" "EyeLid2Ro_L_Mdv.i1x";
connectAttr "unitConversion74.o" "EyeLid2Ro_L_Mdv.i1y";
connectAttr "unitConversion75.o" "EyeLid2Ro_L_Mdv.i1z";
connectAttr "EyeLid2_L_NrbTmp.rx" "unitConversion73.i";
connectAttr "EyeLid2_L_NrbTmp.ry" "unitConversion74.i";
connectAttr "EyeLid2_L_NrbTmp.rz" "unitConversion75.i";
connectAttr "EyeLid2Ro_L_Mdv.ox" "unitConversion76.i";
connectAttr "EyeLid2Ro_L_Mdv.oy" "unitConversion77.i";
connectAttr "EyeLid2Ro_L_Mdv.oz" "unitConversion78.i";
connectAttr "unitConversion79.o" "Mouth12Ro_L_Mdv.i1x";
connectAttr "unitConversion80.o" "Mouth12Ro_L_Mdv.i1y";
connectAttr "unitConversion81.o" "Mouth12Ro_L_Mdv.i1z";
connectAttr "Mouth12_L_NrbTmp.rx" "unitConversion79.i";
connectAttr "Mouth12_L_NrbTmp.ry" "unitConversion80.i";
connectAttr "Mouth12_L_NrbTmp.rz" "unitConversion81.i";
connectAttr "Mouth12Ro_L_Mdv.ox" "unitConversion82.i";
connectAttr "Mouth12Ro_L_Mdv.oy" "unitConversion83.i";
connectAttr "Mouth12Ro_L_Mdv.oz" "unitConversion84.i";
connectAttr "unitConversion85.o" "CheekARo_L_Mdv.i1x";
connectAttr "unitConversion86.o" "CheekARo_L_Mdv.i1y";
connectAttr "unitConversion87.o" "CheekARo_L_Mdv.i1z";
connectAttr "CheekA_L_NrbTmp.rx" "unitConversion85.i";
connectAttr "CheekA_L_NrbTmp.ry" "unitConversion86.i";
connectAttr "CheekA_L_NrbTmp.rz" "unitConversion87.i";
connectAttr "CheekARo_L_Mdv.ox" "unitConversion88.i";
connectAttr "CheekARo_L_Mdv.oy" "unitConversion89.i";
connectAttr "CheekARo_L_Mdv.oz" "unitConversion90.i";
connectAttr "unitConversion91.o" "EyeLid1Ro_L_Mdv.i1x";
connectAttr "unitConversion92.o" "EyeLid1Ro_L_Mdv.i1y";
connectAttr "unitConversion93.o" "EyeLid1Ro_L_Mdv.i1z";
connectAttr "EyeLid1_L_NrbTmp.rx" "unitConversion91.i";
connectAttr "EyeLid1_L_NrbTmp.ry" "unitConversion92.i";
connectAttr "EyeLid1_L_NrbTmp.rz" "unitConversion93.i";
connectAttr "EyeLid1Ro_L_Mdv.ox" "unitConversion94.i";
connectAttr "EyeLid1Ro_L_Mdv.oy" "unitConversion95.i";
connectAttr "EyeLid1Ro_L_Mdv.oz" "unitConversion96.i";
connectAttr "unitConversion97.o" "EyeBrowBRo_L_Mdv.i1x";
connectAttr "unitConversion98.o" "EyeBrowBRo_L_Mdv.i1y";
connectAttr "unitConversion99.o" "EyeBrowBRo_L_Mdv.i1z";
connectAttr "EyeBrowB_L_NrbTmp.rx" "unitConversion97.i";
connectAttr "EyeBrowB_L_NrbTmp.ry" "unitConversion98.i";
connectAttr "EyeBrowB_L_NrbTmp.rz" "unitConversion99.i";
connectAttr "EyeBrowBRo_L_Mdv.ox" "unitConversion100.i";
connectAttr "EyeBrowBRo_L_Mdv.oy" "unitConversion101.i";
connectAttr "EyeBrowBRo_L_Mdv.oz" "unitConversion102.i";
connectAttr "unitConversion103.o" "EyeBrowCRo_L_Mdv.i1x";
connectAttr "unitConversion104.o" "EyeBrowCRo_L_Mdv.i1y";
connectAttr "unitConversion105.o" "EyeBrowCRo_L_Mdv.i1z";
connectAttr "EyeBrowC_L_NrbTmp.rx" "unitConversion103.i";
connectAttr "EyeBrowC_L_NrbTmp.ry" "unitConversion104.i";
connectAttr "EyeBrowC_L_NrbTmp.rz" "unitConversion105.i";
connectAttr "EyeBrowCRo_L_Mdv.ox" "unitConversion106.i";
connectAttr "EyeBrowCRo_L_Mdv.oy" "unitConversion107.i";
connectAttr "EyeBrowCRo_L_Mdv.oz" "unitConversion108.i";
connectAttr "unitConversion109.o" "Mouth7Ro_L_Mdv.i1x";
connectAttr "unitConversion110.o" "Mouth7Ro_L_Mdv.i1y";
connectAttr "unitConversion111.o" "Mouth7Ro_L_Mdv.i1z";
connectAttr "Mouth7_L_NrbTmp.rx" "unitConversion109.i";
connectAttr "Mouth7_L_NrbTmp.ry" "unitConversion110.i";
connectAttr "Mouth7_L_NrbTmp.rz" "unitConversion111.i";
connectAttr "Mouth7Ro_L_Mdv.ox" "unitConversion112.i";
connectAttr "Mouth7Ro_L_Mdv.oy" "unitConversion113.i";
connectAttr "Mouth7Ro_L_Mdv.oz" "unitConversion114.i";
connectAttr "unitConversion115.o" "Mouth5Ro_L_Mdv.i1x";
connectAttr "unitConversion116.o" "Mouth5Ro_L_Mdv.i1y";
connectAttr "unitConversion117.o" "Mouth5Ro_L_Mdv.i1z";
connectAttr "Mouth5_L_NrbTmp.rx" "unitConversion115.i";
connectAttr "Mouth5_L_NrbTmp.ry" "unitConversion116.i";
connectAttr "Mouth5_L_NrbTmp.rz" "unitConversion117.i";
connectAttr "Mouth5Ro_L_Mdv.ox" "unitConversion118.i";
connectAttr "Mouth5Ro_L_Mdv.oy" "unitConversion119.i";
connectAttr "Mouth5Ro_L_Mdv.oz" "unitConversion120.i";
connectAttr "unitConversion121.o" "Mouth9Ro_L_Mdv.i1x";
connectAttr "unitConversion122.o" "Mouth9Ro_L_Mdv.i1y";
connectAttr "unitConversion123.o" "Mouth9Ro_L_Mdv.i1z";
connectAttr "Mouth9_L_NrbTmp.rx" "unitConversion121.i";
connectAttr "Mouth9_L_NrbTmp.ry" "unitConversion122.i";
connectAttr "Mouth9_L_NrbTmp.rz" "unitConversion123.i";
connectAttr "Mouth9Ro_L_Mdv.ox" "unitConversion124.i";
connectAttr "Mouth9Ro_L_Mdv.oy" "unitConversion125.i";
connectAttr "Mouth9Ro_L_Mdv.oz" "unitConversion126.i";
connectAttr "unitConversion127.o" "Mouth2Ro_L_Mdv.i1x";
connectAttr "unitConversion128.o" "Mouth2Ro_L_Mdv.i1y";
connectAttr "unitConversion129.o" "Mouth2Ro_L_Mdv.i1z";
connectAttr "Mouth2_L_NrbTmp.rx" "unitConversion127.i";
connectAttr "Mouth2_L_NrbTmp.ry" "unitConversion128.i";
connectAttr "Mouth2_L_NrbTmp.rz" "unitConversion129.i";
connectAttr "Mouth2Ro_L_Mdv.ox" "unitConversion130.i";
connectAttr "Mouth2Ro_L_Mdv.oy" "unitConversion131.i";
connectAttr "Mouth2Ro_L_Mdv.oz" "unitConversion132.i";
connectAttr "unitConversion133.o" "EyeLid12Ro_L_Mdv.i1x";
connectAttr "unitConversion134.o" "EyeLid12Ro_L_Mdv.i1y";
connectAttr "unitConversion135.o" "EyeLid12Ro_L_Mdv.i1z";
connectAttr "EyeLid12_L_NrbTmp.rx" "unitConversion133.i";
connectAttr "EyeLid12_L_NrbTmp.ry" "unitConversion134.i";
connectAttr "EyeLid12_L_NrbTmp.rz" "unitConversion135.i";
connectAttr "EyeLid12Ro_L_Mdv.ox" "unitConversion136.i";
connectAttr "EyeLid12Ro_L_Mdv.oy" "unitConversion137.i";
connectAttr "EyeLid12Ro_L_Mdv.oz" "unitConversion138.i";
connectAttr "unitConversion139.o" "Mouth3Ro_L_Mdv.i1x";
connectAttr "unitConversion140.o" "Mouth3Ro_L_Mdv.i1y";
connectAttr "unitConversion141.o" "Mouth3Ro_L_Mdv.i1z";
connectAttr "Mouth3_L_NrbTmp.rx" "unitConversion139.i";
connectAttr "Mouth3_L_NrbTmp.ry" "unitConversion140.i";
connectAttr "Mouth3_L_NrbTmp.rz" "unitConversion141.i";
connectAttr "Mouth3Ro_L_Mdv.ox" "unitConversion142.i";
connectAttr "Mouth3Ro_L_Mdv.oy" "unitConversion143.i";
connectAttr "Mouth3Ro_L_Mdv.oz" "unitConversion144.i";
connectAttr "unitConversion145.o" "Mouth4Ro_L_Mdv.i1x";
connectAttr "unitConversion146.o" "Mouth4Ro_L_Mdv.i1y";
connectAttr "unitConversion147.o" "Mouth4Ro_L_Mdv.i1z";
connectAttr "Mouth4_L_NrbTmp.rx" "unitConversion145.i";
connectAttr "Mouth4_L_NrbTmp.ry" "unitConversion146.i";
connectAttr "Mouth4_L_NrbTmp.rz" "unitConversion147.i";
connectAttr "Mouth4Ro_L_Mdv.ox" "unitConversion148.i";
connectAttr "Mouth4Ro_L_Mdv.oy" "unitConversion149.i";
connectAttr "Mouth4Ro_L_Mdv.oz" "unitConversion150.i";
connectAttr "unitConversion151.o" "EyeLid11Ro_L_Mdv.i1x";
connectAttr "unitConversion152.o" "EyeLid11Ro_L_Mdv.i1y";
connectAttr "unitConversion153.o" "EyeLid11Ro_L_Mdv.i1z";
connectAttr "EyeLid11_L_NrbTmp.rx" "unitConversion151.i";
connectAttr "EyeLid11_L_NrbTmp.ry" "unitConversion152.i";
connectAttr "EyeLid11_L_NrbTmp.rz" "unitConversion153.i";
connectAttr "EyeLid11Ro_L_Mdv.ox" "unitConversion154.i";
connectAttr "EyeLid11Ro_L_Mdv.oy" "unitConversion155.i";
connectAttr "EyeLid11Ro_L_Mdv.oz" "unitConversion156.i";
connectAttr "unitConversion157.o" "EyeLid10Ro_L_Mdv.i1x";
connectAttr "unitConversion158.o" "EyeLid10Ro_L_Mdv.i1y";
connectAttr "unitConversion159.o" "EyeLid10Ro_L_Mdv.i1z";
connectAttr "EyeLid10_L_NrbTmp.rx" "unitConversion157.i";
connectAttr "EyeLid10_L_NrbTmp.ry" "unitConversion158.i";
connectAttr "EyeLid10_L_NrbTmp.rz" "unitConversion159.i";
connectAttr "EyeLid10Ro_L_Mdv.ox" "unitConversion160.i";
connectAttr "EyeLid10Ro_L_Mdv.oy" "unitConversion161.i";
connectAttr "EyeLid10Ro_L_Mdv.oz" "unitConversion162.i";
connectAttr "unitConversion163.o" "EyeLid9Ro_L_Mdv.i1x";
connectAttr "unitConversion164.o" "EyeLid9Ro_L_Mdv.i1y";
connectAttr "unitConversion165.o" "EyeLid9Ro_L_Mdv.i1z";
connectAttr "EyeLid9_L_NrbTmp.rx" "unitConversion163.i";
connectAttr "EyeLid9_L_NrbTmp.ry" "unitConversion164.i";
connectAttr "EyeLid9_L_NrbTmp.rz" "unitConversion165.i";
connectAttr "EyeLid9Ro_L_Mdv.ox" "unitConversion166.i";
connectAttr "EyeLid9Ro_L_Mdv.oy" "unitConversion167.i";
connectAttr "EyeLid9Ro_L_Mdv.oz" "unitConversion168.i";
connectAttr "unitConversion90.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "unitConversion86.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "unitConversion88.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "EyeLid1Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[3].dn";
connectAttr "unitConversion91.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[4].dn"
		;
connectAttr "unitConversion92.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[5].dn"
		;
connectAttr "unitConversion87.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[6].dn"
		;
connectAttr "unitConversion89.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[7].dn"
		;
connectAttr "unitConversion75.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[8].dn"
		;
connectAttr "unitConversion77.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[9].dn"
		;
connectAttr "unitConversion78.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[10].dn"
		;
connectAttr "Mouth12Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[11].dn"
		;
connectAttr "unitConversion80.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[12].dn"
		;
connectAttr "unitConversion79.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[13].dn"
		;
connectAttr "unitConversion76.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[14].dn"
		;
connectAttr "unitConversion81.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[15].dn"
		;
connectAttr "unitConversion82.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[16].dn"
		;
connectAttr "unitConversion84.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[17].dn"
		;
connectAttr "unitConversion85.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[18].dn"
		;
connectAttr "CheekARo_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[19].dn";
connectAttr "unitConversion83.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[20].dn"
		;
connectAttr "unitConversion40.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[21].dn"
		;
connectAttr "unitConversion44.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[22].dn"
		;
connectAttr "unitConversion37.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[23].dn"
		;
connectAttr "unitConversion54.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[24].dn"
		;
connectAttr "unitConversion50.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[25].dn"
		;
connectAttr "unitConversion58.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[26].dn"
		;
connectAttr "unitConversion38.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[27].dn"
		;
connectAttr "unitConversion46.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[28].dn"
		;
connectAttr "unitConversion36.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[29].dn"
		;
connectAttr "unitConversion41.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[30].dn"
		;
connectAttr "EyeLid7Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[31].dn"
		;
connectAttr "EyeBrowARo_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[32].dn"
		;
connectAttr "unitConversion42.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[33].dn"
		;
connectAttr "EyeLid3Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[34].dn"
		;
connectAttr "unitConversion39.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[35].dn"
		;
connectAttr "unitConversion43.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[36].dn"
		;
connectAttr "unitConversion45.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[37].dn"
		;
connectAttr "unitConversion47.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[38].dn"
		;
connectAttr "unitConversion48.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[39].dn"
		;
connectAttr "unitConversion49.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[40].dn"
		;
connectAttr "unitConversion51.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[41].dn"
		;
connectAttr "unitConversion52.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[42].dn"
		;
connectAttr "unitConversion53.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[43].dn"
		;
connectAttr "Mouth11Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[44].dn"
		;
connectAttr "unitConversion55.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[45].dn"
		;
connectAttr "unitConversion56.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[46].dn"
		;
connectAttr "unitConversion57.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[47].dn"
		;
connectAttr "unitConversion26.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[48].dn"
		;
connectAttr "unitConversion23.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[49].dn"
		;
connectAttr "unitConversion29.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[50].dn"
		;
connectAttr "unitConversion30.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[51].dn"
		;
connectAttr "Mouth10Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[52].dn"
		;
connectAttr "unitConversion33.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[53].dn"
		;
connectAttr "unitConversion34.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[54].dn"
		;
connectAttr "unitConversion35.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[55].dn"
		;
connectAttr "unitConversion25.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[56].dn"
		;
connectAttr "unitConversion20.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[57].dn"
		;
connectAttr "unitConversion22.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[58].dn"
		;
connectAttr "unitConversion28.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[59].dn"
		;
connectAttr "unitConversion31.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[60].dn"
		;
connectAttr "unitConversion32.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[61].dn"
		;
connectAttr "unitConversion21.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[62].dn"
		;
connectAttr "unitConversion24.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[63].dn"
		;
connectAttr "unitConversion19.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[64].dn"
		;
connectAttr "EyeLid5Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[65].dn"
		;
connectAttr "unitConversion27.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[66].dn"
		;
connectAttr "EyeLid6Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[67].dn"
		;
connectAttr "unitConversion5.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[68].dn"
		;
connectAttr "unitConversion6.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[69].dn"
		;
connectAttr "unitConversion7.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[70].dn"
		;
connectAttr "unitConversion9.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[71].dn"
		;
connectAttr "unitConversion10.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[72].dn"
		;
connectAttr "unitConversion12.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[73].dn"
		;
connectAttr "CheekCRo_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[74].dn";
connectAttr "unitConversion13.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[75].dn"
		;
connectAttr "unitConversion15.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[76].dn"
		;
connectAttr "unitConversion17.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[77].dn"
		;
connectAttr "unitConversion16.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[78].dn"
		;
connectAttr "unitConversion18.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[79].dn"
		;
connectAttr "unitConversion11.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[80].dn"
		;
connectAttr "EyeLid4Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[81].dn"
		;
connectAttr "unitConversion8.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[82].dn"
		;
connectAttr "unitConversion14.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[83].dn"
		;
connectAttr "EyeLid3Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[84].dn"
		;
connectAttr "PuffTr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[85].dn";
connectAttr "EyeLid12Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[86].dn"
		;
connectAttr "CheekBTr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[87].dn";
connectAttr "CheekCTr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[88].dn";
connectAttr "EyeLid10Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[89].dn"
		;
connectAttr "EyeLid8Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[90].dn"
		;
connectAttr "EyeLid7Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[91].dn"
		;
connectAttr "unitConversion3.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[92].dn"
		;
connectAttr "EyeLid5Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[93].dn"
		;
connectAttr "Mouth10Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[94].dn"
		;
connectAttr "Mouth5Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[95].dn";
connectAttr "Mouth9Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[96].dn";
connectAttr "EyeBrowBTr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[97].dn"
		;
connectAttr "EyeLid8Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[98].dn"
		;
connectAttr "EyeLid6Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[99].dn"
		;
connectAttr "EyeLid2Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[100].dn"
		;
connectAttr "Mouth12Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[101].dn"
		;
connectAttr "EyeLid1Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[102].dn"
		;
connectAttr "EyeBrowATr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[103].dn"
		;
connectAttr "Mouth11Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[104].dn"
		;
connectAttr "CheekATr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[105].dn"
		;
connectAttr "EyeLid4Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[106].dn"
		;
connectAttr "Mouth7Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[107].dn"
		;
connectAttr "EyeBrowCTr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[108].dn"
		;
connectAttr "Mouth2Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[109].dn"
		;
connectAttr "Mouth3Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[110].dn"
		;
connectAttr "Mouth4Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[111].dn"
		;
connectAttr "EyeLid11Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[112].dn"
		;
connectAttr "EyeLid9Tr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[113].dn"
		;
connectAttr "unitConversion1.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[114].dn"
		;
connectAttr "unitConversion2.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[115].dn"
		;
connectAttr "unitConversion4.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[116].dn"
		;
connectAttr "EyeLid2Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[117].dn"
		;
connectAttr "unitConversion73.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[118].dn"
		;
connectAttr "unitConversion74.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[119].dn"
		;
connectAttr "unitConversion61.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[120].dn"
		;
connectAttr "unitConversion63.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[121].dn"
		;
connectAttr "unitConversion64.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[122].dn"
		;
connectAttr "PuffRo_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[123].dn";
connectAttr "unitConversion62.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[124].dn"
		;
connectAttr "unitConversion65.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[125].dn"
		;
connectAttr "unitConversion67.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[126].dn"
		;
connectAttr "unitConversion68.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[127].dn"
		;
connectAttr "unitConversion69.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[128].dn"
		;
connectAttr "unitConversion71.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[129].dn"
		;
connectAttr "unitConversion66.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[130].dn"
		;
connectAttr "unitConversion60.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[131].dn"
		;
connectAttr "CheekBRo_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[132].dn"
		;
connectAttr "unitConversion72.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[133].dn"
		;
connectAttr "unitConversion70.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[134].dn"
		;
connectAttr "unitConversion59.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[135].dn"
		;
connectAttr "EyeBrowA_L_NrbTmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[136].dn"
		;
connectAttr "lambert2SG.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[137].dn";
connectAttr "EyeBrowA_L_NrbTmpShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[138].dn"
		;
connectAttr "EyeBrowA_R_NrbTmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[139].dn"
		;
connectAttr "unitConversion113.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[140].dn"
		;
connectAttr "unitConversion125.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[141].dn"
		;
connectAttr "Mouth2Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[142].dn"
		;
connectAttr "unitConversion120.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[143].dn"
		;
connectAttr "unitConversion135.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[144].dn"
		;
connectAttr "unitConversion137.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[145].dn"
		;
connectAttr "unitConversion140.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[146].dn"
		;
connectAttr "unitConversion99.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[147].dn"
		;
connectAttr "EyeBrowCRo_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[148].dn"
		;
connectAttr "EyeBrowBRo_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[149].dn"
		;
connectAttr "unitConversion115.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[150].dn"
		;
connectAttr "unitConversion126.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[151].dn"
		;
connectAttr "unitConversion133.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[152].dn"
		;
connectAttr "unitConversion134.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[153].dn"
		;
connectAttr "unitConversion93.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[154].dn"
		;
connectAttr "unitConversion94.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[155].dn"
		;
connectAttr "unitConversion110.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[156].dn"
		;
connectAttr "unitConversion100.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[157].dn"
		;
connectAttr "unitConversion112.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[158].dn"
		;
connectAttr "unitConversion104.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[159].dn"
		;
connectAttr "Mouth5Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[160].dn"
		;
connectAttr "unitConversion102.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[161].dn"
		;
connectAttr "unitConversion107.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[162].dn"
		;
connectAttr "Mouth7Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[163].dn"
		;
connectAttr "unitConversion121.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[164].dn"
		;
connectAttr "unitConversion96.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[165].dn"
		;
connectAttr "unitConversion105.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[166].dn"
		;
connectAttr "unitConversion106.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[167].dn"
		;
connectAttr "unitConversion122.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[168].dn"
		;
connectAttr "unitConversion123.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[169].dn"
		;
connectAttr "unitConversion130.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[170].dn"
		;
connectAttr "unitConversion131.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[171].dn"
		;
connectAttr "unitConversion124.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[172].dn"
		;
connectAttr "unitConversion109.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[173].dn"
		;
connectAttr "unitConversion117.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[174].dn"
		;
connectAttr "Mouth3Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[175].dn"
		;
connectAttr "unitConversion98.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[176].dn"
		;
connectAttr "unitConversion95.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[177].dn"
		;
connectAttr "unitConversion139.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[178].dn"
		;
connectAttr "EyeLid12Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[179].dn"
		;
connectAttr "unitConversion103.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[180].dn"
		;
connectAttr "unitConversion101.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[181].dn"
		;
connectAttr "unitConversion108.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[182].dn"
		;
connectAttr "unitConversion111.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[183].dn"
		;
connectAttr "unitConversion116.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[184].dn"
		;
connectAttr "unitConversion118.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[185].dn"
		;
connectAttr "unitConversion114.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[186].dn"
		;
connectAttr "unitConversion119.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[187].dn"
		;
connectAttr "unitConversion127.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[188].dn"
		;
connectAttr "unitConversion97.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[189].dn"
		;
connectAttr "Mouth9Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[190].dn"
		;
connectAttr "unitConversion128.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[191].dn"
		;
connectAttr "unitConversion129.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[192].dn"
		;
connectAttr "unitConversion132.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[193].dn"
		;
connectAttr "unitConversion136.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[194].dn"
		;
connectAttr "unitConversion138.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[195].dn"
		;
connectAttr "unitConversion142.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[196].dn"
		;
connectAttr "unitConversion143.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[197].dn"
		;
connectAttr "unitConversion144.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[198].dn"
		;
connectAttr "Mouth4Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[199].dn"
		;
connectAttr "unitConversion145.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[200].dn"
		;
connectAttr "unitConversion146.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[201].dn"
		;
connectAttr "unitConversion147.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[202].dn"
		;
connectAttr "unitConversion141.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[203].dn"
		;
connectAttr "unitConversion150.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[204].dn"
		;
connectAttr "EyeLid11Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[205].dn"
		;
connectAttr "unitConversion152.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[206].dn"
		;
connectAttr "unitConversion154.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[207].dn"
		;
connectAttr "EyeLid9Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[208].dn"
		;
connectAttr "unitConversion166.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[209].dn"
		;
connectAttr "unitConversion156.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[210].dn"
		;
connectAttr "unitConversion157.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[211].dn"
		;
connectAttr "unitConversion167.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[212].dn"
		;
connectAttr "unitConversion161.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[213].dn"
		;
connectAttr "unitConversion168.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[214].dn"
		;
connectAttr "unitConversion149.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[215].dn"
		;
connectAttr "unitConversion158.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[216].dn"
		;
connectAttr "unitConversion159.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[217].dn"
		;
connectAttr "unitConversion162.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[218].dn"
		;
connectAttr "unitConversion153.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[219].dn"
		;
connectAttr "unitConversion163.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[220].dn"
		;
connectAttr "unitConversion160.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[221].dn"
		;
connectAttr "unitConversion165.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[222].dn"
		;
connectAttr "unitConversion148.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[223].dn"
		;
connectAttr "unitConversion164.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[224].dn"
		;
connectAttr "EyeLid10Ro_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[225].dn"
		;
connectAttr "unitConversion155.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[226].dn"
		;
connectAttr "unitConversion151.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[227].dn"
		;
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert2.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "DetailNrb_defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of DetailNrb.ma
