//Maya ASCII 2015 scene
//Name: HeadDefTmpJnt.ma
//Last modified: Mon, Mar 12, 2018 11:51:12 PM
//Codeset: 1252
requires maya "2015";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2015";
fileInfo "version" "2015";
fileInfo "cutIdentifier" "201402282131-909040";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
createNode joint -n "Head_TmpJnt";
	setAttr ".t" -type "double3" 0 5.6530239985590587 -1.3812444063763665 ;
	setAttr ".radi" 0.25;
createNode joint -n "HeadMid_TmpJnt" -p "Head_TmpJnt";
	setAttr ".t" -type "double3" 0 2.9263652677465419 1.3812444063763665 ;
	setAttr ".radi" 0.25;
createNode joint -n "HeadUprBase_TmpJnt" -p "HeadMid_TmpJnt";
	setAttr ".t" -type "double3" 0 0 -0.5 ;
	setAttr ".radi" 0.25;
createNode joint -n "HeadUprTip_TmpJnt" -p "HeadUprBase_TmpJnt";
	setAttr ".t" -type "double3" 0 2 0 ;
	setAttr ".radi" 0.25;
createNode joint -n "HeadLwrBase_TmpJnt" -p "HeadMid_TmpJnt";
	setAttr ".t" -type "double3" 0 0 0.5 ;
	setAttr ".radi" 0.25;
createNode joint -n "HeadLwrTip_TmpJnt" -p "HeadLwrBase_TmpJnt";
	setAttr ".t" -type "double3" 0 -2 0 ;
	setAttr ".radi" 0.25;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr "Head_TmpJnt.s" "HeadMid_TmpJnt.is";
connectAttr "HeadMid_TmpJnt.s" "HeadUprBase_TmpJnt.is";
connectAttr "HeadUprBase_TmpJnt.s" "HeadUprTip_TmpJnt.is";
connectAttr "HeadMid_TmpJnt.s" "HeadLwrBase_TmpJnt.is";
connectAttr "HeadLwrBase_TmpJnt.s" "HeadLwrTip_TmpJnt.is";
// End of HeadDefTmpJnt.ma
