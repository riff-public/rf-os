//Maya ASCII 2018ff09 scene
//Name: EyeCtrl.ma
//Last modified: Mon, Jan 13, 2020 06:26:48 PM
//Codeset: 1252
requires maya "2018ff09";
requires -nodeType "RedshiftOptions" -nodeType "RedshiftPostEffects" "redshift4maya" "2.6.43";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201903222215-65bada0e52";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "A314156C-47CE-C443-3C38-1FA7A4D934C6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.9804652712693049 13.631062115464545 3.2616000248072337 ;
	setAttr ".r" -type "double3" -26.738352729604586 42.600000000000513 2.1604179238833138e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "5875061E-4C88-DD5C-BF8B-92A0F6E0F128";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 3.0552054103091328;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.73815861587319309 12.173351363070683 1.0706577569733853 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "171E05B5-4876-E20B-D2C0-05A924B38B62";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "BBA78F1A-4D1D-6295-87A9-8F85C0E9CF49";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 9.0011155200000044;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "B88253D3-4725-1C49-5306-6F895B7ADE15";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.013148067394595689 12.190500704141785 1000.1743735783393 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "4E3EF5A0-4658-04D6-3D00-87BB385C9D8B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 999.10489072115581;
	setAttr ".ow" 1.2651864576;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" -0.013148067394595689 12.190500704141785 1.0694828571834751 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "88B6326C-4B2D-91D6-A078-C3A44BCCBDF3";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "3E50236E-47D3-A4E0-C2E3-21B26C7CA2E3";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "EyeRigCtrl_C_Grp";
	rename -uid "A244AB38-4FA4-F7B4-EA81-0C816DD347F3";
	setAttr ".t" -type "double3" 0 11.826261201700408 -0.33645801058560054 ;
	setAttr ".s" -type "double3" 0.344 0.344 0.344 ;
createNode transform -n "EyeBshCtrlZro_C_Grp" -p "EyeRigCtrl_C_Grp";
	rename -uid "DCDD229E-425D-7B94-0B48-299FCB603C48";
createNode transform -n "EyeLidsBshCtrlZro_C_Grp" -p "EyeBshCtrlZro_C_Grp";
	rename -uid "B9BB0260-426D-DF1A-6A94-FCBA67B30607";
	setAttr ".t" -type "double3" 0 1.0513126839743592 4.0870374063054529 ;
	setAttr ".r" -type "double3" 0 0 -90.000000000000028 ;
	setAttr ".s" -type "double3" 0.76442252294160862 0.76442252294160862 0.76442252294160862 ;
createNode transform -n "EyeLidsBsh_C_Ctrl" -p "EyeLidsBshCtrlZro_C_Grp";
	rename -uid "9728AFEA-4FD6-8523-5280-7A9EEB455991";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 1.9984014443252865e-15 ;
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mntl" -type "double3" -0.5 -0.5 -1 ;
	setAttr ".mxtl" -type "double3" 0.5 0.5 1 ;
	setAttr ".mtxe" yes;
	setAttr ".mtye" yes;
	setAttr ".xtxe" yes;
	setAttr ".xtye" yes;
	setAttr ".mnrl" -type "double3" -45 -45 -29.999999999999996 ;
	setAttr ".mxrl" -type "double3" 45 45 29.999999999999996 ;
	setAttr ".mrze" yes;
	setAttr ".xrze" yes;
createNode nurbsCurve -n "EyeLidsBsh_C_CtrlShape" -p "EyeLidsBsh_C_Ctrl";
	rename -uid "2B3554F9-4516-4BB5-CC54-DCACAB76291A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 43 0 no 3
		44 1 2 3 4 5 6 7 8 9 10 11 13 14 15 16 17 18 19 20 21 22 23 25 26 27 28 29
		 30 31 32 33 34 35 37 38 39 40 41 42 43 44 45 46 47
		44
		0.99082340927807466 -0.0017238739503836536 -1.2884893724789053e-15
		0.97863953267814729 0.10263594706333978 -1.2884893724789053e-15
		0.94203228051417787 0.206122011050896 -1.3717560993257921e-15
		0.88190035056062344 0.30449047168986537 -1.3301227359023487e-15
		0.79972665298133272 0.39531878993784086 -1.3440005237101632e-15
		0.6975334141544256 0.4763711781305533 -1.2607337968632764e-15
		0.57783626453542125 0.5456506559529527 -1.3162449480945343e-15
		0.44358432768055661 0.60145180510041407 -1.3162449480945343e-15
		0.29808228877170007 0.64240140357924835 -1.3717560993257921e-15
		0.14491344717664748 0.66748960772321453 -1.3717560993257921e-15
		-0.012151232110831241 0.67610048281520874 -1.2884893724789053e-15
		-0.012151232110831241 0.67610048281520874 -1.2884893724789053e-15
		-0.1692441046584463 0.66802025730676184 -1.3717560993257921e-15
		-0.32249682593434686 0.64344963573808844 -1.3717560993257921e-15
		-0.46813637169426425 0.60299181012848824 -1.3162449480945343e-15
		-0.60257605153442295 0.54764451317610707 -1.3162449480945343e-15
		-0.72250656007195024 0.47876979505553285 -1.2607337968632764e-15
		-0.82497302928548877 0.39806310152527835 -1.3440005237101632e-15
		-0.90745309681756015 0.30751290312950841 -1.3301227359023487e-15
		-0.96791699482670379 0.20934814323085899 -1.3717560993257921e-15
		-1.0048736377193508 0.10598633743810898 -1.2884893724789053e-15
		-1.0174157258463279 -0.0017238400288297209 -1.2884893724789053e-15
		-1.0174157258463279 -0.0017238400288297209 -1.2884893724789053e-15
		-1.0052318492464003 -0.10608366104255318 -1.2884893724789053e-15
		-0.96862459708243154 -0.20956972503010945 -1.2052226456320186e-15
		-0.90849266712887766 -0.3079381856690781 -1.246856009055462e-15
		-0.82631896954958606 -0.39876650391705376 -1.2329782212476475e-15
		-0.72412573072267949 -0.47981889210976547 -1.3162449480945343e-15
		-0.60442858110367603 -0.54909836993216576 -1.2607337968632764e-15
		-0.47017664424881295 -0.60489951907962725 -1.2607337968632764e-15
		-0.32467460533995612 -0.64584911755846131 -1.2052226456320186e-15
		-0.17150576374490309 -0.67093732170242815 -1.2052226456320186e-15
		-0.01444108445742443 -0.67954819679442102 -1.2884893724789053e-15
		-0.01444108445742443 -0.67954819679442102 -1.2884893724789053e-15
		0.14265178809019027 -0.67146797128597357 -1.2052226456320186e-15
		0.29590450936609058 -0.64689734971730128 -1.2052226456320186e-15
		0.44154405512600986 -0.60643952410770174 -1.2607337968632764e-15
		0.57598373496616906 -0.55109222715532058 -1.2607337968632764e-15
		0.69591424350369657 -0.48221750903474625 -1.3162449480945343e-15
		0.79838071271723521 -0.40151081550449153 -1.2329782212476475e-15
		0.88086078024930725 -0.31096061710872142 -1.246856009055462e-15
		0.94132467825844968 -0.21279585721007216 -1.2052226456320186e-15
		0.978281321151099 -0.10943405141732243 -1.2884893724789053e-15
		0.99082340927807466 -0.0017238739503836536 -1.2884893724789053e-15
		;
createNode transform -n "EyeLidsUprBshCtrlZro_C_Grp" -p "EyeLidsBsh_C_Ctrl";
	rename -uid "C11E1FCF-404F-D9E5-3688-FE93C5AEE63D";
	setAttr ".t" -type "double3" -6.802207541655088e-15 0.90584864387993802 8.4440016262884263e-16 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "EyeLidsUprBsh_C_Ctrl" -p "EyeLidsUprBshCtrlZro_C_Grp";
	rename -uid "7F159033-4408-036C-165C-FEA1C147CEAA";
	addAttr -ci true -sn "eyeLids" -ln "eyeLids" -at "double";
	addAttr -ci true -sn "lidsInUD" -ln "lidsInUD" -min -10 -max 10 -at "double";
	addAttr -ci true -sn "lidsMidUD" -ln "lidsMidUD" -min -10 -max 10 -at "double";
	addAttr -ci true -sn "lidsOutUD" -ln "lidsOutUD" -min -10 -max 10 -at "double";
	addAttr -ci true -sn "lidsTurnFC" -ln "lidsTurnFC" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -3.9968028886505493e-15 0 -3.7747582837255449e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mntl" -type "double3" -1 -0.5 -1 ;
	setAttr ".mxtl" -type "double3" 1 1.5 1 ;
	setAttr ".mtye" yes;
	setAttr ".xtye" yes;
	setAttr -l on -k on ".eyeLids";
	setAttr -k on ".lidsInUD";
	setAttr -k on ".lidsMidUD";
	setAttr -k on ".lidsOutUD";
	setAttr -k on ".lidsTurnFC";
createNode nurbsCurve -n "EyeLidsUprBsh_C_CtrlShape" -p "EyeLidsUprBsh_C_Ctrl";
	rename -uid "C8A05D11-4A92-E46E-C378-3EB4D91B81D0";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 43 0 no 3
		44 1 2 3 4 5 6 7 8 9 10 11 13 14 15 16 17 18 19 20 21 22 23 25 26 27 28 29
		 30 31 32 33 34 35 37 38 39 40 41 42 43 44 45 46 47
		44
		-0.099443098422977758 0.049217281738067047 9.8403619619843158e-18
		-0.092287945769431265 0.023419250570031001 9.8403619619843158e-18
		-0.082408809080342066 -0.0017435156386027032 6.8219605696323463e-19
		-0.071017872932817444 -0.025651518587196609 5.2612790094737782e-18
		-0.058813098207550783 -0.047715972112498825 3.73491802530359e-18
		-0.046489269782594551 -0.067393747667264886 1.2893083930324671e-17
		-0.033511008795473571 -0.08420002049447349 6.7876399936439502e-18
		-0.02245657412952675 -0.097721080037575056 6.7876399936439502e-18
		-0.010478509216864247 -0.10762418410221934 6.8219605696323463e-19
		0.002128200246792054 -0.11366511371357876 6.8219605696323463e-19
		0.015053181585218673 -0.11569556373764578 9.8403619619843158e-18
		0.015053181585218673 -0.11569556373764578 9.8403619619843158e-18
		0.027978162923645227 -0.11366511371357875 6.8219605696323463e-19
		0.040584872387301497 -0.1076241841022193 6.8219605696323463e-19
		0.052562937299963974 -0.097721080037575056 6.7876399936439502e-18
		0.063617371965910802 -0.084200020494473449 6.7876399936439502e-18
		0.076595632953031789 -0.067393747667264844 1.2893083930324671e-17
		0.088919461377988354 -0.047715972112498894 3.73491802530359e-18
		0.10112423610325491 -0.025651518587196623 5.2612790094737782e-18
		0.11251517225077952 -0.0017435156386026685 6.8219605696323463e-19
		0.12239430893986865 0.023419250570030997 9.8403619619843158e-18
		0.12954946159341499 0.049217281738067054 9.8403619619843158e-18
		0.12954946159341499 0.049217281738067054 9.8403619619843158e-18
		0.13172022838296776 0.066122742851700014 9.8403619619843158e-18
		0.1289393114043961 0.075059355992172491 1.8998527867005405e-17
		0.12030006650564966 0.082035537702758066 1.4419444914494843e-17
		0.1106153975020034 0.08691836851466761 1.5945805898665024e-17
		0.098577643434162476 0.091273031034565272 6.7876399936439502e-18
		0.084483130916970225 0.094992234263412537 1.2893083930324671e-17
		0.06867912321641173 0.097984424669171524 1.2893083930324671e-17
		0.05155464459421237 0.10017596689816607 1.8998527867005405e-17
		0.033531422289043225 0.10151281565379476 1.8998527867005405e-17
		0.015053181585218629 0.10196215123005004 9.8403619619843158e-18
		0.015053181585218629 0.10196215123005004 9.8403619619843158e-18
		-0.0034250591186059717 0.10151281565379475 1.8998527867005405e-17
		-0.021448281423775167 0.1001759668981661 1.8998527867005405e-17
		-0.038572760045974618 0.097984424669171469 1.2893083930324671e-17
		-0.05437676774653296 0.094992234263412495 1.2893083930324671e-17
		-0.068471280263725093 0.091273031034565272 6.7876399936439502e-18
		-0.080509034331566032 0.086918368514667624 1.5945805898665024e-17
		-0.090193703335212233 0.08203553770275808 1.4419444914494843e-17
		-0.098832948233958981 0.075059355992172505 1.8998527867005405e-17
		-0.10161386521253052 0.06612274285170007 9.8403619619843158e-18
		-0.099443098422977758 0.049217281738067047 9.8403619619843158e-18
		;
createNode transform -n "EyeLidsLwrBshCtrlZro_C_Grp" -p "EyeLidsBsh_C_Ctrl";
	rename -uid "20C0CB17-4F1A-842A-608F-66B546388A45";
	setAttr ".t" -type "double3" 0 -0.9 0 ;
createNode transform -n "EyeLidsLwrBsh_C_Ctrl" -p "EyeLidsLwrBshCtrlZro_C_Grp";
	rename -uid "97486923-4913-0BB8-FB9D-B39FE638B05F";
	addAttr -ci true -sn "eyeLids" -ln "eyeLids" -at "double";
	addAttr -ci true -sn "lidsInUD" -ln "lidsInUD" -min -10 -max 10 -at "double";
	addAttr -ci true -sn "lidsMidUD" -ln "lidsMidUD" -min -10 -max 10 -at "double";
	addAttr -ci true -sn "lidsOutUD" -ln "lidsOutUD" -min -10 -max 10 -at "double";
	addAttr -ci true -sn "lidsTurnFC" -ln "lidsTurnFC" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mntl" -type "double3" -1 -0.5 -1 ;
	setAttr ".mxtl" -type "double3" 1 1.5 1 ;
	setAttr ".mtye" yes;
	setAttr ".xtye" yes;
	setAttr -l on -k on ".eyeLids";
	setAttr -k on ".lidsInUD";
	setAttr -k on ".lidsMidUD";
	setAttr -k on ".lidsOutUD";
	setAttr -k on ".lidsTurnFC";
createNode nurbsCurve -n "EyeLidsLwrBsh_C_CtrlShape" -p "EyeLidsLwrBsh_C_Ctrl";
	rename -uid "A255AC8E-45C2-E587-9EA8-22B27C03B8E4";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 43 0 no 3
		44 1 2 3 4 5 6 7 8 9 10 11 13 14 15 16 17 18 19 20 21 22 23 25 26 27 28 29
		 30 31 32 33 34 35 37 38 39 40 41 42 43 44 45 46 47
		44
		-0.099443098422977758 0.049217281738067047 9.8403619619843158e-18
		-0.092287945769431265 0.023419250570031001 9.8403619619843158e-18
		-0.082408809080342066 -0.0017435156386027032 6.8219605696323463e-19
		-0.071017872932817444 -0.025651518587196609 5.2612790094737782e-18
		-0.058813098207550783 -0.047715972112498825 3.73491802530359e-18
		-0.046489269782594551 -0.067393747667264886 1.2893083930324671e-17
		-0.033511008795473571 -0.08420002049447349 6.7876399936439502e-18
		-0.02245657412952675 -0.097721080037575056 6.7876399936439502e-18
		-0.010478509216864247 -0.10762418410221934 6.8219605696323463e-19
		0.002128200246792054 -0.11366511371357876 6.8219605696323463e-19
		0.015053181585218673 -0.11569556373764578 9.8403619619843158e-18
		0.015053181585218673 -0.11569556373764578 9.8403619619843158e-18
		0.027978162923645227 -0.11366511371357875 6.8219605696323463e-19
		0.040584872387301497 -0.1076241841022193 6.8219605696323463e-19
		0.052562937299963974 -0.097721080037575056 6.7876399936439502e-18
		0.063617371965910802 -0.084200020494473449 6.7876399936439502e-18
		0.076595632953031789 -0.067393747667264844 1.2893083930324671e-17
		0.088919461377988354 -0.047715972112498894 3.73491802530359e-18
		0.10112423610325491 -0.025651518587196623 5.2612790094737782e-18
		0.11251517225077952 -0.0017435156386026685 6.8219605696323463e-19
		0.12239430893986865 0.023419250570030997 9.8403619619843158e-18
		0.12954946159341499 0.049217281738067054 9.8403619619843158e-18
		0.12954946159341499 0.049217281738067054 9.8403619619843158e-18
		0.13172022838296776 0.066122742851700014 9.8403619619843158e-18
		0.1289393114043961 0.075059355992172491 1.8998527867005405e-17
		0.12030006650564966 0.082035537702758066 1.4419444914494843e-17
		0.1106153975020034 0.08691836851466761 1.5945805898665024e-17
		0.098577643434162476 0.091273031034565272 6.7876399936439502e-18
		0.084483130916970225 0.094992234263412537 1.2893083930324671e-17
		0.06867912321641173 0.097984424669171524 1.2893083930324671e-17
		0.05155464459421237 0.10017596689816607 1.8998527867005405e-17
		0.033531422289043225 0.10151281565379476 1.8998527867005405e-17
		0.015053181585218629 0.10196215123005004 9.8403619619843158e-18
		0.015053181585218629 0.10196215123005004 9.8403619619843158e-18
		-0.0034250591186059717 0.10151281565379475 1.8998527867005405e-17
		-0.021448281423775167 0.1001759668981661 1.8998527867005405e-17
		-0.038572760045974618 0.097984424669171469 1.2893083930324671e-17
		-0.05437676774653296 0.094992234263412495 1.2893083930324671e-17
		-0.068471280263725093 0.091273031034565272 6.7876399936439502e-18
		-0.080509034331566032 0.086918368514667624 1.5945805898665024e-17
		-0.090193703335212233 0.08203553770275808 1.4419444914494843e-17
		-0.098832948233958981 0.075059355992172505 1.8998527867005405e-17
		-0.10161386521253052 0.06612274285170007 9.8403619619843158e-18
		-0.099443098422977758 0.049217281738067047 9.8403619619843158e-18
		;
createNode transform -n "EyeLidsBshJntZro_C_Grp" -p "EyeRigCtrl_C_Grp";
	rename -uid "6D1B6085-4385-841C-57A7-EDA56131BB5C";
	setAttr ".t" -type "double3" 0 1.7763568394002505e-15 0 ;
createNode transform -n "EyeLidsBshJntZro_C_Grp" -p "|EyeRigCtrl_C_Grp|EyeLidsBshJntZro_C_Grp";
	rename -uid "CC08F2E3-4214-569E-9FB3-068031AF7453";
	setAttr ".t" -type "double3" 0 1.1197648206695874 2.6524512754545162 ;
createNode joint -n "EyeLidsBsh_C_Jnt" -p "|EyeRigCtrl_C_Grp|EyeLidsBshJntZro_C_Grp|EyeLidsBshJntZro_C_Grp";
	rename -uid "52198082-40C2-C8BA-65DC-2FA3A36B8271";
	addAttr -ci true -sn "eyelidsFollow" -ln "eyelidsFollow" -min 0 -max 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".radi" 0.5;
	setAttr -k on ".eyelidsFollow" 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "163E2C48-4949-3FB8-AB30-56BFCADCBC4D";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "F93C6C1C-424F-5129-2D33-2E9414D07310";
	setAttr ".bsdt[0].bscd" -type "Int32Array" 0 ;
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "E72C48F5-4018-FADC-9E59-1E8FD990EBB0";
createNode displayLayerManager -n "layerManager";
	rename -uid "1240FD93-40D6-13CE-7BF5-03AC1F6C7A0A";
createNode displayLayer -n "defaultLayer";
	rename -uid "175F62AD-4380-8798-1C93-86907A7C0E5F";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "9E3CB267-4523-7FDB-8CAC-BCB60673619C";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "A1FF5408-4BE6-47C6-98DD-E59D95F8DCE0";
	setAttr ".g" yes;
createNode RedshiftOptions -s -n "redshiftOptions";
	rename -uid "9B809307-4892-557A-5EB7-E39AD1FC35B8";
createNode RedshiftPostEffects -n "defaultRedshiftPostEffects";
	rename -uid "C1A957DF-4C55-0E6B-5E9E-2C98B1B253B0";
	setAttr ".clrMgmtDisplayMode" -type "string" "RS_COLORMANAGEMENTDISPLAYMODE_SRGB";
	setAttr -s 2 ".cr[1]" -type "float2" 1 1;
	setAttr -s 2 ".cg[1]" -type "float2" 1 1;
	setAttr -s 2 ".cb[1]" -type "float2" 1 1;
	setAttr -s 2 ".cl[1]" -type "float2" 1 1;
createNode plusMinusAverage -n "FacialCtrlMover_EyeLidsLwrUp_C_Pma";
	rename -uid "9BE52433-458F-F547-BD5C-6D869CC6DCB2";
	setAttr -s 2 ".i1[1]"  0;
createNode multiplyDivide -n "FacialCtrlMover_EyeLidsLwrUD_C_Mdv";
	rename -uid "91074102-4FA8-B1FD-AA4D-8B9FD97970D8";
	setAttr ".i2" -type "float3" 0.72000003 -2 1 ;
createNode clamp -n "FacialCtrlMover_EyeLidsLwrUD_C_Cmp";
	rename -uid "994551C2-46E2-CB49-30D7-3BB0F88B3DE4";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 1.4 0 0 ;
createNode plusMinusAverage -n "FacialCtrlMover_EyeLidsLwrDn_C_Pma";
	rename -uid "86B5E39E-4555-D786-5963-2D9BB00EEACC";
	setAttr -s 2 ".i1[1]"  0;
createNode multiplyDivide -n "EyeLidsUprTurnFC_C_Mdv";
	rename -uid "0C22B5B4-434C-FF38-B99B-99A146BBAC3E";
	setAttr ".i2" -type "float3" -0.1 0.1 1 ;
createNode clamp -n "EyeLidsUprTurnFC_C_Cmp";
	rename -uid "8AAAD717-469A-1DAC-F64A-D08B3BF2D947";
	setAttr ".mn" -type "float3" -10 0 0 ;
	setAttr ".mx" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "EyeLidsUprOutUD_C_Mdv";
	rename -uid "07A3D4B5-430A-D50F-3822-85A42B09F746";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsUprOutUD_C_Cmp";
	rename -uid "F0F10210-48F1-6114-5B6D-23B12CAEE30A";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsUprMidUD_C_Mdv";
	rename -uid "518533E9-4750-E4E0-40BA-B5B0A51F1F9C";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsUprMidUD_C_Cmp";
	rename -uid "23DDC1DE-4612-582C-0C48-83A708D77B84";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsUprInUD_C_Mdv";
	rename -uid "E1482ECF-49F8-F6F5-9D1F-D29E474504C2";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsUprInUD_C_Cmp";
	rename -uid "63B479E7-465E-42E7-1A59-539DF42E3D01";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsLwrTurnFC_C_Mdv";
	rename -uid "78B7A310-488D-11B6-3FED-20964D4F0990";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsLwrTurnFC_C_Cmp";
	rename -uid "BBEABF94-474E-8B58-0DBF-9E8FBEBFB20D";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsLwrOutUD_C_Mdv";
	rename -uid "34062926-414E-BDF4-F47C-E8B9FEEF5284";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsLwrOutUD_C_Cmp";
	rename -uid "0CE4D591-49A3-1FED-18DB-2BADE488467C";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsLwrMidUD_C_Mdv";
	rename -uid "9A92EBC6-4E55-76B3-7FA1-12B0952F63AE";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsLwrMidUD_C_Cmp";
	rename -uid "DE29BFB5-40C8-4CAE-BAB2-F89D43452F62";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsLwrInUD_C_Mdv";
	rename -uid "8457D718-4E34-5890-2A1C-AEAA93DB96B0";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsLwrInUD_C_Cmp";
	rename -uid "DF6AE65B-439C-7060-B43B-18B6C74C9DB6";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeBallUD_C_Mdv";
	rename -uid "1FD2BF6A-43C6-C024-AE1D-C9A1300B4DA9";
	setAttr ".i2" -type "float3" 2 -2 1 ;
createNode clamp -n "EyeBallUD_C_Cmp";
	rename -uid "47A6ED39-4546-074E-6591-1993CE634391";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 0.5 0 0 ;
createNode multiplyDivide -n "EyeBallFC_C_Mdv";
	rename -uid "91DBC03C-49D7-CC9A-B7BB-A6B4D58D85D6";
	setAttr ".i2" -type "float3" -0.03334 0.03334 1 ;
createNode clamp -n "EyeBallFC_C_Cmp";
	rename -uid "C3B9E30F-4891-48D4-C52B-13A79B92A681";
	setAttr ".mn" -type "float3" -30 0 0 ;
	setAttr ".mx" -type "float3" 0 30 0 ;
createNode unitConversion -n "unitConversion200";
	rename -uid "1E617043-43C3-C687-A650-13B8358831B1";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion199";
	rename -uid "3DFD0264-4B1A-5D32-F526-469C78E8E516";
	setAttr ".cf" 57.295779513082323;
createNode clamp -n "EyeLidsUprAmpUD_C_Cmp";
	rename -uid "ACF0E3CD-4C45-3BF3-68A2-519595C318D7";
	setAttr ".mn" -type "float3" -1 -1 -1 ;
	setAttr ".mx" -type "float3" 1 1 1 ;
createNode plusMinusAverage -n "EyeLidsUprDn_C_Pma";
	rename -uid "E71F5648-475B-6261-34FE-309F38F90BB5";
	setAttr -s 3 ".i1[1:2]"  0 0;
	setAttr -s 2 ".i1";
createNode multiplyDivide -n "EyeLidsFollowUprUD_C_Mdv";
	rename -uid "2DC7B507-44A5-65BD-5F72-56B56BEA8DF7";
	setAttr ".i2" -type "float3" -0.029999999 0.02 1 ;
createNode clamp -n "EyeLidsFollowUprUD_C_Cmp";
	rename -uid "30EF3B07-4E40-D1B0-BAD9-09B46125500A";
	setAttr ".mn" -type "float3" -100 0 0 ;
	setAttr ".mx" -type "float3" 0 100 0 ;
createNode blendColors -n "EyeLidsFollowUprUD_C_Bcl";
	rename -uid "5CC7B144-4658-2DA9-DEB2-9E8137DA7C81";
createNode unitConversion -n "unitConversion211";
	rename -uid "C0494199-4CB7-1612-AEC9-09B62BF55C1D";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion212";
	rename -uid "62329E4E-400C-D90F-8794-3C94F0F46FBF";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "EyeLidsUprUD_C_Mdv";
	rename -uid "64EDEBA4-4FC9-0175-BC25-FC891643F1D3";
	setAttr ".i2" -type "float3" -2 0.72000003 1 ;
createNode clamp -n "EyeLidsUprUD_C_Cmp";
	rename -uid "8ED3DB0D-48CC-819E-6C02-3C8A0CEE8694";
	setAttr ".mn" -type "float3" -0.5 0 0 ;
	setAttr ".mx" -type "float3" 0 1.4 0 ;
createNode plusMinusAverage -n "EyeLidsUprUp_C_Pma";
	rename -uid "AF2CA0BC-4C2C-AEF4-4B6B-BDBC629A23EB";
	setAttr -s 3 ".i1[1:2]"  0 0;
	setAttr -s 2 ".i1";
createNode clamp -n "EyeLidsLwrAllUD_C_Cmp";
	rename -uid "4E1E6D17-4B03-8F81-E399-109BA8B617AE";
	setAttr ".mx" -type "float3" 1 1 0 ;
createNode clamp -n "EyeBallAmpIO_C_Cmp";
	rename -uid "8097D19D-43E4-20D8-79D7-A0ACE40F3717";
	setAttr ".mn" -type "float3" -1 -1 -1 ;
	setAttr ".mx" -type "float3" 1 1 1 ;
createNode plusMinusAverage -n "EyeBallIn_C_Pma";
	rename -uid "6A994DE6-4E2C-184D-89B2-75BE58339DE4";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode multiplyDivide -n "EyeBallFollowIO_C_Mdv";
	rename -uid "9B17D234-4627-A4FD-7B7C-909A6F3C862E";
	setAttr ".i2" -type "float3" -0.0049999999 0.0049999999 1 ;
createNode clamp -n "EyeBallFollowIO_C_Cmp";
	rename -uid "7A8A2969-4ED8-3D16-1643-97B66ACF30BA";
	setAttr ".mn" -type "float3" -300 0 0 ;
	setAttr ".mx" -type "float3" 0 300 0 ;
createNode blendColors -n "EyeBallFollowIO_C_Bcl";
	rename -uid "173F5E11-44F1-7EA0-1347-339336270B80";
createNode unitConversion -n "unitConversion215";
	rename -uid "6A677F33-4603-4EB6-2767-D1BB400D65BE";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion216";
	rename -uid "50BB1B1F-4D04-F816-8862-D3A2275B5225";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "EyeBallIO_C_Mdv";
	rename -uid "9730BAE9-499E-A311-5B8F-FD9224E8009F";
	setAttr ".i2" -type "float3" -2 2 1 ;
createNode clamp -n "EyeBallIO_C_Cmp";
	rename -uid "46B497A3-452A-28E9-7E0C-16BFB928BE48";
	setAttr ".mn" -type "float3" -0.5 0 0 ;
	setAttr ".mx" -type "float3" 0 0.5 0 ;
createNode plusMinusAverage -n "EyeBallOut_C_Pma";
	rename -uid "9DD7556B-4218-C2EF-F334-25919A900EAE";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "8BC1A9B4-4DD6-44CA-FB78-21BCE1759C7C";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode nodeGraphEditorInfo -n "MayaNodeEditorSavedTabsInfo";
	rename -uid "67CD7F90-4D47-D166-11C3-B3BC73FB1114";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -2828.5444588524833 5699.4484742887225 ;
	setAttr ".tgi[0].vh" -type "double2" 5305.2761130814806 11514.448177083525 ;
	setAttr -s 54 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 1887.142822265625;
	setAttr ".tgi[0].ni[0].y" 10115.7138671875;
	setAttr ".tgi[0].ni[0].nvs" 18304;
	setAttr ".tgi[0].ni[1].x" 955.71429443359375;
	setAttr ".tgi[0].ni[1].y" 10041.4287109375;
	setAttr ".tgi[0].ni[1].nvs" 18304;
	setAttr ".tgi[0].ni[2].x" 341.42855834960938;
	setAttr ".tgi[0].ni[2].y" 9694.2861328125;
	setAttr ".tgi[0].ni[2].nvs" 18304;
	setAttr ".tgi[0].ni[3].x" 1580;
	setAttr ".tgi[0].ni[3].y" 9964.2861328125;
	setAttr ".tgi[0].ni[3].nvs" 18304;
	setAttr ".tgi[0].ni[4].x" 1887.142822265625;
	setAttr ".tgi[0].ni[4].y" 9121.4287109375;
	setAttr ".tgi[0].ni[4].nvs" 18304;
	setAttr ".tgi[0].ni[5].x" 1887.142822265625;
	setAttr ".tgi[0].ni[5].y" 9964.2861328125;
	setAttr ".tgi[0].ni[5].nvs" 18304;
	setAttr ".tgi[0].ni[6].x" 955.71429443359375;
	setAttr ".tgi[0].ni[6].y" 9825.7138671875;
	setAttr ".tgi[0].ni[6].nvs" 18304;
	setAttr ".tgi[0].ni[7].x" 1511.4285888671875;
	setAttr ".tgi[0].ni[7].y" 9020;
	setAttr ".tgi[0].ni[7].nvs" 18304;
	setAttr ".tgi[0].ni[8].x" 1268.5714111328125;
	setAttr ".tgi[0].ni[8].y" 9611.4287109375;
	setAttr ".tgi[0].ni[8].nvs" 18304;
	setAttr ".tgi[0].ni[9].x" 648.5714111328125;
	setAttr ".tgi[0].ni[9].y" 9834.2861328125;
	setAttr ".tgi[0].ni[9].nvs" 18304;
	setAttr ".tgi[0].ni[10].x" 1511.4285888671875;
	setAttr ".tgi[0].ni[10].y" 8918.5712890625;
	setAttr ".tgi[0].ni[10].nvs" 18304;
	setAttr ".tgi[0].ni[11].x" 1511.4285888671875;
	setAttr ".tgi[0].ni[11].y" 8614.2861328125;
	setAttr ".tgi[0].ni[11].nvs" 18304;
	setAttr ".tgi[0].ni[12].x" 34.285713195800781;
	setAttr ".tgi[0].ni[12].y" 9897.142578125;
	setAttr ".tgi[0].ni[12].nvs" 18304;
	setAttr ".tgi[0].ni[13].x" 648.5714111328125;
	setAttr ".tgi[0].ni[13].y" 9612.857421875;
	setAttr ".tgi[0].ni[13].nvs" 18304;
	setAttr ".tgi[0].ni[14].x" 752.85711669921875;
	setAttr ".tgi[0].ni[14].y" 8800;
	setAttr ".tgi[0].ni[14].nvs" 18304;
	setAttr ".tgi[0].ni[15].x" 1580;
	setAttr ".tgi[0].ni[15].y" 10065.7138671875;
	setAttr ".tgi[0].ni[15].nvs" 18304;
	setAttr ".tgi[0].ni[16].x" 1268.5714111328125;
	setAttr ".tgi[0].ni[16].y" 10065.7138671875;
	setAttr ".tgi[0].ni[16].nvs" 18304;
	setAttr ".tgi[0].ni[17].x" 1580;
	setAttr ".tgi[0].ni[17].y" 9761.4287109375;
	setAttr ".tgi[0].ni[17].nvs" 18304;
	setAttr ".tgi[0].ni[18].x" 1887.142822265625;
	setAttr ".tgi[0].ni[18].y" 9020;
	setAttr ".tgi[0].ni[18].nvs" 18304;
	setAttr ".tgi[0].ni[19].x" 1887.142822265625;
	setAttr ".tgi[0].ni[19].y" 9862.857421875;
	setAttr ".tgi[0].ni[19].nvs" 18304;
	setAttr ".tgi[0].ni[20].x" 1268.5714111328125;
	setAttr ".tgi[0].ni[20].y" 9510;
	setAttr ".tgi[0].ni[20].nvs" 18304;
	setAttr ".tgi[0].ni[21].x" 341.42855834960938;
	setAttr ".tgi[0].ni[21].y" 10011.4287109375;
	setAttr ".tgi[0].ni[21].nvs" 18304;
	setAttr ".tgi[0].ni[22].x" 1887.142822265625;
	setAttr ".tgi[0].ni[22].y" 9710;
	setAttr ".tgi[0].ni[22].nvs" 18304;
	setAttr ".tgi[0].ni[23].x" 955.71429443359375;
	setAttr ".tgi[0].ni[23].y" 10142.857421875;
	setAttr ".tgi[0].ni[23].nvs" 18304;
	setAttr ".tgi[0].ni[24].x" 1580;
	setAttr ".tgi[0].ni[24].y" 9862.857421875;
	setAttr ".tgi[0].ni[24].nvs" 18304;
	setAttr ".tgi[0].ni[25].x" 1887.142822265625;
	setAttr ".tgi[0].ni[25].y" 8224.2861328125;
	setAttr ".tgi[0].ni[25].nvs" 18304;
	setAttr ".tgi[0].ni[26].x" 1132.857177734375;
	setAttr ".tgi[0].ni[26].y" 8768.5712890625;
	setAttr ".tgi[0].ni[26].nvs" 18304;
	setAttr ".tgi[0].ni[27].x" 341.42855834960938;
	setAttr ".tgi[0].ni[27].y" 9795.7138671875;
	setAttr ".tgi[0].ni[27].nvs" 18304;
	setAttr ".tgi[0].ni[28].x" 1580;
	setAttr ".tgi[0].ni[28].y" 10167.142578125;
	setAttr ".tgi[0].ni[28].nvs" 18304;
	setAttr ".tgi[0].ni[29].x" 1511.4285888671875;
	setAttr ".tgi[0].ni[29].y" 8817.142578125;
	setAttr ".tgi[0].ni[29].nvs" 18304;
	setAttr ".tgi[0].ni[30].x" 1511.4285888671875;
	setAttr ".tgi[0].ni[30].y" 9121.4287109375;
	setAttr ".tgi[0].ni[30].nvs" 18304;
	setAttr ".tgi[0].ni[31].x" 1580;
	setAttr ".tgi[0].ni[31].y" 9452.857421875;
	setAttr ".tgi[0].ni[31].nvs" 18304;
	setAttr ".tgi[0].ni[32].x" 341.42855834960938;
	setAttr ".tgi[0].ni[32].y" 10112.857421875;
	setAttr ".tgi[0].ni[32].nvs" 18304;
	setAttr ".tgi[0].ni[33].x" 1580;
	setAttr ".tgi[0].ni[33].y" 9660;
	setAttr ".tgi[0].ni[33].nvs" 18304;
	setAttr ".tgi[0].ni[34].x" 1887.142822265625;
	setAttr ".tgi[0].ni[34].y" 8918.5712890625;
	setAttr ".tgi[0].ni[34].nvs" 18304;
	setAttr ".tgi[0].ni[35].x" 1887.142822265625;
	setAttr ".tgi[0].ni[35].y" 9558.5712890625;
	setAttr ".tgi[0].ni[35].nvs" 18304;
	setAttr ".tgi[0].ni[36].x" 1887.142822265625;
	setAttr ".tgi[0].ni[36].y" 9452.857421875;
	setAttr ".tgi[0].ni[36].nvs" 18304;
	setAttr ".tgi[0].ni[37].x" 1268.5714111328125;
	setAttr ".tgi[0].ni[37].y" 9814.2861328125;
	setAttr ".tgi[0].ni[37].nvs" 18304;
	setAttr ".tgi[0].ni[38].x" 1887.142822265625;
	setAttr ".tgi[0].ni[38].y" 9351.4287109375;
	setAttr ".tgi[0].ni[38].nvs" 18304;
	setAttr ".tgi[0].ni[39].x" 1887.142822265625;
	setAttr ".tgi[0].ni[39].y" 9250;
	setAttr ".tgi[0].ni[39].nvs" 18304;
	setAttr ".tgi[0].ni[40].x" 648.5714111328125;
	setAttr ".tgi[0].ni[40].y" 10035.7138671875;
	setAttr ".tgi[0].ni[40].nvs" 18304;
	setAttr ".tgi[0].ni[41].x" 955.71429443359375;
	setAttr ".tgi[0].ni[41].y" 9724.2861328125;
	setAttr ".tgi[0].ni[41].nvs" 18304;
	setAttr ".tgi[0].ni[42].x" 1580;
	setAttr ".tgi[0].ni[42].y" 9351.4287109375;
	setAttr ".tgi[0].ni[42].nvs" 18304;
	setAttr ".tgi[0].ni[43].x" 1887.142822265625;
	setAttr ".tgi[0].ni[43].y" 8817.142578125;
	setAttr ".tgi[0].ni[43].nvs" 18304;
	setAttr ".tgi[0].ni[44].x" 1887.142822265625;
	setAttr ".tgi[0].ni[44].y" 8354.2861328125;
	setAttr ".tgi[0].ni[44].nvs" 18304;
	setAttr ".tgi[0].ni[45].x" 1511.4285888671875;
	setAttr ".tgi[0].ni[45].y" 8715.7138671875;
	setAttr ".tgi[0].ni[45].nvs" 18304;
	setAttr ".tgi[0].ni[46].x" 1268.5714111328125;
	setAttr ".tgi[0].ni[46].y" 9712.857421875;
	setAttr ".tgi[0].ni[46].nvs" 18304;
	setAttr ".tgi[0].ni[47].x" 648.5714111328125;
	setAttr ".tgi[0].ni[47].y" 9732.857421875;
	setAttr ".tgi[0].ni[47].nvs" 18304;
	setAttr ".tgi[0].ni[48].x" 1268.5714111328125;
	setAttr ".tgi[0].ni[48].y" 10167.142578125;
	setAttr ".tgi[0].ni[48].nvs" 18304;
	setAttr ".tgi[0].ni[49].x" 1887.142822265625;
	setAttr ".tgi[0].ni[49].y" 8484.2861328125;
	setAttr ".tgi[0].ni[49].nvs" 18304;
	setAttr ".tgi[0].ni[50].x" 445.71429443359375;
	setAttr ".tgi[0].ni[50].y" 8914.2861328125;
	setAttr ".tgi[0].ni[50].nvs" 18304;
	setAttr ".tgi[0].ni[51].x" 1887.142822265625;
	setAttr ".tgi[0].ni[51].y" 8664.2861328125;
	setAttr ".tgi[0].ni[51].nvs" 18304;
	setAttr ".tgi[0].ni[52].x" 1580;
	setAttr ".tgi[0].ni[52].y" 9250;
	setAttr ".tgi[0].ni[52].nvs" 18304;
	setAttr ".tgi[0].ni[53].x" 1580;
	setAttr ".tgi[0].ni[53].y" 9558.5712890625;
	setAttr ".tgi[0].ni[53].nvs" 18304;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "9176A5C3-41EF-FB93-0735-829DFA6AEF39";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -5501.3734077686886 -4482.600554478191 ;
	setAttr ".tgi[0].vh" -type "double2" 5603.2506930977388 4564.1023827414301 ;
	setAttr -s 9 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" -124.28571319580078;
	setAttr ".tgi[0].ni[0].y" 3270;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" -55.714286804199219;
	setAttr ".tgi[0].ni[1].y" -3698.571533203125;
	setAttr ".tgi[0].ni[1].nvs" 1923;
	setAttr ".tgi[0].ni[2].x" -124.28571319580078;
	setAttr ".tgi[0].ni[2].y" 3541.428466796875;
	setAttr ".tgi[0].ni[2].nvs" 1923;
	setAttr ".tgi[0].ni[3].x" -55.714286804199219;
	setAttr ".tgi[0].ni[3].y" -3880;
	setAttr ".tgi[0].ni[3].nvs" 1923;
	setAttr ".tgi[0].ni[4].x" -91.428573608398438;
	setAttr ".tgi[0].ni[4].y" 51.428569793701172;
	setAttr ".tgi[0].ni[4].nvs" 1922;
	setAttr ".tgi[0].ni[5].x" -91.428573608398438;
	setAttr ".tgi[0].ni[5].y" 512.85711669921875;
	setAttr ".tgi[0].ni[5].nvs" 1923;
	setAttr ".tgi[0].ni[6].x" -91.428573608398438;
	setAttr ".tgi[0].ni[6].y" -212.85714721679688;
	setAttr ".tgi[0].ni[6].nvs" 1923;
	setAttr ".tgi[0].ni[7].x" -91.428573608398438;
	setAttr ".tgi[0].ni[7].y" 331.42855834960938;
	setAttr ".tgi[0].ni[7].nvs" 1923;
	setAttr ".tgi[0].ni[8].x" -91.428573608398438;
	setAttr ".tgi[0].ni[8].y" 694.28570556640625;
	setAttr ".tgi[0].ni[8].nvs" 1923;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av -k on ".unw" 1;
	setAttr -av -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -av -k on ".ihi";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".cons" no;
	setAttr -av ".ta" 0;
	setAttr -av ".tq";
	setAttr ".tmr" 1024;
	setAttr -av ".aoam";
	setAttr -av ".aora";
	setAttr -av ".hfd";
	setAttr -av ".hfe";
	setAttr -av ".hfa";
	setAttr -av ".mbe";
	setAttr -av -k on ".mbsof";
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 3 ".u";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRedshiftPostEffects.msg" ":redshiftOptions.postEffects";
connectAttr "FacialCtrlMover_EyeLidsLwrUD_C_Mdv.ox" "FacialCtrlMover_EyeLidsLwrUp_C_Pma.i1[0]"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUD_C_Cmp.opr" "FacialCtrlMover_EyeLidsLwrUD_C_Mdv.i1x"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUD_C_Cmp.opg" "FacialCtrlMover_EyeLidsLwrUD_C_Mdv.i1y"
		;
connectAttr "EyeLidsLwrBsh_C_Ctrl.ty" "FacialCtrlMover_EyeLidsLwrUD_C_Cmp.ipr";
connectAttr "EyeLidsLwrBsh_C_Ctrl.ty" "FacialCtrlMover_EyeLidsLwrUD_C_Cmp.ipg";
connectAttr "FacialCtrlMover_EyeLidsLwrUD_C_Mdv.oy" "FacialCtrlMover_EyeLidsLwrDn_C_Pma.i1[0]"
		;
connectAttr "EyeLidsUprTurnFC_C_Cmp.opr" "EyeLidsUprTurnFC_C_Mdv.i1x";
connectAttr "EyeLidsUprTurnFC_C_Cmp.opg" "EyeLidsUprTurnFC_C_Mdv.i1y";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsTurnFC" "EyeLidsUprTurnFC_C_Cmp.ipr";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsTurnFC" "EyeLidsUprTurnFC_C_Cmp.ipg";
connectAttr "EyeLidsUprOutUD_C_Cmp.opr" "EyeLidsUprOutUD_C_Mdv.i1x";
connectAttr "EyeLidsUprOutUD_C_Cmp.opg" "EyeLidsUprOutUD_C_Mdv.i1y";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsOutUD" "EyeLidsUprOutUD_C_Cmp.ipr";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsOutUD" "EyeLidsUprOutUD_C_Cmp.ipg";
connectAttr "EyeLidsUprMidUD_C_Cmp.opr" "EyeLidsUprMidUD_C_Mdv.i1x";
connectAttr "EyeLidsUprMidUD_C_Cmp.opg" "EyeLidsUprMidUD_C_Mdv.i1y";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsMidUD" "EyeLidsUprMidUD_C_Cmp.ipr";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsMidUD" "EyeLidsUprMidUD_C_Cmp.ipg";
connectAttr "EyeLidsUprInUD_C_Cmp.opr" "EyeLidsUprInUD_C_Mdv.i1x";
connectAttr "EyeLidsUprInUD_C_Cmp.opg" "EyeLidsUprInUD_C_Mdv.i1y";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsInUD" "EyeLidsUprInUD_C_Cmp.ipr";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsInUD" "EyeLidsUprInUD_C_Cmp.ipg";
connectAttr "EyeLidsLwrTurnFC_C_Cmp.opr" "EyeLidsLwrTurnFC_C_Mdv.i1x";
connectAttr "EyeLidsLwrTurnFC_C_Cmp.opg" "EyeLidsLwrTurnFC_C_Mdv.i1y";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsTurnFC" "EyeLidsLwrTurnFC_C_Cmp.ipr";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsTurnFC" "EyeLidsLwrTurnFC_C_Cmp.ipg";
connectAttr "EyeLidsLwrOutUD_C_Cmp.opr" "EyeLidsLwrOutUD_C_Mdv.i1x";
connectAttr "EyeLidsLwrOutUD_C_Cmp.opg" "EyeLidsLwrOutUD_C_Mdv.i1y";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsOutUD" "EyeLidsLwrOutUD_C_Cmp.ipr";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsOutUD" "EyeLidsLwrOutUD_C_Cmp.ipg";
connectAttr "EyeLidsLwrMidUD_C_Cmp.opr" "EyeLidsLwrMidUD_C_Mdv.i1x";
connectAttr "EyeLidsLwrMidUD_C_Cmp.opg" "EyeLidsLwrMidUD_C_Mdv.i1y";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsMidUD" "EyeLidsLwrMidUD_C_Cmp.ipr";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsMidUD" "EyeLidsLwrMidUD_C_Cmp.ipg";
connectAttr "EyeLidsLwrInUD_C_Cmp.opr" "EyeLidsLwrInUD_C_Mdv.i1x";
connectAttr "EyeLidsLwrInUD_C_Cmp.opg" "EyeLidsLwrInUD_C_Mdv.i1y";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsInUD" "EyeLidsLwrInUD_C_Cmp.ipr";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsInUD" "EyeLidsLwrInUD_C_Cmp.ipg";
connectAttr "EyeBallUD_C_Cmp.opr" "EyeBallUD_C_Mdv.i1x";
connectAttr "EyeBallUD_C_Cmp.opg" "EyeBallUD_C_Mdv.i1y";
connectAttr "EyeLidsBsh_C_Ctrl.ty" "EyeBallUD_C_Cmp.ipr";
connectAttr "EyeLidsBsh_C_Ctrl.ty" "EyeBallUD_C_Cmp.ipg";
connectAttr "EyeBallFC_C_Cmp.opr" "EyeBallFC_C_Mdv.i1x";
connectAttr "EyeBallFC_C_Cmp.opg" "EyeBallFC_C_Mdv.i1y";
connectAttr "unitConversion199.o" "EyeBallFC_C_Cmp.ipr";
connectAttr "unitConversion200.o" "EyeBallFC_C_Cmp.ipg";
connectAttr "EyeLidsBsh_C_Ctrl.rz" "unitConversion200.i";
connectAttr "EyeLidsBsh_C_Ctrl.rz" "unitConversion199.i";
connectAttr "EyeLidsUprUp_C_Pma.o1" "EyeLidsUprAmpUD_C_Cmp.ipr";
connectAttr "EyeLidsUprDn_C_Pma.o1" "EyeLidsUprAmpUD_C_Cmp.ipg";
connectAttr "EyeLidsUprUD_C_Mdv.oy" "EyeLidsUprDn_C_Pma.i1[0]";
connectAttr "EyeLidsFollowUprUD_C_Mdv.oy" "EyeLidsUprDn_C_Pma.i1[2]";
connectAttr "EyeLidsFollowUprUD_C_Cmp.opr" "EyeLidsFollowUprUD_C_Mdv.i1x";
connectAttr "EyeLidsFollowUprUD_C_Cmp.opg" "EyeLidsFollowUprUD_C_Mdv.i1y";
connectAttr "EyeLidsFollowUprUD_C_Bcl.opr" "EyeLidsFollowUprUD_C_Cmp.ipr";
connectAttr "EyeLidsFollowUprUD_C_Bcl.opg" "EyeLidsFollowUprUD_C_Cmp.ipg";
connectAttr "EyeLidsBsh_C_Jnt.eyelidsFollow" "EyeLidsFollowUprUD_C_Bcl.b";
connectAttr "unitConversion211.o" "EyeLidsFollowUprUD_C_Bcl.c1r";
connectAttr "unitConversion212.o" "EyeLidsFollowUprUD_C_Bcl.c1g";
connectAttr "EyeLidsBsh_C_Jnt.rx" "unitConversion211.i";
connectAttr "EyeLidsBsh_C_Jnt.rx" "unitConversion212.i";
connectAttr "EyeLidsUprUD_C_Cmp.opr" "EyeLidsUprUD_C_Mdv.i1x";
connectAttr "EyeLidsUprUD_C_Cmp.opg" "EyeLidsUprUD_C_Mdv.i1y";
connectAttr "EyeLidsUprBsh_C_Ctrl.ty" "EyeLidsUprUD_C_Cmp.ipr";
connectAttr "EyeLidsUprBsh_C_Ctrl.ty" "EyeLidsUprUD_C_Cmp.ipg";
connectAttr "EyeLidsUprUD_C_Mdv.ox" "EyeLidsUprUp_C_Pma.i1[0]";
connectAttr "EyeLidsFollowUprUD_C_Mdv.ox" "EyeLidsUprUp_C_Pma.i1[2]";
connectAttr "FacialCtrlMover_EyeLidsLwrUp_C_Pma.o1" "EyeLidsLwrAllUD_C_Cmp.ipr";
connectAttr "FacialCtrlMover_EyeLidsLwrDn_C_Pma.o1" "EyeLidsLwrAllUD_C_Cmp.ipg";
connectAttr "EyeBallIn_C_Pma.o1" "EyeBallAmpIO_C_Cmp.ipr";
connectAttr "EyeBallOut_C_Pma.o1" "EyeBallAmpIO_C_Cmp.ipg";
connectAttr "EyeBallIO_C_Mdv.ox" "EyeBallIn_C_Pma.i1[0]";
connectAttr "EyeBallFollowIO_C_Mdv.ox" "EyeBallIn_C_Pma.i1[1]";
connectAttr "EyeBallFollowIO_C_Cmp.opr" "EyeBallFollowIO_C_Mdv.i1x";
connectAttr "EyeBallFollowIO_C_Cmp.opg" "EyeBallFollowIO_C_Mdv.i1y";
connectAttr "EyeBallFollowIO_C_Bcl.opr" "EyeBallFollowIO_C_Cmp.ipr";
connectAttr "EyeBallFollowIO_C_Bcl.opg" "EyeBallFollowIO_C_Cmp.ipg";
connectAttr "unitConversion215.o" "EyeBallFollowIO_C_Bcl.c1r";
connectAttr "unitConversion216.o" "EyeBallFollowIO_C_Bcl.c1g";
connectAttr "EyeLidsBsh_C_Jnt.eyelidsFollow" "EyeBallFollowIO_C_Bcl.b";
connectAttr "EyeLidsBsh_C_Jnt.ry" "unitConversion215.i";
connectAttr "EyeLidsBsh_C_Jnt.ry" "unitConversion216.i";
connectAttr "EyeBallIO_C_Cmp.opr" "EyeBallIO_C_Mdv.i1x";
connectAttr "EyeBallIO_C_Cmp.opg" "EyeBallIO_C_Mdv.i1y";
connectAttr "EyeLidsBsh_C_Ctrl.tx" "EyeBallIO_C_Cmp.ipr";
connectAttr "EyeLidsBsh_C_Ctrl.tx" "EyeBallIO_C_Cmp.ipg";
connectAttr "EyeBallIO_C_Mdv.oy" "EyeBallOut_C_Pma.i1[0]";
connectAttr "EyeBallFollowIO_C_Mdv.oy" "EyeBallOut_C_Pma.i1[1]";
connectAttr "EyeBallAmpIO_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "EyeBallIO_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[1].dn";
connectAttr "unitConversion212.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "EyeBallUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[3].dn";
connectAttr "EyeLidsLwrInUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[4].dn"
		;
connectAttr "EyeBallUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[5].dn";
connectAttr "EyeLidsUprUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[6].dn"
		;
connectAttr "EyeLidsLwrMidUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[7].dn"
		;
connectAttr "unitConversion200.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[8].dn"
		;
connectAttr "EyeLidsBsh_C_Ctrl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[9].dn"
		;
connectAttr "EyeLidsLwrOutUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[10].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUp_C_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[11].dn"
		;
connectAttr "EyeLidsBsh_C_Jnt.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[12].dn"
		;
connectAttr "EyeLidsUprBsh_C_Ctrl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[13].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[14].dn"
		;
connectAttr "EyeBallIn_C_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[15].dn"
		;
connectAttr "EyeBallIO_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[16].dn"
		;
connectAttr "EyeLidsUprDn_C_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[17].dn"
		;
connectAttr "EyeLidsLwrMidUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[18].dn"
		;
connectAttr "EyeLidsUprInUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[19].dn"
		;
connectAttr "unitConversion199.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[20].dn"
		;
connectAttr "unitConversion215.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[21].dn"
		;
connectAttr "EyeLidsUprAmpUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[22].dn"
		;
connectAttr "EyeBallFollowIO_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[23].dn"
		;
connectAttr "EyeLidsUprInUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[24].dn"
		;
connectAttr "EyeLidsUprBsh_C_CtrlShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[25].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[26].dn"
		;
connectAttr "unitConversion211.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[27].dn"
		;
connectAttr "EyeBallOut_C_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[28].dn"
		;
connectAttr "EyeLidsLwrTurnFC_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[29].dn"
		;
connectAttr "EyeLidsLwrInUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[30].dn"
		;
connectAttr "EyeLidsUprOutUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[31].dn"
		;
connectAttr "unitConversion216.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[32].dn"
		;
connectAttr "EyeLidsUprUp_C_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[33].dn"
		;
connectAttr "EyeLidsLwrOutUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[34].dn"
		;
connectAttr "EyeBallFC_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[35].dn"
		;
connectAttr "EyeLidsUprOutUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[36].dn"
		;
connectAttr "EyeLidsUprUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[37].dn"
		;
connectAttr "EyeLidsUprMidUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[38].dn"
		;
connectAttr "EyeLidsUprTurnFC_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[39].dn"
		;
connectAttr "EyeBallFollowIO_C_Bcl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[40].dn"
		;
connectAttr "EyeLidsFollowUprUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[41].dn"
		;
connectAttr "EyeLidsUprMidUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[42].dn"
		;
connectAttr "EyeLidsLwrTurnFC_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[43].dn"
		;
connectAttr "EyeLidsBsh_C_CtrlShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[44].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrDn_C_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[45].dn"
		;
connectAttr "EyeLidsFollowUprUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[46].dn"
		;
connectAttr "EyeLidsFollowUprUD_C_Bcl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[47].dn"
		;
connectAttr "EyeBallFollowIO_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[48].dn"
		;
connectAttr "EyeLidsLwrBsh_C_CtrlShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[49].dn"
		;
connectAttr "EyeLidsLwrBsh_C_Ctrl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[50].dn"
		;
connectAttr "EyeLidsLwrAllUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[51].dn"
		;
connectAttr "EyeLidsUprTurnFC_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[52].dn"
		;
connectAttr "EyeBallFC_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[53].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrDn_C_Pma.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "unitConversion199.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUp_C_Pma.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "unitConversion200.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "defaultRedshiftPostEffects.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[4].dn"
		;
connectAttr "unitConversion211.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[5].dn"
		;
connectAttr "unitConversion216.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[6].dn"
		;
connectAttr "unitConversion212.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[7].dn"
		;
connectAttr "unitConversion215.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[8].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUp_C_Pma.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "FacialCtrlMover_EyeLidsLwrDn_C_Pma.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "defaultRedshiftPostEffects.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "EyeLidsUprAmpUD_C_Cmp.op" ":internal_standInShader.ic";
// End of EyeCtrl.ma
