//Maya ASCII 2012 scene
//Name: tmpJntAnimal.ma
//Last modified: Fri, Jun 17, 2016 02:48:01 PM
//Codeset: 1252
requires maya "2012";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2012";
fileInfo "version" "2012 x64";
fileInfo "cutIdentifier" "201201172029-821146";
fileInfo "osv" "Microsoft Windows 7 Ultimate Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 154.51283810125511 71.064700523846426 83.212366678733645 ;
	setAttr ".r" -type "double3" -16.538352729603165 64.600000000000307 0 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 193.89534228349595;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode joint -n "root_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 0 20.629556711672961 -3.4119754553017119 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 39.094574767651494 -1.4943476618321787 1;
	setAttr ".radi" 0.4;
createNode joint -n "spine1_tmpJnt" -p "root_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0 1.1593292218982576 -1.204647993937888 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 41.547517051134925 -5.2233962666346141 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "spine2_tmpJnt" -p "spine1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0 -0.28552360756954442 8.2058851624057763 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 40.750401574510782 8.3598589745017797 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "neck_tmpJnt" -p "spine2_tmpJnt";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0 3.4089734940009855 6.4432083324829312 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 47.210270454588908 23.982340629917111 1;
	setAttr ".radi" 0.4;
createNode joint -n "head1_tmpJnt" -p "neck_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 8.5680250113807119 3.9748996465273656 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 63.446336306127407 31.514617009660721 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "head2_tmpJnt" -p "head1_tmpJnt";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 3.1822470872659849 0.20010772998842441 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 69.47656768338048 31.893813181144633 1;
	setAttr ".radi" 0.4;
createNode joint -n "ear1_lft_tmpJnt" -p "head2_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 2.3258164533994155 -0.045998122883126769 -1.2206565033081596 ;
	setAttr ".r" -type "double3" -9.9392333795734948e-017 7.9513867036587919e-016 -3.2799470152592518e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yzx";
	setAttr ".jo" -type "double3" 11.94075458424472 4.0179841020492413 -18.331881992747604 ;
	setAttr ".bps" -type "matrix" 0.94691745335815425 -0.31374765247538922 -0.070069587496217792 0
		 0.32147680557966812 0.92415105202813563 0.20639160959104513 0 -2.8449465006019641e-016 -0.21796156450501203 0.97595735378064929 0
		 4.4073294657550663 69.389403074128552 29.58071776609912 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ear2_lft_tmpJnt" -p "ear1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 1.2434497875801753e-014 1.3538486711478441 -1.6875389974302379e-014 ;
	setAttr ".r" -type "double3" -4.9696166897867449e-017 -3.975693351829396e-016 1.7241821476758432e-034 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yzx";
	setAttr ".jo" -type "double3" 11.808234418738371 1.1440797354871068 13.296698745582272 ;
	setAttr ".bps" -type "matrix" 0.99527178819434292 -0.088417402284545962 -0.0402048578866738 0
		 0.097129128609469512 0.9060036606841162 0.41197487690700996 0 -2.3045756763950797e-015 -0.41393203524278432 0.91030778871751195 0
		 5.2320747589969052 71.76030267613983 30.11021322462182 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ear3_lft_tmpJnt" -p "ear2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" -2.7755575615628914e-014 1.5230024502389057 3.3750779948604759e-014 ;
	setAttr ".r" -type "double3" -1.5840653198695249e-015 -8.4483483726374669e-016 -3.9384212266559953e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yzx";
	setAttr ".jo" -type "double3" 10.576277877710515 0.26418564279854095 4.1592661216079954 ;
	setAttr ".bps" -type "matrix" 0.99968460000870463 -0.020564028307948651 -0.014416006561664172 0
		 0.025113751321458468 0.81857712734586463 0.5738473543380298 0 -2.5256086616838444e-015 -0.57402840289130497 0.81883538802011835 0
		 5.5123922343172422 74.375055453497765 31.299184638689756 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ear4_lft_tmpJnt" -p "ear3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -6.6613381477509392e-015 2.0194675995742131 8.8817841970012523e-015 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.99968460000870463 -0.020564028307948651 -0.014416006561664172 0
		 0.025113751321458468 0.81857712734586463 0.5738473543380298 0 -2.5256086616838444e-015 -0.57402840289130497 0.81883538802011835 0
		 5.6084978040715194 77.507595081067336 33.495189776859597 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ear1_rgt_tmpJnt" -p "head2_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -2.3258114549349451 -0.045968340316683509 -1.2206529267538233 ;
	setAttr ".r" -type "double3" 0 0 -3.1805546814635168e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yzx";
	setAttr ".jo" -type "double3" -168.05924541575527 -4.0179841020492475 18.331881992747604 ;
	setAttr ".bps" -type "matrix" 0.94691745335815425 0.31374765247538922 0.070069587496217903 0
		 0.32147680557966812 -0.92415105202813552 -0.20639160959104516 0 -1.7347234759768076e-016 0.21796156450501206 -0.97595735378064918 0
		 -4.4073199938641521 69.389459510904757 29.580724543527012 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ear2_rgt_tmpJnt" -p "ear1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -4.1969102364092237e-005 -1.3537452254032516 -1.5271488863710658e-005 ;
	setAttr ".r" -type "double3" 8.0010828705566563e-015 -1.1797716345471951e-031 1.689669674527493e-015 ;
	setAttr ".s" -type "double3" 0.99999999999999989 1 1 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yzx";
	setAttr ".jo" -type "double3" 11.808234418738351 1.144079735487108 13.296698745582267 ;
	setAttr ".bps" -type "matrix" 0.99527178819434303 0.088417402284546032 0.040204857886673939 0
		 0.097129128609469581 -0.9060036606841162 -0.41197487690700962 0 -2.1404133421573166e-015 0.41393203524278394 -0.91030778871751195 0
		 -5.23207757756734 71.760146695846018 30.110202214495597 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ear3_rgt_tmpJnt" -p "ear2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" -1.0074916290125913e-006 -1.5230662580853505 2.7599084990725942e-005 ;
	setAttr ".r" -type "double3" 3.1836606918946329e-015 -1.4908850069360232e-016 2.4848083448933712e-017 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yzx";
	setAttr ".jo" -type "double3" 10.576277877710515 0.26418564279853651 4.1592661216080486 ;
	setAttr ".bps" -type "matrix" 0.99968460000870474 0.020564028307947992 0.014416006561663907 0
		 0.025113751321457708 -0.81857712734586463 -0.57384735433802936 0 -2.2913785408345151e-015 0.57402840289130441 -0.81883538802011846 0
		 -5.51240869722319 74.375030500617001 31.299175756720459 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ear4_rgt_tmpJnt" -p "ear3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 7.0291499678631908e-006 -2.0195183640906578 2.5733565863106378e-005 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 0.99968460000870474 0.020564028307947992 0.014416006561663907 0
		 0.025113751321457708 -0.81857712734586463 -0.57384735433802936 0 -2.2913785408345151e-015 0.57402840289130441 -0.81883538802011846 0
		 -5.6085033670804956 77.507677138513301 33.495196359397042 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "jaw1_lwr_tmpJnt" -p "head1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 0.03956185877427032 1.2415502774641176 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 63.52130445146021 33.867305293848503 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "jaw2_lwr_tmpJnt" -p "jaw1_lwr_tmpJnt";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0 -2.4296245510729335 4.365868424404594 ;
	setAttr ".radi" 0.4;
createNode joint -n "jaw3_lwr_tmpJnt" -p "jaw2_lwr_tmpJnt";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 0 0 2.1970214002293744 ;
	setAttr ".radi" 0.4;
createNode joint -n "teeth_lwr_tmpJnt" -p "jaw2_lwr_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 0 0.015711991618864829 1.1655994370861968 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 223.88262395048474 12.418563328530155 1;
	setAttr ".radi" 0.4;
createNode joint -n "tongue1_tmpJnt" -p "jaw1_lwr_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -5.0817162496644025e-035 -2.3808521832646328 2.1413155562812829 ;
	setAttr ".r" -type "double3" -1.9083328088781101e-014 -7.0622500768802538e-031 1.176100896151986e-046 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" 98.95399545178401 7.0622500768802538e-031 180 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.84443606484883982 0.53565635661551347 0
		 0 -0.53565635661551347 0.84443606484883982 0 0 222.64327363418465 -1.3727481207023438 1;
	setAttr ".radi" 0.4;
createNode joint -n "tongue2_tmpJnt" -p "tongue1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 2.4851889958478363e-017 1.2337407498721227 0.011044575308424953 ;
	setAttr ".r" -type "double3" 3.975693351829396e-015 0 0 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -4.0973391511530428 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.084133709421899183 0.99645447409247523 0
		 0 -0.99645447409247523 0.084133709421899183 0 0 225.5005072402341 0.43969856072858127 1;
	setAttr ".radi" 0.4;
createNode joint -n "tongue3_tmpJnt" -p "tongue2_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 8.0097929275676829e-018 0.91537991868257862 -3.1974423109204508e-014 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -2.6508370880522065 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 -0.081552711316839971 0.99666902995772477 0
		 0 -0.99666902995772477 -0.081552711316839971 0 0 225.71745581610085 3.0091725629561377 1;
	setAttr ".radi" 0.4;
createNode joint -n "tongue4_tmpJnt" -p "tongue3_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 4.8524827619152476e-018 0.85673733164427901 3.5527136788005009e-015 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -2.8986362677436528 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 -0.098936905545293025 0.99509370851248047 0
		 0 -0.99509370851248047 -0.098936905545293025 0 0 225.47154161508337 6.0145302539939607 1;
	setAttr ".radi" 0.4;
createNode joint -n "tongue5_tmpJnt" -p "tongue4_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 4.8445543917814027e-018 0.78227088770197284 3.5527136788005009e-014 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -2.0981428438333589 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 -0.11816985903832641 0.9929933959573255 0
		 0 -0.9929933959573255 -0.11816985903832641 0 0 225.1592345963669 9.1556710420160119 1;
	setAttr ".radi" 0.4;
createNode joint -n "tongue6_tmpJnt" -p "tongue5_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 3.2551144062268319e-018 0.72600573940935931 -1.4210854715202004e-014 ;
	setAttr ".r" -type "double3" 0 0 -4.4139062980501586e-032 ;
	setAttr ".jo" -type "double3" 3.9955317923235678 180 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 -0.11816985903832641 0.9929933959573255 0
		 0 -0.9929933959573255 -0.11816985903832641 0 0 224.78384531194365 12.310105543157466 1;
	setAttr ".radi" 0.4;
createNode joint -n "jaw1_upr_tmpJnt" -p "head1_tmpJnt";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.8178529265960262e-017 -1.6425042259243838 5.6074187018687116 ;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".radi" 0.4;
createNode joint -n "jaw2_upr_tmpJnt" -p "jaw1_upr_tmpJnt";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0 0 2.5912786275981254 ;
	setAttr ".radi" 0.4;
createNode joint -n "teeth_upr_tmpJnt" -p "jaw1_upr_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -1.2864598940251297e-017 -0.043391534027666268 1.2561700586482196 ;
	setAttr ".jo" -type "double3" 0 0 -180 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -2.5021681837478968e-030 226.2211830093936 12.418563328530155 1;
	setAttr ".radi" 0.4;
createNode joint -n "eye_lft_tmpJnt" -p "head1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 1.5422186132612765 1.5041008717337547 3.5435743981680279 ;
	setAttr ".radi" 0.4;
createNode joint -n "eye_rgt_tmpJnt" -p "head1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.542218640956178 1.5040295374653141 3.5436227303878756 ;
	setAttr ".radi" 0.4;
createNode joint -n "eyeTrgt_tmpJnt" -p "head1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.3847450653213117e-008 1.5040652045995344 12.517698156835808 ;
	setAttr ".radi" 0.4;
createNode joint -n "eyeTrgt_lft_tmpJnt" -p "eyeTrgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 1.542218627108727 3.566713422031853e-005 -2.416610992383994e-005 ;
	setAttr ".radi" 0.4;
createNode joint -n "eyeTrgt_rgt_tmpJnt" -p "eyeTrgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.542218627108727 -3.566713422031853e-005 2.416610992383994e-005 ;
	setAttr ".radi" 0.4;
createNode joint -n "clavFront_lft_tmpJnt" -p "spine2_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0.70347627779794508 -0.38610604071332943 8.7505860184206554 ;
	setAttr ".jo" -type "double3" 0 0 -89.999999999999986 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1.0000000000000002 0 0 1.0000000000000002 2.2204460492503131e-016 0 0
		 0 0 1 0 1.3330595039290227 40.018746018607096 28.354729366352228 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "upLegFront_lft_tmpJnt" -p "clavFront_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 1.52027436230183 3.2625632960997253 -1.6098725189891034 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -90.000000000000028 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 7.5154868952971778 37.1378867043599 26.696138020488331 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "lowLegFront_lft_tmpJnt" -p "upLegFront_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 6.6613381477509392e-015 6.7600078180342713 -2.0883565478833948 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 7.5154868952971698 24.327941361677972 21.346732706204833 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ankleFront_lft_tmpJnt" -p "lowLegFront_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 3.9968028886505635e-015 10.678026840332652 0.82270645077458759 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 7.5154868952971601 4.0935061547482006 22.905728635082102 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ballFront_lft_tmpJnt" -p "ankleFront_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 1.3322676295501878e-015 2.1602070008110967 1.1321918942023803 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 7.5154868952971574 1.7763568394002505e-015 25.051187142314646 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "toeFront_lft_tmpJnt" -p "ballFront_lft_tmpJnt";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 0 2.0058133233534665 3.9876955904016853e-016 ;
	setAttr ".ro" 1;
	setAttr ".radi" 0.4;
createNode joint -n "thumbFrontFoot1_lft_tmpJnt" -p "toeFront_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 1.6155886878093497 -0.10299784845931725 0.22719270718525308 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -6.361109362927028e-015 1.9738928024388945e-014 4.3829224748190706e-030 ;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562648086886 3.3809123766256222 25.20550640416533 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbFrontFoot2_lft_tmpJnt" -p "thumbFrontFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 2.6645352591003757e-015 0.92576787951842121 -3.1086244689504383e-015 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562389251162 3.3809123766256186 18.1141638774779 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbFrontFoot3_lft_tmpJnt" -p "thumbFrontFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -1.3322676295501878e-015 0.63058043554024934 3.2313158220625618e-015 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562464328882 3.3809123766256284 20.17107393750047 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbFrontFoot4_lft_tmpJnt" -p "thumbFrontFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 1.0218541541130968 2.1037555427542346e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562648086886 3.3809123766256222 25.20550640416533 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFrontFoot1_lft_tmpJnt" -p "toeFront_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.75118361611044859 0.5487847603948719 0.32102508156220488 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -6.361109362927028e-015 1.9738928024388945e-014 4.3829224748190706e-030 ;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736497602165 3.0356842493631335 24.950677281414283 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFrontFoot2_lft_tmpJnt" -p "indexFrontFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 2.7149900105882807e-009 0.91972559424033395 -0.016453541419196638 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736265256656 3.5823867347681335 18.867787120980601 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFrontFoot3_lft_tmpJnt" -p "indexFrontFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -9.9510444329098391e-010 0.51124777198153026 -0.074670399598353532 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736355656542 3.0356842493631389 21.06178202391191 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFrontFoot4_lft_tmpJnt" -p "indexFrontFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -2.6645352591003757e-015 0.90619583172745344 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736497602165 3.0356842493631335 24.950677281414283 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFrontFoot1_lft_tmpJnt" -p "toeFront_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -0.075847768987047814 0.62145349667773786 0.34729186241259252 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -6.361109362927028e-015 1.9738928024388945e-014 4.3829224748190706e-030 ;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.632825854991474 2.5509665919724842 23.422981436211693 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFrontFoot2_lft_tmpJnt" -p "middleFrontFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 5.5441438107095564e-009 0.97308055092308265 -0.062194519388097835 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.833229097571561 3.545618914658315 18.322361793410607 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFrontFoot3_lft_tmpJnt" -p "middleFrontFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -1.9347714541595451e-009 0.55856531325370451 -0.12918715907726763 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.739121441737378 2.5509665939787247 20.717569587931241 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFrontFoot4_lft_tmpJnt" -p "middleFrontFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 0.63090526850224382 1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.632825854991474 2.5509665919724842 23.422981436211693 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFrontFoot1_lft_tmpJnt" -p "toeFront_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -0.94612093844308198 0.60273712923109635 0.2915792639177896 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -6.361109362927028e-015 1.9738928024388945e-014 4.3829224748190706e-030 ;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.225828301514859 2.8802883702174338 21.003340089239849 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFrontFoot2_lft_tmpJnt" -p "ringFrontFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.2997105613976601e-009 0.92072713237502946 0.013077546184747324 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.387754888646541 3.2321558806201516 16.882020110151402 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFrontFoot3_lft_tmpJnt" -p "ringFrontFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 0.33019146419784917 -0.081992660476667978 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.332123888260764 2.8802883722236743 18.297928240959397 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFrontFoot4_lft_tmpJnt" -p "ringFrontFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 0.63090526850224471 1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.225828301514859 2.8802883702174338 21.003340089239849 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFrontFoot1_lft_tmpJnt" -p "toeFront_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.7441352260005099 0.32478929448824267 0.20194413712321294 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -6.361109362927028e-015 1.9738928024388945e-014 4.3829224748190706e-030 ;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.320681697400357 2.0419542914872575 17.740972323318903 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFrontFoot2_lft_tmpJnt" -p "pinkyFrontFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 6.2172489379008766e-015 0.58181578466233619 -3.1086244689504383e-015 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936116 0
		 -0.14442492233936111 -2.7258954824815886e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 31.048633193268319 2.0419543052267 12.753471087826441 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFrontFoot3_lft_tmpJnt" -p "pinkyFrontFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -4.4408920985006262e-015 0.54360309640941562 2.9420910152566648e-015 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.711712061830106 2.0419542988676174 15.06185917254086 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFrontFoot4_lft_tmpJnt" -p "pinkyFrontFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 0.63090526850224382 -1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.320681697400357 2.0419542914872575 17.740972323318903 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "heelFront_lft_tmpJnt" -p "ankleFront_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 1.3322676295501878e-015 2.160207000811095 -1.3007746051732063 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 7.5154868952971574 1.7763568394002505e-015 25.051187142314646 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "footOutFront_lft_tmpJnt" -p "ankleFront_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -1.9130608572734267 2.1602070008110963 1.1321918942023803 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 7.5154868952971574 1.7763568394002505e-015 25.051187142314646 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "footInFront_lft_tmpJnt" -p "ankleFront_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 1.6744175636308567 2.1602070008110954 1.1321918942023803 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 7.5154868952971574 1.7763568394002505e-015 25.051187142314646 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "kneeFront_lft_tmpJnt" -p "lowLegFront_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 5.3290705182007514e-015 -6.4756709239955992 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 7.5154868952971698 24.327941361677972 21.346732706204833 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "clavFront_rgt_tmpJnt" -p "spine2_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -0.703476 -0.38606232600167445 8.7505382868338231 ;
	setAttr ".jo" -type "double3" -180 7.0622500768802503e-031 89.999999999999972 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1.0000000000000002 0 0 1.0000000000000002 2.2204460492503131e-016 0 0
		 0 0 1 0 1.3330595039290227 40.018746018607096 28.354729366352228 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "upLegFront_rgt_tmpJnt" -p "clavFront_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -1.5202999999999989 -3.2625639999999994 1.6098 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 7.0167092985348807e-015 -7.0167092985348752e-015 -90.000000000000057 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 7.5154868952971778 37.1378867043599 26.696138020488331 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "lowLegFront_rgt_tmpJnt" -p "upLegFront_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 -6.7600000000000016 2.0883800000000008 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 7.5154868952971698 24.327941361677972 21.346732706204833 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ankleFront_rgt_tmpJnt" -p "lowLegFront_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 4.4408920985006262e-016 -10.678049999999999 -0.82271000000000072 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 7.5154868952971601 4.0935061547482006 22.905728635082102 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ballFront_rgt_tmpJnt" -p "ankleFront_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -4.4408920985006262e-016 -2.1602097400000009 -1.1321700000000003 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 89.999999999999986 -1.9738928024388941e-014 -5.7055094273191935e-015 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 7.5154868952971574 1.7763568394002505e-015 25.051187142314646 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "toeFront_rgt_tmpJnt" -p "ballFront_rgt_tmpJnt";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" 0 -2.0057999999999989 -5.0979186150268418e-016 ;
	setAttr ".ro" 1;
	setAttr ".radi" 0.4;
createNode joint -n "thumbFrontFoot1_rgt_tmpJnt" -p "toeFront_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.6155899999999996 0.10300000000000153 -0.22719273999999992 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562648086886 3.3809123766256222 25.20550640416533 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbFrontFoot2_rgt_tmpJnt" -p "thumbFrontFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 4.4408920985006262e-016 -0.92580000000000062 -1.9428902930940239e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562389251162 3.3809123766256186 18.1141638774779 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbFrontFoot3_rgt_tmpJnt" -p "thumbFrontFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -4.4408920985006262e-016 -0.63059999999999938 -1.3877787807814457e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562464328882 3.3809123766256284 20.17107393750047 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbFrontFoot4_rgt_tmpJnt" -p "thumbFrontFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 -1.0218000000000025 -2.4980018054066022e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562648086886 3.3809123766256222 25.20550640416533 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFrontFoot1_rgt_tmpJnt" -p "toeFront_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -0.75117999999999929 -0.54879999999999818 -0.32102474000000009 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736497602165 3.0356842493631335 24.950677281414283 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFrontFoot2_rgt_tmpJnt" -p "indexFrontFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 -0.91970000000000063 0.016452999999999773 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736265256656 3.5823867347681335 18.867787120980601 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFrontFoot3_rgt_tmpJnt" -p "indexFrontFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 -0.51130000000000209 0.074670999999999876 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736355656542 3.0356842493631389 21.06178202391191 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexFrontFoot4_rgt_tmpJnt" -p "indexFrontFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 -0.90620000000000012 -1.6653345369377348e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736497602165 3.0356842493631335 24.950677281414283 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFrontFoot1_rgt_tmpJnt" -p "toeFront_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.075850000000001305 -0.62149999999999928 -0.34729174000000007 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.632825854991474 2.5509665919724842 23.422981436211693 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFrontFoot2_rgt_tmpJnt" -p "middleFrontFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0 -0.97309999999999874 0.062193999999999749 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.833229097571561 3.545618914658315 18.322361793410607 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFrontFoot3_rgt_tmpJnt" -p "middleFrontFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 -0.55850000000000222 0.12918799999999989 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.739121441737378 2.5509665939787247 20.717569587931241 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleFrontFoot4_rgt_tmpJnt" -p "middleFrontFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 -0.63090000000000046 -1.3877787807814457e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.632825854991474 2.5509665919724842 23.422981436211693 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFrontFoot1_rgt_tmpJnt" -p "toeFront_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.94612000000000096 -0.60279999999999845 -0.29157974000000009 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.225828301514859 2.8802883702174338 21.003340089239849 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFrontFoot2_rgt_tmpJnt" -p "ringFrontFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0 -0.92070000000000007 -0.013077000000000172 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.387754888646541 3.2321558806201516 16.882020110151402 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFrontFoot3_rgt_tmpJnt" -p "ringFrontFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 -0.33020000000000138 0.081992999999999927 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.332123888260764 2.8802883722236743 18.297928240959397 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringFrontFoot4_rgt_tmpJnt" -p "ringFrontFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 -0.63090000000000046 -1.6653345369377348e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.225828301514859 2.8802883702174338 21.003340089239849 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFrontFoot1_rgt_tmpJnt" -p "toeFront_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 1.7441300000000006 -0.32479999999999798 -0.20194374000000001 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.320681697400357 2.0419542914872575 17.740972323318903 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFrontFoot2_rgt_tmpJnt" -p "pinkyFrontFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 8.8817841970012523e-016 -0.58180000000000121 -1.3877787807814457e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936116 0
		 -0.14442492233936111 -2.7258954824815886e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 31.048633193268319 2.0419543052267 12.753471087826441 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFrontFoot3_rgt_tmpJnt" -p "pinkyFrontFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 -0.54359999999999964 -1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.711712061830106 2.0419542988676174 15.06185917254086 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyFrontFoot4_rgt_tmpJnt" -p "pinkyFrontFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 -0.63090000000000224 -1.3877787807814457e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.320681697400357 2.0419542914872575 17.740972323318903 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "heelFront_rgt_tmpJnt" -p "ankleFront_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 -2.1602097400000009 1.3007799999999996 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 7.0167092985348689e-015 7.016709298534876e-015 179.99999999999997 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 7.5154868952971574 1.7763568394002505e-015 25.051187142314646 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "footOutFront_rgt_tmpJnt" -p "ankleFront_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 1.9130599999999993 -2.1602097400000009 -1.1321700000000003 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 7.0167092985348689e-015 7.016709298534876e-015 179.99999999999997 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 7.5154868952971574 1.7763568394002505e-015 25.051187142314646 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "footInFront_rgt_tmpJnt" -p "ankleFront_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -1.6744200000000005 -2.1602097400000009 -1.1321700000000003 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 7.0167092985348689e-015 7.016709298534876e-015 179.99999999999997 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 7.5154868952971574 1.7763568394002505e-015 25.051187142314646 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "kneeFront_rgt_tmpJnt" -p "lowLegFront_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 0 6.4756700000000009 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 7.5154868952971698 24.327941361677972 21.346732706204833 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pelvis_tmpJnt" -p "root_tmpJnt";
	setAttr ".uoc" yes;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0 1.2209116066380084 -3.0016261407613447 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 41.40815359333795 -7.1823095455063646 1;
	setAttr ".radi" 0.4;
createNode joint -n "clavBack_lft_tmpJnt" -p "pelvis_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0.7034762777979453 1.3862499641370363 -1.9501869218145291 ;
	setAttr ".jo" -type "double3" 0 0 -89.999999999999986 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1.0000000000000002 0 0 1.0000000000000002 2.2204460492503131e-016 0 0
		 0 0 1 0 1.3330595039290227 44.035042015643654 -10.877836022533698 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "upLegBack_lft_tmpJnt" -p "clavBack_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0.028107965241520816 2.3544751849411836 -0.64883627002380528 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -90.000000000000028 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 5.7946961235399295 43.981778541971636 -12.107354889831603 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "midLegBack_lft_tmpJnt" -p "upLegBack_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 10.064141654542965 0.86040994462577025 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 5.7946961235399232 24.91063129096251 -10.476912343071207 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "lowLegBack_lft_tmpJnt" -p "midLegBack_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -2.2204460492503131e-015 6.0465270895290031 -2.7790141367894474 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 5.7946961235399215 13.45270348749499 -15.743033353144998 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ankleBack_lft_tmpJnt" -p "lowLegBack_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 1.3322676295501878e-015 4.7381686931870934 1.4065361856891805 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 5.7946961235399179 4.4740626903334206 -13.077703349662533 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ballBack_lft_tmpJnt" -p "ankleBack_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 1.7763568394002505e-015 2.3610326161389246 1.0958479522040463 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 5.7946961235399179 1.6875389974302379e-014 -11.001115163747418 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "toeBack_lft_tmpJnt" -p "ballBack_lft_tmpJnt";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 2.0058133233534985 1.3359539169366386e-015 ;
	setAttr ".ro" 1;
	setAttr ".radi" 0.4;
createNode joint -n "thumbBackFoot1_lft_tmpJnt" -p "toeBack_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 1.6822796447007013 0.038723827843982761 0.22719270718525661 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -6.361109362927028e-015 1.9738928024388945e-014 4.3829224748190706e-030 ;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562648086886 3.3809123766256222 25.20550640416533 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbBackFoot2_lft_tmpJnt" -p "thumbBackFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 2.6645352591003757e-015 0.92576787951842121 -3.1086244689504383e-015 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562389251162 3.3809123766256186 18.1141638774779 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbBackFoot3_lft_tmpJnt" -p "thumbBackFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -1.3322676295501878e-015 0.63058043554024934 3.2313158220625618e-015 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562464328882 3.3809123766256284 20.17107393750047 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbBackFoot4_lft_tmpJnt" -p "thumbBackFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 1.0218541541130968 2.1037555427542346e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562648086886 3.3809123766256222 25.20550640416533 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexBackFoot1_lft_tmpJnt" -p "toeBack_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.8178745730018 0.69050643669817102 0.32102508156220799 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -6.361109362927028e-015 1.9738928024388945e-014 4.3829224748190706e-030 ;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736497602165 3.0356842493631335 24.950677281414283 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexBackFoot2_lft_tmpJnt" -p "indexBackFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 2.7149900105882807e-009 0.91972559424033395 -0.016453541419196638 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736265256656 3.5823867347681335 18.867787120980601 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexBackFoot3_lft_tmpJnt" -p "indexBackFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -9.9510444329098391e-010 0.51124777198153026 -0.074670399598353532 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736355656542 3.0356842493631389 21.06178202391191 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexBackFoot4_lft_tmpJnt" -p "indexBackFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -2.6645352591003757e-015 0.90619583172745344 0 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736497602165 3.0356842493631335 24.950677281414283 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleBackFoot1_lft_tmpJnt" -p "toeBack_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -0.0091568120956959653 0.76317517298103699 0.34729186241259569 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -6.361109362927028e-015 1.9738928024388945e-014 4.3829224748190706e-030 ;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.632825854991474 2.5509665919724842 23.422981436211693 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleBackFoot2_lft_tmpJnt" -p "middleBackFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 5.5441438107095564e-009 0.97308055092308265 -0.062194519388097835 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.833229097571561 3.545618914658315 18.322361793410607 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleBackFoot3_lft_tmpJnt" -p "middleBackFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -1.9347714541595451e-009 0.55856531325370451 -0.12918715907726763 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.739121441737378 2.5509665939787247 20.717569587931241 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleBackFoot4_lft_tmpJnt" -p "middleBackFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 0.63090526850224382 1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.632825854991474 2.5509665919724842 23.422981436211693 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringBackFoot1_lft_tmpJnt" -p "toeBack_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -0.87942998155173013 0.74445880553439547 0.29157926391779276 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -6.361109362927028e-015 1.9738928024388945e-014 4.3829224748190706e-030 ;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.225828301514859 2.8802883702174338 21.003340089239849 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringBackFoot2_lft_tmpJnt" -p "ringBackFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 1.2997105613976601e-009 0.92072713237502946 0.013077546184747324 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.387754888646541 3.2321558806201516 16.882020110151402 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringBackFoot3_lft_tmpJnt" -p "ringBackFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 0.33019146419784917 -0.081992660476667978 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.332123888260764 2.8802883722236743 18.297928240959397 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringBackFoot4_lft_tmpJnt" -p "ringBackFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 0.63090526850224471 1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.225828301514859 2.8802883702174338 21.003340089239849 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyBackFoot1_lft_tmpJnt" -p "toeBack_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.677444269109158 0.4665109707915418 0.20194413712321646 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -6.361109362927028e-015 1.9738928024388945e-014 4.3829224748190706e-030 ;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.320681697400357 2.0419542914872575 17.740972323318903 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyBackFoot2_lft_tmpJnt" -p "pinkyBackFoot1_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 6.2172489379008766e-015 0.58181578466233619 -3.1086244689504383e-015 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936116 0
		 -0.14442492233936111 -2.7258954824815886e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 31.048633193268319 2.0419543052267 12.753471087826441 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyBackFoot3_lft_tmpJnt" -p "pinkyBackFoot2_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -4.4408920985006262e-015 0.54360309640941562 2.9420910152566648e-015 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.711712061830106 2.0419542988676174 15.06185917254086 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyBackFoot4_lft_tmpJnt" -p "pinkyBackFoot3_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 0.63090526850224382 -1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.320681697400357 2.0419542914872575 17.740972323318903 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "heelBack_lft_tmpJnt" -p "ankleBack_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 1.3322676295501878e-015 2.3610326161389232 -1.201727029718807 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 7.5154868952971574 1.7763568394002505e-015 25.051187142314646 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "footOutBack_lft_tmpJnt" -p "ankleBack_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -1.434061225806492 2.361032616138925 1.0958479522040481 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 7.5154868952971574 1.7763568394002505e-015 25.051187142314646 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "footInBack_lft_tmpJnt" -p "ankleBack_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 1.4009959975739128 2.3610326161389246 1.0958479522040481 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 7.5154868952971574 1.7763568394002505e-015 25.051187142314646 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "kneeBack_lft_tmpJnt" -p "midLegBack_lft_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 1.2434497875801753e-014 7.1036068932673313 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 5.7946961235399232 24.91063129096251 -10.476912343071207 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "tail1_tmpJnt" -p "pelvis_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0 1.367757746303635 -5.5251128137795771 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 -0.047819106915031995 -0.99885601215282704 0
		 0 0.99885601215282704 -0.047819106915031995 0 0 43.490457613506443 -17.652178081434975 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "tail2_tmpJnt" -p "tail1_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0 4.8217351749957196 7.1054273576010019e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 -0.04292412977652181 -0.99907833480810104 0
		 0 0.99907833480810104 -0.04292412977652181 0 0 43.053034220293171 -26.789174030434271 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "tail3_tmpJnt" -p "tail2_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 4.3490807426886668 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.049633887188148103 -0.99876747906737229 0
		 0 0.99876747906737229 0.049633887188148103 0 0 42.698955760836355 -35.030508671516507 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "tail4_tmpJnt" -p "tail3_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 4.4974660331311078 -3.5527136788005009e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.13955150961757307 -0.99021481314079329 0
		 0 0.99021481314079329 0.13955150961757307 0 0 43.122483506879412 -43.553027522941647 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "tail5_tmpJnt" -p "tail4_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0 4.2116948766380702 -3.5527136788005009e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.22855193375874344 -0.97353172191518167 0
		 0 0.97353172191518167 0.22855193375874344 0 0 44.247249297515893 -51.534021424436354 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "tail6_tmpJnt" -p "tail5_tmpJnt";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 0 2.0444721779100981 3.5527136788005009e-015 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 0.4;
createNode joint -n "clavBack_rgt_tmpJnt" -p "pelvis_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -0.703476 1.3862316816890292 -1.9501884039369433 ;
	setAttr ".jo" -type "double3" -180 7.0622500768802503e-031 89.999999999999972 ;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1.0000000000000002 0 0 1.0000000000000002 2.2204460492503131e-016 0 0
		 0 0 1 0 1.3330595039290227 44.035042015643654 -10.877836022533698 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "upLegBack_rgt_tmpJnt" -p "clavBack_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -0.02809999999999846 -2.3544739999999997 0.64883000000000024 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 7.0167092985348807e-015 -7.0167092985348752e-015 -90.000000000000057 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 5.7946961235399295 43.981778541971636 -12.107354889831603 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "midLegBack_rgt_tmpJnt" -p "upLegBack_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 -10.0641 -0.8604099999999999 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 5.7946961235399232 24.91063129096251 -10.476912343071207 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "lowLegBack_rgt_tmpJnt" -p "midLegBack_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -4.4408920985006262e-016 -6.04656 2.7789900000000003 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 5.7946961235399215 13.45270348749499 -15.743033353144998 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ankleBack_rgt_tmpJnt" -p "lowLegBack_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 0 -4.73817 -1.4065100000000008 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 5.7946961235399179 4.4740626903334206 -13.077703349662533 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ballBack_rgt_tmpJnt" -p "ankleBack_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 4.4408920985006262e-016 -2.3610297400000002 -1.0958500000000004 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 89.999999999999986 -1.9738928024388941e-014 -5.7055094273191935e-015 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 5.7946961235399179 1.6875389974302379e-014 -11.001115163747418 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "toeBack_rgt_tmpJnt" -p "ballBack_rgt_tmpJnt";
	setAttr ".uoc" yes;
	setAttr ".t" -type "double3" -8.8817841970012523e-016 -2.0058099999999994 -5.0979186150268418e-016 ;
	setAttr ".ro" 1;
	setAttr ".radi" 0.4;
createNode joint -n "thumbBackFoot1_rgt_tmpJnt" -p "toeBack_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -1.6822799999999996 -0.038719999999999644 -0.22719273999999995 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562648086886 3.3809123766256222 25.20550640416533 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbBackFoot2_rgt_tmpJnt" -p "thumbBackFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 8.8817841970012523e-016 -0.92576999999999909 -1.9428902930940239e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562389251162 3.3809123766256186 18.1141638774779 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbBackFoot3_rgt_tmpJnt" -p "thumbBackFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 -0.63057999999999925 -1.3877787807814457e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562464328882 3.3809123766256284 20.17107393750047 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "thumbBackFoot4_rgt_tmpJnt" -p "thumbBackFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 2.2204460492503131e-016 -1.021850000000001 -2.4980018054066022e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 14.483562648086886 3.3809123766256222 25.20550640416533 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexBackFoot1_rgt_tmpJnt" -p "toeBack_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -0.81786999999999965 -0.69049999999999923 -0.32102474000000014 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736497602165 3.0356842493631335 24.950677281414283 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexBackFoot2_rgt_tmpJnt" -p "indexBackFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 8.8817841970012523e-016 -0.9197299999999986 0.016452999999999718 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736265256656 3.5823867347681335 18.867787120980601 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexBackFoot3_rgt_tmpJnt" -p "indexBackFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -4.4408920985006262e-016 -0.51125000000000131 0.07467099999999996 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736355656542 3.0356842493631389 21.06178202391191 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "indexBackFoot4_rgt_tmpJnt" -p "indexBackFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 -0.90618999999999916 -2.2204460492503131e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99999999999999911 -1.887412057596498e-008 3.6500241499888554e-008 0
		 3.6500241499888581e-008 -1.4172455962248266e-015 0.99999999999999933 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 19.654736497602165 3.0356842493631335 24.950677281414283 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleBackFoot1_rgt_tmpJnt" -p "toeBack_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.0091600000000005011 -0.76316999999999791 -0.34729174000000013 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.632825854991474 2.5509665919724842 23.422981436211693 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleBackFoot2_rgt_tmpJnt" -p "middleBackFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 4.4408920985006262e-016 -0.97308000000000039 0.062193999999999749 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.833229097571561 3.545618914658315 18.322361793410607 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleBackFoot3_rgt_tmpJnt" -p "middleBackFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -4.4408920985006262e-016 -0.55857000000000046 0.12918799999999986 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.739121441737378 2.5509665939787247 20.717569587931241 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "middleBackFoot4_rgt_tmpJnt" -p "middleBackFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 -0.63089999999999957 -1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 23.632825854991474 2.5509665919724842 23.422981436211693 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringBackFoot1_rgt_tmpJnt" -p "toeBack_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.8794300000000006 -0.74445999999999835 -0.29157974000000014 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.225828301514859 2.8802883702174338 21.003340089239849 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringBackFoot2_rgt_tmpJnt" -p "ringBackFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 8.8817841970012523e-016 -0.92071999999999932 -0.013077000000000172 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.387754888646541 3.2321558806201516 16.882020110151402 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringBackFoot3_rgt_tmpJnt" -p "ringBackFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -4.4408920985006262e-016 -0.33020000000000138 0.081992999999999899 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.332123888260764 2.8802883722236743 18.297928240959397 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "ringBackFoot4_rgt_tmpJnt" -p "ringBackFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 -0.63090000000000046 -1.3877787807814457e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.99922904108406752 -1.8859569321739476e-008 -0.03925969248751842 0
		 -0.039259692487518413 -7.4099427431652264e-010 0.99922904108406774 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 27.225828301514859 2.8802883702174338 21.003340089239849 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyBackFoot1_rgt_tmpJnt" -p "toeBack_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 1.6774500000000008 -0.46650999999999776 -0.20194374000000004 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.320681697400357 2.0419542914872575 17.740972323318903 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyBackFoot2_rgt_tmpJnt" -p "pinkyBackFoot1_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0 -0.58182000000000045 -1.3877787807814457e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936116 0
		 -0.14442492233936111 -2.7258954824815886e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 31.048633193268319 2.0419543052267 12.753471087826441 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyBackFoot3_rgt_tmpJnt" -p "pinkyBackFoot2_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 -0.54359999999999964 -1.1102230246251565e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.711712061830106 2.0419542988676174 15.06185917254086 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "pinkyBackFoot4_rgt_tmpJnt" -p "pinkyBackFoot3_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 -0.63090000000000135 -1.3877787807814457e-016 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -0.98951576127278995 -1.8676239485899002e-008 -0.14442492233936113 0
		 -0.14442492233936108 -2.7258954824815882e-009 0.98951576127279017 0 -1.887412057596492e-008 0.99999999999999978 1.9984014443252818e-015 0
		 30.320681697400357 2.0419542914872575 17.740972323318903 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "heelBack_rgt_tmpJnt" -p "ankleBack_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 0 -2.3610297400000002 1.2017100000000003 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 7.0167092985348689e-015 7.016709298534876e-015 179.99999999999997 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 7.5154868952971574 1.7763568394002505e-015 25.051187142314646 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "footOutBack_rgt_tmpJnt" -p "ankleBack_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 1.4340600000000001 -2.3610297400000002 -1.0958500000000004 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 7.0167092985348689e-015 7.016709298534876e-015 179.99999999999997 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 7.5154868952971574 1.7763568394002505e-015 25.051187142314646 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "footInBack_rgt_tmpJnt" -p "ankleBack_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -1.4009899999999997 -2.3610297400000002 -1.0958500000000004 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 7.0167092985348689e-015 7.016709298534876e-015 179.99999999999997 ;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -4.930380657631326e-032 -2.2204460492503136e-016 1.0000000000000002 0
		 2.2204460492503146e-016 1.0000000000000004 2.2204460492503131e-016 0 7.5154868952971574 1.7763568394002505e-015 25.051187142314646 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode joint -n "kneeBack_rgt_tmpJnt" -p "midLegBack_rgt_tmpJnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" yes;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 8.8817841970012523e-016 0 -7.1036 ;
	setAttr ".ro" 1;
	setAttr ".bps" -type "matrix" -1.0000000000000002 2.2204460492503141e-016 0 0 -2.2204460492503141e-016 -1.0000000000000002 0 0
		 0 0 1 0 5.7946961235399232 24.91063129096251 -10.476912343071207 1;
	setAttr ".radi" 0.4;
	setAttr ".liw" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr "spine1_tmpJnt.s" "spine2_tmpJnt.is";
connectAttr "spine2_tmpJnt.s" "neck_tmpJnt.is";
connectAttr "neck_tmpJnt.s" "head1_tmpJnt.is";
connectAttr "head1_tmpJnt.s" "head2_tmpJnt.is";
connectAttr "head2_tmpJnt.s" "ear1_lft_tmpJnt.is";
connectAttr "ear1_lft_tmpJnt.s" "ear2_lft_tmpJnt.is";
connectAttr "ear2_lft_tmpJnt.s" "ear3_lft_tmpJnt.is";
connectAttr "ear3_lft_tmpJnt.s" "ear4_lft_tmpJnt.is";
connectAttr "head2_tmpJnt.s" "ear1_rgt_tmpJnt.is";
connectAttr "ear1_rgt_tmpJnt.s" "ear2_rgt_tmpJnt.is";
connectAttr "ear2_rgt_tmpJnt.s" "ear3_rgt_tmpJnt.is";
connectAttr "ear3_rgt_tmpJnt.s" "ear4_rgt_tmpJnt.is";
connectAttr "head1_tmpJnt.s" "jaw1_lwr_tmpJnt.is";
connectAttr "jaw1_lwr_tmpJnt.s" "jaw2_lwr_tmpJnt.is";
connectAttr "jaw2_lwr_tmpJnt.s" "jaw3_lwr_tmpJnt.is";
connectAttr "jaw2_lwr_tmpJnt.s" "teeth_lwr_tmpJnt.is";
connectAttr "jaw1_lwr_tmpJnt.s" "tongue1_tmpJnt.is";
connectAttr "tongue1_tmpJnt.s" "tongue2_tmpJnt.is";
connectAttr "tongue3_tmpJnt.s" "tongue4_tmpJnt.is";
connectAttr "tongue5_tmpJnt.s" "tongue6_tmpJnt.is";
connectAttr "head1_tmpJnt.s" "jaw1_upr_tmpJnt.is";
connectAttr "jaw1_upr_tmpJnt.s" "jaw2_upr_tmpJnt.is";
connectAttr "jaw1_upr_tmpJnt.s" "teeth_upr_tmpJnt.is";
connectAttr "head1_tmpJnt.s" "eye_lft_tmpJnt.is";
connectAttr "head1_tmpJnt.s" "eye_rgt_tmpJnt.is";
connectAttr "head1_tmpJnt.s" "eyeTrgt_tmpJnt.is";
connectAttr "eyeTrgt_tmpJnt.s" "eyeTrgt_lft_tmpJnt.is";
connectAttr "eyeTrgt_tmpJnt.s" "eyeTrgt_rgt_tmpJnt.is";
connectAttr "spine2_tmpJnt.s" "clavFront_lft_tmpJnt.is";
connectAttr "upLegFront_lft_tmpJnt.s" "lowLegFront_lft_tmpJnt.is";
connectAttr "lowLegFront_lft_tmpJnt.s" "ankleFront_lft_tmpJnt.is";
connectAttr "ankleFront_lft_tmpJnt.s" "ballFront_lft_tmpJnt.is";
connectAttr "ballFront_lft_tmpJnt.s" "toeFront_lft_tmpJnt.is";
connectAttr "toeFront_lft_tmpJnt.s" "thumbFrontFoot1_lft_tmpJnt.is";
connectAttr "thumbFrontFoot1_lft_tmpJnt.s" "thumbFrontFoot2_lft_tmpJnt.is";
connectAttr "thumbFrontFoot2_lft_tmpJnt.s" "thumbFrontFoot3_lft_tmpJnt.is";
connectAttr "toeFront_lft_tmpJnt.s" "indexFrontFoot1_lft_tmpJnt.is";
connectAttr "indexFrontFoot1_lft_tmpJnt.s" "indexFrontFoot2_lft_tmpJnt.is";
connectAttr "indexFrontFoot2_lft_tmpJnt.s" "indexFrontFoot3_lft_tmpJnt.is";
connectAttr "toeFront_lft_tmpJnt.s" "middleFrontFoot1_lft_tmpJnt.is";
connectAttr "middleFrontFoot1_lft_tmpJnt.s" "middleFrontFoot2_lft_tmpJnt.is";
connectAttr "middleFrontFoot2_lft_tmpJnt.s" "middleFrontFoot3_lft_tmpJnt.is";
connectAttr "toeFront_lft_tmpJnt.s" "ringFrontFoot1_lft_tmpJnt.is";
connectAttr "ringFrontFoot1_lft_tmpJnt.s" "ringFrontFoot2_lft_tmpJnt.is";
connectAttr "ringFrontFoot2_lft_tmpJnt.s" "ringFrontFoot3_lft_tmpJnt.is";
connectAttr "toeFront_lft_tmpJnt.s" "pinkyFrontFoot1_lft_tmpJnt.is";
connectAttr "pinkyFrontFoot1_lft_tmpJnt.s" "pinkyFrontFoot2_lft_tmpJnt.is";
connectAttr "pinkyFrontFoot2_lft_tmpJnt.s" "pinkyFrontFoot3_lft_tmpJnt.is";
connectAttr "ankleFront_lft_tmpJnt.s" "heelFront_lft_tmpJnt.is";
connectAttr "ankleFront_lft_tmpJnt.s" "footOutFront_lft_tmpJnt.is";
connectAttr "ankleFront_lft_tmpJnt.s" "footInFront_lft_tmpJnt.is";
connectAttr "lowLegFront_lft_tmpJnt.s" "kneeFront_lft_tmpJnt.is";
connectAttr "spine2_tmpJnt.s" "clavFront_rgt_tmpJnt.is";
connectAttr "upLegFront_rgt_tmpJnt.s" "lowLegFront_rgt_tmpJnt.is";
connectAttr "lowLegFront_rgt_tmpJnt.s" "ankleFront_rgt_tmpJnt.is";
connectAttr "ankleFront_rgt_tmpJnt.s" "ballFront_rgt_tmpJnt.is";
connectAttr "ballFront_rgt_tmpJnt.s" "toeFront_rgt_tmpJnt.is";
connectAttr "toeFront_rgt_tmpJnt.s" "thumbFrontFoot1_rgt_tmpJnt.is";
connectAttr "thumbFrontFoot1_rgt_tmpJnt.s" "thumbFrontFoot2_rgt_tmpJnt.is";
connectAttr "thumbFrontFoot2_rgt_tmpJnt.s" "thumbFrontFoot3_rgt_tmpJnt.is";
connectAttr "toeFront_rgt_tmpJnt.s" "indexFrontFoot1_rgt_tmpJnt.is";
connectAttr "indexFrontFoot1_rgt_tmpJnt.s" "indexFrontFoot2_rgt_tmpJnt.is";
connectAttr "indexFrontFoot2_rgt_tmpJnt.s" "indexFrontFoot3_rgt_tmpJnt.is";
connectAttr "toeFront_rgt_tmpJnt.s" "middleFrontFoot1_rgt_tmpJnt.is";
connectAttr "middleFrontFoot1_rgt_tmpJnt.s" "middleFrontFoot2_rgt_tmpJnt.is";
connectAttr "middleFrontFoot2_rgt_tmpJnt.s" "middleFrontFoot3_rgt_tmpJnt.is";
connectAttr "toeFront_rgt_tmpJnt.s" "ringFrontFoot1_rgt_tmpJnt.is";
connectAttr "ringFrontFoot1_rgt_tmpJnt.s" "ringFrontFoot2_rgt_tmpJnt.is";
connectAttr "ringFrontFoot2_rgt_tmpJnt.s" "ringFrontFoot3_rgt_tmpJnt.is";
connectAttr "toeFront_rgt_tmpJnt.s" "pinkyFrontFoot1_rgt_tmpJnt.is";
connectAttr "pinkyFrontFoot1_rgt_tmpJnt.s" "pinkyFrontFoot2_rgt_tmpJnt.is";
connectAttr "pinkyFrontFoot2_rgt_tmpJnt.s" "pinkyFrontFoot3_rgt_tmpJnt.is";
connectAttr "ankleFront_rgt_tmpJnt.s" "heelFront_rgt_tmpJnt.is";
connectAttr "ankleFront_rgt_tmpJnt.s" "footOutFront_rgt_tmpJnt.is";
connectAttr "ankleFront_rgt_tmpJnt.s" "footInFront_rgt_tmpJnt.is";
connectAttr "lowLegFront_rgt_tmpJnt.s" "kneeFront_rgt_tmpJnt.is";
connectAttr "root_tmpJnt.s" "pelvis_tmpJnt.is";
connectAttr "pelvis_tmpJnt.s" "clavBack_lft_tmpJnt.is";
connectAttr "clavBack_lft_tmpJnt.s" "upLegBack_lft_tmpJnt.is";
connectAttr "upLegBack_lft_tmpJnt.s" "midLegBack_lft_tmpJnt.is";
connectAttr "midLegBack_lft_tmpJnt.s" "lowLegBack_lft_tmpJnt.is";
connectAttr "lowLegBack_lft_tmpJnt.s" "ankleBack_lft_tmpJnt.is";
connectAttr "ankleBack_lft_tmpJnt.s" "ballBack_lft_tmpJnt.is";
connectAttr "ballBack_lft_tmpJnt.s" "toeBack_lft_tmpJnt.is";
connectAttr "toeBack_lft_tmpJnt.s" "thumbBackFoot1_lft_tmpJnt.is";
connectAttr "thumbBackFoot1_lft_tmpJnt.s" "thumbBackFoot2_lft_tmpJnt.is";
connectAttr "thumbBackFoot2_lft_tmpJnt.s" "thumbBackFoot3_lft_tmpJnt.is";
connectAttr "toeBack_lft_tmpJnt.s" "indexBackFoot1_lft_tmpJnt.is";
connectAttr "indexBackFoot1_lft_tmpJnt.s" "indexBackFoot2_lft_tmpJnt.is";
connectAttr "indexBackFoot2_lft_tmpJnt.s" "indexBackFoot3_lft_tmpJnt.is";
connectAttr "toeBack_lft_tmpJnt.s" "middleBackFoot1_lft_tmpJnt.is";
connectAttr "middleBackFoot1_lft_tmpJnt.s" "middleBackFoot2_lft_tmpJnt.is";
connectAttr "middleBackFoot2_lft_tmpJnt.s" "middleBackFoot3_lft_tmpJnt.is";
connectAttr "toeBack_lft_tmpJnt.s" "ringBackFoot1_lft_tmpJnt.is";
connectAttr "ringBackFoot1_lft_tmpJnt.s" "ringBackFoot2_lft_tmpJnt.is";
connectAttr "ringBackFoot2_lft_tmpJnt.s" "ringBackFoot3_lft_tmpJnt.is";
connectAttr "toeBack_lft_tmpJnt.s" "pinkyBackFoot1_lft_tmpJnt.is";
connectAttr "pinkyBackFoot1_lft_tmpJnt.s" "pinkyBackFoot2_lft_tmpJnt.is";
connectAttr "pinkyBackFoot2_lft_tmpJnt.s" "pinkyBackFoot3_lft_tmpJnt.is";
connectAttr "ankleBack_lft_tmpJnt.s" "heelBack_lft_tmpJnt.is";
connectAttr "ankleBack_lft_tmpJnt.s" "footOutBack_lft_tmpJnt.is";
connectAttr "ankleBack_lft_tmpJnt.s" "footInBack_lft_tmpJnt.is";
connectAttr "midLegBack_lft_tmpJnt.s" "kneeBack_lft_tmpJnt.is";
connectAttr "pelvis_tmpJnt.s" "tail1_tmpJnt.is";
connectAttr "tail1_tmpJnt.s" "tail2_tmpJnt.is";
connectAttr "tail2_tmpJnt.s" "tail3_tmpJnt.is";
connectAttr "tail3_tmpJnt.s" "tail4_tmpJnt.is";
connectAttr "tail4_tmpJnt.s" "tail5_tmpJnt.is";
connectAttr "tail5_tmpJnt.s" "tail6_tmpJnt.is";
connectAttr "pelvis_tmpJnt.s" "clavBack_rgt_tmpJnt.is";
connectAttr "clavBack_rgt_tmpJnt.s" "upLegBack_rgt_tmpJnt.is";
connectAttr "upLegBack_rgt_tmpJnt.s" "midLegBack_rgt_tmpJnt.is";
connectAttr "midLegBack_rgt_tmpJnt.s" "lowLegBack_rgt_tmpJnt.is";
connectAttr "lowLegBack_rgt_tmpJnt.s" "ankleBack_rgt_tmpJnt.is";
connectAttr "ankleBack_rgt_tmpJnt.s" "ballBack_rgt_tmpJnt.is";
connectAttr "ballBack_rgt_tmpJnt.s" "toeBack_rgt_tmpJnt.is";
connectAttr "toeBack_rgt_tmpJnt.s" "thumbBackFoot1_rgt_tmpJnt.is";
connectAttr "thumbBackFoot1_rgt_tmpJnt.s" "thumbBackFoot2_rgt_tmpJnt.is";
connectAttr "thumbBackFoot2_rgt_tmpJnt.s" "thumbBackFoot3_rgt_tmpJnt.is";
connectAttr "toeBack_rgt_tmpJnt.s" "indexBackFoot1_rgt_tmpJnt.is";
connectAttr "indexBackFoot1_rgt_tmpJnt.s" "indexBackFoot2_rgt_tmpJnt.is";
connectAttr "indexBackFoot2_rgt_tmpJnt.s" "indexBackFoot3_rgt_tmpJnt.is";
connectAttr "toeBack_rgt_tmpJnt.s" "middleBackFoot1_rgt_tmpJnt.is";
connectAttr "middleBackFoot1_rgt_tmpJnt.s" "middleBackFoot2_rgt_tmpJnt.is";
connectAttr "middleBackFoot2_rgt_tmpJnt.s" "middleBackFoot3_rgt_tmpJnt.is";
connectAttr "toeBack_rgt_tmpJnt.s" "ringBackFoot1_rgt_tmpJnt.is";
connectAttr "ringBackFoot1_rgt_tmpJnt.s" "ringBackFoot2_rgt_tmpJnt.is";
connectAttr "ringBackFoot2_rgt_tmpJnt.s" "ringBackFoot3_rgt_tmpJnt.is";
connectAttr "toeBack_rgt_tmpJnt.s" "pinkyBackFoot1_rgt_tmpJnt.is";
connectAttr "pinkyBackFoot1_rgt_tmpJnt.s" "pinkyBackFoot2_rgt_tmpJnt.is";
connectAttr "pinkyBackFoot2_rgt_tmpJnt.s" "pinkyBackFoot3_rgt_tmpJnt.is";
connectAttr "ankleBack_rgt_tmpJnt.s" "heelBack_rgt_tmpJnt.is";
connectAttr "ankleBack_rgt_tmpJnt.s" "footOutBack_rgt_tmpJnt.is";
connectAttr "ankleBack_rgt_tmpJnt.s" "footInBack_rgt_tmpJnt.is";
connectAttr "midLegBack_rgt_tmpJnt.s" "kneeBack_rgt_tmpJnt.is";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of tmpJntAnimal.ma
