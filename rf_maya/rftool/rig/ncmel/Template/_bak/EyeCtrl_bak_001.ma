//Maya ASCII 2018ff09 scene
//Name: EyeCtrl.ma
//Last modified: Mon, Jan 13, 2020 03:45:09 PM
//Codeset: 1252
requires maya "2018ff09";
requires -nodeType "RedshiftOptions" -nodeType "RedshiftPostEffects" "redshift4maya" "2.6.43";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201903222215-65bada0e52";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	rename -uid "A314156C-47CE-C443-3C38-1FA7A4D934C6";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.9353728066447795 11.712270147240494 2.9299307233322907 ;
	setAttr ".r" -type "double3" 7.4616472703957006 66.200000000000287 -9.8519134971101826e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "5875061E-4C88-DD5C-BF8B-92A0F6E0F128";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 4.1906121779323708;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.73815861587319309 12.173351363070683 1.0706577569733853 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "171E05B5-4876-E20B-D2C0-05A924B38B62";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "BBA78F1A-4D1D-6295-87A9-8F85C0E9CF49";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 9.0011155200000044;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "B88253D3-4725-1C49-5306-6F895B7ADE15";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.013148067394595689 12.190500704141785 1000.1743735783393 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "4E3EF5A0-4658-04D6-3D00-87BB385C9D8B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 999.10489072115581;
	setAttr ".ow" 1.2651864576;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" -0.013148067394595689 12.190500704141785 1.0694828571834751 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "88B6326C-4B2D-91D6-A078-C3A44BCCBDF3";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "3E50236E-47D3-A4E0-C2E3-21B26C7CA2E3";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "BshRigCtrl_C_Grp";
	rename -uid "A244AB38-4FA4-F7B4-EA81-0C816DD347F3";
	setAttr ".t" -type "double3" 0 11.826261201700408 -0.33645801058560054 ;
	setAttr ".s" -type "double3" 0.344 0.344 0.344 ;
createNode transform -n "FacialBshCtrlZro_C_Grp" -p "BshRigCtrl_C_Grp";
	rename -uid "DCDD229E-425D-7B94-0B48-299FCB603C48";
createNode transform -n "EyeLidsBshCtrlZro_C_Grp" -p "FacialBshCtrlZro_C_Grp";
	rename -uid "B9BB0260-426D-DF1A-6A94-FCBA67B30607";
	setAttr ".t" -type "double3" 0 1.0513126839743592 4.0870374063054529 ;
	setAttr ".r" -type "double3" 0 0 -90.000000000000028 ;
	setAttr ".s" -type "double3" 0.76442252294160862 0.76442252294160862 0.76442252294160862 ;
createNode transform -n "EyeLidsBsh_C_Ctrl" -p "EyeLidsBshCtrlZro_C_Grp";
	rename -uid "9728AFEA-4FD6-8523-5280-7A9EEB455991";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" 0 0 1.9984014443252865e-15 ;
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mntl" -type "double3" -0.5 -0.5 -1 ;
	setAttr ".mxtl" -type "double3" 0.5 0.5 1 ;
	setAttr ".mtxe" yes;
	setAttr ".mtye" yes;
	setAttr ".xtxe" yes;
	setAttr ".xtye" yes;
	setAttr ".mnrl" -type "double3" -45 -45 -29.999999999999996 ;
	setAttr ".mxrl" -type "double3" 45 45 29.999999999999996 ;
	setAttr ".mrze" yes;
	setAttr ".xrze" yes;
createNode nurbsCurve -n "EyeLidsBsh_C_CtrlShape" -p "EyeLidsBsh_C_Ctrl";
	rename -uid "2B3554F9-4516-4BB5-CC54-DCACAB76291A";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 43 0 no 3
		44 1 2 3 4 5 6 7 8 9 10 11 13 14 15 16 17 18 19 20 21 22 23 25 26 27 28 29
		 30 31 32 33 34 35 37 38 39 40 41 42 43 44 45 46 47
		44
		0.99082340927807466 -0.0017238739503836536 -1.2884893724789053e-15
		0.97863953267814729 0.10263594706333978 -1.2884893724789053e-15
		0.94203228051417787 0.206122011050896 -1.3717560993257921e-15
		0.88190035056062344 0.30449047168986537 -1.3301227359023487e-15
		0.79972665298133272 0.39531878993784086 -1.3440005237101632e-15
		0.6975334141544256 0.4763711781305533 -1.2607337968632764e-15
		0.57783626453542125 0.5456506559529527 -1.3162449480945343e-15
		0.44358432768055661 0.60145180510041407 -1.3162449480945343e-15
		0.29808228877170007 0.64240140357924835 -1.3717560993257921e-15
		0.14491344717664748 0.66748960772321453 -1.3717560993257921e-15
		-0.012151232110831241 0.67610048281520874 -1.2884893724789053e-15
		-0.012151232110831241 0.67610048281520874 -1.2884893724789053e-15
		-0.1692441046584463 0.66802025730676184 -1.3717560993257921e-15
		-0.32249682593434686 0.64344963573808844 -1.3717560993257921e-15
		-0.46813637169426425 0.60299181012848824 -1.3162449480945343e-15
		-0.60257605153442295 0.54764451317610707 -1.3162449480945343e-15
		-0.72250656007195024 0.47876979505553285 -1.2607337968632764e-15
		-0.82497302928548877 0.39806310152527835 -1.3440005237101632e-15
		-0.90745309681756015 0.30751290312950841 -1.3301227359023487e-15
		-0.96791699482670379 0.20934814323085899 -1.3717560993257921e-15
		-1.0048736377193508 0.10598633743810898 -1.2884893724789053e-15
		-1.0174157258463279 -0.0017238400288297209 -1.2884893724789053e-15
		-1.0174157258463279 -0.0017238400288297209 -1.2884893724789053e-15
		-1.0052318492464003 -0.10608366104255318 -1.2884893724789053e-15
		-0.96862459708243154 -0.20956972503010945 -1.2052226456320186e-15
		-0.90849266712887766 -0.3079381856690781 -1.246856009055462e-15
		-0.82631896954958606 -0.39876650391705376 -1.2329782212476475e-15
		-0.72412573072267949 -0.47981889210976547 -1.3162449480945343e-15
		-0.60442858110367603 -0.54909836993216576 -1.2607337968632764e-15
		-0.47017664424881295 -0.60489951907962725 -1.2607337968632764e-15
		-0.32467460533995612 -0.64584911755846131 -1.2052226456320186e-15
		-0.17150576374490309 -0.67093732170242815 -1.2052226456320186e-15
		-0.01444108445742443 -0.67954819679442102 -1.2884893724789053e-15
		-0.01444108445742443 -0.67954819679442102 -1.2884893724789053e-15
		0.14265178809019027 -0.67146797128597357 -1.2052226456320186e-15
		0.29590450936609058 -0.64689734971730128 -1.2052226456320186e-15
		0.44154405512600986 -0.60643952410770174 -1.2607337968632764e-15
		0.57598373496616906 -0.55109222715532058 -1.2607337968632764e-15
		0.69591424350369657 -0.48221750903474625 -1.3162449480945343e-15
		0.79838071271723521 -0.40151081550449153 -1.2329782212476475e-15
		0.88086078024930725 -0.31096061710872142 -1.246856009055462e-15
		0.94132467825844968 -0.21279585721007216 -1.2052226456320186e-15
		0.978281321151099 -0.10943405141732243 -1.2884893724789053e-15
		0.99082340927807466 -0.0017238739503836536 -1.2884893724789053e-15
		;
createNode transform -n "EyeLidsUprBshCtrlZro_C_Grp" -p "EyeLidsBsh_C_Ctrl";
	rename -uid "C11E1FCF-404F-D9E5-3688-FE93C5AEE63D";
	setAttr ".t" -type "double3" -6.802207541655088e-15 0.90584864387993802 8.4440016262884263e-16 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
createNode transform -n "EyeLidsUprBsh_C_Ctrl" -p "EyeLidsUprBshCtrlZro_C_Grp";
	rename -uid "7F159033-4408-036C-165C-FEA1C147CEAA";
	addAttr -ci true -sn "eyeLids" -ln "eyeLids" -at "double";
	addAttr -ci true -sn "lidsInUD" -ln "lidsInUD" -min -10 -max 10 -at "double";
	addAttr -ci true -sn "lidsMidUD" -ln "lidsMidUD" -min -10 -max 10 -at "double";
	addAttr -ci true -sn "lidsOutUD" -ln "lidsOutUD" -min -10 -max 10 -at "double";
	addAttr -ci true -sn "lidsTurnFC" -ln "lidsTurnFC" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".t" -type "double3" -3.9968028886505493e-15 0 -3.7747582837255449e-15 ;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mntl" -type "double3" -1 -0.5 -1 ;
	setAttr ".mxtl" -type "double3" 1 1.5 1 ;
	setAttr ".mtye" yes;
	setAttr ".xtye" yes;
	setAttr -l on -k on ".eyeLids";
	setAttr -k on ".lidsInUD";
	setAttr -k on ".lidsMidUD";
	setAttr -k on ".lidsOutUD";
	setAttr -k on ".lidsTurnFC";
createNode nurbsCurve -n "EyeLidsUprBsh_C_CtrlShape" -p "EyeLidsUprBsh_C_Ctrl";
	rename -uid "C8A05D11-4A92-E46E-C378-3EB4D91B81D0";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".cc" -type "nurbsCurve" 
		1 43 0 no 3
		44 1 2 3 4 5 6 7 8 9 10 11 13 14 15 16 17 18 19 20 21 22 23 25 26 27 28 29
		 30 31 32 33 34 35 37 38 39 40 41 42 43 44 45 46 47
		44
		-0.099443098422977758 0.049217281738067047 9.8403619619843158e-18
		-0.092287945769431265 0.023419250570031001 9.8403619619843158e-18
		-0.082408809080342066 -0.0017435156386027032 6.8219605696323463e-19
		-0.071017872932817444 -0.025651518587196609 5.2612790094737782e-18
		-0.058813098207550783 -0.047715972112498825 3.73491802530359e-18
		-0.046489269782594551 -0.067393747667264886 1.2893083930324671e-17
		-0.033511008795473571 -0.08420002049447349 6.7876399936439502e-18
		-0.02245657412952675 -0.097721080037575056 6.7876399936439502e-18
		-0.010478509216864247 -0.10762418410221934 6.8219605696323463e-19
		0.002128200246792054 -0.11366511371357876 6.8219605696323463e-19
		0.015053181585218673 -0.11569556373764578 9.8403619619843158e-18
		0.015053181585218673 -0.11569556373764578 9.8403619619843158e-18
		0.027978162923645227 -0.11366511371357875 6.8219605696323463e-19
		0.040584872387301497 -0.1076241841022193 6.8219605696323463e-19
		0.052562937299963974 -0.097721080037575056 6.7876399936439502e-18
		0.063617371965910802 -0.084200020494473449 6.7876399936439502e-18
		0.076595632953031789 -0.067393747667264844 1.2893083930324671e-17
		0.088919461377988354 -0.047715972112498894 3.73491802530359e-18
		0.10112423610325491 -0.025651518587196623 5.2612790094737782e-18
		0.11251517225077952 -0.0017435156386026685 6.8219605696323463e-19
		0.12239430893986865 0.023419250570030997 9.8403619619843158e-18
		0.12954946159341499 0.049217281738067054 9.8403619619843158e-18
		0.12954946159341499 0.049217281738067054 9.8403619619843158e-18
		0.13172022838296776 0.066122742851700014 9.8403619619843158e-18
		0.1289393114043961 0.075059355992172491 1.8998527867005405e-17
		0.12030006650564966 0.082035537702758066 1.4419444914494843e-17
		0.1106153975020034 0.08691836851466761 1.5945805898665024e-17
		0.098577643434162476 0.091273031034565272 6.7876399936439502e-18
		0.084483130916970225 0.094992234263412537 1.2893083930324671e-17
		0.06867912321641173 0.097984424669171524 1.2893083930324671e-17
		0.05155464459421237 0.10017596689816607 1.8998527867005405e-17
		0.033531422289043225 0.10151281565379476 1.8998527867005405e-17
		0.015053181585218629 0.10196215123005004 9.8403619619843158e-18
		0.015053181585218629 0.10196215123005004 9.8403619619843158e-18
		-0.0034250591186059717 0.10151281565379475 1.8998527867005405e-17
		-0.021448281423775167 0.1001759668981661 1.8998527867005405e-17
		-0.038572760045974618 0.097984424669171469 1.2893083930324671e-17
		-0.05437676774653296 0.094992234263412495 1.2893083930324671e-17
		-0.068471280263725093 0.091273031034565272 6.7876399936439502e-18
		-0.080509034331566032 0.086918368514667624 1.5945805898665024e-17
		-0.090193703335212233 0.08203553770275808 1.4419444914494843e-17
		-0.098832948233958981 0.075059355992172505 1.8998527867005405e-17
		-0.10161386521253052 0.06612274285170007 9.8403619619843158e-18
		-0.099443098422977758 0.049217281738067047 9.8403619619843158e-18
		;
createNode transform -n "EyeLidsLwrBshCtrlZro_C_Grp" -p "EyeLidsBsh_C_Ctrl";
	rename -uid "20C0CB17-4F1A-842A-608F-66B546388A45";
	setAttr ".t" -type "double3" 0 -0.9 0 ;
createNode transform -n "EyeLidsLwrBsh_C_Ctrl" -p "EyeLidsLwrBshCtrlZro_C_Grp";
	rename -uid "97486923-4913-0BB8-FB9D-B39FE638B05F";
	addAttr -ci true -sn "eyeLids" -ln "eyeLids" -at "double";
	addAttr -ci true -sn "lidsInUD" -ln "lidsInUD" -min -10 -max 10 -at "double";
	addAttr -ci true -sn "lidsMidUD" -ln "lidsMidUD" -min -10 -max 10 -at "double";
	addAttr -ci true -sn "lidsOutUD" -ln "lidsOutUD" -min -10 -max 10 -at "double";
	addAttr -ci true -sn "lidsTurnFC" -ln "lidsTurnFC" -min -10 -max 10 -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".mntl" -type "double3" -1 -0.5 -1 ;
	setAttr ".mxtl" -type "double3" 1 1.5 1 ;
	setAttr ".mtye" yes;
	setAttr ".xtye" yes;
	setAttr -l on -k on ".eyeLids";
	setAttr -k on ".lidsInUD";
	setAttr -k on ".lidsMidUD";
	setAttr -k on ".lidsOutUD";
	setAttr -k on ".lidsTurnFC";
createNode nurbsCurve -n "EyeLidsLwrBsh_C_CtrlShape" -p "EyeLidsLwrBsh_C_Ctrl";
	rename -uid "A255AC8E-45C2-E587-9EA8-22B27C03B8E4";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 43 0 no 3
		44 1 2 3 4 5 6 7 8 9 10 11 13 14 15 16 17 18 19 20 21 22 23 25 26 27 28 29
		 30 31 32 33 34 35 37 38 39 40 41 42 43 44 45 46 47
		44
		-0.099443098422977758 0.049217281738067047 9.8403619619843158e-18
		-0.092287945769431265 0.023419250570031001 9.8403619619843158e-18
		-0.082408809080342066 -0.0017435156386027032 6.8219605696323463e-19
		-0.071017872932817444 -0.025651518587196609 5.2612790094737782e-18
		-0.058813098207550783 -0.047715972112498825 3.73491802530359e-18
		-0.046489269782594551 -0.067393747667264886 1.2893083930324671e-17
		-0.033511008795473571 -0.08420002049447349 6.7876399936439502e-18
		-0.02245657412952675 -0.097721080037575056 6.7876399936439502e-18
		-0.010478509216864247 -0.10762418410221934 6.8219605696323463e-19
		0.002128200246792054 -0.11366511371357876 6.8219605696323463e-19
		0.015053181585218673 -0.11569556373764578 9.8403619619843158e-18
		0.015053181585218673 -0.11569556373764578 9.8403619619843158e-18
		0.027978162923645227 -0.11366511371357875 6.8219605696323463e-19
		0.040584872387301497 -0.1076241841022193 6.8219605696323463e-19
		0.052562937299963974 -0.097721080037575056 6.7876399936439502e-18
		0.063617371965910802 -0.084200020494473449 6.7876399936439502e-18
		0.076595632953031789 -0.067393747667264844 1.2893083930324671e-17
		0.088919461377988354 -0.047715972112498894 3.73491802530359e-18
		0.10112423610325491 -0.025651518587196623 5.2612790094737782e-18
		0.11251517225077952 -0.0017435156386026685 6.8219605696323463e-19
		0.12239430893986865 0.023419250570030997 9.8403619619843158e-18
		0.12954946159341499 0.049217281738067054 9.8403619619843158e-18
		0.12954946159341499 0.049217281738067054 9.8403619619843158e-18
		0.13172022838296776 0.066122742851700014 9.8403619619843158e-18
		0.1289393114043961 0.075059355992172491 1.8998527867005405e-17
		0.12030006650564966 0.082035537702758066 1.4419444914494843e-17
		0.1106153975020034 0.08691836851466761 1.5945805898665024e-17
		0.098577643434162476 0.091273031034565272 6.7876399936439502e-18
		0.084483130916970225 0.094992234263412537 1.2893083930324671e-17
		0.06867912321641173 0.097984424669171524 1.2893083930324671e-17
		0.05155464459421237 0.10017596689816607 1.8998527867005405e-17
		0.033531422289043225 0.10151281565379476 1.8998527867005405e-17
		0.015053181585218629 0.10196215123005004 9.8403619619843158e-18
		0.015053181585218629 0.10196215123005004 9.8403619619843158e-18
		-0.0034250591186059717 0.10151281565379475 1.8998527867005405e-17
		-0.021448281423775167 0.1001759668981661 1.8998527867005405e-17
		-0.038572760045974618 0.097984424669171469 1.2893083930324671e-17
		-0.05437676774653296 0.094992234263412495 1.2893083930324671e-17
		-0.068471280263725093 0.091273031034565272 6.7876399936439502e-18
		-0.080509034331566032 0.086918368514667624 1.5945805898665024e-17
		-0.090193703335212233 0.08203553770275808 1.4419444914494843e-17
		-0.098832948233958981 0.075059355992172505 1.8998527867005405e-17
		-0.10161386521253052 0.06612274285170007 9.8403619619843158e-18
		-0.099443098422977758 0.049217281738067047 9.8403619619843158e-18
		;
createNode transform -n "EyeLidsBshJntZro_C_Grp" -p "BshRigCtrl_C_Grp";
	rename -uid "6D1B6085-4385-841C-57A7-EDA56131BB5C";
	setAttr ".t" -type "double3" 0 1.7763568394002505e-15 0 ;
createNode transform -n "EyeLidsBshJntZro_C_Grp" -p "|BshRigCtrl_C_Grp|EyeLidsBshJntZro_C_Grp";
	rename -uid "CC08F2E3-4214-569E-9FB3-068031AF7453";
	setAttr ".t" -type "double3" 0 1.1197648206695874 2.6524512754545162 ;
createNode joint -n "EyeLidsBsh_C_Jnt" -p "|BshRigCtrl_C_Grp|EyeLidsBshJntZro_C_Grp|EyeLidsBshJntZro_C_Grp";
	rename -uid "52198082-40C2-C8BA-65DC-2FA3A36B8271";
	addAttr -ci true -sn "eyelidsFollow" -ln "eyelidsFollow" -min 0 -max 1 -at "double";
	setAttr -l on -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".radi" 0.5;
	setAttr -k on ".eyelidsFollow" 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "018B071E-4BAB-5E71-0514-AC8D711D5A8A";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "6CACC0DC-4D42-9C18-CDAB-389188660E34";
	setAttr ".bsdt[0].bscd" -type "Int32Array" 0 ;
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "8D96327F-4233-0EDC-F351-61895DB01B58";
createNode displayLayerManager -n "layerManager";
	rename -uid "9AD623DF-4805-7CB1-455C-BEA2374D5C4E";
createNode displayLayer -n "defaultLayer";
	rename -uid "175F62AD-4380-8798-1C93-86907A7C0E5F";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "E55F6ACE-4F9D-661F-89ED-27937D0D7150";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "A1FF5408-4BE6-47C6-98DD-E59D95F8DCE0";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "04941DE0-4FF4-6526-3F2A-0EBCE609A26B";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode RedshiftOptions -s -n "redshiftOptions";
	rename -uid "9B809307-4892-557A-5EB7-E39AD1FC35B8";
createNode RedshiftPostEffects -n "defaultRedshiftPostEffects";
	rename -uid "C1A957DF-4C55-0E6B-5E9E-2C98B1B253B0";
	setAttr ".clrMgmtDisplayMode" -type "string" "RS_COLORMANAGEMENTDISPLAYMODE_SRGB";
	setAttr -s 2 ".cr[1]" -type "float2" 1 1;
	setAttr -s 2 ".cg[1]" -type "float2" 1 1;
	setAttr -s 2 ".cb[1]" -type "float2" 1 1;
	setAttr -s 2 ".cl[1]" -type "float2" 1 1;
createNode blendColors -n "FacialCtrlMover_EyeLidsLwrAuto_R_Bcl";
	rename -uid "0633B1BA-4C33-58C8-946B-3D898DAE8A2D";
	setAttr ".b" 0;
createNode blendColors -n "FacialCtrlMover_EyeLidsLwrAuto_L_Bcl";
	rename -uid "4366DD94-4393-8DE1-EC24-99A87DB4B255";
	setAttr ".b" 0;
createNode blendColors -n "EyeLidsUprAuto_R_Bcl";
	rename -uid "9BE72795-428E-2A20-B2B1-129E782F438A";
	setAttr ".b" 0;
createNode blendColors -n "EyeLidsUprAuto_L_Bcl";
	rename -uid "F391E7D0-4F42-B176-9E80-FBBAFD636C6F";
	setAttr ".b" 0;
createNode remapValue -n "NoseTwist_R_Rem";
	rename -uid "754DBC2B-45CD-058A-BFE8-689DA739D654";
	setAttr ".imx" 10;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "NoseTwist_L_Rem";
	rename -uid "0E246120-4636-667D-DC56-EFA1BA089CAD";
	setAttr ".imx" 10;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "NoseSnarl_Rem";
	rename -uid "84F3A45A-4CE8-3526-BDF9-D99C33D9D40F";
	setAttr ".imx" 10;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowSquintOut_R_Rem";
	rename -uid "29E3CD1B-4D48-C6C5-DA86-FAAD877FB951";
	setAttr ".imx" 10;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowSquintOut_L_Rem";
	rename -uid "E516DB33-4E5F-BED3-CC15-FAA806E6AE0B";
	setAttr ".imx" 10;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowSquintIn_R_Rem";
	rename -uid "A420DE48-4D7C-0538-0FBE-CC868BAE82BF";
	setAttr ".imx" 10;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowSquintIn_L_Rem";
	rename -uid "585ED258-4BC8-B0C7-609B-79803ECC0165";
	setAttr ".imx" 10;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowPull_R_Rem";
	rename -uid "42D2A30B-4AA6-4989-0469-D38F7F26805E";
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowPull_L_Rem";
	rename -uid "E2BB32FA-42D1-5A0D-30DF-D0A91B03EB84";
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowOutTurnF_R_Rem";
	rename -uid "633D47E8-4376-6AEE-8859-E697FAEDBFCA";
	setAttr ".imx" -45;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowOutTurnF_L_Rem";
	rename -uid "0EB273D0-41F3-FCC6-CC32-53AD8A2DB521";
	setAttr ".imx" -45;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowOutTurnC_R_Rem";
	rename -uid "3039341D-4C02-10A7-DE99-B6B08DE15C89";
	setAttr ".imx" 45;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowOutTurnC_L_Rem";
	rename -uid "5AC807D7-4FDB-FCC8-2294-9BBB14437812";
	setAttr ".imx" 45;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowMidTurnF_R_Rem";
	rename -uid "76206A43-49AB-65CD-DE60-15A74252681C";
	setAttr ".imx" -45;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowMidTurnF_L_Rem";
	rename -uid "E292C034-4137-2483-33C3-23A8E7D37B41";
	setAttr ".imx" -45;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowMidTurnC_R_Rem";
	rename -uid "10496051-4693-592D-35EB-D3B5C2CEB8E1";
	setAttr ".imx" 45;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowMidTurnC_L_Rem";
	rename -uid "B23E5ACA-4E09-F4E1-3762-BD81CA7590B6";
	setAttr ".imx" 45;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowInTurnF_R_Rem";
	rename -uid "DB414FAB-4CF5-D3EC-0A67-67A49712C57C";
	setAttr ".imx" -45;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowInTurnF_L_Rem";
	rename -uid "E2975487-4597-763C-D9EA-3AA327536838";
	setAttr ".imx" -45;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowInTurnC_R_Rem";
	rename -uid "B5A1C4B5-4EBD-71C1-872F-13B3A616FDD3";
	setAttr ".imx" 45;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode remapValue -n "EyebrowInTurnC_L_Rem";
	rename -uid "D1A722EE-477E-923A-F936-F0B458845BB0";
	setAttr ".imx" 45;
	setAttr -s 2 ".vl[0:1]"  0 0 1 1 1 1;
	setAttr -s 2 ".cl";
	setAttr ".cl[0].cli" 1;
	setAttr ".cl[1].clp" 1;
	setAttr ".cl[1].clc" -type "float3" 1 1 1 ;
	setAttr ".cl[1].cli" 1;
createNode plusMinusAverage -n "PuffOut_R_Pma";
	rename -uid "AC0EA521-44B1-633B-D865-4B909E202579";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode multiplyDivide -n "Puff_R_Mdv";
	rename -uid "F4068976-42E2-0B27-41A1-F2B5F99169A7";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode clamp -n "Puff_R_Cmp";
	rename -uid "F048187C-481A-8684-B3E6-E19DAC7CFF36";
	setAttr ".mn" -type "float3" -0.5 0 0 ;
	setAttr ".mx" -type "float3" 0 0.5 0 ;
createNode blendColors -n "Puff_R_Bcl";
	rename -uid "EE037CE2-4A05-521B-CABC-A4A2F3484CC7";
createNode multiplyDivide -n "PuffAttr_R_Mdv";
	rename -uid "63DC8AC8-429E-7012-A38D-DB83F5A99930";
	setAttr ".i2" -type "float3" -0.1 0.1 1 ;
createNode clamp -n "PuffAttr_R_Cmp";
	rename -uid "1012A5F3-4F06-C278-A78A-118A76608E9B";
	setAttr ".mn" -type "float3" -10 0 0 ;
	setAttr ".mx" -type "float3" 0 10 0 ;
createNode plusMinusAverage -n "PuffOut_L_Pma";
	rename -uid "3C49A917-4088-513B-F566-238D19B7D002";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode multiplyDivide -n "PuffAttr_L_Mdv";
	rename -uid "DCF31CAC-47A8-1879-2B25-9DBB737D07F4";
	setAttr ".i2" -type "float3" -0.1 0.1 1 ;
createNode clamp -n "PuffAttr_L_Cmp";
	rename -uid "0F367ACA-44AF-A3CA-97DA-D9AFA9D005A9";
	setAttr ".mn" -type "float3" -10 0 0 ;
	setAttr ".mx" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "Puff_L_Mdv";
	rename -uid "405509A9-4E28-FCFF-C4AC-50ACCC377B3B";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode clamp -n "Puff_L_Cmp";
	rename -uid "00C51D9D-4D44-7830-E0C3-EC8A6F88C28D";
	setAttr ".mn" -type "float3" -0.5 0 0 ;
	setAttr ".mx" -type "float3" 0 0.5 0 ;
createNode blendColors -n "Puff_L_Bcl";
	rename -uid "E74DAB8E-4E58-8C30-9BB6-1FB7F06E216F";
	setAttr ".c2" -type "float3" 0 0 0 ;
createNode plusMinusAverage -n "PuffIn_R_Pma";
	rename -uid "360C4A66-42B3-AD67-BE9C-569EE8E1A949";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode plusMinusAverage -n "PuffIn_L_Pma";
	rename -uid "2A21DA78-48FA-90B6-551A-55A08AAFFB1C";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode plusMinusAverage -n "FacialCtrlMover_EyeLidsLwrUp_R_Pma";
	rename -uid "2901EA2D-41F9-12DD-8D64-7BAB1FD9AD52";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode multiplyDivide -n "FacialCtrlMover_EyeLidsLwrUD_R_Mdv";
	rename -uid "233C5D29-4F49-3A4E-7D81-04B91430D7FB";
	setAttr ".i2" -type "float3" 0.72000003 -2 1 ;
createNode clamp -n "FacialCtrlMover_EyeLidsLwrUD_R_Cmp";
	rename -uid "550014BF-4E8D-142E-18DD-4C9BB08CE842";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 1.4 0 0 ;
createNode multiplyDivide -n "FacialCtrlMover_EyeLidsLwrAuto_R_Mdv";
	rename -uid "2E24D397-47C5-C065-7F1E-FE85BBED8768";
	setAttr ".i2" -type "float3" 0.5 -0.1 1 ;
createNode clamp -n "FacialCtrlMover_EyeLidsLwrAuto_R_Cmp";
	rename -uid "02CEE32F-40D2-6FC2-4229-49A8D32C9EF1";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 1.4 0 0 ;
createNode plusMinusAverage -n "FacialCtrlMover_EyeLidsLwrUp_C_Pma";
	rename -uid "9BE52433-458F-F547-BD5C-6D869CC6DCB2";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode multiplyDivide -n "FacialCtrlMover_EyeLidsLwrUD_C_Mdv";
	rename -uid "91074102-4FA8-B1FD-AA4D-8B9FD97970D8";
	setAttr ".i2" -type "float3" 0.72000003 -2 1 ;
createNode clamp -n "FacialCtrlMover_EyeLidsLwrUD_C_Cmp";
	rename -uid "994551C2-46E2-CB49-30D7-3BB0F88B3DE4";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 1.4 0 0 ;
createNode multiplyDivide -n "FacialCtrlMover_EyeLidsLwrAuto_L_Mdv";
	rename -uid "555A6225-43A9-D231-399E-489654CE2E07";
	setAttr ".i2" -type "float3" 0.5 -0.1 1 ;
createNode clamp -n "FacialCtrlMover_EyeLidsLwrAuto_L_Cmp";
	rename -uid "C1B04F01-4F5F-4530-0C6A-E89BC23FC177";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 1.4 0 0 ;
createNode plusMinusAverage -n "FacialCtrlMover_EyeLidsLwrDn_R_Pma";
	rename -uid "45525B95-4702-F37F-91E7-E9868374DAD2";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode plusMinusAverage -n "FacialCtrlMover_EyeLidsLwrDn_C_Pma";
	rename -uid "86B5E39E-4555-D786-5963-2D9BB00EEACC";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode plusMinusAverage -n "CheekUp_R_Pma";
	rename -uid "CFBC017B-4C2A-BE9C-8D99-25BF50722BF1";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode multiplyDivide -n "CheekAttr_R_Mdv";
	rename -uid "899F1BF5-4C42-A88C-7AAF-5FB9330C0102";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "CheekAttr_R_Cmp";
	rename -uid "B0BA45DF-4877-A331-DE49-198385C07A70";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "Cheek_R_Mdv";
	rename -uid "5E8479FE-428A-D5BC-FA9F-FBB8B4130FA1";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode clamp -n "Cheek_R_Cmp";
	rename -uid "EE354DF8-468A-C78A-96AF-64B603BBA0CC";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 0.5 0 0 ;
createNode blendColors -n "Cheek_R_Bcl";
	rename -uid "50E53593-4C5B-69B6-F44D-72A07E5056B6";
createNode plusMinusAverage -n "CheekUp_L_Pma";
	rename -uid "CA63FED3-4A15-3733-7A04-D3A4066B48F5";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode multiplyDivide -n "Cheek_L_Mdv";
	rename -uid "D76AD4E1-46AE-1E4A-E25A-45B342970985";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode clamp -n "Cheek_L_Cmp";
	rename -uid "E4145AAF-46BC-3EEB-0A55-AEA3329DBDC6";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 0.5 0 0 ;
createNode blendColors -n "Cheek_L_Bcl";
	rename -uid "E9D22554-45F2-E35B-3052-4A88D085BEEB";
	setAttr ".c2" -type "float3" 0 0 0 ;
createNode multiplyDivide -n "CheekAttr_L_Mdv";
	rename -uid "F17DE512-494B-3EDD-B528-0D950C4FF6F0";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "CheekAttr_L_Cmp";
	rename -uid "2CF823A1-448E-2F8E-249B-498AE52F96CB";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode plusMinusAverage -n "CheekDn_R_Pma";
	rename -uid "BCEE8AD7-4B22-0E29-B42C-EFA497C84757";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode plusMinusAverage -n "CheekDn_L_Pma";
	rename -uid "F1994F80-411F-DDD0-E945-DDA1452A9BBD";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode multiplyDivide -n "PuffAttrIO_R_Mdv";
	rename -uid "6C7D4231-483D-611C-5052-4B8CA5F00CB4";
	setAttr ".i2" -type "float3" -0.1 0.1 1 ;
createNode clamp -n "PuffAttrIO_R_Cmp";
	rename -uid "A3028889-4AC3-6578-B601-ECAD81ECCE48";
	setAttr ".mn" -type "float3" -10 0 0 ;
	setAttr ".mx" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "PuffAttrIO_L_Mdv";
	rename -uid "19CD8BB6-4E39-75DA-CA18-3091EA6F2C7D";
	setAttr ".i2" -type "float3" -0.1 0.1 1 ;
createNode clamp -n "PuffAttrIO_L_Cmp";
	rename -uid "07A7C407-47E2-F060-FDDB-2DB2321B63EA";
	setAttr ".mn" -type "float3" -10 0 0 ;
	setAttr ".mx" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "NoseUD_R_Mdv";
	rename -uid "2B26FB96-4976-A223-1853-B2B15D3B57A1";
	setAttr ".i2" -type "float3" -0.1 0.1 1 ;
createNode clamp -n "NoseUD_R_Cmp";
	rename -uid "F355768F-4672-1FFE-B01D-CFBCA53B3501";
	setAttr ".mn" -type "float3" -10 0 0 ;
	setAttr ".mx" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "NoseUD_Mdv";
	rename -uid "EEC34064-43B2-B287-6465-6FAE6CD51FD3";
	setAttr ".i2" -type "float3" 2.8559999 -2.8559999 1 ;
createNode clamp -n "NoseUD_Cmp";
	rename -uid "B3507359-4817-E0FB-F2FC-6DB324600062";
	setAttr ".mn" -type "float3" 0 -0.34999999 0 ;
	setAttr ".mx" -type "float3" 0.34999999 0 0 ;
createNode multiplyDivide -n "NoseUD_L_Mdv";
	rename -uid "CDE6FBE4-464E-99B0-1DFB-B59BD6012FE0";
	setAttr ".i2" -type "float3" -0.1 0.1 1 ;
createNode clamp -n "NoseUD_L_Cmp";
	rename -uid "AFB900B9-40AA-73AD-1AD0-08A6EB5895A4";
	setAttr ".mn" -type "float3" -10 0 0 ;
	setAttr ".mx" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "NoseStSq_Mdv";
	rename -uid "E221126C-49AC-642E-B66F-BBA1EFBA04DC";
	setAttr ".i2" -type "float3" -0.1 0.1 1 ;
createNode clamp -n "NoseStSq_Cmp";
	rename -uid "E5AB4EF4-4797-9E2C-F8AB-0584A9ABF355";
	setAttr ".mn" -type "float3" -10 0 0 ;
	setAttr ".mx" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "NoseLR_Mdv";
	rename -uid "0723D259-4D11-F78A-4532-6F9768AF907F";
	setAttr ".i2" -type "float3" 2.8559999 -2.8559999 1 ;
createNode clamp -n "NoseLR_Cmp";
	rename -uid "F2ED9089-4B43-4314-9743-AD959496411C";
	setAttr ".mn" -type "float3" 0 -0.34999999 0 ;
	setAttr ".mx" -type "float3" 0.34999999 0 0 ;
createNode multiplyDivide -n "NoseFC_R_Mdv";
	rename -uid "C9E7BF6B-462A-3265-CF54-8280CFB0D353";
	setAttr ".i2" -type "float3" -0.1 0.1 1 ;
createNode clamp -n "NoseFC_R_Cmp";
	rename -uid "C1EA88EC-4132-AA1D-D70D-B8851899845E";
	setAttr ".mn" -type "float3" -10 0 0 ;
	setAttr ".mx" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "NoseFC_L_Mdv";
	rename -uid "C6014E2F-4801-A693-A086-09A972E4C641";
	setAttr ".i2" -type "float3" -0.1 0.1 1 ;
createNode clamp -n "NoseFC_L_Cmp";
	rename -uid "223A3433-48F0-C29F-4B63-D1874C2661A3";
	setAttr ".mn" -type "float3" -10 0 0 ;
	setAttr ".mx" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "MouthU_Mdv";
	rename -uid "57C82C98-4AFE-1124-F008-5CA4366B4ECC";
	setAttr ".i2" -type "float3" 0.1 0 1 ;
createNode clamp -n "MouthU_Cmp";
	rename -uid "E547D16D-49D9-9DB7-F54F-DE9C1DD75825";
	setAttr ".mn" -type "float3" -10 0 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "MouthUD_Mdv";
	rename -uid "C1A863A7-4101-3EBA-DA89-ACBC5640010F";
	setAttr ".i2" -type "float3" 4 -2 1 ;
createNode clamp -n "MouthUD_Cmp";
	rename -uid "777068F5-450A-4A60-CE27-54A3012139DE";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 0.25 0 0 ;
createNode multiplyDivide -n "MouthPull_Mdv";
	rename -uid "476B7F77-4E2C-8DFD-F412-E6B266B341C3";
	setAttr ".i2" -type "float3" 4 -4 1 ;
createNode clamp -n "MouthPull_Cmp";
	rename -uid "4413D98A-4C37-C787-4813-39A7CD69E292";
	setAttr ".mn" -type "float3" -0.25 0 0 ;
	setAttr ".mx" -type "float3" 0.25 0 0 ;
createNode multiplyDivide -n "MouthLR_Mdv";
	rename -uid "6B3EE694-47DA-793A-9B7F-B9897E457E44";
	setAttr ".i2" -type "float3" 2.5 -2.5 1 ;
createNode clamp -n "MouthLR_Cmp";
	rename -uid "97F65909-45A8-D19B-A25E-41A62504FB2E";
	setAttr ".mn" -type "float3" 0 -0.40000001 0 ;
	setAttr ".mx" -type "float3" 0.40000001 0 0 ;
createNode multiplyDivide -n "MouthFC_Mdv";
	rename -uid "21424C1E-4A65-0ABA-80AE-96BCB083CDC0";
	setAttr ".i2" -type "float3" -0.03334 0.03334 1 ;
createNode clamp -n "MouthFC_Cmp";
	rename -uid "8BE0ED28-41EC-6938-474A-05838F6FFA5B";
	setAttr ".mn" -type "float3" -30 0 0 ;
	setAttr ".mx" -type "float3" 0 30 0 ;
createNode multiplyDivide -n "MouthClench_Mdv";
	rename -uid "DEDAE1D9-4FC9-8FBC-F9EF-528E311AEBF6";
	setAttr ".i2" -type "float3" 0.1 0 1 ;
createNode clamp -n "MouthClench_Cmp";
	rename -uid "AA5904C0-42AC-7681-D3B5-17BB795EFA10";
	setAttr ".mn" -type "float3" -10 0 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "LipsUprCurlIO_Mdv";
	rename -uid "B30BCEFF-410C-CB64-93F5-9CA5FFD7D741";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "LipsUprCurlIO_Cmp";
	rename -uid "0789B01B-43A9-D5E7-6A49-B792C6BEDD54";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "LipsRgtUprAttrUD_Mdv";
	rename -uid "A94B946A-4A51-A44E-0003-2BB577847DB3";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "LipsRgtUprAttrUD_Cmp";
	rename -uid "3752028A-43C4-BE8C-4874-82AB1AA1793F";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "LipsRgtLwrAttrUD_Mdv";
	rename -uid "B7080B06-4087-7F73-81C3-DDB2A6DC9F20";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "LipsRgtLwrAttrUD_Cmp";
	rename -uid "74C2A8C5-4CBC-9441-1256-DC908DFAEEE0";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "LipsPartIO_R_Mdv";
	rename -uid "40CFF16D-4548-7D99-42B1-408C29297FC8";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "LipsPartIO_R_Cmp";
	rename -uid "686AE5EA-4583-7DEA-F27E-14B67C90066C";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "LipsPartIO_L_Mdv";
	rename -uid "40D9CFE2-4163-B4F9-EAEB-7FB15F855640";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "LipsPartIO_L_Cmp";
	rename -uid "2CBB3B4F-478E-F53C-9F75-EE80D8BA09E9";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "LipsMidUprAttrUD_Mdv";
	rename -uid "7A75C041-4E3F-F68E-6459-DDB511AABA94";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "LipsMidUprAttrUD_Cmp";
	rename -uid "D84837B5-4E46-E8F8-24AC-5E9A623512F9";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "LipsMidLwrAttrUD_Mdv";
	rename -uid "C6793B2F-459C-B1ED-30C2-62BEFFE50E74";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "LipsMidLwrAttrUD_Cmp";
	rename -uid "873451BB-4E9F-960E-9005-5D91E914AF2C";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "LipsLwrCurlIO_Mdv";
	rename -uid "5C6C0FC0-405A-26F7-219E-4EACEEC2810F";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "LipsLwrCurlIO_Cmp";
	rename -uid "83AC3D3E-46E1-D09F-D26E-DA90C38A41B3";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "LipsLftUprAttrUD_Mdv";
	rename -uid "5A6AFB2E-4AB5-7C9F-EA57-CDBA37351DA6";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "LipsLftUprAttrUD_Cmp";
	rename -uid "6DD793B9-4A18-1F02-A603-DC97043C15B4";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "LipsLftLwrAttrUD_Mdv";
	rename -uid "3D40583F-4F0D-5B3B-8C0C-E8A71B5692AA";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "LipsLftLwrAttrUD_Cmp";
	rename -uid "32D21C19-4C60-FB3F-3F20-4D822FA48496";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "LipsCnrUD_R_Mdv";
	rename -uid "48DCE22C-48DF-7A37-1624-CBB2B38BD2DE";
	setAttr ".i2" -type "float3" 2 -2 1 ;
createNode clamp -n "LipsCnrUD_R_Cmp";
	rename -uid "A5A4C4F1-461B-7298-01EA-758057B7BD9C";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 0.5 0 0 ;
createNode multiplyDivide -n "LipsCnrUD_L_Mdv";
	rename -uid "1A6A9E79-4DF8-2AC6-4337-B0AE4226306F";
	setAttr ".i2" -type "float3" 2 -2 1 ;
createNode clamp -n "LipsCnrUD_L_Cmp";
	rename -uid "B67C5400-44E0-2109-95E6-F4867A156CFD";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 0.5 0 0 ;
createNode multiplyDivide -n "LipsCnrIO_R_Mdv";
	rename -uid "E52DD9DB-40A2-AC84-6BAE-B89CD4146590";
	setAttr ".i2" -type "float3" -2 2 1 ;
createNode clamp -n "LipsCnrIO_R_Cmp";
	rename -uid "5B2D8A7D-4E3B-CD43-9E8F-688543DDEA01";
	setAttr ".mn" -type "float3" -0.5 0 0 ;
	setAttr ".mx" -type "float3" 0 0.5 0 ;
createNode multiplyDivide -n "LipsCnrIO_L_Mdv";
	rename -uid "2A3F30E3-442E-C22F-A884-A39847FEAF06";
	setAttr ".i2" -type "float3" -2 2 1 ;
createNode clamp -n "LipsCnrIO_L_Cmp";
	rename -uid "5A330264-4A1D-C3DA-C4C8-DCA074488F2D";
	setAttr ".mn" -type "float3" -0.5 0 0 ;
	setAttr ".mx" -type "float3" 0 0.5 0 ;
createNode multiplyDivide -n "LipsAllUprUD_Mdv";
	rename -uid "A9A28ECD-4EF5-53A8-85B7-93BD8D4DEE23";
	setAttr ".i2" -type "float3" 4 -4 1 ;
createNode clamp -n "LipsAllUprUD_Cmp";
	rename -uid "5CC59D3E-4B52-9D8C-1C80-B3B6E0E64F31";
	setAttr ".mn" -type "float3" 0 -0.25 0 ;
	setAttr ".mx" -type "float3" 0.25 0 0 ;
createNode multiplyDivide -n "LipsAllLwrUD_Mdv";
	rename -uid "A4EDD777-4F36-3BDE-F583-D08AE1A5ED0E";
	setAttr ".i2" -type "float3" 4 -4 1 ;
createNode clamp -n "LipsAllLwrUD_Cmp";
	rename -uid "0AAF7700-4F03-736A-5895-FA96B5BC0E88";
	setAttr ".mn" -type "float3" 0 -0.25 0 ;
	setAttr ".mx" -type "float3" 0.25 0 0 ;
createNode multiplyDivide -n "FaceUprTz_Mdv";
	rename -uid "8E840229-445B-0161-359B-1DBA04371AF3";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode clamp -n "FaceUprTz_Cmp";
	rename -uid "0011B382-4388-693F-916C-95921D1178A5";
	setAttr ".mn" -type "float3" 0 -100 0 ;
	setAttr ".mx" -type "float3" 100 0 0 ;
createNode multiplyDivide -n "FaceUprTy_Mdv";
	rename -uid "2AFFEF54-467F-E32C-4098-10AA82480DF2";
	setAttr ".i2" -type "float3" 3 -3 1 ;
createNode clamp -n "FaceUprTy_Cmp";
	rename -uid "38F45CFC-455B-8530-119E-D2949E8A045F";
	setAttr ".mn" -type "float3" 0 -100 0 ;
	setAttr ".mx" -type "float3" 100 0 0 ;
createNode multiplyDivide -n "FaceUprTx_Mdv";
	rename -uid "B0E9EA32-47EE-968C-7D19-11AA15DFFAE3";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode clamp -n "FaceUprTx_Cmp";
	rename -uid "942DEA80-407B-6B71-7214-E39EE0414E28";
	setAttr ".mn" -type "float3" 0 -100 0 ;
	setAttr ".mx" -type "float3" 100 0 0 ;
createNode multiplyDivide -n "FaceLwrTz_Mdv";
	rename -uid "8C51DAE7-4F03-70A4-11E0-64863DE31C93";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode clamp -n "FaceLwrTz_Cmp";
	rename -uid "315FFBB0-400C-6A0E-44C1-F5BA6945BFD5";
	setAttr ".mn" -type "float3" 0 -100 0 ;
	setAttr ".mx" -type "float3" 100 0 0 ;
createNode multiplyDivide -n "FaceLwrTy_Mdv";
	rename -uid "652F4A78-4302-F8E0-5CA1-ADB788E33038";
	setAttr ".i2" -type "float3" -1 3 1 ;
createNode clamp -n "FaceLwrTy_Cmp";
	rename -uid "7B017269-4BA9-B195-5B71-5FAADAAF0923";
	setAttr ".mn" -type "float3" -100 0 0 ;
	setAttr ".mx" -type "float3" 0 100 0 ;
createNode multiplyDivide -n "FaceLwrTx_Mdv";
	rename -uid "30A2D5A9-4C70-9E1F-3741-28A9E710A49B";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode clamp -n "FaceLwrTx_Cmp";
	rename -uid "638F6B02-401E-7290-9DB4-63A4059DD380";
	setAttr ".mn" -type "float3" 0 -100 0 ;
	setAttr ".mx" -type "float3" 100 0 0 ;
createNode multiplyDivide -n "EyebrowUD_R_Mdv";
	rename -uid "67DFE2EF-44B7-2F91-3207-CDB718704A2B";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode clamp -n "EyebrowUD_R_Cmp";
	rename -uid "8AABEB5F-4A48-4FDC-AE62-D2BB5A38DD7F";
	setAttr ".mn" -type "float3" 0 -1 0 ;
	setAttr ".mx" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "EyebrowUD_L_Mdv";
	rename -uid "2FB61C3C-4C45-05C2-26F2-9B81CD64F3ED";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode clamp -n "EyebrowUD_L_Cmp";
	rename -uid "A2999673-43E7-24A8-5921-1880DB299A46";
	setAttr ".mn" -type "float3" 0 -1 0 ;
	setAttr ".mx" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "EyebrowPullIO_Mdv";
	rename -uid "4865B8A6-469F-3E0E-6B11-F7A566090014";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode multiplyDivide -n "EyebrowOutUD_R_Mdv";
	rename -uid "AEE29453-4A80-65D3-8512-C3AF1CE0BAC1";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode clamp -n "EyebrowOutUD_R_Cmp";
	rename -uid "A04BFEA0-4631-B184-5F71-F7A01AF36A80";
	setAttr ".mn" -type "float3" 0 -1 0 ;
	setAttr ".mx" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "EyebrowOutUD_L_Mdv";
	rename -uid "A2E6462F-465D-19CA-D4AD-07A721FC386B";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode clamp -n "EyebrowOutUD_L_Cmp";
	rename -uid "442AAE5A-4707-1E70-88A1-F5904DA701F8";
	setAttr ".mn" -type "float3" 0 -1 0 ;
	setAttr ".mx" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "EyebrowOutIO_R_Mdv";
	rename -uid "1CF38906-4F71-919C-0F13-5888E3381A83";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode clamp -n "EyebrowOutIO_R_Cmp";
	rename -uid "AE76FEDA-4815-F667-7FCD-A7A73176C777";
	setAttr ".mn" -type "float3" -1 0 0 ;
	setAttr ".mx" -type "float3" 0 1 0 ;
createNode multiplyDivide -n "EyebrowOutIO_L_Mdv";
	rename -uid "08A48143-4512-A968-AAA7-22B64FC39A6D";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode clamp -n "EyebrowOutIO_L_Cmp";
	rename -uid "2A583011-4738-7233-5454-0E85F5A3A607";
	setAttr ".mn" -type "float3" -1 0 0 ;
	setAttr ".mx" -type "float3" 0 1 0 ;
createNode multiplyDivide -n "EyebrowMidUD_R_Mdv";
	rename -uid "D8900DFC-4705-329C-8FDA-2ABB14EE8B32";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode clamp -n "EyebrowMidUD_R_Cmp";
	rename -uid "7592ECA3-415B-D574-A33C-00BC3FAED6E5";
	setAttr ".mn" -type "float3" 0 -1 0 ;
	setAttr ".mx" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "EyebrowMidUD_L_Mdv";
	rename -uid "680CF9E8-4D60-640B-2084-F4A0ADC75B1F";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode clamp -n "EyebrowMidUD_L_Cmp";
	rename -uid "8B11303F-42D6-371A-3863-35A10459B19F";
	setAttr ".mn" -type "float3" 0 -1 0 ;
	setAttr ".mx" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "EyebrowMidIO_R_Mdv";
	rename -uid "A8F6A50B-47B0-BD16-F54D-E388505B66BC";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode clamp -n "EyebrowMidIO_R_Cmp";
	rename -uid "31DD824F-403C-F560-41E4-4284FA44F418";
	setAttr ".mn" -type "float3" -1 0 0 ;
	setAttr ".mx" -type "float3" 0 1 0 ;
createNode multiplyDivide -n "EyebrowMidIO_L_Mdv";
	rename -uid "8A149B46-49F5-FFE1-23BE-53B889B49D0F";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode clamp -n "EyebrowMidIO_L_Cmp";
	rename -uid "C6C2FE09-4FAE-25D4-B37A-B3A3B2BC2977";
	setAttr ".mn" -type "float3" -1 0 0 ;
	setAttr ".mx" -type "float3" 0 1 0 ;
createNode multiplyDivide -n "EyebrowInUD_R_Mdv";
	rename -uid "2FA232E6-4D0D-2A29-F6A2-27949EC33855";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode clamp -n "EyebrowInUD_R_Cmp";
	rename -uid "52141200-45E3-4D5C-F796-27AA87F2322F";
	setAttr ".mn" -type "float3" 0 -1 0 ;
	setAttr ".mx" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "EyebrowInUD_L_Mdv";
	rename -uid "E38BF0F2-44B6-A533-D8CB-F8B35C955667";
	setAttr ".i2" -type "float3" 1 -1 1 ;
createNode clamp -n "EyebrowInUD_L_Cmp";
	rename -uid "4546409F-4176-9D0C-2DE2-829F68521931";
	setAttr ".mn" -type "float3" 0 -1 0 ;
	setAttr ".mx" -type "float3" 1 0 0 ;
createNode multiplyDivide -n "EyebrowInIO_R_Mdv";
	rename -uid "C0013D2B-4A4F-41FE-3577-E49CD674A00C";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode clamp -n "EyebrowInIO_R_Cmp";
	rename -uid "F2BED860-4C60-D90D-A850-DFA88D215087";
	setAttr ".mn" -type "float3" -1 0 0 ;
	setAttr ".mx" -type "float3" 0 1 0 ;
createNode multiplyDivide -n "EyebrowInIO_L_Mdv";
	rename -uid "DC11E324-48C2-A42B-AF24-02BA10827987";
	setAttr ".i2" -type "float3" -1 1 1 ;
createNode clamp -n "EyebrowInIO_L_Cmp";
	rename -uid "769DFE57-45C4-B201-4A0F-7DADF7DF57B6";
	setAttr ".mn" -type "float3" -1 0 0 ;
	setAttr ".mx" -type "float3" 0 1 0 ;
createNode multiplyDivide -n "EyebrowIO_R_Mdv";
	rename -uid "DF38D2AA-4D82-6982-5A78-BCBAC0A0BF49";
	setAttr ".i2" -type "float3" 2 -2 1 ;
createNode clamp -n "EyebrowIO_R_Cmp";
	rename -uid "33E19932-4B59-F057-8592-64B3F6A64F2C";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 0.5 0 0 ;
createNode multiplyDivide -n "EyebrowIO_L_Mdv";
	rename -uid "A078DD66-4172-2D69-FF96-1AB024E0349B";
	setAttr ".i2" -type "float3" 2 -2 1 ;
createNode clamp -n "EyebrowIO_L_Cmp";
	rename -uid "49DC8058-4EE8-C5BB-EAE8-B3AE6E57596D";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 0.5 0 0 ;
createNode multiplyDivide -n "EyebrowFC_R_Mdv";
	rename -uid "7B76930D-4591-051B-9A1A-7B9890F0D5F9";
	setAttr ".i2" -type "float3" -0.033300001 0.033300001 1 ;
createNode clamp -n "EyebrowFC_R_Cmp";
	rename -uid "3FA84790-4713-BFCB-9C26-9E935FBDB11F";
	setAttr ".mn" -type "float3" 0 -30 0 ;
	setAttr ".mx" -type "float3" 30 0 0 ;
createNode multiplyDivide -n "EyebrowFC_L_Mdv";
	rename -uid "47FD5A61-429A-655C-B194-92A682D9DE99";
	setAttr ".i2" -type "float3" -0.033300001 0.033300001 1 ;
createNode clamp -n "EyebrowFC_L_Cmp";
	rename -uid "EA3F7E38-4F16-66DE-7F30-82B2EDB11D9E";
	setAttr ".mn" -type "float3" 0 -30 0 ;
	setAttr ".mx" -type "float3" 30 0 0 ;
createNode multiplyDivide -n "EyeLidsUprTurnFC_R_Mdv";
	rename -uid "D2ADF7C9-4632-3019-8D29-71AD40D2E666";
	setAttr ".i2" -type "float3" -0.1 0.1 1 ;
createNode clamp -n "EyeLidsUprTurnFC_R_Cmp";
	rename -uid "1CD6FF11-469E-4F80-53B6-83892381705D";
	setAttr ".mn" -type "float3" -10 0 0 ;
	setAttr ".mx" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "EyeLidsUprTurnFC_C_Mdv";
	rename -uid "0C22B5B4-434C-FF38-B99B-99A146BBAC3E";
	setAttr ".i2" -type "float3" -0.1 0.1 1 ;
createNode clamp -n "EyeLidsUprTurnFC_C_Cmp";
	rename -uid "8AAAD717-469A-1DAC-F64A-D08B3BF2D947";
	setAttr ".mn" -type "float3" -10 0 0 ;
	setAttr ".mx" -type "float3" 0 10 0 ;
createNode multiplyDivide -n "EyeLidsUprOutUD_R_Mdv";
	rename -uid "3E69F676-4B0D-5FD6-241C-A1A3BDF54551";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsUprOutUD_R_Cmp";
	rename -uid "C8EEA02D-44D5-D1B0-72F2-2E965CC852B8";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsUprOutUD_C_Mdv";
	rename -uid "07A3D4B5-430A-D50F-3822-85A42B09F746";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsUprOutUD_C_Cmp";
	rename -uid "F0F10210-48F1-6114-5B6D-23B12CAEE30A";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsUprMidUD_R_Mdv";
	rename -uid "E2EA22F7-44D5-DAF5-2D4C-5C938213A063";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsUprMidUD_R_Cmp";
	rename -uid "2D2A362A-402E-0F5F-F983-81A92045C0C6";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsUprMidUD_C_Mdv";
	rename -uid "518533E9-4750-E4E0-40BA-B5B0A51F1F9C";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsUprMidUD_C_Cmp";
	rename -uid "23DDC1DE-4612-582C-0C48-83A708D77B84";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsUprInUD_R_Mdv";
	rename -uid "38F5A678-45E8-82AA-EC1B-BB90EF8DB344";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsUprInUD_R_Cmp";
	rename -uid "13190F59-487B-9236-BECE-E1B9BBD277D2";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsUprInUD_C_Mdv";
	rename -uid "E1482ECF-49F8-F6F5-9D1F-D29E474504C2";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsUprInUD_C_Cmp";
	rename -uid "63B479E7-465E-42E7-1A59-539DF42E3D01";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsLwrTurnFC_R_Mdv";
	rename -uid "10A754CC-414A-2D26-19B7-7289731BD905";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsLwrTurnFC_R_Cmp";
	rename -uid "DBC5B4C9-4633-6CF1-C5A7-A6940EE39C51";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsLwrTurnFC_C_Mdv";
	rename -uid "78B7A310-488D-11B6-3FED-20964D4F0990";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsLwrTurnFC_C_Cmp";
	rename -uid "BBEABF94-474E-8B58-0DBF-9E8FBEBFB20D";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsLwrOutUD_R_Mdv";
	rename -uid "C3760CD2-4E84-C72F-EC3E-699ACB0DF9DF";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsLwrOutUD_R_Cmp";
	rename -uid "12969A5F-42C7-0596-8623-CABE168AEAD2";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsLwrOutUD_C_Mdv";
	rename -uid "34062926-414E-BDF4-F47C-E8B9FEEF5284";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsLwrOutUD_C_Cmp";
	rename -uid "0CE4D591-49A3-1FED-18DB-2BADE488467C";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsLwrMidUD_R_Mdv";
	rename -uid "033FC339-4AA6-9788-A3ED-1CB374FCCD67";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsLwrMidUD_R_Cmp";
	rename -uid "2CCB4ED9-4C7A-6D49-A7B6-82B796CA7FB4";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsLwrMidUD_C_Mdv";
	rename -uid "9A92EBC6-4E55-76B3-7FA1-12B0952F63AE";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsLwrMidUD_C_Cmp";
	rename -uid "DE29BFB5-40C8-4CAE-BAB2-F89D43452F62";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsLwrInUD_R_Mdv";
	rename -uid "BC74D9F6-48D9-4BE5-0792-C38DF1E009FA";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsLwrInUD_R_Cmp";
	rename -uid "CED8FA5E-4BDF-ED37-E255-5F82E290DBD9";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeLidsLwrInUD_C_Mdv";
	rename -uid "8457D718-4E34-5890-2A1C-AEAA93DB96B0";
	setAttr ".i2" -type "float3" 0.1 -0.1 1 ;
createNode clamp -n "EyeLidsLwrInUD_C_Cmp";
	rename -uid "DF6AE65B-439C-7060-B43B-18B6C74C9DB6";
	setAttr ".mn" -type "float3" 0 -10 0 ;
	setAttr ".mx" -type "float3" 10 0 0 ;
createNode multiplyDivide -n "EyeBallUD_R_Mdv";
	rename -uid "B9D6C5B4-4DB9-C27E-09D9-758DDBDA15F8";
	setAttr ".i2" -type "float3" 2 -2 1 ;
createNode clamp -n "EyeBallUD_R_Cmp";
	rename -uid "57D5D65F-4703-9962-1023-93B5C389E925";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 0.5 0 0 ;
createNode multiplyDivide -n "EyeBallUD_C_Mdv";
	rename -uid "1FD2BF6A-43C6-C024-AE1D-C9A1300B4DA9";
	setAttr ".i2" -type "float3" 2 -2 1 ;
createNode clamp -n "EyeBallUD_C_Cmp";
	rename -uid "47A6ED39-4546-074E-6591-1993CE634391";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 0.5 0 0 ;
createNode multiplyDivide -n "EyeBallFC_R_Mdv";
	rename -uid "4EE14D97-4FC1-ACC0-1159-B4906E176874";
	setAttr ".i2" -type "float3" -0.03334 0.03334 1 ;
createNode clamp -n "EyeBallFC_R_Cmp";
	rename -uid "09F34EBC-4C00-1E6C-6FA4-7E8908FA7F9A";
	setAttr ".mn" -type "float3" -30 0 0 ;
	setAttr ".mx" -type "float3" 0 30 0 ;
createNode multiplyDivide -n "EyeBallFC_C_Mdv";
	rename -uid "91DBC03C-49D7-CC9A-B7BB-A6B4D58D85D6";
	setAttr ".i2" -type "float3" -0.03334 0.03334 1 ;
createNode clamp -n "EyeBallFC_C_Cmp";
	rename -uid "C3B9E30F-4891-48D4-C52B-13A79B92A681";
	setAttr ".mn" -type "float3" -30 0 0 ;
	setAttr ".mx" -type "float3" 0 30 0 ;
createNode unitConversion -n "unitConversion200";
	rename -uid "1E617043-43C3-C687-A650-13B8358831B1";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion199";
	rename -uid "3DFD0264-4B1A-5D32-F526-469C78E8E516";
	setAttr ".cf" 57.295779513082323;
createNode clamp -n "EyeLidsUprAmpUD_R_Cmp";
	rename -uid "AF39669D-4C3D-0C45-4D6D-C28F90644CEA";
	setAttr ".mn" -type "float3" -1 -1 -1 ;
	setAttr ".mx" -type "float3" 1 1 1 ;
createNode plusMinusAverage -n "EyeLidsUprUp_R_Pma";
	rename -uid "A25E1B16-4BEB-7B34-E3B0-899DCD16B02F";
	setAttr -s 3 ".i1";
	setAttr -s 3 ".i1";
createNode multiplyDivide -n "EyeLidsFollowUprUD_R_Mdv";
	rename -uid "18D00C87-43B9-A29F-7AE5-7B9F7316AEDA";
	setAttr ".i2" -type "float3" -0.029999999 0.02 1 ;
createNode clamp -n "EyeLidsFollowUprUD_R_Cmp";
	rename -uid "185DC2DD-49A2-2BFC-5376-76872561D35B";
	setAttr ".mn" -type "float3" -100 0 0 ;
	setAttr ".mx" -type "float3" 0 100 0 ;
createNode blendColors -n "EyeLidsFollowUprUD_R_Bcl";
	rename -uid "A0FB56ED-42EA-EBEA-422C-0C8357F7B31B";
createNode multiplyDivide -n "EyeLidsUprAuto_R_Mdv";
	rename -uid "12F7FF0E-4312-1B34-A402-709472990C44";
	setAttr ".i2" -type "float3" 0.5 -0.2 1 ;
createNode clamp -n "EyeLidsUprAuto_R_Cmp";
	rename -uid "E5F18B54-41E7-0B71-8D1C-6AA9515FE589";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 1.4 0 0 ;
createNode multiplyDivide -n "EyeLidsUprUD_R_Mdv";
	rename -uid "06B5C717-48F6-3742-0AA1-30A9511E33EF";
	setAttr ".i2" -type "float3" -2 0.72000003 1 ;
createNode clamp -n "EyeLidsUprUD_R_Cmp";
	rename -uid "B088110C-48E7-F1CE-3AE9-68A0036B7AC4";
	setAttr ".mn" -type "float3" -0.5 0 0 ;
	setAttr ".mx" -type "float3" 0 1.4 0 ;
createNode plusMinusAverage -n "EyeLidsUprDn_R_Pma";
	rename -uid "920F80B9-441D-A656-B0B8-408AA88F236D";
	setAttr -s 3 ".i1";
	setAttr -s 3 ".i1";
createNode clamp -n "EyeLidsUprAmpUD_C_Cmp";
	rename -uid "ACF0E3CD-4C45-3BF3-68A2-519595C318D7";
	setAttr ".mn" -type "float3" -1 -1 -1 ;
	setAttr ".mx" -type "float3" 1 1 1 ;
createNode plusMinusAverage -n "EyeLidsUprDn_C_Pma";
	rename -uid "E71F5648-475B-6261-34FE-309F38F90BB5";
	setAttr -s 3 ".i1";
	setAttr -s 3 ".i1";
createNode multiplyDivide -n "EyeLidsFollowUprUD_C_Mdv";
	rename -uid "2DC7B507-44A5-65BD-5F72-56B56BEA8DF7";
	setAttr ".i2" -type "float3" -0.029999999 0.02 1 ;
createNode clamp -n "EyeLidsFollowUprUD_C_Cmp";
	rename -uid "30EF3B07-4E40-D1B0-BAD9-09B46125500A";
	setAttr ".mn" -type "float3" -100 0 0 ;
	setAttr ".mx" -type "float3" 0 100 0 ;
createNode blendColors -n "EyeLidsFollowUprUD_C_Bcl";
	rename -uid "5CC7B144-4658-2DA9-DEB2-9E8137DA7C81";
createNode unitConversion -n "unitConversion211";
	rename -uid "C0494199-4CB7-1612-AEC9-09B62BF55C1D";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion212";
	rename -uid "62329E4E-400C-D90F-8794-3C94F0F46FBF";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "EyeLidsUprUD_C_Mdv";
	rename -uid "64EDEBA4-4FC9-0175-BC25-FC891643F1D3";
	setAttr ".i2" -type "float3" -2 0.72000003 1 ;
createNode clamp -n "EyeLidsUprUD_C_Cmp";
	rename -uid "8ED3DB0D-48CC-819E-6C02-3C8A0CEE8694";
	setAttr ".mn" -type "float3" -0.5 0 0 ;
	setAttr ".mx" -type "float3" 0 1.4 0 ;
createNode multiplyDivide -n "EyeLidsUprAuto_L_Mdv";
	rename -uid "8C035603-4659-3F7C-1468-BCBDD495204B";
	setAttr ".i2" -type "float3" 0.5 -0.2 1 ;
createNode clamp -n "EyeLidsUprAuto_L_Cmp";
	rename -uid "5C97E6A3-4BAB-F98E-808A-44A3609BF662";
	setAttr ".mn" -type "float3" 0 -0.5 0 ;
	setAttr ".mx" -type "float3" 1.4 0 0 ;
createNode plusMinusAverage -n "EyeLidsUprUp_C_Pma";
	rename -uid "AF2CA0BC-4C2C-AEF4-4B6B-BDBC629A23EB";
	setAttr -s 3 ".i1";
	setAttr -s 3 ".i1";
createNode clamp -n "EyeLidsLwrAllUD_R_Cmp";
	rename -uid "718A9864-4B0B-FF92-19D6-D69008E781DF";
	setAttr ".mx" -type "float3" 1 1 0 ;
createNode clamp -n "EyeLidsLwrAllUD_C_Cmp";
	rename -uid "4E1E6D17-4B03-8F81-E399-109BA8B617AE";
	setAttr ".mx" -type "float3" 1 1 0 ;
createNode clamp -n "EyeBallAmpIO_R_Cmp";
	rename -uid "4357F9B7-4146-38A0-8B1D-CE98E43BE64A";
	setAttr ".mn" -type "float3" -1 -1 -1 ;
	setAttr ".mx" -type "float3" 1 1 1 ;
createNode plusMinusAverage -n "EyeBallIn_R_Pma";
	rename -uid "77259115-47A3-B574-E277-3EA21A04AD3C";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode multiplyDivide -n "EyeBallIO_R_Mdv";
	rename -uid "A9B3C43B-4847-C9B6-11D6-9EB42646F141";
	setAttr ".i2" -type "float3" -2 2 1 ;
createNode clamp -n "EyeBallIO_R_Cmp";
	rename -uid "4A1D8B4F-4CBA-6BC7-A7AD-9C9DA4F9E685";
	setAttr ".mn" -type "float3" -0.5 0 0 ;
	setAttr ".mx" -type "float3" 0 0.5 0 ;
createNode multiplyDivide -n "EyeBallFollowIO_R_Mdv";
	rename -uid "DF704E1F-48EB-21A2-2C71-5FB796AD4814";
	setAttr ".i2" -type "float3" 0.0049999999 -0.0049999999 1 ;
createNode clamp -n "EyeBallFollowIO_R_Cmp";
	rename -uid "3E3081EE-43EB-3F20-FD85-288D8290A05E";
	setAttr ".mn" -type "float3" 0 -300 0 ;
	setAttr ".mx" -type "float3" 300 0 0 ;
createNode blendColors -n "EyeBallFollowIO_R_Bcl";
	rename -uid "18F1A714-4B17-1BFF-E836-56B147902663";
createNode plusMinusAverage -n "EyeBallOut_R_Pma";
	rename -uid "AD8144FE-44F2-1189-C79E-8FA8B34A04FE";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode clamp -n "EyeBallAmpIO_C_Cmp";
	rename -uid "8097D19D-43E4-20D8-79D7-A0ACE40F3717";
	setAttr ".mn" -type "float3" -1 -1 -1 ;
	setAttr ".mx" -type "float3" 1 1 1 ;
createNode plusMinusAverage -n "EyeBallIn_C_Pma";
	rename -uid "6A994DE6-4E2C-184D-89B2-75BE58339DE4";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode multiplyDivide -n "EyeBallFollowIO_C_Mdv";
	rename -uid "9B17D234-4627-A4FD-7B7C-909A6F3C862E";
	setAttr ".i2" -type "float3" -0.0049999999 0.0049999999 1 ;
createNode clamp -n "EyeBallFollowIO_C_Cmp";
	rename -uid "7A8A2969-4ED8-3D16-1643-97B66ACF30BA";
	setAttr ".mn" -type "float3" -300 0 0 ;
	setAttr ".mx" -type "float3" 0 300 0 ;
createNode blendColors -n "EyeBallFollowIO_C_Bcl";
	rename -uid "173F5E11-44F1-7EA0-1347-339336270B80";
createNode unitConversion -n "unitConversion215";
	rename -uid "6A677F33-4603-4EB6-2767-D1BB400D65BE";
	setAttr ".cf" 57.295779513082323;
createNode unitConversion -n "unitConversion216";
	rename -uid "50BB1B1F-4D04-F816-8862-D3A2275B5225";
	setAttr ".cf" 57.295779513082323;
createNode multiplyDivide -n "EyeBallIO_C_Mdv";
	rename -uid "9730BAE9-499E-A311-5B8F-FD9224E8009F";
	setAttr ".i2" -type "float3" -2 2 1 ;
createNode clamp -n "EyeBallIO_C_Cmp";
	rename -uid "46B497A3-452A-28E9-7E0C-16BFB928BE48";
	setAttr ".mn" -type "float3" -0.5 0 0 ;
	setAttr ".mx" -type "float3" 0 0.5 0 ;
createNode plusMinusAverage -n "EyeBallOut_C_Pma";
	rename -uid "9DD7556B-4218-C2EF-F334-25919A900EAE";
	setAttr -s 2 ".i1";
	setAttr -s 2 ".i1";
createNode nodeGraphEditorInfo -n "MayaNodeEditorSavedTabsInfo";
	rename -uid "47EDD746-4FEA-4D05-6D23-AD9898541DAA";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" 224.6612197617716 8570.237754687445 ;
	setAttr ".tgi[0].vh" -type "double2" 1884.8625059370963 9757.1424694288489 ;
	setAttr -s 260 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 1427.142822265625;
	setAttr ".tgi[0].ni[0].y" 12907.142578125;
	setAttr ".tgi[0].ni[0].nvs" 18304;
	setAttr ".tgi[0].ni[1].x" 505.71429443359375;
	setAttr ".tgi[0].ni[1].y" 16362.857421875;
	setAttr ".tgi[0].ni[1].nvs" 18304;
	setAttr ".tgi[0].ni[2].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[2].y" 8487.142578125;
	setAttr ".tgi[0].ni[2].nvs" 18304;
	setAttr ".tgi[0].ni[3].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[3].y" 11477.142578125;
	setAttr ".tgi[0].ni[3].nvs" 18304;
	setAttr ".tgi[0].ni[4].x" 1427.142822265625;
	setAttr ".tgi[0].ni[4].y" 11867.142578125;
	setAttr ".tgi[0].ni[4].nvs" 18304;
	setAttr ".tgi[0].ni[5].x" 1358.5714111328125;
	setAttr ".tgi[0].ni[5].y" 15900;
	setAttr ".tgi[0].ni[5].nvs" 18304;
	setAttr ".tgi[0].ni[6].x" 1427.142822265625;
	setAttr ".tgi[0].ni[6].y" 5887.14306640625;
	setAttr ".tgi[0].ni[6].nvs" 18304;
	setAttr ".tgi[0].ni[7].x" 812.85711669921875;
	setAttr ".tgi[0].ni[7].y" 15234.2861328125;
	setAttr ".tgi[0].ni[7].nvs" 18304;
	setAttr ".tgi[0].ni[8].x" 1120;
	setAttr ".tgi[0].ni[8].y" 14077.142578125;
	setAttr ".tgi[0].ni[8].nvs" 18304;
	setAttr ".tgi[0].ni[9].x" 1120;
	setAttr ".tgi[0].ni[9].y" 16362.857421875;
	setAttr ".tgi[0].ni[9].nvs" 18304;
	setAttr ".tgi[0].ni[10].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[10].y" 2377.142822265625;
	setAttr ".tgi[0].ni[10].nvs" 18304;
	setAttr ".tgi[0].ni[11].x" 802.85711669921875;
	setAttr ".tgi[0].ni[11].y" 16745.71484375;
	setAttr ".tgi[0].ni[11].nvs" 18304;
	setAttr ".tgi[0].ni[12].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[12].y" 3937.142822265625;
	setAttr ".tgi[0].ni[12].nvs" 18304;
	setAttr ".tgi[0].ni[13].x" 1427.142822265625;
	setAttr ".tgi[0].ni[13].y" 10177.142578125;
	setAttr ".tgi[0].ni[13].nvs" 18304;
	setAttr ".tgi[0].ni[14].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[14].y" 2507.142822265625;
	setAttr ".tgi[0].ni[14].nvs" 18304;
	setAttr ".tgi[0].ni[15].x" 802.85711669921875;
	setAttr ".tgi[0].ni[15].y" 16542.857421875;
	setAttr ".tgi[0].ni[15].nvs" 18304;
	setAttr ".tgi[0].ni[16].x" 1115.7142333984375;
	setAttr ".tgi[0].ni[16].y" 16644.28515625;
	setAttr ".tgi[0].ni[16].nvs" 18304;
	setAttr ".tgi[0].ni[17].x" 1427.142822265625;
	setAttr ".tgi[0].ni[17].y" 14410;
	setAttr ".tgi[0].ni[17].nvs" 18304;
	setAttr ".tgi[0].ni[18].x" 812.85711669921875;
	setAttr ".tgi[0].ni[18].y" 15335.7138671875;
	setAttr ".tgi[0].ni[18].nvs" 18304;
	setAttr ".tgi[0].ni[19].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[19].y" 7967.14306640625;
	setAttr ".tgi[0].ni[19].nvs" 18304;
	setAttr ".tgi[0].ni[20].x" 1427.142822265625;
	setAttr ".tgi[0].ni[20].y" 15618.5712890625;
	setAttr ".tgi[0].ni[20].nvs" 18304;
	setAttr ".tgi[0].ni[21].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[21].y" 13947.142578125;
	setAttr ".tgi[0].ni[21].nvs" 18304;
	setAttr ".tgi[0].ni[22].x" 1427.142822265625;
	setAttr ".tgi[0].ni[22].y" 10047.142578125;
	setAttr ".tgi[0].ni[22].nvs" 18304;
	setAttr ".tgi[0].ni[23].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[23].y" 13297.142578125;
	setAttr ".tgi[0].ni[23].nvs" 18304;
	setAttr ".tgi[0].ni[24].x" 1427.142822265625;
	setAttr ".tgi[0].ni[24].y" 4457.14306640625;
	setAttr ".tgi[0].ni[24].nvs" 18304;
	setAttr ".tgi[0].ni[25].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[25].y" 6927.14306640625;
	setAttr ".tgi[0].ni[25].nvs" 18304;
	setAttr ".tgi[0].ni[26].x" 497.14285278320313;
	setAttr ".tgi[0].ni[26].y" 15567.142578125;
	setAttr ".tgi[0].ni[26].nvs" 18304;
	setAttr ".tgi[0].ni[27].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[27].y" 6277.14306640625;
	setAttr ".tgi[0].ni[27].nvs" 18304;
	setAttr ".tgi[0].ni[28].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[28].y" 13037.142578125;
	setAttr ".tgi[0].ni[28].nvs" 18304;
	setAttr ".tgi[0].ni[29].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[29].y" 9787.142578125;
	setAttr ".tgi[0].ni[29].nvs" 18304;
	setAttr ".tgi[0].ni[30].x" 1427.142822265625;
	setAttr ".tgi[0].ni[30].y" 5367.14306640625;
	setAttr ".tgi[0].ni[30].nvs" 18304;
	setAttr ".tgi[0].ni[31].x" 1427.142822265625;
	setAttr ".tgi[0].ni[31].y" 9527.142578125;
	setAttr ".tgi[0].ni[31].nvs" 18304;
	setAttr ".tgi[0].ni[32].x" 1117.142822265625;
	setAttr ".tgi[0].ni[32].y" 15668.5712890625;
	setAttr ".tgi[0].ni[32].nvs" 18304;
	setAttr ".tgi[0].ni[33].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[33].y" 10047.142578125;
	setAttr ".tgi[0].ni[33].nvs" 18304;
	setAttr ".tgi[0].ni[34].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[34].y" 12647.142578125;
	setAttr ".tgi[0].ni[34].nvs" 18304;
	setAttr ".tgi[0].ni[35].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[35].y" 6537.14306640625;
	setAttr ".tgi[0].ni[35].nvs" 18304;
	setAttr ".tgi[0].ni[36].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[36].y" 14308.5712890625;
	setAttr ".tgi[0].ni[36].nvs" 18304;
	setAttr ".tgi[0].ni[37].x" 497.14285278320313;
	setAttr ".tgi[0].ni[37].y" 15668.5712890625;
	setAttr ".tgi[0].ni[37].nvs" 18304;
	setAttr ".tgi[0].ni[38].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[38].y" 6147.14306640625;
	setAttr ".tgi[0].ni[38].nvs" 18304;
	setAttr ".tgi[0].ni[39].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[39].y" 6667.14306640625;
	setAttr ".tgi[0].ni[39].nvs" 18304;
	setAttr ".tgi[0].ni[40].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[40].y" 9657.142578125;
	setAttr ".tgi[0].ni[40].nvs" 18304;
	setAttr ".tgi[0].ni[41].x" 1427.142822265625;
	setAttr ".tgi[0].ni[41].y" 16261.4287109375;
	setAttr ".tgi[0].ni[41].nvs" 18304;
	setAttr ".tgi[0].ni[42].x" 1427.142822265625;
	setAttr ".tgi[0].ni[42].y" 10827.142578125;
	setAttr ".tgi[0].ni[42].nvs" 18304;
	setAttr ".tgi[0].ni[43].x" 1427.142822265625;
	setAttr ".tgi[0].ni[43].y" 10437.142578125;
	setAttr ".tgi[0].ni[43].nvs" 18304;
	setAttr ".tgi[0].ni[44].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[44].y" 12517.142578125;
	setAttr ".tgi[0].ni[44].nvs" 18304;
	setAttr ".tgi[0].ni[45].x" 1427.142822265625;
	setAttr ".tgi[0].ni[45].y" 10307.142578125;
	setAttr ".tgi[0].ni[45].nvs" 18304;
	setAttr ".tgi[0].ni[46].x" 1427.142822265625;
	setAttr ".tgi[0].ni[46].y" 15002.857421875;
	setAttr ".tgi[0].ni[46].nvs" 18304;
	setAttr ".tgi[0].ni[47].x" 812.85711669921875;
	setAttr ".tgi[0].ni[47].y" 14771.4287109375;
	setAttr ".tgi[0].ni[47].nvs" 18304;
	setAttr ".tgi[0].ni[48].x" 1427.142822265625;
	setAttr ".tgi[0].ni[48].y" 6277.14306640625;
	setAttr ".tgi[0].ni[48].nvs" 18304;
	setAttr ".tgi[0].ni[49].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[49].y" 15850;
	setAttr ".tgi[0].ni[49].nvs" 18304;
	setAttr ".tgi[0].ni[50].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[50].y" 13167.142578125;
	setAttr ".tgi[0].ni[50].nvs" 18304;
	setAttr ".tgi[0].ni[51].x" 802.85711669921875;
	setAttr ".tgi[0].ni[51].y" 16644.28515625;
	setAttr ".tgi[0].ni[51].nvs" 18304;
	setAttr ".tgi[0].ni[52].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[52].y" 7057.14306640625;
	setAttr ".tgi[0].ni[52].nvs" 18304;
	setAttr ".tgi[0].ni[53].x" 1358.5714111328125;
	setAttr ".tgi[0].ni[53].y" 15798.5712890625;
	setAttr ".tgi[0].ni[53].nvs" 18304;
	setAttr ".tgi[0].ni[54].x" 1427.142822265625;
	setAttr ".tgi[0].ni[54].y" 7837.14306640625;
	setAttr ".tgi[0].ni[54].nvs" 18304;
	setAttr ".tgi[0].ni[55].x" 1427.142822265625;
	setAttr ".tgi[0].ni[55].y" 13557.142578125;
	setAttr ".tgi[0].ni[55].nvs" 18304;
	setAttr ".tgi[0].ni[56].x" 188.57142639160156;
	setAttr ".tgi[0].ni[56].y" 16795.71484375;
	setAttr ".tgi[0].ni[56].nvs" 18304;
	setAttr ".tgi[0].ni[57].x" 1427.142822265625;
	setAttr ".tgi[0].ni[57].y" 8487.142578125;
	setAttr ".tgi[0].ni[57].nvs" 18304;
	setAttr ".tgi[0].ni[58].x" 580;
	setAttr ".tgi[0].ni[58].y" 15900;
	setAttr ".tgi[0].ni[58].nvs" 18304;
	setAttr ".tgi[0].ni[59].x" 1427.142822265625;
	setAttr ".tgi[0].ni[59].y" 15517.142578125;
	setAttr ".tgi[0].ni[59].nvs" 18304;
	setAttr ".tgi[0].ni[60].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[60].y" 2637.142822265625;
	setAttr ".tgi[0].ni[60].nvs" 18304;
	setAttr ".tgi[0].ni[61].x" 1427.142822265625;
	setAttr ".tgi[0].ni[61].y" 8617.142578125;
	setAttr ".tgi[0].ni[61].nvs" 18304;
	setAttr ".tgi[0].ni[62].x" 1427.142822265625;
	setAttr ".tgi[0].ni[62].y" 13817.142578125;
	setAttr ".tgi[0].ni[62].nvs" 18304;
	setAttr ".tgi[0].ni[63].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[63].y" 7187.14306640625;
	setAttr ".tgi[0].ni[63].nvs" 18304;
	setAttr ".tgi[0].ni[64].x" 1427.142822265625;
	setAttr ".tgi[0].ni[64].y" 13427.142578125;
	setAttr ".tgi[0].ni[64].nvs" 18304;
	setAttr ".tgi[0].ni[65].x" 804.28570556640625;
	setAttr ".tgi[0].ni[65].y" 15465.7138671875;
	setAttr ".tgi[0].ni[65].nvs" 18304;
	setAttr ".tgi[0].ni[66].x" 1427.142822265625;
	setAttr ".tgi[0].ni[66].y" 9007.142578125;
	setAttr ".tgi[0].ni[66].nvs" 18304;
	setAttr ".tgi[0].ni[67].x" 1427.142822265625;
	setAttr ".tgi[0].ni[67].y" 9397.142578125;
	setAttr ".tgi[0].ni[67].nvs" 18304;
	setAttr ".tgi[0].ni[68].x" 1427.142822265625;
	setAttr ".tgi[0].ni[68].y" 12647.142578125;
	setAttr ".tgi[0].ni[68].nvs" 18304;
	setAttr ".tgi[0].ni[69].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[69].y" 14128.5712890625;
	setAttr ".tgi[0].ni[69].nvs" 18304;
	setAttr ".tgi[0].ni[70].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[70].y" 3677.142822265625;
	setAttr ".tgi[0].ni[70].nvs" 18304;
	setAttr ".tgi[0].ni[71].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[71].y" 8357.142578125;
	setAttr ".tgi[0].ni[71].nvs" 18304;
	setAttr ".tgi[0].ni[72].x" 1427.142822265625;
	setAttr ".tgi[0].ni[72].y" 9267.142578125;
	setAttr ".tgi[0].ni[72].nvs" 18304;
	setAttr ".tgi[0].ni[73].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[73].y" 2247.142822265625;
	setAttr ".tgi[0].ni[73].nvs" 18304;
	setAttr ".tgi[0].ni[74].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[74].y" 16311.4287109375;
	setAttr ".tgi[0].ni[74].nvs" 18304;
	setAttr ".tgi[0].ni[75].x" 1427.142822265625;
	setAttr ".tgi[0].ni[75].y" 5627.14306640625;
	setAttr ".tgi[0].ni[75].nvs" 18304;
	setAttr ".tgi[0].ni[76].x" 1115.7142333984375;
	setAttr ".tgi[0].ni[76].y" 16542.857421875;
	setAttr ".tgi[0].ni[76].nvs" 18304;
	setAttr ".tgi[0].ni[77].x" 1427.142822265625;
	setAttr ".tgi[0].ni[77].y" 11217.142578125;
	setAttr ".tgi[0].ni[77].nvs" 18304;
	setAttr ".tgi[0].ni[78].x" 1427.142822265625;
	setAttr ".tgi[0].ni[78].y" 12387.142578125;
	setAttr ".tgi[0].ni[78].nvs" 18304;
	setAttr ".tgi[0].ni[79].x" 970;
	setAttr ".tgi[0].ni[79].y" 15798.5712890625;
	setAttr ".tgi[0].ni[79].nvs" 18304;
	setAttr ".tgi[0].ni[80].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[80].y" 10827.142578125;
	setAttr ".tgi[0].ni[80].nvs" 18304;
	setAttr ".tgi[0].ni[81].x" 1427.142822265625;
	setAttr ".tgi[0].ni[81].y" 13167.142578125;
	setAttr ".tgi[0].ni[81].nvs" 18304;
	setAttr ".tgi[0].ni[82].x" 1427.142822265625;
	setAttr ".tgi[0].ni[82].y" 14641.4287109375;
	setAttr ".tgi[0].ni[82].nvs" 18304;
	setAttr ".tgi[0].ni[83].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[83].y" 11607.142578125;
	setAttr ".tgi[0].ni[83].nvs" 18304;
	setAttr ".tgi[0].ni[84].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[84].y" 10177.142578125;
	setAttr ".tgi[0].ni[84].nvs" 18304;
	setAttr ".tgi[0].ni[85].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[85].y" 4067.142822265625;
	setAttr ".tgi[0].ni[85].nvs" 18304;
	setAttr ".tgi[0].ni[86].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[86].y" 8877.142578125;
	setAttr ".tgi[0].ni[86].nvs" 18304;
	setAttr ".tgi[0].ni[87].x" 198.57142639160156;
	setAttr ".tgi[0].ni[87].y" 16131.4287109375;
	setAttr ".tgi[0].ni[87].nvs" 18304;
	setAttr ".tgi[0].ni[88].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[88].y" 3547.142822265625;
	setAttr ".tgi[0].ni[88].nvs" 18304;
	setAttr ".tgi[0].ni[89].x" 1427.142822265625;
	setAttr ".tgi[0].ni[89].y" 6537.14306640625;
	setAttr ".tgi[0].ni[89].nvs" 18304;
	setAttr ".tgi[0].ni[90].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[90].y" 4587.14306640625;
	setAttr ".tgi[0].ni[90].nvs" 18304;
	setAttr ".tgi[0].ni[91].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[91].y" 13557.142578125;
	setAttr ".tgi[0].ni[91].nvs" 18304;
	setAttr ".tgi[0].ni[92].x" 1427.142822265625;
	setAttr ".tgi[0].ni[92].y" 12127.142578125;
	setAttr ".tgi[0].ni[92].nvs" 18304;
	setAttr ".tgi[0].ni[93].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[93].y" 1857.142822265625;
	setAttr ".tgi[0].ni[93].nvs" 18304;
	setAttr ".tgi[0].ni[94].x" 1427.142822265625;
	setAttr ".tgi[0].ni[94].y" 6147.14306640625;
	setAttr ".tgi[0].ni[94].nvs" 18304;
	setAttr ".tgi[0].ni[95].x" 812.85711669921875;
	setAttr ".tgi[0].ni[95].y" 15002.857421875;
	setAttr ".tgi[0].ni[95].nvs" 18304;
	setAttr ".tgi[0].ni[96].x" 580;
	setAttr ".tgi[0].ni[96].y" 15798.5712890625;
	setAttr ".tgi[0].ni[96].nvs" 18304;
	setAttr ".tgi[0].ni[97].x" 1427.142822265625;
	setAttr ".tgi[0].ni[97].y" 8227.142578125;
	setAttr ".tgi[0].ni[97].nvs" 18304;
	setAttr ".tgi[0].ni[98].x" 188.57142639160156;
	setAttr ".tgi[0].ni[98].y" 16694.28515625;
	setAttr ".tgi[0].ni[98].nvs" 18304;
	setAttr ".tgi[0].ni[99].x" 812.85711669921875;
	setAttr ".tgi[0].ni[99].y" 16261.4287109375;
	setAttr ".tgi[0].ni[99].nvs" 18304;
	setAttr ".tgi[0].ni[100].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[100].y" 1727.142822265625;
	setAttr ".tgi[0].ni[100].nvs" 18304;
	setAttr ".tgi[0].ni[101].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[101].y" 10697.142578125;
	setAttr ".tgi[0].ni[101].nvs" 18304;
	setAttr ".tgi[0].ni[102].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[102].y" 5367.14306640625;
	setAttr ".tgi[0].ni[102].nvs" 18304;
	setAttr ".tgi[0].ni[103].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[103].y" 3287.142822265625;
	setAttr ".tgi[0].ni[103].nvs" 18304;
	setAttr ".tgi[0].ni[104].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[104].y" 9527.142578125;
	setAttr ".tgi[0].ni[104].nvs" 18304;
	setAttr ".tgi[0].ni[105].x" 1427.142822265625;
	setAttr ".tgi[0].ni[105].y" 13297.142578125;
	setAttr ".tgi[0].ni[105].nvs" 18304;
	setAttr ".tgi[0].ni[106].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[106].y" 4847.14306640625;
	setAttr ".tgi[0].ni[106].nvs" 18304;
	setAttr ".tgi[0].ni[107].x" 1427.142822265625;
	setAttr ".tgi[0].ni[107].y" 13037.142578125;
	setAttr ".tgi[0].ni[107].nvs" 18304;
	setAttr ".tgi[0].ni[108].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[108].y" 14410;
	setAttr ".tgi[0].ni[108].nvs" 18304;
	setAttr ".tgi[0].ni[109].x" 1427.142822265625;
	setAttr ".tgi[0].ni[109].y" 15335.7138671875;
	setAttr ".tgi[0].ni[109].nvs" 18304;
	setAttr ".tgi[0].ni[110].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[110].y" 5887.14306640625;
	setAttr ".tgi[0].ni[110].nvs" 18304;
	setAttr ".tgi[0].ni[111].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[111].y" 14641.4287109375;
	setAttr ".tgi[0].ni[111].nvs" 18304;
	setAttr ".tgi[0].ni[112].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[112].y" 5237.14306640625;
	setAttr ".tgi[0].ni[112].nvs" 18304;
	setAttr ".tgi[0].ni[113].x" 1427.142822265625;
	setAttr ".tgi[0].ni[113].y" 4977.14306640625;
	setAttr ".tgi[0].ni[113].nvs" 18304;
	setAttr ".tgi[0].ni[114].x" 1120;
	setAttr ".tgi[0].ni[114].y" 16261.4287109375;
	setAttr ".tgi[0].ni[114].nvs" 18304;
	setAttr ".tgi[0].ni[115].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[115].y" 1597.142822265625;
	setAttr ".tgi[0].ni[115].nvs" 18304;
	setAttr ".tgi[0].ni[116].x" 1358.5714111328125;
	setAttr ".tgi[0].ni[116].y" 16131.4287109375;
	setAttr ".tgi[0].ni[116].nvs" 18304;
	setAttr ".tgi[0].ni[117].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[117].y" 14771.4287109375;
	setAttr ".tgi[0].ni[117].nvs" 18304;
	setAttr ".tgi[0].ni[118].x" 1120;
	setAttr ".tgi[0].ni[118].y" 14410;
	setAttr ".tgi[0].ni[118].nvs" 18304;
	setAttr ".tgi[0].ni[119].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[119].y" 8617.142578125;
	setAttr ".tgi[0].ni[119].nvs" 18304;
	setAttr ".tgi[0].ni[120].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[120].y" 11347.142578125;
	setAttr ".tgi[0].ni[120].nvs" 18304;
	setAttr ".tgi[0].ni[121].x" 1427.142822265625;
	setAttr ".tgi[0].ni[121].y" 7577.14306640625;
	setAttr ".tgi[0].ni[121].nvs" 18304;
	setAttr ".tgi[0].ni[122].x" 1427.142822265625;
	setAttr ".tgi[0].ni[122].y" 8097.14306640625;
	setAttr ".tgi[0].ni[122].nvs" 18304;
	setAttr ".tgi[0].ni[123].x" 1427.142822265625;
	setAttr ".tgi[0].ni[123].y" 5497.14306640625;
	setAttr ".tgi[0].ni[123].nvs" 18304;
	setAttr ".tgi[0].ni[124].x" 577.14288330078125;
	setAttr ".tgi[0].ni[124].y" 16131.4287109375;
	setAttr ".tgi[0].ni[124].nvs" 18304;
	setAttr ".tgi[0].ni[125].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[125].y" 8227.142578125;
	setAttr ".tgi[0].ni[125].nvs" 18304;
	setAttr ".tgi[0].ni[126].x" 1120;
	setAttr ".tgi[0].ni[126].y" 14178.5712890625;
	setAttr ".tgi[0].ni[126].nvs" 18304;
	setAttr ".tgi[0].ni[127].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[127].y" 10957.142578125;
	setAttr ".tgi[0].ni[127].nvs" 18304;
	setAttr ".tgi[0].ni[128].x" 1427.142822265625;
	setAttr ".tgi[0].ni[128].y" 9787.142578125;
	setAttr ".tgi[0].ni[128].nvs" 18304;
	setAttr ".tgi[0].ni[129].x" 1120;
	setAttr ".tgi[0].ni[129].y" 14540;
	setAttr ".tgi[0].ni[129].nvs" 18304;
	setAttr ".tgi[0].ni[130].x" 812.85711669921875;
	setAttr ".tgi[0].ni[130].y" 14540;
	setAttr ".tgi[0].ni[130].nvs" 18304;
	setAttr ".tgi[0].ni[131].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[131].y" 15285.7138671875;
	setAttr ".tgi[0].ni[131].nvs" 18304;
	setAttr ".tgi[0].ni[132].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[132].y" 11867.142578125;
	setAttr ".tgi[0].ni[132].nvs" 18304;
	setAttr ".tgi[0].ni[133].x" 577.14288330078125;
	setAttr ".tgi[0].ni[133].y" 16030;
	setAttr ".tgi[0].ni[133].nvs" 18304;
	setAttr ".tgi[0].ni[134].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[134].y" 15567.142578125;
	setAttr ".tgi[0].ni[134].nvs" 18304;
	setAttr ".tgi[0].ni[135].x" 1427.142822265625;
	setAttr ".tgi[0].ni[135].y" 12777.142578125;
	setAttr ".tgi[0].ni[135].nvs" 18304;
	setAttr ".tgi[0].ni[136].x" 1427.142822265625;
	setAttr ".tgi[0].ni[136].y" 7967.14306640625;
	setAttr ".tgi[0].ni[136].nvs" 18304;
	setAttr ".tgi[0].ni[137].x" 1427.142822265625;
	setAttr ".tgi[0].ni[137].y" 6407.14306640625;
	setAttr ".tgi[0].ni[137].nvs" 18304;
	setAttr ".tgi[0].ni[138].x" 1427.142822265625;
	setAttr ".tgi[0].ni[138].y" 16592.857421875;
	setAttr ".tgi[0].ni[138].nvs" 18304;
	setAttr ".tgi[0].ni[139].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[139].y" 12777.142578125;
	setAttr ".tgi[0].ni[139].nvs" 18304;
	setAttr ".tgi[0].ni[140].x" 1427.142822265625;
	setAttr ".tgi[0].ni[140].y" 10697.142578125;
	setAttr ".tgi[0].ni[140].nvs" 18304;
	setAttr ".tgi[0].ni[141].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[141].y" 5757.14306640625;
	setAttr ".tgi[0].ni[141].nvs" 18304;
	setAttr ".tgi[0].ni[142].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[142].y" 9137.142578125;
	setAttr ".tgi[0].ni[142].nvs" 18304;
	setAttr ".tgi[0].ni[143].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[143].y" 7577.14306640625;
	setAttr ".tgi[0].ni[143].nvs" 18304;
	setAttr ".tgi[0].ni[144].x" 1120;
	setAttr ".tgi[0].ni[144].y" 14641.4287109375;
	setAttr ".tgi[0].ni[144].nvs" 18304;
	setAttr ".tgi[0].ni[145].x" 1427.142822265625;
	setAttr ".tgi[0].ni[145].y" 7447.14306640625;
	setAttr ".tgi[0].ni[145].nvs" 18304;
	setAttr ".tgi[0].ni[146].x" 495.71429443359375;
	setAttr ".tgi[0].ni[146].y" 16644.28515625;
	setAttr ".tgi[0].ni[146].nvs" 18304;
	setAttr ".tgi[0].ni[147].x" 1427.142822265625;
	setAttr ".tgi[0].ni[147].y" 11997.142578125;
	setAttr ".tgi[0].ni[147].nvs" 18304;
	setAttr ".tgi[0].ni[148].x" 1427.142822265625;
	setAttr ".tgi[0].ni[148].y" 7317.14306640625;
	setAttr ".tgi[0].ni[148].nvs" 18304;
	setAttr ".tgi[0].ni[149].x" 1427.142822265625;
	setAttr ".tgi[0].ni[149].y" 9917.142578125;
	setAttr ".tgi[0].ni[149].nvs" 18304;
	setAttr ".tgi[0].ni[150].x" 812.85711669921875;
	setAttr ".tgi[0].ni[150].y" 14308.5712890625;
	setAttr ".tgi[0].ni[150].nvs" 18304;
	setAttr ".tgi[0].ni[151].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[151].y" 12127.142578125;
	setAttr ".tgi[0].ni[151].nvs" 18304;
	setAttr ".tgi[0].ni[152].x" 1427.142822265625;
	setAttr ".tgi[0].ni[152].y" 14308.5712890625;
	setAttr ".tgi[0].ni[152].nvs" 18304;
	setAttr ".tgi[0].ni[153].x" 495.71429443359375;
	setAttr ".tgi[0].ni[153].y" 16745.71484375;
	setAttr ".tgi[0].ni[153].nvs" 18304;
	setAttr ".tgi[0].ni[154].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[154].y" 11087.142578125;
	setAttr ".tgi[0].ni[154].nvs" 18304;
	setAttr ".tgi[0].ni[155].x" 1427.142822265625;
	setAttr ".tgi[0].ni[155].y" 7707.14306640625;
	setAttr ".tgi[0].ni[155].nvs" 18304;
	setAttr ".tgi[0].ni[156].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[156].y" 2767.142822265625;
	setAttr ".tgi[0].ni[156].nvs" 18304;
	setAttr ".tgi[0].ni[157].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[157].y" 13817.142578125;
	setAttr ".tgi[0].ni[157].nvs" 18304;
	setAttr ".tgi[0].ni[158].x" 1427.142822265625;
	setAttr ".tgi[0].ni[158].y" 14128.5712890625;
	setAttr ".tgi[0].ni[158].nvs" 18304;
	setAttr ".tgi[0].ni[159].x" 1427.142822265625;
	setAttr ".tgi[0].ni[159].y" 11477.142578125;
	setAttr ".tgi[0].ni[159].nvs" 18304;
	setAttr ".tgi[0].ni[160].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[160].y" 12907.142578125;
	setAttr ".tgi[0].ni[160].nvs" 18304;
	setAttr ".tgi[0].ni[161].x" 804.28570556640625;
	setAttr ".tgi[0].ni[161].y" 15668.5712890625;
	setAttr ".tgi[0].ni[161].nvs" 18304;
	setAttr ".tgi[0].ni[162].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[162].y" 14540;
	setAttr ".tgi[0].ni[162].nvs" 18304;
	setAttr ".tgi[0].ni[163].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[163].y" 10307.142578125;
	setAttr ".tgi[0].ni[163].nvs" 18304;
	setAttr ".tgi[0].ni[164].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[164].y" 5627.14306640625;
	setAttr ".tgi[0].ni[164].nvs" 18304;
	setAttr ".tgi[0].ni[165].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[165].y" 10437.142578125;
	setAttr ".tgi[0].ni[165].nvs" 18304;
	setAttr ".tgi[0].ni[166].x" 198.57142639160156;
	setAttr ".tgi[0].ni[166].y" 16311.4287109375;
	setAttr ".tgi[0].ni[166].nvs" 18304;
	setAttr ".tgi[0].ni[167].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[167].y" 7317.14306640625;
	setAttr ".tgi[0].ni[167].nvs" 18304;
	setAttr ".tgi[0].ni[168].x" 1427.142822265625;
	setAttr ".tgi[0].ni[168].y" 9137.142578125;
	setAttr ".tgi[0].ni[168].nvs" 18304;
	setAttr ".tgi[0].ni[169].x" 1427.142822265625;
	setAttr ".tgi[0].ni[169].y" 14540;
	setAttr ".tgi[0].ni[169].nvs" 18304;
	setAttr ".tgi[0].ni[170].x" 1427.142822265625;
	setAttr ".tgi[0].ni[170].y" 13947.142578125;
	setAttr ".tgi[0].ni[170].nvs" 18304;
	setAttr ".tgi[0].ni[171].x" 1427.142822265625;
	setAttr ".tgi[0].ni[171].y" 8747.142578125;
	setAttr ".tgi[0].ni[171].nvs" 18304;
	setAttr ".tgi[0].ni[172].x" 1427.142822265625;
	setAttr ".tgi[0].ni[172].y" 7057.14306640625;
	setAttr ".tgi[0].ni[172].nvs" 18304;
	setAttr ".tgi[0].ni[173].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[173].y" 15104.2861328125;
	setAttr ".tgi[0].ni[173].nvs" 18304;
	setAttr ".tgi[0].ni[174].x" 1427.142822265625;
	setAttr ".tgi[0].ni[174].y" 8877.142578125;
	setAttr ".tgi[0].ni[174].nvs" 18304;
	setAttr ".tgi[0].ni[175].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[175].y" 4457.14306640625;
	setAttr ".tgi[0].ni[175].nvs" 18304;
	setAttr ".tgi[0].ni[176].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[176].y" 7837.14306640625;
	setAttr ".tgi[0].ni[176].nvs" 18304;
	setAttr ".tgi[0].ni[177].x" 1427.142822265625;
	setAttr ".tgi[0].ni[177].y" 16694.28515625;
	setAttr ".tgi[0].ni[177].nvs" 18304;
	setAttr ".tgi[0].ni[178].x" 1427.142822265625;
	setAttr ".tgi[0].ni[178].y" 14872.857421875;
	setAttr ".tgi[0].ni[178].nvs" 18304;
	setAttr ".tgi[0].ni[179].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[179].y" 12257.142578125;
	setAttr ".tgi[0].ni[179].nvs" 18304;
	setAttr ".tgi[0].ni[180].x" 970;
	setAttr ".tgi[0].ni[180].y" 15900;
	setAttr ".tgi[0].ni[180].nvs" 18304;
	setAttr ".tgi[0].ni[181].x" 1427.142822265625;
	setAttr ".tgi[0].ni[181].y" 11737.142578125;
	setAttr ".tgi[0].ni[181].nvs" 18304;
	setAttr ".tgi[0].ni[182].x" 1120;
	setAttr ".tgi[0].ni[182].y" 14308.5712890625;
	setAttr ".tgi[0].ni[182].nvs" 18304;
	setAttr ".tgi[0].ni[183].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[183].y" 7707.14306640625;
	setAttr ".tgi[0].ni[183].nvs" 18304;
	setAttr ".tgi[0].ni[184].x" 1427.142822265625;
	setAttr ".tgi[0].ni[184].y" 16362.857421875;
	setAttr ".tgi[0].ni[184].nvs" 18304;
	setAttr ".tgi[0].ni[185].x" 1427.142822265625;
	setAttr ".tgi[0].ni[185].y" 12517.142578125;
	setAttr ".tgi[0].ni[185].nvs" 18304;
	setAttr ".tgi[0].ni[186].x" 1427.142822265625;
	setAttr ".tgi[0].ni[186].y" 5237.14306640625;
	setAttr ".tgi[0].ni[186].nvs" 18304;
	setAttr ".tgi[0].ni[187].x" 198.57142639160156;
	setAttr ".tgi[0].ni[187].y" 16412.857421875;
	setAttr ".tgi[0].ni[187].nvs" 18304;
	setAttr ".tgi[0].ni[188].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[188].y" 2117.142822265625;
	setAttr ".tgi[0].ni[188].nvs" 18304;
	setAttr ".tgi[0].ni[189].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[189].y" 8097.14306640625;
	setAttr ".tgi[0].ni[189].nvs" 18304;
	setAttr ".tgi[0].ni[190].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[190].y" 12387.142578125;
	setAttr ".tgi[0].ni[190].nvs" 18304;
	setAttr ".tgi[0].ni[191].x" 1427.142822265625;
	setAttr ".tgi[0].ni[191].y" 11607.142578125;
	setAttr ".tgi[0].ni[191].nvs" 18304;
	setAttr ".tgi[0].ni[192].x" 1427.142822265625;
	setAttr ".tgi[0].ni[192].y" 11347.142578125;
	setAttr ".tgi[0].ni[192].nvs" 18304;
	setAttr ".tgi[0].ni[193].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[193].y" 3417.142822265625;
	setAttr ".tgi[0].ni[193].nvs" 18304;
	setAttr ".tgi[0].ni[194].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[194].y" 7447.14306640625;
	setAttr ".tgi[0].ni[194].nvs" 18304;
	setAttr ".tgi[0].ni[195].x" 1120;
	setAttr ".tgi[0].ni[195].y" 14771.4287109375;
	setAttr ".tgi[0].ni[195].nvs" 18304;
	setAttr ".tgi[0].ni[196].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[196].y" 3157.142822265625;
	setAttr ".tgi[0].ni[196].nvs" 18304;
	setAttr ".tgi[0].ni[197].x" 1427.142822265625;
	setAttr ".tgi[0].ni[197].y" 11087.142578125;
	setAttr ".tgi[0].ni[197].nvs" 18304;
	setAttr ".tgi[0].ni[198].x" 1427.142822265625;
	setAttr ".tgi[0].ni[198].y" 13687.142578125;
	setAttr ".tgi[0].ni[198].nvs" 18304;
	setAttr ".tgi[0].ni[199].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[199].y" 5497.14306640625;
	setAttr ".tgi[0].ni[199].nvs" 18304;
	setAttr ".tgi[0].ni[200].x" 1427.142822265625;
	setAttr ".tgi[0].ni[200].y" 4717.14306640625;
	setAttr ".tgi[0].ni[200].nvs" 18304;
	setAttr ".tgi[0].ni[201].x" 1427.142822265625;
	setAttr ".tgi[0].ni[201].y" 7187.14306640625;
	setAttr ".tgi[0].ni[201].nvs" 18304;
	setAttr ".tgi[0].ni[202].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[202].y" 1987.142822265625;
	setAttr ".tgi[0].ni[202].nvs" 18304;
	setAttr ".tgi[0].ni[203].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[203].y" 16660;
	setAttr ".tgi[0].ni[203].nvs" 18304;
	setAttr ".tgi[0].ni[204].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[204].y" 16081.4287109375;
	setAttr ".tgi[0].ni[204].nvs" 18304;
	setAttr ".tgi[0].ni[205].x" 1120;
	setAttr ".tgi[0].ni[205].y" 15335.7138671875;
	setAttr ".tgi[0].ni[205].nvs" 18304;
	setAttr ".tgi[0].ni[206].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[206].y" 9267.142578125;
	setAttr ".tgi[0].ni[206].nvs" 18304;
	setAttr ".tgi[0].ni[207].x" 1427.142822265625;
	setAttr ".tgi[0].ni[207].y" 5107.14306640625;
	setAttr ".tgi[0].ni[207].nvs" 18304;
	setAttr ".tgi[0].ni[208].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[208].y" 9397.142578125;
	setAttr ".tgi[0].ni[208].nvs" 18304;
	setAttr ".tgi[0].ni[209].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[209].y" 6017.14306640625;
	setAttr ".tgi[0].ni[209].nvs" 18304;
	setAttr ".tgi[0].ni[210].x" 1427.142822265625;
	setAttr ".tgi[0].ni[210].y" 4587.14306640625;
	setAttr ".tgi[0].ni[210].nvs" 18304;
	setAttr ".tgi[0].ni[211].x" 1120;
	setAttr ".tgi[0].ni[211].y" 15104.2861328125;
	setAttr ".tgi[0].ni[211].nvs" 18304;
	setAttr ".tgi[0].ni[212].x" 1427.142822265625;
	setAttr ".tgi[0].ni[212].y" 10567.142578125;
	setAttr ".tgi[0].ni[212].nvs" 18304;
	setAttr ".tgi[0].ni[213].x" 1427.142822265625;
	setAttr ".tgi[0].ni[213].y" 15234.2861328125;
	setAttr ".tgi[0].ni[213].nvs" 18304;
	setAttr ".tgi[0].ni[214].x" 1120;
	setAttr ".tgi[0].ni[214].y" 15002.857421875;
	setAttr ".tgi[0].ni[214].nvs" 18304;
	setAttr ".tgi[0].ni[215].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[215].y" 2897.142822265625;
	setAttr ".tgi[0].ni[215].nvs" 18304;
	setAttr ".tgi[0].ni[216].x" 1427.142822265625;
	setAttr ".tgi[0].ni[216].y" 6797.14306640625;
	setAttr ".tgi[0].ni[216].nvs" 18304;
	setAttr ".tgi[0].ni[217].x" 1120;
	setAttr ".tgi[0].ni[217].y" 14872.857421875;
	setAttr ".tgi[0].ni[217].nvs" 18304;
	setAttr ".tgi[0].ni[218].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[218].y" 8747.142578125;
	setAttr ".tgi[0].ni[218].nvs" 18304;
	setAttr ".tgi[0].ni[219].x" 968.5714111328125;
	setAttr ".tgi[0].ni[219].y" 16030;
	setAttr ".tgi[0].ni[219].nvs" 18304;
	setAttr ".tgi[0].ni[220].x" 968.5714111328125;
	setAttr ".tgi[0].ni[220].y" 16131.4287109375;
	setAttr ".tgi[0].ni[220].nvs" 18304;
	setAttr ".tgi[0].ni[221].x" 1115.7142333984375;
	setAttr ".tgi[0].ni[221].y" 16745.71484375;
	setAttr ".tgi[0].ni[221].nvs" 18304;
	setAttr ".tgi[0].ni[222].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[222].y" 6407.14306640625;
	setAttr ".tgi[0].ni[222].nvs" 18304;
	setAttr ".tgi[0].ni[223].x" 812.85711669921875;
	setAttr ".tgi[0].ni[223].y" 16362.857421875;
	setAttr ".tgi[0].ni[223].nvs" 18304;
	setAttr ".tgi[0].ni[224].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[224].y" 4327.14306640625;
	setAttr ".tgi[0].ni[224].nvs" 18304;
	setAttr ".tgi[0].ni[225].x" 1117.142822265625;
	setAttr ".tgi[0].ni[225].y" 15465.7138671875;
	setAttr ".tgi[0].ni[225].nvs" 18304;
	setAttr ".tgi[0].ni[226].x" 202.85714721679688;
	setAttr ".tgi[0].ni[226].y" 15900;
	setAttr ".tgi[0].ni[226].nvs" 18304;
	setAttr ".tgi[0].ni[227].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[227].y" 13687.142578125;
	setAttr ".tgi[0].ni[227].nvs" 18304;
	setAttr ".tgi[0].ni[228].x" 1427.142822265625;
	setAttr ".tgi[0].ni[228].y" 6017.14306640625;
	setAttr ".tgi[0].ni[228].nvs" 18304;
	setAttr ".tgi[0].ni[229].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[229].y" 14872.857421875;
	setAttr ".tgi[0].ni[229].nvs" 18304;
	setAttr ".tgi[0].ni[230].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[230].y" 11997.142578125;
	setAttr ".tgi[0].ni[230].nvs" 18304;
	setAttr ".tgi[0].ni[231].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[231].y" 11217.142578125;
	setAttr ".tgi[0].ni[231].nvs" 18304;
	setAttr ".tgi[0].ni[232].x" 505.71429443359375;
	setAttr ".tgi[0].ni[232].y" 15234.2861328125;
	setAttr ".tgi[0].ni[232].nvs" 18304;
	setAttr ".tgi[0].ni[233].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[233].y" 5107.14306640625;
	setAttr ".tgi[0].ni[233].nvs" 18304;
	setAttr ".tgi[0].ni[234].x" 1427.142822265625;
	setAttr ".tgi[0].ni[234].y" 4847.14306640625;
	setAttr ".tgi[0].ni[234].nvs" 18304;
	setAttr ".tgi[0].ni[235].x" 1117.142822265625;
	setAttr ".tgi[0].ni[235].y" 15567.142578125;
	setAttr ".tgi[0].ni[235].nvs" 18304;
	setAttr ".tgi[0].ni[236].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[236].y" 4717.14306640625;
	setAttr ".tgi[0].ni[236].nvs" 18304;
	setAttr ".tgi[0].ni[237].x" 1427.142822265625;
	setAttr ".tgi[0].ni[237].y" 6927.14306640625;
	setAttr ".tgi[0].ni[237].nvs" 18304;
	setAttr ".tgi[0].ni[238].x" 1120;
	setAttr ".tgi[0].ni[238].y" 15234.2861328125;
	setAttr ".tgi[0].ni[238].nvs" 18304;
	setAttr ".tgi[0].ni[239].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[239].y" 3807.142822265625;
	setAttr ".tgi[0].ni[239].nvs" 18304;
	setAttr ".tgi[0].ni[240].x" 1427.142822265625;
	setAttr ".tgi[0].ni[240].y" 10957.142578125;
	setAttr ".tgi[0].ni[240].nvs" 18304;
	setAttr ".tgi[0].ni[241].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[241].y" 4977.14306640625;
	setAttr ".tgi[0].ni[241].nvs" 18304;
	setAttr ".tgi[0].ni[242].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[242].y" 9917.142578125;
	setAttr ".tgi[0].ni[242].nvs" 18304;
	setAttr ".tgi[0].ni[243].x" 1427.142822265625;
	setAttr ".tgi[0].ni[243].y" 8357.142578125;
	setAttr ".tgi[0].ni[243].nvs" 18304;
	setAttr ".tgi[0].ni[244].x" 1358.5714111328125;
	setAttr ".tgi[0].ni[244].y" 16030;
	setAttr ".tgi[0].ni[244].nvs" 18304;
	setAttr ".tgi[0].ni[245].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[245].y" 3027.142822265625;
	setAttr ".tgi[0].ni[245].nvs" 18304;
	setAttr ".tgi[0].ni[246].x" 804.28570556640625;
	setAttr ".tgi[0].ni[246].y" 15567.142578125;
	setAttr ".tgi[0].ni[246].nvs" 18304;
	setAttr ".tgi[0].ni[247].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[247].y" 13427.142578125;
	setAttr ".tgi[0].ni[247].nvs" 18304;
	setAttr ".tgi[0].ni[248].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[248].y" 15002.857421875;
	setAttr ".tgi[0].ni[248].nvs" 18304;
	setAttr ".tgi[0].ni[249].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[249].y" 10567.142578125;
	setAttr ".tgi[0].ni[249].nvs" 18304;
	setAttr ".tgi[0].ni[250].x" 1427.142822265625;
	setAttr ".tgi[0].ni[250].y" 15104.2861328125;
	setAttr ".tgi[0].ni[250].nvs" 18304;
	setAttr ".tgi[0].ni[251].x" 1427.142822265625;
	setAttr ".tgi[0].ni[251].y" 9657.142578125;
	setAttr ".tgi[0].ni[251].nvs" 18304;
	setAttr ".tgi[0].ni[252].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[252].y" 11737.142578125;
	setAttr ".tgi[0].ni[252].nvs" 18304;
	setAttr ".tgi[0].ni[253].x" 1427.142822265625;
	setAttr ".tgi[0].ni[253].y" 6667.14306640625;
	setAttr ".tgi[0].ni[253].nvs" 18304;
	setAttr ".tgi[0].ni[254].x" 1427.142822265625;
	setAttr ".tgi[0].ni[254].y" 14771.4287109375;
	setAttr ".tgi[0].ni[254].nvs" 18304;
	setAttr ".tgi[0].ni[255].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[255].y" 9007.142578125;
	setAttr ".tgi[0].ni[255].nvs" 18304;
	setAttr ".tgi[0].ni[256].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[256].y" 4197.14306640625;
	setAttr ".tgi[0].ni[256].nvs" 18304;
	setAttr ".tgi[0].ni[257].x" 1427.142822265625;
	setAttr ".tgi[0].ni[257].y" 5757.14306640625;
	setAttr ".tgi[0].ni[257].nvs" 18304;
	setAttr ".tgi[0].ni[258].x" 1734.2857666015625;
	setAttr ".tgi[0].ni[258].y" 6797.14306640625;
	setAttr ".tgi[0].ni[258].nvs" 18304;
	setAttr ".tgi[0].ni[259].x" 1427.142822265625;
	setAttr ".tgi[0].ni[259].y" 12257.142578125;
	setAttr ".tgi[0].ni[259].nvs" 18304;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "7DE60D66-47E7-2463-95BC-95844731E0C3";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -5501.3734077686886 -4482.600554478191 ;
	setAttr ".tgi[0].vh" -type "double2" 5603.2506930977388 4564.1023827414301 ;
	setAttr -s 36 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" -55.714286804199219;
	setAttr ".tgi[0].ni[0].y" -2430;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" -124.28571319580078;
	setAttr ".tgi[0].ni[1].y" 3270;
	setAttr ".tgi[0].ni[1].nvs" 1923;
	setAttr ".tgi[0].ni[2].x" -55.714286804199219;
	setAttr ".tgi[0].ni[2].y" 12.857142448425293;
	setAttr ".tgi[0].ni[2].nvs" 1923;
	setAttr ".tgi[0].ni[3].x" -55.714286804199219;
	setAttr ".tgi[0].ni[3].y" -3698.571533203125;
	setAttr ".tgi[0].ni[3].nvs" 1923;
	setAttr ".tgi[0].ni[4].x" -55.714286804199219;
	setAttr ".tgi[0].ni[4].y" 2184.28564453125;
	setAttr ".tgi[0].ni[4].nvs" 1923;
	setAttr ".tgi[0].ni[5].x" -124.28571319580078;
	setAttr ".tgi[0].ni[5].y" 3541.428466796875;
	setAttr ".tgi[0].ni[5].nvs" 1923;
	setAttr ".tgi[0].ni[6].x" -55.714286804199219;
	setAttr ".tgi[0].ni[6].y" -2701.428466796875;
	setAttr ".tgi[0].ni[6].nvs" 1923;
	setAttr ".tgi[0].ni[7].x" -55.714286804199219;
	setAttr ".tgi[0].ni[7].y" -3880;
	setAttr ".tgi[0].ni[7].nvs" 1923;
	setAttr ".tgi[0].ni[8].x" -91.428573608398438;
	setAttr ".tgi[0].ni[8].y" 51.428569793701172;
	setAttr ".tgi[0].ni[8].nvs" 1922;
	setAttr ".tgi[0].ni[9].x" -124.28571319580078;
	setAttr ".tgi[0].ni[9].y" 2998.571533203125;
	setAttr ".tgi[0].ni[9].nvs" 1923;
	setAttr ".tgi[0].ni[10].x" -55.714286804199219;
	setAttr ".tgi[0].ni[10].y" -530;
	setAttr ".tgi[0].ni[10].nvs" 1923;
	setAttr ".tgi[0].ni[11].x" -55.714286804199219;
	setAttr ".tgi[0].ni[11].y" 284.28570556640625;
	setAttr ".tgi[0].ni[11].nvs" 1923;
	setAttr ".tgi[0].ni[12].x" -55.714286804199219;
	setAttr ".tgi[0].ni[12].y" -2158.571533203125;
	setAttr ".tgi[0].ni[12].nvs" 1923;
	setAttr ".tgi[0].ni[13].x" -55.714286804199219;
	setAttr ".tgi[0].ni[13].y" 827.14288330078125;
	setAttr ".tgi[0].ni[13].nvs" 1923;
	setAttr ".tgi[0].ni[14].x" -55.714286804199219;
	setAttr ".tgi[0].ni[14].y" 1641.4285888671875;
	setAttr ".tgi[0].ni[14].nvs" 1923;
	setAttr ".tgi[0].ni[15].x" -55.714286804199219;
	setAttr ".tgi[0].ni[15].y" -2972.857177734375;
	setAttr ".tgi[0].ni[15].nvs" 1923;
	setAttr ".tgi[0].ni[16].x" -55.714286804199219;
	setAttr ".tgi[0].ni[16].y" 555.71429443359375;
	setAttr ".tgi[0].ni[16].nvs" 1923;
	setAttr ".tgi[0].ni[17].x" -125.71428680419922;
	setAttr ".tgi[0].ni[17].y" 3768.571533203125;
	setAttr ".tgi[0].ni[17].nvs" 1923;
	setAttr ".tgi[0].ni[18].x" -55.714286804199219;
	setAttr ".tgi[0].ni[18].y" -801.4285888671875;
	setAttr ".tgi[0].ni[18].nvs" 1923;
	setAttr ".tgi[0].ni[19].x" -55.714286804199219;
	setAttr ".tgi[0].ni[19].y" -1344.2857666015625;
	setAttr ".tgi[0].ni[19].nvs" 1923;
	setAttr ".tgi[0].ni[20].x" -55.714286804199219;
	setAttr ".tgi[0].ni[20].y" -3244.28564453125;
	setAttr ".tgi[0].ni[20].nvs" 1923;
	setAttr ".tgi[0].ni[21].x" -91.428573608398438;
	setAttr ".tgi[0].ni[21].y" 512.85711669921875;
	setAttr ".tgi[0].ni[21].nvs" 1923;
	setAttr ".tgi[0].ni[22].x" -55.714286804199219;
	setAttr ".tgi[0].ni[22].y" -1072.857177734375;
	setAttr ".tgi[0].ni[22].nvs" 1923;
	setAttr ".tgi[0].ni[23].x" -55.714286804199219;
	setAttr ".tgi[0].ni[23].y" 2455.71435546875;
	setAttr ".tgi[0].ni[23].nvs" 1923;
	setAttr ".tgi[0].ni[24].x" -124.28571319580078;
	setAttr ".tgi[0].ni[24].y" 2727.142822265625;
	setAttr ".tgi[0].ni[24].nvs" 1923;
	setAttr ".tgi[0].ni[25].x" -55.714286804199219;
	setAttr ".tgi[0].ni[25].y" -1887.142822265625;
	setAttr ".tgi[0].ni[25].nvs" 1923;
	setAttr ".tgi[0].ni[26].x" -91.428573608398438;
	setAttr ".tgi[0].ni[26].y" -212.85714721679688;
	setAttr ".tgi[0].ni[26].nvs" 1923;
	setAttr ".tgi[0].ni[27].x" -91.428573608398438;
	setAttr ".tgi[0].ni[27].y" 331.42855834960938;
	setAttr ".tgi[0].ni[27].nvs" 1923;
	setAttr ".tgi[0].ni[28].x" -55.714286804199219;
	setAttr ".tgi[0].ni[28].y" 1370;
	setAttr ".tgi[0].ni[28].nvs" 1923;
	setAttr ".tgi[0].ni[29].x" -127.14286041259766;
	setAttr ".tgi[0].ni[29].y" 3995.71435546875;
	setAttr ".tgi[0].ni[29].nvs" 1923;
	setAttr ".tgi[0].ni[30].x" -91.428573608398438;
	setAttr ".tgi[0].ni[30].y" 694.28570556640625;
	setAttr ".tgi[0].ni[30].nvs" 1923;
	setAttr ".tgi[0].ni[31].x" -55.714286804199219;
	setAttr ".tgi[0].ni[31].y" -258.57144165039063;
	setAttr ".tgi[0].ni[31].nvs" 1923;
	setAttr ".tgi[0].ni[32].x" -55.714286804199219;
	setAttr ".tgi[0].ni[32].y" 1098.5714111328125;
	setAttr ".tgi[0].ni[32].nvs" 1923;
	setAttr ".tgi[0].ni[33].x" -55.714286804199219;
	setAttr ".tgi[0].ni[33].y" 1912.857177734375;
	setAttr ".tgi[0].ni[33].nvs" 1923;
	setAttr ".tgi[0].ni[34].x" -55.714286804199219;
	setAttr ".tgi[0].ni[34].y" -3471.428466796875;
	setAttr ".tgi[0].ni[34].nvs" 1923;
	setAttr ".tgi[0].ni[35].x" -55.714286804199219;
	setAttr ".tgi[0].ni[35].y" -1615.7142333984375;
	setAttr ".tgi[0].ni[35].nvs" 1923;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av -k on ".unw" 1;
	setAttr -av -k on ".etw";
	setAttr -av -k on ".tps";
	setAttr -av -k on ".tms";
select -ne :hardwareRenderingGlobals;
	setAttr -av -k on ".ihi";
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".cons" no;
	setAttr -av ".ta" 0;
	setAttr -av ".tq";
	setAttr ".tmr" 1024;
	setAttr -av ".aoam";
	setAttr -av ".aora";
	setAttr -av ".hfd";
	setAttr -av ".hfe";
	setAttr -av ".hfa";
	setAttr -av ".mbe";
	setAttr -av -k on ".mbsof";
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 30 ".u";
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -av -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -av -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -av -k on ".isu";
	setAttr -av -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRedshiftPostEffects.msg" ":redshiftOptions.postEffects";
connectAttr "Puff_R_Mdv.oy" "PuffOut_R_Pma.i1[0]";
connectAttr "PuffAttr_R_Mdv.oy" "PuffOut_R_Pma.i1[1]";
connectAttr "Puff_R_Cmp.opr" "Puff_R_Mdv.i1x";
connectAttr "Puff_R_Cmp.opg" "Puff_R_Mdv.i1y";
connectAttr "Puff_R_Bcl.opr" "Puff_R_Cmp.ipr";
connectAttr "Puff_R_Bcl.opg" "Puff_R_Cmp.ipg";
connectAttr "PuffAttr_R_Cmp.opr" "PuffAttr_R_Mdv.i1x";
connectAttr "PuffAttr_R_Cmp.opg" "PuffAttr_R_Mdv.i1y";
connectAttr "Puff_L_Mdv.oy" "PuffOut_L_Pma.i1[0]";
connectAttr "PuffAttr_L_Mdv.oy" "PuffOut_L_Pma.i1[1]";
connectAttr "PuffAttr_L_Cmp.opr" "PuffAttr_L_Mdv.i1x";
connectAttr "PuffAttr_L_Cmp.opg" "PuffAttr_L_Mdv.i1y";
connectAttr "Puff_L_Cmp.opr" "Puff_L_Mdv.i1x";
connectAttr "Puff_L_Cmp.opg" "Puff_L_Mdv.i1y";
connectAttr "Puff_L_Bcl.opr" "Puff_L_Cmp.ipr";
connectAttr "Puff_L_Bcl.opg" "Puff_L_Cmp.ipg";
connectAttr "Puff_R_Mdv.ox" "PuffIn_R_Pma.i1[0]";
connectAttr "PuffAttr_R_Mdv.ox" "PuffIn_R_Pma.i1[1]";
connectAttr "Puff_L_Mdv.ox" "PuffIn_L_Pma.i1[0]";
connectAttr "PuffAttr_L_Mdv.ox" "PuffIn_L_Pma.i1[1]";
connectAttr "FacialCtrlMover_EyeLidsLwrUD_R_Mdv.ox" "FacialCtrlMover_EyeLidsLwrUp_R_Pma.i1[0]"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_R_Mdv.ox" "FacialCtrlMover_EyeLidsLwrUp_R_Pma.i1[1]"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUD_R_Cmp.opr" "FacialCtrlMover_EyeLidsLwrUD_R_Mdv.i1x"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUD_R_Cmp.opg" "FacialCtrlMover_EyeLidsLwrUD_R_Mdv.i1y"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_R_Cmp.opr" "FacialCtrlMover_EyeLidsLwrAuto_R_Mdv.i1x"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_R_Cmp.opg" "FacialCtrlMover_EyeLidsLwrAuto_R_Mdv.i1y"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_R_Bcl.opr" "FacialCtrlMover_EyeLidsLwrAuto_R_Cmp.ipr"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_R_Bcl.opg" "FacialCtrlMover_EyeLidsLwrAuto_R_Cmp.ipg"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUD_C_Mdv.ox" "FacialCtrlMover_EyeLidsLwrUp_C_Pma.i1[0]"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_L_Mdv.ox" "FacialCtrlMover_EyeLidsLwrUp_C_Pma.i1[1]"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUD_C_Cmp.opr" "FacialCtrlMover_EyeLidsLwrUD_C_Mdv.i1x"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUD_C_Cmp.opg" "FacialCtrlMover_EyeLidsLwrUD_C_Mdv.i1y"
		;
connectAttr "EyeLidsLwrBsh_C_Ctrl.ty" "FacialCtrlMover_EyeLidsLwrUD_C_Cmp.ipr";
connectAttr "EyeLidsLwrBsh_C_Ctrl.ty" "FacialCtrlMover_EyeLidsLwrUD_C_Cmp.ipg";
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_L_Cmp.opr" "FacialCtrlMover_EyeLidsLwrAuto_L_Mdv.i1x"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_L_Cmp.opg" "FacialCtrlMover_EyeLidsLwrAuto_L_Mdv.i1y"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_L_Bcl.opr" "FacialCtrlMover_EyeLidsLwrAuto_L_Cmp.ipr"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_L_Bcl.opg" "FacialCtrlMover_EyeLidsLwrAuto_L_Cmp.ipg"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUD_R_Mdv.oy" "FacialCtrlMover_EyeLidsLwrDn_R_Pma.i1[0]"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_R_Mdv.oy" "FacialCtrlMover_EyeLidsLwrDn_R_Pma.i1[1]"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUD_C_Mdv.oy" "FacialCtrlMover_EyeLidsLwrDn_C_Pma.i1[0]"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_L_Mdv.oy" "FacialCtrlMover_EyeLidsLwrDn_C_Pma.i1[1]"
		;
connectAttr "Cheek_R_Mdv.ox" "CheekUp_R_Pma.i1[0]";
connectAttr "CheekAttr_R_Mdv.ox" "CheekUp_R_Pma.i1[1]";
connectAttr "CheekAttr_R_Cmp.opr" "CheekAttr_R_Mdv.i1x";
connectAttr "CheekAttr_R_Cmp.opg" "CheekAttr_R_Mdv.i1y";
connectAttr "Cheek_R_Cmp.opr" "Cheek_R_Mdv.i1x";
connectAttr "Cheek_R_Cmp.opg" "Cheek_R_Mdv.i1y";
connectAttr "Cheek_R_Bcl.opr" "Cheek_R_Cmp.ipr";
connectAttr "Cheek_R_Bcl.opg" "Cheek_R_Cmp.ipg";
connectAttr "Cheek_L_Mdv.ox" "CheekUp_L_Pma.i1[0]";
connectAttr "CheekAttr_L_Mdv.ox" "CheekUp_L_Pma.i1[1]";
connectAttr "Cheek_L_Cmp.opr" "Cheek_L_Mdv.i1x";
connectAttr "Cheek_L_Cmp.opg" "Cheek_L_Mdv.i1y";
connectAttr "Cheek_L_Bcl.opr" "Cheek_L_Cmp.ipr";
connectAttr "Cheek_L_Bcl.opg" "Cheek_L_Cmp.ipg";
connectAttr "CheekAttr_L_Cmp.opr" "CheekAttr_L_Mdv.i1x";
connectAttr "CheekAttr_L_Cmp.opg" "CheekAttr_L_Mdv.i1y";
connectAttr "Cheek_R_Mdv.oy" "CheekDn_R_Pma.i1[0]";
connectAttr "CheekAttr_R_Mdv.oy" "CheekDn_R_Pma.i1[1]";
connectAttr "Cheek_L_Mdv.oy" "CheekDn_L_Pma.i1[0]";
connectAttr "CheekAttr_L_Mdv.oy" "CheekDn_L_Pma.i1[1]";
connectAttr "PuffAttrIO_R_Cmp.opr" "PuffAttrIO_R_Mdv.i1x";
connectAttr "PuffAttrIO_R_Cmp.opg" "PuffAttrIO_R_Mdv.i1y";
connectAttr "PuffAttrIO_L_Cmp.opr" "PuffAttrIO_L_Mdv.i1x";
connectAttr "PuffAttrIO_L_Cmp.opg" "PuffAttrIO_L_Mdv.i1y";
connectAttr "NoseUD_R_Cmp.opr" "NoseUD_R_Mdv.i1x";
connectAttr "NoseUD_R_Cmp.opg" "NoseUD_R_Mdv.i1y";
connectAttr "NoseUD_Cmp.opr" "NoseUD_Mdv.i1x";
connectAttr "NoseUD_Cmp.opg" "NoseUD_Mdv.i1y";
connectAttr "NoseUD_L_Cmp.opr" "NoseUD_L_Mdv.i1x";
connectAttr "NoseUD_L_Cmp.opg" "NoseUD_L_Mdv.i1y";
connectAttr "NoseStSq_Cmp.opr" "NoseStSq_Mdv.i1x";
connectAttr "NoseStSq_Cmp.opg" "NoseStSq_Mdv.i1y";
connectAttr "NoseLR_Cmp.opr" "NoseLR_Mdv.i1x";
connectAttr "NoseLR_Cmp.opg" "NoseLR_Mdv.i1y";
connectAttr "NoseFC_R_Cmp.opr" "NoseFC_R_Mdv.i1x";
connectAttr "NoseFC_R_Cmp.opg" "NoseFC_R_Mdv.i1y";
connectAttr "NoseFC_L_Cmp.opr" "NoseFC_L_Mdv.i1x";
connectAttr "NoseFC_L_Cmp.opg" "NoseFC_L_Mdv.i1y";
connectAttr "MouthU_Cmp.opr" "MouthU_Mdv.i1x";
connectAttr "MouthUD_Cmp.opr" "MouthUD_Mdv.i1x";
connectAttr "MouthUD_Cmp.opg" "MouthUD_Mdv.i1y";
connectAttr "MouthPull_Cmp.opr" "MouthPull_Mdv.i1x";
connectAttr "MouthLR_Cmp.opr" "MouthLR_Mdv.i1x";
connectAttr "MouthLR_Cmp.opg" "MouthLR_Mdv.i1y";
connectAttr "MouthFC_Cmp.opr" "MouthFC_Mdv.i1x";
connectAttr "MouthFC_Cmp.opg" "MouthFC_Mdv.i1y";
connectAttr "MouthClench_Cmp.opr" "MouthClench_Mdv.i1x";
connectAttr "LipsUprCurlIO_Cmp.opr" "LipsUprCurlIO_Mdv.i1x";
connectAttr "LipsUprCurlIO_Cmp.opg" "LipsUprCurlIO_Mdv.i1y";
connectAttr "LipsRgtUprAttrUD_Cmp.opr" "LipsRgtUprAttrUD_Mdv.i1x";
connectAttr "LipsRgtUprAttrUD_Cmp.opg" "LipsRgtUprAttrUD_Mdv.i1y";
connectAttr "LipsRgtLwrAttrUD_Cmp.opr" "LipsRgtLwrAttrUD_Mdv.i1x";
connectAttr "LipsRgtLwrAttrUD_Cmp.opg" "LipsRgtLwrAttrUD_Mdv.i1y";
connectAttr "LipsPartIO_R_Cmp.opr" "LipsPartIO_R_Mdv.i1x";
connectAttr "LipsPartIO_R_Cmp.opg" "LipsPartIO_R_Mdv.i1y";
connectAttr "LipsPartIO_L_Cmp.opr" "LipsPartIO_L_Mdv.i1x";
connectAttr "LipsPartIO_L_Cmp.opg" "LipsPartIO_L_Mdv.i1y";
connectAttr "LipsMidUprAttrUD_Cmp.opr" "LipsMidUprAttrUD_Mdv.i1x";
connectAttr "LipsMidUprAttrUD_Cmp.opg" "LipsMidUprAttrUD_Mdv.i1y";
connectAttr "LipsMidLwrAttrUD_Cmp.opr" "LipsMidLwrAttrUD_Mdv.i1x";
connectAttr "LipsMidLwrAttrUD_Cmp.opg" "LipsMidLwrAttrUD_Mdv.i1y";
connectAttr "LipsLwrCurlIO_Cmp.opr" "LipsLwrCurlIO_Mdv.i1x";
connectAttr "LipsLwrCurlIO_Cmp.opg" "LipsLwrCurlIO_Mdv.i1y";
connectAttr "LipsLftUprAttrUD_Cmp.opr" "LipsLftUprAttrUD_Mdv.i1x";
connectAttr "LipsLftUprAttrUD_Cmp.opg" "LipsLftUprAttrUD_Mdv.i1y";
connectAttr "LipsLftLwrAttrUD_Cmp.opr" "LipsLftLwrAttrUD_Mdv.i1x";
connectAttr "LipsLftLwrAttrUD_Cmp.opg" "LipsLftLwrAttrUD_Mdv.i1y";
connectAttr "LipsCnrUD_R_Cmp.opr" "LipsCnrUD_R_Mdv.i1x";
connectAttr "LipsCnrUD_R_Cmp.opg" "LipsCnrUD_R_Mdv.i1y";
connectAttr "LipsCnrUD_L_Cmp.opr" "LipsCnrUD_L_Mdv.i1x";
connectAttr "LipsCnrUD_L_Cmp.opg" "LipsCnrUD_L_Mdv.i1y";
connectAttr "LipsCnrIO_R_Cmp.opr" "LipsCnrIO_R_Mdv.i1x";
connectAttr "LipsCnrIO_R_Cmp.opg" "LipsCnrIO_R_Mdv.i1y";
connectAttr "LipsCnrIO_L_Cmp.opr" "LipsCnrIO_L_Mdv.i1x";
connectAttr "LipsCnrIO_L_Cmp.opg" "LipsCnrIO_L_Mdv.i1y";
connectAttr "LipsAllUprUD_Cmp.opr" "LipsAllUprUD_Mdv.i1x";
connectAttr "LipsAllUprUD_Cmp.opg" "LipsAllUprUD_Mdv.i1y";
connectAttr "LipsAllLwrUD_Cmp.opr" "LipsAllLwrUD_Mdv.i1x";
connectAttr "LipsAllLwrUD_Cmp.opg" "LipsAllLwrUD_Mdv.i1y";
connectAttr "FaceUprTz_Cmp.opr" "FaceUprTz_Mdv.i1x";
connectAttr "FaceUprTz_Cmp.opg" "FaceUprTz_Mdv.i1y";
connectAttr "FaceUprTy_Cmp.opr" "FaceUprTy_Mdv.i1x";
connectAttr "FaceUprTy_Cmp.opg" "FaceUprTy_Mdv.i1y";
connectAttr "FaceUprTx_Cmp.opr" "FaceUprTx_Mdv.i1x";
connectAttr "FaceUprTx_Cmp.opg" "FaceUprTx_Mdv.i1y";
connectAttr "FaceLwrTz_Cmp.opr" "FaceLwrTz_Mdv.i1x";
connectAttr "FaceLwrTz_Cmp.opg" "FaceLwrTz_Mdv.i1y";
connectAttr "FaceLwrTy_Cmp.opr" "FaceLwrTy_Mdv.i1x";
connectAttr "FaceLwrTy_Cmp.opg" "FaceLwrTy_Mdv.i1y";
connectAttr "FaceLwrTx_Cmp.opr" "FaceLwrTx_Mdv.i1x";
connectAttr "FaceLwrTx_Cmp.opg" "FaceLwrTx_Mdv.i1y";
connectAttr "EyebrowUD_R_Cmp.opr" "EyebrowUD_R_Mdv.i1x";
connectAttr "EyebrowUD_R_Cmp.opg" "EyebrowUD_R_Mdv.i1y";
connectAttr "EyebrowUD_L_Cmp.opr" "EyebrowUD_L_Mdv.i1x";
connectAttr "EyebrowUD_L_Cmp.opg" "EyebrowUD_L_Mdv.i1y";
connectAttr "EyebrowOutUD_R_Cmp.opr" "EyebrowOutUD_R_Mdv.i1x";
connectAttr "EyebrowOutUD_R_Cmp.opg" "EyebrowOutUD_R_Mdv.i1y";
connectAttr "EyebrowOutUD_L_Cmp.opr" "EyebrowOutUD_L_Mdv.i1x";
connectAttr "EyebrowOutUD_L_Cmp.opg" "EyebrowOutUD_L_Mdv.i1y";
connectAttr "EyebrowOutIO_R_Cmp.opr" "EyebrowOutIO_R_Mdv.i1x";
connectAttr "EyebrowOutIO_R_Cmp.opg" "EyebrowOutIO_R_Mdv.i1y";
connectAttr "EyebrowOutIO_L_Cmp.opr" "EyebrowOutIO_L_Mdv.i1x";
connectAttr "EyebrowOutIO_L_Cmp.opg" "EyebrowOutIO_L_Mdv.i1y";
connectAttr "EyebrowMidUD_R_Cmp.opr" "EyebrowMidUD_R_Mdv.i1x";
connectAttr "EyebrowMidUD_R_Cmp.opg" "EyebrowMidUD_R_Mdv.i1y";
connectAttr "EyebrowMidUD_L_Cmp.opr" "EyebrowMidUD_L_Mdv.i1x";
connectAttr "EyebrowMidUD_L_Cmp.opg" "EyebrowMidUD_L_Mdv.i1y";
connectAttr "EyebrowMidIO_R_Cmp.opr" "EyebrowMidIO_R_Mdv.i1x";
connectAttr "EyebrowMidIO_R_Cmp.opg" "EyebrowMidIO_R_Mdv.i1y";
connectAttr "EyebrowMidIO_L_Cmp.opr" "EyebrowMidIO_L_Mdv.i1x";
connectAttr "EyebrowMidIO_L_Cmp.opg" "EyebrowMidIO_L_Mdv.i1y";
connectAttr "EyebrowInUD_R_Cmp.opr" "EyebrowInUD_R_Mdv.i1x";
connectAttr "EyebrowInUD_R_Cmp.opg" "EyebrowInUD_R_Mdv.i1y";
connectAttr "EyebrowInUD_L_Cmp.opr" "EyebrowInUD_L_Mdv.i1x";
connectAttr "EyebrowInUD_L_Cmp.opg" "EyebrowInUD_L_Mdv.i1y";
connectAttr "EyebrowInIO_R_Cmp.opr" "EyebrowInIO_R_Mdv.i1x";
connectAttr "EyebrowInIO_R_Cmp.opg" "EyebrowInIO_R_Mdv.i1y";
connectAttr "EyebrowInIO_L_Cmp.opr" "EyebrowInIO_L_Mdv.i1x";
connectAttr "EyebrowInIO_L_Cmp.opg" "EyebrowInIO_L_Mdv.i1y";
connectAttr "EyebrowIO_R_Cmp.opr" "EyebrowIO_R_Mdv.i1x";
connectAttr "EyebrowIO_R_Cmp.opg" "EyebrowIO_R_Mdv.i1y";
connectAttr "EyebrowIO_L_Cmp.opr" "EyebrowIO_L_Mdv.i1x";
connectAttr "EyebrowIO_L_Cmp.opg" "EyebrowIO_L_Mdv.i1y";
connectAttr "EyebrowFC_R_Cmp.opg" "EyebrowFC_R_Mdv.i1x";
connectAttr "EyebrowFC_R_Cmp.opr" "EyebrowFC_R_Mdv.i1y";
connectAttr "EyebrowFC_L_Cmp.opg" "EyebrowFC_L_Mdv.i1x";
connectAttr "EyebrowFC_L_Cmp.opr" "EyebrowFC_L_Mdv.i1y";
connectAttr "EyeLidsUprTurnFC_R_Cmp.opr" "EyeLidsUprTurnFC_R_Mdv.i1x";
connectAttr "EyeLidsUprTurnFC_R_Cmp.opg" "EyeLidsUprTurnFC_R_Mdv.i1y";
connectAttr "EyeLidsUprTurnFC_C_Cmp.opr" "EyeLidsUprTurnFC_C_Mdv.i1x";
connectAttr "EyeLidsUprTurnFC_C_Cmp.opg" "EyeLidsUprTurnFC_C_Mdv.i1y";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsTurnFC" "EyeLidsUprTurnFC_C_Cmp.ipr";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsTurnFC" "EyeLidsUprTurnFC_C_Cmp.ipg";
connectAttr "EyeLidsUprOutUD_R_Cmp.opr" "EyeLidsUprOutUD_R_Mdv.i1x";
connectAttr "EyeLidsUprOutUD_R_Cmp.opg" "EyeLidsUprOutUD_R_Mdv.i1y";
connectAttr "EyeLidsUprOutUD_C_Cmp.opr" "EyeLidsUprOutUD_C_Mdv.i1x";
connectAttr "EyeLidsUprOutUD_C_Cmp.opg" "EyeLidsUprOutUD_C_Mdv.i1y";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsOutUD" "EyeLidsUprOutUD_C_Cmp.ipr";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsOutUD" "EyeLidsUprOutUD_C_Cmp.ipg";
connectAttr "EyeLidsUprMidUD_R_Cmp.opr" "EyeLidsUprMidUD_R_Mdv.i1x";
connectAttr "EyeLidsUprMidUD_R_Cmp.opg" "EyeLidsUprMidUD_R_Mdv.i1y";
connectAttr "EyeLidsUprMidUD_C_Cmp.opr" "EyeLidsUprMidUD_C_Mdv.i1x";
connectAttr "EyeLidsUprMidUD_C_Cmp.opg" "EyeLidsUprMidUD_C_Mdv.i1y";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsMidUD" "EyeLidsUprMidUD_C_Cmp.ipr";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsMidUD" "EyeLidsUprMidUD_C_Cmp.ipg";
connectAttr "EyeLidsUprInUD_R_Cmp.opr" "EyeLidsUprInUD_R_Mdv.i1x";
connectAttr "EyeLidsUprInUD_R_Cmp.opg" "EyeLidsUprInUD_R_Mdv.i1y";
connectAttr "EyeLidsUprInUD_C_Cmp.opr" "EyeLidsUprInUD_C_Mdv.i1x";
connectAttr "EyeLidsUprInUD_C_Cmp.opg" "EyeLidsUprInUD_C_Mdv.i1y";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsInUD" "EyeLidsUprInUD_C_Cmp.ipr";
connectAttr "EyeLidsUprBsh_C_Ctrl.lidsInUD" "EyeLidsUprInUD_C_Cmp.ipg";
connectAttr "EyeLidsLwrTurnFC_R_Cmp.opr" "EyeLidsLwrTurnFC_R_Mdv.i1x";
connectAttr "EyeLidsLwrTurnFC_R_Cmp.opg" "EyeLidsLwrTurnFC_R_Mdv.i1y";
connectAttr "EyeLidsLwrTurnFC_C_Cmp.opr" "EyeLidsLwrTurnFC_C_Mdv.i1x";
connectAttr "EyeLidsLwrTurnFC_C_Cmp.opg" "EyeLidsLwrTurnFC_C_Mdv.i1y";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsTurnFC" "EyeLidsLwrTurnFC_C_Cmp.ipr";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsTurnFC" "EyeLidsLwrTurnFC_C_Cmp.ipg";
connectAttr "EyeLidsLwrOutUD_R_Cmp.opr" "EyeLidsLwrOutUD_R_Mdv.i1x";
connectAttr "EyeLidsLwrOutUD_R_Cmp.opg" "EyeLidsLwrOutUD_R_Mdv.i1y";
connectAttr "EyeLidsLwrOutUD_C_Cmp.opr" "EyeLidsLwrOutUD_C_Mdv.i1x";
connectAttr "EyeLidsLwrOutUD_C_Cmp.opg" "EyeLidsLwrOutUD_C_Mdv.i1y";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsOutUD" "EyeLidsLwrOutUD_C_Cmp.ipr";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsOutUD" "EyeLidsLwrOutUD_C_Cmp.ipg";
connectAttr "EyeLidsLwrMidUD_R_Cmp.opr" "EyeLidsLwrMidUD_R_Mdv.i1x";
connectAttr "EyeLidsLwrMidUD_R_Cmp.opg" "EyeLidsLwrMidUD_R_Mdv.i1y";
connectAttr "EyeLidsLwrMidUD_C_Cmp.opr" "EyeLidsLwrMidUD_C_Mdv.i1x";
connectAttr "EyeLidsLwrMidUD_C_Cmp.opg" "EyeLidsLwrMidUD_C_Mdv.i1y";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsMidUD" "EyeLidsLwrMidUD_C_Cmp.ipr";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsMidUD" "EyeLidsLwrMidUD_C_Cmp.ipg";
connectAttr "EyeLidsLwrInUD_R_Cmp.opr" "EyeLidsLwrInUD_R_Mdv.i1x";
connectAttr "EyeLidsLwrInUD_R_Cmp.opg" "EyeLidsLwrInUD_R_Mdv.i1y";
connectAttr "EyeLidsLwrInUD_C_Cmp.opr" "EyeLidsLwrInUD_C_Mdv.i1x";
connectAttr "EyeLidsLwrInUD_C_Cmp.opg" "EyeLidsLwrInUD_C_Mdv.i1y";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsInUD" "EyeLidsLwrInUD_C_Cmp.ipr";
connectAttr "EyeLidsLwrBsh_C_Ctrl.lidsInUD" "EyeLidsLwrInUD_C_Cmp.ipg";
connectAttr "EyeBallUD_R_Cmp.opr" "EyeBallUD_R_Mdv.i1x";
connectAttr "EyeBallUD_R_Cmp.opg" "EyeBallUD_R_Mdv.i1y";
connectAttr "EyeBallUD_C_Cmp.opr" "EyeBallUD_C_Mdv.i1x";
connectAttr "EyeBallUD_C_Cmp.opg" "EyeBallUD_C_Mdv.i1y";
connectAttr "EyeLidsBsh_C_Ctrl.ty" "EyeBallUD_C_Cmp.ipr";
connectAttr "EyeLidsBsh_C_Ctrl.ty" "EyeBallUD_C_Cmp.ipg";
connectAttr "EyeBallFC_R_Cmp.opr" "EyeBallFC_R_Mdv.i1x";
connectAttr "EyeBallFC_R_Cmp.opg" "EyeBallFC_R_Mdv.i1y";
connectAttr "EyeBallFC_C_Cmp.opr" "EyeBallFC_C_Mdv.i1x";
connectAttr "EyeBallFC_C_Cmp.opg" "EyeBallFC_C_Mdv.i1y";
connectAttr "unitConversion199.o" "EyeBallFC_C_Cmp.ipr";
connectAttr "unitConversion200.o" "EyeBallFC_C_Cmp.ipg";
connectAttr "EyeLidsBsh_C_Ctrl.rz" "unitConversion200.i";
connectAttr "EyeLidsBsh_C_Ctrl.rz" "unitConversion199.i";
connectAttr "EyeLidsUprUp_R_Pma.o1" "EyeLidsUprAmpUD_R_Cmp.ipr";
connectAttr "EyeLidsUprDn_R_Pma.o1" "EyeLidsUprAmpUD_R_Cmp.ipg";
connectAttr "EyeLidsUprUD_R_Mdv.ox" "EyeLidsUprUp_R_Pma.i1[0]";
connectAttr "EyeLidsUprAuto_R_Mdv.ox" "EyeLidsUprUp_R_Pma.i1[1]";
connectAttr "EyeLidsFollowUprUD_R_Mdv.ox" "EyeLidsUprUp_R_Pma.i1[2]";
connectAttr "EyeLidsFollowUprUD_R_Cmp.opr" "EyeLidsFollowUprUD_R_Mdv.i1x";
connectAttr "EyeLidsFollowUprUD_R_Cmp.opg" "EyeLidsFollowUprUD_R_Mdv.i1y";
connectAttr "EyeLidsFollowUprUD_R_Bcl.opr" "EyeLidsFollowUprUD_R_Cmp.ipr";
connectAttr "EyeLidsFollowUprUD_R_Bcl.opg" "EyeLidsFollowUprUD_R_Cmp.ipg";
connectAttr "EyeLidsUprAuto_R_Cmp.opr" "EyeLidsUprAuto_R_Mdv.i1x";
connectAttr "EyeLidsUprAuto_R_Cmp.opg" "EyeLidsUprAuto_R_Mdv.i1y";
connectAttr "EyeLidsUprAuto_R_Bcl.opr" "EyeLidsUprAuto_R_Cmp.ipr";
connectAttr "EyeLidsUprAuto_R_Bcl.opg" "EyeLidsUprAuto_R_Cmp.ipg";
connectAttr "EyeLidsUprUD_R_Cmp.opr" "EyeLidsUprUD_R_Mdv.i1x";
connectAttr "EyeLidsUprUD_R_Cmp.opg" "EyeLidsUprUD_R_Mdv.i1y";
connectAttr "EyeLidsUprUD_R_Mdv.oy" "EyeLidsUprDn_R_Pma.i1[0]";
connectAttr "EyeLidsUprAuto_R_Mdv.oy" "EyeLidsUprDn_R_Pma.i1[1]";
connectAttr "EyeLidsFollowUprUD_R_Mdv.oy" "EyeLidsUprDn_R_Pma.i1[2]";
connectAttr "EyeLidsUprUp_C_Pma.o1" "EyeLidsUprAmpUD_C_Cmp.ipr";
connectAttr "EyeLidsUprDn_C_Pma.o1" "EyeLidsUprAmpUD_C_Cmp.ipg";
connectAttr "EyeLidsUprUD_C_Mdv.oy" "EyeLidsUprDn_C_Pma.i1[0]";
connectAttr "EyeLidsUprAuto_L_Mdv.oy" "EyeLidsUprDn_C_Pma.i1[1]";
connectAttr "EyeLidsFollowUprUD_C_Mdv.oy" "EyeLidsUprDn_C_Pma.i1[2]";
connectAttr "EyeLidsFollowUprUD_C_Cmp.opr" "EyeLidsFollowUprUD_C_Mdv.i1x";
connectAttr "EyeLidsFollowUprUD_C_Cmp.opg" "EyeLidsFollowUprUD_C_Mdv.i1y";
connectAttr "EyeLidsFollowUprUD_C_Bcl.opr" "EyeLidsFollowUprUD_C_Cmp.ipr";
connectAttr "EyeLidsFollowUprUD_C_Bcl.opg" "EyeLidsFollowUprUD_C_Cmp.ipg";
connectAttr "EyeLidsBsh_C_Jnt.eyelidsFollow" "EyeLidsFollowUprUD_C_Bcl.b";
connectAttr "unitConversion211.o" "EyeLidsFollowUprUD_C_Bcl.c1r";
connectAttr "unitConversion212.o" "EyeLidsFollowUprUD_C_Bcl.c1g";
connectAttr "EyeLidsBsh_C_Jnt.rx" "unitConversion211.i";
connectAttr "EyeLidsBsh_C_Jnt.rx" "unitConversion212.i";
connectAttr "EyeLidsUprUD_C_Cmp.opr" "EyeLidsUprUD_C_Mdv.i1x";
connectAttr "EyeLidsUprUD_C_Cmp.opg" "EyeLidsUprUD_C_Mdv.i1y";
connectAttr "EyeLidsUprBsh_C_Ctrl.ty" "EyeLidsUprUD_C_Cmp.ipr";
connectAttr "EyeLidsUprBsh_C_Ctrl.ty" "EyeLidsUprUD_C_Cmp.ipg";
connectAttr "EyeLidsUprAuto_L_Cmp.opr" "EyeLidsUprAuto_L_Mdv.i1x";
connectAttr "EyeLidsUprAuto_L_Cmp.opg" "EyeLidsUprAuto_L_Mdv.i1y";
connectAttr "EyeLidsUprAuto_L_Bcl.opr" "EyeLidsUprAuto_L_Cmp.ipr";
connectAttr "EyeLidsUprAuto_L_Bcl.opg" "EyeLidsUprAuto_L_Cmp.ipg";
connectAttr "EyeLidsUprUD_C_Mdv.ox" "EyeLidsUprUp_C_Pma.i1[0]";
connectAttr "EyeLidsUprAuto_L_Mdv.ox" "EyeLidsUprUp_C_Pma.i1[1]";
connectAttr "EyeLidsFollowUprUD_C_Mdv.ox" "EyeLidsUprUp_C_Pma.i1[2]";
connectAttr "FacialCtrlMover_EyeLidsLwrUp_R_Pma.o1" "EyeLidsLwrAllUD_R_Cmp.ipr";
connectAttr "FacialCtrlMover_EyeLidsLwrDn_R_Pma.o1" "EyeLidsLwrAllUD_R_Cmp.ipg";
connectAttr "FacialCtrlMover_EyeLidsLwrUp_C_Pma.o1" "EyeLidsLwrAllUD_C_Cmp.ipr";
connectAttr "FacialCtrlMover_EyeLidsLwrDn_C_Pma.o1" "EyeLidsLwrAllUD_C_Cmp.ipg";
connectAttr "EyeBallIn_R_Pma.o1" "EyeBallAmpIO_R_Cmp.ipr";
connectAttr "EyeBallOut_R_Pma.o1" "EyeBallAmpIO_R_Cmp.ipg";
connectAttr "EyeBallFollowIO_R_Mdv.ox" "EyeBallIn_R_Pma.i1[0]";
connectAttr "EyeBallIO_R_Mdv.ox" "EyeBallIn_R_Pma.i1[1]";
connectAttr "EyeBallIO_R_Cmp.opr" "EyeBallIO_R_Mdv.i1x";
connectAttr "EyeBallIO_R_Cmp.opg" "EyeBallIO_R_Mdv.i1y";
connectAttr "EyeBallFollowIO_R_Cmp.opr" "EyeBallFollowIO_R_Mdv.i1x";
connectAttr "EyeBallFollowIO_R_Cmp.opg" "EyeBallFollowIO_R_Mdv.i1y";
connectAttr "EyeBallFollowIO_R_Bcl.opr" "EyeBallFollowIO_R_Cmp.ipr";
connectAttr "EyeBallFollowIO_R_Bcl.opg" "EyeBallFollowIO_R_Cmp.ipg";
connectAttr "EyeBallFollowIO_R_Mdv.oy" "EyeBallOut_R_Pma.i1[0]";
connectAttr "EyeBallIO_R_Mdv.oy" "EyeBallOut_R_Pma.i1[1]";
connectAttr "EyeBallIn_C_Pma.o1" "EyeBallAmpIO_C_Cmp.ipr";
connectAttr "EyeBallOut_C_Pma.o1" "EyeBallAmpIO_C_Cmp.ipg";
connectAttr "EyeBallIO_C_Mdv.ox" "EyeBallIn_C_Pma.i1[0]";
connectAttr "EyeBallFollowIO_C_Mdv.ox" "EyeBallIn_C_Pma.i1[1]";
connectAttr "EyeBallFollowIO_C_Cmp.opr" "EyeBallFollowIO_C_Mdv.i1x";
connectAttr "EyeBallFollowIO_C_Cmp.opg" "EyeBallFollowIO_C_Mdv.i1y";
connectAttr "EyeBallFollowIO_C_Bcl.opr" "EyeBallFollowIO_C_Cmp.ipr";
connectAttr "EyeBallFollowIO_C_Bcl.opg" "EyeBallFollowIO_C_Cmp.ipg";
connectAttr "unitConversion215.o" "EyeBallFollowIO_C_Bcl.c1r";
connectAttr "unitConversion216.o" "EyeBallFollowIO_C_Bcl.c1g";
connectAttr "EyeLidsBsh_C_Jnt.eyelidsFollow" "EyeBallFollowIO_C_Bcl.b";
connectAttr "EyeLidsBsh_C_Jnt.ry" "unitConversion215.i";
connectAttr "EyeLidsBsh_C_Jnt.ry" "unitConversion216.i";
connectAttr "EyeBallIO_C_Cmp.opr" "EyeBallIO_C_Mdv.i1x";
connectAttr "EyeBallIO_C_Cmp.opg" "EyeBallIO_C_Mdv.i1y";
connectAttr "EyeLidsBsh_C_Ctrl.tx" "EyeBallIO_C_Cmp.ipr";
connectAttr "EyeLidsBsh_C_Ctrl.tx" "EyeBallIO_C_Cmp.ipg";
connectAttr "EyeBallIO_C_Mdv.oy" "EyeBallOut_C_Pma.i1[0]";
connectAttr "EyeBallFollowIO_C_Mdv.oy" "EyeBallOut_C_Pma.i1[1]";
connectAttr "EyeBallUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[0].dn";
connectAttr "EyeBallFollowIO_C_Bcl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "LipsAllUprUD_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "EyebrowOutUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "FaceLwrTz_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[4].dn";
connectAttr "FacialCtrlMover_EyeLidsLwrDn_C_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[5].dn"
		;
connectAttr "MouthClench_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[6].dn";
connectAttr "EyeBallFollowIO_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[7].dn"
		;
connectAttr "unitConversion199.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[8].dn"
		;
connectAttr "EyeBallFollowIO_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[9].dn"
		;
connectAttr "EyebrowInTurnC_R_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[10].dn"
		;
connectAttr "EyeLidsFollowUprUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[11].dn"
		;
connectAttr "EyebrowSquintOut_R_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[12].dn"
		;
connectAttr "EyeLidsUprInUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[13].dn"
		;
connectAttr "EyebrowMidTurnC_R_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[14].dn"
		;
connectAttr "EyeLidsUprUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[15].dn"
		;
connectAttr "EyeLidsUprAuto_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[16].dn"
		;
connectAttr "PuffAttr_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[17].dn";
connectAttr "EyeBallIO_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[18].dn"
		;
connectAttr "LipsMidUprAttrUD_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[19].dn"
		;
connectAttr "EyeLidsUprDn_R_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[20].dn"
		;
connectAttr "EyeLidsLwrOutUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[21].dn"
		;
connectAttr "EyebrowOutIO_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[22].dn"
		;
connectAttr "EyeLidsLwrOutUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[23].dn"
		;
connectAttr "PuffAttrIO_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[24].dn"
		;
connectAttr "PuffAttrIO_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[25].dn"
		;
connectAttr "EyeLidsUprAuto_R_Bcl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[26].dn"
		;
connectAttr "MouthPull_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[27].dn";
connectAttr "EyeLidsLwrMidUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[28].dn"
		;
connectAttr "EyebrowUD_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[29].dn"
		;
connectAttr "LipsCnrIO_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[30].dn"
		;
connectAttr "EyebrowMidIO_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[31].dn"
		;
connectAttr "EyeLidsFollowUprUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[32].dn"
		;
connectAttr "EyebrowOutIO_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[33].dn"
		;
connectAttr "EyeLidsUprMidUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[34].dn"
		;
connectAttr "MouthFC_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[35].dn";
connectAttr "PuffIn_R_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[36].dn";
connectAttr "EyeLidsFollowUprUD_R_Bcl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[37].dn"
		;
connectAttr "FaceUprTy_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[38].dn";
connectAttr "NoseFC_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[39].dn";
connectAttr "EyebrowUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[40].dn"
		;
connectAttr "EyeBallIn_C_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[41].dn"
		;
connectAttr "EyebrowInIO_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[42].dn"
		;
connectAttr "EyeLidsUprOutUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[43].dn"
		;
connectAttr "EyeLidsUprOutUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[44].dn"
		;
connectAttr "EyeLidsLwrTurnFC_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[45].dn"
		;
connectAttr "Cheek_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[46].dn";
connectAttr "Cheek_L_Bcl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[47].dn";
connectAttr "MouthPull_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[48].dn";
connectAttr "EyeLidsLwrAllUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[49].dn"
		;
connectAttr "EyeLidsLwrMidUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[50].dn"
		;
connectAttr "EyeLidsUprAuto_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[51].dn"
		;
connectAttr "MouthU_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[52].dn";
connectAttr "FacialCtrlMover_EyeLidsLwrUp_C_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[53].dn"
		;
connectAttr "NoseUD_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[54].dn";
connectAttr "EyeBallFC_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[55].dn"
		;
connectAttr "unitConversion212.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[56].dn"
		;
connectAttr "LipsAllUprUD_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[57].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[58].dn"
		;
connectAttr "EyeLidsUprUp_R_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[59].dn"
		;
connectAttr "EyebrowInTurnC_L_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[60].dn"
		;
connectAttr "LipsAllLwrUD_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[61].dn"
		;
connectAttr "EyeLidsLwrInUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[62].dn"
		;
connectAttr "LipsPartIO_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[63].dn"
		;
connectAttr "EyeLidsLwrInUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[64].dn"
		;
connectAttr "EyeLidsUprUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[65].dn"
		;
connectAttr "EyebrowMidUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[66].dn"
		;
connectAttr "EyebrowInUD_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[67].dn"
		;
connectAttr "EyeLidsUprMidUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[68].dn"
		;
connectAttr "EyeBallFC_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[69].dn"
		;
connectAttr "EyebrowOutTurnC_L_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[70].dn"
		;
connectAttr "LipsLftLwrAttrUD_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[71].dn"
		;
connectAttr "EyeLidsUprTurnFC_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[72].dn"
		;
connectAttr "EyebrowInTurnF_L_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[73].dn"
		;
connectAttr "EyeBallAmpIO_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[74].dn"
		;
connectAttr "LipsPartIO_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[75].dn"
		;
connectAttr "EyeLidsUprUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[76].dn"
		;
connectAttr "EyeLidsUprMidUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[77].dn"
		;
connectAttr "EyebrowIO_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[78].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[79].dn"
		;
connectAttr "EyebrowInIO_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[80].dn"
		;
connectAttr "EyeLidsLwrMidUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[81].dn"
		;
connectAttr "PuffAttr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[82].dn";
connectAttr "EyebrowInUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[83].dn"
		;
connectAttr "EyeLidsUprInUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[84].dn"
		;
connectAttr "EyebrowOutTurnF_R_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[85].dn"
		;
connectAttr "FaceUprTx_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[86].dn";
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_R_Bcl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[87].dn"
		;
connectAttr "EyebrowMidTurnF_L_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[88].dn"
		;
connectAttr "MouthFC_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[89].dn";
connectAttr "LipsRgtUprAttrUD_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[90].dn"
		;
connectAttr "EyeBallFC_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[91].dn"
		;
connectAttr "EyebrowMidIO_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[92].dn"
		;
connectAttr "NoseTwist_R_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[93].dn"
		;
connectAttr "FaceUprTy_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[94].dn";
connectAttr "Cheek_R_Bcl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[95].dn";
connectAttr "FacialCtrlMover_EyeLidsLwrUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[96].dn"
		;
connectAttr "LipsLftUprAttrUD_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[97].dn"
		;
connectAttr "unitConversion211.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[98].dn"
		;
connectAttr "EyeBallIO_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[99].dn"
		;
connectAttr "EyebrowInTurnF_R_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[100].dn"
		;
connectAttr "EyebrowInIO_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[101].dn"
		;
connectAttr "LipsCnrIO_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[102].dn"
		;
connectAttr "EyebrowOutTurnC_R_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[103].dn"
		;
connectAttr "EyebrowMidIO_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[104].dn"
		;
connectAttr "EyeLidsLwrOutUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[105].dn"
		;
connectAttr "NoseUD_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[106].dn";
connectAttr "EyeLidsLwrMidUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[107].dn"
		;
connectAttr "PuffOut_R_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[108].dn";
connectAttr "EyeBallOut_R_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[109].dn"
		;
connectAttr "MouthClench_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[110].dn"
		;
connectAttr "PuffOut_L_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[111].dn";
connectAttr "LipsCnrUD_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[112].dn"
		;
connectAttr "NoseUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[113].dn";
connectAttr "EyeBallIO_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[114].dn"
		;
connectAttr "EyebrowMidTurnC_L_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[115].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrDn_R_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[116].dn"
		;
connectAttr "CheekUp_L_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[117].dn";
connectAttr "PuffAttr_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[118].dn"
		;
connectAttr "LipsAllLwrUD_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[119].dn"
		;
connectAttr "EyeLidsUprTurnFC_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[120].dn"
		;
connectAttr "LipsRgtLwrAttrUD_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[121].dn"
		;
connectAttr "MouthUD_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[122].dn";
connectAttr "LipsCnrIO_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[123].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[124].dn"
		;
connectAttr "LipsLftUprAttrUD_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[125].dn"
		;
connectAttr "unitConversion200.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[126].dn"
		;
connectAttr "EyebrowOutIO_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[127].dn"
		;
connectAttr "EyebrowUD_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[128].dn"
		;
connectAttr "Puff_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[129].dn";
connectAttr "Puff_L_Bcl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[130].dn";
connectAttr "EyeBallAmpIO_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[131].dn"
		;
connectAttr "FaceLwrTz_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[132].dn";
connectAttr "FacialCtrlMover_EyeLidsLwrUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[133].dn"
		;
connectAttr "EyeLidsUprAmpUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[134].dn"
		;
connectAttr "EyeLidsUprInUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[135].dn"
		;
connectAttr "LipsMidUprAttrUD_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[136].dn"
		;
connectAttr "NoseStSq_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[137].dn";
connectAttr "EyeLidsUprUp_C_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[138].dn"
		;
connectAttr "EyeLidsUprInUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[139].dn"
		;
connectAttr "EyebrowInIO_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[140].dn"
		;
connectAttr "LipsUprCurlIO_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[141].dn"
		;
connectAttr "EyebrowFC_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[142].dn"
		;
connectAttr "LipsRgtLwrAttrUD_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[143].dn"
		;
connectAttr "PuffAttr_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[144].dn"
		;
connectAttr "NoseLR_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[145].dn";
connectAttr "EyeLidsUprAuto_L_Bcl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[146].dn"
		;
connectAttr "EyebrowOutUD_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[147].dn"
		;
connectAttr "LipsLwrCurlIO_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[148].dn"
		;
connectAttr "FaceLwrTx_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[149].dn";
connectAttr "Puff_R_Bcl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[150].dn";
connectAttr "EyebrowMidIO_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[151].dn"
		;
connectAttr "Puff_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[152].dn";
connectAttr "EyeLidsFollowUprUD_C_Bcl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[153].dn"
		;
connectAttr "EyebrowFC_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[154].dn"
		;
connectAttr "LipsMidLwrAttrUD_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[155].dn"
		;
connectAttr "EyebrowOutTurnF_L_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[156].dn"
		;
connectAttr "EyeLidsLwrInUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[157].dn"
		;
connectAttr "EyeBallFC_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[158].dn"
		;
connectAttr "EyebrowOutUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[159].dn"
		;
connectAttr "EyeBallUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[160].dn"
		;
connectAttr "EyeLidsFollowUprUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[161].dn"
		;
connectAttr "PuffIn_L_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[162].dn";
connectAttr "EyeLidsLwrTurnFC_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[163].dn"
		;
connectAttr "LipsPartIO_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[164].dn"
		;
connectAttr "EyeLidsUprOutUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[165].dn"
		;
connectAttr "unitConversion215.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[166].dn"
		;
connectAttr "LipsLwrCurlIO_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[167].dn"
		;
connectAttr "EyebrowFC_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[168].dn"
		;
connectAttr "Puff_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[169].dn";
connectAttr "EyeLidsLwrOutUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[170].dn"
		;
connectAttr "FaceLwrTy_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[171].dn";
connectAttr "MouthU_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[172].dn";
connectAttr "CheekUp_R_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[173].dn";
connectAttr "FaceUprTx_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[174].dn";
connectAttr "PuffAttrIO_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[175].dn"
		;
connectAttr "NoseUD_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[176].dn";
connectAttr "EyeLidsUprDn_C_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[177].dn"
		;
connectAttr "CheekAttr_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[178].dn"
		;
connectAttr "EyebrowMidUD_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[179].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[180].dn"
		;
connectAttr "EyebrowIO_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[181].dn"
		;
connectAttr "Puff_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[182].dn";
connectAttr "LipsMidLwrAttrUD_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[183].dn"
		;
connectAttr "EyeBallOut_C_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[184].dn"
		;
connectAttr "EyeLidsUprOutUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[185].dn"
		;
connectAttr "LipsCnrUD_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[186].dn"
		;
connectAttr "unitConversion216.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[187].dn"
		;
connectAttr "EyebrowPull_R_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[188].dn"
		;
connectAttr "MouthUD_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[189].dn";
connectAttr "EyebrowIO_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[190].dn"
		;
connectAttr "EyebrowInUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[191].dn"
		;
connectAttr "EyeLidsUprTurnFC_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[192].dn"
		;
connectAttr "NoseSnarl_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[193].dn";
connectAttr "NoseLR_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[194].dn";
connectAttr "Cheek_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[195].dn";
connectAttr "EyebrowPull_L_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[196].dn"
		;
connectAttr "EyebrowFC_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[197].dn"
		;
connectAttr "EyeBallUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[198].dn"
		;
connectAttr "LipsCnrIO_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[199].dn"
		;
connectAttr "NoseFC_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[200].dn";
connectAttr "LipsPartIO_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[201].dn"
		;
connectAttr "EyebrowSquintIn_R_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[202].dn"
		;
connectAttr "EyeLidsUprAmpUD_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[203].dn"
		;
connectAttr "EyeLidsLwrAllUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[204].dn"
		;
connectAttr "EyeBallIO_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[205].dn"
		;
connectAttr "EyeLidsUprTurnFC_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[206].dn"
		;
connectAttr "LipsCnrUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[207].dn"
		;
connectAttr "EyebrowInUD_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[208].dn"
		;
connectAttr "FaceUprTz_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[209].dn";
connectAttr "LipsRgtUprAttrUD_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[210].dn"
		;
connectAttr "CheekAttr_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[211].dn"
		;
connectAttr "EyeLidsLwrTurnFC_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[212].dn"
		;
connectAttr "EyeBallIn_R_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[213].dn"
		;
connectAttr "Cheek_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[214].dn";
connectAttr "EyebrowMidTurnF_R_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[215].dn"
		;
connectAttr "MouthLR_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[216].dn";
connectAttr "CheekAttr_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[217].dn"
		;
connectAttr "FaceLwrTy_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[218].dn";
connectAttr "FacialCtrlMover_EyeLidsLwrUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[219].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[220].dn"
		;
connectAttr "EyeLidsFollowUprUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[221].dn"
		;
connectAttr "NoseStSq_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[222].dn";
connectAttr "EyeBallFollowIO_C_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[223].dn"
		;
connectAttr "EyebrowPullIO_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[224].dn"
		;
connectAttr "EyeLidsUprUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[225].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_L_Bcl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[226].dn"
		;
connectAttr "EyeBallUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[227].dn"
		;
connectAttr "FaceUprTz_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[228].dn";
connectAttr "CheekDn_L_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[229].dn";
connectAttr "EyebrowOutUD_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[230].dn"
		;
connectAttr "EyeLidsUprMidUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[231].dn"
		;
connectAttr "EyeBallFollowIO_R_Bcl.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[232].dn"
		;
connectAttr "LipsCnrUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[233].dn"
		;
connectAttr "NoseUD_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[234].dn";
connectAttr "EyeLidsUprAuto_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[235].dn"
		;
connectAttr "NoseFC_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[236].dn";
connectAttr "PuffAttrIO_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[237].dn"
		;
connectAttr "EyeBallFollowIO_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[238].dn"
		;
connectAttr "NoseTwist_L_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[239].dn"
		;
connectAttr "EyebrowOutIO_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[240].dn"
		;
connectAttr "NoseUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[241].dn";
connectAttr "FaceLwrTx_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[242].dn";
connectAttr "LipsLftLwrAttrUD_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[243].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUp_R_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[244].dn"
		;
connectAttr "EyebrowSquintOut_L_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[245].dn"
		;
connectAttr "EyeLidsUprAuto_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[246].dn"
		;
connectAttr "EyeLidsLwrInUD_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[247].dn"
		;
connectAttr "CheekDn_R_Pma.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[248].dn";
connectAttr "EyeLidsLwrTurnFC_C_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[249].dn"
		;
connectAttr "CheekAttr_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[250].dn"
		;
connectAttr "EyebrowUD_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[251].dn"
		;
connectAttr "EyebrowIO_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[252].dn"
		;
connectAttr "NoseFC_R_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[253].dn";
connectAttr "Cheek_L_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[254].dn";
connectAttr "EyebrowMidUD_R_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[255].dn"
		;
connectAttr "EyebrowSquintIn_L_Rem.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[256].dn"
		;
connectAttr "LipsUprCurlIO_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[257].dn"
		;
connectAttr "MouthLR_Mdv.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[258].dn";
connectAttr "EyebrowMidUD_L_Cmp.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[259].dn"
		;
connectAttr "EyebrowInTurnC_L_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrDn_C_Pma.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "NoseSnarl_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "unitConversion199.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "EyebrowMidTurnC_R_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[4].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUp_C_Pma.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[5].dn"
		;
connectAttr "EyebrowInTurnF_R_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[6].dn"
		;
connectAttr "unitConversion200.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[7].dn"
		;
connectAttr "defaultRedshiftPostEffects.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[8].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUp_R_Pma.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[9].dn"
		;
connectAttr "EyebrowOutTurnC_L_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[10].dn"
		;
connectAttr "EyebrowOutTurnC_R_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[11].dn"
		;
connectAttr "EyebrowInTurnC_R_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[12].dn"
		;
connectAttr "EyebrowMidTurnF_R_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[13].dn"
		;
connectAttr "EyebrowSquintOut_L_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[14].dn"
		;
connectAttr "EyebrowMidTurnC_L_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[15].dn"
		;
connectAttr "EyebrowSquintIn_R_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[16].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_L_Bcl.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[17].dn"
		;
connectAttr "EyebrowOutTurnF_R_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[18].dn"
		;
connectAttr "NoseTwist_L_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[19].dn"
		;
connectAttr "EyeLidsUprAuto_R_Bcl.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[20].dn"
		;
connectAttr "unitConversion211.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[21].dn"
		;
connectAttr "EyebrowOutTurnF_L_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[22].dn"
		;
connectAttr "EyebrowSquintOut_R_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[23].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrDn_R_Pma.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[24].dn"
		;
connectAttr "EyebrowInTurnF_L_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[25].dn"
		;
connectAttr "unitConversion216.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[26].dn"
		;
connectAttr "unitConversion212.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[27].dn"
		;
connectAttr "NoseTwist_R_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[28].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_R_Bcl.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[29].dn"
		;
connectAttr "unitConversion215.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[30].dn"
		;
connectAttr "EyebrowPull_L_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[31].dn"
		;
connectAttr "EyebrowSquintIn_L_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[32].dn"
		;
connectAttr "EyebrowMidTurnF_L_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[33].dn"
		;
connectAttr "EyeLidsUprAuto_L_Bcl.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[34].dn"
		;
connectAttr "EyebrowPull_R_Rem.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[35].dn"
		;
connectAttr "FacialCtrlMover_EyeLidsLwrUp_C_Pma.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "FacialCtrlMover_EyeLidsLwrDn_C_Pma.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_L_Bcl.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "FacialCtrlMover_EyeLidsLwrDn_R_Pma.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "FacialCtrlMover_EyeLidsLwrUp_R_Pma.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "FacialCtrlMover_EyeLidsLwrAuto_R_Bcl.msg" ":defaultRenderUtilityList1.u"
		 -na;
connectAttr "EyeLidsUprAuto_L_Bcl.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyeLidsUprAuto_R_Bcl.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowInTurnF_L_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowInTurnC_L_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowMidTurnC_L_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowMidTurnF_L_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowOutTurnF_L_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowOutTurnC_L_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowInTurnF_R_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowInTurnC_R_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowMidTurnC_R_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowMidTurnF_R_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowOutTurnF_R_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowOutTurnC_R_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowPull_L_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowPull_R_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "NoseSnarl_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "NoseTwist_L_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "NoseTwist_R_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowSquintIn_L_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowSquintOut_L_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowSquintIn_R_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "EyebrowSquintOut_R_Rem.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRedshiftPostEffects.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "EyeLidsUprAmpUD_C_Cmp.op" ":internal_standInShader.ic";
// End of EyeCtrl.ma
