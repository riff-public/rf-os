import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  HEAD SQUASH STRETCH RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class HeadDef( object ):

    def __init__( self , tipTmpJnt  = 'HeadTip_TmpJnt' ,
                         baseTmpJnt = 'HeadBase_TmpJnt' ,
                         elem       = 'upr'
                 ):

        ##-- Info
        elem = elem.capitalize()

        #-- Create Main Group
        self.headDefCtrlGrp = rt.createNode( 'transform' , 'Head%sDefCtrl_Grp' %elem )
        self.headDefSkinGrp = rt.createNode( 'transform' , 'Head%sDefSkin_Grp' %elem )
        self.headDefStillGrp = rt.createNode( 'transform' , 'Head%sDefStill_Grp' %elem )
        
        #-- Create Joint
        self.tipJnt = rt.createJnt( 'Head%sTip_Jnt' %elem , tipTmpJnt )
        self.tipJntZro = rt.addGrp( self.tipJnt )
        self.tipJntOfst = rt.addGrp( self.tipJnt , 'Ofst' )

        self.midJnt = rt.createJnt( 'Head%sMid_Jnt' %elem )
        self.midJntZro = rt.addGrp( self.midJnt )
        self.midJntOfst = rt.addGrp( self.midJnt , 'Ofst' )

        self.baseJnt = rt.createJnt( 'Head%sBase_Jnt' %elem , baseTmpJnt )
        self.baseJntZro = rt.addGrp( self.baseJnt )
        self.baseJntOfst = rt.addGrp( self.baseJnt , 'Ofst' )

        mc.delete( mc.pointConstraint ( self.tipJnt , self.baseJnt , self.midJntZro , mo = False ))

        #-- Create Controls
        self.headCtrl = rt.createCtrl( 'Head%s_Ctrl' %elem , 'cube' , 'green' )
        self.headZro = rt.addGrp( self.headCtrl )

        self.headZro.snap( self.tipJnt )

        #-- Create Aim UpVector Group
        self.midUpVecZro = rt.createNode( 'transform' , 'Head%sMidUpVecZro_Grp' %elem )
        self.midUpVecGrp = rt.createNode( 'transform' , 'Head%sMidUpVec_Grp' %elem )
        self.midAimGrp = rt.createNode( 'transform' , 'Head%sMidAim_Grp' %elem )
        self.midAimGrp.parent( self.midUpVecGrp )
        self.midUpVecGrp.parent( self.midUpVecZro )
        self.midUpVecZro.snap( self.midJnt )

        #-- Rig process
        mc.pointConstraint( self.tipJnt , self.baseJnt , self.midJntOfst , mo = True )
        mc.pointConstraint( self.tipJnt , self.baseJnt , self.midUpVecGrp , mo = True )
        mc.orientConstraint( self.midAimGrp , self.midJntOfst , mo = False )
        mc.aimConstraint( self.baseJnt , self.midAimGrp , aim = (0,-1,0) , u = (0,0,1) , wut='objectrotation' , wuo = self.midUpVecGrp , wu= (0,0,1) , mo = True )
        
        self.headCtrl.attr('tx') >> self.tipJntOfst.attr('tx')
        self.headCtrl.attr('ty') >> self.tipJntOfst.attr('ty')
        self.headCtrl.attr('tz') >> self.tipJntOfst.attr('tz')

        self.tipDistGrp = rt.createNode( 'transform' , 'Head%sTipDist_Grp' %elem )
        self.baseDistGrp = rt.createNode( 'transform' , 'Head%sBaseDist_Grp' %elem )
        mc.parentConstraint( self.tipJnt , self.tipDistGrp , mo = False )
        mc.parentConstraint( self.baseJnt , self.baseDistGrp , mo = False )

        self.tipDefaultDistGrp = rt.createNode( 'transform' , 'Head%sTipDefaultDist_Grp' %elem )
        self.baseDefaultDistGrp = rt.createNode( 'transform' , 'Head%sBaseDefaultDist_Grp' %elem )
        self.tipDefaultDistGrp.snap( self.tipJnt )
        self.baseDefaultDistGrp.snap( self.baseJnt )

        self.tipDist = rt.createNode( 'distanceBetween' , 'Head%sTipDistance_Dist' %elem )
        self.tipDefaultDist = rt.createNode( 'distanceBetween' , 'Head%sTipDefaultDistance_Dist' %elem )
        self.tipNrmDivMdv = rt.createNode( 'multiplyDivide' , 'Head%sTipSqStNormDiv_Mdv' %elem )
        self.tipPowMdv = rt.createNode( 'multiplyDivide' , 'Head%sTipSqStPow_Mdv' %elem )
        self.tipDivMdv = rt.createNode( 'multiplyDivide' , 'Head%sTipSqStDiv_Mdv' %elem )
        self.tipPma = rt.createNode( 'plusMinusAverage' , 'Head%sTipSqSt_Pma' %elem )

        self.tipDistGrp.attr('t') >> self.tipDist.attr('point1')
        self.baseDistGrp.attr('t') >> self.tipDist.attr('point2')
        self.tipDefaultDistGrp.attr('t') >> self.tipDefaultDist.attr('point1')
        self.baseDefaultDistGrp.attr('t') >> self.tipDefaultDist.attr('point2')
        self.tipDist.attr('distance') >> self.tipNrmDivMdv.attr('i1x')
        self.tipDefaultDist.attr('distance') >> self.tipNrmDivMdv.attr('i2x')
        self.tipNrmDivMdv.attr('ox') >> self.tipPowMdv.attr('i1x')
        self.tipPowMdv.attr('ox') >> self.tipDivMdv.attr('i2x')
        self.tipDivMdv.attr('ox') >> self.tipPma.attr('i1[0]')
        self.tipPma.attr('o1') >> self.midJnt.attr('sx')
        self.tipPma.attr('o1') >> self.midJnt.attr('sz')

        self.tipNrmDivMdv.attr('op').value = 2
        self.tipPowMdv.attr('op').value = 3
        self.tipDivMdv.attr('op').value = 2
        self.tipDivMdv.attr('i1x').value = 1
        self.tipPowMdv.attr('i2x').value = 2
        
        #-- Adjust Hierarchy
        mc.parent( self.headZro , self.headDefCtrlGrp )
        mc.parent( self.tipJntZro , self.midJntZro , self.baseJntZro , self.headDefSkinGrp )
        mc.parent( self.tipDefaultDistGrp , self.baseDefaultDistGrp , self.tipDistGrp , self.baseDistGrp , self.midUpVecZro , self.headDefStillGrp )

        #-- Cleanup
        for obj in ( self.tipJntZro , self.tipJntOfst , self.midJntZro , self.midJntOfst , 
                     self.baseJntZro , self.baseJntOfst , self.headZro , self.midUpVecZro , self.midAimGrp ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        self.headCtrl.lockHideAttrs( 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        for obj in ( self.tipJntZro , self.baseJntZro ) :
            obj.attr('v').value = 0

        mc.select( cl = True )

class Run( HeadDef ) :

    def __init__( self , headTmpJnt       = 'Head_TmpJnt' ,
    					 uprBaseTmpJnt    = 'HeadUprBase_TmpJnt' , 
                         uprTipTmpJnt     = 'HeadUprTip_TmpJnt' , 
                         lwrBaseTmpJnt    = 'HeadLwrBase_TmpJnt' , 
                         lwrTipTmpJnt     = 'HeadLwrTip_TmpJnt' , 
                         midTmpJnt        = 'HeadMid_TmpJnt' ):

        #-- Create Main Group
        self.defCtrlGrp = rt.createNode( 'transform' , 'HeadDefCtrl_Grp' )
        self.defSkinGrp = rt.createNode( 'transform' , 'HeadDefSkin_Grp' )
        self.defStillGrp = rt.createNode( 'transform' , 'HeadDefStill_Grp' )

        #-- Rig process
        self.uprDef = HeadDef(    
                                    tipTmpJnt  = uprTipTmpJnt ,
                                    baseTmpJnt = uprBaseTmpJnt ,
                                    elem       = 'upr' ,
                               )

        self.lwrDef = HeadDef(    
                                    tipTmpJnt  = lwrTipTmpJnt ,
                                    baseTmpJnt = lwrBaseTmpJnt ,
                                    elem       = 'lwr' ,
                               )
        
        #-- Create Joint
        self.headJnt = rt.createJnt( 'HeadShare_Jnt' , headTmpJnt )
        self.headJntZro = rt.addGrp( self.headJnt )

        self.midJnt = rt.createJnt( 'HeadMid_Jnt' , midTmpJnt )
        self.midJntZro = rt.addGrp( self.midJnt )
        self.midJntOfst = rt.addGrp( self.midJnt , 'Ofst' )

        #-- Create Controls
        self.midCtrl = rt.createCtrl( 'HeadMid_Ctrl' , 'square' , 'green' )
        self.midZro = rt.addGrp( self.midCtrl )

        self.midZro.snap( self.midJnt )

        #-- Rig process
        self.midCtrl.attr('tx') >> self.midJntOfst.attr('tx')
        self.midCtrl.attr('ty') >> self.midJntOfst.attr('ty')
        self.midCtrl.attr('tz') >> self.midJntOfst.attr('tz')

        mc.parentConstraint( self.midJnt , self.uprDef.baseJnt , mo = True )
        mc.parentConstraint( self.midJnt , self.lwrDef.baseJnt , mo = True )

        #-- Adjust Hierarchy

        mc.parent( self.uprDef.headDefCtrlGrp , self.lwrDef.headDefCtrlGrp , self.midZro , self.defCtrlGrp )
        mc.parent( self.headJntZro , self.uprDef.headDefSkinGrp , self.lwrDef.headDefSkinGrp , self.midJntZro , self.defSkinGrp )
        mc.parent( self.uprDef.headDefStillGrp , self.lwrDef.headDefStillGrp , self.defStillGrp )

        #-- Cleanup
        for obj in ( self.defCtrlGrp , self.defSkinGrp , self.defStillGrp , self.midZro ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        self.midCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )