import maya.cmds as mc
import maya.mel as mm

def move_to_origin():
    
    ofstList = []
    animList = []
    consList = []
    parsList = []
    
    sels = mc.ls( sl = True )
    
    minTime = mc.playbackOptions( q = True , min = True )
    maxTime = mc.playbackOptions( q = True , max = True )  
    
    i = 1
    
    for sel in sels :
        
        if not mc.objExists( 'Origin_Grp' ) :
            mc.createNode( 'transform' , n = 'Origin_Grp' )
            mc.delete(mc.parentConstraint( sels , 'Origin_Grp' , mo = False ))
        
        ofst = mc.createNode( 'transform' , n = 'Offset%s_Grp' %i )
        anim = mc.createNode( 'transform' , n = 'Anim%s_Grp' %i )
        mc.parent( anim , ofst )
        mc.delete(mc.parentConstraint( sel , ofst , mo = False ))
        cons = mc.parentConstraint( sel , anim , mo = False )[0]
        mc.parent( ofst , 'Origin_Grp' )
        
        pars = mc.listRelatives( sel , c = True , type = 'constraint' )
        
        ofstList.append( ofst )
        animList.append( anim )
        consList.append( cons )

        if pars :
        	parsList.append( pars[0] )
        
        
        i+=1
        
    mc.bakeResults( animList , sels , simulation = True , t = ( minTime , maxTime ))
    
    for j in range(len(sels)) :
        mc.parentConstraint( animList[j] , sels[j] , mo = False )
    
    mc.delete( consList , parsList )
    
    mc.setAttr( 'Origin_Grp.tx' , 0 )
    mc.setAttr( 'Origin_Grp.ty' , 0 )
    mc.setAttr( 'Origin_Grp.tz' , 0 )
    mc.setAttr( 'Origin_Grp.rx' , 0 )
    mc.setAttr( 'Origin_Grp.ry' , 0 )
    mc.setAttr( 'Origin_Grp.rz' , 0 )