import maya.cmds as mc
import maya.mel as mm
import os 
import re

def makeCacheDirectory(namespace='', assetName=''):
    sceneName = mc.file(q=True, exn=True)

    if sceneName:
        scenePath = sceneName.split('/maya')[0]
        
        cacheDir = '%s/cache' % scenePath
        if not os.path.exists(cacheDir):
            os.mkdir(cacheDir)

        assetCacheDir = '%s/%s_%s' % (cacheDir, namespace, assetName)

        if not os.path.exists(assetCacheDir):
            os.mkdir(assetCacheDir)

        
        infoCacheDir = '%s/ShotInfo' %cacheDir

        if not os.path.exists('%s.txt'%infoCacheDir):
            infoCacheTxt = open( "%s.txt" %infoCacheDir , "a" )
            infoCacheTxt.close()
        

        return assetCacheDir , infoCacheDir


def getCachePath(cacheDir='', assetName=''):
    files = os.listdir(cacheDir)
    abcFiles = []
    cacheName = ''

    abcFileRegx = re.compile(r'_v[0-9]{3}\.abc$')

    for each in files:
        if abcFileRegx.search(each):
            abcFiles.append(each)

    if abcFiles:
        lastestFile = sorted(abcFiles)[-1]
        lastestVersion = re.findall(r'_v([0-9]{3})\.abc', lastestFile)

        if lastestVersion:
            nextVersion = '%03d' % (int(lastestVersion[0]) + 1)
            cacheName = '%s/%s_v%s.abc' % (cacheDir, assetName, nextVersion)

    else:
        cacheName = '%s/%s_v001.abc' % (cacheDir, assetName)

    return cacheName


def getFrameRange():
    minFrame = mc.playbackOptions(q=True, min=True)
    maxFrame = mc.playbackOptions(q=True, max=True)

    return int(minFrame), int(maxFrame)

def abcCommand(obj, cachePath):
    frameRange = getFrameRange()

    cmd = 'AbcExport -j ' # start command
    cmd += '"-frameRange %s %s ' % (frameRange[0], frameRange[1])
    cmd += '-uvWrite -worldSpace -writeVisibility -dataFormat ogawa '
    cmd += '-root %s ' % obj
    cmd += '-file %s"' % cachePath

    return cmd

def getInformation():
    
    minFrame = mc.playbackOptions(q=True, min=True)
    maxFrame = mc.playbackOptions(q=True, max=True)
    
    sels = mc.ls(sl=True)
    infoSrc = ''

    for sel in sels :
        namespace = mc.referenceQuery(sel, namespace=True)
        placeCtrl = '%s:Place_Ctrl' %namespace[1:]
        headCtrl = '%s:Head_Ctrl' %namespace[1:]
        
        # Place_Ctrl
        if mc.objExists( placeCtrl ) :
            sclPlaceCtrl = mc.getAttr( '%s.sy' %placeCtrl )

            infoSrc += '%s = %s \r\n' %(placeCtrl,sclPlaceCtrl)
    
        # Head_Ctrl
        if mc.objExists( headCtrl ) :
            sxHeadCtrl = mc.getAttr( '%s.sx' %headCtrl )
            syHeadCtrl = mc.getAttr( '%s.sy' %headCtrl )
            szHeadCtrl = mc.getAttr( '%s.sz' %headCtrl )

            infoSrc += '%s = %s , %s , %s \r\n\r\n' %(headCtrl,sxHeadCtrl,syHeadCtrl,szHeadCtrl)

    frameRange = 'PreRoll = %s - %s \r\nFrameRange = %s - %s' %(minFrame-30,maxFrame+30,minFrame,maxFrame)

    return frameRange , infoSrc

def writeInformation( infoPath='' ) :
    
    shotInfo = getInformation()

    if os.path.exists( infoPath ) :
        
        infoTxt = open( "%s" %infoPath , "w" )
        infoTxt.write( "%s\r\n\r\n%s\r\n" %(shotInfo[0],shotInfo[1]))
        infoTxt.close()

def createCache():
    sels = mc.ls(sl=True)

    geoRegx = re.compile('Geo_Grp')
    assetRegx = re.compile(r'[a-zA-Z]+')

    if sels:
        for sel in sels:
            namespace = mc.referenceQuery(sel, namespace=True)
            assetName = assetRegx.findall(namespace)

            if namespace and assetName:
                cacheDir = makeCacheDirectory(namespace[1:], assetName[0])  # cache directory
                cachePath = getCachePath(cacheDir[0], assetName[0])         # cache path file

                children = mc.listRelatives(sel, c=True, type='transform', f=True)

                for child in children:
                    if geoRegx.search(child):
                        cmd = abcCommand(child, cachePath)
                        # print cmd
                        mm.eval(cmd)
                    else :
                        cam = mc.listRelatives(child, ad=True, s=True, type='camera')

                        if cam :
                            cmd = abcCommand(sel, cachePath)
                            # print cmd
                            mm.eval(cmd)

        writeInformation( '%s.txt' %cacheDir[1] )

    else:
        mc.warning('no selected object.')