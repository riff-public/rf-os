import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel

class ParentHelperUI( object ) :

    def __init__ ( self ) :
        if pm.window( 'ParentHelperUI' , exists = True ) :
            pm.deleteUI( 'ParentHelperUI' )
        
        width = 200
        window = pm.window( 'ParentHelperUI' , width = width , mnb = True , mxb = False , sizeable = True , rtf = True )
        
        allLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] )
        with allLayout :
            frameLayout = pm.frameLayout ( label = 'ParentHelper' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            
            with frameLayout :
                layout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width-4 ) ] )
                
                with layout :
                    pm.button( 'Attach' , label = 'Attach' , h = 35 , c = attach )
                    pm.separator( vis = False )
                    pm.button( 'Detach' , label = 'Detach' , h = 35 , c = detach )
                    pm.separator( vis = False )

        pm.showWindow( window )
    
def getInfo( *args ):
    sels = mc.ls( sl = True )
    name = sels[1]
    namePart = name.split( '_' )
    nameComb = ''.join( namePart )
    mainGrp = '%s_Grp' %nameComb
    posiGrp = '%sPosi_Grp' %nameComb
    locCtrl = '%sCons_Ctrl' %nameComb
    constraintGrp = 'ConstraintAnim_Grp'
    currTime = mc.currentTime( q = True )

    return {    
                 'sels'            : sels , 
                 'name'            : name , 
                 'nameComb'        : nameComb , 
                 'currTime'        : currTime ,
                 'mainGrp'         : mainGrp ,
                 'posiGrp'         : posiGrp ,
                 'locCtrl'         : locCtrl ,
                 'constraintGrp'   : constraintGrp
            }

def get_world_position( obj ):
    return [ mc.xform( obj , q = True , rp = True , ws = True ) , mc.xform( obj , q = True , ro = True , ws = True ) ]

def set_world_position( obj , posRot ):
    pos = posRot[0]
    rot = posRot[1]
    objPiv = mc.xform( obj , q = True , rp = True , ws = True )
    diff = ( pos[0]-objPiv[0] , pos[1]-objPiv[1]  , pos[2]-objPiv[2] )
    mc.xform( obj , t = diff , r = True , ws = True )
    mc.xform( obj , ro = rot , a = True , ws = True )

def add_tag_frist_position( obj ):
    position = get_world_position( obj )
    mc.addAttr( obj , ln = 'firstFrame' , at = 'float' , dv = mc.currentTime( q = True ) )
    mc.addAttr( obj , ln = 'firstPosRot' , dt = 'string' )
    mc.addAttr( obj , ln = 'endPosRot' , dt = 'string' )
    mc.setAttr( '%s.firstPosRot' %obj , '%s' %position  , type = 'string' )
    mc.setAttr( '%s.endPosRot' %obj , '%s' %position  , type = 'string' )

def get_tag_position( obj , firstEnd ):
    return eval( mc.getAttr ( '%s.%sPosRot' %(obj,firstEnd) ))

def snap( *args ):
    mc.delete( mc.parentConstraint ( args , mo = False ))
    mc.select( cl = True )

def makeLocator( name = '' ):
    cuv = mc.curve( n = name , d = 1 , p = [(0,1,0),(0,-1,0),(0,0,0),(-1,0,0),(1,0,0),(0,0,0),(0,0,-1),(0,0,1)] )
    shapes = mc.rename( mc.listRelatives( cuv , shapes = True ) , '%sShape' %name )
    return  cuv

def createGroup( *args ):
    info = getInfo()

    if not mc.objExists( info['constraintGrp'] ) :
        consGrp = mc.createNode( 'transform' , n = info['constraintGrp'] )

    mainGrp = mc.createNode( 'transform' , n = info['mainGrp'] )
    posiGrp = mc.createNode( 'transform' , n = info['posiGrp'] )
    locCtrl = makeLocator( info['locCtrl'] )

    mc.parent( posiGrp , mainGrp )
    mc.parent( locCtrl , posiGrp )
    mc.parent( mainGrp , consGrp )
    snap( info['sels'][1] , mainGrp )

    mc.parentConstraint( locCtrl , info['sels'][1] , mo  = False )
    add_tag_frist_position( info['mainGrp'] )

def transfer_anim_keys( *args ):

def attach( *args ):
    info = getInfo()

    if not mc.objExists( info['mainGrp'] ) :
        createGroup()

    cons = mc.parentConstraint( info['sels'][0] , info['mainGrp'] , mo = True )[0]
    targetList = mc.parentConstraint( cons , q = True  , targetList = True )

    for target in targetList :
        for  i in range(len(targetList)) :
            if info['sels'][0] == targetList[i] :
                mc.setKeyframe( cons , at = '%sW%s' %( info['sels'][0] , i ) , v = 1 , t = info['currTime'] )
                mc.setKeyframe( cons , at = '%sW%s' %( info['sels'][0] , i ) , v = 0 , t = info['currTime']-1 )
            else :
                if not mc.getAttr( '%s.%sW%s' %( cons , targetList[i] , i )) == 0 :
                    mc.setKeyframe( cons , at = '%sW%s' %( targetList[i] , i ) , v = 0 , t = info['currTime'] )
                    mc.setKeyframe( cons , at = '%sW%s' %( targetList[i] , i ) , v = 1 , t = info['currTime']-1 )

    endPosRot = get_world_position( info['mainGrp'] )
    mc.setAttr( '%s.endPosRot' %info['mainGrp'] , '%s' %endPosRot  , type = 'string' )

    if len(targetList) > 1 :
        firstPosRot = get_tag_position( info['mainGrp'] , 'first' )
        firstFrame = mc.getAttr( '%s.firstFrame' %info['mainGrp'] )
        diffRot = set_world_position( info['posiGrp'] , firstPosRot )
        mc.setKeyframe( info['posiGrp'] , at = ['translate', 'rotate'] , t = firstFrame-1 )
        mc.setAttr( '%s.t' %info['posiGrp'] , firstPosRot[0][0] , firstPosRot[0][1] , firstPosRot[0][2] )
        mc.setAttr( '%s.r' %info['posiGrp'] , firstPosRot[1][0] , firstPosRot[1][1] , firstPosRot[1][2] )
        mc.setKeyframe( info['posiGrp'] , at = ['translate', 'rotate'] , t = firstFrame )

def detach( *args ):
    info = getInfo()

    cons = mc.listRelatives( info['mainGrp'] , ad = True , type = 'constraint' )[0]
    targetList = mc.parentConstraint( cons , q = True  , targetList = True )

    for target in targetList :
        for  i in range(len(targetList)) :
            if not mc.getAttr( '%s.%sW%s' %( cons , targetList[i] , i )) == 0 :
                mc.setKeyframe( cons , at = '%sW%s' %( targetList[i] , i ) , v = 0 , t = info['currTime'] )
    
    mc.setKeyframe( info['posiGrp'] , at = ['translate', 'rotate'] , t = info['currTime']-1 )

    parentEndGrp = mc.createNode( 'transform' )
    positionEndGrp = mc.createNode( 'transform' )
    endPosRot = get_tag_position( info['mainGrp'] , 'end' )
    mc.setAttr( '%s.t' %parentEndGrp , endPosRot[0][0] , endPosRot[0][1] , endPosRot[0][2] )
    mc.setAttr( '%s.r' %parentEndGrp , endPosRot[1][0] , endPosRot[1][1] , endPosRot[1][2] )

    snap( info['posiGrp'] , positionEndGrp )
    
    mc.parent( positionEndGrp , parentEndGrp )
    pos = mc.getAttr( '%s.t' %positionEndGrp )[0]
    rot = mc.getAttr( '%s.r' %positionEndGrp )[0]

    mc.setKeyframe( info['posiGrp'] , at = 'tx' , v = pos[0] , t = info['currTime'] )
    mc.setKeyframe( info['posiGrp'] , at = 'ty' , v = pos[1] , t = info['currTime'] )
    mc.setKeyframe( info['posiGrp'] , at = 'tz' , v = pos[2] , t = info['currTime'] )

    mc.setKeyframe( info['posiGrp'] , at = 'rx' , v = rot[0] , t = info['currTime'] )
    mc.setKeyframe( info['posiGrp'] , at = 'ry' , v = rot[1] , t = info['currTime'] )
    mc.setKeyframe( info['posiGrp'] , at = 'rz' , v = rot[2] , t = info['currTime'] )