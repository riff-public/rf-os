import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  SUPER RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , name     = [ ] ,
                         tmpJnt   = [ ] , 
                         child    = [ ] , 
                         parent   = '' ,
                         size     = 1
                 ):

        ##-- Info
        lenght = len(tmpJnt)
        
        self.ctrlArray = []

        #-- Create Main Controls
        for i in range(lenght) :
            ctrl = rt.createCtrl( '%sSuper_Ctrl' %name[i] , 'square' , 'yellow' )
            ctrl.snapPoint( tmpJnt[i] )

            #-- Adjust Shape Controls
            ctrl.rotateShape( 0 , 45 , 0 )
            ctrl.scaleShape( size * 7 )
            
            #-- Adjust Hierarchy
            if i == 0 :
                ctrl.parent( parent )
            else :
                ctrl.parent( self.ctrlArray[i-1] )
                
        
            #-- Rig process
            ctrl.freeze()

            self.ctrlArray.append(ctrl)

        #-- Adjust Hierarchy
        mc.parent( child , self.ctrlArray[-1] )

        mc.select( cl = True )