import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  HEAD SQUASH STRETCH RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , uprTmpJnt = 'HeadUpr_TmpJnt' ,
                         lwrTmpJnt = 'HeadLwr_TmpJnt' ,
                         parent    = '' ,
                         ctrlGrp   = 'Ctrl_Grp' , 
                         skinGrp   = 'Skin_Grp' , 
                         stillGrp  = 'Still_Grp' ,
                         uprDef    = True ,
                         lwrDef    = True ,
                         midDef    = True ,
                         size      = 1 
                 ):

        #-- Create Main Group
        self.headDefCtrlGrp = rt.createNode( 'transform' , 'HeadDefCtrl_Grp' )
        self.headDefSkinGrp = rt.createNode( 'transform' , 'HeadDefSkin_Grp' )
        self.headDefStillGrp = rt.createNode( 'transform' , 'HeadDefStill_Grp' )
        mc.parentConstraint( parent , self.headDefCtrlGrp , mo = False )
        mc.scaleConstraint( parent , self.headDefCtrlGrp , mo = False )
        mc.parentConstraint( parent , self.headDefSkinGrp , mo = False )
        mc.scaleConstraint( parent , self.headDefSkinGrp , mo = False )
        self.headDefStillGrp.snap( parent )
        
        #-- Create Joint
        self.uprJnt = rt.createJnt( 'HeadUpr_Jnt' , uprTmpJnt )
        self.uprJntZro = rt.addGrp( self.uprJnt )
        self.uprJntOfst = rt.addGrp( self.uprJnt , 'Ofst' )

        self.midJnt = rt.createJnt( 'HeadMid_Jnt' )
        self.midJntZro = rt.addGrp( self.midJnt )
        self.midJntOfst = rt.addGrp( self.midJnt , 'Ofst' )

        self.lwrJnt = rt.createJnt( 'HeadLwr_Jnt' , lwrTmpJnt )
        self.lwrJntZro = rt.addGrp( self.lwrJnt )
        self.lwrJntOfst = rt.addGrp( self.lwrJnt , 'Ofst' )

        mc.delete( mc.pointConstraint ( self.uprJnt , self.lwrJnt , self.midJntZro , mo = False ))

        #-- Create Still Joint
        #self.midStillJnt = rt.createJnt( '%sDefMidStill%sJnt' %( name , side ))
        #self.midStillJntZro = rt.addGrp( self.midStillJnt )

        #self.rootStillJnt = rt.createJnt( '%sDefRootStill%sJnt' %( name , side ) , parent )
        #self.rootStillJntZro = rt.addGrp( self.rootStillJnt )

        #mc.parent( self.midStillJntZro , self.rootStillJntZro , self.jntStillGrp )

        #-- Create Controls
        self.headUprCtrl = rt.createCtrl( 'HeadUpr_Ctrl' , 'cube' , 'green' )
        self.headUprZro = rt.addGrp( self.headUprCtrl )

        self.headUprZro.snap( self.uprJnt )

        #-- Adjust Shape Controls
        self.headUprCtrl.scaleShape( size )

        #-- Create Aim UpVector Group
        self.midUpVecZro = rt.createNode( 'transform' , 'HeadMidUpVecZro_Grp' )
        self.midUpVecGrp = rt.createNode( 'transform' , 'HeadMidUpVec_Grp' )
        self.midAimGrp = rt.createNode( 'transform' , 'HeadMidAim_Grp' )
        self.midAimGrp.parent( self.midUpVecGrp )
        self.midUpVecGrp.parent( self.midUpVecZro )
        self.midUpVecZro.snap( self.midJnt )

        #-- Rig process
        mc.pointConstraint( self.uprJnt , self.lwrJnt , self.midJntOfst , mo = True )
        mc.pointConstraint( self.uprJnt , self.lwrJnt , self.midUpVecGrp , mo = True )
        mc.orientConstraint( self.midAimGrp , self.midJntOfst , mo = False )
        mc.aimConstraint( self.lwrJnt , self.midAimGrp , aim = (0,-1,0) , u = (0,0,1) , wut='objectrotation' , wuo = self.midUpVecGrp , wu= (0,0,1) , mo = True )
        
        self.headUprCtrl.attr('tx') >> self.uprJntOfst.attr('tx')
        self.headUprCtrl.attr('ty') >> self.uprJntOfst.attr('ty')
        self.headUprCtrl.attr('tz') >> self.uprJntOfst.attr('tz')

        self.uprDistGrp = rt.createNode( 'transform' , 'HeadUprDist_Grp' )
        self.lwrDistGrp = rt.createNode( 'transform' , 'HeadLwrDist_Grp' )
        mc.parentConstraint( self.uprJnt , self.uprDistGrp , mo = False )
        mc.parentConstraint( self.lwrJnt , self.lwrDistGrp , mo = False )

        self.uprDefaultDistGrp = rt.createNode( 'transform' , 'HeadUprDefaultDist_Grp' )
        self.lwrDefaultDistGrp = rt.createNode( 'transform' , 'HeadLwrDefaultDist_Grp' )
        self.uprDefaultDistGrp.snap( self.uprJnt )
        self.lwrDefaultDistGrp.snap( self.lwrJnt )
        mc.parentConstraint( parent , self.uprDefaultDistGrp , mo = True )
        mc.parentConstraint( parent , self.lwrDefaultDistGrp , mo = True )

        self.uprDist = rt.createNode( 'distanceBetween' , 'HeadUprDistance_Dist' )
        self.uprDefaultDist = rt.createNode( 'distanceBetween' , 'HeadUprDefaultDistance_Dist' )
        self.uprNrmDivMdv = rt.createNode( 'multiplyDivide' , 'HeadUprSqStNormDiv_Mdv' )
        self.uprPowMdv = rt.createNode( 'multiplyDivide' , 'HeadUprSqStPow_Mdv' )
        self.uprDivMdv = rt.createNode( 'multiplyDivide' , 'HeadUprSqStDiv_Mdv' )
        self.uprPma = rt.createNode( 'plusMinusAverage' , 'HeadUprSqSt_Pma' )

        self.uprDistGrp.attr('t') >> self.uprDist.attr('point1')
        self.lwrDistGrp.attr('t') >> self.uprDist.attr('point2')
        self.uprDefaultDistGrp.attr('t') >> self.uprDefaultDist.attr('point1')
        self.lwrDefaultDistGrp.attr('t') >> self.uprDefaultDist.attr('point2')
        self.uprDist.attr('distance') >> self.uprNrmDivMdv.attr('i1x')
        self.uprDefaultDist.attr('distance') >> self.uprNrmDivMdv.attr('i2x')
        self.uprNrmDivMdv.attr('ox') >> self.uprPowMdv.attr('i1x')
        self.uprPowMdv.attr('ox') >> self.uprDivMdv.attr('i2x')
        self.uprDivMdv.attr('ox') >> self.uprPma.attr('i1[0]')
        self.uprPma.attr('o1') >> self.midJnt.attr('sx')
        self.uprPma.attr('o1') >> self.midJnt.attr('sz')

        self.uprNrmDivMdv.attr('op').value = 2
        self.uprPowMdv.attr('op').value = 3
        self.uprDivMdv.attr('op').value = 2
        self.uprDivMdv.attr('i1x').value = 1
        self.uprPowMdv.attr('i2x').value = 2

        #mc.connectAttr( '%s.t' %self.midJntOfst , '%s.t' %self.midStillJnt )
        #mc.connectAttr( '%s.r' %self.midJntOfst , '%s.r' %self.midStillJnt )
        #mc.connectAttr( '%s.s' %self.midJnt , '%s.s' %self.midStillJnt )
        
        #-- Adjust Hierarchy
        mc.parent( self.headUprZro , self.midUpVecZro , self.headDefCtrlGrp )
        mc.parent( self.uprJntZro , self.midJntZro , self.lwrJntZro , self.headDefSkinGrp )
        mc.parent( self.uprDefaultDistGrp , self.lwrDefaultDistGrp , self.uprDistGrp , self.lwrDistGrp , self.headDefStillGrp )

        if mc.objExists( ctrlGrp ) :
            mc.parent( self.headDefCtrlGrp , ctrlGrp )

        if mc.objExists( skinGrp ) :
            mc.parent( self.headDefSkinGrp , skinGrp )

        if mc.objExists( stillGrp ) :
            mc.parent( self.headDefStillGrp , stillGrp )

        #-- Cleanup
        for obj in ( self.uprJntZro , self.uprJntZro , self.uprJntOfst , self.midJntZro , self.midJntZro , self.midJntOfst , 
                     self.lwrJntZro , self.lwrJntZro , self.lwrJntOfst , self.headUprZro , self.midUpVecZro , self.midUpVecGrp , self.midAimGrp ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        self.headUprCtrl.lockHideAttrs( 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        mc.select( cl = True )