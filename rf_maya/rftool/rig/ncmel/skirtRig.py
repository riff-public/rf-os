import maya.cmds as mc
from rf_maya.rftool.rig.ncmel import core
reload( core )
from rf_maya.rftool.rig.ncmel import rigTools as rt
reload( rt )

class run(object):
	def __init__(self , name 	= 'Skirt' ,
						elem 			= '' ,
						side 			= '' ,
						mainTmp			= 'Skirt_TmpJnt' ,
						tmpJnt 			= ['SkirtA_C_TmpJnt' ,
											'SkirtB_L_TmpJnt' ,
											'SkirtC_L_TmpJnt' ,
											'SkirtD_L_TmpJnt' ,
											'SkirtE_C_TmpJnt' ,
											'SkirtB_R_TmpJnt' ,
											'SkirtC_R_TmpJnt' ,
											'SkirtD_R_TmpJnt'] , 
						mainRootJnt 	= ['UpLeg_L_Jnt','UpLeg_R_Jnt'] ,
						mainEndLoc 		= ['LegPos_L_Loc','LegPos_R_Loc'] ,
						parent 			= 'Pelvis_Jnt' ,
						axisAim 		= ['y+','y-'] ,
						ctrlGrp			= 'AddCtrl_Grp' ,
						skinGrp			= 'Skin_Grp' ,
						jntGrp			= 'Jnt_Grp' ,
						stillGrp		= 'Still_Grp' ,
						axisRotate 		= 'x-' ,
						nrb 			= 'Skirt_Nrb' ,
						allMover		= 'AllMover_Ctrl',
						size 			= 1):

		##-- Info
		## Keep min max range u and v 0 to 1

		elem = elem.capitalize()

		self.ctrlGrp = core.Dag(ctrlGrp)
		self.skinGrp = core.Dag(skinGrp)
		self.jntGrp = core.Dag(jntGrp)
		self.stillGrp = core.Dag(stillGrp)
		self.nrb = (core.Dag(nrb)).shape
		self.allMover = core.Dag(allMover)

		if side == '':
			side = '_'
		else :
			side = '_{}_'.format(side)

		#-- Create mainControl
		self.mainJnt = rt.createJnt('{}{}{}Jnt'.format(name,elem,side) , mainTmp)
		self.mainCtrl = rt.createCtrl( '{}{}{}Ctrl'.format(name,elem,side) , 'stick' , 'pink' )
		self.mainJntZro = rt.addGrp(self.mainJnt)
		self.mainZro = rt.addGrp( self.mainCtrl )
		self.mainZro.snap(mainTmp)
		self.mainCtrl.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
		mc.parentConstraint(self.mainCtrl , self.mainJntZro , mo=False)
		mc.scaleConstraint(self.mainCtrl , self.mainJntZro , mo=False)
		mc.parentConstraint(parent , self.mainZro , mo=True)
		mc.scaleConstraint(parent , self.mainZro , mo=True)

		#-- Adjust Shape Controls
		self.mainCtrl.scaleShape( size )

		self.mainCtrl.addTitleAttr('{}Setting'.format(name))
		# self.mainCtrl.addAttr('ampDistance{}'.format(name) , 0)
		# self.mainCtrl.attr('ampDistance{}'.format(name)).value = 0.25

		if axisRotate == 'x+':
			rotateAxis = 'rx'
			driveValue = 1
		elif axisRotate == 'x-':
			rotateAxis = 'rx'
			driveValue = -1
		if axisRotate == 'y+':
			rotateAxis = 'ry'
			driveValue = 1
		elif axisRotate == 'y-':
			rotateAxis = 'ry'
			driveValue = -1
		if axisRotate == 'z+':
			rotateAxis = 'rz'
			driveValue = 1
		elif axisRotate == 'z-':
			rotateAxis = 'rz'
			driveValue = -1
		for each in axisAim:
			if 'x' in each:
				axisFst = 'ry'
				axisSnd = 'rz'
			elif 'y' in each:
				axisFst = 'rx'
				axisSnd = 'rz'
			elif 'z' in each:
				axisFst = 'rx'
				axisSnd = 'ry'

		self.locList = []
		self.flcList = []
		self.jntGrpList = []
		self.ctrlGrpList = []
		self.cndRstList = []
		self.NonRollJntGrp = []

		for num in range(len(mainRootJnt)):
			self.rootJnt = mainRootJnt[num]
			self.endLoc = mainEndLoc[num]
			self.endLocNode = core.Dag(mc.listRelatives(self.endLoc,c=True)[0])
			mc.parentConstraint(self.rootJnt , self.endLoc , mo=True)
			nameJnt = self.rootJnt.split('_')
			if len(nameJnt) == 2:
				mainSide = '_'
			else :
				mainSide = '_{}_'.format(nameJnt[1])

			# create NonRoll
			NonRollJnt = rt.addNonRollJnt( name , elem , self.rootJnt , self.endLoc , parent , axisAim[num] )
			self.NonRollJntGrp.append(NonRollJnt['jntGrp'])
			self.NonRollRootNr = NonRollJnt['rootNr']

			# create main follicle
			self.flc = mc.listRelatives(mc.createNode('follicle'),p=True)
			self.flc = mc.rename(self.flc , '{}{}Pos{}Flc'.format(name,elem,mainSide))
			self.flc = core.Dag('{}{}Pos{}Flc'.format(name,elem,mainSide))
			self.flcNode = self.flc.shape
			self.cps = rt.createNode('closestPointOnSurface' , '{}{}{}Cps'.format(name,elem,mainSide))
			self.nrb.attr('worldSpace[0]') >> self.cps.attr('inputSurface')
			self.endLocNode.attr('worldPosition[0]') >> self.cps.attr('inPosition')
			self.cps.attr('parameterU') >> self.flcNode.attr('parameterU')
			self.cps.attr('parameterV') >> self.flcNode.attr('parameterV')
			self.nrb.attr('worldSpace[0]') >> self.flcNode.attr('inputSurface')
			self.nrb.attr('worldMatrix[0]') >> self.flcNode.attr('inputWorldMatrix')
			self.flcNode.attr('outTranslate') >> self.flc.attr('t')
			self.flcNode.attr('outRotate') >> self.flc.attr('r')
			self.flcList.append(self.flc)

			# rig rotate NonRoll
			# create attr switch for negative axis
			self.mainCtrl.addAttr('AxisN{}{}{}'.format(axisFst.capitalize(),nameJnt[0],mainSide) , 0 , 1)
			self.mainCtrl.addAttr('AxisN{}{}{}'.format(axisSnd.capitalize(),nameJnt[0],mainSide) , 0 , 1)
			self.mainCtrl.attr('AxisN{}{}{}'.format(axisFst.capitalize(),nameJnt[0],mainSide)).value = 1
			self.mainCtrl.attr('AxisN{}{}{}'.format(axisSnd.capitalize(),nameJnt[0],mainSide)).value = 1

			self.cndNAF = rt.createNode('condition' , '{}{}AxisN{}{}Cnd'.format(nameJnt[0],elem,axisFst.capitalize(),mainSide))
			self.cndNAS = rt.createNode('condition' , '{}{}AxisN{}{}Cnd'.format(nameJnt[0],elem,axisSnd.capitalize(),mainSide))
			self.mainCtrl.attr('AxisN{}{}{}'.format(axisFst.capitalize(),nameJnt[0],mainSide)) >> self.cndNAF.attr('firstTerm')
			self.mainCtrl.attr('AxisN{}{}{}'.format(axisSnd.capitalize(),nameJnt[0],mainSide)) >> self.cndNAS.attr('firstTerm')
			self.cndNAF.attr('operation').value = 2
			self.cndNAS.attr('operation').value = 2
			# self.cndNAF.attr('colorIfTrueR').value = -1
			# self.cndNAF.attr('colorIfFalseR').value = 0
			# self.cndNAS.attr('colorIfTrueR').value = -1
			# self.cndNAS.attr('colorIfFalseR').value = 0
			self.NonRollRootNr.attr(axisFst) >> self.cndNAF.attr('colorIfTrueR')
			self.cndNAF.attr('colorIfFalseR').value = 0
			self.NonRollRootNr.attr(axisSnd) >> self.cndNAS.attr('colorIfTrueR')
			self.cndNAS.attr('colorIfFalseR').value = 0

			# # create condition for calculate positiveAxis
			# self.cndFst = rt.createNode('condition' , '{}{}Rotate{}{}Cnd'.format(name,elem,axisFst,mainSide))
			# self.cndSnd = rt.createNode('condition' , '{}{}Rotate{}{}Cnd'.format(name,elem,axisSnd,mainSide))
			# self.NonRollRootNr.attr(axisFst) >> self.cndFst.attr('firstTerm')
			# self.NonRollRootNr.attr(axisSnd) >> self.cndSnd.attr('firstTerm')
			# self.cndFst.attr('operation').value = 3
			# self.cndFst.attr('colorIfTrueR').value = 1
			# self.cndNAF.attr('outColorR') >> self.cndFst.attr('colorIfFalseR')
			# # self.cndFst.attr('colorIfFalseR').value = -1
			# self.cndSnd.attr('operation').value = 3
			# self.cndSnd.attr('colorIfTrueR').value = 1
			# self.cndNAS.attr('outColorR') >> self.cndSnd.attr('colorIfFalseR')
			# # self.cndSnd.attr('colorIfFalseR').value = -1
			# self.mdvNgt = rt.createNode('multiplyDivide' , '{}{}NegateRotate{}Mdv'.format(name,elem,mainSide))
			# self.NonRollRootNr.attr(axisFst) >> self.mdvNgt.attr('i1x')
			# self.cndFst.attr('outColorR') >> self.mdvNgt.attr('i2x')
			# self.NonRollRootNr.attr(axisSnd) >> self.mdvNgt.attr('i1z')
			# self.cndSnd.attr('outColorR') >> self.mdvNgt.attr('i2z')

			self.mdvPowFst = rt.createNode('multiplyDivide' , '{}{}Pow{}{}Mdv'.format(name,elem,axisFst.capitalize(),mainSide))
			self.mdvPowSnd = rt.createNode('multiplyDivide' , '{}{}Pow{}{}Mdv'.format(name,elem,axisSnd.capitalize(),mainSide))
			self.mdvSqrFst = rt.createNode('multiplyDivide' , '{}{}Sqr{}{}Mdv'.format(name,elem,axisFst.capitalize(),mainSide))
			self.mdvSqrSnd = rt.createNode('multiplyDivide' , '{}{}Sqr{}{}Mdv'.format(name,elem,axisSnd.capitalize(),mainSide))
			self.mdvPowFst.attr('operation').value = 3
			self.mdvPowSnd.attr('operation').value = 3
			self.mdvSqrFst.attr('operation').value = 3
			self.mdvSqrSnd.attr('operation').value = 3
			self.mdvPowFst.attr('i2x').value = 2
			self.mdvPowSnd.attr('i2x').value = 2
			self.mdvSqrFst.attr('i2x').value = 0.5
			self.mdvSqrSnd.attr('i2x').value = 0.5
			self.cndNAF.attr('outColorR') >> self.mdvPowFst.attr('i1x')
			self.cndNAS.attr('outColorR') >> self.mdvPowSnd.attr('i1x')
			self.mdvPowFst.attr('ox') >> self.mdvSqrFst.attr('i1x')
			self.mdvPowSnd.attr('ox') >> self.mdvSqrSnd.attr('i1x')

			self.cndRst = rt.createNode('condition' , '{}{}RotateResult{}Cnd'.format(name,elem,mainSide))
			self.cndRst.attr('operation').value = 3
			self.mdvSqrFst.attr('ox') >> self.cndRst.attr('firstTerm')
			self.mdvSqrSnd.attr('ox') >> self.cndRst.attr('secondTerm')
			self.mdvSqrFst.attr('ox') >> self.cndRst.attr('colorIfTrueR')
			self.mdvSqrSnd.attr('ox') >> self.cndRst.attr('colorIfFalseR')
			self.cndRstList.append(self.cndRst)
			# self.cmp = rt.createNode('clamp' , '{}{}RotateLimit{}Cmp'.format(name,elem,mainSide))
			# self.cndRst.attr('outColorR') >> self.cmp.attr('inputR')
			# self.cmp.attr('maxR').value = 1000000000
			# self.cmp.attr('maxG').value = 1000000000
			# self.cmp.attr('maxB').value = 1000000000
			# self.cmpList.append(self.cmp)


		# rig each loc
		self.disList = []
		self.disListLft = []
		self.disListRgt = []
		self.ofstList = []
		for i in range(len(tmpJnt)):
			self.tmpJnt = core.Dag(tmpJnt[i])
			self.nameT , self.sideT , self.typeT = self.tmpJnt.getElemName()
			self.mainCtrl.addAttr('distance{}{}'.format(self.nameT, self.sideT) , 0)
			self.mainCtrl.attr('distance{}{}'.format(self.nameT, self.sideT)).value = 0.25

			#-- Create ctrl Group
			self.grpCtrl = rt.createNode( 'transform' ,  '{}Ctrl{}Grp'.format(self.nameT , self.sideT ))
			self.grpCtrl.snap(tmpJnt[i])

			if mc.objExists(parent):
				self.parsCons = mc.parentConstraint( parent , self.grpCtrl , mo = True )
				self.sclsCons = mc.scaleConstraint( parent , self.grpCtrl , mo = True )
			
			#-- Create Joint
			self.jnt = rt.createJnt('{}{}{}Jnt'.format(self.nameT,elem,self.sideT) , tmpJnt[i])
			
			# self.tmpEnd = mc.listRelatives(tmpJnt[i],c=True)[0]
			# self.jntEnd = rt.createJnt('{}{}End{}Jnt'.format(self.nameT,elem,self.sideT) , self.tmpEnd)

			self.Zro = rt.addGrp(self.jnt)
			# self.endZro = rt.addGrp(self.jntEnd)
			# self.endZro.parent(self.jnt)
			self.jntGrpList.append(self.Zro)
			self.jnt.attr('ssc').v = 0
			# self.jntEnd.attr('ssc').v = 0

			#-- Create Root Controls
			self.ctrl = rt.createCtrl( '{}{}{}Ctrl'.format(self.nameT,elem,self.sideT) , 'square' , 'cyan' , jnt = True )
			self.gmbl = rt.addGimbal( self.ctrl )
			self.zroGrp = rt.addGrp( self.ctrl )
			self.Ofst = rt.addGrp( self.ctrl , 'Ofst')
			self.parentGrp = rt.addGrp( self.ctrl , 'Pars')
			self.zroGrp.snap(tmpJnt[i])

			# #-- Create End Controls
			# self.ctrlEnd = rt.createCtrl( '{}{}End{}Ctrl'.format(self.nameT,elem,self.sideT) , 'square' , 'cyan' , jnt = True )
			# self.gmblEnd = rt.addGimbal( self.ctrlEnd )
			# self.zroEndGrp = rt.addGrp( self.ctrlEnd )
			# self.OfstEnd = rt.addGrp( self.ctrlEnd , 'Ofst')
			# self.parentEndGrp = rt.addGrp( self.ctrlEnd , 'Pars')
			# self.zroEndGrp.snap(self.tmpEnd)

			#-- Rig process
			mc.parentConstraint( self.gmbl , self.jnt , mo = False )
			mc.scaleConstraint( self.gmbl , self.jnt , mo = False )
			# mc.parentConstraint( self.gmblEnd , self.jntEnd , mo = False )
			# mc.scaleConstraint( self.gmblEnd , self.jntEnd , mo = False )

			# Rig child jnt
			self.tmpChild = mc.listRelatives(tmpJnt[i],ad=True,type='joint')[::-1]
			self.listCtrl = [self.ctrl]
			self.listGmbl = [self.gmbl]
			if self.tmpChild:
				self.listJnt = [self.jnt]
				self.listOfst = [self.Ofst]
				for n in range(len(self.tmpChild)):
					# create jnt
					self.nameC = self.tmpChild[n].split('_')[0]
					self.jntC = rt.createJnt('{}{}{}Jnt'.format(self.nameC,elem,self.sideT) , self.tmpChild[n])
					self.zroJnt = rt.addGrp(self.jntC)
					self.zroJnt.parent(self.listJnt[n])
					self.listJnt.append(self.jntC)

					# create ctrl
					self.ctrlC = rt.createCtrl( '{}{}{}Ctrl'.format(self.nameC,elem,self.sideT) , 'square' , 'cyan' , jnt = True )
					self.gmblC = rt.addGimbal( self.ctrlC )
					self.zroCGrp = rt.addGrp( self.ctrlC )
					self.OfstC = rt.addGrp( self.ctrlC , 'Ofst')
					self.parentCGrp = rt.addGrp( self.ctrlC , 'Pars')
					self.zroCGrp.snap(self.tmpChild[n])
					self.zroCGrp.parent(self.listGmbl[n])
					self.listOfst.append(self.OfstC)
					self.listCtrl.append(self.ctrlC)
					self.listGmbl.append(self.gmblC)

					# rig
					mc.parentConstraint( self.gmblC , self.jntC , mo = False )
					mc.scaleConstraint( self.gmblC , self.jntC , mo = False )

				#-- Add stretch
				for n in range(1,len(self.listCtrl)):
					rt.addFkStretch( obj = self.listCtrl[n-1] , traget = self.listOfst[n] , ax = 'y' , val = 1 )

			#-- Adjust Shape Controls
			for ctrl in ( self.listCtrl + self.listGmbl ) :
				ctrl.scaleShape( size )

			if mc.objExists(ctrlGrp):
				rt.localWorld( self.ctrl , ctrlGrp , self.grpCtrl , self.zroGrp , 'orient' )

			# self.zroEndGrp.parent(self.gmbl)
			self.zroGrp.parent(self.grpCtrl)
			self.ofstList.append(self.Ofst)
			self.ctrlGrpList.append(self.grpCtrl)

			# create each loc on joint
			self.loc = mc.listRelatives(mc.createNode('locator'),p=True)
			self.loc = core.Dag(mc.rename(self.loc , '{}{}{}Loc'.format(self.nameT , elem , self.sideT)))
			self.loc.snap(self.jnt)
			mc.parentConstraint(self.mainJnt , self.loc , mo=True)
			mc.scaleConstraint(self.mainJnt , self.loc , mo=True)
			self.locList.append(self.loc)

			for eachMainLoc in mainEndLoc:
				nameLoc = eachMainLoc.split('_')
				if len(nameLoc) == 2:
					sideObj = '_'
					self.sideMain = ''
				else:
					sideObj = '_{}_'.format(nameLoc[1])
					if sideObj == '_L_':
						self.sideMain = 'Lft'
					elif sideObj == '_R_':
						self.sideMain = 'Rgt'

				self.dis = rt.createNode('distanceBetween' , '{}{}{}{}Dis'.format(self.nameT,elem,self.sideMain,self.sideT))
				self.disList.append(self.dis)
				if len(nameLoc) > 2:
					if self.sideMain == 'Lft': self.disListLft.append(self.dis)
					elif self.sideMain == 'Rgt': self.disListRgt.append(self.dis)

		if len(mainEndLoc) == 1:
			self.mainLoc = core.Dag(mainEndLoc[0])
			for i in range(len(self.disList)):
				self.loc = self.locList[i]
				self.mainLoc.attr('worldPosition[0]') >> self.disList[i].attr('point1')
				self.loc.attr('worldPosition[0]') >> self.disList[i].attr('point2')
		else:
			for i in range(len(mainEndLoc)):
				checkSide = mainEndLoc[i].split('_')[1]
				self.mainLoc = core.Dag(mainEndLoc[i])
				if checkSide == 'L':
					for v in range(len(self.disListLft)):
						self.loc = self.locList[v].shape
						self.mainLoc.attr('worldPosition[0]') >> self.disListLft[v].attr('point1')
						self.loc.attr('worldPosition[0]') >> self.disListLft[v].attr('point2')
				else:
					for v in range(len(self.disListRgt)):
						self.loc = self.locList[v].shape
						self.mainLoc.attr('worldPosition[0]') >> self.disListRgt[v].attr('point1')
						self.loc.attr('worldPosition[0]') >> self.disListRgt[v].attr('point2')

		# add attr amp
		self.mainCtrl.addTitleAttr('{}AmpPositive'.format(name))
		for i in range(len(tmpJnt)):
			self.tmpJnt = core.Dag(tmpJnt[i])
			self.name , self.side , self.type = self.tmpJnt.getElemName()
			self.mainCtrl.addAttr('{}{}AmpP{}'.format(self.name,elem,self.side),0)
			self.mainCtrl.attr('{}{}AmpP{}'.format(self.name,elem,self.side)).value = 1
		self.mainCtrl.addTitleAttr('{}AmpNegative'.format(name))
		for i in range(len(tmpJnt)):
			self.tmpJnt = core.Dag(tmpJnt[i])
			self.name , self.side , self.type = self.tmpJnt.getElemName()
			self.mainCtrl.addAttr('{}{}AmpN{}'.format(self.name,elem,self.side),0)
			self.mainCtrl.attr('{}{}AmpN{}'.format(self.name,elem,self.side)).value = 1


		for i in range(len(tmpJnt)):
			self.tmpJnt = core.Dag(tmpJnt[i])
			self.name , self.side , self.type = self.tmpJnt.getElemName()
			self.mdvDis = rt.createNode('multiplyDivide' , '{}{}Dis{}Mdv'.format(self.name,elem,self.side))
			self.mdvDisAmp = rt.createNode('multiplyDivide' , '{}{}DisAmp{}Mdv'.format(self.name,elem,self.side))
			self.rmpP = rt.createNode('remapValue' , '{}{}{}RmpP'.format(self.name,elem,self.side))
			self.rmpN = rt.createNode('remapValue' , '{}{}{}RmpN'.format(self.name,elem,self.side))
			self.mdvDis.attr('operation').value = 2
			self.allMover.attr('sy') >> self.mdvDis.attr('i1x')
			self.mdvDisAmp.attr('operation').value = 2
			self.rmpP.attr('inputMin').value = 0.85
			self.rmpN.attr('inputMin').value = 0.85
			self.rmpN.attr('inputMax').value = 0
			self.mainCtrl.attr('distance{}{}'.format(self.name, self.side)) >> self.mdvDisAmp.attr('i2x')
			self.mdvDis.attr('ox') >> self.mdvDisAmp.attr('i1x')
			self.mdvDisAmp.attr('ox') >> self.rmpP.attr('inputValue')
			self.mdvDisAmp.attr('ox') >> self.rmpN.attr('inputValue')

			if self.disListRgt and self.disListLft:
				self.disL = core.Dag(self.disListLft[i])
				self.disR = core.Dag(self.disListRgt[i])
				# self.mdvDisSca = rt.createNode('multiplyDivide' , '{}{}DisSca{}Mdv'.format(self.name,elem,self.side))
				# self.disL.attr('distance') >> self.mdvDisSca.attr('i1x')
				self.cndDis = rt.createNode('condition' , '{}{}Dis{}Cnd'.format(self.name,elem,self.side))
				self.cndRtt = rt.createNode('condition' , '{}{}Rtt{}Cnd'.format(self.name,elem,self.side))
				self.cndDis.attr('operation').value = 5
				self.cndRtt.attr('operation').value = 3
				self.disL.attr('distance') >> self.cndDis.attr('firstTerm')
				self.disR.attr('distance') >> self.cndDis.attr('secondTerm')
				self.disL.attr('distance') >> self.cndDis.attr('colorIfTrueR')
				self.disR.attr('distance') >> self.cndDis.attr('colorIfFalseR')
				self.cndDis.attr('outColorR') >> self.mdvDis.attr('i2x')
			else:
				self.dis = core.Dag(self.disList[i])
				self.dis.attr('distance') >> self.mdvDis.attr('i2x')
			
			self.mdvTtlP = rt.createNode('multiplyDivide' , '{}{}TotalP{}Mdv'.format(self.name,elem,self.side))
			self.mdvAmpP = rt.createNode('multiplyDivide' , '{}{}AmpP{}Mdv'.format(self.name,elem,self.side))
			self.mdvRstP = rt.createNode('multiplyDivide' , '{}{}ResultP{}Mdv'.format(self.name,elem,self.side))
			self.mdvTtlN = rt.createNode('multiplyDivide' , '{}{}TotalN{}Mdv'.format(self.name,elem,self.side))
			self.mdvAmpN = rt.createNode('multiplyDivide' , '{}{}AmpN{}Mdv'.format(self.name,elem,self.side))
			self.mdvRstN = rt.createNode('multiplyDivide' , '{}{}ResultN{}Mdv'.format(self.name,elem,self.side))
			self.pma = rt.createNode('plusMinusAverage' , '{}{}Result{}Pma'.format(self.name,elem,self.side))
			self.rmpP.attr('outValue') >> self.mdvTtlP.attr('i1x')
			self.rmpN.attr('outValue') >> self.mdvTtlN.attr('i1x')

			if len(mainRootJnt) > 1:
				self.cndRstList[0].attr('outColorR') >> self.cndRtt.attr('firstTerm')
				self.cndRstList[1].attr('outColorR') >> self.cndRtt.attr('secondTerm')
				self.cndRstList[0].attr('outColorR') >> self.cndRtt.attr('colorIfTrueG')
				self.cndRstList[1].attr('outColorR') >> self.cndRtt.attr('colorIfFalseG')
				self.cndRtt.attr('outColorG') >> self.mdvTtlP.attr('i2x')
				self.cndRtt.attr('outColorG') >> self.mdvTtlN.attr('i2x')
			else:
				self.cndRstList[0].attr('outColorR') >> self.mdvTtlP.attr('i2x')
				self.cndRstList[1].attr('outColorR') >> self.mdvTtlN.attr('i2x')

			self.mdvTtlP.attr('ox') >> self.mdvAmpP.attr('i1x')
			self.mdvTtlN.attr('ox') >> self.mdvAmpN.attr('i1x')

			self.mainCtrl.attr('{}{}AmpP{}'.format(self.name,elem,self.side)) >> self.mdvAmpP.attr('i2x')
			self.mainCtrl.attr('{}{}AmpN{}'.format(self.name,elem,self.side)) >> self.mdvAmpN.attr('i2x')
			self.mdvAmpP.attr('ox') >> self.mdvRstP.attr('i1x')
			self.mdvAmpN.attr('ox') >> self.mdvRstN.attr('i1x')

			self.mdvRstP.attr('i2x').value = driveValue
			self.mdvRstN.attr('i2x').value = -driveValue
			self.mdvRstP.attr('ox') >> self.pma.attr('i1[0]')
			self.mdvRstN.attr('ox') >> self.pma.attr('i1[1]')
			self.pma.attr('o1') >> self.ofstList[i].attr(rotateAxis)

		# adjust
		self.mainJntZro.parent(self.skinGrp)
		self.mainZro.parent(self.ctrlGrp)
		mc.parent(self.ctrlGrpList , self.mainZro)
		self.grp = rt.createNode('transform' , '{}{}Still{}Grp'.format(name,elem,side))
		self.grp.parent(self.stillGrp)
		mc.skinCluster('{}{}{}Jnt'.format(name,elem,side) , nrb , tsb=True ,n ='{}{}{}Skc'.format(name,elem,side))
		mc.parent(nrb , self.locList , self.flcList , mainEndLoc , self.grp)
		mc.parent(self.NonRollJntGrp , self.jntGrp)
		for each in self.jntGrpList:
			mc.parent(each , self.mainJnt)
			mc.parentConstraint('{}{}{}Jnt'.format(name,elem,side),each,mo=True)
			mc.scaleConstraint('{}{}{}Jnt'.format(name,elem,side),each,mo=True)
				
# exsample
# from ncmel import skirtRig as skr
# reload(skr)

# skr.run(name 	= 'Skirt' ,
# 		elem 			= '' ,
# 		side 			= '' ,
# 		mainTmp			= 'Skirt_TmpJnt' ,
# 		tmpJnt 			= ['SkirtRootA_C_TmpJnt' ,
# 							'SkirtRootB_L_TmpJnt' ,
# 							'SkirtRootC_L_TmpJnt' ,
# 							'SkirtRootD_L_TmpJnt' ,
# 							'SkirtRootE_C_TmpJnt' ,
# 							'SkirtRootB_R_TmpJnt' ,
# 							'SkirtRootC_R_TmpJnt' ,
# 							'SkirtRootD_R_TmpJnt'] , 
# 		mainRootJnt 	= ['UpLeg2RbnDtl_L_Jnt','UpLeg2RbnDtl_R_Jnt'] ,
# 		mainEndLoc 		= ['LegPos_L_Loc','LegPos_R_Loc'] ,
# 		parent 			= 'Pelvis_Jnt' ,
# 		axisAim 		= ['y+','y-'] ,
# 		ctrlGrp			= 'AddCtrl_Grp' ,
# 		skinGrp			= 'Skin_Grp' ,
# 		jntGrp			= 'Jnt_Grp' ,
# 		stillGrp		= 'Still_Grp' ,
# 		axisRotate 		= 'x-' ,
# 		nrb 			= 'Skirt_Nrb' ,
# 		allMover		= 'AllMover_Ctrl',
# 		size 			= 1)

# skr.run(name 	= 'Sleeve' ,
# 		elem 			= '' ,
# 		side 			= 'L' ,
# 		mainTmp			= 'Sleeve_L_TmpJnt' ,
# 		tmpJnt 			= ['SleeveRootA_L_TmpJnt' ,
# 							'SleeveRootB_L_TmpJnt' ,
# 							'SleeveRootC_L_TmpJnt' ,
# 							'SleeveRootD_L_TmpJnt' ,
# 							'SleeveRootE_L_TmpJnt' ,
# 							'SleeveRootF_L_TmpJnt' ,
# 							'SleeveRootG_L_TmpJnt' ,
# 							'SleeveRootH_L_TmpJnt'] , 
# 		mainRootJnt 	= ['UpArm2RbnDtl_L_Jnt'] ,
# 		mainEndLoc 		= ['SleevePos_L_Loc'] ,
# 		parent 			= 'Clav_L_Jnt' ,
# 		axisAim 		= ['y+'] ,
# 		ctrlGrp			= 'AddCtrl_Grp' ,
# 		skinGrp			= 'Skin_Grp' ,
# 		jntGrp			= 'Jnt_Grp' ,
# 		stillGrp		= 'Still_Grp' ,
# 		axisRotate 		= 'x-' ,
# 		nrb 			= 'Sleeve_L_Nrb' ,
# 		allMover		= 'AllMover_Ctrl',
# 		size 			= 1)
		
# skr.run(name 	= 'Sleeve' ,
# 		elem 			= '' ,
# 		side 			= 'R' ,
# 		mainTmp			= 'Sleeve_R_TmpJnt' ,
# 		tmpJnt 			= ['SleeveRootA_R_TmpJnt' ,
# 							'SleeveRootB_R_TmpJnt' ,
# 							'SleeveRootC_R_TmpJnt' ,
# 							'SleeveRootD_R_TmpJnt' ,
# 							'SleeveRootE_R_TmpJnt' ,
# 							'SleeveRootF_R_TmpJnt' ,
# 							'SleeveRootG_R_TmpJnt' ,
# 							'SleeveRootH_R_TmpJnt'] , 
# 		mainRootJnt 	= ['UpArm2RbnDtl_R_Jnt'] ,
# 		mainEndLoc 		= ['SleevePos_R_Loc'] ,
# 		parent 			= 'Clav_R_Jnt' ,
# 		axisAim 		= ['y-'] ,
# 		ctrlGrp			= 'AddCtrl_Grp' ,
# 		skinGrp			= 'Skin_Grp' ,
# 		jntGrp			= 'Jnt_Grp' ,
# 		stillGrp		= 'Still_Grp' ,
# 		axisRotate 		= 'x-' ,
# 		nrb 			= 'Sleeve_R_Nrb' ,
# 		size 			= 1)	
		
# skr.run(name 	= 'CollarLwr' ,
# 		elem 			= '' ,
# 		side 			= '' ,
# 		mainTmp			= 'CollarLwr_TmpJnt' ,
# 		tmpJnt 			= ['CollarLwrRootA_C_TmpJnt' ,
# 							'CollarLwrRootB_L_TmpJnt' ,
# 							'CollarLwrRootC_L_TmpJnt' ,
# 							'CollarLwrRootD_L_TmpJnt' ,
# 							'CollarLwrRootE_C_TmpJnt' ,
# 							'CollarLwrRootB_R_TmpJnt' ,
# 							'CollarLwrRootC_R_TmpJnt' ,
# 							'CollarLwrRootD_R_TmpJnt'] , 
# 		mainRootJnt 	= ['Clav_L_Jnt','Clav_R_Jnt'] ,
# 		mainEndLoc 		= ['UpArmPos_L_Loc','UpArmPos_R_Loc'] ,
# 		parent 			= 'Spine5_Jnt' ,
# 		axisAim 		= ['y+','y-'] ,
# 		ctrlGrp			= 'AddCtrl_Grp' ,
# 		skinGrp			= 'Skin_Grp' ,
# 		jntGrp			= 'Jnt_Grp' ,
# 		stillGrp		= 'Still_Grp' ,
# 		axisRotate 		= 'x-' ,
# 		nrb 			= 'CollarLwr_Nrb' ,
# 		allMover		= 'AllMover_Ctrl',
# 		size 			= 1)

# skr.run(name 	= 'CollarUpr' ,
# 		elem 			= '' ,
# 		side 			= '' ,
# 		mainTmp			= 'CollarUpr_TmpJnt' ,
# 		tmpJnt 			= ['CollarUprRootA_C_TmpJnt' ,
# 							'CollarUprRootB_L_TmpJnt' ,
# 							'CollarUprRootC_L_TmpJnt' ,
# 							'CollarUprRootD_L_TmpJnt' ,
# 							'CollarUprRootE_C_TmpJnt' ,
# 							'CollarUprRootB_R_TmpJnt' ,
# 							'CollarUprRootC_R_TmpJnt' ,
# 							'CollarUprRootD_R_TmpJnt'] , 
# 		mainRootJnt 	= ['Neck2RbnDtl_Jnt'] ,
# 		mainEndLoc 		= ['CollarUprPos_L_Loc'] ,
# 		parent 			= 'Spine5_Jnt' ,
# 		axisAim 		= ['y+'] ,
# 		ctrlGrp			= 'AddCtrl_Grp' ,
# 		skinGrp			= 'Skin_Grp' ,
# 		jntGrp			= 'Jnt_Grp' ,
# 		stillGrp		= 'Still_Grp' ,
# 		axisRotate 		= 'x+' ,
# 		nrb 			= 'CollarUpr_Nrb' ,
# 		allMover		= 'AllMover_Ctrl',
# 		size 			= 1)
