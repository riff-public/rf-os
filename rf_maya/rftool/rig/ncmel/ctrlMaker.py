import pymel.core as pm
import maya.cmds as mc
from functools import partial
from ncmel import core
from ncmel import rigTools as rt
reload(core)
reload(rt)

class ctrlMakerUI( object ) :
    
    def __init__( self , version = '1.0' ) :
        
        if pm.window( 'CtrlMakerUI' , exists = True ) :
            pm.deleteUI( 'CtrlMakerUI' )
        
        width = 224
        
        window = pm.window( 'CtrlMakerUI', title = "Control Tools  %s" %version , width = width , mnb = True , mxb = False , sizeable = True , rtf = True )

        allLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width ) ] )
        with allLayout :
            ctrlShapeframeLayout = pm.frameLayout ( label = 'Control Shape' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            with ctrlShapeframeLayout :

                ctrlShapeLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width-2 ) ] )
                with ctrlShapeLayout :
                    pm.button( 'ctrlBut' , label = 'Select Shape Ctrl' , h = 80 , c = self.createCurve )
                    pm.popupMenu()
                    labels = ( 'sphere' , 'cylinder' , 'stick' , 'circle' , 'arrowBall' , 'arrowCircle' , 'pyramid' , 'capsule' ,
                               'arrowCross' , 'triangle' , 'plus' , 'daimond' , 'square' , 'cube' , 'arrow' , 'locator' , 'null' , 'line' )
                    
                    for label in labels :
                        mc.menuItem( l = label , c = partial( self.changeButtonCtrl , label ))

                    pm.separator( h = 2 , vis = False )

                    paletteLayout = pm.rowColumnLayout ( w = width , nc = 10 , columnWidth = [ ( 1 , width/10 ),( 2 , width/10 ),( 3 , width/10 ),( 4 , width/10 ),( 5 , width/10 ),
                                                                                              ( 6 , width/10 ),( 7 , width/10 ),( 8 , width/10 ),( 9 , width/10 ),( 10 , width/10 )] )
                    with paletteLayout :
                        pm.button( label = '' , bgc = [1,0,0] , h = width/10 , c = partial( self.paletteColor , 'red' ))
                        pm.button( label = '' , bgc = [0,1,0] , h = width/10 , c = partial( self.paletteColor , 'green' ))
                        pm.button( label = '' , bgc = [0,0,1] , h = width/10 , c = partial( self.paletteColor , 'blue' ))
                        pm.button( label = '' , bgc = [1,1,0] , h = width/10 , c = partial( self.paletteColor , 'yellow' ))
                        pm.button( label = '' , bgc = [0,1,1] , h = width/10 , c = partial( self.paletteColor , 'cyan' ))
                        pm.button( label = '' , bgc = [1,0.7,1] , h = width/10 , c = partial( self.paletteColor , 'pink' ))
                        pm.button( label = '' , bgc = [0.3,0.5,0.3] , h = width/10 , c = partial( self.paletteColor , 'darkGreen' ))
                        pm.button( label = '' , bgc = [1,1,1] , h = width/10 , c = partial( self.paletteColor , 'white' ))
                        pm.button( label = '' , bgc = [0,0,0] , h = width/10 , c = partial( self.paletteColor , 'black' ))
                        pm.button( label = '' , h = width/10 , c = partial( self.paletteColor , 'none' ))
            
            transformationframeLayout = pm.frameLayout ( label = 'Transformation Tools' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            with transformationframeLayout :
                transformationLayout = pm.rowColumnLayout ( w = width , nc = 3 , columnWidth = [ ( 1 , width/3 ) , ( 2 , width/3 ) , ( 3 , width/3 ) ] )
                with transformationLayout :
                    pm.button( label = 'Freeze' , h = 25 , c = self.freeze )
                    pm.button( label = 'Reset' , h = 25 , c = self.reset )
                    pm.button( label = 'Center' , h = 25 , c = self.centerPivot )
            
            additionframeLayout = pm.frameLayout ( label = 'Addition Tools' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            with additionframeLayout :
                additionLayout = pm.rowColumnLayout ( w = width , nc = 3 , columnWidth = [ ( 1 , width/3 ) , ( 2 , width/3 ) , ( 3 , width/3 ) ] )
                with additionLayout :
                    pm.button( label = 'Joint' , h = 25 , c = self.addJoint )
                    pm.button( label = 'zGrp' , h = 25 , c = self.addZroGrp )
                    pm.button( label = 'Gmbl' , h = 25 , c = self.addGimbal )
            
            snapframeLayout = pm.frameLayout ( label = 'Snap Tools' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            with snapframeLayout :
                snapLayout = pm.rowColumnLayout ( w = width , nc = 4 , columnWidth = [ ( 1 , width/4 ) , ( 2 , width/4 ) , ( 3 , width/4 ) , ( 4 , width/4 )] )
                with snapLayout :
                    pm.button( label = 'Translate' , h = 25 , c = self.snapTranslate )
                    pm.button( label = 'Rotate' , h = 25 , c = self.snapRotate )
                    pm.button( label = 'Scale' , h = 25 , c = self.snapScale )
                    pm.button( label = 'All' , h = 25 , c = self.snapAll )
            
            consframeLayout = pm.frameLayout ( label = 'Constraint Tools' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            with consframeLayout :
                constraintLayout = pm.rowColumnLayout ( w = width , nc = 4 , columnWidth = [ ( 1 , width/4 ) , ( 2 , width/4 ) , ( 3 , width/4 ) , ( 4 , width/4 ) ] )
                with constraintLayout :
                    pm.button( label = 'Point' , h = 25 , c = self.pointConstraint )
                    pm.button( label = 'Orient' , h = 25 , c = self.orientConstraint )
                    pm.button( label = 'Parent' , h = 25 , c = self.parentConstraint )
                    pm.button( label = 'Scale' , h = 25 , c = self.scaleConstraint )
            
            attrframeLayout = pm.frameLayout ( label = 'Attribute Tools' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            with attrframeLayout :
                attrLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [ ( 1 , width-2 ) ] )
                with attrLayout :
                    pm.button( 'attrBut' , label = 'Lock and Hide' , h = 35 , c = self.lockHideAttr )
                    pm.popupMenu()
                    labels = ( 'Lock and Hide' , 'Lock' , 'Hide' )
                    
                    for label in labels :
                        mc.menuItem( l = label , c = partial( self.changeButtonAttr , label ))

                    pm.separator( h = 2 , vis = False )

                    paletteLayout = pm.rowColumnLayout ( w = width , nc = 10 , columnWidth = [ ( 1 , width/10 ),( 2 , width/10 ),( 3 , width/10 ),( 4 , width/10 ),( 5 , width/10 ),
                                                                                              ( 6 , width/10 ),( 7 , width/10 ),( 8 , width/10 ),( 9 , width/10 ),( 10 , width/10 )] )
                    
                    listLabelAttrs = ('tx','ty','tz','rx','ry','rz','sx','sy','sz','v')
                    for listLabelAttr in listLabelAttrs :
                        with paletteLayout :
                            self.makeButtonAttrColor( listLabelAttr )
            
            toggleframeLayout = pm.frameLayout ( label = 'Toggle Tools' , collapsable = True , collapse = False , bgc = ( 0 , 0 , 0 ))
            with toggleframeLayout :
                toggleLayout = pm.rowColumnLayout ( w = width , nc = 3 , columnWidth = [ ( 1 , width/3 ) , ( 2 , width/3 ) , ( 3 , width/3 ) ] )
                with toggleLayout :
                    pm.button( label = 'Poly' , bgc = [0,0.5,0] , h = 25 , c = self.pointConstraint )
                    pm.button( label = 'Curve' , bgc = [0,0.5,0] , h = 25 , c = self.orientConstraint )
                    pm.button( label = 'Joint' , bgc = [0,0.5,0] , h = 25 , c = self.parentConstraint )
            
        pm.showWindow( window )

    def changeButtonCtrl( self , label = '' , arg = None ) :
        mc.button( 'ctrlBut' , e = True , l = label )

    def changeButtonAttr( self , label = '' , arg = None ) :
        mc.button( 'attrBut' , e = True , l = label )

    def makeButtonAttrColor( self , label = '' , arg = None ) :
        def changeButtonAttrColor( *args ) :
            val = pm.button( label , q = True , bgc = True )
            if val[1] == 0 :
                pm.button( label , e = True , bgc = [0,0.5,0] )
            else :
                pm.button( label , e = True , bgc = [0.5,0,0] )
    
        pm.button( label , label = '%s' %label.upper() , bgc = [0.5,0,0] , h = 224/10 , c = changeButtonAttrColor )
    
    def createCurve( self , arg = None ) :
        sels = pm.ls( sl = True )
        shape = mc.button( 'ctrlBut' , q = True , l = True )

        if sels :
            for sel in sels :
                ctrl = rt.rigCtrl( sel , shape , 'none' )
        else :
            ctrl = rt.createCtrl( 'curve1' , shape , 'none' )

        pm.select( ctrl )
    
    def paletteColor( self , col = '' , arg = None ) :
        sels = pm.ls( sl = True )
        for sel in sels :
            obj = core.Dag(sel)
            obj.setColor( col )

    def freeze( self , arg = None ) :
        sels = pm.ls( sl = True )
        pm.makeIdentity( sels , a = True )

    def reset( self , arg = None ) :
        sels = pm.ls( sl = True )
        pm.makeIdentity( sels , a = False , t = 1 , r = 1 , s = 1 )

    def centerPivot( self , arg = None ) :
        sels = pm.ls( sl = True )
        pm.xform( sels , cpc = True )

    def addJoint( self , arg = None ) :
        sels = pm.ls( sl = True )
        if sels :
            for sel in sels :
                if '_' in sel :
                    prefix = sel.split( '_' )[0]
                    suffix = sel.replace( prefix , '' )
                else :
                    suffix = sel
                jnt = rt.createJnt( '%s_Jnt' %suffix , sel )
        else :
            jnt = pm.createNode( 'joint' )

        pm.select( jnt )

    def addZroGrp( self , arg = None ) :
        sels = pm.ls( sl = True )
        for sel in sels :
            zGrp = rt.addGrp( sel )
    
    def addGimbal( self , arg = None ) :
        sels = mc.ls( sl = True )
        for sel in sels :
            gmbl = rt.addGimbal( sel )
    
    def snapTranslate( self , arg = None ) :
        sels = mc.ls( sl = True )
        mc.delete(mc.pointConstraint( sels , mo = False ))
        mc.select( sels[0] )
    
    def snapRotate( self , arg = None ) :
        sels = mc.ls( sl = True )
        mc.delete(mc.orientConstraint( sels , mo = False ))
        mc.select( sels[0] )
    
    def snapScale( self , arg = None ) :
        sels = mc.ls( sl = True )
        mc.delete(mc.scaleConstraint( sels , mo = False ))
        mc.select( sels[0] )
    
    def snapAll( self , arg = None ) :
        sels = mc.ls( sl = True )
        mc.delete(mc.parentConstraint( sels , mo = False ))
        mc.select( sels[0] )
    
    def pointConstraint( self , arg = None ) :
        sels = mc.ls( sl = True )
        mc.pointConstraint( sels[0] , sels[1] , mo = True )
        mc.select( sels[0] , sels[1] )
    
    def orientConstraint( self , arg = None ) :
        sels = mc.ls( sl = True )
        mc.orientConstraint( sels[0] , sels[1] , mo = True )
        mc.select( sels[0] , sels[1] )
    
    def scaleConstraint( self , arg = None ) :
        sels = mc.ls( sl = True )
        mc.scaleConstraint( sels[0] , sels[1] , mo = True )
        mc.select( sels[0] , sels[1] )
    
    def parentConstraint( self , arg = None ) :
        sels = mc.ls( sl = True )
        mc.parentConstraint( sels[0] , sels[1] , mo = True )
        mc.select( sels[0] , sels[1] )

    def lockHideAttr( self , arg = None ) :
        sels = mc.ls( sl = True )

        label = pm.button( 'attrBut' , q = True , label = True )

        listAttrButs = ( 'tx' , 'ty' , 'tz' ,
                         'sx' , 'sy' , 'sz' ,
                         'rx' , 'ry' , 'rz' ,
                         'v' )

        for each in listAttrButs :
            val = pm.button( each , q = True , bgc = True )

            for sel in sels :
                if not val[1] == 0 :
                    if 'Lock' in label :
                        pm.setAttr( '%s.%s' %(sel,each) , l = False )
                    if 'Hide' in label :
                        pm.setAttr( '%s.%s' %(sel,each) , k = True , cb = True )
                else :
                    if 'Lock' in label :
                        pm.setAttr( '%s.%s' %(sel,each) , l = True )
                    if 'Hide' in label :
                        pm.setAttr( '%s.%s' %(sel,each) , k = False , cb = False )