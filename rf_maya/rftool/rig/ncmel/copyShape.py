import maya.cmds as mc
import maya.mel as mm

def copyShapeAll() :
    
    sel = mc.ls( sl = True )
    
    grpA = mc.listRelatives( sel[0] , f = True , ad = True , type = 'transform' )
    grpB = mc.listRelatives( sel[1] , f = True , ad = True , type = 'transform' )
    
    if grpA :
        for i in range(len(grpA)) :
            if not mc.listRelatives( grpA[i] , s = True ) : continue
            mc.select( grpA[i] , grpB[i] )
            mm.eval( 'copyShape ;' )
    else :
        mm.eval( 'copyShape ;' )
    
    mc.select( sel[0] , sel[1] )