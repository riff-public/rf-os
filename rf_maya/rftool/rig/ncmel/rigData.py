import os
import re
import pickle

import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm

'''
def getDataFld():

    scnPath = os.path.normpath(mc.file(q=True, l=True)[0])
    fldPath = os.path.split(scnPath)[0]

    dataFld = os.path.join(fldPath, 'rigData')

    if not os.path.exists(dataFld):
        os.mkdir(dataFld)

    return dataFld
'''

def getDataFld():
    scnPath = os.path.normpath( mc.file( q = True , l = True )[0])

    tmpAry = scnPath.split( '\\' )
    tmpAry[3] = 'publ'
    tmpAry[-4] = 'data'
    
    dataFld = '\\'.join( tmpAry[0:-3] )
    
    if not os.path.isdir( dataFld ) :
        os.mkdir( dataFld )
    
    return dataFld

def writeAttr( obj = '' , flPath = '' ):
    shapes = mc.listRelatives( obj , shapes = True )[0]
    
    exception = [ 'controlPoints.xValue' ,
                  'controlPoints.yValue' ,
                  'controlPoints.zValue' ,
                  'colorSet.clamped' ,
                  'colorSet.representation' ]
    
    aDct = {}
                    
    for each in ( obj , shapes ) :
        attrs = mc.listAttr( each , keyable = True , s = True , unlocked = True )
        
        if attrs :
            aDctAttr = {}
            
            ary = each.split( ':' )
            nm = ary[-1]
            nmsp = ':'.join( ary[0:-1] )
            aDctAttr['namespace'] = nmsp
            
            for attr in attrs :
                if not attr in exception :
                    aDctAttr[attr] = mc.getAttr( '%s.%s' %( each , attr ))
                    
            aDct[nm] = aDctAttr
        
        flObj = open(flPath, 'w')
        pickle.dump(aDct, flObj)
        flObj.close()

def writeSelectedAttr():
    fld = getDataFld()
    dataFld = os.path.join( fld , 'ctrlAttribute' )
    
    if not os.path.isdir( dataFld ) :
        os.mkdir( dataFld )
        
    suffix = '_Attribute'
    
    for sel in mc.ls( sl = True ):
        flNmExt = '%s%s.dat' % (sel.split(':')[-1], suffix)
        flPath = os.path.join(dataFld, flNmExt)
        writeAttr(sel, flPath)

    mc.confirmDialog( title = 'Progress' , message = 'Exporting attribute is done.' )
    print '\n%s' %dataFld

def writeAllCtrlAttr():
    all = mc.ls( ap = True )

    ctrlList = []
    ndTypeList = [ 'transform' , 'joint' ]

    for ctrl in  all :
        if '_Ctrl' in ctrl :
            if mc.nodeType( ctrl ) in ndTypeList :
                ctrlList.append( ctrl )
    
    if ctrlList :
        mc.select( ctrlList )
        writeSelectedAttr()

    mc.select( cl = True )

def readAttr( obj = '' , flPath = '' ):
    flObj = open(flPath, 'r')
    aDct = pickle.load(flObj)
    flObj.close()
    
    shapes = mc.listRelatives( obj , shapes = True )[0]
    
    objNmsp = ''
    objNm = obj
    shpNm = shapes
    
    if ':' in obj :
        objAry = obj.split( ':' )
        objNm = objAry[-1]
        objNmsp = ':'.join( objAry[0:-1] ) + ':'
        
        shpAry = shapes.split( ':' )
        shpNm = shpAry[-1]
        
    for each in ( objNm , shpNm ):
        objs = aDct[each]
        keys = objs.keys()
        
        for key in keys :
            if not key == 'namespace' :
                ctrl = '%s%s.%s' %(objNmsp,each,key)

                if not mc.listConnections( ctrl ) :
                    mc.setAttr( ctrl , objs[key] )
                else :
                    print 'Cannot set value. %s already connections.' %ctrl

def readSelectedAttr():
    dataFld = getDataFld()
    sels =  mc.ls( sl = True )
    
    for sel in sels:
        selNm = sel
        selNmsp = ''
        
        if ':' in sel :
            selAry = sel.split( ':' )
            selNm = selAry[-1]
            selNmsp = ':'.join( selAry[0:-1] ) + ':'
            
        flNm = '%s_Attribute.dat' %selNm
        
        try:
            readAttr( sel , os.path.join( dataFld , 'ctrlAttribute' , flNm ))
            print 'Importing %s done.' %flNm
        except:
            print 'Cannot find attribute file for %s' %selNm
            
    mc.confirmDialog( title = 'Progress' , message = 'Importing attribute is done.')

def readAllCtrlAttr():
    all = mc.ls( ap = True )

    ctrlList = []
    ndTypeList = [ 'transform' , 'joint' ]

    for ctrl in  all :
        if '_Ctrl' in ctrl :
            if mc.nodeType( ctrl ) in ndTypeList :
                ctrlList.append( ctrl )
    
    if ctrlList :
        mc.select( ctrlList )
        readSelectedAttr()

    mc.select( cl = True )
    
def writeWeight( geo = '' , flPath = '' ):
    shapes = mc.listRelatives( '%s' %geo , shapes = True )

    if shapes :
        skn = mm.eval('findRelatedSkinCluster("%s")' % geo)

        if skn:
            sknMeth = mc.getAttr('%s.skinningMethod' % skn)
            infs = mc.skinCluster(skn, q = True, inf = True)
            sknSet = mc.listConnections('%s.message' % skn, d=True, s=False)[0]
            
            flObj = open(flPath, 'w')
            wDct = {}

            wDct['influences'] = infs
            wDct['name'] = skn
            wDct['set'] = sknSet
            wDct['skinningMethod'] = sknMeth

            for ix in xrange(mc.polyEvaluate(geo, v = True)):
                currVtx = '%s.vtx[%d]' % (geo, ix)
                skinVal = mc.skinPercent(skn, currVtx, q = True, v = True)
                wDct[ix] = skinVal

            pickle.dump(wDct, flObj)
            flObj.close()
        else:
            print '%s has no related skinCluster node.' % geo

def writeSelectedWeight():
    fld = getDataFld()
    dataFld = os.path.join( fld , 'skinWeight' )

    if not os.path.isdir( dataFld ) :
        os.mkdir( dataFld )
        
    suffix = '_SkinWeight'
    for sel in mc.ls(sl=True):
        
        flNmExt = '%s%s.dat' % (sel.split(':')[-1], suffix)
        flPath = os.path.join(dataFld, flNmExt)
        writeWeight(sel, flPath)

    mc.confirmDialog( title = 'Progress' , message = 'Exporting weight is done.' )
    print '\n%s' %dataFld

def readWeight( geo = '' , flPath = '' , searchFor = '' , replaceWith = '' , prefix = '' , suffix = '' ):
    print 'Loading %s' % flPath
    flObj = open(flPath, 'r')
    wDct = pickle.load(flObj)
    flObj.close()
    
    # infs = wDct['influences']
    infs = []
    
    for oInf in wDct['influences']:
        if searchFor:
            oInf = oInf.replace(searchFor, replaceWith)

        currInf = '%s%s%s' % (prefix, oInf, suffix)

        infs.append(currInf)
    
    for inf in infs:
        if not mc.objExists(inf):
            print 'Cannot find %s ' % inf

    oSkn = mm.eval('findRelatedSkinCluster "%s"' % geo)
    if oSkn:
        mc.skinCluster(oSkn, e = True, ub = True)

    tmpSkn = mc.skinCluster(infs, geo, tsb=True)[0]
    skn = mc.rename(tmpSkn, wDct['name'])
    mc.setAttr('%s.skinningMethod' % skn, wDct['skinningMethod'])

    sknSet = mc.listConnections('%s.message' % skn, d=True, s=False)[0]
    mc.rename(sknSet, wDct['set'])
    
    for inf in infs:
        mc.setAttr('%s.liw' % inf, False)
    
    mc.setAttr('%s.normalizeWeights' % skn, False)
    mc.skinPercent(skn, geo, nrm=False, prw=100)
    mc.setAttr('%s.normalizeWeights' % skn, True)
    
    vtxNo = mc.polyEvaluate(geo, v = True)
    
    for ix in xrange(vtxNo):        
        for iy in xrange(len(infs)):
            wVal = wDct[ix][iy]
            if wVal:
                wlAttr = '%s.weightList[%s].weights[%s]' % (skn, ix, iy)
                mc.setAttr(wlAttr, wVal)
        
        # Percent calculation
        if ix == (vtxNo - 1):
            print '100%% done.'
        else:
            prePrcnt = 0
            if ix > 0:
                prePrcnt = int((float(ix - 1) / vtxNo) * 100.00)
            
            prcnt = int((float(ix) / vtxNo) * 100.00)

            if not prcnt == prePrcnt:
                print '%s%% done.' % str(prcnt)

def readSelectedWeight( searchFor ='' , replaceWith ='' , prefix ='' , suffix ='' ):
    sels =  mc.ls(sl=True)
    for sel in sels:
        dataFld = getDataFld()
        flNm = '%s_SkinWeight.dat' % sel
        
        try:
            print 'Importing %s.' % sel

            readWeight(sel, os.path.join(dataFld , 'skinWeight' , flNm), searchFor, replaceWith, prefix, suffix)
            print 'Importing %s done.' % flNm
        except:
            print 'Cannot find weight file for %s' % sel
    
    mc.select(sels)
    mc.confirmDialog( title = 'Progress' , message = 'Importing weight is done.')

def writeCtrlShape( fn = 'ctrlShape.dat' ):
    flPath = os.path.join(getDataFld(), fn)
    flObj = open(flPath, 'w')
    
    ctrlDict = {}
    
    for ctrl in mc.ls('*Ctrl'):
        
        shapes = mc.listRelatives(ctrl, s=True)

        if not shapes: continue # Continue if current controller has no shape.
        
        if mc.nodeType(shapes[0]) == 'nurbsCurve':
            
            cv = mc.getAttr('%s.spans' % shapes[0]) + mc.getAttr('%s.degree' % shapes[0])
        
            for ix in range(0, cv):
                cvName = '%s.cv[%s]' % (shapes[0], str(ix))
                ctrlDict[cvName] = mc.xform(cvName, q = True, os = True, t = True)

            # Write color property
            if mc.getAttr('%s.overrideEnabled' % shapes[0]):
                colVal = mc.getAttr('%s.overrideColor' % shapes[0])
                ctrlDict[shapes[0]] = colVal
        
    pickle.dump(ctrlDict, flObj)
    flObj.close()

def readCtrlShape( fn = 'ctrlShape.dat' , searchFor ='' , replaceWith = '' , prefix = '', suffix ='' ):
    flPath = os.path.join(getDataFld(), fn)
    flObj = open(flPath, 'r')
    ctrlDict = pickle.load(flObj)
    flObj.close()

    for key in ctrlDict.keys():

        curr = '%s%s%s' % (prefix, key.replace(searchFor, replaceWith), suffix)

        if '.' in curr:
            # If current is cv, read the position.
            if mc.objExists(curr):
                mc.xform(curr, os=True, t=ctrlDict[curr])
        else:
            # If current is object, read the color.
            if mc.objExists(curr):
                mc.setAttr('%s.overrideEnabled' % curr, 1)
                mc.setAttr('%s.overrideColor' % curr, ctrlDict[curr])

def writeSelectedHierarchy():
    suffix = '_Skel'
    sel = mc.ls(sl=True)[0]
    flNm = sel

    if ':' in sel:
        flNm = sel.split(':')[-1]

    dataFld = getDataFld()
    flNmExt = '%s%s.dat' % ( flNm , suffix )
    
    flPath = '%s\\%s' % ( dataFld , flNmExt )
    writeHierarchy( root = sel , flPath = flPath )
    
    mc.confirmDialog( title = 'Progress' , message = 'Exporting hierarchy is done.' )
    print flPath

def writeHierarchy( root = '' , flPath = '' ):
    skelDict = {}
    infTypes = ('joint', 'transform')
        
    if ':' in root :
        sp = ':'
    else :
        sp = '|'

    rootPos = mc.xform(root, q=True, t=True)
    rootOri = mc.xform(root, q=True, ro=True)

    rootInfo = {}
    rootInfo['name'] = root.split(sp)[-1]
    rootInfo['type'] = mc.nodeType(root)
    rootInfo['rotateOrder'] = mc.getAttr('%s.rotateOrder' %root)
    rootInfo['pos'] = rootPos
    rootInfo['ori'] = rootOri

    skelDict[0] = rootInfo

    for ix, chd in enumerate(sorted(mc.listRelatives(root, ad=True, f=True))):
        type_ = mc.nodeType(chd)
        
        if not type_ in infTypes: continue
        
        if ':' in chd :
            sp = ':'
        else :
            sp = '|'

        infoDict = {}
        infoDict['name'] = chd.split(sp)[-1]
        infoDict['type'] = type_
        infoDict['rotateOrder'] = mc.getAttr('%s.rotateOrder' %chd)
        infoDict['pos'] = mc.xform(chd, q=True, t=True, ws=True)
        infoDict['ori'] = mc.xform(chd, q=True, ro=True, ws=True)
        infoDict['parent'] = mc.listRelatives(chd, p=True, f=True)[0].split(sp)[-1]
        
        if type_ == 'joint' :
            infoDict['radius'] = mc.getAttr('%s.radius' %chd)
        
        skelDict[ix+1] = infoDict

    flObj = open(flPath, 'w')
    pickle.dump(skelDict, flObj)
    flObj.close()

def readHierarchy( flPath = '' ):
    print 'Loading %s' % flPath
    flObj = open(flPath, 'r')
    skelDict = pickle.load(flObj)
    flObj.close()

    for key in sorted(skelDict.keys()):
        
        obj = mc.createNode(skelDict[key]['type'], n=skelDict[key]['name'])
        mc.setAttr('%s.rotateOrder'%obj, skelDict[key]['rotateOrder'])
        mc.xform(obj, t=skelDict[key]['pos'], ws=True)
        mc.xform(obj, ro=skelDict[key]['ori'], ws=True)
        
        if key == 0:
            continue
        
        if skelDict[key]['type'] == 'joint':
            mc.makeIdentity(obj, a=True, r=True, s=True)
            mc.setAttr('%s.radius'%obj, skelDict[key]['radius'])
        
        mc.parent(obj, skelDict[key]['parent'])
        
    # Find parents for the ribbon joints.
    for obj in mc.listRelatives(skelDict[0]['name']):
        
        if 'Spine' in obj :
            mc.parent( obj , 'Root_Jnt' )
            spine = mc.listRelatives( obj ) 
            for i in range( len ( spine )):
                if not  i == 0 :
                    mc.parent( spine[i] , spine[i-1] )
        else :
            exp = r'(.+)RbnSkin(.+)Grp'
            reObj = re.match( exp , obj )
    
            if not reObj:
                continue
    
            currPar = '%s%sJnt' % (reObj.group(1), reObj.group(2))
            if mc.objExists(currPar):
                mc.parent(obj, currPar)
    
    mc.select( skelDict[0]['name'] )
                
def buildSkelGrp():
    dataFld = getDataFld()  
    flPath = os.path.join(dataFld, 'Skin_Grp_Skel.dat')
    readHierarchy(flPath=flPath)