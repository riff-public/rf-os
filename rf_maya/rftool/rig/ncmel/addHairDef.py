import maya.cmds as mc

def mutipleAddHairDef( ctrl = 'Hair_Ctrl' ):
    
    sels = mc.ls( sl = True )

    listRandom = ( 0.57 , 0.92 , 0.72 , 0.48 , 1 , 0.88 , 0.64 , 0.73 , 0.82 )
    
    randIndex = 0
    
    for sel in sels :
        name = sel.split('Geo')[0]
        mc.select( sel )
        addHairDef( name , ctrl , listRandom[randIndex] )
        
        if randIndex == 8 :
            randIndex = 0
        else :
            randIndex+=1
        
def addHairDef( name = 'HairAA' , masterCtrl = '' , randomValue = 1 ):

    # MasterCtrl
    attrList = ( 'roll' , 'wave' , 'offset' , 'wavelength' , 'amplitude' , 'bend' , 'curvature' )

    for attr in attrList :
        if not mc.objExists( '%s.%s' %(masterCtrl,attr) ) :
            mc.addAttr( masterCtrl , ln = attr , at = 'float' , k = True )

    mc.setAttr( '%s.amplitude' %masterCtrl , 0.1 )
    mc.setAttr( '%s.wavelength' %masterCtrl , 1 )

    sel = mc.ls( sl = True )
    defGrp = mc.createNode( 'transform' , n = '%sDef_Grp' %name )
    mc.delete(mc.parentConstraint( '%s1_TmpJnt' %name , defGrp , mo = False ))
    mc.delete(mc.aimConstraint( '%s4_TmpJnt' %name , defGrp , mo = False , aimVector = (0,-1,0) , upVector = (0,0,1)))

    bendDef , bendHandle = mc.nonLinear( sel , type = 'bend' )
    sineDef , sineHandle = mc.nonLinear( sel , type = 'sine' )

    mc.parent( bendHandle , sineHandle , defGrp )
    resetTransform( bendHandle )
    resetTransform( sineHandle )

    # ConnectAttribute
    mc.connectAttr( '%s.amplitude' %masterCtrl , '%s.amplitude' %sineDef ) 
    mc.connectAttr( '%s.wavelength' %masterCtrl , '%s.wavelength' %sineDef ) 
    
    # Bend
    mc.setAttr( '%s.lowBound' %bendDef , -3 )
    mc.setAttr( '%s.highBound' %bendDef , 0 )
    mc.connectAttr( '%s.curvature' %masterCtrl , '%s.curvature' %bendDef ) 

    # Sine
    mc.setAttr( '%s.lowBound' %sineDef , -3 )
    mc.setAttr( '%s.highBound' %sineDef , 0 )
    mc.setAttr( '%s.dropoff' %sineDef , -1 )

    # Random Node
    # OffSet
    mdlOffset = mc.createNode( 'multDoubleLinear' , n = '%sSine_Mdl' %name )
    mc.connectAttr( '%s.offset' %masterCtrl , '%s.input1' %mdlOffset )
    mc.setAttr( '%s.input2' %mdlOffset , randomValue )
    mc.connectAttr( '%s.output' %mdlOffset , '%s.offset' %sineDef )
    
    # Roll
    mdlRoll = mc.createNode( 'multDoubleLinear' , n = '%sRoll_Mdl' %name )
    mc.connectAttr( '%s.roll' %masterCtrl , '%s.input1' %mdlRoll )
    mc.setAttr( '%s.input2' %mdlRoll , randomValue )
    mc.connectAttr( '%s.output' %mdlRoll , '%s.ry' %bendHandle )
    mc.connectAttr( '%s.output' %mdlRoll , '%s.ry' %sineHandle )
    
    # Rename
    mc.rename( sineDef , '%sSine_Def' %name )
    mc.rename( sineHandle , '%sSineHandle_Def' %name )
    mc.rename( bendDef , '%sBend_Def' %name )
    mc.rename( bendHandle , '%sBendHandle_Def' %name )

def resetTransform( obj):
    listAttrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' )
    
    for attr in listAttrs :
        mc.setAttr( '%s.%s' %(obj,attr) , 0 )