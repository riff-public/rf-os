import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
from rf_utils.context import context_info
reload(context_info)
reload( core )
reload( rt )

from rf_maya.rftool.rig.rigScript import core
reload(core)

# -------------------------------------------------------------------------------------------------------------
#
#  MAIN RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , assetName = '' , size = 1 ):
        
        if assetName :
            self.assetName = '%s' %assetName
        else:
            self.assetName = 'Rig'

        #-- Create Main Group
        self.assetGrp = rt.createNode( 'transform' , '%s_Grp' %self.assetName )
        self.geoGrp = rt.createNode( 'transform' , 'Geo_Grp' )
        self.jntGrp = rt.createNode( 'transform' , 'Jnt_Grp' )
        self.ikhGrp = rt.createNode( 'transform' , 'Ikh_Grp' )
        self.ctrlGrp = rt.createNode( 'transform' , 'Ctrl_Grp' )
        self.mainCtrlGrp = rt.createNode( 'transform' , 'MainCtrl_Grp' )
        self.allMoverGrp = rt.createNode( 'transform' , 'AllMover_Grp' )
        self.facialCtrlGrp = rt.createNode( 'transform' , 'FacialCtrl_Grp' )
        self.addRigCtrlGrp = rt.createNode( 'transform' , 'AddCtrl_Grp' )
        self.skinGrp = rt.createNode( 'transform' , 'Skin_Grp' )
        self.stillGrp = rt.createNode( 'transform' , 'Still_Grp' )
        self.techGrp = rt.createNode( 'transform' , 'TechGeo_Grp' )

        #-- Create Main Controls
        self.allMoverCtrl = rt.createCtrl( 'AllMover_Ctrl' , 'square' , 'yellow' )
        self.offsetCtrl = rt.createCtrl( 'Offset_Ctrl' , 'arrowCross' , 'yellow' )

        #-- Add Tag
        self.allMoverCtrl.addBoolTag( 'lockScale' )

        #-- fix unlock scale for prop type
        asset = context_info.ContextPathInfo()
        assetType = asset.type 
        if assetType == 'prop':
            mc.setAttr ('{}.lockScale'.format(self.allMoverCtrl) ,0 )

        #-- Adjust Shape Controls
        self.allMoverCtrl.rotateShape( 0 , 45 , 0 )
        self.allMoverCtrl.scaleShape( size * 7 )
        self.offsetCtrl.scaleShape( size * 1.5 )

        #-- Adjust Hierarchy
        mc.parent( self.offsetCtrl , self.allMoverCtrl )
        mc.parent( self.allMoverCtrl , self.allMoverGrp )
        mc.parent( self.ctrlGrp , self.skinGrp , self.jntGrp , self.ikhGrp , self.offsetCtrl )
        mc.parent( self.mainCtrlGrp , self.facialCtrlGrp , self.addRigCtrlGrp , self.ctrlGrp )
        mc.parent( self.geoGrp , self.allMoverGrp , self.stillGrp , self.techGrp, self.assetGrp )
        
        #-- Cleanup
        for obj in ( self.assetGrp , self.allMoverGrp , self.geoGrp , self.techGrp, self.jntGrp , self.ikhGrp , self.ctrlGrp , self.skinGrp , self.stillGrp , self.mainCtrlGrp , self.facialCtrlGrp , self.addRigCtrlGrp ) :
            obj.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

        self.assetGrp.lockHideAttrs( 'v' )

        #-- fix unlock scaleX Z for prop type
        asset = context_info.ContextPathInfo()
        assetType = asset.type 
        if assetType == 'prop':
            self.allMoverCtrl.lockHideAttrs( 'v' )
        else:
            #-- Rig process
            self.allMoverCtrl.attr('sy') >> self.allMoverCtrl.attr('sx')
            self.allMoverCtrl.attr('sy') >> self.allMoverCtrl.attr('sz')
            self.allMoverCtrl.lockHideAttrs('sx', 'sz', 'v' )
            
        self.offsetCtrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        self.jntGrp.attr('v').value = 0
        self.ikhGrp.attr('v').value = 0
        self.stillGrp.attr('v').value = 0
        mc.select( cl = True )

# -------------------------------------------------------------------------------------------------------------
#
#  PROP RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Prop( Run ):
    
    def __init__( self , assetName = '' , pars = 'MdGeo_Grp' , size = 1 ):
        
        sels = mc.ls( sl = True )
        if not sels:
            return
        sel = sels[0]

        super( Prop , self ).__init__( assetName , size )

        # col = [ 'pink' , 'cyan' , 'red' , 'green' , 'blue' , 'brown' ]
        self.mdGrp = rt.createNode( 'transform' , 'MdGeo_Grp' )
        self.prGrp = rt.createNode( 'transform' , 'PrGeo_Grp' )

        mc.parent( self.mdGrp , self.prGrp , self.geoGrp )

        # nameSplit = sel.split('_')
        # propName = '_'.join( nameSplit[0:-1] )

        # self.porpCtrl = rt.createCtrl( '%s_Ctrl' %propName , 'square' , col[i%6] )
        # self.porpGmbl = rt.addGimbal( self.porpCtrl )
        # self.porpZro = rt.addGrp( self.porpCtrl )
        # self.porpPars = rt.addGrp( self.porpCtrl , 'Pars' )
        # self.porpZro.snap( sel )

        mc.parentConstraint( self.offsetCtrl , pars , mo = False )
        mc.scaleConstraint( self.offsetCtrl , pars , mo = False )
        # mc.parent( self.porpZro , self.ctrlGrp )
        mc.parent( sel , pars , r=True)

        ## Check Axis and fix Cv of Control
        cn = core.Node()
        cn.createCvAxis()

        mc.select( cl = True )

