from ncmel import core
reload(core)
from ncmel import rigTools
reload(rigTools)
from ncmel import mainRig
reload(mainRig)
from ncmel import rootRig
reload(rootRig)
from ncmel import pelvisRig
reload(pelvisRig)
from ncmel import spineRig
reload(spineRig)
from ncmel import pelvisRig
reload(pelvisRig)
from ncmel import torsoRig
reload(torsoRig)
from ncmel import neckRig
reload(neckRig)
from ncmel import headRig
reload(headRig)
from ncmel import eyeRig
reload(eyeRig)
from ncmel import jawRig
reload(jawRig)
from ncmel import clavicleRig
reload(clavicleRig)
from ncmel import armRig
reload(armRig)
from ncmel import legRig
reload(legRig)
from ncmel import legAnimalRig
reload(legAnimalRig)
from ncmel import fingerRig
reload(fingerRig)
from ncmel import ribbonRig
reload(ribbonRig)
from ncmel import subRig
reload(subRig)
from ncmel import fkRig
reload(fkRig)
from ncmel import tailRig
reload(tailRig)
from ncmel import superRig
reload(superRig)
from ncmel import rigHairDyn
reload(rigHairDyn)

def run( size = 3 ):
    
    print "#############################"
    print "# Generate >> Main Group"
    main = mainRig.Run( 
                                    assetName = '' ,
                                    size      = size
                       )

    print "# Generate >> Root"
    root = rootRig.Run( 
                                    rootTmpJnt = 'Root_TmpJnt' ,
                                    ctrlGrp    =  main.ctrlGrp ,
                                    skinGrp    =  main.skinGrp ,
                                    size       =  size
                       )

    print "# Generate >> Torso"
    torso = torsoRig.Run(   
                                    pelvisTmpJnt = 'Pelvis_TmpJnt' ,
                                    spine1TmpJnt = 'Spine1_TmpJnt' ,
                                    spine2TmpJnt = 'Spine2_TmpJnt' ,
                                    spine3TmpJnt = 'Spine3_TmpJnt' ,
                                    spine4TmpJnt = 'Spine4_TmpJnt' ,
                                    spine5TmpJnt = 'Spine5_TmpJnt' ,
                                    parent       =  root.rootJnt ,
                                    ctrlGrp      =  main.ctrlGrp ,
                                    skinGrp      =  main.skinGrp ,
                                    jntGrp       =  main.jntGrp ,
                                    stillGrp     =  main.stillGrp ,
                                    elem         = '' ,
                                    axis         = 'y' ,
                                    size         =  size
                         )
    
    print "# Generate >> Neck"
    neck = neckRig.Run(    
                                    neckTmpJnt = 'Neck_TmpJnt' ,
                                    headTmpJnt = 'Head_TmpJnt' ,
                                    parent     =  torso.jntDict[-1] ,
                                    ctrlGrp    =  main.ctrlGrp ,
                                    skinGrp    =  main.skinGrp ,
                                    elem       = '' ,
                                    side       = '' ,
                                    ribbon     = True ,
                                    head       = True ,
                                    axis       = 'y' ,
                                    size       = size
                       )
                     
    print "# Generate >> Head"
    head = headRig.Run(    
                                    headTmpJnt      = 'Head_TmpJnt' ,
                                    headEndTmpJnt   = 'HeadEnd_TmpJnt' ,
                                    eyeTrgTmpJnt    = 'EyeTrgt_TmpJnt' ,
                                    eyeLftTrgTmpJnt = 'EyeTrgt_L_TmpJnt' ,
                                    eyeRgtTrgTmpJnt = 'EyeTrgt_R_TmpJnt' ,
                                    eyeLftTmpJnt    = 'Eye_L_TmpJnt' ,
                                    eyeRgtTmpJnt    = 'Eye_R_TmpJnt' ,
                                    jawUpr1TmpJnt   = 'JawUpr1_TmpJnt' ,
                                    jawUprEndTmpJnt = 'JawUprEnd_TmpJnt' ,
                                    jawLwr1TmpJnt   = 'JawLwr1_TmpJnt' ,
                                    jawLwr2TmpJnt   = 'JawLwr2_TmpJnt' ,
                                    jawLwrEndTmpJnt = 'JawLwrEnd_TmpJnt' ,
                                    parent          =  neck.neckEndJnt ,
                                    ctrlGrp         =  main.ctrlGrp ,
                                    skinGrp         =  main.skinGrp ,
                                    elem            = '' ,
                                    side            = '' ,
                                    eyeRig          = False ,
                                    jawRig          = False ,
                                    size            = size 
                       )
                     
    print "# Generate >> Eye"
    eye = eyeRig.Run(    
                                    eyeTrgTmpJnt    = 'EyeTrgt_TmpJnt' ,
                                    eyeLftTrgTmpJnt = 'EyeTrgt_L_TmpJnt' ,
                                    eyeRgtTrgTmpJnt = 'EyeTrgt_R_TmpJnt' ,
                                    eyeLftTmpJnt    = 'Eye_L_TmpJnt' ,
                                    eyeRgtTmpJnt    = 'Eye_R_TmpJnt' ,
                                    parent          =  head.headJnt ,
                                    ctrlGrp         =  main.ctrlGrp ,
                                    skinGrp         =  main.skinGrp ,
                                    elem            = '' ,
                                    side            = '' ,
                                    size            = size 
                       )

    '''
    print "# Adjust >> Eye Scale"
    eyeAdjL = core.Dag(head.eyeAdjLftJnt)
    eyeAdjL.attr('rx').value = 30
    eyeAdjL.attr('ry').value = 30
    eyeAdjL.attr('rz').value = 5
    eyeAdjL.attr('sx').value = 0.6
    eyeAdjL.attr('sy').value = 1.3
    eyeAdjL.attr('sz').value = 0.5
    eyeAdjR = core.Dag(head.eyeAdjRgtJnt)
    eyeAdjR.attr('rx').value = 30
    eyeAdjR.attr('ry').value = -30
    eyeAdjR.attr('rz').value = -5
    eyeAdjR.attr('sx').value = 0.6
    eyeAdjR.attr('sy').value = 1.3
    eyeAdjR.attr('sz').value = 0.5
    '''

    print "# Generate >> Jaw"
    jaw = jawRig.Run(    
                                    jawUpr1TmpJnt   = 'JawUpr1_TmpJnt' ,
                                    jawUprEndTmpJnt = 'JawUprEnd_TmpJnt' ,
                                    jawLwr1TmpJnt   = 'JawLwr1_TmpJnt' ,
                                    jawLwr2TmpJnt   = 'JawLwr2_TmpJnt' ,
                                    jawLwrEndTmpJnt = 'JawLwrEnd_TmpJnt' ,
                                    parent          =  head.headJnt ,
                                    ctrlGrp         =  main.ctrlGrp ,
                                    skinGrp         =  main.skinGrp ,
                                    elem            = '' ,
                                    side            = '' ,
                                    size            = size 
                       )

    '''
    print "# Adjust >> Jaw Move Direction"
    jawAdj = core.Dag(jaw.jawLwr1CtrlShape)
    jawAdj.attr('jawRx').value = -0.5
    jawAdj.attr('jawTy').value = -0.025
    jawAdj.attr('jawTz').value = 0.015
    '''

    print "# Generate >> Clavicle Left"
    clavL = clavicleRig.Run(   
                                    clavTmpJnt  = 'Clav_L_TmpJnt' ,
                                    upArmTmpJnt = 'UpArm_L_TmpJnt' ,
                                    parent      =  torso.jntDict[-1] ,
                                    ctrlGrp     =  main.ctrlGrp ,
                                    jntGrp      =  main.jntGrp ,
                                    skinGrp     =  main.skinGrp ,
                                    elem        = '' ,
                                    side        = 'L' ,
                                    axis        = 'y' ,
                                    size        = size
                            )
    
    print "# Generate >> Clavicle Right"
    clavR = clavicleRig.Run(   
                                    clavTmpJnt  = 'Clav_R_TmpJnt' ,
                                    upArmTmpJnt = 'UpArm_R_TmpJnt' ,
                                    parent      =  torso.jntDict[-1] ,
                                    ctrlGrp     =  main.ctrlGrp ,
                                    jntGrp      =  main.jntGrp ,
                                    skinGrp     =  main.skinGrp ,
                                    elem        = '' ,
                                    side        = 'R' ,
                                    axis        = 'y' ,
                                    size        = size
                            )
     
    print "# Generate >> Arm Left"
    armL = armRig.Run(              upArmTmpJnt   = 'UpArm_L_TmpJnt' ,
                                    forearmTmpJnt = 'Forearm_L_TmpJnt' ,
                                    wristTmpJnt   = 'Wrist_L_TmpJnt' ,
                                    handTmpJnt    = 'Hand_L_TmpJnt' ,
                                    elbowTmpJnt   = 'Elbow_L_TmpJnt' ,
                                    parent        = 'ClavEnd_L_Jnt' , 
                                    ctrlGrp       =  main.ctrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    ribbon        = True ,
                                    size          = size
                    )

    rigTools.addMoreSpace( obj = armL.wristIkLocWor ,
                           spaces = [ ('Shoulder' , armL.wristIkLocWor['localSpace'] ) ,
                                      ('Head', head.headJnt ) ,
                                      ('Chest', torso.jntDict[-1] ) ,
                                      ('Pelvis', torso.pelvisJnt ) ] )
    
    print "# Generate >> Arm Right"
    armR = armRig.Run(              
                                    upArmTmpJnt   = 'UpArm_R_TmpJnt' ,
                                    forearmTmpJnt = 'Forearm_R_TmpJnt' ,
                                    wristTmpJnt   = 'Wrist_R_TmpJnt' ,
                                    handTmpJnt    = 'Hand_R_TmpJnt' ,
                                    elbowTmpJnt   = 'Elbow_R_TmpJnt' ,
                                    parent        = 'ClavEnd_R_Jnt' , 
                                    ctrlGrp       =  main.ctrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    ribbon        = True ,
                                    size          = size
                    )

    rigTools.addMoreSpace( obj = armR.wristIkLocWor ,
                           spaces = [ ('Shoulder' , armR.wristIkLocWor['localSpace'] ) ,
                                      ('Head', head.headJnt ) ,
                                      ('Chest', torso.jntDict[-1] ) ,
                                      ('Pelvis', torso.pelvisJnt ) ] )
    
    '''
    print "# Generate >> Leg Left"
    legL = legAnimalRig.Run(              
                                    upLegTmpJnt   = 'UpLeg_L_TmpJnt' ,
                                    midLegTmpJnt  = 'MidLeg_L_TmpJnt' ,
                                    lowLegTmpJnt  = 'LowLeg_L_TmpJnt' ,
                                    ankleTmpJnt   = 'Ankle_L_TmpJnt' ,
                                    ballTmpJnt    = 'Ball_L_TmpJnt' ,
                                    toeTmpJnt     = 'Toe_L_TmpJnt' ,
                                    heelTmpJnt    = 'Heel_L_TmpJnt' ,
                                    footInTmpJnt  = 'FootIn_L_TmpJnt' ,
                                    footOutTmpJnt = 'FootOut_L_TmpJnt' ,
                                    kneeTmpJnt    = 'Knee_L_TmpJnt' ,
                                    parent        =  torso.pelvisJnt , 
                                    ctrlGrp       =  main.ctrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    ribbon        = True ,
                                    foot          = True ,
                                    size          = size
                    )

    print "# Generate >> Leg Right"
    legR = legAnimalRig.Run(              
                                    upLegTmpJnt   = 'UpLeg_R_TmpJnt' ,
                                    midLegTmpJnt  = 'MidLeg_R_TmpJnt' ,
                                    lowLegTmpJnt  = 'LowLeg_R_TmpJnt' ,
                                    ankleTmpJnt   = 'Ankle_R_TmpJnt' ,
                                    ballTmpJnt    = 'Ball_R_TmpJnt' ,
                                    toeTmpJnt     = 'Toe_R_TmpJnt' ,
                                    heelTmpJnt    = 'Heel_R_TmpJnt' ,
                                    footInTmpJnt  = 'FootIn_R_TmpJnt' ,
                                    footOutTmpJnt = 'FootOut_R_TmpJnt' ,
                                    kneeTmpJnt    = 'Knee_R_TmpJnt' ,
                                    parent        =  torso.pelvisJnt , 
                                    ctrlGrp       =  main.ctrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    ribbon        = True ,
                                    foot          = True ,
                                    size          = size
                    )

    '''
    print "# Generate >> Leg Left"
    legL = legRig.Run(              
                                    upLegTmpJnt   = 'UpLeg_L_TmpJnt' ,
                                    lowLegTmpJnt  = 'LowLeg_L_TmpJnt' ,
                                    ankleTmpJnt   = 'Ankle_L_TmpJnt' ,
                                    ballTmpJnt    = 'Ball_L_TmpJnt' ,
                                    toeTmpJnt     = 'Toe_L_TmpJnt' ,
                                    heelTmpJnt    = 'Heel_L_TmpJnt' ,
                                    footInTmpJnt  = 'FootIn_L_TmpJnt' ,
                                    footOutTmpJnt = 'FootOut_L_TmpJnt' ,
                                    kneeTmpJnt    = 'Knee_L_TmpJnt' ,
                                    parent        =  torso.pelvisJnt , 
                                    ctrlGrp       =  main.ctrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    ribbon        = True ,
                                    foot          = True ,
                                    size          = size
                    )

    rigTools.addMoreSpace( obj = legL.ankleIkLocWor ,
                           spaces = [ ('UpLeg' , legL.ankleIkLocWor['localSpace'] ) ,
                                      ('OffsetCtrl', main.offsetCtrl ) ] )
    
    print "# Generate >> Leg Right"
    legR = legRig.Run(              
                                    upLegTmpJnt   = 'UpLeg_R_TmpJnt' ,
                                    lowLegTmpJnt  = 'LowLeg_R_TmpJnt' ,
                                    ankleTmpJnt   = 'Ankle_R_TmpJnt' ,
                                    ballTmpJnt    = 'Ball_R_TmpJnt' ,
                                    toeTmpJnt     = 'Toe_R_TmpJnt' ,
                                    heelTmpJnt    = 'Heel_R_TmpJnt' ,
                                    footInTmpJnt  = 'FootIn_R_TmpJnt' ,
                                    footOutTmpJnt = 'FootOut_R_TmpJnt' ,
                                    kneeTmpJnt    = 'Knee_R_TmpJnt' ,
                                    parent        =  torso.pelvisJnt , 
                                    ctrlGrp       =  main.ctrlGrp ,
                                    jntGrp        =  main.jntGrp ,
                                    skinGrp       =  main.skinGrp ,
                                    stillGrp      =  main.stillGrp ,
                                    ikhGrp        =  main.ikhGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    ribbon        = True ,
                                    foot          = True ,
                                    size          = size
                    )

    rigTools.addMoreSpace( obj = legR.ankleIkLocWor ,
                           spaces = [ ('UpLeg' , legR.ankleIkLocWor['localSpace'] ) ,
                                      ('OffsetCtrl', main.offsetCtrl ) ] )


    print "# Generate >> Thumb Left"
    fngrThumbL = fingerRig.Run(         
                                    fngr          = 'Thumb' ,
                                    fngrTmpJnt    =['Thumb1_L_TmpJnt' , 
                                                    'Thumb2_L_TmpJnt' , 
                                                    'Thumb3_L_TmpJnt' , 
                                                    'Thumb4_L_TmpJnt' ] ,
                                    parent        =  armL.handJnt , 
                                    armCtrl       =  armL.armCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                             )

    finger = 'Thumb'
    ctrlShape = core.Dag(armL.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -4.5
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8

    
    print "# Generate >> Index Left"
    fngrIndexL = fingerRig.Run(         
                                    fngr          = 'Index' ,
                                    fngrTmpJnt    =['Index1_L_TmpJnt' , 
                                                    'Index2_L_TmpJnt' , 
                                                    'Index3_L_TmpJnt' , 
                                                    'Index4_L_TmpJnt' , 
                                                    'Index5_L_TmpJnt' ] ,
                                    parent        =  armL.handJnt , 
                                    armCtrl       =  armL.armCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                             )

    finger = 'Index'
    ctrlShape = core.Dag(armL.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s2RelaxRx' %finger).value = -1
    ctrlShape.attr('%s3RelaxRx' %finger).value = -1
    ctrlShape.attr('%s4RelaxRx' %finger).value = -1
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = -4
    ctrlShape.attr('%sSpread' %finger).value = -4
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9

    
    print "# Generate >> Middle Left"
    fngrMiddleL = fingerRig.Run(         
                                    fngr          = 'Middle' ,
                                    fngrTmpJnt    =['Middle1_L_TmpJnt' , 
                                                    'Middle2_L_TmpJnt' , 
                                                    'Middle3_L_TmpJnt' , 
                                                    'Middle4_L_TmpJnt' , 
                                                    'Middle5_L_TmpJnt' ] ,
                                    parent        =  armL.handJnt , 
                                    armCtrl       =  armL.armCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                               )

    finger = 'Middle'
    ctrlShape = core.Dag(armL.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
    ctrlShape.attr('%s2RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s4RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9
    

    print "# Generate >> Hand Cup Left"
    handCupL = fingerRig.HandCup( 
                                     handCupTmpJnt = 'HandCup_L_TmpJnt' ,
                                     armCtrl       =  armL.armCtrl ,
                                     parent        =  armL.handJnt ,
                                     elem          = '' ,
                                     side          = 'L' ,
                                     size          = size 
                                )


    print "# Generate >> Ring Left"
    fngrRingL = fingerRig.Run(         
                                    fngr          = 'Ring' ,
                                    fngrTmpJnt    =['Ring1_L_TmpJnt' , 
                                                    'Ring2_L_TmpJnt' , 
                                                    'Ring3_L_TmpJnt' , 
                                                    'Ring4_L_TmpJnt' , 
                                                    'Ring5_L_TmpJnt' ] ,
                                    parent        =  handCupL.handCupJnt , 
                                    armCtrl       =  armL.armCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                               )

    finger = 'Ring'
    ctrlShape = core.Dag(armL.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -1
    ctrlShape.attr('%s2RelaxRx' %finger).value = -5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -5
    ctrlShape.attr('%s4RelaxRx' %finger).value = -5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = 3
    ctrlShape.attr('%sSpread' %finger).value = 3
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9
    

    print "# Generate >> Pinky Left"
    fngrPinkyL = fingerRig.Run(         
                                    fngr          = 'Pinky' ,
                                    fngrTmpJnt    =['Pinky1_L_TmpJnt' , 
                                                    'Pinky2_L_TmpJnt' , 
                                                    'Pinky3_L_TmpJnt' , 
                                                    'Pinky4_L_TmpJnt' , 
                                                    'Pinky5_L_TmpJnt' ] ,
                                    parent        =  handCupL.handCupJnt , 
                                    armCtrl       =  armL.armCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                               )

    finger = 'Pinky'
    ctrlShape = core.Dag(armL.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -2
    ctrlShape.attr('%s2RelaxRx' %finger).value = -7.5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -7.5
    ctrlShape.attr('%s4RelaxRx' %finger).value = -7.5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = 5
    ctrlShape.attr('%sSpread' %finger).value = 5
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9


    print "# Generate >> Thumb Right"
    fngrThumbR = fingerRig.Run(         
                                    fngr          = 'Thumb' ,
                                    fngrTmpJnt    =['Thumb1_R_TmpJnt' , 
                                                    'Thumb2_R_TmpJnt' , 
                                                    'Thumb3_R_TmpJnt' , 
                                                    'Thumb4_R_TmpJnt' ] ,
                                    parent        =  armR.handJnt , 
                                    armCtrl       =  armR.armCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                             )

    finger = 'Thumb'
    ctrlShape = core.Dag(armR.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -4.5
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    

    print "# Generate >> Index Right"
    fngrIndexR = fingerRig.Run(         
                                    fngr          = 'Index' ,
                                    fngrTmpJnt    =['Index1_R_TmpJnt' , 
                                                    'Index2_R_TmpJnt' , 
                                                    'Index3_R_TmpJnt' , 
                                                    'Index4_R_TmpJnt' , 
                                                    'Index5_R_TmpJnt' ] ,
                                    parent        =  armR.handJnt , 
                                    armCtrl       =  armR.armCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                             )

    finger = 'Index'
    ctrlShape = core.Dag(armR.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s2RelaxRx' %finger).value = -1
    ctrlShape.attr('%s3RelaxRx' %finger).value = -1
    ctrlShape.attr('%s4RelaxRx' %finger).value = -1
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = -4
    ctrlShape.attr('%sSpread' %finger).value = -4
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9
    

    print "# Generate >> Middle Right"
    fngrMiddleR = fingerRig.Run(         
                                    fngr          = 'Middle' ,
                                    fngrTmpJnt    =['Middle1_R_TmpJnt' , 
                                                    'Middle2_R_TmpJnt' , 
                                                    'Middle3_R_TmpJnt' , 
                                                    'Middle4_R_TmpJnt' , 
                                                    'Middle5_R_TmpJnt' ] ,
                                    parent        =  armR.handJnt , 
                                    armCtrl       =  armR.armCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                               )

    finger = 'Middle'
    ctrlShape = core.Dag(armR.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
    ctrlShape.attr('%s2RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s4RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9
    

    print "# Generate >> Hand Cup Right"
    handCupR = fingerRig.HandCup( 
                                     handCupTmpJnt = 'HandCup_R_TmpJnt' ,
                                     armCtrl       =  armR.armCtrl ,
                                     parent        =  armR.handJnt ,
                                     elem          = '' ,
                                     side          = 'R' ,
                                     size          = size 
                                )
    

    print "# Generate >> Ring Right"
    fngrRingR = fingerRig.Run(         
                                    fngr          = 'Ring' ,
                                    fngrTmpJnt    =['Ring1_R_TmpJnt' , 
                                                    'Ring2_R_TmpJnt' , 
                                                    'Ring3_R_TmpJnt' , 
                                                    'Ring4_R_TmpJnt' , 
                                                    'Ring5_R_TmpJnt' ] ,
                                    parent        =  handCupR.handCupJnt , 
                                    armCtrl       =  armR.armCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                               )

    finger = 'Ring'
    ctrlShape = core.Dag(armR.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -1
    ctrlShape.attr('%s2RelaxRx' %finger).value = -5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -5
    ctrlShape.attr('%s4RelaxRx' %finger).value = -5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = 3
    ctrlShape.attr('%sSpread' %finger).value = 3
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9
    

    print "# Generate >> Pinky Right"
    fngrPinkyR = fingerRig.Run(         
                                    fngr          = 'Pinky' ,
                                    fngrTmpJnt    =['Pinky1_R_TmpJnt' , 
                                                    'Pinky2_R_TmpJnt' , 
                                                    'Pinky3_R_TmpJnt' , 
                                                    'Pinky4_R_TmpJnt' , 
                                                    'Pinky5_R_TmpJnt' ] ,
                                    parent        =  handCupR.handCupJnt , 
                                    armCtrl       =  armR.armCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                               )
    
    finger = 'Pinky'
    ctrlShape = core.Dag(armR.armCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s4FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -2
    ctrlShape.attr('%s2RelaxRx' %finger).value = -7.5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -7.5
    ctrlShape.attr('%s4RelaxRx' %finger).value = -7.5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s4SlideRx' %finger).value = 4.5
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%s4ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = 5
    ctrlShape.attr('%sSpread' %finger).value = 5
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9


    print "# Generate >> Thumb Foot Left"
    fngrThumbFootL = fingerRig.Run(         
                                    fngr          = 'ThumbFoot' ,
                                    fngrTmpJnt    =['ThumbFoot1_L_TmpJnt' , 
                                                    'ThumbFoot2_L_TmpJnt' , 
                                                    'ThumbFoot3_L_TmpJnt' , 
                                                    'ThumbFoot4_L_TmpJnt' ] ,
                                    parent        =  legL.toeJnt , 
                                    armCtrl       =  legL.legCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                             )

    finger = 'ThumbFoot'
    ctrlShape = core.Dag(legL.legCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s2RelaxRx' %finger).value = -1
    ctrlShape.attr('%s3RelaxRx' %finger).value = -1
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = -5
    ctrlShape.attr('%sSpread' %finger).value = -5
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9

    
    print "# Generate >> Index Foot Left"
    fngrIndexFootL = fingerRig.Run(         
                                    fngr          = 'IndexFoot' ,
                                    fngrTmpJnt    =['IndexFoot1_L_TmpJnt' , 
                                                    'IndexFoot2_L_TmpJnt' , 
                                                    'IndexFoot3_L_TmpJnt' , 
                                                    'IndexFoot4_L_TmpJnt' ] ,
                                    parent        =  legL.toeJnt , 
                                    armCtrl       =  legL.legCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                             )

    finger = 'IndexFoot'
    ctrlShape = core.Dag(legL.legCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s2RelaxRx' %finger).value = -1
    ctrlShape.attr('%s3RelaxRx' %finger).value = -1
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = -4
    ctrlShape.attr('%sSpread' %finger).value = -4
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9

    
    print "# Generate >> Middle Foot Left"
    fngrMiddleFootL = fingerRig.Run(         
                                    fngr          = 'MiddleFoot' ,
                                    fngrTmpJnt    =['MiddleFoot1_L_TmpJnt' , 
                                                    'MiddleFoot2_L_TmpJnt' , 
                                                    'MiddleFoot3_L_TmpJnt' , 
                                                    'MiddleFoot4_L_TmpJnt' ] ,
                                    parent        =  legL.toeJnt , 
                                    armCtrl       =  legL.legCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                               )

    finger = 'MiddleFoot'
    ctrlShape = core.Dag(legL.legCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
    ctrlShape.attr('%s2RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9


    print "# Generate >> Ring Foot Left"
    fngrRingFootL = fingerRig.Run(         
                                    fngr          = 'RingFoot' ,
                                    fngrTmpJnt    =['RingFoot1_L_TmpJnt' , 
                                                    'RingFoot2_L_TmpJnt' , 
                                                    'RingFoot3_L_TmpJnt' , 
                                                    'RingFoot4_L_TmpJnt' ] ,
                                    parent        =  legL.toeJnt , 
                                    armCtrl       =  legL.legCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                               )

    finger = 'RingFoot'
    ctrlShape = core.Dag(legL.legCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -1
    ctrlShape.attr('%s2RelaxRx' %finger).value = -5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = 3
    ctrlShape.attr('%sSpread' %finger).value = 3
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9
    

    print "# Generate >> Pinky Foot Left"
    fngrPinkyFootL = fingerRig.Run(         
                                    fngr          = 'PinkyFoot' ,
                                    fngrTmpJnt    =['PinkyFoot1_L_TmpJnt' , 
                                                    'PinkyFoot2_L_TmpJnt' , 
                                                    'PinkyFoot3_L_TmpJnt' , 
                                                    'PinkyFoot4_L_TmpJnt' ] ,
                                    parent        =  legL.toeJnt , 
                                    armCtrl       =  legL.legCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'L' ,
                                    size          = size 
                               )

    finger = 'PinkyFoot'
    ctrlShape = core.Dag(legL.legCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -2
    ctrlShape.attr('%s2RelaxRx' %finger).value = -7.5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -7.5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = 5
    ctrlShape.attr('%sSpread' %finger).value = 5
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9


    print "# Generate >> Thumb Foot Right"
    fngrThumbFootR = fingerRig.Run(         
                                    fngr          = 'ThumbFoot' ,
                                    fngrTmpJnt    =['ThumbFoot1_R_TmpJnt' , 
                                                    'ThumbFoot2_R_TmpJnt' , 
                                                    'ThumbFoot3_R_TmpJnt' , 
                                                    'ThumbFoot4_R_TmpJnt' ] ,
                                    parent        =  legR.toeJnt , 
                                    armCtrl       =  legR.legCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                             )

    finger = 'ThumbFoot'
    ctrlShape = core.Dag(legR.legCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s2RelaxRx' %finger).value = -1
    ctrlShape.attr('%s3RelaxRx' %finger).value = -1
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = -5
    ctrlShape.attr('%sSpread' %finger).value = -5
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9

    
    print "# Generate >> Index Foot Right"
    fngrIndexFootR = fingerRig.Run(         
                                    fngr          = 'IndexFoot' ,
                                    fngrTmpJnt    =['IndexFoot1_R_TmpJnt' , 
                                                    'IndexFoot2_R_TmpJnt' , 
                                                    'IndexFoot3_R_TmpJnt' , 
                                                    'IndexFoot4_R_TmpJnt' ] ,
                                    parent        =  legR.toeJnt , 
                                    armCtrl       =  legR.legCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                             )

    finger = 'IndexFoot'
    ctrlShape = core.Dag(legR.legCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s2RelaxRx' %finger).value = -1
    ctrlShape.attr('%s3RelaxRx' %finger).value = -1
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = -4
    ctrlShape.attr('%sSpread' %finger).value = -4
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9

    
    print "# Generate >> Middle Foot Right"
    fngrMiddleFootR = fingerRig.Run(         
                                    fngr          = 'MiddleFoot' ,
                                    fngrTmpJnt    =['MiddleFoot1_R_TmpJnt' , 
                                                    'MiddleFoot2_R_TmpJnt' , 
                                                    'MiddleFoot3_R_TmpJnt' , 
                                                    'MiddleFoot4_R_TmpJnt' ] ,
                                    parent        =  legR.toeJnt , 
                                    armCtrl       =  legR.legCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                               )

    finger = 'MiddleFoot'
    ctrlShape = core.Dag(legR.legCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -0.5
    ctrlShape.attr('%s2RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -2.5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9


    print "# Generate >> Ring Foot Right"
    fngrRingFootR = fingerRig.Run(         
                                    fngr          = 'RingFoot' ,
                                    fngrTmpJnt    =['RingFoot1_R_TmpJnt' , 
                                                    'RingFoot2_R_TmpJnt' , 
                                                    'RingFoot3_R_TmpJnt' , 
                                                    'RingFoot4_R_TmpJnt' ] ,
                                    parent        =  legR.toeJnt , 
                                    armCtrl       =  legR.legCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                               )

    finger = 'RingFoot'
    ctrlShape = core.Dag(legR.legCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -1
    ctrlShape.attr('%s2RelaxRx' %finger).value = -5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = 3
    ctrlShape.attr('%sSpread' %finger).value = 3
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9
    

    print "# Generate >> Pinky Foot Right"
    fngrPinkyFootR = fingerRig.Run(         
                                    fngr          = 'PinkyFoot' ,
                                    fngrTmpJnt    =['PinkyFoot1_R_TmpJnt' , 
                                                    'PinkyFoot2_R_TmpJnt' , 
                                                    'PinkyFoot3_R_TmpJnt' , 
                                                    'PinkyFoot4_R_TmpJnt' ] ,
                                    parent        =  legR.toeJnt , 
                                    armCtrl       =  legR.legCtrl ,
                                    ctrlGrp       =  main.ctrlGrp ,
                                    elem          = '' ,
                                    side          = 'R' ,
                                    size          = size 
                               )

    finger = 'PinkyFoot'
    ctrlShape = core.Dag(legR.legCtrl.shape)
    ctrlShape.attr('%s2FistRx' %finger).value = -9
    ctrlShape.attr('%s3FistRx' %finger).value = -9
    ctrlShape.attr('%s1RelaxRx' %finger).value = -2
    ctrlShape.attr('%s2RelaxRx' %finger).value = -7.5
    ctrlShape.attr('%s3RelaxRx' %finger).value = -7.5
    ctrlShape.attr('%s2SlideRx' %finger).value = 3.5
    ctrlShape.attr('%s3SlideRx' %finger).value = -8
    ctrlShape.attr('%s1ScruchRx' %finger).value = 1.5
    ctrlShape.attr('%s2ScruchRx' %finger).value = 3.5
    ctrlShape.attr('%s3ScruchRx' %finger).value = -8
    ctrlShape.attr('%sBaseSpread' %finger).value = 5
    ctrlShape.attr('%sSpread' %finger).value = 5
    ctrlShape.attr('%sBaseBreak' %finger).value = -4.5
    ctrlShape.attr('%sBreak' %finger).value = -4.5
    ctrlShape.attr('%sBaseFlex' %finger).value = -9
    ctrlShape.attr('%sFlex' %finger).value = -9

    print "# Generate >> CleanUp"
    rigTools.jntRadius( r = 0.3 )