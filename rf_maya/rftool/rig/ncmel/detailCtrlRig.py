import pymel.core as pm
import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# --------------------------------------------------------------------------------------------
#
# Add Detail Control
#
# description :  - select two edge
#                - run script
#                - duplicate base model to skin detail
#                - copy weight base model to nurb from script
#                - bind skin joint detail to model skin detail
#
# --------------------------------------------------------------------------------------------

def detailCtrlUI( version = '1.0') :
    
    width = 225

    if pm.window( 'DetailCtrlUI' , exists = True ) :
        pm.deleteUI( 'DetailCtrlUI' )
        
    window = pm.window( 'DetailCtrlUI', title = "Detail Control  %s" %version , width = width ,
                         mnb = True , mxb = False , sizeable = True , rtf = True )
    
    layout1 = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = ( 1 , width ))
    with layout1 :
        pm.separator( vis = False , h = 1 )
    
    layout2 = pm.rowColumnLayout ( w = width , nc = 2 , columnWidth = [( 1 , 50 ) , ( 2 , 170 )])
    with layout2 :
        pm.text( label = 'Name :' )
        pm.textField( 'nameTF' , text = ''  )
        pm.text( label = 'Side :' )
        pm.textField( 'sideTF' , text = ''  )
        pm.popupMenu()
        pm.menuItem( l = 'L' , c = "pm.textField( 'sideTF' , e = True , tx = 'L' )" )
        pm.menuItem( l = 'R' , c = "pm.textField( 'sideTF' , e = True , tx = 'R' )" )
        pm.menuItem( l = 'C' , c = "pm.textField( 'sideTF' , e = True , tx = 'C' )" )
        pm.text( label = 'Shape :' )
        pm.textField( 'shapeTF' , text = ''  )
        pm.popupMenu()
        pm.menuItem( l = 'cube' , c = "pm.textField( 'shapeTF' , e = True , tx = 'cube' )" )
        pm.menuItem( l = 'sphere' , c = "pm.textField( 'shapeTF' , e = True , tx = 'sphere' )" )
        pm.menuItem( l = 'square' , c = "pm.textField( 'shapeTF' , e = True , tx = 'square' )" )
        pm.menuItem( l = 'circle' , c = "pm.textField( 'shapeTF' , e = True , tx = 'circle' )" )
    
    layout4 = pm.rowColumnLayout ( w = width , nc = 2 , columnWidth = [( 1 , 50 ) , ( 2 , 170 )])
    with layout4 :
        pm.text( label = 'Mirror :' )
        pm.optionMenu ( 'mirrorOM' , label = '' )
        mc.menuItem ( l = 'ON' )
        mc.menuItem ( l = 'OFF' )
    
    layout5 = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = ( 1 , 220 ))
    with layout5 :
        pm.separator( vis = False , h = 1 )
        pm.button( label = 'Process' , h = 35 , c = runAddDtlCtrlNurb )

    pm.showWindow( window )

def runAddDtlCtrlNurb( *args ) :
    name = pm.textField( 'nameTF' , q = True , tx = True )
    side = pm.textField( 'sideTF' , q = True , tx = True )
    shape = pm.textField( 'shapeTF' , q = True , tx = True )
    mirror = pm.optionMenu( 'mirrorOM' , q = True , v = True )
    
    addDtlCtrlNurb( name = name , side = side , shape = shape , mirror = mirror , charSize = 1 )
    
def addDtlCtrlNurb( name = '' , side = '' , shape = '' , mirror = 'ON' , charSize = 1 ) :
    
    ##-- Info
    # name = name.capitalize()

    if side == 'L' :
        side = '_L_'
        sideMirr = '_R_'
    elif side == 'R' :
        side = '_R_'
        sideMirr = '_L_'
    elif side == '' :
        side = '_'
        mirror = 'OFF'
    else :
        side = '_%s_' %side
        mirror = 'OFF'

    if mirror == 'ON' :
        sides = [ side , sideMirr ]
    else :
        sides = [ side ]

    #-- Rig process
    sel = mc.ls( sl = True )
    
    if sel :
        obj = sel[0].split('.')[0]
        
        for side in sides :
            #-- Nurb
            dtlNrb = mc.loft( sel , ch = False , d = 1 , n = '%sDtl%sNrb' % ( name , side ))[0]
            dtlNrb = core.Dag(dtlNrb)

            if mirror == 'ON' :
                if side == sideMirr :
                    dtlNrb.attr('sx').value = -1

            #-- Controls
            dtlCtrl = rt.createCtrl( '%sDtl%sctrl' %( name , side ) , shape , 'green' , jnt = False )
            dtlZro = rt.addGrp( dtlCtrl )
            dtlOfst = rt.addGrp( dtlCtrl , 'Ofst' )
            
            dtlCtrlShape = core.Dag(dtlCtrl.shape)

            for attr in ( 'parameterU' , 'parameterV' ) :
                dtlCtrlShape.addAttr( attr )
                
            dtlCtrlShape.attr('parameterU').value = 0.5
            dtlCtrlShape.attr('parameterV').value = 0.5

            if mirror == 'ON' :
                if side == sideMirr :
                    dtlZro.attr('sx').value = -1
                    
            #-- Shape
            dtlCtrl.scaleShape( charSize * 0.1 )
                
            #-- PointOnSurfaceInfo
            dtlPosi = rt.createNode( 'pointOnSurfaceInfo' , '%sDtl%sPosi' %( name , side ))
            dtlPosi.attr('turnOnPercentage').value = 1

            dtlPosi.attr('position') >> dtlZro.attr('t')
            dtlNrb.attr('worldSpace[0]') >> dtlPosi.attr('inputSurface')
            dtlCtrlShape.attr('parameterU') >> dtlPosi.attr('parameterU')
            dtlCtrlShape.attr('parameterV') >> dtlPosi.attr('parameterV')
                
            #-- Joint
            dtlJnt = rt.createJnt( '%sDtl%sJnt' %( name , side ))
            dtlJntZro = rt.addGrp( dtlJnt )

            dtlJnt.attr('radius').value = charSize * 0.1
            dtlJntZro.snap( dtlZro )

            if mirror == 'ON' :
                if side == sideMirr :
                    dtlJntZro.attr('sx').value = -1

            dtlCtrl.attr('t') >> dtlJnt.attr('t')
            dtlCtrl.attr('r') >> dtlJnt.attr('r')
            dtlCtrl.attr('s') >> dtlJnt.attr('s')
                
            #-- Counter
            mdv = rt.createNode( 'multiplyDivide' , '%sDtl%sMdv' %( name , side ))
            mdv.attr('i2x').value = -1
            mdv.attr('i2y').value = -1
            mdv.attr('i2z').value = -1

            dtlCtrl.attr('t') >> mdv.attr('i1')
            mdv.attr('o') >> dtlOfst.attr('t')
        
        mc.select( cl = True )