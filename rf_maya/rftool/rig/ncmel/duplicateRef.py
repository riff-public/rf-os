import maya.cmds as mc

def dupOffsetRef(offset=(0,0,0)):
    sel = mc.ls(sl=True) or []
    newObjDup = []
    for i in sel:
        if mc.referenceQuery(i,isNodeReferenced=True):
            nameSpace = str(i).split(':')[0]
            obj       = str(i).split(':')[-1]
            fileName = mc.referenceQuery( sel[0], filename=True)
            load = mc.file(fileName,r=True,namespace=nameSpace,options='v=0')
            newNameSpace = mc.file(load,q=True,namespace=True)
            newObj = '%s:%s' %(newNameSpace,obj)
            mc.delete(mc.pointConstraint(i,newObj,offset=offset))
            mc.delete(mc.orientConstraint(i,newObj))
            newObjDup.append(newObj)
    if newObjDup:
        mc.select(newObjDup) 