import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  EYE RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , eyeTrgTmpJnt     = 'EyeTrgt_TmpJnt' ,
                         eyeLftTrgTmpJnt  = 'EyeTrgt_L_TmpJnt' ,
                         eyeRgtTrgTmpJnt  = 'EyeTrgt_R_TmpJnt' ,
                         eyeLftTmpJnt     = 'Eye_L_TmpJnt' ,
                         eyeRgtTmpJnt     = 'Eye_R_TmpJnt' ,
                         parent           = 'Head_Jnt' ,
                         ctrlGrp          = 'Ctrl_Grp' ,
                         skinGrp          = 'Skin_Grp' ,
                         elem             = '' ,
                         side             = '' ,
                         size             = 1 
                 ):

        ##-- Info
        elem = elem.capitalize()

        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        #-- Create Main Group Eye
        self.eyeCtrlGrp = rt.createNode( 'transform' , 'Eye%sCtrl%sGrp' %( elem , side ))
        mc.parentConstraint( parent , self.eyeCtrlGrp , mo = False )
        mc.scaleConstraint( parent , self.eyeCtrlGrp , mo = False )
    
        #-- Create Joint Eye 
        self.eyeAdjLftJnt = rt.createJnt( 'EyeAdj%sLFT%sJnt' %( elem , side ) , eyeLftTmpJnt )
        self.eyeAdjRgtJnt = rt.createJnt( 'EyeAdj%sRGT%sJnt' %( elem , side ) , eyeRgtTmpJnt )
        self.eyeLftJnt = rt.createJnt( 'Eye%sLFT%sJnt' %( elem , side ) , eyeLftTmpJnt )
        self.eyeRgtJnt = rt.createJnt( 'Eye%sRGT%sJnt' %( elem , side ) , eyeRgtTmpJnt )
        self.eyeLidLftJnt = rt.createJnt( 'EyeLid%sLFT%sJnt' %( elem , side ) , eyeLftTmpJnt )
        self.eyeLidRgtJnt = rt.createJnt( 'EyeLid%sRGT%sJnt' %( elem , side ) , eyeRgtTmpJnt )
        
        mc.parent( self.eyeLftJnt , self.eyeAdjLftJnt )
        mc.parent( self.eyeLidLftJnt , self.eyeLftJnt )
        mc.parent( self.eyeAdjLftJnt , parent )
        mc.parent( self.eyeRgtJnt , self.eyeAdjRgtJnt )
        mc.parent( self.eyeLidRgtJnt , self.eyeRgtJnt )
        mc.parent( self.eyeAdjRgtJnt , parent )

        #-- Create Controls Eye
        self.eyeLftCtrl = rt.createCtrl( 'Eye%sLFT%sCtrl' %( elem , side ) , 'sphere' , 'red' )
        self.eyeLftGmbl = rt.addGimbal( self.eyeLftCtrl )
        self.eyeLftZro = rt.addGrp( self.eyeLftCtrl )
        self.eyeLftAim = rt.addGrp( self.eyeLftCtrl , 'Aim' )
        self.eyeLftZro.snap( self.eyeLftJnt )

        self.eyeRgtCtrl = rt.createCtrl( 'Eye%sRGT%sCtrl' %( elem , side ) , 'sphere' , 'red' )
        self.eyeRgtGmbl = rt.addGimbal( self.eyeRgtCtrl )
        self.eyeRgtZro = rt.addGrp( self.eyeRgtCtrl )
        self.eyeRgtAim = rt.addGrp( self.eyeRgtCtrl , 'Aim' )
        self.eyeRgtZro.snap( self.eyeRgtJnt )

        self.eyeTrgCtrl = rt.createCtrl( 'EyeTrgt%s%sCtrl' %( elem , side ) , 'capsule' , 'yellow' )
        self.eyeTrgZro = rt.addGrp( self.eyeTrgCtrl )
        self.eyeTrgZro.snap( eyeTrgTmpJnt )

        self.eyeLftTrgCtrl = rt.createCtrl( 'EyeTrgt%sLFT%sCtrl' %( elem , side ) , 'circle' , 'red' )
        self.eyeLftTrgZro = rt.addGrp( self.eyeLftTrgCtrl )
        self.eyeLftTrgZro.snap( eyeLftTrgTmpJnt )

        self.eyeRgtTrgCtrl = rt.createCtrl( 'EyeTrgt%sRGT%sCtrl' %( elem , side ) , 'circle' , 'blue' )
        self.eyeRgtTrgZro = rt.addGrp( self.eyeRgtTrgCtrl )
        self.eyeRgtTrgZro.snap( eyeRgtTrgTmpJnt )

        #-- Adjust Shape Controls Eye
        for ctrl in ( self.eyeLftCtrl , self.eyeLftGmbl , self.eyeRgtCtrl , self.eyeRgtGmbl ) :
            ctrl.scaleShape( size * 0.5 )

        for ctrl in ( self.eyeLftTrgCtrl , self.eyeRgtTrgCtrl ) :
            ctrl.scaleShape( size * 0.45 )
            ctrl.rotateShape( 90 , 0 , 0 )

        self.eyeTrgCtrl.scaleShape( size * 0.55 )

        #-- Rig process Eye
        mc.parentConstraint( self.eyeLftGmbl , self.eyeLftJnt , mo = False )
        mc.scaleConstraint( self.eyeLftGmbl , self.eyeLftJnt , mo = False )
        mc.parentConstraint( self.eyeRgtGmbl , self.eyeRgtJnt , mo = False )
        mc.scaleConstraint( self.eyeRgtGmbl , self.eyeRgtJnt , mo = False )
        rt.localWorld( self.eyeTrgCtrl , ctrlGrp , self.eyeCtrlGrp , self.eyeTrgZro , 'parent' )

        self.eyeLftJnt.attr('ssc').value = 0
        self.eyeRgtJnt.attr('ssc').value = 0
        self.eyeLidLftJnt.attr('ssc').value = 0
        self.eyeLidRgtJnt.attr('ssc').value = 0
        
        mc.aimConstraint( self.eyeLftTrgCtrl , self.eyeLftAim , aim = (0,0,1) , u = (0,1,0) , wut = "objectrotation" , wu = (0,1,0) , wuo = self.eyeLftZro , mo = 1 )[0]
        mc.aimConstraint( self.eyeRgtTrgCtrl , self.eyeRgtAim , aim = (0,0,1) , u = (0,1,0) , wut = "objectrotation" , wu = (0,1,0) , wuo = self.eyeRgtZro , mo = 1 )[0]
        
        for ctrl in ( self.eyeLftCtrl , self.eyeRgtCtrl ) :
            ctrl.addAttr( 'eyelidsFollow' , 0 , 1 )

        self.lidLftCons = core.Dag(mc.orientConstraint( parent , self.eyeLidLftJnt , mo = True )[0])
        self.lidRgtCons = core.Dag(mc.orientConstraint( parent , self.eyeLidRgtJnt , mo = True )[0])

        '''
        self.lidLftCons = core.Dag(mc.orientConstraint( parent , self.eyeLftJnt , self.eyeLidLftJnt , mo = True )[0])
        self.lidRgtCons = core.Dag(mc.orientConstraint( parent , self.eyeRgtJnt , self.eyeLidRgtJnt , mo = True )[0])

        self.lidLftRev = rt.createNode( 'reverse' , 'EyeLid%sLFT%sRev' %( elem , side ))
        self.lidRgtRev = rt.createNode( 'reverse' , 'EyeLid%sRGT%sRev' %( elem , side ))

        self.eyeLftCtrl.attr('eyelidsFollow') >> self.lidLftCons.attr('Eye%sLFT%sJntW1' %( elem , side ))
        self.eyeLftCtrl.attr('eyelidsFollow') >> self.lidLftRev.attr('ix')
        self.lidLftRev.attr('ox') >> self.lidLftCons.attr('Head%s%sJntW0' %( elem , side ))

        self.eyeRgtCtrl.attr('eyelidsFollow') >> self.lidRgtCons.attr('Eye%sRGT%sJntW1' %( elem , side ))
        self.eyeRgtCtrl.attr('eyelidsFollow') >> self.lidRgtRev.attr('ix')
        self.lidRgtRev.attr('ox') >> self.lidRgtCons.attr('Head%s%sJntW0' %( elem , side ))
        '''
        
        #-- Adjust Hierarchy Eye
        mc.parent( self.eyeLftZro , self.eyeRgtZro , self.eyeTrgZro , self.eyeCtrlGrp )
        mc.parent( self.eyeLftTrgZro , self.eyeRgtTrgZro , self.eyeTrgCtrl )
        mc.parent( self.eyeCtrlGrp , ctrlGrp )

        #-- Cleanup Eye
        for grp in ( self.eyeCtrlGrp , self.eyeLftZro , self.eyeRgtZro , self.eyeLftTrgZro , self.eyeRgtTrgZro , self.eyeTrgZro ) :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for ctrl in ( self.eyeLftGmbl , self.eyeRgtGmbl , self.eyeLftTrgCtrl , self.eyeRgtTrgCtrl , self.eyeTrgCtrl ) :
            ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        for ctrl in ( self.eyeLftCtrl , self.eyeRgtCtrl ) :
            ctrl.lockHideAttrs( 'v' )
    
    mc.select( cl = True )