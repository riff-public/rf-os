import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  JAW RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , jawUpr1TmpJnt    = 'JawUpr1_TmpJnt' ,
                         jawUprEndTmpJnt  = 'JawUprEnd_TmpJnt' ,
                         jawLwr1TmpJnt    = 'JawLwr1_TmpJnt' ,
                         jawLwr2TmpJnt    = 'JawLwr2_TmpJnt' ,
                         jawLwrEndTmpJnt  = 'JawLwrEnd_TmpJnt' ,
                         parent           = 'Head_Jnt' ,
                         ctrlGrp          = 'Ctrl_Grp' ,
                         skinGrp          = 'Skin_Grp' ,
                         elem             = '' ,
                         side             = '' ,
                         size             = 1 
                 ):

        ##-- Info
        elem = elem.capitalize()

        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        #-- Create Main Group Jaw
        self.jawCtrlGrp = rt.createNode( 'transform' , 'Jaw%sCtrl%sGrp' %( elem , side ))

        if parent :
            mc.parentConstraint( parent , self.jawCtrlGrp , mo = False )
            mc.scaleConstraint( parent , self.jawCtrlGrp , mo = False )
    
        #-- Create Joint Jaw 
        self.jawLwr1Jnt = rt.createJnt( 'Jaw%sLwr1%sJnt' %( elem , side ) , jawLwr1TmpJnt )
        self.jawLwr2Jnt = rt.createJnt( 'Jaw%sLwr2%sJnt' %( elem , side ) , jawLwr2TmpJnt )
        self.jawLwrEndJnt = rt.createJnt( 'Jaw%sLwrEnd%sJnt' %( elem , side ) , jawLwrEndTmpJnt )
        self.jawUpr1Jnt = rt.createJnt( 'Jaw%sUpr1%sJnt' %( elem , side ) , jawUpr1TmpJnt )
        self.jawUprEndJnt = rt.createJnt( 'Jaw%sUprEnd%sJnt' %( elem , side ) , jawUprEndTmpJnt )

        mc.parent( self.jawLwrEndJnt , self.jawLwr2Jnt )
        mc.parent( self.jawLwr2Jnt , self.jawLwr1Jnt )
        mc.parent( self.jawUprEndJnt , self.jawUpr1Jnt )

        if parent :
            mc.parent( self.jawLwr1Jnt , parent )
            mc.parent( self.jawUpr1Jnt , parent )

        #-- Create Controls Jaw
        self.jawLwr1Ctrl = rt.createCtrl( 'Jaw%sLwr1%sCtrl' %( elem , side ) , 'square' , 'red' )
        self.jawLwr1Gmbl = rt.addGimbal( self.jawLwr1Ctrl )
        self.jawLwr1Zro = rt.addGrp( self.jawLwr1Ctrl )
        self.jawLwr1Zro.snap( self.jawLwr1Jnt )

        self.jawLwr2Ctrl = rt.createCtrl( 'Jaw%sLwr2%sCtrl' %( elem , side ) , 'square' , 'yellow' )
        self.jawLwr2Gmbl = rt.addGimbal( self.jawLwr2Ctrl )
        self.jawLwr2Zro = rt.addGrp( self.jawLwr2Ctrl )
        self.jawLwr2Zro.snap( self.jawLwr2Jnt )

        self.jaw1UprCtrl = rt.createCtrl( 'Jaw%sUpr1%sCtrl' %( elem , side ) , 'square' , 'yellow' )
        self.jaw1UprGmbl = rt.addGimbal( self.jaw1UprCtrl )
        self.jaw1UprZro = rt.addGrp( self.jaw1UprCtrl )
        self.jaw1UprZro.snap( self.jawUpr1Jnt )

        #-- Adjust Shape Controls Jaw
        for ctrl in ( self.jaw1UprCtrl , self.jaw1UprGmbl , self.jawLwr2Ctrl , self.jawLwr2Gmbl ) :
            ctrl.scaleShape( size * 0.3 )
            ctrl.rotateShape( 90 , 0 , 0 )

        for ctrl in ( self.jawLwr1Ctrl , self.jawLwr1Gmbl ) :
            ctrl.scaleShape( size * 0.9 )
    
        #-- Adjust Rotate Order Jaw
        for obj in ( self.jawLwr1Ctrl , self.jawLwr2Ctrl , self.jaw1UprCtrl , self.jawLwr1Jnt , self.jawLwr2Jnt , self.jawUpr1Jnt ) :
            obj.setRotateOrder( 'zyx' )

        #-- Rig process Jaw
        self.jawTransformGrp = rt.createNode( 'transform' , 'Jaw%sLwr1Transform%sGrp' %( elem , side ))
        self.jawDrvGrp = rt.createNode( 'transform' , 'Jaw%sLwr1Drv%sGrp' %( elem , side ))
        self.jawDrvGrp.snap( jawLwr1TmpJnt )

        mc.parentConstraint( self.jawDrvGrp , self.jawLwr1Jnt , mo = False )
        mc.scaleConstraint( self.jawDrvGrp , self.jawLwr1Jnt , mo = False )
        mc.parentConstraint( self.jawLwr2Gmbl , self.jawLwr2Jnt , mo = False )
        mc.scaleConstraint( self.jawLwr2Gmbl , self.jawLwr2Jnt , mo = False )
        mc.parentConstraint( self.jaw1UprGmbl , self.jawUpr1Jnt , mo = False )
        mc.scaleConstraint( self.jaw1UprGmbl , self.jawUpr1Jnt , mo = False )
        mc.parentConstraint( self.jawLwr1Gmbl , self.jawTransformGrp , mo = False )
        mc.scaleConstraint( self.jawLwr1Gmbl , self.jawTransformGrp , mo = False )

        self.jawLwr1Jnt.attr('ssc').value = 0
        self.jawLwr2Jnt.attr('ssc').value = 0
        self.jawUpr1Jnt.attr('ssc').value = 0
    
        self.jawLwr1DrvMdv = rt.createNode( 'multiplyDivide' , 'Jaw%sLwr1Drv%sMdv' %( elem , side ))
        self.jawLwr1DrvPma = rt.createNode( 'plusMinusAverage' , 'Jaw%sLwr1Drv%sPma' %( elem , side ))
        self.jawLwr1DrvCmp = rt.createNode( 'clamp' , 'Jaw%sLwr1Drv%sCmp' %( elem , side ))
        self.jawLwr1DrvCmp.attr('opr') >> self.jawLwr1DrvMdv.attr('i1x')
        self.jawLwr1DrvCmp.attr('opr') >> self.jawLwr1DrvMdv.attr('i1y')
        self.jawLwr1DrvCmp.attr('opr') >> self.jawLwr1DrvMdv.attr('i1z')
        self.jawLwr1DrvMdv.attr('ox') >> self.jawDrvGrp.attr('rx')
        self.jawLwr1DrvMdv.attr('oy') >> self.jawDrvGrp.attr('ty')
        self.jawLwr1DrvMdv.attr('oz') >> self.jawDrvGrp.attr('tz')
        self.jawLwr1DrvPma.addAttr('constant')
        self.jawLwr1DrvPma.attr('constant').value = -(self.jawTransformGrp.attr('rx').value)
        self.jawLwr1DrvPma.attr('constant') >> self.jawLwr1DrvPma.attr('i1[0]')
        self.jawTransformGrp.attr('rx') >> self.jawLwr1DrvPma.attr('i1[1]')
        self.jawLwr1DrvPma.attr('o1') >> self.jawLwr1DrvCmp.attr('ipr')
        self.jawLwr1DrvCmp.attr('mxr').value = 100

        self.jawLwr1CtrlShape = core.Dag(self.jawLwr1Ctrl.shape)
        self.jawLwr1CtrlShape.addAttr( 'jawRx' , chanelBox = False )
        self.jawLwr1CtrlShape.addAttr( 'jawTy' , chanelBox = False )
        self.jawLwr1CtrlShape.addAttr( 'jawTz' , chanelBox = False )
        self.jawLwr1CtrlShape.attr('jawRx') >> self.jawLwr1DrvMdv.attr('i2x')
        self.jawLwr1CtrlShape.attr('jawTy') >> self.jawLwr1DrvMdv.attr('i2y')
        self.jawLwr1CtrlShape.attr('jawTz') >> self.jawLwr1DrvMdv.attr('i2z')

        #-- Adjust Hierarchy Jaw
        mc.parent( self.jaw1UprZro , self.jawLwr1Zro , self.jawTransformGrp , self.jawCtrlGrp )
        mc.parent( self.jawDrvGrp , self.jawLwr1Gmbl )
        mc.parent( self.jawLwr2Zro , self.jawDrvGrp )
        
        if ctrlGrp :
            mc.parent( self.jawCtrlGrp , ctrlGrp )

        #-- Cleanup Jaw
        for grp in ( self.jawCtrlGrp , self.jaw1UprZro , self.jawLwr1Zro , self.jawLwr2Zro ) :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for ctrl in ( self.jawLwr1Ctrl , self.jawLwr2Ctrl , self.jaw1UprCtrl , self.jawLwr1Gmbl , self.jawLwr2Gmbl , self.jaw1UprGmbl ) :
            ctrl.lockHideAttrs( 'v' )
    
    mc.select( cl = True )