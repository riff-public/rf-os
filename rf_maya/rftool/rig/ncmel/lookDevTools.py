import maya.cmds as mc
import maya.mel as mm
import maya.OpenMaya as om
import pymel.core as pm
import os
import shutil

def sceneInfo( folderName = 'texture/rig' , step = -1 ):
    # Get path file and create folder texture
    # ex.folderName = 'texture/rig' , step = -1
    # Do create folder "texture" in path file with out last / -1 step
    # and create folder "rig" in folder "texture"

    filePath = mc.file( q = True , sn = True )
    scnDir = '/'.join( filePath.split('/')[:step] )

    if not os.path.exists( '%s/%s' %( scnDir , folderName )) :
        os.makedirs( '%s/%s' %( scnDir , folderName ))

    return '%s/%s' %( scnDir , folderName )

def prepareGeoGrp( obj = 'Geo_Grp' ) :
    # Prepare group all geo for lookDev

    if pm.objExists( obj ) :
        suffix = obj.split( '_' )[0]
        prefix = obj.replace( suffix , '' )
        
        shad = pm.duplicate( obj , n = '%sShad%s' %(suffix,prefix))[0]
        
        # Clear Constraint Node
        consNds = pm.listRelatives( shad , ad = True , type = 'constraint' )
        
        if consNds :
            pm.delete( consNds )

        # Clear Shape Orig Node
        transNds = pm.listRelatives( shad , ad = True , type = 'transform' )
        if transNds :
            for transNd in transNds :
                shapes = pm.listRelatives( transNd , s = True , f = True )
                for shape in shapes :
                    if mc.getAttr('%s.intermediateObject' %shape ):
                        connections = pm.listConnections( shape , s = True , d = False )
                        if not connections :
                            pm.delete( shape )
            
        # Rename        
        childs = pm.listRelatives( shad , ad = True , f = True )
            
        for child in childs :
            objName = child.split('|')[-1]
            
            suffix = objName.split( '_' )[0]
            prefix = objName.replace( suffix , '' )
            
            pm.rename( child , '%sShad%s' %(suffix,prefix))

        pm.parent( shad , w = True )
        return shad

def exportFile( path = '' ) :
    # Export file to select path

    if path :
        pathName = path
    else :
        pathName = mc.fileDialog2( cap = 'Path File' , fileFilter = "*.ma" , dialogStyle = 1 , fm = 5 )

    mc.file( r'%s' %pathName  , type = 'mayaAscii' , force = True , es = True , pr = True )
    return r'%s' %pathName

def addBlendshape( src = '' , trgt = '' ) :
    # Create blend shape with 'frontOfChain' option.

    name = trgt.split(':')[-1]
    nameList = name.split('_')

    if len(nameList) > 1:
        nameList[-1] = 'Bsn'
    else:
        nameList.append( 'Bsn' )

    bsn = '_'.join( nameList )
    mc.blendShape( src , trgt , frontOfChain = True , origin = 'local' , n = bsn )
    mc.setAttr( '%s.w[0]' %bsn , 1 )

def transferShade() :
    ## Transfer material from frist obj to any obj selected

    sels = mc.ls( sl = True , l = True )
    mc.hyperShade( smn = True )
    mat = mc.ls( sl = True )[0]
    
    for sel in sels :
        if not sel == sels[0]:
            mc.select( sel , r = True )
            mc.hyperShade( assign = mat )
    
    mc.select( sels , r = True )

def transferShadeWithOutRef() :
    ## Transfer material from frist obj to any obj selected with out reference

    sels = mc.ls( sl = True , l = True )
    mc.hyperShade( smn = True )
    refMat = mc.ls( sl = True )[0]
    
    if ':' in refMat:
        mat = refMat.split(':')[-1]

        if not mc.objExists( mat ):
            mc.duplicate( upstreamNodes = True )
    
        for sel in sels :
            if not sel == sels[0]:
                mc.select( sel , r = True )
                mc.hyperShade( assign = mat )
    else:
        transferShade()
    
    mc.select( sels , r = True )

def listHierarchy( parent = '' ) :
    # List all hierarchy in parent

    if parent :
        mc.select( parent , hi = True )
        chld = mc.ls( sl = True , type = 'transform' )
        mc.select( cl = True )

    return chld

def compareHierarchy( src = '' , trgt = '' ) :
    # Compare hierarchy

    if src and trgt :
        srcLen = len(src)

        for i in range(srcLen) :
            suffix = src[i].split( '_' )[0]
            prefix = src[i].replace( suffix , '' )
            
            if '%sShad%s' %(suffix,prefix) == trgt[i] :
                print True
            else :
                print False