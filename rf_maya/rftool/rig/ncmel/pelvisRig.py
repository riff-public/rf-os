import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  PELVIS RIGGING MODULE [ FK Only ]
#
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , pelvisTmpJnt = 'Pelvis_TmpJnt' , 
                         parent       = 'Root_jnt' ,
                         ctrlGrp      = 'Ctrl_Grp' , 
                         skinGrp      = 'Skin_Grp' , 
                         size         = 1 
                 ):

        #-- Create Main Group
        self.pelvisCtrlGrp = rt.createNode( 'transform' , 'PelvisCtrl_Grp' )
        mc.parentConstraint( parent , self.pelvisCtrlGrp , mo = False )
        
        #-- Create Joint
        self.pelvisJnt = rt.createJnt( 'Pelvis_Jnt' , pelvisTmpJnt )

        #-- Create Controls
        self.pelvisCtrl = rt.createCtrl( 'Pelvis_Ctrl' , 'cube' , 'yellow' , jnt = True )
        self.pelvisGmbl = rt.addGimbal( self.pelvisCtrl )
        self.pelvisZro = rt.addGrp( self.pelvisCtrl )
        self.pelvisPars = rt.addGrp( self.pelvisCtrl , 'Pars' )

        self.pelvisZro.snapPoint( self.pelvisJnt )
        self.pelvisCtrl.snapJntOrient( self.pelvisJnt )

        #-- Adjust Shape Controls
        for ctrl in ( self.pelvisCtrl , self.pelvisGmbl ) :
            ctrl.scaleShape( size )

        #-- Adjust Rotate Order
        for obj in ( self.pelvisCtrl , self.pelvisJnt ) :
            obj.setRotateOrder( 'xzy' )

        #-- Rig process
        mc.parentConstraint( self.pelvisGmbl , self.pelvisJnt , mo = False )
        rt.localWorld( self.pelvisCtrl , ctrlGrp , self.pelvisCtrlGrp , self.pelvisZro , 'orient' )
        
        #-- Adjust Hierarchy
        mc.parent( self.pelvisZro , self.pelvisCtrlGrp )
        mc.parent( self.pelvisCtrlGrp , ctrlGrp )
        mc.parent( self.pelvisJnt , parent )

        #-- Cleanup
        for obj in ( self.pelvisCtrlGrp , self.pelvisZro ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for obj in ( self.pelvisCtrl , self.pelvisGmbl ) :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        mc.select( cl = True )