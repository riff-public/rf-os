from collections import defaultdict

import maya.cmds as mc
import maya.mel as mm
import pymel.core as pm
from rftool.rig.ncmel import core
from rftool.rig.ncmel import rigTools as rt
reload( core )
reload( rt )
from lpRig import rigTools as lrr
reload(lrr)
from lpRig import rigTools  as rigTools
reload(rigTools)
from utaTools.utapy import utaCore
reload(utaCore)
# --------------------------------------------------------------------------------------------
#
# Duplicate Blendshape
#
# description :  - Cut head geomety [ eye , eye brow , eyelash , in mouth ] 
#                - Select Group and run script "dupBlendshape()"
#
# --------------------------------------------------------------------------------------------
def groupInShapeEditor(bshNode, name, indices):
    # generate mel arg
    argLists = []
    for i in indices:
        argLists.append('"%s.%s"' %(bshNode, i))

    mm.eval('blendShapeCreateTargetGroup {%s};' %(', '.join(argLists)))

    # name it
    bshNode = pm.PyNode(bshNode)
    nextIndex = max(bshNode.targetDirectory.getArrayIndices())
    print 'Grouping in shapeEditor %s: %s' %(nextIndex, name)
    bshNode.targetDirectory[nextIndex].directoryName.set(name)

def dupBlendshape(  eyebrow = True ,
                    eye     = True ,
                    mouth   = True ,
                    nose    = True ,
                    head    = True ,
                    threeEye = True) :
    
    #------------------------------------------------------
    #   Infomation All Blendshape 
    #------------------------------------------------------

    bshBase =         [ ]
    
    bshNode =         [ ]
    
    bshInBtw =        [ ]
    
    side =            [ 'L' , 'R']

    eyeSide =         [ 'L' , 'R']

    if threeEye:
        eyeSide =    [ 'L' , 'R', 'C']    

    eyebrows_side =    ['EyebrowUp' ,
                        'EyebrowDn' ,
                        'EyebrowIn' ,
                        'EyebrowOut' ,
                        'EyebrowPull' ,
                        'EyebrowInnerUp' ,
                        'EyebrowInnerDn' ,
                        'EyebrowInnerIn' ,
                        'EyebrowInnerOut' ,
                        'EyebrowInnerTurnF' ,
                        'EyebrowInnerTurnC' ,
                        'EyebrowMiddleUp' ,
                        'EyebrowMiddleDn' ,
                        'EyebrowMiddleIn' ,
                        'EyebrowMiddleOut' ,
                        'EyebrowMiddleTurnF' ,
                        'EyebrowMiddleTurnC' ,
                        'EyebrowOuterUp' ,
                        'EyebrowOuterDn' ,
                        'EyebrowOuterIn' ,
                        'EyebrowOuterOut' ,
                        'EyebrowOuterTurnF' ,
                        'EyebrowOuterTurnC' ,
                        'EyebrowTurnF' ,
                        'EyebrowTurnC' ,
                        'EyebrowSquintIn',
                        'EyebrowSquintOut' ]
    
    lids_side =        ['UpLidUp' ,
                        'UpLidDn' ,
                        'LoLidUp' ,
                        'LoLidDn' ,
                        'UpLidTurnF' ,
                        'UpLidTurnC' ,
                        'LoLidTurnF' ,
                        'LoLidTurnC' ,
                        'UpLidInUp' ,
                        'UpLidMidUp' ,
                        'UpLidOutUp' ,
                        'UpLidInDn' ,
                        'UpLidMidDn' ,
                        'UpLidOutDn' ,
                        'LoLidInUp' ,
                        'LoLidMidUp' ,
                        'LoLidOutUp' ,
                        'LoLidInDn' ,
                        'LoLidMidDn' ,
                        'LoLidOutDn' ,
                        'EyeBallUp' ,
                        'EyeBallDn' ,
                        'EyeBallIn' ,
                        'EyeBallOut' ,
                        'EyeBallTurnF' ,
                        'EyeBallTurnC' ]
    
    lips =             ['MouthUp' ,
                        'MouthDn' ,
                        'MouthLeft' ,
                        'MouthRight' ,
                        'MouthTurnF' ,
                        'MouthTurnC' ,
                        'UpLipsUp' ,
                        'UpLipsDn' ,
                        'LoLipsUp' ,
                        'LoLipsDn' ,
                        'UpLipsAllUp' ,
                        'UpLipsAllDn' ,
                        'LoLipsAllUp' ,
                        'LoLipsAllDn' ,
                        'UpLipsCurlIn' ,
                        'UpLipsCurlOut' ,
                        'LoLipsCurlIn' ,
                        'LoLipsCurlOut' ,
                        'MouthClench' ,
                        'MouthPull' ,
                        'MouthU' ]
    
    lips_side =        ['UpLipsUp' ,
                        'UpLipsDn' ,
                        'LoLipsUp' ,
                        'LoLipsDn' ,
                        'CornerUp' ,
                        'CornerDn' ,
                        'CornerIn' ,
                        'CornerOut' ,
                        'LipsPartIn' ,
                        'LipsPartOut' ,
                        'CheekUp' ,
                        'CheekDn' ,
                        'PuffIn' ,
                        'PuffOut' ,
                        'PuckerIn' ,
                        'PuckerOut' ]
    
    noses =            ['NoseUp' ,
                        'NoseDn' ,
                        'NoseLeft' ,
                        'NoseRight' ,
                        'NoseStretch' ,
                        'NoseSquash' ,
                        'NoseSnarl' ]
    
    noses_side =       ['NoseUp' ,
                        'NoseDn' ,
                        'NoseTurnF' ,
                        'NoseTurnC' ,
                        'NoseTwist' ,]
    
    heads =            ['HeadUprStretch' ,
                        'HeadUprSquash' ,
                        'HeadUprLeft' ,
                        'HeadUprRight' ,
                        'HeadUprFront' ,
                        'HeadUprBack' ,
                        'HeadLwrStretch' ,
                        'HeadLwrSquash' ,
                        'HeadLwrLeft' ,
                        'HeadLwrRight' ,
                        'HeadLwrFront' ,
                        'HeadLwrBack' ,
                        'HeadEyebrowPull' ]
    
    inBetween_side =   ['UpLidDnInBtw' ,
                        'LoLidUpInBtw' ]
    
    fontText = 'MS Shell Dlg 2, 27.8pt'
    groupData = defaultdict(list)  # {name:()}
    gi = 0

    #------------------------------------------------------
    #   Create Main Group
    #------------------------------------------------------
    
    sel = mc.ls( sl = True )[0]
    lid_status = False
    # sel = 'BshRigGeo_Grp'
    bshBase.append( sel )
    

    if not mc.objExists ("BshRigStill_Grp"):
        facialBshStillGrp = mc.createNode( 'transform' , n = 'BshRigStill_Grp' )
        bshDeleteGrp = 'Delete_Grp'
        if not mc.objExists(bshDeleteGrp):
            bshDeleteGrp = mc.createNode( 'transform' , n = 'Delete_Grp' )

        allBshGrp = mc.createNode( 'transform' , n = 'Bsh_Grp' )
        allTextGrp = mc.createNode( 'transform' , n = 'Text_Grp' )

        mc.parent( allBshGrp , allTextGrp , bshDeleteGrp )
        mc.parent( sel , facialBshStillGrp )
    else:
        facialBshStillGrp = 'BshRigStill_Grp'
        bshDeleteGrp = 'Delete_Grp'
        allBshGrp = 'Bsh_Grp'
        allTextGrp = 'Text_Grp'

    mc.select( sel )
    
    #------------------------------------------------------
    #   CleanUp ShapeOrig
    #------------------------------------------------------
    if not mc.ls(type ='blendShape'):
        shapeOrig = mc.ls( '*Orig*' )
        if shapeOrig :
            mc.delete(shapeOrig)
        
    #------------------------------------------------------
    #   Check Mesh Size
    #------------------------------------------------------
    
    bb = mc.exactWorldBoundingBox()

    yOffset = float( abs( bb[1] - bb[4] ) * 1.2 )
    xOffset = float( abs( bb[0] - bb[3] ) * 1.3 )
    xVal = 0
    yVal = 0
    #------------------------------------------------------
    #   EYEBROW Duplicate
    #------------------------------------------------------
    
    if eyebrow == True :
        for index, sides in enumerate(side) :
            xVal = xOffset*(1+index)
            yVal = 0
            
            for eyebrow in eyebrows_side :
                if not mc.objExists( 'EyebrowBsh_%s_Grp' %sides ) :
                    bshGrp = mc.createNode( 'transform' , n = 'EyebrowBsh_%s_Grp' %sides )
                    mc.parent( bshGrp , allBshGrp )
                    
                if not mc.objExists( 'EyebrowText_%s_Grp' %sides ) :
                    textGrp = mc.createNode( 'transform' , n = 'EyebrowText_%s_Grp' %sides )
                    mc.parent( textGrp , allTextGrp )
                
                mc.select( sel , r = True )
                duppedNode = mc.duplicate( sel , rr = True )[0]
                bshNodes = mc.rename( duppedNode , '%s_%s' %(eyebrow , sides) )
                mc.parent( bshNodes , bshGrp )
                mc.move( 0 , yVal , 0 , bshNodes , r = True )
                
                txtCrv = mc.textCurves( n = '%s_' %bshNodes , f = fontText , t = bshNodes )[0]
                mc.select( txtCrv , r = True )
                txtBb = mc.exactWorldBoundingBox()
                mc.xform( txtCrv , cp = True )
                
                objWidth = abs(bb[0] - bb[3])
                txtWidth = abs(txtBb[0] - txtBb[3])
                
                tmpLoc = mc.spaceLocator()
                sclVal = objWidth/txtWidth
                mc.scale( sclVal , sclVal , sclVal , tmpLoc , r = True )
                mc.move( 0 , bb[1]+yVal , bb[5] , tmpLoc , r = True )
                
                mc.delete( mc.parentConstraint( tmpLoc , txtCrv ))  
                mc.delete( mc.scaleConstraint( tmpLoc , txtCrv ))
                mc.parentConstraint( bshNodes , txtCrv , mo = True )
                
                mc.setAttr( '%s.overrideEnabled' % txtCrv, 1 )
                mc.setAttr( '%s.overrideDisplayType' % txtCrv, 2 )
                
                mc.delete( tmpLoc )
                mc.parent( txtCrv , textGrp )

                bshNode.append( bshNodes )

                yVal += yOffset

                groupData['Eyebrow_%s' %sides].append(gi)
                gi += 1
                
            mc.move( xVal , 0 , 0 , bshGrp , r=True )

    #------------------------------------------------------
    #   EYE Duplicate
    #------------------------------------------------------
    
    if eye == True :
        
        for index, sides in enumerate(eyeSide) :
            xVal = xOffset*(3+index)
            yVal = 0
            
            for lid in lids_side :
                if not mc.objExists( 'LidBsh_%s_Grp' %sides ) :
                    bshGrp = mc.createNode( 'transform' , n = 'LidBsh_%s_Grp' %sides )
                    mc.parent( bshGrp , allBshGrp )
                    
                if not mc.objExists( 'LidText_%s_Grp' %sides ) :
                    textGrp = mc.createNode( 'transform' , n = 'LidText_%s_Grp' %sides )
                    mc.parent( textGrp , allTextGrp )
                
                mc.select( sel , r = True )
                duppedNode = mc.duplicate( sel , rr = True )[0]
                bshNodes = mc.rename( duppedNode , '%s_%s' %(lid , sides) )
                mc.parent( bshNodes , bshGrp )
                mc.move( 0 , yVal , 0 , bshNodes , r = True )
                
                txtCrv = mc.textCurves( n = '%s_' %bshNodes , f = fontText , t = bshNodes )[0]
                mc.select( txtCrv , r = True )
                txtBb = mc.exactWorldBoundingBox()
                mc.xform( txtCrv , cp = True )
                
                objWidth = abs(bb[0] - bb[3])
                txtWidth = abs(txtBb[0] - txtBb[3])
                
                tmpLoc = mc.spaceLocator()
                sclVal = objWidth/txtWidth
                mc.scale( sclVal , sclVal , sclVal , tmpLoc , r = True )
                mc.move( 0 , bb[1]+yVal , bb[5] , tmpLoc , r = True )
                
                mc.delete( mc.parentConstraint( tmpLoc , txtCrv ))  
                mc.delete( mc.scaleConstraint( tmpLoc , txtCrv ))
                mc.parentConstraint( bshNodes , txtCrv , mo = True )
                
                mc.setAttr( '%s.overrideEnabled' % txtCrv, 1 )
                mc.setAttr( '%s.overrideDisplayType' % txtCrv, 2 )
                
                mc.delete( tmpLoc )
                mc.parent( txtCrv , textGrp )
                
                bshNode.append( bshNodes )

                groupData['Eye_%s' %sides].append(gi)
                gi += 1
                
                yVal += yOffset
            mc.move( xVal , 0 , 0 , bshGrp , r=True )
        
        yVal = 0
    
    #------------------------------------------------------
    #   MOUTH Duplicate
    #------------------------------------------------------
    
    if mouth == True :
        for lip in lips :
            if not mc.objExists( 'LipBsh_Grp' ) :
                bshGrp = mc.createNode( 'transform' , n = 'LipBsh_Grp' )
                mc.parent( bshGrp , allBshGrp )
                
            if not mc.objExists( 'LipText_Grp' ) :
                textGrp = mc.createNode( 'transform' , n = 'LipText_Grp' )
                mc.parent( textGrp , allTextGrp )
            
            mc.select( sel , r = True )
            duppedNode = mc.duplicate( sel , rr = True )[0]
            bshNodes = mc.rename( duppedNode , '%s' %lip )
            mc.parent( bshNodes , bshGrp )
            mc.move( 0 , yVal , 0 , bshNodes , r = True )
                
            txtCrv = mc.textCurves( n = '%s_' %bshNodes , f = fontText , t = bshNodes )[0]
            mc.select( txtCrv , r = True )
            txtBb = mc.exactWorldBoundingBox()
            mc.xform( txtCrv , cp = True )
            
            objWidth = abs(bb[0] - bb[3])
            txtWidth = abs(txtBb[0] - txtBb[3])
            
            tmpLoc = mc.spaceLocator()
            sclVal = objWidth/txtWidth
            mc.scale( sclVal , sclVal , sclVal , tmpLoc , r = True )
            mc.move( 0 , bb[1]+yVal , bb[5] , tmpLoc , r = True )
            
            mc.delete( mc.parentConstraint( tmpLoc , txtCrv ))  
            mc.delete( mc.scaleConstraint( tmpLoc , txtCrv ))
            mc.parentConstraint( bshNodes , txtCrv , mo = True )
            
            mc.setAttr( '%s.overrideEnabled' % txtCrv, 1 )
            mc.setAttr( '%s.overrideDisplayType' % txtCrv, 2 )
            
            mc.delete( tmpLoc )
            mc.parent( txtCrv , textGrp )
            
            bshNode.append( bshNodes )

            groupData['Mouth'].append(gi)
            gi += 1
                
            yVal += yOffset
        
        xVal = xOffset*5
        mc.move( xVal , 0 , 0 , bshGrp , r=True )
        
        for sides in side :
            xVal = xOffset*6
            yVal = 0
            
            for lip in lips_side :
                if not mc.objExists( 'LipBsh_%s_Grp' %sides ) :
                    bshGrp = mc.createNode( 'transform' , n = 'LipBsh_%s_Grp' %sides )
                    mc.parent( bshGrp , allBshGrp )
                    
                if not mc.objExists( 'LipText_%s_Grp' %sides ) :
                    textGrp = mc.createNode( 'transform' , n = 'LipText_%s_Grp' %sides )
                    mc.parent( textGrp , allTextGrp )
                
                mc.select( sel , r = True )
                duppedNode = mc.duplicate( sel , rr = True )[0]
                bshNodes = mc.rename( duppedNode , '%s_%s' %(lip , sides) )
                mc.parent( bshNodes , bshGrp )
                mc.move( 0 , yVal , 0 , bshNodes , r = True )
                
                txtCrv = mc.textCurves( n = '%s_' %bshNodes , f = fontText , t = bshNodes )[0]
                mc.select( txtCrv , r = True )
                txtBb = mc.exactWorldBoundingBox()
                mc.xform( txtCrv , cp = True )
                
                objWidth = abs(bb[0] - bb[3])
                txtWidth = abs(txtBb[0] - txtBb[3])
                
                tmpLoc = mc.spaceLocator()
                sclVal = objWidth/txtWidth
                mc.scale( sclVal , sclVal , sclVal , tmpLoc , r = True )
                mc.move( 0 , bb[1]+yVal , bb[5] , tmpLoc , r = True )
                
                mc.delete( mc.parentConstraint( tmpLoc , txtCrv ))  
                mc.delete( mc.scaleConstraint( tmpLoc , txtCrv ))
                mc.parentConstraint( bshNodes , txtCrv , mo = True )
                
                mc.setAttr( '%s.overrideEnabled' % txtCrv, 1 )
                mc.setAttr( '%s.overrideDisplayType' % txtCrv, 2 )
                
                mc.delete( tmpLoc )
                mc.parent( txtCrv , textGrp )
                
                bshNode.append( bshNodes )

                
                groupData['Mouth_%s' %(sides)].append(gi)
                gi += 1

                yVal += yOffset
            
            mc.move( xVal , 0 , 0 , bshGrp , r=True )
        
        yVal = 0
    
    #------------------------------------------------------
    #   NOSE Duplicate
    #------------------------------------------------------
    
    if nose == True :
        for nose in noses :
            if not mc.objExists( 'NoseBsh_Grp' ) :
                bshGrp = mc.createNode( 'transform' , n = 'NoseBsh_Grp' )
                mc.parent( bshGrp , allBshGrp )
                
            if not mc.objExists( 'NoseText_Grp' ) :
                textGrp = mc.createNode( 'transform' , n = 'NoseText_Grp' )
                mc.parent( textGrp , allTextGrp )
            
            mc.select( sel , r = True )
            duppedNode = mc.duplicate( sel , rr = True )[0]
            bshNodes = mc.rename( duppedNode , '%s' %nose )
            mc.parent( bshNodes , bshGrp )
            mc.move( 0 , yVal , 0 , bshNodes , r = True )
                
            txtCrv = mc.textCurves( n = '%s_' %bshNodes , f = fontText , t = bshNodes )[0]
            mc.select( txtCrv , r = True )
            txtBb = mc.exactWorldBoundingBox()
            mc.xform( txtCrv , cp = True )
            
            objWidth = abs(bb[0] - bb[3])
            txtWidth = abs(txtBb[0] - txtBb[3])
            
            tmpLoc = mc.spaceLocator()
            sclVal = objWidth/txtWidth
            mc.scale( sclVal , sclVal , sclVal , tmpLoc , r = True )
            mc.move( 0 , bb[1]+yVal , bb[5] , tmpLoc , r = True )
            
            mc.delete( mc.parentConstraint( tmpLoc , txtCrv ))  
            mc.delete( mc.scaleConstraint( tmpLoc , txtCrv ))
            mc.parentConstraint( bshNodes , txtCrv , mo = True )
            
            mc.setAttr( '%s.overrideEnabled' % txtCrv, 1 )
            mc.setAttr( '%s.overrideDisplayType' % txtCrv, 2 )
            
            mc.delete( tmpLoc )
            mc.parent( txtCrv , textGrp )
            
            bshNode.append( bshNodes )
            groupData['Nose'].append(gi)
            gi += 1

            yVal += yOffset
        
        xVal = xOffset*7
        mc.move( xVal , 0 , 0 , bshGrp , r=True )
        
        for index,sides in enumerate(side) :
            xVal = xOffset*(8+index)
            yVal = 0
            
            for nose in noses_side :
                if not mc.objExists( 'NoseBsh_%s_Grp' %sides ) :
                    bshGrp = mc.createNode( 'transform' , n = 'NoseBsh_%s_Grp' %sides )
                    mc.parent( bshGrp , allBshGrp )
                    
                if not mc.objExists( 'NoseText_%s_Grp' %sides ) :
                    textGrp = mc.createNode( 'transform' , n = 'NoseText_%s_Grp' %sides )
                    mc.parent( textGrp , allTextGrp )
                
                mc.select( sel , r = True )
                duppedNode = mc.duplicate( sel , rr = True )[0]
                bshNodes = mc.rename( duppedNode , '%s_%s' %(nose , sides) )
                mc.parent( bshNodes , bshGrp )
                mc.move( 0 , yVal , 0 , bshNodes , r = True )
                
                txtCrv = mc.textCurves( n = '%s_' %bshNodes , f = fontText , t = bshNodes )[0]
                mc.select( txtCrv , r = True )
                txtBb = mc.exactWorldBoundingBox()
                mc.xform( txtCrv , cp = True )
                
                objWidth = abs(bb[0] - bb[3])
                txtWidth = abs(txtBb[0] - txtBb[3])
                
                tmpLoc = mc.spaceLocator()
                sclVal = objWidth/txtWidth
                mc.scale( sclVal , sclVal , sclVal , tmpLoc , r = True )
                mc.move( 0 , bb[1]+yVal , bb[5] , tmpLoc , r = True )
                
                mc.delete( mc.parentConstraint( tmpLoc , txtCrv ))  
                mc.delete( mc.scaleConstraint( tmpLoc , txtCrv ))
                mc.parentConstraint( bshNodes , txtCrv , mo = True )
                
                mc.setAttr( '%s.overrideEnabled' % txtCrv, 1 )
                mc.setAttr( '%s.overrideDisplayType' % txtCrv, 2 )
                
                mc.delete( tmpLoc )
                mc.parent( txtCrv , textGrp )
                
                bshNode.append( bshNodes )

                groupData['Nose_%s' %(sides)].append(gi)
                gi += 1

                yVal += yOffset
            
            mc.move( xVal , 0 , 0 , bshGrp , r=True )
        
        yVal = 0
    
    #------------------------------------------------------
    #   HEAD Duplicate
    #------------------------------------------------------
    
    if head == True : 
        for head in heads :
            if not mc.objExists( 'HeadBsh_Grp' ) :
                bshGrp = mc.createNode( 'transform' , n = 'HeadBsh_Grp' )
                mc.parent( bshGrp , allBshGrp )
                
            if not mc.objExists( 'HeadText_Grp' ) :
                textGrp = mc.createNode( 'transform' , n = 'HeadText_Grp' )
                mc.parent( textGrp , allTextGrp )
            
            mc.select( sel , r = True )
            duppedNode = mc.duplicate( sel , rr = True )[0]
            bshNodes = mc.rename( duppedNode , '%s' %head )
            mc.parent( bshNodes , bshGrp )
            mc.move( 0 , yVal , 0 , bshNodes , r = True )
                
            txtCrv = mc.textCurves( n = '%s_' %bshNodes , f = fontText , t = bshNodes )[0]
            mc.select( txtCrv , r = True )
            txtBb = mc.exactWorldBoundingBox()
            mc.xform( txtCrv , cp = True )
            
            objWidth = abs(bb[0] - bb[3])
            txtWidth = abs(txtBb[0] - txtBb[3])
            
            tmpLoc = mc.spaceLocator()
            sclVal = objWidth/txtWidth
            mc.scale( sclVal , sclVal , sclVal , tmpLoc , r = True )
            mc.move( 0 , bb[1]+yVal , bb[5] , tmpLoc , r = True )
            
            mc.delete( mc.parentConstraint( tmpLoc , txtCrv ))  
            mc.delete( mc.scaleConstraint( tmpLoc , txtCrv ))
            mc.parentConstraint( bshNodes , txtCrv , mo = True )
            
            mc.setAttr( '%s.overrideEnabled' % txtCrv, 1 )
            mc.setAttr( '%s.overrideDisplayType' % txtCrv, 2 )
            
            mc.delete( tmpLoc )
            mc.parent( txtCrv , textGrp )
            
            bshNode.append( bshNodes )
            groupData['Head'].append(gi)
            gi += 1

            yVal += yOffset
        
        xVal = xOffset*10
        mc.move( xVal , 0 , 0 , bshGrp , r=True )
        
        yVal = 0
    
    
    #------------------------------------------------------
    #   EYE InBetween Duplicate
    #------------------------------------------------------
    
    if threeEye == True :
        for index, sides in enumerate(eyeSide) :
            print index, '....index'
            print sides, '.....sides'
            xVal = xOffset*(11+index) 
            yVal = 0
            
            for inBetweens in inBetween_side :
                if not mc.objExists( 'InBetweenBsh_%s_Grp' %sides ) :
                    bshGrp = mc.createNode( 'transform' , n = 'InBetweenBsh_%s_Grp' %sides )
                    mc.parent( bshGrp , allBshGrp )
                    
                if not mc.objExists( 'InBetweenText_%s_Grp' %sides ) :
                    textGrp = mc.createNode( 'transform' , n = 'InBetweenText_%s_Grp' %sides )
                    mc.parent( textGrp , allTextGrp )
                
                mc.select( sel , r = True )
                duppedNode = mc.duplicate( sel , rr = True )[0]
                bshNodes = mc.rename( duppedNode , '%s_%s' %(inBetweens , sides) )
                mc.parent( bshNodes , bshGrp )
                mc.move( 0 , yVal , 0 , bshNodes , r = True )
                
                txtCrv = mc.textCurves( n = '%s_' %bshNodes , f = fontText , t = bshNodes )[0]
                mc.select( txtCrv , r = True )
                txtBb = mc.exactWorldBoundingBox()
                mc.xform( txtCrv , cp = True )
                
                objWidth = abs(bb[0] - bb[3])
                txtWidth = abs(txtBb[0] - txtBb[3])
                
                tmpLoc = mc.spaceLocator()
                sclVal = objWidth/txtWidth
                mc.scale( sclVal , sclVal , sclVal , tmpLoc , r = True )
                mc.move( 0 , bb[1]+yVal , bb[5] , tmpLoc , r = True )
                
                mc.delete( mc.parentConstraint( tmpLoc , txtCrv ))
                mc.delete( mc.scaleConstraint( tmpLoc , txtCrv ))
                mc.parentConstraint( bshNodes , txtCrv , mo = True )
                
                mc.setAttr( '%s.overrideEnabled' % txtCrv, 1 )
                mc.setAttr( '%s.overrideDisplayType' % txtCrv, 2 )
                
                mc.delete( tmpLoc )
                mc.parent( txtCrv , textGrp )
                
                bshInBtw.append( bshNodes )

                yVal += yOffset
            lid_status = True
            mc.move( xVal , 0 , 0 , bshGrp , r=True )
            print bshGrp, '.......bshGrp'
        yVal = 0
    
    
    #------------------------------------------------------
    #   Create blendshape
    #------------------------------------------------------
    # print '# Generate >> Add Blendshape Node'
    bsnName = mc.blendShape( bshNode , bshBase , n = 'FacialAll_Bsn' )
    # print '# Generate >> Add Attribute at Locator' , len(bshNode)
    FacialAllGrp = utaCore.addFacialAllLoc(bshNode ,min = 0 , max = 10, at = 'float')
    mc.parent(FacialAllGrp, 'BshRigStill_Grp')


    if lid_status:
        indexUpLidDnLFT = bshNode.index( 'UpLidDn_L' )
        indexLoLidUpLFT = bshNode.index( 'LoLidUp_L' )
        indexUpLidDnRGT = bshNode.index( 'UpLidDn_R' )
        indexLoLidUpRGT = bshNode.index( 'LoLidUp_R' )


        mc.blendShape( bsnName , edit = True , ib = True , t = ( bshBase[0] , indexUpLidDnLFT , 'UpLidDnInBtw_L' , 0.5 ))
        mc.blendShape( bsnName , edit = True , ib = True , t = ( bshBase[0] , indexLoLidUpLFT , 'LoLidUpInBtw_L' , 0.5 ))
        mc.blendShape( bsnName , edit = True , ib = True , t = ( bshBase[0] , indexUpLidDnRGT , 'UpLidDnInBtw_R' , 0.5 ))
        mc.blendShape( bsnName , edit = True , ib = True , t = ( bshBase[0] , indexLoLidUpRGT , 'LoLidUpInBtw_R' , 0.5 ))

    if threeEye:
        indexUpLidDnCEN = bshNode.index( 'UpLidDn_C' )
        indexLoLidUpCEN = bshNode.index( 'LoLidUp_C' )
        mc.blendShape( bsnName , edit = True , ib = True , t = ( bshBase[0] , indexUpLidDnCEN , 'UpLidDnInBtw_C' , 0.5 ))
        mc.blendShape( bsnName , edit = True , ib = True , t = ( bshBase[0] , indexLoLidUpCEN , 'LoLidUpInBtw_C' , 0.5 ))

    # create shape editor group
    for name, indices in groupData.iteritems():
        try:
            groupInShapeEditor(bshNode='FacialAll_Bsn', 
                            name=name, 
                            indices=indices)
        except:
            pass

    mc.select( cl = True )
    return bsnName

    ## Connect BlendShape to FacialAll_Loc

# --------------------------------------------------------------------------------------------
#
# Connect Blendshape to Control
#
# --------------------------------------------------------------------------------------------


def connectBshNodeToLocAttr(eyebrow = True ,
                            eye     = True ,
                            mouth   = True ,
                            nose    = True ,
                            head    = True ,
                            threeEye = True,
                            bshName = 'FacialAll_Loc') :
     
    #------------------------------------------------------
    #   EYEBROW CONNECT
    #------------------------------------------------------
    if eyebrow == True :
        for side in ( 'L' , 'R' ) :
            mc.connectAttr('EyebrowUD_%s_Mdv.ox' %side , '%s.EyebrowUp_%s' %(bshName , side))
            mc.connectAttr('EyebrowUD_%s_Mdv.oy' %side , '%s.EyebrowDn_%s' %(bshName , side))
            mc.connectAttr('EyebrowIO_%s_Mdv.oy' %side , '%s.EyebrowIn_%s' %(bshName , side))
            mc.connectAttr('EyebrowIO_%s_Mdv.ox' %side , '%s.EyebrowOut_%s' %(bshName , side))
            mc.connectAttr('EyebrowFC_%s_Mdv.ox' %side , '%s.EyebrowTurnF_%s' %(bshName , side))
            mc.connectAttr('EyebrowFC_%s_Mdv.oy' %side , '%s.EyebrowTurnC_%s' %(bshName , side))
            
            mc.connectAttr('EyebrowInUD_%s_Mdv.ox' %side , '%s.EyebrowInnerUp_%s' %(bshName , side))
            mc.connectAttr('EyebrowInUD_%s_Mdv.oy' %side , '%s.EyebrowInnerDn_%s' %(bshName , side))
            mc.connectAttr('EyebrowInIO_%s_Mdv.ox' %side , '%s.EyebrowInnerIn_%s' %(bshName , side))
            mc.connectAttr('EyebrowInIO_%s_Mdv.oy' %side , '%s.EyebrowInnerOut_%s' %(bshName , side))
            mc.connectAttr('EyebrowInTurnF_%s_Rem.outValue' %side , '%s.EyebrowInnerTurnF_%s' %(bshName , side))
            mc.connectAttr('EyebrowInTurnC_%s_Rem.outValue' %side , '%s.EyebrowInnerTurnC_%s' %(bshName , side))
            
            mc.connectAttr('EyebrowMidUD_%s_Mdv.ox' %side , '%s.EyebrowMiddleUp_%s' %(bshName , side))
            mc.connectAttr('EyebrowMidUD_%s_Mdv.oy' %side , '%s.EyebrowMiddleDn_%s' %(bshName , side))
            mc.connectAttr('EyebrowMidIO_%s_Mdv.ox' %side , '%s.EyebrowMiddleIn_%s' %(bshName , side))
            mc.connectAttr('EyebrowMidIO_%s_Mdv.oy' %side , '%s.EyebrowMiddleOut_%s' %(bshName , side))
            mc.connectAttr('EyebrowMidTurnF_%s_Rem.outValue' %side , '%s.EyebrowMiddleTurnF_%s' %(bshName , side))
            mc.connectAttr('EyebrowMidTurnC_%s_Rem.outValue' %side , '%s.EyebrowMiddleTurnC_%s' %(bshName , side))
            
            mc.connectAttr('EyebrowOutUD_%s_Mdv.ox' %side , '%s.EyebrowOuterUp_%s' %(bshName , side))
            mc.connectAttr('EyebrowOutUD_%s_Mdv.oy' %side , '%s.EyebrowOuterDn_%s' %(bshName , side))
            mc.connectAttr('EyebrowOutIO_%s_Mdv.ox' %side , '%s.EyebrowOuterIn_%s' %(bshName , side))
            mc.connectAttr('EyebrowOutIO_%s_Mdv.oy' %side , '%s.EyebrowOuterOut_%s' %(bshName , side))
            mc.connectAttr('EyebrowOutTurnF_%s_Rem.outValue' %side , '%s.EyebrowOuterTurnF_%s' %(bshName , side))
            mc.connectAttr('EyebrowOutTurnC_%s_Rem.outValue' %side , '%s.EyebrowOuterTurnC_%s' %(bshName , side))
            
            mc.connectAttr('EyebrowPull_%s_Rem.outValue' %side , '%s.EyebrowPull_%s' %(bshName , side))
            mc.connectAttr('EyebrowSquintIn_%s_Rem.outValue' %side , '%s.EyebrowSquintIn_%s' %(bshName , side))
            mc.connectAttr('EyebrowSquintOut_%s_Rem.outValue' %side , '%s.EyebrowSquintOut_%s' %(bshName , side))
    

    #------------------------------------------------------
    #   EYE CONNECT
    #------------------------------------------------------

    if eye == True :
        for side in ( 'L' , 'R' ) :
            
            mc.connectAttr('EyeBallUD_%s_Mdv.ox' %side , '%s.EyeBallUp_%s' %(bshName , side))
            mc.connectAttr('EyeBallUD_%s_Mdv.oy' %side , '%s.EyeBallDn_%s' %(bshName , side))
            mc.connectAttr('EyeBallAmpIO_%s_Cmp.opr' %side , '%s.EyeBallIn_%s' %(bshName , side))
            mc.connectAttr('EyeBallAmpIO_%s_Cmp.opg' %side , '%s.EyeBallOut_%s' %(bshName , side))
            mc.connectAttr('EyeBallFC_%s_Mdv.ox' %side , '%s.EyeBallTurnF_%s' %(bshName , side))
            mc.connectAttr('EyeBallFC_%s_Mdv.oy' %side , '%s.EyeBallTurnC_%s' %(bshName , side))
            
            mc.connectAttr('EyeLidsUprAmpUD_%s_Cmp.opr' %side , '%s.UpLidUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprAmpUD_%s_Cmp.opg' %side , '%s.UpLidDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprInUD_%s_Mdv.ox' %side , '%s.UpLidInUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprMidUD_%s_Mdv.ox' %side , '%s.UpLidMidUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprOutUD_%s_Mdv.ox' %side , '%s.UpLidOutUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprInUD_%s_Mdv.oy' %side , '%s.UpLidInDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprMidUD_%s_Mdv.oy' %side , '%s.UpLidMidDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprOutUD_%s_Mdv.oy' %side , '%s.UpLidOutDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprTurnFC_%s_Mdv.ox' %side , '%s.UpLidTurnF_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprTurnFC_%s_Mdv.oy' %side , '%s.UpLidTurnC_%s' %(bshName , side))
            
            mc.connectAttr('EyeLidsLwrAllUD_%s_Cmp.opr' %side , '%s.LoLidUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrAllUD_%s_Cmp.opg' %side , '%s.LoLidDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrInUD_%s_Mdv.ox' %side , '%s.LoLidInUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrMidUD_%s_Mdv.ox' %side , '%s.LoLidMidUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrOutUD_%s_Mdv.ox' %side , '%s.LoLidOutUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrInUD_%s_Mdv.oy' %side , '%s.LoLidInDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrMidUD_%s_Mdv.oy' %side , '%s.LoLidMidDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrOutUD_%s_Mdv.oy' %side , '%s.LoLidOutDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrTurnFC_%s_Mdv.ox' %side , '%s.LoLidTurnF_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrTurnFC_%s_Mdv.oy' %side , '%s.LoLidTurnC_%s' %(bshName , side))

    if threeEye == True :
        for side in ('C') :
            
            mc.connectAttr('EyeBallUD_%s_Mdv.ox' %side , '%s.EyeBallUp_%s' %(bshName , side))
            mc.connectAttr('EyeBallUD_%s_Mdv.oy' %side , '%s.EyeBallDn_%s' %(bshName , side))
            mc.connectAttr('EyeBallAmpIO_%s_Cmp.opr' %side , '%s.EyeBallIn_%s' %(bshName , side))
            mc.connectAttr('EyeBallAmpIO_%s_Cmp.opg' %side , '%s.EyeBallOut_%s' %(bshName , side))
            mc.connectAttr('EyeBallFC_%s_Mdv.ox' %side , '%s.EyeBallTurnF_%s' %(bshName , side))
            mc.connectAttr('EyeBallFC_%s_Mdv.oy' %side , '%s.EyeBallTurnC_%s' %(bshName , side))
            
            mc.connectAttr('EyeLidsUprAmpUD_%s_Cmp.opr' %side , '%s.UpLidUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprAmpUD_%s_Cmp.opg' %side , '%s.UpLidDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprInUD_%s_Mdv.ox' %side , '%s.UpLidInUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprMidUD_%s_Mdv.ox' %side , '%s.UpLidMidUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprOutUD_%s_Mdv.ox' %side , '%s.UpLidOutUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprInUD_%s_Mdv.oy' %side , '%s.UpLidInDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprMidUD_%s_Mdv.oy' %side , '%s.UpLidMidDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprOutUD_%s_Mdv.oy' %side , '%s.UpLidOutDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprTurnFC_%s_Mdv.ox' %side , '%s.UpLidTurnF_%s' %(bshName , side))
            mc.connectAttr('EyeLidsUprTurnFC_%s_Mdv.oy' %side , '%s.UpLidTurnC_%s' %(bshName , side))
            
            mc.connectAttr('EyeLidsLwrAllUD_%s_Cmp.opr' %side , '%s.LoLidUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrAllUD_%s_Cmp.opg' %side , '%s.LoLidDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrInUD_%s_Mdv.ox' %side , '%s.LoLidInUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrMidUD_%s_Mdv.ox' %side , '%s.LoLidMidUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrOutUD_%s_Mdv.ox' %side , '%s.LoLidOutUp_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrInUD_%s_Mdv.oy' %side , '%s.LoLidInDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrMidUD_%s_Mdv.oy' %side , '%s.LoLidMidDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrOutUD_%s_Mdv.oy' %side , '%s.LoLidOutDn_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrTurnFC_%s_Mdv.ox' %side , '%s.LoLidTurnF_%s' %(bshName , side))
            mc.connectAttr('EyeLidsLwrTurnFC_%s_Mdv.oy' %side , '%s.LoLidTurnC_%s' %(bshName , side))

    #------------------------------------------------------
    #   MOUTH CONNECT
    #------------------------------------------------------

    if mouth == True :
        mc.connectAttr('MouthUD_Mdv.ox' , '%s.MouthUp' %bshName )
        mc.connectAttr('MouthUD_Mdv.oy' , '%s.MouthDn' %bshName )
        mc.connectAttr('MouthLR_Mdv.ox' , '%s.MouthLeft' %bshName )
        mc.connectAttr('MouthLR_Mdv.oy' , '%s.MouthRight' %bshName )
        mc.connectAttr('MouthFC_Mdv.ox' , '%s.MouthTurnF' %bshName )
        mc.connectAttr('MouthFC_Mdv.oy' , '%s.MouthTurnC' %bshName )
        mc.connectAttr('MouthClench_Mdv.ox' , '%s.MouthClench' %bshName )
        mc.connectAttr('MouthPull_Mdv.ox' , '%s.MouthPull' %bshName )
        mc.connectAttr('MouthU_Mdv.ox' , '%s.MouthU' %bshName )
        
        mc.connectAttr('LipsUprCurlIO_Mdv.ox' , '%s.UpLipsCurlIn' %bshName )
        mc.connectAttr('LipsUprCurlIO_Mdv.oy' , '%s.UpLipsCurlOut' %bshName )
        mc.connectAttr('LipsLftUprAttrUD_Mdv.ox' , '%s.UpLipsUp_L' %bshName )
        mc.connectAttr('LipsLftUprAttrUD_Mdv.oy' , '%s.UpLipsDn_L' %bshName )
        mc.connectAttr('LipsRgtUprAttrUD_Mdv.ox' , '%s.UpLipsUp_R' %bshName )
        mc.connectAttr('LipsRgtUprAttrUD_Mdv.oy' , '%s.UpLipsDn_R' %bshName )
        mc.connectAttr('LipsMidUprAttrUD_Mdv.ox' , '%s.UpLipsUp' %bshName )
        mc.connectAttr('LipsMidUprAttrUD_Mdv.oy' , '%s.UpLipsDn' %bshName )
        mc.connectAttr('LipsAllUprUD_Mdv.ox' , '%s.UpLipsAllUp' %bshName )
        mc.connectAttr('LipsAllUprUD_Mdv.oy' , '%s.UpLipsAllDn' %bshName )
        
        mc.connectAttr('LipsLwrCurlIO_Mdv.ox' , '%s.LoLipsCurlIn' %bshName )
        mc.connectAttr('LipsLwrCurlIO_Mdv.oy' , '%s.LoLipsCurlOut' %bshName )
        mc.connectAttr('LipsLftLwrAttrUD_Mdv.ox' , '%s.LoLipsUp_L' %bshName )
        mc.connectAttr('LipsLftLwrAttrUD_Mdv.oy' , '%s.LoLipsDn_L' %bshName )
        mc.connectAttr('LipsRgtLwrAttrUD_Mdv.ox' , '%s.LoLipsUp_R' %bshName )
        mc.connectAttr('LipsRgtLwrAttrUD_Mdv.oy' , '%s.LoLipsDn_R' %bshName )
        mc.connectAttr('LipsMidLwrAttrUD_Mdv.ox' , '%s.LoLipsUp' %bshName )
        mc.connectAttr('LipsMidLwrAttrUD_Mdv.oy' , '%s.LoLipsDn' %bshName )
        mc.connectAttr('LipsAllLwrUD_Mdv.ox' , '%s.LoLipsAllUp' %bshName )
        mc.connectAttr('LipsAllLwrUD_Mdv.oy' , '%s.LoLipsAllDn' %bshName )
        
        for side in ( 'L' , 'R' ) :
            mc.connectAttr('CheekUp_%s_Pma.o1' %side , '%s.CheekUp_%s' %(bshName , side))
            mc.connectAttr('CheekDn_%s_Pma.o1' %side , '%s.CheekDn_%s' %(bshName , side))
            mc.connectAttr('PuffIn_%s_Pma.o1' %side , '%s.PuffIn_%s' %(bshName , side))
            mc.connectAttr('PuffOut_%s_Pma.o1' %side , '%s.PuffOut_%s' %(bshName , side))
            mc.connectAttr('LipsCnrUD_%s_Mdv.ox' %side , '%s.CornerUp_%s' %(bshName , side))
            mc.connectAttr('LipsCnrUD_%s_Mdv.oy' %side , '%s.CornerDn_%s' %(bshName , side))
            mc.connectAttr('LipsCnrIO_%s_Mdv.ox' %side , '%s.CornerIn_%s' %(bshName , side))
            mc.connectAttr('LipsCnrIO_%s_Mdv.oy' %side , '%s.CornerOut_%s' %(bshName , side))
            mc.connectAttr('LipsPartIO_%s_Mdv.ox' %side , '%s.LipsPartIn_%s' %(bshName , side))
            mc.connectAttr('LipsPartIO_%s_Mdv.oy' %side , '%s.LipsPartOut_%s' %(bshName , side))
            mc.connectAttr('PuffAttrIO_%s_Mdv.ox' %side , '%s.PuckerIn_%s' %(bshName , side))
            mc.connectAttr('PuffAttrIO_%s_Mdv.oy' %side , '%s.PuckerOut_%s' %(bshName , side))
        
    #------------------------------------------------------
    #   NOSE CONNECT
    #------------------------------------------------------

    if nose == True :
        mc.connectAttr('NoseUD_Mdv.ox' , '%s.NoseUp' %bshName )
        mc.connectAttr('NoseUD_Mdv.oy' , '%s.NoseDn' %bshName )
        mc.connectAttr('NoseLR_Mdv.ox' , '%s.NoseLeft' %bshName )
        mc.connectAttr('NoseLR_Mdv.oy' , '%s.NoseRight' %bshName )
        mc.connectAttr('NoseStSq_Mdv.ox' , '%s.NoseStretch' %bshName )
        mc.connectAttr('NoseStSq_Mdv.oy' , '%s.NoseSquash' %bshName )
        mc.connectAttr('NoseSnarl_Rem.outValue' , '%s.NoseSnarl' %bshName )
        
        for side in ( 'L' , 'R' ) :
            mc.connectAttr('NoseUD_%s_Mdv.ox' %side , '%s.NoseUp_%s' %(bshName , side))
            mc.connectAttr('NoseUD_%s_Mdv.oy' %side , '%s.NoseDn_%s' %(bshName , side))
            
            mc.connectAttr('NoseFC_%s_Mdv.ox' %side , '%s.NoseTurnC_%s' %(bshName , side))
            mc.connectAttr('NoseFC_%s_Mdv.oy' %side , '%s.NoseTurnF_%s' %(bshName , side))
            
            mc.connectAttr('NoseTwist_%s_Rem.outValue' %side , '%s.NoseTwist_%s' %(bshName , side))
    
    #------------------------------------------------------
    #   HEAD CONNECT
    #------------------------------------------------------

    if head == True :
        mc.connectAttr('FaceUprTx_Mdv.ox' , '%s.HeadUprLeft' %bshName )
        mc.connectAttr('FaceUprTx_Mdv.oy' , '%s.HeadUprRight' %bshName )
        mc.connectAttr('FaceUprTy_Mdv.ox' , '%s.HeadUprStretch' %bshName )
        mc.connectAttr('FaceUprTy_Mdv.oy' , '%s.HeadUprSquash' %bshName )
        mc.connectAttr('FaceUprTz_Mdv.ox' , '%s.HeadUprFront' %bshName )
        mc.connectAttr('FaceUprTz_Mdv.oy' , '%s.HeadUprBack' %bshName )
       
        mc.connectAttr('FaceLwrTx_Mdv.ox' , '%s.HeadLwrLeft' %bshName )
        mc.connectAttr('FaceLwrTx_Mdv.oy' , '%s.HeadLwrRight' %bshName )
        mc.connectAttr('FaceLwrTy_Mdv.ox' , '%s.HeadLwrStretch' %bshName )
        mc.connectAttr('FaceLwrTy_Mdv.oy' , '%s.HeadLwrSquash' %bshName )
        mc.connectAttr('FaceLwrTz_Mdv.ox' , '%s.HeadLwrFront' %bshName )
        mc.connectAttr('FaceLwrTz_Mdv.oy' , '%s.HeadLwrBack' %bshName )

        mc.connectAttr('EyebrowPullIO_Mdv.ox' , '%s.HeadEyebrowPull' %bshName )

    ## connectLoLidUpDnLR
    connectLoLidUpDnLR()


def connectBshMoverCtrl(facialLoc = 'FacialAll_Loc',
                        bshName = 'FacialAll_Bsn',
                        connection = True ) :
    
    BshRigStillGrp = 'BshRigStill_Grp'
    i = 0
    addAttrLoc = []
    attrListAll = []
    if not mc.objExists(facialLoc):
        bshNameRefObj = mc.ls(bshName)
        for eachFclNode in bshNameRefObj:
            if eachFclNode == 'FacialAll_Bsn':
                bshNode = mc.listAttr(eachFclNode + '.w', m = True)
                for each in bshNode:
                    if not 'Btw' in each:
                        attrLists = mc.listConnections('%s.%s' % (eachFclNode, each) ,p  = True)
                        if len(attrLists) == 1:
                            attrList = attrLists[0]
                        else:
                            attrList = attrLists[-1]
                        attrListAll.append(attrList)
                        mc.disconnectAttr(attrList, '%s.%s' %(eachFclNode ,each))
                        addAttrLoc.append(each)
    
        FacialAllGrp , FacialAllLoc = utaCore.addFacialAllLoc(addAttrLoc ,min = 0 , max = 10, at = 'float')

        if not mc.objExists(BshRigStillGrp):
            BshRigStillGrp = mc.createNode('transform', n = 'BshRigStill_Grp', em = True)
            mc.parent(FacialAllGrp, BshRigStillGrp)
            mc.select(cl =True)
        else:
            mc.parent(FacialAllGrp, BshRigStillGrp)
            mc.select(cl =True)

        for each in attrListAll:
            mc.connectAttr(each, '%sShape.%s' % (FacialAllLoc, addAttrLoc[i]))
            i += 1

    else:
        FacialLocShape = '%sShape' % facialLoc
        attrLoc = utaCore.listAttribute(obj = [FacialLocShape])

        bshNameRef = mc.ls('*:%s' % bshName)
        bshNodeAll = {}
        if not bshNameRef:
            bshNameRefObj = mc.ls('Facial*_Bsn')

            for eachFacial in bshNameRefObj: 

                if eachFacial == 'FacialAll_Bsn':

                    bshNode = mc.listAttr(eachFacial + '.w', m = True)
                    bshNodeAll[eachFacial] = bshNode
                    if connection:
                        for each in attrLoc:
                            if not 'Btw' in each:
                                mc.connectAttr('%s.%s' % (FacialLocShape, each ), '%s.%s' %(eachFacial ,each))
                    else:
                        try:
                            for each in attrLoc:
                                if not 'Btw' in each:
                                    mc.disconnectAttr('%s.%s' % (FacialLocShape, each ), '%s.%s' %(eachFacial ,each))
                        except: pass
                else:

                    bshNode = mc.listAttr(eachFacial + '.w', m = True)
                    facialBshNode = eachFacial
                    if connection:
                        for each in attrLoc:
                            for eachFacial in bshNode:
                                if not 'Btw' in eachFacial:
                                    name, side, lastName = utaCore.splitName([eachFacial])
                                    if side == '_':
                                        side = ''
                                    else :
                                        side = side.split('_')[1]
                                        side = '_'+ side
                                    if each == (name + side):
                                        mc.connectAttr('%s.%s' % (FacialLocShape, each ), '%s.%s%s_%s' %(facialBshNode ,name, side, lastName))
                    else:
                        try:
                            for i in range(len(bshNode)):
                                if 'Btw' in bshNode[i]:
                                    pass
                                else:
                                    name, side, lastName = utaCore.splitName([bshNode[i]])
                                    if side == '_':
                                        side = ''
                                    else :
                                        side = side.split('_')[1]
                                        side = '_'+ side

                                    for eachAttrLoc in attrLoc:
                                        if eachAttrLoc == (name + side):
                                            mc.disconnectAttr('%s.%s' % (FacialLocShape, eachAttrLoc ), '%s.%s%s_%s' %(facialBshNode ,name, side, lastName))
                        except:pass

                    bshNodeAll[eachFacial] = bshNode   
  

def connectBshAttrCtrl( eyeBrowLFT = 'EyebrowBsh_L_Ctrl' ,
                        eyeBrowRGT = 'EyebrowBsh_R_Ctrl' ,
                        eyeLFT     = 'EyeBsh_L_Ctrl' ,
                        eyeRGT     = 'EyeBsh_R_Ctrl' ,
                        nose       = 'NoseBsh_Ctrl' ,
                        mouth      = 'MouthBsh_Ctrl' ,
                        mouthLFT   = 'MouthBsh_L_Ctrl' ,
                        mouthRGT   = 'MouthBsh_R_Ctrl' ,
                        faceUpr    = 'FaceUprBsh_Ctrl' ,
                        faceLwr    = 'FaceLwrBsh_Ctrl' ,
                        bshName    = 'FacialAll_Bsn' ) :
    
    bshNode = core.Dag(bshName)

    #------------------------------------------------------
    #   EYEBROW CONNECT
    #------------------------------------------------------
    for side in ( 'L' , 'R' ) :
        
        if side == 'L' :
            eyeBrowCtrl = core.Dag(eyeBrowLFT)
        else :
            eyeBrowCtrl = core.Dag(eyeBrowRGT)

        if eyeBrowCtrl :
            attrDict = { 'AllUD'    : ( 'EyebrowUp' , 'EyebrowDn' ) , 
                         'AllIO'    : ( 'EyebrowIn' , 'EyebrowOut' ) , 
                         'InnerUD'  : ( 'EyebrowInnerUp' , 'EyebrowInnerDn' ) , 
                         'InnerIO'  : ( 'EyebrowInnerIn' , 'EyebrowInnerOut' ) , 
                         'MiddleUD' : ( 'EyebrowMiddleUp' , 'EyebrowMiddleDn' ) , 
                         'MiddleIO' : ( 'EyebrowMiddleIn' , 'EyebrowMiddleOut' ) , 
                         'OuterUD'  : ( 'EyebrowOuterUp' , 'EyebrowOuterDn' ) , 
                         'OuterIO'  : ( 'EyebrowOuterIn' , 'EyebrowOuterOut' ) , 
                         'TurnUD'   : ( 'EyebrowTurnF' , 'EyebrowTurnC' ) }
            
            
            for attr in attrDict :
                mdv = rt.createNode( 'multiplyDivide' , '%s_%s_Mdv' %(attr , side))
                mdv.attr('i2x').value = 0.1
                mdv.attr('i2y').value = -0.1

                cmp = rt.createNode( 'clamp' , '%s_%s_Cmp' %(attr , side))
                cmp.attr('mng').value = -10
                cmp.attr('mxr').value = 10

                eyeBrowCtrl.attr('%s' %attr) >> cmp.attr('ipr')
                eyeBrowCtrl.attr('%s' %attr) >> cmp.attr('ipg')

                cmp.attr('opr') >> mdv.attr('i1x')
                cmp.attr('opg') >> mdv.attr('i1y')

                mdv.attr('ox') >> bshNode.attr('%s_%s' %(attrDict[attr][0] , side))
                mdv.attr('oy') >> bshNode.attr('%s_%s' %(attrDict[attr][1] , side))

    #------------------------------------------------------
    #   EYE CONNECT
    #------------------------------------------------------
    for side in ( 'L' , 'R' ) :
        
        if side == 'L' :
            eyeCtrl = core.Dag(eyeLFT)
        else :
            eyeCtrl = core.Dag(eyeRGT)

        if eyeCtrl :
            attrDict = { 'UpLidUD' : ( 'UpLidUp' , 'UpLidDn' ) , 
                         'LoLidUD' : ( 'LoLidUp' , 'LoLidDn' ) , 
                         'UpLidTW' : ( 'UpLidTurnF' , 'UpLidTurnC' ) , 
                         'LoLidTW' : ( 'LoLidTurnF' , 'LoLidTurnC' ) , 
                         'UpInnerUD' : ( 'UpLidInUp' , 'UpLidInDn' )  , 
                         'UpMidUD' : ( 'UpLidMidUp' , 'UpLidMidDn' ) , 
                         'UpOuterUD' : ( 'UpLidOutUp' , 'UpLidOutDn' ) , 
                         'LoInnerUD' : ( 'LoLidInUp' , 'LoLidInDn' ) , 
                         'LoMidUD' : ( 'LoLidMidUp' , 'LoLidMidDn' ) , 
                         'LoOuterUD' : ( 'LoLidOutUp' , 'LoLidOutDn' ) , 
                         'EyeBallUD' : ( 'EyeBallUp' , 'EyeBallDn' ) , 
                         'EyeBallIO' : ( 'EyeBallIn' , 'EyeBallOut' ) , 
                         'EyeBallTW' : ( 'EyeBallTurnF' , 'EyeBallTurnC' ) }
            
            for attr in attrDict :
                mdv = rt.createNode( 'multiplyDivide' , '%s_%s_Mdv' %(attr , side))
                if not attr == 'LoLidUD' :
                    mdv.attr('i2x').value = 0.1
                    mdv.attr('i2y').value = -0.1
                else :
                    mdv.attr('i2x').value = -0.1
                    mdv.attr('i2y').value = 0.1
                    
                cmp = rt.createNode( 'clamp' , '%s_%s_Cmp' %(attr , side))
                
                if not attr == 'LoLidUD' :
                    cmp.attr('mng').value = -10
                    cmp.attr('mxr').value = 10
                else :
                    cmp.attr('mnr').value = -10
                    cmp.attr('mxg').value = 10

                eyeCtrl.attr('%s' %attr) >> cmp.attr('ipr')
                eyeCtrl.attr('%s' %attr) >> cmp.attr('ipg')

                cmp.attr('opr') >> mdv.attr('i1x')
                cmp.attr('opg') >> mdv.attr('i1y')

                mdv.attr('ox') >> bshNode.attr('%s_%s' %(attrDict[attr][0] , side))
                mdv.attr('oy') >> bshNode.attr('%s_%s' %(attrDict[attr][1] , side))

    #------------------------------------------------------
    #   NOSE CONNECT
    #------------------------------------------------------
    noseCtrl = core.Dag(nose)

    if noseCtrl :
        attrDict = { 'NoseUD' : ( 'NoseUp' , 'NoseDn' ) , 
                     'NoseLR' : ( 'NoseLeft' , 'NoseRight' ) , 
                     'NoseSTQ' : ( 'NoseStretch' , 'NoseSquash' ) , 
                     'NoseLftUD' : ( 'NoseUp_L' , 'NoseDn_L' ) , 
                     'NoseLftTurn' : ( 'NoseTurnF_L' , 'NoseTurnC_L' ) , 
                     'NoseRgtUD' : ( 'NoseUp_R' , 'NoseDn_R' ) , 
                     'NoseRgtTurn' : ( 'NoseTurnF_R' , 'NoseTurnC_R' ) }
            
        for attr in attrDict :
            mdv = rt.createNode( 'multiplyDivide' , '%s_Mdv' %attr )
            mdv.attr('i2x').value = 0.1
            mdv.attr('i2y').value = -0.1
            
            cmp = rt.createNode( 'clamp' , '%s_Cmp' %attr )
            cmp.attr('mng').value = -10
            cmp.attr('mxr').value = 10

            noseCtrl.attr('%s' %attr) >> cmp.attr('ipr')
            noseCtrl.attr('%s' %attr) >> cmp.attr('ipg')

            cmp.attr('opr') >> mdv.attr('i1x')
            cmp.attr('opg') >> mdv.attr('i1y')

            mdv.attr('ox') >> bshNode.attr('%s' %(attrDict[attr][0] ))
            mdv.attr('oy') >> bshNode.attr('%s' %(attrDict[attr][1] ))

    #------------------------------------------------------
    #   MOUTH CONNECT
    #------------------------------------------------------
    mouthCtrl = core.Dag(mouth)

    if mouthCtrl :
        attrDict = { 'MouthUD' : ( 'MouthUp' , 'MouthDn' ) ,
                     'MouthLR' : ( 'MouthLeft' , 'MouthRight' ) ,
                     'MouthTurn' : ( 'MouthTurnF' , 'MouthTurnC' ) ,
                     'UpLipsUD' : ( 'UpLipsUp' , 'UpLipsDn' ) ,
                     'LoLipsUD' : ( 'LoLipsUp' , 'LoLipsDn' ) ,
                     'UpLipsCurlIO' : ( 'UpLipsCurlIn' , 'UpLipsCurlOut' ) ,
                     'LoLipsCurlIO' : ( 'LoLipsCurlIn' , 'LoLipsCurlOut' ) ,
                     'MouthClench' : 'MouthClench' ,
                     'MouthPull' : 'MouthPull' ,
                     'MouthU' : 'MouthU' ,
                     'EyebrowPullIO' : 'HeadEyebrowPull' }
            
        for attr in attrDict :
            mdv = rt.createNode( 'multiplyDivide' , '%s_Mdv' %attr )  
            mdv.attr('i2x').value = 0.1
            mdv.attr('i2y').value = -0.1 
            
            if not attr in ( 'MouthClench' , 'MouthPull' , 'MouthU' , 'EyebrowPullIO' ) :
                cmp = rt.createNode( 'clamp' , '%s_Cmp' %attr )
                cmp.attr('mng').value = -10
                cmp.attr('mxr').value = 10

                mouthCtrl.attr('%s' %attr) >> cmp.attr('ipr')
                mouthCtrl.attr('%s' %attr) >> cmp.attr('ipg')

                cmp.attr('opr') >> mdv.attr('i1x')
                cmp.attr('opg') >> mdv.attr('i1y')

                mdv.attr('ox') >> bshNode.attr('%s' %(attrDict[attr][0] ))
                mdv.attr('oy') >> bshNode.attr('%s' %(attrDict[attr][1] ))
                
            else :
                mouthCtrl.attr('%s' %attr) >> mdv.attr('i1x')
                mdv.attr('ox') >> bshNode.attr('%s' %(attrDict[attr] ))

    #------------------------------------------------------
    #   MOUTH SIDE CONNECT
    #------------------------------------------------------
    for side in ( 'L' , 'R' ) :
        
        if side == 'L' :
            mouthCtrl = core.Dag(mouthLFT)
        else :
            mouthCtrl = core.Dag(mouthRGT)

        if mouthCtrl :
            attrDict = { 'UpLipUD' : ( 'UpLipsUp' , 'UpLipsDn' ) ,
                         'LoLipUD' : ( 'LoLipsUp' , 'LoLipsDn' ) ,
                         'CornerUD' : ( 'CornerUp' , 'CornerDn' ) ,
                         'CornerIO' : ( 'CornerIn' , 'CornerOut' ) ,
                         'LipPartIO' : ( 'LipsPartIn' , 'LipsPartOut' ) ,
                         'CheekUprUD' : ( 'CheekUp' , 'CheekDn' ) ,
                         'PuffIO' : ( 'PuffIn' , 'PuffOut' ) ,
                         'PuckerIO' : ( 'PuckerIn' , 'PuckerOut' ) }
            
            for attr in attrDict :
                mdv = rt.createNode( 'multiplyDivide' , '%s_%s_Mdv' %(attr , side))
                mdv.attr('i2x').value = 0.1
                mdv.attr('i2y').value = -0.1
                
                cmp = rt.createNode( 'clamp' , '%s_%s_Cmp' %(attr , side))
                cmp.attr('mng').value = -10
                cmp.attr('mxr').value = 10

                mouthCtrl.attr('%s' %attr) >> cmp.attr('ipr')
                mouthCtrl.attr('%s' %attr) >> cmp.attr('ipg')

                cmp.attr('opr') >> mdv.attr('i1x')
                cmp.attr('opg') >> mdv.attr('i1y')

                mdv.attr('ox') >> bshNode.attr('%s_%s' %(attrDict[attr][0] , side))
                mdv.attr('oy') >> bshNode.attr('%s_%s' %(attrDict[attr][1] , side))

    #------------------------------------------------------
    #   FACE UPR CONNECT
    #------------------------------------------------------
    faceUprCtrl = core.Dag(faceUpr)

    if faceUprCtrl :
        attrDict = { 'tx' : ( 'HeadUprLeft' , 'HeadUprRight' ) , 
                     'ty' : ( 'HeadUprStretch' , 'HeadUprSquash' ) , 
                     'tz' : ( 'HeadUprFront' , 'HeadUprBack' ) }
            
        for attr in attrDict :
            mdv = rt.createNode( 'multiplyDivide' , 'HeadUpr%s_Mdv' %attr )
            mdv.attr('i2x').value = 0.1
            mdv.attr('i2y').value = -0.1

            cmp = rt.createNode( 'clamp' , 'HeadUpr%s_Cmp' %attr )
            cmp.attr('mng').value = -100
            cmp.attr('mxr').value = 100

            faceUprCtrl.attr('%s' %attr) >> cmp.attr('ipr')
            faceUprCtrl.attr('%s' %attr) >> cmp.attr('ipg')

            cmp.attr('opr') >> mdv.attr('i1x')
            cmp.attr('opg') >> mdv.attr('i1y')

            mdv.attr('ox') >> bshNode.attr('%s' %(attrDict[attr][0] ))
            mdv.attr('oy') >> bshNode.attr('%s' %(attrDict[attr][1] ))

    #------------------------------------------------------
    #   FACE LWR CONNECT
    #------------------------------------------------------
    faceLwrCtrl = core.Dag(faceLwr)

    if faceLwrCtrl :
        attrDict = { 'tx' : ( 'HeadLwrLeft' , 'HeadLwrRight' ) , 
                     'ty' : ( 'HeadLwrStretch' , 'HeadLwrSquash' ) , 
                     'tz' : ( 'HeadLwrFront' , 'HeadLwrBack' ) }
            
        for attr in attrDict :
            mdv = rt.createNode( 'multiplyDivide' , 'HeadLwr%s_Mdv' %attr )

            if not attr == 'ty' :
                mdv.attr('i2x').value = 1
                mdv.attr('i2y').value = -1
            else :
                mdv.attr('i2x').value = -1
                mdv.attr('i2y').value = 1

            cmp = rt.createNode( 'clamp' , 'HeadLwr%s_Cmp' %attr )

            if not attr == 'ty' :
                cmp.attr('mng').value = -100
                cmp.attr('mxr').value = 100
            else :
                cmp.attr('mnr').value = -100
                cmp.attr('mxg').value = 100

            faceLwrCtrl.attr('%s' %attr) >> cmp.attr('ipr')
            faceLwrCtrl.attr('%s' %attr) >> cmp.attr('ipg')

            cmp.attr('opr') >> mdv.attr('i1x')
            cmp.attr('opg') >> mdv.attr('i1y')

            mdv.attr('ox') >> bshNode.attr('%s' %(attrDict[attr][0] ))
            mdv.attr('oy') >> bshNode.attr('%s' %(attrDict[attr][1] ))

    mc.select( cl = True)

'''
# --------------------------------------------------------------------------------------------
#
# REMAP VALUE NODE ***
# Connect Blendshape to Control
#
# --------------------------------------------------------------------------------------------

def connectBshMoverCtrl( eyebrowAll = 'EyebrowBsh' ,
                         eyebrowIn = 'EyebrowInBsh' ,
                         eyebrowMid = 'EyebrowMidBsh' ,
                         eyebrowOut = 'EyebrowOutBsh' ,
                         bshName = 'FacialAll_Bsn' ) :

    bshNode = core.Dag(bshName)

    #------------------------------------------------------
    #   EYEBROW CONNECT
    #------------------------------------------------------

    eyebrowDicts = { eyebrowAll : ( 'Up' , 'Dn' , 'In' , 'Out' , 'TurnF' , 'TurnC' ) ,
                     eyebrowIn  : ( 'InnerUp' , 'InnerDn' , 'InnerIn' , 'InnerOut' , 'InnerTurnF' , 'InnerTurnC' ) ,
                     eyebrowMid : ( 'MiddleUp', 'MiddleDn', 'MiddleIn', 'MiddleOut', 'MiddleTurnF', 'MiddleTurnC') ,
                     eyebrowOut : ( 'OuterUp' , 'OuterDn' , 'OuterIn' , 'OuterOut' , 'OuterTurnF' , 'OuterTurnC' ) }


    if eyebrow == True :
        for side in ( 'L' , 'R' ) :
            for key in eyebrowDicts :
                for elem in eyebrowDicts[key] :

                    remap = rt.createNode( 'remapValue' , n = 'Eyebrow%s_%s_Rem' %(elem , side))
                    pma = rt.createNode( 'plusMinusAverage' , n = 'Eyebrow%s_%s_Pma' %(elem , side))
                    clamp = rt.createNode( 'clamp' , n = 'Eyebrow%s_%s_Cmp' %(elem , side))

                    ctrl = core.Dag(key)
                    # ctrl.attr() >> remap.attr('inValue')
                    remap.attr('outValue') >> pma.attr('i1[0]')
                    pma.attr('o1') >> clamp.attr('ipr')
                    clamp.attr('opr') >> bshNode.attr('Eyebrow%s_%s' %(elem , side))


            mc.connectAttr('EyebrowUD_%s_Mdv.ox' %side , '%s.EyebrowUp_%s' %(bshName , side))
            mc.connectAttr('EyebrowUD_%s_Mdv.oy' %side , '%s.EyebrowDn_%s' %(bshName , side))
            mc.connectAttr('EyebrowIO_%s_Mdv.oy' %side , '%s.EyebrowIn_%s' %(bshName , side))
            mc.connectAttr('EyebrowIO_%s_Mdv.ox' %side , '%s.EyebrowOut_%s' %(bshName , side))
            mc.connectAttr('EyebrowFC_%s_Mdv.ox' %side , '%s.EyebrowTurnF_%s' %(bshName , side))
            mc.connectAttr('EyebrowFC_%s_Mdv.oy' %side , '%s.EyebrowTurnC_%s' %(bshName , side))
            
            mc.connectAttr('EyebrowInUD_%s_Mdv.ox' %side , '%s.EyebrowInnerUp_%s' %(bshName , side))
            mc.connectAttr('EyebrowInUD_%s_Mdv.oy' %side , '%s.EyebrowInnerDn_%s' %(bshName , side))
            mc.connectAttr('EyebrowInIO_%s_Mdv.ox' %side , '%s.EyebrowInnerIn_%s' %(bshName , side))
            mc.connectAttr('EyebrowInIO_%s_Mdv.oy' %side , '%s.EyebrowInnerOut_%s' %(bshName , side))
            
            mc.connectAttr('EyebrowMidUD_%s_Mdv.ox' %side , '%s.EyebrowMiddleUp_%s' %(bshName , side))
            mc.connectAttr('EyebrowMidUD_%s_Mdv.oy' %side , '%s.EyebrowMiddleDn_%s' %(bshName , side))
            mc.connectAttr('EyebrowMidIO_%s_Mdv.ox' %side , '%s.EyebrowMiddleIn_%s' %(bshName , side))
            mc.connectAttr('EyebrowMidIO_%s_Mdv.oy' %side , '%s.EyebrowMiddleOut_%s' %(bshName , side))
            
            mc.connectAttr('EyebrowOutUD_%s_Mdv.ox' %side , '%s.EyebrowOuterUp_%s' %(bshName , side))
            mc.connectAttr('EyebrowOutUD_%s_Mdv.oy' %side , '%s.EyebrowOuterDn_%s' %(bshName , side))
            mc.connectAttr('EyebrowOutIO_%s_Mdv.ox' %side , '%s.EyebrowOuterIn_%s' %(bshName , side))
            mc.connectAttr('EyebrowOutIO_%s_Mdv.oy' %side , '%s.EyebrowOuterOut_%s' %(bshName , side))
'''

def connectLoLidUpDnLR():
    EyeCtrl = mc.ls('*:EyeLidsBsh_L_Jnt', '*:EyeLidsBsh_R_Jnt')
    ListsUp = mc.ls('*:FacialCtrlMover_EyeLidsLwrUp_L_Pma', '*:FacialCtrlMover_EyeLidsLwrUp_R_Pma')
    ListsDn = mc.ls('*:FacialCtrlMover_EyeLidsLwrDn_L_Pma', '*:FacialCtrlMover_EyeLidsLwrDn_R_Pma')
    i = 0
    for each in ListsUp:
        ## SplitName
        ListsSp = each.split('_')
        ListsName = ListsSp[1]
        ListsSide = ListsSp[2]

        ## CreateNode
        LoLidBcl = mc.createNode('blendColors' , n = '%sCon_%s_Bcl' % (ListsName, ListsSide))
        LoLidCmp = mc.createNode('clamp', n = '%sCon_%s_Cmp' % (ListsName, ListsSide))
        mc.setAttr('%s.maxR' % LoLidCmp, 40)
        mc.setAttr('%s.minG' % LoLidCmp, -40)       
        LoLidMdv = mc.createNode('multiplyDivide', n = '%sCon_%s_Mdv' % (ListsName, ListsSide))
        mc.setAttr('%s.input2X' % LoLidMdv, 0.04)
        mc.setAttr('%s.input2Y' % LoLidMdv, -0.002)

        ## ConnectAttr
        mc.connectAttr('%s.eyelidsFollow' % EyeCtrl[i], '%s.blender' % LoLidBcl)
        mc.connectAttr('%s.rotateX' % EyeCtrl[i], '%s.color1R' % LoLidBcl)
        mc.connectAttr('%s.rotateX' % EyeCtrl[i], '%s.color1G' % LoLidBcl)
        mc.connectAttr('%s.outputG' % LoLidBcl, '%s.inputG' % LoLidCmp)
        mc.connectAttr('%s.outputR' % LoLidBcl, '%s.inputR' % LoLidCmp)
        mc.connectAttr('%s.outputG' % LoLidCmp, '%s.input1Y' % LoLidMdv)
        mc.connectAttr('%s.outputR' % LoLidCmp, '%s.input1X' % LoLidMdv)
        mc.connectAttr('%s.outputY' % LoLidMdv, '%s.input1D[2]' % each)
        mc.connectAttr('%s.outputX' % LoLidMdv, '%s.input1D[2]' % ListsDn[i])
        i += 1


def wrapAndDupNewObj(bshNode = 'FacialAll_Bsn', targetObjs = [], part = '' , dupObj = '', *args):
    all_grp = []
    for targetObj in targetObjs:
        childObjs = mc.listRelatives(targetObj, c = True)  
        for childObj in childObjs:
            splitObj = childObj.split('_') 
            
            if len(splitObj) == 1: 
                childObj = childObj 

            elif len(splitObj) == 2: 

                if splitObj[-1] == 'L': 
                    childObj = childObj 

                elif splitObj[-1] == 'R': 
                    childObj = childObj 

                else: 
                    childObj = ''

            else: 

                if not splitObj[-1] == 'L' or 'R': 
                    childObj = '' 

            if childObj:
                InBtwName = 'InBtw'
                if InBtwName in childObj:
                    bshNodeName = childObj.split(InBtwName)
                    name = bshNodeName[0]
                    side = bshNodeName[-1]
                    childInBtwObj = '{}{}'.format(name, side)
                    childInBtwName = '{}{}{}'.format(name, InBtwName, side)
                    valueBsh = 0.5
                    mc.setAttr('{}.{}'.format (bshNode, childInBtwObj), valueBsh)
                    dupObjs = mc.duplicate(dupObj, n = '{}_{}'.format(childInBtwName, part),rr=True)

                    mc.delete(mc.parentConstraint(childObj, dupObjs[0],skipTranslate='x'))

                    all_grp.append(dupObjs[0])
                    mc.setAttr('{}.{}'.format (bshNode, childInBtwObj), 0)

                else:

                    mc.setAttr('{}.{}'.format (bshNode, childObj), 1)
                    dupObjs = mc.duplicate(dupObj, n = '{}_{}'.format(childObj, part),rr=True)
                    mc.delete(mc.parentConstraint(childObj, dupObjs[0],skipTranslate='x'))
                    all_grp.append(dupObjs[0])
                    mc.setAttr('{}.{}'.format (bshNode, childObj), 0)

    # Clear OrigShape of GeoDup
    for each in all_grp:
        obj = utaCore.listGeo(sels = each)
        for eachObj in obj:
            utaCore.cleanOrig(lists = [eachObj])

    return all_grp



