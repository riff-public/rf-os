import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  NONLINEAR DEFORM RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , name      = '' ,
                         uprTmpJnt = 'Upr_TmpJnt' ,
                         lwrTmpJnt = 'Lwr_TmpJnt' ,
                         parent    = '' ,
                         ctrlGrp   = 'Ctrl_Grp' , 
                         skinGrp   = 'Skin_Grp' , 
                         stillGrp  = 'Still_Grp' , 
                         side      = '' ,
                         size      = 1 
                 ):

        ##-- Info
        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        #-- Create Main Group
        self.ctrlGrp = rt.createNode( 'transform' , '%sDefCtrl%sGrp' %( name , side ))
        self.skinGrp = rt.createNode( 'transform' , '%sDefSkin%sGrp' %( name , side ))
        self.stillGrp = rt.createNode( 'transform' , '%sDefStill%sGrp' %( name , side ))
        self.jntStillGrp = rt.createNode( 'transform' , '%sJntDefStill%sGrp' %( name , side ))
        mc.parentConstraint( parent , self.ctrlGrp , mo = False )
        mc.scaleConstraint( parent , self.ctrlGrp , mo = False )
        mc.parentConstraint( parent , self.skinGrp , mo = False )
        mc.scaleConstraint( parent , self.skinGrp , mo = False )
        self.jntStillGrp.parent( self.stillGrp )
        self.stillGrp.snap( parent )
        
        #-- Create Joint
        self.uprJnt = rt.createJnt( '%sDefUpr%sJnt' %( name , side ) , uprTmpJnt )
        self.uprJntZro = rt.addGrp( self.uprJnt )
        self.uprJntOfst = rt.addGrp( self.uprJnt , 'Ofst' )

        self.midJnt = rt.createJnt( '%sDefMid%sJnt' %( name , side ))
        self.midJntZro = rt.addGrp( self.midJnt )
        self.midJntOfst = rt.addGrp( self.midJnt , 'Ofst' )

        self.lwrJnt = rt.createJnt( '%sDefLwr%sJnt' %( name , side ) , lwrTmpJnt )
        self.lwrJntZro = rt.addGrp( self.lwrJnt )
        self.lwrJntOfst = rt.addGrp( self.lwrJnt , 'Ofst' )

        mc.delete( mc.pointConstraint ( self.uprJnt , self.lwrJnt , self.midJntZro , mo = False ))

        #-- Create Still Joint
        #self.midStillJnt = rt.createJnt( '%sDefMidStill%sJnt' %( name , side ))
        #self.midStillJntZro = rt.addGrp( self.midStillJnt )

        #self.rootStillJnt = rt.createJnt( '%sDefRootStill%sJnt' %( name , side ) , parent )
        #self.rootStillJntZro = rt.addGrp( self.rootStillJnt )

        #mc.parent( self.midStillJntZro , self.rootStillJntZro , self.jntStillGrp )

        #-- Create Controls
        self.uprCtrl = rt.createCtrl( '%sDefUpr%sCtrl' %( name , side ) , 'cube' , 'green' )
        self.uprZro = rt.addGrp( self.uprCtrl )
        self.uprCtrl.addAttr( 'autoSquash' , 0 , 1 )

        self.uprZro.snap( self.uprJnt )

        #-- Adjust Shape Controls
        self.uprCtrl.scaleShape( size )

        #-- Create Aim UpVector Group
        self.midUpVecZro = rt.createNode( 'transform' , '%sDefMidUpVecZro%sGrp' %( name , side ))
        self.midUpVecGrp = rt.createNode( 'transform' , '%sDefMidUpVec%sGrp' %( name , side ))
        self.midAimGrp = rt.createNode( 'transform' , '%sDefMidAim%sGrp' %( name , side ))
        self.midAimGrp.parent( self.midUpVecGrp )
        self.midUpVecGrp.parent( self.midUpVecZro )
        self.midUpVecZro.snap( self.midJnt )

        #-- Rig process
        mc.pointConstraint( self.uprJnt , self.lwrJnt , self.midJntOfst , mo = True )
        mc.pointConstraint( self.uprJnt , self.lwrJnt , self.midUpVecGrp , mo = True )
        mc.orientConstraint( self.midAimGrp , self.midJntOfst , mo = False )
        mc.aimConstraint( self.lwrJnt , self.midAimGrp , aim = (0,-1,0) , u = (0,0,1) , wut='objectrotation' , wuo = self.midUpVecGrp , wu= (0,0,1) , mo = False )
        
        self.uprCtrl.attr('tx') >> self.uprJntOfst.attr('tx')
        self.uprCtrl.attr('ty') >> self.uprJntOfst.attr('ty')
        self.uprCtrl.attr('tz') >> self.uprJntOfst.attr('tz')

        self.uprDistGrp = rt.createNode( 'transform' , '%sDefUprDist%sGrp' %( name , side ))
        self.lwrDistGrp = rt.createNode( 'transform' , '%sDefLwrDist%sGrp' %( name , side ))
        mc.parentConstraint( self.uprJnt , self.uprDistGrp , mo = False )
        mc.parentConstraint( self.lwrJnt , self.lwrDistGrp , mo = False )

        self.uprDefaultDistGrp = rt.createNode( 'transform' , '%sDefUprDefaultDist%sGrp' %( name , side ))
        self.lwrDefaultDistGrp = rt.createNode( 'transform' , '%sDefLwrDefaultDist%sGrp' %( name , side ))
        self.uprDefaultDistGrp.snap( self.uprJnt )
        self.lwrDefaultDistGrp.snap( self.lwrJnt )
        mc.parentConstraint( parent , self.uprDefaultDistGrp , mo = True )
        mc.parentConstraint( parent , self.lwrDefaultDistGrp , mo = True )

        self.uprDist = rt.createNode( 'distanceBetween' , '%sDefUprDistance%sDist' %( name , side ))
        self.uprDefaultDist = rt.createNode( 'distanceBetween' , '%sDefUprDefaultDistance%sDist' %( name , side ))
        self.uprNrmDivMdv = rt.createNode( 'multiplyDivide' , '%sDefUprSqStNormDiv%sMdv' %( name , side ))
        self.uprPowMdv = rt.createNode( 'multiplyDivide' , '%sDefUprSqStPow%sMdv' %( name , side ))
        self.uprDivMdv = rt.createNode( 'multiplyDivide' , '%sDefUprSqStDiv%sMdv' %( name , side ))
        self.uprAutoBcl = rt.createNode( 'blendColors' , '%sDefUprSqStAuto%sBcl' %( name , side ))
        self.uprPma = rt.createNode( 'plusMinusAverage' , '%sDefUprSqSt%sPma' %( name , side ))

        self.uprDistGrp.attr('t') >> self.uprDist.attr('point1')
        self.lwrDistGrp.attr('t') >> self.uprDist.attr('point2')
        self.uprDefaultDistGrp.attr('t') >> self.uprDefaultDist.attr('point1')
        self.lwrDefaultDistGrp.attr('t') >> self.uprDefaultDist.attr('point2')
        self.uprDist.attr('distance') >> self.uprNrmDivMdv.attr('i1x')
        self.uprDefaultDist.attr('distance') >> self.uprNrmDivMdv.attr('i2x')
        self.uprNrmDivMdv.attr('ox') >> self.uprPowMdv.attr('i1x')
        self.uprPowMdv.attr('ox') >> self.uprDivMdv.attr('i2x')
        self.uprDivMdv.attr('ox') >> self.uprPma.attr('i1[0]')
        self.uprPma.attr('o1') >> self.midJnt.attr('sx')
        self.uprPma.attr('o1') >> self.midJnt.attr('sz')
        self.uprCtrl.attr('autoSquash') >> self.uprAutoBcl.attr('b')
        self.uprAutoBcl.attr('opr') >> self.uprPowMdv.attr('i2x')

        self.uprNrmDivMdv.attr('op').value = 2
        self.uprPowMdv.attr('op').value = 3
        self.uprDivMdv.attr('op').value = 2
        self.uprDivMdv.attr('i1x').value = 1
        self.uprAutoBcl.attr('c1r').value = 2
        self.uprCtrl.attr('autoSquash').value = 1

        #mc.connectAttr( '%s.t' %self.midJntOfst , '%s.t' %self.midStillJnt )
        #mc.connectAttr( '%s.r' %self.midJntOfst , '%s.r' %self.midStillJnt )
        #mc.connectAttr( '%s.s' %self.midJnt , '%s.s' %self.midStillJnt )

        #-- Adjust Hierarchy
        mc.parent( self.uprZro , self.midUpVecZro , self.ctrlGrp )
        mc.parent( self.uprJntZro , self.midJntZro , self.lwrJntZro , self.skinGrp )
        mc.parent( self.uprDefaultDistGrp , self.lwrDefaultDistGrp , self.uprDistGrp , self.lwrDistGrp , self.stillGrp )

        if mc.objExists( ctrlGrp ) :
            mc.parent( self.ctrlGrp , ctrlGrp )

        if mc.objExists( skinGrp ) :
            mc.parent( self.skinGrp , skinGrp )

        if mc.objExists( stillGrp ) :
            mc.parent( self.stillGrp , stillGrp )

        #-- Cleanup
        for obj in ( self.uprJntZro , self.uprJntZro , self.uprJntOfst , self.midJntZro , self.midJntZro , self.midJntOfst , 
                     self.lwrJntZro , self.lwrJntZro , self.lwrJntOfst , self.uprZro , self.midUpVecZro , self.midUpVecGrp , self.midAimGrp ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        self.uprCtrl.lockHideAttrs( 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
        
        mc.select( cl = True )