import maya.cmds as mc
import maya.mel as mm
from ncmel import core
from ncmel import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  EYE RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , eyeTrgTmpJnt  = 'EyeTrgt_TmpJnt' ,
                         eyeTmpJnt     = 'Eye_TmpJnt' ,
                         parent        = 'Head_Jnt' ,
                         ctrlGrp       = 'Ctrl_Grp' ,
                         skinGrp       = 'Skin_Grp' ,
                         elem          = '' ,
                         side          = '' ,
                         size          = 1 
                 ):

        ##-- Info
        elem = elem.capitalize()

        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        #-- Create Main Group Eye
        self.eyeCtrlGrp = rt.createNode( 'transform' , 'Eye%sCtrl%sGrp' %( elem , side ))
        mc.parentConstraint( parent , self.eyeCtrlGrp , mo = False )
        mc.scaleConstraint( parent , self.eyeCtrlGrp , mo = False )
    
        #-- Create Joint Eye 
        self.eyeAdjJnt = rt.createJnt( 'EyeAdj%s%sJnt' %( elem , side ) , eyeTmpJnt )
        self.eyeJnt = rt.createJnt( 'Eye%s%sJnt' %( elem , side ) , eyeTmpJnt )
        self.eyeLidJnt = rt.createJnt( 'EyeLid%s%sJnt' %( elem , side ) , eyeTmpJnt )
        
        mc.parent( self.eyeJnt , self.eyeAdjJnt )
        mc.parent( self.eyeLidJnt , self.eyeJnt )
        mc.parent( self.eyeAdjJnt , parent )

        #-- Create Controls Eye
        self.eyeCtrl = rt.createCtrl( 'Eye%s%sCtrl' %( elem , side ) , 'sphere' , 'red' )
        self.eyeGmbl = rt.addGimbal( self.eyeCtrl )
        self.eyeZro = rt.addGrp( self.eyeCtrl )
        self.eyeAim = rt.addGrp( self.eyeCtrl , 'Aim' )
        self.eyeZro.snap( self.eyeJnt )

        self.eyeTrgCtrl = rt.createCtrl( 'EyeTrgt%s%sCtrl' %( elem , side ) , 'circle' , 'red' )
        self.eyeTrgZro = rt.addGrp( self.eyeTrgCtrl )
        self.eyeTrgZro.snap( eyeTrgTmpJnt )

        #-- Adjust Shape Controls Eye
        for ctrl in ( self.eyeCtrl , self.eyeGmbl ) :
            ctrl.scaleShape( size * 0.5 )

        self.eyeTrgCtrl.scaleShape( size * 0.45 )
        self.eyeTrgCtrl.rotateShape( 90 , 0 , 0 )

        #-- Rig process Eye
        mc.parentConstraint( self.eyeGmbl , self.eyeJnt , mo = False )
        mc.scaleConstraint( self.eyeGmbl , self.eyeJnt , mo = False )

        self.eyeJnt.attr('ssc').value = 0
        self.eyeLidJnt.attr('ssc').value = 0
        
        mc.aimConstraint( self.eyeTrgCtrl , self.eyeAim , aim = (0,0,1) , u = (0,1,0) , wut = "objectrotation" , wu = (0,1,0) , wuo = self.eyeZro , mo = 1 )[0]
        
        self.eyeCtrl.addAttr( 'eyelidsFollow' , 0 , 1 )

        self.lidLftCons = core.Dag(mc.orientConstraint( parent , self.eyeLidJnt , mo = True )[0])
        
        #-- Adjust Hierarchy Eye
        mc.parent( self.eyeZro , self.eyeTrgZro , self.eyeCtrlGrp )
        mc.parent( self.eyeCtrlGrp , ctrlGrp )

        #-- Cleanup Eye
        for grp in ( self.eyeCtrlGrp , self.eyeZro , self.eyeTrgZro , self.eyeTrgZro ) :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for ctrl in ( self.eyeGmbl , self.eyeTrgCtrl , self.eyeTrgCtrl ) :
            ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        self.eyeCtrl.lockHideAttrs( 'v' )
    
    mc.select( cl = True )