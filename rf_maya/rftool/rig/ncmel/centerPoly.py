import maya.cmds as mc
import pymel.core as pm

def jnt2PolyMeshUI():

    width = 292
    
    if pm.window( 'Jnt2PolyMesh' , exists = True ) :
        pm.deleteUI( 'Jnt2PolyMesh' )
        
    window = pm.window( 'Jnt2PolyMesh', title = "Jnt2PolyMesh" , width = width , mnb = True , mxb = False , sizeable = True , rtf = True )
    
    layout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = ( 1 , width ))
    with layout :
        separatorA = pm.rowColumnLayout ( w = width , nc = 1 )
        with separatorA :
            pm.separator( vis = False )
        
        ALayout = pm.rowColumnLayout ( w = width , nc = 6 , columnWidth = [( 1 , 50 ) , ( 2 , 78 ) , ( 3 , 45 ) , ( 4 , 35 ) , ( 5 , 45 ) , ( 6 , 35 )])
        with ALayout :
            pm.text( label = 'Name   :' )
            pm.textField( 'nameTF' , en = True )
            pm.text( label = ' Side   :' )
            pm.textField( 'sideTF' , en = True )
            pm.text( label = ' Num   :' )
            pm.textField( 'numTF' , en = True )
            
        DLayout = pm.rowColumnLayout ( w = width , nc = 1 , columnWidth = [( 1 , 288 )])
        with DLayout :
            pm.separator( h = 5 , vis = False )
            pm.button( label = 'Generate Curve' , h = 30 , c = generate_curve )
            
        ELayout = pm.rowColumnLayout ( w = width , nc = 2 , columnWidth = [( 1 , 144 ),( 2 , 144 )])
        with ELayout :
            pm.separator( h = 5 , vis = False )
            pm.separator( h = 5 , vis = False )
            pm.button( label = 'Reverse Curve' , h = 30 , c = reverse_curve )
            pm.button( label = 'Create Joint' , h = 30 , c = create_joint_to_center )
                
    pm.showWindow( window )

def getInfo( *args ):
    name = pm.textField( 'nameTF' , q = True , text = True )
    side = pm.textField( 'sideTF' , q = True , text = True )
    num = int(pm.textField( 'numTF' , q = True , text = True ))
    
    if side :
        side = '_%s_' %side
    else :
        side = '_'
    
    return    {  'name' : name ,
                 'side' : side ,
                 'num' : num   }
    
def generate_curve( *args ):
    info = getInfo()
    
    sel = pm.ls( sl = True )
    if sel :
        edge = pm.polyToCurve()[0]
        pm.delete( edge , ch = True )
        mc.rebuildCurve( edge , rt = 0 , s = info['num'] )
        mc.setAttr( '%s.dispCV' %edge , 1 )
        pm.delete( edge , ch = True )
    
def reverse_curve( *args ):
    sels = mc.ls( sl = True )
    for sel in sels :
        mc.reverseCurve( sel )
    
    mc.select( sels )

def create_joint_to_center( *args ):
    info = getInfo()
    sels = mc.ls( sl = True )
    edgeList = [ sels[0] , sels[1] ]
    
    ep = 0
    eps = mc.getAttr( '%s.spans' %edgeList[0] )
    numLoop = 1
    jntList = [ ]
    
    while ep <= eps :
        posiA = mc.pointPosition( '%s.ep[%s]' %(edgeList[0] , ep))
        posiB = mc.pointPosition( '%s.ep[%s]' %(edgeList[1] , ep))
        
        pointA = mc.createNode( 'transform' )
        pointB = mc.createNode( 'transform' )
        
        mc.setAttr( '%s.t' %pointA , posiA[0] , posiA[1] , posiA[2] )
        mc.setAttr( '%s.t' %pointB , posiB[0] , posiB[1] , posiB[2] )
        
        jnt = mc.createNode( 'joint' , n = '%s%s%sJnt' %(info['name'],numLoop,info['side']) )
        mc.delete(mc.pointConstraint( pointA , pointB , jnt , mo = False ))
        mc.delete( pointA , pointB )
        
        jntList.append(jnt)
            
        ep +=1
        numLoop +=1
    
    for i in range(0,len(jntList)) :
        if not i == len(jntList)-1 :
            orientJnt( jntList[i] , jntList[i+1] )
            
        if not i == 0 :
            mc.parent( jntList[i] , jntList[i-1] ) 
        
    mc.delete( mc.orientConstraint ( jntList[-2] , jntList[-1] , mo = False ))
    mc.makeIdentity( jntList[0] , a = True )
    mc.delete( edgeList )
    
def orientJnt( src = '' , trgt = '' ) :
    objUpvec = mc.duplicate( src , rr = True )[0]
    tmpUpChldrn = mc.listRelatives( objUpvec , f = True )

    if tmpUpChldrn :
        mc.delete( tmpUpChldrn )
    
    mc.delete( mc.aimConstraint( trgt , src , aim = (0,1,0) , wuo = objUpvec , wut = 'objectrotation' , wu = (1,0,0) , u = (1,0,0) ))
    mc.makeIdentity( trgt , a = True )
    mc.delete( objUpvec )