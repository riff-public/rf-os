import pymel.core as pm
import maya.cmds as mc
import maya.mel as mm

class Node( object ) :

    def __init__( self , name ) :
        self.name = str( name )
        self.side = str( '_' )
        self.type = str( '' )
    
    def __str__( self ) :
        return str( self.name )
        
    def getName( self ) :
        return self.name

    def rename( self , newName ) :
        self.name = str(pm.rename(self.name , newName ))

    def getElemName( self ) :
        elem = self.name.split('_')

        lenElem = len(elem)
        if lenElem == 2 :
            self.name = elem[0]
            self.type = elem[-1]
        else :
            self.name = elem[0]
            self.side = '_%s_' %elem[1]
            self.type = elem[-1]
        
        return self.name , self.side , self.type

    def getExists( self ) :
        return pm.objExists( self.name )

    def getSide( self ) :
        sides = [ 'LFT' , 'RGT' , 'CEN' ,
                  'Lft' , 'Rgt' , 'Cen' , 
                  'lft' , 'rgt' , 'cen' ,
                  'L' , 'R' , 'C' ,'Fnt', 'Bck']
        
        currentSide = '_'
        for side in sides :
            if '_%s_' %side in self.name :
                currentSide = '_%s_' %side

        return currentSide

    def getNodeType( self ) :
        return pm.nodeType( self.name )

    def addTag( self , attrName = '' , str = '' ):
        pm.addAttr( self.name , ln = attrName , dt = 'string' , k = False )
        pm.setAttr( '%s.%s' %( self.name , attrName ) , str )

    def addBoolTag( self , attrName = '' , val = True ):
        pm.addAttr( self.name , ln = attrName , at = 'bool' , k = False )
        pm.setAttr( '%s.%s' %( self.name , attrName ) , val )

    def addMsgTag( self , msg = '' ):
        pm.addAttr( msg , ln = 'inbox' , dt = 'string' , k = False )
        pm.connectAttr( '%s.message' %self.name , '%s.inbox' %msg )

    def attr( self , attrName = '' ) :
        return Attribute( '%s.%s' % ( self.name , attrName ))

    def addAttr( self , attrName = '' , minVal = None , maxVal = None , chanelBox = True ) :
        pm.addAttr( self.name , ln = attrName , at = 'float' , k = chanelBox )

        if not minVal == None :
            pm.addAttr( '%s.%s' %( self.name , attrName ) , e = True , min = int(minVal))
        
        if not maxVal == None :
            pm.addAttr( '%s.%s' %( self.name , attrName ) , e = True , max = int(maxVal))

    def addTitleAttr( self , attrName = '' ) :
        if not pm.objExists( '%s.%s' %( self.name , attrName )) :
            pm.addAttr( self.name , ln = attrName , nn = '__%s' %attrName , at = 'float' , k = True )
            self.lockAttrs( attrName )

    def lockHideAttrs( self , *args ) :
        for arg in args :
            pm.setAttr( '%s.%s' % ( self , arg ) , l = True , k = False )
        
    def lockAttrs( self , *args ) :
        for arg in args :
            pm.setAttr( '%s.%s' % ( self , arg ) , l = True )

    def unlockAttrs( self , *args ) :
        for arg in args :
            pm.setAttr( '%s.%s' % ( self , arg ) , l = False )

    def lockHideKeyableAttrs( self ) :
        attrs = pm.listAttr( self.name , k = True )
        
        for attr in attrs :
            if pm.attributeQuery( attr.split( '.' )[ 0 ] , n = self.name , multi = True ) :
                continue
            else :
                pm.setAttr( '%s.%s' % ( self.name , attr ) , l = True , k = False )
    
    def lockKeyableAttrs( self ) :
        attrs = pm.listAttr( self.name , k = True )
        
        for attr in attrs :
            if pm.attributeQuery( attr.split( '.' )[ 0 ] , n = self.name , multi = True ) :
                continue
            else :
                pm.setAttr( '%s.%s' % ( self.name , attr ) , l = True )  

class Attribute( object ) :
    
    def __init__( self , attrName = '' ) :
        self.name = str( attrName )
    
    def __str__( self ) :
        return str( self.name )
    
    def __repr__( self ) :
        return str( self.name )

    def __rshift__( self , attr = '' ):
        if pm.objExists( attr ):
            conds = pm.listConnections( self.name , p = True )
            try :
                if conds and not attr in conds:
                    pm.connectAttr( self.name , attr , f = True )
                if conds and attr in conds:
                    print '%s already connected to %s ' %( self.name , attr )
                if not conds:
                    pm.connectAttr( self.name , attr , f = True )
            except:
                print 'Can\'t connect %s to %s' %( self.name , attr )
        else:
            pm.warning('%s doest\'t exists!' %attr )

    def getLock( self ) :
        return pm.getAttr( self.name , l = True )

    def setLock( self , lock = True ) :
        pm.setAttr( self.name , l = lock )

    lock = property( getLock , setLock )
    l = property( getLock , setLock )

    def getHide( self ) :
        return pm.getAttr( self.name , k = False )

    def setHide( self , hide = True ) :
        pm.setAttr( self.name , k = not hide )
        pm.setAttr( self.name , cb = not hide )

    hide = property( getHide , setHide )
    h = property( getHide , setHide )

    def setLockHide( self ) :
        pm.setAttr( self.name , l = True , k = False , cb = False )

    def getVal( self ):
        val = pm.getAttr( self.name )
        if type( val ) == type([]) or type( val ) == type(()):
            return val[0]
        else:
            return val

    def setVal( self , val ) :
        name , attr = self.name.split('.')
        lockAttr = pm.getAttr( self.name , l = True )
    
        if lockAttr == True :
            pm.setAttr( self.name , lock = False )
            pm.setAttr( self.name , val )
            pm.setAttr( self.name , lock = True )
        else :
            pm.setAttr( self.name , val )

    value = property( getVal , setVal )
    v = property( getVal , setVal )

    def setMin( self , minVal ) :
        pm.addAttr( self.name , e = True , minValue = minVal )

    def setMax( self , maxVal ) :
        pm.addAttr( self.name , e = True , maxValue = maxVal )

    def setRange( self , minVal , maxVal ) :
        pm.addAttr( self.name , e = True , minValue = minVal , maxValue = maxVal )

    def getConnections( self ) :
        return pm.listConnections( self.name )

    connect = property( getConnections )

    def getExists( self ) :
        return pm.objExists( self )

    exists = property( getExists )

class Dag( Node ) :

    def pynode( self ):
        return pm.general.PyNode( self.name )

    def getShape( self ) :
        shapes = pm.listRelatives( self.name , shapes = True )
        if shapes :
            if len( shapes ) > 1 :
                return shapes[ 0 ]
            else :
                return shapes[ 0 ]

    def correctShapeName( self ) :
        return pm.rename( self.shape , '%sShape' %self.name )

    shape = property( getShape )

    def getColor( self ) :
        return pm.getAttr( '%s.overrideColor' %self.shape )

    def setColor( self , col ) :
        colDict = { 'black'         : 1 ,
                    'gray'          : 2 ,
                    'softGray'      : 3 ,
                    'darkRed'       : 4 ,
                    'darkBlue'      : 5 ,
                    'blue'          : 6 ,
                    'darkGreen'     : 7 ,
                    'magenta'       : 9 ,
                    'brown'         : 11 ,
                    'red'           : 13 ,
                    'green'         : 14 ,
                    'white'         : 16 ,
                    'yellow'        : 17 ,
                    'cyan'          : 18 ,
                    'pink'          : 20 ,
                    'none'          : 0  }
        
        if type( col ) == type( str() ) :
            if col in colDict.keys() :
                colId = colDict[col]
            else :
                colId = 0
        else :
            colId = col
        
        pm.setAttr( '%s.overrideEnabled' %self.shape , 1 )
        pm.setAttr( '%s.overrideColor' %self.shape , colId )

    def getRotateOrder( self ) :
        dict = { 'xyz' : 0 ,
                 'yzx' : 1 ,
                 'zxy' : 2 ,
                 'xzy' : 3 ,
                 'yxz' : 4 ,
                 'zyx' : 5 }
                  
        id = pm.getAttr( '%s.rotateOrder' %self.name )
        
        for key in dict.keys() :
            if id == dict[ key ] :
                return id

    def setRotateOrder( self , rotateOrder ) :
        dict = { 'xyz' : 0 ,
                 'yzx' : 1 ,
                 'zxy' : 2 ,
                 'xzy' : 3 ,
                 'yxz' : 4 ,
                 'zyx' : 5 }
        
        if rotateOrder in dict.keys() :
            val = dict[ rotateOrder ]
        else :
            val = rotateOrder
            
        pm.setAttr( '%s.rotateOrder' %self.name , val )

    def getPivot( self ) :
        return pm.xform( self.name , q = True , t = True , ws = True  )

    @property
    def getWorldSpace( self ) :
        pars = self.getParent()
        null = Dag( mc.createNode( 'transform' ))
        null.parent(pars)
        tr = mc.getAttr( '%s.translate' %null )[0]
        ro = mc.getAttr( '%s.rotate' %null )[0]
        mc.delete( null )

        return { 'translate' : tr , 'rotate' : ro }

    def getDef( self , deformers ) :
        if deformers :
            listDef = [ deformers ]
        else :
            listDef = [ 'skinCluster' , 'tweak' , 'cluster' ]
        
        deformers = []
        
        for df in listDef :
            try :
                defNode = pm.listConnections( self.shape , type = df )[0]
                defSet = pm.listConnections( defNode , type = 'objectSet' )[0]
                
                deformers.append( defNode )
                deformers.append( defSet )
            except TypeError :
                continue
    
        return deformers

    def getParent( self ) :
        par = pm.listRelatives( self.name , p = True )

        if par :
            return par[0]

    def getChild( self ) :
        return pm.listRelatives( self.name , ad = True , type = 'transform' )

    def parent( self , target = '' ) :
        if target :
            pm.parent( self.name , target )

        pm.select( cl = True )

    def parentShape( self , target = '' ) :
        pm.parent( self.shape , target , r = True , s = True )
        
        target = Dag(target)
        target.correctShapeName()
        
        pm.delete( self.name )

    def freeze( self ) :
        pm.makeIdentity( self.name , a = True )
        pm.select( cl = True )

    def snap( self , target ) :
        pm.delete( pm.parentConstraint ( target , self.name , mo = False ))
        pm.select( cl = True )

    def snapPoint( self , target ) :
        pm.delete( pm.pointConstraint ( target , self.name , mo = False ))
        pm.select( cl = True )

    def snapOrient( self , target ) :
        pm.delete( pm.orientConstraint ( target , self.name , mo = False ))
        pm.select( cl = True )
    
    def snapScale( self , target ) :
        pm.delete( pm.scaleConstraint ( target , self.name , mo = False ))
        pm.select( cl = True )

    def snapAim( self , target , aim = (0,1,0) , upvec = (1,0,0)) :
        pm.delete( pm.aimConstraint ( target , self.name , mo = False  , aimVector = aim , upVector = upvec ))
        pm.select( cl = True )

    def snapJntOrient( self , target ) :
        pm.delete( pm.orientConstraint (( target , self.name ) , mo = False ))
        ro = pm.getAttr( '%s.rotate' %self.name )
        pm.setAttr( '%s.jointOrient' %self.name , ro[0] , ro[1] , ro[2] )
        pm.setAttr( '%s.rotate' %self.name , 0 , 0 , 0 )

    def snapWorldOrient( self ) :
        worGrp = pm.createNode( 'transform' )
        pm.delete( pm.orientConstraint (( worGrp , self.name ) , mo = False ))
        pm.delete( worGrp )

    def clearCons( self ) :
        listAttr = [ 'translateX' , 'translateY' , 'translateZ' , 'rotateX' , 'rotateY' , 'rotateZ' , 'scaleX' , 'scaleY' , 'scaleZ' ]
        
        for attr in listAttr :
            pm.setAttr( '%s.%s' %( self.name , attr ) , l = False )

        cons = pm.listRelatives( self.name , ad = True , type = 'constraint' )
        lists = mc.listHistory( self.name )

        if cons :
            for con in cons :
                for list in lists :
                    if con == list :
                        try :
                            pm.delete(con)
                        except :
                            pass

    def translateShape( self  , target ) :
        piv = self.getPivot()
        shape = self.getShape()

        degree = pm.getAttr( '%s.degree' %shape )
        spans = pm.getAttr( '%s.spans' %shape )

        cv = ( degree + spans ) - 1

        tx = pm.getAttr( '%s.tx' %target )
        ty = pm.getAttr( '%s.ty' %target )
        tz = pm.getAttr( '%s.tz' %target )

        pm.select( self.name + '.cv[0:%s]' %cv )
        pm.move( tx ,ty ,tz , r = True , os = True , wd = True )
        pm.select( cl = True )

    def rotateShape( self , rx , ry , rz ) :
        piv = self.getPivot()
        shape = self.getShape()

        degree = pm.getAttr( '%s.degree' %shape )
        spans = pm.getAttr( '%s.spans' %shape )

        cv = ( degree + spans ) - 1

        pm.select( self.name + '.cv[0:%s]' %cv )
        mc.rotate( rx ,ry ,rz , r = True , os = True ,  p = ( piv[0] , piv[1] , piv[2] ))
        pm.select( cl = True )

    def scaleShape( self , value ) :
        shape = self.getShape()

        degree = pm.getAttr( '%s.degree' %shape )
        spans = pm.getAttr( '%s.spans' %shape )

        cv = ( degree + spans ) - 1

        pm.select( self.name + '.cv[0:%s]' %cv )
        mc.scale( value , value , value , r = True )
        pm.select( cl = True )

    def show( self ) :
        pm.setAttr( '%s.v' %self.name , 1 )

    def hide( self ) :
        pm.setAttr( '%s.v' %self.name , 0 )

    def setDisplay( self , type ):
        typeDics = { 'normal'    : 0 ,
                     'template'  : 1 ,
                     'reference' : 2 }

        if type in typeDics.keys():
            pm.setAttr( '%s.overrideEnabled' %self.shape , 1 )
            pm.setAttr( '%s.overrideDisplayType' %self.shape , typeDics[type] )
            
            if type == 'normal':
                pm.setAttr( '%s.overrideEnabled' %self.shape , 1 )

    def hideAttributeAi( self ) :
        shape = self.getShape()

        for attr in ( 'aiRenderCurve' , 'aiCurveWidth' , 'aiSampleRate' , 'aiCurveShaderR' , 'aiCurveShaderG' , 'aiCurveShaderB' ) :
            if pm.objExists( '%s.%s' %(shape,attr)) :
                pm.setAttr( '%s.%s' %(shape,attr) , k = False , cb = False )