import pymel.core as pm

def blendShape_all_geo(srcGrp, desGrp):
    srcGrp = pm.PyNode(srcGrp)
    desGrp = pm.PyNode(desGrp)

    srcGeos = [geo.getParent() for geo in srcGrp.getChildren(ad=True, type='mesh')]
    desGeos = [geo.getParent() for geo in desGrp.getChildren(ad=True, type='mesh')]

    for geo in srcGeos:
        geoName = geo.nodeName()
        index_to_remove = []
        for i, des in enumerate(desGeos):
            desName = des.nodeName()
            if geoName.split(':')[-1] == desName.split(':')[-1]:
                exst_bsh = [h for h in des.listHistory() if h.type()=='blendShape']
                if exst_bsh:  # add target to old blendshape
                    bshNode = exst_bsh[0]
                    exst_targets = pm.blendShape(bshNode, q=True, t=True)
                    if geo.shortName() not in exst_targets:
                        # find proper index
                        index = bshNode.getGeometryIndices()[-1] + 1
                        # do add the target
                        try:
                            pm.blendShape(bshNode, edit=True, t=(des, index, geo, 1.0))
                            bshNode.w[index].set(1.0)
                        except Exception, e:
                            print e
                else:  # create new bsh
                    try:
                        bshNode = pm.blendShape(geo, des, foc=True)[0]
                        bshNode.w[0].set(1.0)
                    except Exception, e:
                            print e

                # turn on weight for the target
                

                # add index for this geo to be remove from the list
                index_to_remove.append(i)

        for r in index_to_remove:
            desGeos.pop(r)