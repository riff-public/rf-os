import random
import logging
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())

import pymel.core as pm
from nuTools import misc

def rig():
    sels = pm.selected(type='transform')
    colors = ['yellow', 'red', 'green', 'blue', 'lightBlue', 'orange', 'lightBrown', 'brown', 'pink', 'medBlue']
    for sel in sels:
        # get shape
        shp = sel.getShape()
        if not shp:
            logger.error('Cannot find shape: {}'.format(sel))
            continue
        logger.info('Shape: {}'.format(shp))

        # list abc connections
        abcNodes = shp.inputs(type='AlembicNode')
        if not abcNodes:
            pm.warning('Cannot find AlembicNode: {}'.format(shp))
        abcNode = abcNodes[0]
        logger.info('AlembicNode: {}'.format(abcNode))

        # get ramp shader
        sgs = shp.outputs(type='shadingEngine')
        rampShader = None
        for sg in sgs:
            shaders = sg.surfaceShader.inputs(type='rampShader')
            if shaders:
                rampShader = shaders[0]
                break
        logger.info('Ramp Shader: {}'.format(rampShader))

        # create Controller
        logger.info('Rigging...')
        pm.select(sel, r=True)
        rets = misc.createScaledSimpleCtrlAtObjectCenter( ctrlShp='crossCircle', 
            gimbal=True, 
            color=colors[random.randint(0, len(colors)-1)], 
            scale=1, 
            axis='+y',
            name=sel.nodeName(),
            createJnt=False, 
            rad=1, 
            useCenterPivot=True,
            parCons=True, 
            scaleCons=True, 
            directConnect=False, 
            geoVis=True)
        
        # add extra controls
        for ret in rets:
            ctrl = ret['ctrl']

            # abc offset 
            if abcNode:
                sepAttr = misc.addNumAttr(ctrl, '__Alembic__', 'double', hide=False, min=0, max=1, dv=0)
                sepAttr.lock()
                misc.addNumAttr(ctrl, 'abcOffset', 'long', hide=False, dv=abcNode.offset.get())
                pm.connectAttr(ctrl.abcOffset, abcNode.offset, f=True)
                logger.info('Alembic attr connected')

            # ramp controls
            if rampShader:
                for i in rampShader.color.getArrayIndices():
                    sepAttr = misc.addNumAttr(ctrl, '__Ramp{}__'.format(i), 'double', hide=False, min=0, max=1, dv=0)
                    sepAttr.lock()
                    current_pos = rampShader.color[i].color_Position.get()
                    pos_attr = misc.addNumAttr(ctrl, 'position_{}'.format(i), 'float', hide=False, min=0, max=1, dv=current_pos)
                    pm.connectAttr(pos_attr, rampShader.color[i].color_Position, f=True)

                    current_colors = rampShader.color[i].color_Color.get()
                    attrName = 'rampColor_{}'.format(i)
                    pm.addAttr(ctrl, ln=attrName, uac=True, at='float3', k=True)
                    pm.addAttr(ctrl, ln='rampColorR_{}'.format(i), at="float", parent=attrName, dv=current_colors[0], k=True)
                    pm.addAttr(ctrl, ln='rampColorG_{}'.format(i), at="float", parent=attrName, dv=current_colors[1], k=True)
                    pm.addAttr(ctrl, ln='rampColorB_{}'.format(i), at="float", parent=attrName, dv=current_colors[2], k=True)
                    
                    pm.connectAttr(ctrl.attr(attrName), rampShader.color[i].color_Color, f=True)

                logger.info('Ramp shader connected')