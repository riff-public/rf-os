import maya.cmds as mc
import maya.mel as mm

def run(prefix='prefix') :
	# prefix = 'aaaa'
	edges = mc.filterExpand(sm = 32)

	if len(edges) >= 2 :
		transform = edges[0].split('.')[0]
		meshShape = mc.listRelatives(transform, s = True)[0]
		e1 = edges[0].split('[')[-1].split(']')[0]
		e2 = edges[1].split('[')[-1].split(']')[0]

		# create curveFromMeshEdge
		cfme1 = mc.createNode('curveFromMeshEdge', n = '%s1_cfme' % prefix)
		cfme2 = mc.createNode('curveFromMeshEdge', n = '%s2_cfme' % prefix)

		# create loft
		loft = mc.createNode('loft', n = '%s_loft' % prefix)

		# set edge index
		mc.setAttr('%s.ei[0]' % cfme1, int(e1))
		mc.setAttr('%s.ei[0]' % cfme2, int(e2))

		# connect shape to curveFromMeshEdge
		mc.connectAttr('%s.worldMesh[0]' % meshShape, '%s.inputMesh' % cfme1, f = True)
		mc.connectAttr('%s.worldMesh[0]' % meshShape, '%s.inputMesh' % cfme2, f = True)

		# connect curveFromMeshEdge to loft
		mc.connectAttr('%s.outputCurve' % cfme1, '%s.inputCurve[0]' % loft, f = True)
		mc.connectAttr('%s.outputCurve' % cfme2, '%s.inputCurve[1]' % loft, f = True)

		# createNode pointOnSurfaceInfo
		poi = mc.createNode('pointOnSurfaceInfo', n = '%s_poi' % prefix)
		mc.setAttr('%s.turnOnPercentage' % poi, 1)
		mc.setAttr('%s.parameterU' % poi, 0.5)
		mc.setAttr('%s.parameterV' % poi, 0.5)

		# connect loft to poi
		mc.connectAttr('%s.outputSurface' % loft, '%s.inputSurface' % poi, f = True)

		# createNode aimConstraint
		aim = mc.createNode('aimConstraint', n = '%s_aim' % prefix)

		# connect poi to aimConstraint
		mc.connectAttr('%s.tangentV' % poi, '%s.target[0].targetTranslate' % aim, f = True)
		mc.connectAttr('%s.normal' % poi, '%s.worldUpVector' % aim, f = True)

		# create locator
		loc = mc.spaceLocator(n = '%s_loc' % prefix)[0]

		# connect poi to loc
		mc.connectAttr('%s.position' % poi, '%s.translate' % loc, f = True)
		mc.connectAttr('%s.constraintRotate' % aim, '%s.rotate' % loc, f = True)