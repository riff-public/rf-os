import maya.cmds as mc
import pymel.core as pm
import os
import re
from rf_maya.rftool.rig.utils.crvScript import crvFunc
reload(crvFunc)
from rf_maya.rftool.rig.utils.crvScript import ctrlData
reload(ctrlData)


class ShapeCtrl(object):
	
	def __init__(self):
		#self.set_value()
		if mc.window('Ctrl', q=True, ex=True):
			mc.deleteUI('Ctrl', window=True)
		
		crv_dict = crvFunc.load_ctrl_dict()
		crv_shape =  crv_dict.keys()
		mc.window('Ctrl' , t='Ctrl Tools' , s = 1)
		#mc.columnLayout( 'Ctrl',adjustableColumn=True )
		
		#curve shape ui
		mainColumm = mc.columnLayout(adj = True,cal = 'left')
		mc.optionMenu( 'crvShape',label = 'cruve shape :')
		for i in sorted(crv_shape):
			mc.menuItem( l = i)
		mc.setParent('..')
		
		#curve dictionary ui
		mc.rowLayout(numberOfColumns = 3 , adj = True)
		mc.text( l = 'add :', al = "left")
		mc.textField('newCtrl' ,text = "ctrl shape name", w = 240 , cc = self.confirm_dia_addShape)
		mc.button('Add', l='+', w=19, h=20 , c = self.confirm_dia_addShape)
		mc.setParent('..')
		
		#curve createName ctrl ui
		mc.rowLayout(numberOfColumns = 3 , adj = True)
		mc.text( l = 'name :', al = "left")
		mc.textField('nameCtrl' ,text = "name_ctrl" , w = 260  ,cc = self.create_ctrl)
		mc.setParent('..')
		
		#create ctrl and setShape button ui
		mc.rowLayout(numberOfColumns=2 , adj = True)
		mc.button('createCtrlBt', l='create', w=150, h=30, al='Left', c = self.create_ctrl)
		mc.button('setShapeBt', l='setShape', w=170, h=30, al='Left', c = self.set_shape)
		mc.setParent('..')
		mc.separator(style = 'none' , h = 2.5)
		
		#colour override ui
		color_palette = None
		columns = DisplayColorOverride.MAX_OVERRIDE_COLORS / 2
		rows = 2
		cell_width = 17
		cell_height = 17
		ShapeCtrl.color_palette = mc.palettePort(dim = ( columns, rows), t = 0,w = (columns*cell_width),h = (rows*cell_width),td = True, ced = False);
		
		for index in range(1, DisplayColorOverride.MAX_OVERRIDE_COLORS):
			color_component = mc.colorIndex(index, q=True)
			mc.palettePort(ShapeCtrl.color_palette,e = True, rgb = (index, color_component[0], color_component[1], color_component[2]))
		
		# Create the Override and Default buttons
		mc.button(l="Override",c=self.override)
		mc.separator(style = 'none' , h = 2.5)
		
		# scale crv fuction ui                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
		mc.rowLayout(numberOfColumns = 4 , adj = True)
		mc.text( l = 'scale ctrl:', al = "left")
		mc.textField('scaleScore' ,text = "0.1", w = 230)
		mc.button('upScale', l=u'\u25b2', w=25, h=20 , c = self.upScaleCtrl)
		mc.button('downSacle', l=u'\u25BC', w=25, h=20 , c = self.downScaleCtrl)
		mc.setParent('..')
		mc.separator(style = 'none' , h = 2.5)
		
		#create copyShape anfd mirrorShape ui
		mc.rowLayout(numberOfColumns=2 , adj = True)
		mc.button('coppyShpBt', l='copy shape', w=150, h=30, al='Left', c = self.copy_shape)
		mc.setParent('..')
		mc.separator(style = 'none' , h = 2.5)
		
		mc.rowLayout(numberOfColumns=2 , adj = True)
		#mc.text( l = 'Flip Shape :', al = "left")
		mc.radioButtonGrp( 'flip_shapes',l='Flip Shape :', labelArray3=['X', 'Y', 'Z'], nrb=3 , cw = [[1,60],[2,60],[3,60]], adj = 1,cat=[(1, "right",5)])
		mc.button('mirrorShpBt', l='mirror shape', w=100, h=30, al='Left', c = self.flip_shape)
		mc.setParent('..')
		mc.separator(style = 'none' , h = 2.5)
		
		#Read write ctrl working file button ui
		mc.rowLayout(numberOfColumns=2 , adj = True)
		mc.button('wrtCtrlWrkFld', l=' Write Shape  \n on Working File', w=150, h=50, al='Center', c = self.write_shp_work_fld)
		mc.button('rdCtrlWrkFld', l=' Read Shape  \n on Working File', w=170, h=50, al='Center', c = self.read_shp_work_fld)
		mc.setParent('..')
		mc.separator(style = 'none' , h = 2.5)
		
		#Read write select ctrl working file button ui
		mc.rowLayout(numberOfColumns=2 , adj = True)
		mc.button('wrtCtrlPublFld', l=' Write Select Shape  \n on Publish File', w=150, h=50, al='Center', c = self.write_shp_publ_fld)
		mc.button('rdCtrlPublFld', l=' Read Select Shape  \n on Publish File', w=170, h=50, al='Center', c = self.read_shp_publ_fld)
		mc.setParent('..')
		mc.separator(style = 'none' , h = 2.5)


		#mc.palettePort(color_palette,edit=True,rgbValue=(0, 0.6, 0.6, 0.6))
		mc.setParent('..')
		mc.separator(style = 'none' , h = 2.5 ,p = mainColumm)
		
		mc.showWindow('Ctrl')
		
		mc.window('Ctrl' ,e=True)
		#self.refresh_display()

	def create_ctrl(self, *args):
		getOptMn = mc.optionMenu('crvShape' , q = 1 , v = 1)
		getNameCtrl = mc.textField('nameCtrl',q=1,tx=1)
		
		
		crvFunc.create_curve(shape = getOptMn, name = getNameCtrl)
		color_index = mc.palettePort(ShapeCtrl.color_palette, q=True, setCurCell=True)
		DisplayColorOverride.override_color(color_index)
	
	def confirm_dia_addShape(self, *args):
		crv_dict = crvFunc.load_ctrl_dict()
		crv_shape =  crv_dict.keys()
		getNewNameCtrl = mc.textField('newCtrl' ,q = 1 ,tx = 1)

		if getNewNameCtrl in crv_shape:
			a = mc.confirmDialog(t = 'Curve Shape', m = 'This shape already exist\nDo you want to override?', b = ['Yes','No'], db = 'Yes', cb = 'No', ds = 'No')
			if a == 'Yes':
				crvFunc.add_ctrl_to_dict(shape = getNewNameCtrl ,crv = mc.ls(sl = True)[0])
			else:pass
		else:
			crvFunc.add_ctrl_to_dict(shape = getNewNameCtrl ,crv = mc.ls(sl = True)[0])
		
	def set_shape(self, *args):
		getOptMn = mc.optionMenu('crvShape' , q = 1 , v = 1)
		
		crvFunc.select_set_shape(shape = getOptMn)

	def override(self, *args):
		color_index = mc.palettePort(ShapeCtrl.color_palette, q=True, setCurCell=True)
		DisplayColorOverride.override_color(color_index)
		
	
	def upScaleCtrl(self, *args):
		getScaleValCtrl = mc.textField('scaleScore',q=1,tx=1)
		a = 1 + float(getScaleValCtrl)
		print a
		
		crvFunc.scaleCtrl(objs = mc.ls(sl = True) ,size = float(a))
		
	def downScaleCtrl(self, *args):
		getScaleValCtrl = mc.textField('scaleScore',q=1,tx=1)
		a = 1 - float(getScaleValCtrl)
		
		crvFunc.scaleCtrl(objs = mc.ls(sl = True) ,size = float(a))
		
	def copy_shape(self, *args):		
		crvFunc.select_copy_crv_shape()
		
	def flip_shape(self, *args):
		getRdoBt = mc.radioButtonGrp('flip_shapes', q =1 ,sl = True)
		sels = mc.ls(sl = True)
		selCvLst = []	
		for sel in sels: 
			selCv = '{}.cv[:]' .format(sel)		
			selCvLst.append(selCv)	
		
		if 	getRdoBt == 1:
			mc.scale(-1,1,1,selCvLst)
			
		elif getRdoBt == 2:
			mc.scale(1,-1,1,selCvLst)
			
		elif getRdoBt == 3:
			mc.scale(1,1,-1,selCvLst)

		
	def write_shp_publ_fld(self, *args):
		objs = mc.ls(sl = True)
		
		if objs:
			ctrlData.write_ctrl_each_publFld()
		else:
			ctrlData.write_ctrl_dataFld()
	
	def read_shp_publ_fld(self, *args):
		objs = mc.ls(sl = True)
		
		if objs:
			ctrlData.read_ctrl_each_publFld()
		else:
			ctrlData.read_ctrl_dataFld()
			
	def write_shp_work_fld(self, *args):
		objs = mc.ls(sl = True)
		
		if objs:
			ctrlData.write_ctrl_each_workFld()
		else:
			ctrlData.write_ctrl_workFld()
	
	def read_shp_work_fld(self, *args):
		objs = mc.ls(sl = True)
		
		if objs:
			ctrlData.read_ctrl_each_workFld()
		else:
			ctrlData.read_ctrl_workFld()


class DisplayColorOverride:
	
	MAX_OVERRIDE_COLORS = 32
	
	@staticmethod
	def override_color(color_index):
		if (color_index >= DisplayColorOverride.MAX_OVERRIDE_COLORS):
			om.MGlobal.displayError("Color index out-of-range (must be less than 32)")
			return False
		
		selection = mc.ls(selection=True)
		if not selection:
			om.MGlobal.displayError("No objects selected")
			return False

		for obj in selection:
			shapeNodes = mc.listRelatives(obj, shapes=True)
			
			for shape in shapeNodes:
				try:
					mc.setAttr("{0}.overrideEnabled".format(shape), True)
					mc.setAttr("{0}.overrideColor".format(shape), color_index)
				except:
					om.MGlobal.displayWarning("Failed to override color: {0}".format(shape))
		
		return True
		
	@staticmethod
	def use_defaults():
		shapes = DisplayColorOverride.shape_nodes()
		if shapes == None:
			om.MGlobal.displayError("No objects selected")
			return False
			
		for shape in shapes:
			try:
				mc.setAttr("{0}.overrideEnabled".format(shape), False)
			except:
				om.MGlobal.displayWarning("Failed to restore defaults: {0}".format(shape))
			
		return True
		
	@staticmethod
	def shape_nodes():
		selection = mc.ls(selection=True)
		if not selection:
			return None
		  
		shapes = []   
		for obj in selection:
			shapes.extend(mc.listRelatives(obj, shapes=True))
						  
		return shapes
					

def show():
	ShapeCtrl()
#ShapeCtrl()
