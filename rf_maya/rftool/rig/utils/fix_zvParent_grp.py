import pymel.core as pm

from nuTools import misc

def add_to_selected():
    '''
    from rf_maya.rftool.rig.utils import fix_zvParent_grp
    fix_zvParent_grp.add_to_selected()
    '''
    sels = pm.selected(type='transform')
    default_values = {'t':0.0, 'r':0.0, 's':1.0}
    lightBlue = (0.0, 1.0, 1.0)
    for ctrl in sels:
        ctrlName = ctrl.nodeName()
        parentGrp = ctrl.getParent()
        parGrpName = ''
        if ctrlName == 'AllMover_Ctrl':
            parGrpName = 'AllMover_Grp'
        else:
            # figure out the parGrp name
            np = ctrlName.split('_')
            side = ''
            if len(np) > 2:
                side = '_%s' %np[1]

            parGrpName = '%s%sPars%s_Grp' %(np[0], np[-1].title(), side)

        if parentGrp.nodeName() == parGrpName and misc.hasCleanChannelBox(parentGrp) and parentGrp.outlinerColor.get()==lightBlue:
            pm.warning('%s already has parGrp' %ctrlName)
            continue
        # actually create the grp
        currentIndx = parentGrp.getChildren(type='transform').index(ctrl)
        parGrp = pm.group(ctrl, r=True, n=parGrpName)
        pm.xform(parGrp, os=True, piv=(0, 0, 0))
        pm.reorder(parGrp, r=currentIndx+1)
        parGrp.useOutlinerColor.set(True)
        parGrp.outlinerColor.set(lightBlue)  # set it to blue

