import maya.cmds as mc
import maya.mel as mm

def run () :
    filename = mc.file ( q = True , sn = True )
    reffile = mc.file ( filename , q = True , r = True )
    size = len(reffile)
    
    if size == 0 :
        removeNamespace ()
    else :
        refNamespace ()

def refNamespace () :
    filename = mc.file ( q = True , sn = True )
    reffile = mc.file ( filename , q = True , r = True )

    for ref in reffile :
        refprefix = mc.referenceQuery ( ref , referenceNode = True ).replace ( "RN" , "" )
        mc.file( ref , ir = True )
        mc.namespace( mv = [ refprefix ,":" ] ,force = True )

def removeNamespace () :
    sel = mc.ls ( sl = True )
    size = len(sel)
    
    if size == 0 :
        mc.warning( 'No Select Object. Please Select One Object.' )
    else :
        refprefix = sel[0].split(':')[0]
        mc.namespace( mv = [ refprefix ,":" ] ,force = True )

def namespace () :
    run()