import maya.cmds as mc
import maya.mel as mm

import core
reload(core)

def curveShape( shape = '' ) :
    crvDict = { 'sphere'        : [(0,1.250064,0,),(0,1,0,),(1.93883e-007,0.92388,-0.382683,),(3.58248e-007,0.707107,-0.707107,),(4.68074e-007,0.382683,-0.923879,),(5.06639e-007,0,-1,),(0,0,-1.250064,),(5.06639e-007,0,-1,),(4.68074e-007,-0.382683,-0.923879,),(3.58248e-007,-0.707107,-0.707107,),(1.93883e-007,-0.92388,-0.382683,),(0,-1,0,),(0,-1.250064,0,),(0,-1,0,),(-5.70243e-008,-0.92388,0.382683,),(-1.05367e-007,-0.707107,0.707107,),(-1.37669e-007,-0.382683,0.92388,),(-1.49012e-007,0,1,),(0,0,1.250064,),(-1.49012e-007,0,1,),(-1.37669e-007,0.382683,0.92388,),(-1.05367e-007,0.707107,0.707107,),(-5.70243e-008,0.92388,0.382683,),(0,1,0,),(0.382683,0.92388,0,),(0.707107,0.707107,0,),(0.92388,0.382683,0,),(1,0,0,),(1.250064,0,0,),(1,0,0,),(0.92388,-0.382683,0,),(0.707107,-0.707107,0,),(0.382683,-0.92388,0,),(0,-1,0,),(0,-1.250064,0,),(0,-1,0,),(-0.382683,-0.92388,-1.36858e-007,),(-0.707107,-0.707107,-2.52881e-007,),(-0.92388,-0.382683,-3.30405e-007,),(-1,0,-3.57628e-007,),(-1.250064,0,0,),(-1,0,-3.57628e-007,),(-0.92388,0,0.382683,),(-0.707107,0,0.707107,),(-0.382684,0,0.923879,),(-1.49012e-007,0,1,),(0.382683,0,0.92388,),(0.707107,0,0.707107,),(0.92388,0,0.382683,),(1,0,0,),(0.92388,0,-0.382683,),(0.707107,0,-0.707106,),(0.382684,0,-0.923879,),(5.06639e-007,0,-1,),(-0.382683,0,-0.92388,),(-0.707106,0,-0.707107,),(-0.923879,0,-0.382684,),(-1,0,-3.57628e-007,),(-0.92388,0.382683,-3.30405e-007,),(-0.707107,0.707107,-2.52881e-007,),(-0.382683,0.92388,-1.36858e-007,),(0,1,0)] ,
                'cylinder'      : [(-2.98023e-008,0.5,1,),(0.309017,0.5,0.951057,),(0.587785,0.5,0.809017,),(0.809017,0.5,0.587785,),(0.951057,0.5,0.309017,),(1,0.5,0,),(0.951057,0.5,-0.309017,),(0.809018,0.5,-0.587786,),(0.587786,0.5,-0.809017,),(0.309017,0.5,-0.951057,),(0,0.5,-1,),(-0.309017,0.5,-0.951057,),(-0.587785,0.5,-0.809017,),(-0.809017,0.5,-0.587785,),(-0.951057,0.5,-0.309017,),(-1,0.5,0,),(-0.951057,0.5,0.309017,),(-0.809017,0.5,0.587785,),(-0.587785,0.5,0.809017,),(-0.309017,0.5,0.951057,),(-2.98023e-008,0.5,1,),(-2.98023e-008,-0.5,1,),(0.309017,-0.5,0.951057,),(0.587785,-0.5,0.809017,),(0.698401,-0.5,0.698401,),(0.698401,0.5,0.698401,),(0.698401,-0.5,0.698401,),(0.809017,-0.5,0.587785,),(0.951057,-0.5,0.309017,),(1,-0.5,0,),(1,0.5,0,),(1,-0.5,0,),(0.951057,-0.5,-0.309017,),(0.809018,-0.5,-0.587786,),(0.698402,-0.5,-0.698402,),(0.698402,0.5,-0.698402,),(0.698402,-0.5,-0.698402,),(0.587786,-0.5,-0.809017,),(0.309017,-0.5,-0.951057,),(0,-0.5,-1,),(0,0.5,-1,),(0,-0.5,-1,),(-0.309017,-0.5,-0.951057,),(-0.587785,-0.5,-0.809017,),(-0.698401,-0.5,-0.698401,),(-0.698401,0.5,-0.698401,),(-0.698401,-0.5,-0.698401,),(-0.809017,-0.5,-0.587785,),(-0.951057,-0.5,-0.309017,),(-1,-0.5,0,),(-1,0.5,0,),(-1,-0.5,0,),(-0.951057,-0.5,0.309017,),(-0.809017,-0.5,0.587785,),(-0.698401,-0.5,0.698401,),(-0.698401,0.5,0.698401,),(-0.698401,-0.5,0.698401,),(-0.587785,-0.5,0.809017,),(-0.309017,-0.5,0.951057,),(-2.98023e-008,-0.5,1,),(-2.98023e-008,0.5,1)] ,
                'stick'         : [(0,0,0),(0,1.499085,0),(0.0523598,1.501829,0),(0.104146,1.510032,0),(0.154791,1.523602,0),(0.20374,1.542392,0),(0.250457,1.566195,0),(0.29443,1.594752,0),(0.335177,1.627748,0),(0.372252,1.664823,0),(0.405248,1.70557,0),(0.433805,1.749543,0),(0.457608,1.79626,0),(0.476398,1.845209,0),(0.489968,1.895854,0),(0.498171,1.94764,0),(0.500915,2,0),(0.498171,2.05236,0),(0.489968,2.104146,0),(0.476398,2.154791,0),(0.457608,2.20374,0),(0.433805,2.250457,0),(0.405248,2.29443,0),(0.372252,2.335177,0),(0.335177,2.372252,0),(0.29443,2.405248,0),(0.250457,2.433805,0),(0.20374,2.457608,0),(0.154791,2.476398,0),(0.104146,2.489968,0),(0.0523598,2.498171,0),(0,2.500915,0),(-0.0523598,2.498171,0),(-0.104146,2.489968,0),(-0.154791,2.476398,0),(-0.20374,2.457608,0),(-0.250457,2.433805,0),(-0.29443,2.405248,0),(-0.335177,2.372252,0),(-0.372252,2.335177,0),(-0.405248,2.29443,0),(-0.433805,2.250457,0),(-0.457608,2.20374,0),(-0.476398,2.154791,0),(-0.489968,2.104146,0),(-0.498171,2.05236,0),(-0.500915,2,0),(-0.498171,1.94764,0),(-0.489968,1.895854,0),(-0.476398,1.845209,0),(-0.457608,1.79626,0),(-0.433805,1.749543,0),(-0.405248,1.70557,0),(-0.372252,1.664823,0),(-0.335177,1.627748,0),(-0.29443,1.594752,0),(-0.250457,1.566195,0),(-0.20374,1.542392,0),(-0.154791,1.523602,0),(-0.104146,1.510032,0),(-0.0523598,1.501829,0),(0,1.499085,0),(0,2.576874,0),(0,2,0),(0.576874,2,0),(-0.576874,2,0)] ,
                'circle'        : [(1.125,0,0),(1.004121,0,0),(0.991758,0,-0.157079),(0.954976,0,-0.31029),(0.894678,0,-0.455861) ,(0.812351,0,-0.590207),(0.710021,0,-0.710021),(0.590207,0,-0.812351),(0.455861,0,-0.894678),(0.31029,0,-0.954976),(0.157079,0,-0.991758),(0,0,-1.004121),(0,0,-1.125),(0,0,-1.004121),(-0.157079,0,-0.991758),(-0.31029,0,-0.954976),(-0.455861,0,-0.894678),(-0.590207,0,-0.812351),(-0.710021,0,-0.710021),(-0.812351,0,-0.590207),(-0.894678,0,-0.455861) ,(-0.954976,0,-0.31029),(-0.991758,0,-0.157079),(-1.004121,0,0),(-1.125,0,0),(-1.004121,0,0),(-0.991758,0,0.157079),(-0.954976,0,0.31029),(-0.894678,0,0.455861),(-0.812351,0,0.590207),(-0.710021,0,0.710021),(-0.590207,0,0.812351),(-0.455861,0,0.894678),(-0.31029,0,0.954976),(-0.157079,0,0.991758),(0,0,1.004121),(0,0,1.125) ,(0,0,1.004121),(0.157079,0,0.991758),(0.31029,0,0.954976),(0.455861,0,0.894678),(0.590207,0,0.812351),(0.710021,0,0.710021),(0.812351,0,0.590207),(0.894678,0,0.455861),(0.954976,0,0.31029),(0.991758,0,0.157079),(1.004121,0,0)] ,
                'arrowBall'     : [(-0.101079,1.958922,0.406811),(-0.101079,1.849883,0.83017),(-0.101079,1.66815,1.093854),(-0.354509,1.66815,1.093854),(0,1.322858,1.357538),(0.354509,1.66815,1.093854),(0.101079,1.66815,1.093854),(0.101079,1.849883,0.83017),(0.101079,1.958922,0.406811),(0.527367,1.849883,0.406811),(0.791052,1.66815,0.406811),(0.791052,1.66815,0.657311),(1.054736,1.322858,0.302802),(0.791052,1.66815,-0.0517064),(0.791052,1.66815,0.198794),(0.527367,1.849883,0.198794),(0.101079,1.958922,0.198794),(0.101079,1.849883,-0.224565),(0.101079,1.66815,-0.488249),(0.354509,1.66815,-0.488249),(0,1.322858,-0.751934),(-0.354509,1.66815,-0.488249),(-0.101079,1.66815,-0.488249),(-0.101079,1.849883,-0.224565),(-0.101079,1.958922,0.198794),(-0.527367,1.849883,0.198794),(-0.791052,1.66815,0.198794),(-0.791052,1.66815,-0.0517064),(-1.054736,1.322858,0.302802),(-0.791052,1.66815,0.657311),(-0.791052,1.66815,0.406811),(-0.527367,1.849883,0.406811),(-0.101079,1.958922,0.406811)] ,
                'arrowCircle'   : [(-0.407582,0,2.049652),(-0.52278,0,2.613902),(-1.045561,0,2.613902),(1.37739e-008,0,3.659463),(1.045561,0,2.613902),(0.52278,0,2.613902),(0.407582,0,2.049652),(1.161124,0,1.737523),(1.737531,0,1.161114),(2.049654,0,0.407569),(2.613902,0,0.52278),(2.613902,0,1.045561),(3.659463,0,0),(2.613902,0,-1.045561),(2.613902,0,-0.52278),(2.049648,0,-0.407595),(1.737521,0,-1.16114),(1.161091,0,-1.73752),(0.407573,0,-2.049738) ,(0.52278,0,-2.613902),(1.045561,0,-2.613902),(1.37739e-008,0,-3.659463),(-1.045561,0,-2.613902),(-0.52278,0,-2.613902),(-0.407573,0,-2.049738),(-1.161091,0,-1.73752),(-1.737521,0,-1.16114),(-2.049648,0,-0.407595),(-2.613902,0,-0.52278),(-2.613902,0,-1.045561),(-3.659463,0,0),(-2.613902,0,1.045561),(-2.613902,0,0.52278),(-2.049654,0,0.407569),(-1.737531,0,1.161114),(-1.161124,0,1.737523),(-0.407582,0,2.04965)] ,
                'pyramid'       : [(-0.999999,0.0754167,-0.999999),(-0.999999,0.0754167,0.999999),(0.999999,0.0754167,0.999999),(0.999999,0.0754167,-0.999999),(-0.999999,0.0754167,-0.999999),(-0.999999,-0.0695844,-0.999999),(-0.112596,-1,-0.112596),(-0.112596,-1,0.112596),(0.112596,-1,0.112596),(0.999999,-0.0695844,0.999999),(0.999999,0.0754167,0.999999),(0.999999,-0.0695844,0.999999),(-0.999999,-0.0695844,0.999999),(-0.999999,0.0754167,0.999999),(-0.999999,-0.0695844,0.999999),(-0.112596,-1,0.112596),(-0.999999,-0.0695844,0.999999),(-0.999999,-0.0695844,-0.999999),(0.999999,-0.0695844,-0.999999),(0.999999,0.0754167,-0.999999),(0.999999,-0.0695844,-0.999999),(0.112596,-1,-0.112596),(-0.112596,-1,-0.112596),(0.112596,-1,-0.112596),(0.112596,-1,0.112596),(0.112596,-1,-0.112596),(0.999999,-0.0695844,-0.999999),(0.999999,-0.0695844,0.999999)] ,
                'capsule'       : [(-2.011489,0,0),(-1.977023,0.261792,0),(-1.875975,0.505744,0),(-1.71523,0.71523,0),(-1.505744,0.875975,0),(-1.261792,0.977023,0),(-1,1.011489,0),(1,1.011489,0),(1.261792,0.977023,0),(1.505744,0.875975,0),(1.71523,0.71523,0),(1.875975,0.505744,0),(1.977023,0.261792,0),(2.011489,0,0),(1.977023,-0.261792,0),(1.875975,-0.505744,0),(1.71523,-0.71523,0),(1.505744,-0.875975,0),(1.261792,-0.977023,0),(1,-1.011489,0),(-1,-1.011489,0),(-1.261792,-0.977023,0),(-1.505744,-0.875975,0), (-1.71523,-0.71523,0),(-1.875975,-0.505744,0),(-1.977023,-0.261792,0),(-2.011489,0,0)] ,
                'arrowCross'    : [(-3.629392,0,-2.087399),(-3.629392,0,-1.723768),(-1.723768,0,-1.723768),(-1.723768,0,-3.629392),(-2.087399,0,-3.629392),(0,0,-5.704041),(2.087399,0,-3.629392),(1.723768,0,-3.629392),(1.723768,0,-1.723768),(3.629392,0,-1.723768),(3.629392,0,-2.087399),(5.704041,0,0),(3.629392,0,2.087399),(3.629392,0,1.723768),(1.723768,0,1.723768),(1.723768,0,3.629392),(2.087399,0,3.629392),(0,0,5.704041),(-2.087399,0,3.629392),(-1.723768,0,3.629392),(-1.723768,0,1.723768),(-3.629392,0,1.723768),(-3.629392,0,2.087399),(-5.704041,0,0),(-3.629392,0,-2.087399)] ,
                'triangle'      : [(-1,0,1,),(-1,0,1,),(-0.9,0,0.8,),(-0.8,0,0.6,),(-0.7,0,0.4,),(-0.6,0,0.2,),(-0.5,0,0,),(-0.4,0,-0.2,),(-0.3,0,-0.4,),(-0.2,0,-0.6,),(-0.1,0,-0.8,),(0,0,-1,),(0.1,0,-0.8,),(0.2,0,-0.6,),(0.3,0,-0.4,),(0.4,0,-0.2,),(0.5,0,0,),(0.6,0,0.2,),(0.7,0,0.4,),(0.8,0,0.6,),(0.9,0,0.8,),(1,0,1,),(0.8,0,1,),(0.6,0,1,),(0.4,0,1,),(0.2,0,1,),(0,0,1,),(-0.2,0,1,),(-0.4,0,1,),(-0.6,0,1,),(-0.8,0,1,),(-1,0,1)] ,
                'drop'          : [(0,0,-2.28198),(0.585544,0,-1.569395),(1.08325,0,-1.071748),(1.413628,0,-0.588119) ,(1.531946,0,0.00057226),(1.413628,0,0.585419),(1.08325,0,1.083278),(0.585544,0,1.413621) ,(0,0,1.531949),(-0.585544,0,1.413621),(-1.08325,0,1.083277),(-1.413628,0,0.585419) ,(-1.531946,0,0.000554983),(-1.413628,0,-0.588078),(-1.08325,0,-1.071626),(-0.585544,0,-1.570833),(0,0,-2.28198)] ,
                'plus'          : [(-0.545455,0,2),(0.545455,0,2),(0.545455,0,0.545455),(2,0,0.545455),(2,0,-0.545455),(0.545455,0,-0.545455),(0.545455,0,-2),(-0.545455,0,-2),(-0.545455,0,-0.545455),(-2,0,-0.545455),(-2,0,0.545455),(-0.545455,0,0.545455),(-0.545455,0,2)] ,
                'daimond'       : [(0,1,0,),(0,0,0.625,),(0,-1,0,),(0.625,0,0,),(0,1,0,),(0,0,-0.625,),(0,-1,0,),(-0.625,0,0,),(0,1,0,),(0.625,0,0,),(0,0,-0.625,),(-0.625,0,0,),(0,0,0.625,),(0.625,0,0)] ,
                'square'        : [(0,0,-1.12558),(0,0,-1),(-1,0,-1),(-1,0,0),(-1.12558,0,0),(-1,0,0),(-1,0,1),(0,0,1),(0,0,1.12558),(0,0,1),(1,0,1),(1,0,0),(1.12558,0,0),(1,0,0),(1,0,-1),(0,0,-1)] ,
                'cube'          : [(1,-1,1),(1,1,1),(1,1,-1),(1,-1,-1),(-1,-1,-1),(-1,1,-1),(-1,1,1),(-1,-1,1),(1,-1,1),(1,-1,-1),(-1,-1,-1),(-1,-1,1),(-1,1,1),(1,1,1),(1,1,-1),(-1,1,-1)] ,
                'arrow'         : [(-1,0,0,),(0,0,-1,),(1,0,0,),(0.4,0,0,),(0.4,0,1,),(-0.4,0,1,),(-0.4,0,0,),(-1,0,0)],
                'locator'       : [(0,1,0),(0,-1,0),(0,0,0),(-1,0,0),(1,0,0),(0,0,0),(0,0,-1),(0,0,1)] ,
                'null'          : [(0,0,0),(0,0,0),(0,0,0)] ,
                'line'          : [(0.3,0,0),(-0.3,0,0)] }
    
    if shape in crvDict.keys() :
        return crvDict[shape]

def createNode( nodeType = '' , name = '' ) :
    if name == '' : name = '%s1' %nodeType
    return core.Dag(mc.createNode( '%s' %nodeType , n = name ))

def createCtrl( name = '' , shape = '' , col = '' , traget = '' , jnt = False ) :
    # If not argument traget. Control create to origin 0,0,0
    parameter = curveShape(shape)
    
    if jnt == True :
        ctrl = createNode( 'joint' , '%s' %name )
        curve = core.Dag(mc.curve( n = 'transformTemplate' , d = 1 , p = parameter ))
        curve.parentShape( ctrl )
        ctrl.attr('drawStyle').value = 2
        ctrl.lockHideAttrs( 'radius' )
    else :
        ctrl = core.Dag(mc.curve( n = name , d = 1 , p = parameter ))
        ctrl.correctShapeName()

    if traget :
        ctrl.snap( traget )
    
    ctrl.setColor(col)

    mc.select( cl = True )

    return ctrl

def addGimbal( obj = '' ) :
    ctrl = core.Dag(obj)
    ctrlShape = core.Dag(obj.shape)

    prefix = ctrl.getName().split( '_' )[0]
    suffix = ctrl.getName().replace( prefix , '' )

    gmb = core.Dag(mc.duplicate( obj )[0])
    gmb.rename( '%sGmbl%s' %( prefix , suffix ))
    gmbShape = core.Dag(gmb.shape)
    gmb.setColor( 'white' )
    gmb.scaleShape( 0.8 )

    ctrlShape.addAttr( 'gimbalControl' , 0 , 1 )
    
    ctrlShape.attr('gimbalControl') >> gmbShape.attr('v')
    ctrl.attr('rotateOrder') >> gmb.attr('rotateOrder')

    gmb.snap(obj)
    gmb.parent(obj)

    mc.select( cl = True )
    return gmb

def addConsCtrl( obj = '' ) :
    ctrl = core.Dag(obj)
    side = ctrl.getSide()
    prefix = ctrl.getName().split( '_' )[0]
    
    ctrlCons = createCtrl( '%sCons%s_Ctrl' %( prefix , side ) , 'locator' )
    ctrlConsShape = core.Dag(ctrlCons.shape)

    ctrlShape = core.Dag(ctrl.shape)
    ctrlShape.addAttr( 'constraintControl' , 0 , 1 )

    ctrlShape.attr('constraintControl') >> ctrlConsShape.attr('v')
    ctrl.attr('rotateOrder') >> ctrlCons.attr('rotateOrder')

    pars = ctrl.getParent()
    
    if not pars == None :
        ctrlCons.parent( pars )
        ctrl.parent( ctrlCons )
    else :
        ctrl.parent( ctrlCons )
    
    ctrlCons.setColor( 'white' )
    ctrlCons.scaleShape( 0.8 )

    mc.select( cl = True )
    return ctrlCons
    
def addGrp( obj = '' , type = 'Zro' ) :
    obj = core.Dag(obj)
    side = obj.getSide()
    prefix = obj.getName().split( '_' )[0]
    sufffix = obj.getName().split( '_' )[-1]
    sufffix = sufffix.capitalize()
    
    grp = createNode( 'transform' , '%s%s%s%sGrp' %( prefix , sufffix , type , side ))
    pars = obj.getParent()
    
    grp.snap( obj )
    obj.parent( grp )

    if not pars == None :
        grp.parent( pars )

    mc.select( cl = True )
    return grp

def rigCtrl( obj = '' , shape = 'cube' , col = 'red' ) :
    if obj == '' :
        obj = mc.ls( sl = True )[0]

    obj = core.Dag(obj)

    if '_' in obj.getName() :
        prefix = obj.getName().split( '_' )[0]
        prefix = '%s' %prefix
    else :
        prefix = '%s' %obj
    
    side = obj.getSide()

    ctrl = createCtrl( '%s%sCtrl' %( prefix , side ) , shape , col , obj )
    ctrlZro = addGrp( ctrl )
    ctrlGmbl = addGimbal( ctrl )
    ctrlCons = addConsCtrl( ctrl )
    
    mc.parentConstraint( ctrlGmbl , obj , mo = False )
    mc.scaleConstraint( ctrlGmbl , obj , mo = False )
    
    mc.select( cl = True )

def createJnt( name = '' , traget = '' ) :
    # If not argument traget. Joint create to origin 0,0,0
    jnt = createNode( 'joint' , name )
    
    if traget :
        jnt.snap( traget )
        jnt.freeze()
    
    mc.select( cl = True )
    return jnt

def addJntToCuv( name = '' , side = '' , cuv = '' , skip = 0 ) :
    # Skip = 0 * 1 2 3
    # Skip = 1 * 1 _ 2 _ 3
    # Skip = 3 * 1 _ _ _ 2 _ _ _ 3
    cuv = core.Dag(cuv)
    eps = cuv.attr( 'spans' ).value

    jntDict = []

    if skip == 0 :
        for ep in range(eps+1):
            epName = ep+1
            posi = mc.pointPosition( '%s.ep[%s]' %(cuv , ep))
            jnt = createJnt( '%s%s_%s_Jnt' %(name , epName , side))
            jnt.attr( 'tx' ).value = posi[0]
            jnt.attr( 'ty' ).value = posi[1]
            jnt.attr( 'tz' ).value = posi[2]
            jntDict.append(jnt)
    else :
        ep = 0
        numLoop = 1
        while ep <= eps :
            posi = mc.pointPosition( '%s.ep[%s]' %(cuv , ep))
            jnt = createJnt( '%s%s_%s_Jnt' %(name , numLoop , side))
            jnt.attr( 'tx' ).value = posi[0]
            jnt.attr( 'ty' ).value = posi[1]
            jnt.attr( 'tz' ).value = posi[2]
            jntDict.append(jnt)
            
            ep += (skip+1)
            numLoop += 1 

    return jntDict

def drawCuvOnJnt( name = '' , jnt = '' ) :
    # Draw Ep curve on joint
    size = len(jnt)
    position = []
    
    for i in range(0,size) :
        loc = createNode( 'transform' )
        mc.delete( mc.parentConstraint ( jnt[i] , loc , mo = False ))
        pos = mc.getAttr( '%s.t' %loc )[0]
        position.append(pos)
        mc.delete(loc)
        
    cuv = mc.curve( n = name , ep = position )
    return cuv

def drawLine( parent = '' , target = '' ) :
    # Curve name = argument Parent name 
    # So if you want name curve you can assign argument in parent
    parent = core.Dag(parent)
    side = parent.getSide()
    prefix = parent.getName().split( '_' )[0]
    suffix = parent.getName().replace( prefix , '' )
    
    crv = core.Dag(mc.curve( d = 1 , p = [( 0,0,0 ),( 0,0,0 )]))
    crv.rename( '%sLine%sCrv' %( prefix , side ))
    crvZro = addGrp( crv )

    clstr1 = mc.rename( mc.cluster( '%s.cv[0]' %crv , wn = ( parent , parent ))[0] , '%s1%sClstr'  %( prefix , side ))
    clstr2 = mc.rename( mc.cluster( '%s.cv[1]' %crv , wn = ( target , target ))[0] , '%s2%sClstr'  %( prefix , side ))

    crv.attr( 'overrideEnabled' ).value = 1
    crv.attr( 'overrideDisplayType' ).value = 2
    crv.attr( 'inheritsTransform' ).value = 0
    
    crv.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    
    mc.select( cl = True )
    return crvZro

def addIkh( name = '' , type = '' , stJnt = '' , enJnt = '' ) :
    # Type = ikSplineSolver , ikSCsolver , ikRPsolver
    stJntDag = core.Dag(stJnt)
    side = stJntDag.getSide()
    
    if not type == 'ikSplineSolver' :
        ikh = mc.ikHandle( n = '%s%sIkh' %( name , side ) , sol = type , sj = stJnt , ee = enJnt )
        ikhZro = addGrp(ikh[0])
        mc.rename( ikh[-1] , '%s%sEff' %( name , side ))
        return { 'ikh' : core.Dag(ikh[0]) , 'ikhZro' : core.Dag(ikhZro) }
    else :
        ikh = mc.ikHandle( n = '%s%sIkh' %( name , side ) , sol = type , sj = stJnt , ee = enJnt , ns = 1 )
        ikhZro = addGrp(ikh[0])
        mc.rename( ikh[1] , '%s%sEff' %( name , side ))
        ikhCuv = mc.rename( ikh[2] , '%s%sCuv' %( name , side ))
        return { 'ikh' : core.Dag(ikh[0]) , 'ikhZro' : core.Dag(ikhZro) , 'ikhCuv' : core.Dag(ikhCuv) }
    
    mc.select( cl = True )

def addNonRollJnt( name = '' , rootJnt = '' , endJnt = '' , parent = '' , axis = 'y+' ) :
    
    if axis == 'x+' :
        aimVec = [ 1 , 0 , 0 ]
        upVec = [ 0 , 0 , 1 ]
        rotateOrder = 1
            
    elif axis == 'x-' :
        aimVec = [ -1 , 0 , 0 ]
        upVec = [ 0 , 0 , 1 ]
        rotateOrder = 1
    
    elif axis == 'y+' :
        aimVec =  [ 0 , 1 , 0 ]
        upVec = [ 0 , 0 , 1 ]
        rotateOrder = 2
        
    elif axis == 'y-' :
        aimVec = [ 0 , -1 , 0 ]
        upVec = [ 0 , 0 , 1 ]
        rotateOrder = 2
    
    elif axis == 'z+' :
        aimVec = [ 0 , 0 , 1 ]
        upVec = [ 0 , 1 , 0 ]
        rotateOrder = 0
    
    elif axis == 'z-' :
        aimVec = [ 0 , 0 , -1 ]
        upVec = [ 0 , 1 , 0 ]
        rotateOrder = 0

    rootDag = core.Dag(rootJnt)
    side = rootDag.getSide()

    jntGrp = createNode( 'transform' , '%sNonRollJnt%sGrp' %(name,side))
    jntGrp.snap( rootJnt )

    rootNr = createJnt( '%sNonRoll%sJnt' %(name,side) , rootJnt )
    rootNrZro = addGrp(rootNr)

    mc.delete( 
                   mc.aimConstraint( endJnt , rootNrZro , 
                                     aim = aimVec , 
                                     u = upVec , 
                                     wut='objectrotation' , 
                                     wuo = rootJnt , 
                                     wu= upVec )
              )
    
    endNr = createJnt( '%sNonRollEnd%sJnt' %(name,side) , endJnt )
    endNr.snapOrient( rootNr )
    mc.parent( endNr , rootNr )
    
    ikhDict = addIkh( '%sNonRoll' %name , 'ikRPsolver' , rootNr , endNr )
    ikhNr = ikhDict['ikh']
    ikhNrZro = ikhDict['ikhZro']
    
    ikhNr.attr('poleVectorX').value = 0
    ikhNr.attr('poleVectorY').value = 0
    ikhNr.attr('poleVectorZ').value = 0

    twistGrp = createNode( 'transform' , '%sNonRollTwist%sGrp' %(name,side))
    twistGrp.snap( rootNr )
    twistGrp.parent( rootNr )

    twistGrp.setRotateOrder( rotateOrder )

    mc.parentConstraint( parent , rootNrZro , mo = True )
    mc.pointConstraint( rootJnt , rootNr , mo = True )
    mc.parentConstraint( parent , ikhNrZro , mo = True )
    mc.parentConstraint( endJnt , ikhNr , mo = True )
    mc.orientConstraint( rootJnt , twistGrp , mo = False )
    
    mc.parent( rootNrZro , ikhNrZro , jntGrp )
    mc.select( cl = True )

    return { 'jntGrp' : core.Dag(jntGrp) , 'rootNr' : core.Dag(rootNr) , 'rootNrZro' : core.Dag(rootNrZro) , 'endNr' : core.Dag(endNr) , 'ikhNr' : core.Dag(ikhNr) , 'ikhNrZro' : core.Dag(ikhNrZro) , 'twistGrp' : core.Dag(twistGrp) }
    
def localWorld( obj = '' , worldObj = '' , localObj = '' , consGrp = '' , type = '' ) :
    ctrl = core.Dag(obj)
    side = ctrl.getSide()
    prefix = ctrl.getName().split( '_' )[0]
    suffix = ctrl.getName().replace( prefix , '' )
    
    ctrl.addAttr( 'localWorld' , 0 , 1 )
    
    rev = createNode( 'reverse' , '%sLocWor%sRev' %( prefix , side ))
    world = createNode( 'transform' , '%sWor%sGrp' %( prefix , side ))
    local = createNode( 'transform' , '%sLoc%sGrp' %( prefix , side ))
    
    mc.parent( world , local , localObj )
    
    world.snap( consGrp )
    local.snap( consGrp )
    
    if type == 'orient' :
        conWor = mc.orientConstraint( worldObj , world , mo = 1 )[0]
        conNd = core.Dag(mc.orientConstraint( local , world , consGrp , mo = 1 )[0])
    elif type == 'parent' :
        conWor = mc.parentConstraint( worldObj , world , mo = 1 )[0]
        conNd = core.Dag(mc.parentConstraint( local , world , consGrp , mo = 1 )[0])
    
    ctrl.attr('localWorld') >> rev.attr('ix')
    ctrl.attr('localWorld') >> conNd.attr('%sW1' %world)
    rev.attr('ox') >> conNd.attr('%sW0' %local)
    
    world.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    local.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

    mc.select( cl = True )

def blendFkIk( obj = '' , jntFk = '' , jntIk = '' , jntMain = '' ) :
    ctrl = core.Dag(obj)
    jntFk = core.Dag(jntFk)
    jntIk = core.Dag(jntIk)
    jntMain = core.Dag(jntMain)
    side = ctrl.getSide()

    prefix = ctrl.getName().split('_')[0]
    
    if not mc.objExists( '%s.fkIk' %ctrl ) :
        ctrl.addAttr( 'fkIk' , 0 , 1 )

    pars = core.Dag(mc.parentConstraint( jntFk , jntIk , jntMain , mo = 0 )[0])
    
    if not mc.objExists( '%sFkIk%sRev' %( prefix , side )) :
        rev = createNode( 'reverse' , '%sFkIk%sRev' %( prefix , side ))
        ctrl.attr('fkIk') >> rev.attr('ix')
    else : 
        rev = core.Dag('%sFkIk%sRev' %( prefix , side ))
        
    ctrl.attr('fkIk') >> pars.attr('%sW1' %jntIk)
    rev.attr('ox') >> pars.attr('%sW0' %jntFk)

def addSquash( obj = '' , jnt = '' , ax = ( 'sx' , 'sz'  )) :
    ctrl = core.Dag(obj)
    jnt = core.Dag(jnt)
    side = ctrl.getSide()
    prefix = ctrl.getName().split( '_' )[0]
    
    mdv = createNode( 'multiplyDivide' , '%sSqsh%sMdv' %( prefix , side ))
    pma = createNode( 'plusMinusAverage' , '%sSqsh%sPma' %( prefix , side ))
    
    ctrl.addAttr( 'squash' )
    pma.addAttr( 'default' )

    mdv.attr('i2x').value = 0.5
    pma.attr('default').value = 1

    pma.attr('default') >> pma.attr('i1[0]')
    ctrl.attr('squash') >> mdv.attr('i1x')
    mdv.attr('ox') >> pma.attr('i1[1]')
    pma.attr('o1') >> jnt.attr('%s' %ax[0])
    pma.attr('o1') >> jnt.attr('%s' %ax[1])

def addFkStretch( obj = '' , traget = '' , ax = 'y' , val = 1 ) :
    ctrl = core.Dag(obj)
    traget = core.Dag(traget)
    side = ctrl.getSide()
    prefix = ctrl.getName().split( '_' )[0]
    
    mdv = createNode( 'multiplyDivide' , '%sStrt%sMdv' %( prefix , side ))
    pma = createNode( 'plusMinusAverage' , '%sStrt%sPma' %( prefix , side ))
    
    ctrl.addAttr( 'stretch' )
    pma.addAttr( 'default' )

    valAx = traget.attr( 't%s' %ax).value
    pma.attr('default').value = valAx
    mdv.attr('i2x').value = val

    pma.attr('default') >> pma.attr('i1[0]')
    ctrl.attr('stretch') >> mdv.attr('i1x')
    mdv.attr('ox') >> pma.attr('i1[1]')
    pma.attr('o1') >> traget.attr('t%s' %ax)

def addIkStretch( obj = '' , lockCtrl = '' , axis = '' , type = '' , *args ) :
    ctrl = core.Dag(obj)
    side = ctrl.getSide()

    if 'L' in side  :
        value = 1
    elif 'R' in side :
        value = -1

    prefixDict = []
    
    mdvDict = [ 'x' , 'y' , 'z' ]
    pmaDict = [ 'x' , 'y' , 'z' ]
    bclDict = [ 'r' , 'g' , 'b' ]
    
    gmbl = core.Dag(mc.ls( mc.listRelatives( ctrl , c = True ) , tr = True )[0])

    ctrl.addTitleAttr( 'Stretch' )
    ctrl.addAttr( 'autoStretch' , 0 , 1 )

    for arg in args :
        if not arg == args[-1] :
            arg = core.Dag(arg)
            prefixDict.append(arg.getName().split( 'Ik' )[0])

    for prefix in prefixDict :
            ctrl.addAttr( '%sStretch' %prefix )
    
    ikDistGrp = createNode( 'transform' , '%sIkDist%sGrp' %( type , side ))
    distSt = createNode( 'transform' , '%sIkDistSt%sGrp' %( type , side ))
    distEn = createNode( 'transform' , '%sIkDistEn%sGrp' %( type , side ))
    mc.parent( distSt , distEn , ikDistGrp )

    dist = createNode( 'distanceBetween' , '%sIkAutoStrt%sDist' %( type , side ))
    pma = createNode( 'plusMinusAverage' , '%sIkAutoStrt%sPma' %( type , side ))
    cond = createNode( 'condition' , '%sIkAutoStrt%sCnd' %( type , side ))
    bcl = createNode( 'blendColors' , '%sIkAutoStrt%sBcl' %( type , side ))
    mdvAuto = createNode( 'multiplyDivide' , '%sIkAutoStrt%sMdv' %( type , side ))
    mdvAmp = createNode( 'multiplyDivide' , '%sIkAmpStrt%sMdv' %( type , side ))
    mdvStrt = createNode( 'multiplyDivide' , '%sIkStrt%sMdv' %( type , side ))

    mc.pointConstraint( args[0] , distSt , mo = 0 )
    mc.pointConstraint( gmbl , distEn , mo = 0 )

    distSt.attr('t') >> dist.attr('point1')
    distEn.attr('t') >> dist.attr('point2')

    dist.attr('d') >> mdvAuto.attr('i1x')
    dist.attr('d') >> mdvAuto.attr('i1y')
    dist.attr('d') >> mdvAuto.attr('i1z')

    dist.attr('d') >> cond.attr('ft')
    mdvAuto.attr('o') >> cond.attr('ct')
    cond.attr('oc') >> mdvStrt.attr('i1')
    mdvStrt.attr('o') >> bcl.attr('c1')
    bcl.attr('op') >> pma.attr('i3[0]')
    mdvAmp.attr('o') >> pma.attr('i3[1]')
    ctrl.attr('autoStretch') >> bcl.attr('b')

    cond.attr('op').value = 2
    mdvAuto.attr('op').value = 2

    distValue = dist.attr('distance').value
    cond.attr('st').value = distValue
    mdvAuto.attr('i2x').value = distValue
    mdvAuto.attr('i2y').value = distValue
    mdvAuto.attr('i2z').value = distValue

    for arg in args :
        if not arg == args[-1] :
            i = args.index( arg )
            
            ctrl.attr('%sStretch' %prefixDict[i]) >> mdvAmp.attr('i1%s' %mdvDict[i])
            
            axVal = mc.getAttr( '%s.%s' %( args[i+1] , axis ))
            
            mdvAmp.attr('i2%s' %mdvDict[i]).value = value
            mdvStrt.attr('i2%s' %mdvDict[i]).value = axVal
            bcl.attr('c2%s' %bclDict[i]).value = axVal
    
    if lockCtrl :
        lockCtrl = core.Dag(lockCtrl)
        lockCtrl.addAttr( 'lock' , 0 , 1 )
        
        distLock = createNode( 'transform' , '%sIkDistLock%sGrp' %( type , side ))
        mc.parent( distLock , ikDistGrp )
        
        dist1Lock = createNode( 'distanceBetween' , '%sIkLock1%sDist' %( type , side ))
        dist2Lock = createNode( 'distanceBetween' , '%sIkLock2%sDist' %( type , side ))
        mdvLock = createNode( 'multiplyDivide' , '%sIkLock%sMdv' %( type , side ))
        bclLock = createNode( 'blendColors' , '%sIkLock%sBcl' %( type , side ))

        mc.pointConstraint( lockCtrl , distLock , mo = 0 )

        distSt.attr('t') >> dist1Lock.attr('point1')
        distEn.attr('t') >> dist2Lock.attr('point1')
        distLock.attr('t') >> dist1Lock.attr('point2')
        distLock.attr('t') >> dist2Lock.attr('point2')
        dist1Lock.attr('d') >> mdvLock.attr('i1x')
        dist2Lock.attr('d') >> mdvLock.attr('i1y')
        lockCtrl.attr('lock') >> bclLock.attr('b')
        mdvLock.attr('ox') >> bclLock.attr('c1r')
        mdvLock.attr('oy') >> bclLock.attr('c1g')
        pma.attr('o3x') >> bclLock.attr('c2r')
        pma.attr('o3y') >> bclLock.attr('c2g')

        mc.connectAttr( '%s.opr' %bclLock , '%s.%s' %( args[1] , axis ))
        mc.connectAttr( '%s.opg' %bclLock , '%s.%s' %( args[2] , axis ))

        mdvLock.attr('i2x').value = value
        mdvLock.attr('i2y').value = value
    
        distLock.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    else :
        for i in range(0,len(args)-1) :
            mc.connectAttr( '%s.o3%s' %( pma , pmaDict[i] ) , '%s.%s' %( args[i+1] , axis ))

    distSt.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )
    distEn.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

    return ikDistGrp
    
def orientJnt( src = '' , trgt = '' , objUpvec = '' , aimVec = (0,1,0) , upVec = (0,0,1)) :
    # Auto orient joint aim to traget
    if src == '' :
        src = mc.ls( sl = True )[0]
    if trgt == '' :
        trgt = mc.ls( sl = True )[1]

    if not objUpvec : 
        objUpvec = mc.duplicate( src , rr = True )[0]
        tmpUpChldrn = mc.listRelatives( objUpvec , f = True )
    
        if tmpUpChldrn :
            mc.delete( tmpUpChldrn )
    
    aim = aimVec
    u = upVec
    wuo = objUpvec
    wut = 'objectrotation'
    
    mc.delete( mc.aimConstraint( trgt , src , aim = aim , wuo = objUpvec , wut = wut , u = u ))
    mc.makeIdentity( trgt , a = True )
    
    if objUpvec :
        mc.delete( objUpvec )

def getDist( st = '' , en = '' ) :
    # Check distance 2 object
    if st == '' or st == '' :
        st = mc.ls( sl = True )[0]
        en = mc.ls( sl = True )[1]

    dist1Grp = createNode( 'transform' , 'null1' )
    dist2Grp = createNode( 'transform' , 'null2' )
    distNode = createNode( 'distanceBetween' , 'dist' )

    mc.delete( mc.parentConstraint ( st , dist1Grp , mo = False ))
    mc.delete( mc.parentConstraint ( en , dist2Grp , mo = False ))

    mc.connectAttr( '%s.t' %dist1Grp , '%s.point1' %distNode )
    mc.connectAttr( '%s.t' %dist2Grp , '%s.point2' %distNode )
    
    distance = mc.getAttr( '%s.distance' %distNode )
    mc.delete( dist1Grp , dist2Grp , distNode )
    return distance

def getInfsPerVtcs( obj = '' , influences = 4 ) :
    # Check Influence per vertice
    if obj == '' :
        obj = mc.ls( sl = True )[0]
    
    obj = core.Dag(obj)

    skc = obj.getDef( 'skinCluster' )[0]
    infs = mc.skinCluster( skc , q = True , inf = True )
    vtcs = []
    
    for i in range( mc.polyEvaluate ( obj , v = True )) :
        vtx = '%s.vtx[%s]' % ( obj , i )
        infVals = mc.skinPercent( skc , vtx , q = True , v = True )
        array = []
        
        for j in range( len ( infVals )) :
            if infVals[j-1] > 0 :
                array.append( infVals[j-1] )
        
        if len( array ) > influences :
            vtcs.append( vtx )
    
    if vtcs :
        mc.select( vtcs , r = True )