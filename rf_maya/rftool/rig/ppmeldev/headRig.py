import maya.cmds as mc
import maya.mel as mm
import core as core
import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  HEAD RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , headTmpJnt       = 'Head_TmpJnt' ,
                         headEndTmpJnt    = 'HeadEnd_TmpJnt' ,
                         eyeTrgTmpJnt     = 'EyeTrgt_TmpJnt' ,
                         eyeLftTrgTmpJnt  = 'EyeTrgt_L_TmpJnt' ,
                         eyeRgtTrgTmpJnt  = 'EyeTrgt_R_TmpJnt' ,
                         eyeLftTmpJnt     = 'Eye_L_TmpJnt' ,
                         eyeRgtTmpJnt     = 'Eye_R_TmpJnt' ,
                         jawUpr1TmpJnt    = 'JawUpr1_TmpJnt' ,
                         jawUprEndTmpJnt  = 'JawUprEnd_TmpJnt' ,
                         jawLwr1TmpJnt    = 'JawLwr1_TmpJnt' ,
                         jawLwr2TmpJnt    = 'JawLwr2_TmpJnt' ,
                         jawLwrEndTmpJnt  = 'JawLwrEnd_TmpJnt' ,
                         parent           = 'NeckEnd_Jnt' ,
                         ctrlGrp          = 'Ctrl_Grp' ,
                         skinGrp          = 'Skin_Grp' ,
                         elem             = '' ,
                         side             = '' ,
                         eyeRig           = True ,
                         jawRig           = True ,
                         size             = 1 
                 ):

        ##-- Info
        elem = elem.capitalize()

        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        #-- Create Main Group Head
        self.headCtrlGrp = rt.createNode( 'transform' , 'Head%sCtrl%sGrp' %( elem , side ))
        mc.parentConstraint( parent , self.headCtrlGrp , mo = False )
        
        #-- Create Joint Head 
        if not mc.objExists( 'Head%s%sJnt' %( elem , side )) :
            self.headJnt = rt.createJnt( 'Head%s%sJnt' %( elem , side ) , headTmpJnt )
            self.headJnt.parent( parent )
        else :
            self.headJnt = core.Dag('Head%s%sJnt' %( elem , side ))

        self.headEndJnt = rt.createJnt( 'Head%sEnd%sJnt' %( elem , side ) , headEndTmpJnt )
        self.headEndJnt.parent( self.headJnt )

        #-- Create Controls Head
        self.headCtrl = rt.createCtrl( 'Head%s%sCtrl' %( elem , side ) , 'cube' , 'blue' , jnt = True )
        self.headGmbl = rt.addGimbal( self.headCtrl )
        self.headZro = rt.addGrp( self.headCtrl )
        self.headZro.snapPoint( self.headJnt )
        self.headCtrl.snapJntOrient( self.headJnt )

        #-- Adjust Shape Controls Head
        for ctrl in ( self.headCtrl , self.headGmbl ) :
            ctrl.scaleShape( size )
        
        #-- Adjust Rotate Order Head
        for obj in ( self.headCtrl , self.headJnt , self.headJnt ) :
            obj.setRotateOrder( 'xzy' )

        #-- Rig process Head
        mc.parentConstraint( self.headGmbl , self.headJnt , mo = False )
        mc.scaleConstraint( self.headGmbl , self.headJnt , mo = False )
        rt.localWorld( self.headCtrl , ctrlGrp , self.headCtrlGrp , self.headZro , 'orient' )

        self.headGmbl.attr('ssc').value = 0
        self.headEndJnt.attr('ssc').value = 0
        
        #-- Adjust Hierarchy Head
        mc.parent( self.headZro , self.headCtrlGrp )
        mc.parent( self.headCtrlGrp , ctrlGrp )

        #-- Cleanup Head
        for obj in ( self.headCtrlGrp , self.headZro ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for obj in ( self.headCtrl , self.headGmbl ) :
            obj.lockHideAttrs( 'v' )

        self.headCtrl.attr('localWorld').value = 1

        #-- EYE
        if eyeRig == True :
            #-- Create Main Group Eye
            self.eyeCtrlGrp = rt.createNode( 'transform' , 'Eye%sCtrl%sGrp' %( elem , side ))
            self.eyeCtrlGrp.snap( self.headJnt )
        
            #-- Create Joint Eye 
            self.eyeLftJnt = rt.createJnt( 'Eye%sLFT%sJnt' %( elem , side ) , eyeLftTmpJnt )
            self.eyeRgtJnt = rt.createJnt( 'Eye%sRGT%sJnt' %( elem , side ) , eyeRgtTmpJnt )
            self.eyeLidLftJnt = rt.createJnt( 'EyeLid%sLFT%sJnt' %( elem , side ) , eyeLftTmpJnt )
            self.eyeLidRgtJnt = rt.createJnt( 'EyeLid%sRGT%sJnt' %( elem , side ) , eyeRgtTmpJnt )

            mc.parent( self.eyeLidLftJnt , self.eyeLftJnt )
            mc.parent( self.eyeLftJnt , self.headJnt )
            mc.parent( self.eyeLidRgtJnt , self.eyeRgtJnt )
            mc.parent( self.eyeRgtJnt , self.headJnt )

            #-- Create Controls Eye
            self.eyeLftCtrl = rt.createCtrl( 'Eye%sLFT%sCtrl' %( elem , side ) , 'sphere' , 'red' )
            self.eyeLftGmbl = rt.addGimbal( self.eyeLftCtrl )
            self.eyeLftZro = rt.addGrp( self.eyeLftCtrl )
            self.eyeLftAim = rt.addGrp( self.eyeLftCtrl , 'Aim' )
            self.eyeLftZro.snap( self.eyeLftJnt )

            self.eyeRgtCtrl = rt.createCtrl( 'Eye%sRGT%sCtrl' %( elem , side ) , 'sphere' , 'red' )
            self.eyeRgtGmbl = rt.addGimbal( self.eyeRgtCtrl )
            self.eyeRgtZro = rt.addGrp( self.eyeRgtCtrl )
            self.eyeRgtAim = rt.addGrp( self.eyeRgtCtrl , 'Aim' )
            self.eyeRgtZro.snap( self.eyeRgtJnt )

            self.eyeTrgCtrl = rt.createCtrl( 'EyeTrgt%s%sCtrl' %( elem , side ) , 'capsule' , 'yellow' )
            self.eyeTrgZro = rt.addGrp( self.eyeTrgCtrl )
            self.eyeTrgZro.snap( eyeTrgTmpJnt )

            self.eyeLftTrgCtrl = rt.createCtrl( 'EyeTrgt%sLFT%sCtrl' %( elem , side ) , 'circle' , 'red' )
            self.eyeLftTrgZro = rt.addGrp( self.eyeLftTrgCtrl )
            self.eyeLftTrgZro.snap( eyeLftTrgTmpJnt )

            self.eyeRgtTrgCtrl = rt.createCtrl( 'EyeTrgt%sRGT%sCtrl' %( elem , side ) , 'circle' , 'blue' )
            self.eyeRgtTrgZro = rt.addGrp( self.eyeRgtTrgCtrl )
            self.eyeRgtTrgZro.snap( eyeRgtTrgTmpJnt )

            #-- Adjust Shape Controls Eye
            for ctrl in ( self.eyeLftCtrl , self.eyeLftGmbl , self.eyeRgtCtrl , self.eyeRgtGmbl ) :
                ctrl.scaleShape( size * 0.5 )

            for ctrl in ( self.eyeLftTrgCtrl , self.eyeRgtTrgCtrl ) :
                ctrl.scaleShape( size * 0.45 )
                ctrl.rotateShape( 90 , 0 , 0 )

            self.eyeTrgCtrl.scaleShape( size * 0.55 )

            #-- Rig process Eye
            mc.parentConstraint( self.eyeLftGmbl , self.eyeLftJnt , mo = False )
            mc.scaleConstraint( self.eyeLftGmbl , self.eyeLftJnt , mo = False )
            mc.parentConstraint( self.eyeRgtGmbl , self.eyeRgtJnt , mo = False )
            mc.scaleConstraint( self.eyeRgtGmbl , self.eyeRgtJnt , mo = False )
            rt.localWorld( self.eyeTrgCtrl , ctrlGrp , self.headGmbl , self.eyeTrgZro , 'orient' )

            self.eyeLftJnt.attr('ssc').value = 0
            self.eyeRgtJnt.attr('ssc').value = 0
            self.eyeLidLftJnt.attr('ssc').value = 0
            self.eyeLidRgtJnt.attr('ssc').value = 0
            
            mc.aimConstraint( self.eyeLftTrgCtrl , self.eyeLftAim , aim = (0,0,1) , u = (0,1,0) , wut = "objectrotation" , wu = (0,1,0) , wuo = self.eyeLftZro )[0]
            mc.aimConstraint( self.eyeRgtTrgCtrl , self.eyeRgtAim , aim = (0,0,1) , u = (0,1,0) , wut = "objectrotation" , wu = (0,1,0) , wuo = self.eyeRgtZro )[0]
            
            self.lidLftCons = core.Dag(mc.orientConstraint( self.headJnt , self.eyeLftJnt , self.eyeLidLftJnt , mo = True )[0])
            self.lidRgtCons = core.Dag(mc.orientConstraint( self.headJnt , self.eyeRgtJnt , self.eyeLidRgtJnt , mo = True )[0])

            self.lidLftRev = rt.createNode( 'reverse' , 'EyeLid%sLFT%sRev' %( elem , side ))
            self.lidRgtRev = rt.createNode( 'reverse' , 'EyeLid%sRGT%sRev' %( elem , side ))

            for ctrl in ( self.eyeLftCtrl , self.eyeRgtCtrl ) :
                ctrl.addAttr( 'lidFollow' , 0 , 1 )

            self.eyeLftCtrl.attr('lidFollow') >> self.lidLftCons.attr('Eye%sLFT%sJntW1' %( elem , side ))
            self.eyeLftCtrl.attr('lidFollow') >> self.lidLftRev.attr('ix')
            self.lidLftRev.attr('ox') >> self.lidLftCons.attr('Head%s%sJntW0' %( elem , side ))

            self.eyeRgtCtrl.attr('lidFollow') >> self.lidRgtCons.attr('Eye%sRGT%sJntW1' %( elem , side ))
            self.eyeRgtCtrl.attr('lidFollow') >> self.lidRgtRev.attr('ix')
            self.lidRgtRev.attr('ox') >> self.lidRgtCons.attr('Head%s%sJntW0' %( elem , side ))
        
            #-- Adjust Hierarchy Eye
            mc.parent( self.eyeLftZro , self.eyeRgtZro , self.eyeTrgZro , self.eyeCtrlGrp )
            mc.parent( self.eyeLftTrgZro , self.eyeRgtTrgZro , self.eyeTrgCtrl )
            mc.parent( self.eyeCtrlGrp , self.headGmbl )

            #-- Cleanup Eye
            for grp in ( self.eyeCtrlGrp , self.eyeLftZro , self.eyeRgtZro , self.eyeLftTrgZro , self.eyeRgtTrgZro , self.eyeTrgZro ) :
                grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

            for ctrl in ( self.eyeLftGmbl , self.eyeRgtGmbl , self.eyeLftTrgCtrl , self.eyeRgtTrgCtrl , self.eyeTrgCtrl ) :
                ctrl.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

            for ctrl in ( self.eyeLftCtrl , self.eyeRgtCtrl ) :
                ctrl.lockHideAttrs( 'v' )

        #-- JAW
        if jawRig == True :
            #-- Create Main Group Jaw
            self.jawCtrlGrp = rt.createNode( 'transform' , 'Jaw%sCtrl%sGrp' %( elem , side ))
            self.jawCtrlGrp.snap( self.headJnt )
        
            #-- Create Joint Jaw 
            self.jawLwr1Jnt = rt.createJnt( 'Jaw%sLwr1%sJnt' %( elem , side ) , jawLwr1TmpJnt )
            self.jawLwr2Jnt = rt.createJnt( 'Jaw%sLwr2%sJnt' %( elem , side ) , jawLwr2TmpJnt )
            self.jawLwrEndJnt = rt.createJnt( 'Jaw%sLwrEnd%sJnt' %( elem , side ) , jawLwrEndTmpJnt )
            self.jawUpr1Jnt = rt.createJnt( 'Jaw%sUpr1%sJnt' %( elem , side ) , jawUpr1TmpJnt )
            self.jawUprEndJnt = rt.createJnt( 'Jaw%sUprEnd%sJnt' %( elem , side ) , jawUprEndTmpJnt )
    
            mc.parent( self.jawLwrEndJnt , self.jawLwr2Jnt )
            mc.parent( self.jawLwr2Jnt , self.jawLwr1Jnt )
            mc.parent( self.jawLwr1Jnt , self.headJnt )
            mc.parent( self.jawUprEndJnt , self.jawUpr1Jnt )
            mc.parent( self.jawUpr1Jnt , self.headJnt )

            #-- Create Controls Jaw
            self.jawLwr1Ctrl = rt.createCtrl( 'Jaw%sLwr1%sCtrl' %( elem , side ) , 'square' , 'red' )
            self.jawLwr1Gmbl = rt.addGimbal( self.jawLwr1Ctrl )
            self.jawLwr1Zro = rt.addGrp( self.jawLwr1Ctrl )
            self.jawLwr1Zro.snap( self.jawLwr1Jnt )

            self.jawLwr2Ctrl = rt.createCtrl( 'Jaw%sLwr2%sCtrl' %( elem , side ) , 'square' , 'yellow' )
            self.jawLwr2Gmbl = rt.addGimbal( self.jawLwr2Ctrl )
            self.jawLwr2Zro = rt.addGrp( self.jawLwr2Ctrl )
            self.jawLwr2Zro.snap( self.jawLwr2Jnt )

            self.jaw1UprCtrl = rt.createCtrl( 'Jaw%sUpr1%sCtrl' %( elem , side ) , 'square' , 'yellow' )
            self.jaw1UprGmbl = rt.addGimbal( self.jaw1UprCtrl )
            self.jaw1UprZro = rt.addGrp( self.jaw1UprCtrl )
            self.jaw1UprZro.snap( self.jawUpr1Jnt )

            #-- Adjust Shape Controls Jaw
            for ctrl in ( self.jaw1UprCtrl , self.jaw1UprGmbl , self.jawLwr2Ctrl , self.jawLwr2Gmbl ) :
                ctrl.scaleShape( size * 0.3 )
                ctrl.rotateShape( 90 , 0 , 0 )

            for ctrl in ( self.jawLwr1Ctrl , self.jawLwr1Gmbl ) :
                ctrl.scaleShape( size * 0.9 )
        
            #-- Adjust Rotate Order Jaw
            for obj in ( self.jawLwr1Ctrl , self.jawLwr2Ctrl , self.jaw1UprCtrl , self.jawLwr1Jnt , self.jawLwr2Jnt , self.jawUpr1Jnt ) :
                obj.setRotateOrder( 'zyx' )

            #-- Rig process Jaw
            mc.parentConstraint( self.jawLwr1Gmbl , self.jawLwr1Jnt , mo = False )
            mc.scaleConstraint( self.jawLwr1Gmbl , self.jawLwr1Jnt , mo = False )
            mc.parentConstraint( self.jawLwr2Gmbl , self.jawLwr2Jnt , mo = False )
            mc.scaleConstraint( self.jawLwr2Gmbl , self.jawLwr2Jnt , mo = False )
            mc.parentConstraint( self.jaw1UprGmbl , self.jawUpr1Jnt , mo = False )
            mc.scaleConstraint( self.jaw1UprGmbl , self.jawUpr1Jnt , mo = False )

            self.jawLwr1Jnt.attr('ssc').value = 0
            self.jawLwr2Jnt.attr('ssc').value = 0
            self.jawUpr1Jnt.attr('ssc').value = 0
        
            #-- Adjust Hierarchy Jaw
            mc.parent( self.jaw1UprZro , self.jawLwr1Zro , self.jawCtrlGrp )
            mc.parent( self.jawLwr2Zro , self.jawLwr1Gmbl )
            mc.parent( self.jawCtrlGrp , self.headGmbl )

            #-- Cleanup Jaw
            for grp in ( self.jawCtrlGrp , self.jaw1UprZro , self.jawLwr1Zro , self.jawLwr2Zro ) :
                grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

            for ctrl in ( self.jawLwr1Ctrl , self.jawLwr2Ctrl , self.jaw1UprCtrl , self.jawLwr1Gmbl , self.jawLwr2Gmbl , self.jaw1UprGmbl ) :
                ctrl.lockHideAttrs( 'v' )
    
    mc.select( cl = True )