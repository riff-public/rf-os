import maya.cmds as mc
import maya.mel as mm
import core as core
import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  MAIN RIGGING MODULE
#
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , assetName = '' , size = 1 ):
        
        if assetName :
            self.assetName = '%s' %assetName.capitalize()
        else:
            self.assetName = 'Rig'

        #-- Create Main Group
        self.assetGrp = rt.createNode( 'transform' , '%s_Grp' %self.assetName )
        self.geoGrp = rt.createNode( 'transform' , 'Geo_Grp' )
        self.jntGrp = rt.createNode( 'transform' , 'Jnt_Grp' )
        self.ikhGrp = rt.createNode( 'transform' , 'Ikh_Grp' )
        self.ctrlGrp = rt.createNode( 'transform' , 'Ctrl_Grp' )
        self.skinGrp = rt.createNode( 'transform' , 'Skin_Grp' )
        self.stillGrp = rt.createNode( 'transform' , 'Still_Grp' )
        
        #-- Create Main Controls
        self.allMoverCtrl = rt.createCtrl( 'AllMover_Ctrl' , 'square' , 'yellow' )
        self.offsetCtrl = rt.createCtrl( 'Offset_Ctrl' , 'arrowCross' , 'yellow' )

        #-- Adjust Shape Controls
        self.allMoverCtrl.rotateShape( 0 , 45 , 0 )
        self.allMoverCtrl.scaleShape( size * 7 )
        self.offsetCtrl.scaleShape( size * 1.5 )
        
        #-- Rig process
        self.allMoverCtrl.attr('sy') >> self.allMoverCtrl.attr('sx')
        self.allMoverCtrl.attr('sy') >> self.allMoverCtrl.attr('sz')

        #-- Adjust Hierarchy
        self.offsetCtrl.parent( self.allMoverCtrl )
        mc.parent( self.ctrlGrp , self.skinGrp , self.jntGrp , self.ikhGrp , self.offsetCtrl )
        mc.parent( self.geoGrp , self.allMoverCtrl , self.stillGrp , self.assetGrp )
        
        #-- Cleanup
        for obj in ( self.assetGrp , self.geoGrp , self.jntGrp , self.ikhGrp , self.ctrlGrp , self.skinGrp , self.stillGrp ) :
            obj.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

        self.allMoverCtrl.lockHideAttrs( 'sx' , 'sz' , 'v' )

        self.jntGrp.attr('v').value = 0
        self.ikhGrp.attr('v').value = 0
        self.stillGrp.attr('v').value = 0

        mc.select( cl = True )