import maya.cmds as mc
import maya.mel as mm

class Node( object ) :

    def __init__( self , name ) :
        self.name = str( name )
        self.side = str( '_' )
        self.type = str( '' )
    
    def __str__( self ) :
        return str( self.name )

    def getName( self ) :
        return self.name

    def rename( self , newName ) :
        self.name = str(mc.rename(self.name , newName ))

    def getElemName( self ) :
        elem = self.name.split('_')

        lenElem = len(elem)
        if lenElem == 2 :
            self.name = elem[0]
            self.type = elem[-1]
        else :
            self.name = elem[0]
            self.side = '_%s_' %elem[1]
            self.type = elem[-1]
        
        return self.name , self.side , self.type

    def getExists( self ) :
        return mc.objExists( self.name )

    def getSide( self ) :
        sides = [ 'LFT' , 'RGT' , 'CEN' ,
                  'Lft' , 'Rgt' , 'Cen' , 
                  'lft' , 'rgt' , 'cen' ,
                  'L' , 'R' , 'C' ]
        
        currentSide = '_'
        for side in sides :
            if '_%s_' %side in self.name :
                currentSide = '_%s_' %side

        return currentSide

    def getNodeType( self ) :
        return mc.nodeType( self.name )

    def attr( self , attrName = '' ) :
        return Attribute( '%s.%s' % ( self.name , attrName ))

    def addAttr( self , attrName = '' , minVal = None , maxVal = None ) :
        mc.addAttr( self.name , ln = attrName , at = 'float' , k = True )

        if not minVal == None :
            mc.addAttr( '%s.%s' %( self.name , attrName ) , e = True , min = int(minVal))
        
        if not maxVal == None :
            mc.addAttr( '%s.%s' %( self.name , attrName ) , e = True , max = int(maxVal))

    def addTitleAttr( self , attrName = '' ) :
        if not mc.objExists( '%s.%s' %( self.name , attrName )) :
            mc.addAttr( self.name , ln = attrName , nn = '__%s' %attrName , at = 'float' , k = True )
            self.lockAttrs( attrName )

    def lockHideAttrs( self , *args ) :
        for arg in args :
            mc.setAttr( '%s.%s' % ( self , arg ) , l = True , k = False )
        
    def lockAttrs( self , *args ) :
        for arg in args :
            mc.setAttr( '%s.%s' % ( self , arg ) , l = True )

    def lockHideKeyableAttrs( self ) :
        attrs = mc.listAttr( self.name , k = True )
        
        for attr in attrs :
            if mc.attributeQuery( attr.split( '.' )[ 0 ] , n = self.name , multi = True ) :
                continue
            else :
                mc.setAttr( '%s.%s' % ( self.name , attr ) , l = True , k = False )
    
    def lockKeyableAttrs( self ) :
        attrs = mc.listAttr( self.name , k = True )
        
        for attr in attrs :
            if mc.attributeQuery( attr.split( '.' )[ 0 ] , n = self.name , multi = True ) :
                continue
            else :
                mc.setAttr( '%s.%s' % ( self.name , attr ) , l = True )  

class Attribute( object ) :
    
    def __init__( self , attrName = '' ) :
        self.name = str( attrName )
    
    def __str__( self ) :
        return str( self.name )
    
    def __repr__( self ) :
        return str( self.name )

    def __rshift__( self , attr = '' ):
        if mc.objExists( attr ):
            conds = mc.listConnections( self.name , p = True )
            try :
                if conds and not attr in conds:
                    mc.connectAttr( self.name , attr , f = True )
                if conds and attr in conds:
                    print '%s already connected to %s ' %( self.name , attr )
                if not conds:
                    mc.connectAttr( self.name , attr , f = True )
            except:
                print 'Can\'t connect %s to %s' %( self.name , attr )
        else:
            mc.warning('%s doest\'t exists!' %attr )

    def getLock( self ) :
        return mc.getAttr( self.name , l = True )

    def setLock( self , lock = True ) :
        mc.setAttr( self.name , l = lock )

    lock = property( getLock , setLock )
    l = property( getLock , setLock )

    def getHide( self ) :
        return mc.getAttr( self.name , k = False )

    def setHide( self , hide = True ) :
        mc.setAttr( self.name , k = not hide )
        mc.setAttr( self.name , cb = not hide )

    hide = property( getHide , setHide )
    h = property( getHide , setHide )

    def setLockHide( self ) :
        mc.setAttr( self.name , l = True , k = False , cb = False )

    def getVal( self ):
        val = mc.getAttr( self.name )
        if type( val ) == type([]) or type( val ) == type(()):
            return val[0]
        else:
            return val

    def setVal( self , val ) :
        name , attr = self.name.split('.')
        lockAttr = mc.getAttr( self.name , l = True )
    
        if lockAttr == True :
            mc.setAttr( self.name , lock = False )
            mc.setAttr( self.name , val )
            mc.setAttr( self.name , lock = True )
        else :
            mc.setAttr( self.name , val )

    value = property( getVal , setVal )
    v = property( getVal , setVal )

    def setMin( self , minVal ) :
        mc.addAttr( self.name , e = True , minValue = minVal )

    def setMax( self , maxVal ) :
        mc.addAttr( self.name , e = True , maxValue = maxVal )

    def setRange( self , minVal , maxVal ) :
        mc.addAttr( self.name , e = True , minValue = minVal , maxValue = maxVal )

    def getConnections( self ) :
        return mc.listConnections( self.name )

    connect = property( getConnections )

    def getExists( self ) :
        return mc.objExists( self )

    exists = property( getExists )

class Dag( Node ) :

    #------------------------------------------------------
    #   Information
    #------------------------------------------------------

    def getShape( self ) :
        shapes = mc.listRelatives( self.name , shapes = True )
        if shapes :
            if len( shapes ) > 1 :
                return shapes[ 0 ]
            else :
                return shapes[ 0 ]

    def correctShapeName( self ) :
        return mc.rename( self.shape , '%sShape' %self.name )

    shape = property( getShape )

    def getColor( self ) :
        return mc.getAttr( '%s.overrideColor' %self.shape )

    def setColor( self , col ) :
        colDict = { 'black'         : 1 ,
                    'gray'          : 2 ,
                    'softGray'      : 3 ,
                    'darkRed'       : 4 ,
                    'darkBlue'      : 5 ,
                    'blue'          : 6 ,
                    'darkGreen'     : 7 ,
                    'brown'         : 11 ,
                    'red'           : 13 ,
                    'green'         : 14 ,
                    'white'         : 16 ,
                    'yellow'        : 17 ,
                    'cyan'          : 18 }
        
        if type( col ) == type( str() ) :
            if col in colDict.keys() :
                colId = colDict[col]
            else :
                colId = 0
        else :
            colId = col
        
        mc.setAttr( '%s.overrideEnabled' %self.shape , 1 )
        mc.setAttr( '%s.overrideColor' %self.shape , colId )

    def getRotateOrder( self ) :
        dict = { 'xyz' : 0 ,
                 'yzx' : 1 ,
                 'zxy' : 2 ,
                 'xzy' : 3 ,
                 'yxz' : 4 ,
                 'zyx' : 5 }
                  
        id = mc.getAttr( '%s.rotateOrder' %self.name )
        
        for key in dict.keys() :
            if id == dict[ key ] :
                return id

    def setRotateOrder( self , rotateOrder ) :
        dict = { 'xyz' : 0 ,
                 'yzx' : 1 ,
                 'zxy' : 2 ,
                 'xzy' : 3 ,
                 'yxz' : 4 ,
                 'zyx' : 5 }
        
        if rotateOrder in dict.keys() :
            val = dict[ rotateOrder ]
        else :
            val = rotateOrder
            
        mc.setAttr( '%s.rotateOrder' %self.name , val )

    def getPivot( self ) :
        return mc.xform( self.name , q = True , t = True , ws = True  )

    def getDef( self , deformers ) :
        if deformers :
            listDef = [ deformers ]
        else :
            listDef = [ 'skinCluster' , 'tweak' , 'cluster' ]
        
        deformers = []
        
        for df in listDef :
            try :
                defNode = mc.listConnections( self.shape , type = df )[0]
                defSet = mc.listConnections( defNode , type = 'objectSet' )[0]
                
                deformers.append( defNode )
                deformers.append( defSet )
            except TypeError :
                continue
    
        return deformers

    def getParent( self ) :
        par = mc.listRelatives( self.name , p = True )

        if par :
            return par[0]

    def getChild( self ) :
        return mc.listRelatives( self.name , ad = True , type = 'transform' )

    def parent( self , target = '' ) :
        if target :
            mc.parent( self.name , target )

        mc.select( cl = True )

    def parentShape( self , target = '' ) :
        mc.parent( self.shape , target , r = True , s = True )
        
        target = Dag(target)
        target.correctShapeName()
        
        mc.delete( self.name )

    def freeze( self ) :
        mc.makeIdentity( self.name , a = True )
        mc.select( cl = True )

    def snap( self , target ) :
        mc.delete( mc.parentConstraint ( target , self.name , mo = False ))
        mc.select( cl = True )

    def snapPoint( self , target ) :
        mc.delete( mc.pointConstraint ( target , self.name , mo = False ))
        mc.select( cl = True )

    def snapOrient( self , target ) :
        mc.delete( mc.orientConstraint ( target , self.name , mo = False ))
        mc.select( cl = True )
    
    def snapScale( self , target ) :
        mc.delete( mc.scaleConstraint ( target , self.name , mo = False ))
        mc.select( cl = True )

    def snapAim( self , target , aim = (0,1,0) , upvec = (1,0,0)) :
        mc.delete( mc.aimConstraint ( target , self.name , mo = False  , aimVector = aim , upVector = upvec ))
        mc.select( cl = True )

    def snapJntOrient( self , target ) :
        mc.delete( mc.orientConstraint (( target , self.name ) , mo = False ))
        ro = mc.getAttr( '%s.rotate' %self.name )[0]
        mc.setAttr( '%s.jointOrient' %self.name , ro[0] , ro[1] , ro[2] )
        mc.setAttr( '%s.rotate' %self.name , 0 , 0 , 0 )

    def clearCons( self ) :
        listAttr = [ 'translateX' , 'translateY' , 'translateZ' , 'rotateX' , 'rotateY' , 'rotateZ' , 'scaleX' , 'scaleY' , 'scaleZ' ]
        
        for attr in listAttr :
            mc.setAttr( '%s.%s' %( self.name , attr ) , l = False )
            
        cons = mc.listRelatives( self.name , ad = True , type = 'constraint' )
        
        if cons :
            for con in cons :
                if self.name in con :
                    mc.delete(con)

    def translateShape( self  , target ) :
        piv = self.getPivot()
        shape = self.getShape()

        degree = mc.getAttr( '%s.degree' %shape )
        spans = mc.getAttr( '%s.spans' %shape )

        cv = ( degree + spans ) - 1

        tx = mc.getAttr( '%s.tx' %target )
        ty = mc.getAttr( '%s.ty' %target )
        tz = mc.getAttr( '%s.tz' %target )

        mc.select( self.name + '.cv[0:%s]' %cv )
        mc.move( tx ,ty ,tz , r = True , os = True , wd = True )
        mc.select( cl = True )

    def rotateShape( self , rx , ry , rz ) :
        piv = self.getPivot()
        shape = self.getShape()

        degree = mc.getAttr( '%s.degree' %shape )
        spans = mc.getAttr( '%s.spans' %shape )

        cv = ( degree + spans ) - 1

        mc.select( self.name + '.cv[0:%s]' %cv )
        mc.rotate( rx ,ry ,rz , r = True , os = True ,  p = ( piv[0] , piv[1] , piv[2] ))
        mc.select( cl = True )

    def scaleShape( self , value ) :
        shape = self.getShape()

        degree = mc.getAttr( '%s.degree' %shape )
        spans = mc.getAttr( '%s.spans' %shape )

        cv = ( degree + spans ) - 1

        mc.select( self.name + '.cv[0:%s]' %cv )
        mc.scale( value , value , value , r = True )
        mc.select( cl = True )

    def show( self ) :
        mc.setAttr( '%s.v' %self.name , 1 )

    def hide( self ) :
        mc.setAttr( '%s.v' %self.name , 0 )

    def setDisplay( self , type ):
        typeDics = { 'normal'    : 0 ,
                     'template'  : 1 ,
                     'reference' : 2 }

        if type in typeDics.keys():
            mc.setAttr( '%s.overrideEnabled' %self.shape , 1 )
            mc.setAttr( '%s.overrideDisplayType' %self.shape , typeDics[type] )
            
            if type == 'normal':
                mc.setAttr( '%s.overrideEnabled' %self.shape , 1 )

