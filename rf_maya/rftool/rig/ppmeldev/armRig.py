import maya.cmds as mc
import maya.mel as mm
import core as core
import rigTools as rt
import ribbonRig
reload( core )
reload( rt )
reload( ribbonRig )

# -------------------------------------------------------------------------------------------------------------
#
#  ARM RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , upArmTmpJnt   = 'UpArm_L_tmpJnt' ,
                         forearmTmpJnt = 'Forearm_L_tmpJnt' ,
                         wristTmpJnt   = 'Wrist_L_tmpJnt' ,
                         handTmpJnt    = 'Hand_L_tmpJnt' ,
                         elbowTmpJnt   = 'Elbow_L_tmpJnt' ,
                         parent        = 'ClavEnd_L_Jnt' , 
                         ctrlGrp       = 'Ctrl_Grp' ,
                         jntGrp        = 'Jnt_Grp' ,
                         skinGrp       = 'Skin_Grp' ,
                         stillGrp      = 'Still_Grp' ,
                         ikhGrp        = 'Ikh_Grp' ,
                         elem          = '' ,
                         side          = 'L' ,
                         ribbon        = True ,
                         size          = 1 
                 ):

        ##-- Info
        elem = elem.capitalize()

        if side == '' :
            sideRbn = ''
            side = '_'
        else :
            sideRbn = '%s' %side
            side = '_%s_' %side

        if 'L' in side  :
            valueSide = 1
            axis = 'y+'
            upVec = [ 0 , 0 , 1 ]
        elif 'R' in side :
            valueSide = -1
            axis = 'y-'
            upVec = [ 0 , 0 , -1 ]

        #-- Create Main Group
        self.armCtrlGrp = rt.createNode( 'transform' , 'Arm%sCtrl%sGrp' %( elem , side ))
        self.armJntGrp = rt.createNode( 'transform' , 'Arm%sJnt%sGrp' %( elem , side ))
        mc.parentConstraint( parent , self.armCtrlGrp , mo = False )
        self.armJntGrp.snap( parent )
        
        #-- Create Joint
        self.upArmJnt = rt.createJnt( 'UpArm%s%sJnt' %( elem , side ) , upArmTmpJnt )
        self.forearmJnt = rt.createJnt( 'Forearm%s%sJnt' %( elem , side ) , forearmTmpJnt )
        self.wristJnt = rt.createJnt( 'Wrist%s%sJnt' %( elem , side ) , wristTmpJnt )
        self.handJnt = rt.createJnt( 'Hand%s%sJnt' %( elem , side ) , handTmpJnt )
        self.handJnt.parent( self.wristJnt )
        self.wristJnt.parent( self.forearmJnt )
        self.forearmJnt.parent( self.upArmJnt )
        self.upArmJnt.parent( parent )

        #-- Create Controls
        self.armCtrl = rt.createCtrl( 'Arm%s%sCtrl' %( elem , side ) , 'stick' , 'green' )
        self.armZro = rt.addGrp( self.armCtrl )

        #-- Adjust Shape Controls
        self.armCtrl.scaleShape( size )

        if 'L' in side :
            self.armCtrl.rotateShape( 0 , 90 , 90 )
        else :
            self.armCtrl.rotateShape( 0 , 90 , -90 )
        
        #-- Adjust Rotate Order
        for obj in ( self.upArmJnt , self.forearmJnt , self.wristJnt , self.handJnt ) :
            obj.setRotateOrder( 'yzx' )

        #-- Rig Process
        mc.parentConstraint( self.wristJnt , self.armZro , mo = False )
        upArmNonRollJnt = rt.addNonRollJnt( 'UpArm' , self.upArmJnt , self.forearmJnt , parent , axis )
        forearmNonRollJnt = rt.addNonRollJnt( 'Forearm' , self.forearmJnt , self.wristJnt , self.upArmJnt , axis )

        self.upArmNonRollJntGrp = upArmNonRollJnt['jntGrp']
        self.upArmNonRollRootNr = upArmNonRollJnt['rootNr']
        self.upArmNonRollRootNrZro = upArmNonRollJnt['rootNrZro']
        self.upArmNonRollEndNr = upArmNonRollJnt['endNr']
        self.upArmNonRollIkhNr = upArmNonRollJnt['ikhNr']
        self.upArmNonRollIkhNrZro = upArmNonRollJnt['ikhNrZro']
        self.upArmNonRollTwistGrp = upArmNonRollJnt['twistGrp']

        self.forearmNonRollJntGrp = forearmNonRollJnt['jntGrp']
        self.forearmNonRollRootNr = forearmNonRollJnt['rootNr']
        self.forearmNonRollRootNrZro = forearmNonRollJnt['rootNrZro']
        self.forearmNonRollEndNr = forearmNonRollJnt['endNr']
        self.forearmNonRollIkhNr = forearmNonRollJnt['ikhNr']
        self.forearmNonRollIkhNrZro = forearmNonRollJnt['ikhNrZro']
        self.forearmNonRollTwistGrp = forearmNonRollJnt['twistGrp']

        #-- Adjust Hierarchy
        mc.parent( self.armZro , self.armCtrlGrp )
        mc.parent( self.armCtrlGrp , ctrlGrp )
        mc.parent( self.upArmNonRollJntGrp , self.forearmNonRollJntGrp , self.armJntGrp )
        mc.parent( self.armJntGrp , jntGrp )

        #-- Cleanup
        for obj in ( self.armZro , self.armJntGrp , self.upArmNonRollJntGrp , self.forearmNonRollJntGrp , self.armCtrlGrp ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        self.armCtrl.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        #-- Fk
        #-- Create Main Group Fk
        self.armFkCtrlGrp = rt.createNode( 'transform' , 'Arm%sFkCtrl%sGrp' %( elem , side ))
        self.armFkJntGrp = rt.createNode( 'transform' , 'Arm%sFkJnt%sGrp' %( elem , side ))

        self.armFkCtrlGrp.snap( self.armCtrlGrp )
        self.armFkJntGrp.snap( self.armCtrlGrp )
    
        #-- Create Joint Fk
        self.upArmFkJnt = rt.createJnt( 'UpArm%sFk%sJnt' %( elem , side ) , upArmTmpJnt )
        self.forearmFkJnt = rt.createJnt( 'Forearm%sFk%sJnt' %( elem , side ) , forearmTmpJnt )
        self.wristFkJnt = rt.createJnt( 'Wrist%sFk%sJnt' %( elem , side ) , wristTmpJnt )
        self.handFkJnt = rt.createJnt( 'Hand%sFk%sJnt' %( elem , side ) , handTmpJnt )
        self.handFkJnt.parent( self.wristFkJnt )
        self.wristFkJnt.parent( self.forearmFkJnt )
        self.forearmFkJnt.parent( self.upArmFkJnt )
        self.upArmFkJnt.parent( self.armFkJntGrp )

        #-- Create Controls Fk
        self.upArmFkCtrl = rt.createCtrl( 'UpArm%sFk%sCtrl' %( elem , side ) , 'cylinder' , 'red' , jnt = True )
        self.upArmFkGmbl = rt.addGimbal( self.upArmFkCtrl )
        self.upArmFkZro = rt.addGrp( self.upArmFkCtrl )
        self.upArmFkOfst = rt.addGrp( self.upArmFkCtrl , 'Ofst' )
        self.upArmFkZro.snapPoint( self.upArmFkJnt )
        self.upArmFkCtrl.snapJntOrient( self.upArmFkJnt )

        self.forearmFkCtrl = rt.createCtrl( 'Forearm%sFk%sCtrl' %( elem , side ) , 'cylinder' , 'red' , jnt = True )
        self.forearmFkGmbl = rt.addGimbal( self.forearmFkCtrl )
        self.forearmFkZro = rt.addGrp( self.forearmFkCtrl )
        self.forearmFkOfst = rt.addGrp( self.forearmFkCtrl )
        self.forearmFkZro.snapPoint( self.forearmFkJnt )
        self.forearmFkCtrl.snapJntOrient( self.forearmFkJnt )

        self.wristFkCtrl = rt.createCtrl( 'Wrist%sFk%sCtrl' %( elem , side ) , 'cylinder' , 'red' , jnt = True )
        self.wristFkGmbl = rt.addGimbal( self.wristFkCtrl )
        self.wristFkZro = rt.addGrp( self.wristFkCtrl )
        self.wristFkOfst = rt.addGrp( self.wristFkCtrl )
        self.wristFkZro.snapPoint( self.wristFkJnt )
        self.wristFkCtrl.snapJntOrient( self.wristFkJnt )

        #-- Adjust Shape Controls Fk
        for ctrl in ( self.upArmFkCtrl , self.upArmFkGmbl , self.forearmFkCtrl , self.forearmFkGmbl , self.wristFkCtrl , self.wristFkGmbl ) :
            ctrl.scaleShape( size )
    
        #-- Adjust Rotate Order Fk
        for obj in ( self.upArmFkJnt , self.forearmFkJnt , self.wristFkJnt , self.handFkJnt , self.upArmFkCtrl , self.forearmFkCtrl , self.wristFkCtrl ) :
            obj.setRotateOrder( 'yzx' )

        #-- Rig Process Fk
        mc.parentConstraint( self.upArmFkGmbl , self.upArmFkJnt , mo = False )
        mc.parentConstraint( self.forearmFkGmbl , self.forearmFkJnt , mo = False )
        mc.parentConstraint( self.wristFkGmbl , self.wristFkJnt , mo = False )

        rt.addFkStretch( self.upArmFkCtrl , self.forearmFkOfst , 'x' , valueSide )
        rt.addFkStretch( self.forearmFkCtrl , self.wristFkOfst , 'x' , valueSide )

        rt.localWorld( self.upArmFkCtrl , ctrlGrp , self.armFkCtrlGrp , self.upArmFkZro , 'orient' )

        self.upArmFkCtrl.attr('localWorld').value = 1
    
        #-- Adjust Hierarchy Fk
        mc.parent( self.armFkCtrlGrp , self.armCtrlGrp )
        mc.parent( self.armFkJntGrp , self.armJntGrp )
        mc.parent( self.upArmFkZro , self.armFkCtrlGrp )
        mc.parent( self.forearmFkZro , self.upArmFkGmbl )
        mc.parent( self.wristFkZro , self.forearmFkGmbl )

        #-- Ik
        #-- Create Main Group Ik
        self.armIkCtrlGrp = rt.createNode( 'transform' , 'Arm%sIkCtrl%sGrp' %( elem , side ))
        self.armIkJntGrp = rt.createNode( 'transform' , 'Arm%sIkJnt%sGrp' %( elem , side ))
        self.armIkhGrp = rt.createNode( 'transform' , 'Arm%sIkh%sGrp' %( elem , side ))

        self.armIkCtrlGrp.snap( self.armCtrlGrp )
        self.armIkJntGrp.snap( self.armCtrlGrp )
        self.armIkhGrp.snap( self.armCtrlGrp )
    
        #-- Create Joint Ik
        self.upArmIkJnt = rt.createJnt( 'UpArm%sIk%sJnt' %( elem , side ) , upArmTmpJnt )
        self.forearmIkJnt = rt.createJnt( 'Forearm%sIk%sJnt' %( elem , side ) , forearmTmpJnt )
        self.wristIkJnt = rt.createJnt( 'Wrist%sIk%sJnt' %( elem , side ) , wristTmpJnt )
        self.handIkJnt = rt.createJnt( 'Hand%sIk%sJnt' %( elem , side ) , handTmpJnt )
        self.handIkJnt.parent( self.wristIkJnt )
        self.wristIkJnt.parent( self.forearmIkJnt )
        self.forearmIkJnt.parent( self.upArmIkJnt )
        self.upArmIkJnt.parent( self.armIkJntGrp )

        #-- Create Controls Ik
        self.upArmIkCtrl = rt.createCtrl( 'UpArm%sIk%sCtrl' %( elem , side ) , 'cube' , 'blue' , jnt = True )
        self.upArmIkGmbl = rt.addGimbal( self.upArmIkCtrl )
        self.upArmIkZro = rt.addGrp( self.upArmIkCtrl )
        self.upArmIkOfst = rt.addGrp( self.upArmIkCtrl , 'Ofst' )
        self.upArmIkZro.snapPoint( self.upArmIkJnt )
        self.upArmIkCtrl.snapJntOrient( self.upArmIkJnt )

        self.wristIkCtrl = rt.createCtrl( 'Wrist%sIk%sCtrl' %( elem , side ) , 'cube' , 'blue' , jnt = True )
        self.wristIkGmbl = rt.addGimbal( self.wristIkCtrl )
        self.wristIkZro = rt.addGrp( self.wristIkCtrl )
        self.wristIkOfst = rt.addGrp( self.wristIkCtrl )
        self.wristIkZro.snapPoint( self.wristIkJnt )
        self.wristIkCtrl.snapJntOrient( self.wristIkJnt )

        self.elbowIkCtrl = rt.createCtrl( 'Elbow%sIk%sCtrl' %( elem , side ) , 'sphere' , 'blue' , jnt = True )
        self.elbowIkZro = rt.addGrp( self.elbowIkCtrl )
        self.elbowIkZro.snapPoint( elbowTmpJnt )

        #-- Adjust Shape Controls Ik
        for ctrl in ( self.upArmIkCtrl , self.upArmIkGmbl , self.wristIkCtrl , self.wristIkGmbl , self.elbowIkCtrl ) :
            ctrl.scaleShape( size )
    
        #-- Adjust Rotate Order Ik
        for obj in ( self.upArmIkJnt , self.wristIkJnt , self.handIkJnt , self.upArmIkCtrl , self.wristIkCtrl ) :
            obj.setRotateOrder( 'yzx' )

        #-- Rig Process Ik
        rt.localWorld( self.wristIkCtrl , ctrlGrp , self.upArmIkGmbl , self.wristIkZro , 'parent' )
        rt.localWorld( self.elbowIkCtrl , ctrlGrp , self.wristIkCtrl , self.elbowIkZro , 'parent' )
    
        self.crv = rt.drawLine( self.elbowIkCtrl , self.forearmIkJnt )

        self.wristIkhDict = rt.addIkh( 'Wrist%s' %elem , 'ikRPsolver' , self.upArmIkJnt , self.wristIkJnt )
        self.handIkhDict = rt.addIkh( 'Hand%s' %elem , 'ikSCsolver' , self.wristIkJnt , self.handIkJnt )

        self.wristIkh = self.wristIkhDict['ikh']
        self.wristIkhZro = self.wristIkhDict['ikhZro']
        self.handIkh = self.handIkhDict['ikh']
        self.handIkhZro = self.handIkhDict['ikhZro']

        self.polevector = mc.poleVectorConstraint( self.elbowIkCtrl , self.wristIkh )[0]

        self.wristIkCtrl.addAttr( 'twist' )

        self.wristIkCtrl.attr( 'twist' ) >> self.wristIkh.attr('twist')

        mc.pointConstraint( self.upArmIkGmbl , self.upArmIkJnt , mo = False )
        mc.parentConstraint( self.wristIkGmbl , self.wristIkhZro , mo = True )
        mc.parentConstraint( self.wristIkGmbl , self.handIkhZro , mo = True )

        self.ikStrt = rt.addIkStretch( self.wristIkCtrl , self.elbowIkCtrl ,'ty' , 'Arm%s' %elem , self.upArmIkJnt , self.forearmIkJnt , self.wristIkJnt )
        
        self.wristIkCtrl.attr('localWorld').value = 1
        self.wristIkCtrl.attr('autoStretch').value = 1

        #-- Adjust Hierarchy Ik
        mc.parent( self.armIkCtrlGrp , self.armCtrlGrp )
        mc.parent( self.armIkJntGrp , self.armJntGrp )
        mc.parent( self.armIkhGrp , ikhGrp )
        mc.parent( self.upArmIkZro , self.ikStrt , self.crv , self.armIkCtrlGrp )
        mc.parent( self.wristIkZro , self.elbowIkZro , self.upArmIkGmbl )
        mc.parent( self.wristIkhZro , self.handIkhZro , self.armIkhGrp )

        #-- Blending Fk Ik 
        rt.blendFkIk( self.armCtrl , self.upArmFkJnt , self.upArmIkJnt , self.upArmJnt )
        rt.blendFkIk( self.armCtrl , self.forearmFkJnt , self.forearmIkJnt , self.forearmJnt )
        rt.blendFkIk( self.armCtrl , self.wristFkJnt , self.wristIkJnt , self.wristJnt )
        
        if mc.objExists( 'Arm%sFkIk%sRev' %( elem , side )) :
            self.revFkIk = core.Dag('Arm%sFkIk%sRev' %( elem , side ))

            self.armCtrl.attr( 'fkIk' ) >> self.armIkCtrlGrp.attr('v')
            self.revFkIk.attr( 'ox' ) >> self.armFkCtrlGrp.attr('v')

        #-- Add Hand Scale
        self.armCtrl.addAttr('handScale')
        self.armCtrl.attr('handScale').value = 1

        self.handSclMdv = rt.createNode( 'multiplyDivide' , 'Hand%sScale%sMdv' %( elem , side ))

        self.armCtrl.attr('handScale') >> self.handSclMdv.attr('i1x')
        self.handSclMdv.attr('ox') >> self.wristJnt.attr('sx')
        self.handSclMdv.attr('ox') >> self.wristJnt.attr('sy')
        self.handSclMdv.attr('ox') >> self.wristJnt.attr('sz')
        self.handSclMdv.attr('ox') >> self.wristFkJnt.attr('sx')
        self.handSclMdv.attr('ox') >> self.wristFkJnt.attr('sy')
        self.handSclMdv.attr('ox') >> self.wristFkJnt.attr('sz')
        self.handSclMdv.attr('i2x').value = 1
        self.handJnt.attr('ssc').value = 0

        #-- Cleanup Fk
        for obj in ( self.armFkCtrlGrp , self.armFkJntGrp , self.upArmFkZro , self.upArmFkOfst , self.forearmFkZro , self.forearmFkOfst , self.wristFkZro , self.wristFkOfst ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for obj in ( self.upArmFkCtrl , self.upArmFkGmbl , self.forearmFkCtrl , self.forearmFkGmbl , self.wristFkCtrl , self.wristFkGmbl ) :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        #-- Cleanup Ik
        for obj in ( self.armIkCtrlGrp , self.armIkJntGrp , self.armIkhGrp , self.upArmIkZro , self.elbowIkZro , self.wristIkZro ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for obj in ( self.upArmIkCtrl , self.upArmIkGmbl , self.elbowIkCtrl , self.wristIkCtrl , self.wristIkGmbl ) :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        #-- Ribbon
        if ribbon == True :
            self.upArmPar   = self.upArmJnt.getParent()
            self.forearmPar = self.forearmJnt.getParent()
            self.wristPar   = self.wristJnt.getParent()

            #-- Create Main Group Ribbon
            self.armRbnCtrlGrp = rt.createNode( 'transform' , 'Arm%sRbnCtrl%sGrp' %( elem , side ))
            self.armRbnJntGrp = rt.createNode( 'transform' , 'Arm%sRbnJnt%sGrp' %( elem , side ))
            self.armRbnSkinGrp = rt.createNode( 'transform' , 'Arm%sRbnSkin%sGrp' %( elem , side ))
            self.armRbnStillGrp = rt.createNode( 'transform' , 'Arm%sRbnStill%sGrp' %( elem , side ))
            mc.parent( self.armRbnCtrlGrp , self.armCtrlGrp )
            mc.parent( self.armRbnJntGrp , self.armJntGrp )
            mc.parent( self.armRbnSkinGrp , skinGrp )
            mc.parent( self.armRbnStillGrp , stillGrp )

            #-- Create Controls Ribbon
            self.armRbnCtrl = rt.createCtrl( 'Arm%sRbn%sCtrl' %( elem , side ) , 'locator' , 'yellow' , jnt = True )
            self.armRbnZro = rt.addGrp( self.armRbnCtrl )
            self.armRbnZro.snapPoint( self.forearmJnt )
            self.armRbnCtrl.snapJntOrient( self.forearmJnt )
            mc.parent( self.armRbnZro , self.armRbnCtrlGrp )

            #-- Adjust Shape Controls Ribbon
            self.armRbnCtrl.scaleShape( size )

            #-- Rig Process Ribbon
            mc.parentConstraint( self.forearmJnt , self.armRbnZro , mo = True )

            distRbnUpArm = rt.getDist( self.upArmJnt , self.forearmJnt )
            distRbnForearm = rt.getDist( self.forearmJnt , self.wristJnt )

            self.rbnUpArm = ribbonRig.RibbonHi(  name = 'UpArm%s' %elem , 
                                                 axis = axis ,
                                                 side = sideRbn ,
                                                 dist = distRbnUpArm )
            
            self.rbnForearm = ribbonRig.RibbonHi(  name = 'Forearm%s' %elem , 
                                                   axis = axis ,
                                                   side = sideRbn ,
                                                   dist = distRbnForearm )

            self.rbnUpArm.rbnCtrlGrp.snapPoint(self.upArmJnt)
    
            mc.delete( 
                           mc.aimConstraint( self.forearmJnt , self.rbnUpArm.rbnCtrlGrp , 
                                             aim = self.rbnUpArm.aimVec ,
                                             u = upVec ,
                                             wut='objectrotation' ,
                                             wuo = self.upArmJnt ,
                                             wu= (0,0,1))
                     )
    
            mc.parentConstraint( self.upArmJnt , self.rbnUpArm.rbnCtrlGrp , mo = True )
            mc.parentConstraint( self.upArmJnt , self.rbnUpArm.rbnRootCtrl , mo = True )
            mc.parentConstraint( self.armRbnCtrl , self.rbnUpArm.rbnEndCtrl , mo = True )

            for ctrl in ( self.rbnUpArm.rbnRootCtrl , self.rbnUpArm.rbnEndCtrl) :
                ctrl.lockHideAttrs('tx', 'ty', 'tz' , 'rx' , 'ry' , 'rz' )
                ctrl.hide()

            self.rbnForearm.rbnCtrlGrp.snapPoint(self.forearmJnt)
    
            mc.delete( 
                           mc.aimConstraint( self.wristJnt , self.rbnForearm.rbnCtrlGrp , 
                                             aim = self.rbnForearm.aimVec ,
                                             u = upVec ,
                                             wut='objectrotation' ,
                                             wuo = self.forearmJnt ,
                                             wu= (0,0,1))
                     )
    
            mc.parentConstraint( self.forearmJnt , self.rbnForearm.rbnCtrlGrp , mo = True )
            mc.parentConstraint( self.armRbnCtrl , self.rbnForearm.rbnRootCtrl , mo = True )
            mc.parentConstraint( self.wristJnt , self.rbnForearm.rbnEndCtrl , mo = True )

            for ctrl in ( self.rbnForearm.rbnRootCtrl , self.rbnForearm.rbnEndCtrl) :
                ctrl.lockHideAttrs('tx', 'ty', 'tz' , 'rx' , 'ry' , 'rz' )
                ctrl.hide()

            #-- Adjust Ribbon
            self.rbnUpArmCtrlShape = core.Dag(self.rbnUpArm.rbnCtrl.shape)
            self.rbnForearmCtrlShape = core.Dag(self.rbnForearm.rbnCtrl.shape)

            self.rbnUpArmCtrlShape.attr('rootTwistAmp').value = -1
            self.rbnForearmCtrlShape.attr('rootTwistAmp').value = -1

            #-- Twist Distribute Ribbon
            self.upArmNonRollTwistGrp.attr('ry') >> self.rbnUpArmCtrlShape.attr('rootAbsoluteTwist')
            self.wristJnt.attr('ry') >> self.rbnForearmCtrlShape.attr('endAbsoluteTwist')

            #-- Adjust Hierarchy Ribbon
            mc.parent( self.rbnUpArm.rbnCtrlGrp , self.rbnForearm.rbnCtrlGrp , self.armRbnCtrlGrp )
            mc.parent( self.rbnUpArm.rbnJntGrp , self.rbnForearm.rbnJntGrp , self.armRbnJntGrp )
            mc.parent( self.rbnUpArm.rbnSkinGrp , self.rbnForearm.rbnSkinGrp , self.armRbnSkinGrp )
            mc.parent( self.rbnUpArm.rbnStillGrp , self.rbnForearm.rbnStillGrp , self.armRbnStillGrp )

            #-- Cleanup Ribbon
            for obj in ( self.armRbnJntGrp , self.armRbnSkinGrp , self.armRbnStillGrp ) :
                obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' )

            self.armRbnCtrlGrp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v')
            
            self.rbnUpArm.rbnCtrl.attr('autoTwist').value = 1
            self.rbnForearm.rbnCtrl.attr('autoTwist').value = 1

        mc.select( cl = True )