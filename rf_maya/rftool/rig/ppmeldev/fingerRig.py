import maya.cmds as mc
import maya.mel as mm
import core as core
import rigTools as rt
import ribbonRig
reload( core )
reload( rt )
reload( ribbonRig )

# -------------------------------------------------------------------------------------------------------------
#
#  FINGER RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , fngr          = 'Index' ,
                         fngrTmpJnt    =['Index1_L_TmpJnt' , 
                                         'Index2_L_TmpJnt' , 
                                         'Index3_L_TmpJnt' , 
                                         'Index4_L_TmpJnt' , 
                                         'Index5_L_TmpJnt' ] ,
                         armCtrl       = 'Arm_L_Ctrl' ,
                         parent        = 'Hand_L_Jnt' , 
                         ctrlGrp       = 'Ctrl_Grp' ,
                         elem          = '' ,
                         side          = 'L' ,
                         size          = 1 
                 ):

        ##-- Info
        name = '%s%s' %( fngr , elem )

        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        if 'L' in side  :
            valueSide = 1
        elif 'R' in side :
            valueSide = -1

        self.fngrJntDict = []
        self.fngrCtrlDict = []
        self.fngrGmblDict = []
        self.fngrZroDict = []
        self.fngrDrvDict = []
        self.fngrDrvAMdvDict = []
        self.fngrDrvBMdvDict = []
        self.fngrRxPmaDict = []
        self.fngrRyPmaDict = []
        self.fngrRzPmaDict = []
        self.fngrTyPmaDict = []

        self.fngrMember = len(fngrTmpJnt)
        
        slideValue = { '1' : 0 , '2' : 3.5 , '3' : -7 , '4' : 3.5 , '5' : 0 , '6' : 0 }
        scruchValue = { '1' : 1 , '2' : 3.5 , '3' : -8 , '4' : -8 , '5' : 0 , '6' : 0 }
        
        #-- Create Main Group
        self.fngrCtrlGrp = rt.createNode( 'transform' , '%sCtrl%sGrp' %( name , side ))
        mc.parentConstraint( parent , self.fngrCtrlGrp , mo = False )
        mc.scaleConstraint( parent , self.fngrCtrlGrp , mo = False )

        #-- Create Controls
        self.fngrMainCtrl = rt.createCtrl( '%sAttr%sCtrl' %( name , side ) , 'stick' , 'blue' )
        self.fngrMainZro = rt.addGrp( self.fngrMainCtrl )
        self.fngrMainZro.snap( fngrTmpJnt[0] )

        #-- Adjust Shape Controls
        if 'L' in side :
            self.fngrMainCtrl.rotateShape( 90 , 0 , 0 )
        else :
            self.fngrMainCtrl.rotateShape( -90 , 0 , 0 )

        #-- Add Attribute
        self.fngrMainCtrl.addAttr( 'fold' )
        self.fngrMainCtrl.addAttr( 'twist' )
        self.fngrMainCtrl.addAttr( 'slide' )
        self.fngrMainCtrl.addAttr( 'scruch' )
        self.fngrMainCtrl.addAttr( 'stretch' )

        for i in range(self.fngrMember-1) :
            self.fngrMainCtrl.addTitleAttr( 'Finger%s' %(i+1))
            self.fngrMainCtrl.addAttr( 'fold%s' %(i+1))
            self.fngrMainCtrl.addAttr( 'twist%s' %(i+1))
            self.fngrMainCtrl.addAttr( 'spread%s' %(i+1))

        self.fngrMainCtrlShape = core.Dag(self.fngrMainCtrl.shape)
        self.fngrMainCtrlShape.addAttr( 'detailCtrl' )

        #-- Detail Controls
        for i in range(self.fngrMember) :
            #-- Create Joint
            self.fngrJnt = rt.createJnt( '%s%s%sJnt' %( name , i+1 , side ) , fngrTmpJnt[i] )
            self.fngrJntDict.append( self.fngrJnt )

            if not i == (self.fngrMember-1) :
                #-- Create Controls
                self.fngrCtrl = rt.createCtrl( '%s%s%sCtrl' %( name , i+1 , side ) , 'cylinder' , 'red' )
                self.fngrGmbl = rt.addGimbal( self.fngrCtrl )
                self.fngrZro = rt.addGrp( self.fngrCtrl )
                self.fngrDrv = rt.addGrp( self.fngrCtrl , 'Drv' )
                self.fngrZro.snap( self.fngrJnt )

                #-- Adjust Shape Controls
                for ctrl in ( self.fngrCtrl , self.fngrGmbl ) :
                    ctrl.scaleShape( size*0.35 )
            
                #-- Adjust Rotate Order
                for obj in ( self.fngrCtrl , self.fngrJnt ) :
                    obj.setRotateOrder( 'xyz' )
                
                #-- Rig process
                mc.parentConstraint( self.fngrGmbl , self.fngrJnt , mo = False )
                rt.addSquash( self.fngrCtrl , self.fngrJnt , ax = ( 'sx' , 'sz'  )) 

                #-- Create Node
                #-- DrvA == fold(x) , twist(y) , spread(z)
                #-- DrvB == slide(x) , scruch(y) , stretch(z)
                self.fngrDrvAMdv = rt.createNode( 'multiplyDivide' , '%s%sDrvA%sMdv' %( name , i+1 , side ))
                self.fngrDrvBMdv = rt.createNode( 'multiplyDivide' , '%s%sDrvB%sMdv' %( name , i+1 , side ))
                self.fngrRxPma = rt.createNode( 'plusMinusAverage' , '%s%sRx%sPma' %( name , i+1 , side ))
                self.fngrRyPma = rt.createNode( 'plusMinusAverage' , '%s%sRy%sPma' %( name , i+1 , side ))
                self.fngrRzPma = rt.createNode( 'plusMinusAverage' , '%s%sRz%sPma' %( name , i+1 , side ))
                self.fngrTyPma = rt.createNode( 'plusMinusAverage' , '%s%sTy%sPma' %( name , i+1 , side ))
                
                self.fngrMainCtrl.attr('fold%s' %(i+1)) >> self.fngrDrvAMdv.attr('i1x')
                self.fngrMainCtrl.attr('twist%s' %(i+1)) >> self.fngrDrvAMdv.attr('i1y')
                self.fngrMainCtrl.attr('spread%s' %(i+1)) >> self.fngrDrvAMdv.attr('i1z')
                
                self.fngrMainCtrl.attr('slide') >> self.fngrDrvBMdv.attr('i1x')
                self.fngrMainCtrl.attr('scruch') >> self.fngrDrvBMdv.attr('i1y')
                self.fngrMainCtrl.attr('stretch') >> self.fngrDrvBMdv.attr('i1z')

                self.fngrDrvAMdv.attr('ox') >> self.fngrRxPma.attr('i1[0]')
                self.fngrDrvAMdv.attr('oy') >> self.fngrRyPma.attr('i1[0]')
                self.fngrDrvAMdv.attr('oz') >> self.fngrRzPma.attr('i1[0]')
                self.fngrDrvBMdv.attr('ox') >> self.fngrRxPma.attr('i1[1]')
                self.fngrDrvBMdv.attr('oy') >> self.fngrRxPma.attr('i1[2]')
                self.fngrDrvBMdv.attr('oz') >> self.fngrTyPma.attr('i1[0]')

                self.fngrRxPma.attr('o1') >> self.fngrDrv.attr('rx')
                self.fngrRyPma.attr('o1') >> self.fngrDrv.attr('ry')
                self.fngrRzPma.attr('o1') >> self.fngrDrv.attr('rz')
                self.fngrTyPma.attr('o1') >> self.fngrDrv.attr('ty')
                
                #-- Adjust Attribute Value
                self.fngrDrvAMdv.attr('i2x').value = -9
                self.fngrDrvAMdv.attr('i2y').value = 1
                self.fngrDrvAMdv.attr('i2z').value = 1
                self.fngrDrvBMdv.attr('i2x').value = slideValue['%s'%(i+1)]
                self.fngrDrvBMdv.attr('i2y').value = scruchValue['%s'%(i+1)]
                self.fngrDrvBMdv.attr('i2z').value = valueSide

                #-- Info
                self.fngrCtrlDict.append( self.fngrCtrl )
                self.fngrGmblDict.append( self.fngrGmbl )
                self.fngrZroDict.append( self.fngrZro )
                self.fngrDrvDict.append( self.fngrDrv )
                self.fngrDrvAMdvDict.append( self.fngrDrvAMdv )
                self.fngrDrvBMdvDict.append( self.fngrDrvBMdv )
                self.fngrRxPmaDict.append( self.fngrRxPma )
                self.fngrRyPmaDict.append( self.fngrRyPma )
                self.fngrRzPmaDict.append( self.fngrRzPma )
                self.fngrTyPmaDict.append( self.fngrTyPma )

        self.fngrFoldMdv = rt.createNode( 'multiplyDivide' , '%sFold%sMdv' %( name , side ))
        self.fngrTwistMdv = rt.createNode( 'multiplyDivide' , '%sTwist%sMdv' %( name , side ))

        self.fngrMainCtrl.attr('fold') >> self.fngrFoldMdv.attr('i1x')
        self.fngrMainCtrl.attr('twist') >> self.fngrTwistMdv.attr('i1x')
        self.fngrFoldMdv.attr('i2x').value = -9

        self.fngrMainCtrlShape.attr('detailCtrl') >> self.fngrZroDict[0].attr('v')

        for i in range(self.fngrMember-1) :
            if not i == 0 :
                self.fngrFoldMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[3]')
                self.fngrTwistMdv.attr('ox') >> self.fngrRyPmaDict[i].attr('i1[1]')

        #-- Create Attribute Main Controls
        if armCtrl :
            self.armCtrl = core.Dag(armCtrl)
            self.armCtrlShape = core.Dag(self.armCtrl.shape)
            
            self.armCtrl.addTitleAttr('Finger')

            for attr in ('fist','relax','slide','scruch','baseSpread','spread','baseBreak','break','baseFlex','flex') :
                if not mc.objExists( '%s.%s' %( self.armCtrl , attr )) :
                    self.armCtrl.addAttr(attr)
            
            self.armCtrlShape.addTitleAttr( '%s' %fngr )
            
            #-- Fist
            for i in range(self.fngrMember-1):
                fngrNum = '%s%s' %(fngr,i+1)

                self.fistMainMdv = rt.createNode( 'multiplyDivide' , '%s%sFist%sMdv' %( name , i+1 , side ))
                
                self.armCtrl.attr('fist') >> self.fistMainMdv.attr('i1x')
                self.armCtrl.attr('fist') >> self.fistMainMdv.attr('i1y')
                self.armCtrl.attr('fist') >> self.fistMainMdv.attr('i1z')

                self.fistMainMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[4]')
                self.fistMainMdv.attr('oy') >> self.fngrRyPmaDict[i].attr('i1[2]')
                self.fistMainMdv.attr('oz') >> self.fngrRzPmaDict[i].attr('i1[1]')

                for fistAttr in ('x','y','z'):
                    self.armCtrlShape.addAttr( '%sFistR%s' %(fngrNum,fistAttr))
                    self.armCtrlShape.attr('%sFistR%s' %(fngrNum,fistAttr)) >> self.fistMainMdv.attr('i2%s' %fistAttr)
            
            #-- Relax
            for i in range(self.fngrMember-1):
                fngrNum = '%s%s' %(fngr,i+1)

                self.relaxMainMdv = rt.createNode( 'multiplyDivide' , '%s%sRelax%sMdv' %( name , i+1 , side ))
                
                self.armCtrl.attr('relax') >> self.relaxMainMdv.attr('i1x')
                self.armCtrl.attr('relax') >> self.relaxMainMdv.attr('i1y')
                self.armCtrl.attr('relax') >> self.relaxMainMdv.attr('i1z')

                self.relaxMainMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[5]')
                self.relaxMainMdv.attr('oy') >> self.fngrRyPmaDict[i].attr('i1[3]')
                self.relaxMainMdv.attr('oz') >> self.fngrRzPmaDict[i].attr('i1[2]')

                for relaxAttr in ('x','y','z'):
                    self.armCtrlShape.addAttr( '%sRelaxR%s' %(fngrNum,relaxAttr))
                    self.armCtrlShape.attr('%sRelaxR%s' %(fngrNum,relaxAttr)) >> self.relaxMainMdv.attr('i2%s' %relaxAttr)
            
            #-- Slide
            for i in range(self.fngrMember-1):
                fngrNum = '%s%s' %(fngr,i+1)

                self.slideMainMdv = rt.createNode( 'multiplyDivide' , '%s%sSlide%sMdv' %( name , i+1 , side ))
                
                self.armCtrl.attr('slide') >> self.slideMainMdv.attr('i1x')
                self.armCtrl.attr('slide') >> self.slideMainMdv.attr('i1y')
                self.armCtrl.attr('slide') >> self.slideMainMdv.attr('i1z')

                self.slideMainMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[6]')
                self.slideMainMdv.attr('oy') >> self.fngrRyPmaDict[i].attr('i1[4]')
                self.slideMainMdv.attr('oz') >> self.fngrRzPmaDict[i].attr('i1[3]')

                for slideAttr in ('x','y','z'):
                    self.armCtrlShape.addAttr( '%sSlideR%s' %(fngrNum,slideAttr))
                    self.armCtrlShape.attr('%sSlideR%s' %(fngrNum,slideAttr)) >> self.slideMainMdv.attr('i2%s' %slideAttr)
            
            #-- Scruch
            for i in range(self.fngrMember-1):
                fngrNum = '%s%s' %(fngr,i+1)

                self.scruchMainMdv = rt.createNode( 'multiplyDivide' , '%s%sScruch%sMdv' %( name , i+1 , side ))
                
                self.armCtrl.attr('scruch') >> self.scruchMainMdv.attr('i1x')
                self.armCtrl.attr('scruch') >> self.scruchMainMdv.attr('i1y')
                self.armCtrl.attr('scruch') >> self.scruchMainMdv.attr('i1z')

                self.scruchMainMdv.attr('ox') >> self.fngrRxPmaDict[i].attr('i1[7]')
                self.scruchMainMdv.attr('oy') >> self.fngrRyPmaDict[i].attr('i1[5]')
                self.scruchMainMdv.attr('oz') >> self.fngrRzPmaDict[i].attr('i1[4]')

                for scruchAttr in ('x','y','z'):
                    self.armCtrlShape.addAttr( '%sScruchR%s' %(fngrNum,scruchAttr))
                    self.armCtrlShape.attr('%sScruchR%s' %(fngrNum,scruchAttr)) >> self.scruchMainMdv.attr('i2%s' %scruchAttr)

            #-- Spread
            self.armCtrlShape.addAttr( '%sBaseSpread' %fngr)
            self.armCtrlShape.addAttr( '%sSpread' %fngr)
            self.spreadMainMdv = rt.createNode( 'multiplyDivide' , '%sSpread%sMdv' %( name , side ))
            self.armCtrl.attr('baseSpread') >> self.spreadMainMdv.attr('i1x')
            self.armCtrl.attr('spread') >> self.spreadMainMdv.attr('i1y')
            self.spreadMainMdv.attr('ox') >> self.fngrRzPmaDict[0].attr('i1[5]')
            self.spreadMainMdv.attr('oy') >> self.fngrRzPmaDict[1].attr('i1[5]')
            self.armCtrlShape.attr('%sBaseSpread' %fngr) >> self.spreadMainMdv.attr('i2x')
            self.armCtrlShape.attr('%sSpread' %fngr) >> self.spreadMainMdv.attr('i2y')

            #-- Break
            self.armCtrlShape.addAttr( '%sBaseBreak' %fngr)
            self.armCtrlShape.addAttr( '%sBreak' %fngr)
            self.breakMainMdv = rt.createNode( 'multiplyDivide' , '%sBreak%sMdv' %( name , side ))
            self.armCtrl.attr('baseBreak') >> self.breakMainMdv.attr('i1x')
            self.armCtrl.attr('break') >> self.breakMainMdv.attr('i1y')
            self.breakMainMdv.attr('ox') >> self.fngrRzPmaDict[0].attr('i1[6]')
            self.breakMainMdv.attr('oy') >> self.fngrRzPmaDict[1].attr('i1[6]')
            self.armCtrlShape.attr('%sBaseBreak' %fngr) >> self.breakMainMdv.attr('i2x')
            self.armCtrlShape.attr('%sBreak' %fngr) >> self.breakMainMdv.attr('i2y')

            #-- Flex
            self.armCtrlShape.addAttr( '%sBaseFlex' %fngr)
            self.armCtrlShape.addAttr( '%sFlex' %fngr)
            self.flexMainMdv = rt.createNode( 'multiplyDivide' , '%sFlex%sMdv' %( name , side ))
            self.armCtrl.attr('baseFlex') >> self.flexMainMdv.attr('i1x')
            self.armCtrl.attr('flex') >> self.flexMainMdv.attr('i1y')
            self.flexMainMdv.attr('ox') >> self.fngrRxPmaDict[0].attr('i1[8]')
            self.flexMainMdv.attr('oy') >> self.fngrRxPmaDict[1].attr('i1[8]')
            self.armCtrlShape.attr('%sBaseFlex' %fngr) >> self.flexMainMdv.attr('i2x')
            self.armCtrlShape.attr('%sFlex' %fngr) >> self.flexMainMdv.attr('i2y')
            
        #-- Adjust Hierarchy
        if not mc.objExists( 'Finger%sCtrl%sGrp' %( elem , side )) :
            self.allFngrGrp = rt.createNode( 'transform' , 'Finger%sCtrl%sGrp' %( elem , side ))
            self.allFngrGrp.snap( parent )
            self.allFngrGrp.parent( ctrlGrp )
        else :
            self.allFngrGrp = 'Finger%sCtrl%sGrp' %( elem , side )

        mc.parent( self.fngrMainZro , self.fngrZroDict[0] , self.fngrCtrlGrp )
        mc.parent( self.fngrCtrlGrp , self.allFngrGrp )
        mc.parent( self.fngrJntDict[0] , parent )

        for i in range(self.fngrMember) :
            if not i == 0 :
                mc.parent( self.fngrJntDict[i] , self.fngrJntDict[i-1] )

                if not i == self.fngrMember-1 :
                    mc.parent( self.fngrZroDict[i] , self.fngrGmblDict[i-1] )

        #-- Cleanup
        for obj in self.fngrCtrlDict :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        for jnt in self.fngrJntDict :
            jnt.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for grp in self.fngrZroDict :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for grp in self.fngrDrvDict :
            grp.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        self.fngrMainCtrl.lockHideAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        mc.select( cl = True )