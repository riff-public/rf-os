import maya.cmds as mc
import maya.mel as mm
import core as core
import rigTools as rt
reload( core )
reload( rt )

# -------------------------------------------------------------------------------------------------------------
#
#  CLAVICLE RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , clavTmpJnt  = 'Clav_L_TmpJnt' ,
                         upArmTmpJnt = 'UpArm_L_TmpJnt' ,
                         parent      = 'Spine4Pos_jnt' ,
                         ctrlGrp     = 'Ctrl_Grp' ,
                         jntGrp      = 'Jnt_Grp' ,
                         skinGrp     = 'Skin_Grp' ,
                         elem        = '' ,
                         side        = 'L' ,
                         axis        = 'y' ,
                         size        = 1 
                 ):

        ##-- Info
        elem = elem.capitalize()

        if side == '' :
            side = '_'
        else :
            side = '_%s_' %side

        if 'L' in side  :
            valueSide = 1
            axisNonroll = 'y+'
        elif 'R' in side :
            valueSide = -1
            axisNonroll = 'y-'

        #-- Create Main Group
        self.clavCtrlGrp = rt.createNode( 'transform' , 'Clav%sCtrl%sGrp' %( elem , side ))
        self.clavJntGrp = rt.createNode( 'transform' , 'Clav%sJnt%sGrp' %( elem , side ))
        mc.parentConstraint( parent , self.clavCtrlGrp , mo = False )
        self.clavJntGrp.snap( parent )
        
        #-- Create Joint
        self.clavJnt = rt.createJnt( 'Clav%s%sJnt' %( elem , side ) , clavTmpJnt )
        self.clavEndJnt = rt.createJnt( 'Clav%sEnd%sJnt' %( elem , side ) , upArmTmpJnt )
        self.clavEndJnt.parent( self.clavJnt )
        self.clavJnt.parent( parent )

        #-- Create Controls
        self.clavCtrl = rt.createCtrl( 'Clav%s%sCtrl' %( elem , side ) , 'stick' , 'red' , jnt = True )
        self.clavGmbl = rt.addGimbal( self.clavCtrl )
        self.clavZro = rt.addGrp( self.clavCtrl )
        self.clavZro.snapPoint( self.clavJnt )
        self.clavCtrl.snapJntOrient( self.clavJnt )

        #-- Adjust Shape Controls
        for ctrl in ( self.clavCtrl , self.clavGmbl ) :
            ctrl.scaleShape( size )

            if 'L' in side :
                ctrl.rotateShape( 90 , 0 ,0 )
            else :
                ctrl.rotateShape( -90 , 0 ,0 )
        
        #-- Adjust Rotate Order
        for obj in ( self.clavCtrl , self.clavJnt , self.clavEndJnt ) :
            obj.setRotateOrder( 'xyz' )

        #-- Rig process
        mc.parentConstraint( self.clavGmbl , self.clavJnt , mo = False )
        rt.addFkStretch( self.clavCtrl , self.clavEndJnt , axis , valueSide )
        
        clavNonRollJnt = rt.addNonRollJnt( 'Clav' , self.clavJnt , self.clavEndJnt , parent , axisNonroll )

        self.clavNonRollJntGrp = clavNonRollJnt['jntGrp']
        self.clavNonRollRootNr = clavNonRollJnt['rootNr']
        self.clavNonRollRootNrZro = clavNonRollJnt['rootNrZro']
        self.clavNonRollEndNr = clavNonRollJnt['endNr']
        self.clavNonRollIkhNr = clavNonRollJnt['ikhNr']
        self.clavNonRollIkhNrZro = clavNonRollJnt['ikhNrZro']

        #-- Adjust Hierarchy
        mc.parent( self.clavZro , self.clavCtrlGrp )
        mc.parent( self.clavCtrlGrp , ctrlGrp )
        mc.parent( self.clavNonRollJntGrp , self.clavJntGrp )
        mc.parent( self.clavJntGrp , jntGrp )

        #-- Cleanup
        for obj in ( self.clavCtrlGrp , self.clavJntGrp , self.clavZro ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for obj in ( self.clavCtrl , self.clavGmbl ) :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

    mc.select( cl = True )