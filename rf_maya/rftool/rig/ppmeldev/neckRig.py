import maya.cmds as mc
import maya.mel as mm
import core as core
import rigTools as rt
import ribbonRig
reload( core )
reload( rt )
reload( ribbonRig )

# -------------------------------------------------------------------------------------------------------------
#
#  NECK RIGGING MODULE
#  
# -------------------------------------------------------------------------------------------------------------

class Run( object ):

    def __init__( self , neckTmpJnt = 'Neck_TmpJnt' ,
                         headTmpJnt = 'Head_TmpJnt' ,
                         parent     = 'Spine4Pos_jnt' ,
                         ctrlGrp    = 'Ctrl_Grp' ,
                         jntGrp     = 'Jnt_Grp' ,
                         skinGrp    = 'Skin_Grp' ,
                         stillGrp   = 'Still_Grp' ,
                         elem       = '' ,
                         side       = '' ,
                         ribbon     = True ,
                         head       = True ,
                         axis       = 'y' ,
                         size       = 1
                 ):

        ##-- Info
        elem = elem.capitalize()

        if side == '' :
            sideRbn = ''
            side = '_'
        else :
            sideRbn = '%s' %side
            side = '_%s_' %side

        #-- Create Main Group
        self.neckCtrlGrp = rt.createNode( 'transform' , 'Neck%sCtrl%sGrp' %( elem , side ))
        self.neckJntGrp = rt.createNode( 'transform' , 'Neck%sJnt%sGrp' %( elem , side ))
        mc.parentConstraint( parent , self.neckCtrlGrp , mo = False )
        self.neckJntGrp.snap( parent )
        
        #-- Create Joint
        self.neckJnt = rt.createJnt( 'Neck%s%sJnt' %( elem , side ) , neckTmpJnt )
        self.neckEndJnt = rt.createJnt( 'Neck%sEnd%sJnt' %( elem , side ) , headTmpJnt )
        self.neckEndJnt.parent( self.neckJnt )
        self.neckJnt.parent( parent )

        #-- Create Controls
        self.neckCtrl = rt.createCtrl( 'Neck%s%sCtrl' %( elem , side ) , 'circle' , 'red' )
        self.neckGmbl = rt.addGimbal( self.neckCtrl )
        self.neckZro = rt.addGrp( self.neckCtrl )
        self.neckZro.snap( self.neckJnt )

        #-- Adjust Shape Controls
        for ctrl in ( self.neckCtrl , self.neckGmbl ) :
            ctrl.scaleShape( size )
        
        #-- Adjust Rotate Order
        for obj in ( self.neckCtrl , self.neckJnt , self.neckEndJnt ) :
            obj.setRotateOrder( 'xzy' )

        #-- Rig process
        mc.parentConstraint( self.neckGmbl , self.neckJnt , mo = False )
        rt.addFkStretch( self.neckCtrl , self.neckEndJnt , axis )
        rt.localWorld( self.neckCtrl , ctrlGrp , self.neckCtrlGrp , self.neckZro , 'orient' )

        neckNonRollJnt = rt.addNonRollJnt( 'Neck' , self.neckJnt , self.neckEndJnt , parent , '%s+' %axis )

        self.neckNonRollJntGrp = neckNonRollJnt['jntGrp']
        self.neckNonRollRootNr = neckNonRollJnt['rootNr']
        self.neckNonRollRootNrZro = neckNonRollJnt['rootNrZro']
        self.neckNonRollEndNr = neckNonRollJnt['endNr']
        self.neckNonRollIkhNr = neckNonRollJnt['ikhNr']
        self.neckNonRollIkhNrZro = neckNonRollJnt['ikhNrZro']

        #-- Adjust Hierarchy
        mc.parent( self.neckZro , self.neckCtrlGrp )
        mc.parent( self.neckCtrlGrp , ctrlGrp )
        mc.parent( self.neckNonRollJntGrp , self.neckJntGrp )
        mc.parent( self.neckJntGrp , jntGrp )

        #-- Cleanup
        for obj in ( self.neckCtrlGrp , self.neckZro ) :
            obj.lockAttrs( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' , 'sx' , 'sy' , 'sz' , 'v' )

        for obj in ( self.neckCtrl , self.neckGmbl ) :
            obj.lockHideAttrs( 'sx' , 'sy' , 'sz' , 'v' )

        #-- Create Ribbon
        if head :
            self.headJnt = rt.createJnt( 'Head%s%sJnt' %( elem , side ) , headTmpJnt )
            mc.parent( self.headJnt , self.neckEndJnt )
            posi = self.headJnt
        else :
            posi = self.neckEndJnt

        if ribbon == True :
            rbn = ribbonRig.run(   name = 'Neck%s' %elem , 
                                        posiA = self.neckJnt ,
                                        posiB = posi ,
                                        axis = 'y+' ,
                                        side = sideRbn ,
                                        hi = True )
        else :
            rbn = ribbonRig.run(   name = 'Neck%s' %elem , 
                                        posiA = self.neckJnt ,
                                        posiB = posi ,
                                        axis = 'y+' ,
                                        side = sideRbn ,
                                        hi = False )
        
        #-- Twist Distribute
        self.rbnCtrl = core.Dag(rbn['rbnCtrl'])
        self.rbnCtrlShape = core.Dag(self.rbnCtrl.shape)

        if head :
            self.headJnt.attr('r%s' %axis) >> self.rbnCtrlShape.attr( 'endAbsoluteTwist' )

        self.rbnCtrl.attr('autoTwist').value = 1

        #-- Adjust Hierarchy
        mc.parent( rbn['rbnCtrlGrp'] , self.neckCtrlGrp )
        mc.parent( rbn['rbnJntGrp'] , self.neckJntGrp )
        mc.parent( rbn['rbnSkinGrp'] , skinGrp )
        
        if ribbon == True :
            mc.parent( rbn['rbnStillGrp'] , stillGrp )

        mc.select( cl = True )