import maya.cmds as mc
import maya.mel as mel
from rf_utils import file_utils as fu
import sys

#sys.path.append(r'N:\Staff\Hong\Scripts\Mocap\DarkHorse\newRig')
import MoCapDefinition as mcd
reload(mcd)

####### list mocap fbx file #########
root = 'P:/DarkHorse/Research/Hong/Mocap/mocapData'

fbxList = fu.list_file(root)
print fbxList

####### createNew file and assign HIK Definition and Save as MAYAAscii file ########
for f in fbxList:
    print f
    if '.fbx' in f:
        mc.file(new=1,f=1)
        mc.file('%s/%s'%(root,f),i=1)
        mc.select('*:Hips*')
        mcd.createDefinition()
        mc.file(rename='%s/%s'%(root,f.split('.fbx')[0]))
        #print '%s/%s'%(root,f.split('.fbx')[0])
        mc.file(save=1,type='mayaAscii')