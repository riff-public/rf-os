import maya.cmds as mc
import pymel.core as pmc
import customMocapX as cmx
reload(cmx)


class mocapXAnim():
	def __init__(self):
		self.winName = 'mocapXAnim'
		if mc.ls(sl=True) == []:
			mc.warning('please Select Character.')
		
	def selCtrlList(self,*args):
		if mc.ls(sl=True) != []:
			cmx.selectAllCtrl()
		else:
			mc.confirmDialog(m='please select something from rig')

	def disConHead(self,*args):
		MocapX = cmx.getMocapX()
		cmx.disconnectHead(MocapX,self.headPoseList,self.headCtrl)
		cmx.setZero([self.headCtrl])
	
	def conHead(self,*args):
		MocapX = cmx.getMocapX()
		cmx.connectHead(MocapX,self.headPoseList,self.headCtrl)


	def disConEye(self,*args):
		MocapX = cmx.getMocapX()
		cmx.disconnectEye(MocapX,self.eyePoseRotList,self.eyeRotCtrl)
		cmx.setZero(self.eyeRotCtrl)

	def conEye(self,*args):
		MocapX = cmx.getMocapX()
		cmx.connectEye(MocapX,self.eyePoseRotList,self.eyeRotCtrl)	
	

	def MocapXAnimUI(self):
		ui = mc.window(self.winName,title = 'mocapXAnim')
		mainLayout = mc.columnLayout('mainCol',cal='left',co=['both',28])
		mc.separator(h=10,style ='none')
		mc.text('warning',l='Please select character')
		mc.text('warning2',l='before prese button.')
		mc.separator(h=8,style ='none')
		mc.button('selCtrlList',l='select all control',w=120,h=60,c=self.selCtrlList)
		mc.separator(h=10,style ='none')
		mc.button('conHead',l='connectHead',w=120,h=30,c= self.conHead)
		mc.separator(h=1,style ='none')
		mc.button('disConHead',l='disconnectHead',w=120,h=30,c= self.disConHead)
		mc.separator(h=10,style ='none')
		mc.button('conEye',l='connectEye',w=120,h=30,c= self.conEye)
		mc.separator(h=1,style ='none')
		mc.button('disConEye',l='disconnectEye',w=120,h=30,c= self.disConEye)
		mc.separator(h=14,style ='none')


	def showUI(self):
		self.poseList = cmx.poseList
		self.eyePoseRotList =cmx.eyePoseRotList
		self.headPoseList=cmx.headPoseList
		self.eyeRotOriCtrl=cmx.eyeRotOriCtrl
		self.headOriCtrl=cmx.headOriCtrl
		self.oriList=cmx.oriList
		self.ctrlList,self.eyeRotCtrl,self.headCtrl = cmx.createListFromSel()
		if mc.window(self.winName,exists=1):
			print 'delUI'
			mc.deleteUI(self.winName)
		self.MocapXAnimUI()
		mc.showWindow(self.winName)			
#y = mcxa.mocapXAnim()
#y.showMocapXAnimUI()