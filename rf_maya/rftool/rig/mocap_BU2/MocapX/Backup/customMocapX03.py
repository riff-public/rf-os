import maya.cmds as mc
import maya.mel as mel
import pymel.core as pmc
import sys
import os
sys.path.append('C:/Users/pornpavis.t/Desktop/mocapx_1.1.5_Maya2018_win/scripts/mocapx')
sys.path.append(r'D:\Script\core\rf_maya\rftool\rig\MocapX')
from mocapx import commands as cm
reload(cm)
#import fclCtrlList as fcll
#reload(fcll)
from mocapx.lib import nodes as nd
reload(nd)

poseList = [ 'browDown_L', 'browOuterUp_L', 'cheekSquint_L', 'eyeBlink_L', 'eyeLookDown_L', 'eyeLookIn_L', 'eyeLookOut_L', 'eyeLookUp_L', 'eyeSquint_L', 'eyeWide_L', 'mouthDimple_L', 'mouthFrown_L', 'mouthLowerDown_L', 'mouthPress_L', 'mouthSmile_L', 'mouthStretch_L', 'mouthUpperUp_L', 'noseSneer_L', 'jawLeft', 'mouthLeft', 'browDown_R', 'browOuterUp_R', 'cheekSquint_R', 'eyeBlink_R', 'eyeLookDown_R', 'eyeLookIn_R', 'eyeLookOut_R', 'eyeLookUp_R', 'eyeSquint_R', 'eyeWide_R', 'mouthDimple_R', 'mouthFrown_R', 'mouthLowerDown_R', 'mouthPress_R', 'mouthSmile_R', 'mouthStretch_R', 'mouthUpperUp_R', 'noseSneer_R', 'jawRight', 'mouthRight', 'browInnerUp', 'mouthClose', 'cheekPuff', 'jawOpen', 'mouthRollLower', 'tongueOut', 'mouthPucker', 'mouthShrugUpper', 'mouthFunnel', 'jawForward', 'mouthShrugLower', 'mouthRollUpper']
poseLibName = 'PoseLib'
collectionName = 'AttributeCollection'
eyePoseList=[ 'leftEyeTransformTranslateX','leftEyeTransformTranslateY','leftEyeTransformTranslateZ','rightEyeTransformTranslateX','rightEyeTransformTranslateY','rightEyeTransformTranslateZ']
eyePoseRotList=[ 'leftEyeTransformRotateX','leftEyeTransformRotateY','leftEyeTransformRotateZ','rightEyeTransformRotateX','rightEyeTransformRotateY','rightEyeTransformRotateZ']
headPoseList = ['transformRotateX','transformRotateY','transformRotateZ']

attrList = ['mouthUpperUp_R','mouthPress_L','mouthLowerDown_L','browDown_L','cheekPuff','mouthShrugLower','eyeLookUp_R','jawLeft','eyeBlink_L','eyeLookIn_L','eyeLookOut_R','mouthShrugUpper','mouthFrown_L','jawForward','eyeSquint_R','mouthStretch_L','eyeWide_L','jawRight','jawOpen','cheekSquint_R','noseSneer_R','browOuterUp_L','eyeWide_R','eyeLookDown_R','browOuterUp_R','mouthSmile_R','mouthPress_R','mouthClose','cheekSquint_L','eyeLookDown_L','mouthRight','mouthRollUpper','eyeSquint_L','mouthRollLower','mouthStretch_R','mouthDimple_L','mouthUpperUp_L','mouthPucker','noseSneer_L','browDown_R','browInnerUp','mouthLowerDown_R','eyeLookUp_L','eyeLookIn_R','mouthFunnel','mouthFrown_R','eyeLookOut_L','mouthLeft','mouthDimple_R','eyeBlink_R','mouthSmile_L','tongueOut','lookAtPointX','lookAtPointY','lookAtPointZ','transformTranslateX','transformTranslateY','transformTranslateZ','transformRotateX','transformRotateY','transformRotateZ','leftEyeTransformTranslateX','leftEyeTransformTranslateY','leftEyeTransformTranslateZ','leftEyeTransformRotateX','leftEyeTransformRotateY','leftEyeTransformRotateZ','rightEyeTransformTranslateX','rightEyeTransformTranslateY','rightEyeTransformTranslateZ','rightEyeTransformRotateX','rightEyeTransformRotateY','rightEyeTransformRotateZ']



def createCtrlListAttr(ns,attrName = 'ctrlList',sort=1):
	geoGrp = '{}Geo_Grp'.format(ns)
	ctrlList = mc.ls(sl=True)
	if not mc.objExists(geoGrp+'.{}'.format(attrName)):
		mc.addAttr(geoGrp,ln=attrName,k=0,dt='string')
	ctrlAttrList = [] 
	for i in ctrlList:
		ctrl = i.split(ns)[-1]
		ctrlAttrList.append(ctrl)
	if sort ==1:
		ctrlAttrList.sort()
	else:
		pass
	ctrlStr = ','.join(ctrlAttrList)
	mc.setAttr(geoGrp+'.'+attrName,ctrlStr,type='string')

def getAttrFromCtrlListAttr( ns , attrName = 'ctrlList'):
	geoGrp = '{}Geo_Grp'.format(ns)
	if mc.objExists(geoGrp+'.{}'.format(attrName)):
		ctrlStr = mc.getAttr(geoGrp+'.{}'.format(attrName))
		ctrlStrList = ctrlStr.split(',')
		selList = []
		for i in ctrlStrList:
			ctrl = ns+i
			selList.append(ctrl)
		return selList
	else:
		return None

def createAllCtrlList():
	createCtrlListAttr('ctrlList')

def createLeftCtrlList():
	createCtrlListAttr('leftCtrlList')

def createRightCtrlList():
	createCtrlListAttr('rightCtrlList')

def createMidCtrlList():
	createCtrlListAttr('midCtrlList')

def createEyeCtrlList():
	createCtrlListAttr('eyeCtrlList')

def createHeadCtrlList():
	createCtrlListAttr('headCtrlList')

def getRigDataFld():
	scnPath = os.path.normpath( mc.file( q = True , l = True )[0])
	#print scnPath
	tmpAry = scnPath.split( '\\' )
	#print tmpAry
	tmpAry[3] = 'publ'
	tmpAry[-3] = 'data'
	
	dataFld = '\\'.join( tmpAry[0:-2] )
	
	return dataFld


def createCtrlList(asset):
	ctrlList = []
	eyeRotCtrl = []
	for i in oriList:
		ctrl = i.replace('<Asset>',asset)
		ctrlList.append(ctrl)

	for j in eyeRotOriCtrl:
		ctrl = j.replace('<Asset>',asset)
		eyeRotCtrl.append(ctrl)
	
	headCtrl= headOriCtrl.replace('<Asset>',asset)

	return ctrlList,eyeRotCtrl,headCtrl

def createListFromDir():
	filePathName = getRigDataFld()
	asset = filePathName.split('\\')[5]
	ctrlList,eyeRotCtrl,headCtrl = createCtrlList(asset)
	return ctrlList,eyeRotCtrl,headCtrl

def createListFromSel(ns):
	if mc.ls(sl=True)!= []:
		asset = ns
		ctrlList,eyeRotCtrl,headCtrl = createCtrlList(asset)
		return ctrlList,eyeRotCtrl,headCtrl

def createText(textValue):
	text = mc.textCurves(n=textValue+'_Ctrl',t=textValue)
	mc.delete(text[-1])
	shape = mc.listRelatives(text[0],c=1,ad=1,type ='shape')
	mc.select(shape)
	num=1
	for cv in shape:
		transform = pmc.PyNode(cv.replace('Shape',''))
		transform.rename(textValue+str(num))
		par = transform.getParent().nodeName()
		mc.makeIdentity(par,s=1,t=1,r=1,a=1)
		shape = transform.getShape().nodeName()
		mc.parent(shape,text[0],r=1,s=1)

		num+=1
	text[0] = mc.rename(text[0],text[0].replace('Shape',''))
	transformChild = mc.listRelatives(text[0],c=1,ad=1,type='transform')
	mc.delete(transformChild)
	return text[0]

def createMocapXSystem(poseLibName,collectionName,poseList,clipreader='',ctrlList=[]):
	mc.currentTime(1)
	cm.create_empty_collection(collection_name=collectionName)
	cm.add_controls_to_collection(collectionName, control_list=ctrlList)
	cm.create_empty_poselib(poselib_name = poseLibName)
	for i in poseList:
		cm.create_empty_pose(pose_name=i)
		cm.add_pose_to_poselib(poseLibName, pose_list=poseList, selected_poses=False)
	if clipreader !='':
		for i in poseList:
			mc.connectAttr('{}.{}'.format(clipReader,i),i+'.weight')


def createMocapXDevice():
	default = cm.create_clipreader()
	mc.rename(default,'Default')
	cm.create_realtime_device(adapter_name='MocapX')
	cm.create_clipreader(adapter_name = 'MocapX')
	createPoseValue()
	nd.set_active_source('MocapX','Default')

def connectClipReader(clipReader,PoseLibName,poseList):
	for i in poseList:
		if not mc.connectionInfo('{}.{}'.format(clipReader,i),isSource=1): 
			mc.connectAttr('{}.{}'.format(clipReader,i),i+'.weight')

def disConnectClipReader(clipReader,PoseLibName,poseList):
	for i in poseList:
		if mc.connectionInfo('{}.{}'.format(clipReader,i),isSource=1):
			mc.disconnectAttr('{}.{}'.format(clipReader,i),i+'.weight')

def createFacePoseText():
	grp = mc.createNode('transform',n='Text_Grp')
	mc.currentTime(1)
	text = createText('BaseFcl')
	mc.setKeyframe(text+'.v',v=1,s=0)
	#mc.setKeyframe(ctrlList,s=0)
	mc.currentTime(0)
	mc.setKeyframe(text+'.v',v=0,s=0)
	mc.currentTime(2)
	mc.setKeyframe(text+'.v',v=0,s=0)

	mc.parent(text,grp)
	for i in range(len(poseList)):
		mc.currentTime(i+2)
		text = poseList[i]+'_Ctrl'
		if not mc.objExists(text):
			text = createText(poseList[i])
		mc.setKeyframe(text+'.v',v=1,s=0)
		#mc.setKeyframe(ctrlList,s=0)
		mc.currentTime(i+1)
		mc.setKeyframe(text+'.v',v=0,s=0)
		mc.currentTime(i+3)
		mc.setKeyframe(text+'.v',v=0,s=0)
		mc.parent(text,grp)
	
def resetFacePostFrame(ctrlList,mode=1):
	frameRange = range(55)
	if mode ==1:
		print 'case1'
		newRange = frameRange[22:42]
	elif mode ==0:
		print 'case2'
		newRange = frameRange[2:21]
	elif mode ==2:
		newRange = frameRange[42:]
	elif mode ==3:
		newRange = frameRange
	#mc.currentTime(frameRange[0])
	#for ctrl in ctrlList:
		#for attr in mc.listAttr(k=1,u=1):
			#if ('localWorld' not in attr) and ('scale' not in attr) and ('Scale' not in attr):
				#mc.setAttr('{}.{}'.format(ctrl,attr))
	#mc.setKeyframe(ctrlList,s=0)

	for frame in newRange:
		mc.currentTime(1)
		mc.select(ctrlList)
		mel.eval('timeSliderCopyKey;')
		mc.currentTime(frame)
		mel.eval('timeSliderPasteKey false;')

def getCtrlDict(ctrl1):
	attrList = mc.listAttr(ctrl1,k=1,u=1)
	ctrlAttrDict={}
	for attr in attrList:
		ctrlAttrDict[attr] = mc.getAttr('{}.{}'.format(ctrl1,attr))
	return ctrlAttrDict


def createMirrorDictCtrl(LeftCtrlList,RightCtrlList,midCtrlList,Side='l'):
	ctrlDict = {}
	for idx in range(len(LeftCtrlList)):
		if Side.lower() == 'l':
			opCtrl = RightCtrlList[idx]
			ctrlAttrDict = getCtrlDict(LeftCtrlList[idx])
			ctrlDict[opCtrl] = ctrlAttrDict
		if Side.lower() == 'r':
			opCtrl = LeftCtrlList[idx]
			ctrlAttrDict = getCtrlDict(RigtCtrlList[idx])
			ctrlDict[opCtrl] = ctrlAttrDict
	
	for ctrl in midCtrlList:
		attrList = mc.listAttr(ctrl,k=1,u=1)
		ctrlAttrDict={}
		for attr in attrList:

			if Side.lower() == 'l':
				search = 'Lft'
				replace = 'Rgt'
				search2 = 'Left'
				replace2 =  'Right'
			elif Side.lower() =='r':
				search = 'Rgt'
				replace = 'Lft'
				search2 = 'Right'
				replace2 =  'Left'		
			
			attrData = mc.getAttr('{}.{}'.format(ctrl,attr))

			

			if search in attr:
				attr = attr.replace(search,replace)
				
			elif replace in attr:
				attr =attr.replace(replace,search)
			
			elif search2 in attr:
				attr =attr.replace(search2,replace2)

			elif replace2 in attr:
				attr =attr.replace(replace2,search2)

			if (attr != 'rotateY') and (attr != 'translateX'):
				ctrlAttrDict[attr] = attrData
			else:
				ctrlAttrDict[attr] = -(attrData)

			#print '{} : {}'.format(attr,ctrlAttrDict[attr])
		ctrlDict[ctrl] = ctrlAttrDict			
	return ctrlDict



def mirFrameCtrl(frame,LeftCtrlList,RightCtrlList,midCtrlList,offSetFrame = 20,Side='l'):
	mirFrame = int(frame+offSetFrame)
	mc.currentTime(frame)
	ctrlDict = createMirrorDictCtrl(LeftCtrlList,RightCtrlList,midCtrlList,Side)
	mc.currentTime(frame+offSetFrame)
	if ctrlDict != None:
		for ctrl in ctrlDict.keys():
			for attr in ctrlDict[ctrl].keys():
				value = ctrlDict[ctrl][attr]
				mc.setAttr('{}.{}'.format(ctrl,attr),value)


def mirAllFrame(ns,startFrame,ctrlList,rTol=0):
	#LeftCtrlList = mc.getAttr().split(',')
	#RightCtrlList = mc.getAttr().split(',')
	LeftCtrlList = getAttrFromCtrlListAttr(ns,'leftCtrlList')
	RightCtrlList = getAttrFromCtrlListAttr(ns,'rightCtrlList')
	midCtrlList = getAttrFromCtrlListAttr(ns,'midCtrlList')
	if len(LeftCtrlList) == len(RightCtrlList):
		if rTol==0:
			offsetFrame = 20
			side = 'l'
		else:
			offsetFrame = -20
			side = 'r'
		for i in range(20):
			mirFrameCtrl(i+startFrame,LeftCtrlList,RightCtrlList,midCtrlList,offsetFrame,side)
	else:
		mc.confirmDialog(m='controller on left and right not equal.')
		return None


def multipleUpdatePose(ctrlList,poseList):
	for i in range(len(poseList)):
		print i
		mc.currentTime(i+2)
		#print i
		if mc.getAttr(poseList[i]+'_Ctrl.v') == 1:
			#print 'run'
			cm.update_controls_in_pose(poseList[i],ctrlList)


def connectEye(device,eyePoseList,eyeCtrl):
	for i in eyePoseList:
		if 'left' in i:
			ctrl = eyeCtrl[0]
			attr= i.split('Transform')[-1].lower()
			conAttr = attr[:-1]+attr[-1].upper()
			mc.connectAttr('{}.{}'.format(device,i),'{}.{}'.format(ctrl,conAttr))
		elif 'right' in i:
			ctrl = eyeCtrl[1]
			attr= i.split('Transform')[-1].lower()
			conAttr = attr[:-1]+attr[-1].upper()
			mc.connectAttr('{}.{}'.format(device,i),'{}.{}'.format(ctrl,conAttr))

def connectHead(device,headPoseList,headCtrl):
	for i in headPoseList:
		for ctrl in headCtrl:
			attr = i.split('transform')[-1].lower()
			conAttr = attr[:-1]+attr[-1].upper()
			mc.connectAttr('{}.{}'.format(device,i),'{}.{}'.format(ctrl,conAttr))


def connectForPublish():
	filePathName = getRigDataFld()
	asset = filePathName.split('\\')[5]

	mc.select('*:Geo_Grp')
	ns = pmc.selected()[0].namespace()
	print ns
	geoGrp = ns+'Geo_Grp'
	mc.select(d=1)
	
	if not mc.objExists(geoGrp+'.MocapX'):
		mc.addAttr(geoGrp,ln='MocapX',k=1)
	
	if not mc.objExists(geoGrp+'.AttrCollection'):
		mc.addAttr(geoGrp,ln='AttrCollection',k=1)
	
	if not mc.objExists(geoGrp+'.PoseLib'):
		mc.addAttr(geoGrp,ln='PoseLib',k=1)
	
	if not mc.objExists('MocapX.PoseLib'):
		mc.addAttr('MocapX',ln='connect',k=1)
	
	if not mc.objExists('AttributeCollection.connect'):	
		mc.addAttr('AttributeCollection',ln='connect',k=1)
	
	#if not mc.objExists('PoseLib.connect'):
		#mc.addAttr('PoseLib',ln='connect')
	
	mc.connectAttr('AttributeCollection.connect',geoGrp+'.AttrCollection')
	mc.connectAttr('MocapX.connect',geoGrp+'.MocapX')
	#mc.connectAttr('PoseLib.connect',geoGrp+'.PoseLib')

def createPoseValue():
	if mc.objExists('Default'):
		for i in attrList:
			mc.addAttr('Default',ln=i,h=1)


def connectAll(ns,device):
	ctrlList = getAttrFromCtrlListAttr(ns,'ctrlList')
	headCtrl =  getAttrFromCtrlListAttr(ns,'headCtrlList')
	eyeRotCtrl = getAttrFromCtrlListAttr(ns,'eyeCtrlList')
	connectClipReader(device,poseLibName,poseList)
	connectEye(device,eyePoseRotList,eyeRotCtrl)
	connectHead(device,headPoseList,headCtrl)


def disconnectHead(device,PoseList,headCtrl):
	for i in PoseList:
		for ctrl in headCtrl:
			attr = i.split('transform')[-1].lower()
			conAttr = attr[:-1]+attr[-1].upper()
			pmc.disconnectAttr('{}.{}'.format(ctrl,conAttr))	

def disconnectEye(device,eyePoseList,eyeCtrl):
	for i in eyePoseList:
		print i
		print eyeCtrl
		if 'left' in i:
			ctrl = eyeCtrl[0]
			attr= i.split('Transform')[-1].lower()
			conAttr = attr[:-1]+attr[-1].upper()

			pmc.disconnectAttr('{}.{}'.format(ctrl,conAttr))
		
		elif 'right' in i:
			ctrl = eyeCtrl[1]
			attr= i.split('Transform')[-1].lower()
			conAttr = attr[:-1]+attr[-1].upper()
			print ctrl
			print attr
			#if mc.isConnected('{}.{}'.format(device,i),,iuc=1):
			pmc.disconnectAttr('{}.{}'.format(ctrl,conAttr))


def selectAllCtrl(ns):
	selList = getAttrFromCtrlListAttr(ns,'ctrlList')
	mc.select(selList)




def resetDevice(device):
	for i in attrList:
		mc.setAttr(device+'.'+i,0)


def deleteKey(ctrlList):
	mc.currentTime(1)
	mc.cutKey(ctrlList,cl=1)

def createFclKey(ctrlList):
	for i in range(55)[1:]:
		mc.currentTime(i)
		mc.setKeyframe(ctrlList,s=0)

def importMocapXTmp():
	thisModulePath =  os.path.dirname(sys.modules[__name__].__file__)
	mc.file(thisModulePath+'\\mocapXTmp.ma',i=1)


def modCtrlList(ns,attr='ctrlList',mode = 'add'):
	addCtrlList = mc.ls(sl=True)
	#ns = pmc.selected()[0].namespace()
	geoGrp = '{}Geo_Grp'.format(ns)
	if mc.objExists(geoGrp+'.{}'.format(attr)):
		existsCtrlStr = mc.getAttr(geoGrp+'.{}'.format(attr))
		existsCtrlList =  existsCtrlStr.split(',')
		addCtrlAttrList = []
		for i in addCtrlList:
			ctrl= i.split(ns)[-1]
			addCtrlAttrList.append(ctrl)
		if mode == 'add':
			newCtrlList = list( set(existsCtrlList)|set(addCtrlAttrList) )
		elif mode =='remove':
			newCtrlList = list( set(existsCtrlList) - set(addCtrlAttrList) )
		ctrlStr = ','.join(newCtrlList)
		mc.setAttr(geoGrp+'.{}'.format(attr),ctrlStr,type='string')
	else:
		mc.confirmDialog(m='ctrlList Attribute not found.')

def setZero(ctrlList):
	for i in ctrlList:
		listAttr = mc.listAttr(i,k=1,v=1)
		print listAttr
		for attr in listAttr:
			if ('scale' in attr) or ('scale' in attr):
				mc.setAttr('{}.{}'.format(i,attr),1)
			else:
				mc.setAttr('{}.{}'.format(i,attr),0)

def getMocapX():
	ns = pmc.selected()[0].namespace()
	return '{}MocapX'.format(ns)

#def mirrorTransform(obj,target,axis='x'):
#resetDevice('RealtimeDevice')
#resetDevice('ClipReader')
#createFacePoseText()
#disConnectClipReader('ClipReader',poseLibName,poseList)
#connectClipReader('RealtimeDevice',poseLibName,poseList)
#mirAllFrame(2,ctrlList)
#createFacePoseText(ctrlList)
#connectForPublish()
#connectAll('MocapX')
#disConnectClipReader('MocapX',poseLibName,poseList)
#connectEye(eyePoseList,eyeCtrl)


#ctrlList,eyeRotCtrl,headCtrl = createListFromDir()

##### facial pose Part ##############
#ctrlList,eyeRotCtrl,headCtrl = createListFromDir()
#createCtrlListAttr()
#createFclKey(ctrlList)
#importMocapXTmp()
#selectAllCtrl()
#mirAllFrame(2,ctrlList)

##### MocapX part ##################

#ctrlList,eyeRotCtrl,headCtrl = createListFromDir()
#createCtrlListAttr()
#createMocapXSystem(poseLibName,collectionName,poseList,'',ctrlList)
#multipleUpdatePose(ctrlList,poseList)
#deleteKey(ctrlList)
#cm.get_adapter_node(create_node=True)
#cm.create_realtime_device('MocapX')
#cm.create_clipreader('MocapX')
#createPoseValue()
#cm.set_active_source('MocapX','RealtimeDevice')
#connectAll('MocapX')
#connectClipReader('MocapX',poseLibName,poseList)
#connectEye('MocapX',eyePoseRotList,eyeRotCtrl)
#connectHead('MocapX',headPoseList,headCtrl)
'''
######## anim Part #################
selectAllCtrl()
disconnectAddition('MocapX',headPoseList,headCtrl)
disconnectAddition('MocapX',eyePoseRotList,eyeRotCtrl)
'''


