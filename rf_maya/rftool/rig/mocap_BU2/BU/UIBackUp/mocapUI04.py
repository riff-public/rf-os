import maya.cmds as mc
import maya.mel as mel
import sys
sys.path.append(r'N:\Staff\Hong\Scripts\Mocap\DarkHorse')
import HIKDefinition as HIKD
reload(HIKD)
mocapUI = 'mocapUI'

def createChar(*args):
    NS= mc.textField('nsTextField',q=1,text=1)
    charName = mc.textField('CharacterName',q=1,text=1)
    HIKD.crateChar(charName,NS)
    HIKD.createCustomRigMap(NS)

def moHIKUI():
    if mc.window(mocapUI,exists=1):
        mc.deleteUI(mocapUI)
    win = mc.window(mocapUI,title = 'mocap Tool')
    mc.showWindow(win)
    
    layout1 = mc.columnLayout('column1',rs=5,co=['both',12])
    mc.separator(h=5,st='none')
    mc.text('Head',label = 'Mocap HIK UI V.01',h=20)
    
    mc.separator(h=2,st='singleDash',w=300)
    mc.text('text1',label = '1.Please select reference Character Rig to get namespace.')
    nsTextField = mc.textField('nsTextField',w=150,text='')
    def getNSText(*args):
        NS = HIKD.getNS()
        mc.textField('nsTextField',e=1,text=NS)   
    nsButton= mc.button('nameSpace',label = 'Get NameSpace',w=150,c=getNSText)      
    mc.separator(h=5,w=300,st='singleDash')
    mc.text('text2',label = '2. Please fill name for Creating HIK Character Node.' )
    charName = mc.textField('CharacterName',w=150,text='Character1')

    #createChar = "NS=HIKD.getNS()\ncharName = mc.textField('CharacterName',q=1,text=1)\nHIKD.crateChar(charName)"
    print createChar  
    charButton= mc.button('CharacterCreate',label = 'Create HIK Character',w=150,c=createChar)
    
    
    mc.separator(h=5,w=300,st='singleDash')
    mc.text('text4',label = '3. Reference MotionCapture Data File.')
    moRef = mc.button('MoRef',label = 'Reference',w=150,c='mel.eval("CreateReference;")')
    mc.separator(h=5,w=300,st='singleDash')

    mc.text('text5',label = '4. Select Source from MAYA HIK UI to apply motion to Rig.')
    mc.separator(h=6,w=300,st='singleDash')
    
    mc.text('text6',label = '5. Bake Animation to Controller from MAYA HIK UI.')
    mc.separator(h=6,w=300,st='singleDash')
    
    mc.text('text7',label = '6. Delete Character Node')
    charDel = mc.button('charDel',label = 'Delete',w=150,c="HIKD.delUnknownHIKNode(mc.textField('CharacterName',q=1,text=1))")    
    mc.separator(h=2,w=300,st='singleDash')
    
    mc.text('text8',label = '7. Create Anim Layer for Mocap')
    charAnim = mc.button('charAnim',label='Create',w=150,c="HIKD.moveAnimLayer(HIKD.ctlList,mc.textField('nsTextField',q=1,text=1))")
    mc.separator(h=5,w=300,st='none')
moHIKUI()

