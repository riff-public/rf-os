import maya.cmds as mc
import maya.mel as mel
import os
import sys
from rf_utils import file_utils as fu
#sys.path.append(r'N:\Staff\Hong\Scripts\Mocap\DarkHorse')

#Mocap HIK UI V012
import HIKDefinition as HIKD
reload(HIKD)

import MoCapDefinition as MCD
reload(MCD)

import wholeMocapDef as wmd
reload(wmd)

import matchAllFrame as maf
reload(maf)

import BakeCharFromMocap as bcm
reload(bcm)

modulePath = sys.modules[__name__].__file__
filePath = '/'.join(modulePath.split('\\')[:-1])

if not mc.pluginInfo('mayaHIK',loaded=1,q=1):
	mc.loadPlugin('mayaHIK')

mocapUI = 'mocapUI'
def createChar(*args):
	'''
	Function for Create Character
	'''
	mel.eval("HIKCharacterControlsTool")
	NS= mc.textField('nsTextField',q=1,text=1)
	charName = mc.textField('CharacterName',q=1,text=1)
	HIKD.crateChar(NS,charName)
	HIKD.createCustomRigMap(NS)

def setTPose():
	MCD.setTPose()


def createMocapDefinition(*args):
	MCD.createDefinition()

def changeDirectory(*args):
	browseDir = mc.fileDialog2(ds=1,cap= 'Select directory of Mocap Data Folder.',fileMode=3)[0]
	mc.textField('Directory',e=1,text=browseDir)

def changeWholeDirectory(*args):
	Directory = mc.fileDialog2(ds=1,cap= 'Select directory of Mocap Data Folder.',fileMode=3)[0]
	mc.textField('wholeMocapDestination',e=1,text=Directory)

def createWholeMocapFromFolder(*args):
	Directory = mc.textField('Directory',q=1,text=1)
	wholeMocapDestination = mc.textField('wholeMocapDestination',q=1,text=1)
	wmd.folderMocapDef(Directory,wholeMocapDestination)

def getDestinationForMultipleMocap(*args):
	Directory = mc.fileDialog2(ds=1,cap= 'Select Mocap Data File.',fileMode=3)[0]
	mc.textField('multipleTextFBX',e=1,text= Directory)


def createMultipleMocap(*args):
	Directory = mc.fileDialog2(ds=1,cap= 'Select Mocap Data File.',fileMode=4)
	destinationFolder = mc.textField('multipleTextFBX',q=1,text=1)
	wmd.multipleFolderMocapDef(Directory,destinationFolder)

def referenceMocap(*args):
	mocFile = mc.fileDialog2(ds=1,cap= 'Select Mocap Data File.',fileMode=4)

	for files in mocFile:
		#print files
		namespaces = files.split('/')[-1].split('.')[0]
		mc.file(files,ns=namespaces,r=1)
	return mocFile


def DelChar(*args):
	HIKD.delUnknownHIKNode(mc.textField('CharacterName',q=1,text=1))


def BakeAnim(*args):
	NS = mc.textField('nsTextField',q=1,text=1)
	if mc.objExists('%s:SkinJnt_Grp'%(NS)):
		HIKD.moveAnimLayer(HIKD.HIKCharOldList.AnimCtrlList,mc.textField('nsTextField',q=1,text=1))
	else:
		HIKD.moveAnimLayer(HIKD.HIKCharList.AnimCtrlList,mc.textField('nsTextField',q=1,text=1))     


def matchFKIK(*args):
	NS = mc.textField('nsTextField',q=1,text=1)
	
	if mc.checkBox('LeftLeg',q=1,v=1):
		maf.matchAllFrame(NS,'Leg','L')
	
	if mc.checkBox('RightLeg',q=1,v=1):
		maf.matchAllFrame(NS,'Leg','R')
	
	if mc.checkBox('LeftArm',q=1,v=1):
		maf.matchAllFrame(NS,'Arm','L')
	
	if mc.checkBox('RightArm',q=1,v=1):
		maf.matchAllFrame(NS,'Arm', 'R')

	if mc.checkBox('All',q=1,v=1):
		maf.matchAllAllFrame(NS)

def charBakeDirectory(*args):
	Directory = mc.fileDialog2(ds=1,cap= 'Select Action Data File.',fileMode=3)[0]
	mc.textField('charBakeDF',e=1,text= Directory)

def charSelectBake(*args):
	assetName = mc.textField('assetName',q=1,text=1)
	filePathList = mc.fileDialog2(ds=1,cap= 'Select Action Data File.',fileMode=4)
	Directory = mc.textField('charBakeDF',q=1,text= 1)
	bcm.mulBake(assetName,filePathList,Directory)

def charFolderBake(*args):
	assetName = mc.textField('assetName',q=1,text=1)
	sourceFolder = mc.fileDialog2(ds=1,cap= 'Select Action Data File.',fileMode=3)[0]
	Directory = mc.textField('charBakeDF',q=1,text= 1)
	filePathList = ['{}/{}'.format(sourceFolder,FileName) for FileName in fu.list_file(sourceFolder)]
	print filePathList
	bcm.mulBake(assetName,filePathList,Directory)
####
def helpCommand(*args):
	helpPath = '%s/Explain'%(filePath)
	os.startfile(helpPath)
	#print helpPath
	#subprocess.Popen(helpPath)

############### Motion Capture Main UI #########################
def moHIKUI():
	if mc.window(mocapUI,exists=1):
		mc.deleteUI(mocapUI)
	win = mc.window(mocapUI,title = 'mocap Tool')
	mc.showWindow(win)

	main = mc.columnLayout('main',rs=5,co=['both',12])
	mc.separator(h=5,st='none')
	headLine = mc.rowLayout('main',numberOfColumns=3)
	mc.text('Head',label = 'Mocap HIK UI V.09',h=20)
	mc.separator(w=5)
	mc.button('Help',label = 'Help', c =helpCommand)
	mc.setParent(main)
	tabLayout1 = mc.tabLayout('layout1')
   
	layout1 = mc.columnLayout('HIKtoRig',rs=5,co=['both',12])
	mc.separator(h=2,st='singleDash',w=300)
	mc.text('text1',label = '1.Please select reference Character Rig to get namespace.')
	nsTextField = mc.textField('nsTextField',w=150,text='')
	TPoseCheck = mc.checkBox('TposeCheck',label = 'Auto TPose Set',v=1)    
	
	def getNSText(*args):
		NS = HIKD.getNS()
		mc.textField('nsTextField',e=1,text=NS)
		CharName = NS.split('_')[0]
		mc.textField('CharacterName',e=1,text=CharName)       
		if mc.checkBox('TposeCheck',q=1,v=1):
			HIKD.TPoseSet(NS)
	
	nsButton= mc.button('nameSpace',label = 'Get NameSpace',w=150,c=getNSText)      
	mc.separator(h=5,w=300,st='singleDash')
	
	mc.text('text2',label = '2. Please fill name for Creating HIK Character Node.' )
	charName = mc.textField('CharacterName',w=150,text='Character1')
	charButton= mc.button('CharacterCreate',label = 'Create HIK Character',w=150,c=createChar)
	mc.separator(h=5,w=300,st='singleDash')
	
	mc.text('text4',label = '3. Reference MotionCapture Data File.')
	moRef = mc.button('MoRef',label = 'Reference',w=150,c=referenceMocap)
	mc.separator(h=5,w=300,st='singleDash')

	mc.text('text5',label = '4. Select Source from MAYA HIK UI to apply motion to Rig.')
	#HIKSourcePath = filePath+'/IMG/04-HIKSource.png'
	#mc.image('HIKSourceIMG',i = HIKSourcePath)
	mc.separator(h=6,w=300,st='singleDash')
	
	mc.text('text6',label = '5. Bake Animation to Controller from MAYA HIK UI.')
	#HIKBakePath = filePath+'/IMG/05-Bake.png'
	#mc.image('HIKBakeIMG',i = HIKBakePath)
	mc.separator(h=6,w=300,st='singleDash')
	
	mc.text('text7',label = '6. Delete Character Node')
	charDel = mc.button('charDel',label = 'Delete',w=150,c=DelChar)    
	mc.separator(h=2,w=300,st='singleDash')
	
	mc.text('text10',label = '7. Bake FK to IK from selected option')
	mc.checkBox('LeftLeg',label = 'Left Leg',v=0)
	mc.checkBox('RightLeg',label = 'Right Leg',v=0)
	mc.checkBox('LeftArm',label = 'Left Arm',v=0)
	mc.checkBox('RightArm',label = 'Right Arm',v=0)
	mc.checkBox('All',label = 'Both Arm/Leg',v=1)
	charAnim = mc.button('BakeFKIK',label='BakeFK/IK',w=150,c=matchFKIK)
	mc.separator(h=5,w=300,st='none',p=main)
	mc.separator(h=2,w=300,st='singleDash')

	mc.text('text8',label = '8. Create Anim Layer for Mocap')
	charAnim = mc.button('charAnim',label='Create',w=150,c=BakeAnim)
	mc.separator(h=5,w=300,st='none')
	mc.separator(h=5,w=300,st='none',p=main)

	

#####################   Lay out 2  Mocap to HUMAN IK  ###################################################################
	layout2 = mc.columnLayout('MocapToHIK',rs=5,co=['both',12],p = tabLayout1)

#####################   Single Creating Motion capture definition. ###################################################################    
	frameLayout3 = mc.frameLayout('frameLayout3',label = 'Single Creating Mocap Definition',cl=0,cll=1,w=300,p=layout2)
	sinMocap = mc.columnLayout('singleMocapMocap',w=300,p= frameLayout3)
	mc.separator(h=10,w=300,st='none')
	mc.text('text9',label = '1.Please select Mocap Jnt Chain Data.')     
	mc.separator(h=10,w=300,st='none')
	mc.text('test10',label = '2. Please fill name for Creating HIK Character Node.' )
	mc.separator(h=10,w=300,st='none')
	charName2 = mc.textField('CharacterName2',w=150,text='Character1')
	mc.separator(h=5,w=300,st='none')
	moCapButton2= mc.button('moCapCreate2',label = 'Create HIK Definition',w=150,c=createMocapDefinition)
	mc.separator(h=8,w=300,st='none')
	mc.separator(h=10,w=300,st='none')
	mc.text('save',label = '3. Save this file to Motion Capture Library.' )
	mc.separator(h=20,w=310,st='singleDash',p=layout2)

#####################   Multiple Creating Motion capture definition. ################################################################### 
	frameLayout2 = mc.frameLayout('frameLayout2',label = 'Multiple Creating Mocap Definition',cl=1,cll=1,w=300,p=layout2)
	multMocap = mc.columnLayout('multipleMocap',w=300,p= frameLayout2)
	mc.text('test12',label = '1.Select Destination Folder.')
	multipleTextFBX = mc.textField('multipleTextFBX',w=250,text = 'P:/Library/GeneralLibrary/mocap/DarkHorse')
	mc.separator(h=5,w=300,st='none')
	multipleFbx= mc.button('multipleFbx',label = 'Destination Folder',w=150,c=getDestinationForMultipleMocap)
	mc.separator(h=10,w=300,st='none')
	mc.text('test13',label = '2.Select Multiple FBX.')
	mc.separator(h=5,w=300,st='none')
	runMultipleFBX= mc.button('runMultipleFBX',label = 'Create Mocap File',w=150,c=createMultipleMocap)
	mc.separator(h=5,w=300,st='none')


#####################   Whole Folder Creating Motion capture definition. ################################################################### 
	mc.separator(h=20,w=310,st='singleDash',p=layout2)
	frameLayout1 = mc.frameLayout('frameLayout1',label = 'Whole Folder Creating Mocap Definition',cl=1,cll=1,w=300,p=layout2)
	layout3 = mc.columnLayout('loopMocap',w=300,p=frameLayout1)
	mc.text('test11',label = '1. Please fill directory of Mocap Folder.' )
	mc.separator(h=3,w=300,st='none')
	rowLayout1 = mc.rowLayout('textFieldLayout',w=300,numberOfColumns =2)
	Directory = mc.textField('Directory',w=225,text='P:/Library/GeneralLibrary/mocap/DarkHorse/fbx')
	browse = mc.button('browser',w=75,label = 'Browse',c=changeDirectory)
	mc.setParent(layout3)

	mc.separator(h=3,w=300,st='none')
	mc.separator(h=5,w=300,st='none') 
	mc.separator(h=5,w=300,st='none')  
	mc.text('test14',label = '2. Select Destination Folder.' )
	
	rowLayout2 = mc.rowLayout('textFieldLayout2',w=300,numberOfColumns =2)
	wholeMocapDestination = mc.textField('wholeMocapDestination',w=225,text='P:/Library/GeneralLibrary/mocap/DarkHorse/maya')
	saveWholeMOcap = mc.button('browser',w=75,label = 'Browse',c=changeWholeDirectory)
	mc.setParent(layout3)
	mc.separator(h=3,w=300,st='none')    
	loopMocapButton= mc.button('loopMocapButton',label = 'create mocap file',w=150,c=createWholeMocapFromFolder)
	mc.separator(h=5,w=300,st='none')
	#mc.separator(h=5,w=310,st='singleDash',p=frameLayout1)

#################### Bake Character UI #############################
	mc.setParent(tabLayout1)
	layout3 = mc.columnLayout('BakeCharacter',rs=5,co=['both',12])
	mc.separator(h=2,st='singleDash',w=300)
	mc.text('textAsset',label = '1. Character Asset Name')
	assetName = mc.textField('assetName',w=150,text='')

	mc.separator(h=2,st='singleDash',w=300)
	mc.text('textDest',label = '2. Destination Folder')
	charBakeDestinationFolder = mc.textField('charBakeDF',w=300,text='')
	charBakeDFButton = mc.button('CBbrowser',w=75,label = 'Browse',c=charBakeDirectory)
	
	mc.separator(h=2,st='singleDash',w=300)
	mc.text('multipleSelect',label = '3a.Bake Action by multiple Select File')
	charBakeMSButton = mc.button('CBMulButton',w=150, label = 'multiple select action',c=charSelectBake)

	mc.separator(h=2,st='singleDash',w=300)
	mc.text('CBSourceFolder',label = '3b.Bake Action by Select Source Folder')
	charBakeSourceFolder = mc.textField('charBakeSF',w=300,text='')
	charBakeSFButton = mc.button('CBSFButton',w=150, label = 'Select Source Foler',c=charFolderBake)
	mc.separator(h=2,st='singleDash',w=300)


moHIKUI()




