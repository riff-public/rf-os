import math
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mm
import os
import json
import pickle

import ioData as iod
reload(iod)

import poseData as posd
reload(posd)

import ctrlData as ctrld
reload(ctrld)

import hikList as hikl
reload(hikl)


def get_data_folder( createFld = True ):
    scnPath = os.path.normpath( mc.file( q = True , l = True )[0])

    tmpAry = scnPath.split( '\\' )
    tmpAry[3] = 'publ'
    tmpAry[-3] = 'data'
    
    dataFld = '\\'.join( tmpAry[0:-2] )
    
    if createFld :
        if not os.path.isdir( dataFld ) :
            os.mkdir( dataFld )
    
    return dataFld


# def get_data_folder( createFld = True ):
#     fld_path = iod.get_data_folder('rigData')
    
#     if createFld :
#         if not os.path.isdir( fld_path ) :
#             os.mkdir( fld_path )
    
#     return fld_path


def get_mocap_folder():
	data_folder = get_data_folder() 
	mocap_folder = os.path.join(data_folder,'mocap')
	
	if not os.path.isdir(mocap_folder):
		os.mkdir(mocap_folder)
	return mocap_folder


def get_mocap_folder_from_asset(obj_name = ''):
	data_folder = get_data_folder(obj_name) 
	mocap_folder = os.path.join(data_folder,'mocap')
	if not os.path.isdir(mocap_folder):
		os.mkdir(mocap_folder)
	return mocap_folder	

fld_path = get_mocap_folder()
tPose_file_name = 'tPose'
tPose_path = os.path.join(fld_path, tPose_file_name)

def write_tpose( obj_list = None, file_name= tPose_path , fld_path = fld_path):
	if obj_list ==None:
		obj_list = mc.ls('*:*_Ctrl' , '*_Ctrl')
	posd.write_pose(obj_list,file_name,fld_path)

def read_tpose(file_path = tPose_path, ns = None , search_for = '', replace_as = ''):
	posd.read_pose(file_path, ns, search_for, replace_as)


hik_dict = hikl.part_dict.copy()
#def edit_hik_char():

def get_asset_path(object_name = ''):
    scnPath = pmc.PyNode(object_name).referenceFile().path
    tmpAry = scnPath.split( '/' )
    tmpAry[3] = 'publ'
    tmpAry[-2] = 'rig'
    tmpAry[-1] = 'data'
    tmpAry.append('mocap')
    #print tmpAry
    dataFld = '\\'.join(tmpAry)
    if not os.path.isdir( dataFld ) :
    	os.mkdir( dataFld )
    return dataFld

