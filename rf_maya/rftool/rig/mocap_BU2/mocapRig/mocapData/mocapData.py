import math
import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mm
import os
import json
import pickle
import maya.OpenMaya as om

import ioData as iod
reload(iod)

import poseData as posd
reload(posd)

import ctrlData as ctrld
reload(ctrld)

import hikList as hikl
reload(hikl)

# hik function 
def get_project_dict():
    with open(os.path.join(os.path.dirname(__file__),'project_dict')) as f:
        proj_dict = json.load(f)
    return proj_dict

def get_project_from_path(file_path):
    proj_dict = get_project_dict()
    split_path = file_path.split('/')
    project_name = split_path[1]
    asset_name = split_path[proj_dict[project_name]]
    return project_name,asset_name

def get_project_from_asset(obj_name):
    proj_path = pmc.PyNode(obj_name).referenceFile().path
    return get_project_from_path(proj_path)

def get_project_from_scene():
    proj_path = pmc.sceneName()
    return get_project_from_path(proj_path)   

def get_mocap_folder(project , asset ):
    file_path = r'P:\Library\Mocap\%s\asset\%s\data'%(project, asset)
    return file_path

def create_mocap_folder(project , asset):
    file_path == r'P:\Library\Mocap\%s\asset\%s\data'%(project, asset)
    return file_path

def write_tpose( project , asset, file_name = 'tPose' ,obj_list = None ):
    if obj_list ==None:
		obj_list = mc.ls('*:*_Ctrl' , '*_Ctrl')
    fld_path = r'P:\Library\Mocap\%s\asset\%s\data'%(project, asset)
    posd.write_pose(obj_list,file_name,fld_path)

def read_tpose(project , asset ,  ns = None , search_for = '', replace_as = ''):
    file_path = r'P:\Library\Mocap\%s\asset\%s\data\tPose'%(project, asset)
    print file_path
    posd.read_pose(file_path, ns, search_for, replace_as)


def get_polevector(start, mid, end, offsetPoleVec=2):
    x = lambda vec, loc : mc.move(vec.x, vec.y, vec.z, loc)
    pos = mc.xform(start, q=1, ws=1, t=True)
    a = om.MVector(pos[0], pos[1], pos[2])
    pos2 = mc.xform(end, q=1, ws=1, t=True)
    b= om.MVector(pos2[0], pos2[1], pos2[2])
    pos3 = mc.xform(mid, q=1, ws=1, t=True)
    c = om.MVector(pos3[0], pos3[1], pos3[2])
    i = b-a
    d= i* .5
    e = a+d
    f = c-e
    g = f 
    h = e+g
    return (h.x, h.y, h.z)

def ik_to_fk():
    for num in range(len(skn_jnt)):
        mat = mc.xform(skn_jnt[num], q=1, ws=1, m=1)
        mc.xform(fk_ctrl[num] , ws=1, m=mat)
    mc.setAttr(sw_ctrl+'.fkIk',0)
    
def fk_to_ik():
    # get tip
    tip_mat = mc.xform(skn_jnt[-1],ws=1,m=1,q=1)
    mc.xform(ik_ctrl[-1],ws=1,m=tip_mat)
    
    # get base
    base_mat = mc.xform(skn_jnt[0],ws=1,m=1,q=1)
    mc.xform(ik_ctrl[0],ws=1,m=base_mat)

    # ge pole vector
    t = get_polevector(skn_jnt[0], skn_jnt[1], skn_jnt[2], 0)
    mc.xform(ik_ctrl[1],ws=1,t=t)
    mc.setAttr(sw_ctrl+'.fkIk',1)

def create_fkik_snap_dict(swicth_ctrl = '', attr = 'fkIk', fk_list = [],ik_list = [],skin_list = [], fk_at = 0):
    snap_dict = {}

    snap_dict["ctrl"] = swicth_ctrl
    snap_dict["attr"] = attr
    snap_dict["fk_list"] = fk_list
    snap_dict["ik_list"] = ik_list
    snap_dict["skin_list"] = skin_list
    snap_dict["fk_at"] = fk_at
    return snap_dict

def create_char_fkik_dict():
    pass
