import maya.cmds as mc
import maya.mel as mel
from rf_utils import file_utils as fu
import sys
import os
import fbxDef as fbxd
reload(fbxd)

def publish_fbx_def(file_name ='' , location = ''):
	# check output path
	if not location:
		mc.confirmDialog(title = 'folderMocapDef Error', m='no output directory assigned.',b='OK')
		return 
	
	# publish file 
	if file_name:
		fbx_file_name = file_name.split('/')[-1]
		mc.file(new=1,f=1)
		mc.file('%s'%file_name ,i=1)
		fbxd.create_def_in_scene()
		mc.file(rename = '%s/%s'%( location ,fbx_file_name.split('.fbx')[0]))
		mc.file(save=1,type='mayaBinary')

	else:
		mc.confirmDialog(title = 'folderMocapDef Error', m='Please type directory of mocap data folder.',b='OK')

def mul_publish_fbx_def(file_list=[],location = ''):
	# check output path
	if not location:
		mc.confirmDialog(title = 'folderMocapDef Error', m='no output directory assigned.',b='OK')
		return 

	# publish each file in list
	if file_list:
		for f in file_list:
			if '.fbx' in f:
				publish_fbx_def(f, location)
	else:
		mc.confirmDialog(title = 'folderMocapDef Error', m='Please type directory of mocap data folder.',b='OK')