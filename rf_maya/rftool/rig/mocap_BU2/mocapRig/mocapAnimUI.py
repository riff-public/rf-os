import pymel.core as pmc
import maya.cmds as mc
import maya.mel as mel
import os
import json
import pickle

from mocapData import ioData as iod
reload(iod)

from mocapData import poseData as posd
reload(posd)

from mocapData import ctrlData as ctrld
reload(ctrld)

from mocapData import mocapData
reload(mocapData)

from mocapData import hikList as hikl
reload(hikl)

import hikDef as hikd
reload(hikd)

import fbxDef
reload(fbxDef)

# import mulFbxDef as mulFbx
# reload(mulFbx)

if not mc.pluginInfo('mayaHIK',loaded=1,q=1):
	mc.loadPlugin('mayaHIK')

class mocapAnim(object):
	def __init__(self):
		self.winName = 'mocapAnim'
		#self.hik_list = char_list.char_list[:]
		self.ns = ''
		self.char_name = ''
		self.sel = ''

	# ui function 
	def adjColumn(self,*args):
		tabIdx = mc.tabLayout('tmpSetupTab',q=1,sti=1)
		if tabIdx ==1:
			mc.tabLayout('tmpSetupTab',e=1, w=380,h=237)
			mc.columnLayout('mainCol',e=1, w=350,h=263)
			mc.window(self.winName,e=1, w=380,h=263)
			mc.frameLayout('dest_layout',e=1,cl=1)
		elif tabIdx ==2:
			mc.tabLayout('tmpSetupTab',e=1, w=380,h=237)
			mc.columnLayout('mainCol',e=1, w=350,h=263)
			mc.window(self.winName,e=1, w=380,h=263)
			mc.frameLayout('mocap_manual',e=1,cl=1)

	def collapse_frame(self,frame_layout):
		frame_height =  mc.frameLayout(frame_layout,q=1,h=1)
		all_height = mc.tabLayout('tmpSetupTab',q=1,h=1)
		col_height = mc.columnLayout('mainCol',q=1,h=1)
		win_height = mc.window(self.winName,q=1,h=1)

		mc.tabLayout('tmpSetupTab', e=1, h= all_height - frame_height)
		mc.columnLayout('mainCol', e=1, h=col_height - frame_height)
		mc.window(self.winName, e=1, h=win_height - frame_height)

	def expand_frame(self,h):
		all_height = mc.tabLayout('tmpSetupTab',q=1,h=1)
		col_height = mc.columnLayout('mainCol',q=1,h=1)
		win_height = mc.window(self.winName,q=1,h=1)

		mc.tabLayout('tmpSetupTab', e=1, h= all_height + h)
		mc.columnLayout('mainCol', e=1, h=col_height + h)
		mc.window(self.winName, e=1, h=win_height + h)


	def collapse_fbx_frame(self,*args):
		self.collapse_frame('dest_layout')

	def collapse_hik_frame(self,*args):
		self.collapse_frame('mocap_manual')

	def expand_fbx_frame(self,*args):
		self.expand_frame(181)

	def expand_hik_frame(self,*args):
		self.expand_frame(294)
	
	# create hik function
	def get_ns(self,*args):
		self.ns = pmc.selected()[0].namespace()
		self.sel = pmc.selected()[0]
		mc.textField('ns',e=1,tx=self.ns,bgc=[.5,1,.5])
		self.char_list_name = 'char_list'
		self.tPose_file_name = 'tPose'
		self.proj, self.asset = mocapData.get_project_from_asset(pmc.selected()[0].nodeName())
		self.fld_path = mocapData.get_mocap_folder(self.proj, self.asset)
		self.tPose_path = os.path.join(self.fld_path, self.tPose_file_name)
		self.char_path = os.path.join(self.fld_path, self.char_list_name)
		self.load_char_list()
		
		if self.ns:
			self.char_name = self.ns
			self.char_name = self.ns.replace(':','')+'_mocap'
		else:
			self.char_name = self.asset +'_mocap'
		


	def load_char_list(self,*args):
		if os.path.exists(self.char_path):
			with open(self.char_path, 'r') as f:
				self.char_list = json.load(f)
		# else:
		# 	with open(os.path.join(os.path.dirname(__file__),'mocapData','char_list'),'r') as f:
		# 		self.char_list = json.load(f)
	
	def create_hik_character(self,*args):
		pmc.select(self.sel)
		hikd.tPose_set(1)
		pmc.select(self.sel)
		hikd.create_char(self.ns,self.char_name,self.char_list)
		hikd.create_hik_custom_rig(self.ns, self.char_name,self.char_list)
		

	def delete_hik(self,*args):
		hikd.delete_HIK_unknown_node(self.char_name)
	
	def get_fbx_ref(self,*args):
		mocFile = mc.fileDialog2(ds=1,cap= 'Select Mocap Data File.',fileMode=4)
		for files in mocFile:
			#print files
			namespaces = files.split('/')[-1].split('.')[0]
			for sign in ['-','+','*','!','=','/','^','?',' ']:
				if sign in namespaces:
					print 'rename namespaces'
					namespaces.replace(sign,'_')
			mc.file(files,ns=namespaces,r=1)

		return mocFile

	def create_ctrl_list(self,*args):
		self.ctrl_list = []
		for part in self.char_list:
			ctrl = '%s%s'%(self.ns,part[3])
			if pmc.objExists(ctrl):
				print ctrl
				if pmc.PyNode(ctrl).t.isConnected():
					self.ctrl_list.append(ctrl+'.translate')
				if pmc.PyNode(ctrl).r.isConnected():
					self.ctrl_list.append(ctrl+'.rotate')

	def get_time(self,*args):
		self.start = mc.playbackOptions(min=1,q=1)
		self.end = mc.playbackOptions(max=1,q=1)
		self.time = "%s:%s"%(self.start,self.end)

	def set_time(self,start,end):
		mc.playbackOptions(min = start,max=end)

	def get_time_from_ref(self, ref_ns = ''):
		mc.select(mc.ls('%s:*'%ref_ns))
		kf = mc.keyframe(q=1,tc=1)
		return [min(kf),max(kf)]


	def bake_char(self,*args):
		mc.select(cl=1)
		self.create_ctrl_list()
		mel.eval("hikBakeCharacter 0; hikSetCurrentSourceFromCharacter(hikGetCurrentCharacter()); hikUpdateSourceList; hikUpdateContextualUI;")
	
	# create fbx animation function 
	def create_def_in_scene(self,*args):
		fbxDef.create_def_in_scene()

	def browse_destination(self,*args):
		file_list = mc.fileDialog2(ds=1,cap= 'Select Action Data File.',fileMode=3)
		if file_list:
			destination_file = mc.textField('destination_file',e=1,text= file_list[0])

	def mul_publish_fbx(self,*args):
		file_list = mc.fileDialog2(ds=1,cap= 'Select Action Data File.',fileMode=4)
		destination_file = mc.textField('destination_file',q=1,text= 1)
		fbxDef.mul_publish_fbx_def(file_list,destination_file)

	def create_full_hik(self,*args):
		#create hik node
		source_file = mc.fileDialog2(ds=1,cap= 'Select Action Data File.',fileMode=1)
		if source_file:
			self.create_hik_from_path(source_file[0])
		else:
			mc.warning('cannot reference file')
			return		
	
	def create_hik_from_path(self,source_file):
		# reference mocap animation
		self.get_time()
		if '\\' in source_file:
			file_name = source_file.split('\\')[-1].split('.')[0]
		elif '/' in source_file:
			file_name = source_file.split('/')[-1].split('.')[0]

		ref_ns = str(file_name)
		
		for letter in [' ','-','+','*']:
			if letter in ref_ns:
				ref_ns = ref_ns.replace(letter,'_')
		
		mc.file(source_file, ns=ref_ns, r=1)	
		source_char = '%s:%s'%(ref_ns,'Character1')
		animKey = self.get_time_from_ref(ref_ns)
		self.set_time(animKey[0],animKey[1])
		self.create_hik_character()
		# set current human ik source
		hikd.set_hik_source(self.char_name, source_char)
		self.bake_char()
		self.delete_hik()
		self.set_time(self.start,self.end)
		mc.file(source_file,rr=1)
		
	# UI Layout Part
	def show_ui(self):
		if mc.window(self.winName,exists=1):
			mc.deleteUI(self.winName)
		
		ui = mc.window(self.winName,title = 'mocapRig',w=380,h=263)
		# main layout
		mainLayout = mc.columnLayout('mainCol',cal='center',co=['both',15],w=350,adj=1,h=263)
		mc.setParent(mainLayout)
		mc.separator(h=14, style ='none')
		tab_layout = mc.tabLayout('tmpSetupTab', p=mainLayout, cr=1, w=320 ,h=237, sc=self.adjColumn)
		mc.setParent(tab_layout)
		mocap_column = mc.columnLayout('mocap', cal='center', co=['both',15], w=280, adj=1)
		
		# mocapTab
		nsColumn = mc.columnLayout('nsColumn',cal='center',adj=1)
		mc.separator(h=10, style ='none')
		mc.text('This tab is for importing motion capture data to character rig.')
		mc.separator(h=7, style ='none')

		mocap_sub_column = mc.columnLayout('mocap_column', cal='center', w=280, adj=1)
		mc.separator(h=10,style ='none')
		mc.text('Select specific character for transfer data')
		ns_text_field =mc.textField('ns',w=200,ed=0,bgc=[1,.5,.5],ip = 0)
		mc.separator(h=5,style ='none')
		mc.button('getNS',l='get namespace' , c= self.get_ns)
		mc.separator(h=6,style ='none')
		mc.nodeIconButton('create_hik',l='create character',w=120,h=60 ,  st =  "iconAndTextVertical" ,i='poseEditor.png', c= self.create_full_hik)
		mc.separator(h=15, style='none')

		mocap_manual_layout = mc.frameLayout('mocap_manual',l = 'manual create hik / import anim', w=270,cl=1,cll=1,pcc= self.collapse_hik_frame ,pec = self.expand_hik_frame)
		mocap_manual_column = mc.columnLayout('mocap_manual_column', cal='center', w=280, adj=1,)
		mc.separator(h=6, style='none')
		mc.nodeIconButton('create_manual_hik',l='create character',w=120,h=60 ,  st =  "iconAndTextVertical" ,i='poseEditor.png', c= self.create_hik_character)
		mc.separator(h=6, style='none')
		mc.nodeIconButton('import_jnt',l='reference animation',w=120,h=60 ,  st =  "iconAndTextVertical" ,i='addSkinInfluence.png', c = self.get_fbx_ref)
		mc.separator(h=6, style='none')
		mc.nodeIconButton('bake_hik',l='bake anim',w=120,h=60 ,  st =  "iconAndTextVertical" ,i='animateSnapshot.png', c= self.bake_char)
		mc.separator(h=6, style='none')
		mc.nodeIconButton('delete_hik',l='delete_hik',w=120,h=60 ,  st =  "iconAndTextVertical" ,i='deleteActive.png', c= self.delete_hik)
		mc.separator(h=6, style='none')
		mc.setParent(mocap_sub_column)
		mc.separator(h=6, style='none')
		
		# fbx tab
		mc.setParent(tab_layout)
		fbx_column = mc.columnLayout('fbx', cal='center', co=['both',15], w=280, adj=1)
		fbx_sub_column = mc.columnLayout('fbx_column', cal='center', w=280, adj=1)
		mc.separator(h=10,style ='none')
		mc.text('This tab is for preparing fbx files with humanIK \n for transfering animation to charcter rig.')
		mc.separator(style ='none', w = 10,h = 10)
		mc.text('1. Open fbx file \n2. Click button below \n3. Save fbx with humanIK data.')
		mc.separator(style = 'none' , h=10)
		mc.nodeIconButton(' create_fbx',l='create \nfbx / hik', w=120, h=80 , st =  "iconAndTextVertical" ,i='addSkinInfluence.png')
		mc.separator(h=6, style='none')	

		dest_layout = mc.frameLayout('dest_layout',l = 'multiple create fbx / hik', w=270,cl=1,cll=1,pcc= self.collapse_fbx_frame, pec = self.expand_fbx_frame)
		dest_column = mc.columnLayout('dest_column',w=270 ,adj=1,cal = 'center',p= dest_layout)
		mc.separator(h=6, style='none')
		mc.text('please fill destination folder')
		mc.separator(h=3, style='none')
		mc.textField('destination_file')
		mc.separator(h=3, style='none')
		mc.button('browse_destination',l = 'Browse',w = 80 ,c=self.browse_destination)
		mc.separator(h=3, style='none')
		mc.nodeIconButton('folder_fbx',l='create fbx / hik\n     to folder',w=120,h=80 ,  st =  "iconAndTextVertical" ,i='openScript.png', c= self.mul_publish_fbx)
		mc.separator(h=6, style='none')
		mc.setParent(fbx_sub_column)
		mc.separator(h=6, style='none')

		mc.setParent(mainLayout)
		mc.separator(h=10,style ='none')
		mc.showWindow(self.winName)

# import mocapRigUI
# reload(mocapRigUI)

# mocapRigUI.mocapRig().show_ui()