import maya.cmds as mc
import sys
import os
sys.path.append(r'D:\Scripts\core\rf_maya\rftool\rig\mocap')

import HIKDefinition as HIKD
reload(HIKD)

import HIKCharList as HIKCharList
reload(HIKCharList)

import HIKCharOldList as HIKCharOldList
reload(HIKCharList)

#modulePath = sys.modules[__name__].__file__
#filePath = '/'.join(modulePath.split('\\')[:-1])
filePath = 'D:\\Scripts\\core\\rf_maya\\rftool\\rig\\mocap'


charJntList =  ['Ctrl_Grp',"Root_Jnt","UpLeg_L_Jnt","LowLeg_L_Jnt","Ankle_L_Jnt","UpLeg_R_Jnt","LowLeg_R_Jnt","Ankle_R_Jnt","Spine1Pos_Jnt",
				"UpArm_L_Jnt","Forearm_L_Jnt","Wrist_L_Jnt","UpArm_R_Jnt",
				"Forearm_R_Jnt","Wrist_R_Jnt","Head_Jnt","Clav_L_Jnt","Clav_R_Jnt","Neck_Jnt","NeckEnd_Jnt","HeadEnd_Jnt",
				"Spine2Pos_Jnt","Spine3Pos_Jnt","Spine4Pos_Jnt","Toe_L_Jnt","Toe_R_Jnt","Ball_L_Jnt","Ball_R_Jnt","Hand_L_Jnt","Hand_R_Jnt", 			
				"Thumb1_L_Jnt","Thumb2_L_Jnt","Thumb3_L_Jnt","Thumb4_L_Jnt",
				"Index1_L_Jnt","Index2_L_Jnt","Index3_L_Jnt","Index4_L_Jnt","Index5_L_Jnt",
				"Middle1_L_Jnt","Middle2_L_Jnt","Middle3_L_Jnt","Middle4_L_Jnt","Middle5_L_Jnt",
				"Ring1_L_Jnt","Ring2_L_Jnt","Ring3_L_Jnt","Ring4_L_Jnt","Ring5_L_Jnt",
				"Pinky1_L_Jnt","Pinky2_L_Jnt","Pinky3_L_Jnt","Pinky4_L_Jnt","Pinky5_L_Jnt",
				"Thumb1_R_Jnt","Thumb2_R_Jnt","Thumb3_R_Jnt","Thumb4_R_Jnt",
				"Index1_R_Jnt","Index2_R_Jnt","Index3_R_Jnt","Index4_R_Jnt","Index5_R_Jnt",
				"Middle1_R_Jnt","Middle2_R_Jnt","Middle3_R_Jnt","Middle4_R_Jnt","Middle5_R_Jnt",
				"Ring1_R_Jnt","Ring2_R_Jnt","Ring3_R_Jnt","Ring4_R_Jnt","Ring5_R_Jnt",
				"Pinky1_R_Jnt","Pinky2_R_Jnt","Pinky3_R_Jnt","Pinky4_R_Jnt","Pinky5_R_Jnt",]


partJntList = ['Root',"Hips","LeftUpLeg","LeftLeg","LeftFoot","RightUpLeg","RightLeg","RightFoot","Spine","LeftArm",
			"LeftForeArm","LeftHand","RightArm","RightForeArm","RightHand","Head","LeftShoulder",
			"RightShoulder","Neck","Neck1","HeadEnd","Spine1","Spine2","Spine3","LeftToeEnd","RightToeEnd","LeftToeBase",
			"RightToeBase","LeftFingersBase","RightFingersBase",
			"LeftHandThumb1","LeftHandThumb2","LeftHandThumb3","LeftHandThumb4",
			"LeftHandIndex1","LeftHandIndex2","LeftHandIndex3","LeftHandIndex4","LeftHandIndex5",
			"LeftHandMiddle1","LeftHandMiddle2","LeftHandMiddle3","LeftHandMiddle4","LeftHandMiddle5",
			"LeftHandRing1","LeftHandRing2","LeftHandRing3","LeftHandRing4","LeftHandRing5",
			"LeftHandPinky1","LeftHandPinky2","LeftHandPinky3","LeftHandPinky4","LeftHandPinky5",
			
			"RightHandThumb1","RightHandThumb2","RightHandThumb3","RightHandThumb4",
			"RightHandIndex1","RightHandIndex2","RightHandIndex3","RightHandIndex4","RightHandIndex5",
			"RightHandMiddle1","RightHandMiddle2","RightHandMiddle3","RightHandMiddle4","RightHandMiddle5",
			"RightHandRing1","RightHandRing2","RightHandRing3","RightHandRing4","RightHandRing5",
			"RightHandPinky1","RightHandPinky2","RightHandPinky3","RightHandPinky4","RightHandPinky5"]

	
def dupCharJntChain(NS=''):
	HIKD.TPoseSet(NS)
	verCheck = mc.objExists('{}:SkinJnt_Grp'.format(NS))
	#if not verCheck:
		#charJntList = HIKCharList.charJntList
		#partJntList = HIKCharList.partJntList
	#else:
		#charJntList = HIKCharOldList.charJntList
		#partJntList = HIKCharOldList.partJntList
	for i in range(len(charJntList)):
		hikJnt = mc.createNode('joint',n=partJntList[i])
		mc.delete(mc.pointConstraint('%s:%s'%(NS,charJntList[i]),hikJnt))
		mc.makeIdentity(a=1,t=1,r=1,s=1)
	
	for j in range(len(charJntList))[2:]:
		par = mc.listRelatives('%s:%s'%(NS,charJntList[j]),p=1)[0]
		if par != None:
			jnt = par.split(':')[-1]
			if jnt in charJntList:
				ind = charJntList.index(jnt)
				mc.parent(partJntList[j],partJntList[ind])
	mc.parent('RightHandRing1','RightFingersBase')
	mc.parent('RightHandPinky1','RightFingersBase')
	mc.parent('LeftHandRing1','LeftFingersBase')
	mc.parent('LeftHandPinky1','LeftFingersBase')
	mc.parent('Neck','Spine3')
	mc.parent('LeftShoulder','Spine3')
	mc.parent('RightShoulder','Spine3')
	mc.parent('LeftUpLeg','Hips')
	mc.parent('RightUpLeg','Hips')
	mc.parent('LeftArm','LeftShoulder')
	mc.parent('RightArm','RightShoulder')
	mc.parent('Hips','Root')
	
def dupCharMesh(NS=''):
    prGrp = 'PrGeo_Grp'
    dupGrp = mc.duplicate(prGrp,rc=1)[0]
    mocapGrp = mc.rename(dupGrp,'MocapGeo_Grp')
    mc.parent(mocapGrp,w=1)
    parDict = {}
    conList = mc.listRelatives(prGrp,type='parentConstraint',ad=1)
    mc.delete(mc.listRelatives(mocapGrp,type='constraint',ad=1))
    newDict = {}
    for i in conList:
        geo = mc.listRelatives(i,p=1)[0]
        jnt = mc.parentConstraint(i,q=1,tl=1)[0]
        parDict[geo] = jnt
        
    for geo in parDict.keys():
        jnt = parDict[geo]
        if 'Rbn' in jnt:
            if 'UpArm' in jnt:
                if '_L_' in jnt:
                    newjnt = 'UpArm_L_Jnt'
                if '_R_' in jnt:
                    newjnt = 'UpArm_R_Jnt'
            elif 'Forearm' in jnt:
                if '_L_' in jnt:
                    newjnt = 'Forearm_L_Jnt'
                if '_R_' in jnt:
                    newjnt = 'Forearm_R_Jnt'      
            
            if 'UpLeg' in jnt:
                if '_L_' in jnt:
                    newjnt = 'UpLeg_L_Jnt'
                if '_R_' in jnt:
                    newjnt = 'UpLeg_R_Jnt'
                    
            elif 'LowLeg' in jnt:
                if '_L_' in jnt:
                    newjnt = 'LowLeg_L_Jnt'
                if '_R_' in jnt:
                    newjnt = 'LowLeg_R_Jnt'     
            
            elif 'Neck' in jnt:
                newjnt = 'Neck_Jnt'
        
        elif 'Cup' in jnt:
            if '_L_' in jnt:
                newjnt = 'Hand_L_Jnt'
            if '_R_' in jnt:
                newjnt = 'Hand_R_Jnt'
        
        elif 'Pelvis' in jnt:
            newjnt = 'Hips_Jnt'
                        
        elif 'Spine' in jnt:
            nsjnt = jnt.replace('_Jnt','Pos_Jnt')
            newjnt = nsjnt.split(':')[-1]
            print 'Spine Case    ' + newjnt
        
        else:
            newjnt = jnt.split(':')[-1]
        
        
        if newjnt in charJntList:
            ind = charJntList.index(newjnt)
            indJnt = partJntList[ind]
            print 'newJnt is ' + newjnt
            print 'indJnt is '+ indJnt
            print ''
        else:
            indJnt = ''
        
        if indJnt in partJntList: 
            newDict[geo] = indJnt
        
        
            
    mocapGeoGrp = mc.listRelatives(mocapGrp,ad=1,c=1,type= 'transform')
    for keys in newDict.keys():
        for j in mocapGeoGrp:
            if keys in j:
                #print 'key is    '+keys
                #print 'jnt is  '+ newDict[keys]
                #print ''
                if newDict[keys] != '':
                    mc.skinCluster(newDict[keys],j,dr=4,toSelectedBones = 1)
        #if newjnt in partJntList:  
            #mc.skinCluster(newjnt,geo+'1',dr=4,toSelectedBones = 1)


           



dupCharJntChain('ctrl')
dupCharMesh(NS='morkonLeader_md')
