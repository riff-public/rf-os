import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel
import maya.OpenMaya as om
import math


def getShapePoints(shapePath, space=om.MSpace.kObject):
    path = getDagPath(shapePath)
    itGeo = om.MItGeometry(path)
    points = om.MPointArray()
    itGeo.allPositions(points, space)
    return points



def setShapePoints(shapePath, points, space=om.MSpace.kObject):
    path = getDagPath(shapePath)
    itGeo = om.MItGeometry(path)
    itGeo.setAllPositions(points, space)



def getOrigShape(obj):
    shps = obj.getShapes()
    if not shps:
        return
    origShps = []
    for shp in shps:
        intStatus = shp.isIntermediate()
        if 'Orig' in shp.nodeName() or intStatus == True:
            if shp.worldMesh.outputs():
                return shp


def getDagPath(obj):
    try:
        msl = om.MSelectionList()
        msl.add(obj)
        nodeDagPath = om.MDagPath()
        msl.getDagPath(0, nodeDagPath)
    except:
        return None

    return nodeDagPath



def setMatrixRow(matrix, newVector, row):
    setMatrixCell(matrix, newVector.x, row, 0)
    setMatrixCell(matrix, newVector.y, row, 1)
    setMatrixCell(matrix, newVector.z, row, 2)



def setMatrixCell(matrix, value, row, column):
    om.MScriptUtil.setDoubleArray(matrix[row], column, value)



def invertDeformation(base=None, corrective=None, invert=None, name=''):
    if not base or not corrective:
        sels = pm.selected(type='transform')
        try:
            base = sels[0]
            corrective = sels[1]
        except:
            om.MGlobal.displayError('Select base mesh follow with corrective mesh.')
            return False
    
    if base.numVertices() != corrective.numVertices():
        om.MGlobal.displayError('Base mesh and corrective mesh have different vertex count.')
        return False

    # get full path and shape
    baseStr = str(base.longName())
    baseShp = base.getShape(ni=True)
    baseShpStr = baseShp.longName()

    # Get points on base mesh
    basePoints = getShapePoints(baseShpStr)
    numPoints = basePoints.length()

    # get orig shape for base
    origShp = getOrigShape(base)
    if not origShp:
        om.MGlobal.displayError('No intermediate shape found for %s.' % baseStr)
        return
    origShpStr = str(origShp.longName())

    # create invert mesh
    if not name:
        name = '%s_inverted' % corrective.nodeName().split(':')[-1]

    if not invert:
        invertName = mc.duplicate(baseShpStr, name=name)[0]
        invert = pm.PyNode(invertName)
    
    invert.rename(name)

    # delete the unnessary shapes
    shapes = invert.getShapes()
    for shp in shapes:
        if shp.isIntermediate() == True:
            pm.delete(shp)
        else:
            shp.visibility.set(True)

    invertShp = invert.getShape(ni=True)
    invertShpStr = invertShp.longName()


    # corrective shape
    corrStr = str(corrective.longName())
    corrShp = corrective.getShape(ni=True)
    corrShpStr = str(corrShp.longName())


    # Get the component offset on all axes for orig shape
    origPoints = getShapePoints(origShpStr)
    xPoints = om.MPointArray(origPoints)
    yPoints = om.MPointArray(origPoints)
    zPoints = om.MPointArray(origPoints)
    
    for i in range(numPoints):
        xPoints[i].x += 1.0
        yPoints[i].y += 1.0
        zPoints[i].z += 1.0

    setShapePoints(origShpStr, xPoints)
    xPoints = getShapePoints(baseShpStr)

    setShapePoints(origShpStr, yPoints)
    yPoints = getShapePoints(baseShpStr)

    setShapePoints(origShpStr, zPoints)
    zPoints = getShapePoints(baseShpStr)

    setShapePoints(origShpStr, origPoints)
    setShapePoints(invertShpStr, origPoints)

    # Get points on corrective mesh
    correctivePoints = getShapePoints(corrShpStr)

    for i in range(numPoints):
        delta = correctivePoints[i] - basePoints[i]

        if (math.fabs(delta.x) < 0.0001 and math.fabs(delta.y) < 0.0001 and math.fabs(delta.z) < 0.0001):
            continue

        matrix = om.MMatrix()
        setMatrixRow(matrix, xPoints[i] - basePoints[i], 0)
        setMatrixRow(matrix, yPoints[i] - basePoints[i], 1)
        setMatrixRow(matrix, zPoints[i] - basePoints[i], 2)
        matrix = matrix.inverse()

        offset = delta * matrix
        pm.move(invertShp.vtx[i], [offset.x, offset.y, offset.z], r=True)
    
    return invert


def invert(base='', corrective='', name=''):
    
    if not base or not corrective:
        sels = mc.ls(sl=True, l=True, type='transform')
        try:
            base = sels[0]
            corrective = sels[1]
        except:
            om.MGlobal.displayError('Select base mesh follow with corrective mesh.')
            return False

    shapes = mc.listRelatives(base, s=True, f=True)

    origShp, baseShp = '', ''
    for shp in shapes:
        intStatus = mc.getAttr('%s.intermediateObject' %shp)
        if intStatus == True or 'Orig' in shp:
            # if mc.listConnections('%s.worldMesh' %shp, d=True):
            origShp = shp
        else:
            baseShp = shp


    baseShpDag = getDagPath(baseShp)
    origShpDag = getDagPath(origShp)

    # create invert mesh
    invert = mc.duplicate(base)[0]
    if not name:
        name = '%s_inverted' % base.split('|')[-1]
    invert = mc.rename(invert, name)

    # delete the unnessary shapes on duplicated invert mesh
    shapes = mc.listRelatives(invert, s=True, f=True)
    for shp in shapes:
        if mc.getAttr('%s.intermediateObject' %shp) == True:
            mc.delete(shp)
        else:
            mc.setAttr('%s.visibility'%shp, True)

    # corrective shape
    corrShp = mc.listRelatives(corrective, s=True, f=True, ni=True)[0]
    corrShpDag = getDagPath(corrShp)

    # invert shape
    invertShp = mc.listRelatives(invert, s=True, f=True, ni=True)[0]
    invertShpDag = getDagPath(invertShp)

    baseItGeo = om.MItGeometry(baseShpDag)
    origItGeo = om.MItGeometry(origShpDag)
    corrItGeo = om.MItGeometry(corrShpDag)
    invItGeo = om.MItGeometry(invertShpDag)

    
    # get base points
    basePoints = om.MPointArray()
    baseItGeo.allPositions(basePoints, om.MSpace.kObject)

    # get vtx number
    numPoints = basePoints.length()

    # get orig mesh points
    origPoints = om.MPointArray()
    origItGeo.allPositions(origPoints, om.MSpace.kObject)

    # get the component offset on all axes for orig shape
    xPoints = om.MPointArray(origPoints)
    yPoints = om.MPointArray(origPoints)
    zPoints = om.MPointArray(origPoints)

    for i in range(numPoints):
        xPoints[i].x += 1.0
        yPoints[i].y += 1.0
        zPoints[i].z += 1.0

    origItGeo.setAllPositions(xPoints, om.MSpace.kObject)
    baseItGeo.allPositions(xPoints, om.MSpace.kObject)
    
    origItGeo.setAllPositions(yPoints, om.MSpace.kObject)
    baseItGeo.allPositions(yPoints, om.MSpace.kObject)
    
    origItGeo.setAllPositions(zPoints, om.MSpace.kObject)
    baseItGeo.allPositions(zPoints, om.MSpace.kObject)

    origItGeo.setAllPositions(origPoints, om.MSpace.kObject)
    invItGeo.setAllPositions(origPoints, om.MSpace.kObject)

    # Get points on corrective mesh
    correctivePoints = om.MPointArray()
    corrItGeo.allPositions(correctivePoints, om.MSpace.kObject)

    

    # main loop
    invPoints = om.MPointArray()
    while not invItGeo.isDone():
        i = invItGeo.index()
        delta = correctivePoints[i] - basePoints[i]
        currPos = invItGeo.position()

        if (math.fabs(delta.x) < 0.001 and math.fabs(delta.y) < 0.001 and math.fabs(delta.z) < 0.001):
            invPoints.append(currPos)
            invItGeo.next()
            continue

        matrix = om.MMatrix()
        setMatrixRow(matrix, xPoints[i] - basePoints[i], 0)
        setMatrixRow(matrix, yPoints[i] - basePoints[i], 1)
        setMatrixRow(matrix, zPoints[i] - basePoints[i], 2)
        matrix = matrix.inverse()

        offset = delta * matrix
        pt = currPos + offset
        invPoints.append(pt)
        invItGeo.next()

    invItGeo.setAllPositions(invPoints, om.MSpace.kObject)

    invertLongName = mc.listRelatives(invertShp, f=True, p=True)[0]
    return invertLongName



