import sys, struct
import maya.OpenMaya as om
import maya.OpenMayaMPx as ompx

nodeName = 'mddRead'
typeId = om.MTypeId(0x25050001)

class MddRead(ompx.MPxDeformerNode):

	# class var
	time = om.MObject()
	offset = om.MObject()
	cycle = om.MObject()
	mddFile = om.MObject()

	def __init__(self):
		ompx.MPxDeformerNode.__init__(self)
		self.mddFileVal = ''
		self.frames = 0
		self.points = 0
		self.times = None
		self.deltas = None
		self.fileHandle = None


	def loadData(self, fileName, frame, offset, cycle):
		# make sure that we only read the header once only
		if not self.fileHandle and fileName != self.mddFileVal:
			self.deltas = {}
			self.mddFileVal = fileName
			self.fileHandle = open(fileName, 'rb')
			self.frames, self.points = struct.unpack('>2i', self.fileHandle.read(8))
			self.times = struct.unpack(('>%df' %self.frames), self.fileHandle.read(self.frames*4))

		if self.fileHandle and not self.fileHandle.closed:
			headerOffset = 8 + (self.frames * 4)  # handy var to offset header

			if cycle:
				frame = (frame + offset) %self.frames
			else:
				frame = frame + offset
				if frame > self.frames:
					frame = self.frames
				elif frame < 1:
					frame = 1

			# move the cursor position to the right location
			self.fileHandle.seek(headerOffset + (frame * 12 * self.points))
			pointList = []
			for pp in xrange(self.points):
				pval = struct.unpack('>3f', self.fileHandle.read(12))
				mpoint = om.MPoint(pval[0], pval[1], pval[2])
				pointList.append(mpoint)

			self.deltas[frame] = pointList

		return frame



	def deform(self, dataBlock, geoIter, matrix, multiIndex):
		# capture all the neccessary data from the plugin attrs
		fileHandle = dataBlock.inputValue(self.mddFile)
		fileValue = fileHandle.asString()

		timeHandle = dataBlock.inputValue(self.time)
		timeValue = timeHandle.asInt()

		offsetHandle = dataBlock.inputValue(self.offset)
		offsetValue = offsetHandle.asInt() * -1 # adding positive value will move anim backwards(no reason!)

		cycleHandle = dataBlock.inputValue(self.cycle)
		cycleValue = cycleHandle.asBool()

		envelope = ompx.cvar.MPxDeformerNode_envelope
		dataHandleEnvelope = dataBlock.inputValue(envelope)
		envelopeValue = dataHandleEnvelope.asFloat()

		if fileValue != '':
			frame = self.loadData(fileValue, timeValue, offsetValue, cycleValue)
			deformVertPosition = om.MPointArray()

			while not geoIter.isDone():
				pointPosition = geoIter.position()
				cachePosition = self.deltas[frame][geoIter.index()]

				# pointPosition.x += (cachePosition.x - pointPosition.x)  * envelopeValue
				# pointPosition.y += (cachePosition.y - pointPosition.y)  * envelopeValue
				# pointPosition.z += (cachePosition.z - pointPosition.z)  * envelopeValue

				# geoIter.setPosition(pointPosition)
				deformVertPosition.append(cachePosition)
				geoIter.next()

			geoIterator.setAllPositions(mPointArray_meshVert)


def nodeCreator():
	return ompx.asMPxPtr(MddRead())



def nodeInitializer():
	nAttr = om.MFnNumericAttribute()
	tAttr = om.MFnTypedAttribute()

	# create the time attr
	MddRead.time = nAttr.create('time', 'ti', om.MFnNumericData.kInt, 0)
	nAttr.setKeyable(True)
	nAttr.setHidden(True)

	# offset
	MddRead.offset = nAttr.create('offset', 'of', om.MFnNumericData.kInt, 0)
	nAttr.setKeyable(True)

	# cycle
	MddRead.cycle = nAttr.create('cycle', 'cy', om.MFnNumericData.kBoolean, False)
	nAttr.setKeyable(True)

	# file input
	MddRead.mddFile = tAttr.create('mddFile', 'mf', om.MFnData.kString)
	tAttr.setStorable(True)
	tAttr.setKeyable(False)

	# add attr to the node
	try:
		MddRead.addAttribute(MddRead.time)
		MddRead.addAttribute(MddRead.offset)
		MddRead.addAttribute(MddRead.cycle)
		MddRead.addAttribute(MddRead.mddFile)

		# set affects
		outputGeom = ompx.cvar.MPxDeformerNode_outputGeom
		MddRead.attributeAffects(MddRead.time, outputGeom)
		MddRead.attributeAffects(MddRead.offset, outputGeom)
		MddRead.attributeAffects(MddRead.cycle, outputGeom)
		MddRead.attributeAffects(MddRead.mddFile, outputGeom)
	except Exception, e:
		sys.stderr.write('Failed to create attributes for node:  %s\n' %nodeName)
		sys.stderr.write('%s\n' %e)



def initializePlugin(mobject):
	mplugin = ompx.MFnPlugin(mobject, 'Nuternativ', '1.0')
	try:
		mplugin.registerNode(nodeName, typeId, 
			nodeCreator, nodeInitializer, ompx.MPxNode.kDeformerNode)
	except Exception, e:
		sys.stderr.write('Failed to register node:  %s\n' %nodeName)
		sys.stderr.write('%s\n' %e)



def uninitializePlugin(mobject):
	mplugin = ompx.MFnPlugin(mobject)
	try:
		mplugin.deregisterNode(typeId)
	except Exception, e:
		sys.stderr.write('Failed to deregister node:  %s\n' %nodeName)
		sys.stderr.write('%s\n' %e)



