import maya.OpenMaya as om
import maya.OpenMayaMPx as OpenMayaMPx

kPluginCmdName="averageSkinWeights"

kVtxFlag = "-vtx"
kVtxLongFlag = "-vertices"
kValueFlag = "-val"
kValueLongFlag = "-value"


# command
class AverageSkinWeightsCmd(ompx.MPxCommand):
	def __init__(self):
		ompx.MPxCommand.__init__(self)
		self.vtx = None
		self.value = 1.0
		self.dgModify = OM.MDGModifier()
	
	def isUndoable(self):
        return True

	def doIt(self, args):
		argData = OpenMaya.MArgDatabase(self.syntax(), args)

		if argData.isFlagSet(kVtxFlag):
			self.vtx = argData.flagArgumentString(kVtxFlag, 0)
		if argData.isFlagSet(kValueFlag):
			self.value = argData.flagArgumentDouble(kValueFlag, 0)

		self.redoIt()

	def redoIt(self, vtx, value):		
		# select the skinned geo
		selection = om.MSelectionList()
		om.MGlobal.getActiveSelectionList( selection )

		# get dag path for selection
		dagPath = om.MDagPath()
		components = om.MObject()
		array = om.MIntArray()
		selection.getDagPath( 0, dagPath, components )
		dagPath.extendToShape()
		
		# currentNode is MObject to your mesh
		currentNode = dagPath.node()
		mitVtx = om.MItMeshVertex (dagPath)
		
		fnSkin = None
		# get skincluster from shape
		try:
		  itDG = om.MItDependencyGraph(currentNode, om.MFn.kSkinClusterFilter, om.MItDependencyGraph.kUpstream)
		  while not itDG.isDone():
			oCurrentItem = itDG.currentItem()
			fnSkin = oma.MFnSkinCluster(oCurrentItem)
			break
		except:
		  om.MGlobal.displayError("No SkinCluster to paint on")
		  return om.MStatus.kFailure


		  component = om.MFnSingleIndexedComponent().create(om.MFn.kMeshVertComponent)
		  om.MFnSingleIndexedComponent(component).addElement( vtx )
		  
		  oldWeights = om.MDoubleArray()
		  surrWeights = om.MDoubleArray()
		  infCount = om.MScriptUtil()
		  inf = infCount.asUintPtr()
		  surrVtxArray = om.MIntArray()
		  newWeights = om.MDoubleArray()
		  infIndices = om.MIntArray()
		  prevVtxUtil = om.MScriptUtil( )
		  prevVtx = prevVtxUtil.asIntPtr()
		  
		  # create mesh iterator and get conneted vertices for averaging
		  mitVtx = om.MItMeshVertex (dagPath, component)
		  mitVtx.getConnectedVertices(surrVtxArray)
		  surrVtxCount = len(surrVtxArray)
				
		  surrComponents = om.MFnSingleIndexedComponent().create(om.MFn.kMeshVertComponent)
		  om.MFnSingleIndexedComponent(surrComponents).addElements( surrVtxArray )
		  
		  # read weight from single vertex (oldWeights) and from the surrounding vertices (surrWeights)
		  fnSkin.getWeights(dagPath, component, oldWeights, inf)
		  fnSkin.getWeights(dagPath, surrComponents, surrWeights, inf)
		  influenceCount = om.MScriptUtil.getUint(inf)
		  
		  # average all the surrounding vertex weights and multiply and blend it over the origWeight with the weight from the artisan brush
		  for i in xrange(influenceCount):
			infIndices.append( i )
			newWeights.append( 0.0 )
			for j in xrange(i,len(surrWeights),influenceCount):
			  newWeights[i] += (((surrWeights[j] / surrVtxCount) * value) + ((oldWeights[i] / surrVtxCount) * (1-value)))
		  
		  # set the final weights throught the skinCluster again
		  # self.dgModify. 
		  fnSkin.setWeights( dagPath, component, infIndices, newWeights, 1, oldWeights)

		  return om.MStatus.kSuccess

# Creator
def cmdCreator():
	# Create the command
	return ompx.asMPxPtr(AverageSkinWeightsCmd())

# Syntax creator
def syntaxCreator():
	syntax = om.MSyntax()
	return syntax

# Initialize the script plug-in
def initializePlugin(mobject):
	mplugin = ompx.MFnPlugin(mobject, "Nuternativ", "1.0", "Any")
	try:
		mplugin.registerCommand(kPluginCmdName, cmdCreator, syntaxCreator)
	except:
		sys.stderr.write( "Failed to register command: %s\n" % kPluginCmdName )
		raise

# Uninitialize the script plug-in
def uninitializePlugin(mobject):
	mplugin = ompx.MFnPlugin(mobject)
	try:
		mplugin.deregisterCommand(kPluginCmdName)
	except:
		sys.stderr.write( "Failed to unregister command: %s\n" % kPluginCmdName )
		raise

	
