import pymel.core as pm
import maya.OpenMaya as om
import re, os, socket, subprocess, shutil
from collections import defaultdict

from nuTools import misc, config
reload(config)
reload(misc)

from tools import info
reload(info)


class ProjectNavigator(object):

	def __init__(self, autoUpdate=True):

		# UI var
		self.WINDOW_NAME = 'projNavigatorWin'
		self.WINDOW_TITLE = 'Project Navigator v1.2'

		# proj config var
		self.projInfoDict = config.PROJ_DIR_DICT
		self.assetTypeFilter = config.ASSET_TYPE_FILTER
		self.filmEpFilters = config.FILM_EP_FILTER
		self.userDict = config.USER_DEPT_DICT
		self.userDept = 'Picture This'

		self.ASSET_FILM_DEPT = {'asset':['model', 'uv', 'rig', 'pipeline'], 'film':['anim', 'animClean', 'layout', 'light', 'fx']}

		self.PROJECT_LOADED = False
		self.MODE = 'asset'
		self.ASSETPATH = 'asset/3D'
		self.FILMPATH = 'film'
		self.AUTOUPDATE = autoUpdate

		self.deptPath = {'hero':'maya', 'work':'maya/work'}
		self.user = 'user'
		self.modelType = 'anim'
		self.dept = 'rig'
		self.currentMode = 'work'
		self.currentModeIndx = 2
		self.prevTabIndx = None
 		self.defaultAssetPathText = '< Paste your path here! >'
		self.dirExists = False

		# project vars
		self.currentProject = ''
		self.projPath = ''		
		self.projCode = ''
		self.epCode = ''

		self.assetPath = ''
		self.aSubType = True
		# self.hasEp = False
		self.backupMode = False

			

		self.focusAsset = ''
		self.focusFile = ''
		self.focusAssetPath = ''
		self.focusDeptPath = ''
		self.focusWorkPath = ''
		self.focusHeroPath = ''
		self.focusFilePath = ''
		

		self.assetDict = {}
		self.assetDictFiltered = {}
		self.assetEpDict = {}
		self.prevDict = {}
		self.ref = []

		self.currentEp = ''
		self.subTypeFiltered = []
		self.assetTypeFiltered = []
		self.subtypes = []


		# FILM MODE VARS
		self.filmPath = ''
		self.filmEpPath = ''
		self.shotPath = ''
		self.currentSeq = ''
		self.currentShot = ''
		self.seqs = []
		self.shots = []

		# get user from env var
		self.user = misc.getUserFromEnvVar(title=False)	
		self.getUserDept()	



	def UI(self):
		if pm.window(self.WINDOW_NAME, ex=True):
			pm.deleteUI(self.WINDOW_NAME, window=True)
		with pm.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, s=False, mnb=True, mxb=False) as self.mainWindow:
			with pm.frameLayout(lv=False, mh=5, mw=5) as self.mainFrame:

				# header UI
				with pm.frameLayout(lv=False, mh=0, mw=0) as self.headerFrame:
					with pm.rowColumnLayout(nc=2, co=[(1, 'left', 0), (2, 'left', 10)]):
						with pm.columnLayout(adj=True, rs=3, co=['both', 0]):
							with pm.rowColumnLayout(nc=4, co=[(1, 'left', 5), (2, 'left', 3), (3, 'left', 10), (4, 'left', 3)]):
								pm.text(l='Proj:')
								with pm.optionMenu(cc=pm.Callback(self.caller, 'setProject')) as self.projMenu:
									pass
								pm.text('User:')
								self.userTxtFld = pm.textField(w=55)
							with pm.rowColumnLayout(nc=2, co=[(1, 'left', 11), (2, 'left', 3)]):
								pm.text(l='EP:')
								with pm.optionMenu(cc=pm.Callback(self.caller, 'setEp'), w=214) as self.epMenu:
									pass

						with pm.columnLayout(adj=True, rs=3, co=['both', 0]):
							with pm.rowColumnLayout(nc=4, co=[(1, 'left', 17), (2, 'left', 3), (3, 'left', 5), (4, 'left', 5)]):
								pm.text(l='Path:')
								self.pathTextFld = pm.textField(ed=True, w=550, bgc=[0.3, 0, 0], tx=self.defaultAssetPathText, 
												   ec=pm.Callback(self.quickOpenFile))
								self.copyButt = pm.button(l='copy', c=pm.Callback(self.copyPath))
								self.openExplorerButt = pm.button(l='explore', c=pm.Callback(self.openExplorer))
							with pm.rowColumnLayout(nc=2, co=[(1, 'left', 5), (2, 'left', 3)]):
								pm.text(l='Recent:')
								with pm.optionMenu(cc=pm.Callback(self.caller, 'openPrevFile'), w=550) as self.prevMenu:
									pm.menuItem(l='')

				# working UI
				with pm.rowColumnLayout(nc=2, co=[(1, 'left', 0), (2, 'left', 35)]) as self.mainRowColumnLayout:
					with pm.tabLayout() as self.modeTabLayout:

						# asset mode UI
						with pm.rowColumnLayout(nc=2, co=[(1, 'left', 0), (2, 'left', 15)]) as self.assetModeLayout:
							with pm.frameLayout(lv=False, mh=5, mw=5, bs='etchedIn'):
								with pm.columnLayout(adj=True, rs=5):
									with pm.rowColumnLayout(nc=2, rs=((1, 0), (2, 3)), co=[(1, 'left', 20), (2, 'left', 7)]):
										self.fAllChkBox = pm.checkBox(l='All Type', v=False, cc=pm.Callback(self.assetCaller, 'checkFilter'))
										with pm.rowColumnLayout(nc=3, rs=((1, 0), (2, 0), (3, 0), (4, 0)), 
											co=[(1, 'left', 0), (2, 'left', 5), (3, 'left', 5)]):		
											self.fCharacterChkBox = pm.checkBox(l='character', v=True, cc=pm.Callback(self.assetCaller, 'checkFilter'))
											self.fPropChkBox = pm.checkBox(l='prop', v=True, cc=pm.Callback(self.assetCaller, 'checkFilter'))
											self.fSetChkBox = pm.checkBox(l='set', v=True, cc=pm.Callback(self.assetCaller, 'checkFilter'))
											self.fSetDressChkBox = pm.checkBox(l='setDress', v=True, cc=pm.Callback(self.assetCaller, 'checkFilter'))
											self.fVehicleChkBox = pm.checkBox(l='vehicle', v=True, cc=pm.Callback(self.assetCaller, 'checkFilter'))
											self.fClothingChkBox = pm.checkBox(l='clothing', v=True, cc=pm.Callback(self.assetCaller, 'checkFilter'))
											self.fEnvironmentChkBox = pm.checkBox(l='environment', v=True, cc=pm.Callback(self.assetCaller, 'checkFilter'))
											self.fWeaponChkBox = pm.checkBox(l='weapon', v=False, cc=pm.Callback(self.assetCaller, 'checkFilter'))
											self.fMiscChkBox = pm.checkBox(l='misc', v=False, cc=pm.Callback(self.assetCaller, 'checkFilter'))

										pm.text('SubType')
									
										self.subTypeTSL = pm.textScrollList(h=70, w=125, ams=True, sc=pm.Callback(self.assetCaller, 'filterSubtype'))

										pm.text(l='Asset Name')
										self.searchTxtFld = pm.textField(w=200, cc=pm.Callback(self.assetCaller, 'search'), ec=pm.Callback(self.assetCaller, 'search'))
									
									self.assetTSL = pm.textScrollList(h=255, w=320, ams=False, sc=pm.Callback(self.assetCaller, 'refreshAssetList'))
									pm.button(l='Copy Asset Paths', c=pm.Callback(self.copyAllPath, False))

							with pm.frameLayout(lv=False, mh=5, mw=5, bs='etchedIn'):
								with pm.columnLayout(adj=True, rs=2):
									with pm.columnLayout(adj=True, rs=25):
										with pm.rowColumnLayout(nc=6, rs=([1, 5]), co=[(1, 'left', 12), (2, 'left', 5), (3, 'left', 8), 
																		(4, 'left', 8), (5, 'left', 8), (6, 'left', 8)]):
											self.assetDeptRadioCol = pm.radioCollection()
											self.modelRadioButt = pm.radioButton(l='model', sl=False, onc=pm.Callback(self.assetCaller, 'refreshAssetList'))
											self.rigRadioButt = pm.radioButton(l='rig', sl=True, onc=pm.Callback(self.assetCaller, 'refreshAssetList'))
											self.uvRadioButt = pm.radioButton(l='uv', sl=False, onc=pm.Callback(self.assetCaller, 'refreshAssetList'))
											self.shadeRadioButt = pm.radioButton(l='shade', sl=False, onc=pm.Callback(self.assetCaller, 'refreshAssetList'))
											self.devRadioButt = pm.radioButton(l='dev', sl=False, onc=pm.Callback(self.assetCaller, 'refreshAssetList'))
											self.refRadioButt = pm.radioButton(l='ref', sl=False, onc=pm.Callback(self.assetCaller, 'refreshAssetList'))
											with pm.columnLayout(adj=True, rs=2, co=['left', 10]):
												self.modelTypeRadioCol = pm.radioCollection()
												self.animModelRadioButt = pm.radioButton(l='anim', sl=True, en=False, onc=pm.Callback(self.assetCaller, 'setModelType'))
												self.proxyModelRadioButt = pm.radioButton(l='proxy', sl=False, en=False, onc=pm.Callback(self.assetCaller, 'setModelType'))
												self.renderModelRadioButt = pm.radioButton(l='render', sl=False, en=False, onc=pm.Callback(self.assetCaller, 'setModelType'))
												self.bshModelRadioButt = pm.radioButton(l='bsh', sl=False, en=False, onc=pm.Callback(self.assetCaller, 'setModelType'))
										
										# pm.text(l='', h=25)
										
										with pm.columnLayout(adj=True, co=['both', 0]):
											with pm.tabLayout(cc=pm.Callback(self.caller, 'selectWorkHero')) as self.assetWorkHeroTabLayout:
												self.assetHeroTSL = pm.textScrollList(h=250, w=320, ams=False, sc=pm.Callback(self.assetCaller, 'selectFile'), dcc=pm.Callback(self.assetCaller, 'openFile'))
												self.assetWorkTSL = pm.textScrollList(h=250, w=320, ams=False, sc=pm.Callback(self.assetCaller, 'selectFile'), dcc=pm.Callback(self.assetCaller, 'openFile'))
												
												pm.tabLayout(self.assetWorkHeroTabLayout, edit=True, 
															tabLabel=((self.assetHeroTSL, '   Hero   '), (self.assetWorkTSL, '   Work   ')), 
															selectTabIndex=1)
											# pm.popupMenu( parent=self.assetWorkHeroTabLayout )
											# pm.menuItem(l='Backup..', c=pm.Callback(self.caller, 'backupFile'))

									
									with pm.rowColumnLayout(nc=2, co=[(1, 'left', 10), (2, 'left', 10)]):
										pm.button(l='Copy Hero Paths', w=150, c=pm.Callback(self.copyAllPath, True, 'hero'))
										pm.button(l='Copy Work Paths', w=150, c=pm.Callback(self.copyAllPath, True, 'work'))

					
						# film mode UI
						with pm.rowColumnLayout(nc=2, co=[(1, 'left', 0), (2, 'left', 15)]) as self.filmModeLayout:
							with pm.frameLayout(lv=False, mh=5, mw=5, bs='etchedIn'):
								with pm.frameLayout(lv=False, mh=20, mw=20, bs='etchedIn'):
									with pm.rowColumnLayout(nc=4, co=[(1, 'left', 7), (2, 'left', 2), (3, 'left', 67), (4, 'left', 2)]):
										pm.text(l='q')
										self.seqTextField = pm.textField(w=50, h=30, 
															cc=pm.Callback(self.filmCaller, 'searchSeq'), 
															ec=pm.Callback(self.filmCaller, 'searchSeq'))
										pm.text(l='s')
										self.shotTextField = pm.textField(w=50, h=30, 
															cc=pm.Callback(self.filmCaller, 'searchShot'), 
															ec=pm.Callback(self.filmCaller, 'searchShot'))
								with pm.rowColumnLayout(nc=2, co=[(1, 'left', 0), (2, 'left', 5)]):
									with pm.frameLayout(lv=False, mh=10, mw=10, bs='etchedIn'):
										self.seqTSL = pm.textScrollList(h=320, w=100, ams=False, sc=pm.Callback(self.filmCaller, 'selectSeq'))
									with pm.frameLayout(lv=False, mh=10, mw=10, bs='etchedIn'):
										self.shotTSL = pm.textScrollList(h=320, w=100, ams=False, sc=pm.Callback(self.filmCaller, 'selectShot'))

							with pm.frameLayout(lv=False, mh=10, mw=10, bs='etchedIn') as self.filmFileLayout:
								with pm.columnLayout(adj=True, rs=55, co=['both', 0]):
									with pm.rowColumnLayout(nc=5, co=[(1, 'left', 27), (2, 'left', 20), (3, 'left', 20), 
																	 (4, 'left', 20), (5, 'left', 20)]):
										self.filmDeptRadioCol = pm.radioCollection()
										self.animRadioButt = pm.radioButton(l='anim', sl=True, onc=pm.Callback(self.filmCaller, 'selectDept'))
										self.animCleanRadioButt = pm.radioButton(l='animClean', sl=False, onc=pm.Callback(self.filmCaller, 'selectDept'))
										self.layoutRadioButt = pm.radioButton(l='layout', sl=False, onc=pm.Callback(self.filmCaller, 'selectDept'))
										self.fxRadioButt = pm.radioButton(l='fx', sl=False, onc=pm.Callback(self.filmCaller, 'selectDept'))
										self.lightRadioButt = pm.radioButton(l='light', sl=False, onc=pm.Callback(self.filmCaller, 'selectDept'))

									with pm.columnLayout(adj=True, co=['both', 0]):
										# pm.text(l='', h=30)
										with pm.tabLayout(cc=pm.Callback(self.filmCaller, 'selectWorkHero')) as self.filmWorkHeroTabLayout:
											self.filmHeroTSL = pm.textScrollList(h=325, w=381, ams=False, sc=pm.Callback(self.filmCaller, 'selectFile'), dcc=pm.Callback(self.assetCaller, 'openFile'))
											self.filmWorkTSL = pm.textScrollList(h=325, w=381, ams=False, sc=pm.Callback(self.filmCaller, 'selectFile'), dcc=pm.Callback(self.assetCaller, 'openFile'))

											pm.tabLayout(self.filmWorkHeroTabLayout, edit=True, 
														tabLabel=((self.filmHeroTSL, '   Hero   '), (self.filmWorkTSL, '   Work   ')), 
														selectTabIndex=1)	
					
					# edit mode tabLayout
					pm.tabLayout( self.modeTabLayout, edit=True, 
								  tabLabel=((self.assetModeLayout, '   Asset   '), 
								  (self.filmModeLayout, '   Film   ')), 
								  sc=pm.Callback(self.caller, 'setProject'))

					# buttons layout
					# with pm.frameLayout(lv=False, mh=25, mw=15, bv=False) as self.buttonColumnLayout:

					with pm.columnLayout(adj=True, rs=50):
						pm.text(l='')
						with pm.columnLayout(adj=True, rs=5):
							# pm.text(l='', h=180)
							self.openButt = pm.button(l='Open', w=150, h=50, c=pm.Callback(self.caller, 'openFile'))
							
							self.savePPButt = pm.button(l='Save ++', w=120, h=30, c=pm.Callback(self.caller, 'incSave'))
								
						with pm.columnLayout(adj=True, rs=5):
							self.backupButt = pm.button(l='Backup', w=60, h=30, c=pm.Callback(self.caller, 'backupFile'))
							with pm.rowColumnLayout(nc=2, co=[(1, 'left', 20), (2, 'left', 40)]):
								self.backupRadioCol = pm.radioCollection()
								self.normalRadioButt = pm.radioButton(l='normal', sl=True, onc=pm.Callback(self.caller, 'backupModeChecked'))
								self.backupRadioButt = pm.radioButton(l='backup', sl=False, onc=pm.Callback(self.caller, 'backupModeChecked'))
											
						#with pm.columnLayout(adj=False, co=['right', 5]):
							# pm.text('', w=120)
							#self.backupModeChkBox = pm.checkBox(l='see\nbackup', v=False, cc=pm.Callback(self.caller, 'backupModeChecked'))
									

						with pm.columnLayout(adj=True, rs=5):
							with pm.rowColumnLayout(nc=2, co=[(1, 'left', 0), (2, 'left', 10)]):
								self.referenceButt = pm.button(l='create Ref', w=105, h=25, c=pm.Callback(self.createRef))
								self.removeRefButt = pm.button(l='remove Ref', w=70, h=25, c=pm.Callback(self.removeRef))
							self.importRefButt = pm.button(l='Import Ref', h=30, c=pm.Callback(self.importRef))
							with pm.columnLayout(adj=True, co=['left', 40]):
								self.removeNameSpaceChkBox = pm.checkBox(l='Remove Namespace', v=True)

						# pm.text(l='', h=24)
						with pm.columnLayout(adj=True, rs=5):
							self.updateButt = pm.button(l='Update to Current', w=140, h=20, c=pm.Callback(self.autoUpdate))
		
		# get user				
		self.userTxtFld.setText(self.user)
		self.existingProject()

		# auto update to cuurent open scene
		if self.AUTOUPDATE == True:
			self.autoUpdate()



	def getUserDept(self):
		for dept, users in self.userDict.iteritems():
			if self.user.title() in users:
				self.userDept = dept
				print '\nWelcome  %s  from  %s  department.' %(self.user, dept),
				return



	def autoUpdate(self, currentScene=None):
		# get current scene name
		if not currentScene:
			currentScene = pm.sceneName()

		# see if current scene in film or asset
		if '/%s/' %self.ASSETPATH in currentScene:
			self.MODE = 'asset'
			self.modeTabLayout.setSelectTabIndex(1)
			return self.updateAssetUI(currentScene)

		elif '/%s/' %self.FILMPATH in currentScene:
			self.MODE = 'film'
			self.modeTabLayout.setSelectTabIndex(2)
			return self.updateFilmUI(currentScene)

		else: # if current scene failed, try see if user in any dept
			self.caller('resetProj')
			self.projMenu.setValue('N/A')
			self.pathTextFld.setText(currentScene)

			for mode, dept in self.ASSET_FILM_DEPT.iteritems():
				if self.userDept in dept:
					self.MODE = mode
					if self.MODE == 'asset':		
						self.modeTabLayout.setSelectTabIndex(1)
						self.dept = self.userDept 
						self.selectAssetDeptRadioButt()
						return False
					elif self.MODE == 'film':			
						self.modeTabLayout.setSelectTabIndex(2)
						self.dept = self.userDept 
						self.selectFilmDeptRadioButt()
						return False


	def quickOpenFile(self):
		pathFromTextFld = self.pathTextFld.getText()
		cleanPath = misc.cleanPath(pathFromTextFld)

		self.focusFilePath = cleanPath

		# if os.path.isdir(cleanPath) == True:
		

		# else:
		if os.path.isfile(cleanPath) == True:
			res = self.openFile()
		else:
			res = os.path.exists(cleanPath)
			

		if res == True:
			# self.focusFilePath = path
			self.enablePathTxtFld()
		elif res == False:
			self.resetPathTxtFld(pathFromTextFld)
			return
		else:
			self.pathTextFld.setText(self.focusDeptPath)
			return

		updateResult = self.autoUpdate(cleanPath)
		if updateResult == False:
			self.caller('resetProj')
			self.projMenu.setValue('N/A')

		self.pathTextFld.setText(cleanPath)
		

	########### caller functions ###########

	def caller(self, op):
		selectedIndex = self.modeTabLayout.getSelectTabIndex()
		if selectedIndex == 1:
			self.MODE = 'asset'
			self.assetCaller(op)
		elif selectedIndex == 2:
			self.MODE = 'film'
			self.filmCaller(op)



	def assetCaller(self, op):
		
		if op == 'setProject':
			self.setProject()
			if not self.currentProject or self.currentProject == 'N/A':
				self.assetCaller('resetProj')
				return

			self.getAssetPath()
			self.getProjCode()
			
			self.assetCaller('setEp')
			
		elif op == 'setEp':
			self.setAssetEp()

			self.getFilterChecked()	
			self.resetSubType()

			if self.PROJECT_LOADED == False:
				self.assetCaller('resetProj')
				return

			self.getAllAsset()
			self.addSubTypeTSL()

			# self.searchTxtFld.setText('')
			self.getSubTypeFiltered()
			self.updateAssetTSL()

			self.resetFocusVar()
			self.resetAssetTSL()
			self.resetPathTxtFld()

		elif op == 'resetProj':
			# reset UI
			self.epMenu.clear()
			self.resetPathTxtFld()
			self.resetSubType()
			self.resetAssetTSL()
			self.assetTSL.removeAll()
			self.resetFocusVar()
			self.assetDict = {}

			self.resetProjVar()

		elif op == 'search':
			selectedAsset = self.getTSLSelection('assetTSL')
			
			self.getSubTypeFiltered()
			self.updateAssetTSL()
			self.resetFocusVar()

			if selectedAsset in self.assetTSL.getAllItems():
				self.assetTSL.setSelectItem(selectedAsset)
				self.assetCaller('refreshAssetList')
			else:
				self.resetAssetTSL()
				self.resetPathTxtFld()

		elif op == 'checkFilter':
			selectedAsset = self.getTSLSelection('assetTSL')
			
			self.checkAll()
			self.getFilterChecked()	
			self.resetSubType()

			self.getAllAsset()
			self.addSubTypeTSL()

			self.getSubTypeFiltered()
			self.updateAssetTSL()
			
			self.resetFocusVar()

			if selectedAsset in self.assetTSL.getAllItems():
				self.assetTSL.setSelectItem(selectedAsset)
				self.assetCaller('filterSubtype')
			else:
				self.resetAssetTSL()
				self.resetPathTxtFld()

		elif op == 'filterSubtype':
			selectedAsset = self.getTSLSelection('assetTSL')

			self.getSubTypeFiltered()

			self.updateAssetTSL()
			self.resetFocusVar()

			if selectedAsset in self.assetTSL.getAllItems():
				self.assetTSL.setSelectItem(selectedAsset)
				self.assetCaller('refreshAssetList')
			else:
				self.resetAssetTSL()
				self.resetPathTxtFld()

		elif op == 'refreshAssetList':
			self.selectAssetDept()
			self.assetCaller('selectWorkHero')

		elif op == 'setModelType':
			self.setModelType()
			self.assetCaller('selectWorkHero')

		elif op == 'selectWorkHero':
			self.getHeroWork()
			self.updateAssetWorkTSL()
			self.updateAssetPathTxtFld('selectWorkHero')
			self.lockSaveButt()

		elif op == 'selectFile':
			self.updateAssetPathTxtFld('selectFile')

		elif op == 'incSave':
			self.incSave()
			self.assetWorkHeroTabLayout.setSelectTabIndex(2)
			self.getHeroWork()
			self.updateAssetWorkTSL()
			self.selectIncSavedFile()
			self.updateAssetPathTxtFld('selectFile')
			
		elif op == 'openFile':
			self.openFile()
			self.addToPrevOpen()

		elif op == 'openPrevFile':
			currentPrev = self.prevMenu.getValue()
			self.focusFilePath = self.prevDict[currentPrev]
			self.openFile()
			self.prevMenu.setValue('')
			self.updateAssetUI()

		elif op == 'backupModeChecked':
			selectedFile = self.getTSLSelection('assetWorkHeroTSL')
			self.getBackupMode()
			self.assetCaller('refreshAssetList')

		elif op == 'backupFile':
			self.backupFile()
			if self.backupMode == True:
				self.assetCaller('refreshAssetList')

		# elif op == 'switchMode':
		# 	self.updateAssetUI()

	def resetFilmTSL(self):
		self.filmWorkTSL.removeAll()
		self.filmHeroTSL.removeAll()
		self.seqTextField.setText('')
		self.shotTextField.setText('')			

		
	def filmCaller(self, op):
		if op == 'setProject':
			self.setProject()
			if not self.currentProject or self.currentProject == 'N/A':
				self.filmCaller('resetProj')
				return
			self.savePPButt.setEnable(True)
			self.getFilmPath()
			self.getProjCode()
			self.getFilmEps()

			self.resetPathTxtFld()
			self.resetFilmTSL()
			self.resetFocusVar()		

		elif op == 'setEp':
			self.setFilmEp()
			self.getFilmEpCode()

			self.resetPathTxtFld()
			self.resetFilmTSL()

		elif op == 'resetProj':
			# reset UI
			self.epMenu.clear()
			self.resetPathTxtFld()
			self.filmWorkTSL.removeAll()
			self.filmHeroTSL.removeAll()
			self.seqTextField.setText('')
			self.shotTextField.setText('')
			self.seqTSL.removeAll()
			self.shotTSL.removeAll()
			self.resetFocusVar()

			self.resetProjVar()

		elif op == 'searchSeq':
			if self.searchSeqTSL() == True:
				self.filmCaller('selectSeq')

		elif op == 'searchShot':
			if self.searchShotTSL() == True:
				self.filmCaller('selectShot')

		elif op == 'selectSeq':
			selShot = self.getTSLSelection('shotTSL')
			self.selectSeq()
			self.updateFilmPathTxtFld('selectSeq')

			if self.dept == 'layout':
				try:
					self.shotTSL.setSelectItem('all')
					self.filmCaller('selectShot')
				except:
					pass

			if selShot in self.shotTSL.getAllItems():
				self.shotTSL.setSelectItem(selShot)
				self.filmCaller('selectShot')
			else:
				self.filmWorkTSL.removeAll()
				self.filmHeroTSL.removeAll()

		elif op == 'selectShot':
			self.selectShot()
			# self.updateFilmPathTxtFld('selectShot')
			self.filmCaller('selectDept')

		elif op == 'selectDept':
			self.selectFilmDept()
			self.filmCaller('selectWorkHero')

		elif op == 'selectWorkHero':
			self.getHeroWork()
			self.updateFilmWorkTSL()
			self.updateFilmPathTxtFld('selectWorkHero')

		elif op == 'selectFile':
			self.updateFilmPathTxtFld('selectFile')

		elif op == 'incSave':
			self.incSave()
			self.filmWorkHeroTabLayout.setSelectTabIndex(2)
			self.getHeroWork()
			self.updateFilmWorkTSL()
			self.selectIncSavedFile()
			self.updateAssetPathTxtFld('selectFile')

		elif op == 'openFile':
			self.openFile()
			self.addToPrevOpen()

		elif op == 'openPrevFile':
			currentPrev = self.prevMenu.getValue()
			self.focusFilePath = self.prevDict[currentPrev]
			self.openFile()
			self.prevMenu.setValue('')
			self.updateFilmUI()

		elif op == 'backupModeChecked':
			selectedFile = self.getTSLSelection('filmWorkHeroTSL')
			self.getBackupMode()
			self.filmCaller('selectWorkHero')

		elif op == 'backupFile':
			self.backupFile()
			if self.backupMode == True:
				self.assetCaller('selectWorkHero')

		# elif op == 'switchMode':
		# 	self.updateFilmUI()


	########################################



	def updateFilmUI(self, currentPath=None):
		try:
			if not currentPath:
				currentPath = pm.sceneName()

			self.filmCaller('resetProj')
			self.modeTabLayout.setSelectTabIndex(2)

			# if not self.currentProject:
			self.currentProject = self.getCurrentProject(currentPath)
			if not self.currentProject or self.currentProject not in self.projInfoDict.keys():
				self.filmCaller('resetProj')
				return False

			self.projMenu.setValue(self.currentProject)
			self.filmCaller('setProject')

			self.currentEp = self.getCurrentFilmEp(currentPath)

			self.epMenu.setValue(self.currentEp)
			self.filmCaller('setEp')


			seqSearch = re.search(r"(/q\d\d\d\d)", currentPath)
			seq = seqSearch.group()[1:]
			self.seqTSL.setSelectItem(seq)
			self.filmCaller('selectSeq')

			shotSearch = re.search(r"(/s\d\d\d\d)", currentPath)
			shot = shotSearch.group()[1:]
			self.shotTSL.setSelectItem(shot)
			self.filmCaller('selectShot')

			self.getCurrentFilmDept(currentPath)
			self.selectFilmDeptRadioButt()

			self.getCurrentHeroWork(currentPath)

			self.filmCaller('selectDept')

			if os.path.isfile(currentPath) == True:
				fileName = os.path.split(currentPath)[1]
				focusTSL = self.getFocusTSL()
				focusTSL.setSelectItem(fileName)
				self.filmCaller('selectFile')

			return True

		except: return False

	def getCurrentHeroWork(self, currentPath=None):
		if not currentPath:
			currentPath = pm.sceneName()

		if self.MODE == 'asset':
			if '/work/' in currentPath or currentPath.endswith('/work'):
				self.assetWorkHeroTabLayout.setSelectTabIndex(2)
			else:
				self.assetWorkHeroTabLayout.setSelectTabIndex(1)

		elif self.MODE == 'film':
			if 'scenes/work/' in currentPath or currentPath.endswith('scenes/work'):
				self.filmWorkHeroTabLayout.setSelectTabIndex(2)
			else:
				self.filmWorkHeroTabLayout.setSelectTabIndex(1)



	def updateAssetUI(self, currentPath=None):
		
		try:
			if not currentPath:
				currentPath = pm.sceneName()

			self.assetCaller('resetProj')
			self.modeTabLayout.setSelectTabIndex(1)
			

			# if not self.currentProject:
			self.currentProject = self.getCurrentProject(currentPath)

			if not self.currentProject or self.currentProject not in self.projInfoDict.keys():
				self.assetCaller('resetProj')
				return False

			# set proj
			self.projMenu.setValue(self.currentProject)

			self.setProject()		
			self.getAssetPath()
			self.getProjCode()

			if not self.currentEp:
				# get current episode
				self.currentEp = self.getCurrentAssetEp(currentPath)

				# set ep , get all assets
				self.epMenu.setValue(self.currentEp)

			self.assetCaller('setEp')

			# find current asset. select it in the ui. refresh ui
			self.getCurrentAsset(currentPath)

			if self.focusAsset:
				self.assetTSL.setSelectItem(self.focusAsset)
				self.getCurrentAssetDept(currentPath)
				self.selectAssetDeptRadioButt()
				self.getCurrentHeroWork(currentPath)
				self.assetCaller('refreshAssetList')

				if os.path.isfile(currentPath) == True:
					fileName = os.path.split(currentPath)[1]
					focusTSL = self.getFocusTSL()
					focusTSL.setSelectItem(fileName)
					self.assetCaller('selectFile')

			return True

		except: return False


	def resetAssetTSL(self):
		self.assetWorkTSL.removeAll()
		self.assetHeroTSL.removeAll()


	def getBackupMode(self):
		self.backupMode = self.backupRadioButt.getSelect()


	def searchSeqTSL(self):
		searchSeq = self.seqTextField.getText()
		found = []
		
		if searchSeq:
			for seq in self.seqs:
				if seq.endswith(searchSeq):
					self.seqTSL.setSelectItem(seq)
					return True

		# not found, reset			
		self.seqTextField.setText('')
		return False



	def searchShotTSL(self):
		searchShot = self.shotTextField.getText()
		found = []
		
		if searchShot:
			for shot in self.shots:
				if shot.endswith(searchShot):
					self.shotTSL.setSelectItem(shot)
					return True

		# not found, reset
		self.shotTextField.setText('')	
		return False



	def updateFilmWorkTSL(self):
		self.filmHeroTSL.removeAll()
		self.filmWorkTSL.removeAll()

		self.focusHeroPath = '%s/%s' %(self.focusDeptPath, self.deptPath['hero'])

		self.focusWorkPath = '%s/%s' %(self.focusDeptPath, self.deptPath['work'])

		if self.backupMode == True:
			self.focusHeroPath = '%s/_backup' %(self.focusHeroPath)	
			self.focusWorkPath = '%s/_backup' %(self.focusWorkPath)


		if os.path.exists(self.focusHeroPath) == True:
			h = [self.filmHeroTSL.append(f) for f in os.listdir(self.focusHeroPath) if os.path.isfile(os.path.join(self.focusHeroPath,f))]

		if os.path.exists(self.focusWorkPath) == True:
			w = [self.filmWorkTSL.append(f) for f in os.listdir(self.focusWorkPath) if os.path.isfile(os.path.join(self.focusWorkPath,f))]

	def selectFilmDept(self):
		# if not self.shotPath:
		# 	return

		if self.animRadioButt.getSelect() == True:
			self.dept = 'anim'
			self.deptPath = {'hero':'scenes', 'work':'scenes/work'}
		elif self.animCleanRadioButt.getSelect() == True:
			self.dept = 'animClean'
			self.deptPath = {'hero':'scenes', 'work':'scenes/work'}
		elif self.layoutRadioButt.getSelect() == True:
			self.dept = 'layout'
			self.deptPath = {'hero':'scenes', 'work':'scenes/work'}
		elif self.fxRadioButt.getSelect() == True:
			self.dept = 'fx'
			self.deptPath = {'hero':'scenes', 'work':'scenes/work'}
		elif self.lightRadioButt.getSelect() == True:
			self.dept = 'light'
			self.deptPath = {'hero':'scenes', 'work':'scenes/work'}		
		

		self.focusDeptPath = '%s/%s' %(self.shotPath, self.dept)
	


	def updateFilmPathTxtFld(self, op):
		toset = ''
		self.focusFilePath = ''

		if op == 'selectSeq':	
			toset = '%s/%s' %(self.filmEpPath, self.currentSeq)
		elif op == 'selectShot':	
			toset = '%s/%s/%s' %(self.filmEpPath, self.currentSeq, self.currentShot)
		elif op == 'selectDept':
			# toset = self.focusDeptPath
			pass
		elif op == 'selectWorkHero':
			if self.currentMode == 'hero':
		 		toset = self.focusHeroPath
		 	else:
		 		toset = self.focusWorkPath

		elif op == 'selectFile':
			fileSel = self.getTSLSelection('filmWorkHeroTSL')

			if self.currentMode == 'hero':
				focusPath = self.focusHeroPath
			else:
				focusPath = self.focusWorkPath

			self.focusFilePath = '%s/%s' %(focusPath, fileSel)
			toset = self.focusFilePath

		# set txt field
		if os.path.exists(toset):
			self.pathTextFld.setText(toset)
			self.pathTextFld.setBackgroundColor([0,0.4,0])
			self.dirExists = True
		else:
			self.resetPathTxtFld()
			self.assetWorkTSL.removeAll()



	def selectSeq(self):
		selectedSeqs = self.seqTSL.getSelectItem()
		if not selectedSeqs:
			return 

		self.currentSeq = selectedSeqs[0]
		seqPath = '%s/%s' %(self.filmEpPath, self.currentSeq)
		shotFolders = (f for f in os.listdir(seqPath) if os.path.isdir('%s/%s' %(seqPath, f))==True)

		self.shots = []
		self.shotTSL.removeAll()
		for s in shotFolders:
			reSearch = re.search(r"(s\d\d\d\d)", s)
			if reSearch:
				sStr = reSearch.group()
			elif s in ['all', '3D']:
				sStr = s
			else:
				continue

			self.shotTSL.append(s)
			self.shots.append(sStr)

		# fill seq txtFld
		self.seqTextField.setText(self.currentSeq[1:])

		# clear shot vars
		self.shotPath = ''
		self.currentShot = ''



	def selectShot(self):
		selectedShot = self.shotTSL.getSelectItem()
		
		if not selectedShot:
			return

		self.currentShot = selectedShot[0]

		self.shotPath = '%s/%s/%s' %(self.filmEpPath, self.currentSeq, self.currentShot)
		
		# fill shot txtFld
		toFill = self.currentShot
		if self.currentShot not in ['all', '3D']:
			toFill = self.currentShot[1:]

		self.shotTextField.setText(toFill)



	def setFilmEp(self):
		epValue = self.epMenu.getValue()
		self.currentEp = epValue
		self.filmEpPath = '%s/%s' %(self.filmPath, epValue)
		seqFolders = (f for f in os.listdir(self.filmEpPath) if os.path.isdir('%s/%s' %(self.filmEpPath, f))==True)

		self.seqs = []
		self.seqTSL.removeAll()
		self.shotTSL.removeAll()

		for q in seqFolders:
			reSearch = re.search(r"(q\d\d\d\d)", q)
			if reSearch:
				qStr = reSearch.group()
				self.seqTSL.append(q)
				self.seqs.append(qStr)



	def getFilmEpCode(self):
		episode = info.episode(self.currentProject, self.currentEp)
		self.epCode = episode.code()



	def existingProject(self):
		self.projMenu.clear()
		pm.menuItem(l='N/A', parent=self.projMenu)
		for k in sorted(self.projInfoDict.keys()):
			pm.menuItem(l=k, parent=self.projMenu)


	def getFilmPath(self):
		self.projPath = self.projInfoDict[self.currentProject][0]
		self.filmPath = '%s/%s' %(self.projPath, self.FILMPATH)



	def getFilmEps(self):
		self.epMenu.clear()
		self.clearSeqShotTSL()

		pm.menuItem(l='', parent=self.epMenu)
		for ep in (f for f in os.listdir(self.filmPath) if os.path.isdir('%s/%s' %(self.filmPath, f))==True):
			if ep.startswith('_') or ep in self.filmEpFilters:
				continue
			pm.menuItem(l=ep, parent=self.epMenu)

		self.PROJECT_LOADED = True


	def clearSeqShotTSL(self):
		self.seqTSL.removeAll()
		self.shotTSL.removeAll()



	def addToPrevOpen(self):
		label = os.path.split(self.focusFilePath)[-1]

		if self.focusFilePath not in self.prevDict.values():
			self.prevDict[label] = self.focusFilePath
			pm.menuItem(l=label, p=self.prevMenu, ia='')



	def backupFile(self):
		if not self.focusFilePath:
			return

		if self.currentMode == 'work':
			filePath = self.focusWorkPath
		elif self.currentMode == 'hero':
			filePath = self.focusHeroPath
		else:
			return

		if filePath.endswith('/_backup') == False:
			backupDir = '%s/_backup' %filePath
		else:
			backupDir = filePath

		if not os.path.exists(backupDir):
			print 'Backup folder created at : %s' %filePath,
			os.makedirs(backupDir)

		fileName = self.genFileName(backupDir)
		bkFileName = '_backup_%s' %fileName 

		toCopy = self.focusFilePath
		toBackUp = '%s/%s' %(backupDir, bkFileName)

		shutil.copyfile(toCopy, toBackUp)

		pm.confirmDialog( title='Backup success!', 
			message='%s\nBacked up' %toBackUp,
			button=['OK'], defaultButton='OK', cancelButton='OK', dismissString='OK' )
	


	def getSubTypeFiltered(self):
		sels = self.subTypeTSL.getSelectItem()

		if not sels or '' in sels:
			self.subTypeFiltered = []
			return

		self.subTypeFiltered = sels



	def checkAll(self):
		allValue = self.fAllChkBox.getValue()
		allChkBoxes = [self.fCharacterChkBox, self.fPropChkBox, self.fSetChkBox, self.fSetDressChkBox, 
					   self.fVehicleChkBox, self.fWeaponChkBox, self.fEnvironmentChkBox, self.fClothingChkBox,
					   self.fMiscChkBox]
		for c in allChkBoxes:
			c.setEnable(not allValue)



	def createRef(self):
		if self.focusFilePath == pm.sceneName() or not self.focusFilePath:
			return

		fileName = ''
		try:
			fileExt = os.path.split(self.focusFilePath)[-1]
			fileName = os.path.splitext(fileExt)[0]
		except:
			return

		self.ref.append(pm.createReference(self.focusFilePath, ns=fileName))



	def copyPath(self):
		if self.dirExists == False:
			return
		path = self.pathTextFld.getText()
		ctrlPress = misc.checkMod('ctrl')
		if ctrlPress == True:
			toCopyPath = '/'.join(path.split('/')[0:-1])
		else:
			toCopyPath = path
		misc.addToClipBoard(toCopyPath)



	def copyAllPath(self, file, mode='work'):
		result = ''
		currPath = ''
		count = 0
		for path in self.assetDictFiltered.values():
			currPath = path
			if file == True:
				if self.dept not in ['dev', 'ref']:
					currPath += '/%s' %self.dept
					if self.dept == 'model':
						currPath += '/%s' %self.modelType
					currPath += '/%s' %self.deptPath[mode]
					try:
						currPath = misc.getLatestModFileInDir(currPath)
					except:
						pass
				else:
					om.MGlobal.displayWarning('Invalid department selected : %s.' %self.dept)
					return

			result += currPath
			result += '\n'
			count += 1 

		misc.addToClipBoard(result)
		print '\n%s path(s) copied to clipboard.' %count,



	def enablePathTxtFld(self):
		self.pathTextFld.setBackgroundColor([0,0.4,0])
		self.dirExists = True



	def getCurrentProject(self, currentPath):	

		proj = ''
		for k in self.projInfoDict.keys():
			if '/%s/' %k in currentPath:
				proj = k
				break

		return proj



	def getCurrentAssetEp(self, currentPath):

		ep = ''
		projInfo = self.projInfoDict[self.currentProject][1]

		for k in projInfo.keys():
			if '/%s/' %k in currentPath:
				ep = k
				break
		return ep
		


	def getCurrentFilmEp(self, currentPath):
		try:
			splits = currentPath.split('%s/film/' %self.currentProject)
			if len(splits) < 2:
				return ''
			filmParts = splits[-1].split('/')[0]
			return filmParts
		except:
			return ''



	def getCurrentAsset(self, currentPath=None):
		if not self.assetDict or self.PROJECT_LOADED == False:
			return
		if not currentPath:
			currentPath = pm.sceneName()

		for asset in self.assetDict.keys():
			try:
				mname = re.search(r"((\w+))", asset)
				assetStrip = mname.group()
				subType = asset.replace(assetStrip, '').strip()[1:-1]
				if subType: 
					subType = '/%s' %subType
				toSearch = '%s/%s/' %(subType, assetStrip)
			except: pass
		
			if toSearch in currentPath:
				self.focusAsset = asset
				return
			else:
				self.focusAsset = ''
		


	def getCurrentAssetDept(self, currentPath):
		depts = ['rig' ,'model', 'uv', 'shade', 'dev' ,'ref'] 
		modelTypes = ['anim', 'proxy', 'render', 'bsh']
		try:
			self.dept = [d for d in depts if '/%s/' %d in currentPath][0]
			currentDeptPath = ''
			if self.dept == 'model':
				currentModelType = [t for t in modelTypes if '/%s/' %t in currentPath][0]
				self.modelType = currentModelType
				currentDeptPath = '%s/%s' %(currentDeptPath, currentModelType)
			if '/work/' in currentPath:
				self.assetWorkHeroTabLayout.setSelectTabIndex(2)
			else:
				self.assetWorkHeroTabLayout.setSelectTabIndex(1)
	
		except:
			pass



	def getCurrentFilmDept(self, currentPath):
		# currentPath = pm.sceneName()
		depts = ['anim' ,'animClean', 'layout', 'fx', 'light'] 
		try:
			self.dept = [d for d in depts if '/%s/' %d in currentPath][0]
			currentDeptPath = ''
			if '/work/' in currentPath:
				self.filmWorkHeroTabLayout.setSelectTabIndex(2)
			else:
				self.filmWorkHeroTabLayout.setSelectTabIndex(1)
		except:
			pass


	def checkIfAsset(self, path):
		# items = os.walk(path).next()[1]
		items = [f for f in os.listdir(path) if os.path.isdir('%s/%s' %(path, f))==True]
		return 'dev' in items

	def checkAssetCode(self, asset):
		prefix = '%s_' %self.projCode
		return asset.startswith(prefix)


	def addSubTypeTSL(self):
		for st in sorted(self.subtypes):
			self.subTypeTSL.append(st)

	def resetSubType(self):
		self.subTypeFiltered = []
		self.subTypeTSL.removeAll()
		self.subTypeTSL.append('')
		self.subTypeTSL.setSelectItem('')



	def getAllAsset(self):
		# wait cursor on
		pm.waitCursor(st=True)

		self.assetDict = {}
		self.subtypes = []
		# print self.assetPath
		# existingTypeFolder = os.walk(self.assetPath).next()[1]
		existingTypeFolder = (f for f in os.listdir(self.assetPath) if os.path.isdir(os.path.join(self.assetPath, f))==True)
		intersecTypes = set(self.assetTypeFiltered).intersection(existingTypeFolder)

		assets = {}
		projCode = '%s_' %self.projCode

		if self.aSubType == False:
			for typ in intersecTypes:
				typPath = '%s/%s' %(self.assetPath, typ)
				typeFols = (f for f in os.listdir(typPath) if os.path.isdir('%s/%s' %(typPath, f))==True)
				assetFolders = (i for i in typeFols if i.startswith(projCode) or 'dev' in os.listdir('%s/%s' %(typPath, i)))
				for a in assetFolders:
					assetPath = '%s/%s' %(typPath, a)
					assets[a] = assetPath

		else:
			assetDict = defaultdict(list)
			for typ in intersecTypes:
				typPath = '%s/%s' %(self.assetPath, typ)

				# walk, filter out empty folder
				subTypes = (f for f in os.listdir(typPath) if os.path.isdir('%s/%s' %(typPath, f))==True and os.listdir('%s/%s'%(typPath, f)))
				# subTypes = (f for f in typeFols if os.listdir('%s/%s'%(typPath, f)))

				for st in subTypes:
					stPath = '%s/%s' %(typPath, st)

					self.subtypes.append(st)
					foldsInSt = (f for f in os.listdir(stPath) if os.path.isdir('%s/%s' %(stPath, f))==True)
					assetFolders = (f for f in foldsInSt if f.startswith(projCode) or 'dev' in os.listdir('%s/%s' %(stPath, f)))
					# assetFolders = (i for i in os.walk(stPath).next()[1] if i.startswith(projCode) or 'dev' in os.listdir('%s/%s' %(stPath, i)))
					for a in assetFolders:
						assetPath = '%s/%s' %(stPath, a)
						val = (typ, st, assetPath)
						assetDict[a].append(val)
						# if a in assets.keys():
						# 	value = assets[a]
						# 	oldSt = value.split('/')[-2]
						# 	del(assets[a])

						# 	oldKey = '%s (%s)' %(a, oldSt)
						# 	assets[oldKey] = value

						# 	key = '%s (%s)' %(a, st)
						# 	assets[key] = assetPath 

						# else:
						# 	assets[a] = assetPath

			for ast, vals in assetDict.iteritems():
				if len(vals) == 1:  # no duplicate name
					assets[ast] = vals[0][2]
				else:
					for v in vals:
						key = '%s (%s/%s)' %(ast, v[0], v[1])
						assets[key] = v[2]

		
		self.assetDict  = assets
		self.subtypes.sort()
		pm.waitCursor(st=False)
		
	def filterAssets(self):
		newAssetDict = {}
		for asset, path in self.assetDict.iteritems():
			for typ in self.assetTypeFiltered:
				if '/%s/' %typ in path:
					newAssetDict[asset] = path
					break

	def getTSLSelection(self, tsl):
		if tsl == 'assetTSL':
			TSL = self.assetTSL
		elif tsl == 'assetWorkHeroTSL':
			if self.currentMode == 'hero':
				TSL = self.assetHeroTSL
			else:
				TSL = self.assetWorkTSL
		elif tsl == 'seqTSL':
			TSL = self.seqTSL
		elif tsl == 'shotTSL':
			TSL = self.shotTSL
		elif tsl == 'filmWorkHeroTSL':
			if self.currentMode == 'hero':
				TSL = self.filmHeroTSL
			else:
				TSL = self.filmWorkTSL

		try:
			return TSL.getSelectItem()[0]
		except:
			return ''

	def getFocusTSL(self):
		if self.MODE == 'asset':
			if self.currentMode == 'hero':
				tsl = self.assetHeroTSL
			else:
				tsl = self.assetWorkTSL
		else:
			if self.currentMode == 'hero':
				tsl = self.filmHeroTSL
			else:
				tsl = self.filmWorkTSL

		return tsl

			


	def getHeroWork(self):
		if self.MODE == 'asset':
			selTab = self.assetWorkHeroTabLayout.getSelectTabIndex()
		else:
			selTab = self.filmWorkHeroTabLayout.getSelectTabIndex()

		if selTab == 1:
			self.currentMode = 'hero'
			self.currentModeIndx = 1
		else:
			self.currentMode = 'work'
			self.currentModeIndx = 2




	def importRef(self):
		removeNs = self.removeNameSpaceChkBox.getValue()
		try:
			for ref in self.ref:
				ref.importContents(removeNamespace=removeNs)
			self.ref = []
		except:
			pass



	def incSave(self):
		if self.dept in ['dev', 'ref']:
			om.MGlobal.displayWarning('Cannot save to work in : %s.' %self.dept)
			return

		incSaveDir = '%s/%s' %(self.focusDeptPath, self.deptPath['work'])

		if not os.path.exists(incSaveDir):
			return

		fileName = self.genFileName(incSaveDir)
		incSaveFilePath = '%s/%s' %(incSaveDir, fileName)


		if os.path.exists(incSaveFilePath):
			return
		else:
			pm.saveAs(incSaveFilePath)
			self.focusFile = fileName
			print ('\nFile Saved to: %s' %incSaveFilePath),



	def genFileName(self, path):
		self.user = self.userTxtFld.getText()
		if self.dept == 'model':
			deptPart = '%s_%s' %(self.dept, self.modelType)
		else: deptPart = self.dept

		if self.MODE == 'asset':
			mname = re.search(r"((\w+))", self.focusAsset)
			if mname:
				fileStart = mname.group()
			else:
				fileStart = self.focusAsset
		else:
			fileStart = '%s_%s_%s_%s' %(self.projCode, self.epCode, self.currentSeq, self.currentShot)

		fileName = '%s_%s_v%s_%s.ma' %(fileStart, deptPart, misc.genVersion(path), self.user)

		return fileName



	def lockSaveButt(self):
		if self.dept in ['ref', 'dev'] and self.currentMode == 'hero':
			self.savePPButt.setEnable(False)
		else:
			self.savePPButt.setEnable(True)



	def setModelType(self):
		if self.animModelRadioButt.getSelect() == True:
			self.modelType = 'anim'
		elif self.proxyModelRadioButt.getSelect() == True:
			self.modelType = 'proxy'
		elif self.renderModelRadioButt.getSelect() == True:
			self.modelType = 'render'
		elif self.bshModelRadioButt.getSelect() == True:
			self.modelType = 'bsh'
		self.focusDeptPath = '%s/%s/%s' %(self.assetDict[self.getTSLSelection('assetTSL')], 'model', self.modelType)



	def setModelTypeRadioButt(self):
		val = self.modelRadioButt.getSelect()		
		self.animModelRadioButt.setEnable(val)
		self.proxyModelRadioButt.setEnable(val)
		self.renderModelRadioButt.setEnable(val)
		self.bshModelRadioButt.setEnable(val)



	def selectIncSavedFile(self):
		if self.MODE == 'asset':
			tsl = self.assetWorkTSL
		else:
			tsl = self.filmWorkTSL

		if self.focusFile in tsl.getAllItems():
			tsl.setSelectItem(self.focusFile)



	def selectAssetDept(self):
		self.focusAsset = self.getTSLSelection('assetTSL')
		if self.PROJECT_LOADED == False or not self.focusAsset:
			return
		self.setModelTypeRadioButt()
		if self.modelRadioButt.getSelect() == True:
			self.dept = 'model'
			self.deptPath = {'hero':'', 'work':'work'}
		elif self.rigRadioButt.getSelect() == True:
			self.dept = 'rig'
			self.deptPath = {'hero':'maya', 'work':'maya/work'}
		elif self.uvRadioButt.getSelect() == True:
			self.dept = 'uv'
			self.deptPath = {'hero':'maya', 'work':'maya/work'}
		elif self.shadeRadioButt.getSelect() == True:
			self.dept = 'shade'
			self.deptPath = {'hero':'maya', 'work':'maya/work'}
		elif self.devRadioButt.getSelect() == True:
			self.dept = 'dev'
			self.deptPath = {'hero':'maya', 'work':'maya/wip'}		
		elif self.refRadioButt.getSelect() == True:
			self.dept = 'ref'
			self.deptPath = {'hero':'', 'work':''}

		if self.dept == 'model':
			self.focusDeptPath = '%s/%s/%s' %(self.assetDict[self.focusAsset], self.dept, self.modelType)
		else:
			self.focusDeptPath = '%s/%s' %(self.assetDict[self.focusAsset], self.dept)



	def selectAssetDeptRadioButt(self):
		if self.dept == 'model':
			self.modelRadioButt.setSelect(True)
			if self.modelType == 'anim':
				self.animModelRadioButt.setSelect(True)
			elif self.modelType == 'proxy':
				self.proxyModelRadioButt.setSelect(True)
			elif self.modelType == 'render':
				self.renderModelRadioButt.setSelect(True)
			elif self.modelType == 'bsh':
				self.bshModelRadioButt.setSelect(True)
		elif self.dept == 'rig':
			self.rigRadioButt.setSelect(True) 
		elif self.dept == 'uv':
			self.uvRadioButt.setSelect(True) 
		elif self.dept == 'shade':
			self.shadeRadioButt.setSelect(True) 
		elif self.dept == 'dev':
			self.devRadioButt.setSelect(True) 		
		elif self.dept == 'ref':
			self.refRadioButt.setSelect(True) 



	def selectFilmDeptRadioButt(self):
		if self.dept == 'anim':
			self.animRadioButt.setSelect(True)
		elif self.dept == 'animClean':
			self.animCleanRadioButt.setSelect(True) 
		elif self.dept == 'layout':
			self.layoutRadioButt.setSelect(True) 
		elif self.dept == 'fx':
			self.fxRadioButt.setSelect(True) 
		elif self.dept == 'light':
			self.lightRadioButt.setSelect(True) 		



	def openFile(self):
		if not os.path.exists(self.focusFilePath):
			return False

		if not os.path.isfile(self.focusFilePath):
			return False

		result = ''
		if pm.cmds.file(q=True, anyModified=True) == True:
			result = pm.confirmDialog( title='Confirm Open File', 
				message='Scene has been modified',
				button=['Open', 'Cancel'], defaultButton='Open', cancelButton='Cancel', dismissString='Cancel' )
		
		if result == 'Cancel':
			return None
		else:
			pm.openFile(self.focusFilePath, f=True)
			return True
			


	def openExplorer(self):
		currentPath = self.pathTextFld.getText()
		currentPath = misc.convertOsPath(currentPath)

		if os.path.exists(currentPath) == False:
			return

		if os.path.isfile(currentPath) == True:
			toOpen = os.path.dirname(currentPath)
		else:
			toOpen = currentPath 

		try:
			subprocess.Popen(r'explorer "%s"' %toOpen)
		except:
			return



	def removeRef(self):
		if self.ref:
			for r in self.ref:
				r.remove()
			self.ref = []


	def resetProjVar(self):
		self.projPath = ''		
		self.projCode = ''
		self.epCode = ''
		self.currentEp = ''

		self.assetPath = ''
		self.filmPath = ''

	def resetFocusVar(self):
		# asset
		self.focusAsset = ''
		self.focusFile = ''
		self.focusAssetPath = ''
		self.focusDeptPath = ''
		self.focusWorkPath = ''
		self.focusHeroPath = ''
		self.focusFilePath = ''

		# film
		self.filmEpPath = ''
		self.shotPath = ''
		self.currentSeq = ''
		self.currentShot = ''
		self.seqs = []
		self.shots = []



	def resetPathTxtFld(self, txt=''):
		if not txt:
			txt = self.defaultAssetPathText

		self.pathTextFld.setBackgroundColor([0.3,0,0])
		self.pathTextFld.setText(txt)
		self.dirExists = False



	def setProject(self):
		self.PROJECT_LOADED = False
		self.currentProject = self.projMenu.getValue()
		
		if self.currentProject == 'N/A':
			self.assetWorkTSL.removeAll()
			self.assetHeroTSL.removeAll()
			self.assetTSL.removeAll()
			return

	

	def getAssetPath(self):
		self.assetPath = ''
		self.projPath = ''
		self.assetEpDict = {}

		projInfo = self.projInfoDict[self.currentProject] # get a list of [projPath, {ep:subtype} ]
		self.projPath = projInfo[0]
		self.assetEpDict = projInfo[1]

		self.epMenu.clear()
		for ep in sorted(self.assetEpDict.keys()):
			pm.menuItem(l=ep, p=self.epMenu)



	def getProjCode(self):
		self.projCode = ''
		try:
			dirProj = self.projPath.split('/')[-1]
			project = info.project(dirProj)
			self.projCode = project.code()

		except:
			om.MGlobal.displayWarning('Cannot get projCode. Project Navigator might not function correctly.')



	def setAssetEp(self):
		self.currentEp = self.epMenu.getValue()

		epDir = ''
		if self.currentEp != '':
			epDir = '/%s' %self.currentEp

		self.assetPath = '%s/%s%s' %(self.projPath, self.ASSETPATH, epDir)
		self.aSubType = self.assetEpDict[self.currentEp]



	def getFilterChecked(self):
		self.assetTypeFiltered = []

		if self.fAllChkBox.getValue() == True:
			self.assetTypeFiltered = [f for f in os.listdir(self.assetPath) if os.path.isdir('%s/%s' %(self.assetPath, f))==True and f not in self.assetTypeFilter]

		else:
			filterChkBox = [self.fCharacterChkBox, self.fPropChkBox, self.fSetChkBox, self.fSetDressChkBox, 
							self.fVehicleChkBox, self.fWeaponChkBox, self.fEnvironmentChkBox, self.fClothingChkBox,
							self.fMiscChkBox]	
			for chkBox in [c for c in filterChkBox if c.getValue() == True]:
				self.assetTypeFiltered.append(chkBox.getLabel())

		self.PROJECT_LOADED = True
		


	def updateAssetTSL(self):
		sortedAssets = sorted(self.assetDict.iteritems())
		self.assetDictFiltered = {}
		searchFor = self.searchTxtFld.getText()
		self.assetTSL.removeAll()

		if self.aSubType == True:
			subTypes = self.subtypes
			if self.subTypeFiltered:
				subTypes = self.subTypeFiltered
			for name, path in sortedAssets:
				for st in subTypes:
					if '/%s/%s' %(st, name.split(' (')[0]) in path:
						if searchFor:
							if searchFor not in name:
								break
						self.assetTSL.append(name)
						self.assetDictFiltered[name] = path
						break

		else:
			for name, path in sortedAssets:
				if searchFor:
					if searchFor not in name:
						continue
				self.assetTSL.append(name)
				self.assetDictFiltered[name] = path
	


	def updateAssetWorkTSL(self):

		self.assetHeroTSL.removeAll()
		self.assetWorkTSL.removeAll()


		if self.deptPath['hero']:
			self.focusHeroPath = '%s/%s' %(self.focusDeptPath, self.deptPath['hero'])
		else:
			self.focusHeroPath = self.focusDeptPath
		if self.deptPath['work']:
			self.focusWorkPath = '%s/%s' %(self.focusDeptPath, self.deptPath['work'])
		else:
			self.focusWorkPath = ''	

		if self.backupMode == True:
			self.focusHeroPath = '%s/_backup' %(self.focusHeroPath)	
			self.focusWorkPath = '%s/_backup' %(self.focusWorkPath)

		if os.path.exists(self.focusHeroPath) == True:
			h = [self.assetHeroTSL.append(f) for f in os.listdir(self.focusHeroPath) if os.path.isfile(os.path.join(self.focusHeroPath,f)) ]

		if os.path.exists(self.focusWorkPath) == True:
			w = [self.assetWorkTSL.append(f) for f in os.listdir(self.focusWorkPath) if os.path.isfile(os.path.join(self.focusWorkPath,f)) ]

	def updateAssetPathTxtFld(self, op):
		if self.PROJECT_LOADED == False:
			return
		toset = ''
		self.focusFilePath = ''


		if op == 'selectAssetTSL':	
			selAsset = self.getTSLSelection('assetTSL')
			if not selAsset:
				return
			self.focusAssetPath = self.assetDict[selAsset]
			if self.currentMode == 'hero':
		 		toset = self.focusHeroPath
		 	else:
		 		toset = self.focusWorkPath

		elif op == 'selectDept':
			toset = self.focusDeptPath

		elif op == 'selectWorkHero':
			if self.currentMode == 'hero':
		 		toset = self.focusHeroPath
		 	else:
		 		toset = self.focusWorkPath

		elif op == 'selectFile':
			fileSel = self.getTSLSelection('assetWorkHeroTSL')

			if self.currentMode == 'hero':
				focusPath = self.focusHeroPath
			else:
				focusPath = self.focusWorkPath

			self.focusFilePath = '%s/%s' %(focusPath, fileSel)
			toset = self.focusFilePath


		if os.path.exists(toset):
			self.pathTextFld.setText(toset)
			self.enablePathTxtFld()
		else:
			self.resetPathTxtFld()