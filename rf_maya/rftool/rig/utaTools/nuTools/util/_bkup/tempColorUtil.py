import os
from collections import defaultdict
import subprocess

import maya.OpenMaya as om
import maya.OpenMayaRender as omr
import maya.OpenMayaFX as omfx
import pymel.core as pm

from tool.utils import entityInfo
reload(entityInfo)

from nuTools import misc
reload(misc)

RVIO_PATH = "O:/systemTool/convertor/Tweak/RV-3.12.20-32/bin/rvio.exe"

def fixFileTexturePath() :
	fileNodes = ['%s.fileTextureName' %node for node in mc.ls(type='file')]
	assetObj = Asset()
	assetObj.load()

	# check and create texture and texture/hi dir
	textureDir = assetObj.getDeptHeroDir(job='texture', check=False)
	if not os.path.exists(textureDir):
		os.makedirs(textureDir)
	textureHiDir = assetObj.getTextureHeroResDir(res='hi', check=False)
	if not os.path.exists(textureHiDir):
		os.makedirs(textureHiDir)

	getAttr = mc.getAttr
	pathExists = os.path.exists
	popen = subprocess.Popen
	des = os.path.normpath(textureHiDir)
	# i = 0
	# progress bar
	gMainProgressBar = mel.eval('$tmp = $gMainProgressBar');
	mc.progressBar(gMainProgressBar, e=True, beginProgress=True, 
				isInterruptable=True, status='Moving texture files : %s' %textureHiDir, maxValue=len(fileNodes) )

	for fileNode in fileNodes:
		if mc.progressBar(gMainProgressBar, query=True, isCancelled=True ):
			break

		source = getAttr(fileNode)
		src = os.path.normpath(source)
		fileExt = os.path.basename(src)
		if pathExists(src):
			cmd = 'xcopy %s %s /k/i/y' %(src, des)
			xcopy = popen(cmd, shell=True)
			xcopy.wait()
			newFilePath = '%s/%s' % (textureHiDir, fileExt)
			mc.setAttr(fileNode, newFilePath, type='string')
			# print 'Moved(%s/%s)\n\t%s\n\t%s\\%s' %(i, filenum, src, des, fileExt)
		else:
			om.MGlobal.displayError('Texture does not exists : %s' %src)

		mc.progressBar(gMainProgressBar, edit=True, step=1)
	mc.progressBar(gMainProgressBar, edit=True, endProgress=True)


def getAssignedShader(obj):
	''' get shader assigned to obj '''
	shp = obj.getShape(ni=True)
	if shp:
		sgs = list(set(pm.listConnections(shp, d=True, type='shadingEngine')))
		if sgs:
			if len(sgs)>1:
				om.MGlobal.displayWarning('The object has more than one shadingEngine assigned to it, will use first one: %s' %obj.nodeName())
			shaders = sgs[0].surfaceShader.inputs()
			if shaders:
				return shaders[0]

def getColorFileNode(shader, fileNodeKw='_col'):
	''' get file node that point to image withe the fileNode kw in it '''

	# get color file node
	fileNodeHis = [n for n in pm.listHistory(shader) 
				if n.nodeType()=='file' and
				fileNodeKw in os.path.basename(n.fileTextureName.get())]

	if fileNodeHis:
		return fileNodeHis[0]

def getTextureNodes(shader, fileNodeKw='_col'):
	''' get file node that point to image withe the fileNode kw in it '''

	# get color file node
	texNodeHis = [n for n in pm.listHistory(shader) 
				if n.nodeType() in pm.listNodeTypes('texture')]

	if texNodeHis:
		return texNodeHis[0]

def getColorPerVertex(obj, fileNode):
	colors = []
	currentUvSet = obj.getCurrentUVSetName()
	# fileNode = fileNodeHis[0]

	attrs = [a for a in fileNode.outColor.outputs(p=True)]

	mSel = om.MSelectionList()
	mSel.add(obj.getShape(ni=True).longName())
	mSel.add(attrs[0].nodeName())
	mSel.add(attrs[0].name())

	# obj
	objMDagPath = om.MDagPath()
	mSel.getDagPath(0, objMDagPath) 
	objMDagPath.extendToShape()
	objMfnMesh = om.MFnMesh(objMDagPath)

	# shader
	shaderMObj = om.MObject()
	mSel.getDependNode(1, shaderMObj)

	# plug
	texPlug = om.MPlug()
	mSel.getPlug(2, texPlug)
	texAttr = texPlug.attribute()


	vertIt = om.MItMeshVertex(objMDagPath)
	uCoords = om.MDoubleArray()
	vCoords = om.MDoubleArray()
	uvUtil = om.MScriptUtil()

	# local var for speed
	uvUtil_createFromList = uvUtil.createFromList
	uvUtil_asFloat2Ptr = uvUtil.asFloat2Ptr
	vertIt_getUV = vertIt.getUV
	om_MScriptUtil_getFloat2ArrayItem = om.MScriptUtil.getFloat2ArrayItem
	uCoords_append = uCoords.append
	vCoords_append = vCoords.append
	while not vertIt.isDone():
		uvUtil_createFromList([0.0,0.0], 2)
		uvPoint = uvUtil_asFloat2Ptr()

		vertIt_getUV(uvPoint, currentUvSet)
		
		u = om_MScriptUtil_getFloat2ArrayItem(uvPoint, 0, 0)
		v = om_MScriptUtil_getFloat2ArrayItem(uvPoint, 0, 1)
		uCoords_append(u)
		vCoords_append(v)
		
		vertIt.next()

	resultColors = om.MVectorArray()
	resultAlphas = om.MDoubleArray()
	if omfx.MDynamicsUtil.hasValidDynamics2dTexture(shaderMObj, texAttr):
		omfx.MDynamicsUtil.evalDynamics2dTexture(shaderMObj, 
												texAttr, 
												uCoords, 
												vCoords, 
												resultColors, 
												resultAlphas)

		
		colors_append = colors.append
		for i in xrange(resultColors.length()):
			color = [resultColors[i].x, resultColors[i].y, resultColors[i].z]
			colors_append(color)

	return colors

def setVertexColors_api(obj, colors, indices=None):
	#colors = [om.MColor([c[0]*0.80, c[1]*0.80, c[2]*0.80]) for c in colors]
	selectionList = om.MSelectionList()
	selectionList.add(obj)
	nodeDagPath = om.MDagPath()
	selectionList.getDagPath(0, nodeDagPath)
	mfnMesh = om.MFnMesh(nodeDagPath)
	if not indices:
		numVert = mfnMesh.numVertices()
		vertUtil = om.MScriptUtil()
		indices = om.MIntArray()
		vertUtil.createIntArrayFromList(range(numVert), indices)

	mfnMesh.setVertexColors(colors, indices, None)

def convertListToMColorArray(colors):
	mColArray = om.MColorArray()
	mColArray_append = mColArray.append
	for c in colors:
		mCol = om.MColor(c[0], c[1], c[2])
		mColArray_append(mCol)
	return mColArray

def createPreviewShader_file(obj, shader, fileNode, previewSize=0.125):
	info = entityInfo.info()
	texturePath = info.texturePath(task=True)
	if not os.path.exists(texturePath):
		om.MGlobal.displayError('Current scene is not in pipeline.')
		return

	currTexPath = os.path.normpath(fileNode.fileTextureName.get()).replace('\\', '/')
	if not os.path.exists(currTexPath):
		om.MGlobal.displayError('Current texture path does not exists: %s' %currTexPath)
		return
	fileName = os.path.basename(currTexPath)
	fn, ext = os.path.splitext(fileName)

	previewResPath = '%s/preview' %texturePath
	if not os.path.exists(previewResPath):
		os.makedirs(previewResPath)
	previewTexPath = '%s/%s.jpg' %(previewResPath, fn)

	# convert to preview resolution
	cmd = '"%s"' %RVIO_PATH
	cmd += ' "%s"' %(currTexPath)
	cmd += ' -scale %s -quality 1.0' %(previewSize)
	cmd += ' -o "%s"' %(previewTexPath)
	# print cmd
	subprocess.Popen(cmd, shell=True)


	name = fileNode.nodeName().split(':')[-1]
	tmpLambert = pm.shadingNode('lambert', asShader=True, n='%s_tmpShd' %(name))
	tmpFile = pm.shadingNode('file', asTexture=True, n='%s_file' %(name))
	tmpFile.fileTextureName.set(previewTexPath)
	pm.connectAttr(tmpFile.outColor, tmpLambert.color)

	# print obj, shader, fileNode
	# set default color
	colors = getColorPerVertex(obj, fileNode)
	# average the list
	avgRgb = [sum(col)/len(col) for col in zip(*colors)]
	tmpFile.defaultColor.set(avgRgb)

	return tmpLambert

def createPreviewShader(obj, shader, fileNode=None):
	tmpLambert = pm.shadingNode('lambert', asShader=True, n='%s_tmpShd' %(shader.nodeName().split(':')[-1]))
	if not fileNode:
		# om.MGlobal.displayWarning('%s: cannot find file node, using shader outColor.' %obj.nodeName())
		avgRgb = shader.outColor.get()
	else:
		colors = getColorPerVertex(obj, fileNode)
		# average the list
		avgRgb = [sum(col)/len(col) for col in zip(*colors)]

	tmpLambert.color.set(avgRgb)

	return tmpLambert

def sampleShaderToImage(sampleAttr, outputPath, widthHeight=512, filterSize=1e-04):
	filePath, ext = os.path.splitext(outputPath)
	if not filePath or not ext or not os.path.exists(os.path.dirname(outputPath)):
		om.MGlobal.displayError('Invalid output path: %s' %outputPath)
		return

	depth = 4
	uCoords = om.MFloatArray()
	vCoords = om.MFloatArray()
	filterSizes = om.MFloatArray()

	u = 0.0
	v = 0.0
	inc = 1.0/widthHeight
	for ui in xrange(widthHeight):
		u = inc*ui
		for vi in xrange(widthHeight):
			v = inc*vi
			uCoords.append(u)
			vCoords.append(v)
			filterSizes.append(filterSize)

	# these are the arguments required to sample a 3D texture:
	numSamples = widthHeight*widthHeight

	# we don't need to set these arguments
	useShadowMaps = False
	reuseMaps = False
	cameraMatrix = om.MFloatMatrix()
	points = None

	normals = None
	refPoints =None
	tangentUs = None
	tangentVs = None
	if not filterSize:
		filterSizes = None

	# create the return arguments
	resultColors = om.MFloatVectorArray()
	resultTransparencies = om.MFloatVectorArray()

	# and this is the call to sample the points
	omr.MRenderUtil.sampleShadingNetwork(sampleAttr, 
		numSamples, 
		
		useShadowMaps, 
		reuseMaps, 
		cameraMatrix, 
		points,
		
		vCoords, 
		uCoords, 
		
		
		normals, 
		refPoints, 
		tangentUs, 
		tangentVs, 
		filterSizes,
		 
		resultColors, 
		resultTransparencies)

	# and return the sampled colors as a list
	colorArray = []
	# sampledTranspacenyArray = []
	for i in xrange(resultColors.length()):
		resultColVector = om.MFloatVector(resultColors[i])
		colorArray.append(resultColVector.x * 255)
		colorArray.append(resultColVector.y * 255)
		colorArray.append(resultColVector.z * 255)

		# alpha
		resultTransVector = om.MFloatVector(resultTransparencies[i])
		tranValue = 1.0 - ((resultTransVector.x + resultTransVector.y + resultTransVector.z)/3)
		colorArray.append(tranValue*255)

	outMImage = om.MImage()
	outMImage.create(widthHeight, widthHeight, depth, om.MImage.kByte)

	util = om.MScriptUtil()
	util.createFromList(colorArray, (widthHeight*widthHeight*depth))

	outMImage.setPixels(util.asUcharPtr(), widthHeight, widthHeight)
	outMImage.writeToFile(outputPath, ext[1:])

def convertTextureToVertexColors(source=None, destination=None, fileNodeKw='_col'):
	if not source or not destination:
		objs = misc.getSel(num=2)
		if len(objs) != 2:
			om.MGlobal.displayError('Invalid selection: please select source and desitnation transform')
			return
		source = objs[0]
		destination = objs[1]

	shader = getAssignedShader(source)
	if not shader:
		om.MGlobal.displayWarning('Skipping %s: cannot find shader.' %source.nodeName())
		return 
	fileNode = getColorFileNode(shader, fileNodeKw)
	if not fileNode:
		om.MGlobal.displayWarning('Skipping %s: cannot find file node.' %source.nodeName())
		return 

	colors = getColorPerVertex(source, fileNode)
	if colors:
		mCols = convertListToMColorArray(colors)
		destinationShp = destination.getShape(ni=True)
		setVertexColors_api(obj=destinationShp.longName(), colors=mCols, indices=None)
		pm.polyColorPerVertex(destination, cdo=True)
		# assign default shader
		pm.sets(pm.PyNode('initialShadingGroup'), e=True, nw=True, fe=destinationShp)

def convertAllToPreviewShader_file(matchDict, texturePreview, widthHeight='512'):
	''' 
	matchDict = {animObj : shadingObj, ...} 
	texturePreview = [shadingObjToPreview, ...]

	'''

	convertedShaders = {}  # {source: crated shader}
	toAssignDict = defaultdict(list)  # {created shader : [shp, ...]}
	for destination, source in matchDict.iteritems():
		# print destination, source
		shader = getAssignedShader(source)
		if not shader:
			om.MGlobal.displayError('Cannot find shader: %s' %source.nodeName())
			continue
		# print source
		desShp = destination.getShape(ni=True)
		if not desShp:
			om.MGlobal.displayError('has no shape node: %s' %destination.nodeName())
			continue

		# first time converting this shader
		if shader not in convertedShaders:
			fileNode = getColorFileNode(shader)
			if not fileNode:
				textureNodes = getTextureNodes(shader)
				# om.MGlobal.displayError('Cannot find file node for shader: %s' %(shader.nodeName()))
				# continue
				tempLambert = createPreviewShader(source, shader, fileNode=textureNodes)
			else:
				tempLambert = createPreviewShader_file(source, shader, fileNode, previewSize=0.125)

			convertedShaders[shader] = tempLambert
		else:  # this shader already been converted
			tempLambert = convertedShaders[shader]

		toAssignDict[tempLambert].append(desShp)

	for shader, destinations in toAssignDict.iteritems():
		sg = pm.sets(renderable=True, noSurfaceShader=True, empty=True, n='%sSG' %shader.nodeName())
		pm.connectAttr(shader.outColor, sg.surfaceShader)
		for des in destinations:
			pm.sets(sg, e=True, nw=True, fe=des)
			om.MGlobal.displayInfo('CONVERTED: %s ---> %s' %(shader, des))		
