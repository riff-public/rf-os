import pymel.core as pm
import maya.OpenMaya as om
from functools import partial 

class TempPivot(object):
	def __init__(self, ctrl=None):
		if not ctrl:
			try:
				ctrl = pm.selected()[0]
			except:
				pass
			if not ctrl:
				om.MGlobal.displayError('Select a control object.')

		self.ctrl = ctrl
		self.loc1 = None
		self.loc2 = None
		self.parCons = None
		self.mainId = None
		self.create()

	def create(self):
		self.loc1 = pm.spaceLocator()
		# set rotate order
		# self.loc1.rotateOrder.set(self.ctrl.rotateOrder.get())

		# lock attrs
		self.loc1.scale.lock()
		self.loc1.visibility.lock()
		
		# snap loc1 to ctrl
		pm.delete(pm.parentConstraint(self.ctrl, self.loc1))
		
		pm.select(self.loc1, r=True)

	def connect(self):
		self.loc2 = pm.spaceLocator()
		self.loc2.rotateOrder.set(self.ctrl.rotateOrder.get())
		pm.parent(self.loc2, self.loc1)
		self.parCons = pm.parentConstraint(self.ctrl, self.loc2)
		self.loc2.visibility.set(False)
		
		# create script job
		self.mainId = pm.scriptJob(cu=True, ac=[self.loc1.rotate, partial(self.moveCtrl)])
		pm.select(self.loc1, r=True)
		self.loc1.translate.lock()
		# kill job
		pm.scriptJob(ro=True, event=['SelectionChanged', partial(self.killJob)])

	def killJob(self):
		pm.scriptJob(kill=self.mainId, force=True)
		pm.delete(self.loc1)
		pm.select(self.ctrl, r=True)

	def moveCtrl(self):
		consAttr = [a for a in self.parCons.listAttr(ud=True) if a.name().endswith('W0')][0]
		consAttr.set(0)
		locTrans = pm.xform(self.loc2, q=True, ws=True, t=True)
		locRots = pm.xform(self.loc2, q=True, ws=True, ro=True)

		try:
			pm.xform(self.ctrl, ws=True, t=locTrans)
		except:
			om.MGlobal.displayWarning('Cannot set translate value on : %s' %(self.ctrl.nodeName()))

		try:
			pm.xform(self.ctrl, ws=True, ro=locRots)
		except:
			om.MGlobal.displayWarning('Cannot set rotate value on : %s' %(self.ctrl.nodeName()))

		consAttr.set(1)
