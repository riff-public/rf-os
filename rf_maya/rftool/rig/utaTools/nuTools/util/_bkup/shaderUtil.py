import os
from collections import defaultdict
import  colorsys
import math

import maya.OpenMaya as om
import maya.OpenMayaRender as omr
import pymel.core as pm

from nuTools import misc
reload(misc)

PREVIEW_RESOLUTION = 512
AVG_RESOLUTION = 8
DEFAULT_FILTER_SIZE = 0  # 1e-05
TEXTURE_EXT = 'png'
TEXURE_SUFFIX = '_col'

COL_TOLERANCE = 10
TRANS_TOLERANCE = 10

TINT_FACTOR = 0.125  # scale factor for value of color
TRANS_FACTOR = 0.667  # sacle factor for value of transparency
SAT_FACTOR = 0.15  # scale factor for saturation of color


def getSG(objShp):
	sgs = [sg for sg in (set(pm.listConnections(objShp, d=True, type='shadingEngine')))
			if sg.nodeName() != 'initialShadingGroup']  # exclude the deafult SG
	return sgs


def getAssignedShader(objShp, getPlug=False, getSE=False):
	''' get shader assigned to obj '''
	sgs = getSG(objShp)
	if sgs:
		if len(sgs)>1:
			om.MGlobal.displayWarning('The object has more than one shadingEngine assigned to it, \
				will use first one: %s' %objShp.nodeName())
		
		inputArgs = {}
		if getPlug == True:
			inputArgs = {'p':True}
		shaders = sgs[0].surfaceShader.inputs(**inputArgs)
		if shaders:
			return shaders[0], sgs[0]

def getIncrementName(name):
	exit = False
	i = 1
	while not exit:
		incName = '%s%s' %(name, str(i))
		lamberts = pm.ls('%s_tmpShd' %incName, type='lambert')
		if not lamberts:
			return incName
		i += 1

def createTexturePreviewShader(texturePath, rgbList=None, transList=None, name='obj'):
	name = getIncrementName(name)
	tmpLambert = pm.shadingNode('lambert', asShader=True, n='%s_tmpShd' %(name))
	tmpFile = pm.shadingNode('file', asTexture=True, n='%s_file' %(name))
	tmpFile.fileTextureName.set(texturePath)
	pm.connectAttr(tmpFile.outColor, tmpLambert.color)

	if transList:
		avgTrans = averageRGBs(transList)
		if [t for t in avgTrans if t > 0.0]:
			pm.connectAttr(tmpFile.outTransparency, tmpLambert.transparency)
	
	# average the list, set default color for the lambert
	if rgbList:
		avgRgb = averageRGBs(rgbList)
		avgRgb = [avgRgb[0]/255, avgRgb[1]/255, avgRgb[2]/255]
		tmpFile.defaultColor.set(avgRgb)

	return tmpLambert


def averageRGBs(rgbList):
	avgRgb = [sum(col)/len(col) for col in zip(*rgbList)]

	return avgRgb


def createAveragePreviewShader(avgRgb, avgTrans, name='obj'):
	name = getIncrementName(name)
	tmpLambert = pm.shadingNode('lambert', asShader=True, n='%s_tmpShd' %(name))

	# normalize from 255 to 1 first
	avgRgb = [avgRgb[0]/255, avgRgb[1]/255, avgRgb[2]/255]
	tmpLambert.color.set(avgRgb) 
	tmpLambert.transparency.set([avgTrans[0]/255, avgTrans[1]/255, avgTrans[2]/255])

	return tmpLambert


def sampleShader_uv(sampleAttr, widthHeight, uCoords, vCoords):
	# these are the arguments required to sample a 3D texture:
	numSamples = widthHeight*widthHeight

	# we don't need to set these arguments
	useShadowMaps = False
	reuseMaps = False
	cameraMatrix = om.MFloatMatrix()
	points = None

	normals = None
	refPoints =None
	tangentUs = None
	tangentVs = None
	filterSizes = None

	# create the return arguments
	resultColors = om.MFloatVectorArray()
	resultTransparencies = om.MFloatVectorArray()

	# and this is the call to sample the points
	omr.MRenderUtil.sampleShadingNetwork(sampleAttr, 
		numSamples, 
		useShadowMaps, 
		reuseMaps, 
		cameraMatrix, 
		points,
		
		uCoords, 
		vCoords, 

		normals, 
		refPoints, 
		tangentUs, 
		tangentVs, 
		filterSizes,
		 
		resultColors, 
		resultTransparencies)

	# and return the sampled colors as a list
	rgbs = []
	trans = []
	# sampledTranspacenyArray = []
	for i in xrange(resultColors.length()):
		# --- result from sample
		resultColVector = resultColors[i]
		resTransVec = resultTransparencies[i]

		# --- rgb list
		hsv = colorsys.rgb_to_hsv(resultColVector.x, 
								resultColVector.y, 
								resultColVector.z)
		
		# modify the color
		hsv = [hsv[0], 
			hsv[1] + ((1.0 - hsv[1])*SAT_FACTOR),
			hsv[2] + ((1.0 - hsv[2])*TINT_FACTOR)]

		rgb = colorsys.hsv_to_rgb(hsv[0], hsv[1], hsv[2])
		rgb = [rgb[0]*255, rgb[1]*255, rgb[2]*255]
		rgbs.append(rgb)

		# --- transparency list
		if resTransVec.x > 0 or resTransVec.y > 0 or resTransVec.z > 0:
			tr = resTransVec.x + ((1.0 - resTransVec.x) * TRANS_FACTOR)
			tg = resTransVec.y + ((1.0 - resTransVec.y) * TRANS_FACTOR)
			tb = resTransVec.z + ((1.0 - resTransVec.z) * TRANS_FACTOR)
		else:
			tr = resTransVec.x
			tg = resTransVec.y
			tb = resTransVec.z

		trans.append([tr * 255, 
					tg * 255, 
					tb * 255])

	return rgbs, trans


def sampleShader_full(sampleAttr, widthHeight, filterSize=DEFAULT_FILTER_SIZE):
	numSamples = widthHeight*widthHeight

	uCoords = om.MFloatArray()
	vCoords = om.MFloatArray()
	filterSizes = om.MFloatArray()

	uMin = 0.0
	vMin = 0.0
	uMax = 1.0
	vMax = 1.0

	uLen = uMax - uMin
	vLen = vMax - vMin

	incU = uLen/(widthHeight-1)
	incV = vLen/(widthHeight-1)

	for vi in xrange(widthHeight):
		v = vMin + (incV*vi)
		for ui in xrange(widthHeight):
			u = uMin + (incU*ui)
			
			uCoords.append(u)
			vCoords.append(v)

			filterSizes.append(filterSize)

	# these are the arguments required to sample a 3D texture:
	useShadowMaps = False
	reuseMaps = False
	cameraMatrix = om.MFloatMatrix()
	points = None

	normals = None
	refPoints =None
	tangentUs = None
	tangentVs = None
	if filterSize == 0:
		filterSizes = None

	# create the return arguments
	resultColors = om.MFloatVectorArray()
	resultTransparencies = om.MFloatVectorArray()

	# and this is the call to sample the points
	omr.MRenderUtil.sampleShadingNetwork(sampleAttr, 
		numSamples, 
		useShadowMaps, 
		reuseMaps, 
		cameraMatrix, 
		points,
		
		uCoords, 
		vCoords, 

		normals, 
		refPoints, 
		tangentUs, 
		tangentVs, 
		filterSizes,
		 
		resultColors, 
		resultTransparencies)

	# and return the sampled colors as a list
	rgba = []
	for i in xrange(resultColors.length()):
		# --- result from sample
		resultColVector = resultColors[i]
		resTransVec = resultTransparencies[i]

		# --- flat color
		rgba.append(resultColVector[0]*255)
		rgba.append(resultColVector[1]*255)
		rgba.append(resultColVector[2]*255)

		# average and invert the value to translate from transparency to alpha
		alphaValue = 1.0 - ((resTransVec.x + resTransVec.y + resTransVec.z)/3)
		rgba.append(alphaValue * 255)

	return rgba


def getUvSamplePointInBoundingBox(objShp, uvSetName, widthHeight):
	mSel = om.MSelectionList()
	mSel.add(objShp)

	# obj
	objMDagPath = om.MDagPath()
	mSel.getDagPath(0, objMDagPath) 
	objMDagPath.extendToShape()
	objMfnMesh = om.MFnMesh(objMDagPath)

	numUv = objMfnMesh.numUVs()

	meshUCoords = om.MFloatArray()
	meshVCoords = om.MFloatArray()
	meshUvCoords = []

	# get all UVs
	objMfnMesh.getUVs(meshUCoords, meshVCoords, uvSetName)

	# get uv bounding box
	uMin, vMin, uMax, vMax = 1.0, 1.0, 0.0, 0.0
	for i in xrange(numUv):
		# u
		if meshUCoords[i] < uMin:
			uMin = meshUCoords[i]
		if meshUCoords[i] > uMax:
			uMax = meshUCoords[i]
		# v
		if meshVCoords[i] < vMin:
			vMin = meshVCoords[i]
		if meshVCoords[i] > vMax:
			vMax = meshVCoords[i]

		meshUvCoords.append((meshUCoords[i], meshVCoords[i]))

	minUv = (uMin, vMin)
	minUv = (uMax, vMax)

	uLen = uMax - uMin
	vLen = vMax - vMin

	incU = uLen/(widthHeight-1)
	incV = vLen/(widthHeight-1)

	sampleUCoords = om.MFloatArray()
	sampleVCoords = om.MFloatArray()
	sampleUVCoords = []
	for vi in xrange(widthHeight):
		v = vMin + (incV*vi)
		for ui in xrange(widthHeight):
			u = uMin + (incU*ui)

			closestDist = 1000
			cu, cv = 0, 0
			for mu, mv in meshUvCoords:
				dist = misc.get2dDistance(a=(u, v), b=(mu, mv))
				if dist < closestDist:
					closestDist = dist
					cu = mu
					cv = mv

			sampleUCoords.append(cu)
			sampleVCoords.append(cv)
			sampleUVCoords.append((round(cu, 2), round(cv, 2)))

	return sampleUCoords, sampleVCoords, sampleUVCoords

def writeSquareImage(rgbaFlatList, outputPath, widthHeight, depth=4):
	filePath, ext = os.path.splitext(outputPath)
	if not filePath or not ext or not os.path.exists(os.path.dirname(outputPath)):
		om.MGlobal.displayError('Invalid output path: %s' %outputPath)
		return

	if len(rgbaFlatList) != (widthHeight*widthHeight*depth):
		om.MGlobal.displayError('Invalid size of rgbaFlatList != widthHeight*widthHeight*depth')
		return

	outMImage = om.MImage()
	outMImage.create(widthHeight, widthHeight, depth, om.MImage.kByte)

	util = om.MScriptUtil()
	util.createFromList(rgbaFlatList, (widthHeight*widthHeight*depth))
	outMImage.setPixels(util.asUcharPtr(), widthHeight, widthHeight)
	
	outMImage.writeToFile(outputPath, ext[1:])  # skip the '.' 

	return outputPath


def convertAllToPreviewShader(matchDict, 
							doTexture, 
							doAverage,
							textureDir='',
							textureExt=TEXTURE_EXT,
							previewResolution=PREVIEW_RESOLUTION, 
							averageResolution=AVG_RESOLUTION,
							filterSize=DEFAULT_FILTER_SIZE):
	''' 
	matchDict = {animObj : shadingObj, ...} 
	doTexture = [shadingObjToDoPreviewTexture, ...]
	doAverage = [shadingObjToDoAverageColor]

	'''
	if doTexture and not os.path.exists(textureDir):
		om.MGlobal.displayError('Preview texture directory does not exist: %s' %textureDir)
		return
	if not isinstance(doTexture, (list, tuple)):
		om.MGlobal.displayError('Parameter doTexture must provide list of objects to convert to preview texture shader.')
		return
	if not isinstance(doAverage, (list, tuple)):
		om.MGlobal.displayError('Parameter doAverage must provide list of objects to convert to average color shader.')
		return
	if not doTexture and not doAverage:
		om.MGlobal.displayError('Parameter doTexture and doAverage can not be an empty list.')
		return

	# create ambient light
	amLightShp = pm.shadingNode('ambientLight', asLight=True, n='tempBakeAmbient_lightShape')

	texShaders = {}  # {source: crated shader}
	avgColors = {}  # {tranDist: [color, tran]}
	avgBBs = defaultdict(dict)  # {shader: {uvStr: tempShader, ...}}
	toAssignDict = defaultdict(list)  # {created shader : [shp, ...]}

	for destination, source in matchDict.iteritems():
		srcName = source.nodeName()
		srcShp = source.getShape(ni=True)
		if not srcShp:
			om.MGlobal.displayError('Cannot find source shape: %s' %srcName)
			continue

		srcUvSetName = srcShp.getCurrentUVSetName()
		desName = destination.nodeName()
		desShp = destination.getShape(ni=True)
		if not desShp:
			om.MGlobal.displayError('Cannot find destination shape: %s' %desName)
			continue

		shader, sg = getAssignedShader(srcShp, getPlug=False, getSE=True)
		if not shader:
			om.MGlobal.displayError('Cannot find source shader: %s' %srcName)
			continue

		shaderName = shader.nodeName().split(':')[-1]  # get rid of namespace
		sgName = sg.nodeName()

		tempLambert = None
		uSam, vSam, rUVSam = getUvSamplePointInBoundingBox(srcShp.longName(), 
												uvSetName=srcUvSetName,
												widthHeight=averageResolution)

		# has to convert UV points to string to use as dict key
		# uvStr = '(u1,v1),...,(un,vn)'
		uvStr = ','.join(['(%s,%s)' %(us, vs) for us, vs in rUVSam])

		if source in doTexture:
			# first time converting this shader
			if shader not in texShaders:
				print 'do texture : using SG %s' %sgName
				rgba = sampleShader_full(sampleAttr=sgName, 
										widthHeight=previewResolution, 
										filterSize=filterSize)
				rgbs, trans = sampleShader_uv(sampleAttr=sgName, 
											widthHeight=averageResolution, 
											uCoords=uSam,
											vCoords=vSam)
					
				# write to file
				outputPath = '%s/%s.%s' %(textureDir, shaderName, textureExt)
				texturePath = writeSquareImage(rgba, outputPath, widthHeight=previewResolution)

				# create temp lambert
				tempLambert = createTexturePreviewShader(texturePath, rgbList=rgbs, name=shaderName)

				texShaders[shader] = tempLambert
			else:  # this shader already been converted
				tempLambert = texShaders[shader]

		elif source in doAverage:  # calculate average color

			# check if the shader has been converted and if uv points is the same as object before it
			if shader in avgBBs and uvStr in avgBBs[shader]:
				tempLambert = avgBBs[shader][uvStr]
				print 'average: reusing shader %s' %tempLambert.nodeName()
			else:
				rgbs, trans = sampleShader_uv(sampleAttr=sgName, 
											widthHeight=averageResolution, 
											uCoords=uSam,
											vCoords=vSam)

				avgRgb = averageRGBs(rgbs)
				avgTrans = averageRGBs(trans)
				colTrs = [avgRgb, avgTrans]

				# loop to compare with existing colors first
				for createdLambert, ct in avgColors.iteritems():
					exCol = ct[0]
					exTrs = ct[1]

					colorDist = misc.getDistanceFromPosition(exCol, avgRgb)
					tranDist = misc.getDistanceFromPosition(exTrs, avgTrans)

					# if color and transparency is roughly the same as colors we had before
					if colorDist < COL_TOLERANCE and tranDist < TRANS_TOLERANCE:
						tempLambert = createdLambert
						print 'average : reusing shader %s' %tempLambert.nodeName()
						break
				else:
					print 'average : using SG %s' %sgName
					# create new lambert set to average rgb values
					tempLambert = createAveragePreviewShader(avgRgb=avgRgb,
															avgTrans=avgTrans, 
															name=shaderName)
					# store this new color and lambert
					avgColors[tempLambert] = colTrs
	
					avgBBs[shader][uvStr] = tempLambert

		if tempLambert:
			toAssignDict[tempLambert].append(desShp)

	for shader, destinations in toAssignDict.iteritems():
		sg = pm.sets(renderable=True, noSurfaceShader=True, empty=True, n='%sSG' %shader.nodeName())
		pm.connectAttr(shader.outColor, sg.surfaceShader)
		pm.sets(sg, e=True, nw=True, fe=destinations)	
	
	pm.delete(amLightShp)