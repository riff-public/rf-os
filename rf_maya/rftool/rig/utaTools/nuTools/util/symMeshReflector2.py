import maya.OpenMaya as om
# from functools import partial
import collections
import maya.cmds as mc

# import time

DEFAULT_TOL = 0.00001
DEFAULT_EMPTY_TEXT = '-'

class SymMeshReflectors(object):
	def __init__(self, slotNum=5):
		#UI vars
		self.WINDOW_TITLE = 'Sym Mesh Reflector v.2.0'
		self.WINDOW_NAME = 'smrWin'

		self.slotNum = slotNum

		self.objs = [None] * slotNum
		self.statuses = [False] * slotNum
		self.focusIndx = 0

	def UI(self):
		"""
		The maya.cmds UI function. 

		"""
		if mc.window(self.WINDOW_NAME, ex=True):
			mc.deleteUI(self.WINDOW_NAME, window=True)

		mc.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, s=False, mnb=True, mxb=False)
		mc.columnLayout(adj=True, rs=0, co=['both', 0])
		mc.frameLayout(label='by Nuternativ : nuttynew@hotmail.com', fn='smallObliqueLabelFont', mh=5, mw=5)

		mc.rowColumnLayout(nc=2, co=([1, 'left', 100], [2, 'left', 5]))
		mc.text(l='tolerance')
		self.tolFloatField = mc.floatField(w=58, v=DEFAULT_TOL, pre=6, min=0.0, cc=lambda *args: self.changeTol())
		mc.setParent('..')

		mc.columnLayout(adj=True, rs=5, co=['both', 20])
		self.statusTxt = mc.text(l='No Base Mesh Data.', bgc=[0.4, 0, 0], h=25)
		self.slotTsl = mc.textScrollList(w=100, h=(14*self.slotNum), nr=self.slotNum, allowMultiSelection=False, 
					sc=lambda *args: self.changeSlot())
		mc.setParent('..')

		mc.columnLayout(adj=True, rs=5, co=['both', 20])
		mc.button(l='Get Base Mesh Data', c=lambda *args: self.assignSlot(), h=35)
		mc.setParent('..')

		mc.rowColumnLayout(nc=3, co=([1, 'left', 23], [2, 'left', 10], [3, 'left', 10]))
		mc.button(l='Check Sym', h=20, w=100, c=lambda *args: self.checkSymmetry())
		mc.button(l='Sel Moved', h=20, w=70, c=lambda *args: self.getMovedVtx())
		mc.button(l='Clear Slot', c=lambda *args: self.clearSlot())
		mc.setParent('..')

		mc.frameLayout( label='Operations', mh=5, mw=5, cll=False, cl=False)

		mc.rowColumnLayout(nc=3, co=([1, 'left', 0], [2, 'left', 3], [3, 'left', 3]))
		self.revertFloatFld = mc.floatField(v=100, max=100, min=0, pre=2, w=42, cc=lambda *args: self.fillRevertMesh())
		self.revertSlider = mc.floatSliderGrp(v=100, max=100, min=0, cw2=[170, 10], f=False, pre=2, el='%',
						dc=lambda *args: self.dragRevertMesh(), cc=lambda *args: self.endDragRevertMesh())
		mc.button(l='Revert', c=lambda *args: self.revertToBase())
		mc.setParent('..')


		mc.rowColumnLayout(nc=3, rs=([1, 5]), co=([1, 'left', 0], [2, 'left', 20], [3, 'left', 20]))
		mc.button(l='M >>', w=80, h=30, c=lambda *args: self.mirror_neg())
		mc.button(l='Flip', w=80, h=30, c=lambda *args: self.flip())
		mc.button(l='<< M', w=80, h=30, c=lambda *args: self.mirror_pos())
		mc.button(l='Subtract', w=80, h=25, c=lambda *args: self.subtractFromMeshA())
		mc.button(l='Copy n Flip', w=80, h=25, c=lambda *args: self.copyAndFlip())
		mc.button(l='Add', w=80, h=25, c=lambda *args: self.addFromMeshA())
		mc.setParent('..')

		mc.rowColumnLayout(nc=3, co=([1, 'left', 0], [2, 'left', 3], [3, 'left', 11]))
		self.copyFloatFld = mc.floatField(v=0, max=100, min=0, pre=2, w=42, cc=lambda *args: self.fillCopyMesh())
		self.copySlider = mc.floatSliderGrp(v=0, max=100, min=0, cw2=[170, 10], f=False, pre=2, el='%',
						dc=lambda *args: self.dragCopyMesh(), cc=lambda *args: self.endDragCopyMesh())
		mc.button(l='Copy', c=lambda *args: self.copyFromMeshA())
		mc.setParent('..')

		mc.setParent('..')

		mc.showWindow()

		self.initSlot()

	def initSlot(self):
		mc.textScrollList(self.slotTsl, e=True, ra=True)
		for i in xrange(self.slotNum):
			mc.textScrollList(self.slotTsl, e=True, append=DEFAULT_EMPTY_TEXT)

		mc.textScrollList(self.slotTsl, e=True, selectIndexedItem=1)

	def copyAndFlip(self):
		smrObj = self.getFocusObj()
		if smrObj:
			sels = mc.ls(sl=True, l=True)
			smrObj.caller('copyFromMeshA')
			for mesh in smrObj.focusVtx:
				mc.select(mesh, r=True)
				smrObj.caller('flip')
			mc.select(sels, r=True)

	def changeTol(self):
		value = mc.floatField(self.tolFloatField, q=True, v=True)
		smrObj = self.objs[self.focusIndx]
		if smrObj:
			smrObj.changeTol(value=value)

	def getFocusIndx(self):
		 # get ui slot selection
		self.focusIndx = mc.textScrollList(self.slotTsl, q=True, selectIndexedItem=True)[0] - 1

	def assignSlot(self):
		self.getFocusIndx()
		value = mc.floatField(self.tolFloatField, q=True, v=True)
		smrObj = SymMeshReflector(tol=value)
		self.objs[self.focusIndx] = smrObj


		smrObj.caller('analyzeBaseGeo')
		if not smrObj. baseMesh:
			return

		meshLn = smrObj.baseMesh
		meshSn = meshLn.split('|')[-1]

		tslIndx = (self.focusIndx+1)
		mc.textScrollList(self.slotTsl, e=True, rii=tslIndx)
		mc.textScrollList(self.slotTsl, e=True, ap=(tslIndx, meshSn))
		mc.textScrollList(self.slotTsl, e=True, selectIndexedItem=tslIndx)

		if smrObj.baseMesh and smrObj.baseMeshData:
			self.statuses[self.focusIndx] = True
			self.setStatusTxt(True)
		else:
			self.statuses[self.focusIndx] = False
			self.setStatusTxt(False)

	def clearSlot(self):
		self.getFocusIndx()
		self.objs[self.focusIndx] = None

		tslIndx = (self.focusIndx+1)
		mc.textScrollList(self.slotTsl, e=True, rii=tslIndx)
		mc.textScrollList(self.slotTsl, e=True, ap=(tslIndx, DEFAULT_EMPTY_TEXT))
		mc.textScrollList(self.slotTsl, e=True, selectIndexedItem=tslIndx)

		self.statuses[self.focusIndx] = False
		self.setStatusTxt(False)

	def getFocusObj(self):
		smrObj = self.objs[self.focusIndx]
		return smrObj

	def setStatusTxt(self, status):
		if status == True:
			mc.text(self.statusTxt, e=True, bgc=([0, 0.4, 0]), l='Loaded.')
		else:
			mc.text(self.statusTxt, e=True, bgc=([0.4, 0, 0]), l='No Base Mesh Data.')

	def changeSlot(self):
		self.getFocusIndx()
		smrObj = self.getFocusObj()

		self.setStatusTxt(self.statuses[self.focusIndx])
		if smrObj:
			mc.floatField(self.tolFloatField, e=True, v=smrObj.tol)

	def getMovedVtx(self):
		smrObj = self.getFocusObj()
		if smrObj:
			smrObj.caller('getMovedVtx')

	def checkSymmetry(self):
		smrObj = self.getFocusObj()
		if smrObj:
			smrObj.caller('checkSymmetry')

	def revertToBase(self):
		smrObj = self.getFocusObj()
		if smrObj:
			smrObj.caller('revertToBase')

	def mirror_neg(self):
		smrObj = self.getFocusObj()
		if smrObj:
			smrObj.caller('mirror_neg')

	def flip(self):
		smrObj = self.getFocusObj()
		if smrObj:
			smrObj.caller('flip')

	def mirror_pos(self):
		smrObj = self.getFocusObj()
		if smrObj:
			smrObj.caller('mirror_pos')

	def subtractFromMeshA(self):
		smrObj = self.getFocusObj()
		if smrObj:
			smrObj.caller('subtractFromMeshA')

	def copyFromMeshA(self):
		smrObj = self.getFocusObj()
		if smrObj:
			smrObj.caller('copyFromMeshA')

	def addFromMeshA(self):
		smrObj = self.getFocusObj()
		if smrObj:
			smrObj.caller('addFromMeshA')

	def fillRevertMesh(self):
		smrObj = self.getFocusObj()
		if smrObj:
			percent = mc.floatField(self.revertFloatFld, q=True, value=True)
			smrObj.fillRevertMesh(percent=percent)
		mc.floatField(self.revertFloatFld, e=True, value=100)

	def dragRevertMesh(self):
		smrObj = self.getFocusObj()
		if smrObj:
			percent = mc.floatSliderGrp(self.revertSlider, q=True, value=True)
			smrObj.dragRevertMesh(percent=percent)
			 #set the float field
			mc.floatField(self.revertFloatFld, e=True, value=percent)

	def endDragRevertMesh(self, *args):
		smrObj = self.getFocusObj()
		if smrObj:
			smrObj.endDragRevertMesh()
		#set the float field and float slider back to 100
		mc.floatSliderGrp(self.revertSlider, e=True, value=100)   
		mc.floatField(self.revertFloatFld, e=True, value=100)
		
	def dragCopyMesh(self):
		smrObj = self.getFocusObj()
		if smrObj:
			percent = mc.floatSliderGrp(self.copySlider, q=True, value=True)

			smrObj.dragCopyMesh(percent=percent)
			 #set the float field
			mc.floatField(self.copyFloatFld, e=True, value=percent)

	def fillCopyMesh(self):
		smrObj = self.getFocusObj()
		if smrObj:
			percent = mc.floatField(self.copyFloatFld, q=True, value=True)
			smrObj.fillCopyMesh(percent=percent)
		mc.floatField(self.copyFloatFld, e=True, value=0)

	def endDragCopyMesh(self, *args):
		smrObj = self.getFocusObj()
		if smrObj:
			smrObj.endDragCopyMesh()

		#set the float field and float slider back to 0
		mc.floatSliderGrp(self.copySlider, e=True, value=0)   
		mc.floatField(self.copyFloatFld, e=True, value=0)


class SymMeshReflector(object):
	"""
	Tool for mirroring mesh modification on one side to the other.
	By given a symmetry mesh once, the script can manipulate another geometry with the same 
	topology. 

	"""

	def __init__(self, tol=0.001):

		# global vars
		self.tol = tol
		self.baseVtxNum = 0
		self.revertSliderDragging = False
		self.copySliderDragging = False

		# base mesh vars
		self.baseMesh = None
		self.baseMeshData = []
		self.meshCenter = []
		
		self.noneIndx = []
		self.filterIndx = {'pos':[], 'neg':[]}

		self.midDict = {}
		self.pairDict = {}

		# multiple action vars
		self.focusVtx = collections.defaultdict(list)  #{mesh:[vtx1, vtx2, ...]}
		self.focusIndx = collections.defaultdict(list)  #{mesh:[indx1, indx2, ...]}
		self.currTrans = collections.defaultdict(list)
		self.relTrans = collections.defaultdict(list)

		# meshA vars
		self.meshA = None
		self.meshAData = []
		self.meshACenter = []

		self.filterAIndx = {'pos':[], 'neg':[]}
		self.noneAIndx = []
		self.midAPosIndx = []
		self.midANegIndx = []

		self.midADict = {}
		self.pairADict = {}

		# meshA action vars
		self.movedAVtxs = []
		self.movedAIndxs = []
		self.meshAFocusVtxs = []
		self.meshAFocusIndxs = []

		# ui
		self.statusTxt = None

	def addFromMeshA(self):
		"""
		Add self.meshA modifications to each items in self.focusVtx.

		"""
		mc.undoInfo(ock=True)
		for mesh in self.focusVtx:
			#get all vtx current position
			currTrans = self.getCurrOsTrans(mesh)

			for v, i in zip(self.focusVtx[mesh], self.focusIndx[mesh]):
				if self.checkEqualPoint(self.meshAData[i], self.baseMeshData[i]) == True:
					continue

				relTrans = self.getRelativeTrans(self.meshAData[i], self.baseMeshData[i])
				mc.move(currTrans[i][0]+relTrans[0], currTrans[i][1]+relTrans[1], currTrans[i][2]+relTrans[2], v, ls=True)
		mc.undoInfo(cck=True)

	def getGeoData(self, mesh=None):
		"""
		Get all vertices position for self.mesh (no mirroring or pairing data)

		"""
		if not mesh:
			try: 
				mesh = mc.ls(sl=True, l=True)[0]
			except:
				return

		self.baseMeshData = []

		#iterate thru each mesh 
		mfnMesh = self.getMFnMesh(obj=mesh)
		pArray = om.MPointArray()   
		mfnMesh.getPoints(pArray, om.MSpace.kObject)

		for i in xrange(0, pArray.length()):
			self.baseMeshData.append([pArray[i].x, pArray[i].y, pArray[i].z])

	def analyzeBaseGeo(self, mesh=None):
		"""
		Analyze a mesh to be used as base geometry. Collect all data, pair vtxs, mids, etc.. 

		"""
		#reset base mesh data
		self.baseMesh = None
		self.baseMeshData = []
		posIndx, negIndx, self.noneIndx = [], [], []
		self.filterIndx = {'pos':[], 'neg':[]}
		self.meshCenter = []
		self.pairDict = {}
		self.midDict = {}

		if not mesh:
			try: 
				mesh = mc.ls(sl=True, l=True)[0]
			except:
				om.MGlobal.displayWarning('Please select base mesh.')
				return 

		self.baseMesh = mesh

		#iterate thru each mesh 
		try:
			mfnMesh = self.getMFnMesh(obj=self.baseMesh)
		except:
			return
		
		if not mfnMesh:
			return

		pWArray = om.MPointArray()   
		mfnMesh.getPoints(pWArray, om.MSpace.kWorld)
		pLArray = om.MPointArray()   
		mfnMesh.getPoints(pLArray, om.MSpace.kObject)
		pos, neg = [], []

		#finding the mesh center
		self.meshCenter = mc.objectCenter(self.baseMesh, l=True)
		# self.meshCenter = mc.xform(self.baseMesh, q=True, rotatePivot=True)

		#for each vertex, store all in orig and get those in the middle
		self.baseVtxNum = pWArray.length()
		for i in xrange(0, self.baseVtxNum):
			self.baseMeshData.append([pLArray[i].x, pLArray[i].y, pLArray[i].z])
			self.midDict[i] = False

			#found that it is in the mid
			if abs(pWArray[i].x - self.meshCenter[0]) <= self.tol:
				self.filterIndx['pos'].append(i)
				self.filterIndx['neg'].append(i)
				self.midDict[i] = True
				continue

			if pWArray[i].x > self.meshCenter[0]:
				pos.append(i)
			else:
				neg.append(i)

		#matching left and right vertex
		for p in pos:
			mirrorPos = om.MPoint((pLArray[p].x*-1), pLArray[p].y, pLArray[p].z)
			for n in neg:
				if pLArray[n].distanceTo(mirrorPos) <= self.tol:
					self.filterIndx['neg'].append(n)
					self.filterIndx['pos'].append(p)
					self.pairDict[p] = n
					self.pairDict[n] = p
					neg.pop(neg.index(n))
					break
			else:
				self.noneIndx.append(p)

		#add negative verts that'was left unmatched to none    
		self.noneIndx.extend(neg)

		if self.noneIndx:
			nonSymVtxs = self.getVtxFromIndx(self.baseMesh, self.noneIndx)
			mc.select(nonSymVtxs, r=True)
			om.MGlobal.displayWarning('Mesh is NOT symmetrical. Not all vertex can be mirrored.')
		else:
			mc.select(self.baseMesh, r=True)
			om.MGlobal.displayInfo('Mesh is symmetrical.')

	def analyzeAGeo(self, mesh=None):
		"""
		Analyze a mesh to be used as self.meshA. Collect all data, pair vtxs, mids, etc.. 
		Warn user if the mesh is not all symmetry. Called by 'check symmetry' action.

		"""
		if not mesh:
			try: 
				mesh = mc.sl(sl=True, l=True)[0]
			except:
				return 

		#reset data
		self.meshAData = []
		posAIndx, negAIndx, midAIndx = [], [], []
		self.noneAIndx = []
		self.filterAIndx = {'pos':[], 'neg':[]}
		self.meshACenter = []
		self.pairADict = {}
		self.midADict = {}

		#iterate thru each mesh 
		try:
			mfnMesh = self.getMFnMesh(obj=mesh)
		except:
			return

		if not mfnMesh:
			return

		pWArray = om.MPointArray()   
		mfnMesh.getPoints(pWArray, om.MSpace.kWorld)
		pLArray = om.MPointArray()   
		mfnMesh.getPoints(pLArray, om.MSpace.kObject)
		pos, neg = [], []

		#finding the mesh center
		meshAData = mc.listRelatives(mesh, p=True, f=True)[0]
		self.meshACenter = mc.objectCenter(mesh, l=True)

		#for each vertex, store all in orig and get those in the middle
		vtxNum = pWArray.length()

		for i in xrange(0, vtxNum):
			self.meshAData.append([pLArray[i].x, pLArray[i].y, pLArray[i].z])
			self.midADict[i] = False

			#found that it is in the mid
			if abs(pWArray[i].x - self.meshACenter[0]) <= self.tol:
				self.filterAIndx['pos'].append(i)
				self.filterAIndx['neg'].append(i)
				self.midADict[i] = True
				continue

			if pWArray[i].x > self.meshACenter[0]:
				pos.append(i)
			else:
				neg.append(i)

		#matching left and right vertex
		for p in pos:
			find = om.MPoint((pWArray[p].x*-1)+(2*self.meshACenter[0]), pWArray[p].y, pWArray[p].z)
			for n in neg:
				if pWArray[n].distanceTo(find) <= self.tol:
					self.filterAIndx['neg'].append(n)
					self.filterAIndx['pos'].append(p)
					self.pairADict[p] = n
					self.pairADict[n] = p
					neg.pop(neg.index(n))
					break
			else:
				self.noneAIndx.append(p)


		#add negative verts that'was left unmatched to none    
		self.noneAIndx.extend(neg)
		if self.noneAIndx:
			nonSymVtxs = self.getVtxFromIndx(mesh, self.noneAIndx)
			mc.select(nonSymVtxs, r=True)
			om.MGlobal.displayWarning('Mesh is NOT symmetrical. Not all vertex can be mirrored.')
		else:
			mc.select(mesh, r=True)
			om.MGlobal.displayInfo('Mesh is symmetrical.')

	def caller(self, op, sels=[]):
		"""
		Caller function for all the actions.

		"""
		mc.waitCursor(state=True)

		if op in ['mirror_pos', 'mirror_neg']:
			if self.getUserSelection(sels) == True:
				self.mirrorVtx(filterIndxList=self.filterIndx[op.split('_')[-1]])
			else:
				om.MGlobal.displayError('Mirror : Select a mesh to mirror.')

		elif op == 'revertToBase':
			if self.getUserSelection(sels) == True:
				self.revertVtxToBase()

		elif op == 'flip':
			if self.getUserSelection(sels) == True:
				self.flipVtx(filterIndxList=self.filterIndx['pos'])

		elif op == 'flipApi':
			if self.getUserSelection(sels) == True:
				self.flipVtxApi()

		elif op == 'copyFromMeshA':
			if self.getMeshASelection(child=True) == True:
				self.getAGeoData(mesh=self.meshA)
				self.copyFromMeshA()
			else:
				om.MGlobal.displayError('Copy : Select the source mesh, then destination meshes with the same vertex count.')

		elif op == 'addFromMeshA':
			if self.getMeshASelection(child=True) == True:
				self.getAGeoData(mesh=self.meshA)
				self.addFromMeshA()
			else:
				om.MGlobal.displayError('Add : Select the source mesh, then destination meshes with the same vertex count.')

		elif op == 'subtractFromMeshA':
			if self.getMeshASelection(child=True) == True:
				self.getAGeoData(mesh=self.meshA)
				self.subtractFromMeshA()
			else:
				om.MGlobal.displayError('Subtract : Select the source mesh, then destination meshes with the same vertex count.')

		elif op == 'checkSymmetry':
			self.getMeshASelection(child=False)
			self.analyzeAGeo(mesh=self.meshA)

		elif op == 'getMovedVtx':
			if self.getUserSelection(sels) == True:
				self.getMovedVtx(sel=True)

		elif op == 'analyzeBaseGeo':
			self.analyzeBaseGeo()
		
		mc.waitCursor(state=False)

	def copyFromMeshA(self):
		"""
		Copy self.meshA modifications to each items in self.focusVtx.

		"""
		mc.undoInfo(ock=True)
		for mesh, vertices in self.focusVtx.iteritems():
			# get all vtx current position
			currTrans = self.getCurrOsTrans(mesh)
			for v, i in zip(vertices, self.focusIndx[mesh]):
				if self.checkEqualPoint(currTrans[i], self.meshAData[i]) == True:
					continue
				mc.move(self.meshAData[i][0], self.meshAData[i][1], self.meshAData[i][2], v, ls=True)
		mc.undoInfo(cck=True)

	def checkSymPoint(self, pointA, pointB):
		"""
		Accept 2 point positions(A and B). Calculate if pointB is the mirror of point A under the tolerance.
		Return: boolean

		"""
		mPointA = om.MPoint(pointA[0], pointA[1], pointA[2])
		mPointB = om.MPoint(pointB[0], pointB[1], pointB[2])
		mirMPoint = om.MPoint((pointA[0]*-1) + (2*self.meshCenter[0]), pointA[1], pointA[2])
		return mPointB.distanceTo(mirMPoint) <= self.tol

	def checkEqualPoint(self, pointA, pointB):
		"""
		Accept 2 points position, see if pointA is equal to point B under the tolerance.
		Return: boolean

		"""
		mPointA = om.MPoint(pointA[0], pointA[1], pointA[2])
		mPointB = om.MPoint(pointB[0], pointB[1], pointB[2])
		return mPointA.distanceTo(mPointB) <= self.tol

	def changeTol(self, value):
		print value
		self.tol = value

	def fillRevertMesh(self, percent):
		if self.getUserSelection() == True:
			self.getAGeoData(self.meshA)
			self.getRelTransToBase()

			self.revertVtxByPercent(src=self.baseMeshData, percent=percent * 0.01)
			self.endDragRevertMesh()

	def endDragRevertMesh(self, *args):
		if self.revertSliderDragging == True:        
			mc.undoInfo(cck=True)

		self.revertSliderDragging = False

	def getRelTransToBase(self):
		self.relTrans = {}
		for mesh, vertices in self.focusVtx.iteritems():
			currTrans = self.getCurrOsTrans(mesh)
			rels = {}
			for v, i in zip(vertices, self.focusIndx[mesh]):
				r = self.getRelativeTrans(currTrans[i], self.baseMeshData[i])
				if self.checkEqualPoint(r, [0.0, 0.0, 0.0]) == True:
					continue
				else:
					rels[i] = r
			self.relTrans[mesh] = rels

	def dragRevertMesh(self, percent):
		if self.revertSliderDragging == False:
			if self.getUserSelection() == True:
				self.getAGeoData(self.meshA)
				self.getRelTransToBase()
				mc.undoInfo(ock=True)

				self.revertSliderDragging = True

		if self.revertSliderDragging == True:
			self.revertVtxByPercent(src=self.baseMeshData, percent=percent*0.01)
	
	def revertVtxByPercent(self, src, percent):
		"""
		Revert self.meshA(one object) to base mesh by the percent given.

		"""	
		for mesh, rels in self.relTrans.iteritems():
			for i, r in rels.iteritems():
				v = '%s.vtx[%s]' %(mesh, i)
				mc.move(src[i][0]+r[0]*percent, 
					src[i][1]+r[1]*percent, 
					src[i][2]+r[2]*percent, 
					v, ls=True)

	def copyVtxByPercent(self, orig, percent):
		"""
		Copy self.meshA(one object) to select mesh by the percent given.

		"""
		for mesh, rels in self.relTrans.iteritems():
			for i, r in rels.iteritems():
				v = '%s.vtx[%s]' %(mesh, i)
				mc.move(orig[mesh][i][0]+r[0]*percent, 
					orig[mesh][i][1]+r[1]*percent, 
					orig[mesh][i][2]+r[2]*percent, 
					v, ls=True)

	def getRelTransToMeshA(self):
		self.relTrans = {}
		for mesh, vertices in self.focusVtx.iteritems():
			self.currTrans[mesh] = self.getCurrOsTrans(mesh)
			rels = {}
			for v, i in zip(vertices, self.focusIndx[mesh]):
				r = self.getRelativeTrans(self.meshAData[i], self.currTrans[mesh][i])
				if self.checkEqualPoint(r, [0.0, 0.0, 0.0]) == True:
					continue
				else:
					rels[i] = r
			self.relTrans[mesh] = rels

	def dragCopyMesh(self, percent):
		if self.copySliderDragging == False:
			
			if self.getMeshASelection(child=True) == True:
				self.getAGeoData(self.meshA)
				self.getRelTransToMeshA()
				mc.undoInfo(ock=True)

				self.copySliderDragging = True

		if self.copySliderDragging == True:
			self.copyVtxByPercent(orig=self.currTrans, percent=percent*0.01)

	def fillCopyMesh(self, percent):
		if self.getMeshASelection(child=True) == True:
			self.getAGeoData(self.meshA)
			self.getRelTransToMeshA()

			self.copyVtxByPercent(orig=self.currTrans, percent=percent*0.01)
	 		self.endDragCopyMesh()

	def endDragCopyMesh(self, *args):
		if self.copySliderDragging == True:        
			mc.undoInfo(cck=True)

		self.copySliderDragging = False

	def filterSelection(self):
		"""
		Filter all focus var to be only those that has the same vertex count as self.baseMesh.

		"""
		res = True
		toDel = []
		for mesh in self.focusVtx:
			meshVtxNum = mc.polyEvaluate(mesh, v=True)
			if meshVtxNum != self.baseVtxNum:
				toDel.append(mesh)

		for mesh in toDel:
			del(self.focusVtx[mesh])
			del(self.focusIndx[mesh])


		if not self.focusIndx:
			res = False

		return res

	def flipVtx(self, filterIndxList):
		"""
		Flip each focus mesh.
		Args: filterIndexList : List of vertex that will be use as filter for user selection to prevent
								user from selecting vertex from both sides. 

		"""
		# startTime = time.time()
		for mesh in self.focusVtx:
			#get all vtx current position
			currTrans = self.getCurrOsTrans(mesh)

			#filter out vtx
			filterIndxs = list(set(self.focusIndx[mesh]).intersection(filterIndxList))
			filterVtxs = self.getVtxFromIndx(mesh, filterIndxs)

			for v, i in zip(filterVtxs, filterIndxs):

				#if it's in the middle of the mesh
				if self.midDict[i] == True:
					mc.move(currTrans[i][0]*-1, currTrans[i][1], currTrans[i][2], v, ls=True)

				#it's on the left or right side
				else:
					c = self.pairDict[i]
					# if both verts are already sym, skip to increse speed of moving stady point
					if self.checkSymPoint(currTrans[i], currTrans[c]) == True:
						continue

					cVtx = '%s.vtx[%i]' %(mesh, c)
					mc.move(currTrans[i][0]*-1, currTrans[i][1], currTrans[i][2], cVtx, ls=True)
					mc.move(currTrans[c][0]*-1, currTrans[c][1], currTrans[c][2], v, ls=True)

	def flipVtxApi(self):
		"""
		Flip each focus mesh.
		Args: filterIndexList : List of vertex that will be use as filter for user selection to prevent
								user from selecting vertex from both sides. 

		"""
		for mesh in self.focusVtx:
			# get MFnMesh 
			mFnMesh = self.getMFnMesh(mesh)
			dagPath = self.getDagPath(mesh)

			# get all points
			vtxPointArray = om.MPointArray()
			mFnMesh.getPoints(vtxPointArray, om.MSpace.kObject)      

			resultPointArray = om.MPointArray()
			geoIterator = om.MItGeometry(dagPath)

			cPos = {}
			while( not geoIterator.isDone()):
				pointPosition = geoIterator.position()
				
				vIndx = geoIterator.index()

				if self.midDict[vIndx] == True:
					pointPosition.x = pointPosition.x*-1
					
				else:
					c = self.pairDict[vIndx]
					
					if vIndx in cPos.keys():
						pointPosition.x = cPos[vIndx].x*-1
						pointPosition.y = cPos[vIndx].y 
						pointPosition.z = cPos[vIndx].z
					else:
						cPos[c] = vtxPointArray[vIndx]     
						pointPosition.x = vtxPointArray[c].x*-1
						pointPosition.y = vtxPointArray[c].y 
						pointPosition.z = vtxPointArray[c].z 

				resultPointArray.append(pointPosition)    
				geoIterator.next()

			geoIterator.setAllPositions(resultPointArray)

	def getAGeoData(self, mesh=None):
		"""
		Get all vertices position for self.meshA

		"""
		if not mesh:
			try: 
				mesh = mc.ls(sl=True, l=True)[0]
			except:
				return

		self.meshAData = []
		mfnMesh = self.getMFnMesh(obj=mesh)
		if not mfnMesh:
			return

		pArray = om.MPointArray()   
		mfnMesh.getPoints(pArray, om.MSpace.kObject)

		for i in xrange(0, pArray.length()):
			self.meshAData.append([pArray[i].x, pArray[i].y, pArray[i].z])

	def getUserSelection(self, sels=[]):
		"""
		Get user selection for multiple mesh operations (mirror, flip).
		Stores data to : 
		self.focusVtx       : mesh:[vtx1, vtx2, vtx3, ...]
		self.focusIndx      : mesh:[0, 1, 2, ...]
		self.focusVtxList   : [vtx1, vtx2, vtx3, ...] 
		self.focusIndxList  : [0, 1, 2, ...]

		"""
		self.resetFocusData()
		if not sels:
			sels = mc.filterExpand(fp=True, ex=True, sm=(31, 12))
			if not sels:
				return False

		for sel in sels:
			typ = mc.objectType(sel)
			if typ == 'transform':
				shps = mc.listRelatives(sel, f=True, ni=True, shapes=True, type='mesh')
				if shps:
					shp = shps[0]
					numVtx = mc.polyEvaluate(shp, v=True)
					numVtxRange = xrange(0, numVtx)
					vtxs = self.getVtxFromIndx(shp, numVtxRange)
					self.focusVtx[shp].extend(vtxs)
					self.focusIndx[shp].extend(numVtxRange)

			elif typ == 'mesh':
				if '.vtx' in sel:
					splits = sel.split('.vtx')
					mesh = splits[0]
					indx = int(splits[-1][1:-1])
					self.focusVtx[mesh].append(sel)
					self.focusIndx[mesh].append(indx)
				else:
					numVtx = mc.polyEvaluate(sel, v=True)
					numVtxRange = xrange(0, numVtx)
					vtxs = self.getVtxFromIndx(sel, numVtxRange)
					self.focusVtx[sel].extend(vtxs)
					self.focusIndx[sel].extend(numVtxRange)

		res = self.filterSelection()

		return res

	def getMovedVtxMeshA(self):
		"""
		Check if self.meshA vertices have moved from the base mesh and store those in self.moveAVtxs and self.moveAIndxs.

		"""
		# Clear the vars
		self.movedAVtxs = []
		self.movedAIndxs = []

		for v, i in zip(self.meshAFocusVtxs, self.meshAFocusIndxs):
			if self.checkEqualPoint(self.meshAData[i], self.baseMeshData[i]) == False:
				self.movedAVtxs.append(v)
				self.movedAIndxs.append(i)

	def getMovedVtx(self, sel=True):
		"""
		From focus data retrieved from function 'getUserSelection()'. Compare any of the vertices has moved from the original.
		Called by 'Select moved vertices' action.

		"""
		toSel = []
		for mesh in self.focusVtx:
			#get all vtx current position
			currTrans = self.getCurrOsTrans(mesh)

			for v, i in zip(self.focusVtx[mesh], self.focusIndx[mesh]):
				if self.checkEqualPoint(currTrans[i], self.baseMeshData[i]) == False:
					toSel.append(v)

		if sel == True:
			if toSel:
				mc.select(toSel, r=True)
			else:
				mc.select(cl=True)
				om.MGlobal.displayInfo('No vertex was modified.')

	def getVtxFromIndx(self, mesh, indxs):
		"""
		Accept mesh(str) and index(int) combine string to retrieve vertices name.
		Return: List

		"""
		return ['%s.vtx[%i]' %(mesh, i) for i in indxs]

	def getRelativeTrans(self, pointA, pointB):
		"""
		Accept 2 point positions(A and B). Subtract B from A to get relative transform.
		Return: list(position)

		"""
		return [pointA[0] - pointB[0], pointA[1] - pointB[1], pointA[2] - pointB[2]]

	def getCurrOsTrans(self, mesh):
		"""
		Given mesh name as string, will iterate through each vertex and get its position.
		Return: list([vtx1.x, vtx1.y, vtx1.z], [...], ...)

		"""
		mFnMesh = self.getMFnMesh(mesh)
		vtxPointArray = om.MPointArray()    
		mFnMesh.getPoints(vtxPointArray, om.MSpace.kObject)
			
		# return a list off all points positions
		return [[vtxPointArray[i][0], vtxPointArray[i][1], vtxPointArray[i][2]] for i in xrange(0, vtxPointArray.length())]

	def getMFnMesh(self, obj):
		"""
		Given mesh name as string, return mfnMesh.

		"""
		try:
			msl = om.MSelectionList()
			msl.add(obj)
			nodeDagPath = om.MDagPath()
			msl.getDagPath(0, nodeDagPath)
			return om.MFnMesh(nodeDagPath)
		except:
			return None

	def getDagPath(self, obj):
		"""
		Given mesh name as string, return mfnMesh.

		"""
		try:
			msl = om.MSelectionList()
			msl.add(obj)
			nodeDagPath = om.MDagPath()
			msl.getDagPath(0, nodeDagPath)
		except:
			return

		return nodeDagPath

	def getMeshASelection(self, sels=[], child=False):
		"""
		Get user selection for operation that has other mesh involve in operation (subtract, add, copy).
		Stores data to : 
		self.meshA               : mesh 
		self.meshAFocusVtx       : mesh:[vtx1, vtx2, vtx3, ...]
		self.meshAFocusIndx      : mesh:[0, 1, 2, ...]

		"""
		self.resetMeshAFocusData()

		if not sels:
			sels = mc.ls(sl=True, l=True, fl=True)

			if not sels:
				om.MGlobal.displayError('Select something!')
				return False

			if len(sels) <= child:
				return False

			if child == True:
				res = self.getUserSelection(sels=sels[1:])
				if res == False:
					self.resetFocusData()
					return res

		meshA = sels[0]

		if '.vtx' in meshA:
			splits = meshA.split('.vtx')
			self.meshA = splits[0]
			vtxNum = mc.polyEvaluate(self.meshA, v=True)
			for s in sels:
				splits = s.split('.vtx')
				if splits[0] == self.meshA:
					self.meshAFocusVtxs.append(s)
					self.meshAFocusIndxs.append(int(splits[-1][1:-1]))
		else:
			typ = mc.objectType(meshA)
			if typ == 'transform':
				shps = mc.listRelatives(meshA, ni=True, f=True, shapes=True, type='mesh')
				if shps:
					self.meshA = shps[0]
					vtxNum = mc.polyEvaluate(self.meshA, v=True)
					indxRange = xrange(0, vtxNum)
					self.meshAFocusVtxs = self.getVtxFromIndx(self.meshA, indxRange)
					self.meshAFocusIndxs = indxRange

			elif typ == 'mesh':
				self.meshA = meshA
				vtxNum = mc.polyEvaluate(self.meshA, v=True)
				indxRange = xrange(0, vtxNum)
				self.meshAFocusVtxs = self.getVtxFromIndx(self.meshA, indxRange)
				self.meshAFocusIndxs = indxRange

		if vtxNum != len(self.baseMeshData):
			return False

		return True

	def mirrorVtx(self, filterIndxList):
		"""
		Mirror each focus mesh to the other side.
		Args: filterIndexList : List of vertex that will be use as filter for user selection to prevent
								user from selecting vertex from both sides. 

		"""
		for mesh in self.focusVtx:
			#get all vtx current position
			currTrans = self.getCurrOsTrans(mesh)

			#filter out vtx
			filterIndxs = list(set(self.focusIndx[mesh]).intersection(filterIndxList))
			filterVtxs = self.getVtxFromIndx(mesh, filterIndxs)

			for v, i in zip(filterVtxs, filterIndxs):

				#if it's in the middle of the mesh
				if self.midDict[i] == True:
					mc.move(self.baseMeshData[i][0], currTrans[i][1], currTrans[i][2], v, ls=True)

				#it's on the left or right side
				else:
					c = self.pairDict[i]

					# if both verts are already sym, skip to increse speed of moving stady point
					if self.checkSymPoint(currTrans[i], currTrans[c]) == True:
						continue

					cVtx = '%s.vtx[%i]' %(mesh, c)
					mc.move(currTrans[i][0]*-1, currTrans[i][1], currTrans[i][2], cVtx, ls=True)

	def revertVtxToBase(self):
		"""
		Revert self.focusVtx(multiple objects) to base mesh.

		"""
		for mesh in self.focusVtx:
			#get all vtx current position
			currTrans = self.getCurrOsTrans(mesh)

			for v, i in zip(self.focusVtx[mesh], self.focusIndx[mesh]):
				if self.checkEqualPoint(currTrans[i], self.baseMeshData[i]) == True:
					continue
				mc.move(self.baseMeshData[i][0], self.baseMeshData[i][1], self.baseMeshData[i][2], v, ls=True)

	def resetFocusData(self):
		"""
		Reset all focus variables for multiple operations(mirror, flip).

		"""
		self.focusVtx = collections.defaultdict(list) 
		self.focusIndx = collections.defaultdict(list)
		# self.relTrans = collections.defaultdict(list)
		# self.currTrans = collections.defaultdict(list)

	def resetMeshAFocusData(self):
		"""
		Reset all focus variables for meshA operations(subtract, copy, add).

		"""
		self.movedAVtxs = []
		self.movedAIndxs = []
		self.meshAFocusVtxs = []
		self.meshAFocusIndxs = []
		self.meshA = None

	def subtractFromMeshA(self):
		"""
		Subtract self.meshA modifications from each items in self.focusVtx.

		"""
		mc.undoInfo(ock=True)
		for mesh in self.focusVtx.keys():
			#get all vtx current position
			currTrans = self.getCurrOsTrans(mesh)

			for v, i in zip(self.focusVtx[mesh], self.focusIndx[mesh]):
				if self.checkEqualPoint(self.meshAData[i], self.baseMeshData[i]) == True:
					continue
				relTrans = self.getRelativeTrans(self.meshAData[i], self.baseMeshData[i])
				mc.move(currTrans[i][0]-relTrans[0], currTrans[i][1]-relTrans[1], currTrans[i][2]-relTrans[2], v, ls=True)
		mc.undoInfo(cck=True)

	def setStatusTxt(self, status):
		if status == True:
			mc.text(self.statusTxt, e=True, bgc=([0, 0.4, 0]), l='Loaded.')
		else:
			mc.text(self.statusTxt, e=True, bgc=([0.4, 0, 0]), l='No Base Mesh Data.')



















