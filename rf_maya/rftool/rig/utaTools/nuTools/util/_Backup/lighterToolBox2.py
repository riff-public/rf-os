# maya modules
import maya.cmds as mc
import pymel.core as pm
import maya.mel as mel
import maya.OpenMaya as om

# tool modules
import nuTools.misc as misc

# check if VRay plugin is loaded?
pluginFileName = 'vrayformaya.mll'
if pm.pluginInfo(pluginFileName, q=True, l=True) == False:
	pm.loadPlugin(pluginFileName)


def getNodeVisible(node):
	'''
		Check the node and its parent(s) for visibility.
	'''

	if node.visibility.get() == False:
		return False

	parents = pm.listRelatives(node, allParents=True)

	if parents:
		for p in parents:
			if p.visibility.get() == False:
				return False

	return True


def getAllGeoInRef(ref, visibleOnly=True):
	nodes = ref.nodes()
	geo = []
	for n in [i for i in nodes if misc.checkIfPly(i)]:
		try:
			shp = n.getShape(ni=True)
			if visibleOnly == True:
				pass
			else:
				geo.append(n)
		except: pass

	return geo


def getRefFromObjects(objs=[]):
	refs = set()
	for obj in [o for o in objs if o.isReferenced()]:
		refPath = pm.referenceQuery(obj, f=True)
		ref = pm.FileReference(refPath)
		refs.add(ref)

	return refPaths 


def convertSelectionToAllGeoInRef():
	sels = pm.selected(type='transform')
	refs = getRefFromObjects(sels)

	geos = []
	for ref in refs:
		geo = getAllGeoInRef(ref)
		if geo:
			geos.extend(geo)
	if geos:
		pm.select(geos, r=True)


def setRenderAttrs(attr):
	sels = pm.selected(type='transform')
	value = not(misc.checkMod(check='ctrl'))

	for sel in sels:
		if sel.hasAttr(attr) == True:
			objAttr = sel.attr(attr)
			mc.setAttr(objAttr, value)

	om.MGlobal.displayInfo('%s : %s' %(attr, value))


def getMatInScene(matName):
	mat = None
	sgs = []

	try:
		mat = pm.PyNode(matName)
		sgs = mat.outColor.outputs(et=True, type='shadingEngine')
	except:
		pass

	if not mat or mat.type() != 'VRayMtl':
		mat = pm.shadingNode('VRayMtl', asShader=True, n=matName)
	
	if not sgs:
		sg = mc.sets(n='%sSG' %mat, r=True, nw=True)
		mc.connectAttr('%s.outColor' %mat, '%s.surfaceShader' %sg, f=True)
	else:
		sg = sgs[0]

	return mat, sg


def getShapes(objs):
	if not isinstance(objs, (list, set, tuple)):
		objs = [objs]

	shps = []
	for obj in objs:
		try:
			shp = obj.getShape(ni=True)
			shps.append(shp)
		except:
			pass

	return shps


def assignSimpleMat():
	matName = 'simple_mat'
	sels = pm.selected(type='transform')
	simpleMat, sg = getMatInScene(matName=matName)
	shps = getShapes(sels)

	if shps:
		pm.sets(sg, e=True, nw=True, fe=shps)	


def createVrayREMultiMatte():
	mmRe = mel.eval('vrayAddRenderElement "MultiMatteElement"')
	mmRe = pm.PyNode(mmRe)
	mmRe.rename('vrayRE_Id')
	mmRe.attr('vray_considerforaa_multimatte').set(1)
	mmRe.attr('vray_redid_multimatte').set(1)
	mmRe.attr('vray_greenid_multimatte').set(2)
	mmRe.attr('vray_blueid_multimatte').set(3)
	mmRe.attr('vray_usematid_multimatte').set(1)
	mmRe.attr('vray_name_multimatte').set('multimatte_Id')

def addVrayIdAttrs(mat):
	if not mat.hasAttr('vrayColorId'):
		mel.eval('vrayAddAttr %s vrayColorId' %mat)

	if not mat.hasAttr('vrayMaterialId'):
		mel.eval('vrayAddAttr %s vrayMaterialId' %mat)


def assignIdMat(idNum=1):
	idNames = {1: 'mainId', 2: 'secondaryId', 3: 'extraId'}
	color = [0.0, 0.0, 0.0]
	color[idNum] = 1.0

	matName = '%s_mat' %idNames[idNum]
	sels = pm.selected(type='transform')

	idMat, sg = getMatInScene(matName=matName)

	# set color
	idMat.diffuseColor.set(color)

	# add id attributes - material ID and multimatte ID
	addVrayIdAttrs(idMat)

	idMat.attr('vrayMaterialId').set(idNum)

	# create multiMatte
	if not pm.objExists('vrayRE_Id'):
		createVrayREMultiMatte()

	shps = getShapes(sels)
	if shps:
		pm.sets(sg, e=True, nw=True, fe=shps)	


def copyAssignedMat():
	sels = pm.selected(type='transform')
	if len(sels) < 2:
		om.MGlobal.displayError('Select at least 2 objects - (destnation(s), source)')
		return

	des = sels[:-1]
	src = sels[-1]

	# get src shape and SG
	try:
		srcShp = src.getShape(ni=True)
		sg = srcShp.attr('instObjGroups').outputs(et=True, type='shadingEngine')[0]
	except:
		om.MGlobal.displayError('Cannot get shape and SG for : %s' %src)
		return

	# get des shapes
	desShps = getShapes(des)
	if desShps:
		mc.sets(sg, e=True, nw=True, fe=desShps)	


def toggleAllCorneaVisibility():
	references = pm.listReferences()
	chaRefs = []
	value = not(misc.checkMod(check='ctrl'))

	for ref in references:
		try:
			path = ref.path
			if path.split('/')[4] == 'char':
				chaRefs.append(ref)
		except:
			pass

	for chaRef in chaRefs:
		geoInRef = getAllGeoInRef(chaRef, visibleOnly=False)
		corneaInRef = [geo for geo in geoInRef if 'cornea' in geo.nodeName() and pm.polyEvaluate(geo, v=True) in [242, 382]]
		if corneaInRef:
			for cornea in corneaInRef:
				try:
					cornea.getShape(ni=True).visibility.set(value)
				except:
					continue
		else:
			om.MGlobal.displayWarning('Cannot find cornea for ref node : %s' %chaRef)




