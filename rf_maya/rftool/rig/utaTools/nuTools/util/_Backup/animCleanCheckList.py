import pymel.core as pm 
import maya.OpenMaya as om
import maya.mel as mel
import re, sys, os, subprocess

from rigTool import ptTechAnim
reload(ptTechAnim) 

from tools import info
reload(info)

from nuTools import misc, config 
reload(misc)
reload(config)

import ptFilmPublish.pubFilmCore as pubFilmCore
reload(pubFilmCore)



class AnimCleanCheckList(object):
    
    def __init__(self):
        self.WINDOW_NAME = 'animCleanCheckListWin'
        self.WINDOW_TITLE = 'AnimClean Checklist v3.7'
        self.qtPath = 'C:\\Program Files (x86)\\QuickTime\\QuickTimePlayer.exe'
        self.RED = [0.5, 0, 0]
        self.GREEN = [0, 0.7, 0]
        self.sgStatusMap = {'N/A' : 'N/A', 'noaprv':'Sup - Approved', 'daily': 'Daily',
                            'aprv':'Client Approved', 'intapr': 'Waiting for Client', 
                            'wtg': 'Wait to Start', 'ip': 'In Progress', 'rdy':'Ready to Start',
                            'udt':'Need Update', 'rtfx':'Ready to Fix' , 'wfc':'Ready to Check', 
                            'cmpt':'Complete', 'hld':'On Hold'}

        self.statusIndx = 0      
        self.info = ''
        self.sgInfo = '' 
        self.filmPath = ''

        self.defaultRemoveObjTypes = 'polySoftEdge, displayLayer, objectSet, vraySettings'
        self.defaultSeqMannagerName = 'sequenceManager1'
        self.defaultSequencerName = 'sequencer1'
        self.defaultInfoTxt = 'N/A'
        self.defaultSgInfoTxt = 'N/A'


        self.nTempLayout = 'accN_tempLayout'
        self.oldDisplayApp = 'smoothShaded'
        self.wosDisplay = False
        self.cam = 'persp'

        self._df = True
        self._dim = True
        self._ca = True
        self._hs = True
        self._ha = True
        self._ikh = True
        self._j = True
        self._sds = True
        self._lt = True
        self._lc = True
        self._ncl = True
        self._npa = True
        self._nr = True
        self._nc = True
        self._str = True
        self._hu = True
        self._dy = True
        self._pv = True
        self._pl = True
        self._fl = True
        self._fo = True
        self._dc = True
        self._tx = True
        self._mt = True
        self._m = True
        self._ns = True
        self._pm = True


        self.modelPanel = None
        self.displayState = 'normal'


        self.shotInfoStatus = False
        self.projInfoStatus = False

        self.seqManager = None
        self.shotNode = None
        self.shotCam = None

        self.startFrame = 101
        self.endFrame = 201

        self.currentEpisode = ''
        self.currentProj = ''
        self.projCode = ''
        self.epCode = ''

        self.eps = []
        self.qStr = 'q0000'
        self.shotStr = 's0000'
        self.seq = '0000'
        self.shot = '0000'


    def UI(self):
        if pm.window(self.WINDOW_NAME, ex=True):
            pm.deleteUI(self.WINDOW_NAME, window=True)
        with pm.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, s=False, mnb=True, mxb=False) as self.mainWindow:
            with pm.frameLayout(lv=False, mh=10, mw=10):
                

                with pm.frameLayout(l='Open File', mh=5, mw=5, bs='in'):
                    with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 5), (2, 'left', 5)]):
                        pm.text(l='Project')
                        self.projMenu = pm.optionMenu(cc=pm.Callback(self.selectProject))

                        pm.text(l='Episode')
                        self.episodeMenu = pm.optionMenu(cc=pm.Callback(self.selectEpisode))

                    with pm.rowColumnLayout(nc=4, rs=(1, 5), co=[(1, 'left', 5), (2, 'left', 5), (3, 'left', 5), (4, 'left', 5)]):
                        pm.text(l='Sequence')
                        self.seqTxtField = pm.textField(tx=self.seq, w=40, cc=pm.Callback(self.setSeq))
                        pm.text(l='Shot')
                        self.shotTxtField = pm.textField(tx=self.shot, w=40, cc=pm.Callback(self.setShot))


                    with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 5), (2, 'left', 15)]):
                        pm.button(l='Anim Hero', w=125, h=25, c=pm.Callback(self.openHero, 'anim'))
                        pm.button(l='AnimClean Work', w=125, h=25, c=pm.Callback(self.openWork, 'animClean'))

                pm.button(l='Save++ to AnimClean', h=30, c=pm.Callback(self.incSave, 'animClean'))

                with pm.frameLayout(lv=False, bs='in', mh=5, mw=10):     
                    with pm.columnLayout(adj=True, rs=15, co=['both', 5]):
                        self.infoText = pm.text(l=self.defaultInfoTxt)
                        pm.button(l="Refesh Data", w=215, h=20, c=pm.Callback(self.refresh))

                with pm.frameLayout(l='Display', bs='etchedIn', mh=5, mw=5, cll=True, cl=False):
                    pm.button(l='Normal View', h=25, w=65, c=pm.Callback(self.setDisplayToNormal))
                    with pm.frameLayout(lv=False, bs='in', mh=5):
                        with pm.rowColumnLayout(nc=3, co=[(1, 'left', 18), (2, 'left', 10), (3, 'left', 10)]):
                            pm.button(l='Wireframe', h=25, w=70, c=pm.Callback(self.setDisplayWireframe))
                            pm.button(l='Shaded', h=25, w=65, c=pm.Callback(self.setDisplayToShaded))
                            pm.button(l='BoundingBox', h=25, w=75, c=pm.Callback(self.setDisplayBB))
                

                with pm.frameLayout(l='Check List', bs='etchedIn', mh=5, mw=5, cll=True, cl=False):
                    with pm.rowColumnLayout(nc=2, co=[(1, 'left', 10), (2, 'left', 10)]):
                        self.manualCheckBox = pm.checkBox(l='', h=30, cc=pm.Callback(self.manualCheck, 'manual'))
                        self.manualTxt = pm.text(l='Check Intersection / Group Nodes', h=30, w=215, bgc=self.RED)

                    with pm.rowColumnLayout(nc=2, rs=[(1, 5),(2, 5)],
                        co=[(1, 'left', 10), (2, 'left', 10)]) as self.autoRowCol:

                        self.remUnloadRefChkBox = pm.checkBox(l='', h=30, cc=pm.Callback(self.manualCheck, 'removeUnloadRef'))
                        self.remUnloadRefButt = pm.button(l='Remove Unload References', h=30, w=215, c=pm.Callback(self.removeUnloadRef))

                        self.deleteAllChkBox = pm.checkBox(l='', h=30, cc=pm.Callback(self.manualCheck, 'deleteAll'))
                        with pm.frameLayout(lv=False, mh=5, mw=5, bs='etchedIn'):
                            with pm.rowColumnLayout(nc=2, co=[(1, 'left', 5), (2, 'left', 5)]):
                                pm.text(l='type:')
                                self.removeTxtFld = pm.textField(tx=self.defaultRemoveObjTypes, w=150)
                            self.deleteAllButt = pm.button(l='Delete All', h=30, w=200, c=pm.Callback(self.deleteAllByType))

                    with pm.frameLayout(l='Asset Switch', bs='etchedIn', mh=5, mw=5):
                        with pm.rowColumnLayout(nc=2, co=[(1, 'left', 15), (2, 'left', 20)]):
                            self.switchAnimAssetButt = pm.button(l='Anim Asset', h=25, w=105, c=pm.Callback(self.toggleAsset, ['_Render'], '_Anim'))
                            self.switchRenderAssetButt = pm.button(l='Render Asset', h=25, w=105, c=pm.Callback(self.toggleAsset, ['_Anim', '_Proxy'], '_Render'))
                    with pm.rowColumnLayout(nc=2, rs=[(1, 5),(2, 5)],
                         co=[(1, 'left', 10), (2, 'left', 10)])  as self.fixSeqRowCol:
                        self.fixSeqChkBox = pm.checkBox(l='', h=30, cc=pm.Callback(self.manualCheck, 'fixSequencer'))
                        self.fixSeqButt = pm.button(l='Fix Sequencer', h=30, w=215, c=pm.Callback(self.fixSequencer))


                with pm.frameLayout(l='Publishing', bs='etchedIn', mh=5, mw=5, cll=True, cl=False):
                    with pm.rowColumnLayout(nc=3, co=[(1, 'left', 0), (2, 'left', 5), (3, 'left', 5)]): 
                        with pm.frameLayout(lv=False, bs='etchedIn', mh=5, mw=5):   
                            pm.text(l='Anim')
                            pm.text(l='AnimClean')
                            pm.text(l='Light')
                        with pm.frameLayout(lv=False, bs='etchedIn', mh=5, mw=5):     
                            self.sgAnimText = pm.text(l=self.defaultSgInfoTxt, h=25, w=115)
                            self.sgAnimCleanText = pm.text(l=self.defaultSgInfoTxt, h=25, w=115)
                            self.sgLightText = pm.text(l=self.defaultSgInfoTxt, h=25, w=115)
                        with pm.frameLayout(lv=False, bs='etchedIn', mh=5, mw=5):   
                            pm.button(l='view', c=pm.Callback(self.viewPlayblast, 'anim'))
                            pm.button(l='view', c=pm.Callback(self.viewPlayblast, 'animClean'))
                            with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 2), (2, 'left', 3)]):
                                pm.text(l='explorer')
                                self.openDirChkBox = pm.checkBox(l='', v=False)


                    pm.button(l='Publish Film', w=215, h=30, c=pm.Callback(self.publish))
                    pm.button(l='Save++ to Light / Set ShotGun', w=215, h=30, c=pm.Callback(self.saveToLightWork))
                    # pm.button(l='Set Shotgun', w=215, h=35, c=pm.Callback(self.setShotGun))
                


        self.getProj()
        self.getEps()
        self.refresh()


    def getPath(self, lookin):
        path = '%s/%s/q%s/s%s/%s' %(self.filmPath, self.currentEpisode, self.seq, self.shot, lookin)
        return path



    def viewPlayblast(self, dept):
        path = self.getPath(dept)
        seq = 'q%s'%self.seq
        shot = 's%s'%self.shot
        pbFile = '%s_%s_%s_%s_%s.mov' %(self.projCode, self.epCode, seq, shot, dept)
        #winFilePath = '%s/playblast/%s' %(path, pbFile)
        winPath = path.replace('/', '\\')


        pbPath = '%s/playblast/%s' %(path, pbFile)

        dirCheckbox = self.openDirChkBox.getValue()

        if dirCheckbox == True:       
            try:
                if os.path.exists('%s\\playblast' %winPath):
                    dirToOpen = '%s\\playblast' %winPath
                elif os.path.exists(winPath):
                    dirToOpen = winPath
                else:
                    om.MGlobal.displayError('Cannot find playblast directory')
                    return
                subprocess.Popen(r'explorer "%s"' %dirToOpen)
            except:
                om.MGlobal.displayError('Cannot open directory : %s' %path)
                return
        else:
            try:
                #subprocess.call([self.qtPath, winFilePath])
                #os.system('start %s' %winFilePath)
                # print winFilePath
                mel.eval('launch -mov "%s"' %pbPath)
            except:
                om.MGlobal.displayError('Cannot open playblast video file : %s' %pbPath)
                return

            


    def saveToLightWork(self):
        # set to boundingBox
        self.setDisplayBB()

        # save to light
        res = self.incSave('light')

        #set shot gun
        # if res == True:
        #     ptTechAnim.sgLightUpdate(pm.sceneName())

        # self.getSGStatus()
        # self.refreshSGinfo()



    def setSeq(self):
        seq = self.seqTxtField.getText()
        seq = re.sub("[^0-9]", "", seq)
        self.seq = seq.zfill(4)
        self.seqTxtField.setText(self.seq)



    def setShot(self):
        shot = self.shotTxtField.getText()
        shot = re.sub("[^0-9]", "", shot)
        self.shot = shot.zfill(4)
        self.shotTxtField.setText(self.shot)



    def openHero(self, dept):
        self.setSeq()
        self.setShot()
        path = self.getPath('%s/scenes' %dept)
        if os.path.exists(path) == False:
            om.MGlobal.displayError('%s  : Does not exists!' %path)
            return

        files = os.walk(path).next()[2]
        openFile = None
        for i in files:
            if i.endswith('_q%s_s%s_%s.ma' %(self.seq, self.shot, dept)) == True:
                openFile = i
        filePath = '%s/%s' %(path, openFile)
        filePath = misc.cleanPath(filePath)

        result = ''
        if openFile:
            if pm.cmds.file(q=True, anyModified=True) == True:
                result = pm.confirmDialog( title='Confirm Open File', 
                         message='Scene has been modified',
                         button=['Open', 'Cancel'], defaultButton='Open', cancelButton='Cancel', dismissString='Cancel' )
            
            if result == 'Cancel':
                return
            else:
                print 'Opening : %s' %filePath
                pm.openFile(filePath, f=True)
                self.refresh()
        else:
            om.MGlobal.displayError('Cannot find hero file  :  %s' %filePath)
            return



    def openWork(self, dept):
        self.setSeq()
        self.setShot()
        path = self.getPath('%s/scenes/work' %dept)
        if os.path.exists(path) == False:
            om.MGlobal.displayError('%s  : Does not exists!' %path)
            return

        files = os.walk(path).next()[2]

        filePaths = []
        for f in files:
            filePaths.append('%s/%s' %(path, f))

        filePaths.sort(key=os.path.getmtime, reverse=True)

        result = ''
        if filePaths:
            if pm.cmds.file(q=True, anyModified=True) == True:
                result = pm.confirmDialog( title='Confirm Open File', 
                         message='Scene has been modified',
                         button=['Open', 'Cancel'], defaultButton='Open', cancelButton='Cancel', dismissString='Cancel' )
            
            if result == 'Cancel':
                return
            else:
                print 'Opening : %s' %filePaths[0]
                pm.openFile(filePaths[0], f=True)
                self.refresh()
        else:
            om.MGlobal.displayError('Cannot find any work file in  :  %s' %path)
            return



    def selectEpisode(self):
        self.currentEpisode = self.episodeMenu.getValue()
        episode = info.episode(self.currentProj, self.currentEpisode)
        self.epCode = episode.code()


    def selectProject(self):
        self.currentProj = self.projMenu.getValue()
        projPath = config.PROJ_DIR_DICT[self.currentProj][0]
        self.filmPath = '%s/film' %projPath

        project = info.project(self.currentProj)
        self.projCode = project.code()

        self.getEps()



    def incSave(self, dept):
        self.getSeqShot(setText=False)

        path = self.getPath('%s/scenes/work' %dept)
        if os.path.exists(path) == False:
            om.MGlobal.displayError('%s  : Does not exists!' %path)
            return

        version = misc.genVersion(path)
        user = misc.getUserFromEnvVar()

        fileName = '%s_%s_q%s_s%s_%s_v%s_%s.ma' %(self.projCode, self.epCode, self.seq, self.shot, dept, version, user)

        saveAsPath = '%s/%s' %(path, fileName)
        if not os.path.exists(saveAsPath):
            pm.saveAs(saveAsPath)
            return True
        else:
            om.MGlobal.displayWarning('%s  : Exists! This script will not override any file.')
            return False



    def getProj(self):
        self.projMenu.clear()
        for proj in config.PROJ_DIR_DICT.keys():
            filmPath = config.PROJ_DIR_DICT[proj]
            pm.menuItem(l=proj, parent=self.projMenu)

        self.selectProject()


    def getEps(self):
        if not os.path.exists(self.filmPath):
            om.MGlobal.displayError('%s  :Film path does not exists!' %self.filmPath)
            return

        self.episodeMenu.clear()
        epFolders = os.walk(self.filmPath).next()[1]
        self.eps = []
        for e in epFolders:
            epPath = '%s/%s' %(self.filmPath, e)
            if e.startswith('_') == False:
                self.eps.append(e)
                pm.menuItem(l=e, parent=self.episodeMenu)

        self.selectEpisode()



    def publish(self):
        self.setDisplayBB()

        ptFilmObj = pubFilmCore.MyForm(pubFilmCore.getMayaWindow())
        ptFilmObj.ui.playlist_checkBox.setChecked(False)
        ptFilmObj.ui.status_comboBox.setCurrentIndex(self.statusIndx)
        ptFilmObj.publish()
        # ptFilmObj.show()

        self.getSGStatus()
        self.refreshSGinfo()



    def setShotGun(self):
        sceneName = pm.sceneName()
        
        result = ''
        if '/%s/light/scenes/work/' %self.shotStr not in sceneName:
            om.MGlobal.displayError('Please save current scene to light/work directory.')
            return

        ptTechAnim.sgLightUpdate(sceneName)



    def manualCheck(self, op):
        chkBox = None
        button = None

        if op == 'removeUnloadRef':
            chkBox = self.remUnloadRefChkBox
            button = self.remUnloadRefButt
        elif op == 'deleteAll':
            chkBox = self.deleteAllChkBox
            button = self.deleteAllButt
        elif op == 'fixSequencer':
            chkBox = self.fixSeqChkBox
            button = self.fixSeqButt
        elif op == 'manual':
            chkBox = self.manualCheckBox
            button = self.manualTxt

        manualStatus = chkBox.getValue()
        if manualStatus == True:
            button.setBackgroundColor(self.GREEN)
        else:
            button.setBackgroundColor(self.RED)



    def updateStatus(self, op):
        chkBox = None
        button = None

        if op == 'removeUnloadRef':
            chkBox = self.remUnloadRefChkBox
            button = self.remUnloadRefButt
        elif op == 'deleteAll':
            chkBox = self.deleteAllChkBox
            button = self.deleteAllButt
        elif op == 'fixSequencer':
            chkBox = self.fixSeqChkBox
            button = self.fixSeqButt

        chkBox.setValue(True)
        button.setBackgroundColor(self.GREEN)



    def refreshSGinfo(self):
        if not self.sgAnimInfo:
            self.sgAnimInfo = self.defaultSgInfoTxt
        self.setAnimTextStatus()

        if not self.sgAnimCleanInfo:
            self.sgAnimCleanInfo = self.defaultSgInfoTxt
        self.setAnimCleanTextStatus()

        if not self.sgLightInfo:
            self.sgLightInfo = self.defaultSgInfoTxt
        self.setLightTextStatus()

        print '\n####### SG INFO #######\nAnim : %s\nAnim Clean : %s\nLight : %s' %(self.sgAnimInfo, self.sgAnimCleanInfo, self.sgLightInfo)



    def setAnimTextStatus(self):
        self.sgAnimText.setLabel(self.sgStatusMap[self.sgAnimInfo])
        self.setTextColor(self.sgAnimText, self.sgAnimInfo)



    def setAnimCleanTextStatus(self):
        self.sgAnimCleanText.setLabel(self.sgStatusMap[self.sgAnimCleanInfo])
        self.setTextColor(self.sgAnimCleanText, self.sgAnimCleanInfo)



    def setLightTextStatus(self):
        self.sgLightText.setLabel(self.sgStatusMap[self.sgLightInfo])
        self.setTextColor(self.sgLightText, self.sgLightInfo)



    def setTextColor(self, text, data):
        if data == 'aprv':
            color = [0.1, 0.7, 0]
        elif data == 'cmpt':
            color = [0, 1, 0]
        elif data == 'noaprv':
            color = [1, 0, 0]
        elif data in ['intapr', 'udt'] == True:
            color = [1, 1, 0]
        elif data == 'ip':
            color = [0.25, 0.35, 0]
        elif data == 'rdy':
            color = [0.25, 0.25, 0.25]
        elif data == 'daily':
            color = [1, 0, 1]
        elif data == 'hld':
            color = [0.334,0.334,0.334]
        else:
            color = [0, 0, 0]

        text.setBackgroundColor(color)



    def refresh(self):
        self.resetValues()

        self.getProjInfo()

        if self.projInfoStatus == False:
            return

        self.getShotInfo()

        if self.shotInfoStatus == False:
            return

        self.createDisplayLayout()

        if not self.info:
            self.info = self.defaultInfoTxt
        self.infoText.setLabel(self.info)

        self.getSGStatus()
        self.refreshSGinfo()


        print '\n####### SHOT INFO #######\n%s' %self.info
        print '\n####### SG INFO #######\nAnim : %s\nAnim Clean : %s\nLight : %s' %(self.sgAnimInfo, self.sgAnimCleanInfo, self.sgLightInfo)



    def resetValues(self):
        self.shotInfoStatus = False
        self.projInfoStatus = False

        self.seqManager = None
        self.shotNode = None
        self.shotCam = None

        self.shotStr = ''
        self.startFrame = 101
        self.endFrame = 201

        self.displayState == 'normal'
        #self.currentEpisode = ''
        #self.currentProj = ''
        #self.projCode = ''
        #self.epCode = ''

        self.eps = []
        self.qStr = ''
        self.shotStr = ''
        self.seq = '0000'
        self.shot = '0000'

        self.info = ''
        self.sgAnimInfo = ''
        self.sgAnimCleanInfo = ''
        self.sgLightInfo = ''
        self.statusIndx = 0

        #ui
        self.infoText.setLabel(self.defaultInfoTxt)

        chkBoxs = [self.remUnloadRefChkBox, self.deleteAllChkBox, 
                  self.fixSeqChkBox, self.manualCheckBox]
        for chkBox in chkBoxs:
            chkBox.setValue(False)

        buttons = [self.remUnloadRefButt, self.deleteAllButt, 
                   self.fixSeqButt, self.manualTxt]
        for button in buttons:
            button.setBackgroundColor(self.RED)



    def getSeqShot(self, setText=True):
        currentScene = pm.sceneName()

        try:
            reSearch = re.search(r"(/q\d\d\d\d/)", currentScene)
            qStr = reSearch.group()
            self.qStr = qStr[1:-1]
            self.seq = qStr[2:-1]

            reSearch = re.search(r"(/s\d\d\d\d/)", currentScene)
            shotStr = reSearch.group()
            self.shotStr = shotStr[1:-1]
            self.shot = shotStr[2:-1]

            if setText == True:
                self.seqTxtField.setText(self.seq)
                self.shotTxtField.setText(self.shot)

            return True

        except:
            om.MGlobal.displayError('Cannot get sequence and shot number.')
            return False



    def getProjInfo(self):
        currentScene = pm.sceneName()

        self.projInfoStatus = False

        try:
            for k, v in config.PROJ_DIR_DICT.iteritems():
                if v[0] in currentScene:
                    self.currentProj = k
                    self.projMenu.setValue(k)
                    self.selectProject()
                    break

            for ep in self.eps:
                if '/%s/' %ep in currentScene:
                    self.currentEpisode = ep
                    self.episodeMenu.setValue(ep)
                    break

            if not self.currentProj or not self.currentEpisode:
                return self.projInfoStatus

            # get project code and episode code from shot gun
            project = info.project(self.currentProj)
            self.projCode = project.code()

            episode = info.episode(self.currentProj, self.currentEpisode)
            self.epCode = episode.code()

            self.projInfoStatus = True


        except:
            self.resetValues()
            om.MGlobal.displayError('Cannot get project and episode info.')
        
        return self.projInfoStatus



    def getSGStatus(self):
        sceneName = pm.sceneName()

        # get anim status from shotgun
        try:
            self.sgAnimInfo = ptTechAnim.getAnimSGStatus(sceneName)
            self.getSgIndex()
        except:
            om.MGlobal.displayError('%s  : Cannot get Shotgun status for current scene.' %sceneName)
            return      

        

        self.sgAnimCleanInfo = ptTechAnim.getAnimCleanSGStatus(sceneName)
        self.sgLightInfo = ptTechAnim.getLightSGStatus(sceneName)



    def getSgIndex(self):
        # if self.sgAnimInfo == 'noaprv':
        #     self.statusIndx = 0
        # elif self.sgAnimInfo == 'intapr':
        #     self.statusIndx = 1
        # elif self.sgAnimInfo in ['aprv', 'cmpt']:
        #     self.statusIndx = 2
        # elif self.sgAnimInfo == 'daily':
        #     self.statusIndx = 3
        # else:
        #     self.statusIndx = 0
        self.statusIndx = 3



    def getShotInfo(self):
        self.shotInfoStatus = False

        try:
            # get sequence and shot number from file directory
            if self.getSeqShot(setText=True) == False:
                return

            # get nodes in the scene
            self.getSeqManager()
            self.getShotNode()
            self.getCam()

            # get frame
            self.startFrame = pm.playbackOptions(q=True, min=True)
            self.endFrame = pm.playbackOptions(q=True, max=True)

            #update info
            self.info = 'Seq : %s\nShot : %s\nCam : %s\nFrame : %s - %s' %(self.qStr, 
                self.shotStr, self.shotCam.nodeName(), self.startFrame, self.endFrame)
            
            self.shotInfoStatus = True
        except:
            self.resetValues()
            om.MGlobal.displayWarning('Cannot get shot info.')
            
        return self.shotInfoStatus



    def getShotNode(self):
        try:
            self.shotNode = [n for n in pm.ls(self.shotStr) if pm.nodeType(n) == 'shot'][0]

        except:
            self.shotNode = None
            om.MGlobal.displayWarning('Cannot find shot for  s%s' %self.shot)



    def getCam(self):
        try:
            self.shotCam = pm.ls('s%s*' %self.shot, type='camera')[0]
        except:
            self.shotCam = None
            om.MGlobal.displayWarning('Cannot find camera for  s%s' %self.shot)



    def getSeqManager(self):
        try:
            self.seqManager = pm.PyNode(self.defaultSeqMannagerName)
        except:
            self.seqManager = None
            om.MGlobal.displayWarning('Cannot find  %s' %self.defaultSeqMannagerName)



    def fixSequencer(self):
        if self.shotInfoStatus == False or self.projInfoStatus == False:
            return

        
        seqElemNames = self.seqManager.attr('sequences').elements()

        for name in seqElemNames:
            elemAttr = self.seqManager.attr(name)
            connections = pm.listConnections(elemAttr, s=True, d=False, p=True)
            for c in connections:
                pm.disconnectAttr(source=c, destination=elemAttr)

        # set frame
        self.shotNode.startFrame.set(self.startFrame)
        self.shotNode.endFrame.set(self.endFrame)
        self.shotNode.sequenceStartFrame.set(self.startFrame)

        # set shot name
        self.shotNode.shotName.set(self.shotStr)

        # disconnect unwanted shots
        shotMsgOutputs = pm.listConnections(self.shotNode.message, d=True, s=False, p=True)
        for output in shotMsgOutputs:
            output.disconnect()

        # create new sequence node
        if pm.objExists(self.defaultSequencerName):
            pm.delete(self.defaultSequencerName)
        self.seqNode = pm.createNode('sequencer', n=self.defaultSequencerName)

        if pm.isConnected(self.shotCam.message, self.shotNode.currentCamera) == False:
            pm.connectAttr(self.shotCam.message, self.shotNode.currentCamera, f=True)
        pm.connectAttr(self.shotNode.message, self.seqNode.shots[0], f=True)
        pm.connectAttr(self.seqNode.message, self.seqManager.sequences[0], f=True )

        self.updateStatus('fixSequencer')
        print '\nSequencer Fixed.',

        

    def removeUnloadRef(self):
        refDict = misc.removeUnloadRef()
        if not refDict['removed']:
            print '\n\n No Reference removed.',

        removedSize = str(len(refDict['removed']))
        text =  '\n####### %s REFERENCE REMOVED #######' %removedSize
                    
        for removed in refDict['removed']:
            text += '\n%s' %removed

        self.updateStatus('removeUnloadRef')
        print text



    def deleteAllByType(self):
        typeStr = self.removeTxtFld.getText()
        if not typeStr:
            om.MGlobal.displayWarning('Please specify node type(s) to delete.')
            return
        typeStr = typeStr.replace(' ', '')
        splits = typeStr.split(',')

        text =  '\n####### DELETED NODES #######' 
        for type in splits:
            rets = misc.deleteAllTypeInScene(type=type)
            if rets:
                for r in rets:
                    text += '\n%s' %r

        self.updateStatus('deleteAll')
        print text



    def getOldPanelVis(self):
        self._df = pm.modelEditor(self.modelPanel, q=True, df=True)
        self._dim = pm.modelEditor(self.modelPanel, q=True, dim=True)
        self._ca = pm.modelEditor(self.modelPanel, q=True, ca=True)
        self._hs = pm.modelEditor(self.modelPanel, q=True, hs=True)
        self._ha = pm.modelEditor(self.modelPanel, q=True, ha=True)
        self._ikh = pm.modelEditor(self.modelPanel, q=True, ikh=True)
        self._j = pm.modelEditor(self.modelPanel, q=True, j=True)
        self._sds = pm.modelEditor(self.modelPanel, q=True, sds=True)
        self._lt = pm.modelEditor(self.modelPanel, q=True, lt=True)
        self._lc = pm.modelEditor(self.modelPanel, q=True, lc=True)
        self._ncl = pm.modelEditor(self.modelPanel, q=True, ncl=True)
        self._npa = pm.modelEditor(self.modelPanel, q=True, npa=True)
        self._nr = pm.modelEditor(self.modelPanel, q=True, nr=True)
        self._nc = pm.modelEditor(self.modelPanel, q=True, nc=True)
        self._str = pm.modelEditor(self.modelPanel, q=True, str=True)
        self._hu = pm.modelEditor(self.modelPanel, q=True, hu=True)
        self._dy = pm.modelEditor(self.modelPanel, q=True, dy=True)
        self._pv = pm.modelEditor(self.modelPanel, q=True, pv=True)
        self._pl = pm.modelEditor(self.modelPanel, q=True, pl=True)
        self._fl = pm.modelEditor(self.modelPanel, q=True, fl=True)
        self._fo = pm.modelEditor(self.modelPanel, q=True, fo=True)
        self._dc = pm.modelEditor(self.modelPanel, q=True, dc=True)
        self._tx = pm.modelEditor(self.modelPanel, q=True, tx=True)
        self._mt = pm.modelEditor(self.modelPanel, q=True, mt=True)
        self._m = pm.modelEditor(self.modelPanel, q=True, m=True)
        self._ns = pm.modelEditor(self.modelPanel, q=True, ns=True)
        self._pm = pm.modelEditor(self.modelPanel, q=True, pm=True)



    def getCurrentDisplay(self):
        self.cam = pm.modelEditor(self.modelPanel, q=True, cam=True)
        self.oldDisplayApp = pm.modelEditor(self.modelPanel, q=True, displayAppearance=True)
        self.wosDisplay = pm.modelEditor(self.modelPanel, q=True, wos=True)
        self.getOldPanelVis()



    def createDisplayLayout(self):
        # set single view
        mel.eval('setNamedPanelLayout("Single Perspective View")')

        self.modelPanel = mel.eval('getPanel -wl "Persp View"')
        pm.modelEditor(self.modelPanel, e=True, activeView =True)
        self.getCurrentDisplay()

        pm.panelConfiguration(l=self.nTempLayout, sc=True)
        mel.eval('updatePanelLayoutFromCurrent "%s"' %self.nTempLayout)

        self.displayState = 'normal'



    def setDisplayWireframe(self):
        if self.displayState == 'normal':
            self.getCurrentDisplay()
            mel.eval('updatePanelLayoutFromCurrent "%s"' %self.nTempLayout)

        mel.eval('setNamedPanelLayout("Single Perspective View")')

        pm.modelEditor(self.modelPanel, e=True, cam=self.shotCam, wos=False, displayAppearance='wireframe', 
        df=False, dim=False, ca=False, hs=False, ha=False, ikh=False, j=False, sds=False,
        lt=False, lc=False, ncl=False, npa=False, nr=False, nc=False, str=False, hu=False,
        dy=False, pv=False, pl=False, fl=False, fo=False, dc=False, tx=False, mt=False,
        m=True, ns=True, pm=True )

        self.displayState = 'wireframe'



    def setDisplayToShaded(self):
        if self.displayState == 'normal':
            self.getCurrentDisplay()
            mel.eval('updatePanelLayoutFromCurrent "%s"' %self.nTempLayout)

        mel.eval('setNamedPanelLayout("Single Perspective View")')
        
        pm.modelEditor(self.modelPanel, e=True, cam=self.shotCam, wos=False, da='smoothShaded', av=True,
        df=False, dim=False, ca=False, hs=False, ha=False, ikh=False, j=False, sds=False,
        lt=False, lc=False, ncl=False, npa=False, nr=False, nc=False, str=False, hu=False,
        dy=False, pv=False, pl=False, fl=False, fo=False, dc=False, tx=False, mt=False,
        m=True, ns=True, pm=True )

        self.displayState = 'shaded'



    def setDisplayBB(self):
        if self.displayState == 'normal':
            self.getCurrentDisplay()
            mel.eval('updatePanelLayoutFromCurrent "%s"' %self.nTempLayout)

        mel.eval('setNamedPanelLayout("Single Perspective View")')

        pm.modelEditor(self.modelPanel, e=True, cam=self.shotCam, wos=False, da='boundingBox', av=True,
        df=False, dim=False, ca=False, hs=False, ha=False, ikh=False, j=False, sds=False,
        lt=False, lc=False, ncl=False, npa=False, nr=False, nc=False, str=False, hu=False,
        dy=False, pv=False, pl=False, fl=False, fo=False, dc=False, tx=False, mt=False,
        m=True, ns=True, pm=True )

        self.displayState = 'boundingBox'



    def setDisplayToNormal(self):
        mel.eval('setNamedPanelLayout("%s")' %self.nTempLayout)
        
        pm.modelEditor(self.modelPanel, e=True, cam=self.cam, da=self.oldDisplayApp, av=True,
                       wos=self.wosDisplay,df=self._df, dim=self._dim, ca=self._ca, hs=self._hs, 
                       ha=self._ha, ikh=self._ikh, j=self._j, sds=self._sds,lt=self._lt, lc=self._lc, 
                       ncl=self._ncl, npa=self._npa, nr=self._nr, nc=self._nc, str=self._str, 
                       hu=self._hu, dy=self._dy, pv=self._pv, pl=self._pl, fl=self._fl, 
                       fo=self._fo, dc=self._dc, tx=self._tx, mt=self._mt, m=self._m, ns=self._ns, 
                       pm=self._pm )

        self.displayState = 'normal'



    def toggleAsset(self, search, to):
        refDict = misc.switchAsset(search, to)
        text =  '\n####### REPLACED ASSETS #######'
        for ref in refDict['newRefs']:
            text += '\n%s : %s' %(ref.refNode, ref.path)

        print text

        if refDict['oldRefs']:  
            warningMsg = '\n####### OLD ASSETS #######'
            for ref in refDict['newRefs']:
                warningMsg += '\n%s : %s' %(ref.refNode, ref.path)
            om.MGlobal.displayWarning(warningMsg)


