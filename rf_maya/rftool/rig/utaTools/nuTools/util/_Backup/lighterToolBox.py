# maya modules
import maya.cmds as mc
import pymel.core as pm
import maya.mel as mel
import maya.OpenMaya as om

# tool modules
import nuTools.misc as misc

# check if VRay plugin is loaded?
pluginFileName = 'vrayformaya.mll'
if mc.pluginInfo(pluginFileName, q=True, l=True) == False:
	mc.loadPlugin(pluginFileName)


def getNodeVisible(node):
	if mc.getAttr('%s.visibility' %node) == False:
		return False

	parentPath = mc.listRelatives(node, f=True, allParents=True)[0]

	splits = parentPath.split('|')
	s = len(splits)
	for p in splits:
		parent = '|'.join(splits[:s])
		if mc.getAttr('%s.visibility' %parent) == False:
			return False
		s -= 1

	return True


def getAllVisibleGeoInRef(ref):
	nodes = mc.referenceQuery(ref, nodes=True, dp=True)
	geo = []
	for n in [i for i in nodes if mc.objectType(i)=='transform']:
		try:
			shp = mc.listRelatives(n, shapes=True, f=True, ni=True)[0]
			if shp and mc.objectType(shp)=='mesh' and getNodeVisible(shp)==True:
				geo.append(n)
		except: pass

	return geo


def getAllGeoInRef(ref):
	nodes = mc.referenceQuery(ref, nodes=True, dp=True)
	geo = []
	for n in [i for i in nodes if mc.objectType(i)=='transform']:
		try:
			shp = mc.listRelatives(n, shapes=True, f=True, ni=True)[0]
			if shp and mc.objectType(shp)=='mesh':
				geo.append(n)
		except: pass

	return geo


def getRefFromObjects(objs=[]):
	refPaths = set()
	for obj in objs:
		refPath = mc.referenceQuery(obj, f=True)
		refPaths.add(refPath)

	return refPaths 


def convertSelectionToAllGeoInRef():
	sels = mc.ls(sl=True, l=True, type='transform')
	refs = getRefFromObjects(sels)

	geos = []
	for ref in refs:
		geo = getAllVisibleGeoInRef(ref)
		if geo:
			geos.extend(geo)
	if geos:
		mc.select(geos, r=True)


def setRenderAttrs(attr):
	sels = mc.ls(sl=True, l=True, type='transform')
	value = not(misc.checkMod(check='ctrl'))

	for sel in sels:
		objAttr = '%s.%s' %(sel, attr)
		if mc.objExists(objAttr):
			mc.setAttr(objAttr, value)

	om.MGlobal.displayInfo('%s : %s' %(attr, value))


def getMatInScene(matName):	
	mat = None
	if mc.objExists(matName) and mc.objectType(matName) == 'VRayMtl':
		mat = matName
	else:
		mat = mc.shadingNode('VRayMtl', asShader=True, n=matName)

	sgs = mc.listConnections('%s.outColor' %mat, d=True, et=True, type='shadingEngine')
	if not sgs:
		sg = mc.sets(n='%sSG' %mat, r=True, nw=True)
		mc.connectAttr('%s.outColor' %mat, '%s.surfaceShader' %sg, f=True)
	else:
		sg = sgs[0]

	return mat, sg


def getShapes(objs):
	if not isinstance(objs, (list, set, tuple)):
		objs = [objs]

	shps = []
	for obj in objs:
		try:
			shp = mc.listRelatives(obj, shapes=True, f=True, ni=True)[0]
			shps.append(shp)
		except:
			pass

	return shps


def assignSimpleMat():
	matName = 'simple_mat'
	sels = mc.ls(sl=True, l=True, type='transform')
	simpleMat, sg = getMatInScene(matName=matName)
	shps = getShapes(sels)

	if shps:
		mc.sets(shps, e=True, nw=True, fe=sg)	


def createVrayREMultiMatte():
	mmRe = mel.eval('vrayAddRenderElement "MultiMatteElement"')
	mmRe = mc.rename(mmRe, 'vrayRE_Id')
	mc.setAttr('%s.vray_considerforaa_multimatte'% mmRe, 1)
	mc.setAttr('%s.vray_redid_multimatte'% mmRe, 1)
	mc.setAttr('%s.vray_greenid_multimatte'% mmRe, 2)
	mc.setAttr('%s.vray_blueid_multimatte'% mmRe, 3)
	mc.setAttr('%s.vray_usematid_multimatte'% mmRe, 1)


def addVrayIdAttrs(mat):
	if not mc.objExists('%s.vrayColorId' %mat):
		mel.eval('vrayAddAttr %s vrayColorId' %mat)

	if not mc.objExists('%s.vrayMaterialId' %mat):
		mel.eval('vrayAddAttr %s vrayMaterialId' %mat)


def assignIdMat(idNum=1):
	idNames = {1: 'mainId', 2: 'secondaryId', 3: 'extraId'}
	matName = '%s_mat' %idNames[idNum]
	sels = mc.ls(sl=True, l=True, type='transform')

	idMat, sg = getMatInScene(matName=matName)

	# add id attributes - material ID and multimatte ID
	addVrayIdAttrs(idMat)

	mc.setAttr('%s.vrayMaterialId' %idMat, idNum)

	# create multiMatte
	if not mc.objExists('vrayRE_Id'):
		createVrayREMultiMatte()

	shps = getShapes(sels)
	if shps:
		mc.sets(shps, e=True, nw=True, fe=sg)	


def copyAssignedMat():
	sels = mc.ls(sl=True, l=True, type='transform')
	if len(sels) < 2:
		om.MGlobal.displayError('Select at least 2 objects - (destnation(s), source)')
		return

	des = sels[:-1]
	src = sels[-1]

	# get src shape and SG
	try:
		srcShp = getShapes(src)[0]
		sg = mc.listConnections('%s.instObjGroups' %srcShp, d=True, et=True, type='shadingEngine')[0]
	except:
		om.MGlobal.displayError('Cannot get shape and SG for : %s' %src)
		return

	# get des shapes
	desShps = getShapes(des)

	if desShps:
		mc.sets(desShps, e=True, nw=True, fe=sg)	


def toggleAllCorneaVisibility():
	references = mc.ls(type='reference')
	chaRefs = []
	value = not(misc.checkMode(check='ctrl'))

	for ref in references:
		try:
			path = mc.referenceQuery(ref, f=True)
			if path.split('/')[4] == 'char':
				chaRefs.append(ref)
		except:
			pass

	for chaRef in chaRefs:
		geoInRef = getAllGeoInRef(chaRef)
		shpInRef = getShapes(geoInRef)
		corneas = [geo for geo in shpInRef if 'cornea' in geo and mc.polyEvaluate(geo, v=True) in [242, 382]]
		if corneas:
			for cornea in corneas:
				mc.setAttr('%s.visibility' %cornea, value)
		else:
			om.MGlobal.displayWarning('Cannot find cornea for ref node : %s' %chaRef)




