# util modules
import sys, os, re

# maya modules
import maya.cmds as mc
import pymel.core as pm
import maya.mel as mel
import maya.OpenMaya as om

# QT modules
from PyQt4 import QtCore, QtGui
import sip
import ui
reload(ui)

# nuTools modules
from nuTools import misc, techAnimToolBox
reload(misc)
reload(techAnimToolBox)

# pt modules
sys.path.append('O:/studioTools/maya/python')
from utils.vray import vray_utils as vrayUtils
reload(vrayUtils)
from utils import mayaTools
reload(mayaTools)
from utils import pipelineTools as pipe
reload(pipe)
from sg import utils as sgUtils
reload(sgUtils)


PROJ_DRIVE = 'P:'
RENDER_ELEM_PATH = 'O:/studioTools/maya/preset/techAnim/check/diffuse.mb'
EP_CONFIG_PATH  = 'P:/.config/episodeConfig'
PROJ_CONFIG_PATH = 'P:/.config/projectConfig'
RENDER_LAYER_NAME = 'techAnim_test'

GREY = QtGui.QColor(100, 100, 100)
GREEN = QtGui.QColor(10, 125, 10)

def run():
	'''
		from tools.pt_animCleanChecklist import app as pt_animCleanChecklist_app
		reload(pt_animCleanChecklist_app)
		pt_animCleanChecklist_app.run()
	'''
	import maya.OpenMayaUI as omui
	global ptAnimCleanChecklistApp

	try:
		ptAnimCleanChecklistApp.ui.close()
	except:
		pass
	
	ptr = omui.MQtUtil.mainWindow()
	ptAnimCleanChecklistApp = PtAnimCleanChecklist(sip.wrapinstance(long(ptr), QtCore.QObject))
	ptAnimCleanChecklistApp.ui.show()

	return ptAnimCleanChecklistApp

# UI class
class PtAnimCleanChecklistUi(QtGui.QMainWindow, ui.Ui_MainWindow):
	def __init__(self, parent=None):
		super(PtAnimCleanChecklistUi, self).__init__(parent)
		self.setupUi(self)

class PtAnimCleanChecklist(object): 
	def __init__(self, parent=None) : 
		self._projs = []
		self._eps = []

		self.proj = ''
		self.ep = ''
		self.projCode = ''
		self.epCode = ''
		self.user = ''
		self.seq = ''
		self.shot = ''

		self.ui = PtAnimCleanChecklistUi(parent)

		completer = self.ui.ep_comboBox.completer()
		completer.setCompletionMode(QtGui.QCompleter.PopupCompletion)
		self.ui.ep_comboBox.setCompleter(completer)

		self.buttons = [self.ui.openAnimWork_pushButton,
						self.ui.saveAnim_pushButton,
						self.ui.exportCache_pushButton,
						self.ui.openBuildCache_pushButton,
						self.ui.openAnimCleanWork_pushButton,
						self.ui.updateCache_pushButton,
						self.ui.rebuildScene_pushButton,
						self.ui.switchAsset_pushButton,
						self.ui.cleanUpNode_pushButton,
						self.ui.saveAnimClean_pushButton,
						self.ui.filmPublish_pushButton,
						self.ui.testRender_pushButton,
						self.ui.publishImage_pushButton,
						self.ui.openLightWork_pushButton,
						self.ui.lightSave_pushButton]

		self.getProjects()

		# connect ui
		self.ui.getCurrent_pushButton.clicked.connect(self.getCurrent)
		self.ui.openDir_pushButton.clicked.connect(self.openDir)

		self.ui.animPB_pushButton.clicked.connect(self.openAnimPb)
		self.ui.animCleanPB_pushButton.clicked.connect(self.openTechAnimPb)

		self.ui.openAnimWork_pushButton.clicked.connect(self.openAnimWork)
		self.ui.saveAnim_pushButton.clicked.connect(self.saveToAnim)
		self.ui.exportCache_pushButton.clicked.connect(self.exportCache)

		self.ui.openBuildCache_pushButton.clicked.connect(self.openBuildCache)
		self.ui.openAnimCleanWork_pushButton.clicked.connect(self.openAnimCleanWork)
		self.ui.rebuildScene_pushButton.clicked.connect(self.rebuildScene)
		self.ui.updateCache_pushButton.clicked.connect(self.updateCache)
		self.ui.switchAsset_pushButton.clicked.connect(self.switchAsset)
		self.ui.cleanUpNode_pushButton.clicked.connect(self.cleanUpNode)
		self.ui.saveAnimClean_pushButton.clicked.connect(self.saveToAnimClean)
		self.ui.filmPublish_pushButton.clicked.connect(self.filmPublish)

		self.ui.testRender_pushButton.clicked.connect(self.testRender)
		self.ui.publishImage_pushButton.clicked.connect(self.doPublishImage)

		self.ui.openLightWork_pushButton.clicked.connect(self.openLightWork)
		self.ui.lightSave_pushButton.clicked.connect(self.saveToLight)

		self.ui.seq_lineEdit.editingFinished.connect(self.seqAutoComplete)
		self.ui.sh_lineEdit.editingFinished.connect(self.shotAutoComplete)

		self.ui.resetColor_pushButton.clicked.connect(self.resetColor)
		self.ui.proj_comboBox.currentIndexChanged.connect(self.getEps)

	def resetColor(self):
		for button in self.buttons:
			palette = QtGui.QPalette(button.palette())
			palette.setColor(QtGui.QPalette.Button, GREY)
			button.setPalette(palette)

	def getProjects(self):
		self.ui.proj_comboBox.clear()
		self._projs = []
		with open(PROJ_CONFIG_PATH, 'r') as f:
			data = f.read().splitlines()
			for d in data:
				proj = d.split(':')[0]
				self._projs.append(proj)

		self._projs.sort()
		for proj in self._projs:
			self.ui.proj_comboBox.addItem(proj)

	def getEps(self):
		curr_proj = self.ui.proj_comboBox.currentText()
		self.ui.ep_comboBox.clear()
		self._eps = []
		with open(EP_CONFIG_PATH, 'r') as f:
			data = f.read().splitlines()
		for d in data:
			dsplits = d.split(':')
			if dsplits[0] == curr_proj:
				ep = dsplits[1]
				self._eps.append(ep)
		self._eps.sort()

		for ep in self._eps:
			self.ui.ep_comboBox.addItem(ep)

	def openDir(self):
		self.getShotInfo()
		if os.path.exists(self.shot_path):
			os.startfile(self.shot_path)
		else:
			om.MGlobal.MGlobal_displayError('Shot path do not exists!')

	def seqAutoComplete(self):
		curr_text = str(self.ui.seq_lineEdit.text())
		match = re.match('q[0-9][0-9][0-9][0-9]$', curr_text)
		if not match:
			try:
				num = ''.join([a for a in curr_text if a.isdigit()==True])[-4:]
				seq = 'q%s' %(num.zfill(4))
				self.ui.seq_lineEdit.setText(seq)
				self.ui.sh_lineEdit.setFocus(True)
			except:
				om.MGlobal.displayWarning('Sequence auto complete failed!')

	def shotAutoComplete(self):
		curr_text = str(self.ui.sh_lineEdit.text())
		match = re.match('s[0-9][0-9][0-9][0-9]$', curr_text)
		if not match:
			try:
				num = ''.join([a for a in curr_text if a.isdigit()==True])[-4:]
				seq = 's%s' %(num.zfill(4))
				self.ui.sh_lineEdit.setText(seq)
				self.ui.animPB_pushButton.setFocus(True)
			except:
				om.MGlobal.displayWarning('Shot auto complete failed!')

	def getShotInfo(self):
		#--- info
		self.user = mc.optionVar(q='PTuser')

		self.proj = str(self.ui.proj_comboBox.currentText())
		self.ep = str(self.ui.ep_comboBox.lineEdit().text())

		# read proj code
		self.projCode = None
		with open(PROJ_CONFIG_PATH, 'r') as f:
			data = f.read().splitlines()
			for d in data:
				dsplits = d.split(':')
				if dsplits[0] == self.proj:
					self.projCode = dsplits[2]
					break
		# read ep code
		self.epCode = None
		with open(EP_CONFIG_PATH, 'r') as f:
			data = f.read().splitlines()
			for d in data:
				dsplits = d.split(':')
				if dsplits[0] == self.proj and dsplits[1] == self.ep:
					self.epCode = dsplits[3]
					break

		self.seq = str(self.ui.seq_lineEdit.text())
		self.shot = str(self.ui.sh_lineEdit.text())

		self.getPaths()

	def getCurrent(self):
		self.user = mc.optionVar(q='PTuser')

		curr_path = pm.sceneName()
		basename, ext = os.path.splitext(os.path.basename(curr_path))
		psplits = curr_path.split('/')
		nsplits = basename.split('_')

		self.proj = psplits[1]
		self.projCode = nsplits[0]

		self.ep = psplits[3]
		self.epCode = nsplits[1]

		self.seq = psplits[4]
		self.shot = psplits[5]

		self.getPaths()
		self.updateInfoUi()
		self.resetColor()

	def updateInfoUi(self):
		for i, p in enumerate(self._projs):
			if p == self.proj:
				self.ui.proj_comboBox.setCurrentIndex(i)
				break
		self.ui.ep_comboBox.lineEdit().setText(self.ep)
		self.ui.seq_lineEdit.setText(self.seq)
		self.ui.sh_lineEdit.setText(self.shot)

	def getPaths(self):
		self.camName = '%s_cam' %self.shot

		self.shot_path = 'P:/%s/film/%s/%s/%s' %(self.proj, self.ep, self.seq, self.shot)
		self.anim_path = '%s/anim' %self.shot_path
		self.animClean_path = '%s/animClean' %self.shot_path
		self.light_path = '%s/light' %self.shot_path

		self.anim_work_path = '%s/scenes/work' %self.anim_path
		self.animClean_work_path = '%s/scenes/work' %self.animClean_path

		self.anim_pb_file_path = '%s/playblast/%s_%s_%s_%s_anim.mov' %(self.anim_path, self.projCode, self.epCode, self.seq, self.shot)
		self.animClean_pb_path = '%s/playblast/%s_%s_%s_%s_animClean.mov' %(self.animClean_path, self.projCode, self.epCode, self.seq, self.shot)
		self.buildCache_file_path = '%s/%s_%s_%s_%s_buildCache.ma' %(self.animClean_work_path, self.projCode, self.epCode, self.seq, self.shot)

		self.light_work_path = '%s/scenes/work' %self.light_path

	def setGreen(self, toSet):
		for button in self.buttons:
			color = GREY
			if button == toSet:
				color = GREEN
			palette = QtGui.QPalette(button.palette())
			palette.setColor(QtGui.QPalette.Button, color)
			button.setPalette(palette)

	def updateCache(self):
		from ptCacheTools import importCache
		reload(importCache)
		importCacheApp = importCache.MyForm(importCache.getMayaWindow())
		importCacheApp.importCacheAction()

		self.setGreen(self.ui.updateCache_pushButton)

	def rebuildScene(self):
		from ptCacheTools import importCache
		reload(importCache)
		importCacheApp = importCache.MyForm(importCache.getMayaWindow())
		importCacheApp.importCamera()
		importCacheApp.buildAssetAction()
		importCacheApp.importCacheAction()
		importCacheApp.buildAll()

		self.setGreen(self.ui.rebuildScene_pushButton)

	def cleanUpNode(self):
		techAnimToolBox.cleanAllDisplayAndRenderLayer()

		self.setGreen(self.ui.cleanUpNode_pushButton)

	def switchAsset(self):
		from assetManager import main
		reload(main)
		if mc.window('assetmanagerWindow', exists = True) : 
			mc.deleteUI('assetmanagerWindow')
		assetManApp = main.MyForm(main.getMayaWindow())
		assetManApp.show()

		self.setGreen(self.ui.switchAsset_pushButton)

	def exportCache(self):
		from ptCacheTools import exportCache
		reload(exportCache)
		if mc.window('ExportCacheWindow', exists = True) : 
			mc.deleteUI('ExportCacheWindow')
		exportCacheApp = exportCache.MyForm(exportCache.getMayaWindow())
		exportCacheApp.show()

		self.setGreen(self.ui.exportCache_pushButton)

	def openAnimWork(self):
		self.getShotInfo()
		latestFile = misc.getLatestVersion(path=self.anim_work_path, getFile=True)
		latestFilePath = '%s/%s' %(self.anim_work_path, latestFile)
		if os.path.exists(latestFilePath):
			pm.openFile(latestFilePath, f=True)
			self.setGreen(self.ui.openAnimWork_pushButton)
		else:
			om.MGlobal.displayError('File not found: %s' %latestFile)

	def openBuildCache(self):
		self.getShotInfo()
		if not os.path.exists(self.buildCache_file_path):
			pm.newFile(f=True)
			pm.saveAs(self.buildCache_file_path)
		else:
			pm.openFile(self.buildCache_file_path, f=True)

		self.setGreen(self.ui.openBuildCache_pushButton)

	def openAnimCleanWork(self):
		self.getShotInfo()
		latestFile = misc.getLatestVersion(path=self.animClean_work_path, getFile=True)
		latestFilePath = '%s/%s' %(self.animClean_work_path, latestFile)
		if os.path.exists(latestFilePath):
			pm.openFile(latestFilePath, f=True)
			self.setGreen(self.ui.openAnimCleanWork_pushButton)
		else:
			om.MGlobal.displayError('File not found: %s' %latestFile)

	def openLightWork(self):
		self.getShotInfo()
		latestFile = misc.getLatestVersion(path=self.light_work_path, getFile=True)
		latestFilePath = '%s/%s' %(self.light_work_path, latestFile)
		if os.path.exists(latestFilePath):
			pm.openFile(latestFilePath, f=True)
			self.setGreen(self.ui.openLightWork_pushButton)
		else:
			om.MGlobal.displayError('File not found: %s' %latestFile)

	def filmPublish(self):
		import ptFilmPublish.pubFilmCore as pubFilmCore
		reload(pubFilmCore)
		from playBlastPlus import pbpCore
		reload(pbpCore)
		self.getShotInfo()

		#--- film publish
		pubFilmApp = pubFilmCore.MyForm(pubFilmCore.getMayaWindow())
		pubFilmApp.show()
		pubFilmApp.ui.status_comboBox.setCurrentIndex(3)

		# publish
		# techAnimToolBox.closeAllFloatingWin()
		techAnimToolBox.setViewPort(mode='smoothShaded', cam=self.camName, tex=True)
		pbpApp = pbpCore.MyForm(pbpCore.getMayaWindow())
		pbpApp.showHud()
		pubFilmApp.publish()
		pubFilmApp.close()

		self.setGreen(self.ui.filmPublish_pushButton)
		self.openTechAnimPb()

	def saveToLight(self):
		self.getShotInfo()
		ver = misc.genVersion(self.light_work_path)
		lightSavePath = '%s/%s_%s_%s_%s_light_v%s_%s.ma' %(self.light_work_path, self.projCode, self.epCode, self.seq, self.shot, ver, self.user)
		pm.saveAs(lightSavePath)

		self.setGreen(self.ui.lightSave_pushButton)

	def saveToAnim(self):
		self.getShotInfo()
		ver = misc.genVersion(self.anim_work_path)
		animSavePath = '%s/%s_%s_%s_%s_anim_v%s_%s.ma' %(self.anim_work_path, self.projCode, self.epCode, self.seq, self.shot, ver, self.user)
		pm.saveAs(animSavePath)

		self.setGreen(self.ui.saveAnim_pushButton)

	def saveToAnimClean(self):
		self.getShotInfo()
		ver = misc.genVersion(self.animClean_work_path)
		animCleanSavePath = '%s/%s_%s_%s_%s_animClean_v%s_%s.ma' %(self.animClean_work_path, self.projCode, self.epCode, self.seq, self.shot, ver, self.user)
		pm.saveAs(animCleanSavePath)

		self.setGreen(self.ui.saveAnimClean_pushButton)

	def openPb(self, path):
		if os.path.exists(path):
			os.startfile(path)
		else:
			om.MGlobal.displayError('Playblast not found!')

	def openAnimPb(self):
		self.getShotInfo()
		self.openPb(self.anim_pb_file_path)

	def openTechAnimPb(self):
		self.getShotInfo()
		self.openPb(self.animClean_pb_path)

	def doCreateRenderLayers(self, arg = None) : 
		pipe.duplicatedMasterLayer(RENDER_LAYER_NAME)
		mc.editRenderLayerGlobals( currentRenderLayer=RENDER_LAYER_NAME )
		mc.setAttr('defaultRenderLayer.renderable', 0)

	def importRenderElement(self):
		mc.file(RENDER_ELEM_PATH, i = True)

	def deleteRenderLayer(self,  arg = None) :
		mc.select(clear=True)
		mc.editRenderLayerGlobals( currentRenderLayer='defaultRenderLayer' )

		vrayElements = mc.ls(type='VRayRenderElement')
		if vrayElements:
			mc.delete(vrayElements)

		if mc.objExists(RENDER_LAYER_NAME):
			mc.delete(RENDER_LAYER_NAME)

	def enableOnlyDiffuseVrayElement(self):
		vrayRE_types = ['VRayRenderElementSet', 'VRayRenderElement']
		vrayElems = pm.ls(type=vrayRE_types)

		for re in vrayElems:
			typ = re.vrayClassType.get()
			if typ != 'diffuseChannel' or re.nodeName() != 'vrayRE_Diffuse': 
				enAttr = re.attr('enabled')
				pm.editRenderLayerAdjustment(enAttr)
				enAttr.set(False)

	def testRender(self, arg = None) :
		# check if diffuse exists
		if not mc.objExists('vrayRE_Diffuse'): 
			self.importRenderElement()

		if not mc.objExists(RENDER_LAYER_NAME):
			self.doCreateRenderLayers()

		self.setRenderSetting()

		self.enableOnlyDiffuseVrayElement()
		self.getShotInfo()

		curr_path = pm.sceneName()

		#check images folder
		dirname = os.path.dirname(curr_path)
		filenameExt = os.path.basename(curr_path)
		filename = os.path.splitext(filenameExt)[0]
		splits = dirname.split('/')
		deptPath = '/'.join(splits[0:7])
		tmpPath = '{0}/images/tmp'.format(deptPath)
		
		# filename = "{0}{1}".format(imgPath, os.path.basename(path))

		if not os.path.exists(tmpPath):
			os.makedirs(tmpPath)
			print 'tmp folder created.'

		print tmpPath

		mc.vray("showVFB")
		mc.vray("vfbControl", "-setregion", "reset")

		#render image
		mc.vrend("-camera", self.camName)
		
		# find path
		savePicPath = '%s/%s.png' %(tmpPath, filename)
		channelNames = mc.vray("vfbControl", "-getchannelnames")
		for i, name in enumerate(channelNames):
			if name == 'diffuse':
				mc.vray("vfbControl", "-setchannel", i)
				break

		mc.vray("vfbControl", "-saveimage", savePicPath)

		tmpPicPath = '%s/%s.diffuse.png' %(tmpPath, filename)
		print 'image saved to : %s' %tmpPicPath

		print "*********Render Diffuse Complete**************",

		self.setGreen(self.ui.testRender_pushButton)

	def doPublishImage(self, arg = None) : 
		self.getShotInfo()

		#get fileName
		curr_path = pm.sceneName()
		filePath = os.path.dirname(curr_path).split('/')

		fileNameExt = os.path.basename(curr_path)
		fileName, ext = os.path.splitext(fileNameExt)
		fileNameElems = fileName.split('_')

		newFileName = '%s_%s_%s_%s_animClean_%s_%s.diffuse.png' %(fileNameElems[0], fileNameElems[1], 
								fileNameElems[2], fileNameElems[3], fileNameElems[5], self.user)

		# get src path
		imgPath = '%s/images' %self.animClean_path
		tmpPath = '%s/tmp' %imgPath
		tmpPicPath = '%s/%s' %(tmpPath, newFileName)

		# print tmpPicPath
		if not os.path.exists(tmpPicPath):
			print 'Tmp render pic do not exists: %s' %tmpPicPath
			return
		else:
			print 'Found tmp pic : %s' %tmpPicPath

		# get destination path
		versionPath = ''
		#create version folder
		for each in os.path.basename(curr_path).split('_') :

			if each[0] == 'v' and each[1:].isdigit() :
				versionPath = '{0}/diffuse/{1}'.format(imgPath, each)

		if not os.path.exists(versionPath):
			os.makedirs(versionPath)

		dest = "{0}/{1}".format(versionPath, newFileName)
		
		#copy Image
		mc.sysFile(tmpPicPath, copy=dest )#copy file 

		#connect ShotGun   
		projName = filePath[1]
		episode = filePath[3]
		sequenceName = filePath[4]
		shotName = filePath[5]
		taskName = 'diffuse'
		pipeline = 'TechAnim'
		status = 'daily'
		sgUtils.createShotTask2(projName, episode, sequenceName, shotName, pipeline, taskName, status)

		pubVersion = newFileName
		description = ''
		localFile = dest.replace('/','\\')
		movieFile = localFile
		thumbnailPath = localFile
		playlistID = 0
		upload = 1
		frameCount = 0
		result = sgUtils.sgCreateShotVersionPlaylist(projName, episode, sequenceName, shotName, taskName, pubVersion, status, description, localFile, movieFile, thumbnailPath, frameCount, playlistID, upload = 1)
		
		if result :
			self.deleteRenderLayer()
			print "*********Publish Image Complete**************",
			self.setGreen(self.ui.publishImage_pushButton)
			
		else :
			print "Publish Image Failed!!",
		
	def setRenderSetting(self, arg = None) : 
		#set render setting Low Quality
		mel.eval('unifiedRenderGlobalsWindow;')

		mel.eval('editRenderLayerAdjustment "defaultRenderGlobals.currentRenderer";')
		mel.eval('editRenderLayerAdjustment "vraySettings.imageFormatStr";')
		mel.eval('editRenderLayerAdjustment "defaultRenderGlobals.imageFormat";')
		mel.eval('editRenderLayerAdjustment "vraySettings.aspectLock";')
		mel.eval('editRenderLayerAdjustment "vraySettings.width";')
		mel.eval('editRenderLayerAdjustment "vraySettings.height";')
		mel.eval('editRenderLayerAdjustment "vraySettings.aspectRatio";')
		mel.eval('editRenderLayerAdjustment "vraySettings.aspectLock";')
		mel.eval('editRenderLayerAdjustment "vraySettings.sRGBOn";')
		mel.eval('editRenderLayerAdjustment "vraySettings.vfbOn";')
		mel.eval('editRenderLayerAdjustment "vraySettings.globopt_mtl_glossy";')
		mel.eval('editRenderLayerAdjustment "vraySettings.globopt_geom_displacement";')
		mel.eval('editRenderLayerAdjustment "vraySettings.globopt_light_doDefaultLights";')
		mel.eval('editRenderLayerAdjustment "vraySettings.globopt_light_doShadows";')
		mel.eval('editRenderLayerAdjustment "vraySettings.globopt_light_doLights";')
		mel.eval('editRenderLayerAdjustment "vraySettings.globopt_mtl_reflectionRefraction";')
		mel.eval('editRenderLayerAdjustment "vraySettings.globopt_mtl_limitDepth";')
		mel.eval('editRenderLayerAdjustment "vraySettings.globopt_mtl_maxDepth";')
		mel.eval('editRenderLayerAdjustment "vraySettings.globopt_mtl_filterMaps";')
		mel.eval('editRenderLayerAdjustment "vraySettings.samplerType";')
		mel.eval('editRenderLayerAdjustment "vraySettings.aaFilterOn";')
		mel.eval('editRenderLayerAdjustment "vraySettings.sys_rayc_maxLevels";')
		mel.eval('editRenderLayerAdjustment "vraySettings.sys_rayc_faceLevelCoef";')
		mel.eval('editRenderLayerAdjustment "vraySettings.sys_regsgen_xc";')
		mel.eval('editRenderLayerAdjustment "vraySettings.relements_separateFolders";')
		mel.eval('editRenderLayerAdjustment "vraySettings.relements_enableall";')
		mel.eval('editRenderLayerAdjustment "vraySettings.ddisplac_maxSubdivs";')
		mel.eval('editRenderLayerAdjustment "vraySettings.sys_rayc_dynMemLimit";')
		mel.eval('editRenderLayerAdjustment "defaultRenderGlobals.animation";')
		mel.eval('editRenderLayerAdjustment "vraySettings.hideRVOn";')
		mel.eval('editRenderLayerAdjustment "vraySettings.vfbOffBatch";')
		mel.eval('editRenderLayerAdjustment "vraySettings.vfbRgnOffBatch";')
		mel.eval('editRenderLayerAdjustment "vraySettings.dontSaveImage";')
		mel.eval('editRenderLayerAdjustment "vraySettings.animType";')

		mel.eval('setAttr -type "string" defaultRenderGlobals.currentRenderer "vray";')
		mel.eval('setAttr -type "string" vraySettings.imageFormatStr "png";')
		mel.eval('setAttr "defaultRenderGlobals.imageFormat" 32;')
		mel.eval('setAttr "vraySettings.aspectLock" 0;')
		mel.eval('setAttr "vraySettings.width" 960;')
		mel.eval('setAttr "vraySettings.height" 540;')
		mel.eval('setAttr "vraySettings.aspectRatio" 1.778;')
		mel.eval('setAttr "vraySettings.aspectLock" 1;')
		mel.eval('setAttr "vraySettings.sRGBOn" 1;')
		mel.eval('setAttr "vraySettings.vfbOn" 1;')
		mel.eval('setAttr "vraySettings.globopt_mtl_glossy" 0;')
		mel.eval('setAttr "vraySettings.globopt_geom_displacement" 0;')
		mel.eval('setAttr "vraySettings.globopt_light_doDefaultLights" 0;')
		mel.eval('setAttr "vraySettings.globopt_light_doShadows" 0;')
		mel.eval('setAttr "vraySettings.globopt_light_doLights" 0;')
		mel.eval('setAttr "vraySettings.globopt_mtl_reflectionRefraction" 0;')
		mel.eval('setAttr "vraySettings.globopt_mtl_limitDepth" 1;')
		mel.eval('setAttr "vraySettings.globopt_mtl_maxDepth" 1;')
		mel.eval('setAttr "vraySettings.globopt_mtl_filterMaps" 0;')
		mel.eval('setAttr "vraySettings.samplerType" 0;')
		mel.eval('setAttr "vraySettings.aaFilterOn" 0;')
		mel.eval('setAttr "vraySettings.sys_rayc_maxLevels" 80;')
		mel.eval('setAttr "vraySettings.sys_rayc_faceLevelCoef" 1;')
		mel.eval('setAttr "vraySettings.sys_regsgen_xc" 100;')
		mel.eval('setAttr "vraySettings.relements_separateFolders" 0;')
		mel.eval('setAttr "vraySettings.relements_enableall" 1;')
		mel.eval('setAttr "vraySettings.ddisplac_maxSubdivs" 1;')
		mel.eval('setAttr "vraySettings.sys_rayc_dynMemLimit" 10000;')
		mel.eval('setAttr "defaultRenderGlobals.animation" 0;')
		mel.eval('setAttr "vraySettings.hideRVOn" 0;')
		mel.eval('setAttr "vraySettings.vfbOffBatch" 0;')
		mel.eval('setAttr "vraySettings.vfbRgnOffBatch" 0;')
		mel.eval('setAttr "vraySettings.dontSaveImage" 1;')
		mel.eval('setAttr "vraySettings.animType" 0;')

		mel.eval('window -e -vis 0 unifiedRenderGlobalsWindow;')

		print "*************Setting Vray Low Quality Complete*************************"
