import sys

from PySide import QtCore, QtGui
from shiboken import wrapInstance

import maya.cmds as mc

import app
reload(app)

UI_WIN_NAME = 'nuPicker_MainWindow'

def mayaRun(debug=False):
	'''
		import nuPicker.run
		reload(nuPicker.run)
		nuPicker.run.mayaRun()
	'''
	import maya.OpenMayaUI as omui
	global IgPickerApp

	# try:
	# 	IgPickerApp.ui.close()
	# except:
	# 	pass
	if mc.window(UI_WIN_NAME, q=True, ex=True):
		mc.deleteUI(UI_WIN_NAME, window=True)
	
	ptr = omui.MQtUtil.mainWindow()
	NuPickerApp = app.NuPicker(parent=wrapInstance(long(ptr), QtGui.QWidget))
	# IgPickerApp.ui.show()

	return NuPickerApp