# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'cmdDialog.ui'
#
# Created: Sat May 23 11:04:55 2015
#      by: PyQt4 UI code generator 4.8.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_cmd_dialog(object):
    def setupUi(self, cmd_dialog):
        cmd_dialog.setObjectName(_fromUtf8("cmd_dialog"))
        cmd_dialog.resize(450, 200)
        self.gridLayout = QtGui.QGridLayout(cmd_dialog)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.cmd_textEdit = QtGui.QTextEdit(cmd_dialog)
        self.cmd_textEdit.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.cmd_textEdit.setFrameShape(QtGui.QFrame.WinPanel)
        self.cmd_textEdit.setLineWrapMode(QtGui.QTextEdit.NoWrap)
        self.cmd_textEdit.setObjectName(_fromUtf8("cmd_textEdit"))
        self.gridLayout.addWidget(self.cmd_textEdit, 0, 0, 1, 1)
        self.main_buttonBox = QtGui.QDialogButtonBox(cmd_dialog)
        self.main_buttonBox.setOrientation(QtCore.Qt.Vertical)
        self.main_buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.main_buttonBox.setObjectName(_fromUtf8("main_buttonBox"))
        self.gridLayout.addWidget(self.main_buttonBox, 0, 1, 1, 1)
        self.language_horizontalLayout = QtGui.QHBoxLayout()
        self.language_horizontalLayout.setSpacing(50)
        self.language_horizontalLayout.setContentsMargins(50, -1, -1, -1)
        self.language_horizontalLayout.setObjectName(_fromUtf8("language_horizontalLayout"))
        self.mel_radioButton = QtGui.QRadioButton(cmd_dialog)
        self.mel_radioButton.setChecked(True)
        self.mel_radioButton.setObjectName(_fromUtf8("mel_radioButton"))
        self.language_horizontalLayout.addWidget(self.mel_radioButton)
        self.python_radioButton = QtGui.QRadioButton(cmd_dialog)
        self.python_radioButton.setObjectName(_fromUtf8("python_radioButton"))
        self.language_horizontalLayout.addWidget(self.python_radioButton)
        self.gridLayout.addLayout(self.language_horizontalLayout, 1, 0, 1, 1)

        self.retranslateUi(cmd_dialog)
        QtCore.QObject.connect(self.main_buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), cmd_dialog.accept)
        QtCore.QObject.connect(self.main_buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), cmd_dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(cmd_dialog)

    def retranslateUi(self, cmd_dialog):
        cmd_dialog.setWindowTitle(QtGui.QApplication.translate("cmd_dialog", "Bind command button", None, QtGui.QApplication.UnicodeUTF8))
        self.mel_radioButton.setText(QtGui.QApplication.translate("cmd_dialog", "MEL", None, QtGui.QApplication.UnicodeUTF8))
        self.python_radioButton.setText(QtGui.QApplication.translate("cmd_dialog", "Python", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    cmd_dialog = QtGui.QDialog()
    ui = Ui_cmd_dialog()
    ui.setupUi(cmd_dialog)
    cmd_dialog.show()
    sys.exit(app.exec_())

