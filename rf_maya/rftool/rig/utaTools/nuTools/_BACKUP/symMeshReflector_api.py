import maya.OpenMaya as om
import maya.mel as mel


class SymMeshReflector(object):

    def __init__(self):
        self.tol = 0.0001

        self.baseMeshData = []

        self.posIndx = []
        self.negIndx = []
        self.midIndx = []
        self.noneIndx = []
        
        self.midPosIndx = []
        self.midNegIndx = []

        self.midDict = {}
        self.pairDict = {}
        self.focusData = {}  #{mesh:[vtx1, vtx2, ...]}
        self.focusIndx = {}  #{mesh:[indx1, indx2, ...]}

        self.displaceTrans = []

    def getVtxFromIndx(self, mesh, indx):
        return map(lambda i: '%s.vtx[%i]' %(mesh, i), indx)

    def caller(self, op):
        # get user sel, convert selection to vertex
        sels = mel.eval('filterExpand -fp true -ex true -sm 31 -sm 12;')

        if not sels: 
            return
        self.focusData = {}  #{mesh:[vtx1, vtx2, ...]}
        self.focusIndx = {}  #{mesh:[indx1, indx2, ...]}

        for sel in sels:
            typ = mel.eval('objectType %s' %sel)
            if typ == 'transform':
                shps = mel.eval('listRelatives -shapes %s;' %s)
                if shps:
                    shp = shps[0]
                    if shp not in self.focusData.keys():
                        self.focusData[shp] = []
                        self.focusIndx[shp] = []
                    numVtx = mel.eval('polyEvaluate -v %s;' %shp)
                    if numVtx: 
                        numVtxRange = range(0, numVtx[0])
                    self.focusData[shp].extend(self.getVtxFromIndx(shp, numVtxRange))
                    self.focusIndx[shp].extend(numVtxRange)
            elif typ == 'mesh':
                if '.vtx' in sel:
                    splits = sel.split('.vtx')
                    mesh = splits[0]
                    indx = int(splits[-1][1:-1])
                    if mesh not in self.focusData.keys():
                        self.focusData[mesh] = []
                        self.focusIndx[mesh] = []
                    self.focusData[mesh].append(sel)
                    self.focusIndx[mesh].append(indx)
                else:
                    if sel not in self.focusData.keys():
                        self.focusData[sel] = []
                        self.focusIndx[sel] = []
                    numVtx = mel.eval('polyEvaluate -v %s;' %sel)
                    if numVtx: 
                        numVtxRange = range(0, numVtx[0])
                    self.focusData[sel].extend(self.getVtxFromIndx(sel, numVtxRange))
                    self.focusIndx[sel].extend(numVtxRange)

        if op == 'mirror_pos_to_neg':
            self.mirrorVtx(filterIndxList=self.midPosIndx)

        elif op == 'mirror_neg_to_pos':
            self.mirrorVtx(filterIndxList=self.midNegIndx)

        elif op == 'revertToBase':
            self.revertVtxToBase()

        elif op == 'flip':
            self.flipVtx(filterIndxList=self.midPosIndx)

    def getMFnMesh(self, obj):
        """
        Given PyNode of type Mesh, return mfnMesh.
        """
        try:
            msl = om.MSelectionList()
            msl.add(obj)
            nodeDagPath = om.MDagPath()
            msl.getDagPath(0, nodeDagPath)
        except:
            raise

        return om.MFnMesh(nodeDagPath)

    def analyzeBaseGeo(self):
        """
        Build data array of all the meshes.

        """

        objs = mel.eval('ls -sl -type "transform";')
        if not objs:
            return

        #get all shapes underneath the transform
        meshes = mel.eval('listRelatives -ad -shapes -f -ni %s;' %objs[0])
        mesh = None
        if meshes:
            mesh = meshes[0]
        else: return

        #reset data
        self.baseMeshData = []
        self.posIndx = []
        self.negIndx = []
        self.midIndx = []
        self.noneIndx = []
        self.pairDict = {}
        self.midDict = {}

        #for speed sake, turn off undo
        #mel.eval('undoInfo -st false;')

        #iterate thru each mesh 
        mfnMesh = self.getMFnMesh(obj=mesh)
        pArray = om.MPointArray()   
        mfnMesh.getPoints(pArray, om.MSpace.kObject)
        pos, neg = [], []

        #finding the mesh center
        self.meshCenter = mel.eval('objectCenter -l %s;' %mesh)

        #for each vertex, store all in orig and get those in the middle
        vtxNum = pArray.length()

        for i in range(0, vtxNum):
            foundPair = False
            self.baseMeshData.append([pArray[i].x, pArray[i].y, pArray[i].z])
            self.midDict[i] = False

            #found that it is in the mid
            if abs(pArray[i].x - self.meshCenter[0]) <= self.tol:
                self.midIndx.append(i)
                self.midDict[i] = True
                continue

            if pArray[i].x > self.meshCenter[0]:
                pos.append(i)
            else:
                neg.append(i)

        #matching left and right vertex
        for p in pos:
            foundPair = False
            find = om.MPoint((pArray[p].x*-1)+(2*self.meshCenter[0]), pArray[p].y, pArray[p].z)
            for n in neg:
                if pArray[n].distanceTo(find) <= self.tol:
                    self.negIndx.append(n)
                    self.posIndx.append(p)
                    self.pairDict[p] = n
                    self.pairDict[n] = p
                    foundPair = True
                    break
            if foundPair == False:
                self.noneIndx.append(p)
            else:
                neg.pop(neg.index(n)) #cut out neg vert that has already matched with pos for more speed

        #add negative verts that'was left unmatched to none    
        self.noneIndx.extend(neg)

        self.midPosIndx = self.midIndx + self.posIndx
        self.midNegIndx = self.midIndx + self.negIndx

        #turn undo back on
        #mel.eval('undoInfo -st true;')

        if self.noneIndx:
            nonSymVtxs = self.getVtxFromIndx(mesh, self.noneIndx)
            toSelect = ' '.join('"{0}"'.format(v) for v in nonSymVtxs)
            mel.eval('select -r %s;' %toSelect)
            om.MGlobal.displayWarning('Mesh is NOT symmetrical. Not all vertex can be mirrored.')
        else:
            om.MGlobal.displayInfo('Mesh is symmetrical.')


    def revertVtxToBase(self):
        for mesh in self.focusData.keys():
            #get all vtx current position
            currTrans = self.getCurrOsTrans(mesh)

            for v, i in zip(self.focusData[mesh], self.focusIndx[mesh]):
                if currTrans[i] == self.baseMeshData[i]:
                    continue
                #mel.eval('move -ls %f %f %f %s' %(self.baseMeshData[i][0], self.baseMeshData[i][1], self.baseMeshData[i][2], v))
                self.displaceTrans[i] = [self.baseMeshData[i][0], self.baseMeshData[i][1], self.baseMeshData[i][2]]


            self.displaceMesh(mesh, self.displaceTrans)


    def flipVtx(self, filterIndxList):
        for mesh in self.focusData.keys():
            #get all vtx current position
            currTrans = self.getCurrOsTrans(mesh)
            displaceTrans = [[0.0, 0.0, 0.0] * len(currTrans)]

            #filter out vtx
            filterIndxs = list(set(self.focusIndx[mesh]).intersection(filterIndxList))
            filterVtxs = self.getVtxFromIndx(mesh, filterIndxs)

            for v, i in zip(filterVtxs, filterIndxs):

                #if it's in the middle of the mesh
                if self.midDict[i] == True:
                    mel.eval('move -ls %f %f %f %s ' %(self.baseMeshData[i][0], currTrans[i][1], currTrans[i][2], v) )

                #it's on the left or right side
                else:
                    c = self.pairDict[i]

                    # if both verts are already sym, skip to increse speed of moving stady point
                    if self.checkSymPoint(currTrans[i], currTrans[c]) == True:
                        continue

                    cVtx = '%s.vtx[%i]' %(mesh, c)
                    mel.eval('move -ls %f %f %f %s' %(currTrans[i][0]*-1, currTrans[i][1], currTrans[i][2], cVtx))
                    mel.eval('move -ls %f %f %f %s' %(currTrans[c][0]*-1, currTrans[c][1], currTrans[c][2], v))


    def mirrorVtx(self, filterIndxList):
        for mesh in self.focusData.keys():
            #get all vtx current position
            currTrans = self.getCurrOsTrans(mesh)

            #filter out vtx
            filterIndxs = list(set(self.focusIndx[mesh]).intersection(filterIndxList))
            filterVtxs = self.getVtxFromIndx(mesh, filterIndxs)

            for v, i in zip(filterVtxs, filterIndxs):

                #if it's in the middle of the mesh
                if self.midDict[i] == True:
                    pass
                    mel.eval('move -ls %f %f %f %s ' %(self.baseMeshData[i][0], currTrans[i][1], currTrans[i][2], v) )

                #it's on the left or right side
                else:
                    c = self.pairDict[i]

                    # if both verts are already sym, skip to increse speed of moving stady point
                    if self.checkSymPoint(currTrans[i], currTrans[c]) == True:
                        continue

                    cVtx = '%s.vtx[%i]' %(mesh, c)
                    mel.eval('move -ls %f %f %f %s' %(currTrans[i][0]*-1, currTrans[i][1], currTrans[i][2], cVtx))

    def checkSymPoint(self, pointA, pointB):
        mPointA = om.MPoint(pointA[0], pointA[1], pointA[2])
        mPointB = om.MPoint(pointB[0], pointB[1], pointB[2])
        mirMPoint = om.MPoint((pointA[0]*-1)+(2*self.meshCenter[0]), pointA[1], pointA[2])
        if mPointB.distanceTo(mirMPoint) <= self.tol:
            return True
        else:
            return False

    def getCurrOsTrans(self, mesh):
        #reset displace array
        self.displaceTrans = []

        # get the active selection
        selection = om.MSelectionList()
        selection.add(mesh)
        iterSel = om.MItSelectionList(selection, om.MFn.kMesh )

        # go thru selection
        while not iterSel.isDone():
     
            # get dagPath
            dagPath = om.MDagPath()
            iterSel.getDagPath(dagPath)
     
            # create empty point array
            vtxPointArray = om.MPointArray()
            
            # create function set and get points in world space
            currentInMeshMFnMesh = om.MFnMesh(dagPath)
            currentInMeshMFnMesh.getPoints(vtxPointArray, om.MSpace.kObject)
            
            pointPos = []
            for i in range(0 , vtxPointArray.length()):
                pointPos.append([vtxPointArray[i][0], vtxPointArray[i][1], vtxPointArray[i][2]])
                self.displaceTrans.append([])

            # return a list off all points positions
            return pointPos
            #return map(lambda i: [vtxPointArray[i][0], vtxPointArray[i][1], vtxPointArray[i][2]], range(0, vtxPointArray.length()))

    def displaceMesh(self, mesh, displaceTrans):
        # get the dag path for the shapeNode using an API selection list
        selection = om.MSelectionList()
        dagPath = om.MDagPath()
        try:  
            selection.add(mesh)
            selection.getDagPath(0, dagPath)
              
            # initialize a geometry iterator
            geoIter = om.MItGeometry(dagPath)

            # get the positions of all the vertices in world space
            pArray = om.MPointArray()
            geoIter.allPositions(pArray, om.MSpace.kObject)

            # displace each of the vertices
            for i in xrange(pArray.length()):
                if displaceTrans[i]:
                    pArray[i].x = displaceTrans[i][0]
                    pArray[i].y = displaceTrans[i][1]
                    pArray[i].z = displaceTrans[i][2]
                    
            # update the surface of the geometry with the changes
            geoIter.setAllPositions(pArray)
            #meshFn = om.MFnMesh(dagPath)
            #meshFn.updateSurface()
        except: 
            return    

