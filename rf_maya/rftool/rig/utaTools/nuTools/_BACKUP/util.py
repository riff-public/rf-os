import pymel.core as pm
import re, os, socket
from pprint import pprint
from nuTools import misc
from nuTools import config


class Renamer():
	def __init__(self):
		if pm.window('renamerMainWin', ex=True):
			pm.deleteUI('renamerMainWin')
		self.mainWin = pm.window('renamerMainWin', title='Renamer v1.2', s=False, mnb=True, mxb=False, w=255, h=350)
		self.mCol = pm.columnLayout(adj=True, rs=3, co=['both', 3])

		self.srRowCol = pm.rowColumnLayout(nc=2, co=[(1, 'left', 90), (2, 'left', 25)])
		self.srRadioCol = pm.radioCollection()
		self.srSelRadioButt = pm.radioButton(l='Selected', sl=True, cc=pm.Callback(self.uiChange,'scope'))
		self.srSceneRadioButt = pm.radioButton(l='Scene', sl=False)
		pm.setParent('..')

		self.hCol = pm.columnLayout(adj=True, co=['left', 140])
		self.heirachyChkBox = pm.checkBox(l='Heirachy', v=False)
		pm.setParent('..')

		self.srNsRowCol = pm.rowColumnLayout(nc=2, co=[(1, 'left', 3), (2, 'left', 3)])

		self.srTxRowCol = pm.rowColumnLayout(nc=2, co=[(1, 'left', 3), (2, 'left', 3)])
		self.searchTxt = pm.text(l='Search: ')
		self.searchTxtFld = pm.textField(w=200)
		self.replaceTxt = pm.text(l='Replace: ')
		self.replaceTxtFld = pm.textField(w=200)
		pm.setParent('..')

		self.nsRemButt = pm.button(l='Remove\nnameSpace', bgc=[(0.0), (0.3), (0.3)], c=pm.Callback(self.getTargets, 'removeNameSpace'))
		pm.setParent('..')

		self.srButt = pm.button(l='Search and Replace',bgc=[(0.0), (0.5), (0.0)], h=38, c=pm.Callback(self.getTargets,'searchReplace'))

		self.lrRowCol = pm.rowColumnLayout(nc=3, co=[(1, 'left', 7), (2, 'left', 7), (3, 'left', 7)])
		self.lrButt = pm.button(l='LFT / RHT', h=28, bgc=[(0.15), (0.1), (0.3)], c=pm.Callback(self.getTargets, 'LR'), w=100)
		self.lrButt = pm.button(l='UP / LO', h=28, bgc=[(0.15), (0.1), (0.3)], c=pm.Callback(self.getTargets, 'UL'), w=100)
		self.lrButt = pm.button(l='FR / BK', h=28, bgc=[(0.15), (0.1), (0.3)], c=pm.Callback(self.getTargets, 'FB'), w=100)
		pm.setParent('..')

		self.reRowCol = pm.rowColumnLayout(nc=3, co=[(1, 'both', 3), (2, 'both', 3), (3, 'both', 3)], rs=(1, 3))
		self.prefixTxt = pm.text(l='Prefix')
		self.elemTxt = pm.text(l='Element')
		self.suffixTxt = pm.text(l='Suffix')
		self.prefixTxtFld = pm.textField(w=85)
		self.elemTxtFld = pm.textField(w=155)
		self.suffixTxtFld = pm.textField(w=65)
		self.prefixAddButt = pm.button(l='Add\nPrefix', bgc=[(0.0), (0.2), (0.3)], c=pm.Callback(self.getTargets,'addPrefix'))
		self.elementButt = pm.button(l='Rename', h=38, bgc=[(0.0), (0.5), (0.0)], c=pm.Callback(self.getTargets,'rename'))
		self.suffixAddButt = pm.button(l='Add\nSuffix', bgc=[(0.0), (0.2), (0.3)], c=pm.Callback(self.getTargets,'addSuffix'))
		pm.setParent('..')

		self.lrRowCol = pm.columnLayout(adj=True, co=['left', 250])
		self.autoSuffixButt = pm.button(l='Auto Suffix', h=30, bgc=[(0.0), (0.3), (0.3)], c=pm.Callback(self.getTargets,'autoSuffix'))
		pm.setParent('..')

		self.sep01 = pm.separator()

		self.hashTxt = pm.text(l='Replace  #  with')
		
		self.rnRowCol = pm.rowColumnLayout(nc=2, co=[(1, 'left', 70), (2, 'left', 52)])
		self.rnRadioCol = pm.radioCollection()
		self.rnHashNumRadioButt = pm.radioButton(l='Number', sl=True, cc=pm.Callback(self.uiChange,'hash'))
		self.rnHashAlRadioButt = pm.radioButton(l='Alphabet', cc=pm.Callback(self.uiChange,'hash'))
		pm.setParent('..')

		self.rnRowCol = pm.rowColumnLayout(nc=4, co=[(1, 'left', 70), (2, 'left', 3), (3, 'left', 30), (4, 'left', 3)])
		self.numStartTxt = pm.text(l='Starts from:')
		self.numStartIntFld = pm.intField(w=35, v=1)
		self.capitalChkBox = pm.checkBox(l='Upper Case', v=True, en=False)
		pm.setParent('..')

		self.sepRowCol = pm.rowColumnLayout(nc=2, co=[(1, 'left', 75), (2, 'left', 3)])
		self.sepTxt = pm.text(l='Seperator:')
		self.sepTxtFld = pm.textField(w=85, tx='_')
		pm.setParent('..')

		pm.showWindow()

	def uiChange(self, operation):
		if operation == 'hash':
			if self.rnHashNumRadioButt.getSelect() == True:
				self.numStartIntFld.setEnable(True)
				self.capitalChkBox.setEnable(False)
			else:
				self.numStartIntFld.setEnable(False)
				self.capitalChkBox.setEnable(True)
		if operation == 'scope':
			if self.srSelRadioButt.getSelect() == True:
				self.heirachyChkBox.setEnable(True)
			else:
				self.heirachyChkBox.setEnable(False)

	def getTargets(self, operation):
		rets = []
		hei = self.heirachyChkBox.getValue()
		if self.srSelRadioButt.getSelect() == True:
			if hei == True:
				sels = misc.getSel(selType='any', num='inf')
				ret = []
				for s in sels:
					childs = []
					childs = pm.listRelatives(s, ad=True, typ='transform', ni=True)
					childs.append(s)
					rets.append(childs[::-1])
			else:
				rets = misc.getSel('any', 'inf')

		elif self.srSceneRadioButt.getSelect() == True:
			hei = False
			rets = pm.ls()

		if not rets:
			return

		if hei == True:
			for i in rets:
				self.caller(i, operation)
		else:
			self.caller(rets, operation)	

	def caller(self, rets, operation):
		if operation == 'searchReplace':
			self.searchReplace(rets)
		elif operation == 'rename':
			self.rename(rets)
		elif operation == 'addSuffix':
			self.addSuffix(rets)
		elif operation == 'addPrefix':
			self.addPrefix(rets)
		elif operation == 'autoSuffix':
			self.autoSuffix(rets)
		elif operation == 'LR':
			self.flipPosition(rets, operation)
		elif operation == 'UL':
			self.flipPosition(rets, operation)
		elif operation == 'FB':
			self.flipPosition(rets, operation)
		elif operation == 'removeNameSpace':
			self.removeNameSpace(rets)

	def removeNameSpace(self, rets):
		i = 0
		f = 0
		for obj in rets:
			objName = obj.nodeName()
			nameSplits = objName.split(':')
			if len(nameSplits) > 1:
				newName = nameSplits[-1]
				f += 1
				try:
					obj.rename(newName)
					i += 1
				except:
					pass
		print '\n%s  nameSpace(s) found,  %s  removed.' %(f, i),	

	def searchReplace(self, rets):
		searchTxt = self.searchTxtFld.getText()
		searchFor = searchTxt.split(',')

		replaceWith = self.replaceTxtFld.getText()
		i = 0

		for obj in rets:
			objName = obj.nodeName()
			for s in searchFor:
				if s in objName:
					newName = objName.replace(s, replaceWith)
					try:
						obj.rename(newName)
						i += 1
					except:
						pass
		print '\n%s  matching found, %s renamed.' %(len(rets), i),

	def autoSuffix(self, rets):	
		sep = self.sepTxtFld.getText()
		r = 0
		for i in rets:
			suffix = misc.getSuffix(i)
			if not i.nodeName().endswith('%s%s' %(sep, suffix)):
				newName = '%s%s%s' %(i.nodeName(), sep, suffix)
			try:
				i.rename(newName)
				r+=1
			except:
				pass
		print '\nAuto suffix added to  %s  out of  %s.' %(r, len(rets)),

	def flipPosition(self, rets, op):		 
		r = 0
		result = []
		for i in rets:
			ret = self.doFlip(i, op)
			if ret:
				r+=1

		print '\n%s out of  %s  positions has been flipped.' %(r, len(rets)),

	def doFlip(self, obj, op):
		if op == 'LR':
			posDict = {"Lft":"Rht", "LFT":"RHT", "_lf_":"_rt_", "L_":"R_"}
		elif op == 'UL':
			posDict = {"Up":"Lo", "UP":"LO", "UPR":"LOW", "uppr":"lowr"}
		elif op == 'FB':
			posDict = {"Fr":"Bk", "Frn":"Bck", "FR":"BK", "_fr_":"_bk_", "fnt_":"bck_"}

		oldName = obj.nodeName()
		mname = re.search(r"(\d+$)", oldName)
		try:
			endDigit = mname.group()
			oldName = oldName.replace(endDigit, '')
		except:
			pass
		doRename = False
		for k, v in posDict.iteritems():
			if k in oldName:
				newName = oldName.replace(k, v)
				doRename = True
			elif v in oldName:
				newName = oldName.replace(v, k)
				doRename = True
			if doRename:
				pm.rename(obj, newName)		
				return True

	def rename(self, rets):
		nameTxt = self.elemTxtFld.getText()
		i = 0
		r = 0
		hashDict = self.hash(nameTxt)

		for i in range(len(rets)):
			if hashDict['padNum'] > 0:
				toReplace = self.getHashReplace(i, hashDict['padNum'])
				hashReplace = nameTxt.replace(hashDict['hash'], toReplace)
				newName = '%s%s%s' %(hashDict['start'], toReplace, hashDict['end'])
			else:
				newName = nameTxt
			try:
				rets[i].rename(newName)
				r += 1
			except:
				pass
		print '\nFrom %s object(s), %s renamed.' %(len(rets), r),

	def addPrefix(self, rets):
		prefixTxt = self.prefixTxtFld.getText()
		sep = self.sepTxtFld.getText()
		hashDict = self.hash(prefixTxt)
		r = 0
		for i in range(len(rets)):
			if hashDict['padNum'] > 0:
				toReplace = self.getHashReplace(i, hashDict['padNum'])
				hashReplace = prefixTxt.replace(hashDict['hash'], toReplace)
				newName = '%s%s%s%s%s' %(hashDict['start'], toReplace, hashDict['end'], sep, rets[i].nodeName())
			else:
				newName = '%s%s%s' %(prefixTxt, sep, rets[i].nodeName())
			try:
				rets[i].rename(newName)
				r+=1
			except:
				pass
		print '\nPrefix added to  %s  out of  %s.' %(r, len(rets)),

	def addSuffix(self, rets,):
		suffixTxt = self.suffixTxtFld.getText()
		sep = self.sepTxtFld.getText()
		hashDict = self.hash(suffixTxt)
		r = 0
		for i in range(len(rets)):
			if hashDict['padNum'] > 0:
				toReplace = self.getHashReplace(i, hashDict['padNum'])
				hashReplace = suffixTxt.replace(hashDict['hash'], toReplace)
				newName = '%s%s%s%s%s' %(rets[i].nodeName(), sep, hashDict['start'], toReplace, hashDict['end'])
			else:
				newName = '%s%s%s' %(rets[i].nodeName(), sep, suffixTxt)
			try:
				rets[i].rename(newName)
				r+=1
			except:
				pass
		print '\nSuffix added to  %s  out of  %s.' %(r, len(rets)),

	def getHashReplace(self, order, padNum):
		if self.rnHashNumRadioButt.getSelect() == True:
			startNum = self.numStartIntFld.getValue()
			num = str(order + startNum)
			return num.zfill(padNum)
		else:
			chaSet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 
					'm', 'n', 'o', 'p', 'q','r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

			hashMult = order/26
			rem = order%26
			hashStr = '#' * (hashMult+1)

			ret = '%s' %(hashStr)
			output = chaSet[rem]
			
			for i in range(hashMult):
				output = chaSet[hashMult-1]+output
			if self.capitalChkBox.getValue() == True:
				output = output.upper()
			return ret.replace(hashStr, output)

	def hash(self,nameTxt):
		start, end, hashes = [], [], [] 
		firstHashFound = False
		for i in nameTxt:
			if i == '#':
				hashes.append(i)
				firstHashFound = True
			else:
				if firstHashFound == True:
					end.append(i)
				else:
					start.append(i)

		padding = len(hashes)
		startStr = ''.join(start)
		endStr = ''.join(end)
		hashStr = ''.join(hashes)

		retDict = {'start': startStr, 'end': endStr, 'hash':hashStr, 'padNum':padding}
		return retDict





############################################################################################################################################

############################################################################################################################################
############################################################################################################################################
############################################################################################################################################

############################################################################################################################################






class GeoReplacer(object):
	"""
	Geometry Replacer Class helps you match geometries with the same name/position to identify them in as a pair.
	After each object is identified, you can choose a method for replacing old geometry with the new one.

	Instance Command :
		from nuTools import util as ut
		reload(ut)
		geoReplacer = ut.GeoReplacer()
		geoReplacer.UI()

	Methods are:
		1. copyUv : Will copy the UV from the new object to the old one. If the old object has deformer(s) applied,
					this method will not leave any history on the mesh.
		2. replace : Replace the old object with the new one by deleting the old one and parent the new one to the 
					same heirachy.
		3. copySkinWeight : Apply smooth skin and copy skin weight from the old one to the new one.
	"""		
	def __init__(self):
		self.WINDOW_NAME = 'geoReplacerMainWin'
		self.WINDOW_TITLE = 'Geometry Replacer v2.0'
		self.assetPathDefault = 'asset/3D'
		self.driveDefalut = 'P'
		self.ref = []
		self.matchDict = {}
		self.partMatchDict = {}
		self.noMatch = []
		self.sels = []
		self.unSels = []

	def UI(self):
		if pm.window(self.WINDOW_NAME, ex=True):
			pm.deleteUI(self.WINDOW_NAME, window=True)
		with pm.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, s=False, mnb=True, mxb=False) as self.mainWindow:
			with pm.columnLayout(adj=True, rs=0, co=['both', 5]):
				with pm.rowColumnLayout(nc=3, rs=(1, 5), co=[(1, 'both', 3), (2, 'both', 3), (3, 'both', 3)]):
					pm.text(l='Import From:') 
					with pm.optionMenu() as self.impOptionMenu:
						pm.menuItem(l='uv')
						pm.menuItem(l='model')
						pm.menuItem(l='rig')
						pm.menuItem(l='shade')
						pm.menuItem(l='dRig')
						pm.menuItem(l='dAnimShading')
						pm.menuItem(l='dModel')
						pm.menuItem(l='dShading')
						pm.menuItem(l='ref')
					pm.button(l='Import', c=pm.Callback(self.importObj), bgc=[0.33,0.2,0.1])
				with pm.rowColumnLayout(nc=2, co=[(1, 'left', 5), (2, 'left', 5)]):
					self.refCheckBox = pm.checkBox(l='Reference', v=False)
					pm.button(l='Remove Ref',w=70 , c=pm.Callback(self.removeRef))
					pm.text(l='Drive:')
					self.driveTextField = pm.textField(tx=self.driveDefalut)
					pm.text(l='assetPath: ')
					self.assetPathTextField = pm.textField(tx=self.assetPathDefault)
				pm.separator()
				with pm.columnLayout(adj=True, rs=0, co=['both', 5]):
					self.feedBackTxt = pm.text(l='Select original objects and press "Match Selected".', h=20, bgc=[0.3,0,0])
					self.feedBackNoMatchTxt = pm.text(l='N/A', h=20, bgc=[0.3,0,0])
		with pm.columnLayout(adj=True, rs=5, co=['both', 5]):
			with pm.rowColumnLayout(nc=2, co=[(1, 'left', 60), (2, 'left', 5)]):
				prefixTxt = pm.text(l='Prefix: ')
				self.prefixTxtFld = pm.textField()

			with pm.columnLayout(adj=True, rs=5, co=['both', 65]):
				pm.button(l='Match by Name', w=100, h=25, bgc=(0,0.3,0.3), c=pm.Callback(self.matchObjCall, 'name'))
				pm.button(l='Match by Center and Volume', w=100, h=25, bgc=(0.4,0.0,0.0), c=pm.Callback(self.matchObjCall, 'centerAndVolume'))
				#pm.button(l='Match by Volume', w=100, h=25, bgc=(0.4,0.0,0.0), c=pm.Callback(self.matchObjCall, 'volume'))
				with pm.rowColumnLayout(nc=2, co=[(1, 'left', 0), (2, 'left', 5)]):
					pm.button(l='Select\nMatched', w=75, h=35, c=pm.Callback(self.selMatched))
					pm.button(l='Select\nNon Matched', w=75, h=35, c=pm.Callback(self.selNoMatched))
		with pm.columnLayout(adj=True, rs=5, co=['both', 5]):
			pm.separator()
			methodTxt = pm.text(l='Auto Replace Methods')
			with pm.columnLayout(adj=True, rs=5, co=['left', 90]):
				with pm.optionMenu() as self.metOptionMenu:
						pm.menuItem(l='copyUv')
						pm.menuItem(l='replace')
						pm.menuItem(l='replace(relative)')
						pm.menuItem(l='copySkinWeight')
				self.oldShadeChkBox = pm.checkBox(l='Use Old Shader', v=True)
		with pm.columnLayout(adj=True, rs=5, co=['both', 50]):
			pm.button(l='Replace Selected', w=100, h=30, bgc=(0,0.3,0.3), c=pm.Callback(self.replaceObj, 'selected'))
			pm.button(l='Replace All', w=100, h=40, bgc=(0,0.4,0.2), c=pm.Callback(self.replaceObj, 'all'))
		with pm.columnLayout(adj=True, rs=5, co=['both', 0]):
			pm.separator()
			pm.text(l='Manual Replace')
			with pm.columnLayout(adj=True, rs=5, co=['both', 50]):
				pm.button(l='Parent to Group', w=100, h=40, bgc=(0,0.2,0.45), c=pm.Callback(self.parentToGroup))
		pm.showWindow(self.mainWindow)

	def importObj(self):
		dept = self.impOptionMenu.getValue()
		reference = self.refCheckBox.getValue()
		drive = self.driveTextField.getText()
		assetPath = self.assetPathTextField.getText()
		self.ref = misc.importFromPipeline(dept=dept, ref=reference, drive=drive, assetPath=assetPath)

		#reset prefix text field
		self.prefixTxtFld.setText('')
		if reference == True:
			nameSpace = self.ref.namespace
			self.prefixTxtFld.setText('%s_' %nameSpace)

	def matchObjCall(self, method):
		prefix = self.prefixTxtFld.getText()
		
		#reset the status txt
		self.feedBackTxt.setBackgroundColor([0.3,0,0])
		self.feedBackNoMatchTxt.setBackgroundColor([0.3,0,0])
		self.feedBackTxt.setLabel('Select original objects and press "Match Selected".')
		self.feedBackNoMatchTxt.setLabel('N/A')

		self.getSelMesh()

		self.matchDict, self.noMatch = self.matchObj(prefix=prefix, method=method)

		matchNum = len(self.matchDict)
		noMatchNum = len(self.noMatch)
		self.feedBackTxt.setLabel('%s  object(s) matched.' %matchNum)
		self.feedBackNoMatchTxt.setLabel('%s object(s) unable to find match.' %noMatchNum)

		if matchNum > 0:
			self.feedBackTxt.setBackgroundColor([0,0.5,0])
		if noMatchNum == 0:
			self.feedBackNoMatchTxt.setBackgroundColor([0,0.5,0])

	def selMatched(self):
		sels = misc.getSel(num='inf')
		if not sels:
			return
		orig, new = [], []
		for s in sels:
			if s in self.matchDict.keys():
				orig.append(s)
				new.append(self.matchDict[s])
		pm.select([orig, new], r=True)

	def selNoMatched(self):
		if self.noMatch == []:
			return
		pm.select(self.noMatch, r=True)

	def replaceObj(self, scope):
		if not self.matchDict:
			return

		method = self.metOptionMenu.getValue()
		useOldShade = self.oldShadeChkBox.getValue()
		match = {}
		if scope == 'selected':
			sels = misc.getSel(num='inf')
			for s in sels:
				if s in self.matchDict.keys():
					newObj = self.matchDict[s]
					self.partMatchDict[s] = newObj
			match = self.partMatchDict

		elif scope == 'all':
			match = self.matchDict

		self.batchOpOnPairObjs(objDict=match, op=method, oldShade=useOldShade)

	def removeRef(self):
		if not self.ref:
			return
		self.ref.remove()

	def getSelMesh(self):
		self.sels = []
		sels = misc.getSel(num='inf')
		if not sels:
			pm.error('No selection! Select polygon(s) to find another with matching name.')

		filteredSel = []
		for s in sels:
			tran = None
			tran = self.checkIfPolygons(s)
			if tran:
				self.sels.append(tran)

	def checkIfPolygons(self, obj):
		shp = obj.getShape()
		if shp:
			meshShp = filter(lambda x: isinstance(x, (pm.nt.Mesh)), [shp])
			if meshShp:
				trans = meshShp[0].getParent()
				return trans

	def getUnselected(self):
		if not self.sels:
			return
		self.unSels = []
		allTrans = pm.ls(type='transform')
		for i in allTrans:
			ply = None
			ply = self.checkIfPolygons(i)
			if ply not in self.sels and ply:
				self.unSels.append(ply)


	def matchObj(self, prefix='', method='name'):
		if not self.sels:
			return

		retDict, noMatch = {}, []
		objNum = len(self.sels)

		#Progress bar
		pBarWin = pm.window('Progress', mnb=False, mxb=False, s=False)
		pm.columnLayout(adj=True, co=['both', 5])
		pm.text(l='Matching Geometries by %s...' %method)
		pBar = pm.progressBar(w=200, beginProgress=True, isInterruptable=True, maxValue=objNum, imp=False)
		pm.showWindow(pBarWin)

		for sel in self.sels:
			#if user hit cancel
			if pm.progressBar(pBar, q=True, isCancelled=True ) :
				break
			newObj = ''
			matchFound = False
			if method == 'name':
				sameNames = pm.ls('%s%s' %(prefix, sel.nodeName()))
				for s in sameNames:
					if s == sel:
						sameNames.remove(s)	
				#we got more than one matching name. 
				if len(sameNames) > 1:
					#Will compare each object center and get the closest one.
					newObj, matchFound = self.compareCenter(sel, sameNames)
				else:
					if sameNames != []:
						newObj = sameNames[0]
						matchFound = True

			elif method == 'centerAndVolume':
				self.getUnselected()
				newObj, matchFound = self.compareCenterAndVolume(sel, self.unSels)
				try:
					if matchFound == True:
						self.unSels.remove(newObj)
				except: pass
				
			if matchFound == True and newObj not in retDict.values():
				newObj = pm.PyNode(newObj)
				retDict[sel] = newObj
			else:
				pm.warning('\nCannot find match for  %s' %sel),
				noMatch.append(sel)

			#increment progress bar
			pm.progressBar(pBar, e=True, step=1)

		pm.progressBar(pBar, e=True, endProgress=True)
		pm.deleteUI(pBarWin)

		pprint(retDict)
		return retDict, noMatch

	def compareCenter(self, oldObj, newObjs):
		matchFound = False
		closeObjs = []
		closestObject = ''
		oldCenter = pm.objectCenter(oldObj, gl=True)
		for s in newObjs:
			center = pm.objectCenter(s, gl=True)
			if center == oldCenter:
				closeObjs.append(s)
		
		closeObjNum = len(closeObjs)
		if closeObjNum == 1:
			matchFound = True
			closestObject = closeObjs[0]
		#elif closeObjNum > 1:

		return closestObject, matchFound

	def compareCenterAndVolume(self, oldObj, newObjs):
		matchFound = False
		closeObjs, dists = [], []
		closestObject = ''
		oldCenter = pm.objectCenter(oldObj, gl=True)
		for s in newObjs:
			center = pm.objectCenter(s, gl=True)
			dist = misc.getDistanceFromPosition(oldCenter, center)
			if not dists and not closeObjs:
				dists.append(dist)
				closeObjs.append(s)
				continue
			if dist < min(dists):
				dists = [dist]
				closeObjs = [s]
			elif dist == min(dists):
				dists.append(dist)
				closeObjs.append(s)
			
		if len(dists) == 1:
			closestObject = closeObjs[0]
			matchFound = True
		else:
			closestObject, matchFound = self.compareVolume(oldObj, closeObjs)

		return closestObject, matchFound

	# def compareCenterAndVolumeX(self, oldObj, newObjs):
	# 	matchFound = False
	# 	checkVol = True
	# 	dists = {}
	# 	closestObject = ''
	# 	oldCenter = pm.objectCenter(oldObj, gl=True)
	# 	for s in newObjs:
	# 		center = pm.objectCenter(s, gl=True)
	# 		if center == oldCenter:
	# 			closestObject = s
	# 			checkVol = False
	# 			matchFound = True
	# 			break
	# 		dist = misc.getDistanceFromPosition(oldCenter, center)
	# 		dists[dist] = s

	# 	if checkVol == True:
	# 		for key in sorted(dists.iterkeys())[0:10]:
	# 			matchFound = self.compareVolumeTwoObjects(oldObj, dists[key])
	# 			if matchFound == True:
	# 				closestObject = dists[key]
	# 				break

	# 	return closestObject, matchFound

	# def compareVolumeTwoObjects(self, oldObj, newObj, tol=0.1):
	# 	matchFound = False
	# 	pm.select(oldObj, r=True)
	# 	oldVol = pm.mel.eval('computePolysetVolume;')
	# 	pm.select(newObj, r=True)
	# 	newVol = pm.mel.eval('computePolysetVolume;')

	# 	if abs(oldVol - newVol) <= oldVol*tol:
	# 		matchFound = True

	# 	return matchFound

	def compareVolume(self, oldObj, newObjs):
		matchFound = False
		vols = []
		closestObject = ''
		pm.select(oldObj, r=True)
		oldVol = pm.mel.eval('computePolysetVolume;')
		for s in newObjs:
			pm.select(s, r=True)
			newVol = pm.mel.eval('computePolysetVolume;')
			vols.append(newVol)
		
		diff = [abs(i-oldVol) for i in vols]
		closest = min(diff)

		if diff.count(closest) == 1:
			idx = diff.index(closest)
			closestObject = newObjs[idx]
			matchFound = True

		return closestObject, matchFound

	def batchOpOnPairObjs(self, objDict, op, oldShade=True):
		if not objDict:
			pm.error('/nThis function need dictionary of {oldObject:newObject} as input.'),
		result = []
		#Progress bar
		pBarWin = pm.window('Progress', mnb=False, mxb=False, s=False)
		pm.columnLayout(adj=True, co=['both', 5])
		pm.text(l='Replacing Geometries...')
		objNum = len(objDict.keys())
		pBar = pm.progressBar(w=200, beginProgress=True, isInterruptable=True, maxValue=objNum, imp=False)
		pm.showWindow(pBarWin)

		for k, v in objDict.iteritems():
			#if user hit cancel
			if pm.progressBar(pBar, q=True, isCancelled=True ) :
				break

			if op == 'copyUv':
				k = [k]
				ret = misc.copyUv(parent=v, child=k, rename=False, printRes=False)
				result.append({ret['uv']:'%s%s' %(v, k)})
			elif op == 'replace':
				oldParent = k.getParent()
				if oldShade == True:
					misc.transferShadeAssign(parent=k, child=v)
				pm.delete(k)
				pm.parent(v, oldParent)
				result.append(v)
			elif op == 'replace(relative)':
				oldParent = k.getParent()
				if oldShade == True:
					misc.transferShadeAssign(parent=k, child=v)
				pm.delete(k)
				print 'parenting  %s  :  %s' %(v, oldParent)
				pm.parent(v, oldParent, r=True)
				result.append(v)
			elif op == 'copySkinWeight':
				scs = misc.copySkinWeight(parent=k, child=v)
				if oldShade == True:
					misc.transferShadeAssign(parent=k, child=v)
				result = scs

			#increment progress bar
			pm.progressBar(pBar, e=True, step=1)

		pm.progressBar(pBar, e=True, endProgress=True)
		pm.deleteUI(pBarWin)
		pprint(result)

	def parentToGroup(self):
		sels = misc.getSel(num=2)
		newGrp = sels[0]
		oldGrp = sels[1]

		oldChilds = pm.listRelatives(oldGrp, children=True, type='transform')
		newChilds = pm.listRelatives(newGrp, children=True, type='transform')

		pm.delete(oldChilds)
		pm.parent(newChilds, oldGrp, r=True)





############################################################################################################################################

############################################################################################################################################
############################################################################################################################################
############################################################################################################################################

############################################################################################################################################




class AssetNavigator(object):
	def __init__(self):
		self.WINDOW_NAME = 'assetNavigatorWin'
		self.WINDOW_TITLE = 'Asset Navigator v2.9'
		self.projAssetDirDict = config.PROJ_ASSET_ROOT_DIR
		self.PROJECT_LOADED = False
		self.deptPath = {'hero':'maya', 'work':'maya/work'}
		self.modelType = 'anim'
		self.dept = 'rig'
		self.currentMode = 'work'
		self.defaultAssetPathText = '< Directory does not exists! >'
		self.dirExists = False

		#vars
		self.focusAsset = ''
		self.focusFile = ''
		self.focusAssetPath = ''
		self.focusDeptPath = ''
		self.focusWorkPath = ''
		self.focusHeroPath = ''
		self.focusFilePath = ''
		self.ref = []
		self.assetDict = {}
		self.prevDict = {}

		#get ip, and auto-fill user
		ipAddress = socket.gethostbyname(socket.gethostname())
		try :
			self.user = config.USER_IP[str(ipAddress)]
		except:
			self.user = 'User'

	def UI(self):
		if pm.window(self.WINDOW_NAME, ex=True):
			pm.deleteUI(self.WINDOW_NAME, window=True)
		with pm.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, s=False, mnb=True, mxb=False) as self.mainWindow:
			with pm.columnLayout(adj=True, rs=5, co=['both', 5]):
				with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 0), (2, 'left', 0)]):
					with pm.columnLayout(adj=True, rs=10, co=['both', 0]):
						with pm.rowColumnLayout(nc=4, rs=(1, 5), co=[(1, 'left', 5), (2, 'left', 5), (3, 'left', 10), (4, 'left', 3)]):
							pm.text(l='Project: ')
							with pm.optionMenu(cc=pm.Callback(self.caller, 'setProject')) as self.projMenu:
								pm.menuItem(l='N/A')
								for k in sorted(self.projAssetDirDict.keys()):
									pm.menuItem(l=k)
							pm.text('User: ')
							self.userTxtFld = pm.textField(w=85)
						with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 65), (2, 'left', 5)]):
							pm.text(l='Filter: ')
							with pm.rowColumnLayout(nc=2, rs=((1, 0), (2, 0), (3, 0), (4, 0), (5, 0)), co=[(1, 'left', 5), (2, 'left', 15), (3, 'left', 5), (4, 'left', 5), (5, 'left', 5), (6, 'left', 5), (7, 'left', 5), (8, 'left', 5)]):		
								self.fCharacterChkBox = pm.checkBox(l='character', v=True, cc=pm.Callback(self.caller, 'search'))
								self.fPropChkBox = pm.checkBox(l='prop', v=True, cc=pm.Callback(self.caller, 'search'))
								self.fSetChkBox = pm.checkBox(l='set', v=True, cc=pm.Callback(self.caller, 'search'))
								self.fSetDressChkBox = pm.checkBox(l='setDress', v=True, cc=pm.Callback(self.caller, 'search'))
								self.fVehicleChkBox = pm.checkBox(l='vehicle', v=True, cc=pm.Callback(self.caller, 'search'))
								self.fClothingChkBox = pm.checkBox(l='clothing', v=True, cc=pm.Callback(self.caller, 'search'))
								self.fEnvironmentChkBox = pm.checkBox(l='environment', v=True, cc=pm.Callback(self.caller, 'search'))
								self.fWeaponChkBox = pm.checkBox(l='weapon', v=False, cc=pm.Callback(self.caller, 'search'))
								self.fMiscChkBox = pm.checkBox(l='misc', v=False, cc=pm.Callback(self.caller, 'search'))
								with pm.rowColumnLayout(nc=2, rs=(1, 0), co=[(1, 'left', 0), (2, 'left', 3)]):
									self.fEtcChkBox = pm.checkBox(l='', v=False, cc=pm.Callback(self.caller, 'search'))
									self.fEtcTxtFld = pm.textField(w=73, cc=pm.Callback(self.caller, 'search'))
						with pm.columnLayout(adj=True, rs=5, co=['left', 8]):
							with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 15), (2, 'left', 5)]):
								pm.text(l='Search: ')
								self.searchTxtFld = pm.textField(w=215, cc=pm.Callback(self.caller, 'search'), ec=pm.Callback(self.caller, 'search'))
							self.assetTSL = pm.textScrollList(h=300, ams=False, sc=pm.Callback(self.caller, 'refreshAssetList'))
					with pm.columnLayout(adj=True, rs=4, co=['both', 0]):
						with pm.rowColumnLayout(nc=3, rs=(1, 5), co=[(1, 'left', 20), (2, 'left', 5), (3, 'left', 5)]):
							pm.text(l='Path: ')
							self.assetPathTextFld = pm.textField(ed=False, w=400, bgc=[0.3, 0, 0], tx=self.defaultAssetPathText)
							self.copyButt = pm.button(l='copy', c=pm.Callback(self.copyPath))
						with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 8), (2, 'left', 5)]):
							pm.text(l='Recent: ')
							with pm.optionMenu(cc=pm.Callback(self.caller, 'openPrevFile'), w=400) as self.prevMenu:
								pm.menuItem(l='')
						with pm.rowColumnLayout(nc=6, rs=([1, 0]), co=[(1, 'left', 20), (2, 'left', 5), (3, 'left', 8), 
														(4, 'left', 8), (5, 'left', 8), (6, 'left', 8)]):
							self.deptRadioCol = pm.radioCollection()
							self.modelRadioButt = pm.radioButton(l='model', sl=False, onc=pm.Callback(self.caller, 'refreshAssetList'))
							self.rigRadioButt = pm.radioButton(l='rig', sl=True, onc=pm.Callback(self.caller, 'refreshAssetList'))
							self.uvRadioButt = pm.radioButton(l='uv', sl=False, onc=pm.Callback(self.caller, 'refreshAssetList'))
							self.shadeRadioButt = pm.radioButton(l='shade', sl=False, onc=pm.Callback(self.caller, 'refreshAssetList'))
							self.devRadioButt = pm.radioButton(l='dev', sl=False, onc=pm.Callback(self.caller, 'refreshAssetList'))
							self.refRadioButt = pm.radioButton(l='ref', sl=False, onc=pm.Callback(self.caller, 'refreshAssetList'))
							with pm.columnLayout(adj=True, rs=2, co=['left', 10]):
								self.modelTypeRadioCol = pm.radioCollection()
								self.animModelRadioButt = pm.radioButton(l='anim', sl=True, en=False, onc=pm.Callback(self.caller, 'setModelType'))
								self.proxyModelRadioButt = pm.radioButton(l='proxy', sl=False, en=False, onc=pm.Callback(self.caller, 'setModelType'))
								self.renderModelRadioButt = pm.radioButton(l='render', sl=False, en=False, onc=pm.Callback(self.caller, 'setModelType'))
								self.bshModelRadioButt = pm.radioButton(l='bsh', sl=False, en=False, onc=pm.Callback(self.caller, 'setModelType'))
						with pm.rowColumnLayout(nc=2, co=[(1, 'left', 10), (2, 'left', 10)]):
							with pm.columnLayout(adj=True, co=['both', 0]):
								with pm.tabLayout(cc=pm.Callback(self.caller, 'selectWorkHero')) as self.workHeroTabLayout:
									self.heroTSL = pm.textScrollList(h=278, w=312, ams=False, sc=pm.Callback(self.caller, 'selectFile'))
									self.workTSL = pm.textScrollList(h=278, w=312, ams=False, sc=pm.Callback(self.caller, 'selectFile'))
									pm.tabLayout( self.workHeroTabLayout, edit=True, tabLabel=((self.heroTSL, '   Hero   '), (self.workTSL, '   Work   ')), selectTabIndex=1)
							with pm.columnLayout(adj=True, rs=5, co=['both', 3]):
								pm.text(l='', h=15)
								self.openButt = pm.button(l='Open', w=140, h=50, c=pm.Callback(self.caller, 'openFile'))
								self.savePPButt = pm.button(l='Save ++', w=140, h=50, c=pm.Callback(self.caller, 'incSave'))
								with pm.rowColumnLayout(nc=2, co=[(1, 'left', 0), (2, 'left', 5)]):
									self.referenceButt = pm.button(l='Create\nRef', w=80, h=35, c=pm.Callback(self.createRef))
									self.removeRefButt = pm.button(l='Remove\nRef', w=80, h=35, c=pm.Callback(self.removeRef))
								self.importRefButt = pm.button(l='Import Ref', h=40, c=pm.Callback(self.importRef))
								with pm.columnLayout(adj=True, co=['left', 30]):
									self.removeNameSpaceChkBox = pm.checkBox(l='Remove Namespace', v=True)
								pm.separator()	
								pm.text(l='', h=25)
								self.updateButt = pm.button(l='Update to Current', w=140, h=25, c=pm.Callback(self.updateUI))
								#pm.text(l='', h=15)
		self.updateUI()
		self.userTxtFld.setText(self.user)
								

	def addToPrevOpen(self):
		rootDir = self.projAssetDirDict[self.currentProject]
		label = self.focusFilePath.split(rootDir)[1]
		if len(label) > 65:
			label = label[-65:]
		if self.focusFilePath not in self.prevDict.values():
			self.prevDict[label] = self.focusFilePath
			pm.menuItem(l=label, p=self.prevMenu, ia='')

	def caller(self, op):
		if op == 'setProject':
			self.setProject()
			if self.PROJECT_LOADED == False:
				return
			self.updateAssetTSL('setProject')
			self.updateWorkTSL()
			self.resetAssetPathTxtFld()
			self.workTSL.removeAll()
			self.resetVar()
		elif op == 'search':
			selectedAsset = self.getTSLFocus('assetTSL')
			self.setProject()
			self.updateAssetTSL('search')
			self.workTSL.removeAll()
			self.resetAssetPathTxtFld()
			self.resetVar()
			if selectedAsset in self.assetTSL.getAllItems():
				self.assetTSL.setSelectItem(selectedAsset)
				self.caller('refreshAssetList')
		elif op == 'refreshAssetList':
			self.selectDept()
			self.getHeroWork()
			self.updateWorkTSL()
			self.updateAssetPathTxtFld('selectAssetTSL')
			self.lockSaveButt()
		elif op == 'refreshToCurrent':
			self.getCurrentDept()
			self.selectDeptRadioButt()
			self.caller('refreshAssetList')
		elif op == 'setModelType':
			self.setModelType()
			self.getHeroWork()
			self.updateWorkTSL()
			self.updateAssetPathTxtFld('selectDept')		
		elif op == 'selectWorkHero':
			self.getHeroWork()
			self.updateWorkTSL()
			self.updateAssetPathTxtFld('selectWorkHero')
			self.lockSaveButt()
		elif op == 'selectFile':
			self.updateAssetPathTxtFld('selectFile')
		elif op == 'incSave':
			self.incSave()
			self.workHeroTabLayout.setSelectTabIndex(2)
			self.getHeroWork()
			self.updateWorkTSL()
			self.setWorkTSL()
			self.updateAssetPathTxtFld('selectFile')
		elif op == 'openFile':
			self.openFile()
			self.addToPrevOpen()
		elif op == 'openPrevFile':
			currentPrev = self.prevMenu.getValue()
			if not currentPrev in self.prevDict.keys():
				return
			self.focusFilePath = self.prevDict[currentPrev]
			self.openFile()
			self.prevMenu.setValue('')
			self.updateUI()

	def createRef(self):
		if self.focusFilePath == pm.sceneName() or not self.focusFilePath:
			return
		self.ref.append(pm.createReference(self.focusFilePath, defaultNamespace=True))

	def copyPath(self):
		if self.dirExists == False:
			return
		path = self.assetPathTextFld.getText()
		ctrlPress = misc.checkMod('ctrl')
		if ctrlPress == True:
			toCopyPath = '/'.join(path.split('/')[0:-1])
		else:
			toCopyPath = path
		misc.addToClipBoard(toCopyPath)

	def enableAssetPathTxtFld(self):
		self.assetPathTextFld.setBackgroundColor([0,0.4,0])
		self.dirExists = True

	def getCurrentProject(self):
		currentPath = pm.sceneName()
		try:
			ks = {}
			for k, v in self.projAssetDirDict.iteritems():
				if v in currentPath:
					ks[len(v)] = k
			return ks[max(ks.keys())]
		except:
			return

	def getCurrentAsset(self):
		if not self.assetDict or self.PROJECT_LOADED == False:
			return
		currentPath = pm.sceneName()
		for asset in self.assetDict.keys():
			if '/%s/'%asset in currentPath:
				self.focusAsset = asset
				
	def getCurrentDept(self):
		currentPath = pm.sceneName()
		depts = ['rig' ,'model', 'uv', 'shade', 'dev' ,'ref'] 
		modelTypes = ['anim', 'proxy', 'render']
		try:
			self.dept = [d for d in depts if '/%s/' %d in currentPath][0]
			currentDeptPath = ''
			if self.dept == 'model':
				currentModelType = [t for t in modelTypes if '/%s/' %t in currentPath][0]
				self.modelType = currentModelType
				currentDeptPath = '%s/%s' %(currentDeptPath, currentModelType)
			if '/work/' in currentPath:
				self.workHeroTabLayout.setSelectTabIndex(2)
			else:
				self.workHeroTabLayout.setSelectTabIndex(1)
			#self.focusWorkPath = '%s/%s' %(self.focusAssetPath, currentDeptPath)		
		except:
			pass

	def genVersion(self, incSavePath):
		version = '001'
		try:
			ver = 0
			files = [ f for f in os.listdir(incSavePath) if os.path.isfile(os.path.join(incSavePath,f)) ]
			num = []
			for fName in files:
				if self.focusAsset in fName:
					fName = fName.split(self.focusAsset)[1]
				v = ''
				for i in fName:
					if i.isdigit():
						v += str(i)
				if v:
					num.append(int(v))

			ver = max(num) + 1
			version = str(ver)
		except:
			pass
		return version.zfill(3)

	def getAllAsset(self, assetPathRoot, hasSubType=True):
		existingTypeFolder = os.walk(assetPathRoot).next()[1]
		typePaths = []
		try:
			for assetType in self.assetTypeFiltered:
				if assetType in existingTypeFolder:
					assetTypePath = '%s/%s' %(assetPathRoot, assetType)
					if os.path.exists(assetTypePath):
						typePaths.append(assetTypePath)

			stPaths, assets = [], {}
			for typ in typePaths:
				if hasSubType == False:
					assetFolders = os.walk(typ).next()[1]
					for a in assetFolders:
						assetPath = '%s/%s' %(typ, a)
						if os.path.exists(assetPath):
							assets[a] = assetPath

				else:
					subTypes = os.walk(typ).next()[1]
					for st in subTypes:
						stPath = '%s/%s' %(typ, st)
						if os.path.exists(stPath):
							assetFolders = os.walk(stPath).next()[1]
							for a in assetFolders:
								assetPath = '%s/%s' %(stPath, a)
								if os.path.exists(assetPath):
									assets[a] = assetPath
			self.assetDict  = assets
		except:
			self.assetDict = {}

	def getTSLFocus(self, tsl):
		if tsl == 'assetTSL':
			TSL = self.assetTSL
		elif tsl == 'workHeroTSL':
			if self.currentMode == 'hero':
				TSL = self.heroTSL
			else:
				TSL = self.workTSL
		try:
			return TSL.getSelectItem()[0]
		except:
			pass

	def getHeroWork(self):
		selTab = self.workHeroTabLayout.getSelectTabIndex()
		if selTab == 1:
			self.currentMode = 'hero'
		else:
			self.currentMode = 'work'

	def importRef(self):
		removeNs = self.removeNameSpaceChkBox.getValue()
		try:
			for ref in self.ref:
				ref.importContents(removeNamespace=removeNs)
			self.ref = []
		except:
			pass

	def incSave(self):
		if self.dept in ['dev', 'ref']:
			return
		incSavePath = '%s/%s/%s' %(self.focusAssetPath, self.dept, self.deptPath['work'])

		if not os.path.exists(incSavePath):
			return

		self.user = self.userTxtFld.getText()
		fileName = '%s_%s_v%s_%s.ma' %(self.focusAsset, self.dept, self.genVersion(incSavePath), self.user)
		pathToSave = '%s/%s' %(incSavePath, fileName)

		if os.path.exists(pathToSave):
			return
		else:
			pm.saveAs(pathToSave)
			self.focusFile = fileName
			print ('\nFile Saved to: %s' %pathToSave),

	def lockSaveButt(self):
		if self.dept in ['ref', 'dev'] and self.currentMode != 'work':
			self.savePPButt.setEnable(False)
		else:
			self.savePPButt.setEnable(True)

	def setModelType(self):
		if self.animModelRadioButt.getSelect() == True:
			self.modelType = 'anim'
		elif self.proxyModelRadioButt.getSelect() == True:
			self.modelType = 'proxy'
		elif self.renderModelRadioButt.getSelect() == True:
			self.modelType = 'render'
		elif self.bshModelRadioButt.getSelect() == True:
			self.modelType = 'bsh'
		self.focusDeptPath = '%s/%s/%s' %(self.assetDict[self.getTSLFocus('assetTSL')], 'model', self.modelType)

	def setModelTypeRadioButt(self):
		val = self.modelRadioButt.getSelect()		
		self.animModelRadioButt.setEnable(val)
		self.proxyModelRadioButt.setEnable(val)
		self.renderModelRadioButt.setEnable(val)
		self.bshModelRadioButt.setEnable(val)

	def setWorkTSL(self):
		try:
			if self.focusFile:
				self.workTSL.setSelectItem(self.focusFile)
		except: pass

	def selectDept(self):
		self.focusAsset = self.getTSLFocus('assetTSL')
		if self.PROJECT_LOADED == False or not self.focusAsset:
			return
		self.setModelTypeRadioButt()
		if self.modelRadioButt.getSelect() == True:
			self.dept = 'model'
			self.deptPath = {'hero':'', 'work':'work'}
		elif self.rigRadioButt.getSelect() == True:
			self.dept = 'rig'
			self.deptPath = {'hero':'maya', 'work':'maya/work'}
		elif self.uvRadioButt.getSelect() == True:
			self.dept = 'uv'
			self.deptPath = {'hero':'maya', 'work':'maya/work'}
		elif self.shadeRadioButt.getSelect() == True:
			self.dept = 'shade'
			self.deptPath = {'hero':'maya', 'work':'maya/work'}
		elif self.devRadioButt.getSelect() == True:
			self.dept = 'dev'
			self.deptPath = {'hero':'maya', 'work':'maya/wip'}		
		elif self.refRadioButt.getSelect() == True:
			self.dept = 'ref'
			self.deptPath = {'hero':'', 'work':''}

		if self.dept == 'model':
			self.focusDeptPath = '%s/%s/%s' %(self.assetDict[self.focusAsset], self.dept, self.modelType)
		else:
			self.focusDeptPath = '%s/%s' %(self.assetDict[self.focusAsset], self.dept)

	def selectDeptRadioButt(self):
		if self.dept == 'model':
			self.modelRadioButt.setSelect(True)
			if self.modelType == 'anim':
				self.animModelRadioButt.setSelect(True)
			elif self.modelType == 'proxy':
				self.proxyModelRadioButt.setSelect(True)
			elif self.modelType == 'render':
				self.renderModelRadioButt.setSelect(True)
		elif self.dept == 'rig':
			self.rigRadioButt.setSelect(True) 
		elif self.dept == 'uv':
			self.uvRadioButt.setSelect(True) 
		elif self.dept == 'shade':
			self.shadeRadioButt.setSelect(True) 
		elif self.dept == 'dev':
			self.devRadioButt.setSelect(True) 		
		elif self.dept == 'ref':
			self.refRadioButt.setSelect(True) 

	def openFile(self):
		if not os.path.exists(self.focusFilePath):
			return
		result = ''
		if pm.cmds.file(q=True, anyModified=True) == True:
			result = pm.confirmDialog( title='Confirm Open File', 
				message='Scene has been modified',
				button=['Open', 'Cancel'], defaultButton='Open', cancelButton='Cancel', dismissString='Cancel' )
		
		if result == 'Cancel':
			return
		else:
			pm.openFile(self.focusFilePath, f=True)
			

	def removeRef(self):
		if self.ref:
			for r in self.ref:
				r.remove()
			self.ref = []

	def resetVar(self):
		self.focusAsset = ''
		self.focusFile = ''
		self.focusAssetPath = ''
		self.focusDeptPath = ''
		self.focusWorkPath = ''
		self.focusHeroPath = ''
		self.focusFilePath = ''

	def resetAssetPathTxtFld(self):
		self.assetPathTextFld.setText(self.defaultAssetPathText)
		self.assetPathTextFld.setBackgroundColor([0.3,0,0])
		self.dirExists = False

	def setProject(self):
		self.PROJECT_LOADED = False
		self.currentProject = self.projMenu.getValue()
		if self.currentProject == 'N/A':
			self.workTSL.removeAll()
			self.assetTSL.removeAll()
			self.assetPathTextFld.setText('')
			return

		filterChkBox = [self.fCharacterChkBox, self.fPropChkBox, self.fSetChkBox, self.fSetDressChkBox, 
						self.fVehicleChkBox, self.fWeaponChkBox, self.fEnvironmentChkBox, self.fClothingChkBox,
						self.fMiscChkBox, self.fEtcChkBox]
		self.assetTypeFiltered = []
		self.assetDict = {}
		assetPathRoot = None
		for chkBox in filterChkBox:
			if chkBox.getValue() == True:
				if chkBox == self.fEtcChkBox:
					self.assetTypeFiltered.append(self.fEtcTxtFld.getText())
				else:
					self.assetTypeFiltered.append(chkBox.getLabel())
		if self.currentProject in self.projAssetDirDict.keys():
			assetPathRoot = self.projAssetDirDict[self.currentProject]

		aSubType = config.PROJ_ASSET_SUBTYPE[self.currentProject]

		if assetPathRoot:
			self.getAllAsset(assetPathRoot, aSubType)
			self.PROJECT_LOADED = True



	def updateAssetTSL(self, op):
		sortedAssets = sorted(self.assetDict.iterkeys(), key=lambda s: s.lower())
		if op == 'search':
			searchFor = self.searchTxtFld.getText()
			found = []
			for a in sortedAssets:
				if searchFor in a:
					found.append(a)
			self.assetTSL.removeAll()
			found = sorted(found, key=lambda s: s.lower())
			for f in found:
				self.assetTSL.append(f)

		elif op == 'setProject':
			self.assetTSL.removeAll()
			for a in sortedAssets:
				self.assetTSL.append(a)

	def updateUI(self):
		self.currentProject = self.getCurrentProject()
		if self.currentProject:
			self.projMenu.setValue(self.currentProject)
		else:
			return
		self.setProject()
		if self.PROJECT_LOADED == False:
			return
		self.updateAssetTSL('setProject')
		self.getCurrentAsset()
		if self.focusAsset:
			self.assetTSL.setSelectItem(self.focusAsset)
			self.caller('refreshToCurrent')
	
	def updateWorkTSL(self):
		if self.dept not in ['rig', 'model', 'uv', 'shade', 'dev', 'ref']:
			return
		try:
			if self.deptPath['hero']:
				self.focusHeroPath = '%s/%s' %(self.focusDeptPath, self.deptPath['hero'])
			else:
				self.focusHeroPath = self.focusDeptPath
			if self.deptPath['work']:
				self.focusWorkPath = '%s/%s' %(self.focusDeptPath, self.deptPath['work'])
			else:
				self.focusWorkPath = ''

			self.heroTSL.removeAll()
			self.workTSL.removeAll()

			heroFiles = [ f for f in os.listdir(self.focusHeroPath) if os.path.isfile(os.path.join(self.focusHeroPath,f)) ]
			if heroFiles:
				for h in heroFiles:
					self.heroTSL.append(h)

			workFiles = [ f for f in os.listdir(self.focusWorkPath) if os.path.isfile(os.path.join(self.focusWorkPath,f)) ]
			if workFiles:
				for w in workFiles:
					self.workTSL.append(w)

		except:
			pass

	def updateAssetPathTxtFld(self, op):
		if self.PROJECT_LOADED == False:
			return
		toset = ''
		self.focusFilePath = ''

		if op == 'selectAssetTSL':	
			selAsset = self.getTSLFocus('assetTSL')
			self.focusAssetPath = self.assetDict[selAsset]
			if self.currentMode == 'hero':
		 		toset = self.focusHeroPath
		 	else:
		 		toset = self.focusWorkPath
		elif op == 'selectDept':
			toset = self.focusDeptPath
		elif op == 'selectWorkHero':
			if self.currentMode == 'hero':
		 		toset = self.focusHeroPath
		 	else:
		 		toset = self.focusWorkPath
		elif op == 'selectFile':
			self.fileSel = self.getTSLFocus('workHeroTSL')
			if self.currentMode == 'hero':
				focusPath = self.focusHeroPath
			else:
				focusPath = self.focusWorkPath
			self.focusFilePath = '%s/%s' %(focusPath, self.fileSel)
			toset = self.focusFilePath

		if os.path.exists(toset):
			self.assetPathTextFld.setText(toset)
			self.assetPathTextFld.setBackgroundColor([0,0.4,0])
			self.dirExists = True
		else:
			self.resetAssetPathTxtFld()
			self.workTSL.removeAll()