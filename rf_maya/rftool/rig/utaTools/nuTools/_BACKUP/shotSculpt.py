import pymel.core as pm
import maya.OpenMaya as om
import maya.mel as mel
import nuTools.misc as misc

from nuTools.corrective import invertDeformation as idef

reload(misc)
reload(idef)



class ShotSculpt(object):

	def __init__(self):
		self.WINDOW_NAME = 'shotCorrectiveSculptWin'
		self.WINDOW_TITLE = 'Shot Corrective Sculpt v1.15'
		self.shotSculptGrpName = 'shotSculpt_grp'
		self.wfEditColorCode = 20
		self.wfOrigColorCode = 23

		self.shotSculptGrp = None
		self.origMesh = None
		self.editMesh = None
		
	def UI(self):
		if pm.window(self.WINDOW_NAME, ex=True):
			pm.deleteUI(self.WINDOW_NAME, window=True)
		with pm.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, s=False, mb=True, mnb=True, mxb=False) as self.mainWindow:
			pm.menu(l='Tools')
			pm.menuItem(l='remove targets', c=pm.Callback(self.removeTargets))
			pm.menuItem(l='remove selected sculpts', c=pm.Callback(self.removeSelectedSculpts))
			pm.menuItem(l='remove all sculpts', c=pm.Callback(self.removeAllSculpts))
			pm.setParent('..', menu=True)
			with pm.frameLayout(lv=False, bs='etchedIn', mh=5, mw=5, fn='smallObliqueLabelFont', parent=self.mainWindow):
				with pm.rowColumnLayout(nc=3, co=[(1, 'left', 5), (2, 'left', 15), (3, 'left', 15)]):
					self.selCtrl = pm.button(l='Select Ctrl', h=30, w=65, c=pm.Callback(self.selectEditGrp))
					self.sculpt = pm.button(l='Sculpt', h=30, w=100, c=pm.Callback(self.sculpt))
					with pm.columnLayout(rs=3):
						self.applyButton = pm.button(l='Apply', h=30, w=65, en=False, c=pm.Callback(self.apply))
						self.revertButton = pm.button(l='Revert', h=30, w=65, en=False, c=pm.Callback(self.revert))
			
		pm.showWindow(self.mainWindow)

	def getMeshGrp(self):
		if pm.objExists('|%s' %self.shotSculptGrpName) == False:
			self.shotSculptGrp = pm.group(em=True, n=self.shotSculptGrpName)
		else:
			self.shotSculptGrp = pm.PyNode('|%s' %self.shotSculptGrpName)

	def removeTargets(self):
		self.getMeshGrp()
		editGrps = self.shotSculptGrp.getChildren()
		for grp in editGrps:
			meshes = [m for m in grp.getChildren(type='transform') if misc.checkIfPly(m)==True]
			pm.delete(meshes)

	def getOrigMesh(self):
		sel, shp = None, None
		# get selection, expecting animated original mesh.
		try:
			sel = misc.getSel()
			if not sel:
				om.MGlobal.displayError('Please select a transform.')
				return sel, shp

			self.getMeshGrp()
			if sel in self.shotSculptGrp.getChildren():
				sel = sel.attr('animMesh').inputs()[0]

			shp = sel.getShape(ni=True)
			if not shp:
				om.MGlobal.displayError('%s  has no shape.' %sel)
		except:
			return sel, shp

		return sel, shp

	def removeSelectedSculpts(self):
		sel, shp = self.getOrigMesh()

		# make sure this is not an edit mesh under cleanMeshGrp
		if sel in pm.listRelatives(self.shotSculptGrp, ad=True, type='transform'):
			# if user select edit mesh try to get the animMesh
			try:
				editGrp = [i for i in self.shotSculptGrp.getChildren(type='transform') if i == sel.getParent()]
				sel = editGrp[0].animMesh.inputs()[0]
			# om.MGlobal.displayError('Please select animated mesh not edit mesh.')
			return

		self.origMesh = sel

		# find editGrp for current origMesh, if not one, create.
		editGrps = self.shotSculptGrp.getChildren()
		editGrp = None

		for i in shp.message.outputs():
			if i in editGrps:
				editGrp = i
				break

		# delete bsh
		toDel = []
		bshNode = editGrp.bshNode.inputs()
		if bshNode:
			toDel.append(bshNode)
		parNode = editGrp.parBshNode.inputs()
		if parNode:
			toDel.append(parNode) 

		pm.delete(toDel)
		pm.delete(editGrp)

	def removeAllSculpts(self):
		self.getMeshGrp()
		editGrps = self.shotSculptGrp.getChildren()

		# delete blendShapes
		for grp in editGrps:
			toDel = []
			bshNode = grp.bshNode.inputs()
			if bshNode:
				toDel.append(bshNode)
			parNode = grp.parBshNode.inputs()
			if parNode:
				toDel.append(parNode)

			pm.delete(toDel)
		pm.delete(self.shotSculptGrp)


	def sculpt(self):
		sel, shp = self.getOrigMesh()

		# make sure this is not an edit mesh under cleanMeshGrp
		if sel in pm.listRelatives(self.shotSculptGrp, ad=True, type='transform'):
			# if user select edit mesh try to get the animMesh
			try:
				editGrp = [i for i in self.shotSculptGrp.getChildren(type='transform') if i == sel.getParent()]
				sel = editGrp[0].animMesh.inputs()[0]
			# om.MGlobal.displayError('Please select animated mesh not edit mesh.')
			return

		self.origMesh = sel

		# find editGrp for current origMesh, if not one, create.
		editGrps = self.shotSculptGrp.getChildren()
		editGrp = None

		for i in shp.message.outputs():
			if i in editGrps:
				editGrp = i
				break

		if not editGrp:
			nsSplits = sel.nodeName().split(':')
			nodeNameNoNs = nsSplits[-1]
			grpName = '%s__%s' %('__'.join(nsSplits[:-1]), nodeNameNoNs)
			editGrp = pm.group(em=True, n=grpName)

			misc.addMsgAttr(editGrp, 'animMesh')
			pm.connectAttr(shp.message, editGrp.animMesh, f=True)

			misc.hideAttr(editGrp, hide=True, t=True, r=True, s=True, v=True)
			pm.parent(editGrp, self.shotSculptGrp)

		# get current frame number.
		currentFrame = int(pm.currentTime(q=True))

		# get edit mesh, if not one already created, duplicate origMesh.
		editMeshes = editGrp.getChildren()
		editMesh = None
		editGrpName = editGrp.nodeName()

		for m in editMeshes:
			if m.nodeName() == 'f%s_%s' %(currentFrame, editGrpName):
				editMesh = m
				break

		if not editMesh:
			# turn off any shot sculpt bsh first
			# oldValues = self.turnOffCtrls(editGrp)

			editMesh = pm.duplicate(sel)[0]
			misc.cleanUnuseOrigShape([editMesh])
			misc.lockAttr(editMesh, lock=False, t=True, r=True, s=True, v=True)
			editMesh.rename('f%s_%s' %(currentFrame, editGrpName))
			pm.parent(editMesh, editGrp)

			# apply lambert1
			try:
				lambert1 = pm.ls('lambert1', type='lambert')[0]
				pm.hyperShade(editMesh, assign=lambert1)
			except:
				pass

			# display smooth preview
			editShp = editMesh.getShape(ni=True)
			editShp.displaySubdComps.set(shp.displaySubdComps.get())
			editShp.smoothLevel.set(shp.smoothLevel.get())
			editShp.overrideEnabled.set(True)
			editShp.overrideColor.set(self.wfEditColorCode)

			# turn shot sculpt bsh back on
			# self.turnOnCtrls(editGrp, oldValues)

			# add frame number as an attribute for the edit mesh
			misc.addNumAttr(editMesh, 'editFrame', 'long', dv=currentFrame, lock=True, hide=True)

		# hide origMesh shape, unhide edit mesh so user can edit the mesh.
		editMesh.visibility.set(True)

		try:
			shp.visibility.set(False)
			shp.overrideEnabled.set(True)
			shp.overrideColor.set(self.wfOrigColorCode)
		except:
			pass

		# select editMesh. toggle on vertex component mode.
		pm.select(editMesh, r=True)
		# pm.selectMode(component=True)
		# pm.selectType(pv=True)

		self.applyButton.setEnable(True)
		self.revertButton.setEnable(True)
		self.editMesh = editMesh
		om.MGlobal.displayInfo('EDITTING:  f%s  for  %s.' %(currentFrame, self.origMesh)),
	   
	def apply(self):
		if not self.editMesh:
			om.MGlobal.displayError('Invalid selection. Please select editMesh.')
			return

		try:
			self.getMeshGrp()

			# Get editGrp from editMesh parent. 
			editGrp = self.editMesh.getParent()

			# Get origMesh
			editGrpConnections = pm.listConnections(editGrp.attr('animMesh'), s=True, d=False)
			if not editGrpConnections:
				om.MGlobal.displayError('Cannot find original animated mesh from current selected edit mesh.')
				return
			self.origMesh = editGrpConnections[0]
			origShp = self.origMesh.getShape(ni=True)
			origShp.visibility.set(True)
		except:
			return

		currFrame = pm.currentTime(q=True)
		editFrame = self.editMesh.editFrame.get()
		pm.currentTime(editFrame, e=True)
		oldValues = self.turnOffCtrls(editGrp)
		
		origMeshName = editGrp.nodeName()

		invertMesh = None
		# Apply invert mesh for origShape
		if misc.getOrigShape(self.origMesh, False):
			# get existing invert mesh under editMesh transform
			try:
				invertMesh = [shp for shp in self.editMesh.getShapes() if shp.isIntermediate()==True and 'inverted' in shp.nodeName()]
				invertMesh = invertMesh[0]
			except: 
				pass
			
			tmpInvTrans = None
			# if found invert mesh built a new transform for it, unintermediate it
			if invertMesh:
				tmpInvTrans = pm.group(em=True, n='tmpInverted_ply')
				misc.snapTransform('parent', self.editMesh, tmpInvTrans, False, True)
				pm.parent(invertMesh, tmpInvTrans, r=True, s=True)
				invertMesh.intermediateObject.set(False)

			invertShape = idef.invertDeformation(base=self.origMesh, 
												corrective=self.editMesh, 
												invert=tmpInvTrans)
		else:
			invertShape = pm.duplicate(self.editMesh)[0]
			misc.cleanUnuseOrigShape([invertShape])
			misc.lockAttr(invertShape, lock=False, t=True, r=True, s=True, v=True)
			invertShape.rename('f%s_%s_inverted' %(editFrame, origMeshName))


		invertShapeName = invertShape.nodeName()

		# If editGrp do not have ctrl attribute. Add ctrl attr to edit grpH
		attrName = 'f%s' %editFrame
		if editGrp.hasAttr(attrName) == False:   
			ctrlAttr = misc.addNumAttr(editGrp, attrName, 'float', min=0, max=1)
		else:
			ctrlAttr = editGrp.attr(attrName)

		bshNode = None
		index = 0

		# if this is the first time applying changes. Do blendshape to the original mesh.
		if editGrp.hasAttr('bshNode') == False or not editGrp.bshNode.inputs():
			# check if the mesh already has blendshape applied in its rig
			parallel = False
			if [i for i in pm.listHistory(self.origMesh, il=True) if pm.objectType(i) in ['blendShape', 'wrap']]:
				parallel = True

			# create new blendshape
			bshNode = pm.blendShape(invertShape, self.origMesh, n='shotSculpt_%s_bsh' %origMeshName, foc=not(parallel), par=parallel)[0]

			# lock all objectSet associated with the blendshape
			try:
				bshNodeSet = [i for i in pm.listConnections(bshNode.message, d=True, s=False, type='objectSet') if i.isReferenced() == False][0]
				bshNodeSet.rename('shotSculpt_%s_bshSet' %origMeshName)
				# pm.lockNode(bshNodeSet, l=True)
			except:
				om.MGlobal.displayWarning('Failed to rename and lock shotSculpt blendshape set.')

			parNode = None
			try:
				if parallel == True:
					parNode = [i for i in pm.listConnections(bshNode.outputGeometry[0], d=True, s=False, type='blendShape') if i.isReferenced() == False][0]
					parNode.rename('shotSculpt_%s_parallelBsh' %origMeshName)
					parNodeSet = [i for i in pm.listConnections(parNode.message, d=True, s=False, type='objectSet') if i.isReferenced() == False][0]
					parNodeSet.rename('shotSculpt_%s_parallelBshSet' %origMeshName)
					# pm.lockNode(parNodeSet, l=True)
			except:
				om.MGlobal.displayWarning('Failed to rename and lock shotSculpt parallel blendshape set.')

			#connect bsh node message to edit group for later query
			misc.addMsgAttr(editGrp, 'bshNode')
			pm.connectAttr(bshNode.message, editGrp.bshNode, f=True)

			misc.addMsgAttr(editGrp, 'parBshNode')
			if parNode:
				pm.connectAttr(parNode.message, editGrp.parBshNode, f=True)

		# else add to existing blendshape node
		else:
			bshNode = editGrp.bshNode.inputs()[0]
			indexList = bshNode.weightIndexList()

			# if editMesh for this frame do not exists
			if bshNode.hasAttr(invertShapeName) == False:
				index = [i for i in range(0, max(indexList)+2) if i not in indexList][0]
				tg = (self.origMesh, index, invertShape, 1 )
				pm.blendShape(bshNode, e=True, t=tg, w=(index, 1), tc=False)

			# # if this frame has been edited before
			# else:
			# 	bshAttr = bshNode.attr(invertShapeName)
			# 	bshAttr.disconnect()
			# 	index = bshAttr.index()

			# 	# materialize
			# 	tempShp = pm.polyCube(n='TEMP_shotSculptMesh_ply', ch=False)[0].getShape(ni=True)

			# 	pm.connectAttr(bshNode.outputGeometry[0], tempShp.inMesh)
			# 	pm.refresh()
			# 	bshAttr.set(1)
			# 	tempShp.inMesh.disconnect()

			# 	pm.connectAttr(tempShp.worldMesh[0], 
			# 		bshNode.inputTarget[0].inputTargetGroup[index].inputTargetItem[6000].inputGeomTarget, f=True)

			# 	# new we got a target mesh to remove from blendshape node
			# 	pm.blendShape(bshNode, e=True, rm=True, t=(self.origMesh, index, tempShp, 1), tc=False)
				
			# 	# add new target
			# 	pm.blendShape(bshNode, e=True, t=(self.origMesh, index, invertShape, 1 ), w=(index, 1), tc=False)

			# 	# delete temp mesh
			# 	pm.delete(tempShp.getParent())

		# connect ctrl attr
		bshAttr = bshNode.attr('w[%s]' %index)
		if pm.isConnected(ctrlAttr, bshAttr) == False:
			pm.connectAttr(ctrlAttr, bshAttr)
		
		# hide editMesh
		self.editMesh.visibility.set(False)

		# delete invert mesh
		# pm.delete(invertShape)

		# parent invert shpe to editMesh transform, set intermediate true to hide it
		invertShapeShp = invertShape.getShape(ni=True)
		pm.parent(invertShapeShp, self.editMesh, r=True, s=True)
		invertShapeShp.intermediateObject.set(True)
		pm.delete(invertShape)

		# turn ctrl attrs back on
		pm.currentTime(currFrame, e=True)
		self.turnOnCtrls(editGrp, oldValues)

		# set key frame
		pm.setKeyframe(ctrlAttr, v=1.0, t=editFrame)

		pm.select(editGrp, r=True)
		self.applyButton.setEnable(False)
		self.revertButton.setEnable(False)
		om.MGlobal.displayInfo('APPLIED:  f%s  for  %s.' %(editFrame, self.origMesh))
		
	def revert(self):
		editGrp = self.editMesh.getParent()
		bshNode = editGrp.bshNode.inputs()[0]
		bshNode.envelope.set(0)

		# copy the mesh
		origFnMesh = misc.getMfnMesh(self.origMesh.longName())
		origPArray = om.MPointArray()   
		origFnMesh.getPoints(origPArray, om.MSpace.kWorld)

		# get shape node for both editMesh and origMesh
		editMeshShp = self.editMesh.getShape(ni=True)
		origShp = self.origMesh.getShape(ni=True)

		for i in xrange(0, origPArray.length()):
			vtx = '%s.vtx[%i]' %(editMeshShp.longName(), i)
			mel.eval('move -ws %f %f %f %s;' %( origPArray[i].x, 
												origPArray[i].y, 
												origPArray[i].z, 
												vtx
											   ))

		bshNode.envelope.set(1)

		# hide/unhide
		self.apply()

		# disable buttons
		self.applyButton.setEnable(False)
		self.revertButton.setEnable(False)

	def turnOnCtrls(self, editGrp, valueDict):
		for a, v in valueDict.iteritems():
			a.set(v)

	def turnOffCtrls(self, editGrp):
		attrs = editGrp.listAttr(st='f*', se=True, s=True, k=True, sn=True)
		retDict = {}
		for a in attrs:
			retDict[a] = a.get()
			a.set(0)

		return retDict

	def selectEditGrp(self):
		sel = misc.getSel()
		try:
			shp = sel.getShape(ni=True)
			self.getMeshGrp() 
			outputs = shp.message.outputs()
		except:
			return
		
		editGrps = self.shotSculptGrp.getChildren()
		for i in outputs:
			if i in editGrps:
				pm.select(i, r=True)
				break