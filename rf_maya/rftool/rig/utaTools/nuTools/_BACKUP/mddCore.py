import maya.OpenMaya as om
import maya.cmds as mc
import os, struct

import nuTools.plugins.mddRead as mddRead
reload(mddRead)

mddReadPluginPath = '%s\\mddRead.py' %os.path.dirname(mddRead.__file__)
try:
	if not mc.pluginInfo('mddRead.py',q=1,loaded=1):
		mc.loadPlugin(mddReadPluginPath, quiet=1)
except Exception, e:
	om.MGlobal.displayError('Cannot load MDD read plugin from  %s' %mddReadPluginPath)
	raise e


def zero_file(filepath):
	'''
	If a file fails, this replaces it with 1 char, better not remove it?
	'''
	file = open(filepath, 'w')
	file.write('\n') # aparently macosx needs some data in a blank file?
	file.close()


def getDirFromPath (path):
	basename = os.path.basename(path)  
	split = basename.split(".")
	difName = os.path.dirname(path) 

	return (difName, split[0])


def nameToNode(name):
	selectionList = om.MSelectionList()
	selectionList.add(name)
	node = om.MObject()
	selectionList.getDependNode(0, node)

	return node


def getVertexCount(objects):
	numverts = 0
	shape = objects[1]

	# Convert the string name to an actual object
	meshShapeMObj =  nameToNode(shape)

	# Create the vertex iterator with that object
	objectSurfaceIt = om.MItMeshVertex(meshShapeMObj)

	# Get number of vertices	
	numverts = objectSurfaceIt.count()

	return numverts


def checkVertcount(objects, vertcount, mddFile):
	'''
	check and make sure the vertcount is consistent throghout the frame range
	'''
	totalCount = 0
	currentObject = nameToNode(objects[1])
	
	iteratorObject = om.MItMeshVertex(currentObject)
	totalCount += iteratorObject.count()

	if totalCount != vertcount:
		om.MGlobal.displayError('Number of verts has changed during animation cannot export')
		mddFile.close()
		zero_file(filepath)
		return



def getObjectTransform(transform):
	mat_flip = om.MMatrix()
	mat_flip_list = [1.0, 0.0, 0.0, 0.0,0.0, 1.0, 0.0, 0.0,0.0, 0.0, 1.0, 0.0,0.0, 0.0, 0.0, 1.0]
	worldMatrixList = mc.getAttr('%s.worldMatrix'%transform)

	# Convert list to MMatrix object
	temp_mMatrix = om.MMatrix()
	om.MScriptUtil.createMatrixFromList(worldMatrixList, temp_mMatrix)
	transform = temp_mMatrix * mat_flip	
	return transform


def getSelectedVertexPositions(objects):
	vertexPositionList = []

	transform = objects[0]
	shape = nameToNode(objects[1])

	# Convert the string name to an actual object
	transformMatrix = getObjectTransform(transform)

	# Create the surfaceVertex iterator with that object
	objectSurfaceIt = om.MItMeshVertex(shape)

	# Use that iterator for something - here it's returning a list of CV positions
	while not objectSurfaceIt.isDone():
		position = objectSurfaceIt.position() * transformMatrix # returns an object of type MPoint multiplied by transform matrix
		vertexPositionList.append(position.x)
		vertexPositionList.append(position.y)
		vertexPositionList.append(position.z)
		objectSurfaceIt.next()

        #Return the list of CV positions
	return vertexPositionList


def mddWrite(filePath, objects, startFrame, endFrame, fps):
	(difName,basename) = getDirFromPath(filePath)
	filePath = difName+"/"+basename+".mdd"

	mddFile = open(filePath, 'wb')
	numframes = endFrame-startFrame+1

	# set reference frame time
	mc.currentTime( startFrame, edit=True )

	# write the header
	numverts = getVertexCount(objects)
	mddFile.write(struct.pack(">2i", numframes, numverts))

	# write the frame times, sec
	mddFile.write(struct.pack(">%df" % (numframes), *[frame/fps for frame in xrange(numframes)]) ) # seconds	

	# checking vertex count for the model
	checkVertcount(objects, numverts, mddFile)
	
	# Use that iterator for something - here it's returning a list of vertex positions
	vertexPositionList= getSelectedVertexPositions(objects)

	# write out referece model vertex position
	mddFile.write(struct.pack(">%df" % (numverts*3), *[v for v in vertexPositionList]))

	vertexPositionList = None
	amount = 0
	frameRange = (endFrame-startFrame)+1

	mc.progressWindow(	title='Exporting sequence', progress=amount, status='Finished: 0%', isInterruptable=True , maxValue = frameRange)

	for frame in xrange(startFrame, endFrame+1):#in order to start at desired frame
		if mc.progressWindow( query=True, isCancelled=True ) :
			break

		mc.currentTime( frame, edit=True )

		# Check vertex  count, its shouldnt be changed over time
		checkVertcount(objects, numverts, mddFile)
		vertexPositionList= getSelectedVertexPositions(objects)

		# Write the vertex data
		mddFile.write(struct.pack(">%df" % (numverts*3), *[v for v in vertexPositionList]))
		if mc.progressWindow( query=True, progress=True ) >= frameRange  :
			break
		amount += 1

		mc.progressWindow( edit=True, progress=amount, status=('Finished: ' + `amount` + '%' ) )

	mc.progressWindow(endProgress=1)

	vertexPositionList = None
	mddFile.close()


def mddRead(filePath, objects, cycle=False, offset=0):
	# apply deformer
	mddNode = mc.deformer(objects[0], type='mddRead')[0]

	# connect time1
	mc.connectAttr('time1.outTime', '%s.time' %mddNode, f=True)

	# set mdd file path
	mc.setAttr('%s.mddFile' %mddNode, filePath, type='string')
	mc.setAttr('%s.cycle' %mddNode, cycle)
	mc.setAttr('%s.offset' %mddNode, offset)


