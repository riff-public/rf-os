############## PT ##################
# user
# USER_DEPT_DICT = {
# 	'model'		:['Eye', 'Muu', 'Nu', 'Nung', 'Pear', 'Pui', 'Tle', 'Chat'],
# 	'uv'		:['kong', 'Nack', 'Nus'],
# 	'rig'		:['Aou', 'Nunu', 'Ken', 'Nun', 'Pride'],
# 	'pipeline'	:['Nook', 'Ob', 'Preaw', 'Ta'],
# 	'layout'	:['Aom', 'Aor', 'Cherry', 'Eak', 'Hector', 'Jate', 'Leng', 'Pomme'],
# 	'anim'		:['Alpha', 'Bank', 'Bird', 'Boat', 'Bong', 'Daii', 'David', 'Dew', 'Fah', 'Gluay', 'Ifran', 'Kla', 'Kwan', 'May', 'Oh', 'Pable',
# 				  'Pang', 'Pangz', 'Prome', 'Riccardo', 'Tong', 'Zin', 'Zohaib'],
# 	'light'		:['Arm', 'Aum', 'Eric', 'Fluke', 'Max', 'Name', 'Ome'],
# 	'fx'		:['Ram']
# }

# # project directory  projName : [projPath, {ep:subtype}]

# PROJ_DIR_DICT = { 	
# 	'BANG'							: ['P:/BANG',
# 		{'Coin'						: False, 
# 		 'hummelFK'					: False,}],
# 	'Lego_Friends'					: ['P:/Lego_Friends', 
# 		{''							: True }],

# 	'Lego_City'						: ['P:/Lego_City', 
# 		{''							: True ,
# 		'2014_CityPolice' 			: False }],

# 	'Lego_Duplo'					: ['P:/Lego_Duplo', 
# 		{''							: True }],
	
# 	'Lego_TVC' 						: ['P:/Lego_TVC', 
# 		{'Lego_TMNT_2014'			: False,
# 		'Lego_Ninjago_2014'			: False,
# 		'Lego_ChimaSocialG_2014'	: False, 
# 		'Lego_ChimaBAF_2014'		: False, 
# 		'Lego_Movie_2014'			: False, 
# 		'Lego_SuperheroGOTG_2014'	: False,
# 		'Lego_ChimaPT1_2014'		: False,
# 		'Lego_ChimaPT2_2014'		: False,
# 		'Lego_Hobbit_pf2014'		: False,
# 		'Lego_Ninjago_f2014'		: False,
# 		'Lego_SuperHeroC_f2014'		: False,
# 		'Lego_Chima_f2014'			: False,
# 		'Lego_CTY_t2014'			: False}],

# 	'twoWarts'						: ['P:/twoWarts', 
# 		{''							: True }]

# 	# 'x_archive'					: ['P:/x_archive', 
# 	# 	{''							: True }],

# 	# '2013_Fall_LOTR'				: ['P:/z_to_archive/LEGO/TVC/03_LEGO FALL 2013/Lego_LOTR', 
# 	# 	{''							: False }],

# 	# 'Project_X' 					: ['P:/z_to_archive/PT_PROJECTS/Project_X', 
# 	# 	{''							: True }],
							
# 	# 'Lego_Superheroes'			: ['P:/z_to_archive/LEGO/TVC/03_LEGO FALL 2013/Lego_Superheroes', 
# 	# 	{'2013_Fall_SuperheroA' 	: False,
# 	# 	'2013_Fall_SuperheroB' 		: False }],

# 	# 'Lego_Chima'					: ['P:/z_to_archive/LEGO/TVC/03_LEGO FALL 2013/Lego_Chima', 
# 	# 	{'2013_Fall_SG' 			: False, 
# 	# 	'2013_Fall_LegendBeast' 	: False,
# 	# 	'2013_Fall_PT' 				: False, 
# 	# 	'2013_KingOfScorpion' 		: False,
# 	# 	'2013_LavalCragger' 		: False }],

# 	# 'Lego_FriendsX'				: ['P:/z_to_archive/LEGO/FRIENDS/Lego_FriendsX', 
# 	# 	{''							: True }],

# 	# 'Friends_Blooper' 			: ['P:/z_to_archive/LEGO/FRIENDS/Friends_Blooper', 
# 	# 	{''							: True }],
# }

# # drag sync context tool config
# frd_syncInfo = {
# 	1:{'none'       :   {   'x':[   ('crnrMouthIO', 1), ('mouthClench', 0.225), ('loLipsCurlIO', -0.1), ('upLipsCurlIO',-0.1), ('mouthPull', -0.225), ('cheekIO', 0.1),],
#                             'y':[   ('crnrMouthUD', 1), ('cheekLwrIO', 0.334), ('upLipsUD', 0.1667), ('loLipsUD', 0.1667), ('puffIO', 0.1), ('cheekIO', -0.1), ('ebInnerUD', 1), ('ebMidUD', 0.334), ('ebOuterUD', 0.125), ('upLidTW', 0.1), ('loLidTW', 0.05)]}, 
#         'ctrl'      :   {   'x':[   ('lipsPartIO', 0.5)],
#                             'y':[   ('loLipsUD', 1), ('loLidUD', 1)]},
#         'shift'     :   {   'x':[   ('lipsPartIO', 0.5)],
#                             'y':[   ('upLipsUD', 1), ('upLidUD', 1)]}},
#     2:{ 'none'      :   {   'x':[   ('rotateY', 1), ('ebIO', 1)],
#                             'y':[   ('rotateX', -1), ('mouthUD', 0.1), ('ebUD', 1)]},
#         'ctrl'      :   {   'x':[   ('translateX', 0.0334)],
#                             'y':[   ('translateY', 0.0334)]},
#         'shift'     :   {   'x':[   ('mouthLR', 1), ('cheekIO', 0.1)],
#                             'y':[   ('mouthUD', 1), ('cheekLwrIO', 0.1)]}}}



########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################

def mapLocalEnv(localRoot):
	import sys
	import maya.mel as mel

	try:
		# append python local path
		sys.path.append('%s/python' %localRoot)

		# get mel local path
		mayaScriptPath = mel.eval('getenv "MAYA_SCRIPT_PATH";')
		mayaPluginPath = mel.eval('getenv "MAYA_PLUG_IN_PATH";')
		mayaXbmLanguagePath = mel.eval('getenv "XBMLANGPATH";')

		# append path
		mayaScriptPath += ';%s/mel' %localRoot
		mayaPluginPath += ';%s/plugin' %localRoot
		mayaXbmLanguagePath += ';%s/icons' %localRoot

		# add paths
		mel.eval('putenv "MAYA_SCRIPT_PATH" "%s";' %mayaScriptPath)
		mel.eval('putenv "MAYA_PLUG_IN_PATH" "%s";' %mayaPluginPath)
		mel.eval('putenv "XBMLANGPATH" "%s";' %mayaXbmLanguagePath)	

		# refresh 
		mel.eval('rehash;')			

	except:
		mel.eval('Error: cannot add local env paths...')

# tool directory
GLOBAL_SYSTOOL_PATH = 'P:/sysTool'

TOOL_DIR = '%s/python/nuTools' %GLOBAL_SYSTOOL_PATH
TEMPLATE_DIR = '%s/python/nuTools/template' %GLOBAL_SYSTOOL_PATH
BTP_DIR = '%s/python/nuTools/btp' %GLOBAL_SYSTOOL_PATH
BSH_PUB_LOG_DIR = '%s/tmp/bshPublish.log' %GLOBAL_SYSTOOL_PATH
RIG_MOD_PKG_DIR = {	'pkmel':'%s/python/pkmel' %GLOBAL_SYSTOOL_PATH, 
					'nuTools.rigTools':'%s/python/nuTools/rigTools' %GLOBAL_SYSTOOL_PATH}

MAYAPY_PATH = 'c:/program files/autodesk/maya2012/bin/mayapy.exe'

# tool filter
RIG_MOD_FILTER = ['crm_ui', 'weightTools', 'weightPuller', 'rigTools', 'core', 'ctrlShapeTools', 'controlMaker', 'skinTools', 'matcher', 'baseRig']
TEMP_CLS_FILTER = ['TempJoint', 'TempJoints', 'MirrorTempConnection']
ASSET_TYPE_FILTER = ['_textures', '_temp', 'images', 'light', 'mattePaint', 'movies', 'particles', 'renderData']
FILM_EP_FILTER = ['temp', 'edit', 'comment']


NODE_TYPE_DICT = {	'multiplyDivide':'mdv', 
					'multDoubleLinear':'mdl',
					'plusMinusAverage':'pma',
					'avgCurves':'avgCrvs',
					'addDoubleLinear':'adl',
					'blendColors':'bcol',
					'blendTwoAttr':'btAttr',
					'condition':'cond',
					'clamp':'cmp',
					'curveInfo':'cif',
					'cluster':'cls',
					'curveFromSurfaceIso':'cfsi',
					'distanceBetween':'dist',
					'locator':'loc',
					'loft':'loft',
					'reverse':'rev',
					'pointOnSurfaceInfo':'posi',
					'pointOnCurveInfo':'poci',
					'polyEdgeToCurve':'plyEtc',
					'setRange':'sr',
					'blendShape':'bsh',
					'lattice':'lat',
					'pairBlend':'pBlnd',
					'polySmoothFace':'plySmf',
					'joint':'jnt',
					'skinCluster':'skc',
					'angleBetween':'anBtw',
					'decomposeMatrix':'dMtx',
					'ikHandle':'ikHndl',
					'vectorProduct':'vecPd',
					'wire':'wire',
					'wrap':'wrp'
}




USER_DEPT_DICT = {}
PROJ_DIR_DICT = { 'nine' : 'P:/nine/all'}



