import pymel.all as pm
import maya.cmds as mc
from nuTools import misc
from nuTools import network as nw
from nuTools import controller as ctrl

class BaseRig(object):
	def __init__(self, name):
		self.network = nw.Network(name=name)

	def create(self):
		print 'Creating rig groups..'
		rigGrp = pm.group(em=True, n='Rig_Grp')

		superRootGrp = pm.group(em=True, n='SuperRoot_Grp')
		textureGrp = pm.group(em=True, n='Texture_Grp')
		lightGrp = pm.group(em=True, n='Light_Grp')
		geoGrp = pm.group(em=True, n='Geo_Grp')
		pm.parent([superRootGrp, textureGrp, lightGrp, geoGrp], rigGrp)

		utilGrp = pm.group(em=True, n='Util_Grp')
		animUtilGrp = pm.group(em=True, n='Anim_Util_Grp')
		blockingUtilGrp = pm.group(em=True, n='Blocking_Util_Grp')
		pm.parent([animUtilGrp, blockingUtilGrp], utilGrp)

		aJointGrp = pm.group(em=True, n='Joint_Grp')
		aCurveGrp = pm.group(em=True, n='Curve_Grp')
		aSurfaceGrp = pm.group(em=True, n='Surface_Grp')
		aLocatorGrp = pm.group(em=True, n='Locator_Grp')
		aIkHandleGrp = pm.group(em=True, n='IkHandle_Grp')
		aClusterGrp = pm.group(em=True, n='Cluster_Grp')
		pm.parent([aJointGrp,aCurveGrp,aSurfaceGrp,aLocatorGrp,aIkHandleGrp,aClusterGrp], animUtilGrp)

		bJointGrp = pm.group(em=True, n='Joint_Grp')
		bCurveGrp = pm.group(em=True, n='Curve_Grp')
		bSurfaceGrp = pm.group(em=True, n='Surface_Grp')
		bLocatorGrp = pm.group(em=True, n='Locator_Grp')
		bIkHandleGrp = pm.group(em=True, n='IkHandle_Grp')
		bClusterGrp = pm.group(em=True, n='Cluster_Grp')
		pm.parent([bJointGrp,bCurveGrp,bSurfaceGrp,bLocatorGrp,bIkHandleGrp,bClusterGrp], blockingUtilGrp)

		proxyGeoGrp = pm.group(em=True, n='Proxy_Geo_Grp')
		blockingGeoGrp = pm.group(em=True, n='Blocking_Geo_Grp')
		animGeoGrp = pm.group(em=True, n='Anim_Geo_Grp')
		pm.parent([proxyGeoGrp, blockingGeoGrp, animGeoGrp], geoGrp)

		print 'Creating root ctrl...'
		superRootCtrl = ctrl.Controller(name='SuperRoot_Ctrl', st='directionalSquare', scale=4)
		superRootCtrl.setColor('yellow')
		pm.parent([superRootCtrl, utilGrp], superRootGrp)

		rootCtrl = ctrl.Controller(name='Root_Ctrl', st='oneDirectionFatArrow', scale=2)
		rootCtrl.setColor('yellow')
		rootCtrlGrp = pm.group(em=True, n='Root_Ctrl_Grp')
		pm.parent(rootCtrl, rootCtrlGrp)
		pm.parent(rootCtrlGrp, superRootCtrl)
		
		
		animCtrlGrp = pm.group(em=True, n='Anim_Ctrl_Grp')
		blockingCtrlGrp = pm.group(em=True, n='Blocking_Ctrl_Grp')
		pm.parent([animCtrlGrp, blockingCtrlGrp], rootCtrl)

		print 'Adding Rig_Grp pipeline attributes...'
		rigGrp.addAttr('assetID', at='long', dv=0)
		rigGrp.addAttr('assetType', dt='string')
		rigGrp.addAttr('assetSubType', dt='string')
		rigGrp.addAttr('assetName', dt='string')
		rigGrp.addAttr('assetShader', dt='string')
		rigGrp.addAttr('proxies', at='message')
		rigGrp.addAttr('project', dt='string')

		rigGrp.assetType.set('{assetType}')
		rigGrp.assetSubType.set('{assetSubType}')
		rigGrp.assetName.set('{assetName}')
		rigGrp.assetShader.set('{assetShader}')

		print 'Creating sets...'
		pm.sets([animCtrlGrp, animUtilGrp, animGeoGrp], n='Anim_Set')
		pm.sets([blockingCtrlGrp, blockingGeoGrp, blockingUtilGrp], n='Blocking_Set')
		pm.sets([animCtrlGrp, animUtilGrp, proxyGeoGrp], n='Proxy_Set')
		pm.sets(animGeoGrp, n='Render_Set')

		print 'Storing variables and connecting to network...'

		self.rigGrp = rigGrp
		self.superRootGrp = superRootGrp
		self.superRootCtrl = superRootCtrl
		self.rootCtrl = rootCtrl
		self.animCtrlGrp = animCtrlGrp
		self.blockingCtrlGrp = blockingCtrlGrp
		self.utilGrp = utilGrp

		self.animUtilGrp = animUtilGrp
		self.aJointGrp = aJointGrp
		self.aCurveGrp = aCurveGrp
		self.aSurfaceGrp = aSurfaceGrp
		self.aLocatorGrp = aLocatorGrp
		self.aIkHandleGrp = aIkHandleGrp
		self.aClusterGrp = aClusterGrp

		self.blockingUtilGrp = blockingUtilGrp
		self.bJointGrp = bJointGrp
		self.bCurveGrp = bCurveGrp
		self.bSurfaceGrp = bSurfaceGrp
		self.bLocatorGrp = bLocatorGrp
		self.bIkHandleGrp = bIkHandleGrp
		self.bClusterGrp = bClusterGrp

		self.textureGrp = textureGrp
		self.lightGrp  = lightGrp

		self.geoGrp  = geoGrp   
		self.proxyGeoGrp = proxyGeoGrp
		self.blockingGeoGrp = blockingGeoGrp
		self.animGeoGrp = animGeoGrp 

		self.network.connectOut(self.rigGrp, 'rigGrp')
		self.network.connectOut(self.superRootGrp, 'superRootGrp')
		self.network.connectOut(self.superRootCtrl, 'superRootCtrl')
		self.network.connectOut(self.rootCtrl, 'rootCtrl')
		self.network.connectOut(self.animCtrlGrp, 'animCtrlGrp')
		self.network.connectOut(self.blockingCtrlGrp, 'blockingCtrlGrp')
		self.network.connectOut(self.utilGrp, 'utilGrp')

		self.network.connectOut(self.animUtilGrp, 'animUtilGrp')
		self.network.connectOut(self.aJointGrp, 'aJointGrp')
		self.network.connectOut(self.aCurveGrp, 'aCurveGrp')
		self.network.connectOut(self.aSurfaceGrp, 'aSurfaceGrp')
		self.network.connectOut(self.aLocatorGrp, 'aLocatorGrp')
		self.network.connectOut(self.aIkHandleGrp, 'aIkHandleGrp')
		self.network.connectOut(self.aClusterGrp, 'aClusterGrp')

		self.network.connectOut(self.blockingUtilGrp, 'blockingUtilGrp')
		self.network.connectOut(self.bJointGrp, 'bJointGrp')
		self.network.connectOut(self.bCurveGrp, 'bCurveGrp')
		self.network.connectOut(self.bSurfaceGrp, 'bSurfaceGrp')
		self.network.connectOut(self.bLocatorGrp, 'bLocatorGrp')
		self.network.connectOut(self.bIkHandleGrp, 'bIkHandleGrp')
		self.network.connectOut(self.bClusterGrp, 'bClusterGrp')

		self.network.connectOut(self.textureGrp, 'textureGrp')
		self.network.connectOut(self.lightGrp, 'lightGrp')

		self.network.connectOut(self.geoGrp, 'geoGrp')
		self.network.connectOut(self.proxyGeoGrp,'proxyGeoGrp')
		self.network.connectOut(self.blockingGeoGrp, 'blockingGeoGrp')
		self.network.connectOut(self.animGeoGrp, 'animGeoGrp')


		returnDict = {'rigGrp':rigGrp, 'superRootGrp':superRootGrp, 'superRootCtrl':superRootCtrl, 'rootCtrl':rootCtrl,
					 'animCtrlGrp':animCtrlGrp, 'blockingCtrlGrp':blockingCtrlGrp, 'utilGrp':utilGrp, 'animUtilGrp':animUtilGrp, 
					 'aJointGrp':aJointGrp, 'aCurveGrp':aCurveGrp, 'aSurfaceGrp':aSurfaceGrp, 'aLocatorGrp':aLocatorGrp, 
					 'aIkHandleGrp':aIkHandleGrp, 'aClusterGrp':aClusterGrp, 'blockingUtilGrp':blockingUtilGrp, 'bJointGrp':bJointGrp, 
					 'bCurveGrp':bCurveGrp, 'bSurfaceGrp':bSurfaceGrp, 'bLocatorGrp':bLocatorGrp, 'bIkHandleGrp':bIkHandleGrp, 
					 'bClusterGrp':bClusterGrp, 'textureGrp':textureGrp, 'lightGrp':lightGrp, 'geoGrp':geoGrp, 'proxyGeoGrp':proxyGeoGrp, 
					 'blockingGeoGrp':blockingGeoGrp, 'animGeoGrp':animGeoGrp}
		return returnDict

	def autoFillRigGrp(self):
		rigGrp = self.rigGrp
		result = misc.autoFillRigGrp(rigGrp=rigGrp)
		return result


##############################################################################################################################
##############################################################################################################################
##############################################################################################################################
##############################################################################################################################
##############################################################################################################################
##############################################################################################################################


#Base Body part class
class JointPart(object):
	def __init__(self, jnts, parentNetwork, element, position):
		self.network = nw.PartNetwork(name='%s_Part' %element)
		
		self.element = element
		self.position = position
		self.parentNetwork = parentNetwork
		self.jnts = self.castJnts(jnts)

		#setting and connecting
		self.network.connectToParentNetwork(parentNetwork)
		self.network.setElem(element)
		self.network.setPos(position)
		self.network.connectOut(self.jnts, 'jnts')

	def castJnts(self, jnts):
		if not isinstance(jnts, (list, tuple)):
			jnts = [jnts]
		joints = []
		for j in jnts:
			pyNode = None
			if not isinstance(j, pm.nt.Joint):
				try:
					pyNode = pm.PyNode(j)
				except:
					continue
				if isinstance(pyNode, pm.nt.Joint):
					joints.append(pyNode)
			else:
				joints.append(j)

		if len(joints) < 1:
			pm.error('No input joint.')
		return joints

	def checkJntNum(self, jnts, num):
		result = False
		#check jnt num first
		if num == 'inf' and len(jnts) > 0:
			result = True
		if len(self.jnts) == num:
			result = True
		return result
			

class SingleJoint(JointPart):
	def __init__(self, jnts, parentNetwork, element, position):
		JointPart.__init__(self, jnts, parentNetwork, element, position)
		self.checkJntNum(jnts, 1)


class SingleChainJoint(JointPart):
	def __init__(self, jnts, parentNetwork, element, position):
		JointPart.__init__(self, jnts, parentNetwork, element, position)
		self.checkJntNum(jnts, 2)

##############################################################################################################################
##############################################################################################################################
##############################################################################################################################
##############################################################################################################################
##############################################################################################################################
##############################################################################################################################


#The base rig class. All rig class will inherit from this class.
class RigObject(object):
	def __init__(self, bodyPartNetwork, parent):
		self.rigPrefix = ''

		self.bodyPartNetwork = bodyPartNetwork

		self.element = self.bodyPartNetwork.getElem()
		self.position = self.bodyPartNetwork.getPos()
		self.jnts = self.bodyPartNetwork.getJnts()
		self.network = nw.RigNetwork(name='%s_Rig' %self.element)
		self.parent = self.getParent(parent)

		#setting and connecting
		self.network.connectToParentNetwork(bodyPartNetwork)
		self.network.setElem(self.element)
		self.network.setPos(self.position)
		self.network.connectIn(self.parent, 'parent')

		#future attributes	
		self.method = None
		self.controller = None
		self.gimbalCtrl = None
		self.rigConstraints = None
		self.partConstraints = None
		self.rigGrp = None
		self.jntGrp = None
		self.rigJnts = None

	def rigJntName(self):
		pass

	def getParent(self, parent):
		parents = []
		if not isinstance(parent, (list, tuple)):
			parent = [parent]
		for p in parent:
			pyNode = None
			if isinstance(p, str):
				try:
					pyNode = pm.PyNode(p)
				except:
					continue
				parents.append(pyNode)
			else:
				parents.append(p)

		if len(parents) < 1:
			pm.error('No valid parent(s)')
		return parents

class SingleJointParentRig(RigObject):
	def __init__(self, bodyPartNetwork, parent=None):
		RigObject.__init__(self, bodyPartNetwork, parent)
		self.rigPrefix = 'SJP'
		self.method = 'SingleJointParent'

	def createRigJnt(self, jnt):
		jntName = misc.nameObj('%s%s' %(self.rigPrefix, self.element), self.position, 'jnt')
		rigJnt = pm.duplicate(jnt, po=True, n=jntName)[0]
		return rigJnt

	def rig(self, ctrlShp='crossCircle', ctrlColor='yellow'):
		#crate main group
		rigGrpName = misc.nameObj(elem='%s%sRig' %(self.rigPrefix, self.element), pos=self.position, typ='grp')
		rigGrp = pm.group(em=True, n=rigGrpName)

		jntGrpName = misc.nameObj(elem='%s%sJnt' %(self.rigPrefix, self.element), pos=self.position, typ='grp')
		jntGrp = pm.group(em=True, n=jntGrpName)

		#get self.jnts out of list
		self.jnts = self.jnts[0]

		#create rig joint
		rigJnt = self.createRigJnt(self.jnts)
		pm.parent(rigJnt, jntGrp)

		ctrlName = misc.nameObj(elem=self.element, pos=self.position, typ='ctrl')
		controller = ctrl.Controller(name=ctrlName, st=ctrlShp)
		controller.setColor(ctrlColor)
		controller.lockAttr(v=True)
		controller.hideAttr(v=True)
		gimbalCtrl = controller.addGimbal()
		zgrp = misc.zgrp(controller, suffix='zgrp')
		pm.parent(zgrp, rigGrp)

		rigParCons = []
		#constraint to rig joint
		jntParCons = pm.parentConstraint(gimbalCtrl, rigJnt)
		rigParCons.append(jntParCons)

		#constraint skin joint to rig joint
		partParCons = pm.parentConstraint(rigJnt, self.jnts)

		#find Anim_Ctrl_Grp and parent rigGrp to it..
		mainNetwork = self.bodyPartNetwork.getParentNetwork()
		pm.parent(rigGrp, mainNetwork.animCtrlGrp.get())
		pm.parent(jntGrp, mainNetwork.aJointGrp.get())

		#connect to parent
		if self.parent:
			parentParCons = pm.parentConstraint(self.parent, rigGrp)
			rigParCons.append(parentParCons)

		#registering to var
		self.method = self.method
		self.controller = controller
		self.gimbalCtrl = gimbalCtrl
		self.rigConstraints = rigParCons
		self.partConstraints = partParCons
		self.rigGrp = rigGrp
		self.rigJnts = rigJnt

		#registering the same to network node
		self.network.setMethod(self.method)
		self.network.connectOut(rigGrp, 'rigGrp')
		self.network.connectOut(jntGrp, 'jntGrp')
		self.network.connectOut(rigParCons, 'rigConstraints')
		self.network.connectOut(partParCons, 'partConstraints')
		self.network.connectOut(controller, 'ctrls')
		self.network.connectOut(rigJnt, 'jnts')

		#connecting __toDel

	def remove(self):
		pass