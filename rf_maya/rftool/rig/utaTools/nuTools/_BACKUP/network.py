import pymel.all as pm
from nuTools import misc

VERSION = 'beta 1.00'

def getMainNetwork(chaName):
    networks = Network.list()
    for network in networks:
        if network.getChaName() == chaName and network.getType() == 'main_network':
            return network


########################################################################
#################################NETWORK################################
########################################################################

class Network(pm.nt.Network):
    NODE_TYPE = 'main_network'
    @classmethod
    def list(cls,*args,**kwargs):
        """ 
        Returns all instances the subnode in the scene 
        """
        kwargs['type'] = cls.__melnode__
        return [ node for node in pm.ls(*args,**kwargs) if isinstance(node,cls)]

    @classmethod
    def _isVirtual( cls, obj, name ):
        fn = pm.api.MFnDependencyNode(obj)
        try:
            if fn.hasAttribute('TYPE'):
                plug = fn.findPlug('TYPE')
                if plug.asString() == 'main_network':
                    return True
                return False
        except:
            pass
        return False

    @classmethod
    def _preCreateVirtual(cls, **kwargs):
        postKwargs = {}
        retKwargs = {}
        parentNetwork = None
        chaName = ''
        #name arg
        if 'n' in kwargs:
            name = kwargs.pop( 'n' )
        elif 'name' in kwargs:
            name = kwargs.get( 'name' )
        elif 'n' not in kwargs:
            name = ''

        postKwargs = {'name': name}

        #parentNetwork arg
        if 'pnw' in kwargs:
            parentNetwork = kwargs.pop( 'pnw' )
        elif 'parentNetwork' in kwargs:
            parentNetwork = kwargs.get( 'parentNetwork' )

        if parentNetwork:
            postKwargs['parentNetwork'] = parentNetwork
            chaName = parentNetwork.chaName.get()
            postKwargs['name'] = chaName

        retKwargs['name'] = name

        return retKwargs, postKwargs
    @classmethod
    def _postCreateVirtual(cls, newNode, **postKwargs):
        """
        This is called after creation, pymel/cmds allowed. Treat newNode as self.  
        Will add basic attribute for a network node.
        """
        newNode.addAttr('VERSION', dt='string')
        newNode.addAttr('TYPE', dt='string')

        newNode.TYPE.set('main_network')
        newNode.TYPE.lock()   
                
        newNode.VERSION.set(VERSION)
        newNode.VERSION.lock()

        newNode.addAttr('chaName', dt='string')
        newNode.addAttr('globalScale', at='double')
        newNode.addAttr('childNetwork', at='message')

        try:
            newNode.chaName.set(postKwargs['name'])
            newNode.chaName.lock()
        except:
            pass

    def get(self, channel):
        if self.hasAttr(channel):
            attr = self.attr(channel)
            typ = attr.type()
            if typ == 'message':
                ret = self.attr(channel).listConnections(s=False, d=True)
            else:
                ret = attr.get()
            return ret

    def source(self, channel):
        if self.hasAttr(channel):
            attr = self.attr(channel)
            typ = attr.type()
            if typ == 'message':
                ret = attr.listConnections(s=True, d=False)
                if ret:
                    ret = ret[0]
                return ret

    def getVersion(self):
        return self.VERSION.get()

    def getType(self):
        return self.TYPE.get()

    def getChaName(self):
        return self.chaName.get()

    def setChaName(self, chaName):
        if self.chaName.isLocked():
            self.chaName.unlock()
        self.chaName.set(chaName)
        self.chaName.lock()

    def connectOut(self, obj, channel):
        if not isinstance(obj, (list, tuple)):
            obj = [obj]
        for i in obj:
            if not i.hasAttr('network'):
                misc.addMsgAttr(i, 'network')
            if not self.hasAttr(channel):
                self.addMsgAttr(channel)
            try:
                pm.connectAttr(self.attr(channel), i.attr('network'))
            except:
                pass

    def connectIn(self, obj, channel):
        if not isinstance(obj, (list, tuple)):
            obj = [obj]
        if not self.hasAttr(channel):
            misc.addMsgAttr(self, channel)
        for i in obj:   
            pm.connectAttr(i.attr('message'), self.attr(channel))

    def addMsgAttr(self, attr):
        if self.hasAttr(attr):
            if self.attr(attr).type() == 'message':
                return self.attr(attr)
            else:
                pm.error('Same attibute name of other type exists!')

        pm.addAttr(self, ln=attr, at='message')
        newAttr = self.attr(attr)
        return newAttr

    def addStrAttr(self, attr, txt=''):
        if self.hasAttr(attr):
            if self.attr(attr).type() == 'string':
                return self.attr(attr)
            else:
                pm.error('Same attibute name of other type exists!')

        pm.addAttr(self, ln=attr, dt='string')
        newAttr = self.attr(attr)
        if txt:
            pm.setAttr(newAttr, txt, type='string')     
        return newAttr

pm.factories.registerVirtualClass(Network, nameRequired=False)


########################################################################
###############################PART NETWORK#############################
########################################################################

class PartNetwork(Network):
    SUBNODE_TYPE = 'part_network'
    @classmethod
    def _isVirtual( cls, obj, name ):
        fn = pm.api.MFnDependencyNode(obj)
        try:
            if fn.hasAttribute('TYPE'):
                plug = fn.findPlug('TYPE')
                if plug.asString() == cls.SUBNODE_TYPE:
                    return True
                return False
        except:
            pass
        return False

    @classmethod
    def _postCreateVirtual(cls, newNode, **postKwargs):
        #Network._postCreateVirtual(newNode)
        newNode.addAttr('TYPE', dt='string')
        newNode.TYPE.set('part_network')
        newNode.TYPE.lock()
        newNode.addAttr('childNetwork', at='message')
        #newNode.chaName.set(postKwargs['name'])
        #newNode.chaName.lock()

        newNode.addAttr('parentNetwork', at='message')
        newNode.addAttr('jnts', at='message')
        newNode.addAttr('element', dt='string') 
        newNode.addAttr('position', dt='string')

        #pm.connectAttr(postKwargs['parentNetwork'].childNetwork, newNode.parentNetwork)

    def getParentNetwork(self):
        return self.source('parentNetwork')

    def getJnts(self):
        return self.get('jnts')

    def getElem(self):
        return self.element.get()

    def setElem(self, elem):
        if self.element.isLocked():
            self.element.unlock()
        result = self.element.set(elem)
        self.element.lock()
        return result

    def getPos(self):
        return self.position.get()

    def setPos(self, pos):
        if self.position.isLocked():
            self.position.unlock()
        result = self.position.set(pos)
        self.position.lock()
        return result

    def connectToParentNetwork(self, parent):
        if not self.hasAttr('parentNetwork'):
            misc.addMsgAttr(self, 'parentNetwork')   
        pm.connectAttr(parent.attr('childNetwork'), self.attr('parentNetwork'))

pm.factories.registerVirtualClass(PartNetwork, nameRequired=False)


########################################################################
###############################RIG NETWORK##############################
########################################################################

class RigNetwork(PartNetwork):
    SUBNODE_TYPE = 'rig_network'
    @classmethod
    def _isVirtual( cls, obj, name ):
        fn = pm.api.MFnDependencyNode(obj)
        try:
            if fn.hasAttribute('TYPE'):
                plug = fn.findPlug('TYPE')
                if plug.asString() == cls.SUBNODE_TYPE:
                    return True
                return False
        except:
            pass
        return False

    @classmethod
    def _postCreateVirtual(cls, newNode, **postKwargs):
        #Network._postCreateVirtual(newNode)
        newNode.addAttr('TYPE', dt='string')
        newNode.TYPE.set('rig_network')
        newNode.TYPE.lock()
        #newNode.chaName.set(postKwargs['name'])
        #newNode.chaName.lock()

        newNode.addAttr('parent', at='message')
        newNode.addAttr('ctrls', at='message')
        newNode.addAttr('jnts', at='message')
        newNode.addAttr('rigConstraints', at='message')
        newNode.addAttr('partConstraints', at='message')
        newNode.addAttr('rigGrp', at='message')
        newNode.addAttr('jntGrp', at='message')
        newNode.addAttr('utilGrp', at='message')
        newNode.addAttr('__toDel', at='message')

        newNode.addAttr('element', dt='string') 
        newNode.addAttr('position', dt='string')
        newNode.addAttr('method', dt='string') 

    def getMethod(self):
        return self.method.get()

    def setMethod(self, method):
        if self.method.isLocked():
            self.method.unlock()
        result = self.method.set(method)
        self.method.lock()
        return result

pm.factories.registerVirtualClass(RigNetwork, nameRequired=False)



