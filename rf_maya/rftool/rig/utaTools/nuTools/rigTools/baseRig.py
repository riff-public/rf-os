import pymel.core as pm
import maya.OpenMaya as om

from utaTools.nuTools import misc
reload(misc)

DEFAULT_ELEMENT = 'limb'
DEFAULT_SIDE = ''
DEFAULT_SIZE = 1.00
DEFAULT_ROTATEORDER = 'zxy'

class BaseRig(object):
	"""
	The base class for all the rig classes.

	"""
	def __init__(self, 
				parent=None,
				animGrp=None, 
				skinGrp=None, 
				utilGrp=None, 
				stillGrp=None, 
				elem=DEFAULT_ELEMENT, 
				side=DEFAULT_SIDE, 
				size=DEFAULT_SIZE,
				rotateOrder=DEFAULT_ROTATEORDER,
				**kwargs):

		if 'rigGrp' in kwargs:
			rigGrp = kwargs['rigGrp']

			self.animGrp = rigGrp.animGrp
			self.skinGrp = rigGrp.skinGrp
			self.utilGrp = rigGrp.utilGrp
			self.stillGrp = rigGrp.stillGrp
		else:
			if isinstance(animGrp, (str, unicode)) == True:
				try:
					animGrp = pm.PyNode(animGrp)
				except Exception, e:
					print e
					om.MGlobal.displayError('Cannot cast %s to PyNode!' %(animGrp))
			self.animGrp = animGrp

			if isinstance(skinGrp, (str, unicode)) == True:
				try:
					skinGrp = pm.PyNode(skinGrp)
				except Exception, e:
					print e
					om.MGlobal.displayError('Cannot cast %s to PyNode!' %(skinGrp))
			self.skinGrp = skinGrp

			if isinstance(utilGrp, (str, unicode)) == True:
				try:
					utilGrp = pm.PyNode(utilGrp)
				except Exception, e:
					print e
					om.MGlobal.displayError('Cannot cast %s to PyNode!' %(utilGrp))
			self.utilGrp = utilGrp

			if isinstance(stillGrp, (str, unicode)) == True:
				try:
					stillGrp = pm.PyNode(stillGrp)
				except Exception, e:
					print e
					om.MGlobal.displayError('Cannot cast %s to PyNode!' %(stillGrp))
			self.stillGrp = stillGrp


		self.parent = parent
		self.elem = elem
		self.side = side
		self.size = size
		self.rotateOrder = rotateOrder
		self.network = None
		# self.createNetwork()

	def createNetwork(self):
		clsName = self.__class__.__name__
		self.network = pm.createNode('network', name='%s%s_%s' %(self.elem, self.side, clsName))
		
		modStr = self.__class__.__module__
		misc.addStrAttr(obj=self.network, attr='__module__', 
			txt=modStr, lock=True)

		misc.addStrAttr(obj=self.network, attr='__class__', 
			txt=clsName, lock=True)

	def register_str(self, name, text):
		misc.addStrAttr(obj=self.network, attr='name', 
			txt=text, lock=True)

	def register_node(self, node, name):
		if not self.network.hasAttr(name):
			misc.addMsgAttr(self.network, name)
		pm.connectAttr(self.network.name, node.message, f=True)

	def register(self):
		pass
		# for name, value in vars(self):

	def lockInf(self, jnts, value):
		for j in jnts:
			j.lockInfluenceWeights.set(value)

	def jntsArgs(self, jnts):
		if not jnts:
			return

		single = False
		if not isinstance(jnts, (list, tuple, set)):
			jnts = [jnts]
			single = True

		joints = []
		for j in jnts:
			if not isinstance(j, pm.PyNode):
				try:
					j = pm.PyNode(j)
				except Exception, e:
					print e
					pass
			joints.append(j)

		if joints:
			if single == True:
				return joints[0]
			else:
				return joints