import pymel.core as pm

from nuTools import misc, controller
reload(misc)
reload(controller)
import nuTools.rigTools.baseRig as baseRig
reload(baseRig)

class LegRig(baseRig.BaseRig):
	def __init__(self, 
				jnts=[],
				aimAxis='y',
				upAxis='z',
				fkRotateOrder='yzx',
				ikRotateOrder='xzy',
				createFootRig=True,
				**kwargs):
		super(LegRig, self).__init__(**kwargs)
		# temp joints
		self.tmpJnts = self.jntsArgs(jnts)

		# axis
		self.aimAxis = aimAxis
		self.upAxis = upAxis
		self.fkRotateOrder = fkRotateOrder
		self.ikRotateOrder = ikRotateOrder
		self.createFootRig = createFootRig

		# joints
		self.jnts = []
		self.fkJnts = []
		self.ikJnts = []

		# controllers
		self.settingCtrl = []
		self.fkCtrls = []
		self.fkGCtrls = []
		self.ikCtrls = []
		self.ikGCtrls = []

		# groups
		self.fkCtrlZgrps = []
		self.ikCtrlZgrps = []

		# Iks
		self.ikHndls = []
		self.ikEffs = []
		self.ikHndlZgrps = []

	def rig(self):
		# --- translate axis from string to vector
		# aimAxis = misc.vectorStr(self.aimAxis)
		aimTran = 't%s' %(self.aimAxis[-1])

		# upAxis = misc.vectorStr(self.upAxis)
		self.bendAxis = misc.crossAxis(self.aimAxis, self.upAxis)[-1]

		# --- get class name to use for naming
		_name = (self.elem, self.side)

		# --- create main groups
		self.rigCtrlGrp = pm.group(em=True, n='%sRig%s_grp' %_name)
		self.rigFkCtrlGrp = pm.group(em=True, n='%sFkCtrl%s_grp' %_name)
		self.rigIkCtrlGrp = pm.group(em=True, n='%sIkCtrl%s_grp' %_name)
		pm.parent([self.rigFkCtrlGrp, self.rigIkCtrlGrp], self.rigCtrlGrp)

		self.rigUtilGrp = pm.group(em=True, n='%sUtil%s_grp' %_name)
		self.rigIkhGrp = pm.group(em=True, n='%sIkh%s_grp' %_name)
		# self.rigUtilGrp.visibility.set(False)
		self.rigIkhGrp.visibility.set(False)
		pm.parent([self.rigIkhGrp], self.rigUtilGrp)

		# self.rigSkinGrp = pm.group(em=True, n='%sSkin%s_grp' %_name)
		# self.rigStillGrp = pm.group(em=True, n='%sStill%s_grp' %_name)
		# self.rigStillGrp.visibility.set(False)

		# parent main grps
		pm.parent(self.rigCtrlGrp, self.animGrp)
		pm.parent(self.rigUtilGrp, self.utilGrp)
		# pm.parent(self.rigStillGrp, self.stillGrp)
		self.parentCons =  misc.snapTransform('parent', self.parent, self.rigCtrlGrp, True, False)

		# --- create setting controller
		self.settingCtrl = controller.Controller(name='%sSetting%s_ctrl' %_name, 
					st='stick', 
					axis='-z',
					color='green', 
					scale=(self.size))
		self.settingCtrl.lockAttr(t=True, r=True, s=True, v=True)
		self.settingCtrl.hideAttr(t=True, r=True, s=True, v=True)
		sizeAttr = misc.addNumAttr(self.settingCtrl, 'size', 'double', hide=False, dv=1.0)
		fkikAttr = misc.addNumAttr(self.settingCtrl, 'fkIk', 'double', hide=False, min=0, max=1, dv=1.0)
		
		# add attribute on setting controller
		settingCtrlShp = self.settingCtrl.getShape()
		# misc.addNumAttr(settingCtrlShp, 'stretchAmp', 'double', hide=False, min=0, max=1, dv=0.1)

		# group setting controller
		settingCtrlZgrp = misc.zgrp(self.settingCtrl, element='Zro', suffix='grp')[0]
		misc.snapTransform('parent', self.tmpJnts[2], settingCtrlZgrp, False, True)

		# create reverse node on fkIk switch attribute and connect
		ikfkRev = pm.createNode('reverse', n='%sIkFk%s_rev' %_name)
		pm.connectAttr(fkikAttr, ikfkRev.inputX)
		pm.connectAttr(fkikAttr, self.rigIkCtrlGrp.visibility)
		pm.connectAttr(ikfkRev.outputX, self.rigFkCtrlGrp.visibility)

		# --- create main, fk and ik jnts
		jntElems = ['', 'Fk', 'Ik']
		pos = ['Upr', 'Knee', 'Ankle', 'Ball', 'Toe']
		jlst = ['self.jnts', 'self.fkJnts', 'self.ikJnts']
		glst = [ 'self.rigUtilGrp', 'self.rigFkCtrlGrp', 'self.rigIkCtrlGrp']
		rad = self.tmpJnts[0].radius.get()
		m = 1.0  # radius multiplier
		for e in xrange(3):
			# duplicate joint from tmpJnt
			upJnt = pm.duplicate(self.tmpJnts[0], po=True, n='%s%s%s%s_jnt' %(self.elem, pos[0], jntElems[e], self.side))[0]
			kneeJnt = pm.duplicate(self.tmpJnts[1], po=True, n='%s%s%s%s_jnt' %(self.elem, pos[1], jntElems[e], self.side))[0]
			ankleJnt = pm.duplicate(self.tmpJnts[2], po=True, n='%s%s%s%s_jnt' %(self.elem, pos[2], jntElems[e], self.side))[0]
			ballJnt = pm.duplicate(self.tmpJnts[3], po=True, n='%s%s%s%s_jnt' %(self.elem, pos[3], jntElems[e], self.side))[0]
			toeJnt = pm.duplicate(self.tmpJnts[4], po=True, n='%s%s%s%s_jnt' %(self.elem, pos[4], jntElems[e], self.side))[0]

			# parent into fk chain
			pm.parent(toeJnt, ballJnt)
			pm.parent(ballJnt, ankleJnt)
			pm.parent(ankleJnt, kneeJnt)
			pm.parent(kneeJnt, upJnt)
			
			# set radius
			for j in [upJnt, kneeJnt, ankleJnt, ballJnt, toeJnt]:
				j.radius.set(rad*m)

			# freeze it
			pm.makeIdentity(upJnt, apply=True)

			# store to list
			exec('%s = [upJnt, kneeJnt, ankleJnt, ballJnt, toeJnt]' %jlst[e])
			# exec('pm.parent(upJnt, %s)' %glst[e])
			parentTo = self.parent
			if e > 0:
				parentTo = self.rigUtilGrp

				# hide ik/fk jnts
				upJnt.visibility.set(False)

			# connect scale
			pm.connectAttr(self.settingCtrl.size, ankleJnt.scaleX)
			pm.connectAttr(self.settingCtrl.size, ankleJnt.scaleY)
			pm.connectAttr(self.settingCtrl.size, ankleJnt.scaleZ)

			pm.parent(upJnt, parentTo)
			

			m *= 0.5

		# add zro joint to ik and fk chain
		# zroFkJnts = misc.addOffsetJnt(sels=self.fkJnts, element='Zro')
		# zroIkJnts = misc.addOffsetJnt(sels=self.ikJnts, element='Zro')

		# --- parent and scale constraint main joint to both ik and fk joint
		for j in xrange(5):
			parConsNode = pm.parentConstraint([self.fkJnts[j], self.ikJnts[j]], self.jnts[j])
			# connect parent cons
			pm.connectAttr(ikfkRev.outputX, parConsNode.attr('%sW0' %self.fkJnts[j].nodeName()))
			pm.connectAttr(self.settingCtrl.fkIk, parConsNode.attr('%sW1' %self.ikJnts[j].nodeName()))

			# scConsNode = pm.scaleConstraint([self.fkJnts[j], self.ikJnts[j]], self.jnts[j])
			# connect scale cons
			# pm.connectAttr(ikfkRev.outputX, scConsNode.attr('%sW0' %self.fkJnts[j].nodeName()))
			# pm.connectAttr(self.settingCtrl.fkIk, scConsNode.attr('%sW1' %self.ikJnts[j].nodeName()))

		# throw setting zgrp to ctrl group
		pm.parent(settingCtrlZgrp, self.rigCtrlGrp)
		misc.snapTransform('parent', self.jnts[2], settingCtrlZgrp, False, False)

		# ------ FK rig
		for i in xrange(4):
			# FK controller shape
			if i == 3:
				fkCtrlShp = 'halfCrossCircle'
			else:
				fkCtrlShp = 'crossCircle'

			fkCtrl = controller.Controller(name='%s%sFk%s_ctrl' %(self.elem, pos[i], self.side),
										st=fkCtrlShp, scale=self.size)
			fkCtrl.rotateOrder.set(self.fkRotateOrder)
			fkCtrl.setColor('red')
			fkCtrl.lockAttr(t=False, s=True, v=True)
			fkCtrl.hideAttr(t=False, s=True, v=True)

			fkGCtrl = fkCtrl.addGimbal()

			fkCtrlZgrp = misc.zgrp(fkCtrl, element='Zro', suffix='grp')[0]

			self.fkGCtrls.append(fkGCtrl)
			self.fkCtrls.append(fkCtrl)
			self.fkCtrlZgrps.append(fkCtrlZgrp)

			parent = self.rigFkCtrlGrp
			if i > 0:
				parent = self.fkGCtrls[i-1]

			pm.parent(fkCtrlZgrp, parent)

			misc.snapTransform('parent', self.fkJnts[i], fkCtrlZgrp, False, True)
			misc.snapTransform('parent', fkGCtrl, self.fkJnts[i], False, False)

		# foot scale
		fkFootScaleGrp = pm.group(em=True, n='%sFootFkScale%s_grp' %_name)
		misc.snapTransform('parent', self.fkJnts[2], fkFootScaleGrp, False, True)
		pm.parent(fkFootScaleGrp, self.fkGCtrls[2])
		pm.parent(self.fkCtrlZgrps[3], fkFootScaleGrp)
		pm.connectAttr(self.settingCtrl.size, fkFootScaleGrp.scaleX)
		pm.connectAttr(self.settingCtrl.size, fkFootScaleGrp.scaleY)
		pm.connectAttr(self.settingCtrl.size, fkFootScaleGrp.scaleZ)

		# --- connect Fk stretch
		pos = ['Upr', 'Lwr']
		defaultLenAttrs, defaultDistAttrs = [], []
		for i in xrange(2):
			# add default len attr
			defaultLenValue = self.fkJnts[i+1].attr(aimTran).get()
			defLenAttr = misc.addNumAttr(settingCtrlShp, 'defaultLen%s' %(pos[i]), 'double', key=False, hide=True, dv=defaultLenValue)

			defaultDistValue = misc.getDistanceFromPosition(self.fkJnts[i].getTranslation('world'), self.fkJnts[i+1].getTranslation('world'))
			defDistAttr = misc.addNumAttr(settingCtrlShp, 'defaultDist%s' %(pos[i]), 'double', key=False, hide=True, dv=defaultDistValue)

			# stretchAmpMdl = pm.createNode('multDoubleLinear', n='%s%sFkStretchAmp%s_mdl' %(self.elem, pos[i], self.side))
			stretchMdl = pm.createNode('multDoubleLinear', n='%s%sFkStretch%s_mdl' %(self.elem, pos[i], self.side))
			stretchAdl = pm.createNode('addDoubleLinear', n='%s%sFkStretch%s_adl' %(self.elem, pos[i], self.side))
			misc.addNumAttr(self.fkCtrls[i], 'stretch', 'double')

			# pm.connectAttr(self.fkCtrls[i].stretch, stretchAmpMdl.input1)
			# pm.connectAttr(settingCtrlShp.stretchAmp, stretchAmpMdl.input2)
			pm.connectAttr(self.fkCtrls[i].stretch, stretchMdl.input1)
			pm.connectAttr(defLenAttr, stretchMdl.input2)
			pm.connectAttr(defLenAttr, stretchAdl.input1)
			pm.connectAttr(stretchMdl.output, stretchAdl.input2)
			pm.connectAttr(stretchAdl.output, self.fkCtrlZgrps[i+1].attr(aimTran))

			defLenAttr.setKeyable(False)
			defLenAttr.showInChannelBox(False)
			defLenAttr.setLocked(True)
			defaultLenAttrs.append(defLenAttr)

			defDistAttr.setKeyable(False)
			defDistAttr.showInChannelBox(False)
			defDistAttr.setLocked(True)
			defaultDistAttrs.append(defDistAttr)

		# --- local world for upLeg
		# misc.addNumAttr(self.fkCtrls[i], 'localWorld', 'double', dv=1.0)
		misc.createLocalWorld(objs=[self.fkCtrls[0], self.rigFkCtrlGrp, self.animGrp, self.fkCtrlZgrps[0]], 
			constraintType='orient', attrName='localWorld')

		# --- create default len attrs on setting ctrl shape
		defLimbLenAttr = misc.addNumAttr(settingCtrlShp, 'defaultLen', 'double', key=False, hide=True, dv=defaultLenValue)
		defLenAdl = pm.createNode('addDoubleLinear', n='%sDefaultLen%s_adl' %_name)
		pm.connectAttr(defaultLenAttrs[0], defLenAdl.input1)
		pm.connectAttr(defaultLenAttrs[1], defLenAdl.input2)
		pm.connectAttr(defLenAdl.output, defLimbLenAttr)

		defLimbLenAttr.setKeyable(False)
		defLimbLenAttr.showInChannelBox(False)
		defLimbLenAttr.setLocked(True)

		# --- create default dist attrs on setting ctrl shape
		defLimbDistAttr = misc.addNumAttr(settingCtrlShp, 'defaultDist', 'double', key=False, hide=True, dv=defaultLenValue)
		defDestAdl = pm.createNode('addDoubleLinear', n='%sDefaultDist%s_adl' %_name)
		pm.connectAttr(defaultDistAttrs[0], defDestAdl.input1)
		pm.connectAttr(defaultDistAttrs[1], defDestAdl.input2)
		pm.connectAttr(defDestAdl.output, defLimbDistAttr)

		defLimbDistAttr.setKeyable(False)
		defLimbDistAttr.showInChannelBox(False)
		defLimbDistAttr.setLocked(True)

		# ------ IK rig
		# --- legRoot ik ctrl
		baseIkCtrl = controller.Controller(name='%sIkBase%s_ctrl' %(_name),
										st='locator', scale=self.size)
		baseIkCtrl.setColor('blue')
		baseIkCtrl.lockAttr(r=True, s=True, v=True)
		baseIkCtrl.hideAttr(r=True, s=True, v=True)
		baseIkCtrlZgrp = misc.zgrp(baseIkCtrl, element='Zro', suffix='grp')[0]	

		# --- leg pv ctrl
		pvIkCtrl = controller.Controller(name='%sIkPv%s_ctrl' %(_name),
										st='3dDiamond', scale=self.size*0.334)
		pvIkCtrl.setColor('blue')
		pvIkCtrl.lockAttr(r=True, s=True, v=True)
		pvIkCtrl.hideAttr(r=True, s=True, v=True)
		pvIkCtrlZgrp = misc.zgrp(pvIkCtrl, element='Zro', suffix='grp')[0]
		pvIkCtrlLocWorAttr = misc.addNumAttr(pvIkCtrl, 'localWorld', 'double', min=0, max=1, dv=0)
		pinAttr = misc.addNumAttr(pvIkCtrl, 'pin', 'double', min=0, max=1, dv=0)

		# --- legIK ctrl
		mainIkCtrl = controller.Controller(name='%sIk%s_ctrl' %(_name),
										st='diamond', scale=self.size)
		mainIkCtrl.rotateOrder.set(self.ikRotateOrder)
		mainIkCtrl.setColor('blue')
		mainIkGCtrl = mainIkCtrl.addGimbal()
		mainIkCtrl.lockAttr(r=False, s=True, v=True)
		mainIkCtrl.hideAttr(r=False, s=True, v=True)
		mainIkCtrlZgrp = misc.zgrp(mainIkCtrl, element='Zro', suffix='grp')[0]

		# add attributes
		ikCtrlLocWorAttr = misc.addNumAttr(mainIkCtrl, 'localWorld', 'double', min=0, max=1, dv=1)
		ikCtrlSepAttr = misc.addNumAttr(mainIkCtrl, '__ik__', 'double', hide=False, min=0, max=1, dv=0)
		ikCtrlSepAttr.lock()
		twistAttr = misc.addNumAttr(mainIkCtrl, 'twist', 'double')
		autoStretchAttr = misc.addNumAttr(mainIkCtrl, 'autoStretch', 'double', min=0, max=1, dv=0)
		upperStchAttr = misc.addNumAttr(mainIkCtrl, 'upperStretch', 'double')
		lowerStchAttr = misc.addNumAttr(mainIkCtrl, 'lowerStretch', 'double')
		# softIkAttr = misc.addNumAttr(mainIkCtrl, 'softIk', 'double', min=0, max=1, dv=0)

		# --- store ik controllers to list
		self.ikCtrls = [baseIkCtrl, pvIkCtrl, mainIkCtrl]
		self.ikCtrlZgrps = [baseIkCtrlZgrp, pvIkCtrlZgrp, mainIkCtrlZgrp]

		# --- position ik ctrls
		upLegLen = misc.getDistanceFromPosition(self.ikJnts[0].getTranslation('world'), self.ikJnts[1].getTranslation('world'))
		lwrLegLen = misc.getDistanceFromPosition(self.ikJnts[1].getTranslation('world'), self.ikJnts[2].getTranslation('world'))
		legLen = upLegLen + lwrLegLen

		misc.snapTransform('parent', self.ikJnts[0], self.ikCtrlZgrps[0], False, True)
		pvPosRet = misc.getPoleVectorPosition(jnts=self.ikJnts[0:3], createLoc=False, ro=False, offset=(1.25*legLen))
		pm.xform(self.ikCtrlZgrps[1], ws=True, t=pvPosRet['translation'])
		misc.snapTransform('parent', self.ikJnts[2], self.ikCtrlZgrps[2], False, True)

		# throw ik ctrl zgrp to ik ctrl grp
		pm.parent(self.ikCtrlZgrps, self.rigIkCtrlGrp)

		# point ik base to ik base ctrl
		misc.snapTransform('point', self.ikCtrls[0], self.ikJnts[0], True, False)

		# ---- create Ik handle
		ikHndl, ikEff = pm.ikHandle(sj=self.ikJnts[0], ee=self.ikJnts[2], sol='ikRPsolver', n='%sAnkle%s_ikHndl' %_name)
		midIkHndl, midIkEff = pm.ikHandle(sj=self.ikJnts[2], ee=self.ikJnts[3], sol='ikRPsolver', n='%sBall%s_ikHndl' %_name)
		tipIkHndl, tipIkEff = pm.ikHandle(sj=self.ikJnts[3], ee=self.ikJnts[4], sol='ikRPsolver', n='%sToe%s_ikHndl' %_name)

		ikHndlZgrp = misc.zgrp(ikHndl, element='Zro', suffix='grp')[0]
		midIkHndlZgrp = misc.zgrp(midIkHndl, element='Zro', suffix='grp')[0]
		tipIkHndlZgrp = misc.zgrp(tipIkHndl, element='Zro', suffix='grp')[0]

		self.ikHndls = [ikHndl, midIkHndl, tipIkHndl]
		self.ikEffs = [ikEff, midIkEff, tipIkEff]
		self.ikHndlZgrps = [ikHndlZgrp, midIkHndlZgrp, tipIkHndlZgrp]
		pm.parent(self.ikHndlZgrps, self.rigIkhGrp)

		# --- ik piv groups
		self.ikPosGrp = pm.group(em=True, n='%sAnkleIkHndlPiv%s_grp' %_name)
		self.midIkPivGrp = pm.group(em=True, n='%sBallIkHndlPiv%s_grp' %_name)
		self.tipIkPivGrp = pm.group(em=True, n='%sToeIkHndlPiv%s_grp' %_name)
		
		misc.snapTransform('point', self.ikHndls[0], self.ikPosGrp, False, True)
		misc.snapTransform('point', self.ikHndls[1], self.midIkPivGrp, False, True)
		misc.snapTransform('point', self.ikHndls[2], self.tipIkPivGrp, False, True)

		ikFootScaleGrp = pm.group(em=True, n='%sFootIkScale%s_grp' %_name)
		misc.snapTransform('parent', self.ikJnts[2], ikFootScaleGrp, False, True)
		pm.connectAttr(self.settingCtrl.size, ikFootScaleGrp.scaleX)
		pm.connectAttr(self.settingCtrl.size, ikFootScaleGrp.scaleY)
		pm.connectAttr(self.settingCtrl.size, ikFootScaleGrp.scaleZ)

		pm.parent([self.ikPosGrp, self.midIkPivGrp, self.tipIkPivGrp], ikFootScaleGrp)
		pm.parent(ikFootScaleGrp, mainIkGCtrl)

		misc.snapTransform('parent', self.ikPosGrp, self.ikHndlZgrps[0], False, False)
		misc.snapTransform('parent', self.midIkPivGrp, self.ikHndlZgrps[1], False, False)
		misc.snapTransform('parent', self.tipIkPivGrp, self.ikHndlZgrps[2], False, False)

		# toLockHide = {'t':True, 'r':True, 's':True, 'v':True}
		# misc.lockAttr(self.ikPosGrp, **toLockHide)
		# misc.hideAttr(self.ikPosGrp, **toLockHide)
		# misc.lockAttr(self.tipIkPivGrp, **toLockHide)
		# misc.hideAttr(self.tipIkPivGrp, **toLockHide)

		# --- polevector constriant
		pm.poleVectorConstraint(self.ikCtrls[1], self.ikHndls[0])

		# --- add pv annotation line
		annDict = misc.annPointer(pointFrom=self.ikJnts[1], pointTo=self.ikCtrls[1], 
				ref=True, constraint=True)
		annGrp = pm.group(em=True, n='%sAnnPointer%s_grp' %_name)
		pm.parent([annDict['ann'], annDict['loc']], annGrp)
		pm.parent(annGrp, self.rigIkCtrlGrp)

		# --- ik ctrl and pv ik ctrl local world
		misc.createLocalWorld(objs=[self.ikCtrls[2], self.ikCtrls[0], self.animGrp, self.ikCtrlZgrps[2]], constraintType='parent', attrName='localWorld')
		misc.createLocalWorld(objs=[self.ikCtrls[1], self.ikCtrls[2], self.animGrp, self.ikCtrlZgrps[1]], constraintType='parent', attrName='localWorld')

		# --- connect twist
		if defaultLenAttrs[0].get() > 0.0:
			twistMultValue = 1.0
		else:
			twistMultValue = -1.0
		ikTwstMdl = pm.createNode('multDoubleLinear', n='%sIkTwist%s_mdl' %_name)
		ikTwstMdl.input2.set(twistMultValue)
		pm.connectAttr(self.ikCtrls[2].twist, ikTwstMdl.input1)
		pm.connectAttr(ikTwstMdl.output, self.ikHndls[0].twist)

		# --- create position groups
		baseIkPosGrp = pm.group(em=True, n='%sIkBasePos%s_grp' %_name)
		misc.snapTransform('point', self.ikCtrls[0], baseIkPosGrp, False, False)

		pvIkPosGrp = pm.group(em=True, n='%sIkPvPos%s_grp' %_name)
		misc.snapTransform('point', self.ikCtrls[1], pvIkPosGrp, False, False)

		ikPosGrp = pm.group(em=True, n='%sIkPos%s_grp' %_name)
		misc.snapTransform('point', self.ikHndlZgrps[0], ikPosGrp, False, False)
		pm.parent([baseIkPosGrp, pvIkPosGrp, ikPosGrp], self.rigIkCtrlGrp)

		# --- connect  auto stertch
		ikDist = pm.createNode('distanceBetween', n='%sIkAutoStretchIk%s_dist' %_name)
		pm.connectAttr(baseIkPosGrp.translate, ikDist.point1)
		pm.connectAttr(ikPosGrp.translate, ikDist.point2)

		autoStchDivOrigMdv = pm.createNode('multiplyDivide', n='%sIkAutoStretchDivOrig%s_mdv' %_name)
		autoStchDivOrigMdv.operation.set(2)
		pm.connectAttr(ikDist.distance, autoStchDivOrigMdv.input1X)
		pm.connectAttr(settingCtrlShp.defaultDist, autoStchDivOrigMdv.input2X)

		autoStchCond = pm.createNode('condition', n='%sIkAutoStretch%s_cond' %_name)
		autoStchCond.operation.set(2)
		autoStchCond.colorIfFalseR.set(1.0)
		pm.connectAttr(ikDist.distance, autoStchCond.firstTerm)
		pm.connectAttr(settingCtrlShp.defaultDist, autoStchCond.secondTerm)
		pm.connectAttr(autoStchDivOrigMdv.outputX, autoStchCond.colorIfTrueR)

		autoStchBta = pm.createNode('blendTwoAttr', n='%sIkAutoStretch%s_bta' %_name)
		autoStchBta.input[0].set(1.0)
		pm.connectAttr(self.ikCtrls[2].autoStretch, autoStchBta.attributesBlender)
		pm.connectAttr(autoStchCond.outColorR, autoStchBta.input[1])

		autoStchMultLenMdv = pm.createNode('multiplyDivide', n='%sIkAutoStretchMultLen%s_mdv' %_name)
		pm.connectAttr(autoStchBta.output, autoStchMultLenMdv.input1X)
		pm.connectAttr(autoStchBta.output, autoStchMultLenMdv.input1Y)
		pm.connectAttr(settingCtrlShp.defaultLenUpr, autoStchMultLenMdv.input2X)
		pm.connectAttr(settingCtrlShp.defaultLenLwr, autoStchMultLenMdv.input2Y)

		# --- connect manual stretch
		stchMdv = pm.createNode('multiplyDivide', n='%sIkStretch%s_mdv' %_name)
		pm.connectAttr(self.ikCtrls[2].upperStretch, stchMdv.input1X)
		pm.connectAttr(self.ikCtrls[2].lowerStretch, stchMdv.input1Y)
		pm.connectAttr(settingCtrlShp.defaultLenUpr, stchMdv.input2X)
		pm.connectAttr(settingCtrlShp.defaultLenLwr, stchMdv.input2Y)

		# --- combine auto stretch and manual stretch together
		ikStchPma = pm.createNode('plusMinusAverage', n='%sIkStretch%s_pma' %_name)
		pm.connectAttr(autoStchMultLenMdv.outputX, ikStchPma.input2D[0].input2Dx)
		pm.connectAttr(autoStchMultLenMdv.outputY, ikStchPma.input2D[0].input2Dy)
		pm.connectAttr(stchMdv.outputX, ikStchPma.input2D[1].input2Dx)
		pm.connectAttr(stchMdv.outputY, ikStchPma.input2D[1].input2Dy)

		# --- connect ik lock
		ikUprLockDist = pm.createNode('distanceBetween', n='%sIkUprLock%s_dist' %_name)
		pm.connectAttr(baseIkPosGrp.translate, ikUprLockDist.point1)
		pm.connectAttr(pvIkPosGrp.translate, ikUprLockDist.point2)

		ikLwrLockDist = pm.createNode('distanceBetween', n='%sIkLwrLock%s_dist' %_name)
		pm.connectAttr(pvIkPosGrp.translate, ikLwrLockDist.point1)
		pm.connectAttr(ikPosGrp.translate, ikLwrLockDist.point2)

		ikLockDivLenMdv = pm.createNode('multiplyDivide', n='%sIkLockDivLen%s_mdv' %_name)
		ikLockDivLenMdv.operation.set(2)
		pm.connectAttr(ikUprLockDist.distance, ikLockDivLenMdv.input1X)
		pm.connectAttr(ikLwrLockDist.distance, ikLockDivLenMdv.input1Y)
		pm.connectAttr(settingCtrlShp.defaultLenUpr, ikLockDivLenMdv.input2X)
		pm.connectAttr(settingCtrlShp.defaultLenLwr, ikLockDivLenMdv.input2Y)

		ikLockMultLenMdv = pm.createNode('multiplyDivide', n='%sIkLockMultLen%s_mdv' %_name)
		pm.connectAttr(ikLockDivLenMdv.outputX, ikLockMultLenMdv.input1X)
		pm.connectAttr(ikLockDivLenMdv.outputY, ikLockMultLenMdv.input1Y)
		pm.connectAttr(settingCtrlShp.defaultLenUpr, ikLockMultLenMdv.input2X)
		pm.connectAttr(settingCtrlShp.defaultLenLwr, ikLockMultLenMdv.input2Y)

		# connect blend colors for lock
		ikLockBcol = pm.createNode('blendColors', n='%sIkLock%s_bco' %_name)
		pm.connectAttr(self.ikCtrls[1].pin, ikLockBcol.blender)
		pm.connectAttr(ikLockMultLenMdv.outputX, ikLockBcol.color1R)
		pm.connectAttr(ikLockMultLenMdv.outputY, ikLockBcol.color1G)
		pm.connectAttr(ikStchPma.output2D.output2Dx, ikLockBcol.color2R)
		pm.connectAttr(ikStchPma.output2D.output2Dy, ikLockBcol.color2G)

		# --- connect to ik joint
		pm.connectAttr(ikLockBcol.outputR, self.ikJnts[1].attr(aimTran))
		pm.connectAttr(ikLockBcol.outputG, self.ikJnts[2].attr(aimTran))

		if self.createFootRig == True:
			ikCtrlSepAttr = misc.addNumAttr(mainIkCtrl, '__foot__', 'double', hide=False, min=0, max=1, dv=0)
			ikCtrlSepAttr.lock()

			misc.addNumAttr(mainIkCtrl, 'toeRoll', 'double')
			misc.addNumAttr(mainIkCtrl, 'ballRoll', 'double')
			misc.addNumAttr(mainIkCtrl, 'heelRoll', 'double')

			misc.addNumAttr(mainIkCtrl, 'toeTwist', 'double')
			misc.addNumAttr(mainIkCtrl, 'ballTwist', 'double')
			misc.addNumAttr(mainIkCtrl, 'heelTwist', 'double')

			misc.addNumAttr(mainIkCtrl, 'toeBend', 'double')
			misc.addNumAttr(mainIkCtrl, 'footRock', 'double')

			ballTwstIkPivGrp = pm.group(em=True, n='%sBallTwstIkPiv%s_grp' %_name)
			ballTwstIkPivZroGrp = misc.zgrp(ballTwstIkPivGrp, element='Zro', suffix='grp')[0]
			misc.snapTransform('parent', self.tmpJnts[3], ballTwstIkPivZroGrp, False, True)
			pm.parent(ballTwstIkPivZroGrp, ikFootScaleGrp)

			toeIkPivGrp = pm.group(em=True, n='%sToeIkPiv%s_grp' %_name)
			toeIkPivZroGrp = misc.zgrp(toeIkPivGrp, element='Zro', suffix='grp')[0]
			misc.snapTransform('parent', self.tmpJnts[4], toeIkPivZroGrp, False, True)
			pm.parent(toeIkPivZroGrp, ballTwstIkPivGrp)

			heelIkPivGrp = pm.group(em=True, n='%sHeelIkPiv%s_grp' %_name)
			heelIkPivZroGrp = misc.zgrp(heelIkPivGrp, element='Zro', suffix='grp')[0]
			misc.snapTransform('parent', self.tmpJnts[5], heelIkPivZroGrp, False, True)
			pm.parent(heelIkPivZroGrp, toeIkPivGrp)

			footInIkPivGrp = pm.group(em=True, n='%sFootInIkPiv%s_grp' %_name)
			footInIkPivZroGrp = misc.zgrp(footInIkPivGrp, element='Zro', suffix='grp')[0]
			misc.snapTransform('parent', self.tmpJnts[6], footInIkPivZroGrp, False, True)
			pm.parent(footInIkPivZroGrp, heelIkPivGrp)
			# set min limit
			rotLimitMinName = 'rotateMin%s' %self.aimAxis.upper()
			footInIkPivGrp.setLimited(rotLimitMinName, True)
			footInIkPivGrp.setLimit(rotLimitMinName, 0.0)

			footOutIkPivGrp = pm.group(em=True, n='%sFootOutIkPiv%s_grp' %_name)
			footOutIkPivZroGrp = misc.zgrp(footOutIkPivGrp, element='Zro', suffix='grp')[0]
			misc.snapTransform('parent', self.tmpJnts[7], footOutIkPivZroGrp, False, True)
			pm.parent(footOutIkPivZroGrp, footInIkPivGrp)
			# set max limit
			rotLimitMaxName = 'rotateMax%s' %self.aimAxis.upper()
			footOutIkPivGrp.setLimited(rotLimitMaxName, True)
			footOutIkPivGrp.setLimit(rotLimitMaxName, 0.0)

			ballRollIkPivGrp = pm.group(em=True, n='%sBallRollIkPiv%s_grp' %_name)
			ballRollIkPivZroGrp = misc.zgrp(ballRollIkPivGrp, element='Zro', suffix='grp')[0]
			misc.snapTransform('parent', self.tmpJnts[3], ballRollIkPivZroGrp, False, True)
			pm.parent(ballRollIkPivZroGrp, footOutIkPivGrp)

			pm.parent([self.ikPosGrp, self.midIkPivGrp], ballRollIkPivGrp)

			toeBendIkPivGrp = pm.group(em=True, n='%sToeBendIkPiv%s_grp' %_name)
			toeBendIkPivZroGrp = misc.zgrp(toeBendIkPivGrp, element='Zro', suffix='grp')[0]
			misc.snapTransform('parent', self.tmpJnts[3], toeBendIkPivZroGrp, False, True)
			pm.parent(toeBendIkPivZroGrp, footOutIkPivGrp)

			pm.parent(self.tipIkPivGrp, toeBendIkPivGrp)

			# connect attrs
			aimRot = 'r%s' %self.aimAxis
			upRot =  'r%s' %self.upAxis
			bendRot = 'r%s' %self.bendAxis

			pm.connectAttr(mainIkCtrl.toeRoll, toeIkPivGrp.attr(bendRot))
			pm.connectAttr(mainIkCtrl.ballRoll, ballRollIkPivGrp.attr(bendRot))
			pm.connectAttr(mainIkCtrl.heelRoll, heelIkPivGrp.attr(bendRot))

			pm.connectAttr(mainIkCtrl.toeTwist, toeIkPivGrp.attr(upRot))
			pm.connectAttr(mainIkCtrl.ballTwist, ballTwstIkPivGrp.attr(upRot))
			pm.connectAttr(mainIkCtrl.heelTwist, heelIkPivGrp.attr(upRot))

			pm.connectAttr(mainIkCtrl.footRock, footInIkPivGrp.attr(aimRot))
			pm.connectAttr(mainIkCtrl.footRock, footOutIkPivGrp.attr(aimRot))

			pm.connectAttr(mainIkCtrl.toeBend, toeBendIkPivGrp.attr(bendRot))

			
