import sys
import maya.standalone as std

std.initialize(name='python')
func = sys.argv[1]
input = sys.argv[2]
logPath = sys.argv[3]


from nuTools.batch import batchOperator as batOp
reload(batOp)



def doBatch(func, input):
	# instance the class
	batchOperator = batOp.BatchOperator(func=func, inputs=input)

	#execute the function. grap status and output
	status, output = batchOperator.executeFunc(input)

	# niceResult = batchOperator.logResult(input, status, output)

	stdResult = '_STD_BATCH_RESULT_%s_STD_BATCH_RESULT_%s' %(status, output)
	sys.stdout.write(stdResult)
	# sys.stdout.write('_BATCH_OUTPUT_%s' %output)
	# sys.stdout.write('_OUTPUT_TO_SCRIPT_EDITOR_%s' %niceResult)

	return stdResult


doBatch(func, input)


	