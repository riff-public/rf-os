import pymel.core as pm
import shutil, os
from nuTools import misc

reload(misc)

def fix_cty2D_texturePathAndPublish(inputPath):
	"""
	Fix texture path in rig work file
	save it
	publish anim and render

	args:
		inputPath = path to asset version folder
					ie. "P:/nine/all/asset/setDress/ch4TownHouse/townHouseM/rig/version"
	"""

	## open the most recent rig work
	try:
		assetDir = os.path.normpath(inputPath)
		# assetName = assetDir.split('\\')[-1]
		rigWorkDir = '%s\\rig\\maya\\work' %assetDir
		latestWork =  misc.getLatestVersion(rigWorkDir, search='_rig_', mayaFileOnly=True, getFile=True)
		fRigWorkPath = os.path.join(rigWorkDir, latestWork)
		pm.openFile(fRigWorkPath, f=True)
	except:
		return 'Failed', 'Open latest rig work file'

	## search and replace file nodes
	res = ''
	try:
		for f in pm.ls(type='file'):
			ex = 'New path exists: '
			suc = 'Success: '
			old_path = f.fileTextureName.get()
			new_path = old_path.replace('cty_', 'cty2D_')
			if os.path.exists(os.path.normpath(new_path)):
				ex += 'True'
				try:
					f.fileTextureName.set(new_path)
					suc += 'True'
				except:
					suc += 'False'
					print 'Cannot set path for %s' %f.nodeName()
			else:
				ex += 'False'
				print 'New path does not exists: %s' %new_path

			res += '%s\n%s\n%s\n%s' %(ex, suc, old_path, new_path)
			res += '\n\n'
	except:
		return 'Failed', 'Replacing file texture name'



	## publish anim rig and render rig
	try:
		from publish.rig import publish_rig_app as rigPubApp
		reload(rigPubApp)

		rigPub = rigPubApp.MyForm(rigPubApp.getMayaWindow())
		rigPub.ui.anim_checkBox.setChecked(True)
		rigPub.ui.render_checkBox.setChecked(True)
		rigPub.ui.animRigStatus_comboBox.setCurrentIndex(1)
		rigPub.ui.renderRigStatus_comboBox.setCurrentIndex(1)
		rigPub.doPublish()
	except:
		return 'Failed', 'Publish to anim and render rig'

	# SUCCESS
	return 'Success', res
	 

