import pymel.core as pm 
import maya.OpenMaya as om
import subprocess, datetime, inspect, sys, os


from nuTools import misc
from nuTools.batch import batchFuncs as batch

reload(misc)
reload(batch)

BATCH_FUNC_PATH = 'O:/studioTools/maya/python/tool/rig/nuTools/batch/batchCall.py'
MAYAPY_PATH = 'C:/program files/autodesk/maya2015/bin/mayapy.exe'

class BatchOperator(object):

	def __init__(self, func='', inputs=[], 
				logPath = 'C:/temp', 
				mayaPath = MAYAPY_PATH,
				batchPath = BATCH_FUNC_PATH, 
				batchMode = 1, stdMode=False):

		self._VERSION = '1.2'
		self.WINDOW_NAME = 'batchOperatorWin'
		self.WINDOW_TITLE = 'Batch Operator %s' %self._VERSION

		self.statusDict = {'Failed':False, 'Success':True}

		self.funcName = func
		self.func = None
		self.funcPath = ''
		self.inputs = inputs
		
		self.batchMode = batchMode
		self.stdMode = stdMode

		self.mayaPath = mayaPath
		self.batchPath = batchPath
		self.logPath = logPath
		

		self.logFileName = 'batch'
		self.logExt = 'log'
		self.logFile = None

		self.ui = False
		self.user = 'user'

		# if input is not a list - str, make it a list
		if isinstance(self.inputs, (tuple, list)) == False:
			self.inputs = [str(self.inputs)]

		# get user
		try:
			self.user = misc.getUserFromEnvVar()		
		except:
			pass 
		if not self.user:
			self.user = 'user'

		# try to load function
		if self.funcName:
			try:
				self.loadFunction(self.funcName)
			except:
				om.MGlobal.displayWarning('Cannot load batch function specified.')



	def UI(self):
		if pm.window(self.WINDOW_NAME, ex=True):
			pm.deleteUI(self.WINDOW_NAME, window=True)
		with pm.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, s=True, mnb=True, mxb=True) as self.mainWindow:
			with pm.columnLayout(adj=True, rs=5, co=['both', 0]):
				with pm.columnLayout(adj=True, rs=3, co=['both', 5]):
					with pm.rowColumnLayout(nc=3, rs=(1, 5), co=[(1, 'left', 5), (2, 'left', 5), (3, 'left', 5)]):
						pm.text(l='Operation')
						self.funcMenu = pm.optionMenu(w=185, cc=pm.Callback(self.selectFunc))
						pm.button(l='refresh', w=53, c=pm.Callback(self.getAllFunctions))

					with pm.rowColumnLayout(nc=4, rs=(1, 5), co=[(1, 'left', 10), (2, 'left', 5), (3, 'left', 5), (4, 'left', 5)]):
						pm.text(l='Log Path')
						self.logPathTxtFld = pm.textField(tx=self.logPath, w=375, cc=pm.Callback(self.setLogPath))
						pm.button(l='...', c=pm.Callback(self.browseLogPath))
						pm.button(l='log', c=pm.Callback(self.seeLog))

					with pm.frameLayout(l='Inputs', bs='etchedIn', mh=10, cll=True, cl=False):
						with pm.columnLayout(adj=True, rs=5, co=['both', 10]):
							self.inputScrollField = pm.scrollField(ed=True, h=300, wordWrap=False,
													ec=pm.Callback(self.fillInput), cc=pm.Callback(self.fillInput))
							self.inputTSL = pm.textScrollList(vis=False, h=300, fn='fixedWidthFont')


					with pm.frameLayout(lv=False, bs='in', mh=10, mw=10):
						with pm.columnLayout(adj=True, rs=5, co=['both', 20]):
							self.currentProcessTxt = pm.text(l='No operation')
							self.currentInputTxt = pm.text(l='', h=30, bgc=[0.0, 0.0, 0.0])

					with pm.columnLayout(adj=True, rs=5, cat=['both', 150]):
						with pm.rowColumnLayout(nc=2, co=[(1, 'left', 0), (2, 'left', 5)]):
							pm.text(l='confirm')
							with pm.optionMenu(w=80, cc=pm.Callback(self.selectMode)) as self.confirmMenu:
								pm.menuItem(l='Everytime', parent=self.confirmMenu)
								pm.menuItem(l='If failed', parent=self.confirmMenu)
								pm.menuItem(l='No', parent=self.confirmMenu)
						with pm.rowColumnLayout(nc=1, co=[(1, 'left', 0)]):
							self.stdChkBox = pm.checkBox(l='standalone mode', v=False, cc=pm.Callback(self.checkStd))
						pm.button(l='Batch!', h=35, c=pm.Callback(self.batch))

					with pm.frameLayout(l='Result', bs='etchedIn', mh=15, cll=True, cl=True):
						with pm.columnLayout(adj=True, rs=5, co=['both', 15]):
							with pm.frameLayout(lv=False, bs='etchedIn', mh=5, mw=5):
								pm.text(l='Success')
								self.successScrollField = pm.scrollField(ed=False, w=240, h=200, wordWrap=False)
							with pm.frameLayout(lv=False, bs='etchedIn', mh=5, mw=5):
								pm.text(l='Failed')
								self.failedScrollField = pm.scrollField(ed=False, w=240, h=200, wordWrap=False)


		self.getAllFunctions()
		self.ui = True



	def batch(self):
		if not self.inputs or not self.func:
			return

		if self.ui == True:
			self.successScrollField.clear()
			self.failedScrollField.clear()

		status = 'Success'

		inputNum = len(self.inputs)

		# set start time
		startTime = datetime.datetime.now()

		for i in range(inputNum):

			if self.batchMode == 1:
				confirm = True
			elif self.batchMode == 2:
				confirm = not self.statusDict[status]
			elif self.batchMode == 3:
				confirm = False

			proceed = True
			if confirm == True:
				proceed = self.confirmUser(self.inputs[i])

			if proceed == True:
				if i == 0:
					self.createLog()
					self.switchInputToTSL()

				self.selectInputTSL(i)
				self.setProcessText(self.inputs[i], i, inputNum)

				if self.stdMode == True:
					maya = subprocess.Popen([self.mayaPath, self.batchPath , self.funcName, self.inputs[i], self.logFile.name], 
				   		   stdout=subprocess.PIPE, stderr=subprocess.PIPE)
					out, err = maya.communicate()
					result = out.split('_STD_BATCH_RESULT_')
					status = result[-2]
					output = result[-1]
				else:
					status, output = self.executeFunc(self.inputs[i])
				
				self.fillResultScrollField(status, self.inputs[i], i)
				niceResult = self.logResult(self.inputs[i], status, output)
				print niceResult

			else:
				self.finishLog(i, inputNum, startTime)
				self.switchInputToScrollField()
				self.resetProcessText()
				return

		self.finishLog(i, inputNum, startTime)
		self.switchInputToScrollField()
		self.resetProcessText()



	def browseLogPath(self):
		path = pm.fileDialog2(dir=self.logPath, fm=3, ds=2,
					   cap='Select your logging directory...', okc='Select')

		if path:
			self.logPath
			self.logPathTxtFld.setText(str(self.logPath[0]))



	def createLog(self):
		# get date time
		datetimes = str(datetime.datetime.now())
		splits = datetimes.split(' ')
		date = splits[0]
		time = splits[1]

		# dir to write log
		version = misc.genVersion(self.logPath)
		createLogPath = '%s/%s_%s_v%s_%s.%s' %(self.logPath, self.funcName, self.logFileName, version, self.user, self.logExt)
		

		toWrite = '###############  BATCH OPERATOR V%s  ###############' %self._VERSION
		toWrite += '\n#####################################################'
		toWrite += '\nfunction : %s' %self.funcName
		toWrite += '\nfrom : %s' %self.funcPath
		toWrite += '\ndate : %s' %date
		toWrite += '\ntime : %s' %time
		toWrite += '\n#####################################################'

		self.logFile = misc.writeLog(toWrite=toWrite, dir=createLogPath, mode='w')



	def checkStd(self):
		self.stdMode = self.stdChkBox.getValue()



	def confirmUser(self, input):
		press = pm.confirmDialog( title='Confirm Continue', 
				message='Proceed?\n%s' %input,
				button=['Yes', 'No'], defaultButton='Yes', cancelButton='No', dismissString='No' )
		if press == 'Yes':
			return True
		else:
			return False



	def executeFunc(self, input):
		status, output = self.func(input)
		return status, output



	def fillInput(self):
		txts = self.inputScrollField.getText()
		if txts:
			self.inputs = txts.split()
		else:
			self.inputs = []



	def fillResultScrollField(self, status, input, index):
		if self.ui == False:
			return

		if index > 0:
			toFill = '\n%s' %input
		else:
			toFill = input

		if self.statusDict[status] == True:
			field = self.successScrollField
		else:
			field = self.failedScrollField

		field.insertText(toFill)



	def finishLog(self, current, lenInputs, startTime):
		endTime = datetime.datetime.now()
		timeSpent = str(endTime - startTime)
		timeSplit = timeSpent.split(':')

		hr = timeSplit[0].zfill(2)
		minute = timeSplit[1]
		sec = timeSplit[2].split('.')[0]
		timeStr = '%s hr %s min %s sec' %(hr, minute, sec)

		toWrite = '\n\n\n#####################################################'
		toWrite += '\n#########  %s/%s done in %s  #########' %(current+1, lenInputs, timeStr)
		toWrite += '\n#####################################################'

		misc.writeLog(toWrite, self.logFile.name, 'a')



	def getAllFunctions(self):
		funcs = inspect.getmembers(sys.modules[batch.__name__], inspect.isfunction)
		allFuncs = zip(*funcs)[0]

		self.funcMenu.clear()
		pm.menuItem(l='', parent=self.funcMenu)

		for func in allFuncs:
			pm.menuItem(l=func, parent=self.funcMenu)



	def loadFunction(self, func):
		exec('self.func = batch.%s' %func)
		self.funcName = self.func.__name__
		self.funcPath = inspect.getfile(self.func)



	def logResult(self, input, status, output):
		# if isinstance(output, (tuple, list)) == False:
		outputs = output.split(',')

		toWrite = '\n\n\n%s:\t%s\n' %(status, input)
		for i in outputs:
			toWrite += '\n\t\t\t%s' % i

		misc.writeLog(toWrite, self.logFile.name, 'a')
		return toWrite



	def resetProcessText(self):
		if self.ui == False:
			return

		self.currentProcessTxt.setLabel('No operation')
		self.currentInputTxt.setLabel('')
		self.currentInputTxt.setBackgroundColor([0.0, 0.0, 0.0])



	def selectMode(self):
		self.batchMode = self.confirmMenu.getSelect()



	def seeLog(self):
		if not self.logFile:
			logPath = self.logPath
		else:
			logPath = self.logFile.name

		logPath = misc.convertOsPath(logPath)

		if not os.path.exists(logPath):
			return

		try:
			subprocess.Popen(r'explorer "%s"' %logPath)
		except:
			return



	def setLogPath(self):
		path = misc.cleanPath(self.logPathTxtFld.getText())
		self.logPath = path



	def selectFunc(self):
		func = self.funcMenu.getValue()
		if func:
			self.loadFunction(func)
			print (self.func.__doc__)

	

	def selectInputTSL(self, i):
		self.inputTSL.setSelectIndexedItem(i+1)



	def switchInputToTSL(self):
		if self.ui == False:
			return

		self.inputScrollField.setVisible(False)
		self.inputTSL.setVisible(True)

		self.inputTSL.removeAll()
		for i in self.inputs:
			self.inputTSL.append(i)



	def switchInputToScrollField(self):
		if self.ui == False:
			return

		self.inputTSL.setVisible(False)
		self.inputScrollField.setVisible(True)



	def setProcessText(self, input, current, lenInputs):
		if self.ui == False:
			return

		self.currentProcessTxt.setLabel('Now processing...(%s/%s)' %(current+1, lenInputs))
		self.currentInputTxt.setLabel(input)
		self.currentInputTxt.setBackgroundColor([0.7, 0.7, 0.0])

		pm.refresh(cv=self.currentProcessTxt, fe=True)
		pm.refresh(cv=self.currentInputTxt, fe=True)


	