//Maya ASCII 2012 scene
//Name: TEMPLATE_skirtPos_02.ma
//Last modified: Thu, Nov 28, 2013 04:36:08 PM
//Codeset: 1252
requires maya "2012";
requires "Mayatomr" "2012.0m - 3.9.1.36 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2012";
fileInfo "version" "2012 x64";
fileInfo "cutIdentifier" "001200000000-796618";
fileInfo "osv" "Microsoft Windows 7 Business Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -10.454312350333938 2.3075313044709196 9.1352426799156881 ;
	setAttr ".r" -type "double3" -8.7383527296120143 -55.400000000000134 -1.4002772369915344e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 13.96706859732282;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.71190467155329817 1.4279886859071584 1.1470726559142645 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.087780912359665897 100.10003952158785 0.0096439053204650325 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 13.180658013390111;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.2101482220101816 1.6980866886394697 100.17266742256591 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 9.3867486857655322;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.10000000000002 1.2377201718929918 -1.5105509624715336 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 7.5128283551999333;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "TEMP_tempRoot_ctrl";
	addAttr -ci true -sn "_TYPE" -ln "_TYPE" -dt "string";
	addAttr -ci true -sn "shapeType" -ln "shapeType" -dt "string";
	addAttr -ci true -sn "nodes" -ln "nodes" -at "message";
	addAttr -ci true -sn "loftNodes" -ln "loftNodes" -at "message";
	addAttr -ci true -sn "surfaces" -ln "surfaces" -at "message";
	addAttr -ci true -sn "hip_loc" -ln "hip_loc" -at "message";
	addAttr -ci true -sn "upLegLFT_loc" -ln "upLegLFT_loc" -at "message";
	addAttr -ci true -sn "upLegRHT_loc" -ln "upLegRHT_loc" -at "message";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".rp" -type "double3" -5.5511151231257827e-016 2.3776667611844418 0.021088135040003064 ;
	setAttr ".sp" -type "double3" -5.5511151231257827e-016 2.3776667611844418 0.021088135040003064 ;
	setAttr -l on "._TYPE" -type "string" "rigTempRoot";
	setAttr -l on ".shapeType" -type "string" "crossCircle";
createNode nurbsCurve -n "TEMP_tempRoot_ctrlShape" -p "TEMP_tempRoot_ctrl";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 49 0 no 3
		50 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49
		50
		-1.772028641110666 2.3776667611844418 0.021088135040003064
		-1.5746093592746542 2.3776667611844418 0.021088135040003064
		-1.5552185533516489 2.3776667611844418 -0.2252352021907354
		-1.4975364414420236 2.3776667611844418 -0.46548507329224509
		-1.4029864921698159 2.3776667611844418 -0.69376199163379182
		-1.2738778879695767 2.3776667611844418 -0.90443534799140357
		-1.1134213414044143 2.3776667611844418 -1.0923332063644091
		-0.92552348303140863 2.3776667611844418 -1.2527897529295706
		-0.71485012667379721 2.3776667611844418 -1.3818983571298102
		-0.48657320833225026 2.3776667611844418 -1.4764483064020166
		-0.24632333723073954 2.3776667611844418 -1.534130418311642
		-5.5511151231257827e-016 2.3776667611844418 -1.5535212242346459
		-5.5511151231257827e-016 2.3776667611844418 -1.7509405060706604
		-5.5511151231257827e-016 2.3776667611844418 -1.5535212242346459
		0.24632333723073843 2.3776667611844418 -1.534130418311642
		0.48657320833224904 2.3776667611844418 -1.4764483064020166
		0.7148501266737961 2.3776667611844418 -1.3818983571298102
		0.92552348303140752 2.3776667611844418 -1.2527897529295706
		1.1134213414044134 2.3776667611844418 -1.0923332063644091
		1.2738778879695758 2.3776667611844418 -0.90443534799140357
		1.402986492169815 2.3776667611844418 -0.69376199163379182
		1.4975364414420227 2.3776667611844418 -0.46548507329224509
		1.555218553351648 2.3776667611844418 -0.2252352021907354
		1.5746093592746528 2.3776667611844418 0.021088135040003064
		1.7720286411106652 2.3776667611844418 0.021088135040003064
		1.5746093592746528 2.3776667611844418 0.021088135040003064
		1.555218553351648 2.3776667611844418 0.26741147227074152
		1.4975364414420227 2.3776667611844418 0.50766134337225122
		1.402986492169815 2.3776667611844418 0.73593826171379795
		1.2738778879695758 2.3776667611844418 0.9466116180714097
		1.1134213414044134 2.3776667611844418 1.134509476444415
		0.92552348303140752 2.3776667611844418 1.2949660230095765
		0.7148501266737961 2.3776667611844418 1.4240746272098166
		0.48657320833224904 2.3776667611844418 1.518624576482023
		0.24632333723073843 2.3776667611844418 1.5763066883916483
		-5.5511151231257827e-016 2.3776667611844418 1.5956974943146522
		-5.5511151231257827e-016 2.3776667611844418 1.7931167761506663
		-5.5511151231257827e-016 2.3776667611844418 1.5956974943146522
		-0.24632333723073954 2.3776667611844418 1.5763066883916483
		-0.48657320833225026 2.3776667611844418 1.518624576482023
		-0.71485012667379721 2.3776667611844418 1.4240746272098166
		-0.92552348303140863 2.3776667611844418 1.2949660230095765
		-1.1134213414044143 2.3776667611844418 1.134509476444415
		-1.2738778879695767 2.3776667611844418 0.9466116180714097
		-1.4029864921698159 2.3776667611844418 0.73593826171379795
		-1.4975364414420236 2.3776667611844418 0.50766134337225122
		-1.5552185533516489 2.3776667611844418 0.26741147227074152
		-1.5746093592746542 2.3776667611844418 0.021088135040003064
		-1.772028641110666 2.3776667611844418 0.021088135040003064
		-1.5746093592746542 2.3776667611844418 0.021088135040003064
		;
createNode transform -n "TEMP_use_grp" -p "TEMP_tempRoot_ctrl";
createNode transform -n "TEMP_skirtPosSkinUP_nrbs" -p "TEMP_use_grp";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -dt "string";
createNode nurbsSurface -n "TEMP_skirtPosSkinUP_nrbsShape" -p "TEMP_skirtPosSkinUP_nrbs";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 2 no 
		9 0 0 0 0.34362137771231138 0.68724275542462276 1.09185075113247 1.496458746840317
		 1.496458746840317 1.496458746840317
		19 -0.14285714285714285 -0.071428571428571425 0 0.071428571428571425 0.14285714285714285
		 0.21428571428571427 0.2857142857142857 0.35714285714285715 0.42857142857142855 0.5
		 0.5714285714285714 0.6428571428571429 0.7142857142857143 0.7857142857142857 0.8571428571428571
		 0.9285714285714286 1 1.0714285714285714 1.1428571428571428
		
		119
		0.40161904558499423 2.3810782346047734 -0.6555097445758471
		-1.0563918926607116e-016 2.3810782346047734 -0.71348737383288796
		-0.40161904558499273 2.3810782346047734 -0.6555097445758471
		-0.7474869474886745 2.3810782346047734 -0.44302235081432784
		-0.92661966950650709 2.3810782346047734 -0.10001826035178799
		-0.95289875719560235 2.3810782346047734 0.27933633639391919
		-0.75275911542024454 2.3810782346047734 0.62102829820522465
		-0.39994582740344442 2.3810782346047734 0.81493064746692778
		-8.959711171289248e-016 2.3810782346047734 0.8828367240961541
		0.39994582740344581 2.3810782346047734 0.81493064746692778
		0.75275911542024343 2.3810782346047734 0.62102829820522309
		0.95289875719560257 2.3810782346047734 0.27933633639392086
		0.92661966950650687 2.3810782346047734 -0.10001826035178943
		0.74748694748867506 2.3810782346047734 -0.44302235081432606
		0.40161904558499423 2.3810782346047734 -0.6555097445758471
		-1.0563918926607116e-016 2.3810782346047734 -0.71348737383288796
		-0.40161904558499273 2.3810782346047734 -0.6555097445758471
		0.42282844227551908 2.2788703615468373 -0.70669673566407232
		-1.1809525862148361e-016 2.2789110390423151 -0.76845142457409976
		-0.42282844227551736 2.2788703615468373 -0.70669673566407232
		-0.78698940562048869 2.2788070030562535 -0.48030325188111578
		-0.9755676036920653 2.278846430427409 -0.11478611206791062
		-1.0031485320361657 2.2790014554554725 0.28943956356001377
		-0.79231858707294001 2.2793105546826702 0.65341681538125618
		-0.42089682001592432 2.2795974099209002 0.8598558244204082
		-9.4962671582499252e-016 2.2797068061610357 0.93213056325560228
		0.42089682001592577 2.2795974099209002 0.8598558244204082
		0.79231858707293878 2.2793105546826702 0.65341681538125451
		1.0031485320361659 2.2790014554554725 0.28943956356001554
		0.97556760369206508 2.278846430427409 -0.11478611206791205
		0.78698940562048936 2.2788070030562539 -0.48030325188111428
		0.42282844227551908 2.2788703615468373 -0.70669673566407232
		-1.1809525862148361e-016 2.2789110390423151 -0.76845142457409976
		-0.42282844227551736 2.2788703615468373 -0.70669673566407232
		0.465677785919556 2.075329392749814 -0.81003693825930467
		-1.4443189627788405e-016 2.0753683222478068 -0.87939868720900827
		-0.46567778591955378 2.075329392749814 -0.81003693825930467
		-0.8667392403925831 2.0752700308491656 -0.55576404386549216
		-1.0744275887921275 2.0753114687162437 -0.14524376940316941
		-1.1048105029431499 2.0754644738057615 0.30875458552677237
		-0.87262664860786199 2.0757667706822955 0.71756595896117825
		-0.4635642340058157 2.076047286260728 0.94944968040570177
		-1.0602644735593039e-015 2.0761542979481513 1.0306356557760983
		0.46356423400581726 2.076047286260728 0.94944968040570177
		0.87262664860786043 2.0757667706822955 0.71756595896117625
		1.1048105029431501 2.0754644738057615 0.30875458552677432
		1.0744275887921275 2.0753114687162437 -0.14524376940317082
		0.86673924039258377 2.0752700308491661 -0.55576404386549105
		0.465677785919556 2.075329392749814 -0.81003693825930467
		-1.4443189627788405e-016 2.0753683222478068 -0.87939868720900827
		-0.46567778591955378 2.075329392749814 -0.81003693825930467
		0.53536276866437216 1.7536800024651464 -0.97786555097058492
		-1.9094746762317416e-016 1.7536543059577854 -1.0594033731240966
		-0.5353627686643696 1.7536800024651464 -0.97786555097058492
		-0.99639154118850826 1.7537213007969825 -0.67906789690893732
		-1.235184234702392 1.7537000987824887 -0.19677818063216748
		-1.2702633914439381 1.7536068092519475 0.33663973830256683
		-1.0035454845047709 1.7534180275092799 0.81717103245197709
		-0.53322904888786915 1.7532428044060668 1.0899284977076662
		-1.2447649148178189e-015 1.753176013800166 1.1854633578823162
		0.53322904888787082 1.7532428044060668 1.0899284977076662
		1.0035454845047691 1.7534180275092799 0.81717103245197464
		1.2702633914439381 1.7536068092519475 0.3366397383025691
		1.2351842347023918 1.7537000987824887 -0.19677818063216906
		0.99639154118850903 1.7537213007969823 -0.67906789690893632
		0.53536276866437216 1.7536800024651464 -0.97786555097058492
		-1.9094746762317416e-016 1.7536543059577854 -1.0594033731240966
		-0.5353627686643696 1.7536800024651464 -0.97786555097058492
		0.61106547663296973 1.4148340155500916 -1.1599258023263104
		-2.4560279568081356e-016 1.4149014352076512 -1.254334327434856
		-0.61106547663296729 1.4148340155500916 -1.1599258023263104
		-1.1373482139612814 1.4147209712949769 -0.81382951196414166
		-1.409890807806538 1.4147629589939781 -0.25505438096468908
		-1.4497665683499599 1.414990632069826 0.36289781795974996
		-1.1450897868664671 1.4154620783603156 0.9193192216010162
		-0.60830542014045141 1.4158997667237583 1.2349084769889322
		-1.4474812842729075e-015 1.4160664761601041 1.3453981258341454
		0.6083054201404533 1.4158997667237583 1.2349084769889322
		1.1450897868664651 1.4154620783603156 0.91931922160101376
		1.4497665683499601 1.414990632069826 0.36289781795975246
		1.4098908078065377 1.4147629589939781 -0.25505438096469102
		1.1373482139612823 1.4147209712949769 -0.81382951196414011
		0.61106547663296973 1.4148340155500916 -1.1599258023263104
		-2.4560279568081356e-016 1.4149014352076512 -1.254334327434856
		-0.61106547663296729 1.4148340155500916 -1.1599258023263104
		0.66559674167462446 1.1785995032043104 -1.290876170223008
		-2.8805744749897089e-016 1.1786648646262519 -1.3943964963543527
		-0.66559674167462224 1.1785995032043104 -1.290876170223008
		-1.2388401336483501 1.1784911648620069 -0.91138567762238709
		-1.5357030060546033 1.1785355198838185 -0.29870538309236494
		-1.5791448199637057 1.1787608145183368 0.37886497814671638
		-1.247292527598229 1.1792242511631914 0.9889890360997653
		-0.66260548103771333 1.1796544746911768 1.3350561146079405
		-1.5972218422867035e-015 1.1798183763595667 1.456219687722172
		0.66260548103771533 1.1796544746911768 1.3350561146079405
		1.2472925275982267 1.1792242511631914 0.98898903609976263
		1.5791448199637061 1.1787608145183368 0.37886497814671916
		1.5357030060546029 1.1785355198838185 -0.29870538309236733
		1.2388401336483512 1.1784911648620069 -0.91138567762238476
		0.66559674167462446 1.1785995032043104 -1.290876170223008
		-2.8805744749897089e-016 1.1786648646262519 -1.3943964963543527
		-0.66559674167462224 1.1785995032043104 -1.290876170223008
		0.69315547566566127 1.0611427432982721 -1.357007513049123
		-3.1027815902458189e-016 1.0611427432982721 -1.4651196909813149
		-0.69315547566565927 1.0611427432982721 -1.357007513049123
		-1.2900898907463174 1.0611427432982721 -0.96077753232939855
		-1.5992555752488915 1.0611427432982721 -0.32117015729945542
		-1.6446107289136329 1.0611427432982721 0.38622081725967605
		-1.2991891406712639 1.0611427432982721 1.0233814367387279
		-0.69026766355297819 1.0611427432982721 1.384955470611545
		-1.6743143256699699e-015 1.0611427432982721 1.5115814457211476
		0.69026766355298019 1.0611427432982721 1.384955470611545
		1.2991891406712617 1.0611427432982721 1.0233814367387253
		1.6446107289136334 1.0611427432982721 0.38622081725967894
		1.5992555752488911 1.0611427432982721 -0.32117015729945797
		1.2900898907463187 1.0611427432982721 -0.96077753232939567
		0.69315547566566127 1.0611427432982721 -1.357007513049123
		-3.1027815902458189e-016 1.0611427432982721 -1.4651196909813149
		-0.69315547566565927 1.0611427432982721 -1.357007513049123
		
		;
createNode transform -n "TEMP_skirtPosSkinLO_nrbs" -p "TEMP_use_grp";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -dt "string";
createNode nurbsSurface -n "TEMP_skirtPosSkinLO_nrbsShape" -p "TEMP_skirtPosSkinLO_nrbs";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 2 no 
		9 0 0 0 0.29884309777751994 0.59768619555503988 0.99451400602524542 1.391341816495451
		 1.391341816495451 1.391341816495451
		19 -0.14285714285714285 -0.071428571428571425 0 0.071428571428571425 0.14285714285714285
		 0.21428571428571427 0.2857142857142857 0.35714285714285715 0.42857142857142855 0.5
		 0.5714285714285714 0.6428571428571429 0.7142857142857143 0.7857142857142857 0.8571428571428571
		 0.9285714285714286 1 1.0714285714285714 1.1428571428571428
		
		119
		0.54195677273809884 1.2598612174123747 -0.96021919014900203
		-1.9429980759233245e-016 1.2598612174123747 -1.0384559400954207
		-0.54195677273809706 1.2598612174123747 -0.96021919014900203
		-1.0086812818718345 1.2598612174123747 -0.67348233482187703
		-1.2504083438321725 1.2598612174123747 -0.21062233864745641
		-1.2858701320890984 1.2598612174123747 0.30129011977741849
		-1.0157957032344098 1.2598612174123747 0.76237949023809992
		-0.53969888199380278 1.2598612174123747 1.0240371297037287
		-1.2607973896294018e-015 1.2598612174123747 1.1156716239934006
		0.53969888199380456 1.2598612174123747 1.0240371297037287
		1.0157957032344083 1.2598612174123747 0.76237949023809815
		1.2858701320890986 1.2598612174123747 0.30129011977742071
		1.2504083438321723 1.2598612174123747 -0.21062233864745808
		1.0086812818718356 1.2598612174123747 -0.67348233482187492
		0.54195677273809884 1.2598612174123747 -0.96021919014900203
		-1.9429980759233245e-016 1.2598612174123747 -1.0384559400954207
		-0.54195677273809706 1.2598612174123747 -0.96021919014900203
		0.55257031688908254 1.1686259496908782 -1.008203669575231
		-2.0234297949804604e-016 1.1689660265766304 -1.0894430624121332
		-0.55257031688908054 1.1686259496908782 -1.008203669575231
		-1.0289039427593636 1.1677332015688484 -0.71016130192309945
		-1.2760065898531079 1.1669009907742209 -0.22852524504241339
		-1.3123984179294452 1.1665704612983063 0.30447506552230263
		-1.0364862424530126 1.167049175158563 0.78445533944794099
		-0.55048957170796387 1.1677469543138104 1.0565785544915034
		-1.2901791961291482e-015 1.1680348918775283 1.1518224035913733
		0.55048957170796564 1.1677469543138104 1.0565785544915034
		1.0364862424530108 1.167049175158563 0.78445533944793944
		1.3123984179294452 1.1665704612983063 0.30447506552230491
		1.2760065898531079 1.1669009907742209 -0.22852524504241523
		1.0289039427593649 1.1677332015688484 -0.71016130192309701
		0.55257031688908254 1.1686259496908782 -1.008203669575231
		-2.0234297949804604e-016 1.1689660265766304 -1.0894430624121332
		-0.55257031688908054 1.1686259496908782 -1.008203669575231
		0.57689487322719857 0.98456707247014374 -1.1034256804829021
		-2.2096192970754051e-016 0.98485323821014792 -1.1910522529381438
		-0.57689487322719624 0.98456707247014374 -1.1034256804829021
		-1.0742112076101353 0.98381591175988459 -0.78203822221673414
		-1.332213516340041 0.98310642080536925 -0.26283039949842324
		-1.3702241400104405 0.98279711295978767 0.31165534121160515
		-1.0821628025085614 0.98313432426370151 0.82902084335150317
		-0.57474938571054213 0.98366924656047994 1.1224100947101201
		-1.3567605925708609e-015 0.98389417643551835 1.2251126467472204
		0.57474938571054413 0.98366924656047994 1.1224100947101201
		1.0821628025085592 0.98313432426370151 0.82902084335150183
		1.3702241400104405 0.98279711295978767 0.31165534121160765
		1.3322135163400408 0.98310642080536925 -0.26283039949842546
		1.0742112076101367 0.98381591175988459 -0.78203822221673081
		0.57689487322719857 0.98456707247014374 -1.1034256804829021
		-2.2096192970754051e-016 0.98485323821014792 -1.1910522529381438
		-0.57689487322719624 0.98456707247014374 -1.1034256804829021
		0.62633750432953816 0.67932799064193516 -1.2563943138253637
		-2.589970238112592e-016 0.67904828882715673 -1.3547262113870389
		-0.62633750432953539 0.67932799064193516 -1.2563943138253637
		-1.1654528171231937 0.68006230929829736 -0.89627256291865365
		-1.4444401579343218 0.68073756722601741 -0.31541436741509521
		-1.4852939886916539 0.68097824061748957 0.32671953373434148
		-1.1735067698833783 0.68051890075049737 0.90518896557513384
		-0.62361769416016888 0.67989276035557111 1.2336768989712494
		-1.4913389671991431e-015 0.67963857938262939 1.3487640730434531
		0.62361769416017099 0.67989276035557111 1.2336768989712494
		1.173506769883375 0.68051890075049737 0.90518896557513273
		1.4852939886916541 0.68097824061748957 0.3267195337343442
		1.4444401579343213 0.68073756722601741 -0.31541436741509771
		1.165452817123195 0.68006230929829736 -0.89627256291864976
		0.62633750432953816 0.67932799064193516 -1.2563943138253637
		-2.589970238112592e-016 0.67904828882715673 -1.3547262113870389
		-0.62633750432953539 0.67932799064193516 -1.2563943138253637
		0.6904048428442594 0.35339193672872066 -1.4136114627906711
		-3.0804252046119921e-016 0.35439646457088703 -1.5222764738954813
		-0.69040484284425674 0.35339193672872066 -1.4136114627906711
		-1.2855359813478544 0.35075443888421354 -1.0143453021351816
		-1.5942287291574979 0.34836538313004761 -0.36806280098614347
		-1.6396152135138267 0.34762319377909889 0.34778966641634579
		-1.2948030892362117 0.34953000196124229 0.99225053777455563
		-0.68764395428443026 0.35198345492304361 1.357137063230496
		-1.6667503447284745e-015 0.3529643638894272 1.4847408330233485
		0.68764395428443237 0.35198345492304361 1.357137063230496
		1.2948030892362081 0.34953000196124229 0.99225053777455452
		1.6396152135138271 0.34762319377909884 0.34778966641634868
		1.5942287291574972 0.34836538313004761 -0.36806280098614602
		1.285535981347856 0.35075443888421354 -1.0143453021351776
		0.6904048428442594 0.35339193672872066 -1.4136114627906711
		-3.0804252046119921e-016 0.35439646457088703 -1.5222764738954813
		-0.69040484284425674 0.35339193672872066 -1.4136114627906711
		0.74586871827334411 0.11847257728518777 -1.5218809237889968
		-3.5064680571437456e-016 0.11940551758832037 -1.6384621742274472
		-0.74586871827334145 0.11847257728518777 -1.5218809237889968
		-1.3888106558731206 0.11602309055092874 -1.0936866216225565
		-1.7223085068123485 0.11379699203357072 -0.40085694456648013
		-1.7713602303551901 0.11308298246379241 0.36638522879667101
		-1.3988722210876972 0.11480189221352409 1.0571583671709379
		-0.74292535511926894 0.11703909078950582 1.448393467425191
		-1.8186261942272421e-015 0.11793633309827732 1.585239547947721
		0.74292535511927094 0.11703909078950585 1.448393467425191
		1.3988722210876938 0.11480189221352406 1.0571583671709364
		1.7713602303551905 0.11308298246379234 0.36638522879667396
		1.7223085068123483 0.11379699203357072 -0.40085694456648258
		1.3888106558731221 0.11602309055092871 -1.0936866216225525
		0.74586871827334411 0.11847257728518777 -1.5218809237889968
		-3.5064680571437456e-016 0.11940551758832037 -1.6384621742274472
		-0.74586871827334145 0.11847257728518777 -1.5218809237889968
		0.77604059521592528 5.7746336079876891e-017 -1.5752868926591499
		-3.7393146261699589e-016 6.2975341896113039e-017 -1.6963267485904876
		-0.77604059521592272 5.7746336079876891e-017 -1.5752868926591499
		-1.4443543502781517 4.1486921203742253e-017 -1.1316771097531786
		-1.7904889914152045 1.7947161522761705e-017 -0.41558771069583234
		-1.8412675565160488 -1.0278920922733736e-017 0.37639061483070646
		-1.454541656843094 -4.0045879182437063e-017 1.0897406838232668
		-0.7728074685805969 -5.7270517175993734e-017 1.4945504823635118
		-1.901074327127987e-015 -6.3210437055195038e-017 1.6363179509094161
		0.77280746858059879 -5.7270517175993685e-017 1.4945504823635118
		1.4545416568430907 -4.0045879182437137e-017 1.0897406838232648
		1.8412675565160492 -1.0278920922733767e-017 0.37639061483070946
		1.7904889914152042 1.7947161522761742e-017 -0.41558771069583467
		1.4443543502781535 4.1486921203742142e-017 -1.131677109753175
		0.77604059521592528 5.7746336079876891e-017 -1.5752868926591499
		-3.7393146261699589e-016 6.2975341896113039e-017 -1.6963267485904876
		-0.77604059521592272 5.7746336079876891e-017 -1.5752868926591499
		
		;
createNode transform -n "TEMP_tmp_grp" -p "TEMP_tempRoot_ctrl";
createNode transform -n "TEMP_ctrl_grp" -p "TEMP_tmp_grp";
createNode transform -n "TEMP_hip_tmpLoc" -p "TEMP_ctrl_grp";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr -l on -k off ".v";
	setAttr -l on ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267757 ;
	setAttr ".sp" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267757 ;
createNode locator -n "TEMP_hip_tmpLocShape" -p "TEMP_hip_tmpLoc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".lp" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267761 ;
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "TEMP_upLegLFT_tmpLoc" -p "TEMP_ctrl_grp";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr -l on -k off ".v";
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.65052955929067524 1.7177713660101057 0.0018951183943218888 ;
	setAttr ".sp" -type "double3" 0.65052955929067524 1.7177713660101057 0.0018951183943218888 ;
createNode locator -n "TEMP_upLegLFT_tmpLocShape" -p "TEMP_upLegLFT_tmpLoc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".lp" -type "double3" 0.65052955929067524 1.7177713660101055 0.0018951183943218888 ;
	setAttr ".los" -type "double3" 0.1 0.1 0.1 ;
createNode transform -n "TEMP_upLegRHT_tmpLoc" -p "TEMP_ctrl_grp";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr -l on -k off ".v";
	setAttr ".r" -type "double3" 0 180 0 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.65053000000000016 1.7177674982481212 0.0018951200000008502 ;
	setAttr ".sp" -type "double3" -0.65053000000000016 1.7177674982481212 0.0018951200000008502 ;
createNode locator -n "TEMP_upLegRHT_tmpLocShape" -p "TEMP_upLegRHT_tmpLoc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".lp" -type "double3" -0.65053000000000016 1.7177674982481212 0.0018951200000008502 ;
	setAttr ".los" -type "double3" 0.1 0.1 0.1 ;
createNode transform -n "TEMP_still_grp" -p "TEMP_tmp_grp";
	setAttr ".it" no;
createNode transform -n "TEMP_ann_grp" -p "TEMP_still_grp";
createNode transform -n "TEMP_upLegLFTPointer_loc" -p "TEMP_ann_grp";
	setAttr ".v" no;
createNode locator -n "TEMP_upLegLFTPointer_locShape" -p "TEMP_upLegLFTPointer_loc";
	setAttr -k off ".v";
createNode parentConstraint -n "TEMP_upLegLFTPointer_loc_parentConstraint1" -p "TEMP_upLegLFTPointer_loc";
	addAttr -ci true -k true -sn "w0" -ln "upLegLFT_tmpLocW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 0 180 ;
	setAttr ".rst" -type "double3" 0.65052955929067524 1.7177713660101057 0.0018951183943218888 ;
	setAttr -k on ".w0";
createNode transform -n "TEMP_upLegLFTPointer_ann" -p "TEMP_ann_grp";
createNode annotationShape -n "TEMP_upLegLFTPointer_annShape" -p "TEMP_upLegLFTPointer_ann";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "TEMP_upLegLFTPointer_ann_parentConstraint1" -p "TEMP_upLegLFTPointer_ann";
	addAttr -ci true -k true -sn "w0" -ln "hip_tmpLocW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267757 ;
	setAttr -k on ".w0";
createNode transform -n "TEMP_upLegRHTPointer_loc" -p "TEMP_ann_grp";
	setAttr ".v" no;
createNode locator -n "TEMP_upLegRHTPointer_locShape" -p "TEMP_upLegRHTPointer_loc";
	setAttr -k off ".v";
createNode parentConstraint -n "TEMP_upLegRHTPointer_loc_parentConstraint1" -p "TEMP_upLegRHTPointer_loc";
	addAttr -ci true -k true -sn "w0" -ln "upLegRHT_tmpLocW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 180 0 ;
	setAttr ".rst" -type "double3" -0.65053000000000016 1.7177674982481212 0.0018951200000008502 ;
	setAttr -k on ".w0";
createNode transform -n "TEMP_upLegRHTPointer_ann" -p "TEMP_ann_grp";
createNode annotationShape -n "TEMP_upLegRHTPointer_annShape" -p "TEMP_upLegRHTPointer_ann";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "TEMP_upLegRHTPointer_ann_parentConstraint1" -p "TEMP_upLegRHTPointer_ann";
	addAttr -ci true -k true -sn "w0" -ln "hip_tmpLocW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267757 ;
	setAttr -k on ".w0";
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".minsp" 1;
	setAttr ".maxsp" 3;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "256";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	setAttr ".dat" 16;
createNode multDoubleLinear -n "TEMP_skirtPosLegMirror_mdl";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr ".i2" -1;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
lockNode -l 1 ;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
lockNode -l 1 ;
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".dsm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
lockNode -l 1 ;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
lockNode -l 1 ;
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
lockNode -l 1 ;
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
lockNode -l 1 ;
select -ne :defaultRenderUtilityList1;
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
lockNode -l 1 ;
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
lockNode -l 1 ;
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
lockNode -l 1 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".rp";
	setAttr -k on ".cai";
	setAttr -k on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -k on ".ei";
	setAttr -k on ".ex";
	setAttr -av -k on ".es";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bf";
	setAttr -k on ".fii";
	setAttr -av -k on ".sf";
	setAttr -k on ".gr";
	setAttr -k on ".li";
	setAttr -k on ".ls";
	setAttr -k on ".mb";
	setAttr -k on ".ti";
	setAttr -k on ".txt";
	setAttr -k on ".mpr";
	setAttr -k on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -k on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -k on ".as";
	setAttr -k on ".ds";
	setAttr -k on ".lm";
	setAttr -k on ".fir";
	setAttr -k on ".aap";
	setAttr -k on ".gh";
	setAttr -cb on ".sd";
lockNode -l 1 ;
select -ne :ikSystem;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".gsn";
	setAttr -k on ".gsv";
	setAttr -s 4 ".sol";
connectAttr "TEMP_tempRoot_ctrl.surfaces" "TEMP_skirtPosSkinUP_nrbs.tempRoot";
connectAttr "TEMP_tempRoot_ctrl.surfaces" "TEMP_skirtPosSkinLO_nrbs.tempRoot";
connectAttr "TEMP_tempRoot_ctrl.hip_loc" "TEMP_hip_tmpLoc.tempRoot";
connectAttr "TEMP_tempRoot_ctrl.upLegLFT_loc" "TEMP_upLegLFT_tmpLoc.tempRoot";
connectAttr "TEMP_skirtPosLegMirror_mdl.o" "TEMP_upLegRHT_tmpLoc.tx";
connectAttr "TEMP_upLegLFT_tmpLoc.ty" "TEMP_upLegRHT_tmpLoc.ty";
connectAttr "TEMP_upLegLFT_tmpLoc.tz" "TEMP_upLegRHT_tmpLoc.tz";
connectAttr "TEMP_tempRoot_ctrl.upLegRHT_loc" "TEMP_upLegRHT_tmpLoc.tempRoot";
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.ctx" "TEMP_upLegLFTPointer_loc.tx"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.cty" "TEMP_upLegLFTPointer_loc.ty"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.ctz" "TEMP_upLegLFTPointer_loc.tz"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.crx" "TEMP_upLegLFTPointer_loc.rx"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.cry" "TEMP_upLegLFTPointer_loc.ry"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.crz" "TEMP_upLegLFTPointer_loc.rz"
		;
connectAttr "TEMP_upLegLFTPointer_loc.ro" "TEMP_upLegLFTPointer_loc_parentConstraint1.cro"
		;
connectAttr "TEMP_upLegLFTPointer_loc.pim" "TEMP_upLegLFTPointer_loc_parentConstraint1.cpim"
		;
connectAttr "TEMP_upLegLFTPointer_loc.rp" "TEMP_upLegLFTPointer_loc_parentConstraint1.crp"
		;
connectAttr "TEMP_upLegLFTPointer_loc.rpt" "TEMP_upLegLFTPointer_loc_parentConstraint1.crt"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.t" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tt"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.rp" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].trp"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.rpt" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].trt"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.r" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tr"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.ro" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tro"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.s" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].ts"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.pm" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tpm"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.w0" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tw"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.ctx" "TEMP_upLegLFTPointer_ann.tx"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.cty" "TEMP_upLegLFTPointer_ann.ty"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.ctz" "TEMP_upLegLFTPointer_ann.tz"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.crx" "TEMP_upLegLFTPointer_ann.rx"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.cry" "TEMP_upLegLFTPointer_ann.ry"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.crz" "TEMP_upLegLFTPointer_ann.rz"
		;
connectAttr "TEMP_upLegLFTPointer_locShape.wm" "TEMP_upLegLFTPointer_annShape.dom"
		 -na;
connectAttr "TEMP_upLegLFTPointer_ann.ro" "TEMP_upLegLFTPointer_ann_parentConstraint1.cro"
		;
connectAttr "TEMP_upLegLFTPointer_ann.pim" "TEMP_upLegLFTPointer_ann_parentConstraint1.cpim"
		;
connectAttr "TEMP_upLegLFTPointer_ann.rp" "TEMP_upLegLFTPointer_ann_parentConstraint1.crp"
		;
connectAttr "TEMP_upLegLFTPointer_ann.rpt" "TEMP_upLegLFTPointer_ann_parentConstraint1.crt"
		;
connectAttr "TEMP_hip_tmpLoc.t" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tt"
		;
connectAttr "TEMP_hip_tmpLoc.rp" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].trp"
		;
connectAttr "TEMP_hip_tmpLoc.rpt" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].trt"
		;
connectAttr "TEMP_hip_tmpLoc.r" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tr"
		;
connectAttr "TEMP_hip_tmpLoc.ro" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tro"
		;
connectAttr "TEMP_hip_tmpLoc.s" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].ts"
		;
connectAttr "TEMP_hip_tmpLoc.pm" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tpm"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.w0" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tw"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.ctx" "TEMP_upLegRHTPointer_loc.tx"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.cty" "TEMP_upLegRHTPointer_loc.ty"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.ctz" "TEMP_upLegRHTPointer_loc.tz"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.crx" "TEMP_upLegRHTPointer_loc.rx"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.cry" "TEMP_upLegRHTPointer_loc.ry"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.crz" "TEMP_upLegRHTPointer_loc.rz"
		;
connectAttr "TEMP_upLegRHTPointer_loc.ro" "TEMP_upLegRHTPointer_loc_parentConstraint1.cro"
		;
connectAttr "TEMP_upLegRHTPointer_loc.pim" "TEMP_upLegRHTPointer_loc_parentConstraint1.cpim"
		;
connectAttr "TEMP_upLegRHTPointer_loc.rp" "TEMP_upLegRHTPointer_loc_parentConstraint1.crp"
		;
connectAttr "TEMP_upLegRHTPointer_loc.rpt" "TEMP_upLegRHTPointer_loc_parentConstraint1.crt"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.t" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tt"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.rp" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].trp"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.rpt" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].trt"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.r" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tr"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.ro" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tro"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.s" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].ts"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.pm" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tpm"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.w0" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tw"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.ctx" "TEMP_upLegRHTPointer_ann.tx"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.cty" "TEMP_upLegRHTPointer_ann.ty"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.ctz" "TEMP_upLegRHTPointer_ann.tz"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.crx" "TEMP_upLegRHTPointer_ann.rx"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.cry" "TEMP_upLegRHTPointer_ann.ry"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.crz" "TEMP_upLegRHTPointer_ann.rz"
		;
connectAttr "TEMP_upLegRHTPointer_locShape.wm" "TEMP_upLegRHTPointer_annShape.dom"
		 -na;
connectAttr "TEMP_upLegRHTPointer_ann.ro" "TEMP_upLegRHTPointer_ann_parentConstraint1.cro"
		;
connectAttr "TEMP_upLegRHTPointer_ann.pim" "TEMP_upLegRHTPointer_ann_parentConstraint1.cpim"
		;
connectAttr "TEMP_upLegRHTPointer_ann.rp" "TEMP_upLegRHTPointer_ann_parentConstraint1.crp"
		;
connectAttr "TEMP_upLegRHTPointer_ann.rpt" "TEMP_upLegRHTPointer_ann_parentConstraint1.crt"
		;
connectAttr "TEMP_hip_tmpLoc.t" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tt"
		;
connectAttr "TEMP_hip_tmpLoc.rp" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].trp"
		;
connectAttr "TEMP_hip_tmpLoc.rpt" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].trt"
		;
connectAttr "TEMP_hip_tmpLoc.r" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tr"
		;
connectAttr "TEMP_hip_tmpLoc.ro" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tro"
		;
connectAttr "TEMP_hip_tmpLoc.s" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].ts"
		;
connectAttr "TEMP_hip_tmpLoc.pm" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tpm"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.w0" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tw"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "TEMP_upLegLFT_tmpLoc.tx" "TEMP_skirtPosLegMirror_mdl.i1";
connectAttr "TEMP_tempRoot_ctrl.nodes" "TEMP_skirtPosLegMirror_mdl.tempRoot";
connectAttr "TEMP_skirtPosSkinLO_nrbsShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "TEMP_skirtPosSkinUP_nrbsShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "TEMP_skirtPosLegMirror_mdl.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of TEMPLATE_skirtPos_02.ma
