//Maya ASCII 2012 scene
//Name: bshCtrlFriends.ma
//Last modified: Thu, May 14, 2015 05:21:49 PM
//Codeset: 1252
requires maya "2012";
requires "Mayatomr" "2012.0m - 3.9.1.48 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2012";
fileInfo "version" "2012 x64";
fileInfo "cutIdentifier" "201201172029-821146";
fileInfo "osv" "Microsoft Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.8686051410400053 11.507181319675894 5.0872091155372345 ;
	setAttr ".r" -type "double3" -4.538352729195557 10.600000000000822 0 ;
	setAttr ".rp" -type "double3" 0 -3.5527136788005009e-015 1.7763568394002505e-015 ;
	setAttr ".rpt" -type "double3" -1.7669057640736263e-016 -9.3671696363680272e-017 
		-3.9809070816125032e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 4.7348854506819595;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.00034747251359246167 11.132526898384096 0.44771529436111379 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.5315355626884917 100.45844266046294 0.78924046296869088 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".rp" -type "double3" -5.5511151231257827e-017 0 -1.4210854715202004e-014 ;
	setAttr ".rpt" -type "double3" 0 -1.4210854715202007e-014 1.4210854715202007e-014 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 88.932489665955117;
	setAttr ".ow" 0.20817605519162372;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" -0.31277558364673819 11.168030738830566 1.2386828573856794 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 31.297407763986005 24.309300333413209 197.71624905097198 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 5.5435604252730561;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 142.20406774686421 23.574200767941925 0.25775531159343001 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 2.4562386765110227;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "faceRig_grp";
	setAttr ".t" -type "double3" 0 10 3.5527136788005009e-015 ;
createNode transform -n "faceRigOffset_grp" -p "faceRig_grp";
createNode transform -n "mouthBshCtrl_grp" -p "faceRigOffset_grp";
createNode transform -n "lipBshCtrlZroLFT_grp" -p "mouthBshCtrl_grp";
createNode transform -n "lipBshLFT_ctrl" -p "lipBshCtrlZroLFT_grp";
	addAttr -ci true -k true -sn "crnrMouthIO" -ln "crnrMouthIO" -at "double";
	addAttr -ci true -k true -sn "crnrMouthUD" -ln "crnrMouthUD" -at "double";
	addAttr -ci true -k true -sn "upLipsUD" -ln "upLipsUD" -at "double";
	addAttr -ci true -k true -sn "loLipsUD" -ln "loLipsUD" -at "double";
	addAttr -ci true -k true -sn "lipsPartIO" -ln "lipsPartIO" -at "double";
	addAttr -ci true -k true -sn "cheekUprIO" -ln "cheekUprIO" -at "double";
	addAttr -ci true -k true -sn "cheekLwrIO" -ln "cheekLwrIO" -at "double";
	addAttr -ci true -k true -sn "puffIO" -ln "puffIO" -at "double";
	addAttr -ci true -k true -sn "cheekIO" -ln "cheekIO" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "lipBshLFT_ctrlShape" -p "lipBshLFT_ctrl";
	addAttr -ci true -sn "gimbleControl" -ln "gimbleControl" -min 0 -max 1 -at "double";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		0.53253700000000004 0.45097500000000001 1.32368
		0.58755999999999986 0.43572300000000003 1.32368
		0.64258300000000013 0.42047099999999998 1.32368
		0.68922900000000009 0.40754099999999999 1.32368
		0.70208300000000001 0.403978 1.32368
		0.71151900000000001 0.38801099999999999 1.32368
		0.70208300000000001 0.37204399999999999 1.32368
		0.68922900000000009 0.368481 1.32368
		0.64258300000000013 0.35555100000000001 1.32368
		0.58755999999999986 0.34029900000000002 1.32368
		0.53253700000000004 0.32504699999999992 1.32368
		0.44148100000000001 0.29980700000000005 1.32368
		0.42862599999999995 0.29624400000000001 1.32368
		0.47732599999999992 0.38801099999999999 1.32368
		0.42862599999999995 0.47977799999999998 1.32368
		0.44148100000000001 0.476215 1.32368
		0.53253700000000004 0.45097500000000001 1.32368
		;
	setAttr -k on ".gimbleControl";
createNode transform -n "lipBshGmblLFT_ctrl" -p "lipBshLFT_ctrl";
	addAttr -ci true -k true -sn "__lips__" -ln "__lips__" -at "double";
	addAttr -ci true -k true -sn "upLipsUD" -ln "upLipsUD" -at "double";
	addAttr -ci true -k true -sn "loLipsUD" -ln "loLipsUD" -at "double";
	addAttr -ci true -k true -sn "crnrMouthUD" -ln "crnrMouthUD" -at "double";
	addAttr -ci true -k true -sn "crnrMouthIO" -ln "crnrMouthIO" -at "double";
	addAttr -ci true -k true -sn "lipsPartIO" -ln "lipsPartIO" -at "double";
	addAttr -ci true -k true -sn "cheekUprIO" -ln "cheekUprIO" -at "double";
	addAttr -ci true -k true -sn "cheekLwrIO" -ln "cheekLwrIO" -at "double";
	addAttr -ci true -k true -sn "puffIO" -ln "puffIO" -at "double";
	addAttr -ci true -sn "cheekIO" -ln "cheekIO" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on -k on ".__lips__";
	setAttr -k on ".cheekIO";
createNode nurbsCurve -n "lipBshGmblLFT_ctrlShape" -p "lipBshGmblLFT_ctrl";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		0.54755100000000012 0.42578940000000026 1.3236799999999986
		0.58056479999999988 0.41663820000000024 1.3236799999999986
		0.61357860000000009 0.40748700000000021 1.3236799999999986
		0.64156620000000009 0.39972900000000022 1.3236799999999986
		0.64927860000000004 0.39759120000000026 1.3236799999999986
		0.65494019999999997 0.38801100000000022 1.3236799999999986
		0.64927860000000004 0.37843080000000023 1.3236799999999986
		0.64156620000000009 0.37629300000000021 1.3236799999999986
		0.61357860000000009 0.36853500000000028 1.3236799999999986
		0.58056479999999988 0.35938380000000025 1.3236799999999986
		0.54755100000000012 0.35023260000000023 1.3236799999999986
		0.49291740000000001 0.33508860000000029 1.3236799999999986
		0.48520439999999998 0.33295080000000021 1.3236799999999986
		0.5144244 0.38801100000000022 1.3236799999999986
		0.48520439999999998 0.44307120000000022 1.3236799999999986
		0.49291740000000001 0.44093340000000025 1.3236799999999986
		0.54755100000000012 0.42578940000000026 1.3236799999999986
		;
createNode transform -n "lipBshCtrlZro_grp" -p "mouthBshCtrl_grp";
createNode transform -n "lipBsh_ctrl" -p "lipBshCtrlZro_grp";
	addAttr -ci true -k true -sn "__face__" -ln "__face__" -at "double";
	addAttr -ci true -k true -sn "faceSquash" -ln "faceSquash" -at "double";
	addAttr -ci true -k true -sn "__nose__" -ln "__nose__" -at "double";
	addAttr -ci true -k true -sn "noseSquash" -ln "noseSquash" -at "double";
	addAttr -ci true -k true -sn "leftUp" -ln "leftUp" -at "double";
	addAttr -ci true -k true -sn "leftTurn" -ln "leftTurn" -at "double";
	addAttr -ci true -k true -sn "rightUp" -ln "rightUp" -at "double";
	addAttr -ci true -k true -sn "rightTurn" -ln "rightTurn" -at "double";
	addAttr -ci true -k true -sn "__mouth__" -ln "__mouth__" -at "double";
	addAttr -ci true -k true -sn "mouthLR" -ln "mouthLR" -at "double";
	addAttr -ci true -k true -sn "mouthUD" -ln "mouthUD" -at "double";
	addAttr -ci true -k true -sn "mouthTurn" -ln "mouthTurn" -at "double";
	addAttr -ci true -k true -sn "upLipsUD" -ln "upLipsUD" -at "double";
	addAttr -ci true -k true -sn "loLipsUD" -ln "loLipsUD" -at "double";
	addAttr -ci true -k true -sn "upLipsMidUD" -ln "upLipsMidUD" -at "double";
	addAttr -ci true -k true -sn "loLipsMidUD" -ln "loLipsMidUD" -at "double";
	addAttr -ci true -k true -sn "upLipsCurlIO" -ln "upLipsCurlIO" -at "double";
	addAttr -ci true -k true -sn "loLipsCurlIO" -ln "loLipsCurlIO" -at "double";
	addAttr -ci true -k true -sn "mouthClench" -ln "mouthClench" -at "double";
	addAttr -ci true -k true -sn "mouthPull" -ln "mouthPull" -at "double";
	addAttr -ci true -k true -sn "mouthU" -ln "mouthU" -at "double";
	setAttr -l on -k on ".__face__";
	setAttr -l on -k on ".__nose__";
	setAttr -l on -k on ".__mouth__";
createNode nurbsCurve -n "lipBsh_ctrlShape" -p "lipBsh_ctrl";
	addAttr -ci true -sn "gimbleControl" -ln "gimbleControl" -min 0 -max 1 -at "double";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		0 0.29624400000000001 1.32368
		0.18781400000000004 0.29624400000000001 1.32368
		0.34703400000000006 0.29624400000000001 1.32368
		0.39091100000000001 0.29624400000000001 1.32368
		0.42311900000000002 0.38801099999999999 1.32368
		0.39091100000000001 0.47977799999999998 1.32368
		0.34703400000000006 0.47977799999999998 1.32368
		0.18781400000000004 0.47977799999999998 1.32368
		0 0.47977799999999998 1.32368
		-0.18781400000000004 0.47977799999999998 1.32368
		-0.34703400000000006 0.47977799999999998 1.32368
		-0.39091100000000001 0.47977799999999998 1.32368
		-0.42311900000000002 0.38801099999999999 1.32368
		-0.39091100000000001 0.29624400000000001 1.32368
		-0.34703400000000006 0.29624400000000001 1.32368
		-0.18781400000000004 0.29624400000000001 1.32368
		0 0.29624400000000001 1.32368
		;
	setAttr -k on ".gimbleControl";
createNode transform -n "lipBshGmbl_ctrl" -p "lipBsh_ctrl";
	addAttr -ci true -k true -sn "__lips__" -ln "__lips__" -at "double";
	addAttr -ci true -k true -sn "mouthUD" -ln "mouthUD" -at "double";
	addAttr -ci true -k true -sn "mouthLR" -ln "mouthLR" -at "double";
	addAttr -ci true -k true -sn "mouthTurn" -ln "mouthTurn" -at "double";
	addAttr -ci true -k true -sn "upLipsUD" -ln "upLipsUD" -at "double";
	addAttr -ci true -k true -sn "loLipsUD" -ln "loLipsUD" -at "double";
	addAttr -ci true -k true -sn "upLipsCurlIO" -ln "upLipsCurlIO" -at "double";
	addAttr -ci true -k true -sn "loLipsCurlIO" -ln "loLipsCurlIO" -at "double";
	addAttr -ci true -k true -sn "mouthClench" -ln "mouthClench" -at "double";
	addAttr -ci true -k true -sn "mouthPull" -ln "mouthPull" -at "double";
	addAttr -ci true -k true -sn "mouthU" -ln "mouthU" -at "double";
	addAttr -ci true -sn "thickness" -ln "thickness" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on -k on ".__lips__";
	setAttr -k on ".thickness";
createNode nurbsCurve -n "lipBshGmbl_ctrlShape" -p "lipBshGmbl_ctrl";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		0 0.3240528876794736 1.32368
		0.15586786002263445 0.3240528876794736 1.32368
		0.28800540393737895 0.3240528876794736 1.32368
		0.3244191648615547 0.3240528876794736 1.32368
		0.35114875922411021 0.38801099999999661 1.32368
		0.3244191648615547 0.45196911232051912 1.32368
		0.28800540393737895 0.45196911232051912 1.32368
		0.15586786002263445 0.45196911232051912 1.32368
		0 0.45196911232051912 1.32368
		-0.15586786002263445 0.45196911232051912 1.32368
		-0.28800540393737895 0.45196911232051912 1.32368
		-0.3244191648615547 0.45196911232051912 1.32368
		-0.35114875922411021 0.38801099999999661 1.32368
		-0.3244191648615547 0.3240528876794736 1.32368
		-0.28800540393737895 0.3240528876794736 1.32368
		-0.15586786002263445 0.3240528876794736 1.32368
		0 0.3240528876794736 1.32368
		;
createNode transform -n "lipBshCtrlZroRGT_grp" -p "mouthBshCtrl_grp";
createNode transform -n "lipBshRGT_ctrl" -p "lipBshCtrlZroRGT_grp";
	addAttr -ci true -k true -sn "crnrMouthIO" -ln "crnrMouthIO" -at "double";
	addAttr -ci true -k true -sn "crnrMouthUD" -ln "crnrMouthUD" -at "double";
	addAttr -ci true -k true -sn "upLipsUD" -ln "upLipsUD" -at "double";
	addAttr -ci true -k true -sn "loLipsUD" -ln "loLipsUD" -at "double";
	addAttr -ci true -k true -sn "lipsPartIO" -ln "lipsPartIO" -at "double";
	addAttr -ci true -k true -sn "cheekUprIO" -ln "cheekUprIO" -at "double";
	addAttr -ci true -k true -sn "cheekLwrIO" -ln "cheekLwrIO" -at "double";
	addAttr -ci true -k true -sn "puffIO" -ln "puffIO" -at "double";
	addAttr -ci true -k true -sn "cheekIO" -ln "cheekIO" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "lipBshRGT_ctrlShape" -p "lipBshRGT_ctrl";
	addAttr -ci true -sn "gimbleControl" -ln "gimbleControl" -min 0 -max 1 -at "double";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		-0.58755999999999986 0.43572300000000003 1.32368
		-0.53253700000000004 0.45097500000000001 1.32368
		-0.44148100000000001 0.476215 1.32368
		-0.42862599999999995 0.47977799999999998 1.32368
		-0.47732599999999992 0.38801099999999999 1.32368
		-0.42862599999999995 0.29624400000000001 1.32368
		-0.44148100000000001 0.29980700000000005 1.32368
		-0.53253700000000004 0.32504699999999992 1.32368
		-0.58755999999999986 0.34029900000000002 1.32368
		-0.64258300000000013 0.35555100000000001 1.32368
		-0.68922900000000009 0.368481 1.32368
		-0.70208300000000001 0.37204399999999999 1.32368
		-0.71151900000000001 0.38801099999999999 1.32368
		-0.70208300000000001 0.403978 1.32368
		-0.68922900000000009 0.40754099999999999 1.32368
		-0.64258300000000013 0.42047099999999998 1.32368
		-0.58755999999999986 0.43572300000000003 1.32368
		;
	setAttr -k on ".gimbleControl";
createNode transform -n "lipBshGmblRGT_ctrl" -p "lipBshRGT_ctrl";
	addAttr -ci true -k true -sn "__lips__" -ln "__lips__" -at "double";
	addAttr -ci true -k true -sn "upLipsUD" -ln "upLipsUD" -at "double";
	addAttr -ci true -k true -sn "loLipsUD" -ln "loLipsUD" -at "double";
	addAttr -ci true -k true -sn "crnrMouthUD" -ln "crnrMouthUD" -at "double";
	addAttr -ci true -k true -sn "crnrMouthIO" -ln "crnrMouthIO" -at "double";
	addAttr -ci true -k true -sn "lipsPartIO" -ln "lipsPartIO" -at "double";
	addAttr -ci true -k true -sn "cheekUprIO" -ln "cheekUprIO" -at "double";
	addAttr -ci true -k true -sn "cheekLwrIO" -ln "cheekLwrIO" -at "double";
	addAttr -ci true -k true -sn "puffIO" -ln "puffIO" -at "double";
	addAttr -ci true -sn "cheekIO" -ln "cheekIO" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on -k on ".__lips__";
	setAttr -k on ".cheekIO";
createNode nurbsCurve -n "lipBshGmblRGT_ctrlShape" -p "lipBshGmblRGT_ctrl";
	setAttr -k off ".v" no;
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		-0.58056479999999988 0.41663820000000024 1.3236799999999986
		-0.54755100000000012 0.42578940000000026 1.3236799999999986
		-0.49291740000000001 0.44093340000000025 1.3236799999999986
		-0.48520439999999998 0.44307120000000022 1.3236799999999986
		-0.5144244 0.38801100000000022 1.3236799999999986
		-0.48520439999999998 0.33295080000000021 1.3236799999999986
		-0.49291740000000001 0.33508860000000029 1.3236799999999986
		-0.54755100000000012 0.35023260000000023 1.3236799999999986
		-0.58056479999999988 0.35938380000000025 1.3236799999999986
		-0.61357860000000009 0.36853500000000028 1.3236799999999986
		-0.64156620000000009 0.37629300000000021 1.3236799999999986
		-0.64927860000000004 0.37843080000000023 1.3236799999999986
		-0.65494019999999997 0.38801100000000022 1.3236799999999986
		-0.64927860000000004 0.39759120000000026 1.3236799999999986
		-0.64156620000000009 0.39972900000000022 1.3236799999999986
		-0.61357860000000009 0.40748700000000021 1.3236799999999986
		-0.58056479999999988 0.41663820000000024 1.3236799999999986
		;
createNode transform -n "lidBshProxyLocZroLFT_grp" -p "faceRigOffset_grp";
	setAttr ".t" -type "double3" 0.3833629100271822 1.1325268983840964 0.44771529436110979 ;
createNode transform -n "lidBshProxyLFT_loc" -p "lidBshProxyLocZroLFT_grp";
	addAttr -ci true -k true -sn "upLidUD" -ln "upLidUD" -at "double";
	addAttr -ci true -k true -sn "loLidUD" -ln "loLidUD" -at "double";
	addAttr -ci true -k true -sn "upLidTW" -ln "upLidTW" -at "double";
	addAttr -ci true -k true -sn "loLidTW" -ln "loLidTW" -at "double";
createNode locator -n "lidBshProxyLFT_locShape" -p "lidBshProxyLFT_loc";
	setAttr -k off ".v";
createNode transform -n "lidBshProxyLocZroRGT_grp" -p "faceRigOffset_grp";
	setAttr ".t" -type "double3" -0.38266796499999722 1.1325268983840964 0.44771529436111068 ;
createNode transform -n "lidBshProxyRGT_loc" -p "lidBshProxyLocZroRGT_grp";
	addAttr -ci true -k true -sn "upLidUD" -ln "upLidUD" -at "double";
	addAttr -ci true -k true -sn "loLidUD" -ln "loLidUD" -at "double";
	addAttr -ci true -k true -sn "upLidTW" -ln "upLidTW" -at "double";
	addAttr -ci true -k true -sn "loLidTW" -ln "loLidTW" -at "double";
createNode locator -n "lidBshProxyRGT_locShape" -p "lidBshProxyRGT_loc";
	setAttr -k off ".v";
createNode transform -n "ebBshCtrlZroRGT_grp" -p "faceRigOffset_grp";
	setAttr ".t" -type "double3" -0.45232346499999987 1.5801739999999995 1.2748004610756958 ;
createNode transform -n "ebBshRGT_ctrl" -p "ebBshCtrlZroRGT_grp";
	addAttr -ci true -k true -sn "ebIO" -ln "ebIO" -at "double";
	addAttr -ci true -k true -sn "ebUD" -ln "ebUD" -at "double";
	addAttr -ci true -k true -sn "ebTurn" -ln "ebTurn" -at "double";
	addAttr -ci true -k true -sn "ebInnerIO" -ln "ebInnerIO" -at "double";
	addAttr -ci true -k true -sn "ebInnerUD" -ln "ebInnerUD" -at "double";
	addAttr -ci true -k true -sn "ebInnerTurn" -ln "ebInnerTurn" -at "double";
	addAttr -ci true -k true -sn "ebMidUD" -ln "ebMidUD" -at "double";
	addAttr -ci true -k true -sn "ebOuterUD" -ln "ebOuterUD" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on ".t";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -l on ".r";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "ebBshRGT_ctrlShape" -p "ebBshRGT_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		0.32090050000000003 0.035758000000000401 0
		0.17366949999999992 0.12227500000000058 0
		-4.9999999984784438e-007 0.16899700000000051 0
		-0.17367049999999984 0.12227500000000058 0
		-0.32090049999999981 0.035758000000000401 0
		-0.3614735 0.00071600000000060504 0
		-0.39125549999999998 -0.08413999999999966 0
		-0.3614735 -0.16899699999999962 0
		-0.32090049999999981 -0.13395499999999938 0
		-0.17367049999999984 -0.047437999999999647 0
		-4.9999999984784438e-007 4.4408920985006262e-016 0
		0.17366949999999992 -0.047437999999999647 0
		0.32090050000000003 -0.13395499999999938 0
		0.36147250000000009 -0.16899699999999962 0
		0.3912555000000002 -0.08413999999999966 0
		0.36147250000000009 0.00071600000000060504 0
		0.32090050000000003 0.035758000000000401 0
		;
createNode transform -n "ebBshCtrlZroLFT_grp" -p "faceRigOffset_grp";
	setAttr ".t" -type "double3" 0.45298753500000011 1.5801739999999995 1.2748004610756958 ;
createNode transform -n "ebBshLFT_ctrl" -p "ebBshCtrlZroLFT_grp";
	addAttr -ci true -k true -sn "ebIO" -ln "ebIO" -at "double";
	addAttr -ci true -k true -sn "ebUD" -ln "ebUD" -at "double";
	addAttr -ci true -k true -sn "ebTurn" -ln "ebTurn" -at "double";
	addAttr -ci true -k true -sn "ebInnerIO" -ln "ebInnerIO" -at "double";
	addAttr -ci true -k true -sn "ebInnerUD" -ln "ebInnerUD" -at "double";
	addAttr -ci true -k true -sn "ebInnerTurn" -ln "ebInnerTurn" -at "double";
	addAttr -ci true -k true -sn "ebMidUD" -ln "ebMidUD" -at "double";
	addAttr -ci true -k true -sn "ebOuterUD" -ln "ebOuterUD" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on ".t";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -l on ".r";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "ebBshLFT_ctrlShape" -p "ebBshLFT_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		-0.32090050000000003 0.035758000000000401 0
		-0.17366950000000014 0.12227500000000058 0
		4.9999999984784438e-007 0.16899700000000051 0
		0.17367049999999984 0.12227500000000058 0
		0.32090050000000003 0.035758000000000401 0
		0.36147350000000023 0.00071600000000060504 0
		0.3912555000000002 -0.08413999999999966 0
		0.36147350000000023 -0.16899699999999962 0
		0.32090050000000003 -0.13395499999999938 0
		0.17367049999999984 -0.047437999999999647 0
		4.9999999984784438e-007 4.4408920985006262e-016 0
		-0.17366950000000014 -0.047437999999999647 0
		-0.32090050000000003 -0.13395499999999938 0
		-0.36147250000000009 -0.16899699999999962 0
		-0.3912555000000002 -0.08413999999999966 0
		-0.36147250000000009 0.00071600000000060504 0
		-0.32090050000000003 0.035758000000000401 0
		;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	setAttr ".cdl" 16;
	setAttr -s 17 ".dli[1:16]"  5 10 1 9 7 8 11 12 
		3 4 2 6 13 14 15 16;
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
	setAttr -s 16 ".rlmi[1:15]"  5 4 3 2 1 6 7 8 
		9 10 16 11 12 13 14;
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
	setAttr ".rndr" no;
createNode hyperGraphInfo -n "nodeEditorPanel2Info";
createNode hyperView -n "hyperView1";
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout1";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info";
createNode hyperView -n "weapon_hyperView1";
	setAttr ".dag" no;
createNode hyperLayout -n "weapon_hyperLayout1";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "weapon_groupId37";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info1";
createNode hyperView -n "weapon_hyperView2";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel3Info";
createNode hyperView -n "weapon_hyperView3";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel1Info";
createNode hyperView -n "weapon_hyperView4";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel2Info1";
createNode hyperView -n "hyperView2";
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout2";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "groupId37";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "nodeEditorPanel2Info2";
createNode hyperView -n "hyperView3";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel3Info";
createNode hyperView -n "hyperView4";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel1Info";
createNode hyperView -n "hyperView5";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel2Info3";
createNode hyperView -n "hyperView6";
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout3";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info2";
createNode hyperView -n "weapon_hyperView5";
	setAttr ".dag" no;
createNode hyperLayout -n "weapon_hyperLayout2";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "weapon_groupId38";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info3";
createNode hyperView -n "weapon_hyperView6";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel3Info1";
createNode hyperView -n "weapon_hyperView7";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel1Info1";
createNode hyperView -n "weapon_hyperView8";
	setAttr ".dag" no;
createNode groupId -n "groupId122";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "nodeEditorPanel2Info4";
createNode hyperView -n "hyperView7";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel3Info1";
createNode hyperView -n "hyperView8";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel1Info1";
createNode hyperView -n "hyperView9";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel2Info12";
createNode hyperView -n "hyperView24";
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout9";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info10";
createNode hyperView -n "weapon_hyperView21";
	setAttr ".dag" no;
createNode hyperLayout -n "weapon_hyperLayout6";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "weapon_groupId42";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info11";
createNode hyperView -n "weapon_hyperView22";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel3Info5";
createNode hyperView -n "weapon_hyperView23";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel1Info5";
createNode hyperView -n "weapon_hyperView24";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel2Info13";
createNode hyperView -n "hyperView25";
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout10";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "groupId541";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "nodeEditorPanel2Info14";
createNode hyperView -n "hyperView26";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel3Info5";
createNode hyperView -n "hyperView27";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel1Info6";
createNode hyperView -n "hyperView28";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel2Info15";
createNode hyperView -n "hyperView29";
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout11";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info12";
createNode hyperView -n "weapon_hyperView25";
	setAttr ".dag" no;
createNode hyperLayout -n "weapon_hyperLayout7";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "weapon_groupId43";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info13";
createNode hyperView -n "weapon_hyperView26";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel3Info6";
createNode hyperView -n "weapon_hyperView27";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel1Info6";
createNode hyperView -n "weapon_hyperView28";
	setAttr ".dag" no;
createNode groupId -n "groupId123";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "nodeEditorPanel2Info16";
createNode hyperView -n "hyperView30";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel3Info6";
createNode hyperView -n "hyperView31";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel1Info7";
createNode hyperView -n "hyperView32";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel2Info17";
createNode hyperView -n "hyperView33";
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout12";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info14";
createNode hyperView -n "weapon_hyperView29";
	setAttr ".dag" no;
createNode hyperLayout -n "weapon_hyperLayout8";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "weapon_groupId44";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info15";
createNode hyperView -n "weapon_hyperView30";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel3Info7";
createNode hyperView -n "weapon_hyperView31";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel1Info7";
createNode hyperView -n "weapon_hyperView32";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel2Info18";
createNode hyperView -n "hyperView34";
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout13";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "groupId544";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "nodeEditorPanel2Info19";
createNode hyperView -n "hyperView35";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel3Info7";
createNode hyperView -n "hyperView36";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel1Info8";
createNode hyperView -n "hyperView37";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel2Info20";
createNode hyperView -n "hyperView38";
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout14";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info16";
createNode hyperView -n "weapon_hyperView33";
	setAttr ".dag" no;
createNode hyperLayout -n "weapon_hyperLayout9";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "weapon_groupId45";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info17";
createNode hyperView -n "weapon_hyperView34";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel3Info8";
createNode hyperView -n "weapon_hyperView35";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel1Info8";
createNode hyperView -n "weapon_hyperView36";
	setAttr ".dag" no;
createNode groupId -n "groupId545";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "nodeEditorPanel2Info21";
createNode hyperView -n "hyperView39";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel3Info8";
createNode hyperView -n "hyperView40";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel1Info9";
createNode hyperView -n "hyperView41";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel2Info";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView1";
	setAttr ".dag" no;
createNode hyperLayout -n "tvc_leatherhead_79121_uv_hyperLayout1";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView1";
	setAttr ".dag" no;
createNode hyperLayout -n "tvc_leatherhead_79121_uv_weapon_hyperLayout1";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "tvc_leatherhead_79121_uv_weapon_groupId37";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info1";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView2";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel3Info";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView3";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel1Info";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView4";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel2Info1";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView2";
	setAttr ".dag" no;
createNode hyperLayout -n "tvc_leatherhead_79121_uv_hyperLayout2";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "tvc_leatherhead_79121_uv_groupId37";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel2Info2";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView3";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel3Info";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView4";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel1Info";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView5";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel2Info3";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView6";
	setAttr ".dag" no;
createNode hyperLayout -n "tvc_leatherhead_79121_uv_hyperLayout3";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info2";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView5";
	setAttr ".dag" no;
createNode hyperLayout -n "tvc_leatherhead_79121_uv_weapon_hyperLayout2";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "tvc_leatherhead_79121_uv_weapon_groupId38";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info3";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView6";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel3Info1";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView7";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel1Info1";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView8";
	setAttr ".dag" no;
createNode groupId -n "tvc_leatherhead_79121_uv_groupId38";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel2Info4";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView7";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel3Info1";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView8";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel1Info1";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView9";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel2Info5";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView10";
	setAttr ".dag" no;
createNode hyperLayout -n "tvc_leatherhead_79121_uv_hyperLayout4";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info4";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView9";
	setAttr ".dag" no;
createNode hyperLayout -n "tvc_leatherhead_79121_uv_weapon_hyperLayout3";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "tvc_leatherhead_79121_uv_weapon_groupId39";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info5";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView10";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel3Info2";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView11";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel1Info2";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView12";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel2Info6";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView11";
	setAttr ".dag" no;
createNode hyperLayout -n "tvc_leatherhead_79121_uv_hyperLayout5";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "tvc_leatherhead_79121_uv_groupId168";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel2Info7";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView12";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel3Info2";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView13";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel1Info2";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView14";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel2Info8";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView15";
	setAttr ".dag" no;
createNode hyperLayout -n "tvc_leatherhead_79121_uv_hyperLayout6";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info6";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView13";
	setAttr ".dag" no;
createNode hyperLayout -n "tvc_leatherhead_79121_uv_weapon_hyperLayout4";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "tvc_leatherhead_79121_uv_weapon_groupId40";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info7";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView14";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel3Info3";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView15";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel1Info3";
createNode hyperView -n "tvc_leatherhead_79121_uv_weapon_hyperView16";
	setAttr ".dag" no;
createNode groupId -n "tvc_leatherhead_79121_uv_groupId169";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel2Info9";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView16";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel3Info3";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView17";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "tvc_leatherhead_79121_uv_nodeEditorPanel1Info3";
createNode hyperView -n "tvc_leatherhead_79121_uv_hyperView18";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel2Info5";
createNode hyperView -n "hyperView10";
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout4";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info4";
createNode hyperView -n "weapon_hyperView9";
	setAttr ".dag" no;
createNode hyperLayout -n "weapon_hyperLayout3";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "weapon_groupId39";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info5";
createNode hyperView -n "weapon_hyperView10";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel3Info2";
createNode hyperView -n "weapon_hyperView11";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel1Info2";
createNode hyperView -n "weapon_hyperView12";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel2Info6";
createNode hyperView -n "hyperView11";
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout5";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "groupId534";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "nodeEditorPanel2Info7";
createNode hyperView -n "hyperView12";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel3Info2";
createNode hyperView -n "hyperView13";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel1Info2";
createNode hyperView -n "hyperView14";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel2Info8";
createNode hyperView -n "hyperView15";
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout6";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info6";
createNode hyperView -n "weapon_hyperView13";
	setAttr ".dag" no;
createNode hyperLayout -n "weapon_hyperLayout4";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "weapon_groupId40";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info7";
createNode hyperView -n "weapon_hyperView14";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel3Info3";
createNode hyperView -n "weapon_hyperView15";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel1Info3";
createNode hyperView -n "weapon_hyperView16";
	setAttr ".dag" no;
createNode groupId -n "groupId535";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "nodeEditorPanel2Info9";
createNode hyperView -n "hyperView16";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel3Info3";
createNode hyperView -n "hyperView17";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel1Info3";
createNode hyperView -n "hyperView18";
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel1Info4";
createNode hyperView -n "hyperView19";
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout7";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "nodeEditorPanel2Info10";
createNode hyperView -n "hyperView20";
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout8";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info8";
createNode hyperView -n "weapon_hyperView17";
	setAttr ".dag" no;
createNode hyperLayout -n "weapon_hyperLayout5";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
createNode groupId -n "weapon_groupId41";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel2Info9";
createNode hyperView -n "weapon_hyperView18";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel3Info4";
createNode hyperView -n "weapon_hyperView19";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "weapon_nodeEditorPanel1Info4";
createNode hyperView -n "weapon_hyperView20";
	setAttr ".dag" no;
createNode groupId -n "groupId538";
	setAttr ".ihi" 0;
createNode hyperGraphInfo -n "nodeEditorPanel2Info11";
createNode hyperView -n "hyperView21";
	setAttr ".vl" -type "double2" -1103.2942358246123 -478.94812494816654 ;
	setAttr ".vh" -type "double2" 59.673688234673662 180.23970728588304 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel3Info4";
createNode hyperView -n "hyperView22";
	setAttr ".vl" -type "double2" -2404.2228339306812 -1065.543659388474 ;
	setAttr ".vh" -type "double2" 955.06521422495166 494.3202397956245 ;
	setAttr ".dag" no;
createNode hyperGraphInfo -n "nodeEditorPanel1Info5";
createNode hyperView -n "hyperView23";
	setAttr ".dag" no;
createNode groupId -n "groupId928";
	setAttr ".ihi" 0;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".minsp" 1;
	setAttr ".maxsp" 3;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "256";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	setAttr ".dat" 16;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 100 -ast 1 -aet 100 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
lockNode -l 1 ;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
lockNode -l 1 ;
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -s 26 ".gn";
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
lockNode -l 1 ;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
lockNode -l 1 ;
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
lockNode -l 1 ;
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
lockNode -l 1 ;
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
lockNode -l 1 ;
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren" -type "string" "vray";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -k on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs";
	setAttr -k on ".ef";
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -k on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".rv";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mot";
	setAttr -av -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
lockNode -l 1 ;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w" 1920;
	setAttr -av -k on ".h" 1080;
	setAttr -av -k on ".pa" 0.99956250190734863;
	setAttr -av -k on ".al" yes;
	setAttr -av -k on ".dar" 1.7769999504089355;
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
lockNode -l 1 ;
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off ".ctrs" 256;
	setAttr -av -k off ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
lockNode -l 1 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".rp";
	setAttr -k on ".cai";
	setAttr -k on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -k on ".ei";
	setAttr -av -k on ".ex";
	setAttr -av -k on ".es";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bf";
	setAttr -k on ".fii";
	setAttr -av -k on ".sf";
	setAttr -k on ".gr";
	setAttr -k on ".li";
	setAttr -k on ".ls";
	setAttr -av -k on ".mb";
	setAttr -k on ".ti";
	setAttr -k on ".txt";
	setAttr -k on ".mpr";
	setAttr -k on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -k on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -k on ".as";
	setAttr -k on ".ds";
	setAttr -k on ".lm";
	setAttr -av -k on ".fir";
	setAttr -k on ".aap";
	setAttr -av -k on ".gh";
	setAttr -cb on ".sd";
lockNode -l 1 ;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "hyperView1.msg" "nodeEditorPanel2Info.b[0]";
connectAttr "hyperLayout1.msg" "hyperView1.hl";
connectAttr "weapon_hyperView1.msg" "weapon_nodeEditorPanel2Info.b[0]";
connectAttr "weapon_hyperLayout1.msg" "weapon_hyperView1.hl";
connectAttr "weapon_hyperView2.msg" "weapon_nodeEditorPanel2Info1.b[0]";
connectAttr "weapon_hyperView3.msg" "weapon_nodeEditorPanel3Info.b[0]";
connectAttr "weapon_hyperView4.msg" "weapon_nodeEditorPanel1Info.b[0]";
connectAttr "hyperView2.msg" "nodeEditorPanel2Info1.b[0]";
connectAttr "hyperLayout2.msg" "hyperView2.hl";
connectAttr "hyperView3.msg" "nodeEditorPanel2Info2.b[0]";
connectAttr "hyperView4.msg" "nodeEditorPanel3Info.b[0]";
connectAttr "hyperView5.msg" "nodeEditorPanel1Info.b[0]";
connectAttr "hyperView6.msg" "nodeEditorPanel2Info3.b[0]";
connectAttr "hyperLayout3.msg" "hyperView6.hl";
connectAttr "weapon_hyperView5.msg" "weapon_nodeEditorPanel2Info2.b[0]";
connectAttr "weapon_hyperLayout2.msg" "weapon_hyperView5.hl";
connectAttr "weapon_hyperView6.msg" "weapon_nodeEditorPanel2Info3.b[0]";
connectAttr "weapon_hyperView7.msg" "weapon_nodeEditorPanel3Info1.b[0]";
connectAttr "weapon_hyperView8.msg" "weapon_nodeEditorPanel1Info1.b[0]";
connectAttr "hyperView7.msg" "nodeEditorPanel2Info4.b[0]";
connectAttr "hyperView8.msg" "nodeEditorPanel3Info1.b[0]";
connectAttr "hyperView9.msg" "nodeEditorPanel1Info1.b[0]";
connectAttr "hyperView24.msg" "nodeEditorPanel2Info12.b[0]";
connectAttr "hyperLayout9.msg" "hyperView24.hl";
connectAttr "weapon_hyperView21.msg" "weapon_nodeEditorPanel2Info10.b[0]";
connectAttr "weapon_hyperLayout6.msg" "weapon_hyperView21.hl";
connectAttr "weapon_hyperView22.msg" "weapon_nodeEditorPanel2Info11.b[0]";
connectAttr "weapon_hyperView23.msg" "weapon_nodeEditorPanel3Info5.b[0]";
connectAttr "weapon_hyperView24.msg" "weapon_nodeEditorPanel1Info5.b[0]";
connectAttr "hyperView25.msg" "nodeEditorPanel2Info13.b[0]";
connectAttr "hyperLayout10.msg" "hyperView25.hl";
connectAttr "hyperView26.msg" "nodeEditorPanel2Info14.b[0]";
connectAttr "hyperView27.msg" "nodeEditorPanel3Info5.b[0]";
connectAttr "hyperView28.msg" "nodeEditorPanel1Info6.b[0]";
connectAttr "hyperView29.msg" "nodeEditorPanel2Info15.b[0]";
connectAttr "hyperLayout11.msg" "hyperView29.hl";
connectAttr "weapon_hyperView25.msg" "weapon_nodeEditorPanel2Info12.b[0]";
connectAttr "weapon_hyperLayout7.msg" "weapon_hyperView25.hl";
connectAttr "weapon_hyperView26.msg" "weapon_nodeEditorPanel2Info13.b[0]";
connectAttr "weapon_hyperView27.msg" "weapon_nodeEditorPanel3Info6.b[0]";
connectAttr "weapon_hyperView28.msg" "weapon_nodeEditorPanel1Info6.b[0]";
connectAttr "hyperView30.msg" "nodeEditorPanel2Info16.b[0]";
connectAttr "hyperView31.msg" "nodeEditorPanel3Info6.b[0]";
connectAttr "hyperView32.msg" "nodeEditorPanel1Info7.b[0]";
connectAttr "hyperView33.msg" "nodeEditorPanel2Info17.b[0]";
connectAttr "hyperLayout12.msg" "hyperView33.hl";
connectAttr "weapon_hyperView29.msg" "weapon_nodeEditorPanel2Info14.b[0]";
connectAttr "weapon_hyperLayout8.msg" "weapon_hyperView29.hl";
connectAttr "weapon_hyperView30.msg" "weapon_nodeEditorPanel2Info15.b[0]";
connectAttr "weapon_hyperView31.msg" "weapon_nodeEditorPanel3Info7.b[0]";
connectAttr "weapon_hyperView32.msg" "weapon_nodeEditorPanel1Info7.b[0]";
connectAttr "hyperView34.msg" "nodeEditorPanel2Info18.b[0]";
connectAttr "hyperLayout13.msg" "hyperView34.hl";
connectAttr "hyperView35.msg" "nodeEditorPanel2Info19.b[0]";
connectAttr "hyperView36.msg" "nodeEditorPanel3Info7.b[0]";
connectAttr "hyperView37.msg" "nodeEditorPanel1Info8.b[0]";
connectAttr "hyperView38.msg" "nodeEditorPanel2Info20.b[0]";
connectAttr "hyperLayout14.msg" "hyperView38.hl";
connectAttr "weapon_hyperView33.msg" "weapon_nodeEditorPanel2Info16.b[0]";
connectAttr "weapon_hyperLayout9.msg" "weapon_hyperView33.hl";
connectAttr "weapon_hyperView34.msg" "weapon_nodeEditorPanel2Info17.b[0]";
connectAttr "weapon_hyperView35.msg" "weapon_nodeEditorPanel3Info8.b[0]";
connectAttr "weapon_hyperView36.msg" "weapon_nodeEditorPanel1Info8.b[0]";
connectAttr "hyperView39.msg" "nodeEditorPanel2Info21.b[0]";
connectAttr "hyperView40.msg" "nodeEditorPanel3Info8.b[0]";
connectAttr "hyperView41.msg" "nodeEditorPanel1Info9.b[0]";
connectAttr "tvc_leatherhead_79121_uv_hyperView1.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel2Info.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperLayout1.msg" "tvc_leatherhead_79121_uv_hyperView1.hl"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView1.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperLayout1.msg" "tvc_leatherhead_79121_uv_weapon_hyperView1.hl"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView2.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info1.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView3.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel3Info.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView4.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel1Info.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView2.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel2Info1.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperLayout2.msg" "tvc_leatherhead_79121_uv_hyperView2.hl"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView3.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel2Info2.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView4.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel3Info.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView5.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel1Info.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView6.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel2Info3.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperLayout3.msg" "tvc_leatherhead_79121_uv_hyperView6.hl"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView5.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info2.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperLayout2.msg" "tvc_leatherhead_79121_uv_weapon_hyperView5.hl"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView6.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info3.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView7.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel3Info1.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView8.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel1Info1.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView7.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel2Info4.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView8.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel3Info1.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView9.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel1Info1.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView10.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel2Info5.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperLayout4.msg" "tvc_leatherhead_79121_uv_hyperView10.hl"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView9.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info4.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperLayout3.msg" "tvc_leatherhead_79121_uv_weapon_hyperView9.hl"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView10.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info5.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView11.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel3Info2.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView12.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel1Info2.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView11.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel2Info6.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperLayout5.msg" "tvc_leatherhead_79121_uv_hyperView11.hl"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView12.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel2Info7.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView13.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel3Info2.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView14.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel1Info2.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView15.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel2Info8.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperLayout6.msg" "tvc_leatherhead_79121_uv_hyperView15.hl"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView13.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info6.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperLayout4.msg" "tvc_leatherhead_79121_uv_weapon_hyperView13.hl"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView14.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel2Info7.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView15.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel3Info3.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_hyperView16.msg" "tvc_leatherhead_79121_uv_weapon_nodeEditorPanel1Info3.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView16.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel2Info9.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView17.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel3Info3.b[0]"
		;
connectAttr "tvc_leatherhead_79121_uv_hyperView18.msg" "tvc_leatherhead_79121_uv_nodeEditorPanel1Info3.b[0]"
		;
connectAttr "hyperView10.msg" "nodeEditorPanel2Info5.b[0]";
connectAttr "hyperLayout4.msg" "hyperView10.hl";
connectAttr "weapon_hyperView9.msg" "weapon_nodeEditorPanel2Info4.b[0]";
connectAttr "weapon_hyperLayout3.msg" "weapon_hyperView9.hl";
connectAttr "weapon_hyperView10.msg" "weapon_nodeEditorPanel2Info5.b[0]";
connectAttr "weapon_hyperView11.msg" "weapon_nodeEditorPanel3Info2.b[0]";
connectAttr "weapon_hyperView12.msg" "weapon_nodeEditorPanel1Info2.b[0]";
connectAttr "hyperView11.msg" "nodeEditorPanel2Info6.b[0]";
connectAttr "hyperLayout5.msg" "hyperView11.hl";
connectAttr "hyperView12.msg" "nodeEditorPanel2Info7.b[0]";
connectAttr "hyperView13.msg" "nodeEditorPanel3Info2.b[0]";
connectAttr "hyperView14.msg" "nodeEditorPanel1Info2.b[0]";
connectAttr "hyperView15.msg" "nodeEditorPanel2Info8.b[0]";
connectAttr "hyperLayout6.msg" "hyperView15.hl";
connectAttr "weapon_hyperView13.msg" "weapon_nodeEditorPanel2Info6.b[0]";
connectAttr "weapon_hyperLayout4.msg" "weapon_hyperView13.hl";
connectAttr "weapon_hyperView14.msg" "weapon_nodeEditorPanel2Info7.b[0]";
connectAttr "weapon_hyperView15.msg" "weapon_nodeEditorPanel3Info3.b[0]";
connectAttr "weapon_hyperView16.msg" "weapon_nodeEditorPanel1Info3.b[0]";
connectAttr "hyperView16.msg" "nodeEditorPanel2Info9.b[0]";
connectAttr "hyperView17.msg" "nodeEditorPanel3Info3.b[0]";
connectAttr "hyperView18.msg" "nodeEditorPanel1Info3.b[0]";
connectAttr "hyperView19.msg" "nodeEditorPanel1Info4.b[0]";
connectAttr "hyperLayout7.msg" "hyperView19.hl";
connectAttr "hyperView20.msg" "nodeEditorPanel2Info10.b[0]";
connectAttr "hyperLayout8.msg" "hyperView20.hl";
connectAttr "weapon_hyperView17.msg" "weapon_nodeEditorPanel2Info8.b[0]";
connectAttr "weapon_hyperLayout5.msg" "weapon_hyperView17.hl";
connectAttr "weapon_hyperView18.msg" "weapon_nodeEditorPanel2Info9.b[0]";
connectAttr "weapon_hyperView19.msg" "weapon_nodeEditorPanel3Info4.b[0]";
connectAttr "weapon_hyperView20.msg" "weapon_nodeEditorPanel1Info4.b[0]";
connectAttr "hyperView21.msg" "nodeEditorPanel2Info11.b[0]";
connectAttr "hyperView22.msg" "nodeEditorPanel3Info4.b[0]";
connectAttr "hyperView23.msg" "nodeEditorPanel1Info5.b[0]";
connectAttr "weapon_groupId37.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId37.msg" ":initialShadingGroup.gn" -na;
connectAttr "weapon_groupId38.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId122.msg" ":initialShadingGroup.gn" -na;
connectAttr "weapon_groupId42.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId541.msg" ":initialShadingGroup.gn" -na;
connectAttr "weapon_groupId43.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId123.msg" ":initialShadingGroup.gn" -na;
connectAttr "weapon_groupId44.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId544.msg" ":initialShadingGroup.gn" -na;
connectAttr "weapon_groupId45.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId545.msg" ":initialShadingGroup.gn" -na;
connectAttr "tvc_leatherhead_79121_uv_weapon_groupId37.msg" ":initialShadingGroup.gn"
		 -na;
connectAttr "tvc_leatherhead_79121_uv_groupId37.msg" ":initialShadingGroup.gn" -na
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_groupId38.msg" ":initialShadingGroup.gn"
		 -na;
connectAttr "tvc_leatherhead_79121_uv_groupId38.msg" ":initialShadingGroup.gn" -na
		;
connectAttr "tvc_leatherhead_79121_uv_weapon_groupId39.msg" ":initialShadingGroup.gn"
		 -na;
connectAttr "tvc_leatherhead_79121_uv_groupId168.msg" ":initialShadingGroup.gn" 
		-na;
connectAttr "tvc_leatherhead_79121_uv_weapon_groupId40.msg" ":initialShadingGroup.gn"
		 -na;
connectAttr "tvc_leatherhead_79121_uv_groupId169.msg" ":initialShadingGroup.gn" 
		-na;
connectAttr "weapon_groupId39.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId534.msg" ":initialShadingGroup.gn" -na;
connectAttr "weapon_groupId40.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId535.msg" ":initialShadingGroup.gn" -na;
connectAttr "weapon_groupId41.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId538.msg" ":initialShadingGroup.gn" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr ":perspShape.msg" ":defaultRenderGlobals.sc";
// End of bshCtrlFriends.ma
