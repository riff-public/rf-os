import os, sys, shutil, re
sys.path.append('P:/sysTool/python')


def removeRenderVersion(seq_path):
	seq_path = os.path.normpath(seq_path)
	if not os.path.exists(seq_path):
		print 'path do not exists: %s' %seq_path
		return

	dirs = os.listdir(seq_path)
	shots = []
	for fol in sorted([d for d in dirs if re.match('sh[0-9][0-9][0-9][0-9]$', d)]):
		shotPath = '%s\\%s' %(seq_path, fol)
		if os.path.isdir(shotPath):
			shots.append(fol)

	print '---------- SHOTS FOUND ----------'
	'\n'.join(shots)

	all_size = 0
	for shot in shots:
		shotPath = os.path.join(seq_path, shot)
		print '\n---Looking in %s' %shotPath
		nuke_path = os.path.join(shotPath, 'nuke')
		if not os.path.exists(nuke_path):
			print '%s  Skipped, path do not exists: %s' %(shot, nuke_path)
			continue

		hv_folds = [d for d in os.listdir(nuke_path) if os.path.isdir(os.path.join(nuke_path, d))]
		print 'Folder founded in %s:\n  %s' %(shot, '\n  '.join(hv_folds))
		if len(hv_folds) != 2 or 'hero' not in hv_folds or 'version' not in hv_folds:
			print 'folder found do not match the pattern hero, version'
			continue 

		hero_path = os.path.join(nuke_path, 'hero')
		version_path = os.path.join(nuke_path, 'version')

		inside_hero = os.listdir(hero_path)
		print 'Folder founded in hero:\n  %s' %('\n  '.join(inside_hero))

		nuke_fols = [fol for fol in inside_hero if fol.endswith('_nuke')]
		if not nuke_fols:
			print 'Cannot find _nuke folder in hero'
			continue
		nuke_fol = nuke_fols[0]

		nuke_path = os.path.join(hero_path, nuke_fol)
		dpxs = [f for f in os.listdir(nuke_path) if os.path.isfile(os.path.join(nuke_path, f)) and os.path.splitext(f)[-1] == '.dpx']
		if not dpxs:
			print 'No rendered dpx images in %s' %nuke_path
			continue
		else:
			print '%s dpx files in %s' %(len(dpxs), nuke_fol)

		inside_version = [f for f in os.listdir(version_path) if os.path.isdir(os.path.join(version_path, f))]
		print 'Version folder(s) founded:\n  %s' %('\n  '.join(inside_version))

		if not inside_version:
			print 'Nothing found in %s' %version_path
			continue

		# print '%s\nDelete? (y/n/q)' %('\n'.join([os.path.join(version_path, p) for p in inside_version]))
		# userInput = raw_input()

		# if userInput == 'y':
		version_size = 0
		for v in inside_version:
			version = os.path.join(version_path, v)
			ver_dpxs = os.listdir(version)
			version_size = sum([os.path.getsize(os.path.join(version, f)) for f in ver_dpxs])
			all_size += version_size
			print 'remove %s (%0.2f mb.)' %(version, version_size/(1024*1024.0))
			shutil.rmtree(version)
		# if userInput == 'q':
			# print 'Quit by user'
			# break
	print 'Total: %0.2f mb. removed' %(all_size/(1021*1024.0))

if __name__ == "__main__": 
	print '\nRight-click and paste sequence path.'
	seq_path = str(raw_input())
	removeRenderVersion(seq_path)