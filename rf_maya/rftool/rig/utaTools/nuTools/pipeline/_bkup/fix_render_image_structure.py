import sys, os, shutil, re
sys.path.append('P:/sysTool/python')

from nuTools.pipeline import iglooPipelineOs
reload(iglooPipelineOs)
from nuTools import fileTools
reload(fileTools)

IMG_FORMATS = ('.dpx', '.tif', '.tiff')

def fix(seq_path, job):
	shot_paths = iglooPipelineOs.getShotsInSeqFromPath(seqPath=seq_path)

	if not shot_paths:
		err = '\nERROR: Cannot get shot(s) from %s' %(shot_paths)
		print err
		return

	for path in shot_paths:
		path = os.path.normpath(path)
		print '\n------------------------------'
		print '---------- Shot: %s' %path

		splits = path.split('\\')
		ch = splits[-3]
		seq = splits[-2]
		sh = splits[-1]

		depts = [d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))==True]
		if job not in depts:
			print 'Error : %s no %s folder. Conitnue? (y/n)' %(path, job)
			answer = raw_input()
			if answer == 'y':
				continue
			elif answer == 'q':
				return

		hero_path = os.path.join(path, job, 'hero')
		if not os.path.exists(hero_path):
			print 'Error : %s no hero folder. Conitnue? (y/n)' %path
			answer = raw_input()
			if answer == 'y':
				continue
			elif answer == 'q':
				return

		inside_heros = [d for d in os.listdir(hero_path) if os.path.isdir(os.path.join(hero_path, d))==True]
		print '\n--- Found\n%s\ncontinue?' %'\n'.join(inside_heros)
		answer = raw_input()
		if answer == 'y':
			pass
		elif answer == 'n':
			continue
		elif answer == 'q':
			return

		edit_fold_name = '%s_%s_%s_edit' %(ch, seq, sh)
		nuke_fold_name = '%s_%s_%s_%s' %(ch, seq, sh, job)
		fold_names = [edit_fold_name, nuke_fold_name]


		if not os.path.exists(os.path.join(hero_path, edit_fold_name)):
			for i in inside_heros:
				print '\n--- Folder : %s.' %i
				if i.endswith('_edit'):
					print '\tRename %s ---> %s?' %(i, edit_fold_name)
					answer = raw_input()
					if answer == 'y':
						# renaming edit
						os.rename(os.path.join(hero_path, i), os.path.join(hero_path, edit_fold_name))
						print '\tRenamed: %s ---> %s' %(i, edit_fold_name)
					elif answer == 'n':
						continue	
					elif answer == 'q':
						return
		else:
			print '\n%s : Folder name is already correct.' %edit_fold_name
		print '\n--- Done renaming edit folder.' 

		if not os.path.exists(os.path.join(hero_path, nuke_fold_name)):				
			for i in inside_heros:
				print '\n--- Folder : %s.' %i
				if i.endswith('_%s' %job):
					print '\tRename %s ---> %s?' %(i, nuke_fold_name)
					answer = raw_input()
					if answer == 'y':
						os.rename(os.path.join(hero_path, i), os.path.join(hero_path, nuke_fold_name))
						print '\tRenamed: %s ---> %s' %(i, nuke_fold_name)
					elif answer == 'n':
						continue
					elif answer == 'q':
						return		
		else:
			print '\n%s : Folder name is already correct.' %nuke_fold_name
		print '\n--- Done renaming %s folder.' %job 

		edit_dir = os.path.join(hero_path, edit_fold_name)
		inside_edit = [f for f in os.listdir(edit_dir) if os.path.isfile(os.path.join(edit_dir, f))]

		for f in inside_edit:
			if f.endswith('.mov'):
				print '\n--- File %s' %f
				print '\tDelete %s?' %f
				answer = raw_input()
				if answer == 'y':
					os.remove(os.path.join(edit_dir, f))
					print '\tRemoved %s' %f
				elif answer == 'q':
					return

		# renaming nuke
		nuke_dir = os.path.join(hero_path, nuke_fold_name)
		# print nuke_dir
		seqs, imgs, ext = fileTools.getFileSeqInDir(nuke_dir, exts=IMG_FORMATS)

		# print 'Images\n%s' %'\n'.join(imgs)
		# print 'Seq\n%s' %'\n'.join(seqs)
		fls = [f for f in os.listdir(nuke_dir) if os.path.isfile(os.path.join(nuke_dir, f)) and os.path.splitext(os.path.join(nuke_dir, f))[-1] in IMG_FORMATS]
		num_seq = (int(seqs[-1]) - int(seqs[0])) + 1
		print '\nSeq : %s - %s (%s)' %(seqs[0], seqs[-1], num_seq)
		print 'Ext : %s' %ext
		print '--- %s files found\n--- %s seq files found' %(len(fls), len(seqs))
		
		print 'Start rename?'
		answer = raw_input()
		if answer == 'y':
			pass
		elif answer == 'n':
			continue
		elif answer == 'q':
			return

		ask = True
		answer = 'n'
		for img, seq in zip(imgs, seqs):
			seq_int = int(seq)
			new_name = '%s.%s%s' %(nuke_fold_name, str(seq_int).zfill(4), ext)
			if img != new_name:
				if ask == True:
					print '\noldname is : %s\nnew name is : %s\nRename?(y/yall/n)' %(img, new_name)
					answer = raw_input()
				if answer == 'yall':
					ask = False
				elif ansawer == 'nall':
					break

				if answer == 'y' or answer == 'yall':
					os.rename(os.path.join(nuke_dir, img), os.path.join(nuke_dir, new_name))
					print 'Renamed %s ---> %s' %(img, new_name)
				elif answer == 'q':
					return

		print '\n----- DONE %s_%s_%s' %(ch, seq, sh)


if __name__ == '__main__': 
	print '\nSeq Path?'
	seqPath = raw_input()

	print '\nJob?'
	job = raw_input()

	# do it
	fix(os.path.normpath(str(seqPath)), str(job))