import sys, os
from distutils import dir_util as dirutil
sys.path.append('P:\\sysTool\\python')


def fixAlembicPathCmd():
	cmd = '''
	global proc fixAlembicFilePath() { 
		string $currentScene = `file -q -sn`;
		string $dirSplits[];
		tokenize $currentScene "/" $dirSplits;
		string $osSplits[];
		tokenize $dirSplits[size($dirSplits)-3] "_" $osSplits;
		string $shotName = ($osSplits[1]+"_"+$osSplits[2]+"_"+$osSplits[3]);
		string $job = $osSplits[4];
		string $newPath = "";
		int $i;
		for($i=0; $i<size($dirSplits)-1; $i++) {
			$newPath += $dirSplits[$i];
			$newPath += "/";
			if($dirSplits[$i] == ("outsource_"+$shotName+"_"+$job)) {
				break;
			}
		}
		$newPath += "data/"+$shotName+"_"+$job+".abc";
		string $alembicNodes[] = `ls -type "AlembicNode"`;
		string $node;
		int $fixed = false;
		for($node in $alembicNodes) {
			if(`getAttr ($node+".abc_File")` != $newPath) {
				setAttr -typ "string" ($node+".abc_File") $newPath;
				print ("Alembic path fixed : "+$newPath+"\\n");
				$fixed = true;
			}
		}
		if($fixed == true) {
			warning "\\nPlease SAVE and RE-OPEN the scene for alembic to reload.";
		}
	}
	fixAlembicFilePath;
	'''
	return cmd	

def exportShotAsAlembic(job, outputdir=None, inputdir=None, cam=None):
	'''
	Export anim shot using Maya's standard alembic implementation.
	Ignore every objects under group named 'util'.
	@run
		from nuTools.pipeline import outsource
		reload(outsource)
		outsource.exportShotAsAlembic(job, outputdir='D:')
	'''
	# maya modules
	import pymel.core as pm
	from pymel import versions
	import maya.OpenMaya as om
	import maya.cmds as mc
	import maya.mel as mel

	# custom modules
	from nuTools.pipeline import iglooPipeline
	reload(iglooPipeline)
	from nuTools.pipeline import pipeTools
	reload(pipeTools)

	if versions.fullName() < 2013:
		om.MGlobal.displayError('Maya version must be greather than 2013 to use alembic.')
		return

	# if no inputdir arg passed, inputdir is the current scene
	openScene = True
	if not inputdir:
		# do not re open the scene again
		inputdir = pm.sceneName()
		openScene = False

	# check input dir
	inputdir = os.path.normpath(inputdir)
	if not os.path.exists(inputdir):
		om.MGlobal.displayError('Path do not exists : %s' %inputdir)
		return
	inputdir = inputdir.replace('\\', '/')

	# open the scene. turn off promt
	mel.eval('file -pmt false;')
	# inputdir arg not passed
	if openScene == True:
		pm.openFile(inputdir, f=True)

	# get shot info
	shot = iglooPipeline.Shot()
	retcode = shot.load()
	if retcode != 1:
		om.MGlobal.displayError('Current shot is not properly in the pipeline.')
		return

	# get the camera for this shot
	if not cam:
		cam = iglooPipeline.getShotCam(shot=shot)
		if not cam:
			om.MGlobal.displayError('Cannot find shot camera!')
			return
		# iglooPipeline.getShotCam returns camera shape, need to get transform
		cam = cam.getParent()
	else:
		try:
			cam = pm.ls(cam)[0]
		except:
			om.MGlobal.displayError('Cannot find camera: %s' %cam)
			return

	# get root group to export
	excludes = ['persp', 'top', 'front', 'side', 'util']
	objs = [a for a in mc.ls(assemblies=True) if a not in excludes and mc.getAttr('%s.visibility' %a) == True]

	# get existing namespaces
	sysNss = ['UI', 'shared']
	mc.namespace(set=':')
	namespaces = [ns for ns in mc.namespaceInfo(lon=True, r=True) if ns not in sysNss]

	if not objs:
		om.MGlobal.displayError('No valid polygon object to export.')
		return

	# if outputdir arg is not passed, try to make folder in job
	if not outputdir:
		outputdir = shot.getOutsourceDeptDir(job=job, check=False)
		if not os.path.exists(outputdir):
			os.makedirs(outputdir)
			om.MGlobal.displayInfo('Folder created : %s' %outputdir)

	# check output dir
	outputdir = os.path.normpath(outputdir)
	outputdir = outputdir.replace('\\', '/')

	heroPath = '%s/hero' %(outputdir)
	if not os.path.exists(heroPath):
		os.makedirs(heroPath)
		om.MGlobal.displayInfo('Folder created : %s' %heroPath)
	dataPath = '%s/data' %(outputdir)
	if not os.path.exists(dataPath):
		os.makedirs(dataPath)
		om.MGlobal.displayInfo('Folder created : %s' %dataPath)
	outputPath = '%s/output' %(outputdir)
	if not os.path.exists(outputPath):
		os.makedirs(outputPath)
		om.MGlobal.displayInfo('Folder created : %s' %outputPath)
	versionPath = '%s/version' %(outputdir)
	if not os.path.exists(versionPath):
		os.makedirs(versionPath)
		om.MGlobal.displayInfo('Folder created : %s' %versionPath)

	# file path
	fileName = '%s_%s_%s_%s.ma' %(shot.ch, shot.seq, shot.sh, job)
	camFileName = '%s_%s_%s_%s_cam.ma' %(shot.ch, shot.seq, shot.sh, job)
	filePath = '%s/%s' %(heroPath, fileName)
	camFilePath = '%s/%s' %(heroPath, camFileName)

	# cache path
	cacheName = '%s_%s_%s_%s.abc' %(shot.ch, shot.seq, shot.sh, job)
	cachePath = '%s/%s' %(dataPath, cacheName)

	# create baked camera
	bakedCam = iglooPipeline.createBakedCamera(cam=cam, 
											   bakedCamName='%s_%s_%s_cam' %(shot.ch, shot.seq, shot.sh))
	bakedCamShpName = bakedCam.getShape().longName()
	pm.select(bakedCam, r=True)

	# export camera
	mel.eval('file -force -options "v=0" -typ "mayaAscii" -es "%s";' %camFilePath)

	# get time range
	startFrame = pm.playbackOptions(q=True, min=True)
	endFrame = pm.playbackOptions(q=True, max =True)

	objStr = '-root '
	objStr += ' -root '.join(objs)

	# export alembic
	# pm.refresh(suspend=True)
	pm.AbcExport(j='-frameRange %s %s -ro -uvWrite -worldSpace -writeVisibility %s -file %s' %(startFrame, endFrame, objStr, cachePath))
	# pm.refresh(suspend=False)
	# pm.refresh(force=True)

	# open new file
	pm.newFile(f=True)

	# set start end frame
	pm.playbackOptions(e=True, min=startFrame)
	pm.playbackOptions(e=True, max =endFrame)
	pm.currentTime(startFrame, e=True)

	# reference camera
	pm.createReference(camFilePath, ns='cam')

	# create empty namespace
	for ns in namespaces:
		mc.namespace(add=ns)

	# import alembic
	pm.AbcImport(cachePath, mode='import')

	# delete all empty group and nurbsCurves
	allTrs = pm.ls(type='transform')
	for tr in sorted(allTrs, key=lambda name:len(name.longName().split('|')), reverse=True):
		children = tr.getChildren()
		if not children or isinstance(children[0], pm.nt.NurbsCurve):
			pm.delete(tr)

	# set viewport
	mel.eval('setNamedPanelLayout("Single Perspective View");')
	modelpanel = mc.getPanel(wl='Persp View')
	bakedCamShpNameNs = bakedCamShpName.replace('|', '|cam:')
	if not bakedCamShpNameNs:
		bakedCamShpNameNs = 'perspShape'
	mc.modelEditor(modelpanel, e=True, cam=bakedCamShpNameNs, wos=False, displayAppearance='wireframe', 
			df=False, dim=False, ca=False, hs=False, ha=False, ikh=False, j=False, sds=False,
			lt=False, lc=False, ncl=False, npa=False, nr=False, nc=False, str=False, hu=False,
			dy=False, pv=False, pl=False, fl=False, fo=False, dc=False, tx=False, mt=False,
			m=True, ns=True, pm=True )

	# set render resolution
	resNodes = pm.ls(type='resolution')
	if resNodes:
		defResNode = resNodes[0]
		defResNode.width.set(1920)
		defResNode.height.set(800)
		defResNode.deviceAspectRatio.set(2.4)

	# create script node for fix alembic path if our outsource copy the directory
	# to his/her computer this will help them fix the alembic path automatically
	# mc.scriptNode(st=2, bs=fixAlembicPathCmd(),nu n='fileAlembicPath_script', stp='mel')

	# save it
	pm.saveAs(filePath, f=True)
	om.MGlobal.displayInfo('Shot exported successfully done : %s_%s_%s' %(shot.ch, shot.seq, shot.sh))

def exportShotAsAlembic_exocortex(job, outputdir=None, inputdir=None):
	'''
	Export anim shot using Exocortex's alembic plugin.
	Ignore every objects under group named 'util'.
	@run
		from nuTools.pipeline import outsource
		reload(outsource)
		outsource.exportShotAsAlembic_exocortex(outputdir='D:')
	'''
	# maya modules
	import pymel.core as pm
	import maya.OpenMaya as om
	import maya.cmds as mc
	import maya.mel as mel

	# custom modules
	from nuTools.pipeline import iglooPipeline
	reload(iglooPipeline)
	from nuTools.pipeline import pipeTools
	reload(pipeTools)

	# check if Alembic plugin is loaded?
	try:
		alembic_pluginFileName = 'MayaExocortexAlembic.mll'
		if mc.pluginInfo(alembic_pluginFileName, q=True, l=True) == False:
			mc.loadPlugin(alembic_pluginFileName)
	except:
		om.MGlobal.displayError('Cannot load Exocortex alembic plugin.')
		return


	# if no inputdir arg passed, inputdir is the current scene
	# and do not re open the scene again
	openScene = True
	if not inputdir:
		inputdir = pm.sceneName()
		openScene = False

	# check input dir
	inputdir = os.path.normpath(inputdir)
	if not os.path.exists(inputdir):
		om.MGlobal.displayError('Path do not exists : %s' %inputdir)
		return
	inputdir = inputdir.replace('\\', '/')

	# open the scene
	mel.eval('file -pmt false;')
	if openScene == True:
		pm.openFile(inputdir, f=True)

	# get shot info
	shot = iglooPipeline.Shot()
	retcode = shot.load()
	if retcode != 1:
		om.MGlobal.displayError('Current shot is not properly in the pipeline.')
		return

	# get the camera for this shot
	cam = iglooPipeline.getShotCam(shot=shot)
	if not cam:
		om.MGlobal.displayError('Cannot find shot camera!')
		return
	# iglooPipeline.getShotCam returns camera shape, need to get transform
	cam = cam.getParent()

	# get all polygons to export
	objs = []
	for ref in (r for r in pm.listReferences() if r.isLoaded() and not r.isDeferred()):
		splits = ref.path.split('/')
		# except camera that's typed 'util'
		if splits[4] in ['util']:
			continue

		nodes = ref.nodes()
		nodes = [n.longName() for n in nodes if n.type()=='transform']
		nodes = pipeTools.getAllVisibleObjectInNodes(nodes=nodes, types=['mesh'])
		if nodes:
			objs.extend(nodes)

	if not objs:
		om.MGlobal.displayError('No valid polygon object to export.')
		return

	# if outputdir arg is not passed, try to make folder in job
	if not outputdir:
		outputdir = shot.getOutsourceDeptDir(job=job, check=False)
		if not os.path.exists(outputdir):
			os.makedirs(outputdir)
			om.MGlobal.displayInfo('Folder created : %s' %outputdir)

	# check output dir
	outputdir = os.path.normpath(outputdir)
	outputdir = outputdir.replace('\\', '/')

	heroPath = '%s/hero' %(outputdir)
	if not os.path.exists(heroPath):
		os.makedirs(heroPath)
		om.MGlobal.displayInfo('Folder created : %s' %heroPath)
	dataPath = '%s/data' %(outputdir)
	if not os.path.exists(dataPath):
		os.makedirs(dataPath)
		om.MGlobal.displayInfo('Folder created : %s' %dataPath)
	outputPath = '%s/output' %(outputdir)
	if not os.path.exists(outputPath):
		os.makedirs(outputPath)
		om.MGlobal.displayInfo('Folder created : %s' %outputPath)
	versionPath = '%s/version' %(outputdir)
	if not os.path.exists(versionPath):
		os.makedirs(versionPath)
		om.MGlobal.displayInfo('Folder created : %s' %versionPath)

	# file path
	fileName = '%s_%s_%s_%s.ma' %(shot.ch, shot.seq, shot.sh, job)
	camFileName = '%s_%s_%s_%s_cam.ma' %(shot.ch, shot.seq, shot.sh, job)
	filePath = '%s/%s' %(heroPath, fileName)
	camFilePath = '%s/%s' %(heroPath, camFileName)

	# cache path
	cacheName = '%s_%s_%s_%s.abc' %(shot.ch, shot.seq, shot.sh, job)
	cachePath = '%s/%s' %(dataPath, cacheName)

	# create baked camera
	bakedCam = iglooPipeline.createBakedCamera(cam=cam, 
											   bakedCamName='%s_%s_%s_cam' %(shot.ch, shot.seq, shot.sh))
	bakedCamShpName = bakedCam.getShape().longName()
	pm.select(bakedCam, r=True)

	# export camera
	mel.eval('file -force -options "v=0" -typ "mayaAscii" -es "%s";' %camFilePath)

	# get time range
	startFrame = pm.playbackOptions(q=True, min=True)
	endFrame = pm.playbackOptions(q=True, max =True)

	objStr = ','.join(objs)

	# export alembic
	exjobStr = 'in=%s;out=%s;step=1;substep=1;filename=%s;' %(startFrame, 
															endFrame, 
															cachePath)
	exjobStr += 'objects=%s' %(objStr)
	exjobStr += ';ogawa=0;purepointcache=0;uvs=1;facesets=0;normals=1;dynamictopology=0;globalspace=1;withouthierarchy=0;transformcache=0'
	mc.ExocortexAlembic_export(j=exjobStr)

	# open new file
	pm.newFile(f=True)

	# set start end frame
	pm.playbackOptions(e=True, min=startFrame)
	pm.playbackOptions(e=True, max =endFrame)
	pm.currentTime(startFrame, e=True)

	# reference camera, import alembic
	pm.createReference(camFilePath, ns='cam')
	imjobStr = 'filename=%s;normals=0;uvs=1;facesets=0;multi=0;fitTimeRange=0' %cachePath
	mc.ExocortexAlembic_import(j=imjobStr)

	# set viewport
	mel.eval('setNamedPanelLayout("Single Perspective View");')
	modelpanel = mc.getPanel(wl='Persp View')
	bakedCamShpNameNs = bakedCamShpName.replace('|', '|cam:')
	if not bakedCamShpNameNs:
		bakedCamShpNameNs = 'perspShape'
	mc.modelEditor(modelpanel, e=True, cam=bakedCamShpNameNs, wos=False, displayAppearance='wireframe', 
			df=False, dim=False, ca=False, hs=False, ha=False, ikh=False, j=False, sds=False,
			lt=False, lc=False, ncl=False, npa=False, nr=False, nc=False, str=False, hu=False,
			dy=False, pv=False, pl=False, fl=False, fo=False, dc=False, tx=False, mt=False,
			m=True, ns=True, pm=True )

	# set render resolution
	resNodes = pm.ls(type='resolution')
	if resNodes:
		defResNode = resNodes[0]
		defResNode.width.set(1920)
		defResNode.height.set(800)
		defResNode.deviceAspectRatio.set(2.4)

	# save it
	pm.saveAs(filePath, f=True)
	om.MGlobal.displayInfo('Shot exported successfully done : %s_%s_%s' %(shot.ch, shot.seq, shot.sh))

def copyShot_out(shots, outputdir, job='', proj='nine'):
	'''
		Copy all outsourced shot folders out to a specified directory.
		@param
			list(str) shots - ['4.4.2340', '4.5.2560', '4.5.2660']
			str job - the specified suffix str for outsource shot folder.
			str proj - the project to work on.
	'''

	from nuTools.pipeline import iglooPipelineOs
	reload(iglooPipelineOs)

	# shots = ['4.3.235']
	if not isinstance(shots, (list, tuple)):
		raise TypeError, 'Shot argrument must be a list!'

	outputdir = outputdir.replace('"', '')
	outputdir = outputdir.replace('/', '\\')
	outputdir = os.path.normpath(outputdir)

	if not os.path.exists(outputdir):
		raise IOError, 'Path do not exists : %s' %outputdir

	for shot in shots:
		shotElems = shot.split('.')
		if len(shotElems) != 3:
			raise ValueError, 'Skipped, Invalid shot argrument : %s' %shot
			continue

		shotObj = None
		try:
			shotObj = iglooPipelineOs.getShot(ch=shotElems[0], 
										seq=shotElems[1], 
										sh=shotElems[2], 
										proj=proj)
		except:
			raise IOError, 'Skipped, Cannot get shot : %s' %shot
			continue

		deptDir = None
		try:
			deptDir = shotObj.getOutsourceDeptDir(job=job, check=True)
		except:
			raise IOError, 'Skipped, Cannot get outsource directory : outsource_%s' %job
			continue

		deptDir = deptDir.replace('/', '\\')
		if job:
			jobStr = '_%s' %job
		shotDir = '%s\\%s_%s_%s%s' %(outputdir, shotObj.ch, shotObj.seq, shotObj.sh, jobStr)
		if not os.path.exists(shotDir):
			os.makedirs(shotDir)
			print 'Folder created : %s' %shotDir

		# copy the entire folder
		dirutil.copy_tree(deptDir, shotDir)

	print 'Copy shot out done.'

def copyShot_in(inputdir, proj='nine'):
	'''
		Copy all outsourced shot folders into correct pipeline directories.
		A shot folder should looks like - 'outsource_ch04_seq005_sh0250_efx'.
		@param
			str inputdir - the directory that contains all shot folders
			str proj - the project to work on.
	'''
	from nuTools.pipeline import iglooPipelineOs
	reload(iglooPipelineOs)

	# shots = ['4.3.235']
	if not isinstance(shots, (list, tuple)):
		raise TypeError, 'Shot argrument must be a list!'

	inputdir = inputdir.replace('"', '')
	inputdir = inputdir.replace('/', '\\')
	inputdir = os.path.normpath(inputdir)

	if not os.path.exists(inputdir):
		raise IOError, 'Path do not exists : %s' %inputdir

	shotsFolders = (fol for fol in os.path.listdir(inputdir) if os.path.isdir(fol) and fol.startswith('outsource_'))

	for folder in shotsFolders:
		elems = folder.split('_')
		if len(elems) != 5:
			raise ValueError, 'Skipped, Invalid shot argrument : %s' %shot
			continue

		ch = elems[1].replace('ch', '')
		seq = elems[2].replace('seq', '')
		sh = elems[3].replace('sh', '')
		shotObj = None
		try:
			shotObj = iglooPipelineOs.getShot(ch=ch, 
										seq=seq, 
										sh=sh, 
										proj=proj)
		except:
			raise IOError, 'Skipped, Cannot get shot : %s' %folder
			continue

		source = '%s\\%s' %(inputdir, folder)
		destination = shotObj.PATH.replace('/', '\\')

		# copy the entire folder
		dirutil.copy_tree(source, destination)

	print 'Copy shot in done.'





