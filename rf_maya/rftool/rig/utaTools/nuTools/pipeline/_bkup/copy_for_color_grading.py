import sys, os, shutil
sys.path.append('P:/sysTool/python')
from nuTools.pipeline import iglooPipelineOs
reload(iglooPipelineOs)

def copyRenderImage_out(proj, ch, seq, shots, job, dest):
	# shots = [2253, 2236, ...]
	shotObjs = []
	if isinstance(shots, (list, tuple)):
		for shot in shots:
			shotObj = None
			try:
				shotObj = iglooPipelineOs.getShot(ch=ch, 
											seq=seq, 
											sh=shot, 
											proj=proj,
											mode=1)
				shotObjs.append(shotObj)
			except Exception, e:
				print e
				print 'WARNING: Skipped, Cannot get shot : %s' %shot
				continue
	elif shots == 'all':
		shotObjs = iglooPipelineOs.getShotsInSeq(ch, seq, proj=proj, mode=1)
	else:
		raise TypeError, 'Shot argrument must be a list of shot num(int) or str("all") to get all shots in the sequence.'

		
	if not shotObjs:
		print '\nERROR: Cannot get shot(s) for %s %s %s' %(proj, ch, seq)
		return

	dest = dest.replace('"', '')
	dest = dest.replace('/', '\\')
	dest = os.path.normpath(dest)

	if not os.path.exists(dest):
		print 'ERROR: Path do not exists : %s' %dest

	curr_sh = 0
	num_shots = len(shotObjs)

	for shotObj in shotObjs:
		print '\n'
		# get hero dir first
		heroDir = None
		try:
			heroDir = shotObj.getDeptHeroDir(job=job, check=True)
		except Exception, e:
			print e
			print 'WARNING: Skipped, Cannot get department directory : %s' %job
			continue
		heroDir = heroDir.replace('/', '\\')

		files = []
		
		imageFolderName = '%s_%s_%s_nuke' %(shotObj.ch, shotObj.seq, shotObj.sh)
		imageFolder = os.path.join(heroDir, imageFolderName)
		if os.path.exists(imageFolder):
			files = [f for f in os.listdir(imageFolder) if os.path.isfile(os.path.join(imageFolder, f))]
		else:
			print 'WARNING: Path does not exists %s' %imageFolder
			continue

		if not files:
			print 'WARNING: No image found in %s' %heroDir
			continue

		# get/create destination dir
		destFolder = '%s\\%s' %(dest, imageFolderName)
		if not os.path.exists(destFolder):
			os.makedirs(destFolder)
			print 'Folder created : %s' %destFolder

		num_files = len(files)
		percent = 0.0
		print '-----Copying from %s-----' %(imageFolder)
		for f in files:

			fp = os.path.join(imageFolder, f)
			# shutil.copy(fp, destFolder)
			# dirutil.copy_tree(fp, destFolder)

			percent += (1.0/num_files)*100
			percentStr = '%.2f' %percent
			print 'copying %s\t(%s %%)   ' %(shotObj.sh, percentStr)

		curr_sh += 1
		print '-----Shot(s) copied %s/%s-----' %(curr_sh, num_shots)


if __name__ == "__main__": 
	print '\nProject?'
	proj = str(raw_input())

	print '\nChapter?'
	ch = int(raw_input())

	print '\nSequence?'
	seq = int(raw_input())

	print '\nAll shots? (y/n)'
	allsh = str(raw_input())
	if allsh == 'y':
		shots = 'all'
	elif allsh == 'n':
		print 'Type in shot number separate by space. (ie. 2205 2206 ...)'
		shotStr = str(raw_input())
		shots = shotStr.split()
	else:
		raise SystemError, 'Invalid input: only accept "y" or "n"'

	print '\nJob?'
	job = str(raw_input())

	print '\nCopy destination?'
	dest = os.path.normpath(str(raw_input()))

	# do it
	copyRenderImage_out(proj, ch, seq, shots, job, dest)