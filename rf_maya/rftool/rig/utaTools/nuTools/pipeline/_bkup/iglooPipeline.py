import os, sys, re, subprocess, shutil
import difflib, collections
from tempfile import mkstemp

import pymel.core as pm
import maya.cmds as mc
import maya.OpenMaya as om
import maya.mel as mel

import nuTools.config as config
import nuTools.fileTools as fileTools
from nuTools.pipeline import pipeTools as pipeTools
from nuTools import misc

reload(config)
reload(fileTools)
reload(pipeTools)
reload(misc)

# MAIN_DRIVE = config.MAIN_DRIVE

class Entity(object):
	def __init__(self, path=''):
		if not path:
			path = pm.sceneName()

		self.splits = []
		self.fileext = ''  # file with ext

		self.inputpath = path
		self.file = ''   # just the file
		self.ext = ''	# just the ext

		self.drive = ''
		self.proj = ''
		self.entityType = ''
		self.job = ''
		self.elem = ''
		self.name = ''

		self.PATH = ''
		self.FILEPATH = ''

	def splitPath(self):
		self.checkPathExist(self.inputpath)
		path = os.path.normpath(self.inputpath)
		path = path.replace('\\', '/')

		for proj in config.PROJ_DIR_DICT.keys():
			psplit = '/%s/' %proj
			if psplit in path:
				drive_splits = path.split(psplit)
				# print drive_splits
				self.splits = [drive_splits[0], proj]
				# print psplit.join(drive_splits[1:]).split('/')
				self.splits.extend(psplit.join(drive_splits[1:]).split('/'))

				self.drive = self.splits[0]
				self.proj = self.splits[1]
				self.entityType = self.splits[3]
				break
				
		if len(self.splits) < 7:
			sys.exit('Entity does not exists!')

	def checkPathExist(self, path, check=True):
		if not os.path.exists(path):
			if check == True:
				if not path:
					path = '<pipeline path>'
				sys.exit('%s  : Does not exists!' %path)
			else:
				return False
		return path

	def getLatestVersionNumInPath(self, path, inc=False):
		exts = ['.ma', '.mb']
		files = [ f for f in os.listdir( path ) if os.path.splitext(f)[-1] in exts]

		numList = []
		version = '001'
		try :
			for f in files :	
				vre = re.search(r"(_v\d\d\d.)", f)
				if vre:
					v = vre.group()
					numList.append(int(v[2:-1]))

			ver = max(numList)

			if inc == True:
				ver += 1
			version = str(ver).zfill(3)
		except :
			pass 

		return version

	def getLatestVersionNum(self, job, elem, inc=False):
		exts = ['.ma', '.mb']
		path = self.getVersionDir(job)
		files = [ f for f in os.listdir( path ) if os.path.splitext(f)[-1] in exts and '_%s_%s_' %(job, elem) in f ]

		numList = []
		version = '001'
		try :
			for f in files :	
				vre = re.search(r"(_v\d\d\d.)", f)
				if vre:
					v = vre.group()
					numList.append(int(v[2:-1]))

			ver = max(numList)

			if inc == True:
				ver += 1
			version = str(ver).zfill(3)
		except :
			pass 

		return version

	def getDeptDir(self, job, check=True):
		self.checkPathExist(self.PATH)
		path = '%s/%s' %(self.PATH, job)

		self.checkPathExist(path, check)
		return path

	def getOutsourceDeptDir(self, job='', check=True):
		self.checkPathExist(self.PATH)
		if job:
			job = '_%s' %job
		path = '%s/outsource_%s_%s_%s%s' %(self.PATH, self.ch, self.seq, self.sh, job)

		self.checkPathExist(path, check)
		return path

	def getExistingJobDirs(self):
		self.checkPathExist(self.PATH)
		dirs = [d for d in os.listdir(self.PATH) if os.path.isdir('%s/%s' %(self.PATH, d))]
		return dirs

	def getDeptOutputDir(self, job, check=True):
		deptDir = self.getDeptDir(job, check=check)
		path = '%s/output' %(deptDir)

		self.checkPathExist(path, check)
		return path 

	def getDeptDataDir(self, job, check=True):
		deptDir = self.getDeptDir(job, check=check)
		path = '%s/data' %(deptDir)

		self.checkPathExist(path, check)
		return path 

	def getDeptOutsourceDir(self, job, check=True):
		deptDir = self.getDeptDir(job, check=check)
		path = '%s/outsource' %(deptDir)

		self.checkPathExist(path, check)
		return path 

	def getVersionDir(self, job, check=True):
		self.checkPathExist(self.PATH)
		path = '%s/%s/version' %(self.PATH, job)

		self.checkPathExist(path, check)
		return path

	def getVersionFiles(self, job, elem=''):
		self.checkPathExist(self.PATH)
		path = '%s/%s/version' %(self.PATH, job)
		exts = ['.ma', '.mb']
		allFiles = [ f for f in os.listdir(path ) if os.path.splitext(f)[-1] in exts ]
		if elem:
			elem = '_%s_' %elem

		files = {}
		for f in allFiles:
			if '_%s%s' %(job, elem) in f:
				files[f] = '%s/%s' %(path, f)

		return files

	def getDeptHeroDir(self, job, check=True):
		path = '%s/%s/hero' %(self.PATH, job)
		self.checkPathExist(path, check)

		return path

	def getHeroDir(self, check=True):
		path = '%s/hero' %(self.PATH)
		self.checkPathExist(path, check)

		return path

class Shot(Entity):
	def __init__(self, path=''):
		Entity.__init__(self, path)

		self.ch = ''
		self.seq = ''
		self.sh = ''


	def load(self):
		self.checkPathExist(self.inputpath, True)

		try:
			self.getShotData()
		except: 
			om.MGlobal.displayError('Failed getting shot data : %s' %self.inputpath)
			self.PATH = self.inputpath
			return 0

		if os.path.isfile(self.inputpath) == True:
			try:
				self.getShotFileData()
			except: 
				om.MGlobal.displayError('Failed getting shot file data : %s' %self.inputpath)
				return -1

		return 1


	def getShotData(self):
		self.splitPath()
		self.ch = self.splits[4]
		self.seq = self.splits[5]
		self.name = self.splits[6]
		self.sh = self.name

		indexSplits = len(self.splits)
		self.PATH = '/'.join(self.splits[:7])

	def getShotFileData(self):
		self.fileext = os.path.basename(self.inputpath)
		extSplits = os.path.splitext(self.fileext)

		self.file = extSplits[0]
		self.ext = extSplits[1]

		self.job = self.splits[7]

		self.FILEPATH = self.inputpath

		verNum = ''
		elem = self.file

		verSearch = re.search(r"(_v\d\d\d.)", self.fileext)
		if verSearch:
			verNum = verSearch.group()
			elem = self.fileext.split(verNum)[0]

		self.elem = elem.split('_')[-1]	

	def getSeqMasterDir(self, check=False):
		seqPath = '/'.join(self.splits[:6])
		masterPath = '%s/master' %seqPath
		if check == True:
			self.checkPathExist(masterPath, check)

		return masterPath

	def getSeqMasterDeptDir(self, job, check=False):
		masterPath = self.getSeqMasterDir(check=check)
		masterDeptPath = '%s/%s' %(masterPath, job)
		if check == True:
			self.checkPathExist(masterDeptPath, check)

		return masterDeptPath

	def getMasterDeptHeroDir(self, job, check=True):
		masterPath = self.getSeqMasterDir(check=check)
		path = '%s/%s/hero' %(masterPath, job)
		self.checkPathExist(path, check)

		return path

	def getMasterDeptHero(self, job, elem, check=True):
		masterPath = self.getSeqMasterDir(check=check)
		path = '%s/%s/hero/%s_%s_%s_%s_%s.ma' %(masterPath, job, self.ch, self.seq, self.sh, job, elem)
		self.checkPathExist(path, check)

		return path

	def getDeptHero(self, job, elem, check=True):
		path = '%s/%s/hero/%s_%s_%s_%s_%s.ma' %(self.PATH, job, self.ch, self.seq, self.sh, job, elem)
		self.checkPathExist(path, check)

		return path

	def getHero(self, elem, check=True):
		path = '%s/hero/%s_%s_%s_%s.ma' %(self.PATH, self.ch, self.seq, self.sh, elem)
		self.checkPathExist(path, check)

		return path
	
	def getVersion(self, job, elem, version, check=True):
		versionStr = 'v%s' %str(version).zfill(3)
		allVersions = self.getVersionFiles(job, elem=elem)
		for f, p in allVersions.iteritems():
			fn, ext = os.path.splitext(p)
			splits = fn.split('_')
			if len(splits) != 6:
				continue
			ver = splits[5]
			if ver == versionStr:
				if check == True:
					self.checkPathExist(p, check)
				return p

	def backupDeptHero(self, job, elem):
		self.checkPathExist(self.PATH)

		deptHeroFile = self.getDeptHero(job, elem, True)
		
		deptHeroDir = self.getDeptHeroDir(job, True)
		backupFolderPath = '%s/.%s_%s_%s_%s_%sBackup' % (deptHeroDir, self.ch, self.seq, self.sh, job, elem)
	
		if not os.path.exists(backupFolderPath) :
			os.makedirs(backupFolderPath)
		setDirHidden(backupFolderPath)
		
		versionNumStr = self.getLatestVersionNumInPath(backupFolderPath, inc=True)
		path = '%s/%s_%s_%s_%s_%s_v%s.ma' %(backupFolderPath, self.ch, self.seq, self.sh, job, elem, versionNumStr)
		shutil.copy2(deptHeroFile, path)

		return path

	def backupHero(self, elem):
		self.checkPathExist(self.PATH)

		heroFile = self.getHero(elem, True)

		heroDir = self.getHeroDir(True)
		backupFolderPath = '%s/.%s_%s_%s_%sBackup' % (heroDir, self.ch, self.seq, self.sh, elem)
	
		if not os.path.exists(backupFolderPath) :
			os.makedirs(backupFolderPath)
		setDirHidden(backupFolderPath)
		
		versionNumStr = self.getLatestVersionNumInPath(backupFolderPath, inc=True)
		path = '%s/%s_%s_%s_%s_v%s.ma' %(backupFolderPath, self.ch, self.seq, self.sh, elem, versionNumStr)
		shutil.copy2(heroFile, path)

		return path

	def getLatestVersion(self, job, elem, check=True):
		self.checkPathExist(self.PATH)

		versionPath = self.getVersionDir(job)
		versionNumStr = self.getLatestVersionNum(job, elem, inc=False)
		path = '%s/%s_%s_%s_%s_%s_v%s.ma' %(versionPath, self.ch, self.seq, self.sh, job, elem, versionNumStr)

		self.checkPathExist(path, check)
		return path

	def getIncVersion(self, job, elem):
		self.checkPathExist(self.PATH)

		versionPath = self.getVersionDir(job)
		versionNumStr = self.getLatestVersionNum(job, elem, inc=True)
		path = '%s/%s_%s_%s_%s_%s_v%s.ma' %(versionPath, self.ch, self.seq, self.sh, job, elem, versionNumStr)

		return path

class Asset(Entity):
	def __init__(self, path=''):
		Entity.__init__(self, path)

		self.type = ''
		self.subType = ''
		self.asset = ''

	def load(self):
		self.checkPathExist(self.inputpath, True)

		try:
			self.getAssetData()
		except: 
			om.MGlobal.displayError('Failed getting asset data : %s' %self.inputpath)
			self.PATH = self.inputpath
			return 0 

		if os.path.isfile(self.inputpath) == True:
			try:
				self.getAssetFileData()
			except: 
				om.MGlobal.displayError('Failed getting asset file data : %s' %self.inputpath)
				return -1
				
		return 1

	def getAssetData(self):
		self.splitPath()
		self.type = self.splits[4]
		self.subType = self.splits[5]
		self.name = self.splits[6]
		self.asset = self.name

		indexSplits = len(self.splits)
		self.PATH = '/'.join(self.splits[:7])
		
	def getAssetFileData(self):
		self.fileext = os.path.basename(self.inputpath)
		extSplits = os.path.splitext(self.fileext)

		self.file = extSplits[0]
		self.ext = extSplits[1]

		self.job = self.splits[7]

		self.FILEPATH = self.inputpath

		verNum = ''
		elem = self.file

		verSearch = re.search(r"(_v\d\d\d.)", self.fileext)
		if verSearch:
			verNum = verSearch.group()
			elem = self.fileext.split(verNum)[0]

		self.elem = elem.split('_')[-1]		

	def getLatestVersion(self, job, elem, check=True):
		self.checkPathExist(self.PATH)

		versionPath = self.getVersionDir(job)
		versionNumStr = self.getLatestVersionNum(job, elem, inc=False)
		path = '%s/%s_%s_%s_v%s.ma' %(versionPath, self.asset, job, elem, versionNumStr)

		self.checkPathExist(path, check)
		return path

	def getIncVersion(self, job, elem):
		self.checkPathExist(self.PATH)

		versionPath = self.getVersionDir(job)
		versionNumStr = self.getLatestVersionNum(job, elem, inc=True)
		path = '%s/%s_%s_%s_v%s.ma' %(versionPath, self.asset, job, elem, versionNumStr)

		return path
	
	def getDeptHero(self, job, elem, check=True, ext='ma'):
		path = '%s/%s/hero/%s_%s_%s.%s' %(self.PATH, job, self.asset, job, elem, ext)
		self.checkPathExist(path, check)

		return path

	def getHero(self, elem, check=True, ext='ma'):
		path = '%s/hero/%s_%s.%s' %(self.PATH, self.asset, elem, ext)
		self.checkPathExist(path, check)

		return path

	def getTextureHeroResDir(self, res, check=True):
		path = self.getDeptHeroDir(job='texture', check=check)
		resDir = '%s/%s' %(path, res)
		self.checkPathExist(resDir, check)

		return resDir

	def getTextureHeroResDirs(self, check=True):
		path = self.getDeptHeroDir(job='texture', check=check)
		rets = []
		for res in ['hi', 'med', 'low', 'rig']:
			resPath = '%s/%s' %(path, res)
			self.checkPathExist(path, check)
			rets.append(resPath)

		return rets
	
	def backupDeptHero(self, job, elem):
		self.checkPathExist(self.PATH)

		deptHeroFile = self.getDeptHero(job, elem, True)
		
		deptHeroDir = self.getDeptHeroDir(job, True)
		backupFolderPath = '%s/.%s_%s_%sBackup' % (deptHeroDir, self.asset, job, elem)
	
		if not os.path.exists(backupFolderPath) :
			os.makedirs(backupFolderPath)
		setDirHidden(backupFolderPath)
		
		versionNumStr = self.getLatestVersionNumInPath(backupFolderPath, inc=True)
		path = '%s/%s_%s_%s_v%s.ma' %(backupFolderPath, self.asset, job, elem, versionNumStr)
		shutil.copy2(deptHeroFile, path)

		return path

	def backupHero(self, elem):
		self.checkPathExist(self.PATH)

		heroFile = self.getHero(elem, True)

		heroDir = self.getHeroDir(True)
		backupFolderPath = '%s/.%s_%sBackup' % (heroDir, self.asset, elem)
	
		if not os.path.exists(backupFolderPath) :
			os.makedirs(backupFolderPath)
		setDirHidden(backupFolderPath)
		
		versionNumStr = self.getLatestVersionNumInPath(backupFolderPath, inc=True)
		path = '%s/%s_%s_v%s.ma' %(backupFolderPath, self.asset, elem, versionNumStr)
		shutil.copy2(heroFile, path)

		return path

	def getLatestVersion(self, job, elem, check=True):
		self.checkPathExist(self.PATH)

		versionPath = self.getVersionDir(job)
		versionNumStr = self.getLatestVersionNum(job, elem, inc=False)
		path = '%s/%s_%s_%s_v%s.ma' %(versionPath, self.asset, job, elem, versionNumStr)

		self.checkPathExist(path, check)
		return path

	def getIncVersion(self, job, elem):
		self.checkPathExist(self.PATH)

		versionPath = self.getVersionDir(job)
		versionNumStr = self.getLatestVersionNum(job, elem, inc=True)
		path = '%s/%s_%s_%s_v%s.ma' %(versionPath, self.asset, job, elem, versionNumStr)

		return path


def makeDir(directory):
	if not os.path.exists(directory):
		os.makedirs(directory)
	return directory

def setDirHidden(directory):
	if 'win' in sys.platform :
		import ctypes
		FILE_ATTRIBUTE_HIDDEN = 0x02
		ctypes.windll.kernel32.SetFileAttributesW( ur'%s' % directory , FILE_ATTRIBUTE_HIDDEN )

def findAssetPath(name, proj='nine'):
	assetPath = '%s/asset' %config.PROJ_DIR_DICT[proj][0]
	for typ in [t for t in os.listdir(assetPath) if os.path.isdir(os.path.join(assetPath, t))]:
		typePath = '%s/%s' %(assetPath, typ)

		for subType in [s for s in os.listdir(typePath) if os.path.isdir(os.path.join(typePath, s))]:
			subTypePath = '%s/%s' %(typePath, subType)

			for asset in [a for a in os.listdir(subTypePath) if os.path.isdir(os.path.join(subTypePath, a))]:
				if asset == name:
					path = os.path.normpath('%s/%s' %(subTypePath, name))
					path = path.replace('\\', '/')
					return path

def getAssetsInType(typ, assetFilter='', proj='nine'):
	assetPath = '%s/asset' %config.PROJ_DIR_DICT[proj][0]
	assets = []
	# for typ in [t for t in os.listdir(assetPath) if os.path.isdir(os.path.join(assetPath, t))]:
	typePath = '%s/%s' %(assetPath, typ)

	for subType in [s for s in os.listdir(typePath) if os.path.isdir(os.path.join(typePath, s))]:
		subTypePath = '%s/%s' %(typePath, subType)
		assetsInSubType = [a for a in os.listdir(subTypePath) if os.path.isdir(os.path.join(subTypePath, a))]
		if assetFilter and assetsInSubType:
			assetsInSubType = difflib.get_close_matches(assetFilter, assetsInSubType, 20)
		if assetsInSubType:
			assets.extend(assetsInSubType)

	return assets

def getAsset(typ, subTyp, name, proj='nine'):
	assetPath = '%s/asset/%s/%s/%s' %(config.PROJ_DIR_DICT[proj][0], typ, subTyp, name)
	if not os.path.exists(assetPath):
		return None
	assetObj = Asset(assetPath)
	assetObj.load()
	return assetObj

def getShot(ch, seq, sh, proj='nine', mode=0):
	ch = 'ch%s' %str(ch).zfill(2)
	seq = 'seq%s' %str(seq).zfill(3)
	sh = 'sh%s' %str(sh).zfill(4)

	shotPath = '%s/scene/%s/%s/%s' %(config.PROJ_DIR_DICT[proj][mode], ch, seq, sh)
	shotObj = Shot(shotPath)
	shotObj.load()
	return shotObj

def getShotsInSeq(ch, seq, proj='nine', mode=0):
	ch = 'ch%s' %str(ch).zfill(2)
	seq = 'seq%s' %str(seq).zfill(3)

	seqPath = '%s/scene/%s/%s' %(config.PROJ_DIR_DICT[proj][mode], ch, seq)
	dirs = os.listdir(seqPath)

	shots = []
	for fol in sorted([d for d in dirs if re.match('sh[0-9][0-9][0-9][0-9]$', d) or re.match('s[0-9][0-9][0-9][0-9]$', d)]):
		shotPath = '%s/%s' %(seqPath, fol)
		if os.path.isdir(shotPath):
			shotObj = Shot(shotPath)
			shotObj.load()
			shots.append(shotObj)

	return shots

def getShotsInSeqFromPath(seqPath):
	dirs = os.listdir(seqPath)

	shots = []
	for fol in sorted([d for d in dirs if re.match('sh[0-9][0-9][0-9][0-9]$', d) or re.match('s[0-9][0-9][0-9][0-9]$', d)]):
		shotPath = os.path.join(seqPath, fol)
		if os.path.isdir(shotPath):
			shots.append(shotPath)

	return shots


def getShotPathsInSeq(ch, seq, proj='nine', drive=''):
	ch = 'ch%s' %str(ch).zfill(2)
	seq = 'seq%s' %str(seq).zfill(3)

	seqPath = '%s/%s/all/scene/%s/%s' %(drive, proj, ch, seq)
	dirs = os.listdir(seqPath)

	shots = []
	for fol in sorted([d for d in dirs if re.match('sh[0-9][0-9][0-9][0-9]$', d) or re.match('s[0-9][0-9][0-9][0-9]$', d)]):
		shotPath = '%s/%s' %(seqPath, fol)
		if os.path.isdir(shotPath):
			shots.append(shotPath)

	return shots

def getAllAssetInSeq(ch, seq, job, elem, proj='nine', search=''):
	import pprint
	shots = getShotsInSeq(ch, seq, proj=proj, mode=0)
	ret = {}
	for shot in shots:
		f = None
		try:
			f = shot.getLatestVersion(job, elem, check=False)
		except:
			continue
		if not os.path.exists(f):
			om.MGlobal.displayWarning('Path does not exists : %s' %f)
			continue

		refs = pipeTools.getRefInFile(filePath=f)
		if search:
			refs = [r for r in refs if search in r]
		ret[shot.sh] = refs
	pprint.pprint(ret)
	return ret

def assetPublishStd(mode='local', path=''):
	if not path:
		path = pm.sceneName()
	if not os.path.exists(path):
		return
	from nuTools.pipeline import igAssetPublish as igAssetPublish
	reload(igAssetPublish)

	mayaPyPath = config.MAYAPY_PATH.replace('/', '\\')
	scriptPath = igAssetPublish.__file__
	path = path.replace('/', '\\')

	command = '{0} {1} {2} {3}'.format(mayaPyPath, scriptPath, mode, path) 
	subprocess.Popen(command)

def assetPublish(mode='local', cleanup=True):
	mbElems = ['cache']

	# instance Asset Object
	asset = Asset(pm.sceneName())
	asset.load()

	both = False
	retPaths = []

	# get path
	if mode == 'local' or mode == 'both':
		heroPath = asset.getDeptHero(asset.job, asset.elem, False)
		print heroPath
		makeDir(asset.getDeptHeroDir(asset.job, False))
		try:
			asset.backupDeptHero(asset.job, asset.elem)
		except: pass

		if mode == 'both':
			both = True

	elif mode == 'library':
		heroPath = asset.getHero(asset.elem, False)
		makeDir(asset.getHeroDir(False))
		try:
			asset.backupHero(asset.elem)
		except: pass

	else:
		return

	hashStr = '#' * 25

	print '\n%s Asset Publish : %s %s' %(hashStr, mode, hashStr)
	print asset.inputpath

	# get local dir to create tmp file for reading
	heroPath = os.path.normpath(heroPath)

	# save as .mb if elem is cache
	if asset.elem in mbElems:
		heroPath = '%s.mb' %os.path.splitext(heroPath)[0]

	# do the operations and save current scene to local dir
	print '\nImporting references...'
	pipeTools.importAllRefs()

	print '\nRemoving all namespaces...'
	pipeTools.removeAllNameSpace()

	print '\nParenting pre-constrainted objects...'
	pipeTools.parentPreConsObj()

	if cleanup == False:
		pm.saveAs(heroPath, f=True)

	else:
		print '\nRemoving unused nodes...'
		try:
			mel.eval('MLdeleteUnused;')
		except Exception, error:
			print error

		pm.saveAs(heroPath, f=True)

		# do .ma remove operation on the temp file
		print '\nRe-writing Maya Ascii file...'
		pipeTools.removeUnusedNodeFromMaFile(heroPath)


	retPaths.append(heroPath)

	# if publish both, get library hero path and copy the tmp file to it
	if both == True:
		libHeroPath = os.path.normpath(asset.getHero(asset.elem, False))
		if asset.elem in mbElems:
			libHeroPath = '%s.mb' %os.path.splitext(libHeroPath)[0]
		makeDir(asset.getHeroDir(False))
		try:
			asset.backupHero(asset.elem)
		except: pass

		print '\nCopying to library...'
		shutil.copy2(heroPath, libHeroPath)
		retPaths.append(libHeroPath)

	# os.chmod(heroPath, stat.S_IREAD)
	# os.remove(tmpFilePath)
	# os.rmdir(tmpDir)

	# if re.match( "maya\\.exe", os.path.basename(sys.executable), re.I):
	# 	print '\nOpening Published File...'
	# 	pm.openFile(retPaths[-1], f=True)
	
	res = '\n'

	res += '\n'.join(retPaths)
	res += '\n%s Publish Complete %s\n' %(hashStr, hashStr)

	print res
	return heroPath

def moveCharAsset(srcTyp, srcSubTyp, srcName, desTyp, desSubTyp, desName):
	'''
		Move character from one to another. ie mrWong --> mrWongInjure
		The move includes - model_med, rig_def, rig_bodyRig, rig_addRig(optional), 
							rig_combRig(optional), rig_blendShape(optional)
	'''

	hStr = '#' * 25
	print '\n%s Moving  %s  to  %s %s' %(hStr, srcName, desName, hStr)

	# instance source asset object
	srcAsset = Asset('P:/nine/all/asset/%s/%s/%s' %(srcTyp, srcSubTyp, srcName))
	srcAsset.load()

	# instance destination asset object
	desAsset = Asset('P:/nine/all/asset/%s/%s/%s' %(desTyp, desSubTyp, desName))
	desAsset.load()

	print '\nAssets are, \nsource :\t\t%s\ndestination :\t%s' %(srcAsset.PATH, desAsset.PATH)

	# get source version files
	srcRigBodyRig = srcAsset.getLatestVersion('rig', 'bodyRig')
	srcRigDef = srcAsset.getLatestVersion('rig', 'def')
	srcModelMed = srcAsset.getLatestVersion('model', 'med')

	# get source hero files
	srcRigBodyRigHero = srcAsset.getDeptHero('rig', 'bodyRig')
	srcRigDefHero = srcAsset.getDeptHero('rig', 'def')
	srcModelMedHero = srcAsset.getDeptHero('model', 'med')

	srcRigBsh, srcRigAddRig, srcRigCombRig = None, None, None
	srcRigBshHero, srcRigAddRigHeor, srcRigCombRigHero = None, None, None
	hasBsh, hasAddRig = False, False
	try: 
		srcRigBsh = srcAsset.getLatestVersion('rig', 'blendShape', True)
		srcRigBshHero = srcAsset.getDeptHero('rig', 'blendShape', True)
		hasBsh = True
	except: 
		om.MGlobal.displayWarning('\nNo blendShape file',)

	srcRigAddRigHero, srcRigCombRigHero = None, None
	try: 
		srcRigAddRig = srcAsset.getLatestVersion('rig', 'addRig', True)
		srcRigCombRig = srcAsset.getLatestVersion('rig', 'combRig', True)
		srcRigAddRigHero = srcAsset.getDeptHero('rig', 'addRig', True)
		srcRigCombRigHero = srcAsset.getDeptHero('rig', 'combRig', True)
		hasAddRig = True
	except: 
		om.MGlobal.displayWarning('\nNo addRig & combRig file',)

	print '\nHero files are...'
	for f in [srcRigBodyRigHero, srcRigDefHero, srcRigBshHero, srcRigAddRigHero, srcRigCombRigHero, srcModelMedHero]:
		print f

	print '\nVersion files are...'
	for f in [srcRigBodyRig, srcRigDef, srcRigBsh, srcRigAddRig, srcRigCombRig, srcModelMed]:
		print f

	# create folders
	heroDir = desAsset.getHeroDir(False)
	rigDir = desAsset.getDeptDir('rig', False)
	rigHeroDir = desAsset.getDeptHeroDir('rig', False)
	rigVerDir = desAsset.getVersionDir('rig', False)
	modelDir = desAsset.getDeptDir('model', False)
	modelHeroDir = desAsset.getDeptHeroDir('model', False)
	modelVerDir = desAsset.getVersionDir('model', False)
	dirs = [heroDir, rigDir, rigHeroDir, rigVerDir, modelDir, modelHeroDir, modelVerDir]

	for d in dirs:
		if not os.path.exists(d):
			os.makedirs(d)
			print 'Folder created at :\t%s' %d

	print '\nCopying files...\n'
	# get version desitnation files
	desRigBsh, desAddRig, desCombRig = None, None, None
	desRigBodyRig = desAsset.getIncVersion('rig', 'bodyRig')
	desRigDef = desAsset.getIncVersion('rig', 'def')
	desModelMed = desAsset.getIncVersion('model', 'med')

	# get hero destination files
	desRigBodyRigHero = desAsset.getDeptHero('rig', 'bodyRig', False)
	desRigDefHero = desAsset.getDeptHero('rig', 'def', False)
	desModelMedHero = desAsset.getDeptHero('model', 'med', False)

	# copy hero files first
	shutil.copy(srcRigBodyRigHero, desRigBodyRigHero)
	shutil.copy(srcRigDefHero, desRigDefHero)
	shutil.copy(srcModelMedHero, desModelMedHero)

	# copy version files
	shutil.copy(srcRigBodyRig, desRigBodyRig)
	shutil.copy(srcRigDef, desRigDef)
	shutil.copy(srcModelMed, desModelMed)

	# try to replace model med in bodyRig version file
	try:
		fileTools.replace(desRigBodyRig, srcModelMedHero, desModelMedHero)
		print 'Reference for modelMed replaced.'
	except: pass

	# copy hero & version for optional files
	desRigBshHero = None
	if hasBsh == True:
		desRigBsh = desAsset.getIncVersion('rig', 'blendShape')
		desRigBshHero = desAsset.getDeptHero('rig', 'blendShape', False)

		shutil.copy(srcRigBsh, desRigBsh)
		shutil.copy(srcRigBshHero , desRigBshHero)

		# replace ref on version files
		fileTools.replace(desRigDef, srcRigBshHero, desRigBshHero)
		print 'Reference for rigDef replaced.'

	desRigAddRig, desRigCombRig, desRigAddRigHero, desRigCombRigHero = None, None, None, None
	if hasAddRig == True:
		desRigAddRig = desAsset.getIncVersion('rig', 'addRig')
		desRigCombRig = desAsset.getIncVersion('rig', 'combRig')
		desRigAddRigHero = desAsset.getDeptHero('rig', 'addRig', False)
		desRigCombRigHero = desAsset.getDeptHero('rig', 'combRig', False)

		shutil.copy(srcRigAddRig, desRigAddRig)
		shutil.copy(srcRigCombRig, desRigCombRig)
		shutil.copy(srcRigAddRigHero, desRigAddRigHero)
		shutil.copy(srcRigCombRigHero, desRigCombRigHero)

		# replace ref on version files
		fileTools.replace(desRigCombRig, srcRigAddRigHero, desRigAddRigHero)
		fileTools.replace(desRigCombRig, srcRigBodyRigHero, desRigBodyRigHero)

		print 'Reference for combRig replaced.'

	print '\nHero files are...'
	for f in [desRigBodyRigHero, desRigDefHero, desRigBshHero, desRigAddRigHero, desRigCombRigHero, desModelMedHero]:
		print f

	print '\nVersion files are...'
	for f in [desRigBodyRigHero, desRigDef, desRigBsh, desRigAddRig, desRigCombRig, desModelMed]:
		print f


	print '\n%s Character asset successfully moved. %s' %(hStr, hStr)

def moveCharAsset2(srcTyp, srcSubTyp, srcName, desTyp, desSubTyp, desName, rig=['def', 'bodyRig', 'addRig', 'combRig', 'blendShape'], mesh=['model', 'med']):
	'''
		Move character from one to another. ie mrWong --> mrWongInjure
		The move includes - model_med, rig_def, rig_bodyRig, rig_addRig(optional), 
							rig_combRig(optional), rig_blendShape(optional)
	'''

	hStr = '#' * 25
	print '\n%s Moving  %s  to  %s %s' %(hStr, srcName, desName, hStr)

	# instance source asset object
	srcAsset = Asset('P:/nine/all/asset/%s/%s/%s' %(srcTyp, srcSubTyp, srcName))
	srcAsset.load()

	# instance destination asset object
	desAsset = Asset('P:/nine/all/asset/%s/%s/%s' %(desTyp, desSubTyp, desName))
	desAsset.load()

	print '\nsource :\t\t%s\ndestination :\t%s' %(srcAsset.PATH, desAsset.PATH)

	# create folders
	heroDir = desAsset.getHeroDir(False)
	if not os.path.exists(heroDir):
		os.makedirs(heroDir)

	rigDir = desAsset.getDeptDir('rig', False)
	modelDir = desAsset.getDeptDir('model', False)
	shadingDir = desAsset.getDeptDir('shading', False)
	dirs = [rigDir, modelDir, shadingDir]
	depts = ['rig', 'model', 'shading']
	for directory, dept in zip(dirs, depts):
		if not os.path.exists(directory):
			os.makedirs(directory)
		hero_dir = desAsset.getDeptHeroDir(dept, False)
		if not os.path.exists(hero_dir):
			os.makedirs(hero_dir)
		version_dir = desAsset.getVersionDir(dept, False)
		if not os.path.exists(version_dir):
			os.makedirs(version_dir)

	print '\nCopying files...\n'
	# copy mesh
	if mesh:
		mesh_dept = mesh[0]
		mesh_elem = mesh[1]
		src_mesh_version_path = srcAsset.getLatestVersion(mesh_dept, mesh_elem)
		src_mesh_hero_path = srcAsset.getDeptHero(mesh_dept, mesh_elem)
		des_mesh_version_path = desAsset.getIncVersion(mesh_dept, mesh_elem)
		des_mesh_hero_path = desAsset.getDeptHero(mesh_dept, mesh_elem, False)
		shutil.copy(src_mesh_version_path, des_mesh_version_path)
		shutil.copy(src_mesh_hero_path, des_mesh_hero_path)

	# copy rig
	for elem in rig:
		try:
			src_hero_path = srcAsset.getDeptHero('rig', elem)
			des_hero_path = desAsset.getDeptHero('rig', elem, False)
			shutil.copy(src_hero_path, des_hero_path)
		except:
			pass

		try:
			src_version_path = srcAsset.getLatestVersion('rig', elem)
			des_version_path = desAsset.getIncVersion('rig', elem)
			shutil.copy(src_version_path, des_version_path)
		except:
			continue

		# try to replace ref to new asset in the new version file
		refs = pipeTools.getRefInFile(des_version_path)
		for ref in refs:
			pathsplit = ref.split('/')
			pathsplit[4] = desTyp
			pathsplit[5] = desSubTyp
			pathsplit[6] = desName
			old_filename = pathsplit[-1]
			pathsplit[-1] = old_filename.replace(srcName, desName)
			new_asset_ref = '/'.join(pathsplit)
			if os.path.exists(new_asset_ref) == True:
				fileTools.replace(des_version_path, ref, new_asset_ref)
				print '\nReference replaced in rig_%s\n\t%s\n\t%s' %(elem, ref, new_asset_ref)

	print '\n%s Character asset successfully moved. %s' %(hStr, hStr)

def replaceRefInSceneFile(ch, seq, sh, pairDict, job, elem='master', srcHero='med', desHero='med'):
	'''
		pairDict = { oldAssetName: newAssetName, ...}
	'''
	hStr = '#' * 25

	# get file path
	igShot = getShot(ch, seq, sh)
	latestAnimPath = igShot.getLatestVersion(job, elem)
	incAnimPath = igShot.getIncVersion(job, elem)

	
	# get asset hero file path as pair {oldPath: newPath}
	pairPathDict = {}
	for a, b in pairDict.iteritems():
		try:
			aPath = findAssetPath(a)
			aObj = Asset(aPath)
			aObj.load()
			aHeroPath = aObj.getHero(srcHero)
		except: 
			om.MGlobal.displayWarning('\nCannot get hero file path for: %s_%s' %(a, srcHero))
			continue

		try:
			bPath = findAssetPath(b)
			bObj = Asset(bPath)
			bObj.load()
			bHeroPath = bObj.getHero(desHero)
		except: 
			om.MGlobal.displayWarning('Cannot get hero file path for: %s_%s' %(b, desHero))
			continue

		pairPathDict[aHeroPath] = bHeroPath

	# copy to new version
	shutil.copy(latestAnimPath, incAnimPath)

	# replace refs
	fileTools.replaceRefFromMaFile(incAnimPath, pairPathDict)

	print '\n%s Replace Reference Complete %s' %(hStr, hStr)
	print '%s\n' %incAnimPath
	olds = fileTools.getRefInfoFromMaFile(latestAnimPath, printResult=False)
	news = fileTools.getRefInfoFromMaFile(incAnimPath, printResult=False)

	for old, new in zip(olds, news):
		if old != new:
			print 'OLD: %s' %old[0]
			print 'NEW: %s' %new[0]
			print '%s  --->   %s' %(old[1], new[1])
			print '%s  --->   %s\n' %(old[2], new[2])

def getShotCam(shot=None, returnName=False):

	defcam = ['perspShape', 'topShape', 'frontShape', 'sideShape']
	cams = [c for c in pm.ls(type='camera') if c.nodeName() not in defcam]
	if not shot:
		shot = Shot(path=pm.sceneName())
		shot.load()
		
	currShotCode = '%s%s%s' %(shot.ch, shot.seq, shot.sh)
	ret = None
	shotCams = {}
	for cam in cams:
		camtr = cam.getParent()
		try:
			ch0 = camtr.ChapX0.get()
			ch1 = camtr.Chap0X.get()
			sq0 = camtr.SEQX00.get()
			sq1 = camtr.SEQ0X0.get()
			sq2 = camtr.SEQ00X.get()
			sh0 = camtr.sX000.get()
			sh1 = camtr.s0X00.get()
			sh2 = camtr.s00X0.get()
			sh3 = camtr.s000X.get()
			attrShotCode = 'ch%s%sseq%s%s%ssh%s%s%s%s' %(ch0,ch1,sq0,sq1,sq2,sh0,sh1,sh2,sh3)
			shotCams[attrShotCode] = cam
		except:
			pass
	
	if shotCams:
		if len(shotCams) > 1:
			for code, cam in shotCams.iteritems():
				if currShotCode == attrShotCode:
					ret = cam
		else:
			ret = shotCams.values()[0]

	if not ret:
		closeMatches = []
		camNames = [cam.nodeName() for cam in cams]
		closeMatches = difflib.get_close_matches(currShotCode, camNames, 20)
		if closeMatches:
			ret = [i for i in cams if i.nodeName()==closeMatches[0]][0]
		else:
			if cams:
				ret = cams[0]
			else:
				ret = pm.PyNode('perspShape')
	if returnName == True:
		ret = ret.longName()
	return ret

def createBakedCamera(cam=None, bakedCamName=''):
	shotCamShp = None
	shotCamTr = None

	# if the arg not passed
	if not cam:
		try: # get selection of the camera transform
			shotCamTr = pm.ls(sl=True, type='transform')[0]
			shotCamShp = shotCamTr.getShape(type='camera')
		except:
			pass

		if not shotCamShp or not shotCamTr:
			try:
				shotCamShp = getShotCam()
				shotCamTr = shotCamShp.getParent()
			except:
				return
	else: # user pass the arg as camera transform
		try:
			shotCamTr = cam
			shotCamShp = cam.getShape(type='camera')
		except:
			return
	
	if not bakedCamName:
		bakedCamName = 'baked_%s' %(shotCamTr.nodeName().split(':')[-1])

	try:
		if pm.objExists(bakedCamName):
			pm.delete(bakedCamName)
	except:
		pass

	bakedCamTr = shotCamTr.duplicate(rr=True, n=bakedCamName)[0]
	misc.unlockChannelbox(obj=bakedCamTr, heirachy=False)
	bakedCamShp = bakedCamTr.getShape()

	if bakedCamTr.getParent():
		pm.parent(bakedCamTr, w=True)
	
	minTime = pm.playbackOptions(q=True, min=True)
	maxTime = pm.playbackOptions(q=True, max=True)

	animCurves = pm.listConnections(shotCamShp, s=True, type='animCurve')
	attrs = []
	if animCurves:
		for animCurve in animCurves:
			attr = pm.listConnections(animCurve, d=True, p=True )[0]
			attrName = attr.attrName()
			pm.copyKey(shotCamShp, attribute=attrName)
			pm.pasteKey(bakedCamShp, attribute=attrName)
	
	parCons = pm.parentConstraint(shotCamTr, bakedCamTr)

	pm.refresh(suspend=True)

	pm.bakeResults(bakedCamTr, simulation=True, t=(minTime, maxTime))

	pm.refresh(suspend=False)
	pm.refresh(force=True)

	pm.delete(parCons)
	pm.select(bakedCamTr, r=True)

	return bakedCamTr

def fixFileTexturePath() :
	fileNodes = ['%s.fileTextureName' %node for node in mc.ls(type='file')]
	assetObj = Asset()
	assetObj.load()

	# check and create texture and texture/hi dir
	textureDir = assetObj.getDeptHeroDir(job='texture', check=False)
	if not os.path.exists(textureDir):
		os.makedirs(textureDir)
	textureHiDir = assetObj.getTextureHeroResDir(res='hi', check=False)
	if not os.path.exists(textureHiDir):
		os.makedirs(textureHiDir)

	# result = mc.confirmDialog(title='Confirm', 
	# 						message='All texture files will be moved to:\n%s' % textureHiDir, 
	# 						button=['Yes','No'], defaultButton='Yes',
	# 						cancelButton='No', dismissString='No')
	# if result == 'Yes' :
	getAttr = mc.getAttr
	pathExists = os.path.exists
	popen = subprocess.Popen
	des = os.path.normpath(textureHiDir)
	# i = 0
	# progress bar
	gMainProgressBar = mel.eval('$tmp = $gMainProgressBar');
	mc.progressBar(gMainProgressBar, e=True, beginProgress=True, 
				isInterruptable=True, status='Moving texture files : %s' %textureHiDir, maxValue=len(fileNodes) )

	for fileNode in fileNodes:
		if mc.progressBar(gMainProgressBar, query=True, isCancelled=True ):
			break

		source = getAttr(fileNode)
		src = os.path.normpath(source)
		fileExt = os.path.basename(src)
		if pathExists(src):
			cmd = 'xcopy %s %s /k/i/y' %(src, des)
			xcopy = popen(cmd, shell=True)
			xcopy.wait()
			newFilePath = '%s/%s' % (textureHiDir, fileExt)
			mc.setAttr(fileNode, newFilePath, type='string')
			# print 'Moved(%s/%s)\n\t%s\n\t%s\\%s' %(i, filenum, src, des, fileExt)
		else:
			om.MGlobal.displayError('Texture does not exists : %s' %src)

		mc.progressBar(gMainProgressBar, edit=True, step=1)
	mc.progressBar(gMainProgressBar, edit=True, endProgress=True)

def genAllTextureRes():
	from PIL import Image
	exceptions = ['.exr', '.hdr']

	assetObj = Asset()
	assetObj.load()
	textureDir = assetObj.getDeptHeroDir(job='texture', check=False)
	if not os.path.exists(textureDir):
		os.makedirs(textureDir)
	textureDirs = assetObj.getTextureHeroResDirs(check=False)
	for txdir in textureDirs:
		if not os.path.exists(txdir):
			os.makedirs(txdir)

	textureHiDir = textureDirs[0]
	cleanedPath = os.path.normpath(textureHiDir)
	resdirDict = {1.0:textureDirs[0], 0.5:textureDirs[1], 0.25:textureDirs[2], 0.025:textureDirs[3]}
	files = [f for f in os.listdir(cleanedPath) if os.path.isfile(os.path.join(cleanedPath, f))]
	# progress bar
	gMainProgressBar = mel.eval('$tmp = $gMainProgressBar');
	mc.progressBar(gMainProgressBar, e=True, beginProgress=True, 
				isInterruptable=True, status='Resizing texture files : %s' %textureHiDir, maxValue=len(files)*3)
	for fl in files:
		if mc.progressBar(gMainProgressBar, query=True, isCancelled=True ):
			break

		fileName , fileExt = os.path.splitext(fl)
		if fileExt in exceptions:
			mc.progressBar(gMainProgressBar, edit=True, step=1)
			continue
			
		imgFilePath = os.path.join(cleanedPath, fl)
		oW, oH = 0, 0
		imgObj = None
		try:
			imgObj = Image.open(imgFilePath)
			oW, oH = imgObj.size
		except Exception as e:
			print e
			om.MGlobal.displayError('PIL open failed : %s' %imgFilePath)
			mc.progressBar(gMainProgressBar, edit=True, step=1)
			continue

		for mult in [0.5, 0.25, 0.025]:
			nW, nH = int(oW*mult ), int(oH*mult)
			outputImgPath = '%s\\%s' %(os.path.normpath(resdirDict[mult]), fl)
			try:
				imgObj.thumbnail((nW, nH), Image.ANTIALIAS)
				imgObj.save(outputImgPath)
				# print 'Resize Success : %s' %outputImgPath
			except Exception as e:
				print e
				om.MGlobal.displayError('Resize Failed : %s' %outputImgPath)
			mc.progressBar(gMainProgressBar, edit=True, step=1)

	mc.progressBar(gMainProgressBar, edit=True, endProgress=True)

def blendshapeGeoGrp(job=''):
	if not job:
		job = 'Med'
		if misc.checkMod('ctrl') == True:
			job = 'Vray'
	geoGrp, geoRes = None, None
	try:
		geoGrp = pm.ls('*:geo_grp')[0]
		geoRes = pm.ls('*:geo%s_grp' %job)[0]
	except:
		pass
	ret = None
	if geoGrp and geoRes:
		geoGrp.visibility.set(False)
		ret = pm.blendShape(geoGrp, geoRes, origin='world', before=True, weight=(0, 1.0), n='geo%s_bsh' %job)
	else:
		om.MGlobal.displayError('Cannot find geo_grp or geo%s_grp.' %job)
	return ret

def createRigGrp():
	res = False

	rigGrp = None
	rigGrps = pm.ls('|rig_grp', l=True)
	if rigGrps:
		rigGrp = rigGrps[0]
		print 'The scene already has a rig_grp, will parent %s under it.' %grp
	else:
		rigGrp = pm.group(em=True, n='rig_grp')

	cams = ['persp', 'top', 'front', 'side']
	lsgrps = [a for a in pm.ls(assemblies=True) if a.nodeName() not in cams and a.nodeName()!='rig_grp']
	wstr = [0, 0, 0]
	for grp in lsgrps:
		tr = pm.xform(grp, q=True, ws=True, t=True)
		ro = pm.xform(grp, q=True, ws=True, ro=True)

		if tr != wstr and ro != wstr:

			print '%s transform values is not at [0, 0, 0], fixing.' %grp
			try:
				pm.xform(grp, ws=True, t=wstr)
				pm.xform(grp, ws=True, ro=wstr)
			except:
				print 'Cannot fix transform values for %s.' %grp
				return res

		pm.parent(grp, rigGrp)
		print 'Parented : %s --> %s' %(grp, rigGrp)

	if len([a for a in pm.ls(assemblies=True) if a.nodeName() not in cams]) != 1:
		return res

	# clean up. unlock normals, delete history
	geos = [geo for geo in pm.ls(type='transform') if misc.checkIfPly(geo)==True]
	pm.polyNormalPerVertex(geos, ufn=True)
	pm.delete(geos, ch=True)

	return True

def writeGeoData():
	cams = ['persp', 'top', 'front', 'side']
	topGrps = [a for a in pm.ls(assemblies=True) if a.nodeName() not in cams]
	if not topGrps:
		return False

	rigGrp = topGrps[0]
	children = [c.longName() for c in rigGrp.getChildren(ad=True)]
	children.insert(0, rigGrp.longName())
	text = '\n'.join(children)

	# write to file
	wFileH, wFilePath = mkstemp()
	wFile = open(wFilePath,'w')
	wFile.writelines(text)

	# close file
	wFile.close()
	os.close(wFileH)

	return wFilePath

def convertAllVrayMtlToLambert():
	vrayShds = mc.ls(type=['VRayMtl', 'VRayFastSSS2'])
	for shd in vrayShds:
		sg = mc.listConnections('%s.outColor' %shd)[0]
		fileNode = ''
		attr = 'color'
		if mc.nodeType(shd) == 'VRayFastSSS2':
			attr = 'diffuseTex'
		if mc.objExists('%s.%s'% (shd, attr)):
			fileNodes = mc.listConnections('%s.%s' %(shd, attr))
			if fileNodes :
				fileNode = fileNodes[0]
		if mc.objExists('%s.surfaceShader' %sg):
			lambert = mc.shadingNode('lambert', asShader=True)
			mc.connectAttr('%s.outColor' %lambert, '%s.surfaceShader' %sg, f=True)
			if fileNode:
				mc.connectAttr('%s.outColor' %fileNode, '%s.color' %lambert, f=True)
			
			mc.delete(shd)
			mc.rename(lambert, shd)
			print 'Converted : %s' %shd
		else:
			print 'Cannot convert to lambert : %s' %shd


def getAllMovInSeq(ch, seq, jobs=[], proj='nine', mode=0):
	shots = getShotsInSeq(ch=ch, seq=seq, proj=proj, mode=mode)

	mov_paths = {}
	for shot in shots:
		mov_paths[shot] = []
		if not jobs:
			jobs = shot.getExistingJobDirs()
		# print '\n%s' %shot.sh
		mov_dict = {}
		for job in jobs:
			output_path = shot.getDeptOutputDir(job=job, check=False)
			if os.path.exists(output_path):
				movs = [f for f in os.listdir(output_path) if os.path.isfile('%s/%s' %(output_path, f)) and os.path.splitext(f)[-1] == '.mov']
				if movs:
					for mov in sorted(movs):
						mov_paths[shot].append('%s/%s' %(output_path, mov))

	return mov_paths # {shot:[path1, path2, ...]}
