import sys, os, shutil


def createCacheHero(openpath, savepath):
	import pymel.core as pm
	from nuTools.pipeline import iglooPipeline
	reload(iglooPipeline)

	resStrDict = {True:'SUCCESS', False:'FAILED'}
	res = False
	pm.openFile(openpath, f=True)
	tmpDataPath, txtSavepath = '', ''
	try:
		res = iglooPipeline.createRigGrp()
		dirname = os.path.dirname(savepath)
		txtSavepath = '%s\\cacheGeo.txt' %(dirname)
		if res == True:
			tmpDataPath = iglooPipeline.writeGeoData()
	except Exception, e:
		print e
		raw_input()

	print '\n\nRESULT : %s' %(resStrDict[res])
	userInput = 'y'
	if res == True:
		print '%s\n%s\nSave Scene? (y/n)' %(savepath, txtSavepath)
		userInput = raw_input()

		if userInput == 'y':
			pm.saveAs(savepath, f=True)
			shutil.move(tmpDataPath, txtSavepath)
	else:
		print 'Please check asset file.'
		userInput = raw_input()


if __name__ == "__main__": 
	createCacheHero(sys.argv[1], sys.argv[2])