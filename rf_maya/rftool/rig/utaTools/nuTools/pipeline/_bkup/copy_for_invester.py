import sys, os, shutil, subprocess
sys.path.append('P:/sysTool/python')
from nuTools.pipeline import iglooPipelineOs
reload(iglooPipelineOs)

IMG_FORMATS = ('.dpx', '.tif', '.tiff')

def log(path, text):
	with open(path, 'a') as f:
		f.write('\n%s' %text)


def copyRenderImage_out(seqPath, job, dest, log_path):
	dest = dest.replace('"', '')
	dest = dest.replace('/', '\\')
	dest = os.path.normpath(dest)

	# log the header
	header = '------------------------------------------'
	header += '\n------------------------------------------'
	header += '\nseq_path : %s' %seqPath
	header += '\njob : %s' %job
	header += '\ndest : %s' %dest
	header += '\nlog_path : %s' %log_path
	header += '\n------------------------------------------'
	header += '\n------------------------------------------'
	log(log_path, header)

	if not os.path.exists(dest):
		err = 'ERROR: Destination path do not exists : %s' %dest
		print err
		log(log_path, err)
		return

	shotPaths = iglooPipelineOs.getShotsInSeqFromPath(seqPath=seqPath)

	if not shotPaths:
		err = '\nERROR: Cannot get shot(s) from %s' %(shotPaths)
		print err
		log(log_path, err)
		return

	curr_sh = 0
	num_shots = len(shotPaths)

	sl = '---------- SHOT LIST ----------\n'
	sl += '\n'.join(shotPaths)
	sl += '\n%s shots found, Proceed? (y/n)' %num_shots
	print sl
	log(log_path, sl)

	answer = raw_input()
	if answer != 'y':
		return
	log(log_path, answer)
	
	seq_name = os.path.split(seqPath)[-1]
	all_size = 0.0
	for path in shotPaths:
		# print '\n'
		ospath = os.path.normpath(path)
		shotName = os.path.split(ospath)[-1]
		# get hero dir first
		heroDir = os.path.join(ospath, job, 'hero')
		if not os.path.exists(heroDir):
			err = 'Cannot get department directory : %s' %heroDir
			print err
			log(log_path, err)
			continue

		inside_hero = [f for f in os.listdir(heroDir) if os.path.isdir(os.path.join(heroDir, f)) and f.endswith('_%s' %job)]
		if not inside_hero:
			err = 'Cannot find folder with _%s' %job
			print err
			log(log_path, err)
			continue


		imageFolderName = inside_hero[0]
		imageFolder = os.path.join(heroDir, imageFolderName)

		files = [f for f in os.listdir(imageFolder) if os.path.isfile(os.path.join(imageFolder, f)) and os.path.splitext(f)[-1] in IMG_FORMATS]
		if not files:
			err = 'WARNING: No image found in %s' %imageFolder
			print err
			log(log_path, err)

		# get/create destination dir
		destShotFolder = os.path.join(dest, shotName)
		if not os.path.exists(destShotFolder):
			os.makedirs(destShotFolder)
			fc = 'Folder created : %s' %destShotFolder
			print fc
			log(log_path, fc)

		destJobFolder = os.path.join(destShotFolder, job)
		if not os.path.exists(destJobFolder):
			os.makedirs(destJobFolder)
			fc = 'Folder created : %s' %destJobFolder
			print fc
			log(log_path, fc)

		destHeroFolder = os.path.join(destJobFolder, 'hero')
		if not os.path.exists(destHeroFolder):
			os.makedirs(destHeroFolder)
			fc = 'Folder created : %s' %destHeroFolder
			print fc
			log(log_path, fc)

		destFolder = os.path.join(destHeroFolder, imageFolderName)
		if not os.path.exists(destFolder):
			os.makedirs(destFolder)
			fc = 'Folder created : %s' %destFolder
			# print fc
			log(log_path, fc)

		num_files = len(files)
		percent = 0.0
		img_seq_size = sum([os.path.getsize(os.path.join(imageFolder, f)) for f in files])
		# print '-----%s Size: %s-----' %(imageFolderName, img_seq_size)
		all_size += img_seq_size

		# print '-----Copying from %s-----' %(imageFolder)
		for f in files:
			src_path = os.path.join(imageFolder, f)
			dest_path = os.path.join(destFolder, f)
			# shutil.copy(fp, destFolder)
			copy_cmd = ["copy", "%s" %src_path, "%s" %dest_path, "/y", ">nul"]
			xcopy = subprocess.call(copy_cmd, shell=True)
			# percent += (1.0/num_files)*100
			# percentStr = '%.2f' %percent
			# pg = 'copying %s\t(%s %%)   ' %(shotName, percentStr)
			# print pg
			# log(log_path, pg)

		curr_sh += 1
		sc = 'copied %s/%s (%.2d%%)' %(curr_sh, num_shots, (float(curr_sh)/float(num_shots))*100.0)
		print sc
		log(log_path, sc)

	ts = 'TOTAL SIZE of %s = %s (%s mb.)' %(seq_name, all_size, all_size/(1024*1024.0))
	print ts
	log(log_path, ts)

if __name__ == '__main__': 
	print '\nSeq Path?'
	seqPath = raw_input()

	print '\nJob?'
	job = raw_input()

	print '\nCopy destination?'
	dest = raw_input()

	print '\nLog path?'
	log_path = raw_input()

	# do it
	copyRenderImage_out(os.path.normpath(str(seqPath)), str(job), os.path.normpath(str(dest)), os.path.normpath(str(log_path)))