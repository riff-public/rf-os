import os, sys
import collections, re, subprocess, shutil

sys.path.append('P:\\sysTool\\python')
from nuTools import config
reload(config)

MAIN_DRIVE = config.MAIN_DRIVE

class Entity(object):
	def __init__(self, path):
		self.splits = []
		self.fileext = ''  # file with ext

		self.inputpath = path
		self.file = ''   # just the file
		self.ext = ''	# just the ext

		self.drive = ''
		self.proj = ''
		self.job = ''
		self.elem = ''
		self.name = ''

		self.PATH = ''
		self.FILEPATH = ''

	def splitPath(self):
		self.checkPathExist(self.inputpath)
		path = os.path.normpath(self.inputpath)
		path = path.replace('\\', '/')

		for proj in config.PROJ_DIR_DICT.keys():
			psplit = '/%s/' %proj
			if psplit in path:
				drive_splits = path.split(psplit)
				self.splits = [drive_splits[0], proj]
				self.splits.extend(psplit.join(drive_splits[1:]).split('/'))

				self.drive = self.splits[0]
				self.proj = self.splits[1]
				self.entityType = self.splits[3]
				break

		# self.splits = path.split('/')
		# self.drive = self.splits[0]
		# self.proj = self.splits[1]
		# self.entityType = self.splits[3]

		if len(self.splits) < 7:
			sys.exit('Entity does not exists!')

	def checkPathExist(self, path, check=True):
		if not os.path.exists(path):
			if check == True:
				sys.exit('%s  : Does not exists!' %path)
			else:
				return False
		return path

	def getLatestVersionNumInPath(self, path, inc=False):
		exts = ['.ma', '.mb']
		files = [ f for f in os.listdir( path ) if os.path.splitext(f)[-1] in exts]

		numList = []
		version = '001'
		try :
			for f in files :	
				vre = re.search(r"(_v\d\d\d.)", f)
				if vre:
					v = vre.group()
					numList.append(int(v[2:-1]))

			ver = max(numList)

			if inc == True:
				ver += 1
			version = str(ver).zfill(3)
		except :
			pass 

		return version

	def getLatestVersionNum(self, job, elem, inc=False):
		exts = ['.ma', '.mb']
		path = self.getVersionDir(job)
		files = [ f for f in os.listdir( path ) if os.path.splitext(f)[-1] in exts and '_%s_%s_' %(job, elem) in f ]

		numList = []
		version = '001'
		try :
			for f in files :	
				vre = re.search(r"(_v\d\d\d.)", f)
				if vre:
					v = vre.group()
					numList.append(int(v[2:-1]))

			ver = max(numList)

			if inc == True:
				ver += 1
			version = str(ver).zfill(3)
		except :
			pass 

		return version

	def getDeptDir(self, job, check=True):
		self.checkPathExist(self.PATH)
		path = '%s/%s' %(self.PATH, job)

		self.checkPathExist(path, check)
		return path

	def getOutsourceDeptDir(self, job='', check=True):
		self.checkPathExist(self.PATH)
		if job:
			job = '_%s' %job
		path = '%s/outsource_%s_%s_%s%s' %(self.PATH, self.ch, self.seq, self.sh, job)

		self.checkPathExist(path, check)
		return path

	def getVersionDir(self, job, check=True):
		self.checkPathExist(self.PATH)
		path = '%s/%s/version' %(self.PATH, job)

		self.checkPathExist(path, check)
		return path

	def getVersionFiles(self, job, elem=''):
		self.checkPathExist(self.PATH)
		path = '%s/%s/version' %(self.PATH, job)
		exts = ['.ma', '.mb']
		allFiles = [ f for f in os.listdir(path ) if os.path.splitext(f)[-1] in exts ]
		if elem:
			elem = '_%s_' %elem

		files = {}
		for f in allFiles:
			if '_%s%s' %(job, elem) in f:
				files[f] = '%s/%s' %(path, f)

		return files

	def getExistingJobDirs(self):
		self.checkPathExist(self.PATH)
		dirs = [d for d in os.listdir(self.PATH) if os.path.isdir('%s/%s' %(self.PATH, d))]
		return dirs

	def getDeptOutputDir(self, job, check=True):
		deptDir = self.getDeptDir(job, check=check)
		path = '%s/output' %(deptDir)

		self.checkPathExist(path, check)
		return path 

	def getDeptHeroDir(self, job, check=True):
		path = '%s/%s/hero' %(self.PATH, job)
		self.checkPathExist(path, check)

		return path

	def getHeroDir(self, check=True):
		path = '%s/hero' %(self.PATH)
		self.checkPathExist(path, check)

		return path

class Shot(Entity):
	def __init__(self, path=''):
		Entity.__init__(self, path)

		self.ch = ''
		self.seq = ''
		self.sh = ''


	def load(self):
		self.checkPathExist(self.inputpath, True)

		try:
			self.getShotData()
		except: 
			print 'Failed getting shot data : %s' %self.inputpath
			return

		if os.path.isfile(self.inputpath) == True:
			try:
				self.getShotFileData()
			except: 
				print 'Failed getting shot file data : %s' %self.inputpath
				return


	def getShotData(self):
		self.splitPath()

		self.ch = self.splits[4]
		self.seq = self.splits[5]
		self.name = self.splits[6]
		self.sh = self.name

		indexSplits = len(self.splits)
		self.PATH = '/'.join(self.splits[:7])

	def getShotFileData(self):
		self.fileext = os.path.basename(self.inputpath)
		extSplits = os.path.splitext(self.fileext)

		self.file = extSplits[0]
		self.ext = extSplits[1]

		self.job = self.splits[7]

		self.FILEPATH = self.inputpath

		verNum = ''
		elem = self.file

		verSearch = re.search(r"(_v\d\d\d.)", self.fileext)
		if verSearch:
			verNum = verSearch.group()
			elem = self.fileext.split(verNum)[0]

		self.elem = elem.split('_')[-1]	

	def getDeptHero(self, job, elem, check=True):
		path = '%s/%s/hero/%s_%s_%s_%s_%s.ma' %(self.PATH, job, self.ch, self.seq, self.sh, job, elem)
		self.checkPathExist(path, check)

		return path

	def getHero(self, elem, check=True):
		path = '%s/hero/%s_%s_%s_%s.ma' %(self.PATH, self.ch, self.seq, self.sh, elem)
		self.checkPathExist(path, check)

		return path
	
	def backupDeptHero(self, job, elem):
		self.checkPathExist(self.PATH)

		deptHeroFile = self.getDeptHero(job, elem, True)
		
		deptHeroDir = self.getDeptHeroDir(job, True)
		backupFolderPath = '%s/.%s_%s_%s_%s_%sBackup' % (deptHeroDir, self.ch, self.seq, self.sh, job, elem)
	
		if not os.path.exists(backupFolderPath) :
			os.makedirs(backupFolderPath)
		setDirHidden(backupFolderPath)
		
		versionNumStr = self.getLatestVersionNumInPath(backupFolderPath, inc=True)
		path = '%s/%s_%s_%s_%s_%s_v%s.ma' %(backupFolderPath, self.ch, self.seq, self.sh, job, elem, versionNumStr)
		shutil.copy2(deptHeroFile, path)

		return path

	def backupHero(self, elem):
		self.checkPathExist(self.PATH)

		heroFile = self.getHero(elem, True)

		heroDir = self.getHeroDir(True)
		backupFolderPath = '%s/.%s_%s_%s_%sBackup' % (heroDir, self.ch, self.seq, self.sh, elem)
	
		if not os.path.exists(backupFolderPath) :
			os.makedirs(backupFolderPath)
		setDirHidden(backupFolderPath)
		
		versionNumStr = self.getLatestVersionNumInPath(backupFolderPath, inc=True)
		path = '%s/%s_%s_%s_%s_v%s.ma' %(backupFolderPath, self.ch, self.seq, self.sh, elem, versionNumStr)
		shutil.copy2(heroFile, path)

		return path

	def getLatestVersion(self, job, elem, check=True):
		self.checkPathExist(self.PATH)

		versionPath = self.getVersionDir(job)
		versionNumStr = self.getLatestVersionNum(job, elem, inc=False)
		path = '%s/%s_%s_%s_%s_%s_v%s.ma' %(versionPath, self.ch, self.seq, self.sh, job, elem, versionNumStr)

		self.checkPathExist(path, check)
		return path

	def getIncVersion(self, job, elem):
		self.checkPathExist(self.PATH)

		versionPath = self.getVersionDir(job)
		versionNumStr = self.getLatestVersionNum(job, elem, inc=True)
		path = '%s/%s_%s_%s_%s_%s_v%s.ma' %(versionPath, self.ch, self.seq, self.sh, job, elem, versionNumStr)

		return path

class Asset(Entity):
	def __init__(self, path=''):
		Entity.__init__(self, path)

		self.type = ''
		self.subType = ''
		self.asset = ''

	def load(self):
		self.checkPathExist(self.inputpath, True)

		try:
			self.getAssetData()
		except: 
			print 'Failed getting asset data : %s' %self.inputpath
			return

		if os.path.isfile(self.inputpath) == True:
			try:
				self.getAssetFileData()
			except: 
				print 'Failed getting asset file data : %s' %self.inputpath
				return

	def getAssetData(self):
		self.splitPath()
		self.type = self.splits[4]
		self.subType = self.splits[5]
		self.name = self.splits[6]
		self.asset = self.name

		indexSplits = len(self.splits)
		self.PATH = '/'.join(self.splits[:7])
		
	def getAssetFileData(self):
		self.fileext = os.path.basename(self.inputpath)
		extSplits = os.path.splitext(self.fileext)

		self.file = extSplits[0]
		self.ext = extSplits[1]

		self.job = self.splits[7]

		self.FILEPATH = self.inputpath

		verNum = ''
		elem = self.file

		verSearch = re.search(r"(_v\d\d\d.)", self.fileext)
		if verSearch:
			verNum = verSearch.group()
			elem = self.fileext.split(verNum)[0]

		self.elem = elem.split('_')[-1]		

	def getLatestVersion(self, job, elem, check=True):
		self.checkPathExist(self.PATH)

		versionPath = self.getVersionDir(job)
		versionNumStr = self.getLatestVersionNum(job, elem, inc=False)
		path = '%s/%s_%s_%s_v%s.ma' %(versionPath, self.asset, job, elem, versionNumStr)

		self.checkPathExist(path, check)
		return path

	def getIncVersion(self, job, elem):
		self.checkPathExist(self.PATH)

		versionPath = self.getVersionDir(job)
		versionNumStr = self.getLatestVersionNum(job, elem, inc=True)
		path = '%s/%s_%s_%s_v%s.ma' %(versionPath, self.asset, job, elem, versionNumStr)

		return path

	def getDeptHero(self, job, elem, check=True):
		path = '%s/%s/hero/%s_%s_%s.ma' %(self.PATH, job, self.asset, job, elem)
		self.checkPathExist(path, check)

		return path

	def getHero(self, elem, check=True):
		path = '%s/hero/%s_%s.ma' %(self.PATH, self.asset, elem)
		self.checkPathExist(path, check)

		return path
		
	def getTextureHeroResDir(self, res, check=True):
		path = self.getDeptHeroDir(job='texture', check=check)
		resDir = '%s/%s' %(path, res)
		self.checkPathExist(resDir, check)

		return resDir

	def getTextureHeroResDirs(self, check=True):
		path = self.getDeptHeroDir(job='texture', check=check)
		rets = []
		for res in ['hi', 'med', 'low', 'rig']:
			resPath = '%s/%s' %(path, res)
			self.checkPathExist(path, check)
			rets.append(resPath)

		return rets

	def backupDeptHero(self, job, elem):
		self.checkPathExist(self.PATH)

		deptHeroFile = self.getDeptHero(job, elem, True)
		
		deptHeroDir = self.getDeptHeroDir(job, True)
		backupFolderPath = '%s/.%s_%s_%sBackup' % (deptHeroDir, self.asset, job, elem)
	
		if not os.path.exists(backupFolderPath) :
			os.makedirs(backupFolderPath)
		setDirHidden(backupFolderPath)
		
		versionNumStr = self.getLatestVersionNumInPath(backupFolderPath, inc=True)
		path = '%s/%s_%s_%s_v%s.ma' %(backupFolderPath, self.asset, job, elem, versionNumStr)
		shutil.copy2(deptHeroFile, path)

		return path

	def backupHero(self, elem):
		self.checkPathExist(self.PATH)

		heroFile = self.getHero(elem, True)

		heroDir = self.getHeroDir(True)
		backupFolderPath = '%s/.%s_%sBackup' % (heroDir, self.asset, elem)
	
		if not os.path.exists(backupFolderPath) :
			os.makedirs(backupFolderPath)
		setDirHidden(backupFolderPath)
		
		versionNumStr = self.getLatestVersionNumInPath(backupFolderPath, inc=True)
		path = '%s/%s_%s_v%s.ma' %(backupFolderPath, self.asset, elem, versionNumStr)
		shutil.copy2(heroFile, path)

		return path

	def getLatestVersion(self, job, elem, check=True):
		self.checkPathExist(self.PATH)

		versionPath = self.getVersionDir(job)
		versionNumStr = self.getLatestVersionNum(job, elem, inc=False)
		path = '%s/%s_%s_%s_v%s.ma' %(versionPath, self.asset, job, elem, versionNumStr)

		self.checkPathExist(path, check)
		return path

	def getIncVersion(self, job, elem):
		self.checkPathExist(self.PATH)

		versionPath = self.getVersionDir(job)
		versionNumStr = self.getLatestVersionNum(job, elem, inc=True)
		path = '%s/%s_%s_%s_v%s.ma' %(versionPath, self.asset, job, elem, versionNumStr)

		return path


def setDirHidden(directory):
	if 'win' in sys.platform :
		import ctypes
		FILE_ATTRIBUTE_HIDDEN = 0x02
		ctypes.windll.kernel32.SetFileAttributesW( ur'%s' % directory , FILE_ATTRIBUTE_HIDDEN )


def makeDir(directory):
	if not os.path.exists(directory):
		os.makedirs(directory)
	return directory


def findAssetPath(name, proj='nine'):
	assetPath = '%s/asset' %config.PROJ_DIR_DICT[proj][0]
	for typ in [t for t in os.listdir(assetPath) if os.path.isdir(os.path.join(assetPath, t))]:
		typePath = '%s/%s' %(assetPath, typ)

		for subType in [s for s in os.listdir(typePath) if os.path.isdir(os.path.join(typePath, s))]:
			subTypePath = '%s/%s' %(typePath, subType)

			for asset in [a for a in os.listdir(subTypePath) if os.path.isdir(os.path.join(subTypePath, a))]:
				if asset == name:
					path = os.path.normpath('%s/%s' %(subTypePath, name))
					path = path.replace('\\', '/')
					return path

def getAssetsInType(typ, assetFilter='', proj='nine'):
	assetPath = '%s/asset' %config.PROJ_DIR_DICT[proj][0]
	assets = []
	# for typ in [t for t in os.listdir(assetPath) if os.path.isdir(os.path.join(assetPath, t))]:
	typePath = '%s/%s' %(assetPath, typ)

	for subType in [s for s in os.listdir(typePath) if os.path.isdir(os.path.join(typePath, s))]:
		subTypePath = '%s/%s' %(typePath, subType)
		assetsInSubType = [a for a in os.listdir(subTypePath) if os.path.isdir(os.path.join(subTypePath, a))]
		if assetFilter and assetsInSubType:
			assetsInSubType = difflib.get_close_matches(assetFilter, assetsInSubType, 20)
		if assetsInSubType:
			assets.extend(assetsInSubType)

	return assets

def getAsset(typ, subTyp, name, proj='nine'):
	assetPath = '%s/asset/%s/%s/%s' %(config.PROJ_DIR_DICT[proj][0], typ, subTyp, name)
	if not os.path.exists(assetPath):
		return None
	assetObj = Asset(assetPath)
	assetObj.load()
	return assetObj

def getShot(ch, seq, sh, proj='nine', mode=0):
	ch = 'ch%s' %str(ch).zfill(2)
	seq = 'seq%s' %str(seq).zfill(3)
	sh = 'sh%s' %str(sh).zfill(4)

	shotPath = '%s/scene/%s/%s/%s' %(config.PROJ_DIR_DICT[proj][mode], ch, seq, sh)
	shotObj = Shot(shotPath)
	shotObj.load()
	return shotObj

def getShotsInSeq(ch, seq, proj='nine', mode=0):
	ch = 'ch%s' %str(ch).zfill(2)
	seq = 'seq%s' %str(seq).zfill(3)

	seqPath = '%s/scene/%s/%s' %(config.PROJ_DIR_DICT[proj][mode], ch, seq)
	dirs = os.listdir(seqPath)

	shots = []
	for fol in sorted([d for d in dirs if re.match('sh[0-9][0-9][0-9][0-9]$', d) or re.match('s[0-9][0-9][0-9][0-9]$', d)]):
		shotPath = '%s/%s' %(seqPath, fol)
		if os.path.isdir(shotPath):
			shotObj = Shot(shotPath)
			shotObj.load()
			shots.append(shotObj)

	return shots

def getShotsInSeqFromPath(seqPath):
	dirs = os.listdir(seqPath)

	shots = []
	for fol in sorted([d for d in dirs if re.match('sh[0-9][0-9][0-9][0-9]$', d) or re.match('s[0-9][0-9][0-9][0-9]$', d)]):
		shotPath = os.path.join(seqPath, fol)
		if os.path.isdir(shotPath):
			shots.append(shotPath)

	return shots


def getShotPathsInSeq(ch, seq, proj='nine', drive=MAIN_DRIVE):
	ch = 'ch%s' %str(ch).zfill(2)
	seq = 'seq%s' %str(seq).zfill(3)

	seqPath = '%s/%s/all/scene/%s/%s' %(drive, proj, ch, seq)
	dirs = os.listdir(seqPath)

	shots = []
	for fol in sorted([d for d in dirs if re.match('sh[0-9][0-9][0-9][0-9]$', d) or re.match('s[0-9][0-9][0-9][0-9]$', d)]):
		shotPath = '%s/%s' %(seqPath, fol)
		if os.path.isdir(shotPath):
			shots.append(shotPath)

	return shots

def getAllAssetInSeq(ch, seq, job, elem, proj='nine', search=''):
	import pprint
	shots = getShotsInSeq(ch, seq, proj=proj, mode=0)
	ret = {}
	for shot in shots:
		f = None
		try:
			f = shot.getLatestVersion(job, elem, check=False)
		except:
			continue
		if not os.path.exists(f):
			om.MGlobal.displayWarning('Path does not exists : %s' %f)
			continue

		refs = pipeTools.getRefInFile(filePath=f)
		if search:
			refs = [r for r in refs if search in r]
		ret[shot.sh] = refs
	pprint.pprint(ret)
	return ret
	
def assetPublishStd(mode='local', path=''):
	if not os.path.exists(path):
		return

	from nuTools.pipeline import igAssetPublish as igAssetPublish
	reload(igAssetPublish)

	scriptPath = igAssetPublish.__file__

	command = '{0} {1} {2} {3}'.format(config.MAYAPY_PATH, scriptPath, mode, path)  
	subprocess.Popen(command)


def getAllMovInSeq(ch, seq, jobs=[], proj='nine', mode=0):
	shots = getShotsInSeq(ch=ch, seq=seq, proj=proj, mode=mode)

	mov_paths = {}
	for shot in shots:
		mov_paths[shot] = []
		if not jobs:
			jobs = shot.getExistingJobDirs()
		# print '\n%s' %shot.sh
		mov_dict = {}
		for job in jobs:
			output_path = shot.getDeptOutputDir(job=job, check=False)
			if os.path.exists(output_path):
				movs = [f for f in os.listdir(output_path) if os.path.isfile('%s/%s' %(output_path, f)) and os.path.splitext(f)[-1] == '.mov']
				if movs:
					for mov in sorted(movs):
						mov_paths[shot].append('%s/%s' %(output_path, mov))

	return mov_paths # {shot:[path1, path2, ...]}