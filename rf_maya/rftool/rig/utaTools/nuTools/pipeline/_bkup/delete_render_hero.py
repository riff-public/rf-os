import sys, os, shutil, subprocess
sys.path.append('P:/sysTool/python')
from nuTools.pipeline import iglooPipelineOs
reload(iglooPipelineOs)

IMG_FORMATS = ('.dpx', '.tif', '.tiff')

def log(path, text):
	with open(path, 'a') as f:
		f.write('\n%s' %text)


def copyRenderImage_out(seqPath, backupPath, job, log_path):
	# log the header
	header = '-------------- DELETE HERO --------------'
	header += '\n------------------------------------------'
	header += '\nseq_path : %s' %seqPath
	header += '\nbackup_path : %s' %backupPath
	header += '\njob : %s' %job
	header += '\nlog_path : %s' %log_path
	header += '\n------------------------------------------'
	header += '\n------------------------------------------'
	log(log_path, header)

	seq_splits = os.path.split(seqPath)
	seq_name = '%s_%s' %(os.path.split(seq_splits[0])[-1], seq_splits[-1])

	# get source shots
	shotPaths = iglooPipelineOs.getShotsInSeqFromPath(seqPath=seqPath)
	if not shotPaths:
		err = '\nERROR: Cannot get shot(s) from %s' %(shotPaths)
		print err
		log(log_path, err)
		return

	# get backup shots
	bkPaths = iglooPipelineOs.getShotsInSeqFromPath(seqPath=backupPath)
	if not bkPaths:
		err = '\nERROR: Cannot get shot(s) from %s' %(bkPaths)
		print err
		log(log_path, err)
		return

	# compare the number of shots for both source and backup
	num_shots = len(shotPaths)
	bk_num_shots = len(bkPaths)
	ns = '\n-----%s\n\tSource has %s shots\n\tBackup has %s shots' %(seq_name, num_shots, bk_num_shots)
	print ns
	log(log_path, ns)

	if num_shots != bk_num_shots:
		err = '\nERROR: number of shot of src and bkup is not equal!'
		print err
		log(log_path, err)

		loop_list, other_list = [], []
		loop_list = [os.path.split(s)[-1] for s in shotPaths]
		other_list = [os.path.split(s)[-1] for s in bkPaths]

		diff = []
		for sh in loop_list:
			if sh not in other_list:
				diff.append(sh)

		dmsg = 'Shot num difference found\n%s\nContinue?(y/n)' %('\n'.join(diff))
		print dmsg
		log(log_path, dmsg)
		answer = raw_input()
		log(log_path, answer)
		if answer != 'y':
			return
		else:
			# remove those omit
			for d in diff:
				value = os.path.join(seqPath, d)
				shotPaths.remove(value)

	all_size = 0.0
	delete_paths = []
	bk_paths = []
	valid = True
	for s_path, b_path in zip(shotPaths, bkPaths):
		# normalize path of the source path
		ospath = os.path.normpath(s_path)

		# get the shot name
		shotName = os.path.split(ospath)[-1]

		# backup paths
		bk_ospath = os.path.normpath(b_path)

		# backup: get hero dir
		bk_heroDir = os.path.join(bk_ospath, job, 'hero')
		if not os.path.exists(bk_heroDir):
			err = '\nERROR BKUP: %s Cannot get department directory : %s' %(shotName, bk_heroDir)
			print err
			log(log_path, err)
			continue

		# backup: get image folder
		bk_inside_hero = [f for f in os.listdir(bk_heroDir) if os.path.isdir(os.path.join(bk_heroDir, f)) and f.endswith('_%s' %job)]
		if not bk_inside_hero:
			err = '\nERROR BKUP: %s Cannot find folder with _%s' %(shotName, job)
			print err
			log(log_path, err)
			continue

		# backup: get files in image folder
		bk_imageFolderName = bk_inside_hero[0]
		bk_imageFolder = os.path.join(bk_heroDir, bk_imageFolderName)
		bk_files = [f for f in os.listdir(bk_imageFolder) if os.path.isfile(os.path.join(bk_imageFolder, f)) and os.path.splitext(f)[-1] in IMG_FORMATS]
		if not bk_files:
			err = '\nERROR BKUP: %s No image found in %s' %(shotName, bk_imageFolder)
			print err
			log(log_path, err)
			continue

		# backup: get the size of all images combinded
		bk_num_files = len(bk_files)
		bk_img_seq_size = sum([os.path.getsize(os.path.join(bk_imageFolder, f)) for f in bk_files])
		bk_paths.append(bk_imageFolder)

		# source path
		# source: get hero dir
		heroDir = os.path.join(ospath, job, 'hero')
		if not os.path.exists(heroDir):
			err = '\nERROR SRC: %s Cannot get department directory : %s' %(shotName, heroDir)
			print err
			log(log_path, err)
			continue

		# source: get image folder
		inside_hero = [f for f in os.listdir(heroDir) if os.path.isdir(os.path.join(heroDir, f)) and f.endswith('_%s' %job)]
		if not inside_hero:
			err = '\nERROR SRC: %s Cannot find folder with _%s' %(shotName, job)
			print err
			log(log_path, err)
			continue

		# source: get files in image folder
		imageFolderName = inside_hero[0]
		imageFolder = os.path.join(heroDir, imageFolderName)
		files = [f for f in os.listdir(imageFolder) if os.path.isfile(os.path.join(imageFolder, f)) and os.path.splitext(f)[-1] in IMG_FORMATS]
		if not files:
			err = '\nERROR SRC: %s No image found in %s' %(shotName, imageFolder)
			print err
			log(log_path, err)
			continue

		# source: get the size of all images combinded
		num_files = len(files)
		img_seq_size = sum([os.path.getsize(os.path.join(imageFolder, f)) for f in files])

		# print image file found summay to user
		cf = '\n%s\tsrc : %s files size %s(%s mb.)' %(shotName, num_files, img_seq_size, img_seq_size/(1024*1024.0))
		cf += '\n\tbackup : %s files  size %s(%s mb.)' %(bk_num_files, bk_img_seq_size, bk_img_seq_size/(1024*1024.0))
		
		print cf
		log(log_path, cf)

		# compare number of frames
		if num_files != bk_num_files:
			err = '\nERROR: %s frames number is not equal' %shotName
			print err
			log(log_path, err)
			valid = False

		# compare image sizes
		if img_seq_size != bk_img_seq_size:
			err = '\nERROR: %s size is not equal' %shotName
			print err
			log(log_path, err)
			valid = False

		# add source size to all_size
		all_size += img_seq_size
		# add path to be deleted
		delete_paths.append(imageFolder)

	# if a single byte or a frame gone missing, just stop!
	num_del_path = len(delete_paths)
	num_bk_path = len(bk_paths)
	if not valid or num_del_path != num_bk_path or num_del_path == 0 or num_bk_path == 0:
		vmsg = '\nERROR: non matching source and backup.'
		print vmsg
		log(log_path, vmsg)
		return

	del_num_shot = len(delete_paths)
	
	# done checking, confirm user for deletion
	sl = '\nCHECKING COMPLETE!\nReady to delete.'
	sl += '\n---------- SHOT LIST ----------\n'
	sl += '\n'.join(shotPaths)
	sl += '\n\n%s\n%s shots found' %(seq_name, del_num_shot)
	sl += '\ntotal size of %s (%s GB.)' %(all_size, all_size/(1024*1024*1024.0))
	sl += '\nDelete?(y/n)'
	print sl
	log(log_path, sl)
	answer = raw_input()
	log(log_path, answer)
	if answer != 'y':
		return

	# deletion loop
	curr_sh = 0
	for imageFolder in delete_paths:
		try:
			dc = '\nDELETING : %s' %imageFolder
			print dc
			log(log_path, dc)

			del_cmd = ["del", "%s" %imageFolder, "/Q", "/S"]
			del_operation = subprocess.call(del_cmd, shell=True)

			curr_sh += 1
			sc = 'DELETED %s/%s (%.2d%%)' %(curr_sh, del_num_shot, (float(curr_sh)/float(del_num_shot))*100.0)
			print sc
			log(log_path, sc)
		except Exception, e:
			print e
			break

	# print out completion details
	cmsg = '\nDELETE DONE : %s' %seq_name
	cmsg += '\n%s shots deleted' %(curr_sh)
	cmsg += '\nspace free : %s (%s GB.)' %(all_size, all_size/(1024*1024*1024.0))
	print cmsg
	log(log_path, cmsg)

if __name__ == '__main__': 
	print '\nSeq Path?'
	seqPath = raw_input()
	
	print '\nBackup Path?'
	backupPath = raw_input()

	print '\nJob?'
	job = raw_input()

	print '\nLog path?'
	log_path = raw_input()

	# do it
	copyRenderImage_out(os.path.normpath(str(seqPath)), os.path.normpath(str(backupPath)), str(job), os.path.normpath(str(log_path)))