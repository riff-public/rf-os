import os
import pymel.core as pm
import maya.mel as mel
import maya.OpenMaya as om 

from nuTools.pipeline import iglooPipeline
reload(iglooPipeline)

class IgClothCacheManager(object):

	def __init__(self):
		self.WINDOW_NAME = 'igClothCacheManagerWin'
		self.WINDOW_TITLE = 'Igloo Cloth Cache Manager beta'

	def UI(self):
		if pm.window(self.WINDOW_NAME, ex=True):
			pm.deleteUI(self.WINDOW_NAME, window=True)
		with pm.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, s=True, mnb=True, mxb=False) as self.mainWindow:
			with pm.columnLayout(adj=True):
				with pm.frameLayout(lv=False, mh=10, mw=10):
					with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 115), (2, 'left', 85)]):
						self.modeRadioCol = pm.radioCollection()
						self.geoCacheModeRadioButt = pm.radioButton('geoCacheModeRadioButt', l='Geometry Cache', sl=False)
						self.nClothCacheRadioButt = pm.radioButton('nClothCacheRadioButt', l='nCloth Cache', sl=True)

			with pm.frameLayout(lv=False, mh=5, mw=5) as self.geoCacheModeFrame:
				with pm.rowColumnLayout(nc=4, rs=(1, 5), co=[(1, 'left', 5), (2, 'left', 5), (3, 'left', 5), (4, 'left', 5)]):
					pm.text(l='Cache Path:')
					self.pathTxtFld = pm.textField(w=400, cc=pm.Callback(self.pathChanged), ec=pm.Callback(self.updateTsl))
					pm.button(l='refresh', c=pm.Callback(self.refresh))
					pm.button(l='...', c=pm.Callback(self.browse))
				with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 5), (2, 'left', 5)]):
					with pm.columnLayout(adj=True):
						with pm.tabLayout() as self.heroVersionTabLayout:
							with pm.frameLayout(lv=False, mh=0, mw=0) as self.geoCacheHeroFrame:
								self.heroTSL = pm.textScrollList(h=300, w=260, ams=False)
							with pm.frameLayout(lv=False, mh=0, mw=0) as self.geoCacheVersionFrame:
								self.versionTSL = pm.textScrollList(h=300, w=260, ams=False)

							pm.tabLayout(self.heroVersionTabLayout, edit=True, tabLabel=((self.geoCacheHeroFrame, 'Hero'), (self.geoCacheVersionFrame, 'Version')), 
								selectTabIndex=2, cc=pm.Callback(self.updateTsl))
					
						pm.button(l='Import', w=150, h=30, c=pm.Callback(self.importCache))

					with pm.tabLayout() as self.ExportSettingsTabLayout:
						with pm.frameLayout(lv=False, mh=5, mw=5) as self.exportSettingsFrame:
							with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 0), (2, 'left', 15)]):
								pm.text(l='File destribution')
								with pm.columnLayout(adj=True):
									self.fileDestributionRadioCol = pm.radioCollection()
									self.oneFilePerFrameRadioButt = pm.radioButton('oneFilePerFrameRadioButt', l='One file per frame', sl=False)
									self.oneFileRadioButt = pm.radioButton('oneFileRadioButt', l='One file', sl=True)
								pm.text(l='Time range')
								with pm.columnLayout(adj=True):
									self.timeRangeRadioCol = pm.radioCollection()
									self.renderSettingsRadioButt = pm.radioButton('renderSettingsRadioButt', l='Render Settings', sl=False, onc=pm.Callback(self.timeRangeChanged))
									self.timeSliderRadioButt = pm.radioButton('timeSliderRadioButt', l='Time Slider', sl=True, onc=pm.Callback(self.timeRangeChanged))
									self.startEndRadioButt = pm.radioButton('startEndRadioButt', l='Start/End', sl=False, onc=pm.Callback(self.timeRangeChanged))
									with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 0), (2, 'left', 5)]):
										self.startFloatField = pm.floatField(v=1.0, en=False)
										self.endFloatField = pm.floatField(v=10.0, en=False)
								pm.text(l='Suspend refresh')
								self.suspendRefreshCheckBox = pm.checkBox(l='', v=False)
							with pm.columnLayout(adj=True):
								with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 0), (2, 'left', 0)]):
									pm.text(l='Cache prefix: ')
									self.cachePrefixTextField = pm.textField(w=180, tx='')
								
								pm.button(l='Export', w=150, h=30, c=pm.Callback(self.exportCache))
						pm.tabLayout(self.ExportSettingsTabLayout, edit=True, tabLabel=(self.exportSettingsFrame, 'Export Settings'))

	def pathChanged(self):
		self.versionTSL.removeAll()
		self.heroTSL.removeAll()

	def browse(self):
		currPath = pm.sceneName()
		stDir = ''
		try:
			shot = iglooPipeline.Shot(currPath)
			shot.load()
			stDir = shot.PATH
		except:
			pass

		path = pm.fileDialog2(ds=2, fm=3, dir=stDir, cap='Browse Cache Directory', okc='Select Directory')
		if path:
			# fill text field
			self.pathTxtFld.setText(path[0])

			# update tsl
			self.updateTsl()

	def exportCache(self):
		if not pm.selected():
			om.MGlobal.displayError('Must select nCloth object(s) to cache.')
			return

		currentRadioSel = self.modeRadioCol.getSelect()

		if self.suspendRefreshCheckBox.getValue() == True:
			pm.refresh(suspend=True)
		
		sels = pm.selected(type='transform')
		obj = None
		if len(sels) != 1 or not sels[0].getShape(ni=True):
			om.MGlobal.displayError('Invalid selection.')
		obj = sels[0]

		hist = pm.listHistory(obj, pdo=True)
		doDel = False
		for node in hist:
			if pm.nodeType(node) == "cacheFile":
				doDel = True
				break

		try:
			if currentRadioSel == 'geoCacheModeRadioButt':
				self.exportGeoCache(deleteCache=doDel)
			else:
				self.exportNClothCache(deleteCache=doDel)
		except:
			pass
			# print e

		pm.refresh(suspend=False)
		pm.refresh(force=True)
		self.updateTsl()

	def importCache(self):
		currentRadioSel = self.modeRadioCol.getSelect()

		# doImportCacheFile(string $fileName, string $fileType,string $geometries[],string $channelNames[])
		dirPath = self.getDirPath()
		tslDict = {1:self.heroTSL, 2:self.versionTSL}
		tsl = tslDict[self.heroVersionTabLayout.getSelectTabIndex()]
		fileName = ''
		try:
			fileName = tsl.getSelectItem()[0]
		except:
			om.MGlobal.displayError('Select a file from hero/version list.')
			return

		filePath = '%s/%s' %(dirPath, fileName)
		cmd = 'doImportCacheFile("%s", "xmlcache", {}, {})' %filePath
		mel.eval(cmd)

	def getTimeRangeModeArg(self):
		currentRadioSel = self.timeRangeRadioCol.getSelect()
		timeRangeDict = {'renderSettingsRadioButt':'1', 'timeSliderRadioButt':'2', 'startEndRadioButt':'3'}
		return timeRangeDict[currentRadioSel]

	def getFileDesArg(self):
		currentRadioSel = self.fileDestributionRadioCol.getSelect()
		fileDesDict = {'oneFilePerFrameRadioButt':'OneFilePerFrame', 'oneFileRadioButt':'OneFile'}
		return fileDesDict[currentRadioSel]

	def getDirPath(self):
		clothPath = self.pathTxtFld.getText()
		selIndx = self.heroVersionTabLayout.getSelectTabIndex()
		elem = 'version'
		if selIndx == 1:
			elem = 'hero'
		exportpath = '%s/%s' %(clothPath, elem)
		exportpath = os.path.normpath(exportpath.replace(' ', ''))
		exportpath = exportpath.replace('\\', '/')
		if not os.path.exists(exportpath):
			# error
			raise IOError('Directory does not exist!  %s' %exportpath)

		return exportpath

	def getCachePrefix(self):
		cachePrefix = self.cachePrefixTextField.getText()
		if not cachePrefix:
			raise SystemError('Please specify cache prefix!')
		else:
			return cachePrefix

	def exportGeoCache(self, deleteCache=True):
		if deleteCache == True:
			try:
				mel.eval('deleteCacheFile 3 {"keep", "", "geometry"};')
			except:
				pass

		# doCreateGeometryCache 6 { "3", "0", "10", "OneFilePerFrame", "1", "","0","","0", "add", "0", "1", "1","0","1","mcc","0" } ;
		# get args
		mode = 6
		args = [self.getTimeRangeModeArg(), # time range mode = 0 : use $args[1] and $args[2] as start-end 
											# time range mode = 1 : use render globals
											# time range mode = 2 : use timeline
		str(self.startFloatField.getValue()),  # start frame (if time range mode == 0)
		str(self.endFloatField.getValue()),  # end frame (if time range mode == 0)
		self.getFileDesArg(),  # cache file distribution, either "OneFile" or "OneFilePerFrame"
		'1',  # whether to refresh during caching
		self.getDirPath(),  # directory for cache files, if "", then use project data dir
		'1',  # whether to create a cache per geometry
		self.getCachePrefix(),  # name of cache file. An empty string can be used to specify that an auto-generated name is acceptable.
		'1',  # whether the specified cache name is to be used as a prefix
		'add',  # action to perform: "add", "replace", "merge", "mergeDelete" or "export"
		'1',  # force save even if it overwrites existing files
		'1',  # simulation rate, the rate at which the cloth simulation is forced to run
		'1',  # sample mulitplier, the rate at which samples are written, as a multiple of simulation rate.
		'0',  # whether modifications should be inherited from the cache about to be replaced. Valid only when $action == "replace".
		'1',  # whether to store doubles as floats
		'mcc',  # name of cache format
		'0']  # whether to export in local or world space 

		argStr = '%s { %s }' %(mode, ', '.join(['"%s"' %i for i in args]))

		cmd = 'doCreateGeometryCache %s;' %argStr
		mel.eval(cmd)

	def exportNClothCache(self, deleteCache=True):
		if deleteCache == True:
			try:
				mel.eval('deleteCacheFile 3 {"keep", "", "nCloth"};')
			except:
				pass

		# doCreateNclothCache 4 { "3", "0", "10", "OneFile", "1", "",
		# "0","","0", "add", "0", "1", "1","0","1" } ;
		# get args
		mode = 4
		args = [self.getTimeRangeModeArg(), # time range mode = 0 : use $args[1] and $args[2] as start-end 
											# time range mode = 1 : use render globals
											# time range mode = 2 : use timeline
		str(self.startFloatField.getValue()),  # start frame (if time range mode == 0)
		str(self.endFloatField.getValue()),  # end frame (if time range mode == 0)
		self.getFileDesArg(),  # cache file distribution, either "OneFile" or "OneFilePerFrame"
		'1',  # whether to refresh during caching
		self.getDirPath(),  # directory for cache files, if "", then use project data dir
		'1',  # whether to create a cache per geometry
		self.getCachePrefix(),  # name of cache file. An empty string can be used to specify that an auto-generated name is acceptable.
		'1',  # whether the specified cache name is to be used as a prefix
		'add',  # action to perform: "add", "replace", "merge", "mergeDelete" or "export"
		'1',  # force save even if it overwrites existing files
		'1',  # simulation rate, the rate at which the cloth simulation is forced to run
		'1',  # sample mulitplier, the rate at which samples are written, as a multiple of simulation rate.
		'0',  # whether modifications should be inherited from the cache about to be replaced. Valid only when $action == "replace".
		'1']  # whether to store doubles as floats

		argStr = '%s { %s }' %(mode, ', '.join(['"%s"' %i for i in args]))

		cmd = 'doCreateNclothCache %s;' %argStr
		mel.eval(cmd)

	def timeRangeChanged(self):
		currentRadioSel = self.timeRangeRadioCol.getSelect()
		if currentRadioSel == 'startEndRadioButt':
			self.setStartEndField(value=True)
		else:
			self.setStartEndField(value=False)

	def setStartEndField(self, value):
		self.startFloatField.setEnable(value)
		self.endFloatField.setEnable(value)

	def prepareFolders(self, rootPath):
		if not os.path.exists(rootPath):
			os.makedirs(rootPath)

		cachePath = '%s/cache' %rootPath
		if not os.path.exists(cachePath):
			os.makedirs(cachePath)

		clothPath = '%s/cloth' %cachePath
		if not os.path.exists(clothPath):
			os.makedirs(clothPath)

		heroPath = '%s/hero' %clothPath
		if not os.path.exists(heroPath):
			os.makedirs(heroPath)

		versionPath = '%s/version' %clothPath
		if not os.path.exists(versionPath):
			os.makedirs(versionPath)

		return clothPath, heroPath, versionPath

	def refresh(self):
		currPath = pm.sceneName()
		if not currPath:
			return

		# reset variable
		self.clothPath = None
		self.heroPath = None
		self.versionPath = None

		shot = iglooPipeline.Shot(currPath)
		shot.load()
		dataPath = shot.getDeptDataDir(job=shot.job, check=False)

		# create folder if nescessary
		clothPath, heroPath, versionPath = self.prepareFolders(rootPath=dataPath)

		# fill text field
		self.pathTxtFld.setText(clothPath)

		# update tsl
		self.updateTsl()

	def updateTsl(self):
		pathText = self.pathTxtFld.getText()
		if not pathText or not os.path.exists(pathText):
			om.MGlobal.displayError('No path or path does not exist.  %s' %pathText)
			return

		# remove all items from TSL
		self.versionTSL.removeAll()
		self.heroTSL.removeAll()

		heroPath = '%s/hero' %pathText
		versionPath = '%s/version' %pathText
		hfiles = sorted([f for f in os.listdir(heroPath) if os.path.splitext(f)[-1] != '.mc' and os.path.isfile('%s/%s' %(heroPath, f))])
		vfiles = sorted([f for f in os.listdir(versionPath) if os.path.splitext(f)[-1] != '.mc' and os.path.isfile('%s/%s' %(versionPath, f))])
		
		for f in hfiles:
			self.heroTSL.append(f)
		for f in vfiles:
			self.versionTSL.append(f)