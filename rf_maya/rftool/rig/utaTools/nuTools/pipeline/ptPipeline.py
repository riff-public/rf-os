# system, util modules
import os, re
# from pprint import pprint

# maya modules
import pymel.core as pm
from maya import OpenMaya as om
import maya.cmds as mc
import maya.mel as mel

# custom modules
from nuTools import misc, config
reload(misc)
reload(config)


def getRigGrpAttr():
	"""
	Find the Rig_Grp in the scene and get asset attributes values from it. 
		return: dict {'rigGrp':rigGrp, 
					 'proj':proj, 
					 'assetType':assetType, 
					 'assetSubType':assetSubType, 
					 'assetName':assetName}

	"""

	proj, assetType, assetSubType, assetName = None, None, None, None
	try:
		pos = ['|Rig_Grp', '|Rig:Rig_Grp', 'Rig_Grp']
		toCast = None
		for p in pos:
			if pm.objExists(p):
				toCast = p
		rigGrp = pm.PyNode(toCast)
		if rigGrp.hasAttr('project'):
			proj = rigGrp.project.get()
		else:
			proj = pm.sceneName().split('/')[1]
		if rigGrp.hasAttr('assetType'):
			assetType = rigGrp.assetType.get()
		if rigGrp.hasAttr('assetSubType'):
			assetSubType = rigGrp.assetSubType.get()
		if rigGrp.hasAttr('assetName'):
			assetName = rigGrp.assetName.get()
	except:
		pm.error('Cannot find Rig_Grp or invalid Rig_Grp!')

	return {'rigGrp':rigGrp, 'proj':proj, 'assetType':assetType, 'assetSubType':assetSubType, 'assetName':assetName}

def getPipelinePath(dept='rig', drive='P', assetPath='asset/3D', copy=True):
	"""
	Get directory path in the production pipeline of an asset from the existing Rig_Grp in the scene.
		args:
			dept = Department you want to get path.(str)
			drive = Network drive.(str)
			assetPath = Path from project path to asset.(str)
			copy = To copy path to your clipboard or not?(bool)
			work = Get work directory instead of the path to hero file.(bool)
			fileInc = Also include file in the path.

		return: The path(str)
	"""

	rgAttr = getRigGrpAttr()

	# if not dept in ['model', 'uv', 'rig', 'shade', 'dRig', 'dAnimShading', 'dModel', 'dShading', 'ref']:
	# 	return
		
	deptMd = '%s_md' %dept

	root = '%s:/%s/%s/%s/%s/%s/publish/%s/%s' %(drive, rgAttr['proj'], assetPath, rgAttr['assetType'], rgAttr['assetSubType'], 
									rgAttr['assetName'], dept, deptMd)

	version = str(misc.getLatestVersion(path=root)).zfill(3)
	fileName = '%s_%s_v%s.ma' %(rgAttr['assetName'], deptMd, version)
		

	path = '%s/%s' %(root, fileName)

	if os.path.exists(path) == True:
		if copy == True:
			misc.addToClipBoard(path)
		return path
	else:
		print path
		pm.error('Invalid path! Check your argruments.')

def importFromPipeline(dept='uv', ref=True, drive='P', assetPath='asset/3D'):
	"""
	Import or reference a hero file of an asset from the existing Rig_Grp in the scene.
		args:
			dept = Department you want to get path.(str)
			ref = Do reference instead of import.(bool)
			drive = Network drive.(str)
			assetPath = Path from project path to asset.(str)

		return: The path(str)
	"""

	toImport = getPipelinePath(dept=dept, drive=drive, assetPath=assetPath, 
			   copy=False)	

	fileName = '%s' %dept
	try:
		fileExt = os.path.split(toImport)[-1]
		fileName = os.path.splitext(fileExt)[0]
	except:
		pass


	try:
		if ref == True:
			refObj = pm.createReference(toImport, ns=fileName)
			return refObj
		else:
			importObj = pm.importFile(toImport, ns=fileName)
			return fileName		
	except:
		pm.warning('Invalid path:  %s' %toImport)

