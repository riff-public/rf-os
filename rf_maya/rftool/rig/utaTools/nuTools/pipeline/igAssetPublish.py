import sys


def assetPublish(mode, filename):
	import pymel.core as pm
	from nuTools.pipeline import iglooPipeline as igpipe

	pm.openFile(filename, f=True)

	igpipe.assetPublish(mode=mode)
	print "Press enter to exit."
	ri = raw_input()


if __name__ == "__main__": 
	assetPublish(sys.argv[1], sys.argv[2])