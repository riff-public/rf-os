import maya.cmds as mc
import pkmel.core as pc
reload( pc )
import pkmel.rigTools as rigTools
reload( rigTools )
import pkmel.mainGroup as pmain
reload( pmain )
import pkmel.rootRig as proot
reload( proot )
import pkmel.pelvisRig as ppelv
reload( ppelv )
import pkmel.spineRig as pspi
reload( pspi )
import pkmel.humanSpineRig as phspi
reload( phspi )
import pkmel.neckRig as pneck
reload( pneck )
import pkmel.headRig as phead
reload( phead )
import pkmel.clavicleRig as pclav
reload( pclav )
import pkmel.armRig as parm
reload( parm )
import pkmel.legRig as pleg
reload( pleg )
import pkmel.backLegRig as pbleg
reload( pbleg )
import pkmel.fingerRig as pfngr
reload( pfngr )
import pkmel.thumbRig as pthmb
reload( pthmb )
import pkmel.ribbon as prbn
reload( prbn )


placement = pc.Dag( 'placement_tmpCtrl' )
# Naming
charName = ''
elem = ''

# Rig
mainGroup = pmain.MainGroup()
rigTools.nodeNaming( mainGroup , charName = charName , elem = elem , side = '' )

anim = mainGroup.anim_grp
jnt = mainGroup.jnt_grp
skin = mainGroup.skin_grp
ikh = mainGroup.ikh_grp
still = mainGroup.still_grp
size = mainGroup.charSize

# Root
rootRig = proot.RootRig( animGrp = anim ,
			skinGrp = skin ,
			charSize = size ,
			tmpJnt = 'root_tmpJnt' )

rigTools.nodeNaming( rootRig ,
			charName = charName ,
			elem = elem ,
			side = '' )

# Spine	
spineRig = pspi.SpineRig( parent = rootRig.root_jnt.name ,
				animGrp = anim ,
				jntGrp = jnt ,
				ikhGrp = ikh ,
				skinGrp = skin ,
				stillGrp = still ,
				ribbon = True,
				ax = 'z' ,
				charSize = size ,
				tmpJnt = ( 'spine1_tmpJnt' , 'spine2_tmpJnt' , 'neck1_tmpJnt' ) )

rigTools.nodeNaming( spineRig ,
			charName = charName ,
			elem = elem ,
			side = '' )

rigTools.dummyNaming( obj = spineRig.lowSpineRbn ,
			attr = 'rbn' ,
			dummy = 'lowSpineRbn' ,
			charName = charName ,
			elem = elem ,
			side = '' )

rigTools.dummyNaming( obj = spineRig.upSpineRbn ,
			attr = 'rbn' ,
			dummy = 'upSpineRbn' ,
			charName = charName ,
			elem = elem ,
			side = '' )

# Pelvis
pelvisRig = ppelv.PelvisRig( parent = spineRig.spine1_jnt.name ,
				animGrp = anim ,
				skinGrp = skin ,
				charSize = size ,
				tmpJnt = 'pelvis_tmpJnt' )

rigTools.nodeNaming( pelvisRig ,
			charName = charName ,
			elem = elem ,
			side = '' )

# Neck
neckRig = pneck.NeckRig( parent = spineRig.spine2_jnt ,
			animGrp = anim ,
			jntGrp = jnt ,
			skinGrp = skin ,
			stillGrp = still ,
			ribbon = True ,
			ax = 'y' ,
			charSize = size ,
			tmpJnt = ( 'neck1_tmpJnt' , 'head1_tmpJnt' ) )

rigTools.nodeNaming( neckRig ,
			charName = charName ,
			elem = elem ,
			side = '' )

rigTools.dummyNaming( obj = neckRig.neckRbn ,
			attr = 'rbn' ,
			dummy = 'neckRbn' ,
			charName = charName ,
			elem = elem ,
			side = '' )

# Head
headRig = phead.HeadRig( parent = neckRig.neck2_jnt ,
			animGrp = anim ,
			skinGrp = skin ,
			charSize = size ,
			tmpJnt = ( 'head1_tmpJnt' , 'head2_tmpJnt' , 'eyeLFT_tmpJnt' , 'eyeRGT_tmpJnt' , 'jaw1LWR_tmpJnt' , 'jaw2LWR_tmpJnt' , 'jaw3LWR_tmpJnt' , 'jaw1UPR_tmpJnt' , 'jaw2UPR_tmpJnt' , 'eye_tmpJnt' , 'eyeTrgtLFT_tmpJnt' , 'eyeTrgtRGT_tmpJnt' ) )

rigTools.nodeNaming( headRig ,
			charName = charName ,
			elem = elem ,
			side = '' )

attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' )
for attr in attrs :
	neckRig.headSpc_grp.attr( attr ).l = 0

headPosGrp_parCons = pc.parentConstraint( headRig.head1_jnt , neckRig.headSpc_grp )

# Clavicle left
clavLRig = pclav.ClavicleRig( parent = spineRig.spine2_jnt ,
				side = 'LFT' ,
				animGrp = anim ,
				skinGrp = skin ,
				charSize = size ,
				tmpJnt = ( 'clavLFT_tmpJnt' , 'upLegLFT_tmpJnt' ) )

rigTools.nodeNaming( clavLRig ,
			charName = charName ,
			elem = elem ,
			side = 'LFT' )

# Clavicle right:
clavRRig = pclav.ClavicleRig( parent = spineRig.spine2_jnt ,
				side = 'RGT' ,
				animGrp = anim ,
				skinGrp = skin ,
				charSize = size ,
				tmpJnt = ( 'clavRGT_tmpJnt' , 'upLegRGT_tmpJnt' ) )

rigTools.nodeNaming( clavRRig ,
			charName = charName ,
			elem = elem ,
			side = 'RGT' )

# Hip left
hipLRig = pclav.ClavicleRig( parent = pelvisRig.pelvis_jnt ,
				side = 'LFT' ,
				animGrp = anim ,
				skinGrp = skin ,
				charSize = size ,
				tmpJnt = ( 'hipLFT_tmpJnt' , 'upLegBLFT_tmpJnt' ) )

rigTools.nodeNaming( hipLRig ,
			charName = charName ,
			elem = 'Hip' ,
			side = 'LFT' )

# Hip right:
hipRRig = pclav.ClavicleRig( parent = pelvisRig.pelvis_jnt ,
				side = 'RGT' ,
				animGrp = anim ,
				skinGrp = skin ,
				charSize = size ,
				tmpJnt = ( 'hipRGT_tmpJnt' , 'upLegBRGT_tmpJnt' ) )

rigTools.nodeNaming( hipRRig ,
			charName = charName ,
			elem = 'Hip' ,
			side = 'RGT' )

# Leg left
legLRig = pleg.LegRig( parent = clavLRig.clav2_jnt ,
			side = 'LFT' ,
			animGrp = anim ,
			jntGrp = jnt ,
			ikhGrp = ikh ,
			skinGrp = skin ,
			stillGrp = still ,
			ribbon = True ,
			charSize = size ,
			tmpJnt = ( 'upLegLFT_tmpJnt' , 'lowLegLFT_tmpJnt' , 'ankleLFT_tmpJnt' , 'ballLFT_tmpJnt' , 'toeLFT_tmpJnt' , 'heelLFT_tmpJnt' , 'footInLFT_tmpJnt' , 'footOutLFT_tmpJnt' , 'kneeIkLFT_tmpJnt' ) )

rigTools.nodeNaming( legLRig ,
			charName = charName ,
			elem = elem ,
			side = 'LFT' )

rigTools.dummyNaming( obj = legLRig.upLegRbn ,
			attr = 'rbn' ,
			dummy = 'upLegRbn' ,
			charName = charName ,
			elem = elem ,
			side = 'LFT' )

rigTools.dummyNaming( obj = legLRig.lowLegRbn ,
			attr = 'rbn' ,
			dummy = 'lowLegRbn' ,
			charName = charName ,
			elem = elem ,
			side = 'LFT' )

# Leg right
legRRig = pleg.LegRig( parent = clavRRig.clav2_jnt ,
			side = 'RGT' ,
			animGrp = anim ,
			jntGrp = jnt ,
			ikhGrp = ikh ,
			skinGrp = skin ,
			stillGrp = still ,
			ribbon = True ,
			charSize = size ,
			tmpJnt = ( 'upLegRGT_tmpJnt' , 'lowLegRGT_tmpJnt' , 'ankleRGT_tmpJnt' , 'ballRGT_tmpJnt' , 'toeRGT_tmpJnt' , 'heelRGT_tmpJnt' , 'footInRGT_tmpJnt' , 'footOutRGT_tmpJnt' , 'kneeIkRGT_tmpJnt' ) )

rigTools.nodeNaming( legRRig ,
			charName = charName ,
			elem = elem ,
			side = 'RGT' )

rigTools.dummyNaming( obj = legRRig.upLegRbn ,
			attr = 'rbn' ,
			dummy = 'upLegRbn' ,
			charName = charName ,
			elem = elem ,
			side = 'RGT' )

rigTools.dummyNaming( obj = legRRig.lowLegRbn ,
			attr = 'rbn' ,
			dummy = 'lowLegRbn' ,
			charName = charName ,
			elem = elem ,
			side = 'RGT' )

# Back leg left
bLegLRig = pbleg.BackLegRig( parent = hipLRig.clav2_jnt ,
			side = 'LFT' ,
			animGrp = anim ,
			jntGrp = jnt ,
			ikhGrp = ikh ,
			skinGrp = skin ,
			stillGrp = still ,
			ribbon = True ,
			charSize = size ,
			tmpJnt = ( 'upLegBLFT_tmpJnt' , 'midLegBLFT_tmpJnt' , 'lowLegBLFT_tmpJnt' ,
			 'ankleBLFT_tmpJnt' , 'ballBLFT_tmpJnt' , 'toeBLFT_tmpJnt' , 'heelBLFT_tmpJnt' ,
			  'footInBLFT_tmpJnt' , 'footOutBLFT_tmpJnt' , 'kneeIkBLFT_tmpJnt' ) )

rigTools.nodeNaming( bLegLRig ,
			charName = charName ,
			elem = 'Back' ,
			side = 'LFT' )

rigTools.dummyNaming( obj = bLegLRig.upLegRbn ,
			attr = 'rbn' ,
			dummy = 'upLegBackRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'LFT' )

rigTools.dummyNaming( obj = bLegLRig.midLegRbn ,
			attr = 'rbn' ,
			dummy = 'midLegBackRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'LFT' )

rigTools.dummyNaming( obj = bLegLRig.lowLegRbn ,
			attr = 'rbn' ,
			dummy = 'lowLegBackRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'LFT' )

# Back leg right
bLegRRig = pbleg.BackLegRig( parent = hipRRig.clav2_jnt ,
			side = 'RGT' ,
			animGrp = anim ,
			jntGrp = jnt ,
			ikhGrp = ikh ,
			skinGrp = skin ,
			stillGrp = still ,
			ribbon = True ,
			charSize = size ,
			tmpJnt = ( 'upLegBRGT_tmpJnt' , 'midLegBRGT_tmpJnt' , 'lowLegBRGT_tmpJnt' , 'ankleBRGT_tmpJnt' , 'ballBRGT_tmpJnt' , 'toeBRGT_tmpJnt' , 'heelBRGT_tmpJnt' , 'footInBRGT_tmpJnt' , 'footOutBRGT_tmpJnt' , 'kneeIkBRGT_tmpJnt' ) )

rigTools.nodeNaming( bLegRRig ,
			charName = charName ,
			elem = 'Back' ,
			side = 'RGT' )

rigTools.dummyNaming( obj = bLegRRig.upLegRbn ,
			attr = 'rbn' ,
			dummy = 'upLegBackRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'RGT' )

rigTools.dummyNaming( obj = bLegRRig.midLegRbn ,
			attr = 'rbn' ,
			dummy = 'midLegBackRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'RGT' )

rigTools.dummyNaming( obj = bLegRRig.lowLegRbn ,
			attr = 'rbn' ,
			dummy = 'lowLegBackRbn' ,
			charName = charName ,
			elem = '' ,
			side = 'RGT' )

if placement.exists :
	mc.delete( placement )
