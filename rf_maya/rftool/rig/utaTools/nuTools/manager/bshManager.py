import pymel.core as pm
import maya.OpenMaya as om
import maya.mel as mel
import os, socket, pprint ,inspect, sys, subprocess
from nuTools.manager import btp
from nuTools import misc, config, ui
from nuTools.util import symMeshReflector as smr

reload(misc)
reload(config)
reload(ui)
reload(smr)
reload(btp)

class BshManager(object):

	def __init__(self):
		self.WINDOW_TITLE = 'Blendshape Manager v1.5'
		self.WINDOW_NAME = 'bmWin'

		self.gap = 5.0
		self.width = 0
		self.height = 0

		self.exportPath = ''
		self.importPath = ''
		self.focusPartName = ''
		self.focusProjName = ''
		self.focusBtpName = ''
		self.currentRangeVal = 'p'
		self.patternDetail = ''

		self.focusInbs = []
		self.tAttrs = []
		self.nodes = []
		self.selTargets =[]
		self.focusParts = []
		self.focusPartNames = []
		self.allTargets = []
		self.sepAttrs = []
		self.selInbs = []
		self.testTargets = []

		self.projs = {}	
		self.btps = {}
		self.partGrps = {}
		self.targetDic = {}
		self.focusTgts = {}
		self.focusInbsDic = {}
		self.tAttrsDict = {}
		self.baseMeshesDict = {}

		self.currentProj = None
		self.btp = None
		self.bufferGrp = None
		self.tmpGrp = None
		self.txtGrp = None
		self.bsh = None
		self.focusPart = None
		self.selTgt = None
		self.currentBaseGeo = None
		self.currentSmrObj = None
		self.bshTgtGrp = None
		self.revertSliderDragging = False

		#get all patterns avaliable in btp
		self.utilizePattern()

	def UI(self):
		"""
		The main UI function

		"""
		if pm.window(self.WINDOW_NAME, ex=True):
			pm.deleteUI(self.WINDOW_NAME, window=True)
		with pm.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, w=300, h=600, s=True, mnb=True, mxb=True) as self.mainWindow:
			# with pm.columnLayout(adj=0):
			with pm.frameLayout(label='Manage, Sculpt and Publish Blendshape for Characters.', fn='smallObliqueLabelFont', mh=5, mw=5):
				with pm.rowColumnLayout(nc=3, co=[(1, 'left', 5), (2, 'left', 5), (3, 'left', 5)]):
					self.exportGrpTxt = pm.text(l='Bffr Group: ')
					self.exportGrpTxtFld = pm.textField(w=300, ed=False)
					self.exportGrpButt = pm.button(l='<<', c=pm.Callback(self.loadExportGrpUi))
				with pm.rowColumnLayout(nc=3, co=[(1, 'left', 5), (2, 'left', 5), (3, 'left', 175)]):
					pm.text(l='Global Scale')
					self.gsFloatFld = pm.floatField(w=50, v=1.00, cc=pm.Callback(self.updateGlobalScale))
					self.selBufferGrpButt = pm.button(l='Select Buffer Grp', w=120, h=25, c=pm.Callback(self.selectBufferGrp))
				with pm.tabLayout() as self.mainTabLayout:

					# Manage
					with pm.columnLayout(adj=True, rs=0, co=['both', 0]) as self.manageColumnlayout:
						with pm.frameLayout( label='Start', mh=5, mw=5):
							with pm.rowColumnLayout(nc=4, co=[(1, 'left', 50), (2, 'left', 5), (3, 'left', 10), (4, 'left', 5)]):
								pm.text(l='Proj: ')
								with pm.optionMenu(w=60, cc=pm.Callback(self.setProject)) as self.projMenu:
									pm.menuItem(l='')
								pm.text(l='BTP: ')
								with pm.optionMenu(w=160, cc=pm.Callback(self.selectBtpUi)) as self.btpMenu:
									pm.menuItem(l='')
							with pm.rowColumnLayout(nc=3, rs=[1, 5], co=[(1, 'left', 50), (2, 'left', 5), (3, 'left', 5)]):
								self.bufferGrpTxt = pm.text(l='DUPLICATE: ')
								self.bufferGrpTxtFld = pm.textField(w=200, ed=False)
								self.bufferGrpButt = pm.button(l='<<', c=pm.Callback(self.loadBufferGrp))
							with pm.columnLayout(adj=True, rs=5, co=['both', 45]):
								self.prepareForSculptButt = pm.button(l='Prepare for Sculpt', h=30, c=pm.Callback(self.prepareForSculpt))

						with pm.frameLayout( label='Parts', mh=5, mw=5):
							with pm.columnLayout(adj=True, rs=5, co=['left', 30]):
								with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 76), (2, 'left', 5)]):
									self.partTSL = pm.textScrollList(ams=True, h=70, w=190, sc=pm.Callback(self.updateInfo))
									with pm.columnLayout(adj=True , rs= 10):
										self.exportButt = pm.button(l='Export  >>', w=75, h=20, c=pm.Callback(self.exportPart))
										self.importButt = pm.button(l='<<  Import', w=75, h=20, c=pm.Callback(self.importPart))
										pm.text(l='', h=17)
										with pm.rowColumnLayout(nc=2, rs=(1, 5), co=[(1, 'left', 0), (2, 'left', 5)]):
											self.hidePartButt = pm.button(l='hide', w=33, h=20, c=pm.Callback(self.setPartVisibility, False))
											self.showPartButt = pm.button(l='show', w=36, h=20, c=pm.Callback(self.setPartVisibility, True))
								with pm.rowColumnLayout(nc=2, co=[(1, 'left', 81), (2, 'left', 5)]):
									self.addNewPartButt = pm.button(l='New Part', w=88, h=25, c=pm.Callback(self.createNewPart))
									self.removePartButt = pm.button(l='Remove Part', w=88, h=20, c=pm.Callback(self.removePart))
						with  pm.frameLayout(label='Targets', mh=5, mw=5):
							with pm.columnLayout(adj=True, rs=0):
								with pm.rowColumnLayout(nc=3, co=([1, 'left', 70], [2, 'left', 5], [3, 'left', 5])):
									pm.text(l='-15')
									self.testSlider = pm.floatSliderGrp(v=0, max=15, min=-15, cw=[1, 220], ss=0.01, dc=pm.Callback(self.testBsh))
									pm.text(l='+15')
								with pm.rowColumnLayout(nc=2, co=([1, 'left', 130], [2, 'left', 136])):
									pm.text(l='|', h=3)
									pm.text(l='|', h=3)
							with pm.columnLayout(adj=True, rs=5, co=['both', 5]):
								with pm.rowColumnLayout(nc=2, co=([1, 'left', 45], [2, 'left', 5])):
									self.targetTSLN = pm.textScrollList(ams=True, h=100, w=150, sc=pm.Callback(self.selectFocusTarget, 'n'))
									self.targetTSLP = pm.textScrollList(ams=True, h=100, w=150, sc=pm.Callback(self.selectFocusTarget, 'p'))
								with pm.rowColumnLayout(nc=2, co=([1, 'left', 95], [2, 'left', 90])):
									self.removeButtN = pm.button(l='Remove', w=60, h=20, c=pm.Callback(self.removeTarget, self.targetTSLN))
									self.removeButtP = pm.button(l='Remove', w=60, h=20, c=pm.Callback(self.removeTarget, self.targetTSLP))
							with pm.rowColumnLayout(nc=2, rs=([1, 5]), co=([1, 'left', 34], [2, 'left', 5])):
								with pm.columnLayout(adj=True, rs=5):
									with pm.rowColumnLayout(nc=2, rs=([1, 5]), co=([1, 'left', 22], [2, 'left', 5])):
										pm.text(l='Attribute')
										self.addTgtAttrTxtFld = pm.textField(w=195)
									with pm.rowColumnLayout(nc=3, co=([1, 'left', 5], [2, 'left', 5], [3, 'left', 5])):
										pm.text(l='Target Name')
										self.negTgtNameTxtFld = pm.textField(w=94)
										self.posTgtNameTxtFld = pm.textField(w=94)
									with pm.columnLayout(adj=True, rs=5, co=['left', 71]):
										self.addTgtButt = pm.button(l='Add Target', w=60, h=20, c=pm.Callback(self.addNewTarget))
						with  pm.frameLayout(label='Inbetweens', mh=5, mw=5):
							with pm.columnLayout(adj=False, rs=5, co=['left', 106]):
								self.inbTSL = pm.textScrollList(ams=True, h=50, w=195, sc=pm.Callback(self.selectFocusInb))
							with pm.rowColumnLayout(nc=2, co=([1, 'left', 103], [2, 'left', 10])):
								self.addInbTgtButt = pm.button(l='Add Inb', w=95, h=20, c=pm.Callback(self.addInbTarget))
								self.removeInbTgtButt = pm.button(l='Remove Inb', w=95, h=20, c=pm.Callback(self.removeInbTarget))
						with pm.frameLayout( label='Finalize', mh=5, mw=5):
							with pm.columnLayout(adj=True, rs=5, co=['both', 45]):
								with pm.rowColumnLayout(nc=2, co=([1, 'left', 40], [2, 'left', 10])):
									self.connectButt = pm.button(l='Connect to Bffr', h=25, w=100, c=pm.Callback(self.connect))
									self.connectToCtrlButt = pm.button(l='Connect to Ctrl', h=35, w=100, c=pm.Callback(self.connectToCtrl))
								self.disconnectButt = pm.button(l='Disconnect', h=25, c=pm.Callback(self.disconnect))
					# Sculpt
					with pm.columnLayout(adj=True, rs=0, co=['both', 0]) as self.sculptColumnlayout:
						with pm.frameLayout( label='Tools', mh=5, mw=5):
							with pm.rowColumnLayout(nc=2, co=([1, 'left', 145], [2, 'left', 5])):
								pm.text(l='tolerance')
								self.tolFloatField = pm.floatField(w=58, v=0.0001, pre=6, min=0.0, cc=pm.Callback(self.changeTol))
							with pm.columnLayout(adj=True, rs=5, co=['both', 50]):
								pm.text(l='Base Geometry')
							with pm.rowColumnLayout(nc=2, co=([1, 'left', 110], [2, 'left', 15])):
								self.baseGeoTSL = pm.textScrollList(ams=False, h=110, w=170, sc=pm.Callback(self.selectBaseGeo))
								with pm.columnLayout(adj=True, rs=5, co=['both', 0]):
									pm.button(l='Check Symmetry', h=35, w=90, c=pm.Callback(self.checkSymmetryMesh))
									pm.button(l='Sel Moved Vtx', h=20, w=90, c=pm.Callback(self.selectMoveVtxMesh))
									pm.text(l='', h=25)
									self.filterBaseGeoChkBox = pm.checkBox(l='filter selection', v=True, cc=pm.Callback(self.filterBaseGeoSel))
							with pm.columnLayout(adj=True, rs=0, co=['both', 20]):
								self.revertSlider = pm.floatSliderGrp(v=100, max=100, min=0, cw3=[42, 20, 20], f=True, pre=2, el='%',
													dc=pm.Callback(self.dragRevertMesh), cc=pm.Callback(self.endDragRevertMesh))
							with pm.columnLayout(adj=True, rs=5, co=['both', 100]):
								pm.button(l='Reverse to Base', c=pm.Callback(self.revertToBaseMesh))
							with pm.frameLayout( label='Mirror Mesh', mh=5, mw=5):
								pm.text(l='Select multiple meshes.')
								with pm.rowColumnLayout(nc=3, co=([1, 'left', 65], [2, 'left', 10], [3, 'left', 10])):
									pm.button(l='M >>', w=80, h=30, c=pm.Callback(self.mirrorMesh, 'negToPos'))
									pm.button(l='Flip', w=80, h=30, c=pm.Callback(self.flipMesh))
									pm.button(l='<< M', w=80, h=30, c=pm.Callback(self.mirrorMesh, 'posToNeg'))
							with pm.frameLayout( label='Copy Mesh', mh=5, mw=5):
								pm.text(l='Select a source mesh, shift select destination meshes.')
								with pm.rowColumnLayout(nc=3, co=([1, 'left', 65], [2, 'left', 10], [3, 'left', 10])):
									pm.button(l='Subtract', w=80, h=25, c=pm.Callback(self.modAMesh, 'subtractFromMeshA'))
									pm.button(l='Copy', w=80, h=25, c=pm.Callback(self.modAMesh, 'copyFromMeshA'))
									pm.button(l='Add', w=80, h=25, c=pm.Callback(self.modAMesh, 'addFromMeshA'))

								with pm.frameLayout( label='Post Operation', mh=5, mw=5, li=155, fn='smallObliqueLabelFont'):

									with pm.rowColumnLayout(nc=4, co=([1, 'left', 90], [2, 'left', 10], [3, 'left', 10], [4, 'left', 10])):
										self.postCopyRadioCol = pm.radioCollection()
										self.postCopyNoneRadioButt = pm.radioButton(l='none', sl=False)
										self.postCopyFlipRadioButt = pm.radioButton(l='flip', sl=True)	
										self.postCopyNegToPosMirrRadioButt = pm.radioButton(l='M >>', sl=False)
										self.postCopyPosToNegMirrRadioButt = pm.radioButton(l='<< M', sl=False)

					pm.tabLayout( self.mainTabLayout, edit=True, selectTabIndex=1, tabLabel=((self.manageColumnlayout, ' Manage '), 
						(self.sculptColumnlayout, ' Sculpt ')))

		for name in sorted(self.projs.keys()):
			pm.menuItem(l=name, p=self.projMenu)

		#set gap value
		self.gap = self.gap*self.gsFloatFld.getValue()
	
	def addInbTarget(self):
		self.updateInfo()
		self.getCurrentTgtTSL()

		if not self.focusTgts:
			return

		#get width again to make sure
		self.getWidth(self.bufferGrp)

		for name in self.selTgt:
			toAdd = self.focusTgts[name]
			inbs = self.getInbTargets(toAdd)

			inbNum = 0
			if inbs:
				inbNum = len(inbs)
				for i in inbs:
					pm.xform(i, t=[self.width*-1, 0, 0], r=True)

			#recentTgt = toAdd
				
			oldPos = pm.xform(toAdd, q=True, ws=True, t=True)

			position = [oldPos[0] - self.width, oldPos[1], oldPos[2]]

			#duplicate
			inbOrder = str(inbNum+1).zfill(2)
			name = '%s_inb%s' %(toAdd.nodeName(), inbOrder)
			newInb = pm.duplicate(toAdd, n=name)[0]
			pm.xform(newInb, ws=True, t=position)

			self.removeUnusedAttr(newInb, 'TXT')
			self.removeUnusedAttr(newInb, 'p')
			self.removeUnusedAttr(newInb, 'n')
			self.removeUnusedAttr(newInb, 'INB')

			#report
			self.report(toAdd, newInb, 'INB', 'inb_%s' %inbOrder)

		#move all in row
			part =  self.getPartFromTarget(toAdd)
			allTargets = self.getTargetsFromPart(part)
			toMove = []
			txts = []
			for t in allTargets:
				if pm.xform(t, q=True, ws=True, t=True)[0] < oldPos[0]:
					continue
				toMove.append(t)
				txt = self.getTxtFromTarget(t)
				if txt:
					txts.append(txt)
				inbetweens = self.getInbTargets(t)
				if inbetweens:
					toMove.extend(inbetweens)

			pm.xform(toMove, t=(self.width, 0, 0), r=True)
			if txts:
				for txt in txts:
					misc.lockAttr(txt, t=True, lock=False)
				pm.xform(txts, t=(self.width, 0, 0), r=True)
				for txt in txts:
					misc.lockAttr(txt, t=True, lock=True)
			

		#select all in target including inbs
		self.currentTsl.setSelectItem(self.selTgt)
		self.selectFocusTarget(self.currentRangeVal)

	def addNewTarget(self):
		#disconnect rig first
		self.disconnect()

		#get name dic from ui
		self.getFocusPart()
		if not self.focusPart:
			return
		negName = self.negTgtNameTxtFld.getText()
		posName = self.posTgtNameTxtFld.getText()
		attr = self.addTgtAttrTxtFld.getText()

		targetsInPart = self.getTargetsFromPart(self.focusPart)

		#perform security check for crashing target name
		if negName in targetsInPart or posName in targetsInPart:
			om.MGlobal.displayError('Target with this name already exists!, try removing it before adding new one.')
			return

		nameDic = {'p':posName, 'n':negName}

		#calc position
		targetX, targetY = [] ,[]
		#adding to existing part
		if targetsInPart:
			for t in targetsInPart:
				pos = pm.xform(t, q=True, ws=True, t=True)
				targetX.append(pos[0])

			maxX = max(targetX)
			oldPos = pm.xform(targetsInPart[0], q=True, ws=True, t=True)
			position = [maxX+(self.gap*2), oldPos[1], oldPos[2]]
		# adding to newly created part
		else:
			for p in self.partGrps.values():
				targetsInPart = self.getTargetsFromPart(p)
				if not targetsInPart:
					continue
				pos = pm.xform(targetsInPart[0], q=True, ws=True, t=True)
				targetY.append(pos[1])

			maxY = max(targetY)
			z = 0
			nameParts = misc.nameSplit(self.focusPartName)

			if nameParts['pos'] in ['Rht', 'RHT', 'rt', 'RGT', 'Rgt']:
				z = -1
			position = [self.gap*2, maxY+self.gap, self.gap*z]

		#duplicate
		bshObj = btp.BshTarget(self.bufferGrp, nameDic, attr, self.focusPart, position, self.gap)
		bshObj.create()

		toSel = []
		#parent
		if bshObj.obj['p']:
			pm.parent(bshObj.obj['p'], self.focusPart)
			toSel.append(bshObj.obj['p'])

		if bshObj.obj['n']:
			pm.parent(bshObj.obj['n'], self.focusPart)
			toSel.append(bshObj.obj['n'])

		pm.parent(bshObj.obj['txt'], self.txtGrp)
		toSel.extend(bshObj.obj['txt'])

		#update info
		self.updateInfo()
		pm.select(toSel, r=True)

	def browseFile(self, stDir, mode, title, okTitle):
		multipleFilters = "Maya Files (*.ma *.mb);;Maya ASCII (*.ma);;Maya Binary (*.mb);;All Files (*.*)"
		path = pm.fileDialog2(ff=multipleFilters, ds=2, fm=1, dir=stDir, cap=title, okc=okTitle)
		if path:
			return path[0]

	def clearVal(self):
		self.exportPath = ''
		self.importPath = ''
		self.focusPartName = ''

		self.bufferGrp = None
		self.exportGrp = None
		self.tmpGrp = None
		self.txtGrp = None
		self.focusPart = None
		self.bsh = None
		self.selTgt = None

		self.partGrps = {}
		self.targetDic = {}
		self.focusInbsDic = {}
		self.focusTgts = {}
		self.tAttrsDict = {}

		self.tAttrs = []
		self.nodes = []		
		self.allTargets = []
		self.sepAttrs = []	
		self.focusParts = []
		self.focusPartNames = []
		self.selTargets = []	
		self.focusInbs = []
		self.selInbs = []
		self.testTargets = []

	def clearUi(self):
		self.partTSL.removeAll()
		self.targetTSLN.removeAll()
		self.targetTSLP.removeAll()
		self.inbTSL.removeAll()
		self.bufferGrpTxtFld.setText('')
		self.gsFloatFld.setValue(1.0)

	def createNewPart(self):
		if not self.partGrps or not self.bufferGrp:
			return

		newName = self.showNewPartPromptWin()

		if not newName:
			return
		self.focusPartName = newName
		#create new part group
		partGrp = pm.group(n=self.focusPartName, em=True)
		misc.addStrAttr(partGrp, 'PART', self.focusPartName, lock=True)
		self.partGrps[self.focusPartName] = partGrp

		#parent to bshTgtGrp
		pm.parent(self.partGrps[self.focusPartName], self.bshTgtGrp)

		#update part tsl
		self.updatePartTSL()
		self.partTSL.setSelectItem(self.partGrps[self.focusPartName].nodeName())

	def connect(self):
		if not self.targetDic or not self.bufferGrp:
			return

		#disconnect first
		self.disconnect()

		bshNode = pm.blendShape(self.allTargets, self.bufferGrp, n='facialBuffer_bsn', bf=True)[0]
		self.reportBuff(bshNode, '_BSH')
		self.getBsh()

		#add inb targets
		for target in self.allTargets:
			inbTargets = []
			if target.hasAttr('INB'):
				inbTargets = self.getInbTargets(target)
			if inbTargets:
				n = 1
				inbTargetNum = len(inbTargets) + 1
				indx = self.getBshIndexFromTarget(target)
				for inb in inbTargets:
					w = round(float(n)/inbTargetNum, 3)
					pm.blendShape(self.bsh, e=True, ib=True, t=(self.bufferGrp, indx, inb, w))
					n += 1

		for partGrp in sorted(self.partGrps.values()):
			#create sep attr
			sepAttr = misc.addNumAttr(self.bufferGrp, '___%s___' %partGrp.nodeName(), 'double', hide=False, lock=True)
			self.sepAttrs.append(sepAttr)
			partName = partGrp.PART.get()
			attrs = [i for i in pm.listAttr(partGrp, ud=True) if i != "PART"]

			#pBar
			pBar = ui.SmallPBarWindow(txt='Connecting %s' %partGrp.nodeName(), times=len(attrs))
			for attr in attrs:
				#if user hit cancel
				pBar.end(pBar.getCancelled())
				tAttr = misc.addNumAttr(self.bufferGrp, '%s__%s' %(partName, attr), 'double', hide=False)
				self.tAttrs.append(tAttr)

				pTarget, nTarget = None, None
				for plug in pm.listConnections(partGrp.attr(attr), d=True, s=False, p=True):
					if plug.shortName() == 'p':
						pTarget = plug.plugNode()
					elif plug.shortName() == 'n':
						nTarget = plug.plugNode()

				self.createConnection(tAttr, nTarget, pTarget)

				self.tAttrsDict[tAttr] = [nTarget, pTarget]
				#increment pBar for this attr
				pBar.increment()

			#kill pBar for this part
			pBar.end()

		self.updateInfo()
		self.getCurrentTgtTSL()
		self.selectFocusTarget(self.currentRangeVal)
		pm.select(self.bufferGrp, r=True)

	def createConnection(self, source, nTarget, pTarget):
		mdvNodeName = '%s_mdv' %source.shortName()
		mdvNode = pm.createNode('multiplyDivide', n=mdvNodeName)

		clampNodeName = '%s_cmp' %source.shortName()
		clampNode = pm.createNode('clamp', n=clampNodeName)

		clampNode.minR.set(0)
		clampNode.minG.set(0)
		clampNode.maxR.set(999)
		clampNode.maxG.set(999)

		pm.connectAttr(source, mdvNode.input1X, f=True)
		pm.connectAttr(source, mdvNode.input1Y, f=True)

		pm.connectAttr(mdvNode.outputX, clampNode.inputR, f=True)
		pm.connectAttr(mdvNode.outputY, clampNode.inputG, f=True)

		nStatus, pStatus = False, False
		if nTarget:
			nPlug = self.getBshPlug(nTarget)
			if nPlug:
				pm.connectAttr(clampNode.outputR, nPlug, f=True)
				mdvNode.input2X.set(-0.1)
				nStatus = True
		else:
			clampNode.minG.set(-999)

		if pTarget:
			pPlug = self.getBshPlug(pTarget)
			if pPlug:
				pm.connectAttr(clampNode.outputG, pPlug, f=True)
				mdvNode.input2Y.set(0.1)
				pStatus = True
		else:
			clampNode.minR.set(-999)


		if nStatus == True or pStatus == True:
			self.reportBuff(mdvNode, '_NODE')
			self.reportBuff(clampNode, '_NODE')
			print '%s connected.' %(source.shortName())
		else:
			pm.delete(mdvNode)
			pm.warning('Cannot find plug to connect  %s  to  %s and %s' %(source, nTarget, pTarget))

	def connectToCtrl(self):
		sel = misc.getSel()
		if not sel:
			return
		for part in self.getSelectedParts():
			attrs = self.bufferGrp.listAttr(st='%s__*' %part, ud=True)
			for attr in attrs:
				arrtName = attr.attrName().split('__')[-1]
				newAttr = misc.addNumAttr(sel, arrtName, 'double', hide=False)
				newShpAttr = misc.addNumAttr(sel.getShape(ni=True), 'sdk_%s' %arrtName, 'double', hide=False)

				adlNodeName = '%s__%sSdk_adl' %(part, arrtName)
				adlNode = pm.createNode('addDoubleLinear', n=adlNodeName)
				self.reportBuff(adlNode, '_NODE')
				
				pm.connectAttr(newAttr, adlNode.input1)
				pm.connectAttr(newShpAttr, adlNode.input2)

				pm.connectAttr(adlNode.output, attr, f=True)

	def changeTol(self):
		if self.currentSmrObj:
			self.currentSmrObj.tol = self.tolFloatField.getValue()

	def checkSymmetryMesh(self):
		if not self.currentSmrObj:
			return

		self.currentSmrObj.caller(op='checkSymmetry')

	def disconnect(self):
		self.updateInfo()

		#remove existing nodes and attr
		if self.nodes:
			pm.delete(self.nodes)
			self.nodes = []
			print ('Utility Nodes Deleted!')

		if self.bsh:
			pm.delete(self.bsh)
			self.bsh = None
			print ('Blendshape Nodes Deleted!')

		if self.tAttrs:
			for i in self.tAttrs:
				i.delete()
			self.tAttrs = []
			print ('Attribute Deleted!')

		if self.sepAttrs:
			for i in self.sepAttrs:
				i.setLocked(False)
				i.delete()
			self.sepAttrs = []
			print ('Separator Attribute  Deleted!')

	def dragRevertMesh(self):
		if not self.currentSmrObj:
			return

		if self.revertSliderDragging == False:
			self.revertSliderDragging = True
			pm.undoInfo(ock=True)
			if self.currentSmrObj.getMeshASelection(child=False) == True:
				self.currentSmrObj.getAGeoData(self.currentSmrObj.meshA)
				self.currentSmrObj.getMovedVtxMeshA()

		percent = self.revertSlider.getValue()
		self.currentSmrObj.revertVtxByPercent(percent=percent)

	def exportPart(self):
		if not self.bufferGrp:
			return

		toExport = self.getSelectedParts(getTxt=True)

		if not toExport:
			return

		#add buffer grp
		toExport.append(self.bufferGrp)

		#select it first
		pm.select(toExport, r=True)

		#get current dir as export path
		currentDir = '/'.join(pm.sceneName().split('/')[0:-1])
		self.exportPath = self.browseFile(stDir=currentDir, mode=0, title='Share sculpt work with other modeller.', okTitle='Export')

		if not self.exportPath:
			return

		#export selected
		pm.exportSelected(self.exportPath, f=True, ch=False, constraints=False, expressions=False, type='mayaAscii')

		pm.select(cl=True)

	def endDragRevertMesh(self):
		self.revertSliderDragging = False
		self.revertSlider.setValue(100)
		pm.undoInfo(cck=True)

	def flipMesh(self):
		if not self.currentSmrObj:
			return

		self.currentSmrObj.caller(op='flip')

	def filterBaseGeoSel(self):
		if not self.currentSmrObj:
			return

		self.currentSmrObj.filterBaseGeo = self.filterBaseGeoChkBox.getValue()

	def get(self, pNetwork, attr):

		if pNetwork.hasAttr(attr):
			return pm.listConnections(pNetwork.attr(attr), d=True, s=False)

	def getParentNetwork(self, cNode, attr):
		if cNode.hasAttr(attr):
			return pm.listConnections(cNode.attr(attr), s=True, d=False)

	def getTxtGrp(self):
		try:
			txtGrp = pm.listConnections(self.bufferGrp.txtGrp, d=True, s=False)[0]
		except:
			om.MGlobal.displayError('Cannot find textGrp!')
			return

		self.txtGrp = txtGrp 

	def getTxt(self, target):
		try:
			txt = pm.listConnections(target.attr('TXT'), d=True, s=False)[0]
		except:
			return

		return txt

	def getAllTxtInPart(self, part):
		targets = self.getTargetsFromPart(part)
		if not targets:
			return
		txts = []
		for t in targets:
			txt = self.getTxt(t)
			if txt:
				txts.append(txt)
		return txts

	def getTmpGrp(self):
		try:
			tmpGrp = pm.listConnections(self.bufferGrp.tmpGrp, d=True, s=False)[0]
		except:
			om.MGlobal.displayError('Cannot find tmpGrp!')
			return

		self.tmpGrp = tmpGrp

	def getBshTgtGrp(self):
		try:
			bshTgtGrp = pm.listConnections(self.bufferGrp.bshTgtGrp, d=True, s=False)[0]
		except:
			om.MGlobal.displayError('Cannot find blendShape Target Group!')
			return
			
		self.bshTgtGrp = bshTgtGrp 

	def getBufferGrp(self):
		try:
			bufferGrp = pm.listConnections(self.bufferGrp, d=True, s=False)[0]
		except:
			om.MGlobal.displayError('Cannot find bufferGrp!')
			return
			
		self.bufferGrp = bufferGrp
		try:
			self.bufferGrpTxtFld.setText(self.bufferGrp.nodeName())
		except: pass

	def getBshPlug(self, target):
		attrName = target.nodeName()
		if self.bsh.hasAttr(attrName):
			return self.bsh.attr(attrName)
		else:
			return None

	def getInbTargets(self, target):
		if not target.hasAttr('INB'):
			return []
		inbTargetPlugs = pm.listConnections(target.INB, d=True, s=False, p=True)
		inbTargets =  map(lambda plug: plug.plugNode(), sorted(inbTargetPlugs))
		return inbTargets

	def getBshIndexFromTarget(self, target):
		if not self.bsh:
			return
		#get a shape
		shps = filter(lambda b: isinstance(b, pm.nt.Mesh)==True, pm.listRelatives(self.bufferGrp, ad=True))
		tshps = filter(lambda x: isinstance(x, pm.nt.Mesh)==True, pm.listRelatives(target, ad=True))
		if not tshps or not shps:
			return
		shp = shps[0]
		tshp = tshps[0]
		for i in range(self.bsh.numWeights()):
			t = self.bsh.getTargets(shp, i)
			if t:
				t = t[0]
			else: continue
			if t == tshp:
				return i

	def getParts(self):
		if not self.bshTgtGrp:
			return
		children = self.bshTgtGrp.getChildren()
		for c in children:
			if c.hasAttr('PART'):
				self.partGrps[c.nodeName()] = c

	def getTxtFromPart(self, part):
		txts = []
		try:
			targets = part.getChildren()
		except: return []

		for i in targets:
			txts.append(self.getTxt(i))
		return txts

	def getTxtFromTarget(self, target):
		txt = None
		try:
			txt = target.TXT.outputs()[0]
		except: pass
		return txt

	def getPartFromTarget(self, target):
		part = []
		valRangeAttr = [i for i in pm.listAttr(target, ud=True) if i in ['p', 'n']]
		if not valRangeAttr:
			return
		valRangeAttr = valRangeAttr[0]
		try:
			part = pm.listConnections(target.attr(valRangeAttr), s=True)[0]
		except: pass

		return part

	def getSelectedParts(self, getTxt=True):
		#get tsl sel
		selParts = self.partTSL.getSelectItem()

		retList = []
		txts = []
		for s in selParts:
			retList.append(self.partGrps[s])
			if getTxt == True:
				txt = self.getTxtFromPart(self.partGrps[s])
				txts.extend(txt)
		retList.extend(txts)

		return retList

	def getWidth(self, obj):		
		bb = obj.getBoundingBox()
		self.width = abs(bb[1][0] - bb[0][0])

	def getHeight(self, obj):		
		bb = obj.getBoundingBox()
		self.height = abs(bb[1][1] - bb[0][1])

	def getCurrentTgtTSL(self):
		pSels = self.targetTSLP.getSelectItem()
		nSels = self.targetTSLN.getSelectItem()
		if not self.selTgt:
			self.currentRangeVal = 'p'
			return

		if self.selTgt[0] in pSels:
			self.currentTsl = self.targetTSLP
			self.currentRangeVal = 'p'
		else:
			self.currentTsl = self.targetTSLN
			self.currentRangeVal = 'n'

	def getTargetsFromPart(self, part=None):
		if not part:
			return
		attrs = pm.listAttr(part, ud=True)

		targets = []
		for a in attrs:
			attr = part.attr(a)
			if attr.get(type=True) == 'message':
				tgt = pm.listConnections(attr, d=True)
				if tgt:
					targets.extend(tgt)
		return targets

	def getFocusPart(self):
		tslSel = self.partTSL.getSelectItem()
		if not tslSel:
			return
		self.focusPartName = tslSel[0]
		self.focusPart = self.partGrps[self.focusPartName]

	def getFocusParts(self):
		tslSel = self.partTSL.getSelectItem()
		if tslSel:
			self.focusPartNames = tslSel
			self.focusParts = map(lambda x: self.partGrps[x], self.focusPartNames)

	def getAttrFromTarget(self, target):
		valRangeAttr = [i for i in pm.listAttr(target, ud=True) if i in ['p', 'n']]
		if not valRangeAttr:
			return
		valRangeAttr = valRangeAttr[0]
		partAttr = pm.listConnections(target.attr(valRangeAttr), s=True, p=True)
		if not partAttr:
			return []
		return partAttr[0]

	def getTAttrs(self):
		if not self.bufferGrp:
			return 
		udAttrs = pm.listAttr(self.bufferGrp, ud=True, s=True, k=True, v=True, u=True)
		attrs =  map(lambda name: self.bufferGrp.attr(name), udAttrs)
		self.tAttrs = [i for i in attrs if '___' not in i.name()]

	def getTAttrFromBshPlug(self, bshPlug):
		cmps = pm.listConnections(bshPlug, s=True, d=False, p=False, type='clamp')
		clamps = list(set(cmps))
		tAttrs = []
		for c in clamps:
			mdvs = pm.listConnections(c, s=True, d=False, p=False, type='multiplyDivide')
			if mdvs:
				for mdv in mdvs:
					cons = pm.listConnections(mdv, s=True, d=False, p=True, type='transform')
				tAttrs = [c for c in cons if c.shortName() != '_NODE']
		if tAttrs:
			return tAttrs[0]

	def getSepAttrs(self):
		if not self.bufferGrp:
			return 
		udAttrs = pm.listAttr(self.bufferGrp, ud=True, s=True, k=False, v=True, u=False)
		attrs =  map(lambda name: self.bufferGrp.attr(name), udAttrs)
		self.sepAttrs = [i for i in attrs if '___' in i.name()]

	def getNodes(self):
		if not self.bufferGrp:
			return

		self.nodes = self.get(self.bufferGrp, '_NODE')

	def getBsh(self):
		if not self.bufferGrp:
			return
		bshNode = self.get(self.bufferGrp, '_BSH')
		if bshNode:
			self.bsh = bshNode[0]

	def getTargetFromInb(self, inb):
		udAttrs = pm.listAttr(inb, ud=True)
		for attr in udAttrs:
			if attr.startswith('inb_'):
				return self.getParentNetwork(inb, attr)[0]

	def importPart(self):
		if not self.partGrps:
			return

		#get current dir as export path
		currentDir = '/'.join(pm.sceneName().split('/')[0:-1])
		self.importPath = self.browseFile(stDir=currentDir, mode=1, title='Import sculpted blendshape targets.', okTitle='Import')
		fileName = ''
		#import the file
		try:
			fileName = self.importPath.split('/')[-1]
			ns = fileName.split('.')[0]
		except:
			ns = 'IMPORT_BSH'

		if not os.path.exists(self.importPath):
			print self.importPath
			return

		importedObjs = pm.importFile(self.importPath, loadNoReferences=True, returnNewNodes=True, namespace=ns)

		#delete existing ,batch remove prefix
		#parent into proper group
		keep, toRemove, toSel = [], [], []
		iImportGrp, iTmpGrp = None, None

		pBar = ui.SmallPBarWindow(txt='Identifying Nodes...', times=len(importedObjs))
		allShaderTypes = pm.listNodeTypes('shader')
		# print self.partGrps
		for obj in [o for o in importedObjs if o.nodeType()=='transform']:
			#if cancelled
			pBar.end(pBar.getCancelled())
			if obj in keep:
				pBar.increment()
				continue

			#part grp found
			if obj.hasAttr('PART'):
				partName = obj.PART.get().strip()

				if self.partGrps[partName]:
					txts = self.getAllTxtInPart(self.partGrps[partName])
					pm.delete(self.partGrps[partName])
					if txts:
						pm.delete(txts)
				pm.parent(obj, self.bshTgtGrp)
				toSel.append(obj)
				geos = pm.listRelatives(obj, ad=True)
				geos.insert(0, obj)
				keep.extend(geos)
			elif obj.hasAttr('GEO'):
				pm.parent(obj, self.txtGrp)
				crvs = pm.listRelatives(obj, ad=True)
				crvs.insert(0, obj)
				keep.extend(crvs)
				toSel.append(obj)
			# elif obj.type() in allShaderTypes:
				# continue

			# else:
			#  	toRemove.append(obj)

			pBar.increment()
		pBar.end()

		# toRemove.extend([iImportGrp, iTmpGrp, etc])
		# pm.delete(toRemove)

		misc.removeNameSpace(keep)
		self.updateInfo()

		pm.select(toSel, r=True)

	def loadBufferGrp(self, sel=None):
		if not sel:
			sel = misc.getSel()
		try:
			self.bufferGrpTxtFld.setText('')
			self.bufferGrpTxtFld.setText(sel.nodeName())
		except:
			pass

		self.bufferGrp = sel

		if self.bufferGrp:
			misc.addStrAttr(self.bufferGrp, '_TYPE', txt='bufferGrp', lock=True)
			misc.addMsgAttr(self.bufferGrp, '_NODE')
			misc.addMsgAttr(self.bufferGrp, '_BSH')
			misc.hideAttr(self.bufferGrp, t=True, r=True, s=True, v=True)

	def loadExportGrpUi(self, sel=None):
		self.clearUi()
		if not sel:
			sel = misc.getSel()
		try:
			if not sel.hasAttr('_TYPE'):
				self.exportGrpTxtFld.setText('')
				return
			if sel._TYPE.get() != 'bufferGrp':
				self.exportGrpTxtFld.setText('')
				return
			self.exportGrpTxtFld.setText(sel.nodeName())
		except:
			self.exportGrpTxtFld.setText('')
			pass

		self.loadExportGrp(sel)

		#update details
		# self.updatePatternDetail()

		#populate base mesh, load to UI
		baseMeshes = [m for m in pm.listRelatives(self.bufferGrp, ad=True) if isinstance(m, pm.nt.Mesh) and m.isIntermediate() == False]
		self.baseGeoTSL.removeAll()
		for m in baseMeshes:
			tNodeName = m.getParent().nodeName()
			self.baseGeoTSL.append(tNodeName)
			self.baseMeshesDict[tNodeName] = m

	def loadExportGrp(self, sel):
		#clear everything first
		self.clearVal()
		#load
		self.bufferGrp = sel
		self.updateInfo()

		pm.select(cl=True)

	def mirrorMesh(self, op):
		if not self.currentSmrObj:
			return
		if op == 'negToPos':
			self.currentSmrObj.caller(op='mirror_neg')
		else:
			self.currentSmrObj.caller(op='mirror_pos')

	def modAMesh(self, op):
		if not self.currentSmrObj:
			return
		self.currentSmrObj.caller(op=op)

		if self.currentSmrObj.focusVtx:
		#post ops
			if self.postCopyFlipRadioButt.getSelect() == True:
				self.currentSmrObj.caller(op='flip', sels=self.currentSmrObj.focusVtx)
			elif self.postCopyPosToNegMirrRadioButt.getSelect() == True:
				self.currentSmrObj.caller(op='mirror_pos_to_neg', sels=self.currentSmrObj.focusVtx)
			elif self.postCopyNegToPosMirrRadioButt.getSelect() == True:
				self.currentSmrObj.caller(op='mirror_neg_to_pos', sels=self.currentSmrObj.focusVtx)

	def prepareForSculpt(self):
		if not self.btp:
			om.MGlobal.displayError('No blendshape pattern defined! Set project and blendShape template pattern(BTP) first.')
			return
		
		if not self.bufferGrp:
			om.MGlobal.displayError('Please load Buffer Group to be used as original object for duplicating blendShapes.')
			return

		# self.exportGrp = pm.group(n='export_grp', em=True)
		if pm.objExists('|bshTgt_grp'):
			self.bshTgtGrp = pm.PyNode('|bshTgt_grp')
		else:
			self.bshTgtGrp = pm.group(n='bshTgt_grp', em=True)
			
		if pm.objExists('|delete_grp'):
			self.tmpGrp = pm.PyNode('|delete_grp')
		else:
			self.tmpGrp = pm.group(n='delete_grp', em=True)

		# misc.addStrAttr(self.bufferGrp, '_TYPE', txt='exportGrp', lock=True)
		misc.addStrAttr(self.bufferGrp, '_PROJ', txt=self.focusProjName, lock=True)
		misc.addStrAttr(self.bufferGrp, '_BTP', txt=self.focusBtpName, lock=True)
		misc.addStrAttr(self.bufferGrp, '_GAP', txt=self.gap, lock=True)
		misc.addStrAttr(self.tmpGrp, '_TYPE', txt='tmpGrp', lock=True)

		self.btp.create(self.bufferGrp)

		pm.parent(self.btp.partGrps.values(), self.bshTgtGrp)
		pm.parent([self.bshTgtGrp, self.btp.txtGrp], self.tmpGrp)

		self.reportBuff(self.bshTgtGrp, 'bshTgtGrp')
		self.reportBuff(self.tmpGrp, 'tmpGrp')
		self.reportBuff(self.btp.txtGrp, 'txtGrp')

		#load export grp
		self.loadExportGrpUi(self.bufferGrp)

		#clear selection
		pm.select(cl=True)

	def reportBuff(self, obj, attr):
		source = misc.addMsgAttr(self.bufferGrp, attr)
		destination = misc.addMsgAttr(obj, 'networkParent')
		if pm.isConnected(source, destination) == False:
			pm.connectAttr(source, destination, f=True)

	def report(self, pObj, cObj, pAttr, cAttr):
		source = misc.addMsgAttr(pObj, pAttr)
		destination = misc.addMsgAttr(cObj, cAttr)
		if pm.isConnected(source, destination) == False:
			pm.connectAttr(source, destination, f=True)

	def removeUnusedAttr(self, obj, attr):
		try:
			if obj.hasAttr(attr):
				obj.attr(attr).delete()
		except:
			pass

	def removePart(self):
		self.getFocusParts()
		if not self.focusPartNames:
			return
		toDel = None
		for part in self.focusPartNames:
			partGrp = self.partGrps[part]
			toDel = self.getTxtFromPart(partGrp)
			toDel.append(partGrp)
			pm.delete(toDel)
			del self.partGrps[part]

		self.updateInfo()

	def removeTarget(self, tsl):
		if not self.focusTgts:
			return
		selTgt = tsl.getSelectItem()
		if not selTgt:
			return
		i = 0
		for t in selTgt:
			if t == '':
				continue
			partAttr = self.getAttrFromTarget(self.focusTgts[t])
			txt = self.getTxtFromTarget(self.focusTgts[t])
			inbs = self.getInbTargets(self.focusTgts[t])
			pm.delete(self.focusTgts[t], txt)
			if inbs:
				pm.delete(inbs)
			if partAttr.isConnected() == False:
				partAttr.delete()
			i += 1
		if i > 0:
			self.updateInfo()

	def removeInbTarget(self):
		self.updateInfo()
		self.getCurrentTgtTSL()

		if not self.focusTgts:
			return

		self.getWidth(self.bufferGrp)
		self.getFocusParts()
		for part in self.focusParts:
			for target in self.getTargetsFromPart(part):
				inbs = self.getInbTargets(target)
				if not inbs:
					continue
				for inb in inbs:
					if inb in self.selInbs:
						delPos = pm.xform(inb, q=True, ws=True, t=True)
						pm.delete(inb)
						toMove, txts = [], []
						for t in self.getTargetsFromPart(part):		
							if pm.xform(t, q=True, ws=True, t=True)[0] > delPos[0]:
								toMove.append(t)
								inbetweens = self.getInbTargets(t)
								if inbetweens:
									moveInb = [tInb for tInb in inbetweens if pm.xform(tInb, q=True, ws=True, t=True)[0] > delPos[0]]
									toMove.extend(moveInb)
								txt = self.getTxtFromTarget(t)
								if txt:
									txts.append(txt)

						pm.xform(toMove, t=[self.width*-1, 0, 0], r=True)
						if txts:
							for txt in txts:
								misc.lockAttr(txt, t=True, lock=False)
							pm.xform(txts, t=[self.width*-1, 0, 0], r=True)
							for txt in txts:
								misc.lockAttr(txt, t=True, lock=False)

				if not self.getInbTargets(target):
					target.INB.delete()

		#select all in target including inbs
		self.currentTsl.setSelectItem(self.selTgt)
		self.selectFocusTarget(self.currentRangeVal)

	def revertToBaseMesh(self):
		if not self.currentSmrObj:
			return
		self.currentSmrObj.caller(op='revertToBase')

	def resetTestSlider(self):
		
		self.testSlider.setValue(0)

		for attr in self.testTargets:
			attr.set(0)
		self.testTargets = []

	def setProject(self):
		try:
			self.focusProjName = self.projMenu.getValue()
			self.btpMenu.clear()
			pm.menuItem(l='', p=self.btpMenu)
		except: pass

		if not self.focusProjName:
			self.currentProj = None
			return
		self.currentProj = self.projs[self.focusProjName]
		reload(self.currentProj)

		members = inspect.getmembers(sys.modules[self.currentProj.__name__], inspect.isclass)
		classes = []
		for m in members:
			classes.append(m[1])

		for c in sorted(classes, key=lambda cls: cls.__name__):
			clsName = c.__name__
			self.btps[clsName] = c
			try:
				pm.menuItem(l=clsName, p=self.btpMenu)
			except:
				pass

	def setPartVisibility(self, vis=True):
		self.getFocusParts()
		toHide = []
		for part in self.focusParts:
			toHide.append(part)
			txts = self.getTxtFromPart(part)
			if txts:
				toHide.extend(txts)

		for item in toHide:
			if not item:
				continue
			if item.hasAttr('visibility'):
				vAttr = item.attr('visibility')
				if vAttr.isLocked() == True:
					vAttr.setLocked(False)
				vAttr.set(vis)

	def showNewPartPromptWin(self):
		pTxtFld = ui.PromptTxtFld(title='New Part', message='Name:')
		return pTxtFld.txt

	def selectBtpUi(self):
		self.focusBtpName = self.btpMenu.getValue()
		self.selectBtp()
		# self.updatePatternDetail()

	def selectBtp(self):
		if not self.focusBtpName:
			self.btp = None
			return
		self.btp = self.btps[self.focusBtpName](self.gap)

	def selectBufferGrp(self):
		if not self.bufferGrp:
			return
		pm.select(self.bufferGrp, r=True)

	def selectFocusInb(self):
		self.selInbs = []
		selected = self.inbTSL.getSelectItem()
		for inb in self.focusInbs:
			if inb.nodeName() in selected:
				self.selInbs.append(inb)
		if self.selInbs:
			pm.select(self.selInbs, r=True)

	def selectFocusTarget(self, rangeVal):
		self.resetTestSlider()

		if rangeVal == 'p':
			tsl = self.targetTSLP
			ptsl = self.targetTSLN
		elif rangeVal == 'n':
			tsl = self.targetTSLN
			ptsl = self.targetTSLP

		self.selTgt = tsl.getSelectItem()
		selTgtIndex = tsl.getSelectIndexedItem()
		ptsl.deselectAll()
		ptsl.setSelectIndexedItem(selTgtIndex)

		self.selTargets = []
		self.focusInbs = []
		self.focusInbsDic = {}
		self.testTargets = []
		toSel = []

		for sel in self.selTgt:
			if sel not in self.focusTgts.keys() or not sel:
				continue
			target = self.focusTgts[sel]
			if target:
				inbs = self.getInbTargets(target)
				if inbs:
					toSel.extend(inbs)
				toSel.append(target)
				self.selTargets.append(target)
				self.focusInbsDic[target] = inbs

		#set up for slider
		if not self.tAttrsDict:
			om.MGlobal.displayWarning('Cannot operate test slider. Please press Connect to Bffr button and try again.')
		for t in self.selTargets:
			for k, v in self.tAttrsDict.iteritems():
				if t in v:
					if k.isConnected():
						testTgt = k.inputs(p=True)[0]
					else:
						testTgt = k
					self.testTargets.append(testTgt)

		#select targets
		if not toSel:
			pm.select(cl=True)
		else:
			pm.select(toSel, r=True)

		self.updateInbTSL()

	def selectBaseGeo(self):
		tslSel = self.baseGeoTSL.getSelectItem()
		if not tslSel:
			return
		self.currentBaseGeo = self.baseMeshesDict[tslSel[0]]

		#turn off bsh evelope first
		if self.bsh:
			self.bsh.envelope.set(0)

		sels = pm.selected()

		#instance symMeshGeo object
		self.currentSmrObj = smr.SymMeshReflector(tol=self.tolFloatField.getValue())
		self.currentSmrObj.analyzeBaseGeo(self.currentBaseGeo.longName())

		#turn envelope back on
		if self.bsh:
			self.bsh.envelope.set(1)

		if sels:
			pm.select(sels, r=True)

	def selectMoveVtxMesh(self):
		if not self.currentSmrObj:
			return

		self.currentSmrObj.caller(op='getMovedVtx')

	def testBsh(self):
		currentValue = self.testSlider.getValue()
		for attr in self.testTargets:
			attr.set(currentValue)

	def updateGlobalScale(self):
		fldVal = self.gsFloatFld.getValue()
		self.gap = self.gap * fldVal

	def utilizePattern(self):
		#get file in btp
		for f in os.listdir(os.path.dirname(btp.__file__)):
			if f.endswith('.py') and not f.startswith('__') and f != 'btpBase.py':
				modName = f.split('.')[0]
				exec('from btp import %s' %modName)
				exec('self.projs[modName] = %s' %modName)

	def updateTargetTSL(self):
		preSelN = self.targetTSLN.getSelectItem()
		preSelP = self.targetTSLP.getSelectItem()

		self.targetTSLP.removeAll()
		self.targetTSLN.removeAll()
		self.focusTgts = {}

		selParts = self.getSelectedParts(getTxt=False)
		if not selParts:
			return

		for part in selParts:
			targetDic = self.targetDic[part.nodeName()]
			for kAttr, dic in sorted(targetDic.iteritems()):
				for rangeVal, target in sorted(dic.iteritems()):
					if target:
						if rangeVal == 'p':
							self.targetTSLP.append(target.nodeName())
							self.focusTgts[target.nodeName()] = target
							continue
						elif rangeVal == 'n':
							self.targetTSLN.append(target.nodeName())
							self.focusTgts[target.nodeName()] = target
							continue
					else:
						if rangeVal == 'p':
							self.targetTSLP.append('')
						elif rangeVal == 'n':
							self.targetTSLN.append('')

		toSelN = [i for i in preSelN if i in self.targetTSLN.getAllItems()]
		self.targetTSLN.setSelectItem(toSelN)
		toSelP = [i for i in preSelP if i in self.targetTSLP.getAllItems()]
		self.targetTSLP.setSelectItem(toSelP)

	def updatePartTSL(self):
		preSel = self.partTSL.getSelectItem()
		self.partTSL.removeAll()
		for part in sorted(self.partGrps.keys()):
			self.partTSL.append(part)
		toSel = [sel for sel in preSel if sel in self.partTSL.getAllItems()]
		if toSel:
			self.partTSL.setSelectItem(toSel)

	def updateInbTSL(self):
		self.inbTSL.removeAll()
		self.focusInbs = []
		for inbs in self.focusInbsDic.values():
			for inb in inbs:
				self.inbTSL.append(inb.nodeName())
				self.focusInbs.append(inb)

	def updateInfo(self):
		if not self.bufferGrp:
			return
		
		self.focusProjName = self.bufferGrp._PROJ.get()
		self.focusBtpName = self.bufferGrp._BTP.get()
		self.gap = float(self.bufferGrp._GAP.get())


		#set proj and btp

		try:
			self.projMenu.setValue(self.focusProjName)
			self.setProject()	
		except: pass
		
		try:
			self.btpMenu.setValue(self.focusBtpName)
			self.selectBtp()
		except: pass

		self.getTmpGrp()
		self.getTxtGrp()

		self.getBshTgtGrp()
		# self.getBufferGrp()
		self.getParts()

		self.getWidth(self.bufferGrp)
		self.getHeight(self.bufferGrp)
		
		self.targetDic = {}
		self.allTargets = []


		#get connection info
		self.getNodes()
		self.getBsh()
		self.getTAttrs()
		self.getSepAttrs()

		for part in sorted(self.partGrps.values()):
			targetsAttr = []
			attrDic = {}
			tNum = 1
			attrs = [attr for attr in pm.listAttr(part, ud=True) if attr != "PART"]
			if not attrs:
				self.targetDic[part.nodeName()] = {}
			for a in attrs:
				attr = part.attr(a)
				if not attr.get(type=True) == 'message':
					continue
				targetsAttr = pm.listConnections(attr, d=True, s=False, p=True)
				tDic = {'p':None, 'n':None}
				tAttr = None
				for t in sorted(targetsAttr):
					targetRange = t.shortName()
					target = t.plugNode()

					tDic[targetRange] = target
					self.allTargets.append(target)
					tNumStr = str(tNum)
					attrDic['%s%s' %(tNumStr.zfill(2), attr.shortName())] =  tDic
					self.targetDic[part.nodeName()] = attrDic
					try: 
						bshPlug = self.getBshPlug(target)
						tAttr = self.getTAttrFromBshPlug(bshPlug)
					except: pass
				if tAttr:
					self.tAttrsDict[tAttr] = [tDic['n'], tDic['p']]
				tNum += 1

		try:
			#update part txtFld
			self.updatePartTSL()

			#update target TSL
			self.updateTargetTSL()

			#update inb TSL
			self.updateInbTSL()

			#update gs floatFld
			self.gsFloatFld.setValue(self.gap/5)

			#reset testSlider
			self.resetTestSlider()
		except: pass



	


