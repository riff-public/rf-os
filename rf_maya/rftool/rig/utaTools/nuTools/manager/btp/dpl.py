from nuTools.manager import btp

class AnimalExtra03(btp.BtpBase):

    def __init__(self, gap=5):
        btp.BtpBase.__init__(self, gap)

        self.mouthMID = {'NAME':'mouthMID', 'SIDE':'MID',
                        '01mouth_O'        :       {'p':'mouth_O',            'n':''},
                        '02mouth_U'        :       {'p':'mouth_U',       'n':''}}   

        self.parts = [self.mouthMID]