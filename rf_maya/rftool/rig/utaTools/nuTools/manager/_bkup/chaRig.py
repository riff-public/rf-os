import pymel.core as pm
import os, socket, inspect, sys, pkgutil
from nuTools import misc, config, ui, template
from nuTools.util import symMeshReflector as smr
import maya.OpenMaya as OpenMaya

reload(misc)
reload(config)
reload(ui)
reload(template)
# reload(rigTools)
reload(smr)



class ChaRig(object):

	def __init__(self, gap=5):
		self.WINDOW_TITLE = 'Character Rig Manager v3.3'
		self.WINDOW_NAME = 'crmWin'

		self.gap = gap
		self.exportPath = ''
		self.refPath = ''
		self.rigDescription = ''
		self.currentPackageName = ''

		self.ref 			= None
		self.rootCtrl 		= None
		self.rgAttr 		= None
		self.partsGrp 		= None
		self.currentRigUi 	= None
		self.currentBaseUi 	= None

		self.skinStatus		= False
		self.dtlStatus		= False
		self.bshStatus 		= False

		self.meshDict 		= {'head':None, 'body':None}
		self.grpDict 		= {'mainGeoGrp':None, 'fullBufferGeoGrp':None, 'partBufferGeoGrp':None, 'partDtlGeoGrp':None, 'partSkinGeoGrp':None, 'partBshGeoGrp':None}
		self.bshNodeDict 	= {'defBshNode':None, 'bufferBshNode':None, 'mainBshNode':None}

		self.tempClsDict 	= {}
		self.tempDict 		= {}
		self.rigModUiDict 	= {}
		self.baseUiDict 	= {}
		self.bshAttrDict 	= {}

		self.defGrpKeys 	= ['partDtlGeoGrp', 'partSkinGeoGrp', 'partBshGeoGrp']

		#get ip, and auto-fill user
		# ipAddress = socket.gethostbyname(socket.gethostname())
		# try :
		# 	self.user = config.USER_IP[str(ipAddress)]
		# except:
		# 	self.user = 'User'
		# self.user = misc.getUserFromEnvVar(title=False)	


	def UI(self):
		"""
		The main UI function.

		"""
		if pm.window(self.WINDOW_NAME, ex=True):
			pm.deleteUI(self.WINDOW_NAME, window=True)
		with pm.window(self.WINDOW_NAME, title=self.WINDOW_TITLE, s=False, mnb=True, mxb=False) as self.mainWindow:
			with pm.frameLayout( label='Manage Character Rig from start to finish!', borderStyle='out', mh=5, mw=5, fn='smallObliqueLabelFont'):
				with pm.columnLayout(adj=True, co=['both', 5]):
					with pm.rowColumnLayout(nc=3, co=[(1, 'left', 5), (2, 'left', 5), (3, 'left', 5)]):
						self.rootCtrlTxt = pm.text(l='Root_Ctrl: ')
						self.rootCtrlTxtFld = pm.textField(text='<< Please Load Root_Ctrl >>', w=300, ed=False, bgc=[0.4,0,0])
						self.rootCtrlButt = pm.button(l='<<', c=pm.Callback(self.loadRootCtrl))
			with pm.tabLayout() as self.mainTabLayout:

				with pm.columnLayout(adj=True, rs=0, co=['both', 0]) as self.prepColumnLayout:
					with pm.frameLayout( label='Clean Up Model', borderStyle='out', mh=5, mw=5):
						with pm.rowColumnLayout(nc=3, co=[(1, 'left', 23), (2, 'left', 15), (3, 'left', 15)]):
							
							pm.button(l='Clean Transform', w=110, c=pm.Callback(self.cleanMesh, 'freezeMovePivDeleteHis'))
							pm.button(l='Clean Shade', w=100, c=pm.Callback(self.cleanMesh, 'cleanShade'))
							pm.button(l='Check Symmetry', w=100, c=pm.Callback(self.cleanMesh, 'checkSymmetry'))

					with pm.frameLayout( label='Import Rig_Grp', borderStyle='out', mh=5, mw=5):
						with pm.columnLayout(adj=True, co=['both', 25]):
							self.importRigGrpButt = pm.button(l='Import Rig Grp', h=45, c=pm.Callback(self.importRigGrp))
					with pm.frameLayout( label='Prepare Group', borderStyle='out', mh=5, mw=5):
						with pm.columnLayout(adj=True, rs=5, co=['both', 25]):	
							
							self.cutButt = pm.button(l='Cut Mesh', h=35, w=80, c=pm.Callback(self.cutMesh))
							with pm.rowColumnLayout(nc=3, rs=((1, 5), (2,5)),
								co=[(1, 'left', 5), (2, 'left', 5), (3, 'left', 5)]):
								self.modelTxt = pm.text(l='Full Model: ')
								self.modelTxtFld = pm.textField(w=235, ed=False)
								self.loadModelButt = pm.button(l='<<', c=pm.Callback(self.loadModel))
								self.partialModelTxt = pm.text(l='Cut Model: ')
								self.partialModelTxtFld = pm.textField(w=235, ed=False)
								self.loadpartialModelButt = pm.button(l='<<', c=pm.Callback(self.loadPartialModel))
								self.partGrpTxt = pm.text(l='Parts Grp: ')
								self.partsGrpTxtFld = pm.textField(w=235, ed=False)
								self.loadPartsGrpButt = pm.button(l='<<', c=pm.Callback(self.loadParts))
							
							self.duplicateButt = pm.button(l='Create Group', h=40, w=80, c=pm.Callback(self.createGrp))
							
					with pm.frameLayout( label='Export Model', borderStyle='out', mh=5, mw=5):
						with pm.rowColumnLayout(nc=3, rs=((1, 5), (2,5)),
							co=[(1, 'left', 35), (2, 'left', 5), (3, 'left', 5)]):
							self.exportPathTxt = pm.text(l='Path: ')
							self.exportPathTxtFld = pm.textField(w=240)
							self.exportPathBrowseButt = pm.button('...', w=26, c=pm.Callback(self.browseCall, 'export_bsh'))
						with pm.columnLayout(adj=True, rs=5, co=['both', 25]):	
							self.exportButt = pm.button(l='Export', h=45, w=81, c=pm.Callback(self.modelExport))

				with pm.columnLayout() as self.groupingColumnLayout:
					with pm.frameLayout( label='Reference BlendShape', borderStyle='out', mh=5, mw=5, w=400):
						with pm.rowColumnLayout(nc=4, co=[(1, 'left', 5), (2, 'left', 5), (3, 'left', 5), (4, 'left', 5)]):
							pm.text(l='Ref Path: ')
							self.refPathTxtFld = pm.textField(w=223)
							self.browseRefButt = pm.button(l='...', c=pm.Callback(self.browseCall, 'ref_bsh'))
							self.createRefButt = pm.button(l='Reference', c=pm.Callback(self.createRef))
					with pm.frameLayout( label='Deform Groups', borderStyle='out', mh=15, mw=5, w=400):
						with pm.rowColumnLayout(nc=3, rs=((1, 15), (2,15)), co=[(1, 'both', 15), (2, 'both', 10), (3, 'both', 15)]):
							self.skinTxt = pm.text(l='Skin', bgc=[0.3,0,0], h=25)
							self.dtlTxt = pm.text(l='Dtl', bgc=[0.3,0,0], h=25)
							self.bshTxt = pm.text(l='BlendShape', bgc=[0.3,0,0], h=25)
							self.connectSkinGeoButt = pm.button(l=' Connect\nDisconnect ', h=50, w=100, c=pm.Callback(self.connectBshCall, 'skin'))
							self.connectDtlGeoButt = pm.button(l=' Connect\nDisconnect ', h=50, w=100, c=pm.Callback(self.connectBshCall, 'dtl'))
							self.connectBshGeoButt = pm.button(l=' Connect\nDisconnect ', h=50, w=100, c=pm.Callback(self.connectBshCall, 'bsh'))
					with pm.frameLayout( label='Buffer Groups', borderStyle='out', mh=15, mw=5, w=400):
						with pm.columnLayout(adj=True, rs=15, co=['both', 100]):	
							self.bufferTxt = pm.text(l='Partial Buffer  >>>  Full Buffer', bgc=[0.3,0,0], h=25)
							self.bufferConnectButt = pm.button(l=' Connect\nDisconnect ', h=50, c=pm.Callback(self.connectBshCall, 'buffer'))
							self.mainTxt = pm.text(l='Main', bgc=[0.3,0,0], h=25)
							self.mainConnectButt = pm.button(l=' Connect\nDisconnect ', h=50, c=pm.Callback(self.connectBshCall, 'main'))
					with  pm.frameLayout(label='Controls', borderStyle='out', mh=5, mw=5, cll=True, cl=True, w=400):
						pm.text(l='Choose part from drop-down menu, select a ctrl and hit connect.')
						with pm.rowColumnLayout(nc=3, co=[(1, 'left', 44), (2, 'left', 5), (3, 'left', 10)]):
							pm.text(l='Bsh Parts: ')
							with pm.optionMenu(w=186, cc=pm.Callback(self.checkTempClass)) as self.bshPartMenu:
								pm.menuItem(l='')
							self.connectBshToCtrlButt = pm.button(l=' Connect ', h=25, c=pm.Callback(self.connectBshToCtrl))

				with pm.columnLayout(w=388) as self.rigColumnLayout:
					with  pm.frameLayout(label='Template', borderStyle='out', mh=5, mw=5, cll=True, cl=False):
						with  pm.frameLayout(label='Import', borderStyle='out', mh=5, mw=5, cll=True, cl=False):
							with pm.rowColumnLayout(nc=4, co=[(1, 'left', 5), (2, 'left', 5), (3, 'left', 5), (4, 'left', 5)]):
								pm.text(l='Path: ')
								self.importTempTxtFld = pm.textField(w=215, ed=True)
								self.importTempBrowseButt = pm.button(l='...', c=pm.Callback(self.browseCall, 'import_temp'))
								self.importButt = pm.button(l='Import Template', c=pm.Callback(self.importTemplate))
						with  pm.frameLayout(label='Create', borderStyle='out', mh=5, mw=5, cll=True, cl=True):
							with pm.rowColumnLayout(nc=2, co=[(1, 'left', 5), (2, 'left', 15)]):
								with pm.columnLayout(adj=True, rs=5, co=['left', 5]):	
									with pm.rowColumnLayout(nc=2, co=[(1, 'left', 20), (2, 'left', 5)]):
										pm.text(l='Template: ')
										with pm.optionMenu(w=202, cc=pm.Callback(self.checkTempClass)) as self.tempCreateMenu:
											pm.menuItem(l='')
									with pm.rowColumnLayout(nc=4, co=[(1, 'left', 30), (2, 'left', 5), (3, 'left', 10), (4, 'left', 5)]):
										pm.text(l='Element')
										self.tempElemTxtFld = pm.textField(tx='Limb', w=120, ed=True, text='Limb')
										pm.text(l='Side')
										self.tempSideTxtFld = pm.textField(tx='', w=50, ed=True)
								self.tempMirrorChkBox = pm.checkBox(l='mirror', v=False)
							with pm.columnLayout(adj=True, rs=5, co=['both', 40]):
								with pm.rowColumnLayout(nc=2, co=[(1, 'left', 20), (2, 'left', 5)]):
									pm.text(l='Template Scale: ')
									self.tempScaleFloatSliderGrp = pm.floatSliderGrp(f=True, v=1, max=10, min=0.01, fs=0.1, cw=([1,28], [2,150]))
									pm.text(l='Joint Num: ')
									self.tempJntNumIntSliderGrp = pm.intSliderGrp(f=True, v=4, max=30, min=1, fs=1, cw=([1,28], [2,150]), en=False)		
								self.createTempButt = pm.button(l='Create', w=100, h=37, c=pm.Callback(self.createTemplate))
								with pm.rowColumnLayout(nc=2, co=[(1, 'left', 55), (2, 'left', 30)]):
									self.connectTempButt = pm.button(l='Parent', w=80, c=pm.Callback(self.connectTemplate))
									self.disConnectTempButt = pm.button(l='Un-parent', w=80, c=pm.Callback(self.disconnectTemplate))
								with pm.rowColumnLayout(nc=2, co=[(1, 'left', 55), (2, 'left', 10)]):
									self.mirrorTempButt = pm.button(l='Mirror', w=80, c=pm.Callback(self.mirrorTemplate))
									self.mirrorConnectChkBox = pm.checkBox(l='connect mirror', v=True)
									#self.disconnectTempButt = pm.button(l='Disconnect Mirror Connection', w=80, c=pm.Callback(self.connectTemplate))			
							#with pm.columnLayout(adj=True, rs=5, co=['both', 80]):	
								#self.saveTempButt = pm.button(l='Save Template', h=25)
						self.bakeTempButt = pm.button(l='Bake Template', h=35, c=pm.Callback(self.bakeTemplates))

					with  pm.frameLayout(label='Rig Module', borderStyle='out', mh=5, mw=5, cll=True, cl=False):
						with pm.columnLayout(adj=True, w=388):
							with pm.rowColumnLayout(nc=2, co=[(1, 'left', 60), (2, 'left', 5)]):
								pm.text(l='Modules: ')
								with pm.optionMenu(w=202, cc=pm.Callback(self.updateRigSettings)) as self.rigModuleMenu:
									pm.menuItem(l='')
						with pm.columnLayout(adj=True):
							self.rigDescriptionTxt = pm.text(l=self.rigDescription)
						with  pm.frameLayout(label='Settings', borderStyle='in', mh=3, mw=3, cll=True, cl=False):
							#with pm.scrollLayout() as self.settingScrollLayout:
							with pm.columnLayout(adj=True) as self.rigModuleSettingColumnLayout:
								pass
						self.rigButt = pm.button(l='Rig', h=35, c=pm.Callback(self.callRig))
				with pm.columnLayout() as self.extraColumnLayout:
					pass
				pm.tabLayout( self.mainTabLayout, edit=True, selectTabIndex=3, 
					tabLabel=((self.prepColumnLayout, 'Prepare Model'), 
							(self.groupingColumnLayout, ' Connecting '),
							(self.rigColumnLayout, '     Rig     '),
							(self.extraColumnLayout, '    Extra    ')))

		#find all UIs for rig template
		self.getRigTemplateUis()

		#find all UIs for rig modules 
		self.getRigModulesUis()



	def getRigTemplateUis(self):
		tempClasses = inspect.getmembers(sys.modules[template.__name__], inspect.isclass)
		for cls in sorted(tempClasses)[::-1]:
			if cls[0] in config.TEMP_CLS_FILTER:
				continue
			pm.menuItem(l=cls[0], p=self.tempCreateMenu, ia='')
			self.tempClsDict[cls[0]] = cls[1]


	def getRigModulesUis(self):
		# print config.RIG_MOD_FILTER

		# print config.RIG_MOD_PKG_DIR
		for packageName, directory in config.RIG_MOD_PKG_DIR.iteritems():
			rigMods = [name for _, name, _ in pkgutil.iter_modules([directory])]
			# packageName = directory.split('/')[-1]
			
			for mod in sorted(rigMods)[::-1]:
				# filter out unwanted modules
				if mod in config.RIG_MOD_FILTER:
					continue
				# add to option menu
				pm.menuItem(l='%s.%s' %(packageName, mod), p=self.rigModuleMenu, ia='')

			try:
				exec('import %s.crm_ui as crmUi' %packageName)
				reload (crmUi)
				rigUi = inspect.getmembers(sys.modules[crmUi.__name__], inspect.isclass)
				
				for ui in rigUi:
					if ui[0] == 'BaseUi':
						self.baseUiDict[packageName] = ui[1](parent=self.rigModuleSettingColumnLayout)
						continue
					self.rigModUiDict['%s.%s' %(packageName,ui[0])] = ui[1]
			except Exception, e:
				print e
				OpenMaya.MGlobal.displayWarning('Cannot load rig ui for:  %s' %packageName)



	def analyzeTemplate(self, tempRoot):
		clsName = tempRoot._TEMPLATE.get()
		elem = tempRoot.elem.get()
		side = tempRoot.pos.get()

		tmpJnts = sorted(tempRoot.tmpJnts.outputs())
		subCtrls = sorted(tempRoot.subCtrls.outputs())

		if not clsName in self.tempClsDict.keys():
			return
		temp = self.tempClsDict[clsName]()

		#elem and side are string
		if elem:
			temp.elem = elem
		if side:
			temp.pos = side
		temp.tmpJnts = tmpJnts
		temp.rootCtrl = tempRoot

		try:
			tempScale = tempRoot.tempScale.get()
			temp.scale = float(tempScale)

			ctrlAxis = tempRoot.ctrlAxis.get()
			temp.ctrlAxis = ctrlAxis

			if self.tempJntNumIntSliderGrp.getEnabled() == True:
				jntNum = tempRoot.tempJntNum.get()
				temp.num = float(jntNum)
	
		except: pass

		#get tmpJntDict by label
		for tj in temp.tmpJnts:
			label = tj._LABEL.get()
			temp.tmpJntDict[label] = tj

		#subtemplates
		for sr in subCtrls:
			name = sr.elem.get()
			temp.subCtrls[name] = sr

		return temp



	def annotateGrp(self, grp, name):
		annShp = pm.createNode('annotationShape')
		trans = annShp.getParent()
		trans.rename('%s_ann' %name)
		
		annShp.displayArrow.set(False)
		annShp.text.set(name)

		bb = grp.getBoundingBox()
		grpWs = pm.xform(grp, q=True, ws=True, t=True)
		lowY = bb.min().y
		tenthGap = self.gap * 0.1
		pm.xform(trans, ws=True, t=(grpWs[0], lowY, grpWs[2]))
		misc.setDisplayType(trans, False, 'ref')
		pm.parent(trans, self.grpDict['miscGrp'])



	def bakeTemplates(self):
		if not self.rootCtrl:
			selTemp = misc.getSel(num='inf')
			if not selTemp:
				OpenMaya.MGlobal.displayError('Please load Root_Ctrl or select any template rootCtrl you want to bake.')
				return
			self.tempDict = {}
			for sel in selTemp:
				if sel.hasAttr('_SUBTYPE'):
					if not sel._SUBTYPE.get() == 'TEMP_Controller':
						continue
					tempName = '%s%s' %(sel.elem.get(), sel.pos.get())
					tempObj = self.analyzeTemplate(sel)
					self.tempDict[tempName] = tempObj
		else:
			self.getTemplate()
		if len(self.tempDict) == 0:
			OpenMaya.MGlobal.displayWarning('No template to bake.')
			return

		template.bake(self.tempDict.values())
		self.tempDict = {}



	def browseCall(self, op):
		stDir = ''
		if op == 'export_bsh':
			if not self.exportPath:
				stDir = pm.sceneName()
			else:
				stDir = self.exportPath
			path = self.browseFile(stDir, 0, 'Export to')
			if path:
				dirPath = '/'.join(path.split('/')[0:-1])
				if os.path.exists(dirPath):
					self.exportPath = path
					self.exportPathTxtFld.setText(self.exportPath)

		elif op == 'ref_bsh':
			try: stDir = misc.getPipelinePath('model', subDept='bsh', fileInc=False, work=False)
			except: stDir = '/'.join(pm.sceneName().split('/')[0:-1])
			path = self.browseFile(stDir, 1, 'Create Reference')
			if path:
				if os.path.exists(path):
					self.refPath = path
					self.refPathTxtFld.setText(self.refPath)

		elif op == 'import_temp':
			try: 
				stDir = config.TEMPLATE_DIR
			except: stDir = '/'.join(pm.sceneName().split('/')[0:-1])
			path = self.browseFile(stDir, 1, 'Import Template')
			if path:
				if os.path.exists(path):
					self.importTempTxtFld.setText(path)



	def browseFile(self, stDir, mode, title):
		multipleFilters = "Maya Files (*.ma *.mb);;Maya ASCII (*.ma);;Maya Binary (*.mb);;All Files (*.*)"
		path = pm.fileDialog2(ff=multipleFilters, ds=1, fm=mode, dir=stDir, cap=title)
		if path:
			return path[0]



	def checkTempClass(self):
		currentCls = self.tempCreateMenu.getValue()
		if currentCls in template._CUSTOMIZEABLE_JNT_NUM_CLS:
			self.tempJntNumIntSliderGrp.setEnable(True)
		else: 
			self.tempJntNumIntSliderGrp.setEnable(False)



	def connectTemplate(self):
		sels = misc.getSel(num='inf')
		if len(sels) < 2:
			return
		child = sels[0:-1]
		parent = sels[-1]
		if isinstance(child, (list,tuple)) == False:
			child = [child]

		pm = template.getMirrored(parent)
		if pm:
			pm = pm[0]
		else:
			pm = parent

		for c in child:
			template.connectTempJnts(c, parent)
			cm = template.getMirrored(c)
			if pm:
				for m in template.getMirrored(c):
					template.connectTempJnts(m, pm)



	def clearRigUi(self):
		try:
			self.baseUiDict[self.currentPackageName].clearUi()
		except: 
			OpenMaya.MGlobal.displayWarning('No rig module UI for module  :%s' %self.currentPackageName)
		self.currentPackageName = ''
		self.currentRigUi = None
		self.currentBaseUi = None



	def callRig(self):
		self.currentRigUi.call()



	def clearVal(self):
		self.rootCtrl = None
		self.partsGrp = None
		self.meshDict = {'head':None, 'body':None}
		self.grpDict = {'mainGeoGrp':None, 'fullBufferGeoGrp':None, 'partBufferGeoGrp':None, 'partDtlGeoGrp':None, 
						'partSkinGeoGrp':None, 'partBshGeoGrp':None}
		self.exportPath = ''
		self.refPath = ''
		self.ref = None

		self.bshAttrDict = {}



	def clearTxtFlds(self):
		self.modelTxtFld.setText('')
		self.partialModelTxtFld.setText('')
		self.partsGrpTxtFld.setText('')
		self.exportPathTxtFld.setText('')
		self.tempCreateMenu.setValue('')



	def connectBshCall(self, op):
		source = None
		destination = None
		if op == 'skin':
			source = self.grpDict['partSkinGeoGrp']
			destination = self.grpDict['partBufferGeoGrp']
			bshName = 'def'
		elif op == 'dtl':
			source = self.grpDict['partDtlGeoGrp']
			destination = self.grpDict['partBufferGeoGrp']
			bshName = 'def'
		elif op == 'bsh':
			source = self.grpDict['partBshGeoGrp']
			destination = self.grpDict['partBufferGeoGrp']
			bshName = 'def'
		elif op == 'buffer':
			source = self.grpDict['partBufferGeoGrp']
			destination = self.grpDict['fullBufferGeoGrp']
			bshName = 'buffer'
		elif op == 'main':
			source = self.grpDict['fullBufferGeoGrp']
			destination = self.grpDict['mainGeoGrp']
			bshName = 'main'

		if source and destination:
			self.connectBsh(source, destination, bshName)

		self.getBshConnectionInfo()



	def connectBshToCtrl(self, ctrl=None):
		if not self.bshAttrDict:
			return
		if not ctrl:
			ctrl = misc.getSel()
			if not ctrl:
				return

		focusPart = self.bshPartMenu.getValue()
		if not focusPart:
			return
		for attr in self.bshAttrDict[focusPart]:
			oldName = attr.shortName()
			attrName = ''.join(oldName.split('__')[1:])
			ctrlAttr = misc.addNumAttr(ctrl, attrName, 'double', dv=0, hide=False, lock=False, key=True)
			pm.connectAttr(ctrlAttr, attr, f=True)

		print 'Connect  %s  to  %s' %(ctrl, self.bshAttrDict[focusPart])



	def connectBsh(self, source, destination, bshName):
		bshNode = None
		bshNode = misc.getBshNodeFromTransform(obj=destination, source=True)

		index = 0

		if not bshNode:
			bshNode = pm.blendShape(source, destination, n='%s_bsh' %bshName, foc=True, tc=False, w=(index, 1))[0]
			return	
		else:
			bshNode = bshNode[0]

		indexList = bshNode.weightIndexList()
		index = [i for i in range(0, max(indexList)+2) if i not in indexList][0]

		tg = (destination, index, source, 1 )
			
		# if the source and destination are connected via blendShape, remove it
		sourceBsh = misc.getBshNodeFromTransform(destination, source=True)
		desBsh = misc.getBshNodeFromTransform(source, source=False)

		if list(set(sourceBsh).intersection(desBsh)):
			pm.blendShape(bshNode, e=True, rm=True, t=tg, tc=False)
			#if no target at all, delete the node
			if bshNode.getTarget() == []:
				pm.delete(bshNode)
		#else add target
		else:
			pm.blendShape(bshNode, e=True, t=tg, w=(index, 1), tc=False)



	def cleanMesh(self, op):
		sels = misc.getSel(num='inf')
		if not sels:
			return
		if not isinstance(sels, (list, tuple)):
			sels = [sels]

		if op == 'checkSymmetry':
			smrObj = smr.SymMeshReflector()
			smrObj.analyzeAGeo()

		elif op == 'freezeMovePivDeleteHis':
			for sel in sels:
				misc.unlockChannelbox(obj=sel)
				misc.cleanUnuseOrigShape(obj=sel)
			pm.xform(sels, ws=True, piv=[0,0,0])
			pm.makeIdentity(sels, a=True)
			pm.delete(sels, ch=True)

		elif op == 'cleanShade':
			lambert = pm.PyNode('lambert1')
			pm.hyperShade(assign=lambert)



	def createGrp(self):
		if not self.rootCtrl:
			OpenMaya.MGlobal.displayError('Cannot find Root_Ctrl. Import Rig_Grp first!')
			return
		if not self.meshDict['body'] and not self.meshDict['head']:
			OpenMaya.MGlobal.displayError('Please load model to textField above first.')
			return
		if not self.meshDict['head']:
			self.meshDict['head'] = self.meshDict['body']


		self.grpDict['mainGeoGrp'] = pm.group(em=True, n='mainGeo_grp')
		self.grpDict['fullBufferGeoGrp'] = pm.group(em=True, n='fullBufferGeo_grp')
		self.grpDict['partBufferGeoGrp'] = pm.group(em=True, n='partBufferGeo_grp')
		self.grpDict['partDtlGeoGrp'] = pm.group(em=True, n='partDtlGeo_grp')
		self.grpDict['partSkinGeoGrp'] = pm.group(em=True, n='partSkinGeo_grp')
		self.grpDict['partBshGeoGrp'] = pm.group(em=True, n='partBshGeo_grp')

		#move it..
		pm.xform(self.grpDict['partBufferGeoGrp'], ws=True, t=(0, self.gap, 0))
		pm.xform(self.grpDict['partDtlGeoGrp'], ws=True, t=(0, self.gap*2, 0))
		pm.xform(self.grpDict['partSkinGeoGrp'], ws=True, t=(0, self.gap*3, 0))
		pm.xform(self.grpDict['partBshGeoGrp'], ws=True, t=((self.gap*-1), self.gap, 0))
		
		#report to rootCtrl
		self.report(self.grpDict['mainGeoGrp'], 'mainGeoGrp')
		self.report(self.grpDict['fullBufferGeoGrp'], 'fullBufferGeoGrp')
		self.report(self.grpDict['partBufferGeoGrp'], 'partBufferGeoGrp')
		self.report(self.grpDict['partDtlGeoGrp'], 'partDtlGeoGrp')
		self.report(self.grpDict['partSkinGeoGrp'], 'partSkinGeoGrp')
		self.report(self.grpDict['partBshGeoGrp'], 'partBshGeoGrp')

		self.grpDict['miscGrp'] = pm.group(em=True, n='misc_grp')
		pm.parent(self.grpDict['miscGrp'], self.grpDict['utilGrp'])
		self.report(self.grpDict['miscGrp'], 'miscGrp')

		pOldName = misc.nameSplit(self.meshDict['head'].nodeName())
		fOldName = misc.nameSplit(self.meshDict['body'].nodeName())
		if self.partsGrp:
			partsGrpName = misc.nameSplit(self.partsGrp.nodeName())

		#fullBufferGeoGrp
		mesh = pm.duplicate(self.meshDict['body'])[0]
		elem = '%s%s'  %(fOldName['elem'], 'FullBuffer')
		mesh.rename(misc.nameObj(elem, fOldName['pos'], 'ply'))
		pm.parent(mesh, self.grpDict['fullBufferGeoGrp'], r=True)
		if self.partsGrp:
			partsGrp = pm.duplicate(self.partsGrp)[0]
			partsGrp.rename(misc.nameObj(elem, partsGrpName['pos'], 'grp'))
			pm.parent(partsGrp, self.grpDict['fullBufferGeoGrp'], r=True)	
		self.annotateGrp(self.grpDict['fullBufferGeoGrp'], 'fullBufferGeoGrp')

		elemNames = ['PartBuffer', 'PartDtl', 'partSkin', 'partBsh']
		keyNames = ['partBufferGeoGrp', 'partDtlGeoGrp', 'partSkinGeoGrp', 'partBshGeoGrp']

		for elemName, keyName in zip(elemNames, keyNames):
			#partBufferGeoGrp
			mesh = pm.duplicate(self.meshDict['head'])[0]
			elem = '%s%s' %(fOldName['elem'], elemName)
			mesh.rename(misc.nameObj(elem, pOldName['pos'], 'ply'))
			pm.parent(mesh, self.grpDict[keyName], r=True)
			if self.partsGrp:
				partsGrp = pm.duplicate(self.partsGrp)[0]
				partsGrp.rename(misc.nameObj(elem, partsGrpName['pos'], 'grp'))
				pm.parent(partsGrp, self.grpDict[keyName], r=True)		
			self.annotateGrp(self.grpDict[keyName], keyName)

		#parent the rest
		pm.parent([self.meshDict['body'], self.partsGrp], self.grpDict['mainGeoGrp'], r=True)
		pm.parent([self.grpDict['partSkinGeoGrp'], self.grpDict['partDtlGeoGrp'], self.grpDict['partSkinGeoGrp'], 
			self.grpDict['partBshGeoGrp'], self.grpDict['partBufferGeoGrp'], self.grpDict['fullBufferGeoGrp'], 
			self.grpDict['mainGeoGrp']], self.grpDict['animGeoGrp'])

		pm.delete(self.meshDict['head'])

		#get export path
		modelSubDepts = ['bsh', 'anim']
		for sd in modelSubDepts:
			try:
				exportDir = misc.getPipelinePath(dept='model_bsh', work=True, copy=False, fileInc=False)
				break
			except:
				pass
		version = misc.genVersion(exportDir)
		fileName = '%s_bsh_v%s.ma' %(self.rgAttr['assetName'], version)
		self.exportPath = '%s/%s' %(exportDir, fileName)
		self.setExportPath()

		pm.select(cl=True)
		print('\nDeform and buffer groups created.'),



	def createRef(self):
		self.refPath = self.refPathTxtFld.getText()
		if not os.path.exists(self.refPath) and not os.path.isfile(self.refPath):
			return
		else:
			fileName = self.refPath.replace('\\', '/')
			fileName = self.refPath.split('/')[-1].split('.')[0]

		if self.grpDict['partBshGeoGrp']:
			if self.grpDict['partBshGeoGrp'].isReferenced():
				#replace ref
				self.ref = pm.FileReference(pm.referenceQuery(self.grpDict['partBshGeoGrp'], rfn=True))
				self.ref.replaceWith(self.refPath)
			else:
				cDialog = ui.ConfirmDialog(title='Confirm Delete', message='Replace existing bufferGrp with referenced one?')
				if cDialog.ret == True:
					pm.delete(self.grpDict['partBshGeoGrp'])
					self.ref = pm.createReference(self.refPath, ns=fileName)
				else: return
		else:
			self.ref = pm.createReference(self.refPath, ns=fileName)
		#try to find the bufferGrp
		try:
			refNodes = self.ref.nodes()
			refTransforms = [node for node in refNodes if node.type() == 'transform']
			for i in refTransforms:
				if i.hasAttr('_TYPE'):
					if i._TYPE.get() == 'bufferGrp':
						if i.hasAttr('NODE') and i.hasAttr('BSH'):
							if not i.NODE.outputs() or not i.BSH.outputs():
								OpenMaya.MGlobal.displayWarning('Buffer Group was not rigged!')
						self.grpDict['partBshGeoGrp'] = i
						break

			pm.xform(self.grpDict['partBshGeoGrp'], ws=True, t=[self.gap*-1, self.gap, 0])
			if self.grpDict['partBshGeoGrp'] not in self.grpDict['animGeoGrp'].getChildren():
				pm.parent(self.grpDict['partBshGeoGrp'], self.grpDict['animGeoGrp'])
			self.report(self.grpDict['partBshGeoGrp'], 'partBshGeoGrp')
			self.getBshConnectionInfo()
			self.getBshParts()
		except: 
			OpenMaya.MGlobal.displayWarning('Cannot find bufferGrp')
			#pop up for user to load buffer grp
			return

		

		#misc.addStrAttr(self.rootCtrl, 'refPath', self.refPath)



	def cutMesh(self):
		cutMeshes = misc.separateReorderMesh()
		self.meshDict['head'] = cutMeshes[0]
		self.meshDict['body'] = cutMeshes[1]

		self.meshDict['head'].rename('head_ply')
		self.meshDict['body'].rename('body_ply')

		self.partialModelTxtFld.setText(self.meshDict['head'].nodeName())
		self.modelTxtFld.setText(self.meshDict['body'].nodeName())



	def createTemplate(self):
		#get temp class in the menu item
		clsName = self.tempCreateMenu.getValue()
		if not clsName:
			OpenMaya.MGlobal.displayError('Please select a type of template from the drop-down menu.')
			return

		if not self.rootCtrl:
			OpenMaya.MGlobal.displayWarning('Root_Ctrl was not loaded.')

		#get elem, side
		elem = '%s' %self.tempElemTxtFld.getText()
		side = '%s' %self.tempSideTxtFld.getText()
		tempScale = self.tempScaleFloatSliderGrp.getValue()
		jntNum = self.tempJntNumIntSliderGrp.getValue()
		if not elem:
			return

		#check for existing template with crashing name
		self.detectTemplateInScene()
		self.getTemplate()

		if '%s%s' %(elem, side) in self.tempDict.keys():
			OpenMaya.MGlobal.displayError('Template with the same element and side already exists! Try something else!')
			return

		temp = self.tempClsDict[clsName](elem=elem, pos=side)
		temp.scale = tempScale

		#if the temp cls jnt num can be specify
		if hasattr(temp, 'num') == True:
			temp.num = jntNum
		temp.create()

		self.tempDict['%s%s'%(elem, side)] = temp
		self.report(temp.rootCtrl, '_TEMPLATE')

		if self.tempMirrorChkBox.getValue() == True:
			self.mirrorTemplate(tempRoot=temp.rootCtrl)



	def disconnectTemplate(self):
		objs = misc.getSel(num='inf')
		if not objs:
			return
		if isinstance(objs, (list,tuple)) == False:
			objs = [objs]

		for obj in objs:	
			template.disconnectTempJnts(obj)
			for m in template.getMirrored(obj):
				template.disconnectTempJnts(m)



	def detectTemplateInScene(self):
		self.tempDict = {}
		assembliesItems = pm.ls(assemblies=True, tr=True)
		templateRoots = [item for item in assembliesItems if template.isTemplateRoot(item) == True]
		if not templateRoots:
			return
		
		if self.rootCtrl == True:
			for tempRoot in templateRoots:
				self.report(tempRoot, '_TEMPLATE')
		else:
			for tempRoot in templateRoots:
				temp = self.analyzeTemplate(tempRoot)
				if not temp:
					continue
				self.tempDict['%s%s'%(temp.elem, temp.pos)] = temp



	def getTemplate(self):
		if not self.rootCtrl or not self.rootCtrl.hasAttr('_TEMPLATE'):
			return

		self.tempDict = {}
		templates = self.get('_TEMPLATE')
		for i in templates:
			temp = self.analyzeTemplate(i)
			self.tempDict['%s%s'%(temp.elem, temp.pos)] = temp



	def getBshParts(self):
		#find bsh parts
		if self.grpDict['partBshGeoGrp']:
			if self.grpDict['partBshGeoGrp'].hasAttr('_NODE'):
				if self.grpDict['partBshGeoGrp'].attr('_NODE').outputs():
					try:
						attrs = pm.listAttr(self.grpDict['partBshGeoGrp'], ud=True)
						self.bshAttrDict = {}
						for a in attrs:
							if a.startswith('___'):
								continue
							attrObj = self.grpDict['partBshGeoGrp'].attr(a)
							if attrObj.type() != 'double':
								continue

							partName = a.split('__')[0]
							if partName not in self.bshAttrDict.keys():
								self.bshAttrDict[partName] = []

							self.bshAttrDict[partName].append(attrObj)

						#clear parts menu first
						self.bshPartMenu.clear()
						pm.menuItem(l='', p=self.bshPartMenu)
						
						for partName in sorted(self.bshAttrDict.keys())[::-1]:
							pm.menuItem(l=partName, p=self.bshPartMenu, ia='')
					except:
						OpenMaya.MGlobal_displayWarning('Cannot find rigged blendShape parts. Bsh Part menuItem not loaded.')



	# def genVersion(self, incSavePath):
	# 	version = '001'
	# 	try:
	# 		ver = 0
	# 		files = [ f for f in os.listdir(incSavePath) if os.path.isfile(os.path.join(incSavePath,f)) ]
	# 		num = []
	# 		for fName in files:
	# 			if self.rgAttr['assetName'] in fName:
	# 				fName = fName.split(self.rgAttr['assetName'])[1]
	# 			v = ''
	# 			for i in fName:
	# 				if i.isdigit():
	# 					v += str(i)
	# 			if v:
	# 				num.append(int(v))

	# 		ver = max(num) + 1
	# 		version = str(ver)
	# 	except:
	# 		pass
	# 	return version.zfill(3)



	def get(self, askFor):
		return pm.listConnections(self.rootCtrl.attr(askFor), d=True, s=False)



	def getBshConnectionInfo(self):
		self.resetConnectionStatus()
		#get connection info for deformer groups
		if self.grpDict['partBufferGeoGrp']:
			bshNode = misc.getBshNodeFromTransform(obj=self.grpDict['partBufferGeoGrp'], source=True, search='def_bsh')
			if bshNode:
				bshNode = bshNode[0]
				self.report(bshNode, 'defBshNode')
				self.bshNodeDict['defBshNode'] = bshNode
				#tgts = [t for t in bshNode.getTarget() if isinstance(t, (pm.nt.Mesh, pm.nt.Transform))]
				tgts = bshNode.getTarget()

				for t in tgts:
					try:
						if self.grpDict['partSkinGeoGrp']:
							if self.grpDict['partSkinGeoGrp'].isParentOf(t):
								self.skinStatus = True
								continue
						if self.grpDict['partDtlGeoGrp']:
							if self.grpDict['partDtlGeoGrp'].isParentOf(t):
								self.dtlStatus = True
								continue
						if self.grpDict['partBshGeoGrp']:
							if self.grpDict['partBshGeoGrp'].isParentOf(t):
								self.bshStatus = True
								continue
					except: pass

		#get connection info for buffer groups
		if self.grpDict['fullBufferGeoGrp']:
			bshNode = misc.getBshNodeFromTransform(obj=self.grpDict['fullBufferGeoGrp'], source=True)
			if bshNode:
				bshNode = bshNode[0]
				self.report(bshNode, 'bufferBshNode')
				self.bshNodeDict['bufferBshNode'] = bshNode
				tgts = bshNode.getTarget()
				for t in tgts:
					try:
						if self.grpDict['partBufferGeoGrp'].isParentOf(t):
							self.bufferStatus = True
							self.bufferBshNode = bshNode
							break
					except: pass


		#get connection info for buffer groups
		if self.grpDict['mainGeoGrp']:
			bshNode = misc.getBshNodeFromTransform(obj=self.grpDict['mainGeoGrp'], source=True)

			if bshNode:
				bshNode = bshNode[0]
				self.report(bshNode, 'mainBshNode')
				self.bshNodeDict['mainBshNode'] = bshNode
				tgts = bshNode.getTarget()

				for t in tgts:
					try:
						if self.grpDict['fullBufferGeoGrp'].isParentOf(t):
							self.mainStatus = True
							self.mainBshNode = bshNode
							break
					except: pass



		self.updateBshConnectionStatus()



	def importRigGrp(self):
		nodes = misc.importRigGrp()
		try:
			rootCtrl = [i for i in nodes if i.nodeName() == 'Root_Ctrl'][0]
			self.loadRootCtrl(rootCtrl)
		except:
			pm.warning('Cannot find "Root_Ctrl" in imported objects. Message attribute connections skipped.')
			return 

		for node in nodes:
			nodeName = node.nodeName()
			if nodeName == 'Anim_Geo_Grp':
				self.grpDict['animGeoGrp'] = node
				self.report(node, 'animGeoGrp')
			elif nodeName == 'Anim_Ctrl_Grp':
				self.grpDict['animCtrlGrp'] = node
				self.report(node, 'animCtrlGrp')
			elif nodeName == 'Util_Grp':
				self.grpDict['utilGrp'] = node
				self.report(node, 'utilGrp')
		misc.autoFillRigGrp()



	def importTemplate(self):
		path = self.importTempTxtFld.getText()

		if os.path.isfile(path) == False or os.path.exists(path) == False:
			OpenMaya.MGlobal.displayError('Invalid import path!')
			return

		importedNodes = pm.importFile(path, returnNewNodes=True)

		#get all template node
		tempRoots = [ node for node in importedNodes if template.isTemplateRoot(node) == True]
		for tempRoot in tempRoots:
			self.report(tempRoot, '_TEMPLATE')

		self.getTemplate()



	def loadRootCtrl(self, sel=None):
		if not sel:
			sel = misc.getSel(selType='transform')
			if not sel:
				return

		rootCtrlColorVal = True

		self.clearVal()
		self.clearTxtFlds()

		self.rootCtrlTxtFld.setText(sel.nodeName())
		self.rootCtrl = sel

		if not sel.hasAttr('_TEMPLATE') :
			try: 
				if not isinstance(sel.getShape(), pm.nt.NurbsCurve):
					OpenMaya.MGlobal.displayWarning('Are you sure this is the rootCtrl?')
					rootCtrlColorVal = False
				else:
					misc.addMsgAttr(self.rootCtrl, '_TEMPLATE')
			except:
				return

		#get rig_grp attributes for pipeline vars
		self.rgAttr = misc.getRigGrpAttr()

		#load all msg connections to self.attr..
		udAttrs = pm.listAttr(self.rootCtrl, ud=True)
		for a in udAttrs:
			attr = self.rootCtrl.attr(a)
			if attr.get(type=True) == 'message':
				connectTo = self.get(a)
				if connectTo:
					if isinstance(connectTo[0], pm.nt.Transform) and a.endswith('Grp')==True:
						self.grpDict[a] = connectTo[0]

		# if no connections
		grpKeys = ['animCtrlGrp', 'animGeoGrp', 'utilGrp']
		grpNames = ['Anim_Ctrl_Grp', 'Anim_Geo_Grp', 'Util_Grp']

		for key, name in zip(grpKeys, grpNames):
			if key not in self.grpDict.keys():
				try:
					self.grpDict[key] = pm.PyNode(name)
				except:
					pm.warning('Cannot find  %s' %name)

	
		try:
			self.getBshConnectionInfo()

			if self.grpDict['partBshGeoGrp'].isReferenced():
				self.ref = pm.FileReference(pm.referenceQuery(self.grpDict['partBshGeoGrp'], rfn=True))
				self.refPath = self.ref.path
				self.refPathTxtFld.setText(self.refPath)
		except: pass

		self.getBshParts()
						
		#get created template
		self.detectTemplateInScene()
		self.getTemplate()

		#set color
		self.setLoadRootCtrlColor(rootCtrlColorVal)



	def loadModel(self, sel=None):
		if not sel:
			sel = misc.getSel()
		try:
			self.modelTxtFld.setText(sel.nodeName())
		except:
			self.modelTxtFld.setText('')
		self.meshDict['body'] = sel



	def loadPartialModel(self, sel=None):
		if not sel:
			sel = misc.getSel()
		try:
			self.partialModelTxtFld.setText(sel.nodeName())
		except:
			self.partialModelTxtFld.setText('')
		self.meshDict['head'] = sel



	def loadParts(self, sel=None):
		sel = misc.getSel()
		try:
			self.partsGrpTxtFld.setText(sel.nodeName())
		except:
			self.partsGrpTxtFld.setText('')
		self.partsGrp = sel 



	def modelExport(self):	
		if not self.grpDict['partBshGeoGrp']:
			toExport = misc.getSel()
		else:
			toExport = self.grpDict['partBshGeoGrp']

		self.exportPath = self.exportPathTxtFld.getText()
		if os.path.exists('/'.join(self.exportPath.split('/')[0:-1])) == True:
			if os.path.exists(self.exportPath):
				result = pm.confirmDialog( title='Confirm Overwrite File', 
				message='File already exists!',
				button=['Overwrite', 'Cancel'], defaultButton='Cancel', cancelButton='Cancel', dismissString='Cancel' )
				if result == 'Cancel':
					return
				else:
					pass
			pm.parent(toExport, w=True)
			pm.xform(toExport, ws=True, t=[0, 0, 0])

			pm.select(toExport, r=True)
			pm.exportSelected(self.exportPath, f=True, ch=False, constraints=False, expressions=False, type='mayaAscii')
			
			pm.delete(toExport)
			#misc.addStrAttr(self.rootCtrl, 'exportPath', self.exportPath)
			self.grpDict['partBshGeoGrp'] = None
		else:
			pm.warning('Export directory does not exists!')



	def mirrorTemplate(self, tempRoot=None):
		if not tempRoot:
			sel = misc.getSel()
			if not sel:
				OpenMaya.MGlobal.displayWarning('Select template ctrl.')
				return
			if not sel.hasAttr('_SUBTYPE') or not sel.hasAttr('_TEMPLATE'):
				return
			if sel._SUBTYPE.get() != 'TEMP_Controller' or not sel._TEMPLATE.get():
				return
			tempRoot = sel

		temp = self.analyzeTemplate(tempRoot)
		mSide = 'MIRRORED'
		if temp.pos:
			mSide = temp.pos.replace('LFT', 'RHT')
			if mSide == temp.pos:
				mSide = 'MIRRORED'

		connectMirror = self.mirrorConnectChkBox.getValue()
		mTemp = template.mirrorTemp(temp, pos=mSide, connect=connectMirror)
		self.tempDict['%s%s'%(temp.elem, mSide)] = mTemp
		self.report(mTemp.rootCtrl, '_TEMPLATE')
		


	def resetConnectionStatus(self):
		self.skinTxt.setBackgroundColor([0.3,0,0])
		self.dtlTxt.setBackgroundColor([0.3,0,0])
		self.bshTxt.setBackgroundColor([0.3,0,0])
		self.bufferTxt.setBackgroundColor([0.3,0,0])
		self.mainTxt.setBackgroundColor([0.3,0,0])

		self.skinStatus, self.dtlStatus, self.bshStatus = False, False, False
		self.bufferStatus, self.mainStatus = False, False

		self.bshNodeDict = {'defBshNode':None, 'bufferBshNode':None, 'mainBshNode':None}



	def report(self, obj, attr):
		if not self.rootCtrl:
			return
		source = misc.addMsgAttr(self.rootCtrl, attr)
		destination = misc.addMsgAttr(obj, 'networkParent')
		if pm.isConnected(source, destination) == False:
			pm.connectAttr(source, destination, f=True)



	def setExportPath(self):
		self.exportPathTxtFld.setText('')
		dirPath = '/'.join(self.exportPath.split('/')[0:-1])
		if os.path.exists(dirPath):
			self.exportPathTxtFld.setText(self.exportPath)



	def setLoadRootCtrlColor(self, loaded):
		if loaded == True:
			bgc = [0,0.4,0]
		else:
			bgc = [0.4,0,0]
		self.rootCtrlTxtFld.setBackgroundColor(bgc)



	def updateBshConnectionStatus(self):
		if self.skinStatus == True:
			self.skinTxt.setBackgroundColor([0,0.4,0])
		if self.dtlStatus == True:
			self.dtlTxt.setBackgroundColor([0,0.4,0])
		if self.bshStatus == True:
			self.bshTxt.setBackgroundColor([0,0.4,0])
		if self.bufferStatus == True:
			self.bufferTxt.setBackgroundColor([0,0.4,0])
		if self.mainStatus == True:
			self.mainTxt.setBackgroundColor([0,0.4,0])

	

	def updateRigSettings(self):
		currentMod = self.rigModuleMenu.getValue()
		self.currentPackageName = '.'.join(currentMod.split('.')[:-1])
		if not currentMod or currentMod not in self.rigModUiDict.keys():
			self.clearRigUi()
			return

		#display module descriptions..
		des = self.rigModUiDict[currentMod]._DESCRIPTION
		if not des: des = ''
		self.rigDescription = des
		self.rigDescriptionTxt.setLabel(self.rigDescription)

		#instance base ui
		
		if not type(self.baseUiDict[self.currentPackageName]) is type(self.currentBaseUi):
			try: pm.deleteUI(self.currentBaseUi.masterCol)
			except: pass
			self.baseUiDict[self.currentPackageName].create()
			self.currentBaseUi = self.baseUiDict[self.currentPackageName]

		try: pm.deleteUI(self.currentRigUi.rigCol)
		except: pass

		#instance rig ui
		self.currentRigUi = self.rigModUiDict[currentMod](baseUi=self.baseUiDict[self.currentPackageName],
							parent=self.baseUiDict[self.currentPackageName].mainCol)