import sys
import maya.standalone as std

std.initialize(name='python')
filename = sys.argv[1]
saveAsPath = sys.argv[2]

import pymel.core as pm
# from nuTools import config
# reload(config)

def bshPublish(filename, saveAsPath):
	# saveAsPath = file(config.BSH_PUB_LOG_DIR).read()
	# os.remove(config.BSH_PUB_LOG_DIR)	
	try:

		pm.openFile(filename, o=True, f=True)

		#remove all references
		for ref in pm.listReferences():
			ref.remove()

		
		exportGrp = pm.ls( '*.bufferGrp' , objectsOnly = True, recursive=True)[0]
		bufferGrp = exportGrp.attr('bufferGrp').outputs()[0]
	

		#now we get what we came for 'bufferGrp'
		pm.parent(bufferGrp, w=True)
		
		for obj in pm.ls(assemblies=True):
			if obj == bufferGrp or obj.nodeName() in ['persp', 'top', 'front', 'side']:
				continue
			try:
				pm.delete(obj)
			except: pass
		

		#save as to path read form log
		pm.saveAs(saveAsPath)

		sys.stdout.write('_OUTPUT_TO_SCRIPT_EDITORHero Path: %s' %saveAsPath)
		return saveAsPath

	except: 
		Exception, e
		sys.stderr.write(str(e))
		sys.exit(-1)


bshPublish(filename, saveAsPath)