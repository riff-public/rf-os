import maya.cmds as mc
import maya.mel as mm

import os
import shutil
import sys
import re

import pkmel.core as pc
reload( pc )

def jointAt( obj ) :
	'''
	Create a pkrig joint at postion of given object
	'''
	target = pc.Dag( obj )
	
	jnt = pc.Joint()
	
	jnt.snap( target )
	jnt.freeze( r = True , s = True )
	jnt.rotateOrder = target.rotateOrder
	if target.attr( 'radius' ).exists :
		jnt.attr( 'radius' ).v = target.attr( 'radius' ).v
	mc.select( cl = True )
	
	return jnt