//Maya ASCII 2015 scene
//Name: TEMPLATE_proxySkinJoint_frd.ma
//Last modified: Fri, Apr 29, 2016 07:31:18 PM
//Codeset: 1252
requires maya "2015";
requires "Mayatomr" "2012.0m - 3.9.1.48 ";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2015";
fileInfo "version" "2015";
fileInfo "cutIdentifier" "201503261530-955654";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -n "proxySkinJnt_grp";
	setAttr ".ove" yes;
	setAttr ".t" -type "double3" 0 7.1260128007052179 0.13611055408258466 ;
createNode joint -n "rootProxySkin_jnt" -p "proxySkinJnt_grp";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.2932755928752746 -0.014049040266633131 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 6.8327372078299433 0.12206151381595153 1;
	setAttr ".radi" 0.15;
createNode joint -n "spine1ScaProxySkin_jnt" -p "rootProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.3851912913957268e-017 -0.11986437613689116 0.044791273281719432 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 6.3851912913957268e-017 6.7128728316930522 0.16685278709767096 1;
	setAttr ".radi" 0.15;
createNode joint -n "upLegProxySkinLFT_jnt" -p "spine1ScaProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.50903734710215187 -0.36555985398162338 -0.078563077945647292 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-016 0 0 -1.2246467991473532e-016 -1 0 0
		 0 0 1 0 0.50903734710215198 6.3473129777114288 0.088289709152023665 1;
	setAttr ".radi" 0.05;
createNode joint -n "lowLegProxySkinLFT_jnt" -p "upLegProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.6613381477509392e-016 2.7967910517431025 0.072205726117346658 ;
	setAttr ".ro" 1;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-016 0 0 -1.2246467991473532e-016 -1 0 0
		 0 0 1 0 0.50903734710215232 3.5505219259683263 0.16049543526937032 1;
	setAttr ".radi" 0.05;
createNode joint -n "ankleProxySkinLFT_jnt" -p "lowLegProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 3.0940209246478534 -0.23661895580548556 ;
	setAttr ".ro" 1;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-016 0 0 -1.2246467991473532e-016 -1 0 0
		 0 0 1 0 0.50903734710215198 0.45650100132047289 -0.076123520536115236 1;
	setAttr ".radi" 0.05;
createNode joint -n "ballProxySkinLFT_jnt" -p "ankleProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -2.2204460492503131e-016 0.45650100132047278 0.78946211553877621 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-016 0 0 -2.7192621468937821e-032 -2.2204460492503131e-016 1.0000000000000002 0
		 1.2246467991473535e-016 1.0000000000000002 2.2204460492503131e-016 0 0.50903734710215209 1.1102230246251565e-016 0.71333859500266095 1;
	setAttr ".radi" 0.05;
createNode joint -n "toeProxySkinLFT_jnt" -p "ballProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 1.1102230246251565e-016 0.4189600849912043 5.1905461304417951e-016 ;
	setAttr ".ro" 1;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-016 0 0 -2.7192621468937821e-032 -2.2204460492503131e-016 1.0000000000000002 0
		 1.2246467991473535e-016 1.0000000000000002 2.2204460492503131e-016 0 0.50903734710215198 5.370490889554662e-016 1.1322986799938652 1;
	setAttr ".radi" 0.05;
createNode joint -n "lowLegRbnDtl1ProxySkinLFT_jnt" -p "lowLegProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -4.4408920985006262e-016 0.30940209246478512 -0.02366189558054857 ;
	setAttr ".jo" -type "double3" 4.3732508047329679 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 -2.4651903288156619e-032 0.99708846072979362 0.076253534209835661 0
		 0 -0.076253534209835661 0.99708846072979374 0 0.50903734710215276 3.2411198335035412 0.13683353968882175 1;
	setAttr ".radi" 0.1;
createNode joint -n "lowLegRbnDtl2ProxySkinLFT_jnt" -p "lowLegProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.3306690738754696e-016 0.92820627739435535 -0.070985686741645709 ;
	setAttr ".jo" -type "double3" 4.3732508047329679 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 -2.4651903288156619e-032 0.99708846072979362 0.076253534209835661 0
		 0 -0.076253534209835661 0.99708846072979374 0 0.50903734710215254 2.6223156485739709 0.089509748527724614 1;
	setAttr ".radi" 0.1;
createNode joint -n "lowLegRbnDtl3ProxySkinLFT_jnt" -p "lowLegProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -4.4408920985006262e-016 1.5470104623239256 -0.11830947790274278 ;
	setAttr ".jo" -type "double3" 4.3732508047329679 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 -2.4651903288156619e-032 0.99708846072979362 0.076253534209835661 0
		 0 -0.076253534209835661 0.99708846072979374 0 0.50903734710215254 2.0035114636444007 0.042185957366627544 1;
	setAttr ".radi" 0.1;
createNode joint -n "lowLegRbnDtl4ProxySkinLFT_jnt" -p "lowLegProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 2.1658146472534971 -0.16563326906383993 ;
	setAttr ".jo" -type "double3" 4.3732508047329679 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 -2.4651903288156619e-032 0.99708846072979362 0.076253534209835661 0
		 0 -0.076253534209835661 0.99708846072979374 0 0.50903734710215232 1.3847072787148291 -0.0051378337944696095 1;
	setAttr ".radi" 0.1;
createNode joint -n "lowLegRbnDtl5ProxySkinLFT_jnt" -p "lowLegProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 2.7846188321830678 -0.21295706022493702 ;
	setAttr ".jo" -type "double3" 4.3732508047329679 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 -2.4651903288156619e-032 0.99708846072979362 0.076253534209835661 0
		 0 -0.076253534209835661 0.99708846072979374 0 0.50903734710215209 0.76590309378525845 -0.052461624955566694 1;
	setAttr ".radi" 0.1;
createNode joint -n "upLegRbnDtl1ProxySkinLFT_jnt" -p "upLegProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.3306690738754696e-016 0.27967910517431083 0.0072205726117349628 ;
	setAttr ".jo" -type "double3" -1.4788965222021235 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 2.4651903288156619e-032 0 0 -2.4651903288156619e-032 0.99966689879583526 -0.025808747585213617 0
		 -3.8518598887744717e-034 0.025808747585213617 0.99966689879583526 0 0.50903734710215232 6.0676338725371179 0.095510281763758628 1;
	setAttr ".radi" 0.1;
createNode joint -n "upLegRbnDtl2ProxySkinLFT_jnt" -p "upLegProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.6613381477509392e-016 0.83903731552293337 0.021661717835204403 ;
	setAttr ".jo" -type "double3" -1.4788965222021235 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 2.4651903288156619e-032 0 0 -2.4651903288156619e-032 0.99966689879583526 -0.025808747585213617 0
		 -3.8518598887744717e-034 0.025808747585213617 0.99966689879583526 0 0.50903734710215254 5.5082756621884954 0.10995142698722807 1;
	setAttr ".radi" 0.1;
createNode joint -n "upLegRbnDtl3ProxySkinLFT_jnt" -p "upLegProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -7.7715611723760958e-016 1.3983955258715515 0.036102863058673385 ;
	setAttr ".jo" -type "double3" -1.4788965222021235 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 2.4651903288156619e-032 0 0 -2.4651903288156619e-032 0.99966689879583526 -0.025808747585213617 0
		 -3.8518598887744717e-034 0.025808747585213617 0.99966689879583526 0 0.50903734710215254 4.9489174518398773 0.12439257221069705 1;
	setAttr ".radi" 0.1;
createNode joint -n "upLegRbnDtl4ProxySkinLFT_jnt" -p "upLegProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.5511151231257827e-016 1.9577537362201713 0.050544008282142283 ;
	setAttr ".jo" -type "double3" -1.4788965222021235 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 2.4651903288156619e-032 0 0 -2.4651903288156619e-032 0.99966689879583526 -0.025808747585213617 0
		 -3.8518598887744717e-034 0.025808747585213617 0.99966689879583526 0 0.50903734710215232 4.3895592414912574 0.13883371743416595 1;
	setAttr ".radi" 0.1;
createNode joint -n "upLegRbnDtl5ProxySkinLFT_jnt" -p "upLegProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.6613381477509392e-016 2.517111946568793 0.064985153505611709 ;
	setAttr ".jo" -type "double3" -1.4788965222021235 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 2.4651903288156619e-032 0 0 -2.4651903288156619e-032 0.99966689879583526 -0.025808747585213617 0
		 -3.8518598887744717e-034 0.025808747585213617 0.99966689879583526 0 0.50903734710215232 3.8302010311426358 0.15327486265763537 1;
	setAttr ".radi" 0.1;
createNode joint -n "upLegProxySkinRGT_jnt" -p "spine1ScaProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.50903700000000007 -0.36556283169305193 -0.078563087097670958 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 180 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 0 -1.2246467991473532e-016 0 0 1 0 0 1.2246467991473532e-016 0 -1 0
		 -0.50903699999999996 6.3473100000000002 0.088289699999999999 1;
	setAttr ".radi" 0.05;
createNode joint -n "lowLegProxySkinRGT_jnt" -p "upLegProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -2.79679 -0.0722053 ;
	setAttr ".ro" 1;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 0 -1.2246467991473532e-016 0 0 1 0 0 1.2246467991473532e-016 0 -1 0
		 -0.50903699999999996 3.5505200000000001 0.160495 1;
	setAttr ".radi" 0.05;
createNode joint -n "ankleProxySkinRGT_jnt" -p "lowLegProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -3.0940190000000003 0.23661850000000001 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 1.636275939199274e-006 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.99999999999999956 2.8558402610189875e-008 -1.2246467991473527e-016 0
		 2.8558402610189875e-008 0.99999999999999956 3.4973956345330449e-024 0 1.2246467991473532e-016 0 -1 0
		 -0.50903699999999996 0.45650099999999982 -0.076123500000000011 1;
	setAttr ".radi" 0.05;
createNode joint -n "ballProxySkinRGT_jnt" -p "ankleProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.1102230246251565e-016 -0.45650099999999988 -0.78946250000000029 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 89.999999999999972 0 -1.4787793334710986e-006 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 2.7488343306720213e-009 -1.2246467991473532e-016 0
		 1.2246468253294356e-016 5.5511151231257847e-016 -0.99999999999999989 0 -2.7488343306720147e-009 -0.99999999999999978 -5.5511151264921342e-016 0
		 -0.50903701303693927 1.6653345369377348e-016 0.71333900000000028 1;
	setAttr ".radi" 0.05;
createNode joint -n "toeProxySkinRGT_jnt" -p "ballProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.41896099999999903 -5.4604619575453378e-016 ;
	setAttr ".ro" 1;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 0 -1.2246467991473532e-016 0 1.2246467991473532e-016 4.4408920985006262e-016 -1 0
		 5.4385242937875642e-032 -1 -4.4408920985006262e-016 0 -0.50903699999999996 4.4402034118923291e-016 1.1322969060342896 1;
	setAttr ".radi" 0.05;
createNode joint -n "lowLegRbnDtl1ProxySkinRGT_jnt" -p "lowLegProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -4.4408920985006262e-016 -0.30940189999999967 0.023661850000000123 ;
	setAttr ".jo" -type "double3" 4.3732451229438221 180 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 2.4492935982947069e-016 0 -1.8676705100819688e-017 0.9970884682915413 0.076253435332632791 0
		 -2.4421624023199466e-016 -0.076253435332632791 0.99708846829154152 0 -0.50903699999999952 3.2411181000000004 0.13683314999999988 1;
	setAttr ".radi" 0.1;
createNode joint -n "lowLegRbnDtl2ProxySkinRGT_jnt" -p "lowLegProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -4.4408920985006262e-016 -0.9282057000000008 0.070985550000000217 ;
	setAttr ".jo" -type "double3" 4.3732451229438221 180 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 2.4492935982947069e-016 0 -1.8676705100819688e-017 0.9970884682915413 0.076253435332632791 0
		 -2.4421624023199466e-016 -0.076253435332632791 0.99708846829154152 0 -0.50903699999999952 2.6223142999999993 0.089509449999999782 1;
	setAttr ".radi" 0.1;
createNode joint -n "lowLegRbnDtl3ProxySkinRGT_jnt" -p "lowLegProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 -1.547009500000001 0.11830925 ;
	setAttr ".jo" -type "double3" 4.3732451229438221 180 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 2.4492935982947069e-016 0 -1.8676705100819688e-017 0.9970884682915413 0.076253435332632791 0
		 -2.4421624023199466e-016 -0.076253435332632791 0.99708846829154152 0 -0.50903699999999974 2.0035104999999991 0.042185749999999994 1;
	setAttr ".radi" 0.1;
createNode joint -n "lowLegRbnDtl4ProxySkinRGT_jnt" -p "lowLegProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.2204460492503131e-016 -2.1658133000000008 0.16563294999999981 ;
	setAttr ".jo" -type "double3" 4.3732451229438221 180 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 2.4492935982947069e-016 0 -1.8676705100819688e-017 0.9970884682915413 0.076253435332632791 0
		 -2.4421624023199466e-016 -0.076253435332632791 0.99708846829154152 0 -0.50903699999999974 1.3847066999999993 -0.0051379499999998079 1;
	setAttr ".radi" 0.1;
createNode joint -n "lowLegRbnDtl5ProxySkinRGT_jnt" -p "lowLegProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -2.7846171000000015 0.21295664999999994 ;
	setAttr ".jo" -type "double3" 4.3732451229438221 180 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 2.4492935982947069e-016 0 -1.8676705100819688e-017 0.9970884682915413 0.076253435332632791 0
		 -2.4421624023199466e-016 -0.076253435332632791 0.99708846829154152 0 -0.50903699999999996 0.76590289999999861 -0.052461649999999943 1;
	setAttr ".radi" 0.1;
createNode joint -n "upLegRbnDtl1ProxySkinRGT_jnt" -p "upLegProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.27967899999999979 -0.0072205300000001693 ;
	setAttr ".jo" -type "double3" 178.52111164563843 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 1.2246467991473535e-016 1.2246467991473532e-016 0
		 -1.1926324467315779e-016 0.99966690247500944 -0.025808605076988438 0 -1.2558452979275617e-016 0.025808605076988438 0.99966690247500956 0
		 -0.50903699999999996 6.0676310000000004 0.095510230000000168 1;
	setAttr ".radi" 0.1;
createNode joint -n "upLegRbnDtl2ProxySkinRGT_jnt" -p "upLegProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.83903700000000114 -0.021661590000000369 ;
	setAttr ".jo" -type "double3" 178.52111164563843 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 1.2246467991473535e-016 1.2246467991473532e-016 0
		 -1.1926324467315779e-016 0.99966690247500944 -0.025808605076988438 0 -1.2558452979275617e-016 0.025808605076988438 0.99966690247500956 0
		 -0.50903699999999996 5.5082729999999991 0.10995129000000037 1;
	setAttr ".radi" 0.1;
createNode joint -n "upLegRbnDtl3ProxySkinRGT_jnt" -p "upLegProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -1.3983950000000007 -0.036102649999999931 ;
	setAttr ".jo" -type "double3" 178.52111164563843 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 1.2246467991473535e-016 1.2246467991473532e-016 0
		 -1.1926324467315779e-016 0.99966690247500944 -0.025808605076988438 0 -1.2558452979275617e-016 0.025808605076988438 0.99966690247500956 0
		 -0.50903699999999996 4.9489149999999995 0.12439234999999993 1;
	setAttr ".radi" 0.1;
createNode joint -n "upLegRbnDtl4ProxySkinRGT_jnt" -p "upLegProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 -1.9577530000000003 -0.050543709999999548 ;
	setAttr ".jo" -type "double3" 178.52111164563843 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 1.2246467991473535e-016 1.2246467991473532e-016 0
		 -1.1926324467315779e-016 0.99966690247500944 -0.025808605076988438 0 -1.2558452979275617e-016 0.025808605076988438 0.99966690247500956 0
		 -0.50903700000000018 4.3895569999999999 0.13883340999999955 1;
	setAttr ".radi" 0.1;
createNode joint -n "upLegRbnDtl5ProxySkinRGT_jnt" -p "upLegProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.2204460492503131e-016 -2.5171110000000017 -0.064984769999999664 ;
	setAttr ".jo" -type "double3" 178.52111164563843 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 1.2246467991473535e-016 1.2246467991473532e-016 0
		 -1.1926324467315779e-016 0.99966690247500944 -0.025808605076988438 0 -1.2558452979275617e-016 0.025808605076988438 0.99966690247500956 0
		 -0.50903700000000018 3.8301989999999986 0.15327446999999966 1;
	setAttr ".radi" 0.1;
createNode joint -n "spine2ScaProxySkin_jnt" -p "rootProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.4297893648567407e-016 0.45877723285358307 0.068276679482380628 ;
	setAttr ".r" -type "double3" 7.9513867036587919e-016 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -1.4856079173557453 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.9996638688057311 -0.0259258443325966 0
		 0 0.0259258443325966 0.9996638688057311 0 3.4297893648567407e-016 7.2915144406835264 0.19033819329833215 1;
	setAttr ".radi" 0.15;
createNode joint -n "spine3ScaProxySkin_jnt" -p "spine2ScaProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.7753056527488158e-016 0.55348507784094547 0.040007249129140998 ;
	setAttr ".r" -type "double3" -7.9513867036587899e-016 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.4856079173557462 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 2.0816681711721685e-017 0 0 -2.0816681711721685e-017 1 0
		 7.2050950176055565e-016 7.8458506966373447 0.21598242677452451 1;
	setAttr ".radi" 0.15;
createNode joint -n "spine4ScaProxySkin_jnt" -p "spine3ScaProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.5425899425820411e-016 0.62466380460052395 -3.5527136788005009e-015 ;
	setAttr ".s" -type "double3" 1 0.99999999999999989 0.99999999999999989 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 9.7476849601875976e-016 8.4705145012378686 0.21598242677452095 1;
	setAttr ".radi" 0.15;
createNode joint -n "spine5ScaProxySkin_jnt" -p "spine4ScaProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.492970868264509e-016 0.99999999999999289 -0.15585564817312011 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 1.2240655828452107e-015 9.4705145012378615 0.060126778601400893 1;
	setAttr ".radi" 0.15;
createNode joint -n "neck1ProxySkin_jnt" -p "spine5ScaProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.2500864349848624e-015 0.30446359082666952 -0.095045883941949211 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 -2.6020852139651712e-017 9.7749780920645311 -0.034919105340548304 1;
	setAttr ".radi" 0.05;
createNode joint -n "neck2ProxySkin_jnt" -p "neck1ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 1.0819078353428075 -0.01886908635170071 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 2.7755575615628914e-017 0 0 -2.7755575615628914e-017 1 0
		 -2.6020852139652106e-017 10.856885927407339 -0.053788191692248979 1;
	setAttr ".radi" 0.05;
createNode joint -n "head1ProxySkin_jnt" -p "neck1ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.9443045261050599e-031 1.0819078353428075 -0.018869086351700703 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 -2.6020852139652106e-017 10.856885927407339 -0.053788191692248979 1;
	setAttr ".radi" 0.05;
createNode joint -n "head2ProxySkin_jnt" -p "head1ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 1.6465041642181841 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -2.6020852139652109e-017 12.64656436677493 -0.053788191692249028 1;
	setAttr ".radi" 0.05;
createNode joint -n "jaw1LWRProxySkin_jnt" -p "head1ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.053273046991858308 0.19915975822275878 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 -2.6020852139652106e-017 10.80361288041548 0.14537156653050975 1;
	setAttr ".radi" 0.05;
createNode joint -n "jaw2LWRProxySkin_jnt" -p "jaw1LWRProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.25573693215361004 0.10979934618849238 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 -2.6020852139652106e-017 10.54787594826187 0.25517091271900211 1;
	setAttr ".radi" 0.05;
createNode joint -n "jaw3LWRProxySkin_jnt" -p "jaw2LWRProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 -0.13230848947173435 0.71229372376233502 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -2.6020852139652109e-017 10.377191939779944 1.0562692302355619 1;
	setAttr ".radi" 0.05;
createNode joint -n "lowerTeethProxySkin_jnt" -p "jaw2LWRProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.3939183968479942e-017 -0.053915726399003105 0.3580942817370984 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 -2.0816681711721642e-018 10.493960221862867 0.6132651944561005 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue1ProxySkin_jnt" -p "jaw1LWRProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.3939183968479942e-017 -0.66797203780611625 -0.12611578702049323 ;
	setAttr ".jo" -type "double3" 13.85805656745163 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.97089208063779964 0.23951736420310818 0
		 0 -0.23951736420310818 0.97089208063779964 0 0 10.072923878713889 0.025607429179778735 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue2ProxySkin_jnt" -p "tongue1ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 0 0.28650586803189704 0 ;
	setAttr ".jo" -type "double3" 28.772324334095408 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.73573807236441735 0.67726618760535473 0
		 0 -0.67726618760535473 0.73573807236441735 0 0 10.375278529070872 0.10019778824469508 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue3ProxySkin_jnt" -p "tongue2ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 0 0.093143792340696585 -1.7763568394002505e-015 ;
	setAttr ".jo" -type "double3" 31.873398313337596 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.26717481468004534 0.96364807808695585 0
		 0 -0.96364807808695585 0.26717481468004534 0 0 10.449767044537673 0.16876641991609428 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue4ProxySkin_jnt" -p "tongue3ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 0 0.13416181630625168 0 ;
	setAttr ".jo" -type "double3" 16.357191654539537 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 -0.015026210923865246 0.99988710011944437 0
		 0 -0.99988710011944437 -0.015026210923865246 0 0 10.488728629764587 0.30929335082498055 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue5ProxySkin_jnt" -p "tongue4ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 0 0.12175890697676858 5.3290705182007514e-015 ;
	setAttr ".jo" -type "double3" 26.466671488175098 1.2656250278176432e-005 2.0100001834297848e-005 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99999999999991407 2.1559693622946364e-007 3.5409079351199768e-007 0
		 -2.1559693622946334e-007 -0.45907821461042292 0.88839585369927321 0 3.5409079351199783e-007 -0.88839585369927321 -0.45907821461033699 0
		 0 10.486739961266659 0.44162504692358262 1;
	setAttr ".radi" 0.05;
createNode joint -n "tongue6ProxySkin_jnt" -p "tongue5ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0 0.2317389126322178 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99999999999991407 2.1559693622946364e-007 3.5409079351199768e-007 0
		 -2.1559693622946334e-007 -0.45907821461042292 0.88839585369927321 0 3.5409079351199783e-007 -0.88839585369927321 -0.45907821461033699 0
		 -5.4306738661579809e-008 10.371102693585179 0.66540318727493086 1;
	setAttr ".radi" 0.05;
createNode joint -n "jaw1UPRProxySkin_jnt" -p "head1ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.2746451893017067e-017 -0.26234162280091766 0.85439437555777153 ;
	setAttr ".jo" -type "double3" 0 0 180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.224646799147353e-016 2.5493082627129202e-033 0
		 -1.2246467991473532e-016 -0.99999999999999989 -2.0816681711721682e-017 0 0 -2.0816681711721682e-017 0.99999999999999989 0
		 -3.8767304032669173e-017 10.594544304606421 0.8006061838655224 1;
	setAttr ".radi" 0.05;
	setAttr ".liw" yes;
createNode joint -n "jaw2UPRProxySkin_jnt" -p "jaw1UPRProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.5663315849624653e-018 0.053618166393235356 0.21330705326003618 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -1 1.2246467991473532e-016 0 0 -1.2246467991473532e-016 -1 0 0
		 0 0 1 0 -3.9875691153801112e-017 10.513451373935434 1.1067568396314549 1;
	setAttr ".radi" 0.05;
createNode joint -n "upperTeethProxySkin_jnt" -p "jaw1UPRProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.046789543940466e-017 -0.050771703534607582 -0.15684221771952656 ;
	setAttr ".jo" -type "double3" 0 0 -180 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 -2.0816681711729623e-018 10.645316008141029 0.64376396614599585 1;
	setAttr ".radi" 0.05;
createNode joint -n "eyeProxySkinLFT_jnt" -p "head1ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.35157795960861127 0.31867106071784868 0.61413293532834978 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 0.35157795960861127 11.175556988125187 0.56034474363610065 1;
	setAttr ".radi" 0.05;
createNode joint -n "lidProxySkinLFT_jnt" -p "eyeProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0 1.1102230246251565e-016 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 0.35157795960861127 11.175556988125187 0.56034474363610076 1;
	setAttr ".radi" 0.05;
createNode joint -n "irisProxySkinLFT_jnt" -p "eyeProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.049445421226084307 -3.8425730686242332e-007 0.28041776830009024 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 0.40102338083469558 11.17555660386788 0.84076251193619078 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "pupilProxySkinLFT_jnt" -p "irisProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 0.40102338083469558 11.17555660386788 0.84076251193619078 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "eyeSpecProxySkinLFT_jnt" -p "eyeProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.15108081190030753 0.057692792102185919 0.29011472952799322 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.54636822990099865 11.265977071776941 0.9290897048907325 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "eyeProxySkinRGT_jnt" -p "head1ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.351578 0.31870094678524907 0.61413329635686897 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 -0.351578 11.175586874192588 0.56034510466461984 1;
	setAttr ".radi" 0.05;
createNode joint -n "lidProxySkinRGT_jnt" -p "eyeProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0 1.1102230246251565e-016 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 -0.351578 11.175586874192588 0.56034510466461995 1;
	setAttr ".radi" 0.05;
createNode joint -n "irisProxySkinRGT_jnt" -p "eyeProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -0.04954200000000003 -3.0270324707259988e-005 0.28041740727157105 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 -0.40112000000000003 11.17555660386788 0.84076251193619078 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "pupilProxySkinRGT_jnt" -p "irisProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 5;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 -0.40112000000000003 11.17555660386788 0.84076251193619078 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "eyeSpecProxySkinRGT_jnt" -p "eyeProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 0.042623901881453874 0.057668117516710637 0.32426527557637108 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -0.33581967186798489 11.265982736431209 0.96621025606127275 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "earProxySkinLFT_jnt" -p "head1ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.82911281329266362 0.22510805249768673 -0.030742431689400114 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 0.82911281329266362 11.081993979905025 -0.084530623381649087 1;
	setAttr ".radi" 0.05;
createNode joint -n "earProxySkinRGT_jnt" -p "head1ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.82911319999999999 0.22513694678524843 -0.030742451643130893 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 -0.82911319999999999 11.082022874192587 -0.084530643335379865 1;
	setAttr ".radi" 0.05;
createNode joint -n "noseProxySkin_jnt" -p "head1ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.3939183968479612e-017 -0.085037719419164048 1.0301510831507459 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99999999999999989 2.0816681711721682e-017 0
		 0 -2.0816681711721682e-017 0.99999999999999989 0 -2.0816681711724939e-018 10.771848207988175 0.97636289145849664 1;
	setAttr ".radi" 0.05;
createNode joint -n "neckRbnProxySkin_jnt" -p "neck1ProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.1629758220391547e-033 0.54095391767140377 -0.0094345431758503656 ;
	setAttr ".r" -type "double3" -3.975693351829396e-016 0 0 ;
	setAttr ".jo" -type "double3" -0.99916959655089099 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 0.99984794799389343 -0.017437915368540013 0
		 0 0.017437915368540013 0.99984794799389343 0 -2.6020852139651718e-017 10.315932009735935 -0.044353648516398655 1;
	setAttr ".radi" 0.05;
createNode joint -n "clav1ProxySkinLFT_jnt" -p "spine5ScaProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.13883839999999881 0.14298892949857489 0.3579527896174024 ;
	setAttr ".jo" -type "double3" 0 0 -89.999999999999986 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 -2.0816681711721685e-017 0
		 1.0000000000000002 2.2204460492503128e-016 4.6222318665293654e-033 0 0 -2.0816681711721682e-017 0.99999999999999989 0
		 0.13883840000000003 9.6135034307364364 0.41807956821880321 1;
	setAttr ".radi" 0.05;
createNode joint -n "clav2ProxySkinLFT_jnt" -p "clav1ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.1338159639423786 0.63077111459762825 -0.42090445754459743 ;
	setAttr ".ro" 4;
	setAttr ".jo" -type "double3" 0 0 -0.73499999986512199 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.01282781816459683 -0.99991772015558167 -2.0814968918389138e-017 0
		 0.99991772015558189 -0.012827818164596827 -2.67032607788254e-019 0 0 -2.0816681711721682e-017 0.99999999999999989 0
		 0.76960951459762839 9.4796874667940578 -0.0028248893257941665 1;
	setAttr ".radi" 0.05;
createNode joint -n "upArmProxySkinLFT_jnt" -p "clav2ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 7.1054273576010019e-015 2.2204460492503131e-016 5.6812193838240432e-017 ;
	setAttr ".ro" 1;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.01282781816459683 -0.99991772015558167 -2.0814968918389138e-017 0
		 0.99991772015558189 -0.012827818164596827 -2.67032607788254e-019 0 0 -2.0816681711721682e-017 0.99999999999999989 0
		 0.7696095145976285 9.4796874667940507 -0.0028248893257941097 1;
	setAttr ".radi" 0.05;
createNode joint -n "forearmProxySkinLFT_jnt" -p "upArmProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.0658141036401503e-014 1.6423207410052836 -0.057603252582036094 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -0.065657037458636408 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.013973646685032374 -0.99990236383275044 -2.0814649250704497e-017 0
		 0.99990236383275066 -0.01397364668503237 -2.908849553943737e-019 0 0 -2.0816681711721682e-017 0.99999999999999989 0
		 2.4117951257078571 9.4586200749604785 -0.060428141907830198 1;
	setAttr ".radi" 0.05;
createNode joint -n "wristProxySkinLFT_jnt" -p "forearmProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 9.7291064093951718e-012 1.7313194827237246 0.015578141295368664 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" -5.9490654010057266e-005 0 -1.7343429625413607 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.044229663016451468 -0.9990213896155834 -2.0796310290829497e-017 0
		 0.99902138961504505 -0.044229663016427612 -1.0383077866410006e-006 0 1.0372916878578536e-006 -4.5924003531264993e-008 0.99999999999946088 0
		 4.1429455690328689 9.4344272282002564 -0.044850000612461534 1;
	setAttr ".radi" 0.05;
createNode joint -n "handProxySkinLFT_jnt" -p "wristProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.2897239659869228e-012 0.32515157113905468 -9.7144514654701197e-017 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 5.9490654010057171e-005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.044229663016451468 -0.9990213896155834 -2.0796310290829497e-017 0
		 0.99902138961558362 -0.044229663016451454 -9.2178360484949234e-019 0 1.2705494208814505e-021 -2.0816734651280889e-017 1 0
		 4.4677789434678239 9.4200458837818015 -0.044850338219869786 1;
	setAttr ".radi" 0.05;
createNode joint -n "index1ProxySkinLFT_jnt" -p "handProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.071834096998856012 0.00032921715714095967 0.16053431606821644 ;
	setAttr ".r" -type "double3" 1.1708416921137571e-013 2.0375428428125564e-015 9.1440947092076119e-015 ;
	setAttr ".jo" -type "double3" 89.360934000058279 -87.989208589893806 -89.550000000135228 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" -3.9140701048760405e-013 1.2191090942133126e-014 4.7708320221952333e-015 ;
	setAttr ".bps" -type "matrix" -0.035064497254326463 0.0012765646267359299 0.99938423612495253 0
		 0.99825468274758067 -0.047503609812379492 0.035085544421040643 0 0.047519147770440376 0.99887025055186573 0.00039135357936772447 0
		 4.4712850363529864 9.4917951220234595 0.11568397784834666 1;
	setAttr ".radi" 0.05;
createNode joint -n "index2ProxySkinLFT_jnt" -p "index1ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.9694666704478436e-012 0.19351237480890937 -1.0658141036401503e-014 ;
	setAttr ".r" -type "double3" -1.5034154322013597e-012 -8.200972097914489e-012 2.0683750195479057e-009 ;
	setAttr ".jo" -type "double3" -0.036695873975336753 0.00035905388685073447 -0.059031175517570686 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" 0 2.1874078461139952e-015 1.791518517735802e-014 ;
	setAttr ".bps" -type "matrix" -0.036093266442251665 0.0013192467646882618 0.99934755501051897 0
		 0.99818738750984692 -0.04814199984863609 0.036114917511209627 0 0.048158234330134921 0.99883963049057467 0.00042074654607467587 0
		 4.6644596706758286 9.4826025856766503 0.12247346486376054 1;
	setAttr ".radi" 0.05;
createNode joint -n "index3ProxySkinLFT_jnt" -p "index2ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -9.42048522523109e-012 0.26095578074455705 -1.7763568394002505e-014 ;
	setAttr ".r" -type "double3" 1.0507420822539006e-012 3.3843587104431415e-014 -6.2940989556702915e-014 ;
	setAttr ".jo" -type "double3" 0.076261868950438363 -0.00033710950906703396 0.053965028716363203 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" 0 -8.2026034965551996e-015 -2.7830918333950969e-014 ;
	setAttr ".bps" -type "matrix" -0.035152807032660563 0.0012797796480793486 0.99938112966063997 0
		 0.99828415545534144 -0.046813702576130149 0.035174169754665058 0 0.046829746150720282 0.99890281780319634 0.000368049307991249 0
		 4.9249424397131696 9.4700396525195156 0.13189786135000917 1;
	setAttr ".radi" 0.05;
createNode joint -n "index4ProxySkinLFT_jnt" -p "index3ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.529790597120666e-012 0.18089324235916138 -7.1054273576010019e-015 ;
	setAttr ".r" -type "double3" 1.3099623860844504e-013 7.0624871206770642e-015 -5.6068688591843472e-014 ;
	setAttr ".jo" -type "double3" -0.00044774901363409407 0.00086087772789912522 -0.037678314120231211 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" 0 1.0681475677118825e-014 -2.7824961599902952e-014 ;
	setAttr ".bps" -type "matrix" -0.035809985284727355 0.0012955558923288784 0.99935777701924022 0
		 0.9982604567558524 -0.04682065697623676 0.035831362769382973 0 0.046837009207518411 0.99890247152324285 0.00038334482051135123 0
		 5.1055252973894927 9.461571370073667 0.13826063095769633 1;
	setAttr ".radi" 0.05;
createNode joint -n "index5ProxySkinLFT_jnt" -p "index4ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.7144358689420471e-012 0.15827241539955139 5.5067062021407764e-014 ;
	setAttr ".r" -type "double3" 0 -2.7373269929431614e-014 0 ;
	setAttr ".jo" -type "double3" 0 -0.00046662615242786209 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.035809603835314723 0.0013036911165710803 0.99935778010812049 0
		 0.9982604567558524 -0.04682065697623676 0.035831362769382973 0 0.046837300848312849 0.99890246093889756 0.00037520588813141692 0
		 5.2635223910783084 9.454160951603491 0.14393174728455338 1;
	setAttr ".radi" 0.05;
createNode joint -n "middle1ProxySkinLFT_jnt" -p "handProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.096357116251471808 -0.021749316773724381 0.019502999900416068 ;
	setAttr ".r" -type "double3" 4.6793910751031964e-013 1.1310536984911554e-014 -8.8459177078203548e-015 ;
	setAttr ".jo" -type "double3" -91.43192046207075 -89.63534427554994 90.449999999868268 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" 3.584982087678362e-012 -1.1350915120515888e-014 -4.7708320221956301e-015 ;
	setAttr ".bps" -type "matrix" 0.0063601865284547794 -0.00023155013695949178 0.9999797470008368 0
		 0.99809649290800173 -0.061342567199117431 -0.0063624126060650207 0 0.061342798045665614 0.99811674459151123 -0.00015904078988902145 0
		 4.4503127535823879 9.5172706689104363 -0.025347338319453714 1;
	setAttr ".radi" 0.05;
createNode joint -n "middle2ProxySkinLFT_jnt" -p "middle1ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -7.6428648566190249e-012 0.21218860366370951 1.5987211554602254e-014 ;
	setAttr ".r" -type "double3" 3.5581273611017844e-013 -3.6926849864549743e-011 2.0679373898677073e-009 ;
	setAttr ".jo" -type "double3" -0.041209118778132228 -0.00048942433951585935 0.002886725239324824 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" 1.9100540534516016e-012 1.7051871198902175e-014 1.9593408357938934e-014 ;
	setAttr ".bps" -type "matrix" 0.006410997470262021 -0.00022611477136106066 0.99997942378008287 0
		 0.99805179320772563 -0.062060419830101306 -0.0064126722476400789 0 0.062060592861173838 0.99807236870009686 -0.00017219494150651592 0
		 4.6620974547341349 9.5042544752313258 -0.026697369773909747 1;
	setAttr ".radi" 0.05;
createNode joint -n "middle3ProxySkinLFT_jnt" -p "middle2ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.0632317075376374e-011 0.29458412528038114 -1.7763568394002505e-014 ;
	setAttr ".r" -type "double3" -1.1276060636685708e-012 4.5859432630875865e-015 -1.5968671791180336e-014 ;
	setAttr ".jo" -type "double3" 0.17882062970741608 6.702681183394762e-005 -0.0011365007323972211 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" -71.036426519073288 8.4075201840072483e-015 2.1740323517251835e-014 ;
	setAttr ".bps" -type "matrix" 0.006391127832397865 -0.0002260513442409475 0.99997955098382885 0
		 0.99824075068459728 -0.058945134500564766 -0.006393339609529443 0 0.058945374353569099 0.9982611982940639 -0.00015107222612264831 0
		 4.9561076692206791 9.4859724607411273 -0.028586441229322669 1;
	setAttr ".radi" 0.05;
createNode joint -n "middle4ProxySkinLFT_jnt" -p "middle3ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -7.4071469444159099e-012 0.20521691441535594 4.9737991503207013e-014 ;
	setAttr ".r" -type "double3" 2.0340140784632155e-012 -2.3926360445737093e-014 2.7584834993917281e-014 ;
	setAttr ".jo" -type "double3" 0.24410080716207422 5.0774676641925357e-005 0.0015474822695821509 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" -83.553142532007968 1.5892963616316056e-014 2.7653581114838974e-015 ;
	setAttr ".bps" -type "matrix" 0.0064180367392902525 -0.0002285280176806159 0.99997937807704751 0
		 0.99848264628439198 -0.054691651427985627 -0.0064209292939906796 0 0.054691990943206967 0.9985032654122995 -0.00012283176835339927 0
		 5.1609635559197962 9.4738759221191753 -0.029898462664206781 1;
	setAttr ".radi" 0.05;
createNode joint -n "middle5ProxySkinLFT_jnt" -p "middle4ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.2919970882785448e-012 0.17431402206421254 1.9539925233402755e-014 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0064180367392902525 -0.0002285280176806159 0.99997937807704751 0
		 0.99848264628439198 -0.054691651427985627 -0.0064209292939906796 0 0.054691990943206967 0.9985032654122995 -0.00012283176835339927 0
		 5.3350130819549078 9.4643424003854495 -0.03101772068112409 1;
	setAttr ".radi" 0.05;
createNode joint -n "ring1ProxySkinLFT_jnt" -p "handProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.094180770444811657 -0.044301815421710522 -0.092723603045687039 ;
	setAttr ".r" -type "double3" -4.0552072188659847e-014 2.2611755938529645e-015 -1.3119788061037007e-014 ;
	setAttr ".jo" -type "double3" -90.97128152872628 -83.803528305899817 90.449999999820648 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.10786667499657975 -0.0039270142876548543 0.99415761274757974 0
		 0.99273996304299472 -0.053104354933116195 -0.10792262628629704 0 0.05321791242219448 0.99858124659234515 -0.0018296861756776708 0
		 4.4276860660021375 9.5160939423137627 -0.13757394126555683 1;
	setAttr ".radi" 0.05;
createNode joint -n "ring2ProxySkinLFT_jnt" -p "ring1ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -7.567724225054917e-012 0.21038604780426384 3.0198066269804258e-014 ;
	setAttr ".r" -type "double3" 1.5479859027016729e-012 0 0 ;
	setAttr ".jo" -type "double3" 0.2495726218512751 0 0 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" -88.156538719980375 0 0 ;
	setAttr ".bps" -type "matrix" 0.10786667499657975 -0.0039270142876548543 0.99415761274757974 0
		 0.99296235441120184 -0.048754180822349918 -0.10792957228908485 0 0.048893180990250945 0.99880308790628292 -0.0013595740246127091 0
		 4.6365447033232892 9.504921526958249 -0.16027935608611099 1;
	setAttr ".radi" 0.05;
createNode joint -n "ring3ProxySkinLFT_jnt" -p "ring2ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.0189238341951068e-011 0.28313311934471042 -1.5987211554602254e-014 ;
	setAttr ".r" -type "double3" 9.8348714290879682e-014 0 0 ;
	setAttr ".jo" -type "double3" -0.44068493512937257 0 0 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" -88.156538719980375 0 0 ;
	setAttr ".bps" -type "matrix" 0.10786667499657975 -0.0039270142876548543 0.99415761274757974 0
		 0.99255693042551929 -0.056434860142619489 -0.10791592294445797 0 0.056528933206398485 0.99839856025510543 -0.0021896554703895107 0
		 4.9176852321185009 9.4911176036609444 -0.19083779256798969 1;
	setAttr ".radi" 0.05;
createNode joint -n "ring4ProxySkinLFT_jnt" -p "ring3ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.322387058332879e-012 0.17579954862594249 5.3290705182007514e-015 ;
	setAttr ".r" -type "double3" 2.8828746417452908e-013 0 0 ;
	setAttr ".jo" -type "double3" -0.14419453942172436 0 0 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" -88.156538719980375 0 0 ;
	setAttr ".bps" -type "matrix" 0.10786667499657975 -0.0039270142876548543 0.99415761274757974 0
		 0.99241152269480504 -0.058947317953516216 -0.10791007056186205 0 0.059026689283418518 0.99825337077561205 -0.0024612369587561772 0
		 5.0921762924721765 9.4811963807211335 -0.20980936311746284 1;
	setAttr ".radi" 0.05;
createNode joint -n "ring5ProxySkinLFT_jnt" -p "ring4ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.6238902423899617e-012 0.15640121698379694 1.7763568394002505e-015 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.10786667499657975 -0.0039270142876548543 0.99415761274757974 0
		 0.99241152269480504 -0.058947317953516216 -0.10791007056186205 0 0.059026689283418518 0.99825337077561205 -0.0024612369587561772 0
		 5.2473906623697806 9.4719769484552963 -0.2266866294837365 1;
	setAttr ".radi" 0.05;
createNode joint -n "pinky1ProxySkinLFT_jnt" -p "handProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.070713421598908255 -0.069291364650414522 -0.19039963955702227 ;
	setAttr ".r" -type "double3" 1.033680271475643e-014 -2.6835930124848424e-014 -3.1805546814635187e-015 ;
	setAttr ".jo" -type "double3" -91.081810359677235 -78.198135734683419 90.449999999841836 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" -2.7829853462805765e-014 -9.6410563781862932e-015 
		-3.4986101496098681e-014 ;
	setAttr ".bps" -type "matrix" 0.20439249457969288 -0.0074411512779193377 0.97886073443935284 0
		 0.97735142738119596 -0.054474176927880633 -0.20449144589436832 0 0.054844284619544943 0.99848745275826534 -0.0038614930896312197 0
		 4.4016830188744667 9.493754838200454 -0.23524997777689205 1;
	setAttr ".radi" 0.05;
createNode joint -n "pinky2ProxySkinLFT_jnt" -p "pinky1ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -7.1502803677958582e-012 0.19875196162008368 3.5527136788005009e-015 ;
	setAttr ".r" -type "double3" 3.3074289955537717e-012 0 0 ;
	setAttr ".jo" -type "double3" 0.19963920455319875 0 0 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" 1.888454342118963e-015 0 0 ;
	setAttr ".bps" -type "matrix" 0.20439249457969288 -0.0074411512779193377 0.97886073443935284 0
		 0.97753659140910831 -0.050994762114431783 -0.20450365936372641 0 0.051438512962106359 0.99867119889608891 -0.0031489489826899471 0
		 4.5959335322572068 9.4829279886784548 -0.27589305378992413 1;
	setAttr ".radi" 0.05;
createNode joint -n "pinky3ProxySkinLFT_jnt" -p "pinky2ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -8.7715390506559743e-012 0.24372555315494449 -8.8817841970012523e-015 ;
	setAttr ".r" -type "double3" 5.9237830942257995e-014 0 0 ;
	setAttr ".jo" -type "double3" -0.56403620498940021 0 0 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" -73.893419638676178 0 0 ;
	setAttr ".bps" -type "matrix" 0.20439249457969288 -0.0074411512779193377 0.97886073443935284 0
		 0.97698285784234973 -0.060823340168520201 -0.20446275155377333 0 0.061059017693423541 0.99812080960092375 -0.0051619569845742126 0
		 4.8341841787257973 9.470499262074167 -0.3257358212991448 1;
	setAttr ".radi" 0.05;
createNode joint -n "pinky4ProxySkinLFT_jnt" -p "pinky3ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.3435034175208784e-012 0.14867991209030151 1.2434497875801753e-014 ;
	setAttr ".r" -type "double3" -4.0179350936925827e-014 0 0 ;
	setAttr ".jo" -type "double3" -0.2213875943429606 0 0 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" -3.6029721000953897e-015 0 0 ;
	setAttr ".bps" -type "matrix" 0.20439249457969288 -0.0074411512779193377 0.97886073443935284 0
		 0.97673963670636688 -0.064679557885368957 -0.20444127978093896 0 0.064833558025186297 0.99787834131191167 -0.0059519487886301719 0
		 4.9794419041424378 9.461456053204925 -0.35613532523113156 1;
	setAttr ".radi" 0.05;
createNode joint -n "pinky5ProxySkinLFT_jnt" -p "pinky4ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -4.6372905515568164e-012 0.12894588708877741 7.1054273576010019e-015 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.20439249457969288 -0.0074411512779193377 0.97886073443935284 0
		 0.97673963670636688 -0.064679557885368957 -0.20444127978093896 0 0.064833558025186297 0.99787834131191167 -0.0059519487886301719 0
		 5.1053884630513631 9.4531158902369281 -0.38249718741458899 1;
	setAttr ".radi" 0.05;
createNode joint -n "thumb1ProxySkinLFT_jnt" -p "wristProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.012084538774733034 0.17039640549673241 0.1591404739149237 ;
	setAttr ".r" -type "double3" -6.3611093629270351e-015 -3.180554681463516e-015 2.0673605429512861e-014 ;
	setAttr ".jo" -type "double3" 58.726900602226138 -35.906113562725814 -72.096381831644507 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" -3.1805546814635152e-015 -3.1805546814635168e-014 
		-6.3611093629270327e-015 ;
	setAttr ".bps" -type "matrix" -0.78101418331446315 -0.214667181402214 0.58645958657905084 0
		 0.62090511762237544 -0.36770989043585495 0.69229059749934185 0 0.067034919049601266 0.90482453420786535 0.42047459129361536 0
		 4.3137098829904295 9.4389633580171548 0.11429029637846175 1;
	setAttr ".radi" 0.05;
createNode joint -n "thumb2ProxySkinLFT_jnt" -p "thumb1ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.0027852957690095437 0.1839878097557478 -1.6999734953060397e-012 ;
	setAttr ".r" -type "double3" -7.2520065834622068e-010 -6.0487175183247476e-010 1.8400865258120098e-009 ;
	setAttr ".jo" -type "double3" -1.459343170236951 1.7112786981139454 13.976548495956962 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" -1.5182178987298507e-014 -2.300932527371263e-014 4.2521282801987861e-015 ;
	setAttr ".bps" -type "matrix" -0.60965878641504412 -0.32401118380709693 0.72341752599370934 0
		 0.7896565348098018 -0.32768481264622401 0.51871497047336657 0 0.068983484828704714 0.88749051619098029 0.45563347384927194 0
		 4.4301242111481134 9.3719071322409029 0.24002986372095514 1;
	setAttr ".radi" 0.05;
createNode joint -n "thumb3ProxySkinLFT_jnt" -p "thumb2ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.0019784649266521015 0.22931264340872415 0.00096787958213440106 ;
	setAttr ".r" -type "double3" -1.7592552277524298e-014 1.9049987008059578e-014 -9.3432736945295969e-015 ;
	setAttr ".jo" -type "double3" -0.1001403796666358 -0.050850715758917864 0.17425363140180541 ;
	setAttr ".ssc" no;
	setAttr ".pa" -type "double3" 6.363742192081538e-015 -1.9044624286924606e-014 -1.2791388122212647e-014 ;
	setAttr ".bps" -type "matrix" -0.60719292564248584 -0.32421848474094267 0.72539584035339333 0
		 0.79138431633382578 -0.32824902496324454 0.5157164351380733 0 0.070905676151927971 0.88720626224287125 0.45589190969622567 0
		 4.6124753947629396 9.2982648903610698 0.35798750690965281 1;
	setAttr ".radi" 0.05;
createNode joint -n "thumb4ProxySkinLFT_jnt" -p "thumb3ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.0013897984503961069 0.19772319495675816 0.001738043752750329 ;
	setAttr ".r" -type "double3" -6.7405655235830327e-014 1.0059979535083125e-014 -4.4717777396880125e-014 ;
	setAttr ".jo" -type "double3" -0.66779108195938741 -0.002316688761703883 -0.19875968567707808 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.60993172000846874 -0.32304196331502832 0.72362088614486708 0
		 0.78839290680509422 -0.33968983693790733 0.51288150598409377 0 0.080124412178343776 0.88332027295804205 0.46187159899089386 0
		 4.7699175431817533 9.2353550460530709 0.45974081422759239 1;
	setAttr ".radi" 0.05;
createNode joint -n "palmProxySkinLFT_jnt" -p "wristProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.072019205767340111 0.1450430993690448 -0.15726462283144191 ;
	setAttr ".s" -type "double3" 1 1.0000000000000002 1.0000000000000002 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -91.081810363299212 -78.198195187394106 90.450000003387004 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.20439249490621911 -0.0074411592648131603 0.97886073431045673 0
		 0.97735142731298175 -0.05447417525936659 -0.20449144666486643 0 0.054844284618265425 0.99848745278977236 -0.0038614849608921875 0
		 4.2910319497911207 9.4999607550392255 -0.20211477404319811 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "forearmRbnDtl1ProxySkinLFT_jnt" -p "forearmProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 9.681144774731365e-013 0.17313194827237277 0.0015578141295370662 ;
	setAttr ".jo" -type "double3" -4.969817858737948e-017 -0.515524529898027 89.99999999967838 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99986188965121214 -0.013973081063508188 0.008997479019164957 0
		 0.013973646690645668 0.99990236383267195 2.0814649250702864e-017 0 -0.0089966005397979151 0.00012572759292028867 0.99995952186641013 0
		 2.5849101700403585 9.4562007902844609 -0.058870327778293131 1;
	setAttr ".radi" 0.1;
createNode joint -n "forearmRbnDtl2ProxySkinLFT_jnt" -p "forearmProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.90789614609821e-012 0.51939584481711831 0.0046734423886108864 ;
	setAttr ".jo" -type "double3" -4.969817858737948e-017 -0.515524529898027 89.99999999967838 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99986188965121214 -0.013973081063508188 0.008997479019164957 0
		 0.013973646690645668 0.99990236383267195 2.0814649250702864e-017 0 -0.0089966005397979151 0.00012572759292028867 0.99995952186641013 0
		 2.9311402587053617 9.4513622209324222 -0.055754699519219311 1;
	setAttr ".radi" 0.1;
createNode joint -n "forearmRbnDtl3ProxySkinLFT_jnt" -p "forearmProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 4.8547832420808845e-012 0.86565974136186341 0.0077890706476843458 ;
	setAttr ".jo" -type "double3" -4.969817858737948e-017 -0.515524529898027 89.99999999967838 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99986188965121214 -0.013973081063508188 0.008997479019164957 0
		 0.013973646690645668 0.99990236383267195 2.0814649250702864e-017 0 -0.0089966005397979151 0.00012572759292028867 0.99995952186641013 0
		 3.2773703473703639 9.4465236515803763 -0.052639071260145852 1;
	setAttr ".radi" 0.1;
createNode joint -n "forearmRbnDtl4ProxySkinLFT_jnt" -p "forearmProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 6.7981176243847585e-012 1.2119236379066076 0.010904698906757798 ;
	setAttr ".jo" -type "double3" -4.969817858737948e-017 -0.515524529898027 89.99999999967838 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99986188965121214 -0.013973081063508188 0.008997479019164957 0
		 0.013973646690645668 0.99990236383267195 2.0814649250702864e-017 0 -0.0089966005397979151 0.00012572759292028867 0.99995952186641013 0
		 3.6236004360353657 9.4416850822283358 -0.049523443001072399 1;
	setAttr ".radi" 0.1;
createNode joint -n "forearmRbnDtl5ProxySkinLFT_jnt" -p "forearmProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 8.7485574340462335e-012 1.5581875344513532 0.014020327165831625 ;
	setAttr ".jo" -type "double3" -4.969817858737948e-017 -0.515524529898027 89.99999999967838 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99986188965121214 -0.013973081063508188 0.008997479019164957 0
		 0.013973646690645668 0.99990236383267195 2.0814649250702864e-017 0 -0.0089966005397979151 0.00012572759292028867 0.99995952186641013 0
		 3.9698305247003685 9.4368465128762864 -0.046407814741998572 1;
	setAttr ".radi" 0.1;
createNode joint -n "upArmRbnDtl1ProxySkinLFT_jnt" -p "upArmProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.3290705182007514e-015 0.16423207410052854 -0.0057603252582036548 ;
	setAttr ".r" -type "double3" 1.987846675914698e-016 -3.975693351829396e-016 -6.8967285907033728e-034 ;
	setAttr ".jo" -type "double3" 0 2.0087859661419185 89.999999999999602 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99930323435890134 -0.012819935003913931 -0.035052746915681131 0
		 0.012827818164603714 0.99991772015558156 2.0814968918389135e-017 0 0.03504986278111849 -0.0004496502636042605 0.9993854636393632 0
		 0.93382807570865167 9.4775807276107003 -0.0085852145839977641 1;
	setAttr ".radi" 0.1;
createNode joint -n "upArmRbnDtl2ProxySkinLFT_jnt" -p "upArmProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -3.5527136788005009e-015 0.49269622230158527 -0.0172809757746109 ;
	setAttr ".r" -type "double3" 1.987846675914698e-016 -3.975693351829396e-016 -6.8967285907033728e-034 ;
	setAttr ".jo" -type "double3" 0 2.0087859661419185 89.999999999999602 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99930323435890134 -0.012819935003913931 -0.035052746915681131 0
		 0.012827818164603714 0.99991772015558156 2.0814968918389135e-017 0 0.03504986278111849 -0.0004496502636042605 0.9993854636393632 0
		 1.2622651979306974 9.4733672492439851 -0.020105865100405008 1;
	setAttr ".radi" 0.1;
createNode joint -n "upArmRbnDtl3ProxySkinLFT_jnt" -p "upArmProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.7763568394002505e-015 0.82116037050264123 -0.028801626291018022 ;
	setAttr ".r" -type "double3" 1.987846675914698e-016 -3.975693351829396e-016 -6.8967285907033728e-034 ;
	setAttr ".jo" -type "double3" 0 2.0087859661419185 89.999999999999602 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99930323435890134 -0.012819935003913931 -0.035052746915681131 0
		 0.012827818164603714 0.99991772015558156 2.0814968918389135e-017 0 0.03504986278111849 -0.0004496502636042605 0.9993854636393632 0
		 1.5907023201527424 9.4691537708772717 -0.03162651561681213 1;
	setAttr ".radi" 0.1;
createNode joint -n "upArmRbnDtl4ProxySkinLFT_jnt" -p "upArmProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 1.1496245187036984 -0.040322276807425193 ;
	setAttr ".r" -type "double3" 1.987846675914698e-016 -3.975693351829396e-016 -6.8967285907033728e-034 ;
	setAttr ".jo" -type "double3" 0 2.0087859661419185 89.999999999999602 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99930323435890134 -0.012819935003913931 -0.035052746915681131 0
		 0.012827818164603714 0.99991772015558156 2.0814968918389135e-017 0 0.03504986278111849 -0.0004496502636042605 0.9993854636393632 0
		 1.9191394423747887 9.4649402925105584 -0.043147166133219297 1;
	setAttr ".radi" 0.1;
createNode joint -n "upArmRbnDtl5ProxySkinLFT_jnt" -p "upArmProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.0658141036401503e-014 1.4780886669047555 -0.051842927323832437 ;
	setAttr ".r" -type "double3" 1.987846675914698e-016 -3.975693351829396e-016 -6.8967285907033728e-034 ;
	setAttr ".jo" -type "double3" 0 2.0087859661419185 89.999999999999602 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99930323435890134 -0.012819935003913931 -0.035052746915681131 0
		 0.012827818164603714 0.99991772015558156 2.0814968918389135e-017 0 0.03504986278111849 -0.0004496502636042605 0.9993854636393632 0
		 2.2475765645968346 9.4607268141438343 -0.054667816649626541 1;
	setAttr ".radi" 0.1;
createNode joint -n "clav1ScaProxySkinLFT_jnt" -p "clav1ProxySkinLFT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0 -1.6653345369377348e-016 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 2.2204460492503131e-016 -1 -2.0816681711721685e-017 0
		 1.0000000000000002 2.2204460492503128e-016 4.6222318665293654e-033 0 0 -2.0816681711721682e-017 0.99999999999999989 0
		 0.13883840000000003 9.6135034307364364 0.41807956821880304 1;
	setAttr ".radi" 0.1;
createNode joint -n "clav1ProxySkinRGT_jnt" -p "spine5ScaProxySkin_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.13883800000000121 0.14298549876213862 0.35795322139859898 ;
	setAttr ".r" -type "double3" -4.7044035846079438e-046 2.1186750230640761e-030 -2.5444437451708134e-014 ;
	setAttr ".jo" -type "double3" 180 0 89.999999999999957 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 7.7715611723760958e-016 0.99999999999999989 2.0816681711721682e-017 0
		 1 -6.6613381477509383e-016 1.2246467991473527e-016 0 1.2246467991473532e-016 2.0816681711721583e-017 -0.99999999999999989 0
		 -0.13883799999999999 9.6135000000000002 0.41807999999999979 1;
	setAttr ".radi" 0.05;
createNode joint -n "clav2ProxySkinRGT_jnt" -p "clav1ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -0.13381000000000043 -0.63077200000000011 0.42090488999999992 ;
	setAttr ".ro" 4;
	setAttr ".jo" -type "double3" 0 0 -0.73499999986489006 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.012827818164592223 0.99991772015558156 1.9244014272857825e-017 0
		 0.99991772015558167 0.012827818164592332 1.2272163614771331e-016 0 1.2246467991473532e-016 2.0816681711721583e-017 -0.99999999999999989 0
		 -0.76961000000000024 9.4796899999999997 -0.0028248900000001353 1;
	setAttr ".radi" 0.05;
createNode joint -n "upArmProxySkinRGT_jnt" -p "clav2ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 1.1102230246251565e-016 2.2291196666301971e-016 ;
	setAttr ".ro" 1;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.012827818164592223 0.99991772015558156 1.9244014272857825e-017 0
		 0.99991772015558167 0.012827818164592332 1.2272163614771331e-016 0 1.2246467991473532e-016 2.0816681711721583e-017 -0.99999999999999989 0
		 -0.76961000000000013 9.4796899999999997 -0.0028248900000003583 1;
	setAttr ".radi" 0.05;
createNode joint -n "forearmProxySkinRGT_jnt" -p "upArmProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.5516519794877013e-006 -1.6423251629910234 0.05760320999999969 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 0 0 -0.065657037450659109 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.013973646684888548 0.99990236383275222 1.910337108219796e-017 0
		 0.99990236383275233 0.013973646684888657 1.2274360788815658e-016 0 1.2246467991473532e-016 2.0816681711721583e-017 -0.99999999999999989 0
		 -2.4118000000000004 9.4586199999999856 -0.060428100000000241 1;
	setAttr ".radi" 0.05;
createNode joint -n "wristProxySkinRGT_jnt" -p "forearmProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.8402774514546536e-006 -1.7313189996623697 -0.015578100000000615 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 5.9482968137252059e-005 0 -1.7343429625495241 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.044229663016450101 0.99902138961558329 1.5379738766783229e-017 0
		 0.99902138961504494 0.04422966301642637 -1.038173642730474e-006 0 -1.0371576752235505e-006 -4.591807035516475e-008 -0.99999999999946099 0
		 -4.1429499999999937 9.4344300000000061 -0.044849999999999841 1;
	setAttr ".radi" 0.05;
createNode joint -n "handProxySkinRGT_jnt" -p "wristProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1938549544510124e-006 -0.32514814054251984 6.3756022943006752e-007 ;
	setAttr ".ro" 1;
	setAttr ".jo" -type "double3" 5.9464583652052061e-005 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.044229663016450101 0.99902138961558329 1.5379738766783229e-017 0
		 0.99902138961343045 0.044229663016354892 -2.0760264157850715e-006 0 -2.0739947947769413e-006 -9.1821948768057987e-008 -0.99999999999784495 0
		 -4.4677799999999932 9.4200499999999998 -0.044850299999999774 1;
	setAttr ".radi" 0.05;
createNode joint -n "index1ProxySkinRGT_jnt" -p "handProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.071835030822123613 -0.0003334200295750378 -0.16053429930815716 ;
	setAttr ".r" -type "double3" 1.3517357396219947e-014 6.5909541348296703e-015 4.7708320221952759e-015 ;
	setAttr ".jo" -type "double3" 89.360907373261298 -87.989327535840218 -89.549973389734902 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.035064497218368726 -0.0012765646251441582 -0.99938423612621607 0
		 0.99825468274884122 0.04750360981246584 -0.0350855443850479 0 0.047519147770484778 -0.9988702505518634 -0.00039135357925001134 0
		 -4.4712900000000015 9.4918000000000173 0.11568400000000019 1;
	setAttr ".radi" 0.05;
createNode joint -n "index2ProxySkinRGT_jnt" -p "index1ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.337431626060372e-007 -0.19350808603768765 7.6756308295955478e-006 ;
	setAttr ".r" -type "double3" -1.0003928510440875e-011 -8.9105099852374145e-014 6.2208323860511536e-012 ;
	setAttr ".jo" -type "double3" -0.036695873911831919 0.00035905388683717685 -0.059031175510038454 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.036093266406164011 -0.0013192467630905717 -0.99934755501182426 0
		 0.99818738751120251 0.048141999847617169 -0.036114917475087376 0 0.048158234329074033 -0.99883963049062563 -0.00042074654589357053 0
		 -4.6644599999999894 9.4825999999999908 0.12247299999999993 1;
	setAttr ".radi" 0.05;
createNode joint -n "index3ProxySkinRGT_jnt" -p "index2ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -7.0693316428505071e-007 -0.26095288991928545 -2.7966553002300998e-006 ;
	setAttr ".r" -type "double3" 9.1665719748612971e-012 3.6254178786117306e-016 -1.2780492322360658e-014 ;
	setAttr ".jo" -type "double3" 0.076261868875729055 -0.00033710950897458962 0.053965028709969824 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.03515280699668312 -0.0012797796464863798 -0.99938112966190729 0
		 0.99828415545659666 0.046813702576412007 -0.035174169718652427 0 0.046829746150959063 -0.99890281780318502 -0.0003680493079055458 0
		 -4.9249399926184738 9.4700400003560272 0.13189799973293281 1;
	setAttr ".radi" 0.05;
createNode joint -n "index4ProxySkinRGT_jnt" -p "index3ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.3021097902975463e-008 -0.18090045452117476 1.381111655263112e-006 ;
	setAttr ".r" -type "double3" 8.1454077696720993e-010 -3.1434396293209208e-013 9.6526348159702494e-012 ;
	setAttr ".jo" -type "double3" -0.00044774924201422821 0.00086087772797327294 -0.037678314119900108 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.035809985248745041 -0.0012955558907345322 -0.99935777702053152 0
		 0.99826045675694486 0.046820656980501286 -0.035831362733363882 0 0.046837009211736745 -0.9989024715230449 -0.00038334482056950257 0
		 -5.1055299862140071 9.4615700006563497 0.13826099950727397 1;
	setAttr ".radi" 0.05;
createNode joint -n "index5ProxySkinRGT_jnt" -p "index4ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.3831488394899782e-007 -0.15826530754566193 -8.5719856102173253e-008 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.035809985248745041 -0.0012955558907345322 -0.99935777702053152 0
		 0.99826045675694486 0.046820656980501286 -0.035831362733363882 0 0.046837009211736745 -0.9989024715230449 -0.00038334482056950257 0
		 -5.2635199834751258 9.4541600007848121 0.1439319994089652 1;
	setAttr ".radi" 0.05;
createNode joint -n "middle1ProxySkinRGT_jnt" -p "handProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.096352167285552071 0.02175287102624246 -0.01950304515957688 ;
	setAttr ".r" -type "double3" -5.5331712224085623e-013 2.0882484631005439e-014 -4.2738703532167027e-015 ;
	setAttr ".jo" -type "double3" -91.431773721783586 -89.635225329602207 90.449853262560822 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0063601865644326658 0.00023155013855210478 -0.99997974700060743 0
		 0.99809649290777958 0.061342567198987528 0.0063624126420728658 0 0.061342798045540006 -0.99811674459151867 0.00015904079050564095 0
		 -4.4503100000000053 9.5172700000000212 -0.025347299999999792 1;
	setAttr ".radi" 0.05;
createNode joint -n "middle2ProxySkinRGT_jnt" -p "middle1ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.3961140427423916e-008 -0.21219412635117063 3.4740955570100596e-006 ;
	setAttr ".r" -type "double3" 6.0792357452874735e-012 -2.0774640502734119e-011 1.2270868257643979e-010 ;
	setAttr ".jo" -type "double3" -0.04120911873974354 -0.00048942432392759451 0.0028867251514800916 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0064109975046929453 0.00022611477313117206 -0.9999794237798616 0
		 0.99805179320755322 0.062060419829302972 0.0064126722821146219 0 0.062060592860380813 -0.99807236870014582 0.00017219494187157132 0
		 -4.6620999999999979 9.5042500000000096 -0.026697399999999795 1;
	setAttr ".radi" 0.05;
createNode joint -n "middle3ProxySkinRGT_jnt" -p "middle2ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.9622887465675687e-008 -0.2945837974548331 -1.9972832898673687e-006 ;
	setAttr ".r" -type "double3" -1.2883802561366374e-011 -5.6894759847414248e-012 3.20284026251702e-010 ;
	setAttr ".jo" -type "double3" 0.17882062966523021 6.7026793513365643e-005 -0.0011365005982870958 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0063911278691847406 0.00022605134583719922 -0.99997955098359315 0
		 0.99824075068436469 0.058945134500500804 0.0063933396463464534 0 0.058945374353509647 -0.99826119829406712 0.00015107222669725562 0
		 -4.9561100116986223 9.485969999272573 -0.02858640007516566 1;
	setAttr ".radi" 0.05;
createNode joint -n "middle4ProxySkinRGT_jnt" -p "middle3ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1766407189556238e-007 -0.20521065592765719 -6.180270812450317e-006 ;
	setAttr ".r" -type "double3" 2.4663250708456155e-012 2.3262694970526454e-011 -4.7192749847968998e-010 ;
	setAttr ".jo" -type "double3" 0.24410080716821847 5.0774676970269527e-005 0.0015474821538865461 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0064180367740610681 0.0002285280191635605 -0.99997937807682369 0
		 0.99848264628417682 0.054691651427814993 0.0064209293287905769 0 0.054691990943041453 -0.99850326541230838 0.00012283176877333178 0
		 -5.1609600144666485 9.4738799991091263 -0.029898500092893767 1;
	setAttr ".radi" 0.05;
createNode joint -n "middle5ProxySkinRGT_jnt" -p "middle4ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -6.2537884572277935e-008 -0.17431485652923584 6.4426550991925069e-006 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.0064180367740610681 0.0002285280191635605 -0.99997937807682369 0
		 0.99848264628417682 0.054691651427814993 0.0064209293287905769 0 0.054691990943041453 -0.99850326541230838 0.00012283176877333178 0
		 -5.3350100217403424 9.4643399987106935 -0.031017700139668616 1;
	setAttr ".radi" 0.05;
createNode joint -n "ring1ProxySkinRGT_jnt" -p "handProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.094172847068373144 0.044298776842532028 0.092723608034768909 ;
	setAttr ".r" -type "double3" -8.7465253740246703e-015 1.3219180394832742e-014 3.9756933518293861e-016 ;
	setAttr ".jo" -type "double3" -90.97127287376253 -83.803409359953179 90.449991395423837 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.10786667503234836 0.0039270142892380766 -0.99415761274369252 0
		 0.99273996303911172 0.053104354932937692 0.10792262632209929 0 0.053217912422121948 -0.99858124659234837 0.0018296861760013741 0
		 -4.4276900000000055 9.5160900000000215 -0.13757399999999975 1;
	setAttr ".radi" 0.05;
createNode joint -n "ring2ProxySkinRGT_jnt" -p "ring1ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 5.287672292797474e-007 -0.21037730015595457 -1.9515095388555892e-006 ;
	setAttr ".r" -type "double3" -2.7543603541474054e-012 0 0 ;
	setAttr ".jo" -type "double3" 0.24957262184742116 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.10786667503234836 0.0039270142892380766 -0.99415761274369252 0
		 0.9929623544073154 0.048754180822238591 0.1079295723248881 0 0.048893180990262117 -0.99880308790628214 0.0013595740247877199 0
		 -4.6365399999999966 9.5049200000000109 -0.16027899999999934 1;
	setAttr ".radi" 0.05;
createNode joint -n "ring3ProxySkinRGT_jnt" -p "ring2ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -4.4599470105932681e-007 -0.28314238786697299 -4.3824449278417887e-006 ;
	setAttr ".r" -type "double3" 2.4935051741004992e-012 0 0 ;
	setAttr ".jo" -type "double3" -0.44068493512310797 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.10786667503234836 0.0039270142892380766 -0.99415761274369252 0
		 0.99255693042163906 0.056434860142398992 0.10791592298025905 0 0.05652893320627124 -0.99839856025511153 0.0021896554708280913 0
		 -4.9176899944685335 9.4911200002716036 -0.19083799939876031 1;
	setAttr ".radi" 0.05;
createNode joint -n "ring4ProxySkinRGT_jnt" -p "ring3ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -4.4803678334037755e-007 -0.17579837143421173 -1.1597913545102756e-006 ;
	setAttr ".r" -type "double3" -2.8004287008617292e-012 0 0 ;
	setAttr ".jo" -type "double3" -0.1441945394061718 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.10786667503234836 0.0039270142892380766 -0.99415761274369252 0
		 0.99241152269094113 0.058947317953024769 0.1079100705976626 0 0.059026689283012121 -0.9982533707756347 0.0024612369592555642 0
		 -5.0921800002824043 9.4811999999410244 -0.20980900003087422 1;
	setAttr ".radi" 0.05;
createNode joint -n "ring5ProxySkinRGT_jnt" -p "ring4ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.1984843707457671e-006 -0.15639699995517908 8.2287753144782982e-007 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.10786667503234836 0.0039270142892380766 -0.99415761274369252 0
		 0.99241152269094113 0.058947317953024769 0.1079100705976626 0 0.059026689283012121 -0.9982533707756347 0.0024612369592555642 0
		 -5.2473900073039577 9.4719799995239633 -0.22668700079436455 1;
	setAttr ".radi" 0.05;
createNode joint -n "pinky1ProxySkinRGT_jnt" -p "handProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.070704295689303365 0.069295435292553442 0.1903995561412562 ;
	setAttr ".r" -type "double3" -3.2600685485001048e-014 -3.0811623476677833e-015 -5.5659706925611536e-015 ;
	setAttr ".jo" -type "double3" -91.081805792040115 -78.198016788736808 90.449995528762429 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.2043924946149116 0.0074411512794778732 -0.97886073443198696 0
		 0.97735142737383851 0.054474176927549287 0.20449144592961954 0 0.054844284619400288 -0.99848745275827178 0.0038614930900134846 0
		 -4.4016800000000007 9.4937500000000217 -0.23524999999999985 1;
	setAttr ".radi" 0.05;
createNode joint -n "pinky2ProxySkinRGT_jnt" -p "pinky1ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 8.1493730763604333e-008 -0.19875107119863555 -6.8107121293792261e-006 ;
	setAttr ".r" -type "double3" 3.7579992927332865e-012 0 0 ;
	setAttr ".jo" -type "double3" 0.19963920454778783 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.2043924946149116 0.0074411512794778732 -0.97886073443198696 0
		 0.97753659140174554 0.050994762114194729 0.20450365939897847 0 0.051438512962079658 -0.99867119889608946 0.0031489489829686951 0
		 -4.5959299999999939 9.4829300000000192 -0.27589300000000055 1;
	setAttr ".radi" 0.05;
createNode joint -n "pinky3ProxySkinRGT_jnt" -p "pinky2ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 3.5023388700317071e-007 -0.24372503161430625 1.3042249307204656e-006 ;
	setAttr ".r" -type "double3" -3.9988517656038013e-012 0 0 ;
	setAttr ".jo" -type "double3" -0.56403620498338947 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.2043924946149116 0.0074411512794778732 -0.97886073443198696 0
		 0.97698285783499395 0.06082334016817844 0.20446275158902147 0 0.061059017693221862 -0.99812080960093297 0.0051619569851785218 0
		 -4.8341799979709563 9.4705000001058419 -0.32573599957551996 1;
	setAttr ".radi" 0.05;
createNode joint -n "pinky4ProxySkinRGT_jnt" -p "pinky3ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -9.3430933056914967e-007 -0.14868183434009463 -3.3391217328926359e-006 ;
	setAttr ".r" -type "double3" -3.3192566832754641e-012 0 0 ;
	setAttr ".jo" -type "double3" -0.22138759433632088 0 0 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.2043924946149116 0.0074411512794778732 -0.97886073443198696 0
		 0.97673963669901942 0.064679557884911587 0.20444127981618518 0 0.064833558024843002 -0.99787834131192976 0.0059519487893469822 0
		 -4.9794399962419993 9.4614600002134956 -0.35613499921368402 1;
	setAttr ".radi" 0.05;
createNode joint -n "pinky5ProxySkinRGT_jnt" -p "pinky4ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -5.6721732311082462e-007 -0.12894926965236753 -3.8654066258914099e-007 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.2043924946149116 0.0074411512794778732 -0.97886073443198696 0
		 0.97673963669901942 0.064679557884911587 0.20444127981618518 0 0.064833558024843002 -0.99787834131192976 0.0059519487893469822 0
		 -5.1053900000306269 9.4531199999626043 -0.38249700000668113 1;
	setAttr ".radi" 0.05;
createNode joint -n "thumb1ProxySkinRGT_jnt" -p "wristProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.012078224151652606 -0.17039269733215878 -0.15913982310287847 ;
	setAttr ".r" -type "double3" -3.1805546814635176e-015 -7.9513867036587935e-015 4.7708320221952759e-015 ;
	setAttr ".jo" -type "double3" 58.7268554465332 -35.906226777004512 -72.096355349655553 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.78101418329336247 0.21466718140314867 -0.58645958660680897 0
		 0.6209051176472834 0.36770989043695768 -0.6922905974764163 0 0.067034919064729109 -0.90482453420719533 -0.42047459129264519 0
		 -4.3137099999999986 9.4389600000000105 0.11429000000000014 1;
	setAttr ".radi" 0.05;
createNode joint -n "thumb2ProxySkinRGT_jnt" -p "thumb1ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.002782998144156501 -0.1839831326258049 -5.5250188619737628e-006 ;
	setAttr ".r" -type "double3" -7.3915593835543151e-013 -4.2838095865961576e-014 2.5006489980920954e-014 ;
	setAttr ".jo" -type "double3" -1.4593431702379287 1.711278698112648 13.976548495961231 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.60965878638895576 0.32401118380825217 -0.72341752601517761 0
		 0.78965653482850828 0.32768481264704336 -0.51871497044437098 0 0.068983484845127729 -0.88749051619025576 -0.45563347384819625 0
		 -4.4301199999999969 9.3719100000000015 0.24002999999999969 1;
	setAttr ".radi" 0.05;
createNode joint -n "thumb3ProxySkinRGT_jnt" -p "thumb2ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.0019810680647189827 -0.22932232916354789 -0.00096176508712630948 ;
	setAttr ".r" -type "double3" -1.7604563802248068e-014 -5.7911071894329324e-013 1.9167816971627172e-012 ;
	setAttr ".jo" -type "double3" -0.10014037964023377 -0.050850715758408938 0.17425363139999281 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.60719292561635174 0.32421848474209858 -0.72539584037475202 0
		 0.79138431635243756 0.32824902496366054 -0.51571643510924758 0 0.070905676167990234 -0.8872062622422946 -0.45589190969484905 0
		 -4.6124799972654893 9.2982600011347429 0.35798799820373034 1;
	setAttr ".radi" 0.05;
createNode joint -n "thumb4ProxySkinRGT_jnt" -p "thumb3ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.0013919085731037839 -0.19771809875965352 -0.001746485248023788 ;
	setAttr ".r" -type "double3" 5.585938768024456e-013 6.2609424677801291e-013 -1.8659710852291644e-012 ;
	setAttr ".jo" -type "double3" -0.66779108198420545 -0.0023166887622509433 -0.19875968567305236 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" -0.60993171998234263 0.32304196331619744 -0.72362088616636622 0
		 0.78839290682361618 0.33968983693868054 -0.51288150595510951 0 0.080124412194969671 -0.88332027295731685 -0.461871598989396 0
		 -4.7699199924390232 9.2353600031366501 0.45974099505850097 1;
	setAttr ".radi" 0.05;
createNode joint -n "palmProxySkinRGT_jnt" -p "wristProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0.072015400160983845 -0.14503655428835893 0.15726515057321375 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "yxz";
	setAttr ".jo" -type "double3" -91.081810352294724 -78.198076314123995 90.449999992615446 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.2043924938349487 0.0074411432899828614 -0.97886073465558343 0
		 0.97735142753677817 0.054474178596623256 0.20449144470623917 0 0.054844284622484717 -0.99848745272675365 0.003861501196003776 0
		 -4.291030000000001 9.4999600000000051 -0.20211500000000093 1;
	setAttr -k on -cb off ".radi" 0.05;
createNode joint -n "forearmRbnDtl1ProxySkinRGT_jnt" -p "forearmProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.8402774887581472e-007 -0.17313189996623546 -0.00155781000000009 ;
	setAttr ".r" -type "double3" -2.484808344893373e-016 4.9696166897867517e-017 2.544443745170814e-014 ;
	setAttr ".jo" -type "double3" 180 -0.51552330722148554 90.000093995335632 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99986191276516345 0.013971440758901046 -0.0089974576802972859 0
		 -0.013972006316956239 0.99990238675556631 -1.0319443260985619e-016 0 0.0089965794092614609 0.00012571253554576628 0.99995952205841332 0
		 -2.5849149999999983 9.4562009999999912 -0.058870290000000172 1;
	setAttr ".radi" 0.1;
createNode joint -n "forearmRbnDtl2ProxySkinRGT_jnt" -p "forearmProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 8.5208323419294629e-007 -0.51939569989870948 -0.0046734300000002282 ;
	setAttr ".r" -type "double3" -2.484808344893373e-016 4.9696166897867517e-017 2.544443745170814e-014 ;
	setAttr ".jo" -type "double3" 180 -0.51552330722148554 90.000093995335632 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99986191276516345 0.013971440758901046 -0.0089974576802972859 0
		 -0.013972006316956239 0.99990238675556631 -1.0319443260985619e-016 0 0.0089965794092614609 0.00012571253554576628 0.99995952205841332 0
		 -2.9311449999999968 9.45136299999999 -0.055754670000000076 1;
	setAttr ".radi" 0.1;
createNode joint -n "forearmRbnDtl3ProxySkinRGT_jnt" -p "forearmProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.4201387283918621e-006 -0.8656594998311844 -0.0077890500000003179 ;
	setAttr ".r" -type "double3" -2.484808344893373e-016 4.9696166897867517e-017 2.544443745170814e-014 ;
	setAttr ".jo" -type "double3" 180 -0.51552330722148554 90.000093995335632 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99986191276516345 0.013971440758901046 -0.0089974576802972859 0
		 -0.013972006316956239 0.99990238675556631 -1.0319443260985619e-016 0 0.0089965794092614609 0.00012571253554576628 0.99995952205841332 0
		 -3.2773749999999966 9.4465249999999994 -0.052639050000000034 1;
	setAttr ".radi" 0.1;
createNode joint -n "forearmRbnDtl4ProxySkinRGT_jnt" -p "forearmProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1.9881942172617073e-006 -1.2119232997636575 -0.010904670000000415 ;
	setAttr ".r" -type "double3" -2.484808344893373e-016 4.9696166897867517e-017 2.544443745170814e-014 ;
	setAttr ".jo" -type "double3" 180 -0.51552330722148554 90.000093995335632 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99986191276516345 0.013971440758901046 -0.0089974576802972859 0
		 -0.013972006316956239 0.99990238675556631 -1.0319443260985619e-016 0 0.0089965794092614609 0.00012571253554576628 0.99995952205841332 0
		 -3.6236049999999942 9.4416870000000017 -0.049523429999999979 1;
	setAttr ".radi" 0.1;
createNode joint -n "forearmRbnDtl5ProxySkinRGT_jnt" -p "forearmProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 2.5562497043551957e-006 -1.558187099696132 -0.014020290000000539 ;
	setAttr ".r" -type "double3" -2.484808344893373e-016 4.9696166897867517e-017 2.544443745170814e-014 ;
	setAttr ".jo" -type "double3" 180 -0.51552330722148554 90.000093995335632 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99986191276516345 0.013971440758901046 -0.0089974576802972859 0
		 -0.013972006316956239 0.99990238675556631 -1.0319443260985619e-016 0 0.0089965794092614609 0.00012571253554576628 0.99995952205841332 0
		 -3.9698349999999936 9.4368490000000023 -0.046407809999999897 1;
	setAttr ".radi" 0.1;
createNode joint -n "upArmRbnDtl1ProxySkinRGT_jnt" -p "upArmProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.5516519919221992e-007 -0.16423251629910229 0.0057603210000000994 ;
	setAttr ".r" -type "double3" 7.9513867036587919e-016 1.987846675914697e-016 1.9083328088781101e-014 ;
	setAttr ".jo" -type "double3" -180 2.0087790781497241 89.999910980546147 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99930321865320759 0.012821487657965038 0.035052626771416753 0
		 -0.012829371719307914 0.99991770022401727 1.4067446692074428e-016 0 -0.035049741948085869 -0.00044970317858881564 0.99938546785333215 0
		 -0.93382900000000013 9.4775829999999974 -0.0085852110000004776 1;
	setAttr ".radi" 0.1;
createNode joint -n "upArmRbnDtl2ProxySkinRGT_jnt" -p "upArmProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -7.6549559580030291e-007 -0.4926975488973081 0.017280963000000097 ;
	setAttr ".r" -type "double3" 7.9513867036587919e-016 1.987846675914697e-016 1.9083328088781101e-014 ;
	setAttr ".jo" -type "double3" -180 2.0087790781497241 89.999910980546147 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99930321865320759 0.012821487657965038 0.035052626771416753 0
		 -0.012829371719307914 0.99991770022401727 1.4067446692074428e-016 0 -0.035049741948085869 -0.00044970317858881564 0.99938546785333215 0
		 -1.2622670000000014 9.4733689999999946 -0.02010585300000051 1;
	setAttr ".radi" 0.1;
createNode joint -n "upArmRbnDtl3ProxySkinRGT_jnt" -p "upArmProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.2758259959610996e-006 -0.82116258149551224 0.028801604999999821 ;
	setAttr ".r" -type "double3" 7.9513867036587919e-016 1.987846675914697e-016 1.9083328088781101e-014 ;
	setAttr ".jo" -type "double3" -180 2.0087790781497241 89.999910980546147 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99930321865320759 0.012821487657965038 0.035052626771416753 0
		 -0.012829371719307914 0.99991770022401727 1.4067446692074428e-016 0 -0.035049741948085869 -0.00044970317858881564 0.99938546785333215 0
		 -1.5907050000000009 9.4691549999999864 -0.031626495000000275 1;
	setAttr ".radi" 0.1;
createNode joint -n "upArmRbnDtl4ProxySkinRGT_jnt" -p "upArmProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -1.7861563907928257e-006 -1.1496276140937165 0.040322246999999582 ;
	setAttr ".r" -type "double3" 7.9513867036587919e-016 1.987846675914697e-016 1.9083328088781101e-014 ;
	setAttr ".jo" -type "double3" -180 2.0087790781497241 89.999910980546147 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99930321865320759 0.012821487657965038 0.035052626771416753 0
		 -0.012829371719307914 0.99991770022401727 1.4067446692074428e-016 0 -0.035049741948085869 -0.00044970317858881564 0.99938546785333215 0
		 -1.9191430000000005 9.4649409999999854 -0.043147137000000071 1;
	setAttr ".radi" 0.1;
createNode joint -n "upArmRbnDtl5ProxySkinRGT_jnt" -p "upArmProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" -2.2964867802954814e-006 -1.4780926466919215 0.051842888999999587 ;
	setAttr ".r" -type "double3" 7.9513867036587919e-016 1.987846675914697e-016 1.9083328088781101e-014 ;
	setAttr ".jo" -type "double3" -180 2.0087790781497241 89.999910980546147 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 0.99930321865320759 0.012821487657965038 0.035052626771416753 0
		 -0.012829371719307914 0.99991770022401727 1.4067446692074428e-016 0 -0.035049741948085869 -0.00044970317858881564 0.99938546785333215 0
		 -2.2475810000000012 9.4607269999999897 -0.054667779000000118 1;
	setAttr ".radi" 0.1;
createNode joint -n "clav1ScaProxySkinRGT_jnt" -p "clav1ProxySkinRGT_jnt";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 0 0 -1.1102230246251565e-016 ;
	setAttr ".ssc" no;
	setAttr ".bps" -type "matrix" 7.7715611723760958e-016 0.99999999999999989 2.0816681711721682e-017 0
		 1 -6.6613381477509383e-016 1.2246467991473527e-016 0 1.2246467991473532e-016 2.0816681711721583e-017 -0.99999999999999989 0
		 -0.13883799999999999 9.6135000000000002 0.4180799999999999 1;
	setAttr ".radi" 0.1;
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -2.0131754768021493 11.402180749061269 10.303632560356847 ;
	setAttr ".r" -type "double3" -26.738352729602354 -11.399999999999991 -4.0557077008446981e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 11.404685731347072;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 1.5964318773598052e-006 6.2710198525102445 0.31938684241192278 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 100 -ast 1 -aet 100 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
	setAttr -k on ".etw";
	setAttr -k on ".tps";
	setAttr -k on ".tms";
lockNode -l 1 ;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
lockNode -l 1 ;
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
lockNode -l 1 ;
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
lockNode -l 1 ;
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
lockNode -l 1 ;
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -k on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs";
	setAttr -k on ".ef";
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep" 1;
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -k on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".rv";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mot";
	setAttr -av -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
lockNode -l 1 ;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w" 1920;
	setAttr -av -k on ".h" 1080;
	setAttr -av -k on ".pa" 0.99956250190734863;
	setAttr -av -k on ".al" yes;
	setAttr -av -k on ".dar" 1;
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
lockNode -l 1 ;
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
lockNode -l 1 ;
select -ne :hardwareRenderingGlobals;
	setAttr ".vac" 2;
select -ne :defaultHardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".rp";
	setAttr -k on ".cai";
	setAttr -k on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -k on ".ei";
	setAttr -av -k on ".ex";
	setAttr -av -k on ".es";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bf";
	setAttr -k on ".fii";
	setAttr -av -k on ".sf";
	setAttr -k on ".gr";
	setAttr -k on ".li";
	setAttr -k on ".ls";
	setAttr -av -k on ".mb";
	setAttr -k on ".ti";
	setAttr -k on ".txt";
	setAttr -k on ".mpr";
	setAttr -k on ".wzd";
	setAttr -k on ".fn";
	setAttr -k on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -k on ".as";
	setAttr -k on ".ds";
	setAttr -k on ".lm";
	setAttr -av -k on ".fir";
	setAttr -k on ".aap";
	setAttr -av -k on ".gh";
	setAttr -cb on ".sd";
lockNode -l 1 ;
connectAttr "rootProxySkin_jnt.s" "spine1ScaProxySkin_jnt.is";
connectAttr "upLegProxySkinLFT_jnt.s" "lowLegProxySkinLFT_jnt.is";
connectAttr "lowLegProxySkinLFT_jnt.s" "ankleProxySkinLFT_jnt.is";
connectAttr "ankleProxySkinLFT_jnt.s" "ballProxySkinLFT_jnt.is";
connectAttr "ballProxySkinLFT_jnt.s" "toeProxySkinLFT_jnt.is";
connectAttr "lowLegProxySkinLFT_jnt.s" "lowLegRbnDtl1ProxySkinLFT_jnt.is";
connectAttr "lowLegProxySkinLFT_jnt.s" "lowLegRbnDtl2ProxySkinLFT_jnt.is";
connectAttr "lowLegProxySkinLFT_jnt.s" "lowLegRbnDtl3ProxySkinLFT_jnt.is";
connectAttr "lowLegProxySkinLFT_jnt.s" "lowLegRbnDtl4ProxySkinLFT_jnt.is";
connectAttr "lowLegProxySkinLFT_jnt.s" "lowLegRbnDtl5ProxySkinLFT_jnt.is";
connectAttr "upLegProxySkinLFT_jnt.s" "upLegRbnDtl1ProxySkinLFT_jnt.is";
connectAttr "upLegProxySkinLFT_jnt.s" "upLegRbnDtl2ProxySkinLFT_jnt.is";
connectAttr "upLegProxySkinLFT_jnt.s" "upLegRbnDtl3ProxySkinLFT_jnt.is";
connectAttr "upLegProxySkinLFT_jnt.s" "upLegRbnDtl4ProxySkinLFT_jnt.is";
connectAttr "upLegProxySkinLFT_jnt.s" "upLegRbnDtl5ProxySkinLFT_jnt.is";
connectAttr "upLegProxySkinRGT_jnt.s" "lowLegProxySkinRGT_jnt.is";
connectAttr "lowLegProxySkinRGT_jnt.s" "ankleProxySkinRGT_jnt.is";
connectAttr "ankleProxySkinRGT_jnt.s" "ballProxySkinRGT_jnt.is";
connectAttr "ballProxySkinRGT_jnt.s" "toeProxySkinRGT_jnt.is";
connectAttr "lowLegProxySkinRGT_jnt.s" "lowLegRbnDtl1ProxySkinRGT_jnt.is";
connectAttr "lowLegProxySkinRGT_jnt.s" "lowLegRbnDtl2ProxySkinRGT_jnt.is";
connectAttr "lowLegProxySkinRGT_jnt.s" "lowLegRbnDtl3ProxySkinRGT_jnt.is";
connectAttr "lowLegProxySkinRGT_jnt.s" "lowLegRbnDtl4ProxySkinRGT_jnt.is";
connectAttr "lowLegProxySkinRGT_jnt.s" "lowLegRbnDtl5ProxySkinRGT_jnt.is";
connectAttr "upLegProxySkinRGT_jnt.s" "upLegRbnDtl1ProxySkinRGT_jnt.is";
connectAttr "upLegProxySkinRGT_jnt.s" "upLegRbnDtl2ProxySkinRGT_jnt.is";
connectAttr "upLegProxySkinRGT_jnt.s" "upLegRbnDtl3ProxySkinRGT_jnt.is";
connectAttr "upLegProxySkinRGT_jnt.s" "upLegRbnDtl4ProxySkinRGT_jnt.is";
connectAttr "upLegProxySkinRGT_jnt.s" "upLegRbnDtl5ProxySkinRGT_jnt.is";
connectAttr "spine2ScaProxySkin_jnt.s" "spine3ScaProxySkin_jnt.is";
connectAttr "spine3ScaProxySkin_jnt.s" "spine4ScaProxySkin_jnt.is";
connectAttr "spine4ScaProxySkin_jnt.s" "spine5ScaProxySkin_jnt.is";
connectAttr "neck1ProxySkin_jnt.s" "neck2ProxySkin_jnt.is";
connectAttr "neck1ProxySkin_jnt.s" "head1ProxySkin_jnt.is";
connectAttr "jaw1LWRProxySkin_jnt.s" "jaw2LWRProxySkin_jnt.is";
connectAttr "jaw2LWRProxySkin_jnt.s" "jaw3LWRProxySkin_jnt.is";
connectAttr "jaw2LWRProxySkin_jnt.s" "lowerTeethProxySkin_jnt.is";
connectAttr "jaw1LWRProxySkin_jnt.s" "tongue1ProxySkin_jnt.is";
connectAttr "tongue1ProxySkin_jnt.s" "tongue2ProxySkin_jnt.is";
connectAttr "tongue2ProxySkin_jnt.s" "tongue3ProxySkin_jnt.is";
connectAttr "tongue3ProxySkin_jnt.s" "tongue4ProxySkin_jnt.is";
connectAttr "tongue4ProxySkin_jnt.s" "tongue5ProxySkin_jnt.is";
connectAttr "tongue5ProxySkin_jnt.s" "tongue6ProxySkin_jnt.is";
connectAttr "jaw1UPRProxySkin_jnt.s" "jaw2UPRProxySkin_jnt.is";
connectAttr "jaw1UPRProxySkin_jnt.s" "upperTeethProxySkin_jnt.is";
connectAttr "eyeProxySkinLFT_jnt.s" "lidProxySkinLFT_jnt.is";
connectAttr "eyeProxySkinLFT_jnt.s" "irisProxySkinLFT_jnt.is";
connectAttr "eyeProxySkinLFT_jnt.s" "eyeSpecProxySkinLFT_jnt.is";
connectAttr "eyeProxySkinRGT_jnt.s" "lidProxySkinRGT_jnt.is";
connectAttr "eyeProxySkinRGT_jnt.s" "irisProxySkinRGT_jnt.is";
connectAttr "eyeProxySkinRGT_jnt.s" "eyeSpecProxySkinRGT_jnt.is";
connectAttr "neck1ProxySkin_jnt.s" "neckRbnProxySkin_jnt.is";
connectAttr "clav1ProxySkinLFT_jnt.s" "clav2ProxySkinLFT_jnt.is";
connectAttr "clav2ProxySkinLFT_jnt.s" "upArmProxySkinLFT_jnt.is";
connectAttr "upArmProxySkinLFT_jnt.s" "forearmProxySkinLFT_jnt.is";
connectAttr "forearmProxySkinLFT_jnt.s" "wristProxySkinLFT_jnt.is";
connectAttr "wristProxySkinLFT_jnt.s" "handProxySkinLFT_jnt.is";
connectAttr "handProxySkinLFT_jnt.s" "index1ProxySkinLFT_jnt.is";
connectAttr "index1ProxySkinLFT_jnt.s" "index2ProxySkinLFT_jnt.is";
connectAttr "index2ProxySkinLFT_jnt.s" "index3ProxySkinLFT_jnt.is";
connectAttr "index3ProxySkinLFT_jnt.s" "index4ProxySkinLFT_jnt.is";
connectAttr "index4ProxySkinLFT_jnt.s" "index5ProxySkinLFT_jnt.is";
connectAttr "handProxySkinLFT_jnt.s" "middle1ProxySkinLFT_jnt.is";
connectAttr "middle1ProxySkinLFT_jnt.s" "middle2ProxySkinLFT_jnt.is";
connectAttr "middle2ProxySkinLFT_jnt.s" "middle3ProxySkinLFT_jnt.is";
connectAttr "middle3ProxySkinLFT_jnt.s" "middle4ProxySkinLFT_jnt.is";
connectAttr "middle4ProxySkinLFT_jnt.s" "middle5ProxySkinLFT_jnt.is";
connectAttr "handProxySkinLFT_jnt.s" "ring1ProxySkinLFT_jnt.is";
connectAttr "ring1ProxySkinLFT_jnt.s" "ring2ProxySkinLFT_jnt.is";
connectAttr "ring2ProxySkinLFT_jnt.s" "ring3ProxySkinLFT_jnt.is";
connectAttr "ring3ProxySkinLFT_jnt.s" "ring4ProxySkinLFT_jnt.is";
connectAttr "ring4ProxySkinLFT_jnt.s" "ring5ProxySkinLFT_jnt.is";
connectAttr "handProxySkinLFT_jnt.s" "pinky1ProxySkinLFT_jnt.is";
connectAttr "pinky1ProxySkinLFT_jnt.s" "pinky2ProxySkinLFT_jnt.is";
connectAttr "pinky2ProxySkinLFT_jnt.s" "pinky3ProxySkinLFT_jnt.is";
connectAttr "pinky3ProxySkinLFT_jnt.s" "pinky4ProxySkinLFT_jnt.is";
connectAttr "pinky4ProxySkinLFT_jnt.s" "pinky5ProxySkinLFT_jnt.is";
connectAttr "wristProxySkinLFT_jnt.s" "thumb1ProxySkinLFT_jnt.is";
connectAttr "thumb1ProxySkinLFT_jnt.s" "thumb2ProxySkinLFT_jnt.is";
connectAttr "thumb2ProxySkinLFT_jnt.s" "thumb3ProxySkinLFT_jnt.is";
connectAttr "thumb3ProxySkinLFT_jnt.s" "thumb4ProxySkinLFT_jnt.is";
connectAttr "wristProxySkinLFT_jnt.s" "palmProxySkinLFT_jnt.is";
connectAttr "forearmProxySkinLFT_jnt.s" "forearmRbnDtl1ProxySkinLFT_jnt.is";
connectAttr "forearmProxySkinLFT_jnt.s" "forearmRbnDtl2ProxySkinLFT_jnt.is";
connectAttr "forearmProxySkinLFT_jnt.s" "forearmRbnDtl3ProxySkinLFT_jnt.is";
connectAttr "forearmProxySkinLFT_jnt.s" "forearmRbnDtl4ProxySkinLFT_jnt.is";
connectAttr "forearmProxySkinLFT_jnt.s" "forearmRbnDtl5ProxySkinLFT_jnt.is";
connectAttr "upArmProxySkinLFT_jnt.s" "upArmRbnDtl1ProxySkinLFT_jnt.is";
connectAttr "upArmProxySkinLFT_jnt.s" "upArmRbnDtl2ProxySkinLFT_jnt.is";
connectAttr "upArmProxySkinLFT_jnt.s" "upArmRbnDtl3ProxySkinLFT_jnt.is";
connectAttr "upArmProxySkinLFT_jnt.s" "upArmRbnDtl4ProxySkinLFT_jnt.is";
connectAttr "upArmProxySkinLFT_jnt.s" "upArmRbnDtl5ProxySkinLFT_jnt.is";
connectAttr "clav1ProxySkinLFT_jnt.s" "clav1ScaProxySkinLFT_jnt.is";
connectAttr "clav1ProxySkinRGT_jnt.s" "clav2ProxySkinRGT_jnt.is";
connectAttr "clav2ProxySkinRGT_jnt.s" "upArmProxySkinRGT_jnt.is";
connectAttr "upArmProxySkinRGT_jnt.s" "forearmProxySkinRGT_jnt.is";
connectAttr "forearmProxySkinRGT_jnt.s" "wristProxySkinRGT_jnt.is";
connectAttr "wristProxySkinRGT_jnt.s" "handProxySkinRGT_jnt.is";
connectAttr "handProxySkinRGT_jnt.s" "index1ProxySkinRGT_jnt.is";
connectAttr "index1ProxySkinRGT_jnt.s" "index2ProxySkinRGT_jnt.is";
connectAttr "index2ProxySkinRGT_jnt.s" "index3ProxySkinRGT_jnt.is";
connectAttr "index3ProxySkinRGT_jnt.s" "index4ProxySkinRGT_jnt.is";
connectAttr "index4ProxySkinRGT_jnt.s" "index5ProxySkinRGT_jnt.is";
connectAttr "handProxySkinRGT_jnt.s" "middle1ProxySkinRGT_jnt.is";
connectAttr "middle1ProxySkinRGT_jnt.s" "middle2ProxySkinRGT_jnt.is";
connectAttr "middle2ProxySkinRGT_jnt.s" "middle3ProxySkinRGT_jnt.is";
connectAttr "middle3ProxySkinRGT_jnt.s" "middle4ProxySkinRGT_jnt.is";
connectAttr "middle4ProxySkinRGT_jnt.s" "middle5ProxySkinRGT_jnt.is";
connectAttr "handProxySkinRGT_jnt.s" "ring1ProxySkinRGT_jnt.is";
connectAttr "ring1ProxySkinRGT_jnt.s" "ring2ProxySkinRGT_jnt.is";
connectAttr "ring2ProxySkinRGT_jnt.s" "ring3ProxySkinRGT_jnt.is";
connectAttr "ring3ProxySkinRGT_jnt.s" "ring4ProxySkinRGT_jnt.is";
connectAttr "ring4ProxySkinRGT_jnt.s" "ring5ProxySkinRGT_jnt.is";
connectAttr "handProxySkinRGT_jnt.s" "pinky1ProxySkinRGT_jnt.is";
connectAttr "pinky1ProxySkinRGT_jnt.s" "pinky2ProxySkinRGT_jnt.is";
connectAttr "pinky2ProxySkinRGT_jnt.s" "pinky3ProxySkinRGT_jnt.is";
connectAttr "pinky3ProxySkinRGT_jnt.s" "pinky4ProxySkinRGT_jnt.is";
connectAttr "pinky4ProxySkinRGT_jnt.s" "pinky5ProxySkinRGT_jnt.is";
connectAttr "wristProxySkinRGT_jnt.s" "thumb1ProxySkinRGT_jnt.is";
connectAttr "thumb1ProxySkinRGT_jnt.s" "thumb2ProxySkinRGT_jnt.is";
connectAttr "thumb2ProxySkinRGT_jnt.s" "thumb3ProxySkinRGT_jnt.is";
connectAttr "thumb3ProxySkinRGT_jnt.s" "thumb4ProxySkinRGT_jnt.is";
connectAttr "wristProxySkinRGT_jnt.s" "palmProxySkinRGT_jnt.is";
connectAttr "forearmProxySkinRGT_jnt.s" "forearmRbnDtl1ProxySkinRGT_jnt.is";
connectAttr "forearmProxySkinRGT_jnt.s" "forearmRbnDtl2ProxySkinRGT_jnt.is";
connectAttr "forearmProxySkinRGT_jnt.s" "forearmRbnDtl3ProxySkinRGT_jnt.is";
connectAttr "forearmProxySkinRGT_jnt.s" "forearmRbnDtl4ProxySkinRGT_jnt.is";
connectAttr "forearmProxySkinRGT_jnt.s" "forearmRbnDtl5ProxySkinRGT_jnt.is";
connectAttr "upArmProxySkinRGT_jnt.s" "upArmRbnDtl1ProxySkinRGT_jnt.is";
connectAttr "upArmProxySkinRGT_jnt.s" "upArmRbnDtl2ProxySkinRGT_jnt.is";
connectAttr "upArmProxySkinRGT_jnt.s" "upArmRbnDtl3ProxySkinRGT_jnt.is";
connectAttr "upArmProxySkinRGT_jnt.s" "upArmRbnDtl4ProxySkinRGT_jnt.is";
connectAttr "upArmProxySkinRGT_jnt.s" "upArmRbnDtl5ProxySkinRGT_jnt.is";
connectAttr "clav1ProxySkinRGT_jnt.s" "clav1ScaProxySkinRGT_jnt.is";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of TEMPLATE_proxySkinJoint_frd.ma
