import maya.cmds as mc
import pkmel.core as pc
reload( pc )
import pkmel.rigTools as rigTools
reload( rigTools )
import pkmel.mainGroup as pmain
reload( pmain )
import pkmel.rootRig as proot
reload( proot )
import pkmel.pelvisRig as ppelv
reload( ppelv )
import pkmel.spineRig as pspi
reload( pspi )
import pkmel.neckRig2 as pneck
reload( pneck )
import pkmel.headRig as phead
reload( phead )
import pkmel.clavicleRig as pclav
reload( pclav )
import pkmel.armRig2 as parm
reload( parm )
import pkmel.legRig2 as pleg
reload( pleg )
import pkmel.fingerRig as pfngr
reload( pfngr )
import pkmel.thumbRig as pthmb
reload( pthmb )
import pkmel.ribbon as prbn
reload( prbn )
import pkmel.fkRig as pfk
reload( pfk )
import pkmel.backLegRig2 as pbleg
reload( pbleg )
import pkmel.fkGroupRig as pfkg
reload( pfkg )
import pkmel.tailRig as ptail
reload( ptail )

def main() :

	# Naming
	charName = ''
	elem = ''

	# Rig
	mainGroup = pmain.MainGroup()
	rigTools.nodeNaming(
							mainGroup ,
							charName=charName ,
							elem=elem ,
							side=''
						)

	anim = 'anim_grp'
	jnt = 'jnt_grp'
	skin = 'skin_grp'
	ikh = 'ikh_grp'
	still = 'still_grp'
	size = 0.25
	
	# Arrange hierarchy for Friends!!!!
	mc.parent( mainGroup.placement_ctrl , w=True )
	mc.parent( mainGroup.still_grp , w=True )
	mainGroup.offset_ctrl.name = 'placementCtrlOfst_grp'
	mc.delete( mainGroup.offset_ctrl.shape )
	mc.select( mainGroup.placement_ctrl )
	placementGimbal = rigTools.doAddGimbal()
	mainGroup.offset_ctrl.parent( placementGimbal )
	mc.delete( mainGroup.rig_grp )

	# Root
	print 'Rigging Root'
	rootRig = proot.RootRig(
								animGrp=anim ,
								skinGrp=skin ,
								charSize=size ,
								tmpJnt='root_tmpJnt'
							)

	rigTools.nodeNaming(
							rootRig ,
							charName=charName ,
							elem=elem ,
							side=''
						)

	# Pelvis
	print 'Rigging pelvis'
	pelvisRig = ppelv.PelvisRig(
									parent=rootRig.root_jnt ,
									animGrp=anim ,
									skinGrp=skin ,
									charSize=size ,
									rotateOrder='zxy',
									tmpJnt='pelvis_tmpJnt'
								)

	rigTools.nodeNaming(
							pelvisRig ,
							charName=charName ,
							elem=elem ,
							side=''
						)

	# Spine
	print 'Rigging Spine'
	spineRig = pspi.SpineRig(
								parent=rootRig.root_jnt ,
								animGrp=anim ,
								jntGrp=jnt ,
								skinGrp=skin ,
								stillGrp=still ,
								ribbon=False ,
								ax='z' ,
								rotateOrder='zxy',
								charSize=size ,
								tmpJnt=(
											'spine1_tmpJnt' ,
											'spine2_tmpJnt' ,
											'spine3_tmpJnt' ,
											'chestIkPiv_jnt' ,
										)
							)

	rigTools.nodeNaming(
							spineRig ,
							charName=charName ,
							elem=elem ,
							side=''
						)

	rigTools.dummyNaming(
							obj=spineRig.lowSpineRbn ,
							attr='rbn' ,
							dummy='lowSpineRbn' ,
							charName=charName ,
							elem=elem ,
							side=''
						)

	rigTools.dummyNaming(
							obj=spineRig.upSpineRbn ,
							attr='rbn' ,
							dummy='upSpineRbn' ,
							charName=charName ,
							elem=elem ,
							side=''
						)

	# Spine fix
	spinePos = pc.Null()
	spinePos.name = 'spinePos_grp'
	spinePos.snap( spineRig.spineRig_grp )
	mc.pointConstraint( pelvisRig.pelvis_jnt , spinePos )
	spinePos.parent( spineRig.spineRig_grp )

	spineElems = [
					spineRig.spineCtrl_grp ,
					spineRig.spineFkCtrl_grp ,
					spineRig.spineIkCtrl_grp ,
					spineRig.spineRbnAnim_grp
					]
	for spineElem in spineElems :
		
		attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' )
		for attr in attrs :
			spineElem.attr( attr ).lock = 0
		
		# spineElem.parent( spinePos )
		mc.parentConstraint( spinePos , spineElem , mo=True )

	attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' )
	for attr in attrs :
		spineRig.spineIkCtrlLoc_grp.attr( attr ).lock = 0
	mc.parentConstraint( rootRig.root_jnt , spineRig.spineIkCtrlLoc_grp , mo=True )

	# Neck
	print 'Rigging Neck'
	neckRig = pneck.NeckRig(
								parent=spineRig.spine3_jnt ,
								animGrp=anim ,
								jntGrp=jnt ,
								skinGrp=skin ,
								stillGrp=still ,
								ribbon=False ,
								ax='y' ,
								rotateOrder='zxy',
								charSize=size ,
								tmpJnt=( 'neck1_tmpJnt' , 'head1_tmpJnt' )
							)

	rigTools.nodeNaming(
							neckRig ,
							charName=charName ,
							elem=elem ,
							side=''
						)

	rigTools.dummyNaming(
							obj=neckRig.neckRbn ,
							attr='rbn' ,
							dummy='neckRbn' ,
							charName=charName ,
							elem=elem ,
							side=''
						)
	mc.setAttr( 'neckRbn_ctrlShape.upVector' , 1 , 0 , 0 )

	# Head
	print 'Rigging Head'
	headRig = phead.HeadRig(
								parent = neckRig.neck2_jnt ,
								animGrp = anim ,
								skinGrp = skin ,
								charSize = size ,
								rotateOrder='zxy',
								tmpJnt = (
											'head1_tmpJnt' ,
											'head2_tmpJnt' ,
											'eyeLFT_tmpJnt' ,
											'eyeRGT_tmpJnt' ,
											'lowerJaw1_tmpJnt' ,
											'lowerJaw2_tmpJnt' ,
											'lowerJaw3_tmpJnt' ,
											'upperJaw1_tmpJnt' ,
											'upperJaw2_tmpJnt' ,
											'eyeTrgt_tmpJnt' ,
											'eyeTrgtLFT_tmpJnt' ,
											'eyeTrgtRGT_tmpJnt'
										)
							)

	rigTools.nodeNaming(
							headRig ,
							charName = '' ,
							elem = '' ,
							side = ''
						)

	# attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' )
	# for attr in attrs :
	# 	neckRig.neckRbn.rbnRootTwst_grp.attr( attr ).l = 0

	# pc.parentConstraint( headRig.head1_jnt , neckRig.neckRbn.rbnRootTwst_grp )
	# pc.parentConstraint( headRig.head1_jnt , neckRig.neckRbn.rbnEndCtrlZro_grp )
	
	attrs = ( 'tx' , 'ty' , 'tz' , 'rx' , 'ry' , 'rz' )
	for attr in attrs :
		neckRig.neckRbn.rbnEnd_ctrl.attr( attr ).l = 0
		neckRig.neckRbnEndNonRoll_jnt.attr( attr ).l = 0

	pc.pointConstraint( headRig.head1_jnt , neckRig.neckRbn.rbnEnd_ctrl )
	pc.parentConstraint( headRig.head1_jnt , neckRig.neckRbnEndNonRoll_jnt )
	neckRig.neckRbn.rbnEnd_ctrl.hide()

	# Left iris
	lIrisRig = phead.IrisRig(
								parent=headRig.eyeLFT_jnt ,
								eyeGmbl=headRig.eyeGmblLFT_ctrl ,
								charSize=size ,
								tmpJnt='irisLFT_tmpJnt'
							)

	rigTools.nodeNaming(
							lIrisRig ,
							charName = '' ,
							elem = '' ,
							side = 'LFT'
						)

	# Right iris
	rIrisRig = phead.IrisRig(
								parent=headRig.eyeRGT_jnt ,
								eyeGmbl=headRig.eyeGmblRGT_ctrl ,
								charSize=size ,
								tmpJnt='irisRGT_tmpJnt'
							)

	rigTools.nodeNaming(
							rIrisRig ,
							charName = '' ,
							elem = '' ,
							side = 'RGT'
						)

	# Left Eye Spec
	lEyeSpecRig = phead.EyeSpecRig(
										parent=headRig.eyeLFT_jnt ,
										worldSpace = headRig.head1_jnt ,
										eyeGmbl=headRig.eyeGmblLFT_ctrl ,
										charSize=size ,
										tmpJnt='eyeSpecLFT_tmpJnt'
									)

	rigTools.nodeNaming(
							lEyeSpecRig ,
							charName = '' ,
							elem = '' ,
							side = 'LFT'
						)

	# Right Eye Spec
	REyeSpecRig = phead.EyeSpecRig(
										parent=headRig.eyeRGT_jnt ,
										worldSpace = headRig.head1_jnt ,
										eyeGmbl=headRig.eyeGmblRGT_ctrl ,
										charSize=size ,
										tmpJnt='eyeSpecRGT_tmpJnt'
									)

	rigTools.nodeNaming(
							REyeSpecRig ,
							charName = '' ,
							elem = '' ,
							side = 'RGT'
						)

	# Left Scap
	print 'Rigging Left Scap'
	lftScapRig = pclav.ClavicleRig(
										parent=spineRig.spine2_jnt ,
										side='LFT' ,
										animGrp=anim ,
										skinGrp=skin ,
										charSize=size ,
										tmpJnt = (
													'scapLFT_tmpJnt' ,
													'upLegFrontLFT_tmpJnt'
												)
									)

	rigTools.nodeNaming(
							lftScapRig ,
							charName = charName ,
							elem = 'Scap' ,
							side = 'LFT'
						)

	# Right Scap
	print 'Rigging Right Scap'
	rgtScapRig = pclav.ClavicleRig(
										parent=spineRig.spine2_jnt ,
										side='RGT' ,
										animGrp=anim ,
										skinGrp=skin ,
										charSize=size ,
										tmpJnt = (
													'scapRGT_tmpJnt' ,
													'upLegFrontRGT_tmpJnt'
												)
									)

	rigTools.nodeNaming(
							rgtScapRig ,
							charName = charName ,
							elem = 'Scap' ,
							side = 'RGT'
						)

	print lftScapRig

	# Left Front Leg
	print 'Rigging Left Front Leg'
	lftFrLeg = pbleg.BackLegRig(
									parent=lftScapRig.clav2_jnt ,
									side='LFT' ,
									animGrp=anim ,
									jntGrp=jnt ,
									skinGrp=skin ,
									stillGrp=still ,
									ribbon=True ,
									charSize=size ,
									tmpJnt=(
												'upLegFrontLFT_tmpJnt' ,
												'midLegFrontLFT_tmpJnt' ,
												'lowLegFrontLFT_tmpJnt' ,
												'ankleFrontLFT_tmpJnt' ,
												'ballFrontLFT_tmpJnt' ,
												'toeFrontLFT_tmpJnt' ,
												'heelFrontLFT_tmpJnt' ,
												'footInFrontLFT_tmpJnt' ,
												'footOutFrontLFT_tmpJnt' ,
												'kneeIkFrontPivLFT_tmpJnt'
											)
								)

	rigTools.nodeNaming(
							lftFrLeg ,
							charName=charName ,
							elem='Front' ,
							side='LFT'
						)
	rigTools.dummyNaming(
							obj=lftFrLeg.upLegRbn ,
							attr='rbn' ,
							dummy='upLegRbn' ,
							charName=charName ,
							elem='Front' ,
							side='LFT'
						)
	rigTools.dummyNaming(
							obj=lftFrLeg.midLegRbn ,
							attr='rbn' ,
							dummy='midLegRbn' ,
							charName=charName ,
							elem='Front' ,
							side='LFT'
						)
	rigTools.dummyNaming(
							obj=lftFrLeg.lowLegRbn ,
							attr='rbn' ,
							dummy='lowLegRbn' ,
							charName=charName ,
							elem='Front' ,
							side='LFT'
						)

	legIkLFTCtrlShape = pc.Dag(lftFrLeg.legIk_ctrl.shape)
	legIkLFTCtrlShape.attr('autoFlexAmp').v = -135
	legIkLFTCtrlShape.attr('autoFlexMin').v = -45
	legIkLFTCtrlShape.attr('autoFlexMax').v = 0

	# Right Front Leg
	print 'Rigging Right Front Leg'
	rgtFrLeg = pbleg.BackLegRig(
									parent=rgtScapRig.clav2_jnt ,
									side='RGT' ,
									animGrp=anim ,
									jntGrp=jnt ,
									skinGrp=skin ,
									stillGrp=still ,
									ribbon=True ,
									charSize=size ,
									tmpJnt=(
												'upLegFrontRGT_tmpJnt' ,
												'midLegFrontRGT_tmpJnt' ,
												'lowLegFrontRGT_tmpJnt' ,
												'ankleFrontRGT_tmpJnt' ,
												'ballFrontRGT_tmpJnt' ,
												'toeFrontRGT_tmpJnt' ,
												'heelFrontRGT_tmpJnt' ,
												'footInFrontRGT_tmpJnt' ,
												'footOutFrontRGT_tmpJnt' ,
												'kneeIkFrontPivRGT_tmpJnt'
											)
								)

	rigTools.nodeNaming(
							rgtFrLeg ,
							charName=charName ,
							elem='Front' ,
							side='RGT'
						)
	rigTools.dummyNaming(
							obj=rgtFrLeg.upLegRbn ,
							attr='rbn' ,
							dummy='upLegRbn' ,
							charName=charName ,
							elem='Front' ,
							side='RGT'
						)
	rigTools.dummyNaming(
							obj=rgtFrLeg.midLegRbn ,
							attr='rbn' ,
							dummy='midLegRbn' ,
							charName=charName ,
							elem='Front' ,
							side='RGT'
						)
	rigTools.dummyNaming(
							obj=rgtFrLeg.lowLegRbn ,
							attr='rbn' ,
							dummy='lowLegRbn' ,
							charName=charName ,
							elem='Front' ,
							side='RGT'
						)

	legIkRGTCtrlShape = pc.Dag(rgtFrLeg.legIk_ctrl.shape)
	legIkRGTCtrlShape.attr('autoFlexAmp').v = -135
	legIkRGTCtrlShape.attr('autoFlexMin').v = -45
	legIkRGTCtrlShape.attr('autoFlexMax').v = 0

	# Left Hip
	print 'Rigging Left Hip'
	lftHipRig = pclav.ClavicleRig(
										parent=pelvisRig.pelvis_jnt ,
										side='LFT' ,
										animGrp=anim ,
										skinGrp=skin ,
										charSize=size ,
										tmpJnt = (
													'hipLFT_tmpJnt' ,
													'upLegBackLFT_tmpJnt'
												)
									)

	rigTools.nodeNaming(
							lftHipRig ,
							charName = charName ,
							elem = 'Hip' ,
							side = 'LFT'
						)

	# Right Hip
	print 'Rigging Right Hip'
	rgtHipRig = pclav.ClavicleRig(
										parent=pelvisRig.pelvis_jnt ,
										side='RGT' ,
										animGrp=anim ,
										skinGrp=skin ,
										charSize=size ,
										tmpJnt = (
													'hipRGT_tmpJnt' ,
													'upLegBackRGT_tmpJnt'
												)
									)

	rigTools.nodeNaming(
							rgtHipRig ,
							charName = charName ,
							elem = 'Hip' ,
							side = 'RGT'
						)

	# Left Back Leg
	print 'Rigging Left Back Leg'
	lftBckLeg = pbleg.BackLegRig(
									parent=lftHipRig.clav2_jnt ,
									side='LFT' ,
									animGrp=anim ,
									jntGrp=jnt ,
									skinGrp=skin ,
									stillGrp=still ,
									ribbon=True ,
									charSize=size ,
									tmpJnt=(
												'upLegBackLFT_tmpJnt' ,
												'midLegBackLFT_tmpJnt' ,
												'lowLegBackLFT_tmpJnt' ,
												'ankleBackLFT_tmpJnt' ,
												'ballBackLFT_tmpJnt' ,
												'toeBackLFT_tmpJnt' ,
												'heelBackLFT_tmpJnt' ,
												'footInBackLFT_tmpJnt' ,
												'footOutBackLFT_tmpJnt' ,
												'kneeIkBackPivLFT_tmpJnt'
											)
								)

	rigTools.nodeNaming(
							lftBckLeg ,
							charName=charName ,
							elem='Back' ,
							side='LFT'
						)
	rigTools.dummyNaming(
							obj=lftBckLeg.upLegRbn ,
							attr='rbn' ,
							dummy='upLegRbn' ,
							charName=charName ,
							elem='Back' ,
							side='LFT'
						)
	rigTools.dummyNaming(
							obj=lftBckLeg.midLegRbn ,
							attr='rbn' ,
							dummy='midLegRbn' ,
							charName=charName ,
							elem='Back' ,
							side='LFT'
						)
	rigTools.dummyNaming(
							obj=lftBckLeg.lowLegRbn ,
							attr='rbn' ,
							dummy='lowLegRbn' ,
							charName=charName ,
							elem='Back' ,
							side='LFT'
						)

	# Right Back Leg
	print 'Rigging Right Back Leg'
	lftBckLeg = pbleg.BackLegRig(
									parent=rgtHipRig.clav2_jnt ,
									side='RGT' ,
									animGrp=anim ,
									jntGrp=jnt ,
									skinGrp=skin ,
									stillGrp=still ,
									ribbon=True ,
									charSize=size ,
									tmpJnt=(
												'upLegBackRGT_tmpJnt' ,
												'midLegBackRGT_tmpJnt' ,
												'lowLegBackRGT_tmpJnt' ,
												'ankleBackRGT_tmpJnt' ,
												'ballBackRGT_tmpJnt' ,
												'toeBackRGT_tmpJnt' ,
												'heelBackRGT_tmpJnt' ,
												'footInBackRGT_tmpJnt' ,
												'footOutBackRGT_tmpJnt' ,
												'kneeIkBackPivRGT_tmpJnt'
											)
								)

	rigTools.nodeNaming(
							lftBckLeg ,
							charName=charName ,
							elem='Back' ,
							side='RGT'
						)
	rigTools.dummyNaming(
							obj=lftBckLeg.upLegRbn ,
							attr='rbn' ,
							dummy='upLegRbn' ,
							charName=charName ,
							elem='Back' ,
							side='RGT'
						)
	rigTools.dummyNaming(
							obj=lftBckLeg.midLegRbn ,
							attr='rbn' ,
							dummy='midLegRbn' ,
							charName=charName ,
							elem='Back' ,
							side='RGT'
						)
	rigTools.dummyNaming(
							obj=lftBckLeg.lowLegRbn ,
							attr='rbn' ,
							dummy='lowLegRbn' ,
							charName=charName ,
							elem='Back' ,
							side='RGT'
						)

	# Left Ear
	print 'Rigging Left Ear'
	lftEarRig = pfk.FkRig(
								parent=headRig.head2_jnt ,
								animGrp=anim ,
								charSize=size ,
								tmpJnt=[
											'ear1LFT_jnt' ,
											'ear2LFT_jnt' ,
											'ear3LFT_jnt' ,
											'ear4LFT_jnt' ,
											'ear5LFT_jnt'
										] ,
								name='ear' ,
								side='LFT' ,
								ax='y' ,
								shape='circle'
							)

	# Right Ear
	print 'Rigging Right Ear'
	rgtEarRig = pfk.FkRig(
								parent=headRig.head2_jnt ,
								animGrp=anim ,
								charSize=size ,
								rotateOrder='zxy',
								tmpJnt=[
											'ear1RGT_jnt' ,
											'ear2RGT_jnt' ,
											'ear3RGT_jnt' ,
											'ear4RGT_jnt' ,
											'ear5RGT_jnt'
										] ,
								name='ear' ,
								side='RGT' ,
								ax='y' ,
								shape='circle'
							)

	# Tail
	print 'Rigging Tail'
	tailRig = pfk.FkRig(
								parent=pelvisRig.pelvis_jnt ,
								animGrp=anim ,
								charSize=size ,
								rotateOrder='zxy',
								tmpJnt=[
											'tail1_jnt' ,
											'tail2_jnt' ,
											'tail3_jnt' ,
											'tail4_jnt' ,
											'tail5_jnt'
										] ,
								name='tail' ,
								side='' ,
								ax='y' ,
								shape='circle'
							)

	# Tongue
	print 'Rigging Tongue'
	tongueRig = pfk.FkRig(
								parent = headRig.jaw1LWR_jnt ,
								animGrp = 'anim_grp' ,
								charSize = size ,
								rotateOrder='zxy',
								tmpJnt = [
											'tongue1_jnt' ,
											'tongue2_jnt' ,
											'tongue3_jnt' ,
											'tongue4_jnt' 
										] ,
								name = 'tongue' ,
								side = '' ,
								ax='y' ,
								shape='circle'
						)

	# Upper Teeth
	print 'Rigging Upper Teeth'
	upperTeethRig = pfkg.FkGroupRig(
										parent = headRig.head1_jnt ,
										animGrp = 'anim_grp' ,
										charSize = size ,
										tmpJnt = [ 'upperTeeth_jnt' ] ,
										name = 'upperTeeth' ,
										side = '' ,
										shape='cube'
									)

	# Lower Teeth
	print 'Rigging Lower Teeth'
	lowerTeethRig = pfkg.FkGroupRig(
										parent = headRig.jaw1LWR_jnt ,
										animGrp = 'anim_grp' ,
										charSize = size ,
										tmpJnt = [ 'lowerTeeth_jnt' ] ,
										name = 'lowerTeeth' ,
										side = '' ,
										shape='cube'
									)
	# nose
	print 'Rigging Nose'
	noseRig = pfkg.FkGroupRig(
								parent = 'jaw1UPR_jnt' ,
								animGrp = 'anim_grp' ,
								charSize = size ,
								tmpJnt = ( 
											  'nose_jnt',

											   ) ,
								name = 'nose' ,
								side = '' ,
								shape='cube'
								)

	# left horn
	print 'Rigging Left Horn'
	hornRigLFT = pfkg.FkGroupRig(
										parent = headRig.head2_jnt ,
										animGrp = 'anim_grp' ,
										charSize = size ,
										tmpJnt = [ 'hornLFT_jnt' ] ,
										name = 'horn' ,
										side = 'LFT' ,
										shape='cube'
									)

	# right horn
	print 'Rigging Right Horn'
	hornRigRGT = pfkg.FkGroupRig(
										parent = headRig.head2_jnt ,
										animGrp = 'anim_grp' ,
										charSize = size ,
										tmpJnt = [ 'hornRGT_jnt' ] ,
										name = 'horn' ,
										side = 'RGT' ,
										shape='cube'
									)

	mc.setAttr( 'jaw1CtrlZroUPR_grp.v' , l=False )
	mc.setAttr( 'jaw2CtrlZroLWR_grp.v' , l=False )

	jawUp = pc.Dag( 'jaw1CtrlZroUPR_grp' )
	jawUp.attr( 'v' ).v = 0
	jawUp.lockHideAttrs( 'v' )

	jawLo = pc.Dag( 'jaw2CtrlZroLWR_grp' )
	jawLo.attr( 'v' ).v = 0
	jawLo.lockHideAttrs( 'v' )

	rigTools.adjustRadius( 0.1 )
	mc.delete( 'tmpJnt_grp' )
	mc.parent( 'addJnt_grp' , 'skin_grp' )