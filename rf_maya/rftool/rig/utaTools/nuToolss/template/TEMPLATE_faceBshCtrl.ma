//Maya ASCII 2012 scene
//Name: TEMPLATE_faceBshCtrl.ma
//Last modified: Thu, Oct 24, 2013 02:34:13 PM
//Codeset: 1252
requires maya "2012";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2012";
fileInfo "version" "2012 x64";
fileInfo "cutIdentifier" "001200000000-796618";
fileInfo "osv" "Microsoft Windows 7 Business Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 2.1489866616923621 4.6148824277621365 13.088208673698077 ;
	setAttr ".r" -type "double3" -17.738352729602823 9.399999999999741 -1.2089414763194675e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 14.272300471330253;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "browCtrlLFT_zgrp";
	setAttr ".t" -type "double3" 1 2 0 ;
createNode transform -n "browLFT_ctrl" -p "browCtrlLFT_zgrp";
	addAttr -ci true -sn "_TYPE" -ln "_TYPE" -dt "string";
	addAttr -ci true -sn "shapeType" -ln "shapeType" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on "._TYPE" -type "string" "Controller";
	setAttr -l on ".shapeType" -type "string" "browShape";
createNode nurbsCurve -n "browLFT_ctrlShape" -p "browLFT_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 26 0 no 3
		27 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26
		27
		-0.5 -0.13688750000000002 3.0395130856675222e-017
		-0.47926999999999997 -0.17179750000000002 3.8146708014608064e-017
		-0.45041500000000001 -0.17746750000000003 3.9405700924532994e-017
		-0.350385 -0.13924250000000002 3.091804590127367e-017
		-0.24809500000000001 -0.10850750000000003 2.4093504968902836e-017
		-0.096780000000000005 -0.080842500000000012 1.7950640973651843e-017
		0.096780000000000005 -0.080842500000000012 1.7950640973651843e-017
		0.24809500000000001 -0.10850750000000003 2.4093504968902836e-017
		0.350385 -0.13924250000000002 3.091804590127367e-017
		0.45041500000000001 -0.17746750000000003 3.9405700924532994e-017
		0.47926999999999997 -0.17179750000000002 3.8146708014608064e-017
		0.5 -0.13688750000000002 3.0395130856675222e-017
		0.50170000000000003 -0.090782500000000016 2.0157764346606655e-017
		0.49080499999999999 -0.045377500000000008 1.0075829059985608e-017
		0.43320999999999998 0.0113075 -2.5107693701897907e-018
		0.36735000000000001 0.065002500000000005 -1.4433454431639346e-017
		0.28179500000000002 0.11751250000000002 -2.609301663625274e-017
		0.20691999999999999 0.14822750000000001 -3.2913116676525075e-017
		0.10156 0.17095750000000001 -3.7960190546471036e-017
		-0.10156 0.17095750000000001 -3.7960190546471036e-017
		-0.20691999999999999 0.14822750000000001 -3.2913116676525075e-017
		-0.28179500000000002 0.11751250000000002 -2.609301663625274e-017
		-0.36735000000000001 0.065002500000000005 -1.4433454431639346e-017
		-0.43320999999999998 0.0113075 -2.5107693701897907e-018
		-0.49080499999999999 -0.045377500000000008 1.0075829059985608e-017
		-0.50170000000000003 -0.090782500000000016 2.0157764346606655e-017
		-0.5 -0.13688750000000002 3.0395130856675222e-017
		;
createNode transform -n "browCtrlRHT_zgrp";
	setAttr ".t" -type "double3" -1 2 0 ;
createNode transform -n "browRHT_ctrl" -p "browCtrlRHT_zgrp";
	addAttr -ci true -sn "_TYPE" -ln "_TYPE" -dt "string";
	addAttr -ci true -sn "shapeType" -ln "shapeType" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on "._TYPE" -type "string" "Controller";
	setAttr -l on ".shapeType" -type "string" "browShape";
createNode nurbsCurve -n "browRHT_ctrlShape" -p "browRHT_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 26 0 no 3
		27 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26
		27
		-0.5 -0.13526000000000002 3.0033753262159733e-017
		-0.47926999999999997 -0.17017000000000002 3.7785330420092575e-017
		-0.45041500000000001 -0.17584000000000002 3.9044323330017504e-017
		-0.350385 -0.13761500000000002 3.0556668306758181e-017
		-0.24809500000000001 -0.10688000000000003 2.3732127374387347e-017
		-0.096780000000000005 -0.079215000000000008 1.7589263379136354e-017
		0.096780000000000005 -0.079215000000000008 1.7589263379136354e-017
		0.24809500000000001 -0.10688000000000003 2.3732127374387347e-017
		0.350385 -0.13761500000000002 3.0556668306758181e-017
		0.45041500000000001 -0.17584000000000002 3.9044323330017504e-017
		0.47926999999999997 -0.17017000000000002 3.7785330420092575e-017
		0.5 -0.13526000000000002 3.0033753262159733e-017
		0.50170000000000003 -0.089155000000000012 1.9796386752091166e-017
		0.49080499999999999 -0.043750000000000004 9.7144514654701191e-018
		0.43320999999999998 0.012935000000000004 -2.87214696470528e-018
		0.36735000000000001 0.066630000000000009 -1.4794832026154835e-017
		0.28179500000000002 0.11914000000000002 -2.6454394230768229e-017
		0.20691999999999999 0.14985500000000002 -3.3274494271040564e-017
		0.10156 0.17258500000000002 -3.8321568140986526e-017
		-0.10156 0.17258500000000002 -3.8321568140986526e-017
		-0.20691999999999999 0.14985500000000002 -3.3274494271040564e-017
		-0.28179500000000002 0.11914000000000002 -2.6454394230768229e-017
		-0.36735000000000001 0.066630000000000009 -1.4794832026154835e-017
		-0.43320999999999998 0.012935000000000004 -2.87214696470528e-018
		-0.49080499999999999 -0.043750000000000004 9.7144514654701191e-018
		-0.50170000000000003 -0.089155000000000012 1.9796386752091166e-017
		-0.5 -0.13526000000000002 3.0033753262159733e-017
		;
createNode transform -n "mouthCtrlMID_zgrp";
createNode transform -n "mouthMID_ctrl" -p "mouthCtrlMID_zgrp";
	addAttr -ci true -sn "_TYPE" -ln "_TYPE" -dt "string";
	addAttr -ci true -sn "shapeType" -ln "shapeType" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on "._TYPE" -type "string" "Controller";
	setAttr -l on ".shapeType" -type "string" "roundCrnrRec";
createNode nurbsCurve -n "mouthMID_ctrlShape" -p "mouthMID_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		1 21 0 no 3
		22 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21
		22
		0 0.22407000000000005 -4.9753534625551763e-017
		0.72921000000000002 0.22407000000000005 -4.9753534625551763e-017
		0.86178999999999994 0.22393000000000005 -4.9722448380862259e-017
		0.93713000000000002 0.19707000000000005 -4.3758330292575919e-017
		0.97972999999999999 0.15878000000000003 -3.5256242369996472e-017
		1.0010300000000001 0.10773000000000003 -2.3920865288573624e-017
		1.0010300000000001 -0.10856000000000003 2.41051623106614e-017
		0.97972999999999999 -0.15961000000000003 3.5440539392084247e-017
		0.93713000000000002 -0.19789000000000007 4.3940406868614448e-017
		0.86178999999999994 -0.22331000000000006 4.9584780725808743e-017
		0.72921000000000002 -0.22351000000000004 4.9629189646793745e-017
		-0.72921000000000002 -0.22351000000000004 4.9629189646793745e-017
		-0.86178999999999994 -0.22331000000000006 4.9584780725808743e-017
		-0.93713000000000002 -0.19789000000000007 4.3940406868614448e-017
		-0.97972999999999999 -0.15961000000000003 3.5440539392084247e-017
		-1.0010300000000001 -0.10856000000000003 2.41051623106614e-017
		-1.0010300000000001 0.10773000000000003 -2.3920865288573624e-017
		-0.97972999999999999 0.15878000000000003 -3.5256242369996472e-017
		-0.93713000000000002 0.19707000000000005 -4.3758330292575919e-017
		-0.86178999999999994 0.22393000000000005 -4.9722448380862259e-017
		-0.72921000000000002 0.22407000000000005 -4.9753534625551763e-017
		0 0.22407000000000005 -4.9753534625551763e-017
		;
createNode transform -n "mouthCtrlLFT_zgrp";
	setAttr ".t" -type "double3" 1.5 0 0 ;
createNode transform -n "mouthLFT_ctrl" -p "mouthCtrlLFT_zgrp";
	addAttr -ci true -sn "_TYPE" -ln "_TYPE" -dt "string";
	addAttr -ci true -sn "shapeType" -ln "shapeType" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on "._TYPE" -type "string" "Controller";
	setAttr -l on ".shapeType" -type "string" "square";
createNode nurbsCurve -n "mouthLFT_ctrlShape" -p "mouthLFT_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		0.25 0.25000000000000006 -5.5511151231257827e-017
		0.25 -0.25000000000000006 5.5511151231257827e-017
		-0.25 -0.25000000000000006 5.5511151231257827e-017
		-0.25 0.25000000000000006 -5.5511151231257827e-017
		0.25 0.25000000000000006 -5.5511151231257827e-017
		;
createNode transform -n "mouthCtrlRHT_zgrp";
	setAttr ".t" -type "double3" -1.5 0 0 ;
createNode transform -n "mouthRHT_ctrl" -p "mouthCtrlRHT_zgrp";
	addAttr -ci true -sn "_TYPE" -ln "_TYPE" -dt "string";
	addAttr -ci true -sn "shapeType" -ln "shapeType" -dt "string";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr -l on "._TYPE" -type "string" "Controller";
	setAttr -l on ".shapeType" -type "string" "square";
createNode nurbsCurve -n "mouthRHT_ctrlShape" -p "mouthRHT_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 17;
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		0.25 0.25000000000000006 -5.5511151231257827e-017
		0.25 -0.25000000000000006 5.5511151231257827e-017
		-0.25 -0.25000000000000006 5.5511151231257827e-017
		-0.25 0.25000000000000006 -5.5511151231257827e-017
		0.25 0.25000000000000006 -5.5511151231257827e-017
		;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
lockNode -l 1 ;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
lockNode -l 1 ;
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
lockNode -l 1 ;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -k on ".mico";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -k on ".micr";
	setAttr -k on ".micg";
	setAttr -k on ".micb";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
lockNode -l 1 ;
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
lockNode -l 1 ;
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
lockNode -l 1 ;
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
lockNode -l 1 ;
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
lockNode -l 1 ;
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
lockNode -l 1 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".rp";
	setAttr -k on ".cai";
	setAttr -k on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -k on ".ei";
	setAttr -k on ".ex";
	setAttr -av -k on ".es";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bf";
	setAttr -k on ".fii";
	setAttr -av -k on ".sf";
	setAttr -k on ".gr";
	setAttr -k on ".li";
	setAttr -k on ".ls";
	setAttr -k on ".mb";
	setAttr -k on ".ti";
	setAttr -k on ".txt";
	setAttr -k on ".mpr";
	setAttr -k on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -k on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -k on ".as";
	setAttr -k on ".ds";
	setAttr -k on ".lm";
	setAttr -k on ".fir";
	setAttr -k on ".aap";
	setAttr -k on ".gh";
	setAttr -cb on ".sd";
lockNode -l 1 ;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of TEMPLATE_faceBshCtrl.ma
