//Maya ASCII 2015 scene
//Name: TEMPLATE_bshCtrlFriends3.ma
//Last modified: Tue, Jun 07, 2016 04:30:41 PM
//Codeset: 1252
requires maya "2015";
requires "Mayatomr" "2012.0m - 3.9.1.48 ";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2015";
fileInfo "version" "2015";
fileInfo "cutIdentifier" "201503261530-955654";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 2.4207521068587856 13.363789518569707 6.6698185345659136 ;
	setAttr ".r" -type "double3" -26.138352729196296 21.800000000001063 0 ;
	setAttr ".rp" -type "double3" 0 -3.5527136788005009e-015 1.7763568394002505e-015 ;
	setAttr ".rpt" -type "double3" -1.7669057640736263e-016 -9.3671696363680272e-017 
		-3.9809070816125032e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 6.5282524799595718;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0 10.388010999999999 1.3236800000000035 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.5315355626884917 100.45844266046294 0.78924046296869088 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".rp" -type "double3" -5.5511151231257827e-017 0 -1.4210854715202004e-014 ;
	setAttr ".rpt" -type "double3" 0 -1.4210854715202007e-014 1.4210854715202007e-014 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 88.932489665955117;
	setAttr ".ow" 0.20817605519162372;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" -0.31277558364673819 11.168030738830566 1.2386828573856794 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 31.297407763986005 24.309300333413209 197.71624905097198 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 5.5435604252730561;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 142.20406774686421 23.574200767941925 0.25775531159343001 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 2.4562386765110227;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "faceRig_grp";
createNode transform -n "faceRigOffset_grp" -p "faceRig_grp";
createNode transform -n "mouthBshCtrl_grp" -p "faceRigOffset_grp";
createNode transform -n "lipBshCtrlZroLFT_grp" -p "mouthBshCtrl_grp";
	setAttr ".t" -type "double3" 0.57007249999999998 20.388011 1.3236800000000071 ;
	setAttr ".rp" -type "double3" 0 -10 -3.5527136788005009e-015 ;
	setAttr ".sp" -type "double3" 0 -10 -3.5527136788005009e-015 ;
createNode transform -n "lipBshLFT_ctrl" -p "lipBshCtrlZroLFT_grp";
	addAttr -ci true -k true -sn "crnrMouthIO" -ln "crnrMouthIO" -at "double";
	addAttr -ci true -k true -sn "crnrMouthUD" -ln "crnrMouthUD" -at "double";
	addAttr -ci true -k true -sn "upLipsUD" -ln "upLipsUD" -at "double";
	addAttr -ci true -k true -sn "loLipsUD" -ln "loLipsUD" -at "double";
	addAttr -ci true -sn "upCrnrLipsUD" -ln "upCrnrLipsUD" -at "double";
	addAttr -ci true -sn "loCrnrLipsUD" -ln "loCrnrLipsUD" -at "double";
	addAttr -ci true -k true -sn "lipsPartIO" -ln "lipsPartIO" -at "double";
	addAttr -ci true -k true -sn "cheekIO" -ln "cheekIO" -at "double";
	addAttr -ci true -k true -sn "cheekUprIO" -ln "cheekUprIO" -at "double";
	addAttr -ci true -k true -sn "cheekLwrIO" -ln "cheekLwrIO" -at "double";
	addAttr -ci true -k true -sn "puffIO" -ln "puffIO" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0 -10 -3.5527136788005009e-015 ;
	setAttr ".sp" -type "double3" 0 -10 -3.5527136788005009e-015 ;
	setAttr -k on ".upCrnrLipsUD";
	setAttr -k on ".loCrnrLipsUD";
createNode nurbsCurve -n "lipBshLFT_ctrlShape" -p "lipBshLFT_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		-0.037535499999999944 -9.9370360000000009 -3.5527136788005009e-015
		0.017487499999999878 -9.9522880000000011 -3.5527136788005009e-015
		0.072510500000000144 -9.9675400000000014 -3.5527136788005009e-015
		0.11915650000000011 -9.9804700000000004 -3.5527136788005009e-015
		0.13201050000000003 -9.9840330000000002 -3.5527136788005009e-015
		0.14144650000000003 -10 -3.5527136788005009e-015
		0.13201050000000003 -10.015967 -3.5527136788005009e-015
		0.11915650000000011 -10.019530000000001 -3.5527136788005009e-015
		0.072510500000000144 -10.03246 -3.5527136788005009e-015
		0.017487499999999878 -10.047712000000001 -3.5527136788005009e-015
		-0.037535499999999944 -10.062964000000001 -3.5527136788005009e-015
		-0.12859149999999997 -10.088204000000001 -3.5527136788005009e-015
		-0.14144650000000003 -10.091767000000001 -3.5527136788005009e-015
		-0.092746500000000065 -10 -3.5527136788005009e-015
		-0.14144650000000003 -9.908233000000001 -3.5527136788005009e-015
		-0.12859149999999997 -9.9117960000000007 -3.5527136788005009e-015
		-0.037535499999999944 -9.9370360000000009 -3.5527136788005009e-015
		;
createNode transform -n "lipBshCtrlZro_grp" -p "mouthBshCtrl_grp";
	setAttr ".t" -type "double3" 0 20.388011 1.3236800000000071 ;
	setAttr ".rp" -type "double3" 0 -10 -3.5527136788005009e-015 ;
	setAttr ".sp" -type "double3" 0 -10 -3.5527136788005009e-015 ;
createNode transform -n "lipBsh_ctrl" -p "lipBshCtrlZro_grp";
	addAttr -ci true -k true -sn "__face__" -ln "__face__" -at "double";
	addAttr -ci true -k true -sn "faceSquash" -ln "faceSquash" -at "double";
	addAttr -ci true -k true -sn "__nose__" -ln "__nose__" -at "double";
	addAttr -ci true -k true -sn "noseSquash" -ln "noseSquash" -at "double";
	addAttr -ci true -k true -sn "leftUp" -ln "leftUp" -at "double";
	addAttr -ci true -k true -sn "leftTurn" -ln "leftTurn" -at "double";
	addAttr -ci true -k true -sn "rightUp" -ln "rightUp" -at "double";
	addAttr -ci true -k true -sn "rightTurn" -ln "rightTurn" -at "double";
	addAttr -ci true -k true -sn "__mouth__" -ln "__mouth__" -at "double";
	addAttr -ci true -k true -sn "mouthLR" -ln "mouthLR" -at "double";
	addAttr -ci true -k true -sn "mouthUD" -ln "mouthUD" -at "double";
	addAttr -ci true -k true -sn "mouthTurn" -ln "mouthTurn" -at "double";
	addAttr -ci true -k true -sn "upLipsUD" -ln "upLipsUD" -at "double";
	addAttr -ci true -k true -sn "loLipsUD" -ln "loLipsUD" -at "double";
	addAttr -ci true -k true -sn "upLipsMidUD" -ln "upLipsMidUD" -at "double";
	addAttr -ci true -k true -sn "loLipsMidUD" -ln "loLipsMidUD" -at "double";
	addAttr -ci true -k true -sn "upLipsCurlIO" -ln "upLipsCurlIO" -at "double";
	addAttr -ci true -k true -sn "loLipsCurlIO" -ln "loLipsCurlIO" -at "double";
	addAttr -ci true -k true -sn "mouthClench" -ln "mouthClench" -at "double";
	addAttr -ci true -k true -sn "mouthPull" -ln "mouthPull" -at "double";
	addAttr -ci true -k true -sn "mouthU" -ln "mouthU" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0 -10 -3.5527136788005009e-015 ;
	setAttr ".sp" -type "double3" 0 -10 -3.5527136788005009e-015 ;
	setAttr -l on -k on ".__face__";
	setAttr -l on -k on ".__nose__";
	setAttr -l on -k on ".__mouth__";
createNode nurbsCurve -n "lipBsh_ctrlShape" -p "lipBsh_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		0 -10.091767000000001 -3.5527136788005009e-015
		0.18781400000000004 -10.091767000000001 -3.5527136788005009e-015
		0.34703400000000006 -10.091767000000001 -3.5527136788005009e-015
		0.39091100000000001 -10.091767000000001 -3.5527136788005009e-015
		0.42311900000000002 -10 -3.5527136788005009e-015
		0.39091100000000001 -9.908233000000001 -3.5527136788005009e-015
		0.34703400000000006 -9.908233000000001 -3.5527136788005009e-015
		0.18781400000000004 -9.908233000000001 -3.5527136788005009e-015
		0 -9.908233000000001 -3.5527136788005009e-015
		-0.18781400000000004 -9.908233000000001 -3.5527136788005009e-015
		-0.34703400000000006 -9.908233000000001 -3.5527136788005009e-015
		-0.39091100000000001 -9.908233000000001 -3.5527136788005009e-015
		-0.42311900000000002 -10 -3.5527136788005009e-015
		-0.39091100000000001 -10.091767000000001 -3.5527136788005009e-015
		-0.34703400000000006 -10.091767000000001 -3.5527136788005009e-015
		-0.18781400000000004 -10.091767000000001 -3.5527136788005009e-015
		0 -10.091767000000001 -3.5527136788005009e-015
		;
createNode transform -n "lipBshCtrlZroRGT_grp" -p "mouthBshCtrl_grp";
	setAttr ".t" -type "double3" -0.57007249999999998 20.388011 1.3236800000000071 ;
	setAttr ".rp" -type "double3" 0 -10 -3.5527136788005009e-015 ;
	setAttr ".sp" -type "double3" 0 -10 -3.5527136788005009e-015 ;
createNode transform -n "lipBshRGT_ctrl" -p "lipBshCtrlZroRGT_grp";
	addAttr -ci true -k true -sn "crnrMouthIO" -ln "crnrMouthIO" -at "double";
	addAttr -ci true -k true -sn "crnrMouthUD" -ln "crnrMouthUD" -at "double";
	addAttr -ci true -k true -sn "upLipsUD" -ln "upLipsUD" -at "double";
	addAttr -ci true -k true -sn "loLipsUD" -ln "loLipsUD" -at "double";
	addAttr -ci true -sn "upCrnrLipsUD" -ln "upCrnrLipsUD" -at "double";
	addAttr -ci true -sn "loCrnrLipsUD" -ln "loCrnrLipsUD" -at "double";
	addAttr -ci true -k true -sn "lipsPartIO" -ln "lipsPartIO" -at "double";
	addAttr -ci true -k true -sn "cheekIO" -ln "cheekIO" -at "double";
	addAttr -ci true -k true -sn "cheekUprIO" -ln "cheekUprIO" -at "double";
	addAttr -ci true -k true -sn "cheekLwrIO" -ln "cheekLwrIO" -at "double";
	addAttr -ci true -k true -sn "puffIO" -ln "puffIO" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0 -10 -3.5527136788005009e-015 ;
	setAttr ".sp" -type "double3" 0 -10 -3.5527136788005009e-015 ;
	setAttr -k on ".upCrnrLipsUD";
	setAttr -k on ".loCrnrLipsUD";
createNode nurbsCurve -n "lipBshRGT_ctrlShape" -p "lipBshRGT_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		-0.017487499999999878 -9.9522880000000011 -3.5527136788005009e-015
		0.037535499999999944 -9.9370360000000009 -3.5527136788005009e-015
		0.12859149999999997 -9.9117960000000007 -3.5527136788005009e-015
		0.14144650000000003 -9.908233000000001 -3.5527136788005009e-015
		0.092746500000000065 -10 -3.5527136788005009e-015
		0.14144650000000003 -10.091767000000001 -3.5527136788005009e-015
		0.12859149999999997 -10.088204000000001 -3.5527136788005009e-015
		0.037535499999999944 -10.062964000000001 -3.5527136788005009e-015
		-0.017487499999999878 -10.047712000000001 -3.5527136788005009e-015
		-0.072510500000000144 -10.03246 -3.5527136788005009e-015
		-0.11915650000000011 -10.019530000000001 -3.5527136788005009e-015
		-0.13201050000000003 -10.015967 -3.5527136788005009e-015
		-0.14144650000000003 -10 -3.5527136788005009e-015
		-0.13201050000000003 -9.9840330000000002 -3.5527136788005009e-015
		-0.11915650000000011 -9.9804700000000004 -3.5527136788005009e-015
		-0.072510500000000144 -9.9675400000000014 -3.5527136788005009e-015
		-0.017487499999999878 -9.9522880000000011 -3.5527136788005009e-015
		;
createNode transform -n "lidBshProxyLocZroLFT_grp" -p "faceRigOffset_grp";
	setAttr ".t" -type "double3" 0.3833629100271822 11.132526898384096 0.44771529436111335 ;
createNode transform -n "lidBshProxyLFT_loc" -p "lidBshProxyLocZroLFT_grp";
	addAttr -ci true -k true -sn "upLidUD" -ln "upLidUD" -at "double";
	addAttr -ci true -k true -sn "loLidUD" -ln "loLidUD" -at "double";
	addAttr -ci true -k true -sn "upLidTW" -ln "upLidTW" -at "double";
	addAttr -ci true -k true -sn "loLidTW" -ln "loLidTW" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "lidBshProxyLFT_locShape" -p "lidBshProxyLFT_loc";
	setAttr -k off ".v";
createNode transform -n "lidBshProxyLocZroRGT_grp" -p "faceRigOffset_grp";
	setAttr ".t" -type "double3" -0.38266796499999722 11.132526898384096 0.44771529436111424 ;
createNode transform -n "lidBshProxyRGT_loc" -p "lidBshProxyLocZroRGT_grp";
	addAttr -ci true -k true -sn "upLidUD" -ln "upLidUD" -at "double";
	addAttr -ci true -k true -sn "loLidUD" -ln "loLidUD" -at "double";
	addAttr -ci true -k true -sn "upLidTW" -ln "upLidTW" -at "double";
	addAttr -ci true -k true -sn "loLidTW" -ln "loLidTW" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode locator -n "lidBshProxyRGT_locShape" -p "lidBshProxyRGT_loc";
	setAttr -k off ".v";
createNode transform -n "ebBshCtrlZroRGT_grp" -p "faceRigOffset_grp";
	setAttr ".t" -type "double3" -0.45232346499999987 11.580174 1.2748004610756993 ;
createNode transform -n "ebBshRGT_ctrl" -p "ebBshCtrlZroRGT_grp";
	addAttr -ci true -k true -sn "ebIO" -ln "ebIO" -at "double";
	addAttr -ci true -k true -sn "ebUD" -ln "ebUD" -at "double";
	addAttr -ci true -k true -sn "ebTurn" -ln "ebTurn" -at "double";
	addAttr -ci true -k true -sn "ebInnerIO" -ln "ebInnerIO" -at "double";
	addAttr -ci true -k true -sn "ebInnerUD" -ln "ebInnerUD" -at "double";
	addAttr -ci true -k true -sn "ebInnerTurn" -ln "ebInnerTurn" -at "double";
	addAttr -ci true -k true -sn "ebMidUD" -ln "ebMidUD" -at "double";
	addAttr -ci true -k true -sn "ebOuterUD" -ln "ebOuterUD" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on ".t";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -l on ".r";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "ebBshRGT_ctrlShape" -p "ebBshRGT_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		0.32090050000000003 0.035758000000000401 0
		0.17366949999999992 0.12227500000000058 0
		-4.9999999984784438e-007 0.16899700000000051 0
		-0.17367049999999984 0.12227500000000058 0
		-0.32090049999999981 0.035758000000000401 0
		-0.3614735 0.00071600000000060504 0
		-0.39125549999999998 -0.08413999999999966 0
		-0.3614735 -0.16899699999999962 0
		-0.32090049999999981 -0.13395499999999938 0
		-0.17367049999999984 -0.047437999999999647 0
		-4.9999999984784438e-007 4.4408920985006262e-016 0
		0.17366949999999992 -0.047437999999999647 0
		0.32090050000000003 -0.13395499999999938 0
		0.36147250000000009 -0.16899699999999962 0
		0.3912555000000002 -0.08413999999999966 0
		0.36147250000000009 0.00071600000000060504 0
		0.32090050000000003 0.035758000000000401 0
		;
createNode transform -n "ebBshCtrlZroLFT_grp" -p "faceRigOffset_grp";
	setAttr ".t" -type "double3" 0.45298753500000011 11.580174 1.2748004610756993 ;
createNode transform -n "ebBshLFT_ctrl" -p "ebBshCtrlZroLFT_grp";
	addAttr -ci true -k true -sn "ebIO" -ln "ebIO" -at "double";
	addAttr -ci true -k true -sn "ebUD" -ln "ebUD" -at "double";
	addAttr -ci true -k true -sn "ebTurn" -ln "ebTurn" -at "double";
	addAttr -ci true -k true -sn "ebInnerIO" -ln "ebInnerIO" -at "double";
	addAttr -ci true -k true -sn "ebInnerUD" -ln "ebInnerUD" -at "double";
	addAttr -ci true -k true -sn "ebInnerTurn" -ln "ebInnerTurn" -at "double";
	addAttr -ci true -k true -sn "ebMidUD" -ln "ebMidUD" -at "double";
	addAttr -ci true -k true -sn "ebOuterUD" -ln "ebOuterUD" -at "double";
	setAttr -l on -k off ".v";
	setAttr -l on ".t";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -l on ".r";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
createNode nurbsCurve -n "ebBshLFT_ctrlShape" -p "ebBshLFT_ctrl";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 6;
	setAttr ".cc" -type "nurbsCurve" 
		3 14 0 no 3
		19 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14
		17
		-0.32090050000000003 0.035758000000000401 0
		-0.17366950000000014 0.12227500000000058 0
		4.9999999984784438e-007 0.16899700000000051 0
		0.17367049999999984 0.12227500000000058 0
		0.32090050000000003 0.035758000000000401 0
		0.36147350000000023 0.00071600000000060504 0
		0.3912555000000002 -0.08413999999999966 0
		0.36147350000000023 -0.16899699999999962 0
		0.32090050000000003 -0.13395499999999938 0
		0.17367049999999984 -0.047437999999999647 0
		4.9999999984784438e-007 4.4408920985006262e-016 0
		-0.17366950000000014 -0.047437999999999647 0
		-0.32090050000000003 -0.13395499999999938 0
		-0.36147250000000009 -0.16899699999999962 0
		-0.3912555000000002 -0.08413999999999966 0
		-0.36147250000000009 0.00071600000000060504 0
		-0.32090050000000003 0.035758000000000401 0
		;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	setAttr ".cdl" 16;
	setAttr -s 17 ".dli[1:16]"  5 10 1 9 7 8 11 12 
		3 4 2 6 13 14 15 16;
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
	setAttr -s 16 ".rlmi[1:15]"  5 4 3 2 1 6 7 8 
		9 10 16 11 12 13 14;
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
	setAttr ".rndr" no;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 100 -ast 1 -aet 100 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -av -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
	setAttr -k on ".etw";
	setAttr -k on ".tps";
	setAttr -k on ".tms";
lockNode -l 1 ;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
lockNode -l 1 ;
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
lockNode -l 1 ;
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
lockNode -l 1 ;
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
lockNode -l 1 ;
select -ne :defaultRenderingList1;
	setAttr -k on ".ihi";
select -ne :initialShadingGroup;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -cb on ".ren";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -av -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -k on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs";
	setAttr -k on ".ef";
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep" 1;
	setAttr -k on ".fec";
	setAttr -av -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -k on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".rv";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -av -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mot";
	setAttr -av -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -av -k on ".bls";
	setAttr -av -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
lockNode -l 1 ;
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w" 1920;
	setAttr -av -k on ".h" 1080;
	setAttr -av -k on ".pa" 0.99956250190734863;
	setAttr -av -k on ".al" yes;
	setAttr -av -k on ".dar" 1.7769999504089355;
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
lockNode -l 1 ;
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off -cb on ".ctrs" 256;
	setAttr -av -k off -cb on ".btrs" 512;
	setAttr -k off -cb on ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off -cb on ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
lockNode -l 1 ;
select -ne :hardwareRenderingGlobals;
	setAttr ".vac" 2;
select -ne :defaultHardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".rp";
	setAttr -k on ".cai";
	setAttr -k on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -k on ".ei";
	setAttr -av -k on ".ex";
	setAttr -av -k on ".es";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bf";
	setAttr -k on ".fii";
	setAttr -av -k on ".sf";
	setAttr -k on ".gr";
	setAttr -k on ".li";
	setAttr -k on ".ls";
	setAttr -av -k on ".mb";
	setAttr -k on ".ti";
	setAttr -k on ".txt";
	setAttr -k on ".mpr";
	setAttr -k on ".wzd";
	setAttr -k on ".fn";
	setAttr -k on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -k on ".as";
	setAttr -k on ".ds";
	setAttr -k on ".lm";
	setAttr -av -k on ".fir";
	setAttr -k on ".aap";
	setAttr -av -k on ".gh";
	setAttr -cb on ".sd";
lockNode -l 1 ;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr ":perspShape.msg" ":defaultRenderGlobals.sc";
// End of TEMPLATE_bshCtrlFriends3.ma
