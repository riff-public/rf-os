//Maya ASCII 2012 scene
//Name: TEMPLATE_skirtPos_01.ma
//Last modified: Thu, Nov 28, 2013 04:25:18 PM
//Codeset: 1252
requires maya "2012";
requires "Mayatomr" "2012.0m - 3.9.1.36 ";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2012";
fileInfo "version" "2012 x64";
fileInfo "cutIdentifier" "001200000000-796618";
fileInfo "osv" "Microsoft Windows 7 Business Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.58939252326378466 4.2490311555522142 9.2946272794101716 ;
	setAttr ".r" -type "double3" -18.938352729620636 3.4000000000004205 -9.9567589887152275e-017 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 9.9049571474319151;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.71190467155329817 1.4279886859071584 1.1470726559142645 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.087780912359665897 100.10003952158785 0.0096439053204650325 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 13.180658013390111;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.95215700449221796 2.3202217813003334 100.17266742256591 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 7.9813464183847707;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.10000000000002 0.43919102873622301 -0.086507323841961636 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 7.5128283551999333;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "TEMP_tempRoot_ctrl";
	addAttr -ci true -sn "_TYPE" -ln "_TYPE" -dt "string";
	addAttr -ci true -sn "shapeType" -ln "shapeType" -dt "string";
	addAttr -ci true -sn "nodes" -ln "nodes" -at "message";
	addAttr -ci true -sn "loftNodes" -ln "loftNodes" -at "message";
	addAttr -ci true -sn "surfaces" -ln "surfaces" -at "message";
	addAttr -ci true -sn "hip_loc" -ln "hip_loc" -at "message";
	addAttr -ci true -sn "upLegLFT_loc" -ln "upLegLFT_loc" -at "message";
	addAttr -ci true -sn "upLegRHT_loc" -ln "upLegRHT_loc" -at "message";
	setAttr ".ove" yes;
	setAttr ".ovc" 18;
	setAttr ".rp" -type "double3" -5.5511151231257827e-016 2.3776667611844418 0.021088135040003064 ;
	setAttr ".sp" -type "double3" -5.5511151231257827e-016 2.3776667611844418 0.021088135040003064 ;
	setAttr -l on "._TYPE" -type "string" "rigTempRoot";
	setAttr -l on ".shapeType" -type "string" "crossCircle";
createNode nurbsCurve -n "TEMP_tempRoot_ctrlShape" -p "TEMP_tempRoot_ctrl";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 49 0 no 3
		50 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
		 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49
		50
		-1.772028641110666 2.3776667611844418 0.021088135040003064
		-1.5746093592746542 2.3776667611844418 0.021088135040003064
		-1.5552185533516489 2.3776667611844418 -0.2252352021907354
		-1.4975364414420236 2.3776667611844418 -0.46548507329224509
		-1.4029864921698159 2.3776667611844418 -0.69376199163379182
		-1.2738778879695767 2.3776667611844418 -0.90443534799140357
		-1.1134213414044143 2.3776667611844418 -1.0923332063644091
		-0.92552348303140863 2.3776667611844418 -1.2527897529295706
		-0.71485012667379721 2.3776667611844418 -1.3818983571298102
		-0.48657320833225026 2.3776667611844418 -1.4764483064020166
		-0.24632333723073954 2.3776667611844418 -1.534130418311642
		-5.5511151231257827e-016 2.3776667611844418 -1.5535212242346459
		-5.5511151231257827e-016 2.3776667611844418 -1.7509405060706604
		-5.5511151231257827e-016 2.3776667611844418 -1.5535212242346459
		0.24632333723073843 2.3776667611844418 -1.534130418311642
		0.48657320833224904 2.3776667611844418 -1.4764483064020166
		0.7148501266737961 2.3776667611844418 -1.3818983571298102
		0.92552348303140752 2.3776667611844418 -1.2527897529295706
		1.1134213414044134 2.3776667611844418 -1.0923332063644091
		1.2738778879695758 2.3776667611844418 -0.90443534799140357
		1.402986492169815 2.3776667611844418 -0.69376199163379182
		1.4975364414420227 2.3776667611844418 -0.46548507329224509
		1.555218553351648 2.3776667611844418 -0.2252352021907354
		1.5746093592746528 2.3776667611844418 0.021088135040003064
		1.7720286411106652 2.3776667611844418 0.021088135040003064
		1.5746093592746528 2.3776667611844418 0.021088135040003064
		1.555218553351648 2.3776667611844418 0.26741147227074152
		1.4975364414420227 2.3776667611844418 0.50766134337225122
		1.402986492169815 2.3776667611844418 0.73593826171379795
		1.2738778879695758 2.3776667611844418 0.9466116180714097
		1.1134213414044134 2.3776667611844418 1.134509476444415
		0.92552348303140752 2.3776667611844418 1.2949660230095765
		0.7148501266737961 2.3776667611844418 1.4240746272098166
		0.48657320833224904 2.3776667611844418 1.518624576482023
		0.24632333723073843 2.3776667611844418 1.5763066883916483
		-5.5511151231257827e-016 2.3776667611844418 1.5956974943146522
		-5.5511151231257827e-016 2.3776667611844418 1.7931167761506663
		-5.5511151231257827e-016 2.3776667611844418 1.5956974943146522
		-0.24632333723073954 2.3776667611844418 1.5763066883916483
		-0.48657320833225026 2.3776667611844418 1.518624576482023
		-0.71485012667379721 2.3776667611844418 1.4240746272098166
		-0.92552348303140863 2.3776667611844418 1.2949660230095765
		-1.1134213414044143 2.3776667611844418 1.134509476444415
		-1.2738778879695767 2.3776667611844418 0.9466116180714097
		-1.4029864921698159 2.3776667611844418 0.73593826171379795
		-1.4975364414420236 2.3776667611844418 0.50766134337225122
		-1.5552185533516489 2.3776667611844418 0.26741147227074152
		-1.5746093592746542 2.3776667611844418 0.021088135040003064
		-1.772028641110666 2.3776667611844418 0.021088135040003064
		-1.5746093592746542 2.3776667611844418 0.021088135040003064
		;
createNode transform -n "TEMP_use_grp" -p "TEMP_tempRoot_ctrl";
createNode transform -n "TEMP_skirtPosSkin_nrbs" -p "TEMP_use_grp";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr ".ovdt" 2;
createNode nurbsSurface -n "TEMP_skirtPosSkin_nrbsShape" -p "TEMP_skirtPosSkin_nrbs";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 2 no 
		11 0 0 0 0.4103709391944485 0.82074187838889701 1.2311128175833455 1.635645145283138
		 2.0401774729829305 2.4447098006827233 2.4447098006827233 2.4447098006827233
		21 -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18
		
		171
		0.35992571730626449 2.3776667611844418 -0.72307648241012534
		3.235604978257672e-016 2.3776667611844418 -0.78210407333306076
		-0.35992571730626516 2.3776667611844418 -0.72307648241012534
		-0.66505600688740363 2.3776667611844418 -0.55498012527289076
		-0.86893754816764968 2.3776667611844418 -0.30340614849823
		-0.94053122467786077 2.3776667611844418 -0.0066544095263063622
		-0.86893754816764968 2.3776667611844418 0.29009732944561728
		-0.66505600688740363 2.3776667611844418 0.54167130622027826
		-0.3599257173062651 2.3776667611844418 0.70976766335751273
		-4.7532047808898832e-018 2.3776667611844418 0.76879525428044804
		0.35992571730626516 2.3776667611844418 0.70976766335751251
		0.66505600688740363 2.3776667611844418 0.54167130622027826
		0.86893754816765034 2.3776667611844418 0.29009732944561728
		0.9405312246778601 2.3776667611844418 -0.0066544095263064438
		0.86893754816764968 2.3776667611844418 -0.3034061484982305
		0.66505600688740329 2.3776667611844418 -0.55498012527289109
		0.35992571730626449 2.3776667611844418 -0.72307648241012534
		3.235604978257672e-016 2.3776667611844418 -0.78210407333306076
		-0.35992571730626516 2.3776667611844418 -0.72307648241012534
		0.37107690919087299 2.2439610927788016 -0.75653150025434235
		3.235604978257672e-016 2.2439192680000124 -0.81774323400177507
		-0.37107690919087366 2.2439610927788016 -0.75653150025434246
		-0.68564328069028613 2.2440657161811473 -0.58222818040225088
		-0.89581100194355345 2.2441810551475396 -0.32139414600914118
		-0.96960234817670499 2.2442511215187895 -0.013748861971372841
		-0.89579664841728102 2.2442472602082488 0.293885745075327
		-0.6856235453356796 2.2441846754759287 0.55469468020016854
		-0.37106334913455102 2.2441121541929587 0.72897384820377731
		-2.336534597932112e-017 2.2440808303087425 0.79017585241016486
		0.37106334913455108 2.2441121541929587 0.7289738482037772
		0.6856235453356796 2.2441846754759287 0.55469468020016843
		0.89579664841728157 2.2442472602082488 0.29388574507532689
		0.96960234817670421 2.2442511215187895 -0.013748861971372909
		0.89581100194355345 2.2441810551475396 -0.32139414600914168
		0.68564328069028568 2.2440657161811473 -0.58222818040225133
		0.37107690919087299 2.2439610927788016 -0.75653150025434235
		3.235604978257672e-016 2.2439192680000124 -0.81774323400177507
		-0.37107690919087366 2.2439610927788016 -0.75653150025434246
		0.39348974553069138 1.9769281781784807 -0.82443274547521372
		3.235604978257672e-016 1.9768598717233998 -0.89007807485142099
		-0.39348974553069194 1.9769281781784807 -0.82443274547521384
		-0.72704548996075924 1.977099164103207 -0.6375122718873707
		-0.94988923683546544 1.9772880071041443 -0.35781495351410553
		-1.0281253187739583 1.9774034002242158 -0.027938641172244847
		-0.94986512246372246 1.9773984557738822 0.30191979692704568
		-0.72701231942806377 1.977297715826843 0.58157510042757821
		-0.39346694357876311 1.977180438277188 0.76845514971670836
		-4.8193117009558303e-017 1.977129726645015 0.83408419547751
		0.39346694357876322 1.977180438277188 0.76845514971670825
		0.72701231942806377 1.977297715826843 0.58157510042757787
		0.9498651224637229 1.9773984557738822 0.3019197969270454
		1.0281253187739574 1.9774034002242158 -0.027938641172244892
		0.94988923683546544 1.9772880071041443 -0.35781495351410597
		0.72704548996075891 1.977099164103207 -0.63751227188737125
		0.39348974553069138 1.9769281781784807 -0.82443274547521372
		3.235604978257672e-016 1.9768598717233998 -0.89007807485142099
		-0.39348974553069194 1.9769281781784807 -0.82443274547521384
		0.42746860610387583 1.5771950152498828 -0.92935060409810721
		3.235604978257672e-016 1.5771706358379423 -1.0018754996034636
		-0.42746860610387638 1.5771950152498828 -0.92935060409810721
		-0.78984815340547687 1.5772562981534892 -0.72282587168684276
		-1.0319705648259287 1.5773247192734592 -0.41375950100095782
		-1.1169861609237033 1.5773679697742633 -0.04921075248379099
		-1.0319605162385246 1.5773691232353217 0.31533067989569019
		-0.78983430081486072 1.5773363199308437 0.62437986083868235
		-0.42745906142118317 1.577296950547102 0.83088806397285386
		-4.8210491158051583e-017 1.5772798050614258 0.90340630383335396
		0.42745906142118345 1.577296950547102 0.83088806397285375
		0.78983430081486072 1.5773363199308437 0.6243798608386818
		1.031960516238525 1.5773691232353217 0.31533067989568975
		1.1169861609237026 1.5773679697742633 -0.049210752483791032
		1.0319705648259287 1.5773247192734592 -0.41375950100095821
		0.78984815340547665 1.5772562981534892 -0.7228258716868432
		0.42746860610387583 1.5771950152498828 -0.92935060409810721
		3.235604978257672e-016 1.5771706358379423 -1.0018754996034636
		-0.42746860610387638 1.5771950152498828 -0.92935060409810721
		0.46167233530053631 1.1798523995364496 -1.0369142590976006
		3.235604978257672e-016 1.1798675542530201 -1.1165472649751589
		-0.46167233530053697 1.1798523995364496 -1.0369142590976006
		-0.85306529198679859 1.1798146107476457 -0.81013441492297833
		-1.1145921580783866 1.1797732987104042 -0.47072483531517151
		-1.2064313851560939 1.1797488815343913 -0.070354373425367142
		-1.1145966813222727 1.1797516519998756 0.33001951700403936
		-0.85307149658785264 1.1797758100880769 0.66943715994105379
		-0.46167658771605435 1.1798032567162551 0.89622476775461668
		-1.114545495234315e-017 1.1798150549315296 0.97586090247434187
		0.4616765877160548 1.1798032567162551 0.89622476775461657
		0.85307149658785264 1.1797758100880769 0.66943715994105324
		1.114596681322273 1.1797516519998756 0.33001951700403886
		1.2064313851560933 1.1797488815343913 -0.070354373425367212
		1.1145921580783866 1.1797732987104042 -0.4707248353151719
		0.85306529198679792 1.1798146107476457 -0.81013441492297866
		0.46167233530053631 1.1798523995364496 -1.0369142590976006
		3.235604978257672e-016 1.1798675542530201 -1.1165472649751589
		-0.46167233530053697 1.1798523995364496 -1.0369142590976006
		0.49612230269343305 0.7845783292322851 -1.1471623336574317
		3.235604978257672e-016 0.78454229293814004 -1.2341603492407518
		-0.49612230269343405 0.7845783292322851 -1.1471623336574317
		-0.91670116793397305 0.78466771107683542 -0.89942117468611371
		-1.1977090664142853 0.78476404881749251 -0.52866884156179217
		-1.2963795877358373 0.78481827025579975 -0.09135503307695314
		-1.1977009915095616 0.78480625655897662 0.34595236413809372
		-0.91669015799815856 0.78474295063582145 0.71668960307290153
		-0.49611480581021444 0.78467305876469329 0.96441620799727401
		6.2420219947843447e-017 0.78464322987166724 1.0514083524554252
		0.49611480581021505 0.78467305876469329 0.96441620799727401
		0.91669015799815856 0.78474295063582145 0.71668960307290097
		1.197700991509562 0.78480625655897662 0.34595236413809322
		1.2963795877358364 0.78481827025579975 -0.091355033076953265
		1.1977090664142853 0.78476404881749251 -0.5286688415617925
		0.91670116793397216 0.78466771107683542 -0.89942117468611404
		0.49612230269343305 0.7845783292322851 -1.1471623336574317
		3.235604978257672e-016 0.78454229293814004 -1.2341603492407518
		-0.49612230269343405 0.7845783292322851 -1.1471623336574317
		0.53078065947496389 0.39173358202588765 -1.2599452231132373
		3.235604978257672e-016 0.39165074452981635 -1.3545276641000479
		-0.53078065947496522 0.39173358202588765 -1.2599452231132373
		-0.98072312392307626 0.39193951092007379 -0.99061792117172365
		-1.2813316834630928 0.39216281818334542 -0.5875878013975302
		-1.3868755611706423 0.39229120830146103 -0.11222788262754188
		-1.2813105034331309 0.39226889078692301 0.36311559778646768
		-0.98069415892548384 0.39212909115712763 0.76610703625358056
		-0.53076087258992632 0.39197294732032939 1.0353970669728552
		1.7186334327720227e-016 0.39190611134836678 1.1299644797078905
		0.53076087258992721 0.39197294732032939 1.0353970669728549
		0.98069415892548384 0.39212909115712757 0.7661070362535799
		1.2813105034331314 0.39226889078692301 0.3631155977864674
		1.3868755611706414 0.39229120830146103 -0.11222788262754207
		1.2813316834630928 0.39216281818334542 -0.58758780139753064
		0.98072312392307504 0.39193951092007384 -0.99061792117172409
		0.53078065947496389 0.39173358202588765 -1.2599452231132373
		3.235604978257672e-016 0.39165074452981635 -1.3545276641000479
		-0.53078065947496522 0.39173358202588765 -1.2599452231132373
		0.55411444558635381 0.13043739696210233 -1.3371114719757622
		3.235604978257672e-016 0.13038852340609286 -1.4368982320192036
		-0.55411444558635536 0.13043739696210233 -1.3371114719757622
		-1.0238511859854507 0.13055895135950663 -1.052955257458974
		-1.3376993716777736 0.13069093147879862 -0.62771347727164684
		-1.4478987138049564 0.1307671468665077 -0.12613457319051175
		-1.337686550200579 0.13075463826950146 0.37543442099915042
		-1.023833642397403 0.13067286879769194 0.80065288430699577
		-0.55410245406367564 0.13058130659172981 1.0847866348213118
		2.6897158094783987e-016 0.13054208915262111 1.1845643378635333
		0.55410245406367664 0.13058130659172981 1.0847866348213113
		1.023833642397403 0.13067286879769191 0.80065288430699511
		1.3376865502005795 0.13075463826950146 0.37543442099915031
		1.4478987138049555 0.1307671468665077 -0.12613457319051205
		1.3376993716777736 0.13069093147879862 -0.62771347727164728
		1.0238511859854493 0.13055895135950668 -1.0529552574589744
		0.55411444558635381 0.13043739696210233 -1.3371114719757622
		3.235604978257672e-016 0.13038852340609286 -1.4368982320192036
		-0.55411444558635536 0.13043739696210233 -1.3371114719757622
		0.56583322368236011 0 -1.37617297793532
		3.235604978257672e-016 0 -1.4785934687199667
		-0.56583322368236177 0 -1.37617297793532
		-1.0455234683500296 0 -1.084504096859283
		-1.3660422426552454 0 -0.64799076865069127
		-1.4785934687199691 0 -0.13308819052612839
		-1.3660422426552454 0 0.38181438759843439
		-1.0455234683500296 0 0.81832771580702668
		-0.56583322368236089 0 1.1099965968830634
		3.235604978257672e-016 0 1.2124170876677098
		0.565833223682362 0 1.1099965968830632
		1.0455234683500296 0 0.81832771580702612
		1.3660422426552459 0 0.38181438759843439
		1.4785934687199682 0 -0.13308819052612872
		1.3660422426552454 0 -0.64799076865069183
		1.0455234683500283 0 -1.0845040968592832
		0.56583322368236011 0 -1.37617297793532
		3.235604978257672e-016 0 -1.4785934687199667
		-0.56583322368236177 0 -1.37617297793532
		
		;
createNode transform -n "TEMP_tmp_grp" -p "TEMP_tempRoot_ctrl";
createNode transform -n "TEMP_ctrl_grp" -p "TEMP_tmp_grp";
createNode transform -n "TEMP_hip_tmpLoc" -p "TEMP_ctrl_grp";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr -l on -k off ".v";
	setAttr -l on ".tx";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267757 ;
	setAttr ".sp" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267757 ;
createNode locator -n "TEMP_hip_tmpLocShape" -p "TEMP_hip_tmpLoc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".lp" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267761 ;
	setAttr ".los" -type "double3" 0.3 0.3 0.3 ;
createNode transform -n "TEMP_upLegLFT_tmpLoc" -p "TEMP_ctrl_grp";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr -l on -k off ".v";
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" 0.65052955929067524 1.7177713660101057 0.0018951183943218888 ;
	setAttr ".sp" -type "double3" 0.65052955929067524 1.7177713660101057 0.0018951183943218888 ;
createNode locator -n "TEMP_upLegLFT_tmpLocShape" -p "TEMP_upLegLFT_tmpLoc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".lp" -type "double3" 0.65052955929067524 1.7177713660101055 0.0018951183943218888 ;
	setAttr ".los" -type "double3" 0.1 0.1 0.1 ;
createNode transform -n "TEMP_upLegRHT_tmpLoc" -p "TEMP_ctrl_grp";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr -l on -k off ".v";
	setAttr ".r" -type "double3" 0 180 0 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".rp" -type "double3" -0.65053000000000016 1.7177674982481212 0.0018951200000008502 ;
	setAttr ".sp" -type "double3" -0.65053000000000016 1.7177674982481212 0.0018951200000008502 ;
createNode locator -n "TEMP_upLegRHT_tmpLocShape" -p "TEMP_upLegRHT_tmpLoc";
	setAttr -k off ".v";
	setAttr ".ove" yes;
	setAttr ".ovc" 13;
	setAttr ".lp" -type "double3" -0.65053000000000016 1.7177674982481212 0.0018951200000008502 ;
	setAttr ".los" -type "double3" 0.1 0.1 0.1 ;
createNode transform -n "TEMP_still_grp" -p "TEMP_tmp_grp";
	setAttr ".it" no;
createNode transform -n "TEMP_ann_grp" -p "TEMP_still_grp";
createNode transform -n "TEMP_upLegLFTPointer_loc" -p "TEMP_ann_grp";
	setAttr ".v" no;
createNode locator -n "TEMP_upLegLFTPointer_locShape" -p "TEMP_upLegLFTPointer_loc";
	setAttr -k off ".v";
createNode parentConstraint -n "TEMP_upLegLFTPointer_loc_parentConstraint1" -p "TEMP_upLegLFTPointer_loc";
	addAttr -ci true -k true -sn "w0" -ln "upLegLFT_tmpLocW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 0 180 ;
	setAttr ".rst" -type "double3" 0.65052955929067524 1.7177713660101057 0.0018951183943218888 ;
	setAttr -k on ".w0";
createNode transform -n "TEMP_upLegLFTPointer_ann" -p "TEMP_ann_grp";
createNode annotationShape -n "TEMP_upLegLFTPointer_annShape" -p "TEMP_upLegLFTPointer_ann";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "TEMP_upLegLFTPointer_ann_parentConstraint1" -p "TEMP_upLegLFTPointer_ann";
	addAttr -ci true -k true -sn "w0" -ln "hip_tmpLocW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267757 ;
	setAttr -k on ".w0";
createNode transform -n "TEMP_upLegRHTPointer_loc" -p "TEMP_ann_grp";
	setAttr ".v" no;
createNode locator -n "TEMP_upLegRHTPointer_locShape" -p "TEMP_upLegRHTPointer_loc";
	setAttr -k off ".v";
createNode parentConstraint -n "TEMP_upLegRHTPointer_loc_parentConstraint1" -p "TEMP_upLegRHTPointer_loc";
	addAttr -ci true -k true -sn "w0" -ln "upLegRHT_tmpLocW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 180 0 ;
	setAttr ".rst" -type "double3" -0.65053000000000016 1.7177674982481212 0.0018951200000008502 ;
	setAttr -k on ".w0";
createNode transform -n "TEMP_upLegRHTPointer_ann" -p "TEMP_ann_grp";
createNode annotationShape -n "TEMP_upLegRHTPointer_annShape" -p "TEMP_upLegRHTPointer_ann";
	setAttr -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr ".txt" -type "string" "";
createNode parentConstraint -n "TEMP_upLegRHTPointer_ann_parentConstraint1" -p "TEMP_upLegRHTPointer_ann";
	addAttr -ci true -k true -sn "w0" -ln "hip_tmpLocW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" 1.2325951644078309e-032 2.4479657363983005 0.027059386924267757 ;
	setAttr -k on ".w0";
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr ".minsp" 1;
	setAttr ".maxsp" 3;
	setAttr ".rflr" 10;
	setAttr ".rfrr" 10;
	setAttr ".maxr" 20;
	setAttr -s 28 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "256";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
	setAttr ".dat" 16;
createNode multDoubleLinear -n "TEMP_skirtPosLegMirror_mdl";
	addAttr -ci true -sn "tempRoot" -ln "tempRoot" -at "message";
	setAttr ".i2" -1;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 1;
	setAttr -av ".unw" 1;
lockNode -l 1 ;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
lockNode -l 1 ;
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
lockNode -l 1 ;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
lockNode -l 1 ;
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".s";
lockNode -l 1 ;
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
lockNode -l 1 ;
select -ne :defaultRenderUtilityList1;
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
lockNode -l 1 ;
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
lockNode -l 1 ;
select -ne :defaultObjectSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
lockNode -l 1 ;
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr -av ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
lockNode -l 1 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -av -k on ".rp";
	setAttr -k on ".cai";
	setAttr -k on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -k on ".ei";
	setAttr -k on ".ex";
	setAttr -av -k on ".es";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bf";
	setAttr -k on ".fii";
	setAttr -av -k on ".sf";
	setAttr -k on ".gr";
	setAttr -k on ".li";
	setAttr -k on ".ls";
	setAttr -k on ".mb";
	setAttr -k on ".ti";
	setAttr -k on ".txt";
	setAttr -k on ".mpr";
	setAttr -k on ".wzd";
	setAttr -k on ".fn" -type "string" "im";
	setAttr -k on ".if";
	setAttr -k on ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -k on ".as";
	setAttr -k on ".ds";
	setAttr -k on ".lm";
	setAttr -k on ".fir";
	setAttr -k on ".aap";
	setAttr -k on ".gh";
	setAttr -cb on ".sd";
lockNode -l 1 ;
connectAttr "TEMP_tempRoot_ctrl.surfaces" "TEMP_skirtPosSkin_nrbs.tempRoot";
connectAttr "TEMP_tempRoot_ctrl.hip_loc" "TEMP_hip_tmpLoc.tempRoot";
connectAttr "TEMP_tempRoot_ctrl.upLegLFT_loc" "TEMP_upLegLFT_tmpLoc.tempRoot";
connectAttr "TEMP_skirtPosLegMirror_mdl.o" "TEMP_upLegRHT_tmpLoc.tx";
connectAttr "TEMP_upLegLFT_tmpLoc.ty" "TEMP_upLegRHT_tmpLoc.ty";
connectAttr "TEMP_upLegLFT_tmpLoc.tz" "TEMP_upLegRHT_tmpLoc.tz";
connectAttr "TEMP_tempRoot_ctrl.upLegRHT_loc" "TEMP_upLegRHT_tmpLoc.tempRoot";
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.ctx" "TEMP_upLegLFTPointer_loc.tx"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.cty" "TEMP_upLegLFTPointer_loc.ty"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.ctz" "TEMP_upLegLFTPointer_loc.tz"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.crx" "TEMP_upLegLFTPointer_loc.rx"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.cry" "TEMP_upLegLFTPointer_loc.ry"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.crz" "TEMP_upLegLFTPointer_loc.rz"
		;
connectAttr "TEMP_upLegLFTPointer_loc.ro" "TEMP_upLegLFTPointer_loc_parentConstraint1.cro"
		;
connectAttr "TEMP_upLegLFTPointer_loc.pim" "TEMP_upLegLFTPointer_loc_parentConstraint1.cpim"
		;
connectAttr "TEMP_upLegLFTPointer_loc.rp" "TEMP_upLegLFTPointer_loc_parentConstraint1.crp"
		;
connectAttr "TEMP_upLegLFTPointer_loc.rpt" "TEMP_upLegLFTPointer_loc_parentConstraint1.crt"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.t" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tt"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.rp" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].trp"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.rpt" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].trt"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.r" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tr"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.ro" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tro"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.s" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].ts"
		;
connectAttr "TEMP_upLegLFT_tmpLoc.pm" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tpm"
		;
connectAttr "TEMP_upLegLFTPointer_loc_parentConstraint1.w0" "TEMP_upLegLFTPointer_loc_parentConstraint1.tg[0].tw"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.ctx" "TEMP_upLegLFTPointer_ann.tx"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.cty" "TEMP_upLegLFTPointer_ann.ty"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.ctz" "TEMP_upLegLFTPointer_ann.tz"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.crx" "TEMP_upLegLFTPointer_ann.rx"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.cry" "TEMP_upLegLFTPointer_ann.ry"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.crz" "TEMP_upLegLFTPointer_ann.rz"
		;
connectAttr "TEMP_upLegLFTPointer_locShape.wm" "TEMP_upLegLFTPointer_annShape.dom"
		 -na;
connectAttr "TEMP_upLegLFTPointer_ann.ro" "TEMP_upLegLFTPointer_ann_parentConstraint1.cro"
		;
connectAttr "TEMP_upLegLFTPointer_ann.pim" "TEMP_upLegLFTPointer_ann_parentConstraint1.cpim"
		;
connectAttr "TEMP_upLegLFTPointer_ann.rp" "TEMP_upLegLFTPointer_ann_parentConstraint1.crp"
		;
connectAttr "TEMP_upLegLFTPointer_ann.rpt" "TEMP_upLegLFTPointer_ann_parentConstraint1.crt"
		;
connectAttr "TEMP_hip_tmpLoc.t" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tt"
		;
connectAttr "TEMP_hip_tmpLoc.rp" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].trp"
		;
connectAttr "TEMP_hip_tmpLoc.rpt" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].trt"
		;
connectAttr "TEMP_hip_tmpLoc.r" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tr"
		;
connectAttr "TEMP_hip_tmpLoc.ro" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tro"
		;
connectAttr "TEMP_hip_tmpLoc.s" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].ts"
		;
connectAttr "TEMP_hip_tmpLoc.pm" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tpm"
		;
connectAttr "TEMP_upLegLFTPointer_ann_parentConstraint1.w0" "TEMP_upLegLFTPointer_ann_parentConstraint1.tg[0].tw"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.ctx" "TEMP_upLegRHTPointer_loc.tx"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.cty" "TEMP_upLegRHTPointer_loc.ty"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.ctz" "TEMP_upLegRHTPointer_loc.tz"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.crx" "TEMP_upLegRHTPointer_loc.rx"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.cry" "TEMP_upLegRHTPointer_loc.ry"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.crz" "TEMP_upLegRHTPointer_loc.rz"
		;
connectAttr "TEMP_upLegRHTPointer_loc.ro" "TEMP_upLegRHTPointer_loc_parentConstraint1.cro"
		;
connectAttr "TEMP_upLegRHTPointer_loc.pim" "TEMP_upLegRHTPointer_loc_parentConstraint1.cpim"
		;
connectAttr "TEMP_upLegRHTPointer_loc.rp" "TEMP_upLegRHTPointer_loc_parentConstraint1.crp"
		;
connectAttr "TEMP_upLegRHTPointer_loc.rpt" "TEMP_upLegRHTPointer_loc_parentConstraint1.crt"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.t" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tt"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.rp" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].trp"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.rpt" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].trt"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.r" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tr"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.ro" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tro"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.s" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].ts"
		;
connectAttr "TEMP_upLegRHT_tmpLoc.pm" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tpm"
		;
connectAttr "TEMP_upLegRHTPointer_loc_parentConstraint1.w0" "TEMP_upLegRHTPointer_loc_parentConstraint1.tg[0].tw"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.ctx" "TEMP_upLegRHTPointer_ann.tx"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.cty" "TEMP_upLegRHTPointer_ann.ty"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.ctz" "TEMP_upLegRHTPointer_ann.tz"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.crx" "TEMP_upLegRHTPointer_ann.rx"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.cry" "TEMP_upLegRHTPointer_ann.ry"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.crz" "TEMP_upLegRHTPointer_ann.rz"
		;
connectAttr "TEMP_upLegRHTPointer_locShape.wm" "TEMP_upLegRHTPointer_annShape.dom"
		 -na;
connectAttr "TEMP_upLegRHTPointer_ann.ro" "TEMP_upLegRHTPointer_ann_parentConstraint1.cro"
		;
connectAttr "TEMP_upLegRHTPointer_ann.pim" "TEMP_upLegRHTPointer_ann_parentConstraint1.cpim"
		;
connectAttr "TEMP_upLegRHTPointer_ann.rp" "TEMP_upLegRHTPointer_ann_parentConstraint1.crp"
		;
connectAttr "TEMP_upLegRHTPointer_ann.rpt" "TEMP_upLegRHTPointer_ann_parentConstraint1.crt"
		;
connectAttr "TEMP_hip_tmpLoc.t" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tt"
		;
connectAttr "TEMP_hip_tmpLoc.rp" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].trp"
		;
connectAttr "TEMP_hip_tmpLoc.rpt" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].trt"
		;
connectAttr "TEMP_hip_tmpLoc.r" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tr"
		;
connectAttr "TEMP_hip_tmpLoc.ro" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tro"
		;
connectAttr "TEMP_hip_tmpLoc.s" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].ts"
		;
connectAttr "TEMP_hip_tmpLoc.pm" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tpm"
		;
connectAttr "TEMP_upLegRHTPointer_ann_parentConstraint1.w0" "TEMP_upLegRHTPointer_ann_parentConstraint1.tg[0].tw"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "TEMP_upLegLFT_tmpLoc.tx" "TEMP_skirtPosLegMirror_mdl.i1";
connectAttr "TEMP_tempRoot_ctrl.nodes" "TEMP_skirtPosLegMirror_mdl.tempRoot";
connectAttr "TEMP_skirtPosSkin_nrbsShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "TEMP_skirtPosLegMirror_mdl.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of TEMPLATE_skirtPos_01.ma
